CREATE TYPE [dbo].[tvp_Invoice_Collection_Queue_Sel] AS TABLE
(
[ICO_User_Info_Id] [int] NULL,
[Collection_Start_Date] [date] NULL,
[Collection_End_Date] [date] NULL,
[Priority_Cd] [int] NULL,
[Client_Id] [int] NULL,
[Site_Id] [int] NULL,
[Account_Id] [int] NULL,
[Country_Id] [int] NULL,
[State_Id] [int] NULL,
[Commodity_Id] [int] NULL,
[Vendor_Type] [char] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor_Id] [int] NULL,
[Client_Contact_Info_Id] [int] NULL,
[Account_Contact_Info_Id] [int] NULL,
[Vendor_Contact_Info_Id] [int] NULL,
[Issue_Status] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Previously_Chased] [bit] NULL,
[Record_Type_Cd] [int] NULL,
[ICR_Status_Cd] [int] NULL,
[Days_Since_Last_Chased_From] [date] NULL,
[Days_Since_Last_Chased_To] [date] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Invoice_Collection_Queue_Sel] TO [CBMSApplication]
GO
