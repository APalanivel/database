CREATE TYPE [dbo].[tvp_Cbms_Image_Id_Encrypt] AS TABLE
(
[CBMS_IMAGE_ID] [bigint] NOT NULL,
[Encrypt_Cbms_Image_Id] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Cbms_Image_Id_Encrypt] TO [CBMSApplication]
GO
