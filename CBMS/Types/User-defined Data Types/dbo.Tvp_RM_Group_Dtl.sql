CREATE TYPE [dbo].[Tvp_RM_Group_Dtl] AS TABLE
(
[RM_Group_Participant_Id] [int] NOT NULL,
[RM_Group_Participant_Type_Cd] [int] NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[Tvp_RM_Group_Dtl] TO [CBMSApplication]
GO
