CREATE TYPE [dbo].[tvp_Ec_Account_Group_Type] AS TABLE
(
[Ec_Account_Group_Type_Id] [int] NULL,
[Group_Type_Name] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Ec_Account_Group_Type] TO [CBMSApplication]
GO
