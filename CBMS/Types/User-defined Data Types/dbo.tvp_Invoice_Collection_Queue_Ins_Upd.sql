CREATE TYPE [dbo].[tvp_Invoice_Collection_Queue_Ins_Upd] AS TABLE
(
[Invoice_Collection_Queue_Id] [int] NULL,
[Invoice_Collection_Account_Config_Id] [int] NOT NULL,
[Commodity_Id] [int] NULL,
[Invoice_Collection_Queue_Type_Cd] [int] NULL,
[Invoice_Collection_Exception_Type_Cd] [int] NULL,
[Collection_Start_Dt] [date] NOT NULL,
[Collection_End_Dt] [date] NOT NULL,
[Is_Manual] [bit] NULL,
[Invoice_Request_Type_Cd] [int] NULL,
[Status_Cd] [int] NOT NULL,
[Is_Locked] [bit] NULL,
[Received_Status_Updated_Dt] [date] NULL,
[Invoice_File_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account_Invoice_Collection_Month_Id] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Event_Desc] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Invoice_Collection_Queue_Ins_Upd] TO [CBMSApplication]
GO
