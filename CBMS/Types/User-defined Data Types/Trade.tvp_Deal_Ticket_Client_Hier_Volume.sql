CREATE TYPE [Trade].[tvp_Deal_Ticket_Client_Hier_Volume] AS TABLE
(
[Client_Hier_Id] [int] NOT NULL,
[Account_Id] [int] NOT NULL,
[Deal_Month] [date] NOT NULL,
[Total_Volume] [decimal] (24, 2) NOT NULL,
[Contract_Id] [int] NULL
)
GO
GRANT EXECUTE ON TYPE:: [Trade].[tvp_Deal_Ticket_Client_Hier_Volume] TO [CBMSApplication]
GO
