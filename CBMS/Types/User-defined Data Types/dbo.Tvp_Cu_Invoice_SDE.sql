CREATE TYPE [dbo].[Tvp_Cu_Invoice_SDE] AS TABLE
(
[Exception_Type_Cd] [int] NOT NULL,
[Account_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[Tvp_Cu_Invoice_SDE] TO [CBMSApplication]
GO
