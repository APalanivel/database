CREATE TYPE [dbo].[ImageFileNames] AS TABLE
(
[BillId] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Image_Filename] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Barcode] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[ImageFileNames] TO [CBMSApplication]
GO
