CREATE TYPE [dbo].[tvpCategory] AS TABLE
(
[Category_ID] [int] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpCategory] TO [CBMSApplication]
GO
