CREATE TYPE [Trade].[tvp_Trade_Alert_Upd] AS TABLE
(
[Trade_Id] [int] NOT NULL,
[Status_Id] [int] NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [Trade].[tvp_Trade_Alert_Upd] TO [CBMSApplication]
GO
