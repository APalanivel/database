CREATE TYPE [dbo].[Cost_Usage_Bucket] AS TABLE
(
[Commodity_Id] [int] NULL,
[Bucket_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Bucket_Type] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Bucket_Uom_Id] [int] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[Cost_Usage_Bucket] TO [CBMSApplication]
GO
