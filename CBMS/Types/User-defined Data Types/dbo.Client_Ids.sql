CREATE TYPE [dbo].[Client_Ids] AS TABLE
(
[Client_Id] [int] NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[Client_Ids] TO [CBMSApplication]
GO
