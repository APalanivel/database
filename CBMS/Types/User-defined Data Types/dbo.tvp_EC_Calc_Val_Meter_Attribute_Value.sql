CREATE TYPE [dbo].[tvp_EC_Calc_Val_Meter_Attribute_Value] AS TABLE
(
[EC_Meter_Attribute_Id] [int] NOT NULL,
[EC_Meter_Attribute_Value] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_EC_Calc_Val_Meter_Attribute_Value] TO [CBMSApplication]
GO
