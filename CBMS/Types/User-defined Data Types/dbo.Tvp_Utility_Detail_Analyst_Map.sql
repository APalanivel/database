CREATE TYPE [dbo].[Tvp_Utility_Detail_Analyst_Map] AS TABLE
(
[Utility_Detail_Id] [int] NOT NULL,
[Group_Info_Id] [int] NOT NULL,
[Analyst_Id] [int] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[Tvp_Utility_Detail_Analyst_Map] TO [CBMSApplication]
GO
