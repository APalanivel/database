CREATE TYPE [dbo].[tvp_CalcVal_Dates_Calculation_Tester] AS TABLE
(
[Actual_Date] [date] NULL,
[Calculated_Date] [date] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_CalcVal_Dates_Calculation_Tester] TO [CBMSApplication]
GO
