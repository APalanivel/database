CREATE TYPE [dbo].[Invoice_List] AS TABLE
(
[invoice_id] [int] NULL,
[Exception_type] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[Invoice_List] TO [CBMSApplication]
GO
