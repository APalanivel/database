CREATE TYPE [dbo].[Tvp_Budget_Details] AS TABLE
(
[Budget_Account_ID] [int] NOT NULL,
[Month_Identifier] [smalldatetime] NOT NULL,
[Variable] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Variable_Value] [decimal] (32, 16) NULL,
[Transportation] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Transportation_Value] [decimal] (32, 16) NULL,
[Transmission] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Transmission_Value] [decimal] (32, 16) NULL,
[Distribution] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Distribution_Value] [decimal] (32, 16) NULL,
[Other_Bundled] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Other_Bundled_Value] [decimal] (32, 16) NULL,
[Other_Fixed_Costs] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Other_Fixed_Costs_Value] [decimal] (32, 16) NULL,
[Sourcing_Tax] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sourcing_Tax_Value] [decimal] (32, 16) NULL,
[Rates_Tax] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Rates_Tax_Value] [decimal] (32, 16) NULL,
[Budget_Usage] [decimal] (32, 16) NULL,
[Is_Manual_Generation] [bit] NULL,
[Is_Manual_Sourcing_Tax] [bit] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[Tvp_Budget_Details] TO [CBMSApplication]
GO
