CREATE TYPE [dbo].[tvp_EC_Contract_Attribute_Value] AS TABLE
(
[EC_Contract_Attribute_value_Id] [int] NULL,
[EC_Contract_Attribute_Value] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_EC_Contract_Attribute_Value] TO [CBMSApplication]
GO
