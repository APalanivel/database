CREATE TYPE [dbo].[tvp_Invoice_Collection_Exception_Queue_Sel] AS TABLE
(
[ICO_User_Info_Id] [int] NULL,
[Collection_Start_Date] [date] NULL,
[Collection_End_Date] [date] NULL,
[Exception_Type_Cd] [int] NULL,
[Priority_Cd] [int] NULL,
[Client_Id] [int] NULL,
[Site_Id] [int] NULL,
[Account_Id] [int] NULL,
[Ubm_ID] [int] NULL,
[Country_Id] [int] NULL,
[State_Id] [int] NULL,
[Commodity_Id] [int] NULL,
[Vendor_Type] [char] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor_Id] [int] NULL,
[Comment_Id] [int] NULL,
[Status_Cd] [int] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Invoice_Collection_Exception_Queue_Sel] TO [CBMSApplication]
GO
