CREATE TYPE [Trade].[Tvp_Deal_Ticket_Financial_Counter_Party_Contact] AS TABLE
(
[RM_COUNTERPARTY_ID] [int] NOT NULL,
[Contact_Info_Id] [int] NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [Trade].[Tvp_Deal_Ticket_Financial_Counter_Party_Contact] TO [CBMSApplication]
GO
