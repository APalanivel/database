CREATE TYPE [dbo].[tvp_Cu_Invoice_Recalc_Response_Is_Hide_On_RA_Update] AS TABLE
(
[Cu_Invoice_Recalc_Response_Id] [int] NOT NULL,
[Is_Hidden_On_RA] [bit] NOT NULL DEFAULT ((0))
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Cu_Invoice_Recalc_Response_Is_Hide_On_RA_Update] TO [CBMSApplication]
GO
