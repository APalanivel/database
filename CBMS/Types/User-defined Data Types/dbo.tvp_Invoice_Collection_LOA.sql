CREATE TYPE [dbo].[tvp_Invoice_Collection_LOA] AS TABLE
(
[Invoice_Collection_LOA_Id] [int] NULL,
[Seq_No] [smallint] NOT NULL,
[LOA_Valid_From_Dt] [date] NOT NULL,
[LOA_Valid_End_Dt] [date] NULL,
[Cbms_Image_Id] [int] NOT NULL,
[LOA_Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Operation_Type] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Invoice_Collection_LOA] TO [CBMSApplication]
GO
