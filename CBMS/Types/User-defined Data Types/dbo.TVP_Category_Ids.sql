CREATE TYPE [dbo].[TVP_Category_Ids] AS TABLE
(
[category_Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[TVP_Category_Ids] TO [CBMSApplication]
GO
