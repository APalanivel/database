CREATE TYPE [dbo].[Get_CostUsage_Key] AS TABLE
(
[Row_Number] [int] NULL,
[Service_Month] [datetime] NULL,
[Client_Hier_Id] [int] NULL,
[Account_Id] [int] NULL
)
GO

GRANT EXECUTE ON TYPE:: [dbo].[Get_CostUsage_Key] TO [CBMSApplication]
GO
