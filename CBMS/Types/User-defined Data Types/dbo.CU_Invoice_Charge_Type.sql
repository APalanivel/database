CREATE TYPE [dbo].[CU_Invoice_Charge_Type] AS TABLE
(
[CU_INVOICE_CHARGE_ID] [int] NULL,
[CU_INVOICE_ID] [int] NULL,
[COMMODITY_TYPE_ID] [int] NULL,
[UBM_SERVICE_TYPE_ID] [int] NULL,
[UBM_SERVICE_CODE] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UBM_BUCKET_CODE] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CHARGE_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CHARGE_VALUE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UBM_INVOICE_DETAILS_ID] [int] NULL,
[CU_DETERMINANT_CODE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Bucket_Master_Id] [int] NULL,
[Is_Delete] [bit] NULL,
[EC_Invoice_Sub_Bucket_Master_Id] [int] NULL
)
GO

GRANT EXECUTE ON TYPE:: [dbo].[CU_Invoice_Charge_Type] TO [CBMSApplication]
GO
