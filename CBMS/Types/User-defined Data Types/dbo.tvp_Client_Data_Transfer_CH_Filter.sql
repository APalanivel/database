CREATE TYPE [dbo].[tvp_Client_Data_Transfer_CH_Filter] AS TABLE
(
[Client_Data_Transfer_CH_Filter_Id] [int] NOT NULL,
[Client_Data_Transfer_Setup_Id] [int] NOT NULL,
[Client_Hier_Id] [int] NOT NULL,
[Hier_Level_Cd] [int] NOT NULL,
[Is_Copy_All_Site] [bit] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Client_Data_Transfer_CH_Filter] TO [CBMSApplication]
GO
