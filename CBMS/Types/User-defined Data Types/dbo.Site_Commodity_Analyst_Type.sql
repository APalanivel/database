CREATE TYPE [dbo].[Site_Commodity_Analyst_Type] AS TABLE
(
[Site_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Analyst_User_Info_Id] [int] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[Site_Commodity_Analyst_Type] TO [CBMSApplication]
GO
