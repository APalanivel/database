CREATE TYPE [dbo].[tvp_Variance_log_Ven] AS TABLE
(
[Variance_log_Id] [int] NULL,
[Category_name] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Variance_log_Ven] TO [CBMSApplication]
GO
