CREATE TYPE [dbo].[Tvp_Budget_Usage] AS TABLE
(
[Account_Id] [int] NOT NULL,
[Month_Identifier] [smalldatetime] NOT NULL,
[Volume] [decimal] (32, 16) NULL,
PRIMARY KEY CLUSTERED  ([Account_Id], [Month_Identifier])
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[Tvp_Budget_Usage] TO [CBMSApplication]
GO
