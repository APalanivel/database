CREATE TYPE [dbo].[Holiday_Master_Details] AS TABLE
(
[Old_Holiday_Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[New_Holiday_Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Year_Identifier] [int] NOT NULL,
[Holiday_Dt] [date] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[Holiday_Master_Details] TO [CBMSApplication]
GO
