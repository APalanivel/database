CREATE TYPE [dbo].[tvp_RM_Client_Hier_Forecast_Volume] AS TABLE
(
[Client_Hier_Id] [int] NOT NULL,
[Service_Month] [date] NOT NULL,
[Forecast_Volume] [decimal] (28, 12) NOT NULL,
[Uom_Id] [int] NOT NULL,
[Volume_Source_Cd] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
PRIMARY KEY CLUSTERED  ([Client_Hier_Id], [Service_Month])
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_RM_Client_Hier_Forecast_Volume] TO [CBMSApplication]
GO
