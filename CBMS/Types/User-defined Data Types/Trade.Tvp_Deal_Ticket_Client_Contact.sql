CREATE TYPE [Trade].[Tvp_Deal_Ticket_Client_Contact] AS TABLE
(
[Contact_Info_Id] [int] NOT NULL,
[Client_Hier_Id] [int] NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [Trade].[Tvp_Deal_Ticket_Client_Contact] TO [CBMSApplication]
GO
