CREATE TYPE [dbo].[tvp_Client_Data_Transfer_Data_Map] AS TABLE
(
[From_Key_Id] [int] NOT NULL,
[To_Key_Id] [int] NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Client_Data_Transfer_Data_Map] TO [CBMSApplication]
GO
