CREATE TYPE [dbo].[tvp_Invoice_Recalc_Process] AS TABLE
(
[Cu_Invoice_Id] [int] NOT NULL,
[Status_Cd] [int] NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Invoice_Recalc_Process] TO [CBMSApplication]
GO
