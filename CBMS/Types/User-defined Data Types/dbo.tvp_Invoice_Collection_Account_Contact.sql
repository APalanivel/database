CREATE TYPE [dbo].[tvp_Invoice_Collection_Account_Contact] AS TABLE
(
[Invoice_Collection_Account_Config_Id] [int] NOT NULL,
[Contact_Info_Id] [int] NULL,
[Is_Primary] [bit] NULL,
[Is_CC] [bit] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Invoice_Collection_Account_Contact] TO [CBMSApplication]
GO
