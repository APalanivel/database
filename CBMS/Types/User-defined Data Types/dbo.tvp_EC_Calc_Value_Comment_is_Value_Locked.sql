CREATE TYPE [dbo].[tvp_EC_Calc_Value_Comment_is_Value_Locked] AS TABLE
(
[Ec_Calc_Val_Id] [int] NOT NULL,
[Calc_Value] [decimal] (28, 10) NULL,
[Uom_Id] [int] NULL,
[Currency_Unit_Id] [int] NULL,
[Comment_Text] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Is_Value_Locked] [bit] NOT NULL,
[Expected_Slots] [int] NULL,
[Actual_Slots] [int] NULL,
[Missing_Slots] [int] NULL,
[Edited_Slots] [int] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_EC_Calc_Value_Comment_is_Value_Locked] TO [CBMSApplication]
GO
