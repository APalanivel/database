CREATE TYPE [dbo].[tvp_Cbms_Smartgroup_Rule] AS TABLE
(
[Cbms_Rule_Filter_Id] [int] NULL,
[Filter_Condition_Id] [int] NULL,
[Filter_Value] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Cbms_Smartgroup_Rule] TO [CBMSApplication]
GO
