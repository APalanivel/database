CREATE TYPE [dbo].[tvp_Calc_Val_Conversion] AS TABLE
(
[EC_Calc_Val_Id] [int] NULL,
[Calc_Value] [decimal] (28, 10) NULL,
[Base_Uom] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Converted_Uom] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Calc_Val_Conversion] TO [CBMSApplication]
GO
