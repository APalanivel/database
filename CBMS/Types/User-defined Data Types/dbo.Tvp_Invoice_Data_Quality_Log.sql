CREATE TYPE [dbo].[Tvp_Invoice_Data_Quality_Log] AS TABLE
(
[Account_ID] [int] NULL,
[Commodity_ID] [int] NULL,
[Service_Month] [date] NULL,
[UOM_Id] [int] NULL,
[Currency_Unit_Id] [int] NULL,
[Baseline_UOM_Id] [int] NULL,
[Test_Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Test_Category] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Execution_Ts] [datetime] NOT NULL,
[Is_Failure] [bit] NOT NULL,
[Is_Closed] [bit] NOT NULL,
[Current_Value] [decimal] (16, 4) NULL,
[Baseline_Value] [decimal] (16, 4) NULL,
[Is_Inclusive] [bit] NOT NULL,
[Is_Multiple_Condition] [bit] NOT NULL,
[Baseline_Desc] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Baseline_Start_Service_Month] [date] NULL,
[Baseline_End_Service_Month] [date] NULL,
[Partition_Key] [date] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[Tvp_Invoice_Data_Quality_Log] TO [CBMSApplication]
GO
