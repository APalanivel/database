CREATE TYPE [dbo].[tvp_Client_Data_Transfer_CH_Map] AS TABLE
(
[From_Client_Hier_Id] [int] NOT NULL,
[To_Client_Hier_Id] [int] NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Client_Data_Transfer_CH_Map] TO [CBMSApplication]
GO
