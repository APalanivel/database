CREATE TYPE [dbo].[TVP_Variance_log_Detail] AS TABLE
(
[Request_data] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Response_data] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Error_desc] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[TVP_Variance_log_Detail] TO [CBMSApplication]
GO
