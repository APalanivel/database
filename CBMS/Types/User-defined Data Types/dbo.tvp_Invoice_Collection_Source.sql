CREATE TYPE [dbo].[tvp_Invoice_Collection_Source] AS TABLE
(
[Column_Type] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column_Name] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column_Value] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Invoice_Collection_Source] TO [CBMSApplication]
GO
