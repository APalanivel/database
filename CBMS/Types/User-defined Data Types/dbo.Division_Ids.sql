CREATE TYPE [dbo].[Division_Ids] AS TABLE
(
[Division_Id] [int] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[Division_Ids] TO [CBMSApplication]
GO
