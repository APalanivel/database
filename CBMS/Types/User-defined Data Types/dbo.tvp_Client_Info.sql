CREATE TYPE [dbo].[tvp_Client_Info] AS TABLE
(
[Contact_Info_Id] [int] NULL,
[First_Name] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Last_Name] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Email_Address] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone_Number] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fax_Number] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Job_Position] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Location] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comment_Text] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comment_Id] [int] NULL,
[Is_Active] [bit] NOT NULL,
[Language_Cd] [int] NOT NULL,
[Operation_Type] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Client_Info] TO [CBMSApplication]
GO
