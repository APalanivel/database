CREATE TYPE [dbo].[tvp_Invoice_Collection_Queue_Collection_Start_End_Date] AS TABLE
(
[Invoice_Collection_Queue_Id] [int] NULL,
[Collection_Start_Date] [date] NULL,
[Collection_End_Date] [date] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Invoice_Collection_Queue_Collection_Start_End_Date] TO [CBMSApplication]
GO
