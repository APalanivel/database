CREATE TYPE [dbo].[Tvp_IP_Processing_Invoice] AS TABLE
(
[Processing_Cu_Invoice_Id] [int] NOT NULL,
[Cu_Invoice_Id] [int] NOT NULL,
[Account_Id] [int] NOT NULL,
[Service_Month] [date] NOT NULL,
[Is_Reported] [bit] NOT NULL,
[Is_Previously_Reported] [bit] NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[Tvp_IP_Processing_Invoice] TO [CBMSApplication]
GO
