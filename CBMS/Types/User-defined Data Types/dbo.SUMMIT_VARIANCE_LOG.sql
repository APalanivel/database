CREATE TYPE [dbo].[SUMMIT_VARIANCE_LOG] AS TABLE
(
[Client_ID] [int] NULL,
[Client_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Site_ID] [int] NULL,
[Site_Name] [varchar] (807) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State_ID] [int] NULL,
[State_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account_ID] [int] NULL,
[Account_Number] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Service_Month] [date] NULL,
[Queue_Id] [int] NULL,
[Queue_Name] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Queue_Dt] [datetime] NULL,
[Is_Failure] [bit] NULL,
[UOM_Cd] [int] NULL,
[UOM_Label] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Execution_Dt] [datetime] NULL,
[Current_Value] [decimal] (32, 8) NULL,
[Baseline_Value] [decimal] (32, 8) NULL,
[Low_Tolerance] [decimal] (32, 8) NULL,
[High_Tolerance] [decimal] (32, 8) NULL,
[Test_Description] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Category_Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Commodity_ID] [int] NULL,
[Commodity_Desc] [varchar] (55) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Consumtion_Level_Desc] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Variance_Status_Cd] [int] NULL,
[Variance_Status_Desc] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor_name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor_ID] [int] NULL,
[Is_Inclusive] [bit] NULL,
[Is_Multiple_Condition] [bit] NULL,
[Ref_Variance_Log_Id] [int] NULL,
[Baseline_Cd] [int] NULL,
[Baseline_Desc] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Baseline_Service_Month] [date] NULL,
[Baseline_Account_Id] [int] NULL,
[AVT_R2_Score] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO



GRANT EXECUTE ON TYPE:: [dbo].[SUMMIT_VARIANCE_LOG] TO [CBMSApplication]
GO
