CREATE TYPE [Trade].[tvp_Rm_Budget_Volume_Price_Dtl] AS TABLE
(
[Client_Hier_Id] [int] NOT NULL,
[Service_Month] [date] NOT NULL,
[Forecast_Volume] [decimal] (28, 3) NULL,
[Total_Volume_Hedged] [decimal] (28, 3) NULL,
[WACOG_Hedge_Price] [decimal] (28, 3) NULL,
[Budget_Target_Price] [decimal] (28, 3) NULL
)
GO
GRANT EXECUTE ON TYPE:: [Trade].[tvp_Rm_Budget_Volume_Price_Dtl] TO [CBMSApplication]
GO
