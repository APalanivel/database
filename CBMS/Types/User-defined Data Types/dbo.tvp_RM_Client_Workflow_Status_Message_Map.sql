CREATE TYPE [dbo].[tvp_RM_Client_Workflow_Status_Message_Map] AS TABLE
(
[Workflow_Status_Map_Id] [int] NOT NULL,
[Workflow_Status_Message] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_RM_Client_Workflow_Status_Message_Map] TO [CBMSApplication]
GO
