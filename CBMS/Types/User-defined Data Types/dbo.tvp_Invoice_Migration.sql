CREATE TYPE [dbo].[tvp_Invoice_Migration] AS TABLE
(
[Status] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UBM_BATCH_MASTER_LOG_ID] [int] NOT NULL,
[CBMS_IMAGE_ID] [int] NOT NULL,
[CBMS_IMAGE_SIZE] [decimal] (18, 0) NULL,
[CBMS_IMAGE_CONTENT_TYPE] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CBMS_Image_FileName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CBMS_Image_Directory] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Invoice_Migration] TO [CBMSApplication]
GO
