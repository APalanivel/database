CREATE TYPE [dbo].[TVP_Consumption_Level_Params] AS TABLE
(
[Category] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Average_Consumption_Level] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AA_Distance] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AA_Max_Accounts] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AA_Min_Accounts] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Error_Metric] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Linear_Regression_Sigma_Value] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[TVP_Consumption_Level_Params] TO [CBMSApplication]
GO
