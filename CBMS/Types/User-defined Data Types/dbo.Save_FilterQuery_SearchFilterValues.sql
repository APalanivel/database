CREATE TYPE [dbo].[Save_FilterQuery_SearchFilterValues] AS TABLE
(
[SearchFilter_Name] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SearchFilterValue] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ControlType] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[Save_FilterQuery_SearchFilterValues] TO [CBMSApplication]
GO
