CREATE TYPE [Trade].[tvp_Hedge_Contract_Sites] AS TABLE
(
[Contract_Id] [int] NOT NULL,
[Site_Id] [int] NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [Trade].[tvp_Hedge_Contract_Sites] TO [CBMSApplication]
GO
