CREATE TYPE [dbo].[tvp_Forecast_Month] AS TABLE
(
[Forecast_Month] [date] NOT NULL,
[Source_Month] [date] NULL,
PRIMARY KEY CLUSTERED  ([Forecast_Month])
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Forecast_Month] TO [CBMSApplication]
GO
