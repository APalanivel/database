CREATE TYPE [dbo].[IDM_Site_Info] AS TABLE
(
[Client_Hier_Id] [int] NOT NULL,
[Is_IDM_Enabled] [bit] NULL,
[IDM_Node_Id] [int] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[IDM_Site_Info] TO [CBMSApplication]
GO
