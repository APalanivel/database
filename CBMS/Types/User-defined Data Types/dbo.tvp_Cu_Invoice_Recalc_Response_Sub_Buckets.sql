CREATE TYPE [dbo].[tvp_Cu_Invoice_Recalc_Response_Sub_Buckets] AS TABLE
(
[Sub_Bucket_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Determinant_Value] [decimal] (28, 10) NULL,
[Uom_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Expected_Slots] [int] NULL,
[Actual_Slots] [int] NULL,
[Missing_Slots] [int] NULL,
[Edited_Slots] [int] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Cu_Invoice_Recalc_Response_Sub_Buckets] TO [CBMSApplication]
GO
