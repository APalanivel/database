CREATE TYPE [Trade].[tvp_Deal_Total_Hedge_Volume] AS TABLE
(
[Deal_Month] [date] NOT NULL,
[Total_Volume] [decimal] (28, 6) NOT NULL,
[Client_Generated_Price] [decimal] (28, 6) NULL,
[Trigger_Target_Price] [decimal] (28, 6) NULL,
[Client_Generated_Fixed_Dt] [date] NULL,
UNIQUE NONCLUSTERED  ([Deal_Month])
)
GO
GRANT EXECUTE ON TYPE:: [Trade].[tvp_Deal_Total_Hedge_Volume] TO [CBMSApplication]
GO
