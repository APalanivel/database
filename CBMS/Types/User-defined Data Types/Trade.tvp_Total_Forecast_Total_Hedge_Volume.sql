CREATE TYPE [Trade].[tvp_Total_Forecast_Total_Hedge_Volume] AS TABLE
(
[Deal_Month] [date] NOT NULL,
[Total_Hedge_Volume] [decimal] (28, 6) NOT NULL,
[Total_Forecast_Volume] [decimal] (28, 6) NOT NULL,
UNIQUE NONCLUSTERED  ([Deal_Month])
)
GO
GRANT EXECUTE ON TYPE:: [Trade].[tvp_Total_Forecast_Total_Hedge_Volume] TO [CBMSApplication]
GO
