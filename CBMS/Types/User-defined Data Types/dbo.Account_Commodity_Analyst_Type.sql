CREATE TYPE [dbo].[Account_Commodity_Analyst_Type] AS TABLE
(
[Account_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Analyst_User_Info_Id] [int] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[Account_Commodity_Analyst_Type] TO [CBMSApplication]
GO
