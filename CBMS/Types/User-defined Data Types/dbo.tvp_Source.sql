CREATE TYPE [dbo].[tvp_Source] AS TABLE
(
[Source_Id] [int] NOT NULL,
[Source_Type] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Source] TO [CBMSApplication]
GO
