CREATE TYPE [dbo].[tvp_Invoice_Collection_Queue_Month] AS TABLE
(
[Invoice_Collection_Queue_Id] [int] NOT NULL,
[Account_Invoice_Collection_Month_Id] [int] NOT NULL,
PRIMARY KEY CLUSTERED  ([Invoice_Collection_Queue_Id], [Account_Invoice_Collection_Month_Id])
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Invoice_Collection_Queue_Month] TO [CBMSApplication]
GO
