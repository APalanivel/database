CREATE TYPE [dbo].[CU_Invoice_Determinant_Type] AS TABLE
(
[CU_INVOICE_DETERMINANT_ID] [int] NULL,
[CU_INVOICE_ID] [int] NULL,
[COMMODITY_TYPE_ID] [int] NULL,
[UBM_METER_NUMBER] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UBM_SERVICE_TYPE_ID] [int] NULL,
[UBM_SERVICE_CODE] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UBM_BUCKET_CODE] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UNIT_OF_MEASURE_TYPE_ID] [int] NULL,
[UBM_UNIT_OF_MEASURE_CODE] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DETERMINANT_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DETERMINANT_VALUE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UBM_INVOICE_DETAILS_ID] [int] NULL,
[CU_DETERMINANT_NO] [int] NULL,
[CU_DETERMINANT_CODE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Bucket_Master_Id] [int] NULL,
[Is_Delete] [bit] NULL,
[EC_Invoice_Sub_Bucket_Master_Id] [int] NULL
)
GO

GRANT EXECUTE ON TYPE:: [dbo].[CU_Invoice_Determinant_Type] TO [CBMSApplication]
GO
