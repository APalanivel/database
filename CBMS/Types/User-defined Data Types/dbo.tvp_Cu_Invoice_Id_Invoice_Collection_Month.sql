CREATE TYPE [dbo].[tvp_Cu_Invoice_Id_Invoice_Collection_Month] AS TABLE
(
[Cu_Invoice_Id] [int] NOT NULL,
[Account_Invoice_Collection_Month_Id] [int] NOT NULL,
[Invoice_Begin_Dt] [date] NULL,
[Invoice_End_Dt] [date] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Cu_Invoice_Id_Invoice_Collection_Month] TO [CBMSApplication]
GO
