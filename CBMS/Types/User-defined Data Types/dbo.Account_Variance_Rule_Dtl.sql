CREATE TYPE [dbo].[Account_Variance_Rule_Dtl] AS TABLE
(
[Account_Id] [int] NOT NULL,
[Variance_Rule_Dtl_Id] [int] NOT NULL,
PRIMARY KEY CLUSTERED  ([Account_Id], [Variance_Rule_Dtl_Id])
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[Account_Variance_Rule_Dtl] TO [CBMSApplication]
GO
