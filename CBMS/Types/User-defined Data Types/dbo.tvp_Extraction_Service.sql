CREATE TYPE [dbo].[tvp_Extraction_Service] AS TABLE
(
[Bucket_Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Bucket_Type] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Bucket_Master_Id] [int] NOT NULL,
[Default_uom_Type_Id] [int] NULL,
[Service_Type] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Variance_category_cd] [int] NULL,
[variance_category_name] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[perform_test] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Extraction_Service] TO [CBMSApplication]
GO
