CREATE TYPE [dbo].[tvp_Invoice_Collection_Queue_Account_Config_Id_Sel] AS TABLE
(
[Invoice_Collection_Queue_Id] [int] NULL,
[Invoice_Collection_Account_Config_Id] [int] NULL,
[Invoice_Collection_Issue_Log_Id] [int] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Invoice_Collection_Queue_Account_Config_Id_Sel] TO [CBMSApplication]
GO
