CREATE TYPE [Trade].[tvp_Rm_Budget_Current_Hedge_Position_Dtl] AS TABLE
(
[Service_Month] [date] NOT NULL,
[Total_Forecast_Volume] [decimal] (28, 3) NULL,
[Total_Volume_Hedged] [decimal] (28, 3) NULL,
[Hedged_Unit_Cost] [decimal] (28, 3) NULL
)
GO
GRANT EXECUTE ON TYPE:: [Trade].[tvp_Rm_Budget_Current_Hedge_Position_Dtl] TO [CBMSApplication]
GO
