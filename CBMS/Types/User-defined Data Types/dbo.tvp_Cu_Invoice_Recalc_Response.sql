CREATE TYPE [dbo].[tvp_Cu_Invoice_Recalc_Response] AS TABLE
(
[Charge_Name] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Bucket_Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Determinant_Unit] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Determinant_Value] [decimal] (28, 10) NULL,
[Rate_Unit] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Rate_Currency] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Rate_Amount] [decimal] (28, 10) NULL,
[Net_Amount] [decimal] (28, 10) NULL,
[Calculator_Working] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Net_Amount_Currency] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Is_Hidden_On_RA] [bit] NOT NULL DEFAULT ((0)),
[Is_Valid_Charge] [bit] NOT NULL DEFAULT ((1)),
[Charge_GUID] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Is_Locked] [bit] NOT NULL DEFAULT ((0)),
[Is_Send_To_Sys] [bit] NOT NULL DEFAULT ((0)),
[Comment_Text] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO


GRANT EXECUTE ON TYPE:: [dbo].[tvp_Cu_Invoice_Recalc_Response] TO [CBMSApplication]
GO
