CREATE TYPE [dbo].[tvp_Bucket] AS TABLE
(
[Bucket_Master_Id] [int] NOT NULL,
[Uom_Id] [int] NULL,
PRIMARY KEY CLUSTERED  ([Bucket_Master_Id])
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Bucket] TO [CBMSApplication]
GO
