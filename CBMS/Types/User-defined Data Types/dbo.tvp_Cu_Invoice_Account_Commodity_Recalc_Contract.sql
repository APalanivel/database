CREATE TYPE [dbo].[tvp_Cu_Invoice_Account_Commodity_Recalc_Contract] AS TABLE
(
[Contract_Id] [int] NOT NULL,
[Contract_Recalc_Begin_Dt] [date] NULL,
[Contract_Recalc_End_Dt] [date] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Cu_Invoice_Account_Commodity_Recalc_Contract] TO [CBMSApplication]
GO
