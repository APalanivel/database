CREATE TYPE [dbo].[tvp_Sites_To_RM_Group] AS TABLE
(
[Filter_Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Filter_Condition] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Filter_Value] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Sites_To_RM_Group] TO [CBMSApplication]
GO
