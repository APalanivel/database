CREATE TYPE [dbo].[tvp_Invoice_Collection_Queue_Received_Status_Upd_dtls] AS TABLE
(
[Invoice_Collection_Queue_Id] [int] NOT NULL,
[CBMS_Image_FileName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[File_Received_Date] [date] NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Invoice_Collection_Queue_Received_Status_Upd_dtls] TO [CBMSApplication]
GO
