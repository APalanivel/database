CREATE TYPE [Trade].[tvp_Total_Forecast_Current_Deal_Hedged_ToDate_Volumes] AS TABLE
(
[Deal_Month] [date] NOT NULL,
[Total_Hedge_Volume] [decimal] (28, 6) NOT NULL,
[Total_Forecast_Volume] [decimal] (28, 6) NOT NULL,
[Volume_Hedged_ToDate] [decimal] (28, 6) NULL,
UNIQUE NONCLUSTERED  ([Deal_Month])
)
GO
GRANT EXECUTE ON TYPE:: [Trade].[tvp_Total_Forecast_Current_Deal_Hedged_ToDate_Volumes] TO [CBMSApplication]
GO
