CREATE TYPE [dbo].[tvp_Invoice_Collection_Frequency_And_Expected_Dtl] AS TABLE
(
[Seq_No] [smallint] NOT NULL,
[Is_Primary] [bit] NOT NULL,
[Invoice_Frequency_Cd] [int] NULL,
[Invoice_Frequency_Pattern_Cd] [int] NULL,
[Expected_Invoice_Raised_Day] [tinyint] NULL,
[Expected_Invoice_Received_Day] [tinyint] NULL,
[Expected_Invoice_Raised_Month] [tinyint] NULL,
[Expected_Invoice_Received_Month] [tinyint] NULL,
[Expected_Invoice_Raised_Day_Lag] [tinyint] NULL,
[Expected_Invoice_Received_Day_Lag] [tinyint] NULL,
[Custom_Days] [tinyint] NULL,
[Custom_Invoice_Received_Day] [tinyint] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Invoice_Collection_Frequency_And_Expected_Dtl] TO [CBMSApplication]
GO
