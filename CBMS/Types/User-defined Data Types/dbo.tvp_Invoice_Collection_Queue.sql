CREATE TYPE [dbo].[tvp_Invoice_Collection_Queue] AS TABLE
(
[Invoice_Collection_Queue_Id] [int] NOT NULL,
[Invoice_File_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Invoice_Date_Received] [date] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Invoice_Collection_Queue] TO [CBMSApplication]
GO
