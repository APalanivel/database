CREATE TYPE [Trade].[tvp_Deal_Ticket_Filter_Participant] AS TABLE
(
[Participant_Id] [int] NOT NULL,
[Deal_Participant_Type_Cd] [int] NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [Trade].[tvp_Deal_Ticket_Filter_Participant] TO [CBMSApplication]
GO
