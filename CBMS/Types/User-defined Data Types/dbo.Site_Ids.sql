CREATE TYPE [dbo].[Site_Ids] AS TABLE
(
[Site_Id] [int] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[Site_Ids] TO [CBMSApplication]
GO
