CREATE TYPE [Trade].[tvp_Rm_Budget_Price_Dtl] AS TABLE
(
[Service_Month] [date] NOT NULL,
[Rm_Budget_Price] [decimal] (28, 3) NULL
)
GO
GRANT EXECUTE ON TYPE:: [Trade].[tvp_Rm_Budget_Price_Dtl] TO [CBMSApplication]
GO
