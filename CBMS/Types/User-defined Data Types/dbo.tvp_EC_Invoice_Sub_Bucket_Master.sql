CREATE TYPE [dbo].[tvp_EC_Invoice_Sub_Bucket_Master] AS TABLE
(
[EC_Invoice_Sub_Bucket_Master_Id] [int] NULL,
[Sub_Bucket_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_EC_Invoice_Sub_Bucket_Master] TO [CBMSApplication]
GO
