CREATE TYPE [dbo].[tvp_Account_Invoice_Collection_Source] AS TABLE
(
[Invoice_Collection_Source_Cd] [int] NULL,
[Invoice_Source_Type_Cd] [int] NULL,
[Is_Primary] [int] NULL,
[Collection_Instruction] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Invoice_Source_Method_of_Contact_Cd] [int] NULL,
[URL] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Login_Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Passcode] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Instruction] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Instruction_Document_Image_Id] [int] NULL,
[UBM_Id] [int] NULL,
[UBM_Type_Cd] [int] NULL,
[File_Source_Cd] [int] NULL,
[File_Vendor_Id] [int] NULL,
[Generic_Key] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Image_Source_Cd] [int] NULL,
[Image_Vendor_Id] [int] NULL,
[Image_URL] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Image_Login_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Image_Passcode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Image_Load_Location] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO

GRANT EXECUTE ON TYPE:: [dbo].[tvp_Account_Invoice_Collection_Source] TO [CBMSApplication]
GO
