CREATE TYPE [dbo].[Tvp_Budget_Nymex_Forecast] AS TABLE
(
[Account_Id] [int] NOT NULL,
[Month_Identifier] [smalldatetime] NOT NULL,
[Price] [decimal] (32, 16) NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[Tvp_Budget_Nymex_Forecast] TO [CBMSApplication]
GO
