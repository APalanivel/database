CREATE TYPE [dbo].[tvp_Bucket_Site_Interval_Dtl] AS TABLE
(
[Client_Hier_Id] [int] NOT NULL,
[Bucket_Master_Id] [int] NOT NULL,
[Service_Start_Dt] [date] NOT NULL,
[Service_End_Dt] [date] NOT NULL,
[Data_Source_Cd] [int] NOT NULL,
[Bucket_Daily_Avg_Value] [decimal] (28, 10) NULL,
[Uom_Type_Id] [int] NULL,
[Currency_Unit_Id] [int] NULL,
PRIMARY KEY CLUSTERED  ([Client_Hier_Id], [Bucket_Master_Id], [Service_Start_Dt], [Service_End_Dt], [Data_Source_Cd])
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Bucket_Site_Interval_Dtl] TO [CBMSApplication]
GO
