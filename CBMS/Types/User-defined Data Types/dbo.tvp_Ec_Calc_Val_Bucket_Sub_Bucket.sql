CREATE TYPE [dbo].[tvp_Ec_Calc_Val_Bucket_Sub_Bucket] AS TABLE
(
[Bucket_Master_Id] [int] NOT NULL,
[EC_Invoice_Sub_Bucket_Master_Id] [int] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Ec_Calc_Val_Bucket_Sub_Bucket] TO [CBMSApplication]
GO
