CREATE TYPE [dbo].[CU_Invoice_Charge_Account_Type] AS TABLE
(
[CU_INVOICE_CHARGE_ACCOUNT_ID] [int] NULL,
[CU_INVOICE_CHARGE_ID] [int] NULL,
[CU_INVOICE_ID] [int] NULL,
[CU_DETERMINANT_CODE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACCOUNT_ID] [int] NULL,
[Charge_Expression] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Charge_Value] [decimal] (16, 4) NULL,
[Is_Delete] [bit] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[CU_Invoice_Charge_Account_Type] TO [CBMSApplication]
GO
