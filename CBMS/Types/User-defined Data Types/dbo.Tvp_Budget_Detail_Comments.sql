CREATE TYPE [dbo].[Tvp_Budget_Detail_Comments] AS TABLE
(
[Budget_Account_ID] [int] NOT NULL,
[Variable_Comments] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Transportation_Comments] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Transmission_Comments] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Distribution_Comments] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Other_Bundled_Comments] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Other_Fixed_Costs_Comments] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sourcing_Tax_Comments] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Rates_Tax_Comments] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsSourcingCompleted] [bit] NULL,
[IsRatesCompleted] [bit] NULL,
[IsRatesReviewCompleted] [bit] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[Tvp_Budget_Detail_Comments] TO [CBMSApplication]
GO
