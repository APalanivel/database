CREATE TYPE [dbo].[tvp_Bucket_Master_IDs] AS TABLE
(
[Bucket_Master_Id] [int] NOT NULL,
PRIMARY KEY CLUSTERED  ([Bucket_Master_Id])
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvp_Bucket_Master_IDs] TO [CBMSApplication]
GO
