CREATE TYPE [dbo].[TVP_Variance_History_Data_Point] AS TABLE
(
[Service_Month] [date] NULL,
[Account_id] [int] NULL,
[Commodity_Id] [int] NULL,
[category_Name] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Baseline_Value] [decimal] (28, 10) NULL,
[High_Tolerance] [decimal] (28, 10) NULL,
[Low_Tolerance] [decimal] (28, 10) NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[TVP_Variance_History_Data_Point] TO [CBMSApplication]
GO
