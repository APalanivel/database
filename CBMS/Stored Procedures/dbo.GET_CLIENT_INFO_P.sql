SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE dbo.GET_CLIENT_INFO_P

@userId varchar(10),
@sessionId varchar(20),
@clientId int


as
	set nocount on

select 	CLIENT_NAME , ent1.ENTITY_NAME FREQUENCY, 
	ent2.ENTITY_NAME CLIENT_TYPE,
	ent3.ENTITY_NAME START_MONTH,
--	ent4.entity_name service,
	UBM_START_DATE

from 	CLIENT, 
	ENTITY ent1,
	ENTITY ent2,
	ENTITY ent3
--	ENTITY ent4
where 
	client_id=@clientId and 
	report_frequency_type_id=ent1.entity_id and
	ent2.entity_id =client_type_id and
	ent3.entity_id =fiscalyear_startmonth_type_id 
--	and ent4.entity_id =+ ubm_service_id
GO
GRANT EXECUTE ON  [dbo].[GET_CLIENT_INFO_P] TO [CBMSApplication]
GO
