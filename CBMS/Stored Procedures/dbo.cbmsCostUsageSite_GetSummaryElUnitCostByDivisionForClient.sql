SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE       PROCEDURE [dbo].[cbmsCostUsageSite_GetSummaryElUnitCostByDivisionForClient]
	( @MyAccountId int
	, @currency_unit_id int
	, @el_unit_of_measure_type_id int
	, @ng_unit_of_measure_type_id int
	, @begin_date datetime
	, @end_date datetime
	, @client_id int
	)
AS
BEGIN

	   select d.division_id
		, d.division_name

		, sum(isNull(cu.el_cost, 0) * isNull(cuc.conversion_factor, 0)) working_cost
		, sum(isNull(cu.el_usage, 0) * isNull(cuc.conversion_factor, 0)) working_usage
		, convert(bit, min(convert(int, isNull(ip.el_is_complete, 0)))) is_complete
		, convert(bit, max(convert(int, isNull(ip.el_is_published, 0)))) is_published
		, convert(bit, max(convert(int, isNull(ip.el_under_review, 0)))) under_review
	     from client c WITH (NOLOCK) 
	     join division d  WITH (NOLOCK) on d.client_id = c.client_id
	     join site s  WITH (NOLOCK) on s.division_id = d.division_id
  left outer join cost_usage_site cu  WITH (NOLOCK) on (cu.site_id = s.site_id and cu.is_default = 1 and cu.service_month between @begin_date and @end_date)
  left outer join currency_unit_conversion cuc on cuc.base_unit_id = cu.currency_unit_id and cuc.converted_unit_id = @currency_unit_id and cuc.conversion_date = cu.service_month and cuc.currency_group_id = c.currency_group_id
  left outer join consumption_unit_conversion con on con.base_unit_id = cu.el_unit_of_measure_type_id and con.converted_unit_id = @el_unit_of_measure_type_id
  left outer join invoice_participation_site ip  WITH (NOLOCK) on ip.site_id = s.site_id and ip.service_month = cu.service_month
	    where c.client_id = @client_id
	      and (ip.is_published = 1 or ip.is_published is null)
	      and s.closed = 0
	      and s.not_managed = 0
	      
	 group by d.division_id
		, d.division_name

	 order by d.division_name

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCostUsageSite_GetSummaryElUnitCostByDivisionForClient] TO [CBMSApplication]
GO
