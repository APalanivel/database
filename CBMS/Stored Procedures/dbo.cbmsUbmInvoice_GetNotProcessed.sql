SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsUbmInvoice_GetNotProcessed

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@UbmBatchMasterLogId	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE     PROCEDURE [dbo].[cbmsUbmInvoice_GetNotProcessed]
(
	@MyAccountId int
	,@UbmBatchMasterLogId int
)
AS
select 	ui.ubm_invoice_id
	,ui.invoice_identifier
	,ui.cbms_image_id
	,ui.ubm_client_id
	,ui.ubm_client_name
	,ui.city
	,ui.state
	,ui.ubm_account_id
	,ui.ubm_account_number
	,u.ubm_name
from ubm_invoice ui with (nolock)
join ubm_batch_master_log ubml with (nolock) on ubml.ubm_batch_master_log_id = ui.ubm_batch_master_log_id
join ubm u with (nolock) on u.ubm_id = ubml.ubm_id 
where ui.ubm_batch_master_log_id = @UbmBatchMasterLogId
and ui.is_processed = 0
GO
GRANT EXECUTE ON  [dbo].[cbmsUbmInvoice_GetNotProcessed] TO [CBMSApplication]
GO
