SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsRMInsight_GetAuthors

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE     procedure [dbo].[cbmsRMInsight_GetAuthors]
( @MyAccountId int
)
as
begin

SELECT distinct author_id
	,ui.username
	,ui.first_name + ' ' + ui.last_name [author]

FROM RM_RISK_INSIGHT rri
	join user_info ui on ui.user_info_id = rri.author_id

end
GO
GRANT EXECUTE ON  [dbo].[cbmsRMInsight_GetAuthors] TO [CBMSApplication]
GO
