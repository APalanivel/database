SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
                           
 NAME: dbo.Supplier_Account_Dtls_Sel_By_Supplier_Account_Id                       
                            
 DESCRIPTION:        
  Get the Suppler account associated invoices count based on Supplier Account.                     
                            
 INPUT PARAMETERS:        
                           
 Name                               DataType          Default       Description        
-------------------------------------------------------------------------------------    
 @Sup_Account_Id   INT          
                            
 OUTPUT PARAMETERS:        
                                 
 Name                               DataType          Default       Description        
-------------------------------------------------------------------------------------                              
 USAGE EXAMPLES:                                
-------------------------------------------------------------------------------------                   
  
 
 EXEC dbo.Account_Dtls_Sel_By_Supplier_Account_Id  1740998    
  
 EXEC dbo.Account_Dtls_Sel_By_Supplier_Account_Id 1728901    
                     
                           
 AUTHOR INITIALS:      
         
 Initials                   Name        
-------------------------------------------------------------------------------------    
 NR                     Narayana Reddy                              
                             
 MODIFICATIONS:      
                             
 Initials               Date            Modification      
-------------------------------------------------------------------------------------    
 NR                     2019-06-13      Created for ADD Contract.                          
                           
******/

CREATE PROCEDURE [dbo].[Account_Dtls_Sel_By_Supplier_Account_Id]
    (
        @Account_Id INT = NULL
        , @Contract_Id INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;


        SELECT
            ch.Client_Hier_Id
            , ch.Client_Id
            , ch.Site_Id
            , ch.Sitegroup_Id
            , Uti_cha.Account_Id AS Utility_Account_Id
            , Uti_cha.Account_Number AS Utility_Account_Number
            , Sup_Cha.Account_Id AS Supplier_Account_Id
            , Sup_Cha.Account_Number AS Supplier_Account_Number
            , Sup_Cha.Supplier_Account_begin_Dt
            , NULLIF(Sup_Cha.Supplier_Account_End_Dt, '9999-12-31') AS Supplier_Account_End_Dt
            , Sup_Cha.Meter_Number
            , COUNT(DISTINCT cism.CU_INVOICE_ID) AS Invoices
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account Uti_cha
                ON Uti_cha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN Core.Client_Hier_Account Sup_Cha
                ON Uti_cha.Meter_Id = Sup_Cha.Meter_Id
                   AND  Uti_cha.Client_Hier_Id = Sup_Cha.Client_Hier_Id
            LEFT JOIN dbo.CONTRACT c
                ON c.CONTRACT_ID = Sup_Cha.Supplier_Contract_ID
            LEFT JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.Account_ID = Sup_Cha.Account_Id
                   AND  (   cism.Begin_Dt BETWEEN Sup_Cha.Supplier_Account_begin_Dt
                                          AND     Sup_Cha.Supplier_Account_End_Dt
                            OR  cism.End_Dt BETWEEN Sup_Cha.Supplier_Account_begin_Dt
                                            AND     Sup_Cha.Supplier_Account_End_Dt
                            OR  Sup_Cha.Supplier_Account_begin_Dt BETWEEN cism.Begin_Dt
                                                                  AND     cism.End_Dt
                            OR  Sup_Cha.Supplier_Account_End_Dt BETWEEN cism.Begin_Dt
                                                                AND     cism.End_Dt)
        WHERE
            Uti_cha.Account_Type = 'Utility'
            AND Sup_Cha.Account_Type = 'Supplier'
            AND (   @Account_Id IS NULL
                    OR  Sup_Cha.Account_Id = @Account_Id)
            AND (   @Contract_Id IS NULL
                    OR  Sup_Cha.Supplier_Contract_ID = @Contract_Id)
        GROUP BY
            ch.Client_Hier_Id
            , ch.Client_Id
            , ch.Site_Id
            , ch.Sitegroup_Id
            , Uti_cha.Account_Id
            , Uti_cha.Account_Number
            , Sup_Cha.Account_Id
            , Sup_Cha.Account_Number
            , Sup_Cha.Supplier_Account_begin_Dt
            , NULLIF(Sup_Cha.Supplier_Account_End_Dt, '9999-12-31')
            , Sup_Cha.Meter_Number
            , Sup_Cha.Supplier_Contract_ID;

    END;



GO
GRANT EXECUTE ON  [dbo].[Account_Dtls_Sel_By_Supplier_Account_Id] TO [CBMSApplication]
GO
