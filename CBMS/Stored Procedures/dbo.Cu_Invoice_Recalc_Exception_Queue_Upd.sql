SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
  dbo.Cu_Invoice_Recalc_Exception_Queue_Upd  
  
DESCRIPTION:  
  
  
INPUT PARAMETERS:  
 Name								DataType				Default		Description  
-------------------------------------------------------------------------------------  
 @Cu_Invoice_Id						INT
 @Account_Id						INT
 @Commodity_Id						INT
 @Status_Cd							INT
 @Date_In_Queue						DATETIME
 @User_Info_Id						INT
  

OUTPUT PARAMETERS:  
 Name								DataType				Default		Description  
-------------------------------------------------------------------------------------  
  
USAGE EXAMPLES:  
-------------------------------------------------------------------------------------  
 BEGIN Transaction
 
 select * from Cu_Invoice_Recalc_Exception_Queue
 EXEC Cu_Invoice_Recalc_Exception_Queue_Upd 8,'2015-11-06',49
 select * from Cu_Invoice_Recalc_Exception_Queue
 
 
 
 
 ROLLBACK Transaction 
  
  
AUTHOR INITIALS:  
 Initials	Name  
-------------------------------------------------------------------------------------  
 RKV		Ravi Kumar Vegesna  
   
MODIFICATIONS  
  
 Initials		Date			Modification  
-------------------------------------------------------------------------------------  
 RKV			2015-11-06		Created for AS400-PII 
            
              
******/  
CREATE PROCEDURE [dbo].[Cu_Invoice_Recalc_Exception_Queue_Upd]
      ( 
       @Cu_Invoice_Recalc_Exception_Queue_Id INT
      ,@Status_Cd INT
      ,@User_Info_Id INT = NULL )
AS 
BEGIN  
      
         
      
      UPDATE
            Cu_Invoice_Recalc_Exception_Queue
      SET   
            Status_Cd = @Status_Cd
           ,User_Info_Id = @User_Info_Id
           ,Last_Change_Ts = GETDATE()
      WHERE
            Cu_Invoice_Recalc_Exception_Queue_Id = @Cu_Invoice_Recalc_Exception_Queue_Id
                  
     
      
       
END     
   

;
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Recalc_Exception_Queue_Upd] TO [CBMSApplication]
GO
