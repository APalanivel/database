SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME: SSO_Savings_Merge_For_Data_Transfer
  
DESCRIPTION:  
 This procedure is for savings full data transfer
        
INPUT PARAMETERS:  
 Name      DataType          Default     Description  
---------------------------------------------------------------------------  
 
OUTPUT PARAMETERS:  
 Name              DataType          Default     Description  
-----------------------------------------------------------------  
  
USAGE EXAMPLES:  
---------------------------------------------------------------------------------- 
BEGIN TRAN       
	DECLARE @tvp_CH_Map [tvp_Client_Data_Transfer_CH_Map]
	DECLARE @tvp_Data_Map [tvp_Client_Data_Transfer_Data_Map]
	EXEC [SSO_Savings_Merge_For_Data_Transfer] @tvp_CH_Map,@tvp_Data_Map
ROLLBACK TRAN

AUTHOR INITIALS:  
 Initials   Name				Date  
------------------------------------------------------------  
 MSV		Muhamed Shahid V

MODIFICATIONS  
 Initials    Date			Modification  
------------------------------------------------------------  
 MSV         13-SEP-2019   created
******/
CREATE PROCEDURE [dbo].[SSO_Savings_Merge_For_Data_Transfer]
(
	@tvp_CH_Map [tvp_Client_Data_Transfer_CH_Map] READONLY,
	@tvp_Data_Map [tvp_Client_Data_Transfer_Data_Map] READONLY
)
AS
BEGIN
    SET NOCOUNT ON;
    BEGIN TRY
        BEGIN TRANSACTION;

        CREATE TABLE #CH_Map
        (
            From_Client_Hier_Id INT,
            To_Client_Hier_Id INT
        );

        CREATE TABLE #Data_Map
        (
            From_Key_Id INT,
            To_Key_Id INT
        );

		CREATE TABLE #From_Savings_Ids
        (
            SSO_SAVINGS_ID INT
        );

        CREATE TABLE #Savings_Out
        (
            From_Savings_Id INT,
            To_Savings_Id INT,
			Action_Type  VARCHAR(10)
        );

		INSERT INTO #CH_Map
		(
			From_Client_Hier_Id,
			To_Client_Hier_Id
		)
		SELECT From_Client_Hier_Id,
				To_Client_Hier_Id
		FROM @tvp_CH_Map

		INSERT INTO #Data_Map
		(
			From_Key_Id,
			To_Key_Id
		)
		SELECT From_Key_Id,
				To_Key_Id
		FROM @tvp_Data_Map

        /* Savings Merge */

		INSERT INTO #From_Savings_Ids
		(
			SSO_SAVINGS_ID
		)
		SELECT svg.SSO_SAVINGS_ID
		FROM dbo.SSO_SAVINGS svg
			INNER JOIN dbo.SSO_SAVINGS_OWNER_MAP som
				ON som.SSO_SAVINGS_ID = svg.SSO_SAVINGS_ID
			INNER JOIN #CH_Map chm
				ON chm.From_Client_Hier_Id = som.Client_Hier_Id
		GROUP BY svg.SSO_SAVINGS_ID

        MERGE INTO dbo.SSO_SAVINGS tgt
        USING
        (
            SELECT svg.SSO_SAVINGS_ID,
                   svg.SAVINGS_TITLE,
                   svg.SAVINGS_DESCRIPTION,
                   svg.CURRENCY_UNIT_ID,
                   svg.COMMODITY_TYPE_ID,
                   svg.[START_DATE],
                   svg.END_DATE,
                   svg.SAVINGS_CATEGORY_TYPE_ID,
                   svg.VENDOR_ID,
                   svg.IS_ACTUAL_VISIBLE,
                   svg.TOTAL_ESTIMATED_SAVINGS,
                   ISNULL(sdk.To_Key_Id, 0) AS To_Key_Id
            FROM #From_Savings_Ids fs
				INNER JOIN dbo.SSO_SAVINGS svg
					ON fs.SSO_SAVINGS_ID = svg.SSO_SAVINGS_ID
                LEFT JOIN #Data_Map sdk
                    ON sdk.From_Key_Id = svg.SSO_SAVINGS_ID
        ) src
        ON tgt.SSO_SAVINGS_ID = src.To_Key_Id
        WHEN MATCHED THEN
            UPDATE SET SAVINGS_TITLE = src.SAVINGS_TITLE,
                       SAVINGS_DESCRIPTION = src.SAVINGS_DESCRIPTION,
                       COMMODITY_TYPE_ID = src.COMMODITY_TYPE_ID,
                       CURRENCY_UNIT_ID = src.CURRENCY_UNIT_ID,
                       SAVINGS_CATEGORY_TYPE_ID = src.SAVINGS_CATEGORY_TYPE_ID,
                       [START_DATE] = src.[START_DATE],
                       END_DATE = src.END_DATE,
                       IS_ACTUAL_VISIBLE = src.IS_ACTUAL_VISIBLE,
                       VENDOR_ID = src.VENDOR_ID,
                       TOTAL_ESTIMATED_SAVINGS = src.TOTAL_ESTIMATED_SAVINGS,
                       updated_date = GETDATE()
        WHEN NOT MATCHED THEN
            INSERT
            (
                SAVINGS_TITLE,
                SAVINGS_DESCRIPTION,
                CURRENCY_UNIT_ID,
                COMMODITY_TYPE_ID,
                [START_DATE],
                END_DATE,
                SAVINGS_CATEGORY_TYPE_ID,
                VENDOR_ID,
                IS_ACTUAL_VISIBLE,
                TOTAL_ESTIMATED_SAVINGS,
                updated_date
            )
            VALUES
            (
				src.SAVINGS_TITLE, 
				src.SAVINGS_DESCRIPTION, 
				src.CURRENCY_UNIT_ID, 
				src.COMMODITY_TYPE_ID, 
				src.[START_DATE],
				src.END_DATE, 
				src.SAVINGS_CATEGORY_TYPE_ID, 
				src.VENDOR_ID, 
				src.IS_ACTUAL_VISIBLE,
				src.TOTAL_ESTIMATED_SAVINGS, 
				GETDATE()
			)
			OUTPUT src.SSO_SAVINGS_ID,
				   INSERTED.SSO_SAVINGS_ID,
				   $action
			INTO #Savings_Out
			(
				From_Savings_Id,
				To_Savings_Id,
				Action_Type
			);

		/* Data map insert */
		INSERT INTO #Data_Map
		(
		    From_Key_Id,
		    To_Key_Id
		)
		SELECT	From_Savings_Id,
				To_Savings_Id
		FROM #Savings_Out
		WHERE Action_Type = 'INSERT'
		
		/* Owner Map Insert */
		INSERT INTO dbo.SSO_SAVINGS_OWNER_MAP
		(
		    SSO_SAVINGS_ID,
		    Client_Hier_Id
		)
		SELECT	isv.To_Savings_Id,
				cm.To_Client_Hier_Id
		FROM #Savings_Out isv
		INNER JOIN dbo.SSO_SAVINGS_OWNER_MAP swm 
			ON swm.SSO_SAVINGS_ID = isv.From_Savings_Id
		INNER JOIN #CH_Map cm
			ON cm.From_Client_Hier_Id = swm.Client_Hier_Id	
		WHERE Action_Type = 'INSERT'
		
		/* Savings Detail Merge */
		MERGE INTO dbo.SSO_SAVINGS_DETAIL tgt
        USING
        (
            SELECT svd.SERVICE_MONTH,
                   svd.ESTIMATED_SAVINGS_VALUE,
                   svd.ACTUAL_SAVINGS_VALUE,
                   dm.To_Key_Id
            FROM #From_Savings_Ids fs
				INNER JOIN dbo.SSO_SAVINGS_DETAIL svd
					ON fs.SSO_SAVINGS_ID = svd.SSO_SAVINGS_ID
                INNER JOIN #Data_Map dm
                    ON dm.From_Key_Id = svd.SSO_SAVINGS_ID
        ) src
        ON tgt.SSO_SAVINGS_ID = src.To_Key_Id
			AND tgt.SERVICE_MONTH = src.SERVICE_MONTH
        WHEN MATCHED THEN
            UPDATE SET ESTIMATED_SAVINGS_VALUE = src.ESTIMATED_SAVINGS_VALUE,
                       ACTUAL_SAVINGS_VALUE = src.ACTUAL_SAVINGS_VALUE
        WHEN NOT MATCHED THEN
            INSERT
            (
                SSO_SAVINGS_ID,
				SERVICE_MONTH,
				ESTIMATED_SAVINGS_VALUE,
				ACTUAL_SAVINGS_VALUE
            )
            VALUES
            (
				src.To_Key_Id, 
				src.SERVICE_MONTH, 
				src.ESTIMATED_SAVINGS_VALUE, 
				src.ACTUAL_SAVINGS_VALUE
			);
		
		/* Owner Map Delete */
		DELETE tswm
		FROM dbo.SSO_SAVINGS_OWNER_MAP tswm
		INNER JOIN #Data_Map dm 
			ON dm.To_Key_Id = tswm.SSO_SAVINGS_ID
		LEFT JOIN #From_Savings_Ids fsid 
			ON fsid.SSO_SAVINGS_ID = dm.From_Key_Id
		LEFT JOIN #CH_Map chm
			ON chm.To_Client_Hier_Id = tswm.Client_Hier_Id
		LEFT JOIN dbo.SSO_SAVINGS_OWNER_MAP fswm 
			ON fswm.SSO_SAVINGS_ID = fsid.SSO_SAVINGS_ID
				AND fswm.Client_Hier_Id = chm.From_Client_Hier_Id
		WHERE fswm.SSO_SAVINGS_ID IS NULL

		/* Savings Detail Delete */
		DELETE tsd
		FROM dbo.SSO_SAVINGS_DETAIL tsd
		INNER JOIN #Data_Map dm 
			ON dm.To_Key_Id = tsd.SSO_SAVINGS_ID
		LEFT JOIN #From_Savings_Ids fsid 
			ON fsid.SSO_SAVINGS_ID = dm.From_Key_Id
		LEFT JOIN dbo.SSO_SAVINGS_DETAIL fsd 
			ON fsd.SSO_SAVINGS_ID = fsid.SSO_SAVINGS_ID
				AND fsd.SERVICE_MONTH = tsd.SERVICE_MONTH
		WHERE fsd.SSO_SAVINGS_ID IS NULL
		
		/* Savings Delete */
		DELETE tsv
		FROM dbo.SSO_SAVINGS tsv
		INNER JOIN #Data_Map dm ON dm.To_Key_Id = tsv.SSO_SAVINGS_ID
		LEFT JOIN #From_Savings_Ids fsid 
			ON fsid.SSO_SAVINGS_ID = dm.From_Key_Id
		WHERE fsid.SSO_SAVINGS_ID IS NULL

		/* Data map delete*/
		DELETE dm
		FROM #Data_Map dm
		LEFT JOIN #From_Savings_Ids fsid 
			ON fsid.SSO_SAVINGS_ID = dm.From_Key_Id
		WHERE fsid.SSO_SAVINGS_ID IS NULL
		
		SELECT	From_Key_Id,
				To_Key_Id
		FROM #Data_Map

        COMMIT TRAN;
    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION;
        EXEC usp_RethrowError;
    END CATCH;

END;
GO
GRANT EXECUTE ON  [dbo].[SSO_Savings_Merge_For_Data_Transfer] TO [CBMSApplication]
GO
