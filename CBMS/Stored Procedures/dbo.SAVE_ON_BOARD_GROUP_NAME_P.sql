SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SAVE_ON_BOARD_GROUP_NAME_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@clientId      	int       	          	
	@name          	varchar(4000)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE dbo.SAVE_ON_BOARD_GROUP_NAME_P  

@userId varchar(10),
@sessionId varchar(20),
@clientId int, 
@name varchar(4000)

AS
set nocount on
	INSERT INTO RM_GROUP (GROUP_NAME,CLIENT_ID) VALUES( @name , @clientId )
GO
GRANT EXECUTE ON  [dbo].[SAVE_ON_BOARD_GROUP_NAME_P] TO [CBMSApplication]
GO
