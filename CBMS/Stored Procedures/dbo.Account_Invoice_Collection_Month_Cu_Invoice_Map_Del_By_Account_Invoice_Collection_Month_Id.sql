SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                
 NAME: [Account_Invoice_Collection_Month_Cu_Invoice_Map_Del_By_Account_Invoice_Collection_Month_Id]                  
                                
 DESCRIPTION:                                
   To delete the record from Account_Invoice_Collection_Month Invoice Map
                                
 INPUT PARAMETERS:                  
                             
 Name									DataType			Default       Description                
---------------------------------------------------------------------------------------------------------------              
 @Account_Invoice_Collection_Month_Id	INT					NULL         

 OUTPUT PARAMETERS:                  
                                   
 Name									DataType			Default       Description                
---------------------------------------------------------------------------------------------------------------              
                                
 USAGE EXAMPLES:                                    
---------------------------------------------------------------------------------------------------------------                                    
            
        
  EXEC dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map_Del_By_Account_Invoice_Collection_Month_Id 563
        
                               
 AUTHOR INITIALS:                
               
 Initials				Name                
---------------------------------------------------------------------------------------------------------------                              
 PR						Pradip Rajput  
                                 
 MODIFICATIONS:              
                  
 Initials				Date             Modification              
---------------------------------------------------------------------------------------------------------------              
 PR						2020-04-11		Created for IC Revamp
                               
******/    
CREATE PROCEDURE [dbo].[Account_Invoice_Collection_Month_Cu_Invoice_Map_Del_By_Account_Invoice_Collection_Month_Id]  
      (   
       @Account_Invoice_Collection_Month_Id INT
	    )  
AS   
BEGIN                          
      DELETE  
            aicmcim  
      FROM  
            dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map aicmcim  
            where 
			aicmcim.Account_Invoice_Collection_Month_Id = @Account_Invoice_Collection_Month_Id
END;      
;  
GO
GRANT EXECUTE ON  [dbo].[Account_Invoice_Collection_Month_Cu_Invoice_Map_Del_By_Account_Invoice_Collection_Month_Id] TO [CBMSApplication]
GO
