SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.EC_Contract_Attribute_Sel       
              
Description:              
			This sproc to get the attribute details for a Given id's.      
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
	@Country_Id							INT					NULL
    @State_Id							INT					NULL
    @Commodity_Id						INT					NULL   
    @Start_Index						INT					1                        
    @End_Index							INT					2147483647  
    @Total_Count						INT					0                       
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

   Exec dbo.EC_Contract_Attribute_Sel 
   
   Exec dbo.EC_Contract_Attribute_Sel @Country_Id=98
   
   Exec dbo.EC_Contract_Attribute_Sel @Country_Id=98,@State_Id=236
   
   Exec dbo.EC_Contract_Attribute_Sel @Start_Index=1,@End_Index=5
   
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy               
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2015-04-22		Created For AS400.         
             
******/ 
CREATE PROCEDURE [dbo].[EC_Contract_Attribute_Sel]
      ( 
       @Country_Id INT = NULL
      ,@State_Id INT = NULL
      ,@Commodity_Id INT = NULL
      ,@Start_Index INT = 1
      ,@End_Index INT = 2147483647
      ,@Total_Count INT = 0 )
AS 
BEGIN
      SET NOCOUNT ON 
       
      IF @Total_Count = 0 
            BEGIN                     
                  SELECT
                        @Total_Count = COUNT(1)
                  FROM
                        dbo.EC_Contract_Attribute eca
                        INNER JOIN dbo.Commodity c
                              ON eca.Commodity_Id = c.Commodity_Id
                        INNER JOIN dbo.STATE s
                              ON s.STATE_ID = eca.State_Id
                        INNER JOIN dbo.COUNTRY cr
                              ON cr.COUNTRY_ID = s.COUNTRY_ID
                        INNER JOIN code cd
                              ON cd.Code_Id = eca.Attribute_Type_Cd
                  WHERE
                        ( @Country_Id IS NULL
                          OR cr.COUNTRY_ID = @Country_Id )
                        AND ( @State_Id IS NULL
                              OR s.STATE_ID = @State_Id )
                        AND ( @Commodity_Id IS NULL
                              OR c.Commodity_Id = @Commodity_Id )
                        
            END;
      WITH  Cte_Contract_Attribute
              AS ( SELECT TOP ( @End_Index )
                        eca.EC_Contract_Attribute_Id
                       ,cr.COUNTRY_NAME
                       ,s.STATE_NAME
                       ,eca.EC_Contract_Attribute_Name
                       ,cd.Code_Value AS Attribute_Type
                       ,c.Commodity_Name
                       ,ROW_NUMBER() OVER ( ORDER BY cr.COUNTRY_NAME, s.STATE_NAME, c.Commodity_Name, eca.EC_Contract_Attribute_Name ) AS Row_Num
                   FROM
                        dbo.EC_Contract_Attribute eca
                        INNER JOIN dbo.Commodity c
                              ON eca.Commodity_Id = c.Commodity_Id
                        INNER JOIN dbo.STATE s
                              ON s.STATE_ID = eca.State_Id
                        INNER JOIN dbo.COUNTRY cr
                              ON cr.COUNTRY_ID = s.COUNTRY_ID
                        INNER JOIN code cd
                              ON cd.Code_Id = eca.Attribute_Type_Cd
                   WHERE
                        ( @Country_Id IS NULL
                          OR cr.COUNTRY_ID = @Country_Id )
                        AND ( @State_Id IS NULL
                              OR s.STATE_ID = @State_Id )
                        AND ( @Commodity_Id IS NULL
                              OR c.Commodity_Id = @Commodity_Id ))
            SELECT
                  EC_Contract_Attribute_Id
                 ,cca.COUNTRY_NAME
                 ,cca.STATE_NAME
                 ,cca.Commodity_Name
                 ,cca.EC_Contract_Attribute_Name
                 ,cca.Attribute_Type
                 ,@Total_Count AS Total_Rows
                 ,cca.Row_Num
            FROM
                  Cte_Contract_Attribute cca
            WHERE
                  cca.Row_Num BETWEEN @Start_Index AND @End_Index
            ORDER BY
                  Row_Num       
           
END
            

;
GO
GRANT EXECUTE ON  [dbo].[EC_Contract_Attribute_Sel] TO [CBMSApplication]
GO
