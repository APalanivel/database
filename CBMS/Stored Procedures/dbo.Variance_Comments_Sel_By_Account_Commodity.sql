SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/******
NAME:
	CBMS.dbo.Variance_Comments_Sel_By_Account_Commodity

DESCRIPTION:

	Used to select the variance comments created for other services by account, commodity and service month.
	
INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Account_Id		INT
	@Commodity_Id	INT				NULL
	@Service_Month	Date

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Variance_Comments_Sel_By_Account_Commodity 302499,290,'2013-08-01'
	EXEC dbo.Variance_Comments_Sel_By_Account_Commodity 302539,290,'2010-06-01'
	EXEC dbo.Variance_Comments_Sel_By_Account_Commodity 412659,NULL,'2012-09-01'
	EXEC dbo.Variance_Comments_Sel_By_Account_Commodity 495119,NULL,'2012-02-01'
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hari
	RR			Raghu Reddy
	AP			Arunkumar Palanivel
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG        	03/24/2010	Created
	HG			04/16/2010	Default value (NULL) set for @Commodity_id parameter, so that this procedure can be used to get all the 
							variance entries logged for the given account and service month.
	RR			2014-07-07	Data Operations-3 Modified order by to get old to new in ascending order
	AP			Feb 21 2020	Modified procedure for AVT
******/

CREATE PROCEDURE [dbo].[Variance_Comments_Sel_By_Account_Commodity]
      @Account_id    INT
    , @Commodity_Id  INT = NULL
    , @Service_Month DATE
AS
      BEGIN

            SET NOCOUNT ON;

            SELECT
                        c.Comment_Text
                      , c.Comment_User_Info_Id
                      , ui.FIRST_NAME + space(1) + ui.LAST_NAME AS comment_by
                      , c.Comment_Dt
                      , cd.Code_Value AS Comment_Type
                      , stuff((     SELECT
                                          ',' + c.Code_Value
                                    FROM  dbo.Variance_Comment_Category_Map vccm
                                         LEFT JOIN
                                         dbo.Code c
                                                ON c.Code_Id = vccm.Variance_Category_Cd
                                    WHERE vccm.Cost_Usage_Account_Dtl_Comment_Map_Id = map.Cost_Usage_Account_Dtl_Comment_Map_Id
                                    FOR XML PATH(''))
                            , 1
                            , 1
                            , '') AS Category
            FROM        dbo.Cost_Usage_Account_Dtl_Comment_Map map
                        JOIN
                        dbo.Comment c
                              ON map.Comment_ID = c.Comment_ID
                        JOIN
                        dbo.USER_INFO ui
                              ON ui.USER_INFO_ID = c.Comment_User_Info_Id
                        JOIN
                        dbo.Code cd
                              ON cd.Code_Id = c.Comment_Type_CD
            WHERE       ( map.Account_Id = @Account_id )
                        --AND ( @Commodity_Id IS NULL
                        --      OR map.Commodity_Id = @Commodity_Id )
                        AND   ( map.Service_Month = @Service_Month )
            --GROUP BY    cd.Code_Id
            ORDER BY    c.Comment_Dt;

      END;
GO

GRANT EXECUTE ON  [dbo].[Variance_Comments_Sel_By_Account_Commodity] TO [CBMSApplication]
GO
