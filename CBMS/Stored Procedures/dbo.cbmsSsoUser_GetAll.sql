SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsSsoUser_GetAll

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE  Procedure [dbo].[cbmsSsoUser_GetAll]
	( @MyAccountId int
	)
As
BEGIN
	
	   select user_info_id
		, username
		, queue_id
		, null as passcode
		, first_name
		, middle_name
		, last_name
		, email_address
		, is_history
		, first_name + '  ' + last_name as full_name
	     from user_info ui
            where (is_history <> 1)
         order by first_name

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoUser_GetAll] TO [CBMSApplication]
GO
