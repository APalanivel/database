SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_UPLOADED_SMU_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	int       	          	
	@sessionId     	varchar(200)	          	
	@rfpId         	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

-- slect * fr

--select * from SR_RFP_SMU
 --select * from cbms_image where CBMS_IMAGE_ID=130587
-- exec DBO.SR_RFP_GET_UPLOADED_SMU_P 1,'1',147

CREATE         PROCEDURE DBO.SR_RFP_GET_UPLOADED_SMU_P

@userId int,
@sessionId varchar(200),
@rfpId int

AS
set nocount on
declare @entityId int
select @entityId = (SELECT ENTITY_ID FROM ENTITY where entity_name='SMU' and entity_type=100)

SELECT I.CBMS_DOC_ID, S.CBMS_IMAGE_ID, I.CONTENT_TYPE, U.USERNAME, S.UPLOADED_DATE, S.COMMENTS
FROM 
	SR_RFP_SMU S, CBMS_IMAGE I, USER_INFO U, sr_rfp_account rfp_account(nolock)
WHERE 
	S.SR_RFP_ID=@rfpId AND 
	s.sr_rfp_id = rfp_account.sr_rfp_id and
	rfp_account.is_deleted = 0 and
	S.CBMS_IMAGE_ID = I.CBMS_IMAGE_ID AND
        S.UPLOADED_BY = U.USER_INFO_ID 	AND
	I.CBMS_IMAGE_TYPE_ID = @entityId

group by I.CBMS_DOC_ID, S.CBMS_IMAGE_ID, I.CONTENT_TYPE, U.USERNAME, S.UPLOADED_DATE, S.COMMENTS
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_UPLOADED_SMU_P] TO [CBMSApplication]
GO
