SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
  
NAME:   
   [DBO].[Account_Information_Sel_By_Account_Id]    
       
DESCRIPTION:   
   To Get Account Information for selected Account Id.  
        
INPUT PARAMETERS:          
NAME   DATATYPE DEFAULT  DESCRIPTION  
------------------------------------------------------------------------------  
@Account_Id  INT      Utility Account  
  
OUTPUT PARAMETERS:  
NAME   DATATYPE DEFAULT  DESCRIPTION  
------------------------------------------------------------------------------  
USAGE EXAMPLES:  
------------------------------------------------------------------------------  
  
 EXEC dbo.Account_Information_Sel_By_Account_Id  101527  
  
 EXEC dbo.Account_Information_Sel_By_Account_Id  364  
  
AUTHOR INITIALS:            
INITIALS NAME            
------------------------------------------------------------------------------  
PNR		 PANDARINATH  
NR		 Narayana Reddy  
RKV		Ravi Kumar vegesna 
SP		Srinivas Patchava
SLP		Sri Lakshmi Pallikonda
 
            
MODIFICATIONS             
INITIALS		 DATE			MODIFICATION            
------------------------------------------------------------------------------  
PNR				25-MAY-10		CREATED    
BCH				2012-01-24		Added Site_Id column to select list    
NR				2015-05-28		For AS400 Added Client_Id,State_Id,Sitegroup_Id in select list.   
								Removed base tables and replaced with Client_hier and Client_Hier_Account tables.  
RKV				2015-10-01		Added Country_Name in the select list as Part of AS400-PII     
NR				2017-01-12		MAINT-4737 Added State_Name in Output List.   
RKV				2018-06-05		SE2017-566 added new Column original_Account_Number used to genrate the act xml 
SP				2019-09-03      MAINT-9168 Modified the script to refer the base tables for Utility account and site as the  data is not returning if no meter is associate to account     
								Added Group by to avoid returning the duplciate results if multi meters are added.
SLP				2019-11-01		Added as part of IC project to include the Acct type for URL Navigation
*/

CREATE PROCEDURE [dbo].[Account_Information_Sel_By_Account_Id]
(@Account_Id INT)
AS
BEGIN

    SET NOCOUNT ON;

    SELECT a.ACCOUNT_NUMBER AS Account_Number,
           v.VENDOR_NAME AS Vendor_Name,
           ch.Client_Name,
           ch.Site_Id,
           RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' Site_Name,
           a.NOT_MANAGED AS Not_Managed,
           ch.Client_Id,
           ch.State_Id,
           ch.Sitegroup_Id,
           ch.Country_Name,
           ch.State_Name,
           a.ACCOUNT_NUMBER AS Original_Account_Number,
          'Utility' AS Account_type
    FROM dbo.ACCOUNT a
        INNER JOIN dbo.ENTITY actyp
            ON actyp.ENTITY_ID = a.ACCOUNT_TYPE_ID
        INNER JOIN Core.Client_Hier ch
            ON ch.Site_Id = a.SITE_ID
        LEFT JOIN dbo.VENDOR v
            ON v.VENDOR_ID = a.VENDOR_ID
    WHERE a.ACCOUNT_ID = @Account_Id
          AND actyp.ENTITY_NAME = 'Utility'
    UNION
    SELECT cha.Display_Account_Number AS Account_Number,
           cha.Account_Vendor_Name AS Vendor_Name,
           ch.Client_Name,
           ch.Site_Id,
           RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' Site_Name,
           cha.Account_Not_Managed AS Not_Managed,
           ch.Client_Id,
           ch.State_Id,
           ch.Sitegroup_Id,
           ch.Country_Name,
           ch.State_Name,
           cha.Account_Number AS Original_Account_Number,
           'Supplier' AS Account_type
    FROM Core.Client_Hier ch
        INNER JOIN Core.Client_Hier_Account cha
            ON ch.Client_Hier_Id = cha.Client_Hier_Id
    WHERE cha.Account_Id = @Account_Id
          AND cha.Account_Type = 'Supplier'
    GROUP BY cha.Display_Account_Number,
             cha.Account_Vendor_Name,
             ch.Client_Name,
             ch.Site_Id,
             RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')',
             cha.Account_Not_Managed,
             ch.Client_Id,
             ch.State_Id,
             ch.Sitegroup_Id,
             ch.Country_Name,
             ch.State_Name,
             cha.Account_Number,
             cha.Account_Type;

END;


GO




GRANT EXECUTE ON  [dbo].[Account_Information_Sel_By_Account_Id] TO [CBMSApplication]
GO
