SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [DBO].[RM_Onboard_Hedge_Setup_Existence_Status_Sel_For_Site]  
     
DESCRIPTION: 
	To Get RM Onboard Hedge Setup Existence Status for Given Site Id.
      
INPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
	@Site_Id		INT			  		
               
OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        
    
	EXEC dbo.RM_Onboard_Hedge_Setup_Existence_Status_Sel_For_Site  258
	
	EXEC dbo.RM_Onboard_Hedge_Setup_Existence_Status_Sel_For_Site  280

AUTHOR INITIALS:          
	INITIALS	NAME          
------------------------------------------------------------          
	PNR			PANDARINATH
          
MODIFICATIONS:           
	INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
	PNR			25-JUN-10	CREATED 
	RR			27-05-2019	GRM - Modified script to refer new schema  
  
*/  

CREATE PROCEDURE [dbo].[RM_Onboard_Hedge_Setup_Existence_Status_Sel_For_Site] ( @Site_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;

      DECLARE @Status BIT = 0;

     
	
	
	------------------Own config
      SELECT
            @Status = 1
      FROM
            Core.Client_Hier ch
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                  ON ch.Client_Hier_Id = chob.Client_Hier_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                  ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY hdgtyp
                  ON chhc.Hedge_Type_Id = hdgtyp.ENTITY_ID
      WHERE
            ch.Site_Id > 0
            AND ch.Site_Id = @Site_Id
            AND hdgtyp.Entity_Name IN ( 'Physical', 'Financial', 'Physical & Financial' )
            AND CAST(DATEADD(dd, -DATEPART(dd, GETDATE()) + 1, GETDATE()) AS DATE) BETWEEN chhc.Config_Start_Dt
                                                                                   AND     chhc.Config_End_Dt
      
     ------------------Default config                   
      SELECT
            @Status = 1
      FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier chclient
                  ON ch.Client_Id = chclient.Client_Id
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                  ON chclient.Client_Hier_Id = chob.Client_Hier_Id
                     AND ch.Country_Id = chob.Country_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                  ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY hdgtyp
                  ON chhc.Hedge_Type_Id = hdgtyp.ENTITY_ID
      WHERE
            ch.Site_Id > 0
            AND chclient.Sitegroup_Id = 0
            AND ch.Site_Id = @Site_Id
            AND hdgtyp.Entity_Name IN ( 'Physical', 'Financial', 'Physical & Financial' )
            AND CAST(DATEADD(dd, -DATEPART(dd, GETDATE()) + 1, GETDATE()) AS DATE) BETWEEN chhc.Config_Start_Dt
                                                                                   AND     chhc.Config_End_Dt
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              Trade.RM_Client_Hier_Onboard siteob1
                             WHERE
                              siteob1.Client_Hier_Id = ch.Client_Hier_Id
                              AND siteob1.Commodity_Id = chob.Commodity_Id )
                              
      SELECT
            @Status AS RM_Onboard_Hedge_Setup_Existence_Status
      

END

GO
GRANT EXECUTE ON  [dbo].[RM_Onboard_Hedge_Setup_Existence_Status_Sel_For_Site] TO [CBMSApplication]
GO
