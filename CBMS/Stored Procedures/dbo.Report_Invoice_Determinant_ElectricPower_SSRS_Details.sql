SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
    
                                    
/******                                    
NAME: dbo.Report_Invoice_Determinant_ElectricPower_SSRS_Details                                    
                                     
                                    
DESCRIPTION:                                    
           To Get Pivoted data for determinants for Electric Power for selected Clients.                          
           This sp is called from SSRS Report.                          
           sp to Get Executed only if the difference between the input months is less than 12                          
                                    
INPUT PARAMETERS:                                    
 Name   DataType  Default Description                                    
------------------------------------------------------------                                    
@Client_IdO  VARCHAR(MAX)  
@Client_StatusO BIT  
@StartDateO  DATETIME  
@EndDateO  DATETIME                                                 
                                    
OUTPUT PARAMETERS:                                    
 Name   DataType  Default Description                                    
------------------------------------------------------------                          
                                  
                                    
USAGE EXAMPLES:                                    
------------------------------------------------------------                                    
EXEC dbo.Report_Invoice_Determinant_ElectricPower_SSRS_Details '235',0,'1/1/2012','12/1/2012'                  
EXEC dbo.Report_Invoice_Determinant_ElectricPower_SSRS '235',NULL,'1/1/2012','12/1/2012'                  
EXEC dbo.Report_Invoice_Determinant_ElectricPower_SSRS_Details '11395,11397,11401,11402,11404,11454,11506',0,'6/1/2010','5/1/2011'                                      
EXEC dbo.Report_Invoice_Determinant_ElectricPower_SSRS '11395,11397,11401,11402,11404,11454,11506',0,'6/1/2010','5/1/2011'                                      
                  
                                    
AUTHOR INITIALS:                                    
 Initials Name                                    
------------------------------------------------------------                                    
AKR             Ashok Kumar Raju                  
                                     
MODIFICATIONS                                    
                                    
 Initials Date   Modification                                    
------------------------------------------------------------                                    
AKR      2014-05-07     Cloned from Report_Invoice_Determinant_ElectricPower_SSRS, Added few addtitional columns  
******/                                    
                               
CREATE PROCEDURE dbo.Report_Invoice_Determinant_ElectricPower_SSRS_Details
      ( 
       @Client_IdO VARCHAR(MAX)
      ,@Client_StatusO BIT
      ,@StartDateO DATETIME
      ,@EndDateO DATETIME )
AS 
BEGIN                                    
                                   
      SET NOCOUNT ON                                    
                             
      IF DATEDIFF(m, @StartDateO, @EndDateO) < 12 
            BEGIN                          
                               
                                
                  DECLARE @Months VARCHAR(MAX) = ''                                    
                  DECLARE @Months_m VARCHAR(MAX) = ''                           
                  DECLARE @StartMonth DATE                           
                  DECLARE @loop INT = 1                               
                  DECLARE @SQLString NVARCHAR(MAX)                            
                                 
                                  
                  SELECT
                        @Months = @Months + '[' + CONVERT(VARCHAR(10), dd.date_d, 110) + '],'
                  FROM
                        meta.Date_Dim dd
                  WHERE
                        dd.date_d BETWEEN @StartDateO AND @EndDateO
                  ORDER BY
                        dd.Date_D                                
                                    
                                    
                                    
                  SELECT
                        @StartMonth = CASE WHEN DATEPART("D", @StartDateO) = 1 THEN CONVERT(VARCHAR(10), @StartDateO, 110)
                                           ELSE CONVERT(VARCHAR(10), DATEADD(dd, -( DAY(DATEADD(mm, 1, @StartDateO)) - 1 ), DATEADD(mm, 1, @StartDateO)), 110)
                                      END                               
                                    
                                              
                                
                  WHILE @loop <= 12 
                        BEGIN                        
                                
                                                        
                              IF @loop <= DATEDIFF("M", @StartMonth, @EndDateO) + 1 
                                    BEGIN              
                                          SET @Months_m = @Months_m + 'CASE WHEN [' + CONVERT(VARCHAR(10), DATEADD(mm, @loop - 1, @StartMonth), 110) + '] =-1000 THEN ''FALSE''                 
                                         WHEN [DeterminantUOM] = ''Months'' and [' + CONVERT(VARCHAR(10), DATEADD(mm, @loop - 1, @StartMonth), 110) + '] = 0 THEN ''Unable to Calculate''                   
                                          ELSE ' + 'CAST ([' + CONVERT(VARCHAR(10), DATEADD(mm, @loop - 1, @StartMonth), 110) + '] AS VARCHAR) END ' + '''' + CAST(@loop AS VARCHAR(10)) + ''','                         
                                                  
                                    END              
                              ELSE 
                                    BEGIN              
                                          SET @Months_m = @Months_m + 'NULL' + ' ''' + CAST(@loop AS VARCHAR(10)) + ''','                                            
                                    END              
                                
                                
                              SET @loop = @loop + 1                        
                        END                
                            
                          
                                              
                  SELECT
                        @Months = STUFF(@Months, LEN(@Months), 1, '')                                 
                  SELECT
                        @Months_m = STUFF(@Months_m, LEN(@Months_m), 1, '')                                 
                         
                              
                             
                  SET @SQLString = '                         
                            
                            
    DECLARE @Commodity_ID INT                   
    DECLARE @client_ID_List TABLE              
      (               
       Client_ID INT PRIMARY KEY )                         
                            
    SELECT              
      @Commodity_ID = com.Commodity_Id              
    FROM              
      dbo.Commodity com              
    WHERE              
      com.Commodity_Name = ''Electric Power''                             
                              
      INSERT   INTO @client_ID_List (Client_ID)                         
                  SELECT                          
                        fn.segments                          
                  FROM                          
                        dbo.ufn_split(@client_id, '','') fn;               
                                            
          SELECT           
                
        [AccountID]                          
                             ,[Meter Number]                          
                             ,[Rate Name]                          
                             ,[Region]                          
                             ,[State]                          
    ,[Client Name]                          
                             ,[Site Name]                          
                             ,[Utility]                          
                             ,[Utility Account Number]                          
                             ,[Site Type]            
       ,[Service Level]     
       , [Rate Id]      
       ,  [Utility Id]   
       ,  [Site Id]    
        into #CTE_Meter_Rate_Counts           
        from          
         (                                   
          SELECT                          
                              cha.ACCOUNT_ID [AccountID]                          
                             ,case WHEN count(meter_id) = 1 THEN max(cha.Meter_Number)                          
                                   ELSE ''Multiple''                          
                              END [Meter Number]                          
                             ,case WHEN count(DISTINCT Rate_id) = 1 THEN max(cha.Rate_Name)                          
                                   ELSE ''Multiple''                        
                              END [Rate Name]                          
                             ,case WHEN count(DISTINCT Rate_id) = 1 THEN cast(max(cha.Rate_id) as varchar)                         
                                   ELSE ''Multiple''                        
                              END [Rate Id]      
                             ,cha.Account_Vendor_Id [Utility Id]                                                
                             ,ch.Region_Name [Region]                          
                             ,ch.State_Name [State]                          
                             ,ch.Client_Name [Client Name]                          
                             ,ch.Site_name [Site Name]   
                             ,ch.Site_Id [Site Id]                         
          ,cha.Account_Vendor_Name [Utility]                          
                             ,cha.Account_Number [Utility Account Number]                          
                             ,stype.ENTITY_NAME [Site Type]            
        ,slcd.ENTITY_NAME [Service Level]            
                         FROM                          
                              core.Client_Hier_Account cha                          
                              INNER JOIN core.client_hier ch                          
                                    ON cha.Client_hier_Id = ch.Client_Hier_Id                          
         AND cha.Account_Type = ''Utility''                          
                              INNER JOIN dbo.SITE s             
         ON ch.site_Id = s.SITE_ID            
                              INNER JOIN dbo.ENTITY stype             
         ON s.SITE_TYPE_ID = stype.ENTITY_ID            
                              INNER JOIN dbo.ENTITY slcd             
         ON cha.Account_Service_level_Cd = slcd.ENTITY_ID            
                 INNER JOIN @client_ID_List cl                          
                                    ON cl.Client_ID = ch.Client_Id                          
                         WHERE                          
                              commodity_id = @Commodity_ID                          
                              AND ch.Country_Name IN ( ''usa'', ''canada'', ''mexico'', ''puerto rico'' )                    
                              and (@Client_Status IS NULL or ch.Client_Not_Managed = @Client_Status)          
                              and cha.Account_Not_Managed = 0          
                         GROUP BY                          
                              cha.ACCOUNT_ID                          
                             ,ch.Region_Name                          
                             ,ch.State_Name                          
                             ,ch.Client_Name                          
                             ,ch.Site_name                          
                       ,cha.Account_Vendor_Name                          
                             ,cha.Account_Number             
                             ,stype.ENTITY_NAME             
                             ,slcd.ENTITY_NAME     
                              ,cha.Account_Vendor_Id    
                              ,ch.Site_Id    
                             )   k          
            
  CREATE CLUSTERED INDEX ix_account_Id on #CTE_Meter_Rate_Counts (AccountID)                                           
                             
            SELECT                          
                  [Region]                          
                 ,[State]                          
                 ,[Client Name]                          
                 ,[Site Name]                          
                 ,[Utility]                          
                 ,[Utility Account Number]                          
                 ,[Rate Name]                          
                 ,[Meter Number]                          
                 ,[AccountID]     
                 , [Rate Id]      
       ,  [Utility Id]   
       ,  [Site Id]                         
                 ,[CU_INVOICE_ID]                        
                 ,[Commodity Name]                        
                 ,[Determinant Bucket]                          
                 ,[DeterminantUOM]                          
                 ,[Service Month]             
                 ,[Site Type]            
                 ,[Service Level]                                   
                 ,DeterminantValue = case WHEN DeterminantUOM IN ( ''%'', ''kw'', ''mw'', ''kvar'', ''kva'' ) THEN max(DeterminantValue)                          
                                          ELSE sum(DeterminantValue)                          
                                     END                          
            INTO                          
                  #Temp_For_Pivot                          
            FROM                          
                  ( SELECT                          
                        mc.[Region]                          
                       ,mc.[State]                          
                       ,mc.[Client Name]                          
                       ,mc.[Site Name]                          
                       ,mc.[Utility]                          
                       ,mc.[Utility Account Number]                          
                       ,mc.[Rate Name]                          
                       ,mc.[Meter Number]                          
                       ,mc.[AccountID]       
                       , mc.[Rate Id]      
       ,  mc.[Utility Id]   
       ,  mc.[Site Id]                     
                       ,cuid.[CU_INVOICE_ID]  [CU_INVOICE_ID]                        
                       ,com.commodity_Name [Commodity Name]                        
                       ,bm.Bucket_Name [Determinant Bucket]                          
                       ,e.ENTITY_NAME [DeterminantUOM]                          
                       ,cuism.SERVICE_MONTH [Service Month]                          
                       ,mc.[Site Type]            
                       ,mc.[Service Level]            
                       ,CASE WHEN cuida.DETERMINANT_VALUE IS NULL THEN             
                 CASE             
                       WHEN ( isnumeric(cuid.DETERMINANT_VALUE) = 1            
            OR cuid.DETERMINANT_VALUE IS NULL ) THEN cast(isnull(( replace(replace(cuid.determinant_value, '','', ''''), '' '', '''') ), 0) AS DECIMAL(32, 18))             
                       WHEN ( isnumeric(cuida.DETERMINANT_VALUE) = 0            
                              AND cuid.DETERMINANT_VALUE IS NOT NULL ) THEN -1000 END            
                       ELSE            
                        cuida.DETERMINANT_VALUE            
                  END [DeterminantValue]            
           FROM                          
                        #CTE_Meter_Rate_Counts mc          
                        JOIN dbo.CU_INVOICE_SERVICE_MONTH cuism                          
                              ON cuism.Account_ID = mc.AccountId                          
                       JOIN dbo.CU_INVOICE_DETERMINANT cuid            
                        ON cuid.CU_INVOICE_ID = cuism.CU_INVOICE_ID            
                           AND cuid.COMMODITY_TYPE_ID = @Commodity_ID     
                        INNER JOIN dbo.CU_Invoice cu     
                         ON cu.CU_INVOICE_ID =  cuid.CU_INVOICE_ID                
                JOIN dbo.CU_INVOICE_DETERMINANT_ACCOUNT cuida             
                     ON cuid.CU_INVOICE_DETERMINANT_ID = cuida.CU_INVOICE_DETERMINANT_ID            
                    AND cuism.Account_ID = cuida.ACCOUNT_ID                 
                        JOIN dbo.Commodity com                        
                              ON com.commodity_ID = @Commodity_ID                        
                        LEFT JOIN dbo.Bucket_Master bm                          
                     ON bm.Bucket_Master_Id = cuid.Bucket_Master_Id                          
                        LEFT JOIN dbo.ENTITY e                          
                              ON e.ENTITY_ID = cuid.UNIT_OF_MEASURE_TYPE_ID                          
                    WHERE                          
                        (SERVICE_MONTH BETWEEN @StartDate AND @EndDate)    
                        AND cu.Is_Reported = 1 ) x                          
            GROUP BY                          
                  [Region]                          
                 ,[State]                          
                 ,[Client Name]                          
                 ,[Site Name]                          
                 ,[Utility]                          
                 ,[Utility Account Number]                          
                 ,[Rate Name]                          
                 ,[Meter Number]                          
                 ,[AccountID]                          
                 ,[CU_INVOICE_ID]                          
                 ,[Commodity Name]                        
                 ,[Determinant Bucket]                          
                 ,[DeterminantUOM]                          
                 ,[Service Month]               
                 ,[Site Type]            
                 ,[Service Level]    
                 , [Rate Id]      
       ,  [Utility Id]   
       ,  [Site Id]              
          
CREATE CLUSTERED INDEX ix_Temp_For_Pivot_AIS ON #Temp_For_Pivot (CU_INVOICE_ID, AccountId, [Service Month])                 
                                   
    ; WITH                   
            CTE_Months_Invoice_Count                  
      AS                  
            ( SELECT                  
                  cuism.Account_ID                  
                 ,cuism.Service_Month                  
                 ,cuism.CU_INVOICE_ID                  
                 ,count(cuism.CU_INVOICE_ID) OVER ( PARTITION BY cuism.ACCOUNT_ID, cuism.SERVICE_MONTH ) AS Invoice_Count                  
                 ,count(cuism.SERVICE_MONTH) OVER ( PARTITION BY cuism.CU_INVOICE_ID, cuism.ACCOUNT_ID ) AS Month_Count                  
              FROM                  
                  dbo.CU_INVOICE_SERVICE_MONTH cuism                  
                  INNER JOIN core.Client_Hier_Account cha                  
                        ON cuism.Account_ID = cha.Account_Id                  
                  INNER JOIN core.Client_Hier ch                  
                        ON cha.Client_Hier_Id = ch.Client_Hier_Id                  
              WHERE                  
                  ch.Client_Id IN ( SELECT                  
                                          Client_ID                  
                                    FROM                  
                              @client_ID_List )                  
                  AND commodity_id = @Commodity_ID             
                  AND cha.Account_Not_Managed = 0                     
                  AND ch.Country_Name IN ( ''usa'', ''canada'', ''mexico'', ''puerto rico'' )               
                  GROUP BY               
                  cuism.Account_ID                    
                 ,cuism.Service_Month                    
                 ,cuism.CU_INVOICE_ID )                  
                                                    
                                  
                                         
            INSERT INTO #Temp_For_Pivot                   
                                        
              SELECT                    
                    DISTINCT [Region]                          
      ,[State]                          
                   ,[Client Name]                          
                   ,[Site Name]                          
                   ,[Utility]                          
                   ,[Utility Account Number]                          
                   ,[Rate Name]                          
                   ,[Meter Number]                          
                   ,[AccountID]        
                   , [Rate Id]      
       ,  [Utility Id]   
       ,  [Site Id]                      
                   ,0                     
                   ,[Commodity Name]                        
                   , [Determinant Bucket]                          
                   ,[DeterminantUOM]                          
     , [Service Month]                     
                   , [Site Type]            
                   ,[Service Level]            
                   ,CASE WHEN MAX(Invoice_Count) = 1 THEN MAX(Month_Count)                  
      WHEN MAX(Invoice_Count) > 1 AND MAX(Month_Count) = MIN(Month_Count) THEN MAX(Month_Count)                  
      ELSE 0  END   [Billing Days]                    
                  FROM                    
                    (  SELECT                    
                         DISTINCT temp.[Region]                          
                       ,temp.[State]                          
                       ,temp.[Client Name]                          
                       ,temp.[Site Name]                          
                       ,temp.[Utility]                          
                       ,temp.[Utility Account Number]                          
        ,temp.[Rate Name]                          
                       ,temp.[Meter Number]                          
                       ,temp.[AccountID]            
                       , temp.[Rate Id]      
       ,  temp.[Utility Id]   
       ,  temp.[Site Id]                  
                       ,temp.CU_INVOICE_ID                        
                       ,temp.[Commodity Name]                        
                       ,''Count of Months'' [Determinant Bucket]                          
                       ,''Months'' [DeterminantUOM]                          
                       ,temp.[SERVICE MONTH] [Service Month]                   
                       ,temp.[Site Type]            
                       ,temp.[Service Level]            
                       ,cuser.Invoice_Count                  
                       ,cuser.Month_Count                    
                       FROM                    
                       #Temp_For_Pivot  temp                      
                       INNER JOIN  CTE_Months_Invoice_Count cuser                    
                         ON temp.CU_INVOICE_ID = cuser.CU_INVOICE_ID AND cuser.Account_ID = temp.[AccountID]                    
                        AND temp.[SERVICE MONTH] = cuser.service_month                  
                       INNER JOIN dbo.CU_INVOICE cuin              
                        ON cuin.CU_INVOICE_ID = temp.CU_INVOICE_ID              
                        WHERE  cuin.Is_Reported = 1 and cuin.Is_DNT = 0 and cuin.Is_Duplicate = 0               
                       ) k                    
                      GROUP BY                    
                         [Region]                          
                       ,[State]                          
                       ,[Client Name]                
                       ,[Site Name]                          
                       ,[Utility]                          
                       ,[Utility Account Number]                          
                       ,[Rate Name]                          
                       ,[Meter Number]                          
                       ,[AccountID]     
                       , [Rate Id]      
       ,  [Utility Id]   
       ,  [Site Id]                         
                       ,[Commodity Name]                        
                       ,[Determinant Bucket]                          
                       ,[DeterminantUOM]                          
                       ,[Service Month]                   
                       ,[Site Type]            
                       ,[Service Level]            
                                         
                       
   SELECT                                
   [Region]                                    
     ,[State]                                    
     ,[Client Name]                                    
     ,[Site Name]                              
     ,[Utility]                                    
     ,[Utility Account Number]                                    
     ,[Rate Name]                                    
     ,[Meter Number]                                     
     ,[AccountID]        
       , [Rate Id]      
       ,  [Utility Id]   
       ,  [Site Id]                                      
     ,[Commodity Name]                                
     ,[Determinant Bucket]                  
     ,[DeterminantUOM]                       
     ,[Site Type]            
     ,[Service Level]     
     ,ch.Client_Id [Client Id]                      
     ,ch.Sitegroup_Id [Division Id]  
     ,ch.Sitegroup_Name [Division Name]  
     ,ch.Country_Name [Country]  
     ,ch.Site_Address_Line1 [Address 1]  
     ,ch.Site_Address_Line2 [Address 2]  
     ,ch.City   
     ,ch.ZipCode  
     ,' + @Months_m + '                                    
     FROM                                     
    (                                    
    SELECT                                     
   [Region]                                    
     ,[State]                                    
     ,[Client Name]                                    
     ,[Site Name]                                    
     ,[Utility]                      
     ,[Utility Account Number]                                    
     ,[Meter Number]                                     
     ,[AccountID]                                    
     ,[Commodity Name]                                 
     ,[Determinant Bucket]                  
     ,[DeterminantUOM]                                    
     ,[Service Month]                                    
     ,[Rate Name]                                    
     ,[Site Type]            
     ,[Service Level]            
     ,DeterminantValue   
       , [Rate Id]      
       ,  [Utility Id]   
       ,  [Site Id]                                           
    FROM #Temp_For_Pivot) tem                                    
    PIVOT (SUM([DeterminantValue])                                    
    FOR [Service Month] IN (' + @Months + ')) AS pvt  
    INNER JOIN core.client_hier ch   
    ON ch.site_id = pvt.[Site Id]   
       
    '                                    
                                      
                     
                  EXEC sp_executesql 
                        @SQLString
                       ,N'@Client_Id VARCHAR(MAX),@Client_Status BIT, @StartDate DATETIME, @EndDate DATETIME '
                       ,@Client_Id = @Client_IdO
                       ,@Client_Status = @Client_StatusO
                       ,@StartDate = @StartDateO
                       ,@EndDate = @EndDateO                             
                                
            END                           
                                    
END;
;
GO
GRANT EXECUTE ON  [dbo].[Report_Invoice_Determinant_ElectricPower_SSRS_Details] TO [CBMS_SSRS_Reports]
GO
