SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE dbo.SAVE_MARKET_PRICE_POINT_DETAIL_P
@pricePointId int,
@shortName varchar(50),
@showOnDV int
AS
begin
update	market_price_point  
set 	market_price_point.market_price_point_short_name = @shortName,
	market_price_point.is_show_on_dv = @showOnDV
where 	market_price_point.market_price_point_id = @pricePointId
end





GO
GRANT EXECUTE ON  [dbo].[SAVE_MARKET_PRICE_POINT_DETAIL_P] TO [CBMSApplication]
GO
