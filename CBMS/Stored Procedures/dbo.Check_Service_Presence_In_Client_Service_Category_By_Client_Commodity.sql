SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	
	dbo.Check_Service_Presence_In_Client_Service_Category_By_Client_Commodity

DESCRIPTION: 

	Used to check if the given commodity id is added to any of the level2 and level3 service categories for that client.

INPUT PARAMETERS:
      Name					DataType          Default     Description
------------------------------------------------------------------ 
	  @Commodity_Id   int

OUTPUT PARAMETERS:
      Name              DataType          Default     Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	DECLARE @oIs_Service_Added_To_Service_Category BIT

		EXEC dbo.Check_Service_Presence_In_Client_Service_Category_By_Client_Commodity 10069,290, @Is_Service_Added_To_Service_Category = @oIs_Service_Added_To_Service_Category OUTPUT

		SELECT @oIs_Service_Added_To_Service_Category
GO

	DECLARE @oIs_Service_Added_To_Service_Category BIT

		EXEC dbo.Check_Service_Presence_In_Client_Service_Category_By_Client_Commodity 10069,57, @Is_Service_Added_To_Service_Category = @oIs_Service_Added_To_Service_Category OUTPUT

		SELECT @oIs_Service_Added_To_Service_Category

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan G

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	HG			2011-11-14	Created for UDE requirement

******/
CREATE PROCEDURE dbo.Check_Service_Presence_In_Client_Service_Category_By_Client_Commodity
      ( 
	   @Client_Id INT
       ,@Commodity_Id INT
      ,@Is_Service_Added_To_Service_Category BIT OUT )
AS
BEGIN

      SET NOCOUNT ON
	  
      SET @Is_Service_Added_To_Service_Category = 0

      IF EXISTS ( SELECT
                        1
                  FROM
                        dbo.Client_Service_Category_Commodity cc
                        INNER JOIN dbo.Client_Service_Category sc
							ON sc.Client_Service_Category_Id = cc.Client_Service_Category_Id
                  WHERE
                        cc.Commodity_Id = @Commodity_Id 
                        AND sc.Client_Id = @Client_Id)
            BEGIN
                  SET @Is_Service_Added_To_Service_Category = 1
            END

END
GO
GRANT EXECUTE ON  [dbo].[Check_Service_Presence_In_Client_Service_Category_By_Client_Commodity] TO [CBMSApplication]
GO
