SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.UBM_IS_AVISTA_CLIENT_THERE_P
	@user_id varchar(10),
	@session_id varchar(20),
	@client_id int
	AS
	set nocount on
		select 
			count(ubm_feed_frequency_id) NO_OF_RECORDS
		from 
			ubm_feed_frequency 
		where 
			ubm_client_id = @client_id
			and ubm_id=(select ubm_id from ubm where ubm_name='Avista')

GO
GRANT EXECUTE ON  [dbo].[UBM_IS_AVISTA_CLIENT_THERE_P] TO [CBMSApplication]
GO
