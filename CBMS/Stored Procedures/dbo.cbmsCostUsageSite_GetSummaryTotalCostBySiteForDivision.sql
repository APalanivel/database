
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsCostUsageSite_GetSummaryTotalCostBySiteForDivision

DESCRIPTION:
		
INPUT PARAMETERS:
	Name				   DataType	Default	Description
------------------------------------------------------------
	@Currency_Unit_ID	   INT
	@Begin_Date		   DATETIME
	@End_Date			   DATETIME
	@Division_ID		   INT
	@Site_ID			   INT		NULL

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
EXECUTE dbo.cbmsCostUsageSite_GetSummaryTotalCostBySiteForDivision 3, '01/01/2011', '12/01/2011', 12241039, NULL


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
    AP		 Athmaram Pabbathi
    
MODIFICATIONS

	Initials	Date		   Modification
------------------------------------------------------------
	AP		08/12/2011   Removed unused parameters @MyAccountId, @el_unit_of_measure_type_id, @ng_unit_of_measure_type_id and Removed Cost_Usage_Site, Invoice_Participation_Site, vsSiteName tables and 
					   used Cost_Usage_Account_Dtl, Bucket_Master, Account, Invoice_Participation, commodity, Address, State tables 
    AP		08/22/2011   Added qualifiers to all objects with the owner name.
    AP		08/23/2011   Removed base tables from CTE_Client_Dtl and used Client_Hier table and removed NOLOCK statements
    AP		09/22/2011   Removed hardcoded bucket name and used dbo.Cost_usage_Bucket_Sel_By_Commodity instead and removed CTEs and used temp tables instead.
    AP		03/29/2012   Modified code to use Client_Hier_Id instead Site_Id for join on CUSD and in temp tables
    AP		04/02/2012   Added Client_Hier_Id in the result set
******/
CREATE       PROCEDURE [dbo].[cbmsCostUsageSite_GetSummaryTotalCostBySiteForDivision]
      ( 
       @Currency_Unit_ID INT
      ,@Begin_Date DATETIME
      ,@End_Date DATETIME
      ,@Division_ID INT
      ,@Site_ID INT = NULL )
AS 
BEGIN
      SET NOCOUNT ON ;
    
      DECLARE
            @EL_Commodity_ID INT
           ,@NG_Commodity_ID INT

      DECLARE @Cost_Usage_Bucket_Id TABLE
            ( 
             Bucket_Master_Id INT PRIMARY KEY CLUSTERED
            ,Bucket_Name VARCHAR(200)
            ,Bucket_Type VARCHAR(25) )

      CREATE TABLE #Client_Dtl
            ( 
             Client_Hier_Id INT PRIMARY KEY CLUSTERED
            ,Site_Id INT
            ,Site_Name VARCHAR(200)
            ,Currency_Group_Id INT )


      CREATE TABLE #Cost_Usage_Site_Dtl
            ( 
             Client_Hier_Id INT
            ,Service_Month DATE
            ,Working_Value DECIMAL(28, 10) )

      CREATE INDEX IDX_Cost_Usage_Site_Dtl ON #Cost_Usage_Site_Dtl(Client_Hier_Id, Service_Month)

      CREATE TABLE #Invoice_Participation_Site
            ( 
             Client_Hier_Id INT
            ,Service_Month DATE
            ,Is_Complete SMALLINT
            ,Is_Published SMALLINT
            ,NG_Is_Complete SMALLINT
            ,NG_Is_Published SMALLINT
            ,NG_Under_Review SMALLINT
            ,EL_Is_Complete SMALLINT
            ,EL_Is_Published SMALLINT
            ,EL_Under_Review SMALLINT )

      CREATE INDEX IDX_Invoice_Participation_Site ON #Invoice_Participation_Site(Client_Hier_Id, Service_Month)
            
      SELECT
            @EL_Commodity_ID = com.Commodity_ID
      FROM
            dbo.Commodity com
      WHERE
            com.Commodity_Name = 'Electric Power' 
            
                
      SELECT
            @NG_Commodity_ID = com.Commodity_ID
      FROM
            dbo.Commodity com
      WHERE
            com.Commodity_Name = 'Natural Gas' 

      INSERT      INTO @Cost_Usage_Bucket_Id
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Bucket_Type )
                  EXEC dbo.Cost_usage_Bucket_Sel_By_Commodity 
                        @Commodity_id = @EL_Commodity_ID 

      INSERT      INTO @Cost_Usage_Bucket_Id
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Bucket_Type )
                  EXEC dbo.Cost_usage_Bucket_Sel_By_Commodity 
                        @Commodity_id = @NG_Commodity_ID ; 

      INSERT      INTO #Client_Dtl
                  ( 
                   Client_Hier_Id
                  ,Site_Id
                  ,Site_Name
                  ,Currency_Group_Id )
                  SELECT
                        ch.Client_Hier_Id
                       ,CH.Site_ID
                       ,CH.Site_Name
                       ,CH.Client_Currency_Group_ID AS Currency_Group_ID
                  FROM
                        Core.Client_Hier CH
                  WHERE
                        CH.Sitegroup_Id = @Division_ID
                        AND ( @Site_ID IS NULL
                              OR CH.Site_ID = @Site_ID )
                        AND CH.Site_Closed = 0
                        AND CH.Site_Not_Managed = 0
                        AND CH.Site_ID > 0
                  GROUP BY
                        ch.Client_Hier_Id
                       ,CH.Site_ID
                       ,CH.Site_Name
                       ,CH.Client_Currency_Group_ID
                       
      INSERT      INTO #Cost_Usage_Site_Dtl
                  ( 
                   Client_Hier_Id
                  ,Service_Month
                  ,Working_Value )
                  SELECT
                        CUSD.Client_Hier_Id
                       ,CUSD.Service_Month
                       ,isnull(sum(CUSD.Bucket_Value * CurConv.Conversion_Factor), 0) AS Working_Value
                  FROM
                        dbo.Cost_Usage_Site_Dtl CUSD
                        INNER JOIN #Client_Dtl CD
                              ON CD.Client_Hier_Id = CUSD.Client_Hier_Id
                        INNER JOIN @Cost_Usage_Bucket_Id CUB
                              ON CUB.Bucket_Master_Id = CUSD.Bucket_Master_Id
                        INNER JOIN dbo.Currency_Unit_Conversion CurConv
                              ON CurConv.Base_Unit_ID = CUSD.Currency_Unit_ID
                                 AND CurConv.Conversion_Date = CUSD.Service_Month
                                 AND CurConv.Currency_Group_ID = CD.Currency_Group_ID
                  WHERE
                        CUB.Bucket_Type = 'Charge'
                        AND CurConv.Converted_Unit_ID = @Currency_Unit_ID
                        AND CUSD.Service_Month BETWEEN @Begin_Date
                                               AND     @End_Date
                  GROUP BY
                        CUSD.Client_Hier_Id
                       ,CUSD.Service_Month

      INSERT      INTO #Invoice_Participation_Site
                  ( 
                   Client_Hier_Id
                  ,Service_Month
                  ,Is_Complete
                  ,Is_Published
                  ,NG_Is_Complete
                  ,NG_Is_Published
                  ,NG_Under_Review
                  ,EL_Is_Complete
                  ,EL_Is_Published
                  ,EL_Under_Review )
                  SELECT
                        cd.Client_Hier_Id
                       ,IP.Service_Month
                       ,( case WHEN ( sum(cast(IP.Is_Expected AS INT)) = sum(cast(IP.Is_Received AS INT)) ) THEN 1
                               ELSE 0
                          END ) AS Is_Complete
                       ,( case WHEN sum(cast(IP.Is_Received AS INT)) > 0 THEN 1
                               ELSE 0
                          END ) AS Is_Published
                       ,( case WHEN ( sum(case WHEN CHA.Commodity_Id = @NG_Commodity_ID
                                                    AND IP.Is_Expected = 1 THEN 1
                                               ELSE 0
                                          END) = sum(case WHEN CHA.Commodity_Id = @NG_Commodity_ID
                                                               AND IP.Is_Received = 1 THEN 1
                                                          ELSE 0
                                                     END) ) THEN 1
                               ELSE 0
                          END ) AS NG_Is_Complete
                       ,( case WHEN sum(case WHEN CHA.Commodity_Id = @NG_Commodity_ID
                                                  AND IP.Is_Received = 1 THEN 1
                                             ELSE 0
                                        END) > 0 THEN 1
                               ELSE 0
                          END ) AS NG_Is_Published
                       ,isnull(max(case WHEN CHA.Commodity_Id = @NG_Commodity_ID
                                             AND IP.Recalc_Under_Review = 1 THEN 1
                                        WHEN CHA.Commodity_Id = @NG_Commodity_ID
                                             AND IP.Variance_Under_Review = 1 THEN 1
                                   END), 0) NG_Under_Review
                       ,( case WHEN ( sum(case WHEN CHA.Commodity_Id = @EL_Commodity_ID
                                                    AND IP.Is_Expected = 1 THEN 1
                                               ELSE 0
                                          END) = sum(case WHEN CHA.Commodity_Id = @EL_Commodity_ID
                                                               AND IP.Is_Received = 1 THEN 1
                                                          ELSE 0
                                                     END) ) THEN 1
                               ELSE 0
                          END ) AS EL_Is_Complete
                       ,( case WHEN sum(case WHEN CHA.Commodity_Id = @EL_Commodity_ID
                                                  AND IP.Is_Received = 1 THEN 1
                                             ELSE 0
                                        END) > 0 THEN 1
                               ELSE 0
                          END ) AS EL_Is_Published
                       ,isnull(max(case WHEN CHA.Commodity_Id = @EL_Commodity_ID
                                             AND IP.Recalc_Under_Review = 1 THEN 1
                                        WHEN CHA.Commodity_Id = @EL_Commodity_ID
                                             AND IP.Variance_Under_Review = 1 THEN 1
                                   END), 0) EL_Under_Review
                  FROM
                        dbo.Invoice_Participation IP
                        INNER JOIN #Client_Dtl CD
                              ON CD.Site_ID = IP.Site_ID
                        INNER JOIN ( SELECT
                                          cha.Account_Id
                                         ,cha.Commodity_Id
                                     FROM
                                          Core.Client_Hier_Account cha
                                     GROUP BY
                                          cha.Account_Id
                                         ,cha.Commodity_Id ) CHA
                              ON CHA.Account_ID = IP.Account_ID
                  WHERE
                        IP.Service_Month BETWEEN @Begin_Date
                                         AND     @End_Date
                        AND CHA.Commodity_Id IN ( @EL_Commodity_ID, @NG_Commodity_ID )
                  GROUP BY
                        cd.Client_Hier_Id
                       ,IP.Service_Month


      SELECT
            CD.Site_ID
           ,cd.Client_Hier_Id
           ,CD.Site_Name
           ,sum(CUSD.Working_Value) Working_Value
           ,convert(BIT, min(convert(INT, isnull(IPS.Is_Complete, 0)))) Is_Complete
           ,convert(BIT, max(convert(INT, isnull(IPS.Is_Published, 0)))) Is_Published
           ,convert(BIT, min(convert(INT, isnull(IPS.NG_Is_Complete, 0)))) NG_Is_Complete
           ,convert(BIT, max(convert(INT, isnull(IPS.NG_Is_Published, 0)))) NG_Is_Published
           ,convert(BIT, max(convert(INT, isnull(IPS.NG_Under_Review, 0)))) NG_Under_Review
           ,convert(BIT, min(convert(INT, isnull(IPS.EL_Is_Complete, 0)))) EL_Is_Complete
           ,convert(BIT, max(convert(INT, isnull(IPS.EL_Is_Published, 0)))) EL_Is_Published
           ,convert(BIT, max(convert(INT, isnull(IPS.EL_Under_Review, 0)))) EL_Under_Review
      FROM
            #Client_Dtl CD
            LEFT OUTER JOIN #Cost_Usage_Site_Dtl CUSD
                  ON CUSD.Client_Hier_Id = CD.Client_Hier_Id
            LEFT OUTER JOIN #Invoice_Participation_Site IPS
                  ON IPS.Client_Hier_Id = CD.Client_Hier_Id
                     AND IPS.Service_Month = CUSD.Service_Month
      WHERE
            ( IPS.Is_Published = 1
              OR IPS.Is_Published IS NULL )
      GROUP BY
            CD.Site_ID
           ,cd.Client_Hier_Id
           ,CD.Site_Name
      ORDER BY
            CD.Site_Name

END
;
GO

GRANT EXECUTE ON  [dbo].[cbmsCostUsageSite_GetSummaryTotalCostBySiteForDivision] TO [CBMSApplication]
GO
