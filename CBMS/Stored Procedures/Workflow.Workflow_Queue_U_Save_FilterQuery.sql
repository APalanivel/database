SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

  
/******        
NAME:    [Workflow].[Workflow_Queue_U_Save_FilterQuery]    
     
DESCRIPTION: Updating the Saved Filer values in Workflow.workflow_queue_saved_filter_query_value    
      
        
 INPUT PARAMETERS:        
 Name   DataType  Default Description        
  @TVP     Save_FilterQuery_SearchFilterValues READONLY             
  @TVPIdValues   Save_FilterQuery_SearchFilterValues READONLY       
  @vcSubQueueName  NVARCHAR(255)      
  @Module_Id   INT      
  @int_Created_User_Id INT     
------------------------------------------------------------        
     
 OUTPUT PARAMETERS:        
 Name   DataType  Default Description        
------------------------------------------------------------        
    
     
 USAGE EXAMPLES:    EXEC Workflow.Workflow_Queue_U_Save_FilterQuery   
            @TVP = NULL,             -- Save_FilterQuery_SearchFilterValues    
                                                @TVPIdValues = NULL,     -- Save_FilterQuery_SearchFilterValues    
                                                @vcSubQueueName = N'',   -- nvarchar(255)    
                                                @Module_Id = 0,       -- int    
                                                @int_Created_User_Id = 0 -- int    
------------------------------------------------------------        
AUTHOR INITIALS:        
 Initials Name        
------------------------------------------------------------        
RT   Ramakrishna Thummala    
------------------------------------------------------------     
 MODIFICATIONS         
     
 Initials Date        Modification        
------------------------------------------------------------        
 RT   SEP-2019 Ramakrishna Thummala Created    
    
******/  

    
CREATE  PROCEDURE [Workflow].[Workflow_Queue_U_Save_FilterQuery]              
(              
  @TVP     AS Save_FilterQuery_SearchFilterValues READONLY               
 ,@TVPIdValues   AS Save_FilterQuery_SearchFilterValues READONLY         
 ,@Saved_Filter_Id  INT        
 ,@Module_Id   AS INT        
 ,@int_Created_User_Id AS INT           
            
            
)              
AS              
BEGIN          
         
 SELECT SF.Workflow_Queue_Search_Filter_Id AS SearchFilters_Id,T.SearchFilterValue [Key] ,TV.SearchFilterValue  [Value]            
 INTO #tvp  FROM  Workflow.workflow_queue_search_filter SF                 
 JOIN Workflow.search_filter MSF  ON SF.Search_Filter_Id = MSF.Search_Filter_Id               
 JOIN  @TVP T ON T.SearchFilter_Name = MSF.Filter_Name      
 JOIN  @TVPIdValues TV ON TV.SearchFilter_Name = MSF.Filter_Name              
 WHERE SF.Workflow_Queue_Id = @Module_Id         
        
 DECLARE @int_FilterId INT         
         
      
           
 IF EXISTS (SELECT TOP 1 1 FROM #tvp)   AND   @Saved_Filter_Id != 0         
 BEGIN              
          
  DELETE FROM Workflow.workflow_queue_saved_filter_query_value WHERE Workflow_Queue_Saved_Filter_Query_Id = @Saved_Filter_Id        
        
  INSERT INTO Workflow.workflow_queue_saved_filter_query_value               
     (          
      Workflow_Queue_Saved_Filter_Query_Id          
      ,Workflow_Queue_Search_Filter_Id          
      ,Selected_Value          
      ,Selected_KeyValue          
      ,Created_User_Id          
      ,Created_Ts          
      ,Updated_User_Id          
      ,Last_Change_Ts        
   )             
            
   SELECT           
     @Saved_Filter_Id,          
     v.SearchFilters_Id,           
     [Key] ,          
     [Value],          
     @int_Created_User_Id,          
     GETDATE(),          
     @int_Created_User_Id,          
     GETDATE()         
  FROM #TVP v       
            
 END            
               
END       
     
       
   

GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_U_Save_FilterQuery] TO [CBMSApplication]
GO
