SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******************************************************************************************************                        
NAME: dbo.Invoice_Submission_Log_Upd
    
DESCRIPTION:    
    
      Update record in Invoice_Submission_Upd
    
INPUT PARAMETERS:    
      NAME                                      DATATYPE    DEFAULT           DESCRIPTION    
------------------------------------------------------------------------    

      
OUTPUT PARAMETERS:              
      NAME              DATATYPE    DEFAULT           DESCRIPTION 
-----------------------------------------------------------      



------------------------------------------------------------              
USAGE EXAMPLES:              
------------------------------------------------------------            

	Exec Invoice_Submission_Log_Upd
         
AUTHOR INITIALS:              
      INITIALS    NAME              
------------------------------------------------------------              
		HK			Harish Kurma
		      
MODIFICATIONS:    
      INITIALS    DATE			MODIFICATION              
------------------------------------------------------------              
      HK		  10 29 2018	Created
	  KVK		  11/12/2018	modified to alter SystemFileName to remove special characters which is the same logic in scraping process
	  KVK		  11/13/2018	append 'MI' to system file name
******************************************************************************************************/
CREATE PROCEDURE [dbo].[Invoice_Submission_Log_Upd]
(
    @Invoice_Submission_Log_Id INT,
    @Site_Id INT,
    @System_File_Name NVARCHAR(255),
    @Comment NVARCHAR(MAX),
    @User_Info_Id INT
)
AS
BEGIN

    SET NOCOUNT ON;

    DECLARE @Status_Cd INT;
    DECLARE @Comment_Type_Cd INT;

    SELECT @Status_Cd = cd.Code_Id
    FROM dbo.Code cd
        JOIN dbo.Codeset cs
            ON cd.Codeset_Id = cs.Codeset_Id
    WHERE cs.Codeset_Name = 'ProjectStatus'
          AND cd.Code_Dsc = 'Completed';

    SELECT @Comment_Type_Cd = cd.Code_Id
    FROM dbo.Code cd
        JOIN dbo.Codeset cs
            ON cd.Codeset_Id = cd.Codeset_Id
    WHERE cs.Codeset_Name = 'CommentType'
          AND cd.Code_Value = 'VarianceComment';


    DECLARE @Client_Hier_Id INT;

    SELECT @Client_Hier_Id = c.Client_Hier_Id
    FROM Core.Client_Hier c
    WHERE c.Site_Id = @Site_Id;

    DECLARE @Comment_Id INT;

    INSERT INTO dbo.Comment
    (
        Comment_Type_CD,
        Comment_User_Info_Id,
        Comment_Text,
        Comment_Dt
    )
    SELECT @Comment_Type_Cd,
           @User_Info_Id,
           @Comment,
           GETDATE()
    WHERE ISNULL(RTRIM(LTRIM(@Comment)),'') <> '';

    SELECT @Comment_Id = SCOPE_IDENTITY();
	
	DECLARE @invalid_string VARCHAR(100)
	
	SELECT @invalid_string = App_Config_Value
    FROM dbo.App_Config
    WHERE App_Config_Cd = 'Invalid_Characters';

    SELECT @System_File_Name = dbo.ufn_Remove_Special_Characters(@System_File_Name, @invalid_string);
    SELECT @System_File_Name = REPLACE(@System_File_Name, SPACE(1), SPACE(0));

    UPDATE isl
    SET isl.Status_Cd = @Status_Cd,
        isl.System_File_Name = 'MI_'+@System_File_Name,
        isl.Client_Hier_Id = @Client_Hier_Id,
        isl.Comment_Id = @Comment_Id
    FROM dbo.Invoice_Submission_Log isl
    WHERE isl.Invoice_Submission_Log_Id = @Invoice_Submission_Log_Id;
END;







GO
GRANT EXECUTE ON  [dbo].[Invoice_Submission_Log_Upd] TO [CBMSApplication]
GO
