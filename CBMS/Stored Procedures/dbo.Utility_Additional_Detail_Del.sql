SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: dbo.Utility_Additional_Detail_Del  

DESCRIPTION: It Deletes Utility Additional Detail for Selected Utility Additional Detail Id.
      
INPUT PARAMETERS:          
	NAME							DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Utility_Additional_Detail_Id	INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
	Begin Tran
		EXEC dbo.Utility_Additional_Detail_Del  2225
	Rollback Tran

	select * from dbo.Utility_Additional_Detail where Utility_Additional_Detail_Id = 2225

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE			MODIFICATION
------------------------------------------------------------
	PNR			23-August-10	CREATED
*/

CREATE PROCEDURE dbo.Utility_Additional_Detail_Del
    (
       @Utility_Additional_Detail_Id INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE 
	FROM
		dbo.Utility_Additional_Detail 
	WHERE
		Utility_Additional_Detail_Id = @Utility_Additional_Detail_Id
END
GO
GRANT EXECUTE ON  [dbo].[Utility_Additional_Detail_Del] TO [CBMSApplication]
GO
