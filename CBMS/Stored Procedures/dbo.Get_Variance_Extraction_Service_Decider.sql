SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




































/******                              
 NAME: dbo.[[Get_Variance_Process_Engine_By_Category]]                  
                              
 DESCRIPTION:   
                            
Rules:				  
				  Electric Power:
                Usage:
                                Cooling Degree Days
                                Heating Degree Days
                                Historical Total Usage
                Demand:
                                Historical Usage
                                Historical Demand
                Billed Demand:
                                Historical Usage
                                Historical Billed Demand
                Cost:
                                Historical Usage
                                (Strongly encouraged) Historical Billed Demand
                                (Alternative) Historical Demand
                                Historical Cost

Natural Gas:
                Usage:
                                Cooling Degree Days
                                Heating Degree Days
                                Historical Total Usage
                Cost:
                                Historical Usage
                                Historical Cost

Water:
                Usage:
                                Cooling Degree Days
                                Heating Degree Days
                                Historical Total Usage
                Cost:
                                Historical Usage
                                Historical Cost

Waste Water:
                Usage:
                                Cooling Degree Days
                                Heating Degree Days
                                Historical Total Usage
                Cost:
                                Historical Usage
                                Historical Cost
          
                              
 INPUT PARAMETERS:                
                           
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
@Account_Id			INT
@Commodity_ID      INT               NULL   
@service_month	   Datetime
      
                                  
 OUTPUT PARAMETERS:                
                                 
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
                              
 USAGE EXAMPLES:                                  
---------------------------------------------------------------------------------------------------------------                                  
      
--Commodity Level      
   
DECLARE	@return_value INT

EXEC dbo.[Get_Variance_Extraction_Service_Decider] 
      @Account_Id = 1671185               -- int
    , @Commodity_Id = 290            -- int
    , @Service_Month = '2019-01-01' -- date

SELECT	'Return Value' = @return_value

GO

  
     
                             
 AUTHOR INITIALS:    
 Arunkumar Palanivel AP          
             
 Initials              Name              
---------------------------------------------------------------------------------------------------------------  
     Steps(added by Arun):

	 1.Find the buckets associated to the account for the commodity and service month.
	 2.If the bucket is Total cost, then we have to give total cost.
		For total cost , we have to give additional info as per the above rule.
	3.check 12 of 18 data points to decide linear analogous

	Method Identification - 12 of 18 Rule
If the selected account has USAGE values for 12 or more months out of the last 18 months (start counting backwards from/not including the service month of the invoice start date), the query service will select Linear Regression variance method and continue with history query.

Note that 12 out of 18 rule does not require that the values be in 12 consecutive months. Just require 12 of 18 months to have usage values.

A value of 0 counts as a value and should be counted toward 12 of 18 rule.

Months marked as "No Data This Period" do NOT count as values toward the 12 of 18 rule.

Estimated usage values (either created by Gap-Filling procedure or entered by a user) will NOT count as values toward the 12 of 18 rule.

REQUIREMENT UPDATE (1/17/2020): Months with manually-entered values will NOT count as values toward the 12 of 18 rule.

REQUIREMENT UPDATE (1/17/2020): Months that were manually excluded as anomalous usage (see Anomalous Month section within Review Variance page) will NOT count as values toward the 12 of 18 rule.

If the selected account has usage values for less than 12 months out of the last 18 months, the query service will select Analogous Account variance method.
	4.It is possible that for same account /commodity one category can be linear and another one can be analogus. in that case, we have to send two request.
	however SP will give only on result set. 
	5.
                               
 MODIFICATIONS:            
                
 Initials              Date             Modification            
---------------------------------------------------------------------------------------------------------------   
AP					Jan 21 2020			New procedure to get data points for variance testing (as per the requirement mentioned above)  
AP					Feb 19,2020			Modified procedure to exclude anomalous month data 
AP					march 11,2020		Splitting this procedures in to small pieces and mocing few logic to UI      
   
******/
CREATE PROCEDURE [dbo].[Get_Variance_Extraction_Service_Decider]
      (
      @Account_Id    INT
    , @Commodity_Id  INT
    , @Service_Month DATE )
AS
      BEGIN

            SET NOCOUNT ON;

			DECLARE @start_date_performance DATETIME2 = GETDATE()
			
            DECLARE
                  @Client_Hier_Id                      INT
                , @Extraction_Service_History_Rule_Use INT
                , @Extraction_Service_History_Rule_Of  INT
				,@Second_Previous_Month Date

            CREATE TABLE [#Category_Service_Type]
                  ( [Bucket_Name]          VARCHAR(255)
                  , [Bucket_Type]          VARCHAR(25) NOT NULL
                  , [Bucket_Master_Id]     INT         NOT NULL
                  , [Default_uom_Type_Id]  INT
                  , [Service_Type]         VARCHAR(20)
                  , Variance_category_cd   INT
                  , variance_category_name VARCHAR(20),
				  Perform_Test           VARCHAR(10));

            DECLARE @Bucket_List TABLE
                  ( Bucket_Master_Id       INT
                  , Bucket_Name            VARCHAR(255)
                  , Default_uom_Type_Id    INT
                  , Bucket_Type_cd         INT
                  , Perform_Test           VARCHAR(10) DEFAULT 'False'
                  , Optional_Or_Mandatory  BIT
                  , Variance_category_cd   INT
                  , variance_category_name VARCHAR(20));

            DECLARE @Tvp_Bucket_category TABLE
                  ( row_num                INT IDENTITY(1, 1)
                  , Bucket_Master_Id       INT
                  , Variance_Category_Cd   INT
                  , variance_category_name VARCHAR(20));

            DECLARE @Bucket_master_id_TVP AS tvp_Bucket_Master_IDs
                  --( Bucket_Master_Id INT );


            SELECT
                  @Extraction_Service_History_Rule_Use = vepv.Param_Value
            FROM  dbo.Variance_Extraction_Service_Param vesp
                  INNER JOIN
                  dbo.Variance_Engine_Param_Value vepv
                        ON vepv.Variance_Extraction_Service_Param_Id = vesp.Variance_Extraction_Service_Param_Id
            WHERE vesp.Param_Name = 'Extraction_Service_History_Rule_Use';

            SELECT
                  @Extraction_Service_History_Rule_Of = vepv.Param_Value
            FROM  dbo.Variance_Extraction_Service_Param vesp
                  INNER JOIN
                  dbo.Variance_Engine_Param_Value vepv
                        ON vepv.Variance_Extraction_Service_Param_Id = vesp.Variance_Extraction_Service_Param_Id
            WHERE vesp.Param_Name = 'Extraction_Service_History_Rule_Of';

                 DECLARE
                  @Start_Date         DATE = dateadd(DAY, - ( day(dateadd(MONTH, -@Extraction_Service_History_Rule_Of, @Service_Month))) + 1, dateadd(MONTH, -@Extraction_Service_History_Rule_Of, @Service_Month))
                , @Previous_Month     DATE = dateadd(DAY, - ( day(dateadd(MONTH, -1, @Service_Month))) + 1, dateadd(MONTH, -1, @Service_Month))
                , @END_Date           DATE = dateadd(DAY, - ( day(@Service_Month)), @Service_Month)
                , @36_Months_Start_Dt DATE = dateadd(DAY, - ( day(dateadd(MONTH, -36, @Service_Month))) + 1, dateadd(MONTH, -36, @Service_Month));

            SELECT
                  @Second_Previous_Month = dateadd(DAY, - ( day(dateadd(MONTH, -1, @Previous_Month))) + 1, dateadd(MONTH, -1, @Previous_Month));


            --if the record exist for current service month then perform test is true
            INSERT INTO @Bucket_List (
                                           Bucket_Master_Id
                                         , Bucket_Name
                                         , Default_uom_Type_Id
                                         , Bucket_Type_cd
                                         , Perform_Test
                                         , Optional_Or_Mandatory
                                         , Variance_category_cd
                                         , variance_category_name
                                     )
                        SELECT
                              BM.Bucket_Master_Id
                            , BM.Bucket_Name
                            , BM.Default_Uom_Type_Id
                            , BM.Bucket_Type_Cd
                            , 'True'
                            , 1
                            , vcbn.Variance_Category_Cd
                            , c.Code_Value
                        FROM  dbo.Bucket_Master BM
                              INNER JOIN
                              dbo.Variance_category_Bucket_Map vcbn
                                    ON vcbn.Bucket_master_Id = BM.Bucket_Master_Id
                              JOIN
                              dbo.Code c
                                    ON c.Code_Id = vcbn.Variance_Category_Cd
                              JOIN
                              dbo.Cost_Usage_Account_Dtl cuad
                                    ON cuad.Bucket_Master_Id = BM.Bucket_Master_Id
                        WHERE BM.Commodity_Id = @Commodity_Id
                              AND   cuad.ACCOUNT_ID = @Account_Id
                              AND   cuad.Service_Month = @Service_Month
                              AND   BM.Is_Shown_On_Account = 1
                              AND   BM.Is_Active = 1;



            INSERT INTO @Bucket_List (
                                           Bucket_Master_Id
                                         , Bucket_Name
                                         , Default_uom_Type_Id
                                         , Bucket_Type_cd
                                         , Perform_Test
                                         , Optional_Or_Mandatory
                                         , Variance_category_cd
                                         , variance_category_name
                                     )
                        SELECT      DISTINCT
                                    BM.Bucket_Master_Id
                                  , BM.Bucket_Name
                                  , BM.Default_Uom_Type_Id
                                  , BM.Bucket_Type_Cd
                                  , 'False'
                                  , VCRM.Category_Required
                                  , VCBM.Variance_Category_Cd
                                  , c.Code_Value
                        FROM        dbo.Variance_Category_Required_Map VCRM
                                    JOIN
                                    dbo.Variance_category_Bucket_Map VCBM
                                          ON VCBM.Variance_Category_Cd = VCRM.Required_Variance_Category_Cd
                                    JOIN
                                    dbo.Code c
                                          ON c.Code_Id = VCBM.Variance_Category_Cd
                                    JOIN
                                    dbo.Bucket_Master BM
                                          ON BM.Bucket_Master_Id = VCRM.Bucket_Master_Id
                        WHERE       BM.Commodity_Id = @Commodity_Id
                                    AND   NOT EXISTS
                              (     SELECT
                                          1
                                    FROM  @Bucket_List bl1
                                    WHERE bl1.Bucket_Master_Id = VCRM.Bucket_Master_Id );

            SELECT
                  @Client_Hier_Id = cha.Client_Hier_Id
            FROM  Core.Client_Hier_Account cha
            WHERE cha.Account_Id = @Account_Id
                  AND   cha.Commodity_Id = @Commodity_Id;
				   

            INSERT      #Category_Service_Type (
                                                     Bucket_Name
                                                   , Bucket_Type
                                                   , Bucket_Master_Id
                                                   , Default_uom_Type_Id
                                                   , Service_Type
                                                   , Variance_category_cd
                                                   , variance_category_name
												   ,Perform_Test
                                               )
                        SELECT
                                    BL.Bucket_Name
                                  , bt.Code_Value AS Bucket_Type
                                  , BL.Bucket_Master_Id
                                  , BL.Default_uom_Type_Id
                                  , CASE WHEN count(sm.DATE_D) >= @Extraction_Service_History_Rule_Use
                                               THEN 'Linear'
                                         WHEN count(sm.DATE_D) < @Extraction_Service_History_Rule_Use
                                              AND    BL.Perform_Test = 'True'
                                               THEN 'Analogous'
                                         WHEN count(sm.DATE_D) < @Extraction_Service_History_Rule_Use
                                              AND    BL.Perform_Test = 'false'
                                              AND    BL.Optional_Or_Mandatory = 1
                                               THEN 'Analogous'
                                    END Service_Type
                                  , BL.Variance_category_cd
                                  , BL.variance_category_name
								  , bl.Perform_Test
                        --INTO
                        --    #Category_Service_Type
                        FROM        @Bucket_List BL
                                    INNER JOIN
                                    dbo.Code bt
                                          ON bt.Code_Id = BL.Bucket_Type_cd
                                    CROSS JOIN meta.DATE_DIM sm
                                    LEFT OUTER JOIN
                                    (dbo.Cost_Usage_Account_Dtl cuad
                                     INNER JOIN
                                     dbo.Code dsc
                                           ON dsc.Code_Id = cuad.Data_Source_Cd
                                     INNER JOIN
                                     dbo.Code dtc
                                           ON dtc.Code_Id = cuad.Data_Type_Cd)
                                          ON cuad.Bucket_Master_Id = BL.Bucket_Master_Id
                                             AND     sm.DATE_D = cuad.Service_Month
                                             AND     cuad.ACCOUNT_ID = @Account_Id
                                             AND     cuad.Client_Hier_ID = @Client_Hier_Id
                        WHERE       sm.DATE_D
                                    BETWEEN @Start_Date AND @Service_Month
                                    AND  cuad.Bucket_Value IS NOT NULL
                                    AND  dsc.Code_Value NOT IN (
                                               'CBMS'
                                             , 'DE')
                                    AND  dtc.Code_Value NOT IN ('Estimated')
                                    AND  NOT EXISTS
                              (     SELECT
                                          1
                                    FROM  dbo.Variance_Account_Month_Anomalous_Status vra
                                    WHERE vra.Account_Id = cuad.ACCOUNT_ID
                                          AND  vra.Service_Month = cuad.Service_Month
                                          AND  vra.Is_Anomalous = 1 )
                        GROUP BY    BL.Bucket_Name
                                  , bt.Code_Value
                                  , BL.Bucket_Master_Id
                                  , BL.Default_uom_Type_Id
                                  , BL.Perform_Test
                                  , BL.Optional_Or_Mandatory
                                  , BL.Variance_category_cd
                                  , BL.variance_category_name
                        ORDER BY    bt.Code_Value
                                  , BL.Bucket_Name;
								   

							DELETE cst FROM [#Category_Service_Type]  cst							
							WHERE cst.Service_Type ='Analogous'
							AND  NOT EXISTS (
							SELECT 1 FROM dbo.Cost_Usage_Account_Dtl cuad 
								WHERE cuad.ACCOUNT_ID = @Account_Id 
								AND cuad.Bucket_Master_Id = cst.Bucket_Master_Id
								AND cuad.Service_Month = @Previous_Month)


								/*
						  4.  If CBMS does not have historical usage data, all four tests will need to be analogous 
						  (usage is required for linear regression testing of the other buckets).  

								*/

								IF EXISTS (SELECT 1 FROM #Category_Service_Type 
								WHERE Service_Type ='Analogous'
								AND variance_category_name ='Usage')

								begin
								UPDATE #Category_Service_Type 
								SET Service_Type ='Analogous'

								end
								
								SELECT * FROM #Category_Service_Type
								WHERE Service_Type IS NOT NULL

							DROP TABLE #Category_Service_Type

      END;

GO
GRANT EXECUTE ON  [dbo].[Get_Variance_Extraction_Service_Decider] TO [CBMSApplication]
GO
