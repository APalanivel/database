
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [DBO].[Sr_Rfp_History_Ins]  
     
DESCRIPTION: 
	To Get Meter Information for Selected Account Id.
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Account_Id		INT						Utility Account
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        
  
	BEGIN TRANSACTION
		DECLARE @Sr_Rfp_Id INT
		SELECT * FROM dbo.SR_RFP rfp JOIN dbo.SR_RFP_ACCOUNT acc ON rfp.SR_RFP_ID = acc.SR_RFP_ID 
			WHERE rfp.INITIATED_BY = 16 AND rfp.COMMODITY_TYPE_ID = 291 AND acc.ACCOUNT_ID = 1
		EXEC dbo.Sr_Rfp_History_Ins 1,291,16,@Sr_Rfp_Id = @Sr_Rfp_Id OUTPUT
		SELECT @Sr_Rfp_Id
		SELECT * FROM dbo.SR_RFP rfp JOIN dbo.SR_RFP_ACCOUNT acc ON rfp.SR_RFP_ID = acc.SR_RFP_ID 
			WHERE rfp.SR_RFP_ID = @Sr_Rfp_Id
	ROLLBACK TRANSACTION
	
     
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
HG			Harihara Suthan G
RR			Raghu Reddy


MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
HG			07/31/2010	Created
HG			08/18/2010	Changed the status type of RFP to "New" from "Open" when creating a new RFP.
RR			2015-07-24	Global Sourcing - Inserting Queue id into dbo.SR_RFP table 

*/

CREATE PROCEDURE [dbo].[Sr_Rfp_History_Ins]
      @Account_Id_List VARCHAR(MAX)
     ,@Commodity_Id INT
     ,@Initiated_User_Id INT
     ,@Sr_Rfp_Id INT OUT
AS 
BEGIN

      SET NOCOUNT ON;
	
      DECLARE
            @Rfp_Status_Type_Id INT
           ,@Bid_Status_Type_Id INT
           ,@Queue_Id INT
	  
      DECLARE @Initated_Date DATE = getdate()
	  
      DECLARE @table_vendor TABLE
            ( 
             Account_id INT
            ,Contract_End_Date DATE
            ,Vendor_Id INT
            ,Max_Contract_End_Date DATE )

      SELECT
            @Rfp_Status_Type_Id = Entity_Id
      FROM
            dbo.Entity
      WHERE
            Entity_Description = 'RFP Status'
            AND Entity_Name = 'New'

      SELECT
            @Bid_Status_Type_Id = Entity_Id
      FROM
            dbo.Entity
      WHERE
            Entity_Description = 'BID_STATUS'
            AND Entity_Name = 'New'
            
      SELECT
            @Queue_Id = ui.QUEUE_ID
      FROM
            dbo.USER_INFO ui
      WHERE
            ui.USER_INFO_ID = @Initiated_User_Id
	
      INSERT      INTO dbo.SR_RFP
                  ( 
                   RFP_STATUS_TYPE_ID
                  ,COMMODITY_TYPE_ID
                  ,INITIATED_BY
                  ,Queue_Id )
      VALUES
                  ( 
                   @Rfp_Status_Type_Id
                  ,@Commodity_Id
                  ,@Initiated_User_Id
                  ,@Queue_Id )

      SET @Sr_Rfp_Id = scope_identity()
	
	
      INSERT      INTO SR_RFP_ACCOUNT
                  ( 
                   ACCOUNT_ID
                  ,SR_RFP_ID
                  ,BID_STATUS_TYPE_ID
                  ,IS_DELETED )
                  SELECT
                        cast(Segments AS INT)
                       ,@Sr_Rfp_Id
                       ,@Bid_Status_Type_Id
                       ,0
                  FROM
                        dbo.ufn_split(@Account_Id_List, ',')

      INSERT      INTO dbo.SR_RFP_ACCOUNT_METER_MAP
                  ( 
                   METER_ID
                  ,SR_RFP_ACCOUNT_ID )
                  SELECT
                        m.METER_ID
                       ,RfpAcc.SR_RFP_ACCOUNT_ID
                  FROM
                        dbo.SR_RFP_ACCOUNT RfpAcc
                        JOIN dbo.METER m
                              ON m.ACCOUNT_ID = RfpAcc.ACCOUNT_ID
                        JOIN dbo.Rate r
                              ON r.Rate_Id = m.Rate_Id
                  WHERE
                        RfpAcc.SR_RFP_ID = @Sr_Rfp_Id
                        AND r.COMMODITY_TYPE_ID = @Commodity_Id
		
      INSERT      INTO @table_vendor
                  SELECT
                        met.account_id
                       ,con.contract_end_date
                       ,suppacc.vendor_id
                       ,max(Con.Contract_End_Date) OVER ( PARTITION BY met.Account_Id ) Max_Contract_End_Date
                  FROM
                        meter met
                        JOIN supplier_account_meter_map meterMap
                              ON meterMap.meter_id = met.meter_id
                        JOIN account suppacc
                              ON meterMap.account_id = suppacc.account_id
                        JOIN contract con
                              ON meterMap.Contract_Id = Con.CONTRACT_ID
                        JOIN entity E2
                              ON con.contract_type_id = E2.entity_id
                        JOIN dbo.SR_RFP_ACCOUNT rfpAcc
                              ON rfpAcc.ACCOUNT_ID = met.ACCOUNT_ID
                  WHERE
                        E2.Entity_type = 129
                        AND E2.Entity_name = 'Supplier'
                        AND meterMap.is_history = 0
                        AND rfpAcc.SR_RFP_ID = @Sr_Rfp_Id
                        AND con.COMMODITY_TYPE_ID = @Commodity_Id
                  GROUP BY
                        met.account_id
                       ,con.contract_end_date
                       ,suppacc.vendor_id


      INSERT      INTO dbo.SR_RFP_CHECKLIST
                  ( 
                   SR_RFP_ACCOUNT_ID
                  ,RFP_INITIATED_DATE
                  ,INCUMBANT_ID )
                  SELECT
                        RfpAcc.SR_RFP_ACCOUNT_ID
                       ,@Initated_Date
                       ,v.Vendor_Id
                  FROM
                        dbo.SR_RFP_ACCOUNT RfpAcc
                        LEFT OUTER JOIN @table_vendor v
                              ON v.Account_Id = RfpAcc.Account_Id
                                 AND v.Contract_End_Date = v.Max_Contract_End_Date
                  WHERE
                        RfpAcc.SR_RFP_ID = @Sr_Rfp_Id
                  GROUP BY
                        RfpAcc.SR_RFP_ACCOUNT_ID
                       ,v.Vendor_Id

END

;
GO

GRANT EXECUTE ON  [dbo].[Sr_Rfp_History_Ins] TO [CBMSApplication]
GO
