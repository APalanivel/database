SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:        
 cbms_prod.dbo.Contract_Meters_Sel_For_Invoice         
        
DESCRIPTION:        
        
        
INPUT PARAMETERS:        
 Name     DataType  Default Description        
------------------------------------------------------------        
@contract_id	INT
@meter_id		VARCHAR(MAX) 
        
OUTPUT PARAMETERS:        
 Name   DataType  Default Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:        
------------------------------------------------------------        
	EXEC Contract_Meters_Sel_For_Invoice @contract_id = 11994,@meter_id = '3994,3995,3996,3997,'
	EXEC Contract_Meters_Sel_For_Invoice @contract_id = 12802,@meter_id = '885,886,887'
	
	Select * from supplier_account_meter_map where contract_Id = 166826

	EXEC CONTRACT_METERS_Sel_For_Invoice 166826,'1227304'

	SELECT DISTINCT TOP 100
      map.CONTRACT_ID
     ,map.meter_id
FROM
      dbo.supplier_account_meter_map AS map
      JOIN dbo.account a ON a.account_id = map.ACCOUNT_ID
      JOIN dbo.cu_invoice_service_month cism ON ci.ACCOUNT_ID = map.ACCOUNT_ID
      JOIN dbo.CU_INVOICE ci ON cism.CU_INVOICE_ID = ci.CU_INVOICE_ID
ORDER BY
	CONTRACT_ID
        
AUTHOR INITIALS:        
 Initials Name        
------------------------------------------------------------        
SKA		Shobhit Kumar Agrawal

MODIFICATIONS                
 Initials  Date   Modification        
------------------------------------------------------------        
SKA		06/03/2010  Created
SKA		06/11/2010	Added more usage example	
HG		07/19/2010  Removed the logic of fetching data from SAMM table
					(( SAMM.meter_disassociation_date > Account.Supplier_Account_Begin_Dt  
						OR SAMM.meter_disassociation_date IS NULL ))
					(After contract enhancement application is populating both Meter_association_date and meter_disassociation_Date column supplier_Account_meter_map and removing the entries when ever is_history = 1)
					Account.Account_id is mapped with Cu_Invoice.Account_Id changed this join to Cu_Invoice_Service_Month as Account_Id column moved to this table as a part of Group Bill Schema changes.
NR		2019-12-02	Add Contract - Added invoice begin and end dates and supplier dates.					
******/

CREATE PROCEDURE [dbo].[Contract_Meters_Sel_For_Invoice]
    (
        @contract_id INT
        , @meter_id VARCHAR(MAX)
    )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            map.ACCOUNT_ID
            , map.METER_ID
            , a.ACCOUNT_NUMBER
            , cism.SERVICE_MONTH
            , cism.Begin_Dt
            , cism.End_Dt
            , sac.Supplier_Account_Begin_Dt
            , sac.Supplier_Account_End_Dt
            , sac.Supplier_Account_Config_Id
        FROM
            dbo.SUPPLIER_ACCOUNT_METER_MAP AS map
            JOIN ufn_split(@meter_id, ',') segm
                ON CAST(segm.Segments AS INT) = map.METER_ID
            JOIN dbo.ACCOUNT a
                ON a.ACCOUNT_ID = map.ACCOUNT_ID
            JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.Account_ID = a.ACCOUNT_ID
            JOIN dbo.CU_INVOICE ci
                ON ci.CU_INVOICE_ID = cism.CU_INVOICE_ID
            JOIN dbo.Supplier_Account_Config sac
                ON sac.Supplier_Account_Config_Id = map.Supplier_Account_Config_Id
                   AND  sac.Account_Id = map.ACCOUNT_ID
        WHERE
            map.Contract_ID = @contract_id
            AND (   cism.Begin_Dt BETWEEN sac.Supplier_Account_Begin_Dt
                                  AND     ISNULL(sac.Supplier_Account_End_Dt, '9999-12-31')
                    OR  cism.End_Dt BETWEEN sac.Supplier_Account_Begin_Dt
                                    AND     ISNULL(sac.Supplier_Account_End_Dt, '9999-12-31')
                    OR  sac.Supplier_Account_Begin_Dt BETWEEN cism.Begin_Dt
                                                      AND     cism.End_Dt
                    OR  ISNULL(sac.Supplier_Account_End_Dt, '9999-12-31') BETWEEN cism.Begin_Dt
                                                                          AND     cism.End_Dt)
        GROUP BY
            map.ACCOUNT_ID
            , map.METER_ID
            , a.ACCOUNT_NUMBER
            , cism.SERVICE_MONTH
            , cism.Begin_Dt
            , cism.End_Dt
            , sac.Supplier_Account_Begin_Dt
            , sac.Supplier_Account_End_Dt
            , sac.Supplier_Account_Config_Id;

    END;



GO
GRANT EXECUTE ON  [dbo].[Contract_Meters_Sel_For_Invoice] TO [CBMSApplication]
GO
