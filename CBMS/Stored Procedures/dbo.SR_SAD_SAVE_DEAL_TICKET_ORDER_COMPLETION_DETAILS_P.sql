SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO

--select * from SR_DT_ORDER_COMPLETION_INFO where SR_DEAL_TICKET_ID = 922
CREATE   PROCEDURE dbo.SR_SAD_SAVE_DEAL_TICKET_ORDER_COMPLETION_DETAILS_P
@userId varchar(10),
@sessionId varchar(20),
@srDealTicketId int,
@orderPlacedDate datetime,
@saPlacingOrder varchar(2000),
@verbalConfirmDate datetime,
@saCompletingOrder varchar(2000),
@buyerComments varchar(2000),
@sellerComments varchar(2000)


AS
	set nocount on
IF (select count(1) from SR_DT_ORDER_COMPLETION_INFO where SR_DEAL_TICKET_ID = @srDealTicketId) = 0
BEGIN

	insert into SR_DT_ORDER_COMPLETION_INFO
		(SR_DEAL_TICKET_ID, ORDER_PLACED_DATE, SA_PLACING_ORDER, VERBAL_CONFIRM_DATE, 
		 SA_COMPLETING_ORDER, BUYER_COMMENTS, SELLER_COMMENTS)
	
	values	(@srDealTicketId, @orderPlacedDate, @saPlacingOrder, @verbalConfirmDate,
		 @saCompletingOrder, @buyerComments, @sellerComments)

END

ELSE

BEGIN

	update SR_DT_ORDER_COMPLETION_INFO set ORDER_PLACED_DATE=@orderPlacedDate, 
		SA_PLACING_ORDER=@saPlacingOrder, VERBAL_CONFIRM_DATE=@verbalConfirmDate,
		SA_COMPLETING_ORDER=@saCompletingOrder, BUYER_COMMENTS=@buyerComments,
		SELLER_COMMENTS=@sellerComments 
	where
		SR_DEAL_TICKET_ID=@srDealTicketId


END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_SAVE_DEAL_TICKET_ORDER_COMPLETION_DETAILS_P] TO [CBMSApplication]
GO
