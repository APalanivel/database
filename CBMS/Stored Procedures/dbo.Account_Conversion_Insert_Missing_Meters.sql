SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********           
    
NAME:    
     dbo.Account_Conversion_Insert_Missing_Meters    
           
DESCRIPTION:              
       
          
 INPUT PARAMETERS:              
                         
 Name                        DataType         Default       Description            
---------------------------------------------------------------------------------------------------------------          
              
 OUTPUT PARAMETERS:              
                               
 Name                        DataType         Default       Description            
---------------------------------------------------------------------------------------------------------------          
              
 USAGE EXAMPLES:                                
---------------------------------------------------------------------------------------------------------------                                
     
 BEGIN TRAN    
 EXEC dbo.Account_Conversion_Insert_Missing_Meters    
 ROLLBACK TRAN    
    
 AUTHOR INITIALS:    
    
 Initials   Name    
---------------------------------------------------------------------------------------------------------------    
 AKR        Ashok Kumar Raju    
 RR			Raghu Reddy    
 SP			Sandeep Pigilam    
 SC		    Sreenivasulu Cheerala
 MODIFICATIONS:    
    
 Initials   Date        Modification    
---------------------------------------------------------------------------------------------------------------    
 AKR        2014-06-01  Created.    
 RR   2015-11-30 Global Sourcing - Phase2 - Modified to insert new column Meter_Type into meter table    
 HG   2017-03-08 CPH using the temp table built with in SSIS package instead of Physical Staging table.    
 SP   2018-02-13 MAINT-6838,Modify the SQL statement which inserts the data in to Meter table to take the min address id if more than one matches.    
 SC		2019-05-12 added insert script for primary meter and budget calc level table.              
*********/
CREATE PROCEDURE [dbo].[Account_Conversion_Insert_Missing_Meters]
AS
    BEGIN

        DECLARE
            @enity_id INT
            , @Budget_Calc_Level INT;

        SELECT
            @Budget_Calc_Level = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            c.Code_Value = 'Account'
            AND cs.Codeset_Name = 'BudgetCalcLevel';

        SELECT
            @enity_id = ENTITY_ID
        FROM
            CBMS.dbo.ENTITY
        WHERE
            ENTITY_NAME = 'ACCOUNT_TABLE'
            AND ENTITY_DESCRIPTION = 'Table_Type';

        /*-----  Inserting Missing Meters. ------------ First inserting into Account_variance as left joining with meters*/
        INSERT INTO CBMS.dbo.Account_Variance_Consumption_Level
             (
                 ACCOUNT_ID
                 , Variance_Consumption_Level_Id
             )
        SELECT
            a.ACCOUNT_ID
            , conv_a.[Consumption Level ID]
        FROM
            #Data_Conversion_Accounts_Stage conv_a
            JOIN CBMS.dbo.ACCOUNT a
                ON a.SITE_ID = conv_a.[Site id]
                   AND  a.ACCOUNT_NUMBER = conv_a.[ACCOUNT NUMBER]
                   AND  a.VENDOR_ID = conv_a.[Vendor ID]
            LEFT JOIN CBMS.dbo.Account_Variance_Consumption_Level accl
                ON accl.ACCOUNT_ID = a.ACCOUNT_ID
                   AND  accl.Variance_Consumption_Level_Id = conv_a.[Consumption Level ID]
            LEFT JOIN CBMS.dbo.METER m
                ON m.ACCOUNT_ID = a.ACCOUNT_ID
                   AND  m.METER_NUMBER = conv_a.METER
        WHERE
            m.METER_NUMBER IS NULL
            AND accl.Variance_Consumption_Level_Id IS NULL
            AND conv_a.Is_Validation_Passed = 1
            AND a.ACCOUNT_TYPE_ID = 38
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    CBMS.dbo.Account_Variance_Consumption_Level avcl
                               WHERE
                                    avcl.ACCOUNT_ID = a.ACCOUNT_ID
                                    AND avcl.Variance_Consumption_Level_Id = conv_a.[Consumption Level ID])
        GROUP BY
            a.ACCOUNT_ID
            , conv_a.[Consumption Level ID];

        INSERT INTO CBMS.dbo.ENTITY_AUDIT
             (
                 ENTITY_ID
                 , ENTITY_IDENTIFIER
                 , USER_INFO_ID
                 , AUDIT_TYPE
                 , MODIFIED_DATE
             )
        SELECT
            @enity_id
            , a.ACCOUNT_ID
            , 16
            , 2
            , GETDATE()
        FROM
            CBMS.dbo.ACCOUNT a
            JOIN #Data_Conversion_Accounts_Stage conv_a
                ON a.ACCOUNT_NUMBER = conv_a.[ACCOUNT NUMBER]
                   AND  a.SITE_ID = conv_a.[Site id]
                   AND  a.VENDOR_ID = conv_a.[Vendor ID]
            LEFT JOIN CBMS.dbo.METER m
                ON m.ACCOUNT_ID = a.ACCOUNT_ID
                   AND  m.METER_NUMBER = conv_a.METER
        WHERE
            m.METER_NUMBER IS NULL
            AND a.ACCOUNT_TYPE_ID = 38
            AND conv_a.Is_Validation_Passed = 1
        GROUP BY
            a.ACCOUNT_ID;


        INSERT INTO CBMS.dbo.METER
             (
                 RATE_ID
                 , PURCHASE_METHOD_TYPE_ID
                 , ACCOUNT_ID
                 , ADDRESS_ID
                 , METER_NUMBER
                 , Meter_Type_Cd
             )
        SELECT
            conv_a.[Rate ID]
            , e.ENTITY_ID
            , a.ACCOUNT_ID
            , MIN(CASE WHEN conv_a.[Is Default To Primary] = 'NO' THEN ads.ADDRESS_ID
                      ELSE s.PRIMARY_ADDRESS_ID
                  END) address_id
            , conv_a.METER
            , mtrtyp.Code_Id
        FROM
            #Data_Conversion_Accounts_Stage conv_a
            INNER JOIN CBMS.dbo.ENTITY e
                ON e.ENTITY_DESCRIPTION = 'Purchase Method'
                   AND  e.ENTITY_NAME = conv_a.[Purchase Method]
            JOIN CBMS.dbo.SITE s
                ON s.SITE_ID = conv_a.[Site id]
            JOIN CBMS.dbo.ACCOUNT a
                ON a.SITE_ID = s.SITE_ID
                   AND  a.ACCOUNT_NUMBER = conv_a.[ACCOUNT NUMBER]
                   AND  a.VENDOR_ID = conv_a.[Vendor ID]
            LEFT JOIN CBMS.dbo.METER m
                ON m.ACCOUNT_ID = a.ACCOUNT_ID
                   AND  m.METER_NUMBER = conv_a.METER
            LEFT JOIN CBMS.dbo.ADDRESS ads
                ON ads.ADDRESS_PARENT_ID = s.SITE_ID
                   AND  conv_a.[Site Address] = ads.ADDRESS_LINE1
            LEFT JOIN(dbo.Code mtrtyp
                      JOIN dbo.Codeset cs
                          ON mtrtyp.Codeset_Id = cs.Codeset_Id)
                ON mtrtyp.Code_Value = LTRIM(RTRIM(conv_a.Meter_Type))
                   AND  cs.Codeset_Name = 'Meter Type'
        WHERE
            m.METER_NUMBER IS NULL
            AND a.ACCOUNT_TYPE_ID = 38
            AND conv_a.Is_Validation_Passed = 1
        GROUP BY
            conv_a.[Rate ID]
            , e.ENTITY_ID
            , a.ACCOUNT_ID
            , conv_a.METER
            , mtrtyp.Code_Id;

        ---Insert into Primary Meter table   ;    
        WITH Cte_Primary
        AS (
               SELECT
                    a.ACCOUNT_ID
                    , conv_a.Commodity
                    , m.METER_ID
                    , DENSE_RANK() OVER (PARTITION BY
                                             a.ACCOUNT_ID
                                             , conv_a.Commodity
                                         ORDER BY
                                             m.METER_ID ASC) AS Row_Num
               FROM
                    #Data_Conversion_Accounts_Stage conv_a
                    INNER JOIN CBMS.dbo.ACCOUNT a
                        ON a.SITE_ID = conv_a.[Site id]
                           AND  a.ACCOUNT_NUMBER = conv_a.[ACCOUNT NUMBER]
                           AND  a.VENDOR_ID = conv_a.[Vendor ID]
                    INNER JOIN dbo.METER m
                        ON m.ACCOUNT_ID = a.ACCOUNT_ID
                           AND  m.RATE_ID = conv_a.[Rate ID]
               WHERE
                    NOT EXISTS (   SELECT
                                        1
                                   FROM
                                        Budget.Account_Commodity_Primary_Meter acpm
                                   WHERE
                                        acpm.Account_Id = a.ACCOUNT_ID
                                        AND acpm.Commodity_Id = conv_a.Commodity)
           )
        INSERT INTO Budget.Account_Commodity_Primary_Meter
             (
                 Account_Id
                 , Commodity_Id
                 , Meter_Id
                 , Created_User_Id
                 , Created_Ts
                 , Updated_User_Id
                 , Last_Change_Ts
             )
        SELECT
            cte.ACCOUNT_ID
            , cte.Commodity
            , cte.METER_ID
            , 16
            , GETDATE()
            , 16
            , GETDATE()
        FROM
            Cte_Primary cte
        WHERE
            cte.Row_Num = 1;

        ---insert Budget Calc Level As Account  for each commodity              
        INSERT INTO Budget.Account_Commodity_Budget_Calc_Level
             (
                 Account_Id
                 , Commodity_Id
                 , Budget_Calc_Level_Cd
                 , Created_User_Id
                 , Created_Ts
                 , Updated_User_Id
                 , Last_Change_Ts
             )
        SELECT  DISTINCT
                a.ACCOUNT_ID
                , conv_a.Commodity
                , @Budget_Calc_Level
                , 16
                , GETDATE()
                , 16
                , GETDATE()
        FROM
            #Data_Conversion_Accounts_Stage conv_a
            INNER JOIN CBMS.dbo.ACCOUNT a
                ON a.SITE_ID = conv_a.[Site id]
                   AND  a.ACCOUNT_NUMBER = conv_a.[ACCOUNT NUMBER]
                   AND  a.VENDOR_ID = conv_a.[Vendor ID]
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                Budget.Account_Commodity_Budget_Calc_Level Acbcl
                           WHERE
                                Acbcl.Account_Id = a.ACCOUNT_ID
                                AND Acbcl.Commodity_Id = conv_a.Commodity);

    END;
GO


GRANT EXECUTE ON  [dbo].[Account_Conversion_Insert_Missing_Meters] TO [ETL_Execute]
GO
