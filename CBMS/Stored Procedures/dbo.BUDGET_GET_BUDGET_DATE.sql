SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.BUDGET_GET_BUDGET_DATE

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@budgetId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

--exec dbo.BUDGET_GET_BUDGET_START_MONTH '' ,''

--select budget_start_month from budget where budget_id = 1427

CREATE	PROCEDURE dbo.BUDGET_GET_BUDGET_DATE
	@budgetId int

	AS
begin
		set nocount on
		
		select	budget_date

		from	budget 

		where 	budget_id = @budgetId
		

		end
GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_BUDGET_DATE] TO [CBMSApplication]
GO
