SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:   dbo.Invoice_Collection_Queue_Bulk_Invoice_Received_Status_Upd           
                  
Description:                  
   This sproc to get the Invoice Collection Records for the given filters         
                               
 Input Parameters:                  
    Name         DataType        Default   Description                    
-----------------------------------------------------------------------------------------------------------------                    
  
        
 Output Parameters:                        
    Name        DataType   Default   Description                    
-----------------------------------------------------------------------------------------------------------------                    
                  
 Usage Examples:                      
-----------------------------------------------------------------------------------------------------------------                    
    
   declare  @tvp_Invoice_Collection_Queue_Received_Status_Upd_dtls tvp_Invoice_Collection_Queue_Received_Status_Upd_dtls
   Select * from @tvp_Invoice_Collection_Queue_Received_Status_Upd_dtls
    

            
                  
Author Initials:                  
    Initials  Name                  
----------------------------------------------------------------------------------------                    
 RKV    Ravi Kumar Vegesna    
 
 Modifications:                  
    Initials        Date   Modification                  
----------------------------------------------------------------------------------------                    
    RKV    2019-07-04  Created For Invoice_Collection.
	RKV    2019-09-05  Added user_info_id and Last_change_Ts update to the ICQ table.    
           
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Queue_Bulk_Invoice_Received_Status_Upd]
     (
         @tvp_Invoice_Collection_Queue tvp_Invoice_Collection_Queue READONLY
         , @User_Info_Id INT
     )
AS
    BEGIN

        DECLARE @Received_Status_Cd INT;

        DECLARE @ICQ_Status_Upd TABLE
              (
                  Invoice_Collection_Queue_Id INT
                  , Upload_Status VARCHAR(250)
              );

        SELECT
            @Received_Status_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'ICR Status'
            AND c.Code_Value = 'Received';

        INSERT INTO @ICQ_Status_Upd
             (
                 Invoice_Collection_Queue_Id
                 , Upload_Status
             )
        SELECT
            icq.Invoice_Collection_Queue_Id
            , 'Failed; Current Status is received'
        FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN @tvp_Invoice_Collection_Queue ticq
                ON ticq.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
        WHERE
            icq.Status_Cd = @Received_Status_Cd;



        UPDATE
            icq
        SET
            icq.Invoice_File_Name = ticq.Invoice_File_Name
            , icq.Status_Cd = @Received_Status_Cd
            , icq.Received_Status_Updated_Dt = ticq.Invoice_Date_Received
            , icq.Updated_User_Id = ISNULL(@User_Info_Id,icq.Updated_User_Id)
            , icq.Last_Change_Ts = GETDATE()
        FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN @tvp_Invoice_Collection_Queue ticq
                ON ticq.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                @ICQ_Status_Upd isu
                           WHERE
                                isu.Invoice_Collection_Queue_Id = ticq.Invoice_Collection_Queue_Id);



        SELECT
            ticq.Invoice_Collection_Queue_Id
            , ISNULL(isu.Upload_Status, 'Success') Upload_Status
        FROM
            @tvp_Invoice_Collection_Queue ticq
            LEFT OUTER JOIN @ICQ_Status_Upd isu
                ON isu.Invoice_Collection_Queue_Id = ticq.Invoice_Collection_Queue_Id;

    END;






GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Queue_Bulk_Invoice_Received_Status_Upd] TO [CBMSApplication]
GO
