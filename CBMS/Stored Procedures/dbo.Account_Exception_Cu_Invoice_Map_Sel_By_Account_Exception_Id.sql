SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
                           
 NAME: dbo.Account_Exception_Cu_Invoice_Map_Sel_By_Account_Exception_Id                  
                            
 DESCRIPTION:        
  Exception history details based on Exception Id.                     
                            
 INPUT PARAMETERS:        
                           
 Name                               DataType          Default       Description        
-------------------------------------------------------------------------------------    
 @Account_Exception_Id   INT          
                            
 OUTPUT PARAMETERS:        
                                 
 Name                               DataType          Default       Description        
-------------------------------------------------------------------------------------                              
 USAGE EXAMPLES:                                
-------------------------------------------------------------------------------------                   
  
 
 EXEC dbo.Account_Exception_Cu_Invoice_Map_Sel_By_Account_Exception_Id  376706    
  
                     
                           
 AUTHOR INITIALS:      
         
 Initials                   Name        
-------------------------------------------------------------------------------------    
 NR                     Narayana Reddy                              
                             
 MODIFICATIONS:      
                             
 Initials               Date            Modification      
-------------------------------------------------------------------------------------    
 NR                     2020-06-16      Created for SE2017- 981                         
                           
******/

CREATE PROCEDURE [dbo].[Account_Exception_Cu_Invoice_Map_Sel_By_Account_Exception_Id]
     (
         @Account_Exception_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @New_Exception_Status_Cd INT
            , @In_Progress_Exception_Status_Cd INT;


        SELECT
            @New_Exception_Status_Cd = MAX(CASE WHEN c.Code_Value = 'New' THEN c.Code_Id
                                           END)
            , @In_Progress_Exception_Status_Cd = MAX(CASE WHEN c.Code_Value = 'In Progress' THEN c.Code_Id
                                                     END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Status'
            AND cs.Std_Column_Name = 'Exception_Status_Cd'
            AND c.Code_Value IN ( 'New', 'In Progress' );



        SELECT
            cism.CU_INVOICE_ID
            , ci.CBMS_IMAGE_ID
            , ae.Account_Id
            , ci.IS_PROCESSED
        FROM
            dbo.Account_Exception ae
            INNER JOIN dbo.Account_Exception_Cu_Invoice_Map aeci
                ON ae.Account_Exception_Id = aeci.Account_Exception_Id
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.CU_INVOICE_ID = aeci.Cu_Invoice_Id
            INNER JOIN dbo.CU_INVOICE ci
                ON ci.CU_INVOICE_ID = cism.CU_INVOICE_ID
        WHERE
            ae.Account_Exception_Id = @Account_Exception_Id
            AND aeci.Status_Cd IN ( @New_Exception_Status_Cd, @In_Progress_Exception_Status_Cd )
            AND ae.Exception_Status_Cd IN ( @New_Exception_Status_Cd, @In_Progress_Exception_Status_Cd )
        GROUP BY
            cism.CU_INVOICE_ID
            , ci.CBMS_IMAGE_ID
            , ae.Account_Id
            , ci.IS_PROCESSED;


    END;

GO
GRANT EXECUTE ON  [dbo].[Account_Exception_Cu_Invoice_Map_Sel_By_Account_Exception_Id] TO [CBMSApplication]
GO
