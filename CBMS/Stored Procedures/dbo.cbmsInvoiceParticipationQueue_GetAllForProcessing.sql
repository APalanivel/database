
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********  
NAME:	dbo.cbmsInvoiceParticipationQueue_GetAllForProcessing   

DESCRIPTION:     

	
INPUT PARAMETERS:    
Name								DataType		Default		Description    
----------------------------------------------------------------------------    
@MyAccountId					 	INT      

OUTPUT PARAMETERS:    
Name					DataType		Default		Description    
----------------------------------------------------------------------------    

USAGE EXAMPLES:    
----------------------------------------------------------------------------   
 
EXEC cbmsInvoiceParticipationQueue_GetAllForProcessing 49



AUTHOR INITIALS:    
Initials	Name    
----------------------------------------------------------------------------    
NR			Narayana Reddy
 
MODIFICATIONS     
Initials	Date			Modification    
-----------------------------------------------------------------------------    
NR			2017-04-24		Added Header and Modified for MAINT-5046(5244).
******/

CREATE  PROCEDURE [dbo].[cbmsInvoiceParticipationQueue_GetAllForProcessing] ( @MyAccountId INT )
AS 
BEGIN

      SET nocount ON

      DECLARE
            @batch_id INT
           ,@Pending_Status_Cd INT
           ,@Error_Status_Cd INT
      
      
      SELECT
            @Pending_Status_Cd = MAX(CASE WHEN c.Code_Value = 'Pending' THEN c.Code_Id
                                          ELSE 0
                                     END)
           ,@Error_Status_Cd = MAX(CASE WHEN c.Code_Value = 'Error' THEN c.Code_Id
                                        ELSE 0
                                   END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Request Status'
            AND c.Code_Value IN ( 'Pending', 'Error' )
      



      INSERT      INTO invoice_participation_batch
                  ( batch_date )
      VALUES
                  ( GETDATE() )

      SET @batch_id = @@IDENTITY

      UPDATE
            invoice_participation_queue
      SET   
            invoice_participation_batch_id = @batch_id
           ,Status_Cd = @Pending_Status_Cd
           ,Error_Desc = NULL
      FROM
            invoice_participation_queue q
            JOIN vwAccountCommodity vac
                  ON vac.account_id = q.account_id
      WHERE
            ( q.invoice_participation_batch_id IS NULL )
            OR ( q.INVOICE_PARTICIPATION_BATCH_ID IS NOT NULL
                 AND q.Status_Cd = @Error_Status_Cd )

--	set nocount off

      SELECT
            invoice_participation_queue_id
           ,event_type
           ,client_id
           ,division_id
           ,site_id
           ,account_id
           ,service_month
           ,event_by_id
           ,event_date
           ,invoice_participation_batch_id
      FROM
            invoice_participation_queue ipq WITH ( NOLOCK )
      WHERE
            ipq.INVOICE_PARTICIPATION_BATCH_ID IS NOT NULL
            AND ipq.Status_Cd = @Pending_Status_Cd
      ORDER BY
            invoice_participation_queue_id ASC

END


;
GO

GRANT EXECUTE ON  [dbo].[cbmsInvoiceParticipationQueue_GetAllForProcessing] TO [CBMSApplication]
GO
