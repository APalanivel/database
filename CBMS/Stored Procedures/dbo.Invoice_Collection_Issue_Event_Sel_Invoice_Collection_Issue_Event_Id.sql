SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
 NAME: dbo.Invoice_Collection_Issue_Event_Sel_Invoice_Collection_Issue_Event_Id          
                          
 DESCRIPTION:                          
   To get Invoice_Collection_Issue_Event_desc        
                          
 INPUT PARAMETERS:            
                       
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
@Invoice_Collection_Issue_Event_Id INT  
  
                               
 OUTPUT PARAMETERS:            
                             
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
                          
 USAGE EXAMPLES:                              
---------------------------------------------------------------------------------------------------------------                              
  
          exec [dbo].[Invoice_Collection_Issue_Event_Sel_Invoice_Collection_Issue_Event_Id] 61                      
                         
 AUTHOR INITIALS:          
         
 Initials              Name          
---------------------------------------------------------------------------------------------------------------                        
RKV      Ravi kumar vegesna                           
  
 MODIFICATIONS:        
            
 Initials              Date             Modification        
---------------------------------------------------------------------------------------------------------------        
 RKV                   2018-02-03       SE2017-273,Created           
                        
******/      
  
CREATE PROCEDURE [dbo].[Invoice_Collection_Issue_Event_Sel_Invoice_Collection_Issue_Event_Id]
      ( 
       @Invoice_Collection_Issue_Event_Id INT
      ,@Issue_Event_Type_Cd INT
      ,@Issue_Status_Cd INT
      ,@Start_Index INT = 1
      ,@End_Index INT = 2147483647 )
AS 
BEGIN                  
      SET NOCOUNT ON;    
  
 
  
      DECLARE
            @Invoice_Collection_Activity_Id INT
           ,@Event_Desc VARCHAR(MAX)
           ,@Total_Count INT 
  
      CREATE TABLE #Event_Cnt
            ( 
             Invoice_Collection_Queue_Id INT
            ,Event_Cnt INT )
  
  
  
      SELECT
            @Event_Desc = ie.Event_Desc
           ,@Invoice_Collection_Activity_Id = ica.Invoice_Collection_Activity_Id
      FROM
            Invoice_Collection_Issue_Event ie
            INNER JOIN dbo.Invoice_Collection_Issue_Log icil
                  ON ie.Invoice_Collection_Issue_Log_Id = icil.Invoice_Collection_Issue_Log_Id
            INNER JOIN dbo.Invoice_Collection_Activity ica
                  ON icil.Invoice_Collection_Activity_Id = ica.Invoice_Collection_Activity_Id
      WHERE
            ie.Invoice_Collection_Issue_Event_Id = @Invoice_Collection_Issue_Event_Id
            AND ie.Issue_Event_Type_Cd =@Issue_Event_Type_Cd
            AND icil.Issue_Status_Cd = @Issue_Status_Cd ;  
      
      SELECT TOP ( @End_Index )
            cha.Display_Account_Number
           ,CAST(icq.Collection_Start_Dt AS VARCHAR(15)) + ' / ' + CAST(icq.Collection_End_Dt AS VARCHAR(15)) Period_To_Chase
           ,ie.Event_Desc
           ,ie.Invoice_Collection_Issue_Event_Id
           ,ica.Invoice_Collection_Activity_Id
           ,ROW_NUMBER() OVER ( ORDER BY cha.Display_Account_Number ) + 1 AS Row_Num
           ,COUNT(1) OVER ( ) Total_Count
           ,icil.Invoice_Collection_Queue_Id
           
      INTO
            #Invoice_Collection_Issue_Event
      FROM
            dbo.Invoice_Collection_Issue_Log icil
            INNER JOIN dbo.Invoice_Collection_Activity ica
                  ON icil.Invoice_Collection_Activity_Id = ica.Invoice_Collection_Activity_Id
            INNER JOIN Invoice_Collection_Issue_Event ie
                  ON icil.Invoice_Collection_Issue_Log_Id = ie.Invoice_Collection_Issue_Log_Id
            INNER JOIN dbo.Invoice_Collection_Queue icq
                  ON icq.Invoice_Collection_Queue_Id = icil.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                  ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN core.Client_Hier_Account cha
                  ON cha.Account_Id = icac.Account_Id
      WHERE
            ica.Invoice_Collection_Activity_Id = @Invoice_Collection_Activity_Id
            AND ie.Event_Desc = @Event_Desc
            AND ie.Issue_Event_Type_Cd =@Issue_Event_Type_Cd
            AND ie.Invoice_Collection_Issue_Event_Id <> @Invoice_Collection_Issue_Event_Id
      GROUP BY
            cha.Display_Account_Number
           ,icq.Collection_Start_Dt
           ,icq.Collection_End_Dt
           ,ie.Event_Desc
           ,ie.Invoice_Collection_Issue_Event_Id
           ,ica.Invoice_Collection_Activity_Id
           ,icil.Invoice_Collection_Queue_Id
            
      SELECT
            @Total_Count = ISNULL(MAX(Total_Count) + 1, 1)
      FROM
            #Invoice_Collection_Issue_Event icie
   
            /* updating the row number for the selected event as it has to be always the first record*/
            
      INSERT      INTO #Invoice_Collection_Issue_Event
                  ( 
                   Display_Account_Number
                  ,Period_To_Chase
                  ,Event_Desc
                  ,Invoice_Collection_Issue_Event_Id
                  ,Invoice_Collection_Activity_Id
                  ,Row_Num
                  ,Total_Count
                  ,Invoice_Collection_Queue_Id )
                  SELECT
                        cha.Display_Account_Number
                       ,CAST(icq.Collection_Start_Dt AS VARCHAR(15)) + ' / ' + CAST(icq.Collection_End_Dt AS VARCHAR(15)) Period_To_Chase
                       ,ie.Event_Desc
                       ,ie.Invoice_Collection_Issue_Event_Id
                       ,ica.Invoice_Collection_Activity_Id
                       ,1
                       ,@Total_Count
                       ,icil.Invoice_Collection_Queue_Id
                  FROM
                        dbo.Invoice_Collection_Issue_Log icil
                        INNER JOIN dbo.Invoice_Collection_Activity ica
                              ON icil.Invoice_Collection_Activity_Id = ica.Invoice_Collection_Activity_Id
                        INNER JOIN Invoice_Collection_Issue_Event ie
                              ON icil.Invoice_Collection_Issue_Log_Id = ie.Invoice_Collection_Issue_Log_Id
                        INNER JOIN dbo.Invoice_Collection_Queue icq
                              ON icq.Invoice_Collection_Queue_Id = icil.Invoice_Collection_Queue_Id
                        INNER JOIN dbo.Invoice_Collection_Account_Config icac
                              ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                        INNER JOIN core.Client_Hier_Account cha
                              ON cha.Account_Id = icac.Account_Id
                  WHERE
                        ica.Invoice_Collection_Activity_Id = @Invoice_Collection_Activity_Id
                        AND ie.Event_Desc = @Event_Desc
                        AND ie.Invoice_Collection_Issue_Event_Id = @Invoice_Collection_Issue_Event_Id
                  GROUP BY
                        cha.Display_Account_Number
                       ,icq.Collection_Start_Dt
                       ,icq.Collection_End_Dt
                       ,ie.Event_Desc
                       ,ie.Invoice_Collection_Issue_Event_Id
                       ,ica.Invoice_Collection_Activity_Id
                       ,icil.Invoice_Collection_Queue_Id
            
      INSERT      INTO #Event_Cnt
                  ( 
                   Invoice_Collection_Queue_Id
                  ,Event_Cnt )
                  SELECT
                        icil.Invoice_Collection_Queue_Id
                       ,COUNT(icil.Invoice_Collection_Issue_Log_Id) Event_Cnt
                  FROM
                        dbo.Invoice_Collection_Issue_Log icil
                        INNER JOIN dbo.Invoice_Collection_Issue_Event icie
                              ON icil.Invoice_Collection_Issue_Log_Id = icie.Invoice_Collection_Issue_Log_Id
                        INNER JOIN #Invoice_Collection_Issue_Event icie2
                              ON icie2.Invoice_Collection_Queue_Id = icil.Invoice_Collection_Queue_Id
                  GROUP BY
                        icil.Invoice_Collection_Queue_Id
    
      
      SELECT
            cicie.Display_Account_Number
           ,cicie.Period_To_Chase
           ,cicie.Event_Desc
           ,cicie.Invoice_Collection_Issue_Event_Id
           ,cicie.Invoice_Collection_Activity_Id
           ,@Total_Count Total_Count
           ,cicie.Row_Num
           ,cicie.Invoice_Collection_Queue_Id
           ,ec.Event_Cnt
           ,@Issue_Event_Type_Cd Issue_Event_Type_Cd
           ,@Issue_Status_Cd Issue_Status_Cd
           
      FROM
            #Invoice_Collection_Issue_Event cicie
            INNER JOIN #Event_Cnt ec
                  ON ec.Invoice_Collection_Queue_Id = cicie.Invoice_Collection_Queue_Id
      WHERE
            cicie.Row_Num BETWEEN @Start_Index AND @End_Index
      ORDER BY
            Row_Num 
                  
                  
      DROP TABLE #Invoice_Collection_Issue_Event,#Event_Cnt  
END;  
  
  
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Issue_Event_Sel_Invoice_Collection_Issue_Event_Id] TO [CBMSApplication]
GO
