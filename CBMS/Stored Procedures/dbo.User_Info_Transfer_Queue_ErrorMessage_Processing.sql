
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****************************************************************************************    
 Name:  dbo.User_Info_Transfer_Queue_ErrorMessage_Processing
     
 Description:    
  This Procedure is used to Re process the Switch Default Carbon Map Emission_CHS Queue messages .
			Note: Only DBA has to execute this
 Input Parameters:    
 Name     DataType  Default Description    
--------------------------------------------------------------------------------------    
  
 Output Parameters:    
 Name     Datatype  Default Description    
--------------------------------------------------------------------------------------   
   
 Usage Examples:  
--------------------------------------------------------------------------------------   
 EXEC dbo.User_Info_Transfer_Queue_ErrorMessage_Processing
   
 Author Initials:    
 Initials		Name  
--------------------------------------------------------------------------------------  
 DRG		    Dhilu Raichal George
 DSC			Kaushik
   
 Modifications :    
 Initials		Date		Modification    
--------------------------------------------------------------------------------------   
 DRG			4/20/2012	Created 
 DSC			05/28/2014	Added code to compare message_received_ts in poisontable and msg_monitor table
							and removed END Conversation @Conversation_Handle statment.
***********************************************************************************************/
CREATE PROCEDURE [dbo].[User_Info_Transfer_Queue_ErrorMessage_Processing]
AS 
BEGIN

	SET NOCOUNT ON ;

	DECLARE
		 @id1						INT = 1
	   , @id2						INT = 1
	   , @Conversation_Group_Id		UNIQUEIDENTIFIER
	   , @Conversation_Handle		UNIQUEIDENTIFIER
	   , @Old_Conversation_Handle	UNIQUEIDENTIFIER
	   , @Message_Body				XML
	   , @Message_Type				VARCHAR(255)
	   , @Status					VARCHAR(25) 
	   , @Completed_Status_Cd		VARCHAR(25) 
	   , @Error_Message_Ts			DATETIME


	CREATE TABLE #User_Info_Queue_ErrorMessage
	( 
		  Message_Id				INT IDENTITY(1, 1)
		, Conversation_Group_Id		UNIQUEIDENTIFIER
		, [Conversation_Handle]		UNIQUEIDENTIFIER
		, Message_Body				XML
		, Message_Sequence_Number	INT
		, Message_Type				VARCHAR(255) 
		, Message_Received_Ts		DATETIME
		, Queue_Name				VARCHAR(255)
	)
   	INSERT INTO #User_Info_Queue_ErrorMessage
	( 
	    Conversation_Group_Id
	  , Conversation_Handle
	  , MESSAGE_Body
	  , Message_Sequence_Number
	  , Message_Type 
	  , Message_Received_Ts 
	  , Queue_Name
	)
      SELECT
             [Conversation_Group_Id]
           , [Conversation_Handle]
           , convert(XML, MESSAGE_Body)
           , Message_Sequence_Number
           , Message_Type
           , Message_Received_Ts
           , Queue_Name
      FROM
            dbo.Service_Broker_Poison_Message
      WHERE
            Queue_Name = 'User_Info_Transfer_Queue'
            AND MESSAGE_Body IS NOT NULL
      ORDER BY
            Message_Received_Ts
           ,Conversation_Group_Id
           ,Conversation_Handle
           ,Message_Sequence_Number
           
	SELECT @id2 = @@ROWCOUNT  

	SELECT 
			@Error_Message_Ts = min(Message_Received_Ts) 
	FROM 
			#User_Info_Queue_ErrorMessage
           
    CREATE TABLE #Service_Broker_Message_Monitor 
    (
		  Message_Body			XML
		, Queue_name			VARCHAR(255)
		, Message_Received_Ts	DATETIME
		, Message_Status		VARCHAR(10)
    
    )
    INSERT INTO #Service_Broker_Message_Monitor
    (
		  Message_Body			
		, Queue_name			
		, Message_Received_Ts	
		, Message_Status		
	)
		SELECT 
			  CONVERT(xml,MESSAGE_Body) as message_body
			, Queue_Name 
			, Message_Received_Ts 
			, Message_Status 
		FROM
			dbo.Service_Broker_Message_Monitor msg_mon
		WHERE
			Queue_Name  = 'User_Info_Transfer_Queue'
			AND Message_Body IS NOT NULL
			AND Message_Status = @Completed_Status_Cd
			AND msg_mon.Message_Received_Ts >= @Error_Message_Ts

        
	WHILE (@id2 >= @id1 )
	BEGIN
	
		BEGIN TRY 
			
			BEGIN TRANSACTION
				
				SELECT
                      @Conversation_Group_Id = Conversation_Group_Id
                     ,@Old_Conversation_Handle = Conversation_Handle
                     ,@Message_Type = Message_Type
                     ,@Message_Body = MESSAGE_Body
                FROM
                    #User_Info_Queue_ErrorMessage
                WHERE
                    Message_Id = @id1	
                    

				
				IF ( @Message_Type = '//Change_Control/Message/User_Info_Transfer' ) 
				BEGIN
					IF NOT EXISTS (	SELECT 1
									FROM 
										#User_Info_Queue_ErrorMessage  queue_em
										INNER JOIN #Service_Broker_Message_Monitor msg_monitor
												ON msg_monitor.Queue_Name  = queue_em.Queue_Name 
									WHERE
										Message_Id = @id1	
										AND msg_monitor.Message_Received_Ts > queue_em.Message_Received_Ts 
										AND queue_em.MESSAGE_Body.value('(/User_Info_Changes/User_Info_Change/USER_INFO_ID/node())[1]', 'INT')  = 
											msg_monitor.MESSAGE_Body.value('(/User_Info_Changes/User_Info_Change/USER_INFO_ID/node())[1]', 'INT')
									)
						BEGIN 
							 SET @Conversation_Handle = NULL ;
							;BEGIN DIALOG CONVERSATION @Conversation_Handle --begin new conversation
								FROM SERVICE [//Change_Control/Service/CBMS/User_Info_Transfer]
								TO SERVICE '//Change_Control/Service/CBMS/User_Info_Transfer'
								ON CONTRACT [//Change_Control/Contract/User_Info_Transfer] ;
						
								;SEND ON CONVERSATION @Conversation_Handle 
									MESSAGE TYPE [//Change_Control/Message/User_Info_Transfer] (@Message_Body) ;
								;END CONVERSATION @Conversation_Handle	
							END	
						END
						
			
				IF(	@Message_Type = '//Change_Control/Message/User_PassCode_Transfer')
				BEGIN 
					IF NOT EXISTS (	SELECT 1
									FROM 
										#User_Info_Queue_ErrorMessage  queue_em
										INNER JOIN #Service_Broker_Message_Monitor msg_monitor
												ON msg_monitor.Queue_Name  = queue_em.Queue_Name 
									WHERE
										Message_Id = @id1	
										AND msg_monitor.Message_Received_Ts > queue_em.Message_Received_Ts 
										AND queue_em.MESSAGE_Body.value('(/User_PassCode_Changes/User_PassCode_Change/User_PassCode_Id/node())[1]', 'INT')  = 
											msg_monitor.MESSAGE_Body.value('(/User_PassCode_Changes/User_PassCode_Change/User_PassCode_Id/node())[1]', 'INT')
										AND queue_em.MESSAGE_Body.value('(/User_PassCode_Changes/User_PassCode_Change/User_Info_Id/node())[1]', 'INT')  = 
											msg_monitor.MESSAGE_Body.value('(/User_PassCode_Changes/User_PassCode_Change/User_Info_Id/node())[1]', 'INT')
									)
						BEGIN 
							 SET @Conversation_Handle = NULL ;
							;BEGIN DIALOG CONVERSATION @Conversation_Handle --begin new conversation
								FROM SERVICE [//Change_Control/Service/CBMS/User_Info_Transfer]
								TO SERVICE '//Change_Control/Service/CBMS/User_Info_Transfer'
								ON CONTRACT [//Change_Control/Contract/User_Info_Transfer] ;
						
								;SEND ON CONVERSATION @Conversation_Handle 
									MESSAGE TYPE [//Change_Control/Message/User_PassCode_Transfer] (@Message_Body) ;
								;END CONVERSATION @Conversation_Handle	
							END	
						END	
							

						
				SET @id1 = @id1 + 1;
				
				-- Delete error message from poison table
				DELETE 
				FROM 
					dbo.Service_Broker_Poison_Message
				WHERE 
					Queue_Name = 'User_Info_Transfer_Queue'
					AND Conversation_Group_Id = @Conversation_Group_Id
					AND Conversation_Handle = @Old_Conversation_Handle;
						
		COMMIT TRANSACTION
			
		END TRY
		BEGIN CATCH
			DECLARE @errormessage NVARCHAR(4000) = ERROR_MESSAGE()
			IF @@TRANCOUNT > 1
				ROLLBACK TRANSACTION
			RAISERROR(60001, 16, 1, 'SP:User_Info_Transfer_Queue_ErrorMessage_Processing', @errormessage)
		END CATCH	
	END
END



;




;
GO
