SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
                         
 NAME: dbo.RM_Group_Sel_By_Client_Id                     
                          
 DESCRIPTION:      
		  To get the manage Risk Management Groups.

 INPUT PARAMETERS:      
                         
 Name                               DataType          Default       Description      
-------------------------------------------------------------------------------------
 @Client_Id							INT        
                          
 OUTPUT PARAMETERS:      
                               
 Name                               DataType          Default       Description      
-------------------------------------------------------------------------------------
                          
 USAGE EXAMPLES:                              
-------------------------------------------------------------------------------------
          
		  EXEC  dbo.RM_Group_Sel_By_Client_Id 235,1,10

		  EXEC  dbo.RM_Group_Sel_By_Client_Id 1005

		    EXEC  dbo.RM_Group_Sel_By_Client_Id_1 1005


		  EXEC  dbo.RM_Group_Sel_By_Client_Id 12240

		EXEC dbo.CODE_SEL_BY_CodeSet_Name @CodeSet_Name = 'Risk Management Groups';
        EXEC dbo.CODE_SEL_BY_CodeSet_Name @CodeSet_Name = 'RM Group Participants';
	 
                         
 AUTHOR INITIALS:    
       
 Initials                   Name      
-------------------------------------------------------------------------------------
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
-------------------------------------------------------------------------------------
 NR                     2018-07-19      Created for GRM-51.                        
                         
******/


CREATE PROCEDURE [dbo].[RM_Group_Sel_By_Client_Id]
    (
        @Client_Id INT
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
        , @Total_Count INT = 0
    )
AS
    BEGIN

        SET NOCOUNT ON;



        DECLARE
            @Groups_of_Contracts_Cd INT
            , @RM_Manual_Group_Cd INT
            , @RM_Smart_Group_Cd INT
            , @Module_Cd INT;


        CREATE TABLE #RM_Groups
             (
                 RM_GROUP_ID INT
                 , GROUP_NAME VARCHAR(100)
                 , Country_Name VARCHAR(MAX)
                 , Commodity_Name VARCHAR(100)
                 , RM_Group_Type_Cd INT
                 , RM_Group_Type VARCHAR(100)
                 , Last_Updated_By VARCHAR(100)
                 , Last_Change_Ts DATETIME
                 , Sites_Cnt INT
             );

        SELECT
            @Groups_of_Contracts_Cd = MAX(CASE WHEN c.Code_Value = 'Contract' THEN c.Code_Id
                                          END)
            , @RM_Manual_Group_Cd = MAX(CASE WHEN c.Code_Value = 'RM Manual Group' THEN c.Code_Id
                                        END)
            , @RM_Smart_Group_Cd = MAX(CASE WHEN c.Code_Value = 'RM Smart Group' THEN c.Code_Id
                                       END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Risk Management Groups'
            AND cs.Std_Column_Name = 'RM_Group_Type_Cd'
            AND c.Code_Value IN ( 'Contract', 'RM Manual Group', 'RM Smart Group' );

        SELECT
            @Module_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Application Module'
            AND c.Code_Value = 'CBMSRiskManagement';





        --> RM_Manual_Group 

        INSERT INTO #RM_Groups
             (
                 RM_GROUP_ID
                 , GROUP_NAME
                 , Country_Name
                 , Commodity_Name
                 , RM_Group_Type_Cd
                 , RM_Group_Type
                 , Last_Updated_By
                 , Last_Change_Ts
                 , Sites_Cnt
             )
        SELECT
            rg.Cbms_Sitegroup_Id AS RM_GROUP_ID
            , rg.Group_Name
            , CASE WHEN LEN(cht.Country_Name) > 1 THEN LEFT(cht.Country_Name, LEN(cht.Country_Name) - 1)
              END AS Country_Name
            , c.Commodity_Name
            , cd.Code_Id AS RM_Group_Type_Cd
            , cd.Code_Value AS RM_Group_Type
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Last_Updated_By
            , rg.Last_Change_Ts
            , COUNT(DISTINCT ch.Site_Id)
        FROM
            dbo.Cbms_Sitegroup rg
            CROSS APPLY (   SELECT
                                ch.Country_Name + ', '
                            FROM
                                dbo.Cbms_Sitegroup_Participant rgd
                                INNER JOIN Core.Client_Hier ch
                                    ON ch.Site_Id = rgd.Group_Participant_Id
                            WHERE
                                rgd.Cbms_Sitegroup_Id = rg.Cbms_Sitegroup_Id
                                AND ch.Client_Id = @Client_Id
                            GROUP BY
                                ch.Country_Name
                            FOR XML PATH('')) cht(Country_Name)
            LEFT JOIN dbo.Commodity c
                ON c.Commodity_Id = rg.Commodity_Id
            INNER JOIN dbo.Code cd
                ON cd.Code_Id = rg.Group_Type_Cd
            LEFT JOIN dbo.USER_INFO ui
                ON ui.USER_INFO_ID = rg.Updated_User_Id
            LEFT JOIN(dbo.Cbms_Sitegroup_Participant rgd
                      INNER JOIN Core.Client_Hier ch
                          ON ch.Site_Id = rgd.Group_Participant_Id)
                ON rgd.Cbms_Sitegroup_Id = rg.Cbms_Sitegroup_Id
                   AND  ch.Client_Id = @Client_Id
        WHERE
            rg.Client_Id = @Client_Id
            AND rg.Module_Cd = @Module_Cd
            AND rg.Group_Type_Cd = @RM_Manual_Group_Cd
        GROUP BY
            rg.Cbms_Sitegroup_Id
            , rg.Group_Name
            , CASE WHEN LEN(cht.Country_Name) > 1 THEN LEFT(cht.Country_Name, LEN(cht.Country_Name) - 1)
              END
            , c.Commodity_Name
            , cd.Code_Id
            , cd.Code_Value
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME
            , rg.Last_Change_Ts;








        --RM_Smart_Group

        INSERT INTO #RM_Groups
             (
                 RM_GROUP_ID
                 , GROUP_NAME
                 , Country_Name
                 , Commodity_Name
                 , RM_Group_Type_Cd
                 , RM_Group_Type
                 , Last_Updated_By
                 , Last_Change_Ts
                 , Sites_Cnt
             )
        SELECT
            rg.Cbms_Sitegroup_Id RM_GROUP_ID
            , rg.Group_Name
            , CASE WHEN LEN(cht.Country_Name) > 1 THEN LEFT(cht.Country_Name, LEN(cht.Country_Name) - 1)
              END AS Country_Name
            , C.Commodity_Name
            , cd.Code_Id AS RM_Group_Type_Cd
            , cd.Code_Value AS RM_Group_Type
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Last_Updated_By
            , rg.Last_Change_Ts
            , COUNT(DISTINCT ch.Site_Id)
        FROM
            dbo.Cbms_Sitegroup rg
            CROSS APPLY (   SELECT
                                ch.Country_Name + ', '
                            FROM
                                dbo.Cbms_Sitegroup_Participant rgd
                                INNER JOIN Core.Client_Hier ch
                                    ON rgd.Group_Participant_Id = ch.Site_Id
                            WHERE
                                rgd.Cbms_Sitegroup_Id = rg.Cbms_Sitegroup_Id
                                AND ch.Client_Id = @Client_Id
                            GROUP BY
                                ch.Country_Name
                            FOR XML PATH('')) cht(Country_Name)
            LEFT JOIN dbo.Commodity C
                ON C.Commodity_Id = rg.Commodity_Id
            INNER JOIN dbo.Code cd
                ON cd.Code_Id = rg.Group_Type_Cd
            LEFT JOIN dbo.USER_INFO ui
                ON ui.USER_INFO_ID = rg.Updated_User_Id
            LEFT JOIN(dbo.Cbms_Sitegroup_Participant rgd
                      INNER JOIN Core.Client_Hier ch
                          ON rgd.Group_Participant_Id = ch.Site_Id)
                ON rgd.Cbms_Sitegroup_Id = rg.Cbms_Sitegroup_Id
                   AND  ch.Client_Id = @Client_Id
        WHERE
            rg.Client_Id = @Client_Id
            AND rg.Module_Cd = @Module_Cd
            AND rg.Group_Type_Cd = @RM_Smart_Group_Cd
        GROUP BY
            rg.Cbms_Sitegroup_Id
            , rg.Group_Name
            , CASE WHEN LEN(cht.Country_Name) > 1 THEN LEFT(cht.Country_Name, LEN(cht.Country_Name) - 1)
              END
            , cd.Code_Id
            , cd.Code_Value
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME
            , rg.Last_Change_Ts
            , C.Commodity_Name;

        --Contract

        INSERT INTO #RM_Groups
             (
                 RM_GROUP_ID
                 , GROUP_NAME
                 , Country_Name
                 , Commodity_Name
                 , RM_Group_Type_Cd
                 , RM_Group_Type
                 , Last_Updated_By
                 , Last_Change_Ts
                 , Sites_Cnt
             )
        SELECT
            rg.Cbms_Sitegroup_Id RM_GROUP_ID
            , rg.Group_Name
            , CASE WHEN LEN(cht.Country_Name) > 1 THEN LEFT(cht.Country_Name, LEN(cht.Country_Name) - 1)
              END
            , c.Commodity_Name
            , cd.Code_Id AS RM_Group_Type_Cd
            , cd.Code_Value AS RM_Group_Type
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Last_Updated_By
            , rg.Last_Change_Ts
            , COUNT(DISTINCT ch.Site_Id)
        FROM
            dbo.Cbms_Sitegroup rg
            CROSS APPLY (   SELECT
                                ch.Country_Name + ', '
                            FROM
                                dbo.Cbms_Sitegroup_Participant rgd
                                INNER JOIN Core.Client_Hier_Account cha
                                    ON cha.Supplier_Contract_ID = rgd.Group_Participant_Id
                                INNER JOIN Core.Client_Hier ch
                                    ON ch.Client_Hier_Id = cha.Client_Hier_Id
                            WHERE
                                rgd.Cbms_Sitegroup_Id = rg.Cbms_Sitegroup_Id
                                AND ch.Client_Id = @Client_Id
                            GROUP BY
                                ch.Country_Name
                            FOR XML PATH('')) cht(Country_Name)
            LEFT JOIN dbo.Commodity c
                ON c.Commodity_Id = rg.Commodity_Id
            INNER JOIN dbo.Code cd
                ON cd.Code_Id = rg.Group_Type_Cd
            LEFT JOIN dbo.USER_INFO ui
                ON ui.USER_INFO_ID = rg.Updated_User_Id
            LEFT JOIN(dbo.Cbms_Sitegroup_Participant rgd
                      INNER JOIN Core.Client_Hier_Account cha
                          ON cha.Supplier_Contract_ID = rgd.Group_Participant_Id
                      INNER JOIN Core.Client_Hier ch
                          ON ch.Client_Hier_Id = cha.Client_Hier_Id)
                ON rgd.Cbms_Sitegroup_Id = rg.Cbms_Sitegroup_Id
                   AND  ch.Client_Id = @Client_Id
        WHERE
            rg.Client_Id = @Client_Id
            AND rg.Module_Cd = @Module_Cd
            AND rg.Group_Type_Cd = @Groups_of_Contracts_Cd
        GROUP BY
            rg.Cbms_Sitegroup_Id
            , rg.Group_Name
            , CASE WHEN LEN(cht.Country_Name) > 1 THEN LEFT(cht.Country_Name, LEN(cht.Country_Name) - 1)
              END
            , c.Commodity_Name
            , cd.Code_Id
            , cd.Code_Value
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME
            , rg.Last_Change_Ts;


        IF @Total_Count = 0
            BEGIN

                SELECT  @Total_Count = COUNT(1)FROM #RM_Groups rgd;
            END;




        WITH CTE_Rm_Dtl
        AS (
               SELECT   TOP (@End_Index)
                        rdg.RM_GROUP_ID
                        , rdg.GROUP_NAME
                        , rdg.Country_Name
                        , rdg.Commodity_Name
                        , rdg.RM_Group_Type_Cd
                        , rdg.RM_Group_Type
                        , rdg.Last_Updated_By
                        , rdg.Last_Change_Ts
                        , rdg.Sites_Cnt
                        , ROW_NUMBER() OVER (ORDER BY
                                                 rdg.Last_Change_Ts DESC) AS Row_Num
               FROM
                    #RM_Groups rdg
               GROUP BY
                   rdg.RM_GROUP_ID
                   , rdg.GROUP_NAME
                   , rdg.Country_Name
                   , rdg.Commodity_Name
                   , rdg.RM_Group_Type_Cd
                   , rdg.RM_Group_Type
                   , rdg.Last_Updated_By
                   , rdg.Last_Change_Ts
                   , rdg.Sites_Cnt
           )
        SELECT
            rgd.RM_GROUP_ID
            , rgd.GROUP_NAME
            , rgd.Country_Name
            , rgd.Commodity_Name
            , rgd.RM_Group_Type_Cd
            , rgd.RM_Group_Type
            , rgd.Last_Updated_By
            , rgd.Last_Change_Ts
            , rgd.Row_Num
            , @Total_Count AS Total_Count
            --, CAST(100 AS INT) Sites_Cnt
            , rgd.Sites_Cnt
        FROM
            CTE_Rm_Dtl rgd
        WHERE
            rgd.Row_Num BETWEEN @Start_Index
                        AND     @End_Index
        ORDER BY
            rgd.Row_Num;
    END;











GO
GRANT EXECUTE ON  [dbo].[RM_Group_Sel_By_Client_Id] TO [CBMSApplication]
GO
