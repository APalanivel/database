SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--select * from division where not_managed = 1 and client_id = 1005
--select * from division where   client_id = 140
--select * from client where client_id = 140
--select * from vwsitename where client_id=140
--select * from site where client_id=140
--exec GET_DIVISIONS_UNDER_CLIENT_P -1 ,-1 ,140

CREATE     PROCEDURE dbo.GET_DIVISIONS_UNDER_CLIENT_P 
@userId varchar,
@sessionId varchar,
@clientId int
as
	set nocount on
	select	DIVISION_ID,
		DIVISION_NAME
	
	from	DIVISION 
	
	where 	CLIENT_ID=@clientId
		--and NOT_MANAGED = 0 
	
	order by DIVISION_NAME
GO
GRANT EXECUTE ON  [dbo].[GET_DIVISIONS_UNDER_CLIENT_P] TO [CBMSApplication]
GO
