SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********
NAME:  dbo.Holiday_Master_Sel_By_TOU_Schedule_Term_Id

DESCRIPTION:  
Used to select Holiday Names by given term.

INPUT PARAMETERS:
      Name							DataType          Default     Description
------------------------------------------------------------
	  @Time_Of_Use_Schedule_Term_Id INT
	  

OUTPUT PARAMETERS:
      Name							DataType          Default     Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------

	Exec dbo.Holiday_Master_Sel_By_TOU_Schedule_Term_Id 14


AUTHOR INITIALS:
	Initials Name
------------------------------------------------------------
	BCH		 Balaraju


	Initials Date		 Modification
------------------------------------------------------------
	BCH		 2012-07-17  Created
	
*************/
CREATE PROCEDURE dbo.Holiday_Master_Sel_By_TOU_Schedule_Term_Id
( 
 @Time_Of_Use_Schedule_Term_Id INT )
AS 
BEGIN
      SET NOCOUNT ON ;
      SELECT
            hm.Holiday_Name
      FROM
            dbo.Holiday_Master hm
            JOIN dbo.Time_Of_Use_Schedule_Term_Holiday trmhl
                  ON hm.Holiday_Master_Id = trmhl.Holiday_Master_Id
      WHERE
            trmhl.Time_Of_Use_Schedule_Term_Id = @Time_Of_Use_Schedule_Term_Id
      GROUP BY
            hm.Holiday_Name

END

;
GO
GRANT EXECUTE ON  [dbo].[Holiday_Master_Sel_By_TOU_Schedule_Term_Id] TO [CBMSApplication]
GO
