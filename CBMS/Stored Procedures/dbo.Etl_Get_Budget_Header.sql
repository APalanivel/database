SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	cmbs.dbo.Etl_Get_Budget_Header


DESCRIPTION:
	Gets all changes made between the MinDBTS and the MaxDBTS. 
	This procedure is stricly used for ETL applications 


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MinDBTS		bigint					minimum row version 
	@MaxDBTS		bigint        	        max row version   	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
EXEC dbo.Etl_Get_Budget_Header 1, 100

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CMH			Chad Hattabaugh

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	CMH			06/08/2009	Created        	
******/
CREATE PROCEDURE [dbo].[Etl_Get_Budget_Header]  
(    
	@MinDBTS        BIGINT    
	,@MaxDBTS       BIGINT    
)    
AS     
BEGIN     
    SET NOCOUNT ON    
        
 SELECT    
	b.CLIENT_ID    
	,b.BUDGET_ID    
	,B.Budget_Name    
	,convert(DateTime, convert(char(4), B.Budget_Start_Year) + '-01-01') AS Budget_Start_Year
	,b.IS_POSTED_TO_DV   
	,CASE WHEN b.COMMODITY_TYPE_ID = 290 THEN 12 else 25 End as Base_UOM_Id   
 FROM 
	Budget b (NOLOCK)    
 WHERE  
	B.ROW_VERSION between @MinDBTS and @MaxDBTS    
   
END    
 
 
 

 

  
  
  
  
  
  
  
  
   
  
GO
GRANT EXECUTE ON  [dbo].[Etl_Get_Budget_Header] TO [CBMSApplication]
GO
