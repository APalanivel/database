SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********     
NAME:    
	dbo.CBMS_UPDATE_VENDOR_INFO_P    

DESCRIPTION:  This procedure used to update the records in VENDOR table

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------------    
@vendor_name            varchar(200),
@vendor_id              int
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    

    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
-- exec [dbo].[CBMS_UPDATE_VENDOR_INFO_P] 
-- @vendor_name ='Adam Electric Cooperative, Inc. ', -- Original value = 'Adams Electric Cooperative, Inc.',
-- @vendor_id = 1


Initials Name    
------------------------------------------------------------    
DR       Deana Ritter

Initials Date  Modification    
------------------------------------------------------------    
DR    8/4/2009	   Removed Linked Server Updates
 DMR		  09/10/2010 Modified for Quoted_Identifier

          
******/
CREATE PROCEDURE [dbo].[CBMS_UPDATE_VENDOR_INFO_P] 
@vendor_name varchar(200),
@vendor_id int

AS


UPDATE 
	VENDOR 
SET 
	VENDOR_NAME = @vendor_name 
WHERE 
	VENDOR_ID = @vendor_id
GO
GRANT EXECUTE ON  [dbo].[CBMS_UPDATE_VENDOR_INFO_P] TO [CBMSApplication]
GO
