SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.GET_DEAL_TICKET_COUNTER_PARTY_P
	@rm_deal_ticket_id INT

AS
BEGIN

	SET NOCOUNT ON

	SELECT DISTINCT
		rmcp.rm_counterparty_id , rmcp.counterparty_name
	FROM dbo.rm_counterparty rmcp INNER JOIN dbo.rm_deal_ticket_counter_party rmdtcp ON rmdtcp.rm_counterparty_id = rmcp.rm_counterparty_id
	WHERE rmdtcp.rm_deal_ticket_id = @rm_deal_ticket_id
	ORDER BY rmcp.counterparty_name

END
GO
GRANT EXECUTE ON  [dbo].[GET_DEAL_TICKET_COUNTER_PARTY_P] TO [CBMSApplication]
GO
