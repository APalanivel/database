SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:                  
     dbo.Account_Invoice_Watch_List_Sel_By_Account_Group_Id                  
                  
Description:                  
			Getting Watch List - Based on Account Group Id.                  
                  
 Input Parameters:                  
 Name                                        DataType        Default        Description                    
--------------------------------------------------------------------------------------------------                   
 @Account_Group_Id  								INT

                  
 Output Parameters:                        
 Name                                        DataType        Default        Description                    
--------------------------------------------------------------------------------------------------                   
                  
 Usage Examples:                      
--------------------------------------------------------------------------------------------------


EXEC dbo.Account_Invoice_Watch_List_Sel_By_Account_Group_Id
    @Account_Group_Id = 22651
   
  

   Select
    *
from
    Account_Invoice_Processing_Config
where
    account_Id in ( 1726, 960780, 983902 )
	and Watch_List_Group_Info_Id IS NOT NULL

       Select * from account where account_id =22651

   select * from core.client_hier_Account where account_group_id=22651
              
 Author Initials:                  
  Initials        Name                  
--------------------------------------------------------------------------------------------------                   
  SP              Srinivas Patchava
                   
 Modifications:                  
  Initials        Date              Modification                  
--------------------------------------------------------------------------------------------------                   
  SP              2019-07-12		Created for SE2017-758              
                 
******/
CREATE PROCEDURE [dbo].[Account_Invoice_Watch_List_Sel_By_Account_Group_Id]
    (
        @Account_Group_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;


        WITH Cte_Watch_List
        AS (
               SELECT
                    aipc.Account_Id
                    , aipc.Config_Start_Dt
                    , aipc.Config_End_Dt
                    , aipc.Watch_List_Group_Info_Id
                    , ROW_NUMBER() OVER (ORDER BY
                                             aipc.Config_Start_Dt DESC) AS Row_Num
               FROM
                    dbo.Account_Invoice_Processing_Config aipc
                    INNER JOIN Core.Client_Hier_Account cha
                        ON cha.Account_Id = aipc.Account_Id
               WHERE
                    cha.Account_Group_ID = @Account_Group_Id
                    AND aipc.Watch_List_Group_Info_Id IS NOT NULL
                    AND aipc.Is_Active = 1
               GROUP BY
                   aipc.Account_Id
                   , aipc.Config_Start_Dt
                   , aipc.Config_End_Dt
                   , aipc.Watch_List_Group_Info_Id
           )
        SELECT
            cw.Account_Id
            , cw.Config_Start_Dt
            , cw.Config_End_Dt
            , cw.Watch_List_Group_Info_Id
        FROM
            Cte_Watch_List cw
        WHERE
            cw.Row_Num = 1;
    END;

GO
GRANT EXECUTE ON  [dbo].[Account_Invoice_Watch_List_Sel_By_Account_Group_Id] TO [CBMSApplication]
GO
