SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Invoice_Collection_Contact_Info_By_Invoice_Collection_Account_Config       
              
Description:              
			This sproc to get the Contact Information of Accounts.      
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
@Invoice_Collection_Account_Config_Id		 Varchar
    
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

   Exec dbo.Invoice_Collection_Contact_Info_By_Invoice_Collection_Account_Config 2
   
  
   
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2016-12-29		Created For Invoice_Collection.         
             
******/ 
CREATE PROCEDURE [dbo].[Invoice_Collection_Contact_Info_By_Invoice_Collection_Account_Config]
      ( 
       @Invoice_Collection_Account_Config_Id VARCHAR(MAX) 
       )
AS 
BEGIN
      SET NOCOUNT ON 
       
SELECT
      cci.Code_Value Invoice_Source
     ,ci.First_Name + ' ' + ci.Last_Name Contact_Name
     ,ci.Job_Position
     ,ci.Location
     ,ci.Email_Address
     ,ci.Phone_Number
     ,ci.Fax_Number
     ,ui.FIRST_NAME + ' ' + ui.LAST_NAME  Invoice_Collection_Officer
     ,icaci.Is_Primary
     ,ui.EMAIL_ADDRESS AS Invoice_Collection_Officer_Email_Address
     ,icaci.Is_Include_In_CC
FROM
      dbo.Invoice_Collection_Account_Config icac
      INNER JOIN dbo.Invoice_Collection_Account_Contact icaci
            ON icac.Invoice_Collection_Account_Config_Id = icaci.Invoice_Collection_Account_Config_Id
      INNER JOIN dbo.Contact_Info ci
            ON icaci.Contact_Info_Id = ci.Contact_Info_Id
      INNER JOIN dbo.USER_INFO ui
      ON ui.USER_INFO_ID = icac.Invoice_Collection_Officer_User_Id      
      LEFT OUTER JOIN dbo.Code cci
            ON cci.Code_Id = ci.Contact_Level_Cd
WHERE
      EXISTS ( SELECT
                        1
                     FROM
                        dbo.ufn_split(@Invoice_Collection_Account_Config_Id, ',') ufn
                     WHERE
                        ufn.Segments = icac.Invoice_Collection_Account_Config_Id )
     
           
			     
      
END;

;


;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Contact_Info_By_Invoice_Collection_Account_Config] TO [CBMSApplication]
GO
