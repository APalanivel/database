SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        dbo.RM_Deal_Ticket_Client_Currency_Sel                      
                        
Description:                        
        To get site's hedge configurations  
                        
Input Parameters:                        
    Name    DataType        Default     Description                          
--------------------------------------------------------------------------------  
 @Client_Id   INT  
    @Commodity_Id  INT  
    @Hedge_Type  INT  
    @Start_Dt DATE  
    @End_Dt  DATE  
    @Contract_Id  VARCHAR(MAX)  
    @Site_Id   INT    NULL  
                        
 Output Parameters:                              
 Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
     
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
  
 EXEC dbo.RM_Deal_Ticket_Client_Currency_Sel  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2017-01-01',@End_Dt='2017-01-01',@Hedge_Type=586,@Contract_Id = '162277,162278'  
 EXEC dbo.RM_Deal_Ticket_Client_Currency_Sel  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2017-05-01',@End_Dt='2017-05-01',@Hedge_Type=586,@Site_Str = '41270,41241'  
  
 EXEC dbo.RM_Deal_Ticket_Client_Currency_Sel  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2019-01-01',@End_Dt='2019-02-01',@Hedge_Type=586,@Contract_Id = '148361'  
 EXEC dbo.RM_Deal_Ticket_Client_Currency_Sel  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2019-01-01',@End_Dt='2019-02-01',@Hedge_Type=586,@Site_Str = '24401,24415'  
    
 EXEC dbo.RM_Deal_Ticket_Client_Currency_Sel  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2019-01-01',@End_Dt='2019-01-01',@Hedge_Type=586,@Site_Str = '24401,24415',@RM_Group_Id='730',@Division_Id='1928'  
    
 EXEC dbo.RM_Deal_Ticket_Client_Currency_Sel  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2017-01-01',@End_Dt='2019-02-01',@Hedge_Type=586,@Contract_Id = '148361,162278'  
    
  
Author Initials:                        
    Initials    Name                        
--------------------------------------------------------------------------------  
    RR          Raghu Reddy     
                         
 Modifications:                        
    Initials Date           Modification                        
--------------------------------------------------------------------------------  
 PR          02-11-2018    Created GRM  
                       
******/
CREATE PROCEDURE [dbo].[RM_Deal_Ticket_Client_Currency_Sel]
      ( 
       @Client_Id INT = NULL
      ,@Commodity_Id INT = NULL
      ,@Hedge_Type INT = NULL
      ,@Start_Dt DATE = NULL
      ,@End_Dt DATE = NULL
      ,@Contract_Id VARCHAR(MAX) = NULL
      ,@Uom_Id INT = NULL
      ,@Site_Str VARCHAR(MAX) = NULL
      ,@Division_Id VARCHAR(MAX) = NULL
      ,@RM_Group_Id VARCHAR(MAX) = NULL )
AS 
BEGIN
      SET NOCOUNT ON;

      SELECT
            cu.CURRENCY_UNIT_ID
           ,cu.CURRENCY_UNIT_NAME
           ,CASE WHEN cu.CURRENCY_UNIT_NAME = 'USD' THEN 1
                 ELSE 0
            END AS Default_Currency
      FROM
            dbo.CURRENCY_UNIT cu
      GROUP BY
            cu.CURRENCY_UNIT_ID
           ,cu.CURRENCY_UNIT_NAME;


END;

GO
GRANT EXECUTE ON  [dbo].[RM_Deal_Ticket_Client_Currency_Sel] TO [CBMSApplication]
GO
