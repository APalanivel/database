SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                          
NAME:   dbo.Report_DE_RM_details              
                         
DESCRIPTION:                           
   Risk Management Report              
                         
INPUT PARAMETERS:                          
Name       DataType  Default  Description                
------------------------------------------------------------                
 @ClientId      INT              
 @divisionId     INT     0              
 @GroupId      INT     0              
 @StartDate      VARCHAR(12)              
 @EndDate      VARCHAR(12)              
 @unitId      INT              
 @currencyUnit     INT              
              
 OUTPUT PARAMETERS:                          
 Name      DataType  Default  Description                          
------------------------------------------------------------                         
       select * from client where client_name like '%patheon%'         
                       
 USAGE EXAMPLES:                          
------------------------------------------------------------                
              
dbo.[Get_RM_Data]              
     @ClientId = 10648                       
                  
 AUTHOR INITIALS:                          
 Initials Name                          
-----------------------------------------------------------------------------------------------------------------
	KVK		Vinay K           
                         
 MODIFICATIONS:                           
 Initials  Date   Modification                          
-----------------------------------------------------------------------------------------------------------------                               
	KVK		8/1/2017	Created, its copy of an existing procedure (CBMS.dbo.Report_DE_RM_details)
						this procedure requires some optimization.
	KVK		09/26/2017	Modified to check the site exists in RM_ONBOARD_HEDGE_SETUP table with INCLUDE_IN_REPORTS as 1
	KVK		08/31/2018	Modified to refer Date_Dim from view 
******/

CREATE PROCEDURE [dbo].[Get_RM_Data] ( @ClientId INT )
AS
BEGIN
      SET NOCOUNT ON;
      DECLARE
            @StartDate DATE = convert(VARCHAR(10), year(getdate()) - 2) + '-01-01' --2 years back
          , @EndDate DATE = convert(VARCHAR(10), year(getdate()) + 3) + '-12-01';  --3 years ahead

      DECLARE
            @unitId INT = 25
          , @currencyUnit INT = 3;

      DECLARE @Commodity_UOM_Base_Key_Id INT;

      SELECT
            @Commodity_UOM_Base_Key_Id = Commodity_UOM_Base_Key_Id
      FROM
            POC_BITool_Qlik2.dbo.Commodity_UOM_Base_Key
      WHERE
            Commodity_Id = 291
            AND Base_Commodity_UOM_Id = @unitId;


      DECLARE
            @StartYear INT
          , @EndYear INT
          , @Client_Currency_Group_Id INT
          , @Max_FORECAST_AS_OF_DATE DATETIME
          , @Min_FORECAST_AS_OF_DATE DATETIME
          , @Currency VARCHAR(10)
          , @frequency_type VARCHAR(7) = 'Monthly';



      CREATE TABLE #Forecast
            ( Month_Identifier DATETIME NULL
            , SiteId INT NULL
            , Forecasted_Usage DECIMAL(20, 3) NULL );

      CREATE TABLE #Hedge_Volume
            ( Month_Identifier DATETIME NULL
            , SiteId INT NULL
            , Hedge_Volume DECIMAL(20, 3) NULL
            , Hedge_Price DECIMAL(8, 3) NULL
            , Hedge_Type VARCHAR(12) NULL );

      CREATE TABLE #BudgetWACOGPRICE
            ( Volume DECIMAL(32, 16) NULL
            , PriceVolume DECIMAL(32, 16) NULL
            , Month_Identifier DATETIME NULL
            , SiteId INT NULL );

      CREATE TABLE #RM_Budget
            ( BudgetWACOGPRICE DECIMAL(8, 3) NULL
            , Month_Identifier DATETIME NULL
            , SiteId INT NULL
            , Hedge_Type VARCHAR(12) NULL );

      CREATE TABLE #Sites
            ( SiteId INT
            , Hedge_Type VARCHAR(12) NULL );

      CREATE TABLE #Hedge
            ( RM_DEAL_TICKET_ID INT NULL
            , SITE_ID INT NULL
            , HEDGE_PRICE DECIMAL(8, 3) NULL
            , PREMIUM DECIMAL(8, 3) NULL
            , MONTH_IDENTIFIER VARCHAR(12) NULL
            , OPTION_NAME VARCHAR(200) NULL
            , HEDGE_TYPE VARCHAR(12) NULL
            , MAX_HEDGE_PERCENT DECIMAL(8, 3) NULL
            , CONVERSION_FACTOR DECIMAL(10, 6) NULL
            , VOLUME_CONVERSION_FACTOR DECIMAL(10, 6) NULL
            , CURRENCY_TYPE_ID INT NULL
            , HEDGE_VOLUME DECIMAL(20, 2) NULL );

      CREATE TABLE #Hedge_with_Type
            ( RM_DEAL_TICKET_ID INT NULL
            , SITE_ID INT NULL
            , HEDGE_PRICE DECIMAL(8, 3) NULL
            , PREMIUM DECIMAL(8, 3) NULL
            , MONTH_IDENTIFIER VARCHAR(12) NULL
            , OPTION_NAME VARCHAR(200) NULL
            , HEDGE_TYPE VARCHAR(12) NULL
            , MAX_HEDGE_PERCENT DECIMAL(8, 3) NULL
            , CONVERSION_FACTOR DECIMAL(10, 6) NULL
            , VOLUME_CONVERSION_FACTOR DECIMAL(10, 6) NULL
            , CURRENCY_TYPE_ID INT NULL
            , HEDGE_VOLUME DECIMAL(20, 2) NULL );

      CREATE TABLE #Contract
            ( Month_Identifier DATETIME NULL
            , SiteId INT NULL
            , ContractMeterVolume DECIMAL(20, 3) NULL );
      SET @StartYear = year(@StartDate);
      SET @EndYear = year(@EndDate);

      SELECT
            @Max_FORECAST_AS_OF_DATE = max(FORECAST_AS_OF_DATE)
      FROM
            RM_FORECAST_VOLUME
      WHERE
            FORECAST_YEAR = @StartYear
            AND CLIENT_ID = @ClientId;

      SELECT
            @Min_FORECAST_AS_OF_DATE = max(FORECAST_AS_OF_DATE)
      FROM
            RM_FORECAST_VOLUME
      WHERE
            FORECAST_YEAR = @EndYear
            AND CLIENT_ID = @ClientId;

      SELECT
            @Currency = SYMBOL
      FROM
            CURRENCY_UNIT
      WHERE
            CURRENCY_UNIT_ID = @currencyUnit;

      INSERT INTO #BudgetWACOGPRICE
      EXEC GET_DEAL_CALCULATOR_BUDGET_WACOG_FOR_SITES_P
            @clientId = @ClientId
          , @startDate = @StartDate
          , @endDate = @EndDate
          , @startYear = @StartYear
          , @endYear = @EndYear;

      INSERT INTO #Forecast ( Month_Identifier
                            , SiteId
                            , Forecasted_Usage )
                  SELECT
                        convert(VARCHAR(12), volumedetails.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER
                      , volumedetails.SITE_ID
                      , cast(sum(volumedetails.VOLUME * consumption.CONVERSION_FACTOR) AS DECIMAL(15, 3)) FORECASTED_USAGE
                  FROM
                        dbo.SITE s
                        INNER JOIN RM_FORECAST_VOLUME_DETAILS volumedetails
                              ON volumedetails.SITE_ID = s.SITE_ID
                        INNER JOIN RM_FORECAST_VOLUME volume
                              ON volumedetails.RM_FORECAST_VOLUME_ID = volume.RM_FORECAST_VOLUME_ID
                        INNER JOIN RM_ONBOARD_HEDGE_SETUP onboard
                              ON volumedetails.SITE_ID = onboard.SITE_ID
                        INNER JOIN CONSUMPTION_UNIT_CONVERSION consumption
                              ON onboard.VOLUME_UNITS_TYPE_ID = consumption.BASE_UNIT_ID
                                 AND consumption.CONVERTED_UNIT_ID = @unitId
                  WHERE
                        s.Client_ID = @ClientId
                        AND onboard.INCLUDE_IN_REPORTS = 1
                        AND volume.FORECAST_AS_OF_DATE IN ( @Max_FORECAST_AS_OF_DATE, @Min_FORECAST_AS_OF_DATE )
                        AND volumedetails.MONTH_IDENTIFIER
                        BETWEEN @StartDate AND @EndDate
                  GROUP BY
                        volumedetails.MONTH_IDENTIFIER
                      , volumedetails.SITE_ID;



      INSERT INTO #Forecast ( Month_Identifier
                            , SiteId
                            , Forecasted_Usage )
                  SELECT
                        convert(VARCHAR(12), volumedetails.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER
                      , volumedetails.SITE_ID
                      , cast(sum(volumedetails.VOLUME * consumption.CONVERSION_FACTOR) AS DECIMAL(15, 3)) FORECASTED_USAGE
                  FROM
                        RM_FORECAST_VOLUME_DETAILS volumedetails
                        INNER JOIN RM_FORECAST_VOLUME volume
                              ON volumedetails.RM_FORECAST_VOLUME_ID = volume.RM_FORECAST_VOLUME_ID
                        INNER JOIN RM_ONBOARD_HEDGE_SETUP onboard
                              ON volumedetails.SITE_ID = onboard.SITE_ID
                        INNER JOIN CONSUMPTION_UNIT_CONVERSION consumption
                              ON onboard.VOLUME_UNITS_TYPE_ID = consumption.BASE_UNIT_ID
                                 AND consumption.CONVERTED_UNIT_ID = @unitId
                        INNER JOIN dbo.RM_GROUP_SITE_MAP rgsm
                              ON rgsm.SITE_ID = volumedetails.SITE_ID
                  WHERE
                        onboard.INCLUDE_IN_REPORTS = 1
                        AND volume.FORECAST_AS_OF_DATE IN ( @Max_FORECAST_AS_OF_DATE, @Min_FORECAST_AS_OF_DATE )
                        AND volumedetails.MONTH_IDENTIFIER
                        BETWEEN @StartDate AND @EndDate
                  GROUP BY
                        volumedetails.MONTH_IDENTIFIER
                      , volumedetails.SITE_ID;



      INSERT INTO #Hedge_with_Type ( RM_DEAL_TICKET_ID
                                   , SITE_ID
                                   , HEDGE_PRICE
                                   , PREMIUM
                                   , MONTH_IDENTIFIER
                                   , OPTION_NAME
                                   , HEDGE_TYPE
                                   , MAX_HEDGE_PERCENT
                                   , CONVERSION_FACTOR
                                   , VOLUME_CONVERSION_FACTOR
                                   , CURRENCY_TYPE_ID
                                   , HEDGE_VOLUME )
      EXEC [dbo].[Report_DE_RM_Hedge]
            @fromDate = @StartDate
          , @toDate = @EndDate
          , @clientId = @ClientId
          , @unitId = @unitId;





      INSERT INTO #Hedge ( RM_DEAL_TICKET_ID
                         , SITE_ID
                         , HEDGE_PRICE
                         , PREMIUM
                         , MONTH_IDENTIFIER
                         , OPTION_NAME
                         , HEDGE_TYPE
                         , MAX_HEDGE_PERCENT
                         , CONVERSION_FACTOR
                         , VOLUME_CONVERSION_FACTOR
                         , CURRENCY_TYPE_ID
                         , HEDGE_VOLUME )
                  SELECT
                        RM_DEAL_TICKET_ID
                      , SITE_ID
                      , sum(HEDGE_PRICE)
                      , PREMIUM
                      , MONTH_IDENTIFIER
                      , OPTION_NAME
                      , '' AS HEDGE_TYPE
                      , MAX_HEDGE_PERCENT
                      , CONVERSION_FACTOR
                      , VOLUME_CONVERSION_FACTOR
                      , CURRENCY_TYPE_ID
                      , sum(HEDGE_VOLUME)
                  FROM
                        #Hedge_with_Type
                  GROUP BY
                        RM_DEAL_TICKET_ID
                      , SITE_ID
                      , PREMIUM
                      , MONTH_IDENTIFIER
                      , OPTION_NAME
                      , MAX_HEDGE_PERCENT
                      , CONVERSION_FACTOR
                      , VOLUME_CONVERSION_FACTOR
                      , CURRENCY_TYPE_ID;


      INSERT INTO #Contract ( SiteId
                            , Month_Identifier
                            , ContractMeterVolume )
                  SELECT
                        sit.SITE_ID
                      , cmv.MONTH_IDENTIFIER
                      , sum(  CASE WHEN freq_type.ENTITY_NAME = 'Daily'
                                        AND @frequency_type = 'Monthly' THEN (( round(cmv.VOLUME * cuc.CONVERSION_FACTOR, 0)) * dd.DAYS_IN_MONTH_NUM )
                                   WHEN freq_type.ENTITY_NAME = CASE WHEN @frequency_type = 'Monthly' THEN 'Month'
                                                                     ELSE  @frequency_type
                                                                END THEN ( round(cmv.VOLUME * cuc.CONVERSION_FACTOR, 0))
                                   ELSE  NULL
                              END) AS ContractMeterVolume
                  FROM
                        dbo.CONTRACT con
                        JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP map
                              ON map.Contract_ID = con.CONTRACT_ID
                        JOIN dbo.ACCOUNT suppacc
                              ON suppacc.ACCOUNT_ID = map.ACCOUNT_ID
                        JOIN Core.Client_Hier_Account ca
                              ON ca.Meter_Id = map.METER_ID
                                 AND ca.Account_Type = 'Utility'
                        JOIN Core.Client_Hier ch
                              ON ch.Client_Hier_Id = ca.Client_Hier_Id
                        JOIN dbo.SITE sit
                              ON sit.SITE_ID = ch.Site_Id
                        JOIN dbo.Commodity cm
                              ON cm.Commodity_Id = con.COMMODITY_TYPE_ID
                                 AND cm.Commodity_Name = 'Natural Gas'
                        JOIN dbo.ENTITY S_type
                              ON S_type.ENTITY_ID = sit.SITE_TYPE_ID
                        LEFT JOIN dbo.CONTRACT_METER_VOLUME cmv
                              ON cmv.CONTRACT_ID = con.CONTRACT_ID
                                 AND cmv.METER_ID = ca.Meter_Id
                        LEFT JOIN dbo.ENTITY freq_type
                              ON freq_type.ENTITY_ID = cmv.FREQUENCY_TYPE_ID
                        LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                              ON cuc.BASE_UNIT_ID = cmv.UNIT_TYPE_ID
                                 AND cuc.CONVERTED_UNIT_ID = @unitId
                        LEFT JOIN dbo.ENTITY ent_uofm
                              ON ent_uofm.ENTITY_ID = cmv.UNIT_TYPE_ID
                                 AND ent_uofm.ENTITY_DESCRIPTION IN ( 'Unit for Gas' )
                        INNER JOIN meta.DATE_DIM dd
                              ON dd.DATE_D = cmv.MONTH_IDENTIFIER
                  WHERE
                        map.IS_HISTORY <> 1
                        AND ch.Client_Id = @ClientId
                        AND cmv.MONTH_IDENTIFIER
                        BETWEEN @StartDate AND @EndDate
                        AND ca.Account_Not_Managed = 0
                        AND ch.Site_Not_Managed = 0
                        AND freq_type.ENTITY_NAME IS NOT NULL
                        AND cmv.VOLUME <> 0
                  GROUP BY
                        sit.SITE_ID
                      , cmv.MONTH_IDENTIFIER;




      INSERT INTO #Hedge_Volume ( Month_Identifier
                                , SiteId
                                , Hedge_Volume
                                , Hedge_Price
                                , Hedge_Type )
                  SELECT
                        k.MONTH_IDENTIFIER
                      , k.SITE_ID
                      , sum(k.HEDGE_VOLUME) HEDGE_VOLUME
                      , sum(HEDGE_PRICE) / sum(HEDGE_VOLUME) HEDGE_PRICE
                      , HEDGE_TYPE
                  FROM
                        (     SELECT
                                    Hedge.MONTH_IDENTIFIER
                                  , Hedge.SITE_ID
                                  , Hedge.HEDGE_VOLUME
                                  , CASE WHEN cu.CURRENCY_UNIT_NAME != @Currency THEN CASE WHEN @Currency = 'USD' THEN cast(( Hedge.HEDGE_VOLUME * ( Hedge.HEDGE_PRICE * Hedge.CONVERSION_FACTOR ) / Hedge.VOLUME_CONVERSION_FACTOR ) AS DECIMAL(10, 3))
                                                                                           WHEN @Currency = 'CAN' THEN cast(( Hedge.HEDGE_VOLUME * (( Hedge.HEDGE_PRICE / Hedge.CONVERSION_FACTOR ) * Hedge.VOLUME_CONVERSION_FACTOR )) AS DECIMAL(10, 3))
                                                                                      END
                                         ELSE  cast(( Hedge.HEDGE_VOLUME * ( Hedge.HEDGE_PRICE / Hedge.VOLUME_CONVERSION_FACTOR )) AS DECIMAL(10, 3))
                                    END AS HEDGE_PRICE
                                  , HEDGE_TYPE
                              FROM
                                    #Hedge Hedge
                                    INNER JOIN dbo.CURRENCY_UNIT cu
                                          ON Hedge.CURRENCY_TYPE_ID = cu.CURRENCY_UNIT_ID ) k
                  GROUP BY
                        k.MONTH_IDENTIFIER
                      , k.SITE_ID
                      , k.HEDGE_TYPE
                  HAVING
                        sum(k.HEDGE_VOLUME) > 0;


      INSERT INTO #Sites ( SiteId
                         , Hedge_Type )
                  SELECT
                        SiteId
                      , Hedge_Type
                  FROM
                        #Hedge_Volume
                  GROUP BY
                        SiteId
                      , Hedge_Type;


      CREATE TABLE #Dates ( MONTH_IDENTIFIER DATETIME );
      INSERT INTO #Dates ( MONTH_IDENTIFIER )
                  SELECT
                          cast(DATE_D AS DATETIME) AS MONTH_IDENTIFIER
                  FROM
                          meta.DATE_DIM
                  WHERE
                          DATE_D
                  BETWEEN @StartDate AND @EndDate;




      INSERT INTO #RM_Budget ( BudgetWACOGPRICE
                             , Month_Identifier
                             , SiteId
                             , Hedge_Type )
                  SELECT
                        CASE WHEN sum(Volume) = 0 THEN 0
                             ELSE  CASE WHEN rm.SiteId IS NULL THEN BudgetWACOGPRICE
                                        ELSE  sum(PriceVolume) / sum(Volume)
                                   END
                        END test
                      , Alldays.MONTH_IDENTIFIER
                      , s.SiteId
                      , s.Hedge_Type
                  FROM
                        #Sites s
                        CROSS JOIN #Dates Alldays
                        LEFT JOIN #BudgetWACOGPRICE rm
                              ON s.SiteId = rm.SiteId
                                 AND Alldays.MONTH_IDENTIFIER = rm.Month_Identifier
                        LEFT JOIN
                              (     SELECT
                                          CASE sum(a.Volume)
                                               WHEN 0 THEN 0.0
                                               ELSE  sum(a.PriceVolume) / sum(a.Volume)
                                          END AS BudgetWACOGPRICE
                                        , a.Month_Identifier MONTH_IDENTIFIER
                                    FROM
                                          #BudgetWACOGPRICE a
                                    GROUP BY
                                          a.Month_Identifier ) k
                              ON Alldays.MONTH_IDENTIFIER = k.MONTH_IDENTIFIER
                  GROUP BY
                        Alldays.MONTH_IDENTIFIER
                      , s.SiteId
                      , s.Hedge_Type
                      , rm.SiteId
                      , BudgetWACOGPRICE;




      SELECT
           
            ch.Client_Hier_Id
          , isnull(gn.GROUP_NAME, '') Group_Name           
          , dd_N.Service_Month_Id  
          , @Commodity_UOM_Base_Key_Id AS Volume_UOM_Base_Key_Id
          , fc.Forecasted_Usage
          , hv.Hedge_Volume
          , hv.Hedge_Price
          , rmb.BudgetWACOGPRICE
          , ct.ContractMeterVolume
          , isnull(hv.Hedge_Volume, 0) * isnull(hv.Hedge_Price, 0) AS HedgePriceTotal
          , isnull(fc.Forecasted_Usage, 0) * isnull(rmb.BudgetWACOGPRICE, 0) AS BudgetWACOGPRICETotal
      FROM
            Core.Client_Hier ch
            CROSS JOIN #Dates dd
            INNER JOIN POC_BITool_Qlik2.dbo.vw_Date_Dim dd_N
                  ON dd_N.Service_Month = dd.MONTH_IDENTIFIER
            LEFT JOIN dbo.RM_ONBOARD_HEDGE_SETUP rhs
                  ON ch.Site_Id = rhs.SITE_ID
            INNER JOIN dbo.ENTITY enht
                  ON enht.ENTITY_ID = rhs.HEDGE_TYPE_ID
            LEFT JOIN #RM_Budget rmb
                  ON rmb.SiteId = ch.Site_Id
                     AND dd.MONTH_IDENTIFIER = rmb.Month_Identifier
            LEFT JOIN #Hedge_Volume hv
                  ON rmb.SiteId = hv.SiteId
                     AND rmb.Month_Identifier = hv.Month_Identifier
                     AND rmb.Hedge_Type = hv.Hedge_Type
            LEFT JOIN #Hedge h
                  ON rmb.SiteId = h.SITE_ID
                     AND rmb.Month_Identifier = h.MONTH_IDENTIFIER
                     AND rmb.Hedge_Type = h.HEDGE_TYPE
            LEFT JOIN #Forecast fc
                  ON fc.Month_Identifier = dd.MONTH_IDENTIFIER
                     AND fc.SiteId = ch.Site_Id
            LEFT JOIN #Contract ct
                  ON ct.Month_Identifier = dd.MONTH_IDENTIFIER
                     AND ct.SiteId = ch.Site_Id
            LEFT JOIN
                  (     SELECT
                              rgsm.SITE_ID
                            , rg.RM_GROUP_ID
                            , rg.GROUP_NAME
                        FROM
                              dbo.RM_GROUP_SITE_MAP rgsm
                              INNER JOIN dbo.RM_GROUP rg
                                    ON rgsm.RM_GROUP_ID = rg.RM_GROUP_ID ) gn
                  ON ch.Site_Id = gn.SITE_ID
      WHERE
            ch.Site_Closed = 0
            AND enht.ENTITY_NAME != 'Does not hedge'
            AND ch.Client_Id = @ClientId
            AND ch.Site_Id > 0
            AND (     enht.ENTITY_ID IS NOT NULL
                      OR rmb.Month_Identifier IS NOT NULL )
            AND EXISTS (     SELECT
                                   1
                             FROM
                                   dbo.RM_ONBOARD_HEDGE_SETUP RM
                             WHERE
                                   INCLUDE_IN_REPORTS = 1
                                   AND RM.SITE_ID = ch.Site_Id )
      GROUP BY
            ch.Client_Hier_Id
          , isnull(gn.GROUP_NAME, '')
          , dd_N.Service_Month_Id         
          , fc.Forecasted_Usage    
          , hv.Hedge_Volume
          , hv.Hedge_Price
          , rmb.BudgetWACOGPRICE
          , ct.ContractMeterVolume;
                          
END
GO

GRANT EXECUTE ON  [dbo].[Get_RM_Data] TO [CBMSApplication]
GO
