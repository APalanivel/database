SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- exec BUDGET_GET_BUDGET_ACCOUNTS_P '','',1

CREATE  PROCEDURE dbo.BUDGET_GET_BUDGET_ACCOUNTS_HEADER_P
	@userId varchar(10),
	@sessionId varchar(20),
	@budget_id int
	AS
	begin
	set nocount on 
	select  budget.budget_id as budgetId,
		cli.client_name as clientName,
		budget_commodity.entity_name as commodity,
		budget.due_date as dueDate
	
	from 	budget budget(nolock)
		join client cli(nolock)on cli.client_id = budget.client_id and budget.budget_id = @budget_id		
		join entity budget_commodity(nolock)on budget_commodity.entity_id = budget.commodity_type_id
		and budget_commodity.entity_type = 157
		
		

	end









GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_BUDGET_ACCOUNTS_HEADER_P] TO [CBMSApplication]
GO
