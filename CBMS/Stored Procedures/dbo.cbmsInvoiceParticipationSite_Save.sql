SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsInvoiceParticipationSite_Save

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@site_id       	int       	          	
	@service_month 	datetime  	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE           PROCEDURE [dbo].[cbmsInvoiceParticipationSite_Save]
   (
     @MyAccountId int
   , @site_id int
   , @service_month datetime
	
   )
AS
BEGIN
      exec cbmsinvoiceparticipationsite_save3 @myaccountid,
         @site_id, @service_month
/*
	set nocount on

	if exists(select * from invoice_participation where site_id = @site_id and service_month = @service_month)
	begin

	  declare @this_id int
		, @expected_count int
		, @received_count int
		, @true_received_count int
		, @is_complete bit
		, @is_published bit
		, @under_review bit
		, @ng_expected_count int
		, @ng_received_count int
		, @ng_true_received_count int
		, @ng_is_complete bit
		, @ng_is_published bit
		, @ng_under_review bit
		, @el_expected_count int
		, @el_received_count int
		, @el_true_received_count int
		, @el_is_complete bit
		, @el_is_published bit
		, @el_under_review bit


	   select @this_id = invoice_participation_site_id
	     from invoice_participation_site 
	    where site_id = @site_id
	      and service_month = @service_month



	   select @expected_count = 	isNull(max(y.ng_expected_count) + max(y.el_expected_count) , 0)
		, @received_count = 	isNull(max(y.ng_received_count) + max(y.el_received_count) , 0)
		, @is_complete = 	isNull(case when max(y.ng_is_complete) = 1 and max(y.el_is_complete) = 1 then 1 else 0 end , 0)
		, @is_published = 	isNull(case when max(y.ng_is_published) = 1 or max(y.el_is_published) = 1 then 1 else 0 end , 0)
		, @under_review = 	isNull(case when max(y.ng_under_review) = 1 or max(y.el_under_review) = 1 then 1 else 0 end , 0)

		, @ng_expected_count = 	isNull(max(y.ng_expected_count) 	, 0)
		, @ng_received_count = 	isNull(max(y.ng_received_count) 	, 0)
		, @ng_is_complete = 	isNull(max(y.ng_is_complete) 	, 0)
		, @ng_is_published = 	isNull(max(y.ng_is_published) 	, 0)
		, @ng_under_review = 	isNull(max(y.ng_under_review) 	, 0)

		, @el_expected_count = 	isNull(max(y.el_expected_count) 	, 0)
		, @el_received_count = 	isNull(max(y.el_received_count) 	, 0)
		, @el_is_complete = 	isNull(max(y.el_is_complete) 	, 0)
		, @el_is_published = 	isNull(max(y.el_is_published) 	, 0)
		, @el_under_review = 	isNull(max(y.el_under_review) 	, 0)

	     from (

		   select x.site_id
			, x.service_month
	
			, 0 expected_count
			, 0 received_count
			, 0 is_complete
			, 0 is_published
			, 0 under_review
	
			, sum(case when x.is_expected = 1 then 1 else 0 end) ng_expected_count
			, sum(case when x.is_expected = 1 and x.is_received = 1 then 1 else 0 end) ng_received_count
			, sum(case when x.is_received = 1 then 1 else 0 end) ng_actual_received_count
			, case when sum(case when x.is_expected = 1 then 1 else 0 end) = sum(case when x.is_expected = 1 and x.is_received = 1 then 1 else 0 end) then 1 else 0 end ng_is_complete
			, case when sum(case when x.is_received = 1 then 1 else 0 end) > 0 then 1 else 0 end ng_is_published
			, isNull(max(case when x.recalc_under_review = 1 then 1 else x.variance_under_review end), 0) ng_under_review
	
			, 0 el_expected_count
			, 0 el_received_count
			, 0 el_actual_received_count
			, 0 el_is_complete
			, 0 el_is_published
			, 0 el_under_review
	
		     from invoice_participation x 
		     join vwAccountCommodity vam on vam.account_id = x.account_id

		    where x.site_id = @site_id
		      and x.service_month = @service_month
		      and vam.commodity_type_id = 291
	
		 group by x.site_id
			, x.service_month
	
		  union all
	
		   select x.site_id
			, x.service_month
	
			, 0 expected_count
			, 0 received_count
			, 0 is_complete
			, 0 is_published
			, 0 under_review
	
			, 0 ng_expected_count
			, 0 ng_received_count
			, 0 ng_actual_received_count
			, 0 ng_is_complete
			, 0 ng_is_published
			, 0 ng_under_review
	
			, sum(case when x.is_expected = 1 then 1 else 0 end) el_expected_count
			, sum(case when x.is_expected = 1 and x.is_received = 1 then 1 else 0 end) el_received_count
			, sum(case when x.is_received = 1 then 1 else 0 end) el_actual_received_count
			, case when sum(case when x.is_expected = 1 then 1 else 0 end) = sum(case when x.is_expected = 1 and x.is_received = 1 then 1 else 0 end) then 1 else 0 end el_is_complete
			, case when sum(case when x.is_received = 1 then 1 else 0 end) > 0 then 1 else 0 end el_is_published
			, isNull(max(case when x.recalc_under_review = 1 then 1 else x.variance_under_review end), 0) el_under_review
	
		     from invoice_participation x 
		     join vwAccountCommodity vam  on vam.account_id = x.account_id
	
		    where x.site_id = @site_id
		      and x.service_month = @service_month
		      and vam.commodity_type_id = 290
	
		 group by x.site_id
			, x.service_month


		  ) y

	 group by y.site_id
		, y.service_month


		if @this_id is null
		begin

			insert into invoice_participation_site
				( site_id, service_month
				, expected_count, received_count, is_complete, is_published, under_review 
				, ng_expected_count, ng_received_count, ng_is_complete, ng_is_published, ng_under_review 
				, el_expected_count, el_received_count, el_is_complete, el_is_published, el_under_review 
				)
			values
				( @site_id, @service_month
				, @expected_count, @received_count, @is_complete, @is_published, @under_review 
				, @ng_expected_count, @ng_received_count, @ng_is_complete, @ng_is_published, @ng_under_review 
				, @el_expected_count, @el_received_count, @el_is_complete, @el_is_published, @el_under_review 
				)
	
		end
		else
		begin

		   update invoice_participation_site 
		      set expected_count 	= @expected_count
			, received_count 	= @received_count
			, is_complete 		= @is_complete
			, is_published 		= @is_published
			, under_review 		= @under_review
			, ng_expected_count 	= @ng_expected_count
			, ng_received_count 	= @ng_received_count
			, ng_is_complete 	= @ng_is_complete
			, ng_is_published 	= @ng_is_published
			, ng_under_review 	= @ng_under_review
			, el_expected_count 	= @el_expected_count
			, el_received_count 	= @el_received_count
			, el_is_complete 	= @el_is_complete
			, el_is_published 	= @el_is_published
			, el_under_review 	= @el_under_review
		    where invoice_participation_site_id = @this_id

		end

	end -- exists
*/
   END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvoiceParticipationSite_Save] TO [CBMSApplication]
GO
