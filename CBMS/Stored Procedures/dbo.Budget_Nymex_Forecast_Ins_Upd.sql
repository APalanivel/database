SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******************************************************************************************************         
NAME: dbo.Budget_Nymex_Forecast_Ins_Upd
    
DESCRIPTION:    
    
     This procedure will be used to save the Budget Nymex Forecast data.
         
INPUT PARAMETERS:    
      NAME							DATATYPE				DEFAULT           DESCRIPTION    
------------------------------------------------------------------------    
	
    @Tvp_Budget_Nymex_Forecast		tvp_Budget_Nymex_Forecast	
                    
OUTPUT PARAMETERS:              
      NAME              DATATYPE    DEFAULT           DESCRIPTION       
           
------------------------------------------------------------              
USAGE EXAMPLES:              
------------------------------------------------------------         
	
	BEGIN TRAN       
        
	DECLARE @Tvp_Budget_Nymex_Forecast tvp_Budget_Nymex_Forecast
	INSERT INTO @Tvp_Budget_Nymex_Forecast VALUES (592,'2013-11-01',1000)
	EXEC dbo.Budget_Nymex_Forecast_Ins_Upd @Tvp_Budget_Nymex_Forecast,'Electric Power'
	
	ROLLBACK TRAN    
       
AUTHOR INITIALS:              
      INITIALS    NAME              
------------------------------------------------------------              
      RMG         Rani Mary George
              
MODIFICATIONS:    
      INITIALS    DATE           MODIFICATION              
------------------------------------------------------------              
      RMG 		  2013-03-25	 CREATED
******************************************************************************************************/  
CREATE PROCEDURE dbo.Budget_Nymex_Forecast_Ins_Upd
      ( 
       @Tvp_Budget_Nymex_Forecast tvp_Budget_Nymex_Forecast READONLY
      ,@Commodity_Name VARCHAR(200) )
AS 
BEGIN
		
      SET NOCOUNT ON ;
      
      DECLARE @Nymex_Type_Id INT
      DECLARE @Commodity_Id INT
	
      DECLARE @TempTable TABLE ( Account_Id INT ) ;  
		
		--To get Nymex Forecast Type Id
      SELECT
            @Nymex_Type_Id = enty.ENTITY_ID
      FROM
            dbo.ENTITY enty
      WHERE
            enty.ENTITY_NAME = 'Manual'
            AND enty.ENTITY_DESCRIPTION = 'NYMEX_TYPE' 
			
		--To get Commodity Id	
      SELECT
            @Commodity_Id = enty.ENTITY_ID
      FROM
            dbo.ENTITY enty
      WHERE
            enty.ENTITY_NAME = @Commodity_Name
            AND enty.ENTITY_DESCRIPTION = 'Commodity' ;
           
      WITH  CTE
              AS ( SELECT
                        tvp.ACCOUNT_ID
                       ,tvp.MONTH_IDENTIFIER
                       ,tvp.Price
                   FROM
                        @Tvp_Budget_Nymex_Forecast AS tvp
                        LEFT JOIN dbo.BUDGET_NYMEX_FORECAST bnf
                              ON bnf.ACCOUNT_ID = tvp.ACCOUNT_ID
                                 AND bnf.MONTH_IDENTIFIER = tvp.MONTH_IDENTIFIER
                                 AND bnf.COMMODITY_TYPE_ID = @Commodity_Id
                        LEFT JOIN dbo.RM_FORECAST_DETAILS AS rfd
                              ON rfd.MONTH_IDENTIFIER = tvp.MONTH_IDENTIFIER
                        INNER JOIN dbo.RM_FORECAST AS rf
                              ON rf.RM_FORECAST_ID = rfd.RM_FORECAST_ID
                   WHERE
                        rf.FORECAST_AS_OF_DATE = ( SELECT
                                                      max(FORECAST_FROM_DATE)
                                                   FROM
                                                      dbo.RM_FORECAST AS rf
                                                      INNER JOIN dbo.ENTITY AS en
                                                            ON rf.FORECAST_TYPE_ID = en.ENTITY_ID
                                                   WHERE
                                                      en.ENTITY_NAME = 'Nymex' )
                        AND ( ( bnf.ACCOUNT_ID IS NULL
                                AND tvp.Price = rfd.MODERATE_PRICE )
                              OR ( bnf.ACCOUNT_ID IS NOT NULL
                                   AND tvp.Price = bnf.PRICE ) )
                   GROUP BY
                        tvp.MONTH_IDENTIFIER
                       ,tvp.Price
                       ,tvp.ACCOUNT_ID),
            CTE_Nymex_Forecast
              AS ( SELECT
                        tv.Account_Id
                       ,tv.Month_Identifier
                       ,tv.Price
                   FROM
                        @Tvp_Budget_Nymex_Forecast tv
                   WHERE
                        tv.Account_Id IN ( SELECT
                                                tvp1.Account_Id
                                           FROM
                                                @Tvp_Budget_Nymex_Forecast tvp1
                                                LEFT JOIN CTE src
                                                      ON src.Price = tvp1.Price
                                                         AND src.Month_Identifier = tvp1.Month_Identifier
                                                         AND src.Account_Id = tvp1.Account_Id
                                           WHERE
                                                src.Account_Id IS NULL ))
            MERGE dbo.BUDGET_NYMEX_FORECAST tgt
                  USING CTE_Nymex_Forecast AS src
                  ON tgt.COMMODITY_TYPE_ID = @Commodity_Id
                        AND tgt.ACCOUNT_ID = src.Account_Id
                        AND tgt.MONTH_IDENTIFIER = src.Month_Identifier
                  WHEN MATCHED AND tgt.PRICE <> src.Price
                        THEN
				UPDATE   SET  
                              tgt.PRICE = src.Price
                             ,tgt.NYMEX_TYPE_ID = @Nymex_Type_Id
                  WHEN NOT MATCHED 
                        THEN
				INSERT
                              ( 
                               ACCOUNT_ID
                              ,NYMEX_TYPE_ID
                              ,COMMODITY_TYPE_ID
                              ,MONTH_IDENTIFIER
                              ,PRICE )
                         VALUES
                              ( 
                               src.Account_Id
                              ,@Nymex_Type_Id
                              ,@Commodity_Id
                              ,src.Month_Identifier
                              ,src.Price )
                  OUTPUT
                        Deleted.ACCOUNT_ID
                        INTO @TempTable ;
				
      UPDATE
            bnf
      SET   
            bnf.NYMEX_TYPE_ID = @Nymex_Type_Id
      FROM
            @Tvp_Budget_Nymex_Forecast tvp
            INNER JOIN ( SELECT
                              Account_Id
                         FROM
                              @TempTable
                         GROUP BY
                              Account_Id ) temp
                  ON temp.Account_Id = tvp.Account_Id
            INNER JOIN dbo.BUDGET_NYMEX_FORECAST bnf
                  ON bnf.ACCOUNT_ID = tvp.Account_Id
                     AND bnf.MONTH_IDENTIFIER = tvp.Month_Identifier
                     AND bnf.COMMODITY_TYPE_ID = @Commodity_Id
      WHERE
            temp.Account_Id IS NOT NULL
                    															
END





;
GO
GRANT EXECUTE ON  [dbo].[Budget_Nymex_Forecast_Ins_Upd] TO [CBMSApplication]
GO
