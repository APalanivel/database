SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:   [dbo].[[Invoice_Collection_Batch_Dtls_Ins_Volume_Charge_Check_Records]]               
                      
Description:                      
   This sproc is used to fill the ICQ Batch Table.        
         
  [__$operation] Identifies the Data Changes and it can be one of the following.      
    1 = delete      
       2 = insert      
    3 = update (captured column values are those before the update operation)      
    4 = update (captured column values are those after the update operation)            
      
 Input Parameters:                      
    Name     DataType   Default     Description                        
--------------------------------------------------------------------------------------       
                       
 Output Parameters:                            
    Name     DataType   Default     Description                        
--------------------------------------------------------------------------------------                        
      
 Usage Examples:                          
--------------------------------------------------------------------------------------           
  BEGIN TRAN      
 exec dbo.[Invoice_Collection_Batch_Dtls_Ins_Volume_Charge_Check_Records]_Received_Not_Processed_Records      
  ROLLBACK      
      
Author Initials:                      
    Initials Name                      
--------------------------------------------------------------------------------------                        
 RKV   Ravi Kumar Vegesna      
       
 Modifications:                      
    Initials        Date  Modification                      
--------------------------------------------------------------------------------------                        
    RKV    2019-10-2019  Created For Invoice_Collection.      
       
******/  
CREATE PROCEDURE [dbo].[Invoice_Collection_Batch_Dtls_Ins_Volume_Charge_Check_Records]  
AS  
    BEGIN  
  
        SET NOCOUNT ON;  
  
        DECLARE  
            @Record_Cnt INT = 1  
            , @Status_Cd INT  
            , @Upload_Batch_Id INT  
            , @Start_Dt DATETIME  
            , @ICR_Batch_Type_Cd INT  
            , @Batch_Size INT  
            , @Min_CT_Ver BIGINT  
            , @Max_CT_Ver BIGINT;  
  
  
        DECLARE @Custom_Frequency_Cd INT;



        SELECT
            @Custom_Frequency_Cd = c.Code_Id
        FROM
            Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            Code_Value = 'Custom'
            AND cs.Codeset_Name = 'InvoiceFrequency';
		
		
		
		CREATE TABLE #Active_Icac  
             (  
                 Invoice_Collection_Account_Config_Id INT NOT NULL  
             );  
  
  
        INSERT INTO #Active_Icac  
             (  
                 Invoice_Collection_Account_Config_Id  
             )  
        SELECT  
            icac.Invoice_Collection_Account_Config_Id  
        FROM  
            dbo.Invoice_Collection_Account_Config icac  
            INNER JOIN Core.Client_Hier_Account cha  
                ON cha.Account_Id = icac.Account_Id  
            INNER JOIN Core.Client_Hier ch  
                ON ch.Client_Hier_Id = cha.Client_Hier_Id  
        WHERE  
            icac.Is_Chase_Activated = 1  
            AND icac.is_Config_Complete = 1  
            AND (   EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Invoice_collection_New_Batch_Clients icnbc
                               WHERE
                                    icnbc.Client_Id = ch.Client_Id)
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.Account_Invoice_Collection_Frequency aicf
                                   WHERE
                                        aicf.Invoice_Frequency_Cd = @Custom_Frequency_Cd
                                        AND aicf.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id))  
        GROUP BY  
            icac.Invoice_Collection_Account_Config_Id;  
  
        INSERT INTO #Active_Icac  
             (  
                 Invoice_Collection_Account_Config_Id  
             )  
        SELECT  
            icac.Invoice_Collection_Account_Config_Id  
        FROM  
            dbo.Invoice_Collection_Account_Config icac  
        WHERE  
            icac.Is_Chase_Activated = 1  
            AND icac.is_Config_Complete = 1  
            AND NOT EXISTS (SELECT  TOP 1   * FROM  #Active_Icac)  
        GROUP BY  
            icac.Invoice_Collection_Account_Config_Id;  
  
  
  
  
        SELECT  
            @Batch_Size = CAST(App_Config_Value AS INT)  
        FROM  
            dbo.App_Config ac  
        WHERE  
            App_Config_Cd = 'IC_Batch_Size';  
  
       SELECT  @Max_CT_Ver = CHANGE_TRACKING_CURRENT_VERSION();  
  
  
  
  
  
  
  
        SELECT  
            @ICR_Batch_Type_Cd = c.Code_Id  
        FROM  
            dbo.Code c  
            INNER JOIN dbo.Codeset cs  
                ON c.Codeset_Id = cs.Codeset_Id  
        WHERE  
            cs.Codeset_Name = 'ICBatchType'  
            AND c.Code_Value = 'Volume Charge Check';  
  
  
  
        SELECT  
            @Status_Cd = Code_Id  
        FROM  
            dbo.Code c  
            INNER JOIN dbo.Codeset cs  
                ON c.Codeset_Id = cs.Codeset_Id  
        WHERE  
            cs.Std_Column_Name = 'Invoice_Collection_Queue_Batch_Status_Cd'  
            AND c.Code_Value = 'Pending';  
  
        SELECT  
            @Min_CT_Ver = CAST(App_Config_Value AS BIGINT)  
        FROM  
            dbo.App_Config ac  
        WHERE  
            App_Config_Cd = 'IC_Batch_Process_Last_Run_CT_Volume_Charge_Check_Records_Data';  
  
  
        CREATE TABLE #Invoice_Collection_Batch_Dtl  
             (  
                 Invoice_Collection_Account_Config_Id INT NOT NULL  
                 , Collection_Start_Dt DATE NOT NULL  
                 , Collection_End_Dt DATE NOT NULL  
                 , Row_Num INT IDENTITY(1, 1)  
             );  
  
  
  
  
        -- Invoice Posted      
        INSERT INTO #Invoice_Collection_Batch_Dtl  
             (  
                 Invoice_Collection_Account_Config_Id  
                 , Collection_Start_Dt  
                 , Collection_End_Dt  
             )  
        SELECT  
            icac.Invoice_Collection_Account_Config_Id  
            , icac.Invoice_Collection_Service_Start_Dt  
            , icac.Invoice_Collection_Service_End_Dt  
        FROM  
            CHANGETABLE(CHANGES dbo.Cost_Usage_Account_Dtl, @Min_CT_Ver) ct  
            INNER JOIN dbo.Invoice_Collection_Account_Config icac  
                ON icac.Account_Id = ct.Account_Id  
            INNER JOIN dbo.Bucket_Master bm  
                ON ct.Bucket_Master_Id = bm.Bucket_Master_Id  
            INNER JOIN dbo.Account_Invoice_Collection_Month aicm  
                ON aicm.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id  
                   AND  ct.Service_Month = aicm.Service_Month  
            INNER JOIN dbo.Invoice_Collection_Queue_Month_Map icqmp  
                ON aicm.Account_Invoice_Collection_Month_Id = icqmp.Account_Invoice_Collection_Month_Id  
        WHERE  
            ct.SYS_CHANGE_VERSION <= @Max_CT_Ver  
             AND EXISTS (   SELECT  
                                1  
                           FROM  
                                #Active_Icac ai  
                           WHERE  
                                ai.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id)  
            AND NOT EXISTS (   SELECT  
                                    1  
                               FROM  
                                    #Invoice_Collection_Batch_Dtl bd  
                               WHERE  
                                    bd.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id)  
        GROUP BY  
            icac.Invoice_Collection_Account_Config_Id  
            , icac.Invoice_Collection_Service_Start_Dt  
            , icac.Invoice_Collection_Service_End_Dt;  
  
        -- Invoice Related Changes      
        INSERT INTO #Invoice_Collection_Batch_Dtl  
             (  
                 Invoice_Collection_Account_Config_Id  
                 , Collection_Start_Dt  
                 , Collection_End_Dt  
             )  
        SELECT  
            icac.Invoice_Collection_Account_Config_Id  
            , icac.Invoice_Collection_Service_Start_Dt  
            , icac.Invoice_Collection_Service_End_Dt  
        FROM  
            CHANGETABLE(CHANGES dbo.Cost_Usage_Account_Dtl, @Min_CT_Ver) ct  
            INNER JOIN dbo.Invoice_Collection_Account_Config icac  
                ON icac.Account_Id = ct.Account_Id  
            INNER JOIN dbo.Bucket_Master bm  
          ON ct.Bucket_Master_Id = bm.Bucket_Master_Id  
            INNER JOIN dbo.Account_Invoice_Collection_Month aicm  
                ON aicm.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id  
                   AND  ct.Service_Month = aicm.Service_Month  
            INNER JOIN dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map aicmcim  
                ON aicm.Account_Invoice_Collection_Month_Id = aicmcim.Account_Invoice_Collection_Month_Id  
        WHERE  
            ct.SYS_CHANGE_VERSION <= @Max_CT_Ver  
             AND EXISTS (   SELECT  
                                1  
                           FROM  
                                #Active_Icac ai  
                           WHERE  
                                ai.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id)  
            AND NOT EXISTS (   SELECT  
                                    1  
                               FROM  
                                    #Invoice_Collection_Batch_Dtl bd  
                               WHERE  
                                    bd.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id)  
        GROUP BY  
            icac.Invoice_Collection_Account_Config_Id  
            , icac.Invoice_Collection_Service_Start_Dt  
            , icac.Invoice_Collection_Service_End_Dt;  
  
  
  
  
        DELETE  
        icbd  
        FROM  
            #Invoice_Collection_Batch_Dtl icbd  
            INNER JOIN dbo.Invoice_Collection_Account_Config icac  
                ON icac.Invoice_Collection_Account_Config_Id = icbd.Invoice_Collection_Account_Config_Id  
            INNER JOIN Core.Client_Hier_Account cha  
                ON cha.Account_Id = icac.Account_Id  
        WHERE  
            cha.Account_Not_Managed = 1;  
  
        BEGIN TRY  
            BEGIN TRAN;  
  
            WHILE (@Record_Cnt < (SELECT    MAX(Row_Num)FROM    #Invoice_Collection_Batch_Dtl))  
                BEGIN  
  
                    INSERT INTO dbo.Invoice_Collection_Batch  
                         (  
                             Invoice_Collection_Batch_Type_Cd  
                             , Status_Cd  
                         )  
                    SELECT  @ICR_Batch_Type_Cd, @Status_Cd;  
  
                    SELECT  @Upload_Batch_Id = SCOPE_IDENTITY();  
  
                    INSERT INTO dbo.Invoice_Collection_Batch_Dtl  
                         (  
                             Invoice_Collection_Account_Config_Id  
                             , Invoice_Collection_Batch_Id  
                             , Collection_Start_Dt  
                             , Collection_End_Dt  
                             , Status_Cd  
                         )  
                    SELECT  
                        Invoice_Collection_Account_Config_Id  
                        , @Upload_Batch_Id  
                        , MAX(Collection_Start_Dt)  
                        , MAX(Collection_End_Dt)  
                        , @Status_Cd  
                    FROM  
                        #Invoice_Collection_Batch_Dtl icbd  
                    WHERE  
                        Row_Num BETWEEN @Record_Cnt  
                                AND     @Record_Cnt + (CAST(@Batch_Size AS INT) - 1)  
                    GROUP BY  
                        Invoice_Collection_Account_Config_Id;  
  
                    SET @Record_Cnt = @Record_Cnt + CAST(@Batch_Size AS INT);  
                END;  
  
  
            UPDATE  
                dbo.App_Config  
            SET  
                App_Config_Value = CAST(@Max_CT_Ver AS VARCHAR(255))  
            WHERE  
                App_Config_Cd = 'IC_Batch_Process_Last_Run_CT_Volume_Charge_Check_Records_Data';  
  
  
            DROP TABLE #Invoice_Collection_Batch_Dtl;  
  
  
            COMMIT TRAN;  
        END TRY  
        BEGIN CATCH  
  
            IF @@TRANCOUNT > 0  
                BEGIN  
                    ROLLBACK TRAN;  
                END;  
  
            EXEC dbo.usp_RethrowError;  
  
        END CATCH;  
  
    END;  
  
  
  
  
    ;  
  
  
  
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Batch_Dtls_Ins_Volume_Charge_Check_Records] TO [CBMSApplication]
GO
