SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.Exceptions_INS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@EXCEPTION_DATE	datetime  	          	
	@ERROR_NUM     	int       	          	
	@ERROR_MSG     	varchar(2500)	          	
	@DETAIL_INFO   	varchar(2500)	          	
	@STACK_TRACE   	text      	          	
	@SOURCE_APP    	varchar(255)	          	
	@SOURCE_HOST   	varchar(255)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	@O_EXCEPTION_ID	int       	NULL      	

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE dbo.Exceptions_INS_P
(	 @EXCEPTION_DATE	DATETIME
	,@ERROR_NUM		INT
	,@ERROR_MSG		VARCHAR(2500)
	,@DETAIL_INFO		VARCHAR(2500)
	,@STACK_TRACE		TEXT
	,@SOURCE_APP		VARCHAR(255)
	,@SOURCE_HOST		VARCHAR(255)
	
	,@O_EXCEPTION_ID	INT			= NULL	OUTPUT
)
AS
BEGIN 

	SET NOCOUNT ON 
Declare @O_EXCEPTION_ID_P	INT

Exec Session_db.dbo.Exceptions_INS_P
	 @EXCEPTION_DATE
   ,@ERROR_NUM
	,@ERROR_MSG
	,@DETAIL_INFO
	,@STACK_TRACE
	,@SOURCE_APP
	,@SOURCE_HOST
	,@O_EXCEPTION_ID_P	

   Return @O_EXCEPTION_ID_P

	

END
GO
GRANT EXECUTE ON  [dbo].[Exceptions_INS_P] TO [CBMSApplication]
GO
