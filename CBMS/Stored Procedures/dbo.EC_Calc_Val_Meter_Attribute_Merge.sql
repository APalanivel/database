SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
            
Name:   
	dbo.EC_Calc_Val_Meter_Attribute_Upd   
              
Description:              
			To update  Calc Val meter attribute configuration
              
 Input Parameters:              
    Name						DataType		Default		Description                
---------------------------------------------------------------------------------
	@EC_Calc_Val_Id				INT
    @EC_Meter_Attribute_Id		INT
    @EC_Meter_Attribute_Value	NVARCHAR(255)
    @User_Info_Id				INT
    
Output Parameters:                    
    Name						DataType		Default		Description                
---------------------------------------------------------------------------------
              
Usage Examples:                  
---------------------------------------------------------------------------------
select * from EC_Calc_Val_Meter_Attribute where EC_Meter_Attribute_Id = 61 and EC_Calc_Val_Id  = 28	
	
	BEGIN TRANSACTION
		EXEC dbo.EC_Calc_Val_Meter_Attribute_Merge
			@EC_Calc_Val_Id  = 28
			,@EC_Meter_Attribute_Id = 61
			,@EC_Meter_Attribute_Value  = '28'
			,@User_Info_Id = 49
	select * from EC_Calc_Val_Meter_Attribute where EC_Meter_Attribute_Id = 61 and EC_Calc_Val_Id  = 28
	
	ROLLBACK TRANSACTION
Author Initials:              
    Initials	Name              
---------------------------------------------------------------------------------
	RKV			Ravi Kumar Vegesna               
 
Modifications:              
	Initials    Date		Modification              
---------------------------------------------------------------------------------
    RKV			2015-06-18	Created For AS400.         
             
******/ 

CREATE PROCEDURE [dbo].[EC_Calc_Val_Meter_Attribute_Merge]
      ( 
       @EC_Calc_Val_Id INT
      ,@EC_Meter_Attribute_Id INT
      ,@EC_Meter_Attribute_Value NVARCHAR(255) = NULL 
      ,@User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 
      
      MERGE INTO dbo.EC_Calc_Val_Meter_Attribute tgt
            USING 
                  ( SELECT
                        @EC_Calc_Val_Id AS EC_Calc_Val_Id
                       ,@EC_Meter_Attribute_Id AS EC_Meter_Attribute_Id
                       ,@EC_Meter_Attribute_Value AS EC_Meter_Attribute_Value
                       ,@User_Info_Id AS User_Info_Id ) src
            ON ( tgt.EC_Calc_Val_Id = src.EC_Calc_Val_Id
                 AND tgt.EC_Meter_Attribute_Id = src.EC_Meter_Attribute_Id )
            WHEN MATCHED AND  src.EC_Meter_Attribute_Value IS NOT NULL
                  AND src.EC_Meter_Attribute_Value <> ''
                  THEN UPDATE
                    SET 
                        tgt.EC_Meter_Attribute_Value = src.EC_Meter_Attribute_Value
                       ,tgt.Update_User_Id = @User_Info_Id
            WHEN MATCHED AND ( src.EC_Meter_Attribute_Value IS  NULL
                               OR src.EC_Meter_Attribute_Value = '' )
                  THEN  DELETE
            WHEN NOT MATCHED AND  src.EC_Meter_Attribute_Value IS NOT NULL
                  AND src.EC_Meter_Attribute_Value <> ''
                  THEN INSERT
                        ( 
                         [EC_Calc_Val_Id]
                        ,[EC_Meter_Attribute_Id]
                        ,[EC_Meter_Attribute_Value]
                        ,[Created_User_Id]
                        ,[Update_User_Id] )
                    VALUES
                        ( 
                         src.EC_Calc_Val_Id
                        ,src.EC_Meter_Attribute_Id
                        ,src.EC_Meter_Attribute_Value
                        ,src.User_Info_Id
                        ,src.User_Info_Id );      
      
      
                  
END;




;
GO
GRANT EXECUTE ON  [dbo].[EC_Calc_Val_Meter_Attribute_Merge] TO [CBMSApplication]
GO
