SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME: [DBO].[Meter_Tax_Exemption_Document_Dtl_Sel_By_Account_Id]

DESCRIPTION:

	To Get meter tax Exemption Documents details for Selected Account Id.

INPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
@Account_Id		INT						Utility Account
@StartIndex		INT			1			
@EndIndex		INT			2147483647


OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------

	EXEC Meter_Tax_Exemption_Document_Dtl_Sel_By_Account_Id 9538,1,10

	EXEC Meter_Tax_Exemption_Document_Dtl_Sel_By_Account_Id 9986

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			25-MAY-10	CREATED

*/

CREATE PROCEDURE dbo.Meter_Tax_Exemption_Document_Dtl_Sel_By_Account_Id
 (
	@Account_Id		INT
	,@StartIndex	INT		= 1
	,@EndIndex		INT		= 2147483647
 )
AS
BEGIN

	SET NOCOUNT ON;

	WITH Cte_Meter_Exemption_Doc_List AS
	(
		SELECT
			 mcm.Cbms_Image_Id
			 , ROW_NUMBER() OVER(ORDER BY Cbms_Image_Id) Row_Num
			 , COUNT(1) OVER() Total_Rows
		FROM
			dbo.METER m
			JOIN dbo.METER_CBMS_IMAGE_MAP mcm
				 ON mcm.METER_ID = m.METER_ID
		WHERE
			 m.Account_Id = @Account_Id
	)
	SELECT
		img.Cbms_Doc_Id
		,edl.Cbms_Image_Id
		,edl.Total_Rows
	FROM
		Cte_Meter_Exemption_Doc_List edl
		JOIN dbo.Cbms_Image img
			ON img.Cbms_Image_Id = edl.Cbms_Image_Id
	WHERE
		edl.Row_Num BETWEEN @StartIndex AND @EndIndex

END
GO
GRANT EXECUTE ON  [dbo].[Meter_Tax_Exemption_Document_Dtl_Sel_By_Account_Id] TO [CBMSApplication]
GO
