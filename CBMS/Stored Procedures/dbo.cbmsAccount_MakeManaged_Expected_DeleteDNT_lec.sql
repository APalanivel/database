SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create  procedure dbo.cbmsAccount_MakeManaged_Expected_DeleteDNT_lec
@account_id int
as
begin
update account
	set
	not_managed = '0'
	,not_expected_date = null
	,not_expected_by_id = null
	,not_expected = '0'
	where account_id = @account_id

delete do_not_track
	where account_id = @account_id
end

/*
select *
from account
where account_id = 2679
*/


GO
GRANT EXECUTE ON  [dbo].[cbmsAccount_MakeManaged_Expected_DeleteDNT_lec] TO [CBMSApplication]
GO
