SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******

 NAME: CBMS.dbo.Client_Hier_Account_Data_Verification

 DESCRIPTION:

	Used to compare the Client_Hier table data with the base table values

 INPUT PARAMETERS:
 Name				DataType		Default			Description
-----------------------------------------------------------------
 
 OUTPUT PARAMETERS:
 Name				DataType		Default			Description
-----------------------------------------------------------------
 
 USAGE EXAMPLES:
-----------------------------------------------------------------

	EXEC dbo.Client_Hier_Account_Data_Verification
 
 AUTHOR INITIALS:
 Initials	Name 
-----------------------------------------------------------------
 SKA		Shobhit Kumar A
 NR			Narayana Reddy
 SP			Srinivas Patchava

 
 MODIFICATIONS   
 Initials	Date			Modification
------------------------------------------------------------  
 SKA		1/6/2011		Created to compare the Client_Hier_Account table data with base table values.
 NR			08-03-2017		Contract Placeholder - Removed Account_CONSOLIDATED_BILLING_POSTED_TO_UTILITY column.
 SP			2018-12-18		MAINT-8029 - Added Account_Not_Expected_Dt mismatch records.
 NR			2019-04-03		Data2.0 - Removed WATCHLIST-GROUP-INFO-ID  column from Account Table.
 RKV		2019-12-30		D20-1762  Removed steps 6,7,8 ubm details 

 
******/

CREATE PROCEDURE [dbo].[Client_Hier_Account_Data_Verification]
AS
    BEGIN

        SET NOCOUNT ON;

        CREATE TABLE #Account_Meter_Address
             (
                 Account_Id INT
                 , Meter_Id INT
                 , Meter_Address_Id INT
                 , Address_Line1 VARCHAR(200)
                 , Address_Line2 VARCHAR(200)
                 , City VARCHAR(200)
                 , ZipCode VARCHAR(30)
                 , State_Id INT
                 , State_Name VARCHAR(200)
                 , Country_Id INT
                 , Country_Name VARCHAR(200)
                 , Meter_Rate_Id INT
                 , Meter_Rate_Name VARCHAR(200)
                 , Commodity_Type_id INT
                 , Geo_Long DECIMAL(32, 16)
                 , Geo_Lat DECIMAL(32, 16)
                 , PRIMARY KEY
                   (
                       Account_Id
                       , Meter_Id
                   )
             );

        DECLARE @Client_Hier_Account_Data_Mismatch TABLE
              (
                  Column_Name VARCHAR(60)
                  , Account_ID INT
                  , Meter_ID INT
                  , Source_Value VARCHAR(200)
                  , Target_Value VARCHAR(200)
              );

        INSERT INTO #Account_Meter_Address
             (
                 Account_Id
                 , Meter_Id
                 , Meter_Address_Id
                 , Address_Line1
                 , Address_Line2
                 , City
                 , ZipCode
                 , State_Id
                 , State_Name
                 , Country_Id
                 , Country_Name
                 , Meter_Rate_Id
                 , Meter_Rate_Name
                 , Commodity_Type_id
                 , Geo_Long
                 , Geo_Lat
             )
        SELECT
            util.ACCOUNT_ID
            , m.METER_ID
            , m.ADDRESS_ID
            , addr.ADDRESS_LINE1
            , addr.ADDRESS_LINE2
            , addr.CITY
            , addr.ZIPCODE
            , addr.STATE_ID
            , st.STATE_NAME
            , cy.COUNTRY_ID
            , cy.COUNTRY_NAME
            , r.RATE_ID
            , r.RATE_NAME
            , r.COMMODITY_TYPE_ID
            , addr.GEO_LONG
            , addr.GEO_LAT
        FROM
            dbo.ACCOUNT util
            INNER JOIN dbo.ENTITY actType
                ON ACCOUNT_TYPE_ID = actType.ENTITY_ID
            INNER JOIN dbo.METER m
                ON util.ACCOUNT_ID = m.ACCOUNT_ID
            INNER JOIN dbo.ADDRESS addr
                ON m.ADDRESS_ID = addr.ADDRESS_ID
            INNER JOIN dbo.STATE st
                ON st.STATE_ID = addr.STATE_ID
            INNER JOIN dbo.COUNTRY cy
                ON cy.COUNTRY_ID = st.COUNTRY_ID
            INNER JOIN dbo.RATE r
                ON r.RATE_ID = m.RATE_ID
        WHERE
            actType.ENTITY_NAME = 'Utility';

        INSERT INTO #Account_Meter_Address
             (
                 Account_Id
                 , Meter_Id
                 , Meter_Address_Id
                 , Address_Line1
                 , Address_Line2
                 , City
                 , ZipCode
                 , State_Id
                 , State_Name
                 , Country_Id
                 , Country_Name
                 , Meter_Rate_Id
                 , Meter_Rate_Name
                 , Commodity_Type_id
                 , Geo_Long
                 , Geo_Lat
             )
        SELECT
            sup.ACCOUNT_ID
            , samm.METER_ID
            , m.ADDRESS_ID
            , addr.ADDRESS_LINE1
            , addr.ADDRESS_LINE2
            , addr.CITY
            , addr.ZIPCODE
            , addr.STATE_ID
            , st.STATE_NAME
            , cy.COUNTRY_ID
            , cy.COUNTRY_NAME
            , r.RATE_ID
            , r.RATE_NAME
            , r.COMMODITY_TYPE_ID
            , addr.GEO_LONG
            , addr.GEO_LAT
        FROM
            dbo.ACCOUNT sup
            INNER JOIN dbo.ENTITY actType
                ON sup.ACCOUNT_TYPE_ID = actType.ENTITY_ID
            INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                ON sup.ACCOUNT_ID = samm.ACCOUNT_ID
            INNER JOIN dbo.METER m
                ON m.METER_ID = samm.METER_ID
            INNER JOIN dbo.ADDRESS addr
                ON m.ADDRESS_ID = addr.ADDRESS_ID
            INNER JOIN dbo.STATE st
                ON st.STATE_ID = addr.STATE_ID
            INNER JOIN dbo.COUNTRY cy
                ON cy.COUNTRY_ID = st.COUNTRY_ID
            INNER JOIN dbo.RATE r
                ON r.RATE_ID = m.RATE_ID
        WHERE
            actType.ENTITY_NAME = 'supplier';

        --1
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Account_Invoice_Source_Cd'
            , cha.Account_Id
            , cha.Meter_Id
            , a.INVOICE_SOURCE_TYPE_ID
            , cha.Account_Invoice_Source_Cd
        FROM
            ACCOUNT a
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = a.ACCOUNT_ID
                   AND  ISNULL(a.INVOICE_SOURCE_TYPE_ID, 0) != ISNULL(cha.Account_Invoice_Source_Cd, 0);

        --2
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Account_Is_Data_Entry_Only'
            , cha.Account_Id
            , cha.Meter_Id
            , a.Is_Data_Entry_Only
            , cha.Account_Is_Data_Entry_Only
        FROM
            dbo.ACCOUNT a
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = a.ACCOUNT_ID
                   AND  ISNULL(a.Is_Data_Entry_Only, 0) != ISNULL(cha.Account_Is_Data_Entry_Only, 0);
        --3
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Account_Not_Expected'
            , cha.Account_Id
            , cha.Meter_Id
            , a.NOT_EXPECTED
            , cha.Account_Not_Expected
        FROM
            dbo.ACCOUNT a
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = a.ACCOUNT_ID
                   AND  a.NOT_EXPECTED != cha.Account_Not_Expected;

        --4
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Account_Not_Managed'
            , cha.Account_Id
            , cha.Meter_Id
            , a.NOT_MANAGED
            , cha.Account_Not_Managed
        FROM
            dbo.ACCOUNT a
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = a.ACCOUNT_ID
                   AND  a.NOT_MANAGED != cha.Account_Not_Managed;
        --5
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Account_Service_level_Cd'
            , cha.Account_Id
            , cha.Meter_Id
            , a.SERVICE_LEVEL_TYPE_ID
            , cha.Account_Service_level_Cd
        FROM
            dbo.ACCOUNT a
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = a.ACCOUNT_ID
                   AND  ISNULL(a.SERVICE_LEVEL_TYPE_ID, 0) != ISNULL(cha.Account_Service_level_Cd, 0);

        
        --9
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Account_Vendor_Id'
            , cha.Account_Id
            , cha.Meter_Id
            , a.VENDOR_ID
            , cha.Account_Vendor_Id
        FROM
            dbo.ACCOUNT a
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = a.ACCOUNT_ID
        WHERE
            a.VENDOR_ID != cha.Account_Vendor_Id;

        --10 Account_Vendor_Name	
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Account_Vendor_Name'
            , cha.Account_Id
            , cha.Meter_Id
            , av.VENDOR_NAME
            , cha.Account_Vendor_Name
        FROM
            dbo.ACCOUNT a
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = a.ACCOUNT_ID
            INNER JOIN dbo.VENDOR av
                ON av.VENDOR_ID = a.VENDOR_ID
        WHERE
            cha.Account_Vendor_Name != av.VENDOR_NAME;

        --11 Account_Vendor_Type_ID
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Account_Vendor_Type_ID'
            , cha.Account_Id
            , cha.Meter_Id
            , av.VENDOR_TYPE_ID
            , cha.Account_Vendor_Type_ID
        FROM
            dbo.ACCOUNT a
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = a.ACCOUNT_ID
            INNER JOIN dbo.VENDOR av
                ON av.VENDOR_ID = a.VENDOR_ID
        WHERE
            av.VENDOR_TYPE_ID != cha.Account_Vendor_Type_ID;

        --12
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Account_Eligibility_Dt'
            , cha.Account_Id
            , cha.Meter_Id
            , a.ELIGIBILITY_DATE
            , cha.Account_Eligibility_Dt
        FROM
            dbo.ACCOUNT a
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = a.ACCOUNT_ID
                   AND  ISNULL(a.ELIGIBILITY_DATE, '1/1/1900') != ISNULL(cha.Account_Eligibility_Dt, '1/1/1900');
        --13



        --14
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Account_Group_ID'
            , cha.Account_Id
            , cha.Meter_Id
            , a.ACCOUNT_GROUP_ID
            , cha.Account_Group_ID
        FROM
            dbo.ACCOUNT a
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = a.ACCOUNT_ID
        WHERE
            ISNULL(a.ACCOUNT_GROUP_ID, 0) != ISNULL(cha.Account_Group_ID, 0);

        --15  --This section  removed WATCHLIST-GROUP-INFO-ID


        --16
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Supplier_Account_Begin_Dt'
            , cha.Account_Id
            , cha.Meter_Id
            , a.Supplier_Account_Begin_Dt
            , cha.Supplier_Account_begin_Dt
        FROM
            dbo.ACCOUNT a
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = a.ACCOUNT_ID
        WHERE
            ISNULL(a.Supplier_Account_Begin_Dt, '1/1/1900') != ISNULL(cha.Supplier_Account_begin_Dt, '1/1/1900');

        --17
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Supplier_Account_End_Dt'
            , cha.Account_Id
            , cha.Meter_Id
            , a.Supplier_Account_End_Dt
            , cha.Supplier_Account_End_Dt
        FROM
            ACCOUNT a
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = a.ACCOUNT_ID
        WHERE
            ISNULL(a.Supplier_Account_End_Dt, '1/1/1900') != ISNULL(cha.Supplier_Account_End_Dt, '1/1/1900');

        --18
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Supplier_Account_Recalc_Type_Cd'
            , cha.Account_Id
            , cha.Meter_Id
            , a.Supplier_Account_Recalc_Type_Cd
            , cha.Supplier_Account_Recalc_Type_Cd
        FROM
            dbo.ACCOUNT a
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = a.ACCOUNT_ID
        WHERE
            ISNULL(a.Supplier_Account_Recalc_Type_Cd, 0) != ISNULL(cha.Supplier_Account_Recalc_Type_Cd, 0);



        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Supplier_Account_Recalc_Type_Dsc'
            , cha.Account_Id
            , cha.Meter_Id
            , rtc.Code_Value
            , cha.Supplier_Account_Recalc_Type_Dsc
        FROM
            dbo.ACCOUNT a
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = a.ACCOUNT_ID
            INNER JOIN dbo.Code rtc
                ON rtc.Code_Id = a.Supplier_Account_Recalc_Type_Cd
        WHERE
            rtc.Code_Value != ISNULL(cha.Supplier_Account_Recalc_Type_Dsc, '');


        --19
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Supplier_Contract_ID'
            , cha.Account_Id
            , cha.Meter_Id
            , samm.Contract_ID
            , cha.Supplier_Contract_ID
        FROM
            dbo.ACCOUNT sup
            INNER JOIN dbo.ENTITY actType
                ON sup.ACCOUNT_TYPE_ID = actType.ENTITY_ID
            INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                ON sup.ACCOUNT_ID = samm.ACCOUNT_ID
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = sup.ACCOUNT_ID
                   AND  cha.Meter_Id = samm.METER_ID
        WHERE
            cha.Supplier_Contract_ID != samm.Contract_ID;

        --20
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Supplier_Meter_Association_Date'
            , cha.Account_Id
            , cha.Meter_Id
            , samm.METER_ASSOCIATION_DATE
            , cha.Supplier_Meter_Association_Date
        FROM
            dbo.ACCOUNT sup
            INNER JOIN dbo.ENTITY actType
                ON sup.ACCOUNT_TYPE_ID = actType.ENTITY_ID
            INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                ON sup.ACCOUNT_ID = samm.ACCOUNT_ID
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = sup.ACCOUNT_ID
                   AND  cha.Meter_Id = samm.METER_ID
        WHERE
            ISNULL(cha.Supplier_Meter_Association_Date, '1/1/1900') != ISNULL(samm.METER_ASSOCIATION_DATE, '1/1/1900');

        --21
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Supplier_Meter_Disassociation_Date'
            , cha.Account_Id
            , cha.Meter_Id
            , samm.METER_DISASSOCIATION_DATE
            , cha.Supplier_Meter_Association_Date
        FROM
            dbo.ACCOUNT sup
            INNER JOIN dbo.ENTITY actType
                ON sup.ACCOUNT_TYPE_ID = actType.ENTITY_ID
            INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                ON sup.ACCOUNT_ID = samm.ACCOUNT_ID
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = sup.ACCOUNT_ID
                   AND  cha.Meter_Id = samm.METER_ID
        WHERE
            ISNULL(cha.Supplier_Meter_Disassociation_Date, '1/1/1900') != ISNULL(
                                                                              samm.METER_DISASSOCIATION_DATE
                                                                              , '1/1/1900');


        --22

        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Meter_Address_ID'
            , sub.Account_Id
            , sub.Meter_Id
            , sub.Meter_Address_Id
            , cha.Meter_Address_ID
        FROM
            #Account_Meter_Address sub
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = sub.Account_Id
                   AND  cha.Meter_Id = sub.Meter_Id
        WHERE
            cha.Meter_Address_ID != sub.Meter_Address_Id;

        --23
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Meter_Address_Line_1'
            , sub.Account_Id
            , sub.Meter_Id
            , sub.Address_Line1
            , cha.Meter_Address_Line_1
        FROM
            #Account_Meter_Address sub
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = sub.Account_Id
                   AND  cha.Meter_Id = sub.Meter_Id
        WHERE
            ISNULL(cha.Meter_Address_Line_1, '') != ISNULL(sub.Address_Line1, '');

        --24
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Meter_Address_Line_2'
            , sub.Account_Id
            , sub.Meter_Id
            , sub.Address_Line2
            , cha.Meter_Address_Line_2
        FROM
            #Account_Meter_Address sub
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = sub.Account_Id
                   AND  cha.Meter_Id = sub.Meter_Id
        WHERE
            ISNULL(cha.Meter_Address_Line_2, '') != ISNULL(sub.Address_Line2, '');

        --25
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Meter_City'
            , sub.Account_Id
            , sub.Meter_Id
            , sub.City
            , cha.Meter_City
        FROM
            #Account_Meter_Address sub
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = sub.Account_Id
                   AND  cha.Meter_Id = sub.Meter_Id
        WHERE
            cha.Meter_City != sub.City;

        --26
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Meter_ZipCode'
            , sub.Account_Id
            , sub.Meter_Id
            , sub.ZipCode
            , cha.Meter_ZipCode
        FROM
            #Account_Meter_Address sub
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = sub.Account_Id
                   AND  cha.Meter_Id = sub.Meter_Id
        WHERE
            cha.Meter_ZipCode != sub.ZipCode;

        --27
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Meter_State_Id'
            , sub.Account_Id
            , sub.Meter_Id
            , sub.State_Id
            , cha.Meter_State_Id
        FROM
            #Account_Meter_Address sub
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = sub.Account_Id
                   AND  cha.Meter_Id = sub.Meter_Id
        WHERE
            cha.Meter_State_Id != sub.State_Id;

        --28
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Meter_Country_Id'
            , sub.Account_Id
            , sub.Meter_Id
            , sub.Country_Id
            , cha.Meter_Country_Id
        FROM
            #Account_Meter_Address sub
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = sub.Account_Id
                   AND  cha.Meter_Id = sub.Meter_Id
        WHERE
            cha.Meter_Country_Id != sub.Country_Id;

        --29
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Rate_Id'
            , sub.Account_Id
            , sub.Meter_Id
            , sub.Meter_Rate_Id
            , cha.Rate_Id
        FROM
            #Account_Meter_Address sub
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = sub.Account_Id
                   AND  cha.Meter_Id = sub.Meter_Id
        WHERE
            ISNULL(cha.Rate_Id, 0) != ISNULL(sub.Meter_Rate_Id, 0);


        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Commodity_Id'
            , sub.Account_Id
            , sub.Meter_Id
            , sub.Commodity_Type_id
            , cha.Commodity_Id
        FROM
            #Account_Meter_Address sub
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = sub.Account_Id
                   AND  cha.Meter_Id = sub.Meter_Id
        WHERE
            ISNULL(cha.Commodity_Id, 0) != ISNULL(sub.Commodity_Type_id, 0);

        --30
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Meter_Geo_Lat'
            , sub.Account_Id
            , sub.Meter_Id
            , sub.Geo_Lat
            , cha.Meter_Geo_Lat
        FROM
            #Account_Meter_Address sub
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = sub.Account_Id
                   AND  cha.Meter_Id = sub.Meter_Id
        WHERE
            ISNULL(cha.Meter_Geo_Lat, 0) != ISNULL(sub.Geo_Lat, 0);

        --31
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Meter_Geo_Long'
            , sub.Account_Id
            , sub.Meter_Id
            , sub.Geo_Long
            , cha.Meter_Geo_Long
        FROM
            #Account_Meter_Address sub
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = sub.Account_Id
                   AND  cha.Meter_Id = sub.Meter_Id
        WHERE
            ISNULL(cha.Meter_Geo_Long, 0) != ISNULL(sub.Geo_Long, 0);

        --32 Meter_State_Name
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Meter_State_Name'
            , sub.Account_Id
            , sub.Meter_Id
            , sub.State_Name
            , cha.Meter_State_Name
        FROM
            #Account_Meter_Address sub
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = sub.Account_Id
                   AND  cha.Meter_Id = sub.Meter_Id
        WHERE
            cha.Meter_State_Name != sub.State_Name;

        --33 Meter_Country_Name PENDING
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Meter_Country_Name'
            , sub.Account_Id
            , sub.Meter_Id
            , sub.Country_Name
            , cha.Meter_Country_Name
        FROM
            #Account_Meter_Address sub
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = sub.Account_Id
                   AND  cha.Meter_Id = sub.Meter_Id
        WHERE
            cha.Meter_Country_Name != sub.Country_Name;

        --34 Rate_Name
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Rate_Name'
            , sub.Account_Id
            , sub.Meter_Id
            , sub.Meter_Rate_Name
            , cha.Rate_Name
        FROM
            #Account_Meter_Address sub
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = sub.Account_Id
                   AND  cha.Meter_Id = sub.Meter_Id
        WHERE
            ISNULL(cha.Rate_Name, '') != ISNULL(sub.Meter_Rate_Name, '');



        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Account_Id' AS Column_Name
            , Account_Id
            , Meter_Id
            , '' AS Source_Value
            , cha.Account_Id AS Target_Value
        FROM
            Core.Client_Hier_Account cha
        WHERE
            NOT EXISTS (SELECT  1 FROM  dbo.ACCOUNT acc WHERE   acc.ACCOUNT_ID = cha.Account_Id);

        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Meter_id' AS Column_Name
            , Account_Id
            , Meter_Id
            , '' AS Source_Value
            , cha.Meter_Id AS Target_Value
        FROM
            Core.Client_Hier_Account cha
        WHERE
            NOT EXISTS (SELECT  1 FROM  dbo.METER m WHERE   m.METER_ID = cha.Meter_Id);

        WITH Cte_Account_Meters
        AS (
               SELECT
                    ua.ACCOUNT_ID
                    , m.METER_ID
               FROM
                    dbo.ACCOUNT ua
                    JOIN dbo.METER m
                        ON m.ACCOUNT_ID = ua.ACCOUNT_ID
               UNION
               SELECT
                    samm.ACCOUNT_ID
                    , samm.METER_ID
               FROM
                    dbo.SUPPLIER_ACCOUNT_METER_MAP samm
           )
        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Account_Id_Meter_Id' AS Column_Name
            , Account_Id
            , Meter_Id
            , '' AS Source_Value
            , cha.Meter_Id AS Target_Value
        FROM
            Core.Client_Hier_Account cha
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                Cte_Account_Meters acc
                           WHERE
                                acc.ACCOUNT_ID = cha.Account_Id
                                AND acc.METER_ID = cha.Meter_Id);

        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Account_Number' AS Column_Name
            , cha.Account_Id
            , cha.Meter_Id
            , acc.ACCOUNT_NUMBER AS Source_Value
            , cha.Account_Number AS Target_Value
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN dbo.ACCOUNT acc
                ON acc.ACCOUNT_ID = cha.Account_Id
        WHERE
            ISNULL(acc.ACCOUNT_NUMBER, '') != ISNULL(cha.Account_Number, '');



        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Account_Number_Search' AS Column_Name
            , cha.Account_Id
            , cha.Meter_Id
            , acc.Account_Number_Search AS Source_Value
            , cha.Account_Number_Search AS Target_Value
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN dbo.ACCOUNT acc
                ON acc.ACCOUNT_ID = cha.Account_Id
        WHERE
            ISNULL(acc.Account_Number_Search, '') != ISNULL(cha.Account_Number_Search, '');



        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Meter_Number' AS Column_Name
            , cha.Account_Id
            , cha.Meter_Id
            , m.METER_NUMBER AS Source_Value
            , cha.Meter_Number AS Target_Value
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN dbo.METER m
                ON m.METER_ID = cha.Meter_Id
        WHERE
            ISNULL(m.METER_NUMBER, '') != ISNULL(cha.Meter_Number, '');



        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Meter_Number_Search' AS Column_Name
            , cha.Account_Id
            , cha.Meter_Id
            , m.Meter_Number_Search AS Source_Value
            , cha.Meter_Number_Search AS Target_Value
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN dbo.METER m
                ON m.METER_ID = cha.Meter_Id
        WHERE
            ISNULL(m.Meter_Number_Search, '') != ISNULL(cha.Meter_Number_Search, '');

        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Client_Hier_Id' AS Column_Name
            , cha.Account_Id
            , cha.Meter_Id
            , ch.Client_Hier_Id AS Source_Value
            , cha.Client_Hier_Id AS Target_Value
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN (   SELECT
                                util.ACCOUNT_ID
                                , m.METER_ID
                                , util.SITE_ID
                           FROM
                                dbo.ACCOUNT util
                                INNER JOIN dbo.ENTITY actType
                                    ON ACCOUNT_TYPE_ID = actType.ENTITY_ID
                                INNER JOIN dbo.METER m
                                    ON util.ACCOUNT_ID = m.ACCOUNT_ID
                           WHERE
                                actType.ENTITY_NAME = 'Utility'
                           UNION
                           SELECT
                                sup.ACCOUNT_ID
                                , samm.METER_ID
                                , util.SITE_ID
                           FROM
                                dbo.ACCOUNT sup
                                INNER JOIN dbo.ENTITY actType
                                    ON sup.ACCOUNT_TYPE_ID = actType.ENTITY_ID
                                INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                                    ON sup.ACCOUNT_ID = samm.ACCOUNT_ID
                                INNER JOIN dbo.METER m
                                    ON m.METER_ID = samm.METER_ID
                                LEFT JOIN dbo.ACCOUNT util
                                    ON m.ACCOUNT_ID = util.ACCOUNT_ID
                           WHERE
                                actType.ENTITY_NAME = 'supplier') ac
                ON ac.ACCOUNT_ID = cha.Account_Id
                   AND  ac.METER_ID = cha.Meter_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Site_Id = ac.SITE_ID
        WHERE
            cha.Client_Hier_Id != ch.Client_Hier_Id;


        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Account_Type' AS COlumn_Name
            , cha.Account_Id
            , cha.Meter_Id
            , ENTITY_NAME Source_Value
            , cha.Account_Type Target_Value
        FROM
            dbo.ACCOUNT acc
            JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = acc.ACCOUNT_ID
            JOIN dbo.ENTITY at
                ON at.ENTITY_ID = acc.ACCOUNT_TYPE_ID
        WHERE
            at.ENTITY_NAME != cha.Account_Type;




        INSERT INTO @Client_Hier_Account_Data_Mismatch
             (
                 Column_Name
                 , Account_ID
                 , Meter_ID
                 , Source_Value
                 , Target_Value
             )
        SELECT
            'Account_Not_Expected_Dt'
            , cha.Account_Id
            , cha.Meter_Id
            , a.NOT_EXPECTED_DATE
            , cha.Account_Not_Expected_Dt
        FROM
            dbo.ACCOUNT a
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = a.ACCOUNT_ID
                   AND  a.NOT_EXPECTED_DATE != cha.Account_Not_Expected_Dt;






        DROP TABLE #Account_Meter_Address;



        SELECT  * FROM  @Client_Hier_Account_Data_Mismatch;


    END;




GO

GRANT EXECUTE ON  [dbo].[Client_Hier_Account_Data_Verification] TO [CBMSApplication]
GO
