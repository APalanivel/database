
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Time_Zone_Sel]
           
DESCRIPTION:             
			To get service condition template details 
			
INPUT PARAMETERS:            
	Name						DataType			Default			Description  
---------------------------------------------------------------------------------  
	@Search_Str					VARCHAR(50)			NULL
	@Tz_Filtered_Module			VARCHAR(25)			'Sourcing'
    


OUTPUT PARAMETERS:
	Name						DataType			Default			Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  

	EXEC dbo.Time_Zone_Sel
	EXEC dbo.Time_Zone_Sel @Search_Str = 'india'
	EXEC dbo.Time_Zone_Sel  @Search_Str=  'IST'
	EXEC dbo.Time_Zone_Sel  @Search_Str = 'canada'
	EXEC dbo.Time_Zone_Sel  @Search_Str=  'EST'
	EXEC dbo.Time_Zone_Sel  @Search_Str ='UTC'

	EXEC dbo.Time_Zone_Sel @Tz_Filtered_Module ='ALL'
	EXEC dbo.Time_Zone_Sel  @Search_Str = 'india',@Tz_Filtered_Module ='ALL'
	EXEC dbo.Time_Zone_Sel  @Search_Str=  'IST',@Tz_Filtered_Module ='ALL'
	EXEC dbo.Time_Zone_Sel  @Search_Str = 'canada',@Tz_Filtered_Module ='ALL'
	EXEC dbo.Time_Zone_Sel  @Search_Str=  'EST',@Tz_Filtered_Module ='ALL'
	EXEC dbo.Time_Zone_Sel  @Search_Str ='UTC',@Tz_Filtered_Module ='ALL'
	
		
 AUTHOR INITIALS:            
	Initials	Name            
---------------------------------------------------------------------------------  
	RR			Raghu Reddy
	NR			Narayana Reddy

 MODIFICATIONS:
	Initials	Date		Modification
---------------------------------------------------------------------------------  
	RR			2016-03-02	Global Sourcing - Phase3 - GCS-478 Created
	NR			2017-03-01	MAINT-4853 Added Filtered Code table to show the sourcing timezones.
	
******/

CREATE  PROCEDURE [dbo].[Time_Zone_Sel]
      ( 
       @Search_Str VARCHAR(50) = NULL
      ,@Tz_Filtered_Module VARCHAR(25) = 'Sourcing' )
AS 
BEGIN

      SET NOCOUNT ON;
      
      SELECT
            @Search_Str = '%' + @Search_Str + '%';
      WITH  Cte_Zones
              AS ( SELECT
                        tz.Time_Zone_Id
                       ,tz.Time_Zone
                       ,tz.Time_Zone_Code
                       ,tz.GMT
                       ,tz.Is_Default
                       ,DENSE_RANK() OVER ( ORDER  BY CAST(REPLACE(GMT_Offset, ':', '') AS INT) DESC ) AS Zone_Order
                   FROM
                        dbo.Time_Zone tz
                   WHERE
                        ( @Search_Str IS NULL
                          OR ( tz.Time_Zone LIKE @Search_Str
                               OR tz.Time_Zone_Code LIKE @Search_Str
                               OR tz.GMT LIKE @Search_Str ) )
                        AND ( @Tz_Filtered_Module = 'ALL'
                              OR ( @Tz_Filtered_Module <> 'ALL'
                                   AND EXISTS ( SELECT
                                                      1
                                                FROM
                                                      dbo.Filtered_Time_Zone ftz
                                                      INNER JOIN dbo.Code c
                                                            ON ftz.Tz_Filtered_Module_Cd = c.Code_Id
                                                      INNER JOIN dbo.Codeset cs
                                                            ON c.Codeset_Id = cs.Codeset_Id
                                                WHERE
                                                      tz.Time_Zone_Id = ftz.Time_Zone_Id
                                                      AND c.Code_Value = @Tz_Filtered_Module
                                                      AND cs.Codeset_Name = 'TzFilteredModule' ) ) ))
            SELECT
                  tz.Time_Zone_Id
                 ,tz.Time_Zone
                 ,tz.Time_Zone_Code
                 ,tz.GMT
                 ,tz.Is_Default
            FROM
                  Cte_Zones tz
            ORDER BY
                  tz.Zone_Order
END;
;

;


;
GO

GRANT EXECUTE ON  [dbo].[Time_Zone_Sel] TO [CBMSApplication]
GO
