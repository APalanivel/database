SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******************************************************************************************************
NAME : dbo.[Site_Upd_For_Client_Data_Transfer]

DESCRIPTION: This procedure used to transfer sites from a source client to destination client.  

 INPUT PARAMETERS:      

 Name			DataType		Default			Description      
--------------------------------------------------------------------        

 @Message		XML   

 OUTPUT PARAMETERS:      

 Name   DataType  Default Description      
--------------------------------------------------------------------      

  USAGE EXAMPLES:
--------------------------------------------------------------------  
  
BEGIN TRAN  
	EXEC [Site_Upd_For_Client_Data_Transfer2]  
		@From_Site_Client_Hier_Id = 1146,
		@To_Site_Client_Hier_Id = 761560,
		@To_Division_Client_Hier_Id = 761605
ROLLBACK TRAN

AUTHOR INITIALS:      

 Initials		Name      
-------------------------------------------------------------------       
 MSV			Muhamed Shahid V

 MODIFICATIONS  

 Initials		Date			Modification  
--------------------------------------------------------------------  
 MSV			18 Jul 2019		Created 	
 
*****************************************************************************************************/
CREATE PROCEDURE [dbo].[Site_Upd_For_Client_Data_Transfer]
	(
		@From_Site_Client_Hier_Id INT,
		@To_Site_Client_Hier_Id INT,
		@To_Division_Client_Hier_Id INT
	)
AS
BEGIN
	
    SET NOCOUNT ON

    DECLARE
        @From_Site_Id INT
		,@Division_Id INT
        ,@Site_name VARCHAR(200)
        ,@Site_Id INT
        ,@User_Info_Id INT
		,@SITE_TYPE_ID INT
        ,@UBMSITE_ID VARCHAR(30)
        ,@SITE_PRODUCT_SERVICE VARCHAR(2000)
        ,@PRODUCTION_SCHEDULE VARCHAR(2000)
        ,@SHUTDOWN_SCHEDULE VARCHAR(2000)
        ,@IS_ALTERNATE_POWER BIT
        ,@IS_ALTERNATE_GAS BIT
        ,@DOING_BUSINESS_AS VARCHAR(200)
        ,@NAICS_CODE VARCHAR(30)
        ,@TAX_NUMBER VARCHAR(30)
        ,@DUNS_NUMBER VARCHAR(30)
        ,@Closed BIT
        ,@Closed_By_Id INTEGER
        ,@closed_date DATETIME
        ,@NOT_MANAGED BIT
        ,@CONTRACTING_ENTITY VARCHAR(200)
        ,@CLIENT_LEGAL_STRUCTURE VARCHAR(4000)
        ,@firstName VARCHAR(50)
        ,@lastName VARCHAR(50)
        ,@contactEmailId VARCHAR(50)
        ,@emailCC VARCHAR(400)
        ,@emailApproval BIT
        ,@dbViewApproval BIT
        ,@Analyst_Mapping_Cd INT
        ,@Weather_Station_Cd VARCHAR(10)
        ,@Site_Reference_Number VARCHAR(30)
		,@Is_Analyst_Mapping_Cd_Changed BIT
		,@Time_Zone NVARCHAR(50)
		,@Time_Zone_Id INT
		,@Site_name2 VARCHAR(200)
        ,@Closed2 BIT
        ,@closed_date2 DATETIME
        ,@NOT_MANAGED2 BIT
        ,@Division_Id2 INT
        ,@SITE_TYPE_ID2 INT
        ,@Analyst_Mapping_Cd2 BIT
        ,@Weather_Station_Cd2 VARCHAR(10)
        ,@Site_Reference_Number2 VARCHAR(30)
		,@Time_Zone_Id2 INT
		
    SELECT
        @User_Info_Id = ui.User_Info_Id
    FROM
        dbo.USER_INFO AS ui
    WHERE
        ui.username = 'conversion'

	BEGIN TRY
		BEGIN TRAN;
		
		SELECT	@From_Site_Id = ch.Site_Id
		FROM Core.Client_Hier ch 
		WHERE ch.Client_Hier_Id = @From_Site_Client_Hier_Id    
		
		SELECT	@Site_Id = ch.Site_Id
		FROM Core.Client_Hier ch
		WHERE ch.Client_Hier_Id = @To_Site_Client_Hier_Id      
		
		SELECT @Division_Id = ch.Sitegroup_Id
		FROM Core.Client_Hier ch
		WHERE ch.Client_Hier_Id = @To_Division_Client_Hier_Id    
		    
        SELECT
            @Site_Name = SITE_NAME
            ,@SITE_TYPE_ID = SITE_TYPE_ID
            ,@UBMSITE_ID = UBMSITE_ID
            ,@SITE_PRODUCT_SERVICE = SITE_PRODUCT_SERVICE
            ,@PRODUCTION_SCHEDULE = PRODUCTION_SCHEDULE
            ,@SHUTDOWN_SCHEDULE = SHUTDOWN_SCHEDULE
            ,@IS_ALTERNATE_POWER = IS_ALTERNATE_POWER
            ,@IS_ALTERNATE_GAS = IS_ALTERNATE_GAS
            ,@DOING_BUSINESS_AS = DOING_BUSINESS_AS
            ,@NAICS_CODE = NAICS_CODE
            ,@TAX_NUMBER = TAX_NUMBER
            ,@DUNS_NUMBER = DUNS_NUMBER
            ,@Closed = CLOSED
			,@Closed_By_Id = CLOSED_BY_ID
			,@closed_date = CLOSED_DATE
			,@NOT_MANAGED = NOT_MANAGED
            ,@CONTRACTING_ENTITY = CONTRACTING_ENTITY
            ,@CLIENT_LEGAL_STRUCTURE = CLIENT_LEGAL_STRUCTURE
			,@firstName = LP_CONTACT_FIRST_NAME
            ,@lastName = LP_CONTACT_LAST_NAME
            ,@contactEmailId = LP_CONTACT_EMAIL_ADDRESS
            ,@emailCC = LP_CONTACT_CC
            ,@emailApproval = IS_PREFERENCE_BY_EMAIL
            ,@dbViewApproval = IS_PREFERENCE_BY_DV
            ,@Analyst_Mapping_Cd = Analyst_Mapping_Cd
            ,@Weather_Station_Cd = Weather_Station_Code
            ,@Site_Reference_Number = SITE_REFERENCE_NUMBER
			,@Time_Zone_Id = Time_Zone_Id
        FROM
            dbo.SITE
        WHERE
            SITE_ID = @From_Site_Id		
			
		SELECT	@Site_name2 = s.SITE_NAME
				,@Closed2 = s.CLOSED
				,@closed_date2 = s.CLOSED_DATE
				,@NOT_MANAGED2 = s.NOT_MANAGED
				,@Division_Id2 = s.DIVISION_ID
				,@SITE_TYPE_ID2 = s.SITE_TYPE_ID
				,@Analyst_Mapping_Cd2 = s.Analyst_Mapping_Cd
				,@Weather_Station_Cd2 = s.Weather_Station_Code
				,@Site_Reference_Number2 = s.SITE_REFERENCE_NUMBER
				,@Time_Zone_Id2 = s.Time_Zone_Id
		FROM
            dbo.SITE s
        WHERE
            SITE_ID = @Site_Id	
				
		
		UPDATE a
        SET
            a.ADDRESS_TYPE_ID = a2.ADDRESS_TYPE_ID
            ,a.STATE_ID = a2.STATE_ID
            ,a.ADDRESS_LINE1 = a2.ADDRESS_LINE1
            ,a.ADDRESS_LINE2 = a2.ADDRESS_LINE2
            ,a.CITY = a2.CITY
            ,a.ZIPCODE = a2.ZIPCODE
            ,a.PHONE_NUMBER = a2.PHONE_NUMBER
            ,a.IS_PRIMARY_ADDRESS = a2.IS_PRIMARY_ADDRESS
            ,a.ADDRESS_PARENT_ID = s.site_Id
            ,a.ADDRESS_PARENT_TYPE_ID = a2.ADDRESS_PARENT_TYPE_ID
            ,a.IS_HISTORY = a2.IS_HISTORY
            ,a.GEO_LAT = a2.GEO_LAT
            ,a.GEO_LONG = a2.GEO_LONG
            ,a.SQUARE_FOOTAGE = a2.SQUARE_FOOTAGE
            ,a.Is_System_Generated_Geocode = a2.Is_System_Generated_Geocode
        FROM
            site s
            INNER JOIN dbo.ADDRESS a
                    ON A.ADDRESS_ID = S.PRIMARY_ADDRESS_ID
            INNER JOIN site s2
                    ON s2.site_Id = @From_Site_Id
            INNER JOIN dbo.ADDRESS a2
                    ON a2.ADDRESS_ID = s2.PRIMARY_ADDRESS_ID
        WHERE
            s.site_id = @Site_Id
		AND (a.ADDRESS_LINE1 <> a2.ADDRESS_LINE1
				OR a.ADDRESS_LINE2 <> a2.ADDRESS_LINE1
				OR a.CITY <> a2.CITY
				OR a.ZIPCODE <> a2.ZIPCODE
				OR a.State_ID <> a2.State_ID
				OR ISNULL(a.GEO_LAT,0) <> ISNULL(a2.GEO_LAT,0)
				OR ISNULL(a.GEO_LONG,0) <> ISNULL(a2.GEO_LONG,0)
			)
		
        SET @Is_Analyst_Mapping_Cd_Changed = 0

        SELECT
            @Is_Analyst_Mapping_Cd_Changed = 1
        FROM
            dbo.SITE s
            INNER JOIN dbo.SITE s1
                    ON s1.Site_Id = @Site_Id
            AND s.Site_Id = @From_Site_Id
                        AND s.Analyst_Mapping_Cd <> s1.Analyst_Mapping_Cd        
						
		SELECT @Time_Zone = Time_Zone
		FROM dbo.Time_Zone
		WHERE Time_Zone_Id = @Time_Zone_Id
		
		IF ( @Site_name <> @Site_name2 
				OR @Closed <> @Closed2
				OR @closed_date <> @closed_date2
				OR @NOT_MANAGED <> @NOT_MANAGED2
				OR @Division_Id <> @Division_Id2
				OR @SITE_TYPE_ID <> @SITE_TYPE_ID2
				OR ISNULL(@Analyst_Mapping_Cd,0) <> ISNULL(@Analyst_Mapping_Cd2,0)
				OR ISNULL(@Site_Reference_Number,'') <> ISNULL(@Site_Reference_Number2,'')
			)
		BEGIN    
		
			EXEC dbo.GET_UPDATE_SITE_INFO_P
				@site_type_id = @Site_Type_Id
				,@site_name = @Site_Name
				,@division_id = @Division_Id
				,@ubmsite_id = @UBMSITE_ID
				,@site_reference_number = @site_reference_number
				,@site_product_service = @site_product_service
				,@production_schedule = @production_schedule
				,@shutdown_schedule = @shutdown_schedule
				,@is_alternate_power = @is_alternate_power
				,@is_alternate_gas = @is_alternate_gas
				,@doing_business_as = @doing_business_as
				,@naics_code = @naics_code
				,@tax_number = @tax_number
				,@duns_number = @duns_number
				,@closed = @closed
				,@closed_date = @closed_date
				,@not_managed = @not_managed
				,@closed_by_id = @closed_by_id
				,@user_info_id = @user_info_id
				,@contracting_entity = @contracting_entity
				,@client_legal_structure = @client_legal_structure
				,@firstName = @firstName
				,@lastName = @lastName
				,@contactEmailId = @contactEmailId
				,@emailCC = @emailCC
				,@emailApproval = @emailApproval
				,@dbViewApproval = @dbViewApproval
				,@site_id = @Site_Id
				,@Analyst_Mapping_Cd = @Analyst_Mapping_Cd
				,@Is_Analyst_Mapping_Cd_Changed = @Is_Analyst_Mapping_Cd_Changed

				 EXEC dbo.ADD_ENTITY_AUDIT_ITEM_P
					@entity_identifier = @Site_Id
					,@user_info_id = @User_Info_Id
					,@audit_type = 2
					,@entity_name = 'SITE_TABLE'
					,@entity_type = 500
		END
   
		IF ( ISNULL(@Weather_Station_Cd,0) <> ISNULL(@Weather_Station_Cd2,0)
				OR ISNULL(@Time_Zone_Id,0) <> ISNULL(@Time_Zone_Id2,0)
			)
		BEGIN  
			EXEC dbo.Site_Upd_Weather_Station_Cd
				@Site_Id = @Site_Id
				,@Weather_Station_Cd = @Weather_Station_Cd
				,@Time_Zone = @Time_Zone
		END

		COMMIT TRAN;
	END TRY
	BEGIN CATCH

		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRAN;
		END;

		EXEC dbo.usp_RethrowError;
	END CATCH;
END;
GO
GRANT EXECUTE ON  [dbo].[Site_Upd_For_Client_Data_Transfer] TO [CBMSApplication]
GO
