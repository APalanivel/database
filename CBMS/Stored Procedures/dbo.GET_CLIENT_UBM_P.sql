SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE dbo.GET_CLIENT_UBM_P
	@ubm_service_id int
	AS
	begin
		set nocount on

		select  a.ubm_name,
			a.ubm_id 
		from 	ubm a 
	     		join ubm_service b on b.ubm_id = a.ubm_id
			and b.ubm_service_id = @ubm_service_id

	end
GO
GRANT EXECUTE ON  [dbo].[GET_CLIENT_UBM_P] TO [CBMSApplication]
GO
