SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        dbo.Cbms_Sitegroup_Del                    
                      
Description:                      
        To get site's hedge configurations
                      
Input Parameters:                      
    Name							DataType        Default     Description                        
--------------------------------------------------------------------------------
	@Cbms_Sitegroup_Id				INT
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	  
                    
Usage Examples:                          
--------------------------------------------------------------------------------
	
		EXEC dbo.Cbms_Sitegroup_Del 657
                    
 Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy   
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			2018-09-29     Global Risk Management - Created 
                     
******/
CREATE PROCEDURE [dbo].[Cbms_Sitegroup_Del]
      ( 
       @Cbms_Sitegroup_Id INT
      ,@User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;
      
      DECLARE
            @Client_Name VARCHAR(200)
           ,@Cbms_Sitegroup VARCHAR(200)
           ,@Audit_Function SMALLINT = 1
           ,@Lookup_Value XML
           ,@Current_Ts DATETIME = GETDATE()
           ,@User_Name VARCHAR(100)
           ,@Application_Name VARCHAR(50);

      SELECT
            @Application_Name = ISNULL(@Application_Name, 'CBMS Group Delete')
            
            
      SELECT
            @User_Name = ui.USERNAME
      FROM
            dbo.USER_INFO ui
      WHERE
            ui.USER_INFO_ID = @User_Info_Id;
      SELECT
            @Client_Name = ch.Client_Name
           ,@Cbms_Sitegroup = rg.Group_Name
      FROM
            dbo.Cbms_Sitegroup rg
            INNER JOIN Core.Client_Hier ch
                  ON ch.Client_Id = rg.CLIENT_ID
      WHERE
            ch.Sitegroup_Id = 0
            AND ch.Site_Id = 0
            AND rg.Cbms_Sitegroup_Id = @Cbms_Sitegroup_Id;

      SET @Lookup_Value = ( SELECT
                              @Cbms_Sitegroup_Id Cbms_Sitegroup_Id
                             ,@Cbms_Sitegroup Cbms_Sitegroup
            FOR
                            XML RAW );

     
      DELETE
            rgd
      FROM
            dbo.Cbms_Sitegroup_Participant rgd
      WHERE
            rgd.Cbms_Sitegroup_Id = @Cbms_Sitegroup_Id
            
      DELETE
            pv
      FROM
            dbo.CBMS_Sitegroup_Rule rgd
            INNER JOIN dbo.Cbms_Sitegroup_Rule_Param_Value pv
                  ON rgd.CBMS_Sitegroup_Rule_Id = pv.CBMS_Sitegroup_Rule_Id
      WHERE
            rgd.Cbms_Sitegroup_Id = @Cbms_Sitegroup_Id
            
      DELETE
            rgd
      FROM
            dbo.CBMS_Sitegroup_Rule rgd
      WHERE
            rgd.Cbms_Sitegroup_Id = @Cbms_Sitegroup_Id
            
      DELETE
            rgd
      FROM
            dbo.Cbms_Sitegroup rgd
      WHERE
            rgd.Cbms_Sitegroup_Id = @Cbms_Sitegroup_Id


      EXEC dbo.Application_Audit_Log_Ins 
            @Client_Name
           ,@Application_Name
           ,@Audit_Function
           ,'RM_GROUP'
           ,@Lookup_Value
           ,@User_Name
           ,@Current_Ts;
      
     
                          
                          
                          
      
                          
                               
                               
END;
GO
GRANT EXECUTE ON  [dbo].[Cbms_Sitegroup_Del] TO [CBMSApplication]
GO
