SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[seSupplyDemandOverview_GetAll]
AS
BEGIN
	set nocount on
	   select SupplyDemandOverviewId
		, YearNumber
		, YearLabel
		, DryProduction
		, NetImports
		, SupplementalGasFuels
		, TotalGasSupply
		, ResidentialCommercial
		, Industrial 
		, PowerGeneration
		, OtherConsumption
		, TotalConsumption
		, Imbalance
		, BalanceFactor
		, TotalChange
	     from seSupplyDemandOverview
	 order by YearNumber
END
GO
GRANT EXECUTE ON  [dbo].[seSupplyDemandOverview_GetAll] TO [CBMSApplication]
GO
