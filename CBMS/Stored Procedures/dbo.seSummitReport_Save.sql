SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE          Procedure dbo.seSummitReport_Save
	( @ReportId int = null
	, @ReportTypeId int
	, @ReportDate datetime
	, @Headline varchar(255)
	, @DirFileName varchar(255)
	, @ContentTypeId int
	, @HideFromDemo bit
	, @ClientName varchar(50) = null
	, @cbms_image_id int = null
	)
As
BEGIN
	set nocount on

	declare @ThisId int
	declare @StatusId int

	
	set @StatusId = dbo.dmlLookup_GetId ('Content Status', 'Approved')

	set @ThisId = @ReportId

	if @ThisId is null
	begin

		insert into seSummitReport
			( ReportTypeId
			, ReportDate
			, Headline
			, DirFileName
			, StatusId
			, ContentTypeId
			, HideFromDemo
			, ClientName
			, cbms_image_id
			)
		values
			( @ReportTypeId
			, @ReportDate
			, @Headline
			, @DirFileName
			, @StatusId
			, @ContentTypeId
			, @HideFromDemo
			, @ClientName
			, @cbms_image_id 
			)
	
		set @ThisId = @@IDENTITY



	end
	else
	begin


		update seSummitReport
	           set ReportTypeId = @ReportTypeId
			, ReportDate = @ReportDate
			, Headline = @Headline
			, DirFileName = @DirFileName
			, ContentTypeId = @ContentTypeId
			, HideFromDemo = @HideFromDemo
			, ClientName = @ClientName
			, cbms_image_id = @cbms_image_id 
		   where ReportId = @ThisId

	end
	exec seSummitReport_Get @ThisId

END
GO
GRANT EXECUTE ON  [dbo].[seSummitReport_Save] TO [CBMSApplication]
GO
