SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Client_Commodity_Variance_Test_Config_Sel

DESCRIPTION:
	To get the details of Variance test config in Variance test config page by just passing @Client_Id or @Client_Id and @Client_Commodity_Id.
	To get the details of the variance in save and submit page(Variance test to run) then @Client_Id,@Commodity_Id and @Service_Month.
	

INPUT PARAMETERS:
	Name								DataType		Default	Description
---------------------------------------------------------------
@Client_Id									INT
@Client_Commodity_Id						INT   NULL
@Commodity_Id								INT   NULL
@Service_Month								DATE  NULL


OUTPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	
USAGE EXAMPLES:
------------------------------------------------------------
    
		EXEC dbo.Client_Commodity_Variance_Test_Config_Sel 
			@Client_Id=11278
			,@Client_Commodity_Id = 1528

		EXEC dbo.Client_Commodity_Variance_Test_Config_Sel 
			@Client_Id=11278
			,@Commodity_Id = 290
			,@Service_Month='2016-11-02'
      
		EXEC dbo.Client_Commodity_Variance_Test_Config_Sel 
			@Client_Id=11278
			,@Commodity_Id = 290
			,@Service_Month='2016-12-02'
			
		EXEC dbo.Client_Commodity_Variance_Test_Config_Sel 
			@Client_Id=11278
			,@Commodity_Id = 290
			,@Service_Month='2019-12-02'
			
		EXEC dbo.Client_Commodity_Variance_Test_Config_Sel 
			@Client_Id=11278
			,@Commodity_Id = 290
			,@Service_Month='2004-12-02'
			
		EXEC dbo.Client_Commodity_Variance_Test_Config_Sel 13769,NULL,290
		EXEC dbo.Client_Commodity_Variance_Test_Config_Sel 13769,NULL,290,'2004-12-02'
		EXEC dbo.Client_Commodity_Variance_Test_Config_Sel 13769,NULL,290,'2016-03-01'
      
AUTHOR INITIALS:

	Initials	Name
------------------------------------------------------------
SP				Sandeep Pigilam
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	SP        2016-11-01	Created for Variance Test EnhancementS Phase II.

******/                                        
                                       
CREATE PROCEDURE [dbo].[Client_Commodity_Variance_Test_Config_Sel]
      ( 
       @Client_Id INT
      ,@Client_Commodity_Id INT = NULL
      ,@Commodity_Id INT = NULL
      ,@Service_Month DATE = NULL )
AS 
BEGIN         
      SET NOCOUNT ON;
      
      DECLARE @Tbl_Tests AS TABLE
            ( 
             Client_Commodity_Variance_Test_Config_Id INT
            ,Client_Commodity_Id INT
            ,Commodity_Name VARCHAR(50)
            ,Code_Id INT
            ,Code_Value VARCHAR(25)
            ,Start_Dt DATE
            ,End_Dt DATE )
           
      INSERT      INTO @Tbl_Tests
                  ( 
                   Client_Commodity_Variance_Test_Config_Id
                  ,Client_Commodity_Id
                  ,Commodity_Name
                  ,Code_Id
                  ,Code_Value
                  ,Start_Dt
                  ,End_Dt )
                  SELECT
                        acirt.Client_Commodity_Variance_Test_Config_Id
                       ,cc.Client_Commodity_Id
                       ,com.Commodity_Name
                       ,c.Code_Id
                       ,c.Code_Value
                       ,acirt.Start_Dt
                       ,acirt.End_Dt
                  FROM
                        dbo.Client_Commodity_Variance_Test_Config acirt
                        INNER JOIN Core.Client_Commodity cc
                              ON acirt.Client_Commodity_Id = cc.Client_Commodity_Id
                        INNER JOIN dbo.Code hl
                              ON hl.Code_Id = cc.Hier_Level_Cd
                        INNER JOIN dbo.Commodity com
                              ON cc.Commodity_ID = com.Commodity_Id
                        INNER JOIN dbo.Code c
                              ON acirt.Variance_Test_Type_Cd = c.Code_Id
                        INNER JOIN dbo.Code csc
                              ON csc.Code_Id = cc.Commodity_Service_Cd
                  WHERE
                        cc.Client_Id = @Client_Id
                        AND ( @Client_Commodity_Id IS NULL
                              OR cc.Client_Commodity_Id = @Client_Commodity_Id )
                        AND hl.Code_Value = 'Account'
                        AND csc.Code_Value = 'Invoice'
                        AND ( @Commodity_Id IS NULL
                              OR com.Commodity_Id = @Commodity_Id )
                        AND ( @Service_Month IS NULL
                              OR ( @Service_Month IS NOT NULL
                                   AND acirt.End_Dt IS NOT NULL
                                   AND @Service_Month BETWEEN acirt.Start_Dt
                                                      AND     acirt.End_Dt )
                              OR ( @Service_Month IS NOT NULL
                                   AND acirt.End_Dt IS NULL
                                   AND @Service_Month >= acirt.Start_Dt ) )
                  ORDER BY
                        CASE WHEN com.Commodity_Name = 'Electric Power' THEN 1
                             WHEN com.Commodity_Name = 'Natural Gas' THEN 2
                             ELSE 3
                        END
                       ,com.Commodity_Name
                       ,acirt.Start_Dt
                       
      INSERT      INTO @Tbl_Tests
                  ( 
                   Client_Commodity_Variance_Test_Config_Id
                  ,Client_Commodity_Id
                  ,Commodity_Name
                  ,Code_Id
                  ,Code_Value
                  ,Start_Dt
                  ,End_Dt )
                  SELECT
                        NULL AS Client_Commodity_Variance_Test_Config_Id
                       ,NULL AS Client_Commodity_Id
                       ,com.Commodity_Name
                       ,cd.Code_Id
                       ,cd.Code_Value
                       ,NULL AS Start_Dt
                       ,NULL AS End_Dt
                  FROM
                        dbo.Code cd
                        INNER JOIN dbo.Codeset cs
                              ON cd.Codeset_Id = cs.Codeset_Id
                        CROSS JOIN dbo.Commodity com
                  WHERE
                        EXISTS ( SELECT
                                    1
                                 FROM
                                    Core.Client_Commodity cc
                                 WHERE
                                    cc.Client_Id = @Client_Id
                                    AND ( @Commodity_Id IS NULL
                                          OR cc.Commodity_Id = @Commodity_Id )
                                    AND com.Commodity_Id = cc.Commodity_Id )
                        AND cs.Codeset_Name = 'VarianceTestType'
                        AND cd.Code_Value = 'DEO Variance Testing'
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          @Tbl_Tests )
                                          
      SELECT
            Client_Commodity_Variance_Test_Config_Id
           ,Client_Commodity_Id
           ,Commodity_Name
           ,Code_Id
           ,Code_Value
           ,Start_Dt
           ,End_Dt
      FROM
            @Tbl_Tests
                 
    
END



;
GO
GRANT EXECUTE ON  [dbo].[Client_Commodity_Variance_Test_Config_Sel] TO [CBMSApplication]
GO
