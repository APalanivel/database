SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    Trade.Deal_Ticket_Site_Name_Dtl_Sel
   
    
DESCRIPTION:   
   
  This procedure to get site name details for deal ticket.
    
INPUT PARAMETERS:    
      Name				DataType       Default        Description    
-----------------------------------------------------------------------------    
	@Deal_Ticket_Id		INT
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec Trade.Deal_Ticket_Site_Name_Dtl_Sel  131861
	Exec Trade.Deal_Ticket_Site_Name_Dtl_Sel  131454,255
	Exec Trade.Deal_Ticket_Site_Name_Dtl_Sel  131454,260

       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	SP          SrinivasaRao Patchava    
    
MODIFICATIONS     
	Initials    Date        Modification      
-------------------------------------------------------------------------------------------------------------------       
	SP          2018-01-22  Global Risk Management - Create sp to get site name details for deal ticket
     
    
******/

CREATE PROCEDURE [Trade].[Deal_Ticket_Site_Name_Dtl_Sel]
      ( 
       @Deal_Ticket_Id INT
      ,@Trade_Id INT = NULL )
AS 
BEGIN
      SET NOCOUNT ON;


      SELECT
            RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
           ,ch.Client_Id
           ,ch.Sitegroup_Id
           ,ch.Site_Id
      FROM
            Trade.Deal_Ticket_Client_Hier dtch
            INNER JOIN Core.Client_Hier ch
                  ON ch.Client_Hier_Id = dtch.Client_Hier_Id
      WHERE
            dtch.Deal_Ticket_Id = @Deal_Ticket_Id
            AND ( @Trade_Id IS NULL
                  OR EXISTS ( SELECT
                                    1
                              FROM
                                    Trade.Deal_Ticket_Client_Hier_TXN_Status dtchts
                                    INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status dtchws
                                          ON dtchts.Deal_Ticket_Client_Hier_Workflow_Status_Id = dtchws.Deal_Ticket_Client_Hier_Workflow_Status_Id
                              WHERE
                                    dtchts.Cbms_Trade_Number = @Trade_Id
                                    AND dtchws.Deal_Ticket_Client_Hier_Id = dtch.Deal_Ticket_Client_Hier_Id ) );


END;



GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Site_Name_Dtl_Sel] TO [CBMSApplication]
GO
