
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsCostUsage_GetNonSystemForAccount

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Account_ID    INT       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
USE CBMS
	exec dbo.cbmsCostUsage_GetNonSystemForAccount 38009
	exec dbo.cbmsCostUsage_GetNonSystemForAccount 19536
	exec dbo.cbmsCostUsage_GetNonSystemForAccount 105994
	exec dbo.cbmsCostUsage_GetNonSystemForAccount 93605
	exec dbo.cbmsCostUsage_GetNonSystemForAccount 44709

AUTHOR INITIALS:
	Initials	 Name
------------------------------------------------------------
	AP		 Athmaram Pabbathi

MODIFICATIONS:
	Initials	Date		   Modification
------------------------------------------------------------
    AP		 08/01/2011  Modified the SP logic and Remove Cost_Usage table & use Cost_Usage_Account_Dtl, Bucket_Master, Commodity tables
					   Removed Tax_Rate column since it is not used in application.
    AP		 08/22/2011  Removed filter on Bucket_Name and added BM.Is_Shown_On_Account = 1 filter & Added qualifiers to all objects with the owner name
******/
CREATE PROCEDURE [dbo].[cbmsCostUsage_GetNonSystemForAccount] ( @Account_ID INT )
AS 
BEGIN

      SET NOCOUNT ON ;

      SELECT
            CUAD.Cost_Usage_Account_Dtl_Id AS Cost_Usage_ID
           ,CUAD.Account_ID
           ,cha.Site_ID
           ,cha.Site_Name
           ,CUAD.Service_Month
           ,CUAD.Created_By_ID
           ,UI.UserName
           ,CUAD.Created_Ts AS Created_Date
           ,CUAD.Currency_Unit_ID
           ,CU.Currency_Unit_Name
           ,CUAD.UOM_Type_Id
           ,UOM.Entity_Name AS UOM_Type_Name
           ,BM.Bucket_Name
           ,CD.Code_Value AS Bucket_Type
           ,CUAD.Bucket_Value
           ,CUAD.Updated_Ts AS Updated_Date
           ,COM.Commodity_Name
      FROM
            dbo.Cost_Usage_Account_Dtl CUAD
            INNER JOIN dbo.Bucket_Master BM
                  ON BM.Bucket_Master_Id = CUAD.Bucket_Master_Id
            INNER JOIN dbo.Commodity COM
                  ON COM.Commodity_Id = BM.Commodity_Id
            INNER JOIN ( SELECT
                              ch.Client_Hier_Id
                             ,ch.Site_Id
                             ,rtrim(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' AS Site_Name
                             ,cha.Account_Id
                             ,cha.Commodity_Id
                         FROM
                              Core.Client_Hier ch
                              INNER JOIN Core.Client_Hier_Account cha
                                    ON cha.Client_Hier_Id = ch.Client_Hier_Id
                         WHERE
                              cha.Account_Id = @Account_ID
                         GROUP BY
                              ch.Client_Hier_Id
                             ,ch.Site_Id
                             ,rtrim(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_Name + ')'
                             ,cha.Account_Id
                             ,cha.Commodity_Id ) cha
                  ON cha.Account_Id = cuad.Account_Id
                     AND cha.Client_Hier_Id = cuad.Client_Hier_Id
                     AND cha.Commodity_Id = com.Commodity_Id
            INNER JOIN dbo.User_Info UI
                  ON UI.User_Info_ID = CUAD.Created_By_ID
            INNER JOIN dbo.Code CD
                  ON CD.Code_Id = BM.Bucket_Type_Cd
            LEFT OUTER JOIN dbo.Entity UOM
                  ON UOM.Entity_ID = CUAD.UOM_Type_Id
            LEFT OUTER JOIN dbo.Currency_Unit CU
                  ON CU.Currency_Unit_ID = CUAD.Currency_Unit_ID
      WHERE
            COM.Commodity_Name IN ( 'Natural Gas', 'Electric Power' )
            AND BM.Is_Category = 1
            AND BM.Is_Shown_On_Account = 1	   

END
;
GO

GRANT EXECUTE ON  [dbo].[cbmsCostUsage_GetNonSystemForAccount] TO [CBMSApplication]
GO
