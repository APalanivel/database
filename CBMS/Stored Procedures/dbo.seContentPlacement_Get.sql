SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure dbo.seContentPlacement_Get
	(
		@ContentPlacementId int
	)
As
BEGIN
	set nocount on
	 select ContentPlacementId
				, ContentPageId
				, PlacementLabel
		 from seContentPlacement
		where ContentPlacementId = @ContentPlacementId

END
GO
GRANT EXECUTE ON  [dbo].[seContentPlacement_Get] TO [CBMSApplication]
GO
