SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
 NAME: dbo.Invoice_Collection_Issue_Event_Sel_Invoice_Collection_Activity_Id_Account_Id          
                          
 DESCRIPTION:                          
   To get Invoice_Collection_Issue_Event_desc        
                          
 INPUT PARAMETERS:            
                       
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
@Invoice_Collection_Issue_Event_Id INT  
  
                               
 OUTPUT PARAMETERS:            
                             
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
                          
 USAGE EXAMPLES:                              
---------------------------------------------------------------------------------------------------------------                              
  
			  exec [dbo].[Invoice_Collection_Issue_Event_Sel_Invoice_Collection_Activity_Id_Account_Id] 58,1114432,'sdsad',102311                      
                         
 AUTHOR INITIALS:          
         
 Initials              Name          
---------------------------------------------------------------------------------------------------------------                        
RKV      Ravi kumar vegesna                           
  
 MODIFICATIONS:        
            
 Initials              Date             Modification        
---------------------------------------------------------------------------------------------------------------        
 RKV                   2018-02-03       SE2017-273,Created           
                        
******/      
  
CREATE PROCEDURE [dbo].[Invoice_Collection_Issue_Event_Sel_Invoice_Collection_Activity_Id_Account_Id]
      ( 
       @Invoice_Collection_Issue_Event_Ids VARCHAR(MAX)
      ,@Start_Index INT = 1
      ,@End_Index INT = 2147483647 )
AS 
BEGIN                  
      SET NOCOUNT ON;    
  
        
  
      DECLARE @Total_Count INT 
      CREATE TABLE #Invoice_Collection_Queue_Cnt
            ( 
             Invoice_Collection_Queue_Id INT
            ,Open_Records_Cnt INT )
      
      SELECT TOP ( @End_Index )
            cha.Display_Account_Number
           ,CAST(icq.Collection_Start_Dt AS VARCHAR(15)) + ' / ' + CAST(icq.Collection_End_Dt AS VARCHAR(15)) Period_To_Chase
           ,ie.Event_Desc
           ,ie.Invoice_Collection_Issue_Event_Id
           ,ica.Invoice_Collection_Activity_Id
           ,ROW_NUMBER() OVER ( ORDER BY cha.Display_Account_Number ) AS Row_Num
           ,COUNT(1) OVER ( ) Total_Count
           ,ie.Created_Ts
           ,ie.Last_Change_Ts
           ,icq.Invoice_Collection_Queue_Id
      INTO
            #Invoice_Collection_Issue_Event
      FROM
            dbo.Invoice_Collection_Issue_Log icil
            INNER JOIN dbo.Invoice_Collection_Activity ica
                  ON icil.Invoice_Collection_Activity_Id = ica.Invoice_Collection_Activity_Id
            INNER JOIN Invoice_Collection_Issue_Event ie
                  ON icil.Invoice_Collection_Issue_Log_Id = ie.Invoice_Collection_Issue_Log_Id
            INNER JOIN dbo.Invoice_Collection_Queue icq
                  ON icq.Invoice_Collection_Queue_Id = icil.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                  ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN core.Client_Hier_Account cha
                  ON cha.Account_Id = icac.Account_Id
      WHERE
            EXISTS ( SELECT
                        1
                     FROM
                        dbo.ufn_split(@Invoice_Collection_Issue_Event_Ids, ',') ufn
                     WHERE
                        ufn.Segments = ie.Invoice_Collection_Issue_Event_Id )
      GROUP BY
            cha.Display_Account_Number
           ,icq.Collection_Start_Dt
           ,icq.Collection_End_Dt
           ,ie.Event_Desc
           ,ie.Invoice_Collection_Issue_Event_Id
           ,ica.Invoice_Collection_Activity_Id
           ,ie.Created_Ts
           ,ie.Last_Change_Ts
           ,icq.Invoice_Collection_Queue_Id
            
      INSERT      INTO #Invoice_Collection_Queue_Cnt
                  ( 
                   Invoice_Collection_Queue_Id
                  ,Open_Records_Cnt )
                  SELECT
                        icil.Invoice_Collection_Queue_Id
                       ,COUNT(icie.Invoice_Collection_Issue_Event_Id)
                  FROM
                        dbo.Invoice_Collection_Issue_Log icil
                        INNER JOIN dbo.Invoice_Collection_Issue_Event icie
                              ON icil.Invoice_Collection_Issue_Log_Id = icie.Invoice_Collection_Issue_Log_Id
                        INNER JOIN #Invoice_Collection_Issue_Event icq
                              ON icq.Invoice_Collection_Queue_Id = icil.Invoice_Collection_Queue_Id
                        INNER JOIN dbo.Code c
                              ON Issue_Event_Type_Cd = c.Code_Id
                  WHERE
                        c.Code_Value IN ( 'IC External Comments', 'IC Internal Comments' )
                  GROUP BY
                        icil.Invoice_Collection_Queue_Id
      
      SELECT
            cicie.Display_Account_Number
           ,cicie.Period_To_Chase
           ,cicie.Event_Desc
           ,cicie.Invoice_Collection_Issue_Event_Id
           ,cicie.Invoice_Collection_Activity_Id
           ,@Total_Count Total_Count
           ,cicie.Row_Num
           ,cicie.Created_Ts
           ,cicie.Last_Change_Ts
           ,icqc.Open_Records_Cnt
      FROM
            #Invoice_Collection_Issue_Event cicie
            INNER JOIN #Invoice_Collection_Queue_Cnt icqc
                  ON cicie.Invoice_Collection_Queue_Id = icqc.Invoice_Collection_Queue_Id
      WHERE
            cicie.Row_Num BETWEEN @Start_Index AND @End_Index
      ORDER BY
            Row_Num 
                  
                  
      DROP TABLE #Invoice_Collection_Issue_Event  
END;  
  
  
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Issue_Event_Sel_Invoice_Collection_Activity_Id_Account_Id] TO [CBMSApplication]
GO
