SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.Client_Comment_Sel_By_Client_Id

DESCRIPTION:

INPUT PARAMETERS:
	Name					DataType		Default	Description
---------------------------------------------------------------
	@Client_Id				INT
	

OUTPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	
USAGE EXAMPLES:
------------------------------------------------------------

exec dbo.Client_Comment_Sel_By_Client_Id 11819
exec dbo.Client_Comment_Sel_By_Client_Id 11803
exec dbo.Client_Comment_Sel_By_Client_Id 11807


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	BCH			Balaraju
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	 BCH       	2012-09-03	Created

******/
CREATE PROCEDURE dbo.Client_Comment_Sel_By_Client_Id ( @Client_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;
      SELECT
            com.Comment_ID
           ,com.Comment_Text
           ,cd.Code_Value AS Comment_Type
      FROM
            dbo.Client_Comment_Map ccm
            JOIN dbo.Comment com
                  ON com.Comment_ID = ccm.Comment_Id
            JOIN dbo.Code cd
                  ON cd.Code_Id = com.Comment_Type_CD
      WHERE
            ccm.Client_Id = @Client_Id            
     
END
;
GO
GRANT EXECUTE ON  [dbo].[Client_Comment_Sel_By_Client_Id] TO [CBMSApplication]
GO
