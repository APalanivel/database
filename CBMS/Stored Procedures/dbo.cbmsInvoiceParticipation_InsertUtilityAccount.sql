SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsInvoiceParticipation_InsertUtilityAccount

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@user_info_id  	int       	          	
	@account_id    	int       	          	
	@site_id       	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE       PROCEDURE [dbo].[cbmsInvoiceParticipation_InsertUtilityAccount]
	( @user_info_id int
	, @account_id int
	, @site_id int
	)
AS
BEGIN

	set nocount on

	EXEC cbmsInvoiceParticipationQueue_Save
		  @user_info_id
		, 1 -- see called sproc for definition
		, null
		, null
		, @site_id 
		, @account_id 
		, null
		, 1

--	set nocount off

END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvoiceParticipation_InsertUtilityAccount] TO [CBMSApplication]
GO
