SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE dbo.SR_SAD_RPT_ALTERNATE_FUELS_P 
AS
	set nocount on
select 
  entity_id as ALTERNATE_FUEL_ID
 ,entity_name as ALTERNATE_FUEL_NAME
from entity 
where  
entity_type = 116 
order by entity_name
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_RPT_ALTERNATE_FUELS_P] TO [CBMSApplication]
GO
