SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                                
                      
NAME: Invoice_Collection_Queue_ServiceMonth_Insert                  
                      
DESCRIPTION:                       
	To create the Invoice collection month map
                            
INPUT PARAMETERS:                                
NAME									DATATYPE DEFAULT  DESCRIPTION                                
------------------------------------------------------------------------------                                
@Invoice_Collection_Queue_Id			INT
@Account_Invoice_Collection_Month_Id	NVARCHAR(MAX)                                      

OUTPUT PARAMETERS:                                
NAME   DATATYPE DEFAULT  DESCRIPTION                                                      
------------------------------------------------------------------------------

USAGE EXAMPLES:                                
------------------------------------------------------------------------------


AUTHOR INITIALS:                                
INITIALS	NAME                                
------------------------------------------------------------                                
PR			Pradip Rajput
                    
MODIFICATIONS                      
INITIALS	DATE(YYYY-MM-DD)	MODIFICATION                      
------------------------------------------------------------                      
PR			2018-10-02			Created
*/
CREATE PROCEDURE [dbo].[Invoice_Collection_Queue_ServiceMonth_Insert]
      (
       @Invoice_Collection_Queue_Id INT
      ,@Account_Invoice_Collection_Month_Id NVARCHAR(MAX) )
AS
BEGIN

      SET NOCOUNT ON;  

      INSERT      INTO Invoice_Collection_Queue_Month_Map
                  ( Invoice_Collection_Queue_Id
                  ,Account_Invoice_Collection_Month_Id
                  ,Created_Ts )
                  SELECT
                        @Invoice_Collection_Queue_Id
                       ,Account_month_ids.Segments
                       ,GETDATE()
                  FROM
                        dbo.ufn_split(@Account_Invoice_Collection_Month_Id, ',') AS Account_month_ids
                                               
END;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Queue_ServiceMonth_Insert] TO [CBMSApplication]
GO
