
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	dbo.SR_RFP_SET_RFP_SELECTED_PRICE_PRODUCTS_P

DESCRIPTION: 


INPUT PARAMETERS:    
    Name                DataType          Default     Description    
---------------------------------------------------------------------------------    
	@rfpId				int
	@priceProductId		int
	@isSystemProduct	int
	@productDetails		varchar(4000)
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------

	SELECT * FROM dbo.SR_RFP_PRICING_PRODUCT WHERE SR_RFP_ID = 13256

	BEGIN TRANSACTION
		SELECT * FROM dbo.SR_RFP_SELECTED_PRODUCTS WHERE SR_RFP_ID = 13256
		EXEC dbo.SR_RFP_SET_RFP_SELECTED_PRICE_PRODUCTS_P 13256,1249801,NULL,'Test'
		SELECT * FROM dbo.SR_RFP_SELECTED_PRODUCTS WHERE SR_RFP_ID = 13256
	ROLLBACK TRANSACTION

	BEGIN TRANSACTION
		SELECT * FROM dbo.SR_RFP_SELECTED_PRODUCTS WHERE SR_RFP_ID = 13256
		EXEC dbo.SR_RFP_SET_RFP_SELECTED_PRICE_PRODUCTS_P 13256,1249802,NULL,'Test'
		SELECT * FROM dbo.SR_RFP_SELECTED_PRODUCTS WHERE SR_RFP_ID = 13256
	ROLLBACK TRANSACTION

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DR			Deana Ritter

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	DR			08/04/2009	Removed Linked Server Updates
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2016-03-28	Global Sourcing - Phase3 - GCS-606 Modified script to aviod duplicate entries


******/
CREATE PROCEDURE [dbo].[SR_RFP_SET_RFP_SELECTED_PRICE_PRODUCTS_P]
      ( 
       @rfpId INT
      ,@priceProductId INT
      ,@isSystemProduct INT
      ,@productDetails VARCHAR(4000) )
AS 
BEGIN
      SET NOCOUNT ON;
      
      DECLARE @commodity VARCHAR(20)


      INSERT      INTO dbo.SR_RFP_SELECTED_PRODUCTS
                  ( 
                   SR_RFP_ID
                  ,PRICING_PRODUCT_ID
                  ,IS_SYSTEM_PRODUCT
                  ,PRODUCT_DETAILS )
                  SELECT
                        @rfpId
                       ,@priceProductId
                       ,NULL
                       ,-- it will always ZERO-- see bug no cr#7669
                        @productDetails
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.SR_RFP_SELECTED_PRODUCTS
                                     WHERE
                                          SR_RFP_ID = @rfpId
                                          AND PRICING_PRODUCT_ID = @priceProductId )

END
;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_SET_RFP_SELECTED_PRICE_PRODUCTS_P] TO [CBMSApplication]
GO
