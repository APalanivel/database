SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********  
NAME:  dbo.Holiday_Master_Sel_By_Country_TOU_Schedule_Term_Id_Start_Year_End_Year  
  
DESCRIPTION:    
 Selects the Holidays from the Holiday_Master table and TOU_Schedule_Term_Holiday tables  
  
INPUT PARAMETERS:  
Name							DataType        Default     Description  
-------------------------------------------------------------------  
@Country_Id						INT  
@Time_Of_Use_Schedule_Term_Id	INT  
@Start_Year						INT   
@End_Year						INT   
  
      
  
OUTPUT PARAMETERS:  
      Name        DataType          Default     Description  
------------------------------------------------------------  
  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
  
Exec dbo.Holiday_Master_Sel_By_Country_TOU_Schedule_Term_Id_Start_Year_End_Year 4, 1, 2011, 2018  
  
  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 CPE	  Chaitanya Panduga Eshwar  
  
  
 Initials Date  Modification  
------------------------------------------------------------  
 CPE	  2012-07-11  Created  
  
******/  
CREATE PROCEDURE dbo.Holiday_Master_Sel_By_Country_TOU_Schedule_Term_Id_Start_Year_End_Year
( 
 @Country_Id INT
,@Time_Of_Use_Schedule_Term_Id INT
,@Start_Year INT
,@End_Year INT )
AS 
BEGIN  
      SET NOCOUNT ON ;
      WITH  CTE_Peak_Holiday
              AS ( SELECT
                        TOUSTP.Time_Of_Use_Schedule_Term_Id
                   FROM
                        dbo.Time_Of_Use_Schedule_Term_Peak TOUSTP
                        JOIN dbo.Time_of_Use_Schedule_Term_Peak_Dtl TOUSTPD
                              ON TOUSTP.Time_Of_Use_Schedule_Term_Peak_Id = TOUSTPD.Time_Of_Use_Schedule_Term_Peak_Id
                   WHERE
                        TOUSTP.Time_Of_Use_Schedule_Term_Id = @Time_Of_Use_Schedule_Term_Id
                        AND TOUSTPD.Is_Holiday_Active = 1
                   GROUP BY
                        TOUSTP.Time_Of_Use_Schedule_Term_Id)
            SELECT
                  HM.Holiday_Master_Id
                 ,HM.Year_Identifier
                 ,HM.Holiday_Name
                 ,ISNULL(TOUSTH.Holiday_Dt, HM.Holiday_Dt) AS Holiday_Dt
                 ,HM.COUNTRY_ID
                 ,TOUSTH.Is_Rule_One_Active
                 ,TOUSTH.Is_Rule_Two_Active
                 ,TOUSTH.Is_Manual_Override
                 ,CASE WHEN TOUSTH.Time_Of_Use_Schedule_Term_Id IS NULL THEN NULL
                       WHEN CTE.Time_Of_Use_Schedule_Term_Id IS NULL THEN 0
                       ELSE 1
                  END AS Peak_Exists
            FROM
                  dbo.Holiday_Master HM
                  LEFT JOIN dbo.Time_Of_Use_Schedule_Term_Holiday TOUSTH
                        ON HM.Holiday_Master_Id = TOUSTH.Holiday_Master_Id
                           AND TOUSTH.Time_Of_Use_Schedule_Term_Id = @Time_Of_Use_Schedule_Term_Id
                  LEFT JOIN CTE_Peak_Holiday CTE
                        ON TOUSTH.Time_Of_Use_Schedule_Term_Id = CTE.Time_Of_Use_Schedule_Term_Id
            WHERE
                  COUNTRY_ID = @Country_Id
                  AND YEAR(HM.Holiday_Dt) BETWEEN @Start_Year
                                          AND     @End_Year
            ORDER BY
                  HM.Year_Identifier  
END   

;
GO
GRANT EXECUTE ON  [dbo].[Holiday_Master_Sel_By_Country_TOU_Schedule_Term_Id_Start_Year_End_Year] TO [CBMSApplication]
GO
