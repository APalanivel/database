SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_FORECAST_VOLUMES_SITES_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@clientId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE      PROCEDURE  dbo.GET_FORECAST_VOLUMES_SITES_P
@userId varchar(10),
@sessionId varchar(20),
@clientId int

AS
set nocount on

select 	site.site_id SITE_ID,
	site.site_name SITE_NAME
from 	rm_forecast_volume_details details, 
	rm_forecast_volume forecast,
	vwSiteName site
where 	forecast.client_id = @clientId
	and details.rm_forecast_volume_id = forecast.rm_forecast_volume_id
	and site.site_id = details.site_id
	
group by site.site_id,site.site_name
GO
GRANT EXECUTE ON  [dbo].[GET_FORECAST_VOLUMES_SITES_P] TO [CBMSApplication]
GO
