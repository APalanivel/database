SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE   procedure dbo.cbmsRmMarketOutlook_Save

	(@OutlookId int = null
	, @MarketGroupid int
	, @IntervalTypeId int
	, @PubDate smalldatetime
	--, @CbmsImageId int = null
	--, @OutlookTitle varchar(255)
	--, @Overview varchar(5000)	
	)
as
BEGIN
	set nocount on
	declare @ThisId int

	 
	set @ThisId  = @OutlookId

if @ThisId is null
	begin
	
		insert into rm_market_outlook
			( rm_market_group_id
			, interval_type_id
			, pub_date
			--, cbms_image_id
			--, rm_market_outlook_title
			--, overview
			 )
		values
			(@MarketGroupId
			, @IntervalTypeId
			, @PubDate
			--, @CbmsImageId
			--, @OutlookTitle
			--, @Overview			
			)
	
		set @ThisId = @@IDENTITY

	end
else
	begin
	
		update rm_market_outlook
		set rm_market_group_id = @MarketGroupId
			, interval_type_id = @IntervalTypeId
			, pub_date = @PubDate
			--, cbms_image_id = @CbmsImageId
			--, rm_market_outlook_title = @OutlookTitle
			--, overview = @Overview			
			
		where rm_market_outlook_id = @OutlookId
	
	end
	
set nocount off

	select * from rm_market_outlook where rm_market_outlook_id = @ThisId

END



GO
GRANT EXECUTE ON  [dbo].[cbmsRmMarketOutlook_Save] TO [CBMSApplication]
GO
