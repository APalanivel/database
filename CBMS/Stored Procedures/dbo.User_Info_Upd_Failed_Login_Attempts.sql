SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  

 NAME: DV2.dbo.User_Info_Upd_Failed_Login_Attempts  
 
 DESCRIPTION: 
	
	To Update User_Info Login Attempt details for the given user Id.
	This procedure will be called in two places,
	
		-- When ever a password gets changed this procedure will be called by other procedure/application to reset the Login_Attempt to 0.
		-- When ever a user trying with the wrong password application will increment the login attempt value using this procedure.

 INPUT PARAMETERS:  
 Name							DataType		Default Description  
------------------------------------------------------------
 @User_Info_Id					INT
 @Failed_Login_Attempts			INT
 
 OUTPUT PARAMETERS:
 Name   DataType  Default Description
------------------------------------------------------------

 USAGE EXAMPLES:
------------------------------------------------------------

		EXEC dbo.User_Info_Upd_Failed_Login_Attempts 422, 0
		EXEC dbo.User_Info_Upd_Failed_Login_Attempts 18989, 7
		
		SELECT
			*
		FROM
			User_Info
		WHERE
			Client_Id iS NOT NULL
			AND USER_INFO_ID = 422
		ORDER BY
			Failed_Login_Attempts DESC

 AUTHOR INITIALS:
 Initials	Name
------------------------------------------------------------
 PNR		Pandarinath
 
 MODIFICATIONS
 Initials	Date			Modification
------------------------------------------------------------
 PNR		10/12/2010		Created as a part of DV/SV Password expiration project.

******/

CREATE PROCEDURE dbo.User_Info_Upd_Failed_Login_Attempts
(
	 @User_Info_Id			INT
	,@Failed_Login_Attempts	INT
)
AS
BEGIN

    SET NOCOUNT ON;

    UPDATE
		dbo.USER_INFO
		SET Failed_Login_Attempts = @Failed_Login_Attempts
	WHERE
		USER_INFO_ID = @User_Info_Id

END
GO
GRANT EXECUTE ON  [dbo].[User_Info_Upd_Failed_Login_Attempts] TO [CBMSApplication]
GO
