SET NUMERIC_ROUNDABORT OFF 
GO

SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/******
 NAME:  
	dbo.cbmsSsoProjectOwner_Save
 
 DESCRIPTION:   
 INPUT PARAMETERS:  
 Name			DataType  Default Description  
------------------------------------------------------------  
 @Project_id	int
 @owner_id		int
 @Hier_Level_Cd int
 
 OUTPUT PARAMETERS:  

 Name   DataType  Default Description  
------------------------------------------------------------  
 USAGE EXAMPLES:
------------------------------------------------------------  
  
  EXEC dbo.cbmsSsoProjectOwner_Save
  
 AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 CPE		Chaitanya Panduga Eshwar
 RMG        Rani Mary George
 
 MODIFICATIONS:
 Initials Date			Modification  
------------------------------------------------------------  
 CPE	  03/22/2011	Modified the SP to capture the Client_Hier_Id from the OwnerId and Owner_Type_Id.
 RMG      12/24/2014    Modified to return Client_Hier_Id(To fix the issue related to documents saving from Rate comparison)
            
******/

CREATE PROCEDURE [dbo].[cbmsSsoProjectOwner_Save]
( 
 @Project_id int
,@owner_id int
,@Hier_Level_Cd int )
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE
            @Client_Hier_Id int

      SELECT
            @Client_Hier_Id = Client_Hier_Id
      FROM
            Core.Client_Hier CH
            JOIN dbo.Code C
                  ON CH.Hier_level_Cd = C.Code_Id
      WHERE
            CH.Hier_level_Cd = @Hier_Level_Cd
            AND ( ( CH.Client_Id = @owner_id
                    AND C.Code_Value = 'Corporate' )
                  OR ( CH.Sitegroup_Id = @owner_id
                       AND C.Code_Value = 'Division' )
                  OR ( CH.Site_Id = @owner_id
                       AND C.Code_Value = 'Site' ) )

      INSERT INTO
            dbo.SSO_PROJECT_OWNER_MAP
            ( SSO_Project_ID
            ,Client_Hier_Id )
            SELECT
                  @Project_id
                 ,@Client_Hier_Id
            WHERE
                  NOT EXISTS ( SELECT
                                    1
                               FROM
                                    dbo.SSO_PROJECT_OWNER_MAP
                               WHERE
                                    SSO_Project_ID = @Project_id
                                    AND Client_Hier_Id = @Client_Hier_Id )


	 SELECT @Client_Hier_Id AS Client_Hier_Id
END



;
GO

GRANT EXECUTE ON  [dbo].[cbmsSsoProjectOwner_Save] TO [CBMSApplication]
GO
