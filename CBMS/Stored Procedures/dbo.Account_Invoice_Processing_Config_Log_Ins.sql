SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:                  
        dbo.Account_Invoice_Processing_Config_Log_Ins                    
                  
Description:                  
			Watch List - Ability to timeband watch list settings.                  
                  
 Input Parameters:                  
 Name                                        DataType        Default        Description                    
--------------------------------------------------------------------------------------------------                   
  @Account_Invoice_Processing_Config_Id		INT
  @Field_Name								NVARCHAR(255)
  @Change_Type								NVARCHAR(255)
  @Previous_Value							NVARCHAR(MAX) = NULL
  @Current_Value							NVARCHAR(MAX) = NULL
  @Is_Updated_Using_Apply_All				BIT = 0
  @Event_By_User_Id							INT
		 
		                   
 Output Parameters:                        
 Name                                        DataType        Default        Description                    
--------------------------------------------------------------------------------------------------                   
                  
 Usage Examples:                      
--------------------------------------------------------------------------------------------------


BEGIN TRAN 

SELECT * FROM [dbo].[Account_Invoice_Processing_Config] where Account_Invoice_Processing_Config_Id=3
SELECT * FROM [dbo].[Account_Invoice_Processing_Config_Log] where Account_Invoice_Processing_Config_Id=3


     

	 EXEC dbo.Account_Invoice_Processing_Config_Log_Ins
		  @Account_Invoice_Processing_Config_Id =3
         , @Field_Name ='Invoice Processing Instructions'
         , @Change_Type = 'Edited'
         , @Previous_Value  = 'test 123'
         , @Current_Value  = 'test 1234'
         , @Is_Updated_Using_Apply_All  = 0
         , @Event_By_User_Id =49


SELECT * FROM [dbo].[Account_Invoice_Processing_Config] where Account_Invoice_Processing_Config_Id=3
SELECT * FROM [dbo].[Account_Invoice_Processing_Config_Log] where Account_Invoice_Processing_Config_Id=3


ROLLBACK TRAN
                 
 Author Initials:                  
  Initials        Name                  
--------------------------------------------------------------------------------------------------                   
  NR              Narayana Reddy    
                   
 Modifications:                  
  Initials        Date              Modification                  
--------------------------------------------------------------------------------------------------                   
  NR              2019-01-03		Created for Data2.0 - Watch List - B.                
                 
******/

CREATE PROCEDURE [dbo].[Account_Invoice_Processing_Config_Log_Ins]
    (
        @Account_Invoice_Processing_Config_Id INT
        , @Field_Name NVARCHAR(255)
        , @Change_Type NVARCHAR(255)
        , @Previous_Value NVARCHAR(MAX) = NULL
        , @Current_Value NVARCHAR(MAX) = NULL
        , @Is_Updated_Using_Apply_All BIT = 0
        , @Event_By_User_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        INSERT INTO dbo.Account_Invoice_Processing_Config_Log
             (
                 Account_Invoice_Processing_Config_Id
                 , Field_Name
                 , Change_Type
                 , Previous_Value
                 , Current_Value
                 , Is_Updated_Using_Apply_All
                 , Event_By_User_Id
                 , Event_Ts
             )
        SELECT
            @Account_Invoice_Processing_Config_Id
            , @Field_Name
            , @Change_Type
            , @Previous_Value
            , @Current_Value
            , @Is_Updated_Using_Apply_All
            , @Event_By_User_Id
            , GETDATE();

    END;



GO
GRANT EXECUTE ON  [dbo].[Account_Invoice_Processing_Config_Log_Ins] TO [CBMSApplication]
GO
