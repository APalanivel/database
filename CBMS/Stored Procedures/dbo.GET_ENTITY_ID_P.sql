SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE dbo.GET_ENTITY_ID_P
	@entityName varchar(200),
	@entityType int
	AS
	begin
		set nocount on

		select 	entity_id 
		from 	entity 
		where 	entity_name = @entityName 
			and entity_type = @entityType

	end
GO
GRANT EXECUTE ON  [dbo].[GET_ENTITY_ID_P] TO [CBMSApplication]
GO
