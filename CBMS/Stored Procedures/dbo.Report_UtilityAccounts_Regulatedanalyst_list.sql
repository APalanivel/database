
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
	dbo.Report_UtilityAccounts_Regulatedanalyst_list
	
DESCRIPTION:
	This Procedure is to get All User Name for Regulated Analyst
	
INPUT PARAMETERS:
Name			DataType		Default	Description
-------------------------------------------------------------------------------------------
	
OUTPUT PARAMETERS:
Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------
Report_UtilityAccounts_Regulatedanalyst_list  
	
AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------
SSR			Sharad srivastava
SP			Sandeep Pigilam

MODIFICATIONS
Initials	Date		Modification
------------------------------------------------------------
 SSR		09/05/2010	Created
 SKA		11/30/2010	Vendor_Commodity is for other services only, for regulated market details we have to refere Utility_Detail_Analyst_Map
 SP			2014-09-09  For Data Transition used a table variable @Group_Legacy_Name to handle the hardcoded group names  

*/
CREATE PROC [dbo].[Report_UtilityAccounts_Regulatedanalyst_list]
AS 
BEGIN

      SET NOCOUNT ON
        
      DECLARE @Group_Legacy_Name TABLE
            ( 
             GROUP_INFO_ID INT
            ,GROUP_NAME VARCHAR(200)
            ,Legacy_Group_Name VARCHAR(200) )
            
      INSERT      INTO @Group_Legacy_Name
                  ( 
                   GROUP_INFO_ID
                  ,GROUP_NAME
                  ,Legacy_Group_Name )
                  EXEC dbo.Group_Info_Sel_By_Group_Legacy 
                        @Group_Legacy_Name_Cd_Value = 'Regulated_Market'  
      SELECT
            0 USER_INFO_ID
           ,' None' RegulatedMarketsAnalyst
      UNION ALL
      SELECT
            ui.USER_INFO_ID
           ,ui.first_Name + ' ' + ui.last_name AS RegulatedMarketsAnalyst
      FROM
            UTILITY_DETAIL ud
            JOIN Utility_Detail_Analyst_Map udam
                  ON udam.Utility_Detail_ID = ud.UTILITY_DETAIL_ID
            JOIN GROUP_INFO gi
                  ON gi.GROUP_INFO_ID = udam.Group_Info_ID
            JOIN user_info ui
                  ON ui.USER_INFO_ID = udam.Analyst_ID
            INNER JOIN @Group_Legacy_Name gil
                  ON gi.Group_Info_ID = gil.GROUP_INFO_ID
      GROUP BY
            ui.USER_INFO_ID
           ,ui.first_Name
           ,ui.last_name
      ORDER BY
            RegulatedMarketsAnalyst
  
END
;
GO

GRANT EXECUTE ON  [dbo].[Report_UtilityAccounts_Regulatedanalyst_list] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_UtilityAccounts_Regulatedanalyst_list] TO [CBMSApplication]
GO
