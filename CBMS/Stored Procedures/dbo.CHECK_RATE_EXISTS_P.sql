SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE dbo.CHECK_RATE_EXISTS_P 
	@rateName VARCHAR(200),
	@vendorID INT,
	@entityID INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT rate_name 
	FROM dbo.rate 
	WHERE rate_name = @rateName 
		AND vendor_id = @vendorID 
		AND commodity_type_id = @entityID

END
GO
GRANT EXECUTE ON  [dbo].[CHECK_RATE_EXISTS_P] TO [CBMSApplication]
GO
