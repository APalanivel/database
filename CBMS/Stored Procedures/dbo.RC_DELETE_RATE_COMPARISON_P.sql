SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.RC_DELETE_RATE_COMPARISON_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(20)	          	
	@sessionId     	varchar(20)	          	
	@rateComparisonId	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE DBO.RC_DELETE_RATE_COMPARISON_P 

@userId varchar(20),
@sessionId varchar(20),
@rateComparisonId int

AS
set nocount on
	delete from RC_RATE_COMPARISON 
	where 
	RC_RATE_COMPARISON_ID=@rateComparisonId
GO
GRANT EXECUTE ON  [dbo].[RC_DELETE_RATE_COMPARISON_P] TO [CBMSApplication]
GO
