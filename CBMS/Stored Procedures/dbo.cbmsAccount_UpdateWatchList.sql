SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	cbms_prod.dbo.cbmsAccount_UpdateWatchList

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@account_id    	int       	          	
	@dm_watch_list 	bit       	null      	
	@ra_watch_list 	bit       	null      	
	@processing_instructions	varchar(6000)	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hari
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script
	HG			11/05/2009	Removed unused parameter @myaccount_id from the calling procedure cbmsAccount_Get
        	
******/

CREATE PROCEDURE dbo.cbmsAccount_UpdateWatchList
	( @MyAccountId int
	, @account_id int
	, @dm_watch_list bit = null
	, @ra_watch_list bit = null
	, @processing_instructions varchar(6000) = null
	)
AS
BEGIN

	SET NOCOUNT ON

	UPDATE
		account
		SET dm_watch_list = @dm_watch_list
			, ra_watch_list = @ra_watch_list
			, processing_instructions = @processing_instructions
	WHERE
		account_id = @account_id

	EXEC dbo.cbmsAccount_Get @account_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsAccount_UpdateWatchList] TO [CBMSApplication]
GO
