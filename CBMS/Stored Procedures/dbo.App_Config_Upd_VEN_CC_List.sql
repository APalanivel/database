SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 dbo.App_Config_Upd_VEN_CC_List  
  
DESCRIPTION:  
  To Update dbo.App_Config table by VEN_CC_List  
  
 INPUT PARAMETERS:            
                       
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
@AppId							INT  
@AppConfigCode					VARCHAR(255)  
@ConfigVal						VARCHAR(MAX)
  
  
 OUTPUT PARAMETERS:            
                             
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
  
 USAGE EXAMPLES:                              
---------------------------------------------------------------------------------------------------------------                              
BEGIN TRAN 
SELECT
      *
FROM
      dbo.App_config 
SELECT
      *
FROM
      dbo.App_config
WHERE
      App_Id = 1
      AND App_Config_Cd = 'Variance_Exception_Notice_Email_Address'
      
EXEC dbo.App_Config_Upd_VEN_CC_List 
      @App_Id =1  
      ,@App_Config_Cd ='Variance_Exception_Notice_Email_Address'
      ,@App_Config_Value ='Tim Jones(tim.jones@ems.schneider-electric.com);Sarah Nelson(sarah.nelson@ems.schneider-electric.com);sandeep.pigilam@ems.schneider-electric.com'
     
SELECT
      *
FROM
      dbo.App_config
WHERE
      App_Id = 1
      AND App_Config_Cd = 'Variance_Exception_Notice_Email_Address'
SELECT
      *
FROM
      dbo.App_config 
ROLLBACK  
  
 AUTHOR INITIALS:          
         
 Initials              Name          
---------------------------------------------------------------------------------------------------------------                        
 SP					Sandeep Pigilam  
  
 MODIFICATIONS:        
            
 Initials              Date             Modification        
---------------------------------------------------------------------------------------------------------------        
  SP				2014-09-04		Created for Data Transition.  
  
******/  
  
CREATE PROCEDURE [dbo].[App_Config_Upd_VEN_CC_List]
      ( 
       @App_Id INT
      ,@App_Config_Cd VARCHAR(255)
      ,@App_Config_Value VARCHAR(MAX)
      ,@Is_Active BIT = 1
      ,@Start_Dt DATETIME = '1900-01-01 00:00:00.000'
      ,@End_Dt DATETIME = '2099-12-31 00:00:00.000' )
AS 
BEGIN  
      SET NOCOUNT ON    
      
      DECLARE @App_Config TABLE
            ( 
             App_Id INT
            ,App_Config_Cd VARCHAR(255)
            ,App_Config_Value VARCHAR(255)
            ,Is_Active BIT
            ,Start_Dt DATETIME
            ,End_Dt DATETIME )
      
      INSERT      INTO @App_Config
                  ( 
                   App_Id
                  ,App_Config_Cd
                  ,App_Config_Value
                  ,Is_Active
                  ,Start_Dt
                  ,End_Dt )
                  SELECT
                        @App_Id
                       ,@App_Config_Cd
                       ,ufn.Segments
                       ,@Is_Active
                       ,@Start_Dt
                       ,@End_Dt
                  FROM
                        dbo.ufn_split(@App_Config_Value, ';') ufn
                        
      MERGE INTO dbo.App_Config AS tgt
            USING 
                  ( SELECT
                        App_Id
                       ,App_Config_Cd
                       ,App_Config_Value
                       ,Is_Active
                       ,Start_Dt
                       ,End_Dt
                    FROM
                        @App_Config ) AS src
            ON tgt.App_Id = src.App_Id
                  AND tgt.App_Config_Cd = src.App_Config_Cd
                  AND tgt.App_Config_Value = src.App_Config_Value
            WHEN NOT MATCHED BY SOURCE AND tgt.App_Id = App_Id
                  AND tgt.App_Config_Cd = @App_Config_Cd
                  THEN        
				DELETE
            WHEN NOT MATCHED BY TARGET 
                  THEN         
				INSERT
                    VALUES
                        ( 
                         src.App_Id
                        ,src.App_Config_Cd
                        ,src.App_Config_Value
                        ,src.Is_Active
                        ,src.Start_Dt
                        ,src.End_Dt );       
      

END 
;
GO
GRANT EXECUTE ON  [dbo].[App_Config_Upd_VEN_CC_List] TO [CBMSApplication]
GO
