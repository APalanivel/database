SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.Rm_Budget_Current_Hedge_Position_Dtl_Ins                        
                          
Description:                          
        To get market price and forecast pirce if a selected index   
                          
Input Parameters:                          
    Name    DataType        Default     Description                            
--------------------------------------------------------------------------------    
	@Index_Id   INT    
    @Start_Dt	Date    
	@End_Dt		Date
                          
 Output Parameters:                                
	Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
Usage Examples:                              
--------------------------------------------------------------------------------    
	SELECT * FROM dbo.ENTITY e WHERE e.ENTITY_TYPE=272

	EXEC Trade.Rm_Budget_Current_Hedge_Position_Dtl_Ins  584,'2019-01-01','2019-12-01'  
    
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR			2019-12-23	RM-Budgets Enahancement - Created
	RR			2020-02-24	GRM-1769 Modified insert to merge                
******/
CREATE PROCEDURE [Trade].[Rm_Budget_Current_Hedge_Position_Dtl_Ins]
    (
        @Rm_Budget_Id INT
        , @tvp_Rm_Budget_Current_Hedge_Position_Dtl AS [Trade].[tvp_Rm_Budget_Current_Hedge_Position_Dtl] READONLY
    )
AS
    BEGIN

        SET NOCOUNT ON;

        --INSERT INTO Trade.Rm_Budget_Current_Hedge_Position_Dtl
        --     (
        --         Rm_Budget_Id
        --         , Service_Month
        --         , Total_Forecast_Volume
        --         , Total_Volume_Hedged
        --         , Hedged_Unit_Cost
        --     )
        --SELECT
        --    @Rm_Budget_Id
        --    , Service_Month
        --    , Total_Forecast_Volume
        --    , Total_Volume_Hedged
        --    , Hedged_Unit_Cost
        --FROM
        --    @tvp_Rm_Budget_Current_Hedge_Position_Dtl;

        MERGE INTO Trade.Rm_Budget_Current_Hedge_Position_Dtl AS tgt
        USING
        (   SELECT
                @Rm_Budget_Id AS Rm_Budget_Id
                , Service_Month
                , Total_Forecast_Volume
                , Total_Volume_Hedged
                , Hedged_Unit_Cost
            FROM
                @tvp_Rm_Budget_Current_Hedge_Position_Dtl) AS src
        ON tgt.Rm_Budget_Id = src.Rm_Budget_Id
           AND  tgt.Service_Month = src.Service_Month
        WHEN MATCHED THEN UPDATE SET
                              tgt.Total_Forecast_Volume = src.Total_Forecast_Volume
                              , tgt.Total_Volume_Hedged = src.Total_Volume_Hedged
                              , tgt.Hedged_Unit_Cost = src.Hedged_Unit_Cost
        WHEN NOT MATCHED THEN INSERT (Rm_Budget_Id
                                      , Service_Month
                                      , Total_Forecast_Volume
                                      , Total_Volume_Hedged
                                      , Hedged_Unit_Cost)
                              VALUES
                                  (@Rm_Budget_Id
                                   , src.Service_Month
                                   , src.Total_Forecast_Volume
                                   , src.Total_Volume_Hedged
                                   , src.Hedged_Unit_Cost);
    END;

GO
GRANT EXECUTE ON  [Trade].[Rm_Budget_Current_Hedge_Position_Dtl_Ins] TO [CBMSApplication]
GO
