SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE dbo.GET_BD_DETAIL_P
	@parentBdId INT,
	@parentMasterId INT,
	@parentDisplayId VARCHAR(4),
	@parentRateId INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT bd.billing_determinant_id , 
		bd.billing_determinant_master_id , 
		bdm.bd_display_id,
		rs.rate_id, 
		bd.bd_parent_id rs_id
	FROM dbo.rate_schedule rs INNER JOIN dbo.billing_determinant bd ON bd.bd_parent_id = rs.rate_schedule_id
		INNER JOIN dbo.billing_determinant_master bdm ON bdm.billing_determinant_master_id = bd.billing_determinant_master_id			
			AND bd.bd_parent_id = rs.rate_schedule_id
	WHERE bdm.bd_display_id  = @parentDisplayId
		AND bdm.bd_parent_id <> rs.rate_id		
		AND bdm.bd_parent_type_id = 96 
		AND bd.bd_parent_type_id= 97				
		AND bd.billing_determinant_id  > @parentBdId
		AND bd.billing_determinant_master_id = @parentMasterId
		AND rs.rate_id <>  @parentRateId

END
GO
GRANT EXECUTE ON  [dbo].[GET_BD_DETAIL_P] TO [CBMSApplication]
GO
