SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********         
  
NAME:  
     dbo.Report_DE_Account_Site_Status_Sel_By_Client  
         
DESCRIPTION:            
     
        
 INPUT PARAMETERS:            
                       
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------   
 @Client_Id       INT       
            
 OUTPUT PARAMETERS:            
                             
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
            
 USAGE EXAMPLES:                              
---------------------------------------------------------------------------------------------------------------                              
   
 EXEC dbo.Report_DE_Account_Site_Status_Sel_By_Client 12248  
   
    
 AUTHOR INITIALS:          
         
 Initials              Name          
---------------------------------------------------------------------------------------------------------------                        
 AKR                   Ashok Kumar Raju          
  
    
 MODIFICATIONS:        
            
 Initials              Date             Modification        
---------------------------------------------------------------------------------------------------------------        
 AKR                   2014-06-01      Created.  
*********/  
  
  
CREATE PROCEDURE dbo.Report_DE_Account_Site_Status_Sel_By_Client ( @Client_Id INT )
AS 
BEGIN  
      SET NOCOUNT ON  
        
        
      SELECT
            Site_name [Site Name]
           ,ca.Account_Vendor_Name [Vendor]
           ,ca.Account_Number [Acct#]
           ,ca.Account_Type [Account Type]
           ,com.Commodity_Name [Service Type]
           ,ch.Site_Address_Line1 + ' ' + ch.city + ' ' + ch.state_name [Site Address]
           ,[Site Active/Inactive] = CASE WHEN ch.Site_Not_Managed = 1 THEN 'Inactive'
                                          ELSE 'Active'
                                     END
           ,[Site Closed Date] = CASE WHEN ch.Site_Closed_Dt != '1900-01-01' THEN ch.Site_Closed_Dt
                                      ELSE NULL
                                 END
           ,[Account Active/Inactive] = CASE WHEN ca.Account_Not_Managed = '1' THEN 'Inactive'
                                             ELSE 'Active'
                                        END
           ,[Account Not Expected Date] = ca.Account_Not_Expected_Dt
      FROM
            Core.Client_Hier ch
            JOIN Core.Client_Hier_Account ca
                  ON ca.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN Commodity com
                  ON com.Commodity_Id = ca.Commodity_Id
      WHERE
            ch.CLIENT_Id = @Client_Id
      GROUP BY
            Site_name
           ,ca.Account_Vendor_Name
           ,ca.Account_Number
           ,ca.Account_Type
           ,com.Commodity_Name
           ,ch.Site_Address_Line1
           ,ch.city
           ,ch.state_name
           ,CASE WHEN ch.Site_Not_Managed = 1 THEN 'Inactive'
                 ELSE 'Active'
            END
           ,CASE WHEN ch.Site_Closed_Dt != '1900-01-01' THEN ch.Site_Closed_Dt
                 ELSE NULL
            END
           ,CASE WHEN ca.Account_Not_Managed = '1' THEN 'Inactive'
                 ELSE 'Active'
            END
           ,ca.Account_Not_Expected_Dt
      ORDER BY
            Site_name
           ,ca.Account_Number
           ,com.Commodity_Name  
END  
  
  
;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Account_Site_Status_Sel_By_Client] TO [CBMS_SSRS_Reports]
GO
