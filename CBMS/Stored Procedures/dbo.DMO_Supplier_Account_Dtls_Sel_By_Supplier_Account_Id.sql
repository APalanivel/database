SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.DMO_Supplier_Account_Dtls_Sel_By_Supplier_Account_Id
           
DESCRIPTION:             
			To get DMO supplier accounts associated to a client
			
INPUT PARAMETERS:            
	Name					DataType		Default		Description  
---------------------------------------------------------------------------------  
	@Supplier_Account_Id	INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	          
	EXEC dbo.DMO_Supplier_Account_Dtls_Sel_By_Supplier_Account_Id 1
	
	EXEC dbo.DMO_Supplier_Account_Dtls_Sel_By_Supplier_Account_Id 1149234
	
   EXEC dbo.DMO_Supplier_Account_Dtls_Sel_By_Supplier_Account_Id 1197550,507644
	
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy
	NR			Narayana Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-02-17	Contract placeholder - CP-54 Created
	NR			2020-01-16	MAINT-9734 - Added @Supplier_Account_Config_Id new parameter.
******/

CREATE PROCEDURE [dbo].[DMO_Supplier_Account_Dtls_Sel_By_Supplier_Account_Id]
    (
        @Supplier_Account_Id INT
        , @Supplier_Account_Config_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            chsite.Client_Name
            , chsite.Site_Id
            , chasupp.Account_Id AS Supplier_Account_Id
            , chasupp.Account_Number AS Supplier_Account_Number
            , chasupp.Account_Vendor_Name AS Supplier_Account_Vendor_Name
            , chasupp.Account_Not_Managed
            , chasupp.Supplier_Account_begin_Dt
            , chasupp.Supplier_Account_End_Dt
        FROM
            Core.Client_Hier chsite
            INNER JOIN Core.Client_Hier_Account chautil
                ON chsite.Client_Hier_Id = chautil.Client_Hier_Id
            INNER JOIN Core.Client_Hier_Account chasupp
                ON chautil.Meter_Id = chasupp.Meter_Id
        WHERE
            chasupp.Account_Id = @Supplier_Account_Id
            AND chasupp.Supplier_Account_Config_Id = @Supplier_Account_Config_Id
            AND chasupp.Account_Type = 'Supplier'
            AND chautil.Account_Type = 'Utility'
        GROUP BY
            chsite.Client_Name
            , chsite.Site_Id
            , chasupp.Account_Id
            , chasupp.Account_Number
            , chasupp.Account_Vendor_Name
            , chasupp.Account_Not_Managed
            , chasupp.Supplier_Account_begin_Dt
            , chasupp.Supplier_Account_End_Dt;


    END;
    ;

    ;


GO
GRANT EXECUTE ON  [dbo].[DMO_Supplier_Account_Dtls_Sel_By_Supplier_Account_Id] TO [CBMSApplication]
GO
