SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********
NAME:  dbo.Copy_TOU_Schedule_Term_All_Details

DESCRIPTION:  
	Loads all the details of the base term to the new term

INPUT PARAMETERS:
Name								DataType        Default     Description
-------------------------------------------------------------------
@New_Time_Of_Use_Schedule_Term_Id	INT
@Base_Time_Of_Use_Schedule_Term_Id	INT
				

OUTPUT PARAMETERS:
      Name								DataType          Default     Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------

Exec dbo.Copy_TOU_Schedule_Term_All_Details 8, 16

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CPE			Chaitanya Panduga Eshwar


	Initials Date		Modification
------------------------------------------------------------
	CPE		2012-07-11  Created

******/
CREATE PROCEDURE dbo.Copy_TOU_Schedule_Term_All_Details
( 
 @New_Time_Of_Use_Schedule_Term_Id INT
,@Base_Time_Of_Use_Schedule_Term_Id INT )
AS 
BEGIN
      SET NOCOUNT ON

      DECLARE
            @Start_Year INT
           ,@End_Year INT

	  -- Get the start and end year of the term	       
      SELECT
            @Start_Year = DATEPART(yy, Start_Dt)
           ,@End_Year = DATEPART(yy, End_Dt)
      FROM
            dbo.Time_Of_Use_Schedule_Term
      WHERE
            Time_Of_Use_Schedule_Term_Id = @New_Time_Of_Use_Schedule_Term_Id

      BEGIN TRY
            BEGIN TRAN
			-- Load all the holidays of the base term which fall within the range into the new term
            INSERT
                  dbo.Time_Of_Use_Schedule_Term_Holiday
                  ( 
                   Holiday_Master_Id
                  ,Time_Of_Use_Schedule_Term_Id
                  ,Is_Rule_One_Active
                  ,Is_Rule_Two_Active
                  ,Is_Manual_Override
                  ,Holiday_Dt )
                  SELECT
                        TOUSTH.Holiday_Master_Id
                       ,@New_Time_Of_Use_Schedule_Term_Id
                       ,TOUSTH.Is_Rule_One_Active
                       ,TOUSTH.Is_Rule_Two_Active
                       ,TOUSTH.Is_Manual_Override
                       ,TOUSTH.Holiday_Dt
                  FROM
                        dbo.Time_Of_Use_Schedule_Term_Holiday TOUSTH
                        JOIN dbo.Holiday_Master HM
                              ON TOUSTH.Holiday_Master_Id = HM.Holiday_Master_Id
                  WHERE
                        TOUSTH.Time_Of_Use_Schedule_Term_Id = @Base_Time_Of_Use_Schedule_Term_Id
                        AND HM.Year_Identifier BETWEEN @Start_Year
                                               AND     @End_Year

			-- Load all the Seasons of the base term into the new term
            INSERT
                  dbo.SEASON
                  ( 
                   SEASON_NAME
                  ,SEASON_FROM_DATE
                  ,SEASON_TO_DATE
                  ,SEASON_PARENT_ID
                  ,SEASON_PARENT_TYPE_ID )
                  SELECT
                        SEA.SEASON_NAME
                       ,SEA.SEASON_FROM_DATE
                       ,SEA.SEASON_TO_DATE
                       ,@New_Time_Of_Use_Schedule_Term_Id
                       ,SEA.SEASON_PARENT_TYPE_ID
                  FROM
                        dbo.SEASON SEA
                        JOIN dbo.ENTITY Ent
                              ON SEA.SEASON_PARENT_TYPE_ID = Ent.ENTITY_ID
                  WHERE
                        SEA.SEASON_PARENT_ID = @Base_Time_Of_Use_Schedule_Term_Id
                        AND Ent.ENTITY_NAME = 'Time Of Use Schedule Term'
                        AND Entity_Type = 120

			-- Load all the peaks of the base term into the new term
            INSERT
                  dbo.Time_Of_Use_Schedule_Term_Peak
                  ( 
                   Peak_Name
                  ,Time_Of_Use_Schedule_Term_Id
                  ,SEASON_ID )
                  SELECT
                        TOUSTP.Peak_Name
                       ,@New_Time_Of_Use_Schedule_Term_Id
                       ,New_SEA.SEASON_ID
                  FROM
                        dbo.Time_Of_Use_Schedule_Term_Peak TOUSTP
                        LEFT JOIN dbo.SEASON Base_SEA
                              ON TOUSTP.SEASON_ID = Base_SEA.SEASON_ID
                        LEFT JOIN ( dbo.SEASON New_SEA
                                    JOIN dbo.ENTITY Ent
                                          ON New_SEA.SEASON_PARENT_TYPE_ID = Ent.ENTITY_ID
                                             AND Ent.ENTITY_NAME = 'Time Of Use Schedule Term'
                                             AND Entity_Type = 120
                                             AND New_SEA.SEASON_PARENT_ID = @New_Time_Of_Use_Schedule_Term_Id )
                                    ON Base_SEA.SEASON_NAME = New_SEA.SEASON_NAME
                  WHERE
                        TOUSTP.Time_Of_Use_Schedule_Term_Id = @Base_Time_Of_Use_Schedule_Term_Id

			-- Load all the peak details of the base term into the new term
            INSERT
                  dbo.Time_of_Use_Schedule_Term_Peak_Dtl
                  ( 
                   Time_Of_Use_Schedule_Term_Peak_Id
                  ,Start_Time
                  ,End_Time
                  ,Day_Of_Week_Cd
                  ,Is_Holiday_Active )
                  SELECT
                        New_Peak.Time_Of_Use_Schedule_Term_Peak_Id
                       ,TOUSTPD.Start_Time
                       ,TOUSTPD.End_Time
                       ,TOUSTPD.Day_Of_Week_Cd
                       ,TOUSTPD.Is_Holiday_Active
                  FROM
                        dbo.Time_of_Use_Schedule_Term_Peak_Dtl TOUSTPD
                        JOIN dbo.Time_Of_Use_Schedule_Term_Peak Base_Peak
                              ON TOUSTPD.Time_Of_Use_Schedule_Term_Peak_Id = Base_Peak.Time_Of_Use_Schedule_Term_Peak_Id
                        LEFT JOIN dbo.SEASON Base_SEA
                              ON Base_Peak.SEASON_ID = Base_SEA.SEASON_ID
                        LEFT JOIN ( dbo.SEASON New_SEA
                                    JOIN dbo.ENTITY Ent
                                          ON New_SEA.SEASON_PARENT_TYPE_ID = Ent.ENTITY_ID
                                             AND Ent.ENTITY_NAME = 'Time Of Use Schedule Term'
                                             AND Entity_Type = 120
                                             AND New_SEA.SEASON_PARENT_ID = @New_Time_Of_Use_Schedule_Term_Id )
                                    ON ISNULL(Base_SEA.SEASON_NAME, '') = ISNULL(New_SEA.SEASON_NAME, '')
                        LEFT JOIN dbo.Time_Of_Use_Schedule_Term_Peak New_Peak
                              ON Base_Peak.Peak_Name = New_Peak.Peak_Name
                                 AND ISNULL(New_Peak.SEASON_ID, 0) = ISNULL(New_SEA.SEASON_ID, 0)
                  WHERE
                        Base_Peak.Time_Of_Use_Schedule_Term_Id = @Base_Time_Of_Use_Schedule_Term_Id
                        AND New_Peak.Time_Of_Use_Schedule_Term_Id = @New_Time_Of_Use_Schedule_Term_Id

            COMMIT TRAN
      END TRY
      BEGIN CATCH
            IF @@TRANCOUNT > 0 
                  ROLLBACK
            EXEC dbo.usp_RethrowError
      END CATCH
END 


;
GO
GRANT EXECUTE ON  [dbo].[Copy_TOU_Schedule_Term_All_Details] TO [CBMSApplication]
GO
