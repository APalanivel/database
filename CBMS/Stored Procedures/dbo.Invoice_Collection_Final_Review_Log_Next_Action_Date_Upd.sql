SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Invoice_Collection_Final_Review_Log_Next_Action_Date_Upd       
              
Description:              
			This sproc is used to update the status of the Final_Review.      
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
@Invoice_Collection_Activity_Id 		INT					
                           
                    
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

   Exec dbo.Invoice_Collection_Final_Review_Log_Next_Action_Date_Upd
   
   
   
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna               
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2019-08-20		Created For Invoice_Collection.         
             
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Final_Review_Log_Next_Action_Date_Upd]
     (
         @Invoice_Collection_Final_Review_Log_Id INT
         , @User_Info_Id INT
         , @Next_Action_Date DATE = NULL
     )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Invoice_Collection_Activity_Log_Id INT;

        INSERT INTO dbo.Invoice_Collection_Activity_Log
             (
                 Invoice_Collection_Activity_Id
                 , Next_Action_Dt
                 , Created_User_Id
             )
        SELECT
            iccl.Invoice_Collection_Activity_Id
            , @Next_Action_Date
            , @User_Info_Id
        FROM
            dbo.Invoice_Collection_Final_Review_Log iccl
        WHERE
            Invoice_Collection_Final_Review_Log_Id = @Invoice_Collection_Final_Review_Log_Id
            AND @Next_Action_Date IS NOT NULL;




        SELECT
            @Invoice_Collection_Activity_Log_Id = IDENT_CURRENT('Invoice_Collection_Activity_Log');

        INSERT INTO dbo.Invoice_Collection_Activity_Log_Queue_Map
             (
                 Invoice_Collection_Activity_Log_Id
                 , Invoice_Collection_Queue_Id
                 , Collection_Start_Dt
                 , Collection_End_Dt
             )
        SELECT
            @Invoice_Collection_Activity_Log_Id
            , icclqm.Invoice_Collection_Queue_Id
            , icq.Collection_Start_Dt
            , icq.Collection_End_Dt
        FROM
            dbo.Invoice_Collection_Final_Review_Log_Queue_Map icclqm
            INNER JOIN dbo.Invoice_Collection_Queue icq
                ON icq.Invoice_Collection_Queue_Id = icclqm.Invoice_Collection_Queue_Id
        WHERE
            icclqm.Invoice_Collection_Final_Review_Log_Id = @Invoice_Collection_Final_Review_Log_Id
            AND @Next_Action_Date IS NOT NULL;


        UPDATE
            icq
        SET
            icq.Next_Action_Dt = CASE WHEN @Next_Action_Date IS NOT NULL THEN @Next_Action_Date
                                     ELSE icq.Next_Action_Dt
                                 END
            , icq.Last_Change_Ts = GETDATE()
        FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN dbo.Invoice_Collection_Final_Review_Log_Queue_Map icclqm
                ON icclqm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
        WHERE
            icclqm.Invoice_Collection_Final_Review_Log_Id = @Invoice_Collection_Final_Review_Log_Id
            ;







    END;

    ;




GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Final_Review_Log_Next_Action_Date_Upd] TO [CBMSApplication]
GO
