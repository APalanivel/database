SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    Trade.Deal_Ticket_Search_By_Client
   
    
DESCRIPTION:   
   
  This procedure to get summary page details Deal Ticket Approval Queue.
    
INPUT PARAMETERS:    
      Name          DataType       Default        Description    
-----------------------------------------------------------------------------    
	 @Client_Id		INT
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	Exec Trade.Deal_Ticket_Search_By_Client @Start_Index = 1, End_Index = 15 --91
	Exec Trade.Deal_Ticket_Search_By_Client  @Client_Id = 10003 , @Start_Index = 1, @End_Index = 25
	Exec Trade.Deal_Ticket_Search_By_Client  @Deal_Ticket_Number = 131861
	Exec Trade.Deal_Ticket_Search_By_Client  @Deal_Ticket_Number = 132164
	Exec Trade.Deal_Ticket_Search_By_Client  @Trade_Number = 360

	Exec Trade.Deal_Ticket_Search_By_Client @Client_Id=1005,@Deal_Ticket_Number=150,@Site_Client_Hier_Id=1992
	
	Exec Trade.Deal_Ticket_Search_By_Client @Client_Id=10003, @Trade_Number=321
	
		Exec Trade.Deal_Ticket_Search_By_Client @Client_Id=10003, @Sort_Key = 'Deal_Ticket_Number', @Sort_Order = 'Asc'
	Exec Trade.Deal_Ticket_Search_By_Client @Client_Id=10003, @Sort_Key = 'client_Name', @Sort_Order = 'Asc'
		Exec Trade.Deal_Ticket_Search_By_Client @Client_Id=10003, @Sort_Key = 'Date_Initiated', @Sort_Order = 'Asc'
	
			Exec Trade.Deal_Ticket_Search_By_Client @Client_Id=10003, @Sort_Key = 'Deal_Ticket_Number', @Sort_Order = 'Asc'

		Exec Trade.Deal_Ticket_Search_By_Client @Client_Id=1005
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	NR          Narayana Reddy
	RR			Raghu Reddy
    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	NR          2019-02-08	GRM Proejct
	RR			2019-11-05	GRM-1574 Changed @Trade_Number input search from table Trade.Deal_Ticket_Client_Hier_TXN_Status 
							to Trade.Deal_Ticket_Client_Hier_Volume_Dtl
				2020-01-20	GRM-1595, Using Subselect to get the details from Volume Dtl
											LEFT JOIN on User_Info ( Used to get the Created user) changed to INNER JOIN 
******/
CREATE PROC [Trade].[Deal_Ticket_Search_By_Client]
    (
        @Deal_Ticket_Number NVARCHAR(255) = NULL
        , @Client_Id INT = NULL
        , @Site_Client_Hier_Id VARCHAR(MAX) = NULL
        , @Hedge_Start_Dt DATE = NULL
        , @Hedge_End_Dt DATE = NULL
        , @Workflow_Status_Id VARCHAR(MAX) = NULL
        , @Currently_With_User_Info_Id VARCHAR(MAX) = NULL
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
        , @Trade_Number NVARCHAR(255) = NULL
        , @Sort_Key VARCHAR(50) = NULL
        , @Sort_Order VARCHAR(50) = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;
        -----------------Need to modify the sort, replace case
        SELECT  @Sort_Key = ISNULL(@Sort_Key, 'Deal_Ticket_Number');
        SELECT  @Sort_Order = ISNULL(@Sort_Order, 'Desc');

        DECLARE
            @Input_Sort_Key VARCHAR(MAX) = NULL
            , @Input_Sort_Order VARCHAR(MAX) = NULL;

        CREATE TABLE #Tbl_Dt_Dtls
             (
                 Deal_Ticket_Id INT
                 , Deal_Ticket_Number NVARCHAR(255)
                 , Trade_Number VARCHAR(50)
                 , Client_Name VARCHAR(200)
                 , Sites VARCHAR(600)
                 , Date_Initiated VARCHAR(50)
                 , Deal_Type VARCHAR(50)
                 , Hedge_Type VARCHAR(200)
                 , Hedge_Start_Dt VARCHAR(20)
                 , Hedge_End_Dt VARCHAR(20)
                 , Commodity_Name VARCHAR(50)
                 , Deal_Status NVARCHAR(255)
                 , Initiated_By VARCHAR(50)
                 , Currently_With VARCHAR(50)
                 , Row_Num INT IDENTITY(1, 1)
                 , Client_Id INT
                 , Sitegroup_Id INT
                 , Site_Id INT
             );

        DECLARE
            @Total INT
            , @All_Are_Null BIT = 0;

        DECLARE
            @SQL1 VARCHAR(MAX) = ''
            , @SQL2 VARCHAR(MAX) = ''
            , @SQL3 VARCHAR(MAX) = '';

        SELECT
            @All_Are_Null = 1
        WHERE
            @Deal_Ticket_Number IS NULL
            AND @Client_Id IS NULL
            AND @Site_Client_Hier_Id IS NULL
            AND @Hedge_Start_Dt IS NULL
            AND @Hedge_End_Dt IS NULL
            AND @Currently_With_User_Info_Id IS NULL
            AND @Trade_Number IS NULL
            AND @Workflow_Status_Id IS NULL;

        SET @SQL1 = 'INSERT      INTO #Tbl_Dt_Dtls
                  ( 
                   Deal_Ticket_Id
                  ,Deal_Ticket_Number
                  ,Trade_Number
                  ,Client_Name
                  ,Sites
                  ,Date_Initiated
                  ,Deal_Type
                  ,Hedge_Type
                  ,Hedge_Start_Dt
                  ,Hedge_End_Dt
                  ,Commodity_Name
                  ,Deal_Status
                  ,Initiated_By
                  ,Currently_With
                  ,Client_Id 
				  ,Sitegroup_Id 
				  ,Site_Id
                  )
                  SELECT
                        dt.Deal_Ticket_Id
                       ,dt.Deal_Ticket_Number
                       ,CASE WHEN COUNT(DISTINCT dtchvd.Trade_Number) > 1 THEN ''Multiple''
                             ELSE CAST(MAX(dtchvd.Trade_Number) AS VARCHAR(10))
                        END AS Trade_Number
                       ,ch.Client_Name
                       ,CASE WHEN COUNT(DISTINCT ch.Site_Id) > 1 THEN ''Multiple''
                             ELSE MAX(RTRIM(ch.City) + '', '' + ch.State_Name + '' ('' + ch.Site_name + '')'')
                        END AS Sites
                       ,REPLACE(CONVERT(VARCHAR(15), dt.Created_Ts, 106), '' '', ''-'') AS Date_Initiated
                       ,atype.Code_Value + '' '' + ttype.Code_Value AS Deal_Type
                       ,ht.ENTITY_NAME AS Hedge_Type
                       ,SUBSTRING(DATENAME(month, dt.Hedge_Start_Dt), 1, 3) + ''-'' + CAST(DATEPART(year, dt.Hedge_Start_Dt) AS VARCHAR(5)) AS Hedge_Start_Dt
                       ,SUBSTRING(DATENAME(month, dt.Hedge_End_Dt), 1, 3) + ''-'' + CAST(DATEPART(year, dt.Hedge_End_Dt) AS VARCHAR(5)) AS Hedge_End_Dt
                       ,c.Commodity_Name
                       ,CASE WHEN COUNT(DISTINCT ws.Workflow_Status_Id) > 1 THEN ''Multiple''
                             ELSE MAX(ws.Workflow_Status_Name)
                        END AS Deal_Status
                       ,ui.FIRST_NAME + '' '' + ui.LAST_NAME AS Initiated_By
                       ,cui.FIRST_NAME + '' '' + cui.LAST_NAME AS Currently_With
                       ,ch.Client_Id 
						,CASE WHEN COUNT(DISTINCT ch.Site_Id) > 1 THEN -1
                             ELSE MAX(ch.Sitegroup_Id)
                        END AS Sitegroup_Id
						,CASE WHEN COUNT(DISTINCT ch.Site_Id) > 1 THEN -1
                             ELSE MAX(ch.Site_Id)
                        END AS Site_Id 
                  FROM
                        Trade.Deal_Ticket dt
                        INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                              ON dtch.Deal_Ticket_Id = dt.Deal_Ticket_Id
                        INNER JOIN Core.Client_Hier ch
                              ON ch.Client_Id = dt.Client_Id
                                 AND ch.Client_Hier_Id = dtch.Client_Hier_Id
                        INNER JOIN dbo.Commodity c
                              ON c.Commodity_Id = dt.Commodity_Id
                        INNER JOIN dbo.Workflow w
                              ON w.Workflow_Id = dt.Workflow_Id
                        INNER JOIN dbo.USER_INFO ui
							  ON ui.USER_INFO_ID = dt.Created_User_Id
						INNER JOIN dbo.Code ttype
                              ON dt.Deal_Ticket_Type_Cd = ttype.Code_Id
                        INNER JOIN dbo.Code atype
                           ON dt.Trade_Action_Type_Cd = atype.Code_Id
                        INNER JOIN dbo.ENTITY ht
                              ON dt.Hedge_Type_Cd = ht.ENTITY_ID
                        INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                              ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
                        INNER JOIN Trade.Workflow_Status_Map wsm
                              ON chws.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
                        INNER JOIN Trade.Workflow_Status ws
                              ON wsm.Workflow_Status_Id = ws.Workflow_Status_Id
                        LEFT JOIN dbo.USER_INFO cui
                              ON cui.QUEUE_ID = dt.QUEUE_ID
						INNER JOIN (
									   SELECT
										   vd.Deal_Ticket_Id
										  ,vd.Trade_Number
									   FROM
										   Trade.Deal_Ticket_Client_Hier_Volume_Dtl vd
									   GROUP BY vd.Deal_Ticket_Id
											   ,vd.Trade_Number
								   ) dtchvd
							ON dtchvd.Deal_Ticket_Id = dt.Deal_Ticket_Id';


        SET @Input_Sort_Order = CASE WHEN @Sort_Order = 'Asc' THEN ' Asc'
                                    WHEN @Sort_Order = 'Desc' THEN ' Desc'
                                END;

        SET @Input_Sort_Key = CASE WHEN @Sort_Key = 'Deal_Ticket_Number' THEN 'dt.Deal_Ticket_Id'
                                  WHEN @Sort_Key = 'Client_Name' THEN 'ch.Client_Name'
                                  WHEN @Sort_Key = 'Trade_Number' THEN
                                      ' CASE WHEN COUNT(DISTINCT dtchvd.Trade_Number) > 1 THEN ''Multiple''
																			 ELSE CAST(MAX(dtchvd.Trade_Number) AS VARCHAR(10))
																		END'
                                  WHEN @Sort_Key = 'Sites' THEN
                                      'CASE WHEN COUNT(DISTINCT ch.Site_Id) > 1 THEN ''Multiple''
																 ELSE MAX(RTRIM(ch.City) + '', '' + ch.State_Name + '' ('' + ch.Site_name + '')'')
																END'
                                  WHEN @Sort_Key = 'Deal_Type' THEN 'atype.Code_Value + '' '' + ttype.Code_Value'
                                  WHEN @Sort_Key = 'Deal_Status' THEN
                                      'CASE WHEN COUNT(DISTINCT ws.Workflow_Status_Id) > 1 THEN ''Multiple''
																		 ELSE MAX(ws.Workflow_Status_Name)
																	END'
                                  WHEN @Sort_Key = 'Currently_With' THEN 'cui.FIRST_NAME + '' '' + cui.LAST_NAME'
                                  WHEN @Sort_Key = 'Date_Initiated' THEN 'dt.Created_Ts'
                                  WHEN @Sort_Key = 'Hedge_Start_Dt' THEN 'dt.Hedge_Start_Dt'
                                  WHEN @Sort_Key = 'Hedge_End_Dt' THEN 'dt.Hedge_End_Dt'
                              END;

        SET @SQL2 = @SQL2
                    + CASE WHEN @All_Are_Null = 1 THEN
                               ' AND ws.Workflow_Status_Name NOT	IN ( ''Closed'', ''Order Executed'', ''Expired'', ''Canceled'',''Completed'' )'
                          ELSE ''
                      END;
        SET @SQL2 = @SQL2
                    + CASE WHEN @All_Are_Null = 0
                                AND   @Deal_Ticket_Number IS NOT NULL THEN
                               ' AND dt.Deal_Ticket_Id =  ' + CAST(@Deal_Ticket_Number AS VARCHAR(20))
                          ELSE ''
                      END;
        SET @SQL2 = @SQL2
                    + CASE WHEN @All_Are_Null = 0
                                AND   @Client_Id IS NOT NULL THEN
                               ' AND ch.Client_Id = ' + CAST(@Client_Id AS VARCHAR(20))
                          ELSE ''
                      END;
        SET @SQL2 = @SQL2
                    + CASE WHEN @All_Are_Null = 0
                                AND   @Site_Client_Hier_Id IS NOT NULL THEN
                               ' AND EXISTS ( SELECT
                                                1
                                               FROM
                                                dbo.ufn_split(''' + @Site_Client_Hier_Id + ''', '','')'
                               + '
                                               WHERE
                                                CAST(Segments AS INT) = ch.Site_Id  ) '
                          ELSE ''
                      END;
        SET @SQL2 = @SQL2
                    + CASE WHEN @All_Are_Null = 0
                                AND   @Hedge_Start_Dt IS NOT NULL
                                AND   @Hedge_End_Dt IS NULL THEN
                               ' AND ''' + CAST(@Hedge_Start_Dt AS VARCHAR(20))
                               + ''' between dt.Hedge_Start_Dt AND dt.Hedge_End_Dt '
                          ELSE ''
                      END;
        SET @SQL2 = @SQL2
                    + CASE WHEN @All_Are_Null = 0
                                AND   @Hedge_Start_Dt IS NULL
                                AND   @Hedge_End_Dt IS NOT NULL THEN
                               ' AND ''' + CAST(@Hedge_End_Dt AS VARCHAR(20))
                               + ''' between dt.Hedge_Start_Dt and dt.Hedge_End_Dt '
                          ELSE ''
                      END;
        SET @SQL2 = @SQL2
                    + CASE WHEN @All_Are_Null = 0
                                AND   @Hedge_Start_Dt IS NOT NULL
                                AND   @Hedge_End_Dt IS NOT NULL THEN
                               ' AND ' + '( ''' + CAST(@Hedge_Start_Dt AS VARCHAR(20))
                               + ''' between dt.Hedge_Start_Dt and dt.Hedge_End_Dt OR '
                               + '
                                    '''                                    + CAST(@Hedge_End_Dt AS VARCHAR(20))
                               + ''' between dt.Hedge_Start_Dt and dt.Hedge_End_Dt 
                                    OR  dt.Hedge_Start_Dt BETWEEN '        + ''''
                               + CAST(@Hedge_Start_Dt AS VARCHAR(20)) + '''' + ' AND '''
                               + CAST(@Hedge_End_Dt AS VARCHAR(20))
                               + '''
                                    OR  dt.Hedge_End_Dt BETWEEN '          + ''''
                               + CAST(@Hedge_Start_Dt AS VARCHAR(20)) + '''' + ' AND '''
                               + CAST(@Hedge_End_Dt AS VARCHAR(20)) + ''' )'
                          ELSE ''
                      END;
        SET @SQL2 = @SQL2
                    + CASE WHEN @All_Are_Null = 0
                                AND   @Workflow_Status_Id IS NOT NULL THEN
                               ' AND EXISTS ( SELECT
                                                1
                                               FROM
                                                dbo.ufn_split(''' + @Workflow_Status_Id + ''', '','')'
                               + '
                                               WHERE
                                                CAST(Segments AS INT) = ws.Workflow_Status_Id )  '
                          ELSE ''
                      END;
        SET @SQL2 = @SQL2
                    + CASE WHEN @All_Are_Null = 0
                                AND   @Currently_With_User_Info_Id IS NOT NULL THEN
                               ' AND EXISTS ( SELECT
                                                1
                                               FROM
                                                dbo.ufn_split(''' + @Currently_With_User_Info_Id
                               + ''', '','') usid
                                                        INNER JOIN dbo.USER_INFO cui
                    ON CAST(usid.Segments AS INT) = cui.USER_INFO_ID'
                               + '
                                     WHERE
														cui.QUEUE_ID = dt.QUEUE_ID )  '
                          ELSE ''
                      END;
        SET @SQL2 = @SQL2
                    + CASE WHEN @All_Are_Null = 0
                                AND   @Trade_Number IS NOT NULL THEN
                               ' AND dtchvd.Trade_Number = ' + CAST(@Trade_Number AS VARCHAR(20))

                          ELSE ''
                      END;

        SET @SQL3 = ' GROUP BY
                        dt.Deal_Ticket_Id
                       ,dt.Deal_Ticket_Number
                       ,ch.Client_Name
                       ,dt.Created_Ts
                       ,atype.Code_Value + '' '' + ttype.Code_Value
                       ,ht.ENTITY_NAME
                       ,dt.Hedge_Start_Dt
                       ,dt.Hedge_End_Dt
                       ,c.Commodity_Name
                       ,ui.FIRST_NAME + '' '' + ui.LAST_NAME
                       ,cui.FIRST_NAME + '' '' + cui.LAST_NAME
                       ,ch.Client_Id ';

        --PRINT ( @SQL1 + ' WHERE chws.Is_Active = 1 ' + ' ' + @SQL2 + ' ' + @SQL3 + ' ORDER BY  ' + @Input_Sort_Key + ' ' + @Input_Sort_Order ) 
        EXEC (@SQL1 + ' WHERE chws.Is_Active = 1 ' + ' ' + @SQL2 + ' ' + @SQL3 + ' ORDER BY  ' + @Input_Sort_Key + ' ' + @Input_Sort_Order);

        SELECT  @Total = MAX(Row_Num)FROM   #Tbl_Dt_Dtls;

        SELECT
            Deal_Ticket_Id
            , Deal_Ticket_Number
            , Trade_Number
            , Client_Name
            , Sites
            , Date_Initiated
            , Deal_Type
            , Hedge_Type
            , Hedge_Start_Dt
            , Hedge_End_Dt
            , Commodity_Name
            , Deal_Status
            , Initiated_By
            , Currently_With
            , Row_Num
            , @Total AS Total
            , Client_Id
            , Sitegroup_Id
            , Site_Id
        FROM
            #Tbl_Dt_Dtls
        WHERE
            Row_Num BETWEEN @Start_Index
                    AND     @End_Index
        ORDER BY
            Row_Num;

			DROP TABLE #Tbl_Dt_Dtls 
    END;
GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Search_By_Client] TO [CBMSApplication]
GO
