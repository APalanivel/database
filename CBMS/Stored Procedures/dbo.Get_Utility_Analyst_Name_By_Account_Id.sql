
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:

	dbo.Get_Utility_Analyst_Name_By_Account_Id
            
DESCRIPTION:            
 Procedure to get the details of the invoice control analysts for the given contract
             
INPUT PARAMETERS:            
 Name          DataType Default  Description              
------------------------------------------------------------              
 @Contract_Id  INT  
             
OUTPUT PARAMETERS:              
 Name		   DataType Default  Description              
------------------------------------------------------------           
             
USAGE EXAMPLES:            
------------------------------------------------------------              
  
EXEC dbo.Get_Utility_Analyst_Name_By_Account_Id 95248
EXEC dbo.Get_Utility_Analyst_Name_By_Account_Id 95247
EXEC dbo.Get_Utility_Analyst_Name_By_Account_Id 95339
EXEC dbo.Get_Utility_Analyst_Name_By_Account_Id  2178
EXEC dbo.Get_Utility_Analyst_Name_By_Account_Id  4641--supplier account with multiple utility accounts
EXEC dbo.Get_Utility_Analyst_Name_By_Account_Id  8008
              
AUTHOR INITIALS:              
 Initials	Name              
------------------------------------------------------------              
 HG			Harihara Suthan G
             
 MODIFICATIONS
 Initials Date		 Modification              
------------------------------------------------------------              
 HG		  2013-05-14 Created for Maint-1816
 SP		  2014-09-02 for Data Transition, Added condition gi.Is_Shown_On_VEN_CC_List = 1 in WHERE Clause.
 SP		  2014-10-15 Maint-3120,Logic to get first Utility account mapped to supplier account.
******/

CREATE PROCEDURE [dbo].[Get_Utility_Analyst_Name_By_Account_Id] ( @Account_Id INT )
AS 
BEGIN
	
      SET NOCOUNT ON

      DECLARE @Temp_Account_Id INT= @Account_Id
		
      SELECT TOP 1
            @Temp_Account_Id = ucha.Account_Id
      FROM
            core.Client_Hier_Account cha
            JOIN core.Client_Hier_Account ucha
                  ON cha.Meter_Id = ucha.Meter_Id
      WHERE
            cha.Account_Type = 'supplier'
            AND ucha.Account_Type = 'Utility'
            AND cha.ACCOUNT_ID = @Account_Id
                      
      SELECT
            gi.GROUP_NAME
           ,udam.Analyst_ID
           ,ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME AS Analyst_Name
           ,ui.EMAIL_ADDRESS
      FROM
            dbo.Account ac
            INNER JOIN dbo.UTILITY_DETAIL ud
                  ON ud.VENDOR_ID = ac.VENDOR_ID
            INNER JOIN dbo.Utility_Detail_Analyst_Map udam
                  ON udam.Utility_Detail_ID = ud.UTILITY_DETAIL_ID
            INNER JOIN dbo.GROUP_INFO gi
                  ON gi.GROUP_INFO_ID = udam.Group_Info_ID
            INNER JOIN dbo.USER_INFO ui
                  ON ui.USER_INFO_ID = udam.Analyst_ID
      WHERE
            ac.Account_Id = @Temp_Account_Id
            AND gi.Is_Shown_On_VEN_CC_List = 1
    

END;



;
GO


GRANT EXECUTE ON  [dbo].[Get_Utility_Analyst_Name_By_Account_Id] TO [CBMSApplication]
GO
