SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[SR_RFP_DELETE_COMMUNICATIONS_P] 
	@cbmsImageId INT,
	@rfpId INT
AS
BEGIN

	DELETE FROM dbo.sr_rfp_communications 
	WHERE sr_rfp_id = @rfpId 
		AND cbms_image_id = @cbmsImageId

	/*delete from 
		cbms_image 
			where cbms_image_id = @cbmsImageId */

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_DELETE_COMMUNICATIONS_P] TO [CBMSApplication]
GO
