SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BulkProcess_UpdateBatchDtl]  
     (  
         @batch_detail_id INT  
         , @Status_Cd [INT]  
         , @Error_Desc NVARCHAR(MAX) = NULL  
     )  
AS  
    BEGIN  
        SET NOCOUNT ON;  
		
		select @Error_Desc = case when '' <> ltrim(rtrim(@Error_Desc)) 
								then @Error_Desc 
								else null 
								end
  
        UPDATE  
            [lxbipbd]  
        SET  
            [lxbipbd].[Status_Cd] = @Status_Cd  
            , [lxbipbd].[Error_Msg] = CASE WHEN [lxbipbd].[Error_Msg] IS NOT NULL  
                                                AND @Error_Desc IS NOT NULL THEN  
                                               [lxbipbd].[Error_Msg] + ' ' + @Error_Desc  
                                          WHEN [lxbipbd].[Error_Msg] IS NULL  
                                               AND  @Error_Desc IS NOT NULL THEN @Error_Desc  
                                          ELSE @Error_Desc  
                                      END  
        FROM  
            [LOGDB].[dbo].[XL_Bulk_Invoice_Process_Batch_Dtl] AS [lxbipbd]  
        WHERE  
            [lxbipbd].[XL_Bulk_Data_Process_Batch_Dtl_Id] = @batch_detail_id;  
  
  
    END;  
  
  
  
GO
GRANT EXECUTE ON  [dbo].[BulkProcess_UpdateBatchDtl] TO [CBMSApplication]
GO
