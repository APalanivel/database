SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Contract_Del]  

DESCRIPTION: It Deletes Contract for Selected Contract Id.     
      
INPUT PARAMETERS:          
	NAME				DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Contract_Id		INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
	EXEC Contract_Del 20149
  Rollback Tran

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			24-JUN-10	CREATED

*/

CREATE PROCEDURE dbo.Contract_Del
    (
      @Contract_Id INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.CONTRACT
	WHERE
		Contract_Id = @Contract_Id

END
GO
GRANT EXECUTE ON  [dbo].[Contract_Del] TO [CBMSApplication]
GO
