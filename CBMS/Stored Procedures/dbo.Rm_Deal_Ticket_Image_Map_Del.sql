SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: DBO.Rm_Deal_Ticket_Image_Map_Del  

DESCRIPTION: It Deletes Rm Deal Ticket Image Map Del for Selected Rm Deal Ticket Image Map Id.
      
INPUT PARAMETERS:          
	NAME							DATATYPE	DEFAULT		DESCRIPTION         
-------------------------------------------------------------------
	@Rm_Deal_Ticket_Image_Map_Id	INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  	BEGIN TRAN
		EXEC dbo.rm_deal_ticket_image_map_del  228
	ROLLBACK TRAN	
	
	SELECT
		Rm_Deal_Ticket_Image_Map_Id
	FROM
		dbo.rm_deal_ticket_image_map
	WHERE
		Rm_Deal_Ticket_Image_Map_Id =  228

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			31-AUG-10	CREATED

*/

CREATE PROCEDURE dbo.Rm_Deal_Ticket_Image_Map_Del
    (
      @Rm_Deal_Ticket_Image_Map_Id	INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.Rm_Deal_Ticket_Image_Map
	WHERE
		Rm_Deal_Ticket_Image_Map_Id = @Rm_Deal_Ticket_Image_Map_Id
		
END
GO
GRANT EXECUTE ON  [dbo].[Rm_Deal_Ticket_Image_Map_Del] TO [CBMSApplication]
GO
