SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/******
NAME:	dbo.[Cbms_Image_Sel_By_Utility_Id ]


DESCRIPTION:

	To select the cbms image uploaded for the utility
	Output of this SP will be passed to the image server application to remove the entries from CBMS_IMAGE table and remove the file from file system.
	
INPUT PARAMETERS:
     Name                 DataType          Default     Description
------------------------------------------------------------------  
	@Vendor_Id				INT							Utility Vendor_id

OUTPUT PARAMETERS:
      Name              DataType          Default     Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Cbms_Image_Sel_By_Utility_Id 10
	EXEC dbo.Cbms_Image_Sel_By_Utility_Id 12

	SELECT
		*
	FROM
		Utility_Profile

	SELECT
		*
	FROM
		Utility_Document

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	GK			Gopi Konga

MODIFICATIONS

	Initials	Date			Modification
------------------------------------------------------------
	  GK		05/05/2011		CREATED

******/

CREATE PROCEDURE [dbo].[Cbms_Image_Sel_By_Utility_Id] ( @Vendor_Id INT )
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE @Cbms_Image_Id_List TABLE ( Cbms_Image_Id INT )

      INSERT INTO
            @Cbms_Image_Id_List ( Cbms_Image_Id )
            SELECT
                  Cbms_Image_Id
            FROM
                  dbo.Utility_Profile
            WHERE
                  Vendor_Id = @Vendor_Id

      INSERT INTO
            @Cbms_Image_Id_List ( Cbms_Image_Id )
            SELECT
                  Cbms_Image_Id
            FROM
                  dbo.Utility_Document
            WHERE
                  Vendor_Id = @Vendor_Id

      SELECT
            Cbms_Image_Id
      FROM
            @Cbms_Image_Id_List

END

GO
GRANT EXECUTE ON  [dbo].[Cbms_Image_Sel_By_Utility_Id] TO [CBMSApplication]
GO
GO