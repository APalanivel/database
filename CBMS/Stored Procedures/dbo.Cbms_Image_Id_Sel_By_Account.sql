SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Cbms_Image_Id_Sel_By_Account]  
     
DESCRIPTION:

	To Get ONLY Cancel renegotiation document image and Meter tax exemption image list of the selected account.
	
	Used by Delete Utility Account tool , image id selected by this procedure will be deleted via image server.
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Account_Id		INT

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Cbms_Image_Id_Sel_By_Account  9538
	
	EXEC dbo.Cbms_Image_Id_Sel_By_Account  9986
	
	SELECT
		*
	FROM
		SR_Cancel_Renegotiation_Document
	WHERE
		Account_Id = 9538
			
AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
HG			Harihara Suthan G

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
HG			06/08/2010	Created

*/

CREATE PROCEDURE dbo.Cbms_Image_Id_Sel_By_Account
	@Account_id		INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Cbms_Image TABLE(Cbms_image_Id INT)

	INSERT INTO @Cbms_Image(Cbms_image_Id)
	SELECT
		Cbms_Image_Id
	FROM
		dbo.SR_CANCEL_RENEGOTIATION_DOCUMENT
	WHERE
		ACCOUNT_ID = @Account_id

	INSERT INTO @Cbms_Image(Cbms_image_Id)
	SELECT
		mcm.CBMS_IMAGE_ID
	FROM
		dbo.METER m
		JOIN dbo.METER_CBMS_IMAGE_MAP mcm
			 ON mcm.METER_ID = m.METER_ID
	WHERE
		 m.Account_Id = @Account_Id

	SELECT
		Cbms_Image_Id
	FROM
		@Cbms_Image

END
GO
GRANT EXECUTE ON  [dbo].[Cbms_Image_Id_Sel_By_Account] TO [CBMSApplication]
GO
