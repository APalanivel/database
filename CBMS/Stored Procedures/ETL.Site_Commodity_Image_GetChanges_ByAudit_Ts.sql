SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          
NAME:   etl.Site_Commodity_Image_GetChanges_ByAudit_Ts       

    
DESCRIPTION: 
	Gets all Site_Commodity_Image records that have changed
	This is used by the IP SSIS Process. 
	       
	
      
INPUT PARAMETERS:          
Name			DataType	Default		Description          
------------------------------------------------------------          
@Audit_Start_Ts	DateTime
@AUdit_End0Ts	DateTime
                
OUTPUT PARAMETERS:          
Name			DataType	Default		Description          
------------------------------------------------------------ 

         
USAGE EXAMPLES:          
------------------------------------------------------------ 

       
     
AUTHOR INITIALS:          
Initials	Name          
------------------------------------------------------------          
CMH			Chad Hattabaugh  
     
     
MODIFICATIONS           
Initials	Date	Modification          
------------------------------------------------------------          
CMH			10/06/2010	Created
*****/ 
CREATE PROCEDURE etl.Site_Commodity_Image_GetChanges_ByAudit_Ts
(	@Audit_Start_Ts		DATETIME
	,@Audit_End_Ts		DATETIME		) 
AS 
BEGIN 
	SET NOCOUNT ON; 

	WITH cte_Audit
	AS (	SELECT 
				max(Audit_Ts) as Max_Audit_Ts 
				,Site_Id
				,Commodity_Id
				,Service_Month
			FROM 
				Site_Commodity_Image_Audit adt
			WHERE 
				Audit_Ts between @Audit_Start_Ts AND @Audit_End_Ts
			GROUP BY 
				Site_Id
				,Commodity_Id
				,Service_Month )
	SELECT
		adt.Site_Id
		,adt.Commodity_Id
		,adt.Service_Month
		,adt.Audit_Ts
		,min(adt.Audit_Function)		as Audit_Function
		,sci.CBMS_Image_Id
	FROM 
		cte_Audit cte
		INNER JOIN Site_Commodity_Image_Audit adt 
			ON cte.Site_Id = adt.Site_Id
				AND cte.Commodity_Id = adt.Commodity_Id
				AND cte.Service_Month = adt.Service_Month
				AND cte.Max_Audit_Ts = adt.Audit_Ts
		LEFT JOIN Site_Commodity_Image sci
			ON adt.Site_Id = sci.SITE_ID
				AND adt.Commodity_Id = sci.Commodity_Id
				AND adt.Service_Month = sci.SERVICE_MONTH
	GROUP BY 
		adt.Site_Id
		,adt.Commodity_Id
		,adt.Service_Month
		,adt.Audit_Ts
		,sci.CBMS_IMAGE_ID
END

GO
GRANT EXECUTE ON  [ETL].[Site_Commodity_Image_GetChanges_ByAudit_Ts] TO [CBMSApplication]
GRANT EXECUTE ON  [ETL].[Site_Commodity_Image_GetChanges_ByAudit_Ts] TO [ETL_Execute]
GO
