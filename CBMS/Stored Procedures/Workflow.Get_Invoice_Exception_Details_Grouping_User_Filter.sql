SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
















  
  
    
/******                
NAME:    [Workflow].[Get_Invoice_Exception_Details_Grouping_User_Filter]            
DESCRIPTION: THIS STORED PROCEDURE IS CREATED TO get the invoice exception details based on the group id             
------------------------------------------------------------               
 INPUT PARAMETERS:                
 Name   DataType  Default Description              
 @MODULE_ID int              
   ,@Workflow_Queue_Search_Filter_Group_id int   -- group id from application              
   ,@Logged_In_User int              
   ,@User_Info_Id    varchar (max)   ---  User selected from drop down Id 49,1,2,3              
   ,@Exception_Type   varchar (max)              
   ,@Account_Number   varchar(500)              
   ,@Client     varchar(500)              
   ,@Site      varchar(500)              
   ,@Country     varchar(500)              
   ,@State      varchar(500)              
   ,@City      varchar(500)               
   ,@Commodity     varchar(500)              
   ,@Invoice_ID    varchar(500)              
   ,@Priority     varchar(500)   --- Yes              
   ,@Status     varchar(500)               
   ,@COMMENTS     VARCHAR(500)              
   ,@Start_Date_in_Queue  Datetime              
   ,@End_Date_in_Queue   Datetime              
   ,@Start_Date_in_CBMS  Datetime              
   ,@End_Date_in_CBMS   Datetime               
   ,@Data_Source    Varchar(100)              
   ,@Vendor     Varchar(100)              
   ,@Vendor_Type    varchar(100)              
   ,@Month      datetime              
   ,@Filename      Varchar(100)              
   ,@startindex    int=1              
   ,@endindex     int=2147483647              
   ,@ONLY_COUNT_NEEDED   BIT              
                 
------------------------------------------------------------                
 OUTPUT PARAMETERS:                
 Name   DataType  Default Description               
 @TOTAL_COUNT    INT OUTPUT             
------------------------------------------------------------                
 USAGE EXAMPLES:                
------------------------------------------------------------                
AUTHOR INITIALS:                
Initials Name                
------------------------------------------------------------                
AKP   ARUNKUMAR PALANIVEL Summit Energy             
TRK   RAMAKRISHNA THUMMALA Summit Energy          
 MODIFICATIONS                 
 Initials Date   Modification                
------------------------------------------------------------                
 AKP    SEP 5,2019 Created            
 TRK   15-Oct-2019 Updated the @Queue_id = '-2' to  @Queue_id = '-1,0'          
******/              
              
CREATE PROCEDURE [Workflow].[Get_Invoice_Exception_Details_Grouping_User_Filter]                
    -- Add the parameters for the stored procedure here                   
    @MODULE_ID INT,                
    @Workflow_Queue_Search_Filter_Group_id INT = NULL, -- group id from application                   
    @Queue_id VARCHAR(MAX) = NULL,                     ---  User selected from drop down Id 49,1,2,3                  
    @Exception_Type VARCHAR(MAX) = NULL,                
    @Account_Number VARCHAR(500) = NULL,                
    @Client VARCHAR(500) = NULL,                
    @Site VARCHAR(500) = NULL,                
    @Country VARCHAR(500) = NULL,                
    @State VARCHAR(500) = NULL,                
    @City VARCHAR(500) = NULL,                
    @Commodity VARCHAR(500) = NULL,                
    @Invoice_ID VARCHAR(500) = NULL,                
    @Priority INT = NULL,                                 --- Yes                  
    @Exception_Status INT = NULL,                
    @COMMENTS VARCHAR(500) = NULL,                
    @Start_Date_in_Queue DATETIME = NULL,                
    @End_Date_in_Queue DATETIME = NULL,                
    @Start_Date_in_CBMS DATETIME = NULL,                
    @End_Date_in_CBMS DATETIME = NULL,                
    @Data_Source INT = NULL,                
    @Vendor VARCHAR(100) = NULL,                
    @Vendor_Type VARCHAR(100) = NULL,                
    @Month DATETIME = NULL,                
    @Filename VARCHAR(100) = NULL,                
    @startindex INT = 1,                
    @endindex INT = 2147483647,                
    @ONLY_COUNT_NEEDED INT = NULL,                
    @Sorting_Column_name VARCHAR(200) = NULL,          --send workflow.workflow_queue_output_column -> Data_Source_Column_Name                
    @Sorting_Type VARCHAR(10) = NULL,                  --- send 1 or 0. 1 - Asc, 0 - Desc                
    @TOTAL_COUNT INT OUTPUT                
AS              
BEGIN -- SET NOCOUNT ON added to prevent extra result sets from                   
    -- interfering with SELECT statements.                   
    DECLARE @Proc_name VARCHAR(100) = 'Get_Invoice_Exception_Details_Grouping_User_Filter',                
            @Input_Params VARCHAR(1000),                
            @Error_Line INT,                
            @Error_Message VARCHAR(3000),                
			@SQL NVARCHAR(MAX),    
			@SQL1 NVARCHAR(MAX),              
            @SQL2 NVARCHAR(MAX),   
			@SQL3 NVARCHAR(MAX),                 
            @Data_Source_Column_Name VARCHAR(100);  
			
			 IF (@Start_Date_in_Queue is NULL AND @End_Date_in_Queue IS NOT null)
   begin
   
   SET @Start_Date_in_Queue = '2000-01-01 00:00:00'  
   end

    IF (@Start_Date_in_Queue is NOT NULL AND @End_Date_in_Queue IS  null)
	begin
   
   SET @End_Date_in_Queue =  CONVERT(VARCHAR(19), GETDATE(), 120) 
   END
   
      IF (@Start_Date_in_CBMS is NULL AND @End_Date_in_cbms IS NOT null)
   begin
   
   SET @Start_Date_in_CBMS = '2000-01-01 00:00:00'  
   end

    IF (@Start_Date_in_CBMS is NOT NULL AND @End_Date_in_cbms IS  null)
	begin
   
   SET @End_Date_in_cbms =  CONVERT(VARCHAR(19), GETDATE(), 120) 
   end    
       
   set @End_Date_in_Queue = dateadd (d, 1, @End_Date_in_Queue )       
   set @End_Date_in_CBMS =dateadd (d,1 , @End_Date_in_CBMS ) 
   
    if (isnull(@Sorting_Column_name,'') ='')
   set @Sorting_Column_name  ='Inv Id'
   
   if (isnull(@Sorting_Type,'') ='')
   set @Sorting_Type ='asc'         
                 
        SET @Data_Source_Column_Name =                  
    (                  
        SELECT case when Data_Source_Column_Name  ='Data Source' then '[Data Source]'        
  else Data_Source_Column_Name        
  end                
        FROM Workflow.Workflow_Queue_Output_Column                  
        WHERE Display_Column_Name = @Sorting_Column_name    
		AND workflow_queue_id = @MODULE_ID              
    );          
                
                
    SELECT @Input_Params                
        = '@Workflow_Queue_Search_Filter_Group_Id: ' + CAST(@Workflow_Queue_Search_Filter_Group_id AS VARCHAR);                
    SET NOCOUNT ON;                
                
                
                
    BEGIN TRY                
                
        IF (@Queue_id IS NOT NULL)                
        BEGIN                
            SELECT CAST(Segments AS INT) AS User_info_id                
            INTO #temp_user_info                
            FROM dbo.ufn_split(@Queue_id, ',');                
        END;                
                
        IF (@Exception_Type IS NOT NULL)                
        BEGIN                
            SELECT CAST(Segments AS INT) AS exception_type                
            INTO #temp_Exception_Type                
            FROM dbo.ufn_split(@Exception_Type, ',');                
        END;                
                
                
                
        --IF (@Commodity IS NOT NULL)                
        --BEGIN                
        --    SELECT CAST(Segments AS INT) AS commodity_id                
        --    INTO #temp_Commodity                
        --    FROM dbo.ufn_split(@Commodity, ',');                 
        --END;                
                
                
        CREATE TABLE #DEFAULTEXCEPTIONS                
        (                
            UserGroupId INT NULL,                
            UserGroupName VARCHAR(300) NULL,                
            usergroupcount INT NULL,                
            systemgroupid INT NULL,                
            SystemGroupCount INT NULL,                
            ubmid INT NULL,                
            ubmaccountcode VARCHAR(200) NULL,                
            [RN] [BIGINT] NULL,                
            [CU_INVOICE_ID] [INT] NULL,                
            [ACCOUNT] [VARCHAR](MAX) NULL,                
            [Account_Id] varchar(50) NULL,                
            [EXCEPTION_TYPE] [VARCHAR](MAX) NULL,                
            [EXCEPTION_STATUS_TYPE] [VARCHAR](MAX) NULL,                
            [ASSIGNEE] [VARCHAR](MAX) NULL,                
            [CLIENTID] INT NULL,                
            [CLIENT] [VARCHAR](MAX) NULL,             
            [COMMODITY] [VARCHAR](MAX) NULL,                
            [DATA SOURCE] [VARCHAR](MAX) NULL,                
            [DATE_IN_QUEUE] [DATETIME] NULL,                
            [DATE_IN_CBMS] [DATETIME] NULL,                
            [FILEID] INT NULL,                
            [FILENAME] [VARCHAR](MAX) NULL,                
            [SERVICE_MONTH] [VARCHAR](MAX) NULL,                
            [SITE] [VARCHAR](MAX) NULL,                
            [COUNTRY] [NVARCHAR](MAX) NULL,                
            [STATE] [NVARCHAR](MAX) NULL,                
            [VENDOR] [NVARCHAR](MAX) NULL,                
            [VENDOR_TYPE] [NVARCHAR](MAX) NULL,                
            [COMMENTS] VARCHAR(MAX) NULL,                
            [is_priority] INT ,
			[priority_cd] numeric (10,0)                 
        );                
                
                
        CREATE TABLE #DEFAULTEXCEPTIONSGROUPDETAILS                
        (             
            index_range INT NULL,                
            UserGroupId INT NULL,                
            UserGroupName VARCHAR(300) NULL,                
            usergroupcount INT NULL,                
            systemgroupid INT NULL,                
            SystemGroupCount INT NULL,                
            ubmid INT NULL,                
  ubmaccountcode VARCHAR(200) NULL,                
            [CU_INVOICE_ID] INT NULL,                
            [ACCOUNT] [VARCHAR](MAX) NULL,                
            [Account_Id] varchar(50) NULL,                
            [EXCEPTION_TYPE] [VARCHAR](MAX) NULL,                
            [EXCEPTION_STATUS_TYPE] [VARCHAR](MAX) NULL,                
            [ASSIGNEE] [VARCHAR](MAX) NULL,                
            [CLIENTID] INT NULL,                
            [CLIENT] [VARCHAR](MAX) NULL,                
 [COMMODITY] [VARCHAR](MAX) NULL,                
            [DATA SOURCE] [VARCHAR](MAX) NULL,                
            [DATE_IN_QUEUE] [DATETIME] NULL,                
            [DATE_IN_CBMS] [DATETIME] NULL,                
            [FILEID] INT NULL,                
            [FILENAME] [VARCHAR](MAX) NULL,                
            [SERVICE_MONTH] [VARCHAR](MAX) NULL,                
            [SITE] [VARCHAR](MAX) NULL,                
           [COUNTRY] [NVARCHAR](MAX) NULL,                
            [STATE] [NVARCHAR](MAX) NULL,                
            [VENDOR] [NVARCHAR](MAX) NULL,                
            [VENDOR_TYPE] [NVARCHAR](MAX) NULL,                
            [COMMENTS] VARCHAR(MAX) NULL,                
            [is_priority] INT ,
			[priority_cd] numeric (10,0)                 
        );                
                
                
                
        SET @sql1                
            = '   insert #DEFAULTEXCEPTIONS                  
     SELECT                   
   wqg.Workflow_Queue_Search_Filter_Group_Id,                  
   wqg.Filter_Group_Name,                
   null,                 
   null,                
   null,                
    CI.UBM_ID,                
    CI.UBM_ACCOUNT_CODE,                
     ROW_NUMBER ()OVER(PARTITION BY CUEX.CU_INVOICE_ID ORDER BY (select null)    ) AS RN,                   
   CUEX.CU_INVOICE_ID                  
    ,  ( CASE WHEN CUEX.EXCEPTION_TYPE = ' + '''' + 'WRONG ACCOUNT' + ''''                    
              + ' THEN NULL                      
             WHEN AG.ACCOUNT_GROUP_ID IS NULL                      
                   THEN COALESCE(CHA.Account_Number, CUEX.UBM_Account_Number,INV_LBL.ACCOUNT_NUMBER)                      
             ELSE COALESCE( AG.GROUP_BILLING_NUMBER,CHA.ACCOUNT_NUMBER, CUEX.UBM_ACCOUNT_NUMBER,INV_LBL.account_number)                           
        END ) AS ACCOUNT                 ,(CASE WHEN CUEX.EXCEPTION_TYPE = ' + '''' + 'WRONG ACCOUNT' + ''''                    
              + ' THEN NULL  
			  when    AG.ACCOUNT_GROUP_ID IS NOT NULL then '+''''+'MULTIPLE'+''''+           
        'ELSE COALESCE(cast(CHA.Account_Id as varchar), cast(CUEX.Account_Id as varchar))   
        END ) AS ACCOUNT_ID          
    , CUEX.EXCEPTION_TYPE                  
    , CUEX.EXCEPTION_STATUS_TYPE                   
    , UI.FIRST_NAME + ' + '''' + ' ' + ''''                
              + '+ UI.LAST_NAME ASSIGNEE                  
   , CASE WHEN CUEX.CLIENT_HIER_ID IS NOT NULL                  
                 THEN CH.Client_Id                  
           ELSE  CI.CLient_id                  
      END CLIENT_ID                  
    , CASE WHEN CUEX.CLIENT_HIER_ID IS NOT NULL                  
                 THEN CH.CLIENT_NAME                  
           ELSE  coalesce(CL.CLient_Name,CUEX.UBM_CLIENT_NAME , INV_LBL.CLIENT_NAME)                 
      END CLIENT                   
    , ISNULL(COM.COMMODITY_NAME , INV_LBL.COMMODITY) AS  COMMODITY                  
    , UBM.UBM_NAME [DATA SOURCE]                  
    , CUEX.DATE_IN_QUEUE                  
    , CUEX.DATE_IN_CBMS                  
 , CUEX.CBMS_IMAGE_ID [FILEID]                
    , CUEX.CBMS_DOC_ID [FILENAME]                  
    , ( CASE WHEN CUEX.EXCEPTION_TYPE = ' + '''' + 'WRONG MONTH' + ''''                
              + 'THEN NULL                  
             WHEN AG.ACCOUNT_GROUP_ID IS NULL and SM.SERVICE_MONTH is not null THEN CONVERT(VARCHAR(10),  SM.SERVICE_MONTH , 101)                  
             when CUEX.SERVICE_MONTH is not null then CUEX.SERVICE_MONTH               
    else null                
                     
        END ) AS SERVICE_MONTH                  
    , CASE WHEN CUEX.CLIENT_HIER_ID IS NOT NULL                  
       THEN isnull(CH.SITE_NAME , inv_lbl.site_name)                 
           ELSE  isnull(CUEX.UBM_SITE_NAME  , inv_lbl.site_name)                
      END [SITE]                  
    ,isnull( C.country_name, INV_LBL.COUNTRY) as country,                  
 isnull(S.state_name, INV_LBL.STATE_NAME) as state,                  
  CASE WHEN  CI.ACCOUNT_GROUP_ID IS NULL THEN isnull(CHA.Account_Vendor_Name  ,INV_LBL.VENDOR_NAME )                
     WHEN CI.ACCOUNT_GROUP_ID IS NOT NULL THEN isnull(V.VENDOR_NAME  ,INV_LBL.VENDOR_NAME )                 
     ELSE INV_LBL.VENDOR_NAME                  
     end  AS VENDOR                   
 ,  CASE WHEN  CI.ACCOUNT_GROUP_ID IS NULL THEN isnull(AVT.ENTITY_NAME ,inv_lbl.vendor_type )                 
   WHEN CI.ACCOUNT_GROUP_ID IS NOT NULL THEN isnull(VT.ENTITY_NAME  ,inv_lbl.vendor_type )                 
     ELSE inv_lbl.vendor_type                  
     end AS VENDOR_TYPE                   
    , NULL,                
 isnull(cia.is_priority ,0) ,
 case when cia.is_priority is null or cia.is_priority=0
 then 9999999
 else cia.source_cd
 end                  
FROM  DBO.CU_EXCEPTION_DENORM CUEX                  
JOIN dbo.QUEUE q ON q.QUEUE_ID = CUEX.QUEUE_ID                  
                 INNER JOIN dbo.Entity qt ON qt.ENTITY_ID = q.QUEUE_TYPE_ID                  
      JOIN                  
      DBO.CU_INVOICE CI                  
            ON CI.CU_INVOICE_ID = CUEX.CU_INVOICE_ID 
			LEFT JOIN CLIENT CL 
			on CL.CLIENT_ID = CI.CLIENT_ID                   
   JOIN WORKFLOW.WORKFLOW_QUEUE_SEARCH_FILTER_GROUP_INVOICE_MAP WQI                  
   ON WQI.CU_INVOICE_ID = CI.CU_INVOICE_ID                  
   JOIN WORKFLOW.WORKFLOW_QUEUE_SEARCH_FILTER_GROUP WQG                  
   ON WQG.Workflow_Queue_Search_Filter_Group_Id = wqi.Workflow_Queue_Search_Filter_Group_Id                 
 LEFT JOIN WORKFLOW.CU_INVOICE_ATTRIBUTE CIA                   
 ON  CIA.CU_INVOICE_ID = cuex.CU_INVOICE_ID                  
      LEFT JOIN                  
      DBO.CU_INVOICE_SERVICE_MONTH SM                  
            ON SM.CU_INVOICE_ID = CUEX.CU_INVOICE_ID                  
               AND SM.ACCOUNT_ID = CUEX.ACCOUNT_ID                  
      LEFT OUTER JOIN                  
      DBO.USER_INFO UI                  
            ON UI.QUEUE_ID = CUEX.QUEUE_ID                  
      LEFT JOIN                  
      DBO.CU_EXCEPTION_TYPE CET                  
            ON CUEX.EXCEPTION_TYPE = CET.EXCEPTION_TYPE                  
      LEFT JOIN                  
      DBO.ACCOUNT_GROUP AG                  
   ON AG.ACCOUNT_GROUP_ID = CI.ACCOUNT_GROUP_ID                  
      LEFT JOIN                  
      (DBO.VENDOR V                  
       INNER JOIN                  
       DBO.ENTITY VT                  
             ON VT.ENTITY_ID = V.VENDOR_TYPE_ID)                  
            ON V.VENDOR_ID = AG.VENDOR_ID                  
                  
      LEFT JOIN                  
      CORE.CLIENT_HIER_ACCOUNT CHA                  
            ON CHA.CLIENT_HIER_ID = CUEX.CLIENT_HIER_ID                  
               AND CHA.ACCOUNT_ID = CUEX.ACCOUNT_ID                   
       LEFT JOIN                  
      (DBO.VENDOR AV                  
       INNER JOIN                  
       DBO.ENTITY AVT                  
             ON AVT.ENTITY_ID = AV.VENDOR_TYPE_ID)                  
            ON AV.VENDOR_ID = CHA.Account_Vendor_Id                  
      LEFT JOIN                  
      CORE.CLIENT_HIER CH                  
            ON CH.CLIENT_HIER_ID = CUEX.CLIENT_HIER_ID                   
 LEFT JOIN COUNTRY c                  
            ON CH.COUNTRY_ID = C.COUNTRY_ID                   
   LEFT JOIN state s                  
            ON CH.state_id = s.state_id                     
  LEFT JOIN                  
      DBO.COMMODITY COM                  
            ON COM.COMMODITY_ID = CHA.COMMODITY_ID                  
      LEFT JOIN                  
      DBO.CU_INVOICE_LABEL INV_LBL                  
            ON CUEX.CU_INVOICE_ID = INV_LBL.CU_INVOICE_ID                  
      LEFT JOIN                  
      DBO.UBM UBM                  
ON UBM.UBM_ID = CI.UBM_ID                
  LEFT JOIN dbo.Entity status_type ON status_type.ENTITY_name = cuex.EXCEPTION_STATUS_TYPE                 
  AND STATUS_TYPE.ENTITY_description =' + '''' + 'UBM Exception Status' + ''''                
              + ' OUTER APPLY                  
      (     SELECT                  
                        UAC.ACCOUNT_VENDOR_NAME + ' + '''' + ', ' + ''''                
              + 'FROM        CORE.CLIENT_HIER_ACCOUNT UAC                  
                        INNER JOIN                  
      CORE.CLIENT_HIER_ACCOUNT SAC                  
                              ON SAC.METER_ID = UAC.METER_ID                  
            WHERE      UAC.ACCOUNT_TYPE = ' + '''' + 'UTILITY' + '''' + 'AND   SAC.ACCOUNT_TYPE = ' + ''''                
              + 'SUPPLIER' + '''' + 'AND   CHA.ACCOUNT_TYPE = ' + '''' + 'SUPPLIER' + ''''                
              + 'AND   SAC.ACCOUNT_ID = CHA.ACCOUNT_ID                  
            GROUP BY    UAC.ACCOUNT_VENDOR_NAME                  
            FOR XML PATH(' + '''' + ''''                
              + ')) UV(VENDOR_NAME)                  
   WHERE    wqi.Workflow_Queue_Search_Filter_Group_Id = ' + CAST(@Workflow_Queue_Search_Filter_Group_id AS VARCHAR)                
              + ' AND   CUEX.EXCEPTION_TYPE <> ' + '''' + 'FAILED RECALC' + ''''                
             
			 
			 set @sql2= CASE                
                    WHEN @Account_Number IS NOT NULL THEN                
                        ' and ( CASE WHEN CUEX.EXCEPTION_TYPE = ' + '''' + 'WRONG ACCOUNT' + ''''                
                        + ' THEN NULL                  
             WHEN AG.ACCOUNT_GROUP_ID IS NULL                  
                   THEN COALESCE(CHA.Account_Number, CUEX.UBM_Account_Number,INV_LBL.ACCOUNT_NUMBER)                  
             ELSE  COALESCE(AG.GROUP_BILLING_NUMBER, CHA.ACCOUNT_NUMBER, CUEX.UBM_ACCOUNT_NUMBER,INV_LBL.account_number)                      
        END )   like'                
                        + '''%' + @Account_Number + '%'''                
                    ELSE                
                        ''                
                END + CASE                
                          WHEN @Queue_id IS NOT NULL                
        AND @Queue_id = '-1' THEN                
                              'AND qt.[Entity_Name] = ' + '''' + 'Private' + ''''            
         WHEN @Queue_id IS NOT NULL                  
                               AND @Queue_id = '-1,0' THEN                  
                              ''                
                          WHEN @Queue_id IS NOT NULL                
                               AND @Queue_id = '0' THEN                
                              'AND qt.[Entity_Name] = ' + '''' + 'Public' + ''''                
                          WHEN @Queue_id IS NOT NULL                
                               AND @Queue_id != '-1'                
                               AND @Queue_id != '0' THEN                
                              'and cuex.queue_id in (select user_info_id from #temp_user_info)'                
                          ELSE                
                              ''                
                      END                
              + CASE        
                    WHEN @Client IS NOT NULL THEN                
                        'and CASE WHEN CUEX.CLIENT_HIER_ID IS NOT NULL                  
                 THEN CH.CLIENT_NAME                  
           ELSE   isnull(CUEX.UBM_CLIENT_NAME , INV_LBL.CLIENT_NAME)                  
      END like ' + '''%' + @Client + '%'''                
                    ELSE                
                        ''                
                END + CASE                
                          WHEN @Commodity IS NOT NULL THEN                
                              'and ISNULL(COM.COMMODITY_NAME , INV_LBL.COMMODITY) like  ' + '''%' + @Commodity + '%'''                
                          ELSE                
                              ''                
                      END + CASE                
					    WHEN @Data_Source IS NOT NULL AND @Data_Source = 0 THEN   
								'and CI.UBM_Invoice_id is null'
                                WHEN @Data_Source IS NOT NULL AND @data_source <> 0 THEN                
            'and ubm.UBM_ID=' + CAST(@Data_Source AS VARCHAR)                
                                ELSE                
                                    ''                
                            END                
              + CASE                
                    WHEN @Start_Date_in_CBMS IS NOT NULL THEN                
                        ' and cuex.Date_In_CBMS between ''' + CAST(@Start_Date_in_CBMS AS VARCHAR) + '''' + ' and '''                
                        + CAST(@End_Date_in_CBMS AS VARCHAR) + ''''                
                    ELSE                
                        ''                
                END                
              + CASE                
                    WHEN @Start_Date_in_Queue IS NOT NULL THEN                
                        'and cuex.DATE_IN_QUEUE between ''' + CAST(@Start_Date_in_Queue AS VARCHAR) + '''' + ' and '''                
                        + CAST(@End_Date_in_Queue AS VARCHAR) + ''''                
                    ELSE                
                        ''                
                END + CASE                
                          WHEN @Filename IS NOT NULL THEN                
                              'and CUEX.CBMS_DOC_ID like ' + '''%' + @Filename + '%'''               
                          ELSE                
                              ''                
                      END + CASE                
                                WHEN @Invoice_ID IS NOT NULL THEN                
                                    'and cuex.CU_INVOICE_ID=' + @Invoice_ID                
                                ELSE                
                                    ''                
                            END + CASE                
                                      WHEN @Exception_Status IS NOT NULL THEN                
                                          'and STATUS_TYPE.ENTITY_ID=' + CAST(@Exception_Status AS VARCHAR)                
                                      ELSE                
                                          ''                
            END                
              + CASE                
                    WHEN @Month IS NOT NULL THEN                
                        'and CASE WHEN CUEX.EXCEPTION_TYPE = ' + '''' + 'WRONG MONTH' + ''''                
                        + 'THEN NULL                  
             WHEN AG.ACCOUNT_GROUP_ID IS NULL and SM.SERVICE_MONTH is not null THEN CONVERT(VARCHAR(10),  SM.SERVICE_MONTH , 101)                  
             when CUEX.SERVICE_MONTH is not null then CUEX.SERVICE_MONTH              
     else null                
    END'                
                        + '=' + '''' + CONVERT(VARCHAR(10), @Month, 101) + ''''                
                    ELSE                
                        ''                
                END                
              + CASE                
                    WHEN @Site IS NOT NULL THEN                
                        'and CASE WHEN CUEX.CLIENT_HIER_ID IS NOT NULL                  
                 THEN isnull(CH.SITE_NAME , inv_lbl.site_name)                 
           ELSE  isnull(CUEX.UBM_SITE_NAME  , inv_lbl.site_name)                
      END   like' + '''%' + @Site + '%'''                
                    ELSE                
                        ''                
                END + CASE                
                          WHEN @Exception_Type IS NOT NULL THEN                
                              'and cet.cu_exception_type_id in (select exception_type from #temp_exception_type)'                
                          ELSE                
                              ''                
                      END + CASE                
                                WHEN @City IS NOT NULL THEN                
                                    'and ch.city=' + '''' + @City + ''''                
                                ELSE                
                                    ''                
                            END + CASE                
                                      WHEN @Priority IS NOT NULL                
                                           AND @Priority = 1 THEN                
                                       'and    CIA.Is_Priority =1'                
                                      WHEN @Priority = 0 THEN          
            'and (CIA.Is_Priority is null or CIA.Is_Priority =0)'              
                                      --    'AND ( CIA.CU_INVOICE_ID IS NULL OR CIA.Is_Priority =0)'                
                                      ELSE                
                                          ''                
                                  END                
              + CASE                
                    WHEN @Vendor IS NOT NULL THEN                
                        'and   CASE WHEN  CI.ACCOUNT_GROUP_ID IS NULL THEN isnull(CHA.Account_Vendor_Name  ,INV_LBL.VENDOR_NAME )                
     WHEN CI.ACCOUNT_GROUP_ID IS NOT NULL THEN isnull(V.VENDOR_NAME  ,INV_LBL.VENDOR_NAME )                 
     ELSE INV_LBL.VENDOR_NAME                  
     end like' + '''%' + @Vendor + '%'''                
                    ELSE                
                        ''                
                END                
              + CASE                
                    WHEN @Vendor_Type IS NOT NULL THEN                
                        'and CASE WHEN  CI.ACCOUNT_GROUP_ID IS NULL THEN isnull(AVT.ENTITY_NAME ,inv_lbl.vendor_type )                 
   WHEN CI.ACCOUNT_GROUP_ID IS NOT NULL THEN isnull(VT.ENTITY_NAME  ,inv_lbl.vendor_type )                 
     ELSE inv_lbl.vendor_type                  
     end like' + '''%' + @Vendor_Type + '%'''                
                    ELSE                
                        ''                
                END + CASE                
             WHEN @Country IS NOT NULL THEN                
                              'and isnull( C.country_name, INV_LBL.COUNTRY) like ' + '''%' + @Country + '%'''                
                          ELSE                
                              ''                                      END + CASE                
                                WHEN @State IS NOT NULL THEN                
   'and isnull(S.state_name, INV_LBL.STATE_NAME) like ' + '''%' + @State + '%'''                
                                ELSE                
                                    ''                
                            END;                
                
       set @sql = @sql1 + @sql2  
	   
	  -- SELECT @sql             
        EXECUTE sp_executesql @sql;                
                
        UPDATE DF                
        SET DF.COMMODITY = CASE                
                               WHEN (                
                                    (                
                                        SELECT COUNT(DISTINCT COMMODITY)                
                       FROM #DEFAULTEXCEPTIONS DF1                
                                        WHERE COMMODITY IS NOT NULL                
                                              AND DF.CU_INVOICE_ID = DF1.CU_INVOICE_ID                
                                    ) > 1                
                                    ) THEN                                                 'MULTIPLE'                
                               ELSE                
                                   DF.COMMODITY                
                           END,                
            DF.SITE = CASE                
                 WHEN (                
                               (                
                                   SELECT COUNT(DISTINCT [SITE])                
                                   FROM #DEFAULTEXCEPTIONS DF1                
                                   WHERE SITE IS NOT NULL                
                                         AND DF.CU_INVOICE_ID = DF1.CU_INVOICE_ID                
                               ) > 1                
                               ) THEN                
                              'MULTIPLE'                
                          ELSE                
                              DF.SITE                
                      END,                
            DF.ACCOUNT = CASE                
                             WHEN (                
                                  (                
                                      SELECT COUNT(DISTINCT ACCOUNT)                
                                      FROM #DEFAULTEXCEPTIONS DF1                
                                      WHERE ACCOUNT IS NOT NULL                
                                            AND DF.CU_INVOICE_ID = DF1.CU_INVOICE_ID                
                                  ) > 1                
                                  ) THEN                
                                 'MULTIPLE'                
                             ELSE                
                                 DF.ACCOUNT                
                         END ,
						 
						  DF.ACCOUNT_ID = CASE                    
                             WHEN (                    
                  (                    
                                      SELECT COUNT(DISTINCT ACCOUNT_ID)                    
           FROM #DEFAULTEXCEPTIONS DF1                    
                                      WHERE ACCOUNT_ID IS NOT NULL                    
                                            AND DF.CU_INVOICE_ID = DF1.CU_INVOICE_ID                    
                                  ) > 1                    
                                  ) THEN                    
                                 'MULTIPLE'                    
                             ELSE                    
                                 DF.ACCOUNT_ID                    
                         END                  
        FROM #DEFAULTEXCEPTIONS DF;                
                
        DELETE FROM #DEFAULTEXCEPTIONS                
        WHERE RN <> 1;                
                
        IF (@COMMENTS IS NOT NULL)                
        BEGIN                
                
            UPDATE DFG                
            SET DFG.COMMENTS =                
                (                
                    SELECT TOP (1)                
                           IMAGE_COMMENT                
                    FROM CU_INVOICE_COMMENT CUI                
                WHERE CUI.CU_INVOICE_ID = DFG.CU_INVOICE_ID                
                          AND CUI.IMAGE_COMMENT = @COMMENTS                
                    ORDER BY CUI.CU_INVOICE_COMMENT_ID DESC                
                )                
            FROM #DEFAULTEXCEPTIONS DFG;                
                
            DELETE FROM #DEFAULTEXCEPTIONS                
            WHERE COMMENTS IS NULL;                
                
        END;                
                
        ELSE            
        BEGIN                
                
            UPDATE DFG                
            SET DFG.COMMENTS =                
                (                
                    SELECT TOP (1)                
                           IMAGE_COMMENT                
                    FROM CU_INVOICE_COMMENT CUI                
                    WHERE CUI.CU_INVOICE_ID = DFG.CU_INVOICE_ID                
                    ORDER BY CUI.CU_INVOICE_COMMENT_ID DESC                
                )                
            FROM #DEFAULTEXCEPTIONS DFG;                
                
        END;                
                
        SET @TOTAL_COUNT =                
        (                
            SELECT COUNT(1) FROM #DEFAULTEXCEPTIONS                
        );                
        --IF ONLY COUNT IS NEEDED THEN RETURN THE SP, NO NEED TO REST OF THE OPERATION                
                
        IF (@ONLY_COUNT_NEEDED = 1)                
            RETURN 0;                
                
                
                
        SET @SQL3                
            = 'INSERT INTO #DEFAULTEXCEPTIONSGROUPDETAILS                
        (                
            index_range,                
   UserGroupId,                
            UserGroupName,                
      usergroupcount,                
            systemgroupid,                
            SystemGroupCount,                
            ubmid,                
            ubmaccountcode,                 
            CU_INVOICE_ID,                
            ACCOUNT,  
			ACCOUNT_ID,              
            EXCEPTION_TYPE,                
            EXCEPTION_STATUS_TYPE,                
            ASSIGNEE,                
            CLIENTID,                
            CLIENT,                 
            COMMODITY,                
            [DATA SOURCE],                
            DATE_IN_QUEUE,                
            DATE_IN_CBMS,                
            FILEID,                
            FILENAME,                
            SERVICE_MONTH,                
            SITE,                
            COUNTRY,                
            STATE,                
            VENDOR,                
            VENDOR_TYPE,                
            COMMENTS,                
   is_priority  ,
   [priority_cd]              
        )                
        SELECT   ROW_NUMBER() OVER (ORDER BY UserGroupId DESC, is_priority DESC , [priority_cd] asc'                
 + CASE                
                    WHEN @Data_Source_Column_Name IS NOT NULL THEN                
                        +',' + @Data_Source_Column_Name + ' '                
                    ELSE                
                        ''                
                END + CASE               
                          WHEN @Data_Source_Column_Name IS NOT NULL                
                               AND @Sorting_Type IS NOT NULL THEN                
                              @Sorting_Type                
                          ELSE                
                              ''                
                      END + ') AS index_range,                
  UserGroupId,                
               UserGroupName,' + CAST(@TOTAL_COUNT AS VARCHAR)                
              + ',                
               systemgroupid,                
               SystemGroupCount,                
               ubmid,                
               ubmaccountcode,                 
               CU_INVOICE_ID,                
               ACCOUNT, 
			   Account_ID,               
               EXCEPTION_TYPE,                
               EXCEPTION_STATUS_TYPE,                
               ASSIGNEE,                
               CLIENTID,                
               CLIENT,                 
               COMMODITY,                
               [DATA SOURCE],                
               DATE_IN_QUEUE,                
               DATE_IN_CBMS,                
               FILEID,                
               FILENAME,                
   SERVICE_MONTH,                
               SITE,                
               COUNTRY,                
    STATE,                
               VENDOR,                
               VENDOR_TYPE,                
               COMMENTS,                
        is_priority  ,
		[priority_cd]              
        FROM #DEFAULTEXCEPTIONS';                
                
        EXECUTE sp_executesql @SQL3;                
                
        SELECT DF.CU_INVOICE_ID    AS [Inv Id],                
               DF.[DATE_IN_QUEUE]   AS [Date in Queue],                
               DF.[DATE_IN_CBMS]   AS [Date in CBMS],                
               DF.ASSIGNEE     AS Assignee,                
               DF.EXCEPTION_TYPE   AS [Exception Type],                
               DF.EXCEPTION_STATUS_TYPE     AS [Exception Status],                
               DF.[DATA SOURCE]             AS [Data Source],                
               DF.CLIENT     AS [Client],                
               DF.[SITE]     AS [Site],                
               DF.COUNTRY     AS Country,                
               DF.[STATE]     AS [State],                
               DF.ACCOUNT     AS [Account no],                
               DF.Account_Id    AS Account_Id,                
               DF.VENDOR     AS Vendor,                
               DF.VENDOR_TYPE    AS [Vendor Type],                
               DF.COMMODITY        AS Commodity,                
               DF.SERVICE_MONTH    AS [Month],                
               DF.[FILENAME]    AS Filename,                
               DF.COMMENTS     AS Comments,                
               DF.UserGroupId    AS UserGroupId,                
               DF.UserGroupName    AS UserGroupName,                
               DF.SystemGroupCount   AS SystemGroupCount,                
               DF.CLIENTID     AS ClientID,                
               DF.usergroupcount   AS usergroupcount,                
               DF.systemgroupid    AS systemgroupid,                
               DF.FILEID     AS FileID,                
               DF.ubmid      AS UBMID,                
               DF.ubmaccountcode   AS UBMAccountNumber ,                
               isnull(DF.is_priority, 0) AS [Priority],        
       CASE WHEN EXISTS (SELECT 1 FROM workflow.Cu_Invoice_Workflow_Action_Batch_Dtl CIWABD           
  INNER JOIN DBO.Code C ON CIWABD.Status_Cd=C.Code_Id         
  AND CIWABD.Cu_Invoice_Id =DF.Cu_Invoice_Id        
  WHERE C.Code_Value='Pending' ) THEN  'Pending'  ELSE  NULL END AS [Batch Status],        
  CASE WHEN EXISTS(SELECT 1 FROM workflow.Cu_Invoice_Attribute CU        
  JOIN code c         
  ON cu.Source_Cd = c.Code_Id        
  AND cu.CU_INVOICE_ID = df.CU_INVOICE_ID         
  AND c.Code_Value = 'Systempriorityclient') THEN 'System'        
  WHEN EXISTS (SELECT 1 FROM workflow.Cu_Invoice_Attribute CU        
  JOIN code c         
  ON cu.Source_Cd = c.Code_Id        
  AND cu.CU_INVOICE_ID = df.CU_INVOICE_ID         
  AND c.Code_Value = 'Userprioritymanual') THEN 'User'        
  END AS Priority_Source        
        FROM #DEFAULTEXCEPTIONSGROUPDETAILS DF           
        WHERE DF.index_range                
        BETWEEN @startindex AND @endindex
		ORDER BY DF.index_range;                
                
                
        DROP TABLE #DEFAULTEXCEPTIONSGROUPDETAILS;                
        DROP TABLE #DEFAULTEXCEPTIONS;                
    --DELETE THE ENTRIES WHICH WAS DEFAULT EARLIER AND NOW USER DID NOT WANT TOR                  
                
   END TRY                
    BEGIN CATCH                
                
                
                
        -- Entry made to the logging SP to capture the errors.                  
        SELECT @Error_Line = error_line(),                
               @Error_Message = error_message();                
                
        INSERT INTO StoredProc_Error_Log                
        (                
            StoredProc_Name,                
            Error_Line,                
            Error_message,                
            Input_Params                
        )                
        VALUES                
        (@Proc_name, @Error_Line, @Error_Message, @Input_Params);                
        RETURN -99;                
    END CATCH;                
END;                                                                                                                                                                                                                          
GO
GRANT EXECUTE ON  [Workflow].[Get_Invoice_Exception_Details_Grouping_User_Filter] TO [CBMSApplication]
GO
