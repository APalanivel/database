
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
            

/******
            
NAME: [DBO].[Cu_Invoice_Get_Determinant_Category_Buckets_By_Priority_Invoice_Id]
                 
DESCRIPTION:            
    To Get determinant category buckets by invoice and priority            
                  
INPUT PARAMETERS:                      
NAME					   DATATYPE DEFAULT  DESCRIPTION                      
------------------------------------------------------------                      
@Cu_Invoice_Id			   INT            
@Category_Bucket_Master_Id  INT            
@Priority_Order		   INT            
            
OUTPUT PARAMETERS:            
NAME   DATATYPE DEFAULT  DESCRIPTION            
------------------------------------------------------------                      

USAGE EXAMPLES:                      
------------------------------------------------------------              
	EXEC Cu_Invoice_Get_Determinant_Category_Buckets_By_Priority_Invoice_Id  7530068, 101015,1

AUTHOR INITIALS:                      
INITIALS	  NAME                      
------------------------------------------------------------                      
PKY		  Pavan K Yadalam  
RKV       Ravi Kumar Vegesna          
            
MODIFICATIONS            
INITIALS	DATE		MODIFICATION            
------------------------------------------------------------                      
PKY		25-JUL-11	Created for additional data requirement
RKV     2016-10-04  Added two Parameters Country_Id and State_Id,also added new join 
					Country_State_Category_Bucket_Aggregation_Rule as part of PF Enhancement.

*/          

CREATE PROCEDURE [dbo].[Cu_Invoice_Get_Determinant_Category_Buckets_By_Priority_Invoice_Id]
      ( 
       @Cu_Invoice_Id INT
      ,@Category_Bucket_Master_Id INT
      ,@Priority_Order INT 
       ,@Country_Id INT = NULL
      ,@State_Id INT = NULL )
AS 
BEGIN

      SET NOCOUNT ON

      SELECT
            r.Category_Bucket_Master_Id
           ,CASE WHEN cscbar.Category_Bucket_Master_Id IS NULL THEN cd.Code_Value
                 ELSE 'CountryStateAggregationRule'
            END AS Aggregation_Type_Cd
           ,ISNULL(cscbar.Is_Aggregate_Category_Bucket, r.Is_Aggregate_Category_Bucket) Is_Aggregate_Category_Bucket
           ,r.Child_Bucket_Master_Id
           ,d.CU_Invoice_Determinant_Id
           ,d.Commodity_Type_Id
           ,d.Unit_of_Measure_Type_Id
           ,d.Determinant_Name
           ,d.Determinant_Value
           ,d.CU_Determinant_No
           ,d.CU_Determinant_Code
           ,d.Bucket_Master_Id
           ,e.Entity_Name
      FROM
            dbo.Bucket_Category_Rule r
            INNER JOIN dbo.Code cd
                  ON r.Aggregation_Type_CD = cd.Code_Id
            LEFT OUTER JOIN ( dbo.CU_Invoice_Determinant d
                              INNER JOIN dbo.Entity e
                                    ON e.Entity_ID = d.Unit_of_Measure_Type_Id )
                              ON r.Child_Bucket_Master_Id = d.Bucket_Master_Id
                                 AND d.CU_Invoice_Id = @Cu_Invoice_Id
            INNER JOIN dbo.Code cdlvl
                  ON r.CU_Aggregation_Level_Cd = cdlvl.Code_Id
            LEFT OUTER JOIN ( dbo.Country_State_Category_Bucket_Aggregation_Rule cscbar
                              INNER JOIN code csalvcd
                                    ON csalvcd.Code_Id = cscbar.CU_Aggregation_Level_Cd
                                       AND csalvcd.Code_Value = 'Invoice' )
                              ON r.Category_Bucket_Master_Id = cscbar.Category_Bucket_Master_Id
                                 AND ( @Country_Id IS NULL
                                       OR cscbar.Country_Id = @Country_Id )
                                 AND ( @State_Id IS NULL
                                       OR cscbar.State_Id = @Country_Id )
                                 AND cscbar.Priority_Order = @Priority_Order
      WHERE
            r.Category_Bucket_Master_Id = @Category_Bucket_Master_Id
            AND @Priority_Order = ISNULL(cscbar.Priority_Order, r.Priority_Order)
            AND cdlvl.Code_Value = 'Invoice'

END;
;
GO

GRANT EXECUTE ON  [dbo].[Cu_Invoice_Get_Determinant_Category_Buckets_By_Priority_Invoice_Id] TO [CBMSApplication]
GO
