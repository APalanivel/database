SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE dbo.GET_CEM_USER_EMAIL_ID_P
@userId varchar(10),
@sessionId varchar(20),
@clientId int
AS
SELECT 	
	EMAIL_ADDRESS 
FROM 
	USER_INFO 
WHERE 
	USER_INFO_ID IN (select user_info_id from client_cem_map where client_id=@clientId)
GO
GRANT EXECUTE ON  [dbo].[GET_CEM_USER_EMAIL_ID_P] TO [CBMSApplication]
GO
