SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [dbo].[Time_Of_Use_Schedule_Term_Peak_Sel_By_RateId]

DESCRIPTION:

	To get the time of use schedule peak details
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@RateId			INT
@Start_Dt		DATE
@End_Dt			DATE


OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------  

	EXEC dbo.Time_Of_Use_Schedule_Term_Peak_Sel_By_RateId 35797,'01-24-2005','12-31-2005'
     
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
TP			Anoop

MODIFICATIONS
INITIALS	DATE		MODIFICATION
-----------------------------------------------------------
TP			2012-07-23	Created 

*/
CREATE PROCEDURE [dbo].[Time_Of_Use_Schedule_Term_Peak_Sel_By_RateId]
      ( 
       @RateId INT
      ,@Start_Dt DATETIME
      ,@End_Dt DATETIME )
AS 
BEGIN

      SET NOCOUNT ON ;
			--To get the dates
		
      SELECT
            RS.RATE_SCHEDULE_ID
           ,case WHEN RS.RS_START_DATE < @Start_Dt THEN @Start_Dt
                 ELSE RS.RS_START_DATE
            END AS Start_Dt
           ,case WHEN isnull(RS.RS_END_DATE, @End_Dt) > @End_Dt THEN @End_Dt
                 ELSE isnull(RS.RS_END_DATE, @End_Dt)
            END AS End_Dt
      FROM
            dbo.RATE_SCHEDULE RS
      WHERE
            RS.RATE_ID = @RateId
            AND ( ( @Start_Dt <= RS.RS_END_DATE
                    AND @End_Dt > = Rs.RS_START_DATE )
                  OR ( RS.RS_START_DATE BETWEEN @Start_Dt AND @End_Dt )
                  OR ( RS.RS_START_DATE < @Start_Dt
                       AND RS.RS_END_DATE IS NULL ) )
      ORDER BY
            RS.RS_START_DATE  


			--Time Use Schedule details	
      SELECT
            RS.RATE_SCHEDULE_ID
           ,STPD.Time_Of_Use_Schedule_Term_Peak_Id
           ,STPD.Start_Time
           ,STPD.End_Time
           ,STPD.Is_Holiday_Active
           ,cd.Code_Value Day_Of_Week
           ,isnull(S.SEASON_ID, -1) AS SEASON_ID
           ,month(S.SEASON_FROM_DATE) AS Season_Start_Month
           ,day(S.SEASON_FROM_DATE) AS Season_Start_Day
           ,month(S.SEASON_TO_DATE) AS Season_End_Month
           ,day(S.SEASON_TO_DATE) AS Season_End_Day
           ,isnull(RS.Time_Of_Use_Schedule_Term_Id, -1) AS Term_Id
           ,TOUST.Start_Dt AS Term_Start_Dt
           ,TOUST.End_Dt AS Term_End_Dt
      FROM
            dbo.RATE_SCHEDULE RS
            LEFT JOIN dbo.Time_Of_Use_Schedule_Term TOUST
                  ON TOUST.Time_Of_Use_Schedule_Term_Id = RS.Time_Of_Use_Schedule_Term_Id
            LEFT JOIN dbo.Time_Of_Use_Schedule_Term_Peak STP
                  ON RS.Time_Of_Use_Schedule_Term_Id = STP.Time_Of_Use_Schedule_Term_Id
            LEFT JOIN dbo.Time_of_Use_Schedule_Term_Peak_Dtl STPD
                  ON STP.Time_Of_Use_Schedule_Term_Peak_Id = STPD.Time_Of_Use_Schedule_Term_Peak_Id
            LEFT JOIN Code cd
                  ON cd.Code_Id = STPD.Day_Of_Week_Cd
            LEFT JOIN dbo.SEASON S
                  ON S.SEASON_ID = STP.SEASON_ID
      WHERE
            RS.RATE_ID = @RateId
            AND ( ( @Start_Dt <= RS.RS_END_DATE
                    AND @End_Dt > = Rs.RS_START_DATE )
                  OR ( RS.RS_START_DATE BETWEEN @Start_Dt AND @End_Dt )
                  OR ( RS.RS_START_DATE < @Start_Dt
                       AND RS.RS_END_DATE IS NULL ) )
      ORDER BY
            STPD.Start_Time						  
									  
			--Holiday details
      SELECT
            TOUH.Holiday_Dt
           ,RS.RATE_SCHEDULE_ID
      FROM
            dbo.Time_Of_Use_Schedule_Term_Holiday TOUH
            INNER JOIN dbo.RATE_SCHEDULE RS
                  ON RS.Time_Of_Use_Schedule_Term_Id = TOUH.Time_Of_Use_Schedule_Term_Id
      WHERE
            RS.RATE_ID = @RateId
            AND ( ( @Start_Dt <= RS.RS_END_DATE
                    AND @End_Dt > = Rs.RS_START_DATE )
                  OR ( RS.RS_START_DATE BETWEEN @Start_Dt AND @End_Dt )
                  OR ( RS.RS_START_DATE < @Start_Dt
                       AND RS.RS_END_DATE IS NULL ) ) 
                 

END



;
GO
GRANT EXECUTE ON  [dbo].[Time_Of_Use_Schedule_Term_Peak_Sel_By_RateId] TO [CBMSApplication]
GO
