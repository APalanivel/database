SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Address_Del]  

DESCRIPTION: It Deletes Address for Selected Address Id.     
      
INPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Address_Id		INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
	EXEC Address_Del 196302
  Rollback Tran

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			28-JUN-10	CREATED

*/

CREATE PROCEDURE dbo.Address_Del
    (
      @Address_Id INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.ADDRESS
	WHERE
		Address_Id = @Address_Id

END
GO
GRANT EXECUTE ON  [dbo].[Address_Del] TO [CBMSApplication]
GO
