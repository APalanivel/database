
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******      
  
  
NAME:      
dbo.UOMConversion_Sel_By_Commodity_ID     
  
DESCRIPTION:      
Used to get UOM Conversion from COMMODITY_UOM_CONVERSION table    
  
INPUT PARAMETERS:      
	Name			DataType	Default		Description      
------------------------------------------------------------      
	@Commodity_Id	INT
	@Base_UOM_Cd	INT
  
            
OUTPUT PARAMETERS:      
	Name			DataType	Default		Description      
------------------------------------------------------------      


USAGE EXAMPLES:      
------------------------------------------------------------    
 
EXEC dbo.UOMConversion_Sel_By_Commodity_ID 290,100082
EXEC dbo.UOMConversion_Sel_By_Commodity_ID 290,100082,290,100098
EXEC dbo.UOMConversion_Sel_By_Commodity_ID 291,100046,290,100098
EXEC dbo.UOMConversion_Sel_By_Commodity_ID 58,100098
EXEC dbo.UOMConversion_Sel_By_Commodity_ID 67,100069
  

AUTHOR INITIALS:      
	Initials	Name      
------------------------------------------------------------      
	GB			Geetansu Behera      
	CMH			Chad Hattabaugh       
	HG			Hari    
	SKA			Shobhit Kr Agrawal    
	RR			Raghu Reddy
  
MODIFICATIONS       
	Initials	Date		Modification      
------------------------------------------------------------      
	GB			28/04/09	created    
	SKA			01/07/09	Modified as per the coding standards    
	SS			14/07/09	Eliminated Subselect  
	SKA			02/09/09	Removed the subselect and used the ANSI joins.  
	GB			16/09/09	Removed @Commodity_Name, @Base_UOM_Value from input parameter  
	GB			17/09/09	GET @Base_Uom_value from select query   
	SKA			09-OCT-09	Added one condition in where clause (cm.IS_SUSTAINABLE = 1) As per bug #11833  
	DMR			09/10/2010	Modified for Quoted_Identifier  
	RR			2012-09-03	Maint-1492 added two new input parameters @CONVERTED_COMMODITY_ID, @CONVERTED_UOM_CD and both are optional
							Added @COMMODITY_NAME to get base commodity name, previously same converted commodity name is also returning as base
							commodity name

******/  
CREATE PROCEDURE [dbo].[UOMConversion_Sel_By_Commodity_ID]
      @Commodity_Id AS INT
     ,@Base_UOM_Cd AS INT
     ,@CONVERTED_COMMODITY_ID AS INT = NULL
     ,@CONVERTED_UOM_CD AS INT = NULL
AS 
BEGIN    
    
      SET NOCOUNT ON     
  
      DECLARE
            @Base_UOM_Value AS VARCHAR(25)
           ,@COMMODITY_NAME AS VARCHAR(50)
      
      SELECT
            @Base_UOM_Value = CODE_VALUE
      FROM
            CODE c
      WHERE
            c.CODE_ID = @Base_UOM_Cd 
            
      SELECT
            @COMMODITY_NAME = Commodity_Name
      FROM
            dbo.Commodity c
      WHERE
            c.Commodity_Id = @Commodity_Id              
  
      SELECT
            @Commodity_Id AS 'BASE_COMMODITY_ID'
           ,@COMMODITY_NAME AS 'BASE_COMMODITY_NAME'
           ,@Base_UOM_Cd AS 'BASE_UOM_CD'
           ,@Base_UOM_Value AS 'BASE_UOM_VALUE'
           ,cuc.BASE_COMMODITY_ID AS 'CONVERTED_COMMODITY_ID'
           ,cm.COMMODITY_NAME AS 'CONVERTED_COMMODITY_NAME'
           ,cuc.BASE_UOM_CD AS 'CONVERTED_UOM_CD'
           ,c.CODE_VALUE AS 'CONVERTED_UOM_VALUE'
           ,cuc1.CONVERSION_FACTOR
      FROM
            dbo.COMMODITY_UOM_CONVERSION cuc
            LEFT OUTER JOIN dbo.COMMODITY cm
                  ON cm.COMMODITY_ID = cuc.CONVERTED_COMMODITY_ID
            LEFT OUTER JOIN dbo.CODE c
                  ON cuc.CONVERTED_UOM_CD = c.CODE_ID
            LEFT OUTER JOIN dbo.COMMODITY_UOM_CONVERSION cuc1
                  ON cuc.BASE_COMMODITY_ID = cuc1.Converted_Commodity_Id
                     AND cuc.BASE_UOM_CD = cuc1.CONVERTED_UOM_CD
                     AND cuc1.BASE_COMMODITY_ID = @Commodity_Id
                     AND cuc1.Base_UOM_Cd = @Base_UOM_Cd
                     AND cuc1.Is_Active = 1
      WHERE
            cuc.BASE_COMMODITY_ID = cuc.CONVERTED_COMMODITY_ID
            AND cuc.BASE_UOM_CD = cuc.CONVERTED_UOM_CD
            AND cuc.IS_ACTIVE = 1
            AND cm.IS_ACTIVE = 1
            AND c.IS_ACTIVE = 1
            AND cm.IS_SUSTAINABLE = 1
            AND ( @CONVERTED_COMMODITY_ID IS NULL
                  OR cuc.Converted_Commodity_Id = @CONVERTED_COMMODITY_ID )
            AND ( @CONVERTED_UOM_CD IS NULL
                  OR cuc.Converted_UOM_Cd = @CONVERTED_UOM_CD )
END

;
GO

GRANT EXECUTE ON  [dbo].[UOMConversion_Sel_By_Commodity_ID] TO [CBMSApplication]
GO
