SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[cbmsInvoiceParticipation_MakeNotExpectedForSiteDate]
	( @MyAccountId int
	, @site_id int
	, @close_date datetime
	)
AS
BEGIN

   update invoice_participation
      set is_expected = 0
    where site_id = @site_id
      and is_received = 0
      and service_month >= @close_date

END




GO
GRANT EXECUTE ON  [dbo].[cbmsInvoiceParticipation_MakeNotExpectedForSiteDate] TO [CBMSApplication]
GO
