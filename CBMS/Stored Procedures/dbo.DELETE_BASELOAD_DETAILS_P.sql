SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE dbo.DELETE_BASELOAD_DETAILS_P
	@baseloadID INT
AS
BEGIN

	SET NOCOUNT ON

	DELETE FROM dbo.BASELOAD_DETAILS WHERE BASELOAD_ID = @baseloadID

END 


GO
GRANT EXECUTE ON  [dbo].[DELETE_BASELOAD_DETAILS_P] TO [CBMSApplication]
GO
