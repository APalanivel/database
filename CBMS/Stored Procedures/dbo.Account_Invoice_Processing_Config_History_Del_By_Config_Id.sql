SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: dbo.Account_Invoice_Processing_Config_History_Del_By_Config_Id
     
DESCRIPTION: 
	To Delete Account Processing Instruction configs  for selected config.
      
INPUT PARAMETERS:          
NAME										DATATYPE			DEFAULT			DESCRIPTION          
------------------------------------------------------------------------------------------------------          
@Account_Invoice_Processing_Config_Id		INT				
                
OUTPUT PARAMETERS:          
NAME										DATATYPE			DEFAULT			DESCRIPTION          
------------------------------------------------------------------------------------------------------  
        
USAGE EXAMPLES:          
------------------------------------------------------------------------------------------------------  
  
	BEGIN TRAN

	SELECT * FROM Account_Invoice_Processing_Config WHERE Account_Invoice_Processing_Config_Id =    12 
	SELECT * FROM Account_Invoice_Processing_Config_Log WHERE Account_Invoice_Processing_Config_Id =    12 

		EXEC Account_Invoice_Processing_Config_History_Del_By_Config_Id  12

	SELECT * FROM Account_Invoice_Processing_Config WHERE Account_Invoice_Processing_Config_Id =    12
	SELECT * FROM Account_Invoice_Processing_Config_Log WHERE Account_Invoice_Processing_Config_Id =    12 
 

	ROLLBACK TRAN
	
	
	
	 
	
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------------------------------------------------  
NR			Narayana Reddy
          
MODIFICATIONS           
INITIALS	DATE			MODIFICATION          
------------------------------------------------------------------------------------------------------  
NR			2019-03-01		WatchList - Created	

*/

CREATE PROCEDURE [dbo].[Account_Invoice_Processing_Config_History_Del_By_Config_Id]
    (
        @Account_Invoice_Processing_Config_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DELETE
        aipcl
        FROM
            dbo.Account_Invoice_Processing_Config_Log aipcl
        WHERE
            aipcl.Account_Invoice_Processing_Config_Id = @Account_Invoice_Processing_Config_Id;


        DELETE
        aipc
        FROM
            dbo.Account_Invoice_Processing_Config aipc
        WHERE
            aipc.Account_Invoice_Processing_Config_Id = @Account_Invoice_Processing_Config_Id;

    END;


GO
GRANT EXECUTE ON  [dbo].[Account_Invoice_Processing_Config_History_Del_By_Config_Id] TO [CBMSApplication]
GO
