SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: DBO.Rm_Deal_Ticket_Del  

DESCRIPTION: It Deletes Rm Deal Ticket for Selected Rm Deal Ticket Id.
      
INPUT PARAMETERS:          
	NAME					DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Rm_Deal_Ticket_Id		INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
	EXEC dbo.Rm_Deal_Ticket_Del 101945
  Rollback Tran

	SELECT * FROM RM_DEAL_TICKET a 
	WHERE NOT EXISTS (SELECT 1 FROM RM_DEAL_TICKET_DETAILS b WHERE a.Rm_Deal_Ticket_Id = b.Rm_Deal_Ticket_Id)


AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH
	RR			Raghu Reddy

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			31-AUG-10	CREATED
	RR			2019-06-19	GRM - Updated to new schema
*/

CREATE PROCEDURE [dbo].[Rm_Deal_Ticket_Del]
      ( 
       @Rm_Deal_Ticket_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;

      DELETE FROM
            Trade.Deal_Ticket
      WHERE
            Deal_Ticket_Id = @Rm_Deal_Ticket_Id
END

GO
GRANT EXECUTE ON  [dbo].[Rm_Deal_Ticket_Del] TO [CBMSApplication]
GO
