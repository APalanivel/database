SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE dbo.SR_RFP_GET_DEFAULT_LP_ADDITIONAL_ROWS_P
	@user_id varchar(10),
	@session_id varchar(20),
	@vendor_id int,
	@commodity_type_id int
	AS
	set nocount on
select	additional_row.sr_load_profile_additional_row_id as row_id,
       	additional_row.row_name,
	additional_row.row_value
from
	sr_load_profile_default_setup setup(nolock),
	sr_load_profile_additional_row additional_row(nolock)
where 
	additional_row.sr_load_profile_default_setup_id = setup.sr_load_profile_default_setup_id	
	and setup.commodity_type_id = @commodity_type_id
	and setup.vendor_id = @vendor_id
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_DEFAULT_LP_ADDITIONAL_ROWS_P] TO [CBMSApplication]
GO
