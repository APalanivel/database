SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
   
/*****
        
NAME: dbo.Report_DE_Vendor_SVRCID_Details      
    
DESCRIPTION:        
	used to get the details of vendors
       
INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
    
USAGE EXAMPLES:
------------------------------------------------------------
EXEC Report_DE_Vendor_SVRCID_Details      
    
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
AKR   Ashok Kumar Raju
     
MAINTENANCE LOG:        
Initials	Date		Modification 
--- ---------- ----------------------------------------------        
  AKR    2012-05-01  Cloned from select statement of Job "2008_VendorRateCommodityState"
******/

CREATE PROCEDURE dbo.Report_DE_Vendor_SVRCID_Details
AS 
BEGIN        
    
      SET NOCOUNT ON ;    
      
      SELECT
            st.State_Name [State]
           ,v.vendor_name [Vendor]
           ,r.rate_name [Rate]
           ,com.Commodity_Name [Commodity]
           ,convert(VARCHAR, st.state_id, 0) + '|' + convert(VARCHAR, v.vendor_id, 0) + '|' + convert(VARCHAR, r.rate_id, 0) + '|' + convert(VARCHAR, r.commodity_type_id, 0) [SVRCID]
      FROM
            dbo.vendor v
            JOIN dbo.vendor_state_map vsm
                  ON vsm.vendor_id = v.vendor_id
            JOIN dbo.rate r
                  ON r.vendor_id = v.vendor_id
            JOIN dbo.Commodity com
                  ON com.Commodity_Id = r.commodity_type_id
            JOIN dbo.state st
                  ON st.state_id = vsm.state_id
      ORDER BY
            state
           ,vendor
           ,commodity
           ,rate
    
END 



;

GO
GRANT EXECUTE ON  [dbo].[Report_DE_Vendor_SVRCID_Details] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_Vendor_SVRCID_Details] TO [CBMSApplication]
GO
