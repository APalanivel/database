SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
Name:      
 dbo.Insert_ICQ_Log_By_Account_Config_Id    
     
Description:      
     
Input Parameters:      
    Name								DataType       Default    Description      
-----------------------------------------------------------------------------      
@Invoice_Collection_Account_Config_id	INT
@Log_Message							NVARCHAR(500)
@User_Info_Id							INT 
     
 Output Parameters:      
 Name      Datatype Default   Description      
-------------------------------------------------------------------------      
     
 Usage Examples:    
------------------------------------------------------------      
 EXEC dbo.Insert_ICQ_Log_by_Account_config_id 553, 'Testing data', 16   
  
        
Author Initials:      
 Initials Name    
------------------------------------------------------------    
 PR  Pradip Rajput
     
 Modifications :      
 Initials   Date				Modification      
------------------------------------------------------------      
 PR			2017-03-30			Created SP  
 
******/                         


CREATE PROCEDURE [dbo].[Insert_ICQ_Log_By_Account_Config_Id]
      ( 
       @Invoice_Collection_Account_Config_id INT
      ,@Log_Message NVARCHAR(500)
      ,@User_Info_Id INT )
AS 
BEGIN                    
      SET NOCOUNT ON                     
         
      INSERT      INTO ICQ_log
                  ( 
                   Invoice_Collection_Account_Config_id
                  ,Log_Message
                  ,User_Info_Id
                  ,Created_Ts )
      VALUES
                  ( 
                   @Invoice_Collection_Account_Config_id
                  ,@Log_Message
                  ,@User_Info_Id
                  ,GETDATE() )
             
            
END;

;
GO
GRANT EXECUTE ON  [dbo].[Insert_ICQ_Log_By_Account_Config_Id] TO [CBMSApplication]
GO
