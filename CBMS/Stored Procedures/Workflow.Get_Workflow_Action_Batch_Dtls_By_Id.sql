SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    [Workflow].[Get_Workflow_Action_Batch_Dtls_By_Id]
DESCRIPTION: 
------------------------------------------------------------   
 INPUT PARAMETERS:    
 Name   DataType  Default Description    
 @CU_INVOICE_WORKFLOW_ACTION_BATCH_ID
------------------------------------------------------------    
 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
 USAGE EXAMPLES:    
------------------------------------------------------------    
AUTHOR INITIALS:    
Initials	Name    
------------------------------------------------------------    
TRK			Ramakrishna Thummala	Summit Energy 
 
 MODIFICATIONS     
 Initials Date   Modification    
------------------------------------------------------------    
 TRK		11/09/2019 Created
******/  
CREATE PROCEDURE [Workflow].[Get_Workflow_Action_Batch_Dtls_By_Id]         
 (        
  @CU_INVOICE_WORKFLOW_ACTION_BATCH_ID INT        
 )        
AS        
BEGIN        
         
 SET NOCOUNT ON;        
        
    SELECT CIWAB.Cu_Invoice_Workflow_Action_Batch_Id      
       ,CIWAB.Workflow_Queue_Action_Id      
         ,CIWABd.Cu_Invoice_Workflow_Action_Batch_Dtl_Id      
          ,CIWABd.Cu_Invoice_Id      
         ,CIWAB.Lookup_Value AS COMMENT      
          ,CIWAB.Requested_User_Id      
       ,C.CODE_VALUE AS WORK_FLOW_ACTION_VALUE   
FROM workflow.Cu_Invoice_Workflow_Action_Batch CIWAB       
INNER JOIN workflow.Cu_Invoice_Workflow_Action_Batch_Dtl CIWABD on  CIWAB.CU_INVOICE_WORKFLOW_ACTION_BATCH_ID = CIWABD.CU_INVOICE_WORKFLOW_ACTION_BATCH_ID        
Inner JOIN workflow.workflow_queue_action WQC ON  CIWAB.Workflow_Queue_Action_Id = WQC.Workflow_Queue_Action_Id      
INNER JOIN DBO.CODE C ON WQC.workflow_queue_action_CD = C.CODE_Id   
WHERE CIWAB.CU_INVOICE_WORKFLOW_ACTION_BATCH_ID = @CU_INVOICE_WORKFLOW_ACTION_BATCH_ID      

END                    

GO
GRANT EXECUTE ON  [Workflow].[Get_Workflow_Action_Batch_Dtls_By_Id] TO [CBMSApplication]
GO
