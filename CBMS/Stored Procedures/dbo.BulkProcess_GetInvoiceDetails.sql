SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                                
NAME:    [BulkProcess_GetInvoiceDetails]                                          
DESCRIPTION:                                             
------------------------------------------------------------                                               
 Unit tests    
 [BulkProcess_GetInvoiceDetails]     200  
------------------------------------------------------------                                                
AUTHOR INITIALS:                                                
Initials Name                                                
------------------------------------------------------------                                                
PK   Prasan kumar                          
                                             
 MODIFICATIONS                                                 
 Initials  Date  Modification     
 PK  13-06-2020  modified status code to status msg  in result set  
------------------------------------------------------------                                                
                          
******/        
CREATE PROCEDURE [dbo].[BulkProcess_GetInvoiceDetails]                 
    (                
        @CU_INVOICE_BULK_BATCH_ID INT                
    )                
AS                
    BEGIN                
                
        SET NOCOUNT ON;                
              
  UPDATE [LOGDB].[dbo].[XL_Bulk_Invoice_Process_Batch]      
  SET Processing_Step = 'Invoice processing'      
  WHERE [XL_Bulk_Invoice_Process_Batch_Id] = @CU_INVOICE_BULK_BATCH_ID       
        
                
        SELECT                
                          
            [xbi].[XL_Bulk_Data_Process_Batch_Dtl_Id] AS [ID]                
            , [xbi].[XL_Bulk_Invoice_Process_Batch_Id]                
            , [xbi].[Cu_Invoice_Id]                
            , [xbi].[Run_Data_Quality_Test]                
            , [xbi].[Run_Recalc]                
            , [xbi].[Run_Variance_Test]                
            , c.Code_Value  as [Status_Cd]                
            , [xbi].[Error_Msg]                
        FROM                
            [LOGDB].[dbo].[XL_Bulk_Invoice_Process_Batch_Dtl] AS [xbi]      
			JOIN  CODE AS c on xbi.Status_Cd = c.code_id    
    
        WHERE                
            [xbi].[XL_Bulk_Invoice_Process_Batch_Id] = @CU_INVOICE_BULK_BATCH_ID        
			AND    [xbi].[Error_Msg] is null         
    END; 
GO
GRANT EXECUTE ON  [dbo].[BulkProcess_GetInvoiceDetails] TO [CBMSApplication]
GO
