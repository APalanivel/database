SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE dbo.SR_SAD_GET_ACCOUNTS_UNDER_SITES_INFO_P
@sites varchar(500)
AS
	--declare @entityId int
	--select @entityId = entity_id from entity where entity_type = 708 and entity_name='C' and entity_description = 'Commercial' 
	DECLARE @SQL varchar(600)

	SET @SQL = 
	'select account_id , account_number 
	from account
	WHERE site_id IN (' + @sites + ')'

	print(@sql)
	EXEC(@SQL)
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_ACCOUNTS_UNDER_SITES_INFO_P] TO [CBMSApplication]
GO
