
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME: 
		dbo.CBMS_GET_ROLLOUT_EMAIL_DATE_P            

DESCRIPTION:            
            
INPUT PARAMETERS:            
	Name				DataType		Default		Description            
-----------------------------------------------------------------            
	@user_id			VARCHAR(10)
    @session_id			VARCHAR(20)
    @site_id			INT      
            
OUTPUT PARAMETERS:            
	Name				DataType		Default		Description            
-----------------------------------------------------------------            
                 
           
                
USAGE EXAMPLES:            
------------------------------------------------------------   
	SELECT TOP 10 * FROM dbo.SITE WHERE ROLLOUT_EMAIL_SENT_BY IS NOT NULL
	         
	EXEC dbo.CBMS_GET_ROLLOUT_EMAIL_DATE_P 0,0,1562
	EXEC dbo.CBMS_GET_ROLLOUT_EMAIL_DATE_P 0,0,3166
	           
AUTHOR INITIALS:            
	Initials	Name            
------------------------------------------------------------            
	RR			Raghu Reddy            
            
MODIFICATIONS:            
	Initials	Date		Modification            
------------------------------------------------------------            
	RR			2014-05-23	Data Operations Enhancements - Added first name and last name in select list
      
******/
CREATE   PROCEDURE dbo.CBMS_GET_ROLLOUT_EMAIL_DATE_P
      @user_id VARCHAR(10)
     ,@session_id VARCHAR(20)
     ,@site_id INT
AS 
SELECT
      s.rollout_email_date
     ,userinfo.username
     ,userinfo.FIRST_NAME + ' ' + userinfo.LAST_NAME AS Name
FROM
      site s
      INNER JOIN user_info userinfo
		ON s.rollout_email_sent_by = userinfo.user_info_id
WHERE
      s.site_id = @site_id





;
GO

GRANT EXECUTE ON  [dbo].[CBMS_GET_ROLLOUT_EMAIL_DATE_P] TO [CBMSApplication]
GO
