SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Rate_Schedule_Upd_TOU_Schedule_Term_Id

DESCRIPTION:
	Updates/Sets the term of a rate schedule.

INPUT PARAMETERS:
	Name									DataType		Default	Description
-------------------------------------------------------------------------------
	@Rate_Schedule_Id						INT
	@Time_Of_Use_Schedule_Term_Id			INT	

OUTPUT PARAMETERS:
	Name									DataType		Default	Description
-------------------------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------------------------
	
Exec dbo.Rate_Schedule_Upd_TOU_Schedule_Term_Id 1711, 402
Exec dbo.Rate_Schedule_Upd_TOU_Schedule_Term_Id 35368, 402
                   
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------------------------
	CPE			Chaitanya Panduga Eshwar

MODIFICATIONS

	Initials	Date		 Modification
------------------------------------------------------------------------------
	CPE			2012-07-25  Created
	BCH			2012-09-06	Given working examples
******/ 

CREATE PROCEDURE dbo.Rate_Schedule_Upd_TOU_Schedule_Term_Id
( 
 @Rate_Schedule_Id INT
,@Time_Of_Use_Schedule_Term_Id INT )
AS 
BEGIN
      SET NOCOUNT ON ;

      BEGIN TRY
            BEGIN TRAN
            UPDATE
                  dbo.RATE_SCHEDULE
            SET   
                  Time_Of_Use_Schedule_Term_Id = NULLIF(@Time_Of_Use_Schedule_Term_Id, 0)
            WHERE
                  RATE_SCHEDULE_ID = @Rate_Schedule_Id
                  AND ISNULL(Time_Of_Use_Schedule_Term_Id, 0) <> ISNULL(@Time_Of_Use_Schedule_Term_Id, 0)

			-- Delete the mappings from the mapping table if the time of use schdule is changed for a rate schedule
            DELETE
                  TOUSTPBD
            FROM
                  dbo.Time_Of_Use_Schedule_Term_Peak_Billing_Determinant TOUSTPBD
                  JOIN dbo.BILLING_DETERMINANT BD
                        ON TOUSTPBD.BILLING_DETERMINANT_ID = BD.BILLING_DETERMINANT_ID
                  JOIN DBO.ENTITY PARENT
                        ON PARENT.ENTITY_ID = BD.BD_PARENT_TYPE_ID
            WHERE
                  BD.BD_PARENT_ID = @Rate_Schedule_Id
                  AND PARENT.entity_type = 120
                  AND PARENT.entity_name = 'Rate Schedule'
                  AND @@ROWCOUNT > 0
            COMMIT TRAN
      END TRY
      BEGIN CATCH
            IF @@TRANCOUNT > 0 
                  ROLLBACK
            EXEC dbo.usp_RethrowError
      END CATCH
END
;
GO
GRANT EXECUTE ON  [dbo].[Rate_Schedule_Upd_TOU_Schedule_Term_Id] TO [CBMSApplication]
GO
