SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

  
/******  
NAME:  
 
 dbo.cbmsSsoDemoUser_Get
 
 DESCRIPTION:

 INPUT PARAMETERS:

 Name			DataType	Default		Description
------------------------------------------------------------
@User_Info_Id	INT

 OUTPUT PARAMETERS:

 Name   DataType  Default Description
------------------------------------------------------------

 
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.cbmsSsoDemoUser_Get 6418
	EXEC dbo.cbmsSsoDemoUser_Get 6287
	

AUTHOR INITIALS:

Initials	Name
------------------------------------------------------------
 HG			Harihara Suthan G

MODIFICATIONS   
 Initials	Date			Modification
------------------------------------------------------------
 HG			11/09/2010		Comments header added
							Removed the passcode column from the select list as it is not used by the applcation , changes made as a prt dv/sv users pwd expiration enhancement.
							Unused @MyAccountId parameter removed
 HG			11/18/2010		Is_Demo_User = 1 filter added in WHERE clause as this procedure is specific for demo users.
******/
CREATE PROCEDURE dbo.cbmsSsoDemoUser_Get
( 
	@user_info_id INT
)
AS
BEGIN

	SET NOCOUNT ON

	SELECT
		user_info_id
		,username
		,queue_id
		,first_name
		,middle_name
		,last_name
		,email_address
		,is_history
		,access_level
		,client_id
		,division_id
		,site_id
		,prospect_name
		,expiration_date
		,is_demo_user
	FROM
		dbo.USER_INFO
	WHERE
		user_info_id = @user_info_id
		AND is_demo_user = 1

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoDemoUser_Get] TO [CBMSApplication]
GO
