SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                            
Name:   dbo.EC_Meter_Attribute_Sel                     
                            
Description:                            
   This sproc to get the attribute details for a Given id's.                    
                                         
 Input Parameters:                            
    Name        DataType   Default   Description                              
----------------------------------------------------------------------------------------                              
 @Country_Id       INT     NULL              
    @State_Id       INT     NULL              
    @Commodity_Id      INT     NULL                  
    @Start_Index      INT     1                                      
    @End_Index       INT     2147483647                
    @Total_Count      INT     0                                     
              
 Output Parameters:                                  
    Name        DataType   Default   Description                              
----------------------------------------------------------------------------------------                              
                            
 Usage Examples:                                
----------------------------------------------------------------------------------------                 
   Exec dbo.EC_Meter_Attribute_Sel               
   Exec dbo.EC_Meter_Attribute_Sel @Country_Id=98              
   Exec dbo.EC_Meter_Attribute_Sel @Country_Id=98,@State_Id=236              
   Exec dbo.EC_Meter_Attribute_Sel @Country_Id=98,@State_Id=236,@Commodity_Id=290              
   Exec dbo.EC_Meter_Attribute_Sel @Start_Index=1,@End_Index=5              
                 
Author Initials:                            
    Initials  Name                            
----------------------------------------------------------------------------------------                              
 NR   Narayana Reddy                 
 RKV     Ravi Kumar Vegesna              
 SC   Sreenivasulu Cheerala                          
 SLP  Sri Lakshmi Pallikonda  
 Modifications:                            
    Initials        Date   Modification                            
----------------------------------------------------------------------------------------                              
    NR    2015-04-22   Created For AS400.               
    RKV   2016-11-02   Maint-4317 Added New output column Attribute_Use              
    SC    2020-05-15   Added @Vendor_Type_Cd    as optional parameter   
    SLP   2020-05-29  Included the logic to search for Vendor type as below   
					 if Blank : no attributes
					 if Distributor : MA that are tagged as Distributor & Both       
					 if Supplier	: MA that are tagged as Supplier & Both       
					 if Both		: all MA 
                           
******/
CREATE PROCEDURE [dbo].[EC_Meter_Attribute_Sel]
    (
        @Country_Id INT = NULL
        , @State_Id INT = NULL
        , @Commodity_Id INT = NULL
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
        , @Total_Count INT = 0
        , @Vendor_Type_Cd INT = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @Vendor_Type_Cd_Both INT;
        SELECT
            @Vendor_Type_Cd_Both = c.Code_Id
        FROM
            dbo.Code AS c
            JOIN dbo.Codeset AS c2
                ON c2.Codeset_Id = c.Codeset_Id
        WHERE
            c2.Codeset_Name = 'VendorType'
            AND c.Code_Value = 'Both';

		--Incase @Vendor_Type_Cd=Both then all meter attributes must be displayed.
		IF (@Vendor_Type_Cd=@Vendor_Type_Cd_Both) 
			SET @Vendor_Type_Cd=NULL

        IF @Total_Count = 0
            BEGIN
                SELECT
                    @Total_Count = COUNT(1)
                FROM
                    dbo.EC_Meter_Attribute ema
                    INNER JOIN dbo.Commodity C
                        ON ema.Commodity_Id = C.Commodity_Id
                    INNER JOIN dbo.STATE s
                        ON s.STATE_ID = ema.State_Id
                    INNER JOIN dbo.COUNTRY cr
                        ON cr.COUNTRY_ID = s.COUNTRY_ID
                    LEFT JOIN Code cd
                        ON cd.Code_Id = ema.Attribute_Type_Cd
                WHERE
                    (   @Country_Id IS NULL
                        OR  cr.COUNTRY_ID = @Country_Id)
                    AND (   @State_Id IS NULL
                            OR  s.STATE_ID = @State_Id)
                    AND (   @Commodity_Id IS NULL
                            OR  C.Commodity_Id = @Commodity_Id)
                    AND (   @Vendor_Type_Cd IS NULL
                            OR  ema.Vendor_Type_Cd IN ( @Vendor_Type_Cd, @Vendor_Type_Cd_Both ));
            END;
        WITH Cte_Meter_Attribute
        AS (
               SELECT   TOP (@End_Index)
                        ema.EC_Meter_Attribute_Id
                        , cr.COUNTRY_NAME
                        , s.STATE_NAME
                        , ema.EC_Meter_Attribute_Name
                        , cd.Code_Value AS Attribute_Type
                        , ema.Is_Used_In_Calc_Vals
                        , C.Commodity_Name
                        , auc.Code_Id AS Attribute_Use_Cd
                        , auc.Code_Value AS Attribute_Use
                        , vcd.Code_Value AS Vendor_Type_Value
                        , ROW_NUMBER() OVER (ORDER BY
                                                 cr.COUNTRY_NAME
                                                 , s.STATE_NAME
                                                 , C.Commodity_Name
                                                 , ema.EC_Meter_Attribute_Name) AS Row_Num
               FROM
                    dbo.EC_Meter_Attribute ema
                    INNER JOIN dbo.Commodity C
                        ON ema.Commodity_Id = C.Commodity_Id
                    INNER JOIN dbo.STATE s
                        ON s.STATE_ID = ema.State_Id
                    INNER JOIN dbo.COUNTRY cr
                        ON cr.COUNTRY_ID = s.COUNTRY_ID
                    INNER JOIN Code cd
                        ON cd.Code_Id = ema.Attribute_Type_Cd
                    INNER JOIN Code auc
                        ON auc.Code_Id = ema.Attribute_Use_Cd
                    LEFT JOIN Code vcd
                        ON vcd.Code_Id = ema.Vendor_Type_Cd
               WHERE
                    (   @Country_Id IS NULL
                        OR  cr.COUNTRY_ID = @Country_Id)
                    AND (   @State_Id IS NULL
                            OR  s.STATE_ID = @State_Id)
                    AND (   @Commodity_Id IS NULL
                            OR  C.Commodity_Id = @Commodity_Id)
                    AND (   @Vendor_Type_Cd IS NULL
                            OR  ema.Vendor_Type_Cd IN ( @Vendor_Type_Cd, @Vendor_Type_Cd_Both ))
           )
        SELECT
            cma.EC_Meter_Attribute_Id
            , cma.COUNTRY_NAME
            , cma.STATE_NAME
            , cma.Commodity_Name
            , cma.EC_Meter_Attribute_Name
            , cma.Attribute_Type
            , cma.Is_Used_In_Calc_Vals
            , cma.Attribute_Use_Cd
            , cma.Attribute_Use
            , cma.Vendor_Type_Value
            , @Total_Count AS Total_Rows
            , cma.Row_Num
        FROM
            Cte_Meter_Attribute cma
        WHERE
            cma.Row_Num BETWEEN @Start_Index
                        AND     @End_Index
        ORDER BY
            Row_Num;
    END;
    ;
GO

GRANT EXECUTE ON  [dbo].[EC_Meter_Attribute_Sel] TO [CBMSApplication]
GO
