SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        dbo.Market_Forecast_Price_Sel_By_Index                        
                          
Description:                          
        To get market price and forecast pirce if a selected index   
                          
Input Parameters:                          
    Name    DataType        Default     Description                            
--------------------------------------------------------------------------------    
	@Index_Id   INT    
    @Start_Dt	Date    
	@End_Dt		Date
                          
 Output Parameters:                                
	Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
Usage Examples:                              
--------------------------------------------------------------------------------    
	SELECT * FROM dbo.ENTITY e WHERE e.ENTITY_TYPE=272

	EXEC dbo.Market_Forecast_Price_Sel_By_Index  584,'2020-01-01','2020-12-01'  
    
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR			2019-11-29	RM-Budgets Enahancement - Created
	                
******/
CREATE PROCEDURE [dbo].[Market_Forecast_Price_Sel_By_Index]
     (
         @Index_Id INT = NULL
         , @Start_Dt DATE = NULL
         , @End_Dt DATE = NULL
         , @Rm_Forecast_Id INT = NULL
         , @Rm_Budget_Id INT = NULL
     )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Tbl_NymexPrice AS TABLE
              (
                  Service_Month DATE
                  , Market_Price DECIMAL(7, 3)
              );

        DECLARE @BATCH_EXECUTION_DATE DATETIME;

        SELECT
            @Start_Dt = rb.Budget_Start_Dt
            , @End_Dt = rb.Budget_End_Dt
            , @Rm_Forecast_Id = rb.Rm_Forecast_Id
        FROM
            Trade.Rm_Budget rb
        WHERE
            rb.Rm_Budget_Id = @Rm_Budget_Id
            AND @Start_Dt IS NULL;

        INSERT INTO @Tbl_NymexPrice
             (
                 Service_Month
                 , Market_Price
             )
        SELECT
            CAST(nymexsettle.MONTH_IDENTIFIER AS DATE) AS Service_Month
            , CAST(nymex.CLOSE_PRICE AS DECIMAL(7, 3)) Market_Price
        FROM
            dbo.RM_NYMEX_SETTLEMENT AS nymexsettle
            JOIN dbo.RM_NYMEX_DATA nymex
                ON nymexsettle.SETTLEMENT_DATE = (nymex.BATCH_EXECUTION_DATE - 1)
            JOIN dbo.RM_NYMEX_MONTH_SYMBOL nymex_symbol
                ON nymex_symbol.MONTH_INDEX = MONTH(nymexsettle.MONTH_IDENTIFIER)
        WHERE
            nymexsettle.MONTH_IDENTIFIER BETWEEN @Start_Dt
                                         AND     @End_Dt
            AND nymex.SYMBOL = 'NG' + nymex_symbol.MONTH_CODE
                               + RIGHT(CONVERT(VARCHAR, YEAR(nymexsettle.MONTH_IDENTIFIER)), CASE WHEN LEN(RTRIM(
                                                                                                               SUBSTRING(
                                                                                                                   nymex.SYMBOL
                                                                                                                   , PATINDEX(
                                                                                                                         '%[0-9]%'
                                                                                                                         , nymex.SYMBOL)
                                                                                                                   , 2))) = 1 THEN
                                                                                                      1
                                                                                                 WHEN LEN(RTRIM(
                                                                                                              SUBSTRING(
                                                                                                                  nymex.SYMBOL
                                                                                                                  , PATINDEX(
                                                                                                                        '%[0-9]%'
                                                                                                                        , nymex.SYMBOL)
                                                                                                                  , 2))) = 2 THEN
                                                                                                     2
                                                                                             END);

        SELECT
            @BATCH_EXECUTION_DATE = MAX(BATCH_EXECUTION_DATE)
        FROM
            dbo.RM_NYMEX_DATA;

        INSERT INTO @Tbl_NymexPrice
             (
                 Service_Month
                 , Market_Price
             )
        SELECT
            CAST(nymexsettle.MONTH_IDENTIFIER AS DATE) AS Service_Month
            , MAX(CAST(nymex.CLOSE_PRICE AS DECIMAL(7, 3)))  AS Market_Price
        FROM
            dbo.RM_NYMEX_SETTLEMENT AS nymexsettle
            JOIN dbo.RM_NYMEX_DATA nymex
                ON @BATCH_EXECUTION_DATE = nymex.BATCH_EXECUTION_DATE
            JOIN dbo.RM_NYMEX_MONTH_SYMBOL nymex_symbol
                ON nymex_symbol.MONTH_INDEX = MONTH(nymexsettle.MONTH_IDENTIFIER)
        WHERE
            nymexsettle.MONTH_IDENTIFIER BETWEEN @Start_Dt
                                         AND     @End_Dt
            AND nymex.SYMBOL = 'NG' + nymex_symbol.MONTH_CODE
                               + RIGHT(CONVERT(VARCHAR, YEAR(nymexsettle.MONTH_IDENTIFIER)), CASE WHEN LEN(RTRIM(
                                                                                                               SUBSTRING(
                                                                                                                   nymex.SYMBOL
                                                                                                                   , PATINDEX(
                                                                                                                         '%[0-9]%'
                                                                                                                         , nymex.SYMBOL)
                                                                                                                   , 2))) = 1 THEN
                                                                                                      1
                                                                                                 WHEN LEN(RTRIM(
                                                                                                              SUBSTRING(
                                                                                                                  nymex.SYMBOL
                                                                                                                  , PATINDEX(
                                                                                                                        '%[0-9]%'
                                                                                                                        , nymex.SYMBOL)
                                                                                                                  , 2))) = 2 THEN
                                                                                                     2
                                                                                             END)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    @Tbl_NymexPrice tnp
                               WHERE
                                    tnp.Service_Month = CAST(nymexsettle.MONTH_IDENTIFIER AS DATE))
        GROUP BY
            CAST(nymexsettle.MONTH_IDENTIFIER AS DATE);




        SELECT
            dd.DATE_D AS Budget_Month
            , CAST(cp.Market_Price AS DECIMAL(28, 3)) AS Current_Market_Price
            , CAST(rfd.AGGRESSIVE_PRICE AS DECIMAL(28, 3)) AS Projected_Market_5_Percent_Price
            , CAST(rfd.MODERATE_PRICE AS DECIMAL(28, 3)) AS Projected_Market_50_Percent_Price
            , CAST(rfd.CONSERVATIVE_PRICE AS DECIMAL(28, 3)) AS Projected_Market_95_Percent_Price
        FROM
            meta.DATE_DIM dd
            LEFT JOIN dbo.RM_FORECAST_DETAILS rfd
                ON rfd.MONTH_IDENTIFIER = dd.DATE_D
                   AND  rfd.RM_FORECAST_ID = @Rm_Forecast_Id
            LEFT JOIN @Tbl_NymexPrice cp
                ON cp.Service_Month = dd.DATE_D
        WHERE
            dd.DATE_D BETWEEN @Start_Dt
                      AND     @End_Dt;


    END;



GO
GRANT EXECUTE ON  [dbo].[Market_Forecast_Price_Sel_By_Index] TO [CBMSApplication]
GO
