SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE dbo.ADD_METER_HLP_P
	@meterID int,
	@unitID int,
	@hlpFromDate datetime,
	@hlpToDate datetime,
	@usageCost decimal(32,16),
	@hlpVolume decimal(32,16)
	AS
	begin
		set nocount on

		insert into HISTORIC_LOAD_PROFILE (
				METER_ID,
				UNIT_ID,
				HLP_FROM_DATE,
				HLP_TO_DATE,
				USAGE_COST,
				HLP_VOLUME) 
		values (	@meterID,
				@unitID,
				@hlpFromDate,
				@hlpToDate,
				@usageCost,
				@hlpVolume)
	end
	



GO
GRANT EXECUTE ON  [dbo].[ADD_METER_HLP_P] TO [CBMSApplication]
GO
