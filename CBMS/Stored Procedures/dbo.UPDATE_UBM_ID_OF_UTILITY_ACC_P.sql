SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- exec dbo.UPDATE_UBM_ID_OF_UTILITY_ACC_P 1, 201

CREATE   PROCEDURE dbo.UPDATE_UBM_ID_OF_UTILITY_ACC_P
	@ubmID int,
	@clientID int
	AS
	begin
		set nocount on

			UPDATE ACCOUNT 	SET UBM_ID = @ubmID 
			from client cli join division d on d.client_id = cli.client_id
			and cli.client_id = @clientID
			join site s on s.division_id = d.division_id
			join account a on a.site_id = s.site_id

	 
			UPDATE ACCOUNT 	SET UBM_ID = @ubmID 
			where account_id in(
			select distinct suppacc.account_id
			from client cli join division d on d.client_id = cli.client_id
			and cli.client_id = @clientID
			join site s on s.division_id = d.division_id
			join account a on a.site_id = s.site_id
			join meter met on met.account_id = a.account_id
			join supplier_account_meter_map map on map.meter_id = met.meter_id
			join account suppacc on suppacc.account_id = map.account_id
			and suppacc.account_type_id = 37)
	end
GO
GRANT EXECUTE ON  [dbo].[UPDATE_UBM_ID_OF_UTILITY_ACC_P] TO [CBMSApplication]
GO
