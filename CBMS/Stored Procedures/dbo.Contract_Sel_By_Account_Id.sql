SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Contract_Sel_By_Account_Id]  
     
DESCRIPTION: 
	To Get Contract Id's and Vendor Details for Selected Account Id.
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Account_Id		INT						Utility Account
@StartIndex		INT			1			
@EndIndex		INT			2147483647
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Contract_Sel_By_Account_Id  123,1,25
	EXEC dbo.Contract_Sel_By_Account_Id  6499

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			25-MAY-10	CREATED

*/

CREATE PROCEDURE dbo.Contract_Sel_By_Account_Id
	(
	@Account_Id		INT
	,@StartIndex	INT		= 1
	,@EndIndex		INT		= 2147483647
	)
AS
BEGIN

	SET NOCOUNT ON;

	WITH Cte_Contract_List AS
	(
		SELECT
			Con.ED_CONTRACT_NUMBER
			,sacc.VENDOR_ID
			,ROW_NUMBER() OVER(ORDER BY con.Ed_Contract_Number) Row_Num
			,COUNT(1) OVER() Total_Rows
		FROM
			dbo.METER m
			JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP map
				 ON map.METER_ID = m.METER_ID
			JOIN dbo.Contract con
				ON con.CONTRACT_ID = map.Contract_ID
			JOIN dbo.ACCOUNT sacc
				 ON sacc.ACCOUNT_ID = map.ACCOUNT_ID
		WHERE
			 m.ACCOUNT_ID = @Account_Id
		GROUP BY
			Con.ED_CONTRACT_NUMBER
			,sacc.VENDOR_ID
	)
	SELECT
		cl.ED_CONTRACT_NUMBER
		,v.VENDOR_NAME
		,cl.Total_Rows
	FROM
		Cte_Contract_List cl
		JOIN VENDOR v
			ON v.VENDOR_ID = cl.VENDOR_ID
	WHERE
		cl.Row_Num BETWEEN @StartIndex AND @EndIndex

END
GO
GRANT EXECUTE ON  [dbo].[Contract_Sel_By_Account_Id] TO [CBMSApplication]
GO
