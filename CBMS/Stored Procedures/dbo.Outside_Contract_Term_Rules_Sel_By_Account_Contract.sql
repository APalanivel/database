SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.Outside_Contract_Term_Rules_Sel_By_Account_Contract            
                        
 DESCRIPTION:                        
			To get Supplier Account Details                       
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
------------------------------------------------------------------------------     
@Account_Id						INT
@Start_Dt						Date
@End_Dt							Date			                      
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
------------------------------------------------------------------------------     
                        
 USAGE EXAMPLES:                            
------------------------------------------------------------------------------     

EXEC dbo.Outside_Contract_Term_Rules_Sel_By_Account_Contract
    @Account_Id = 1655842
    , @Contract_Id = 166699
   
 AUTHOR INITIALS:        
       
 Initials              Name        
------------------------------------------------------------------------------     
 NR						Narayana Reddy
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
------------------------------------------------------------------------------     
 NR                    2019-05-29       Created for - Add Contract.
                       
******/

CREATE PROCEDURE [dbo].[Outside_Contract_Term_Rules_Sel_By_Account_Contract]
    (
        @Account_Id INT
        , @Contract_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;


        SELECT
            'Account Level' AS Account_Level
            , aloct.Account_Id
            , aloct.Contract_Id
            , Parameter_Cd.Code_Value AS OCT_Parameter
            , Dates_Cd.Code_Value AS OCT_Tolerance_Date
            , Range_Cd.Code_Value AS OCT_Range
            , aloct.Config_Start_Dt
            , aloct.Config_End_Dt
            , CASE WHEN cha.Account_Type = 'Utility' THEN 'Consolidated Account'
                  WHEN cha.Account_Type = 'Supplier' THEN 'Supplier Account'
              END AS Valcon_Type
        FROM
            dbo.Account_Outside_Contract_Term_Config aloct
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = aloct.Account_Id
            LEFT JOIN dbo.Code Parameter_Cd
                ON Parameter_Cd.Code_Id = aloct.OCT_Parameter_Cd
            LEFT JOIN dbo.Code Dates_Cd
                ON Dates_Cd.Code_Id = aloct.OCT_Tolerance_Date_Cd
            LEFT JOIN dbo.Code Range_Cd
                ON Range_Cd.Code_Id = aloct.OCT_Dt_Range_Cd
        WHERE
            aloct.Account_Id = @Account_Id
            AND aloct.Contract_Id = @Contract_Id
        GROUP BY
            aloct.Account_Id
            , aloct.Contract_Id
            , Parameter_Cd.Code_Value
            , Dates_Cd.Code_Value
            , Range_Cd.Code_Value
            , aloct.Config_Start_Dt
            , aloct.Config_End_Dt
            , CASE WHEN cha.Account_Type = 'Utility' THEN 'Consolidated Account'
                  WHEN cha.Account_Type = 'Supplier' THEN 'Supplier Account'
              END;




    END;



GO
GRANT EXECUTE ON  [dbo].[Outside_Contract_Term_Rules_Sel_By_Account_Contract] TO [CBMSApplication]
GO
