SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********   
NAME:  dbo.Consumption_Level_Variance_Test_SEL_By_Commodity  
 
DESCRIPTION:  Used to select consumption level value from Variance_consumption_level table using the Commodity Id  

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
@Commodity_Id			Int   
        
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:   

	Consumption_Level_Variance_Test_SEL_By_Commodity 291
	Consumption_Level_Variance_Test_SEL_By_Commodity 290
	

------------------------------------------------------------  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
NK   Nageswara Rao Kosuri         


Initials Date  Modification  
------------------------------------------------------------  
NK	10/12/2009  Created


******/  


CREATE PROCEDURE dbo.Consumption_Level_Variance_Test_SEL_By_Commodity
	@Commodity_Id Int	
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT Variance_Consumption_Level_Id,
		Consumption_Level_Desc
	FROM dbo.Variance_consumption_level		
	WHERE Commodity_Id = @Commodity_Id	

END
GO
GRANT EXECUTE ON  [dbo].[Consumption_Level_Variance_Test_SEL_By_Commodity] TO [CBMSApplication]
GO
