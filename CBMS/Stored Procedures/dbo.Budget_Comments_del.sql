SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Budget_Comments_del

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	 @Vendor_Id				INT
     @Commodity_Type_Id		INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN
		EXEC Budget_Comments_del 125,290
	ROLLBACK TRAN
	
	SELECT * FROM utility_budget_comments WHERE VENDOR_ID = 125 AND COMMODITY_TYPE_ID = 290
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	SKA			01/12/2011	Created
******/
  
CREATE PROCEDURE dbo.Budget_Comments_del
      (
       @Vendor_Id INT
      ,@Commodity_Type_Id INT )
AS 
BEGIN  
  
      SET NOCOUNT ON ;  
  
      DELETE
            dbo.utility_budget_comments
      WHERE
            VENDOR_ID = @Vendor_Id
            AND COMMODITY_TYPE_ID = @Commodity_Type_Id  
  
END 





GO
GRANT EXECUTE ON  [dbo].[Budget_Comments_del] TO [CBMSApplication]
GO
