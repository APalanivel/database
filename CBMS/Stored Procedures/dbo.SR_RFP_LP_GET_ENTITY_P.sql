SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.SR_RFP_LP_GET_ENTITY_P
	AS
	set nocount on	
	select entity_name, entity_id from entity(nolock) 
	where entity_type = 1031 
		and entity_name IN( 'Actual LP Value', 'Actual Avg LP Value', 'Historical Previous LP Value', 'Historical Previous To Previous LP Value')
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_LP_GET_ENTITY_P] TO [CBMSApplication]
GO
