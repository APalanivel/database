SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******    
NAME:    
      dbo.Utility_Account_Commodity_Analyst_Sel  
     
 DESCRIPTION:     
      Gets the Analyst for a given Client,Division,Site,Account.  
        
 INPUT PARAMETERS:    
 Name   DataType Default Description    
------------------------------------------------------------    
  @Vendor_Id  INT     
  @Commodity_Id  INT  
  @Start_Index  INT    1  
  @End_Index  INT   2147483647  
   
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 USAGE EXAMPLES:    
------------------------------------------------------------  
  
 EXEC dbo.Utility_Account_Commodity_Analyst_Sel 10,290  
 EXEC dbo.Utility_Account_Commodity_Analyst_Sel 1202,290  
  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------   
 AKR  Ashok Kumar Raju  
   
  
 MODIFICATIONS     
 Initials Date  Modification  
------------------------------------------------------------  
 AKR        2012-09-26  Created for POCO  
  
******/  
CREATE  PROCEDURE dbo.Utility_Account_Commodity_Analyst_Sel
      ( 
       @Vendor_Id INT
      ,@Commodity_Id INT
      ,@Start_Index INT = 1
      ,@End_Index INT = 2147483647 )
AS 
BEGIN  
      SET nocount ON ;  
        
         
      DECLARE @Commodity_Service_Cd INT  
        
      SELECT
            @Commodity_Service_Cd = Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'CommodityServiceSource'
            AND c.Code_Value = 'Invoice' ;
      WITH  cte_Vendor_Accounts
              AS ( SELECT
                        ch.Client_Name
                       ,ch.Site_name
                       ,cha.Account_Number
                       ,cha.Account_Id
                       ,coalesce(acm.Analyst_User_Info_Id, sca.Analyst_User_Info_Id, ca.Analyst_User_Info_Id) Analyst_User_Info_Id
                       ,row_number() OVER ( ORDER BY ch.Client_Name, ch.Site_name, cha.Account_Number ) Row_Num
                       ,count(1) OVER ( ) AS Total_Row_Count
                   FROM
                        dbo.VENDOR_COMMODITY_MAP vcam
                        INNER JOIN core.Client_Hier_Account cha
                              ON cha.Account_Vendor_Id = vcam.VENDOR_ID
                                 AND vcam.COMMODITY_TYPE_ID = cha.Commodity_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN Core.Client_Commodity cc
                              ON ch.Client_Id = cc.Client_Id
                                 AND cc.Commodity_Id = @Commodity_Id
                                 AND cc.Commodity_Service_Cd = @Commodity_Service_Cd
                        LEFT JOIN dbo.Account_Commodity_Analyst acm
                              ON cha.Account_Id = acm.Account_Id
                                 AND acm.Commodity_Id = @Commodity_Id
                        LEFT JOIN dbo.Site_Commodity_Analyst sca
                              ON ch.Site_Id = sca.Site_Id
                                 AND sca.Commodity_Id = @Commodity_Id
                        LEFT JOIN dbo.Client_Commodity_Analyst ca
                              ON ca.Client_Commodity_Id = cc.Client_Commodity_Id
                   WHERE
                        vcam.COMMODITY_TYPE_ID = @Commodity_Id
                        AND vcam.VENDOR_ID = @Vendor_Id
                        AND cha.Account_Type = 'Utility'
                   GROUP BY
                        ch.Client_Name
                       ,ch.Site_name
                       ,cha.Account_Number
                       ,cha.Account_Id
                       ,acm.Analyst_User_Info_Id
                       ,sca.Analyst_User_Info_Id
                       ,ca.Analyst_User_Info_Id)
            SELECT
                  cte_va.Client_Name
                 ,cte_va.Site_name
                 ,cte_va.Account_Number
                 ,cte_va.Account_Id
                 ,cte_va.Analyst_User_Info_Id
                 ,cte_va.Row_Num
                 ,cte_va.Total_Row_Count
            FROM
                  cte_Vendor_Accounts cte_va
            WHERE
                  cte_va.Row_Num BETWEEN @Start_Index
                                 AND     @End_Index  
        
        
END  
;
GO
GRANT EXECUTE ON  [dbo].[Utility_Account_Commodity_Analyst_Sel] TO [CBMSApplication]
GO
