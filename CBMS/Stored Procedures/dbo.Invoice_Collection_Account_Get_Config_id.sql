SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********                            
NAME: dbo.invoice_collection_Account_Get_Config_id                             
                          
DESCRIPTION:                               
                          
 To get the Account config details                         
                           
INPUT PARAMETERS:                              
Name     DataType Default   Description                              
-------------------------------------------------------------------------                              
@Invoice_collection_Account_Get_Config_id      INT                               
                            
                          
OUTPUT PARAMETERS:                              
Name     DataType Default   Description                              
-------------------------------------------------------------------------                              
                          
USAGE EXAMPLES:                              
------------------------------------------------------------                              
         
 exec invoice_collection_Account_Get_Config_id  234                
                            
                          
AUTHOR INITIALS:                              
Initials Name                              
------------------------------------------------------------                              
PR   Pradip Rajuput                          
                         
                           
MODIFICATIONS                               
Initials Date  Modification                              
------------------------------------------------------------                              
PR   2020-03-31 Created                          
                           
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Account_Get_Config_id]
    @invoice_Collection_Account_Config_Id INT
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            icac.Account_Id
        FROM
            dbo.Invoice_Collection_Account_Config icac
            JOIN dbo.ACCOUNT a
                ON a.ACCOUNT_ID = icac.Account_Id
        WHERE
            icac.Invoice_Collection_Account_Config_Id = @invoice_Collection_Account_Config_Id;

    END;

GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Account_Get_Config_id] TO [CBMSApplication]
GO
