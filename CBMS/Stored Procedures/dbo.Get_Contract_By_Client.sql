SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:      
 Get_Contract_By_Client    
       
DESCRIPTION:      
      
      
INPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
 @Client_Id int    
 @Start_Dt DATE    
 @End_Dt DATE    
 @Site_Id int NULL    
                                              
      
OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
      
USAGE EXAMPLES:      
------------------------------------------------------------      
 SET STATISTICS IO ON      
 EXEC dbo.Get_Contract_By_CLient @Client_Id = 109 ,@Start_Dt ='2010-01-01', @End_Dt ='2019-01-01',  @Site_Id  = NULL    
     
AUTHOR INITIALS:      
 Initials Name      
------------------------------------------------------------      
    PRG   Prasanna Raghavendra G    
 SLP   Sri Lakshmi Pallikonda  
      
MODIFICATIONS:      
 Initials Date  Modification      
------------------------------------------------------------      
PRG  Created For Budget 2.0      
SLP  2020-06-02		Included the parameter @Commodity_Id and removed the condition
					Account_Not_Managed and Client_Not_Managed
	 
******/

CREATE PROC [dbo].[Get_Contract_By_Client]
    (
        @Client_Id INT
        , @Start_Dt DATE = '1900-01-01'
        , @End_Dt DATE = '2099-01-01'
        , @Site_Id INT = NULL
        , @Commodity_Id INT = NULL
    )
AS
    BEGIN

        SELECT
            cha.Supplier_Contract_ID
            , c.ED_CONTRACT_NUMBER
        FROM
            Core.Client_Hier AS ch
            INNER JOIN Core.Client_Hier_Account AS cha
                ON cha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.CONTRACT AS c
                ON cha.Supplier_Contract_ID = c.CONTRACT_ID
        WHERE
            cha.Account_Type = 'Supplier'
            AND ch.Client_Id = @Client_Id
            AND (   @Site_Id IS NULL
                    OR  ch.Site_Id = @Site_Id)
            AND (   @Commodity_Id IS NULL
                    OR  cha.Commodity_Id = @Commodity_Id)
            AND (   c.CONTRACT_START_DATE BETWEEN @Start_Dt
                                          AND     @End_Dt
                    OR  c.CONTRACT_END_DATE BETWEEN @Start_Dt
                                            AND     @End_Dt
                    OR  @Start_Dt BETWEEN c.CONTRACT_START_DATE
                                  AND     c.CONTRACT_END_DATE
                    OR  @End_Dt BETWEEN c.CONTRACT_START_DATE
                                AND     c.CONTRACT_END_DATE)
        GROUP BY
            cha.Supplier_Contract_ID
            , c.ED_CONTRACT_NUMBER
        ORDER BY
            c.ED_CONTRACT_NUMBER;
    END;
GO
GRANT EXECUTE ON  [dbo].[Get_Contract_By_Client] TO [CBMSApplication]
GO
