SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.GET_HEDGE_DETAILS_P
	@hedge_id INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT h.contract_id,
		h.hedge_name,
		h.hedge_transaction_date,
		h.hedge_unit_type_id,
		h.deal_type_id,
		h.transaction_by_type_id,
		hedge_comments,
		DATENAME(MONTH, month_identifier),
		DATENAME(YEAR, month_identifier),
		hd.hedge_detail_id,hd.hedge_volume,hd.hedge_price 
	FROM dbo.hedge h INNER JOIN dbo.hedge_detail hd ON hd.hedge_id=h.hedge_id
		AND h.hedge_id=@hedge_id

END
GO
GRANT EXECUTE ON  [dbo].[GET_HEDGE_DETAILS_P] TO [CBMSApplication]
GO
