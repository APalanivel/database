SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Invoice_Collection_Queue_Exists_Account_Config_Id_Frequency_Id         
                
Description:                
   This sproc is used to fill the ICQ Batch Table.        
                             
 Input Parameters:                
    Name										DataType   Default   Description                  
--------------------------------------------------------------------------------------                  
   @Invoice_Collection_Account_Config_Id INT
   @Account_Invoice_Collection_Frequency_Id VARCHAR(MAX) = NULL                      
   
 Output Parameters:                      
    Name        DataType   Default   Description                  
--------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
--------------------------------------------------------------------------------------     
  exec dbo.Invoice_Collection_Queue_Exists_Account_Config_Id_Frequency_Id 288,266
    
Author Initials:                
    Initials  Name                
--------------------------------------------------------------------------------------                  
 RKV    Ravi Kumar Vegesna  
 Modifications:                
    Initials        Date   Modification                
--------------------------------------------------------------------------------------                  
    RKV    2017-02-03  Created For Invoice_Collection.           
               
******/   
CREATE PROCEDURE [dbo].[Invoice_Collection_Queue_Exists_Account_Config_Id_Frequency_Id]
      ( 
       @Invoice_Collection_Account_Config_Id INT
      ,@Account_Invoice_Collection_Frequency_Id VARCHAR(MAX) = NULL )
AS 
BEGIN  
      SET NOCOUNT ON;  
    
 
      DECLARE @ICQ_Exists BIT = 0
      SELECT
            @ICQ_Exists = 1
      FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN dbo.Invoice_Collection_Queue_Month_Map icqmm
                  ON icq.Invoice_Collection_Queue_Id = icqmm.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Account_Invoice_Collection_Month aicm
                  ON icqmm.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id
                     AND aicm.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
            INNER JOIN dbo.Invoice_Collection_Chase_Log_Queue_Map icclqm
                  ON icq.Invoice_Collection_Queue_Id = icclqm.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Invoice_Collection_Chase_Log iccl
                  ON icclqm.Invoice_Collection_Chase_Log_Id = iccl.Invoice_Collection_Chase_Log_Id
            INNER JOIN dbo.Code sc
                  ON sc.Code_Id = iccl.Status_Cd
                     AND sc.Code_Value = 'Close'
      WHERE
            aicm.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
            AND ( @Account_Invoice_Collection_Frequency_Id IS NULL
                  OR EXISTS ( SELECT
                                    1
                              FROM
                                    dbo.ufn_split(@Account_Invoice_Collection_Frequency_Id, ',') ufn
                              WHERE
                                    aicm.Account_Invoice_Collection_Frequency_Id = ufn.Segments ) )  
		
      SELECT
            @ICQ_Exists ICQ_Exists


END;

;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Queue_Exists_Account_Config_Id_Frequency_Id] TO [CBMSApplication]
GO
