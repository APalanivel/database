SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Account_Invoice_Processing_Config_Sel_By_Cu_Invoice_Id         
                
Description:                
   This sproc to get the bucket details details for a Given id.        
                             
 Input Parameters:                
    Name				DataType		Default				Description                  
----------------------------------------------------------------------------------------                  
    @@Cu_Invoice_Id     INT   
      
 Output Parameters:                      
    Name				DataType		Default				Description                  
----------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
----------------------------------------------------------------------------------------     
  
   Exec dbo.Account_Invoice_Processing_Config_Sel_By_Cu_Invoice_Id 52638583
   
    
Author Initials:                
    Initials		Name                
----------------------------------------------------------------------------------------                  
	NR				Narayana Reddy 
	                
 Modifications:                
    Initials        Date			Modification                
----------------------------------------------------------------------------------------                  
    NR				2019-02-01		Created For Watch List - B.   
               
******/


CREATE PROCEDURE [dbo].[Account_Invoice_Processing_Config_Sel_By_Cu_Invoice_Id]
    (
        @Cu_Invoice_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            cism.CU_INVOICE_ID
            , cism.Account_ID
            , cism.SERVICE_MONTH
            , aipc.Config_Start_Dt
            , aipc.Config_End_Dt
            , aipc.Watch_List_Group_Info_Id
            , cism.Begin_Dt
            , cism.End_Dt
        FROM
            dbo.Account_Invoice_Processing_Config aipc
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.Account_ID = aipc.Account_Id
        WHERE
            cism.CU_INVOICE_ID = @Cu_Invoice_Id
            AND aipc.Watch_List_Group_Info_Id IS NOT NULL
            AND aipc.Is_Active = 1
            AND (   cism.Begin_Dt BETWEEN aipc.Config_Start_Dt
                                  AND     ISNULL(aipc.Config_End_Dt, '2099-12-31')
                    OR  cism.End_Dt BETWEEN aipc.Config_Start_Dt
                                    AND     ISNULL(aipc.Config_End_Dt, '2099-12-31')
                    OR  aipc.Config_Start_Dt BETWEEN cism.Begin_Dt
                                             AND     cism.End_Dt
                    OR  ISNULL(aipc.Config_End_Dt, '2099-12-31') BETWEEN cism.Begin_Dt
                                                                 AND     cism.End_Dt)
        GROUP BY
            cism.CU_INVOICE_ID
            , cism.Account_ID
            , cism.SERVICE_MONTH
            , aipc.Config_Start_Dt
            , aipc.Config_End_Dt
            , aipc.Watch_List_Group_Info_Id
            , cism.Begin_Dt
            , cism.End_Dt;


    END;
    ;


GO
GRANT EXECUTE ON  [dbo].[Account_Invoice_Processing_Config_Sel_By_Cu_Invoice_Id] TO [CBMSApplication]
GO
