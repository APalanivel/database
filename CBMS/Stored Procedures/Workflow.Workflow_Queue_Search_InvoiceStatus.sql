SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    [Workflow].[Workflow_Queue_Search_InvoiceStatus]
DESCRIPTION: it'll return the Invoice status information 
------------------------------------------------------------   
 INPUT PARAMETERS:    
 Name   DataType  Default Description    
 @Key_Word varchar(200) = NULL,    
 @Start_Index INT = 1,    
 @End_Index INT = 2147483647
------------------------------------------------------------    
 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
 USAGE EXAMPLES:    
------------------------------------------------------------    
AUTHOR INITIALS:    
Initials	Name    
------------------------------------------------------------    
TRK			Ramakrishna Thummala	Summit Energy 
 
 MODIFICATIONS     
 Initials Date   Modification    
------------------------------------------------------------    
 TRK		  Aug-2019    Created
 TRK		  Sep-2019 Migrated to Workflow Schema.

******/   
CREATE PROCEDURE [Workflow].[Workflow_Queue_Search_InvoiceStatus]       
 @Key_Word varchar(200) = NULL,        
 @Start_Index INT = 1,        
 @End_Index INT = 2147483647        
AS        
BEGIN        
        
 SET NOCOUNT ON         
  
  DECLARE @ENTITY_TYPE INT 
  SELECT @ENTITY_TYPE = ENTITY_TYPE FROM dbo.ENTITY WHERE ENTITY_DESCRIPTION='UBM Exception Status'
        
 ;WITH CTE         
 AS         
 (        
 SELECT ENTITY_ID        
  , ENTITY_NAME        
  , ROW_NUMBER() OVER (ORDER BY ENTITY_ID,ENTITY_NAME) AS Row_Num        
 FROM dbo.ENTITY        
 WHERE ENTITY_TYPE= @ENTITY_TYPE AND (@Key_Word IS NULL OR [ENTITY_NAME] Like '%' + isNull(@Key_Word, ENTITY_NAME) + '%')        
 )        
 SELECT ENTITY_ID , ENTITY_NAME FROM CTE WHERE Row_Num BETWEEN @Start_Index  AND     @End_Index        
        ORDER BY ENTITY_NAME;        
        
END     

GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_Search_InvoiceStatus] TO [CBMSApplication]
GO
