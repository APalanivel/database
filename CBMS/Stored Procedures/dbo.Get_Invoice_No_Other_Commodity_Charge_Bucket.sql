SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
   
 dbo.Get_Invoice_No_Other_Commodity_Charge_Bucket  
  
DESCRIPTION:  
   Stored procedure will list these charge buckets Fees,Late Fees,Other Charge,Other Commodity Charge,Tax - Misc  
  
 INPUT PARAMETERS:            
                       
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
@Commodity_Id     int     -1  indicates commodity type(water or kerosene)  
@bucket_Type_Code    VARCHAR(25)   'Charge'  indicates which kind of bubket(chnarges or billing determinats)  
@Is_Shown_on_Site    BIT    NULL  
@Is_Shown_On_Account   BIT    NULL  
@Is_Shown_On_Invoice   BIT    NULL  
  
 OUTPUT PARAMETERS:            
                             
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
  
 USAGE EXAMPLES:                              
---------------------------------------------------------------------------------------------------------------                              
   
 EXEC dbo.Get_Invoice_No_Other_Commodity_Charge_Bucket  
  
  
 AUTHOR INITIALS:          
         
 Initials              Name          
---------------------------------------------------------------------------------------------------------------                        
 SP                    Sandeep Pigilam            
  
  
 MODIFICATIONS:        
            
 Initials              Date             Modification        
---------------------------------------------------------------------------------------------------------------        
  SP                   2014-07-16       Data Operations Enhancement Phase III,Created                  
*/ 
  
CREATE PROCEDURE dbo.Get_Invoice_No_Other_Commodity_Charge_Bucket
      @CommodityId INT = -1
     ,@bucket_Type_Cd_Value VARCHAR(25) = 'Charge'
     ,@Is_Shown_on_Site BIT = NULL
     ,@Is_Shown_On_Account BIT = NULL
     ,@Is_Shown_On_Invoice BIT = NULL
AS 
BEGIN  
  
      SET NOCOUNT ON  
  
      SELECT
            bm.Bucket_Master_Id AS Entity_Id
           ,bm.Bucket_Name AS Entity_Name
           ,bkt_type.Code_Value AS Bucket_Type
           ,bm.Is_Shown_on_Site
           ,bm.Is_Shown_On_Account
           ,bm.Is_Shown_On_Invoice
           ,bm.Sort_Order
           ,bm.Default_Uom_Type_Id
      FROM
            dbo.Bucket_Master bm
            INNER JOIN dbo.Code bkt_type
                  ON bkt_type.Code_id = bm.Bucket_Type_Cd
      WHERE
            bm.Commodity_Id = @commodityId
            AND bm.Is_Active = 1
            AND ( @bucket_Type_Cd_Value IS NULL
                  OR bkt_type.Code_Value = @bucket_Type_Cd_Value )
            AND ( @Is_Shown_on_Site IS NULL
                  OR bm.Is_Shown_on_Site = @Is_Shown_on_Site )
            AND ( @Is_Shown_On_Account IS NULL
                  OR bm.is_Shown_On_Account = @Is_Shown_On_Account )
            AND ( @Is_Shown_On_Invoice IS NULL
                  OR bm.Is_Shown_On_Invoice = @Is_Shown_On_Invoice )
            AND bm.Bucket_Name IN ( 'Fees', 'Late Fees', 'Other Charge', 'Other Commodity Charge', 'Tax - Misc' )
      ORDER BY
            bkt_type.Code_Value DESC
           ,bm.Bucket_Name  
   
END; 


;
GO
GRANT EXECUTE ON  [dbo].[Get_Invoice_No_Other_Commodity_Charge_Bucket] TO [CBMSApplication]
GO
