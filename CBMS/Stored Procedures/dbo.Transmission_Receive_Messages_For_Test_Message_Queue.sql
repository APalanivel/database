SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[Transmission_Receive_Messages_For_Test_Message_Queue]
AS
Begin
SELECT getdate()
End
GO
GRANT EXECUTE ON  [dbo].[Transmission_Receive_Messages_For_Test_Message_Queue] TO [sb_CBMS_Service]
GRANT EXECUTE ON  [dbo].[Transmission_Receive_Messages_For_Test_Message_Queue] TO [sb_Sys_Service]
GO
