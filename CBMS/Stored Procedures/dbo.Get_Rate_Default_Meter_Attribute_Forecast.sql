SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                                        
Name :[dbo].[Get_Rate_Default_Meter_Attribute_Forecast]                         
                          
Description:                                        
        To get the rate level default meter attributes for Forecast Type                                  
                          
 Input Parameters:                                        
     Name        DataType   Default   Description                                          
----------------------------------------------------------------------------------------                                          
@Rate_Id          INT                       
                          
 Output Parameters:                                              
    Name        DataType   Default   Description                                          
----------------------------------------------------------------------------------------                                          
                          
 Usage Examples:                                            
----------------------------------------------------------------------------------------                                          
 BEGIN TRAN                            
                         
 EXEC [dbo].[Get_Rate_Default_Meter_Attribute_Forecast]                    
     @Rate_Id =1                              
                          
 ROLLBACK TRAN                               
                       
Author Initials:                                        
    Initials  Name                                        
----------------------------------------------------------------------------------------                                          
SC  Sreenivasulu Cheerala                      
                            
 Modifications:                                        
    Initials        Date  Modification                                        
----------------------------------------------------------------------------------------                                          
 SC    2020-01-24   Initially Created for Budget                      
******/
CREATE PROCEDURE [dbo].[Get_Rate_Default_Meter_Attribute_Forecast]
     (
         @Rate_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @Forcast_Type_Cd INT;

        SELECT
            @Forcast_Type_Cd = cd.Code_Id
        FROM
            dbo.Code cd
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = cd.Codeset_Id
        WHERE
            cs.Codeset_Name = 'MeterAttributeType'
            AND cd.Code_Value = 'Forecasted';


        SELECT
            rdma.Rate_Default_Meter_Attribute_Id
            , ema.EC_Meter_Attribute_Id
            , ema.EC_Meter_Attribute_Name
            , rdma.EC_Meter_Attribute_Value
            , rdma.RATE_ID
            , c.Code_Value AS Attribute_Type
            , rdma.Start_Dt
            , rdma.End_Dt
            , ema.Vendor_Type_Cd
            , rdma.Attribute_Value_Type_Cd Meter_Attribute_Type_Cd
            , cd.Code_Value Meter_Attribute_Name
            , VenCd.Code_Value Vendor_Type_Value
            , Cdd.Code_Value Attribute_Value_Requirement_Option_Name
            , rdma.Attribute_Value_Requirement_Option_Cd
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME Updated_User_Name
            , rdma.Last_Change_Ts Last_Cahnge_Ts
        FROM
            Budget.Rate_Default_Meter_Attribute rdma
            INNER JOIN dbo.RATE r
                ON r.RATE_ID = rdma.RATE_ID
            INNER JOIN dbo.Code Cdd
                ON Cdd.Code_Id = rdma.Attribute_Value_Requirement_Option_Cd
            INNER JOIN dbo.EC_Meter_Attribute ema
                ON ema.EC_Meter_Attribute_Id = rdma.EC_Meter_Attribute_Id
            INNER JOIN Code c
                ON c.Code_Id = ema.Attribute_Type_Cd
            INNER JOIN dbo.Code cd
                ON cd.Code_Id = rdma.Attribute_Value_Type_Cd
            INNER JOIN dbo.USER_INFO ui
                ON ui.USER_INFO_ID = rdma.Updated_User_Id
            LEFT JOIN dbo.Code VenCd
                ON VenCd.Code_Id = ema.Vendor_Type_Cd
        WHERE
            rdma.Attribute_Value_Type_Cd = @Forcast_Type_Cd
            AND rdma.RATE_ID = @Rate_Id
        GROUP BY
            rdma.Rate_Default_Meter_Attribute_Id
            , ema.EC_Meter_Attribute_Id
            , ema.EC_Meter_Attribute_Name
            , rdma.EC_Meter_Attribute_Value
            , c.Code_Value
            , rdma.Start_Dt
            , rdma.End_Dt
            , rdma.RATE_ID
            , ema.Vendor_Type_Cd
            , rdma.Attribute_Value_Type_Cd
            , cd.Code_Value
            , VenCd.Code_Value
            , Cdd.Code_Value
            , rdma.Attribute_Value_Requirement_Option_Cd
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME
            , rdma.Last_Change_Ts
        ORDER BY
            ema.EC_Meter_Attribute_Id;
    END;
GO
GRANT EXECUTE ON  [dbo].[Get_Rate_Default_Meter_Attribute_Forecast] TO [CBMSApplication]
GO
