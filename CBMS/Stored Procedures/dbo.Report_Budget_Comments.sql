SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
	dbo.Report_Budget_Comments
	
DESCRIPTION:

		All of the budget comments for selected budgets
	
INPUT PARAMETERS:
Name				DataType		Default	Description
-------------------------------------------------------------------------------------------
@@Client_id_List	VARCHAR(MAX)
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------

Report_Budget_Comments '223','17,18,2555,2554,3676,4152,4861,4866,3677,4151,1262,1212,5043,5044'
	
AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------
SSR			Sharad srivastava

MODIFICATIONS
Initials	Date		Modification
------------------------------------------------------------
 SSR	   08/19/2010	Created
 */
CREATE PROC [dbo].[Report_Budget_Comments]
    @Client_id_List VARCHAR(MAX)
  , @Budget_Id_list VARCHAR(MAX)
AS 
    BEGIN

        SET NOCOUNT ON

        SELECT
            cl.client_name [Client]
          , s.site_name [Site]
          , acct.account_number [Account]
          , com.Commodity_Name [Commodity]
          , v.vendor_name [Utility]
          , b.budget_name [Budget Name]
          , srvl.entity_name [Service Level]
          , ba.budget_comments [Budget Comments]
          , bc.variable_comments [Variable]
          , bc.Transportation_comments [Transportation]
          , bc.distribution_comments [Distribution]
          , bc.transmission_comments [Transmission]
          , bc.other_bundled_comments [Other Bundled]
          , bc.Other_fixed_costs_comments [Other Fixed Costs]
          , bc.Sourcing_tax_comments [Sourcing Tax]
          , bc.rates_tax_comments [Rates Tax]
        FROM
            dbo.budget_account ba
            JOIN dbo.account acct
                ON acct.account_id = ba.account_id
            JOIN dbo.site s
                ON s.site_id = acct.site_id
            JOIN dbo.client cl
                ON cl.client_id = s.client_id
            JOIN dbo.ufn_split(@Client_id_List, ',') ufn_Client_tmp
                ON CAST(ufn_Client_tmp.Segments AS INT) = cl.CLIENT_ID
            JOIN dbo.budget b
                ON b.budget_id = ba.budget_id
            JOIN dbo.ufn_split(@Budget_Id_list, ',') ufn_budget_tmp
                ON CAST(ufn_budget_tmp.Segments AS INT) = b.BUDGET_ID
            JOIN dbo.Commodity com
                ON com.Commodity_Id = b.COMMODITY_TYPE_ID
            JOIN dbo.entity srvl
                ON srvl.entity_id = acct.service_level_type_id
            JOIN dbo.vendor v
                ON v.vendor_id = acct.vendor_id
            LEFT JOIN dbo.budget_detail_comments bc
                ON bc.budget_account_id = ba.budget_account_id

    END


GO
GRANT EXECUTE ON  [dbo].[Report_Budget_Comments] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_Budget_Comments] TO [CBMSApplication]
GO
