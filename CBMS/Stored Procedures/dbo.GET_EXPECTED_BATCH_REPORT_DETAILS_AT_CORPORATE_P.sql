SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_EXPECTED_BATCH_REPORT_DETAILS_AT_CORPORATE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@batchId       	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE      PROCEDURE dbo.GET_EXPECTED_BATCH_REPORT_DETAILS_AT_CORPORATE_P 
@userId varchar(10),
@sessionId varchar(20),
@batchId int
 AS
set nocount on
select 
	category.report_category_name,
	list.report_name,
	cli.client_name,
	cli.client_id
	 
from 
	rm_batch_configuration config, 
	rm_report_batch batch,
	--rm_batch_configuration_master master,
	report_list list,
	report_category category,
	report_list_report_category_map map,
	client cli

 
where 
	config.rm_batch_configuration_master_id = batch.rm_batch_configuration_master_id
	and list.report_list_id = config.report_list_id
	and map.report_list_id = list.report_list_id
	and category.report_category_id = map.report_category_id
	--and config.client_id in (select distinct client_id from rm_batch_configuration)
	and cli.client_id = config.client_id
	and config.corporate_report  = 1 -- boolean
	and batch.rm_report_batch_id = @batchId

group by 
	category.report_category_name,
	list.report_name,
	cli.client_name,
	cli.client_id,
	config.rm_batch_configuration_master_id

order by 
	category.report_category_name,
	list.report_name,
	cli.client_name
GO
GRANT EXECUTE ON  [dbo].[GET_EXPECTED_BATCH_REPORT_DETAILS_AT_CORPORATE_P] TO [CBMSApplication]
GO
