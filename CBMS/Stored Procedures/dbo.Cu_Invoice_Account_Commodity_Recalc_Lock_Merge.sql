SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Cu_Invoice_Account_Commodity_Recalc_Lock_Merge           
              
Description:              
        To insert Data to Cu_Invoice_Account_Commodity_Recalc_Lock table.              
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
   @Cu_Invoice_Id						 INT
   @Account_Id							 INT
   @Commodity_Id						 INT
   @Is_Locked							 BIT
   @User_Info_Id						 INT
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
    
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                

EXEC Cu_Invoice_Account_Commodity_Recalc_Lock_ins 23647880,660968,291,0,16	         
             
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV			   Ravi Kumar Vegesna
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2016-09-01		Maint-4109: Created For AS400-Enahancement.         
             
******/ 
CREATE PROCEDURE [dbo].[Cu_Invoice_Account_Commodity_Recalc_Lock_Merge]
      ( 
       @Cu_Invoice_Id INT
      ,@Account_Id INT
      ,@Commodity_Id INT
      ,@Is_Locked BIT
      ,@User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON  
      
      DECLARE @Cu_Invoice_Account_Commodity_Id_Out INT
          
      
      SELECT
            @Cu_Invoice_Account_Commodity_Id_Out = Cu_Invoice_Account_Commodity_Id
      FROM
            dbo.Cu_Invoice_Account_Commodity ciac
      WHERE
            ciac.Cu_Invoice_Id = @Cu_Invoice_Id
            AND ciac.Account_Id = @Account_Id
            AND ciac.Commodity_Id = @Commodity_Id
            
      
      
      
      
      IF @Cu_Invoice_Account_Commodity_Id_Out IS NULL 
            EXEC Cu_Invoice_Account_Commodity_Ins 
                  @Cu_Invoice_Id
                 ,@Account_Id
                 ,@Commodity_Id
                 ,@Cu_Invoice_Account_Commodity_Id = @Cu_Invoice_Account_Commodity_Id_Out OUTPUT     
     
      

      MERGE INTO dbo.Cu_Invoice_Account_Commodity_Recalc_Lock AS tgt
            USING 
                  ( SELECT
                        @Cu_Invoice_Account_Commodity_Id_Out Cu_Invoice_Account_Commodity_Id
                       ,@Is_Locked Is_Locked
                       ,@User_Info_Id User_Info_Id ) AS src
            ON tgt.Cu_Invoice_Account_Commodity_Id = src.Cu_Invoice_Account_Commodity_Id
            WHEN NOT MATCHED 
                  THEN              
    INSERT
                        ( 
                         Cu_Invoice_Account_Commodity_Id
                        ,Is_Locked
                        ,Created_User_Id
                        ,Updated_User_Id
                        ,Last_Change_Ts )
                    VALUES
                        ( 
                         src.Cu_Invoice_Account_Commodity_Id
                        ,src.Is_Locked
                        ,src.User_Info_Id
                        ,src.User_Info_Id
                        ,GETDATE() )
            WHEN MATCHED 
                  THEN      
		UPDATE      SET 
                        Is_Locked = src.Is_Locked
                       ,Updated_User_Id = src.User_Info_Id
                       ,Last_Change_Ts = GETDATE();              
                                   
     
     
     
                       


END;
;
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Account_Commodity_Recalc_Lock_Merge] TO [CBMSApplication]
GO
