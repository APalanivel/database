SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** 
NAME: 
dbo.cbmsCostUsage_GetDefault
DESCRIPTION:
To get first currency unit id based on given site,account and server month.
INPUT PARAMETERS: 
Name					DataType	Default		Description 
---------------------------------------------------------------------- 
@Account_Id				INT 
@Site_Client_Hier_Id	INT 
@Commodity_Id			INT 
@Service_Month			DATE

OUTPUT PARAMETERS: 
Name	DataType	Default		Description 
---------------------------------------------------------------- 
USAGE EXAMPLES: 
-------------------------------------------------------------- 

EXEC DBO.Get_Currency_Unit_By_Client_Hier_Account_Commodity_Service_Month 278,1234,290,'2010-05-01'

AUTHOR INITIALS: 
Initials	Name 
------------------------------------------------------------ 
BCH			Balaraju

MODIFICATIONS 
Initials	Date		Modification 
------------------------------------------------------------ 
BCH			2012-05-17	created 
******/

CREATE PROCEDURE dbo.Get_Currency_Unit_By_Client_Hier_Account_Commodity_Service_Month
      ( 
       @Account_Id INT
      ,@Site_Client_Hier_Id INT
      ,@Commodity_Id INT
      ,@Service_Month DATE )
AS 
BEGIN 
      SET NOCOUNT ON ; 
      SELECT
            cuad.CURRENCY_UNIT_ID
      FROM
            dbo.Cost_Usage_Account_Dtl cuad
            JOIN dbo.Bucket_Master bm
                  ON cuad.Bucket_Master_Id = bm.Bucket_Master_Id
            JOIN dbo.Code cd
                  ON cd.code_id = bm.Bucket_Type_Cd
      WHERE
            cuad.ACCOUNT_ID = @Account_Id
            AND cuad.Client_hier_id = @Site_Client_Hier_Id
            AND bm.Commodity_Id = @Commodity_Id
            AND cuad.Service_Month = @Service_Month
            AND cd.Code_Value = 'Charge'
      GROUP BY
            cuad.CURRENCY_UNIT_ID 
END 
;
GO
GRANT EXECUTE ON  [dbo].[Get_Currency_Unit_By_Client_Hier_Account_Commodity_Service_Month] TO [CBMSApplication]
GO
