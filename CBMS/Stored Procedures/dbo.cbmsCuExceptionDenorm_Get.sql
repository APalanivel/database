SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

        
/******                  
        
NAME: [DBO].[cbmsCuExceptionDenorm_Get]          
             
DESCRIPTION:         
 to get the details from cu_exception_denorm tabke on the basis of Cu_invoce_id        
        
INPUT PARAMETERS:                  
NAME   DATATYPE DEFAULT  DESCRIPTION                  
------------------------------------------------------------                  
@cu_invoice_id  INT              
                        
OUTPUT PARAMETERS:                  
NAME   DATATYPE DEFAULT  DESCRIPTION           
               
------------------------------------------------------------                  
USAGE EXAMPLES:                  
------------------------------------------------------------                
 EXEC cbmsCuExceptionDenorm_Get 35237628        
         
AUTHOR INITIALS:        
INITIALS NAME        
------------------------------------------------------------        
SKA  Shobhit Kumar Agrawal        
RKV     Ravi Kumar Vegesna      
AP  ARUNKUMAR PALANIVEL      
        
MODIFICATION        
INITIALS DATE  MODIFICATION        
------------------------------------------------------------        
SKA   10/21/2010 Created        
SKA   11/16/2010 Moved the ch.Site_Id > 0 from where caluse to join clause as it is with Left Join        
RKV   2015-11-03  Added Commodity_id in the Client_Hier_Account join as part of AS400-PII        
RKV   2016-09-28  Added Recalc_failed_reason_type_cd,Recalc_response_error_Dsc as part of UATAS400CI-332    
AP   AUG 30, 2019 ADDED NEW COLUMN IN CU EXCEPTION DENORM TABLE. THE SAME HAS BEEN ADDED IN THIS PROCEDURE TO SHOW THE VALUE IN APPLICATION    
        
*/        
CREATE PROCEDURE [dbo].[cbmsCuExceptionDenorm_Get] ( 
@CU_Invoice_ID INT
, @Account_Id INT = NULL  
        , @Commodity_Id INT = NULL 
  )    
AS     
BEGIN        
        
      SELECT    
            cued.CBMS_IMAGE_ID    
           ,cued.CBMS_DOC_ID    
           ,cued.CU_INVOICE_ID    
           ,cued.QUEUE_ID    
           ,cued.EXCEPTION_TYPE    
           ,cued.EXCEPTION_STATUS_TYPE    
           ,CASE WHEN cued.account_id IS NOT NULL THEN cha.Account_Number    
                 ELSE cued.UBM_Account_Number    
            END AS Account_Number    
           ,cued.SERVICE_MONTH    
           ,CASE WHEN cued.Client_Hier_ID IS NOT NULL THEN ch.Client_Name    
                 ELSE cued.UBM_Client_Name    
            END AS Client_Name    
           ,CASE WHEN cued.Client_Hier_ID IS NOT NULL THEN ch.Site_name    
                 ELSE cued.UBM_Site_Name    
            END site_name    
           ,CASE WHEN cued.Client_Hier_ID IS NOT NULL THEN ch.State_Name    
                 ELSE cued.UBM_State_Name    
            END state_name    
           ,CASE WHEN cued.Client_Hier_ID IS NOT NULL THEN ch.City    
                 ELSE cued.UBM_City    
            END CITY    
           ,cha.Account_Vendor_Name AS vendor_name    
           ,cued.IS_MANUAL    
           ,cued.DATE_IN_QUEUE    
           ,cued.SORT_ORDER    
           ,cued.account_id    
           ,cued.commodity_id    
           ,cued.recalc_type_cd    
           ,cued.Recalc_failed_reason_type_cd    
           ,cued.Recalc_response_error_Dsc    
     ,CUED.Date_In_CBMS    
      FROM    
            dbo.CU_EXCEPTION_DENORM cued    
            LEFT JOIN Core.Client_Hier ch    
                  ON ch.Client_Hier_Id = cued.Client_Hier_ID    
                     AND ch.Site_Id > 0    
            LEFT JOIN Core.Client_Hier_Account cha    
                  ON cha.Account_Id = cued.Account_ID    
                     AND ch.Client_Hier_Id = cha.Client_Hier_Id    
                     AND cha.Commodity_Id = cued.Commodity_Id    
      WHERE    
            cu_invoice_id = @cu_invoice_id   
			 AND (   @Account_Id IS NULL  
                    OR  cued.Account_ID = @Account_Id)  
            AND (   @Commodity_Id IS NULL  
                    OR  cued.Commodity_Id = @Commodity_Id);     
                    
        
END;      
;    
;   
GO


GRANT EXECUTE ON  [dbo].[cbmsCuExceptionDenorm_Get] TO [CBMSApplication]
GO
