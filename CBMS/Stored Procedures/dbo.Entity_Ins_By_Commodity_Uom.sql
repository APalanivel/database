SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******        
  

NAME:        
dbo.Entity_Ins_By_Commodity_Uom       

DESCRIPTION:        
	Used to insert/update UOM conversion into COMMODITY_UOM_CONVERSION table      

INPUT PARAMETERS:        
Name				DataType Default Description        
------------------------------------------------------------        
	  @Base_Commodity_Name AS VARCHAR( 50 )
	, @Base_UOM_Value AS VARCHAR( 25 )
	, @Converted_Commodity_Name AS VARCHAR( 50 )
	, @Converted_UOM_Value AS VARCHAR( 25 )    
      
OUTPUT PARAMETERS:        
Name				DataType Default Description        
------------------------------------------------------------        
USAGE EXAMPLES:        
------------------------------------------------------------      

EXEC dbo.Entity_Ins_By_Commodity_Uom 'Electric Power',82,'Elecric Power',96


AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
GB   Geetansu Behera        
CMH  Chad Hattabaugh         
HG   Hari       
SKA Shobhit Kr Agrawal    

MODIFICATIONS         
Initials Date Modification        
------------------------------------------------------------        
GB    Created      
SKA  08/07/09 Modified    

GB  14/07/09 In Entity Table the Unit for Description is different for Different Commodity,   
			It does not follow standard pattern for electriv power, Natural gas and Alternative Natural Gas  
			Hence those are treated separately in the case  

    
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[Entity_Ins_By_Commodity_Uom]
	  @Base_Commodity_Name AS VARCHAR( 50 )
	, @Base_UOM_Value AS VARCHAR( 25 )
	, @Converted_Commodity_Name AS VARCHAR( 50 )
	, @Converted_UOM_Value AS VARCHAR( 25 )
AS 

BEGIN
    SET  NOCOUNT ON;
    
	BEGIN TRY
    BEGIN TRANSACTION
   
			DECLARE  @BaseUnitFor             AS VARCHAR( 200 )
				   , @ConvertedUnitFor        AS VARCHAR( 200 )
				   , @BaseUnitId              AS INT
				   , @ConvertedUnitId         AS INT
		    
			SELECT      @BaseUnitFor           = ( CASE @Base_Commodity_Name
													   WHEN 'Electric Power' THEN 'Unit for electricity'
													   WHEN 'Natural Gas' THEN 'Unit for Gas'
													   WHEN 'Alternative Natural Gas' THEN 'Unit for alternate fuel'
													   ELSE 'Units for ' + @Base_Commodity_Name
												   END )
					  , @ConvertedUnitFor      = ( CASE @Converted_Commodity_Name
													   WHEN 'Electric Power' THEN 'Unit for electricity'
													   WHEN 'Natural Gas' THEN 'Unit for Gas'
													   WHEN 'Alternative Natural Gas' THEN 'Unit for alternate fuel'
													   ELSE 'Units for ' + @Converted_Commodity_Name
												   END )
		    
			SELECT      @BaseUnitId = ENTITY_ID
			FROM        dbo.ENTITY
			WHERE       ENTITY_NAME = @Base_UOM_Value AND
						ENTITY_DESCRIPTION = @BaseUnitFor
		                
		--new entry in Entity if not found in entity to support existing application  
		                
			IF @BaseUnitId IS NULL
			BEGIN
				DECLARE  @Entity_Type AS INT = 0
		        
				SELECT      @Entity_Type
				FROM        ENTITY
				WHERE       ENTITY_DESCRIPTION = @BaseUnitFor
		        
				SELECT      @Entity_Type = ( MAX( ENTITY_TYPE ) + 1 )
				FROM        ENTITY
				WHERE       @Entity_Type = 0
		        
				INSERT INTO ENTITY(   ENTITY_NAME
									, ENTITY_TYPE
									, ENTITY_DESCRIPTION )
				VALUES  (   @Base_UOM_Value
						  , @Entity_Type
						  , @BaseUnitFor )
		        
				SELECT      @BaseUnitId = ENTITY_ID
				FROM        ENTITY
				WHERE       ENTITY_NAME = @Base_UOM_Value AND
							ENTITY_TYPE = @Entity_Type AND
							ENTITY_DESCRIPTION = @BaseUnitFor
			END
		    
			SELECT      @ConvertedUnitId = ENTITY_ID
			FROM        dbo.ENTITY
			WHERE       ENTITY_NAME = @Converted_UOM_Value AND
						ENTITY_DESCRIPTION = @ConvertedUnitFor
		                
		--new entry in Entity if not found in entity to support existing application  
		                
			IF @ConvertedUnitId IS NULL
			BEGIN
				DECLARE  @Entity_TypeC AS INT = 0
		        
				SELECT      @Entity_TypeC
				FROM        ENTITY
				WHERE       ENTITY_DESCRIPTION = @ConvertedUnitFor
		        
				SELECT      @Entity_TypeC = ( MAX( ENTITY_TYPE ) + 1 )
				FROM        ENTITY
				WHERE       @Entity_TypeC = 0
		        
				INSERT INTO ENTITY(   ENTITY_NAME
									, ENTITY_TYPE
									, ENTITY_DESCRIPTION )
				VALUES  (   @Converted_UOM_Value
						  , @Entity_TypeC
						  , @ConvertedUnitFor )
		        
				SELECT      @ConvertedUnitId = ENTITY_ID
				FROM        ENTITY
				WHERE       ENTITY_NAME = @Converted_UOM_Value AND
							ENTITY_TYPE = @Entity_TypeC AND
							ENTITY_DESCRIPTION = @ConvertedUnitFor
			END
			
			SELECT @BaseUnitId AS BaseUnitId, @ConvertedUnitId AS ConvertedUnitId
 
		COMMIT TRAN
		 END TRY
		 BEGIN CATCH
			 ROLLBACK TRAN
			 EXEC dbo.usp_RethrowError
		 END CATCH   
   
END
GO
GRANT EXECUTE ON  [dbo].[Entity_Ins_By_Commodity_Uom] TO [CBMSApplication]
GO
