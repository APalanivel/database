SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[cbmsPricePointValue_GetForMonth]
	( @MyAccountId int
	, @market_index_id int = null
	, @price_point_id int = null
	, @close_month datetime
	)
AS
BEGIN

	   select pv.price_index_value_id price_point_value_id
		, pr.index_id 		market_index_id
		, mi.entity_name 	market_index
		, pr.price_index_id 	price_point_id
		, pr.pricing_point 	price_point
		, isNull(pv.index_month, @close_month)	close_month
		, pv.index_value	close_price
	     from price_index pr 
	     join entity mi on mi.entity_id = pr.index_id
  left outer join price_index_value pv on pv.price_index_id = pr.price_index_id and pv.index_month = @close_month
	    where pr.price_index_id = isNull(@price_point_id, pr.price_index_id)
	      and pr.index_id = isNull(@market_index_id, pr.index_id)
	 order by mi.entity_name asc
		, pr.pricing_point 

END
GO
GRANT EXECUTE ON  [dbo].[cbmsPricePointValue_GetForMonth] TO [CBMSApplication]
GO
