SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******             
             
 NAME: [dbo].[User_Group_Info_Sel_By_Client_Group]          

 DESCRIPTION:
      To get the users for a given Group_Info_Id and Client.            
     
 INPUT PARAMETERS:

 Name                               DataType       Default          Description
---------------------------------------------------------------------------------------------------------------
 @Group_Info_Id                     INT
 @Client_Id							INT        

 OUTPUT PARAMETERS: 
 Name                               DataType       Default          Description              
---------------------------------------------------------------------------------------------------------------            

 USAGE EXAMPLES:                  
---------------------------------------------------------------------------------------------------------------             

	EXEC dbo.User_Group_Info_Sel_By_Client_Group 215,218
	EXEC dbo.User_Group_Info_Sel_By_Client_Group 277,218
	EXEC dbo.User_Group_Info_Sel_By_Client_Group 217,218
	EXEC dbo.User_Group_Info_Sel_By_Client_Group 227,218

 AUTHOR INITIALS:              
            
 Initials               Name              
---------------------------------------------------------------------------------------------------------------            
 NR                     Narayana Reddy                
               
 MODIFICATIONS:              
             
 Initials               Date            Modification            
---------------------------------------------------------------------------------------------------------------            
 NR                     2013-12-26      Created for RA Admin user management            
             
******/      
CREATE PROCEDURE dbo.User_Group_Info_Sel_By_Client_Group
      ( 
       @Group_Info_Id INT
      ,@Client_Id INT )
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE @Users TABLE
            ( 
             User_Info_Id INT
            ,UserName VARCHAR(30)
            ,First_Name VARCHAR(40)
            ,Middle_Name VARCHAR(40)
            ,Last_Name VARCHAR(40)
            ,Email_Address VARCHAR(150)
            ,Is_Role_Based BIT )

      INSERT      INTO @Users
                  ( 
                   User_Info_Id
                  ,UserName
                  ,First_Name
                  ,Middle_Name
                  ,Last_Name
                  ,Email_Address
                  ,Is_Role_Based )
                  SELECT
                        ui.User_Info_Id
                       ,ui.UserName
                       ,ui.First_Name
                       ,ui.Middle_Name
                       ,ui.Last_Name
                       ,ui.EMAIL_ADDRESS
                       ,0 AS Is_Role_Based
                  FROM
                        dbo.USER_INFO_GROUP_INFO_MAP gm
                        INNER JOIN dbo.User_Info ui
                              ON gm.User_Info_Id = ui.User_Info_Id
                  WHERE
                        ui.Client_Id = @Client_id
                        AND gm.GROUP_INFO_ID = @Group_Info_Id
                        AND ui.IS_HISTORY = 0
                        AND ui.IS_DEMO_USER = 0

      INSERT      INTO @Users
                  ( 
                   User_Info_Id
                  ,UserName
                  ,First_Name
                  ,Middle_Name
                  ,Last_Name
                  ,Email_Address
                  ,Is_Role_Based )
                  SELECT
                        ui.User_Info_id
                       ,ui.username
                       ,ui.first_name
                       ,ui.middle_name
                       ,ui.last_name
                       ,ui.EMAIL_ADDRESS
                       ,1 AS Is_Role_Based
                  FROM
                        dbo.Client_App_Access_Role_Group_Info_Map gim
                        INNER JOIN dbo.Client_App_Access_Role ar
                              ON gim.Client_App_Access_Role_Id = ar.Client_App_Access_Role_Id
                        INNER JOIN dbo.User_Info_Client_App_Access_Role_Map urm
                              ON ar.Client_App_Access_Role_Id = urm.Client_App_Access_Role_Id
                        INNER JOIN dbo.User_Info ui
                              ON urm.User_Info_Id = ui.User_Info_Id
                  WHERE
                        ar.Client_Id = @Client_id
                        AND gim.GROUP_INFO_ID = @Group_Info_Id
                        AND ui.IS_HISTORY = 0
                        AND ui.IS_DEMO_USER = 0
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          @Users usr
                                         WHERE
                                          usr.User_Info_Id = urm.User_Info_Id )
                  GROUP BY
                        ui.User_Info_id
                       ,ui.username
                       ,ui.first_name
                       ,ui.middle_name
                       ,ui.last_name
                       ,ui.EMAIL_ADDRESS                                       
      SELECT
            User_Info_Id
           ,UserName
           ,First_Name
           ,Middle_Name
           ,Last_Name
           ,First_Name + space(1) + Last_Name AS Full_Name
           ,Email_Address
           ,@Group_Info_Id AS Group_Info_Id
           ,Is_Role_Based
      FROM
            @Users

END;
;
GO
GRANT EXECUTE ON  [dbo].[User_Group_Info_Sel_By_Client_Group] TO [CBMSApplication]
GO
