SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE     PROCEDURE dbo.DELETE_ALL_FORECAST_VOLUME_DETAILS_P

@clientId int,
@siteId int

AS
begin
set nocount on
delete  rm_forecast_volume_details  
where
	site_id = @siteId
	and rm_forecast_volume_id in (select rm_forecast_volume_id from rm_forecast_volume(nolock) 
				where  client_id = @clientId)
end
GO
GRANT EXECUTE ON  [dbo].[DELETE_ALL_FORECAST_VOLUME_DETAILS_P] TO [CBMSApplication]
GO
