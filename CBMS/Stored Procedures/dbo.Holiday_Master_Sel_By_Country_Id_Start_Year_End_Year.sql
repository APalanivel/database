SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********  
NAME:  dbo.Holiday_Master_Sel_By_Country_Id_Start_Year_End_Year  
  
DESCRIPTION:    
 Used to select Holiday Master Deatils between given dates.  
 It also returns a flag Term_Exists which will determine whether atleast one term exists with the holiday.  
  
INPUT PARAMETERS:  
      Name              DataType          Default     Description  
------------------------------------------------------------  
   @Country_Id  INT  
   @Start_Year  INT   
      @End_Year   INT   
  
OUTPUT PARAMETERS:  
      Name              DataType          Default     Description  
------------------------------------------------------------  
  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
  
 Exec dbo.Holiday_Master_Sel_By_Country_Id_Start_Year_End_Year 4,2011,2012  
  
  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 BCH  Balaraju  
  
  
 Initials Date  Modification  
------------------------------------------------------------  
 BCH  2012-07-10  Created  
  
******/  
CREATE PROCEDURE dbo.Holiday_Master_Sel_By_Country_Id_Start_Year_End_Year
( 
 @Country_Id INT
,@Start_Year INT
,@End_Year INT )
AS 
BEGIN  
      SET NOCOUNT ON ;  
  
      CREATE TABLE #Holidays
      ( 
       Holiday_Master_Id INT
      ,Year_Identifier INT
      ,Holiday_Name NVARCHAR(100)
      ,Holiday_Dt DATE )  
  
   -- Insert the holidays added to the country within the time frame  
      INSERT
            #Holidays
            ( 
             Holiday_Master_Id
            ,Year_Identifier
            ,Holiday_Name
            ,Holiday_Dt )
            SELECT
                  Holiday_Master_Id
                 ,Year_Identifier
                 ,Holiday_Name
                 ,Holiday_Dt
            FROM
                  dbo.Holiday_Master HM
            WHERE
                  COUNTRY_ID = @Country_Id
                  AND Year_Identifier BETWEEN @Start_Year AND @End_Year   
  
	-- Insert the holidays added to the country outside the time frame  
      INSERT
            #Holidays
            ( 
             Holiday_Master_Id
            ,Year_Identifier
            ,Holiday_Name
            ,Holiday_Dt )
            SELECT
                  NULL
                 ,@Start_Year
                 ,HM.Holiday_Name
                 ,NULL
            FROM
                  dbo.Holiday_Master HM
            WHERE
                  NOT EXISTS ( SELECT
                                    1
                               FROM
                                    #Holidays
                               WHERE
                                    Holiday_Name = HM.Holiday_Name )
                  AND HM.COUNTRY_ID = @Country_Id
            GROUP BY
                  HM.Holiday_Name ;  
      WITH  CTE_TermExists
              AS ( SELECT
                        Hol.Holiday_Name
                   FROM
                        ( SELECT
                              Holiday_Name
                          FROM
                              #Holidays
                          GROUP BY
                              Holiday_Name ) Hol
                        JOIN dbo.Holiday_Master HM
                              ON Hol.Holiday_Name = HM.Holiday_Name
                                 AND HM.COUNTRY_ID = @Country_Id
                        JOIN dbo.Time_Of_Use_Schedule_Term_Holiday TOUSTH
                              ON HM.Holiday_Master_Id = TOUSTH.Holiday_Master_Id
                   GROUP BY
                        Hol.Holiday_Name),
            CTE_Holiday_TimeFrame
              AS ( SELECT
                        HM.Holiday_Name
                       ,MIN(HM.Year_Identifier) AS Min_Year
                       ,MAX(HM.Year_Identifier) AS Max_Year
                   FROM
                        dbo.Holiday_Master HM
                        JOIN ( SELECT
                                    Holiday_Name
                               FROM
                                    #Holidays
                               GROUP BY
                                    Holiday_Name ) Hol
                              ON HM.Holiday_Name = Hol.Holiday_Name
                                 AND HM.COUNTRY_ID = @Country_Id
                   GROUP BY
                        HM.Holiday_Name)
            SELECT
                  Hol.Holiday_Master_Id
                 ,Hol.Year_Identifier
                 ,Hol.Holiday_Name
                 ,Hol.Holiday_Dt
                 ,@Country_Id AS Country_Id
                 ,CASE WHEN CTET.Holiday_Name IS NOT NULL THEN 1
                       ELSE 0
                  END AS Term_Exists
                 ,CASE WHEN @Start_Year <= CTEHTF.Min_Year
                            AND @End_Year >= CTEHTF.Max_Year THEN 1
                       ELSE 0
                  END AS Check_For_Null_Dates
                 ,CASE WHEN tousth.Holiday_Master_Id IS NOT NULL THEN 1
                       ELSE 0
                  END AS Holiday_In_Use
            FROM
                  #Holidays Hol
                  JOIN CTE_Holiday_TimeFrame CTEHTF
                        ON Hol.Holiday_Name = CTEHTF.Holiday_Name
                  LEFT JOIN CTE_TermExists CTET
                        ON Hol.Holiday_Name = CTET.Holiday_Name
                  LEFT JOIN dbo.Time_Of_Use_Schedule_Term_Holiday TOUSTH
                        ON TOUSTH.Holiday_Master_Id = Hol.Holiday_Master_Id
            ORDER BY
                  Holiday_Dt  
END 

;
GO
GRANT EXECUTE ON  [dbo].[Holiday_Master_Sel_By_Country_Id_Start_Year_End_Year] TO [CBMSApplication]
GO
