SET NUMERIC_ROUNDABORT OFF 
GO

SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: dbo.cbmsSsoProjectOwner_GetForProjectFlat
     
DESCRIPTION: 
	
      
INPUT PARAMETERS:          
	NAME				DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
    @sso_project_id		INT
               
OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        
	EXEC dbo.cbmsSsoProjectOwner_GetForProjectFlat 15
	EXEC dbo.cbmsSsoProjectOwner_GetForProjectFlat 86
	EXEC dbo.cbmsSsoProjectOwner_GetForProjectFlat 16078  
	EXEC dbo.cbmsSsoProjectOwner_GetForProjectFlat 13461  
	EXEC dbo.cbmsSsoProjectOwner_GetForProjectFlat 21497
	
AUTHOR INITIALS:          
	INITIALS	NAME          
------------------------------------------------------------          
	CPE			Chaitanya Panduga Eshwar
	NR			Narayana Reddy
          
MODIFICATIONS:           
	INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
	CPE			03/28/2011	Modified the SP to capture Owner Id and Owner Type from Client Hier Id
	NR			2014-11-24	Added the Site_Reference_Number column in output list.
  
******/

CREATE PROCEDURE [dbo].[cbmsSsoProjectOwner_GetForProjectFlat] ( @sso_project_id INT )
AS 
BEGIN

      SELECT
            CASE CD.Code_Value
              WHEN 'Corporate' THEN CH.Client_Id
              WHEN 'Division' THEN CH.Sitegroup_Id
              WHEN 'Site' THEN CH.Site_Id
            END AS owner_id
           ,CASE CD.Code_Value
              WHEN 'Corporate' THEN CH.Client_Name
              WHEN 'Division' THEN CH.Sitegroup_Name
              WHEN 'Site' THEN RTRIM(CH.City) + ', ' + CH.State_Name + ' (' + CH.Site_name + ')'
            END AS owner_name
           ,Ent.ENTITY_ID AS sso_owner_type_id
           ,Ent.ENTITY_NAME AS owner_type
           ,CH.Client_Id
           ,NULLIF(CH.Sitegroup_Id, 0) AS division_id
           ,NULLIF(CH.Site_Id, 0) AS Site_Id
           ,CD.Display_Seq AS owner_sort
           ,CH.Client_Name
           ,ch.Site_Reference_Number
      FROM
            dbo.SSO_PROJECT_OWNER_MAP SPOM
            JOIN Core.Client_Hier CH
                  ON SPOM.Client_Hier_Id = CH.Client_Hier_Id
            JOIN dbo.Code CD
                  ON CH.Hier_level_Cd = CD.Code_Id
            JOIN dbo.ENTITY Ent
                  ON CD.Code_Dsc = Ent.ENTITY_NAME
            JOIN dbo.Codeset CS
                  ON CD.Codeset_Id = CS.Codeset_Id
                     AND Ent.ENTITY_DESCRIPTION = 'SSO Owner Type'
                     AND CS.Codeset_Name = 'HierLevel'
      WHERE
            SPOM.SSO_PROJECT_ID = @sso_project_id
      ORDER BY
            owner_sort
           ,owner_name

END



;
GO

GRANT EXECUTE ON  [dbo].[cbmsSsoProjectOwner_GetForProjectFlat] TO [CBMSApplication]
GO
