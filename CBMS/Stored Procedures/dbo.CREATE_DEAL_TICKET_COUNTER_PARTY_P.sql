SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.CREATE_DEAL_TICKET_COUNTER_PARTY_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@dealTicketId  	int       	          	
	@counterpartyID	int       	          	
	@counterpartyContactName	varchar(200)	          	
	@counterpartyContactPhone	varchar(200)	          	
	@counterpartyContactCell	varchar(200)	          	
	@counterpartyContactFax	varchar(200)	          	
	@counterpartyContactEmail	varchar(1000)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE        PROCEDURE DBO.CREATE_DEAL_TICKET_COUNTER_PARTY_P 

@dealTicketId int,
@counterpartyID int,
@counterpartyContactName varchar(200), 
@counterpartyContactPhone varchar(200), 
@counterpartyContactCell varchar(200), 
@counterpartyContactFax varchar(200), 
@counterpartyContactEmail varchar(1000) 


AS
set nocount on
	INSERT INTO RM_DEAL_TICKET_COUNTER_PARTY 
	(RM_DEAL_TICKET_ID,
	RM_COUNTERPARTY_ID,
	COUNTERPARTY_CONTACT_NAME,
	COUNTERPARTY_CONTACT_PHONE,
	COUNTERPARTY_CONTACT_CELL,
	COUNTERPARTY_CONTACT_FAX,
	COUNTERPARTY_CONTACT_EMAIL) 
	
	VALUES 
	(@dealTicketId, 
	@counterpartyID, 
	@counterpartyContactName,
	@counterpartyContactPhone,
	@counterpartyContactCell, 
	@counterpartyContactFax, 
	@counterpartyContactEmail)
GO
GRANT EXECUTE ON  [dbo].[CREATE_DEAL_TICKET_COUNTER_PARTY_P] TO [CBMSApplication]
GO
