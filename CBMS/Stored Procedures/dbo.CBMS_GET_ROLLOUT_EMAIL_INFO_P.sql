
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******

NAME:
	cbms.dbo.CBMS_GET_ROLLOUT_EMAIL_INFO_P

DESCRIPTION:

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@site_id       	INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.CBMS_GET_ROLLOUT_EMAIL_INFO_P 258269
	EXEC dbo.CBMS_GET_ROLLOUT_EMAIL_INFO_P_123 258269

	EXEC dbo.CBMS_GET_ROLLOUT_EMAIL_INFO_P 391
	EXEC dbo.CBMS_GET_ROLLOUT_EMAIL_INFO_P_123 391

	EXEC dbo.CBMS_GET_ROLLOUT_EMAIL_INFO_P 128
	EXEC dbo.CBMS_GET_ROLLOUT_EMAIL_INFO_P_123 128

	EXEC dbo.CBMS_GET_ROLLOUT_EMAIL_INFO_P 24675
	EXEC dbo.CBMS_GET_ROLLOUT_EMAIL_INFO_P_123 24675

	EXEC dbo.CBMS_GET_ROLLOUT_EMAIL_INFO_P 293056
	EXEC dbo.CBMS_GET_ROLLOUT_EMAIL_INFO_P_123 293056


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath
	RR			Raghu Reddy
	SP			Sandeep Pigilam           
	

MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script
	PNR			20/12/2010	For the value of @emailid variable changed arguement value for first_name and last_name.
	PNR			02/10/2011	Modified logic at case statement as cnt.country_name = 'Canada' instead of stat.state_name.Bug fixing of #21657.
							Replaced the Utility_Analyst_Map view with the base tables
							Site, Account, State and Country tables replaced by Client_hier and Client_hier_account
	PNR			02/17/2011	Replaced 'Terris Lewis' for @emailId variable with CanadaGasRollout<CanadaGasRollout@summitenergy.com>.
	RR			07/15/2011	MAINT-722 fixes the code to return the results irrespective of any analysts mapped to the vendor of site
								-- Modilfied the inner join with the table Vendor_Commodity_Analyst_Map and Utility_Detail_Analyst_Map as left outer join
									Hardcoded variable @emailId changed to select the value from App_Config table, the respective value saved in the
									table as app config code is "Cbms_Rollout_Email_Address"
	SP			2014-09-09  For Data Transition used a table variable @Group_Legacy_Name to handle the hardcoded group names  
	RR			2015-08-06	Global Sourcing - Modified script to get analysts details based on site analyst type selected


******/
  
CREATE PROCEDURE [dbo].[CBMS_GET_ROLLOUT_EMAIL_INFO_P] @Site_Id INT
AS 
BEGIN

      SET NOCOUNT ON;

      DECLARE
            @emailId VARCHAR(255)
           ,@Default_Analyst INT
           ,@Custom_Analyst INT
           ,@Analyst_Mapping_Cd INT
      
      SELECT
            @Default_Analyst = max(case WHEN c.Code_Value = 'Default' THEN c.Code_Id
                                   END)
           ,@Custom_Analyst = max(case WHEN c.Code_Value = 'Custom' THEN c.Code_Id
                                  END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Analyst Type'
            
      DECLARE @Group_Legacy_Name TABLE
            ( 
             GROUP_INFO_ID INT
            ,GROUP_NAME VARCHAR(200)
            ,Legacy_Group_Name VARCHAR(200) )

      SELECT
            @emailId = App_Config_Value
      FROM
            dbo.App_Config
      WHERE
            App_Config_Cd = 'Canada_Cbms_Rollout_Email_Address'
            AND Is_Active = 1 
            
      INSERT      INTO @Group_Legacy_Name
                  ( 
                   GROUP_INFO_ID
                  ,GROUP_NAME
                  ,Legacy_Group_Name )
                  EXEC dbo.Group_Info_Sel_By_Group_Legacy 
                        @Group_Legacy_Name_Cd_Value = 'Regulated_Market';


      WITH  Cte_Analysts
              AS ( SELECT
                        ch.State_name
                       ,region_manager.first_name + space(1) + region_manager.last_name + '<' + region_manager.email_address + '>' AS rm_email_address
                       ,min(case WHEN com.Commodity_Name = 'Natural Gas' THEN case WHEN coalesce(cha.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) = @Custom_Analyst THEN coalesce(aca.Analyst_User_Info_Id, sca.Analyst_User_Info_Id, cca.Analyst_User_Info_Id)
                                                                                   WHEN coalesce(cha.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) = @Default_Analyst THEN vcam.Analyst_Id
                                                                              END
                            END) AS gas_analyst_id
                       ,min(case WHEN com.Commodity_Name = 'Electric Power' THEN case WHEN coalesce(cha.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) = @Custom_Analyst THEN coalesce(aca.Analyst_User_Info_Id, sca.Analyst_User_Info_Id, cca.Analyst_User_Info_Id)
                                                                                      WHEN coalesce(cha.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) = @Default_Analyst THEN vcam.Analyst_Id
                                                                                 END
                            END) AS power_analyst_id
                       ,case WHEN ( ch.Country_Name = 'Canada'
                                    AND @emailId != '' ) THEN @emailId
                             ELSE NULL
                        END cc_always
                       ,udam.Analyst_ID AS regulated_Analyst_id
                       ,cha.Account_Vendor_id
                       ,min(coalesce(cha.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd)) AS Analyst_Mapping_Cd
                   FROM
                        core.Client_Hier ch
                        INNER JOIN core.Client_Hier_Account cha
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.region_manager_map rm_map
                              ON ch.Region_id = rm_map.region_id
                        INNER JOIN dbo.user_info region_manager
                              ON rm_map.user_info_id = region_manager.user_info_id
				---- Gas Analyst / Power Analyst
                        INNER JOIN dbo.Vendor_Commodity_Map vcm
                              ON cha.Account_Vendor_Id = vcm.Vendor_Id
                        INNER JOIN dbo.Commodity com
                              ON vcm.COMMODITY_TYPE_ID = com.Commodity_Id
                        INNER JOIN dbo.Utility_Detail detail
                              ON detail.vendor_id = vcm.vendor_id
                        LEFT OUTER JOIN ( dbo.Utility_Detail_Analyst_Map udam
                                          JOIN dbo.GROUP_INFO gi
                                                ON gi.GROUP_INFO_ID = udam.Group_Info_ID
                                          INNER JOIN @Group_Legacy_Name gil
                                                ON gi.Group_Info_ID = gil.GROUP_INFO_ID )
                                          ON udam.Utility_Detail_ID = detail.UTILITY_DETAIL_ID
                        LEFT OUTER JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                              ON vcam.Vendor_Commodity_Map_Id = vcm.VENDOR_COMMODITY_MAP_ID
                        INNER JOIN core.Client_Commodity ccc
                              ON ccc.Client_Id = ch.Client_Id
                                 AND ccc.Commodity_Id = vcm.COMMODITY_TYPE_ID
                        LEFT JOIN dbo.Account_Commodity_Analyst aca
                              ON aca.Account_Id = cha.Account_Id
                                 AND aca.Commodity_Id = vcm.COMMODITY_TYPE_ID
                        LEFT JOIN dbo.SITE_Commodity_Analyst sca
                              ON sca.Site_Id = ch.Site_Id
                                 AND sca.Commodity_Id = vcm.COMMODITY_TYPE_ID
                        LEFT JOIN dbo.Client_Commodity_Analyst cca
                              ON ccc.Client_Commodity_Id = cca.Client_Commodity_Id
                   WHERE
                        ch.Site_id = @Site_Id
                        AND cha.Account_type = 'Utility'
                   GROUP BY
                        ch.state_name
                       ,region_manager.first_name + space(1) + region_manager.last_name + '<' + region_manager.email_address + '>'
                       ,case WHEN ( ch.COUNTRY_NAME = 'Canada'
                                    AND @emailId != '' ) THEN @emailId
                             ELSE NULL
                        END
                       ,udam.Analyst_ID
                       ,cha.Account_Vendor_id
                       ,cha.Account_Id)
            SELECT
                  cte.state_name
                 ,rate_analyst.first_name + space(1) + rate_analyst.last_name + '<' + rate_analyst.email_address + '>' AS ra_email_address
                 ,gas_analyst.first_name + space(1) + gas_analyst.last_name + '<' + gas_analyst.email_address + '>' AS gas_sa_email_address
                 ,power_analyst.first_name + space(1) + power_analyst.last_name + '<' + power_analyst.email_address + '>' AS power_sa_email_address
                 ,cte.rm_email_address
                 ,gas_analyst_manager.first_name + space(1) + gas_analyst_manager.last_name + '<' + gas_analyst_manager.email_address + '>' AS gas_sm_email_address
                 ,power_analyst_manager.first_name + space(1) + power_analyst_manager.last_name + '<' + power_analyst_manager.email_address + '>' AS power_sm_email_address
                 ,cte.cc_always
                 ,Analyst_Mapping_Cd
            FROM
                  Cte_Analysts cte
                  LEFT JOIN dbo.user_info gas_analyst
                        ON gas_analyst.user_info_id = cte.gas_analyst_id
                  LEFT JOIN dbo.sr_sa_sm_map gas_analyst_map
                        ON gas_analyst_map.sourcing_analyst_id = gas_analyst.user_info_id
                  LEFT JOIN dbo.user_info gas_analyst_manager
                        ON gas_analyst_manager.user_info_id = gas_analyst_map.sourcing_manager_id
                  LEFT JOIN dbo.user_info power_analyst
                        ON power_analyst.user_info_id = cte.power_analyst_id
                  LEFT JOIN dbo.sr_sa_sm_map power_analyst_map
                        ON power_analyst_map.sourcing_analyst_id = power_analyst.user_info_id
                  LEFT JOIN dbo.user_info power_analyst_manager
                        ON power_analyst_manager.user_info_id = power_analyst_map.sourcing_manager_id
                  LEFT JOIN dbo.user_info rate_analyst
                        ON rate_analyst.user_info_id = cte.regulated_Analyst_id

END;

;
GO



GRANT EXECUTE ON  [dbo].[CBMS_GET_ROLLOUT_EMAIL_INFO_P] TO [CBMSApplication]
GO
