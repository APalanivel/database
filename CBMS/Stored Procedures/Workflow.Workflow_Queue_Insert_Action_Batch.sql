SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:    [Workflow].[Workflow_Queue_Insert_Action_Batch]  
DESCRIPTION:   
------------------------------------------------------------     
 INPUT PARAMETERS:      
 Name   DataType  Default Description     
 @Workflow_Queue_Action_Id INT,    
@Lookup_Value NVARCHAR(MAX)=NULL,    
@Requested_User_Id INT,    
@Invoice_Ids NVARCHAR(MAX)=NULL,    
@Route_User_Id INT =NULL,    
@Route_Exception_Type_Id INT=NULL     
------------------------------------------------------------      
 OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
 USAGE EXAMPLES:      
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
TRK   Ramakrishna Thuammala Summit Energy   
   
 MODIFICATIONS       
 Initials Date   Modification      
------------------------------------------------------------      
 TRK    11/09/2019 Created  
  
******/    
CREATE PROC [Workflow].[Workflow_Queue_Insert_Action_Batch]    
(         
 @MODULE_ID INT,  
    @Workflow_Queue_Saved_Filter_Query_Id INT = NULL,  
    @Logged_In_User INT,  
    @Queue_id VARCHAR(MAX) = NULL,     ---  User selected from drop down Id 49,1,2,3            
    @Exception_Type VARCHAR(MAX) = NULL,  
    @Account_Number VARCHAR(500) = NULL,  
    @Client VARCHAR(500) = NULL,  
    @Site VARCHAR(500) = NULL,  
    @Country VARCHAR(500) = NULL,  
    @State VARCHAR(500) = NULL,  
    @City VARCHAR(500) = NULL,  
    @Commodity VARCHAR(500) = NULL,  
    @Invoice_ID VARCHAR(500) = NULL,  
    @Priority INT = 0,                 --- Yes            
    @Exception_Status INT = NULL,  
    @Comments VARCHAR(500) = NULL,  
    @Start_Date_in_Queue DATETIME = NULL,  
    @End_Date_in_Queue DATETIME = NULL,  
    @Start_Date_in_CBMS DATETIME = NULL,  
    @End_Date_in_CBMS DATETIME = NULL,  
    @Data_Source INT = NULL,  
    @Vendor VARCHAR(100) = NULL,  
    @Vendor_Type VARCHAR(100) = NULL,  
    @Month DATETIME = NULL,  
    @Filename VARCHAR(100) = NULL,  
    @invoice_List VARCHAR(MAX) = NULL, ---- if new invoice needs to be added send those invoices   
    @User_Group_Id_List VARCHAR(MAX),  
    @Invoice_List_TVP Invoice_List READONLY,  
 @Workflow_Queue_Action_Id INT,    
 @Lookup_Value NVARCHAR(MAX)=NULL,    
 @JsocnlookUp NVARCHAR(MAX)=NULL  
)    
AS    
BEGIN    
 DECLARE @Proc_name VARCHAR(100) = 'Workflow_Queue_Insert_Action_Batch',  
            @Input_Params VARCHAR(1000),  
            @Error_Line INT,  
            @Error_Message VARCHAR(3000),  
            @Workflow_Queue_Search_Filter_Group_Id INT;  
  
    DECLARE @invoice_table TABLE  
    (  
        Invoice_id INT  
    );  
  
    SELECT @Input_Params  
        = '@User ID:' + CAST(@Logged_In_User AS VARCHAR) + '@Workflow_Queue_Saved_Filter_Query_id:'  
          + CAST(@Workflow_Queue_Saved_Filter_Query_Id AS VARCHAR) + '@invoice_List:' + ISNULL(@invoice_List, '');  
    SET NOCOUNT ON;  
   
    
DECLARE @Status_Cd INT    
DECLARE @Cu_Invoice_Workflow_Action_Batch_Id INT    
    
SELECT @Status_Cd= MAX(Code_Id) from dbo.Code where Code_Dsc ='pending'    
BEGIN TRANSACTION       
BEGIN TRY      
  
		INSERT @invoice_table  
        EXEC [Workflow].[Get_List_Of_Invoices] @MODULE_ID,  
                                               @Workflow_Queue_Saved_Filter_Query_Id,  
                                               @Logged_In_User,  
                                               @Queue_id,  
                                               @Exception_Type,  
                                               @Account_Number,  
                                               @Client,  
                                               @Site,  
                                               @Country,  
                                               @State,      
                                               @City,      
											   @Commodity, 
                                               @Invoice_ID,
                                               @Priority,  
                                               @Exception_Status,  
                                               NULL,  
                                               @Start_Date_in_Queue,  
                                               @End_Date_in_Queue,  
                                               @Start_Date_in_CBMS,  
                                               @End_Date_in_CBMS,  
                                               @Data_Source,  
                                               @Vendor,  
                                               @Vendor_Type,  
                                               @Month,  
                                               @Filename,  
                                               @invoice_List,  
                                               @User_Group_Id_List,  
                                               @Invoice_List_TVP;  
  
  
  
  
  INSERT INTO [Workflow].[Cu_Invoice_Workflow_Action_Batch]    
       ([Workflow_Queue_Action_Id]    
       ,[Lookup_Value]    
       ,[Requested_User_Id]    
       ,[Requested_Ts]    
       ,[Status_Cd]    
      )    
    VALUES    
       (  
     @Workflow_Queue_Action_Id    
       ,@Lookup_Value    
       ,@Logged_In_User    
       ,GETDATE()    
       ,@Status_Cd    
       )    
    
        SET @Cu_Invoice_Workflow_Action_Batch_Id=@@IDENTITY    
    
        
         INSERT INTO [Workflow].[Cu_Invoice_Workflow_Action_Batch_Dtl]    
           ([Cu_Invoice_Workflow_Action_Batch_Id]    
           ,[Cu_Invoice_Id]    
           ,[Status_Cd]    
           )    
  SELECT    
  DISTINCT    
            @Cu_Invoice_Workflow_Action_Batch_Id    
           ,CAST(Invoice_id AS INT)    
           ,@Status_Cd    
            FROM @invoice_table   
              
  SELECT @Cu_Invoice_Workflow_Action_Batch_Id as 'queueId'    
  COMMIT       
END TRY      
BEGIN CATCH      
-- Entry made to the logging SP to capture the errors.      
        SELECT @Error_Line = ERROR_LINE(),  
               @Error_Message = ERROR_MESSAGE();  
  
        INSERT INTO StoredProc_Error_Log  
        (  
            StoredProc_Name,  
            Error_Line,  
            Error_message,  
            Input_Params  
        )  
        VALUES  
        (@Proc_name, @Error_Line, @Error_Message, @Input_Params);  
  
        EXEC [dbo].[usp_RethrowError] @Error_Message;  
  
ROLLBACK       
END CATCH      
    
    
END               
GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_Insert_Action_Batch] TO [CBMSApplication]
GO
