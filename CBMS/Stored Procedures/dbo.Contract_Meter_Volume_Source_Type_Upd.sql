SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 dbo.Contract_Meter_Volume_Source_Type_Upd  
  
DESCRIPTION:  
  
  
INPUT PARAMETERS:  
	Name								DataType	Default		Description  
------------------------------------------------------------------------------------  
	@Contract_Id						INT                     
	@Contract_Meter_Volume_Source_Cd	INT
	@Contract_Meter_Volume_Type_Cd		INT
  
OUTPUT PARAMETERS:  
	Name								DataType	Default		Description  
------------------------------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------------------------------  
  
	BEGIN TRANSACTION  
		EXEC Contract_Meter_Volume_Source_Type_Upd 
	ROLLBACK TRANSACTION  
  
AUTHOR INITIALS:  
	Initials	Name  
------------------------------------------------------------------------------------  
    RR			Raghu Reddy
     
MODIFICATIONS  
	Initials	Date		Modification  
------------------------------------------------------------------------------------  
	RR			2020-04-17	GRM - Created
******/
CREATE PROCEDURE [dbo].[Contract_Meter_Volume_Source_Type_Upd]
    (
        @Contract_Id INT
        , @Contract_Meter_Volume_Source_Cd INT = NULL
        , @Contract_Meter_Volume_Type_Cd INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        UPDATE
            dbo.CONTRACT
        SET
            Contract_Meter_Volume_Source_Cd = ISNULL(@Contract_Meter_Volume_Source_Cd, Contract_Meter_Volume_Source_Cd)
            , Contract_Meter_Volume_Type_Cd = ISNULL(@Contract_Meter_Volume_Type_Cd, Contract_Meter_Volume_Type_Cd)
        WHERE
            CONTRACT_ID = @Contract_Id;


    END;
GO
GRANT EXECUTE ON  [dbo].[Contract_Meter_Volume_Source_Type_Upd] TO [CBMSApplication]
GO
