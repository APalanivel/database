SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[GET_IM_IMAGE_DETAILS_P]
	@startImageId INT
	, @endImageId INT
AS
BEGIN
  
	SET NOCOUNT ON
  
	SELECT CBMS_image_id, CBMS_image_type_id, CBMS_doc_id, content_type
		, CBMS_Image_Location_Id, CBMS_Image_Directory, CBMS_Image_FileName
		, CBMS_image
	FROM dbo.CBMS_image_archive
	WHERE CBMS_image_id >= @startImageId
		AND CBMS_image_id < @endImageId

END
GO
GRANT EXECUTE ON  [dbo].[GET_IM_IMAGE_DETAILS_P] TO [CBMSApplication]
GO
