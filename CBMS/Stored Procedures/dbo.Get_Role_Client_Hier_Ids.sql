SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/******************************************************************************************************      
NAME : dbo.Get_Role_Client_Hier_Ids    
     
DESCRIPTION:   

This Procedure is used to get the client hier ids for all the roles and user site groups that have been effected since last run.
     
 INPUT PARAMETERS:      
 Name             DataType               Default        Description      
--------------------------------------------------------------------   
   
      
 OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
--------------------------------------------------------------------      
 LastRanTs	DATETIME
  
  USAGE EXAMPLES:      
--------------------------------------------------------------------      
     
    EXEC Get_Role_Client_Hier_Ids '12/25/2010'
    
AUTHOR INITIALS:      
 Initials Name      
-------------------------------------------------------------------      
 KVK	Vinay Kumar K
     
 MODIFICATIONS       
 Initials Date  Modification      
--------------------------------------------------------------------
******************************************************************************************************/  

CREATE PROCEDURE [dbo].[Get_Role_Client_Hier_Ids]
( 
 @LastRanTs AS DATETIME )
AS 
BEGIN

      SET NOCOUNT ON ;  

      DECLARE @siteGroupLvlCd INT
      DECLARE @clientHierIds AS TABLE ( Client_Hier_Id INT )
		 
      SELECT
            @siteGroupLvlCd = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON cs.Codeset_Id = c.Codeset_Id
      WHERE
            cs.Codeset_Name = 'HierLevel'
            AND c.Code_Value = 'Site Group'

	
	--Get client hier ids of user site groups

      INSERT INTO
            @clientHierIds ( Client_Hier_Id )
            SELECT
                  ch.Client_Hier_Id
            FROM
                  dbo.User_Security_Role usr
                  INNER JOIN dbo.Sitegroup s
                        ON usr.User_Info_Id = s.Owner_User_Id
                  INNER JOIN Core.Client_Hier ch
                        ON ch.Sitegroup_Id = s.Sitegroup_Id
                  INNER JOIN dbo.Code sgType
                        ON sgType.Code_Id = s.Sitegroup_Type_Cd
                  INNER JOIN dbo.Security_Role sr
                        ON sr.Security_Role_Id = usr.Security_Role_Id
            WHERE
                  usr.Last_Change_Ts > @LastRanTs
                  AND sr.Add_Ts < @LastRanTs
                  AND ch.Hier_level_Cd = @siteGroupLvlCd
                  AND sgType.Code_Value = 'User'
            GROUP BY
                  ch.Client_Hier_Id

	  --Get client hier ids of all the newly created roles

      INSERT INTO
            @clientHierIds ( Client_Hier_Id )
            SELECT
                  ch.Client_Hier_Id
            FROM
                  dbo.Security_Role sr
                  INNER JOIN dbo.Security_Role_client_hier ch
                        ON ch.Security_Role_Id = sr.Security_Role_Id
            WHERE
                  sr.Add_Ts > @LastRanTs
            GROUP BY
                  ch.Client_Hier_Id

      SELECT
            Client_Hier_Id
      FROM
            @clientHierIds

END
GO
GRANT EXECUTE ON  [dbo].[Get_Role_Client_Hier_Ids] TO [CBMSApplication]
GO
