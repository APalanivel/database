SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******      
 
NAME:    
 dbo.cbmsMarketIntel_Get  
   
DESCRIPTION:    
  Outputs market_intel data 
    
INPUT PARAMETERS:    
 Name    DataType  Default Description    
------------------------------------------------------------    
@Market_Intel_Id INT  
              
OUTPUT PARAMETERS:    
Name    DataType  Default Description    
------------------------------------------------------------    
  
USAGE EXAMPLES:    
------------------------------------------------------------  
   
  
AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
GB  Geetansu Behera    
CMH  Chad Hattabaugh     
HG  Hari   
SKA  Shobhit Kr Agrawal  
   
   
MODIFICATIONS     
  
Initials Date  Modification    
------------------------------------------------------------    
GB				Created  
SKA  08/07/09	Modified as per the coding standard  
SS	 14/07/09	Added Brand and Schema owners for all objects
  
******/    

CREATE PROCEDURE [dbo].[cbmsMarketIntel_Get]
(  
	@Market_Intel_Id INT  
)  
AS  
BEGIN  
  
	SET NOCOUNT ON  
  
	SELECT 
		mi.market_intel_id,  
		mi.publish_date,  
		mi.market_intel_title,  
		mi.market_intel,  
		commodity_type = dbo.MARKET_INTEL_COMMODITIES(mi.market_intel_id),  
		state_name = dbo.MARKET_INTEL_STATES(mi.market_intel_id),  
		mi.Publication_cd,  
		c.CODE_VALUE AS Publication_Name
	FROM dbo.market_intel mi   
		LEFT OUTER JOIN dbo.CODE c   
			ON mi.Publication_cd = c.CODE_ID   
	WHERE mi.market_intel_id = @Market_Intel_Id  

END
GO
GRANT EXECUTE ON  [dbo].[cbmsMarketIntel_Get] TO [CBMSApplication]
GO
