SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Alternate_Fuel_Del]  

DESCRIPTION: It Deletes Alternate Fuel for Selected Alternate Fuel Id.
      
INPUT PARAMETERS:          
	NAME					DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Alternate_Fuel_Id		INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
	EXEC Alternate_Fuel_Del 851
  Rollback Tran

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			28-JUN-10	CREATED

*/

CREATE PROCEDURE dbo.Alternate_Fuel_Del
    (
      @Alternate_Fuel_Id INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.Alternate_Fuel
	WHERE
		Alternate_Fuel_Id = @Alternate_Fuel_Id

END
GO
GRANT EXECUTE ON  [dbo].[Alternate_Fuel_Del] TO [CBMSApplication]
GO
