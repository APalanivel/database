SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

















             
/******                  
NAME:    [Workflow].[Get_Invoice_Exception_Details_System_Grouping]              
DESCRIPTION:               
------------------------------------------------------------                 
 INPUT PARAMETERS:                  
 Name   DataType  Default Description                
 @MODULE_ID INT,                
    @Workflow_Queue_Saved_Filter_Query_id INT = NULL, -- group id from application                   
    @Queue_id VARCHAR(MAX) = NULL,                    ---  User selected from drop down Id 49,1,2,3                  
    @Exception_Type VARCHAR(MAX) = NULL,              -- unknown account                
    @Account_Number VARCHAR(500) = NULL,                
    @Client VARCHAR(500) = NULL,                
    @Site VARCHAR(500) = NULL,                
    @Country VARCHAR(500) = NULL,                
    @State VARCHAR(500) = NULL,                
    @City VARCHAR(500) = NULL,                
    @Commodity VARCHAR(500) = NULL,                
    @Invoice_ID VARCHAR(500) = NULL,                
    @Priority INT = 0,                                --- Yes                  
    @Exception_Status INT = NULL,                
    @COMMENTS VARCHAR(500) = NULL,                
    @Start_Date_in_Queue DATETIME = NULL,                
    @End_Date_in_Queue DATETIME = NULL,                
    @Start_Date_in_CBMS DATETIME = NULL,                
    @End_Date_in_CBMS DATETIME = NULL,                
    @Data_Source INT = NULL,                
    @Vendor VARCHAR(100) = NULL,                
    @Vendor_Type VARCHAR(100) = NULL,                
    @Month DATETIME = NULL,                
    @Filename VARCHAR(100) = NULL,                
    @UBMID INT,                                       -- take this value from first invoice and pass it                
    @UBMAccountNumber VARCHAR(200),                   -- -- take this value from first invoice and pass it                
    @startindex INT = 1,                
    @endindex INT = 2147483647,                
    @ONLY_COUNT_NEEDED INT = NULL,                
    @Sorting_Column_name VARCHAR(200) = NULL,         --send workflow.workflow_queue_output_column -> Data_Source_Column_Name                
    @Sorting_Type VARCHAR(10) = NULL,                 --- send 1 or 0. 1 - Asc, 0 - Desc                
                      
------------------------------------------------------------                  
 OUTPUT PARAMETERS:                  
 Name   DataType  Default Description                  
 @TOTAL_COUNT INT OUTPUT                 
------------------------------------------------------------                  
 USAGE EXAMPLES:                  
------------------------------------------------------------                  
AUTHOR INITIALS:                  
Initials Name                  
------------------------------------------------------------                  
AKP   ARUN KUMAR Summit Energy              
TRK   RAMAKRISHNA Summit Energy              
               
 MODIFICATIONS                   
 Initials Date   Modification                  
------------------------------------------------------------                  
 AKP    Sep-2019  Created              
 TRK    Sep-2019  Created              
 TRK   15-Oct-2019 Updated the @Queue_id = '-2' to  @Queue_id = '-1,0'  
 AP		OCt 31, 2019	Modified input parameter module id to module name.       
******/              
CREATE PROCEDURE [Workflow].[Get_Invoice_Exception_Details_System_Grouping]                  
    -- Add the parameters for the stored procedure here                     
    @MODULE_Name VARCHAR (30),                  
    @Workflow_Queue_Saved_Filter_Query_id INT = NULL, -- group id from application                     
    @Queue_id VARCHAR(MAX) = NULL,                    ---  User selected from drop down Id 49,1,2,3                    
    @Exception_Type VARCHAR(MAX) = NULL,              -- unknown account                  
    @Account_Number VARCHAR(500) = NULL,                  
    @Client VARCHAR(500) = NULL,                  
    @Site VARCHAR(500) = NULL,                  
    @Country VARCHAR(500) = NULL,                  
    @State VARCHAR(500) = NULL,                  
    @City VARCHAR(500) = NULL,      
    @Commodity VARCHAR(500) = NULL,                  
    @Invoice_ID VARCHAR(500) = NULL,                  
    @Priority INT = NULL,       --- Yes                    
    @Exception_Status INT = NULL,                  
    @COMMENTS VARCHAR(500) = NULL,                  
    @Start_Date_in_Queue DATETIME = NULL,                  
    @End_Date_in_Queue DATETIME = NULL,                  
    @Start_Date_in_CBMS DATETIME = NULL,                  
    @End_Date_in_CBMS DATETIME = NULL,                  
    @Data_Source INT = NULL,               
    @Vendor VARCHAR(100) = NULL,                  
    @Vendor_Type VARCHAR(100) = NULL,                  
    @Month DATETIME = NULL,                  
    @Filename VARCHAR(100) = NULL,                  
    @UBMID INT= NULL,                                       -- take this value from first invoice and pass it                  
    @UBMAccountNumber VARCHAR(200)= NULL,                   -- -- take this value from first invoice and pass it                  
    @startindex INT = 1,                  
    @endindex INT = 2147483647,                  
    @ONLY_COUNT_NEEDED INT = NULL,                  
    @Sorting_Column_name VARCHAR(200) = NULL,         --send workflow.workflow_queue_output_column -> Data_Source_Column_Name                  
    @Sorting_Type VARCHAR(10) = NULL,                 --- send 1 or 0. 1 - Asc, 0 - Desc                  
    @TOTAL_COUNT INT OUTPUT                  
AS                  
BEGIN -- SET NOCOUNT ON added to prevent extra result sets from                     
    -- interfering with SELECT statements.                     
    DECLARE @Proc_name VARCHAR(100) = 'Get_Invoice_Exception_Details_System_Grouping',                  
            @Input_Params VARCHAR(1000),                  
            @Error_Line INT,                  
            @Error_Message VARCHAR(3000),                  
            @SQL NVARCHAR(MAX),        
   @SQL1 NVARCHAR(MAX),                  
            @SQL2 NVARCHAR(MAX),       
   @SQL3 NVARCHAR(MAX),                   
            @Data_Source_Column_Name VARCHAR(100),  
       @UBMID_Local INT,                                       -- take this value from first invoice and pass it                  
    @UBMAccountNumber_Local VARCHAR(200),
	@Module_id INT 
	
	   SELECT @Module_id=Workflow_queue_id FROM workflow.Workflow_Queue 
					WHERE workflow_queue_name = @MODULE_Name  
      
    select @UBMID_Local =ci.UBM_ID , @UBMAccountNumber_Local = ci.UBM_ACCOUNT_CODE  
    from CU_INVOICE CI  
    where cu_invoice_id = casT(@Invoice_ID  as int) 
	
	 IF (@Start_Date_in_Queue is NULL AND @End_Date_in_Queue IS NOT null)
   begin
   
   SET @Start_Date_in_Queue = '2000-01-01 00:00:00'  
   end

    IF (@Start_Date_in_Queue is NOT NULL AND @End_Date_in_Queue IS  null)
	begin
   
   SET @End_Date_in_Queue =  CONVERT(VARCHAR(19), GETDATE(), 120) 
   END
   
      IF (@Start_Date_in_CBMS is NULL AND @End_Date_in_cbms IS NOT null)
   begin
   
   SET @Start_Date_in_CBMS = '2000-01-01 00:00:00'  
   end

    IF (@Start_Date_in_CBMS is NOT NULL AND @End_Date_in_cbms IS  null)
	begin
   
   SET @End_Date_in_cbms =  CONVERT(VARCHAR(19), GETDATE(), 120) 
   end  
           
   set @End_Date_in_Queue = dateadd (d, 1, @End_Date_in_Queue )           
   set @End_Date_in_CBMS =dateadd (d,1 , @End_Date_in_CBMS )   
   
    if (isnull(@Sorting_Column_name,'') ='')
   set @Sorting_Column_name  ='Inv Id'
   
   if (isnull(@Sorting_Type,'') ='')
   set @Sorting_Type ='asc'               
                  
   SET @Data_Source_Column_Name =                    
    (                    
        SELECT case when Data_Source_Column_Name  ='Data Source' then '[Data Source]'          
  else Data_Source_Column_Name          
  end                  
        FROM Workflow.Workflow_Queue_Output_Column                    
        WHERE Display_Column_Name = @Sorting_Column_name  
		AND workflow_queue_id = @MODULE_ID                  
    );            
                  
                  
    SELECT @Input_Params                  
        = '@Workflow_Queue_Saved_Filter_Query_id: ' + CAST(@Workflow_Queue_Saved_Filter_Query_id AS VARCHAR);                  
    SET NOCOUNT ON;                  
                  
                  
                  
    BEGIN TRY                  
                  
        IF (@Queue_id IS NOT NULL)                  
        BEGIN                  
            SELECT CAST(Segments AS INT) AS User_info_id                  
            INTO #temp_user_info                  
            FROM dbo.ufn_split(@Queue_id, ',');                  
        END;                  
                  
        IF (@Exception_Type IS NOT NULL)                  
        BEGIN                  
            SELECT CAST(Segments AS INT) AS exception_type                  
INTO #temp_Exception_Type                  
            FROM dbo.ufn_split(@Exception_Type, ',');                  
        END;                  
                  
 
                  
        CREATE TABLE #DEFAULTEXCEPTIONS                  
        (                  
            [RN] [BIGINT] NULL,            
            [CU_INVOICE_ID] [INT] NULL,                  
            [ACCOUNT] [VARCHAR](MAX) NULL,                  
            [Account_Id] varchar(50) NULL,                  
            [EXCEPTION_TYPE] [VARCHAR](MAX) NULL,                  
            [EXCEPTION_STATUS_TYPE] [VARCHAR](MAX) NULL,                  
            [ASSIGNEE] [VARCHAR](MAX) NULL,                 
            [CLIENTID] INT NULL,                  
            [CLIENT] [VARCHAR](MAX) NULL,                  
            [COMMODITY] [VARCHAR](MAX) NULL,                  
            [DATA SOURCE] [VARCHAR](MAX) NULL,                  
            [DATE_IN_QUEUE] [DATETIME] NULL,                [DATE_IN_CBMS] [DATETIME] NULL,                  
            [FILEID] INT NULL,                  
            [FILENAME] [VARCHAR](MAX) NULL,                  
            [SERVICE_MONTH] [VARCHAR](MAX) NULL,                  
            [SITE] [VARCHAR](MAX) NULL,                  
            [COUNTRY] [NVARCHAR](MAX) NULL,                  
            [STATE] [NVARCHAR](MAX) NULL,                  
            [VENDOR] [NVARCHAR](MAX) NULL,                  
            [VENDOR_TYPE] [NVARCHAR](MAX) NULL,                  
            [COMMENTS] VARCHAR(MAX) NULL,                  
            [is_priority] INT ,
			[priority_cd] numeric (10,0)                 
        );                  
                  
                  
        CREATE TABLE #DEFAULTEXCEPTIONSGROUPDETAILS                  
        (                  
            index_range INT NULL,                  
            [CU_INVOICE_ID] INT NULL,                  
            [ACCOUNT] [VARCHAR](MAX) NULL,                  
            [Account_Id] varchar(50) NULL,                  
            [EXCEPTION_TYPE] [VARCHAR](MAX) NULL,                  
            [EXCEPTION_STATUS_TYPE] [VARCHAR](MAX) NULL,                  
          [ASSIGNEE] [VARCHAR](MAX) NULL,                  
            [CLIENTID] INT NULL,                  
            [CLIENT] [VARCHAR](MAX) NULL,                  
            [COMMODITY] [VARCHAR](MAX) NULL,                  
            [DATA SOURCE] [VARCHAR](MAX) NULL,                  
            [DATE_IN_QUEUE] [DATETIME] NULL,                  
            [DATE_IN_CBMS] [DATETIME] NULL,                  
            [FILEID] INT NULL,                  
            [FILENAME] [VARCHAR](MAX) NULL,                  
            [SERVICE_MONTH] [VARCHAR](MAX) NULL,                  
            [SITE] [VARCHAR](MAX) NULL,                  
            [COUNTRY] [NVARCHAR](MAX) NULL,                  
            [STATE] [NVARCHAR](MAX) NULL,                  
            [VENDOR] [NVARCHAR](MAX) NULL,                  
            [VENDOR_TYPE] [NVARCHAR](MAX) NULL,                  
            [COMMENTS] VARCHAR(MAX) NULL,                  
            [is_priority] INT   ,
			[priority_cd] numeric (10,0)                   
        );                  
                  
        SET @sql1                  
            = '   insert #DEFAULTEXCEPTIONS                    
     SELECT                       
     ROW_NUMBER ()OVER(PARTITION BY CUEX.CU_INVOICE_ID ORDER BY (select null)    ) AS RN,                     
   CUEX.CU_INVOICE_ID                    
    ,  ( CASE WHEN CUEX.EXCEPTION_TYPE = ' + '''' + 'WRONG ACCOUNT' + ''''                    
              + ' THEN NULL                      
             WHEN AG.ACCOUNT_GROUP_ID IS NULL                      
                   THEN COALESCE(CHA.Account_Number, CUEX.UBM_Account_Number,INV_LBL.ACCOUNT_NUMBER)                      
             ELSE COALESCE( AG.GROUP_BILLING_NUMBER,CHA.ACCOUNT_NUMBER, CUEX.UBM_ACCOUNT_NUMBER,INV_LBL.account_number)                           
        END ) AS ACCOUNT                 ,(CASE WHEN CUEX.EXCEPTION_TYPE = ' + '''' + 'WRONG ACCOUNT' + ''''                    
              + ' THEN NULL  
			  when    AG.ACCOUNT_GROUP_ID IS NOT NULL then '+''''+'MULTIPLE'+''''+           
        'ELSE COALESCE(cast(CHA.Account_Id as varchar), cast(CUEX.Account_Id as varchar))   
        END ) AS ACCOUNT_ID  
    , CUEX.EXCEPTION_TYPE                    
    , CUEX.EXCEPTION_STATUS_TYPE                     
    , UI.FIRST_NAME + ' + '''' + ' ' + ''''                  
              + '+ UI.LAST_NAME ASSIGNEE                    
   , CASE WHEN CUEX.CLIENT_HIER_ID IS NOT NULL                    
                 THEN CH.Client_Id                     
           ELSE  CI.Client_id                    
      END CLIENT_ID                    
    , CASE WHEN CUEX.CLIENT_HIER_ID IS NOT NULL                    
                 THEN CH.CLIENT_NAME                    
           ELSE  coalesce(CL.CLient_Name,CUEX.UBM_CLIENT_NAME , INV_LBL.CLIENT_NAME)                 
      END CLIENT                  
    , ISNULL(COM.COMMODITY_NAME , INV_LBL.COMMODITY) AS  COMMODITY                    
    , UBM.UBM_NAME [DATA SOURCE]                    
    , CUEX.DATE_IN_QUEUE                    
    , CUEX.DATE_IN_CBMS                    
 , CUEX.CBMS_IMAGE_ID [FILEID]                  
    , CUEX.CBMS_DOC_ID [FILENAME]                    
    , ( CASE WHEN CUEX.EXCEPTION_TYPE = ' + '''' + 'WRONG MONTH' + ''''                  
              + 'THEN NULL                    
             WHEN AG.ACCOUNT_GROUP_ID IS NULL and SM.SERVICE_MONTH is not null THEN CONVERT(VARCHAR(10),  SM.SERVICE_MONTH , 101)                    
             when CUEX.SERVICE_MONTH is not null then CUEX.SERVICE_MONTH               
     else null                  
                       
        END ) AS SERVICE_MONTH                    
    , CASE WHEN CUEX.CLIENT_HIER_ID IS NOT NULL                    
                 THEN isnull(CH.SITE_NAME , inv_lbl.site_name)                   
           ELSE  isnull(CUEX.UBM_SITE_NAME  , inv_lbl.site_name)                  
      END [SITE]                    
    ,isnull( C.country_name, INV_LBL.COUNTRY) as country,                    
 isnull(S.state_name, INV_LBL.STATE_NAME) as state,                    
  CASE WHEN  CI.ACCOUNT_GROUP_ID IS NULL THEN isnull(CHA.Account_Vendor_Name  ,INV_LBL.VENDOR_NAME )                  
     WHEN CI.ACCOUNT_GROUP_ID IS NOT NULL THEN isnull(V.VENDOR_NAME  ,INV_LBL.VENDOR_NAME )                   
     ELSE INV_LBL.VENDOR_NAME                    
     end  AS VENDOR                     
 ,  CASE WHEN  CI.ACCOUNT_GROUP_ID IS NULL THEN isnull(AVT.ENTITY_NAME ,inv_lbl.vendor_type )                   
   WHEN CI.ACCOUNT_GROUP_ID IS NOT NULL THEN isnull(VT.ENTITY_NAME  ,inv_lbl.vendor_type )                   
     ELSE inv_lbl.vendor_type                    
     end AS VENDOR_TYPE             
    , NULL,                  
 isnull(cia.is_priority   ,0)  , 
case when cia.is_priority is null or cia.is_priority=0
 then 9999999
 else cia.source_cd
 end                
FROM  DBO.CU_EXCEPTION_DENORM CUEX                    
JOIN dbo.QUEUE q ON q.QUEUE_ID = CUEX.QUEUE_ID                    
                 INNER JOIN dbo.Entity qt ON qt.ENTITY_ID = q.QUEUE_TYPE_ID                    
      JOIN                    
      DBO.CU_INVOICE CI                    
            ON CI.CU_INVOICE_ID = CUEX.CU_INVOICE_ID   
		LEFT JOIN	DBO.CLIENT    CL                
            ON CI.CLIENT_ID = CL.CLIENT_ID                      
 LEFT JOIN WORKFLOW.CU_INVOICE_ATTRIBUTE CIA                     
 ON  CIA.CU_INVOICE_ID = cuex.CU_INVOICE_ID                    
      LEFT JOIN                    
      DBO.CU_INVOICE_SERVICE_MONTH SM                    
        ON SM.CU_INVOICE_ID = CUEX.CU_INVOICE_ID                    
               AND SM.ACCOUNT_ID = CUEX.ACCOUNT_ID        
      LEFT OUTER JOIN                    
      DBO.USER_INFO UI                    
            ON UI.QUEUE_ID = CUEX.QUEUE_ID                    
      LEFT JOIN                    
      DBO.CU_EXCEPTION_TYPE CET                    
            ON CUEX.EXCEPTION_TYPE = CET.EXCEPTION_TYPE                    
      LEFT JOIN                    
      DBO.ACCOUNT_GROUP AG                    
            ON AG.ACCOUNT_GROUP_ID = CI.ACCOUNT_GROUP_ID                    
                    
      LEFT JOIN                    
      (DBO.VENDOR V                    
       INNER JOIN                    
       DBO.ENTITY VT                    
             ON VT.ENTITY_ID = V.VENDOR_TYPE_ID)                    
            ON V.VENDOR_ID = AG.VENDOR_ID                    
                    
      LEFT JOIN                    
      CORE.CLIENT_HIER_ACCOUNT CHA                    
            ON CHA.CLIENT_HIER_ID = CUEX.CLIENT_HIER_ID                    
               AND CHA.ACCOUNT_ID = CUEX.ACCOUNT_ID                     
      LEFT JOIN                    
      (DBO.VENDOR AV                    
       INNER JOIN                    
       DBO.ENTITY AVT                    
             ON AVT.ENTITY_ID = AV.VENDOR_TYPE_ID)                    
            ON AV.VENDOR_ID = CHA.Account_Vendor_Id                    
      LEFT JOIN                    
      CORE.CLIENT_HIER CH                    
            ON CH.CLIENT_HIER_ID = CUEX.CLIENT_HIER_ID                    
   LEFT JOIN COUNTRY c                    
            ON CH.COUNTRY_ID = C.COUNTRY_ID                     
   LEFT JOIN state s                            ON CH.state_id = s.state_id                     
      LEFT JOIN                    
      DBO.COMMODITY COM                    
            ON COM.COMMODITY_ID = CHA.COMMODITY_ID                    
      LEFT JOIN                    
      DBO.CU_INVOICE_LABEL INV_LBL                    
            ON CUEX.CU_INVOICE_ID = INV_LBL.CU_INVOICE_ID                    
      LEFT JOIN                    
      DBO.UBM UBM                    
            ON UBM.UBM_ID = CI.UBM_ID                  
  LEFT JOIN dbo.Entity status_type ON status_type.ENTITY_name = cuex.EXCEPTION_STATUS_TYPE                   
  AND STATUS_TYPE.ENTITY_description =' + '''' + 'UBM Exception Status' + ''''                  
        + ' OUTER APPLY                    
      (     SELECT                    
                        UAC.ACCOUNT_VENDOR_NAME + ' + '''' + ', ' + ''''                  
              + 'FROM        CORE.CLIENT_HIER_ACCOUNT UAC                    
                        INNER JOIN                    
                        CORE.CLIENT_HIER_ACCOUNT SAC                    
                              ON SAC.METER_ID = UAC.METER_ID                    
            WHERE       UAC.ACCOUNT_TYPE = ' + '''' + 'UTILITY' + '''' + 'AND   SAC.ACCOUNT_TYPE = ' + ''''                  
              + 'SUPPLIER' + '''' + 'AND   CHA.ACCOUNT_TYPE = ' + '''' + 'SUPPLIER' + ''''                  
              + 'AND   SAC.ACCOUNT_ID = CHA.ACCOUNT_ID                    
            GROUP BY    UAC.ACCOUNT_VENDOR_NAME                    
            FOR XML PATH(' + '''' + ''''                  
              + ')) UV(VENDOR_NAME)                    
   WHERE  CUEX.EXCEPTION_TYPE = ' + '''' + 'unknown account' + ''''       
                  
              set @sql2 =    
  
 ' and CI.UBM_ID=' + CAST(@UBMID_Local AS VARCHAR) + ' and CI.UBM_ACCOUNT_CODE='                  
              + ''''+@UBMAccountNumber_Local+'''';                  
                  
  set @sql = @sql1 + @sql2    
    
       
        EXECUTE sp_executesql @sql;     
    
            
                  
                  
        UPDATE DF             
        SET DF.COMMODITY = CASE                  
                               WHEN (                  
                                    (                  
                                SELECT COUNT(DISTINCT COMMODITY)                  
                                        FROM #DEFAULTEXCEPTIONS DF1                  
                                        WHERE COMMODITY IS NOT NULL                  
                                              AND DF.CU_INVOICE_ID = DF1.CU_INVOICE_ID                  
                                    ) > 1                  
                                    ) THEN                  
                                   'MULTIPLE'                  
                               ELSE                  
                                   DF.COMMODITY                  
                           END,                  
            DF.SITE = CASE                  
                          WHEN (                  
                               (                  
                                   SELECT COUNT(DISTINCT [SITE])                  
                     FROM #DEFAULTEXCEPTIONS DF1                  
                                   WHERE SITE IS NOT NULL                                               AND DF.CU_INVOICE_ID = DF1.CU_INVOICE_ID                  
                               ) > 1                  
                               ) THEN                  
                              'MULTIPLE'                  
                          ELSE                  
                              DF.SITE                  
                      END,                  
            DF.ACCOUNT = CASE                  
                             WHEN (                  
                                  (                  
                                      SELECT COUNT(DISTINCT ACCOUNT)                  
                                      FROM #DEFAULTEXCEPTIONS DF1                  
                                      WHERE ACCOUNT IS NOT NULL                  
                                            AND DF.CU_INVOICE_ID = DF1.CU_INVOICE_ID                  
) > 1                  
                                  ) THEN                  
                                 'MULTIPLE'                  
                             ELSE                  
                                 DF.ACCOUNT          
                         END  ,
						    DF.ACCOUNT_ID = CASE                    
                             WHEN (                    
                  (                    
                                      SELECT COUNT(DISTINCT ACCOUNT_ID)                    
           FROM #DEFAULTEXCEPTIONS DF1                    
                                      WHERE ACCOUNT_ID IS NOT NULL                    
                                            AND DF.CU_INVOICE_ID = DF1.CU_INVOICE_ID                    
                                  ) > 1                    
                                  ) THEN                    
                                 'MULTIPLE'                    
                             ELSE                    
                                 DF.ACCOUNT_ID                    
                         END                   
        FROM #DEFAULTEXCEPTIONS DF;                  
                  
        DELETE FROM #DEFAULTEXCEPTIONS                  
        WHERE RN <> 1;                  
                  
        IF (@COMMENTS IS NOT NULL)                  
        BEGIN                  
                  
            UPDATE DFG                  
            SET DFG.COMMENTS =                  
                (                  
                    SELECT TOP (1)                  
                           IMAGE_COMMENT                  
                    FROM CU_INVOICE_COMMENT CUI                  
                    WHERE CUI.CU_INVOICE_ID = DFG.CU_INVOICE_ID                  
                          AND CUI.IMAGE_COMMENT = @COMMENTS                  
                    ORDER BY CUI.CU_INVOICE_COMMENT_ID DESC                  
                )                  
            FROM #DEFAULTEXCEPTIONS DFG;                  
            
                  
        END;                  
                  ELSE                  
        BEGIN                  
                  
            UPDATE DFG                  
            SET DFG.COMMENTS =                  
                (                  
                    SELECT TOP (1)                  
                           IMAGE_COMMENT                  
                    FROM CU_INVOICE_COMMENT CUI                  
                    WHERE CUI.CU_INVOICE_ID = DFG.CU_INVOICE_ID                  
                    ORDER BY CUI.CU_INVOICE_COMMENT_ID DESC                  
                )                  
            FROM #DEFAULTEXCEPTIONS DFG;                  
       
        END;                
            
  SET @TOTAL_COUNT = (SELECT COUNT(1) FROM #DEFAULTEXCEPTIONS   )          
                  
                  
        SET @sql3                  
            = '                  
        INSERT INTO #DEFAULTEXCEPTIONSGROUPDETAILS                  
        (                   
                               
            index_range,                  
            CU_INVOICE_ID,                  
            ACCOUNT,                  
   [Account_Id],                  
            EXCEPTION_TYPE,                  
            EXCEPTION_STATUS_TYPE,                  
            ASSIGNEE,                  
            CLIENTID,                  
            CLIENT,                  
            COMMODITY,                  
            [DATA SOURCE],                  
         DATE_IN_QUEUE,                  
            DATE_IN_CBMS,                  
            FILEID,                  
            FILENAME,                  
            SERVICE_MONTH,                  
            SITE,                  
            COUNTRY,                  
            STATE,                  
            VENDOR,                  
            VENDOR_TYPE,                  
            COMMENTS,                  
   is_priority ,
   priority_cd                 
         )                  
        SELECT ROW_NUMBER() OVER (ORDER BY is_priority DESC, priority_cd asc' + CASE                  
                                                                    WHEN @Data_Source_Column_Name IS NOT NULL THEN                  
                                                                  +',' + @Data_Source_Column_Name + ' '                  
                                                                    ELSE                  
                                                                        ''                  
                                                                END + CASE                  
                                                                          WHEN @Data_Source_Column_Name IS NOT NULL                  
                                                                               AND @Sorting_Type IS NOT NULL THEN                  
                                     @Sorting_Type                
                                                                          ELSE                  
                                                                              ''                  
                                                    END                  
              + ') AS index_range,                  
               CU_INVOICE_ID,                  
               ACCOUNT,                  
      [Account_Id],                  
               EXCEPTION_TYPE,                  
        EXCEPTION_STATUS_TYPE,                  
               ASSIGNEE,                  
               CLIENTID,                  
               CLIENT,                  
               COMMODITY,                  
               [DATA SOURCE],                  
               DATE_IN_QUEUE,                  
               DATE_IN_CBMS,                  
               FILEID,                  
               FILENAME,                  
               SERVICE_MONTH,                  
               SITE,                  
               COUNTRY,                  
               STATE,                  
               VENDOR,                  
               VENDOR_TYPE,                  
               COMMENTS,                  
        is_priority ,
		priority_cd                 
        FROM #DEFAULTEXCEPTIONS';                  
                  
        EXECUTE sp_executesql @sql3;                  
                  
        SELECT DF.CU_INVOICE_ID     AS [Inv Id],                  
               DF.[DATE_IN_QUEUE]    AS [Date in Queue],                  
               DF.[DATE_IN_CBMS]    AS [Date in CBMS],                  
               DF.ASSIGNEE      AS Assignee,                  
               DF.EXCEPTION_TYPE    AS [Exception Type],                  
               DF.EXCEPTION_STATUS_TYPE   AS [Exception Status],                  
               DF.[DATA SOURCE]     AS [Data Source],                  
               DF.CLIENT      AS Client,                  
               DF.[SITE]      AS[Site],                  
               DF.COUNTRY      AS Country,                  
               DF.[STATE]      AS [State],                  
               DF.ACCOUNT      AS [Account no],                  
               DF.Account_Id     AS Account_Id,                  
               DF.VENDOR      AS Vendor,                  
               DF.VENDOR_TYPE     AS [Vendor Type],                  
               DF.COMMODITY      AS Commodity,                  
               DF.SERVICE_MONTH     AS [Month],                  
               DF.[FILENAME]     AS [Filename],                  
               DF.COMMENTS      AS Comments,                  
               DF.FILEID      AS FileID,                  
               isnull(DF.is_priority,0)   AS [priority],          
       CASE WHEN EXISTS (SELECT 1 FROM workflow.Cu_Invoice_Workflow_Action_Batch_Dtl CIWABD             
  INNER JOIN DBO.Code C ON CIWABD.Status_Cd=C.Code_Id           
  AND CIWABD.Cu_Invoice_Id =DF.Cu_Invoice_Id          
  WHERE C.Code_Value='Pending' ) THEN  'Pending'  ELSE  NULL END AS [Batch Status],          
  CASE WHEN EXISTS(SELECT 1 FROM workflow.Cu_Invoice_Attribute CU          
  JOIN code c           
  ON cu.Source_Cd = c.Code_Id          
  AND cu.CU_INVOICE_ID = df.CU_INVOICE_ID       
  AND c.Code_Value = 'Systempriorityclient') THEN 'System'          
  WHEN EXISTS (SELECT 1 FROM workflow.Cu_Invoice_Attribute CU          
  JOIN code c           
  ON cu.Source_Cd = c.Code_Id          
  AND cu.CU_INVOICE_ID = df.CU_INVOICE_ID           
  AND c.Code_Value = 'Userprioritymanual') THEN 'User'          
  END AS Priority_Source          
        FROM #DEFAULTEXCEPTIONSGROUPDETAILS DF             
   
        WHERE DF.index_range                  
        BETWEEN @startindex AND @endindex
		ORDER BY DF.index_range;                  
                  
                  
        DROP TABLE #DEFAULTEXCEPTIONSGROUPDETAILS;                  
        DROP TABLE #DEFAULTEXCEPTIONS;                  
    --DELETE THE ENTRIES WHICH WAS DEFAULT EARLIER AND NOW USER DID NOT WANT TOR                    
                  
    END TRY                  
    BEGIN CATCH                  
                  
                  
                  
        -- Entry made to the logging SP to capture the errors.                    
        SELECT @Error_Line = error_line(),                  
               @Error_Message = error_message();                  
                  
        INSERT INTO dbo.StoredProc_Error_Log                  
        (                  
            StoredProc_Name,                  
            Error_Line,                  
            Error_message,                  
            Input_Params                  
        )                  
        VALUES                  
        (@Proc_name, @Error_Line, @Error_Message, @Input_Params);                  
        RETURN -99;                  
                  
    END CATCH;                  
END;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
GO
GRANT EXECUTE ON  [Workflow].[Get_Invoice_Exception_Details_System_Grouping] TO [CBMSApplication]
GO
