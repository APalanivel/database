SET NUMERIC_ROUNDABORT OFF
GO

SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******

NAME:  
  dbo.SR_RFP_LP_UPDATE_STATUS_P
  
DESCRIPTION:
  
INPUT PARAMETERS:  
Name				DataType	Default Description  
------------------------------------------------------------
@user_id			varchar(10)
@session_id			varchar(20)
@rfp_id				int
site_id				int
  
OUTPUT PARAMETERS:  
Name   DataType  Default Description  
------------------------------------------------------------  

USAGE EXAMPLES:  
------------------------------------------------------------  
	
	EXEC dbo.SR_RFP_LP_UPDATE_STATUS_P null,null,4222,16987

AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------
 CPE		Chaitanya Panduga Eshwar
 KVK		VInay K
  
MODIFICATIONS  
 Initials Date		 Modification  
------------------------------------------------------------  
 CPE	  04/18/2011 Replaced the INSERT SSO_PROJECT_OWNER_MAP with EXEC dbo.cbmsSsoProjectOwner_Save
 AKR      2012-01-21 Changes Summit to Schnider Electric as a part of Name Change Project
 KVK	  01/27/2015 as part of documents cbmsSsoProjectOwner_Save procedure has been modifed to return clienthierid but since the procedure is called inside this proc we need to ignore the output
******/
CREATE PROCEDURE [dbo].[SR_RFP_LP_UPDATE_STATUS_P]
      ( 
       @user_id VARCHAR(10)
      ,@session_id VARCHAR(20)
      ,@rfp_id INT
      ,@site_id INT )
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE
            @commodityId INT
           ,@commodityname VARCHAR(20)
           ,@projectTiltle VARCHAR(200)
           ,@projectStatusTypeId INT
           ,@projectownershiptypeid INT
           ,@projectcategorytypeid INT
           ,@projectId INT
           ,@EntCommodityId INT
           ,@EntNGCommodityId INT
           ,@EntEPCommodityId INT
           ,@hier_level_cd INT
           ,@status VARCHAR(100)
           ,@update_status_type_id INT

--@commodityId
      SELECT
            @commodityId = commodity_type_id
      FROM
            dbo.SR_RFP
      WHERE
            sr_rfp_id = @rfp_id

-- @commodityname
      SELECT
            @commodityname = entity_name
      FROM
            dbo.ENTITY
      WHERE
            entity_id = @commodityId

      SELECT
            @projectTiltle = @commodityname + ' Request For Proposal for ' + Site_name
      FROM
            core.Client_Hier
      WHERE
            site_id = @site_id

      SELECT
            @projectStatusTypeId = max(case WHEN ENTITY_DESCRIPTION = 'Project Status Type'
                                                 AND ENTITY_NAME = 'Open' THEN ENTITY_ID
                                            ELSE 0
                                       END)
           ,@projectownershiptypeid = max(case WHEN ENTITY_DESCRIPTION = 'PROJECT OWNERSHIP'
                                                    AND ENTITY_NAME = 'Schneider Electric' THEN ENTITY_ID
                                               ELSE 0
                                          END)
           ,@projectcategorytypeid = max(case WHEN ENTITY_DESCRIPTION = 'SSO Project Category'
                                                   AND ENTITY_NAME = 'RFP' THEN ENTITY_ID
                                              ELSE 0
                                         END)
           ,@EntCommodityId = max(case WHEN ENTITY_DESCRIPTION = 'Commodity'
                                            AND ENTITY_NAME = 'Natural Gas' THEN ENTITY_ID
                                       ELSE 0
                                  END)
           ,@EntNGCommodityId = max(case WHEN ENTITY_DESCRIPTION = 'Project Commodity Type'
                                              AND ENTITY_NAME = 'Natural Gas' THEN ENTITY_ID
                                         ELSE 0
                                    END)
           ,@EntEPCommodityId = max(case WHEN ENTITY_DESCRIPTION = 'Project Commodity Type'
                                              AND ENTITY_NAME = 'Electric Power' THEN ENTITY_ID
                                         ELSE 0
                                    END)
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_DESCRIPTION IN ( 'Project Status Type', 'PROJECT OWNERSHIP', 'SSO Project Category', 'Commodity', 'Project Commodity Type', 'SSO Owner Type' )
            AND ENTITY_NAME IN ( 'Open', 'Schneider Electric', 'RFP', 'Natural Gas', 'Electric Power', 'Site' )

      SELECT
            @hier_level_cd = Code_Id
      FROM
            dbo.CODE cd
            INNER JOIN dbo.Codeset cs
                  ON cs.Codeset_Id = cd.CodeSet_Id
      WHERE
            cs.CodeSet_Name = 'HierLevel'
            AND cd.Code_Value = 'Site'
            AND cd.Is_Active = 1
		
      SELECT
            @commodityId = case @commodityId
                             WHEN @EntCommodityId THEN @EntNGCommodityId
                             ELSE @EntEPCommodityId
                           END

      SELECT
            @status = en.entity_name
      FROM
            dbo.SR_RFP_LP_ACCOUNT_SUMMARY summary
            JOIN dbo.SR_RFP_ACCOUNT rfp_account
                  ON summary.sr_account_group_id = rfp_account.sr_rfp_account_id
            JOIN dbo.ACCOUNT acct
                  ON rfp_account.account_id = acct.account_id
            JOIN dbo.ENTITY en
                  ON en.entity_id = summary.status_type_id
      WHERE
            acct.site_id = @site_id
            AND rfp_account.sr_rfp_id = @rfp_id
            AND rfp_account.is_deleted = 0

      SELECT
            @update_status_type_id = ENTITY_ID
      FROM
            dbo.ENTITY(NOLOCK)
      WHERE
            ENTITY_DESCRIPTION = 'Load Profile Status'
            AND ENTITY_NAME = 'Pending Client Approval'
            AND @status IN ( 'Not Sent', 'Pending Client Approval', 'Client approved' ) 

      SELECT
            @update_status_type_id = entity_id
      FROM
            dbo.entity(NOLOCK)
      WHERE
            ENTITY_DESCRIPTION = 'Load Profile Status'
            AND entity_name = 'Revised - Pending client approval'
            AND @@ROWCOUNT = 0

      UPDATE
            summary
      SET   
            status_type_id = @update_status_type_id
           ,date_sent = getdate()
      FROM
            dbo.SR_RFP_LP_ACCOUNT_SUMMARY summary
            JOIN dbo.SR_RFP_ACCOUNT rfp_account
                  ON summary.sr_account_group_id = rfp_account.sr_rfp_account_id
            JOIN dbo.ACCOUNT acct
                  ON rfp_account.account_id = acct.account_id
      WHERE
            acct.site_id = @site_id
            AND rfp_account.sr_rfp_id = @rfp_id


      UPDATE
            checklist
      SET   
            lp_sent_to_client_date = getdate()
      FROM
            dbo.SR_RFP_CHECKLIST checklist
            JOIN dbo.SR_RFP_ACCOUNT rfp_account
                  ON checklist.sr_rfp_account_id = rfp_account.sr_rfp_account_id
            JOIN dbo.ACCOUNT acct
                  ON rfp_account.account_id = acct.account_id
      WHERE
            acct.site_id = @site_id
            AND rfp_account.sr_rfp_id = @rfp_id

	--added by prasad
      IF NOT EXISTS ( SELECT
                        1
                      FROM
                        RFP_SITE_PROJECT
                      WHERE
                        RFP_ID = @rfp_id
                        AND SITE_ID = @site_id ) 
            BEGIN
                  INSERT      INTO SSO_PROJECT
                              ( 
                               PROJECT_TITLE
                              ,PROJECT_DESCRIPTION
                              ,PROJECT_STATUS_TYPE_ID
                              ,COMMODITY_TYPE_ID
                              ,START_DATE
                              ,PROJECT_OWNERSHIP_TYPE_ID
                              ,IS_URGENT
                              ,PROJECT_CATEGORY_TYPE_ID )
                  VALUES
                              ( 
                               @projectTiltle
                              ,'Schneider Electric has prepared a strategic sourcing RFP for this site. Our focus is to engage the supplier community on your behalf to establish reliable supply, competitive price structures, and a high commitment to service. Please track the steps of this project and activity updates through the Project Detail report.'
                              ,@projectStatusTypeId
                              ,@commodityId
                              ,getdate()
                              ,@projectownershiptypeid
                              ,0
                              ,@projectcategorytypeid )

                  SET @projectId = scope_identity()

                  INSERT      INTO RFP_SITE_PROJECT
                              ( RFP_ID, SITE_ID, SSO_PROJECT_ID )
                  VALUES
                              ( @rfp_id, @site_id, @projectId )

                  --this is table to hold the result from proc
				  CREATE TABLE #cbmsSsoProjectOwner_Save_Output ( Client_Hier_Id INT )

                  INSERT      INTO #cbmsSsoProjectOwner_Save_Output
                              ( 
                               Client_Hier_Id )
                              EXEC dbo.cbmsSsoProjectOwner_Save 
                                    @projectId
                                   ,@site_id
                                   ,@hier_level_cd
            END

      SELECT
            @projectId = SSO_PROJECT_ID
      FROM
            dbo.RFP_SITE_PROJECT
      WHERE
            RFP_ID = @rfp_id
            AND SITE_ID = @site_id

      SELECT TOP 1
            @user_id = user_info_id
      FROM
            dbo.CLIENT_CEM_MAP CCM
            JOIN Core.Client_Hier CH
                  ON CCM.CLIENT_ID = CH.Client_Id
      WHERE
            CH.site_id = @site_id

      INSERT      INTO dbo.SSO_PROJECT_ACTIVITY
                  ( 
                   SSO_PROJECT_ID
                  ,CREATED_BY_ID
                  ,ACTIVITY_DATE
                  ,ACTIVITY_DESCRIPTION )
                  SELECT
                        SSO_PROJECT_ID
                       ,CREATED_BY_ID
                       ,ACTIVITY_DATE
                       ,ACTIVITY_DESCRIPTION
                  FROM
                        ( SELECT
                              @projectId AS SSO_PROJECT_ID
                             ,@user_id AS CREATED_BY_ID
                             ,getdate() AS ACTIVITY_DATE
                             ,'Schneider Electric has initiated a strategic sourcing RFP.  We are evaluating pricing products, length of contract, special contract terms and determining which suppliers should receive the opportunity to submit a proposal.' AS ACTIVITY_DESCRIPTION
                          UNION ALL
                          SELECT
                              @projectId
                             ,@user_id
                             ,getdate()
                             ,'Your approval of the projected consumption values is required. Schneider Electric has provided a snapshot of expected volumes based on historical consumption levels. Your input regarding potential changes in production or consumption will help avoid risks associated with under or over specifying desired purchase levels.' ) A
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.SSO_PROJECT_ACTIVITY
                                     WHERE
                                          SSO_PROJECT_ID = @projectId
                                          AND ACTIVITY_DESCRIPTION = A.ACTIVITY_DESCRIPTION )

      INSERT      INTO dbo.SSO_PROJECT_STEP
                  ( 
                   SSO_PROJECT_ID
                  ,STEP_NO
                  ,IS_ACTIVE
                  ,IS_COMPLETE )
                  SELECT
                        SSO_PROJECT_ID
                       ,STEP_NO
                       ,IS_ACTIVE
                       ,IS_COMPLETE
                  FROM
                        ( SELECT
                              @projectId AS SSO_PROJECT_ID
                             ,1 AS STEP_NO
                             ,0 AS IS_ACTIVE
                             ,1 AS IS_COMPLETE
                          UNION ALL
                          SELECT
                              @projectId
                             ,2
                             ,1
                             ,0 ) A
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.SSO_PROJECT_STEP
                                     WHERE
                                          SSO_PROJECT_ID = @projectId
                                          AND STEP_NO = A.STEP_NO )   

END
;
GO


GRANT EXECUTE ON  [dbo].[SR_RFP_LP_UPDATE_STATUS_P] TO [CBMSApplication]
GO
