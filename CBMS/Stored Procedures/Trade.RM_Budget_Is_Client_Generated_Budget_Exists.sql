SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.RM_Budget_Is_Client_Generated_Budget_Exists                        
                          
Description:                          
        To get market price and forecast pirce if a selected index   
                          
Input Parameters:                          
    Name    DataType        Default     Description                            
--------------------------------------------------------------------------------    
	@Index_Id   INT    
    @Start_Dt	Date    
	@End_Dt		Date
                          
 Output Parameters:                                
	Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
Usage Examples:                              
--------------------------------------------------------------------------------    
	SELECT * FROM dbo.ENTITY e WHERE e.ENTITY_TYPE=272

	EXEC Trade.RM_Budget_Is_Client_Generated_Budget_Exists 584,1005,291,NULL,'2020-04-01','2020-05-01','All Sites'

    
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR			2019-11-29	RM-Budgets Enahancement - Created
	RR			2020-02-13	GRM-1730 Modified @Site_Id input to varchar	                
                
******/
CREATE PROCEDURE [Trade].[RM_Budget_Is_Client_Generated_Budget_Exists]
    (
        @Index_Id INT = NULL
        , @Client_Id INT = NULL
        , @Commodity_Id INT = NULL
        , @Country_Id NVARCHAR(MAX) = NULL
        , @Start_Dt DATE
        , @End_Dt DATE
        , @Participant_Sites VARCHAR(20)
        , @Division_Id INT = NULL
        , @Site_Id VARCHAR(MAX) = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        CREATE TABLE #Participant_Sites
             (
                 Client_Hier_Id INT
             );

        DECLARE @Is_Client_Generated_Budget_Exists BIT = 0;

        INSERT INTO #Participant_Sites
             (
                 Client_Hier_Id
             )
        EXEC Trade.RM_Budget_Participant_Sites_Sel
            @Client_Id = @Client_Id
            , @Commodity_Id = @Commodity_Id
            , @Country_Id = @Country_Id
            , @Start_Dt = @Start_Dt
            , @End_Dt = @End_Dt
            , @Participant_Sites = @Participant_Sites
            , @Division_Id = @Division_Id
            , @Site_Id = @Site_Id
            , @Index_Id = @Index_Id;

        SELECT
            @Is_Client_Generated_Budget_Exists = 1
        FROM
            Trade.Rm_Budget rb
            INNER JOIN Trade.Rm_Budget_Participant rbp
                ON rbp.Rm_Budget_Id = rb.Rm_Budget_Id
            INNER JOIN #Participant_Sites ps
                ON ps.Client_Hier_Id = rbp.Client_Hier_Id
        WHERE
            rb.Is_Client_Generated = 1
            AND rb.Commodity_Id = @Commodity_Id
            AND (   rb.Budget_Start_Dt BETWEEN @Start_Dt
                                       AND     @End_Dt
                    OR  rb.Budget_End_Dt BETWEEN @Start_Dt
                                         AND     @End_Dt
                    OR  @Start_Dt BETWEEN rb.Budget_Start_Dt
                                  AND     rb.Budget_End_Dt
                    OR  @End_Dt BETWEEN rb.Budget_Start_Dt
                                AND     rb.Budget_End_Dt);

        SELECT
            @Is_Client_Generated_Budget_Exists AS Is_Client_Generated_Budget_Exists;



    END;

GO
GRANT EXECUTE ON  [Trade].[RM_Budget_Is_Client_Generated_Budget_Exists] TO [CBMSApplication]
GO
