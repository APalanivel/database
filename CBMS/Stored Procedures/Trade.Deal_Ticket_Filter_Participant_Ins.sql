SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        Trade.Deal_Ticket_Filter_Participant_Ins                      
                        
Description:                        
        
                        
Input Parameters:                        
    Name			DataType        Default     Description                          
--------------------------------------------------------------------------------  
                        
 Output Parameters:                              
 Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
     
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
 
	EXEC Trade.Deal_Ticket_Filter_Participant_Ins  
  
Author Initials:                        
    Initials    Name                        
--------------------------------------------------------------------------------  
    RR          Raghu Reddy     
                         
 Modifications:                        
    Initials	Date        Modification                        
--------------------------------------------------------------------------------  
	RR          02-02-2018  Created GRM  
                       
******/
CREATE PROCEDURE [Trade].[Deal_Ticket_Filter_Participant_Ins]
      ( 
       @Deal_Ticket_Id INT
      ,@Tvp_Deal_Ticket_Filter_Participant AS [Trade].[tvp_Deal_Ticket_Filter_Participant] READONLY
      ,@User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;
      
      INSERT      INTO Trade.Deal_Ticket_Filter_Participant
                  ( 
                   Deal_Ticket_Id
                  ,Participant_Id
                  ,Deal_Participant_Type_Cd
                  ,Created_User_Id
                  ,Created_Ts )
                  SELECT
                        @Deal_Ticket_Id
                       ,Participant_Id
                       ,Deal_Participant_Type_Cd
                       ,@User_Info_Id
                       ,GETDATE()
                  FROM
                        @Tvp_Deal_Ticket_Filter_Participant
                  GROUP BY
                        Participant_Id
                       ,Deal_Participant_Type_Cd

END;
GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Filter_Participant_Ins] TO [CBMSApplication]
GO
