SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******    
                  
NAME: [dbo].[Client_App_Access_Role_Group_Info_Map_Del_By_Client_Group]
                    
DESCRIPTION:                    
			 To Delete all the Client App Access Role and Group Map Records
                    
INPUT PARAMETERS:    
                   
Name                              DataType            Default        Description    
---------------------------------------------------------------------------------------------------------------  
@Client_Id                      INT          
@Group_Info_Id_List				VARCHAR(MAX) 
                    
OUTPUT PARAMETERS:      
                      
Name                              DataType            Default        Description    
---------------------------------------------------------------------------------------------------------------  
                    
USAGE EXAMPLES:                        
---------------------------------------------------------------------------------------------------------------   
                       
			BEGIN TRAN      
			    
            SELECT
                  ui.client_id
                 ,ui.Client_App_Access_Role_Id
                 ,ui.App_Access_Role_Name
            FROM
                  dbo.Client_App_Access_Role_Group_Info_Map uigi
                  INNER JOIN Client_App_Access_Role ui
                        ON uigi.Client_App_Access_Role_Id = ui.Client_App_Access_Role_Id
            WHERE
                  ui.client_id = 235
                  AND uigi.Group_Info_Id IN (264,349,354)


	            
			
			EXEC dbo.Client_App_Access_Role_Group_Info_Map_Del_By_Client_Group 
				@Client_Id = 235
			   ,@Group_Info_Id_List = '264,349,354'
	           
			SELECT
                  ui.client_id
                 ,ui.Client_App_Access_Role_Id
                 ,ui.App_Access_Role_Name
            FROM
                  dbo.Client_App_Access_Role_Group_Info_Map uigi
                  INNER JOIN Client_App_Access_Role ui
                        ON uigi.Client_App_Access_Role_Id = ui.Client_App_Access_Role_Id
            WHERE
                  ui.client_id = 235
                  AND uigi.Group_Info_Id IN (264,349,354)
	                            
			      
			ROLLBACK TRAN   


               
AUTHOR INITIALS:    
                  
Initials                Name    
---------------------------------------------------------------------------------------------------------------  
SP                      Sandeep Pigilam      
                     
MODIFICATIONS:   
                    
Initials                Date            Modification  
---------------------------------------------------------------------------------------------------------------  
 SP                     2014-03-19      MAINT-2677 RA User Admin -To Delete all the Client App Access Role and Group Map Records             
                   
******/         
                  
CREATE  PROCEDURE [dbo].[Client_App_Access_Role_Group_Info_Map_Del_By_Client_Group]
      ( 
       @Client_Id INT
      ,@Group_Info_Id_List VARCHAR(MAX) = NULL )
AS 
BEGIN                  
      SET NOCOUNT ON;    
      
      DECLARE
            @Client_App_Access_Role_Id INT
           ,@Group_Info_Id INT
           ,@Row_Counter INT
           ,@Total_Row_Count INT    
                  
      DECLARE @Group_Info_Id_tbl TABLE
            ( 
             Group_Info_Id INT PRIMARY KEY CLUSTERED )
              
      DECLARE @Client_App_Access_Role_Id_Tbl TABLE
            ( 
             Client_App_Access_Role_Id INT PRIMARY KEY CLUSTERED )          

      DECLARE @Client_App_Access_Role_Group_Info_Map_Del_List TABLE
            ( 
             Client_App_Access_Role_Id INT
            ,Group_Info_Id INT
            ,PRIMARY KEY CLUSTERED ( Client_App_Access_Role_Id, Group_Info_Id )
            ,Row_Num INT IDENTITY(1, 1) )                          
                
                
      INSERT  INTO @Group_Info_Id_tbl
                  ( 
                   Group_Info_Id )
                  SELECT
                        ufn.Segments
                  FROM
                        dbo.ufn_split(@Group_Info_Id_List, ',') ufn  
                     
      INSERT      INTO @Client_App_Access_Role_Id_Tbl
                  ( 
                   Client_App_Access_Role_Id )
                  SELECT
                        caar.Client_App_Access_Role_Id
                  FROM
                        dbo.Client_App_Access_Role caar
                  WHERE
                        caar.CLIENT_ID = @Client_Id


 
      INSERT      INTO @Client_App_Access_Role_Group_Info_Map_Del_List
                  ( 
                   Client_App_Access_Role_Id
                  ,Group_Info_Id )
                  SELECT
                        caargim.Client_App_Access_Role_Id
                       ,caargim.Group_Info_Id
                  FROM
                        dbo.Client_App_Access_Role_Group_Info_Map caargim
                        INNER JOIN @Client_App_Access_Role_Id_Tbl temp
                              ON caargim.Client_App_Access_Role_Id = temp.Client_App_Access_Role_Id
                        INNER JOIN @Group_Info_Id_tbl gl
                              ON gl.Group_Info_Id = caargim.GROUP_INFO_ID


      BEGIN TRY                      
            BEGIN TRANSACTION   
            
            SELECT
                  @Total_Row_Count = MAX(Row_Num)
            FROM
                  @Client_App_Access_Role_Group_Info_Map_Del_List    
                        
            SET @Row_Counter = 1     
          
            WHILE ( @Row_Counter <= @Total_Row_Count ) 
                  BEGIN            
                        SELECT
                              @Client_App_Access_Role_Id = Client_App_Access_Role_Id
                             ,@GROUP_INFO_ID = GROUP_INFO_ID
                        FROM
                              @Client_App_Access_Role_Group_Info_Map_Del_List
                        WHERE
                              Row_Num = @Row_Counter            
            
                        EXEC dbo.Client_App_Access_Role_Group_Info_Map_Del 
                              @GROUP_INFO_ID = @GROUP_INFO_ID
                             ,@Client_App_Access_Role_Id = @Client_App_Access_Role_Id

            
                        SET @Row_Counter = @Row_Counter + 1            
                  END    
                          
                                   
                               
            COMMIT TRANSACTION                               
                                 
                                 
      END TRY                  
      BEGIN CATCH                  
            IF @@TRANCOUNT > 0 
                  BEGIN  
                        ROLLBACK TRANSACTION  
                  END                 
            EXEC dbo.usp_RethrowError                  
      END CATCH                                
                         
END;





;
GO
GRANT EXECUTE ON  [dbo].[Client_App_Access_Role_Group_Info_Map_Del_By_Client_Group] TO [CBMSApplication]
GO
