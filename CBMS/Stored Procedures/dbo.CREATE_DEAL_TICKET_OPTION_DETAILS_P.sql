SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.CREATE_DEAL_TICKET_OPTION_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@optionType    	int       	          	
	@optionTypeName	varchar(200)	          	
	@siteId        	int       	          	
	@volume        	decimal(32,16)	          	
	@dealTicketDetailsId	int       	          	
	@strikePrice   	decimal(32,16)	          	
	@premiumPrice  	decimal(32,16)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE         PROCEDURE DBO.CREATE_DEAL_TICKET_OPTION_DETAILS_P 

@userId varchar,
@sessionId varchar,
@optionType int, 
@optionTypeName varchar(200), 
@siteId int, 
@volume decimal(32,16), 
@dealTicketDetailsId int, 
@strikePrice decimal(32,16), 
@premiumPrice decimal(32,16)

AS
set nocount on
DECLARE @entityId int

select @entityId = ENTITY_ID FROM ENTITY WHERE ENTITY_TYPE=@optionType AND 
ENTITY_NAME=@optionTypeName

INSERT INTO RM_DEAL_TICKET_OPTION_DETAILS 
(OPTION_TYPE_ID, SITE_ID, VOLUME, RM_DEAL_TICKET_DETAILS_ID, STRIKE_PRICE, PREMIUM_PRICE)
VALUES
(@entityId, @siteId, @volume, @dealTicketDetailsId, @strikePrice, @premiumPrice)
GO
GRANT EXECUTE ON  [dbo].[CREATE_DEAL_TICKET_OPTION_DETAILS_P] TO [CBMSApplication]
GO
