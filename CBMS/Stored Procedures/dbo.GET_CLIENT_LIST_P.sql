SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_CLIENT_LIST_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE   PROCEDURE dbo.GET_CLIENT_LIST_P
@userId varchar(10),
@sessionId varchar(20)

AS
set nocount on
	SELECT CLIENT_ID, CLIENT_NAME FROM CLIENT ORDER BY CLIENT_NAME
GO
GRANT EXECUTE ON  [dbo].[GET_CLIENT_LIST_P] TO [CBMSApplication]
GO
