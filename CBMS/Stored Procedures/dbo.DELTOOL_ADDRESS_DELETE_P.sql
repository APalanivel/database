SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.DELTOOL_ADDRESS_DELETE_P


DESCRIPTION: 


**** This procedure is called by a parent procedure.


INPUT PARAMETERS:    
      Name                 DataType          Default     Description    
--------------------------------------------------------------------------------    
	@MyAccountId            int
	@addressId              int
	@is_sv_rpl              bit                  0        Obsolete.  This was part of the linked server updates
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
--exec DELTOOL_ADDRESS_DELETE_P 1,9,0


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE DBO.DELTOOL_ADDRESS_DELETE_P
	( @MyAccountId int
	, @addressId int
	, @is_sv_rpl bit = 0
	)
AS

BEGIN
	SET NOCOUNT ON

   	delete Address
		where address_id = @addressId
		and is_primary_address = 0 -- ensure that a primary address is not deleted

	SET NOCOUNT OFF

declare @siteid int
select @siteid = address_parent_id
from address
where address_id = @addressId

exec cbmsAddress_Get @MyAccountId, @siteid

END
GO
GRANT EXECUTE ON  [dbo].[DELTOOL_ADDRESS_DELETE_P] TO [CBMSApplication]
GO
