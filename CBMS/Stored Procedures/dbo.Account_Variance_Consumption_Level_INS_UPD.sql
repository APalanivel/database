SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********   
NAME:  dbo.Account_Variance_Consumption_Level_INS_UPD  
 
DESCRIPTION:  Used to insert/update account variance consumption level

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    

    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:  
	
	Account_Variance_Consumption_Level_INS_UPD
	404,
	9,
	291

------------------------------------------------------------  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
NK   Nageswara Rao Kosuri         


Initials Date  Modification  
------------------------------------------------------------  
NK	10/21/2009  Created


******/  


CREATE PROCEDURE dbo.Account_Variance_Consumption_Level_INS_UPD
@AccountId int,
@Variance_Consumption_Level_Id int,
@CommodityId int
		
AS
BEGIN
	Declare @Existing_Variance_Consumption_Level_Id int
	SET NOCOUNT ON;	
	
	SELECT @Existing_Variance_Consumption_Level_Id = avcl.Variance_Consumption_Level_Id
				
				FROM Account_Variance_Consumption_Level avcl  
					Join Variance_Consumption_Level vcl ON vcl.Variance_Consumption_Level_Id = avcl.Variance_Consumption_Level_Id
						
				WHERE avcl.Account_Id = @AccountId
					AND vcl.Commodity_Id = @CommodityId
	IF @Existing_Variance_Consumption_Level_Id > 0
	
	BEGIN
			DELETE FROM Account_Variance_Consumption_Level 				
			WHERE ACCOUNT_ID = @AccountId
				AND Variance_Consumption_Level_Id = @Existing_Variance_Consumption_Level_Id
			
			INSERT INTO Account_Variance_Consumption_Level (  
			  ACCOUNT_ID,
			  Variance_Consumption_Level_Id
			 )    
		   VALUES (			     
			  @AccountId,
			  @Variance_Consumption_Level_Id
			 ) 
			  
	END
			
	ELSE			
	 BEGIN
			INSERT INTO Account_Variance_Consumption_Level (  
			  ACCOUNT_ID,
			  Variance_Consumption_Level_Id
			 )    
		   VALUES (			     
			  @AccountId,
			  @Variance_Consumption_Level_Id
			 ) 
	END

END
GO
GRANT EXECUTE ON  [dbo].[Account_Variance_Consumption_Level_INS_UPD] TO [CBMSApplication]
GO
