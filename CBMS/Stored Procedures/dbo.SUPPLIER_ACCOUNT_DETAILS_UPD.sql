SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:

 [DBO].[SUPPLIER_ACCOUNT_DETAILS_UPD]

DESCRIPTION:
	IF "APPLY THIS ON ALL EXISTING" IS SELECTED THEN UPDATE dbo.ALL THE VALUES FOR SELECTED CONTRACT

INPUT PARAMETERS:
NAME								DATATYPE				DEFAULT					DESCRIPTION
-------------------------------------------------------------------------------------------------
@INVOICESOURCEFLAG					BIT,
@SERVICELEVELFLAG					BIT, 
@RECALCTYPEFLAG						BIT,  
@PROCESSINGINSTRUCTIONSFLAG			BIT,          
@NOTEXPECTEDFLAG					BIT,
@NOTMANAGEDFLAG						BIT,
@RAWATCHLISTFLAG					BIT,
@WATCHLISTFLAG						BIT,
@STARTDATEFLAG						BIT,
@ENDDATEFLAG						BIT,
@INVOICE_SOURCE_TYPE_ID				INT,
@SERVICE_LEVEL_TYPE_ID				INT,          
@SUPPLIER_ACCOUNT_RECALC_TYPE_ID	INT,          
@PROCESSINGINSTRUCTIONS				VARCHAR(50),          
@NOTEXPECTED						INT,          
@NOTMANAGED							BIT,          
@NOT_EXPECTED_DT					DATETIME,          
@RAWATCHLIST						bit,          
@WATCHLIST							BIT,          
@STARTDATE							DATETIME,          
@ENDDATE							DATETIME,          
@CONTRACTID							INT,    
@VARIANCE_CONSUMPTION_LEVEL_ID		INT,    
@CONSUMPTIONLEVELFLAG				BIT,
@Is_Invoice_Posting_Blocked_Flag	BIT,
@Is_Invoice_Posting_Blocked			 BIT
    
          
OUTPUT PARAMETERS:          
NAME								DATATYPE				DEFAULT					DESCRIPTION
-------------------------------------------------------------------------------------------------
          
USAGE EXAMPLES:          
-------------------------------------------------------------------------------------------------
          
BEGIN TRAN
	EXEC ACCOUNT_DETAILS_UPD  1940,N'Not Yet Assigned',37,N'0',N'0',NULL,NULL,N'01/01/2014',N'06/30/2014',N'859',0,N'86182',N'31',N'812',N'446504',1,135,0
ROLLBACK TRAN	
          
AUTHOR INITIALS:
INITIALS	NAME
-------------------------------------------------------------------------------------------------
MGB			BHASKARAN GOPALAKRISHNAN
SP			Sandeep Pigilam

MODIFICATIONS
INITIALS DATE		MODIFICATION
-------------------------------------------------------------------------------------------------
MGB		21-AUG-09	CREATED          
HG		23-Oct-09	Contract Table join removed          
					IF LOOP logic substituted with CASE statement.          
					Subselect eliminated with explicit Join.          
MGB		03-Nov-09		included @RA_WATCH_LIST as a parameter           
HG		11/05/2009	Contract.Contract_Recalc_Type_id and Account.Supplier_Account_Recalc_Type_id column relation moved to Code/Codeset from Entity          
					Account.Supplier_Account_Recalc_Type_id renamed to Supplier_Account_Recalc_Type_Cd           
					@Supplier_Account_Recalc_Type_Id parameter renamed to @Supplier_Account_Recalc_Type_Cd          
NK		11/06/2009	commented RA Watch List               
NK		1/21/2009	Added Merge statement to update Is_Data_Entry_Only tests for Supplier Accounts.          
NK		1/22/2009	Merge statement used to insert/delete the accounts removed        
						Added code to delete the existing account dtls from Variance_Rule_Dtl_Account_Override table for the given contract_id        
MGB		6/8/2010	included acc.NOT_MANAGED to 1 or 0 based on  @NOTMANAGED      
MGB		6/12/2010	added @CONSUMPTIONLEVELFLAG and @VARIANCE_CONSUMPTION_LEVEL_ID to store VARIANCE_CONSUMPTION_LEVEL_ID when apply this to all is checked    
SSR		04/07/2011	MAINT-593 Modified the insert/delet on Variance_Rule_Dtl_Account_Override to insert/delete if either one of the flag @CONSUMPTIONLEVELFLAG / @DATAENTRYONLYFLAG is passed as true.
						Update Query moved up before Insert Query as the insert in to Variance_Rule_Dtl_Account_Override happening based on this table data.
						ACCOUNT_VARIANCE_CONSUMPTION_LEVEL included in the join clause of Insert Query which inserts the data in to Variance_Rule_Dtl_Account_Override.
SP		2014-08-06  Data Operations Enhancement Phase III,added @Is_Invoice_Posting_Blocked_Flag,@Is_Invoice_Posting_Blocked as Input parameters.
RKV     2018-08-14  Added parameter @Supplier_Account_Determinant_Source_Cd
NR		2019-04-03	Data2.0 - Removed WATCHLIST-GROUP-INFO-ID  column from Account Table.		
NR		2019-12-10	Add Contract - Saved the Supplier dates new config table. 
						
******/

CREATE PROCEDURE [dbo].[SUPPLIER_ACCOUNT_DETAILS_UPD]
    (
        @INVOICESOURCEFLAG BIT
        , @SERVICELEVELFLAG BIT
        , @RECALCTYPEFLAG BIT
        , @PROCESSINGINSTRUCTIONSFLAG BIT
        , @NOTEXPECTEDFLAG BIT
        , @NOTMANAGEDFLAG BIT
        , @WATCHLISTFLAG BIT
        , @STARTDATEFLAG BIT
        , @ENDDATEFLAG BIT
        , @INVOICE_SOURCE_TYPE_ID INT
        , @SERVICE_LEVEL_TYPE_ID INT
        , @SUPPLIER_ACCOUNT_RECALC_TYPE_CD INT
        , @PROCESSINGINSTRUCTIONS VARCHAR(6000)
        , @NOTEXPECTED INT
        , @NOTMANAGED BIT
        , @NOT_EXPECTED_Dt DATETIME
        , @WATCHLIST BIT
        , @STARTDATE DATETIME
        , @ENDDATE DATETIME
        , @CONTRACTID INT
        , @DATAENTRYONLYFLAG BIT
        , @WATCHLISTGROUPIDFLAG BIT = 0
        , @DATA_ENTRY_ONLY INT
        , @VARIANCE_CONSUMPTION_LEVEL_ID INT
        , @CONSUMPTIONLEVELFLAG BIT
        , @Is_Invoice_Posting_Blocked_Flag BIT
        , @Is_Invoice_Posting_Blocked BIT
        , @Supplier_Account_Determinant_Source_Cd INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        UPDATE
            acc
        SET
            acc.INVOICE_SOURCE_TYPE_ID = CASE WHEN @INVOICESOURCEFLAG = 1 THEN @INVOICE_SOURCE_TYPE_ID
                                             ELSE acc.INVOICE_SOURCE_TYPE_ID
                                         END
            , acc.SERVICE_LEVEL_TYPE_ID = CASE WHEN @SERVICELEVELFLAG = 1 THEN @SERVICE_LEVEL_TYPE_ID
                                              ELSE acc.SERVICE_LEVEL_TYPE_ID
                                          END
            , acc.Supplier_Account_Recalc_Type_Cd = CASE WHEN @RECALCTYPEFLAG = 1 THEN @SUPPLIER_ACCOUNT_RECALC_TYPE_CD
                                                        ELSE acc.Supplier_Account_Recalc_Type_Cd
                                                    END
            , acc.PROCESSING_INSTRUCTIONS = CASE WHEN @PROCESSINGINSTRUCTIONSFLAG = 1 THEN @PROCESSINGINSTRUCTIONS
                                                ELSE acc.PROCESSING_INSTRUCTIONS
                                            END
            , acc.NOT_EXPECTED = CASE WHEN @NOTEXPECTEDFLAG = 1 THEN @NOTEXPECTED
                                     ELSE acc.NOT_EXPECTED
                                 END
            , acc.NOT_EXPECTED_DATE = CASE WHEN @NOTEXPECTEDFLAG = 1 THEN @NOT_EXPECTED_Dt
                                          ELSE acc.NOT_EXPECTED_DATE
                                      END
            , acc.NOT_MANAGED = CASE WHEN @NOTMANAGEDFLAG = 1 THEN @NOTMANAGED
                                    ELSE acc.NOT_MANAGED
                                END
            , acc.DM_WATCH_LIST = CASE WHEN @WATCHLISTFLAG = 1 THEN @WATCHLIST
                                      ELSE acc.DM_WATCH_LIST
                                  END
            , acc.Is_Data_Entry_Only = CASE WHEN @DATAENTRYONLYFLAG = 1 THEN @DATA_ENTRY_ONLY
                                           ELSE acc.Is_Data_Entry_Only
                                       END
            , acc.Is_Invoice_Posting_Blocked = CASE WHEN @Is_Invoice_Posting_Blocked_Flag = 1 THEN
                                                        @Is_Invoice_Posting_Blocked
                                                   ELSE acc.Is_Invoice_Posting_Blocked
                                               END
            , acc.Supplier_Account_Determinant_Source_Cd = CASE WHEN @RECALCTYPEFLAG = 1 THEN
                                                                    @Supplier_Account_Determinant_Source_Cd
                                                               ELSE acc.Supplier_Account_Determinant_Source_Cd
                                                           END
        FROM
            dbo.ACCOUNT acc
            JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP Samm
                ON Samm.ACCOUNT_ID = acc.ACCOUNT_ID
        WHERE
            Samm.Contract_ID = @CONTRACTID;



        UPDATE
            sac
        SET
            sac.Supplier_Account_Begin_Dt = CASE WHEN @STARTDATEFLAG = 1 THEN @STARTDATE
                                                ELSE sac.Supplier_Account_Begin_Dt
                                            END
            , sac.Supplier_Account_End_Dt = CASE WHEN @ENDDATEFLAG = 1 THEN @ENDDATE
                                                ELSE sac.Supplier_Account_End_Dt
                                            END
        FROM
            dbo.Supplier_Account_Config sac
            INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                ON samm.ACCOUNT_ID = sac.Account_Id
                   AND  samm.Supplier_Account_Config_Id = sac.Supplier_Account_Config_Id
                   AND  samm.Contract_ID = sac.Contract_Id
        WHERE
            samm.Contract_ID = @CONTRACTID;




        DELETE
        vrdac
        FROM
            dbo.Variance_Rule_Dtl_Account_Override vrdac
            JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                ON samm.ACCOUNT_ID = vrdac.ACCOUNT_ID
        WHERE
            samm.Contract_ID = @CONTRACTID
            AND (   @CONSUMPTIONLEVELFLAG = 1
                    OR  @DATAENTRYONLYFLAG = 1);

        UPDATE
            dbo.Account_Variance_Consumption_Level
        SET
            Variance_Consumption_Level_Id = CASE WHEN @CONSUMPTIONLEVELFLAG = 1 THEN @VARIANCE_CONSUMPTION_LEVEL_ID
                                                ELSE Variance_Consumption_Level_Id
                                            END
        FROM
            dbo.Account_Variance_Consumption_Level VCL
            JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP SAMM
                ON SAMM.ACCOUNT_ID = VCL.ACCOUNT_ID
        WHERE
            SAMM.Contract_ID = @CONTRACTID;


        INSERT INTO dbo.Variance_Rule_Dtl_Account_Override
             (
                 Variance_Rule_Dtl_Id
                 , ACCOUNT_ID
             )
        SELECT
            vrdtl.Variance_Rule_Dtl_Id
            , acc.ACCOUNT_ID
        FROM
            dbo.Variance_Rule_Dtl vrdtl
            JOIN(dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                 JOIN dbo.ACCOUNT acc
                     ON acc.ACCOUNT_ID = samm.ACCOUNT_ID)
                ON ISNULL(acc.Is_Data_Entry_Only, 0) = vrdtl.Is_Data_Entry_Only
            JOIN Account_Variance_Consumption_Level VCL
                ON samm.ACCOUNT_ID = VCL.ACCOUNT_ID
                   AND  VCL.Variance_Consumption_Level_Id = vrdtl.Variance_Consumption_Level_Id
        WHERE
            samm.Contract_ID = @CONTRACTID
            AND vrdtl.IS_Active = 1
            AND (   @CONSUMPTIONLEVELFLAG = 1
                    OR  @DATAENTRYONLYFLAG = 1)
        GROUP BY
            vrdtl.Variance_Rule_Dtl_Id
            , acc.ACCOUNT_ID;

    END;



GO
GRANT EXECUTE ON  [dbo].[SUPPLIER_ACCOUNT_DETAILS_UPD] TO [CBMSApplication]
GO
