SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.GET_TOAL_NYMEX_BASIS_HEDGE_VOLUME_P
	@contract_id INT,
	@dealType VARCHAR,
	@hedgeDealType INT,
	@nymexBasis VARCHAR,
	@LPSId INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT SUM(hedge_volume) AS hvSum
	FROM dbo.hedge_detail hd INNER JOIN dbo.hedge h ON h.hedge_id=hd.hedge_id
		INNER JOIN dbo.Entity e ON e.entity_ID=h.deal_type_id			
		INNER JOIN dbo.load_Profile_specification lps ON lps.month_identifier = hd.month_identifier
	WHERE h.contract_id=@contract_id
		AND lps.load_profile_specification_id=@LPSId
		AND ((e.entity_name =@dealType OR e.entity_name=@nymexBasis)
			AND (e.entity_type=@hedgeDealType))

END

GO
GRANT EXECUTE ON  [dbo].[GET_TOAL_NYMEX_BASIS_HEDGE_VOLUME_P] TO [CBMSApplication]
GO
