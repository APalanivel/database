
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******************************************************************************************************      
NAME :  dbo.Get_CostUsage_Data_For_Account_By_Client_Energy_Type 

DESCRIPTION:   
Stored Procedure is used to get the client usage data.

INPUT PARAMETERS:      
Name             DataType               Default        Description      
--------------------------------------------------------------------   
@ClientID			INT
@EnergyType			VARCHAR(50)
@DataCaptureLevel	VARCHAR(20)
@BucketCaptureLevel VARCHAR(20)
@CostUsageKeyFields	Get_CostUsage_Key	READONLY		TVP

OUTPUT PARAMETERS:      
Name   DataType  Default Description      
--------------------------------------------------------------------      


USAGE EXAMPLES:      
--------------------------------------------------------------------      
DECLARE @CostUsageKeyFields Get_CostUsage_Key
DECLARE @BucketMasterIds tvp_Bucket
      
INSERT      INTO @BucketMasterIds
            ( Bucket_Master_Id )
VALUES
            ( 100990 ),
            ( 100991 )
          
INSERT      @CostUsageKeyFields
VALUES
            ( 1, '2012-05-01 00:00:00.000', 341739, 671268 ),
            ( 2, '2012-01-01 00:00:00.000', 340013, 669778 )

EXEC dbo.Get_CostUsage_Data_For_Account_By_Client_Energy_Type 
      @EnergyType = 'Electric Power'
     ,@BucketMasterIds = @BucketMasterIds
     ,@CostUsageKeyFields = @CostUsageKeyFields
     
AUTHOR INITIALS:      
Initials	  Name      
-------------------------------------------------------------------      
RK          Raghu Kalvapudi  
KVK			Vinay K   

MODIFICATIONS       
Initials	  Date	    Modification      
--------------------------------------------------------------------
RK		  09/05/2013  Created
KVK		  01/06/2016  modified to have client_Hier_id in JOIN clause
************************/
CREATE PROCEDURE [dbo].[Get_CostUsage_Data_For_Account_By_Client_Energy_Type]
      (
       @EnergyType VARCHAR(50)
      ,@BucketMasterIds tvp_Bucket READONLY
      ,@CostUsageKeyFields Get_CostUsage_Key READONLY )
AS
BEGIN

      DECLARE @Commodity_Id INT;
	
      SELECT
            @Commodity_Id = c.Commodity_Id
      FROM
            dbo.Commodity c
      WHERE
            c.Commodity_Name = @EnergyType; 

      DECLARE @Accounts TABLE
            (
             Account_Id INT
            ,Account_Number VARCHAR(50)
            ,Site_Id INT
            ,Site_Name VARCHAR(200)
            ,Client_Hier_Id INT
            ,Row_No INT
            ,Service_Month DATE
            ,Billing_Start_Dt DATE
            ,Billing_End_Dt DATE );


      DECLARE @Bucket_Masters TABLE
            (
             Bucket_Master_Id INT
            ,Bucket_Name VARCHAR(200)
            ,Bucket_Type VARCHAR(50)
            ,Sort_Order INT );

     
    
      INSERT      INTO @Bucket_Masters
                  (Bucket_Master_Id
                  ,Bucket_Name
                  ,Bucket_Type
                  ,Sort_Order )
                  SELECT
                        bm.Bucket_Master_Id
                       ,bm.Bucket_Name
                       ,c.Code_Value
                       ,bm.Sort_Order
                  FROM
                        @BucketMasterIds bmi
                        INNER JOIN dbo.Bucket_Master bm
                              ON bm.Bucket_Master_Id = bmi.Bucket_Master_Id
                        INNER JOIN dbo.Code c
                              ON c.Code_Id = bm.Bucket_Type_Cd; 

     
    
      INSERT      @Accounts
                  (Account_Id
                  ,Account_Number
                  ,Site_Id
                  ,Site_Name
                  ,Client_Hier_Id
                  ,Row_No
                  ,Service_Month
                  ,Billing_Start_Dt
                  ,Billing_End_Dt )
                  SELECT
                        cukf.Account_Id
                       ,a.ACCOUNT_NUMBER
                       ,ch.Site_Id
                       ,ch.Site_name
                       ,ch.Client_Hier_Id
                       ,cukf.[Row_Number]
                       ,cukf.Service_Month
                       ,cuabd.Billing_Start_Dt
                       ,cuabd.Billing_End_Dt
                  FROM
                        @CostUsageKeyFields cukf
                        INNER JOIN dbo.ACCOUNT a
                              ON a.ACCOUNT_ID = cukf.Account_Id
                        INNER JOIN dbo.Cost_Usage_Account_Billing_Dtl cuabd
                              ON a.ACCOUNT_ID = cuabd.Account_Id
                                 AND cukf.Service_Month = cuabd.Service_Month
                        INNER JOIN Core.Client_Hier ch
                              ON ch.Client_Hier_Id = cukf.Client_Hier_Id;


      SELECT
            cuad.ACCOUNT_ID
           ,a.Account_Number
           ,convert(VARCHAR(20), cuad.Service_Month, 101) Service_Month
           ,convert(VARCHAR(20), a.Billing_Start_Dt, 101) Billing_Start_Dt
           ,convert(VARCHAR(20), a.Billing_End_Dt, 101) Billing_End_Dt
           ,cuad.CURRENCY_UNIT_ID
           ,cuad.UOM_Type_Id
           ,bm.Bucket_Name
           ,bm.Bucket_Master_Id
           ,bm.Bucket_Type
           ,bm.Sort_Order
           ,cuad.Bucket_Value
           ,a.Row_No
      INTO
            #Accounts_Data
      FROM
            @Accounts a
            INNER JOIN dbo.Cost_Usage_Account_Dtl cuad
                  ON a.Account_Id = cuad.ACCOUNT_ID
                     AND cuad.Service_Month = a.Service_Month
                     AND cuad.Client_Hier_Id = a.Client_Hier_Id
            INNER JOIN @Bucket_Masters bm
                  ON bm.Bucket_Master_Id = cuad.Bucket_Master_Id;

      SELECT
            ACCOUNT_ID
           ,Account_Number
           ,convert(VARCHAR(20), Service_Month, 101) Service_Month
           ,Billing_Start_Dt
           ,Billing_End_Dt
           ,ad.CURRENCY_UNIT_ID
           ,cu.CURRENCY_UNIT_NAME
           ,ad.UOM_Type_Id
           ,e.ENTITY_NAME AS UOM_Type
           ,Bucket_Name
           ,Bucket_Master_Id
           ,Bucket_Type
           ,ad.Sort_Order
           ,ad.Bucket_Value
           ,ad.Row_No
      FROM
            #Accounts_Data ad 
            LEFT JOIN dbo.CURRENCY_UNIT cu
                  ON cu.CURRENCY_UNIT_ID = ad.CURRENCY_UNIT_ID
            LEFT JOIN dbo.ENTITY e
                  ON e.ENTITY_ID = ad.UOM_Type_Id
      ORDER BY
            Bucket_Type
           ,Bucket_Name;
            
END;


;
GO

GRANT EXECUTE ON  [dbo].[Get_CostUsage_Data_For_Account_By_Client_Energy_Type] TO [CBMSApplication]
GO
