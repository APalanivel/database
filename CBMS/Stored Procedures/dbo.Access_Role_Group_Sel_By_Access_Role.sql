SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                         
/******         
                     
 NAME: dbo.Access_Role_Group_Sel_By_Access_Role                      
                        
 DESCRIPTION:                        
        To get Groups mapped to Role from Client_App_Access_Role_Group_Info_Map table.                        
                        
 INPUT PARAMETERS:        
                       
 Name                            DataType           Default       Description        
---------------------------------------------------------------------------------------------------------------      
 @Client_App_Access_Role_Id      INT              
                        
 OUTPUT PARAMETERS:             
                        
 Name                            DataType            Default      Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:        
---------------------------------------------------------------------------------------------------------------             
                
		 EXEC dbo.Access_Role_Group_Sel_By_Access_Role 
			  @Client_App_Access_Role_Id = 4             
                     
 AUTHOR INITIALS:        
       
 Initials               Name        
---------------------------------------------------------------------------------------------------------------      
 SP                     Sandeep Pigilam          
                         
 MODIFICATIONS:      
       
 Initials               Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                     2013-12-23       Created for RA Admin user management                      
                       
******/                            
                        
CREATE	 PROCEDURE [dbo].[Access_Role_Group_Sel_By_Access_Role]
      ( 
       @Client_App_Access_Role_Id INT )
AS 
BEGIN 
                       
      SET NOCOUNT ON;             
                        
      SELECT
            caarg.GROUP_INFO_ID
           ,caarg.Client_App_Access_Role_Id
           ,gi.GROUP_NAME
           ,gi.GROUP_DESCRIPTION
           ,gic.Category_Name
           ,gi.Is_Required_For_RA_Login
      FROM
            dbo.Client_App_Access_Role_Group_Info_Map caarg
            INNER JOIN dbo.GROUP_INFO gi
                  ON caarg.GROUP_INFO_ID = gi.GROUP_INFO_ID
            INNER JOIN dbo.Group_Info_Category gic
                  ON gi.Group_Info_Category_Id = gic.Group_Info_Category_Id

      WHERE
            caarg.Client_App_Access_Role_Id = @Client_App_Access_Role_Id         
END;

;
GO
GRANT EXECUTE ON  [dbo].[Access_Role_Group_Sel_By_Access_Role] TO [CBMSApplication]
GO
