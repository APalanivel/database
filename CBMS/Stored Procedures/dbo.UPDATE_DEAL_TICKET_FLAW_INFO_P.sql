SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UPDATE_DEAL_TICKET_FLAW_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@dealTicketId  	int       	          	
	@isFlawless    	bit       	          	
	@costAssociated	decimal(32,16)	          	
	@monthOfPayment	datetime  	          	
	@comments      	varchar(4000)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE     PROCEDURE dbo.UPDATE_DEAL_TICKET_FLAW_INFO_P  

@userId varchar(10),
@sessionId varchar(20),
@dealTicketId int, 
@isFlawless bit, 
@costAssociated decimal(32,16), 
@monthOfPayment datetime, 
@comments varchar(4000)

AS
set nocount on
IF (SELECT COUNT(1) FROM RM_FLAWLESS_EXECUTION WHERE RM_DEAL_TICKET_ID = @dealTicketId) = 0
	INSERT INTO RM_FLAWLESS_EXECUTION 
	(RM_DEAL_TICKET_ID, IS_MISTAKES_DETECTED, COST_TO_SUMMIT, MONTH_DISCOVERED, 
	MONTH_TO_PAY, COMMENTS)
	VALUES
	(@dealTicketId, @isFlawless, @costAssociated, getDate(), 
	@monthOfPayment, @comments)
ELSE
	UPDATE RM_FLAWLESS_EXECUTION 
	SET 
		IS_MISTAKES_DETECTED = @isFlawless, 
		COST_TO_SUMMIT = @costAssociated, 
		MONTH_TO_PAY = @monthOfPayment, 
		COMMENTS = @comments
	WHERE	RM_DEAL_TICKET_ID = @dealTicketId
GO
GRANT EXECUTE ON  [dbo].[UPDATE_DEAL_TICKET_FLAW_INFO_P] TO [CBMSApplication]
GO
