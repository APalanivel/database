SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Account_Audit_History_Del_For_Account]  
     
DESCRIPTION: 

	To delete Account Audit history for the given Account Id.

INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Account_Id		INT						
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        
	BEGIN TRAN
		EXEC Account_Audit_History_Del_For_Account  3103
		
		--EXEC Account_Audit_History_Del_For_Account  3105
		
	ROLLBACK TRAN

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
HG			Harihara Suthan G

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
HG			06/08/2010	Created

*/

CREATE PROCEDURE dbo.Account_Audit_History_Del_For_Account
	 @Account_Id		INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE	 @Tracking_Id			INT
			,@Account_Audit_Id		INT

	DECLARE @Account_Audit_Tracking_List TABLE(Tracking_Id INT PRIMARY KEY CLUSTERED);
	DECLARE @Account_Audit_List TABLE(Account_Audit_Id INT PRIMARY KEY CLUSTERED);

	INSERT INTO @Account_Audit_List
	(
		Account_Audit_Id
	)
	SELECT
		aa.ACCOUNT_AUDIT_ID
	FROM
		dbo.ACCOUNT_AUDIT aa
	WHERE
		aa.ACCOUNT_ID = @Account_Id

	INSERT INTO @Account_Audit_Tracking_List
	(
		Tracking_Id
	)
	SELECT
		aat.TRACKING_ID
	FROM
		dbo.Account_Audit_Tracking aat
		JOIN @Account_Audit_List aal
			ON aal.Account_Audit_Id = aat.ACCOUNT_AUDIT_ID

	WHILE EXISTS(SELECT 1 FROM @Account_Audit_Tracking_List)
		BEGIN

			SET @Tracking_Id = (SELECT TOP 1 Tracking_Id FROM @Account_Audit_Tracking_List)

			EXEC dbo.Account_Audit_Tracking_Del @Tracking_Id

			DELETE
				@Account_Audit_Tracking_List
			WHERE
				Tracking_Id = @Tracking_Id

		END

	WHILE EXISTS(SELECT 1 FROM @Account_Audit_List)
		BEGIN

			SET @Account_Audit_Id = (SELECT TOP 1 Account_Audit_Id FROM @Account_Audit_List)

			EXEC dbo.Account_Audit_Del @Account_Audit_Id

			DELETE
				@Account_Audit_List
			WHERE
				Account_Audit_Id = @Account_Audit_Id

		END

END
GO
GRANT EXECUTE ON  [dbo].[Account_Audit_History_Del_For_Account] TO [CBMSApplication]
GO
