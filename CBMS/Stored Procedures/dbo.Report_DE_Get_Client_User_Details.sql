
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
    
/*********           
NAME:  dbo.Report_DE_Get_Client_User_Details          
         
DESCRIPTION:      
        
INPUT PARAMETERS:            
  Name              DataType          Default     Description            
------------------------------------------------------------            
  @Client_Name		VARCHAR(MAX)      
  @User_Name		VARCHAR(MAX)       
            
            
OUTPUT PARAMETERS:            
      Name              DataType          Default     Description            
------------------------------------------------------------            
            
USAGE EXAMPLES:       
    
exec dbo.Report_DE_Get_Client_User_Details '102,108,109,110,111,112,117,119,124,128,141,147,149,150,154,170,175,180,185,190,191,199,201,203,205,206,207,210,211,216,217,218,220,221,223,224,226,227,228,229,231,232,233,235,1003,1004,1005,1009,1010,1011,1012,

1015,1016,1017,1019,1020,1021,1022,1023,1024,1025,1026,1027,1028,1029,1030,1032,1033,1034,1035,1036,1037,1042,1043,1048,1049,1051,1052,10001,10002,10003,10004,10005,10006,10008,10009,10010,10011,10012,10014,10015,10016,10017,10019'  
    
------------------------------------------------------------      
        
AUTHOR INITIALS:          
Initials	Name          
------------------------------------------------------------          
AKR			Ashok Kumar Raju    
NR			Narayana Reddy
        
        
Initials	Date			Modification          
------------------------------------------------------------          
AKR			2012-09-04  
NR			2016-05-09		REPTMGR-40 - Added Country name column.  
     
******/    
CREATE PROCEDURE [dbo].[Report_DE_Get_Client_User_Details]
      ( 
       @Client_Name VARCHAR(MAX) )
AS 
BEGIN      
      SET NOCOUNT ON;      
      DECLARE @User_Table INT    
      DECLARE @Client_Ids_List TABLE
            ( 
             Client_Id INT PRIMARY KEY )  
      CREATE  TABLE #User_Logins
            ( 
             User_Info_Id INT
            ,Client_Id INT
            ,LAST_UPDATED DATETIME
            ,PRIMARY KEY ( User_Info_Id, Client_Id ) )      
            
          
  
  
      SELECT
            @User_Table = e.ENTITY_ID
      FROM
            dbo.ENTITY e
      WHERE
            e.ENTITY_NAME = 'USER_INFO'
            AND e.ENTITY_DESCRIPTION = 'Table_Type'    
            
      INSERT      INTO @Client_Ids_List
                  ( 
                   Client_Id )
                  SELECT
                        Segments
                  FROM
                        dbo.ufn_split(@Client_Name, ',')     
                           
      INSERT      INTO #User_Logins
                  ( 
                   User_Info_Id
                  ,Client_Id
                  ,LAST_UPDATED )
                  SELECT
                        USER_INFO_ID
                       ,Client_Id
                       ,MAX(LAST_UPDATED)
                  FROM
                        dv2.dbo.SESSION_INFO
                  WHERE
                        USER_INFO_ID IS NOT NULL
                        AND CLIENT_ID IS NOT NULL
                  GROUP BY
                        USER_INFO_ID
                       ,Client_Id  
     
      
      SELECT
            ui.First_Name AS [First Name]
           ,ui.Last_Name AS [Last Name]
           ,ui.UserName AS [User Name]
           ,c.client_name AS [Client Name]
           ,ui.EMAIL_ADDRESS AS [Email Address]
           ,[CEM Name] = LEFT(pl.pl_list, LEN(pl.pl_list) - 1)
           ,ui.locale_code [Language]
           ,CONVERT(DATE, MIN(ea.modified_date)) [Date User Added]
           ,[Last Login] = CONVERT(DATE, LAST_UPDATED)
           ,cr.COUNTRY_NAME AS [Country]
      FROM
            dbo.user_info ui
            INNER JOIN dbo.CLIENT c
                  ON ui.CLIENT_ID = c.CLIENT_ID
            INNER JOIN @Client_Ids_List cl
                  ON cl.Client_Id = c.CLIENT_ID
            INNER JOIN dbo.USER_INFO_GROUP_INFO_MAP uigim
                  ON uigim.USER_INFO_ID = ui.USER_INFO_ID
            INNER JOIN dbo.GROUP_INFO_PERMISSION_INFO_MAP gipim
                  ON gipim.GROUP_INFO_ID = uigim.GROUP_INFO_ID
            LEFT JOIN dbo.ENTITY_AUDIT ea
                  ON ea.ENTITY_IDENTIFIER = ui.USER_INFO_ID
                     AND ENTITY_ID = 1039
            CROSS APPLY ( SELECT
                              ui2.first_name + ' ' + ui2.last_name + ', '
                          FROM
                              dbo.CLIENT_CEM_MAP ccm
                              INNER JOIN dbo.USER_INFO ui2
                                    ON ccm.USER_INFO_ID = ui2.USER_INFO_ID
                          WHERE
                              c.CLIENT_ID = ccm.CLIENT_ID
                          GROUP BY
                              ui2.first_name
                             ,ui2.last_name
            FOR
                          XML PATH('') ) pl ( pl_list )
            LEFT JOIN #User_Logins si
                  ON ui.USER_INFO_ID = si.USER_INFO_ID
                     AND ui.CLIENT_ID = si.Client_Id
            LEFT JOIN dbo.COUNTRY cr
                  ON cr.COUNTRY_ID = ui.country_id
      WHERE
            ACCESS_LEVEL = 1
            AND ui.IS_DEMO_USER = 0
            AND ui.IS_HISTORY = 0
            AND NOT_MANAGED = 0
      GROUP BY
            ui.first_name
           ,ui.last_name
           ,c.client_name
           ,ui.USERNAME
           ,ui.email_address
           ,LEFT(pl.pl_list, LEN(pl.pl_list) - 1)
           ,ui.locale_code
           ,LAST_UPDATED
           ,cr.COUNTRY_NAME
      ORDER BY
            c.CLIENT_NAME
           ,ui.FIRST_NAME
           ,ui.LAST_NAME
           ,ui.USERNAME      
END;
;

;
GO


GRANT EXECUTE ON  [dbo].[Report_DE_Get_Client_User_Details] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_Get_Client_User_Details] TO [CBMSApplication]
GO
