SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[SR_RFP_LP_Account_Additional_Row_Del]  
     
DESCRIPTION: 
	It Deletes SR RFP Load Profile Account Additional Row for Selected 
						SR RFP Load Profile Account Additional Row Id.
      
INPUT PARAMETERS:          
NAME									DATATYPE	DEFAULT		DESCRIPTION
-------------------------------------------------------------------
@SR_RFP_Lp_Account_Additional_Row_Id	INT

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	Begin Tran
		EXEC SR_RFP_LP_Account_Additional_Row_Del  3636
	Rollback Tran

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			28-MAY-10	CREATED

*/

CREATE PROCEDURE dbo.SR_RFP_LP_Account_Additional_Row_Del
   (
    @SR_RFP_Lp_Account_Additional_Row_Id INT
   )
AS
BEGIN

    SET NOCOUNT ON;

    DELETE
   	FROM
		dbo.SR_RFP_LP_ACCOUNT_ADDITIONAL_ROW
	WHERE
		SR_RFP_LP_ACCOUNT_ADDITIONAL_ROW_ID = @SR_RFP_Lp_Account_Additional_Row_Id

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_LP_Account_Additional_Row_Del] TO [CBMSApplication]
GO
