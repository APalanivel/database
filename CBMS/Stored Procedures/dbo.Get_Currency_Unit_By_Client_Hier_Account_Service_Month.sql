SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******   
NAME:   
 dbo.Get_Currency_Unit_By_Client_Hier_Account_Service_Month   
DESCRIPTION:  
  
 To get first currency unit id based on given site, Account_Id and server month.  
  
INPUT PARAMETERS:   
Name					DataType Default  Description   
----------------------------------------------------------------------   
@Site_Client_Hier_Id	INT   
@Service_Month			DATE  

OUTPUT PARAMETERS:   
Name DataType Default  Description   
----------------------------------------------------------------   
USAGE EXAMPLES:   
--------------------------------------------------------------   
  
EXEC dbo.Get_Currency_Unit_By_Client_Hier_Account_Service_Month
      @Site_Client_Hier_Id = 1234
     ,@Service_Month = '2010-01-01'  
  
AUTHOR INITIALS:   
Initials	Name   
------------------------------------------------------------   
NR			Narayana Reddy  
  
MODIFICATIONS   
Initials	Date		Modification   
------------------------------------------------------------   
NR			2017-03-27	Created for MAINT-4902
HG			2020-01-09		MAINT-9751, Simplified the code to improve the performance
								- Saving the data in a table variable and returns the required results queried from the table variable
******/
CREATE PROCEDURE [dbo].[Get_Currency_Unit_By_Client_Hier_Account_Service_Month]
    (
        @Site_Client_Hier_Id INT
       ,@Service_Month       DATE
    )
AS
BEGIN

    SET NOCOUNT ON;

    DECLARE @Currency_Unit TABLE
        (
            Currency_Unit_Id INT
        );

    INSERT INTO @Currency_Unit
    (
        Currency_Unit_Id
    )
    SELECT CURRENCY_UNIT_ID
    FROM
        dbo.Cost_Usage_Account_Dtl cuad
    WHERE
        cuad.Client_Hier_ID = @Site_Client_Hier_Id
        AND cuad.Service_Month = @Service_Month;

    SELECT MAX(cuad.Currency_Unit_Id) AS Currency_Unit_Id
    FROM
        @Currency_Unit cuad;

END;
GO
GRANT EXECUTE ON  [dbo].[Get_Currency_Unit_By_Client_Hier_Account_Service_Month] TO [CBMSApplication]
GO
