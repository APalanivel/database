SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	 dbo.CU_Invoice_Changes_Sel

DESCRIPTION:
			This procedure is used to select changed records between two versions from CU_INVOICE.

INPUT PARAMETERS:
	Name						DataType		Default	Description
------------------------------------------------------------
@Last_Version_Id				BIGINT			Beginnng Change tracking Version Id (From Change_Control_Threshold) 			
@Current_Version_Id				BIGINT			Current Change tracking Version Id

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
It will execute only through the replication replacement SSIS package.

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RK			Raghu Kalvapudi
	
MODIFICATIONS
	Initials	Date			Modification
------------------------------------------------------------
	RK			07/31/2013		Created
******/

CREATE PROCEDURE [dbo].[CU_Invoice_Changes_Sel]
      ( 
       @Last_Version_Id BIGINT
      ,@Current_Version_Id BIGINT )
AS 
BEGIN
      SET NOCOUNT ON;

      SELECT
            convert(CHAR(1), cui_ct.SYS_CHANGE_OPERATION) Op_Code
           ,cui_ct.CU_INVOICE_ID
           ,cui.CBMS_IMAGE_ID
           ,cui.UBM_ID
           ,cui.UBM_INVOICE_ID
           ,cui.UBM_ACCOUNT_CODE
           ,cui.UBM_CLIENT_CODE
           ,cui.UBM_CITY
           ,cui.UBM_STATE_CODE
           ,cui.UBM_VENDOR_CODE
           ,cui.UBM_ACCOUNT_NUMBER
           ,cui.ACCOUNT_GROUP_ID
           ,cui.CLIENT_ID
           ,cui.VENDOR_ID
           ,cui.BEGIN_DATE
           ,cui.END_DATE
           ,cui.BILLING_DAYS
           ,cui.UBM_CURRENCY_CODE
           ,cui.CURRENCY_UNIT_ID
           ,cui.CURRENT_CHARGES
           ,cui.IS_DEFAULT
           ,cui.IS_PROCESSED
           ,cui.IS_REPORTED
           ,cui.IS_DNT
           ,cui.DO_NOT_TRACK_ID
           ,cui.UPDATED_BY_ID
           ,cui.UPDATED_DATE
           ,cui.UBM_INVOICE_IDENTIFIER
           ,cui.UBM_FEED_FILE_NAME
           ,cui.IS_MANUAL
           ,cui.IS_DUPLICATE
      FROM
            CHANGETABLE(CHANGES dbo.CU_INVOICE, @Last_Version_Id) cui_ct
            LEFT JOIN dbo.CU_INVOICE cui
                  ON cui.CU_INVOICE_ID = cui_ct.CU_INVOICE_ID
      WHERE
            cui_ct.SYS_Change_Version <= @Current_Version_Id
            AND ( convert(CHAR(1), cui_ct.SYS_CHANGE_OPERATION) IN ( 'D', 'U' )
                  OR ( convert(CHAR(1), cui_ct.SYS_CHANGE_OPERATION) = 'I'
                       AND cui.IS_PROCESSED = 1
                       AND cui.IS_REPORTED = 1 ) )


END

;
GO
GRANT EXECUTE ON  [dbo].[CU_Invoice_Changes_Sel] TO [ETL_Execute]
GO
