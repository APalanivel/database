SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.EC_Meter_Attribute_Del           
              
Description:              
        To delete Data to EC_Meter_Attribute table.              
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
    @EC_Meter_Attribute_Id				INT
    @User_Info_Id						INT
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                
	DECLARE  @EC_Meter_Attribute_Id_Out INT 
	BEGIN TRAN  
	SELECT * FROM dbo.EC_Meter_Attribute WHERE EC_Meter_Attribute_Name='Test_AS400_Deleted'
    EXEC dbo.EC_Meter_Attribute_Ins 
      @State_Id = 1
     ,@Commodity_Id = 100549
     ,@EC_Meter_Attribute_Name = 'Test_AS400_Deleted'
     ,@Attribute_Type_Cd = 100023
     ,@Is_Used_In_Calc_Vals=1
     ,@User_Info_Id = 100
     ,@EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_Out OUTPUT
	SELECT * FROM dbo.EC_Meter_Attribute WHERE EC_Meter_Attribute_Name='Test_AS400_Deleted'
	
	EXEC dbo.EC_Meter_Attribute_Del 
      @EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_Out
      ,@User_Info_Id=100
     
	SELECT * FROM dbo.EC_Meter_Attribute WHERE EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_Out
	SELECT * FROM dbo.Application_Audit_Log aal WHERE Application_Name='Delete Meter Attribute'
	ROLLBACK TRAN  
	          
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy               
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2015-04-22		Created For AS400.         
             
******/ 
CREATE PROCEDURE [dbo].[EC_Meter_Attribute_Del]
      ( 
       @EC_Meter_Attribute_Id INT
      ,@User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 
      
      DECLARE
            @User_Name VARCHAR(30)
           ,@Current_Ts DATETIME= GETDATE()
           ,@Lookup_Value XML
           
      SELECT
            @User_Name = FIRST_NAME + SPACE(1) + LAST_NAME
      FROM
            dbo.USER_INFO
      WHERE
            USER_INFO_ID = @User_Info_Id
            
            
      SET @Lookup_Value = ( SELECT
                              ema.EC_Meter_Attribute_Id
                             ,ema.EC_Meter_Attribute_Name
                             ,c.Commodity_Name
                             ,s.STATE_NAME
                             ,cd.Code_Value AS Attribute_Type
                            FROM
                              dbo.EC_Meter_Attribute ema
                              INNER JOIN dbo.Commodity c
                                    ON ema.Commodity_Id = c.Commodity_Id
                              INNER JOIN code cd
                                    ON cd.Code_Id = ema.Attribute_Type_Cd
                              INNER JOIN dbo.STATE s
                                    ON ema.State_Id = s.STATE_ID
                            WHERE
                              ema.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id
            FOR
                            XML RAW )
                            
            
      BEGIN TRY
            BEGIN TRAN
            
            DELETE
                  emav
            FROM
                  dbo.EC_Meter_Attribute_Value emav
            WHERE
                  emav.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id
            
      
            DELETE
                  ema
            FROM
                  dbo.EC_Meter_Attribute ema
            WHERE
                  ema.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id
            
            EXEC dbo.Application_Audit_Log_Ins 
                  @Client_Name = NULL
                 ,@Application_Name = 'Delete Meter Attribute'
                 ,@Audit_Function = -1
                 ,@Table_Name = 'EC_Meter_Attribute'
                 ,@Lookup_Value = @Lookup_Value
                 ,@Event_By_User = @User_Name
                 ,@Execution_Ts = @Current_Ts
           
            COMMIT TRAN
      END TRY
      BEGIN CATCH
            IF @@TRANCOUNT > 0 
                  BEGIN
                        ROLLBACK TRAN
                  END
		
            EXEC usp_RethrowError
      END CATCH	
   
END    

;
GO
GRANT EXECUTE ON  [dbo].[EC_Meter_Attribute_Del] TO [CBMSApplication]
GO
