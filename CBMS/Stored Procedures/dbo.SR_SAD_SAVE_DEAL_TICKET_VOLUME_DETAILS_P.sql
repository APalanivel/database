SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_SAVE_DEAL_TICKET_VOLUME_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@srDealTicketId	int       	          	
	@srDeliveryPointId	int       	          	
	@monthIdentifier	datetime  	          	
	@volume        	decimal(18,0)	          	
	@price         	decimal(18,0)	          	
	@fill_volume   	decimal(18,0)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE    PROCEDURE dbo.SR_SAD_SAVE_DEAL_TICKET_VOLUME_DETAILS_P
@srDealTicketId int,
@srDeliveryPointId int,
@monthIdentifier datetime,
@volume decimal,
@price decimal,
@fill_volume decimal

AS
set nocount on
IF (select count(1) from SR_DEAL_TICKET_VOLUME_DETAILS where SR_DEAL_TICKET_ID = @srDealTicketId and 
    MONTH_IDENTIFIER = @monthIdentifier ) = 0
BEGIN

	insert into SR_DEAL_TICKET_VOLUME_DETAILS
		(SR_DEAL_TICKET_ID, SR_DT_DELIVERY_POINT_ID, MONTH_IDENTIFIER, VOLUME, PRICE, FILL_VOLUME)
	
	values	(@srDealTicketId, @srDeliveryPointId, @monthIdentifier,	@volume, @price, @fill_volume)
END

ELSE

BEGIN

	update SR_DEAL_TICKET_VOLUME_DETAILS 
		set SR_DT_DELIVERY_POINT_ID=@srDeliveryPointId,	PRICE=@price, VOLUME=@volume,  FILL_VOLUME = @fill_volume

	where SR_DEAL_TICKET_ID = @srDealTicketId and MONTH_IDENTIFIER=@monthIdentifier

END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_SAVE_DEAL_TICKET_VOLUME_DETAILS_P] TO [CBMSApplication]
GO
