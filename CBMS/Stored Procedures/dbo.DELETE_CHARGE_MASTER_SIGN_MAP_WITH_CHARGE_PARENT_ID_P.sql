SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.DELETE_CHARGE_MASTER_SIGN_MAP_WITH_CHARGE_PARENT_ID_P
	@charge_parent_id INT
AS
BEGIN

	SET NOCOUNT ON

--	DELETE charge_master_sign_map
--	WHERE charge_master_id IN  
--		(SELECT charge_master_id FROM charge_master WHERE charge_parent_type_id = 93 AND charge_parent_id = @charge_parent_id)

	DELETE dbo.charge_master_sign_map
	FROM dbo.charge_master cm
	WHERE charge_master_sign_map.charge_master_id=cm.charge_master_id
		AND cm.charge_parent_type_id = 93
		AND cm.charge_parent_id = @charge_parent_id

END
GO
GRANT EXECUTE ON  [dbo].[DELETE_CHARGE_MASTER_SIGN_MAP_WITH_CHARGE_PARENT_ID_P] TO [CBMSApplication]
GO
