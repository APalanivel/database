SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Baseload_Del]  

DESCRIPTION: It Deletes BaseLoad for Selected Base Load Id.     
      
INPUT PARAMETERS:          
	NAME				DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Baseload_Id		INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
	EXEC Baseload_Del 5565
  Rollback Tran

  Begin Tran
	EXEC Baseload_Del 7139
  Rollback Tran

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			17-JUN-10	CREATED

*/

CREATE PROCEDURE dbo.Baseload_Del
    (
      @Baseload_Id INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.BASELOAD 
	WHERE
		BASELOAD_ID = @Baseload_Id

END
GO
GRANT EXECUTE ON  [dbo].[Baseload_Del] TO [CBMSApplication]
GO
