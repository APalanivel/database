SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
	dbo.Cu_Invoice_Get_AccountID_By_Invoice_ID
DESCRIPTION:

INPUT PARAMETERS:
	Name			DataType		Default	Description
-------------------------------------------------------------------------------------------
	@Cu_invoice_ID	INT 
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------
	EXEC Cu_Invoice_Get_AccountID_By_Invoice_ID 2928687  
	EXEC Cu_Invoice_Get_AccountID_By_Invoice_ID 3307707
	EXEC Cu_Invoice_Get_AccountID_By_Invoice_ID 3017000
	EXEC Cu_Invoice_Get_AccountID_By_Invoice_ID 3722763
	EXEC Cu_Invoice_Get_AccountID_By_Invoice_ID 3722898
	EXEC Cu_Invoice_Get_AccountID_By_Invoice_ID 35238229
	EXEC Cu_Invoice_Get_AccountID_By_Invoice_ID 35238199


	EXEC Cu_Invoice_Get_AccountID_By_Invoice_ID 81287315


	

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SSR			Sharad Srivastava
	NR			Narayana Reddy

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	SSR			04/06/2010	Created   
	SSR			05/21/2010	Mapped Vendor with Supplier Account table 
	SKA			05/31/2010	Add the join with address table (have to use Merge join)
	SSR			07/09/2010  Removed the logic of fetching data from SAMM table
							(( SAMM.meter_disassociation_date > Account.Supplier_Account_Begin_Dt  
								OR SAMM.meter_disassociation_date IS NULL ))
							(After contract enhancement application is populating both Meter_association_date and meter_disassociation_Date column supplier_Account_meter_map and removing the entries when ever is_history = 1)
	RR			2017-03-27	Replaced base tables with cliebt hier
							Contract Placeholder - Account number is returning NULL for DMO supplier accounts as end date is optional for DMO supplier
							accounts, string replaced with display account number. Open end date(NULL) handled in display account number
	NR			2019-10-01	Add Contract - Added Supplier dates filter for supplier account invoice when invoice is not resolved to month yet.
	NR			2020-01-22	MAINT- 9733 - Added Contract_Id in Output list to navigate manage contract page link purpose.
*/
CREATE PROCEDURE [dbo].[Cu_Invoice_Get_AccountID_By_Invoice_ID]
    (
        @Cu_invoice_ID INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            cism.Account_ID AS utility_account_id
            , cha.Display_Account_Number AS utility_account_number
            , ch.Site_Id AS SITE_ID
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' Site_Name
            , ch.Site_Address_Line1 AS address_line1
            , ch.Site_Address_Line2 AS address_line2
            , ch.City AS CITY
            , cha.Meter_State_Id AS STATE_ID
            , cha.Meter_State_Name AS STATE_NAME
            , cha.Account_Type AS Account_type
            , cha.Account_Vendor_Id AS VENDOR_ID
            , cha.Account_Vendor_Name AS VENDOR_NAME
            , c.ED_CONTRACT_NUMBER
            , com.Commodity_Id
            , com.Commodity_Name
            , c.CONTRACT_ID
        FROM
            dbo.CU_INVOICE inv
            JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.CU_INVOICE_ID = inv.CU_INVOICE_ID
            INNER JOIN Core.Client_Hier_Account cha
                ON cism.Account_ID = cha.Account_Id
            INNER JOIN Core.Client_Hier ch
                ON cha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.Commodity com
                ON com.Commodity_Id = cha.Commodity_Id
            LEFT JOIN dbo.CONTRACT c
                ON c.CONTRACT_ID = cha.Supplier_Contract_ID
        WHERE
            inv.CU_INVOICE_ID = @Cu_invoice_ID
            AND (   cism.SERVICE_MONTH IS NULL
                    OR  (   cha.Account_Type = 'Utility'
                            OR  (   cha.Account_Type = 'Supplier'
                                    AND (   cism.Begin_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                                          AND     cha.Supplier_Account_End_Dt
                                            OR  cism.End_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                                            AND     cha.Supplier_Account_End_Dt
                                            OR  cha.Supplier_Account_begin_Dt BETWEEN cism.Begin_Dt
                                                                              AND     cism.End_Dt
                                            OR  cha.Supplier_Account_End_Dt BETWEEN cism.Begin_Dt
                                                                            AND     cism.End_Dt))))
        GROUP BY
            cism.Account_ID
            , cha.Display_Account_Number
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
            , ch.Site_Address_Line1
            , ch.Site_Address_Line2
            , ch.City
            , cha.Meter_State_Id
            , cha.Meter_State_Name
            , cha.Account_Type
            , cha.Account_Vendor_Id
            , cha.Account_Vendor_Name
            , c.ED_CONTRACT_NUMBER
            , com.Commodity_Id
            , com.Commodity_Name
            , c.CONTRACT_ID;


    END;



GO

GRANT EXECUTE ON  [dbo].[Cu_Invoice_Get_AccountID_By_Invoice_ID] TO [CBMSApplication]
GO
