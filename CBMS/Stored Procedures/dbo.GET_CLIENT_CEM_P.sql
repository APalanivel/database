SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_CLIENT_CEM_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@client_id     	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

-- exec GET_CLIENT_CEM_P 10069


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	NR			Narayana Reddy




MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	NR			2019-10-30	Add Contract - Added Order by clause.

******/

CREATE PROCEDURE [dbo].[GET_CLIENT_CEM_P]
    (
        @client_id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            userInfo.LAST_NAME
            , userInfo.FIRST_NAME
            , userInfo.USER_INFO_ID
        FROM
            dbo.CLIENT_CEM_MAP cem
            INNER JOIN dbo.USER_INFO userInfo
                ON userInfo.USER_INFO_ID = cem.USER_INFO_ID
        WHERE
            userInfo.IS_HISTORY = 0
            AND cem.CLIENT_ID = @client_id
        ORDER BY
            userInfo.FIRST_NAME + ' ' + userInfo.LAST_NAME;

    END;

GO
GRANT EXECUTE ON  [dbo].[GET_CLIENT_CEM_P] TO [CBMSApplication]
GO
