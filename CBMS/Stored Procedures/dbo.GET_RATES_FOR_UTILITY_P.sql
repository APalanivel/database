SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.GET_RATES_FOR_UTILITY_P
	@VENDOR_ID INT,
	@RATE_ID INT,
	@COMMODITY_TYPE_ID INT
AS

  
BEGIN  
  
 	SET NOCOUNT ON

	SELECT rate_id
		, rate_name
		, rate_requirements
	FROM dbo.rate
	WHERE vendor_id =@VENDOR_ID
		AND rate_id != @RATE_ID
		AND (@commodity_type_id =0 OR commodity_type_id = @COMMODITY_TYPE_ID)
	ORDER BY rate_name
  
END

GO
GRANT EXECUTE ON  [dbo].[GET_RATES_FOR_UTILITY_P] TO [CBMSApplication]
GO
