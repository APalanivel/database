SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  procedure [dbo].[cbmsSsoProjectContract_Save]
	( @MyAccountId int
	, @project_id int
	, @contract_id int
	)
AS
BEGIN

	set nocount on

	declare @RecordCount int

	  select @RecordCount = count(*)
	    from sso_project_contract_map
	   where sso_project_id = @project_id
	     and contract_id = @contract_id

	if @RecordCount = 0
	begin

		insert into sso_project_contract_map
			(sso_project_id
			, contract_id			
			)
		values
			(@project_id
			,@contract_id
			)
	end


END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoProjectContract_Save] TO [CBMSApplication]
GO
