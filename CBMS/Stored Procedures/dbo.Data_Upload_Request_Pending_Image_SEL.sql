
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [DBO].[Data_Upload_Request_Pending_Image_SEL]  

DESCRIPTION: To get image details , for sending email

      
INPUT PARAMETERS:          
	NAME				DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@@Data_Upload_Queue_Id	int
	
    
OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
table data

USAGE EXAMPLES:
------------------------------------------------------------

	   EXEC Data_Upload_Request_Pending_Image_SEL 1775

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	AS			Arun Skaria

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	AS			19-NOV-13	CREATED

*****/
CREATE PROCEDURE [dbo].[Data_Upload_Request_Pending_Image_SEL]
      (
       @Data_Upload_Queue_Id INT )
AS
BEGIN

      SET NOCOUNT ON;

      SELECT
            ci.CBMS_IMAGE_ID
           ,ci.CBMS_IMAGE_TYPE_ID
           ,ci.CBMS_DOC_ID
           ,ci.DATE_IMAGED
           ,ci.BILLING_DAYS_ADJUSTMENT
           ,ci.CBMS_IMAGE_SIZE
           ,ci.CONTENT_TYPE
           ,ci.INV_SOURCED_IMAGE_ID
           ,ci.is_reported
           ,ci.Cbms_Image_Path
           ,ci.App_ConfigID
           ,ci.CBMS_Image_Directory
           ,ci.CBMS_Image_FileName
           ,ci.CBMS_Image_Location_Id
      FROM
            dbo.Data_Upload_Queue duq
            INNER JOIN dbo.cbms_image ci
                  ON duq.Cbms_Image_Id = ci.CBMS_IMAGE_ID
      WHERE
            duq.Data_Upload_Queue_Id = @Data_Upload_Queue_Id


END
;
GO

GRANT EXECUTE ON  [dbo].[Data_Upload_Request_Pending_Image_SEL] TO [CBMSApplication]
GO
