SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE     PROCEDURE [dbo].[cbmsCuInvoiceLabel_Save]
	( @MyAccountId int
	, @cu_invoice_label_id int = null
	, @cu_invoice_id int
	, @account_number varchar(200) = null
	, @client_name varchar(200) = null
	, @city varchar(200) = null
	, @state_name varchar(200) = null
	, @vendor_name varchar(200) = null
	)
AS
BEGIN

	declare @this_id int

	set nocount on

	set @this_id = @cu_invoice_label_id

	if @this_id is null
	begin

	   select @this_id = cu_invoice_label_id
	     from cu_invoice_label
	    where cu_invoice_id = @cu_invoice_id

	end

	if @this_id is null
	begin

		insert into cu_invoice_label
			( cu_invoice_id
			, account_number, client_name
			, city, state_name, vendor_name
			)
		values
			( @cu_invoice_id
			, @account_number, @client_name
			, @city, @state_name, @vendor_name
			)
	
		set @this_id = @@IDENTITY

	end
	else
	begin

		   update cu_invoice_label with (rowlock)
		      set account_number = @account_number
			, client_name = @client_name
			, city = @city
			, state_name = @state_name
			, vendor_name = @vendor_name
		    where cu_invoice_label_id = @this_id

	end

--	set nocount off

	exec cbmsCuInvoiceLabel_Get @MyAccountId, @this_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCuInvoiceLabel_Save] TO [CBMSApplication]
GO
