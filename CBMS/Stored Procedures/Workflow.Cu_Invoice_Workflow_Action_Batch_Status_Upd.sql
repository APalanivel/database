SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    [workflow].[Cu_Invoice_Workflow_Action_Batch_Status_Upd]
DESCRIPTION: 
------------------------------------------------------------   
 INPUT PARAMETERS:    
 Name   DataType  Default Description    
 @Cu_Invoice_Workflow_Action_Batch_Id INT,  
 @Status_Cd INT     
------------------------------------------------------------    
 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
 USAGE EXAMPLES:    
------------------------------------------------------------    
AUTHOR INITIALS:    
Initials	Name    
------------------------------------------------------------    
TRK			Ramakrishna Thummala	Summit Energy 
 
 MODIFICATIONS     
 Initials Date   Modification    
------------------------------------------------------------    
 TRK		  Sep-2019  Created

******/
CREATE  PROCEDURE [Workflow].[Cu_Invoice_Workflow_Action_Batch_Status_Upd]  
     (  
        @Cu_Invoice_Workflow_Action_Batch_Id INT,  
		@Status_Cd INT   
     )  
AS  
    BEGIN  
        SET NOCOUNT ON;  
        UPDATE  
            CIWAB  SET  
            CIWAB.Status_Cd = @Status_Cd  
            ,CIWAB.Last_Change_Ts = GETDATE()  
                     FROM workflow.Cu_Invoice_Workflow_Action_Batch  CIWAB  
        WHERE   
              CIWAB.Cu_Invoice_Workflow_Action_Batch_Id = @Cu_Invoice_Workflow_Action_Batch_Id   
    END;  
  
GO
GRANT EXECUTE ON  [Workflow].[Cu_Invoice_Workflow_Action_Batch_Status_Upd] TO [CBMSApplication]
GO
