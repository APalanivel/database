SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        Trade.Deal_Ticket_Financial_Counter_Party_Contact_Ins                    
                      
Description:                      
        
                      
Input Parameters:                      
    Name							DataType        Default     Description                        
--------------------------------------------------------------------------------
	@RM_Client_Hier_Onboard_Id		INT
    @Batch_Status_Cd				INT
    @User_Info_Id					INT
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	  
                    
Usage Examples:                          
--------------------------------------------------------------------------------

	DECLARE @Deal_Ticket_Id INT
	EXEC Trade.Deal_Ticket_Financial_Counter_Party_Contact_Ins 235,291,1,'2018-11-14','2018-11-14',1,1,1,1,1,1,1,1,1,1,1,1,1,1,@Deal_Ticket_Id OUT
	SELECT * FROM Trade.Deal_Ticket		
	SELECT @Deal_Ticket_Id
		                     
 Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy   
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			2018-11-14     Global Risk Management - Created 
                     
******/
CREATE PROCEDURE [Trade].[Deal_Ticket_Financial_Counter_Party_Contact_Ins]
      ( 
       @Client_Id INT
      ,@Deal_Ticket_Id INT
      ,@Tvp_Deal_Ticket_Financial_Counter_Party_Contact AS [Trade].[Tvp_Deal_Ticket_Financial_Counter_Party_Contact] READONLY
      ,@User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;
      
      INSERT      INTO Trade.Financial_Counterparty_Client_Contact_Map
                  ( 
                   Client_Id
                  ,RM_COUNTERPARTY_ID
                  ,Contact_Info_Id
                  ,Created_Ts )
                  SELECT
                        @Client_Id
                       ,tvp.RM_COUNTERPARTY_ID
                       ,tvp.Contact_Info_Id
                       ,GETDATE()
                  FROM
                        @Tvp_Deal_Ticket_Financial_Counter_Party_Contact tvp
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          Trade.Financial_Counterparty_Client_Contact_Map fccc
                                     WHERE
                                          fccc.Client_Id = @Client_Id
                                          AND fccc.Contact_Info_Id = tvp.Contact_Info_Id
                                          AND fccc.RM_COUNTERPARTY_ID = tvp.RM_COUNTERPARTY_ID )
     
      MERGE INTO Trade.Deal_Ticket_Financial_Counter_Party_Contact AS tgt
            USING 
                  ( SELECT
                        @Deal_Ticket_Id AS Deal_Ticket_Id
                       ,fccc.Financial_Counterparty_Client_Contact_Map_Id
                    FROM
                        @Tvp_Deal_Ticket_Financial_Counter_Party_Contact tvp
                        INNER JOIN Trade.Financial_Counterparty_Client_Contact_Map fccc
                              ON fccc.Contact_Info_Id = tvp.Contact_Info_Id
                                 AND fccc.RM_COUNTERPARTY_ID = tvp.RM_COUNTERPARTY_ID
                    WHERE
                        fccc.Client_Id = @Client_Id ) AS src
            ON tgt.Deal_Ticket_Id = src.Deal_Ticket_Id
                  AND tgt.Financial_Counterparty_Client_Contact_Map_Id = src.Financial_Counterparty_Client_Contact_Map_Id
            WHEN NOT MATCHED BY SOURCE AND  tgt.Deal_Ticket_Id = @Deal_Ticket_Id
                  THEN
							DELETE
            WHEN NOT MATCHED BY TARGET 
                  THEN
                   INSERT
                        ( 
                         Financial_Counterparty_Client_Contact_Map_Id
                        ,Deal_Ticket_Id
                        ,Created_User_Id
                        ,Created_Ts )
                    VALUES
                        ( 
                         src.Financial_Counterparty_Client_Contact_Map_Id
                        ,@Deal_Ticket_Id
                        ,@User_Info_Id
                        ,GETDATE() );  
		
     
                       
                  
END;
GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Financial_Counter_Party_Contact_Ins] TO [CBMSApplication]
GO
