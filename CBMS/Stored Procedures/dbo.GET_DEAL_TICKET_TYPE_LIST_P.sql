SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.GET_DEAL_TICKET_TYPE_LIST_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@dealType      	varchar(200)	          	
	@dealTypeId    	int       	          	
	@allDealTypes  	bit       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.GET_DEAL_TICKET_TYPE_LIST_P 'Fixed Price',268,0
	EXEC dbo.GET_DEAL_TICKET_TYPE_LIST_P 'Trigger',268,0

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/24/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on.
	
******/

CREATE PROCEDURE dbo.GET_DEAL_TICKET_TYPE_LIST_P
    @dealType		VARCHAR(200),
    @dealTypeId		INT,
    @allDealTypes	BIT
AS 
BEGIN

    SET NOCOUNT ON ;
    
    DECLARE @selectClause VARCHAR(1000)
    DECLARE @fromClause VARCHAR(1000)
    DECLARE @whereClause VARCHAR(1000)
    DECLARE @SQLStatement VARCHAR(8000)
	
    SELECT  @selectClause = ' rmdt.RM_DEAL_TICKET_ID,  rmdt.HEDGE_START_MONTH,  rmdt.HEDGE_END_MONTH,  '
            + ' c.CLIENT_NAME,  d.DIVISION_NAME,  viewSite.SITE_NAME, rmcp.COUNTERPARTY_NAME,  '
            + ' v.VENDOR_NAME,  e2.ENTITY_NAME AS HEDGE_TYPE,  e3.ENTITY_NAME AS HEDGE_LEVEL_TYPE,  '
            + ' e4.ENTITY_NAME AS TRIGGER_TYPE,  e5.ENTITY_NAME AS UNIT_TYPE, e6.ENTITY_NAME AS PRICING_REQUEST_TYPE,  '
            + ' e7.CURRENCY_UNIT_NAME AS CURRENCY_TYPE, e8.ENTITY_NAME AS CONFIRMATION_TYPE, e9.ENTITY_NAME AS DEAL_TYPE '
	
    SELECT  @fromClause = ' CLIENT c,   ENTITY e2 , ENTITY e3, '
            + ' ENTITY e4,   ENTITY e5, ENTITY e6, CURRENCY_UNIT e7, ENTITY e9,  '
            + ' RM_DEAL_TICKET rmdt   '
            + ' LEFT JOIN DIVISION d ON (rmdt.DIVISION_ID = d.DIVISION_ID)  '
            + ' LEFT JOIN SITE s ON (rmdt.SITE_ID = s.SITE_ID)  '
            + ' LEFT JOIN vwSiteName viewSite ON (s.SITE_ID = viewSite.SITE_ID) '
            + ' LEFT JOIN RM_COUNTERPARTY rmcp ON (rmdt.RM_COUNTERPARTY_ID = rmcp.RM_COUNTERPARTY_ID)	'
            + ' LEFT JOIN VENDOR v ON (rmdt.VENDOR_ID = v.VENDOR_ID)  '
            + ' LEFT JOIN ENTITY e8 ON (rmdt.CONFIRMATION_TYPE_ID = e8.ENTITY_ID) ' 

    SELECT  @whereClause = ' rmdt.CLIENT_ID = c.CLIENT_ID AND    '
            + ' rmdt.HEDGE_TYPE_ID = e2.ENTITY_ID AND   '
            + ' rmdt.HEDGE_LEVEL_TYPE_ID = e3.ENTITY_ID AND  '
            + ' rmdt.ACTION_REQUIRED_TYPE_ID = e4.ENTITY_ID AND   '
            + ' rmdt.UNIT_TYPE_ID = e5.ENTITY_ID AND   '
            + ' rmdt.PRICING_REQUEST_TYPE_ID = e6.ENTITY_ID AND   '
            + ' rmdt.CURRENCY_TYPE_ID = e7.CURRENCY_UNIT_ID AND '
            + ' rmdt.DEAL_TYPE_ID = e9.ENTITY_ID  '

    IF @allDealTypes = 0
        BEGIN
            SELECT  @fromClause = ' ENTITY e1, ' + @fromClause
            SELECT  @whereClause = @whereClause
                    + ' AND rmdt.DEAL_TYPE_ID = e1.ENTITY_ID '
                    + ' AND e1.ENTITY_NAME = ' +''''+ @dealType + ''''
                    + ' AND e1.ENTITY_TYPE = ' + STR(@dealTypeId)
        END

    SELECT  @SQLStatement = 'SELECT ' + @selectClause + 'FROM ' + @fromClause
            + 'WHERE ' + @whereClause + ' ORDER BY '
            + 'rmdt.RM_DEAL_TICKET_ID '

    EXEC ( @SQLStatement )
 
END
GO
GRANT EXECUTE ON  [dbo].[GET_DEAL_TICKET_TYPE_LIST_P] TO [CBMSApplication]
GO
