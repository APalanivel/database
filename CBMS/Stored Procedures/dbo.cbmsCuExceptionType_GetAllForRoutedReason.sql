SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsCuExceptionType_GetAllForRoutedReason

DESCRIPTION:

INPUT PARAMETERS:
	Name			DataType		Default				Description
--------------------------------------------------------------------------------
	@MyAccountId   	INT       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default				Description
--------------------------------------------------------------------------------

USAGE EXAMPLES:
--------------------------------------------------------------------------------

EXEC dbo.cbmsCuExceptionType_GetAllForRoutedReason 49

AUTHOR INITIALS:
	Initials			Name
--------------------------------------------------------------------------------
	NR					Narayana Reddy

MODIFICATIONS

	Initials	Date		Modification
--------------------------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	NR			14-08-2018	DATA2.0 - Added IS_Active Filter condition.
******/

CREATE PROCEDURE [dbo].[cbmsCuExceptionType_GetAllForRoutedReason]
    (
        @MyAccountId INT
    )
AS
    BEGIN


        SELECT
            CU_EXCEPTION_TYPE_ID
            , EXCEPTION_TYPE
            , EXCEPTION_GROUP_TYPE_ID
            , STOP_PROCESSING
        FROM
            dbo.CU_EXCEPTION_TYPE WITH (NOLOCK)
        WHERE
            IS_ROUTED_REASON = 1
            AND Is_Active = 1
        ORDER BY
            EXCEPTION_TYPE;

    END;

GO
GRANT EXECUTE ON  [dbo].[cbmsCuExceptionType_GetAllForRoutedReason] TO [CBMSApplication]
GO
