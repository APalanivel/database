
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********     
NAME:    
	dbo.CBMS_ADD_UTILITY_DETAILS_P    

DESCRIPTION:  This procedure used to insert the records in UTILITY_DETAILS table

INPUT PARAMETERS:    
      Name                        DataType          Default     Description    
---------------------------------------------------------------------------    
	@vendor_id			INT
    @website_url		VARCHAR(50)
    @pipeline			VARCHAR(50)
    @main_contact		VARCHAR(200)
    @rate_contact		VARCHAR(200)
    @billing_contact	VARCHAR(40)
    @data_contact		VARCHAR(500)
    @utility_comments	VARCHAR(1000)
    
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    

USAGE EXAMPLES:    
------------------------------------------------------------ 
BEGIN TRANSACTION

DECLARE @Vendor_Id INT
SELECT TOP 1 @Vendor_Id = vendor_Id FROM dbo.VENDOR 


EXEC CBMS_ADD_UTILITY_DETAILS_P
	@VENDOR_ID = @Vendor_id
	,@WEBSITE_URL = 'www.DBATest.Com'
	,@PIPELINE = 'DBA Test'
	,@MAIN_CONTACT = 'Chad Hattabaugh'
	,@RATE_CONTACT = 'Deana Ritter'
	,@BILLING_CONTACT = 'Chad Hattabaugh'
	,@DATA_CONTACT = 'Deana Ritter'
	,@UTILITY_COMMENTS  = 'This is a test'


SELECT * FROM dbo.Utility_Detail
WHERE Vendor_Id = @Vendor_id AND Website_url = 'www.dbatest.com'
	
ROLLBACK TRANSACTION    

Initials Name    
------------------------------------------------------------    
DR       Deana Ritter

Initials Date  Modification    
------------------------------------------------------------    
	DMR		8/4/2009	Removed Linked Server Updates
	DMR		09/10/2010	Modified for Quoted_Identifier
	SKA		01/21/2011	Modifed as we removed few columns from table
	CMH		03/01/2011 Wrapped procedure in begin end 
            
******/  
CREATE PROCEDURE [dbo].[CBMS_ADD_UTILITY_DETAILS_P]
      @VENDOR_ID INT
     ,@WEBSITE_URL VARCHAR(50)
     ,@PIPELINE VARCHAR(100)
     ,@MAIN_CONTACT VARCHAR(200)
     ,@RATE_CONTACT VARCHAR(200)
     ,@BILLING_CONTACT VARCHAR(40)
     ,@DATA_CONTACT VARCHAR(200)
     ,@UTILITY_COMMENTS VARCHAR(1000)
AS 
BEGIN 
	DECLARE @Utility_Detail_Id INT  
	  
	INSERT INTO
		  UTILITY_DETAIL
		  (
		   VENDOR_ID
		  ,WEBSITE_URL
		  ,PIPELINE
		  ,MAIN_CONTACT
		  ,RATE_CONTACT
		  ,BILLING_CONTACT
		  ,DATA_CONTACT
		  ,UTILITY_COMMENTS )
	VALUES
		  (
		   @VENDOR_ID
		  ,@WEBSITE_URL
		  ,@PIPELINE
		  ,@MAIN_CONTACT
		  ,@RATE_CONTACT
		  ,@BILLING_CONTACT
		  ,@DATA_CONTACT
		  ,@UTILITY_COMMENTS )   
	  
	  
	SELECT
		  @Utility_Detail_Id = SCOPE_IDENTITY()  
END   


GO

GRANT EXECUTE ON  [dbo].[CBMS_ADD_UTILITY_DETAILS_P] TO [CBMSApplication]
GO
