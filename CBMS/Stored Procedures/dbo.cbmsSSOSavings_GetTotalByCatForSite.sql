SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******  

NAME:  
 dbo.cbmsSSOSavings_GetTotalByCatForSite
  
DESCRIPTION:
  
 INPUT PARAMETERS:  
 Name				DataType  Default Description  
------------------------------------------------------------  
 @site_id			INT                      
  
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
 USAGE EXAMPLES:  
 ------------------------------------------------------------  
 
 EXEC dbo.cbmsSSOSavings_GetTotalByCatForSite 1697
 EXEC dbo.cbmsSSOSavings_GetTotalByCatForSite 385
   
 AUTHOR INITIALS:
 Initials	Name
------------------------------------------------------------
 PNR		Pandarinath
  
 MODIFICATIONS :
 Initials Date		 Modification  
------------------------------------------------------------  
 PNR	  04/06/2011 Replaced vwCbmsSSOSavingsOwnerFlat with SSO_SAVINGS_OWNER_MAP table.
 
******/  


CREATE PROCEDURE dbo.cbmsSSOSavings_GetTotalByCatForSite
( 
 @site_id INT )
AS 
BEGIN
  
      SELECT
            s.savings_category_type_id
           ,SUM(DISTINCT s.total_estimated_savings) AS total_estimated_savings
           ,@site_id AS site_id
           ,e.entity_name AS savings_category_type
      FROM
            dbo.sso_savings s
            JOIN dbo.entity e
                  ON s.savings_category_type_id = e.entity_id
      WHERE
            EXISTS ( SELECT
                        1
                     FROM
                        dbo.SSO_SAVINGS_OWNER_MAP ssom
                        JOIN Core.Client_Hier ch
                              ON ch.Client_Hier_Id = ssom.Client_Hier_Id
                     WHERE
                        CH.Site_Id = @site_id
                        AND s.sso_savings_id = ssom.sso_savings_id )
      GROUP BY
            s.savings_category_type_id
           ,e.entity_name
END

GO
GRANT EXECUTE ON  [dbo].[cbmsSSOSavings_GetTotalByCatForSite] TO [CBMSApplication]
GO
GO