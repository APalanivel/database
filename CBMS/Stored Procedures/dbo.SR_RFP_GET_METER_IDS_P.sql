SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_METER_IDS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@accountId     	int       	          	
	@commodityTypeId	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE    PROCEDURE dbo.SR_RFP_GET_METER_IDS_P
@accountId int,
@commodityTypeId int

AS
/*
select	m.meter_id 
from	meter m
where 	m.account_id = @accountId*/
	set nocount on
select 	m.meter_id 
from 	meter m, rate r
where 	m.account_id = @accountId
	and m.rate_id = r.rate_id
	and r.commodity_type_id = @commodityTypeId
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_METER_IDS_P] TO [CBMSApplication]
GO
