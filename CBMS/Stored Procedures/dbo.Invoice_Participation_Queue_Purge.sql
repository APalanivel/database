
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******

 NAME: dbo.Invoice_Participation_Queue_Purge

 DESCRIPTION: 
 
	Purge the Invoice_Participation_Queue
		- Insert the processed records based on the Invoice_Particiaption_Queue_Batch_Id to a Invoice_Participation_Queue_Log table
		( As soon the IP batch starts it updates the Invoice_Participation_Batch_Id in IPQ table and no way to know if the batch is completed, thus only the records processed 4 hrs before only will be considered for purging)
		- Delete the processed records from Invoice_Participation_Queue

	This sproc will be scheduled to run from tidal, will not be used by application

 INPUT PARAMETERS:
 Name							DataType		Default		Description
-------------------------------------------------------------------------------

 OUTPUT PARAMETERS:

 Name							DataType		Default		Description
-------------------------------------------------------------------------------

 USAGE EXAMPLES:
-------------------------------------------------------------------------------


BEGIN TRAN

	EXEC dbo.Invoice_Participation_Queue_Purge
	
ROLLBACK TRAN

 AUTHOR INITIALS:
 Initials			Name
-------------------------------------------------------------------------------
 HG					Harihara Suthan G


 MODIFICATIONS
 Initials			Date			Modification
-------------------------------------------------------------------------------
 HG					2013-10-14		Created
 HG                 2014-02-21      Modified to populate the @Total_Batches variable as it is not looping through all the batches
******/
CREATE PROCEDURE [dbo].[Invoice_Participation_Queue_Purge]
AS
BEGIN

      SET NOCOUNT ON

      DECLARE @Batch_To_Purge TABLE
            (
             Invoice_Participation_Batch_Id INT
            ,Batch_Num INT IDENTITY(1, 1) )

      DECLARE
            @Purge_Processed_Before_Hour INT
           ,@Purge_Processed_Before_Ts DATETIME
           ,@Total_Batches INT
           ,@Current_Batch_Num INT = 1
           ,@Invoice_Participation_Batch_Id INT

      SELECT
            @Purge_Processed_Before_Hour = App_Config_Value
      FROM
            App_Config
      WHERE
            App_Config_Cd = 'IP_Queue_Purge_Before_Hour'

      SELECT
            @Purge_Processed_Before_Ts = DATEADD(hour, -@Purge_Processed_Before_Hour, GETDATE())

      INSERT      INTO @Batch_To_Purge
                  (Invoice_Participation_Batch_Id )
                  SELECT
                        b.INVOICE_PARTICIPATION_BATCH_ID
                  FROM
                        dbo.INVOICE_PARTICIPATION_QUEUE q
                        INNER JOIN dbo.INVOICE_PARTICIPATION_BATCH b
                              ON b.INVOICE_PARTICIPATION_BATCH_ID = q.INVOICE_PARTICIPATION_BATCH_ID
                  WHERE
                        b.BATCH_DATE <= @Purge_Processed_Before_Ts
                  GROUP BY
                        b.INVOICE_PARTICIPATION_BATCH_ID
      SET @Total_Batches = @@ROWCOUNT;
      WHILE @Current_Batch_Num < = @Total_Batches
            BEGIN
                  BEGIN TRY
                        BEGIN TRAN
                        
                        SELECT
                              @Invoice_Participation_Batch_Id = Invoice_Participation_Batch_Id
                        FROM
                              @Batch_To_Purge btp
                        WHERE
                              btp.Batch_Num = @Current_Batch_Num

                        INSERT      INTO dbo.Invoice_Participation_Queue_Log
                                    ( Invoice_Participation_Queue_Id
                                    ,Event_Type
                                    ,Client_Id
                                    ,Division_Id
                                    ,Site_Id
                                    ,Account_Id
                                    ,Service_Month
                                    ,Event_By_Id
                                    ,Event_Ts
                                    ,Invoice_Participation_Batch_Id )
                                    SELECT
                                          q.INVOICE_PARTICIPATION_QUEUE_ID
                                         ,q.EVENT_TYPE
                                         ,q.CLIENT_ID
                                         ,q.DIVISION_ID
                                         ,q.SITE_ID
                                         ,q.ACCOUNT_ID
                                         ,q.SERVICE_MONTH
                                         ,q.EVENT_BY_ID
                                         ,q.EVENT_DATE
                                         ,q.INVOICE_PARTICIPATION_BATCH_ID
                                    FROM
                                          dbo.INVOICE_PARTICIPATION_QUEUE q
                                    WHERE
                                          q.INVOICE_PARTICIPATION_BATCH_ID = @Invoice_Participation_Batch_Id

                        DELETE FROM
                              dbo.INVOICE_PARTICIPATION_QUEUE
                        WHERE
                              INVOICE_PARTICIPATION_BATCH_ID = @Invoice_Participation_Batch_Id

                        SET @Current_Batch_Num = @Current_Batch_Num + 1

                        COMMIT TRAN

                  END TRY
                  BEGIN CATCH
                        IF @@TRANCOUNT > 0
                              BEGIN
                                    ROLLBACK TRAN
                              END

                        EXEC dbo.usp_RethrowError

                  END CATCH

            END

END
;
GO


GRANT EXECUTE ON  [dbo].[Invoice_Participation_Queue_Purge] TO [CBMSApplication]
GO
