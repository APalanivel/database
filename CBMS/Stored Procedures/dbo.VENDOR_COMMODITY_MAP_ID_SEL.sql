SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.VENDOR_COMMODITY_MAP_ID_SEL

 DESCRIPTION:

 INPUT PARAMETERS:
 Name				DataType  Default Description
------------------------------------------------------------
 @Vendor_Id			INT
 @Commodity_Type_Id	INT
 
 OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
 USAGE EXAMPLES:      
------------------------------------------------------------      
	EXEC VENDOR_COMMODITY_MAP_ID_SEL	717,60
	EXEC VENDOR_COMMODITY_MAP_ID_SEL	717,291
	EXEC VENDOR_COMMODITY_MAP_ID_SEL	6481,67

	SELECT * FROM Vendor_Commodity_Map

 AUTHOR INITIALS:
 Initials	Name
------------------------------------------------------------
 SKA		Shobhit Kumar Agrawal
 HG			Harihara Suthan Ganesan

 MODIFICATIONS       
 Initials	Date		Modification      
------------------------------------------------------------      
 SKA		01/12/2011	Created
 HG			04/12/2011	MAINT-607 Added this script back which was missed out to include during Summary and Highlights Enhancement.
******/
   
CREATE PROC dbo.VENDOR_COMMODITY_MAP_ID_SEL
( 
 @Vendor_Id			INT
,@Commodity_Type_Id INT )
AS
BEGIN

      SET NOCOUNT ON;

      SELECT
            VENDOR_COMMODITY_MAP_ID
      FROM
            dbo.VENDOR_COMMODITY_MAP
      WHERE
            VENDOR_ID = @Vendor_Id
            AND COMMODITY_TYPE_ID = @COMMODITY_TYPE_ID

END
GO
GRANT EXECUTE ON  [dbo].[VENDOR_COMMODITY_MAP_ID_SEL] TO [CBMSApplication]
GO
