SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
Name:    
 dbo.Cost_Usage_Account_EP_NG_Summary_Sel_For_Data_Compare  
   
 Description:    
   
 Input Parameters:    
    Name       DataType      Default Description    
------------------------------------------------------------------------    
    @Site_Id      INT  
    @Account_Id   INT  
    @Commodity_Id     INT  
    @Report_Year     SMALLINT  
    @UOM_Type_Id     INT  
    @Currency_Unit_Id   INT  
   
 Output Parameters:    
 Name  Datatype  Default Description    
------------------------------------------------------------    
   
 Usage Examples:  
------------------------------------------------------------    
    EXEC dbo.Cost_Usage_Account_EP_NG_Summary_Sel_For_Data_Compare 27,1, 290, 2010, 12, 3  
    EXEC dbo.Cost_Usage_Account_EP_NG_Summary_Sel_For_Data_Compare 27,1, 291, 2010, 25, 3  
    EXEC dbo.Cost_Usage_Account_EP_NG_Summary_Sel_For_Data_Compare 2036,6381,291,2004,25,3  
   
 SELECT * FROM ACcount WHERE ACcount_Id = 1  
   
Author Initials:    
 Initials   Name  
------------------------------------------------------------  
 AP    Athmaram Pabbathi  
 BCH    Balaraju  
  
 Modifications :    
 Initials   Date     Modification    
------------------------------------------------------------    
 AP    10/04/2011  Created   
 AP    10/11/2011  Added Start_Month column in the result set  
 AP    11/16/2011  Added Bucket_Type_Cd in the result set  
 AP    2011-12-15  Modified the SP to use Calendar Year instead of Fiscal Year  BCH    2012-04-12  Modified the sp to get Uom_Name.    
******/
CREATE   PROCEDURE dbo.Cost_Usage_Account_EP_NG_Summary_Sel_For_Data_Compare
      ( 
       @Site_Id INT
      ,@Account_Id INT
      ,@Commodity_Id INT
      ,@Report_Year SMALLINT
      ,@UOM_Type_Id INT
      ,@Currency_Unit_Id INT )
AS 
BEGIN  
  
      SET NOCOUNT ON  
                   
      DECLARE
            @Begin_Date DATETIME
           ,@End_Date DATETIME
           ,@Calendar_Year_Start_Month DATE
           ,@Client_Id INT
           ,@Currency_Group_Id INT
           ,@Commodity_Name VARCHAR(200)
           ,@Currency_Unit_Name VARCHAR(200)  
  
      DECLARE @Service_Month TABLE
            ( 
             Service_Month DATE
            ,Month_Number SMALLINT )  
  
      DECLARE @Bucket_List TABLE
            ( 
             Bucket_Master_Id INT PRIMARY KEY CLUSTERED
            ,Bucket_Name VARCHAR(255)
            ,Default_UOM_Type_Id INT
            ,Bucket_Type VARCHAR(25)
            ,Bucket_Type_Cd INT )  
  
      SELECT
            @Client_Id = CH.Client_Id
           ,@Currency_Group_id = CH.Client_Currency_Group_Id
      FROM
            CBMS.Core.Client_Hier CH
      WHERE
            CH.Site_Id = @Site_Id  
  
  
      SELECT
            @Commodity_Name = COM.Commodity_Name
      FROM
            CBMS.dbo.Commodity COM
      WHERE
            COM.Commodity_Id = @Commodity_Id  
              
      SELECT
            @Currency_Unit_Name = cu.Currency_Unit_Name
      FROM
            dbo.Currency_Unit cu
      WHERE
            cu.Currency_Unit_Id = @Currency_Unit_Id  
              
/* Load the Months into table variable*/  
      INSERT      INTO @Service_Month
                  ( 
                   Service_Month
                  ,Month_Number )
                  SELECT
                        dd.Date_D AS Service_Month
                       ,row_number() OVER ( ORDER BY dd.Date_D ) Month_Number
                  FROM
                        CBMS.meta.Date_Dim dd
                  WHERE
                        dd.Year_Num = @Report_Year  
  
      SET @Calendar_Year_Start_Month = convert(DATE, '1/1/' + convert(VARCHAR(4), @Report_Year))  
  
      INSERT      INTO @Bucket_List
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Default_UOM_Type_Id
                  ,Bucket_Type
                  ,Bucket_Type_Cd )
                  SELECT
                        BM.Bucket_Master_Id
                       ,BM.Bucket_Name
                       ,BM.Default_Uom_Type_Id
                       ,bt.Code_Value
                       ,bt.Code_Id
                  FROM
                        dbo.Bucket_Master BM
                        JOIN dbo.Commodity com
                              ON com.Commodity_Id = bm.Commodity_Id
                        JOIN dbo.Code bt
                              ON bt.Code_Id = bm.Bucket_Type_Cd
                  WHERE
                        BM.Commodity_Id = @Commodity_Id
                        AND ( ( com.Commodity_Name = 'Electric Power'
                                AND ( ( bt.Code_Value = 'Determinant'
                                        AND bm.Bucket_Name IN ( 'Total Usage', 'On-Peak Usage', 'Off-Peak Usage', 'Other-Peak Usage', 'Demand', 'Billed Demand', 'Contract Demand' ) )
                                      OR ( bt.Code_Value = 'Charge'
                                           AND bm.Bucket_Name IN ( 'Total Cost', 'Taxes' ) ) ) )
                              OR ( com.Commodity_Name = 'Natural Gas'
                                   AND ( bt.Code_Value = 'Determinant'
                                         AND bm.Bucket_Name = 'Total Usage' )
                                   OR ( bt.Code_Value = 'Charge'
                                        AND bm.Bucket_Name IN ( 'Total Cost', 'Taxes' ) ) ) )
                        AND BM.Is_Shown_on_Account = 1
                        AND BM.Is_Active = 1 ;
  
      IF @Commodity_Name = 'Electric Power' 
            BEGIN  
                  WITH  CTE_Cost_Usage
                          AS ( SELECT
                                    unpvt.Category AS Bucket_Name
                                   ,unpvt.Value AS Bucket_Value
                                   ,unpvt.Service_Month
                                   ,unpvt.Account_Id
                                   ,unpvt.Month_Number
                                   ,unpvt.EL_UNIT_OF_MEASURE_TYPE_ID
                                   ,( case WHEN unpvt.Category IN ( 'Total Usage', 'On-Peak Usage', 'Off-Peak Usage', 'Other-Peak Usage', 'Demand', 'Billed Demand', 'Contract Demand' ) THEN 'Determinant'
                                           WHEN unpvt.Category IN ( 'Total Cost', 'Taxes' ) THEN 'Charge'
                                      END ) AS Bucket_Type
                               FROM
                                    ( SELECT
                                          CU.Account_Id
                                         ,SM.Service_Month
                                         ,SM.Month_Number
                                         ,@UOM_Type_Id AS EL_UNIT_OF_MEASURE_TYPE_ID
                                         ,CU.EL_Usage * UOMConv.Conversion_Factor AS [Total Usage]
                                         ,CU.EL_On_Peak_Usage * UOMConv.Conversion_Factor AS [On-Peak Usage]
                                         ,CU.EL_Off_Peak_Usage * UOMConv.Conversion_Factor AS [Off-Peak Usage]
                                         ,CU.EL_Int_Peak_Usage * UOMConv.Conversion_Factor AS [Other-Peak Usage]
                                         ,CU.EL_Actual_Demand * Actual_UOMConv.Conversion_Factor AS [Demand]
                                         ,CU.EL_Billing_Demand * Billed_UOMConv.Conversion_Factor AS [Billed Demand]
                                         ,CU.EL_Contract_Demand * Contract_UOMConv.Conversion_Factor AS [Contract Demand]
                                         ,CU.EL_Cost * CurConv.Conversion_Factor AS [Total Cost]
                                         ,CU.EL_Tax * CurConv.Conversion_Factor AS [Taxes]
                                      FROM
                                          CBMS.dbo.Cost_Usage CU
                                          INNER JOIN @Service_Month SM
                                                ON SM.Service_Month = CU.Service_Month
                                          INNER JOIN dbo.Currency_Unit_Conversion CurConv
                                                ON CurConv.Base_Unit_Id = CU.Currency_Unit_Id
                                                   AND CurConv.Conversion_Date = SM.Service_Month
                                                   AND CurConv.Currency_Group_Id = @Currency_Group_Id
                                                   AND CurConv.Converted_Unit_Id = @Currency_Unit_Id
                                          INNER JOIN dbo.Consumption_Unit_Conversion UOMConv
                                                ON UOMConv.Base_Unit_Id = CU.EL_Unit_Of_Measure_Type_Id
                                                   AND UOMConv.Converted_Unit_Id = @UOM_Type_Id
                                          INNER JOIN dbo.Consumption_Unit_Conversion Actual_UOMConv
                                                ON Actual_UOMConv.Base_Unit_Id = CU.EL_Unit_Of_Measure_Type_Id
                                                   AND Actual_UOMConv.Converted_Unit_Id = @UOM_Type_Id
                                          INNER JOIN dbo.Consumption_Unit_Conversion Billed_UOMConv
                                                ON Billed_UOMConv.Base_Unit_Id = CU.EL_Unit_Of_Measure_Type_Id
                                                   AND Billed_UOMConv.Converted_Unit_Id = @UOM_Type_Id
                                          INNER JOIN dbo.Consumption_Unit_Conversion Contract_UOMConv
                                                ON Contract_UOMConv.Base_Unit_Id = CU.EL_Unit_Of_Measure_Type_Id
                                                   AND Contract_UOMConv.Converted_Unit_Id = @UOM_Type_Id
                                      WHERE
                                          CU.Account_Id = @Account_Id
                                          AND cu.Site_Id = @SIte_Id
                                          AND CU.System_Row = 0 ) UNP UNPIVOT ( Value FOR Category IN ( [Total Usage], [On-Peak Usage], [Off-Peak Usage], [Other-Peak Usage], [Demand], [Billed Demand], [Contract Demand], [Total Cost], [Taxes] ) ) AS unpvt)
                        SELECT
                              BL.Bucket_Name
                             ,BL.Bucket_Master_Id
                             ,BL.Bucket_Type_Cd
                             ,case WHEN cu.Bucket_Type = 'Determinant'
                                        AND BL.Bucket_Name = 'Demand' THEN 'KW'
                                   WHEN CU.Bucket_Type = 'Determinant'
                                        AND BL.Bucket_Name <> 'Demand' THEN Entity_Type.Entity_Name
                                   WHEN CU.Bucket_Type = 'Charge' THEN @Currency_Unit_Name
                              END AS Uom_Name
                             ,max(case WHEN CU.Month_Number = 1 THEN CU.Bucket_Value
                                  END) AS Month1
                             ,max(case WHEN CU.Month_Number = 2 THEN CU.Bucket_Value
                                  END) AS Month2
                             ,max(case WHEN CU.Month_Number = 3 THEN CU.Bucket_Value
                                  END) AS Month3
                             ,max(case WHEN CU.Month_Number = 4 THEN CU.Bucket_Value
                                  END) AS Month4
                             ,max(case WHEN CU.Month_Number = 5 THEN CU.Bucket_Value
                                  END) AS Month5
                             ,max(case WHEN CU.Month_Number = 6 THEN CU.Bucket_Value
                                  END) AS Month6
                             ,max(case WHEN CU.Month_Number = 7 THEN CU.Bucket_Value
                                  END) AS Month7
                             ,max(case WHEN CU.Month_Number = 8 THEN CU.Bucket_Value
                                  END) AS Month8
                             ,max(case WHEN CU.Month_Number = 9 THEN CU.Bucket_Value
                                  END) AS Month9
                             ,max(case WHEN CU.Month_Number = 10 THEN CU.Bucket_Value
                                  END) AS Month10
                             ,max(case WHEN CU.Month_Number = 11 THEN CU.Bucket_Value
                                  END) AS Month11
                             ,max(case WHEN CU.Month_Number = 12 THEN CU.Bucket_Value
                                  END) AS Month12
                             ,sum(CU.Bucket_Value) AS Total
                             ,@Calendar_Year_Start_Month AS Start_Month
                        FROM
                              @Bucket_List BL
                              LEFT OUTER JOIN CTE_Cost_Usage CU
                                    ON CU.Bucket_Name = BL.Bucket_Name
                                       AND CU.Bucket_Type = bl.Bucket_Type
                              LEFT OUTER JOIN dbo.ENTITY Entity_Type
                                    ON Entity_Type.ENTITY_Id = CU.EL_UNIT_OF_MEASURE_TYPE_ID
                        GROUP BY
                              BL.Bucket_Name
                             ,BL.Bucket_Master_Id
                             ,BL.Bucket_Type_Cd
                             ,case WHEN cu.Bucket_Type = 'Determinant'
                                        AND BL.Bucket_Name = 'Demand' THEN 'KW'
                                   WHEN CU.Bucket_Type = 'Determinant'
                                        AND BL.Bucket_Name <> 'Demand' THEN Entity_Type.ENTITY_NAME
                                   WHEN CU.Bucket_Type = 'Charge' THEN @Currency_Unit_Name
                              END
                        ORDER BY
                              BL.Bucket_Name  
            END  
      ELSE 
            IF @Commodity_Name = 'Natural Gas' 
                  BEGIN  
                        WITH  CTE_Cost_Usage
                                AS ( SELECT
                                          unpvt.Category AS Bucket_Name
                                         ,unpvt.Value AS Bucket_Value
                                         ,unpvt.Service_Month
                                         ,unpvt.Account_Id
                                         ,unpvt.Month_Number
                                         ,unpvt.NG_UNIT_OF_MEASURE_TYPE_ID
                                         ,( case WHEN unpvt.Category = 'Total Usage' THEN 'Determinant'
                                                 WHEN unpvt.Category IN ( 'Total Cost', 'Taxes' ) THEN 'Charge'
                                            END ) AS Bucket_Type
                                     FROM
                                          ( SELECT
                                                CU.Account_Id
                                               ,SM.Service_Month
                                               ,SM.Month_Number
                                               ,@UOM_Type_Id AS NG_UNIT_OF_MEASURE_TYPE_ID
                                               ,CU.NG_Usage * UOMConv.Conversion_Factor AS [Total Usage]
                                               ,CU.NG_Cost * CurConv.Conversion_Factor AS [Total Cost]
                                               ,CU.NG_Tax * CurConv.Conversion_Factor AS [Taxes]
                                            FROM
                                                dbo.Cost_Usage CU
                                                INNER JOIN @Service_Month SM
                                                      ON SM.Service_Month = CU.Service_Month
                                                INNER JOIN dbo.Currency_Unit_Conversion CurConv
                                                      ON CurConv.Base_Unit_Id = CU.Currency_Unit_Id
                                                         AND CurConv.Conversion_Date = SM.Service_Month
                                                         AND CurConv.Currency_Group_Id = @Currency_Group_Id
                                                         AND CurConv.Converted_Unit_Id = @Currency_Unit_Id
                                                INNER JOIN dbo.Consumption_Unit_Conversion UOMConv
                                                      ON UOMConv.Base_Unit_Id = CU.NG_Unit_oF_Measure_Type_Id
                                                         AND UOMConv.Converted_Unit_Id = @UOM_Type_Id
                                            WHERE
                                                CU.Account_Id = @Account_Id
                                                AND CU.System_Row = 0 ) UNP UNPIVOT ( Value FOR Category IN ( [Total Usage], [Total Cost], [Taxes] ) ) AS unpvt)
                              SELECT
                                    BL.Bucket_Name
                                   ,BL.Bucket_Master_Id
                                   ,BL.Bucket_Type_Cd
                                   ,case WHEN CU.Bucket_Type = 'CHARGE' THEN @Currency_Unit_Name
                                         ELSE Entity_Type.ENTITY_NAME
                                    END AS Uom_Name
                                   ,max(case WHEN CU.Month_Number = 1 THEN CU.Bucket_Value
                                        END) AS Month1
                                   ,max(case WHEN CU.Month_Number = 2 THEN CU.Bucket_Value
                                        END) AS Month2
                                   ,max(case WHEN CU.Month_Number = 3 THEN CU.Bucket_Value
                                        END) AS Month3
                                   ,max(case WHEN CU.Month_Number = 4 THEN CU.Bucket_Value
                                        END) AS Month4
                                   ,max(case WHEN CU.Month_Number = 5 THEN CU.Bucket_Value
                                        END) AS Month5
                                   ,max(case WHEN CU.Month_Number = 6 THEN CU.Bucket_Value
                                        END) AS Month6
                                   ,max(case WHEN CU.Month_Number = 7 THEN CU.Bucket_Value
                                        END) AS Month7
                                   ,max(case WHEN CU.Month_Number = 8 THEN CU.Bucket_Value
                                        END) AS Month8
                                   ,max(case WHEN CU.Month_Number = 9 THEN CU.Bucket_Value
                                        END) AS Month9
                                   ,max(case WHEN CU.Month_Number = 10 THEN CU.Bucket_Value
                                        END) AS Month10
                                   ,max(case WHEN CU.Month_Number = 11 THEN CU.Bucket_Value
                                        END) AS Month11
                                   ,max(case WHEN CU.Month_Number = 12 THEN CU.Bucket_Value
                                        END) AS Month12
                                   ,sum(CU.Bucket_Value) AS Total
                                   ,@Calendar_Year_Start_Month AS Start_Month
                              FROM
                                    @Bucket_List BL
                                    LEFT OUTER JOIN CTE_Cost_Usage CU
                                          ON CU.Bucket_Name = BL.Bucket_Name
                                             AND CU.Bucket_Type = bl.Bucket_Type
                                    LEFT OUTER  JOIN dbo.ENTITY Entity_Type
                                          ON Entity_Type.ENTITY_Id = CU.NG_UNIT_OF_MEASURE_TYPE_ID
                              GROUP BY
                                    BL.Bucket_Name
                                   ,BL.Bucket_Master_Id
                                   ,BL.Bucket_Type_Cd
                                   ,case WHEN CU.Bucket_Type = 'CHARGE' THEN @Currency_Unit_Name
                                         ELSE Entity_Type.ENTITY_NAME
                                    END
                              ORDER BY
                                    BL.Bucket_Name  
                  END  
  
END  
;
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_EP_NG_Summary_Sel_For_Data_Compare] TO [CBMSApplication]
GO
