SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.DELETE_MARKET_PRICE_POINT_STATE_MAP_P
@marketPricePointID int
AS
begin
delete from market_price_point_state_map where market_price_point_id = @marketPricePointID
end




GO
GRANT EXECUTE ON  [dbo].[DELETE_MARKET_PRICE_POINT_STATE_MAP_P] TO [CBMSApplication]
GO
