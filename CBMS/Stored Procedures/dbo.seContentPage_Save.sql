SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure dbo.seContentPage_Save
	(
		@ContentPageId int = null
	,	@PageLabel varchar(50)
	)
As
BEGIN
	set nocount on
	declare @ThisId int


	set @ThisId = @ContentPageId
	
	if @ThisId is null
	begin
	
		select @ThisId = ContentPageId
		  from seContentPage
		 where PageLabel = @PageLabel

	end

	if @ThisId is null
	begin

		insert into seContentPage
			( PageLabel
			)
		values
			( @PageLabel
			)

		set @ThisId = @@IDENTITY

	end
	else
	begin
	
		update seContentPage
		   set PageLabel = @PageLabel
		 where ContentPageId = @ThisId
	
	end
	
	exec seContentPage_Get @ThisId

END
GO
GRANT EXECUTE ON  [dbo].[seContentPage_Save] TO [CBMSApplication]
GO
