SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Site_Sel_Is_IDM_Enabled_By_Site_Id

DESCRIPTION:
	
	To get the Performance Analytics status of a site

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------  	          	
	@site_id       	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Site_Sel_Is_IDM_Enabled_By_Site_Id 1921

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RMG			Rani Mary George

MODIFICATIONS
	Initials	Date			Modification
------------------------------------------------------------
	RMG			2015-01-13	     Created 
******/

CREATE PROCEDURE [dbo].[Site_Sel_Is_IDM_Enabled_By_Site_Id] ( @site_id INT )
AS 
BEGIN
	
      SET NOCOUNT ON;
	
      SELECT
            case WHEN Is_IDM_Enabled = 1 THEN 'Yes'
                 ELSE 'No'
            END AS performance_analytics_status
      FROM
            dbo.SITE s
      WHERE
            s.site_id = @site_id

END
GO
GRANT EXECUTE ON  [dbo].[Site_Sel_Is_IDM_Enabled_By_Site_Id] TO [CBMSApplication]
GO
