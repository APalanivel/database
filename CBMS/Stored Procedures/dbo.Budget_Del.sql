SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Budget_Del]  
     
DESCRIPTION: 
	To Delete Budget for Selected Budget Id.
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Budget_Id		INT			
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        
  
	EXEC dbo.Budget_Del 0
	
	SELECT * FROM Budget b WHERE NOT EXISTS(SELECT 1 FROM Budget_Account ba WHERE ba.Budget_id = b.Budget_id)
	
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR			PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR			08-JUNE-10	CREATED     
*/  

CREATE PROCEDURE dbo.Budget_Del
    (
      @Budget_Id INT
    )
AS 
BEGIN

    SET NOCOUNT ON ;
   
    DELETE 
	FROM	
		dbo.BUDGET
	WHERE 
		BUDGET_ID = @Budget_Id

END
GO
GRANT EXECUTE ON  [dbo].[Budget_Del] TO [CBMSApplication]
GO
