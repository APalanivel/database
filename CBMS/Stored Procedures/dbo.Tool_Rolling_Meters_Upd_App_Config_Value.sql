SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
	 dbo.Tool_Rolling_Meters_Upd_App_Config_Value    
    
DESCRIPTION:    
    
 Rolling_Meters.
     
    
INPUT PARAMETERS:    
 Name					DataType	 Default		Description    
--------------------------------------------------------------------    
 
    
OUTPUT PARAMETERS:    
 Name					DataType	 Default		Description    
--------------------------------------------------------------------    
    
USAGE EXAMPLES:    
--------------------------------------------------------------------    
SELECT
    *
FROM
    dbo.App_Config ac
WHERE
    ac.App_Config_Cd = 'Add_Contract_Rolling_Meter'
	
	
	EXEC dbo.Tool_Rolling_Meters_Upd_App_Config_Value

 
AUTHOR INITIALS:    
	Initials		Name    
--------------------------------------------------------------------    
    NR				Narayana Reddy       
    
MODIFICATIONS    
    
 Initials		Date			Modification    
--------------------------------------------------------------------    
 NR				2019-05-30		Created For - Add Contract Project.
  
******/

CREATE PROC [dbo].[Tool_Rolling_Meters_Upd_App_Config_Value]
AS
    SET NOCOUNT ON;

    BEGIN


        UPDATE
            ac
        SET
            ac.App_Config_Value = CONVERT(VARCHAR(100), GETDATE(), 23)
        FROM
            dbo.App_Config ac
        WHERE
            ac.App_Config_Cd = 'Add_Contract_Rolling_Meter';



    END;
GO
GRANT EXECUTE ON  [dbo].[Tool_Rolling_Meters_Upd_App_Config_Value] TO [CBMSApplication]
GO
