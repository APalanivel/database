SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--exec dbo.SR_RFP_GET_EXPECTED_DETERMINANT_VALUE_COUNT_P 1297
--exec dbo.SR_RFP_GET_DETERMINANT_VALUE_COUNT_P 1297
-- select * from account where site_id = 10308
-- select * from sr_rfp_account where sr_rfp_id = 251
-- select * from sr_rfp_load_profile_setup where sr_rfp_account_id = 1405
-- select * from sr_rfp_load_profile_determinant where sr_rfp_load_profile_setup_id = 836
--update sr_rfp_load_profile_determinant set is_checked = 0 where sr_rfp_load_profile_determinant_id in(1449, 1470)
-- select * from sr_rfp_lp_determinant_values where sr_rfp_load_profile_determinant_id = 892 and reading_type_id in (1077, 1078)
-- select * from entity where entity_type = 1031
CREATE    PROCEDURE dbo.SR_RFP_GET_DETERMINANT_VALUE_COUNT_P
	@rfp_account_id int
	AS

		declare @actual_read_type_id int, @actual_avg_read_type_id int
		
		select @actual_read_type_id = entity_id from entity(nolock) where entity_type = 1031 and entity_name = 'Actual LP Value'
		select @actual_avg_read_type_id = entity_id from entity(nolock) where entity_type = 1031 and entity_name = 'Actual Avg LP Value'

		select  determinant.determinant_name as determinant_name,
			determinant.sr_rfp_load_profile_determinant_id as determinant_id,
			count(value.sr_rfp_lp_determinant_values_id) as actual_count
		from 	sr_rfp_load_profile_setup setup(nolock),
			sr_rfp_account rfp_account(nolock),
			sr_rfp_load_profile_determinant determinant(nolock) 
			left join sr_rfp_lp_determinant_values value (nolock)
			on value.sr_rfp_load_profile_determinant_id = determinant.sr_rfp_load_profile_determinant_id
			and value.lp_value is not null
			and value.reading_type_id in(@actual_read_type_id, @actual_avg_read_type_id)  

		where	setup.sr_rfp_account_id = @rfp_account_id
			and rfp_account.sr_rfp_account_id = setup.sr_rfp_account_id
			and rfp_account.is_deleted = 0
			and determinant.sr_rfp_load_profile_setup_id = setup.sr_rfp_load_profile_setup_id
			and determinant.is_checked = 1
		group by determinant.determinant_name,
			determinant.sr_rfp_load_profile_determinant_id
		
		order by determinant.sr_rfp_load_profile_determinant_id
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_DETERMINANT_VALUE_COUNT_P] TO [CBMSApplication]
GO
