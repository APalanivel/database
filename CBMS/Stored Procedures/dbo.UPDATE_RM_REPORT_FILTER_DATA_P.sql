SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.UPDATE_RM_REPORT_FILTER_DATA_P
@userId varchar(10),
@sessionId varchar(10),
@reportId int,
@inputParameterTypeId int,
@inputParameter int


as
	set nocount on
insert into RM_REPORT_FILTER_DATA(RM_REPORT_ID,INPUT_PARAMETER_TYPE_ID,INPUT_PARAMETER) values (@reportId,@inputParameterTypeId,@inputParameter)
GO
GRANT EXECUTE ON  [dbo].[UPDATE_RM_REPORT_FILTER_DATA_P] TO [CBMSApplication]
GO
