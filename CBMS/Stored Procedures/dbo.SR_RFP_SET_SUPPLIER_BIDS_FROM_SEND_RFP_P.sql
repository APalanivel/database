
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	dbo.SR_RFP_SET_SUPPLIER_BIDS_FROM_SEND_RFP_P

DESCRIPTION: 


INPUT PARAMETERS:    
    Name    DataType          Default     Description    
---------------------------------------------------------------------------------    
	@rfpId	int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRANSACTION
		EXEC dbo.SR_RFP_SET_SUPPLIER_BIDS_FROM_SEND_RFP_P 13258
	ROLLBACK TRANSACTION

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DR			Deana Ritter
	RR			Raghu Reddy
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	DR			08/04/2009	Removed Linked Server Updates
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2016-03-14	Global Sourcing - Phase3 - GCS-524 - Modified "RFP sent to supplier status" entity values

******/
CREATE PROCEDURE [dbo].[SR_RFP_SET_SUPPLIER_BIDS_FROM_SEND_RFP_P] @rfpId INT
AS 
BEGIN
      SET NOCOUNT ON

      DECLARE @isDataFound INT
      DECLARE @summitBidId INT
      DECLARE @supplierCommentsId INT
      DECLARE @supplierServiceId INT
      DECLARE @newProductName VARCHAR(200)
      DECLARE @newIsSubProduct BIT
      DECLARE @deliveryFuelId INT
      DECLARE @balancingToleranceId INT
      DECLARE @riskManagementId INT
      DECLARE @alternateFuelId INT
      DECLARE @pricingScopeId INT
      DECLARE @miscPowerId INT
      DECLARE @newSummitBidId INT
      DECLARE @newSupplierCommentsId INT
      DECLARE @newSupplierServiceId INT
      DECLARE @newDeliveryFuelId INT
      DECLARE @newBalancingToleranceId INT
      DECLARE @newRiskManagementId INT
      DECLARE @newAlternateFuelId INT
      DECLARE @newPricingScopeId INT
      DECLARE @newMiscPowerId INT
      DECLARE @newBidId INT
      DECLARE @newProductTermMapId INT


      BEGIN 
            DECLARE
                  @accountTermsId INT
                 ,@supplierContactId INT
                 ,@supplierBOCId INT
                 ,@selectedProductsId INT

            DECLARE SET_BIDS CURSOR FAST_FORWARD
            FOR
            SELECT
                  map.sr_rfp_account_term_id
                 ,supplier.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                 ,map.SR_RFP_BID_ID
                 ,map.SR_RFP_SELECTED_PRODUCTS_ID
            FROM
                  SR_RFP_SEND_SUPPLIER supplier
                 ,SR_RFP_ACCOUNT_TERM term
                 ,SR_RFP_TERM_PRODUCT_MAP map
                 ,SR_RFP_ACCOUNT account
            WHERE
                  supplier.sr_account_group_id = term.sr_account_group_id
                  AND term.is_bid_group = 0
                  AND supplier.is_bid_group = term.is_bid_group
                  AND map.sr_rfp_account_term_id = term.sr_rfp_account_term_id
                  AND term.is_sop IS NULL
                  AND map.sr_rfp_supplier_contact_vendor_map_id IS NULL
                  AND account.sr_rfp_account_id = term.sr_account_group_id
                  AND account.sr_rfp_bid_group_id IS NULL
                  AND account.sr_rfp_id = @rfpId
                  AND account.is_deleted = 0
                  AND supplier.status_type_id = ( SELECT
                                                      entity_id
                                                  FROM
                                                      entity
                                                  WHERE
                                                      entity_type = 1013
                                                      AND entity_name = 'Post' )
            UNION
            SELECT
                  map.sr_rfp_account_term_id
                 ,supplier.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                 ,map.SR_RFP_BID_ID
                 ,map.SR_RFP_SELECTED_PRODUCTS_ID
            FROM
                  SR_RFP_SEND_SUPPLIER supplier
                 ,SR_RFP_ACCOUNT_TERM term
                 ,SR_RFP_TERM_PRODUCT_MAP map
                 ,SR_RFP_ACCOUNT account
            WHERE
                  supplier.sr_account_group_id = term.sr_account_group_id
                  AND term.is_bid_group = 1
                  AND supplier.is_bid_group = term.is_bid_group
                  AND map.sr_rfp_account_term_id = term.sr_rfp_account_term_id
                  AND term.is_sop IS NULL
                  AND map.sr_rfp_supplier_contact_vendor_map_id IS NULL
                  AND account.sr_rfp_bid_group_id = term.sr_account_group_id
                  AND account.sr_rfp_id = @rfpId
                  AND account.is_deleted = 0
                  AND supplier.status_type_id = ( SELECT
                                                      entity_id
                                                  FROM
                                                      entity
                                                  WHERE
                                                      entity_type = 1013
                                                      AND entity_name = 'Post' )


            OPEN SET_BIDS
            FETCH NEXT FROM SET_BIDS INTO @accountTermsId, @supplierContactId, @supplierBOCId, @selectedProductsId
            WHILE ( @@fetch_status <> -1 ) 
                  BEGIN
                        SELECT
                              @isDataFound = ( SELECT
                                                count(*)
                                               FROM
                                                SR_RFP_TERM_PRODUCT_MAP map
                                               WHERE
                                                map.sr_rfp_account_term_id = @accountTermsId
                                                AND map.sr_rfp_supplier_contact_vendor_map_id = @supplierContactId
                                                AND map.sr_rfp_selected_products_id = @selectedProductsId )

                        IF ( @@fetch_status <> -2
                             AND @isDataFound = 0 ) 
                              BEGIN
			

                                    SELECT
                                          @summitBidId = SR_RFP_BID_REQUIREMENTS_ID
                                    FROM
                                          SR_RFP_BID
                                    WHERE
                                          SR_RFP_BID_ID = @supplierBOCId
						
                                    PRINT '@supplierBOCId' + str(@supplierBOCId)
                                    PRINT '@summitBidId' + str(@summitBidId)
			
	
                                    INSERT      INTO SR_RFP_BID_REQUIREMENTS
                                                ( 
                                                 DELIVERY_POINT
                                                ,TRANSPORTATION_LEVEL_TYPE_ID
                                                ,NOMINATION_TYPE_ID
                                                ,BALANCING_TYPE_ID
                                                ,COMMENTS
                                                ,DELIVERY_POINT_POWER_TYPE_ID
                                                ,SERVICE_LEVEL_POWER_TYPE_ID )
                                                SELECT
                                                      DELIVERY_POINT
                                                     ,TRANSPORTATION_LEVEL_TYPE_ID
                                                     ,NOMINATION_TYPE_ID
                                                     ,BALANCING_TYPE_ID
                                                     ,COMMENTS
                                                     ,DELIVERY_POINT_POWER_TYPE_ID
                                                     ,SERVICE_LEVEL_POWER_TYPE_ID
                                                FROM
                                                      SR_RFP_BID_REQUIREMENTS
                                                WHERE
                                                      SR_RFP_BID_REQUIREMENTS_ID = @summitBidId
				
                                    SELECT
                                          @newSummitBidId = scope_identity()


                                    INSERT      INTO SR_RFP_SUPPLIER_PRICE_COMMENTS
                                                ( 
                                                 IS_CREDIT_APPROVAL
                                                ,NO_CREDIT_COMMENTS
                                                ,PRICE_RESPONSE_COMMENTS )
                                    VALUES
                                                ( 
                                                 NULL
                                                ,NULL
                                                ,NULL )
			
                                    SELECT
                                          @newSupplierCommentsId = scope_identity()

                                    INSERT      INTO SR_RFP_DELIVERY_FUEL
                                                ( 
                                                 DELIVERY_PIPELINE_COMMENTS
                                                ,FUEL_IN_BID_TYPE_ID
                                                ,FUEL_IN_BID_VALUE
                                                ,TRANSPORT_IN_BID_TYPE_ID
                                                ,TRANSPORT_IN_BID_VALUE )
                                    VALUES
                                                ( 
                                                 NULL
                                                ,NULL
                                                ,NULL
                                                ,NULL
                                                ,NULL )
		
                                    SELECT
                                          @newDeliveryFuelId = scope_identity()

                                    INSERT      INTO SR_RFP_BALANCING_TOLERANCE
                                                ( 
                                                 IS_PRICE_VARYING
                                                ,VOLUME_TOLERANCE
                                                ,ABOVE_TOLERANCE_COMMENTS
                                                ,BELOW_TOLERANCE_COMMENTS
                                                ,BALANCING_FREQUENCY_TYPE_ID
                                                ,TOLERANCE_COMMENTS )
                                    VALUES
                                                ( 
                                                 NULL
                                                ,NULL
                                                ,NULL
                                                ,NULL
                                                ,NULL
                                                ,NULL )

                                    SELECT
                                          @newBalancingToleranceId = scope_identity()
		
                                    INSERT      INTO SR_RFP_RISK_MANAGEMENT
                                                ( 
                                                 SWAP_INDEX_NYMEX_TYPE_ID
                                                ,SWAP_INDEX_NYMEX_COMMENTS
                                                ,SWAP_CONTRACT_FIXED_TYPE_ID
                                                ,SWAP_CONTRACT_FIXED_COMMENTS
                                                ,EXPECTED_VARIANCE
                                                ,FIXED_PRICE_TRIGGER_TYPE_ID
                                                ,FIXED_PRICE_TRIGGER_COMMENTS )
                                    VALUES
                                                ( 
                                                 NULL
                                                ,NULL
                                                ,NULL
                                                ,NULL
                                                ,NULL
                                                ,NULL
                                                ,NULL )

                                    SELECT
                                          @newRiskManagementId = scope_identity()

                                    INSERT      INTO SR_RFP_ALTERNATE_FUEL
                                                ( 
                                                 BUYER_LIQUIDATE_TYPE_ID
                                                ,COMMENTS )
                                    VALUES
                                                ( 
                                                 NULL
                                                ,NULL )
		
                                    SELECT
                                          @newAlternateFuelId = scope_identity()
		
	
                                    INSERT      INTO SR_RFP_PRICING_SCOPE
                                                ( 
                                                 PRICE_INCLUDE_LOSES_TYPE_ID
                                                ,LOSS_PERCENT
                                                ,PRICE_INCLUDE_ANCILIARY_SERVICE_TYPE_ID
                                                ,ANCILIARY_SERVICE_ESTIMATED_COST
                                                ,PRICE_INCLUDE_CAPACITY_CHARGES_TYPE_ID
                                                ,CAPACITY_CHARGES_ESTIMATED_COST
                                                ,PRICE_INCLUDE_ISO_CHARGES_TYPE_ID
                                                ,ISO_CHARGES_ESTIMATED_COST
                                                ,PRICE_INCLUDE_TRANSMISSION_TYPE_ID
                                                ,TRANSMISSION_ESTIMATED_COST
                                                ,PRICE_INCLUDE_TAXES_TYPE_ID
                                                ,TAXES_ESTIMATED_COST
                                                ,OTHER_CHARGES
                                                ,DEMAND_CAP_APPLICABLE_TYPE_ID
                                                ,CAP_FLOOR_PRICE )
                                    VALUES
                                                ( 
                                                 NULL
                                                ,NULL
                                                ,NULL
                                                ,NULL
                                                ,NULL
                                                ,NULL
                                                ,NULL
                                                ,NULL
                                                ,NULL
                                                ,NULL
                                                ,NULL
                                                ,NULL
                                                ,NULL
                                                ,NULL
                                                ,NULL )

                                    SELECT
                                          @newPricingScopeId = scope_identity()
		
                                    INSERT      INTO SR_RFP_MISCELLANEOUS_POWER
                                                ( 
                                                 BLOCK_SIZES
                                                ,PRICE_INCLUDES_GREEN_ENERGY_TYPE_ID
                                                ,PERCENT_GREEN_ENERGY
                                                ,CONVERT_PRODUCT_FIXED_PRICE_TYPE_ID
                                                ,CONVERT_PRODUCT_FIXED_PRICE_COMMENTS
                                                ,BUYER_BLOCK_HEDGES_TYPE_ID
                                                ,BUYER_BLOCK_HEDGES_COMMENTS
                                                ,HEAT_PRICING_TRIGGERS_TYPE_ID
                                                ,HEAT_PRICING_TRIGGERS_COMMENTS )
                                    VALUES
                                                ( 
                                                 NULL
                                                ,NULL
                                                ,NULL
                                                ,NULL
                                                ,NULL
                                                ,NULL
                                                ,NULL
                                                ,NULL
                                                ,NULL )
		
                                    SELECT
                                          @newMiscPowerId = scope_identity()
		

                                    IF @newDeliveryFuelId = ''
                                          OR @newDeliveryFuelId IS NULL 
                                          BEGIN
                                                SELECT
                                                      @newDeliveryFuelId = NULL
                                          END
	
                                    IF @newBalancingToleranceId = ''
                                          OR @newBalancingToleranceId IS NULL 
                                          BEGIN
                                                SELECT
                                                      @newBalancingToleranceId = NULL
                                          END
		
	
                                    IF @newRiskManagementId = ''
                                          OR @newRiskManagementId IS NULL 
                                          BEGIN
                                                SELECT
                                                      @newRiskManagementId = NULL
                                          END
	
                                    IF @newAlternateFuelId = ''
                                          OR @newAlternateFuelId IS NULL 
                                          BEGIN
                                                SELECT
                                                      @newAlternateFuelId = NULL
                                          END
	
                                    IF @newPricingScopeId = ''
                                          OR @newPricingScopeId IS NULL 
                                          BEGIN
                                                SELECT
                                                      @newPricingScopeId = NULL
                                          END
	
                                    IF @newMiscPowerId = ''
                                          OR @newMiscPowerId IS NULL 
                                          BEGIN
                                                SELECT
                                                      @newMiscPowerId = NULL
                                          END
	
	
                                    IF @newDeliveryFuelId IS NOT NULL
                                          OR @newBalancingToleranceId IS NOT NULL
                                          OR @newRiskManagementId IS NOT NULL
                                          OR @newAlternateFuelId IS NOT NULL
                                          OR @newPricingScopeId IS NOT NULL
                                          OR @newMiscPowerId IS NOT NULL 
                                          BEGIN
	
                                                INSERT      INTO SR_RFP_SUPPLIER_SERVICE
                                                            ( 
                                                             SR_RFP_DELIVERY_FUEL_ID
                                                            ,SR_RFP_BALANCING_TOLERANCE_ID
                                                            ,SR_RFP_RISK_MANAGEMENT_ID
                                                            ,SR_RFP_ALTERNATE_FUEL_ID
                                                            ,SR_RFP_PRICING_SCOPE_ID
                                                            ,SR_RFP_MISCELLANEOUS_POWER_ID )
                                                VALUES
                                                            ( 
                                                             @newDeliveryFuelId
                                                            ,@newBalancingToleranceId
                                                            ,@newRiskManagementId
                                                            ,@newAlternateFuelId
                                                            ,@newPricingScopeId
                                                            ,@newMiscPowerId )
                                                SELECT
                                                      @newSupplierServiceId = scope_identity()

                                          END
	

                                    IF @newSupplierServiceId = ''
                                          OR @newSupplierServiceId IS NULL 
                                          BEGIN
                                                SELECT
                                                      @newSupplierServiceId = NULL
                                          END
	
	
                                    INSERT      INTO SR_RFP_BID
                                                ( 
                                                 PRODUCT_NAME
                                                ,SR_RFP_BID_REQUIREMENTS_ID
                                                ,SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
                                                ,SR_RFP_SUPPLIER_SERVICE_ID
                                                ,IS_SUB_PRODUCT )
                                    VALUES
                                                ( 
                                                 @newProductName
                                                ,@newSummitBidId
                                                ,@newSupplierCommentsId
                                                ,@newSupplierServiceId
                                                ,0 )

                                    SELECT
                                          @newBidId = scope_identity()
		
                                    INSERT      INTO SR_RFP_TERM_PRODUCT_MAP
                                                ( 
                                                 SR_RFP_ACCOUNT_TERM_ID
                                                ,SR_RFP_SELECTED_PRODUCTS_ID
                                                ,SR_RFP_BID_ID
                                                ,SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                                ,BID_DATE )
                                    VALUES
                                                ( 
                                                 @accountTermsId
                                                ,@selectedProductsId
                                                ,@newBidId
                                                ,@supplierContactId
                                                ,NULL )
                                    SELECT
                                          @newProductTermMapId = scope_identity()
			
			
	
				
                              END
                        FETCH NEXT FROM SET_BIDS INTO @accountTermsId, @supplierContactId, @supplierBOCId, @selectedProductsId
                  END
            CLOSE SET_BIDS
            DEALLOCATE SET_BIDS


      END
END 

;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_SET_SUPPLIER_BIDS_FROM_SEND_RFP_P] TO [CBMSApplication]
GO
