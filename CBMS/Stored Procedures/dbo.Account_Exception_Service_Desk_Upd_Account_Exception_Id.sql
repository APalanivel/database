SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 dbo.Account_Exception_Service_Desk_Upd_Account_Exception_Id  
  
DESCRIPTION:  
  
INPUT PARAMETERS:  
 Name							DataType			Default					Description  
-------------------------------------------------------------------------------------------  
 @Account_Exception_Id			INT  
 @User_Info_Id					INT  
 @Service_Desk_Ticket_XId		NVARCHAR(100)  
  
OUTPUT PARAMETERS:  
 Name							DataType			Default					Description  
-------------------------------------------------------------------------------------------  
   
USAGE EXAMPLES:  
-------------------------------------------------------------------------------------------  
    
EXEC Account_Exception_Service_Desk_Upd_Account_Exception_Id  
    @Account_Exception_Id = 1  
    , @User_Info_Id = 1  
    , @Service_Desk_Ticket_XId = 'se21'  
  
AUTHOR INITIALS:  
 Initials		Name  
-------------------------------------------------------------------------------------------  
 NR				Narayana Reddy  
   
MODIFICATIONS  
 Initials		Date				Modification  
-------------------------------------------------------------------------------------------  
 NR				2019-07-19			Created for Add Contract.  
 NR				2020-03-02			MAINT-9912 - Added Null condition on Exception Start Dt If user is pass the Null
									Value then take already existing Exception Start Dt.
  
******/

CREATE PROCEDURE [dbo].[Account_Exception_Service_Desk_Upd_Account_Exception_Id]
    (
        @Account_Exception_Id INT
        , @User_Info_Id INT
        , @Service_Desk_Ticket_XId NVARCHAR(100)
        , @Exception_Begin_Dt DATE
        , @Exception_End_Dt DATE
        , @Exception_Status_Cd INT
    )
AS
    BEGIN

        SET NOCOUNT ON;


        UPDATE
            ae
        SET
            Service_Desk_Ticket_XId = @Service_Desk_Ticket_XId
            , ae.Last_Change_Ts = GETDATE()
            , ae.Updated_User_Id = @User_Info_Id
            , ae.Exception_Begin_Dt = ISNULL(@Exception_Begin_Dt, ae.Exception_Begin_Dt)
            , ae.Exception_End_Dt = ISNULL(@Exception_End_Dt, ae.Exception_End_Dt)
            , ae.Exception_Status_Cd = @Exception_Status_Cd
        FROM
            dbo.Account_Exception ae
        WHERE
            ae.Account_Exception_Id = @Account_Exception_Id;
    END;
    ;




GO
GRANT EXECUTE ON  [dbo].[Account_Exception_Service_Desk_Upd_Account_Exception_Id] TO [CBMSApplication]
GO
