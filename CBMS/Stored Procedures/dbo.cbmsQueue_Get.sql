SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





create  PROCEDURE [dbo].[cbmsQueue_Get]
	( @MyAccountId int
	, @queue_id int
	)
AS
BEGIN

	   select QUEUE_ID
			, QUEUE_TYPE_ID
	     from QUEUE
	    where QUEUE_ID = @queue_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsQueue_Get] TO [CBMSApplication]
GO
