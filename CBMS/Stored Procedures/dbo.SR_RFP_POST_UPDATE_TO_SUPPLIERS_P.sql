SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_POST_UPDATE_TO_SUPPLIERS_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
	@user_id varchar(10),
	@session_id varchar(20),
	@account_group_id int,
	@is_bid_group bit,
	@contact_vendor_map_id int,
	@comments varchar(4000),
	@email_log_id int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
--exec dbo.SR_RFP_POST_UPDATE_TO_SUPPLIERS_P
--	@user_id =1,
--	@session_id =-1,
--	@account_group_id =31,
--	@is_bid_group =1,
--	@contact_vendor_map_id =13,
--	@comments ='Test Data',
--	@email_log_id = 39

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_POST_UPDATE_TO_SUPPLIERS_P
	@user_id                varchar(10),
	@session_id             varchar(20),
	@account_group_id       int,
	@is_bid_group           bit,
	@contact_vendor_map_id  int,
	@comments               varchar(4000),
	@email_log_id           int
	AS
	
set nocount on	

	INSERT INTO SR_RFP_UPDATE_SUPPLIER(
										sr_account_group_id, 
										is_bid_group, 
										sr_rfp_supplier_contact_vendor_map_id, 
										comments, 
										sr_rfp_email_log_id
										)
								VALUES(
										@account_group_id, 
										@is_bid_group, 
										@contact_vendor_map_id, 
										@comments, 
										@email_log_id
										)
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_POST_UPDATE_TO_SUPPLIERS_P] TO [CBMSApplication]
GO
