SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_TERM_DATES_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@rfp_id        	int       	          	
	@ACCOUNT_GROUP_ID	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--select * from sr_rfp_sop_details


--exec SR_RFP_GET_TERM_DATES_P 70,646

--select * from  sr_rfp_account_term where sr_account_group_id =646

CREATE       PROCEDURE dbo.SR_RFP_GET_TERM_DATES_P

	@rfp_id int,
	@ACCOUNT_GROUP_ID int 

	AS
set nocount on
declare @isgroup int
declare @account int
set @isgroup = 0
set @account = @ACCOUNT_GROUP_ID

select @isgroup = sr_rfp_bid_group_id from sr_rfp_account where sr_rfp_account_id = @ACCOUNT_GROUP_ID and is_deleted = 0

if @isgroup > 0
begin
set @account = @isgroup
end


	select	from_month,
		to_month
		 	 
	from 	sr_rfp_account_term account_term(nolock),
		sr_rfp_term term(nolock),
		sr_rfp_sop_details details
	
	where	term.sr_rfp_id = @rfp_id
		and account_term.sr_rfp_term_id = term.sr_rfp_term_id
		and account_term.SR_ACCOUNT_GROUP_ID = @account and
		account_term.is_sop=1 and
		details.sr_rfp_account_term_id=account_term.sr_rfp_account_term_id and
		details.is_recommended=1
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_TERM_DATES_P] TO [CBMSApplication]
GO
