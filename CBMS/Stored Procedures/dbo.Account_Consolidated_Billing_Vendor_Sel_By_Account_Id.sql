SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
NAME:   dbo.Account_Consolidated_Billing_Vendor_Sel_By_Account_Id  
             
DESCRIPTION:               
   To get consolidated billing configurations  
     
INPUT PARAMETERS:              
 Name    DataType Default  Description    
---------------------------------------------------------------------------------    
 @Account_Id   INT  
  
  
OUTPUT PARAMETERS:  
 Name        DataType  Default  Description    
---------------------------------------------------------------------------------    
  
 USAGE EXAMPLES:  
---------------------------------------------------------------------------------    

  EXEC dbo.Account_Consolidated_Billing_Vendor_Sel_By_Account_Id 29085  
   
   
   
    
 AUTHOR INITIALS:              
 Initials	Name              
-------------------------------------------------------------              
 NR			Narayana Reddy
  
 MODIFICATIONS:  
 Initials	Date			Modification  
------------------------------------------------------------  
 NR			2019-09-26		Add Contract  - Created.
******/

CREATE PROCEDURE [dbo].[Account_Consolidated_Billing_Vendor_Sel_By_Account_Id]
     (
         @Account_Id INT
     )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Supplier_Vendor_Type_Id INT;
        SELECT
            @Supplier_Vendor_Type_Id = e.ENTITY_ID
        FROM
            dbo.ENTITY e
        WHERE
            e.ENTITY_NAME = 'Supplier'
            AND e.ENTITY_TYPE = 155
            AND e.ENTITY_DESCRIPTION = 'Vendor';



        SELECT
            cbv.Account_Id
            , MIN(cbv.Billing_Start_Dt) AS Billing_Start_Dt
            , MAX(cbv.Billing_End_Dt) AS Billing_End_Dt
        FROM
            dbo.Account_Consolidated_Billing_Vendor cbv
        WHERE
            cbv.Account_Id = @Account_Id
            AND cbv.Invoice_Vendor_Type_Id = @Supplier_Vendor_Type_Id
        GROUP BY
            cbv.Account_Id;





    END;

GO
GRANT EXECUTE ON  [dbo].[Account_Consolidated_Billing_Vendor_Sel_By_Account_Id] TO [CBMSApplication]
GO
