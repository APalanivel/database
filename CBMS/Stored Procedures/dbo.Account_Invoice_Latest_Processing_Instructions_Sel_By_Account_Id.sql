SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:                  
        dbo.Account_Invoice_Latest_Processing_Instructions_Sel_By_Account_Id                    
                  
Description:                  
			Showing Latest Procssing instructions.                  
                  
 Input Parameters:                  
 Name                         DataType        Default        Description                    
--------------------------------------------------------------------------                  
 @Account_Id				  INT

                  
 Output Parameters:                        
 Name                         DataType        Default        Description                    
--------------------------------------------------------------------------                  
                  
 Usage Examples:                      
--------------------------------------------------------------------------                  


	 EXEC dbo.Account_Invoice_Latest_Processing_Instructions_Sel_By_Account_Id
		   @Account_Id = 1644607
		  

                 
 Author Initials:                  
  Initials        Name                  
--------------------------------------------------------------------------                  
  NR              Narayana Reddy    
                   
 Modifications:                  
  Initials        Date              Modification                  
--------------------------------------------------------------------------                  
  NR              2019-01-03		Created for Data2.0 - Watch List - B.                
                 
******/

CREATE PROCEDURE [dbo].[Account_Invoice_Latest_Processing_Instructions_Sel_By_Account_Id]
     (
         @Account_Id INT
         , @Start_Index INT = 1
         , @End_Index INT = 2147483647
         , @Sort_Col VARCHAR(50) = NULL
     )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Start_Dt DATE = DATEADD(mm, DATEDIFF(mm, 0, DATEADD(m, 0, GETDATE())), 0)
            , @End_Dt DATE = DATEADD(s, -1, DATEADD(mm, DATEDIFF(mm, 0, GETDATE()) + 1, 0));

        SET @Sort_Col = ISNULL(@Sort_Col, 'Config_Start_Dt DESC');

        WITH Cte_Processing_Config
        AS (
               SELECT
                    aipc.Account_Invoice_Processing_Config_Id
                    , c.Code_Value AS Instruction_Category
                    , aipc.Processing_Instruction
                    , aipc.Config_Start_Dt
                    , aipc.Config_End_Dt
                    , aipc.Watch_List_Group_Info_Id
                    , gi.GROUP_NAME
                    , aipc.Last_Change_Ts
                    , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Updated_By
                    , COUNT(1) OVER () Total_Count
                    , ROW_NUMBER() OVER (ORDER BY (CASE WHEN @Sort_Col = 'GROUP_NAME ASC' THEN gi.GROUP_NAME
                                                       WHEN @Sort_Col = 'Instruction_Category ASC' THEN c.Code_Value
                                                       WHEN @Sort_Col = 'Updated_User ASC' THEN
                                                           ui.FIRST_NAME + ' ' + ui.LAST_NAME
                                                       WHEN @Sort_Col = 'Processing_Instruction ASC' THEN
                                                           aipc.Processing_Instruction
                                                   END) ASC
                                                  , (CASE WHEN @Sort_Col = 'GROUP_NAME DESC' THEN gi.GROUP_NAME
                                                         WHEN @Sort_Col = 'Instruction_Category DESC' THEN c.Code_Value
                                                         WHEN @Sort_Col = 'Updated_User DESC' THEN
                                                             ui.FIRST_NAME + ' ' + ui.LAST_NAME
                                                         WHEN @Sort_Col = 'Processing_Instruction DESC' THEN
                                                             aipc.Processing_Instruction
                                                     END) DESC
                                                  , (CASE WHEN @Sort_Col = 'Config_Start_Dt ASC' THEN
                                                              aipc.Config_Start_Dt
                                                         WHEN @Sort_Col = 'Config_End_Dt ASC' THEN aipc.Config_End_Dt
                                                         WHEN @Sort_Col = 'Last_Change_Ts ASC' THEN aipc.Last_Change_Ts
                                                     END) ASC
                                                  , (CASE WHEN @Sort_Col = 'Config_Start_Dt DESC' THEN
                                                              aipc.Config_Start_Dt
                                                         WHEN @Sort_Col = 'Config_End_Dt DESC' THEN aipc.Config_End_Dt
                                                         WHEN @Sort_Col = 'Last_Change_Ts DESC' THEN
                                                             aipc.Last_Change_Ts
                                                     END) DESC) AS Row_Num
               FROM
                    dbo.Account_Invoice_Processing_Config aipc
                    INNER JOIN dbo.Code c
                        ON aipc.Processing_Instruction_Category_Cd = c.Code_Id
                    INNER JOIN dbo.USER_INFO ui
                        ON ui.USER_INFO_ID = aipc.Updated_User_Id
                    LEFT JOIN dbo.GROUP_INFO gi
                        ON gi.GROUP_INFO_ID = aipc.Watch_List_Group_Info_Id
               WHERE
                    aipc.Is_Active = 1
                    AND aipc.Account_Id = @Account_Id
                    AND (   aipc.Config_Start_Dt BETWEEN @Start_Dt
                                                 AND     @End_Dt
                            OR  ISNULL(aipc.Config_End_Dt, '9999-12-31') BETWEEN @Start_Dt
                                                                         AND     @End_Dt
                            OR  @Start_Dt BETWEEN aipc.Config_Start_Dt
                                          AND     ISNULL(aipc.Config_End_Dt, '9999-12-31')
                            OR  @End_Dt BETWEEN aipc.Config_Start_Dt
                                        AND     ISNULL(aipc.Config_End_Dt, '9999-12-31'))
           )
        SELECT
            cp.Account_Invoice_Processing_Config_Id
            , cp.Instruction_Category
            , cp.Processing_Instruction
            , cp.Config_Start_Dt
            , cp.Config_End_Dt
            , cp.Watch_List_Group_Info_Id
            , cp.GROUP_NAME
            , cp.Last_Change_Ts
            , cp.Updated_By
            , cp.Row_Num
            , cp.Total_Count
        FROM
            Cte_Processing_Config cp
        WHERE
            cp.Row_Num BETWEEN @Start_Index
                       AND     @End_Index
        ORDER BY
            cp.Row_Num;


    END;



GO
GRANT EXECUTE ON  [dbo].[Account_Invoice_Latest_Processing_Instructions_Sel_By_Account_Id] TO [CBMSApplication]
GO
