
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
 dbo.SR_RFP_GET_ACCOUNT_INFO_POST_TO_SUPPLIER_P

DESCRIPTION:
  

INPUT PARAMETERS:
	Name				DataType		Default			Description
-------------------------------------------------------------------------
 @user_id				VARCHAR(10)
 @session_id			VARCHAR(20)
 @rfp_account_group_id	INT
 @is_bid_group			BIT       	          	

OUTPUT PARAMETERS:
	Name				DataType		Default			Description
-------------------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------------------
 EXEC dbo.SR_RFP_GET_ACCOUNT_INFO_POST_TO_SUPPLIER_P
      @user_id = 49
     ,@session_id = -1
     ,@rfp_account_group_id = 827
     ,  -- 817, 2576
      @is_bid_group = 1 
      
      
  EXEC   SR_RFP_GET_ACCOUNT_INFO_POST_TO_SUPPLIER_P null,null,12105709,0

      
      
AUTHOR INITIALS:
	Initials		Name
-------------------------------------------------------------------------
	SS				Subhash Subramanyam
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
				04/28/2009	Autogenerated script
	Suganya		07/30/2009  rfp_account.SR_RFP_ACCOUNT_IDs column added in the Select Clause.
	SS       	07/30/2009	Formatted as per standards,  @user_id and @session_id unused can be removed later
							Used sitegroup instead of division
	RR			2016-06-01	GCS Phase-5 Added meter number,Alternate_Account_Number
******/
CREATE PROCEDURE [dbo].[SR_RFP_GET_ACCOUNT_INFO_POST_TO_SUPPLIER_P]
      ( 
       @user_id AS VARCHAR(10)
      ,@session_id AS VARCHAR(20)
      ,@rfp_account_group_id AS INT
      ,@is_bid_group AS BIT )
AS 
BEGIN
      SET NOCOUNT ON
      IF @is_bid_group = 1 
            BEGIN
                  SELECT
                        ch.Client_Name
                       ,ch.Site_Id
                       ,RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_Name
                       ,cha.Account_Number
                       ,bid_group.GROUP_NAME
                       ,rfp_account.SR_RFP_ACCOUNT_ID
                       ,LEFT(mtr.mtrs, LEN(mtr.mtrs) - 1) AS Meter_Number
                       ,cha.Alternate_Account_Number
                  FROM
                        SR_RFP_ACCOUNT AS rfp_account
                        INNER JOIN SR_RFP_BID_GROUP AS bid_group
                              ON rfp_account.SR_RFP_BID_GROUP_ID = bid_group.SR_RFP_BID_GROUP_ID
                        INNER JOIN core.Client_Hier_Account cha
                              ON rfp_account.ACCOUNT_ID = cha.Account_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        CROSS APPLY ( SELECT
                                          CAST(m.METER_NUMBER AS VARCHAR(200)) + ','
                                      FROM
                                          dbo.METER m
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_METER_MAP sramm
                                                ON m.METER_ID = sramm.METER_ID
                                      WHERE
                                          sramm.SR_RFP_ACCOUNT_ID = rfp_account.SR_RFP_ACCOUNT_ID
                                      GROUP BY
                                          m.METER_NUMBER
                        FOR
                                      XML PATH('') ) mtr ( mtrs )
                  WHERE
                        ( rfp_account.SR_RFP_BID_GROUP_ID = @rfp_account_group_id )
                        AND ( rfp_account.IS_DELETED = 0 )
                  GROUP BY
                        ch.Client_Name
                       ,ch.Site_Id
                       ,RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
                       ,cha.Account_Number
                       ,bid_group.GROUP_NAME
                       ,rfp_account.SR_RFP_ACCOUNT_ID
                       ,LEFT(mtr.mtrs, LEN(mtr.mtrs) - 1)
                       ,cha.Alternate_Account_Number
            END
      ELSE 
            BEGIN
    
                  SELECT
                        ch.Client_Name
                       ,ch.Site_Id
                       ,RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_Name
                       ,cha.Account_Number
                       ,NULL AS group_name
                       ,rfp_account.SR_RFP_ACCOUNT_ID
                       ,LEFT(mtr.mtrs, LEN(mtr.mtrs) - 1) AS Meter_Number
                       ,cha.Alternate_Account_Number
                  FROM
                        SR_RFP_ACCOUNT AS rfp_account
                        INNER JOIN core.Client_Hier_Account cha
                              ON rfp_account.ACCOUNT_ID = cha.Account_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        CROSS APPLY ( SELECT
                                          CAST(m.METER_NUMBER AS VARCHAR(200)) + ','
                                      FROM
                                          dbo.METER m
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_METER_MAP sramm
                                                ON m.METER_ID = sramm.METER_ID
                                      WHERE
                                          sramm.SR_RFP_ACCOUNT_ID = rfp_account.SR_RFP_ACCOUNT_ID
                                      GROUP BY
                                          m.METER_NUMBER
                        FOR
                                      XML PATH('') ) mtr ( mtrs )
                  WHERE
                        ( rfp_account.SR_RFP_ACCOUNT_ID = @rfp_account_group_id )
                        AND ( rfp_account.SR_RFP_BID_GROUP_ID IS NULL )
                        AND ( rfp_account.IS_DELETED = 0 )
                  GROUP BY
                        ch.Client_Name
                       ,ch.Site_Id
                       ,RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
                       ,cha.Account_Number
                       ,rfp_account.SR_RFP_ACCOUNT_ID
                       ,LEFT(mtr.mtrs, LEN(mtr.mtrs) - 1)
                       ,cha.Alternate_Account_Number
            END
      
END







;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_GET_ACCOUNT_INFO_POST_TO_SUPPLIER_P] TO [CBMSApplication]
GO
