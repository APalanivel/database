SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--EXEC dbo.UBM_GET_BATCHLOG_DETAILS_P '1','1',73,'cASS'

CREATE    PROCEDURE dbo.UBM_GET_BUCKET_MAPPINGS_P 
@userId varchar(20),
@sessionId varchar(20),
@ubmId int,
@isCharge int



as
	set nocount on
if @ubmId=0
begin
	
	if @isCharge = 1
	begin
		
		select 
			CBMS_charge_bucket_type_ID CBMS_BUCKET_ID, 
			ubm_charge_bucket_type UBM_BUCKET_NAME,
			ENTITY_NAME CBMS_BUCKET_NAME
		from  
			ubm_charge_bucket_map, 
			ENTITY
		WHERE 
			ENTITY_ID=CBMS_charge_bucket_type_ID 
			and ubm_id =(select ubm_id from ubm where ubm_name like 'avista')
		group by 
			CBMS_charge_bucket_type_ID,
			ubm_charge_bucket_type, 
			ENTITY_NAME
		
	end
	else
	begin
	
		select CBMS_usage_bucket_type_ID CBMS_BUCKET_ID, 
			ubm_usage_bucket_type UBM_BUCKET_NAME,
			ENTITY_NAME CBMS_BUCKET_NAME
		from  UBM_USAGE_BUCKET_MAP, ENTITY
		WHERE ENTITY_ID=CBMS_usage_bucket_type_ID and 
		ubm_id=(select ubm_id from ubm where ubm_name like 'avista')
		group by 
			CBMS_usage_bucket_type_ID,
			ubm_usage_bucket_type, 
			ENTITY_NAME

	
	end

end
else
begin

	if @isCharge = 1
	begin
		
		select CBMS_charge_bucket_type_ID CBMS_BUCKET_ID, ubm_charge_bucket_type UBM_BUCKET_NAME,
			ENTITY_NAME CBMS_BUCKET_NAME
		from  ubm_charge_bucket_map, ENTITY
		WHERE ENTITY_ID=CBMS_charge_bucket_type_ID and ubm_id=@ubmId
		group by 
			CBMS_charge_bucket_type_ID,
			ubm_charge_bucket_type, 
			ENTITY_NAME
		
	end
	else
	begin
	
		select CBMS_usage_bucket_type_ID CBMS_BUCKET_ID, 
			ubm_usage_bucket_type UBM_BUCKET_NAME,
			ENTITY_NAME CBMS_BUCKET_NAME
		from  UBM_USAGE_BUCKET_MAP, ENTITY
		WHERE ENTITY_ID=CBMS_usage_bucket_type_ID and ubm_id=@ubmId
		group by 
			CBMS_usage_bucket_type_ID,
			ubm_usage_bucket_type, 
			ENTITY_NAME

	
	end


end
GO
GRANT EXECUTE ON  [dbo].[UBM_GET_BUCKET_MAPPINGS_P] TO [CBMSApplication]
GO
