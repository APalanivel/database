SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
     
 dbo.[Report_DE_Get_UserInfo_By_Permission]    
     
DESCRIPTION:    
 This report is to get All the User Information for given Permission_Info_Id 
    
INPUT PARAMETERS:    
Name   DataType  Default Description    
-------------------------------------------------------------------------------------------    
@Permission_Info_Id   INT  
     
OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
-------------------------------------------------------------    
[Report_DE_Get_UserInfo_By_Permission]   396
  
AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
AKR         Ashok Kumar Raju    
SP			Sandeep Pigilam
    
MODIFICATIONS    
Initials Date  Modification    
------------------------------------------------------------    
 AKR       2012-10-04   Added parameters @Group_List and @Permission_List 
 SP		   2014-02-26	Modified for RA-Admin used Client_App_Access_Role_Group_Info_Map also to get user groups. 	  
*/    
CREATE PROCEDURE [dbo].[Report_DE_Get_UserInfo_By_Permission]
      ( 
       @Permission_Info_Id INT )
AS 
BEGIN    
    
      SET NOCOUNT ON    
           
      DECLARE @ent_id INT      
              
      
      SELECT
            @ent_id = ent.ENTITY_ID
      FROM
            dbo.ENTITY ent
      WHERE
            ent.ENTITY_NAME = 'USER_INFO'
            AND ent.ENTITY_DESCRIPTION = 'Table_Type'      
      
      SELECT
            uif.First_name
           ,uif.LAST_NAME
           ,uif.USERNAME
           ,CASE WHEN uif.IS_HISTORY = 1 THEN 'Yes'
                 ELSE 'No'
            END [Moved to History]
           ,c.CLIENT_NAME
           ,uif.locale_code
           ,ent_aud.MODIFIED_DATE DateAdded
           ,gif.GROUP_NAME
           ,per_inf.PERMISSION_NAME
           ,uif.EMAIL_ADDRESS
           ,uif.PHONE_NUMBER
           ,user_cr.FIRST_NAME + SPACE(1) + user_cr.LAST_NAME [Created By]
      FROM
            dbo.User_info uif
            JOIN dbo.CLIENT c
                  ON c.CLIENT_ID = uif.CLIENT_ID
            JOIN ( SELECT
                        uif.USER_INFO_ID
                       ,uif.GROUP_INFO_ID
                   FROM
                        dbo.USER_INFO_GROUP_INFO_MAP uif
                   UNION
                   SELECT
                        uicar.User_Info_Id
                       ,cargm.GROUP_INFO_ID
                   FROM
                        dbo.User_Info_Client_App_Access_Role_Map uicar
                        INNER JOIN dbo.Client_App_Access_Role_Group_Info_Map cargm
                              ON uicar.Client_App_Access_Role_Id = cargm.Client_App_Access_Role_Id
                   GROUP BY
                        uicar.User_Info_Id
                       ,cargm.GROUP_INFO_ID ) uif_map
                  ON uif_map.USER_INFO_ID = uif.USER_INFO_ID
            JOIN dbo.GROUP_INFO gif
                  ON gif.GROUP_INFO_ID = uif_map.GROUP_INFO_ID
            JOIN dbo.GROUP_INFO_PERMISSION_INFO_MAP gr_per
                  ON gr_per.GROUP_INFO_ID = gif.GROUP_INFO_ID
            JOIN dbo.PERMISSION_INFO per_inf
                  ON per_inf.PERMISSION_INFO_ID = gr_per.PERMISSION_INFO_ID
            JOIN dbo.ENTITY_AUDIT ent_aud
                  ON ent_aud.ENTITY_IDENTIFIER = uif.USER_INFO_ID
            JOIN dbo.USER_INFO user_cr
                  ON user_cr.USER_INFO_ID = ent_aud.USER_INFO_ID
      WHERE
            ent_aud.AUDIT_TYPE = 1
            AND ent_aud.ENTITY_ID = @ent_id
            AND per_inf.PERMISSION_INFO_ID = @Permission_Info_Id
              
     
END;    
  
  

;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Get_UserInfo_By_Permission] TO [CBMS_SSRS_Reports]
GO
