SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        dbo.RM_Client_Default_Hedge_Config_Sel                    
                      
Description:                      
        To get 
                      
Input Parameters:                      
    Name				DataType        Default     Description                        
--------------------------------------------------------------------------------
	@Client_Id			INT
    @Commodity_Id		INT				NULL
    @Country_Id			VARCHAR(MAX)	NULL 
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	  
                    
Usage Examples:                          
--------------------------------------------------------------------------------
	
		EXEC dbo.RM_Client_Default_Hedge_Config_Sel 235
		EXEC dbo.RM_Client_Default_Hedge_Config_Sel 11236
                     
 Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy        
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			01-08-2018     Global Risk Management - Created                    
                     
******/
CREATE PROCEDURE [dbo].[RM_Client_Default_Hedge_Config_Sel]
     (
         @Client_Id INT
         , @Commodity_Id INT = NULL
         , @Country_Id VARCHAR(MAX) = NULL
     )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Client_Client_Hier_Id INT
            , @RM_Default_Config_Start_Dt DATE
            , @RM_Default_Config_End_Dt DATE;

        SELECT
            @Client_Client_Hier_Id = Client_Hier_Id
        FROM
            Core.Client_Hier
        WHERE
            Client_Id = @Client_Id
            AND Sitegroup_Id = 0;

        SELECT
            @RM_Default_Config_Start_Dt = CAST(App_Config_Value AS DATE)
        FROM
            dbo.App_Config
        WHERE
            App_Config_Cd = 'RM_Default_Config_Start_Dt'
            AND App_Id = -1;

        SELECT
            @RM_Default_Config_End_Dt = CAST(App_Config_Value AS DATE)
        FROM
            dbo.App_Config
        WHERE
            App_Config_Cd = 'RM_Default_Config_End_Dt'
            AND App_Id = -1;

        SELECT
            chob.Client_Hier_Id
            , chob.RM_Client_Hier_Onboard_Id
            , chob.Commodity_Id
            , com.Commodity_Name
            , chob.Country_Id
            , con.COUNTRY_NAME
            , chhc.RM_Client_Hier_Hedge_Config_Id
            , chob.RM_Forecast_UOM_Type_Id
            , uom.ENTITY_NAME AS RM_Forecast_UOM
            , chhc.Config_Start_Dt
            , NULLIF(chhc.Config_End_Dt, @RM_Default_Config_End_Dt) AS Config_End_Dt
            , chhc.Hedge_Type_Id
            , hdgtyp.ENTITY_NAME AS Hedge_Type
            , chhc.Max_Hedge_Pct
            , chhc.RM_Risk_Tolerance_Category_Id
            , rtc.RM_Risk_Tolerance_Category
            , chhc.Risk_Manager_User_Info_Id
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Risk_Manager
            , chhc.Contact_Info_Id AS Client_Contact_Info_Id
            , ci.First_Name + ' ' + ci.Last_Name AS Client_Contact
            , CASE chhc.Include_In_Reports WHEN 1 THEN 'Yes'
                  ELSE 'No'
              END AS Include_In_Reports
            , chhc.Is_Default_Config
        FROM
            Trade.RM_Client_Hier_Onboard chob
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            LEFT JOIN dbo.ENTITY uom
                ON chob.RM_Forecast_UOM_Type_Id = uom.ENTITY_ID
            INNER JOIN dbo.ENTITY hdgtyp
                ON chhc.Hedge_Type_Id = hdgtyp.ENTITY_ID
            LEFT JOIN Trade.RM_Risk_Tolerance_Category rtc
                ON chhc.RM_Risk_Tolerance_Category_Id = rtc.RM_Risk_Tolerance_Category_Id
            LEFT JOIN dbo.Contact_Info ci
                ON chhc.Contact_Info_Id = ci.Contact_Info_Id
            INNER JOIN dbo.Commodity com
                ON chob.Commodity_Id = com.Commodity_Id
            INNER JOIN dbo.COUNTRY con
                ON chob.Country_Id = con.COUNTRY_ID
            LEFT JOIN dbo.USER_INFO ui
                ON chhc.Risk_Manager_User_Info_Id = ui.USER_INFO_ID
        WHERE
            chob.Client_Hier_Id = @Client_Client_Hier_Id
            AND chhc.Is_Default_Config = 1
            AND (   @Commodity_Id IS NULL
                    OR  chob.Commodity_Id = @Commodity_Id)
            AND (   @Country_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Country_Id, ',')
                                   WHERE
                                        chob.Country_Id = CAST(Segments AS INT)))
        ORDER BY
            com.Commodity_Name
            , con.COUNTRY_NAME
            , chhc.Config_Start_Dt;





    END;


GO
GRANT EXECUTE ON  [dbo].[RM_Client_Default_Hedge_Config_Sel] TO [CBMSApplication]
GO
