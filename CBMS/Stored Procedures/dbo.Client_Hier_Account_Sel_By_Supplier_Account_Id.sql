SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                        
 NAME: dbo.Client_Hier_Account_Sel_By_Supplier_Account_Id            
                        
 DESCRIPTION:                        
			To get Supplier Account Details                       
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@Account_Id						INT			                      
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 
  exec [dbo].[Client_Hier_Account_Sel_By_Supplier_Account_Id]  @Account_Id =   7518    

  Client_Hier_Account_Sel_By_Supplier_Account_Id 1741124
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2014-04-29       Created,for Data Operations Enhancement.  
 NR						2019-10-15		Add Contract - Added Supplier_Contract_ID column.             
                       
******/
CREATE PROCEDURE [dbo].[Client_Hier_Account_Sel_By_Supplier_Account_Id]
    (
        @Account_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;


        SELECT
            ch.Client_Id
            , ch.Sitegroup_Id AS Division_Id
            , ch.Site_Id
            , ch.Client_Hier_Id
            , ch.State_Id
            , cha.Supplier_Account_begin_Dt
            , cha.Supplier_Account_End_Dt
            , ch.Site_name
          
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account cha
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
        WHERE
            cha.Account_Type = 'Supplier'
            AND cha.Account_Id = @Account_Id
        GROUP BY
            ch.Client_Id
            , ch.Sitegroup_Id
            , ch.Site_Id
            , ch.Client_Hier_Id
            , ch.State_Id
            , cha.Supplier_Account_begin_Dt
            , cha.Supplier_Account_End_Dt
            , ch.Site_name
            

    END;

GO
GRANT EXECUTE ON  [dbo].[Client_Hier_Account_Sel_By_Supplier_Account_Id] TO [CBMSApplication]
GO
