
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME:
		dbo.cbmsInvSourceEmailLog_Save
		
DESCRIPTION:
			To save the scraped emails in to log table

INPUT PARAMETERS:          
	NAME						DATATYPE		DEFAULT		DESCRIPTION          
---------------------------------------------------------------------          
    @ubm_batch_master_log_id	INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION
---------------------------------------------------------------------

USAGE EXAMPLES:
---------------------------------------------------------------------

		 

AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	HG			Harihara Suthan Ganesan
	
MODIFICATIONS
	INITIALS	DATE		MODIFICATION
-----------------------------------------------------------
	HG			2017-09-06	Modified to accept the NULL values for @from_name and @from_Address column
*/
CREATE PROCEDURE [dbo].[cbmsInvSourceEmailLog_Save]
      (
       @MyAccountId INT
      ,@inv_source_email_log_id INT = NULL
      ,@inv_sourced_image_batch_id INT
      ,@originating_email_id INT = NULL
      ,@from_name VARCHAR(255) = NULL
      ,@from_address VARCHAR(255) = NULL
      ,@subject VARCHAR(1000) = NULL
      ,@received_date DATETIME = NULL )
AS
BEGIN

      DECLARE
            @this_id INT
           ,@System_User_Email_Address VARCHAR(200)

      SET NOCOUNT ON

      SELECT
            @System_User_Email_Address = EMAIL_ADDRESS
      FROM
            dbo.USER_INFO
      WHERE
            USERNAME = 'conversion'

      SET @from_name = ISNULL(@from_name, 'N/A')
      SET @from_address = ISNULL(@from_address, @System_User_Email_Address)

      SET @this_id = @inv_source_email_log_id

      IF @this_id IS NULL
            BEGIN

                  INSERT      INTO dbo.INV_SOURCE_EMAIL_LOG
                              ( INV_SOURCED_IMAGE_BATCH_ID
                              ,ORIGINATING_EMAIL_ID
                              ,FROM_NAME
                              ,FROM_ADDRESS
                              ,SUBJECT
                              ,RECEIVED_DATE )
                  VALUES
                              ( @inv_sourced_image_batch_id
                              ,@originating_email_id
                              ,@from_name
                              ,@from_address
                              ,@subject
                              ,@received_date )
	
                  SET @this_id = @@IDENTITY

                  IF @originating_email_id IS NULL
                        BEGIN

                              UPDATE
                                    dbo.INV_SOURCE_EMAIL_LOG WITH ( ROWLOCK )
                              SET
                                    ORIGINATING_EMAIL_ID = @this_id
                              WHERE
                                    INV_SOURCE_EMAIL_LOG_ID = @this_id

                        END

            END
      ELSE
            BEGIN

                  UPDATE
                        dbo.INV_SOURCE_EMAIL_LOG WITH ( ROWLOCK )
                  SET
                        INV_SOURCED_IMAGE_BATCH_ID = @inv_sourced_image_batch_id
                       ,ORIGINATING_EMAIL_ID = @originating_email_id
                       ,FROM_NAME = @from_name
                       ,FROM_ADDRESS = @from_address
                       ,SUBJECT = @subject
                       ,RECEIVED_DATE = @received_date
                  WHERE
                        INV_SOURCE_EMAIL_LOG_ID = @this_id

            END

--	set nocount off

      EXEC cbmsInvSourceEmailLog_Get
            @MyAccountId
           ,@this_id

END

;
GO

GRANT EXECUTE ON  [dbo].[cbmsInvSourceEmailLog_Save] TO [CBMSApplication]
GO
