SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                   
/******                
                 
NAME: [dbo].Merge_DE_Bucket_Account_Interval_Changes
                
DESCRIPTION:                
  used to Merge the data received from dvdehub Bucket_Account_Interval_Dtl table(change_tracking data)  to Bucket_Account_Interval_Dtl table                   
                    
 Strictly used by ETL            
                
INPUT PARAMETERS:                
NAME               DATATYPE          DEFAULT        DESCRIPTION                
------------------------------------------------------------                               
@Batch_Size        INT            
      
                                        
OUTPUT PARAMETERS:                                  
NAME   DATATYPE DEFAULT  DESCRIPTION                           
                               
------------------------------------------------------------                                  
USAGE EXAMPLES:                                  
------------------------------------------------------------

BEGIN TRAN
    EXEC Merge_DE_Bucket_Account_Interval_Changes   5000
ROLLBACK TRAN
   
AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------                                  
HG			Harihara Suthan G                     
RKV			Ravi Kumar Vegesna

MODIFICATIONS
INITIALS	DATE(YYYY-MM-DD)  MODIFICATION
------------------------------------------------------------
RKV			2013-04-26        Created
*/
CREATE PROCEDURE dbo.Merge_DE_Bucket_Account_Interval_Changes ( @Batch_Size INT )
AS 
BEGIN

      SET NOCOUNT ON;

      DECLARE
            @Min_Bucket_Account_Interval_Dtl_Transfer_Id INT = 1
           ,@Max_Bucket_Account_Interval_Dtl_Transfer_Id INT
           ,@Data_Source_Cd_CBMS INT
           ,@Data_Source_Cd_DE INT
           ,@Data_Source_Cd_Invoice INT
           
      SELECT
            @Data_Source_Cd_CBMS = MAX(CASE WHEN cd.Code_Value = 'CBMS' THEN cd.CODE_ID
                                       END)
           ,@Data_Source_Cd_DE = MAX(CASE WHEN cd.Code_Value = 'DE' THEN cd.CODE_ID
                                     END)
           ,@Data_Source_Cd_Invoice = MAX(CASE WHEN cd.Code_Value = 'Invoice' THEN cd.CODE_ID
                                          END)
      FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code cd
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Std_Column_Name = 'Data_Source_Cd'
            AND cd.Code_Value IN ( 'DE', 'CBMS', 'Invoice' );

      SELECT
            @Max_Bucket_Account_Interval_Dtl_Transfer_Id = MAX(sbsi.Bucket_Account_Interval_Dtl_Transfer_Id)
      FROM
            stage.Bucket_Account_Interval_Dtl_Transfer sbsi
      
      WHILE @Min_Bucket_Account_Interval_Dtl_Transfer_Id <= @Max_Bucket_Account_Interval_Dtl_Transfer_Id 
            BEGIN      
                  MERGE INTO dbo.Bucket_Account_Interval_Dtl tgt_bsid
                        USING 
                              ( SELECT
                                    sbsi.Bucket_Daily_Avg_Value
                                   ,sbsi.Bucket_Master_Id
                                   ,sbsi.Client_Hier_Id
                                   ,sbsi.Account_Id
                                   ,sbsi.Created_Ts
                                   ,sbsi.Created_User_Id
                                   ,sbsi.Currency_Unit_Id
                                   ,sbsi.Data_Source_Cd
                                   ,sbsi.Last_Changed_Ts
                                   ,sbsi.Service_End_Dt
                                   ,sbsi.Service_Start_Dt
                                   ,sbsi.Sys_Change_Operation
                                   ,sbsi.Uom_Type_Id
                                   ,sbsi.Updated_User_Id
                                FROM
                                    stage.Bucket_Account_Interval_Dtl_Transfer sbsi	--excluding manual data if invoice data exists, it can happen if data entered by DE and Invoice on same time/day
                                WHERE
                                    sbsi.Bucket_Account_Interval_Dtl_Transfer_Id BETWEEN @Min_Bucket_Account_Interval_Dtl_Transfer_Id
                                                                                 AND     ( @Min_Bucket_Account_Interval_Dtl_Transfer_Id + @Batch_Size - 1 )
                                    AND NOT EXISTS ( SELECT
                                                      1
                                                     FROM
                                                      dbo.Cost_Usage_Account_Dtl cuad
                                                     WHERE
                                                      cuad.Client_Hier_Id = sbsi.Client_Hier_Id
                                                      AND cuad.Account_Id = sbsi.Account_Id
                                                      AND cuad.Bucket_Master_Id = sbsi.Bucket_Master_Id
                                                      AND cuad.Service_Month = sbsi.Service_Start_Dt
                                                      AND cuad.Data_Source_Cd = @Data_Source_Cd_Invoice ) ) AS src_bsid
                        ON ( tgt_bsid.Client_Hier_Id = src_bsid.Client_Hier_Id
                             AND tgt_bsid.Bucket_Master_Id = src_bsid.Bucket_Master_Id
                             AND tgt_bsid.Service_Start_Dt = src_bsid.Service_Start_Dt
                             AND tgt_bsid.Service_End_Dt = src_bsid.Service_End_Dt
                             AND tgt_bsid.Account_Id = src_bsid.Account_Id )
                        WHEN MATCHED AND ( src_bsid.Sys_Change_Operation = 'D'
                                           AND src_bsid.Bucket_Daily_Avg_Value IS NULL
                                           AND ( tgt_bsid.Data_Source_Cd = @Data_Source_Cd_CBMS
                                                 OR tgt_bsid.Data_Source_Cd = @Data_Source_Cd_DE ) )
                              THEN
								DELETE
                        WHEN MATCHED AND ( src_bsid.Sys_Change_Operation <> 'D'
                                           AND ( tgt_bsid.Data_Source_Cd = @Data_Source_Cd_CBMS
                                                 OR tgt_bsid.Data_Source_Cd = @Data_Source_Cd_DE ) )
                              THEN
								UPDATE
                                   SET
                                    tgt_bsid.Bucket_Daily_Avg_Value = src_bsid.Bucket_Daily_Avg_Value
                                   ,tgt_bsid.Uom_Type_Id = src_bsid.Uom_Type_Id
                                   ,tgt_bsid.Currency_Unit_Id = src_bsid.Currency_Unit_Id
                                   ,tgt_bsid.Data_Source_Cd = src_bsid.Data_Source_Cd
                                   ,tgt_bsid.Last_Changed_Ts = src_bsid.Last_Changed_Ts
                                   ,tgt_bsid.Updated_User_Id = src_bsid.Updated_User_Id
                        WHEN NOT MATCHED AND ( src_bsid.Sys_Change_Operation IN ( 'I', 'U' ) )
                              THEN      
								INSERT
                                    ( 
                                     Client_Hier_Id
                                    ,Account_Id
                                    ,Bucket_Master_Id
                                    ,Service_Start_Dt
                                    ,Service_End_Dt
                                    ,Bucket_Daily_Avg_Value
                                    ,Data_Source_Cd
                                    ,Uom_Type_Id
                                    ,Currency_Unit_Id
                                    ,Created_User_Id
                                    ,Updated_User_Id
                                    ,Created_Ts
                                    ,Last_Changed_Ts )
                                   VALUES
                                    ( 
                                     src_bsid.Client_Hier_Id
                                    ,src_bsid.Account_Id
                                    ,src_bsid.Bucket_Master_Id
                                    ,src_bsid.Service_Start_Dt
                                    ,src_bsid.Service_End_Dt
                                    ,src_bsid.Bucket_Daily_Avg_Value
                                    ,src_bsid.Data_Source_Cd
                                    ,src_bsid.Uom_Type_Id
                                    ,src_bsid.Currency_Unit_Id
                                    ,src_bsid.Created_User_Id
                                    ,src_bsid.Updated_User_Id
                                    ,src_bsid.Created_Ts
                                    ,src_bsid.Last_Changed_Ts );  
                                             
                  SET @Min_Bucket_Account_Interval_Dtl_Transfer_Id = @Min_Bucket_Account_Interval_Dtl_Transfer_Id + @Batch_Size   
            END      
END;
;
GO
GRANT EXECUTE ON  [dbo].[Merge_DE_Bucket_Account_Interval_Changes] TO [ETL_Execute]
GO
