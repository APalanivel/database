SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********   
NAME:  dbo.Account_Level_Variance_Test_SEL_Commodity  
 
DESCRIPTION:  Used to select commodity by account  

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
@Account_Id			Int         
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:  
	
	SELECT
		com.Commodity_Id
		 ,com.Commodity_Name
	FROM
		dbo.Account acc
		LEFT JOIN supplier_account_meter_map map
			ON acc.Account_id = map.Account_id
		INNER JOIN dbo.Meter m
			ON (CASE WHEN map.METER_ID IS NULL THEN m.ACCOUNT_ID ELSE m.METER_ID END) 
				= (CASE WHEN map.METER_ID IS NULL THEN acc.ACCOUNT_ID ELSE map.METER_ID END)
		INNER JOIN dbo.Rate r
			ON r.Rate_id = m.Rate_id
		INNER JOIN dbo.Commodity com
			 ON com.Commodity_Id = r.Commodity_Type_Id
	WHERE
		acc.Account_Id =  79277
	GROUP BY
		com.Commodity_Id
		 ,com.Commodity_Name
		
	Account_Level_Variance_Test_SEL_Commodity 1431
	

------------------------------------------------------------  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
NK   Nageswara Rao Kosuri         


Initials Date  Modification  
------------------------------------------------------------  
NK	10/13/2009  Created
HG	11/30/2009	Removed the @account_type_id calculation
				Using union all in place of If then else
				This query can be further simplified by Left Outer join the Supplier_account_meter_map with the main query but that way query requires more resources to execute hence using UNION ALL.

******/  


CREATE PROCEDURE dbo.Account_Level_Variance_Test_SEL_Commodity
	@Account_Id INT	
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT
		com.Commodity_Id
		 ,com.Commodity_Name
	FROM
		dbo.Account acc
		INNER JOIN supplier_account_meter_map map
			ON acc.Account_id = map.Account_id
		INNER JOIN dbo.Meter m
			ON m.METER_ID = map.METER_ID
		INNER JOIN dbo.Rate r
			ON r.Rate_id = m.Rate_id
		INNER JOIN dbo.Commodity com
			 ON com.Commodity_Id = r.Commodity_Type_Id
	WHERE
		acc.Account_Id =  @Account_Id
	GROUP BY
		com.Commodity_Id
		,com.Commodity_Name

	UNION ALL

	SELECT
		com.Commodity_Id
		,com.Commodity_Name
	FROM
		dbo.Account acc
		INNER JOIN dbo.Meter m
			ON m.ACCOUNT_ID = acc.ACCOUNT_ID
		INNER JOIN dbo.Rate r
			ON r.Rate_id = m.Rate_id
		INNER JOIN dbo.Commodity com
			 ON com.Commodity_Id = r.Commodity_Type_Id
	WHERE
		acc.Account_Id = @Account_Id
	GROUP BY
		com.Commodity_Id
		,com.Commodity_Name

END
GO
GRANT EXECUTE ON  [dbo].[Account_Level_Variance_Test_SEL_Commodity] TO [CBMSApplication]
GO
