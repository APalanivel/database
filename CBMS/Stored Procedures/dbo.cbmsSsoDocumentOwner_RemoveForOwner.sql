SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[cbmsSsoDocumentOwner_RemoveForOwner]
	( @MyAccountId int
	, @document_id int
	)
AS
BEGIN

	set nocount on

	  delete sso_document_owner_map
	   where sso_document_id = @document_id

--	exec cbmsPermissionInfo_GetMy @MyAccountId, @group_info_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoDocumentOwner_RemoveForOwner] TO [CBMSApplication]
GO
