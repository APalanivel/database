SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE [dbo].[cbmsELMarketData_Get]
	( @price_point_id int = null
	)

AS
BEGIN

	   select price_point_id
		, price_point
		, source
	     from el_market_data with (nolock)
	    where price_point_id = @price_point_id


END
GO
GRANT EXECUTE ON  [dbo].[cbmsELMarketData_Get] TO [CBMSApplication]
GO
