SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
 CBMS.dbo.Check_Deal_Ticket_Moved_To_Private_Queue

DESCRIPTION:
  To check if the given deal ticket ids have been already moved to private queue
  Returns true if atleast one of the ticket has been moved to private queue else returns false.

INPUT PARAMETERS:
 Name					DataType  Default Description
------------------------------------------------------------

 @RM_Deal_Ticket_Id_CSV VARCHAR(max)

OUTPUT PARAMETERS:
 Name   DataType  Default Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

BEGIN TRAN
DECLARE @FLAG BIT
EXEC dbo.Check_Deal_Ticket_Moved_To_Private_Queue '113688,120577',1,153,1,'supply',@FLAG OUT -- PUBLIC Deal_TicketS 
SELECT @FLAG
ROLLBACK TRAN

BEGIN TRAN
DECLARE @FLAG BIT
EXEC dbo.Check_Deal_Ticket_Moved_To_Private_Queue '121172,121171,121173',1,1,1,'supply',@FLAG OUT -- PRIVATE Deal_TicketS 
SELECT @FLAG
ROLLBACK TRAN

SELECT  TOP 10 A.RM_Deal_Ticket_Id,A.QUEUE_ID FROM dbo.RM_DEAL_TICKET A JOIN QUEUE B ON A.QUEUE_ID=B.QUEUE_ID JOIN ENTITY E ON E.ENTITY_ID=B.QUEUE_TYPE_ID WHERE E.ENTITY_NAME='PUBLIC'

SELECT * FROM QUEUE WHERE QUEUE_ID IN(126)
SELECT * FROM ENTITY WHERE ENTITY_ID=243
SELECT * FROM GROUP_INFO WHERE QUEUE_ID IN(126)

AUTHOR INITIALS:
 Initials Name
------------------------------------------------------------
 BCH	  Balaraju Chalumuri

MODIFICATIONS

 Initials Date		  Modification
------------------------------------------------------------
 BCH	  2013-05-27  Created for MAINT-1847

******/
CREATE PROCEDURE dbo.Check_Deal_Ticket_Moved_To_Private_Queue
      ( 
       @RM_Deal_Ticket_Id_CSV VARCHAR(MAX)
      ,@UserId VARCHAR(10)
      ,@SessionId VARCHAR(20)
      ,@QueueId INT
      ,@GroupName VARCHAR(200)
      ,@Flag_Status BIT OUT )
AS 
BEGIN
      SET NOCOUNT ON;
      DECLARE @Flag BIT
      DECLARE @RM_Deal_Ticket_List TABLE
            ( 
             RM_Deal_Ticket_Id INT PRIMARY KEY CLUSTERED )
  
      INSERT      INTO @RM_Deal_Ticket_List
                  ( 
                   RM_Deal_Ticket_Id )
                  SELECT
                        segments
                  FROM
                        dbo.ufn_split(@RM_Deal_Ticket_Id_CSV, ',')  
  
      SELECT
            @Flag = MAX(CASE Qu_Type.ENTITY_NAME
                          WHEN 'Public' THEN 0
                          WHEN 'Private' THEN 1
                        END)
      FROM
            dbo.RM_DEAL_TICKET rmd
            INNER JOIN @RM_Deal_Ticket_List rdtl
                  ON rdtl.RM_Deal_Ticket_Id = rmd.RM_DEAL_TICKET_ID
            INNER JOIN dbo.QUEUE QE
                  ON qe.Queue_Id = rmd.Queue_Id
            INNER JOIN dbo.Entity Qu_Type
                  ON Qu_Type.Entity_Id = QE.Queue_Type_Id
           
      IF @Flag = 0 
            BEGIN
                  -- claim deal ticket
                  IF @QueueId IS NULL
                        AND @GroupName IS NULL 
                        BEGIN
                              SELECT
                                    @QueueId = QUEUE_ID
                              FROM
                                    dbo.USER_INFO
                              WHERE
                                    USER_INFO_ID = @UserId 
                        END
						--move deal ticket to a group
                  ELSE 
                        BEGIN
                              IF @GroupName IS NOT NULL 
                                    BEGIN
                                          SELECT
                                                @QueueId = QUEUE_ID
                                          FROM
                                                dbo.GROUP_INFO
                                          WHERE
                                                GROUP_NAME = @GroupName 
                                    END
                        END

                  UPDATE
                        RDL
                  SET   
                        RDL.QUEUE_ID = @QueueId
                  FROM
                        dbo.RM_DEAL_TICKET RDL
                        INNER JOIN @RM_Deal_Ticket_List RDTL
                              ON RDL.RM_DEAL_TICKET_ID = RDTL.RM_DEAL_TICKET_ID                

            END

      SELECT
            @Flag_Status = @Flag
  
END
;
GO
GRANT EXECUTE ON  [dbo].[Check_Deal_Ticket_Moved_To_Private_Queue] TO [CBMSApplication]
GO
