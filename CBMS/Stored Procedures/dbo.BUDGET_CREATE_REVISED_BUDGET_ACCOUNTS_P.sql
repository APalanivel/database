SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- exec BUDGET_CREATE_REVISED_BUDGET_ACCOUNTS_P -1,-1,1,56
-- select * from budget_account  where budget_id = 226
-- delete from budget_account where budget_id = 56
-- select * from budget where budget_id=226

CREATE         PROCEDURE dbo.BUDGET_CREATE_REVISED_BUDGET_ACCOUNTS_P
@user_id varchar(10),
@session_id varchar(20),
@originalBudgetId int,
@createdBudgetId int


AS
begin
set nocount on



	insert into BUDGET_ACCOUNT(
			ACCOUNT_ID,			
			BUDGET_ID,			
			CD_CREATE_MULTIPLIER,
			BUDGET_COMMENTS,
			IS_DELETED,
			RATE_NOTES_ID,
			SOURCING_NOTES_ID,
			HAS_DETAILS_SAVED)

	select 		ACCOUNT_ID,			
			@createdBudgetId,			
			CD_CREATE_MULTIPLIER,
			BUDGET_COMMENTS,
			IS_DELETED,
			RATE_NOTES_ID,
			SOURCING_NOTES_ID,
			HAS_DETAILS_SAVED

	from 		BUDGET_ACCOUNT

	where 		BUDGET_ID = @originalBudgetId
			and is_deleted = 0
                        


				
end
GO
GRANT EXECUTE ON  [dbo].[BUDGET_CREATE_REVISED_BUDGET_ACCOUNTS_P] TO [CBMSApplication]
GO
