SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Invoice_Collection_Exception_Queue_Sel       
              
Description:              
			This sproc to get the Invoice Collection Exceptions for the given filters     
                           
 Input Parameters:              
    Name									DataType								Default			Description                
-----------------------------------------------------------------------------------------------------------------                
	@tvp_Invoice_Collection_Queue_Sel		tvp_Invoice_Collection_Queue_Sel                      
    @Tvp_Invoice_Collection_Sources         Tvp_Invoice_Collection_Sources
    @Start_Index							INT
    @End_Index								INT
    @Total_Count							INT
    
 Output Parameters:                    
    Name								DataType			Default			Description                
-----------------------------------------------------------------------------------------------------------------                
              
 Usage Examples:                  
-----------------------------------------------------------------------------------------------------------------                



 DECLARE
      @tvp_Invoice_Collection_Exception_Queue_Sel tvp_Invoice_Collection_Exception_Queue_Sel
            
 
 INSERT     INTO @tvp_Invoice_Collection_Exception_Queue_Sel
            ( ICO_User_Info_Id )
 VALUES
            ( 71920 ) 
 
 EXEC dbo.Invoice_Collection_Exception_Queue_Sel
      @tvp_Invoice_Collection_Exception_Queue_Sel,@Commodity_Name = 'Elec'
      
      
     
DECLARE @tvp_Invoice_Collection_Exception_Queue_Sel tvp_Invoice_Collection_Exception_Queue_Sel 
INSERT      INTO @tvp_Invoice_Collection_Exception_Queue_Sel
            ( ICO_User_Info_Id, Status_Cd )
VALUES
            ( 60366, 102336 ) 
EXEC dbo.Invoice_Collection_Exception_Queue_Sel 
      @tvp_Invoice_Collection_Exception_Queue_Sel				
   

   DECLARE @tvp_Invoice_Collection_Exception_Queue_Sel tvp_Invoice_Collection_Exception_Queue_Sel;

INSERT INTO @tvp_Invoice_Collection_Exception_Queue_Sel
     (
         ICO_User_Info_Id
     )
VALUES
    (71920);

EXEC dbo.Invoice_Collection_Exception_Queue_Sel @tvp_Invoice_Collection_Exception_Queue_Sel
              
Author Initials:              
    Initials		Name              
---------------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna
	NR				Narayana Reddy
	SP              Srinivas Patchava
	
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------------                
    RKV				2016-12-29		Created For Invoice_Collection. 
    NR				2017-05-23      MAINT-5380 - splitting the multiple comments as "|^$" this keyword instead of Comma(,).  
    RKV             2017-06-20      Maint-5484 Modified to hadle when we have consolidated billing end date is open.         
    RKV             2018-02-21      MAINT-6662 Added two parameters Sort_col and Sort_Order.
	SP              2019-07-21      SE2017-729 Mapping of IC Exceptions Type
	RKV				2019-08-27      Invoice Collection Project removing Invoice collection officer mandatory.
	SLP				2019-11-21		MAINT-9546 : To avoid duplicate records the vendor with 
									oldest Supplier association start date should be selected. 
             
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Exception_Queue_Sel]
(
    @tvp_Invoice_Collection_Exception_Queue_Sel tvp_Invoice_Collection_Exception_Queue_Sel READONLY,
    @Client_Name VARCHAR(200) = NULL,
    @Site_Name VARCHAR(200) = NULL,
    @Account_Number VARCHAR(500) = NULL,
    @Country_Name VARCHAR(200) = NULL,
    @State_Name VARCHAR(200) = NULL,
    @Commodity_Name VARCHAR(50) = NULL,
    @Account_Vendor_Name VARCHAR(200) = NULL,
    @Invoice_Collection_Exception_Type_Cd INT = NULL,
    @Start_Index INT = 1,
    @End_Index INT = 2147483647,
    @Total_Count INT = 0,
    @Sort_Col VARCHAR(50) = 'Created_Date',
    @Sort_Order VARCHAR(15) = 'ASC'
)
AS
BEGIN

    SET NOCOUNT ON;

    DECLARE @Invoice_Collection_Type_Cd INT,
            @Invoice_Collection_Issue_Status_Cd INT,
            @SQL_Statement NVARCHAR(MAX);


    DECLARE @Invoice_Collection_Officer_User_Id INT,
            @Collection_Start_Date DATE,
            @Collection_End_Date DATE,
            @Exception_Type_Cd INT,
            @Priority_Cd INT,
            @Comment_Id INT,
            @Ubm_ID INT,
            @Vendor_Type CHAR(8),
            @Status_Cd INT;

    SELECT @Invoice_Collection_Type_Cd = Code_Id
    FROM dbo.Code c
        INNER JOIN dbo.Codeset cs
            ON c.Codeset_Id = cs.Codeset_Id
    WHERE cs.Std_Column_Name = 'Invoice_Collection_Type_Cd'
          AND c.Code_Value = 'ICE';


    SELECT @Invoice_Collection_Issue_Status_Cd = Code_Id
    FROM dbo.Code c
        INNER JOIN dbo.Codeset cs
            ON c.Codeset_Id = cs.Codeset_Id
    WHERE cs.Std_Column_Name = 'Invoice_Collection_Chase_Status_Cd'
          AND c.Code_Value = 'Open';


    SELECT @Invoice_Collection_Officer_User_Id = ticeqs.ICO_User_Info_Id,
           @Collection_Start_Date = ticeqs.Collection_Start_Date,
           @Collection_End_Date = ticeqs.Collection_End_Date,
           @Exception_Type_Cd = ticeqs.Exception_Type_Cd,
           @Priority_Cd = ticeqs.Priority_Cd,
           @Comment_Id = ticeqs.Comment_Id,
           @Ubm_ID = ticeqs.Ubm_ID,
           @Vendor_Type = ticeqs.Vendor_Type,
           @Status_Cd = ticeqs.Status_Cd
    FROM @tvp_Invoice_Collection_Exception_Queue_Sel ticeqs;

    SELECT icq.Created_Ts Created_Date,
           CASE
               WHEN cpc.Code_Value = 'Day Issued' THEN
                   'High'
               ELSE
                   'Standard'
           END PRIORITY,
           ch.Client_Name,
           ch.Country_Name,
           cha.Display_Account_Number Account_Number,
           icac.Invoice_Collection_Alternative_Account_Number AS Alternate_Account_Number,
           CAST(icq.Collection_Start_Dt AS VARCHAR(15)) Collection_Start_Dt,
           CAST(icq.Collection_End_Dt AS VARCHAR(15)) Collection_End_Dt,
           et.Code_Value EXCEPTION,
           icq.Received_Status_Updated_Dt Received_Date,
           u.UBM_NAME,
           icq.Invoice_File_Name,
           icq.Invoice_Collection_Queue_Id,
           icq.Is_Invoice_Collection_Start_Dt_Changed,
           icq.Is_Invoice_Collection_End_Dt_Changed,
           CASE
               WHEN COUNT(icec.Comment_Desc) > 1 THEN
                   'Multiple'
               WHEN COUNT(icec.Comment_Desc) = 1 THEN
                   MAX(icec.Comment_Desc)
               ELSE
                   ''
           END Comment_Desc,
           icq.Is_Manual,
           ch.Client_Id,
           ch.Sitegroup_Id Division_Id,
           ch.Site_Id,
           ch.Client_Hier_Id,
           cha.Account_Id,
           ISNULL(vd.Account_Vendor_Type, cha.Account_Type) Account_Type,
           CASE
               WHEN LEN(Commodity.Commodity_desc_comma) > 0
                    AND icq.Commodity_Id IN ( -1, 0 ) THEN
                   LEFT(Commodity.Commodity_desc_comma, LEN(Commodity.Commodity_desc_comma) - 1)
               WHEN icq.Commodity_Id > 0 THEN
                   cm.Commodity_Name
               ELSE
                   Commodity.Commodity_desc_comma
           END AS Commodity_Name,
           CASE
               WHEN LEN(Comments.Comment_desc_comma) > 0 THEN
                   LEFT(Comments.Comment_desc_comma, LEN(Comments.Comment_desc_comma) - 3)
               ELSE
                   Comments.Comment_desc_comma
           END AS Comment_desc_comma,
           icmrt.Code_Value AS Invoice_Request_Type,
           vd.Sno AS Sno
    INTO #ICQ_Data
    FROM dbo.Invoice_Collection_Queue icq
        INNER JOIN dbo.Invoice_Collection_Account_Config icac
            ON icac.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
        INNER JOIN Core.Client_Hier_Account cha
            ON cha.Account_Id = icac.Account_Id
        INNER JOIN dbo.Commodity chc
            ON chc.Commodity_Id = cha.Commodity_Id
        INNER JOIN Core.Client_Hier ch
            ON ch.Client_Hier_Id = cha.Client_Hier_Id
        INNER JOIN USER_INFO ui
            ON ui.USER_INFO_ID = icac.Invoice_Collection_Officer_User_Id
        INNER JOIN Code isc
            ON isc.Code_Id = icq.Status_Cd
        LEFT OUTER JOIN dbo.Invoice_Collection_Exception_Comment icec
            ON icec.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id --WHERE icq.Invoice_Collection_Queue_Id=2267240
        LEFT OUTER JOIN Commodity cm
            ON cm.Commodity_Id = icq.Commodity_Id
        LEFT OUTER JOIN Code et
            ON et.Code_Id = icq.Invoice_Collection_Exception_Type_Cd
        LEFT OUTER JOIN dbo.Code icmrt
            ON icmrt.Code_Id = icq.Invoice_Request_Type_Cd
        LEFT OUTER JOIN dbo.Code cpc
            ON cpc.Code_Id = icac.Chase_Priority_Cd
        LEFT OUTER JOIN(dbo.Account_Invoice_Collection_Source aics
        INNER JOIN dbo.Account_Invoice_Collection_UBM_Source_Dtl aicusd
            ON aics.Account_Invoice_Collection_Source_Id = aicusd.Account_Invoice_Collection_Source_Id
        INNER JOIN dbo.UBM u
            ON aicusd.UBM_Id = u.UBM_ID)
            ON icac.Invoice_Collection_Account_Config_Id = aics.Invoice_Collection_Account_Config_Id
               AND aics.Is_Primary = 1
        LEFT OUTER JOIN
        (
            SELECT Account_Vendor_Id = CASE
                                           WHEN scha.Account_Vendor_Name IS NOT NULL
                                                AND asbv1.Account_Id IS NOT NULL THEN
                                               scha.Account_Vendor_Id
                                           WHEN v.VENDOR_NAME IS NOT NULL THEN
                                               v.VENDOR_ID
                                           ELSE
                                               ucha.Account_Vendor_Id
                                       END,
                   Account_Vendor_Type = CASE
                                             WHEN scha.Account_Vendor_Name IS NOT NULL
                                                  AND asbv1.Account_Id IS NOT NULL THEN
                                                 scha.Account_Type
                                             WHEN v.VENDOR_NAME IS NOT NULL THEN
                                                 e.ENTITY_NAME
                                             ELSE
                                                 ucha.Account_Type
                                         END,
                   Account_Vendor_Name = CASE
                                             WHEN scha.Account_Vendor_Name IS NOT NULL
                                                  AND asbv1.Account_Id IS NOT NULL THEN
                                                 scha.Account_Vendor_Name
                                             WHEN v.VENDOR_NAME IS NOT NULL THEN
                                                 v.VENDOR_NAME
                                             ELSE
                                                 ucha.Account_Vendor_Name
                                         END,
                   icac.Invoice_Collection_Account_Config_Id,
                   ROW_NUMBER() OVER (PARTITION BY icac.Invoice_Collection_Account_Config_Id
                                      ORDER BY scha.Supplier_Account_begin_Dt
                                     ) Sno
            FROM Core.Client_Hier_Account ucha
                INNER JOIN Core.Client_Hier ch
                    ON ucha.Client_Hier_Id = ch.Client_Hier_Id
                INNER JOIN dbo.Invoice_Collection_Account_Config icac
                    ON icac.Account_Id = ucha.Account_Id
                LEFT OUTER JOIN Core.Client_Hier_Account scha
                    ON ucha.Meter_Id = scha.Meter_Id
                       AND scha.Account_Type = 'Supplier'
                       AND icac.Invoice_Collection_Service_End_Dt
                       BETWEEN scha.Supplier_Account_begin_Dt AND scha.Supplier_Account_End_Dt
                LEFT OUTER JOIN(dbo.Account_Consolidated_Billing_Vendor asbv
                INNER JOIN dbo.ENTITY e
                    ON asbv.Invoice_Vendor_Type_Id = e.ENTITY_ID
                       AND e.ENTITY_NAME = 'Supplier'
                LEFT OUTER JOIN dbo.VENDOR v
                    ON v.VENDOR_ID = asbv.Supplier_Vendor_Id)
                    ON asbv.Account_Id = ucha.Account_Id
                       AND icac.Invoice_Collection_Service_End_Dt
                       BETWEEN asbv.Billing_Start_Dt AND ISNULL(asbv.Billing_End_Dt, '9999-12-31')
                LEFT OUTER JOIN(dbo.Account_Consolidated_Billing_Vendor asbv1
                INNER JOIN dbo.ENTITY e1
                    ON asbv1.Invoice_Vendor_Type_Id = e1.ENTITY_ID
                       AND e1.ENTITY_NAME = 'Supplier'
                LEFT OUTER JOIN dbo.VENDOR v1
                    ON v1.VENDOR_ID = asbv1.Supplier_Vendor_Id)
                    ON asbv1.Account_Id = ucha.Account_Id
            WHERE ucha.Account_Type = 'Utility'
            GROUP BY CASE
                         WHEN scha.Account_Vendor_Name IS NOT NULL
                              AND asbv1.Account_Id IS NOT NULL THEN
                             scha.Account_Vendor_Id
                         WHEN v.VENDOR_NAME IS NOT NULL THEN
                             v.VENDOR_ID
                         ELSE
                             ucha.Account_Vendor_Id
                     END,
                     CASE
                         WHEN scha.Account_Vendor_Name IS NOT NULL
                              AND asbv1.Account_Id IS NOT NULL THEN
                             scha.Account_Type
                         WHEN v.VENDOR_NAME IS NOT NULL THEN
                             e.ENTITY_NAME
                         ELSE
                             ucha.Account_Type
                     END,
                     CASE
                         WHEN scha.Account_Vendor_Name IS NOT NULL
                              AND asbv1.Account_Id IS NOT NULL THEN
                             scha.Account_Vendor_Name
                         WHEN v.VENDOR_NAME IS NOT NULL THEN
                             v.VENDOR_NAME
                         ELSE
                             ucha.Account_Vendor_Name
                     END,
                     icac.Invoice_Collection_Account_Config_Id,
                     scha.Supplier_Account_begin_Dt
        ) vd
            ON vd.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
        CROSS APPLY
    (
        SELECT icecom.Comment_Desc + ' # ' + ccom.Code_Value + '|^$'
        FROM dbo.Invoice_Collection_Exception_Comment icecom
            INNER JOIN dbo.Code ccom
                ON icecom.Comment_Type_Cd = ccom.Code_Id
        WHERE icecom.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
        GROUP BY icecom.Comment_Desc,
                 ccom.Code_Value
        FOR XML PATH('')
    ) Comments(Comment_desc_comma)
        CROSS APPLY
    (
        SELECT com.Commodity_Name + ','
        FROM dbo.Invoice_Collection_Queue icq2
            INNER JOIN dbo.Invoice_Collection_Account_Config icac2
                ON icq2.Invoice_Collection_Account_Config_Id = icac2.Invoice_Collection_Account_Config_Id
            INNER JOIN Core.Client_Hier_Account cha2
                ON icac2.Account_Id = cha2.Account_Id
            INNER JOIN dbo.Commodity com
                ON cha2.Commodity_Id = com.Commodity_Id
        WHERE icq2.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
        
        GROUP BY com.Commodity_Name
        FOR XML PATH('')
    ) Commodity(Commodity_desc_comma)
    WHERE icq.Invoice_Collection_Queue_Type_Cd = @Invoice_Collection_Type_Cd
          AND
          (
              @Invoice_Collection_Officer_User_Id IS NULL
              OR icac.Invoice_Collection_Officer_User_Id = @Invoice_Collection_Officer_User_Id
          )
          AND
          (
              @Invoice_Collection_Officer_User_Id IS NULL
              OR (EXISTS
    (
        SELECT 1
        FROM dbo.Invoice_Collection_Exception_Type_User_Info_Map icetuim
            INNER JOIN @tvp_Invoice_Collection_Exception_Queue_Sel ticeqs
                ON icetuim.User_Info_Id = ticeqs.ICO_User_Info_Id
        WHERE icetuim.Invoice_Collection_Exception_Type_Cd = icq.Invoice_Collection_Exception_Type_Cd
              AND
              (
                  @Exception_Type_Cd IS NULL
                  OR icetuim.Invoice_Collection_Exception_Type_Cd = @Exception_Type_Cd
              )
    )
                 )
              OR
              (
                  @Invoice_Collection_Officer_User_Id IS NULL
                  OR (EXISTS
    (
        SELECT 1
        FROM dbo.Invoice_Collection_Exception_Type_User_Info_Map icetuim
        WHERE icetuim.Invoice_Collection_Exception_Type_Cd = icq.Invoice_Collection_Exception_Type_Cd
              AND
              (
                  @Exception_Type_Cd IS NULL
                  OR icetuim.Invoice_Collection_Exception_Type_Cd = @Exception_Type_Cd
              )
              AND icetuim.User_Info_Id = -1
    )
                     )
              )
          )
          AND
          (
              (
                  @Collection_Start_Date IS NULL
                  AND @Collection_End_Date IS NULL
              )
              OR
              (
                  @Collection_Start_Date IS NOT NULL
                  AND @Collection_End_Date IS NULL
                  AND @Collection_Start_Date <= icq.Collection_Start_Dt
              )
              OR
              (
                  @Collection_Start_Date IS NULL
                  AND @Collection_End_Date IS NOT NULL
                  AND @Collection_End_Date >= icq.Collection_End_Dt
              )
              OR
              (
                  @Collection_Start_Date IS NOT NULL
                  AND @Collection_End_Date IS NOT NULL
                  AND
                  (
                      @Collection_Start_Date <= icq.Collection_Start_Dt
                      AND @Collection_End_Date >= icq.Collection_End_Dt
                  )
              )
          )
          AND
          (
              @Priority_Cd IS NULL
              OR cpc.Code_Id = @Priority_Cd
          )
          AND
          (
              @Comment_Id IS NULL
              OR
              (
                  @Comment_Id = 1
                  AND icec.Invoice_Collection_Exception_Comment_Id IS NOT NULL
              )
              OR
              (
                  @Comment_Id = 0
                  AND icec.Invoice_Collection_Exception_Comment_Id IS NULL
              )
          )
          AND
          (
              @Client_Name IS NULL
              OR ch.Client_Name LIKE '%' + @Client_Name + '%'
          )
          AND
          (
              @Site_Name IS NULL
              OR ch.Site_name LIKE '%' + @Site_Name + '%'
          )
          AND
          (
              @Account_Number IS NULL
              OR cha.Display_Account_Number LIKE '%' + @Account_Number + '%'
          )
          AND
          (
              @Ubm_ID IS NULL
              OR aicusd.UBM_Id = @Ubm_ID
          )
          AND
          (
              @Country_Name IS NULL
              OR ch.Country_Name LIKE '%' + @Country_Name + '%'
          )
          AND
          (
              @State_Name IS NULL
              OR ch.State_Name LIKE '%' + @State_Name + '%'
          )
          AND
          (
              @Commodity_Name IS NULL
              OR
              (
                  icq.Commodity_Id > 0
                  AND cm.Commodity_Name LIKE '%' + @Commodity_Name + '%'
              )
              OR
              (
                  icq.Commodity_Id IN ( -1, 0 )
                  AND chc.Commodity_Name LIKE '%' + @Commodity_Name + '%'
              )
          )
          AND
          (
              @Vendor_Type IS NULL
              OR ISNULL(vd.Account_Vendor_Type, cha.Account_Type) = @Vendor_Type
          )
          AND
          (
              @Account_Vendor_Name IS NULL
              OR ISNULL(vd.Account_Vendor_Name, cha.Account_Vendor_Name) LIKE '%' + @Account_Vendor_Name + '%'
          )
          AND isc.Code_Value = 'Open'
          AND
          (
              @Invoice_Collection_Exception_Type_Cd IS NULL
              OR icq.Invoice_Collection_Exception_Type_Cd = @Invoice_Collection_Exception_Type_Cd
          )
    GROUP BY icq.Created_Ts,
             cpc.Code_Value,
             ch.Client_Name,
             ch.Country_Name,
             cha.Display_Account_Number,
             icac.Invoice_Collection_Alternative_Account_Number,
             icq.Collection_Start_Dt,
             icq.Collection_End_Dt,
             et.Code_Value,
             icq.Received_Status_Updated_Dt,
             u.UBM_NAME,
             icq.Invoice_File_Name,
             icq.Invoice_Collection_Queue_Id,
             icq.Is_Invoice_Collection_Start_Dt_Changed,
             icq.Is_Invoice_Collection_End_Dt_Changed,
             icq.Is_Manual,
             ch.Client_Id,
             ch.Sitegroup_Id,
             ch.Site_Id,
             ch.Client_Hier_Id,
             cha.Account_Id,
             icmrt.Code_Value,
             ISNULL(vd.Account_Vendor_Type, cha.Account_Type),
             CASE
                 WHEN LEN(Comments.Comment_desc_comma) > 0 THEN
                     LEFT(Comments.Comment_desc_comma, LEN(Comments.Comment_desc_comma) - 3)
                 ELSE
                     Comments.Comment_desc_comma
             END,
             CASE
                 WHEN LEN(Commodity.Commodity_desc_comma) > 0
                      AND icq.Commodity_Id IN ( -1, 0 ) THEN
                     LEFT(Commodity.Commodity_desc_comma, LEN(Commodity.Commodity_desc_comma) - 1)
                 WHEN icq.Commodity_Id > 0 THEN
                     cm.Commodity_Name
                 ELSE
                     Commodity.Commodity_desc_comma
             END,
             vd.Sno;






    SELECT @Total_Count = COUNT(1) OVER ()
    FROM #ICQ_Data
    WHERE @Total_Count = 0 AND ISNULL(Sno,1)=1;

	
    SELECT @SQL_Statement
        = N';WITH Cte_Invoice_Collection_Queue   AS ( SELECT TOP ( ' + CAST(@End_Index AS VARCHAR(MAX))
          + N' )
                      Created_Date
             ,PRIORITY
                     ,Client_Name
                     ,Country_Name
                     ,Account_Number
                     ,Alternate_Account_Number
                     ,Collection_Start_Dt
                     ,Collection_End_Dt
                     ,EXCEPTION
                     ,Received_Date
                     ,UBM_NAME
                     ,Invoice_File_Name
                     ,Invoice_Collection_Queue_Id
                     ,Is_Invoice_Collection_Start_Dt_Changed
                     ,Is_Invoice_Collection_End_Dt_Changed
                     ,Comment_Desc
                     ,Is_Manual
                     ,Client_Id
                     ,Division_Id
                     ,Site_Id
                     ,Client_Hier_Id
                     ,Account_Id
                     ,Account_Type
                     ,Commodity_Name
					 ,Sno
                     ,ROW_NUMBER() OVER ( ORDER BY ' + @Sort_Col + N' ' + @Sort_Order
          + N' ) AS Row_Num
                     ,Comment_desc_comma
                     ,Invoice_Request_Type
                 FROM #ICQ_Data where Isnull(Sno,1)=1 )

    SELECT
          cic.Created_Date
         ,cic.PRIORITY
         ,cic.Client_Name
         ,cic.Country_Name
         ,cic.Account_Number
         ,cic.Alternate_Account_Number
         ,cic.Collection_Start_Dt
         ,cic.Collection_End_Dt
         ,cic.EXCEPTION
         ,cic.Received_Date
         ,cic.Commodity_Name
         ,cic.UBM_NAME
         ,cic.Invoice_File_Name
         ,cic.Invoice_Collection_Queue_Id
         ,cic.Is_Invoice_Collection_Start_Dt_Changed
         ,cic.Is_Invoice_Collection_End_Dt_Changed
         ,cic.Row_Num
         ,cic.Comment_Desc
         ,cic.Is_Manual
         ,cic.Client_Id
         ,cic.Division_Id
         ,cic.Site_Id
         ,cic.Client_Hier_Id
         ,cic.Account_Id
         ,cic.Account_Type
         ,cic.Comment_desc_comma
         ,cic.Invoice_Request_Type
		 , '                + CAST(@Total_Count AS VARCHAR(MAX))
          + N' AS Total_Row_Count
    FROM 
          Cte_Invoice_Collection_Queue cic  
    WHERE 
          cic.Row_Num BETWEEN ' + CAST(@Start_Index AS VARCHAR(MAX)) + N' AND ' + CAST(@End_Index AS VARCHAR(MAX))
          + N' ORDER BY
          Row_Num;';

    EXEC (@SQL_Statement);


    DROP TABLE #ICQ_Data;

END;


;

;








GO


GRANT EXECUTE ON  [dbo].[Invoice_Collection_Exception_Queue_Sel] TO [CBMSApplication]
GO
