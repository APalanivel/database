SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.BUDGET_GET_LAST_UPDATED_USER_DATA_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@clientId      	int       	          	
	@updateTypeName	varchar(20)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

--exec dbo.BUDGET_GET_LAST_UPDATED_USER_DATA_P '207' ,  'Nymex Forecast'
--select * from entity where entity_id = 9
--select * from user_info



CREATE	PROCEDURE dbo.BUDGET_GET_LAST_UPDATED_USER_DATA_P
	@clientId int,
--	@userId varchar(20),
	@updateTypeName varchar(20)

	AS
declare 
	@updateTypeId int
		select @updateTypeId = ( select entity_id from entity where entity_name = @updateTypeName) 
	begin
		set nocount on

		select	user_info.username , budget_client_update_info.updated_date
		from 	user_info  , budget_client_update_info 
		where user_info.user_info_id = budget_client_update_info.user_info_id 
		and   budget_client_update_info.client_id = @clientId  
		--and   bu.user_info_id = @userId  
		and   budget_client_update_info.update_type_id = @updateTypeId
		

	end
GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_LAST_UPDATED_USER_DATA_P] TO [CBMSApplication]
GO
