SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
 NAME: dbo.Cu_Invoice_Account_Recalc_Eligibility_Check              
                          
 DESCRIPTION:                          
   This sproc is used to check the eligibility of the recalc for the given account and invoice.
	This sproc assumes the Cu_Invoice And contract mapping has been done as a part of Resolve to month or Invoice association to Contract as a part of Valcon setup completion and the OCT rules are applied and exception has been raised there.

	For non Consolidated Billing Utility Account flag will be returned as 1
	If invoice is partially falls within the CB config or fully or  Account Type = Supplier then it checkes for the Open valcon exception and returns the flag accordingly.

 INPUT PARAMETERS:            
                       
 Name                        DataType         Default       Description          
------------------------------------------------------------------------------       
@Account_Id					INT  
@Cu_Invoice_Id		        INT               
                          
 OUTPUT PARAMETERS:            
                             
 Name                        DataType         Default       Description          
------------------------------------------------------------------------------       
                          
 USAGE EXAMPLES:                              
------------------------------------------------------------------------------       
   
   SELECT TOP 100 * FROM Contract_Cu_Invoice_Map ORDER BY Cu_Invoice_Id DESC

   EXEC Cu_Invoice_Account_Recalc_Eligibility_Check @Account_Id = 1205897, @Cu_Invoice_Id =75254429

   SELECT
      *
FROM  Account_Exception ae
      JOIN
      dbo.Code st
            On st.Code_Id = ae.Exception_Status_Cd
			JOIN dbo.Code et ON et.Code_Id = ae.Exception_Type_Cd
			JOIN dbo.Account_Exception_Cu_Invoice_Map im ON im.Account_Exception_id = ae.Account_Exception_Id
WHERE Account_Id = 1205897

 AUTHOR INITIALS:          
         
 Initials              Name          
------------------------------------------------------------------------------       
 HG						Hariharasuthan Ganesan
                           
 MODIFICATIONS:        
            
 Initials              Date             Modification        
------------------------------------------------------------------------------       
 HG                    2019-11-18       Created for - Add Contract.  
 NR					   2020-02-20		MAINT-9640 - Changed If Count is 0 or null then Recalc fail else pass. 
										- For multi-month invoice (for both supplier and consolidated), the invoice can be associated to more than one contract 
 HG						2020-06-17		SE2017-981 Modified the procedure to add Overlapping_Sup_Acct_Exception_Type exception to check the eligbility of recalc                        
******/
CREATE PROCEDURE [dbo].[Cu_Invoice_Account_Recalc_Eligibility_Check]
(
	@Account_Id		 INT
	, @Cu_Invoice_Id INT
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE
		@Missing_Contract_Exception_Type_Cd INT
		, @ValCon_Setup_Exception_Type_Cd INT
		, @New_Exception_Status_Cd INT
		, @Inprogress_Exception_Status_Cd INT
		, @Outside_Contract_Term_Exception_Type_Cd INT
		, @Is_Eligible_For_Recalc BIT = 1
		, @Is_Consolidated_Billing BIT = 0
		, @Account_Type VARCHAR(200)
		, @Overlapping_Sup_Acct_Exception_Type_Cd INT;

	SELECT
		@Account_Type = act.ENTITY_NAME
	FROM
		dbo.ACCOUNT a
		INNER JOIN dbo.ENTITY act
			ON act.ENTITY_ID = a.ACCOUNT_TYPE_ID
	WHERE
		a.ACCOUNT_ID = @Account_Id;

	SELECT
		@Is_Consolidated_Billing = 1
	FROM
		dbo.Account_Consolidated_Billing_Vendor acb
		INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH sm
			ON sm.Account_ID = acb.Account_Id
	WHERE
		acb.Account_Id = @Account_Id
		AND sm.CU_INVOICE_ID = @Cu_Invoice_Id
		AND sm.SERVICE_MONTH BETWEEN acb.Billing_Start_Dt AND ISNULL(acb.Billing_End_Dt, '9999/12/31');

	IF(
		  @Account_Type <> 'Utility' OR @Is_Consolidated_Billing = 1
	  )
	BEGIN
		SELECT
			@Missing_Contract_Exception_Type_Cd = c.Code_Id
		FROM
			dbo.Code c
			INNER JOIN dbo.Codeset cs
				ON cs.Codeset_Id = c.Codeset_Id
		WHERE
			c.Code_Value = 'Missing Contract'
			AND cs.Codeset_Name = 'Exception Type'
			AND cs.Std_Column_Name = 'Exception_Type_Cd';

		SELECT
			@ValCon_Setup_Exception_Type_Cd = c.Code_Id
		FROM
			dbo.Code c
			INNER JOIN dbo.Codeset cs
				ON cs.Codeset_Id = c.Codeset_Id
		WHERE
			c.Code_Value = 'Missing Valcon Setup'
			AND cs.Codeset_Name = 'Exception Type'
			AND cs.Std_Column_Name = 'Exception_Type_Cd';

		SELECT
			@Outside_Contract_Term_Exception_Type_Cd = c.Code_Id
		FROM
			dbo.Code c
			INNER JOIN dbo.Codeset cs
				ON cs.Codeset_Id = c.Codeset_Id
		WHERE
			cs.Codeset_Name = 'Exception Type'
			AND cs.Std_Column_Name = 'Exception_Type_Cd'
			AND c.Code_Value = 'Outside Contract Term';

		SELECT
			@Overlapping_Sup_Acct_Exception_Type_Cd = c.Code_Id
		FROM
			dbo.Code c
			INNER JOIN dbo.Codeset cs
				ON cs.Codeset_Id = c.Codeset_Id
		WHERE
			cs.Codeset_Name = 'Exception Type'
			AND cs.Std_Column_Name = 'Exception_Type_Cd'
			AND c.Code_Value = 'Overlapping Sup Account';


		SELECT
			@New_Exception_Status_Cd = MAX(CASE WHEN c.Code_Value = 'New' THEN c.Code_Id END)
			, @Inprogress_Exception_Status_Cd = MAX(CASE WHEN c.Code_Value = 'In Progress' THEN c.Code_Id END)
		FROM
			dbo.Code c
			INNER JOIN dbo.Codeset cs
				ON cs.Codeset_Id = c.Codeset_Id
		WHERE
			c.Code_Value IN ( 'New', 'In Progress' )
			AND cs.Codeset_Name = 'Exception Status'
			AND cs.Std_Column_Name = 'Exception_Status_Cd';

		SELECT
			@Is_Eligible_For_Recalc = 0
		FROM
			dbo.Account_Exception ae
			INNER JOIN dbo.Account_Exception_Cu_Invoice_Map aecim
				ON aecim.Account_Exception_Id = ae.Account_Exception_Id
		WHERE
			ae.Account_Id = @Account_Id
			AND ae.Exception_Status_Cd IN ( @New_Exception_Status_Cd, @Inprogress_Exception_Status_Cd )
			AND aecim.Status_Cd IN ( @New_Exception_Status_Cd, @Inprogress_Exception_Status_Cd )
			AND ae.Exception_Type_Cd IN ( @Missing_Contract_Exception_Type_Cd
										  , @Outside_Contract_Term_Exception_Type_Cd
										  , @Overlapping_Sup_Acct_Exception_Type_Cd
										)
			AND @Is_Eligible_For_Recalc = 1
			AND aecim.Cu_Invoice_Id = @Cu_Invoice_Id;

		SELECT
			@Is_Eligible_For_Recalc = CASE WHEN ISNULL(x.Cnt, 0) = 0 THEN 0 ELSE 1 END
		FROM(
				SELECT
					COUNT(1) Cnt
				FROM
					dbo.Valcon_Account_Config ae
					INNER JOIN dbo.Contract_Cu_Invoice_Map cim
						ON cim.Account_Id = ae.Account_Id AND cim.Contract_Id = ae.Contract_Id
				WHERE
					ae.Account_Id = @Account_Id AND cim.Cu_Invoice_Id = @Cu_Invoice_Id AND ae.Is_Config_Complete = 1
			) x
		WHERE
			@Is_Eligible_For_Recalc = 1;
	END;
	ELSE -- Non Consolidated Billing Utility Account
	BEGIN
		SET @Is_Eligible_For_Recalc = 1;
	END;
	SELECT @Is_Eligible_For_Recalc AS Is_Eligible_For_Recalc;

END;
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Account_Recalc_Eligibility_Check] TO [CBMSApplication]
GO
