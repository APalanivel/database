
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UBM_UPDATE_AVISTA_CONSOLIDATION_ID_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@consolidation 	VARCHAR(50)       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC UBM_UPDATE_AVISTA_CONSOLIDATION_ID_P '03052014'


SELECT TOP 10 * FROM UBM_AVISTA_CONTROL ORDER BY UBM_BATCH_MASTER_LOG_ID DESC

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hariharasuthan Ganesan

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG			2014-04-17	Ecova enhancement- Update statement added to update the consolidation id in Ubm_Avista_Control table as well
	HG			2014-09-05	Modified to update the UBM_AVISTA_CONTROL table only for the records CONSOLIDATION_ID IS NULL so that multiple files in a single batch can have different consolidation id.
******/
CREATE  PROCEDURE [dbo].[UBM_UPDATE_AVISTA_CONSOLIDATION_ID_P]
      @CONSOLIDATION_ID VARCHAR(50)
AS 
BEGIN

      SET NOCOUNT ON

      UPDATE
            UBM_AVISTA_FEED
      SET   
            CONSOLIDATION_ID = @CONSOLIDATION_ID
      WHERE
            UBM_BATCH_MASTER_LOG_ID IS NULL
            AND CONSOLIDATION_ID IS NULL
      
      UPDATE
            dbo.UBM_AVISTA_CONTROL
      SET   
            CONSOLIDATION_ID = @CONSOLIDATION_ID
      WHERE
            UBM_BATCH_MASTER_LOG_ID IS NULL
            AND CONSOLIDATION_ID IS NULL

END

;
GO


GRANT EXECUTE ON  [dbo].[UBM_UPDATE_AVISTA_CONSOLIDATION_ID_P] TO [CBMSApplication]
GO
