SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Contract_Outside_Contract_Term_Config_Sel          
              
Description:              
        To insert Data into  table.              
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
     @Contract_Outside_Contract_Term_Config_Id						INT
    
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                
	
	

    EXEC dbo.Contract_Outside_Contract_Term_Config_Sel 
      @Contract_Id =1148520 
	  ,@Valcon_Account_Config_Type_Cd = 1   
          
           EXEC dbo.Contract_Outside_Contract_Term_Config_Sel 
      @Contract_Id =172758    
          
		  
		        
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2019-06-26		Created For Add contract.   
             

******/
CREATE PROCEDURE [dbo].[Contract_Outside_Contract_Term_Config_Sel]
    (
        @Contract_Id INT
        , @Valcon_Account_Config_Type_Cd INT
    )
AS
    BEGIN
        SET NOCOUNT ON;
        SELECT
            'Contract Level' AS Level_Configured
            , aloct.Contract_Outside_Contract_Term_Config_Id
            , aloct.OCT_Parameter_Cd
            , c.Code_Value AS OCT_Parameter
            , aloct.OCT_Tolerance_Date_Cd
            , c2.Code_Value AS OCT_Tolerance_Date
            , aloct.OCT_Dt_Range_Cd
            , c3.Code_Value AS OCT_Dt_Range
            , aloct.Config_Start_Dt
            , aloct.Config_End_Dt
        FROM
            dbo.Valcon_Contract_Config vcc
            INNER JOIN dbo.Contract_Outside_Contract_Term_Config aloct
                ON aloct.Valcon_Contract_Config_Id = vcc.Valcon_Contract_Config_Id
            INNER JOIN dbo.Code c
                ON c.Code_Id = aloct.OCT_Parameter_Cd
            INNER JOIN dbo.Code c2
                ON c2.Code_Id = aloct.OCT_Tolerance_Date_Cd
            INNER JOIN dbo.Code c3
                ON c3.Code_Id = aloct.OCT_Dt_Range_Cd
        WHERE
            vcc.Contract_Id = @Contract_Id
            AND vcc.Valcon_Account_Config_Type_Cd = @Valcon_Account_Config_Type_Cd;
    END;




GO
GRANT EXECUTE ON  [dbo].[Contract_Outside_Contract_Term_Config_Sel] TO [CBMSApplication]
GO
