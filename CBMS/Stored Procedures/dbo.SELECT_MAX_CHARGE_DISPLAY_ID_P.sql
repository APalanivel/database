SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE dbo.SELECT_MAX_CHARGE_DISPLAY_ID_P
	@rateId	INT,
	@chargeType VARCHAR(200),
	@chargeParentType INT,
	@parentType	VARCHAR(200)
AS
BEGIN

	SET NOCOUNT ON

	SELECT MAX(CAST(SUBSTRING (CM.CHARGE_DISPLAY_ID,2,4) AS INT)) AS chgDispID
	FROM dbo.CHARGE_MASTER cm INNER JOIN dbo.Charge ch ON ch.charge_master_id = cm.charge_master_id
		INNER JOIN dbo.entity ent1 ON ent1.entity_id = cm.charge_type_id
		INNER JOIN dbo.entity ent2 ON ent2.entity_id = cm.charge_type_id
	WHERE cm.CHARGE_PARENT_ID = @rateId
		AND ent1.entity_name = @chargeType
		AND ent1.entity_type = 113
		AND ent2.entity_name = @parentType
		AND ent2.entity_type = @chargeParentType

END
GO
GRANT EXECUTE ON  [dbo].[SELECT_MAX_CHARGE_DISPLAY_ID_P] TO [CBMSApplication]
GO
