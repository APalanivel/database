SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.SR_RFP_LP_GET_ACCOUNT_MONTH_SELECTOR_P
	@rfp_id int,
	@rfp_account_id int
	AS
	if @rfp_account_id > 0
	begin
		select  setup.sr_rfp_account_id,
			en.entity_name
		from 	sr_rfp_load_profile_setup setup(nolock),
			entity en(nolock),
			sr_rfp_account rfp_account
		where 	setup.sr_rfp_id = @rfp_id
			and setup.sr_rfp_account_id = @rfp_account_id
			and rfp_account.sr_rfp_account_id = setup.sr_rfp_account_id
			and rfp_account.is_deleted = 0
			and setup.month_selector_type_id = en.entity_id
	end
	else
	begin
		select  setup.sr_rfp_account_id,
			en.entity_name
		from 	sr_rfp_load_profile_setup setup(nolock),
			entity en(nolock)
		where 	setup.sr_rfp_id = @rfp_id
			and setup.month_selector_type_id = en.entity_id
	end
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_LP_GET_ACCOUNT_MONTH_SELECTOR_P] TO [CBMSApplication]
GO
