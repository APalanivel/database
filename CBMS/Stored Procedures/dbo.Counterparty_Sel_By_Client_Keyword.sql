SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                
NAME:                                
    dbo.Counterparty_Sel_By_Client_Keyword                                
                                
DESCRIPTION:                                
                           
                                
INPUT PARAMETERS:                                
	Name				DataType		Default Description                                
---------------------------------------------------------------
	@Client_Id			INT
    @Commodity_Id		INT
    @Start_Dt			DATE
    @End_Dt				DATE
    @Hedge_Type			INT
                          
                       
OUTPUT PARAMETERS:                                
 Name   DataType  Default Description                                
---------------------------------------------------------------    

	EXEC dbo.Counterparty_Sel_By_Client_Keyword @Client_Id = 11236,@Keyword = 'citi'
	EXEC dbo.Counterparty_Sel_By_Client_Keyword @Client_Id = 11236,@Keyword = 'America'
	EXEC dbo.Counterparty_Sel_By_Client_Keyword @Client_Id = 11236,@Keyword = 'siva'
	EXEC dbo.Counterparty_Sel_By_Client_Keyword @Client_Id = 10003,@Keyword = 'nam t'
	               
 
USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
                   
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	RR			2018-10-25  Created For Risk Management

******/
CREATE PROCEDURE [dbo].[Counterparty_Sel_By_Client_Keyword]
      ( 
       @Client_Id INT
      ,@Keyword VARCHAR(100) = NULL )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @SerachKeyword VARCHAR(100)
     
      SELECT
            @SerachKeyword = '%' + @Keyword + '%'
      
      
      SELECT
            rc.RM_COUNTERPARTY_ID
           ,rc.COUNTERPARTY_NAME
           ,ci.First_Name + ' ' + ci.Last_Name AS NAME
           ,Email_Address
           ,Phone_Number
           ,Mobile_Number
           ,ci.Contact_Info_Id
      FROM
            dbo.RM_COUNTERPARTY rc
            INNER JOIN dbo.Counterparty_Client_Contact_Map ccm
                  ON rc.RM_COUNTERPARTY_ID = ccm.Counterparty_Id
            INNER JOIN dbo.Contact_Info ci
                  ON ccm.Contact_Info_Id = ci.Contact_Info_Id
      WHERE
            ccm.Client_Id = @Client_Id
            AND ( @Keyword IS NULL
                  OR ( rc.COUNTERPARTY_NAME LIKE @SerachKeyword
                       OR ci.First_Name LIKE @SerachKeyword
                       OR ci.Last_Name LIKE @SerachKeyword
                       OR ci.First_Name + ' ' + ci.Last_Name LIKE @SerachKeyword ) )

END;


GO
GRANT EXECUTE ON  [dbo].[Counterparty_Sel_By_Client_Keyword] TO [CBMSApplication]
GO
