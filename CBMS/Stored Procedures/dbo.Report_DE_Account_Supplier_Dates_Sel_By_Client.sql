SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********         
  
NAME:  
     dbo.Report_DE_Account_Supplier_Dates_Sel_By_Client  
         
DESCRIPTION:            
     
        
 INPUT PARAMETERS:            
                       
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
 @Client_Name                VARCHAR(200)  
   
 OUTPUT PARAMETERS:            
                             
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
            
 USAGE EXAMPLES:                              
---------------------------------------------------------------------------------------------------------------                              
   
 EXEC  dbo.Report_DE_Account_Supplier_Dates_Sel_By_Client 'city of boston'  
    
        
 AUTHOR INITIALS:          
         
 Initials              Name          
---------------------------------------------------------------------------------------------------------------                        
 AKR                   Ashok Kumar Raju          
  
    
 MODIFICATIONS:        
            
 Initials              Date             Modification        
---------------------------------------------------------------------------------------------------------------        
 AKR                   2014-06-01      Created.  
*********/  
  
CREATE PROCEDURE dbo.Report_DE_Account_Supplier_Dates_Sel_By_Client
      ( 
       @Client_Name VARCHAR(200) )
AS 
BEGIN  
      SET NOCOUNT ON   
  
      DECLARE @Client_Id INT   
        
      SELECT
            @Client_Id = c.CLIENT_ID
      FROM
            dbo.CLIENT c
      WHERE
            CLIENT_NAME = @Client_Name  
   
      SELECT
            Client_Name [Client]
           ,ca.Account_Type [Account Type]
           ,site_name [Site]
           ,ca.display_account_number [Account Number]
           ,ca.Account_Number [Account]
           ,ca.Supplier_Account_begin_Dt [SAStDate]
           ,ca.Supplier_Account_End_Dt [SAEndDate]
           ,com.Commodity_Name [Commodity]
      FROM
            Core.Client_Hier_Account ca
            JOIN Core.Client_Hier ch
                  ON ch.Client_Hier_Id = ca.Client_Hier_Id
            JOIN Commodity com
                  ON com.Commodity_Id = ca.Commodity_Id
      WHERE
            ch.Client_Id = @Client_Id
      GROUP BY
            Client_Name
           ,ca.Account_Type
           ,site_name
           ,ca.display_account_number
           ,ca.Account_Number
           ,ca.Supplier_Account_begin_Dt
           ,ca.Supplier_Account_End_Dt
           ,com.Commodity_Name
      ORDER BY
            Site_name
           ,Account_Type
           ,Account_Number  
END  

;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Account_Supplier_Dates_Sel_By_Client] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_Account_Supplier_Dates_Sel_By_Client] TO [CBMSReports]
GO
