SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--select * from user_info where user_info_id = 3525
--select * from user_info where user_info_id = 9801
--select * from sr_supplier_contact_info where sr_supplier_contact_info_id = 15 
--select * from sr_rfp_supplier_contact_vendor_map where sr_Rfp_id = 13 and vendor_id = 741
--select * from sr_rfp_supplier_contact_vendor_map where sr_Rfp_id = 736 and vendor_id = 741
--select * from sr_supplier_contact_info where sr_supplier_contact_info_id = 22 
--select * from sr_supplier_contact_info where sr_supplier_contact_info_id = 631


-- exec SR_SW_REP_GET_RFP_FEEDBACK_REPORT_DATA_P '14698',' ',' ',' ',0,0,0,0,741,0,0
-- exec SR_SW_REP_GET_RFP_FEEDBACK_REPORT_DATA_P '14698',' ','','1/1/2008',0,0,0,0,741,0,0

-- exec dbo.SR_SW_REP_GET_RFP_FEEDBACK_REPORT_DATA_P '0','','','',0,0,0,0,0,0,0

CREATE PROCEDURE dbo.SR_SW_REP_GET_RFP_FEEDBACK_REPORT_DATA_P
	@userId VARCHAR(20),
	@sessionId VARCHAR(20),
	@fromDate VARCHAR(20),
	@todate VARCHAR(20),
	@commodityId INT,
	@regionId INT,
	@stateId INT,
	@utilityId INT,
	@supplierId INT,
	@analystId INT,
	@rfpId INT 

AS
BEGIN

	SET NOCOUNT ON

	DECLARE @selectClause001 VARCHAR(8000)
	DECLARE @fromClause001 VARCHAR(8000)
	DECLARE @whereClause001 VARCHAR(8000)	
	DECLARE @groupClause001 VARCHAR(8000)
	DECLARE @SQLStatement001 VARCHAR(8000)

	DECLARE @selectClause002 VARCHAR(8000)
	DECLARE @fromClause002 VARCHAR(8000)
	DECLARE @whereClause002 VARCHAR(8000)	
	DECLARE @groupClause002 VARCHAR(8000)
	DECLARE @SQLStatement002 VARCHAR(8000)

	if @fromDate is null
	begin
		set @fromDate = ''
	end
	if @todate is null
	begin
		set @todate = ''
	end

	SELECT @selectClause001 =' SR_RFP.SR_RFP_ID as RFP_ID, 
								SR_RFP_REASON_NOT_WINNING.COMMENTS as COMMENTS,
								supplier.VENDOR_NAME AS SUPPLIER_NAME, 
								utility.VENDOR_NAME AS UTILITY_NAME,
								reason.ENTITY_NAME AS REASON, 
								--STATE.STATE_NAME, REGION.REGION_NAME, 
								site.site_name AS CITY_STATE,
								client.CLIENT_NAME as CLIENT_NAME,
								commodity.ENTITY_NAME AS COMMODITY,
								ACCOUNT.ACCOUNT_NUMBER AS BID_GROUP,  
								USER_INFO.FIRST_NAME + SPACE(1) + USER_INFO.LAST_NAME AS ANALYST '

	SELECT @selectClause002 =' SR_RFP.SR_RFP_ID as RFP_ID,
							SR_RFP_REASON_NOT_WINNING.COMMENTS as COMMENTS,
							supplier.VENDOR_NAME AS SUPPLIER_NAME, 
							utility.VENDOR_NAME AS UTILITY_NAME,
							reason.ENTITY_NAME AS REASON, 
							--STATE.STATE_NAME, REGION.REGION_NAME, 
							site.site_name AS CITY_STATE,
							client.CLIENT_NAME as CLIENT_NAME,
							commodity.ENTITY_NAME AS COMMODITY,
							SR_RFP_BID_GROUP.GROUP_NAME AS BID_GROUP, 
							USER_INFO.FIRST_NAME + SPACE(1) + USER_INFO.LAST_NAME AS ANALYST '



	SELECT @fromClause001= ' SR_RFP_ACCOUNT 
						JOIN SR_RFP 
							ON SR_RFP_ACCOUNT.SR_RFP_ID = SR_RFP.SR_RFP_ID
						JOIN sr_rfp_checklist rfpChecklist 
							ON SR_RFP_ACCOUNT.sr_rfp_account_id = rfpChecklist.sr_rfp_account_id							
						JOIN SR_RFP_REASON_NOT_WINNING 
							ON SR_RFP_ACCOUNT.SR_RFP_ACCOUNT_ID = SR_RFP_REASON_NOT_WINNING.SR_ACCOUNT_GROUP_ID 							
						JOIN SR_RFP_REASON_NOT_WINNING_MAP 
							ON SR_RFP_REASON_NOT_WINNING_MAP.SR_RFP_REASON_NOT_WINNING_ID = SR_RFP_REASON_NOT_WINNING.SR_RFP_REASON_NOT_WINNING_ID						
						JOIN SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP 
							ON SR_RFP_REASON_NOT_WINNING.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
						JOIN sr_supplier_contact_info info 
							on SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP.sr_supplier_contact_info_id = info.sr_supplier_contact_info_id
						JOIN user_info uinfo 
							on info.user_info_id = uinfo.user_info_id	
						JOIN VENDOR supplier 
							ON SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP.VENDOR_ID = supplier.VENDOR_ID
						JOIN ACCOUNT ON 
							SR_RFP_ACCOUNT.ACCOUNT_ID = ACCOUNT.ACCOUNT_ID
						JOIN VENDOR utility 
							ON ACCOUNT.VENDOR_ID = utility.VENDOR_ID 
						JOIN ENTITY reason 
							ON SR_RFP_REASON_NOT_WINNING_MAP.NOT_WINNING_REASON_TYPE_ID = reason.ENTITY_ID 
						JOIN ENTITY commodity 
							ON SR_RFP.COMMODITY_TYPE_ID = commodity.ENTITY_ID 
						JOIN USER_INFO 
							ON SR_RFP.INITIATED_BY = USER_INFO.USER_INFO_ID 
						JOIN vwSiteName site 
							ON ACCOUNT.SITE_ID = site.SITE_ID 
						JOIN CLIENT client 
							on site.CLIENT_ID = client.CLIENT_ID '


	SELECT @fromClause002= ' SR_RFP_ACCOUNT
						JOIN SR_RFP
							ON SR_RFP_ACCOUNT.SR_RFP_ID = SR_RFP.SR_RFP_ID
						JOIN sr_rfp_checklist rfpChecklist
							on SR_RFP_ACCOUNT.sr_rfp_account_id = rfpChecklist.sr_rfp_account_id
						JOIN ENTITY commodity
							ON SR_RFP.COMMODITY_TYPE_ID = commodity.ENTITY_ID
						JOIN SR_RFP_BID_GROUP
							ON SR_RFP_ACCOUNT.SR_RFP_BID_GROUP_ID = SR_RFP_BID_GROUP.SR_RFP_BID_GROUP_ID
						JOIN ACCOUNT 
							ON ACCOUNT.ACCOUNT_ID = SR_RFP_ACCOUNT.ACCOUNT_ID	
						JOIN VENDOR utility
							ON ACCOUNT.VENDOR_ID = utility.VENDOR_ID				
						JOIN SR_RFP_REASON_NOT_WINNING 
							ON SR_RFP_ACCOUNT.SR_RFP_BID_GROUP_ID = SR_RFP_REASON_NOT_WINNING.SR_ACCOUNT_GROUP_ID							
						JOIN SR_RFP_REASON_NOT_WINNING_MAP 
							ON SR_RFP_REASON_NOT_WINNING_MAP.SR_RFP_REASON_NOT_WINNING_ID = SR_RFP_REASON_NOT_WINNING.SR_RFP_REASON_NOT_WINNING_ID
						JOIN SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP
							ON SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = SR_RFP_REASON_NOT_WINNING.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID							
						JOIN sr_supplier_contact_info info 
							on SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP.sr_supplier_contact_info_id = info.sr_supplier_contact_info_id
						JOIN user_info uinfo 
							on info.user_info_id = uinfo.user_info_id
						JOIN ENTITY reason
							ON reason.ENTITY_ID = SR_RFP_REASON_NOT_WINNING_MAP.NOT_WINNING_REASON_TYPE_ID							
						JOIN VENDOR supplier
							ON SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP.VENDOR_ID = supplier.VENDOR_ID 
						JOIN UTILITY_DETAIL
							ON utility.VENDOR_ID = UTILITY_DETAIL.VENDOR_ID
						JOIN UTILITY_ANALYST_MAP
							ON UTILITY_DETAIL.UTILITY_DETAIL_ID = UTILITY_ANALYST_MAP.UTILITY_DETAIL_ID
						JOIN USER_INFO
							ON UTILITY_ANALYST_MAP.USER_INFO_ID = USER_INFO.USER_INFO_ID
						JOIN vwSiteName site
							ON ACCOUNT.SITE_ID = site.SITE_ID
						JOIN CLIENT client
							on site.CLIENT_ID = client.CLIENT_ID '



	SELECT @whereClause001 = ' SR_RFP_REASON_NOT_WINNING.IS_BID_GROUP = 0 and uinfo.is_history = 0 AND SR_RFP_ACCOUNT.is_deleted = 0 '

	SELECT @whereClause002 = ' SR_RFP_REASON_NOT_WINNING.IS_BID_GROUP = 1 and uinfo.is_history = 0 and SR_RFP_ACCOUNT.is_deleted = 0 '

	/*select @groupClause= '	
				GROUP BY VENDOR.VENDOR_NAME,supplier.VENDOR_NAME ,USER_INFO.FIRST_NAME+' '+USER_INFO.LAST_NAME, ENTITY.ENTITY_NAME,STATE.STATE_NAME,REGION.REGION_NAME
				ORDER BY  supplier.VENDOR_NAME
			     '*/



/*select * from entity where entity_type = 1013'Bid Placed','Processing'
select entity_name from entity  where entity_id in(  select status_type_id from SR_RFP_SEND_SUPPLIER)

select entity_id from entity where entity_name in ('Processing','Bid Placed') and entity_type=1029*/


/*	if (@rfpId =1)
	begin
		select @whereClause001= @whereClause001 + ' and SR_RFP.SR_RFP_ID ='+str(@rfpId)		
		select @whereClause002= @whereClause002 + ' and SR_RFP.SR_RFP_ID ='+str(@rfpId)		
	end
*/

	if (@rfpId <> 0)
	begin
		select @whereClause001= @whereClause001 + ' and SR_RFP.SR_RFP_ID ='+str(@rfpId)		
		select @whereClause002= @whereClause002 + ' and SR_RFP.SR_RFP_ID ='+str(@rfpId)		
	end



	If @commodityId<>0
	begin
		select @whereClause001= @whereClause001 + ' and SR_RFP.COMMODITY_TYPE_ID = '+STR(@commodityId)
		select @whereClause002= @whereClause002 + ' and SR_RFP.COMMODITY_TYPE_ID = '+STR(@commodityId)
	end

	If @regionId<>0
	begin
		select @whereClause001= @whereClause001 + ' and REGION.REGION_ID = '+STR(@regionId)
		select @whereClause002= @whereClause002 + ' and REGION.REGION_ID = '+STR(@regionId)
	end


	If @utilityId<>0
	begin
		select @whereClause001= @whereClause001 + ' and utility.VENDOR_ID = '+STR(@utilityId)
		select @whereClause002= @whereClause002 + ' and utility.VENDOR_ID = '+STR(@utilityId)
	end


	if @stateId<>0
	begin
		SELECT @fromClause001 = @fromClause001 + ' JOIN VENDOR_STATE_MAP ON utility.VENDOR_ID = VENDOR_STATE_MAP.VENDOR_ID JOIN
					                      STATE ON VENDOR_STATE_MAP.STATE_ID = STATE.STATE_ID '
		SELECT @fromClause002 = @fromClause002 + ' JOIN VENDOR_STATE_MAP ON utility.VENDOR_ID = VENDOR_STATE_MAP.VENDOR_ID JOIN
					                      STATE ON VENDOR_STATE_MAP.STATE_ID = STATE.STATE_ID '               


		select @whereClause001= @whereClause001 + ' and VENDOR_STATE_MAP.STATE_ID = '+STR(@stateId)		
		select @whereClause002= @whereClause002 + ' and VENDOR_STATE_MAP.STATE_ID = '+STR(@stateId)		
	end


	if @supplierId<>0
	begin
		select @whereClause001= @whereClause001 + ' and supplier.VENDOR_ID = '+STR(@supplierId)		
		select @whereClause002= @whereClause002 + ' and supplier.VENDOR_ID = '+STR(@supplierId)		
	end

	if @analystId<>0
	begin
		select @whereClause001= @whereClause001 + ' and SR_RFP.INITIATED_BY = '+STR(@analystId)	
		select @whereClause002= @whereClause002 + ' and SR_RFP.INITIATED_BY = '+STR(@analystId)	
	end

	/*if(@fromDate <> '' and @toDate <> '')
		begin
				
				select @whereClause001= @whereClause001 --+' and  SR_RFP_SEND_SUPPLIER_LOG.GENERATION_DATE BETWEEN ''+CONVERT(VARCHAR(12), @fromDate,101)+''  AND ''+ CONVERT(VARCHAR(12), @toDate,101)+'''

		end
	else if (@fromDate ='' and @toDate <>'')
		begin

			select @whereClause001= @whereClause001 --+' SR_RFP_SEND_SUPPLIER_LOG.GENERATION_DATE <= ''+CONVERT(VARCHAR(12), @toDate,101)+'' '
		end
	else if (@fromDate <> '' and @toDate ='')
		begin

			select @whereClause001= @whereClause001 --+' SR_RFP_SEND_SUPPLIER_LOG.GENERATION_DATE >= ''+CONVERT(VARCHAR(12), @fromDate,101)+'''
		end		*/

	if(@fromDate <> '' and @toDate <> '')
		begin
				
				--select @whereClause001= @whereClause001 +" and  SR_RFP_SEND_SUPPLIER_LOG.GENERATION_DATE BETWEEN '"+CONVERT(Varchar(12), @fromDate,101)+"'  AND '"+ CONVERT(Varchar(12), @toDate,101)+"'"
--				select @whereClause001= @whereClause001 +" and  rfpChecklist.rfp_initiated_date BETWEEN '"+CONVERT(Varchar(12), @fromDate,101)+"'  AND '"+ CONVERT(Varchar(12), @toDate,101)+"'"				
--				select @whereClause002= @whereClause002 +" and  rfpChecklist.rfp_initiated_date BETWEEN '"+CONVERT(Varchar(12), @fromDate,101)+"'  AND '"+ CONVERT(Varchar(12), @toDate,101)+"'"

				select @whereClause001= @whereClause001 +' and  rfpChecklist.rfp_initiated_date BETWEEN '+ '''' + CONVERT(Varchar(12), @fromDate,101)+ '''' + ' AND ' + ''''+ CONVERT(Varchar(12), @toDate,101) + ''''
				select @whereClause002= @whereClause002 +' and  rfpChecklist.rfp_initiated_date BETWEEN '+ '''' + CONVERT(Varchar(12), @fromDate,101)+  '''' + ' AND ' + ''''+ CONVERT(Varchar(12), @toDate,101) + ''''


		end
	else if (@fromDate ='' and @toDate <>'')
		begin

			--select @whereClause001= @whereClause001 +" SR_RFP_SEND_SUPPLIER_LOG.GENERATION_DATE <= '"+CONVERT(Varchar(12), @toDate,101)+"' "
--			select @whereClause001= @whereClause001 +" AND rfpChecklist.rfp_initiated_date <= '"+CONVERT(Varchar(12), @toDate,101)+"' "
--			select @whereClause002= @whereClause002 +" AND rfpChecklist.rfp_initiated_date <= '"+CONVERT(Varchar(12), @toDate,101)+"' "

			select @whereClause001= @whereClause001 +' AND rfpChecklist.rfp_initiated_date <= ' + ''''+CONVERT(Varchar(12), @toDate,101)+ ''''
			select @whereClause002= @whereClause002 +' AND rfpChecklist.rfp_initiated_date <= ' + ''''+CONVERT(Varchar(12), @toDate,101)+ ''''

		end
	else if (@fromDate <> '' and @toDate ='')
		begin

			--select @whereClause001= @whereClause001 +" SR_RFP_SEND_SUPPLIER_LOG.GENERATION_DATE >= '"+CONVERT(Varchar(12), @fromDate,101)+"'"
--			select @whereClause001= @whereClause001 +" AND rfpChecklist.rfp_initiated_date >= '"+CONVERT(Varchar(12), @fromDate,101)+"'"
--			select @whereClause002= @whereClause002 +" AND rfpChecklist.rfp_initiated_date >= '"+CONVERT(Varchar(12), @fromDate,101)+"'"

			select @whereClause001= @whereClause001 +' AND rfpChecklist.rfp_initiated_date >= '+ '''' + CONVERT(Varchar(12), @fromDate,101) + ''''
			select @whereClause002= @whereClause002 +' AND rfpChecklist.rfp_initiated_date >= '+ '''' + CONVERT(Varchar(12), @fromDate,101) + ''''

		end		

	--PRINT @SQLStatement

	SELECT @SQLStatement001 =	'SELECT ' +
				@selectClause001 + 
				' FROM ' + 
				@fromClause001 +
				 ' WHERE ' +
				@whereClause001 --+@groupClause

	SELECT @SQLStatement002 =	'SELECT ' +
				@selectClause002 + 
				' FROM ' + 
				@fromClause002 +
				 ' WHERE ' +
				@whereClause002 --+@groupClause


	select @SQLStatement001=	@SQLStatement001+' UNION '+@SQLStatement002
 

	print @SQLStatement001
	EXEC(@SQLStatement001)

END
GO
GRANT EXECUTE ON  [dbo].[SR_SW_REP_GET_RFP_FEEDBACK_REPORT_DATA_P] TO [CBMSApplication]
GO
