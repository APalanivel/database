
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 /******      
NAME:      
 cbms_prod.dbo.GET_RA_QUEUE_ITEM_COUNT      
      
DESCRIPTION:      
  
Private Queue  (User queue count in queue for more than 2 days)  
   
 Incoming Private Count                -  Count  of DISTINCT INVOICE_ID when Is_Manual = 1 in Cu_Invoice  
   
 Exception Private Count               -  Count of DISTINCT INVOICE ID for exception type <>  "Failed Recalc"   
              and Is_Manual = 0 in Cu_Invoice   
                                             Count of DISTINCT INVOICE_ID, Account_id for exception type = "Failed Recalc"   
              and Is_Manual = 0 in Cu_Invoice   
  
Public Queue ( User group queue count and in queue for more than 7 days)  
   
 Incoming_public_count                -  Count of DISTINCT INVOICE_ID when Is_Manual = 1 in Cu_Invoice  
   
 Exception Public Count               -  Count of DISTINCT INVOICE_ID when Is_Manual = 0 in Cu_Invoice  
  
Here we are assuming that by the time a ?Failed Recalc? could arise in an Invoice it is typically in the private queue   
(of some user), and not in the public queue. Typically, public queue cases would not reach to the point of recalc test being executed;  
  
  
      
      
INPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
 @userId         int                         
      
OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
      
USAGE EXAMPLES:      
------------------------------------------------------------      
  
 EXEC GET_RA_QUEUE_ITEM_COUNT 7789    
 EXEC GET_RA_QUEUE_ITEM_COUNT 41    
  
 EXEC dbo.GET_RA_QUEUE_ITEM_COUNT 18653  
 EXEC dbo.GET_RA_QUEUE_ITEM_COUNT 78 
 
 Exec GET_RA_QUEUE_ITEM_COUNT 14832 
   
AUTHOR INITIALS:  
INITIALS	NAME  
------------------------------------------------------------  
SKA			Shobhit Kumar Agrawal  
HG			Harihara Suthan G  
NR			Narayana Reddy
  
MODIFICATION  
INITIALS	DATE			MODIFICATION  
------------------------------------------------------------  
SKA			10/21/2010		Created  
SKA			12/7/2010		modified the logic for @incoming_private_Count based on Reclac (Bug#21882)  
SKA			12/14/2010		Modified the condition for @incoming_private_Count  
HG			12/14/2010		Removed the Binary check sum function which used to get DISTINCT count of invoices based on Invoice_id, Accountid for Failed Recalc exception type.  
							Logic changed to save the cu_exception denorm values in a table variable then select the data from table variable for each condition for private count.  
							Updated the comments header with the logic to get the counts  
SP			2014-07-09		Data Operations Enhancement Phase II ,Seperated Failed Recalc Count from Exception_private_count and incoming_private_Count   
							and showed it seperatly as Recalc_Count     
NR			2016-12-22		MAINT-4618 - In variance count added commodity_Id in group clause because variance page the data is 
							filtering by Account ID, Service Month & Commodity to find total records (count) in the application.
							and also join core.Client_Hier ch table in variance Count.				  
******/  
  
 CREATE PROCEDURE [dbo].[GET_RA_QUEUE_ITEM_COUNT] @UserId INT
 AS 
 BEGIN  
  
      SET NOCOUNT ON      
  
      DECLARE @queue_id INT      
      DECLARE @Current_Ts DATETIME      
      DECLARE
            @incoming_public_Count INT
           ,@incoming_private_Count INT
           ,@exception_public_Count INT
           ,@variance_Count INT
           ,@Recalc_Exception_Private_Count INT
           ,@Other_Exception_Private_Count INT  
  
      DECLARE @Cu_Exception_Private_Queue TABLE
            ( 
             Cu_Invoice_Id INT
            ,Account_Id INT
            ,Exception_Type VARCHAR(200)
            ,Is_Manual BIT
            ,Days_In_Queue INT )  
   
      SET @Current_Ts = GETDATE()  
   
      SELECT
            @queue_id = queue_id
      FROM
            dbo.User_info
      WHERE
            user_info_id = @userId  
   
      INSERT      INTO @Cu_Exception_Private_Queue
                  ( 
                   Cu_Invoice_Id
                  ,Account_Id
                  ,Exception_Type
                  ,Is_Manual
                  ,Days_In_Queue )
                  SELECT
                        Cu_Invoice_Id
                       ,Account_Id
                       ,Exception_Type
                       ,Is_Manual
                       ,DATEDIFF(dd, DATE_IN_QUEUE, @Current_Ts)
                  FROM
                        dbo.Cu_Exception_Denorm
                  WHERE
                        QUEUE_ID = @queue_id  
   
  
      SELECT
            @incoming_private_Count = COUNT(DISTINCT t.cu_invoice_id)
      FROM
            @Cu_Exception_Private_Queue t
      WHERE
            t.is_manual = 1
            AND Days_In_Queue >= 2
            AND Exception_Type != 'Failed Recalc'  
  
      SELECT
            @Recalc_Exception_Private_Count = COUNT(1)
      FROM
            ( SELECT
                  Cu_Invoice_id
                 ,Account_id
              FROM
                  @Cu_Exception_Private_Queue
              WHERE
                  Days_In_Queue >= 2
                  AND Exception_Type = 'Failed Recalc'
              GROUP BY
                  Cu_Invoice_id
                 ,Account_id ) Prv_cnt  
  
      SELECT
            @Other_Exception_Private_Count = COUNT(DISTINCT Cu_Invoice_Id)
      FROM
            @Cu_Exception_Private_Queue
      WHERE
            Days_In_Queue >= 2
            AND Is_Manual = 0
            AND Exception_Type != 'Failed Recalc'  
  
      SELECT
            @incoming_public_Count = ISNULL(SUM(incoming_public_Count), 0)
           ,@exception_public_Count = ISNULL(SUM(exception_public_Count), 0)
      FROM
            ( SELECT
                  incoming_public_Count = CASE WHEN t.is_manual = 1 THEN COUNT(DISTINCT t.cu_invoice_id)
                                          END
                 ,exception_public_Count = CASE WHEN t.is_manual = 0 THEN COUNT(DISTINCT t.cu_invoice_id)
                                           END
              FROM
                  dbo.user_info_group_info_map ugm
                  JOIN dbo.group_info gi
                        ON gi.group_info_id = ugm.group_info_id
                  JOIN dbo.cu_exception_denorm t
                        ON t.queue_id = gi.queue_id
              WHERE
                  ugm.user_info_id = @userId
                  AND @Current_Ts - t.date_in_queue >= 7
              GROUP BY
                  t.is_manual ) Pub_Cnt     
  
      SELECT
            @Variance_Count = ISNULL(SUM(Variance_Count), 0)
      FROM
            ( SELECT
                  1 AS Variance_Count
              FROM
                  dbo.variance_log l
                  INNER JOIN Core.Client_Hier ch
                        ON ch.Site_Id = l.Site_Id
              WHERE
                  queue_id = @queue_id
                  AND Closed_Dt IS NULL
                  AND Is_Failure = 1
                  AND @Current_Ts - queue_dt >= 4
              GROUP BY
                  l.account_id
                 ,l.service_month ) Var_Cnt  
  
      SELECT
            @incoming_public_Count AS Incoming_Public_Count
           ,@incoming_private_Count AS Incoming_Private_Count
           ,@exception_public_Count AS Exception_Public_Count
           ,@Other_Exception_Private_Count AS Exception_Private_Count
           ,@variance_Count AS variance_Count
           ,@Recalc_Exception_Private_Count AS Recalc_Count  
  
 END;

;
GO


GRANT EXECUTE ON  [dbo].[GET_RA_QUEUE_ITEM_COUNT] TO [CBMSApplication]
GO
