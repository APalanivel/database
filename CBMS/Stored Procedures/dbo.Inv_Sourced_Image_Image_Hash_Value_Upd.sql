SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
             
NAME: Inv_Sourced_Image_Image_Hash_Value_Upd

DESCRIPTION:

 To update image hash value in Inv_Sourced_Image table ( Used by the initial upd tool)

INPUT PARAMETERS:
NAME							DATATYPE		DEFAULT  DESCRIPTION
--------------------------------------------------------------------
@Actual_File_Name				VARCHAR(200)
@Archive_Folder_Path			VARCHAR(200)
@Image_Hash_Value				NVARCHAR(128)

OUTPUT PARAMETERS:
NAME     DATATYPE	DEFAULT		DESCRIPTION
--------------------------------------------------------------------
USAGE EXAMPLES:
--------------------------------------------------------------------

BEGIN TRAN

	EXEC dbo.Inv_Sourced_Image_Image_Hash_Value_Upd
		  @Actual_File_Name = '2253107_3013845.tif'
		 ,@Image_Hash_Value = 'asdfasdf'

	SELECT
		  *
	FROM
		  dbo.Inv_Sourced_Image
	WHERE
		  Inv_Sourced_Image_Id = 2253107

ROLLBACK TRAN


1301 Collins_363074_01022014-01302014_invoice.pdf

BEGIN TRAN

	EXEC dbo.Inv_Sourced_Image_Image_Hash_Value_Upd
		  @Actual_File_Name = '2014.01_Electric Power.tif'
		 ,@Image_Hash_Value = 'asdfasdf'

	SELECT
		  *
	FROM
		  dbo.Inv_Sourced_Image
	WHERE
		  Inv_Sourced_Image_Id = 2253107

ROLLBACK TRAN

SELECT * FROM INV_SOURCE WHERE Archive_Folder_Path = '\Ubm\BerryPlastics\Archive'

SELECT TOP 1 * FROM Inv_Sourced_Image ORDER BY Inv_Sourced_Image_Id DESC


AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
HG			Harihara Suthan Ganesan

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
HG			2014-06-20	Created for Invoice scraping image hash enh
KVK			10/30/2018		modified since INV_SOURCED_IMAGE table ORIGINAL_FILENAME, CBMS_FILENAME column datatype is changing from VARCHHAR to NVARCHAR
*/

CREATE PROCEDURE [dbo].[Inv_Sourced_Image_Image_Hash_Value_Upd]
(
    @Actual_File_Name NVARCHAR(200),
    @Image_Hash_Value NVARCHAR(128)
)
AS
BEGIN

    SET NOCOUNT ON;

    DECLARE @invalid_string VARCHAR(100),
            @Inv_Source_Id INT,
            @Inv_Sourced_Image_Id INT;

    BEGIN TRY

        -- Extract the Inv_Sourced_Image_Id from the file name
        SELECT @Inv_Sourced_Image_Id
            = CASE
                  WHEN ISNUMERIC(   CASE
                                        WHEN CHARINDEX('_', @Actual_File_Name, 1) > 1 THEN
                                            SUBSTRING(@Actual_File_Name, 1, CHARINDEX('_', @Actual_File_Name, 1) - 1)
                                        ELSE
                                            '0'
                                    END
                                ) = 1 THEN
                      CAST(CASE
                               WHEN CHARINDEX('_', @Actual_File_Name, 1) > 1 THEN
                                   SUBSTRING(@Actual_File_Name, 1, CHARINDEX('_', @Actual_File_Name, 1) - 1)
                               ELSE
                                   '0'
                           END AS DECIMAL)
                  ELSE
                      0
              END;

        SET @Inv_Sourced_Image_Id = ISNULL(@Inv_Sourced_Image_Id, 0);

        UPDATE dbo.INV_SOURCED_IMAGE
        SET Image_Hash_Value = @Image_Hash_Value
        WHERE INV_SOURCED_IMAGE_ID = @Inv_Sourced_Image_Id;

    END TRY
    BEGIN CATCH
        PRINT 'Error with the file : ' + @Actual_File_Name;
    END CATCH;

END;
GO
GRANT EXECUTE ON  [dbo].[Inv_Sourced_Image_Image_Hash_Value_Upd] TO [CBMSApplication]
GO
