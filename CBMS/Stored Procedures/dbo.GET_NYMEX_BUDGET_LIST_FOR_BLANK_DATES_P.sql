SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE          PROCEDURE dbo.GET_NYMEX_BUDGET_LIST_FOR_BLANK_DATES_P

@userId varchar(10),
@sessionId varchar(20),
@clientId int,
@divisionId int,
@siteId int,
@startDate Datetime,
@endDate Datetime,
@approved int

AS
	set nocount on
IF @startDate IS NULL AND @endDate IS NULL
BEGIN

		IF @approved = 1
		begin
			IF @siteId = -1 and @divisionId > 0 

				SELECT 
					budget.BUDGET_NAME  , cli.CLIENT_NAME ,
					div.DIVISION_NAME , sit.SITE_NAME,
					budget.BUDGET_START_MONTH ,
					budget.BUDGET_END_MONTH, 
					budget.IS_BUDGET_APPROVED,
					budget.RM_BUDGET_ID, budget.is_client_generated
				FROM 
					client cli,	
					RM_BUDGET budget 
					left join division div on budget.division_id = div.division_id 
					left join vwSiteName sit on budget.site_id = sit.site_id 
				WHERE   
					cli.CLIENT_ID=budget.CLIENT_ID
					AND budget.CLIENT_ID=@clientId
					AND budget.DIVISION_ID=@divisionId 


					AND budget.IS_BUDGET_APPROVED=1




			--		and budget.SITE_ID=@siteId
			ELSE
			IF @divisionId = -1 and  @siteId > 0

				SELECT 
					budget.BUDGET_NAME  , cli.CLIENT_NAME ,
					div.DIVISION_NAME , sit.SITE_NAME,
					budget.BUDGET_START_MONTH ,
					budget.BUDGET_END_MONTH, 
					budget.IS_BUDGET_APPROVED,
					budget.RM_BUDGET_ID,budget.is_client_generated
				FROM 
					client cli,	
					RM_BUDGET budget 
					left join division div on budget.division_id = div.division_id 
					left join vwSiteName sit on budget.site_id = sit.site_id 
				WHERE   
					cli.CLIENT_ID=budget.CLIENT_ID
					AND budget.CLIENT_ID=@clientId
					--AND budget.DIVISION_ID=117 
					and budget.site_id = @siteId

					AND budget.IS_BUDGET_APPROVED=1	


			ELSE
			IF @divisionId=-1 AND @siteId=-1
				SELECT 
					budget.BUDGET_NAME  , cli.CLIENT_NAME ,
					div.DIVISION_NAME , sit.SITE_NAME,
					budget.BUDGET_START_MONTH ,
					budget.BUDGET_END_MONTH, 
					budget.IS_BUDGET_APPROVED,
					budget.RM_BUDGET_ID,budget.is_client_generated
				FROM 
					client cli,	
					RM_BUDGET budget 
					left join division div on budget.division_id = div.division_id 
					left join vwSiteName sit on budget.site_id = sit.site_id 
				WHERE   
					cli.CLIENT_ID=budget.CLIENT_ID
					AND budget.CLIENT_ID=@clientId

					AND budget.IS_BUDGET_APPROVED=1	


			ELSE

				SELECT 
					budget.BUDGET_NAME, cli.CLIENT_NAME ,
					div.DIVISION_NAME , sit.SITE_NAME,
					budget.BUDGET_START_MONTH ,
					budget.BUDGET_END_MONTH, 
					budget.IS_BUDGET_APPROVED,
					budget.RM_BUDGET_ID,budget.is_client_generated

				FROM 	RM_BUDGET budget, CLIENT cli,
					DIVISION div ,  vwSiteName sit
				WHERE   cli.CLIENT_ID=budget.CLIENT_ID
					AND budget.CLIENT_ID=@clientId AND 

					div.DIVISION_ID=budget.DIVISION_ID
					AND budget.DIVISION_ID=@divisionId

					AND sit.SITE_ID=budget.SITE_ID
					and budget.SITE_ID=@siteId

					AND budget.IS_BUDGET_APPROVED=1			
		--APPROVED YES END	
		end
		ELSE 	IF @approved = 2
		begin

			IF @siteId = -1 and @divisionId > 0 

				SELECT 
					budget.BUDGET_NAME  , cli.CLIENT_NAME ,
					div.DIVISION_NAME , sit.SITE_NAME,
					budget.BUDGET_START_MONTH ,
					budget.BUDGET_END_MONTH, 
					budget.IS_BUDGET_APPROVED,
					budget.RM_BUDGET_ID, budget.is_client_generated
				FROM 
					client cli,	

					RM_BUDGET budget 
					left join division div on budget.division_id = div.division_id 
					left join vwSiteName sit on budget.site_id = sit.site_id 
				WHERE   
					cli.CLIENT_ID=budget.CLIENT_ID
					AND budget.CLIENT_ID=@clientId
					AND budget.DIVISION_ID=@divisionId 


					 AND budget.IS_BUDGET_APPROVED=0




			--		and budget.SITE_ID=@siteId
			ELSE
			IF @divisionId = -1 and  @siteId > 0

				SELECT 
					budget.BUDGET_NAME  , cli.CLIENT_NAME ,
					div.DIVISION_NAME , sit.SITE_NAME,
					budget.BUDGET_START_MONTH ,
					budget.BUDGET_END_MONTH, 
					budget.IS_BUDGET_APPROVED,
					budget.RM_BUDGET_ID,budget.is_client_generated
				FROM 
					client cli,	
					RM_BUDGET budget 
					left join division div on budget.division_id = div.division_id 
					left join vwSiteName sit on budget.site_id = sit.site_id 
				WHERE   
					cli.CLIENT_ID=budget.CLIENT_ID
					AND budget.CLIENT_ID=@clientId
					--AND budget.DIVISION_ID=117 
					and budget.site_id = @siteId

					 AND budget.IS_BUDGET_APPROVED=0


			ELSE
			IF @divisionId=-1 AND @siteId=-1
				SELECT 
					budget.BUDGET_NAME  , cli.CLIENT_NAME ,
					div.DIVISION_NAME , sit.SITE_NAME,
					budget.BUDGET_START_MONTH ,
					budget.BUDGET_END_MONTH, 
					budget.IS_BUDGET_APPROVED,
					budget.RM_BUDGET_ID,budget.is_client_generated
				FROM 
					client cli,	
					RM_BUDGET budget 
					left join division div on budget.division_id = div.division_id 
					left join vwSiteName sit on budget.site_id = sit.site_id 
				WHERE   
					cli.CLIENT_ID=budget.CLIENT_ID
					AND budget.CLIENT_ID=@clientId

					AND budget.IS_BUDGET_APPROVED=0


			ELSE

				SELECT 
					budget.BUDGET_NAME, cli.CLIENT_NAME ,
					div.DIVISION_NAME , sit.SITE_NAME,
					budget.BUDGET_START_MONTH ,
					budget.BUDGET_END_MONTH, 
					budget.IS_BUDGET_APPROVED,
					budget.RM_BUDGET_ID,budget.is_client_generated

				FROM 	RM_BUDGET budget, CLIENT cli,
					DIVISION div ,  vwSiteName sit
				WHERE   cli.CLIENT_ID=budget.CLIENT_ID
					AND budget.CLIENT_ID=@clientId AND 

					div.DIVISION_ID=budget.DIVISION_ID
					AND budget.DIVISION_ID=@divisionId

					AND sit.SITE_ID=budget.SITE_ID
					and budget.SITE_ID=@siteId

					AND budget.IS_BUDGET_APPROVED=0
		end
		ELSE		
		begin
			IF @siteId = -1 and @divisionId > 0 

				SELECT 
					budget.BUDGET_NAME  , cli.CLIENT_NAME ,
					div.DIVISION_NAME , sit.SITE_NAME,
					budget.BUDGET_START_MONTH ,
					budget.BUDGET_END_MONTH, 
					budget.IS_BUDGET_APPROVED,
					budget.RM_BUDGET_ID, budget.is_client_generated
				FROM 
					client cli,	
					RM_BUDGET budget 
					left join division div on budget.division_id = div.division_id 
					left join vwSiteName sit on budget.site_id = sit.site_id 
				WHERE   
					cli.CLIENT_ID=budget.CLIENT_ID
					AND budget.CLIENT_ID=@clientId
					AND budget.DIVISION_ID=@divisionId 







			--		and budget.SITE_ID=@siteId
			ELSE
			IF @divisionId = -1 and  @siteId > 0



				SELECT 
					budget.BUDGET_NAME  , cli.CLIENT_NAME ,
					div.DIVISION_NAME , sit.SITE_NAME,
					budget.BUDGET_START_MONTH ,
					budget.BUDGET_END_MONTH, 
					budget.IS_BUDGET_APPROVED,
					budget.RM_BUDGET_ID,budget.is_client_generated
				FROM 
					client cli,	
					RM_BUDGET budget 
					left join division div on budget.division_id = div.division_id 
					left join vwSiteName sit on budget.site_id = sit.site_id 
				WHERE   
					cli.CLIENT_ID=budget.CLIENT_ID
					AND budget.CLIENT_ID=@clientId
					--AND budget.DIVISION_ID=117 
					and budget.site_id = @siteId




			ELSE
			IF @divisionId=-1 AND @siteId=-1
				SELECT 
					budget.BUDGET_NAME  , cli.CLIENT_NAME ,
					div.DIVISION_NAME , sit.SITE_NAME,
					budget.BUDGET_START_MONTH ,
					budget.BUDGET_END_MONTH, 
					budget.IS_BUDGET_APPROVED,
					budget.RM_BUDGET_ID,budget.is_client_generated
				FROM 
					client cli,	
					RM_BUDGET budget 
					left join division div on budget.division_id = div.division_id 
					left join vwSiteName sit on budget.site_id = sit.site_id 
				WHERE   
					cli.CLIENT_ID=budget.CLIENT_ID
					AND budget.CLIENT_ID=@clientId



			ELSE

				SELECT 
					budget.BUDGET_NAME, cli.CLIENT_NAME ,
					div.DIVISION_NAME , sit.SITE_NAME,
					budget.BUDGET_START_MONTH ,
					budget.BUDGET_END_MONTH, 
					budget.IS_BUDGET_APPROVED,
					budget.RM_BUDGET_ID,budget.is_client_generated

				FROM 	RM_BUDGET budget, CLIENT cli,
					DIVISION div ,  vwSiteName sit
				WHERE   cli.CLIENT_ID=budget.CLIENT_ID
					AND budget.CLIENT_ID=@clientId AND 

					div.DIVISION_ID=budget.DIVISION_ID
					AND budget.DIVISION_ID=@divisionId

					AND sit.SITE_ID=budget.SITE_ID
					and budget.SITE_ID=@siteId


		end
END 
ELSE IF @startDate IS NOT NULL AND @endDate IS NULL
BEGIN
	
	
			IF @approved = 1
			begin
				IF @siteId = -1 and @divisionId > 0 
	
					SELECT 
						budget.BUDGET_NAME  , cli.CLIENT_NAME ,
						div.DIVISION_NAME , sit.SITE_NAME,
						budget.BUDGET_START_MONTH ,
						budget.BUDGET_END_MONTH, 
						budget.IS_BUDGET_APPROVED,
						budget.RM_BUDGET_ID, budget.is_client_generated
					FROM 
						client cli,	
						RM_BUDGET budget 
						left join division div on budget.division_id = div.division_id 
						left join vwSiteName sit on budget.site_id = sit.site_id 
					WHERE   
						cli.CLIENT_ID=budget.CLIENT_ID
						AND budget.CLIENT_ID=@clientId
						AND budget.DIVISION_ID=@divisionId 
	
						AND budget.BUDGET_START_MONTH=@startDate
						
						AND budget.IS_BUDGET_APPROVED=1
						
						
	
	
	
	
				--		and budget.SITE_ID=@siteId
				ELSE
				IF @divisionId = -1 and  @siteId > 0
	
					SELECT 
						budget.BUDGET_NAME  , cli.CLIENT_NAME ,
						div.DIVISION_NAME , sit.SITE_NAME,
						budget.BUDGET_START_MONTH ,
						budget.BUDGET_END_MONTH, 
						budget.IS_BUDGET_APPROVED,
						budget.RM_BUDGET_ID,budget.is_client_generated
					FROM 
						client cli,	
						RM_BUDGET budget 
						left join division div on budget.division_id = div.division_id 
						left join vwSiteName sit on budget.site_id = sit.site_id 
					WHERE   
						cli.CLIENT_ID=budget.CLIENT_ID
						AND budget.CLIENT_ID=@clientId
						--AND budget.DIVISION_ID=117 
						and budget.site_id = @siteId
						
						AND budget.BUDGET_START_MONTH=@startDate
	
						AND budget.IS_BUDGET_APPROVED=1	
	
	
				ELSE
				IF @divisionId=-1 AND @siteId=-1
					SELECT 
						budget.BUDGET_NAME  , cli.CLIENT_NAME ,
						div.DIVISION_NAME , sit.SITE_NAME,
						budget.BUDGET_START_MONTH ,
						budget.BUDGET_END_MONTH, 
						budget.IS_BUDGET_APPROVED,
						budget.RM_BUDGET_ID,budget.is_client_generated
					FROM 
						client cli,	
						RM_BUDGET budget 
						left join division div on budget.division_id = div.division_id 
						left join vwSiteName sit on budget.site_id = sit.site_id 
					WHERE   
						cli.CLIENT_ID=budget.CLIENT_ID
						AND budget.CLIENT_ID=@clientId
						
						AND budget.BUDGET_START_MONTH=@startDate
	
						AND budget.IS_BUDGET_APPROVED=1	
	
	
				ELSE
	
					SELECT 
						budget.BUDGET_NAME, cli.CLIENT_NAME ,
						div.DIVISION_NAME , sit.SITE_NAME,
						budget.BUDGET_START_MONTH ,
						budget.BUDGET_END_MONTH, 
						budget.IS_BUDGET_APPROVED,
						budget.RM_BUDGET_ID,budget.is_client_generated
	
					FROM 	RM_BUDGET budget, CLIENT cli,
						DIVISION div ,  vwSiteName sit
					WHERE   cli.CLIENT_ID=budget.CLIENT_ID
						AND budget.CLIENT_ID=@clientId AND 
	
						div.DIVISION_ID=budget.DIVISION_ID
						AND budget.DIVISION_ID=@divisionId
	
						AND sit.SITE_ID=budget.SITE_ID
						and budget.SITE_ID=@siteId
						
						AND budget.BUDGET_START_MONTH=@startDate
	
						AND budget.IS_BUDGET_APPROVED=1			
						
			--APPROVED YES END	
			end
			ELSE 	IF @approved = 2
			begin
	
				IF @siteId = -1 and @divisionId > 0 
	
					SELECT 
						budget.BUDGET_NAME  , cli.CLIENT_NAME ,
						div.DIVISION_NAME , sit.SITE_NAME,
						budget.BUDGET_START_MONTH ,
						budget.BUDGET_END_MONTH, 
						budget.IS_BUDGET_APPROVED,
						budget.RM_BUDGET_ID, budget.is_client_generated
					FROM 
						client cli,	
	
						RM_BUDGET budget 
						left join division div on budget.division_id = div.division_id 
						left join vwSiteName sit on budget.site_id = sit.site_id 
					WHERE   
						cli.CLIENT_ID=budget.CLIENT_ID
						AND budget.CLIENT_ID=@clientId
						AND budget.DIVISION_ID=@divisionId 
	
						AND budget.BUDGET_START_MONTH=@startDate
						
						AND budget.IS_BUDGET_APPROVED=0
	
	
	
	
				--		and budget.SITE_ID=@siteId
				ELSE
				IF @divisionId = -1 and  @siteId > 0
	
					SELECT 
						budget.BUDGET_NAME  , cli.CLIENT_NAME ,
						div.DIVISION_NAME , sit.SITE_NAME,
						budget.BUDGET_START_MONTH ,
						budget.BUDGET_END_MONTH, 
						budget.IS_BUDGET_APPROVED,
						budget.RM_BUDGET_ID,budget.is_client_generated
					FROM 
						client cli,	
						RM_BUDGET budget 
						left join division div on budget.division_id = div.division_id 
						left join vwSiteName sit on budget.site_id = sit.site_id 
					WHERE   
						cli.CLIENT_ID=budget.CLIENT_ID
						AND budget.CLIENT_ID=@clientId
						--AND budget.DIVISION_ID=117 
						and budget.site_id = @siteId
						
						AND budget.BUDGET_START_MONTH=@startDate
	
						 AND budget.IS_BUDGET_APPROVED=0
	
	
				ELSE
				IF @divisionId=-1 AND @siteId=-1
					SELECT 
						budget.BUDGET_NAME  , cli.CLIENT_NAME ,
						div.DIVISION_NAME , sit.SITE_NAME,
						budget.BUDGET_START_MONTH ,
						budget.BUDGET_END_MONTH, 
						budget.IS_BUDGET_APPROVED,
						budget.RM_BUDGET_ID,budget.is_client_generated
					FROM 
						client cli,	
						RM_BUDGET budget 
						left join division div on budget.division_id = div.division_id 
						left join vwSiteName sit on budget.site_id = sit.site_id 
					WHERE   
						cli.CLIENT_ID=budget.CLIENT_ID
						AND budget.CLIENT_ID=@clientId
	
						AND budget.BUDGET_START_MONTH=@startDate
						
						AND budget.IS_BUDGET_APPROVED=0
	
	
				ELSE
	
					SELECT 
						budget.BUDGET_NAME, cli.CLIENT_NAME ,
						div.DIVISION_NAME , sit.SITE_NAME,
						budget.BUDGET_START_MONTH ,
						budget.BUDGET_END_MONTH, 
						budget.IS_BUDGET_APPROVED,
						budget.RM_BUDGET_ID,budget.is_client_generated
	
					FROM 	RM_BUDGET budget, CLIENT cli,
						DIVISION div ,  vwSiteName sit
					WHERE   cli.CLIENT_ID=budget.CLIENT_ID
						AND budget.CLIENT_ID=@clientId AND 
	
						div.DIVISION_ID=budget.DIVISION_ID
						AND budget.DIVISION_ID=@divisionId
	
						AND sit.SITE_ID=budget.SITE_ID
						and budget.SITE_ID=@siteId
	
						AND budget.BUDGET_START_MONTH=@startDate
						
						AND budget.IS_BUDGET_APPROVED=0
			end
			ELSE		
			begin
				IF @siteId = -1 and @divisionId > 0 
	
					SELECT 
						budget.BUDGET_NAME  , cli.CLIENT_NAME ,
						div.DIVISION_NAME , sit.SITE_NAME,
						budget.BUDGET_START_MONTH ,
						budget.BUDGET_END_MONTH, 
						budget.IS_BUDGET_APPROVED,
						budget.RM_BUDGET_ID, budget.is_client_generated
					FROM 
						client cli,	
						RM_BUDGET budget 
						left join division div on budget.division_id = div.division_id 
						left join vwSiteName sit on budget.site_id = sit.site_id 
					WHERE   
						cli.CLIENT_ID=budget.CLIENT_ID
						AND budget.CLIENT_ID=@clientId
						AND budget.DIVISION_ID=@divisionId 
						AND budget.BUDGET_START_MONTH=@startDate
	
	
	
	
	
	
	
				--		and budget.SITE_ID=@siteId
				ELSE
				IF @divisionId = -1 and  @siteId > 0
	
	
	
					SELECT 
						budget.BUDGET_NAME  , cli.CLIENT_NAME ,
						div.DIVISION_NAME , sit.SITE_NAME,
						budget.BUDGET_START_MONTH ,
						budget.BUDGET_END_MONTH, 
						budget.IS_BUDGET_APPROVED,
						budget.RM_BUDGET_ID,budget.is_client_generated
					FROM 
						client cli,	
						RM_BUDGET budget 
						left join division div on budget.division_id = div.division_id 
						left join vwSiteName sit on budget.site_id = sit.site_id 
					WHERE   
						cli.CLIENT_ID=budget.CLIENT_ID
						AND budget.CLIENT_ID=@clientId
						--AND budget.DIVISION_ID=117 
						and budget.site_id = @siteId
						AND budget.BUDGET_START_MONTH=@startDate
	
	
	
	
				ELSE
				IF @divisionId=-1 AND @siteId=-1
					SELECT 
						budget.BUDGET_NAME  , cli.CLIENT_NAME ,
						div.DIVISION_NAME , sit.SITE_NAME,
						budget.BUDGET_START_MONTH ,
						budget.BUDGET_END_MONTH, 
						budget.IS_BUDGET_APPROVED,
						budget.RM_BUDGET_ID,budget.is_client_generated
					FROM 
						client cli,	
						RM_BUDGET budget 
						left join division div on budget.division_id = div.division_id 
						left join vwSiteName sit on budget.site_id = sit.site_id 
					WHERE   
						cli.CLIENT_ID=budget.CLIENT_ID
						AND budget.CLIENT_ID=@clientId
						AND budget.BUDGET_START_MONTH=@startDate
	
	
	
				ELSE
	
					SELECT 
						budget.BUDGET_NAME, cli.CLIENT_NAME ,
						div.DIVISION_NAME , sit.SITE_NAME,
						budget.BUDGET_START_MONTH ,
						budget.BUDGET_END_MONTH, 
						budget.IS_BUDGET_APPROVED,
						budget.RM_BUDGET_ID,budget.is_client_generated
	
					FROM 	RM_BUDGET budget, CLIENT cli,
						DIVISION div ,  vwSiteName sit
					WHERE   cli.CLIENT_ID=budget.CLIENT_ID
						AND budget.CLIENT_ID=@clientId AND 
	
						div.DIVISION_ID=budget.DIVISION_ID
						AND budget.DIVISION_ID=@divisionId
	
						AND sit.SITE_ID=budget.SITE_ID
					and budget.SITE_ID=@siteId
					AND budget.BUDGET_START_MONTH=@startDate
			end
					
					
end
ELSE IF @startDate IS  NULL AND @endDate IS NOT NULL
BEGIN


			IF @approved = 1
			begin
				IF @siteId = -1 and @divisionId > 0 
	
					SELECT 
						budget.BUDGET_NAME  , cli.CLIENT_NAME ,
						div.DIVISION_NAME , sit.SITE_NAME,
						budget.BUDGET_START_MONTH ,
						budget.BUDGET_END_MONTH, 
						budget.IS_BUDGET_APPROVED,
						budget.RM_BUDGET_ID, budget.is_client_generated
					FROM 
						client cli,	
						RM_BUDGET budget 
						left join division div on budget.division_id = div.division_id 
						left join vwSiteName sit on budget.site_id = sit.site_id 
					WHERE   
						cli.CLIENT_ID=budget.CLIENT_ID
						AND budget.CLIENT_ID=@clientId
						AND budget.DIVISION_ID=@divisionId 
	
	
						AND budget.IS_BUDGET_APPROVED=1
						
						AND budget.BUDGET_END_MONTH=@endDate
	
	
	
	
				--		and budget.SITE_ID=@siteId
				ELSE
				IF @divisionId = -1 and  @siteId > 0
	
					SELECT 
						budget.BUDGET_NAME  , cli.CLIENT_NAME ,
						div.DIVISION_NAME , sit.SITE_NAME,
						budget.BUDGET_START_MONTH ,
						budget.BUDGET_END_MONTH, 
						budget.IS_BUDGET_APPROVED,
						budget.RM_BUDGET_ID,budget.is_client_generated
					FROM 
						client cli,	
						RM_BUDGET budget 
						left join division div on budget.division_id = div.division_id 
						left join vwSiteName sit on budget.site_id = sit.site_id 
					WHERE   
						cli.CLIENT_ID=budget.CLIENT_ID
						AND budget.CLIENT_ID=@clientId
						--AND budget.DIVISION_ID=117 
						and budget.site_id = @siteId
	
						AND budget.IS_BUDGET_APPROVED=1	
						AND budget.BUDGET_END_MONTH=@endDate
	
	
				ELSE
				IF @divisionId=-1 AND @siteId=-1
					SELECT 
						budget.BUDGET_NAME  , cli.CLIENT_NAME ,
						div.DIVISION_NAME , sit.SITE_NAME,
						budget.BUDGET_START_MONTH ,
						budget.BUDGET_END_MONTH, 
						budget.IS_BUDGET_APPROVED,
						budget.RM_BUDGET_ID,budget.is_client_generated
					FROM 
						client cli,	
						RM_BUDGET budget 
						left join division div on budget.division_id = div.division_id 
						left join vwSiteName sit on budget.site_id = sit.site_id 
					WHERE   
						cli.CLIENT_ID=budget.CLIENT_ID
						AND budget.CLIENT_ID=@clientId
	
						AND budget.IS_BUDGET_APPROVED=1	
						AND budget.BUDGET_END_MONTH=@endDate
	
	
				ELSE
	
					SELECT 
						budget.BUDGET_NAME, cli.CLIENT_NAME ,
						div.DIVISION_NAME , sit.SITE_NAME,
						budget.BUDGET_START_MONTH ,
						budget.BUDGET_END_MONTH, 
						budget.IS_BUDGET_APPROVED,
						budget.RM_BUDGET_ID,budget.is_client_generated
	
					FROM 	RM_BUDGET budget, CLIENT cli,
						DIVISION div ,  vwSiteName sit
					WHERE   cli.CLIENT_ID=budget.CLIENT_ID
						AND budget.CLIENT_ID=@clientId AND 
	
						div.DIVISION_ID=budget.DIVISION_ID
						AND budget.DIVISION_ID=@divisionId
	
						AND sit.SITE_ID=budget.SITE_ID
						and budget.SITE_ID=@siteId
	
						AND budget.IS_BUDGET_APPROVED=1			
						AND budget.BUDGET_END_MONTH=@endDate
			--APPROVED YES END	
			end
			ELSE 	IF @approved = 2
			begin
	
				IF @siteId = -1 and @divisionId > 0 
	
					SELECT 
						budget.BUDGET_NAME  , cli.CLIENT_NAME ,
						div.DIVISION_NAME , sit.SITE_NAME,
						budget.BUDGET_START_MONTH ,
						budget.BUDGET_END_MONTH, 
						budget.IS_BUDGET_APPROVED,
						budget.RM_BUDGET_ID, budget.is_client_generated
					FROM 
						client cli,	
	
						RM_BUDGET budget 
						left join division div on budget.division_id = div.division_id 
						left join vwSiteName sit on budget.site_id = sit.site_id 
					WHERE   
						cli.CLIENT_ID=budget.CLIENT_ID
						AND budget.CLIENT_ID=@clientId
						AND budget.DIVISION_ID=@divisionId 
	
	
						 AND budget.IS_BUDGET_APPROVED=0
						 AND budget.BUDGET_END_MONTH=@endDate
	
	
	
	
				--		and budget.SITE_ID=@siteId
				ELSE
				IF @divisionId = -1 and  @siteId > 0
	
					SELECT 
						budget.BUDGET_NAME  , cli.CLIENT_NAME ,
						div.DIVISION_NAME , sit.SITE_NAME,
						budget.BUDGET_START_MONTH ,
						budget.BUDGET_END_MONTH, 
						budget.IS_BUDGET_APPROVED,
						budget.RM_BUDGET_ID,budget.is_client_generated
					FROM 
						client cli,	
						RM_BUDGET budget 
						left join division div on budget.division_id = div.division_id 
						left join vwSiteName sit on budget.site_id = sit.site_id 
					WHERE   
						cli.CLIENT_ID=budget.CLIENT_ID
						AND budget.CLIENT_ID=@clientId
						--AND budget.DIVISION_ID=117 
						and budget.site_id = @siteId
	
						 AND budget.IS_BUDGET_APPROVED=0
						 AND budget.BUDGET_END_MONTH=@endDate
	
	
				ELSE
				IF @divisionId=-1 AND @siteId=-1
					SELECT 
						budget.BUDGET_NAME  , cli.CLIENT_NAME ,
						div.DIVISION_NAME , sit.SITE_NAME,
						budget.BUDGET_START_MONTH ,
						budget.BUDGET_END_MONTH, 
						budget.IS_BUDGET_APPROVED,
						budget.RM_BUDGET_ID,budget.is_client_generated
					FROM 
						client cli,	
						RM_BUDGET budget 
						left join division div on budget.division_id = div.division_id 
						left join vwSiteName sit on budget.site_id = sit.site_id 
					WHERE   
						cli.CLIENT_ID=budget.CLIENT_ID
						AND budget.CLIENT_ID=@clientId
	
						AND budget.IS_BUDGET_APPROVED=0
						AND budget.BUDGET_END_MONTH=@endDate
	
	
				ELSE
	
					SELECT 
						budget.BUDGET_NAME, cli.CLIENT_NAME ,
						div.DIVISION_NAME , sit.SITE_NAME,
						budget.BUDGET_START_MONTH ,
						budget.BUDGET_END_MONTH, 
						budget.IS_BUDGET_APPROVED,
						budget.RM_BUDGET_ID,budget.is_client_generated
	
					FROM 	RM_BUDGET budget, CLIENT cli,
						DIVISION div ,  vwSiteName sit
					WHERE   cli.CLIENT_ID=budget.CLIENT_ID
						AND budget.CLIENT_ID=@clientId AND 
	
						div.DIVISION_ID=budget.DIVISION_ID
						AND budget.DIVISION_ID=@divisionId
	
						AND sit.SITE_ID=budget.SITE_ID
						and budget.SITE_ID=@siteId
	
						AND budget.IS_BUDGET_APPROVED=0
						AND budget.BUDGET_END_MONTH=@endDate
			end
			ELSE		
			begin
				IF @siteId = -1 and @divisionId > 0 
	
					SELECT 
						budget.BUDGET_NAME  , cli.CLIENT_NAME ,
						div.DIVISION_NAME , sit.SITE_NAME,
						budget.BUDGET_START_MONTH ,
						budget.BUDGET_END_MONTH, 
						budget.IS_BUDGET_APPROVED,
						budget.RM_BUDGET_ID, budget.is_client_generated
					FROM 
						client cli,	
						RM_BUDGET budget 
						left join division div on budget.division_id = div.division_id 
						left join vwSiteName sit on budget.site_id = sit.site_id 
					WHERE   
						cli.CLIENT_ID=budget.CLIENT_ID
						AND budget.CLIENT_ID=@clientId
						AND budget.DIVISION_ID=@divisionId 
						AND budget.BUDGET_END_MONTH=@endDate
	
	
	
	
	
	
	
				--		and budget.SITE_ID=@siteId
				ELSE
				IF @divisionId = -1 and  @siteId > 0
	
	
	
					SELECT 
						budget.BUDGET_NAME  , cli.CLIENT_NAME ,

						div.DIVISION_NAME , sit.SITE_NAME,
						budget.BUDGET_START_MONTH ,
						budget.BUDGET_END_MONTH, 
						budget.IS_BUDGET_APPROVED,
						budget.RM_BUDGET_ID,budget.is_client_generated
					FROM 
						client cli,	
						RM_BUDGET budget 
						left join division div on budget.division_id = div.division_id 
						left join vwSiteName sit on budget.site_id = sit.site_id 
					WHERE   
						cli.CLIENT_ID=budget.CLIENT_ID
						AND budget.CLIENT_ID=@clientId
						--AND budget.DIVISION_ID=117 
						and budget.site_id = @siteId
						AND budget.BUDGET_END_MONTH=@endDate
	
	
	
	
				ELSE
				IF @divisionId=-1 AND @siteId=-1
					SELECT 
						budget.BUDGET_NAME  , cli.CLIENT_NAME ,
						div.DIVISION_NAME , sit.SITE_NAME,
						budget.BUDGET_START_MONTH ,
						budget.BUDGET_END_MONTH, 
						budget.IS_BUDGET_APPROVED,
						budget.RM_BUDGET_ID,budget.is_client_generated
					FROM 
						client cli,	
						RM_BUDGET budget 
						left join division div on budget.division_id = div.division_id 
						left join vwSiteName sit on budget.site_id = sit.site_id 
					WHERE   
						cli.CLIENT_ID=budget.CLIENT_ID
						AND budget.CLIENT_ID=@clientId
						AND budget.BUDGET_END_MONTH=@endDate
	
	
	
				ELSE
	
					SELECT 
						budget.BUDGET_NAME, cli.CLIENT_NAME ,
						div.DIVISION_NAME , sit.SITE_NAME,
						budget.BUDGET_START_MONTH ,
						budget.BUDGET_END_MONTH, 
						budget.IS_BUDGET_APPROVED,
						budget.RM_BUDGET_ID,budget.is_client_generated
	
					FROM 	RM_BUDGET budget, CLIENT cli,
						DIVISION div ,  vwSiteName sit
					WHERE   cli.CLIENT_ID=budget.CLIENT_ID
						AND budget.CLIENT_ID=@clientId AND 
	
						div.DIVISION_ID=budget.DIVISION_ID
						AND budget.DIVISION_ID=@divisionId
	
						AND sit.SITE_ID=budget.SITE_ID
					and budget.SITE_ID=@siteId
					AND budget.BUDGET_END_MONTH=@endDate
			end

end
GO
GRANT EXECUTE ON  [dbo].[GET_NYMEX_BUDGET_LIST_FOR_BLANK_DATES_P] TO [CBMSApplication]
GO
