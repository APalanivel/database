SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Budget_Id_Sel_By_Account_Id]  
     
DESCRIPTION: 
	To Get Budget Id's for Selected Account Id.
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Account_Id		INT						
@StartIndex		INT			1			
@EndIndex		INT			2147483647
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------


	EXEC Budget_Id_Sel_By_Account_Id  101527


AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			25-MAY-10	CREATED

*/

CREATE PROCEDURE dbo.Budget_Id_Sel_By_Account_Id
(
	@Account_Id		INT
	,@StartIndex	INT		= 1
	,@EndIndex		INT		= 2147483647
)
AS
BEGIN

	SET NOCOUNT ON;
	
	WITH Cte_Budget_List AS
	(
		SELECT
			 Budget_Id
			 ,ROW_NUMBER() OVER(ORDER BY Budget_Account_Id) Row_Num
			 ,COUNT(1) OVER() Total_Rows
		FROM
			dbo.BUDGET_ACCOUNT
		WHERE
			Account_Id = @Account_Id
			AND Is_Deleted = 0
	)
	SELECT
		Budget_id
		,Total_Rows
	FROM
		Cte_Budget_List
	WHERE
		Row_Num BETWEEN @StartIndex AND @EndIndex

END
GO
GRANT EXECUTE ON  [dbo].[Budget_Id_Sel_By_Account_Id] TO [CBMSApplication]
GO
