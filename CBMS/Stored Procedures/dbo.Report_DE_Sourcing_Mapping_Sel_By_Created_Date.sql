SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
      
/******      
NAME:      
 dbo.Report_DE_Sourcing_Mapping_Sel_By_Created_Date      
 DESCRIPTION:       
    
 INPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
 @Client_Id    INT        
 @Created_Date DATE    
     
     
 OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------     
     
------------------------------------------------------------      
 USAGE EXAMPLES:      
------------------------------------------------------------      
EXEC dbo.Report_DE_Sourcing_Mapping_Sel_By_Created_Date '2013-01-01'    
    
 AUTHOR INITIALS:      
 Initials Name      
------------------------------------------------------------      
 AKR   Ashok Kumar Raju    
     
 MODIFICATIONS       
 Initials Date  Modification      
------------------------------------------------------------      
 AKR   2013-12-30 Created    
******/      
    
CREATE PROCEDURE dbo.Report_DE_Sourcing_Mapping_Sel_By_Created_Date ( @Created_Date DATE )
AS 
BEGIN    
    
    
      DECLARE
            @Custom_Analyst INT
           ,@Site_Entity_Id INT
           ,@Account_Entity_Id INT  
        
        
        
      SELECT
            @Site_Entity_Id = MAX(CASE WHEN en.Entity_Name = 'SITE_TABLE' THEN en.Entity_Id
                                  END)
           ,@Account_Entity_Id = MAX(CASE WHEN en.Entity_Name = 'ACCOUNT_TABLE' THEN en.Entity_Id
                                     END)
      FROM
            dbo.Entity en
      WHERE
            en.entity_type = 500  
  
          
      SELECT
            @Custom_Analyst = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Analyst Type'
            AND c.Code_Value = 'Default';    
      WITH  cte_Accounts
              AS ( SELECT
                        ch.client_Name
                       ,ch.SITE_Id
                       ,ch.City
                       ,ch.STATE_Name
                       ,cha.Display_Account_Number
                       ,cha.Account_Id
                       ,com.Commodity_Name
                       ,com.Commodity_Id
                       ,ch.Client_Id
                       ,cha.Account_Vendor_Id
                       ,ena.Modified_Date Account_Created_Date
                       ,ui.First_Name + ' ' + ui.Last_Name Account_Created_by
                       ,COALESCE(cha.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) Account_Analyst_Mapping_Cd
                       ,COALESCE(ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) Site_Analyst_Mapping_Cd
                       ,ch.Client_Analyst_Mapping_Cd
                   FROM
                        core.client_hier ch
                        INNER JOIN core.client_hier_Account cha
                              ON ch.client_hier_Id = cha.Client_Hier_Id
                        INNER JOIN dbo.commodity com
                              ON com.commodity_id = cha.Commodity_Id
                        LEFT OUTER JOIN ( core.Client_Commodity ccc
                                          INNER JOIN dbo.Code cd
                                                ON cd.Code_Id = ccc.Commodity_Service_Cd
                                                   AND cd.Code_Value = 'Invoice' )
                                          ON ccc.Client_Id = ch.Client_Id
                                             AND ccc.Commodity_Id = cha.COMMODITY_ID
                        INNER JOIN dbo.entity_audit ena
                              ON ena.ENTITY_IDENTIFIER = cha.Account_Id
                                 AND ena.Audit_Type = 1
                                 AND ena.Entity_Id = @Account_Entity_Id
                        INNER JOIN dbo.user_info ui
                              ON ui.user_info_id = ena.user_info_Id
                   WHERE
                        ena.Modified_Date >= @Created_Date
                   GROUP BY
                        ch.client_Name
                       ,ch.SITE_Id
                       ,ch.City
                       ,ch.STATE_Name
                       ,cha.Display_Account_Number
                       ,cha.Account_Id
                       ,com.Commodity_Name
                       ,com.Commodity_Id
                       ,ch.Client_Id
                       ,cha.Account_Vendor_Id
                       ,ena.Modified_Date
                       ,ui.First_Name + ' ' + ui.Last_Name
                       ,COALESCE(cha.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd)
                       ,COALESCE(ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd)
                       ,ch.Client_Analyst_Mapping_Cd)
            SELECT
                  ca.Client_Name
                 ,ca.SITE_Id
                 ,ca.City
                 ,ca.STATE_Name
                 ,ca.Display_Account_Number
                 ,ca.Commodity_Name
                 ,ca.Commodity_Id
                 ,ca.Client_Id
                 ,ca.Account_Id
                 ,ca.Account_Created_Date
                 ,ca.Account_Vendor_Id
                 ,ens.Modified_Date Site_Created_Date
                 ,ca.Account_Created_By
                 ,caa.Code_Value Account_Sourcing_Mapping
                 ,cas.Code_Value Site_Sourcing_Mapping
                 ,cac.Code_Value Client_Sourcing_Mapping
            FROM
                  cte_Accounts ca
                  INNER JOIN dbo.entity_audit ens
                        ON ens.ENTITY_IDENTIFIER = ca.Site_Id
                           AND ens.Audit_Type = 1
                           AND ens.Entity_Id = @Site_Entity_Id
                  INNER JOIN dbo.Code caa
                        ON caa.Code_Id = ca.Account_Analyst_Mapping_Cd
                  INNER JOIN dbo.Code cas
                        ON cas.Code_Id = ca.Site_Analyst_Mapping_Cd
                  INNER JOIN dbo.Code cac
                        ON cac.Code_Id = ca.Client_Analyst_Mapping_Cd
            WHERE
                  Account_Analyst_Mapping_Cd != Site_Analyst_Mapping_Cd
                  OR Site_Analyst_Mapping_Cd != Client_Analyst_Mapping_Cd  
               
END    
    
    
;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Sourcing_Mapping_Sel_By_Created_Date] TO [CBMS_SSRS_Reports]
GO
