SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Recalc_Lock_Sel_By_Cu_Invoice_Account_Commodity           
              
Description:              
        To insert Data to Cu_Invoice_Account_Commodity_Recalc_Lock table.              
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
   @Cu_Invoice_Id						 INT
   @Account_Id							 INT
   @Commodity_Id						 INT
   
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
    
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                

EXEC Recalc_Lock_Sel_By_Cu_Invoice_Account_Commodity 23647880,660968,291
EXEC Recalc_Lock_Sel_By_Cu_Invoice_Account_Commodity 22643270,53795,291 

             
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV			   Ravi Kumar Vegesna
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2016-09-01		Maint-4109: Created For AS400-Enhancement.         
             
******/ 
CREATE PROCEDURE [dbo].[Recalc_Lock_Sel_By_Cu_Invoice_Account_Commodity]
      ( 
       @Cu_Invoice_Id INT
      ,@Account_Id INT
      ,@Commodity_Id INT )
AS 
BEGIN
      SET NOCOUNT ON  
      
      SELECT
            ciacrl.Is_Locked
           ,ciacrl.Updated_User_Id
           ,ui.FIRST_NAME + ' ' + ui.LAST_NAME UserName
           ,ciacrl.Last_Change_Ts
           ,cha.Account_Not_Managed
      FROM
            core.Client_Hier_Account cha
            LEFT OUTER JOIN dbo.Cu_Invoice_Account_Commodity ciac
                  ON ciac.Account_Id = cha.Account_Id
                     AND ciac.Commodity_Id = cha.Commodity_Id
                     AND ciac.Cu_Invoice_Id = @Cu_Invoice_Id
            LEFT OUTER JOIN dbo.Cu_Invoice_Account_Commodity_Recalc_Lock ciacrl
                  ON ciac.Cu_Invoice_Account_Commodity_Id = ciacrl.Cu_Invoice_Account_Commodity_Id
            LEFT OUTER JOIN dbo.USER_INFO ui
                  ON ciacrl.Updated_User_Id = ui.USER_INFO_ID
      WHERE
            cha.Account_Id = @Account_Id
            AND cha.Commodity_Id = @Commodity_Id
      GROUP BY
            ciacrl.Is_Locked
           ,ciacrl.Updated_User_Id
           ,ui.FIRST_NAME
           ,ui.LAST_NAME
           ,ciacrl.Last_Change_Ts
           ,cha.Account_Not_Managed
            
           
            
END;

;
GO
GRANT EXECUTE ON  [dbo].[Recalc_Lock_Sel_By_Cu_Invoice_Account_Commodity] TO [CBMSApplication]
GO
