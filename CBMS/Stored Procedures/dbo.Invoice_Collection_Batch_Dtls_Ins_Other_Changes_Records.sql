SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:   [dbo].[Invoice_Collection_Batch_Dtls_Ins_Other_Changes_Records]                 
                        
Description:                        
   This sproc is used to fill the ICQ Batch Table.          
           
  [__$operation] Identifies the Data Changes and it can be one of the following.        
    1 = delete        
       2 = insert        
    3 = update (captured column values are those before the update operation)        
    4 = update (captured column values are those after the update operation)              
        
 Input Parameters:                        
    Name     DataType   Default     Description                          
--------------------------------------------------------------------------------------         
                         
 Output Parameters:                              
    Name     DataType   Default     Description                          
--------------------------------------------------------------------------------------                          
        
 Usage Examples:                            
--------------------------------------------------------------------------------------             
  BEGIN TRAN        
 exec dbo.Invoice_Collection_Batch_Dtls_Ins_Other_Changes_Records        
  ROLLBACK        
          
Author Initials:                        
    Initials Name                        
--------------------------------------------------------------------------------------                          
 RKV   Ravi Kumar Vegesna        
 HG    Harihara suthan Ganesan        
        
 Modifications:                        
    Initials        Date  Modification                        
--------------------------------------------------------------------------------------                          
    RKV    2017-02-03  Created For Invoice_Collection Tool Revamp.        
     
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Batch_Dtls_Ins_Other_Changes_Records]
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Record_Cnt INT = 1
            , @Other_Code_Cnt INT = 1
            , @Status_Cd INT
            , @Upload_Batch_Id INT
            , @Start_Dt DATETIME
            , @ICR_Status_Cd INT
            , @Batch_Size INT
            , @Max_CT_Ver BIGINT
            , @Min_CT_CU_Invoice_Ver BIGINT;

        DECLARE
            @begin_time DATETIME
            , @end_time DATETIME
            , @begin_lsn BINARY(10)
            , @end_lsn BINARY(10)
            , @Invoice_Collection_Batch_Type_Cd INT
            , @Invoice_Collection_Batch_Type_New_Config_Cd INT
            , @bimonthly INT
            , @quarterly INT
            , @biannual INT
            , @annual INT
            , @monthly INT
            , @Custom INT
            , @Custom_frq_1_9_Cd INT
            , @Custom_frq_Custom_Days_Cd INT;


        DECLARE @Custom_Frequency_Cd INT;



        SELECT
            @Custom_Frequency_Cd = c.Code_Id
        FROM
            Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            Code_Value = 'Custom'
            AND cs.Codeset_Name = 'InvoiceFrequency';

        SELECT
            @Start_Dt = CAST(App_Config_Value AS DATETIME)
        FROM
            dbo.App_Config ac
        WHERE
            App_Config_Cd = 'IC_Batch_Other_Process_Last_Run_Dt';

        SELECT
            @Batch_Size = CAST(App_Config_Value AS INT)
        FROM
            dbo.App_Config ac
        WHERE
            App_Config_Cd = 'IC_Batch_Size';



        SELECT
            @Min_CT_CU_Invoice_Ver = CAST(App_Config_Value AS BIGINT)
        FROM
            dbo.App_Config ac
        WHERE
            App_Config_Cd = 'IC_Batch_Process_Last_Run_CU_Invoice_Data';

        SELECT
            @ICR_Status_Cd = Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Std_Column_Name = 'Invoice_Collection_Status_Cd'
            AND c.Code_Value = 'Received';

        SELECT  @Max_CT_Ver = CHANGE_TRACKING_CURRENT_VERSION();

        SELECT
            @begin_time = CASE WHEN @Start_Dt = '1900-01-01' THEN GETDATE()
                              ELSE @Start_Dt
                          END
            , @end_time = GETDATE();

        SELECT
            @begin_lsn = sys.fn_cdc_map_time_to_lsn('smallest greater than', @begin_time);
        SELECT
            @end_lsn = sys.fn_cdc_map_time_to_lsn('largest less than or equal', @end_time);


        SELECT
            @Custom_frq_1_9_Cd = MAX(CASE WHEN c.Code_Value = '1-9 Days' THEN c.Code_Id
                                     END)
            , @Custom_frq_Custom_Days_Cd = MAX(CASE WHEN c.Code_Value = 'Custom Days' THEN c.Code_Id
                                               END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'FrequencyPattern'
            AND c.Code_Value IN ( '1-9 Days', 'Custom Days' );

        SELECT
            @bimonthly = MAX(CASE WHEN c.Code_Value = 'bi-monthly' THEN c.Code_Id
                             END)
            , @quarterly = MAX(CASE WHEN c.Code_Value = 'quarterly' THEN c.Code_Id
                               END)
            , @biannual = MAX(CASE WHEN c.Code_Value = 'bi-annual' THEN c.Code_Id
                              END)
            , @annual = MAX(CASE WHEN c.Code_Value = 'annual' THEN c.Code_Id
                            END)
            , @monthly = MAX(CASE WHEN c.Code_Value = 'monthly' THEN c.Code_Id
                             END)
            , @Custom = MAX(CASE WHEN c.Code_Value = 'Custom' THEN c.Code_Id
                            END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'InvoiceFrequency'
            AND c.Code_Value IN ( 'bi-monthly', 'quarterly', 'bi-annual', 'annual', 'monthly', 'Custom' );



        SELECT
            @Status_Cd = Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Std_Column_Name = 'Invoice_Collection_Queue_Batch_Status_Cd'
            AND c.Code_Value = 'Pending';

        CREATE TABLE #IC_Account_Config_Ids_Chase_Priority_Date
             (
                 Invoice_Collection_Account_Config_Id INT NOT NULL
             );

        CREATE TABLE #Active_Icac
             (
                 Invoice_Collection_Account_Config_Id INT NOT NULL
             );
        CREATE TABLE #Invoice_Collection_Batch_Dtl
             (
                 Invoice_Collection_Account_Config_Id INT NOT NULL
                 , Collection_Start_Dt DATE NOT NULL
                 , Collection_End_Dt DATE NOT NULL
                 , Invoice_Collection_Batch_Type_Cd INT
                 , Row_Num INT IDENTITY(1, 1)
             );

        DECLARE @Other_Changes_Codes TABLE
              (
                  Id INT IDENTITY(1, 1)
                  , Code_Id INT
                  , Code_Value VARCHAR(25)
              );

        INSERT INTO #Active_Icac
             (
                 Invoice_Collection_Account_Config_Id
             )
        SELECT
            icac.Invoice_Collection_Account_Config_Id
        FROM
            dbo.Invoice_Collection_Account_Config icac
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = icac.Account_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
        WHERE
            icac.Is_Chase_Activated = 1
            AND icac.is_Config_Complete = 1
            AND (   EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Invoice_collection_New_Batch_Clients icnbc
                               WHERE
                                    icnbc.Client_Id = ch.Client_Id)
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.Account_Invoice_Collection_Frequency aicf
                                   WHERE
                                        aicf.Invoice_Frequency_Cd = @Custom_Frequency_Cd
                                        AND aicf.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id))
        GROUP BY
            icac.Invoice_Collection_Account_Config_Id;

        INSERT INTO #Active_Icac
             (
                 Invoice_Collection_Account_Config_Id
             )
        SELECT
            icac.Invoice_Collection_Account_Config_Id
        FROM
            dbo.Invoice_Collection_Account_Config icac
        WHERE
            icac.Is_Chase_Activated = 1
            AND icac.is_Config_Complete = 1
            AND NOT EXISTS (SELECT  TOP 1   * FROM  #Active_Icac)
        GROUP BY
            icac.Invoice_Collection_Account_Config_Id;

        INSERT INTO @Other_Changes_Codes
             (
                 Code_Id
                 , Code_Value
             )
        SELECT
            Code_Id
            , Code_Value
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'ICBatchType'
            AND c.Code_Value NOT IN ( 'Received Not Processed', 'Volume Charge Check', 'Freq Next Config Run Dt' )
        ORDER BY
            c.Display_Seq;


        -- account config insert (New Config)        

        SELECT
            @Invoice_Collection_Batch_Type_Cd = Code_Id
            , @Invoice_Collection_Batch_Type_New_Config_Cd = Code_Id
        FROM
            @Other_Changes_Codes occ
        WHERE
            occ.Code_Value = 'New Config';

        INSERT INTO #Invoice_Collection_Batch_Dtl
             (
                 Invoice_Collection_Account_Config_Id
                 , Collection_Start_Dt
                 , Collection_End_Dt
                 , Invoice_Collection_Batch_Type_Cd
             )
        SELECT
            x.Invoice_Collection_Account_Config_Id
            , x.Invoice_Collection_Service_Start_Dt
            , x.Invoice_Collection_Service_End_Dt
            , @Invoice_Collection_Batch_Type_Cd
        FROM
            cdc.fn_cdc_get_all_changes_dbo_Invoice_Collection_Account_Config(@begin_lsn, @end_lsn, N'all') x
            INNER JOIN #Active_Icac ai
                ON ai.Invoice_Collection_Account_Config_Id = x.Invoice_Collection_Account_Config_Id
        WHERE
            [__$operation] = 2
            AND x.Is_Chase_Activated = 1
        GROUP BY
            x.Invoice_Collection_Account_Config_Id
            , x.Invoice_Collection_Service_Start_Dt
            , x.Invoice_Collection_Service_End_Dt;
        /*        
     To consider the newly created configurations if the chase is active and it never processed before        
     This will be useful if for some reason the batch did not triggered for a day and newly created configurations on that day may not be considered when the batch runs next time.        
        
   */

        INSERT INTO #Invoice_Collection_Batch_Dtl
             (
                 Invoice_Collection_Account_Config_Id
                 , Collection_Start_Dt
                 , Collection_End_Dt
                 , Invoice_Collection_Batch_Type_Cd
             )
        SELECT
            icac.Invoice_Collection_Account_Config_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt
            , @Invoice_Collection_Batch_Type_Cd
        FROM
            dbo.Invoice_Collection_Account_Config icac
        WHERE
            icac.Is_Chase_Activated = 1
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #Invoice_Collection_Batch_Dtl bd
                               WHERE
                                    icac.Invoice_Collection_Account_Config_Id = bd.Invoice_Collection_Account_Config_Id
                                    AND bd.Invoice_Collection_Batch_Type_Cd = @Invoice_Collection_Batch_Type_Cd)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Account_Invoice_Collection_Month aicm
                               WHERE
                                    aicm.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id)
            AND EXISTS (   SELECT
                                1
                           FROM
                                #Active_Icac ai
                           WHERE
                                ai.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id);


        -- account config update        
        SELECT
            @Invoice_Collection_Batch_Type_Cd = Code_Id
        FROM
            @Other_Changes_Codes occ
        WHERE
            occ.Code_Value = 'Config Changes';



        INSERT INTO #Invoice_Collection_Batch_Dtl
             (
                 Invoice_Collection_Account_Config_Id
                 , Collection_Start_Dt
                 , Collection_End_Dt
                 , Invoice_Collection_Batch_Type_Cd
             )
        SELECT
            x.Invoice_Collection_Account_Config_Id
            , x.Invoice_Collection_Service_Start_Dt
            , x.Invoice_Collection_Service_End_Dt
            , @Invoice_Collection_Batch_Type_Cd
        FROM
            cdc.fn_cdc_get_all_changes_dbo_Invoice_Collection_Account_Config(@begin_lsn, @end_lsn, N'all update old') x
            INNER JOIN (   SELECT
                                MAX([__$start_lsn]) [__$start_lsn]
                           FROM
                                cdc.fn_cdc_get_all_changes_dbo_Invoice_Collection_Account_Config(
                                    @begin_lsn, @end_lsn, N'all update old') x
                           WHERE
                                [__$operation] = 3
                           GROUP BY
                               Invoice_Collection_Account_Config_Id) y
                ON x.[__$start_lsn] = y.[__$start_lsn]
        WHERE
            [__$operation] = 3
            AND EXISTS (   SELECT
                                1
                           FROM
                                #Active_Icac ai
                           WHERE
                                ai.Invoice_Collection_Account_Config_Id = x.Invoice_Collection_Account_Config_Id)
            AND EXISTS (   SELECT
                                1
                           FROM
                                dbo.Invoice_Collection_Account_Config icac
                           WHERE
                                (   icac.Invoice_Collection_Service_Start_Dt <> x.Invoice_Collection_Service_Start_Dt
                                    OR  icac.Invoice_Collection_Service_End_Dt <> x.Invoice_Collection_Service_End_Dt
                                    OR  icac.Additional_Invoices_Per_Period <> x.Additional_Invoices_Per_Period
                                    OR  icac.Chase_Priority_Cd <> x.Chase_Priority_Cd
                                    OR  icac.Additional_Invoices_Per_Period <> x.Additional_Invoices_Per_Period
                                    OR  (   x.Is_Chase_Activated = 0
                                            AND icac.Is_Chase_Activated = 1)
                                    OR  (   x.Is_Chase_Activated = 1
                                            AND icac.Is_Chase_Activated = 0
                                            AND EXISTS (   SELECT
                                                                1
                                                           FROM
                                                                dbo.Invoice_Collection_Queue icq
                                                                INNER JOIN dbo.Code sc
                                                                    ON sc.Code_Id = icq.Status_Cd
                                                           WHERE
                                                                icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                                AND sc.Code_Value = 'Open'))))
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #Invoice_Collection_Batch_Dtl bd
                               WHERE
                                    bd.Invoice_Collection_Account_Config_Id = x.Invoice_Collection_Account_Config_Id
                                    AND bd.Invoice_Collection_Batch_Type_Cd = @Invoice_Collection_Batch_Type_New_Config_Cd)
        GROUP BY
            x.Invoice_Collection_Account_Config_Id
            , x.Invoice_Collection_Service_Start_Dt
            , x.Invoice_Collection_Service_End_Dt;

        -- account Source update        
        SELECT
            @Invoice_Collection_Batch_Type_Cd = Code_Id
        FROM
            @Other_Changes_Codes occ
        WHERE
            occ.Code_Value = 'Config Source Changes';

        INSERT INTO #Invoice_Collection_Batch_Dtl
             (
                 Invoice_Collection_Account_Config_Id
                 , Collection_Start_Dt
                 , Collection_End_Dt
                 , Invoice_Collection_Batch_Type_Cd
             )
        SELECT
            x.Invoice_Collection_Account_Config_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt
            , @Invoice_Collection_Batch_Type_Cd
        FROM
            cdc.fn_cdc_get_all_changes_dbo_Account_Invoice_Collection_Source(@begin_lsn, @end_lsn, N'all update old') x
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON x.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN (   SELECT
                                MAX([__$start_lsn]) [__$start_lsn]
                           FROM
                                cdc.fn_cdc_get_all_changes_dbo_Account_Invoice_Collection_Source(
                                    @begin_lsn, @end_lsn, N'all update old') x
                           WHERE
                                [__$operation] = 3
                           GROUP BY
                               Invoice_Collection_Account_Config_Id) y
                ON x.[__$start_lsn] = y.[__$start_lsn]
        WHERE
            [__$operation] = 3
            AND EXISTS (   SELECT
                                1
                           FROM
                                #Active_Icac ai
                           WHERE
                                ai.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id)
            AND EXISTS (   SELECT
                                1
                           FROM
                                dbo.Account_Invoice_Collection_Source aics
                                INNER JOIN dbo.Invoice_Collection_Account_Config icac1
                                    ON x.Invoice_Collection_Account_Config_Id = icac1.Invoice_Collection_Account_Config_Id
                           WHERE
                                (   aics.Invoice_Collection_Source_Cd <> x.Invoice_Collection_Source_Cd
                                    OR  aics.Is_Primary <> x.Is_Primary)
                                AND aics.Account_Invoice_Collection_Source_Id = x.Account_Invoice_Collection_Source_Id)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #Invoice_Collection_Batch_Dtl bd
                               WHERE
                                    bd.Invoice_Collection_Account_Config_Id = x.Invoice_Collection_Account_Config_Id
                                    AND bd.Invoice_Collection_Batch_Type_Cd = @Invoice_Collection_Batch_Type_New_Config_Cd)
        GROUP BY
            x.Invoice_Collection_Account_Config_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt;





        -- Frequency update        
        SELECT
            @Invoice_Collection_Batch_Type_Cd = Code_Id
        FROM
            @Other_Changes_Codes occ
        WHERE
            occ.Code_Value = 'Config Frequency Changes';

        INSERT INTO #Invoice_Collection_Batch_Dtl
             (
                 Invoice_Collection_Account_Config_Id
                 , Collection_Start_Dt
                 , Collection_End_Dt
                 , Invoice_Collection_Batch_Type_Cd
             )
        SELECT
            icac.Invoice_Collection_Account_Config_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt
            , @Invoice_Collection_Batch_Type_Cd
        FROM
            dbo.Account_Invoice_Collection_Frequency aicf
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON aicf.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN Code cp
                ON cp.Code_Id = icac.Chase_Priority_Cd
        WHERE
            1 = CASE WHEN cp.Code_Value = 'Day Expected'
                          AND   aicf.Invoice_Frequency_Cd IN ( @bimonthly, @quarterly, @biannual, @annual )
                          AND   (   aicf.Expected_Invoice_Received_Day <> 0
                                    OR  aicf.Expected_Invoice_Received_Day IS NOT NULL)
                          AND   aicf.Expected_Invoice_Received_Month IS NOT NULL THEN 1
                    WHEN cp.Code_Value = 'Day Expected'
                         AND aicf.Invoice_Frequency_Cd IN ( @monthly )
                         AND (   aicf.Expected_Invoice_Received_Day <> 0
                                 OR aicf.Expected_Invoice_Received_Day IS NOT NULL) THEN 1
                    WHEN cp.Code_Value = 'Day Issued'
                         AND aicf.Invoice_Frequency_Cd IN ( @bimonthly, @quarterly, @biannual, @annual )
                         AND (   aicf.Expected_Invoice_Raised_Day <> 0
                                 OR aicf.Expected_Invoice_Raised_Day IS NOT NULL)
                         AND aicf.Expected_Invoice_Raised_Month IS NOT NULL THEN 1
                    WHEN cp.Code_Value = 'Day Issued'
                         AND aicf.Invoice_Frequency_Cd IN ( @monthly )
                         AND (   aicf.Expected_Invoice_Raised_Day <> 0
                                 OR aicf.Expected_Invoice_Raised_Day IS NOT NULL) THEN 1
                    WHEN cp.Code_Value = 'Day Issued'
                         AND aicf.Invoice_Frequency_Cd IN ( @Custom )
                         AND aicf.Invoice_Frequency_Pattern_Cd = @Custom_frq_1_9_Cd
                         AND (   aicf.Expected_Invoice_Raised_Day <> 0
                                 OR aicf.Expected_Invoice_Raised_Day IS NOT NULL) THEN 1
                    WHEN cp.Code_Value = 'Day Expected'
                         AND aicf.Invoice_Frequency_Cd IN ( @Custom )
                         AND aicf.Invoice_Frequency_Pattern_Cd = @Custom_frq_1_9_Cd
                         AND (   aicf.Expected_Invoice_Received_Day <> 0
                                 OR aicf.Expected_Invoice_Received_Day IS NOT NULL) THEN 1
                    WHEN aicf.Invoice_Frequency_Cd IN ( @Custom )
                         AND aicf.Invoice_Frequency_Pattern_Cd = @Custom_frq_Custom_Days_Cd
                         AND (   aicf.Custom_Days IS NOT NULL
                                 OR aicf.Custom_Days <> 0)
                         AND (   aicf.Custom_Invoice_Received_Day IS NOT NULL
                                 OR aicf.Custom_Invoice_Received_Day <> 0) THEN 1
                    ELSE 0
                END
            AND EXISTS (   SELECT
                                1
                           FROM
                                dbo.Account_Invoice_Collection_Month aicm
                           WHERE
                                aicf.Invoice_Collection_Account_Config_Id = aicm.Invoice_Collection_Account_Config_Id
                                AND aicm.Seq_No = aicf.Seq_No
                                AND (   (   aicm.Account_Invoice_Collection_Frequency_Id IS NULL
                                            AND aicm.Invoice_Collection_Global_Config_Value_Id IS NULL)
                                        OR  (   aicf.Invoice_Frequency_Cd <> aicm.Invoice_Frequency_Cd
                                                AND aicf.Account_Invoice_Collection_Frequency_Id = ISNULL(
                                                                                                       aicm.Account_Invoice_Collection_Frequency_Id
                                                                                                       , -1))
                                        OR  (   aicf.Invoice_Frequency_Pattern_Cd <> aicm.Invoice_Frequency_Pattern_Cd
                                                AND aicf.Account_Invoice_Collection_Frequency_Id = ISNULL(
                                                                                                       aicm.Account_Invoice_Collection_Frequency_Id
                                                                                                       , -1))))
            AND EXISTS (   SELECT
                                1
                           FROM
                                #Active_Icac ai
                           WHERE
                                ai.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #Invoice_Collection_Batch_Dtl bd
                               WHERE
                                    bd.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                    AND bd.Invoice_Collection_Batch_Type_Cd = @Invoice_Collection_Batch_Type_New_Config_Cd)
        GROUP BY
            icac.Invoice_Collection_Account_Config_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt;


        -- Invoice Posted in between ICQ Start And End Dates        

        SELECT
            @Invoice_Collection_Batch_Type_Cd = Code_Id
        FROM
            @Other_Changes_Codes occ
        WHERE
            occ.Code_Value = 'Invoice Related Changes';


        -- Invoice Posted        
        INSERT INTO #Invoice_Collection_Batch_Dtl
             (
                 Invoice_Collection_Account_Config_Id
                 , Collection_Start_Dt
                 , Collection_End_Dt
                 , Invoice_Collection_Batch_Type_Cd
             )
        SELECT
            icac.Invoice_Collection_Account_Config_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt
            , @Invoice_Collection_Batch_Type_Cd
        FROM
            CHANGETABLE(CHANGES dbo.CU_INVOICE, @Min_CT_CU_Invoice_Ver) ct
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.CU_INVOICE_ID = ct.CU_INVOICE_ID
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icac.Account_Id = cism.Account_ID
            INNER JOIN dbo.Account_Invoice_Collection_Month aicm
                ON aicm.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                   AND  cism.SERVICE_MONTH = aicm.Service_Month
            INNER JOIN dbo.Invoice_Collection_Queue_Month_Map icqmp
                ON aicm.Account_Invoice_Collection_Month_Id = icqmp.Account_Invoice_Collection_Month_Id
        WHERE
            ct.SYS_CHANGE_VERSION <= @Max_CT_Ver
            AND EXISTS (   SELECT
                                1
                           FROM
                                #Active_Icac ai
                           WHERE
                                ai.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #Invoice_Collection_Batch_Dtl bd
                               WHERE
                                    bd.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                    AND bd.Invoice_Collection_Batch_Type_Cd = @Invoice_Collection_Batch_Type_Cd)
        GROUP BY
            icac.Invoice_Collection_Account_Config_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt;

        -- Invoice Related Changes        
        INSERT INTO #Invoice_Collection_Batch_Dtl
             (
                 Invoice_Collection_Account_Config_Id
                 , Collection_Start_Dt
                 , Collection_End_Dt
                 , Invoice_Collection_Batch_Type_Cd
             )
        SELECT
            icac.Invoice_Collection_Account_Config_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt
            , @Invoice_Collection_Batch_Type_Cd
        FROM
            CHANGETABLE(CHANGES dbo.CU_INVOICE, @Min_CT_CU_Invoice_Ver) ct
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.CU_INVOICE_ID = ct.CU_INVOICE_ID
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icac.Account_Id = cism.Account_ID
            INNER JOIN dbo.Account_Invoice_Collection_Month aicm
                ON aicm.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                   AND  cism.SERVICE_MONTH = aicm.Service_Month
            INNER JOIN dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map aicmcim
                ON aicm.Account_Invoice_Collection_Month_Id = aicmcim.Account_Invoice_Collection_Month_Id
        WHERE
            ct.SYS_CHANGE_VERSION <= @Max_CT_Ver
            AND EXISTS (   SELECT
                                1
                           FROM
                                #Active_Icac ai
                           WHERE
                                ai.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #Invoice_Collection_Batch_Dtl bd
                               WHERE
                                    bd.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                    AND bd.Invoice_Collection_Batch_Type_Cd = @Invoice_Collection_Batch_Type_Cd)
        GROUP BY
            icac.Invoice_Collection_Account_Config_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt;





        --Invoice Backed Out        

        SELECT
            @Invoice_Collection_Batch_Type_Cd = Code_Id
        FROM
            @Other_Changes_Codes occ
        WHERE
            occ.Code_Value = 'Invoice Backed Out';


        INSERT INTO #Invoice_Collection_Batch_Dtl
             (
                 Invoice_Collection_Account_Config_Id
                 , Collection_Start_Dt
                 , Collection_End_Dt
                 , Invoice_Collection_Batch_Type_Cd
             )
        SELECT
            icac.Invoice_Collection_Account_Config_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt
            , @Invoice_Collection_Batch_Type_Cd
        FROM
            CHANGETABLE(CHANGES dbo.CU_INVOICE, @Min_CT_CU_Invoice_Ver) ct
            INNER JOIN dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map aicmcim
                ON aicmcim.Cu_Invoice_Id = ct.Cu_Invoice_Id
            INNER JOIN dbo.Account_Invoice_Collection_Month aicm
                ON aicmcim.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON aicm.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
        WHERE
            ct.SYS_CHANGE_VERSION <= @Max_CT_Ver
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.CU_INVOICE_SERVICE_MONTH cism
                               WHERE
                                    cism.CU_INVOICE_ID = aicmcim.Cu_Invoice_Id
                                    AND cism.Account_ID = icac.Account_Id
                                    AND ISNULL(cism.SERVICE_MONTH, '1900-01-01') = ISNULL(
                                                                                       aicm.Service_Month, '1900-01-01'))
            AND EXISTS (   SELECT
                                1
                           FROM
                                #Active_Icac ai
                           WHERE
                                ai.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #Invoice_Collection_Batch_Dtl icbd
                               WHERE
                                    icbd.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                    AND icbd.Invoice_Collection_Batch_Type_Cd = @Invoice_Collection_Batch_Type_Cd)
        GROUP BY
            icac.Invoice_Collection_Account_Config_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt;





        --ICQ Exists With Open Status though it has invoice        

        SELECT
            @Invoice_Collection_Batch_Type_Cd = Code_Id
        FROM
            @Other_Changes_Codes occ
        WHERE
            occ.Code_Value = 'Invoice Related Changes';

        INSERT INTO #Invoice_Collection_Batch_Dtl
             (
                 Invoice_Collection_Account_Config_Id
                 , Collection_Start_Dt
                 , Collection_End_Dt
                 , Invoice_Collection_Batch_Type_Cd
             )
        SELECT
            icac.Invoice_Collection_Account_Config_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt
            , @Invoice_Collection_Batch_Type_Cd
        FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON icac.Account_Id = cism.Account_ID
                   AND  (   icq.Collection_Start_Dt BETWEEN cism.Begin_Dt
                                                    AND     cism.End_Dt
                            OR  icq.Collection_End_Dt BETWEEN cism.Begin_Dt
                                                      AND     cism.End_Dt)
            INNER JOIN CHANGETABLE(CHANGES dbo.CU_INVOICE, @Min_CT_CU_Invoice_Ver) ct
                ON cism.CU_INVOICE_ID = ct.CU_INVOICE_ID
            INNER JOIN dbo.Code icqsc
                ON icq.Status_Cd = icqsc.Code_Id
            INNER JOIN dbo.Code icqec
                ON icq.Invoice_Collection_Exception_Type_Cd = icqec.Code_Id
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = icac.Account_Id
        WHERE
            ct.SYS_CHANGE_VERSION <= @Max_CT_Ver
            AND icqsc.Code_Value = 'Open'
            AND icqec.Code_Value = 'Gap in Invoices'
            AND EXISTS (   SELECT
                                1
                           FROM
                                #Active_Icac ai
                           WHERE
                                ai.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Account_Invoice_Collection_Month_Cu_Invoice_Map aicmci
                               WHERE
                                    aicmci.Cu_Invoice_Id = cism.CU_INVOICE_ID)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #Invoice_Collection_Batch_Dtl bd
                               WHERE
                                    icac.Invoice_Collection_Account_Config_Id = bd.Invoice_Collection_Account_Config_Id
                                    AND bd.Invoice_Collection_Batch_Type_Cd = @Invoice_Collection_Batch_Type_Cd)
        GROUP BY
            icac.Invoice_Collection_Account_Config_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt;





        DELETE
        icbd
        FROM
            #Invoice_Collection_Batch_Dtl icbd
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icac.Invoice_Collection_Account_Config_Id = icbd.Invoice_Collection_Account_Config_Id
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = icac.Account_Id
        WHERE
            cha.Account_Not_Managed = 1;


        SELECT
            icbd.Invoice_Collection_Account_Config_Id
            , icbd.Collection_Start_Dt
            , icbd.Collection_End_Dt
            , icbd.Invoice_Collection_Batch_Type_Cd
            , ROW_NUMBER() OVER (PARTITION BY
                                     icbd.Invoice_Collection_Batch_Type_Cd
                                 ORDER BY
                                     icbd.Invoice_Collection_Account_Config_Id) Sno
            , icbd.Row_Num
        INTO
            #Invoice_Collection_Batch_Dtl_loop
        FROM
            #Invoice_Collection_Batch_Dtl icbd;



        DROP TABLE #Invoice_Collection_Batch_Dtl;

        BEGIN TRY
            BEGIN TRAN;
            WHILE (@Other_Code_Cnt <= (SELECT   MAX(occ.Id)FROM @Other_Changes_Codes occ))
                BEGIN
                    WHILE (@Record_Cnt <= (   SELECT
                                                    MAX(icbt.Sno)
                                              FROM
                                                    #Invoice_Collection_Batch_Dtl_loop icbt
                                                    INNER JOIN @Other_Changes_Codes occ
                                                        ON occ.Code_Id = icbt.Invoice_Collection_Batch_Type_Cd
                                              WHERE
                                                    occ.Id = @Other_Code_Cnt))
                        BEGIN

                            INSERT INTO dbo.Invoice_Collection_Batch
                                 (
                                     Invoice_Collection_Batch_Type_Cd
                                     , Status_Cd
                                 )
                            SELECT
                                occ.Code_Id
                                , @Status_Cd
                            FROM
                                @Other_Changes_Codes occ
                            WHERE
                                occ.Id = @Other_Code_Cnt;

                            SELECT  @Upload_Batch_Id = SCOPE_IDENTITY();

                            INSERT INTO dbo.Invoice_Collection_Batch_Dtl
                                 (
                                     Invoice_Collection_Account_Config_Id
                                     , Invoice_Collection_Batch_Id
                                     , Collection_Start_Dt
                                     , Collection_End_Dt
                                     , Status_Cd
                                 )
                            SELECT
                                Invoice_Collection_Account_Config_Id
                                , @Upload_Batch_Id
                                , MAX(Collection_Start_Dt)
                                , MAX(Collection_End_Dt)
                                , @Status_Cd
                            FROM
                                #Invoice_Collection_Batch_Dtl_loop icbd
                                INNER JOIN @Other_Changes_Codes occ
                                    ON occ.Code_Id = icbd.Invoice_Collection_Batch_Type_Cd
                            WHERE
                                icbd.Sno BETWEEN @Record_Cnt
                                         AND     @Record_Cnt + (CAST(@Batch_Size AS INT) - 1)
                                AND occ.Id = @Other_Code_Cnt
                            GROUP BY
                                Invoice_Collection_Account_Config_Id;

                            SET @Record_Cnt = @Record_Cnt + CAST(@Batch_Size AS INT);
                        END;
                    SET @Record_Cnt = 1;
                    SET @Other_Code_Cnt = @Other_Code_Cnt + 1;
                END;
            UPDATE
                dbo.App_Config
            SET
                App_Config_Value = @end_time
            WHERE
                App_Config_Cd = 'IC_Batch_Other_Process_Last_Run_Dt';

            UPDATE
                dbo.App_Config
            SET
                App_Config_Value = CAST(@Max_CT_Ver AS VARCHAR(255))
            WHERE
                App_Config_Cd = 'IC_Batch_Process_Last_Run_CU_Invoice_Data';



            DROP TABLE
                #Invoice_Collection_Batch_Dtl_loop
                , #Active_Icac;
            DROP TABLE #IC_Account_Config_Ids_Chase_Priority_Date;

            COMMIT TRAN;
        END TRY
        BEGIN CATCH

            IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRAN;
                END;

            EXEC dbo.usp_RethrowError;

        END CATCH;

    END;




    ;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Batch_Dtls_Ins_Other_Changes_Records] TO [CBMSApplication]
GO
