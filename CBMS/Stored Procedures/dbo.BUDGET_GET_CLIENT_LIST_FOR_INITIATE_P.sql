SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.BUDGET_GET_CLIENT_LIST_FOR_INITIATE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE    PROCEDURE dbo.BUDGET_GET_CLIENT_LIST_FOR_INITIATE_P
@userId varchar(10),
@sessionId varchar(20)

AS
SELECT CLIENT_ID, CLIENT_NAME FROM CLIENT 
WHERE NOT_MANAGED = 0  
ORDER BY CLIENT_NAME
GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_CLIENT_LIST_FOR_INITIATE_P] TO [CBMSApplication]
GO
