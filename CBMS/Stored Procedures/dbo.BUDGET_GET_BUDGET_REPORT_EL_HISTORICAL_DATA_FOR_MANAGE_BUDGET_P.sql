
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*=============================================================
Name:
    CBMS.dbo.BUDGET_GET_BUDGET_REPORT_EL_HISTORICAL_DATA_FOR_MANAGE_BUDGET_P
    
Description:
    

Input Parameters:
	Name					DataType		Default	Description
---------------------------------------------------------------
	@BudgetID				INT
	@From_Month			DATETIME
	@To_Month				DATETIME
	@ClientID				INT
	@DivisionID			INT
	@SiteID				INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
    dbo.BUDGET_GET_BUDGET_REPORT_EL_HISTORICAL_DATA_FOR_MANAGE_BUDGET_P 5253,'2008-01-01','2010-12-01',11541,null,42701

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
     RK		Rajesh Kasoju
     AP		Athmaram Pabbathi

MODIFICATIONS:
Initials	Date		    Modification
----------------------------------------------------------------
RK		07/26/2011    Removed @User_ID & @Session_ID parameters and 
						Modified the SP logic and Remove Cost_Usage table & use Cost_Usage_Account_Dtl, Bucket_Master, Commodity tables
AP		09/21/2011    Removed @CommodityID (not required) parameter and replaced Account, Site, Division, Client tables with Client_Hier, Client_Hier_Account tables
				    and removed Budget_Account table & used the Budget_Account_ID reference from view.
				    Removed hardcoded buckets & Bucket_Master table and used dbo.Cost_usage_Bucket_Sel_By_Commodity SP
=============================================================*/

CREATE PROCEDURE [dbo].[BUDGET_GET_BUDGET_REPORT_EL_HISTORICAL_DATA_FOR_MANAGE_BUDGET_P]
      ( 
       @BudgetId INT
      ,@From_Month DATETIME
      ,@To_Month DATETIME
      ,@ClientID INT
      ,@DivisionID INT
      ,@SiteID INT )
AS 
BEGIN
      SET NOCOUNT ON
      
      DECLARE
            @Cons_Converted_Unit_ID INT
           ,@EL_Commodity_Id INT
           ,@UOM_Entity_Type INT

      DECLARE @Cost_Usage_Bucket_Id TABLE
            ( 
             Bucket_Master_Id INT PRIMARY KEY CLUSTERED
            ,Bucket_Name VARCHAR(200)
            ,Bucket_Type VARCHAR(25) )

      SELECT
            @EL_Commodity_Id = com.Commodity_Id
           ,@UOM_Entity_Type = com.UOM_Entity_Type
      FROM
            dbo.Commodity com
      WHERE
            com.Commodity_Name = 'Electric Power'
    
      INSERT      INTO @Cost_Usage_Bucket_Id
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Bucket_Type )
                  EXEC dbo.Cost_usage_Bucket_Sel_By_Commodity 
                        @Commodity_id = @EL_Commodity_Id ;
                        
      SELECT
            @Cons_Converted_Unit_ID = uom.Entity_ID
      FROM
            dbo.ENTITY UOM
      WHERE
            UOM.Entity_Type = @UOM_Entity_Type
            AND UOM.Entity_Name = 'kWh'


      SELECT
            CUAD.Service_Month
           ,( CUAD.Bucket_Value * CNUC.Conversion_Factor ) AS Usage_Value
           ,BAT.Budget_Account_ID
      FROM
            dbo.Cost_Usage_Account_Dtl CUAD
            INNER JOIN @Cost_Usage_Bucket_Id CUB
                  ON CUB.Bucket_Master_Id = CUAD.Bucket_Master_Id
            INNER JOIN dbo.Consumption_Unit_Conversion CNUC
                  ON CNUC.Base_Unit_ID = CUAD.UOM_Type_Id
            INNER JOIN Budget_Account_Type_VW BAT
                  ON BAT.Account_ID = CUAD.Account_ID
            INNER JOIN Budget B
                  ON B.Budget_ID = BAT.Budget_ID
            INNER JOIN ( SELECT
                              CHA.Account_Id
                             ,CH.Site_Id
                             ,CH.Sitegroup_Id
                             ,CH.Client_Id
                             ,ch.Client_Hier_Id
                         FROM
                              Core.Client_Hier_Account CHA
                              INNER JOIN Core.Client_Hier CH
                                    ON CH.Client_Hier_ID = CHA.Client_Hier_Id
                         GROUP BY
                              CHA.Account_Id
                             ,CH.Site_Id
                             ,CH.Sitegroup_Id
                             ,CH.Client_Id
                             ,ch.Client_Hier_Id ) CHA
                  ON CHA.Account_ID = CUAD.Account_ID
                     AND cha.Client_Hier_Id = cuad.Client_Hier_Id
      WHERE
            CUB.Bucket_Type = 'Determinant'
            AND B.Budget_ID = @BudgetID
            AND CNUC.Converted_Unit_ID = @Cons_Converted_Unit_ID
            AND CUAD.Service_Month BETWEEN @From_Month
                                   AND     @To_Month
            AND ( @SiteID IS NULL
                  OR CHA.Site_ID = @SiteID )
            AND ( @DivisionID IS NULL
                  OR CHA.Sitegroup_Id = @DivisionID )
            AND ( @ClientID IS NULL
                  OR CHA.Client_ID = @ClientID )
		
END
;
GO

GRANT EXECUTE ON  [dbo].[BUDGET_GET_BUDGET_REPORT_EL_HISTORICAL_DATA_FOR_MANAGE_BUDGET_P] TO [CBMSApplication]
GO
