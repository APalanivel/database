SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.DMO_Supplier_Account_Open_End_Date_Sel_By_DMO_Supplier_Account
           
DESCRIPTION:             
			To check whether DMO suppleir account meters overlapping with new dates
			
INPUT PARAMETERS:            
	Name					DataType		Default		Description  
---------------------------------------------------------------------------------  
	@Supplier_Account_Id	INT
    @DMO_Supp_Start_Dt	DATE
    @DMO_Supp_End_Dt	DATE			NULL
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
	EXEC dbo.DMO_Supplier_Account_Meter_Sel 235,290,'05/01/2017','05/31/2017',193656
	EXEC dbo.DMO_Supplier_Account_Open_End_Date_Sel_By_DMO_Supplier_Account 1149300,'04/01/2017','04/30/2017'
	EXEC dbo.DMO_Supplier_Account_Open_End_Date_Sel_By_DMO_Supplier_Account 1149300,'05/01/2017','05/31/2017'
	EXEC dbo.DMO_Supplier_Account_Open_End_Date_Sel_By_DMO_Supplier_Account 1149300,'06/01/2017','06/30/2017'
	EXEC dbo.DMO_Supplier_Account_Open_End_Date_Sel_By_DMO_Supplier_Account 1149300,'08/01/2017','09/30/2017'
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-03-16	Contract placeholder - CP-8 Created
******/

CREATE PROCEDURE [dbo].[DMO_Supplier_Account_Open_End_Date_Sel_By_DMO_Supplier_Account]
    (
        @Meters VARCHAR(MAX)
        , @Supplier_Account_Id INT
        , @DMO_Supp_Start_Dt DATE
        , @DMO_Supp_End_Dt DATE = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;


        SELECT
            samm.ACCOUNT_ID
            , samm.Supplier_Account_Config_Id
        FROM
            dbo.SUPPLIER_ACCOUNT_METER_MAP samm
            INNER JOIN dbo.ufn_split(@Meters, ',') mtr
                ON mtr.Segments = samm.METER_ID
        WHERE
            samm.Contract_ID = -1
            AND samm.ACCOUNT_ID <> @Supplier_Account_Id
            AND (   (   samm.METER_DISASSOCIATION_DATE IS NULL
                        AND @DMO_Supp_End_Dt IS NULL
                        AND samm.METER_ASSOCIATION_DATE < @DMO_Supp_Start_Dt)
                    OR  (   samm.METER_DISASSOCIATION_DATE IS NULL
                            AND @DMO_Supp_End_Dt IS NOT NULL
                            AND samm.METER_ASSOCIATION_DATE < @DMO_Supp_Start_Dt
                            AND samm.METER_ASSOCIATION_DATE < @DMO_Supp_End_Dt))
        GROUP BY
            samm.ACCOUNT_ID
            , samm.Supplier_Account_Config_Id;



    END;
    ;

GO
GRANT EXECUTE ON  [dbo].[DMO_Supplier_Account_Open_End_Date_Sel_By_DMO_Supplier_Account] TO [CBMSApplication]
GO
