SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.SR_RFP_GET_RFP_ACCOUNT_P

@accountId int,
@rfpId int

as 
	set nocount on
DECLARE @rfpAccountId int
SELECT @rfpAccountId = (SELECT top 1 SR_RFP_ACCOUNT_ID 

FROM SR_RFP_ACCOUNT

WHERE ACCOUNT_ID =  @accountId AND 
	SR_RFP_ID =@rfpId and is_deleted = 0)



RETURN  @rfpAccountId
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_RFP_ACCOUNT_P] TO [CBMSApplication]
GO
