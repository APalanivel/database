SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.UPDATE_UNIT_FREQUENCY_ELECTRIC_P
	@lps_unit_type_id INT,
	@lps_frequency_type_id INT,
	@month_identifier DATETIME,
	@contract_id INT
AS
BEGIN 

	SET NOCOUNT ON

--	UPDATE load_profile_specification
--	SET ps_unit_type_id = @lps_unit_type_id,
--		lps_frequency_type_id =@lps_frequency_type_id
--	WHERE month_identifier = @month_identifier
--		AND contract_id = @contract_id
--		AND lps_type_id =(
--		SELECT entity_id 
--		FROM entity 
--		WHERE entity_type = 142 
--		 AND entity_name = 'Baseload') 
--		AND lps_unit_type_id = (
--		SELECT entity_id 
--		FROM entity 
--		WHERE entity_type = 101 
--		 AND entity_name = 'kWh')


	UPDATE dbo.load_profile_specification
		SET lps_unit_type_id = @lps_unit_type_id
			, lps_frequency_type_id =@lps_frequency_type_id
	FROM dbo.Entity e1, dbo.Entity e2 
	WHERE (lps_type_id = e1.entity_id
	      AND e1.entity_type = 142 AND e1.entity_name = 'Baseload')
	      AND (lps_unit_type_id = e2.entity_id
	      AND e1.entity_type = 101 AND e2.entity_name = 'kWh')
	      AND month_identifier = @month_identifier
	      AND contract_id = @contract_id		
	
END
GO
GRANT EXECUTE ON  [dbo].[UPDATE_UNIT_FREQUENCY_ELECTRIC_P] TO [CBMSApplication]
GO
