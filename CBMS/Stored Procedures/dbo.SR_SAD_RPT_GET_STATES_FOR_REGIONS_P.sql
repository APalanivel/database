SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_RPT_GET_STATES_FOR_REGIONS_P

DESCRIPTION:

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@regionIds     	varchar(100)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	
	EXEC dbo.SR_SAD_RPT_GET_STATES_FOR_REGIONS_P '1,2'
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/28/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on	        	
	
******/

CREATE PROCEDURE dbo.SR_SAD_RPT_GET_STATES_FOR_REGIONS_P
( @regionIds VARCHAR(100))
AS
BEGIN
	
	SET NOCOUNT ON ;
	
	DECLARE @sql VARCHAR(1000)
	
	SELECT @sql =	' Select STATE_ID, STATE_NAME  	from dbo.STATE s '
			+ ' where s.REGION_ID in ( select IntValue  from dbo.SR_SAD_RPT_FN_GET_CSV_TO_INT('+''''+@regionIds+''''+') )'
			+ ' order by STATE_NAME '
	EXEC(@sql)

	
END	
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_RPT_GET_STATES_FOR_REGIONS_P] TO [CBMSApplication]
GO
