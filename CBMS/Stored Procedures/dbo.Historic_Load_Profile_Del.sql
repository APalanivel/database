SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: dbo.Historic_Load_Profile_Del

DESCRIPTION:

	It Deletes Historic_Load_Profile for given Historic_Load_Profile_Id.

INPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
@HISTORIC_LOAD_PROFILE_ID	INT
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        
	BEGIN TRAN
		EXEC Historic_Load_Profile_Del 29	
	ROLLBACK TRAN

	SELECT *
	FROM
		dbo.HISTORIC_LOAD_PROFILE
		
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR			PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR			26-MAY-10	CREATED

*/

CREATE PROCEDURE dbo.Historic_Load_Profile_Del
    (
      @HISTORIC_LOAD_PROFILE_ID INT
    )
AS
BEGIN

    SET NOCOUNT ON;

    DELETE
   	FROM
		dbo.HISTORIC_LOAD_PROFILE
	WHERE
		HISTORIC_LOAD_PROFILE_ID = @HISTORIC_LOAD_PROFILE_ID

END
GO
GRANT EXECUTE ON  [dbo].[Historic_Load_Profile_Del] TO [CBMSApplication]
GO
