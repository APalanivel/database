SET NUMERIC_ROUNDABORT OFF 
GO

SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******              
NAME: DBO.SUPPLIER_ACCOUNTS_SEL_BY_CONTRACT_ID  
         
DESCRIPTION: TO GET THE SUPPLIER_ACCOUNT DETAILS FOR THE SELECTED CONTRACT ID WITH PAGINATION AND SORTING.    
          
INPUT PARAMETERS:              
NAME   DATATYPE    DEFAULT    DESCRIPTION              
----------------------------------------------------------------------------              
@CONTRACT_ID INT    
@SORTCOLUMN  VARCHAR     CONTRACT_ID    
@SORTINDEX  VARCHAR     ASC    
@STARTINDEX  INT     
@ENDINDEX  INT     
                    
OUTPUT PARAMETERS:              
NAME   DATATYPE    DEFAULT    DESCRIPTION              
----------------------------------------------------------------------------              
USAGE EXAMPLES:              
----------------------------------------------------------------------------              
  USE [CBMS_TK2]    
      
   EXEC dbo.SUPPLIER_ACCOUNTS_SEL_BY_CONTRACT_ID  76178,'supp_acc_number','ASC',1,25    
   EXEC dbo.SUPPLIER_ACCOUNTS_SEL_BY_CONTRACT_ID  26046,'contract_id','Desc',1,25    
   EXEC dbo.SUPPLIER_ACCOUNTS_SEL_BY_CONTRACT_ID  76199,'contract_id','Asc',1,10    
     
   EXEC dbo.SUPPLIER_ACCOUNTS_SEL_BY_CONTRACT_ID  54055,'contract_id','Asc',1,10    
   EXEC dbo.SUPPLIER_ACCOUNTS_SEL_BY_CONTRACT_ID  82948,'contract_id','Asc',1,10    
         
AUTHOR INITIALS:              
 INITIALS NAME              
----------------------------------------------------------------------------              
 DMR   Deana Ritter    
 PNR   PANDARINATH    
 RR   Raghu Reddy  
 NR   Narayana Reddy  
              
MODIFICATIONS               
 INITIALS DATE  MODIFICATION              
------------------------------------------------------------              
 PNR   05/19/2010 Created new object based on the existing sp SUPPLIER_ACCOUNTS_SEL, just added the pagination and sorting logic.  
 PNR   04/13/2011 MAINT-597 fixes the issue by adding column 'acc.WATCHLIST-GROUP-INFO-ID' at SELECT list.  
 RR   2017-04-25 MAINT-5249 Default/undefined length of VARCHAR in convert function(in cross apply) terminating string to 30 charters,  
       defined actual field length.  
 NR   2019-04-03 Data2.0 - Removed WATCHLIST-GROUP-INFO-ID  column from Account Table.  
 NR   209-07-03  Add Contract - Changed Inner to left on Entity.SERVICE_LEVEL_TYPE_ID.   
    
  
*/

CREATE PROCEDURE [dbo].[SUPPLIER_ACCOUNTS_SEL_BY_CONTRACT_ID]
    (
        @CONTRACT_ID INT
        , @SortColumn VARCHAR(200) = 'contract_id'
        , @Sortindex VARCHAR(20) = 'Asc'
        , @StartIndex INT = 1
        , @EndIndex INT = 2147483647
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Sql VARCHAR(8000);

        SET @Sql = '    
  ;WITH CTE_ADS    
  AS    
  (    
   SELECT    
   map.contract_id    
    , map.account_id ACCOUNT_ID        
    , acc.account_number SUPP_ACC_NUMBER        
    , m.meter_number METER_NUMBER      
    , CONVERT(VARCHAR(10), sac.supplier_account_begin_dt, 101) SUPPLIER_ACCOUNT_BEGIN_DT        
    , CONVERT(VARCHAR(10), sac.supplier_account_end_dt, 101) SUPPLIER_ACCOUNT_END_DT        
    , utilacc.account_number UTIL_ACCOUNT_NUMBER    
    , acc.ra_watch_list RA_WATCH_LIST    
    , acc.dm_watch_list DM_WATCH_LIST        
    , acc.not_managed NOT_MANAGED        
    , acc.not_expected NOT_EXPECTED        
    , acc.service_level_type_id SERVICE_LEVEL_TYPE_ID        
    , ent.entity_name ENTITY_NAME  
     
          
   FROM         
    dbo.supplier_account_meter_map map    
    JOIN dbo.account acc     
     ON acc.account_id = map.account_id   
	 Join Supplier_Account_Config sac
	 on sac.account_id = map.account_id   
	 and sac.Contract_id = map.Contract_id 
    JOIN dbo.meter m     
     ON map.meter_id = m.meter_id    
    JOIN dbo.account utilacc     
     ON m.account_id = utilacc.account_id    
    Left JOIN dbo.entity ent     
     ON acc.service_level_type_id = ent.entity_id    
   WHERE     
    map.contract_id = ' + CAST(@CONTRACT_ID AS VARCHAR)
                   + '    
    AND map.is_history = 0    
       
  )     
  ,  Cte_Account_List AS    
  (    
  SELECT     
   CONTRACT_ID    
     ,ACCOUNT_ID        
     ,SUPP_ACC_NUMBER      
     ,SUPPLIER_ACCOUNT_BEGIN_DT        
     ,SUPPLIER_ACCOUNT_END_DT        
     ,RA_WATCH_LIST    
     ,DM_WATCH_LIST        
     ,NOT_MANAGED        
     ,NOT_EXPECTED        
     ,SERVICE_LEVEL_TYPE_ID        
     ,ENTITY_NAME  
        
     ,Row_Num = ROW_NUMBER() OVER (ORDER BY  ' + @SortColumn + SPACE(1) + @Sortindex
                   + ')    
     ,Total=COUNT(1) OVER ()    
   FROM     
  CTE_ADS cds      
   GROUP BY    
  Contract_ID    
    ,Account_Id     
    ,SUPP_ACC_NUMBER      
    ,SUPPLIER_ACCOUNT_BEGIN_DT        
    ,SUPPLIER_ACCOUNT_END_DT        
    ,RA_WATCH_LIST    
    ,DM_WATCH_LIST        
    ,NOT_MANAGED        
    ,NOT_EXPECTED        
    ,SERVICE_LEVEL_TYPE_ID        
    ,ENTITY_NAME  
       
  )'    ;

        SET @Sql = @Sql
                   + '    
  
   SELECT     
  ACCOUNT_ID        
    ,SUPP_ACC_NUMBER      
    ,METER_NUMBER = Left(Mtr_List.Mtr_Num,Len(Mtr_List.Mtr_Num) - 1)      
    ,SUPPLIER_ACCOUNT_BEGIN_DT        
    ,SUPPLIER_ACCOUNT_END_DT        
    ,UTILITY_AACOUNT_NUMBER=Left(Uty_List.Uty_num,LEN(Uty_List.Uty_num)-1)    
    ,RA_WATCH_LIST    
    ,DM_WATCH_LIST        
    ,NOT_MANAGED        
    ,NOT_EXPECTED        
    ,SERVICE_LEVEL_TYPE_ID        
    ,ENTITY_NAME  
      
    ,Total    
   FROM     
    Cte_Account_List cds    
   CROSS APPLY      
   (      
   SELECT       
  TOP 10 CONVERT(VARCHAR(50),ab.METER_NUMBER) + '' , ''        
   FROM         
  CTE_ADS ab    
    WHERE        
     ab.account_id = cds.account_id    
       
   FOR XML PATH('''')      
   )  Mtr_List(Mtr_Num)        
          
   CROSS APPLY       
   (      
    SELECT       
  TOP 10 CONVERT(VARCHAR(50),abc.UTIL_ACCOUNT_NUMBER) + '' , ''        
    FROM         
  CTE_ADS abc    
    WHERE        
     abc.account_id = cds.account_id    
       
     FOR XML PATH('''')      
   )  Uty_List(Uty_Num)               
          
   WHERE     
    Row_Num BETWEEN ' + CAST(@StartIndex AS VARCHAR) + ' AND ' + CAST(@EndIndex AS VARCHAR);

        EXEC (@Sql);
    END;

GO


GRANT EXECUTE ON  [dbo].[SUPPLIER_ACCOUNTS_SEL_BY_CONTRACT_ID] TO [CBMSApplication]
GO
