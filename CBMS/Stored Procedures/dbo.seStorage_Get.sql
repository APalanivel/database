SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  Procedure dbo.seStorage_Get
	(
		@StorageId int
	)
As
BEGIN
	set nocount on
	 select StorageId
				, ReportDate
				, WeekEnding
				, WeekOfYear
				, Volume
				, Change
		 from seStorage
		where StorageId = @StorageId
END
GO
GRANT EXECUTE ON  [dbo].[seStorage_Get] TO [CBMSApplication]
GO
