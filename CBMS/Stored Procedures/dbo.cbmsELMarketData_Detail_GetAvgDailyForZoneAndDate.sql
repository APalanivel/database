SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO




create PROCEDURE [dbo].[cbmsELMarketData_Detail_GetAvgDailyForZoneAndDate]  
	( @price_point_id int
	, @start_date datetime
	, @end_date datetime
	)
AS
BEGIN

	   select 
		 convert(varchar(10),price_point_date,101) as price_point_date
		, avg(price_point_value) as price_point_value
	     from el_market_data_detail zd
	    where price_point_id = @price_point_id
	      and convert(varchar(10),price_point_date,101) >= @start_date
	      and convert(varchar(10),price_point_date,101) <= @end_date
	  group by convert(varchar(10),price_point_date,101) 
    	  order by convert(varchar(10),price_point_date,101) 
		

END



GO
GRANT EXECUTE ON  [dbo].[cbmsELMarketData_Detail_GetAvgDailyForZoneAndDate] TO [CBMSApplication]
GO
