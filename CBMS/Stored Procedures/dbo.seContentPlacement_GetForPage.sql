SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure dbo.seContentPlacement_GetForPage
	( @ContentPageId int )
As
BEGIN
	set nocount on
	 select ContentPlacementId
				, ContentPageId
				, PlacementLabel
		 from seContentPlacement
		where ContentPageId = @ContentPageId
 order by PlacementLabel asc

END
GO
GRANT EXECUTE ON  [dbo].[seContentPlacement_GetForPage] TO [CBMSApplication]
GO
