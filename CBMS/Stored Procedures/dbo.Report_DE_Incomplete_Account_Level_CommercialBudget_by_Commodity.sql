SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:      
    [dbo].[Report_DE_Incomplete_Account_Level_CommercialBudget_by_Commodity]    
    
 DESCRIPTION:  
	
 
INPUT PARAMETERS:      
	Name				DataType		Default					Description      
------------------------------------------------------------------------------------------    
	@Client_ID			INT          
	@Commodity_ID		INT           
	@Begin_Date			DATETIME          
	@End_Date			DATETIME		
	
OUTPUT PARAMETERS:      
	Name				DataType		Default					Description      
------------------------------------------------------------------------------------------    
 USAGE EXAMPLES:      
------------------------------------------------------------------------------------------    

 EXEC dbo.[Report_DE_Incomplete_Account_Level_CommercialBudget_by_Commodity] 12197,290,'01/01/2016','1/31/2016'
 
   
 AUTHOR INITIALS:      
Initials   Name      
------------------------------------------------------------------------------------------    
ABK		   Aditya Bharadwaj
    
MODIFICATIONS       
Initials		Date		Modification      
-------------------------------------------------------------------------------------------      
ABK				2019-01-28  Created for REPTMGR-97. 

******/

CREATE PROCEDURE [dbo].[Report_DE_Incomplete_Account_Level_CommercialBudget_by_Commodity]
    (
        @Client_ID    INT,
        @Commodity_ID INT,
        @Begin_Date   DATETIME,
        @End_Date     DATETIME
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Currency_Gr_id INT;
        DECLARE
            @Entity_unit      INT,
            @Currency_Unit_Id INT;
        DECLARE @Currency_Group_Id INT;

        CREATE TABLE #Category_site
            (
                Client_Name         VARCHAR(200),
                Sitegroup_Name      VARCHAR(200),
                State_Name          VARCHAR(200),
                Site_Name           VARCHAR(200),
                Site_id             INT,
                Client_Id           INT,
                SiteRef             VARCHAR(30),
                Service_Month       DATE,
                Account_Id          INT,
                Account_Number      VARCHAR(200),
                Account_Type        VARCHAR(50),
                Account_Vendor_Name VARCHAR(200),
                PRIMARY KEY CLUSTERED (Client_Id, Site_id, Account_Id, Service_Month)
            );

        CREATE TABLE #CU_Data
            (
                SERVICE_MONTH DATETIME,
                Site_Id       INT,
                Account_Id    INT,
                Total_Cost    DECIMAL(32, 16),
                Total_Usage   DECIMAL(32, 16),
                Utility_Cost  DECIMAL(32, 16),
                Marketer_Cost DECIMAL(32, 16)
            );

        SELECT
            @Currency_Unit_Id = cu.CURRENCY_UNIT_ID
        FROM
            CBMS.dbo.CURRENCY_UNIT cu
        WHERE
            cu.CURRENCY_UNIT_NAME = 'USD';


        SELECT
            @Currency_Gr_id = a.CURRENCY_GROUP_ID
        FROM
            CBMS.dbo.CLIENT a
        WHERE
            a.CLIENT_ID = @Client_ID;

        SELECT
            @Entity_unit = Default_UOM_Entity_Type_Id
        FROM
            CBMS.dbo.Commodity com
        WHERE
            com.Commodity_Id = @Commodity_ID;


        INSERT INTO #Category_site
                    SELECT         DISTINCT
                                   ch.Client_Name,
                                   ch.Sitegroup_Name,
                                   ch.State_Name,
                                   ch.Site_name,
                                   ch.Site_Id,
                                   ch.Client_Id,
                                   ch.Site_Reference_Number,
                                   dd.DATE_D,
                                   cha.Account_Id,
                                   cha.Account_Number,
                                   cha.Account_Type,
                                   cha.Account_Vendor_Name
                    FROM
                                   CBMS.Core.Client_Hier    ch
                        INNER JOIN
                                   Core.Client_Hier_Account cha
                                       ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        CROSS JOIN CBMS.meta.DATE_DIM       dd
                    WHERE
                                   ch.Client_Id = @Client_ID
                                   AND cha.Commodity_Id = @Commodity_ID
                                   AND ch.Site_Id <> 0
                                   AND ch.Site_Not_Managed = 0
                                   AND ch.Site_Closed = 0
                                   AND dd.DATE_D
                                   BETWEEN @Begin_Date AND @End_Date;


        WITH Cte_ipdata
        AS (   SELECT
                       vm.Account_Id,
                       ips.SITE_ID,
                       ips.SERVICE_MONTH
               FROM
                       CBMS.dbo.INVOICE_PARTICIPATION ips
                   INNER JOIN
                       (
                           SELECT
                                   cha.Account_Id,
                                   ch.Site_Id
                           FROM
                                   CBMS.Core.Client_Hier_Account cha
                               JOIN
                                   CBMS.Core.Client_Hier         ch
                                       ON cha.Client_Hier_Id = ch.Client_Hier_Id
                           WHERE
                                   (
                                       cha.Account_Type = 'Utility'
                                       OR
                                           (
                                               (
                                                   cha.Supplier_Meter_Disassociation_Date > cha.Supplier_Account_begin_Dt
                                                   OR cha.Supplier_Meter_Disassociation_Date IS NULL
                                               )
                                               AND cha.Account_Type = 'Supplier'
                                           )
                                   )
                                   AND ch.Client_Id = @Client_ID
                                   AND cha.Commodity_Id = @Commodity_ID
                           GROUP BY
                                   cha.Account_Id,
                                   ch.Site_Id
                       )                              AS vm
                           ON vm.Account_Id = ips.ACCOUNT_ID
                              AND ips.SITE_ID = vm.Site_Id
               WHERE
                       ips.SERVICE_MONTH
               BETWEEN @Begin_Date AND @End_Date
               GROUP BY
                       vm.Account_Id,
                       ips.SITE_ID,
                       ips.SERVICE_MONTH)
        INSERT INTO #CU_Data
            (
                SERVICE_MONTH,
                Site_Id,
                Account_Id,
                Total_Usage,
                Total_Cost
            )
                    SELECT
                            cus.Service_Month,
                            cs.Site_id,
                            cs.Account_Id,
                            MAX(   CASE
                                       WHEN bm.Bucket_Name = 'Total Usage'
                                           THEN
                                   (cus.Bucket_Value * ConsUC.CONVERSION_FACTOR)
                                   END
                               ) Total_Usage,
                            MAX(   CASE
                                       WHEN bm.Bucket_Name = 'Total Cost'
                                           THEN
                                   (cus.Bucket_Value * CUC.CONVERSION_FACTOR)
                                   END
                               ) Total_Cost
                    FROM
                            #Category_site                       cs
                        LEFT JOIN
                            CBMS.dbo.Cost_Usage_Account_Dtl      cus
                                ON cus.ACCOUNT_ID = cs.Account_Id
                                   AND cus.Service_Month = cs.Service_Month
                        LEFT JOIN
                            dbo.Bucket_Master                    bm
                                ON bm.Bucket_Master_Id = cus.Bucket_Master_Id
                        LEFT JOIN
                            Cte_ipdata                           cte_ip
                                ON cte_ip.SITE_ID = cs.Site_id
                                   AND cte_ip.SERVICE_MONTH = cus.Service_Month
                                   AND cte_ip.Account_Id = cus.ACCOUNT_ID
                        LEFT JOIN
                            CBMS.dbo.CURRENCY_UNIT_CONVERSION    CUC
                                ON CUC.BASE_UNIT_ID = cus.CURRENCY_UNIT_ID
                                   AND CUC.CONVERTED_UNIT_ID = @Currency_Unit_Id
                                   AND CUC.CURRENCY_GROUP_ID = @Currency_Gr_id
                                   AND CUC.CONVERSION_DATE = cus.Service_Month
                        LEFT JOIN
                            CBMS.dbo.CONSUMPTION_UNIT_CONVERSION ConsUC
                                ON ConsUC.BASE_UNIT_ID = cus.UOM_Type_Id
                                   AND ConsUC.CONVERTED_UNIT_ID = @Entity_unit
                    WHERE
                            cs.Client_Id = @Client_ID
                            AND bm.Commodity_Id = @Commodity_ID
                            AND cus.Service_Month
                            BETWEEN @Begin_Date AND @End_Date
                    GROUP BY
                            cus.Service_Month,
                            cs.Site_id,
                            cs.Account_Id;
        SELECT
                tbl_cts.Client_Name                                 [Client Name],
                tbl_cts.Sitegroup_Name                              [Division Name],
                tbl_cts.State_Name                                  [State],
                tbl_cts.Site_Name                                   [Site Name],
                tbl_cts.Site_id                                     [Site Id],
                tbl_cts.SiteRef,
                tbl_cts.Account_Id,
                tbl_cts.Account_Number,
                tbl_cts.Account_Type,
                tbl_cts.Account_Vendor_Name                         [Primary Utility],
                a.CONSOLIDATED_BILLING_POSTED_TO_UTILITY            [Consolidated_Billing_Posted_To_Utility],
                Cusdata.Total_Usage                                 Usage,
                MAX(   CASE
                           WHEN tbl_cts.Account_Type = 'Utility'
                               THEN
                               Cusdata.Total_Cost
                       END
                   )                                                AS Utility_Cost,
                MAX(   CASE
                           WHEN tbl_cts.Account_Type = 'Supplier'
                               THEN
                               Cusdata.Total_Cost
                       END
                   )                                                AS Supplier_Cost,
                Cusdata.Total_Cost,
                Cusdata.Total_Cost / NULLIF(Cusdata.Total_Usage, 0) Unit_Cost,
                tbl_cts.Service_Month                               Service_Month,
                ven.Contract_Expiry,
                CASE
                    WHEN tbl_cts.Account_Type = 'Supplier'
                        THEN
                        'Transport'
                    WHEN ven.Contract_Expiry > CONVERT(DATE, GETDATE())
                        THEN
                        'Transport'
                    ELSE
                        'Tariff'
                END                                                 Contract_Near_expiry
        FROM
                #Category_site tbl_cts
            LEFT JOIN
                (
                    SELECT
                            ch.Site_Id,
                            cha.Account_Id,
                            MIN(cha.Account_Vendor_Name) Vendor_Name,
                            MAX(con.CONTRACT_END_DATE)   Contract_Expiry
                    FROM
                            CBMS.Core.Client_Hier_Account cha
                        INNER JOIN
                            CBMS.Core.Client_Hier         ch
                                ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        LEFT JOIN
                            CBMS.Core.Client_Hier_Account chas
                                ON chas.Meter_Id = cha.Meter_Id
                                   AND chas.Client_Hier_Id = cha.Client_Hier_Id
                                   AND cha.Account_Type = 'Utility'
                                   AND chas.Account_Type = 'Supplier'
                        LEFT JOIN
                            CBMS.dbo.CONTRACT             con
                                ON con.CONTRACT_ID = chas.Supplier_Contract_ID
                    WHERE
                            cha.Commodity_Id = @Commodity_ID
                            AND ch.Client_Id = @Client_ID
                    GROUP BY
                            ch.Site_Id,
                            cha.Account_Id
                )              ven
                    ON tbl_cts.Site_id = ven.Site_Id
                       AND ven.Account_Id = tbl_cts.Account_Id
            LEFT JOIN
                #CU_Data       Cusdata
                    ON tbl_cts.Site_id = Cusdata.Site_Id
                       AND tbl_cts.Service_Month = Cusdata.SERVICE_MONTH
                       AND Cusdata.Account_Id = tbl_cts.Account_Id
            LEFT JOIN
                dbo.ACCOUNT    a
                    ON a.ACCOUNT_ID = Cusdata.Account_Id
                       AND a.ACCOUNT_ID = tbl_cts.Account_Id
                       AND a.ACCOUNT_ID = ven.Account_Id
        GROUP BY
                tbl_cts.Client_Name,
                tbl_cts.Sitegroup_Name,
                tbl_cts.State_Name,
                tbl_cts.Site_Name,
                tbl_cts.Site_id,
                tbl_cts.SiteRef,
                tbl_cts.Service_Month,
                Cusdata.Total_Usage,
                Cusdata.Utility_Cost,
                Cusdata.Marketer_Cost,
                Cusdata.Total_Cost,
                tbl_cts.Account_Id,
                tbl_cts.Account_Number,
                tbl_cts.Account_Type,
                tbl_cts.Account_Vendor_Name,
                a.CONSOLIDATED_BILLING_POSTED_TO_UTILITY,
                ven.Contract_Expiry
        ORDER BY
                tbl_cts.Service_Month,
                tbl_cts.Site_Name;

        DROP TABLE #Category_site;
        DROP TABLE #CU_Data;

    END;
    ;

GO
GRANT EXECUTE ON  [dbo].[Report_DE_Incomplete_Account_Level_CommercialBudget_by_Commodity] TO [CBMS_SSRS_Reports]
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Incomplete_Account_Level_CommercialBudget_by_Commodity] TO [CBMSApplication]
GO
