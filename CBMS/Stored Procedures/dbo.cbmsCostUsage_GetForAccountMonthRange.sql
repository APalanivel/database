
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME:
	dbo.cbmsCostUsage_GetForAccountMonthRange

DESCRIPTION:
	Used to return all the unmapped services from either determinant or charges of the gives invoice id.

INPUT PARAMETERS:
Name      DataType		Default		Description
------------------------------------------------------------
@account_id		INT
@begin_month	DATETIME
@end_month		DATETIME

OUTPUT PARAMETERS:
Name      DataType		Default		Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.cbmsCostUsage_GetForAccountMonthRange	443741, '1/1/2011','6/1/2011'
	EXEC dbo.cbmsCostUsage_GetForAccountMonthRange	446460, '1/1/2010','12/1/2010'
	EXEC dbo.cbmsCostUsage_GetForAccountMonthRange	446417, '1/1/2010','12/1/2010'

AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------
HG			Hari
RR			Raghu Reddy

MODIFICATIONS
Initials	Date		Modification
------------------------------------------------------------
HG			05/16/2011	Comments header added
						-- MAINT-584 fixes the code to not select system_row = 1 (backet out entries) for variance test.
						-- cu.System_Row and cu.Is_Default columns removed from the select clause as we have hard coded filter on these columns.
						-- DISTINCT clause replaced by GROUP BY clause
						-- unused @MyAccountId parameter has been removed
HG			06/06/2011	As the changes made to fix MAINT-584 is breaking the application flow removing the filter condition System_Row = 0
							(This condition will be added back once the issue with application fixed in the next MAINT release)
							GROUP BY Condition also removed as Cost_Usage_Id returned by this SP is UNIQUE PK of the table.
RR			2012-04-10	Part of additional data, replaced dbo.cost_usage table with dbo.Cost_Usage_Account_Dtl, removed COST_USAGE_ID
						from result set and added client hier id
						
******/
CREATE  PROCEDURE [dbo].[cbmsCostUsage_GetForAccountMonthRange]
      ( 
       @account_id INT
      ,@begin_month DATETIME
      ,@end_month DATETIME )
AS 
BEGIN

      SET NOCOUNT ON

      SELECT
            cu.account_id
           ,ch.Site_Id
           ,cu.service_month
           ,cu.Client_Hier_Id
      FROM
            dbo.Cost_Usage_Account_Dtl cu
            JOIN Core.Client_Hier ch
                  ON cu.Client_Hier_Id = ch.Client_Hier_Id
      WHERE
            cu.account_id = @account_id
            AND cu.service_month BETWEEN @begin_month
                                 AND     @end_month

END
;
GO

GRANT EXECUTE ON  [dbo].[cbmsCostUsage_GetForAccountMonthRange] TO [CBMSApplication]
GO