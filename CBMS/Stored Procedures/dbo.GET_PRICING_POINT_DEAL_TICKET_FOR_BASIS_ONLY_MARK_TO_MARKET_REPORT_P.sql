SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.GET_PRICING_POINT_DEAL_TICKET_FOR_BASIS_ONLY_MARK_TO_MARKET_REPORT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@fromDate      	varchar(12)	          	
	@toDate        	varchar(12)	          	
	@clientId      	int       	          	
	@siteId        	varchar(1000)	          	
	@divisionId    	int       	          	
	@pricePointId  	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.GET_PRICING_POINT_DEAL_TICKET_FOR_BASIS_ONLY_MARK_TO_MARKET_REPORT_P -1,-1,'01/01/2006','12/01/2006',190,'','',''

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/27/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on        	
	
******/

CREATE PROCEDURE dbo.GET_PRICING_POINT_DEAL_TICKET_FOR_BASIS_ONLY_MARK_TO_MARKET_REPORT_P
    @userId			VARCHAR,
    @sessionId		VARCHAR,
    @fromDate		VARCHAR(12),
    @toDate			VARCHAR(12),
    @clientId		INT,
    @siteId			VARCHAR(1000),
    @divisionId		INT,
    @pricePointId	INT
AS 
BEGIN
    SET NOCOUNT ON ;
    
    DECLARE  @selectQuery1 VARCHAR(2000)
		    ,@fromQuery1   VARCHAR(2000)
			,@whereQuery1  VARCHAR(8000)

    DECLARE @selectQuery2 VARCHAR(2000)
    DECLARE @fromQuery2 VARCHAR(2000)
    DECLARE @whereQuery2 VARCHAR(8000)

    DECLARE @selectQuery3 VARCHAR(2000)
    DECLARE @fromQuery3 VARCHAR(2000)
    DECLARE @whereQuery3 VARCHAR(8000)

    DECLARE @selectQuery4 VARCHAR(2000)
    DECLARE @fromQuery4 VARCHAR(2000)
    DECLARE @whereQuery4 VARCHAR(8000)

    DECLARE @selectQuery5 VARCHAR(2000)
    DECLARE @fromQuery5 VARCHAR(2000)
    DECLARE @whereQuery5 VARCHAR(8000)

    DECLARE @selectQuery6 VARCHAR(2000)
    DECLARE @fromQuery6 VARCHAR(2000)
    DECLARE @whereQuery6 VARCHAR(8000)

    DECLARE @finalQuery1 VARCHAR(8000)
    DECLARE @finalQuery2 VARCHAR(8000)
    DECLARE @finalQuery3 VARCHAR(8000)
    DECLARE @finalQuery4 VARCHAR(8000)
    DECLARE @finalQuery5 VARCHAR(8000)
    DECLARE @finalQuery6 VARCHAR(8000)

    IF ( @siteId = '0' ) 
        BEGIN
            SET @siteId = ''
        END

    SET @selectQuery1 = ' SELECT distinct priceIndex.PRICE_INDEX_ID,priceIndex.PRICING_POINT,''Financial''  HEDGE_TYPE, dealTicket.RM_DEAL_TICKET_ID '	
    SET @fromQuery1 = ' FROM RM_DEAL_TICKET  dealticket,RM_DEAL_TICKET_TRANSACTION_STATUS dealstatus,
	RM_DEAL_TICKET_DETAILS dealdetails,RM_DEAL_TICKET_VOLUME_DETAILS dealvolume,PRICE_INDEX priceIndex '
    SET @whereQuery1 = ' WHERE dealticket.DEAL_TYPE_ID=( select ENTITY_ID from ENTITY where  ENTITY_TYPE=268 AND
	ENTITY_NAME like ''fixed price'') AND dealTicket.HEDGE_TYPE_ID=( select ENTITY_ID
	from ENTITY where  ENTITY_TYPE=273 AND ENTITY_NAME like ''financial'') AND	
	dealTicket.HEDGE_MODE_TYPE_ID IN(select ENTITY_ID from	ENTITY where  ENTITY_TYPE=263 AND ENTITY_NAME IN( ''basis'')
	) AND dealTicket.RM_DEAL_TICKET_ID IN ( select RM_DEAL_TICKET_ID from	RM_DEAL_TICKET
	where ' + '''' + CONVERT(VARCHAR(12), @fromDate, 101) + ''''
        + ' BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH										
        UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where ' + ''''
        + CONVERT(VARCHAR(12), @toDate, 101) + ''''
        + ' BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH
        UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where HEDGE_START_MONTH BETWEEN  '
        + '''' + CONVERT(VARCHAR(12), @fromDate, 101) + '''' + ' AND ' + ''''
        + CONVERT(VARCHAR(12), @toDate, 101) + ''''
        + '
        UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where HEDGE_END_MONTH BETWEEN  '
        + '''' + CONVERT(VARCHAR(12), @fromDate, 101) + '''' + ' AND ' + ''''
        + CONVERT(VARCHAR(12), @toDate, 101) + ''''
        + '
	) AND dealTicket.RM_DEAL_TICKET_ID=dealstatus.RM_DEAL_TICKET_ID AND dealstatus.DEAL_TRANSACTION_STATUS_TYPE_ID IN( select  ENTITY_ID from    ENTITY
	where ENTITY_TYPE=286 AND ENTITY_NAME IN(''order executed'')) AND
	dealticket.CLIENT_ID=' + STR(@clientId)
        + '  AND dealticket.PRICE_INDEX_ID=priceIndex.PRICE_INDEX_ID AND
	dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID AND
	dealdetails.RM_DEAL_TICKET_DETAILS_ID=dealvolume.RM_DEAL_TICKET_DETAILS_ID AND
	dealdetails.MONTH_IDENTIFIER BETWEEN ' + ''''
        + CONVERT(VARCHAR(12), @fromDate, 101) + '''' + ' AND ' + ''''
        + CONVERT(VARCHAR(12), @toDate, 101) + ''''

    SET @selectQuery2 = ' SELECT distinct priceIndex.PRICE_INDEX_ID,priceIndex.PRICING_POINT,''Financial''  HEDGE_TYPE,dealTicket.RM_DEAL_TICKET_ID '	
    SET @fromQuery2 = ' FROM RM_DEAL_TICKET  dealticket,RM_DEAL_TICKET_DETAILS dealdetails,
	RM_DEAL_TICKET_VOLUME_DETAILS dealvolume,PRICE_INDEX priceIndex '
    SET @whereQuery2 = ' WHERE dealticket.PRICING_REQUEST_TYPE_ID IN( select ENTITY_ID from ENTITY where  ENTITY_TYPE=274 AND
	ENTITY_NAME IN(''weighted strip price'',''individual month pricing'')) AND dealticket.DEAL_TYPE_ID=( select ENTITY_ID
	from ENTITY where ENTITY_TYPE=268 AND ENTITY_NAME like ''trigger'') AND dealTicket.HEDGE_TYPE_ID=( select ENTITY_ID
	from ENTITY where  ENTITY_TYPE=273 AND ENTITY_NAME like ''financial'') 
	AND dealTicket.HEDGE_MODE_TYPE_ID IN(   select ENTITY_ID
	from ENTITY where  ENTITY_TYPE=263 AND ENTITY_NAME IN( ''basis'')) AND dealTicket.RM_DEAL_TICKET_ID IN ( select RM_DEAL_TICKET_ID 
 	from RM_DEAL_TICKET where ' + '''' + CONVERT(VARCHAR(12), @fromDate, 101)
        + ''''
        + ' BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH										
	UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where ' + ''''
        + CONVERT(VARCHAR(12), @toDate, 101) + ''''
        + ' BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH
        UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where HEDGE_START_MONTH BETWEEN  '
        + '''' + CONVERT(VARCHAR(12), @fromDate, 101) + '''' + ' AND ' + ''''
        + CONVERT(VARCHAR(12), @toDate, 101) + ''''
        + '
        UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where HEDGE_END_MONTH BETWEEN  '
        + '''' + CONVERT(VARCHAR(12), @fromDate, 101) + '''' + '  AND ' + ''''
        + CONVERT(VARCHAR(12), @toDate, 101) + ''''
        + '
	) AND dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID AND dealdetails.TRIGGER_STATUS_TYPE_ID IN( select  ENTITY_ID
	from ENTITY where ENTITY_TYPE=287 AND ENTITY_NAME IN(''fixed'',''closed'')) AND dealticket.CLIENT_ID='
        + STR(@clientId)
        + ' AND
	dealticket.PRICE_INDEX_ID=priceIndex.PRICE_INDEX_ID AND	dealdetails.RM_DEAL_TICKET_DETAILS_ID=dealvolume.RM_DEAL_TICKET_DETAILS_ID AND
	dealdetails.MONTH_IDENTIFIER BETWEEN ' + ''''
        + CONVERT(VARCHAR(12), @fromDate, 101) + '''' + ' AND ' + ''''
        + CONVERT(VARCHAR(12), @toDate, 101) + '''' + ' '

    SET @selectQuery3 = ' SELECT distinct priceIndex.PRICE_INDEX_ID,priceIndex.PRICING_POINT,''Financial''  HEDGE_TYPE,dealTicket.RM_DEAL_TICKET_ID '	
    SET @fromQuery3 = ' FROM RM_DEAL_TICKET  dealticket,RM_DEAL_TICKET_TRANSACTION_STATUS dealstatus,
	RM_DEAL_TICKET_DETAILS dealdetails,RM_DEAL_TICKET_VOLUME_DETAILS dealvolume,PRICE_INDEX priceIndex '
    SET @whereQuery3 = ' WHERE dealticket.DEAL_TYPE_ID=( select ENTITY_ID from ENTITY  where  ENTITY_TYPE=268 AND ENTITY_NAME like ''options'')
	AND dealTicket.HEDGE_TYPE_ID=( select ENTITY_ID from	  ENTITY where  ENTITY_TYPE=273 AND ENTITY_NAME like ''financial'')
	AND dealTicket.HEDGE_MODE_TYPE_ID IN(   select ENTITY_ID from	ENTITY where  ENTITY_TYPE=263 AND ENTITY_NAME IN( ''basis'')
	) AND dealTicket.RM_DEAL_TICKET_ID IN ( select RM_DEAL_TICKET_ID from	RM_DEAL_TICKET
	where ' + '''' + CONVERT(VARCHAR(12), @fromDate, 101) + ''''
        + ' BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH										
	UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where ' + ''''
        + CONVERT(VARCHAR(12), @toDate, 101) + ''''
        + ' BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH
	UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where HEDGE_START_MONTH BETWEEN  '
        + '''' + CONVERT(VARCHAR(12), @fromDate, 101) + '''' + '  AND ' + ''''
        + CONVERT(VARCHAR(12), @toDate, 101) + ''''
        + '
	UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where HEDGE_END_MONTH BETWEEN  '
        + '''' + CONVERT(VARCHAR(12), @fromDate, 101) + '''' + '  AND ' + ''''
        + CONVERT(VARCHAR(12), @toDate, 101) + ''''
        + '
	) AND dealTicket.RM_DEAL_TICKET_ID=dealstatus.RM_DEAL_TICKET_ID AND dealstatus.DEAL_TRANSACTION_STATUS_TYPE_ID IN( select  ENTITY_ID
	from ENTITY where ENTITY_TYPE=286 AND ENTITY_NAME IN(''order executed'')) AND
	dealticket.CLIENT_ID=' + STR(@clientId)
        + ' AND dealticket.PRICE_INDEX_ID=priceIndex.PRICE_INDEX_ID AND
	dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID AND
	dealdetails.RM_DEAL_TICKET_DETAILS_ID=dealvolume.RM_DEAL_TICKET_DETAILS_ID  AND
	dealdetails.MONTH_IDENTIFIER BETWEEN ' + ''''
        + CONVERT(VARCHAR(12), @fromDate, 101) + '''' + ' AND ' + ''''
        + CONVERT(VARCHAR(12), @toDate, 101) + '''' + ' '

    SET @selectQuery4 = ' SELECT distinct priceIndex.PRICE_INDEX_ID,priceIndex.PRICING_POINT,''Physical'' HEDGE_TYPE,dealTicket.RM_DEAL_TICKET_ID '				
    SET @fromQuery4 = ' FROM RM_DEAL_TICKET  dealticket,RM_DEAL_TICKET_TRANSACTION_STATUS dealstatus,
	RM_DEAL_TICKET_DETAILS dealdetails,RM_DEAL_TICKET_VOLUME_DETAILS dealvolume,PRICE_INDEX priceIndex '
    SET @whereQuery4 = ' WHERE dealticket.DEAL_TYPE_ID=( select ENTITY_ID from ENTITY where  ENTITY_TYPE=268 AND
		ENTITY_NAME like ''fixed price'') AND dealTicket.HEDGE_TYPE_ID=( select ENTITY_ID from  ENTITY
	where  ENTITY_TYPE=273 AND  ENTITY_NAME like ''Physical'')
	 AND dealTicket.HEDGE_MODE_TYPE_ID IN(   select ENTITY_ID
	from ENTITY  where  ENTITY_TYPE=263 AND	 ENTITY_NAME IN( ''basis'')) AND	dealTicket.RM_DEAL_TICKET_ID IN ( select RM_DEAL_TICKET_ID 
 	from RM_DEAL_TICKET where ' + '''' + CONVERT(VARCHAR(12), @fromDate, 101)
        + ''''
        + ' BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH										
        UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET  where ' + ''''
        + CONVERT(VARCHAR(12), @toDate, 101) + ''''
        + ' BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH
        UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET  where HEDGE_START_MONTH BETWEEN  '
        + '''' + CONVERT(VARCHAR(12), @fromDate, 101) + '''' + '  AND ' + ''''
        + CONVERT(VARCHAR(12), @toDate, 101) + ''''
        + '
        UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET  where HEDGE_END_MONTH BETWEEN  '
        + '''' + CONVERT(VARCHAR(12), @fromDate, 101) + '''' + '  AND ' + ''''
        + CONVERT(VARCHAR(12), @toDate, 101) + ''''
        + '
	) AND dealTicket.RM_DEAL_TICKET_ID=dealstatus.RM_DEAL_TICKET_ID AND dealstatus.DEAL_TRANSACTION_STATUS_TYPE_ID IN( select  ENTITY_ID
	from ENTITY where ENTITY_TYPE=286 AND ENTITY_NAME IN(''order executed'')) AND
	dealticket.CLIENT_ID=' + STR(@clientId)
        + '  AND dealticket.PRICE_INDEX_ID=priceIndex.PRICE_INDEX_ID AND
	dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID AND
	dealdetails.RM_DEAL_TICKET_DETAILS_ID=dealvolume.RM_DEAL_TICKET_DETAILS_ID  AND
	dealdetails.MONTH_IDENTIFIER BETWEEN ' + ''''
        + CONVERT(VARCHAR(12), @fromDate, 101) + '''' + ' AND ' + ''''
        + CONVERT(VARCHAR(12), @toDate, 101) + '''' + ' '

    SET @selectQuery5 = ' SELECT distinct priceIndex.PRICE_INDEX_ID,priceIndex.PRICING_POINT,''Physical'' HEDGE_TYPE,dealTicket.RM_DEAL_TICKET_ID '					
    SET @fromQuery5 = ' FROM RM_DEAL_TICKET  dealticket,RM_DEAL_TICKET_DETAILS dealdetails,
	RM_DEAL_TICKET_VOLUME_DETAILS dealvolume,PRICE_INDEX priceIndex '
    SET @whereQuery5 = ' WHERE dealticket.PRICING_REQUEST_TYPE_ID IN( select ENTITY_ID from   ENTITY
	where  ENTITY_TYPE=274 AND ENTITY_NAME IN(''weighted strip price'',''individual month pricing'')) AND	
	dealticket.DEAL_TYPE_ID=( select ENTITY_ID from	 ENTITY where  ENTITY_TYPE=268 AND ENTITY_NAME like ''trigger'') AND	
	dealTicket.HEDGE_TYPE_ID=( select ENTITY_ID from ENTITY where  ENTITY_TYPE=273 AND ENTITY_NAME like ''Physical'') AND
	dealTicket.HEDGE_MODE_TYPE_ID IN(select ENTITY_ID from ENTITY where  ENTITY_TYPE=263 AND ENTITY_NAME IN( ''basis'')) AND			
	dealTicket.RM_DEAL_TICKET_ID IN ( select RM_DEAL_TICKET_ID  from RM_DEAL_TICKET
	where ' + '''' + CONVERT(VARCHAR(12), @fromDate, 101) + ''''
        + ' BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH										
	UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where ' + ''''
        + CONVERT(VARCHAR(12), @toDate, 101) + ''''
        + ' BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH
        UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where HEDGE_START_MONTH BETWEEN  '
        + '''' + CONVERT(VARCHAR(12), @fromDate, 101) + '''' + '  AND ' + ''''
        + CONVERT(VARCHAR(12), @toDate, 101) + ''''
        + '
        UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where HEDGE_END_MONTH BETWEEN  '
        + '''' + CONVERT(VARCHAR(12), @fromDate, 101) + '''' + '  AND ' + ''''
        + CONVERT(VARCHAR(12), @toDate, 101) + ''''
        + '
	) AND dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID AND dealdetails.TRIGGER_STATUS_TYPE_ID IN( select  ENTITY_ID
	from ENTITY  where   ENTITY_TYPE=287 AND ENTITY_NAME IN(''fixed'',''closed'')) AND
	dealticket.CLIENT_ID=' + STR(@clientId)
        + '  AND dealticket.PRICE_INDEX_ID=priceIndex.PRICE_INDEX_ID AND	
	dealdetails.RM_DEAL_TICKET_DETAILS_ID=dealvolume.RM_DEAL_TICKET_DETAILS_ID  AND
	dealdetails.MONTH_IDENTIFIER BETWEEN ' + ''''
        + CONVERT(VARCHAR(12), @fromDate, 101) + '''' + ' AND ' + ''''
        + CONVERT(VARCHAR(12), @toDate, 101) + '''' + ' '

    SET @selectQuery6 = ' SELECT distinct priceIndex.PRICE_INDEX_ID,priceIndex.PRICING_POINT,''Physical'' HEDGE_TYPE,dealTicket.RM_DEAL_TICKET_ID '						
    SET @fromQuery6 = ' FROM RM_DEAL_TICKET  dealticket, RM_DEAL_TICKET_TRANSACTION_STATUS dealstatus,
	RM_DEAL_TICKET_DETAILS dealdetails,RM_DEAL_TICKET_VOLUME_DETAILS dealvolume,PRICE_INDEX priceIndex '
    SET @whereQuery6 = ' WHERE dealticket.DEAL_TYPE_ID=( select ENTITY_ID from ENTITY where  ENTITY_TYPE=268 AND
	 ENTITY_NAME like ''options'') AND dealTicket.HEDGE_TYPE_ID=( select ENTITY_ID from  ENTITY
	where  ENTITY_TYPE=273 AND ENTITY_NAME like ''Physical''	) 
	AND dealTicket.HEDGE_MODE_TYPE_ID IN(   select ENTITY_ID
	 from ENTITY  where  ENTITY_TYPE=263 AND ENTITY_NAME IN( ''basis'')) AND dealTicket.RM_DEAL_TICKET_ID IN ( select RM_DEAL_TICKET_ID 
 	 from RM_DEAL_TICKET  where ' + '''' + CONVERT(VARCHAR(12), @fromDate, 101)
        + ''''
        + ' BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH										
   	 UNION select RM_DEAL_TICKET_ID from	RM_DEAL_TICKET where ' + ''''
        + CONVERT(VARCHAR(12), @toDate, 101) + ''''
        + ' BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH
  	 UNION select RM_DEAL_TICKET_ID from	RM_DEAL_TICKET where HEDGE_START_MONTH BETWEEN  '
        + '''' + CONVERT(VARCHAR(12), @fromDate, 101) + '''' + '  AND ' + ''''
        + CONVERT(VARCHAR(12), @toDate, 101) + ''''
        + '
  	 UNION select RM_DEAL_TICKET_ID from	RM_DEAL_TICKET where HEDGE_END_MONTH BETWEEN  '
        + '''' + CONVERT(VARCHAR(12), @fromDate, 101) + '''' + '  AND ' + ''''
        + CONVERT(VARCHAR(12), @toDate, 101) + ''''
        + '
  	 ) AND dealTicket.RM_DEAL_TICKET_ID=dealstatus.RM_DEAL_TICKET_ID AND	dealstatus.DEAL_TRANSACTION_STATUS_TYPE_ID IN( select  ENTITY_ID
        from ENTITY where ENTITY_TYPE=286 AND ENTITY_NAME IN(''order executed'')) AND
	dealticket.CLIENT_ID=' + STR(@clientId)
        + '  AND dealticket.PRICE_INDEX_ID=priceIndex.PRICE_INDEX_ID AND
	dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID AND
	dealdetails.RM_DEAL_TICKET_DETAILS_ID=dealvolume.RM_DEAL_TICKET_DETAILS_ID  AND
	dealdetails.MONTH_IDENTIFIER BETWEEN ' + ''''
        + CONVERT(VARCHAR(12), @fromDate, 101) + '''' + ' AND ' + ''''
        + CONVERT(VARCHAR(12), @toDate, 101) + '''' + ' '

    IF @divisionId <> ''
        OR @divisionId <> NULL 
        BEGIN
            SELECT  @fromQuery1 = @fromQuery1
                    + ', DIVISION division, SITE site '
            SELECT  @fromQuery2 = @fromQuery2
                    + ', DIVISION division, SITE site '
            SELECT  @fromQuery3 = @fromQuery3
                    + ', DIVISION division, SITE site '
            SELECT  @fromQuery4 = @fromQuery4
                    + ', DIVISION division, SITE site '
            SELECT  @fromQuery5 = @fromQuery5
                    + ', DIVISION division, SITE site '
            SELECT  @fromQuery6 = @fromQuery6
                    + ', DIVISION division, SITE site '

            SELECT  @whereQuery1 = @whereQuery1
                    + ' AND dealvolume.site_id = site.site_id
	and site.division_id = division.division_id
	and division.division_id = ' + STR(@divisionId) + '
	and division.client_id = ' + STR(@clientId) + '  '

            SELECT  @whereQuery2 = @whereQuery2
                    + ' AND dealvolume.site_id = site.site_id
	and site.division_id = division.division_id
	and division.division_id = ' + STR(@divisionId) + '
	and division.client_id = ' + STR(@clientId) + '  '

            SELECT  @whereQuery3 = @whereQuery3
                    + ' AND dealvolume.site_id = site.site_id
	and site.division_id = division.division_id
	and division.division_id = ' + STR(@divisionId) + '
	and division.client_id = ' + STR(@clientId) + '  '

            SELECT  @whereQuery4 = @whereQuery4
                    + ' AND dealvolume.site_id = site.site_id
	and site.division_id = division.division_id
	and division.division_id = ' + STR(@divisionId) + '
	and division.client_id = ' + STR(@clientId) + '  '

            SELECT  @whereQuery5 = @whereQuery5
                    + ' AND dealvolume.site_id = site.site_id
	and site.division_id = division.division_id
	and division.division_id = ' + STR(@divisionId) + '
	and division.client_id = ' + STR(@clientId) + '  '

            SELECT  @whereQuery6 = @whereQuery6
                    + ' AND dealvolume.site_id = site.site_id
	and site.division_id = division.division_id
	and division.division_id = ' + STR(@divisionId) + '
	and division.client_id = ' + STR(@clientId) + '  '

        END


    IF @siteId <> ''
        OR @siteId <> NULL 
        BEGIN
            SELECT  @whereQuery1 = @whereQuery1
                    + ' AND dealvolume.SITE_ID IN( select DISTINCT hedge.SITE_ID from RM_ONBOARD_HEDGE_SETUP hedge
        where hedge.SITE_ID in (' + @siteId
                    + ') AND hedge.INCLUDE_IN_REPORTS=1 )  '

            SELECT  @whereQuery2 = @whereQuery2
                    + ' AND dealvolume.SITE_ID IN( select DISTINCT hedge.SITE_ID from RM_ONBOARD_HEDGE_SETUP hedge
	where hedge.SITE_ID in (' + @siteId
                    + ') AND hedge.INCLUDE_IN_REPORTS=1 )   '

            SELECT  @whereQuery3 = @whereQuery3
                    + ' AND
	dealvolume.SITE_ID IN( select DISTINCT hedge.SITE_ID from RM_ONBOARD_HEDGE_SETUP hedge
	where hedge.SITE_ID in (' + @siteId
                    + ') AND hedge.INCLUDE_IN_REPORTS=1 )  '

            SELECT  @whereQuery4 = @whereQuery4
                    + ' AND
	dealvolume.SITE_ID IN( select DISTINCT hedge.SITE_ID from RM_ONBOARD_HEDGE_SETUP hedge where hedge.SITE_ID in ('
                    + @siteId + ') AND
	hedge.INCLUDE_IN_REPORTS=1 )  '

            SELECT  @whereQuery5 = @whereQuery5
                    + ' AND
	dealvolume.SITE_ID IN( select DISTINCT hedge.SITE_ID from RM_ONBOARD_HEDGE_SETUP hedge
        where hedge.SITE_ID in (' + @siteId
                    + ') AND hedge.INCLUDE_IN_REPORTS=1 ) '

            SELECT  @whereQuery6 = @whereQuery6
                    + ' AND
	dealvolume.SITE_ID IN( select DISTINCT hedge.SITE_ID from RM_ONBOARD_HEDGE_SETUP hedge
        where hedge.SITE_ID in (' + @siteId
                    + ') AND hedge.INCLUDE_IN_REPORTS=1 )  '



        END

    IF @pricePointId <> ''
        OR @pricePointId <> NULL 
        BEGIN

            SELECT  @whereQuery1 = @whereQuery1
                    + ' AND dealticket.PRICE_INDEX_ID=' + STR(@pricePointId)
                    + ' ' 
	
            SELECT  @whereQuery2 = @whereQuery2
                    + ' AND dealticket.PRICE_INDEX_ID=' + STR(@pricePointId)
                    + ' ' 

            SELECT  @whereQuery3 = @whereQuery3
                    + ' AND dealticket.PRICE_INDEX_ID=' + STR(@pricePointId)
                    + ' ' 

            SELECT  @whereQuery4 = @whereQuery4
                    + ' AND dealticket.PRICE_INDEX_ID=' + STR(@pricePointId)
                    + ' ' 

            SELECT  @whereQuery5 = @whereQuery5
                    + ' AND dealticket.PRICE_INDEX_ID=' + STR(@pricePointId)
                    + ' ' 

            SELECT  @whereQuery6 = @whereQuery6
                    + ' AND dealticket.PRICE_INDEX_ID=' + STR(@pricePointId)
                    + ' '


        END


    SELECT  @finalQuery1 = @selectQuery1 + @fromQuery1 + @whereQuery1
		    
    SELECT  @finalQuery2 = @selectQuery2 + @fromQuery2 + @whereQuery2

    SELECT  @finalQuery3 = @selectQuery3 + @fromQuery3 + @whereQuery3

    SELECT  @finalQuery4 = @selectQuery4 + @fromQuery4 + @whereQuery4 

    SELECT  @finalQuery5 = @selectQuery5 + @fromQuery5 + @whereQuery5

    SELECT  @finalQuery6 = @selectQuery6 + @fromQuery6 + @whereQuery6
    
   --print ( @finalQuery1 + ' UNION ' + @finalQuery2 + ' UNION ' + @finalQuery3
   --        + ' UNION ' + @finalQuery4 + ' UNION ' + @finalQuery5 + ' UNION '
   --        + @finalQuery6 + ' order by dealTicket.RM_DEAL_TICKET_ID '
   --       )

    EXEC
        ( @finalQuery1 + ' UNION ' + @finalQuery2 + ' UNION ' + @finalQuery3
          + ' UNION ' + @finalQuery4 + ' UNION ' + @finalQuery5 + ' UNION '
          + @finalQuery6 + ' order by dealTicket.RM_DEAL_TICKET_ID '
        )
END    
GO
GRANT EXECUTE ON  [dbo].[GET_PRICING_POINT_DEAL_TICKET_FOR_BASIS_ONLY_MARK_TO_MARKET_REPORT_P] TO [CBMSApplication]
GO
