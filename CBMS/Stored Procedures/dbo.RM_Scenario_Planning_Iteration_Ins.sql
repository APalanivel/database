SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                 
/******                        
 NAME: dbo.RM_Scenario_Planning_Iteration_Ins            
                        
 DESCRIPTION:                        
			To Insert data into RM_Scenario_Planning_Iteration table.                        
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@RM_Scenario_Report_Id				INT
@Iteration_Num						INT
@Created_User_Id					INT
@RM_Scenario_Planning_Iteration_Id  INT						OUTPUT								              
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
BEGIN TRAN 
declare @RM_Scenario_Planning_Iteration_Id int =0

EXEC dbo.RM_Scenario_Planning_Iteration_Ins 
      @RM_Scenario_Report_Id = 1
           ,@Iteration_Num =2
           ,@Created_User_Id =49
      ,@RM_Scenario_Planning_Iteration_Id=@RM_Scenario_Planning_Iteration_Id OUTPUT

SELECT @RM_Scenario_Planning_Iteration_Id     
     
SELECT * FROM RM_Scenario_Planning_Iteration WHERE RM_Scenario_Report_Id=1

ROLLBACK
        
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2014-10-01       Created                
                       
******/                 

          
CREATE PROCEDURE [dbo].[RM_Scenario_Planning_Iteration_Ins]
      ( 
       @RM_Scenario_Report_Id INT
      ,@Iteration_Num INT
      ,@Created_User_Id INT
      ,@RM_Scenario_Planning_Iteration_Id INT OUTPUT )
AS 
BEGIN                
      SET NOCOUNT ON;     
      
      INSERT      INTO [dbo].[RM_Scenario_Planning_Iteration]
                  ( 
                   [RM_Scenario_Report_Id]
                  ,[Iteration_Num]
                  ,[Created_User_Id]
                  ,[Created_Ts]
                  ,[Updated_User_Id]
                  ,[Last_Change_Ts] )
      VALUES
                  ( 
                   @RM_Scenario_Report_Id
                  ,@Iteration_Num
                  ,@Created_User_Id
                  ,GETDATE()
                  ,@Created_User_Id
                  ,GETDATE() )
                  
      SET @RM_Scenario_Planning_Iteration_Id = SCOPE_IDENTITY()
                                                                   
                                 
                       
END 
;
GO
GRANT EXECUTE ON  [dbo].[RM_Scenario_Planning_Iteration_Ins] TO [CBMSApplication]
GO
