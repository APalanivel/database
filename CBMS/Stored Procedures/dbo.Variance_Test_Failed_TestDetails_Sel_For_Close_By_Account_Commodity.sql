SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/******  
NAME:  
 dbo.Variance_Test_Failed_TestDetails_Sel_For_Close_By_Account_Commodity  
   
  
DESCRIPTION:  
  
 Used to select  variance failed test details in review variance page for the given account, commodity and service month  
  
INPUT PARAMETERS:  
 Name			DataType	Default		Description  
------------------------------------------------------------  
 @Account_Id	INT  
 @Commodity_Id	INT			NULL  
 @Service_Month Date  
  
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
----------------------------------------------------------------  
	EXEC dbo.Variance_Test_Failed_TestDetails_Sel_For_Close_By_Account_Commodity 502616,NULL, '2012-12-01'  
	EXEC dbo.Variance_Test_Failed_TestDetails_Sel_For_Close_By_Account_Commodity 6961,NULL, '2006-01-01'  
	EXEC dbo.Variance_Test_Failed_TestDetails_Sel_For_Close_By_Account_Commodity 350009,NULL, '2009-12-01'  

  EXEC dbo.Variance_Test_Failed_TestDetails_Sel_For_Close_By_Account_Commodity 618093,291, '10/1/2013'
  EXEC dbo.Variance_Test_Failed_TestDetails_Sel_For_Close_By_Account_Commodity 1035985,291, '11/1/2014'

 SELECT * FROM Variance_Log where (Closed_Dt IS NULL) AND (Is_Failure = 1)  
  
  
 
  
AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 HG			Hari  
 AKR		Ashok Kumar Raju
 RR			Raghu Reddy
 SP			Sandeep Pigilam
 AP			Arunkumar Palanivel
MODIFICATIONS  
  
 Initials	Date		Modification  
------------------------------------------------------------  
 HG			03/17/2010	Created  
 HG			04/08/2010	CTE created on Variance_Log and Self Join with Variance_Log table is removed as results can derived directly.  
 HG			04/16/2010	Default value (NULL) set for @Commodity_id parameter, so that this procedure can be used to get all the variance entries logged for the given account and service month.  
 HG			05/06/2010	Order By claused modified to sort the records based on Variance_Log_id  
 AKR		06/13/2012	Removed Cost_Usage_Id from the select list
 RR			2012-06-14	Usage examples corrected
 AKR        2013-07-16  Modified for Detailed Variance, Added UOM_Id and UOM_Name	
 SP			2014-04-28  Data Operations Enhancements,Added Baseline_Cd,Baseline_Desc,Baseline_Service_Month in Select List.
 SP			2014-07=16	Data Operations Enhancement Phase III,Added Baseline_Account_Id in Select List.
 SP			2016-11-02	For Variance Test Enhancement Phase II,Added table variable @Contracts.
 AP			May 27,2020	Added view details hyperlink for variance project phase 2						
******/

CREATE PROCEDURE [dbo].[Variance_Test_Failed_TestDetails_Sel_For_Close_By_Account_Commodity]
      (
      @Account_Id    INT
    , @Commodity_Id  INT = NULL
    , @Service_Month DATE )
AS
      BEGIN

            SET NOCOUNT ON;

            DECLARE @Contracts TABLE
                  ( Contract_Id INT NULL );

            INSERT INTO @Contracts (
                                         Contract_Id
                                   )
                        SELECT
                              cmv.CONTRACT_ID
                        FROM  Core.Client_Hier_Account cha
                              INNER JOIN
                              dbo.CONTRACT_METER_VOLUME cmv
                                    ON cha.Meter_Id = cmv.METER_ID
                                       AND cha.Supplier_Contract_ID = cmv.CONTRACT_ID
                        WHERE cha.Account_Type = 'supplier'
                              AND   cha.Account_Id = @Account_Id
                              AND   cmv.MONTH_IDENTIFIER = @Service_Month
                              AND   cha.Commodity_Id = @Commodity_Id;


            SELECT
                        vlog.Variance_Log_Id
                      , vlog.Variance_Status_Cd
                      , vlog.Variance_Status_Desc AS variance_status_type
                      , vlog.Category_Name
                      , isnull(vlog.Current_Value, 0) AS Current_Value
                      , vlog.Test_Description
                      , isnull(vlog.Baseline_Value, 0) AS Baseline_Value
                      , isnull(vlog.Low_Tolerance, 0) AS Low_Tolerance
                      , isnull(vlog.High_Tolerance, 0) AS High_Tolerance
                      , vlog.Is_Failure
                      , vlog.Closed_Dt
                      , vlog.Execution_Dt
                      , vlog.Is_Inclusive
                      , vlog.Is_Multiple_Condition
                      , Ref_Variance_Log_Id = isnull(vlog.Ref_Variance_Log_Id, vlog.Variance_Log_Id)
                      , vlog.UOM_Cd UOM_Id
                      , vlog.UOM_Label UOM_Name
                      , vlog.Baseline_Cd
                      , vlog.Baseline_Desc
                      , vlog.Baseline_Service_Month
                      , vlog.Baseline_Account_Id
                      , c.Contract_Id
                      , CASE WHEN (     (     SELECT
                                                      count(1)
                                              FROM    dbo.Variance_Historical_Data_Point tvp
                                              WHERE   tvp.Variance_Log_Id = vlog.Variance_Log_Id ) > 4 ) -- this is temporary fix. 
                                   THEN 1
                             ELSE  0
                        END AS view_details
            FROM        dbo.Variance_Log vlog
                        OUTER APPLY @Contracts c
            WHERE       ( vlog.Account_ID = @Account_Id )
                        AND
                              (     @Commodity_Id IS NULL
                                    OR    vlog.Commodity_ID = @Commodity_Id )
                        AND   ( vlog.Service_Month = @Service_Month )
                        AND   ( vlog.Closed_Dt IS NULL )
                        AND   ( vlog.Is_Failure = 1 )
            GROUP BY    vlog.Variance_Log_Id
                      , vlog.Variance_Status_Cd
                      , vlog.Variance_Status_Desc
                      , vlog.Category_Name
                      , isnull(vlog.Current_Value, 0)
                      , vlog.Test_Description
                      , isnull(vlog.Baseline_Value, 0)
                      , isnull(vlog.Low_Tolerance, 0)
                      , isnull(vlog.High_Tolerance, 0)
                      , vlog.Is_Failure
                      , vlog.Closed_Dt
                      , vlog.Execution_Dt
                      , vlog.Is_Inclusive
                      , vlog.Is_Multiple_Condition
                      , vlog.Ref_Variance_Log_Id
                      , vlog.Commodity_ID
                      , vlog.UOM_Cd
                      , vlog.UOM_Label
                      , vlog.Baseline_Cd
                      , vlog.Baseline_Desc
                      , vlog.Baseline_Service_Month
                      , vlog.Baseline_Account_Id
                      , c.Contract_Id
            ORDER BY    vlog.Variance_Log_Id;

      END;


      ;
GO





GRANT EXECUTE ON  [dbo].[Variance_Test_Failed_TestDetails_Sel_For_Close_By_Account_Commodity] TO [CBMSApplication]
GO
