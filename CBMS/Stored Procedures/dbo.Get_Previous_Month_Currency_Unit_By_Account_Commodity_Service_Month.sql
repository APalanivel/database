SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME: dbo.Get_Previous_Month_Currency_Unit_By_Account_Commodity_Service_Month

DESCRIPTION: 
	To get the Previous received Currency_unitid for the given account, commodity and service month

INPUT PARAMETERS:
NAME					DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
@Account_Id				INT
@Site_Client_Hier_Id	INT
@Commodity_Id			INT
@Service_Month			DATE

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Get_Previous_Month_Currency_Unit_By_Account_Commodity_Service_Month 7518,1130, 291,'2005-05-01' 


	SELECT TOP 10 * FROM dbo.Cost_Usage_Account_Dtl 	
	
	SELECT * FROM dbo.Bucket_Master where bucket_master_id = 110

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
HG			Harihara Suthan G

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
HG			2012-04-23	Created for additional data enhancement

*/

CREATE PROCEDURE dbo.Get_Previous_Month_Currency_Unit_By_Account_Commodity_Service_Month
      ( 
       @Account_Id INT
      ,@Site_Client_Hier_Id INT
      ,@Commodity_Id INT
      ,@Service_Month DATE )
AS 
BEGIN

      SET NOCOUNT ON ;

      DECLARE @Previous_Received_Month DATETIME

	-- Get the service month which is less than given service month
      SELECT
            @Previous_Received_Month = max(cuad.Service_Month)
      FROM
            dbo.Cost_Usage_Account_Dtl cuad
            JOIN dbo.Bucket_Master bm
                  ON bm.Bucket_Master_Id = cuad.Bucket_Master_Id
      WHERE
            cuad.Account_id = @Account_Id
            AND cuad.Client_Hier_Id = @Site_Client_Hier_Id
            AND cuad.Service_Month < @service_month
            AND bm.Commodity_Id = @Commodity_Id


	-- Get the service month which is greater than given service month if previous month is not available
      SELECT
            @Previous_Received_Month = min(service_month)
      FROM
            dbo.Cost_Usage_Account_Dtl cuad
            JOIN dbo.Bucket_Master bm
                  ON bm.Bucket_Master_Id = cuad.Bucket_Master_Id
      WHERE
            cuad.Account_id = @Account_Id
            AND cuad.Client_Hier_Id = @Site_Client_Hier_Id
            AND cuad.Service_Month > @service_month
            AND @Previous_Received_Month IS NOT NULL


      SELECT
            cuad.currency_unit_id
      FROM
            dbo.Cost_Usage_Account_Dtl cuad
            JOIN dbo.Bucket_Master bm
                  ON bm.Bucket_Master_Id = cuad.Bucket_Master_Id
      WHERE
            cuad.Account_id = @Account_Id
            AND cuad.Client_Hier_Id = @Site_Client_Hier_Id
            AND cuad.service_month = @Previous_Received_Month
            AND bm.Commodity_Id = @Commodity_id
      GROUP BY
            cuad.currency_unit_id
END
;
GO
GRANT EXECUTE ON  [dbo].[Get_Previous_Month_Currency_Unit_By_Account_Commodity_Service_Month] TO [CBMSApplication]
GO
