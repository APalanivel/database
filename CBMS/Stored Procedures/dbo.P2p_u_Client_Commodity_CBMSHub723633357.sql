SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[P2p_u_Client_Commodity_CBMSHub723633357]
		@c1 int = NULL,
		@c2 int = NULL,
		@c3 int = NULL,
		@c4 int = NULL,
		@c5 int = NULL,
		@c6 int = NULL,
		@c7 int = NULL,
		@c8 int = NULL,
		@c9 bit = NULL,
		@c10 datetime = NULL,
		@c11 int = NULL,
		@c12 int = NULL,
		@c13 datetime = NULL,
		@c14 int = NULL,
		@c15 bit = NULL,
		@pkc1 int = NULL,
		@bitmap binary(2),
		@MSp2pPreVersion varbinary(32) , @MSp2pPostVersion varbinary(32) 

as
begin  
update [Core].[Client_Commodity] set
		[Commodity_Id] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [Commodity_Id] end,
		[Client_Id] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [Client_Id] end,
		[Scope_Cd] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [Scope_Cd] end,
		[UOM_Cd] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [UOM_Cd] end,
		[Frequency_Cd] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [Frequency_Cd] end,
		[Hier_Level_Cd] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [Hier_Level_Cd] end,
		[Commodity_Service_Cd] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [Commodity_Service_Cd] end,
		[Allow_for_DE] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [Allow_for_DE] end,
		[Last_Change_Ts] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [Last_Change_Ts] end,
		[Owner_User_Id] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [Owner_User_Id] end,
		[DE_Config_Last_Change_By_User_Id] = case substring(@bitmap,2,1) & 8 when 8 then @c12 else [DE_Config_Last_Change_By_User_Id] end,
		[DE_Config_Last_Change_Ts] = case substring(@bitmap,2,1) & 16 when 16 then @c13 else [DE_Config_Last_Change_Ts] end,
		[DE_Config_Uom_Cd] = case substring(@bitmap,2,1) & 32 when 32 then @c14 else [DE_Config_Uom_Cd] end,
		[Allow_Utility_Map] = case substring(@bitmap,2,1) & 64 when 64 then @c15 else [Allow_Utility_Map] end		,$sys_p2p_cd_id = @MSp2pPostVersion

where [Client_Commodity_Id] = @pkc1
		and ($sys_p2p_cd_id = @MSp2pPreVersion or $sys_p2p_cd_id is null)
if @@rowcount = 0
begin  
	declare @cur_version varbinary(32) 
		,@conflict_type int = 1
		,@conflict_type_txt nvarchar(20) = N'Update-Update'
		,@reason_code int = 1
		,@reason_text nvarchar(720) = NULL
		,@is_on_disk_winner bit = 0
		,@is_incoming_winner bit = 0
		,@peer_id_current_node int = NULL
		,@peer_id_incoming int
		,@tranid_incoming nvarchar(40)
		,@peer_id_on_disk int
		,@tranid_on_disk nvarchar(40)
	select @peer_id_incoming = sys.fn_replvarbintoint(@MSp2pPostVersion)
		,@tranid_incoming = sys.fn_replp2pversiontotranid(@MSp2pPostVersion)
	select @cur_version = $sys_p2p_cd_id from [Core].[Client_Commodity] 
where [Client_Commodity_Id] = @pkc1
	if @@rowcount = 0  
			select @conflict_type = 3, @conflict_type_txt = N'Update-Delete', @is_on_disk_winner = 1, @reason_text = formatmessage(22823), @reason_code = 0
	else 
	begin  
		select @peer_id_on_disk = sys.fn_replvarbintoint(@cur_version)
			,@tranid_on_disk = sys.fn_replp2pversiontotranid(@cur_version)
		if(@peer_id_incoming > @peer_id_on_disk)
			set @is_incoming_winner = 1
		else
			set @is_on_disk_winner = 1
	end  
		select @peer_id_current_node = p.originator_id from syspublications p join sysarticles a on a.pubid = p.pubid where a.objid = object_id(N'[Core].[Client_Commodity]') and p.options & 0x1 = 0x1
	if (@peer_id_current_node is not null) 
	begin 
		if (@reason_text is NULL)
			if (@peer_id_incoming > @peer_id_on_disk)
			begin  
				select @reason_text = formatmessage(22822,@peer_id_incoming,@peer_id_on_disk,@peer_id_current_node)
			end  
			else  
			begin  
				select @reason_text = formatmessage(22821,@peer_id_incoming,@peer_id_on_disk,@peer_id_current_node)
			end  
		create table #change_id (change_id varbinary(8))
		insert [dbo].[conflict_Core_Client_Commodity] (
		[Client_Commodity_Id],
		[Commodity_Id],
		[Client_Id],
		[Scope_Cd],
		[UOM_Cd],
		[Frequency_Cd],
		[Hier_Level_Cd],
		[Commodity_Service_Cd],
		[Allow_for_DE],
		[Last_Change_Ts],
		[Owner_User_Id],
		[DE_Config_Last_Change_By_User_Id],
		[DE_Config_Last_Change_Ts],
		[DE_Config_Uom_Cd],
		[Allow_Utility_Map]
			,__$originator_id
			,__$origin_datasource
			,__$tranid
			,__$conflict_type
			,__$is_winner
			,__$reason_code
			,__$reason_text
			,__$update_bitmap
			,__$pre_version)
		output inserted.__$row_id into #change_id
		select 
    @pkc1,
    @c2,
    @c3,
    @c4,
    @c5,
    @c6,
    @c7,
    @c8,
    @c9,
    @c10,
    @c11,
    @c12,
    @c13,
    @c14,
    @c15			,@peer_id_current_node
			,@peer_id_incoming
			,@tranid_incoming
			,@conflict_type
			,@is_incoming_winner
			,@reason_code
			,@reason_text
			,@bitmap
			,@MSp2pPreVersion
		insert [dbo].[conflict_Core_Client_Commodity] (

		[Client_Commodity_Id],
		[Commodity_Id],
		[Client_Id],
		[Scope_Cd],
		[UOM_Cd],
		[Frequency_Cd],
		[Hier_Level_Cd],
		[Row_Version],
		[Commodity_Service_Cd],
		[Allow_for_DE],
		[Last_Change_Ts],
		[Owner_User_Id],
		[DE_Config_Last_Change_By_User_Id],
		[DE_Config_Last_Change_Ts],
		[DE_Config_Uom_Cd],
		[Allow_Utility_Map]			,__$originator_id
			,__$origin_datasource
			,__$tranid
			,__$conflict_type
			,__$is_winner
			,__$reason_code
			,__$reason_text
			,__$pre_version
			,__$change_id)
		select 

		[Client_Commodity_Id],
		[Commodity_Id],
		[Client_Id],
		[Scope_Cd],
		[UOM_Cd],
		[Frequency_Cd],
		[Hier_Level_Cd],
		[Row_Version],
		[Commodity_Service_Cd],
		[Allow_for_DE],
		[Last_Change_Ts],
		[Owner_User_Id],
		[DE_Config_Last_Change_By_User_Id],
		[DE_Config_Last_Change_Ts],
		[DE_Config_Uom_Cd],
		[Allow_Utility_Map]			,@peer_id_current_node
			,@peer_id_on_disk
			,@tranid_on_disk
			,@conflict_type
			,@is_on_disk_winner
			,@reason_code
			,@reason_text
			,NULL
			,(select change_id from #change_id)
		from [Core].[Client_Commodity] 

where [Client_Commodity_Id] = @pkc1
	end 
	if(@peer_id_incoming > @peer_id_on_disk)
	begin  
update [Core].[Client_Commodity] set
		[Commodity_Id] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [Commodity_Id] end,
		[Client_Id] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [Client_Id] end,
		[Scope_Cd] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [Scope_Cd] end,
		[UOM_Cd] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [UOM_Cd] end,
		[Frequency_Cd] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [Frequency_Cd] end,
		[Hier_Level_Cd] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [Hier_Level_Cd] end,
		[Commodity_Service_Cd] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [Commodity_Service_Cd] end,
		[Allow_for_DE] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [Allow_for_DE] end,
		[Last_Change_Ts] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [Last_Change_Ts] end,
		[Owner_User_Id] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [Owner_User_Id] end,
		[DE_Config_Last_Change_By_User_Id] = case substring(@bitmap,2,1) & 8 when 8 then @c12 else [DE_Config_Last_Change_By_User_Id] end,
		[DE_Config_Last_Change_Ts] = case substring(@bitmap,2,1) & 16 when 16 then @c13 else [DE_Config_Last_Change_Ts] end,
		[DE_Config_Uom_Cd] = case substring(@bitmap,2,1) & 32 when 32 then @c14 else [DE_Config_Uom_Cd] end,
		[Allow_Utility_Map] = case substring(@bitmap,2,1) & 64 when 64 then @c15 else [Allow_Utility_Map] end		,$sys_p2p_cd_id = @MSp2pPostVersion

where [Client_Commodity_Id] = @pkc1
	end  
		if exists(select * from syspublications p join sysarticles a on a.pubid = p.pubid where a.objid = object_id(N'[Core].[Client_Commodity]') and p.options & 0x10 = 0x10)
			raiserror(22815, 10, -1, @conflict_type_txt, @peer_id_current_node, @peer_id_incoming, @tranid_incoming, @peer_id_on_disk, @tranid_on_disk) with log
		else
			raiserror(22815, 16, -1, @conflict_type_txt, @peer_id_current_node, @peer_id_incoming, @tranid_incoming, @peer_id_on_disk, @tranid_on_disk) with log
end 
end 
GO
