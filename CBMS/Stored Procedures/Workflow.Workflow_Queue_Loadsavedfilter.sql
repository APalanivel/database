SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


  
/******              
NAME:    [Workflow].[Workflow_Queue_Loadsavedfilter]           
DESCRIPTION:   The Stored Procedure will return the Saved Filter information         
------------------------------------------------------------             
 INPUT PARAMETERS:              
 Name   DataType  Default Description           
 Workflow_Queue_Saved_Filter_Query_Id INT,            
 @Module_Id INT             
------------------------------------------------------------              
 OUTPUT PARAMETERS:              
 Name   DataType  Default Description              
 EXEC Workflow.Workflow_Queue_Loadsavedfilter  147,1                      
------------------------------------------------------------              
 USAGE EXAMPLES:           
 EXEC [Workflow].[Workflow_Queue_Loadsavedfilter]                           
 Workflow_Queue_Saved_Filter_Query_Id =147,              
 @Module_Id =1              
------------------------------------------------------------              
AUTHOR INITIALS:              
Initials Name              
------------------------------------------------------------              
TRK   Ramakrishna Summit Energy           
           
 MODIFICATIONS               
 Initials Date   Modification              
------------------------------------------------------------              
 TRK   SEP-2019 Created        
 CPK Oct-2019 Add code for & - d20-1483    
 CPK Nov-2019 Added search filter split by ~ d20-1466
******/          


CREATE PROCEDURE [Workflow].[Workflow_Queue_Loadsavedfilter]                             
 @Saved_Filter_Id INT,                
 @Module_Id INT                         
AS                              
BEGIN                              
     DECLARE @Workflow_sub_queue_id INT           
  SELECT    @Workflow_sub_queue_id=Workflow_Sub_Queue_Id FROM Workflow.Workflow_Sub_Queue           
  WHERE  Workflow_Sub_Queue_Name='Saved Filters'           
  AND Workflow_Queue_Id= @Module_Id          
                          
  SELECT                                
   sf.Workflow_Queue_Saved_Filter_Query_Id AS Id,                              
   sf.Saved_Filter_Name AS Custom_Filter_Name,                 
   sf.Created_User_Id,                              
   replace(isnull(SFV.SearchFilterValue,''),'&amp;','&') SearchFilterValue,                           
   replace(isnull(idSFV.SearchFilterIdValue,''),'&amp;','&') SearchFilterIdValue,                             
   sf.Workflow_sub_queue_id AS SubQId ,          
   sf.Expiration_Dt  AS Expires_On,          
   cast (CASE WHEN sf.Workflow_sub_queue_id=@Workflow_sub_queue_id THEN 1 ELSE 0 END AS BIT) AS Is_SavedFilter          
              
  INTO #tempData                            
  FROM                               
 Workflow.Workflow_queue_saved_filter_query AS sf                              
  CROSS APPLY                              
  (                              
 SELECT  Filter_Name+'~'+Selected_Value+ '#'+c.Code_Dsc+'##'                
 FROM Workflow.Workflow_queue_saved_filter_query_value sv,                    
   Workflow.workflow_queue_search_filter qsf,                
   Workflow.search_filter st ,                
   dbo.Code c                       
 WHERE  sv.Workflow_Queue_Search_Filter_Id = qsf.Workflow_Queue_Search_Filter_Id                   
 AND qsf.Search_Filter_Id = st.Search_Filter_Id                
 AND qsf.Filter_Type_Cd = c.code_id    
 AND sv.Workflow_Queue_Saved_Filter_Query_Id = sf.Workflow_Queue_Saved_Filter_Query_Id                 
    FOR XML PATH('')                               
  ) AS SFV(SearchFilterValue)                        
  CROSS APPLY                              
  (                               
 SELECT  Filter_Name+'~'+Selected_KeyValue+ '#'+c.Code_Dsc+'##'                       
 FROM Workflow.Workflow_queue_saved_filter_query_value sv,                    
   Workflow.workflow_queue_search_filter qsf,                
   Workflow.search_filter st,                
   dbo.Code c                
 WHERE  sv.Workflow_Queue_Search_Filter_Id = qsf.Workflow_Queue_Search_Filter_Id             
 AND qsf.Search_Filter_Id = st.Search_Filter_Id                        
 AND qsf.Filter_Type_Cd = c.code_id                
 AND sv.Workflow_Queue_Saved_Filter_Query_Id = sf.Workflow_Queue_Saved_Filter_Query_Id                
   FOR XML PATH('')                              
  ) AS idSFV(SearchFilterIdValue)                                
  WHERE sf.Workflow_Queue_Saved_Filter_Query_Id = @Saved_Filter_Id                
           
  SELECT t.*,sq.Workflow_Queue_Id AS Module_Id  FROM #tempData t, Workflow.Workflow_sub_queue AS sq                
  WHERE t.SubQId = sq.Workflow_Sub_Queue_Id                
  AND sq.Workflow_Queue_Id =  @Module_Id                  
                             
  IF object_id('tempdb..#tempData') IS NOT NULL DROP TABLE #tempData                         
END 



GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_Loadsavedfilter] TO [CBMSApplication]
GO
