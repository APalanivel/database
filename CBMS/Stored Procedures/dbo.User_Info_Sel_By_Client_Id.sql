SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 /******      
                         
 NAME: [dbo].[User_Info_Sel_By_Client_Id]                       
                          
 DESCRIPTION:      
     TO get the users from user info  table.                          
                          
 INPUT PARAMETERS:      
                         
 Name                   DataType        Default       Description      
---------------------------------------------------------------------------------------------------------------    
 @Client_Id             INT        
                          
 OUTPUT PARAMETERS:      
                               
 Name                   DataType        Default       Description      
---------------------------------------------------------------------------------------------------------------    
                          
 USAGE EXAMPLES:                              
---------------------------------------------------------------------------------------------------------------   
     
 Exec  dbo.User_Info_Sel_By_Client_Id 110          
               
                         
 AUTHOR INITIALS:    
       
 Initials               Name      
---------------------------------------------------------------------------------------------------------------    
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
---------------------------------------------------------------------------------------------------------------    
 NR                     2013-12-18      Created for RA Admin user management                        
                         
******/                          
                        
 CREATE PROCEDURE [dbo].[User_Info_Sel_By_Client_Id] ( @Client_Id INT )
 AS 
 BEGIN  
   
      SET NOCOUNT ON;    
    
      SELECT
            u.USER_INFO_ID
           ,u.FIRST_NAME + ' ' + U.LAST_NAME AS UserName
           ,u.EMAIL_ADDRESS
      FROM
            dbo.USER_INFO u
      WHERE
            u.CLIENT_ID = @Client_Id
            AND u.IS_HISTORY = 0
            AND u.ACCESS_LEVEL = 1  
      ORDER BY  UserName      
             
 END    

 
;

;
GO
GRANT EXECUTE ON  [dbo].[User_Info_Sel_By_Client_Id] TO [CBMSApplication]
GO
