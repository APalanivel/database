SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********   
NAME:  dbo.Variance_Log_UPD  
 
DESCRIPTION:  Used to update variance_log table from Review Variance Page  

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
@Variance_Log_Id			Int	
@Variance_Status_Id			Int
@User_Info_Id				Int  
@Is_Closed					Int
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:   

	dbo.Variance_Log_UPD 8672466,100160, 49, 1
	
	
------------------------------------------------------------  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
NK   Nageswara Rao Kosuri         


Initials Date  Modification  
------------------------------------------------------------  
NK	10/13/2009  Created


******/  


CREATE PROCEDURE dbo.Variance_Log_UPD	    
	@Variance_Log_Id			Int,		
	@Variance_Status_Id			Int,
	@User_Info_Id				Int,
	@Is_Closed					Int
								
AS
BEGIN
	
	SET NOCOUNT ON;	
	
			Declare @closedStatusId int
			SELECT @closedStatusId = cd.Code_Id FROM CODE cd
										JOIN CODESET cset ON cset.Codeset_Id  = cd.Codeset_Id
										WHERE Codeset_Name =  'VarianceStatus'
										AND cd.Code_Value = 'Closed'
			IF 	@Is_Closed = 1  
			BEGIN						
				UPDATE VARIANCE_LOG
		 
				SET Closed_Dt = getDate(),
					Closed_By_User_Info_ID = @User_Info_Id,
					Closed_By_User_Name = (Select FIRST_NAME + ' ' + LAST_NAME
											From User_Info
											Where User_Info_Id = @User_Info_Id),
					Variance_Status_Cd = @closedStatusId,
					Variance_Status_Desc = (Select Code_dsc
											From Code
											Where Code_Id = @closedStatusId)
																			
				
				WHERE Variance_Log_Id =  @Variance_Log_Id
			END
			ELSE
			BEGIN
				UPDATE VARIANCE_LOG
				
				SET Variance_Status_Cd = @Variance_Status_Id,
					Variance_Status_Desc = (Select Code_dsc
											From Code
											Where Code_Id = @Variance_Status_Id)
																			
				
				WHERE Variance_Log_Id =  @Variance_Log_Id
			END	
				
			
END	

GO
GRANT EXECUTE ON  [dbo].[Variance_Log_UPD] TO [CBMSApplication]
GO
