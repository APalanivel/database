SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:           
   dbo.Site_Rule_Filter_Sel          
DESCRIPTION:             
    To get the site rule filter given by the client.        
           
 INPUT PARAMETERS:            
 Name            DataType            Default            Description            
--------------------------------------------------------------------------------              
 @Client_Id      INT          
           
 OUTPUT PARAMETERS:            
 Name            DataType            Default            Description            
--------------------------------------------------------------------------------              
            
 USAGE EXAMPLES:            
--------------------------------------------------------------------------------              
          
 EXEC dbo.RM_Group_Site_Rule_Filter_Sel           
 
 
           
 AUTHOR INITIALS:            
 Initials    Name            
--------------------------------------------------------------------------------              
 NR			 Narayana Reddy
           
 MODIFICATIONS             
 Initials    Date			Modification            
--------------------------------------------------------------------------------              
 NR				25-07-2018		Created For Risk managemnet.		
       
******/

CREATE PROCEDURE [dbo].[RM_Group_Site_Rule_Filter_Sel]
AS
    BEGIN


        SET NOCOUNT ON;

        SELECT
            Cbms_Rule_Filter_Id 
            , Filter_Name
            , Lookup_Sp_Name
            , 'StringOperators' AS Operator_Cd
        FROM
            dbo.Cbms_Rule_Filter
        ORDER BY
            Cbms_Rule_Filter_Id;



    END;



    ;
GO
GRANT EXECUTE ON  [dbo].[RM_Group_Site_Rule_Filter_Sel] TO [CBMSApplication]
GO
