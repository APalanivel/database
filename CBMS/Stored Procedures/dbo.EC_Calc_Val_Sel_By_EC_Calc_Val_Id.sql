SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 /******                    
Name:   dbo.EC_Calc_Val_Sel_By_EC_Calc_Val_Id             
                    
Description:                    
  This sproc to get the  details for a Given id.            
                                 
 Input Parameters:                    
    Name    DataType   Default    Description                      
----------------------------------------------------------------------------------------                      
 @EC_Calc_Val_Id     INT      
          
 Output Parameters:                          
    Name    DataType   Default    Description                      
----------------------------------------------------------------------------------------                      
                    
 Usage Examples:                        
----------------------------------------------------------------------------------------         
      
   Exec dbo.EC_Calc_Val_Sel_By_EC_Calc_Val_Id 26      
         
   Exec dbo.EC_Calc_Val_Sel_By_EC_Calc_Val_Id  29       
            
       
Author Initials:                    
    Initials  Name                    
----------------------------------------------------------------------------------------                      
 NR    Narayana Reddy    
 RKV             Ravi Kumar Vegesna    
                     
 Modifications:                    
    Initials        Date  Modification                    
----------------------------------------------------------------------------------------                      
    NR    2015-05-16  Created For AS400.    
    RKV             2015-10-16  Added two new joins of bucket and sub_buckets as part of AS400-PII     
    RKV    2016-11-18 Added Ec_Account_Group_Type_Id in the result set as part of MAINT-4563     
    NR    2018-02-23 Recalc Data Interval - IDM_Commodity_Measurement_Group_Id  added in Output List.             
                   
******/       
    
CREATE  PROCEDURE [dbo].[EC_Calc_Val_Sel_By_EC_Calc_Val_Id] ( @EC_Calc_Val_Id INT )    
AS     
BEGIN      
      SET NOCOUNT ON       
                  
      SELECT    
            ecv.EC_Calc_Val_Id    
           ,ecv.Commodity_Id    
           ,ecv.Calc_Value_Name    
           ,ecvbm.Bucket_Master_Id    
           ,[EC_Invoice_Sub_Bucket_Master_Id] = LEFT(cvsb.cvsb_list, LEN(cvsb.cvsb_list) - 1)    
           ,ecv.Starting_Period_Cd    
           ,ecv.Starting_Period_Operator_Cd    
           ,ecv.Starting_Period_Operand    
           ,ecv.End_Period_Cd    
           ,ecv.End_Period_Operator_Cd    
           ,ecv.End_Period_Operand    
           ,ecv.Aggregation_Cd    
           ,s.COUNTRY_ID    
           ,s.STATE_ID    
           ,s.STATE_NAME    
           ,ecv.Ec_Account_Group_Type_Id    
           ,ecv.IDM_Commodity_Measurement_Group_Id 
		   ,ecv.Monthly_settings_Cd 
		   ,ecv.Monthly_Setting_Start_Month_Num 	
           ,ecv.Monthly_Setting_End_Month_Num 
		   ,ecv.Multiple_Option_Value_Selection_Cd 
		   ,ecv.Multiple_Option_No_Of_Month 
		   ,ecv.Multiple_Option_Total_Aggregation_Cd 
		   ,ecv.Calc_Val_Type_Cd  
		   ,ecv.Uom_Cd  
		   ,ecv.Supplier_Account_Source_Type_Cd 
         
      FROM    
            dbo.EC_Calc_Val ecv    
            Left JOIN dbo.Ec_Calc_Val_Bucket_Map ecvbm    
                  ON ecv.EC_Calc_Val_Id = ecvbm.Ec_Calc_Val_Id    
            INNER JOIN dbo.STATE s    
                  ON s.STATE_ID = ecv.State_Id    
            CROSS APPLY ( SELECT    
                              CAST(ecvbsbm.EC_Invoice_Sub_Bucket_Master_Id AS VARCHAR) + ','    
                          FROM    
                              dbo.Ec_Calc_Val_Bucket_Sub_Bucket_Map ecvbsbm    
                          WHERE    
                              ecvbsbm.Ec_Calc_Val_Bucket_Map_Id = ecvbm.Ec_Calc_Val_Bucket_Map_Id    
                          GROUP BY    
                              ecvbsbm.EC_Invoice_Sub_Bucket_Master_Id    
            FOR    
                          XML PATH('') ) cvsb ( cvsb_list )    
      WHERE    
            ecv.EC_Calc_Val_Id = @EC_Calc_Val_Id      
                      
END;    
;    
;    
    
GO
GRANT EXECUTE ON  [dbo].[EC_Calc_Val_Sel_By_EC_Calc_Val_Id] TO [CBMSApplication]
GO
