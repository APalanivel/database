SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/**

NAME: dbo.RM_Client_Contact_Is_Exists

    
DESCRIPTION:    

      To check duplicate client contact


INPUT PARAMETERS:    
     NAME					DATATYPE		DEFAULT           DESCRIPTION    
------------------------------------------------------------------------    
	@Client_Id				INT		

                    

OUTPUT PARAMETERS:              
      NAME              DATATYPE    DEFAULT           DESCRIPTION       
-------------------------------------------------------------------------              


USAGE EXAMPLES:              
-------------------------------------------------------------------------              
	SELECT ccm.*,ci.First_Name,ci.Last_Name,ci.Email_Address FROM dbo.Client_Contact_Map ccm 
	INNER JOIN dbo.Contact_Info ci ON ccm.Contact_Info_Id = ci.Contact_Info_Id
    INNER JOIN dbo.Code cd ON ci.Contact_Type_Cd = cd.Code_Id
    INNER JOIN dbo.Codeset cs ON cd.Codeset_Id = cs.Codeset_Id
    WHERE cs.Codeset_Name = 'ContactType' AND cd.Code_Value = 'RM Client Contact' AND ccm.CLIENT_ID = 235
    
	EXEC dbo.RM_Client_Contact_Is_Exists @Client_Id=235,@First_Name='Raghu',@Last_Name='Reddy',@Email_Address='rvintha@ctepl.com'
	EXEC dbo.RM_Client_Contact_Is_Exists @Client_Id=10003,@First_Name='Raghu',@Last_Name='Raghu',@Email_Address='rvintha@ctepl.com'

	 

AUTHOR INITIALS:              
	INITIALS	NAME              
------------------------------------------------------------              
    RR			Raghu Reddy


MODIFICATIONS:    
	INITIALS	DATE		MODIFICATION              
------------------------------------------------------------              
	RR 			31-07-2018	Created - Global Risk Management

**/ 
CREATE PROCEDURE [dbo].[RM_Client_Contact_Is_Exists]
      ( 
       @Client_Id INT = NULL
      ,@First_Name NVARCHAR(60)
      ,@Last_Name NVARCHAR(60)
      ,@Email_Address NVARCHAR(150)
      ,@Contact_Info_Id INT = NULL
      ,@Deal_Ticket_Id INT = NULL )
AS 
BEGIN
	
      SET NOCOUNT ON;   
      
      SELECT
            @Client_Id = Client_Id
      FROM
            Trade.Deal_Ticket
      WHERE
            Deal_Ticket_Id = @Deal_Ticket_Id
            AND @Deal_Ticket_Id IS NOT NULL
      
      DECLARE @RM_Client_Contact_Is_Exists BIT = 0

      SELECT
            @RM_Client_Contact_Is_Exists = 1
      FROM
            dbo.Client_Contact_Map ccm
            INNER JOIN dbo.Contact_Info ci
                  ON ccm.Contact_Info_Id = ci.Contact_Info_Id
            INNER JOIN dbo.Code cd
                  ON ci.Contact_Type_Cd = cd.Code_Id
            INNER JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'ContactType'
            AND cd.Code_Value = 'RM Client Contact'
            AND ccm.CLIENT_ID = @Client_Id
            AND ci.First_Name = @First_Name
            AND ci.Last_Name = @Last_Name
            AND ci.Email_Address = @Email_Address
            AND ( @Contact_Info_Id IS NULL
                  OR ccm.Contact_Info_Id <> @Contact_Info_Id )
                  
      SELECT
            @RM_Client_Contact_Is_Exists AS RM_Client_Contact_Is_Exists
     
				

END;

GO
GRANT EXECUTE ON  [dbo].[RM_Client_Contact_Is_Exists] TO [CBMSApplication]
GO
