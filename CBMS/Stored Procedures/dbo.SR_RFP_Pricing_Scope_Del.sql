SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[SR_RFP_Pricing_Scope_Del]  
     
DESCRIPTION: 
	It Deletes SR RFP Pricing Scope for Selected 
						SR RFP Pricing Scope Id.
      
INPUT PARAMETERS:          
NAME							DATATYPE	DEFAULT		DESCRIPTION          
--------------------------------------------------------------------
@SR_RFP_Pricing_Scope_Id		INT	

   
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------
  
	Begin Tran
		EXEC dbo.SR_RFP_Pricing_Scope_Del  462
	Rollback Tran
    
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR		PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR		    31-MAY-10	CREATED     

*/  

CREATE PROCEDURE dbo.SR_RFP_Pricing_Scope_Del
   (
    @SR_RFP_Pricing_Scope_Id INT
   )
AS
BEGIN

    SET NOCOUNT ON;

    DELETE
   	FROM
		dbo.SR_RFP_PRICING_SCOPE
	WHERE
		SR_RFP_PRICING_SCOPE_ID = @SR_RFP_Pricing_Scope_Id

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_Pricing_Scope_Del] TO [CBMSApplication]
GO
