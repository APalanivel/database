SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.Client_Comment_Del

DESCRIPTION:

INPUT PARAMETERS:
	Name					DataType		Default	Description
---------------------------------------------------------------
	@Client_Id				INT
    @Comment_Id				INT

OUTPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	
USAGE EXAMPLES:
------------------------------------------------------------


BEGIN TRAN
select ccm.* from Client_Comment_Map ccm where ccm.client_id=11803 and ccm.comment_id='1587193' 

EXEC dbo.Client_Comment_Del 
      @Client_Id = 11803
     ,@Comment_Id = 1587193   

select ccm.* from Client_Comment_Map ccm where ccm.client_id=11803 and ccm.comment_id='1587193' 
ROLLBACK TRAN


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	BCH			Balaraju
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	 BCH       	2012-08-30	Created
******/
CREATE PROCEDURE dbo.Client_Comment_Del
      ( 
       @Client_Id INT
      ,@Comment_Id INT )
AS 
BEGIN
      SET NOCOUNT ON; 
 
      DELETE FROM
            dbo.Client_Comment_Map
      WHERE
            Client_Id = @Client_Id
            AND Comment_Id = @Comment_Id    
END
;
GO
GRANT EXECUTE ON  [dbo].[Client_Comment_Del] TO [CBMSApplication]
GO
