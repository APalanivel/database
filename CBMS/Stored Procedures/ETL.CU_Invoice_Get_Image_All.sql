SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
NAME:   etl.CU_Invoice_Get_Image_All                     
              
                  
DESCRIPTION:               
 Gets all image data from CU-Invoice where there is a valid service month              
               
 used specifically for ETL processes                       
                               
INPUT PARAMETERS:                        
Name   DataType Default  Description                        
------------------------------------------------------------                        
              
                              
OUTPUT PARAMETERS:                        
Name   DataType Default  Description                        
------------------------------------------------------------               
              
                       
USAGE EXAMPLES:                        
------------------------------------------------------------               
              
                     
                   
AUTHOR INITIALS:                        
Initials Name                        
------------------------------------------------------------                        
CMH   Chad Hattabaugh                
AKR   Ashok Kumar Raju            
DMR   Deana Ritter          
NM Nagaraju Muppa                   
                   
MODIFICATIONS                         
Initials Date Modification                        
------------------------------------------------------------                        
CMH  12/29/2010 Created              
AKR  2012-08-29 Modified to include Is_Reported Flag            
DMR  12/8/2014 Removed Transaction Isolation Level statements.  Causing transaction logs to fill up          
NM 06-04-2020 MAINT-10003 Added CU_INVOICE_CHARGE,CU_INVOICE_CHARGE_ACCOUNT and CU_INVOICE_DETERMINANT,CU_INVOICE_DETERMINANT_ACCOUNT tables to get commodity Id from determinant & charge table instead of CHA table             
NM 20-05-2020 MAINT-10332 added case statement to eliminate the invoice has ACCOUNT_ID ,Service_Month and Commodity_Id NUll. 

*****/
CREATE PROCEDURE [ETL].[CU_Invoice_Get_Image_All]
AS
BEGIN

	SET NOCOUNT ON;

	IF 1 = 0
	BEGIN
		SELECT
			CAST(NULL AS INT) AS Client_Hier_Id
			, CAST(NULL AS INT) AS Commodity_Id
			, CAST(NULL AS INT) AS ACCOUNT_ID
			, CAST(NULL AS INT) AS Cu_Invoice_id
			, CAST(NULL AS DATE) AS Service_Month
			, CAST(NULL AS INT) AS CBMS_IMAGE_ID
			, CAST(NULL AS BIT) AS IS_REPORTED
			, CAST(NULL AS CHAR(1)) AS Sys_CHANGE_OPERATION
			, CAST(NULL AS VARCHAR(24)) AS DATA_Source;

	END;


	CREATE TABLE #Cu_Invoice_Image_All
	(
		Client_Hier_Id		   INT
		, Commodity_Id		   INT
		, ACCOUNT_ID		   INT
		, Cu_Invoice_id		   INT
		, Service_Month		   DATE
		, CBMS_IMAGE_ID		   INT
		, IS_REPORTED		   BIT
		, Sys_CHANGE_OPERATION CHAR(1)
		, DATA_Source		   VARCHAR(100)
	);

	INSERT INTO #Cu_Invoice_Image_All
		(
			Client_Hier_Id
			, Commodity_Id
			, ACCOUNT_ID
			, Cu_Invoice_id
			, Service_Month
			, CBMS_IMAGE_ID
			, IS_REPORTED
			, Sys_CHANGE_OPERATION
			, DATA_Source
		)
	SELECT
		cha.Client_Hier_Id
		, CIC.COMMODITY_TYPE_ID AS Commodity_Id
		, ism.Account_ID
		, ci.CU_INVOICE_ID
		, CONVERT(DATE, ism.SERVICE_MONTH) AS Service_Month
		, ci.CBMS_IMAGE_ID
		, ci.IS_REPORTED
		, 'i' AS Sys_CHANGE_OPERATION
		, 'CU_Invoice_Get_Image_All' AS DATA_Source
	FROM
		CU_INVOICE ci
		INNER JOIN CU_INVOICE_SERVICE_MONTH ism
			ON ci.CU_INVOICE_ID = ism.CU_INVOICE_ID
		INNER JOIN Core.Client_Hier_Account cha
			ON ism.Account_ID = cha.Account_Id
		/**** MAINT-10003 change added CU_INVOICE_CHARGE and charge_Account tables table */
		INNER JOIN(dbo.CU_INVOICE_CHARGE CIC
				   INNER JOIN dbo.CU_INVOICE_CHARGE_ACCOUNT CICA
					   ON CICA.CU_INVOICE_CHARGE_ID = CICA.CU_INVOICE_CHARGE_ID)
			ON CIC.CU_INVOICE_ID = ci.CU_INVOICE_ID
			   AND CICA.ACCOUNT_ID = cha.Account_Id
			   AND CIC.COMMODITY_TYPE_ID = cha.Commodity_Id
	GROUP BY
		cha.Client_Hier_Id
		, CIC.COMMODITY_TYPE_ID
		, ism.Account_ID
		, ci.CU_INVOICE_ID
		, CONVERT(DATE, ism.SERVICE_MONTH)
		, ci.CBMS_IMAGE_ID
		, ci.IS_REPORTED;

	INSERT INTO #Cu_Invoice_Image_All
		(
			Client_Hier_Id
			, Commodity_Id
			, ACCOUNT_ID
			, Cu_Invoice_id
			, Service_Month
			, CBMS_IMAGE_ID
			, IS_REPORTED
			, Sys_CHANGE_OPERATION
			, DATA_Source
		)
	SELECT
		cha.Client_Hier_Id
		, CID.COMMODITY_TYPE_ID AS Commodity_Id
		, ism.Account_ID
		, ci.CU_INVOICE_ID
		, CONVERT(DATE, ism.SERVICE_MONTH) AS Service_Month
		, ci.CBMS_IMAGE_ID
		, ci.IS_REPORTED
		, 'i' AS Sys_CHANGE_OPERATION
		, 'CU_Invoice_Get_Image_All' AS DATA_Source
	FROM
		CU_INVOICE ci
		INNER JOIN CU_INVOICE_SERVICE_MONTH ism
			ON ci.CU_INVOICE_ID = ism.CU_INVOICE_ID
		INNER JOIN Core.Client_Hier_Account cha
			ON ism.Account_ID = cha.Account_Id
		INNER JOIN(dbo.CU_INVOICE_DETERMINANT CID
				   INNER JOIN dbo.CU_INVOICE_DETERMINANT_ACCOUNT CIDA
					   ON CIDA.CU_INVOICE_DETERMINANT_ID = CID.CU_INVOICE_DETERMINANT_ID)
			ON CID.CU_INVOICE_ID = ci.CU_INVOICE_ID
			   AND CIDA.ACCOUNT_ID = cha.Account_Id
			   AND CID.COMMODITY_TYPE_ID = cha.Commodity_Id
	WHERE
		NOT EXISTS (
					   SELECT
						   1
					   FROM
						   #Cu_Invoice_Image_All ciic
					   WHERE
						   ciic.Client_Hier_Id = cha.Client_Hier_Id
						   AND ciic.ACCOUNT_ID = ism.Account_ID
						   AND ciic.Commodity_Id = CID.COMMODITY_TYPE_ID
						   AND ciic.Cu_Invoice_id = ci.CU_INVOICE_ID
						   AND ciic.Service_Month = CONVERT(DATE, ism.SERVICE_MONTH)
				   )
	GROUP BY
		cha.Client_Hier_Id
		, CID.COMMODITY_TYPE_ID
		, ism.Account_ID
		, ci.CU_INVOICE_ID
		, CONVERT(DATE, ism.SERVICE_MONTH)
		, ci.CBMS_IMAGE_ID
		, ci.IS_REPORTED;


	SELECT
		ciia.Client_Hier_Id
		, ciia.Commodity_Id
		, ciia.ACCOUNT_ID
		, ciia.Cu_Invoice_id
		, ciia.Service_Month
		, ciia.CBMS_IMAGE_ID
		, CASE WHEN ciia.ACCOUNT_ID IS NULL OR ciia.Service_Month IS NULL OR ciia.Commodity_Id IS NULL THEN
				   0
			  ELSE
				  ciia.IS_REPORTED
		  END AS Is_Reported
		, ciia.Sys_CHANGE_OPERATION
		, ciia.DATA_Source
	FROM
		#Cu_Invoice_Image_All ciia;

	DROP TABLE #Cu_Invoice_Image_All;

END;

GO


GRANT EXECUTE ON  [ETL].[CU_Invoice_Get_Image_All] TO [CBMSApplication]
GRANT EXECUTE ON  [ETL].[CU_Invoice_Get_Image_All] TO [ETL_Execute]
GO
