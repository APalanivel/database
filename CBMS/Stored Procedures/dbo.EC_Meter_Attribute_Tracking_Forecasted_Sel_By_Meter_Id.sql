SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******                                    
Name:   dbo.EC_Meter_Attribute_Tracking_Forecasted_Sel_By_Meter_Id                             
                                    
Description:                                    
   This sproc get the attribute tracking  details for the given meter id.                             
                                    
 Input Parameters:                                    
    Name        DataType   Default   Description                                      
----------------------------------------------------------------------------------------                                      
    @Meter_Id       INT                      
                          
 Output Parameters:                                          
    Name        DataType   Default   Description                                      
----------------------------------------------------------------------------------------                                      
                                    
 Usage Examples:                                        
----------------------------------------------------------------------------------------                         
                      
   Exec dbo.EC_Meter_Attribute_Tracking_Forecasted_Sel_By_Meter_Id 804129                      
                         
   Exec dbo.EC_Meter_Attribute_Tracking_Forecasted_Sel_By_Meter_Id 442418                 
                 
   Exec dbo.EC_Meter_Attribute_Tracking_Forecasted_Sel_By_Meter_Id 1303808                    
                         
                       
Author Initials:                                    
    Initials  Name                                    
----------------------------------------------------------------------------------------                                      
 PRV    Pramod Reddy     V                                
 Modifications:                                    
    Initials        Date   Modification                                    
----------------------------------------------------------------------------------------                                      
    PRV    2019-11-28  Created For Budget 2.0             
                                   
******/  
CREATE PROCEDURE [dbo].[EC_Meter_Attribute_Tracking_Forecasted_Sel_By_Meter_Id]  
    (  
        @Meter_Id INT  
    )  
AS  
    BEGIN  
        SET NOCOUNT ON;  
  
        SELECT  
            emat.EC_Meter_Attribute_Tracking_Id  
            , ema.EC_Meter_Attribute_Id  
            , ema.EC_Meter_Attribute_Name  
            , emat.EC_Meter_Attribute_Value  
            , emat.Meter_Id  
            , c.Code_Value AS Attribute_Type  
            , emat.Start_Dt  
            , emat.End_Dt  
            , ema.Vendor_Type_Cd  
            , vend.Code_Value Vendor_Type_Value  
            , emat.Meter_Attribute_Type_Cd  
            , cd.Code_Value Attribute_Type_Name  
        FROM  
            dbo.EC_Meter_Attribute ema  
            INNER JOIN dbo.EC_Meter_Attribute_Tracking emat  
                ON ema.EC_Meter_Attribute_Id = emat.EC_Meter_Attribute_Id  
            INNER JOIN dbo.Code c  
                ON c.Code_Id = ema.Attribute_Type_Cd  
            LEFT JOIN dbo.Code cd  
                ON cd.Code_Id = emat.Meter_Attribute_Type_Cd  
            LEFT JOIN dbo.Codeset cs  
                ON cd.Codeset_Id = cs.Codeset_Id  
            LEFT JOIN dbo.Code vend  
                ON vend.Code_Id = ema.Vendor_Type_Cd  
        WHERE  
            cs.Codeset_Name = 'MeterAttributeType'  
            AND cd.Code_Value = 'Forecasted'  
            AND emat.Meter_Id = @Meter_Id  
        GROUP BY  
            emat.EC_Meter_Attribute_Tracking_Id  
            , ema.EC_Meter_Attribute_Id  
            , ema.EC_Meter_Attribute_Name  
            , emat.EC_Meter_Attribute_Value  
            , emat.Meter_Id  
            , c.Code_Value  
            , emat.Start_Dt  
            , emat.End_Dt  
            , ema.Vendor_Type_Cd  
            , vend.Code_Value  
            , emat.Meter_Attribute_Type_Cd  
           , cd.Code_Value;  
    END;

	
GO
GRANT EXECUTE ON  [dbo].[EC_Meter_Attribute_Tracking_Forecasted_Sel_By_Meter_Id] TO [CBMSApplication]
GO
