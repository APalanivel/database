
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	dbo.SR_RFP_UPDATE_SHORTLIST_DATA_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name				DataType          Default     Description    
---------------------------------------------------------------------------------    
	@accountGroupId		int
	@isBidGroup			int
	@dueDate			datetime
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRANSACTION
		DECLARE @Shortlist_ID INT
		SELECT * FROM dbo.SR_RFP_SOP_SHORTLIST a INNER JOIN dbo.Time_Zone b ON a.Time_Zone_Id = b.Time_Zone_Id
			WHERE SR_ACCOUNT_GROUP_ID = 10000695 AND IS_BID_GROUP = 1
		EXEC dbo.SR_RFP_UPDATE_SHORTLIST_DATA_P 10000695,1,'2016-03-21 12:00:00.000',566, '2016-03-21 02:30:00.000'
		SELECT * FROM dbo.SR_RFP_SOP_SHORTLIST a INNER JOIN dbo.Time_Zone b ON a.Time_Zone_Id = b.Time_Zone_Id
			WHERE SR_ACCOUNT_GROUP_ID = 10000695 AND IS_BID_GROUP = 1
	ROLLBACK TRANSACTION



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DR			Deana Ritter
	RR			Raghu Reddy

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	DR			08/04/2009	Removed Linked Server Updates
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2016-03-21	Global Sourcing - Phase3 - GCS-546 Added new input parameter @Time_Zone_Id	

******/
CREATE PROCEDURE [dbo].[SR_RFP_UPDATE_SHORTLIST_DATA_P]
      ( 
       @accountGroupId INT
      ,@isBidGroup INT
      ,@dueDate DATETIME
      ,@Time_Zone_Id INT = NULL
      ,@Revised_Due_Dt_By_Timezone DATETIME )
AS 
BEGIN
      SET NOCOUNT ON
	
      DECLARE @accountTermId INT
      DECLARE @selectedProductId INT
      DECLARE @supplierContactVendorMapId INT

      UPDATE
            dbo.SR_RFP_SOP_SHORTLIST
      SET   
            REVISED_DUE_DATE = @dueDate
           ,Time_Zone_Id = @Time_Zone_Id
           ,Revised_Due_Dt_By_Timezone = @Revised_Due_Dt_By_Timezone
      WHERE
            SR_ACCOUNT_GROUP_ID = @accountGroupId
            AND IS_BID_GROUP = @isBidGroup


/*
Added by Shailesh for Archiving Price Comments
*/


      DECLARE @error AS INT 
      DECLARE get_price_comments_details CURSOR
      FOR
      SELECT
            dbo.SR_RFP_SOP_SHORTLIST_DETAILS.SR_RFP_ACCOUNT_TERM_ID
           ,dbo.SR_RFP_SOP_SHORTLIST_DETAILS.SR_RFP_SELECTED_PRODUCTS_ID
           ,dbo.SR_RFP_SOP_SHORTLIST_DETAILS.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
      FROM
            dbo.SR_RFP_SOP_SHORTLIST
            INNER JOIN dbo.SR_RFP_SOP_SHORTLIST_DETAILS
                  ON dbo.SR_RFP_SOP_SHORTLIST.SR_RFP_SOP_SHORTLIST_ID = dbo.SR_RFP_SOP_SHORTLIST_DETAILS.SR_RFP_SOP_SHORTLIST_ID
      WHERE
            ( dbo.SR_RFP_SOP_SHORTLIST.SR_ACCOUNT_GROUP_ID = @accountGroupId )
            AND ( dbo.SR_RFP_SOP_SHORTLIST.IS_BID_GROUP = @isBidGroup )

      OPEN get_price_comments_details
 
      FETCH NEXT FROM get_price_comments_details
INTO @accountTermId, @selectedProductId, @supplierContactVendorMapId



      WHILE @@fetch_status = 0 
            BEGIN
	
	
                  IF @@error <> 0 
                        BEGIN
                              SET @error = 1
                        END

	
                  EXEC SR_RFP_ARCHIVE_PRICE_COMMENTS_FOR_SHORTLIST_P 
                        @accountTermId
                       ,@selectedProductId
                       ,@supplierContactVendorMapId	

                  IF @@error <> 0 
                        BEGIN
                              SET @error = 1
                        END
	
                  SET nocount OFF
	

                  FETCH NEXT FROM get_price_comments_details
INTO @accountTermId, @selectedProductId, @supplierContactVendorMapId

            END
      CLOSE get_price_comments_details
      DEALLOCATE get_price_comments_details

	
/*
Added by Shailesh ends for Archiving Price Comments 
*/
END
;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_UPDATE_SHORTLIST_DATA_P] TO [CBMSApplication]
GO
