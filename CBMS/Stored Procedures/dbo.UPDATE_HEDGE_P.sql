SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.UPDATE_HEDGE_P
	@hedge_unit_type_id int,
	@hedge_name varchar(200),
	@deal_type_id int,
	@hedge_transaction_date datetime,
	@transaction_by_type_id int,
	@hedge_comments varchar(4000),
	@contract_id INT,
	@hedge_id int
AS
BEGIN

	SET NOCOUNT ON

	UPDATE dbo.hedge
	SET	hedge_unit_type_id=@hedge_unit_type_id,
		hedge_name=@hedge_name,
		deal_type_id=@deal_type_id,
		hedge_transaction_date=@hedge_transaction_date,
		transaction_by_type_id=@transaction_by_type_id,
		hedge_comments=@hedge_comments,
		contract_id=@contract_id 
	WHERE hedge_id=@hedge_id

END
GO
GRANT EXECUTE ON  [dbo].[UPDATE_HEDGE_P] TO [CBMSApplication]
GO
