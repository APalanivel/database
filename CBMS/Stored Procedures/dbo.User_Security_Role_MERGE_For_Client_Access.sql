SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********  
NAME: dbo.User_Security_Role_MERGE_For_Client_Access   

DESCRIPTION:     
Refreshes Client's corporate security role to the user using Edit Client Access Page.
	
INPUT PARAMETERS:    
Name		           DataType        Default         Description    
----------------------------------------------------------------    
@Client_Id              dbo.Client_Ids
@User_Info_Id           INT
@Portfolio_Client_Id	INT


OUTPUT PARAMETERS:    
Name   DataType  Default Description    
------------------------------------------------------------    

USAGE EXAMPLES:    
------------------------------------------------------------    
SELECT * FROM dbo.User_Security_Role WHERE User_Info_Id = 8062
BEGIN TRAN

--Add two new client's secuirty roles
DECLARE @Client_Ids Client_Ids
INSERT @Client_Ids VALUES (102), (109),(110)
EXEC User_Security_Role_MERGE_For_Client_Access @Client_Ids, 8062

SELECT
      SR.Client_Id,USR.*
FROM
      dbo.User_Security_Role USR
      JOIN dbo.Security_Role SR
            ON USR.Security_Role_Id = SR.Security_Role_Id
WHERE
      User_Info_Id = 8062
      AND SR.Is_Corporate = 1

--Pass one client's securityrole which is already there and another one which is not there.
--The list that we pass should be there in the table
DECLARE @Client_Ids1 Client_Ids
INSERT @Client_Ids1 VALUES (102), (109),(111)
EXEC User_Security_Role_MERGE_For_Client_Access @Client_Ids1, 8062

SELECT
      SR.Client_Id,USR.*
FROM
      dbo.User_Security_Role USR
      JOIN dbo.Security_Role SR
            ON USR.Security_Role_Id = SR.Security_Role_Id
WHERE
      User_Info_Id = 8062
      AND SR.Is_Corporate = 1


-- Now dont pass anything. All securityroles should be deleted
DECLARE @Client_Ids2 Client_Ids
EXEC User_Security_Role_MERGE_For_Client_Access @Client_Ids2, 8062

SELECT
      SR.Client_Id,USR.*
FROM
      dbo.User_Security_Role USR
      JOIN dbo.Security_Role SR
            ON USR.Security_Role_Id = SR.Security_Role_Id
WHERE
      User_Info_Id = 8062
      AND SR.Is_Corporate = 1

ROLLBACK

AUTHOR INITIALS:    
Initials	Name    
------------------------------------------------------------    
RK          Rajesh Kasoju
 
MODIFICATIONS     
Initials	       Date		       Modification    
------------------------------------------------------------    
 RK                14/03/2012      Created    
    
******/

CREATE PROCEDURE [dbo].[User_Security_Role_MERGE_For_Client_Access]
      ( 
       @Client_Ids dbo.Client_Ids READONLY
      ,@User_Info_Id INT
      ,@Portfolio_Client_Id INT )
AS 
BEGIN

      SET NOCOUNT ON ;    
     
      BEGIN TRY
            BEGIN TRANSACTION  
      
            INSERT      dbo.User_Security_Role
                        ( 
                         Security_Role_Id
                        ,User_Info_Id
                        ,Last_Change_Ts )
                        SELECT
                              SR.Security_Role_Id
                             ,@User_Info_Id
                             ,getdate()
                        FROM
                              dbo.Security_Role SR
                              JOIN @Client_Ids CI
                                    ON CI.Client_Id = SR.Client_Id
                        WHERE
                              SR.Is_Corporate = 1
                              AND NOT EXISTS ( SELECT
                                                1
                                               FROM
                                                dbo.User_Security_Role
                                               WHERE
                                                USER_INFO_ID = @User_Info_Id
                                                AND Security_Role_Id = SR.Security_Role_Id )     
         
            DELETE
                  USR
            FROM
                  dbo.USER_Security_Role USR
                  JOIN dbo.Security_Role SR
                        ON USR.Security_Role_Id = SR.Security_Role_Id
            WHERE
                  USR.USER_INFO_ID = @User_Info_Id
                  AND SR.CLIENT_ID <> @Portfolio_Client_Id
                  AND NOT EXISTS ( SELECT
                                    1
                                   FROM
                                    @Client_Ids
                                   WHERE
                                    Client_Id = SR.Client_Id )
            
            COMMIT
      END TRY

      BEGIN CATCH
            IF @@TRANCOUNT > 0 
                  ROLLBACK
                  
            EXECUTE dbo.usp_RethrowError 
      END CATCH      
END        
GO
GRANT EXECUTE ON  [dbo].[User_Security_Role_MERGE_For_Client_Access] TO [CBMSApplication]
GO
