SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
                             
 NAME: dbo.Invoice_Cnt_Sel_By_Account_list_Start_End_Dt                         
                              
 DESCRIPTION:          
 Meter overlap check for missing contracts                    
                              
 INPUT PARAMETERS:          
                             
 Name                               DataType          Default       Description          
-------------------------------------------------------------------------------------      
 @Contract_Id      INT        
     
     
                            
 OUTPUT PARAMETERS:          
                                   
 Name                               DataType          Default       Description          
-------------------------------------------------------------------------------------                                
 USAGE EXAMPLES:                                  
-------------------------------------------------------------------------------------                     
  use cbms  
EXEC dbo.Invoice_Cnt_Sel_By_Account_list_Start_End_Dt
   @Account_List = '1921199'
    , @Start_Dt = '2020-04-01'
    , @End_Dt = '2020-07-31'
	

	Select * from CU_INVOICE_SERVICE_MONTH where account_id = 1921199
     
	 
	 EXEC dbo.Invoice_Cnt_Sel_By_Account_list_Start_End_Dt
   @Account_List = '1921199'
    , @Start_Dt = '2020-01-01'
    , @End_Dt = '2020-07-31'         
              
 AUTHOR INITIALS:        
           
 Initials                   Name          
-------------------------------------------------------------------------------------      
 NR                     Narayana Reddy                                
                               
 MODIFICATIONS:        
                               
 Initials               Date            Modification        
-------------------------------------------------------------------------------------      
 NR                     2020-05-05      MAINT- Extended Contract. 
                           
                             
******/

CREATE PROC [dbo].[Invoice_Cnt_Sel_By_Account_list_Start_End_Dt]
    (
        @Account_List NVARCHAR(MAX)
        , @Start_Dt DATE
        , @End_Dt DATE
    )
AS
    BEGIN
        SET NOCOUNT ON;





        SELECT
            us.Segments AS Account_ID
            , COUNT(DISTINCT cism.CU_INVOICE_ID) AS Inv_Cnt
        FROM
            dbo.ufn_split(@Account_List, ',') us
            LEFT JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.Account_ID = us.Segments
                   AND  cism.SERVICE_MONTH BETWEEN @Start_Dt
                                           AND     ISNULL(@End_Dt, '9999-12-31')
        GROUP BY
            us.Segments;




    END;

GO
GRANT EXECUTE ON  [dbo].[Invoice_Cnt_Sel_By_Account_list_Start_End_Dt] TO [CBMSApplication]
GO
