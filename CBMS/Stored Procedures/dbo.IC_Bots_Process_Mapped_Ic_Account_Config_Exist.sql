SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
                           
 NAME:  dbo.IC_Bots_Process_Mapped_Ic_Account_Config_Exist                     
                            
 DESCRIPTION:        
				TO get the Bot  information for IC Data .                            
                            
 INPUT PARAMETERS:        
                           
 Name                                   DataType        Default       Description        
------------------------------------------------------------------------------------   
@@IC_Bots_Process_Id	INT
		                          
 OUTPUT PARAMETERS:        
                                 
 Name                                   DataType        Default       Description        
------------------------------------------------------------------------------------   
                            
 USAGE EXAMPLES:                                
------------------------------------------------------------------------------------   
 

 Exec dbo.IC_Bots_Process_Mapped_Ic_Account_Config_Exist 1   
 
                           
 AUTHOR INITIALS:      
         
 Initials               Name        
------------------------------------------------------------------------------------   
 NR                     Narayana Reddy                              
                             
 MODIFICATIONS:      
                             
 Initials               Date            Modification      
------------------------------------------------------------------------------------   
 NR                     2020-06-03      Created for SE2017-963 BOT Process.                          
                           
******/

CREATE PROC [dbo].[IC_Bots_Process_Mapped_Ic_Account_Config_Exist]
     (
         @IC_Bots_Process_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;


        DECLARE @Ic_Account_Config_Exists BIT = 0;


        SELECT
            @Ic_Account_Config_Exists = 1
        FROM
            dbo.IC_Bots_Process_Invoice_Collection_Account_Config_Map ibpicacm
        WHERE
            ibpicacm.IC_Bots_Process_Id = @IC_Bots_Process_Id;



        SELECT  @Ic_Account_Config_Exists AS Ic_Account_Config_Exists;

    END;

GO
GRANT EXECUTE ON  [dbo].[IC_Bots_Process_Mapped_Ic_Account_Config_Exist] TO [CBMSApplication]
GO
