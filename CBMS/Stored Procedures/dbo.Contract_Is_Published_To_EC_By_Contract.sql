SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:  Contract_Is_Published_To_EC_By_Contract      
        
DESCRIPTION:        
 To get the Is_Publiushed_To_Ec Status based on the given Contract_Id     
        
INPUT PARAMETERS:        
 Name				DataType   Default  Description        
-------------------------------------------------------------------------------------------------------  
 @Contract_Id		Int			
 
        
OUTPUT PARAMETERS:        
 Name      DataType  Default Description        
------------------------------------------------------------------------------------------------------        
        
        
USAGE EXAMPLES:        
------------------------------------------------------------------------------------------------------  
 
 EXEC Contract_Is_Published_To_EC_By_Contract 10866 

        
AUTHOR INITIALS:        
 Initials Name        
------------------------------------------------------------------------------------------------------  
 RKV      Ravi Kumar Vegesna      


MODIFICATIONS        
        
 Initials	Date			Modification        
------------------------------------------------------------        
 RKV		2015-12-30		Created        
 
******/        
CREATE PROCEDURE [dbo].[Contract_Is_Published_To_EC_By_Contract] ( @Contract_ID INT )
AS 
BEGIN        
        
      SET NOCOUNT ON;
      
      SELECT
            Is_Published_To_EC
      FROM
            CONTRACT
      WHERE
            CONTRACT_ID = @Contract_ID
              
END       

;
GO
GRANT EXECUTE ON  [dbo].[Contract_Is_Published_To_EC_By_Contract] TO [CBMSApplication]
GO
