SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE [dbo].[cbmsEntityAudit_GetLogForEntity]
	( @MyAccountId int
	, @entity_id int
	, @entity_identifier int
	)
AS
BEGIN

	   select ea.entity_id
		, ea.entity_identifier
		, ea.user_info_id
		, ui.first_name + ' ' + ui.last_name modified_by
		, ea.audit_type
		, case ea.audit_type
			when 1 then 'Created'
			when 2 then 'Edited'
			when 3 then 'Deleted'
			end as 'audit_type_name'
		, ea.modified_date
	     from entity_audit ea 
	     join user_info ui on ui.user_info_id = ea.user_info_id
	    where ea.entity_id = @entity_id
	      and ea.entity_identifier = @entity_identifier
	 order by ea.modified_date

END
GO
GRANT EXECUTE ON  [dbo].[cbmsEntityAudit_GetLogForEntity] TO [CBMSApplication]
GO
