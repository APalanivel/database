SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
NAME:    [Workflow].[Get_Workflow_Queue_Filter_Parameters]      
DESCRIPTION:   The Stored Procedure will return the Filter Parameter values    
------------------------------------------------------------         
 INPUT PARAMETERS:          
 Name   DataType  Default Description          
------------------------------------------------------------          
 OUTPUT PARAMETERS:          
 Name   DataType  Default Description          
------------------------------------------------------------          
 USAGE EXAMPLES:    EXEC [Workflow].[Get_Workflow_Queue_Filter_Parameters]      
------------------------------------------------------------          
AUTHOR INITIALS:          
Initials Name          
------------------------------------------------------------          
AKP  ArunKumar      
       
 MODIFICATIONS           
 Initials Date   Modification          
------------------------------------------------------------          
 AKP    SEP-2019 Created      
      
******/      
     
CREATE PROCEDURE [Workflow].[Get_Workflow_Queue_Filter_Parameters]            
 -- Add the parameters for the stored procedure here            
 @Module_Id int           
AS            
BEGIN            
 -- SET NOCOUNT ON added to prevent extra result sets from            
 -- interfering with SELECT statements.            
 SET NOCOUNT ON;            
            
    -- Insert statements for procedure here            
 SELECT WQSFP.Workflow_Queue_Search_Filter_Param_Id,        
WQSFP.Workflow_Queue_Search_Filter_Id,        
SF.Filter_Name AS Control_Name,        
WQSFP.Param_Name,        
WQSFP.Data_Type,        
WQSF.Workflow_Queue_Search_Filter_Id        
FROM Workflow.Search_Filter AS SF        
INNER JOIN workflow.Workflow_Queue_Search_Filter AS WQSF ON SF.Search_Filter_Id = WQSF.Search_Filter_Id        
INNER JOIN Workflow.Workflow_Queue_Search_Filter_Param AS WQSFP ON WQSF.Workflow_Queue_Search_Filter_Id = WQSFP.Workflow_Queue_Search_Filter_Id   
WHERE Workflow_Queue_Id = @Module_Id  
  
END     
GO
GRANT EXECUTE ON  [Workflow].[Get_Workflow_Queue_Filter_Parameters] TO [CBMSApplication]
GO
