
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Client_Commodity_Ins

DESCRIPTION:

To insert the client commodities into the Core.Client_commodity table

INPUT PARAMETERS:
	Name									DataType		Default	Description
-------------------------------------------------------------------------------
	@Commodity_Id							INT    
	@Client_id								INT  
	@freqency_cd							INT    
	@track_level							INT  
	@commodity_service_cd					INT
	@Allow_For_DE							INT  
     	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
--------------------------------------------------------------------------------
@Client_Commodity_Id INT OUT

USAGE EXAMPLES:
--------------------------------------------------------------------------------


BEGIN TRAN

SELECT TOP 10
      *
FROM
      Core.Client_Commodity
WHERE
      Client_Id = 11278
      and commodity_id=101224
ORDER BY
      Client_Commodity_id DESC
      
DECLARE @Client_Commodity_Id INT
EXEC Client_Commodity_Ins 
      @Commodity_Id = 101224
     ,@Client_id = 11278
     ,@freqency_cd = 100016
     ,@track_level = 100273
     ,@commodity_service_cd = 100263
     ,@Allow_For_DE = 1
     ,@Client_Commodity_Id = @Client_Commodity_Id OUT

SELECT @Client_Commodity_Id
     
SELECT TOP 10
      *
FROM
      Core.Client_Commodity
WHERE
      Client_Id = 11278
      and commodity_id=101224
ORDER BY
      Client_Commodity_id DESC
ROLLBACK TRAN


AUTHOR INITIALS:
	Initials	Name
	SKA			Shobhit Kumar Agrawal
	BCH			Balaraju
	SP			Sandeep Pigilam
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
 SKA      		01/19/2010	Created
 DMR			09/10/2010	Modified for Quoted_Identifier
 BCH			10/19/2011	Added @Allow_For_DE input parameter and saved in Core.Client_commodity table 
 BCH			2013-06-10	Enhancement-40, Added Variance_Effective_Dt, Variance_Effective_Dt_Updated_User_Id, Variance_Effective_Dt_Last_Change_Ts input parameters
 SP				2015-09-02  For Data source management Phase II, Added Client_Commodity_Id as Output Parameter.
 SP				2016-11-09	Variance Enhancement Phase II VTE-15,Added Insert for Client_Commodity_Variance_Test_Config table.
							removed columns @Variance_Effective_Dt,@Variance_Effective_Dt_Updated_User_Id,@Variance_Effective_Dt_Last_Change_Ts from Client_Commodity.
******/
CREATE PROCEDURE [dbo].[Client_Commodity_Ins]
      ( 
       @Commodity_Id INT
      ,@Client_id INT
      ,@freqency_cd INT
      ,@track_level INT
      ,@commodity_service_cd INT
      ,@Allow_For_DE INT
      ,@Client_Commodity_Id INT OUT
      ,@User_Info_Id INT )
AS 
BEGIN    

      SET NOCOUNT ON;

      DECLARE @Variance_Start_Date DATE

      SELECT
            @Variance_Start_Date = ap.App_Config_Value
      FROM
            dbo.App_Config ap
      WHERE
            ap.App_Config_Cd = 'Variance_Start_Date'
      
      INSERT      INTO Core.Client_commodity
                  ( 
                   Commodity_Id
                  ,Client_Id
                  ,Frequency_Cd
                  ,Hier_Level_Cd
                  ,Commodity_Service_Cd
                  ,Allow_For_DE )
      VALUES
                  ( 
                   @Commodity_Id
                  ,@Client_id
                  ,@freqency_cd
                  ,@track_level
                  ,@commodity_service_cd
                  ,@Allow_For_DE )

      SELECT
            @Client_Commodity_Id = SCOPE_IDENTITY()               


                                 

      INSERT      INTO dbo.Client_Commodity_Variance_Test_Config
                  ( 
                   Client_Commodity_Id
                  ,Start_Dt
                  ,End_Dt
                  ,Variance_Test_Type_Cd
                  ,Created_Ts
                  ,Created_User_Id
                  ,Updated_User_Id
                  ,Last_Change_Ts )
                  SELECT
                        cc.Client_Commodity_Id
                       ,@Variance_Start_Date
                       ,NULL
                       ,vt.Code_Id
                       ,GETDATE()
                       ,@User_Info_Id
                       ,@User_Info_Id
                       ,GETDATE()
                  FROM
                        Core.Client_Commodity cc
                        INNER JOIN dbo.Code hl
                              ON hl.Code_Id = cc.Hier_Level_Cd
                        INNER JOIN dbo.Code csc
                              ON csc.Code_Id = cc.Commodity_Service_Cd
                        INNER JOIN dbo.Codeset cs
                              ON cs.Codeset_Name = 'VarianceTestType'
                        INNER JOIN dbo.Code vt
                              ON vt.Codeset_Id = cs.Codeset_Id
                                 AND vt.Code_Value = 'Full Variance Testing'
                  WHERE
                        cc.Client_Commodity_Id = @Client_Commodity_Id
                        AND hl.Code_Value = 'Account'
                        AND csc.Code_Value = 'Invoice'
                                  

END





;
GO




GRANT EXECUTE ON  [dbo].[Client_Commodity_Ins] TO [CBMSApplication]
GO
