SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE    procedure dbo.cbmsRmMarketOutlookDetail_Save

	( @AccountId int
	, @OutlookDetailId int = null
	, @OutlookId int
	, @CbmsImageId int
	, @Overview varchar(5000)
	, @OutlookTitle varchar(255)
	, @LanguageType varchar(50)
	)
as
BEGIN
	set nocount on
	declare @ThisId int

	 
	set @ThisId  = @OutlookDetailId

if @ThisId is null
	begin
	
		insert into rm_market_outlook_detail
			( rm_market_outlook_id
			, cbms_image_id
			, overview
			, rm_market_outlook_title
			, language_type
			 )
		values
			( @OutlookId
			, @CbmsImageId
			, @Overview
			, @OutlookTitle
			, @LanguageType
			)
	
		set @ThisId = scope_identity()

	end
else
	begin
		if @CbmsImageId is null 
		begin
			select @cbmsimageid = cbms_image_id 
			from rm_market_outlook_detail 
			where rm_market_outlook_detail_id = @OutlookDetailId
		end
		
		update rm_market_outlook_detail
		set rm_market_outlook_id = @OutlookId			
			, cbms_image_id = @CbmsImageId
			, overview = @Overview
			, rm_market_outlook_title = @OutlookTitle
			, language_type = @LanguageType
		where rm_market_outlook_detail_id = @OutlookDetailId
	
	end
	


	select * from rm_market_outlook_detail where rm_market_outlook_detail_id = @ThisId

END





GO
GRANT EXECUTE ON  [dbo].[cbmsRmMarketOutlookDetail_Save] TO [CBMSApplication]
GO
