SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/******                              
 NAME: dbo.[Insert_Batch_Rerun_Variance_Records]                  
                              
 DESCRIPTION:   
                            
 
                              
 INPUT PARAMETERS:                
                           
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
@Account_Id			INT
@Commodity_ID      INT               NULL   
@service_month	   Datetime
      
                                  
 OUTPUT PARAMETERS:                
                                 
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
                              
 USAGE EXAMPLES:                                  
---------------------------------------------------------------------------------------------------------------                                  
      
--Commodity Level      
   
DECLARE	@return_value INT



EXEC	@return_value = [dbo].[Insert_Batch_Rerun_Variance_Records]
		@Account_Id =1407581 , --1407581 1405819
		@Commodity_Id = 290,
		@Service_Month = N'2017-12-01'

SELECT	'Return Value' = @return_value

GO

  
     
                             
 AUTHOR INITIALS:    
 Arunkumar Palanivel AP          
             
 Initials              Name              
---------------------------------------------------------------------------------------------------------------  
     Steps(added by Arun):

                      
 MODIFICATIONS:            
                
 Initials              Date             Modification            
---------------------------------------------------------------------------------------------------------------   
AP					April 29,2020	Procedure is created
******/
CREATE PROCEDURE [dbo].[Insert_Batch_Rerun_Variance_Records]
      (
      @Account_Id    INT
    , @Commodity_Id  INT
    , @Service_Month DATE
	,@Client_Hier_Id INT ,
--	@client_Id INT,
	@error_msg NVARCHAR (MAX) )
AS
      BEGIN

            SET NOCOUNT ON;

            DECLARE
                  @Pending_Status_Cd INT  

				  SELECT @Pending_Status_Cd = c.Code_Id FROM COde C
				  JOIN dbo.Codeset cs
				  ON cs.Codeset_Id = C.Codeset_Id 
				  WHERE cs.Codeset_Name ='Data Upload Status' 
				  AND c.Code_Dsc='Pending'

            IF NOT EXISTS
                  (     SELECT
                              1
                        FROM  dbo.Variance_Batch_Rerun_Logs
                        WHERE Account_Id = @Account_Id
                              AND   Commodity_Id = @Commodity_Id
                              AND   Service_Month = @Service_Month
							  AND Client_Hier_Id = @Client_Hier_Id
							  AND Status_Cd = @Pending_Status_Cd )
                  BEGIN


                        INSERT INTO dbo.Variance_Batch_Rerun_Logs (
                                                                        Account_Id
                                                                      , Commodity_Id
                                                                      , Service_Month
                                                                      , Status_Cd
																	  , Client_Hier_Id
																	  , Error_Msg
																--	  , Client_id
                                                                  )
                        VALUES
                             ( @Account_Id            -- Account_Id - int
                             , @Commodity_Id          -- Commodity_Id - int
                             , @Service_Month         -- Service_Month - int
                             , @Pending_Status_Cd     -- Status_Cd - int 
							 , @Client_Hier_Id 
							 , @error_msg
							-- , @client_Id
                              );

                  END;
      END;

GO
GRANT EXECUTE ON  [dbo].[Insert_Batch_Rerun_Variance_Records] TO [CBMSApplication]
GO
