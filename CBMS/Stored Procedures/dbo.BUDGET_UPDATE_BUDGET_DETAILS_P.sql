SET NUMERIC_ROUNDABORT OFF 
GO

SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

 NAME:  
	dbo.BUDGET_UPDATE_BUDGET_DETAILS_P
 
 DESCRIPTION:
 
 INPUT PARAMETERS:  
 Name						DataType	  Default Description  
------------------------------------------------------------  
@budget_account_id			INT
@month_identifier			DATETIME
@variable					VARCHAR(300)
@variable_value				DECIMAL(32 16)
@transportation				VARCHAR(300)
@transportation_value		DECIMAL(32 16)
@transmission_value			DECIMAL(32 16)
@distribution_value			DECIMAL(32 16)
@other_bundled_value		DECIMAL(32 16)
@other_fixed_costs_value	DECIMAL(32 16)
@sourcing_tax				VARCHAR(300)
@sourcing_tax_value			DECIMAL(32 16)
@rates_tax_value			DECIMAL(32 16)
@budget_usage				DECIMAL(32 16)
@nymex_forecast				DECIMAL(32 16)

 
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  

 USAGE EXAMPLES:
------------------------------------------------------------  
  
		EXEC dbo.BUDGET_UPDATE_BUDGET_DETAILS_P
			@budget_account_id			= 356850,
			@month_identifier			= '2011-01-01',
			@variable					= '5.82*c3',
			@variable_value				= 9.05243,
			@transportation				= '((((((a1/.95-a1)*0.14)*c1)/1.055056)*(31/1500))*(((1.3*c1)/1.055056)*(326/1500))+(((1.2*c1)/1.055056)*(1143/1500)))*((1500*31)/a2)',
			@transportation_value		= 1.86514,
			@transmission_value			= null,
			@distribution_value			= 0.63771,
			@other_bundled_value		= NULL,
			@other_fixed_costs_value	= NULL,
			@sourcing_tax				= NULL,
			@sourcing_tax_value			= NULL,
			@rates_tax_value			= NULL,
			@budget_usage				= 21610.9493580000020000,
			@nymex_forecast				= 6.2500000000000000
  
 AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 PNR		Pandarinath
 
 MODIFICATIONS:
 Initials Date			Modification  
------------------------------------------------------------  
 PNR	  03/31/2011	MAINT-548 fixes code to save the full formula entered for tranportation
								Extended param length upto 300 and increased the actual column width for the below mentioned columns
									 Variable, transportation, Sourcing_tax
******/
  
CREATE PROCEDURE dbo.BUDGET_UPDATE_BUDGET_DETAILS_P
(
	@budget_account_id			INT,
	@month_identifier			DATETIME,
	@variable					VARCHAR(300),
	@variable_value				DECIMAL(32, 16),
	@transportation				VARCHAR(300),
	@transportation_value		DECIMAL(32, 16),
	@transmission_value			DECIMAL(32, 16),
	@distribution_value			DECIMAL(32, 16),
	@other_bundled_value		DECIMAL(32, 16),
	@other_fixed_costs_value	DECIMAL(32, 16),
	@sourcing_tax				VARCHAR(300),
	@sourcing_tax_value			DECIMAL(32, 16),
	@rates_tax_value			DECIMAL(32, 16),
	@budget_usage				DECIMAL(32, 16),
	@nymex_forecast				DECIMAL(32, 16)
)
AS 
BEGIN  

	SET NOCOUNT ON ;
	
    UPDATE  
		dbo.budget_details
    SET     
		variable				= @variable,
        variable_value			= @variable_value,
        transportation			= @transportation,
        transportation_value	= @transportation_value,
        transmission_value		= @transmission_value,
        distribution_value		= @distribution_value,
        other_bundled_value		= @other_bundled_value,
        other_fixed_costs_value = @other_fixed_costs_value,
        sourcing_tax			= @sourcing_tax,
        sourcing_tax_value		= @sourcing_tax_value,
        rates_tax_value			= @rates_tax_value,
        budget_usage			= @budget_usage,
        nymex_forecast			= @nymex_forecast
    WHERE   
		budget_account_id	= @budget_account_id
        AND month_identifier = @month_identifier  
END
GO

GRANT EXECUTE ON  [dbo].[BUDGET_UPDATE_BUDGET_DETAILS_P] TO [CBMSApplication]
GO
GO