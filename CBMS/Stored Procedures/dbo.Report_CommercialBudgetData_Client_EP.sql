
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
       
/******  
NAME:  
    CBMS.dbo.Report_CommercialBudgetData_Client_EP  
 DESCRIPTION:   INPUT PARAMETERS:  
Name    DataType  Default Description  
------------------------------------------------------------  
@Client_ID  INT      
@Commodity_ID  INT       
@Begin_Date  DATETIME      
@End_Date   DATETIME      OUTPUT PARAMETERS:  
Name   DataType  Default Description  
------------------------------------------------------------  
 USAGE EXAMPLES:  
------------------------------------------------------------  
 EXEC dbo.Report_CommercialBudgetData_Client_EP 10069,290,'01/01/2010','12/01/2010'         
 EXEC dbo.Report_CommercialBudgetData_Client_EP 11467,290,'06/01/2009','04/04/2010'          
 EXEC dbo.Report_CommercialBudgetData_Client_EP 10092,290,'09/09/2009','06/01/2010'           
  AUTHOR INITIALS:  
Initials   Name  
-----------------------------------------------------------  
SSR    Sharad Srivastava 
AP    Athmaram Pabbathi 
RR    Raghu Reddy   
AKR   Ashok Kumar Raju

MODIFICATIONS   
Initials Date  Modification  
------------------------------------------------------------  
SSR   06/22/2010 Created  
AP   08/31/2011 Removed subquery & Cost_Usage_Site, Site, Client_Currency_Group_Map from the script and replaced with CTE "CTE_Cost_Usage_Account_Dtl"  
      and used tables Cost_Usage_Account_Dtl, Client_Hier, Client_Hier_Account, Bucket_Master tables  
AP   09/02/2011 Removed Select statement for finding Client Currency Group ID and added script to get Currency_Unit_Id for 'USD'  
AP   03/29/2012 Removed multiple references of CH & CHA and used temp tables  
RR   2012-05-23 Granted exucute permission to new role "CBMS_SSRS_Reports" and removed for "CBMSApplication" 
AKR  2012-07-10 MOdified the code to filter the vendor names on Utility Accounts and modified the code to use Cost_Usage_Site_Dtl.
AKR  2013-07-30 Modified the code to include not managed accounts
******/  
  
CREATE PROCEDURE [dbo].[Report_CommercialBudgetData_Client_EP]
      ( 
       @Client_ID INT
      ,@Commodity_ID INT
      ,@Begin_Date DATETIME
      ,@End_Date DATETIME )
AS 
BEGIN            
      SET NOCOUNT ON ;          
      DECLARE @Currency_Gr_id INT           
      DECLARE
            @Entity_unit INT
           ,@Currency_Unit_Id INT 
      DECLARE @Currency_Group_Id INT       
      DECLARE @Category_tbl TABLE
            ( 
             Category_name VARCHAR(50) PRIMARY KEY CLUSTERED
            ,Sort_Order INT )       
   
      CREATE TABLE #Category_site
            ( 
             Client_Name VARCHAR(200)
            ,Sitegroup_Name VARCHAR(200)
            ,State_Name VARCHAR(200)
            ,Site_Name VARCHAR(200)
            ,Site_id INT
            ,Client_Id INT
            ,Category_name VARCHAR(50)
            ,SiteRef VARCHAR(30)
            ,SortOrder INT
            ,PRIMARY KEY CLUSTERED ( Client_Id, Site_Id, Category_name ) )
                
      CREATE TABLE #CU_Data
            ( 
             Category VARCHAR(20)
            ,Value DECIMAL(32, 16)
            ,SERVICE_MONTH DATETIME
            ,Site_Id INT
            ,Total_Cost DECIMAL(32, 16)
            ,Total_Usage DECIMAL(32, 16) )   
                   
      SELECT
            @Currency_Unit_Id = cu.Currency_Unit_ID
      FROM
            dbo.Currency_Unit cu
      WHERE
            cu.Currency_Unit_Name = 'USD'  
        
      INSERT      INTO @Category_tbl
      VALUES
                  ( 'Usage', 1 ),
                  ( 'Utility Cost', 2 ),
                  ( 'Supplier Cost', 3 ),
                  ( 'Total Cost', 4 ),
                  ( 'Unit Cost', 5 )          
          
      SELECT
            @Currency_Gr_id = a.CURRENCY_GROUP_ID
      FROM
            dbo.CLIENT a
      WHERE
            a.CLIENT_ID = @Client_ID          
          
      SELECT
            @Entity_unit = ent.ENTITY_ID
      FROM
            dbo.ENTITY ent
      WHERE
            ent.ENTITY_NAME = 'Kwh'
            AND ent.ENTITY_DESCRIPTION = 'Unit for electricity'          
          
            
      INSERT      INTO #Category_site
                  SELECT
                        ch.Client_Name
                       ,ch.Sitegroup_Name
                       ,ch.State_Name
                       ,ch.Site_Name
                       ,ch.Site_ID
                       ,ch.Client_Id
                       ,cat_tbl.Category_name
                       ,s.SITE_REFERENCE_NUMBER
                       ,cat_tbl.Sort_Order
                  FROM
                        Core.Client_Hier ch
                        JOIN dbo.SITE s
                              ON s.Client_ID = ch.Client_Id
                                 AND s.SITE_ID = ch.Site_Id
                        CROSS JOIN @Category_tbl cat_tbl
                  WHERE
                        ch.Client_Id = @Client_ID
                        AND ch.Site_Id <> 0
                        AND ch.Site_Not_Managed = 0
                        AND ch.Site_Closed = 0 ;    
                              
      WITH  Cte_ipdata
              AS ( SELECT
                        SITE_ID
                       ,SERVICE_MONTH
                   FROM
                        ( SELECT
                              SUM(CONVERT(INT, is_expected)) Expected
                             ,SUM(CONVERT(INT, is_received)) Received
                             ,ips.service_month
                             ,ips.site_id
                          FROM
                              dbo.INVOICE_PARTICIPATION ips
                              JOIN dbo.SITE s
                                    ON s.site_id = ips.site_id
                              JOIN ( SELECT
                                          cha.Account_Id
                                         ,cha.Meter_Id
                                         ,ch.Site_Id
                                         ,cha.Commodity_Id
                                     FROM
                                          core.Client_Hier_Account cha
                                          JOIN Core.Client_Hier ch
                                                ON cha.Client_Hier_Id = ch.Client_Hier_Id
                                     WHERE
                                          ( cha.Account_Type = 'Utility'
                                            OR ( ( cha.Supplier_Meter_Disassociation_Date > cha.Supplier_Account_begin_Dt
                                                   OR cha.Supplier_Meter_Disassociation_Date IS NULL )
                                                 AND cha.Account_Type = 'Supplier' ) )
                                          AND ch.Client_Id = @Client_ID ) AS vm
                                    ON vm.account_id = ips.account_id
                                       AND ips.SITE_ID = vm.SITE_ID
                          WHERE
                              s.client_id = @Client_ID
                              AND vm.Commodity_Id = @Commodity_ID
                              AND ips.SERVICE_MONTH BETWEEN @begin_date
                                                    AND     @End_date
                          GROUP BY
                              ips.site_id
                             ,ips.service_month ) sumdata
                   WHERE
                        sumdata.Expected = sumdata.Received)
            INSERT      INTO #CU_Data
                        ( 
                         Category
                        ,Value
                        ,SERVICE_MONTH
                        ,Site_Id
                        ,Total_Cost
                        ,Total_Usage )
                        SELECT
                              CASE WHEN bm.Bucket_Name = 'Total Usage' THEN 'Usage'
                                   WHEN bm.Bucket_Name = 'Utility Cost' THEN 'Utility Cost'
                                   WHEN bm.Bucket_Name = 'Marketer Cost' THEN 'Supplier Cost'
                                   WHEN bm.Bucket_Name = 'Total Cost' THEN 'Total Cost'
                              END AS Category
                             ,CASE WHEN bm.Bucket_Name = 'Total Usage' THEN ( cus.Bucket_Value * ConsUC.Conversion_Factor )
                                   WHEN bm.Bucket_Name = 'Utility Cost' THEN ( cus.Bucket_Value * CUC.Conversion_Factor )
                                   WHEN bm.Bucket_Name = 'Marketer Cost' THEN ( cus.Bucket_Value * CUC.Conversion_Factor )
                                   WHEN bm.Bucket_Name = 'Total Cost' THEN ( cus.Bucket_Value * CUC.Conversion_Factor )
                              END AS Value
                             ,cus.SERVICE_MONTH
                             ,s.Site_Id
                             ,CASE WHEN bm.Bucket_Name = 'Total Cost' THEN ( cus.Bucket_Value * CUC.Conversion_Factor )
                                   ELSE 0
                              END AS Total_Cost
                             ,CASE WHEN bm.Bucket_Name = 'Total Usage' THEN ( cus.Bucket_Value * ConsUC.Conversion_Factor )
                                   ELSE 0
                              END AS Total_Usage
                        FROM
                              dbo.Bucket_Master bm
                              LEFT JOIN dbo.Cost_Usage_Site_Dtl cus
                                    ON bm.Bucket_Master_Id = cus.Bucket_Master_Id
                              JOIN Core.Client_Hier s
                                    ON cus.Client_Hier_Id = s.Client_Hier_Id
                              JOIN Cte_ipdata cte_ip
                                    ON cte_ip.SITE_ID = s.Site_Id
                                       AND cte_ip.SERVICE_MONTH = cus.SERVICE_MONTH
                              LEFT JOIN Currency_Unit_Conversion CUC
                                    ON CUC.Base_Unit_ID = CUS.Currency_Unit_ID
                                       AND CUC.Converted_Unit_ID = @Currency_Unit_Id
                                       AND CUC.Currency_Group_ID = @Currency_Gr_id
                                       AND cuc.Conversion_Date = CUS.Service_Month
                              LEFT JOIN Consumption_Unit_Conversion ConsUC
                                    ON ConsUC.Base_Unit_ID = CUS.UOM_Type_Id
                                       AND ConsUC.Converted_Unit_ID = @Entity_unit
                        WHERE
                              s.Client_Id = @Client_ID
                              AND bm.Commodity_Id = @Commodity_ID
                              AND cus.SERVICE_MONTH BETWEEN @begin_date
                                                    AND     @End_date ;  
   
   
      WITH  Cte_Unit_Cost
              AS ( SELECT
                        'Unit cost' Category
                       ,SUM(cu.Total_Cost) / NULLIF(SUM(cu.Total_Usage), 0) Value
                       ,cu.SERVICE_MONTH
                       ,cu.Site_Id
                   FROM
                        #CU_Data cu
                   GROUP BY
                        cu.Site_Id
                       ,cu.SERVICE_MONTH),
            Cte_Cost_Usage
              AS ( SELECT
                        Category
                       ,SERVICE_MONTH
                       ,Site_Id
                       ,Value
                   FROM
                        #CU_Data
                   UNION
                   SELECT
                        Category
                       ,SERVICE_MONTH
                       ,Site_Id
                       ,Value
                   FROM
                        Cte_Unit_Cost )
            SELECT
                  tbl_cts.Client_Name [Client Name]
                 ,tbl_cts.Sitegroup_Name [Division Name]
                 ,tbl_cts.State_Name [State]
                 ,tbl_cts.Site_Name [Site Name]
                 ,tbl_cts.SiteRef
                 ,ven.Vendor_Name [Primary Utility]
                 ,tbl_cts.Category_name
                 ,Cusdata.value
                 ,dt_dim.Date_D
            FROM
                  #Category_site tbl_cts
                  LEFT JOIN meta.Date_Dim dt_dim
                        ON tbl_cts.Client_Id = @Client_ID
                           AND dt_dim.Date_D BETWEEN @begin_date AND @End_date
                  LEFT JOIN ( SELECT
                                    MIN(cha.Account_Vendor_Name) Vendor_Name
                                   ,ch.Site_Id
                              FROM
                                    core.Client_Hier_Account cha
                                    JOIN core.Client_Hier ch
                                          ON cha.Client_Hier_Id = ch.Client_Hier_Id
                              WHERE
                                    cha.Account_Type = 'Utility'
                                    AND cha.Commodity_Id = @Commodity_ID
                              GROUP BY
                                    ch.Site_ID ) ven
                        ON tbl_cts.Site_Id = ven.SITE_ID
                  LEFT JOIN Cte_Cost_Usage Cusdata
                        ON tbl_cts.Site_id = Cusdata.SITE_ID
                           AND dt_dim.Date_D = Cusdata.service_month
                           AND tbl_cts.Category_name = Cusdata.Category
            GROUP BY
                  tbl_cts.Client_Name
                 ,tbl_cts.Sitegroup_Name
                 ,tbl_cts.State_Name
                 ,tbl_cts.Site_Name
                 ,ven.Vendor_Name
                 ,tbl_cts.Category_name
                 ,Cusdata.value
                 ,tbl_cts.SiteRef
                 ,tbl_cts.SortOrder
                 ,dt_dim.Date_D
            ORDER BY
                  dt_dim.Date_D
                 ,tbl_cts.Site_Name
                 ,tbl_cts.SortOrder   
                 
      DROP TABLE #Category_site
      DROP TABLE #CU_Data
  
END ;
;
GO

GRANT EXECUTE ON  [dbo].[Report_CommercialBudgetData_Client_EP] TO [CBMS_SSRS_Reports]
GO
