SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE dbo.UPDATE_BASELOAD_VOLUME_P
	@baseload_volume decimal(32,16),
	@month_identifier DATETIME,
	@contract_id int
AS
BEGIN

	SET NOCOUNT ON

	UPDATE dbo.baseload_details
		SET baseload_volume = @baseload_volume
	FROM dbo.load_profile_specification l INNER JOIN dbo.baseload_details b ON b.load_profile_specification_id = l.load_profile_specification_id
	WHERE l.contract_id = @contract_id
		AND l.month_identifier = @month_identifier
	
END
GO
GRANT EXECUTE ON  [dbo].[UPDATE_BASELOAD_VOLUME_P] TO [CBMSApplication]
GO
