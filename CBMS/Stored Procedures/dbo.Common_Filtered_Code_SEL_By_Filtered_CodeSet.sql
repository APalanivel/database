
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
Name:    
 dbo.Common_Filtered_Code_SEL_By_Filtered_CodeSet  
   
 Description:    
 Returns the codes falling under the Filtered Codeset Id  
   
 Input Parameters:    
    Name      DataType       Default    Description    
------------------------------------------------------------------------    
 @Filtered_Code_Name   VARCHAR(25)  
 @CodeSet_Name    VARCHAR(25)  
   
 Output Parameters:    
 Name      Datatype Default   Description    
-------------------------------------------------------------------------    
   
 Usage Examples:  
------------------------------------------------------------    
 EXEC dbo.Common_Filtered_Code_SEL_By_Filtered_CodeSet 'Hedge Reporting', 'UOM'  
 EXEC dbo.Common_Filtered_Code_SEL_By_Filtered_CodeSet 'Client Attribute', 'HierLevel' 
EXEC Common_Filtered_Code_SEL_By_Filtered_CodeSet 
      'InvoiceTracking'
     ,'LocalizationLanguage'
     
EXEC Common_Filtered_Code_SEL_By_Filtered_CodeSet 
      'InvoiceTracking'
     ,'LocalizationLanguage'     

 SELECT * FROM CODEset WHERE CODEset_ID = 5
      
Author Initials:    
 Initials Name  
------------------------------------------------------------  
 CPE  Chaitanya Panduga Eshwar  
   
 Modifications :    
 Initials   Date		Modification    
------------------------------------------------------------    
 CPE		10/21/2011	Created SP
 CPE		07/19/2012	Added Code_Dsc to the output
 SP			2016-12-12  Invoice tracking @Filtered_Code_Name is made optional so that the application will do filters without db hit for each filter
						Added  Codeset_Filtered_Value  in select list.
						Added order by Cd.Code_Dsc if @Filtered_Code_Name = 'InvoiceTracking' AND @CodeSet_Name = 'LocalizationLanguage'.
******/
CREATE PROCEDURE [dbo].[Common_Filtered_Code_SEL_By_Filtered_CodeSet]
      ( 
       @Filtered_Code_Name VARCHAR(25) = NULL
      ,@CodeSet_Name VARCHAR(25) )
AS 
BEGIN

      SET NOCOUNT ON

      SELECT
            FC.Filtered_Code_Id AS Code_Id
           ,Cd.Code_Value
           ,Cd.Code_Dsc
           ,FC.Display_Seq
           ,FC.Is_Default
           ,fCd.Code_Value AS Codeset_Filtered_Value
      FROM
            dbo.Filtered_Code FC
            JOIN dbo.Code Cd
                  ON FC.Filtered_Code_Id = Cd.Code_Id
            JOIN dbo.CodeSet CS
                  ON Cd.Codeset_Id = CS.Codeset_Id
            JOIN dbo.Code fCd
                  ON fcd.Code_Id = FC.Codeset_Filtered_Cd
            JOIN dbo.Codeset fCS
                  ON fCd.Codeset_Id = fCS.Codeset_Id
      WHERE
            CS.Codeset_Name = @Codeset_Name
            AND ( @Filtered_Code_Name IS NULL
                  OR fCd.Code_Value = @Filtered_Code_Name )
            AND fCS.Codeset_Name = 'Filtered Codeset'
      ORDER BY
            CASE WHEN @Filtered_Code_Name = 'InvoiceTracking'
                      AND @CodeSet_Name = 'LocalizationLanguage' THEN Cd.Code_Dsc
                 ELSE CAST(FC.Display_Seq AS VARCHAR)
            END


END;


;
GO


GRANT EXECUTE ON  [dbo].[Common_Filtered_Code_SEL_By_Filtered_CodeSet] TO [CBMSApplication]
GO
