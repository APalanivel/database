SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******          

NAME: dbo.Sso_Savings_Detail_Sel_By_Owner_Id_Type
     
DESCRIPTION: 
	To get Sso Savings Detail for given Owner Id and Owner Type by summing up 
						     estimated_Savings_Value based on Sso Savings Title.
      
INPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
    @Owner_Id		INT
    @Owner_Type		VARCHAR(30)	  			Client/Division/Site
               
OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        
    
	EXEC dbo.Sso_Savings_Detail_Sel_By_Owner_Id_Type  1930,'Site',1,5
	
	EXEC dbo.Sso_Savings_Detail_Sel_By_Owner_Id_Type  11292,'Client',1,10
	
	EXEC dbo.Sso_Savings_Detail_Sel_By_Owner_Id_Type  2225,'Division',1,5

AUTHOR INITIALS:          
	INITIALS	NAME          
------------------------------------------------------------          
	PNR			PANDARINATH
	CPE			Chaitanya Panduga Eshwar
	          
MODIFICATIONS:           
	INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
	PNR			25-JUN-10	CREATED  
	PNR			24-AUG-10	Added Pagination Logic and also added two parameters  @StartIndex,@EndIndex
	CPE			04/13/2011	Added code to get Client_Hier_Id from the Owner_Id and Owner_Type    
*/

CREATE PROCEDURE dbo.Sso_Savings_Detail_Sel_By_Owner_Id_Type
( 
 @Owner_Id INT
,@Owner_Type VARCHAR(30)
,@StartIndex INT = 1
,@EndIndex INT = 2147483647 )
AS 
BEGIN

      SET NOCOUNT ON ;

      DECLARE @Client_Hier_Id int

      SELECT
            @Client_Hier_Id = Client_Hier_Id
      FROM
            Core.Client_Hier CH
      WHERE
            ( @OWNER_ID = ch.Client_Id
              AND @Owner_Type = 'Client'
              AND ch.Site_Id = 0
              AND ch.Sitegroup_Id = 0 )
            OR ( @OWNER_ID = ch.Sitegroup_Id
                 AND @Owner_Type = 'Division'
                 AND ch.Site_Id = 0 )
            OR ( @OWNER_ID = ch.Site_Id
                 AND @Owner_Type = 'Site'
                 AND ch.Site_Id > 0 ) ;
            WITH  Cte_Sso_Savings_Detail
                    AS ( SELECT
                              ss.SAVINGS_TITLE
                             ,SUM(ssd.ESTIMATED_SAVINGS_VALUE) Estimated_Savings
                             ,SUM(ssd.ACTUAL_SAVINGS_VALUE) Actual_Savings
                             ,Row_Num = ROW_NUMBER() OVER ( ORDER BY ss.SAVINGS_TITLE )
                             ,Total_Rows = COUNT(1) OVER ( )
                         FROM
                                                           dbo.SSO_SAVINGS ss
                                                           JOIN dbo.SSO_SAVINGS_DETAIL ssd
                                                            ON ssd.SSO_SAVINGS_ID = ss.SSO_SAVINGS_ID
                                                           JOIN dbo.SSO_SAVINGS_OWNER_MAP ssom
                                                            ON ssd.SSO_SAVINGS_ID = ssom.SSO_SAVINGS_ID
                         WHERE
                                                           SSOM.Client_Hier_Id = @Client_Hier_Id
                         GROUP BY
                                                           ss.SAVINGS_TITLE)
            SELECT
                  SAVINGS_TITLE
                 ,Estimated_Savings
                 ,Actual_Savings
                 ,Total_Rows
            FROM
                  Cte_Sso_Savings_Detail
            WHERE
                  Row_Num BETWEEN @StartIndex and @EndIndex
END


GO
GRANT EXECUTE ON  [dbo].[Sso_Savings_Detail_Sel_By_Owner_Id_Type] TO [CBMSApplication]
GO
