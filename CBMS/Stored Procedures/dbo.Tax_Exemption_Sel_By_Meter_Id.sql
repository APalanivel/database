SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Tax_Exemption_Sel_By_Meter_Id]  
     
DESCRIPTION: 
	To Get Tax Exemption for Selected Meter Id with pagination.
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Meter_Id		INT
@StartIndex		INT			1			
@EndIndex		INT			2147483647
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------

	EXEC dbo.Tax_Exemption_Sel_By_Meter_Id  7727,1,10
	EXEC dbo.Tax_Exemption_Sel_By_Meter_Id  27802,1,10
	

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE			MODIFICATION
------------------------------------------------------------
PNR			04-June-10		CREATED

*/

CREATE PROCEDURE dbo.Tax_Exemption_Sel_By_Meter_Id
(
	@Meter_Id		INT
	,@StartIndex	INT		= 1
	,@EndIndex		INT		= 2147483647
)
AS
BEGIN

	SET NOCOUNT ON;

	WITH Cte_Meter_Cbms AS
	(
		SELECT
			 Cbms_Image_Id
			,Row_Num = ROW_NUMBER() OVER(ORDER BY METER_ID)
			,Total_Rows = COUNT(1) OVER()
        FROM
			dbo.METER_CBMS_IMAGE_MAP       
        WHERE
             METER_ID = @Meter_Id
	)
	SELECT
		 i.CBMS_DOC_ID
		 ,i.Cbms_Image_Id
		 ,Total_Rows
	FROM
		 Cte_Meter_Cbms t
		 JOIN dbo.cbms_image i
			ON i.CBMS_IMAGE_ID = t.CBMS_IMAGE_ID
	WHERE
		 Row_Num BETWEEN @StartIndex AND @EndIndex

END
GO
GRANT EXECUTE ON  [dbo].[Tax_Exemption_Sel_By_Meter_Id] TO [CBMSApplication]
GO
