SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.CREATE_SYSTEM_EMAIL_LOG_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@dealTicketId  	int       	          	
	@cbmsImageId   	int       	          	
	@fromEmailAddress	varchar(200)	          	
	@toEmailAddress	varchar(200)	          	
	@emailDate     	datetime  	          	
	@entityType    	int       	          	
	@entityName    	varchar(200)	          	
	@ccEmailAddress	varchar(200)	          	
	@emailName     	varchar(200)	          	
	@triggerMonth  	datetime  	          	
	@subject       	text      	          	
	@content       	text      	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE            PROCEDURE DBO.CREATE_SYSTEM_EMAIL_LOG_P 

@userId varchar(10),
@sessionId varchar(20),
@dealTicketId int, 
@cbmsImageId int, 
@fromEmailAddress varchar(200), 
@toEmailAddress varchar(200), 
@emailDate datetime, 
@entityType int,
@entityName varchar(200),
@ccEmailAddress varchar(200), 
@emailName varchar(200), 
@triggerMonth datetime, 
@subject text, 
@content text

AS
set nocount on
	DECLARE @imageCount int
	SELECT @imageCount = COUNT(1) FROM RM_EMAIL_LOG WHERE CBMS_IMAGE_ID = @cbmsImageId
/*
	IF @imageCount > 0
		BEGIN

			UPDATE RM_EMAIL_LOG SET 
				FROM_EMAIL_ADDRESS = @fromEmailAddress, 
				TO_EMAIL_ADDRESS = @toEmailAddress, 
				GENERATION_DATE = @emailDate, 
				CC_EMAIL_ADDRESS = @ccEmailAddress, 
				SUBJECT = @subject, 
				CONTENT = @content 
			WHERE CBMS_IMAGE_ID = @cbmsImageId
		END
*/
	IF @imageCount = 0
		BEGIN
			DECLARE @emailTypeId int
			select @emailTypeId = ENTITY_ID FROM ENTITY WHERE ENTITY_TYPE = @entityType AND ENTITY_NAME = @entityName
			
			INSERT INTO RM_EMAIL_LOG 
			(RM_DEAL_TICKET_ID, CBMS_IMAGE_ID, FROM_EMAIL_ADDRESS, TO_EMAIL_ADDRESS, GENERATION_DATE, 
			EMAIL_TYPE_ID, CC_EMAIL_ADDRESS, EMAIL_NAME, TRIGGER_MONTH_IDENTIFIER, SUBJECT, CONTENT)
			VALUES(@dealTicketId, @cbmsImageId, @fromEmailAddress, @toEmailAddress, @emailDate, 
			@emailTypeId, @ccEmailAddress, @emailName, @triggerMonth, @subject, @content)
		END
GO
GRANT EXECUTE ON  [dbo].[CREATE_SYSTEM_EMAIL_LOG_P] TO [CBMSApplication]
GO
