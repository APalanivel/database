
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                            
                  
NAME: [DBO].[Cost_Usage_Site_Dtl_Changes_Sel_For_Data_Transfer]                  
                  
DESCRIPTION:                   
      Select the changed Site Level data from cost_usage_site and cost_usage_site_Dtl tables(using change tracking) to transfer        
      it to CBMS                 
                        
INPUT PARAMETERS:                            
NAME   DATATYPE DEFAULT  DESCRIPTION                            
------------------------------------------------------------                            
@Min_CT_Ver BIGINT            
@Max_CT_Ver BIGINT               
                                  
OUTPUT PARAMETERS:                            
NAME   DATATYPE DEFAULT  DESCRIPTION                     
                         
------------------------------------------------------------                            
USAGE EXAMPLES:                            
------------------------------------------------------------                  
 declare @Max_CT_Ver BIGINT        
 SELECT @Max_CT_Ver = CHANGE_TRACKING_CURRENT_VERSION()        
 EXEC dbo.Cost_Usage_Site_Dtl_Changes_Sel_For_Data_Transfer 10916000, @Max_CT_Ver        
        
AUTHOR INITIALS:                            
INITIALS NAME                            
------------------------------------------------------------                            
HG   Harihara Suthan G          
AKR  Ashok Kumar Raju        
                
MODIFICATIONS                  
INITIALS DATE  MODIFICATION                  
------------------------------------------------------------                  
AKR   11/20/2011  Created              
AKR   2011-12-01  Added Client_Hier_ID to @Cost_Usage_Site table              
AKR   2012-03-30  Modified the Script for Additional Data Changes    
AKR   2013-01-04  Modified the code to get all the buckets as a part of Detailed Report.    
RKV   2017-06-22  Added new column Data_Type_Cd,Actual_Bucket_Value,Estimated_Bucket_Value as part of Data enhancement.						
     
*/                  
CREATE PROCEDURE [dbo].[Cost_Usage_Site_Dtl_Changes_Sel_For_Data_Transfer]
      ( 
       @Min_CT_Ver BIGINT
      ,@Max_CT_Ver BIGINT )
AS 
BEGIN                  
                  
      SET NOCOUNT ON                  
                  
      DECLARE
            @CBMS_Data_Source_Cd INT
           ,@Invoice_Data_Source_Cd INT        
                 
                 
      DECLARE @Cost_Usage_Site_Dtl_Changes TABLE
            ( 
             Service_Month DATE
            ,Bucket_Master_Id INT
            ,Client_Hier_ID INT
            ,SYS_CHANGE_OPERATION CHAR(1)
            ,PRIMARY KEY CLUSTERED ( Client_Hier_Id, Bucket_Master_Id, Service_Month ) )      
        
          
      SELECT
            @Invoice_Data_Source_Cd = MAX(CASE WHEN cd.Code_Value = 'Invoice' THEN cd.CODE_ID
                                          END)
           ,@CBMS_Data_Source_Cd = MAX(CASE WHEN cd.Code_Value = 'CBMS' THEN cd.CODE_ID
                                       END)
      FROM
            dbo.codeset cs
            JOIN dbo.code cd
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Std_Column_Name = 'Data_Source_Cd'
            AND cd.Code_Value IN ( 'Invoice', 'CBMS' )       
                  
      INSERT      INTO @Cost_Usage_Site_Dtl_Changes
                  ( 
                   Service_Month
                  ,Bucket_Master_Id
                  ,Client_Hier_ID
                  ,SYS_CHANGE_OPERATION )
                  SELECT
                        ct.Service_Month
                       ,ct.Bucket_Master_Id
                       ,ct.client_hier_id
                       ,CAST(ct.SYS_CHANGE_OPERATION AS CHAR(1)) AS Sys_Change_Operation
                  FROM
                        CHANGETABLE(CHANGES dbo.Cost_Usage_Site_Dtl, @Min_CT_Ver) ct
                  WHERE
                        ct.Sys_Change_Version <= @Max_CT_Ver        
                     
         
     
      SELECT
            cus.Service_Month
           ,cus.Bucket_Master_Id
           ,cusd.Bucket_Value
           ,cusd.Uom_Type_Id
           ,cusd.Currency_Unit_Id
           ,cusd.Created_By_Id AS Created_User_Id
           ,cusd.Created_Ts
           ,ISNULL(cusd.Updated_Ts, cusd.created_Ts) AS Updated_Ts
           ,cusd.Updated_By_Id AS Updated_User_Id
           ,cusd.Data_Source_Cd
           ,cus.Client_Hier_ID
           ,cus.SYS_CHANGE_OPERATION
           ,cusd.Data_Type_Cd
           ,cusd.Actual_Bucket_Value
           ,cusd.Estimated_Bucket_Value
      FROM
            @Cost_Usage_Site_Dtl_Changes cus
            INNER JOIN dbo.Bucket_Master bm
                  ON bm.Bucket_Master_Id = cus.Bucket_Master_Id
            INNER JOIN core.client_hier ch
                  ON ch.client_Hier_ID = cus.client_Hier_ID
            LEFT OUTER JOIN dbo.Cost_Usage_Site_Dtl cusd
                  ON cusd.client_Hier_ID = cus.client_Hier_ID
                     AND cusd.Bucket_Master_Id = cus.Bucket_Master_Id
                     AND cusd.Service_Month = cus.Service_Month
      WHERE
            cus.Sys_Change_Operation = 'D'
            OR ( cus.SYS_CHANGE_OPERATION IN ( 'I', 'U' )
                 AND cusd.Data_Source_Cd IN ( @CBMS_Data_Source_Cd, @Invoice_Data_Source_Cd ) )
      GROUP BY
            cus.Service_Month
           ,cus.Bucket_Master_Id
           ,cusd.Bucket_Value
           ,cusd.UOM_Type_Id
           ,cusd.CURRENCY_UNIT_ID
           ,cusd.Created_By_Id
           ,cusd.Created_Ts
           ,cusd.Updated_Ts
           ,cusd.Updated_By_Id
           ,cusd.Data_Source_Cd
           ,cus.client_hier_id
           ,cus.SYS_CHANGE_OPERATION
           ,cusd.Data_Type_Cd
           ,cusd.Actual_Bucket_Value
           ,cusd.Estimated_Bucket_Value    
        
END;

;


;
GO



GRANT EXECUTE ON  [dbo].[Cost_Usage_Site_Dtl_Changes_Sel_For_Data_Transfer] TO [ETL_Execute]
GO
