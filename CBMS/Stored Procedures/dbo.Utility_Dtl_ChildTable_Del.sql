SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Utility_Dtl_ChildTable_Del

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Vendor_Commodity_Map_Id	INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	SELECT * FROM Utility_Dtl_Election
	BEGIN TRAN
		EXEC Utility_Dtl_ChildTable_Del 17855
	ROLLBACK TRAN
		
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	SKA			01/12/2011	Created
******/

CREATE PROCEDURE [dbo].[Utility_Dtl_ChildTable_Del]
      @Vendor_Commodity_Map_Id INT
AS 
BEGIN  
      SET NOCOUNT ON  
      BEGIN TRY
            BEGIN TRAN
		
            DELETE FROM
                  dbo.Utility_Dtl_Election
            WHERE
                  Vendor_Commodity_Map_Id = @Vendor_Commodity_Map_Id
			 
            DELETE FROM
                  dbo.Utility_Dtl_Transport
            WHERE
                  Vendor_Commodity_Map_Id = @Vendor_Commodity_Map_Id

            DELETE FROM
                  dbo.Utility_Dtl_Service_Level
            WHERE
                  Vendor_Commodity_Map_Id = @Vendor_Commodity_Map_Id
			
            DELETE FROM
                  dbo.Utility_Dtl_Notification
            WHERE
                  Vendor_Commodity_Map_Id = @Vendor_Commodity_Map_Id

            DELETE FROM
                  dbo.Utility_Dtl_Volume_Requirement
            WHERE
                  Vendor_Commodity_Map_Id = @Vendor_Commodity_Map_Id
			
            DELETE FROM
                  dbo.Utility_Dtl_Telemetering
            WHERE
                  Vendor_Commodity_Map_Id = @Vendor_Commodity_Map_Id
			
            DELETE FROM
                  dbo.Utility_Dtl_Opportunity
            WHERE
                  Vendor_Commodity_Map_Id = @Vendor_Commodity_Map_Id																		 
			 
            COMMIT TRAN  
      END TRY
	
      BEGIN CATCH
            ROLLBACK TRAN
            EXEC usp_RethrowError	
      END CATCH

END
GO
GRANT EXECUTE ON  [dbo].[Utility_Dtl_ChildTable_Del] TO [CBMSApplication]
GO
