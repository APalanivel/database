SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
      

/******
NAME:
	 dbo.Report_DE_Active_Utility_NonNAAccounts
		
DESCRIPTION:
	Query to Get the Active Utility Non North Region Accounts
	
INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
EXEC dbo.Report_DE_Active_Utility_NonNAAccounts



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	AKR        Ashok Kumar Raju    

MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	AKR        2012-05-23  cloned from the select statement of dts job "ActiveUtilityNonNAAccounts"
	
******/

CREATE PROCEDURE dbo.Report_DE_Active_Utility_NonNAAccounts
AS 
BEGIN

      SET NOCOUNT ON 
      
      SELECT
            ch.Client_Name Client
           ,ch.State_Name STATE
           ,cha.Account_Number AccountNumber
           ,com.Commodity_Name Commodity
      FROM
            core.client_hier ch
            INNER JOIN core.Client_Hier_Account cha
                  ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN dbo.Commodity com
                  ON cha.Commodity_Id = com.Commodity_Id
      WHERE
            ch.Country_Name NOT IN ( 'USA', 'Canada' )
            AND cha.Account_Not_Managed = 0
            AND cha.Account_Type = 'Utility'
      GROUP BY
            ch.Client_Name
           ,ch.State_Name
           ,cha.Account_Number
           ,com.Commodity_Name 


END



;

GO
GRANT EXECUTE ON  [dbo].[Report_DE_Active_Utility_NonNAAccounts] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_Active_Utility_NonNAAccounts] TO [CBMSApplication]
GO
