SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
                         
 NAME: dbo.Missing_Contract_Image_Exception_Exists_Sel_By_Contract_Id           
                          
 DESCRIPTION:      
		Get the Suppler account dates.               
                          
 INPUT PARAMETERS:      
                         
 Name                               DataType          Default       Description      
-------------------------------------------------------------------------------------  
 @Account_Id			INT        
                          
 OUTPUT PARAMETERS:      
                               
 Name                               DataType          Default       Description      
-------------------------------------------------------------------------------------                            
 USAGE EXAMPLES:                              
-------------------------------------------------------------------------------------                 

Select * from Contract where Contract_Id = 172958

EXEC dbo.Missing_Contract_Image_Exception_Exists_Sel_By_Contract_Id
     @Contract_Id = 172958


 AUTHOR INITIALS:    
       
 Initials                   Name      
-------------------------------------------------------------------------------------  
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
-------------------------------------------------------------------------------------  
 NR                     2019-06-13      Created for ADD Contract.                        
                         
******/

CREATE PROCEDURE [dbo].[Missing_Contract_Image_Exception_Exists_Sel_By_Contract_Id]
    (
        @Contract_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Is_Missing_Contract_Exception_Exists BIT = 0;






        SELECT
            @Is_Missing_Contract_Exception_Exists = 1
        FROM
            dbo.Contract_Exception ce
            INNER JOIN dbo.Code c
                ON c.Code_Id = ce.Exception_Type_Cd
            INNER JOIN dbo.Code ct
                ON ct.Code_Id = ce.Exception_Status_Cd
        WHERE
            ce.Contract_Id = @Contract_Id
            AND c.Code_Value = 'Missing Contract Image'
            AND ct.Code_Value IN ( 'New', 'In Progress' );


        SELECT
            @Is_Missing_Contract_Exception_Exists = 1
        FROM
            dbo.CONTRACT c
        WHERE
            c.CONTRACT_ID = @Contract_Id
            AND DATEADD(DD, 30, CAST(c.Contract_Created_Ts AS DATE)) > =  CAST(GETDATE() AS DATE)
            AND @Is_Missing_Contract_Exception_Exists = 0;


        SELECT
            @Is_Missing_Contract_Exception_Exists AS Is_Missing_Contract_Exception_Exists;


    END;



GO
GRANT EXECUTE ON  [dbo].[Missing_Contract_Image_Exception_Exists_Sel_By_Contract_Id] TO [CBMSApplication]
GO
