SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********     
NAME:    
   [dbo].[RM_Contract_Volumes_Sel_For_Account_Forecast_Volume_Upd]    

DESCRIPTION:    


INPUT PARAMETERS:    
Name							DataType		Default		Description    
-------------------------------------------------------------------------
 @Contract_Id									INT

OUTPUT PARAMETERS:    
	Name				DataType  Default Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------

SELECT MIN(Service_Month),MAX(Service_Month),Client_Hier_Id FROM dbo.RM_Client_Hier_Forecast_Volume WHERE Client_Hier_Id in(18551,18552,18553)
GROUP BY Client_Hier_Id

SELECT b.CONTRACT_START_DATE,b.CONTRACT_END_DATE,b.CONTRACT_ID FROM Core.Client_Hier_Account a 
	INNER JOIN dbo.CONTRACT b ON a.Supplier_Contract_ID = b.CONTRACT_ID 
	WHERE a.Client_Hier_Id in(18551,18552,18553)

EXEC [dbo].[RM_Contract_Volumes_Sel_For_Account_Forecast_Volume_Upd]   162279


AUTHOR INITIALS:    
	Initials	Name    
------------------------------------------------------------    
	RR			Raghu Reddy


MODIFICATIONS     
	Initials	Date			Modification    
------------------------------------------------------------    
	RR			2018-09-30		SP Created

******/

CREATE PROCEDURE [dbo].[RM_Contract_Volumes_Sel_For_Account_Forecast_Volume_Upd]
    (
        @Contract_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        CREATE TABLE #Site_Forecast_UOM
             (
                 Client_Hier_Id INT
                 , RM_Forecast_UOM_Type_Id INT
             );


        INSERT INTO #Site_Forecast_UOM
             (
                 Client_Hier_Id
                 , RM_Forecast_UOM_Type_Id
             )
        SELECT
            ch.Client_Hier_Id
            , chob.RM_Forecast_UOM_Type_Id
        FROM
            Core.Client_Hier ch
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON ch.Client_Hier_Id = chob.Client_Hier_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Client_Hier_Id = ch.Client_Hier_Id
                   AND  cha.Commodity_Id = chob.Commodity_Id
        WHERE
            ch.Site_Id > 0
            AND cha.Supplier_Contract_ID = @Contract_Id
        GROUP BY
            ch.Client_Hier_Id
            , chob.RM_Forecast_UOM_Type_Id;




        INSERT INTO #Site_Forecast_UOM
             (
                 Client_Hier_Id
                 , RM_Forecast_UOM_Type_Id
             )
        SELECT
            ch.Client_Hier_Id
            , chob.RM_Forecast_UOM_Type_Id
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier chclient
                ON ch.Client_Id = chclient.Client_Id
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON chclient.Client_Hier_Id = chob.Client_Hier_Id
                   AND  ch.Country_Id = chob.Country_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Client_Hier_Id = ch.Client_Hier_Id
                   AND  cha.Commodity_Id = chob.Commodity_Id
        WHERE
            ch.Site_Id > 0
            AND chclient.Sitegroup_Id = 0
            AND cha.Supplier_Contract_ID = @Contract_Id
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.RM_Client_Hier_Onboard siteonb
                               WHERE
                                    ch.Client_Hier_Id = siteonb.Client_Hier_Id
                                    AND siteonb.Commodity_Id = chob.Commodity_Id)
        GROUP BY
            ch.Client_Hier_Id
            , chob.RM_Forecast_UOM_Type_Id;


        SELECT
            chsupp.Client_Hier_Id
            , chutil.Account_Id
            , cmv.MONTH_IDENTIFIER AS Service_Month
            , chsupp.Commodity_Id
            , CASE freq.ENTITY_NAME WHEN 'Daily' THEN
                                        SUM(ISNULL((cmv.VOLUME * cuc.CONVERSION_FACTOR), 0)) * dd.DAYS_IN_MONTH_NUM
                  ELSE SUM(ISNULL((cmv.VOLUME * cuc.CONVERSION_FACTOR), 0))
              END AS Volume
            , sfu.RM_Forecast_UOM_Type_Id
        FROM
            Core.Client_Hier_Account chutil
            INNER JOIN Core.Client_Hier_Account chsupp
                ON chutil.Meter_Id = chsupp.Meter_Id
            INNER JOIN dbo.CONTRACT_METER_VOLUME cmv
                ON chsupp.Meter_Id = cmv.METER_ID
                   AND  chsupp.Supplier_Contract_ID = cmv.CONTRACT_ID
            INNER JOIN meta.DATE_DIM dd
                ON CAST(cmv.MONTH_IDENTIFIER AS DATE) = dd.DATE_D
            LEFT JOIN #Site_Forecast_UOM sfu
                ON sfu.Client_Hier_Id = chsupp.Client_Hier_Id
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = cmv.UNIT_TYPE_ID
                   AND  cuc.CONVERTED_UNIT_ID = sfu.RM_Forecast_UOM_Type_Id
            INNER JOIN dbo.ENTITY freq
                ON cmv.FREQUENCY_TYPE_ID = freq.ENTITY_ID
        WHERE
            chsupp.Account_Type = 'Supplier'
            AND chutil.Account_Type = 'Utility'
            AND chsupp.Supplier_Contract_ID = @Contract_Id
        GROUP BY
            chsupp.Client_Hier_Id
            , chutil.Account_Id
            , cmv.MONTH_IDENTIFIER
            , chsupp.Commodity_Id
            , sfu.RM_Forecast_UOM_Type_Id
            , freq.ENTITY_NAME
            , dd.DAYS_IN_MONTH_NUM;



    END;



GO
GRANT EXECUTE ON  [dbo].[RM_Contract_Volumes_Sel_For_Account_Forecast_Volume_Upd] TO [CBMSApplication]
GO
