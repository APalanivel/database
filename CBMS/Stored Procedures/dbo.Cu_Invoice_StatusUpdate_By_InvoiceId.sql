
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
	 cbms_prod.dbo.Cu_Invoice_StatusUpdate_By_InvoiceId   
	 
DESCRIPTION:     
INPUT PARAMETERS:    
 Name					DataType		Default Description    
------------------------------------------------------------    
@MyAccountId			int    
@cu_invoice_id			int           
@currency_unit_id		int				 null          
@is_processed			bit				 0    
@is_reported			bit				 0    
@is_dnt					bit				 0    
@do_not_track_id		int				 null    
@ubm_invoice_identifier varchar(50)		 null          
@is_manual				bit				 0    
@is_duplicate			bit				 0
@Invoice_Type_Cd		INT				 NULL   
@Meter_Read_Type_Cd		INT				 NULL
          
OUTPUT PARAMETERS:    
Name   DataType  Default Description    
------------------------------------------------------------    
 USAGE EXAMPLES:    
------------------------------------------------------------    
USE CBMS_TK3

	EXEC Cu_Invoice_StatusUpdate_By_InvoiceId 49,577,3,1,1,0,NULL,NULL,0,0

 AUTHOR INITIALS:    
 Initials	Name    
------------------------------------------------------------    
 KS			Kailash  
 SP			Sandeep Pigilam
 NR			Narayana Reddy

MODIFICATIONS     
 Initials	Date		Modification    
------------------------------------------------------------    
 KS			19/02/2010	Created 
 SP			2014-07-24	Data Operations Enhancement Phase III,Added Input Parameter @Invoice_Type_Cd
 NR			2016-12-23	MAINT-4737 replaced Invoice_Type_Cd with Meter_Read_Type_Cd and @Invoice_Type_Cd as input.

******/    
CREATE PROCEDURE [dbo].[Cu_Invoice_StatusUpdate_By_InvoiceId]
      ( 
       @MyAccountId INT
      ,@cu_invoice_id INT
      ,@currency_unit_id INT = NULL
      ,@is_processed BIT = 0
      ,@is_reported BIT = 0
      ,@is_dnt BIT = 0
      ,@do_not_track_id INT = NULL
      ,@ubm_invoice_identifier VARCHAR(50) = NULL
      ,@is_manual BIT = 0
      ,@is_duplicate BIT = 0
      ,@Meter_Read_Type_Cd INT = NULL
      ,@Invoice_Type_Cd INT = NULL )
AS 
BEGIN    

      SET NOCOUNT ON    

      UPDATE
            dbo.cu_invoice
      SET   
            currency_unit_id = @currency_unit_id
           ,is_processed = @is_processed
           ,is_reported = @is_reported
           ,is_dnt = @is_dnt
           ,do_not_track_id = @do_not_track_id
           ,updated_by_id = @MyAccountId
           ,updated_date = GETDATE()
           ,ubm_invoice_identifier = @ubm_invoice_identifier
           ,is_manual = @is_manual
           ,is_duplicate = @is_duplicate
           ,Meter_Read_Type_Cd = @Meter_Read_Type_Cd
           ,Invoice_Type_Cd = @Invoice_Type_Cd
      WHERE
            cu_invoice_id = @cu_invoice_id    
		

END;


;
GO


GRANT EXECUTE ON  [dbo].[Cu_Invoice_StatusUpdate_By_InvoiceId] TO [CBMSApplication]
GO
