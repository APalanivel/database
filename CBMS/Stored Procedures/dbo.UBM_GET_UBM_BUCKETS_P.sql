SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--EXEC dbo.UBM_GET_BATCHLOG_DETAILS_P '1','1',73,'cASS'

CREATE    PROCEDURE dbo.UBM_GET_UBM_BUCKETS_P 
@userId varchar(20),
@sessionId varchar(20),
@ubmId int


as
	set nocount on
 declare @ubmName Varchar(100)

	select @ubmName=ubm_name from ubm where ubm_id=@ubmId

if @ubmName like 'cass'
begin
	select distinct ITEM_CODE BUCKETS from UBM_INVOICE_DETAILS
	where  ITEM_CODE not in 
		(
			select UBM_charge_bucket_type from  ubm_charge_bucket_map
		)
	
		and 
		ITEM_CODE not in 
		(
			select ubm_usage_bucket_type from  ubm_usage_bucket_map
		)
end
else if @ubmId = 0 or @ubmName like 'avista'
begin


		select distinct ITEM_TYPE_DESCRIPTION BUCKETS  from UBM_INVOICE_DETAILS
	where  ITEM_TYPE_DESCRIPTION not in 
		(
			select UBM_charge_bucket_type from  ubm_charge_bucket_map
		)
	
		and 
		ITEM_TYPE_DESCRIPTION not in 
		(
			select ubm_usage_bucket_type from  ubm_usage_bucket_map
		)
end
else
begin


		select distinct DETAILS.ITEM_TYPE_DESCRIPTION BUCKETS  from 
		UBM_INVOICE_DETAILS DETAILS,
		UBM_INVOICE INVOICE,
		UBM_BATCH_MASTER_LOG MASTER_LOG
		where  DETAILS.ITEM_TYPE_DESCRIPTION not in 
		(
			select UBM_charge_bucket_type from  ubm_charge_bucket_map
		)
	
		and 
		DETAILS.ITEM_TYPE_DESCRIPTION not in 
		(
			select ubm_usage_bucket_type from  ubm_usage_bucket_map
		)
		AND DETAILS.UBM_INVOICE_ID = INVOICE.UBM_INVOICE_ID
		AND INVOICE.UBM_BATCH_MASTER_LOG_ID = MASTER_LOG.UBM_BATCH_MASTER_LOG_ID
		AND MASTER_LOG.UBM_ID =@ubmId
		
end
GO
GRANT EXECUTE ON  [dbo].[UBM_GET_UBM_BUCKETS_P] TO [CBMSApplication]
GO
