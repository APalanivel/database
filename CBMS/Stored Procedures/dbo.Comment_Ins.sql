
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.Comment_Ins

DESCRIPTION:

INPUT PARAMETERS:
	Name					DataType		Default	Description
---------------------------------------------------------------
	@Comment_Type_CD		Int
	@Comment_User_Info_Id	Int
	@Comment_Dt				Date
	@Comment_Text			VARCHAR(MAX)

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Comment_Id				Int				Inserted identity value to be returned to the calling procedure/application.
	
USAGE EXAMPLES:
------------------------------------------------------------
BEGIN TRAN
	 SELECT
		cd.*
	 FROM
		dbo.Code cd  
		JOIN dbo.Codeset cs  
		ON cs.Codeset_Id = cd.Codeset_Id  
	 WHERE  
		cs.Codeset_Name = 'VarianceComment'  
		AND cd.Code_Value = 'VarianceComment'  

	
		DECLARE @Inserted_Value Int
			
        EXEC dbo.Comment_Ins 
            @Comment_Type_CD = 100230
           ,@Comment_User_Info_Id = 49
           ,@Comment_Dt = '2014-08-08'
           ,@Comment_Text = 'Acceptable variances; winter heating months.'
           ,@Comment_Id=@Inserted_Value OUT
		
		SELECT @Inserted_Value
		select * from comment where Comment_Id=@Inserted_Value
ROLLBACK


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hari
	SP			Sandeep Pigilam
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG        	03/17/2010	Created
	SP			2014-08-08	For Data Operations Enhancement Phase III,Added @Comment_Datetime local variable to save time also.

******/

CREATE PROCEDURE dbo.Comment_Ins
      @Comment_Type_CD INT
     ,@Comment_User_Info_Id INT
     ,@Comment_Dt DATE
     ,@Comment_Text VARCHAR(MAX)
     ,@Comment_Id INT OUT
AS 
BEGIN

      SET NOCOUNT ON
      DECLARE @Comment_Datetime DATETIME= ISNULL(CAST(@Comment_Dt AS DATETIME), GETDATE())

      INSERT      INTO dbo.Comment
                  ( 
                   Comment_Type_CD
                  ,Comment_User_Info_Id
                  ,Comment_Dt
                  ,Comment_Text )
      VALUES
                  ( 
                   @Comment_Type_CD
                  ,@Comment_User_Info_Id
                  ,@Comment_Datetime
                  ,@Comment_Text )

      SET @Comment_Id = SCOPE_IDENTITY()


END

;
GO

GRANT EXECUTE ON  [dbo].[Comment_Ins] TO [CBMSApplication]
GO
