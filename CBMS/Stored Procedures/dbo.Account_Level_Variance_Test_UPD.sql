SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*********   
NAME:  dbo.Account_Level_Variance_Test_UPD  
 
DESCRIPTION:  Used to save all the Account level rules data  

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
	@Variance_Rule_Dtl_Id	Int	 
	@is_Active				bit    
	@Tolerance				decimal(10,0)    
	@Test_Description		varchar(255)  
	@Account_Id				Int 
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:   

	exec Account_Level_Variance_Test_UPD 1425,1,1,321,'nag',141759
	select * from variance_rule_dtl
	
	select * from variance_rule_dtl_account_override where account_id = 141759
	0 , 0,1423	25018
	
------------------------------------------------------------  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
NK   Nageswara Rao Kosuri  
AP	Arunkumar Palanivel       


Initials Date  Modification  
------------------------------------------------------------  
NK	10/13/2009  Created
NK	12/03/2009  Removed IF ELSE statements and Incorporated MERGE INTO
Ap	Feb 13,2020	The procedure is modified to exlcude the addition of variance rule details override for AVT

******/  

    
CREATE PROCEDURE [dbo].[Account_Level_Variance_Test_UPD]    
	@Variance_Rule_Dtl_Id INT,          
	@is_Active BIT,            
	@is_Manual_Override BIT,    
	@Tolerance DECIMAL(16,4),            
	@Test_Description VARCHAR(255),        
	@Account_Id INT          
AS        
BEGIN        

	SET NOCOUNT ON;         
	     
	DECLARE @Is_Rule_Dtl_Active bit

	SELECT @Is_Rule_Dtl_Active = vrd.Is_Active 
	FROM dbo.Variance_Rule_Dtl vrd 
	WHERE vrd.Variance_Rule_Dtl_Id = @Variance_Rule_Dtl_Id   


	IF NOT EXISTS 
	
	(SELECT
                                                                        1
                                                                  FROM  Core.Client_Hier_Account cha
                                                                        JOIN
                                                                        dbo.Account_Variance_Consumption_Level avcl1
                                                                              ON avcl1.ACCOUNT_ID = cha.Account_Id
                                                                        JOIN
                                                                        dbo.Variance_Consumption_Level vcl
                                                                              ON vcl.Variance_Consumption_Level_Id = avcl1.Variance_Consumption_Level_Id
                                                                                 AND vcl.Commodity_Id = cha.Commodity_Id
                                                                        JOIN
                                                                        dbo.Bucket_Master bm
                                                                              ON bm.Commodity_Id = vcl.Commodity_Id
                                                                        JOIN
                                                                        dbo.Variance_category_Bucket_Map vcbm
                                                                              ON vcbm.Bucket_master_Id = bm.Bucket_Master_Id
                                                                        JOIN
                                                                        dbo.Variance_Parameter vp
                                                                              ON vp.Variance_Category_Cd = vcbm.Variance_Category_Cd
                                                                        JOIN
                                                                        dbo.Variance_Parameter_Baseline_Map vpbm
                                                                              ON vpbm.Variance_Parameter_Id = vp.Variance_Parameter_Id
                                                                        JOIN
                                                                        dbo.Variance_Rule vr
                                                                              ON vr.Variance_Baseline_Id = vpbm.Variance_Baseline_Id
                                                                        JOIN
                                                                        dbo.Variance_Rule_Dtl vrd1
                                                                              ON vrd1.Variance_Rule_Id = vr.Variance_Rule_Id
                                                                        JOIN
                                                                        dbo.Variance_Parameter_Commodity_Map vpcm
                                                                              ON vpcm.Variance_Parameter_Id = vp.Variance_Parameter_Id
                                                                        JOIN
                                                                        dbo.Variance_Processing_Engine vpe
                                                                              ON vpe.Variance_Processing_Engine_Id = vpcm.variance_process_Engine_id
                                                                        JOIN
                                                                        dbo.Code vpcat
                                                                              ON vpcat.Code_Id = vpe.Variance_Engine_Cd
                                                                        JOIN
                                                                        dbo.Variance_Consumption_Extraction_Service_Param_Value vesp
                                                                              ON vesp.Variance_Consumption_Level_Id = vcl.Variance_Consumption_Level_Id
                                                                  WHERE cha.Account_Id = @Account_id
                                                                        AND   cha.Account_Type = 'Utility'
                                                                        AND   vpcat.Code_Value = 'Advanced Variance Test'
																		AND vrd1.Variance_Rule_Dtl_Id =@Variance_Rule_Dtl_Id) 
	BEGIN
      
      MERGE INTO dbo.Variance_Rule_Dtl_Account_Override vrdac      
      USING(      
                  SELECT      
                        @Variance_Rule_Dtl_Id Variance_Rule_Dtl_Id      
                        ,@Account_Id Account_Id  
            ) src      
      ON vrdac.Variance_Rule_Dtl_Id = src.Variance_Rule_Dtl_Id      
            AND vrdac.Account_Id = src.Account_Id      
            AND src.Variance_Rule_Dtl_Id = @Variance_Rule_Dtl_Id              
      WHEN MATCHED AND (
							(@Is_Rule_Dtl_Active = 0) AND (@Is_Active = 0)
						) 
						OR ((@Is_Rule_Dtl_Active = 0) AND (@Is_Active = 1) AND vrdac.tolerance IS NULL
						) THEN
		 DELETE      
      WHEN MATCHED AND (@Is_Active = 1 AND @is_Manual_Override = 1)     
	   OR (@Is_Active = 1 AND @is_Manual_Override = 0) THEN      
		 UPDATE      
		 SET vrdac.Tolerance = CASE @is_Manual_Override WHEN 0 THEN NULL ELSE @Tolerance END    
		 ,vrdac.Test_Description = CASE @is_Manual_Override WHEN 0 THEN NULL ELSE @Test_Description END    
	  WHEN NOT MATCHED BY TARGET  AND ((@Is_Active = 1 AND @is_Manual_Override = 0) AND (@Is_Rule_Dtl_Active = 1)   
	   OR ((@is_Manual_Override = 1 AND @Is_Active = 1) AND (@Is_Rule_Dtl_Active = 0 OR @Is_Rule_Dtl_Active = 1)) 
	   OR  ((@is_Manual_Override = 0 AND @Is_Active = 0) AND (@Is_Rule_Dtl_Active = 1))) THEN      
			INSERT(Variance_Rule_Dtl_Id        
			,ACCOUNT_ID     
			,Tolerance    
			,Test_Description         
		   )      
		   VALUES( @Variance_Rule_Dtl_Id      
		   ,@Account_Id    
		   ,CASE WHEN @is_Manual_Override = 0 THEN NULL ELSE @Tolerance END    
		   ,CASE WHEN @is_Manual_Override = 0 THEN NULL ELSE @Test_Description END    
		   )  ;    
   END      
    
     
END 


GO
GRANT EXECUTE ON  [dbo].[Account_Level_Variance_Test_UPD] TO [CBMSApplication]
GO
