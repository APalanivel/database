SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Rfp_Send_Rfp_Email_Attachment_Ins]
           
DESCRIPTION:             
			To get bid responses placed by suppliers 
			
INPUT PARAMETERS:            
	Name			DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Supplier_Id	INT
    @Contact_Id		INT
    @Sr_Rfp_Id		INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT TOP 10 * FROM dbo.SR_RFP_EMAIL_ATTACHMENT
            
	BEGIN TRANSACTION
		SELECT * FROM  dbo.SR_RFP_EMAIL_ATTACHMENT WHERE SR_RFP_EMAIL_LOG_ID = 1           	
		EXEC dbo.Sr_Rfp_Send_Rfp_Email_Attachment_Ins 1,'251056,251056,251057'
		SELECT * FROM  dbo.SR_RFP_EMAIL_ATTACHMENT WHERE SR_RFP_EMAIL_LOG_ID = 1 
	ROLLBACK TRANSACTION
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-04-21	Global Sourcing - Phase3 - GCS-635 Created
******/
CREATE PROCEDURE [dbo].[Sr_Rfp_Send_Rfp_Email_Attachment_Ins]
      ( 
       @Sr_Rfp_Email_Log_Id INT
      ,@Cbms_Image_Id VARCHAR(MAX) )
AS 
BEGIN

      SET NOCOUNT ON;
      
      INSERT      INTO dbo.SR_RFP_EMAIL_ATTACHMENT
                  ( 
                   SR_RFP_EMAIL_LOG_ID
                  ,CBMS_IMAGE_ID )
                  SELECT
                        @Sr_Rfp_Email_Log_Id
                       ,cast(Segments AS INT)
                  FROM
                        dbo.ufn_split(@Cbms_Image_Id, ',')
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.SR_RFP_EMAIL_ATTACHMENT atta
                                     WHERE
                                          atta.SR_RFP_EMAIL_LOG_ID = @Sr_Rfp_Email_Log_Id
                                          AND atta.CBMS_IMAGE_ID = cast(Segments AS INT) );
      
END;
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Send_Rfp_Email_Attachment_Ins] TO [CBMSApplication]
GO
