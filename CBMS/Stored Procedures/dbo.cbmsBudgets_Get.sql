SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE       PROCEDURE [dbo].[cbmsBudgets_Get]
	( @budget_id int = null
	)
AS
BEGIN

	   select budget_id
		, client_id
		, budget_name budget_title
		, date_initiated date_created
		, budget_start_year
		, budget_start_month
		, posted_by user_info_id
		, commodity_type_id
		--, cd_account_factor
		, cbms_image_id
		, (convert(varchar(10),budget_date, 101)) as report_date
	from budget
	    where budget_id = @budget_id

END





GO
GRANT EXECUTE ON  [dbo].[cbmsBudgets_Get] TO [CBMSApplication]
GO
