SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE dbo.GET_BASIS_PRICE_FOR_MARK_TO_MARKET_INDEX_REPORT_P 
@userId varchar,
@sessionId varchar,
@fromDate Varchar(12),
@toDate Varchar(12),
@priceIndexId int

as
	set nocount on
select	CONVERT (Varchar(12), forecastdetails.MONTH_IDENTIFIER, 101)  MONTH_IDENTIFIER,
	 CAST(forecastdetails.BASIS_PRICE as decimal(8,3)) BASIS_PRICE

from	RM_FORECAST forecast,
	RM_FORECAST_DETAILS forecastdetails

where	forecast.PRICE_INDEX_ID=@priceIndexId AND
	forecast.RM_FORECAST_ID=forecastdetails.RM_FORECAST_ID AND
	forecastdetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @fromDate, 101) AND CONVERT(Varchar(12), @toDate, 101)
GO
GRANT EXECUTE ON  [dbo].[GET_BASIS_PRICE_FOR_MARK_TO_MARKET_INDEX_REPORT_P] TO [CBMSApplication]
GO
