
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******************************************************************************************************                                                   
NAME: dbo.BUDGET_ACCOUNT_UPD                                          
                                              
DESCRIPTION:                                              
                                              
     This procedure will be used to save the Budget Upload data to Budget_Details, Budget_Details_Comments and Budget_Account tables.                                          
                                               
                                              
INPUT PARAMETERS:                                              
      NAME     DATATYPE    DEFAULT           DESCRIPTION                                              
------------------------------------------------------------------------                                              
    @tvp_budget_details  Tvp_Budget_Details                                          
    @tvp_budget_details  Tvp_Budget_Detail_Comments                                        
    @budgetId              int                                        
    @userinfoId   INT             
                                                            
OUTPUT PARAMETERS:                                                        
      NAME              DATATYPE    DEFAULT           DESCRIPTION                                                 
                                                     
------------------------------------------------------------                                                        
USAGE EXAMPLES:                                                        
------------------------------------------------------------                                                      
                                                  
Begin Tran                                
 DECLARE @tvp_budget_details Tvp_Budget_Details                                           
 DECLARE @tvp_budget_detail_comments Tvp_Budget_Detail_Comments                                      
                                    
 INSERT INTO @tvp_budget_details VALUES (526597,'2010-09-01','5',5,'6',6,0,0,'0.02',0.02,'7',7,'8',8,'2.03',2.03,'9',9,1,0,12.36)                                         
 INSERT INTO @tvp_budget_detail_comments VALUES (526597,'Variable Comments','','','','','','','',0,0,0)                                       
                                      
 EXEC dbo.BUDGET_ACCOUNT_UPD @tvp_budget_details,  @tvp_budget_detail_comments,8561,49                                    
                                         
Rollback                                         
                                                 
AUTHOR INITIALS:                                                        
	INITIALS    NAME			DATE                                          
------------------------------------------------------------                                                        
	VN          VARADA NAIR		18-March-2013                                        
                                                        
MODIFICATIONS:                                              
	INITIALS    DATE		MODIFICATION                                                        
------------------------------------------------------------                                                        
	VN			03/21/2013  Added a condition to check the value of bit params before updating                                       
							variable n sourcing tax values     
	VN			04/04/2013	Added a condition for checking NULL                                      
	VN			04/05/2013  Modified budget Details Comments to insert budget account id too                         
	VN			04/26/2013	Modified to correct the condition when user selects no fom drop down for sourcing,                        
							rates completed and rates reviewd dates                   
	VN			04/30/2013  Removed commented statements. Added condition to check is_deleted flag.Modified usage ex        
	VN			05/09/2013  Removed hard coded value for EP, NG     
	VN			05/17/2013  Modified Update statements using CASE  
	VN			05/20/2013  Modified Update statements using CASE for BUdget Account table   
	VN			06/26/2013  Modified CASE statements to retain sourcing completed, rates reviewed and 
							rates completed fields when no change is applied                      
	RR			2017-06-05	SE2017-26 - Modofied script to move/delete budget account in the queue
*****************************************************************************************************/                                 
                                        
CREATE PROCEDURE [dbo].[BUDGET_ACCOUNT_UPD]
      ( 
       @Tvp_Budget_Details tvp_budget_details READONLY
      ,@tvp_budget_detail_comments Tvp_Budget_Detail_Comments READONLY
      ,@budgetId INT
      ,@userinfoId INT )
AS 
BEGIN                                          
      SET NOCOUNT ON;                             
                                          
      DECLARE
            @commodity_Id INT
           ,@commodity_Id_EP INT
           ,@commodity_Id_NG INT        
           
      DECLARE
            @Rates_Queue_Cd INT
           ,@Both_Queue_Cd INT
           ,@Sourcing_Queue_Cd INT
           ,@Rates_Completed_By INT
           
          

      DECLARE @Group_Legacy_Name TABLE
            ( 
             GROUP_INFO_ID INT
            ,GROUP_NAME VARCHAR(200)
            ,Legacy_Group_Name VARCHAR(200) ) 
            
      DECLARE
            @Lookup_Value XML
           ,@Client_Name VARCHAR(200)
           ,@User_Name VARCHAR(81)
           ,@Application_Name VARCHAR(30) = 'Budget Complete/Review'
           ,@Audit_Function SMALLINT = 1 --Update
           ,@Execution_Ts DATETIME = GETDATE()
                                                                         
      SELECT
            @commodity_Id = COMMODITY_TYPE_ID
      FROM
            dbo.BUDGET
      WHERE
            BUDGET_ID = @budgetId          
      
      SELECT
            @commodity_Id_EP = ety.ENTITY_ID
      FROM
            dbo.ENTITY ety
      WHERE
            ety.ENTITY_TYPE = 157
            AND ety.ENTITY_NAME = 'Electric Power'         
                  
      SELECT
            @commodity_Id_NG = ety.ENTITY_ID
      FROM
            dbo.ENTITY ety
      WHERE
            ety.ENTITY_TYPE = 157
            AND ety.ENTITY_NAME = 'Natural Gas'    
            
             
            
      INSERT      INTO @Group_Legacy_Name
                  ( 
                   GROUP_INFO_ID
                  ,GROUP_NAME
                  ,Legacy_Group_Name )
                  EXEC dbo.Group_Info_Sel_By_Group_Legacy 
                        @Group_Legacy_Name_Cd_Value = 'Regulated_Markets_Budgets'
                        
      SELECT
            @Rates_Queue_Cd = c.Code_Id
      FROM
            dbo.Code AS c
            JOIN dbo.Codeset AS cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'IncludeInAnalystQueue'
            AND c.Code_Value = 'Yes - Rates only'
  
      SELECT
            @Both_Queue_Cd = c.Code_Id
      FROM
            dbo.Code AS c
            JOIN dbo.Codeset AS cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'IncludeInAnalystQueue'
            AND c.Code_Value = 'Yes - Rates and Sourcing'

      SELECT
            @Sourcing_Queue_Cd = c.Code_Id
      FROM
            dbo.Code AS c
            JOIN dbo.Codeset AS cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'IncludeInAnalystQueue'
            AND c.Code_Value = 'Yes - Sourcing only'
            
      SET @Lookup_Value = ( SELECT
                              ba.BUDGET_ACCOUNT_ID AS BUDGET_ACCOUNT_ID
                             ,ba.RATES_COMPLETED_BY AS RATES_COMPLETED_BY
                             ,ba.RATES_COMPLETED_DATE AS RATES_COMPLETED_DATE
                             ,ba.RATES_REVIEWED_BY AS RATES_REVIEWED_BY
                             ,ba.RATES_REVIEWED_DATE AS RATES_REVIEWED_DATE
                             ,ba.SOURCING_COMPLETED_BY AS SOURCING_COMPLETED_BY
                             ,ba.SOURCING_COMPLETED_DATE AS SOURCING_COMPLETED_DATE
                             ,ba.CANNOT_PERFORMED_BY AS CANNOT_PERFORMED_BY
                             ,ba.CANNOT_PERFORMED_DATE AS CANNOT_PERFORMED_DATE
                             ,baq.Queue_Id AS Queue_Id
                             ,cd.Code_Value AS Queue_Type
                             ,@Execution_Ts AS Log_Time
                            FROM
                              dbo.BUDGET_ACCOUNT ba
                              INNER JOIN @tvp_budget_detail_comments tvpbud
                                    ON ba.BUDGET_ACCOUNT_ID = tvpbud.Budget_Account_ID
                              LEFT JOIN dbo.Budget_Account_Queue baq
                                    ON ba.BUDGET_ACCOUNT_ID = baq.Budget_Account_Id
                              LEFT JOIN dbo.Code cd
                                    ON baq.Analyst_Type_Cd = cd.Code_Id
            FOR
                            XML AUTO ) 
            
                                                                      
      INSERT      INTO dbo.BUDGET_DETAILS
                  ( 
                   BUDGET_ACCOUNT_ID
                  ,MONTH_IDENTIFIER
                  ,Variable
                  ,Variable_Value
                  ,Transportation
                  ,Transportation_Value
                  ,Transmission
                  ,Transmission_Value
                  ,Distribution
                  ,Distribution_Value
                  ,Other_Bundled
                  ,Other_Bundled_Value
                  ,Other_Fixed_Costs
                  ,Other_Fixed_Costs_Value
                  ,Sourcing_Tax
                  ,Sourcing_Tax_Value
                  ,Rates_Tax
                  ,Rates_Tax_Value
                  ,BUDGET_USAGE )
                  SELECT
                        tvp.Budget_Account_ID
                       ,tvp.Month_Identifier
                       ,tvp.Variable
                       ,tvp.Variable_Value
                       ,tvp.Transportation
                       ,tvp.Transportation_Value
                       ,tvp.Transmission
                       ,tvp.Transmission_Value
                       ,tvp.Distribution
                       ,tvp.Distribution_Value
                       ,tvp.Other_Bundled
                       ,tvp.Other_Bundled_Value
                       ,tvp.Other_Fixed_Costs
                       ,tvp.Other_Fixed_Costs_Value
                       ,tvp.Sourcing_Tax
                       ,tvp.Sourcing_Tax_Value
                       ,tvp.Rates_Tax
                       ,tvp.Rates_Tax_Value
                       ,tvp.Budget_Usage
                  FROM
                        @tvp_budget_details tvp
                        INNER JOIN BUDGET_ACCOUNT ba
                              ON ba.BUDGET_ACCOUNT_ID = tvp.Budget_Account_ID
                  WHERE
                        ba.IS_DELETED = 0
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.BUDGET_DETAILS bd
                                         WHERE
                                          bd.BUDGET_ACCOUNT_ID = tvp.Budget_Account_ID
                                          AND bd.Month_Identifier = tvp.Month_Identifier )            
                                         
      UPDATE
            bd
      SET   
            VARIABLE = CASE WHEN ( bd.VARIABLE <> tvp.Variable
                                   OR bd.VARIABLE IS NULL )
                                 AND ( ( tvp.Is_Manual_Generation = 1
                                         OR tvp.Is_Manual_Generation IS NULL )
                                       OR ( tvp.Is_Manual_Generation = 0
                                            AND @commodity_Id = @commodity_Id_NG ) ) THEN tvp.Variable
                            ELSE bd.VARIABLE
                       END
           ,bd.VARIABLE_VALUE = CASE WHEN ( bd.VARIABLE_VALUE <> tvp.Variable_Value
                                            OR bd.Variable_Value IS NULL )
                                          AND ( ( tvp.Is_Manual_Generation = 1
                                                  OR tvp.Is_Manual_Generation IS NULL )
                                                OR ( tvp.Is_Manual_Generation = 0
                                                     AND @commodity_Id = @commodity_Id_NG ) ) THEN tvp.Variable_Value
                                     ELSE bd.VARIABLE_VALUE
                                END
           ,bd.TRANSPORTATION = CASE WHEN ( bd.TRANSPORTATION <> tvp.Transportation
                                            OR bd.Transportation IS NULL )
                                          AND ( ( @commodity_Id <> @commodity_Id_EP
                                                  AND tvp.Is_Manual_Generation = 1 )
                                                OR ( @commodity_Id = @commodity_Id_EP ) )
                                          OR ( tvp.Is_Manual_Generation IS NULL ) THEN tvp.Transportation
                                     ELSE bd.TRANSPORTATION
                                END
           ,bd.TRANSPORTATION_VALUE = CASE WHEN ( bd.TRANSPORTATION_VALUE <> tvp.Transportation_Value
                                                  OR bd.TRANSPORTATION_VALUE IS NULL )
                                                AND ( ( @commodity_Id <> @commodity_Id_EP
                                                        AND tvp.Is_Manual_Generation = 1 )
                                                      OR ( @commodity_Id = @commodity_Id_EP ) )
                                                OR ( tvp.Is_Manual_Generation IS NULL ) THEN tvp.Transportation_Value
                                           ELSE bd.TRANSPORTATION_VALUE
                                      END
           ,bd.TRANSMISSION = CASE WHEN ( bd.TRANSMISSION <> tvp.Transmission
                                          OR bd.Transmission IS NULL ) THEN tvp.Transmission
                                   ELSE bd.TRANSMISSION
                              END
           ,bd.TRANSMISSION_VALUE = CASE WHEN ( bd.TRANSMISSION_VALUE <> tvp.Transmission_Value
                                                OR bd.Transmission_Value IS NULL ) THEN tvp.Transmission_Value
                                         ELSE bd.TRANSMISSION_VALUE
                                    END
           ,bd.DISTRIBUTION = CASE WHEN ( bd.DISTRIBUTION <> tvp.Distribution
                                          OR bd.Distribution IS NULL ) THEN tvp.Distribution
                                   ELSE bd.DISTRIBUTION
                              END
           ,bd.DISTRIBUTION_VALUE = CASE WHEN ( bd.DISTRIBUTION_VALUE <> tvp.Distribution_Value
                                                OR bd.DISTRIBUTION_VALUE IS NULL ) THEN tvp.Distribution_Value
                                         ELSE bd.DISTRIBUTION_VALUE
                                    END
           ,bd.OTHER_BUNDLED = CASE WHEN ( bd.OTHER_BUNDLED <> tvp.Other_Bundled
                                           OR bd.OTHER_BUNDLED IS NULL ) THEN tvp.Other_Bundled
                                    ELSE bd.OTHER_BUNDLED
                               END
           ,bd.OTHER_BUNDLED_VALUE = CASE WHEN ( bd.OTHER_BUNDLED_VALUE <> tvp.Other_Bundled_Value
                                                 OR bd.OTHER_BUNDLED_VALUE IS NULL ) THEN tvp.Other_Bundled_Value
                                          ELSE bd.OTHER_BUNDLED_VALUE
                                     END
           ,bd.OTHER_FIXED_COSTS = CASE WHEN ( bd.OTHER_FIXED_COSTS <> tvp.Other_Fixed_Costs
                                               OR bd.OTHER_FIXED_COSTS IS NULL ) THEN tvp.Other_Fixed_Costs
                                        ELSE bd.OTHER_FIXED_COSTS
                                   END
           ,bd.OTHER_FIXED_COSTS_VALUE = CASE WHEN ( bd.OTHER_FIXED_COSTS_VALUE <> tvp.Other_Fixed_Costs_Value
                                                     OR bd.OTHER_FIXED_COSTS_VALUE IS NULL ) THEN tvp.Other_Fixed_Costs_Value
                                              ELSE bd.OTHER_FIXED_COSTS_VALUE
                                         END
           ,bd.SOURCING_TAX = CASE WHEN ( bd.SOURCING_TAX <> tvp.Sourcing_Tax
                                          OR bd.SOURCING_TAX IS NULL )
                                        AND ( tvp.Is_Manual_Sourcing_Tax = 1
                                              OR tvp.Is_Manual_Sourcing_Tax IS NULL ) THEN tvp.Sourcing_Tax
                                   ELSE bd.SOURCING_TAX
                              END
           ,bd.SOURCING_TAX_VALUE = CASE WHEN ( bd.SOURCING_TAX_VALUE <> tvp.Sourcing_Tax_Value
                                                OR bd.Sourcing_Tax_Value IS NULL )
                                              AND ( tvp.Is_Manual_Sourcing_Tax = 1
                                                    OR tvp.Is_Manual_Sourcing_Tax IS NULL ) THEN tvp.Sourcing_Tax_Value
                                         ELSE bd.SOURCING_TAX_VALUE
                                    END
           ,bd.RATES_TAX = CASE WHEN ( bd.RATES_TAX <> tvp.Rates_Tax
                                       OR bd.RATES_TAX IS NULL ) THEN tvp.Rates_Tax
                                ELSE bd.RATES_TAX
                           END
           ,bd.RATES_TAX_VALUE = CASE WHEN ( bd.RATES_TAX_VALUE <> tvp.Rates_Tax_Value
                                             OR bd.RATES_TAX_VALUE IS NULL ) THEN tvp.Rates_Tax_Value
                                      ELSE bd.RATES_TAX_VALUE
                                 END
           ,bd.IS_MANUAL_GENERATION = CASE WHEN ( bd.IS_MANUAL_GENERATION <> tvp.Is_Manual_Generation
                                                  OR bd.IS_MANUAL_GENERATION IS NULL ) THEN tvp.Is_Manual_Generation
                                           ELSE bd.IS_MANUAL_GENERATION
                                      END
           ,bd.Is_Manual_Sourcing_Tax = CASE WHEN ( bd.Is_Manual_Sourcing_Tax <> tvp.Is_Manual_Sourcing_Tax
                                                    OR bd.Is_Manual_Sourcing_Tax IS NULL ) THEN tvp.Is_Manual_Sourcing_Tax
                                             ELSE bd.Is_Manual_Sourcing_Tax
                                        END
           ,bd.BUDGET_USAGE = CASE WHEN ( bd.BUDGET_USAGE <> tvp.Budget_Usage
                                          OR bd.BUDGET_USAGE IS NULL ) THEN tvp.Budget_Usage
                                   ELSE bd.BUDGET_USAGE
                              END
      FROM
            dbo.BUDGET_DETAILS bd
            INNER JOIN @tvp_budget_details tvp
                  ON bd.BUDGET_ACCOUNT_ID = tvp.Budget_Account_ID
                     AND bd.Month_Identifier = tvp.Month_Identifier
            INNER JOIN dbo.BUDGET_ACCOUNT ba
                  ON ba.BUDGET_ACCOUNT_ID = tvp.Budget_Account_ID
      WHERE
            ba.IS_DELETED = 0    
                                             
      INSERT      INTO dbo.BUDGET_DETAIL_COMMENTS
                  ( 
                   BUDGET_ACCOUNT_ID
                  ,Variable_Comments
                  ,Transportation_Comments
                  ,Transmission_Comments
                  ,Distribution_Comments
                  ,Other_Bundled_Comments
                  ,Other_Fixed_Costs_Comments
                  ,Sourcing_Tax_Comments
                  ,Rates_Tax_Comments )
                  SELECT
                        tvpbdc.Budget_Account_ID
                       ,tvpbdc.Variable_Comments
                       ,tvpbdc.Transportation_Comments
                       ,tvpbdc.Transmission_Comments
                       ,tvpbdc.Distribution_Comments
                       ,tvpbdc.Other_Bundled_Comments
                       ,tvpbdc.Other_Fixed_Costs_Comments
                       ,tvpbdc.Sourcing_Tax_Comments
                       ,tvpbdc.Rates_Tax_Comments
                  FROM
                        @tvp_budget_detail_comments tvpbdc
                        INNER JOIN BUDGET_ACCOUNT ba
                              ON ba.BUDGET_ACCOUNT_ID = tvpbdc.Budget_Account_ID
                  WHERE
                        ba.IS_DELETED = 0
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.BUDGET_DETAIL_COMMENTS bdc
                                         WHERE
                                          bdc.BUDGET_ACCOUNT_ID = tvpbdc.Budget_Account_ID )                                        
   
                                        
      UPDATE
            bdc
      SET   
            bdc.VARIABLE_COMMENTS = CASE WHEN ( bdc.VARIABLE_COMMENTS <> tvpbdc.Variable_Comments
                                                OR bdc.VARIABLE_COMMENTS IS NULL ) THEN tvpbdc.Variable_Comments
                                         ELSE bdc.VARIABLE_COMMENTS
                                    END
           ,bdc.TRANSPORTATION_COMMENTS = CASE WHEN ( bdc.TRANSPORTATION_COMMENTS <> tvpbdc.Transportation_Comments
                                                      OR bdc.TRANSPORTATION_COMMENTS IS NULL ) THEN tvpbdc.Transportation_Comments
                                               ELSE bdc.TRANSPORTATION_COMMENTS
                                          END
           ,bdc.TRANSMISSION_COMMENTS = CASE WHEN ( bdc.TRANSMISSION_COMMENTS <> tvpbdc.Transmission_Comments
                                                    OR bdc.TRANSMISSION_COMMENTS IS NULL ) THEN tvpbdc.Transmission_Comments
                                             ELSE bdc.TRANSMISSION_COMMENTS
                                        END
           ,bdc.DISTRIBUTION_COMMENTS = CASE WHEN ( bdc.DISTRIBUTION_COMMENTS <> tvpbdc.Distribution_Comments
                                                    OR bdc.DISTRIBUTION_COMMENTS IS NULL ) THEN tvpbdc.Distribution_Comments
                                             ELSE bdc.DISTRIBUTION_COMMENTS
                                        END
           ,bdc.OTHER_BUNDLED_COMMENTS = CASE WHEN ( bdc.OTHER_BUNDLED_COMMENTS <> tvpbdc.Other_Bundled_Comments
                                                     OR bdc.OTHER_BUNDLED_COMMENTS IS NULL ) THEN tvpbdc.Other_Bundled_Comments
                                              ELSE bdc.OTHER_BUNDLED_COMMENTS
                                         END
           ,bdc.OTHER_FIXED_COSTS_COMMENTS = CASE WHEN ( bdc.OTHER_FIXED_COSTS_COMMENTS <> tvpbdc.Other_Fixed_Costs_Comments
                                                         OR bdc.OTHER_FIXED_COSTS_COMMENTS IS NULL ) THEN tvpbdc.Other_Fixed_Costs_Comments
                                                  ELSE bdc.OTHER_FIXED_COSTS_COMMENTS
                                             END
           ,bdc.SOURCING_TAX_COMMENTS = CASE WHEN ( bdc.SOURCING_TAX_COMMENTS <> tvpbdc.Sourcing_Tax_Comments
                                                    OR bdc.SOURCING_TAX_COMMENTS IS NULL ) THEN tvpbdc.Sourcing_Tax_Comments
                                             ELSE bdc.SOURCING_TAX_COMMENTS
                                        END
           ,bdc.RATES_TAX_COMMENTS = CASE WHEN ( bdc.RATES_TAX_COMMENTS <> tvpbdc.Rates_Tax_Comments
                                                 OR bdc.RATES_TAX_COMMENTS IS NULL ) THEN tvpbdc.Rates_Tax_Comments
                                          ELSE bdc.RATES_TAX_COMMENTS
                                     END
      FROM
            dbo.BUDGET_DETAIL_COMMENTS bdc
            INNER JOIN @tvp_budget_detail_comments tvpbdc
                  ON bdc.BUDGET_ACCOUNT_ID = tvpbdc.Budget_Account_ID
            INNER JOIN dbo.BUDGET_ACCOUNT ba
                  ON ba.BUDGET_ACCOUNT_ID = tvpbdc.Budget_Account_ID
      WHERE
            ba.IS_DELETED = 0                                         
  

      UPDATE
            dbo.BUDGET_ACCOUNT
      SET   
            Sourcing_Completed_date = CASE WHEN ( ba.SOURCING_COMPLETED_DATE IS NULL
                                                  AND tvpbdc.IsSourcingCompleted = 1 ) THEN GETDATE()
                                           WHEN ( tvpbdc.IsSourcingCompleted = 0 ) THEN NULL
                                           ELSE ( ba.SOURCING_COMPLETED_DATE )
                                      END
           ,Sourcing_Completed_By = CASE WHEN ( ba.SOURCING_COMPLETED_DATE IS NULL
                                                AND tvpbdc.IsSourcingCompleted = 1 ) THEN @userinfoId
                                         WHEN ( tvpbdc.IsSourcingCompleted = 0 ) THEN NULL
                                         ELSE ( ba.SOURCING_COMPLETED_BY )
                                    END
           ,Rates_Completed_Date = CASE WHEN ( ba.Rates_Completed_date IS NULL
                                               AND tvpbdc.IsRatesCompleted = 1 ) THEN GETDATE()
                                        WHEN ( tvpbdc.IsRatesCompleted = 0 ) THEN NULL
                                        ELSE ( ba.Rates_Completed_date )
                                   END
           ,Rates_Completed_By = CASE WHEN ( ba.Rates_Completed_date IS NULL
                                             AND tvpbdc.IsRatesCompleted = 1 ) THEN @userinfoId
                                      WHEN ( tvpbdc.IsRatesCompleted = 0 ) THEN NULL
                                      ELSE ( ba.RATES_COMPLETED_BY )
                                 END
           ,Rates_Reviewed_Date = CASE WHEN ( BA.Rates_Reviewed_Date IS NULL
                                              AND tvpbdc.IsRatesReviewCompleted = 1 ) THEN GETDATE()
                                       WHEN ( tvpbdc.IsRatesReviewCompleted = 0 ) THEN NULL
                                       ELSE ( BA.Rates_Reviewed_Date )
                                  END
           ,Rates_Reviewed_By = CASE WHEN ( BA.Rates_Reviewed_Date IS NULL
                                            AND tvpbdc.IsRatesReviewCompleted = 1 ) THEN @userinfoId
                                     WHEN ( tvpbdc.IsRatesReviewCompleted = 0 ) THEN NULL
                                     ELSE ( ba.RATES_REVIEWED_BY )
                                END
      FROM
            @tvp_budget_detail_comments tvpbdc
            INNER JOIN dbo.BUDGET_ACCOUNT ba
                  ON ba.BUDGET_ACCOUNT_ID = tvpbdc.Budget_Account_Id
      WHERE
            ba.IS_DELETED = 0    
            
            --->Rates Budget - Rates Completed(Analyst Complete) - Queue priority - First Budget Reviewer otherwise Regional Manager
      UPDATE
            baq
      SET   
            baq.Queue_Id = ISNULL(rev.QUEUE_ID, rm.QUEUE_ID)
           ,baq.Routed_By_User_Id = @userinfoId
           ,baq.Routed_Ts = GETDATE()
           ,baq.Last_Change_Ts = GETDATE()
      FROM
            dbo.Budget_Account_Queue baq
            INNER JOIN dbo.BUDGET_ACCOUNT ba
                  ON baq.Budget_Account_Id = ba.BUDGET_ACCOUNT_ID
            INNER JOIN @tvp_budget_detail_comments tvpbud
                  ON ba.BUDGET_ACCOUNT_ID = tvpbud.Budget_Account_ID
            INNER JOIN Core.Client_Hier_Account cha
                  ON ba.ACCOUNT_ID = cha.Account_Id
                                    ---RM Reveiwer
            LEFT JOIN ( dbo.UTILITY_DETAIL AS revud
                        INNER JOIN dbo.Utility_Detail_Analyst_Map revudam
                              ON revud.UTILITY_DETAIL_ID = revudam.Utility_Detail_ID
                        INNER JOIN dbo.GROUP_INFO revgi
                              ON revudam.Group_Info_ID = revgi.GROUP_INFO_ID
                                 AND revgi.GROUP_NAME = 'Budget Reviewer Queue'
                        INNER JOIN dbo.USER_INFO rev
                              ON revudam.Analyst_ID = rev.USER_INFO_ID )
                        ON revud.VENDOR_ID = cha.Account_Vendor_Id
                                    ---Regional Manager
            LEFT JOIN ( dbo.REGION_MANAGER_MAP AS rmm
                        INNER JOIN dbo.STATE AS s
                              ON rmm.REGION_ID = s.REGION_ID
                        INNER JOIN dbo.VENDOR_STATE_MAP AS vsm
                              ON s.STATE_ID = vsm.STATE_ID
                        INNER JOIN dbo.USER_INFO rm
                              ON rmm.USER_INFO_ID = rm.USER_INFO_ID )
                        ON vsm.VENDOR_ID = cha.Account_Vendor_Id
      WHERE
            baq.Analyst_Type_Cd = @Rates_Queue_Cd
            AND ba.Rates_Completed_date IS NULL
            AND tvpbud.IsRatesCompleted = 1
            AND ba.IS_DELETED = 0
            
            

--->Rates Budget - Reviewer un-checked "Rates completed" account, it goes back to analyst queue - Queue priority - First RM Analyst otherwise Regional Manager
      UPDATE
            baq
      SET   
            baq.Queue_Id = ISNULL(ra.QUEUE_ID, rm.QUEUE_ID)
           ,baq.Routed_By_User_Id = @userinfoId
           ,baq.Routed_Ts = GETDATE()
           ,baq.Last_Change_Ts = GETDATE()
      FROM
            dbo.Budget_Account_Queue baq
            INNER JOIN dbo.BUDGET_ACCOUNT ba
                  ON baq.Budget_Account_Id = ba.BUDGET_ACCOUNT_ID
            INNER JOIN @tvp_budget_detail_comments tvpbud
                  ON ba.BUDGET_ACCOUNT_ID = tvpbud.Budget_Account_ID
            INNER JOIN Core.Client_Hier_Account cha
                  ON ba.ACCOUNT_ID = cha.Account_Id
                                    ---RM Analyst
            LEFT JOIN ( dbo.UTILITY_DETAIL AS raud
                        INNER JOIN dbo.Utility_Detail_Analyst_Map raudam
                              ON raud.UTILITY_DETAIL_ID = raudam.Utility_Detail_ID
                        INNER JOIN dbo.GROUP_INFO ragi
                              ON raudam.Group_Info_ID = ragi.GROUP_INFO_ID
                        INNER JOIN @Group_Legacy_Name gil
                              ON ragi.GROUP_INFO_ID = gil.GROUP_INFO_ID
                        INNER JOIN dbo.USER_INFO ra
                              ON raudam.Analyst_ID = ra.USER_INFO_ID )
                        ON raud.VENDOR_ID = cha.Account_Vendor_Id
                                    ---Regional Manager
            LEFT JOIN ( dbo.REGION_MANAGER_MAP AS rmm
                        INNER JOIN dbo.STATE AS s
                              ON rmm.REGION_ID = s.REGION_ID
                        INNER JOIN dbo.VENDOR_STATE_MAP AS vsm
                              ON s.STATE_ID = vsm.STATE_ID
                        INNER JOIN dbo.USER_INFO rm
                              ON rmm.USER_INFO_ID = rm.USER_INFO_ID )
                        ON vsm.VENDOR_ID = cha.Account_Vendor_Id
      WHERE
            baq.Analyst_Type_Cd = @Rates_Queue_Cd
            AND ba.Rates_Completed_date IS NOT NULL
            AND tvpbud.IsRatesCompleted = 0
            AND ba.IS_DELETED = 0

--->Rates Budget - Rates Reviewed - Budget account no longer appears in any rates queue
      DELETE
            baq
      FROM
            dbo.Budget_Account_Queue baq
            INNER JOIN dbo.BUDGET_ACCOUNT ba
                  ON baq.Budget_Account_Id = ba.BUDGET_ACCOUNT_ID
            INNER JOIN @tvp_budget_detail_comments tvpbud
                  ON ba.BUDGET_ACCOUNT_ID = tvpbud.Budget_Account_ID
            INNER JOIN Core.Client_Hier_Account cha
                  ON ba.ACCOUNT_ID = cha.Account_Id
      WHERE
            baq.Analyst_Type_Cd = @Rates_Queue_Cd
            AND ba.Rates_Reviewed_Date IS NULL
            AND tvpbud.IsRatesReviewCompleted = 1
            
--->Sourcing Budget - Sourcing Complete - Budget account no longer appears in any sourcing queue
      DELETE
            baq
      FROM
            dbo.Budget_Account_Queue baq
            INNER JOIN dbo.BUDGET_ACCOUNT ba
                  ON baq.Budget_Account_Id = ba.BUDGET_ACCOUNT_ID
            INNER JOIN @tvp_budget_detail_comments tvpbud
                  ON ba.BUDGET_ACCOUNT_ID = tvpbud.Budget_Account_ID
            INNER JOIN Core.Client_Hier_Account cha
                  ON ba.ACCOUNT_ID = cha.Account_Id
      WHERE
            baq.Analyst_Type_Cd = @Sourcing_Queue_Cd
            AND ba.SOURCING_COMPLETED_DATE IS NULL
            AND tvpbud.IsSourcingCompleted = 1
            
            
            
      SELECT
            @User_Name = FIRST_NAME + SPACE(1) + LAST_NAME
      FROM
            dbo.USER_INFO
      WHERE
            USER_INFO_ID = @userinfoId  
            
      SELECT
            @Client_Name = ch.Client_Name
      FROM
            dbo.BUDGET_ACCOUNT ba
            INNER JOIN @tvp_budget_detail_comments tvpbud
                  ON ba.BUDGET_ACCOUNT_ID = tvpbud.Budget_Account_ID
            INNER JOIN Core.Client_Hier_Account cha
                  ON ba.ACCOUNT_ID = cha.Account_Id
            INNER JOIN Core.Client_Hier ch
                  ON cha.Client_Hier_Id = ch.Client_Hier_Id
     
            
      EXEC dbo.Application_Audit_Log_Ins 
            @Client_Name
           ,@Application_Name
           ,@Audit_Function
           ,'dbo.Budget_Account_Queue'
           ,@Lookup_Value
           ,@User_Name
           ,@Execution_Ts
                   
END;

;
GO

GRANT EXECUTE ON  [dbo].[BUDGET_ACCOUNT_UPD] TO [CBMSApplication]
GO
