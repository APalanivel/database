SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                
NAME:                                
    dbo.Notification_Msg_Dtl_Ids_Sel_By_Notification_Msg_Queue_Id                                
                                
DESCRIPTION:                                
   
                                
INPUT PARAMETERS:                                
 Name						DataType		Default Description                                
---------------------------------------------------------------
 @Keyword					VARCHAR(200)	NULL
 @StartIndex				INT				1
 @EndIndex					INT				2147483647
                          
                       
OUTPUT PARAMETERS:                                
 Name   DataType  Default Description                                
---------------------------------------------------------------    

	EXEC dbo.Notification_Msg_Dtl_Ids_Sel_By_Notification_Msg_Queue_Id 

	               
 
USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	NR			Narayana Reddy
                   
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	NR			2019-02-22  Created For Risk Managemnet.

******/
CREATE PROCEDURE [dbo].[Notification_Msg_Dtl_Ids_Sel_By_Notification_Msg_Queue_Id]
      ( 
       @Notification_Msg_Queue_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      SELECT
            nmd.Notification_Msg_Dtl_Id
      FROM
            dbo.Notification_Msg_Dtl nmd
      WHERE
            nmd.Notification_Msg_Queue_Id = @Notification_Msg_Queue_Id
      
      
      
      

END;
GO
GRANT EXECUTE ON  [dbo].[Notification_Msg_Dtl_Ids_Sel_By_Notification_Msg_Queue_Id] TO [CBMSApplication]
GO
