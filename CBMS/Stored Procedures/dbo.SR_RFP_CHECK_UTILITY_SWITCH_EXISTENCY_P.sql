SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_CHECK_UTILITY_SWITCH_EXISTENCY_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@isBidGroup    		int       	          	
	@srRFPAccountId		int       	          	
	@status VARCHAR(30) OUT
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

exec SR_RFP_CHECK_UTILITY_SWITCH_EXISTENCY_P 941,null,0
exec SR_RFP_CHECK_UTILITY_SWITCH_EXISTENCY_P 0,4731,null

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	SSR			09/27/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on	        	
******/
CREATE PROCEDURE [dbo].[SR_RFP_CHECK_UTILITY_SWITCH_EXISTENCY_P]
    @isBidGroup INT
  , @srRFPAccountId INT
  , @status VARCHAR(30) OUT
AS 
    BEGIN  
        SET nocount ON

        DECLARE @changeNotificationImageId INT
        SELECT
            @changeNotificationImageId = ISNULL(( SELECT
                                                    change_notice_image_id
                                                  FROM
                                                    sr_rfp_utility_switch
                                                  WHERE
                                                    sr_account_group_id = @srRFPAccountId
                                                    AND is_bid_group = @isBidGroup
                                                ), 0)
        DECLARE @flowVerificationImageId INT
        SELECT
            @flowVerificationImageId = ISNULL(( SELECT
                                                    flow_verification_image_id
                                                FROM
                                                    sr_rfp_utility_switch
                                                WHERE
                                                    sr_account_group_id = @srRFPAccountId
                                                    AND is_bid_group = @isBidGroup
                                              ), 0)

        DECLARE @supplierNotificationImageId INT
        SELECT
            @supplierNotificationImageId = ISNULL(( SELECT
                                                        supplier_notice_image_id
                                                    FROM
                                                        sr_rfp_utility_switch
                                                    WHERE
                                                        sr_account_group_id = @srRFPAccountId
                                                        AND is_bid_group = @isBidGroup
                                                  ), 0)
        DECLARE @accExistency INT
        SELECT
            @accExistency = ISNULL(( SELECT
                                        COUNT(1)
                                     FROM
                                        sr_rfp_utility_switch
                                     WHERE
                                        sr_account_group_id = @srRFPAccountId
                                        AND is_bid_group = @isBidGroup
                                   ), 0)
        IF ( ( @changeNotificationImageId > 0 )
             AND ( @flowVerificationImageId = 0 )
             AND ( @supplierNotificationImageId = 0 )
             AND ( @accExistency > 0 ) ) 
            BEGIN
                SELECT
                    @status = 'changeNotice'
	
            END
        ELSE 
            IF ( ( @changeNotificationImageId = 0 )
                 AND ( @flowVerificationImageId > 0 )
                 AND ( @supplierNotificationImageId = 0 )
                 AND ( @accExistency > 0 ) ) 
                BEGIN
                    SELECT
                        @status = 'flowVerification'
                END
            ELSE 
                IF ( ( @changeNotificationImageId = 0 )
                     AND ( @flowVerificationImageId = 0 )
                     AND ( @supplierNotificationImageId > 0 )
                     AND ( @accExistency > 0 ) ) 
                    BEGIN
                        SELECT
                            @status = 'supplierNotice'
                    END
                ELSE 
                    IF ( ( @changeNotificationImageId > 0 )
                         AND ( @flowVerificationImageId > 0 )
                         AND ( @supplierNotificationImageId = 0 )
                         AND ( @accExistency > 0 ) ) 
                        BEGIN
                            SELECT
                                @status = 'updateSupplierNotice'
                        END
                    ELSE 
                        IF ( ( @changeNotificationImageId > 0 )
                             AND ( @flowVerificationImageId = 0 )
                             AND ( @supplierNotificationImageId > 0 )
                             AND ( @accExistency > 0 ) ) 
                            BEGIN
                                SELECT
                                    @status = 'updateFlowVerification'
                            END
                        ELSE 
                            IF ( ( @changeNotificationImageId = 0 )
                                 AND ( @flowVerificationImageId > 0 )
                                 AND ( @supplierNotificationImageId > 0 )
                                 AND ( @accExistency > 0 ) ) 
                                BEGIN
                                    SELECT
                                        @status = 'updateChangeNotice'
                                END


                            ELSE 
                                IF ( ( @changeNotificationImageId > 0 )
                                     AND ( @flowVerificationImageId > 0 )
                                     AND ( @supplierNotificationImageId > 0 ) ) 
                                    BEGIN
                                        SELECT
                                            @status = 'update'	
                                    END
                                ELSE 
                                    IF ( ( @accExistency > 0 )
                                         AND ( @changeNotificationImageId = 0 )
                                         AND ( @flowVerificationImageId = 0 )
                                         AND ( @supplierNotificationImageId = 0 ) ) 
                                        BEGIN
                                            SELECT
                                                @status = 'updateDates'	
                                        END 
                                    ELSE 
                                        BEGIN
                                            SELECT
                                                @status = 'insert'
                                        END

    END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_CHECK_UTILITY_SWITCH_EXISTENCY_P] TO [CBMSApplication]
GO
