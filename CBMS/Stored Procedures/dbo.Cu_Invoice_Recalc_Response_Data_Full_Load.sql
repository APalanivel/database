
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                                              
                                    
NAME: [dbo].[Cu_Invoice_Recalc_Response_Data_Full_Load]                                   
                                    
DESCRIPTION:                                     
      To get all the recalculated charges from CBMS.

INPUT PARAMETERS:                                              
NAME	DATATYPE		DEFAULT			DESCRIPTION
------------------------------------------------------------                                              
                             
                                                    
OUTPUT PARAMETERS:                                              
NAME	DATATYPE		DEFAULT			DESCRIPTION                                       
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

 select top 10
      *
 from
      Cu_Invoice_Recalc_Response

 EXEC Cu_Invoice_Recalc_Response_Data_Full_Load1

AUTHOR INITIALS:  
INITIALS	NAME
------------------------------------------------------------
RKV			Ravi Kumar Vegesna

MODIFICATIONS
INITIALS	DATE		MODIFICATION            
------------------------------------------------------------
RKV			2016-02-26  Created for As400
*/
CREATE PROCEDURE [dbo].[Cu_Invoice_Recalc_Response_Data_Full_Load]
AS 
BEGIN

      SET NOCOUNT ON;

	/*****************************************************************************************
   This is here entirely as a work around for SSIS and temporary tables.  This statement
   allows SSIS to be able to return the meta data about the stored procedure to map columns.
*****************************************************************************************/   
      IF 1 = 2 
            BEGIN

                  SELECT
                        CAST(NULL AS INT) AS Client_Hier_Id
                       ,CAST(NULL AS INT) AS ACCOUNT_ID
                       ,CAST(NULL AS INT) AS Commodity_Id
                       ,CAST(NULL AS DATE) AS SERVICE_MONTH
                       ,CAST(NULL AS INT) AS Cu_Invoice_Recalc_Response_Id
                       ,CAST(NULL AS NVARCHAR(200)) AS Charge_Name
                       ,CAST(NULL AS INT) AS Bucket_Master_Id
                       ,CAST(NULL AS BIT) AS Is_Hidden_On_RA
                       ,CAST(NULL AS DECIMAL(28, 10)) AS Net_Amount
                       ,CAST(NULL AS INT) AS Net_Amt_Currency_Unit_Id
                       ,CAST(NULL AS INT) AS Updated_User_Id
                       ,CAST(NULL AS CHAR(1)) AS Sys_Change_Operation;
            END;

      CREATE TABLE #Recalc_Data
            ( 
             Client_Hier_Id INT
            ,ACCOUNT_ID INT
            ,Commodity_Id INT
            ,SERVICE_MONTH DATE
            ,CU_INVOICE_ID INT
            ,Cu_Invoice_Recalc_Response_Id INT
            ,Charge_Name NVARCHAR(200)
            ,Bucket_Master_Id INT
            ,Calculator_Working NVARCHAR(MAX)
            ,Created_User_Id INT
            ,Determinant_Unit NVARCHAR(200)
            ,Determinant_Value DECIMAL(28, 10)
            ,Is_Hidden_On_RA BIT
            ,Net_Amount DECIMAL(28, 10)
            ,Net_Amt_Currency_Unit_Id INT
            ,Rate_Amount DECIMAL(28, 10)
            ,Rate_Currency NVARCHAR(200)
            ,Rate_Currency_Unit_Id INT
            ,Rate_Unit NVARCHAR(200)
            ,Rate_Uom_Type_Id INT
            ,Recalc_Response_Source_Cd INT
            ,Updated_User_Id INT
            ,Sys_Change_Operation CHAR(1) );
      CREATE CLUSTERED INDEX #cx#Recalc_Data ON #Recalc_Data(Cu_Invoice_Recalc_Response_Id);

      CREATE TABLE #Month_Cnt
            ( 
             Cu_Invoice_Recalc_Response_Id INT
            ,Month_Cnt INT );

      INSERT      INTO #Recalc_Data
                  ( 
                   Client_Hier_Id
                  ,ACCOUNT_ID
                  ,Commodity_Id
                  ,SERVICE_MONTH
                  ,CU_INVOICE_ID
                  ,Cu_Invoice_Recalc_Response_Id
                  ,Charge_Name
                  ,Bucket_Master_Id
                  ,Calculator_Working
                  ,Created_User_Id
                  ,Determinant_Unit
                  ,Determinant_Value
                  ,Is_Hidden_On_RA
                  ,Net_Amount
                  ,Net_Amt_Currency_Unit_Id
                  ,Rate_Amount
                  ,Rate_Currency
                  ,Rate_Currency_Unit_Id
                  ,Rate_Unit
                  ,Rate_Uom_Type_Id
                  ,Recalc_Response_Source_Cd
                  ,Updated_User_Id
                  ,Sys_Change_Operation )
                  SELECT
                        cha.Client_Hier_Id
                       ,rh.ACCOUNT_ID
                       ,rh.Commodity_Id
                       ,cism.SERVICE_MONTH
                       ,cism.CU_INVOICE_ID
                       ,cirr.Cu_Invoice_Recalc_Response_Id
                       ,cirr.Charge_Name
                       ,cirr.Bucket_Master_Id
                       ,cirr.Calculator_Working
                       ,cirr.Created_User_Id
                       ,cirr.Determinant_Unit
                       ,cirr.Determinant_Value
                       ,cirr.Is_Hidden_On_RA
                       ,cirr.Net_Amount
                       ,cirr.Net_Amt_Currency_Unit_Id
                       ,cirr.Rate_Amount
                       ,cirr.Rate_Currency
                       ,cirr.Rate_Currency_Unit_Id
                       ,cirr.Rate_Unit
                       ,cirr.Rate_Uom_Type_Id
                       ,cirr.Recalc_Response_Source_Cd
                       ,ISNULL(cirr.Updated_User_Id, cirr.Created_User_Id) Updated_User_Id
                       ,N'I' Sys_Change_Operation
                  FROM
                        dbo.Cu_Invoice_Recalc_Response cirr
                        INNER JOIN dbo.RECALC_HEADER rh
                              ON cirr.Recalc_Header_Id = rh.RECALC_HEADER_ID
                        INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                              ON rh.CU_INVOICE_ID = cism.CU_INVOICE_ID
                                 AND rh.ACCOUNT_ID = cism.Account_ID
                        INNER JOIN dbo.CU_INVOICE ci
                              ON cism.CU_INVOICE_ID = ci.CU_INVOICE_ID
                        INNER JOIN Core.Client_Hier_Account cha
                              ON cha.Account_Id = cism.Account_ID
                  WHERE
                        cism.SERVICE_MONTH IS NOT NULL
                        AND ci.IS_PROCESSED = 1
                        AND ci.IS_REPORTED = 1
                        AND cirr.Is_Hidden_On_RA = 0
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.CU_EXCEPTION_DENORM ced
                                         WHERE
                                          ced.CU_INVOICE_ID = rh.CU_INVOICE_ID
                                          AND ced.Account_ID = rh.Account_Id
                                          AND ced.Commodity_Id = rh.Commodity_Id
                                          AND ced.SERVICE_MONTH = cism.SERVICE_MONTH
                                          AND ced.EXCEPTION_TYPE = 'Failed Recalc' )
                  GROUP BY
                        cha.Client_Hier_Id
                       ,rh.ACCOUNT_ID
                       ,rh.Commodity_Id
                       ,cism.SERVICE_MONTH
                       ,cirr.Cu_Invoice_Recalc_Response_Id
                       ,cirr.Charge_Name
                       ,cirr.Bucket_Master_Id
                       ,cirr.Calculator_Working
                       ,cirr.Created_User_Id
                       ,cirr.Determinant_Unit
                       ,cirr.Determinant_Value
                       ,cirr.Is_Hidden_On_RA
                       ,cirr.Net_Amount
                       ,cirr.Net_Amt_Currency_Unit_Id
                       ,cirr.Rate_Amount
                       ,cirr.Rate_Currency
                       ,cirr.Rate_Currency_Unit_Id
                       ,cirr.Rate_Unit
                       ,cirr.Rate_Uom_Type_Id
                       ,cirr.Recalc_Response_Source_Cd
                       ,cism.CU_INVOICE_ID
                       ,cirr.Updated_User_Id;

      INSERT      INTO #Month_Cnt
                  ( 
                   Cu_Invoice_Recalc_Response_Id
                  ,Month_Cnt )
                  SELECT
                        Cu_Invoice_Recalc_Response_Id
                       ,NULLIF(COUNT(DISTINCT SERVICE_MONTH), 0)
                  FROM
                        #Recalc_Data
                  GROUP BY
                        Cu_Invoice_Recalc_Response_Id;

      UPDATE
            rd
      SET   
            rd.Net_Amount = rd.Net_Amount / mc.Month_Cnt
      FROM
            #Recalc_Data rd
            INNER JOIN #Month_Cnt mc
                  ON mc.Cu_Invoice_Recalc_Response_Id = rd.Cu_Invoice_Recalc_Response_Id;

      SELECT
            Client_Hier_Id
           ,ACCOUNT_ID
           ,Commodity_Id
           ,SERVICE_MONTH
           ,Cu_Invoice_Recalc_Response_Id
           ,Charge_Name
           ,Bucket_Master_Id
           ,Is_Hidden_On_RA
           ,Net_Amount
           ,Net_Amt_Currency_Unit_Id
           ,Updated_User_Id
           ,Sys_Change_Operation
      FROM
            #Recalc_Data;
            
      DROP TABLE #Recalc_Data;
      DROP TABLE #Month_Cnt;
END;

;
;
GO

GRANT EXECUTE ON  [dbo].[Cu_Invoice_Recalc_Response_Data_Full_Load] TO [CBMSApplication]
GO
