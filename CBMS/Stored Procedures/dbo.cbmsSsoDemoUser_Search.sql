SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/******  
NAME:  
 
 dbo.cbmsSsoDemoUser_Search
 
 DESCRIPTION:

 INPUT PARAMETERS:

 Name			DataType		Default		Description
------------------------------------------------------------
@username		VARCHAR(30)		NULL
@last_name		VARCHAR(40)		NULL
@first_name		VARCHAR(40)		NULL
@is_history		bit				0
@access_level	int				0
@prospect_name	VARCHAR(300)	NULL

 OUTPUT PARAMETERS:

 Name   DataType  Default Description
------------------------------------------------------------

 
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.cbmsSsoDemoUser_Search @Last_Name = 'Baker'

	EXEC dbo.cbmsSsoDemoUser_Search @Is_History = 1
	EXEC dbo.cbmsSsoDemoUser_Search
	
	EXEC dbo.cbmsSsoDemoUser_Search @prospect_name= 'Abbott Laboratories'


AUTHOR INITIALS:

Initials	Name
------------------------------------------------------------
 HG			Harihara Suthan G

MODIFICATIONS   
 Initials	Date			Modification
------------------------------------------------------------
 HG			11/09/2010		Comments header added
							Removed the passcode column from the select list as it is not used by the applcation , changes made as a prt dv/sv users pwd expiration enhancement.
******/

CREATE PROCEDURE dbo.cbmsSsoDemoUser_Search 
( 
	@username			VARCHAR(30)		= NULL
	, @last_name		VARCHAR(40)		= NULL
	, @first_name		VARCHAR(40)		= NULL
	, @is_history		BIT				= 0
	, @access_level		INT				= 0
	, @prospect_name	VARCHAR(300)	= NULL
)
AS
BEGIN

	SET NOCOUNT ON

	SET @username = '%' + @username + '%'
	SET @last_name = '%' + @last_name+ '%'
	SET @first_name = '%' + @first_name + '%'
	SET @prospect_name = '%' + @prospect_name + '%'

	SELECT
		u.user_info_id
		, u.username
		, u.queue_id
		, u.first_name
		, u.middle_name
		, u.last_name
		, u.email_address
		, u.is_history
		, u.access_level
		, u.client_id
		, u.division_id
		, u.site_id
		, c.client_name
		, d.division_name
		, s.site_name
		, u.prospect_name
		, u.expiration_date
	FROM
		dbo.USER_INFO u
		LEFT OUTER JOIN dbo.CLIENT c
			ON c.client_id = u.client_id
		LEFT OUTER JOIN dbo.Division d
			ON d.division_id = u.division_id
		LEFT OUTER JOIN dbo.SITE s
			ON s.site_id = u.site_id
	WHERE
		(@username IS NULL OR u.username LIKE @username)
		AND (@last_name IS NULL OR u.last_name LIKE @last_name)
		AND (@first_name IS NULL OR u.first_name LIKE @first_name)
		AND (@prospect_name IS NULL OR u.prospect_name LIKE @prospect_name)
		AND (u.is_history = @is_history)
		AND (u.access_level >= @access_level)
		AND (u.is_demo_user = 1)
	ORDER BY
		u.last_name
		, u.first_name

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoDemoUser_Search] TO [CBMSApplication]
GO
