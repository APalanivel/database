SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[seDrillingActivity_Get]
	(
		@DrillingActivityId int
	)
As
BEGIN
	set nocount on
	 select DrillingActivityId
				, ReportDate
				, Volume
		 from seDrillingActivity
		where DrillingActivityId = @DrillingActivityId
END
GO
GRANT EXECUTE ON  [dbo].[seDrillingActivity_Get] TO [CBMSApplication]
GO
