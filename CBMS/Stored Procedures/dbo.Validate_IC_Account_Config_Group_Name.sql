SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********    
NAME:  dbo.Validate_IC_Account_Config_Group_Name    
    
DESCRIPTION:      
    
 Validates the Group name when the user trying to update the existing group name   
 @Update_Status = 0 'Success'
 @Update_Status = 1 'Inactive Chase'
 @Update_Status = 1 'Group already mapped with the same name'

    
INPUT PARAMETERS:    
      Name        DataType  Default     Description    
-----------------------------------------------------------------------------------    
   @Invoice_Collection_Account_Config_Id   INT    
   @Invoice_Collection_Account_Config_Group_Name      nvarchar    
        
    
OUTPUT PARAMETERS:    
      Name        DataType          Default     Description    
------------------------------------------------------------    
    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
 EXEC dbo.Validate_IC_Account_Config_Group_Name 82530,'Test1'
 
    
AUTHOR INITIALS:    
 Initials Name    
------------------------------------------------------------    
 RKV   Ravi kumar vegesna   
    
    
 Initials Date  Modification    
------------------------------------------------------------    
 RKV 2019-10-29 Created    
    
******/

CREATE PROCEDURE [dbo].[Validate_IC_Account_Config_Group_Name]
     (
         @Invoice_Collection_Account_Config_Id INT
         , @Invoice_Collection_Account_Config_Group_Name NVARCHAR(255)
     )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Validate_Account_Group_Name TABLE
              (
                  Invoice_Collection_Account_Config_Id INT
                  , Update_Status INT
              );


        INSERT INTO @Validate_Account_Group_Name
             (
                 Invoice_Collection_Account_Config_Id
                 , Update_Status
             )
        SELECT  @Invoice_Collection_Account_Config_Id, 0;


        -- Update_Status = 1 for Inactive Chase Accounts
        UPDATE
            vagn
        SET
            Update_Status = 1
        FROM
            @Validate_Account_Group_Name vagn
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icac.Invoice_Collection_Account_Config_Id = vagn.Invoice_Collection_Account_Config_Id
        WHERE
            icac.Is_Chase_Activated = 0
            AND vagn.Update_Status = 0;

        -- Update_Status = 2 for already group is assigned with the same name
        UPDATE
            vagn
        SET
            Update_Status = 2
        FROM
            @Validate_Account_Group_Name vagn
            INNER JOIN dbo.Invoice_Collection_Account_Config_Group_Map icacgm
                ON icacgm.Invoice_Collection_Account_Config_Id = vagn.Invoice_Collection_Account_Config_Id
            INNER JOIN dbo.Invoice_Collection_Account_Config_Group icacg
                ON icacg.Invoice_Collection_Account_Config_Group_ID = icacgm.Invoice_Collection_Account_Config_Group_Id
        WHERE
            icacg.Invoice_Collection_Account_Config_Group_name = @Invoice_Collection_Account_Config_Group_Name
            AND vagn.Update_Status = 0;


        SELECT
            Invoice_Collection_Account_Config_Id
            , Update_Status
        FROM
            @Validate_Account_Group_Name;

    END;
GO
GRANT EXECUTE ON  [dbo].[Validate_IC_Account_Config_Group_Name] TO [CBMSApplication]
GO
