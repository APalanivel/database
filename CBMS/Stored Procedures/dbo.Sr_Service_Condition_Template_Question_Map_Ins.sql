SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Service_Condition_Template_Question_Map_Ins]
           
DESCRIPTION:             
			To insert service condition template's category and question mapping details
			
INPUT PARAMETERS:            
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  
	@Sr_Service_Condition_Template_Id	INT
    @Sr_Service_Condition_Category_Id	INT
    @Sr_Service_Condition_Question_Id	INT
    @User_Info_Id						INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
		SELECT TOP 2 * FROM dbo.Sr_Service_Condition_Category 
        SELECT TOP 2 * FROM dbo.Sr_Service_Condition_Question
        SELECT TOP 2 * FROM dbo.Sr_Service_Condition_Template 

	BEGIN TRAN
		SELECT * FROM dbo.Sr_Service_Condition_Template_Question_Map a 
			INNER JOIN dbo.Sr_Service_Condition_Template_Category_Map b 
				ON a.Sr_Service_Condition_Template_Category_Map_Id = b.Sr_Service_Condition_Template_Category_Map_Id
			WHERE a.Sr_Service_Condition_Question_Id = 5 AND b.Sr_Service_Condition_Template_Id = 4 
			AND b.Sr_Service_Condition_Template_Id = 5
		
        EXEC dbo.Sr_Service_Condition_Template_Question_Map_Ins 
			@Sr_Service_Condition_Template_Id = 5
			,@Sr_Service_Condition_Category_Id = 4
			,@Sr_Service_Condition_Question_Id = 5
			,@User_Info_Id = 16
			,@Display_Seq = 5
		
		SELECT * FROM dbo.Sr_Service_Condition_Template_Question_Map a 
			INNER JOIN dbo.Sr_Service_Condition_Template_Category_Map b 
				ON a.Sr_Service_Condition_Template_Category_Map_Id = b.Sr_Service_Condition_Template_Category_Map_Id
			WHERE a.Sr_Service_Condition_Question_Id = 5 AND b.Sr_Service_Condition_Category_Id = 4 
			AND b.Sr_Service_Condition_Template_Id = 5
	ROLLBACK
		
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-02-22	Global Sourcing - Phase3 - GCS-448 Created
******/
CREATE PROCEDURE [dbo].[Sr_Service_Condition_Template_Question_Map_Ins]
      ( 
       @Sr_Service_Condition_Template_Id INT
      ,@Sr_Service_Condition_Category_Id INT
      ,@Sr_Service_Condition_Question_Id INT
      ,@User_Info_Id INT
      ,@Display_Seq INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE
            @Sr_Service_Condition_Template_Category_Map_Id INT
           ,@Category_Display_Seq INT
           ,@Question_Display_Seq INT
           
      SELECT
            @Category_Display_Seq = isnull(max(ctcm.Display_Seq), 0) + 1
      FROM
            dbo.Sr_Service_Condition_Template_Category_Map ctcm
      WHERE
            ctcm.Sr_Service_Condition_Template_Id = @Sr_Service_Condition_Template_Id
            
            
      MERGE INTO dbo.Sr_Service_Condition_Template_Category_Map Tgt
            USING 
                  ( SELECT
                        @Sr_Service_Condition_Template_Id AS Sr_Service_Condition_Template_Id
                       ,@Sr_Service_Condition_Category_Id AS Sr_Service_Condition_Category_Id
                       ,@Category_Display_Seq AS Display_Seq
                       ,@User_Info_Id AS User_Info_Id ) src
            ON src.Sr_Service_Condition_Template_Id = tgt.Sr_Service_Condition_Template_Id
                  AND src.Sr_Service_Condition_Category_Id = tgt.Sr_Service_Condition_Category_Id
            WHEN MATCHED 
                  THEN
					UPDATE
                    SET 
                        tgt.Display_Seq = src.Display_Seq
                       ,tgt.Updated_User_Id = src.User_Info_Id
                       ,tgt.Last_Change_Ts = getdate()
            WHEN NOT MATCHED 
                  THEN
					INSERT
                        ( 
                         Sr_Service_Condition_Template_Id
                        ,Sr_Service_Condition_Category_Id
                        ,Display_Seq
                        ,Created_User_Id
                        ,Created_Ts
                        ,Updated_User_Id
                        ,Last_Change_Ts )
                    VALUES
                        ( 
                         src.Sr_Service_Condition_Template_Id
                        ,src.Sr_Service_Condition_Category_Id
                        ,src.Display_Seq
                        ,src.User_Info_Id
                        ,getdate()
                        ,src.User_Info_Id
                        ,getdate() );
      
      SELECT
            @Sr_Service_Condition_Template_Category_Map_Id = Sr_Service_Condition_Template_Category_Map_Id
      FROM
            dbo.Sr_Service_Condition_Template_Category_Map ssctcm
      WHERE
            ssctcm.Sr_Service_Condition_Category_Id = @Sr_Service_Condition_Category_Id
            AND ssctcm.Sr_Service_Condition_Template_Id = @Sr_Service_Condition_Template_Id
      
      SELECT
            @Question_Display_Seq = isnull(max(tqm.Display_Seq), 0) + 1
      FROM
            dbo.Sr_Service_Condition_Template_Question_Map tqm
      WHERE
            tqm.Sr_Service_Condition_Template_Category_Map_Id = @Sr_Service_Condition_Template_Category_Map_Id
            
      DELETE
            tqm
      FROM
            dbo.Sr_Service_Condition_Template_Question_Map tqm
            INNER JOIN dbo.Sr_Service_Condition_Template_Category_Map tcm
                  ON tqm.Sr_Service_Condition_Template_Category_Map_Id = tcm.Sr_Service_Condition_Template_Category_Map_Id
      WHERE
            tqm.Sr_Service_Condition_Question_Id = @Sr_Service_Condition_Question_Id
            AND tcm.Sr_Service_Condition_Template_Id = @Sr_Service_Condition_Template_Id
            AND tqm.Sr_Service_Condition_Template_Category_Map_Id <> @Sr_Service_Condition_Template_Category_Map_Id
            
    
      MERGE INTO dbo.Sr_Service_Condition_Template_Question_Map Tgt
            USING 
                  ( SELECT
                        @Sr_Service_Condition_Template_Category_Map_Id AS Sr_Service_Condition_Template_Category_Map_Id
                       ,@Sr_Service_Condition_Question_Id AS Sr_Service_Condition_Question_Id
                       ,@Question_Display_Seq AS Display_Seq
                       ,@User_Info_Id AS User_Info_Id ) src
            ON src.Sr_Service_Condition_Template_Category_Map_Id = tgt.Sr_Service_Condition_Template_Category_Map_Id
                  AND src.Sr_Service_Condition_Question_Id = tgt.Sr_Service_Condition_Question_Id
            WHEN MATCHED 
                  THEN
					UPDATE
                    SET 
                        tgt.Display_Seq = src.Display_Seq
                       ,tgt.Updated_User_Id = src.User_Info_Id
                       ,tgt.Last_Change_Ts = getdate()
            WHEN NOT MATCHED 
                  THEN
					INSERT
                        ( 
                         Sr_Service_Condition_Template_Category_Map_Id
                        ,Sr_Service_Condition_Question_Id
                        ,Display_Seq
                        ,Created_User_Id
                        ,Created_Ts
                        ,Updated_User_Id
                        ,Last_Change_Ts )
                    VALUES
                        ( 
                         src.Sr_Service_Condition_Template_Category_Map_Id
                        ,src.Sr_Service_Condition_Question_Id
                        ,src.Display_Seq
                        ,src.User_Info_Id
                        ,getdate()
                        ,src.User_Info_Id
                        ,getdate() );
                        
                        
      
            
      DELETE
            tcm
      FROM
            dbo.Sr_Service_Condition_Template_Category_Map tcm
      WHERE
            tcm.Sr_Service_Condition_Template_Id = @Sr_Service_Condition_Template_Id
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              dbo.Sr_Service_Condition_Template_Question_Map tqm
                              INNER JOIN dbo.Sr_Service_Condition_Template_Category_Map tcm1
                                    ON tqm.Sr_Service_Condition_Template_Category_Map_Id = tcm1.Sr_Service_Condition_Template_Category_Map_Id
                             WHERE
                              tcm.Sr_Service_Condition_Template_Id = tcm1.Sr_Service_Condition_Template_Id
                              AND tqm.Sr_Service_Condition_Template_Category_Map_Id = tcm.Sr_Service_Condition_Template_Category_Map_Id )    
            
      
            
END;
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Service_Condition_Template_Question_Map_Ins] TO [CBMSApplication]
GO
