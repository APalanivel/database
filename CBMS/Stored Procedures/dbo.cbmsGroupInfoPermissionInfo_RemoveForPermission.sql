SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.[cbmsGroupInfoPermissionInfo_RemoveForPermission]


DESCRIPTION: 


INPUT PARAMETERS:    
      Name                  DataType          Default     Description    
------------------------------------------------------------------    
	 @MyAccountId              int
	 @permission_info_id       int
        
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
 ---- Remove the new row inserted
 -- exec cbmsGroupInfoPermissionInfo_RemoveForPermissionX
	-- @MyAccountId = 1
	--, @permission_info_id = 348


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates
	   DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[cbmsGroupInfoPermissionInfo_RemoveForPermission]
	( @MyAccountId int
	, @permission_info_id int
	)
AS

BEGIN

	SET NOCOUNT ON

	  DELETE 
				group_info_permission_info_map
	  WHERE 
				permission_info_id = @permission_info_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsGroupInfoPermissionInfo_RemoveForPermission] TO [CBMSApplication]
GO
