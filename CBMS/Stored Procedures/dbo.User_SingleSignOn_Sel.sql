
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:			dbo.User_SingleSignOn_Sel
DESCRIPTION:	Selects the SingleSignOn Credentials for the specified user/client 

INPUT PARAMETERS:
	Name				DataType			Default	Description
-----------------------------------------------------------------
	@user_info_id		INT
	@client_id			INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
EXEC dbo.User_SingleSignOn_Sel 33864, 11278


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	KH			Kevin Horton
	TP			Anoop
	
MODIFICATIONS

	Initials	Date			Modification
------------------------------------------------------------
	KH			01-Nov-2012		Created
	TP			28-Jun-2017		MAINT-5464 - removed Signon_Provider_Cd column		
			
******/
CREATE PROCEDURE [dbo].[User_SingleSignOn_Sel]
      ( 
       @user_info_id INT
      ,@client_id INT )
AS 
BEGIN

      SET NOCOUNT ON;
	
      SELECT
            ssum.Internal_User_ID [user_info_id]
           ,ssum.Client_ID [client_id]
           ,ssum.Provider_XUser_Name
      FROM
            dbo.Single_Signon_User_Map ssum
      WHERE
            ssum.Internal_User_ID = @user_info_id
            AND ssum.Client_ID = @client_id
END;


;
GO

GRANT EXECUTE ON  [dbo].[User_SingleSignOn_Sel] TO [CBMSApplication]
GO
