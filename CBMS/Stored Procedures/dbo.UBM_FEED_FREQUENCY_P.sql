SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.UBM_FEED_FREQUENCY_P
	@client_name varchar(100)
	AS
	set nocount on
	select freqName.entity_name FREQUENCY
	     from ubm_feed_frequency freq 
		left join entity freqName 
			on freqName.entity_id = freq.frequency_type_id 
	where freq.client_name = @client_name
GO
GRANT EXECUTE ON  [dbo].[UBM_FEED_FREQUENCY_P] TO [CBMSApplication]
GO
