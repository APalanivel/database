SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******               
                           
NAME:  dbo.Sr_Rfp_Sel_Open_Accounts_Term_Product_Dtls_Sel_By_Rfp                        
                              
DESCRIPTION:  
			To get RFP open accounts                             
                               
                              
INPUT PARAMETERS:              
 Name					DataType			Default					Description              
-----------------------------------------------------------------------------------  
                                    
OUTPUT PARAMETERS:                   
 Name					DataType			Default					Description              
-----------------------------------------------------------------------------------  
                              

USAGE EXAMPLES:              
-----------------------------------------------------------------------------------  
  
	EXEC dbo.Sr_Rfp_Sel_Open_Accounts_Term_Product_Dtls_Sel_By_Rfp 13300
	EXEC dbo.Sr_Rfp_Sel_Open_Accounts_Term_Product_Dtls_Sel_By_Rfp 13260
    EXEC dbo.Sr_Rfp_Sel_Open_Accounts_Term_Product_Dtls_Sel_By_Rfp 13154
  
  
 AUTHOR INITIALS:              
             
	Initials	Name              
-----------------------------------------------------------------------------------  
	RR			Raghu Reddy                                    
                               
 MODIFICATIONS:            
             
	Initials	Date        Modification            
-----------------------------------------------------------------------------------  
	RR          2016-04-14  GCS-684 Created
                             
******/ 

CREATE PROCEDURE [dbo].[Sr_Rfp_Sel_Open_Accounts_Term_Product_Dtls_Sel_By_Rfp] ( @Sr_Rfp_Id INT )
AS 
BEGIN  
      SET NOCOUNT ON   
      
      
      SELECT
            sra.ACCOUNT_ID
           ,SUBSTRING(DATENAME(month, term.FROM_MONTH), 1, 3) + '-' + SUBSTRING(DATENAME(year, term.FROM_MONTH), 3, 4) FROM_TERM
           ,SUBSTRING(DATENAME(month, term.TO_MONTH), 1, 3) + '-' + SUBSTRING(DATENAME(year, term.TO_MONTH), 3, 4) TO_TERM
           ,srpp.PRODUCT_NAME
      FROM
            dbo.SR_RFP_ACCOUNT sra
            INNER JOIN dbo.ENTITY sts
                  ON sra.BID_STATUS_TYPE_ID = sts.ENTITY_ID
            INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                  ON sra.SR_RFP_ACCOUNT_ID = term.SR_ACCOUNT_GROUP_ID
            INNER JOIN dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                  ON term.SR_RFP_ACCOUNT_TERM_ID = tpm.SR_RFP_ACCOUNT_TERM_ID
            INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                  ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
            INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                  ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
      WHERE
            sra.SR_RFP_ID = @Sr_Rfp_Id
            AND sts.ENTITY_NAME IN ( 'Open', 'New' )
            AND sts.ENTITY_DESCRIPTION = 'BID_STATUS'
            AND term.IS_BID_GROUP = 0
      UNION
      SELECT
            sra.ACCOUNT_ID
           ,SUBSTRING(DATENAME(month, term.FROM_MONTH), 1, 3) + '-' + SUBSTRING(DATENAME(year, term.FROM_MONTH), 3, 4) FROM_TERM
           ,SUBSTRING(DATENAME(month, term.TO_MONTH), 1, 3) + '-' + SUBSTRING(DATENAME(year, term.TO_MONTH), 3, 4) TO_TERM
           ,srpp.PRODUCT_NAME
      FROM
            dbo.SR_RFP_ACCOUNT sra
            INNER JOIN dbo.ENTITY sts
                  ON sra.BID_STATUS_TYPE_ID = sts.ENTITY_ID
            INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                  ON sra.SR_RFP_BID_GROUP_ID = term.SR_ACCOUNT_GROUP_ID
            INNER JOIN dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                  ON term.SR_RFP_ACCOUNT_TERM_ID = tpm.SR_RFP_ACCOUNT_TERM_ID
            INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                  ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
            INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                  ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
      WHERE
            sra.SR_RFP_ID = @Sr_Rfp_Id
            AND sts.ENTITY_NAME IN ( 'Open', 'New' )
            AND sts.ENTITY_DESCRIPTION = 'BID_STATUS'
            AND term.IS_BID_GROUP = 1
   
END  

;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Sel_Open_Accounts_Term_Product_Dtls_Sel_By_Rfp] TO [CBMSApplication]
GO
