SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	cbms_prod.dbo.cbmsUbmInvoiceCharge_GetProkarmaNotMapped

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
cbmsUbmInvoiceCharge_GetProkarmaNotMapped

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
SSR				Sharad Srivastava	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script
SSR				01/28/2010	Replaced bucket_type_id with bucket_master_id
							Hardcoded ubm_id filter replaced by ubm_name
SSR				02/24/2010	Removed NOLOCK HINT							
SKA				03/22/2010	Changed NULL condition of Commodity_type_id to -1
 DMR		  09/10/2010 Modified for Quoted_Identifier
******/
CREATE  PROCEDURE [dbo].[cbmsUbmInvoiceCharge_GetProkarmaNotMapped]
AS

BEGIN

	SET NOCOUNT ON

	SELECT 
		  i.ubm_invoice_id
		, d.ubm_invoice_details_id
		, cm.commodity_type_id	commodity_type_id
		, cm.ubm_service_type_id ubm_service_type_id
		, d.item_code ubm_service_code
		, dm.Bucket_Master_Id bucket_type_id
		, d.item_type_description ubm_bucket_code
		, d.item_name charge_name
		, d.item_amount charge_value
	FROM 
		dbo.ubm_invoice i
		JOIN dbo.ubm_batch_master_log ml 
			ON ml.ubm_batch_master_log_id = i.ubm_batch_master_log_id
		JOIN dbo.ubm_invoice_details d 
			ON d.ubm_invoice_id = i.ubm_invoice_id
		LEFT OUTER JOIN dbo.ubm_service_map cm 
			ON cm.ubm_id = ml.ubm_id 
				AND cm.ubm_service_code = d.item_code
		LEFT OUTER JOIN dbo.ubm_bucket_charge_map dm 
			ON dm.ubm_id = ml.ubm_id 
				AND dm.ubm_bucket_code = d.item_type_description 
				AND ((dm.commodity_type_id = cm.commodity_type_id)
				OR (dm.commodity_type_id =-1 AND cm.commodity_type_id IS NULL)
				)
		JOIN dbo.UBM u
			ON u.UBM_ID = ml.UBM_ID
	WHERE
		i.is_processed = 0
		AND u.UBM_NAME = 'Prokarma'
		AND (dm.Bucket_Master_Id IS NULL OR cm.ubm_service_type_id IS NULL)

END
GO
GRANT EXECUTE ON  [dbo].[cbmsUbmInvoiceCharge_GetProkarmaNotMapped] TO [CBMSApplication]
GO
