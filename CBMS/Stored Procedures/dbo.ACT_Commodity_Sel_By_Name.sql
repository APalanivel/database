SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:[dbo].[ACT_Commodity_Sel_By_Name]
DESCRIPTION:

INPUT PARAMETERS:
	Name						DataType		Default	Description
------------------------------------------------------------
	@ACTCommmodityName			NVARCHAR(255)
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

EXEC dbo.ACT_Commodity_Sel_By_Name
      @ACTCommmodityName = 'Water'

EXEC dbo.ACT_Commodity_Sel_By_Name
      @ACTCommmodityName = 'Ele'

SELECT TOP 10 * FROM ACT_Commodity

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	NJ			Naga Jyothi
	
MODIFICATIONS

	Initials	Date		Modification
---------------------------------------------------------------------------------------
	NJ			2019-07-02	Created	for SE2017-733 ACT Commodity Mapping within CBMS 					
******/
CREATE PROCEDURE [dbo].[ACT_Commodity_Sel_By_Name]
    (
        @ACTCommmodityName NVARCHAR(255) = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;
        SELECT
            ac.ACT_Commodity_Id
            , ac.ACT_Commodity_XName
        FROM
            dbo.ACT_Commodity ac
        WHERE
            @ACTCommmodityName IS NULL
            OR  ac.ACT_Commodity_XName LIKE '%' + @ACTCommmodityName + '%'
        ORDER BY
            ac.ACT_Commodity_XName;
    END;
GO
GRANT EXECUTE ON  [dbo].[ACT_Commodity_Sel_By_Name] TO [CBMSApplication]
GO
