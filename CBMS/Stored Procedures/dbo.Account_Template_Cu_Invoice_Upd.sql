SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
    dbo.Account_Template_Cu_Invoice_Upd  
 
DESCRIPTION:   
    select * from variance_log  

INPUT PARAMETERS:  
 Name				DataType		Default		Description  
------------------------------------------------------------  
 @Queue_Id			INT                
 @Client_Name		VARCHAR(200)	NULL  
 @City				VARCHAR(200)	NULL  
 @State_Name		VARCHAR(50)		NULL  
 @Vendor_Name		VARCHAR(200)	NULL  
 @Account_Number	VARCHAR(200)	NULL  
 @Site_Name			VARCHAR(200)	NULL 

   
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  

 USAGE EXAMPLES:  
------------------------------------------------------------  
 
BEGIN TRANSACTION
	SELECT ACCOUNT_ID,ACCOUNT_GROUP_ID,Template_Cu_Invoice_Id,Template_Cu_Invoice_Updated_User_Id,Template_Cu_Invoice_Last_Change_Ts,getdate()
		FROM dbo.ACCOUNT WHERE ACCOUNT_ID in (41581,468388)
	EXEC dbo.Account_Template_Cu_Invoice_Upd '41581,468388',22689984,16
	SELECT ACCOUNT_ID,ACCOUNT_GROUP_ID,Template_Cu_Invoice_Id,Template_Cu_Invoice_Updated_User_Id,Template_Cu_Invoice_Last_Change_Ts,getdate()
		FROM dbo.ACCOUNT WHERE ACCOUNT_ID in (41581,468388)
ROLLBACK TRANSACTION

BEGIN TRANSACTION
	SELECT ACCOUNT_ID,ACCOUNT_GROUP_ID,Template_Cu_Invoice_Id,Template_Cu_Invoice_Updated_User_Id,Template_Cu_Invoice_Last_Change_Ts,getdate()
		FROM dbo.ACCOUNT WHERE ACCOUNT_ID in (511118,536428)
	EXEC dbo.Account_Template_Cu_Invoice_Upd '511118,536428',14674423,16,NULL
	SELECT ACCOUNT_ID,ACCOUNT_GROUP_ID,Template_Cu_Invoice_Id,Template_Cu_Invoice_Updated_User_Id,Template_Cu_Invoice_Last_Change_Ts,getdate()
		FROM dbo.ACCOUNT WHERE ACCOUNT_ID in (511118,536428)
ROLLBACK TRANSACTION

BEGIN TRANSACTION
	SELECT ACCOUNT_ID,ACCOUNT_GROUP_ID,Template_Cu_Invoice_Id,Template_Cu_Invoice_Updated_User_Id,Template_Cu_Invoice_Last_Change_Ts,getdate()
		FROM dbo.ACCOUNT WHERE ACCOUNT_GROUP_ID = 18131
	EXEC dbo.Account_Template_Cu_Invoice_Upd NULL,14674423,16,18131
	SELECT ACCOUNT_ID,ACCOUNT_GROUP_ID,Template_Cu_Invoice_Id,Template_Cu_Invoice_Updated_User_Id,Template_Cu_Invoice_Last_Change_Ts,getdate()
		FROM dbo.ACCOUNT WHERE ACCOUNT_GROUP_ID = 18131
ROLLBACK TRANSACTION



------------------------------------------------------------
 AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------
RR			Raghu Reddy
 
Initials	Date		Modification
------------------------------------------------------------
RR			2014-06-09  Created Data Operations-2

******/  

CREATE PROCEDURE dbo.Account_Template_Cu_Invoice_Upd
      ( 
       @Account_Ids VARCHAR(MAX) = NULL
      ,@Template_Cu_Invoice_Id INT
      ,@Template_Cu_Invoice_Updated_User_Id INT
      ,@Account_Group_Id INT = NULL )
AS 
BEGIN  
      SET NOCOUNT ON;

      UPDATE
            acc
      SET   
            acc.Template_Cu_Invoice_Id = @Template_Cu_Invoice_Id
           ,acc.Template_Cu_Invoice_Updated_User_Id = @Template_Cu_Invoice_Updated_User_Id
           ,acc.Template_Cu_Invoice_Last_Change_Ts = getdate()
      FROM
            dbo.ACCOUNT acc
      WHERE
            ( ( @Account_Ids IS NOT NULL )
              AND EXISTS ( SELECT
                              1
                           FROM
                              dbo.ufn_split(@Account_Ids, ',') ids
                           WHERE
                              ids.Segments = acc.ACCOUNT_ID ) )
            OR ( @Account_Group_Id IS NOT NULL
                 AND acc.ACCOUNT_GROUP_ID = @Account_Group_Id )

END;

;
GO
GRANT EXECUTE ON  [dbo].[Account_Template_Cu_Invoice_Upd] TO [CBMSApplication]
GO
