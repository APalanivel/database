SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	Commodity_Current_Recalc_Type_Sel_By_Account

DESCRIPTION:

	Return the details of current Recalc Type for the commodity Based on Account_Id 
	
INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@Account_Id			int						(Utility)

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Commodity_Current_Recalc_Type_Sel_By_Account 1148522
	EXEC dbo.Commodity_Current_Recalc_Type_Sel_By_Account 1148523


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RKV			RAVI KUMAR VEGESNA

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	RKV        	2015-08-27 	Created
	
******/

CREATE PROCEDURE [dbo].[Commodity_Current_Recalc_Type_Sel_By_Account] @Account_Id INT
AS 
BEGIN

      SET NOCOUNT ON

      SELECT
            acirt.Commodity_ID
           ,c.Commodity_Name
           ,acirt.Invoice_Recalc_Type_Cd
           ,cd.Code_Value AS Recalc_Type
           ,acirt.Determinant_Source_Cd
           ,Sou_cd.Code_Value AS  Determinant_Source
      FROM
            dbo.Account_Commodity_Invoice_Recalc_Type acirt
            INNER JOIN dbo.Code cd
                  ON acirt.Invoice_Recalc_Type_Cd = cd.Code_Id
            INNER JOIN dbo.Commodity c
                  ON acirt.Commodity_ID = c.Commodity_Id
            LEFT JOIN dbo.Code Sou_cd
                  ON acirt.Determinant_Source_Cd = Sou_cd.Code_Id
      WHERE
            Account_Id = @Account_Id
            AND GETDATE() BETWEEN Start_Dt
                          AND     DATEADD(s, -1, DATEADD(mm, DATEDIFF(m, 0, ISNULL(End_Dt, GETDATE())) + 1, 0))
END;

GO
GRANT EXECUTE ON  [dbo].[Commodity_Current_Recalc_Type_Sel_By_Account] TO [CBMSApplication]
GO
