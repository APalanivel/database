SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
NAME:          
 CBMS.dbo.Cu_Invoice_Account_Commodity_Comment_Ins          
          
DESCRIPTION:          
   Created a new commment for Recalc header.          
          
          
INPUT PARAMETERS:          
 Name     DataType  Default Description          
---------------------------------------------------------------------------------------------------          
 @Cu_Invoice_Id   INT          
 @Account_Id    INT          
 @Commodity_Id   INT          
 @Comment_Type   VARCHAR(25)          
 @Comment_User_Info_Id INT          
 @Comment_Text   VARCHAR(MAX)          
          
OUTPUT PARAMETERS:          
 Name     DataType  Default Description          
---------------------------------------------------------------------------------------------------          
           
USAGE EXAMPLES:          
---------------------------------------------------------------------------------------------------          
          
 SELECT * FROM dbo.Cu_Invoice_Account_Commodity_Comment          
          
 EXEC dbo.CODE_SEL_BY_CodeSet_Name @CodeSet_Name = 'CommentType', @Code_Value = 'Recalc Comment'          
           
 SELECT TOP 10 * FROM dbo.ACCOUNT a          
 SELECT TOP 10 * FROM dbo.CU_INVOICE ci          
          
 BEGIN TRANSACTION          
           
  SELECT * FROM dbo.Cu_Invoice_Account_Commodity_Comment a INNER JOIN dbo.Comment b ON a.Comment_Id = b.Comment_ID          
   WHERE a.Account_Id = 1          
             
        EXEC dbo.Cu_Invoice_Account_Commodity_Comment_Ins           
            @Cu_Invoice_Id = 45327839          
           ,@Account_Id = 1114950          
           ,@Commodity_Id = 291          
           ,@Comment_Type = 'Internal Recalc Comment'          
           ,@User_Info_Id = 16          
           ,@Comment_Text = 'Test_Cu_Invoice'        
     ,@Recalc_Status_Type_Id =   15822        
            
  SELECT * FROM dbo.Cu_Invoice_Account_Commodity_Comment a INNER JOIN dbo.Comment b ON a.Comment_Id = b.Comment_ID          
   WHERE a.Account_Id = 1          
             
 ROLLBACK TRANSACTION          
          
          
AUTHOR INITIALS:          
 Initials Name          
---------------------------------------------------------------------------------------------------          
 NR   Narayana Reddy
 ABK	Aditya Bharadwaj          
           
MODIFICATIONS          
 Initials Date   Modification          
---------------------------------------------------------------------------------------------------          
 NR        2015-05-19  Created for AS400 Phase-2
 ABK		2019-06-14    SE2017-757: Added code to insert data into Cu_Invoice_Account_Commodity_Comment_Recalc_Status_Type_Map  table    
          
******/
CREATE PROCEDURE [dbo].[Cu_Invoice_Account_Commodity_Comment_Ins]
    (
        @Cu_Invoice_Id INT
        , @Account_Id INT
        , @Commodity_Id INT
        , @Comment_Type VARCHAR(25)
        , @User_Info_Id INT
        , @Comment_Text VARCHAR(MAX)
        , @Recalc_Status_Type_Id INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Comment_Id INT
            , @Comment_Type_CD INT
            , @Cu_Invoice_Account_Commodity_Comment_Id INT;

        SELECT
            @Comment_Type_CD = cd.Code_Id
        FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'CommentType'
            AND cd.Code_Value = @Comment_Type;

        EXEC dbo.Comment_Ins
            @Comment_Type_CD = @Comment_Type_CD
            , @Comment_User_Info_Id = @User_Info_Id
            , @Comment_Dt = NULL
            , @Comment_Text = @Comment_Text
            , @Comment_Id = @Comment_Id OUT;


        INSERT INTO dbo.Cu_Invoice_Account_Commodity_Comment
             (
                 Cu_Invoice_Id
                 , Account_Id
                 , Commodity_Id
                 , Comment_Id
             )
        VALUES
            (@Cu_Invoice_Id
             , @Account_Id
             , @Commodity_Id
             , @Comment_Id);


        SET @Cu_Invoice_Account_Commodity_Comment_Id = SCOPE_IDENTITY();


        INSERT INTO dbo.Cu_Invoice_Account_Commodity_Comment_Recalc_Status_Type_Map
             (
                 Cu_Invoice_Account_Commodity_Comment_Id
                 , Recalc_Status_Type_Id
             )
        SELECT
            @Cu_Invoice_Account_Commodity_Comment_Id AS Cu_Invoice_Account_Commodity_Comment_Id
            , @Recalc_Status_Type_Id AS Recalc_Status_Type_Id
        WHERE
            @Recalc_Status_Type_Id IS NOT NULL;

    END;
    ;
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Account_Commodity_Comment_Ins] TO [CBMSApplication]
GO
