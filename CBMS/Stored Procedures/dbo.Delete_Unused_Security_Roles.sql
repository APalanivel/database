SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/******************************************************************************************************    
NAME :	dbo.Delete_Unused_Security_Roles 
   
DESCRIPTION: 
This stored Procedure will delete the unused roles from SECURITY_ROLE_CLIENT_HIER and SECURITY_ROLE
   
 INPUT PARAMETERS:    
 Name             DataType               Default        Description    
--------------------------------------------------------------------      
   
 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
-------------------------------------------------------------------- 
   
  USAGE EXAMPLES:    
--------------------------------------------------------------------    
   
  
AUTHOR INITIALS:    
 Initials Name    
-------------------------------------------------------------------    
 KVK K Vinay Kumar
   
 MODIFICATIONS     
 Initials Date  Modification    
--------------------------------------------------------------------    
  
******************************************************************************************************/

CREATE PROCEDURE [dbo].[Delete_Unused_Security_Roles]
AS 
BEGIN

    SET NOCOUNT ON ;
	
    BEGIN TRY
	
        DELETE
            SRCH
        FROM
            SECURITY_ROLE_CLIENT_HIER SRCH
            INNER JOIN SECURITY_ROLE SR
                ON SR.Security_Role_Id = SRCH.Security_Role_Id
        WHERE
            SR.IS_CORPORATE = 0
            AND SR.Security_Role_Id NOT IN ( SELECT Distinct
                                                USR.Security_Role_Id
                                             FROM
                                                USER_SECURITY_ROLE USR )	

	 
        DELETE
            SR
        FROM
            SECURITY_ROLE SR
        WHERE
            SR.IS_CORPORATE = 0
            AND SR.Security_Role_Id NOT IN ( SELECT Distinct
                                                USR.Security_Role_Id
                                             FROM
                                                USER_SECURITY_ROLE USR )	

		
    END TRY
	
    BEGIN CATCH
		
        EXEC usp_RethrowError
	
    END CATCH

END

GO
GRANT EXECUTE ON  [dbo].[Delete_Unused_Security_Roles] TO [CBMSApplication]
GO
