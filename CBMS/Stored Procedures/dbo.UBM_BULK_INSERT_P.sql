
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******        
NAME: dbo.UBM_BULK_INSERT_P
      
DESCRIPTION:      
      
	To bulk load the data file received from UBM

INPUT PARAMETERS:         
    Name			DataType				Default     Description
-------------------------------------------------------------------
   @userId			VARCHAR(10)
   @sessionId		VARCHAR(20)
   @tableName		VARCHAR(100)
   @fileName		VARCHAR(100)
   @server			VARCHAR(100)

OUTPUT PARAMETERS:
      Name			DataType          Default     Description
---------------------------------------------------------------- 

USAGE EXAMPLES:
----------------------------------------------------------------

EXEC DBO.UBM_BULK_INSERT_P 
      @userId = '-1'
     ,@sessionId = '-1'
     ,@tableName = 'UBM_CASS_ACCOUNT'
     ,@fileName = 'UBM_CASS_ACCOUNT'
     ,@server = 'NAKULA'

AUTHOR INITIALS:
Initials	Name     
------------------------------------------------------------        
HG			Harihara Suthan G

MODIFICATIONS

Initials	Date		Modification
------------------------------------------------------------
HG			2014-09-24	Comments header added and modified for UBM Rename Prokarma to Schneider Electric and addition of new UBM Prokarma

******/

CREATE PROCEDURE [dbo].[UBM_BULK_INSERT_P]
      @userId VARCHAR(10)
     ,@sessionId VARCHAR(20)
     ,@tableName VARCHAR(100)
     ,@fileName VARCHAR(100)
     ,@server VARCHAR(100)
AS 
BEGIN

      SET NOCOUNT ON
	
      DECLARE @bulkInsert VARCHAR(8000)

      IF @TABLENAME IN ( 'ubm_cass_building', 'ubm_cass_facility_group', 'ubm_cass_weather', 'ubm_cass_check', 'ubm_cass_meter' )
		-- Eliminated ubm_cass_facility from the above filter as we need this data to update the missing client id in ubm_invoice table
            BEGIN
	 
                  RETURN

            END
  
      IF ( @fileName LIKE '%summit_%' ) 
            BEGIN

                  SET @bulkInsert = ' BULK INSERT ' + @tableName + ' FROM ' + '''' + '\\' + @server + '\SchneiderExtract\Data\' + @fileName + '.txt' + '''' + ' WITH (MAXERRORS=0, TABLOCK,KEEPNULLS, FORMATFILE =' + '''' + 'C:\UBMFormatFiles\' + @fileName + '.fmt' + '''' + ')'

            END
    ELSE IF ( @fileName LIKE '%Prokarma_%' ) 
            BEGIN

                  SET @bulkInsert = ' BULK INSERT ' + @tableName + ' FROM ' + '''' + '\\' + @server + '\ProkarmaExtract\Data\' + @fileName + '.txt' + '''' + ' WITH (MAXERRORS=0, TABLOCK,KEEPNULLS, FORMATFILE =' + '''' + 'C:\UBMFormatFiles\' + @fileName + '.fmt' + '''' + ')'

            END

      ELSE 
            IF ( @filename LIKE 'cass%' ) 
                  BEGIN

                        SET @bulkInsert = ' BULK INSERT ' + @tableName + ' FROM ' + '''' + '\\' + @server + '\CassExtract\Data\' + @fileName + '.txt' + '''' + ' WITH (MAXERRORS=0, TABLOCK, KEEPNULLS, FORMATFILE = ' + '''' + 'C:\UBMFormatFiles\' + @fileName + '.fmt' + '''' + ')'

                  END
            ELSE 
                  IF ( @filename LIKE 'gfe%' ) 
                        BEGIN

                              SET @bulkInsert = ' BULK INSERT ' + @tableName + ' FROM ' + '''' + '\\' + @server + '\gfeExtract\Data\' + @fileName + '.txt' + '''' + ' WITH (MAXERRORS=0, TABLOCK, KEEPNULLS, FORMATFILE = ' + '''' + 'C:\UBMFormatFiles\' + @fileName + '.fmt' + '''' + ')'

                        END

      EXEC ( @bulkInsert )

END  

;
GO

GRANT EXECUTE ON  [dbo].[UBM_BULK_INSERT_P] TO [CBMSApplication]
GO
