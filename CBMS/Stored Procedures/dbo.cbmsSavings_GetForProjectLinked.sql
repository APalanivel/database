SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create procedure [dbo].[cbmsSavings_GetForProjectLinked]
	(@MyAccountId int
	, @sso_project_id int
	)
as
begin
select map.sso_savings_id
	, s.savings_title
	, s.savings_description
	, cur.currency_unit_name
	, s.total_estimated_savings
	, c.entity_name savings_category_type
	, s.start_date
	, s.end_date
from sso_project_savings_map map
	join sso_savings s on s.sso_savings_id = map.sso_savings_id
	join currency_unit cur on cur.currency_unit_id = s.currency_unit_id
	join entity c on c.entity_id = s.SAVINGS_CATEGORY_TYPE_ID
	
where map.sso_project_id = @sso_project_id

end
	 

GO
GRANT EXECUTE ON  [dbo].[cbmsSavings_GetForProjectLinked] TO [CBMSApplication]
GO
