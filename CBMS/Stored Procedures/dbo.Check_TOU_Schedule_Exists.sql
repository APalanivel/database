SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********
NAME:  dbo.Check_TOU_Schedule_Exists

DESCRIPTION:  

	Checks if the passed in schedule name exists for the vendor and commodity combination.

INPUT PARAMETERS:
	Name				DataType          Default     Description
------------------------------------------------------------
	@Schedule_Name		NVARCHAR(100)
	@Vendor_Id			INT
	@Commodity_Id		INT
				

OUTPUT PARAMETERS:
      Name								DataType          Default     Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
Exec Check_TOU_Schedule_Exists 'new term',10,290
EXEC Check_TOU_Schedule_Exists 'Media Test',38,290

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CPE			Chaitanya Panduga Eshwar
	JK			Jignesh Kumar

	Initials	Date		Modification
------------------------------------------------------------
	CPE			2012-07-23  Created
	JK			2012-08-17  Added Usage Examples

******/ 

CREATE PROCEDURE [dbo].[Check_TOU_Schedule_Exists]
      @Schedule_Name NVARCHAR(100)
     ,@Vendor_Id INT
     ,@Commodity_Id INT
AS 
BEGIN

      SET NOCOUNT ON

      SELECT
            @Schedule_Name
      FROM
            dbo.Time_Of_Use_Schedule
      WHERE
            Schedule_Name = @Schedule_Name
            AND VENDOR_ID = @Vendor_Id
            AND Commodity_Id = @Commodity_Id
END


;
GO
GRANT EXECUTE ON  [dbo].[Check_TOU_Schedule_Exists] TO [CBMSApplication]
GO
