SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.CREATE_DEAL_TICKET_HEDGE_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@hedgeId       	int       	          	
	@monthIdentifier	datetime  	          	
	@hedgeVolume   	decimal(32,16)	          	
	@hedgePrice    	decimal(32,16)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

-- select * from HEDGE_DETAIL where HEDGE_ID = 1751

--exec DBO.CREATE_DEAL_TICKET_HEDGE_DETAILS_P  '-1', '-1', 1749, '04/01/2006',121, 6.7 
--select count(HEDGE_DETAIL_ID) from HEDGE_DETAIL where HEDGE_ID=1749 and MONTH_IDENTIFIER='04/01/2006'


CREATE   PROCEDURE DBO.CREATE_DEAL_TICKET_HEDGE_DETAILS_P 

@userId varchar(10),
@sessionId varchar(20),
@hedgeId int, 
@monthIdentifier datetime, 
@hedgeVolume decimal(32,16), 
@hedgePrice decimal(32,16) 

AS
set nocount on
	declare @hedgeCount int

select @hedgeCount = count(1) from HEDGE_DETAIL where HEDGE_ID=@hedgeId and MONTH_IDENTIFIER=@monthIdentifier

if @hedgeCount=0
begin 
	INSERT INTO HEDGE_DETAIL 
		(HEDGE_ID, MONTH_IDENTIFIER, HEDGE_VOLUME, HEDGE_PRICE)
	VALUES	(@hedgeId, @monthIdentifier, @hedgeVolume, @hedgePrice)
end
else
begin
	update HEDGE_DETAIL set HEDGE_VOLUME=@hedgeVolume , HEDGE_PRICE=@hedgePrice
	where HEDGE_ID=@hedgeId and MONTH_IDENTIFIER=@monthIdentifier
end
GO
GRANT EXECUTE ON  [dbo].[CREATE_DEAL_TICKET_HEDGE_DETAILS_P] TO [CBMSApplication]
GO
