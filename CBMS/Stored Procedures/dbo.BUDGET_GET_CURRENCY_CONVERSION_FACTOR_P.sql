SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE     PROCEDURE dbo.BUDGET_GET_CURRENCY_CONVERSION_FACTOR_P
	@user_id varchar(10),
	@session_id varchar(20),
	@budget_id int
	AS

	begin
	set nocount on

	select	budget_currency_map.conversion_factor,currency_unit.symbol
	from  budget_currency_map
	join currency_unit on currency_unit.currency_unit_id =budget_currency_map.currency_unit_id
	where budget_currency_map.budget_id=@budget_id
	
	end






GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_CURRENCY_CONVERSION_FACTOR_P] TO [CBMSApplication]
GO
