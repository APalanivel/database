SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
NAME:  Calc_Vals_By_Time_By_Account            
            
DESCRIPTION:              
Get all Calcvals of a account in a given time range              
              
INPUT PARAMETERS:              
 Name    DataType   Default   Description              
------------------------------------------------------------------------------              
@int_Account    INT        Account ID ,            
@int_Meter_Id    INT        Meter Id,            
@dt_Service_Month_Start  DATE       Date to process from,            
@dt_Service_Month_End  DATE       Date to process to            
@Attribute_Type   varchar(25)  Actual  
            
OUTPUT PARAMETERS:              
 Name   DataType  Default Description              
------------------------------------------------------------              
              
USAGE EXAMPLES:              
------------------------------------------------------------              
 SET STATISTICS IO ON              
  EXEC Calc_Vals_By_Time_By_Account 54126,37080,'2018-01-01','2019-01-01';     
    
  Example 2  
  declare @p3 dbo.tvp_CalcVal_Dates_Calculation_Tester  
 insert into @p3 values('2019-01-01 00:00:00','2018-01-01 00:00:00')  
 insert into @p3 values('2019-02-01 00:00:00','2018-02-01 00:00:00')  
 insert into @p3 values('2019-03-01 00:00:00','2018-03-01 00:00:00')  
 insert into @p3 values('2019-04-01 00:00:00','2018-04-01 00:00:00')  
 insert into @p3 values('2019-05-01 00:00:00','2018-05-01 00:00:00')  
 insert into @p3 values('2019-06-01 00:00:00','2018-06-01 00:00:00')  
 insert into @p3 values('2019-07-01 00:00:00','2018-07-01 00:00:00')  
 insert into @p3 values('2019-08-01 00:00:00','2018-08-01 00:00:00')  
 insert into @p3 values('2019-09-01 00:00:00','2018-09-01 00:00:00')  
 insert into @p3 values('2019-10-01 00:00:00','2018-10-01 00:00:00')  
 insert into @p3 values('2019-11-01 00:00:00','2018-11-01 00:00:00')  
 insert into @p3 values('2019-12-01 00:00:00','2019-12-01 00:00:00')  
  
 exec Calc_Vals_By_Time_By_Account @int_Account=1743883,  
 @int_Meter_Id=1272497,  
 @tvp_CalcVal_Dates_Calculation_Tester=@p3,  
 @Attribute_Type='Actual'  
  
         
             
            
AUTHOR INITIALS:            
            
 Initials Name              
------------------------------------------------------------              
    PRG		Prasanna Raghavendra G            
    SLP		SriLakshimi Pallikonda          
MODIFICATIONS:              
 Initials Date  Modification              
------------------------------------------------------------              
PRG   25-Apr-2019 Initial Development            
PRG   29-Apr-2019 Added Meter ID as a param         
SLP   18-Apr-2020 Added Attribute_Type as a parameter  
******/
CREATE PROC [dbo].[Calc_Vals_By_Time_By_Account]
    (
        @int_Account INT
        , @tvp_CalcVal_Dates_Calculation_Tester tvp_CalcVal_Dates_Calculation_Tester READONLY
        , @int_Meter_Id INT
        , @Attribute_Type VARCHAR(25) = 'Actual'
    )
AS
    BEGIN
        DECLARE
            --Loop Iterator Variables            
            @intCounter INT = 1
            , @intMyValue INT
            , @intCu_Invoice_ID INT
            , @intAccount_ID INT
            , @Service_Month DATE
            , @intCommodity_Id INT;

        CREATE TABLE #tmp_invoice_processing
             (
                 ID INT IDENTITY(1, 1)
                 , Cu_Invoice_ID INT
                 , Account_ID INT
                 , SERVICE_MONTH DATE
                 , Actual_Service_Month DATE
             );
        --Table to get response value from stored proc.            
        CREATE TABLE #tmp_CalVals_Data
             (
                 CU_Invoice_ID INT
                 , Service_Month DATE
                 , Ec_Calc_Val_id INT
                 , Ec_Calc_Value_Name NVARCHAR(MAX)
                 , Calc_Value DECIMAL(16, 2)
                 , Locked_Calc_Value DECIMAL(16, 2)
                 , Uom_Name VARCHAR(256)
                 , Calc_Type VARCHAR(256)
                 , Is_Value_Locked BIT
                 , Comment_Text VARCHAR(MAX)
                 , Expected_Slots INT
                 , Actual_Slots INT
                 , Missing_Slots INT
                 , Edited_Slots INT
             );

        --This is verified from existing system, that each meter will have single comodity.            
        SELECT
            @intCommodity_Id = Commodity_Id
        FROM
            Core.Client_Hier_Account
        WHERE
            Meter_Id = @int_Meter_Id;


        INSERT INTO #tmp_invoice_processing
             (
                 Cu_Invoice_ID
                 , Account_ID
                 , SERVICE_MONTH
                 , Actual_Service_Month
             )
        SELECT
            MAX(cuism.CU_INVOICE_ID)
            , cuism.Account_ID
            , cuism.SERVICE_MONTH
            , ct.Actual_Date
        FROM
            CU_INVOICE_SERVICE_MONTH cuism
            INNER JOIN @tvp_CalcVal_Dates_Calculation_Tester ct
                ON cuism.SERVICE_MONTH = ct.Calculated_Date
        WHERE
            cuism.Account_ID = @int_Account
        GROUP BY
            cuism.Account_ID
            , cuism.SERVICE_MONTH
            , ct.Actual_Date;

        IF @Attribute_Type = 'Actual'
            SET @int_Meter_Id = NULL;


        WHILE @intCounter <= (SELECT    MAX(ID)FROM #tmp_invoice_processing)
            BEGIN
                --PRINT @intCounter;            
                SELECT
                    @intCu_Invoice_ID = Cu_Invoice_ID
                    , @intAccount_ID = Account_ID
                    , @Service_Month = Actual_Service_Month
                FROM
                    #tmp_invoice_processing
                WHERE
                    ID = @intCounter;


                --Insert values of Stored proc to Temp tables            
                INSERT INTO #tmp_CalVals_Data
                     (
                         Ec_Calc_Val_id
                         , Ec_Calc_Value_Name
                         , Calc_Value
                         , Locked_Calc_Value
                         , Uom_Name
                         , Calc_Type
                         , Is_Value_Locked
                         , Comment_Text
                         , Expected_Slots
                         , Actual_Slots
                         , Missing_Slots
                         , Edited_Slots
                     )
                EXEC CalcVal_Sel_By_Cu_Invoice_Id_Account_Id_Commodity_Id_Service_Month
                    @intAccount_ID
                    , @intCommodity_Id
                    , @intCu_Invoice_ID
                    , @Service_Month
                    , @Attribute_Type
                    , @int_Meter_Id;


                --Update recent calc vals with invoice which is not available in output set of CalcVal_Sel_By_Cu_Invoice_Id_Account_Id_Commodity_Id            
                UPDATE
                    #tmp_CalVals_Data
                SET
                    CU_Invoice_ID = @intCu_Invoice_ID
                    , Service_Month = @Service_Month
                WHERE
                    CU_Invoice_ID IS NULL;



                --Increment the counter            
                SET @intCounter = @intCounter + 1;
            END;



        --Final Output Result.             
        SELECT
            tmp_cal_dat.Service_Month
            , tmp_cal_dat.Ec_Calc_Val_id
            , tmp_cal_dat.Ec_Calc_Value_Name
            , tmp_cal_dat.Calc_Value
            , tmp_cal_dat.Locked_Calc_Value
            , tmp_cal_dat.Uom_Name
            , tmp_cal_dat.Calc_Type
            , tmp_cal_dat.Is_Value_Locked
            , tmp_cal_dat.Comment_Text
            , tmp_cal_dat.Expected_Slots
            , tmp_cal_dat.Actual_Slots
            , tmp_cal_dat.Missing_Slots
            , tmp_cal_dat.Edited_Slots
        FROM
            #tmp_CalVals_Data tmp_cal_dat
        ORDER BY
            tmp_cal_dat.Service_Month;

        DROP TABLE
            #tmp_invoice_processing
            , #tmp_CalVals_Data;

    END;
GO
GRANT EXECUTE ON  [dbo].[Calc_Vals_By_Time_By_Account] TO [CBMSApplication]
GO
