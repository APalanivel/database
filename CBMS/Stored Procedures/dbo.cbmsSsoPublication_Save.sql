SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsSsoPublication_Save

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@sso_publication_id	int       	          	
	@publication_title	varchar(200)	          	
	@publication_description	varchar(1000)	null      	
	@publication_category_type_id	int       	          	
	@publication_date	datetime  	          	
	@full_text     	text      	          	
	@cbms_image_id 	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE          PROCEDURE [dbo].[cbmsSsoPublication_Save]
	( @MyAccountId int
	, @sso_publication_id int
	, @publication_title varchar(200)
	, @publication_description varchar(1000)=null
	, @publication_category_type_id int
	, @publication_date datetime
	, @full_text text
	, @cbms_image_id int
	)
AS
BEGIN

	set nocount on

	  declare @this_id int
	      set @this_id = @sso_publication_id


--	  DECLARE @rpl_active varchar(200)
--	   select @rpl_active = entity_name from entity where entity_type = 50000

	  DECLARE @rpl_sv_active varchar(200)
	   select @rpl_sv_active = entity_name from entity where entity_type = 50001



	if @this_id is null
	begin

		insert into sso_publications
			( 
			publication_title
			, publication_description
			, publication_category_type_id
			, publication_date					
			, full_text	
			, cbms_image_id			
			 )
		 values
			(  
			@publication_title
			, @publication_description
			, @publication_category_type_id
			, @publication_date
			, @full_text
			, @cbms_image_id
			)

--		set @this_id = @@IDENTITY
		set @this_id = scope_identity()


/*
		if @rpl_active = 'True'
		begin

			insert into [01SUMMIT-DB2].sso_prod_1.dbo.sso_publications
				( sso_publication_id, publication_title, publication_description, publication_category_type_id
				, publication_date, full_text, cbms_image_id
				)
			values
				( @this_id, @publication_title, @publication_description, @publication_category_type_id
				, @publication_date, @full_text, @cbms_image_id
				)
		
			insert into [01SUMMIT-DB2].sso_prod_2.dbo.sso_publications
				( sso_publication_id, publication_title, publication_description, publication_category_type_id
				, publication_date, full_text, cbms_image_id
				)
			values
				( @this_id, @publication_title, @publication_description, @publication_category_type_id
				, @publication_date, @full_text, @cbms_image_id
				)
	
		end
*/
	end
	else
	begin

		   update sso_publications
		      set publication_title = @publication_title
			, publication_description = @publication_description
			, publication_category_type_id = @publication_category_type_id
			, publication_date = @publication_date
			, full_text = @full_text
			, cbms_image_id = @cbms_image_id			
		    where sso_publication_id = @this_id
/*
		if @rpl_active = 'True'
		begin
	
			-- UPDATE USER ON SSO DATABASES
			   update [01SUMMIT-DB2].sso_prod_1.dbo.sso_publications
			      set publication_title = @publication_title
				, publication_description = @publication_description
				, publication_category_type_id = @publication_category_type_id
				, publication_date = @publication_date
				, full_text = @full_text
				, cbms_image_id = @cbms_image_id			
			    where sso_publication_id = @this_id
	
		   
			   update [01SUMMIT-DB2].sso_prod_2.dbo.sso_publications
			      set publication_title = @publication_title
				, publication_description = @publication_description
				, publication_category_type_id = @publication_category_type_id
				, publication_date = @publication_date
				, full_text = @full_text
				, cbms_image_id = @cbms_image_id			
			    where sso_publication_id = @this_id
		end
*/
	end
	exec cbmsSsoPublication_Get @MyAccountId, @this_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoPublication_Save] TO [CBMSApplication]
GO
