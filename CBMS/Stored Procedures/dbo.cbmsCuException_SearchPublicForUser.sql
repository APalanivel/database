
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/********  

NAME:  
  dbo.cbmsCuException_SearchPublicForUser  

DESCRIPTION:   

 INPUT PARAMETERS:
 Name				    DataType	 Default  Description  
------------------------------------------------------------  
 @MyAccountId		    int                     
 @client_name		    varchar(200) null         
 @city				    varchar(200) null         
 @vendor_name		    varchar(200) null         
 @account_number	    varchar(200) null         
 @Is_Manaual		    BIT			 --0	  For SearchExceptionPublicForUser & 1 For SearchIncomingPublicForUser 
 @Site_Name			    varchar(200) null   
 @Cu_Exception_Type_Id INT          NULL    
 @Country_Id			INT				    NULL
 @Commodity_Id			INT                 NULL
 @Supplier_Name			VARCHAR(500)	    NULL
 @State_Id				INT					NULL
    

 
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 
 USAGE EXAMPLES:  
------------------------------------------------------------  
 
 EXEC dbo.cbmsCuException_SearchPublicForUser 
      @MyAccountId = 88
     ,@client_name = NULL
     ,@city = NULL
     ,@vendor_name = NULL
     ,@account_number = NULL
     ,@is_manual = 0
     ,@Site_Name = NULL
     ,@Cu_Exception_Type_Id = NULL

 EXEC dbo.cbmsCuException_SearchPublicForUser 
      @MyAccountId = 152
     ,@client_name = NULL
     ,@city = NULL
     ,@vendor_name = NULL
     ,@account_number = NULL
     ,@is_manual = 0
     ,@Site_Name = NULL
     ,@Cu_Exception_Type_Id = NULL
     ,@Supplier_Name='Constellation New Energy'
          
 EXEC dbo.cbmsCuException_SearchPublicForUser @MyAccountId = 49,@is_manual = 1   
 
 EXEC cbmsCuException_SearchPublicForUser @MyAccountId = 10,@Is_Manual = 0, @Vendor_Name = 'Pioneer Electric Cooperative'
 
AUTHOR INITIALS:  
INITIALS NAME  
------------------------------------------------------------  
SKA		 Shobhit Kumar Agrawal
PNR		 Pandarinath
SP		 Sandeep Pigilam.

  
MODIFICATION  
INITIALS DATE		MODIFICATION
------------------------------------------------------------  
SKA		 10/21/2010	Created  
SKA		 01/13/2011	Added the search condition for Account Number Bug#20733
PNR		 03/09/2011	Added Left join with cu_invoice_label to fetch vendor_name when the invoice is not resolved to an account and if the value saved there(MAINT-495)
SP		 2014-05-26 For Data Operations Enhancement 4.2.3 added @Site_Name,@Cu_Exception_Type_Id as input parameters.
SP		 2017-05-17 small Enhancement,SE2017-124 added filters	@State_Id and removed @State_Name,@Country_Id INT = NULL,@Commodity_Id INT = NULL,@Supplier_Name VARCHAR(500) = NULL
*/
CREATE PROCEDURE [dbo].[cbmsCuException_SearchPublicForUser]
      (
       @MyAccountId INT
      ,@client_name VARCHAR(200) = NULL
      ,@city VARCHAR(200) = NULL
      ,@vendor_name VARCHAR(200) = NULL
      ,@account_number VARCHAR(200) = NULL
      ,@is_manual BIT
      ,@Site_Name VARCHAR(200) = NULL
      ,@Cu_Exception_Type_Id INT = NULL
      ,@Country_Id INT = NULL
      ,@Commodity_Id INT = NULL
      ,@Supplier_Name VARCHAR(500) = NULL
      ,@State_Id INT = NULL )
AS
BEGIN  
  
      SET NOCOUNT ON;
   
      SET @client_name = REPLACE(@client_name, '''', '''''')
      SET @city = REPLACE(@city, '''', '''''')  
      SET @vendor_name = REPLACE(@vendor_name, '''', '''''')  
      SET @account_number = REPLACE(@account_number, '''', '''''')  
      SET @account_number = REPLACE(@account_number, SPACE(1), SPACE(0))  
      SET @account_number = REPLACE(@account_number, '-', SPACE(0))  
      SET @account_number = REPLACE(@account_number, '/', SPACE(0))
      SET @Site_Name = REPLACE(@Site_Name, '''', '''''');
 
  
      SELECT
            x.CBMS_IMAGE_ID
           ,x.CBMS_DOC_ID
           ,x.CU_INVOICE_ID
           ,x.QUEUE_ID
           ,x.EXCEPTION_TYPE
           ,x.EXCEPTION_STATUS_TYPE
           ,CASE WHEN x.Account_ID IS NOT NULL THEN cha.Account_Number
                 ELSE x.UBM_Account_Number
            END AS Account_Number
           ,x.SERVICE_MONTH
           ,CASE WHEN x.Client_Hier_ID IS NOT NULL THEN ch.Client_Name
                 ELSE x.UBM_Client_Name
            END AS Client_Name
           ,CASE WHEN x.Client_Hier_ID IS NOT NULL THEN ch.Site_name
                 ELSE UBM_Site_Name
            END site_name
           ,MAX(ISNULL(CASE WHEN cha.Account_Type = 'Utility' THEN cha.Account_Vendor_Name
                            WHEN cha.Account_Type = 'Supplier' THEN LEFT(UV.Vendor_Name, LEN(UV.Vendor_Name) - 1)
                       END, cil.VENDOR_NAME)) AS Utility_Vendor_Name
           ,x.DATE_IN_QUEUE
           ,x.SORT_ORDER
           ,com.Commodity_Name
           ,MAX(ISNULL(CASE WHEN cha.Account_Type = 'Supplier' THEN cha.Account_Vendor_Name
                       END, scha.Account_Vendor_Name)) AS Supplier_Vendor_Name
      FROM
            dbo.USER_INFO_GROUP_INFO_MAP ugm
            JOIN dbo.GROUP_INFO gi
                  ON gi.GROUP_INFO_ID = ugm.GROUP_INFO_ID
            JOIN dbo.CU_EXCEPTION_DENORM x
                  ON x.QUEUE_ID = gi.QUEUE_ID
            JOIN dbo.CU_INVOICE ci
                  ON ci.CU_INVOICE_ID = x.CU_INVOICE_ID
            LEFT OUTER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                  ON cism.CU_INVOICE_ID = ci.CU_INVOICE_ID
            LEFT JOIN dbo.ACCOUNT_GROUP AG
                  ON AG.ACCOUNT_GROUP_ID = ci.ACCOUNT_GROUP_ID
            LEFT JOIN Core.Client_Hier ch
                  ON ch.Client_Hier_Id = x.Client_Hier_ID
            LEFT JOIN Core.Client_Hier_Account cha
                  ON cha.Client_Hier_Id = x.Client_Hier_ID
                     AND cha.Account_Id = x.Account_ID
            LEFT JOIN dbo.Commodity com
                  ON com.Commodity_Id = cha.Commodity_Id
            LEFT JOIN dbo.CU_INVOICE_LABEL cil
                  ON ci.CU_INVOICE_ID = cil.CU_INVOICE_ID
            LEFT  JOIN dbo.CU_EXCEPTION_TYPE cet
                  ON x.EXCEPTION_TYPE = cet.EXCEPTION_TYPE
            LEFT JOIN Core.Client_Hier_Account scha
                  ON cha.Client_Hier_Id = scha.Client_Hier_Id
                     AND cha.Meter_Id = scha.Meter_Id
                     AND scha.Account_Type = 'Supplier'
                     AND cism.SERVICE_MONTH BETWEEN scha.Supplier_Account_begin_Dt
                                            AND     scha.Supplier_Account_End_Dt
            OUTER APPLY ( SELECT
                              uac.Account_Vendor_Name + ', '
                          FROM
                              Core.Client_Hier_Account uac
                              INNER JOIN Core.Client_Hier_Account sac
                                    ON sac.Meter_Id = uac.Meter_Id
                          WHERE
                              uac.Account_Type = 'Utility'
                              AND sac.Account_Type = 'Supplier'
                              AND cha.Account_Type = 'Supplier'
                              AND sac.Account_Id = cha.Account_Id
                          GROUP BY
                              uac.Account_Vendor_Name
            FOR
                          XML PATH('') ) UV ( Vendor_Name )
      WHERE
            ugm.USER_INFO_ID = @MyAccountId
            AND x.IS_MANUAL = @is_manual
            AND ( ( @client_name IS NULL )
                  OR ( ISNULL(ch.Client_Name, x.UBM_Client_Name) LIKE '%' + @client_name + '%' ) )
            AND ( ( @city IS NULL )
                  OR ( ISNULL(ch.City, x.UBM_City) LIKE '%' + @city + '%' ) )
            AND ( ( @State_Id IS NULL )
                  OR EXISTS ( SELECT
                                    1
                              FROM
                                    dbo.STATE s
                              WHERE
                                    s.STATE_ID = @State_Id
                                    AND ( ISNULL(ch.State_Name, UBM_State_Name) = s.STATE_NAME ) ) )
            AND ( @vendor_name IS NULL
                  OR ISNULL(CASE WHEN cha.Account_Type = 'Utility' THEN cha.Account_Vendor_Name
                                 WHEN cha.Account_Type = 'Supplier' THEN LEFT(UV.Vendor_Name, LEN(UV.Vendor_Name) - 1)
                                 ELSE NULL
                            END, cil.VENDOR_NAME) LIKE '%' + @vendor_name + '%' )
            AND ( ( @account_number IS NULL )
                  OR ( ( LEN(@account_number) >= 4 )
                       AND ( REPLACE(REPLACE(REPLACE(ISNULL(AG.GROUP_BILLING_NUMBER, ISNULL(cha.Account_Number, x.UBM_Account_Number)), '/', SPACE(0)), SPACE(1), SPACE(0)), '-', SPACE(0)) LIKE '%' + @account_number + '%' ) )
                  OR ( ( LEN(@account_number) <= 4 )
                       AND ( REPLACE(REPLACE(REPLACE(ISNULL(AG.GROUP_BILLING_NUMBER, ISNULL(cha.Account_Number, x.UBM_Account_Number)), '/', SPACE(0)), SPACE(1), SPACE(0)), '-', SPACE(0)) = @account_number ) ) )
            AND ( ( @Site_Name IS NULL )
                  OR ( ISNULL(ch.Site_name, x.UBM_Site_Name) LIKE '%' + @Site_Name + '%' ) )
            AND ( @Cu_Exception_Type_Id IS NULL
                  OR cet.CU_EXCEPTION_TYPE_ID = @Cu_Exception_Type_Id )
            AND ( @Country_Id IS NULL
                  OR ch.Country_Id = @Country_Id )
            AND ( @Commodity_Id IS NULL
                  OR cha.Commodity_Id = @Commodity_Id )
            AND ( @Supplier_Name IS NULL
                  OR ( cha.Account_Type = 'Supplier'
                       AND cha.Account_Vendor_Name LIKE '%' + @Supplier_Name + '%' )
                  OR ( cha.Account_Type = 'Utility'
                       AND scha.Account_Vendor_Name LIKE '%' + @Supplier_Name + '%' ) )
      GROUP BY
            x.CBMS_IMAGE_ID
           ,x.CBMS_DOC_ID
           ,x.CU_INVOICE_ID
           ,x.QUEUE_ID
           ,x.EXCEPTION_TYPE
           ,x.EXCEPTION_STATUS_TYPE
           ,x.Account_ID
           ,cha.Account_Number
           ,x.UBM_Account_Number
           ,x.SERVICE_MONTH
           ,ch.Client_Name
           ,x.Client_Hier_ID
           ,x.UBM_Client_Name
           ,ch.Site_name
           ,x.UBM_Site_Name
           ,cha.Account_Vendor_Name
           ,x.DATE_IN_QUEUE
           ,x.SORT_ORDER
           ,com.Commodity_Name
           ,cil.VENDOR_NAME
      ORDER BY
            x.SORT_ORDER
           ,x.DATE_IN_QUEUE ASC   
   
END;
;
GO



GRANT EXECUTE ON  [dbo].[cbmsCuException_SearchPublicForUser] TO [CBMSApplication]
GO
