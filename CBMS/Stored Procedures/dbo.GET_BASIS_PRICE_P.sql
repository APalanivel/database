SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_BASIS_PRICE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@indexId       	int       	          	
	@pricePointName	varchar(200)	          	
	@year          	int       	          	
	@startDate     	datetime  	          	
	@endDate       	datetime  	          	
	@clientId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--exec dbo.GET_BASIS_PRICE_P -1,-1,322,'CenterPoINT Energy - East (formerly Reliant - East)',2008,null,null,0

CREATE PROCEDURE dbo.GET_BASIS_PRICE_P
	@userId VARCHAR(10),
	@sessionId VARCHAR(20),
	@indexId INT,
	@pricePointName VARCHAR(200),
	@year INT,
	@startDate DATETIME,--added by Ravi Datt for Currency Conversion
	@endDate DATETIME,--added by Ravi Datt for Currency Conversion
	@clientId INT --added by Ravi Datt for Currency Conversion

AS
BEGIN

	SET NOCOUNT ON;

	IF @year>0
	 BEGIN


		SELECT month_identifier
			, basis_price
		FROM RM_FORECAST a JOIN RM_FORECAST_DETAILS b 
				ON a.RM_FORECAST_ID=b.RM_FORECAST_ID
			JOIN RM_FORECAST c
				ON a.RM_FORECAST_ID=c.RM_FORECAST_ID
			JOIN Price_Index d
				ON d.price_index_id=c.price_index_id
				AND d.index_id=	@indexId AND 
					d.pricing_point=@pricePointName
		WHERE DATEPART(YEAR,b.month_identifier)=@year
		ORDER BY b.month_identifier

	 END
	ELSE IF @year=0 AND (SELECT COUNT(*) FROM RM_CURRENCY_UNIT_CONVERSION WHERE client_Id=@clientId)>0
	 BEGIN

		SELECT basis_price
			, conversion_factor
			, DATEPART(MONTH,b.MONTH_IDENTIFIER) dealMonth
			, DATEPART(YEAR,b.MONTH_IDENTIFIER) dealYear
		FROM RM_FORECAST a JOIN RM_FORECAST_DETAILS b
				ON a.RM_FORECAST_ID=b.RM_FORECAST_ID
			JOIN RM_FORECAST c
				ON a.RM_FORECAST_ID=c.RM_FORECAST_ID
			JOIN Price_Index d
				ON d.price_index_id=c.price_index_id
				AND d.index_id=	@indexId
				AND d.pricing_point=@pricePointName
			JOIN RM_CURRENCY_UNIT_CONVERSION currency
				ON conversion_year=DATEPART(YEAR,b.MONTH_IDENTIFIER)
		WHERE b.month_identifier BETWEEN @startDate AND @endDate
			AND currency.client_Id=@clientId
		
	 END
	ELSE IF @year=0
	 BEGIN

		SELECT basis_price
			, 1 conversion_factor
			, DATEPART(MONTH,b.MONTH_IDENTIFIER) dealMonth
			, DATEPART(YEAR,b.MONTH_IDENTIFIER) dealYear
		FROM RM_FORECAST a JOIN RM_FORECAST_DETAILS b
				ON a.RM_FORECAST_ID=b.RM_FORECAST_ID
			JOIN RM_FORECAST c 
				ON a.RM_FORECAST_ID=c.RM_FORECAST_ID
			JOIN Price_Index d
				ON d.price_index_id=c.price_index_id
				AND d.index_id=	@indexId
				AND d.pricing_point=@pricePointName
		WHERE b.month_identifier BETWEEN @startDate AND @endDate
		
	 END

END
GO
GRANT EXECUTE ON  [dbo].[GET_BASIS_PRICE_P] TO [CBMSApplication]
GO
