SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    Trade.Deal_Ticket_Trade_Price_Updated_Trade_Dtls_Sel_For_ALert
   
    
DESCRIPTION:   
   
    
INPUT PARAMETERS:    
      Name          DataType       Default        Description    
-----------------------------------------------------------------------------    
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec Trade.Deal_Ticket_Trade_Price_Updated_Trade_Dtls_Sel_For_ALert  

	
	
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	RR          Raghu Reddy

    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	RR          2019-09-30	GRM-1530 Created
	RR          2019-09-30	MAINT-9864 Performance tuning - Tbale Trade.Deal_Ticket_Client_Hier_TXN_Status will have records for
							all deal ticket transitions, so the high number record count causing the issue. In this script need
							only records that are trade completed, so added not null filter on trade price and counter party
	RR          2019-09-30	MAINT-9864 Modified to consider 'Order Executed' status also to send price updated alert
******/

CREATE PROC [Trade].[Deal_Ticket_Trade_Price_Updated_Trade_Dtls_Sel_For_ALert]
    (
        @Deal_Ticket_Id VARCHAR(MAX) = NULL
    )
WITH RECOMPILE
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            dt.Deal_Ticket_Id
            , ' ' AS Risk_Manager
            , ' ' AS Risk_Manager_Email
            , ' ' AS Risk_Manager_Phone
            , CQ.EMAIL_ADDRESS AS Currently_With_Email
            , CASE WHEN LEN(CM.CM_Email) > 0 THEN LEFT(CM.CM_Email, LEN(CM.CM_Email) - 1)
              END AS CM_Email
            , ch.Client_Name
            , CASE WHEN COUNT(DISTINCT ch.Site_Id) > 1 THEN 'Multiple'
                  ELSE MAX(RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')')
              END AS Site_Name
            , dtchvd.Trade_Number AS Cbms_Trade_Number
            , chws.Workflow_Status_Map_Id
            , CASE WHEN dttyp.ENTITY_NAME = 'Physical' THEN cha.VENDOR_NAME
                  WHEN dttyp.ENTITY_NAME = 'Financial' THEN cp.COUNTERPARTY_NAME
              END AS Counterparty_Supplier
            , CASE WHEN LEN(CMName.CM_Name) > 0 THEN LEFT(CMName.CM_Name, LEN(CMName.CM_Name) - 1)
              END AS CM_Name
            , ui2.EMAIL_ADDRESS AS Initiator_Email
            , ui2.FIRST_NAME + ' ' + ui2.LAST_NAME AS Initiator_Name
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN dbo.ENTITY dttyp
                ON dt.Hedge_Type_Cd = dttyp.ENTITY_ID
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dtch.Deal_Ticket_Id = dt.Deal_Ticket_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Id = dt.Client_Id
                   AND  ch.Client_Hier_Id = dtch.Client_Hier_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtchvd
                ON dtchvd.Deal_Ticket_Id = dt.Deal_Ticket_Id
                   AND  dtchvd.Client_Hier_Id = dtch.Client_Hier_Id
                   AND  dtchvd.Deal_Month = chws.Trade_Month
            INNER JOIN Trade.Trade_Price tp
                ON dtchvd.Trade_Price_Id = tp.Trade_Price_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_TXN_Status dtchts
                ON dtchts.Trade_Price_Id = dtchvd.Trade_Price_Id
                   AND  dtchts.Cbms_Trade_Number = dtchvd.Trade_Number
            LEFT JOIN dbo.USER_INFO CQ
                ON dt.Queue_Id = CQ.QUEUE_ID
            LEFT JOIN dbo.VENDOR cha
                ON cha.VENDOR_ID = dtchts.Cbms_Counterparty_Id
                   AND  dttyp.ENTITY_NAME = 'Physical'
            LEFT JOIN dbo.RM_COUNTERPARTY cp
                ON cp.RM_COUNTERPARTY_ID = dtchts.Cbms_Counterparty_Id
                   AND  dttyp.ENTITY_NAME = 'Financial'
            CROSS APPLY
        (   SELECT
                ui.EMAIL_ADDRESS + ','
            FROM
                dbo.USER_INFO ui
                INNER JOIN dbo.CLIENT_CEM_MAP ccm
                    ON ui.USER_INFO_ID = ccm.USER_INFO_ID
            WHERE
                ccm.CLIENT_ID = dt.Client_Id
            FOR XML PATH('')) CM(CM_Email)
            CROSS APPLY
        (   SELECT
                ui.FIRST_NAME + ' ' + ui.LAST_NAME + ', '
            FROM
                dbo.USER_INFO ui
                INNER JOIN dbo.CLIENT_CEM_MAP ccm
                    ON ui.USER_INFO_ID = ccm.USER_INFO_ID
            WHERE
                ccm.CLIENT_ID = dt.Client_Id
            GROUP BY
                ui.FIRST_NAME + ' ' + ui.LAST_NAME
            FOR XML PATH('')) CMName(CM_Name)
            LEFT JOIN dbo.USER_INFO ui2
                ON dt.Created_User_Id = ui2.USER_INFO_ID
        WHERE
            chws.Is_Active = 1
            AND tp.Trade_Price IS NOT NULL
            AND dtchts.Trade_Price_Id IS NOT NULL
            AND dtchts.Cbms_Counterparty_Id IS NOT NULL
            AND EXISTS (   SELECT
                                1
                           FROM
                                Trade.Workflow_Status_Map wsm
                                INNER JOIN Trade.Workflow_Status ws
                                    ON wsm.Workflow_Status_Id = ws.Workflow_Status_Id
                           WHERE
                                chws.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
                                AND ws.Workflow_Status_Name IN ( 'Completed', 'Order Executed' ))
            AND tp.Last_Change_Ts > DATEADD(hh, -120, GETDATE())
            AND (   (   chws.Last_Change_Ts < tp.Last_Change_Ts
                        AND chws.Last_Notification_Sent_Ts IS NULL)
                    OR  (   chws.Last_Change_Ts < tp.Last_Change_Ts
                            AND chws.Last_Notification_Sent_Ts < tp.Last_Change_Ts))
            AND (   @Deal_Ticket_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Deal_Ticket_Id, ',') us
                                   WHERE
                                        us.Segments = dt.Deal_Ticket_Id))
        GROUP BY
            dt.Deal_Ticket_Id
            , CQ.EMAIL_ADDRESS
            , CASE WHEN LEN(CM.CM_Email) > 0 THEN LEFT(CM.CM_Email, LEN(CM.CM_Email) - 1)
              END
            , ch.Client_Name
            , CASE WHEN LEN(CMName.CM_Name) > 0 THEN LEFT(CMName.CM_Name, LEN(CMName.CM_Name) - 1)
              END
            , ui2.EMAIL_ADDRESS
            , ui2.FIRST_NAME
            , ui2.LAST_NAME
            , dtchvd.Trade_Number
            , chws.Workflow_Status_Map_Id
            , CASE WHEN dttyp.ENTITY_NAME = 'Physical' THEN cha.VENDOR_NAME
                  WHEN dttyp.ENTITY_NAME = 'Financial' THEN cp.COUNTERPARTY_NAME
              END;

    END;


GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Trade_Price_Updated_Trade_Dtls_Sel_For_ALert] TO [CBMSApplication]
GO
