SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/******************************************************************************************************    

				Temparory - To delete

NAME :	dbo.Get_Role_For_Combination 
   
DESCRIPTION: This procedure used to verify the combination of sites and Divisions are existed in security_Role_client_Hier table If yes Sp will return the RoleID else it will return NULL.

   
 INPUT PARAMETERS:    
 Name             DataType               Default        Description    
--------------------------------------------------------------------      
 @ClientId        INT                                   Client ID 
 @SiteList        VARCHAR(MAX)							list of site IDs with comma separeted
 @DivisionList    VARCHAR(MAX)							list of division IDs with comma separeted
   
 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
--------------------------------------------------------------------    
 @Security_Role_ID INT
  
  USAGE EXAMPLES:    
--------------------------------------------------------------------    
DECLARE @divisionList Division_Ids
INSERT INTO @divisionList VALUES (12208672),(1913)
DECLARE @siteList Site_Ids
INSERT INTO @siteList VALUES (24258),(24268),(24269)
EXEC dbo.Get_Role_For_Combination  11231,@divisionList,@siteList
  
AUTHOR INITIALS:    
 Initials	Name    
-------------------------------------------------------------------    
 KVK		K VINAY KUMAR
 RR			Raghu Reddy
   
 MODIFICATIONS     
 Initials	Date					Modification    
--------------------------------------------------------------------    
KVK			08/11/2010(mm/dd/yyyy)	split the camma separated value by using "ufn_Split".
kvk			08/30/2010(mm/dd/yyyy)	Align the code properly
KVK			12/02/2010				Removed the TRY-CATCH blocks and removed hardcoded values for Hier_level_Cd coulmn
RR			08/26/2011				MAINT-758 Replaced the comma seperated input param for site list and division list into tvp, count(*) with row count, in
									clause with exists


******************************************************************************************************/


CREATE PROCEDURE [dbo].[Get_Role_For_Combination]
      ( 
       @clientId INT
      ,@divisionList Division_Ids READONLY
      ,@siteList Site_Ids READONLY )
AS 
BEGIN


      SET NOCOUNT ON ;
	
      CREATE TABLE #totalSites ( Site_Id INT )
      CREATE INDEX IDX_totalSites ON #totalSites(Site_Id)

      DECLARE
           @Tempcnt INT = 0
		
	
      SELECT
            @Tempcnt = count(1)
      FROM
            @divisionList
	
      INSERT      INTO #totalSites
                  SELECT
                        Site_Id
                  FROM
                        @siteList	
      SELECT
            @Tempcnt = @Tempcnt + @@ROWCOUNT
      
      INSERT      INTO #totalSites
                  SELECT
                        s.site_id
                  FROM
                        dbo.SITE s
                        JOIN @divisionList d
                              ON d.Division_Id = s.division_id
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          #totalSites t
                                     WHERE
                                          t.Site_Id = s.site_id )
	 
      SELECT
            @Tempcnt = @Tempcnt + @@ROWCOUNT

      SELECT
            a.Security_Role_Id
      FROM
            [dbo].[security_role] a
      WHERE
            a.Client_Id = @clientId
            AND @Tempcnt = ( SELECT
                              count(1)
                             FROM
                              [dbo].[Security_Role_Client_Hier] z
                             WHERE
                              z.Security_Role_Id = a.Security_Role_Id )
            AND @Tempcnt = ( SELECT
                              count(1)
                             FROM
                              [dbo].[Security_Role_Client_Hier] b
                              INNER JOIN [Core].[Client_Hier] c
                                    ON b.Client_Hier_Id = c.Client_Hier_Id
                             WHERE
                              b.Security_Role_Id = a.Security_Role_Id
                              AND c.Client_Id = @clientId
                              AND ( ( c.Site_Id > 0
                                      AND EXISTS ( SELECT
                                                      1
                                                   FROM
                                                      #totalSites ts
                                                   WHERE
                                                      c.Site_Id = ts.Site_Id ) )
                                    OR ( c.Site_Id = 0
                                         AND EXISTS ( SELECT
                                                            1
                                                      FROM
                                                            @divisionList ads
                                                      WHERE
                                                            c.Sitegroup_Id = ads.Division_Id ) ) )
                             GROUP BY
                              b.Security_Role_Id )		
							  
      DROP TABLE #totalSites

END

GO
GRANT EXECUTE ON  [dbo].[Get_Role_For_Combination] TO [CBMSApplication]
GO
