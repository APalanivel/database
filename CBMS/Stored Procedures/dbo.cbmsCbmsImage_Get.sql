SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE     procedure [dbo].[cbmsCbmsImage_Get]
	( @MyAccountId int
	, @cbms_image_id int
	)
AS
BEGIN

	   select i.cbms_image_id
		, i.cbms_image_type_id
		, it.entity_name cbms_image_type
		, i.cbms_doc_id
		, i.date_imaged
		, i.billing_days_adjustment
		--, i.cbms_image
		, i.content_type
		, i.cbms_image_size
	     from cbms_image i
  left outer join entity it on it.entity_id = i.cbms_image_type_id
	    where i.cbms_image_id = @cbms_image_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCbmsImage_Get] TO [CBMSApplication]
GO
