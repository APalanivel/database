
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:        
 cbms_prod.dbo.BUDGET_GET_BUDGET_SEARCH_RESULTS_P        
        
DESCRIPTION:        
        
INPUT PARAMETERS:        
 Name   DataType  Default Description        
------------------------------------------------------------        
   @budgetId                AS INT        
 , @year                    AS INT        
 , @commodity_type_id       AS INT        
 , @stateId                 AS INT        
 , @vendorId                AS INT        
 , @clientId                AS INT        
 , @divisionId              AS INT        
 , @siteId                  AS INT        
 , @accountId               AS INT        
 , @regionId                AS INT        
 , @saId                    AS INT        
 , @raId                    AS INT        
 , @budget_status_id        AS INT        
 , @fromDate                AS DATETIME        
 , @toDate                  AS DATETIME        
 , @fieldName               AS VARCHAR( 80 )        
 , @sortState               AS INT        
 , @ratescompleted          AS INT        
 , @ratesreviewed           AS INT        
                    
                 
        
OUTPUT PARAMETERS:        
 Name   DataType  Default Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:        
------------------------------------------------------------        
        
EXEC dbo.BUDGET_GET_BUDGET_SEARCH_RESULTS_P 0, 2013, 290, 0, 0,235, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 'status', 0, 0, 0, 0,10     
EXEC dbo.BUDGET_GET_BUDGET_SEARCH_RESULTS_P 0, 2013, 290, 0, 0,11682, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 'client', 0, 0, 0, 0,10      
EXEC dbo.BUDGET_GET_BUDGET_SEARCH_RESULTS_P 0, 2013, 290, 0, 0,11682, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 'site', 0, 0, 0, 0,10      
EXEC dbo.BUDGET_GET_BUDGET_SEARCH_RESULTS_P 0, 2013, 290, 0, 0,11682, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 'tariffTransport', 0, 0, 0, 0,10      
EXEC dbo.BUDGET_GET_BUDGET_SEARCH_RESULTS_P 0, 2013, 290, 0, 0,11682, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 'dueDate', 0, 0, 0, 0,10      
    
        
AUTHOR INITIALS:        
 Initials Name        
------------------------------------------------------------        
 PNR   Pandari Nath        
 CPE   Chaitanya Panduga Eshwar      
 RKV   Ravi Kumar Vegesna  
 SP	   Sandeep Pigilam           
      
         
MODIFICATIONS        
        
 Initials Date  Modification        
------------------------------------------------------------        
 SKA   01/DEC/09 Remove Budget_Account_Type_VW which was joing the Budget tables again        
       Removed lots of if else conditions and used case statement and used temp table        
 PNR   08/18/2010 Replaced if condition by case statement for Sorting column         
       and also added filter conditions for @ratescompleted,@ratesreviewed params.        
 PNR   08/19/2010 Added Column List of Tempdata table to match column list in union statement.        
 SKA   08/23/2010 Added the where clause for Vendor_id and Region_id        
 SKA   08/23/2010 Modify the where condition for Vendor_id and Region_id        
 DMR   09/10/2010 Modified for Quoted_Identifier        
 CPE   2013-03-22 Rewrote the SP to use core.Client_Hier_Account table for Commercial Budget        
 RKV   2013-11-19 MAINT-2187 Pagination logic Added   
 SP	   2014-09-09 For Data Transition used a table variable @Group_Legacy_Name to handle the hardcoded group names  
 HG	   2015-03-16	MAINT-3433, changed the legacy group name to "Regulated Markets Budgets"

******/        
CREATE PROC [dbo].[BUDGET_GET_BUDGET_SEARCH_RESULTS_P]
      ( 
       @budgetId AS INT
      ,@year AS INT
      ,@commodity_type_id AS INT
      ,@stateId AS INT
      ,@vendorId AS INT
      ,@clientId AS INT
      ,@divisionId AS INT
      ,@siteId AS INT
      ,@accountId AS INT
      ,@regionId AS INT
      ,@saId AS INT
      ,@raId AS INT
      ,@budget_status_id AS INT
      ,@fromDate AS DATETIME
      ,@toDate AS DATETIME
      ,@fieldName AS VARCHAR(80)
      ,@sortState AS INT
      ,@ratescompleted AS INT
      ,@ratesreviewed AS INT
      ,@StartIndex INT = 1
      ,@EndIndex INT = 2147483647
      ,@total_row_cnt INT = 0 )
      WITH RECOMPILE
AS 
BEGIN        
      SET NOCOUNT ON    
        
      DECLARE
            @ServiceLvl_ID1 AS INT
           ,@ServiceLvl_ID2 AS INT
           ,@fieldNameSort AS VARCHAR(80) = ''
           ,@sortOrder AS VARCHAR(4) = ''
           ,@SQLStatement NVARCHAR(MAX)
           ,@SQLStatement1 NVARCHAR(MAX)
           ,@ParamList NVARCHAR(500)
           ,@SQLStatement_For_Row_Count NVARCHAR(MAX)
           ,@SQLStatment_Final NVARCHAR(MAX)  
             

      DECLARE @Group_Legacy_Name TABLE
            ( 
             GROUP_INFO_ID INT
            ,GROUP_NAME VARCHAR(200)
            ,Legacy_Group_Name VARCHAR(200) )
                    
      CREATE TABLE #Budget_Accounts
            ( 
             Budget_Id INT
            ,Budget_Account_Id INT
            ,Account_Id INT
            ,Commodity_Id INT
            ,rates_completed_by INT
            ,rates_reviewed_by INT
            ,sourcing_completed_by INT
            ,DUE_DATE DATE
            ,cd_create_multiplier DECIMAL(32, 16)
            ,RATES_COMPLETED_DATE DATE
            ,RATES_REVIEWED_DATE DATE
            ,sourcing_completed_date DATE
            ,status_type_id INT
            ,POSTED_BY INT
            ,BUDGET_START_MONTH INT
            ,BUDGET_START_YEAR INT PRIMARY KEY ( Account_Id, Commodity_Id, Budget_Id ) )        
        
      CREATE TABLE #Base_Accounts
            ( 
             Row_Num INT IDENTITY(1, 1)
            ,Client_Hier_Id INT NOT NULL
            ,Account_Id INT NOT NULL
            ,Account_Number VARCHAR(50) NOT NULL
            ,Tariff_Transport VARCHAR(10) NOT NULL
            ,Account_Service_level_Cd INT NOT NULL
            ,vcam_Analyst_Id INT
            ,udam_Analyst_ID INT PRIMARY KEY ( Client_Hier_Id, Account_Id ) )        
        
      SELECT
            @ServiceLvl_ID1 = entity_id
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_NAME = 'C'
            AND ENTITY_DESCRIPTION = 'Commercial'        
        
      SELECT
            @ServiceLvl_ID2 = entity_id
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_NAME = 'D'
            AND ENTITY_DESCRIPTION = 'Other'        
        
      SET @fieldNameSort = CASE WHEN @fieldName = 'budgetId'
                                     OR @fieldName IS NULL THEN 'budgetId'
                                WHEN @fieldName = 'client' THEN 'ClientName'
                                WHEN @fieldName = 'site' THEN 'siteName'
                                WHEN @fieldName = 'account' THEN 'accountNumber'
                                WHEN @fieldName = 'tariffTransport' THEN 'Tariff_Transport'
                                WHEN @fieldName = 'dueDate' THEN 'duedate'
                                WHEN @fieldName = 'rates' THEN 'raCompletedBy'
                                WHEN @fieldName = 'ratesReviewed' THEN 'raReviewedBy'
                                WHEN @fieldName = 'sourcing' THEN 'saCompletedBy'
                                WHEN @fieldName = 'status' THEN 'status_type_id'
                                WHEN @fieldName = 'serviceLevel' THEN 'service_level_type_id'
                                ELSE @fieldName
                           END        
        
      SET @sortOrder = CASE WHEN @sortState IN ( 0, 1 ) THEN 'ASC'
                            ELSE 'DESC'
                       END        
      
      INSERT      INTO @Group_Legacy_Name
                  ( 
                   GROUP_INFO_ID
                  ,GROUP_NAME
                  ,Legacy_Group_Name )
                  EXEC dbo.Group_Info_Sel_By_Group_Legacy 
                        @Group_Legacy_Name_Cd_Value = 'Regulated_Markets_Budgets'    
                      
      INSERT      #Budget_Accounts
                  ( 
                   Budget_Id
                  ,Budget_Account_Id
                  ,Account_Id
                  ,Commodity_Id
                  ,rates_completed_by
                  ,rates_reviewed_by
                  ,sourcing_completed_by
                  ,DUE_DATE
                  ,cd_create_multiplier
                  ,RATES_COMPLETED_DATE
                  ,RATES_REVIEWED_DATE
                  ,sourcing_completed_date
                  ,status_type_id
                  ,POSTED_BY
                  ,BUDGET_START_MONTH
                  ,BUDGET_START_YEAR )
                  SELECT
                        b.BUDGET_ID
                       ,ba.BUDGET_ACCOUNT_ID
                       ,ba.ACCOUNT_ID
                       ,b.COMMODITY_TYPE_ID
                       ,ba.RATES_COMPLETED_BY
                       ,ba.RATES_REVIEWED_BY
                       ,ba.SOURCING_COMPLETED_BY
                       ,b.DUE_DATE
                       ,BA.CD_CREATE_MULTIPLIER
                       ,ba.RATES_COMPLETED_DATE
                       ,ba.RATES_REVIEWED_DATE
                       ,ba.SOURCING_COMPLETED_DATE
                       ,b.STATUS_TYPE_ID
                       ,b.POSTED_BY
                       ,b.BUDGET_START_MONTH
                       ,b.BUDGET_START_YEAR
                  FROM
                        dbo.BUDGET AS b
                        INNER JOIN dbo.BUDGET_ACCOUNT AS ba
                              ON b.BUDGET_ID = ba.BUDGET_ID
                  WHERE
                        b.budget_start_year = @year
                        AND ba.is_deleted = 0
                        AND ( @budgetId = 0
                              OR b.budget_id = @budgetId )
                        AND ( @commodity_type_id IS NULL
                              OR b.COMMODITY_TYPE_ID = @commodity_type_id )
                        AND ( ( @budget_status_id = 0 )
                              OR ( b.status_type_id = @budget_status_id ) )
                        AND ( ( @fromDate IS NULL
                                AND @toDate IS NULL )
                              OR b.due_date BETWEEN ISNULL(@fromDate, '01/01/1900')
                                            AND     ISNULL(@toDate, '01/01/2099') )
                        AND ( ( @accountId = 0 )
                              OR ( ba.account_id = @accountId ) )        
                                                        
      INSERT      #Base_Accounts
                  ( 
                   Client_Hier_Id
                  ,Account_Id
                  ,Account_Number
                  ,Account_Service_level_Cd
                  ,Tariff_Transport
                  ,vcam_Analyst_Id
                  ,udam_Analyst_ID )
                  SELECT
                        ch.Client_Hier_Id
                       ,cha.account_id AS accountId
                       ,cha.account_number AS accountNumber
                       ,cha.Account_Service_level_Cd
                       ,CASE WHEN MAX(cha1.Account_Id) IS NOT NULL THEN 'Transport'
                             ELSE 'Tariff'
                        END AS tariff_transport
                       ,vcam.Analyst_Id
                       ,udam.Analyst_ID
                  FROM
                        Core.Client_Hier_Account AS cha
                        INNER JOIN Core.Client_Hier AS ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        LEFT JOIN ( Core.Client_Hier_Account AS cha1
                                    INNER JOIN dbo.CONTRACT AS c
                                          ON cha1.Supplier_Contract_ID = c.CONTRACT_ID
                                             AND c.contract_end_date >= GETDATE()
                                             AND cha1.Account_Type = 'Supplier'
    INNER JOIN dbo.Entity conType
                                          ON conType.Entity_id = c.contract_type_id
                                             AND ConType.Entity_Name = 'Supplier' )
                                    ON cha.Meter_Id = cha1.Meter_Id
                        LEFT JOIN ( dbo.VENDOR_COMMODITY_MAP AS vcm
                                    INNER JOIN dbo.Vendor_Commodity_Analyst_Map AS vcam
                                          ON vcam.Vendor_Commodity_Map_Id = vcm.VENDOR_COMMODITY_MAP_ID
                                             AND vcam.Analyst_Id = @saId
                                             AND vcm.COMMODITY_TYPE_ID = @commodity_type_id )
                                    ON vcm.VENDOR_ID = cha.Account_Vendor_Id
                        LEFT JOIN ( dbo.UTILITY_DETAIL AS ud
                                    INNER JOIN dbo.Utility_Detail_Analyst_Map udam
                                          ON ud.UTILITY_DETAIL_ID = udam.Utility_Detail_ID
                                    INNER JOIN dbo.GROUP_INFO gi
                                          ON udam.Group_Info_ID = gi.GROUP_INFO_ID
                                             AND udam.Analyst_ID = @raId
                                    INNER JOIN @Group_Legacy_Name gil
                                          ON gi.Group_Info_ID = gil.GROUP_INFO_ID )
                                    ON ud.VENDOR_ID = cha.Account_Vendor_Id
                  WHERE
                        cha.Account_Type = 'Utility'
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          #Budget_Accounts
                                     WHERE
                                          cha.Account_Id = Account_ID
                                          AND cha.Commodity_Id = Commodity_Id )
                        AND ( @divisionId = 0
                              OR ch.Sitegroup_Id = @divisionId )
                        AND ( @siteId = 0
                              OR ch.Site_Id = @siteId )
                        AND ( ( @stateId = 0 )
                              OR ( ch.state_id = @stateId ) )
                        AND ( ( @clientId = 0 )
                              OR ( ch.client_id = @clientId ) )
                        AND ( @vendorId = 0
                              OR cha.Account_Vendor_Id = @vendorId )
                        AND ( ( @regionId = 0 )
                              OR ( ch.Region_ID = @regionId ) )
                  GROUP BY
                        ch.Client_Hier_Id
                       ,cha.account_id
                       ,cha.account_number
                       ,cha.Account_Service_level_Cd
                       ,vcam.Analyst_Id
                       ,udam.Analyst_ID        
      
      IF @total_row_cnt = 0 
            BEGIN  
                  SET @SQLStatement_For_Row_Count = 'SELECT        
                  
         @total_cnt = COUNT(1) OVER ( )        
  FROM        
            #Budget_Accounts ba        
            INNER JOIN #Base_Accounts bsa         
  ON ba.Account_Id = bsa.Account_Id        
            INNER JOIN core.Client_Hier AS ch        
                  ON bsa.Client_Hier_Id = ch.Client_Hier_Id        
            INNER JOIN dbo.ENTITY AS bs        
                  ON ba.status_type_id = bs.ENTITY_ID        
            INNER JOIN dbo.ENTITY AS service_level        
                  ON bsa.Account_Service_Level_Cd = service_level.ENTITY_ID        
            INNER JOIN dbo.Commodity Com        
                  ON ba.Commodity_Id = Com.Commodity_Id        
            LEFT JOIN dbo.user_info AS rc        
             ON ba.rates_completed_by = rc.user_info_id        
            LEFT JOIN dbo.user_info AS rr        
                  ON ba.rates_reviewed_by = rr.user_info_id        
        LEFT JOIN dbo.user_info AS sc        
                  ON ba.sourcing_completed_by = sc.user_info_id        
                WHERE   ( @ratescompleted = 0        
                          OR ( ( @ratescompleted = 1        
                                 AND ba.rates_completed_by IS NOT NULL        
                               )        
                               OR ( @ratescompleted = 2        
                                    AND ba.rates_completed_by IS NULL        
                                  )        
                             )        
                        )        
                        AND ( @ratesreviewed = 0        
                              OR ( ( @ratesreviewed = 1        
                                     AND ba.rates_reviewed_by IS NOT NULL        
                                   )        
                                   OR ( @ratesreviewed = 2        
                                        AND ba.rates_reviewed_by IS NULL        
            )        
                 )        
                         )        
                        AND ( @saId = 0        
                              OR ( ba.SOURCING_COMPLETED_BY = @saId        
                                   OR ( bsa.vcam_Analyst_Id IS NOT NULL        
                                        AND bsa.Tariff_Transport = ''Transport''        
                                      )        
                                 )        
                            )        
                        AND ( @raId = 0        
                              OR ( ba.RATES_COMPLETED_BY = @raId        
                                   OR ( bsa.udam_Analyst_ID IS NOT NULL        
                                        AND bsa.Tariff_Transport = ''Transport''        
                                      )        
                                 )        
                            )'  
                              
                              
                  SELECT
                        @ParamList = N'        
        @ServiceLvl_ID1 INT        
       ,@ServiceLvl_ID2 INT        
       ,@ratescompleted INT        
       ,@ratesreviewed INT        
       ,@saId INT        
       ,@raId INT  
       ,@total_cnt INT OUT'  
         
         
                  EXECUTE sp_executesql 
                        @SQLStatement_For_Row_Count
                       ,@ParamList
                       ,@ServiceLvl_ID1
                       ,@ServiceLvl_ID2
                       ,@ratescompleted
                       ,@ratesreviewed
                       ,@saId
                       ,@raId
                       ,@total_row_cnt OUT                          
                              
            END                        
                            
      
        
      SELECT
            @SQLStatement = ';WITH cte_BUDGET_GET_BUDGET_SEARCH_RESULTS        
   AS              
      (SELECT   TOP(' + CAST(@EndIndex AS VARCHAR(10)) + ')   
            ba.Budget_Id AS budgetId        
           ,ch.Client_Name AS clientName        
           ,RTRIM(ch.city) + '', '' + ch.state_name + '' ('' + ch.site_name + '')'' siteName        
           ,bsa.Account_Id AS accountId        
           ,bsa.Account_Number AS AccountNumber        
           ,bsa.Tariff_Transport        
           ,ba.DUE_DATE AS dueDate        
           ,CASE WHEN rc.FIRST_NAME IS NULL        
                      AND rc.LAST_NAME IS NULL        
                      AND bsa.Tariff_Transport = ''Tariff''        
                      AND bsa.Account_Service_level_Cd IN ( @ServiceLvl_ID1, @ServiceLvl_ID2 )        
                      AND ba.cd_create_multiplier IS NOT NULL THEN ''System''        
                 ELSE rc.FIRST_NAME + '' '' + rc.LAST_NAME        
            END AS raCompletedBy        
           ,ba.RATES_COMPLETED_DATE AS raCompletedDate        
           ,CASE WHEN rr.FIRST_NAME IS NULL        
                      AND rr.LAST_NAME IS NULL        
                      AND bsa.Tariff_Transport = ''Tariff''        
                      AND bsa.Account_Service_Level_Cd IN ( @ServiceLvl_ID1, @ServiceLvl_ID2 )        
                      AND ba.cd_create_multiplier IS NOT NULL THEN ''System''        
                 ELSE rr.FIRST_NAME + '' '' + rr.LAST_NAME        
            END AS raReviewedBy        
           ,ba.RATES_REVIEWED_DATE AS raReviewDate        
           ,CASE WHEN sc.FIRST_NAME IS NULL        
                      AND sc.LAST_NAME IS NULL        
                      AND bsa.Tariff_Transport = ''Tariff''        
                      AND bsa.Account_Service_Level_Cd IN ( @ServiceLvl_ID1, @ServiceLvl_ID2 )        
                      AND ba.cd_create_multiplier IS NOT NULL THEN ''System''        
                 ELSE sc.FIRST_NAME + '' '' + sc.LAST_NAME        
            END AS saCompletedBy        
           ,ba.sourcing_completed_date AS saCompletedDate        
           ,bs.ENTITY_NAME AS budgetStatus        
           ,com.Commodity_Name AS commodity        
          ,ba.BUDGET_START_YEAR AS startYear        
           ,ba.BUDGET_START_MONTH AS startMonth        
           ,ba.BUDGET_ACCOUNT_ID AS budgetAccountId        
           ,service_level.ENTITY_NAME AS service_level_char        
           ,ba.POSTED_BY AS isPosted        
           ,CASE WHEN bsa.Tariff_Transport = ''Tariff'' THEN CASE WHEN bsa.Account_Service_Level_Cd IN ( @ServiceLvl_ID1, @ServiceLvl_ID2 )        
                                                                   AND ba.cd_create_multiplier IS NULL THEN ''C&D''        
                                                              ELSE ''C&D Created''        
                                                         END        
                 ELSE ''A&B''        
            END AS budget_account_type        
           ,bsa.Account_Service_Level_Cd AS service_level_type_id        
           ,ba.STATUS_TYPE_ID      
                 
      FROM        
            #Budget_Accounts ba        
            INNER JOIN #Base_Accounts bsa         
      ON ba.Account_Id = bsa.Account_Id        
            INNER JOIN core.Client_Hier AS ch        
                  ON bsa.Client_Hier_Id = ch.Client_Hier_Id        
            INNER JOIN dbo.ENTITY AS bs        
                  ON ba.status_type_id = bs.ENTITY_ID        
            INNER JOIN dbo.ENTITY AS service_level        
                  ON bsa.Account_Service_Level_Cd = service_level.ENTITY_ID        
            INNER JOIN dbo.Commodity Com        
                  ON ba.Commodity_Id = Com.Commodity_Id        
            LEFT JOIN dbo.user_info AS rc        
             ON ba.rates_completed_by = rc.user_info_id        
            LEFT JOIN dbo.user_info AS rr        
                  ON ba.rates_reviewed_by = rr.user_info_id        
            LEFT JOIN dbo.user_info AS sc        
                  ON ba.sourcing_completed_by = sc.user_info_id        
                WHERE   ( @ratescompleted = 0        
                          OR ( ( @ratescompleted = 1        
                                 AND ba.rates_completed_by IS NOT NULL        
                               )        
                               OR ( @ratescompleted = 2        
                                    AND ba.rates_completed_by IS NULL        
                                  )        
                             )        
                        )        
                        AND ( @ratesreviewed = 0        
                              OR ( ( @ratesreviewed = 1        
                                     AND ba.rates_reviewed_by IS NOT NULL        
                                   )        
                                   OR ( @ratesreviewed = 2        
                    AND ba.rates_reviewed_by IS NULL        
                                      )        
                       )        
                            )        
                        AND ( @saId = 0        
                              OR ( ba.SOURCING_COMPLETED_BY = @saId        
                                   OR ( bsa.vcam_Analyst_Id IS NOT NULL        
                                        AND bsa.Tariff_Transport = ''Transport''        
                                      )        
                                 )        
                            )        
                        AND ( @raId = 0        
                              OR ( ba.RATES_COMPLETED_BY = @raId        
                                   OR ( bsa.udam_Analyst_ID IS NOT NULL        
                                        AND bsa.Tariff_Transport = ''Transport''        
                                      )        
                                 )        
                            ))      
                        ,cte_BUDGET_GET_BUDGET_SEARCH_FINAL_RESULTS      
                          AS      
                            (SELECT      
           budgetId      
          ,clientName      
          ,siteName      
          ,accountId      
          ,accountNumber      
   ,tariff_transport     
          ,dueDate      
          ,raCompletedBy      
          ,raCompletedDate      
          ,raReviewedBy      
          ,raReviewDate      
          ,saCompletedBy      
          ,saCompletedDate      
          ,budgetStatus      
          ,commodity      
          ,startYear      
          ,startMonth      
          ,budgetAccountId      
          ,service_level_char      
          ,isPosted      
          ,budget_account_type      
          ,service_level_type_id      
          ,status_type_id      
          ,Row_Num = ROW_NUMBER() OVER(ORDER BY ' + @fieldNameSort + ' ' + @sortOrder + ')      
                         
       FROM       
        cte_BUDGET_GET_BUDGET_SEARCH_RESULTS) '      
              
      SELECT
            @SQLStatement1 = 'SELECT      
           budgetId      
          ,clientName      
          ,siteName      
          ,accountId      
          ,accountNumber      
          ,tariff_transport      
          ,dueDate      
          ,raCompletedBy      
          ,raCompletedDate      
          ,raReviewedBy      
          ,raReviewDate      
          ,saCompletedBy      
          ,saCompletedDate      
          ,budgetStatus      
          ,commodity      
          ,startYear      
          ,startMonth      
          ,budgetAccountId      
          ,service_level_char      
          ,isPosted      
          ,budget_account_type      
          ,service_level_type_id      
          ,status_type_id      
          ,' + CAST(@total_row_cnt AS VARCHAR) + ' AS Total   
       
       FROM       
        cte_BUDGET_GET_BUDGET_SEARCH_FINAL_RESULTS      
        WHERE       
        Row_Num BETWEEN  ' + CAST(@StartIndex AS VARCHAR) + ' AND ' + CAST(@EndIndex AS VARCHAR)        
            
     
     
      SELECT
            @SQLStatment_Final = @SQLStatement + @SqlStatement1
    
      SELECT
            @ParamList = N'        
        @ServiceLvl_ID1 INT        
       ,@ServiceLvl_ID2 INT        
       ,@ratescompleted INT        
       ,@ratesreviewed INT        
       ,@saId INT        
       ,@raId INT'  
         
        
            
      EXECUTE sp_executesql 
            @SQLStatment_Final
           ,@ParamList
           ,@ServiceLvl_ID1
           ,@ServiceLvl_ID2
           ,@ratescompleted
           ,@ratesreviewed
           ,@saId
           ,@raId         
        
      IF OBJECT_ID('tempdb..#Budget_Accounts') IS NOT NULL 
            DROP TABLE #Budget_Accounts        
        
      IF OBJECT_ID('tempdb..#Base_Accounts') IS NOT NULL 
            DROP TABLE #Base_Accounts        

END;
;
GO




GRANT EXECUTE ON  [dbo].[BUDGET_GET_BUDGET_SEARCH_RESULTS_P] TO [CBMSApplication]
GO
