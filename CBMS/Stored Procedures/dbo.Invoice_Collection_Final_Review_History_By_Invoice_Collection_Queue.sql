SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Invoice_Collection_Final_Review_History_By_Invoice_Collection_Queue       
              
Description:              
			This sproc is to get the previously Final_Reviewed values in Invoice Collection Queue .      
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
	 @Invoice_Collection_Queue_Id		 INT
     @User_Info_Id						 INT                    
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

   Exec dbo.Invoice_Collection_Final_Review_History_By_Invoice_Collection_Queue '1,2',16
   
  
   
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2019-08-20		Created For Invoice_Collection.         
             
******/ 
CREATE PROCEDURE [dbo].[Invoice_Collection_Final_Review_History_By_Invoice_Collection_Queue]
      ( 
       @Invoice_Collection_Queue_Id VARCHAR(MAX)
      ,@User_Info_Id INT
      ,@Is_Final_Reviewed_Today BIT = NULL
      ,@Final_Review_From_Date DATE = NULL
      ,@Final_Review_To_Date DATE = NULL )
AS 
BEGIN
      SET NOCOUNT ON 
       
      SELECT
            CASE WHEN LEN(Final_Review.Period_to_Final_Review) > 0 THEN LEFT(Final_Review.Period_to_Final_Review, LEN(Final_Review.Period_to_Final_Review) - 1)
                 ELSE Final_Review.Period_to_Final_Review
            END AS Period_Final_Reviewed
           ,icccm.Last_Change_Ts Last_Final_Reviewed_Date
           ,ui.FIRST_NAME + ' ' + ui.LAST_NAME Final_Reviewed_By
           ,icccm.Contact_Name Final_Reviewed_To
           ,ISNULL(moc.Code_Value, 'Unknown') Method_Of_Contact
           ,icccm.Invoice_Final_Review_Comment
           ,icccm.Invoice_Collection_Final_Review_Log_Id
      FROM
            dbo.Invoice_Collection_Final_Review_Log_Queue_Map iccccmq
            INNER JOIN dbo.Invoice_Collection_Final_Review_Log icccm
                  ON iccccmq.Invoice_Collection_Final_Review_Log_Id = icccm.Invoice_Collection_Final_Review_Log_Id
            INNER JOIN dbo.Code sc
                  ON sc.Code_Id = icccm.Status_Cd
            INNER JOIN dbo.Invoice_Collection_Queue icq
                  ON icq.Invoice_Collection_Queue_Id = iccccmq.Invoice_Collection_Queue_Id
            CROSS APPLY ( SELECT
                              CAST(iccccmq1.Collection_Start_Dt AS VARCHAR(12)) + '#' + CAST(iccccmq1.Collection_End_Dt AS VARCHAR(12)) + ','
                          FROM
                              Invoice_Collection_Final_Review_Log_Queue_Map iccccmq1
                              INNER JOIN dbo.Invoice_Collection_Final_Review_Log icccm1
                                    ON iccccmq1.Invoice_Collection_Final_Review_Log_Id = icccm1.Invoice_Collection_Final_Review_Log_Id
                          WHERE
                              icccm1.Invoice_Collection_Final_Review_Log_Id = icccm.Invoice_Collection_Final_Review_Log_Id
                          GROUP BY
                              iccccmq1.Collection_Start_Dt
                             ,iccccmq1.Collection_End_Dt
            FOR
                          XML PATH('') ) Final_Review ( Period_to_Final_Review )
            LEFT JOIN dbo.Code moc
                  ON moc.Code_Id = icccm.Invoice_Collection_Method_Of_Contact_Cd
            INNER JOIN dbo.ufn_split(@Invoice_Collection_Queue_Id, ',') ufn
                  ON ufn.Segments = icq.Invoice_Collection_Queue_Id
            INNER JOIN dbo.USER_INFO ui
                  ON ui.USER_INFO_ID = icccm.Updated_User_Id
      WHERE
            sc.Code_Value = 'Close'
            AND ( @Is_Final_Reviewed_Today IS NULL
                  AND ( ( @Final_Review_From_Date IS NULL
                          OR icccm.Last_Change_Ts >= @Final_Review_From_Date )
                        AND ( @Final_Review_To_Date IS NULL
                              OR icccm.Last_Change_Ts <= @Final_Review_To_Date ) )
                  OR ( @Is_Final_Reviewed_Today = 1
                       AND CAST(icccm.Created_Ts AS DATE) = CAST(GETDATE() AS DATE) ) )
      GROUP BY
            CASE WHEN LEN(Final_Review.Period_to_Final_Review) > 0 THEN LEFT(Final_Review.Period_to_Final_Review, LEN(Final_Review.Period_to_Final_Review) - 1)
                 ELSE Final_Review.Period_to_Final_Review
            END
           ,icccm.Last_Change_Ts
           ,ui.FIRST_NAME + ' ' + ui.LAST_NAME
           ,icccm.Contact_Name
           ,ISNULL(moc.Code_Value, 'Unknown')
           ,icccm.Invoice_Final_Review_Comment
           ,icccm.Invoice_Collection_Final_Review_Log_Id
           ,ISNULL(icccm.Last_Change_Ts, icccm.Created_Ts)
      ORDER BY
            ISNULL(icccm.Last_Change_Ts, icccm.Created_Ts) DESC
           
			     
      
END;







;



;

;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Final_Review_History_By_Invoice_Collection_Queue] TO [CBMSApplication]
GO
