SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE dbo.UBM_DELETE_USAGE_BUCKET_MAPPING_P 
@userId varchar(10),
@sessionId varchar(20),
@ubmBucketName varchar(100),
@ubmId int
	
as
	set nocount on

	DELETE FROM UBM_USAGE_BUCKET_MAP 
	WHERE 
		UBM_USAGE_BUCKET_TYPE=@ubmBucketName
		and ubm_id = @ubmId

GO
GRANT EXECUTE ON  [dbo].[UBM_DELETE_USAGE_BUCKET_MAPPING_P] TO [CBMSApplication]
GO
