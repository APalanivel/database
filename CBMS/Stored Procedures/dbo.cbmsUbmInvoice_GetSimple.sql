SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [dbo].[cbmsUbmInvoice_GetSimple]
	( @MyAccountId int 
	, @ubm_invoice_id int
	)
AS
BEGIN

	   select i.ubm_invoice_id
		, i.invoice_identifier
		, i.cbms_image_id
		, i.ubm_account_id
		, i.ubm_account_number
		, i.ubm_vendor_id
		, i.ubm_vendor_name
		, i.ubm_client_id ubm_client_code
		, i.ubm_client_name
		, i.current_charges
	     from ubm_invoice i with (nolock) 
	    where i.ubm_invoice_id = @ubm_invoice_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsUbmInvoice_GetSimple] TO [CBMSApplication]
GO
