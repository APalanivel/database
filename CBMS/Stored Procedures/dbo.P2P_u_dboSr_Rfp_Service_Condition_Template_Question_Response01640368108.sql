SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[P2P_u_dboSr_Rfp_Service_Condition_Template_Question_Response01640368108]
		@c1 int = NULL,
		@c2 int = NULL,
		@c3 int = NULL,
		@c4 nvarchar(500) = NULL,
		@c5 int = NULL,
		@c6 datetime = NULL,
		@c7 int = NULL,
		@c8 datetime = NULL,
		@c9 nvarchar(max) = NULL,
		@c10 uniqueidentifier = NULL,
		@pkc1 int = NULL,
		@bitmap binary(2),
		@MSp2pPreVersion varbinary(32) , @MSp2pPostVersion varbinary(32) 

as
begin  
update [dbo].[Sr_Rfp_Service_Condition_Template_Question_Response] set
		[Sr_Rfp_Service_Condition_Template_Question_Map_Id] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [Sr_Rfp_Service_Condition_Template_Question_Map_Id] end,
		[SR_RFP_BID_ID] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [SR_RFP_BID_ID] end,
		[Response_Text_Value] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [Response_Text_Value] end,
		[Created_User_Id] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [Created_User_Id] end,
		[Created_Ts] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [Created_Ts] end,
		[Updated_User_Id] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [Updated_User_Id] end,
		[Last_Change_Ts] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [Last_Change_Ts] end,
		[Comment] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [Comment] end,
		[msrepl_tran_version] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [msrepl_tran_version] end		,$sys_p2p_cd_id = @MSp2pPostVersion

where [Sr_Rfp_Service_Condition_Template_Question_Response_Id] = @pkc1
		and ($sys_p2p_cd_id = @MSp2pPreVersion or $sys_p2p_cd_id is null)
if @@rowcount = 0
begin  
	declare @cur_version varbinary(32) 
		,@conflict_type int = 1
		,@conflict_type_txt nvarchar(20) = N'Update-Update'
		,@reason_code int = 1
		,@reason_text nvarchar(720) = NULL
		,@is_on_disk_winner bit = 0
		,@is_incoming_winner bit = 0
		,@peer_id_current_node int = NULL
		,@peer_id_incoming int
		,@tranid_incoming nvarchar(40)
		,@peer_id_on_disk int
		,@tranid_on_disk nvarchar(40)
	select @peer_id_incoming = sys.fn_replvarbintoint(@MSp2pPostVersion)
		,@tranid_incoming = sys.fn_replp2pversiontotranid(@MSp2pPostVersion)
	select @cur_version = $sys_p2p_cd_id from [dbo].[Sr_Rfp_Service_Condition_Template_Question_Response] 
where [Sr_Rfp_Service_Condition_Template_Question_Response_Id] = @pkc1
	if @@rowcount = 0  
			select @conflict_type = 3, @conflict_type_txt = N'Update-Delete', @is_on_disk_winner = 1, @reason_text = formatmessage(22823), @reason_code = 0
	else 
	begin  
		select @peer_id_on_disk = sys.fn_replvarbintoint(@cur_version)
			,@tranid_on_disk = sys.fn_replp2pversiontotranid(@cur_version)
		if(@peer_id_incoming > @peer_id_on_disk)
			set @is_incoming_winner = 1
		else
			set @is_on_disk_winner = 1
	end  
		select @peer_id_current_node = p.originator_id from syspublications p join sysarticles a on a.pubid = p.pubid where a.objid = object_id(N'[dbo].[Sr_Rfp_Service_Condition_Template_Question_Response]') and p.options & 0x1 = 0x1
	if (@peer_id_current_node is not null) 
	begin 
		if (@reason_text is NULL)
			if (@peer_id_incoming > @peer_id_on_disk)
			begin  
				select @reason_text = formatmessage(22822,@peer_id_incoming,@peer_id_on_disk,@peer_id_current_node)
			end  
			else  
			begin  
				select @reason_text = formatmessage(22821,@peer_id_incoming,@peer_id_on_disk,@peer_id_current_node)
			end  
		create table #change_id (change_id varbinary(8))
		insert [dbo].[conflict_dbo_Sr_Rfp_Service_Condition_Template_Question_Response] (
		[Sr_Rfp_Service_Condition_Template_Question_Response_Id],
		[Sr_Rfp_Service_Condition_Template_Question_Map_Id],
		[SR_RFP_BID_ID],
		[Response_Text_Value],
		[Created_User_Id],
		[Created_Ts],
		[Updated_User_Id],
		[Last_Change_Ts],
		[Comment],
		[msrepl_tran_version]
			,__$originator_id
			,__$origin_datasource
			,__$tranid
			,__$conflict_type
			,__$is_winner
			,__$reason_code
			,__$reason_text
			,__$update_bitmap
			,__$pre_version)
		output inserted.__$row_id into #change_id
		select 
    @pkc1,
    @c2,
    @c3,
    @c4,
    @c5,
    @c6,
    @c7,
    @c8,
    @c9,
    @c10			,@peer_id_current_node
			,@peer_id_incoming
			,@tranid_incoming
			,@conflict_type
			,@is_incoming_winner
			,@reason_code
			,@reason_text
			,@bitmap
			,@MSp2pPreVersion
		insert [dbo].[conflict_dbo_Sr_Rfp_Service_Condition_Template_Question_Response] (

		[Sr_Rfp_Service_Condition_Template_Question_Response_Id],
		[Sr_Rfp_Service_Condition_Template_Question_Map_Id],
		[SR_RFP_BID_ID],
		[Response_Text_Value],
		[Created_User_Id],
		[Created_Ts],
		[Updated_User_Id],
		[Last_Change_Ts],
		[Comment],
		[msrepl_tran_version]			,__$originator_id
			,__$origin_datasource
			,__$tranid
			,__$conflict_type
			,__$is_winner
			,__$reason_code
			,__$reason_text
			,__$pre_version
			,__$change_id)
		select 

		[Sr_Rfp_Service_Condition_Template_Question_Response_Id],
		[Sr_Rfp_Service_Condition_Template_Question_Map_Id],
		[SR_RFP_BID_ID],
		[Response_Text_Value],
		[Created_User_Id],
		[Created_Ts],
		[Updated_User_Id],
		[Last_Change_Ts],
		[Comment],
		[msrepl_tran_version]			,@peer_id_current_node
			,@peer_id_on_disk
			,@tranid_on_disk
			,@conflict_type
			,@is_on_disk_winner
			,@reason_code
			,@reason_text
			,NULL
			,(select change_id from #change_id)
		from [dbo].[Sr_Rfp_Service_Condition_Template_Question_Response] 

where [Sr_Rfp_Service_Condition_Template_Question_Response_Id] = @pkc1
	end 
	if(@peer_id_incoming > @peer_id_on_disk)
	begin  
update [dbo].[Sr_Rfp_Service_Condition_Template_Question_Response] set
		[Sr_Rfp_Service_Condition_Template_Question_Map_Id] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [Sr_Rfp_Service_Condition_Template_Question_Map_Id] end,
		[SR_RFP_BID_ID] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [SR_RFP_BID_ID] end,
		[Response_Text_Value] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [Response_Text_Value] end,
		[Created_User_Id] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [Created_User_Id] end,
		[Created_Ts] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [Created_Ts] end,
		[Updated_User_Id] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [Updated_User_Id] end,
		[Last_Change_Ts] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [Last_Change_Ts] end,
		[Comment] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [Comment] end,
		[msrepl_tran_version] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [msrepl_tran_version] end		,$sys_p2p_cd_id = @MSp2pPostVersion

where [Sr_Rfp_Service_Condition_Template_Question_Response_Id] = @pkc1
	end  
		if exists(select * from syspublications p join sysarticles a on a.pubid = p.pubid where a.objid = object_id(N'[dbo].[Sr_Rfp_Service_Condition_Template_Question_Response]') and p.options & 0x10 = 0x10)
			raiserror(22815, 10, -1, @conflict_type_txt, @peer_id_current_node, @peer_id_incoming, @tranid_incoming, @peer_id_on_disk, @tranid_on_disk) with log
		else
			raiserror(22815, 16, -1, @conflict_type_txt, @peer_id_current_node, @peer_id_incoming, @tranid_incoming, @peer_id_on_disk, @tranid_on_disk) with log
end 
end 
GO
