SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_UPDATE_BID_STATUS_FOR_ACCOUNT_TERMS_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@accountTermId                   int,
@selectedProductId               int,
@supplierContactVendorMapId      int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- check fo the original value at SV before and after update
--select bid_status from sr_rfp_term_product_map where	sr_rfp_account_term_id = 25820
--and sr_rfp_selected_products_id = 4024
--and  sr_rfp_supplier_contact_vendor_map_id = 36244
---- NULL value returned can be reset to 'Refresh'

---- Test Update an entry
--exec DBO.SR_RFP_UPDATE_BID_STATUS_FOR_ACCOUNT_TERMS_P
--@accountTermId = 25820,
--@selectedProductId = 4024,
--@supplierContactVendorMapId = 36244



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE DBO.SR_RFP_UPDATE_BID_STATUS_FOR_ACCOUNT_TERMS_P

@accountTermId int,
@selectedProductId int,
@supplierContactVendorMapId int

AS
	
SET NOCOUNT ON
	
update	sr_rfp_term_product_map set bid_status = 'Refresh'  	
where	sr_rfp_account_term_id = @accountTermId
	and sr_rfp_selected_products_id = @selectedProductId
	and  sr_rfp_supplier_contact_vendor_map_id = @supplierContactVendorMapId
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_UPDATE_BID_STATUS_FOR_ACCOUNT_TERMS_P] TO [CBMSApplication]
GO
