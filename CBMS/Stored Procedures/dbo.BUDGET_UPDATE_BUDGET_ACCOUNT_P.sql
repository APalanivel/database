
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.BUDGET_UPDATE_BUDGET_ACCOUNT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name					DataType		Default	Description
----------------------------------------------------------------
	@budget_account_id		INT
    @saUserId				INT
    @saCompletedDate		DATETIME
    @raUserId				INT
    @raCompletedDate		DATETIME
    @raReviewedUserId		INT
    @raReviewedDate			DATETIME
    @cannotPerformUserId	INT
    @cannotPerformDate		DATETIME
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
-----------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	--SELECT * FROM dbo.BUDGET WHERE BUDGET_ID = 8552
	--SELECT * FROM dbo.Code WHERE Codeset_Id = 164
	--BEGIN TRAN
	--	SELECT * FROM dbo.BUDGET_ACCOUNT AS ba INNER JOIN dbo.Budget_Account_Queue baq 
	--		ON ba.BUDGET_ACCOUNT_ID = baq.Budget_Account_Id WHERE BUDGET_ID = 8552
	--	UPDATE dbo.BUDGET SET Analyst_Queue_Display_Cd = 100579 WHERE BUDGET_ID = 8552
	--	EXEC BUDGET_UPDATE_BUDGET_ACCOUNT_P 8552
	--	SELECT * FROM dbo.BUDGET_ACCOUNT AS ba INNER JOIN dbo.Budget_Account_Queue baq 
	--		ON ba.BUDGET_ACCOUNT_ID = baq.Budget_Account_Id WHERE BUDGET_ID = 8552
	--ROLLBACK

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-05-26	Added header
							SE2017-26 - Modofied script to move/delete budget account in the queue
	
******/
CREATE   PROCEDURE [dbo].[BUDGET_UPDATE_BUDGET_ACCOUNT_P]
      ( 
       @budget_account_id INT
      ,@saUserId INT
      ,@saCompletedDate DATETIME
      ,@raUserId INT
      ,@raCompletedDate DATETIME
      ,@raReviewedUserId INT
      ,@raReviewedDate DATETIME
      ,@cannotPerformUserId INT
      ,@cannotPerformDate DATETIME )
AS 
BEGIN

      DECLARE
            @Rates_Queue_Cd INT
           ,@Both_Queue_Cd INT
           ,@Sourcing_Queue_Cd INT
           ,@OLD_RATES_COMPLETED_BY INT
           ,@OLD_RATES_REVIEWED_BY INT
           ,@OLD_SOURCING_COMPLETED_BY INT
           ,@OLD_CANNOT_PERFORMED_BY INT
           ,@Default_Analyst INT
           ,@Custom_Analyst INT
           ,@Budget_Id INT
          
      DECLARE
            @Lookup_Value XML
           ,@Client_Name VARCHAR(200)
           ,@User_Name VARCHAR(81)
           ,@Application_Name VARCHAR(30) = 'Budget Complete/Review'
           ,@Audit_Function SMALLINT = 1 --Update
           ,@Execution_Ts DATETIME = GETDATE()
           
      
      DECLARE @Group_Legacy_Name TABLE
            ( 
             GROUP_INFO_ID INT
            ,GROUP_NAME VARCHAR(200)
            ,Legacy_Group_Name VARCHAR(200) )  
            
      CREATE TABLE #Budget_Account_Queue
            ( 
             Budget_Account_Id INT
            ,Analyst_Type_Cd INT
            ,Queue_Id INT )
            
      INSERT      INTO @Group_Legacy_Name
                  ( 
                   GROUP_INFO_ID
                  ,GROUP_NAME
                  ,Legacy_Group_Name )
                  EXEC dbo.Group_Info_Sel_By_Group_Legacy 
                        @Group_Legacy_Name_Cd_Value = 'Regulated_Markets_Budgets'
                        
      SELECT
            @Rates_Queue_Cd = c.Code_Id
      FROM
            dbo.Code AS c
            JOIN dbo.Codeset AS cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'IncludeInAnalystQueue'
            AND c.Code_Value = 'Yes - Rates only'
  
      SELECT
            @Both_Queue_Cd = c.Code_Id
      FROM
            dbo.Code AS c
            JOIN dbo.Codeset AS cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'IncludeInAnalystQueue'
            AND c.Code_Value = 'Yes - Rates and Sourcing'

      SELECT
            @Sourcing_Queue_Cd = c.Code_Id
      FROM
            dbo.Code AS c
            JOIN dbo.Codeset AS cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'IncludeInAnalystQueue'
            AND c.Code_Value = 'Yes - Sourcing only'
            
      SELECT
            @Default_Analyst = MAX(CASE WHEN c.Code_Value = 'Default' THEN c.Code_Id
                                   END)
           ,@Custom_Analyst = MAX(CASE WHEN c.Code_Value = 'Custom' THEN c.Code_Id
                                  END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Analyst Type'
            
      SELECT
            @OLD_RATES_COMPLETED_BY = RATES_COMPLETED_BY
           ,@OLD_RATES_REVIEWED_BY = RATES_REVIEWED_BY
           ,@OLD_SOURCING_COMPLETED_BY = SOURCING_COMPLETED_BY
           ,@OLD_CANNOT_PERFORMED_BY = CANNOT_PERFORMED_BY
           ,@Budget_Id = BUDGET_ID
      FROM
            dbo.BUDGET_ACCOUNT
      WHERE
            BUDGET_ACCOUNT_ID = @budget_account_id
            
      SET @Lookup_Value = ( SELECT
                              ba.BUDGET_ACCOUNT_ID AS BUDGET_ACCOUNT_ID
                             ,ba.RATES_COMPLETED_BY AS RATES_COMPLETED_BY
                             ,ba.RATES_COMPLETED_DATE AS RATES_COMPLETED_DATE
                             ,ba.RATES_REVIEWED_BY AS RATES_REVIEWED_BY
                             ,ba.RATES_REVIEWED_DATE AS RATES_REVIEWED_DATE
                             ,ba.SOURCING_COMPLETED_BY AS SOURCING_COMPLETED_BY
                             ,ba.SOURCING_COMPLETED_DATE AS SOURCING_COMPLETED_DATE
                             ,ba.CANNOT_PERFORMED_BY AS CANNOT_PERFORMED_BY
                             ,ba.CANNOT_PERFORMED_DATE AS CANNOT_PERFORMED_DATE
                             ,baq.Queue_Id AS Queue_Id
                             ,cd.Code_Value AS Queue_Type
                             ,@Execution_Ts AS Log_Time
                            FROM
                              dbo.BUDGET_ACCOUNT ba
                              LEFT JOIN dbo.Budget_Account_Queue baq
                                    ON ba.BUDGET_ACCOUNT_ID = baq.Budget_Account_Id
                              LEFT JOIN dbo.Code cd
                                    ON baq.Analyst_Type_Cd = cd.Code_Id
                            WHERE
                              ba.BUDGET_ACCOUNT_ID = @budget_account_id
            FOR
                            XML AUTO )  
      
      UPDATE
            budget_account
      SET   
            sourcing_completed_date = @saCompletedDate
           ,sourcing_completed_by = @saUserId
           ,rates_completed_date = @raCompletedDate
           ,rates_completed_by = @raUserId
           ,rates_reviewed_date = @raReviewedDate
           ,rates_reviewed_by = @raReviewedUserId
           ,cannot_performed_date = @cannotPerformDate
           ,cannot_performed_by = @cannotPerformUserId
           ,has_details_saved = 1
      WHERE
            budget_account_id = @budget_account_id
            
            
--->Rates Budget - Rates Completed(Analyst Complete) - Queue priority - First Budget Reviewer otherwise Regional Manager
      UPDATE
            baq
      SET   
            baq.Queue_Id = ISNULL(rev.QUEUE_ID, rm.QUEUE_ID)
           ,baq.Routed_By_User_Id = @raUserId
           ,baq.Routed_Ts = GETDATE()
           ,baq.Last_Change_Ts = GETDATE()
      FROM
            dbo.Budget_Account_Queue baq
            INNER JOIN dbo.BUDGET_ACCOUNT ba
                  ON baq.Budget_Account_Id = ba.BUDGET_ACCOUNT_ID
            INNER JOIN Core.Client_Hier_Account cha
                  ON ba.ACCOUNT_ID = cha.Account_Id
                                    ---RM Reveiwer
            LEFT JOIN ( dbo.UTILITY_DETAIL AS revud
                        INNER JOIN dbo.Utility_Detail_Analyst_Map revudam
                              ON revud.UTILITY_DETAIL_ID = revudam.Utility_Detail_ID
                        INNER JOIN dbo.GROUP_INFO revgi
                              ON revudam.Group_Info_ID = revgi.GROUP_INFO_ID
                                 AND revgi.GROUP_NAME = 'Budget Reviewer Queue'
                        INNER JOIN dbo.USER_INFO rev
                              ON revudam.Analyst_ID = rev.USER_INFO_ID )
                        ON revud.VENDOR_ID = cha.Account_Vendor_Id
                                    ---Regional Manager
            LEFT JOIN ( dbo.REGION_MANAGER_MAP AS rmm
                        INNER JOIN dbo.STATE AS s
                              ON rmm.REGION_ID = s.REGION_ID
                        INNER JOIN dbo.VENDOR_STATE_MAP AS vsm
                              ON s.STATE_ID = vsm.STATE_ID
                        INNER JOIN dbo.USER_INFO rm
                              ON rmm.USER_INFO_ID = rm.USER_INFO_ID )
                        ON vsm.VENDOR_ID = cha.Account_Vendor_Id
      WHERE
            ba.BUDGET_ACCOUNT_ID = @budget_account_id
            AND baq.Analyst_Type_Cd = @Rates_Queue_Cd
            AND @raUserId IS NOT NULL
            AND @raCompletedDate IS NOT NULL
            AND @OLD_RATES_COMPLETED_BY IS NULL
            AND @raReviewedUserId IS NULL
            AND @raReviewedDate IS NULL 


--->Rates Budget - Rates Reviewed - Budget account no longer appears in any rates queue
      DELETE
            baq
      FROM
            dbo.Budget_Account_Queue baq
            INNER JOIN dbo.BUDGET_ACCOUNT ba
                  ON baq.Budget_Account_Id = ba.BUDGET_ACCOUNT_ID
            INNER JOIN Core.Client_Hier_Account cha
                  ON ba.ACCOUNT_ID = cha.Account_Id
      WHERE
            ba.BUDGET_ACCOUNT_ID = @budget_account_id
            AND baq.Analyst_Type_Cd = @Rates_Queue_Cd
            AND @raReviewedUserId IS NOT NULL
            AND @raReviewedDate IS NOT NULL
            
            
--->Sourcing Budget - Sourcing Complete - Budget account no longer appears in any sourcing queue
      DELETE
            baq
      FROM
            dbo.Budget_Account_Queue baq
            INNER JOIN dbo.BUDGET_ACCOUNT ba
                  ON baq.Budget_Account_Id = ba.BUDGET_ACCOUNT_ID
            INNER JOIN Core.Client_Hier_Account cha
                  ON ba.ACCOUNT_ID = cha.Account_Id
      WHERE
            ba.BUDGET_ACCOUNT_ID = @budget_account_id
            AND @saUserId IS NOT NULL
            AND @saCompletedDate IS NOT NULL
            AND baq.Analyst_Type_Cd = @Sourcing_Queue_Cd
            

--->Budget - Cannot Perform Checked - Budget account no longer appears in any queue
      DELETE
            baq
      FROM
            dbo.Budget_Account_Queue baq
            INNER JOIN dbo.BUDGET_ACCOUNT ba
                  ON baq.Budget_Account_Id = ba.BUDGET_ACCOUNT_ID
            INNER JOIN Core.Client_Hier_Account cha
                  ON ba.ACCOUNT_ID = cha.Account_Id
      WHERE
            ba.BUDGET_ACCOUNT_ID = @budget_account_id
            AND @cannotPerformUserId IS NOT NULL
            AND @cannotPerformDate IS NOT NULL
            


--->Rates Budget - Rates Complate un-checked or both Rates Complate and Rates Reviewed un-checked
	--Account goes back to analyst queue - Queue priority - First RM Analyst otherwise Regional Manager
      MERGE INTO dbo.Budget_Account_Queue AS tgt
            USING 
                  ( SELECT
                        ba.BUDGET_ACCOUNT_ID AS Budget_Account_Id
                       ,@Rates_Queue_Cd AS Analyst_Type_Cd
                       ,ISNULL(ra.QUEUE_ID, rm.QUEUE_ID) AS Queue_Id
                       ,GETDATE() AS Created_Ts
                       ,GETDATE() AS Last_Change_Ts
                       ,GETDATE() AS Routed_Ts
                    FROM
                        dbo.BUDGET_ACCOUNT ba
                        INNER JOIN Core.Client_Hier_Account cha
                              ON ba.ACCOUNT_ID = cha.Account_Id
                                ---RM Analyst
                        LEFT JOIN ( dbo.UTILITY_DETAIL AS raud
                                    INNER JOIN dbo.Utility_Detail_Analyst_Map raudam
                                          ON raud.UTILITY_DETAIL_ID = raudam.Utility_Detail_ID
                                    INNER JOIN dbo.GROUP_INFO ragi
                                          ON raudam.Group_Info_ID = ragi.GROUP_INFO_ID
                                    INNER JOIN @Group_Legacy_Name gil
                                          ON ragi.GROUP_INFO_ID = gil.GROUP_INFO_ID
                                    INNER JOIN dbo.USER_INFO ra
                                          ON raudam.Analyst_ID = ra.USER_INFO_ID )
                                    ON raud.VENDOR_ID = cha.Account_Vendor_Id
                                ---Regional Manager
                        LEFT JOIN ( dbo.REGION_MANAGER_MAP AS rmm
                                    INNER JOIN dbo.STATE AS s
                                          ON rmm.REGION_ID = s.REGION_ID
                                    INNER JOIN dbo.VENDOR_STATE_MAP AS vsm
                                          ON s.STATE_ID = vsm.STATE_ID
                                    INNER JOIN dbo.USER_INFO rm
                                          ON rmm.USER_INFO_ID = rm.USER_INFO_ID )
                                    ON vsm.VENDOR_ID = cha.Account_Vendor_Id
                    WHERE
                        ba.IS_DELETED = 0
                        AND ba.BUDGET_ACCOUNT_ID = @budget_account_id
                        AND @raUserId IS NULL
                        AND @raCompletedDate IS NULL
                        AND @raReviewedUserId IS NULL
                        AND @raReviewedDate IS NULL
                        AND ( @OLD_RATES_COMPLETED_BY IS NOT NULL
                              OR @OLD_RATES_REVIEWED_BY IS NOT NULL )
                    GROUP BY
                        ba.BUDGET_ACCOUNT_ID
                       ,ISNULL(ra.QUEUE_ID, rm.QUEUE_ID) ) src
            ON ( tgt.Budget_Account_Id = src.Budget_Account_Id
                 AND tgt.Analyst_Type_Cd = src.Analyst_Type_Cd )
            WHEN NOT MATCHED 
                  THEN INSERT
                        ( 
                         Budget_Account_Id
                        ,Analyst_Type_Cd
                        ,Queue_Id
                        ,Created_Ts
                        ,Last_Change_Ts )
                    VALUES
                        ( 
                         src.Budget_Account_Id
                        ,src.Analyst_Type_Cd
                        ,src.Queue_Id
                        ,src.Created_Ts
                        ,src.Last_Change_Ts )
            WHEN MATCHED 
                  THEN UPDATE
                    SET 
                        tgt.Queue_Id = src.Queue_Id
                       ,tgt.Routed_Ts = src.Routed_Ts
                       ,tgt.Last_Change_Ts = src.Last_Change_Ts;
                       
                       
 --->Rates Budget - Rates Reviwed un-checked 
	--Account goes back to reviwer queue - Queue priority - First Reviewer otherwise Regional Manager
	
      INSERT      INTO dbo.Budget_Account_Queue
                  ( 
                   Budget_Account_Id
                  ,Analyst_Type_Cd
                  ,Queue_Id
                  ,Created_Ts
                  ,Last_Change_Ts )
                  SELECT
                        @budget_account_id AS Budget_Account_Id
                       ,@Rates_Queue_Cd AS Analyst_Type_Cd
                       ,ISNULL(rev.QUEUE_ID, rm.QUEUE_ID) AS Queue_Id
                       ,GETDATE()
                       ,GETDATE()
                  FROM
                        dbo.BUDGET_ACCOUNT ba
                        INNER JOIN Core.Client_Hier_Account cha
                              ON ba.ACCOUNT_ID = cha.Account_Id
                                    ---RM Reveiwer
                        LEFT JOIN ( dbo.UTILITY_DETAIL AS revud
                                    INNER JOIN dbo.Utility_Detail_Analyst_Map revudam
                                          ON revud.UTILITY_DETAIL_ID = revudam.Utility_Detail_ID
                                    INNER JOIN dbo.GROUP_INFO revgi
                                          ON revudam.Group_Info_ID = revgi.GROUP_INFO_ID
                                             AND revgi.GROUP_NAME = 'Budget Reviewer Queue'
                                    INNER JOIN dbo.USER_INFO rev
                                          ON revudam.Analyst_ID = rev.USER_INFO_ID )
                                    ON revud.VENDOR_ID = cha.Account_Vendor_Id
                                    ---Regional Manager
                        LEFT JOIN ( dbo.REGION_MANAGER_MAP AS rmm
                                    INNER JOIN dbo.STATE AS s
                                          ON rmm.REGION_ID = s.REGION_ID
                                    INNER JOIN dbo.VENDOR_STATE_MAP AS vsm
                                          ON s.STATE_ID = vsm.STATE_ID
                                    INNER JOIN dbo.USER_INFO rm
                                          ON rmm.USER_INFO_ID = rm.USER_INFO_ID )
                                    ON vsm.VENDOR_ID = cha.Account_Vendor_Id
                  WHERE
                        ba.IS_DELETED = 0
                        AND ba.BUDGET_ACCOUNT_ID = @budget_account_id
                        AND ( @OLD_RATES_COMPLETED_BY IS  NOT NULL
                              AND @OLD_RATES_REVIEWED_BY IS NOT NULL )
                        AND @raUserId IS NOT NULL
                        AND @raCompletedDate IS NOT NULL
                        AND @raReviewedUserId IS NULL
                        AND @raReviewedDate IS NULL
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Budget_Account_Queue baqrev
                                         WHERE
                                          baqrev.Budget_Account_Id = ba.BUDGET_ACCOUNT_ID
                                          AND baqrev.Analyst_Type_Cd = @Rates_Queue_Cd )

                        
                       
 --->Sourcing Budget - Sourcing Complate un-checked, account goes back to sourcing analyst queue 
	--Queue priority - Souricng Analyst only, no analyst found then budget in no queue
      INSERT      INTO dbo.Budget_Account_Queue
                  ( 
                   Budget_Account_Id
                  ,Analyst_Type_Cd
                  ,Queue_Id
                  ,Created_Ts
                  ,Last_Change_Ts )
                  SELECT
                        ba.BUDGET_ACCOUNT_ID
                       ,@Sourcing_Queue_Cd
                       ,ui.QUEUE_ID
                       ,GETDATE()
                       ,GETDATE()
                  FROM
                        dbo.BUDGET b
                        INNER JOIN dbo.BUDGET_ACCOUNT ba
                              ON b.BUDGET_ID = ba.BUDGET_ID
                        INNER JOIN Core.Client_Hier_Account cha
                              ON ba.ACCOUNT_ID = cha.Account_Id
                                 AND cha.Commodity_Id = b.COMMODITY_TYPE_ID
                        INNER JOIN Core.Client_Hier CH
                              ON cha.Client_Hier_Id = CH.Client_Hier_Id
                        INNER JOIN dbo.VENDOR_COMMODITY_MAP AS vcm
                              ON vcm.VENDOR_ID = cha.Account_Vendor_Id
                                 AND vcm.COMMODITY_TYPE_ID = cha.Commodity_Id
                        INNER JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                              ON vcam.Vendor_Commodity_Map_Id = vcm.VENDOR_COMMODITY_MAP_ID
                        INNER JOIN core.Client_Commodity ccc
                              ON ccc.Client_Id = ch.Client_Id
                                 AND ccc.Commodity_Id = cha.Commodity_Id
                        LEFT JOIN dbo.Account_Commodity_Analyst aca
                              ON aca.Account_Id = cha.Account_Id
                                 AND aca.Commodity_Id = cha.Commodity_Id
                        LEFT JOIN dbo.SITE_Commodity_Analyst sca
                              ON sca.Site_Id = ch.Site_Id
                                 AND sca.Commodity_Id = cha.Commodity_Id
                        LEFT JOIN dbo.Client_Commodity_Analyst cca
                              ON ccc.Client_Commodity_Id = cca.Client_Commodity_Id
                        INNER JOIN dbo.USER_INFO ui
                              ON ui.USER_INFO_ID = CASE WHEN COALESCE(cha.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) = @Custom_Analyst THEN COALESCE(aca.Analyst_User_Info_Id, sca.Analyst_User_Info_Id, cca.Analyst_User_Info_Id)
                                                        ELSE vcam.Analyst_ID
                                                   END
                  WHERE
                        ba.BUDGET_ACCOUNT_ID = @Budget_Account_Id
                        AND b.Analyst_Queue_Display_Cd IN ( @Both_Queue_Cd, @Sourcing_Queue_Cd )
                        AND ba.IS_DELETED = 0
                        AND @OLD_SOURCING_COMPLETED_BY IS NOT NULL
                        AND @saUserId IS NULL
                        AND @saCompletedDate IS NULL
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Budget_Account_Queue baqsrc
                                         WHERE
                                          baqsrc.Budget_Account_Id = ba.BUDGET_ACCOUNT_ID
                                          AND baqsrc.Analyst_Type_Cd = @Sourcing_Queue_Cd )
                  GROUP BY
                        ba.BUDGET_ACCOUNT_ID
                       ,ui.QUEUE_ID
                      
 --->Budget - Cannot Perform Unchecked - Budget accounts will goes to respective queue(s)
                      
      IF ( @OLD_CANNOT_PERFORMED_BY IS NOT NULL
           AND @cannotPerformUserId IS NULL
           AND @cannotPerformDate IS NULL ) 
            BEGIN 
                  EXEC dbo.Budget_Account_Queue_Ins_Upd 
                        @Budget_Id = @Budget_Id
                       ,@Budget_Account_Id = @Budget_Account_Id
            
            END 

            
      SELECT
            @User_Name = FIRST_NAME + SPACE(1) + LAST_NAME
      FROM
            dbo.USER_INFO
      WHERE
            USER_INFO_ID = COALESCE(@saUserId, @raUserId, @raReviewedUserId, @cannotPerformUserId)  
            
      SELECT
            @Client_Name = ch.Client_Name
      FROM
            dbo.BUDGET_ACCOUNT ba
            INNER JOIN Core.Client_Hier_Account cha
                  ON ba.ACCOUNT_ID = cha.Account_Id
            INNER JOIN Core.Client_Hier ch
                  ON cha.Client_Hier_Id = ch.Client_Hier_Id
      WHERE
            ba.BUDGET_ACCOUNT_ID = @budget_account_id
            
      EXEC dbo.Application_Audit_Log_Ins 
            @Client_Name
           ,@Application_Name
           ,@Audit_Function
           ,'dbo.Budget_Account_Queue'
           ,@Lookup_Value
           ,@User_Name
           ,@Execution_Ts
                  
END







;
GO

GRANT EXECUTE ON  [dbo].[BUDGET_UPDATE_BUDGET_ACCOUNT_P] TO [CBMSApplication]
GO
