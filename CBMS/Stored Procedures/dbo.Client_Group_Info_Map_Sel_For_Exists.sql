SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
    
              
/******                                
NAME:                                
dbo.Client_Group_Info_Map_Sel_For_Exists                            
                               
DESCRIPTION:                                 
This procedure is to check whether the client has atleast a function with  
'ra.reviewdocuments' permission                          
                               
INPUT PARAMETERS:                                
  Name               DataType	Default    Description                                
------------------------------------------------------------------                                       
  @Client_Id		 INT        
                              
                             
                               
OUTPUT PARAMETERS:                                
 Name				DataType	Default   Description                                
------------------------------------------------------------------                               
                  
                             
USAGE EXAMPLES:                                
------------------------------------------------------------------                                 
 EXEC dbo.Client_Group_Info_Map_Sel_For_Exists 11278                 
------------------------------------------------------------------                                 
 EXEC dbo.Client_Group_Info_Map_Sel_For_Exists 10069                 
                        
                              
AUTHOR INITIALS:                                
 Initials	Name                                
-------------------------------------------------------------------                                
SS			Sani Soman                             
 
 MODIFICATIONS:                                 
 Initials	Date		Modification                                
------------------------------------------------------------------                                
 SS			2016/07/22	Created   
******/                             
                            
CREATE PROCEDURE [dbo].[Client_Group_Info_Map_Sel_For_Exists] 
( @Client_Id INT )
AS 
BEGIN                            
                             
      SET NOCOUNT ON;                    
                                      

      SELECT
            1 AS Is_Exists
      WHERE
            EXISTS ( SELECT
                        1
                     FROM
                        dbo.GROUP_INFO_PERMISSION_INFO_MAP gipi
                        JOIN dbo.PERMISSION_INFO p
                              ON gipi.PERMISSION_INFO_ID = p.PERMISSION_INFO_ID
                        JOIN dbo.Client_Group_Info_Map cgim
                              ON cgim.Group_Info_Id = gipi.GROUP_INFO_ID
                     WHERE
                        P.PERMISSION_NAME = 'ra.reviewdocuments'
                        AND cgim.Client_Id = @Client_Id )              
                      
END;




;

;
GO
GRANT EXECUTE ON  [dbo].[Client_Group_Info_Map_Sel_For_Exists] TO [CBMSApplication]
GO
