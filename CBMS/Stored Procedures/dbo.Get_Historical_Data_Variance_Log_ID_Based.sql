SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO















/*********     
NAME:    [dbo].[Get_Historical_Data_Variance_Log_ID_Based]  
   
DESCRIPTION: procedure is created to get the historical data value
  
INPUT PARAMETERS:      
      Name              DataType          Default     Description      
------------------------------------------------------------      
    
          
      
OUTPUT PARAMETERS:      
      Name              DataType          Default     Description      
------------------------------------------------------------      
      
USAGE EXAMPLES:     
  
 
EXEC Get_Historical_Data_Variance_Log_ID_Based 1218724003
  
------------------------------------------------------------    
AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
TRK Ramakrishna Thummala  
AP Arunkumar Palanivel  
  
Initials Date  Modification    
------------------------------------------------------------    

AP				Feb 27,2020
  
******/
CREATE PROCEDURE [dbo].[Get_Historical_Data_Variance_Log_ID_Based]
      (
      @Variance_Log_Id INT )
AS
      BEGIN

            SET NOCOUNT ON;

            DECLARE
                  @commodity_id             INT
                , @Account_id               INT
                , @service_Month            DATE
                , @Category_Name            VARCHAR(20)
                , @Bucket_Master_Id         INT
                , @UOM_Code                 VARCHAR(10)
                , @Site_id                  INT
                , @state_id                 INT
                , @Client_Id                INT
                , @Commodity_name           VARCHAR(50)
                , @currency_unit_id         INT
                , @Client_Currency_Group_Id INT;



            SELECT
                  @commodity_id = Commodity_ID
                , @Account_id = Account_ID
                , @service_Month = Service_Month
                , @Category_Name = Category_Name
                , @UOM_Code = UOM_Label
                , @Site_id = Site_ID
                , @state_id = State_ID
                , @Client_Id = Client_ID
            FROM  dbo.Variance_Log
            WHERE Variance_Log_Id = @Variance_Log_Id;
            SELECT
                  @currency_unit_id = cucm.CURRENCY_UNIT_ID
                , @Client_Currency_Group_Id = ch.Client_Currency_Group_Id
            FROM  dbo.CURRENCY_UNIT_COUNTRY_MAP cucm
                  JOIN
                  dbo.CURRENCY_UNIT cu
                        ON cu.CURRENCY_UNIT_ID = cucm.CURRENCY_UNIT_ID
                  INNER JOIN
                  Core.Client_Hier ch
                        ON ch.Country_Id = cucm.COUNTRY_ID
                  JOIN
                  Core.Client_Hier_Account cha
                        ON cha.Client_Hier_Id = ch.Client_Hier_Id
            WHERE cha.Account_Id = @Account_id;
            SET @currency_unit_id = isnull(@currency_unit_id, 3);

            SELECT
                  @Commodity_name = cm.Commodity_Name
            FROM  dbo.Commodity cm
            WHERE cm.Commodity_Id = @commodity_id;

            SELECT
                  @Bucket_Master_Id = vcbm.Bucket_master_Id
            FROM  dbo.Variance_category_Bucket_Map vcbm
                  JOIN
                  dbo.Bucket_Master bm
                        ON bm.Bucket_Master_Id = vcbm.Bucket_master_Id
                  JOIN
                  dbo.Code c
                        ON vcbm.Variance_Category_Cd = c.Code_Id
            WHERE c.Code_Value = @Category_Name
                  AND   bm.Commodity_Id = @commodity_id;

            SELECT
                  vl.Account_Number AS [Account]
                , vl.Category_Name
                , vl.Service_Month
                , vl.Site_Name AS [Site]
                , vl.Vendor_name AS [Vendor]
                , vl.Consumtion_Level_Desc [Average Consumption Level]
                , vl.Queue_Dt AS [Date Opened]
                , @Site_id AS Site_id
                , @state_id AS State_id
                , @Client_Id AS Client_id
                , @Account_id AS Account_id
                , @Commodity_name AS commodity_name
                , vl.AVT_R2_Score
            FROM  dbo.Variance_Log vl
            WHERE vl.Variance_Log_Id = @Variance_Log_Id;

            SELECT
                        A.Service_Month
                      , cast(A.current_Value AS VARCHAR(40)) + ' ' + @UOM_Code AS [CURRENT]
                      , cast(V.Baseline_Value AS VARCHAR(40)) + ' ' + @UOM_Code AS [Baseline]
                      , cast(V.Low_Tolerance AS VARCHAR(40)) + ' ' + @UOM_Code AS [Low]
                      , cast(V.High_Tolerance AS VARCHAR(40)) + ' ' + @UOM_Code AS [High]
            FROM
                        (     SELECT      DISTINCT   ( CASE WHEN cst.Code_Value = 'Charge'
                                                                  THEN cuad.Bucket_Value * isnull(curconv.CONVERSION_FACTOR, 0)
                                                            WHEN cst.Code_Value = 'Determinant'
                                                                  THEN cuad.Bucket_Value * isnull(uomconv.CONVERSION_FACTOR, 0)
                                                            ELSE  0
                                                       END ) AS current_Value
                                                   , cuad.Service_Month
                                                   , c.Code_Value
                              FROM        dbo.Cost_Usage_Account_Dtl cuad
                                          JOIN
                                          dbo.Bucket_Master bm
                                                ON bm.Bucket_Master_Id = cuad.Bucket_Master_Id
                                          JOIN
                                          dbo.Variance_category_Bucket_Map vcbm
                                                ON vcbm.Bucket_master_Id = bm.Bucket_Master_Id
                                          JOIN
                                          dbo.Code c
                                                ON c.Code_Id = vcbm.Variance_Category_Cd
                                          JOIN
                                          dbo.Code cst
                                                ON cst.Code_Id = bm.Bucket_Type_Cd
                                          LEFT OUTER JOIN
                                          dbo.CURRENCY_UNIT_CONVERSION curconv
                                                ON curconv.BASE_UNIT_ID = cuad.CURRENCY_UNIT_ID
                                                   AND curconv.CONVERSION_DATE = cuad.Service_Month
                                                   AND curconv.CURRENCY_GROUP_ID = @Client_Currency_Group_Id
                                                   AND curconv.CONVERTED_UNIT_ID = @currency_unit_id --@currency_Unit_Id
                                                   AND cst.Code_Value = 'Charge'
                                          LEFT OUTER JOIN
                                          dbo.CONSUMPTION_UNIT_CONVERSION uomconv
                                                ON uomconv.BASE_UNIT_ID = cuad.UOM_Type_Id
                                                   AND uomconv.CONVERTED_UNIT_ID = bm.Default_Uom_Type_Id
                                                   AND cst.Code_Value = 'Determinant'
                                          LEFT OUTER JOIN
                                          dbo.CURRENCY_UNIT cu
                                                ON cu.CURRENCY_UNIT_ID = @currency_unit_id --@currency_Unit_Id
                                          LEFT OUTER JOIN
                                          dbo.ENTITY uom
                                                ON uom.ENTITY_ID = bm.Default_Uom_Type_Id
                              WHERE       cuad.ACCOUNT_ID = @Account_id
                                          AND   bm.Commodity_Id = @commodity_id
                                          AND   cuad.Bucket_Master_Id = @Bucket_Master_Id ) A
                        JOIN
                        dbo.Variance_Historical_Data_Point V
                              ON V.Service_Month = A.Service_Month
            WHERE       V.Variance_Log_Id = @Variance_Log_Id
            ORDER BY    A.Service_Month;


      END;


GO
GRANT EXECUTE ON  [dbo].[Get_Historical_Data_Variance_Log_ID_Based] TO [CBMSApplication]
GO
