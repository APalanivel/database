
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME: dbo.UBM_GET_FTP_ARCHIVE_LOCATIONS_P
      
DESCRIPTION:      
      
 This procedure is used to get the FTP locations used by the Java UBM Batch process

INPUT PARAMETERS:         
    Name			DataType				Default     Description            
------------------------------------------------------------------            
    
OUTPUT PARAMETERS:    
      Name			DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.UBM_GET_FTP_ARCHIVE_LOCATIONS_P  16, -1
	    
AUTHOR INITIALS:
Initials	Name     
------------------------------------------------------------        
HG			Harihara Suthan G

MODIFICATIONS

Initials	Date		Modification
-----------------------------------------------------------
HG			2014-09-24	Comments header added and modified for UBM Rename Prokarma to Schneider Electric and addition of new UBM Prokarma

******/
CREATE PROCEDURE [dbo].[UBM_GET_FTP_ARCHIVE_LOCATIONS_P]
      @userId VARCHAR
     ,@sessionId VARCHAR
AS 
BEGIN

      SET NOCOUNT ON

      SELECT
            Entity_ID
           ,Entity_Name Locations
      FROM
            dbo.ENTITY
      WHERE
            Entity_Description IN ( 'UBM_CASS_FTP_LOCATION', 'UBM_CASS_FTP_ARCHIVE_LOCATION', 'UBM_AVISTA_FTP_LOCATION', 'UBM_AVISTA_FTP_ARCHIVE_LOCATION', 'UBM_FTP_SERVER_NAME', 'UBM_FTP_USERNAME', 'UBM_FTP_PASSWORD', 'UBM_PROKARMA_FTP_LOCATION', 'UBM_PROKARMA_FTP_ARCHIVE_LOCATION', 'UBM_AS400_FTP_LOCATION', 'UBM_AS400_FTP_ARCHIVE_LOCATION', 'UBM_SCHNEIDER_FTP_LOCATION', 'UBM_SCHNEIDER_FTP_ARCHIVE_LOCATION' )
      ORDER BY
            Entity_Type ASC

END

;
GO

GRANT EXECUTE ON  [dbo].[UBM_GET_FTP_ARCHIVE_LOCATIONS_P] TO [CBMSApplication]
GO
