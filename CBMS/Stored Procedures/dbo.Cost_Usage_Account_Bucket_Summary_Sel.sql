SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
Name:  
 CBMS.dbo.Cost_Usage_Account_Bucket_Summary_Sel
 
 Description:
	This procedure will be used to export the data to xls in page CBMS C/U Account Report
	Application will make sure to send either determinant/charge type of buckets in @Bucket_List.
 
 Input Parameters:  
    Name					DataType		    Default Description  
------------------------------------------------------------------------  
    @Bucket_List			Cost_Usage_Bucket	READONLY - Table Value Parameter
    @Report_Year			INT					NULL
    @Client_Id				INT					NULL
    @Division_Id			INT					NULL
    @Site_Client_Hier_Id	INT					NULL
    @RegionId				INT					NULL
    @Country_Id				INT					NULL
    @Site_Not_Managed		INT					NULL
 
 Output Parameters:  
 Name  Datatype  Default Description  
------------------------------------------------------------

 Usage Examples:
------------------------------------------------------------

DECLARE @Bucket_List1 AS Cost_Usage_Bucket
INSERT INTO @Bucket_List1
VALUES (290, 'Total Usage', 'Determinant', 12)
EXECUTE dbo.Cost_Usage_Account_Bucket_Summary_Sel @Bucket_List1, 2005, 10069, NULL, NULL, NULL, NULL, NULL, NULL

DECLARE @Bucket_List1 AS Cost_Usage_Bucket
INSERT INTO @Bucket_List1
VALUES (290, 'Total Usage', 'Determinant', 12)
    
EXECUTE dbo.Cost_Usage_Account_Bucket_Summary_Sel @Bucket_List1, 2011, 1043, NULL, 7191, NULL, NULL, NULL, NULL



DECLARE @Bucket_List1 AS Cost_Usage_Bucket
INSERT INTO @Bucket_List1
VALUES (290, 'Total Usage', 'Determinant', 12)
EXECUTE dbo.Cost_Usage_Account_Bucket_Summary_Sel @Bucket_List1, 2010, 235, 258, NULL, NULL, NULL, NULL, 250

DECLARE @Bucket_List1 AS Cost_Usage_Bucket
INSERT INTO @Bucket_List1
VALUES (290, 'Total Usage', 'Determinant', 12)
EXECUTE dbo.Cost_Usage_Account_Bucket_Summary_Sel @Bucket_List1, 2011, 11554, 12208691, NULL, NULL, NULL, NULL, 250

DECLARE @Bucket_List1 AS Cost_Usage_Bucket 
INSERT INTO @Bucket_List1 
VALUES (290, 'Taxes', 'Charge', 3) 
,(291, 'Taxes', 'Charge', 3) 

EXECUTE dbo.Cost_Usage_Account_Bucket_Summary_Sel @Bucket_List1, 2011, null, NULL, NULL, 5, 5, NULL, 250
 
Author Initials:  
 Initials	Name
------------------------------------------------------------
 HG			Harihara Suthan G
 NR			Narayana Reddy  

  
 Modifications :  
 Initials	  Date			Modification  
------------------------------------------------------------  
  HG		  2012-07-18	MAINT-1362, Created based on dbo.Cost_Usage_Account_Bucket_Value_Sel
							-- Values are pivoted as we have performance issue in application side if all the service months values are returned in a different rows.
   NR		  2020-07-02	SE2017-512 - Added @Rate_Id as input parameter. 

******/

CREATE PROCEDURE [dbo].[Cost_Usage_Account_Bucket_Summary_Sel]
    (
        @Bucket_List AS dbo.Cost_Usage_Bucket READONLY
        , @Report_Year INT
        , @Client_Id INT = NULL
        , @Division_Id INT = NULL
        , @Site_Client_Hier_Id INT = NULL
        , @Region_Id INT = NULL
        , @State_Id INT = NULL
        , @Vendor_Id INT = NULL
        , @Site_Type_Id INT = NULL
        , @Rate_Id INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        CREATE TABLE #Account_Dtl
             (
                 Client_Name VARCHAR(200)
                 , Sitegroup_Name VARCHAR(200)
                 , Site_Id INT
                 , Client_Hier_Id INT
                 , City VARCHAR(200)
                 , State_Name VARCHAR(20)
                 , Region_Name VARCHAR(200)
                 , Address_Line1 VARCHAR(200)
                 , Account_Id INT
                 , Account_Number VARCHAR(200)
                 , Vendor_Name VARCHAR(200)
                 , Client_Currency_Group_Id INT
                 , Row_Num INT
                 , Rate_Name VARCHAR(200)
             );

        CREATE TABLE #CU_Data
             (
                 Account_Id INT
                 , Client_Hier_Id INT
                 , Service_Month DATE
                 , Bucket_Value DECIMAL(28, 10)
                 , Has_Manual_Data BIT
                 , PRIMARY KEY
                   (
                       Account_Id
                       , Client_Hier_Id
                       , Service_Month
                   )
             );

        DECLARE @Bucket_Id TABLE
              (
                  Bucket_Master_Id INT PRIMARY KEY CLUSTERED
                  , Bucket_Name VARCHAR(200)
                  , Bucket_Type VARCHAR(25)
                  , Uom_Type_Id INT
                  , Currency_Unit_Id INT
                  , Commodity_Name VARCHAR(200)
              );

        DECLARE
            @Start_Month DATE
            , @End_Month DATE
            , @Usd_Currency_Unit_Id INT
            , @Site_Type_Name VARCHAR(200);

        SELECT
            @Usd_Currency_Unit_Id = CU.CURRENCY_UNIT_ID
        FROM
            dbo.CURRENCY_UNIT CU
        WHERE
            CU.SYMBOL = 'USD';


        INSERT INTO @Bucket_Id
             (
                 Bucket_Master_Id
                 , Bucket_Name
                 , Bucket_Type
                 , Uom_Type_Id
                 , Currency_Unit_Id
                 , Commodity_Name
             )
        SELECT
            bm.Bucket_Master_Id
            , bl.Bucket_Name
            , bl.Bucket_Type
            , CASE WHEN bl.Bucket_Type = 'Determinant' THEN ISNULL(bl.Bucket_Uom_Id, bm.Default_Uom_Type_Id)
              END
            , CASE WHEN bl.Bucket_Type = 'Charge' THEN ISNULL(bl.Bucket_Uom_Id, @Usd_Currency_Unit_Id)
              END
            , com.Commodity_Name
        FROM
            @Bucket_List bl
            JOIN dbo.Bucket_Master bm
                ON bm.Bucket_Name = bl.Bucket_Name
                   AND  bm.Commodity_Id = bl.Commodity_Id
            JOIN dbo.Code bt
                ON bt.Code_Id = bm.Bucket_Type_Cd
                   AND  bt.Code_Value = bl.Bucket_Type
            JOIN dbo.Commodity com
                ON bm.Commodity_Id = com.Commodity_Id;

        SELECT
            @Site_Type_Name = st.ENTITY_NAME
        FROM
            dbo.ENTITY st
        WHERE
            st.ENTITY_ID = @Site_Type_Id;

        SELECT
            @Start_Month = MIN(dd.DATE_D)
            , @End_Month = MAX(dd.DATE_D)
        FROM    (   SELECT
                        DATEADD(m, ch.Client_Fiscal_Offset, CAST('1/1/' + CAST(@Report_Year AS VARCHAR(4)) AS DATE)) Start_month
                    FROM
                        Core.Client_Hier ch
                    WHERE
                        ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id = 0
                        AND ch.Site_Id = 0
                    UNION ALL
                    SELECT
                        CAST('1/1/' + CAST(@Report_Year AS VARCHAR(4)) AS VARCHAR(10))
                    WHERE
                        @Client_Id IS NULL) X
                CROSS JOIN meta.DATE_DIM dd
        WHERE
            dd.DATE_D BETWEEN X.Start_month
                      AND     DATEADD(MONTH, -1, (DATEADD(YEAR, 1, X.Start_month)));

        INSERT INTO #Account_Dtl
             (
                 Client_Name
                 , Sitegroup_Name
                 , Site_Id
                 , Client_Hier_Id
                 , City
                 , State_Name
                 , Region_Name
                 , Address_Line1
                 , Account_Id
                 , Account_Number
                 , Vendor_Name
                 , Client_Currency_Group_Id
                 , Row_Num
                 , Rate_Name
             )
        SELECT
            CH.Client_Name
            , CH.Sitegroup_Name
            , CH.Site_Id
            , CH.Client_Hier_Id
            , CH.City
            , CH.State_Name
            , CH.Region_Name
            , CH.Site_Address_Line1
            , CHA.Account_Id
            , CHA.Display_Account_Number AS Account_Number
            , CHA.Account_Vendor_Name
            , CH.Client_Currency_Group_Id
            , ROW_NUMBER() OVER (ORDER BY
                                     CH.Client_Name
                                     , CH.Sitegroup_Name
                                     , CHA.Account_Id ASC)
            , CHA.Rate_Name
        FROM
            Core.Client_Hier CH
            INNER JOIN Core.Client_Hier_Account CHA
                ON CH.Client_Hier_Id = CHA.Client_Hier_Id
        WHERE
            (   @Client_Id IS NULL
                OR  CH.Client_Id = @Client_Id)
            AND (   @Division_Id IS NULL
                    OR  CH.Sitegroup_Id = @Division_Id)
            AND (   @Site_Client_Hier_Id IS NULL
                    OR  CH.Client_Hier_Id = @Site_Client_Hier_Id)
            AND (   @Site_Type_Id IS NULL
                    OR  CH.Site_Type_Name = @Site_Type_Name)
            AND (   @State_Id IS NULL
                    OR  CH.State_Id = @State_Id)
            AND (   @Region_Id IS NULL
                    OR  CH.Region_ID = @Region_Id)
            AND (   @Vendor_Id IS NULL
                    OR  CHA.Account_Vendor_Id = @Vendor_Id)
            AND CH.Client_Not_Managed = 0
            AND CH.Site_Not_Managed = 0
            AND CH.Site_Closed = 0
            AND CH.Division_Not_Managed = 0
            AND CHA.Account_Not_Managed = 0
            AND (   CHA.Supplier_Account_begin_Dt IS NULL
                    OR  CHA.Supplier_Account_begin_Dt <= @End_Month)
            AND (   CHA.Supplier_Account_End_Dt IS NULL
                    OR  CHA.Supplier_Account_End_Dt >= @Start_Month)
            AND (   @Rate_Id IS NULL
                    OR  CHA.Rate_Id = @Rate_Id)
        GROUP BY
            CH.Client_Name
            , CH.Sitegroup_Name
            , CH.Site_Id
            , CH.Client_Hier_Id
            , CH.City
            , CH.State_Name
            , CH.Region_Name
            , CH.Site_Address_Line1
            , CHA.Account_Id
            , CHA.Display_Account_Number
            , CHA.Account_Vendor_Name
            , CH.Client_Currency_Group_Id
            , CHA.Rate_Name;

        INSERT INTO #CU_Data
             (
                 Account_Id
                 , Client_Hier_Id
                 , Service_Month
                 , Bucket_Value
                 , Has_Manual_Data
             )
        SELECT
            CUAD.ACCOUNT_ID
            , CUAD.Client_Hier_ID
            , CUAD.Service_Month
            , SUM(CASE WHEN bl.Bucket_Type = 'Determinant'
                            AND bl.Bucket_Name IN ( 'Demand', 'Billed Demand', 'Contract Demand' )
                            AND uom.ENTITY_NAME IN ( 'kVa', 'kvar', 'kW' ) THEN CUAD.Bucket_Value
                      WHEN bl.Bucket_Type = 'Determinant'
                           AND  bl.Bucket_Name NOT IN ( 'Demand', 'Billed Demand', 'Contract Demand' ) THEN
                          CUAD.Bucket_Value * UOMConv.CONVERSION_FACTOR
                      WHEN bl.Bucket_Type = 'Charge' THEN CUAD.Bucket_Value * CurConv.CONVERSION_FACTOR
                  END) AS Bucket_Value
            , MAX(CASE WHEN dsc.Code_Value = 'CBMS'
                            OR  dsc.Code_Value = 'DE' THEN 1
                      ELSE 0
                  END) AS Has_Manual_Data
        FROM
            #Account_Dtl ad
            INNER JOIN dbo.Cost_Usage_Account_Dtl CUAD
                ON CUAD.ACCOUNT_ID = ad.Account_Id
                   AND  CUAD.Client_Hier_ID = ad.Client_Hier_Id
            INNER JOIN @Bucket_Id bl
                ON bl.Bucket_Master_Id = CUAD.Bucket_Master_Id
            INNER JOIN dbo.Code dsc
                ON dsc.Code_Id = CUAD.Data_Source_Cd
            LEFT OUTER JOIN dbo.CONSUMPTION_UNIT_CONVERSION UOMConv
                ON UOMConv.BASE_UNIT_ID = CUAD.UOM_Type_Id
                   AND  UOMConv.CONVERTED_UNIT_ID = bl.Uom_Type_Id
                   AND  bl.Bucket_Type = 'Determinant'
            LEFT OUTER JOIN dbo.CURRENCY_UNIT_CONVERSION CurConv
                ON CurConv.BASE_UNIT_ID = CUAD.CURRENCY_UNIT_ID
                   AND  CurConv.CURRENCY_GROUP_ID = ad.Client_Currency_Group_Id
                   AND  CurConv.CONVERSION_DATE = CUAD.Service_Month
                   AND  CurConv.CONVERTED_UNIT_ID = bl.Currency_Unit_Id
                   AND  bl.Bucket_Type = 'Charge'
            LEFT OUTER JOIN dbo.ENTITY uom
                ON uom.ENTITY_ID = CUAD.UOM_Type_Id
                   AND  bl.Bucket_Type = 'Determinant'
        WHERE
            CUAD.Service_Month BETWEEN @Start_Month
                               AND     @End_Month
        GROUP BY
            CUAD.ACCOUNT_ID
            , CUAD.Client_Hier_ID
            , CUAD.Service_Month;

        WITH Cte_Pivoted_CU_Data
        AS (
               SELECT
                    cuad.Account_Id
                    , cuad.Client_Hier_Id
                    , MAX(CASE WHEN dd.Month_Num = 1
                                    AND cuad.Service_Month = dd.Service_Month THEN cuad.Bucket_Value
                          END) AS Month1_Bucket_Value
                    , MAX(CASE WHEN dd.Month_Num = 2
                                    AND cuad.Service_Month = dd.Service_Month THEN cuad.Bucket_Value
                          END) AS Month2_Bucket_Value
                    , MAX(CASE WHEN dd.Month_Num = 3
                                    AND cuad.Service_Month = dd.Service_Month THEN cuad.Bucket_Value
                          END) AS Month3_Bucket_Value
                    , MAX(CASE WHEN dd.Month_Num = 4
                                    AND cuad.Service_Month = dd.Service_Month THEN cuad.Bucket_Value
                          END) AS Month4_Bucket_Value
                    , MAX(CASE WHEN dd.Month_Num = 5
                                    AND cuad.Service_Month = dd.Service_Month THEN cuad.Bucket_Value
                          END) AS Month5_Bucket_Value
                    , MAX(CASE WHEN dd.Month_Num = 6
                                    AND cuad.Service_Month = dd.Service_Month THEN cuad.Bucket_Value
                          END) AS Month6_Bucket_Value
                    , MAX(CASE WHEN dd.Month_Num = 7
                                    AND cuad.Service_Month = dd.Service_Month THEN cuad.Bucket_Value
                          END) AS Month7_Bucket_Value
                    , MAX(CASE WHEN dd.Month_Num = 8
                                    AND cuad.Service_Month = dd.Service_Month THEN cuad.Bucket_Value
                          END) AS Month8_Bucket_Value
                    , MAX(CASE WHEN dd.Month_Num = 9
                                    AND cuad.Service_Month = dd.Service_Month THEN cuad.Bucket_Value
                          END) AS Month9_Bucket_Value
                    , MAX(CASE WHEN dd.Month_Num = 10
                                    AND cuad.Service_Month = dd.Service_Month THEN cuad.Bucket_Value
                          END) AS Month10_Bucket_Value
                    , MAX(CASE WHEN dd.Month_Num = 11
                                    AND cuad.Service_Month = dd.Service_Month THEN cuad.Bucket_Value
                          END) AS Month11_Bucket_Value
                    , MAX(CASE WHEN dd.Month_Num = 12
                                    AND cuad.Service_Month = dd.Service_Month THEN cuad.Bucket_Value
                          END) AS Month12_Bucket_Value
                    , MAX(CASE WHEN dd.Month_Num = 1
                                    AND cuad.Service_Month = dd.Service_Month THEN
                                   CAST(cuad.Has_Manual_Data AS TINYINT)
                          END) AS Month1_Has_Manual_Data
                    , MAX(CASE WHEN dd.Month_Num = 2
                                    AND cuad.Service_Month = dd.Service_Month THEN
                                   CAST(cuad.Has_Manual_Data AS TINYINT)
                          END) AS Month2_Has_Manual_Data
                    , MAX(CASE WHEN dd.Month_Num = 3
                                    AND cuad.Service_Month = dd.Service_Month THEN
                                   CAST(cuad.Has_Manual_Data AS TINYINT)
                          END) AS Month3_Has_Manual_Data
                    , MAX(CASE WHEN dd.Month_Num = 4
                                    AND cuad.Service_Month = dd.Service_Month THEN
                                   CAST(cuad.Has_Manual_Data AS TINYINT)
                          END) AS Month4_Has_Manual_Data
                    , MAX(CASE WHEN dd.Month_Num = 5
                                    AND cuad.Service_Month = dd.Service_Month THEN
                                   CAST(cuad.Has_Manual_Data AS TINYINT)
                          END) AS Month5_Has_Manual_Data
                    , MAX(CASE WHEN dd.Month_Num = 6
                                    AND cuad.Service_Month = dd.Service_Month THEN
                                   CAST(cuad.Has_Manual_Data AS TINYINT)
                          END) AS Month6_Has_Manual_Data
                    , MAX(CASE WHEN dd.Month_Num = 7
                                    AND cuad.Service_Month = dd.Service_Month THEN
                                   CAST(cuad.Has_Manual_Data AS TINYINT)
                          END) AS Month7_Has_Manual_Data
                    , MAX(CASE WHEN dd.Month_Num = 8
                                    AND cuad.Service_Month = dd.Service_Month THEN
                                   CAST(cuad.Has_Manual_Data AS TINYINT)
                          END) AS Month8_Has_Manual_Data
                    , MAX(CASE WHEN dd.Month_Num = 9
                                    AND cuad.Service_Month = dd.Service_Month THEN
                                   CAST(cuad.Has_Manual_Data AS TINYINT)
                          END) AS Month9_Has_Manual_Data
                    , MAX(CASE WHEN dd.Month_Num = 10
                                    AND cuad.Service_Month = dd.Service_Month THEN
                                   CAST(cuad.Has_Manual_Data AS TINYINT)
                          END) AS Month10_Has_Manual_Data
                    , MAX(CASE WHEN dd.Month_Num = 11
                                    AND cuad.Service_Month = dd.Service_Month THEN
                                   CAST(cuad.Has_Manual_Data AS TINYINT)
                          END) AS Month11_Has_Manual_Data
                    , MAX(CASE WHEN dd.Month_Num = 12
                                    AND cuad.Service_Month = dd.Service_Month THEN
                                   CAST(cuad.Has_Manual_Data AS TINYINT)
                          END) AS Month12_Has_Manual_Data
               FROM
                    #CU_Data cuad
                    CROSS JOIN (   SELECT
                                        dd.DATE_D AS Service_Month
                                        , ROW_NUMBER() OVER (ORDER BY
                                                                 dd.DATE_D) Month_Num
                                   FROM
                                        meta.DATE_DIM dd
                                   WHERE
                                        dd.DATE_D BETWEEN @Start_Month
                                                  AND     @End_Month) dd
               GROUP BY
                   cuad.Account_Id
                   , cuad.Client_Hier_Id
           )
        SELECT --TOP 100
            AD.Client_Name
            , AD.Sitegroup_Name AS Division_Name
            , AD.City
            , AD.State_Name
            , AD.Region_Name
            , AD.City AS Site_Name
            , AD.Address_Line1
            , AD.Account_Id
            , AD.Account_Number
            , AD.Vendor_Name
            , AD.Rate_Name
            , cuad.Month1_Bucket_Value
            , cuad.Month2_Bucket_Value
            , cuad.Month3_Bucket_Value
            , cuad.Month4_Bucket_Value
            , cuad.Month5_Bucket_Value
            , cuad.Month6_Bucket_Value
            , cuad.Month7_Bucket_Value
            , cuad.Month8_Bucket_Value
            , cuad.Month9_Bucket_Value
            , cuad.Month10_Bucket_Value
            , cuad.Month11_Bucket_Value
            , cuad.Month12_Bucket_Value
            , cuad.Month1_Has_Manual_Data
            , cuad.Month2_Has_Manual_Data
            , cuad.Month3_Has_Manual_Data
            , cuad.Month4_Has_Manual_Data
            , cuad.Month5_Has_Manual_Data
            , cuad.Month6_Has_Manual_Data
            , cuad.Month7_Has_Manual_Data
            , cuad.Month8_Has_Manual_Data
            , cuad.Month9_Has_Manual_Data
            , cuad.Month10_Has_Manual_Data
            , cuad.Month11_Has_Manual_Data
            , cuad.Month12_Has_Manual_Data
            , @Start_Month AS Fiscal_Start
        FROM
            #Account_Dtl AD
            LEFT OUTER JOIN Cte_Pivoted_CU_Data cuad
                ON cuad.Client_Hier_Id = AD.Client_Hier_Id
                   AND  cuad.Account_Id = AD.Account_Id
        ORDER BY
            AD.Row_Num;

        DROP TABLE #Account_Dtl;

    END;


GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Bucket_Summary_Sel] TO [CBMSApplication]
GO
