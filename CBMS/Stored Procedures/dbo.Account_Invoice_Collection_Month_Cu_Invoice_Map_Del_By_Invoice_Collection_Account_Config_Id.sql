SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                
 NAME: dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map_Del_By_Invoice_Collection_Account_Config_Id                    
                                
 DESCRIPTION:                                
   To delete the unmapped invoice to Invoice_Collection_Account_Config_Id               
                                
 INPUT PARAMETERS:                  
                             
 Name                        DataType         Default       Description                
---------------------------------------------------------------------------------------------------------------              
@Invoice_Collection_Account_Config_Id INT  NULL         
                                      
 OUTPUT PARAMETERS:                  
                                   
 Name                        DataType         Default       Description                
---------------------------------------------------------------------------------------------------------------              
                                
 USAGE EXAMPLES:                                    
---------------------------------------------------------------------------------------------------------------                                    
            
        
  EXEC dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map_Del_By_Invoice_Collection_Account_Config_Id 563
        
                               
 AUTHOR INITIALS:                
               
 Initials              Name                
---------------------------------------------------------------------------------------------------------------                              
 RKV                  Ravi Kumar Vegesna        
                                 
 MODIFICATIONS:              
                  
 Initials              Date             Modification              
---------------------------------------------------------------------------------------------------------------              
    RKV    2017-01-25  Created as part of MAINT-5761.                 
                               
******/                         
                        
CREATE PROCEDURE [dbo].[Account_Invoice_Collection_Month_Cu_Invoice_Map_Del_By_Invoice_Collection_Account_Config_Id]
      ( 
       @Invoice_Collection_Account_Config_Id INT )
AS 
BEGIN                        
      SET NOCOUNT ON;            
             
      DECLARE @Account_Invoice_Collection_Month_Id TABLE
            ( 
             Account_Invoice_Collection_Month_Id INT )
             
      INSERT      INTO @Account_Invoice_Collection_Month_Id
                  ( 
                   Account_Invoice_Collection_Month_Id )
                  SELECT
                        aicm.Account_Invoice_Collection_Month_Id
                  FROM
                        dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map aicmcim
                        INNER JOIN dbo.Account_Invoice_Collection_Month aicm
                              ON aicmcim.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id
                  WHERE
                        aicm.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.CU_INVOICE_SERVICE_MONTH cism
                                         WHERE
                                          aicmcim.Cu_Invoice_Id = cism.CU_INVOICE_ID
                                          AND SERVICE_MONTH IS NOT NULL )
                        
     
                  
      DELETE
            aicmcim
      FROM
            dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map aicmcim
            INNER JOIN @Account_Invoice_Collection_Month_Id aicmi
                  ON aicmcim.Account_Invoice_Collection_Month_Id = aicmi.Account_Invoice_Collection_Month_Id
                  
                    
            
            
     
     

END;    
;


;
GO
GRANT EXECUTE ON  [dbo].[Account_Invoice_Collection_Month_Cu_Invoice_Map_Del_By_Invoice_Collection_Account_Config_Id] TO [CBMSApplication]
GO
