SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_UPLOAD_COMMUNICATIONS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	int       	          	
	@sessionId     	int       	          	
	@cbmsImageId   	int       	          	
	@rfpId         	int       	          	
	@comments      	varchar(200)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE [dbo].[SR_RFP_UPLOAD_COMMUNICATIONS_P]
	@userId INT,
	@sessionId INT,
	--@cbmsImageDocId varchar(200), 
	--@cbmsImage image ,
	--@contentType varchar(200),
	@cbmsImageId INT,  -- added by Jaya
	@rfpId INT,
	@comments VARCHAR(200)
AS
BEGIN
	

	DECLARE @entityId INT
	SELECT @entityId = (SELECT ENTITY_ID FROM dbo.ENTITY WITH (NOLOCK) WHERE entity_name = 'Communications' AND entity_type = 100 )

	/* 
	 INSERT INTO CBMS_IMAGE (CBMS_IMAGE_TYPE_ID, CBMS_DOC_ID, CBMS_IMAGE, CONTENT_TYPE, DATE_IMAGED) 
	 VALUES (@entityId, @cbmsImageDocId, @cbmsImage, @contentType, getDate())

	declare @cbmsImageId int
	select @cbmsImageId = (select @@Identity)  */  

	--added by Jaya for updating entityid    
	     
	UPDATE dbo.CBMS_IMAGE SET CBMS_IMAGE_TYPE_ID = @entityId WHERE CBMS_IMAGE_ID = @cbmsImageId
	
	INSERT INTO dbo.SR_RFP_COMMUNICATIONS (UPLOADED_BY, SR_RFP_ID, CBMS_IMAGE_ID, COMMENTS, UPLOADED_DATE) 
	VALUES ( @userId, @rfpId, @cbmsImageId, @comments, GETDATE())

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_UPLOAD_COMMUNICATIONS_P] TO [CBMSApplication]
GO
