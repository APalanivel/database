
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******

NAME: [dbo].[Get_Account_By_Site_Client_Hier_Id_Account_Type]

DESCRIPTION: 
	To get the Account details of given site's client hier id and Account type

INPUT PARAMETERS:
NAME					DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
@Site_Client_Hier_Id	INT
@Account_Type			VARCHAR(25)	NULL		Supplier/Utility/NULL(All)

OUTPUT PARAMETERS:
NAME					DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Get_Account_By_Site_Client_Hier_Id_Account_Type 1130
	EXEC dbo.Get_Account_By_Site_Client_Hier_Id_Account_Type 1130,'Supplier'
	EXEC dbo.Get_Account_By_Site_Client_Hier_Id_Account_Type 1130,'Utility',0
	
	SELECT TOP 10 * FROM Core.Client_Hier_Account WHERE Account_Type = 'Supplier'

AUTHOR INITIALS:
INITIALS		NAME
------------------------------------------------------------
HG				Harihara Suthan G
RKV				Ravi Kumar Vegesna

MODIFICATIONS
INITIALS	DATE				MODIFICATION
------------------------------------------------------------
HG			2012-03-27			Created for additional data enhancement
RKV			2016-03-04			Added Account_Not_Managed parameter as part of AS400-CIP
*/

CREATE PROCEDURE [dbo].[Get_Account_By_Site_Client_Hier_Id_Account_Type]
      ( 
       @Site_Client_Hier_Id INT
      ,@Account_Type VARCHAR(25) = NULL
      ,@Account_Not_Managed BIT = NULL
      ,@Commodity_Id VARCHAR(MAX) = NULL )
AS 
BEGIN

      SET NOCOUNT ON;

      SELECT
            cha.Account_Id
           ,cha.Display_Account_Number AS Account_Number
           ,cha.Account_Type
      FROM
            core.Client_Hier_Account cha
      WHERE
            cha.Client_Hier_Id = @Site_Client_Hier_Id
            AND ( @Account_Type IS NULL
                  OR cha.Account_Type = @Account_Type )
            AND ( @Account_Not_Managed IS NULL
                  OR Account_Not_Managed = @Account_Not_Managed )
            AND ( @Commodity_Id IS NULL
                  OR EXISTS ( SELECT
                                    1
                              FROM
                                    dbo.ufn_split(@Commodity_Id, ',') us
                              WHERE
                                    us.Segments = cha.Commodity_Id ) )
      GROUP BY
            cha.Account_Id
           ,cha.Display_Account_Number
           ,cha.Account_Type
      ORDER BY
            cha.Display_Account_Number

END;



;
GO

GRANT EXECUTE ON  [dbo].[Get_Account_By_Site_Client_Hier_Id_Account_Type] TO [CBMSApplication]
GO
