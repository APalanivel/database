SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME: [Workflow].[Workflow_Queue_Search_StateByCountry]      
        
DESCRIPTION:        
  Script to get default module of User     
        
INPUT PARAMETERS:        
 Name    DataType  Default Description        
---------------------------------------------------------------        
 @country_id SQL_VARIANT =NULL,    
  @Key_Word varchar(200) = NULL,    
  @Start_Index INT = 1,    
  @End_Index INT = 2147483647   

OUTPUT PARAMETERS:        
 Name     DataType  Default Description        
------------------------------------------------------------        
         
USAGE EXAMPLES:        
------------------------------------------------------------        
EXEC Workflow.Workflow_Queue_Search_StateByCountry @country_id = NULL, -- sql_variant
                                                   @Key_Word = '',     -- varchar(200)
                                                   @Start_Index = 0,   -- int
                                                   @End_Index = 0      -- int    
AUTHOR INITIALS:        
 Initials Name        
------------------------------------------------------------        
 BR        Bhaskar Rejintala    
      
         
MODIFICATIONS        
 Initials Date   Modification        
------------------------------------------------------------        
 BR      2019-05-27 Created      
 TRK     AUG-2019 Added @Start_Index,@End_Index For Paging        
******/      	
		
CREATE PROC [Workflow].[Workflow_Queue_Search_StateByCountry]    
   (      
  @country_id SQL_VARIANT =NULL,      
  @Key_Word varchar(200) = NULL,      
  @Start_Index INT = 1,      
  @End_Index INT = 2147483647      
   )       
AS        
BEGIN        
       
DECLARE @sql NVARCHAR(MAX)      
      
IF (SQL_VARIANT_PROPERTY(@country_id,'BaseType') IS NULL)  OR  ( SQL_VARIANT_PROPERTY(@country_id,'BaseType') = 'int') OR ( DATALENGTH(@country_id) = 0)      
BEGIN      
     
      
 ;WITH CTE_State      
 AS       
 (      
 SELECT       
  st.state_id  ,      
  st.state_name ,      
  ROW_NUMBER() OVER (ORDER BY st.state_name ) AS Row_Num      
 FROM       
 STATE st        
 JOIN country c       
  ON c.country_id = st.country_id        
 WHERE       
  ( @country_id  IS NULL OR (c.country_id = @country_id) OR (DATALENGTH(@country_id) = 0) )  AND st.state_name  Like '%' + isNull(@Key_Word, st.state_name ) + '%'      
  )       
  SELECT CS.state_id,CS.state_name FROM CTE_State CS WHERE Row_Num BETWEEN @Start_Index  AND     @End_Index      
  ORDER BY CS.state_name       
END      
ELSE      
BEGIN      
      
SELECT @sql =N'SELECT       
 st.state_id,       
 st.state_name        
FROM       
 STATE st        
    JOIN       
 COUNTRY c       
  ON c.country_id = st.country_id        
 WHERE       
  c.country_id in ('+convert(nvarchar(max),@country_id)+')      
 ORDER BY       
  st.state_name '      
  EXEC (@sql)      
end      
      
END;        
     
 
GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_Search_StateByCountry] TO [CBMSApplication]
GO
