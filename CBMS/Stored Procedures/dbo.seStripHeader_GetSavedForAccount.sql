SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure dbo.seStripHeader_GetSavedForAccount
	(
		@AccountId int
	)
AS
BEGIN

	set nocount on
	
	declare @ExpDate datetime
	set @ExpDate = getdate()
	
	 select StripHeaderId
				, AccountId
				, StripName
				, ExpirationDate
				, UniformVolume
		 from seStripHeader
		where AccountId = @AccountId
		  and StripName is not null
		  and ExpirationDate >= @ExpDate
 order by StripName asc
		  
END
GO
GRANT EXECUTE ON  [dbo].[seStripHeader_GetSavedForAccount] TO [CBMSApplication]
GO
