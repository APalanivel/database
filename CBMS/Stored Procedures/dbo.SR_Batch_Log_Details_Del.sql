SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[SR_Batch_Log_Details_Del]  
     
DESCRIPTION: 
	It Deletes SR Batch Log Details for Selected SR Batch Log Detail Id.
      
INPUT PARAMETERS:          
NAME						DATATYPE	DEFAULT		DESCRIPTION          
---------------------------------------------------------------          
@SR_Batch_Log_Details_Id	INT	

   
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        

	BEGIN TRAN
	
		EXEC SR_Batch_Log_Details_Del 27112
	ROLLBACK TRAN
	

AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR			PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR			08-JULY-10	CREATED     

*/
CREATE PROCEDURE dbo.SR_Batch_Log_Details_Del
   (
    @SR_Batch_Log_Details_Id INT
   )
AS 
BEGIN

    SET NOCOUNT ON;

    DELETE
   	FROM
		dbo.SR_BATCH_LOG_DETAILS
	WHERE
		SR_BATCH_LOG_DETAILS_ID = @SR_Batch_Log_Details_Id

END
GO
GRANT EXECUTE ON  [dbo].[SR_Batch_Log_Details_Del] TO [CBMSApplication]
GO
