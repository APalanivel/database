SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                                        
                              
NAME: [dbo].[Get_All_IP_Account_Data]                              
                              
DESCRIPTION:                               
      To select all the IP Account level data to transfer it to Hub
      This sproc will be used by intitial load package

INPUT PARAMETERS:
NAME			DATATYPE	DEFAULT  DESCRIPTION
------------------------------------------------------------

OUTPUT PARAMETERS:                                        
NAME			DATATYPE	DEFAULT  DESCRIPTION                                 
------------------------------------------------------------                                        

USAGE EXAMPLES:                                        
------------------------------------------------------------                              


EXEC dbo.Get_All_IP_Account_Data
                  
AUTHOR INITIALS:                                        
INITIALS	NAME                  
------------------------------------------------------------                                        
RR			Raghu Reddy

MODIFICATIONS                              
INITIALS	DATE		MODIFICATION                              
------------------------------------------------------------                              
RR			2014-03-26  Created

*/
CREATE PROCEDURE dbo.Get_All_IP_Account_Data
AS 
BEGIN

      SET NOCOUNT ON

      SELECT
            ip.Client_Hier_Id
           ,ip.Account_Id
           ,ip.Service_Month
           ,cast(isnull(ip.IS_EXPECTED, 0) AS INT) AS Is_Expected
           ,cast(isnull(ip.IS_RECEIVED, 0) AS INT) AS Is_Received
           ,cha.Commodity_Id
           ,'I' AS Sys_Change_Operation
      FROM
            dbo.INVOICE_PARTICIPATION ip
            INNER JOIN core.Client_Hier_Account cha
                  ON ip.Client_Hier_Id = cha.Client_Hier_Id
                     AND ip.ACCOUNT_ID = cha.Account_Id
            INNER JOIN core.Client_Hier ch
                  ON cha.Client_Hier_Id = ch.Client_Hier_Id
      GROUP BY
            ip.Client_Hier_Id
           ,ip.Account_Id
           ,ip.Service_Month
           ,ip.IS_EXPECTED
           ,ip.IS_RECEIVED
           ,cha.Commodity_Id

END;
;
GO
GRANT EXECUTE ON  [dbo].[Get_All_IP_Account_Data] TO [ETL_Execute]
GO
