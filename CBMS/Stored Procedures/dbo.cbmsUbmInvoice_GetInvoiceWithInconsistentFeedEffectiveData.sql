SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE   PROCEDURE [dbo].[cbmsUbmInvoice_GetInvoiceWithInconsistentFeedEffectiveData]
	( @ubm_batch_master_log_id int )
AS
BEGIN

	   select ui.ubm_invoice_id
		, d.details_count
		, m.meter_details_count
		, f.feed_count
	     from ubm_invoice ui
  left outer join (
		   select l.ubm_id
			, l.ubm_batch_master_log_id
			, ui.ubm_invoice_id
			, count(*) details_count
		     from ubm_batch_master_log l with (nolock)
		     join ubm_invoice ui with (nolock) on ui.ubm_batch_master_log_id = l.ubm_batch_master_log_id
		     join ubm_invoice_details d with (nolock) on d.ubm_invoice_id = ui.ubm_invoice_id
		    where l.ubm_batch_master_log_id = @ubm_batch_master_log_id
		 group by l.ubm_id
			, l.ubm_batch_master_log_id
			, ui.ubm_invoice_id
		  ) d on d.ubm_invoice_id = ui.ubm_invoice_id

  left outer join (
		   select l.ubm_id
			, l.ubm_batch_master_log_id
			, ui.ubm_invoice_id
			, count(*) feed_count
		     from ubm_batch_master_log l with (nolock)
		     join ubm_invoice ui with (nolock) on ui.ubm_batch_master_log_id = l.ubm_batch_master_log_id
		     join ubm_avista_feed f with (nolock) on f.bill_id = ui.invoice_identifier and f.ubm_batch_master_log_id = ui.ubm_batch_master_log_id
		    where l.ubm_batch_master_log_id = @ubm_batch_master_log_id
		 group by l.ubm_id
			, l.ubm_batch_master_log_id
			, ui.ubm_invoice_id
		  ) f on f.ubm_invoice_id = ui.ubm_invoice_id


  left outer join (
		   select l.ubm_id
			, l.ubm_batch_master_log_id
			, ui.ubm_invoice_id
			, count(*) meter_details_count
		     from ubm_batch_master_log l with (nolock)
		     join ubm_invoice ui with (nolock) on ui.ubm_batch_master_log_id = l.ubm_batch_master_log_id
		     join ubm_invoice_meter_details d with (nolock) on d.ubm_invoice_id = ui.ubm_invoice_id
		    where l.ubm_batch_master_log_id = @ubm_batch_master_log_id
		 group by l.ubm_id
			, l.ubm_batch_master_log_id
			, ui.ubm_invoice_id
		  ) m on m.ubm_invoice_id = ui.ubm_invoice_id


	    where ui.ubm_batch_master_log_id = @ubm_batch_master_log_id
	      and (d.details_count != f.feed_count or m.meter_details_count != f.feed_count)


END
GO
GRANT EXECUTE ON  [dbo].[cbmsUbmInvoice_GetInvoiceWithInconsistentFeedEffectiveData] TO [CBMSApplication]
GO
