SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********     
NAME:   dbo.Site_Utility_Acc_Dtls_Sel_By_Client_Commodity      
      
DESCRIPTION:      
			To get site and utility acconuts of given client and commodity 
      
INPUT PARAMETERS:            
	Name				DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Client_Id			INT
    @Commodity_Id		INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
            
	EXEC dbo.Site_Utility_Acc_Dtls_Sel_By_Client_Commodity 235,1719

EXEC dbo.Site_Utility_Acc_Dtls_Sel_By_Client_Commodity
    @Client_Id = 235
    , @City = 'Norcro'
		
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-02-07	Contract placeholder - CP-8 Created
*/


CREATE PROCEDURE [dbo].[Site_Utility_Acc_Dtls_Sel_By_Client_Commodity]
    (
        @Client_Id INT
        , @Site_Id INT = NULL
        , @Commodity_Id INT = NULL
        , @State_Id INT = NULL
        , @City VARCHAR(200) = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            ch.Site_Id
            , ch.Site_name
            , ch.City
            , ch.State_Id
            , ch.State_Name
            , cha.Account_Vendor_Id
            , cha.Account_Vendor_Name
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account cha
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
        WHERE
            ch.Client_Id = @Client_Id
            AND cha.Account_Type = 'Utility'
            AND (   @Commodity_Id IS NULL
                    OR  cha.Commodity_Id = @Commodity_Id)
            AND (   @Site_Id IS NULL
                    OR  ch.Site_Id = @Site_Id)
            AND (   @State_Id IS NULL
                    OR  ch.State_Id = @State_Id)
            AND (   @City IS NULL
                    OR  ch.City LIKE '%' + @City + '%')
        GROUP BY
            ch.Site_Id
            , ch.Site_name
            , ch.City
            , ch.State_Id
            , ch.State_Name
            , cha.Account_Vendor_Id
            , cha.Account_Vendor_Name;

    END;

    ;



GO
GRANT EXECUTE ON  [dbo].[Site_Utility_Acc_Dtls_Sel_By_Client_Commodity] TO [CBMSApplication]
GO
