SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:            
 Get_Meter_By_Account          
             
DESCRIPTION:            
Get Meters for an account, when an account no is passed as a parameter          
            
INPUT PARAMETERS:            
 Name   DataType  Default Description            
------------------------------------------------------------            
 @Account_ID INT          
                                                 
            
OUTPUT PARAMETERS:            
 Name   DataType  Default Description            
------------------------------------------------------------            
            
USAGE EXAMPLES:            
------------------------------------------------------------            
 SET STATISTICS IO ON            
 EXEC dbo.Get_Meter_By_Account 1278377,532845          
 EXEC dbo.Get_Meter_By_Account 12017 -- Mulitple meters      
         
           
AUTHOR INITIALS:            
 Initials Name            
------------------------------------------------------------            
 PRG   Prasanna Raghavendra G          
 SP    Srinivas Patchava          
 SLP   SriLakshimi Pallikonda           
MODIFICATIONS:            
 Initials Date   Modification            
------------------------------------------------------------            
PRG   08-Apr-2019  Created For Budget 2.0           
RKV         2019-06-21      Added new parameter Site_Id           
SP          2019-10-15      Added @Commodity_Id parameter for Maint-9396           
SLP   2020-04-16  Added Is_Primary column in the display        
					of the Calculator changes         
******/
CREATE PROC [dbo].[Get_Meter_By_Account]
    (
        @Account_ID INT
        , @Site_Id INT = NULL
        , @Commodity_Id INT = NULL
    )
AS
    BEGIN

        SELECT
            cha.Meter_Id
            , cha.Meter_Number
            , CASE WHEN (acpm.Meter_Id IS NOT NULL) THEN 1
                  ELSE 0
              END AS Is_Primary
        FROM
            Core.Client_Hier AS ch
            INNER JOIN Core.Client_Hier_Account AS cha
                ON cha.Client_Hier_Id = ch.Client_Hier_Id
            LEFT JOIN Core.Client_Hier_Account AS sup
                ON sup.Meter_Id = cha.Meter_Id
            LEFT JOIN Budget.Account_Commodity_Primary_Meter acpm
                ON acpm.Commodity_Id = cha.Commodity_Id
                   AND  acpm.Meter_Id = cha.Meter_Id
        WHERE
            cha.Account_Id = @Account_ID
            AND (   @Site_Id IS NULL
                    OR  ch.Site_Id = @Site_Id)
            AND (   @Commodity_Id IS NULL
                    OR  cha.Commodity_Id = @Commodity_Id)
        GROUP BY
            cha.Meter_Id
            , cha.Meter_Number
            , CASE WHEN (acpm.Meter_Id IS NOT NULL) THEN 1
                  ELSE 0
              END
        ORDER BY
            Is_Primary DESC;
    END;
GO
GRANT EXECUTE ON  [dbo].[Get_Meter_By_Account] TO [CBMSApplication]
GO
