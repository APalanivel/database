SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.Account_Consolidated_Billing_Vendor_Upd
           
DESCRIPTION:             
			To update consolidated billing configurations
			
INPUT PARAMETERS:            
	Name									DataType	Default		Description  
-----------------------------------------------------------------------------------  
	@Account_Consolidated_Billing_Vendor_Id INT
    @Billing_Start_Dt						DATE
    @Billing_End_Dt							DATE
    @Invoice_Vendor_Type_Id					INT
    @Supplier_Vendor_Id						INT			NULL
    @User_Info_Id							INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT TOP 10 * FROM dbo.Account_Consolidated_Billing_Vendor
            
	BEGIN TRANSACTION
		Declare @Account_Consolidated_Billing_Vendor_Id INT
		SELECT * FROM  dbo.Account_Consolidated_Billing_Vendor WHERE Account_Id = 1
		EXEC dbo.Account_Consolidated_Billing_Vendor_Ins 1,'2018-1-1','2018-12-31',1,1,16
		SELECT @Account_Consolidated_Billing_Vendor_Id = Account_Consolidated_Billing_Vendor_Id FROM  dbo.Account_Consolidated_Billing_Vendor 
			WHERE Account_Id = 1 AND Billing_Start_Dt = '2018-1-1' AND Billing_End_Dt='2018-12-31'
		SELECT * FROM  dbo.Account_Consolidated_Billing_Vendor 
			WHERE Account_Consolidated_Billing_Vendor_Id = @Account_Consolidated_Billing_Vendor_Id
		EXEC dbo.Account_Consolidated_Billing_Vendor_Upd @Account_Consolidated_Billing_Vendor_Id,'2018-1-1','2019-12-31',1,1,16
		SELECT * FROM  dbo.Account_Consolidated_Billing_Vendor 
			WHERE Account_Consolidated_Billing_Vendor_Id = @Account_Consolidated_Billing_Vendor_Id
	ROLLBACK TRANSACTION
	
	
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-01-19	Contract placeholder - CP-50 Created
******/

CREATE PROCEDURE [dbo].[Account_Consolidated_Billing_Vendor_Upd]
      ( 
       @Account_Consolidated_Billing_Vendor_Id INT
      ,@Billing_Start_Dt DATE
      ,@Billing_End_Dt DATE
      ,@Invoice_Vendor_Type_Id INT
      ,@Supplier_Vendor_Id INT = NULL
      ,@User_Info_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      UPDATE
            dbo.Account_Consolidated_Billing_Vendor
      SET   
            Billing_Start_Dt = @Billing_Start_Dt
           ,Billing_End_Dt = @Billing_End_Dt
           ,Invoice_Vendor_Type_Id = @Invoice_Vendor_Type_Id
           ,Supplier_Vendor_Id = @Supplier_Vendor_Id
           ,Updated_User_Id = @User_Info_Id
           ,Last_Change_Ts = GETDATE()
      WHERE
            Account_Consolidated_Billing_Vendor_Id = @Account_Consolidated_Billing_Vendor_Id
                      
      
END;
;
GO
GRANT EXECUTE ON  [dbo].[Account_Consolidated_Billing_Vendor_Upd] TO [CBMSApplication]
GO
