SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.DELETE_CHARGE_WITH_CHARGE_MASTERID_P
	@charge_parent_id INT
AS
BEGIN

	SET NOCOUNT ON

--	DELETE FROM dbo.charge
--	WHERE charge_master_id IN( 
--		SELECT charge_master_id FROM dbo.charge_master WHERE charge_parent_type_id = 93 AND charge_parent_id = @charge_parent_id )

	DELETE dbo.charge
	FROM dbo.Charge_Master cm 
	WHERE charge.Charge_Master_ID=cm.Charge_Master_ID
		AND cm.charge_parent_type_id = 93 
		AND cm.charge_parent_id = @charge_parent_id

END
GO
GRANT EXECUTE ON  [dbo].[DELETE_CHARGE_WITH_CHARGE_MASTERID_P] TO [CBMSApplication]
GO
