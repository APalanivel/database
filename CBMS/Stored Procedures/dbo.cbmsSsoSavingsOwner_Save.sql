SET NUMERIC_ROUNDABORT OFF 
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******
 NAME:  
	dbo.cbmsSsoSavingsOwner_Save
 
 DESCRIPTION:   
 INPUT PARAMETERS:  
 Name			DataType  Default Description  
------------------------------------------------------------  
 @Savings_id	int
 @owner_id		int
 @Hier_Level_Cd int
 
 OUTPUT PARAMETERS:  

 Name   DataType  Default Description  
------------------------------------------------------------  
 USAGE EXAMPLES:
------------------------------------------------------------  
  
  EXEC dbo.cbmsSsoSavingsOwner_Save
  
 AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 CPE		Chaitanya Panduga Eshwar
 
 MODIFICATIONS:
 Initials Date			Modification  
------------------------------------------------------------  
 CPE	  03/22/2011	Modified the SP to capture the Client_Hier_Id from the OwnerId and Owner_Type_Id.
            
******/

CREATE PROCEDURE dbo.cbmsSsoSavingsOwner_Save
( 
 @Savings_id int
,@owner_id int
,@Hier_Level_Cd int )
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE
            @Client_Hier_Id int

      SELECT
            @Client_Hier_Id = Client_Hier_Id
      FROM
            Core.Client_Hier CH
            JOIN dbo.Code C
                  ON CH.Hier_level_Cd = C.Code_Id
      WHERE
            CH.Hier_level_Cd = @Hier_Level_Cd
            AND ( ( CH.Client_Id = @owner_id
                    AND C.Code_Value = 'Corporate' )
                  OR ( CH.Sitegroup_Id = @owner_id
                       AND C.Code_Value = 'Division' )
                  OR ( CH.Site_Id = @owner_id
                       AND C.Code_Value = 'Site' ) )

      INSERT INTO
            dbo.SSO_SAVINGS_OWNER_MAP
            ( 
             SSO_Savings_ID
            ,Client_Hier_Id )
            SELECT
                  @Savings_id
                 ,@Client_Hier_Id
            WHERE
                  NOT EXISTS ( SELECT
                                    1
                               FROM
                                    dbo.SSO_SAVINGS_OWNER_MAP
                               WHERE
                                    SSO_Savings_ID = @Savings_id
                                    AND Client_Hier_Id = @Client_Hier_Id )

END


GO
GRANT EXECUTE ON  [dbo].[cbmsSsoSavingsOwner_Save] TO [CBMSApplication]
GO
