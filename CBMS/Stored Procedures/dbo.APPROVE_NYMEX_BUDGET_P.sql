SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE dbo.APPROVE_NYMEX_BUDGET_P

@userId varchar(10),
@sessionId varchar(20),

@budgetId int,
@monthIdentifier dateTime,
@totalVolumeHedged decimal(32,16),
@wacogOfHedges decimal (32,16),
@totalVolumeUnhedged decimal (32,16),
@projectedMarket decimal (32,16),
@budgetTarget decimal (32,16),
@approve int,
@approveType varchar(50)


as

declare @type int

if @approve=1
begin
	select @type=entity_id from entity where entity_type=260 and entity_name =@approveType
	
	update rm_budget set is_budget_approved=1, APPROVED_TYPE_ID=@type
	where rm_budget_id=@budgetId


	insert into rm_target_budget (RM_BUDGET_ID,
		MONTH_IDENTIFIER,
		TOTAL_VOLUME_HEDGED,
		WACOG_OF_HEDGES,
		TOTAL_VOLUME_UNHEDGED,PROJECTED_MARKET,BUDGET_TARGET) values
		(
			@budgetId,@monthIdentifier,@totalVolumeHedged,@wacogOfHedges,
			@totalVolumeUnhedged,@projectedMarket,@budgetTarget
		
		)
end
else
begin
	update rm_budget set is_budget_approved=0
	where rm_budget_id=@budgetId	
	
	delete from rm_target_budget where rm_budget_id=@budgetId
end
GO
GRANT EXECUTE ON  [dbo].[APPROVE_NYMEX_BUDGET_P] TO [CBMSApplication]
GO
