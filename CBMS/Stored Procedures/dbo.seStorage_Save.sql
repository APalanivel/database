SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE    Procedure dbo.seStorage_Save
	(
		@StorageId int = null
	,	@ReportDate datetime
	,       @WeekEnding datetime
	, @WeekOfYear int
	, @Volume int
	, @Change int
	)
As
BEGIN
	set nocount on
	
	declare @ThisId int
	
	set @ThisId = @StorageId
	
	if @ThisId is null
	begin
		select @ThisId = StorageId
		  from seStorage
		 where ReportDate = @ReportDate
	
	end
	if @ThisId is null
	begin
	
		insert into seStorage
			(ReportDate, WeekEnding, WeekOfYear, Volume, Change)
			values
			(@ReportDate, @WeekEnding, @WeekOfYear, @Volume, @Change)
	
			set @ThisId = @@IDENTITY
	
	end
	else
	begin
	
		update seStorage
		   set ReportDate = @ReportDate
				 , WeekOfYear = @WeekOfYear
				 , Volume = @Volume
				 , Change = @Change
				 , WeekEnding = @WeekEnding
		 where StorageId = @ThisId
	
	end
	
	exec seStorage_Get @ThisId
END
GO
GRANT EXECUTE ON  [dbo].[seStorage_Save] TO [CBMSApplication]
GO
