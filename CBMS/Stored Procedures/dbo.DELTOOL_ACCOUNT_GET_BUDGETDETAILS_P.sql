SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.DELTOOL_ACCOUNT_GET_BUDGETDETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	int       	          	
	@account_id    	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

--select * from budget_account
--select * from budget


--exec DELTOOL_ACCOUNT_GET_BUDGETDETAILS_P 1,127

CREATE     procedure dbo.DELTOOL_ACCOUNT_GET_BUDGETDETAILS_P
( 
	@userId int
	,@account_id int
)
AS
BEGIN

	select ba.budget_id
		,b.BUDGET_NAME as 'BUDGET_NAME'
		,b.DATE_POSTED 
		,b.IS_POSTED_TO_DV
		,ISNULL(b.ORIGINAL_BUDGET_ID , 0)AS ORIGINAL_BUDGET_ID
	from budget_account ba
	left join budget b on ba.budget_id = b.budget_id 
	where ba.account_id = @account_id

END
GO
GRANT EXECUTE ON  [dbo].[DELTOOL_ACCOUNT_GET_BUDGETDETAILS_P] TO [CBMSApplication]
GO
