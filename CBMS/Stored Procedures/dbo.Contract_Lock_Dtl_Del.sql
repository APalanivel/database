SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********       
NAME:  dbo.Contract_Lock_Dtl_Del      
     
DESCRIPTION: Delete LOcks from Contract_Lock_Dtl table
    
INPUT PARAMETERS:        
      Name					DataType          Default     Description        
------------------------------------------------------------        
	  @Contract_Lock_Dtl_Id	INT
     
        
        
OUTPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:   

BEGIN TRAN
select * from dbo.contract_lock_Dtl where contract_Lock_Dtl_Id = 14

EXEC DBO.Contract_Lock_Dtl_Del 14

select * from dbo.contract_lock_Dtl where contract_Lock_Dtl_Id = 14
ROLLBACK TRAN

------------------------------------------------------------  
    
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
AKR		 Ashok Kumar Raju
    
    
Initials Date		Modification      
------------------------------------------------------------      
AKR		2012-09-28  Created new sp (for POCO Project)
******/
CREATE PROCEDURE dbo.Contract_Lock_Dtl_Del
      ( 
       @Contract_Lock_Dtl_Id INT )
AS 
BEGIN
      SET NOCOUNT ON ;
      
      DELETE
            cld
      FROM
            dbo.Contract_Lock_Dtl cld
      WHERE
            cld.Contract_Lock_Dtl_Id = @Contract_Lock_Dtl_Id
                  
END
;
GO
GRANT EXECUTE ON  [dbo].[Contract_Lock_Dtl_Del] TO [CBMSApplication]
GO
