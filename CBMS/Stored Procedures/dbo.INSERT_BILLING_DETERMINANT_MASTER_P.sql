SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE dbo.INSERT_BILLING_DETERMINANT_MASTER_P
	@bdType INT,
	@bdDisplayId CHAR(4),
	@parentId INT,
	@parentType	INT
AS
BEGIN

	SET NOCOUNT ON

	INSERT INTO dbo.billing_determinant_master (
		bd_type_id, 
		bd_display_id, 
		bd_parent_id, 
		bd_parent_type_id)  
	VALUES (@bdType, 
		@bdDisplayId , 
		@parentId, 
		@parentType)

END
GO
GRANT EXECUTE ON  [dbo].[INSERT_BILLING_DETERMINANT_MASTER_P] TO [CBMSApplication]
GO
