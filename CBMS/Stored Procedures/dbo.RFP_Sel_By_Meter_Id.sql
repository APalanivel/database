SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[RFP_Sel_By_Meter_Id]  
     
DESCRIPTION: 
	To Get RFP Information for Selected Meter Id with pagination.
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Meter_Id		INT						
@Start_Index	INT			1
@End_Index		INT			2147483647
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        
  USE [CBMS_TK3]
  

	EXEC RFP_Sel_By_Meter_Id  62461,1,1
	
	EXEC RFP_Sel_By_Meter_Id  559,1,3

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR		PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR		03-JUNE-10		CREATED

*/

CREATE PROCEDURE dbo.RFP_Sel_By_Meter_Id
(
	@Meter_Id		INT
   ,@Start_Index	INT = 1
   ,@End_Index		INT = 2147483647
)
AS
BEGIN

	SET NOCOUNT ON;

	WITH Cte_Rfp_Ids
	AS
	 (
	  SELECT
	        sra.SR_RFP_ID
		    ,Row_Num = ROW_NUMBER() OVER (ORDER BY sra.SR_RFP_ID)
		    ,Total = COUNT(1) OVER ()
	  FROM
		  dbo.SR_Rfp_Account_Meter_Map srra
		  JOIN dbo.SR_RFP_ACCOUNT sra
			   ON srra.SR_RFP_ACCOUNT_ID = sra.SR_RFP_ACCOUNT_ID
	  WHERE
		   srra.METER_ID = @Meter_Id
		   AND sra.IS_DELETED = 0
	)
	SELECT
		cte_rfp.SR_RFP_ID
		,cte_rfp.Total
	FROM
		Cte_Rfp_Ids cte_rfp
	WHERE
		cte_rfp.Row_Num BETWEEN @Start_Index AND @End_Index

END
GO
GRANT EXECUTE ON  [dbo].[RFP_Sel_By_Meter_Id] TO [CBMSApplication]
GO
