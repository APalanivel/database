SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    [Workflow].[Workflow_Queue_Download_Output_Data]
DESCRIPTION: 
------------------------------------------------------------   
 INPUT PARAMETERS:    
 Name   DataType  Default Description    
	@Module_Id INT 
------------------------------------------------------------    
 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
 
------------------------------------------------------------    
 USAGE EXAMPLES:    
 EXEC [Workflow].[Workflow_Queue_Download_Output_Data]  @Module_Id =1
------------------------------------------------------------    
AUTHOR INITIALS:    
Initials	Name    
------------------------------------------------------------    
TRK			Ramakrishna Thummala	Summit Energy 
 
 MODIFICATIONS     
 Initials Date   Modification    
------------------------------------------------------------    
 TRK		  Sep-23--2019  Created

******/
CREATE PROCEDURE [Workflow].[Workflow_Queue_Download_Output_Data]       
(        
 @Module_Id INT = NULL        
        
)        
AS        
BEGIN        
   
 SELECT    
  Workflow_Queue_Download_Id,  
  Workflow_Queue_Download_Name,  
  Workflow_Queue_File_Name + REPLACE(REPLACE(CONVERT(NVARCHAR, GETDATE(), 107), ' ', '_'),',','') + '.' +  Extension_Type  AS [File_Name]  
 FROM  [Workflow].[Workflow_Queue_Download] WHERE Workflow_Queue_Id = @Module_Id  
        
END                      
GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_Download_Output_Data] TO [CBMSApplication]
GO
