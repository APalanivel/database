SET NUMERIC_ROUNDABORT OFF 
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******

NAME: dbo.Sso_Savings_History_Del_By_Vendor_Id

DESCRIPTION:

	Used to delete SSO_SAVINGS history.

INPUT PARAMETERS:
	NAME				DATATYPE	DEFAULT		DESCRIPTION
----------------------------------------------------------------
	@Vendor_Id			INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
	USAGE EXAMPLES:
------------------------------------------------------------ 

	BEGIN TRAN

		EXEC dbo.Sso_Savings_History_Del_By_Vendor_Id 29

	ROLLBACK TRAN

	SELECT TOP 100 * FROM SSO_SAVINGS s 
	WHERE 
		NOT EXISTS(SELECT 1 FROM SSO_SAVINGS_DETAIL d WHERE d.SSo_Savings_Id = s.SSo_Savings_Id)
		AND 
		vendor_id = 29
		
	
AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	HG			Harihara Suthan G
	CPE			Chaitanya Panduga Eshwar

MODIFICATIONS
	INITIALS	DATE			MODIFICATION
------------------------------------------------------------
	HG			9/24/2010		Created
	CPE			04/13/2011		Modified the SP to handle the change in the structure of SAVINGS_OWNER_MAP table.

*/
CREATE PROCEDURE dbo.Sso_Savings_History_Del_By_Vendor_Id
    (
       @Vendor_Id	INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DECLARE @Sso_Savings_Owner_Map_List TABLE(Sso_Savings_Id INT, Client_Hier_Id INT)
	DECLARE @Sso_Project_Savings_Map_List TABLE(Sso_Project_Id INT, Sso_Savings_Id INT)
	DECLARE @Sso_Savings_Detail_List TABLE(Sso_Savings_Id INT, Service_Month DATETIME)
	DECLARE @Sso_Savings_List TABLE(Sso_Savings_Id INT)


	DECLARE @SSo_Savings_Id INT
		,@Client_Hier_Id	INT
		,@Sso_Project_Id	INT
		,@Service_Month		DATETIME

	
	INSERT INTO @Sso_Savings_List
	(
		Sso_Savings_Id
	)
	SELECT
		SSo_Savings_Id
	FROM
		dbo.SSO_SAVINGS ss
	WHERE
		ss.VENDOR_ID = @Vendor_Id
	
	INSERT INTO @Sso_Savings_Detail_List
	(
		Sso_Savings_Id
		,Service_Month
	)
	SELECT
		ssd.Sso_Savings_Id
		,ssd.Service_Month
	FROM
		dbo.SSO_SAVINGS_DETAIL ssd
		JOIN @Sso_Savings_List ss
			ON ss.Sso_Savings_Id = ssd.SSO_SAVINGS_ID


	INSERT INTO @Sso_Project_Savings_Map_List
	(
		Sso_Project_Id
		,Sso_Savings_Id
	)
	SELECT
		psm.SSO_PROJECT_ID
		,psm.SSO_SAVINGS_ID
	FROM
		dbo.SSO_PROJECT_SAVINGS_MAP psm
		JOIN @Sso_Savings_List ss
			ON ss.Sso_Savings_Id = psm.SSO_SAVINGS_ID

	
	INSERT INTO @Sso_Savings_Owner_Map_List
	(
	
		Sso_Savings_Id
		,Client_Hier_Id
	)
	SELECT
		som.SSO_SAVINGS_ID
		,som.Client_Hier_Id
	FROM
		dbo.SSO_SAVINGS_OWNER_MAP som
		JOIN @Sso_Savings_List ss
			ON ss.Sso_Savings_Id = som.SSO_SAVINGS_ID


	BEGIN TRY	
		BEGIN TRAN

			WHILE EXISTS(SELECT 1 FROM @Sso_Savings_Owner_Map_List)
			BEGIN
				
				SELECT TOP 1
					@SSo_Savings_Id =Sso_Savings_Id
					, @Client_Hier_Id = Client_Hier_Id
				FROM
					@Sso_Savings_Owner_Map_List
		
				EXEC dbo.SSO_SAVINGS_OWNER_MAP_Del @SSO_Savings_Id, @Client_Hier_Id
				
				DELETE
				FROM
					@Sso_Savings_Owner_Map_List
				WHERE
					SSo_Savings_Id = @Sso_Savings_Id
					AND Client_Hier_Id = @Client_Hier_Id

			END
			

			WHILE EXISTS(SELECT 1 FROM @Sso_Project_Savings_Map_List)
			BEGIN
				
				SELECT TOP 1
					@Sso_Project_Id =Sso_Project_Id
					, @SSo_Savings_Id = Sso_Savings_Id
				FROM
					@Sso_Project_Savings_Map_List
		
				EXEC dbo.Sso_Project_Savings_Map_Del @Sso_Project_Id,@SSO_Savings_Id

				DELETE
				FROM
					@Sso_Project_Savings_Map_List
				WHERE
					Sso_Project_Id = @Sso_Project_Id
					AND SSo_Savings_Id = @Sso_Savings_Id

			END

			WHILE EXISTS(SELECT 1 FROM @Sso_Savings_Detail_List)
			BEGIN

				SELECT TOP 1
					@SSo_Savings_Id = Sso_Savings_Id
					,@Service_Month = Service_Month
				FROM
					@Sso_Savings_Detail_List

				EXEC dbo.Sso_Savings_Detail_Del @Sso_Savings_Id,@Service_Month

				DELETE
				FROM
					@Sso_Savings_Detail_List
				WHERE
					SSo_Savings_Id = @Sso_Savings_Id
					AND Service_Month = @Service_Month

			END

			WHILE EXISTS(SELECT 1 FROM @Sso_Savings_List)
			BEGIN

				SELECT TOP 1
					@SSo_Savings_Id = Sso_Savings_Id
				FROM
					@Sso_Savings_List

				EXEC dbo.Sso_Savings_Del @Sso_Savings_Id

				DELETE
				FROM
					@Sso_Savings_List
				WHERE
					SSo_Savings_Id = @Sso_Savings_Id

			END

		COMMIT TRAN
	END TRY
	BEGIN CATCH

		ROLLBACK TRAN
		EXEC usp_RethrowError

	END CATCH

END


GO
GRANT EXECUTE ON  [dbo].[Sso_Savings_History_Del_By_Vendor_Id] TO [CBMSApplication]
GO
