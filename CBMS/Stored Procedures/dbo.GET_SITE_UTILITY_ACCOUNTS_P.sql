SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--exec dbo.GET_SITE_UTILITY_ACCOUNTS_P 27

CREATE    PROCEDURE dbo.GET_SITE_UTILITY_ACCOUNTS_P
	@site_id int
	AS
	begin
		set nocount on

		select distinct utilacc.ACCOUNT_ID, 
				utilacc.ACCOUNT_NUMBER,  
				ven.VENDOR_ID, 
				ven.vendor_name  

		from	account utilacc 
			join vendor ven on ven.vendor_id = utilacc.vendor_id
			and utilacc.site_id = @site_id 	

	end
GO
GRANT EXECUTE ON  [dbo].[GET_SITE_UTILITY_ACCOUNTS_P] TO [CBMSApplication]
GO
