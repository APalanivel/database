SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
		dbo.Client_Commodity_Sel_By_Client_Commodity  
  
DESCRIPTION:  

  
INPUT PARAMETERS:  
 Name				DataType  Default Description  
---------------------------------------------------------------  
 @Client_Id			INT  
 @Commodity_Id		INT
   
  
OUTPUT PARAMETERS:  
 Name     DataType  Default Description  
------------------------------------------------------------  
   
USAGE EXAMPLES:  
------------------------------------------------------------  
  
exec dbo.Client_Commodity_Sel_By_Client_Commodity 235,100060  
exec dbo.Client_Commodity_Sel_By_Client_Commodity 11554,1374  
exec dbo.Client_Commodity_Sel_By_Client_Commodity 11568,291  
  
  
AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 RR			Raghu Reddy

MODIFICATIONS  
 Initials	Date		Modification  
------------------------------------------------------------  
 RR			2013-06-19	Created	ENHANCE-40
  
******/  
CREATE PROCEDURE dbo.Client_Commodity_Sel_By_Client_Commodity
      ( 
       @Client_Id INT
      ,@Commodity_Id INT )
AS 
BEGIN  
      SET NOCOUNT ON ;  
      
      SELECT
            cc.Commodity_Id
           ,com.Commodity_Name
           ,cc.Variance_Effective_Dt
      FROM
            Core.Client_Commodity cc
            INNER JOIN dbo.Commodity com
                  ON cc.Commodity_Id = com.Commodity_Id
            INNER JOIN dbo.Code cd
                  ON cd.Code_Id = cc.Commodity_Service_Cd
      WHERE
            cd.Code_Value = 'Invoice'
            AND cc.Client_Id = @Client_Id
            AND cc.Commodity_Id = @Commodity_Id
      GROUP BY
            cc.Commodity_Id
           ,com.Commodity_Name
           ,cc.Variance_Effective_Dt          
       
END ;
;
GO
GRANT EXECUTE ON  [dbo].[Client_Commodity_Sel_By_Client_Commodity] TO [CBMSApplication]
GO
