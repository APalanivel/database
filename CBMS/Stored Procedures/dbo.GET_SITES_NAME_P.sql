SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_SITES_NAME_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@clientId      	int       	          	
	@hedgeTypeId   	int       	          	
	@hedgeLevelId  	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--exec GET_SITES_NAME_P 1,1,201,586,547

CREATE        PROCEDURE dbo.GET_SITES_NAME_P
@userId varchar(10),
@sessionId varchar(20),
@clientId integer,
@hedgeTypeId integer,
@hedgeLevelId integer

AS
set nocount on

IF @hedgeTypeId > 0 AND @hedgeLevelId > 0
	BEGIN
		SELECT vw.SITE_ID,vw.site_name
		FROM SITE s,vwSiteName vw
		where s.site_id=vw.site_id and
		s.SITE_ID IN
		(
		select RM_ONBOARD_HEDGE_SETUP.SITE_ID
		
		from 
			RM_ONBOARD_CLIENT,
			RM_ONBOARD_HEDGE_SETUP
		where
			RM_ONBOARD_CLIENT.client_id=@clientId and 
			RM_ONBOARD_HEDGE_SETUP.HEDGE_TYPE_ID=@hedgeTypeId and
			RM_ONBOARD_HEDGE_SETUP.RM_ONBOARD_CLIENT_ID=RM_ONBOARD_CLIENT.RM_ONBOARD_CLIENT_ID AND
			RM_ONBOARD_HEDGE_SETUP.HEDGE_LEVEL_TYPE_ID IN
			((select entity_id from entity where entity_type=262 and entity_name='CORPORATE'),
			 (select entity_id from entity where entity_type=262 and entity_name='DIVISION'),
			 (select entity_id from entity where entity_type=262 and entity_name='GROUP'),@hedgeLevelId))
order by vw.site_name
		
		
	END
ELSE 
	BEGIN 
		SELECT s.SITE_ID, viewSite.SITE_NAME
		FROM SITE s, DIVISION d, vwSiteName viewSite
		WHERE 	d.CLIENT_ID = @clientId AND 
			d.DIVISION_ID = s.DIVISION_ID AND 
			s.SITE_ID = viewSite.SITE_ID
		ORDER BY viewSite.SITE_NAME
	END
GO
GRANT EXECUTE ON  [dbo].[GET_SITES_NAME_P] TO [CBMSApplication]
GO
