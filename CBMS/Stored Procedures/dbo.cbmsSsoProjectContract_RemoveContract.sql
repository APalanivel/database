SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE     PROCEDURE [dbo].[cbmsSsoProjectContract_RemoveContract]
	( @MyAccountId int
	, @project_id int
	, @contract_id int
	)
AS
BEGIN

	set nocount on

	  delete sso_project_contract_map
	  where sso_project_id = @project_id
	    and contract_id = @contract_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoProjectContract_RemoveContract] TO [CBMSApplication]
GO
