SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:   dbo.Invoice_Collection_Issue_Activity_ID_Search        
                      
Description:                      
                
                                   
 Input Parameters:                      
    Name        DataType   Default   Description                        
----------------------------------------------------------------------------------------                        
                               
         
 Output Parameters:                            
    Name        DataType   Default   Description                        
----------------------------------------------------------------------------------------                        
                      
 Usage Examples:                          
----------------------------------------------------------------------------------------           
        
             
           
   declare @Client_Id varchar(MAX)       , @Start_Date DATE = '2018-01-01',
    @End_Date DATE = '2018-12-01'
           
    Exec dbo.[Invoice_Collection_Issue_Activity_ID_Search_1] @Client_Id='11278'  ,   @Start_Date=@Start_Date, @End_Date=@End_Date
	    
          
           
Author Initials:                      
    Initials  Name                      
----------------------------------------------------------------------------------------                        
 RKV    Ravi Kumar Vegesna
 SLP	Sri Lakshmi Pallikonda        
 Modifications:                      
    Initials        Date   Modification                      
----------------------------------------------------------------------------------------                        
  RKV			2019-09-27   Created for search on the basis of Activity ID      
  SLP           2019-09-28   Included additional parameter  @Issue_Activity_Id, @AlternateAccount ,@TypeOfIssueCodeId
							  @Is_Blocker,@Issue_Entity_Owner_Cd,@Tvp_Invoice_Collection_Sources
            
                     
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Issue_Activity_ID_Search]
(
    @Invoice_Collection_Officer_Id INT = NULL,
    @Start_Date DATE = NULL,
    @End_Date DATE = NULL,
    @Client_Id VARCHAR(MAX) = NULL, --'14278,14643',
    @Site_Id INT = NULL,
    @Account_ID INT = NULL,
    @Country_Id INT = NULL,
    @State_Id INT = NULL,
    @Commodity_Id INT = NULL,
    @Vendor_Type CHAR(8) = NULL,
    @Vendor_Id VARCHAR(MAX) = NULL,
    @Issue_Status_Cd INT = NULL,
    @Issue_Activity_Id INT = NULL,
    @AlternateAccount VARCHAR(255) = NULL,
    @TypeOfIssueCodeId INT = NULL,
    @Is_Blocker BIT = NULL,
    @Issue_Entity_Owner_Cd VARCHAR(50) = NULL,
    @Tvp_Invoice_Collection_Sources tvp_Invoice_Collection_Source READONLY
)
WITH RECOMPILE
AS
BEGIN

    SET NOCOUNT ON;

    

    DECLARE @Source_Type_Client INT,
            @Source_Type_Account INT,
            @Source_Type_Vendor INT,
            @Contact_Level_Client INT,
            @Contact_Level_Account INT,
            @Contact_Level_Vendor INT;
    DECLARE @Cnt_Of_Invoice_Collection_Source BIT = 0,
            @Invoice_Collection_Type_Cd INT,
            @Invoice_Collection_Issue_Status_Cd INT;

    CREATE TABLE #Invoice_Collection_Account_Config_Ids
    (
        Invoice_Collection_Account_Config_Id INT,
        Account_Id INT,
        Invoice_Collection_Service_Start_Dt DATE,
        Invoice_Collection_Service_End_Dt DATE,
        Client_Id INT,
        Client_Name VARCHAR(255),
        Site_Id INT,
        Site_Name VARCHAR(255),
        Country_Id INT,
        Country_Name VARCHAR(255),
        State_Id INT,
        State_Name VARCHAR(255),
        Account_Number VARCHAR(255),
        Commodity_Id INT,
        Commodity_Name VARCHAR(255),
       Account_Type VARCHAR(255),
        Account_Vendor_Name VARCHAR(255),
        Account_Vendor_Id VARCHAR(255),
        Invoice_Collection_Alternative_Account_Number VARCHAR(255)
            PRIMARY KEY CLUSTERED (Invoice_Collection_Account_Config_Id)
    );

    SELECT @Cnt_Of_Invoice_Collection_Source = 1
    FROM @Tvp_Invoice_Collection_Sources tvp;


    /*Vendor details*/

    CREATE TABLE #Client_hier_Account_Details
    (
        Account_Vendor_Name VARCHAR(200),
        Account_Type CHAR(8),
        Account_Vendor_Id INT,
        Account_Id INT,
        Invoice_Collection_Account_Config_Id INT
    );

    CREATE TABLE #Consolidated_Billing_Account_Id
    (
        Account_Id INT
    );


    CREATE TABLE #Vendor_Account_Details
    (
        Account_Vendor_Name VARCHAR(200),
        Account_Type CHAR(8),
        Account_Vendor_Id INT,
        Account_Id INT,
        Invoice_Collection_Account_Config_Id INT
    );


    CREATE TABLE #Ic_Account_Details
    (
        Account_Vendor_Name VARCHAR(200),
        Account_Type CHAR(8),
        Account_Vendor_Id INT,
        Account_Id INT,
        Invoice_Collection_Account_Config_Id INT
    );

    CREATE TABLE #Invoice_Collection_Accounts
    (
        Invoice_Collection_Account_Config_Id INT,
        Account_Id INT,
        Invoice_Collection_Service_End_Dt DATE,
        Account_Vendor_Name VARCHAR(200),
        Account_Type CHAR(8),
        Account_Vendor_Id INT
    );
    CREATE TABLE #Vendor_Dtls
    (
        Account_Vendor_Name VARCHAR(200),
        Account_Vendor_Type CHAR(8),
        Account_Vendor_Id INT,
        Invoice_Collection_Account_Config_Id INT
    );

    CREATE CLUSTERED INDEX cx_#Vendor_Dtls
    ON #Vendor_Dtls (Invoice_Collection_Account_Config_Id);

    DECLARE @Filtered_Vendor_Id TABLE
    (
        Vendor_Id INT
    );

    INSERT INTO @Filtered_Vendor_Id
    (
        Vendor_Id
    )
    SELECT us.Segments
    FROM dbo.ufn_split(@Vendor_Id, ',') us;

    /*Vendor detail table  ends*/
    INSERT INTO #Invoice_Collection_Account_Config_Ids
    (
        Invoice_Collection_Account_Config_Id,
        Account_Id,
        Invoice_Collection_Service_Start_Dt,
        Invoice_Collection_Service_End_Dt,
        Client_Id,
        Client_Name,
        Site_Id,
        Site_Name,
        Country_Id,
        Country_Name,
        State_Id,
        State_Name,
        Account_Number,
        Commodity_Id,
        Commodity_Name,
        Account_Type,
        Account_Vendor_Name,
        Account_Vendor_Id,
        Invoice_Collection_Alternative_Account_Number
    )
    SELECT icac.Invoice_Collection_Account_Config_Id,
           MAX(icac.Account_Id),
           MAX(icac.Invoice_Collection_Service_Start_Dt),
           MAX(icac.Invoice_Collection_Service_End_Dt),
           MAX(ch.Client_Id),
           MAX(ch.Client_Name),
           MAX(ch.Site_Id),
           MAX(ch.Site_name),
           MAX(ch.Country_Id),
           MAX(ch.Country_Name),
           MAX(ch.State_Id),
           MAX(ch.State_Name),
           MAX(cha.Account_Number),
           MAX(cha.Commodity_Id),
           MAX(c.Commodity_Name),
           MAX(cha.Account_Type),
           MAX(cha.Account_Vendor_Name),
           MAX(cha.Account_Vendor_Id),
           MAX(icac.Invoice_Collection_Alternative_Account_Number)
    FROM dbo.Invoice_Collection_Account_Config icac
        INNER JOIN Core.Client_Hier_Account cha
            ON cha.Account_Id = icac.Account_Id
        INNER JOIN Core.Client_Hier ch
            ON ch.Client_Hier_Id = cha.Client_Hier_Id
        INNER JOIN dbo.Commodity c
            ON c.Commodity_Id = cha.Commodity_Id
        LEFT OUTER JOIN dbo.Account_Invoice_Collection_Source aics
            ON aics.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
               AND aics.Is_Primary = 1
        LEFT OUTER JOIN dbo.Code cics
            ON cics.Code_Id = aics.Invoice_Collection_Source_Cd
        LEFT OUTER JOIN dbo.Code cicst
            ON cicst.Code_Id = aics.Invoice_Source_Type_Cd
        LEFT OUTER JOIN dbo.Code cicsm
            ON cicsm.Code_Id = aics.Invoice_Source_Method_of_Contact_Cd
    WHERE (
              @Invoice_Collection_Officer_Id IS NULL
              OR icac.Invoice_Collection_Officer_User_Id = @Invoice_Collection_Officer_Id
          )
          AND
          (
              @Site_Id IS NULL
              OR ch.Site_Id = @Site_Id
          )
          AND
          (
              @State_Id IS NULL
              OR ch.State_Id = @State_Id
          )
          AND
          (
              @Country_Id IS NULL
              OR ch.Country_Id = @Country_Id
          )
          AND
          (
              @Account_ID IS NULL
              OR icac.Account_Id = @Account_ID
          )
          AND
          (
              @Commodity_Id IS NULL
              OR cha.Commodity_Id = @Commodity_Id
          )
          AND
          (
              @Client_Id IS NULL
              OR EXISTS
    (
        SELECT 1
        FROM dbo.ufn_split(@Client_Id, ',') us
        WHERE us.Segments = ch.Client_Id
    )
          )
          AND
          (
              @AlternateAccount IS NULL
              OR icac.Invoice_Collection_Alternative_Account_Number = @AlternateAccount
          )
          AND
          (
              @Cnt_Of_Invoice_Collection_Source = 0
              OR
              (
                  EXISTS
    (
        SELECT 1
        FROM @Tvp_Invoice_Collection_Sources tvp
        WHERE tvp.Column_Name = 'Invoice_Source_Type_Cd'
              AND aics.Invoice_Source_Type_Cd = CAST(tvp.Column_Value AS INT)
              AND tvp.Column_Type = cics.Code_Value
              AND cicst.Code_Value NOT IN ( 'Online', 'Mail Redirect' )
    )
                  AND EXISTS
    (
        SELECT 1
        FROM @Tvp_Invoice_Collection_Sources tvp
        WHERE tvp.Column_Name = 'Invoice_Source_Method_of_Contact_Cd'
              AND aics.Invoice_Source_Method_of_Contact_Cd = CAST(tvp.Column_Value AS INT)
              AND tvp.Column_Type = cics.Code_Value
              AND cicst.Code_Value NOT IN ( 'Online', 'Mail Redirect' )
    )
              )
              OR (EXISTS
    (
        SELECT 1
        FROM @Tvp_Invoice_Collection_Sources tvp
        WHERE tvp.Column_Name = 'Invoice_Source_Type_Cd'
              AND aics.Invoice_Source_Type_Cd = CAST(tvp.Column_Value AS INT)
              AND tvp.Column_Type = cics.Code_Value
              AND cicst.Code_Value IN ( 'Online', 'Mail Redirect' )
    )
                 )
              OR (EXISTS
    (
        SELECT 1
        FROM @Tvp_Invoice_Collection_Sources tvp
        WHERE tvp.Column_Name = 'Invoice_Source_Type_Cd'
              AND aics.Invoice_Source_Type_Cd = CAST(tvp.Column_Value AS INT)
              AND tvp.Column_Type = cics.Code_Value
              AND cicst.Code_Value IN ( 'Data Feed', 'ETL' )
    )
                 )
          )
    GROUP BY icac.Invoice_Collection_Account_Config_Id;



    SELECT @Source_Type_Client = MAX(   CASE
                                            WHEN c.Code_Value = 'Client Primary Contact' THEN
                                                c.Code_Id
                                        END
                                    ),
           @Source_Type_Account = MAX(   CASE
                                             WHEN c.Code_Value = 'Account Primary Contact' THEN
                                                 c.Code_Id
                                         END
                                     ),
           @Source_Type_Vendor = MAX(   CASE
                                            WHEN c.Code_Value = 'Vendor Primary Contact' THEN
                 c.Code_Id
                     END
                                    )
    FROM dbo.Code c
        INNER JOIN dbo.Codeset cs
            ON c.Codeset_Id = cs.Codeset_Id
    WHERE cs.Codeset_Name = 'InvoiceSourceType'
          AND c.Code_Value IN ( 'Account Primary Contact', 'Client Primary Contact', 'Vendor Primary Contact' );


    SELECT @Contact_Level_Client = MAX(   CASE
                                              WHEN c.Code_Value = 'Client' THEN
                                                  c.Code_Id
                                          END
                                      ),
           @Contact_Level_Account = MAX(   CASE
                                               WHEN c.Code_Value = 'Account' THEN
                                                   c.Code_Id
                                           END
                                       ),
           @Contact_Level_Vendor = MAX(   CASE
                                              WHEN c.Code_Value = 'Vendor' THEN
                                                  c.Code_Id
                                          END
                                      )
    FROM dbo.Code c
        INNER JOIN dbo.Codeset cs
            ON c.Codeset_Id = cs.Codeset_Id
    WHERE cs.Codeset_Name = 'ContactLevel';




    INSERT INTO #Invoice_Collection_Accounts
    (
        Invoice_Collection_Account_Config_Id,
        Account_Id,
        Invoice_Collection_Service_End_Dt,
        Account_Vendor_Name,
        Account_Type,
        Account_Vendor_Id
    )
    SELECT icac.Invoice_Collection_Account_Config_Id,
           icac.Account_Id,
           icac.Invoice_Collection_Service_End_Dt,
           ucha.Account_Vendor_Name,
           ucha.Account_Type,
           ucha.Account_Vendor_Id
    FROM dbo.Invoice_Collection_Account_Config icac
        INNER JOIN Core.Client_Hier_Account ucha
            ON icac.Account_Id = ucha.Account_Id
    WHERE ucha.Account_Type = 'Utility'
          AND EXISTS
    (
        SELECT 1
        FROM #Invoice_Collection_Account_Config_Ids icac1
        WHERE icac.Invoice_Collection_Account_Config_Id = icac1.Invoice_Collection_Account_Config_Id
    )
    GROUP BY icac.Invoice_Collection_Account_Config_Id,
             icac.Account_Id,
             icac.Invoice_Collection_Service_End_Dt,
             ucha.Account_Vendor_Name,
             ucha.Account_Type,
             ucha.Account_Vendor_Id;


    INSERT INTO #Client_hier_Account_Details
    (
        Account_Vendor_Name,
        Account_Type,
        Account_Vendor_Id,
        Account_Id,
        Invoice_Collection_Account_Config_Id
    )
    SELECT scha.Account_Vendor_Name,
           scha.Account_Type,
           scha.Account_Vendor_Id,
           ucha1.Account_Id,
           icac.Invoice_Collection_Account_Config_Id
    FROM Core.Client_Hier_Account scha
        INNER JOIN Core.Client_Hier_Account ucha1
            ON ucha1.Meter_Id = scha.Meter_Id
               AND ucha1.Account_Type = 'Utility'
        INNER JOIN dbo.CONTRACT c
            ON c.CONTRACT_ID = scha.Supplier_Contract_ID
        INNER JOIN dbo.ENTITY ce
            ON c.CONTRACT_TYPE_ID = ce.ENTITY_ID
               AND ce.ENTITY_NAME = 'Supplier'
               AND scha.Account_Type = 'Supplier'
        INNER JOIN #Invoice_Collection_Accounts icac
            ON icac.Account_Id = ucha1.Account_Id
               AND (CASE
                        WHEN icac.Invoice_Collection_Service_End_Dt IS NULL
                             OR icac.Invoice_Collection_Service_End_Dt > GETDATE() THEN
                            GETDATE()
                        ELSE
                            icac.Invoice_Collection_Service_End_Dt
                    END
                   )
               BETWEEN scha.Supplier_Meter_Association_Date AND scha.Supplier_Meter_Disassociation_Date
    GROUP BY scha.Account_Vendor_Name,
             scha.Account_Type,
             scha.Account_Vendor_Id,
             ucha1.Account_Id,
             icac.Invoice_Collection_Account_Config_Id;



    INSERT INTO #Consolidated_Billing_Account_Id
    (
        Account_Id
    )
    SELECT asbv1.Account_Id
FROM dbo.Account_Consolidated_Billing_Vendor asbv1
        INNER JOIN dbo.ENTITY e1
            ON asbv1.Invoice_Vendor_Type_Id = e1.ENTITY_ID
               AND e1.ENTITY_NAME = 'Supplier'
        LEFT OUTER JOIN dbo.VENDOR v1
            ON v1.VENDOR_ID = asbv1.Supplier_Vendor_Id
    WHERE EXISTS
    (
        SELECT 1
        FROM #Invoice_Collection_Accounts icac
        WHERE asbv1.Account_Id = icac.Account_Id
    )
    GROUP BY asbv1.Account_Id;


    INSERT INTO #Vendor_Account_Details
    (
        Account_Vendor_Name,
        Account_Type,
        Account_Vendor_Id,
        Account_Id,
        Invoice_Collection_Account_Config_Id
    )
    SELECT v.VENDOR_NAME,
           e.ENTITY_NAME,
           v.VENDOR_ID,
           asbv.Account_Id,
           icac.Invoice_Collection_Account_Config_Id
    FROM dbo.Account_Consolidated_Billing_Vendor asbv
        INNER JOIN dbo.ENTITY e
            ON asbv.Invoice_Vendor_Type_Id = e.ENTITY_ID
               AND e.ENTITY_NAME = 'Supplier'
        LEFT OUTER JOIN dbo.VENDOR v
            ON v.VENDOR_ID = asbv.Supplier_Vendor_Id
        INNER JOIN #Invoice_Collection_Accounts icac
            ON icac.Account_Id = asbv.Account_Id
               AND (CASE
                        WHEN icac.Invoice_Collection_Service_End_Dt IS NULL
                             OR asbv.Billing_End_Dt > GETDATE() THEN
                            GETDATE()
                        ELSE
                            icac.Invoice_Collection_Service_End_Dt
                    END
                   )
               BETWEEN asbv.Billing_Start_Dt AND ISNULL(asbv.Billing_End_Dt, '9999-12-31')
    GROUP BY v.VENDOR_NAME,
             e.ENTITY_NAME,
             v.VENDOR_ID,
             asbv.Account_Id,
             icac.Invoice_Collection_Account_Config_Id;


    INSERT INTO #Ic_Account_Details
    (
        Account_Vendor_Name,
        Account_Type,
        Account_Vendor_Id,
        Account_Id,
        Invoice_Collection_Account_Config_Id
    )
    SELECT icav.VENDOR_NAME,
           'Supplier',
           icav.VENDOR_ID,
           icac.Account_Id,
           aics.Invoice_Collection_Account_Config_Id
    FROM dbo.Invoice_Collection_Account_Contact icc
        INNER JOIN dbo.Account_Invoice_Collection_Source aics
            ON icc.Invoice_Collection_Account_Config_Id = aics.Invoice_Collection_Account_Config_Id
        INNER JOIN dbo.Contact_Info ci
            ON ci.Contact_Info_Id = icc.Contact_Info_Id
               AND
               (
                   (
                       @Source_Type_Client = aics.Invoice_Source_Type_Cd
                       AND @Contact_Level_Client = ci.Contact_Level_Cd
                   )
                   OR
                   (
                       @Source_Type_Account = aics.Invoice_Source_Type_Cd
                       AND @Contact_Level_Account = ci.Contact_Level_Cd
                   )
                   OR
                   (
                       @Source_Type_Vendor = aics.Invoice_Source_Type_Cd
                       AND @Contact_Level_Vendor = ci.Contact_Level_Cd
                   )
               )
        INNER JOIN dbo.Vendor_Contact_Map vcm
            ON ci.Contact_Info_Id = vcm.Contact_Info_Id
        INNER JOIN dbo.VENDOR icav
            ON icav.VENDOR_ID = vcm.VENDOR_ID
        INNER JOIN dbo.ENTITY ve
            ON icav.VENDOR_TYPE_ID = ve.ENTITY_ID
        INNER JOIN #Invoice_Collection_Accounts icac
            ON icc.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
               AND icc.Is_Primary = 1
    GROUP BY icav.VENDOR_NAME,
  icav.VENDOR_ID,
    icac.Account_Id,
             aics.Invoice_Collection_Account_Config_Id;



    INSERT INTO #Vendor_Dtls
    (
        Account_Vendor_Name,
        Account_Vendor_Type,
        Account_Vendor_Id,
        Invoice_Collection_Account_Config_Id
    )
    SELECT avd.Account_Vendor_Name,
           avd.Account_Type,
           avd.Account_Vendor_Id,
           avd.Invoice_Collection_Account_Config_Id
    FROM #Client_hier_Account_Details avd
    WHERE EXISTS
    (
        SELECT 1
        FROM #Consolidated_Billing_Account_Id cbai
        WHERE avd.Account_Id = cbai.Account_Id
    );



    INSERT INTO #Vendor_Dtls
    (
        Account_Vendor_Name,
        Account_Vendor_Type,
        Account_Vendor_Id,
        Invoice_Collection_Account_Config_Id
    )
    SELECT vad.Account_Vendor_Name,
           vad.Account_Type,
           vad.Account_Vendor_Id,
           vad.Invoice_Collection_Account_Config_Id
    FROM #Vendor_Account_Details vad
    WHERE NOT EXISTS
    (
        SELECT 1
        FROM #Vendor_Dtls vd
        WHERE vad.Invoice_Collection_Account_Config_Id = vd.Invoice_Collection_Account_Config_Id
    );



    INSERT INTO #Vendor_Dtls
    (
        Account_Vendor_Name,
        Account_Vendor_Type,
        Account_Vendor_Id,
        Invoice_Collection_Account_Config_Id
    )
    SELECT icd.Account_Vendor_Name,
           icd.Account_Type,
           icd.Account_Vendor_Id,
           icd.Invoice_Collection_Account_Config_Id
    FROM #Ic_Account_Details icd
    WHERE EXISTS
    (
        SELECT 1
        FROM #Consolidated_Billing_Account_Id cbai
        WHERE icd.Account_Id = cbai.Account_Id
    )
          AND NOT EXISTS
    (
        SELECT 1
        FROM #Vendor_Dtls vd
        WHERE icd.Invoice_Collection_Account_Config_Id = vd.Invoice_Collection_Account_Config_Id
    );



    INSERT INTO #Vendor_Dtls
    (
        Account_Vendor_Name,
        Account_Vendor_Type,
        Account_Vendor_Id,
        Invoice_Collection_Account_Config_Id
    )
    SELECT ica.Account_Vendor_Name,
           ica.Account_Type,
           ica.Account_Vendor_Id,
           ica.Invoice_Collection_Account_Config_Id
    FROM #Invoice_Collection_Accounts ica
    WHERE NOT EXISTS
    (
        SELECT 1
        FROM #Vendor_Dtls vd
        WHERE ica.Invoice_Collection_Account_Config_Id = vd.Invoice_Collection_Account_Config_Id
    );

    --SELECT *
    --FROM #Vendor_Dtls vd;

	

    SELECT icaci.Invoice_Collection_Account_Config_Id,
           icil.Invoice_Collection_Activity_Id,
           icie.Event_Desc,
           icie.Invoice_Collection_Issue_Event_Id,
           icemt.Event_Description,
		   MAX(icil.Invoice_Collection_Issue_Log_Id) Invoice_Collection_Issue_Log_Id
    INTO #TempLatestExternalComment
    FROM dbo.Invoice_Collection_Issue_Log icil
        INNER JOIN dbo.Invoice_Collection_Queue icq
            ON icq.Invoice_Collection_Queue_Id = icil.Invoice_Collection_Queue_Id
        INNER JOIN #Invoice_Collection_Account_Config_Ids icaci
            ON icaci.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
        INNER JOIN dbo.Invoice_Collection_Issue_Event icie
            ON icie.Invoice_Collection_Issue_Log_Id = icil.Invoice_Collection_Issue_Log_Id
        INNER JOIN Code ec
            ON icie.Issue_Event_Type_Cd = ec.Code_Id
        INNER JOIN
        (
            SELECT icil1.Invoice_Collection_Activity_Id,
                   MAX(   CASE
                              WHEN c.Code_Value = 'IC External Comments' THEN
                                  icie1.Invoice_Collection_Issue_Event_Id
                              WHEN c.Code_Value = 'IC Internal Comments' THEN
                                  icie1.Invoice_Collection_Issue_Event_Id
                              ELSE
                                  0
                          END
                      ) Invoice_Collection_Issue_Event_Id,
                   MAX(   CASE
                  WHEN c.Code_Value = 'IC External Comments' THEN
                                  'IC External Comments'
                              ELSE
                                  'IC Internal Comments'
                          END
                      ) Event_Description
            FROM Invoice_Collection_Issue_Event icie1
                INNER JOIN Invoice_Collection_Issue_Log icil1
                    ON icil1.Invoice_Collection_Issue_Log_Id = icie1.Invoice_Collection_Issue_Log_Id
                INNER JOIN dbo.Code c
                    ON c.Code_Id = icie1.Issue_Event_Type_Cd
            GROUP BY icil1.Invoice_Collection_Activity_Id
        ) icemt
            ON icemt.Invoice_Collection_Activity_Id = icil.Invoice_Collection_Activity_Id
    WHERE (
              icil.Invoice_Collection_Activity_Id = @Issue_Activity_Id
              OR @Issue_Activity_Id IS NULL
          )
          AND ec.Code_Value IN ( 'IC External Comments', 'IC Internal Comments' )
          AND icemt.Invoice_Collection_Issue_Event_Id = icie.Invoice_Collection_Issue_Event_Id
		   AND (   (   @Start_Date IS NULL
                        AND @End_Date IS NULL)
                    OR  (   @Start_Date IS NOT NULL
                            AND @End_Date IS NULL
                            AND @Start_Date <= icq.Collection_Start_Dt)
                    OR  (   @Start_Date IS NULL
                            AND @End_Date IS NOT NULL
                            AND @End_Date >= icq.Collection_End_Dt)
                    OR  (   @Start_Date IS NOT NULL
                            AND @End_Date IS NOT NULL
                            AND (   @Start_Date <= icq.Collection_Start_Dt
                                    AND @End_Date >= icq.Collection_End_Dt)))
    GROUP BY icaci.Invoice_Collection_Account_Config_Id,
             icil.Invoice_Collection_Activity_Id,
             icie.Event_Desc,
             icie.Invoice_Collection_Issue_Event_Id,
             icemt.Event_Description;



    SELECT icac.Client_Id,
           icac.Client_Name,
           icac.Site_Id,
           icac.Site_Name,
           icac.Country_Id,
           icac.Country_Name,
           icac.State_Id,
           icac.State_Name,
           CASE
               WHEN LEN(Commodities.Commodity_Name) > 0 THEN
                   LEFT(Commodities.Commodity_Name, LEN(Commodities.Commodity_Name) - 1)
               ELSE
                   Commodities.Commodity_Name
           END AS Commodity_Name,
           ISNULL(vd.Account_Vendor_Type, icac.Account_Type) Account_Type,
           ISNULL(vd.Account_Vendor_Name, icac.Account_Vendor_Name) Account_Vendor_Name,
           ISNULL(vd.Account_Vendor_Id, icac.Account_Vendor_Id) AS Account_Vendor_Id,
           icac.Account_Number,
           icac.Account_Id,
           CASE
               WHEN LEN(chase.Period_to_chase) > 0 THEN
                   LEFT(chase.Period_to_chase, LEN(chase.Period_to_chase) - 1)
               ELSE
                   chase.Period_to_chase
           END AS Period_to_chase,
           tlec.Event_Desc,
           ROW_NUMBER() OVER (ORDER BY icac.Account_Number) AS Row_Num,
           COUNT(1) OVER () Total_Count,
           tlec.Invoice_Collection_Activity_Id,
           tlec.Event_Description Event_Type,
           tlec.Invoice_Collection_Issue_Event_Id,
           icac.Invoice_Collection_Alternative_Account_Number,
           ical.Created_Ts Created_Ts,
           ieoc.Code_Value Issue_Entity_Owner,
           isc.Code_Value Issue_Status,
           itc.Code_Value Issue_Type
    FROM #Invoice_Collection_Account_Config_Ids icac
        LEFT OUTER JOIN #Vendor_Dtls vd
            ON vd.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
        JOIN #TempLatestExternalComment tlec
            ON icac.Invoice_Collection_Account_Config_Id = tlec.Invoice_Collection_Account_Config_Id
        JOIN dbo.Invoice_Collection_Activity ical
            ON tlec.[Invoice_Collection_Activity_Id] = ical.[Invoice_Collection_Activity_Id]
        JOIN dbo.Invoice_Collection_Issue_Log icil2
            ON icil2.Invoice_Collection_Activity_Id = tlec.Invoice_Collection_Activity_Id
			and icil2.Invoice_Collection_Issue_Log_Id=tlec.Invoice_Collection_Issue_Log_Id
        JOIN Code ieoc
            ON ieoc.Code_Id = icil2.Issue_Entity_Owner_Cd
        JOIN Code isc
            ON isc.Code_Id = icil2.Issue_Status_Cd
        JOIN Code itc
            ON itc.Code_Id = icil2.Invoice_Collection_Issue_Type_Cd
        CROSS APPLY
    (
        SELECT icac1.Commodity_Name + ','
        FROM #Invoice_Collection_Account_Config_Ids icac1
        WHERE icac.Invoice_Collection_Account_Config_Id = icac1.Invoice_Collection_Account_Config_Id
              AND icac.Account_Id = icac1.Account_Id
        GROUP BY icac1.Commodity_Name
        FOR XML PATH('')
    ) Commodities(Commodity_Name)
        CROSS APPLY
    (
        SELECT CONVERT(VARCHAR(12), icq2.Collection_Start_Dt, 105) + ' , '
               + CONVERT(VARCHAR(12), icq2.Collection_End_Dt, 105) + ' ~ '
               + CAST(ISNULL(icil.Invoice_Collection_Issue_Log_Id, '') AS VARCHAR(12))
               + CAST(CASE
                          WHEN (icil.Is_Blocker = 1) THEN
                              'Y'
                          ELSE
                              'N'
                      END AS VARCHAR(1)) + '#'
        FROM #Invoice_Collection_Account_Config_Ids icac2
            INNER JOIN dbo.Invoice_Collection_Queue icq2
                ON icac2.Invoice_Collection_Account_Config_Id = icq2.Invoice_Collection_Account_Config_Id
            INNER JOIN #TempLatestExternalComment tlec2
                ON tlec2.Invoice_Collection_Account_Config_Id = icac2.Invoice_Collection_Account_Config_Id
            INNER JOIN dbo.Invoice_Collection_Issue_Log icil
                ON icil.Invoice_Collection_Queue_Id = icq2.Invoice_Collection_Queue_Id
				and icil.Invoice_Collection_Issue_Log_Id=tlec.Invoice_Collection_Issue_Log_Id
				
        WHERE icac2.Account_Id = icac.Account_Id
              AND icac.Invoice_Collection_Account_Config_Id = icac2.Invoice_Collection_Account_Config_Id
              AND icil.Invoice_Collection_Activity_Id = ical.Invoice_Collection_Activity_Id
			  and icil.Issue_Status_Cd=icil2.Issue_Status_Cd
		
        GROUP BY icq2.Collection_Start_Dt,
                 icq2.Collection_End_Dt,
                 icil.Invoice_Collection_Issue_Log_Id,
                 icil.Is_Blocker
        FOR XML PATH('')
    ) chase(Period_to_chase)
    WHERE (
              @Issue_Activity_Id IS NULL
              OR ical.Invoice_Collection_Activity_Id = @Issue_Activity_Id
          )
          AND 
		  (
              @Issue_Status_Cd IS NULL
              OR isc.Code_Id = @Issue_Status_Cd
          )

          AND
          (
              @Vendor_Type IS NULL
              OR ISNULL(vd.Account_Vendor_Type, icac.Account_Type) = @Vendor_Type
          )
          AND
          (
              @Is_Blocker IS NULL
              OR (icil2.Is_Blocker = @Is_Blocker)
          )
          AND
          (
              @Issue_Entity_Owner_Cd IS NULL
              OR (icil2.Issue_Entity_Owner_Cd = @Issue_Entity_Owner_Cd)
          )
          AND
          (
              @TypeOfIssueCodeId IS NULL
              OR (icil2.Invoice_Collection_Issue_Type_Cd = @TypeOfIssueCodeId)
          )
          AND
          (
              @Vendor_Id IS NULL
              OR EXISTS
    (
        SELECT 1
        FROM @Filtered_Vendor_Id fvi
        WHERE fvi.Vendor_Id = ISNULL(vd.Account_Vendor_Id, icac.Account_Vendor_Id)
    )
          )
        
    GROUP BY icac.Client_Id,
             icac.Client_Name,
             icac.Site_Id,
             icac.Site_Name,
             icac.Country_Id,
             icac.Country_Name,
             icac.State_Id,
          icac.State_Name,
             CASE
                 WHEN LEN(Commodities.Commodity_Name) > 0 THEN
                     LEFT(Commodities.Commodity_Name, LEN(Commodities.Commodity_Name) - 1)
                 ELSE
   Commodities.Commodity_Name
             END,
             ISNULL(vd.Account_Vendor_Type, icac.Account_Type),
             ISNULL(vd.Account_Vendor_Name, icac.Account_Vendor_Name),
             ISNULL(vd.Account_Vendor_Id, icac.Account_Vendor_Id),
             icac.Account_Number,
             icac.Account_Id,
             icac.Invoice_Collection_Alternative_Account_Number,
             CASE
                 WHEN LEN(chase.Period_to_chase) > 0 THEN
                     LEFT(chase.Period_to_chase, LEN(chase.Period_to_chase) - 1)
                 ELSE
                     chase.Period_to_chase
             END,
             tlec.Event_Desc,
             tlec.Event_Description,
             tlec.Invoice_Collection_Activity_Id,
             tlec.Invoice_Collection_Issue_Event_Id,
             ical.Created_Ts,
             ieoc.Code_Value,
             isc.Code_Value,
             itc.Code_Value;

    DROP TABLE #Client_hier_Account_Details,
               #Consolidated_Billing_Account_Id,
               #Ic_Account_Details,
               #Invoice_Collection_Accounts,
               #Invoice_Collection_Account_Config_Ids,
               #Vendor_Account_Details,
               #Vendor_Dtls,
               #TempLatestExternalComment;




END;








GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Issue_Activity_ID_Search] TO [CBMSApplication]
GO
