SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
NAME:        
 dbo.Invoice_Data_Quality_Log_Is_Closed_Upd  
  
DESCRIPTION:  
  Used to select Invoice data quality failed test dtls for the given account , commodity and service month  
    
 INPUT PARAMETERS:        
 Name				DataType		 Default				 Description        
-------------------------------------------------------------------------------------------------        
 @Account_Id		INT  
 @Commodity_Id		INT				NULL  
 @Service_Month		DATE  
   
 OUTPUT PARAMETERS:       
 Name				DataType		 Default				 Description        
-------------------------------------------------------------------------------------------------        
 USAGE EXAMPLES:        
-------------------------------------------------------------------------------------------------  
      
EXEC dbo.Invoice_Data_Quality_Log_Is_Closed_Upd
    34054
   


  
  
 AUTHOR INITIALS:  
 Initials Name  
-------------------------------------------------------------------------------------------------        
 NR   Narayana Reddy  
   
 MODIFICATIONS  
 Initials      Date       Modification  
------------------------------------------------------------  
 NR    2019-04-23  Created Data2.0 -  Data quality.  
  
******/

CREATE PROCEDURE [dbo].[Invoice_Data_Quality_Log_Is_Closed_Upd]
    (
        @Cu_Invoice_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;


        UPDATE
            idql
        SET
            Is_Closed = 1
        FROM
            LOGDB.dbo.Invoice_Data_Quality_Log idql
        WHERE
            idql.Is_Failure = 1
            AND idql.Cu_Invoice_Id = @Cu_Invoice_Id;





    END;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Data_Quality_Log_Is_Closed_Upd] TO [CBMSApplication]
GO
