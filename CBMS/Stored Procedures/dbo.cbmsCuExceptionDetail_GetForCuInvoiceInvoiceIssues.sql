
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********   
NAME:  dbo.cbmsCuExceptionDetail_GetForCuInvoiceInvoiceIssues  
 
DESCRIPTION:    

INPUT PARAMETERS:    
Name					DataType          Default     Description    
------------------------------------------------------------    
@cu_invoice_id			INT 
@Account_ID				INT					= NULL      
    
OUTPUT PARAMETERS:    
Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:  
USE CBMS_TK3
	EXEC cbmsCuExceptionDetail_GetForCuInvoiceInvoiceIssues 30346120

	EXEC cbmsCuExceptionDetail_GetForCuInvoiceInvoiceIssues 1034533,66919
	EXEC cbmsCuExceptionDetail_GetForCuInvoiceInvoiceIssues 24113966,975410

	grantexecute

------------------------------------------------------------  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
NK   Nageswara Rao Kosuri  
SP	 Sandeep Pigilam
RKV  Ravi Kumar Vegesna       


Initials	Date		Modification  
------------------------------------------------------------  
NK			11/26/2009  Created
NK			12/07/2009  Removed @MyAccountId 
NK			02/11/2010  Added distinct keyword to select clause
SKA			19-FEB-2010	Removed the reference of Supplier Account Meter Map table and chnaged the necessary joins with tables.
SKA			04/29/2010	Removed cu_invoice_account_map(Account_id) with cu_invoice_service_month(Account_id)
						Added Input parameter @Account_ID 
						Removed the hard code value for Entity Type & Distinct clause with Group By
SKA			05/31/2010	Added three more columns in select clause	
SSR			06/07/2010	Mapped cu_exception_detail Account id with Cu_invoice_service_month Account_Id	
DMR			09/10/2010 Modified for Quoted_Identifier
SP			2014-08-06  Data Operations Enhancement Phase III,Added case statement for contract_recalc_type column in select list.
RKV         2015-11-03  Added new columns recalc type and commodity_id and added  new optional parameter @commodity_Id as part of AS400-PII				
******/
CREATE PROCEDURE [dbo].[cbmsCuExceptionDetail_GetForCuInvoiceInvoiceIssues]
      ( 
       @cu_invoice_id INT
      ,@Account_ID INT = NULL
      ,@Commodity_Id INT = NULL )
AS 
BEGIN  
  
      SET NOCOUNT ON;

      SELECT
            ced.cu_exception_detail_id
           ,ced.cu_exception_id
           ,eg.entity_id exception_group_type_id
           ,eg.entity_name exception_group_type
           ,ced.exception_type_id
           ,et.exception_type
           ,ced.exception_status_type_id
           ,es.entity_name exception_status_type
           ,ced.opened_date
           ,ced.is_closed
           ,ced.closed_reason_type_id
           ,cr.entity_name closed_reason_type
           ,ced.closed_by_id
           ,ui.first_name + SPACE(1) + ui.last_name closed_by
           ,ced.closed_date
           ,a.Supplier_Account_Recalc_Type_Cd contract_recalc_type_id
           ,CASE WHEN account_type.ENTITY_NAME = 'Utility' THEN 'Utility Recalc'
                 ELSE rt.Code_Value
            END AS contract_recalc_type
           ,a.Account_Id
           ,a.Account_Number
           ,a.ACCOUNT_TYPE_ID
           ,MIN(CASE WHEN rtdc.Code_Value IS NOT NULL THEN rtdc.Code_Value
                     ELSE rtc.Code_Value
                END) AS Recalc_Type
           ,COALESCE(ced2.Commodity_Id, ced.Commodity_ID, acirt.Commodity_ID) AS Commodity_ID
           ,COALESCE(com2.Commodity_Name, c.Commodity_Name, com.Commodity_Name) AS Commodity_Name
      FROM
            dbo.cu_invoice cinv
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                  ON cism.CU_INVOICE_ID = cinv.CU_INVOICE_ID
            INNER JOIN dbo.Account a
                  ON cism.account_id = a.account_id
            INNER JOIN dbo.cu_exception ce
                  ON cinv.cu_invoice_id = ce.cu_invoice_id
            INNER JOIN dbo.cu_exception_detail ced
                  ON ced.cu_exception_id = ce.cu_exception_id
                     AND ced.Account_ID = cism.Account_ID
            INNER JOIN dbo.cu_exception_type et
                  ON et.cu_exception_type_id = ced.exception_type_id
            INNER JOIN dbo.entity eg
                  ON eg.entity_id = et.exception_group_type_id
            INNER JOIN dbo.entity es
                  ON es.entity_id = ced.exception_status_type_id
            LEFT OUTER JOIN dbo.entity cr
                  ON cr.entity_id = ced.closed_reason_type_id
            LEFT OUTER JOIN dbo.user_info ui
                  ON ui.user_info_id = ced.closed_by_id
            LEFT OUTER JOIN dbo.Code rt
                  ON rt.Code_id = a.Supplier_Account_Recalc_Type_Cd
            LEFT OUTER JOIN ( dbo.CU_EXCEPTION_DENORM ced2
                              INNER JOIN dbo.Commodity com2
                                    ON ced2.Commodity_ID = com2.Commodity_Id )
                              ON a.ACCOUNT_ID = ced2.Account_Id
                                 AND ced2.Commodity_ID = ced.Commodity_Id
                                 AND ced2.CU_INVOICE_ID = ce.CU_INVOICE_ID
            LEFT OUTER JOIN dbo.Code rtdc
                  ON rtdc.Code_id = ced2.Recalc_Type_cd
            LEFT OUTER JOIN ( dbo.Account_Commodity_Invoice_Recalc_Type acirt
                              INNER JOIN dbo.Commodity com
                                    ON acirt.Commodity_ID = com.Commodity_Id )
                              ON acirt.Account_Id = ced.ACCOUNT_ID
                                 AND acirt.Commodity_ID = ced.Commodity_Id
                                 AND ( cism.SERVICE_MONTH IS NULL
                                       OR cism.SERVICE_MONTH BETWEEN acirt.Start_Dt
                                                             AND     ISNULL(acirt.End_Dt, '2099-01-01') )
            LEFT OUTER JOIN dbo.Commodity c
                  ON ced.Commodity_Id = c.Commodity_Id
            LEFT OUTER JOIN dbo.Code rtc
                  ON rtc.Code_id = acirt.Invoice_Recalc_Type_Cd
            INNER JOIN dbo.ENTITY account_type
                  ON a.ACCOUNT_TYPE_ID = account_type.ENTITY_ID
      WHERE
            ce.cu_invoice_id = @cu_invoice_id
            AND eg.ENTITY_DESCRIPTION = 'UBM Exception Group'
            AND eg.ENTITY_NAME = 'Invoice Issues'
            AND ( @Account_ID IS NULL
                  OR cism.Account_ID = @Account_ID )
            AND ( @Commodity_Id IS NULL
                  OR ced.Commodity_Id = @Commodity_Id )
      GROUP BY
            ced.cu_exception_detail_id
           ,ced.cu_exception_id
           ,eg.entity_id
           ,eg.entity_name
           ,ced.exception_type_id
           ,et.exception_type
           ,ced.exception_status_type_id
           ,es.entity_name
           ,ced.opened_date
           ,ced.is_closed
           ,ced.closed_reason_type_id
           ,cr.entity_name
           ,ced.closed_by_id
           ,ui.first_name + SPACE(1) + ui.last_name
           ,ced.closed_date
           ,a.Supplier_Account_Recalc_Type_Cd
           ,rt.Code_Value
           ,a.Account_Id
           ,a.Account_Number
           ,a.ACCOUNT_TYPE_ID
           ,account_type.ENTITY_NAME
           ,COALESCE(ced2.Commodity_Id, ced.Commodity_ID, acirt.Commodity_ID)
           ,COALESCE(com2.Commodity_Name, c.Commodity_Name, com.Commodity_Name)
      ORDER BY
            ced.is_closed
           ,ced.opened_date DESC  
END;

;
GO


GRANT EXECUTE ON  [dbo].[cbmsCuExceptionDetail_GetForCuInvoiceInvoiceIssues] TO [CBMSApplication]
GO
