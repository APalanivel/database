SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/**
NAME:    
   
	dbo.RM_Forecast_Volume_Last_Updated
   
 DESCRIPTION:     
   
	This procedure is to get client onboarded details
   
 INPUT PARAMETERS:    
	Name				DataType		 Default		 Description    
----------------------------------------------------------------------      
	@Client_Id			INT
    @Commodity_Id		INT
    @Country_Id			VARCHAR(MAX)	NULL
    @Start_Dt			DATE
    @End_Dt				DATE			NULL
    @Site_Not_Managed	INT				NULL
    @Sitegroup_Id		INT				NULL
    @Site_Id			VARCHAR(MAX)	NULL
   
 OUTPUT PARAMETERS:    
 Name				DataType		 Default		 Description    
----------------------------------------------------------------------

  
  USAGE EXAMPLES:    
------------------------------------------------------------  

  
	EXEC RM_Forecast_Volume_Last_Updated 11236,291
	   
	
  
AUTHOR INITIALS:    
	Initials	Name    
------------------------------------------------------------    
	RR			Raghu Reddy
    
 MODIFICATIONS     
	Initials	Date		Modification    
------------------------------------------------------------    
	RR			2018-09-06	Created For Global Risk Managemnet		


******/

CREATE  PROCEDURE [dbo].[RM_Forecast_Volume_Last_Updated]
     (
         @Client_Id INT
         , @Commodity_Id INT
     )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Last_Change_Ts DATETIME
            , @Last_Updated_User VARCHAR(100);

        SELECT
            @Last_Change_Ts = MAX(chfv.Last_Change_Ts)
        FROM
            Trade.RM_Client_Hier_Forecast_Volume chfv
            INNER JOIN Core.Client_Hier ch
                ON chfv.Client_Hier_Id = ch.Client_Hier_Id
        WHERE
            ch.Client_Id = @Client_Id
            AND chfv.Commodity_Id = @Commodity_Id;

        SELECT
            @Last_Updated_User = ui.FIRST_NAME + ' ' + ui.LAST_NAME
        FROM
            dbo.USER_INFO ui
            INNER JOIN Trade.RM_Client_Hier_Forecast_Volume chfv
                ON ui.USER_INFO_ID = chfv.Updated_User_Id
            INNER JOIN Core.Client_Hier ch
                ON chfv.Client_Hier_Id = ch.Client_Hier_Id
        WHERE
            ch.Client_Id = @Client_Id
            AND chfv.Commodity_Id = @Commodity_Id
            AND chfv.Last_Change_Ts = @Last_Change_Ts;

        SELECT
            @Last_Updated_User AS Last_Updated_User
            , @Last_Change_Ts AS Last_Change_Ts;



    END;


GO
GRANT EXECUTE ON  [dbo].[RM_Forecast_Volume_Last_Updated] TO [CBMSApplication]
GO
