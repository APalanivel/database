SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE     PROCEDURE dbo.METER_DETAILS_FOR_TEMPLATE_P
	@accountId int,
	@rsStartDate datetime,
	@rsEndDate datetime
	as
	begin
		set nocount on

			select  met.Meter_ID,
				met.Meter_Number, 
				rate.Rate_ID,
				rate.Rate_Name,
				e.Entity_Name,
				met.Account_ID,
				rs.rs_start_date, 
				rs.rs_end_date, 
				rs.Rate_schedule_ID 
				from meter met
				join Rate rate on rate.Rate_ID = met.Rate_ID
				and  met.Account_ID = @accountId 
				join Rate_Schedule rs on rs.rate_id = rate.Rate_ID
				and rs.rs_start_date <= @rsStartDate
				and (rs.rs_end_date >= @rsEndDate  or rs.rs_end_date is null )
				join Entity e on e.Entity_Id = rate.Commodity_Type_ID 
				
				
							
	end
GO
GRANT EXECUTE ON  [dbo].[METER_DETAILS_FOR_TEMPLATE_P] TO [CBMSApplication]
GO
