SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/******      
NAME:    [Workflow].[Workflow_Dynamic_Priority_Client_Attribute]   
DESCRIPTION: This stored procedure is created to update the priority of the invoice  
  
the inputs will come with comma delimited if multiple invoices which need to maek/remove priority  
------------------------------------------------------------     
 INPUT PARAMETERS:      
 Name   DataType  Default Description      
  @CLIENT_ID INT,
  @Priority BIT,
  @User_Id INT   
------------------------------------------------------------      
 OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
 USAGE EXAMPLES:      
 EXEC   [Workflow].[Workflow_Dynamic_Priority_Client_Attribute]   
    @CLIENT_ID =11452,
	@Priority =1  
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
TRK RAMAKRISHNA THUMMALA Summit Energy   
AP	ARUNKUMAR PALANIVEL
   
 MODIFICATIONS       
 Initials Date   Modification      
------------------------------------------------------------      
 TRK    03-OCT-2019 Created  
 AP		OCT 25 2019 MODIFIED FOR BELOW FUNCTIONALITY

Mark Legacy Invoices as Priority if the user marks a client as a priority

If the user removes priority for a client, hold the priority for existing invoices 

Manual Priority will be overridden by Client Priority

When initial the invoice was marked as a priority by a user and then the user marks the client as a priority 

  
******/  
CREATE PROCEDURE [Workflow].[Workflow_Dynamic_Priority_Client_Attribute]
(
 @CLIENT_ID INT,
 @Priority BIT,
 @User_ID INT
)

AS
BEGIN

SET NOCOUNT ON         
        
      DECLARE @Proc_name             VARCHAR(100) = 'Workflow_Dynamic_Priority_Client_Attribute',         
              @Input_Params          VARCHAR (1000),         
              @Error_Line            INT,         
              @Error_Message         VARCHAR(3000) 
			  
			  DECLARE @Source_Cd INT      
		 SELECT @Source_Cd=Code_Id FROM dbo.Code WHERE Code_Value ='Systempriorityclient'      


	  SELECT @Input_Params
			= '@CLIENT_ID:' + CAST(@CLIENT_ID AS VARCHAR) + '@Priority:'
			  + CAST(@Priority AS VARCHAR) ;
BEGIN TRY

MERGE INTO dbo.Client_Attribute TGT              
    USING ( 
			SELECT @CLIENT_ID AS CLIENT_ID , @Priority AS Priority	
		  ) SRC              
			ON SRC.CLIENT_ID = TGT.CLIENT_ID
			AND SRC.Priority=@Priority                     
			WHEN NOT MATCHED THEN INSERT (                
			Client_Id              
		   ,Mark_Invoice_Exception_As_Priority               
		   ,Created_Uer_Id              
		   ,Created_Ts               
		   ,Updated_User_Id              
		   ,Updated_Ts               
			 )              
			VALUES              
			(                
			@CLIENT_ID              
		   ,@Priority              
		   ,@User_ID              
		   ,GETDATE()               
		   ,@User_ID               
		   ,GETDATE()         
		  )              
		WHEN MATCHED THEN UPDATE SET                    
		TGT.Client_Id = @CLIENT_ID              
	  , TGT.Mark_Invoice_Exception_As_Priority=@Priority                    
	  , TGT.Updated_User_Id=@User_ID                
	  , TGT.Updated_Ts =GETDATE();

	 IF  (@Priority =1 )

	 BEGIN 
	 
	  MERGE INTO workflow.cu_invoice_attribute TGT              
    USING ( 
			SELECT DISTINCT ca.cu_invoice_id FROM  
			  cu_invoice ca 
			  JOIN CU_EXCEPTION_DENORM cud 
			  ON ca.CU_INVOICE_ID = cud.CU_INVOICE_ID 
			WHERE ca.client_id = @client_id
			AND cud.EXCEPTION_TYPE <> 'failed recalc' 
			AND NOT EXISTS 
			(SELECT 1 FROM workflow.Cu_Invoice_Attribute CUI 
			where CUI.CU_INVOICE_ID = cud.CU_INVOICE_ID 
			AND CUi.Source_Cd = @Source_Cd
		  )) SRC              
			ON SRC.cu_invoice_id = TGT.cu_invoice_id                    
			WHEN NOT MATCHED THEN INSERT (                
			CU_INVOICE_ID
			,Is_Priority
			,Created_User_Id
			,Created_Ts
			,Updated_User_Id
			,Last_Change_Ts
			,Source_Cd              
			 )              
			VALUES              
			(                
			SRC.cu_invoice_id             
		   ,@Priority              
		   ,16              
		   ,GETDATE()               
		   ,16               
		   ,GETDATE() ,
		   @Source_Cd        
		  )              
		WHEN MATCHED THEN UPDATE SET                    
		TGT.Is_Priority = @priority            
	  , TGT.Updated_User_Id=16                
	  , TGT.Last_Change_Ts =GETDATE(),
	  TGT.Source_Cd = @Source_Cd;

	  
	  END


END TRY         
        
      BEGIN CATCH         
          -- ENTRY MADE TO THE LOGGING SP TO CAPTURE THE ERRORS.         
          SELECT @ERROR_LINE = error_line(),         
                 @ERROR_MESSAGE = error_message()         
        
          INSERT INTO STOREDPROC_ERROR_LOG         
                      (STOREDPROC_NAME,         
                       ERROR_LINE,         
                       ERROR_MESSAGE,         
                       INPUT_PARAMS)         
          VALUES      ( @PROC_NAME,         
                        @ERROR_LINE,         
                        @ERROR_MESSAGE,         
                        @INPUT_PARAMS )         
        
          RETURN -99         
      END CATCH         
  END                                      
GO
GRANT EXECUTE ON  [Workflow].[Workflow_Dynamic_Priority_Client_Attribute] TO [CBMSApplication]
GO
