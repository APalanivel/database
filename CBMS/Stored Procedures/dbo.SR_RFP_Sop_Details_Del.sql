SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[SR_RFP_Sop_Details_Del]  
     
DESCRIPTION: 
	It Deletes SR RFP Sop Details for Selected 
						SR RFP Sop Detail Id.
      
INPUT PARAMETERS:          
NAME							DATATYPE	DEFAULT		DESCRIPTION          
-------------------------------------------------------------------          
@SR_RFP_Sop_Details_Id			INT	

   
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------
  
	Begin Tran
		EXEC SR_RFP_Sop_Details_Del  819
	Rollback Tran
    
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR			PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR		    28-MAY-10	CREATED     

*/  

CREATE PROCEDURE dbo.SR_RFP_Sop_Details_Del
   (
    @SR_RFP_Sop_Details_Id INT
   )
AS
BEGIN

    SET NOCOUNT ON;

    DELETE
   	FROM
		dbo.SR_RFP_SOP_DETAILS
	WHERE
		SR_RFP_SOP_DETAILS_ID = @SR_RFP_Sop_Details_Id

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_Sop_Details_Del] TO [CBMSApplication]
GO
