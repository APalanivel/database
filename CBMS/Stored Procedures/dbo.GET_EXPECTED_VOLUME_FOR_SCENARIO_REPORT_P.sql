
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
		dbo.GET_EXPECTED_VOLUME_FOR_SCENARIO_REPORT_P  
  
DESCRIPTION:
		
 
INPUT PARAMETERS:
	Name				DataType		Default			Description    
---------------------------------------------------------------------   
	@userId				VARCHAR
    @sessionId			VARCHAR
    @fromDate			VARCHAR(12)
    @toDate				VARCHAR(12)
    @fromYear			VARCHAR(12)
    @toYear				VARCHAR(12)
    @clientId			INTEGER
    @divisionId			INTEGER
    @siteId				INTEGER
    @conversionId		INTEGER
    @Country_Name		VARCHAR(200)	NULL

OUTPUT PARAMETERS:    
	Name				DataType		Default		Description    
---------------------------------------------------------------------

USAGE EXAMPLES:
---------------------------------------------------------------------

	EXEC dbo.GET_EXPECTED_VOLUME_FOR_SCENARIO_REPORT_P 1,1,'7-1-2004','12-1-2004',2004,2004,228,0,0,25
	EXEC dbo.GET_EXPECTED_VOLUME_FOR_SCENARIO_REPORT_P 1,1,'7-1-2004','12-1-2004',2004,2004,228,0,0,25,'usa'
	
	EXEC dbo.GET_EXPECTED_VOLUME_FOR_SCENARIO_REPORT_P 1,1,'7-1-2009','12-1-2009',2009,2009,228,0,0,25
	EXEC dbo.GET_EXPECTED_VOLUME_FOR_SCENARIO_REPORT_P 1,1,'7-1-2009','12-1-2009',2009,2009,228,0,0,25,'usa'


AUTHOR INITIALS:
	Initials	Name 
---------------------------------------------------------------------
	RR			Raghu Reddy
  
MODIFICATIONS  
   
	Initials	Date		Modification    
------------------------------------------------------------    
	RR			2014-11-27  Added description header
							New optional input parameter @Country_Name added
	
******/
CREATE PROCEDURE [dbo].[GET_EXPECTED_VOLUME_FOR_SCENARIO_REPORT_P]
      @userId VARCHAR
     ,@sessionId VARCHAR
     ,@fromDate VARCHAR(12)
     ,@toDate VARCHAR(12)
     ,@fromYear VARCHAR(12)
     ,@toYear VARCHAR(12)
     ,@clientId INTEGER
     ,@divisionId INTEGER
     ,@siteId INTEGER
     ,@conversionId INTEGER
     ,@Country_Name VARCHAR(200) = NULL
AS 
SET NOCOUNT ON;

DECLARE @Tbl_FORECAST_DATE TABLE
      ( 
       FORECAST_AS_OF_DATE DATETIME )

INSERT      INTO @Tbl_FORECAST_DATE
            ( 
             FORECAST_AS_OF_DATE )
            SELECT
                  max(FORECAST_AS_OF_DATE)
            FROM
                  RM_FORECAST_VOLUME
            WHERE
                  FORECAST_YEAR = @fromYear
                  AND CLIENT_ID = @clientId
            UNION
            SELECT
                  max(FORECAST_AS_OF_DATE)
            FROM
                  RM_FORECAST_VOLUME
            WHERE
                  FORECAST_YEAR = @toYear
                  AND CLIENT_ID = @clientId

IF @clientId > 0
      AND @divisionId = 0
      AND @siteId = 0 
      SELECT
            A.TOTAL_VOLUME
           ,A.scenarioMonth
           ,A.scenarioYear
           ,onBoardClient.max_hedge_percent
      FROM
            ( SELECT
                  cast(sum(volumedetails.VOLUME * CONVERSION_FACTOR) AS DECIMAL(15, 3)) TOTAL_VOLUME
                 ,datepart(MONTH, volumedetails.MONTH_IDENTIFIER) scenarioMonth
                 ,datepart(YEAR, volumedetails.MONTH_IDENTIFIER) scenarioYear
              FROM
                  dbo.RM_ONBOARD_HEDGE_SETUP rmhs
                  RIGHT JOIN RM_FORECAST_VOLUME_DETAILS volumedetails
                        ON ( rmhs.SITE_ID = volumedetails.SITE_ID )
                 ,dbo.RM_FORECAST_VOLUME volume
                 ,dbo.CONSUMPTION_UNIT_CONVERSION cuc
              WHERE
                  EXISTS ( SELECT
                              1
                           FROM
                              dbo.RM_ONBOARD_HEDGE_SETUP hedge
                              INNER JOIN dbo.RM_ONBOARD_CLIENT onboard
                                    ON onboard.RM_ONBOARD_CLIENT_ID = hedge.RM_ONBOARD_CLIENT_ID
                              INNER JOIN core.Client_Hier ch
                                    ON ch.Site_Id = hedge.SITE_ID
                           WHERE
                              onboard.CLIENT_ID = @clientId
                              AND ( @Country_Name IS NULL
                                    OR ch.Country_Name = @Country_Name )
                              AND volumedetails.SITE_ID = hedge.SITE_ID )
                  AND rmhs.VOLUME_UNITS_TYPE_ID = cuc.BASE_UNIT_ID
                  AND cuc.CONVERTED_UNIT_ID = @conversionId
                  AND volumedetails.RM_FORECAST_VOLUME_ID = volume.RM_FORECAST_VOLUME_ID
                  AND EXISTS ( SELECT
                                    1
                               FROM
                                    @Tbl_FORECAST_DATE tfd
                               WHERE
                                    tfd.FORECAST_AS_OF_DATE = volume.FORECAST_AS_OF_DATE )
                  AND volumedetails.MONTH_IDENTIFIER BETWEEN convert(VARCHAR(12), @fromDate, 101)
                                                     AND     convert(VARCHAR(12), @toDate, 101)
              GROUP BY
                  MONTH_IDENTIFIER ) AS A
           ,RM_ONBOARD_CLIENT onBoardClient
      WHERE
            onBoardClient.client_id = @clientId


ELSE 
      IF @clientId > 0
            AND @divisionId > 0
            AND @siteId = 0 
            BEGIN

                  SELECT
                        A.TOTAL_VOLUME
                       ,A.scenarioMonth
                       ,A.scenarioYear
                       ,onBoardClient.max_hedge_percent
                  FROM
                        ( SELECT
                              cast(sum(volumedetails.VOLUME * CONVERSION_FACTOR) AS DECIMAL(15, 3)) TOTAL_VOLUME
                             ,datepart(MONTH, volumedetails.MONTH_IDENTIFIER) scenarioMonth
                             ,datepart(YEAR, volumedetails.MONTH_IDENTIFIER) scenarioYear
                          FROM
                              RM_ONBOARD_HEDGE_SETUP rmhs
                              RIGHT JOIN RM_FORECAST_VOLUME_DETAILS volumedetails
                                    ON ( rmhs.SITE_ID = volumedetails.SITE_ID )
                             ,RM_FORECAST_VOLUME volume
                             ,CONSUMPTION_UNIT_CONVERSION cuc
                          WHERE
                              EXISTS ( SELECT
                                          1
                                       FROM
                                          dbo.RM_ONBOARD_HEDGE_SETUP hedge
                                          INNER JOIN dbo.RM_ONBOARD_CLIENT onboard
                                                ON onboard.RM_ONBOARD_CLIENT_ID = hedge.RM_ONBOARD_CLIENT_ID
                                          INNER JOIN core.Client_Hier ch
                                                ON ch.Site_Id = hedge.SITE_ID
                                       WHERE
                                          onboard.CLIENT_ID = @clientId
                                          AND hedge.DIVISION_ID = @divisionId
                                          AND ( @Country_Name IS NULL
                                                OR ch.Country_Name = @Country_Name ) )
                              AND rmhs.VOLUME_UNITS_TYPE_ID = cuc.BASE_UNIT_ID
                              AND cuc.CONVERTED_UNIT_ID = @conversionId
                              AND volumedetails.RM_FORECAST_VOLUME_ID = volume.RM_FORECAST_VOLUME_ID
                              AND EXISTS ( SELECT
                                                1
                                           FROM
                                                @Tbl_FORECAST_DATE tfd
                                           WHERE
                                                tfd.FORECAST_AS_OF_DATE = volume.FORECAST_AS_OF_DATE )
                              AND volumedetails.MONTH_IDENTIFIER BETWEEN convert(VARCHAR(12), @fromDate, 101)
                                                                 AND     convert(VARCHAR(12), @toDate, 101)
                          GROUP BY
                              MONTH_IDENTIFIER ) AS A
                       ,RM_ONBOARD_CLIENT onBoardClient
                  WHERE
                        onBoardClient.client_id = @clientId

            END



      ELSE 
            IF @clientId > 0
                  AND @divisionId = 0
                  AND @siteId > 0 
                  BEGIN

                        SELECT
                              A.TOTAL_VOLUME
                             ,A.scenarioMonth
                             ,A.scenarioYear
                             ,onBoardClient.max_hedge_percent
                        FROM
                              ( SELECT
                                    cast(sum(volumedetails.VOLUME * CONVERSION_FACTOR) AS DECIMAL(15, 3)) TOTAL_VOLUME
                                   ,datepart(MONTH, volumedetails.MONTH_IDENTIFIER) scenarioMonth
                                   ,datepart(YEAR, volumedetails.MONTH_IDENTIFIER) scenarioYear
                                FROM
                                    RM_ONBOARD_HEDGE_SETUP rmhs
                                    RIGHT JOIN RM_FORECAST_VOLUME_DETAILS volumedetails
                                          ON ( rmhs.SITE_ID = volumedetails.SITE_ID )
                                   ,RM_FORECAST_VOLUME volume
                                   ,CONSUMPTION_UNIT_CONVERSION cuc
                                WHERE
                                    volumedetails.SITE_ID = @siteId
                                    AND rmhs.VOLUME_UNITS_TYPE_ID = cuc.BASE_UNIT_ID
                                    AND cuc.CONVERTED_UNIT_ID = @conversionId
                                    AND volumedetails.RM_FORECAST_VOLUME_ID = volume.RM_FORECAST_VOLUME_ID
                                    AND EXISTS ( SELECT
                                                      1
                                                 FROM
                                                      @Tbl_FORECAST_DATE tfd
                                                 WHERE
                                                      tfd.FORECAST_AS_OF_DATE = volume.FORECAST_AS_OF_DATE )
                                    AND volumedetails.MONTH_IDENTIFIER BETWEEN convert(VARCHAR(12), @fromDate, 101)
                                                                       AND     convert(VARCHAR(12), @toDate, 101)
                                GROUP BY
                                    MONTH_IDENTIFIER ) AS A
                             ,RM_ONBOARD_CLIENT onBoardClient
                        WHERE
                              onBoardClient.client_id = @clientId

                  END


            ELSE 
                  IF @clientId > 0
                        AND @divisionId > 0
                        AND @siteId > 0 
                        BEGIN

                              SELECT
                                    A.TOTAL_VOLUME
                                   ,A.scenarioMonth
                                   ,A.scenarioYear
                                   ,onBoardClient.max_hedge_percent
                              FROM
                                    ( SELECT
                                          cast(sum(volumedetails.VOLUME * CONVERSION_FACTOR) AS DECIMAL(15, 3)) TOTAL_VOLUME
                                         ,datepart(MONTH, volumedetails.MONTH_IDENTIFIER) scenarioMonth
                                         ,datepart(YEAR, volumedetails.MONTH_IDENTIFIER) scenarioYear
                                      FROM
                                          RM_ONBOARD_HEDGE_SETUP rmhs
                                          RIGHT JOIN RM_FORECAST_VOLUME_DETAILS volumedetails
                                                ON ( rmhs.SITE_ID = volumedetails.SITE_ID )
                                         ,RM_FORECAST_VOLUME volume
                                         ,CONSUMPTION_UNIT_CONVERSION cuc
                                      WHERE
                                          volumedetails.SITE_ID = @siteId
                                          AND rmhs.VOLUME_UNITS_TYPE_ID = cuc.BASE_UNIT_ID
                                          AND cuc.CONVERTED_UNIT_ID = @conversionId
                                          AND volumedetails.RM_FORECAST_VOLUME_ID = volume.RM_FORECAST_VOLUME_ID
                                          AND EXISTS ( SELECT
                                                            1
                                                       FROM
                                                            @Tbl_FORECAST_DATE tfd
                                                       WHERE
                                                            tfd.FORECAST_AS_OF_DATE = volume.FORECAST_AS_OF_DATE )
                                          AND volumedetails.MONTH_IDENTIFIER BETWEEN convert(VARCHAR(12), @fromDate, 101)
                                                                             AND     convert(VARCHAR(12), @toDate, 101)
                                      GROUP BY
                                          MONTH_IDENTIFIER ) AS A
                                   ,RM_ONBOARD_CLIENT onBoardClient
                              WHERE
                                    onBoardClient.client_id = @clientId
	
                        END

;
GO

GRANT EXECUTE ON  [dbo].[GET_EXPECTED_VOLUME_FOR_SCENARIO_REPORT_P] TO [CBMSApplication]
GO
