SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsCuExceptionType_Save

DESCRIPTION:

INPUT PARAMETERS:
	Name							DataType		Default					Description
---------------------------------------------------------------------------------------------
	@cu_exception_type_id			INT
    @exception_type					VARCHAR(200)
    @exception_group_type_id		INT
    @managed_by_group_info_id		INT
    @stop_processing				BIT
    @is_routed_reason				BIT
    @sort_order						INT = 1
    @Individual_User_Info_Id		INT

OUTPUT PARAMETERS:
	Name							DataType		Default					Description
---------------------------------------------------------------------------------------------
	
USAGE EXAMPLES:
---------------------------------------------------------------------------------------------



BEGIN TRANSACTION
EXEC dbo.cbmsCuExceptionType_Save
    @cu_exception_type_id = ''
    , @exception_type = ''
    , @exception_group_type_id = ''
    , @managed_by_group_info_id = ''
    , @stop_processing = ''
    , @is_routed_reason = ''
    , @sort_order = 1
    , @Individual_User_Info_Id = 49

ROLLBACK TRANSACTION


AUTHOR INITIALS:
	Initials	Name
---------------------------------------------------------------------------------------------
	NR			Narayana Reddy
	
MODIFICATIONS
	Initials	Date			Modification
---------------------------------------------------------------------------------------------
	NR       	2018-10-04		D20-17 - Added Header And Passed new paramter Individual User info id to save the
								User_info_id column when we pass group name as "Individual User".

******/


CREATE PROCEDURE [dbo].[cbmsCuExceptionType_Save]
    (
        @cu_exception_type_id INT
        , @exception_type VARCHAR(200)
        , @exception_group_type_id INT
        , @managed_by_group_info_id INT
        , @stop_processing BIT
        , @is_routed_reason BIT
        , @sort_order INT = 1
        , @Managed_By_User_Info_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;


        DECLARE @Is_Individual_User_Group_Info_Id BIT = 0;

        SELECT
            @Is_Individual_User_Group_Info_Id = 1
        FROM
            dbo.GROUP_INFO gi
        WHERE
            gi.GROUP_INFO_ID = @managed_by_group_info_id
            AND gi.GROUP_NAME = 'Individual User';




        DECLARE @this_id INT;
        SET @this_id = @cu_exception_type_id;

        IF @this_id IS NULL
            BEGIN

                INSERT INTO dbo.CU_EXCEPTION_TYPE
                     (
                         EXCEPTION_TYPE
                         , EXCEPTION_GROUP_TYPE_ID
                         , MANAGED_BY_GROUP_INFO_ID
                         , STOP_PROCESSING
                         , IS_ROUTED_REASON
                         , SORT_ORDER
                     )
                VALUES
                    (@exception_type
                     , @exception_group_type_id
                     , @managed_by_group_info_id
                     , @stop_processing
                     , @is_routed_reason
                     , @sort_order);
            END;

        ELSE
            BEGIN

                UPDATE
                    dbo.CU_EXCEPTION_TYPE
                SET
                    EXCEPTION_TYPE = @exception_type
                    , EXCEPTION_GROUP_TYPE_ID = @exception_group_type_id
                    , MANAGED_BY_GROUP_INFO_ID = @managed_by_group_info_id
                    , STOP_PROCESSING = @stop_processing
                    , IS_ROUTED_REASON = @is_routed_reason
                    , SORT_ORDER = @sort_order
                    , Managed_By_User_Info_Id = CASE WHEN @Is_Individual_User_Group_Info_Id = 1 THEN
                                                         @Managed_By_User_Info_Id
                                                    ELSE NULL
                                                END
                WHERE
                    CU_EXCEPTION_TYPE_ID = @this_id;

            END;
    END;







GO
GRANT EXECUTE ON  [dbo].[cbmsCuExceptionType_Save] TO [CBMSApplication]
GO
