SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Supplier_Account_Meter_Map_Del]  

DESCRIPTION: It Deletes Supplier Account Meter Map for Selected Account Id and Meter Id 
			 which are associated to contract.     
      
INPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Account_Id		INT
	@Meter_Id		INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
	EXEC Supplier_Account_Meter_Map_Del 4637,168
  Rollback Tran

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			17-JUN-10	CREATED

*/

CREATE PROCEDURE dbo.Supplier_Account_Meter_Map_Del
    (
       @Account_Id INT
      ,@Meter_Id   INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.SUPPLIER_ACCOUNT_METER_MAP
	WHERE
		Account_Id = @Account_Id
		AND Meter_Id = @Meter_Id

END
GO
GRANT EXECUTE ON  [dbo].[Supplier_Account_Meter_Map_Del] TO [CBMSApplication]
GO
