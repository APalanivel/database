SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:      
    [dbo].[Report_DE_CommercialBudget_by_Commodity_WithDemandAndTaxes]   
     
 DESCRIPTION:   
 
 INPUT PARAMETERS:      
Name				DataType						Default				Description      
-------------------------------------------------------------------------------------      
@Client_ID			INT          
@Commodity_ID		INT           
@Begin_Date			DATETIME          
@End_Date			DATETIME      

OUTPUT PARAMETERS:      
Name			DataType						Default					Description      
-------------------------------------------------------------------------------------      


 USAGE EXAMPLES:      
------------------------------------------------------------------------------------- 
     
 EXEC dbo.[Report_DE_CommercialBudget_by_Commodity_WithDemandAndTaxes] 11261,290,'01/01/2013','12/31/2015' 
             
 EXEC dbo.[Report_DE_CommercialBudget_by_Commodity_WithDemandAndTaxes] 11467,291,'06/01/2009','04/04/2010' 
              
   
AUTHOR INITIALS:      
Initials		Name      
------------------------------------------------------------------------------------- 
LEC				Lynn Cox   
    
MODIFICATIONS       
Initials	Date			Modification      
------------------------------------------------------------------------------------- 
LEC			2016-05-03		Modified to include Add site level EP demand by month, in KW And Taxes
******/      
CREATE PROCEDURE [dbo].[Report_DE_CommercialBudget_by_Commodity_WithDemandAndTaxes]
      ( 
       @Client_ID INT
      ,@Commodity_ID INT
      ,@Begin_Date DATETIME
      ,@End_Date DATETIME )
AS 
BEGIN                  
      SET NOCOUNT ON;     
          
      DECLARE @Currency_Gr_id INT                       
      DECLARE
            @Entity_unit INT
           ,@Currency_Unit_Id INT
           ,@entity_unit_demand INT      
           
      DECLARE @Currency_Group_Id INT    
      
      DECLARE
            @Report_Client_Id INT
           ,@Report_Commodity_Id INT
           ,@Begin_Dt DATETIME
           ,@End_Dt DATETIME
      
                 
               
      CREATE TABLE #Category_site
            ( 
             Client_Name VARCHAR(200)
            ,Sitegroup_Name VARCHAR(200)
            ,State_Name VARCHAR(200)
            ,Site_Name VARCHAR(200)
            ,Site_id INT
            ,Client_Id INT
            ,SiteRef VARCHAR(30)
            ,Service_Month DATE
            ,PRIMARY KEY CLUSTERED ( Client_Id, Site_Id, Service_Month ) )            
                            
      CREATE TABLE #CU_Data
            ( 
             SERVICE_MONTH DATETIME
            ,Site_Id INT
            ,Total_Cost DECIMAL(32, 16)
            ,Total_Usage DECIMAL(32, 16)
            ,Utility_Cost DECIMAL(32, 16)
            ,Marketer_Cost DECIMAL(32, 16)
            ,Demand DECIMAL(32, 16)
            ,Total_Taxes DECIMAL(32, 16) )      
            
            
      SET @Report_Client_Id = @Client_ID    
      SET @Report_Commodity_Id = @Commodity_ID   
      SET @Begin_Dt = @Begin_Date   
      SET @End_Dt = @End_Date          
                               
      SELECT
            @Currency_Unit_Id = cu.Currency_Unit_ID
      FROM
            dbo.Currency_Unit cu
      WHERE
            cu.Currency_Unit_Name = 'USD'              
                    
                      
      SELECT
            @Currency_Gr_id = a.CURRENCY_GROUP_ID
      FROM
            dbo.CLIENT a
      WHERE
            a.CLIENT_ID = @Report_Client_Id                      
                      
      SELECT
            @Entity_unit = Default_UOM_Entity_Type_Id
      FROM
            Commodity com
      WHERE
            Com.Commodity_Id = @Report_Commodity_Id                    
                      
      SELECT
            @Entity_unit_Demand = bm.Default_Uom_Type_Id
      FROM
            Bucket_Master bm
            JOIN Code cd
                  ON cd.Code_Id = bm.Bucket_Type_Cd
      WHERE
            bm.Commodity_Id = @Report_Commodity_Id
            AND bm.Bucket_Name = 'demand'
            AND cd.Code_Value = 'determinant'
            AND bm.Is_Category = 1
                      
                        
      INSERT      INTO #Category_site
                  SELECT
                        ch.Client_Name
                       ,ch.Sitegroup_Name
                       ,ch.State_Name
                       ,ch.Site_Name
                       ,ch.Site_ID
                       ,ch.Client_Id
                       ,s.SITE_REFERENCE_NUMBER
                       ,dd.DATE_D
                  FROM
                        Core.Client_Hier ch
                        JOIN dbo.SITE s
                              ON s.Client_ID = ch.Client_Id
                                 AND s.SITE_ID = ch.Site_Id
                        CROSS JOIN meta.DATE_DIM dd
                  WHERE
                        ch.Client_Id = @Report_Client_Id
                        AND ch.Site_Id <> 0
                        AND ch.Site_Not_Managed = 0
                        AND ch.Site_Closed = 0
                        AND dd.DATE_D BETWEEN @Begin_Dt AND @End_Dt;      
                                          
      WITH  Cte_ipdata
              AS ( SELECT
                        SITE_ID
                       ,SERVICE_MONTH
                   FROM
                        ( SELECT
                              SUM(CONVERT(INT, is_expected)) Expected
                             ,SUM(CONVERT(INT, is_received)) Received
                             ,ips.service_month
                             ,ips.site_id
                          FROM
                              dbo.INVOICE_PARTICIPATION ips
                              JOIN dbo.SITE s
                                    ON s.site_id = ips.site_id
                              JOIN ( SELECT
                                          cha.Account_Id
                                         ,ch.Site_Id
                                     FROM
                                          core.Client_Hier_Account cha
                                          JOIN Core.Client_Hier ch
                                                ON cha.Client_Hier_Id = ch.Client_Hier_Id
                                     WHERE
                                          ( cha.Account_Type = 'Utility'
                                            OR ( ( cha.Supplier_Meter_Disassociation_Date > cha.Supplier_Account_begin_Dt
                                                   OR cha.Supplier_Meter_Disassociation_Date IS NULL )
                                                 AND cha.Account_Type = 'Supplier' ) )
                                          AND ch.Client_Id = @Report_Client_Id
                                          AND cha.Commodity_Id = @Report_Commodity_Id
                                     GROUP BY
                                          cha.Account_Id
                                         ,ch.Site_Id ) AS vm
                                    ON vm.account_id = ips.account_id
                                       AND ips.SITE_ID = vm.SITE_ID
                          WHERE
                              s.client_id = @Report_Client_Id
                              AND ips.SERVICE_MONTH BETWEEN @Begin_Dt AND @End_Dt
                          GROUP BY
                              ips.site_id
                             ,ips.service_month ) sumdata
                   WHERE
                        sumdata.Expected = sumdata.Received)
            INSERT      INTO #CU_Data
                        ( 
                         SERVICE_MONTH
                        ,Site_Id
                        ,Total_Usage
                        ,Utility_Cost
                        ,Marketer_Cost
                        ,Total_Cost
                        ,Total_Taxes
                        ,Demand )
                        SELECT
                              cus.SERVICE_MONTH
                             ,s.Site_Id
                             ,MAX(CASE WHEN bm.Bucket_Name = 'Total Usage' THEN ( cus.Bucket_Value * ConsUC.Conversion_Factor )
                                  END) Total_Usage
                             ,MAX(CASE WHEN bm.Bucket_Name = 'Utility Cost' THEN ( cus.Bucket_Value * CUC.Conversion_Factor )
                                  END) Utility_Cost
                             ,MAX(CASE WHEN bm.Bucket_Name = 'Marketer Cost' THEN ( cus.Bucket_Value * CUC.Conversion_Factor )
                                  END) Marketer_Cost
                             ,MAX(CASE WHEN bm.Bucket_Name = 'Total Cost' THEN ( cus.Bucket_Value * CUC.Conversion_Factor )
                                  END) Total_Cost
                             ,MAX(CASE WHEN bm.Bucket_Name = 'Taxes' THEN ( cus.Bucket_Value * CUC.Conversion_Factor )
                                  END) Total_Taxes
                             ,MAX(CASE WHEN bm.Bucket_Name = 'Demand' THEN ( cus.Bucket_Value * ConsUCDemand.Conversion_Factor )
                                  END) Demand
                        FROM
                              dbo.Bucket_Master bm
                              LEFT JOIN dbo.Cost_Usage_Site_Dtl cus
                                    ON bm.Bucket_Master_Id = cus.Bucket_Master_Id
                              JOIN Core.Client_Hier s
                                    ON cus.Client_Hier_Id = s.Client_Hier_Id
                              JOIN Cte_ipdata cte_ip
                                    ON cte_ip.SITE_ID = s.Site_Id
                                       AND cte_ip.SERVICE_MONTH = cus.SERVICE_MONTH
                              LEFT JOIN Currency_Unit_Conversion CUC
                                    ON CUC.Base_Unit_ID = CUS.Currency_Unit_ID
                                       AND CUC.Converted_Unit_ID = @Currency_Unit_Id
                                       AND CUC.Currency_Group_ID = @Currency_Gr_id
                                       AND cuc.Conversion_Date = CUS.Service_Month
                              LEFT JOIN Consumption_Unit_Conversion ConsUC
                                    ON ConsUC.Base_Unit_ID = CUS.UOM_Type_Id
                                       AND ConsUC.Converted_Unit_ID = @Entity_unit
                              LEFT JOIN Consumption_Unit_Conversion ConsUCDemand
                                    ON ConsUCDemand.Base_Unit_ID = CUS.UOM_Type_Id
                                       AND ConsUCDemand.Converted_Unit_ID = @Entity_unit_Demand
                        WHERE
                              s.Client_Id = @Report_Client_Id
                              AND bm.Commodity_Id = @Report_Commodity_Id
                              AND cus.SERVICE_MONTH BETWEEN @Begin_Dt AND @End_Dt
                        GROUP BY
                              cus.SERVICE_MONTH
                             ,s.Site_Id;              
      SELECT
            tbl_cts.Client_Name [Client Name]
           ,tbl_cts.Sitegroup_Name [Division Name]
           ,tbl_cts.State_Name [State]
           ,tbl_cts.Site_Name [Site Name]
           ,tbl_cts.Site_Id [Site Id]
           ,tbl_cts.SiteRef
           ,ven.Vendor_Name [Primary Utility]
           ,Cusdata.Total_Usage Usage
           ,Cusdata.Utility_Cost
           ,Cusdata.Marketer_Cost Supplier_Cost
           ,Cusdata.Total_Cost
           ,Cusdata.Total_Cost / NULLIF(Cusdata.Total_Usage, 0) Unit_Cost
           ,tbl_cts.service_month Service_Month
           ,Cusdata.Demand
           ,Cusdata.total_taxes
           ,CASE WHEN ven.Contract_Expiry > CONVERT(DATE, GETDATE()) THEN 'Transport'
                 ELSE 'Tariff'
            END Contract_Near_expiry
      FROM
            #Category_site tbl_cts
            LEFT JOIN ( SELECT
                              MIN(cha.Account_Vendor_Name) Vendor_Name
                             ,ch.Site_Id
                             ,MAX(CONTRACT_END_DATE) Contract_Expiry
                        FROM
                              core.Client_Hier_Account cha
                              JOIN core.Client_Hier ch
                                    ON cha.Client_Hier_Id = ch.Client_Hier_Id
                              JOIN core.Client_Hier_Account chas
                                    ON chas.Meter_Id = cha.Meter_Id
                              LEFT JOIN dbo.CONTRACT con
                                    ON con.CONTRACT_ID = chas.Supplier_Contract_ID
                        WHERE
                              cha.Account_Type = 'Utility'
                              AND cha.Commodity_Id = @Report_Commodity_Id
                              AND ch.Client_Id = @Report_Client_Id
                        GROUP BY
                              ch.Site_ID ) ven
                  ON tbl_cts.Site_Id = ven.SITE_ID
            LEFT JOIN #CU_Data Cusdata
                  ON tbl_cts.Site_id = Cusdata.SITE_ID
                     AND tbl_cts.service_month = Cusdata.service_month
      GROUP BY
            tbl_cts.Client_Name
           ,tbl_cts.Sitegroup_Name
           ,tbl_cts.State_Name
           ,tbl_cts.Site_Name
           ,tbl_cts.Site_Id
           ,ven.Vendor_Name
           ,tbl_cts.SiteRef
           ,tbl_cts.service_month
           ,ven.Contract_Expiry
           ,Cusdata.Total_Usage
           ,Cusdata.Utility_Cost
           ,Cusdata.Marketer_Cost
           ,Cusdata.Total_Cost
           ,Cusdata.Demand
           ,Cusdata.Total_Taxes
      ORDER BY
            tbl_cts.service_month
           ,tbl_cts.Site_Name      
                             
      DROP TABLE #Category_site            
      DROP TABLE #CU_Data            
          
    
    
END    

;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_CommercialBudget_by_Commodity_WithDemandAndTaxes] TO [CBMS_SSRS_Reports]
GO
