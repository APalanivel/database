SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/******************************************************************************************************    
NAME :	dbo.Get_Corporate_Role  
   
DESCRIPTION: This procedure used to Get Security_Role_Id of role for the client where IS_CORPORATE = TRUE 
   
 INPUT PARAMETERS:    
 Name             DataType               Default        Description    
--------------------------------------------------------------------      
 @ClientId        INT                                   ClientID
   
 OUTPUT PARAMETERS:    
 Name              DataType  Default Description    
-------------------------------------------------------------------- 
@Security_Role_Id   INT
   
  USAGE EXAMPLES:    
--------------------------------------------------------------------    
   
  
AUTHOR INITIALS:    
 Initials Name    
-------------------------------------------------------------------    
 KVK K Vinay Kumar
   
 MODIFICATIONS     
 Initials Date  Modification    
--------------------------------------------------------------------   
******************************************************************************************************/


CREATE PROCEDURE [dbo].[Get_Corporate_Role]
( 
		@ClientId				INT
 )
AS
BEGIN

	SET NOCOUNT ON
	
	BEGIN TRY
	
		
        
			SELECT Security_Role_Id  
			  FROM Security_Role  
			 WHERE client_id	= @clientid 
			   AND Is_Corporate = 1
	
		
	END TRY
	
	BEGIN CATCH
	
	
		EXEC usp_RethrowError
	
	END CATCH

END

GO
GRANT EXECUTE ON  [dbo].[Get_Corporate_Role] TO [CBMSApplication]
GO
