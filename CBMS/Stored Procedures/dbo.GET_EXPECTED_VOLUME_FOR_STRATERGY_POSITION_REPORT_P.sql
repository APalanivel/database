SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.GET_EXPECTED_VOLUME_FOR_STRATERGY_POSITION_REPORT_P 
@userId varchar,
@sessionId varchar,
@fromDate Varchar(12),
@toDate Varchar(12),
@fromYear Varchar(12),
@toYear Varchar(12),
@clientId int,
@divisionOrSiteId int,
@levelStatus int,
@unitId int


as
	set nocount on
if @levelStatus=1 

	select 	CAST( SUM(volumedetails.VOLUME * consumption.CONVERSION_FACTOR) as decimal(15,3)) TOTAL_VOLUME,
		CONVERT (Varchar(12), volumedetails.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER

	
	from	RM_FORECAST_VOLUME_DETAILS volumedetails,
		RM_FORECAST_VOLUME volume,
		CONSUMPTION_UNIT_CONVERSION consumption,
		RM_ONBOARD_HEDGE_SETUP onboard 


	
	where	volumedetails.SITE_ID IN( select DISTINCT site.SITE_ID
	
					  from	SITE site,
						DIVISION division,
						RM_ONBOARD_HEDGE_SETUP hedge
	
			  		  where	division.CLIENT_ID =@clientId AND
						division.DIVISION_ID=site.DIVISION_ID AND
						site.SITE_ID=hedge.SITE_ID AND
						hedge.INCLUDE_IN_REPORTS=1 AND	
						hedge.HEDGE_TYPE_ID IN(	Select ENTITY_ID
									from   ENTITY
									where  ENTITY_TYPE=273 AND
									       ENTITY_NAME like 'financial'	
								      ) 
					) AND
			volumedetails.RM_FORECAST_VOLUME_ID=volume.RM_FORECAST_VOLUME_ID AND		
			volume.FORECAST_AS_OF_DATE IN(select MAX(FORECAST_AS_OF_DATE) 
							    from  RM_FORECAST_VOLUME
							    where FORECAST_YEAR =@fromYear AND
								  CLIENT_ID=@clientId
						      UNION
		
						      select MAX(FORECAST_AS_OF_DATE) 
							    from  RM_FORECAST_VOLUME
							    where FORECAST_YEAR =@toYear AND
								  CLIENT_ID=@clientId
						    )	AND
		
			volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @fromDate, 101) AND CONVERT(Varchar(12), @toDate, 101) AND

			volumedetails.SITE_ID=onboard.SITE_ID AND
			onboard.VOLUME_UNITS_TYPE_ID=consumption.BASE_UNIT_ID AND
			consumption.CONVERTED_UNIT_ID=@unitId

		
	GROUP BY MONTH_IDENTIFIER


else if @levelStatus=2 

	select 	CAST( SUM(volumedetails.VOLUME * consumption.CONVERSION_FACTOR) as decimal(15,3)) TOTAL_VOLUME,
		CONVERT (Varchar(12), volumedetails.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER
	
	from	RM_FORECAST_VOLUME_DETAILS volumedetails,
		RM_FORECAST_VOLUME volume,
		CONSUMPTION_UNIT_CONVERSION consumption,
		RM_ONBOARD_HEDGE_SETUP onboard 
 					

	where	volumedetails.SITE_ID IN( select DISTINCT site.SITE_ID
	
					  from	SITE site,
						DIVISION division,
						RM_ONBOARD_HEDGE_SETUP hedge
	
			  		  where	division.DIVISION_ID =@divisionOrSiteId AND
						division.DIVISION_ID=site.DIVISION_ID AND
						site.SITE_ID=hedge.SITE_ID AND
					hedge.INCLUDE_IN_REPORTS=1 AND	
					hedge.HEDGE_TYPE_ID IN(	Select ENTITY_ID
								from   ENTITY
								where  ENTITY_TYPE=273 AND
								       ENTITY_NAME like 'financial'	
							      ) 

					) AND		
	
		volumedetails.RM_FORECAST_VOLUME_ID=volume.RM_FORECAST_VOLUME_ID AND		
		volume.FORECAST_AS_OF_DATE IN(select MAX(FORECAST_AS_OF_DATE) 
						    from  RM_FORECAST_VOLUME
						    where FORECAST_YEAR =@fromYear AND
							  CLIENT_ID=@clientId
					      UNION
	
					      select MAX(FORECAST_AS_OF_DATE) 
						    from  RM_FORECAST_VOLUME
						    where FORECAST_YEAR =@toYear AND
							  CLIENT_ID=@clientId
					    )	AND
	
		volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @fromDate, 101) AND CONVERT(Varchar(12), @toDate, 101) AND

		volumedetails.SITE_ID=onboard.SITE_ID AND
		onboard.VOLUME_UNITS_TYPE_ID=consumption.BASE_UNIT_ID AND
		consumption.CONVERTED_UNIT_ID=@unitId

	
	GROUP BY MONTH_IDENTIFIER


else if @levelStatus=3 

	select 	CAST( SUM(volumedetails.VOLUME * consumption.CONVERSION_FACTOR) as decimal(15,3)) TOTAL_VOLUME,
		CONVERT (Varchar(12), volumedetails.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER
	
	from	RM_FORECAST_VOLUME_DETAILS volumedetails,
		RM_FORECAST_VOLUME volume,
		CONSUMPTION_UNIT_CONVERSION consumption,
		RM_ONBOARD_HEDGE_SETUP onboard 

 					
	
	where	volumedetails.SITE_ID IN( select DISTINCT hedge.SITE_ID
	
					  from	RM_ONBOARD_HEDGE_SETUP hedge
	
			  		  where	hedge.SITE_ID =@divisionOrSiteId AND
						hedge.INCLUDE_IN_REPORTS=1 AND	
						hedge.HEDGE_TYPE_ID IN(	Select ENTITY_ID
									from   ENTITY
									where  ENTITY_TYPE=273 AND
									       ENTITY_NAME like 'financial'	
								      ) 
					) AND		

		volumedetails.RM_FORECAST_VOLUME_ID=volume.RM_FORECAST_VOLUME_ID AND		
		volume.FORECAST_AS_OF_DATE IN(select MAX(FORECAST_AS_OF_DATE) 
						    from  RM_FORECAST_VOLUME
						    where FORECAST_YEAR =@fromYear AND
							  CLIENT_ID=@clientId
					      UNION
	
					      select MAX(FORECAST_AS_OF_DATE) 
						    from  RM_FORECAST_VOLUME
						    where FORECAST_YEAR =@toYear AND
							  CLIENT_ID=@clientId
					    )	AND
	
		volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @fromDate, 101) AND CONVERT(Varchar(12), @toDate, 101) AND

		volumedetails.SITE_ID=onboard.SITE_ID AND
		onboard.VOLUME_UNITS_TYPE_ID=consumption.BASE_UNIT_ID AND
		consumption.CONVERTED_UNIT_ID=@unitId

	
	GROUP BY MONTH_IDENTIFIER
GO
GRANT EXECUTE ON  [dbo].[GET_EXPECTED_VOLUME_FOR_STRATERGY_POSITION_REPORT_P] TO [CBMSApplication]
GO
