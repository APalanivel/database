
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME:
		dbo.UBM_INSERT_AVISTA_EFFECTIVE_INVOICE_DATA_P
		
DESCRIPTION:
			To load the data from Ubm stage-1 tables to Stage-2 tables.
      
INPUT PARAMETERS:          
	NAME						DATATYPE		DEFAULT		DESCRIPTION          
---------------------------------------------------------------------          
    @missed_image				VARCHAR(8000)	OUTPUT
    @client_id					INT
    @consolidation_id			VARCHAR(50)
    @ubm_batch_master_log_id	INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION
---------------------------------------------------------------------

USAGE EXAMPLES:
---------------------------------------------------------------------

BEGIN TRAN

		DECLARE @Images VARCHAR(8000)

		EXEC dbo.UBM_INSERT_AVISTA_EFFECTIVE_INVOICE_DATA_P 		
		  @Client_id = 1104
		 ,@Consolidation_Id = '955461'
		 ,@ubm_Batch_Master_Log_id = 13547
		 ,@Missed_Image = @Images OUT

ROLLBACK TRAN
		 


AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	HG			Harihara Suthan G

MODIFICATIONS
	INITIALS	DATE		MODIFICATION
-----------------------------------------------------------
	HG			2013-07-02	Comments header added
							MAINT-1994	- Added Vendor_Meter_Number to the group by clause as It is not showing up twice on the template due to meter # not being a qualification and all other fields are identical.
	HG			2014-11-04	MAINT-3190	- Modified the code to assign CAN(Canadian Dolloar) for the invoice if Ecova send the strings as either "Canadian Dollar" Or "CAD"
										- Also added code to pick the Demand_Uom(instead of raw_uom) if demand_uom is kw, Kvar and kVa
	HG			2017-06-06	PRSUPPORT-760 - Modified the process to look in to the new allowed list to filter out the unwanted client's invoices created in CBMS.
*/

CREATE PROCEDURE [dbo].[UBM_INSERT_AVISTA_EFFECTIVE_INVOICE_DATA_P]
      (
       @missed_image VARCHAR(8000) OUTPUT
      ,@client_id INT
      ,@consolidation_id VARCHAR(50)
      ,@ubm_batch_master_log_id INT )
AS
BEGIN

      SET NOCOUNT ON

	-- Same as Cass, this sproc does two things
	--	1) Load raw avista data into three effective tables
	--	2) Look for image in cbms_image table based on bill id and update rows in ubm_invoice table

      DECLARE
            @bill_id VARCHAR(50)
           ,@cbms_image_id INT
           ,@Ubm_Id INT
           
      SELECT
            @Ubm_Id = UBM_ID
      FROM
            dbo.UBM_BATCH_MASTER_LOG
      WHERE
            UBM_BATCH_MASTER_LOG_ID = @ubm_batch_master_log_id
			
      SET @missed_image = ''

      CREATE TABLE #Allowed_Client_Site_Service
            (
             Customer_Id VARCHAR(30)
            ,Service_Group VARCHAR(30)
            ,Reference_Site_Number VARCHAR(50)
            ,PRIMARY KEY CLUSTERED ( Customer_Id, Service_Group, Reference_Site_Number ) )

      INSERT      INTO #Allowed_Client_Site_Service
                  (Customer_Id
                  ,Service_Group
                  ,Reference_Site_Number )
                  SELECT
                        ucm.UBM_CLIENT_CODE
                       ,usm.UBM_SERVICE_CODE
                       ,acs.Reference_Site_Number
                  FROM
                        dbo.Ubm_Avista_Allowed_Client_Site acs
                        INNER JOIN dbo.UBM_CLIENT_MAP ucm
                              ON ucm.CLIENT_ID = acs.Client_Id
                        INNER JOIN dbo.UBM_SERVICE_MAP usm
                              ON usm.COMMODITY_TYPE_ID = acs.Commodity_Id
                                 AND usm.UBM_ID = ucm.UBM_ID
                  WHERE
                        usm.UBM_ID = @Ubm_Id
                  GROUP BY
                        ucm.UBM_CLIENT_CODE
                       ,usm.UBM_SERVICE_CODE
                       ,acs.Reference_Site_Number


	-- 1) Insert data to effective
      INSERT      INTO dbo.UBM_INVOICE
                  ( UBM_BATCH_MASTER_LOG_ID
                  ,INVOICE_IDENTIFIER
                  ,CBMS_IMAGE_ID
                  ,UBM_ACCOUNT_ID
                  ,UBM_ACCOUNT_NUMBER
                  ,UBM_VENDOR_ID
                  ,UBM_VENDOR_NAME
                  ,UBM_CLIENT_NAME
                  ,BILL_ADDRESS_LINE1
                  ,BILL_ADDRESS_LINE2
                  ,BILL_ADDRESS_LINE3
                  ,AMOUNT_DUE
                  ,PREVIOUS_BALANCE
                  ,PAYMENT_DUE_DATE
                  ,CURRENT_CHARGES
                  ,CITY
                  ,STATE
                  ,ZIP_CODE
                  ,VENDOR_BILLING_DATE
                  ,IS_PROCESSED
                  ,IS_QUARTERLY
                  ,UBM_CLIENT_ID
                  ,CURRENCY )
                  SELECT
                        UBM_BATCH_MASTER_LOG_ID
                       ,BILL_ID
                       ,NULL cbms_image_id
                       ,ACCOUNT_ID
                       ,ACCOUNT_NUMBER
                       ,VENDOR_ID
                       ,VENDOR_NAME
                       ,CUSTOMER_NAME
                       ,ADDRESS_LINE1
                       ,ADDRESS_LINE2
                       ,ADDRESS_LINE3
                       ,AMOUNT_DUE
                       ,PREVIOUS_BALANCE
                       ,DUE_DATE
                       ,CURRENT_CHARGES
                       ,CITY
                       ,STATE_CODE
                       ,ZIPCODE
                       ,VENDOR_BILLING_DATE
                       ,0 is_processed
                       ,0 is_quarterly
                       ,CUSTOMER_ID
                       ,CASE WHEN REPLACE(CURRENCY, '|', SPACE(0)) IN ( 'Canadian Dollars', 'CAD' ) THEN 'CAN'
                             ELSE 'USD'
                        END currency
                  FROM
                        dbo.UBM_AVISTA_FEED avs
                  WHERE
                        avs.UBM_BATCH_MASTER_LOG_ID = @ubm_batch_master_log_id
                        AND avs.CUSTOMER_ID = @client_id
                        AND avs.CONSOLIDATION_ID = @consolidation_id
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          #Allowed_Client_Site_Service acs
                                         WHERE
                                          avs.CUSTOMER_ID = acs.Customer_Id )
                  GROUP BY
                        UBM_BATCH_MASTER_LOG_ID
                       ,BILL_ID
                       ,ACCOUNT_ID
                       ,ACCOUNT_NUMBER
                       ,VENDOR_ID
                       ,VENDOR_NAME
                       ,CUSTOMER_NAME
                       ,ADDRESS_LINE1
                       ,ADDRESS_LINE2
                       ,ADDRESS_LINE3
                       ,AMOUNT_DUE
                       ,PREVIOUS_BALANCE
                       ,DUE_DATE
                       ,CURRENT_CHARGES
                       ,CITY
                       ,STATE_CODE
                       ,ZIPCODE
                       ,VENDOR_BILLING_DATE
                       ,CUSTOMER_ID
                       ,CASE WHEN REPLACE(currency, '|', SPACE(0)) IN ( 'Canadian Dollars', 'CAD' ) THEN 'CAN'
                             ELSE 'USD'
                        END
	
      INSERT      INTO dbo.UBM_INVOICE
                  ( UBM_BATCH_MASTER_LOG_ID
                  ,INVOICE_IDENTIFIER
                  ,CBMS_IMAGE_ID
                  ,UBM_ACCOUNT_ID
                  ,UBM_ACCOUNT_NUMBER
                  ,UBM_VENDOR_ID
                  ,UBM_VENDOR_NAME
                  ,UBM_CLIENT_NAME
                  ,BILL_ADDRESS_LINE1
                  ,BILL_ADDRESS_LINE2
                  ,BILL_ADDRESS_LINE3
                  ,AMOUNT_DUE
                  ,PREVIOUS_BALANCE
                  ,PAYMENT_DUE_DATE
                  ,CURRENT_CHARGES
                  ,CITY
                  ,STATE
                  ,ZIP_CODE
                  ,VENDOR_BILLING_DATE
                  ,IS_PROCESSED
                  ,IS_QUARTERLY
                  ,UBM_CLIENT_ID
                  ,CURRENCY )
                  SELECT
                        UBM_BATCH_MASTER_LOG_ID
                       ,BILL_ID
                       ,NULL cbms_image_id
                       ,ACCOUNT_ID
                       ,ACCOUNT_NUMBER
                       ,VENDOR_ID
                       ,VENDOR_NAME
                       ,CUSTOMER_NAME
                       ,ADDRESS_LINE1
                       ,ADDRESS_LINE2
                       ,ADDRESS_LINE3
                       ,AMOUNT_DUE
                       ,PREVIOUS_BALANCE
                       ,DUE_DATE
                       ,CURRENT_CHARGES
                       ,CITY
                       ,STATE_CODE
                       ,ZIPCODE
                       ,VENDOR_BILLING_DATE
                       ,0 is_processed
                       ,0 is_quarterly
                       ,CUSTOMER_ID
                       ,CASE WHEN REPLACE(CURRENCY, '|', SPACE(0)) IN ( 'Canadian Dollars', 'CAD' ) THEN 'CAN'
                             ELSE 'USD'
                        END currency
                  FROM
                        dbo.UBM_AVISTA_FEED avs
                  WHERE
                        avs.UBM_BATCH_MASTER_LOG_ID = @ubm_batch_master_log_id
                        AND avs.CUSTOMER_ID = @client_id
                        AND avs.CONSOLIDATION_ID = @consolidation_id
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          #Allowed_Client_Site_Service acss
                                     WHERE
                                          acss.Customer_Id = avs.CUSTOMER_ID
                                          AND acss.Service_Group = avs.SERVICE_GROUP
                                          AND acss.Reference_Site_Number = avs.REFERENCE_SITE_NUMBER )
                  GROUP BY
                        UBM_BATCH_MASTER_LOG_ID
                       ,BILL_ID
                       ,ACCOUNT_ID
                       ,ACCOUNT_NUMBER
                       ,VENDOR_ID
                       ,VENDOR_NAME
                       ,CUSTOMER_NAME
                       ,ADDRESS_LINE1
                       ,ADDRESS_LINE2
                       ,ADDRESS_LINE3
                       ,AMOUNT_DUE
                       ,PREVIOUS_BALANCE
                       ,DUE_DATE
                       ,CURRENT_CHARGES
                       ,CITY
                       ,STATE_CODE
                       ,ZIPCODE
                       ,VENDOR_BILLING_DATE
                       ,CUSTOMER_ID
                       ,CASE WHEN REPLACE(currency, '|', SPACE(0)) IN ( 'Canadian Dollars', 'CAD' ) THEN 'CAN'
                             ELSE 'USD'
                        END
					
      INSERT      INTO dbo.UBM_INVOICE_DETAILS
                  ( UBM_INVOICE_ID
                  ,SERVICE_START_DATE
                  ,SERVICE_END_DATE
                  ,BILLING_DAYS
                  ,ITEM_CODE
                  ,ITEM_TYPE_DESCRIPTION
                  ,ITEM_NAME
                  ,ITEM_AMOUNT
                  ,ITEM_QUANTITY
                  ,UNIT_OF_MEASURE_CODE
                  ,ESTIMATE_FLAG
                  ,ITEM_QUANTITY_ACTUAL )
                  SELECT
                        i.UBM_INVOICE_ID
                       ,f.BEGIN_SERVICE_DATE
                       ,f.END_SERVICE_DATE
                       ,DATEDIFF(DAY, f.BEGIN_SERVICE_DATE, f.END_SERVICE_DATE)
                       ,f.SERVICE_GROUP
                       ,f.SERVICE_CODE_DESC
                       ,f.SERVICE_CODE_ALIAS
                       ,f.SERVICE_AMOUNT
                       ,CASE WHEN f.DEMAND_UOM IN ( 'Kw', 'kVa', 'KVar' ) THEN f.DEMAND_QUANTITY
                             ELSE f.BILL_QUANTITY
                        END item_quantity
                       ,CASE WHEN f.DEMAND_UOM IN ( 'Kw', 'kVa', 'KVar' ) THEN f.DEMAND_UOM
                             ELSE f.RAW_UOM
                        END AS raw_uom
                       ,f.ESTIMATE_FLAG
                       ,f.RAW_USAGE
                  FROM
                        dbo.UBM_INVOICE i
                        INNER JOIN dbo.UBM_AVISTA_FEED f
                              ON f.BILL_ID = i.INVOICE_IDENTIFIER
                                 AND f.UBM_BATCH_MASTER_LOG_ID = i.UBM_BATCH_MASTER_LOG_ID
                                 AND f.CURRENT_CHARGES = i.CURRENT_CHARGES
                  WHERE
                        i.UBM_BATCH_MASTER_LOG_ID = @ubm_batch_master_log_id
                        AND f.CUSTOMER_ID = @client_id
                        AND f.CONSOLIDATION_ID = @consolidation_id
                  GROUP BY
                        i.UBM_INVOICE_ID
                       ,f.BEGIN_SERVICE_DATE
                       ,f.END_SERVICE_DATE
                       ,DATEDIFF(DAY, f.BEGIN_SERVICE_DATE, f.END_SERVICE_DATE)
                       ,f.SERVICE_GROUP
                       ,f.SERVICE_CODE_DESC
                       ,f.SERVICE_CODE_ALIAS
                       ,f.SERVICE_AMOUNT
                       ,f.DEMAND_UOM
                       ,f.DEMAND_QUANTITY
                       ,f.BILL_QUANTITY
                       ,f.RAW_UOM
                       ,f.ESTIMATE_FLAG
                       ,f.RAW_USAGE
                       ,f.VENDOR_METER_NUMBER



      INSERT      INTO dbo.UBM_INVOICE_METER_DETAILS
                  ( UBM_INVOICE_ID
                  ,UBM_METER_NUMBER
                  ,UBM_RATE_NAME
                  ,DEMAND_QUANTITY
                  ,DEMAND_UNIT_OF_MEASURE_CODE
                  ,USAGE_QUANTITY
                  ,USAGE_UNIT_OF_MEASURE_CODE
                  ,SERVICE_AMOUNT
                  ,METER_LAST_READING
                  ,METER_CURRENT_READING
                  ,POWER_FACTOR )
                  SELECT
                        i.UBM_INVOICE_ID
                       ,f.VENDOR_METER_NUMBER
                       ,f.RATE_CODE
                       ,f.DEMAND_QUANTITY
                       ,f.DEMAND_UOM
                       ,f.RAW_USAGE
                       ,f.RAW_UOM
                       ,f.SERVICE_AMOUNT
                       ,f.METER_LAST_READING
                       ,f.METER_CURRENT_READING
                       ,f.POWER_FACTOR
                  FROM
                        dbo.UBM_INVOICE i
                        INNER JOIN dbo.UBM_AVISTA_FEED f
                              ON f.BILL_ID = i.INVOICE_IDENTIFIER
                                 AND f.UBM_BATCH_MASTER_LOG_ID = i.UBM_BATCH_MASTER_LOG_ID
                                 AND f.CURRENT_CHARGES = i.CURRENT_CHARGES
                  WHERE
                        i.UBM_BATCH_MASTER_LOG_ID = @ubm_batch_master_log_id
                        AND f.CUSTOMER_ID = @client_id
                        AND f.CONSOLIDATION_ID = @consolidation_id
                  GROUP BY
                        i.UBM_INVOICE_ID
                       ,f.VENDOR_METER_NUMBER
                       ,f.RATE_CODE
                       ,f.DEMAND_QUANTITY
                       ,f.DEMAND_UOM
                       ,f.RAW_USAGE
                       ,f.RAW_UOM
                       ,f.SERVICE_AMOUNT
                       ,f.METER_LAST_READING
                       ,f.METER_CURRENT_READING
                       ,f.POWER_FACTOR

	-- 2) Update ubm_invoice with cbms_image_id

      DECLARE curInvoice CURSOR FAST_FORWARD
      FOR
      SELECT
            BILL_ID
      FROM
            dbo.UBM_AVISTA_FEED
      WHERE
            UBM_BATCH_MASTER_LOG_ID = @ubm_batch_master_log_id
            AND CUSTOMER_ID = @client_id
            AND CONSOLIDATION_ID = @consolidation_id
      GROUP BY
            BILL_ID
      OPEN curInvoice

      FETCH NEXT FROM curInvoice INTO @bill_id
      WHILE ( @@fetch_status = 0 )
            BEGIN

                  SELECT
                        @cbms_image_id = MIN(CBMS_IMAGE_ID)
                  FROM
                        dbo.cbms_image
                  WHERE
                        CBMS_DOC_ID IN ( @bill_id + '.tif', @bill_id + '.pdf', @bill_id + '.html', @bill_id + '.htm', @bill_id + '.jpg' )

                  IF @cbms_image_id IS NOT NULL
                        BEGIN

                              UPDATE
                                    dbo.UBM_INVOICE
                              SET
                                    CBMS_IMAGE_ID = @cbms_image_id
                              WHERE
                                    UBM_BATCH_MASTER_LOG_ID = @ubm_batch_master_log_id
                                    AND INVOICE_IDENTIFIER = @bill_id
                        END
                  ELSE
                        BEGIN

                              SELECT
                                    @missed_image = @missed_image + ', ' + @bill_id

                        END

                  FETCH NEXT FROM curInvoice INTO @bill_id

            END

      CLOSE curInvoice
      DEALLOCATE curInvoice

      SELECT
            @missed_image = SUBSTRING(@missed_image, 3, LEN(@missed_image))

      IF @missed_image = ''
            SELECT
                  @missed_image = '0'

      RETURN

      DROP TABLE #Allowed_Client_Site_Service
END;
;
GO



GRANT EXECUTE ON  [dbo].[UBM_INSERT_AVISTA_EFFECTIVE_INVOICE_DATA_P] TO [CBMSApplication]
GO
