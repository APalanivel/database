SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: dbo.Account_Invoice_Processing_Log_Sel_By_Account_Id_User_Info_Id
     
DESCRIPTION: 
	To Delete Account Processing Instruction configs  for selected config.
      
INPUT PARAMETERS:          
NAME										DATATYPE			DEFAULT			DESCRIPTION          
------------------------------------------------------------------------------------------------------          
@Account_Id									INT	
@Account_Invoice_Processing_Config_Id		INT			
                
OUTPUT PARAMETERS:          
NAME										DATATYPE			DEFAULT			DESCRIPTION          
------------------------------------------------------------------------------------------------------  
        
USAGE EXAMPLES:          
------------------------------------------------------------------------------------------------------  
  


		EXEC Account_Invoice_Processing_Log_Sel_By_Account_Id_Config_Id  1654788,4

	
	 select * from Account_Invoice_Processing_Config
	
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------------------------------------------------  
NR			Narayana Reddy
          
MODIFICATIONS           
INITIALS	DATE			MODIFICATION          
------------------------------------------------------------------------------------------------------  
NR			2019-03-01		WatchList - Created	

*/

CREATE PROCEDURE [dbo].[Account_Invoice_Processing_Log_Sel_By_Account_Id_Config_Id]
    (
        @Account_Id INT
        , @Account_Invoice_Processing_Config_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            aipc.Account_Id
            , aipc.Account_Invoice_Processing_Config_Id
            , aipcl.Field_Name
            , aipcl.Change_Type
            , aipcl.Previous_Value
            , aipcl.Current_Value
            , aipcl.Is_Updated_Using_Apply_All
            , CONVERT(VARCHAR, aipcl.Event_Ts, 101) AS Event_Ts
            , aipcl.Event_By_User_Id
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Event_By_User
        FROM
            dbo.Account_Invoice_Processing_Config aipc
            INNER JOIN dbo.Account_Invoice_Processing_Config_Log aipcl
                ON aipcl.Account_Invoice_Processing_Config_Id = aipc.Account_Invoice_Processing_Config_Id
            INNER JOIN dbo.USER_INFO ui
                ON ui.USER_INFO_ID = aipcl.Event_By_User_Id
        WHERE
            aipc.Account_Id = @Account_Id
            AND aipc.Account_Invoice_Processing_Config_Id = @Account_Invoice_Processing_Config_Id
        ORDER BY
            aipcl.Event_Ts ;






    END;


GO
GRANT EXECUTE ON  [dbo].[Account_Invoice_Processing_Log_Sel_By_Account_Id_Config_Id] TO [CBMSApplication]
GO
