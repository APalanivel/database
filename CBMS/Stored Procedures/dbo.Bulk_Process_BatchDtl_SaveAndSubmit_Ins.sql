SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
NAME:    [Bulk_Process_BatchDtl_SaveAndSubmit_Ins]                  
                   
DESCRIPTION:                   
                    
                    
AUTHOR INITIALS:                      
 Initials Name                      
------------------------------------------------------------                      
 PK prasan kumar                 
                   
 MODIFICATIONS                                    
 Initials Date   Modification              
 PK  12-06-2020  now using existing sp to insert into CU_IVOICE_EVENT table        
 PK 16-06-2020  Added check for invoice exist else Failing invoice    
------------------------------------------------------------                      
                  
******/                  
                  
CREATE  PROCEDURE [dbo].[Bulk_Process_BatchDtl_SaveAndSubmit_Ins]                  
    (                  
        @Batch_Id                INT                  
       ,@Cu_Invoice_Id           INT                    
       ,@Run_Data_Quality_Test   CHAR (3)                  
       ,@Run_Recalc              CHAR (3)                  
       ,@Run_Variance_Test       CHAR (3)                  
       ,@Event_Log_Ins           BIT = 0                  
       ,@User_Id                 INT                  
    )                  
AS                  
BEGIN                  
                  
    SET NOCOUNT ON;    
   
 DECLARE                  
            @ErrorVar INT,                  
			@this_id  INT,                 
            @statusPending INT,
			@fail varchar(100);               
                  
    BEGIN TRY                  
                  
                        
        SELECT @statusPending = MAX(CASE WHEN [cd].[Code_Value] = 'Pending' THEN [cd].[Code_Id]      END)                                                                          
			, @fail =  MAX(CASE WHEN [cd].[Code_Value] = 'Error' THEN [cd].[Code_Id]      END)       
        FROM                                
               [dbo].[Code] AS [cd]                                
		INNER JOIN [dbo].[Codeset] AS [cs]                                
                   ON [cs].[Codeset_Id] = [cd].[Codeset_Id]                                
		WHERE [cd].[Is_Active] = 1                                
               AND [cs].[Codeset_Name] = 'Batch Status';                       
            
        INSERT INTO LOGDB.dbo.XL_Bulk_Invoice_Process_Batch_Dtl                  
        (                  
            XL_Bulk_Invoice_Process_Batch_Id                  
           ,Cu_Invoice_Id                 
           ,Run_Data_Quality_Test                  
           ,Run_Recalc                  
           ,Run_Variance_Test                  
           ,Status_Cd                  
           ,Created_Ts                  
           ,Last_Change_Ts                  
        )                  
        VALUES                  
            (                  
                @Batch_Id, @Cu_Invoice_Id, @Run_Data_Quality_Test, @Run_Recalc, @Run_Variance_Test                  
               ,@statusPending, GETDATE(), GETDATE()                  
            );                  
                  
		SELECT @this_id = @@identity;               
                  
		IF NOT EXISTS ( SELECT 1 FROM CU_INVOICE WHERE CU_INVOICE_ID =@Cu_Invoice_Id )  
		 RAISERROR ('Fail. Incorrect Invoice Id Enetered.', 16, 1 );  
  
        IF @Event_Log_Ins = 1      
			EXEC cbmsCuInvoiceEvent_Save @User_Id,@Cu_Invoice_Id,'Update Bulk Excel - Save and Submit'      
         
    END TRY                  
    BEGIN CATCH                  
                  
		UPDATE LOGDB.dbo.XL_Bulk_Invoice_Process_Batch_Dtl SET Error_Msg = ERROR_MESSAGE(),
		 Status_Cd = @fail    
		WHERE XL_Bulk_Data_Process_Batch_Dtl_Id = @this_id    
  
        --EXEC dbo.usp_RethrowError;                  
                  
    END CATCH;                  
END; 
GO
GRANT EXECUTE ON  [dbo].[Bulk_Process_BatchDtl_SaveAndSubmit_Ins] TO [CBMSApplication]
GO
