
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	[dbo].[cbmsUserByQueueId_Get] 

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
   @Queue_Id int

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
     
USAGE EXAMPLES:
------------------------------------------------------------

 Exec [dbo].[cbmsUserByQueueId_Get]   49


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS
	Initials	Date			Modification
------------------------------------------------------------            	 	
	KC          29-Sept-2009    Created new SProc to get First Name and Last Name  by passing Queue Id	
	NR			2017-05-23		MAINT-5358 Added User_Info_Id in Output List.								
								        	
******/


CREATE  PROCEDURE [dbo].[cbmsUserByQueueId_Get] ( @Queue_id INT )
AS 
BEGIN  
      SELECT
            ui.first_name
           ,ui.last_name
           ,ui.USER_INFO_ID
      FROM
            user_info ui
      WHERE
            ui.queue_id = @Queue_id  
END  

;
GO

GRANT EXECUTE ON  [dbo].[cbmsUserByQueueId_Get] TO [CBMSApplication]
GO
