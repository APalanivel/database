
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******************************************************************************************************        
NAME : dbo.Data_Upload_Request_Pending_SEL   
       
DESCRIPTION:     
Stored Procedure is used to get the data from Data Upload Audit Log.  
   
 INPUT PARAMETERS:        
 Name             DataType               Default        Description        
--------------------------------------------------------------------     
  
  
 OUTPUT PARAMETERS:        
 Name   DataType    Default   Description        
--------------------------------------------------------------------        
DATA_UPLOAD_QUEUE_ID INT  
REQUEST_HANDLER_URL  VARCHAR  
    
  USAGE EXAMPLES:        
--------------------------------------------------------------------        
EXEC Data_Upload_Request_Pending_SEL  
      
AUTHOR INITIALS:        
 Initials Name        
-------------------------------------------------------------------        
Romy Thomas  
       
 MODIFICATIONS         
 Initials Date  Modification        
--------------------------------------------------------------------  
RT   07/13/2011 Created  
JR	 05/17/2013 Modified to get only top 1 row from the pending request to
				enable parallel scheduling of the batch job
  
******************************************************************************************************/    
  
CREATE PROCEDURE [dbo].[Data_Upload_Request_Pending_SEL]
      ( 
       @RequestType VARCHAR(200) )
AS 
BEGIN  
      SET NOCOUNT ON;  
      DECLARE @PENDING_STATUS_CODE INT  
       
      SELECT
            @PENDING_STATUS_CODE = C.CODE_ID
      FROM
            CODE C
            INNER JOIN CODESET CS
                  ON C.CODESET_ID = CS.CODESET_ID
                     AND CS.CODESET_NAME = 'Data Upload Status'
                     AND C.CODE_DSC = 'Pending'  
  
      SELECT TOP 1
            DUQ.DATA_UPLOAD_QUEUE_ID
           ,DURT.REQUEST_HANDLER_URL
      FROM
            DATA_UPLOAD_QUEUE DUQ
            INNER JOIN DATA_UPLOAD_REQUEST_TYPE DURT
                  ON DUQ.DATA_UPLOAD_REQUEST_TYPE_ID = DURT.DATA_UPLOAD_REQUEST_TYPE_ID
                     AND DUQ.STATUS_CD = @PENDING_STATUS_CODE
                     AND DURT.Request_Name = @RequestType
                     AND datediff(MINUTE, DUQ.CREATE_TS, getdate()) > 30
      ORDER BY
            DUQ.DATA_UPLOAD_QUEUE_ID
END  
;
GO

GRANT EXECUTE ON  [dbo].[Data_Upload_Request_Pending_SEL] TO [CBMSApplication]
GO
