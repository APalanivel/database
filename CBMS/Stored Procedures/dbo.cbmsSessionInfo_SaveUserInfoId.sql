SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  Procedure [dbo].[cbmsSessionInfo_SaveUserInfoId]
   (
     @session_info_id int
   , @user_info_id int
	
   )
AS 
   BEGIN

      declare @ThisId int

      set nocount on

      select   @ThisId = session_info_id
      from     session_info 
      where    session_info_id = @session_info_id
               and user_info_id is null

      if @ThisId is not null 
         begin

            update   session_info 
            set      user_info_id = @user_info_id
            where    session_info_id = @session_info_id

         end

      exec cbmsSessionInfo_GetWorking @ThisId

   END
GO
GRANT EXECUTE ON  [dbo].[cbmsSessionInfo_SaveUserInfoId] TO [CBMSApplication]
GO
