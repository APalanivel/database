SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure dbo.seSupplyDemandOverview_Save
	( @SupplyDemandOverviewId int
	, @YearLabel char(4)
	, @DryProduction int
	, @NetImports int
	, @SupplementalGasFuels int
	, @TotalGasSupply int
	, @ResidentialCommercial int
	, @Industrial  int
	, @PowerGeneration int
	, @OtherConsumption int
	, @TotalConsumption int
	, @Imbalance int
	, @BalanceFactor int
	, @TotalChange int
	)
AS
BEGIN
	set nocount on
	   update seSupplyDemandOverview
	      set YearLabel = @YearLabel
		, DryProduction  = @DryProduction 
		, NetImports = @NetImports 
		, SupplementalGasFuels = @SupplementalGasFuels
		, TotalGasSupply = @TotalGasSupply 
		, ResidentialCommercial = @ResidentialCommercial 
		, Industrial = @Industrial  
		, PowerGeneration = @PowerGeneration 
		, OtherConsumption = @OtherConsumption 
		, TotalConsumption = @TotalConsumption 
		, Imbalance = @Imbalance 
		, BalanceFactor = @BalanceFactor 
		, TotalChange = @TotalChange 
	    where SupplyDemandOverviewId = @SupplyDemandOverviewId
	exec seSupplyDemandOverview_Get @SupplyDemandOverviewId
END
GO
GRANT EXECUTE ON  [dbo].[seSupplyDemandOverview_Save] TO [CBMSApplication]
GO
