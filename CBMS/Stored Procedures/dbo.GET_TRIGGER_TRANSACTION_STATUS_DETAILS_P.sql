SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_TRIGGER_TRANSACTION_STATUS_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	@userId        			varchar(10)	          	
	@sessionId     			varchar(20)	          	
	@dealTicketId  			int       	          	
	@isClientConfirmFlag	bit       	          	
	@isCounterpartyFlag		bit       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.GET_TRIGGER_TRANSACTION_STATUS_DETAILS_P -1,-1,105530,1,1

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/28/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on
	
******/

CREATE PROCEDURE dbo.GET_TRIGGER_TRANSACTION_STATUS_DETAILS_P
@userId					VARCHAR(10),
@sessionId				VARCHAR(20),
@dealTicketId			INT, 
@isClientConfirmFlag	BIT,
@isCounterpartyFlag		BIT
AS
BEGIN
	
	SET NOCOUNT ON	;

	DECLARE @sqlStatement VARCHAR(8000)
	DECLARE @query VARCHAR(1000)
	DECLARE @orderByClause	VARCHAR(1000)

	SELECT @query = 
		'SELECT    ' + 
			'e1.ENTITY_NAME AS TRIGGER_STATUS,    ' + 
			'rmdtd.TRIGGER_TRANSACTION_DATE,   ' +  
			'rmdtd.MONTH_IDENTIFIER,    ' + 
			'rmdtd.IS_TRIGGER_CONFIRMED,    ' + 
			'rmdtd.CBMS_IMAGE_ID,    ' + 
			'rmdtd.CLIENT_CONFIRM_CBMS_IMAGE_ID,    ' + 
			'rmdtd.CP_CONFIRM_CBMS_IMAGE_ID,     ' + 
			'e2.ENTITY_NAME AS CONFIRMATION_TYPE    ' + 
		'FROM   ' + 
			'dbo.RM_DEAL_TICKET_DETAILS rmdtd,   ' + 
			'dbo.ENTITY e1, dbo.ENTITY e2   ' + 
		'WHERE   ' + 
			' rmdtd.RM_DEAL_TICKET_ID = ' + STR(@dealTicketId) + 
			' AND e1.ENTITY_ID = rmdtd.TRIGGER_STATUS_TYPE_ID   ' +
			' AND e2.ENTITY_ID = rmdtd.CONFIRMATION_TYPE_ID   ' 

	IF @isClientConfirmFlag > 0
		BEGIN 
			SELECT @orderByClause = 
				' ORDER BY  rmdtd.CLIENT_CONFIRM_CBMS_IMAGE_ID,   ' + 
				' rmdtd.TRIGGER_TRANSACTION_DATE DESC '
		END 
	ELSE IF @isCounterpartyFlag > 0
		BEGIN
			SELECT @orderByClause = 
				' ORDER BY  rmdtd.CP_CONFIRM_CBMS_IMAGE_ID,   ' + 
				' rmdtd.TRIGGER_TRANSACTION_DATE DESC '
		END 
	ELSE 
		BEGIN
			SELECT @orderByClause = 
				' ORDER BY  rmdtd.CBMS_IMAGE_ID,   ' + 
				' rmdtd.TRIGGER_TRANSACTION_DATE DESC '
		END

	SELECT @SQLStatement = @query + @orderByClause	
	
	EXEC (@SQLStatement)

END
GO
GRANT EXECUTE ON  [dbo].[GET_TRIGGER_TRANSACTION_STATUS_DETAILS_P] TO [CBMSApplication]
GO
