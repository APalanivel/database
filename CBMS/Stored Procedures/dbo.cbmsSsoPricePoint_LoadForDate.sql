SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[cbmsSsoPricePoint_LoadForDate] 

	( @MyAccountId int
	, @start_date datetime
	, @end_date datetime
	, @price_point_id int = null
	)
AS
BEGIN

	set nocount on

	  declare @year varchar(4)

	   select pr.index_id 		market_index_id
		, mi.entity_name 	market_index
		, pr.pricing_point 	price_point
		, pr.price_index_id     
		, pv.index_value
		, pv.indeX_month
             from price_index pr
	     join price_index_value pv on pv.price_index_id = pr.price_index_id
	     join entity mi on mi.entity_id = pr.index_id
	    where pv.index_month >= isNull(@start_date, pv.indeX_month)
	      and pv.index_month <= isNull(@end_date, pv.indeX_month)
   	      and pr.price_index_id = isNull(@price_point_id, pr.price_index_id)
		order by pv.indeX_month


END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoPricePoint_LoadForDate] TO [CBMSApplication]
GO
