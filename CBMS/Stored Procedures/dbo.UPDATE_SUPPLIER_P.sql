SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.UPDATE_SUPPLIER_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
	@vendorName VARCHAR(200),
	@vendorId Int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- Test Update an entry
--exec UPDATE_SUPPLIER_P
--	@vendorName = 'Adams Electric Cooperative, Inc.' ,  -- Original value = 'Adams Electric Cooperative, Inc.' 
--	@vendorId = 1
	
---- Check at SV for the update
--select vendor_name from sv..vendor where vendor_id = 1	


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.UPDATE_SUPPLIER_P
	@vendorName VARCHAR(200),
	@vendorId Int
	
AS


BEGIN

	UPDATE 
		VENDOR 
	SET 
		VENDOR_NAME=@vendorName 
	WHERE 
		VENDOR_ID=@vendorId

END
GO
GRANT EXECUTE ON  [dbo].[UPDATE_SUPPLIER_P] TO [CBMSApplication]
GO
