SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********    
    
NAME:                             
     dbo.Invoice_Collection_Excluded   
    
DESCRIPTION:                                            
    
 INPUT PARAMETERS:                                            
    
 Name                        DataType         Default       Description                                          
---------------------------------------------------------------------------------------------------------------                                        
                               
    
 OUTPUT PARAMETERS:                                            
    
 Name                        DataType         Default       Description                                          
---------------------------------------------------------------------------------------------------------------                                        
    
 USAGE EXAMPLES:                                                              
---------------------------------------------------------------------------------------------------------------                                                              
    
 BEGIN TRAN                                  
 EXEC dbo.Report_DE_Invoice_Collection_Excluded           
 ROLLBACK TRAN                             
                                      
    
    
 AUTHOR INITIALS:                                          
    
 Initials   Name                                          
---------------------------------------------------------------------------------------------------------------                                                        
 ABK   Aditya Bharadwaj K                                                                
    
 MODIFICATIONS:                                        
    
 Initials Date        Modification                                        
---------------------------------------------------------------------------------------------------------------                                        
 ABK  2019-02-21   REPTMGR-102 - Created this new SProc to get excluded invoice collection details.  
 RKV  2019-07-18   IC-Project Added new column Comments	
*********/

CREATE PROCEDURE [dbo].[Report_DE_Invoice_Collection_Excluded]
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Source_Type_Client INT
            , @Source_Type_Account INT
            , @Source_Type_Vendor INT
            , @Contact_Level_Client INT
            , @Contact_Level_Account INT
            , @Contact_Level_Vendor INT;

        SELECT
            @Source_Type_Client = MAX(CASE WHEN c.Code_Value = 'Client Primary Contact' THEN c.Code_Id
                                      END)
            , @Source_Type_Account = MAX(CASE WHEN c.Code_Value = 'Account Primary Contact' THEN c.Code_Id
                                         END)
            , @Source_Type_Vendor = MAX(CASE WHEN c.Code_Value = 'Vendor Primary Contact' THEN c.Code_Id
                                        END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'InvoiceSourceType'
            AND c.Code_Value IN ( 'Account Primary Contact', 'Client Primary Contact', 'Vendor Primary Contact' );

        SELECT
            @Contact_Level_Client = MAX(CASE WHEN c.Code_Value = 'Client' THEN c.Code_Id
                                        END)
            , @Contact_Level_Account = MAX(CASE WHEN c.Code_Value = 'Account' THEN c.Code_Id
                                           END)
            , @Contact_Level_Vendor = MAX(CASE WHEN c.Code_Value = 'Vendor' THEN c.Code_Id
                                          END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'ContactLevel';

        CREATE TABLE #Vendor_Dtls
             (
                 Account_Vendor_Name VARCHAR(200)
                 , Invoice_Collection_Account_Config_Id INT
             );


        INSERT INTO #Vendor_Dtls
             (
                 Account_Vendor_Name
                 , Invoice_Collection_Account_Config_Id
             )
        SELECT
            Account_Vendor_Name = CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                                            AND asbv1.Account_Id IS NOT NULL THEN scha.Account_Vendor_Name
                                      WHEN v.VENDOR_NAME IS NOT NULL THEN v.VENDOR_NAME
                                      WHEN icav.VENDOR_NAME IS NOT NULL
                                           AND  asbv1.Account_Id IS NOT NULL THEN icav.VENDOR_NAME
                                      ELSE ucha.Account_Vendor_Name
                                  END
            , icac.Invoice_Collection_Account_Config_Id
        FROM
            dbo.Invoice_Collection_Account_Config icac
            INNER JOIN dbo.Code cpc
                ON cpc.Code_Id = icac.Chase_Priority_Cd
            INNER JOIN Core.Client_Hier_Account ucha
                ON icac.Account_Id = ucha.Account_Id
            LEFT OUTER JOIN(Core.Client_Hier_Account scha
                            INNER JOIN Core.Client_Hier_Account ucha1
                                ON ucha1.Meter_Id = scha.Meter_Id
                                   AND  ucha1.Account_Type = 'Utility'
                            INNER JOIN dbo.CONTRACT c
                                ON c.CONTRACT_ID = scha.Supplier_Contract_ID
                            INNER JOIN dbo.ENTITY ce
                                ON c.CONTRACT_TYPE_ID = ce.ENTITY_ID
                                   AND  ce.ENTITY_NAME = 'Supplier')
                ON ucha.Account_Id = ucha1.Account_Id
                   AND  scha.Account_Type = 'Supplier'
                   AND  (CASE WHEN icac.Invoice_Collection_Service_End_Dt IS NULL
                                   OR   icac.Invoice_Collection_Service_End_Dt > GETDATE() THEN GETDATE()
                             ELSE icac.Invoice_Collection_Service_End_Dt
                         END) BETWEEN scha.Supplier_Meter_Association_Date
                              AND     scha.Supplier_Meter_Disassociation_Date
            LEFT OUTER JOIN(dbo.Account_Consolidated_Billing_Vendor asbv
                            INNER JOIN dbo.ENTITY e
                                ON asbv.Invoice_Vendor_Type_Id = e.ENTITY_ID
                                   AND  e.ENTITY_NAME = 'Supplier'
                            LEFT OUTER JOIN dbo.VENDOR v
                                ON v.VENDOR_ID = asbv.Supplier_Vendor_Id)
                ON asbv.Account_Id = ucha.Account_Id
                   AND  (CASE WHEN icac.Invoice_Collection_Service_End_Dt IS NULL
                                   OR   asbv.Billing_End_Dt > GETDATE() THEN GETDATE()
                             ELSE icac.Invoice_Collection_Service_End_Dt
                         END) BETWEEN asbv.Billing_Start_Dt
                              AND     ISNULL(asbv.Billing_End_Dt, '9999-12-31')
            LEFT OUTER JOIN(dbo.Account_Consolidated_Billing_Vendor asbv1
                            INNER JOIN dbo.ENTITY e1
                                ON asbv1.Invoice_Vendor_Type_Id = e1.ENTITY_ID
                                   AND  e1.ENTITY_NAME = 'Supplier'
                            LEFT OUTER JOIN dbo.VENDOR v1
                                ON v1.VENDOR_ID = asbv1.Supplier_Vendor_Id)
                ON asbv1.Account_Id = ucha.Account_Id
            LEFT OUTER JOIN(dbo.Invoice_Collection_Account_Contact icc
                            INNER JOIN dbo.Account_Invoice_Collection_Source aics
                                ON icc.Invoice_Collection_Account_Config_Id = aics.Invoice_Collection_Account_Config_Id
                            INNER JOIN dbo.Contact_Info ci
                                ON ci.Contact_Info_Id = icc.Contact_Info_Id
                                   AND  (   (   @Source_Type_Client = aics.Invoice_Source_Type_Cd
                                                AND @Contact_Level_Client = ci.Contact_Level_Cd)
                                            OR  (   @Source_Type_Account = aics.Invoice_Source_Type_Cd
                                                    AND @Contact_Level_Account = ci.Contact_Level_Cd)
                                            OR  (   @Source_Type_Vendor = aics.Invoice_Source_Type_Cd
                                                    AND @Contact_Level_Vendor = ci.Contact_Level_Cd))
                            INNER JOIN dbo.Vendor_Contact_Map vcm
                                ON ci.Contact_Info_Id = vcm.Contact_Info_Id
                            INNER JOIN dbo.VENDOR icav
                                ON icav.VENDOR_ID = vcm.VENDOR_ID
                            INNER JOIN dbo.ENTITY ve
                                ON icav.VENDOR_TYPE_ID = ve.ENTITY_ID)
                ON icc.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                   AND  icc.Is_Primary = 1
        WHERE
            ucha.Account_Type = 'Utility'
        GROUP BY
            CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                      AND   asbv1.Account_Id IS NOT NULL THEN scha.Account_Vendor_Name
                WHEN v.VENDOR_NAME IS NOT NULL THEN v.VENDOR_NAME
                WHEN icav.VENDOR_NAME IS NOT NULL
                     AND asbv1.Account_Id IS NOT NULL THEN icav.VENDOR_NAME
                ELSE ucha.Account_Vendor_Name
            END
            , icac.Invoice_Collection_Account_Config_Id;


        SELECT
            ch.Client_Name [Client Name]
            , cha.Display_Account_Number [Account Number]
            , ISNULL(vd.Account_Vendor_Name, cha.Account_Vendor_Name) [Vendor]
            , ch.Site_name [Site]
            , ch.Country_Name [Country]
            , ch.State_Name [State]
            , icq.Collection_Start_Dt [Excluded Start Date]
            , icq.Collection_End_Dt [Excluded End Date]
            , icq.Last_Change_Ts [Invoice Excluded Date]
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME [Invoice Excluded By]
            , ui_owner.FIRST_NAME + ' ' + ui_owner.LAST_NAME Invoice_collection_Owner
            , ui_officer.FIRST_NAME + ' ' + ui_officer.LAST_NAME Invoice_collection_Officer
            , CASE WHEN LEN(Comments.Exclude_Comments) > 0 THEN
                       LEFT(Comments.Exclude_Comments, LEN(Comments.Exclude_Comments) - 1)
                  ELSE Comments.Exclude_Comments
              END AS Comments
            , icq.Invoice_Collection_Queue_Id
        FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN dbo.Invoice_Collection_Account_Config AS icac
                ON icac.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
            INNER JOIN Core.Client_Hier_Account AS cha
                ON cha.Account_Id = icac.Account_Id
            INNER JOIN Core.Client_Hier AS ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN dbo.Code AS c
                ON c.Code_Id = icq.Status_Cd
            INNER JOIN dbo.USER_INFO ui
                ON ui.USER_INFO_ID = icq.Updated_User_Id
            INNER JOIN dbo.USER_INFO ui_officer
                ON ui_officer.USER_INFO_ID = icac.Invoice_Collection_Officer_User_Id
            LEFT JOIN dbo.USER_INFO ui_owner
                ON ui_owner.USER_INFO_ID = icac.Invoice_Collection_Owner_User_Id
            LEFT JOIN #Vendor_Dtls vd
                ON vd.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            CROSS APPLY
        (   SELECT
                (   SELECT
                        icqec.Comment_Desc + ' [' + CONVERT(VARCHAR, icqec.[Created_Ts], 101) + ':' + ui.USERNAME + ']'
                        + ', '
                    FROM
                        dbo.Invoice_Collection_Queue_Exclude_Comment icqec
                        INNER JOIN dbo.USER_INFO ui
                            ON ui.USER_INFO_ID = icqec.Created_User_Id
                    WHERE
                        icqec.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                    GROUP BY
                        icqec.[Created_Ts]
                        , icqec.Comment_Desc
                        , ui.USERNAME
                    FOR XML PATH(''), TYPE).value('.', 'VARCHAR(MAX)') Exclude_Comments) Comments
        WHERE
            c.Code_Value = 'Excluded'
            AND icac.Is_Chase_Activated = 1
        GROUP BY
            ch.Client_Name
            , cha.Display_Account_Number
            , ISNULL(vd.Account_Vendor_Name, cha.Account_Vendor_Name)
            , ch.Site_name
            , ch.Country_Name
            , ch.State_Name
            , icq.Collection_Start_Dt
            , icq.Collection_End_Dt
            , icq.Last_Change_Ts
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME
            , ui_owner.FIRST_NAME + ' ' + ui_owner.LAST_NAME
            , ui_officer.FIRST_NAME + ' ' + ui_officer.LAST_NAME
            , CASE WHEN LEN(Comments.Exclude_Comments) > 0 THEN
                       LEFT(Comments.Exclude_Comments, LEN(Comments.Exclude_Comments) - 1)
                  ELSE Comments.Exclude_Comments
              END
            , icq.Invoice_Collection_Queue_Id;


    END;









GO
GRANT EXECUTE ON  [dbo].[Report_DE_Invoice_Collection_Excluded] TO [CBMSApplication]
GO
