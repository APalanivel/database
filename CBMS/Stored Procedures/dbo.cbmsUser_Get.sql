
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
  
NAME:  
 dbo.cbmsUser_Get  
  
DESCRIPTION:  
  
INPUT PARAMETERS:  
 Name             DataType    Default    Description  
------------------------------------------------------------  
 @User_Info_Id    int  
  
OUTPUT PARAMETERS:  
 Name             DataType     Default   Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
  
 EXEC dbo.CbmsUser_Get 24539  
 EXEC dbo.CbmsUser_Get 64  
 EXEC dbo.CbmsUser_Get 49  
 EXEC dbo.CbmsUser_Get 201
  
AUTHOR INITIALS:  
 Initials    Name  
------------------------------------------------------------  
 PNR         Pandarinath  
 NR          NARAYANA REDDY  
 TP			
  
MODIFICATIONS  
  
 Initials		Date		Modification  
------------------------------------------------------------  
				9/20/2010	Modify Quoted Identifier  
 PNR			11/09/2010	Selected last saved passcode value from user_passcode table instead user_info   
								and also removed unused myaccountid parameter.  
 PNR			11/15/2010	Removed Passcode column from the select list because as it is not required for the application.  
 NR				12/19/2013  For RA Admin ,added the new column Last_Login_Ts,Job_Title,COUNTRY_NAME,STATE_NAME,CELL_NUMBER.  
							Modified to return the columns from Single_signon_usermap
							Returning Is_Single_SignOn_Login column from Client
 NR				2016-02-24	CIP Integration - Added Notification_Type_Cd in Select list with Comma separted.
 TP			28-Jun-2017		MAINT-5464 - removed Signon_Provider_Cd column	
       
******/
CREATE PROCEDURE [dbo].[cbmsUser_Get] ( @user_info_id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Is_Corporate INT  
      
      SELECT
            @Is_Corporate = max(cast(sr.Is_Corporate AS INT))
      FROM
            dbo.User_Security_Role usr
            JOIN dbo.Security_Role sr
                  ON usr.Security_Role_Id = sr.Security_Role_Id
      WHERE
            usr.User_Info_Id = @user_info_id


      SELECT
            ui.user_info_id
           ,ui.username
           ,ui.queue_id
           ,ui.first_name
           ,ui.middle_name
           ,ui.last_name
           ,ui.FIRST_NAME + space(1) + ui.LAST_NAME full_name
           ,ui.email_address
           ,ui.is_history
           ,ui.access_level
           ,ui.client_id
           ,ui.division_id
           ,ui.site_id
           ,ui.phone_number
           ,ui.phone_number_ext
           ,ui.cell_number
           ,ui.fax_number
           ,ui.cem_homepage
           ,ui.locale_code
           ,ui.currency_unit_id
           ,ui.el_uom_id
           ,ui.ng_uom_id
           ,sol.ubm_id
           ,sol.ubm_username
           ,sol.ubm_password
           ,sol.dsm_username
           ,sol.dsm_password
           ,ui.address_1
           ,ui.address_2
           ,ui.city
           ,ui.state_id
           ,ui.zipcode
           ,ui.country_id
           ,ui.queue_id
           ,logins.Login_Ts AS Last_Login_Ts
           ,ui.Job_Title
           ,c.COUNTRY_NAME
           ,s.STATE_NAME
           ,ui.CELL_NUMBER
           ,ssoum.Provider_XUser_Name
           ,cl.Is_Single_SignOn_Login
           ,@Is_Corporate AS Is_Corporate
           ,left(uer.Notification_Type_Cd, len(uer.Notification_Type_Cd) - 1) AS Notification_Type_Cd
      FROM
            dbo.user_info ui
            LEFT OUTER JOIN dbo.Client cl
                  ON cl.CLIENT_ID = ui.Client_Id
            LEFT OUTER JOIN dbo.sso_other_logins sol
                  ON sol.user_info_id = ui.user_info_id
            LEFT OUTER JOIN dbo.Single_Signon_User_Map ssoum
                  ON ssoum.Internal_User_ID = @user_info_id
            LEFT OUTER JOIN ( SELECT
                                    max(rult.Login_Ts) Login_Ts
                                   ,rult.USER_INFO_ID
                              FROM
                                    dbo.RA_User_Login_Ts rult
                              GROUP BY
                                    rult.USER_INFO_ID ) AS logins
                  ON ui.USER_INFO_ID = logins.USER_INFO_ID
            LEFT OUTER  JOIN dbo.COUNTRY c
                  ON c.COUNTRY_ID = ui.country_id
            LEFT OUTER JOIN dbo.STATE s
                  ON ui.state_id = s.STATE_ID
            CROSS APPLY ( SELECT
                              cast(upn.Notification_Type_Cd AS VARCHAR(10)) + ','
                          FROM
                              dbo.User_Push_Notification upn
                          WHERE
                              upn.User_Info_Id = ui.User_Info_Id
            FOR
                          XML PATH('') ) uer ( Notification_Type_Cd )
      WHERE
            ui.user_info_id = @user_info_id    

END;


 
;


;


;
GO



GRANT EXECUTE ON  [dbo].[cbmsUser_Get] TO [CBMSApplication]
GO
