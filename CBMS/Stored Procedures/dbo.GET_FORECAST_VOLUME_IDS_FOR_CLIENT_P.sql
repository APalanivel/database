SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_FORECAST_VOLUME_IDS_FOR_CLIENT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(50)	          	
	@clientId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE    PROCEDURE dbo.GET_FORECAST_VOLUME_IDS_FOR_CLIENT_P
@userId varchar(10),
@sessionId varchar(50),
@clientId int

AS
set nocount on
select	RM_FORECAST_VOLUME_ID,
	CLIENT_ID,
	FORECAST_YEAR,
	FORECAST_AS_OF_DATE 
from	rm_forecast_volume 
where	client_id = @clientId
GO
GRANT EXECUTE ON  [dbo].[GET_FORECAST_VOLUME_IDS_FOR_CLIENT_P] TO [CBMSApplication]
GO
