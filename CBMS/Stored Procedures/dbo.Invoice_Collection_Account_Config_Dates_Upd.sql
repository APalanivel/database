SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
                           
 NAME:  [dbo].[Invoice_Collection_Account_Config_Dates_Upd]                        
                            
 DESCRIPTION:        
				IC tool to update the config dates                          
                            
 INPUT PARAMETERS:        
                           
 Name										 DataType        Default       Description        
---------------------------------------------------------------------------------------------------------------      
 @Invoice_Collection_Account_Config_Id		INT
 @Invoice_Collection_Service_Start_Dt		DATE
 @Invoice_Collection_Service_End_Dt			DATE
 @User_Info_Id								INT


 OUTPUT PARAMETERS:        
                                 
 Name                                   DataType        Default       Description        
---------------------------------------------------------------------------------------------------------------      
                            
 USAGE EXAMPLES:                                
---------------------------------------------------------------------------------------------------------------     
       
 Exec  dbo.Invoice_Collection_Account_Config_Dates_Upd 11 ,'2020-01-01','2020-05-01',49         
                           
 AUTHOR INITIALS:      
         
 Initials               Name        
---------------------------------------------------------------------------------------------------------------      
 NR                     Narayana Reddy                              
                             
 MODIFICATIONS:      
                             
 Initials               Date            Modification      
---------------------------------------------------------------------------------------------------------------      
 NR                     2020-05-11      Created for Small Enhancement.                         
                           
******/

CREATE PROC [dbo].[Invoice_Collection_Account_Config_Dates_Upd]
    (
        @Invoice_Collection_Account_Config_Id INT
        , @Invoice_Collection_Service_Start_Dt DATE
        , @Invoice_Collection_Service_End_Dt DATE
        , @User_Info_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;



        UPDATE
            icac
        SET
            icac.Invoice_Collection_Service_Start_Dt = @Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt = @Invoice_Collection_Service_End_Dt
            , icac.Updated_User_Id = @User_Info_Id
            , icac.Last_Change_Ts = GETDATE()
        FROM
            dbo.Invoice_Collection_Account_Config icac
        WHERE
            icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id;

    END;


GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Account_Config_Dates_Upd] TO [CBMSApplication]
GO
