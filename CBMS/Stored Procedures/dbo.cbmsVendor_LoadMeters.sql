SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******        
 NAME:dbo.cbmsVendor_LoadMeters   
       
 DESCRIPTION:         
           To get the details vendor details for the given Vendor_Id,  
       
 INPUT PARAMETERS:        
 Name					DataType	Default		Description        
 ------------------------------------------------------------          
  Vendor_Id				INT  
  @MyAccountId			INT     
 OUTPUT PARAMETERS:        
 Name					DataType	Default		Description        
 ------------------------------------------------------------        
       
 USAGE EXAMPLES:        
 ------------------------------------------------------------        
 
 EXEC  cbmsVendor_LoadMeters  1,49
 EXEC cbmsVendor_LoadMeters 19,19001
 
 AUTHOR INITIALS:      
 Initials Name      
------------------------------------------------------------      
 RKV    Ravi Kumar Vegesna    
      
 MODIFICATIONS      
 Initials Date  Modification      
------------------------------------------------------------      
     
 RKV   2014-01-20 Comments Header added , MAINT-2487 Added Column has_Account_Group, returns 1 if the vendor have group accounts  
				                                     and returns 0 if the vendor doesnot have group accounts 
 RKV   2018-06-22 Maint-7400 Added inner join Account to Account_group to filter the account_group having accounts
      
******/

CREATE PROCEDURE [dbo].[cbmsVendor_LoadMeters]
    (
        @MyAccountId INT
        , @VendorId INT
    )
AS
    BEGIN
        SET NOCOUNT ON;
        SELECT
            v.VENDOR_ID
            , v.VENDOR_NAME
            , r.RATE_ID
            , r.RATE_NAME
            , m.METER_NUMBER
            , a.ACCOUNT_NUMBER
            , MAX(CASE WHEN rm.VENDOR_ID IS NULL THEN 0
                      ELSE 1
                  END) AS has_deal_ticket
            , MAX(CASE WHEN ag.VENDOR_ID IS NULL THEN 0
                      ELSE 1
                  END) AS has_account_group
        FROM
            dbo.VENDOR v
            LEFT OUTER JOIN dbo.RATE r
                ON r.VENDOR_ID = v.VENDOR_ID
            LEFT OUTER JOIN(dbo.METER m
                            INNER JOIN dbo.ACCOUNT a
                                ON a.ACCOUNT_ID = m.ACCOUNT_ID)
                ON m.RATE_ID = r.RATE_ID
            LEFT OUTER JOIN dbo.RM_DEAL_TICKET rm
                ON rm.VENDOR_ID = v.VENDOR_ID
            LEFT OUTER JOIN(dbo.ACCOUNT_GROUP ag
                            INNER JOIN dbo.ACCOUNT acc
                                ON acc.ACCOUNT_GROUP_ID = ag.ACCOUNT_GROUP_ID)
                ON ag.VENDOR_ID = v.VENDOR_ID
        WHERE
            v.VENDOR_ID = @VendorId
            AND v.VENDOR_TYPE_ID = 289
        GROUP BY
            v.VENDOR_ID
            , v.VENDOR_NAME
            , r.RATE_ID
            , r.RATE_NAME
            , m.METER_NUMBER
            , a.ACCOUNT_NUMBER
        ORDER BY
            r.RATE_NAME
            , m.METER_NUMBER
            , a.ACCOUNT_NUMBER;
    END;
GO

GRANT EXECUTE ON  [dbo].[cbmsVendor_LoadMeters] TO [CBMSApplication]
GO
