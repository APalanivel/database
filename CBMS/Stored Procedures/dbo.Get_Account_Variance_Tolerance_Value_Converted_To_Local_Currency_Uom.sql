
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********
NAME:  dbo.Get_Account_Variance_Tolerance_Value_Converted_To_Local_Currency_Uom

DESCRIPTION:
	This sproc is used to get the account tolerance value convert the master consumption level tolerance values to local UOM/Currency unit

INPUT PARAMETERS:
Name							DataType          Default     Description
-------------------------------------------------------------------------
@Variance_Consumption_Level_Id  int
@Account_id						int

OUTPUT PARAMETERS:
      Name              DataType          Default     Description
-------------------------------------------------------------------------

USAGE EXAMPLES:

-- Canada

	DECLARE @Account_Variance_Rule_Dtl_tvp Account_Variance_Rule_Dtl 

	INSERT      INTO @Account_Variance_Rule_Dtl_tvp
	VALUES
				( 750394, 1421 )

	EXEC Get_Account_Variance_Tolerance_Value_Converted_To_Local_Currency_Uom
		  @Account_Variance_Rule_Dtl_tvp = @Account_Variance_Rule_Dtl_tvp

---- USA 
	DECLARE @Account_Variance_Rule_Dtl_tvp Account_Variance_Rule_Dtl 

	INSERT      INTO @Account_Variance_Rule_Dtl_tvp
	VALUES
				( 7347, 1465 )

	EXEC Get_Account_Variance_Tolerance_Value_Converted_To_Local_Currency_Uom
		  @Account_Variance_Rule_Dtl_tvp = @Account_Variance_Rule_Dtl_tvp

-- Late Fees, Unit cost

DECLARE @Account_Variance_Rule_Dtl_tvp Account_Variance_Rule_Dtl 

INSERT      INTO @Account_Variance_Rule_Dtl_tvp
VALUES
            ( 1148742, 157)
            ,(1148757,144)
            
 --------
DECLARE @Account_Variance_Rule_Dtl_tvp Account_Variance_Rule_Dtl; 
EXEC Get_Account_Variance_Tolerance_Value_Converted_To_Local_Currency_Uom
      @Account_Variance_Rule_Dtl_tvp = @Account_Variance_Rule_Dtl_tvp


    INSERT  INTO @Account_Variance_Rule_Dtl_tvp
            SELECT
                  vrdao.ACCOUNT_ID
                 ,vrd.Variance_Rule_Dtl_Id
            FROM
                  dbo.Variance_Rule_Dtl_Account_Override vrdao
                  LEFT JOIN dbo.Variance_Rule_Dtl vrd
                        ON vrd.Variance_Rule_Dtl_Id = vrdao.Variance_Rule_Dtl_Id
            WHERE
                  ACCOUNT_ID = 1111046;

    EXEC Get_Account_Variance_Tolerance_Value_Converted_To_Local_Currency_Uom
      @Account_Variance_Rule_Dtl_tvp = @Account_Variance_Rule_Dtl_tvp;

-------------------------------------------------------------------------
AUTHOR INITIALS:
	Initials	Name
-------------------------------------------------------------------------
	HG			Harihara Suthan Ganesan

	Initials	Date		Modification
-------------------------------------------------------------------------
	HG			2016-12-02	Created for Variance test enhancement
	HG			2017-01-09	MAINT-4813, script which converts the different parameter values( cost/unit cost/usage) to INT/DECIMAL caused this issue (which was added earlier to get the correct description,since it was not used now removed it)
								, changed it to convert all the values to decimal
******/
CREATE PROCEDURE [dbo].[Get_Account_Variance_Tolerance_Value_Converted_To_Local_Currency_Uom]
      (
       @Account_Variance_Rule_Dtl_tvp AS Account_Variance_Rule_Dtl READONLY )
AS
BEGIN

      SET NOCOUNT ON;

      CREATE TABLE #Account_Rule_Tolerance
            (
             Account_Id INT
            ,Country_Name VARCHAR(200)
            ,Commodity_Id INT
            ,Is_DEO BIT
            ,Variance_Rule_Dtl_Id INT
            ,Baseline_Desc VARCHAR(25)
            ,Test_Description VARCHAR(MAX)
            ,Variance_Category VARCHAR(200)
            ,Variance_Parameter VARCHAR(200)
            ,Default_Tolerance DECIMAL(16, 4)
            ,Default_Test_Description VARCHAR(MAX)
            ,Parameter_Type VARCHAR(20)
            ,Default_Uom_Type_Id INT
            ,Currency_Unit_Id INT
            ,Site_Country_Uom_Type_Id INT
            ,Site_Country_Currency_Id INT
            ,Client_Currency_Group_Id INT
            ,Converted_Uom VARCHAR(200)
            ,Converted_Currency VARCHAR(200)
            ,New_Tolerance_Value DECIMAL(16, 4)
            ,New_Test_Description VARCHAR(255)
            ,PRIMARY KEY CLUSTERED ( Account_Id, Variance_Rule_Dtl_Id ) );

      DECLARE
            @USD_Currency_Unit_Id INT
           ,@Current_Month DATE;
		
      SELECT
            @Current_Month = CAST(DATEADD(DAY, -DATEPART(DAY, GETDATE()) + 1, GETDATE()) AS DATE);

      SELECT
            @USD_Currency_Unit_Id = CURRENCY_UNIT_ID
      FROM
            dbo.CURRENCY_UNIT
      WHERE
            CURRENCY_UNIT_NAME = 'USD';

      DECLARE @CHA TABLE
            (
             Account_Id INT
            ,Variance_Rule_Dtl_Id INT
            ,Commodity_Id INT
            ,Account_Is_Data_Entry_Only BIT
            ,Client_Currency_Group_Id INT
            ,Country_Id INT
            ,Default_Uom_Type_Id INT
            ,Currency_Unit_Id INT
            ,Site_Country_Uom_Type_Id INT
            ,Site_Country_Currency_Id INT
            ,Converted_Uom VARCHAR(200)
            ,Converted_Currency VARCHAR(200)
            ,PRIMARY KEY CLUSTERED ( Account_Id, Variance_Rule_Dtl_Id ) );

      INSERT      INTO @CHA
                  (Account_Id
                  ,Variance_Rule_Dtl_Id
                  ,Account_Is_Data_Entry_Only
                  ,Client_Currency_Group_Id
                  ,Country_Id )
                  SELECT
                        cha.Account_Id
                       ,rdt.Variance_Rule_Dtl_Id
                       ,cha.Account_Is_Data_Entry_Only
                       ,ch.Client_Currency_Group_Id
                       ,MIN(ch.Country_Id) AS Country_Id
                  FROM
                        Core.Client_Hier_Account cha
                        INNER JOIN Core.Client_Hier ch
                              ON ch.Client_Hier_Id = cha.Client_Hier_Id
                        INNER JOIN @Account_Variance_Rule_Dtl_tvp rdt
                              ON rdt.Account_Id = cha.Account_Id
                  GROUP BY
                        cha.Account_Id
                       ,rdt.Variance_Rule_Dtl_Id
                       ,cha.Account_Is_Data_Entry_Only
                       ,ch.Client_Currency_Group_Id;
  
      UPDATE
            cha
      SET
            Commodity_Id = com.Commodity_Id
           ,Default_Uom_Type_Id = com.Default_UOM_Entity_Type_Id
           ,cha.Site_Country_Uom_Type_Id = ccu.Default_Uom_Type_Id
           ,cha.Site_Country_Currency_Id = cu.CURRENCY_UNIT_ID
           ,Converted_Uom = convuom.ENTITY_NAME
           ,Converted_Currency = cu.CURRENCY_UNIT_NAME
      FROM
            @CHA cha
            INNER JOIN dbo.Variance_Rule_Dtl vrd
                  ON vrd.Variance_Rule_Dtl_Id = cha.Variance_Rule_Dtl_Id
            INNER JOIN dbo.Variance_Consumption_Level vcl
                  ON vcl.Variance_Consumption_Level_Id = vrd.Variance_Consumption_Level_Id
            INNER JOIN dbo.Commodity com
                  ON com.Commodity_Id = vcl.Commodity_Id
            LEFT JOIN dbo.Country_Commodity_Uom ccu
                  ON ccu.Commodity_Id = com.Commodity_Id
                     AND ccu.COUNTRY_ID = cha.Country_Id
            LEFT JOIN dbo.CURRENCY_UNIT_COUNTRY_MAP cucm
                  ON cucm.COUNTRY_ID = cha.Country_Id
            LEFT JOIN dbo.ENTITY convuom
                  ON convuom.ENTITY_ID = ccu.Default_Uom_Type_Id
            LEFT JOIN dbo.CURRENCY_UNIT cu
                  ON cu.CURRENCY_UNIT_ID = cucm.CURRENCY_UNIT_ID;

      INSERT      INTO #Account_Rule_Tolerance
                  (Account_Id
                  ,Country_Name
                  ,Commodity_Id
                  ,Is_DEO
                  ,Variance_Rule_Dtl_Id
                  ,Baseline_Desc
                  ,Test_Description
                  ,Variance_Category
                  ,Variance_Parameter
                  ,Default_Tolerance
                  ,Default_Test_Description
                  ,Parameter_Type
                  ,Default_Uom_Type_Id
                  ,Currency_Unit_Id
                  ,Site_Country_Uom_Type_Id
                  ,Site_Country_Currency_Id
                  ,Client_Currency_Group_Id
                  ,Converted_Uom
                  ,Converted_Currency )
                  SELECT
                        cha.Account_Id
                       ,NULL
                       ,cha.Commodity_Id
                       ,ISNULL(cha.Account_Is_Data_Entry_Only, 0)
                       ,vrd.Variance_Rule_Dtl_Id
                       ,bl.Code_Value AS Baseline_Desc
                       ,vrd.Test_Description
                       ,vc.Code_Value AS Variance_Category
                       ,vp.Parameter
                       ,CASE WHEN bl.Code_Value = 'Absolute Value'
                                  AND vc.Code_Value <> 'Load Factor' THEN vrd.Default_Tolerance -- Only for the absolute value tolerance would be converted to local uom otherwise it would be saved as null only
                             ELSE NULL
                        END AS Default_Tolerance
                       ,CASE WHEN bl.Code_Value = 'Absolute Value'
                                  AND vc.Code_Value <> 'Load Factor' THEN vrd.Test_Description -- Only for the absolute value tolerance desc would be updated
                             ELSE NULL
                        END AS Default_Test_Description
                       ,CASE WHEN vp.Parameter IN ( 'Late Fees', 'Total Cost' ) THEN 'Cost'
                             WHEN vp.Parameter IN ( 'Unit Cost' ) THEN 'Unit Cost'
                             ELSE 'Usage'
                        END Parameter_Type
                       ,cha.Default_Uom_Type_Id
                       ,@USD_Currency_Unit_Id AS Default_Currency_Unit_Id
                       ,cha.Site_Country_Uom_Type_Id
                       ,cha.Site_Country_Currency_Id
                       ,cha.Client_Currency_Group_Id
                       ,cha.Converted_Uom
                       ,cha.Converted_Currency
                  FROM
                        @CHA cha
                        INNER JOIN dbo.Variance_Rule_Dtl_Account_Override vrdao
                              ON vrdao.ACCOUNT_ID = cha.Account_Id
                                 AND vrdao.Variance_Rule_Dtl_Id = cha.Variance_Rule_Dtl_Id
                        INNER JOIN dbo.Variance_Rule_Dtl vrd
                              ON vrd.Variance_Rule_Dtl_Id = vrdao.Variance_Rule_Dtl_Id
                        INNER JOIN dbo.Variance_Rule vr
                              ON vr.Variance_Rule_Id = vrd.Variance_Rule_Id
                        INNER JOIN dbo.Variance_Parameter_Baseline_Map vpbm
                              ON vpbm.Variance_Baseline_Id = vr.Variance_Baseline_Id
                        INNER JOIN dbo.Code bl
                              ON bl.Code_Id = vpbm.Baseline_Cd
                        INNER JOIN dbo.Variance_Parameter vp
                              ON vp.Variance_Parameter_Id = vpbm.Variance_Parameter_Id
                        INNER JOIN dbo.Code vc
                              ON vc.Code_Id = vp.Variance_Category_Cd;
      UPDATE
            ua
      SET
            New_Tolerance_Value = CASE WHEN ua.Parameter_Type = 'Cost' THEN CAST(ua.Default_Tolerance * cuc.CONVERSION_FACTOR AS DECIMAL(16, 4))
                                       WHEN ua.Parameter_Type = 'Unit Cost' THEN CAST(ROUND(( ua.Default_Tolerance * cuc.CONVERSION_FACTOR ) / cnconv.CONVERSION_FACTOR, 3) AS DECIMAL(16, 4))
                                       WHEN ua.Parameter_Type = 'Usage' THEN CAST(ua.Default_Tolerance * cnconv.CONVERSION_FACTOR AS DECIMAL(16, 4))
                                  END
      FROM
            #Account_Rule_Tolerance ua
            LEFT JOIN dbo.Commodity com
                  ON com.Commodity_Id = ua.Commodity_Id
            LEFT JOIN dbo.CURRENCY_UNIT_CONVERSION cuc
                  ON cuc.CURRENCY_GROUP_ID = ua.Client_Currency_Group_Id
                     AND cuc.BASE_UNIT_ID = ua.Currency_Unit_Id
                     AND cuc.CONVERTED_UNIT_ID = ua.Site_Country_Currency_Id
                     AND cuc.CONVERSION_DATE = @Current_Month
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cnconv
                  ON cnconv.BASE_UNIT_ID = ua.Default_Uom_Type_Id
                     AND cnconv.CONVERTED_UNIT_ID = ua.Site_Country_Uom_Type_Id

      SELECT
            ua.Variance_Rule_Dtl_Id
           ,ua.Account_Id
           ,ua.New_Tolerance_Value
           ,REPLACE(REPLACE(REPLACE(ua.Default_Test_Description, '$' + CASE WHEN ua.Default_Tolerance < 1 THEN CAST(CAST(ua.Default_Tolerance AS DECIMAL(16, 2)) AS VARCHAR)
                                                                            ELSE ''
                                                                       END + CASE WHEN ua.Default_Tolerance > 1 THEN REPLACE(CAST(CONVERT(VARCHAR, CAST(CAST(ua.Default_Tolerance AS DECIMAL(16, 4)) AS MONEY), 1) AS VARCHAR), '.00', '')
                                                                                  ELSE ''
                                                                             END--
                                                                                                                                      , CASE WHEN ua.New_Tolerance_Value < 1 THEN CAST(CAST(ua.New_Tolerance_Value AS DECIMAL(16, 2)) AS VARCHAR)
                                                                                                                                             ELSE ''
                                                                                                                                        END + CASE WHEN ua.New_Tolerance_Value > 1 THEN REPLACE(CAST(CONVERT(VARCHAR, CAST(CAST(ua.New_Tolerance_Value AS DECIMAL(16, 4)) AS MONEY), 1) AS VARCHAR), '.00', '')
                                                                                                                                                   ELSE ''
                                                                                                                                              END)-- New
                                                                                                                                              , '$' + CASE WHEN ua.Default_Tolerance < 1 THEN CAST(CAST(ua.Default_Tolerance AS DECIMAL(16, 3)) AS VARCHAR)
                                                                                                                                                           ELSE ''
                                                                                                                                                      END + CASE WHEN ua.Default_Tolerance > 1 THEN REPLACE(CAST(CONVERT(VARCHAR, CAST(ua.Default_Tolerance AS INT), 1) AS VARCHAR), '.00', '')
                                                                                                                                                                 ELSE ''
                                                                                                                                                            END--
                                                                                                                                      , CASE WHEN ua.New_Tolerance_Value < 1 THEN CAST(CAST(ua.New_Tolerance_Value AS DECIMAL(16, 3)) AS VARCHAR)
                                                                                                                                             ELSE ''
                                                                                                                                        END + CASE WHEN ua.New_Tolerance_Value > 1 THEN REPLACE(CAST(CONVERT(VARCHAR, CAST(CAST(ua.New_Tolerance_Value AS DECIMAL(16, 4)) AS MONEY), 1) AS VARCHAR), '.00', '')
                                                                                                                                                   ELSE ''
                                                                                                                                              END), defuom.ENTITY_NAME, SiteCntryUom.ENTITY_NAME) AS New_Test_Description
      FROM
            #Account_Rule_Tolerance ua
            LEFT JOIN dbo.ENTITY defuom
                  ON defuom.ENTITY_ID = ua.Default_Uom_Type_Id
            LEFT JOIN dbo.ENTITY SiteCntryUom
                  ON SiteCntryUom.ENTITY_ID = ua.Site_Country_Uom_Type_Id;

      DROP TABLE #Account_Rule_Tolerance;

END;
;
GO

GRANT EXECUTE ON  [dbo].[Get_Account_Variance_Tolerance_Value_Converted_To_Local_Currency_Uom] TO [CBMSApplication]
GO
