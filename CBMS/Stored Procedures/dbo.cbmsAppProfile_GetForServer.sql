SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE     PROCEDURE [dbo].[cbmsAppProfile_GetForServer]
	( @app_server_name varchar(50)
	, @app_server_ip varchar(50)
	)
AS
BEGIN

	   select s.app_server_id
		, p.app_profile_id
		, p.app_profile_name
		, p.app_menu_profile_id
		, p.app_env
		, p.app_logon_url
		, p.app_logout_url
		, p.app_overview_url
		, p.app_cbms_server
		, p.app_int_server
		, p.app_logon_server
		, p.app_allow_manage
		, p.app_is_secure
	     from app_server s
	     join app_profile p on p.app_profile_id = s.app_profile_id
	    where s.app_server_name = @app_server_name
	      and s.app_server_ip = @app_server_ip

END



GO
GRANT EXECUTE ON  [dbo].[cbmsAppProfile_GetForServer] TO [CBMSApplication]
GO
