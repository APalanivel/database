SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_COUNTERPARTY_UNDER_SITE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@siteId        	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE dbo.GET_COUNTERPARTY_UNDER_SITE_P 
@userId varchar,
@sessionId varchar,
@siteId int
as
set nocount on
select	distinct rm_counterparty.rm_counterparty_id,
	counterparty_name, 1 as is_counter_party

from	site,
	RM_ONBOARD_HEDGE_SETUP,
	rm_counterparty,
	rm_deal_ticket,
	division

where 	rm_deal_ticket.client_id = division.client_id AND
	division.division_id = site.division_id AND
	site.site_id = @siteId AND 
	RM_ONBOARD_HEDGE_SETUP.site_id = @siteId AND
	RM_ONBOARD_HEDGE_SETUP.INCLUDE_IN_REPORTS=1 AND
	rm_deal_ticket.rm_counterparty_id = rm_counterparty.rm_counterparty_id

UNION

select	distinct vendor.vendor_id as counterparrty_id,
	vendor_name as counterparty_name, 0 as is_counter_party 

from	rm_deal_ticket,division,RM_ONBOARD_HEDGE_SETUP,
	vendor, site

where 	rm_deal_ticket.client_id = division.client_id AND
	division.division_id =  site.division_id AND
	site.site_id = @siteId AND 
	RM_ONBOARD_HEDGE_SETUP.site_id = @siteId AND
	RM_ONBOARD_HEDGE_SETUP.INCLUDE_IN_REPORTS=1 AND
	rm_deal_ticket.vendor_id = vendor.vendor_id

order by counterparty_name
GO
GRANT EXECUTE ON  [dbo].[GET_COUNTERPARTY_UNDER_SITE_P] TO [CBMSApplication]
GO
