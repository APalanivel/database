SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[FTS_String_Parser]
           
DESCRIPTION:             
			To get the full text search parsed string

INPUT PARAMETERS:            
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  
	@Object_Name						NVARCHAR(255)				name of the table for which FT is enabled
    @Keyword							NVARCHAR(max)				special characters shoudl be removed before passing it to this sproc
OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
		
		EXEC dbo.FTS_String_Parser @Object_Name = 'Cbms_Image', 

 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	HG			Hariharasuthan Ganesan

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-03-02	Global Sourcing - Phase3 - GCS-478 Created
******/
CREATE PROCEDURE [dbo].[FTS_String_Parser]
      (
       @Object_Name NVARCHAR(255)
      ,@Keyword NVARCHAR(MAX) )
      WITH EXECUTE AS CALLER
AS
BEGIN
	
      SET NOCOUNT ON;
      DECLARE
            @Language_Id INT
           ,@Stoplist_Id INT;

      SELECT
            @Language_Id = fic.language_id
           ,@Stoplist_Id = fi.stoplist_id
      FROM
            sys.fulltext_indexes fi
            INNER JOIN sys.fulltext_index_columns fic
                  ON fi.object_id = fic.object_id
            INNER JOIN sys.fulltext_catalogs cat
                  ON fi.fulltext_catalog_id = cat.fulltext_catalog_id
      WHERE
            OBJECT_NAME(fic.object_id) = @Object_Name
      GROUP BY
            fi.stoplist_id
           ,fic.language_id;

      SELECT
            display_term AS Parsed_String
      FROM
            sys.dm_fts_parser(@Keyword, @Language_Id, @Stoplist_Id, 0)
      WHERE
            display_term NOT IN ( '(', ')' ); 
END;
;
GO
GRANT EXECUTE ON  [dbo].[FTS_String_Parser] TO [CBMSApplication]
GO
