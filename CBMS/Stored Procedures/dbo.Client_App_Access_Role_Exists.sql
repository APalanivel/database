SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                 
/******                  
Name:                  
        dbo.Client_App_Access_Role_Exists                    
                  
Description:                  
			To Check weather Client_App_Access_Role exists or not .                  
                  
 Input Parameters:                  
 Name                                        DataType        Default        Description                    
--------------------------------------------------------------------------------------------------                   
 @Client_App_Access_Role_Id_List			 VARCHAR(MAX) 
                  
 Output Parameters:                        
 Name                                        DataType        Default        Description                    
--------------------------------------------------------------------------------------------------                   
                  
 Usage Examples:                      
--------------------------------------------------------------------------------------------------                   

	 EXEC dbo.Client_App_Access_Role_Exists 123
                 
 Author Initials:                  
  Initials        Name                  
--------------------------------------------------------------------------------------------------                   
  SP              Sandeep Pigilam    
                   
 Modifications:                  
  Initials        Date              Modification                  
--------------------------------------------------------------------------------------------------                   
  SP              2014-01-21		Created for RA Admin user management Bulk Upload                
                 
******/                  
                
CREATE PROCEDURE dbo.Client_App_Access_Role_Exists
      ( 
       @Client_App_Access_Role_Id_List VARCHAR(MAX) )
AS 
BEGIN                
      SET NOCOUNT ON;     
                
      DECLARE @Client_App_Access_Role_Ids TABLE
            ( 
             Client_App_Access_Role_Id INT )              
              
              
      INSERT      INTO @Client_App_Access_Role_Ids
                  ( 
                   Client_App_Access_Role_Id )
                  SELECT
                        ufn.Segments
                  FROM
                        dbo.ufn_split(@Client_App_Access_Role_Id_List, ',') ufn  
                        

      SELECT
            MAX(CASE WHEN gi.Client_App_Access_Role_Id IS NULL THEN 1
                     ELSE 0
                END) Is_Client_App_Access_Role_Missing
      FROM
            @Client_App_Access_Role_Ids te
            LEFT OUTER JOIN dbo.Client_App_Access_Role gi
                  ON gi.Client_App_Access_Role_Id = te.Client_App_Access_Role_Id
      GROUP BY
            Te.Client_App_Access_Role_Id
                
                                                 
                       
END 


;
GO
GRANT EXECUTE ON  [dbo].[Client_App_Access_Role_Exists] TO [CBMSApplication]
GO
