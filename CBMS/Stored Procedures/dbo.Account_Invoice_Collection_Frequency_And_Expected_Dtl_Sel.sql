SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                 
/******                        
 NAME: dbo.Account_Invoice_Collection_Frequency_And_Expected_Dtl_Sel            
                        
 DESCRIPTION:                        
			To get the details of invoice collection account level Frequency_And_Expected_Dtl_Sel config             
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@Invoice_Collection_Account_Config_Id	INT		NULL 
                              
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
    

  EXEC dbo.Account_Invoice_Collection_Frequency_And_Expected_Dtl_Sel 
      @Invoice_Collection_Account_Config_Id = 1342210
      

                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2016-12-08       Created   for Invoice tracking             
 RKV				   2019-10-18		Added two new columns Custom_Days,Custom_Invoice_Received_Day
                          
******/                 
                
CREATE PROCEDURE [dbo].[Account_Invoice_Collection_Frequency_And_Expected_Dtl_Sel]
      ( 
       @Invoice_Collection_Account_Config_Id INT )
AS 
BEGIN                
      SET NOCOUNT ON;    
     
      SELECT
            Account_Invoice_Collection_Frequency_Id as Account_Invoice_Collection_Frequency_And_Expected_Dtl_Id
           ,Seq_No
           ,Is_Primary
           ,Invoice_Frequency_Cd
           ,Invoice_Frequency_Pattern_Cd
           ,Expected_Invoice_Raised_Day
           ,Expected_Invoice_Received_Day
           ,Expected_Invoice_Raised_Month
           ,Expected_Invoice_Received_Month
           ,Expected_Invoice_Raised_Day_Lag
           ,Expected_Invoice_Received_Day_Lag
		   ,icac.Custom_Days
		   ,icac.Custom_Invoice_Received_Day
      FROM
            dbo.Account_Invoice_Collection_Frequency icac
      WHERE
            Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
     

END 

;
;

GO
GRANT EXECUTE ON  [dbo].[Account_Invoice_Collection_Frequency_And_Expected_Dtl_Sel] TO [CBMSApplication]
GO
