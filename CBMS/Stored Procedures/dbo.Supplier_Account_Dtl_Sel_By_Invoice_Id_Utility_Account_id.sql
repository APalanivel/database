SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
  dbo.[Supplier_Account_Dtl_Sel_By_Invoice_Id_Utility_Account_id]

DESCRIPTION:   

INPUT PARAMETERS:  

Name			DataType			Default				Description  
-------------------------------------------------------------------------  
@Account_id     INT                     
@Cu_Invoice_Id  INT 
 
OUTPUT PARAMETERS:  
Name			DataType			Default				Description  
-------------------------------------------------------------------------  

USAGE EXAMPLES:  
-------------------------------------------------------------------------  

EXEC [Supplier_Account_Dtl_Sel_By_Invoice_Id_Utility_Account_id] 37557,35237298

AUTHOR INITIALS: 
 
 Initials	Name  
-------------------------------------------------------------------------  
 RKV		Ravi Kumar Vegesna
 NR			Narayana Reddy
 
 MODIFICATIONS:
 Initials		Date			Modification  
------------------------------------------------------------  
 RKV			2015-10-06		Created For AS400.
 NR				2019-04-03		Data2.0 - Removed WATCHLIST-GROUP-INFO-ID  column from Account Table.		

  
						
******/

CREATE PROCEDURE [dbo].[Supplier_Account_Dtl_Sel_By_Invoice_Id_Utility_Account_id]
    (
        @Account_id INT
        , @Cu_Invoice_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            scha.Account_Id
            , scha.Meter_Id
            , scha.Account_Type
            , e.ENTITY_ID AS Account_Type_Id
            , scha.Account_Number
            , scha.Account_Group_ID
            , ch.Site_Id
            , scha.Account_Vendor_Id
            , scha.Supplier_Contract_ID
            , scha.Supplier_Account_begin_Dt
            , scha.Supplier_Account_End_Dt
            , scha.Account_Invoice_Source_Cd
            , scha.Supplier_Account_Recalc_Type_Cd
            , scha.Account_Service_level_Cd
            , scha.Meter_Address_ID
            , scha.Account_Is_Data_Entry_Only
            , cd.Code_Value Recalc_Type
            , ch.Client_Id
            , ch.Client_Name
            , ch.Sitegroup_Id division_id
            , ch.Ubm_Service_Id
            , scha.Account_Vendor_Name
            , crt.ENTITY_NAME Contract_Recalc_Type
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN Core.Client_Hier_Account scha
                ON cha.Meter_Id = scha.Meter_Id
            INNER JOIN dbo.ENTITY e
                ON e.ENTITY_NAME = scha.Account_Type
            INNER JOIN Core.Client_Hier ch
                ON scha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.Account_ID = scha.Account_Id
            INNER JOIN dbo.CONTRACT c
                ON c.CONTRACT_ID = scha.Supplier_Contract_ID
            INNER JOIN dbo.ENTITY crt
                ON c.CONTRACT_RECALC_TYPE_ID = crt.ENTITY_ID
            LEFT OUTER JOIN dbo.Account_Commodity_Invoice_Recalc_Type acirt
                ON acirt.Account_Id = cha.Account_Id
                   AND  acirt.Commodity_ID = cha.Commodity_Id
            LEFT OUTER JOIN dbo.Code cd
                ON acirt.Invoice_Recalc_Type_Cd = cd.Code_Id
        WHERE
            cha.Account_Id = @Account_id
            AND cism.CU_INVOICE_ID = @Cu_Invoice_Id
            AND cha.Account_Type = 'utility'
            AND scha.Account_Type = 'Supplier'
            AND e.ENTITY_TYPE = 111
            AND cism.SERVICE_MONTH BETWEEN scha.Supplier_Account_begin_Dt
                                   AND     scha.Supplier_Account_End_Dt
        GROUP BY
            scha.Account_Id
            , scha.Meter_Id
            , scha.Account_Type
            , scha.Account_Number
            , scha.Account_Group_ID
            , ch.Site_Id
            , scha.Account_Vendor_Id
            , scha.Supplier_Contract_ID
            , scha.Supplier_Account_begin_Dt
            , scha.Supplier_Account_End_Dt
            , scha.Account_Invoice_Source_Cd
            , scha.Supplier_Account_Recalc_Type_Cd
            , scha.Account_Service_level_Cd
            , scha.Meter_Address_ID
            , scha.Account_Is_Data_Entry_Only
            , cd.Code_Value
            , ch.Client_Id
            , ch.Client_Name
            , ch.Sitegroup_Id
            , ch.Ubm_Service_Id
            , scha.Account_Vendor_Name
            , e.ENTITY_ID
            , crt.ENTITY_NAME;




    END;
    ;

GO
GRANT EXECUTE ON  [dbo].[Supplier_Account_Dtl_Sel_By_Invoice_Id_Utility_Account_id] TO [CBMSApplication]
GO
