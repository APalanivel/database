SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                    
Name:   dbo.Invoice_Collection_Records_Queue_Sel                             
                                    
Description:                                    
   This sproc to get the Invoice Collection Records for the given filters                           
                                                 
 Input Parameters:                                    
    Name         DataType       Default   Description                                      
-----------------------------------------------------------------------------------------------------------------                                      
 @tvp_Invoice_Collection_Queue_Sel  tvp_Invoice_Collection_Queue_Sel                                            
    @Tvp_Invoice_Collection_Sources         Tvp_Invoice_Collection_Sources                      
    @Start_Index       INT                      
    @End_Index        INT                      
    @Total_Count       INT                      
                          
 Output Parameters:                                          
    Name        DataType   Default   Description                                      
-----------------------------------------------------------------------------------------------------------------                                      
                                    
 Usage Examples:                                        
-----------------------------------------------------------------------------------------------------------------                                      
                      
   DECLARE          
      @tvp_Invoice_Collection_Queue_Sel tvp_Invoice_Collection_Queue_Sel          
     ,@Tvp_Invoice_Collection_Sources tvp_Invoice_Collection_Source                  
           
 INSERT     INTO @tvp_Invoice_Collection_Queue_Sel          
            ( ICO_User_Info_Id,ICR_Status_Cd )          
 VALUES          
            ( 71920,102349 )           
             
 EXEC dbo.Invoice_Collection_QueueIds_Sel        
      @tvp_Invoice_Collection_Queue_Sel          
     ,@Tvp_Invoice_Collection_Sources     
                  , @Sort_Col  = 'Date_In_Queue'     
                  ,@Start_Index = 1    
                  ,@End_Index = 150      
                                      
                                    
Author Initials:                                    
    Initials  Name                                    
----------------------------------------------------------------------------------------                                      
 RKV    Ravi Kumar Vegesna          
 PRV    Pramod Reddy V 
 SLP	Sri Lakshmi Pallikonda                  
 Modifications:                                   
    Initials        Date			 Modification                                    
----------------------------------------------------------------------------------------                                      
    SLP             20.09.2019       Created to get the QueueIDs details
******/  
CREATE PROCEDURE [dbo].[Invoice_Collection_QueueIds_Sel]  
(  
    @tvp_Invoice_Collection_Queue_Sel tvp_Invoice_Collection_Queue_Sel READONLY,  
    @Tvp_Invoice_Collection_Sources tvp_Invoice_Collection_Source READONLY,  
    @Start_Index INT = 1,  
    @End_Index INT = 2147483647,  
    @Total_Count INT = 0,  
    @Sort_Col VARCHAR(50) = 'Date_In_Queue',  
    @Sort_Order VARCHAR(15) = 'ASC',  
    @Is_Issue_Blocker BIT = NULL,  
    @Last_Action_Start_Date DATE = NULL,  
    @Last_Action_End_Date DATE = NULL,  
    @Chase_Count VARCHAR(MAX) = NULL,  
    @Next_Action_Start_Date DATE = NULL,  
    @Next_Action_End_Date DATE = NULL,  
    @Download_Count VARCHAR(MAX) = NULL,  
    @Final_Review BIT = NULL,  
    @Chase_Created_Start_Date DATE = NULL,  
    @Chase_Created_End_Date DATE = NULL  
)  
AS  
BEGIN  
  
    SET NOCOUNT ON;  
  
    DECLARE @Cnt_Of_Invoice_Collection_Source BIT = 0,  
            @Invoice_Collection_Type_Cd INT,  
            @Invoice_Collection_Issue_Status_Cd INT,  
            @Source_Type_Client INT,  
            @Source_Type_Account INT,  
            @Source_Type_Vendor INT,  
            @Contact_Level_Client INT,  
            @Contact_Level_Account INT,  
            @Contact_Level_Vendor INT,  
            @SQL_Statement NVARCHAR(MAX);  
  
    DECLARE @ICO_User_Info_Id INT = NULL,  
            @Collection_Start_Date DATE = NULL,  
            @Collection_End_Date DATE = NULL,  
            @Priority_Cd INT = NULL,  
            @Client_Id INT = NULL,  
            @Site_Id INT = NULL,  
            @Account_Id INT = NULL,  
            @Country_Id INT = NULL,  
            @State_Id INT = NULL,  
            @Commodity_Id INT = NULL,  
            @Vendor_Type CHAR(8) = NULL,  
            @Vendor_Id INT = NULL,  
            @Client_Contact_Info_Id INT = NULL,  
            @Account_Contact_Info_Id INT = NULL,  
            @Vendor_Contact_Info_Id INT = NULL,  
            @Issue_Status VARCHAR(25) = NULL,  
            @Previously_Chased BIT = NULL,  
            @Record_Type_Cd INT = NULL,  
            @ICR_Status_Cd INT = NULL,  
            @Days_Since_Last_Chased_From DATE = NULL,  
            @Days_Since_Last_Chased_To DATE = NULL,  
            @Record_Type_Cd_Value VARCHAR(25) = NULL;  
  
  
    DECLARE @Sql_Ic_Acc_Config_Select VARCHAR(MAX),  
            @Sql_Ic_Acc_Config_From VARCHAR(MAX),  
            @Sql_Ic_Acc_Config_Where VARCHAR(MAX),  
            @Sql_Ic_Acc_Config_Groupby VARCHAR(MAX),  
            @Sql_Ic_Acc_Config_STMT VARCHAR(MAX);  
  
  
    DECLARE @Sql_IVC_Queue_Select VARCHAR(MAX),  
            @Sql_IVC_Queue_From VARCHAR(MAX),  
            @Sql_IVC_Queue_Where VARCHAR(MAX),  
            @Sql_IVC_Queue_GroupBy VARCHAR(MAX),  
            @Sql_IVC_Stmt VARCHAR(MAX);  
  
    SELECT *  
    INTO #Tvp_Invoice_Collection_Sources  
    FROM @Tvp_Invoice_Collection_Sources;  
  
  
  
    SELECT @Cnt_Of_Invoice_Collection_Source = 1  
    FROM @Tvp_Invoice_Collection_Sources tvp;  
  
    SELECT @Invoice_Collection_Type_Cd = c.Code_Id  
    FROM dbo.Code c  
        INNER JOIN dbo.Codeset cs  
            ON c.Codeset_Id = cs.Codeset_Id  
    WHERE cs.Std_Column_Name = 'Invoice_Collection_Type_Cd'  
          AND c.Code_Value = 'ICR';  
  
  
    SELECT @Invoice_Collection_Issue_Status_Cd = c.Code_Id  
    FROM dbo.Code c  
        INNER JOIN dbo.Codeset cs  
            ON c.Codeset_Id = cs.Codeset_Id  
    WHERE cs.Std_Column_Name = 'Invoice_Collection_Chase_Status_Cd'  
          AND c.Code_Value = 'Open';  
  
    SELECT @Source_Type_Client = MAX(   CASE  
                                            WHEN c.Code_Value = 'Client Primary Contact' THEN  
                                                c.Code_Id  
                                        END  
                                    ),  
           @Source_Type_Account = MAX(   CASE  
                                             WHEN c.Code_Value = 'Account Primary Contact' THEN  
                                                 c.Code_Id  
                                         END  
                                     ),  
           @Source_Type_Vendor = MAX(   CASE  
                                            WHEN c.Code_Value = 'Vendor Primary Contact' THEN  
                                                c.Code_Id  
                                        END  
                                    )  
    FROM dbo.Code c  
        INNER JOIN dbo.Codeset cs  
            ON c.Codeset_Id = cs.Codeset_Id  
    WHERE cs.Codeset_Name = 'InvoiceSourceType'  
          AND c.Code_Value IN ( 'Account Primary Contact', 'Client Primary Contact', 'Vendor Primary Contact' );  
  
  
    SELECT @Contact_Level_Client = MAX(   CASE  
                                              WHEN c.Code_Value = 'Client' THEN  
                                                  c.Code_Id  
                                          END  
                                      ),  
           @Contact_Level_Account = MAX(   CASE  
                                               WHEN c.Code_Value = 'Account' THEN  
                                                   c.Code_Id  
   END  
                                       ),  
           @Contact_Level_Vendor = MAX(   CASE  
                                              WHEN c.Code_Value = 'Vendor' THEN  
                                                  c.Code_Id  
                                          END  
                                      )  
    FROM dbo.Code c  
        INNER JOIN dbo.Codeset cs  
            ON c.Codeset_Id = cs.Codeset_Id  
    WHERE cs.Codeset_Name = 'ContactLevel';  
  
  
    CREATE TABLE #Invoice_Collection_Account_Config  
    (  
        Invoice_Collection_Account_Config_Id INT  
    );  
  
  
    CREATE TABLE #Vendor_Dtls  
    (  
        Account_Vendor_Name VARCHAR(200),  
        Account_Vendor_Type CHAR(8),  
        Account_Vendor_Id INT,  
        Invoice_Collection_Account_Config_Id INT  
    );  
  
    CREATE TABLE #Contact_Dtls  
    (  
        Invoice_Collection_Account_Config_Id INT,  
        Invoice_Collection_Source_Cd INT,  
        Invoice_Source_Type_Cd INT,  
        Invoice_Source_Method_of_Contact_Cd INT,  
        Contact_Level_Cd INT,  
        Contact_Info_Id INT  
    );  
  
    CREATE TABLE #Contact_Dtls_For_search  
    (  
        Invoice_Collection_Account_Config_Id INT,  
        Invoice_Collection_Source_Cd INT,  
        Invoice_Source_Type_Cd INT,  
        Invoice_Source_Method_of_Contact_Cd INT,  
        Contact_Level_Cd INT,  
        Contact_Info_Id INT  
    );  
  
    -------Added for MAINT-9156 changes            
  
    CREATE TABLE #Invoice_Collection_Accounts  
    (  
        Invoice_Collection_Account_Config_Id INT,  
        Account_Id INT,  
        Invoice_Collection_Service_End_Dt DATE,  
        Account_Vendor_Name VARCHAR(200),  
        Account_Type CHAR(8),  
        Account_Vendor_Id INT  
    );  
  
    CREATE TABLE #Client_hier_Account_Details  
    (  
        Account_Vendor_Name VARCHAR(200),  
        Account_Type CHAR(8),  
        Account_Vendor_Id INT,  
        Account_Id INT,  
        Invoice_Collection_Account_Config_Id INT  
    );  
  
    CREATE TABLE #Consolidated_Billing_Account_Id  
    (  
        Account_Id INT  
    );  
  
  
    CREATE TABLE #Vendor_Account_Details  
    (  
        Account_Vendor_Name VARCHAR(200),  
        Account_Type CHAR(8),  
        Account_Vendor_Id INT,  
        Account_Id INT,  
        Invoice_Collection_Account_Config_Id INT  
    );  
  
  
    CREATE TABLE #Ic_Account_Details  
    (  
        Account_Vendor_Name VARCHAR(200),  
        Account_Type CHAR(8),  
        Account_Vendor_Id INT,  
        Account_Id INT,  
        Invoice_Collection_Account_Config_Id INT  
    );  
  
    CREATE TABLE #Invoice_Collection_Queue  
    (  
        Invoice_Collection_Queue_Id INT  
    );  
  
  
  
  
    SELECT @ICO_User_Info_Id = ICO_User_Info_Id,  
           @Collection_Start_Date = Collection_Start_Date,  
           @Collection_End_Date = Collection_End_Date,  
           @Priority_Cd = Priority_Cd,  
           @Client_Id = Client_Id,  
           @Site_Id = Site_Id,  
           @Account_Id = Account_Id,  
           @Country_Id = Country_Id,  
           @State_Id = State_Id,  
           @Commodity_Id = Commodity_Id,  
           @Vendor_Type = Vendor_Type,  
           @Vendor_Id = Vendor_Id,  
           @Client_Contact_Info_Id = Client_Contact_Info_Id,  
           @Account_Contact_Info_Id = Account_Contact_Info_Id,  
           @Vendor_Contact_Info_Id = Vendor_Contact_Info_Id,  
           @Issue_Status = Issue_Status,  
           @Previously_Chased = Previously_Chased,  
           @Record_Type_Cd = Record_Type_Cd,  
           @ICR_Status_Cd = ICR_Status_Cd,  
           @Days_Since_Last_Chased_From = Days_Since_Last_Chased_From,  
           @Days_Since_Last_Chased_To = Days_Since_Last_Chased_To  
    FROM @tvp_Invoice_Collection_Queue_Sel;  
  
  
    SELECT @Record_Type_Cd_Value = c.Code_Value  
    FROM dbo.Code c  
    WHERE c.Code_Id = @Record_Type_Cd;  
  
  
  
    SET @Sql_Ic_Acc_Config_Select = 'SELECT icac.Invoice_Collection_Account_Config_Id ';  
  
    SET @Sql_Ic_Acc_Config_From  
        = ' FROM dbo.Invoice_Collection_Queue icq              
    INNER JOIN dbo.Invoice_Collection_Account_Config icac              
        ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id              
    INNER JOIN Core.Client_Hier_Account cha              
        ON cha.Account_Id = icac.Account_Id              
    INNER JOIN Core.Client_Hier ch              
        ON ch.Client_Hier_Id = cha.Client_Hier_Id              
    INNER JOIN dbo.Commodity com              
        ON com.Commodity_Id = cha.Commodity_Id              
    INNER JOIN dbo.Invoice_Collection_Queue_Month_Map icqmm              
        ON icq.Invoice_Collection_Queue_Id = icqmm.Invoice_Collection_Queue_Id              
    INNER JOIN(dbo.Account_Invoice_Collection_Month aicm              
    LEFT OUTER JOIN dbo.Account_Invoice_Collection_Frequency aicfe              
        ON aicm.Account_Invoice_Collection_Frequency_Id = aicfe.Account_Invoice_Collection_Frequency_Id              
    LEFT OUTER JOIN dbo.Code ifc              
        ON ifc.Code_Id = aicfe.Invoice_Frequency_Cd              
    LEFT OUTER JOIN dbo.Invoice_Collection_Global_Config_Value icgcv              
        ON aicm.Invoice_Collection_Global_Config_Value_Id = icgcv.Invoice_Collection_Global_Config_Value_Id              
    LEFT OUTER JOIN dbo.Code icgc              
        ON icgc.Code_Id = icgcv.Invoice_Frequency_Cd)              
        ON icqmm.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id';  
  
  
  
    SET @Sql_Ic_Acc_Config_Where  
        = ' WHERE icq.Invoice_Collection_Queue_Type_Cd = ' + CAST(@Invoice_Collection_Type_Cd AS VARCHAR(10))  
          + ' AND (   icac.Is_Chase_Activated = 1                
                    OR  (   icac.Is_Chase_Activated = 0                
                            AND EXISTS (   SELECT                
                                                1                
                                           FROM                
                                                dbo.Invoice_Collection_Queue icq1                
                                                INNER JOIN dbo.Code sc                
                                                    ON sc.Code_Id = icq.Status_Cd                
                                           WHERE                
                                                icq1.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id                
                                                AND sc.Code_Value = ''Open'')))';  
  
    SET @Sql_Ic_Acc_Config_Where  
        = @Sql_Ic_Acc_Config_Where  
          + CASE  
                WHEN @ICO_User_Info_Id IS NOT NULL THEN  
                    ' AND icac.Invoice_Collection_Officer_User_Id =' + CAST(@ICO_User_Info_Id AS VARCHAR(20))  
                ELSE  
                    ''  
            END;  
  
  
    SET @Sql_Ic_Acc_Config_Where  
        = @Sql_Ic_Acc_Config_Where  
          + CASE  
                WHEN @Collection_Start_Date IS NULL  
                     AND @Collection_End_Date IS NULL THEN  
                    ''  
                WHEN @Collection_Start_Date IS NOT NULL  
                     AND @Collection_End_Date IS NULL THEN  
                    ' AND ''' + CAST(@Collection_Start_Date AS VARCHAR(20)) + '''<= icq.Collection_Start_Dt'  
                WHEN @Collection_Start_Date IS NULL  
                     AND @Collection_End_Date IS NOT NULL THEN  
                    ' AND ''' + CAST(@Collection_End_Date AS VARCHAR(20)) + '''>= icq.Collection_End_Dt'  
                WHEN @Collection_Start_Date IS NOT NULL  
                     AND @Collection_End_Date IS NOT NULL THEN  
                    ' AND (''' + CAST(@Collection_Start_Date AS VARCHAR(20)) + '''<= icq.Collection_Start_Dt AND '''  
                    + CAST(@Collection_End_Date AS VARCHAR(20)) + '''>= icq.Collection_End_Dt)'  
                ELSE  
                    ''  
            END;  
  
    SET @Sql_Ic_Acc_Config_Where  
        = @Sql_Ic_Acc_Config_Where + CASE  
                                         WHEN @Client_Id IS NOT NULL THEN  
                                             ' AND ch.Client_Id = ' + CAST(@Client_Id AS VARCHAR(20))  
                                         ELSE  
                                             ''  
                                     END + CASE  
                                               WHEN @Site_Id IS NOT NULL THEN  
                                                   ' AND ch.Site_Id = ' + CAST(@Site_Id AS VARCHAR(20))  
                                               ELSE  
                                                   ''  
                                           END + CASE  
                                                     WHEN @Account_Id IS NOT NULL THEN  
                                                         ' AND cha.Account_Id = ' + CAST(@Account_Id AS VARCHAR(20))  
                                                     ELSE  
                                                         ''  
                                                 END  
          + CASE  
                WHEN @Country_Id IS NOT NULL THEN  
                    ' AND ch.Country_Id = ' + CAST(@Country_Id AS VARCHAR(20))  
                ELSE  
                    ''  
            END + CASE  
                      WHEN @State_Id IS NOT NULL THEN  
                          ' AND ch.State_Id = ' + CAST(@State_Id AS VARCHAR(20))  
                      ELSE  
                          ''  
                  END + CASE  
                            WHEN @Commodity_Id IS NOT NULL THEN  
                                ' AND cha.Commodity_Id = ' + CAST(@Commodity_Id AS VARCHAR(20))  
                            ELSE  
                                ''  
                        END + CASE  
                                  WHEN @ICR_Status_Cd IS NOT NULL THEN  
                                      ' AND icq.Status_Cd = ' + CAST(@ICR_Status_Cd AS VARCHAR(20))  
                                  ELSE  
                                      ''  
                              END;  
  
  
    SET @Sql_Ic_Acc_Config_Groupby = ' GROUP BY icac.Invoice_Collection_Account_Config_Id ';  
  
    SET @Sql_Ic_Acc_Config_STMT  
        = @Sql_Ic_Acc_Config_Select + @Sql_Ic_Acc_Config_From + @Sql_Ic_Acc_Config_Where + @Sql_Ic_Acc_Config_Groupby;  
  
    INSERT INTO #Invoice_Collection_Account_Config  
    (  
        Invoice_Collection_Account_Config_Id  
    )  
    EXEC (@Sql_Ic_Acc_Config_STMT);  
  
  
    INSERT INTO #Invoice_Collection_Accounts  
    (  
        Invoice_Collection_Account_Config_Id,  
        Account_Id,  
        Invoice_Collection_Service_End_Dt,  
        Account_Vendor_Name,  
        Account_Type,  
        Account_Vendor_Id  
    )  
    SELECT icac.Invoice_Collection_Account_Config_Id,  
           icac.Account_Id,  
           icac.Invoice_Collection_Service_End_Dt,  
           ucha.Account_Vendor_Name,  
           ucha.Account_Type,  
           ucha.Account_Vendor_Id  
    FROM dbo.Invoice_Collection_Account_Config icac  
        INNER JOIN Core.Client_Hier_Account ucha  
            ON icac.Account_Id = ucha.Account_Id  
    WHERE ucha.Account_Type = 'Utility'  
          AND EXISTS  
    (  
        SELECT 1  
        FROM #Invoice_Collection_Account_Config icac1  
        WHERE icac.Invoice_Collection_Account_Config_Id = icac1.Invoice_Collection_Account_Config_Id  
    )  
          AND  
          (  
              @ICO_User_Info_Id IS NULL  
              OR icac.Invoice_Collection_Officer_User_Id = @ICO_User_Info_Id  
          )  
    GROUP BY icac.Invoice_Collection_Account_Config_Id,  
             icac.Account_Id,  
             icac.Invoice_Collection_Service_End_Dt,  
             ucha.Account_Vendor_Name,  
             ucha.Account_Type,  
             ucha.Account_Vendor_Id;  
  
  
  
    INSERT INTO #Client_hier_Account_Details  
    (  
        Account_Vendor_Name,  
        Account_Type,  
        Account_Vendor_Id,  
        Account_Id,  
        Invoice_Collection_Account_Config_Id  
    )  
    SELECT scha.Account_Vendor_Name,  
           scha.Account_Type,  
           scha.Account_Vendor_Id,  
           ucha1.Account_Id,  
           icac.Invoice_Collection_Account_Config_Id  
    FROM Core.Client_Hier_Account scha  
        INNER JOIN Core.Client_Hier_Account ucha1  
            ON ucha1.Meter_Id = scha.Meter_Id  
               AND ucha1.Account_Type = 'Utility'  
        INNER JOIN dbo.CONTRACT c  
            ON c.CONTRACT_ID = scha.Supplier_Contract_ID  
        INNER JOIN dbo.ENTITY ce  
            ON c.CONTRACT_TYPE_ID = ce.ENTITY_ID  
               AND ce.ENTITY_NAME = 'Supplier'  
               AND scha.Account_Type = 'Supplier'  
        INNER JOIN #Invoice_Collection_Accounts icac  
            ON icac.Account_Id = ucha1.Account_Id  
               AND (CASE  
                        WHEN icac.Invoice_Collection_Service_End_Dt IS NULL  
                             OR icac.Invoice_Collection_Service_End_Dt > GETDATE() THEN  
                            GETDATE()  
                        ELSE  
                            icac.Invoice_Collection_Service_End_Dt  
                    END  
                   )  
               BETWEEN scha.Supplier_Meter_Association_Date AND scha.Supplier_Meter_Disassociation_Date  
    GROUP BY scha.Account_Vendor_Name,  
             scha.Account_Type,  
             scha.Account_Vendor_Id,  
             ucha1.Account_Id,  
             icac.Invoice_Collection_Account_Config_Id;  
  
  
  
    INSERT INTO #Consolidated_Billing_Account_Id  
    (  
        Account_Id  
    )  
    SELECT asbv1.Account_Id  
    FROM dbo.Account_Consolidated_Billing_Vendor asbv1  
        INNER JOIN dbo.ENTITY e1  
            ON asbv1.Invoice_Vendor_Type_Id = e1.ENTITY_ID  
               AND e1.ENTITY_NAME = 'Supplier'  
        LEFT OUTER JOIN dbo.VENDOR v1  
            ON v1.VENDOR_ID = asbv1.Supplier_Vendor_Id  
    WHERE EXISTS  
    (  
        SELECT 1  
        FROM #Invoice_Collection_Accounts icac  
        WHERE asbv1.Account_Id = icac.Account_Id  
    )  
    GROUP BY asbv1.Account_Id;  
  
  
    INSERT INTO #Vendor_Account_Details  
    (  
        Account_Vendor_Name,  
        Account_Type,  
        Account_Vendor_Id,  
        Account_Id,  
        Invoice_Collection_Account_Config_Id  
    )  
    SELECT v.VENDOR_NAME,  
           e.ENTITY_NAME,  
           v.VENDOR_ID,  
           asbv.Account_Id,  
           icac.Invoice_Collection_Account_Config_Id  
    FROM dbo.Account_Consolidated_Billing_Vendor asbv  
        INNER JOIN dbo.ENTITY e  
            ON asbv.Invoice_Vendor_Type_Id = e.ENTITY_ID  
               AND e.ENTITY_NAME = 'Supplier'  
        LEFT OUTER JOIN dbo.VENDOR v  
            ON v.VENDOR_ID = asbv.Supplier_Vendor_Id  
        INNER JOIN #Invoice_Collection_Accounts icac  
            ON icac.Account_Id = asbv.Account_Id  
               AND (CASE  
                        WHEN icac.Invoice_Collection_Service_End_Dt IS NULL  
                             OR asbv.Billing_End_Dt > GETDATE() THEN  
                            GETDATE()  
                        ELSE  
                            icac.Invoice_Collection_Service_End_Dt  
                    END  
                   )  
               BETWEEN asbv.Billing_Start_Dt AND ISNULL(asbv.Billing_End_Dt, '9999-12-31')  
    GROUP BY v.VENDOR_NAME,  
             e.ENTITY_NAME,  
             v.VENDOR_ID,  
             asbv.Account_Id,  
             icac.Invoice_Collection_Account_Config_Id;  
  
  
    INSERT INTO #Ic_Account_Details  
    (  
        Account_Vendor_Name,  
        Account_Type,  
        Account_Vendor_Id,  
        Account_Id,  
        Invoice_Collection_Account_Config_Id  
    )  
    SELECT icav.VENDOR_NAME,  
           'Supplier',  
           icav.VENDOR_ID,  
           icac.Account_Id,  
           aics.Invoice_Collection_Account_Config_Id  
    FROM dbo.Invoice_Collection_Account_Contact icc  
        INNER JOIN dbo.Account_Invoice_Collection_Source aics  
            ON icc.Invoice_Collection_Account_Config_Id = aics.Invoice_Collection_Account_Config_Id  
        INNER JOIN dbo.Contact_Info ci  
            ON ci.Contact_Info_Id = icc.Contact_Info_Id  
               AND  
               (  
                   (  
                       @Source_Type_Client = aics.Invoice_Source_Type_Cd  
                       AND @Contact_Level_Client = ci.Contact_Level_Cd  
                   )  
                   OR  
                   (  
                       @Source_Type_Account = aics.Invoice_Source_Type_Cd  
                       AND @Contact_Level_Account = ci.Contact_Level_Cd  
                   )  
                   OR  
                   (  
                       @Source_Type_Vendor = aics.Invoice_Source_Type_Cd  
                       AND @Contact_Level_Vendor = ci.Contact_Level_Cd  
                   )  
               )  
        INNER JOIN dbo.Vendor_Contact_Map vcm  
            ON ci.Contact_Info_Id = vcm.Contact_Info_Id  
        INNER JOIN dbo.VENDOR icav  
            ON icav.VENDOR_ID = vcm.VENDOR_ID  
        INNER JOIN dbo.ENTITY ve  
            ON icav.VENDOR_TYPE_ID = ve.ENTITY_ID  
        INNER JOIN #Invoice_Collection_Accounts icac  
            ON icc.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id  
               AND icc.Is_Primary = 1  
    GROUP BY icav.VENDOR_NAME,  
             icav.VENDOR_ID,  
             icac.Account_Id,  
             aics.Invoice_Collection_Account_Config_Id;  
  
  
  
    INSERT INTO #Vendor_Dtls  
    (  
        Account_Vendor_Name,  
        Account_Vendor_Type,  
        Account_Vendor_Id,  
        Invoice_Collection_Account_Config_Id  
    )  
    SELECT avd.Account_Vendor_Name,  
           avd.Account_Type,  
           avd.Account_Vendor_Id,  
           avd.Invoice_Collection_Account_Config_Id  
    FROM #Client_hier_Account_Details avd  
    WHERE EXISTS  
    (  
        SELECT 1  
        FROM #Consolidated_Billing_Account_Id cbai  
        WHERE avd.Account_Id = cbai.Account_Id  
    );  
  
    INSERT INTO #Vendor_Dtls  
    (  
        Account_Vendor_Name,  
        Account_Vendor_Type,  
        Account_Vendor_Id,  
        Invoice_Collection_Account_Config_Id  
    )  
    SELECT vad.Account_Vendor_Name,  
           vad.Account_Type,  
           vad.Account_Vendor_Id,  
           vad.Invoice_Collection_Account_Config_Id  
    FROM #Vendor_Account_Details vad  
    WHERE NOT EXISTS  
    (  
        SELECT 1  
        FROM #Vendor_Dtls vd  
        WHERE vad.Invoice_Collection_Account_Config_Id = vd.Invoice_Collection_Account_Config_Id  
    );  
  
    INSERT INTO #Vendor_Dtls  
    (  
        Account_Vendor_Name,  
        Account_Vendor_Type,  
        Account_Vendor_Id,  
        Invoice_Collection_Account_Config_Id  
    )  
    SELECT icd.Account_Vendor_Name,  
           icd.Account_Type,  
           icd.Account_Vendor_Id,  
           icd.Invoice_Collection_Account_Config_Id  
    FROM #Ic_Account_Details icd  
    WHERE EXISTS  
    (  
        SELECT 1  
        FROM #Consolidated_Billing_Account_Id cbai  
        WHERE icd.Account_Id = cbai.Account_Id  
    )  
          AND NOT EXISTS  
    (  
        SELECT 1  
        FROM #Vendor_Dtls vd  
        WHERE icd.Invoice_Collection_Account_Config_Id = vd.Invoice_Collection_Account_Config_Id  
    );  
  
  
  
    INSERT INTO #Vendor_Dtls  
    (  
        Account_Vendor_Name,  
        Account_Vendor_Type,  
        Account_Vendor_Id,  
        Invoice_Collection_Account_Config_Id  
    )  
    SELECT ica.Account_Vendor_Name,  
           ica.Account_Type,  
           ica.Account_Vendor_Id,  
           ica.Invoice_Collection_Account_Config_Id  
    FROM #Invoice_Collection_Accounts ica  
    WHERE NOT EXISTS  
    (  
        SELECT 1  
        FROM #Vendor_Dtls vd  
        WHERE ica.Invoice_Collection_Account_Config_Id = vd.Invoice_Collection_Account_Config_Id  
    );  
  
  
  
  
    INSERT INTO #Contact_Dtls  
    (  
        Invoice_Collection_Account_Config_Id,  
        Invoice_Collection_Source_Cd,  
        Invoice_Source_Type_Cd,  
        Invoice_Source_Method_of_Contact_Cd,  
        Contact_Level_Cd,  
        Contact_Info_Id  
    )  
    SELECT icac.Invoice_Collection_Account_Config_Id,  
           aics.Invoice_Collection_Source_Cd,  
           aics.Invoice_Source_Type_Cd,  
           aics.Invoice_Source_Method_of_Contact_Cd,  
           MAX(ci.Contact_Level_Cd),  
           MAX(ci.Contact_Info_Id)  
    FROM dbo.Invoice_Collection_Account_Config icac  
        LEFT OUTER JOIN dbo.Invoice_Collection_Account_Contact icc  
            ON icac.Invoice_Collection_Account_Config_Id = icc.Invoice_Collection_Account_Config_Id  
               AND icc.Is_Primary = 1  
        LEFT OUTER JOIN dbo.Account_Invoice_Collection_Source aics  
            ON icac.Invoice_Collection_Account_Config_Id = aics.Invoice_Collection_Account_Config_Id  
               AND aics.Is_Primary = 1  
        LEFT OUTER JOIN dbo.Contact_Info ci  
            ON ci.Contact_Info_Id = icc.Contact_Info_Id  
               AND  
               (  
                   (  
                       @Source_Type_Client = aics.Invoice_Source_Type_Cd  
                       AND @Contact_Level_Client = ci.Contact_Level_Cd  
                   )  
                   OR  
                   (  
                       @Source_Type_Account = aics.Invoice_Source_Type_Cd  
                       AND @Contact_Level_Account = ci.Contact_Level_Cd  
                   )  
                   OR  
                   (  
                       @Source_Type_Vendor = aics.Invoice_Source_Type_Cd  
                       AND @Contact_Level_Vendor = ci.Contact_Level_Cd  
                   )  
               )  
    WHERE EXISTS  
    (  
        SELECT 1  
        FROM #Invoice_Collection_Account_Config icac1  
        WHERE icac.Invoice_Collection_Account_Config_Id = icac1.Invoice_Collection_Account_Config_Id  
    )  
    GROUP BY icac.Invoice_Collection_Account_Config_Id,  
             aics.Invoice_Collection_Source_Cd,  
             aics.Invoice_Source_Type_Cd,  
             aics.Invoice_Source_Method_of_Contact_Cd;  
  
  
  
    INSERT INTO #Contact_Dtls_For_search  
    (  
        Invoice_Collection_Account_Config_Id,  
        Invoice_Collection_Source_Cd,  
        Invoice_Source_Type_Cd,  
        Invoice_Source_Method_of_Contact_Cd,  
        Contact_Level_Cd,  
        Contact_Info_Id  
    )  
    SELECT icac.Invoice_Collection_Account_Config_Id,  
           aics.Invoice_Collection_Source_Cd,  
           aics.Invoice_Source_Type_Cd,  
           aics.Invoice_Source_Method_of_Contact_Cd,  
           ci.Contact_Level_Cd,  
           ci.Contact_Info_Id  
    FROM dbo.Invoice_Collection_Account_Config icac  
        INNER JOIN dbo.Invoice_Collection_Account_Contact icc  
            ON icac.Invoice_Collection_Account_Config_Id = icc.Invoice_Collection_Account_Config_Id  
        INNER JOIN dbo.Account_Invoice_Collection_Source aics  
            ON icac.Invoice_Collection_Account_Config_Id = aics.Invoice_Collection_Account_Config_Id  
        INNER JOIN dbo.Contact_Info ci  
            ON ci.Contact_Info_Id = icc.Contact_Info_Id  
               AND  
               (  
                   (  
                       @Source_Type_Client = aics.Invoice_Source_Type_Cd  
                       AND @Contact_Level_Client = ci.Contact_Level_Cd  
                   )  
                   OR  
                   (  
                       @Source_Type_Account = aics.Invoice_Source_Type_Cd  
                       AND @Contact_Level_Account = ci.Contact_Level_Cd  
                   )  
                   OR  
                   (  
                       @Source_Type_Vendor = aics.Invoice_Source_Type_Cd  
                       AND @Contact_Level_Vendor = ci.Contact_Level_Cd  
                   )  
               )  
    WHERE EXISTS  
    (  
        SELECT 1  
        FROM #Invoice_Collection_Account_Config icac1  
        WHERE icac.Invoice_Collection_Account_Config_Id = icac1.Invoice_Collection_Account_Config_Id  
    );  
  
  
  
  
    SET @Sql_IVC_Queue_Select  
        = ' SELECT      
                
             icq.Invoice_Collection_Queue_Id      
            ';  
  
  
    SET @Sql_IVC_Queue_From  
        = ' FROM      
            dbo.Invoice_Collection_Queue icq      
            INNER JOIN dbo.Invoice_Collection_Account_Config icac      
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id      
            INNER JOIN #Invoice_Collection_Account_Config icac1      
                ON icac.Invoice_Collection_Account_Config_Id = icac1.Invoice_Collection_Account_Config_Id      
            INNER JOIN Core.Client_Hier_Account cha      
                ON cha.Account_Id = icac.Account_Id      
            INNER JOIN Core.Client_Hier ch      
                ON ch.Client_Hier_Id = cha.Client_Hier_Id      
            INNER JOIN dbo.Commodity com      
                ON com.Commodity_Id = cha.Commodity_Id      
            INNER JOIN dbo.USER_INFO ui      
                ON ui.USER_INFO_ID = icac.Invoice_Collection_Officer_User_Id      
        INNER JOIN dbo.Invoice_Collection_Queue_Month_Map icqmm      
                ON icq.Invoice_Collection_Queue_Id = icqmm.Invoice_Collection_Queue_Id      
            INNER JOIN(dbo.Account_Invoice_Collection_Month aicm      
                       LEFT OUTER JOIN dbo.Account_Invoice_Collection_Frequency aicfe      
                           ON aicm.Account_Invoice_Collection_Frequency_Id = aicfe.Account_Invoice_Collection_Frequency_Id      
                       LEFT OUTER JOIN dbo.Code ifc      
                           ON ifc.Code_Id = aicfe.Invoice_Frequency_Cd      
                       LEFT OUTER JOIN dbo.Invoice_Collection_Global_Config_Value icgcv      
                           ON aicm.Invoice_Collection_Global_Config_Value_Id = icgcv.Invoice_Collection_Global_Config_Value_Id      
                       LEFT OUTER JOIN dbo.Code icgc      
                           ON icgc.Code_Id = icgcv.Invoice_Frequency_Cd)      
                ON icqmm.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id      
            INNER JOIN dbo.Code cpc      
                ON cpc.Code_Id = icac.Chase_Priority_Cd      
            LEFT OUTER JOIN #Vendor_Dtls vd      
                ON vd.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id      
            INNER JOIN(#Contact_Dtls cd      
                       LEFT OUTER JOIN dbo.Contact_Info ci      
                           ON ci.Contact_Info_Id = cd.Contact_Info_Id      
                              AND   ci.Contact_Level_Cd = cd.Contact_Level_Cd      
                       LEFT OUTER JOIN dbo.Code cci      
                           ON cci.Code_Id = ci.Contact_Level_Cd)      
                ON cd.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id      
            LEFT OUTER JOIN dbo.Code cics      
                ON cics.Code_Id = cd.Invoice_Collection_Source_Cd      
            LEFT OUTER JOIN dbo.Code cicst      
                ON cicst.Code_Id = cd.Invoice_Source_Type_Cd      
            LEFT OUTER JOIN dbo.Code cicsm      
                ON cicsm.Code_Id = cd.Invoice_Source_Method_of_Contact_Cd      
            LEFT OUTER JOIN(dbo.Invoice_Collection_Chase_Log_Queue_Map icccm      
                            INNER JOIN dbo.Invoice_Collection_Chase_Log iccc      
                                ON icccm.Invoice_Collection_Chase_Log_Id = iccc.Invoice_Collection_Chase_Log_Id      
                            INNER JOIN dbo.Code sc      
                                ON sc.Code_Id = iccc.Status_Cd      
                                   AND  sc.Code_Value = ''Close'')      
                ON icccm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id      
            LEFT OUTER JOIN(dbo.Invoice_Collection_Final_Review_Log_Queue_Map icfrlqm      
                            INNER JOIN dbo.Invoice_Collection_Final_Review_Log icfrl      
                                ON icfrlqm.Invoice_Collection_Final_Review_Log_Id = icfrl.Invoice_Collection_Final_Review_Log_Id   
                            INNER JOIN dbo.Code frsc      
                                ON frsc.Code_Id = icfrl.Status_Cd      
                                   AND  frsc.Code_Value = ''Close'')      
                ON icfrlqm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id      
            LEFT OUTER JOIN      
            (   SELECT      
                    MAX(iccc.Last_Change_Ts) Last_Change_Ts      
                    , icccm.Invoice_Collection_Queue_Id      
                    , COUNT(iccc.Invoice_Collection_Chase_Log_Id) Chase_Cnt      
                FROM      
                    dbo.Invoice_Collection_Chase_Log_Queue_Map icccm      
                    INNER JOIN dbo.Invoice_Collection_Chase_Log iccc      
                        ON icccm.Invoice_Collection_Chase_Log_Id = iccc.Invoice_Collection_Chase_Log_Id      
                    INNER JOIN dbo.Code sc      
                        ON sc.Code_Id = iccc.Status_Cd      
                           AND  sc.Code_Value = ''Close''      
                GROUP BY      
              icccm.Invoice_Collection_Queue_Id) lcd      
                ON icccm.Invoice_Collection_Queue_Id = lcd.Invoice_Collection_Queue_Id      
            LEFT OUTER JOIN      
            (   SELECT      
                    Invoice_Collection_Queue_Id      
                    , MAX(CASE WHEN Issue_Status_Cd = ' + CAST(@Invoice_Collection_Issue_Status_Cd AS VARCHAR(20))  
          + ' THEN 1      
                              ELSE 0      
                          END) AS Issue_Status_Cd      
                    , MAX(CASE WHEN Is_Blocker = 1 THEN 1      
                              ELSE 0      
                          END) Is_Blocker      
                    , MAX(Last_Change_Ts) Last_change_Ts      
                FROM      
                    dbo.Invoice_Collection_Issue_Log      
                GROUP BY      
                    Invoice_Collection_Queue_Id) ici      
                ON ici.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id      
            LEFT OUTER JOIN dbo.Code icisc      
                ON icisc.Code_Id = ici.Issue_Status_Cd      
            LEFT OUTER JOIN dbo.Code icmrt      
                ON icmrt.Code_Id = icq.Invoice_Request_Type_Cd ';  
  
  
  
  
    SET @Sql_IVC_Queue_Where  
        = ' where  icq.Invoice_Collection_Queue_Type_Cd = ' + CAST(@Invoice_Collection_Type_Cd AS VARCHAR(20))  
          + ' AND        
          (        
              icac.Is_Chase_Activated = 1        
              OR        
              (        
                  icac.Is_Chase_Activated = 0        
                  AND EXISTS        
    (        
        SELECT 1        
        FROM dbo.Invoice_Collection_Queue icq1        
            INNER JOIN dbo.Code sc        
                ON sc.Code_Id = icq.Status_Cd        
        WHERE icq1.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id        
              AND sc.Code_Value = ''Open''        
    )        
              )        
          ) ';  
  
    SET @Sql_IVC_Queue_Where  
        = @Sql_IVC_Queue_Where  
          + CASE  
                WHEN @ICO_User_Info_Id IS NULL THEN  
                    ''  
                ELSE  
                    ' AND icac.Invoice_Collection_Officer_User_Id =' + CAST(@ICO_User_Info_Id AS VARCHAR(20))  
            END;  
  
    SET @Sql_IVC_Queue_Where  
        = @Sql_IVC_Queue_Where  
          + CASE  
                WHEN @Collection_Start_Date IS NULL  
                     AND @Collection_End_Date IS NULL THEN  
                    ''  
                WHEN @Collection_Start_Date IS NOT NULL  
                     AND @Collection_End_Date IS NULL THEN  
                    ' AND ''' + CAST(@Collection_Start_Date AS VARCHAR(100)) + ''' <= icq.Collection_Start_Dt'  
                WHEN @Collection_Start_Date IS NULL  
                     AND @Collection_End_Date IS NOT NULL THEN  
                    ' AND ''' + CAST(@Collection_End_Date AS VARCHAR(100)) + ''' >= icq.Collection_End_Dt'  
                WHEN @Collection_Start_Date IS NOT NULL  
                     AND @Collection_End_Date IS NOT NULL THEN  
                    ' AND(''' + CAST(@Collection_Start_Date AS VARCHAR(100))  
                    + '''<= icq.Collection_Start_Dt        
                      AND '''                                 + CAST(@Collection_End_Date AS VARCHAR(100))  
                    + '''>= icq.Collection_End_Dt) '  
                ELSE  
                    ''  
            END;  
  
    SET @Sql_IVC_Queue_Where = @Sql_IVC_Queue_Where + CASE  
                                                          WHEN @Priority_Cd IS NULL THEN  
                                                              ''  
                                                          ELSE  
                                                              ' AND cpc.Code_Id =' + CAST(@Priority_Cd AS VARCHAR(20))  
                                                      END;  
  
    SET @Sql_IVC_Queue_Where = @Sql_IVC_Queue_Where + CASE  
                                                          WHEN @Client_Id IS NULL THEN  
                                                              ''  
                                                          ELSE  
                                                              ' AND ch.Client_Id =' + CAST(@Client_Id AS VARCHAR(20))  
                                                      END;  
  
    SET @Sql_IVC_Queue_Where = @Sql_IVC_Queue_Where + CASE  
                                                          WHEN @Site_Id IS NULL THEN  
                                                              ''  
                                                          ELSE  
                                                              ' AND ch.Site_Id =' + CAST(@Site_Id AS VARCHAR(20))  
                                                      END;  
  
    SET @Sql_IVC_Queue_Where  
        = @Sql_IVC_Queue_Where + CASE  
                                     WHEN @Account_Id IS NULL THEN  
                                         ''  
                                     ELSE  
                                         ' AND cha.Account_Id =' + CAST(@Account_Id AS VARCHAR(20))  
                                 END;  
  
    SET @Sql_IVC_Queue_Where = @Sql_IVC_Queue_Where + CASE  
                                                          WHEN @Country_Id IS NULL THEN  
                                                              ''  
                                                          ELSE  
                                                              ' AND ch.Country_Id =' + CAST(@Country_Id AS VARCHAR(20))  
                                                      END;  
  
    SET @Sql_IVC_Queue_Where = @Sql_IVC_Queue_Where + CASE  
                                                          WHEN @State_Id IS NULL THEN  
                                                              ''  
                                                          ELSE  
                                                              ' AND ch.State_Id =' + CAST(@State_Id AS VARCHAR(20))  
                                                      END;  
  
    SET @Sql_IVC_Queue_Where  
        = @Sql_IVC_Queue_Where + CASE  
                                     WHEN @Commodity_Id IS NULL THEN  
                                         ''  
                                     ELSE  
                                         ' AND cha.Commodity_Id =' + CAST(@Commodity_Id AS VARCHAR(20))  
                                 END;  
  
    SET @Sql_IVC_Queue_Where  
        = @Sql_IVC_Queue_Where  
          + CASE  
                WHEN @Vendor_Type IS NULL THEN  
                    ''  
                ELSE  
                    ' AND ISNULL(vd.Account_Vendor_Type, cha.Account_Type) ='''  
                    + RTRIM(CAST(@Vendor_Type AS VARCHAR(20))) + ''''  
            END;  
  
    SET @Sql_IVC_Queue_Where  
        = @Sql_IVC_Queue_Where  
          + CASE  
                WHEN @Vendor_Id IS NULL THEN  
                    ''  
                ELSE  
                    ' AND ISNULL(vd.Account_Vendor_Id, cha.Account_Vendor_Id) =' + CAST(@Vendor_Id AS VARCHAR(20))  
            END;  
  
    SET @Sql_IVC_Queue_Where  
        = @Sql_IVC_Queue_Where  
          + CASE  
                WHEN @Client_Contact_Info_Id IS NULL THEN  
                    ''  
                ELSE  
                    ' AND EXISTS        
    (        
        SELECT 1        
        FROM #Contact_Dtls_For_search cdfs        
        WHERE cdfs.Contact_Info_Id = ' + CAST(@Client_Contact_Info_Id AS VARCHAR(20))  
                    + '        
              AND cdfs.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id        
    )'  
            END;  
  
  
  
    SET @Sql_IVC_Queue_Where  
        = @Sql_IVC_Queue_Where  
          + CASE  
                WHEN @Download_Count IS NULL THEN  
                    ''  
                ELSE  
                    ' AND  ( EXISTS (   SELECT      
                                        1      
                                    FROM      
                                        dbo.ufn_split(''' + @Download_Count  
                    + ''', '','') us       
                                    WHERE      
          ISNULL(icq.Download_Attempt_Cnt, 0) >= CASE WHEN us.Segments LIKE ''%+%'' THEN 4      
                                                                                   ELSE us.Segments      
                                                                               END      
                                        AND ISNULL(icq.Download_Attempt_Cnt, 0) <= CASE WHEN us.Segments LIKE ''%+%'' THEN      
                                                                                            100      
                                                                                       ELSE us.Segments      
                                                                                   END)) '  
            END;  
  
  
    SET @Sql_IVC_Queue_Where  
        = @Sql_IVC_Queue_Where  
          + CASE  
                WHEN @Chase_Count IS NULL THEN  
                    ''  
                ELSE  
                    ' AND (EXISTS (   SELECT      
                                        1      
                                    FROM      
                                        dbo.ufn_split(''' + @Chase_Count  
                    + ''', '','') us      
                                    WHERE      
                                        ISNULL(lcd.Chase_Cnt, 0) >= CASE WHEN us.Segments LIKE ''%+%'' THEN 4      
                                                                                   ELSE us.Segments      
                                                                               END      
                                        AND ISNULL(lcd.Chase_Cnt, 0) <= CASE WHEN us.Segments LIKE ''%+%'' THEN      
                                                                                            100      
                                                                                       ELSE us.Segments      
                                                                                   END)) '  
            END;  
  
  
  
    SET @Sql_IVC_Queue_Where  
        = @Sql_IVC_Queue_Where  
          + CASE  
                WHEN @Account_Contact_Info_Id IS NULL THEN  
                    ''  
                ELSE  
                    ' AND EXISTS        
    (        
        SELECT 1        
        FROM #Contact_Dtls_For_search cdfs        
        WHERE cdfs.Contact_Info_Id = ' + CAST(@Account_Contact_Info_Id AS VARCHAR(20))  
                    + '                AND cdfs.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id        
    )'  
            END;  
  
  
    SET @Sql_IVC_Queue_Where  
        = @Sql_IVC_Queue_Where  
          + CASE  
                WHEN @Vendor_Contact_Info_Id IS NULL THEN  
                    ''  
                ELSE  
                    ' AND EXISTS        
    (        
        SELECT 1        
        FROM #Contact_Dtls_For_search cdfs        
        WHERE cdfs.Contact_Info_Id = ' + CAST(@Vendor_Contact_Info_Id AS VARCHAR(20))  
                    + '        
              AND cdfs.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id        
    )'  
            END;  
  
  
    SET @Sql_IVC_Queue_Where  
        = @Sql_IVC_Queue_Where + CASE  
                                     WHEN @ICR_Status_Cd IS NULL THEN  
                                         ''  
                                     ELSE  
                                         ' AND icq.Status_Cd =' + CAST(@ICR_Status_Cd AS VARCHAR(20))  
                                 END;  
  
    SET @Sql_IVC_Queue_Where = @Sql_IVC_Queue_Where + CASE  
                                                          WHEN @Is_Issue_Blocker IS NULL THEN  
                                                              ''  
                                                          WHEN @Is_Issue_Blocker = 1 THEN  
                                                              ' AND ici.Is_Blocker = 1 '  
                                                          WHEN @Is_Issue_Blocker = 0 THEN  
                                                              ' AND (ici.Is_Blocker = 0 OR  ici.Is_Blocker IS NULL) '  
                                                          ELSE  
                                                              ''  
                                                      END;  
  
    SET @Sql_IVC_Queue_Where = @Sql_IVC_Queue_Where + CASE  
                                                          WHEN @Previously_Chased IS NULL THEN  
                                                              ''  
                                                          WHEN @Previously_Chased = 1 THEN  
                                                              ' AND icccm.Invoice_Collection_Queue_Id IS NOT NULL '  
                                                          WHEN @Previously_Chased = 0 THEN  
                                                              ' AND icccm.Invoice_Collection_Queue_Id IS NULL '  
                                                          ELSE  
                                                              ''  
                                                      END;  
  
  
    SET @Sql_IVC_Queue_Where = @Sql_IVC_Queue_Where + CASE  
                                                          WHEN @Final_Review IS NULL THEN  
                                                              ''  
                                                          WHEN @Final_Review = 1 THEN  
                                                              ' AND icfrlqm.Invoice_Collection_Queue_Id IS NOT NULL'  
                                                          WHEN @Final_Review = 0 THEN  
                                                              ' AND icfrlqm.Invoice_Collection_Queue_Id IS NULL'  
                                                          ELSE  
                                                              ''  
                                                      END;  
  
  
    SET @Sql_IVC_Queue_Where  
        = @Sql_IVC_Queue_Where  
          + CASE  
                WHEN @Next_Action_Start_Date IS NULL THEN  
                    ''  
                ELSE  
                    ' AND icq.Next_Action_Dt >=''' + CAST(@Next_Action_Start_Date AS VARCHAR(20)) + ''''  
            END;  
  
  
    SET @Sql_IVC_Queue_Where  
        = @Sql_IVC_Queue_Where  
          + CASE  
                WHEN @Next_Action_End_Date IS NULL THEN  
                    ''  
                ELSE  
                    ' AND (icq.Next_Action_Dt <=''' + CAST(@Next_Action_End_Date AS VARCHAR(20))  
                    + ''' AND icq.Next_Action_Dt > ''1900-01-01'') '  
            END;  
  
  
    SET @Sql_IVC_Queue_Where  
        = @Sql_IVC_Queue_Where  
          + CASE  
                WHEN @Chase_Created_Start_Date IS NULL THEN  
                    ''  
                ELSE  
                    ' AND iccc.Created_Ts >=''' + CAST(@Chase_Created_Start_Date AS VARCHAR(20)) + ''''  
            END;  
  
  
    SET @Sql_IVC_Queue_Where  
        = @Sql_IVC_Queue_Where  
          + CASE  
                WHEN @Chase_Created_End_Date IS NULL THEN  
                    ''  
                ELSE  
                    ' AND (iccc.Created_Ts <=''' + CAST(@Chase_Created_End_Date AS VARCHAR(20))  
                    + ''' AND iccc.Created_Ts > ''1900-01-01'') '  
            END;  
  
  
    SET @Sql_IVC_Queue_Where  
        = @Sql_IVC_Queue_Where  
          + CASE  
                WHEN @Last_Action_Start_Date IS NULL THEN  
                    ''  
                ELSE  
                    ' AND CAST(icq.Last_Change_Ts as DATE) >=''' + CAST(@Last_Action_Start_Date AS VARCHAR(20)) + ''''  
            END;  
  
  
    SET @Sql_IVC_Queue_Where  
        = @Sql_IVC_Queue_Where  
          + CASE  
                WHEN @Last_Action_End_Date IS NULL THEN  
                    ''  
                ELSE  
                    ' AND (CAST(icq.Last_Change_Ts as DATE) <=''' + CAST(@Last_Action_End_Date AS VARCHAR(20))  
                    + ''' AND icq.Last_Change_Ts > ''1900-01-01'') '  
            END;  
  
  
  
    SET @Sql_IVC_Queue_Where  
        = @Sql_IVC_Queue_Where  
          + CASE  
                WHEN @Days_Since_Last_Chased_From IS NULL THEN  
                    ''  
                ELSE  
                    ' AND lcd.Last_Change_Ts >=''' + CAST(@Days_Since_Last_Chased_From AS VARCHAR(20)) + ''''  
            END;  
  
    SET @Sql_IVC_Queue_Where  
        = @Sql_IVC_Queue_Where  
          + CASE  
                WHEN @Days_Since_Last_Chased_To IS NULL THEN  
                    ''  
                ELSE  
                    ' AND lcd.Last_Change_Ts <=''' + CAST(@Days_Since_Last_Chased_To AS VARCHAR(20)) + ''''  
            END;  
  
    SET @Sql_IVC_Queue_Where  
        = @Sql_IVC_Queue_Where + CASE  
                                     WHEN @Issue_Status IS NULL THEN  
                                         ''  
                                     WHEN @Issue_Status = 'Include' THEN  
                                         '  AND ici.Issue_Status_Cd = 1'  
                                     WHEN @Issue_Status = 'Exclude' THEN  
                                         ' AND (ici.Issue_Status_Cd = 0 OR ici.Invoice_Collection_Queue_Id IS NULL)'  
                                     ELSE  
                                         ''  
                                 END;  
  
  
    SET @Sql_IVC_Queue_Where  
        = @Sql_IVC_Queue_Where + CASE  
                                     WHEN @Record_Type_Cd IS NULL THEN  
                                         ''  
                                     WHEN @Record_Type_Cd_Value = 'Manual ICR' THEN  
                                         ' AND icq.Is_Manual = 1 '  
                                     WHEN @Record_Type_Cd_Value = 'System ICR' THEN  
                                         '  AND icq.Is_Manual = 0 '  
                                     WHEN @Record_Type_Cd_Value = 'System Exception' THEN  
                                         ' AND icq.Invoice_Collection_Exception_Type_Cd IS NOT NULL '  
                                     ELSE  
                                         ''  
                                 END;  
  
    SET @Sql_IVC_Queue_Where  
        = @Sql_IVC_Queue_Where  
          + CASE  
                WHEN @Cnt_Of_Invoice_Collection_Source = 0 THEN  
                    ''  
                ELSE  
                    '  AND        
          (        
              (        
                  EXISTS        
    (        
        SELECT 1        
        FROM #Tvp_Invoice_Collection_Sources tvp        
        WHERE tvp.Column_Name = ''Invoice_Source_Type_Cd''        
              AND cd.Invoice_Source_Type_Cd = CAST(tvp.Column_Value AS INT)        
              AND tvp.Column_Type = cics.Code_Value        
              AND cicst.Code_Value NOT IN ( ''Online'', ''Mail Redirect'' )        
    )        
                  AND EXISTS        
    (        
        SELECT 1        
        FROM #Tvp_Invoice_Collection_Sources tvp        
        WHERE tvp.Column_Name = ''Invoice_Source_Method_of_Contact_Cd''        
              AND cd.Invoice_Source_Method_of_Contact_Cd = CAST(tvp.Column_Value AS INT)        
              AND tvp.Column_Type = cics.Code_Value        
              AND cicst.Code_Value NOT IN ( ''Online'', ''Mail Redirect'' )        
    )        
              )        
              OR (EXISTS        
    (        
        SELECT 1        
        FROM #Tvp_Invoice_Collection_Sources tvp        
        WHERE tvp.Column_Name = ''Invoice_Source_Type_Cd''        
              AND cd.Invoice_Source_Type_Cd = CAST(tvp.Column_Value AS INT)        
              AND tvp.Column_Type = cics.Code_Value        
              AND cicst.Code_Value IN ( ''Online'', ''Mail Redirect'' )        
    )        
                 )        
              OR (EXISTS        
    (        
        SELECT 1        
        FROM #Tvp_Invoice_Collection_Sources tvp        
        WHERE tvp.Column_Name = ''Invoice_Source_Type_Cd''        
              AND cd.Invoice_Source_Type_Cd = CAST(tvp.Column_Value AS INT)        
              AND tvp.Column_Type = cics.Code_Value        
              AND cicst.Code_Value IN ( ''Data Feed'', ''ETL'' )        
    )        
                 )        
 ) '  
            END;  
  
    SET @Sql_IVC_Queue_GroupBy = ' GROUP BY icq.Invoice_Collection_Queue_Id ';  
  
  
    SET @Sql_IVC_Stmt = @Sql_IVC_Queue_Select + @Sql_IVC_Queue_From + @Sql_IVC_Queue_Where + @Sql_IVC_Queue_GroupBy;  
  
  
  
    INSERT INTO #Invoice_Collection_Queue  
    (  
        Invoice_Collection_Queue_Id  
    )  
    EXEC (@Sql_IVC_Queue_Select + @Sql_IVC_Queue_From + @Sql_IVC_Queue_Where + @Sql_IVC_Queue_GroupBy);  
  
  
    SELECT Invoice_Collection_Queue_Id  
    FROM #Invoice_Collection_Queue;  
  
  
    DROP TABLE #Invoice_Collection_Queue;  
    DROP TABLE #Vendor_Dtls;  
    DROP TABLE #Contact_Dtls_For_search;  
    DROP TABLE #Contact_Dtls;  
    DROP TABLE #Invoice_Collection_Account_Config;  
    DROP TABLE #Tvp_Invoice_Collection_Sources;  
  
END;  
  
  
  
  
  
GO
