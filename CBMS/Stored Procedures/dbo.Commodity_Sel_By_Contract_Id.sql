SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Commodity_Sel_By_Contract_Id

DESCRIPTION:

INPUT PARAMETERS:
	Name										DataType		Default		Description
-------------------------------------------------------------------------------------------
	@Account_Commodity_Invoice_Recalc_Type_Id	INT
    @Start_Dt									DATE
    @End_Dt										DATE
    @Invoice_Recalc_Type_Cd						INT
    @User_Info_Id								INT

OUTPUT PARAMETERS:
	Name										DataType		Default		Description
-------------------------------------------------------------------------------------------
	
USAGE EXAMPLES:
-------------------------------------------------------------------------------------------
  
      EXEC Commodity_Sel_By_Contract_Id 
            1



AUTHOR INITIALS:
	Initials	Name
-------------------------------------------------------------------------------------------
	NR			Narayana Reddy
	
MODIFICATIONS
	Initials	Date			Modification
-------------------------------------------------------------------------------------------
	Nr       	2019-07-19		Created for Add Contract.

******/

CREATE PROCEDURE [dbo].[Commodity_Sel_By_Contract_Id]
     (
         @Contract_Id INT
     )
AS
    BEGIN

        SET NOCOUNT ON;


        SELECT
            com.Commodity_Id
            , com.Commodity_Name
        FROM
            dbo.CONTRACT c
            INNER JOIN dbo.Commodity com
                ON com.Commodity_Id = c.COMMODITY_TYPE_ID
        WHERE
            c.CONTRACT_ID = @Contract_Id;

    END;
    ;

GO
GRANT EXECUTE ON  [dbo].[Commodity_Sel_By_Contract_Id] TO [CBMSApplication]
GO
