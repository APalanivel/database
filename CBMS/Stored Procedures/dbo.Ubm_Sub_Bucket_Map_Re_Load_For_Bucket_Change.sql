SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                                                        

 NAME:  dbo.Ubm_Sub_Bucket_Map_Re_Load_For_Bucket_Change                                            
                                                        
 DESCRIPTION:                                                        
   Data Feed  Page ... mapping
                                                        
 INPUT PARAMETERS:                                          
                                                     
 Name									DataType         Default       Description                                        
-------------------------------			--------------------------------------------------------------------------------                                      
@Ubm_Bucket_Charge_Map_Id				INT
@User_Info_Id							INT
                                                              
 OUTPUT PARAMETERS:                                          
                                                           
 Name                        DataType         Default       Description                                        
---------------------------------------------------------------------------------------------------------------                                      
                                                        
 USAGE EXAMPLES:                                                            
---------------------------------------------------------------------------------------------------------------                             
                  
   BEGIN TRAN 
   EXEC [Ubm_Sub_Bucket_Map_Re_Load_For_Bucket_Change] 11233,123                                     
   Rollback TRAN 
                                                       
 AUTHOR INITIALS:                                        
                                       
 Initials              Name                                        
---------------------------------------------------------------------------------------------------------------                                                      
 HKT                  Harish Kumar Tirumandyam                                
                                                         
 MODIFICATIONS:                                      
                                          
 Initials			Date					Modification                                      
------------		----------				-----------------------------------------------------------------------------------------                                      
 HKT				2020-04-24				Created For Data purple.     Data Feed Mapping Page    
                                 
******/
CREATE  PROCEDURE [dbo].[Ubm_Sub_Bucket_Map_Re_Load_For_Bucket_Change]
    (
        @Ubm_Bucket_Charge_Map_Id INT
        , @User_Info_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        BEGIN TRY
            BEGIN TRAN;



            DELETE  FROM
            dbo.Ubm_Bucket_Charge_Ec_Invoice_Sub_Bucket_Map
            WHERE
                Ubm_Bucket_Charge_Ec_Invoice_Sub_Bucket_Map.Ubm_Bucket_Charge_Map_Id = @Ubm_Bucket_Charge_Map_Id;

            INSERT INTO dbo.Ubm_Bucket_Charge_Ec_Invoice_Sub_Bucket_Map
                 (
                     Ubm_Bucket_Charge_Map_Id
                     , State_Id
                     , Ubm_Sub_Bucket_Code
                     , EC_Invoice_Sub_Bucket_Master_Id
                     , Created_User_Id
                     , Last_Change_Ts
                 )
            SELECT
                @Ubm_Bucket_Charge_Map_Id
                , State_Id
                , Sub_Bucket_Name
                , EC_Invoice_Sub_Bucket_Master_Id
                , @User_Info_Id 
                , GETDATE()
            FROM
                dbo.EC_Invoice_Sub_Bucket_Master
            WHERE
                Bucket_Master_Id IN (   SELECT
                                            Bucket_Master_Id
                                        FROM
                                            dbo.UBM_BUCKET_CHARGE_MAP
                                        WHERE
                                            UBM_BUCKET_CHARGE_MAP_ID = @Ubm_Bucket_Charge_Map_Id );


            COMMIT TRAN;
        END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRAN;
                END;

            EXEC dbo.usp_RethrowError;
        END CATCH;


    END;

GO
GRANT EXECUTE ON  [dbo].[Ubm_Sub_Bucket_Map_Re_Load_For_Bucket_Change] TO [CBMSApplication]
GO
