SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                    
/******                        
 NAME: dbo.User_Info_Client_App_Access_Role_Map_Ins_Bulk            
                        
 DESCRIPTION:                        
			To Bulk update the User_Info_Client_App_Access_Role_Map table.                        
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
 @Client_App_Access_Role_Id_List         VARCHAR(MAX)               
 @User_Info_Id_List          VARCHAR(MAX)       
 @Assigned_User_Id           INT                      
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 BEGIN TRAN
           SELECT
            *
           FROM
            dbo.User_Info_Client_App_Access_Role_Map ui
            INNER JOIN dbo.USER_INFO u
                  ON ui.USER_INFO_ID = u.USER_INFO_ID
           WHERE
            u.CLIENT_ID = 235
            AND Client_App_Access_Role_Id IN ( 36, 37 )
            AND u.USER_INFO_ID IN ( 33486, 41121 )
		      
           EXEC [dbo].[User_Info_Client_App_Access_Role_Map_Ins_Bulk] 
            @Client_App_Access_Role_Id_List = '36,37'
           ,@User_Info_Id_List = '33486,41121'
           ,@Assigned_User_Id = 49

           SELECT
            *
           FROM
            dbo.User_Info_Client_App_Access_Role_Map ui
            INNER JOIN dbo.USER_INFO u
                  ON ui.USER_INFO_ID = u.USER_INFO_ID
           WHERE
            u.CLIENT_ID = 235
            AND Client_App_Access_Role_Id IN ( 36, 37 )
            AND u.USER_INFO_ID IN ( 33486, 41121 )
 ROLLBACK	      

DELETE ui
 FROM
      dbo.User_Info_Client_App_Access_Role_Map ui
      INNER JOIN dbo.USER_INFO u
            ON ui.USER_INFO_ID = u.USER_INFO_ID
 WHERE
      u.CLIENT_ID = 235
      AND Client_App_Access_Role_Id in ( 36, 37 )
      AND u.USER_INFO_ID IN ( 33486, 41121 )
                            
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2014-04-23       Created                
                       
******/                 
 

 
CREATE PROCEDURE [dbo].[User_Info_Client_App_Access_Role_Map_Ins_Bulk]
      ( 
       @Client_App_Access_Role_Id_List VARCHAR(MAX)
      ,@User_Info_Id_List VARCHAR(MAX)
      ,@Assigned_User_Id INT )
AS 
BEGIN                
      SET NOCOUNT ON;         
                    
      DECLARE @Client_App_Access_Role_Id_Tbl TABLE
            ( 
             Client_App_Access_Role_Id INT PRIMARY KEY CLUSTERED ) 

      DECLARE @User_Info_Id_Tbl TABLE
            ( 
             User_Info_Id INT PRIMARY KEY CLUSTERED )                                 
                  
                  
      INSERT      INTO @Client_App_Access_Role_Id_Tbl
                  ( 
                   Client_App_Access_Role_Id )
                  SELECT
                        ufn.Segments
                  FROM
                        dbo.ufn_split(@Client_App_Access_Role_Id_List, ',') ufn      


      INSERT      INTO @User_Info_Id_Tbl
                  ( 
                   User_Info_Id )
                  SELECT
                        ufn.Segments
                  FROM
                        dbo.ufn_split(@User_Info_Id_List, ',') ufn                                     
  
      BEGIN TRY                        
            BEGIN TRANSACTION     
              
            INSERT      INTO dbo.User_Info_Client_App_Access_Role_Map
                        ( 
                         Client_App_Access_Role_Id
                        ,User_Info_Id
                        ,Assigned_User_Id
                        ,Assigned_Ts )
                        SELECT
                              urr.Client_App_Access_Role_Id
                             ,ui.User_Info_Id
                             ,@Assigned_User_Id
                             ,GETDATE()
                        FROM
                              @Client_App_Access_Role_Id_Tbl urr
                              CROSS APPLY @User_Info_Id_Tbl ui
                        WHERE
                              NOT EXISTS ( SELECT
                                                1
                                           FROM
                                                dbo.User_Info_Client_App_Access_Role_Map ugm
                                           WHERE
                                                ugm.Client_App_Access_Role_Id = urr.Client_App_Access_Role_Id
                                                AND ugm.User_Info_Id = ui.User_Info_Id )  
                 
                       
            COMMIT TRANSACTION                                 
                                   
                                   
      END TRY                    
      BEGIN CATCH                    
            IF @@TRANCOUNT > 0 
                  BEGIN    
                        ROLLBACK TRANSACTION    
                  END                   
            EXEC dbo.usp_RethrowError                    
      END CATCH  
                       
END 
;
GO
GRANT EXECUTE ON  [dbo].[User_Info_Client_App_Access_Role_Map_Ins_Bulk] TO [CBMSApplication]
GO
