
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********   
NAME:  dbo.Account_Watch_List_Group_SEL  
 
DESCRIPTION:  Used to select account watch list group  

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    

    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:  
	
	Account_Watch_List_Group_SEL

------------------------------------------------------------  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
NK   Nageswara Rao Kosuri         
SP	 Sandeep Pigilam


Initials Date  Modification  
------------------------------------------------------------  
NK	10/21/2009  Created
SP	2014-08-26  for Data Transition,Removed filter "Invoice Processing team"  and added Is_Show_On_Watch_List = 1. 


******/  


CREATE PROCEDURE [dbo].[Account_Watch_List_Group_SEL]
AS 
BEGIN
	
      SET NOCOUNT ON;
	
      SELECT
            ginfo.Group_Info_Id
           ,ginfo.Group_Name
      FROM
            dbo.Group_Info ginfo
      WHERE
            ginfo.Is_Shown_On_Watch_List = 1
      ORDER BY
            ginfo.GROUP_NAME      
	
	
END


;
GO

GRANT EXECUTE ON  [dbo].[Account_Watch_List_Group_SEL] TO [CBMSApplication]
GO
