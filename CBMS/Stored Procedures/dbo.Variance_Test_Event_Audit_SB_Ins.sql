SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME:  
	dbo.Variance_Test_Event_Audit_SB_Ins      

DESCRIPTION:          
	This script excecuted by Service Broker to process input message

INPUT PARAMETERS:          
	Name					DataType			Default		Description          
 -------------------------------------------------------------------------------------------------------    
	@Message				XML								XML string of Variance test event info          
	@Conversation_Handle	UNIQUEIDENTIFER					Conversation Handle that sent the message           

          

OUTPUT PARAMETERS:          
	Name		DataType	Default	Description          
 ------------------------------------------------------------------------------------------------------          
          

 USAGE EXAMPLES:          
 ------------------------------------------------------------------------------------------------------    

 This sp should be executed from SQL Server Service Broker   
 EXEC dbo.Variance_Test_Event_Audit_SB_Ins 
      @Message = '<Variance_Test_Event_Audit>
				   <Variance_Test_Event>
					<Account_Id>1</Account_Id>
					<Service_Month>2014-09-01</Service_Month>
					<Event_Type_Name>C/U Variance Tested</Event_Type_Name>
					<Variance_Routed_To_Queue_Id>16</Variance_Routed_To_Queue_Id>
					<Event_By_User_Id>16</Event_By_User_Id>
					<Variance_Test_Event_Type_Cd>100692</Variance_Test_Event_Type_Cd>
					<Variance_Routed_To_User_Id>16</Variance_Routed_To_User_Id>
					<Event_Ts>2014-09-18T06:29:37.993</Event_Ts>
				  </Variance_Test_Event>
				</Variance_Test_Event_Audit>'
     , -- xml
      @Conversation_Handle = NULL -- uniqueidentifier
 
          

 AUTHOR INITIALS:          
	Initials	Name          
 ------------------------------------------------------------------------------------------------------    
	RR			Raghu Reddy


MODIFICATIONS          
	Initials	Date		Modification          
------------------------------------------------------------------------------------------------------          
	RR			2014-09-17	Created MAINT-3040 Moving non-process dependent auditing to queued update to help performance

******/ 
CREATE PROCEDURE [dbo].[Variance_Test_Event_Audit_SB_Ins]
      ( 
       @Message XML
      ,@Conversation_Handle UNIQUEIDENTIFIER )
AS 
BEGIN

      SET NOCOUNT ON;


      DECLARE
            @Account_Id INT
           ,@Service_Month DATE
           ,@Event_Type_Name VARCHAR(25)
           ,@Variance_Routed_To_Queue_Id INT
           ,@Event_By_User_Id INT
           ,@Variance_Test_Event_Type_Cd INT
           ,@Variance_Routed_To_User_Id INT
           ,@Event_Ts DATETIME

		   
      SELECT
            @Account_Id = aud.vt.value('Account_Id[1]', 'INT')
           ,@Service_Month = aud.vt.value('Service_Month[1]', 'DATE')
           ,@Event_Type_Name = aud.vt.value('Event_Type_Name[1]', 'VARCHAR(25)')
           ,@Variance_Routed_To_Queue_Id = aud.vt.value('Variance_Routed_To_Queue_Id[1]', 'INT')
           ,@Event_By_User_Id = aud.vt.value('Event_By_User_Id[1]', 'INT')
           ,@Variance_Test_Event_Type_Cd = aud.vt.value('Variance_Test_Event_Type_Cd[1]', 'INT')
           ,@Variance_Routed_To_User_Id = aud.vt.value('Variance_Routed_To_User_Id[1]', 'INT')
           ,@Event_Ts = aud.vt.value('Event_Ts[1]', 'DATETIME')
      FROM
            @Message.nodes('/Variance_Test_Event_Audit/Variance_Test_Event') aud ( vt ) 
            
      INSERT      INTO dbo.Variance_Test_Event_Audit
                  ( 
                   Account_Id
                  ,Service_Month
                  ,Variance_Test_Event_Type_Cd
                  ,Variance_Routed_To_User_Id
                  ,Event_By_User_Id
                  ,Event_Ts )
                  SELECT
                        @Account_Id
                       ,@Service_Month
                       ,@Variance_Test_Event_Type_Cd
                       ,@Variance_Routed_To_User_Id
                       ,@Event_By_User_Id
                       ,@Event_Ts


END







GO
