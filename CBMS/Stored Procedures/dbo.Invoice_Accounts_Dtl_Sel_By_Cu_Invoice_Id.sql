SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********       
NAME: dbo.Invoice_Accounts_Dtl_Sel_By_Cu_Invoice_Id
     
DESCRIPTION:  This will fetch all the accounts for the cu_invoice_id
 ,to fill the accounts drop down in invoice details section 
    
INPUT PARAMETERS:        
Name              DataType          Default     Description        
------------------------------------------------------------        
@cu_invoice_id		int
       
OUTPUT PARAMETERS:        
Name              DataType          Default     Description        
------------------------------------------------------------        
USAGE EXAMPLES:   

 
  EXEC CU_Invoice_Accounts_SEL 75254062

	 GRANTEXECUTE
------------------------------------------------------------      

AUTHOR INITIALS:      
Initials	Name      
------------------------------------------------------------      
NR			Narayana Reddy

Initials	 Date		Modification      
------------------------------------------------------------      
NR			2019-11-15	Created For Add Contract.

******/

CREATE PROCEDURE [dbo].[Invoice_Accounts_Dtl_Sel_By_Cu_Invoice_Id]
     (
         @cu_invoice_id AS INT
     )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            cism.Account_ID
            , cism.CU_INVOICE_ID
            , cism.Begin_Dt
            , cism.End_Dt
            , cism.Billing_Days
            , cha.Display_Account_Number AS ACCOUNT_NUMBER
            , ent.ENTITY_NAME
        FROM
            dbo.CU_INVOICE_SERVICE_MONTH cism
            JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = cism.Account_ID
            JOIN dbo.ENTITY ent
                ON ent.ENTITY_NAME = cha.Account_Type
                   AND  ent.ENTITY_DESCRIPTION = 'Account Type'
        WHERE
            cism.CU_INVOICE_ID = @cu_invoice_id
        GROUP BY
            cism.Account_ID
            , cism.CU_INVOICE_ID
            , cism.Begin_Dt
            , cism.End_Dt
            , cism.Billing_Days
            , cha.Display_Account_Number
            , ent.ENTITY_NAME;

    END;

GO
GRANT EXECUTE ON  [dbo].[Invoice_Accounts_Dtl_Sel_By_Cu_Invoice_Id] TO [CBMSApplication]
GO
