SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    dbo.Workflow_Status_Sel
   
    
DESCRIPTION:   
   
    
INPUT PARAMETERS:    
      Name          DataType       Default        Description    
-----------------------------------------------------------------------------    
	 
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec dbo.Workflow_Status_Sel 
	
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	RR          Raghu Reddy
    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	RR          2019-02-07	GRM Proejct.
     
    
******/

CREATE PROC [dbo].[Workflow_Status_Sel]
AS 
BEGIN
      SET NOCOUNT ON;
      
      SELECT
            Workflow_Status_Id
           ,Workflow_Status_Name
      FROM
            Trade.Workflow_Status
      WHERE
            Workflow_Status_Name <> 'DT Created'
                                   
END;

GO
GRANT EXECUTE ON  [dbo].[Workflow_Status_Sel] TO [CBMSApplication]
GO
