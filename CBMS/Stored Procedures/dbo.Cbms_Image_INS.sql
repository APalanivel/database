SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/******  
NAME:  
 
 dbo.Cbms_Image_INS
 
 DESCRIPTION:   
 
 Inserts a record into the CBMS_IMAGE table.
 
 INPUT PARAMETERS:  
 Name			DataType	Default		Description  
------------------------------------------------------------    
		@CBMS_IMAGE_TYPE_ID int,
		@CBMS_DOC_ID varchar(200),
		@DATE_IMAGED datetime,
		@BILLING_DAYS_ADJUSTMENT smallint,
		@CBMS_IMAGE_SIZE decimal(18,0),
		@CONTENT_TYPE varchar(200),
		@INV_SOURCED_IMAGE_ID int,
		@is_reported bit,
		@Cbms_Image_Path varchar(255),
		@App_ConfigID int,
		@CBMS_Image_Directory varchar(255),
		@CBMS_Image_FileName varchar(255),
		@CBMS_Image_Location_Id int

 
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  USAGE EXAMPLES:  
------------------------------------------------------------  

	EXEC dbo.Cbms_Image_INS

AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 GP			Garrett Page    12/15/2009

 MODIFICATIONS   
 Initials	Date		Modification  
------------------------------------------------------------  			    
******/


CREATE PROCEDURE [dbo].[Cbms_Image_INS]
(
		@CBMS_IMAGE_TYPE_ID int = NULL,
		@CBMS_DOC_ID varchar(200),
		@DATE_IMAGED datetime,
		@BILLING_DAYS_ADJUSTMENT smallint = NULL,
		@CBMS_IMAGE_SIZE decimal(18,0) = NULL,
		@CONTENT_TYPE varchar(200) = NULL,
		@INV_SOURCED_IMAGE_ID int = NULL,
		@is_reported bit = NULL,
		@Cbms_Image_Path varchar(255) = NULL,
		@App_ConfigID int = NULL,
		@CBMS_Image_Directory varchar(255) = NULL,
		@CBMS_Image_FileName varchar(255) = NULL,
		@CBMS_Image_Location_Id int = NULL
)
AS
BEGIN

	SET NOCOUNT ON

	INSERT INTO cbms_image
			   ([CBMS_IMAGE_TYPE_ID]
			   ,[CBMS_DOC_ID]
			   ,[DATE_IMAGED]
			   ,[BILLING_DAYS_ADJUSTMENT]
			   ,[CBMS_IMAGE_SIZE]
			   ,[CONTENT_TYPE]
			   ,[INV_SOURCED_IMAGE_ID]
			   ,[is_reported]
			   ,[Cbms_Image_Path]
			   ,[App_ConfigID]
			   ,[CBMS_Image_Directory]
			   ,[CBMS_Image_FileName]
			   ,[CBMS_Image_Location_Id])
		 VALUES
			   (@CBMS_IMAGE_TYPE_ID,
			   @CBMS_DOC_ID,
			   @DATE_IMAGED,
			   @BILLING_DAYS_ADJUSTMENT,
			   @CBMS_IMAGE_SIZE,
			   @CONTENT_TYPE,
			   @INV_SOURCED_IMAGE_ID,
			   @is_reported,
			   @Cbms_Image_Path,
			   @App_ConfigID,
			   @CBMS_Image_Directory,
			   @CBMS_Image_FileName,
			   @CBMS_Image_Location_Id)
			   
		SELECT @@IDENTITY AS CBMS_IMAGE_ID
			   
END


GO
GRANT EXECUTE ON  [dbo].[Cbms_Image_INS] TO [CBMSApplication]
GO
