SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          
NAME:   Security_Role_Client_Hier_INS_Missing_By_Client_Hier       

    
DESCRIPTION: 
	Updates the Security_Role_Client_Hier table from the Client_Hier         
	
      
INPUT PARAMETERS:          
Name			DataType	Default		Description          
------------------------------------------------------------          
@Client_Id		INT			NULL		Specifies client to update. If null Then all 
										clients are updated
                
OUTPUT PARAMETERS:          
Name			DataType	Default		Description          
------------------------------------------------------------ 

         
USAGE EXAMPLES:          
------------------------------------------------------------ 

EXEC Security_Role_Client_Hier_INS_Missing_By_Client_Hier

EXEC Security_Role_Client_Hier_INS_Missing_By_Client_Hier 100
       
     
AUTHOR INITIALS:          
Initials	Name          
------------------------------------------------------------          
CMH			Chad Hattabaugh  
     
     
MODIFICATIONS           
Initials	Date	Modification          
------------------------------------------------------------          
CMH				Created
*****/ 
CREATE PROCEDURE [dbo].[Security_Role_Client_Hier_INS_Missing_By_Client_Hier]
(	@Client_Id		INT			= NULL	)
AS 
BEGIN 
	SET NOCOUNT ON;
	
	INSERT Security_Role_Client_Hier
	(	Security_Role_Id
		,Client_Hier_Id ) 	
	SELECT 
		sr.Security_Role_Id
		,ch.Client_Hier_Id 
	FROM 
		Security_Role sr
		INNER JOIN Core.Client_Hier ch 
			ON sr.Client_Id = ch.Client_Id
		LEFT JOIN Code sgCode 
			ON ch.Sitegroup_Type_cd = sgCode.Code_Id
		LEFT JOIN Security_Role_Client_Hier srch
			ON srch.Client_Hier_Id = ch.Client_Hier_Id
				AND srch.Security_Role_Id = sr.Security_Role_Id
	WHERE 
		ch.Client_Id = isnull(@Client_Id, ch.Client_Id) 
		AND srch.Security_Role_Id IS NULL 
		AND (	ch.Sitegroup_Type_cd IS NULL			-- Client, Site
				OR sgCode.Code_Value = 'Division' )		-- Divisions (Exclude Sitegroups)


END
GO
GRANT EXECUTE ON  [dbo].[Security_Role_Client_Hier_INS_Missing_By_Client_Hier] TO [CBMSApplication]
GO
