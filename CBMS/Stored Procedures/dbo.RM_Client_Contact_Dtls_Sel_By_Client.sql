SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/**

NAME: dbo.RM_Client_Contact_Dtls_Sel_By_Client

    
DESCRIPTION:    

      To get client contact details


INPUT PARAMETERS:    
     NAME					DATATYPE		DEFAULT           DESCRIPTION    
------------------------------------------------------------------------    
	@Client_Id				INT		

                    

OUTPUT PARAMETERS:              
      NAME              DATATYPE    DEFAULT           DESCRIPTION       
-------------------------------------------------------------------------              


USAGE EXAMPLES:              
-------------------------------------------------------------------------              
	EXEC dbo.RM_Client_Contact_Dtls_Sel_By_Client 235
	EXEC dbo.RM_Client_Contact_Dtls_Sel_By_Client 10003

	 

AUTHOR INITIALS:              
	INITIALS	NAME              
------------------------------------------------------------              
    RR			Raghu Reddy


MODIFICATIONS:    
	INITIALS	DATE		MODIFICATION              
------------------------------------------------------------              
	RR 			31-07-2018	Created - Global Risk Management

**/ 
CREATE PROCEDURE [dbo].[RM_Client_Contact_Dtls_Sel_By_Client]
      ( 
       @Client_Id INT
      ,@Start_Index INT = 1
      ,@End_Index INT = 2147483647
      ,@Sort_Key VARCHAR(50) = NULL
      ,@Sort_Order VARCHAR(50) = NULL )
AS 
BEGIN
	
      SET NOCOUNT ON;  
      
      DECLARE @Tbl_ClientContacts AS TABLE
            ( 
             Contact_Info_Id INT
            ,First_Name NVARCHAR(60)
            ,Last_Name NVARCHAR(60)
            ,Email_Address NVARCHAR(150)
            ,Phone_Number NVARCHAR(60)
            ,Mobile_Number NVARCHAR(20)
            ,Fax_Number NVARCHAR(60)
            ,Last_Updated_By NVARCHAR(200)
            ,Row_Num INT
            ,Created_Ts DATETIME )
            
      DECLARE @Total INT
      
      SELECT
            @Sort_Order = ISNULL(@Sort_Order, 'Desc')
           ,@Sort_Key = ISNULL(@Sort_Key, 'lastUpdatedBy')
           
 
           
      INSERT      INTO @Tbl_ClientContacts
                  ( 
                   Contact_Info_Id
                  ,First_Name
                  ,Last_Name
                  ,Email_Address
                  ,Phone_Number
                  ,Mobile_Number
                  ,Fax_Number
                  ,Last_Updated_By
                  ,Row_Num
                  ,Created_Ts )
                  SELECT
                        ccm.Contact_Info_Id
                       ,ci.First_Name
                       ,ci.Last_Name
                       ,ci.Email_Address
                       ,ci.Phone_Number
                       ,ci.Mobile_Number
                       ,ci.Fax_Number
                       ,REPLACE(CONVERT(VARCHAR(20), ci.Last_Change_Ts, 106), ' ', '-') + ' at ' + LTRIM(RIGHT(CONVERT(VARCHAR(20), ci.Last_Change_Ts, 100), 7)) + ' EST by ' + ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Last_Updated_BY
                       ,CASE WHEN @Sort_Order = 'Asc' THEN ROW_NUMBER() OVER ( ORDER BY CASE WHEN @Sort_Key = 'lastUpdatedBy' THEN ci.Last_Change_Ts
                                                                                        END ASC, CASE WHEN @Sort_Key = 'firstName' THEN ci.First_Name
                                                                                                      WHEN @Sort_Key = 'lastName' THEN ci.Last_Name
                                                                                                      WHEN @Sort_Key = 'emailAddress' THEN ci.Email_Address
                                                                                                      WHEN @Sort_Key = 'phoneNumber' THEN ci.Phone_Number
                                                                                                      WHEN @Sort_Key = 'mobileNumber' THEN ci.Mobile_Number
                                                                                                      WHEN @Sort_Key = 'faxNumber' THEN ci.Fax_Number
                                                                                                 END ASC )
                             WHEN @Sort_Order = 'Desc' THEN ROW_NUMBER() OVER ( ORDER BY CASE WHEN @Sort_Key = 'lastUpdatedBy' THEN ci.Last_Change_Ts
                                                                                         END DESC, CASE WHEN @Sort_Key = 'firstName' THEN ci.First_Name
                                                                                                        WHEN @Sort_Key = 'lastName' THEN ci.Last_Name
                                                                                                        WHEN @Sort_Key = 'emailAddress' THEN ci.Email_Address
                                                                                                        WHEN @Sort_Key = 'phoneNumber' THEN ci.Phone_Number
                                                                                                        WHEN @Sort_Key = 'mobileNumber' THEN ci.Mobile_Number
                                                                                                        WHEN @Sort_Key = 'faxNumber' THEN ci.Fax_Number
                                                                                                   END DESC )
                        END
                       ,ci.Created_Ts
                  FROM
                        dbo.Client_Contact_Map ccm
                        INNER JOIN dbo.Contact_Info ci
                              ON ccm.Contact_Info_Id = ci.Contact_Info_Id
                        INNER JOIN dbo.Code cd
                              ON ci.Contact_Type_Cd = cd.Code_Id
                        INNER JOIN dbo.Codeset cs
                              ON cd.Codeset_Id = cs.Codeset_Id
                        INNER JOIN dbo.USER_INFO ui
                              ON ci.Updated_User_Id = ui.USER_INFO_ID
                  WHERE
                        cs.Codeset_Name = 'ContactType'
                        AND cd.Code_Value = 'RM Client Contact'
                        AND ccm.CLIENT_ID = @Client_Id
                        
      SELECT
            @Total = MAX(Row_Num)
      FROM
            @Tbl_ClientContacts
                        
      SELECT
            Contact_Info_Id
           ,First_Name
           ,Last_Name
           ,Email_Address
           ,Phone_Number
           ,Mobile_Number
           ,Fax_Number
           ,Last_Updated_By
           ,Row_Num
           ,@Total AS Total
           ,Created_Ts
      FROM
            @Tbl_ClientContacts
      WHERE
            Row_Num BETWEEN @Start_Index AND @End_Index
      ORDER BY
            Row_Num
     
				

END;

GO
GRANT EXECUTE ON  [dbo].[RM_Client_Contact_Dtls_Sel_By_Client] TO [CBMSApplication]
GO
