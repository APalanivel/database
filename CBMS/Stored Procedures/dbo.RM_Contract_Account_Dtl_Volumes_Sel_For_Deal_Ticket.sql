SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        dbo.RM_Contract_Account_Dtl_Volumes_Sel_For_Deal_Ticket                      
                        
Description:                        
        To get site's hedge configurations  
                        
Input Parameters:                        
    Name    DataType        Default     Description                          
--------------------------------------------------------------------------------  
 @Client_Id   INT  
    @Commodity_Id  INT  
    @Hedge_Type  INT  
    @Start_Dt DATE  
    @End_Dt  DATE  
    @Contract_Id  VARCHAR(MAX)  
    @Site_Id   INT    NULL  
                        
 Output Parameters:                              
 Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
     
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
 EXEC dbo.RM_Client_Hier_Hedge_Config_Sel 11236  
  
 EXEC dbo.Contract_Sel_By_Commodity_Hedge_Type_Period @Client_Id = 11236,@Commodity_Id = 291  
   ,@Start_Dt='2015-01-01',@End_Dt='2017-12-30',@Hedge_Type=586  
    
 SELECT SUM(VOLUME),MONTH_IDENTIFIER FROM dbo.CONTRACT_METER_VOLUME   
 WHERE CONTRACT_ID = 162277 AND MONTH_IDENTIFIER BETWEEN '2017-01-01' AND '2017-06-01' GROUP BY MONTH_IDENTIFIER  
 EXEC dbo.RM_Contract_Account_Dtl_Volumes_Sel_For_Deal_Ticket  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2017-01-01',@End_Dt='2017-06-01',@Hedge_Type=586,@Contract_Id = '162277'  
 EXEC dbo.RM_Contract_Account_Dtl_Volumes_Sel_For_Deal_Ticket  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2017-01-01',@End_Dt='2017-06-01',@Hedge_Type=586,@Contract_Id = '162278'  
 EXEC dbo.RM_Contract_Account_Dtl_Volumes_Sel_For_Deal_Ticket  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2017-01-01',@End_Dt='2017-06-01',@Hedge_Type=586,@Contract_Id = '162277,162278'  
    
 EXEC dbo.RM_Contract_Account_Dtl_Volumes_Sel_For_Deal_Ticket  @Client_Id = 10081,@Commodity_Id = 291  
  ,@Start_Dt='2019-01-01',@End_Dt='2019-01-01',@Hedge_Type=586,@Contract_Id = '149436'  
   
   
  
  
Author Initials:                        
    Initials    Name                        
--------------------------------------------------------------------------------  
    RR          Raghu Reddy     
                         
 Modifications:                        
    Initials Date           Modification                        
--------------------------------------------------------------------------------  
 PR          02-11-2018    Created GRM  
                       
******/
CREATE PROCEDURE [dbo].[RM_Contract_Account_Dtl_Volumes_Sel_For_Deal_Ticket]
    (
        @Client_Id INT
        , @Commodity_Id INT
        , @Hedge_Type INT
        , @Start_Dt DATE
        , @End_Dt DATE
        , @Contract_Id VARCHAR(MAX)
        , @Uom_Id INT = NULL
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Total_Cnt INT;

        CREATE TABLE #Volumes
             (
                 Client_Hier_Id INT
                 , Site_Id INT
                 , Site_name VARCHAR(200)
                 , Account_Id INT
                 , Account_Number VARCHAR(50)
                 , Contract_Vendor VARCHAR(500)
                 , Service_Month DATE
                 , Contract_Volume DECIMAL(28, 0)
                 , Uom_Id INT
             );

        DECLARE @Common_Uom_Id INT;

        DECLARE @Hedge_Type_Input VARCHAR(200);

        SELECT
            @Hedge_Type_Input = ENTITY_NAME
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_ID = @Hedge_Type;


        ----------------------To get site own config  
        INSERT INTO #Volumes
             (
                 Client_Hier_Id
                 , Site_Id
                 , Site_name
                 , Account_Id
                 , Account_Number
                 , Contract_Vendor
                 , Service_Month
                 , Contract_Volume
                 , Uom_Id
             )
        SELECT
            cha.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
            , cha.Account_Id
            , cha.Account_Number
            , con.ED_CONTRACT_NUMBER + '(' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_START_DATE, 106), ' ', '-')
              + ' - ' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_END_DATE, 106), ' ', '-') + ')/'
              + suppacc.Account_Vendor_Name AS Contract_Vendor
            , dd.DATE_D AS Service_Month
            , SUM(ISNULL(cmv.VOLUME * cuc.CONVERSION_FACTOR, 0)) AS Contract_Volume
            , ISNULL(@Uom_Id, chob.RM_Forecast_UOM_Type_Id) AS Uom_Id
        FROM
            meta.DATE_DIM dd
            CROSS JOIN Core.Client_Hier ch
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON ch.Client_Hier_Id = chob.Client_Hier_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY et
                ON et.ENTITY_ID = chhc.Hedge_Type_Id
            INNER JOIN Core.Client_Hier_Account cha
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN Core.Client_Hier_Account suppacc
                ON cha.Meter_Id = suppacc.Meter_Id
            INNER JOIN dbo.CONTRACT con
                ON con.CONTRACT_ID = suppacc.Supplier_Contract_ID
            LEFT JOIN dbo.CONTRACT_METER_VOLUME cmv
                ON cha.Meter_Id = cmv.METER_ID
                   AND  suppacc.Supplier_Contract_ID = cmv.CONTRACT_ID
                   AND  CAST(cmv.MONTH_IDENTIFIER AS DATE) = dd.DATE_D
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = cmv.UNIT_TYPE_ID
                   AND  cuc.CONVERTED_UNIT_ID = ISNULL(@Uom_Id, chob.RM_Forecast_UOM_Type_Id)
        WHERE
            ch.Site_Id > 0
            AND ch.Client_Id = @Client_Id
            AND chob.Commodity_Id = @Commodity_Id
            AND (   @Start_Dt BETWEEN chhc.Config_Start_Dt
                              AND     chhc.Config_End_Dt
                    OR  @End_Dt BETWEEN chhc.Config_Start_Dt
                                AND     chhc.Config_End_Dt
                    OR  chhc.Config_Start_Dt BETWEEN @Start_Dt
                                             AND     @End_Dt
                    OR  chhc.Config_End_Dt BETWEEN @Start_Dt
                                           AND     @End_Dt)
            AND (   (   @Hedge_Type_Input = 'Physical'
                        AND et.ENTITY_NAME IN ( 'Physical', 'Physical & Financial' ))
                    OR  (   @Hedge_Type_Input = 'Financial'
                            AND et.ENTITY_NAME IN ( 'Financial', 'Physical & Financial' )))
            AND CAST(dd.DATE_D AS DATE) BETWEEN @Start_Dt
                                        AND     @End_Dt
            AND EXISTS (   SELECT
                                1
                           FROM
                                dbo.ufn_split(@Contract_Id, ',') con
                           WHERE
                                suppacc.Supplier_Contract_ID = CAST(Segments AS INT))
            AND cha.Account_Type = 'Utility'
            AND suppacc.Account_Type = 'Supplier'
        GROUP BY
            cha.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
            , cha.Account_Id
            , cha.Account_Number
            , con.ED_CONTRACT_NUMBER + '(' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_START_DATE, 106), ' ', '-')
              + ' - ' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_END_DATE, 106), ' ', '-') + ')/'
              + suppacc.Account_Vendor_Name
            , dd.DATE_D
            --,cmv.VOLUME * cuc.CONVERSION_FACTOR  
            , ISNULL(@Uom_Id, chob.RM_Forecast_UOM_Type_Id);



        ----------------------To get default config                         
        INSERT INTO #Volumes
             (
                 Client_Hier_Id
                 , Site_Id
                 , Site_name
                 , Account_Id
                 , Account_Number
                 , Contract_Vendor
                 , Service_Month
                 , Contract_Volume
                 , Uom_Id
             )
        SELECT
            cha.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
            , cha.Account_Id
            , cha.Account_Number
            , con.ED_CONTRACT_NUMBER + '(' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_START_DATE, 106), ' ', '-')
              + ' - ' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_END_DATE, 106), ' ', '-') + ')/'
              + suppacc.Account_Vendor_Name AS Contract_Vendor
            , dd.DATE_D AS Service_Month
            , SUM(ISNULL(cmv.VOLUME * cuc.CONVERSION_FACTOR, 0)) AS Contract_Volume
            , ISNULL(@Uom_Id, chob.RM_Forecast_UOM_Type_Id) AS Uom_Id
        FROM
            meta.DATE_DIM dd
            CROSS JOIN Core.Client_Hier ch
            INNER JOIN Core.Client_Hier chclient
                ON ch.Client_Id = chclient.Client_Id
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON chclient.Client_Hier_Id = chob.Client_Hier_Id
                   AND  ch.Country_Id = chob.Country_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY et
                ON et.ENTITY_ID = chhc.Hedge_Type_Id
            INNER JOIN Core.Client_Hier_Account cha
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN Core.Client_Hier_Account suppacc
                ON cha.Meter_Id = suppacc.Meter_Id
            INNER JOIN dbo.CONTRACT con
                ON con.CONTRACT_ID = suppacc.Supplier_Contract_ID
            LEFT JOIN dbo.CONTRACT_METER_VOLUME cmv
                ON cha.Meter_Id = cmv.METER_ID
                   AND  suppacc.Supplier_Contract_ID = cmv.CONTRACT_ID
                   AND  CAST(cmv.MONTH_IDENTIFIER AS DATE) = dd.DATE_D
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = cmv.UNIT_TYPE_ID
                   AND  cuc.CONVERTED_UNIT_ID = ISNULL(@Uom_Id, chob.RM_Forecast_UOM_Type_Id)
        WHERE
            ch.Site_Id > 0
            AND ch.Client_Id = @Client_Id
            AND chclient.Sitegroup_Id = 0
            AND chob.Commodity_Id = @Commodity_Id
            AND (   @Start_Dt BETWEEN chhc.Config_Start_Dt
                              AND     chhc.Config_End_Dt
                    OR  @End_Dt BETWEEN chhc.Config_Start_Dt
                                AND     chhc.Config_End_Dt
                    OR  chhc.Config_Start_Dt BETWEEN @Start_Dt
                                             AND     @End_Dt
                    OR  chhc.Config_End_Dt BETWEEN @Start_Dt
                                           AND     @End_Dt)
            AND (   (   @Hedge_Type_Input = 'Physical'
                        AND et.ENTITY_NAME IN ( 'Physical', 'Physical & Financial' ))
                    OR  (   @Hedge_Type_Input = 'Financial'
                            AND et.ENTITY_NAME IN ( 'Financial', 'Physical & Financial' )))
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.RM_Client_Hier_Onboard siteob1
                               WHERE
                                    siteob1.Client_Hier_Id = ch.Client_Hier_Id
                                    AND siteob1.Commodity_Id = chob.Commodity_Id)
            AND CAST(dd.DATE_D AS DATE) BETWEEN @Start_Dt
                                        AND     @End_Dt
            AND EXISTS (   SELECT
                                1
                           FROM
                                dbo.ufn_split(@Contract_Id, ',') con
                           WHERE
                                suppacc.Supplier_Contract_ID = CAST(Segments AS INT))
            AND cha.Account_Type = 'Utility'
            AND suppacc.Account_Type = 'Supplier'
        GROUP BY
            cha.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
            , cha.Account_Id
            , cha.Account_Number
            , con.ED_CONTRACT_NUMBER + '(' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_START_DATE, 106), ' ', '-')
              + ' - ' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_END_DATE, 106), ' ', '-') + ')/'
              + suppacc.Account_Vendor_Name
            , dd.DATE_D
            --,cmv.VOLUME * cuc.CONVERSION_FACTOR  
            , ISNULL(@Uom_Id, chob.RM_Forecast_UOM_Type_Id);




        SELECT
            Client_Hier_Id
            , Site_Id
            , Site_name
            , Account_Id
            , Account_Number
            , Contract_Vendor
            , Service_Month
            , Contract_Volume
            , Uom_Id
            , DENSE_RANK() OVER (ORDER BY
                                     Account_Id) AS Row_Num
        INTO
            #Final_Volumes
        FROM
            #Volumes;



        SELECT  @Total_Cnt = MAX(fv.Row_Num)FROM    #Final_Volumes fv;

        SELECT
            fv.Client_Hier_Id
            , fv.Site_Id
            , fv.Site_name
            , fv.Account_Id
            , fv.Account_Number
            , fv.Contract_Vendor
            , fv.Service_Month
            , fv.Contract_Volume
            , fv.Uom_Id
            , fv.Row_Num
            , @Total_Cnt AS Total_Cnt
        FROM
            #Final_Volumes fv
        WHERE
            fv.Row_Num BETWEEN @Start_Index
                       AND     @End_Index
        ORDER BY
            fv.Row_Num;

    END;


GO
GRANT EXECUTE ON  [dbo].[RM_Contract_Account_Dtl_Volumes_Sel_For_Deal_Ticket] TO [CBMSApplication]
GO
