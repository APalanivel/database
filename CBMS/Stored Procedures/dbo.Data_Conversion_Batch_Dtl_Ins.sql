SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********    
    
NAME:                             
     dbo.Data_Conversion_Batch_Dtl_Ins    
    
DESCRIPTION:                                            
    
 INPUT PARAMETERS:                                            
    
 Name                        DataType         Default       Description                                          
---------------------------------------------------------------------------------------------------------------                                        
 @Data_Conversion_Batch_Id   INT                                  
    
 OUTPUT PARAMETERS:                                            
    
 Name                        DataType         Default       Description                                          
---------------------------------------------------------------------------------------------------------------                                        
    
 USAGE EXAMPLES:                                                              
---------------------------------------------------------------------------------------------------------------                                                              
    
 BEGIN TRAN                                  
 EXEC [dbo].Data_Conversion_Batch_Dtl_Ins                                   
      @Data_Conversion_Batch_Id = 152                                  
 ROLLBACK TRAN                                     
    
    
 AUTHOR INITIALS:                                          
    
 Initials   Name                                          
---------------------------------------------------------------------------------------------------------------                                                        
 AKR        Ashok Kumar Raju    
 RR   Raghu Reddy       
 SP   Sandeep Pigilam          
 ABK  Aditya Bharadwaj .K                                                               
    
 MODIFICATIONS:                                        
    
 Initials Date        Modification                                        
---------------------------------------------------------------------------------------------------------------                                        
 AKR  2014-06-01  Created.                                  
 AKR  2014-08-18  Added script to eliminate the records, when there are no commodities mapped  at account level                  
 AKR  2014-09-04  Added the code to validate Sitegroup_Site validations.      
 RR   2015-11-30 Global Sourcing - Phase2 - Added Validation on new column Meter_Type     
 SP   2016-05-19  For Client Conversion added new logic to handle Client_Conversion_Posting table changes.      
 NR   2016-09-13 MAINT-4233 Added Validation on new column Meter_Type,Recalc Type,Alternate Account number.        
 HG   2017-03-08 CPH changes,     
      - replaced COB Posted Utility flag with the new columns    
      - Addeded validation for the consolidated billing congiguration    
 RR   2017-05-08 SE2017-32 - New template for DMO supplier accounts conversion    
 ABK  2018-08-21 DDCR-35 - Added New if clause for meter number update package.  
 ABK  2018-09-05 DDCR-36 - Added new IF Clause for Site Name and Site Ref Number update package  
 ABK  2018-10-08  DDCR-34 - Added new IF Claise for moving sites to new division package.  
 RKV        2020-01-20 Added Validation "UBM Account ID is already assigned to another Account with in the UBM" as part of  
      Multi Account UBM Changes  
*********/  
CREATE PROCEDURE [dbo].[Data_Conversion_Batch_Dtl_Ins]  
     (  
         @Data_Conversion_Batch_Id INT  
     )  
AS  
    BEGIN  
  
        SET NOCOUNT ON;  
        CREATE TABLE #Site_Commodity  
             (  
                 Site_Client_Hier_Id INT  
                 , Site_Id INT  
                 , Commodity_Id INT  
                 , Division_Client_Hier_Id INT  
                 , Client_Client_Hier_Id INT  
                 , PRIMARY KEY CLUSTERED  
                   (  
                       Site_Client_Hier_Id  
                       , Commodity_Id  
     )  
             );  
  
        CREATE TABLE #Site_Commodity_DMO_Config  
             (  
                 Site_Client_Hier_Id INT  
                 , Site_Id INT  
                 , Commodity_Id INT  
                 , Division_Client_Hier_Id INT  
                 , DMO_Start_Dt DATE  
                 , DMO_End_Dt DATE  
             );  
  
        CREATE TABLE #Unique_DMO_List  
             (  
                 Supplier_ACCOUNT_NUMBER VARCHAR(200) NULL  
                 , Supplier_Alternate_Account_Number NVARCHAR(200) NULL  
                 , DMO_Supplier_Account_Start_Date DATE NULL  
                 , DMO_Supplier_Account_End_Date DATE NULL  
                 , Vendor_ID INT NULL  
                 , Has_Multi_Site_Meter BIT DEFAULT (0)  
             );  
  
        DECLARE  
            @Conversion_Type VARCHAR(25)  
            , @Df_Invoice_Reclac_Type_Start_Dt DATE;  
  
        SELECT  
            @Conversion_Type = Conversion_Type  
        FROM  
            ETL.Data_Conversion_Batch  
        WHERE  
            Data_Conversion_Batch_Id = @Data_Conversion_Batch_Id;  
  
        SELECT  
            @Df_Invoice_Reclac_Type_Start_Dt = ac.App_Config_Value  
        FROM  
            dbo.App_Config ac  
        WHERE  
            ac.App_Config_Cd = 'Account_Commodity_Invoice_Reclac_Type_Start_Dt';  
  
        IF @Conversion_Type = 'Client'  
            BEGIN  
  
                PRINT '';  
            /*    
 /* delete the rows that are imported as nulls                                    
  this happens when  user uses the same excel template as last used, and the used excel rows are less than the last used */    
    
                  DELETE FROM    
                        Analysis.dbo.Client_Conversion_Posting    
                  WHERE    
                        Client_Name IS NULL    
                        AND CEM IS NULL    
                        AND CSA IS NULL    
                        AND [Fiscal Year Starts] IS NULL    
                        AND [Risk Tolerance Type] IS NULL;    
    
 /*Client_Name is Imported as NULL*/                          
                  INSERT      INTO ETL.Data_Conversion_Batch_Dtl    
                              (     
                               Data_Conversion_Batch_Id    
                              ,Client_Name    
                              ,CEM    
                              ,CSA    
                              ,client_type    
                              ,[Fiscal Year Starts]    
                              ,[Risk Tolerance Type]    
                              ,Error_Msg )    
                              SELECT    
                                    @Data_Conversion_Batch_Id    
                                   ,Client_Name    
                                   ,CEM    
                                   ,CSA    
                                   ,client_type    
                                   ,[Fiscal Year Starts]    
                                   ,[Risk Tolerance Type]    
                                   ,'CLIENT_NAME is Imported as NULL' ERROR_Msg    
                              FROM    
                                    Analysis.dbo.Client_Conversion_Posting    
                              WHERE    
                                    Client_Name IS NULL;    
    
                  DELETE    
                        Analysis.dbo.Client_Conversion_Posting    
                  WHERE    
                        Client_Name IS NULL;                                      
    
    --Validation for CEM    
    
                  INSERT      INTO ETL.Data_Conversion_Batch_Dtl    
                              (     
                               Data_Conversion_Batch_Id    
                              ,Client_Name    
                              ,CEM    
                              ,CSA    
                              ,client_type    
                              ,[Fiscal Year Starts]    
                              ,[Risk Tolerance Type]    
                              ,Error_Msg )    
                              SELECT    
                                    @Data_Conversion_Batch_Id    
                                   ,Client_Name    
              ,CEM    
                                  ,CSA    
          ,client_type    
                                   ,[Fiscal Year Starts]    
                                   ,[Risk Tolerance Type]    
                                   ,'CEM is not valid' ERROR_Msg    
                              FROM    
     Analysis.dbo.Client_Conversion_Posting ccp    
          LEFT JOIN CBMS.dbo.USER_INFO ui    
                                          ON ui.FIRST_NAME + ' ' + ui.LAST_NAME = ccp.CEM    
                              WHERE    
                                    ui.FIRST_NAME IS NULL;    
    
                  DELETE    
                        ccp    
                  FROM    
               Analysis.dbo.Client_Conversion_Posting ccp    
                        LEFT JOIN CBMS.dbo.USER_INFO ui    
                              ON ui.FIRST_NAME + ' ' + ui.LAST_NAME = ccp.CEM    
                  WHERE    
                        ui.FIRST_NAME IS NULL;    
    
    --Validation for CSA    
                  INSERT      INTO ETL.Data_Conversion_Batch_Dtl    
                              (     
                               Data_Conversion_Batch_Id    
                              ,Client_Name    
                              ,CEM    
                              ,CSA    
                              ,client_type    
                              ,[Fiscal Year Starts]    
                              ,[Risk Tolerance Type]    
                              ,Error_Msg )    
                              SELECT    
                                    @Data_Conversion_Batch_Id    
                                   ,Client_Name    
                                   ,CEM    
                                   ,CSA    
                                   ,client_type    
                                   ,[Fiscal Year Starts]    
                                   ,[Risk Tolerance Type]    
                                   ,'CEM is not valid' ERROR_Msg    
                              FROM    
                                    Analysis.dbo.Client_Conversion_Posting ccp    
                                    LEFT JOIN CBMS.dbo.USER_INFO ui    
                                          ON ui.FIRST_NAME + ' ' + ui.LAST_NAME = ccp.CSA    
                              WHERE    
                                    ui.FIRST_NAME IS NULL;    
                                         
                  DELETE    
                        ccp    
                  FROM    
                        Analysis.dbo.Client_Conversion_Posting ccp    
                        LEFT JOIN CBMS.dbo.USER_INFO ui    
                              ON ui.FIRST_NAME + ' ' + ui.LAST_NAME = ccp.CSA    
                  WHERE    
                        ui.FIRST_NAME IS NULL;    
                            
  */  
  
  
            END;  
  
        IF @Conversion_Type = 'Account'  
            BEGIN  
  
                /* Updating Data_Conversion_Batch_Id for the error rows that are inserted to Data_Conversion_Batch_Dtl during excel inport  */  
  
                UPDATE  
                    ETL.Data_Conversion_Batch_Dtl  
                SET  
                    Data_Conversion_Batch_Id = @Data_Conversion_Batch_Id  
                WHERE  
                    Data_Conversion_Batch_Id = -1  
                    AND Is_Data_Validation_Passed = 0;  
  
  
                /* delete the rows that are imported as nulls                                    
   this happens when  user uses the same excel template as last used, and the used excel rows are less than the last used */  
                DELETE  FROM  
                #Data_Conversion_Accounts_Stage  
                WHERE  
                    [Site id] IS NULL  
                    AND [Vendor ID] IS NULL  
                    AND [ACCOUNT NUMBER] IS NULL  
                    AND METER IS NULL  
                    AND [Consumption Level ID] IS NULL  
                    AND Commodity IS NULL  
                    AND DEO IS NULL  
                    AND [Purchase Method] IS NULL  
                    AND [Rate ID] IS NULL  
                    AND [UBM Account ID] IS NULL;  
  
  
                --Site Id OR Vendor ID OR ACCOUNT NUMBER is Imported as null    
                UPDATE  
                    #Data_Conversion_Accounts_Stage  
                SET  
                    Error_Msg = 'Site Id OR Vendor Id OR Account number is imported as null'  
                    , Is_Validation_Passed = 0  
                WHERE  
                    (   [Site id] IS NULL  
                        OR  [Vendor ID] IS NULL  
                        OR  [ACCOUNT NUMBER] IS NULL  
                        OR  METER IS NULL  
                        OR  [Consumption Level ID] IS NULL)  
                    AND Is_Validation_Passed = 1;  
  
                -- Meter Number Length > 50    
                UPDATE  
                    #Data_Conversion_Accounts_Stage  
                SET  
                    Error_Msg = 'Meter Number Length > 50'  
                    , Is_Validation_Passed = 0  
                FROM  
                    #Data_Conversion_Accounts_Stage  
                WHERE  
                    LEN(METER) > 50  
                    AND Is_Validation_Passed = 1;  
  
                -- deleting sites from staging table which are not present in Site table    
                UPDATE  
                    cca  
                SET  
                    Error_Msg = 'Site_Id is not valid'  
                    , Is_Validation_Passed = 0  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                WHERE  
                    NOT EXISTS (SELECT  1 FROM  dbo.SITE s WHERE s.SITE_ID = cca.[Site id])  
                    AND Is_Validation_Passed = 1;  
  
                UPDATE  
                    cca  
                SET  
                    Error_Msg = 'Account vendor is not valid or the vendor is not mapped to this commodity'  
                    , Is_Validation_Passed = 0  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                WHERE  
                    NOT EXISTS (   SELECT  
                                        1  
                                   FROM  
                                        CBMS.dbo.VENDOR_COMMODITY_MAP vcm  
                                   WHERE  
                                        vcm.COMMODITY_TYPE_ID = cca.Commodity  
                                        AND vcm.VENDOR_ID = cca.[Vendor ID])  
                    AND Is_Validation_Passed = 1;  
  
                UPDATE  
                    cca  
                SET  
                    Error_Msg = 'Rate is not mapped to correct Vendor/Commodity'  
                    , Is_Validation_Passed = 0  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                WHERE  
                    NOT EXISTS (   SELECT  
                                        1  
                                   FROM  
                                        CBMS.dbo.RATE vcm  
                                   WHERE  
                                        vcm.COMMODITY_TYPE_ID = cca.Commodity  
                                        AND vcm.RATE_ID = cca.[Rate ID]  
                                        AND vcm.VENDOR_ID = cca.[Vendor ID])  
                    AND Is_Validation_Passed = 1;  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Commodity is not valid'  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                WHERE  
                    NOT EXISTS (SELECT  1 FROM  CBMS.dbo.Commodity c WHERE  c.Commodity_Id = cca.Commodity)  
                    AND Is_Validation_Passed = 1;  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Commodity is not defined at Account Level'  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                    INNER JOIN CBMS.dbo.SITE s  
                        ON cca.[Site id] = s.SITE_ID  
                WHERE  
                    NOT EXISTS (   SELECT  
                                        1  
                                   FROM  
                                        CBMS.dbo.Commodity c  
                                        INNER JOIN Core.Client_Commodity ccc  
                                            ON c.Commodity_Id = ccc.Commodity_Id  
                                        INNER JOIN dbo.Code cc  
                                            ON ccc.Hier_Level_Cd = cc.Code_Id  
                                        INNER JOIN dbo.Code cl  
                                            ON cl.Code_Id = ccc.Commodity_Service_Cd  
                                   WHERE  
                                        cc.Code_Value = 'Account'  
                                        AND cl.Code_Value = 'Invoice'  
                                        AND ccc.Client_Id = s.Client_Id  
                                        AND ccc.Commodity_Id = cca.Commodity)  
                    AND Is_Validation_Passed = 1;  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Account meter and vendor are same but rates are different'  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                WHERE  
                    EXISTS (   SELECT  
                                    1  
                               FROM  
                                    #Data_Conversion_Accounts_Stage cca_a  
                                    JOIN #Data_Conversion_Accounts_Stage cca_b  
                                        ON cca_a.[ACCOUNT NUMBER] = cca_b.[ACCOUNT NUMBER]  
                                           AND  cca_a.METER = cca_b.METER  
                                           AND  cca_a.[Vendor ID] = cca_b.[Vendor ID]  
                                           AND  cca_a.[Rate ID] <> cca_b.[Rate ID]  
                               WHERE  
                                    cca.[ACCOUNT NUMBER] = cca_a.[ACCOUNT NUMBER]  
                                    AND cca.[Site id] = cca_a.[Site id]  
                                    AND cca.METER = cca_a.METER  
                                    AND cca.[Vendor ID] = cca_a.[Vendor ID])  
                    AND cca.Is_Validation_Passed = 1;  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Site Account and commodity are same but consumption levels are different'  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                WHERE  
                    EXISTS (   SELECT  
                                    1  
                               FROM  
                                    #Data_Conversion_Accounts_Stage cca_a  
                                    JOIN #Data_Conversion_Accounts_Stage cca_b  
                                        ON cca_a.[ACCOUNT NUMBER] = cca_b.[ACCOUNT NUMBER]  
                                           AND  cca_a.Commodity = cca_b.Commodity  
                                           AND  cca_a.[Site id] = cca_b.[Site id]  
                                           AND  cca_a.[Consumption Level ID] <> cca_b.[Consumption Level ID]  
                               WHERE  
                                    cca.[ACCOUNT NUMBER] = cca_a.[ACCOUNT NUMBER]  
                                    AND cca.Commodity = cca_a.Commodity  
                                    AND cca.[Site id] = cca_a.[Site id])  
                    AND cca.Is_Validation_Passed = 1;  
  
                UPDATE  
      cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Vendor provided is under a different state'  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                    JOIN CBMS.dbo.SITE s  
                        ON s.SITE_ID = cca.[Site id]  
                    JOIN CBMS.dbo.ADDRESS a  
                        ON a.ADDRESS_ID = s.PRIMARY_ADDRESS_ID  
                    LEFT JOIN CBMS.dbo.VENDOR_STATE_MAP vsm  
                        ON vsm.VENDOR_ID = cca.[Vendor ID]  
                           AND  vsm.STATE_ID <> a.STATE_ID  
                WHERE  
                    vsm.STATE_ID IS NOT NULL  
                    AND cca.Is_Validation_Passed = 1;  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Service level should be one of A/B/C/D'  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                WHERE  
                    [Service LEVEL] NOT IN ( SELECT ENTITY_NAME FROM    CBMS.dbo.ENTITY WHERE  ENTITY_TYPE = 708 )  
                    AND cca.Is_Validation_Passed = 1;  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Purchase Method is not valid should be Transport/Tariff/Switchable'  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                WHERE  
                    (   cca.[Purchase Method] IS NULL  
                        OR  cca.[Purchase Method] NOT IN (   SELECT  
                                                                    ENTITY_NAME  
                                                             FROM  
                                                                    CBMS.dbo.ENTITY  
                                                             WHERE  
                                                                 ENTITY_DESCRIPTION = 'Purchase Method' ))  
                    AND cca.Is_Validation_Passed = 1;  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'UBM is Not Valid'  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                WHERE  
                    cca.UBM IS NOT NULL  
                    AND LEN(LTRIM(RTRIM(cca.UBM))) > 1  
                    AND LTRIM(RTRIM(cca.UBM)) NOT IN ( SELECT   u.UBM_NAME FROM dbo.UBM u )  
                    AND cca.Is_Validation_Passed = 1;  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'UBM Account ID is already assigned to another Account with in the UBM'  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
     INNER JOIN dbo.UBM u  
     ON u.UBM_NAME = cca.UBM  
                WHERE  
                    cca.[UBM Account ID] IS NOT NULL  
                    AND LEN(LTRIM(RTRIM(cca.[UBM Account ID]))) > 1  
                    AND EXISTS (   SELECT  
                                        1  
                                   FROM  
                                        dbo.Account_Ubm_Account_Code_Map auacm  
                                   WHERE  
                                        auacm.Ubm_Id = u.UBM_ID  
                                        AND auacm.Ubm_Account_Code = cca.[UBM Account ID])  
                    AND cca.Is_Validation_Passed = 1;  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'DEO is not valid should be Yes/No'  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                WHERE  
                    ISNULL(DEO, 'Invalid') NOT IN ( 'Yes', 'No' )  
                    AND cca.Is_Validation_Passed = 1;  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Consumption level is not valid for commodity'  
                FROM  
                    CBMS.dbo.Variance_Consumption_Level vcl  
                    INNER JOIN #Data_Conversion_Accounts_Stage cca  
                        ON cca.[Consumption Level ID] = vcl.Variance_Consumption_Level_Id  
                WHERE  
                    vcl.Commodity_Id != cca.Commodity  
                    AND cca.Is_Validation_Passed = 1;  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Is Default to Primary provided is not valid it should be Yes/No'  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                WHERE  
                    ISNULL([Is Default To Primary], 'Invalid') NOT IN ( 'Yes', 'No' )  
                    AND cca.Is_Validation_Passed = 1;  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Meter Address provided is not valid'  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                    INNER JOIN CBMS.dbo.SITE s  
                        ON s.SITE_ID = cca.[Site id]  
                    LEFT JOIN CBMS.dbo.ADDRESS ads  
                        ON ads.ADDRESS_PARENT_ID = s.SITE_ID  
                           AND  cca.[Site Address] = ads.ADDRESS_LINE1  
                WHERE  
                    ads.ADDRESS_LINE1 IS NULL  
                    AND [Is Default To Primary] = 'No';  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Meters Already Exists in the system with same account/vendor/site'  
                FROM  
                    CBMS.dbo.ACCOUNT a  
                    JOIN #Data_Conversion_Accounts_Stage cca  
                        ON a.SITE_ID = cca.[Site id]  
                           AND  a.ACCOUNT_NUMBER = cca.[ACCOUNT NUMBER]  
                           AND  a.VENDOR_ID = cca.[Vendor ID]  
                    JOIN CBMS.dbo.METER m  
                        ON m.ACCOUNT_ID = a.ACCOUNT_ID  
                           AND  m.METER_NUMBER = cca.METER  
                WHERE  
                    cca.Is_Validation_Passed = 1;  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Meter Type is Not Valid'  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                WHERE  
                    cca.Meter_Type IS NOT NULL  
                    AND LEN(LTRIM(RTRIM(cca.Meter_Type))) > 1  
                    AND LTRIM(RTRIM(cca.Meter_Type)) NOT IN (   SELECT  
                                                                    cd.Code_Value  
                                                                FROM  
                                                                    dbo.Codeset cs  
                                                                    JOIN dbo.Code cd  
                                                                        ON cs.Codeset_Id = cd.Codeset_Id  
                                                                WHERE  
                                                                    cs.Codeset_Name = 'Meter Type' )  
                    AND cca.Is_Validation_Passed = 1;  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Recalc Type is Not Valid'  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                WHERE  
                    cca.Recalc_Type IS NOT NULL  
                    AND LEN(LTRIM(RTRIM(cca.Recalc_Type))) > 1  
                    AND LTRIM(RTRIM(cca.Recalc_Type)) NOT IN (   SELECT  
                                                                        cd.Code_Value  
                                                                 FROM  
                                                                        dbo.Codeset cs  
                                                                        JOIN dbo.Code cd  
                                                                            ON cs.Codeset_Id = cd.Codeset_Id  
                                                                 WHERE  
                                                                     cs.Codeset_Name = 'Recalc Type' )  
                    AND cca.Is_Validation_Passed = 1;  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Recalc Type start date should be less than end date'  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                WHERE  
                    (   cca.Recalc_Type_Start_Dt IS NOT NULL  
                        AND cca.Recalc_Type_End_Dt IS NOT NULL  
                        AND cca.Recalc_Type_Start_Dt > cca.Recalc_Type_End_Dt)  
                    AND cca.Is_Validation_Passed = 1;  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Recalc Type start date should be less than end date'  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                WHERE  
                    (   cca.Recalc_Type_Start_Dt IS NOT NULL  
                        AND cca.Recalc_Type_End_Dt IS NOT NULL  
                        AND cca.Recalc_Type_Start_Dt > cca.Recalc_Type_End_Dt)  
                    AND cca.Is_Validation_Passed = 1;  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Recalc Type end date shuold be greater than or equal to '  
                                  + CAST(@Df_Invoice_Reclac_Type_Start_Dt AS VARCHAR(10))  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                WHERE  
                    (   cca.Recalc_Type_Start_Dt IS NULL  
                        AND cca.Recalc_Type_End_Dt IS NOT NULL  
                        AND cca.Recalc_Type_End_Dt < @Df_Invoice_Reclac_Type_Start_Dt)  
                    AND cca.Is_Validation_Passed = 1;  
  
                -- Consolidated billing can't have different information on each meter added to the accoun.    
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'consolidated billing configuration values should be same for all the meters'  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                    INNER JOIN #Data_Conversion_Accounts_Stage dcav  
                        ON dcav.[ACCOUNT NUMBER] = cca.[ACCOUNT NUMBER]  
                           AND  dcav.METER = cca.METER  
                    INNER JOIN #Data_Conversion_Accounts_Stage dcav1  
                        ON dcav1.[ACCOUNT NUMBER] = dcav.[ACCOUNT NUMBER]  
                           AND  dcav1.METER = dcav.METER  
                           AND  (   ISNULL(dcav1.Consolidated_Billing_Invoice_Type, '') <> ISNULL(  
                                                                                               dcav.Consolidated_Billing_Invoice_Type  
                                                                                               , '')  
                                    OR  ISNULL(dcav1.Consolidated_Billing_Start_Dt, '1/1/1900') <> ISNULL(  
                                                                                                       dcav.Consolidated_Billing_Start_Dt                                                                                                         , '1/1/1900')
  
                                    OR  ISNULL(dcav1.Consolidated_Billing_End_Dt, '1/1/1900') <> ISNULL(  
                                                                                                     dcav.Consolidated_Billing_End_Dt  
                                                                                                     , '1/1/1900')  
                                    OR  ISNULL(dcav1.Supplier_Vendor_Id, 0) <> ISNULL(dcav.Supplier_Vendor_Id, 0))  
                WHERE  
                    cca.Is_Validation_Passed = 1;  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Consolidated billing start date is mandatory when Consolidated Billing Invoice Type is provided'  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                WHERE  
                    cca.Is_Validation_Passed = 1  
                    AND cca.Consolidated_Billing_Start_Dt IS NULL  
                    AND cca.Consolidated_Billing_Invoice_Type IS NOT NULL;  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Consolidated billing start date is mandatory when the end date is given'  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                WHERE  
                    cca.Is_Validation_Passed = 1  
                    AND cca.Consolidated_Billing_Start_Dt IS NULL  
                    AND cca.Consolidated_Billing_End_Dt IS NOT NULL;  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Consolidated billing start date should be greater than the end date'  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                WHERE  
                    cca.Is_Validation_Passed = 1  
                    AND (   ISNULL(cca.Consolidated_Billing_Start_Dt, '1/1/1900') > ISNULL(  
                                                                                        cca.Consolidated_Billing_End_Dt  
                                                                                        , '2099/12/31')  
                            OR  ISNULL(cca.Consolidated_Billing_End_Dt, '2099/12/31') < ISNULL(  
                                                                                            cca.Consolidated_Billing_Start_Dt  
                                                                                            , '1/1/1900'));  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Consolidated billing supplier vendor is not valid'  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                WHERE  
                    cca.Is_Validation_Passed = 1  
                    AND (   cca.Supplier_Vendor_Id IS NOT NULL  
                            AND NOT EXISTS (   SELECT  
                                                    1  
                                               FROM  
                                                    dbo.VENDOR v  
                                                    INNER JOIN dbo.ENTITY vt  
                                                        ON vt.ENTITY_ID = v.VENDOR_TYPE_ID  
                                               WHERE  
                                                    v.VENDOR_ID = cca.Supplier_Vendor_Id  
                                                    AND vt.ENTITY_NAME = 'Supplier'));  
  
                INSERT INTO #Site_Commodity  
                     (  
                         Site_Client_Hier_Id  
                         , Site_Id  
                         , Commodity_Id                           , Division_Client_Hier_Id  
                         , Client_Client_Hier_Id  
                     )  
                SELECT  
                    ch.Client_Hier_Id  
                    , ch.Site_Id  
                    , cca.Commodity  
                    , dvch.Client_Hier_Id  
                    , clch.Client_Hier_Id  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                    INNER JOIN Core.Client_Hier ch  
                        ON ch.Site_Id = cca.[Site Id]  
                    INNER JOIN Core.Client_Hier dvch  
                        ON dvch.Sitegroup_Id = ch.Sitegroup_Id  
                           AND  dvch.Site_Id = 0  
                    INNER JOIN Core.Client_Hier clch  
                        ON clch.Client_Id = dvch.Client_Id  
                           AND  clch.Sitegroup_Id = 0  
                           AND  clch.Site_Id = 0  
                WHERE  
                    cca.Is_Validation_Passed = 1  
                    AND cca.Consolidated_Billing_Start_Dt IS NOT NULL  
                    AND cca.Consolidated_Billing_Invoice_Type = 'Supplier'  
                    AND cca.Supplier_Vendor_Id IS NULL  
                GROUP BY  
                    ch.Client_Hier_Id  
                    , ch.Site_Id  
                    , cca.Commodity  
                    , dvch.Client_Hier_Id  
                    , clch.Client_Hier_Id;  
  
                IF EXISTS (SELECT   TOP 1   Site_Id FROM    #Site_Commodity)  
                    BEGIN  
                        -- Get the configurations defined at the site level    
                        INSERT INTO #Site_Commodity_DMO_Config  
                             (  
                                 Site_Client_Hier_Id  
                                 , Site_Id  
                                 , Commodity_Id  
                                 , DMO_Start_Dt  
                                 , DMO_End_Dt  
                             )  
                        SELECT  
                            sc.Site_Client_Hier_Id  
                            , sc.Site_Id  
                            , sc.Commodity_Id  
                            , dhc.DMO_Start_Dt  
                            , ISNULL(dhc.DMO_End_Dt, '2099-12-31')  
                        FROM  
                            #Site_Commodity sc  
                            INNER JOIN dbo.Client_Hier_DMO_Config dhc  
                                ON dhc.Commodity_Id = sc.Commodity_Id  
                                   AND  dhc.Client_Hier_Id = sc.Site_Client_Hier_Id;  
  
                        -- Get the configurations defined at the Division level and not deleted at the site level    
                        INSERT INTO #Site_Commodity_DMO_Config  
                             (  
                                 Site_Client_Hier_Id  
                                 , Site_Id  
                                 , Commodity_Id  
                                 , DMO_Start_Dt  
                                 , DMO_End_Dt  
                             )  
                        SELECT  
                            sc.Site_Client_Hier_Id  
                            , sc.Site_Id  
                            , sc.Commodity_Id  
                            , dhc.DMO_Start_Dt  
                            , ISNULL(dhc.DMO_End_Dt, '2099-12-31')  
                        FROM  
                            #Site_Commodity sc  
                            INNER JOIN dbo.Client_Hier_DMO_Config dhc  
                                ON dhc.Commodity_Id = sc.Commodity_Id  
                                   AND  dhc.Client_Hier_Id = sc.Division_Client_Hier_Id  
                        WHERE  
                            NOT EXISTS (   SELECT  
                                                1  
                                           FROM  
                                                dbo.Client_Hier_Not_Applicable_DMO_Config chnac  
                                           WHERE  
                                    chnac.Client_Hier_Id = sc.Site_Client_Hier_Id  
                                                AND chnac.Client_Hier_DMO_Config_Id = dhc.Client_Hier_DMO_Config_Id)  
                            AND NOT EXISTS (   SELECT  
                                                    1  
                                               FROM  
                                                    #Site_Commodity_DMO_Config sdc  
                                               WHERE  
                                                    sdc.Site_Client_Hier_Id = sc.Site_Client_Hier_Id  
                                                    AND sdc.Commodity_Id = sc.Commodity_Id  
                                                    AND sdc.DMO_Start_Dt = dhc.DMO_Start_Dt  
                                                    AND sdc.DMO_End_Dt = dhc.DMO_End_Dt);  
  
                        -- Get the configurations defined at the Client level and not deleted at the Division level    
                        INSERT INTO #Site_Commodity_DMO_Config  
                             (  
                                 Site_Client_Hier_Id  
                                 , Site_Id  
                                 , Commodity_Id  
                                 , DMO_Start_Dt  
                                 , DMO_End_Dt  
                             )  
                        SELECT  
                            sc.Site_Client_Hier_Id  
                            , sc.Site_Id  
                            , sc.Commodity_Id  
                            , dhc.DMO_Start_Dt  
                            , ISNULL(dhc.DMO_End_Dt, '2099-12-31')  
                        FROM  
                            #Site_Commodity sc  
                            INNER JOIN dbo.Client_Hier_DMO_Config dhc  
                                ON dhc.Commodity_Id = sc.Commodity_Id  
                                   AND  dhc.Client_Hier_Id = sc.Client_Client_Hier_Id  
                        WHERE  
                            NOT EXISTS (   SELECT  
                                                1  
                                           FROM  
                                                dbo.Client_Hier_Not_Applicable_DMO_Config chnac  
                                           WHERE  
                                                chnac.Client_Hier_Id = sc.Division_Client_Hier_Id  
                                                AND chnac.Client_Hier_DMO_Config_Id = dhc.Client_Hier_DMO_Config_Id)  
                            AND NOT EXISTS (   SELECT  
                                                    1  
                                               FROM  
                                                    #Site_Commodity_DMO_Config sdc  
                                               WHERE  
                                                    sdc.Site_Client_Hier_Id = sc.Site_Client_Hier_Id  
                                                    AND sdc.Commodity_Id = sc.Commodity_Id  
                                                    AND sdc.DMO_Start_Dt = dhc.DMO_Start_Dt  
                                                    AND sdc.DMO_End_Dt = dhc.DMO_End_Dt);  
  
                        UPDATE  
                            cca  
                        SET  
                            Is_Validation_Passed = 0  
                            , Error_Msg = 'Supplier vendor is mandatory if DMO configuration exist'  
                        FROM  
                            #Data_Conversion_Accounts_Stage cca  
                            INNER JOIN #Site_Commodity_DMO_Config dmocnf  
                                ON dmocnf.Site_Id = cca.[Site Id]  
                                   AND  dmocnf.Commodity_Id = cca.Commodity  
                                   AND  (   (cca.Consolidated_Billing_Start_Dt BETWEEN dmocnf.DMO_Start_Dt  
                                                                               AND     dmocnf.DMO_End_Dt)  
                OR  (cca.Consolidated_Billing_End_Dt BETWEEN dmocnf.DMO_Start_Dt  
                                                                                 AND     dmocnf.DMO_End_Dt))  
                        WHERE  
                            cca.Supplier_Vendor_Id IS NULL  
                            AND cca.Is_Validation_Passed = 1;  
  
                    END;  
  
                -- Invoice collection set up validation    
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Set up invoice collection should be Yes/No if invoice collection configuration exist for the client'  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                    INNER JOIN dbo.SITE s  
                        ON s.SITE_ID = cca.[Site id]  
                    INNER JOIN dbo.Invoice_Collection_Client_Config cc  
                        ON cc.Client_Id = s.Client_ID  
                WHERE  
                    NULLIF(RTRIM(LTRIM(cca.Setup_Invoice_Collection)), '') IS NULL  
                    OR  NULLIF(RTRIM(LTRIM(cca.Setup_Invoice_Collection)), '') NOT IN ( 'Yes', 'No' );  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Invoice collection configuration does not exist at the client level, set up invoice collection should be blank at the account level '  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                    INNER JOIN dbo.SITE s  
                        ON s.SITE_ID = cca.[Site id]  
                WHERE  
                    NULLIF(RTRIM(LTRIM(cca.Setup_Invoice_Collection)), '') IS NOT NULL  
                    AND NOT EXISTS (   SELECT  
                                            1  
                                       FROM  
                                            dbo.Invoice_Collection_Client_Config cc  
                                       WHERE  
                                            cc.Client_Id = s.Client_Id);  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Invoice collection configuration should be same for all the meters of an utility account'  
                FROM  
                    #Data_Conversion_Accounts_Stage cca  
                WHERE  
                    EXISTS (   SELECT  
                                    1  
                               FROM  
                                    #Data_Conversion_Accounts_Stage a  
                                    INNER JOIN #Data_Conversion_Accounts_Stage b  
                                        ON b.[ACCOUNT NUMBER] = a.[ACCOUNT NUMBER]  
                                           AND  b.[Site id] = a.[Site id]  
                               WHERE  
                                    ISNULL(RTRIM(LTRIM(a.Setup_Invoice_Collection)), '') <> ISNULL(  
                                                                                                RTRIM(  
                                                                                                    LTRIM(  
                                                                                                        b.Setup_Invoice_Collection))  
                                                                                                , '')  
                                    AND a.[ACCOUNT NUMBER] = cca.[ACCOUNT NUMBER]  
                                    AND a.[Site id] = cca.[Site id]);  
  
                INSERT INTO ETL.Data_Conversion_Batch_Dtl  
                     (  
                         Data_Conversion_Batch_Id  
                         , [Site id]  
                         , [Vendor ID]  
                         , Commodity  
                         , [ACCOUNT NUMBER]  
                         , METER  
                         , [Rate ID]  
                         , [Service LEVEL]  
                         , [Consumption Level ID]  
                         , [UBM Account ID]  
                         , DEO  
                         , [Site Address]  
                         , [Is Default To Primary]  
                         , [Purchase Method]  
                         , UBM  
                         , Consolidated_Billing_Invoice_Type  
                         , Supplier_Vendor_Id  
                         , Consolidated_Billing_Start_Dt  
                         , Consolidated_Billing_End_Dt  
                         , Is_Data_Validation_Passed  
                         , Error_Msg  
                         , Meter_Type  
                         , Alternate_Account_Number  
                         , Recalc_Type  
                         , Recalc_Type_Start_Dt  
                         , Recalc_Type_End_Dt  
                         , Setup_Invoice_Collection  
                     )  
                SELECT  
                    @Data_Conversion_Batch_Id  
                    , cca.[Site id]  
                    , cca.[Vendor ID]  
                    , cca.Commodity  
                    , cca.[ACCOUNT NUMBER]  
                    , cca.METER  
                    , cca.[Rate ID]  
                    , cca.[Service LEVEL]  
                    , cca.[Consumption Level ID]  
                    , cca.[UBM Account ID]  
                    , cca.DEO  
                    , cca.[Site Address]  
                    , cca.[Is Default To Primary]  
                    , cca.[Purchase Method]  
                    , cca.UBM  
                    , cca.Consolidated_Billing_Invoice_Type  
                    , cca.Supplier_Vendor_Id  
                    , cca.Consolidated_Billing_Start_Dt  
                    , cca.Consolidated_Billing_End_Dt  
                    , cca.Is_Validation_Passed  
                    , cca.Error_Msg  
                    , cca.Meter_Type  
                    , cca.Alternate_Account_Number  
                    , cca.Recalc_Type  
                    , cca.Recalc_Type_Start_Dt  
                    , cca.Recalc_Type_End_Dt  
                    , cca.Setup_Invoice_Collection  
                FROM  
                    #Data_Conversion_Accounts_Stage cca;  
  
            END;  
  
        IF @Conversion_Type = 'Sitegroup_Site'  
            BEGIN  
  
                DECLARE @Client_Name VARCHAR(200);  
                DECLARE @Sitegroup_User TABLE  
                      (  
                          Sitegroup_Name VARCHAR(200)  
                          , Add_User VARCHAR(50)  
                          , Sitegroup_Type VARCHAR(50)  
                          , Is_Sg_Name_Add_User_Valid BIT DEFAULT (1)  
                      );  
  
                SELECT  TOP 1  
                        @Client_Name = Client_Name  
                FROM  
                    ETL.Data_Conversion_Sitegroup_Site_Stage  
                WHERE  
                    Client_Name IS NOT NULL;  
  
                INSERT INTO @Sitegroup_User  
                     (  
                         Sitegroup_Name  
                         , Add_User  
                         , Sitegroup_Type  
                     )  
                SELECT  
                    Sitegroup_Name  
                    , Add_User  
                    , Sitegroup_Type  
                FROM  
                    ETL.Data_Conversion_Sitegroup_Site_Stage  
                WHERE  
                    Add_User IS NOT NULL  
                    AND Sitegroup_Name IS NOT NULL  
                    AND Sitegroup_Type IS NOT NULL  
                GROUP BY  
                    Sitegroup_Name  
                    , Add_User  
                    , Sitegroup_Type;  
  
                /* Do Not Convert the Sitegroup if the Add_User associated to it do not exist in the system  */  
  
                UPDATE  
                    su  
                SET  
                    su.Is_Sg_Name_Add_User_Valid = 0  
                FROM  
                    @Sitegroup_User su  
                WHERE  
                    NOT EXISTS (SELECT  1 FROM  dbo.USER_INFO ui WHERE  ui.USERNAME = su.Add_User);  
  
  
                /*               
Rules to check if Sitegroup_Name is Valid.              
1. If the Sitegroup to be created is a Global group and the Sitegroup name already exist as a division , then discard it.             
2. If the Sitegroup to be created is a Global group and the Sitegroup name already exist as a Global Sitegroup and if sitegroup is             
   not a smartgroup , then Need to put the new sites to it.              
3. If the Sitegroup to be created is a User group and the User Sitegroup name already exist for the same user             
   , then Need to put the new sites to it.              
*/  
  
                UPDATE  
                    su  
                SET  
                    su.Is_Sg_Name_Add_User_Valid = 0  
                FROM  
                    @Sitegroup_User su  
                WHERE  
                    su.Sitegroup_Type = 'Division';  
  
                UPDATE  
                    su  
                SET  
                    su.Is_Sg_Name_Add_User_Valid = 0  
                FROM  
                    @Sitegroup_User su  
                WHERE  
                    su.Sitegroup_Type = 'Global'  
                    AND EXISTS (   SELECT  
                                        1  
                                   FROM  
                                        dbo.Sitegroup sg  
                                        INNER JOIN dbo.CLIENT c  
                                            ON sg.Client_Id = c.CLIENT_ID  
                                        INNER JOIN dbo.Code ce  
                                            ON ce.Code_Id = sg.Sitegroup_Type_Cd  
                                   WHERE  
                                        sg.Sitegroup_Name = su.Sitegroup_Name  
                                        AND ce.Code_Value = 'division'  
                                        AND c.CLIENT_NAME = @Client_Name);  
  
                UPDATE  
                    su  
                SET  
                    su.Is_Sg_Name_Add_User_Valid = 0  
                FROM  
                    @Sitegroup_User su  
                    INNER JOIN dbo.Sitegroup sg  
                        ON sg.Sitegroup_Name = su.Sitegroup_Name  
                    INNER JOIN dbo.CLIENT c  
                        ON sg.Client_Id = c.CLIENT_ID  
                    INNER JOIN dbo.Code ce  
                        ON ce.Code_Id = sg.Sitegroup_Type_Cd  
                    INNER JOIN dbo.USER_INFO ui  
                        ON ui.USER_INFO_ID = sg.Add_User_Id  
                WHERE  
                    (   (   su.Sitegroup_Type = 'Global'  
                            AND ce.Code_Value = 'Global'  
                            AND ui.USERNAME != su.Add_User)  
                        OR  (   su.Sitegroup_Type = 'User'  
                                AND ce.Code_Value = 'User'  
                                AND ui.USERNAME != su.Add_User)  
                        OR  (   su.Sitegroup_Type = 'Global'  
                                AND ui.ACCESS_LEVEL = 1)  
                        OR  (   ui.CLIENT_ID != sg.Client_Id  
                                AND ui.ACCESS_LEVEL = 1)  
                        OR  sg.Is_Smart_Group = 1)  
                    AND c.CLIENT_NAME = @Client_Name;  
  
  
  
  
                INSERT INTO ETL.Data_Conversion_Batch_Dtl  
                     (  
                         Data_Conversion_Batch_Id  
                         , Client_Name  
                         , Division_Name  
                         , Sitegroup_Name  
                         , Sitegroup_Type  
                         , Site_Name  
                         , Add_User  
                         , Is_Data_Validation_Passed  
                         , Error_Msg  
                     )  
                SELECT  
                    @Data_Conversion_Batch_Id  
                    , @Client_Name  
 , dcas.Division_Name  
                    , su.Sitegroup_Name  
                    , su.Sitegroup_Type  
                    , dcas.Site_Name  
                    , su.Add_User  
                    , 0  
                    , 'Add_User Given is not Valid or Add_User not valid for given client OR the Sitegroup_name Provided already exists as a Division or Global sitegroup or User specific group or Global group being created by internal user' ERROR_Msg  
                FROM  
                    ETL.Data_Conversion_Sitegroup_Site_Stage dcas  
                    INNER JOIN @Sitegroup_User su  
                        ON dcas.Sitegroup_Name = su.Sitegroup_Name  
                           AND  su.Sitegroup_Type = dcas.Sitegroup_Type  
                WHERE  
                    su.Is_Sg_Name_Add_User_Valid = 0;  
  
  
                INSERT INTO ETL.Data_Conversion_Batch_Dtl  
                     (  
                         Data_Conversion_Batch_Id  
                         , Client_Name  
                         , Division_Name  
                         , Sitegroup_Name  
                         , Sitegroup_Type  
                         , Site_Name  
                         , Add_User  
                         , Is_Data_Validation_Passed  
                         , Error_Msg  
                     )  
                SELECT  
                    @Data_Conversion_Batch_Id  
                    , @Client_Name  
                    , dcas.Division_Name  
                    , su.Sitegroup_Name  
                    , su.Sitegroup_Type  
                    , dcas.Site_Name  
                    , dcas.Add_User  
                    , 0  
                    , 'Multipple Add_User''s are provided for the same combination of Sitegroup_Name and Sitegroup_Type' ERROR_Msg  
                FROM  
                    ETL.Data_Conversion_Sitegroup_Site_Stage dcas  
                    INNER JOIN (   SELECT  
                                        COUNT(Add_User) cnt  
                                        , Sitegroup_Name  
                                        , Sitegroup_Type  
                                   FROM  
                                        @Sitegroup_User  
                                   GROUP BY  
                                       Sitegroup_Name  
                                       , Sitegroup_Type  
                                   HAVING  
                                        COUNT(Add_User) > 1) su  
                        ON dcas.Sitegroup_Name = su.Sitegroup_Name  
                           AND  su.Sitegroup_Type = dcas.Sitegroup_Type;  
  
  
  
                DELETE  
                dcas  
                FROM  
                    ETL.Data_Conversion_Sitegroup_Site_Stage dcas  
                    INNER JOIN (   SELECT  
                                        COUNT(Add_User) cnt  
                                        , Sitegroup_Name  
                                        , Sitegroup_Type  
                                   FROM  
                                        @Sitegroup_User  
                                   GROUP BY  
                                       Sitegroup_Name  
                                       , Sitegroup_Type  
                                   HAVING  
                                        COUNT(Add_User) > 1) su  
                        ON dcas.Sitegroup_Name = su.Sitegroup_Name  
                           AND  su.Sitegroup_Type = dcas.Sitegroup_Type;  
  
  
                UPDATE  
                    su  
                SET  
                    su.Is_Sg_Name_Add_User_Valid = 0  
                FROM  
                    @Sitegroup_User su  
                    INNER JOIN (   SELECT  
                                        COUNT(Add_User) cnt  
                                        , Sitegroup_Name  
                                        , Sitegroup_Type  
                                   FROM  
                                        @Sitegroup_User  
                                   GROUP BY  
                                       Sitegroup_Name  
                                       , Sitegroup_Type  
                                   HAVING  
                                        COUNT(Add_User) > 1) k  
                        ON k.Sitegroup_Name = su.Sitegroup_Name  
                           AND  k.Sitegroup_Type = su.Sitegroup_Type;  
  
  
  
                /* delete the rows that are imported as nulls                                  
   this happens when  user uses the same excel template as last used, and the used excel rows are less than the last used */  
                DELETE  FROM  
                ETL.Data_Conversion_Sitegroup_Site_Stage  
                WHERE  
                    Division_Name IS NULL  
                    AND Site_Name IS NULL  
                    AND Sitegroup_Name IS NULL  
                    AND Sitegroup_Type IS NULL  
                    AND Client_Name IS NULL;  
  
                /*Any Column is Imported as NULL*/  
                INSERT INTO ETL.Data_Conversion_Batch_Dtl  
                     (  
                         Data_Conversion_Batch_Id  
                         , Client_Name  
                         , Division_Name  
                         , Sitegroup_Name  
                         , Sitegroup_Type  
                         , Site_Name  
                         , Add_User  
                         , Is_Data_Validation_Passed  
                         , Error_Msg  
                     )  
                SELECT  
                    @Data_Conversion_Batch_Id  
                    , @Client_Name  
                    , dcas.Division_Name  
                    , su.Sitegroup_Name  
                    , su.Sitegroup_Type  
                    , dcas.Site_Name  
                    , su.Add_User  
                    , 0  
                    , 'Division_Name OR Site_Name ID OR Sitegroup_Name OR Sitegroup_Type is Imported as NULL' ERROR_Msg  
                FROM  
                    ETL.Data_Conversion_Sitegroup_Site_Stage dcas  
                    INNER JOIN @Sitegroup_User su  
                        ON dcas.Sitegroup_Name = su.Sitegroup_Name  
                WHERE  
                    (   dcas.Division_Name IS NULL  
                        OR  dcas.Site_Name IS NULL  
                        OR  dcas.Sitegroup_Name IS NULL  
                        OR  dcas.Sitegroup_Type IS NULL)  
                    AND su.Is_Sg_Name_Add_User_Valid = 1;  
  
  
                DELETE  
                dcas  
                FROM  
                    ETL.Data_Conversion_Sitegroup_Site_Stage dcas  
                    INNER JOIN @Sitegroup_User su  
                        ON dcas.Sitegroup_Name = su.Sitegroup_Name  
                WHERE  
                    (   dcas.Division_Name IS NULL  
                        OR  dcas.Site_Name IS NULL  
                        OR  dcas.Sitegroup_Name IS NULL  
                        OR  dcas.Sitegroup_Type IS NULL)  
                    AND su.Is_Sg_Name_Add_User_Valid = 1;  
  
                /* delete the rows if Division Provided not exists for the client */  
  
                INSERT INTO ETL.Data_Conversion_Batch_Dtl  
                     (  
                         Data_Conversion_Batch_Id  
                         , Client_Name  
                         , Division_Name  
                         , Sitegroup_Name  
                         , Sitegroup_Type  
                         , Site_Name  
                         , Add_User  
                         , Is_Data_Validation_Passed  
                         , Error_Msg  
                     )  
                SELECT  
                    @Data_Conversion_Batch_Id  
                    , @Client_Name  
                    , dcss.Division_Name  
                    , su.Sitegroup_Name  
                    , su.Sitegroup_Type  
                    , dcss.Site_Name  
                    , su.Add_User  
                    , 0  
                    , 'Division_Name provided does not exist for the client' ERROR_Msg  
        FROM  
                    ETL.Data_Conversion_Sitegroup_Site_Stage dcss  
                    INNER JOIN @Sitegroup_User su  
                        ON dcss.Sitegroup_Name = su.Sitegroup_Name  
                WHERE  
                    NOT EXISTS (   SELECT  
                                        1  
                                   FROM  
                                        dbo.CLIENT c  
                                        INNER JOIN dbo.Sitegroup s  
                                            ON c.CLIENT_ID = s.Client_Id  
                                        INNER JOIN dbo.Code co  
                                            ON s.Sitegroup_Type_Cd = co.Code_Id  
                                   WHERE  
                                        c.CLIENT_NAME = @Client_Name  
                                        AND dcss.Division_Name = s.Sitegroup_Name  
                                        AND co.Code_Value = 'Division')  
                    AND su.Is_Sg_Name_Add_User_Valid = 1;  
  
  
  
                DELETE  
                dcss  
                FROM  
                    ETL.Data_Conversion_Sitegroup_Site_Stage dcss  
                    INNER JOIN @Sitegroup_User su  
                        ON dcss.Sitegroup_Name = su.Sitegroup_Name  
                WHERE  
                    NOT EXISTS (   SELECT  
                                        1  
                                   FROM  
                                        dbo.CLIENT c  
                                        INNER JOIN dbo.Sitegroup s  
                                            ON c.CLIENT_ID = s.Client_Id  
                                        INNER JOIN dbo.Code co  
                                            ON s.Sitegroup_Type_Cd = co.Code_Id  
                                   WHERE  
                                        c.CLIENT_NAME = @Client_Name  
                                        AND dcss.Division_Name = s.Sitegroup_Name  
                                        AND co.Code_Value = 'Division')  
                    AND su.Is_Sg_Name_Add_User_Valid = 1;  
  
  
  
                /* delete the rows if Site_Name does not exists for the client */  
  
                INSERT INTO ETL.Data_Conversion_Batch_Dtl  
                     (  
                         Data_Conversion_Batch_Id  
                         , Client_Name  
                         , Division_Name  
                         , Sitegroup_Name  
                         , Sitegroup_Type  
                         , Site_Name  
                         , Add_User  
                         , Is_Data_Validation_Passed  
                         , Error_Msg  
                     )  
                SELECT  
                    @Data_Conversion_Batch_Id  
                    , @Client_Name  
                    , dcss.Division_Name  
                    , su.Sitegroup_Name  
                    , su.Sitegroup_Type  
                    , dcss.Site_Name  
                    , su.Add_User  
                    , 0  
                    , 'Site_Name provided does not exist for the client' ERROR_Msg  
                FROM  
                    ETL.Data_Conversion_Sitegroup_Site_Stage dcss  
                    INNER JOIN @Sitegroup_User su  
                        ON dcss.Sitegroup_Name = su.Sitegroup_Name  
                WHERE  
                    NOT EXISTS (   SELECT  
                                        1  
                                   FROM  
                                        dbo.CLIENT c  
                                        INNER JOIN dbo.SITE s  
                                            ON c.CLIENT_ID = s.Client_ID  
                                   WHERE  
                                        c.CLIENT_NAME = @Client_Name  
                                        AND dcss.Site_Name = s.SITE_NAME)  
                    AND su.Is_Sg_Name_Add_User_Valid = 1;  
  
                DELETE  
                dcss  
                FROM  
                    ETL.Data_Conversion_Sitegroup_Site_Stage dcss  
                    INNER JOIN @Sitegroup_User su  
                        ON dcss.Sitegroup_Name = su.Sitegroup_Name  
                WHERE  
                    NOT EXISTS (   SELECT  
                                        1  
                                   FROM  
                                        dbo.CLIENT c  
                                        INNER JOIN dbo.SITE s  
                                            ON c.CLIENT_ID = s.Client_ID  
                                   WHERE  
                                        c.CLIENT_NAME = @Client_Name  
                                        AND dcss.SITE_NAME = s.SITE_NAME)  
                    AND su.Is_Sg_Name_Add_User_Valid = 1;  
  
  
                /* delete the rows if Client_Name, Division_Name, Site_Name combination provided is not valid */  
  
                INSERT INTO ETL.Data_Conversion_Batch_Dtl  
                     (  
                         Data_Conversion_Batch_Id  
                         , Client_Name  
                         , Division_Name  
                         , Sitegroup_Name  
                         , Sitegroup_Type  
                         , Site_Name  
                         , Add_User  
                         , Is_Data_Validation_Passed  
                         , Error_Msg  
                     )  
                SELECT  
                    @Data_Conversion_Batch_Id  
                    , @Client_Name  
                    , dcss.Division_Name  
                    , su.Sitegroup_Name  
                    , su.Sitegroup_Type  
                    , dcss.Site_Name  
                    , su.Add_User  
                    , 0  
                    , 'Client_Name, Division_Name, Site_Name Combination provided is not valid' ERROR_Msg  
                FROM  
                    ETL.Data_Conversion_Sitegroup_Site_Stage dcss  
                    INNER JOIN @Sitegroup_User su  
                        ON dcss.Sitegroup_Name = su.Sitegroup_Name  
                WHERE  
                    NOT EXISTS (   SELECT  
                                        1  
                                   FROM  
                                        Core.Client_Hier ch  
                                   WHERE  
                                        ch.Client_Name = @Client_Name  
                                        AND dcss.Client_Name = ch.Client_Name  
                                        AND dcss.Division_Name = ch.Sitegroup_Name  
                                        AND dcss.Site_Name = ch.Site_name)  
                    AND su.Is_Sg_Name_Add_User_Valid = 1;  
  
                DELETE  
                dcss  
                FROM  
                    ETL.Data_Conversion_Sitegroup_Site_Stage dcss  
                    INNER JOIN @Sitegroup_User su  
                        ON dcss.Sitegroup_Name = su.Sitegroup_Name  
                WHERE  
                    NOT EXISTS (   SELECT  
                                        1  
                                   FROM  
                                        Core.Client_Hier ch  
                                   WHERE  
                                        ch.Client_Name = @Client_Name  
                                        AND dcss.Client_Name = ch.Client_Name  
                                        AND dcss.Division_Name = ch.Sitegroup_Name  
                                        AND dcss.Site_name = ch.Site_name)  
                    AND su.Is_Sg_Name_Add_User_Valid = 1;  
  
                /* Importing the rest of the data as Is_Data_Validation_Passed = 1  */  
  
                INSERT INTO ETL.Data_Conversion_Batch_Dtl  
                     (  
                         Data_Conversion_Batch_Id  
                         , Client_Name  
                         , Division_Name  
                         , Sitegroup_Name  
                         , Sitegroup_Type  
                         , Site_Name  
                         , Add_User  
                         , Is_Data_Validation_Passed  
                         , Error_Msg  
                     )  
                SELECT  
                    @Data_Conversion_Batch_Id  
                    , @Client_Name  
                    , dcss.Division_Name  
                    , su.Sitegroup_Name  
                    , su.Sitegroup_Type  
                    , dcss.Site_Name  
                    , su.Add_User  
                    , 1  
                    , NULL ERROR_Msg  
                FROM  
                    ETL.Data_Conversion_Sitegroup_Site_Stage dcss  
                    INNER JOIN @Sitegroup_User su  
                        ON dcss.Sitegroup_Name = su.Sitegroup_Name  
                WHERE  
                    su.Is_Sg_Name_Add_User_Valid = 1;  
  
            END;  
  
        IF @Conversion_Type = 'DMOSupplierAccount'  
            BEGIN  
  
                /* Updating Data_Conversion_Batch_Id for the error rows that are inserted to Data_Conversion_Batch_Dtl during excel inport  */  
  
                UPDATE  
                    ETL.Data_Conversion_Batch_Dtl  
                SET  
                    Data_Conversion_Batch_Id = @Data_Conversion_Batch_Id  
                WHERE  
                    Data_Conversion_Batch_Id = -1  
                    AND Is_Data_Validation_Passed = 0;  
  
  
                /* delete the rows that are imported as nulls                                    
   this happens when  user uses the same excel template as last used, and the used excel rows are less than the last used */  
                DELETE  FROM  
                #DMO_Supplier_Account_Conversion_Stage  
                WHERE  
                    Account_Id IS NULL  
                    AND Commodity IS NULL  
                    AND Meter_Number IS NULL  
                    AND DMO_Supplier_Account_Start_Date IS NULL  
                    AND Vendor_ID IS NULL  
                    AND Service_Level IS NULL  
                    AND Consumption_Level_ID IS NULL;  
  
  
                --Site Id OR Vendor ID OR ACCOUNT NUMBER is Imported as null    
                UPDATE  
                    #DMO_Supplier_Account_Conversion_Stage  
                SET  
                    Error_Msg = 'Account Id OR Commodity OR Meter Is OR DMO Supplier Account Start Date OR Vendor ID OR Service LEVEL OR Consumption Level ID is imported as null'  
                    , Is_Validation_Passed = 0  
                WHERE  
                    (   Account_Id IS NULL  
                        OR  Commodity IS NULL  
                        OR  Meter_Number IS NULL  
                        OR  DMO_Supplier_Account_Start_Date IS NULL  
                        OR  Vendor_ID IS NULL  
                        OR  Service_Level IS NULL  
                        OR  Consumption_Level_ID IS NULL)  
                    AND Is_Validation_Passed = 1;  
  
                UPDATE  
                    cca  
                SET  
                    Error_Msg = 'Account_Id is not a valid utility account'  
                    , Is_Validation_Passed = 0  
                FROM  
                    #DMO_Supplier_Account_Conversion_Stage cca  
                WHERE  
                    NOT EXISTS (   SELECT  
                                        1  
                                   FROM  
                                        dbo.ACCOUNT acc  
                                        INNER JOIN dbo.ENTITY ent  
                                            ON acc.ACCOUNT_TYPE_ID = ent.ENTITY_ID  
                                   WHERE  
                                        acc.ACCOUNT_ID = cca.ACCOUNT_ID  
                                        AND ent.ENTITY_NAME = 'Utility'  
                                        AND ent.ENTITY_TYPE = 111)  
                    AND Is_Validation_Passed = 1;  
  
                UPDATE  
                    cca  
                SET  
                    Error_Msg = 'Meter # is not valid OR Meter does not belongs to given utility account'  
                    , Is_Validation_Passed = 0  
                FROM  
                 #DMO_Supplier_Account_Conversion_Stage cca  
                WHERE  
                    NOT EXISTS (   SELECT  
                                        1  
                                   FROM  
                                        dbo.METER acc  
                                   WHERE  
                                        acc.METER_NUMBER = cca.METER_NUMBER  
                                        AND acc.ACCOUNT_ID = cca.ACCOUNT_ID)  
                    AND Is_Validation_Passed = 1;  
  
                UPDATE  
                    cca  
                SET  
                    Error_Msg = 'Meter commodity not matching'  
                    , Is_Validation_Passed = 0  
                FROM  
                    #DMO_Supplier_Account_Conversion_Stage cca  
                WHERE  
                    NOT EXISTS (   SELECT  
                                        1  
                                   FROM  
                                        dbo.METER acc  
                                        INNER JOIN dbo.RATE r  
                                            ON acc.RATE_ID = r.RATE_ID  
                                   WHERE  
                                        acc.METER_NUMBER = cca.METER_NUMBER  
                                        AND r.COMMODITY_TYPE_ID = cca.Commodity)  
                    AND Is_Validation_Passed = 1;  
  
                UPDATE  
                    cca  
                SET  
                    Error_Msg = 'Account vendor is not valid or the vendor is not mapped to this commodity'  
                    , Is_Validation_Passed = 0  
                FROM  
                    #DMO_Supplier_Account_Conversion_Stage cca  
                WHERE  
                    NOT EXISTS (   SELECT  
                                        1  
                                   FROM  
                                        CBMS.dbo.VENDOR_COMMODITY_MAP vcm  
                                   WHERE  
                                        vcm.COMMODITY_TYPE_ID = cca.Commodity  
                                        AND vcm.VENDOR_ID = cca.VENDOR_ID)  
                    AND Is_Validation_Passed = 1;  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Commodity is not valid'  
                FROM  
                    #DMO_Supplier_Account_Conversion_Stage cca  
                WHERE  
                    NOT EXISTS (SELECT  1 FROM  CBMS.dbo.Commodity c WHERE  c.Commodity_Id = cca.Commodity)  
                    AND Is_Validation_Passed = 1;  
  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Service level should be one of A/B/C/D'  
                FROM  
                    #DMO_Supplier_Account_Conversion_Stage cca  
                WHERE  
                    Service_Level NOT IN ( SELECT   ENTITY_NAME FROM    CBMS.dbo.ENTITY WHERE  ENTITY_TYPE = 708 )  
                    AND cca.Is_Validation_Passed = 1;  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'DEO is not valid should be Yes/No'  
                FROM  
                    #DMO_Supplier_Account_Conversion_Stage cca  
                WHERE  
                    ISNULL(DEO, 'Invalid') NOT IN ( 'Yes', 'No' )  
                    AND cca.Is_Validation_Passed = 1;  
  
  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Consumption level is not valid for commodity'  
                FROM  
                    CBMS.dbo.Variance_Consumption_Level vcl  
                    INNER JOIN #DMO_Supplier_Account_Conversion_Stage cca  
                        ON cca.Consumption_Level_ID = vcl.Variance_Consumption_Level_Id  
                WHERE  
 vcl.Commodity_Id != cca.Commodity  
                    AND cca.Is_Validation_Passed = 1;  
  
                -- Invoice collection set up validation    
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Set up invoice collection should be Yes/No if invoice collection configuration exist for the client'  
                FROM  
                    #DMO_Supplier_Account_Conversion_Stage cca  
                    INNER JOIN dbo.ACCOUNT acc  
                        ON acc.ACCOUNT_ID = cca.Account_Id  
                    INNER JOIN dbo.SITE s  
                        ON s.SITE_ID = acc.SITE_ID  
                    INNER JOIN dbo.Invoice_Collection_Client_Config cc  
                        ON cc.Client_Id = s.Client_ID  
                WHERE  
                    NULLIF(RTRIM(LTRIM(cca.Setup_Invoice_Collection)), '') IS NULL  
                    OR  NULLIF(RTRIM(LTRIM(cca.Setup_Invoice_Collection)), '') NOT IN ( 'Yes', 'No' );  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Invoice collection configuration does not exist at the client level, set up invoice collection should be blank at the account level '  
                FROM  
                    #DMO_Supplier_Account_Conversion_Stage cca  
                    INNER JOIN dbo.ACCOUNT acc  
                        ON acc.ACCOUNT_ID = cca.Account_Id  
                    INNER JOIN dbo.SITE s  
                        ON s.SITE_ID = acc.SITE_ID  
                WHERE  
                    NULLIF(RTRIM(LTRIM(cca.Setup_Invoice_Collection)), '') IS NOT NULL  
                    AND NOT EXISTS (   SELECT  
                                            1  
                                       FROM  
                                            dbo.Invoice_Collection_Client_Config cc  
                                       WHERE  
                                            cc.Client_Id = s.Client_Id);  
  
                UPDATE  
                    cca  
                SET  
                    Is_Validation_Passed = 0  
                    , Error_Msg = 'Invoice collection configuration should be same for all the meters of the account'  
                FROM  
                    #DMO_Supplier_Account_Conversion_Stage cca  
                WHERE  
                    EXISTS (   SELECT  
                                    1  
                               FROM  
                                    #DMO_Supplier_Account_Conversion_Stage a  
                                    INNER JOIN #DMO_Supplier_Account_Conversion_Stage b  
                                        ON b.Account_Id = a.Account_Id  
                                           AND  b.Commodity = a.Commodity  
                                           AND  b.DMO_Supplier_Account_Start_Date = a.DMO_Supplier_Account_Start_Date  
                                           AND  b.Supplier_ACCOUNT_NUMBER = a.Supplier_ACCOUNT_NUMBER  
                               WHERE  
                                    ISNULL(RTRIM(LTRIM(a.Setup_Invoice_Collection)), '') <> ISNULL(  
                                                                                                RTRIM(  
                                                                                                    LTRIM(  
                                                                                                        b.Setup_Invoice_Collection))  
                                                                                                , '')  
                                    AND a.Account_Id = cca.Account_Id  
                                    AND a.Meter_Number = cca.Meter_Number);  
  
                INSERT INTO #Unique_DMO_List  
                     (  
                         Supplier_ACCOUNT_NUMBER  
                         , DMO_Supplier_Account_Start_Date  
    , DMO_Supplier_Account_End_Date  
                         , Vendor_ID  
                     )  
                SELECT  
                    Supplier_ACCOUNT_NUMBER  
                    , DMO_Supplier_Account_Start_Date  
                    , DMO_Supplier_Account_End_Date  
                    , Vendor_ID  
                FROM  
                    #DMO_Supplier_Account_Conversion_Stage  
                GROUP BY  
                    Supplier_ACCOUNT_NUMBER  
                    , DMO_Supplier_Account_Start_Date  
                    , DMO_Supplier_Account_End_Date  
                    , Vendor_ID;  
                WITH Cte_Multi_Site_DMO  
                AS (  
                       SELECT  
                            dmoacc.Supplier_ACCOUNT_NUMBER  
                            , dmoacc.Vendor_ID  
                            , dmoacc.DMO_Supplier_Account_Start_Date  
                            , dmoacc.DMO_Supplier_Account_End_Date  
                            , COUNT(DISTINCT cha.Client_Hier_Id) Site_Cnt  
                       FROM  
                            #Unique_DMO_List dmoacc  
                            INNER JOIN #DMO_Supplier_Account_Conversion_Stage stg  
                                ON stg.Supplier_ACCOUNT_NUMBER = dmoacc.Supplier_ACCOUNT_NUMBER  
                                   AND  stg.Vendor_ID = dmoacc.Vendor_ID  
                                   AND  stg.DMO_Supplier_Account_Start_Date = dmoacc.DMO_Supplier_Account_Start_Date  
                            INNER JOIN Core.Client_Hier_Account cha  
                                ON cha.Account_Id = stg.Account_Id  
                       WHERE  
                            cha.Account_Type = 'Utility'  
                       GROUP BY  
                           dmoacc.Supplier_ACCOUNT_NUMBER  
                           , dmoacc.Vendor_ID  
                           , dmoacc.DMO_Supplier_Account_Start_Date  
                           , dmoacc.DMO_Supplier_Account_End_Date  
                       HAVING  
                            COUNT(DISTINCT cha.Client_Hier_Id) > 1  
                   )  
                UPDATE  
                    stg  
                SET  
                    Is_validation_Passed = 0  
                    , Error_Msg = 'DMO Supplier account can''t be created for multi site meters'  
                FROM  
                    Cte_Multi_Site_DMO dmoacc  
                    INNER JOIN #DMO_Supplier_Account_Conversion_Stage stg  
                        ON stg.Supplier_ACCOUNT_NUMBER = dmoacc.Supplier_ACCOUNT_NUMBER  
                           AND  stg.Vendor_ID = dmoacc.Vendor_ID  
                           AND  stg.DMO_Supplier_Account_Start_Date = dmoacc.DMO_Supplier_Account_Start_Date  
                WHERE  
                    dmoacc.Site_Cnt > 1;  
  
                INSERT INTO ETL.Data_Conversion_Batch_Dtl  
                     (  
                         Data_Conversion_Batch_Id  
                         , Account_Id  
                         , Commodity  
                         , METER  
                         , [ACCOUNT NUMBER]  
                         , Alternate_Account_Number  
                         , DMO_Supplier_Account_Start_Date  
                         , DMO_Supplier_Account_End_Date  
                         , [Vendor ID]  
                         , [UBM Account ID]  
                         , [Service LEVEL]  
                         , [Consumption Level ID]  
                         , DEO  
                         , Setup_Invoice_Collection  
                         , Is_Data_Validation_Passed  
                         , Invoice_Source  
                         , Error_Msg  
                     )  
                SELECT  
                    @Data_Conversion_Batch_Id  
                    , cca.Account_Id  
                    , cca.Commodity  
                    , cca.Meter_Number  
                    , cca.Supplier_ACCOUNT_NUMBER  
                    , cca.Supplier_Alternate_Account_Number  
                    , cca.DMO_Supplier_Account_Start_Date  
                    , cca.DMO_Supplier_Account_End_Date  
                    , cca.Vendor_ID  
                    , cca.UBM_Account_ID  
                    , cca.Service_Level  
                    , cca.Consumption_Level_ID  
                    , cca.DEO  
                    , cca.Setup_Invoice_Collection  
                    , cca.Is_Validation_Passed  
                    , cca.Invoice_Source_Type  
                    , cca.Error_Msg  
                FROM  
                    #DMO_Supplier_Account_Conversion_Stage cca;  
  
            END;  
  
  
        IF @Conversion_Type = 'UpdateMeterNumbers'  
            BEGIN  
                /* Updating Data_Conversion_Batch_Id for the error rows that are inserted to Data_Conversion_Batch_Dtl during excel inport  */  
  
                UPDATE  
                    ETL.Data_Conversion_Batch_Dtl  
                SET  
                    Data_Conversion_Batch_Id = @Data_Conversion_Batch_Id  
                WHERE  
                    Data_Conversion_Batch_Id = -1  
                    AND Is_Data_Validation_Passed = 0;  
  
  
                --client name or meter id or meter number imported as null    
  
                UPDATE  
                    #Update_Meter_Numbers_Stage  
                SET  
                    Error_Msg = 'Client Name or Meter ID or Meter Number is imported as null'  
                    , Is_Validation_Passed = 0  
                WHERE  
                    (   Client_Name IS NULL  
                        OR  Meter_Id IS NULL  
                        OR  METER IS NULL)  
                    AND Is_Validation_Passed = 1;  
  
  
                -- Invalid client name  
  
                UPDATE  
                    s  
                SET  
                    Error_Msg = 'Invalid Client Name'  
                    , Is_Validation_Passed = 0  
                FROM  
                    #Update_Meter_Numbers_Stage s  
                WHERE  
                    NOT EXISTS (SELECT  1 FROM  dbo.CLIENT c WHERE  c.CLIENT_NAME = s.CLIENT_NAME)  
                    AND s.IS_Validation_Passed = 1;  
  
                -- Meter Number Length > 50    
  
                UPDATE  
                    #Update_Meter_Numbers_Stage  
                SET  
                    Error_Msg = 'Meter number exceeds the limit of 50 characters'  
                    , Is_Validation_Passed = 0  
                WHERE  
                    LEN(METER) > 50  
                    AND Is_Validation_Passed = 1;  
  
                -- New meter number already exists for account id    
  
                UPDATE  
                    #Update_Meter_Numbers_Stage  
                SET  
                    Error_Msg = 'New meter number is already associated to the same account'  
                    , IS_Validation_Passed = 0  
                FROM  
                    #Update_Meter_Numbers_Stage m  
                    INNER JOIN dbo.METER m3  
                        ON m3.METER_ID = m.Meter_Id  
                WHERE  
                    EXISTS (   SELECT  
                                    1  
                               FROM  
                                    dbo.METER m2  
                               WHERE  
                                    m2.ACCOUNT_ID = m3.ACCOUNT_ID  
                                    AND m2.METER_NUMBER = m.METER  
                                    AND m2.METER_ID <> m.METER_ID)  
                    AND m.IS_Validation_Passed = 1;  
  
                UPDATE  
                    s  
                SET  
                    Error_Msg = 'Meter ID is not associated to the Client'  
                    , Is_Validation_Passed = 0  
                FROM  
                    #Update_Meter_Numbers_Stage s  
                WHERE  
                    NOT EXISTS (   SELECT  
                                        1  
                                   FROM  
                                        Core.Client_Hier H  
                                        JOIN Core.Client_Hier_Account A  
                                ON H.Client_Hier_Id = A.Client_Hier_Id  
                                               AND  H.Client_Name = s.Client_Name  
                                               AND  A.Meter_Id = s.Meter_Id)  
                    AND s.IS_Validation_Passed = 1;  
  
                --validation to check if meter id is entered more than once (Note: with same client name and different meter number)  
                ;  
                WITH CTE_DuplicateMeter  
                AS (  
                       SELECT  
                            Meter_Id  
                            , COUNT(meter_id) AS DuplicateCount  
                       FROM  
                            #Update_Meter_Numbers_Stage  
                       GROUP BY  
                           Meter_Id  
                       HAVING  
                            COUNT(meter_id) > 1  
                   )  
                UPDATE  
                    s  
                SET  
                    s.Error_Msg = 'Duplicate Meter ID'  
                    , s.Is_Validation_Passed = 0  
                FROM  
                    #Update_Meter_Numbers_Stage s  
                    JOIN CTE_DuplicateMeter dm  
                        ON s.Meter_Id = dm.Meter_Id  
                           AND  s.IS_Validation_Passed = 1;  
  
                INSERT INTO ETL.Data_Conversion_Batch_Dtl  
                     (  
                         Data_Conversion_Batch_Id  
                         , Client_Name  
                         , METER  
                         , Meter_ID  
                         , Is_Data_Validation_Passed  
                         , Error_Msg  
                     )  
                SELECT  
                    @Data_Conversion_Batch_Id  
                    , cca.Client_Name  
                    , cca.Meter  
                    , cca.Meter_ID  
                    , cca.Is_Validation_Passed  
                    , cca.Error_Msg  
                FROM  
                    #Update_Meter_Numbers_Stage cca;  
            END;  
  
        IF @Conversion_Type = 'UpdateSiteNameAndSiteRef'  
            BEGIN  
                /* Updating Data_Conversion_Batch_Id for the error rows that are inserted to Data_Conversion_Batch_Dtl during excel import  */  
  
                UPDATE  
                    ETL.Data_Conversion_Batch_Dtl  
                SET  
                    Data_Conversion_Batch_Id = @Data_Conversion_Batch_Id  
                WHERE  
                    Data_Conversion_Batch_Id = -1  
                    AND Is_Data_Validation_Passed = 0;  
  
                --Site ID, New Site Name,and New site reference numbers are imported as null      
  
                DELETE  FROM  
                #Update_SiteName_SiteRefNumbers_Stage  
                WHERE  
                    Site_ID IS NULL  
                    AND New_Site_Name IS NULL  
                    AND New_Site_Reference_Number IS NULL  
                    AND Is_Validation_Passed = 1;  
  
  
                --Site ID is not null, New Site Name and New site reference number are null  
  
                UPDATE  
                    #Update_SiteName_SiteRefNumbers_Stage  
                SET  
                    Error_Msg = 'Site Name and Site Reference Number are imported as null'  
                    , Is_Validation_Passed = 0  
                WHERE  
                    (   Site_ID IS NOT NULL  
                        AND New_Site_Name IS NULL  
                        AND New_Site_Reference_Number IS NULL)  
                    AND Is_Validation_Passed = 1;  
  
                --Invalid Site ID  
  
                UPDATE  
                    #Update_SiteName_SiteRefNumbers_Stage  
                SET  
                    Error_Msg = 'Invalid Site ID'  
                    , Is_Validation_Passed = 0  
                FROM  
                    #Update_SiteName_SiteRefNumbers_Stage s1  
                WHERE  
                    NOT EXISTS (SELECT  1 FROM  dbo.SITE s2 WHERE   s2.SITE_ID = s1.SITE_ID)  
                    AND s1.IS_Validation_Passed = 1;  
  
                -- validation to check duplicate site id  
  
                ;  
                WITH CTE_DuplicateSiteID  
                AS (  
                       SELECT  
                            Site_ID  
                            , COUNT(Site_ID) AS DuplicateCount  
                       FROM  
                            #Update_SiteName_SiteRefNumbers_Stage  
                       GROUP BY  
                           Site_ID  
                       HAVING  
                            COUNT(Site_ID) > 1  
                   )  
                UPDATE  
                    s  
                SET  
                    s.Error_Msg = 'Duplicate Site ID'  
                    , s.Is_Validation_Passed = 0  
                FROM  
                    #Update_SiteName_SiteRefNumbers_Stage s  
                    JOIN CTE_DuplicateSiteID ds  
                        ON s.Site_ID = ds.Site_ID  
                           AND  s.IS_Validation_Passed = 1;  
  
                -- Site Reference Number Length > 30      
  
                UPDATE  
                    #Update_SiteName_SiteRefNumbers_Stage  
                SET  
                    Error_Msg = 'Length of Site Reference Number exceeds 30 characters'  
                    , Is_Validation_Passed = 0  
                WHERE  
                    LEN(New_Site_Reference_Number) > 30  
                    AND Is_Validation_Passed = 1;  
  
                -- Site Name Length > 200    
  
                UPDATE  
                    #Update_SiteName_SiteRefNumbers_Stage  
                SET  
                    Error_Msg = 'Length of Site Name exceeds 200 characters'  
                    , Is_Validation_Passed = 0  
                WHERE  
                    LEN(New_Site_Name) > 200  
                    AND Is_Validation_Passed = 1;  
  
                -- New Site Name already associated to same division within a client      
  
                UPDATE  
                    #Update_SiteName_SiteRefNumbers_Stage  
                SET  
                    Error_Msg = 'New Site Name is already associated to same division within a client'  
                    , IS_Validation_Passed = 0  
                FROM  
                    #Update_SiteName_SiteRefNumbers_Stage s  
                    INNER JOIN dbo.SITE s1  
                        ON s.Site_ID = s1.SITE_ID  
                WHERE  
                    EXISTS (   SELECT  
                                    1  
                               FROM  
                                    dbo.SITE s2  
                               WHERE  
                                    s2.Client_ID = s1.Client_ID  
                                    AND s2.DIVISION_ID = s1.DIVISION_ID  
                                    AND s2.SITE_NAME = s.New_Site_Name  
                                    AND s2.SITE_ID <> s.SITE_ID)  
                    AND s.IS_Validation_Passed = 1;  
  
  
                -- Duplicate New site names mapped to same division   
  
                UPDATE  
                    #Update_SiteName_SiteRefNumbers_Stage  
                SET  
                    Error_Msg = 'New Site Names mapped to site ids that belong to same division'  
                    , Is_Validation_Passed = 0  
                FROM  
                    #Update_SiteName_SiteRefNumbers_Stage r  
                    INNER JOIN dbo.SITE s  
                        ON r.Site_ID = s.SITE_ID  
                WHERE  
                    EXISTS (   SELECT  
                                    1  
                               FROM  
                                    #Update_SiteName_SiteRefNumbers_Stage r1  
                                    INNER JOIN dbo.SITE s1  
                                        ON r1.SITE_ID = s1.SITE_ID  
                                           AND  s1.DIVISION_ID = s.DIVISION_ID  
                                           AND  s1.SITE_ID <> s.SITE_ID  
                                           AND  r1.New_Site_Name = r.New_Site_Name)  
        AND r.Is_Validation_Passed = 1;  
  
                INSERT INTO ETL.Data_Conversion_Batch_Dtl  
                     (  
                         Data_Conversion_Batch_Id  
                         , [Site id]  
                         , Site_Name  
                         , [Site Reference Number]  
                         , Is_Data_Validation_Passed  
                         , Error_Msg  
                     )  
                SELECT  
                    @Data_Conversion_Batch_Id  
                    , cca.Site_ID  
                    , cca.New_Site_Name  
                    , cca.New_Site_Reference_Number  
                    , cca.Is_Validation_Passed  
                    , cca.Error_Msg  
                FROM  
                    #Update_SiteName_SiteRefNumbers_Stage cca;  
  
            END;  
  
        IF @Conversion_Type = 'MoveSitesToNewDivision'  
            BEGIN  
                /* Updating Data_Conversion_Batch_Id for the error rows that are inserted to Data_Conversion_Batch_Dtl during excel import  */  
  
                UPDATE  
                    ETL.Data_Conversion_Batch_Dtl  
                SET  
                    Data_Conversion_Batch_Id = @Data_Conversion_Batch_Id  
                WHERE  
                    Data_Conversion_Batch_Id = -1  
                    AND Is_Data_Validation_Passed = 0;  
  
                -- Deleting record when Division name, site name, site id, New Division are null  
  
                DELETE  FROM  
                #Update_Sites_To_New_Division_Stage  
                WHERE  
                    Division_Name IS NULL  
                    AND Site_Name IS NULL  
                    AND Site_Id IS NULL  
                    AND New_Division IS NULL;  
  
                -- check to make sure that no records are unpopulated  
                UPDATE  
                    #Update_Sites_To_New_Division_Stage  
                SET  
                    Error_Msg = CASE WHEN Site_ID IS NULL THEN 'Site ID is null/blank'  
                                    WHEN Site_Name IS NULL THEN 'Site Name is null/blank'  
                                    WHEN Division_Name IS NULL THEN 'Division Name is null/blank'  
                                    WHEN New_Division IS NULL THEN 'New Division is null/blank'  
                                END  
                    , Is_Validation_Passed = 0  
                FROM  
                    #Update_Sites_To_New_Division_Stage  
                WHERE  
                    (   Site_ID IS NULL  
                        OR  Site_Name IS NULL  
                        OR  Division_Name IS NULL  
                        OR  New_Division IS NULL)  
                    AND Is_Validation_Passed = 1;  
  
                -- check for duplicate sites  
  
                ;  
                WITH CTE_DuplicateSiteID  
                AS (  
                       SELECT  
                            Site_ID  
                            , COUNT(Site_ID) AS DuplicateCount  
                       FROM  
                            #Update_Sites_To_New_Division_Stage  
                       GROUP BY  
                           Site_ID  
                       HAVING  
                            COUNT(Site_ID) > 1  
                   )  
                UPDATE  
                    s  
                SET  
                    s.Error_Msg = 'Duplicate Site ID'  
                    , s.Is_Validation_Passed = 0  
                FROM  
                    #Update_Sites_To_New_Division_Stage s  
                    JOIN CTE_DuplicateSiteID ds  
                        ON s.Site_ID = ds.Site_ID  
                           AND  s.IS_Validation_Passed = 1;  
  
  
                -- Site ID validation  
  
                UPDATE  
                    #Update_Sites_To_New_Division_Stage  
                SET  
                    Error_Msg = 'Invalid Site ID/ Site does not belong to the current division'  
                    , Is_Validation_Passed = 0  
                FROM  
                    #Update_Sites_To_New_Division_Stage s1  
                WHERE  
                    NOT EXISTS (   SELECT  
                                        1  
                                   FROM  
                                        Core.Client_Hier s2  
                                   WHERE  
                                        s2.Site_Id = s1.Site_Id  
                                        AND s2.Sitegroup_Name = s1.Division_Name)  
                    AND s1.IS_Validation_Passed = 1;  
  
  
  
                -- New Division name validation. It must belong to same client  
  
                UPDATE  
                    #Update_Sites_To_New_Division_Stage  
                SET  
                    Error_Msg = 'New Division does not belong to client of Site'  
                    , Is_Validation_Passed = 0  
                FROM  
                    #Update_Sites_To_New_Division_Stage s1  
                    JOIN Core.Client_Hier CH  
                        ON s1.Site_ID = CH.Site_Id  
                WHERE  
                    NOT EXISTS (   SELECT  
                                        1  
                                   FROM  
                                        dbo.Sitegroup s2  
                                        JOIN dbo.Code c  
                                            ON s2.Sitegroup_Type_Cd = c.Code_Id  
                                               AND  c.Code_Value = 'Division'  
                                   WHERE  
                                        s2.Sitegroup_Name = s1.New_Division  
                                        AND s2.Client_Id = CH.Client_Id)  
                    AND s1.IS_Validation_Passed = 1;  
  
  
                -- Current Division and New Division are same  
  
                UPDATE  
                    #Update_Sites_To_New_Division_Stage  
                SET  
                    Error_Msg = 'Current Division and New Division are same'  
                    , Is_Validation_Passed = 0  
                FROM  
                    #Update_Sites_To_New_Division_Stage s  
                WHERE  
                    s.Division_Name = s.New_Division  
                    AND s.IS_Validation_Passed = 1;  
  
  
  
                -- New division should not have the same site name as the site that is being moved  
  
                UPDATE  
                    #Update_Sites_To_New_Division_Stage  
                SET  
                    Error_Msg = 'New division already have same site name'  
                    , Is_Validation_Passed = 0  
                FROM  
                    #Update_Sites_To_New_Division_Stage s  
                    JOIN Core.Client_Hier c1  
                        ON s.Site_Id = c1.Site_Id  
                WHERE  
                    EXISTS (   SELECT  
                                    1  
                               FROM  
                                    Core.Client_Hier c  
                               WHERE  
                                    c.Site_Id <> s.Site_Id  
                                    AND c.Site_name = s.Site_name  
                                    AND c.Sitegroup_Name = s.New_Division  
                                    AND c.Client_Id = c1.Client_Id)  
                    AND s.IS_Validation_Passed = 1;  
  
  
                INSERT INTO ETL.Data_Conversion_Batch_Dtl  
                     (  
                         Data_Conversion_Batch_Id  
                         , [Division_Name]  
                         , Site_Name  
                         , [Site id]  
                         , Is_Data_Validation_Passed  
                         , Error_Msg  
                     )  
                SELECT  
                    @Data_Conversion_Batch_Id  
                    , [New_Division]  
                    , cca.Site_Name  
                    , cca.Site_ID  
                    , cca.Is_Validation_Passed  
                    , cca.Error_Msg  
                FROM  
                    #Update_Sites_To_New_Division_Stage cca;  
  
            END;  
  
        DROP TABLE #Site_Commodity;  
       DROP TABLE #Unique_DMO_List;  
  
    END;  
  
  
  
  
  
  
GO






GRANT EXECUTE ON  [dbo].[Data_Conversion_Batch_Dtl_Ins] TO [ETL_Execute]
GO
