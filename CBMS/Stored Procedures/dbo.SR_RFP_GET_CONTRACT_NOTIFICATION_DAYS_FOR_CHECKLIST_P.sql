SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_CONTRACT_NOTIFICATION_DAYS_FOR_CHECKLIST_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@contractNumber	varchar(20)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE    PROCEDURE DBO.SR_RFP_GET_CONTRACT_NOTIFICATION_DAYS_FOR_CHECKLIST_P

@contractNumber varchar(20)

AS
set nocount on
	select 	 notification_days 
	
	from	contract
	
	where	ed_contract_number = @contractNumber
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_CONTRACT_NOTIFICATION_DAYS_FOR_CHECKLIST_P] TO [CBMSApplication]
GO
