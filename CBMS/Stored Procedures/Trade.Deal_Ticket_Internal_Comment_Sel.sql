SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    Trade.Deal_Ticket_Internal_Comment_Sel
   
    
DESCRIPTION:   
   
  This procedure to get summary page details for deal ticket summery.
    
INPUT PARAMETERS:    
    Name			DataType       Default        Description    
-----------------------------------------------------------------------------    
	@Deal_Ticket_Id   INT
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec Trade.Deal_Ticket_Internal_Comment_Sel  41
	Exec Trade.Deal_Ticket_Internal_Comment_Sel  45
	Exec Trade.Deal_Ticket_Internal_Comment_Sel  291
	Exec Trade.Deal_Ticket_Internal_Comment_Sel  431
	Exec Trade.Deal_Ticket_Internal_Comment_Sel  657
	Exec Trade.Deal_Ticket_Internal_Comment_Sel  131454
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	RR			Raghu Reddy   
    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	RR          2018-11-21  Global Risk Management - Create sp to get summary page details for deal ticket summery
     
    
******/  

CREATE PROCEDURE [Trade].[Deal_Ticket_Internal_Comment_Sel] ( @Deal_Ticket_Id INT )
AS 
BEGIN  
      SET NOCOUNT ON;	
      
      DECLARE @Tbl_Comments AS TABLE
            ( 
             Comment_ID INT
            ,Comment_Text VARCHAR(MAX)
            ,Comment_Date DATETIME
            ,Created_Ts DATETIME
            ,Comment_By VARCHAR(50) )
      
      INSERT      INTO @Tbl_Comments
                  ( 
                   Comment_ID
                  ,Comment_Text
                  ,Comment_Date
                  ,Comment_By )
                  SELECT
                        comm.Comment_ID
                       ,comm.Comment_Text
                       ,dt.Created_Ts AS Comment_Date
                       ,ui.USERNAME AS Comment_By
                  FROM
                        Trade.Deal_Ticket dt
                        INNER JOIN dbo.Comment comm
                              ON dt.Comment_Id = comm.Comment_ID
                        INNER JOIN dbo.USER_INFO ui
                              ON dt.Created_User_Id = ui.USER_INFO_ID
                  WHERE
                        DT.Deal_Ticket_Id = @Deal_Ticket_Id
                        AND comm.Comment_Text IS NOT NULL
                        AND comm.Comment_Text <> ''
                        
      INSERT      INTO @Tbl_Comments
                  ( 
                   Comment_ID
                  ,Comment_Text
                  ,Comment_Date
                  ,Comment_By )
                  SELECT
                        comm.Comment_ID
                       ,comm.Comment_Text
                       ,dt.Created_Ts AS Comment_Date
                       ,ui.USERNAME AS Comment_By
                  FROM
                        Trade.Deal_Ticket_Comment dt
                        INNER JOIN dbo.Comment comm
                              ON dt.Comment_Id = comm.Comment_ID
                        INNER JOIN dbo.USER_INFO ui
                              ON dt.Created_User_Id = ui.USER_INFO_ID
                  WHERE
                        DT.Deal_Ticket_Id = @Deal_Ticket_Id
                        AND comm.Comment_Text IS NOT NULL
                        AND comm.Comment_Text <> ''
                        
                        
      INSERT      INTO @Tbl_Comments
                  ( 
                   Comment_ID
                  ,Comment_Text
                  ,Comment_Date
                  ,Comment_By )
                  SELECT
                        -1 AS Comment_ID
                       ,chws.Workflow_Status_Comment AS Comment_Text
                       ,chws.Created_Ts
                       ,ui.USERNAME AS Comment_By
                  FROM
                        Trade.Deal_Ticket_Client_Hier dtch
                        INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                              ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
                        INNER JOIN dbo.USER_INFO ui
                              ON chws.Updated_User_Id = ui.USER_INFO_ID
                  WHERE
                        dtch.Deal_Ticket_Id = @Deal_Ticket_Id
                        AND chws.Workflow_Status_Comment IS NOT NULL
                        AND chws.Workflow_Status_Comment <> ''
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          Trade.Deal_Ticket_Client_Hier_TXN_Status txns
                                         WHERE
                                          chws.Deal_Ticket_Client_Hier_Workflow_Status_Id = txns.Deal_Ticket_Client_Hier_Workflow_Status_Id )
                  GROUP BY
                        chws.Workflow_Status_Comment
                       ,chws.Created_Ts
                       ,ui.USERNAME
                      
            
         
      SELECT
            Comment_ID
           ,Comment_Text
           --,Comment_Date
           ,REPLACE(CONVERT(VARCHAR(20), Comment_Date, 106), ' ', '-') + ' at ' + LTRIM(RIGHT(CONVERT(VARCHAR(20), Comment_Date, 100), 7)) + ' EST' AS Comment_Date_Time
           ,REPLACE(CONVERT(VARCHAR(20), Comment_Date, 106), ' ', '-') AS Comment_Date
           ,Comment_By
      FROM
            @Tbl_Comments
      ORDER BY
            CAST(Comment_Date AS DATETIME2) DESC
  
END;
GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Internal_Comment_Sel] TO [CBMSApplication]
GO
