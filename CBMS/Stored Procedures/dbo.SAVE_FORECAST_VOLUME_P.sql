SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SAVE_FORECAST_VOLUME_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(50)	          	
	@clientId      	int       	          	
	@forecast_year 	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE [dbo].[SAVE_FORECAST_VOLUME_P]
   @userId varchar(10)
 , @sessionId varchar(50)
 , @clientId int
 , @forecast_year int
--@forecast_as_of_date datetime
AS
set nocount on
   declare @cnt int

   select   @cnt = count(1)
   from     rm_forecast_volume
   where    forecast_year = @forecast_year 
		--and forecast_as_of_date=@forecast_as_of_date 
            and client_id = @clientId
   if ( @cnt > 0 ) 
      update   rm_forecast_volume
      set      forecast_as_of_date = getdate()
      where    --forecast_year=@forecast_year and 
               client_id = @clientId

   else 
      insert   into rm_forecast_volume
               (
                 client_id
               , forecast_year
               , forecast_as_of_date
               )
      values   (
                 @clientId
               , @forecast_year
               , getdate()
               )
GO
GRANT EXECUTE ON  [dbo].[SAVE_FORECAST_VOLUME_P] TO [CBMSApplication]
GO
