SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_RC_DOCUMENTS_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@clientId      	int       	          	
	@sites         	varchar(500)	          	
	@accounts      	varchar(500)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
EXEC SR_SAD_GET_RC_DOCUMENTS_INFO_P 0,null,null

EXEC SR_SAD_GET_RC_DOCUMENTS_INFO_P 218,null,null


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	SSR			09/27/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on	        		        	
******/
CREATE PROCEDURE dbo.SR_SAD_GET_RC_DOCUMENTS_INFO_P
    @clientId INT
  , @sites VARCHAR(500)
  , @accounts VARCHAR(500)
AS
BEGIN

    SET nocount ON
    DECLARE @selectClause VARCHAR(8000)
    DECLARE @fromClause VARCHAR(8000)
    DECLARE @whereClause VARCHAR(8000)
    DECLARE @SQLStatement VARCHAR(8000)

    DECLARE @rcCommercialEntityId INT
    SELECT
        @rcCommercialEntityId = ( SELECT
                                    ENTITY_ID
                                  FROM
                                    ENTITY
                                  WHERE
                                    entity_name = 'RC for Commercial '
                                    AND entity_type = 100
                                )
    DECLARE @rcContractEntityId INT
    SELECT
        @rcContractEntityId = ( SELECT
                                    ENTITY_ID
                                FROM
                                    ENTITY
                                WHERE
                                    entity_name = 'Contract Review '
                                    AND entity_type = 100
                              )

    DECLARE @site INT

    IF ( @sites IS NULL ) 
        BEGIN		
            SET @site = 0
        END 
	

    SELECT
        @selectClause = '	rcDoc.rc_image_id as rcImageId,
					cbmsImage1.cbms_doc_id as rcDocId,
					cbmsImage1.content_type as rcContentType,
					rcDoc.contract_image_id as contractImageId,
					cbmsImage2.cbms_doc_id as contractDocId,
					cbmsImage2.content_type as contractContentType,
					c.client_name as clientName,
					vwSite.site_name as Site,
					a.account_id as accountId,
					a.account_number as accountNumber,
					rcDoc.uploaded_date as uploadedDate'


    SELECT
        @fromClause = ' 	client c, 
					vwSiteName vwSite, 	
					site s,	
					account a			
					left join sr_rc_contract_document_accounts_map accMap on accMap.account_id = a.account_id
					left join sr_rc_contract_document rcDoc on rcDoc.sr_rc_contract_document_id = accMap.sr_rc_contract_document_id
					left join cbms_image cbmsImage1 on cbmsImage1.cbms_image_id = rcDoc.rc_image_id and cbmsImage1.cbms_image_type_id = '
        + STR(@rcCommercialEntityId)
        + ' left join cbms_image cbmsImage2 on cbmsImage2.cbms_image_id = rcDoc.contract_image_id and cbmsImage2.cbms_image_type_id = '
        + STR(@rcContractEntityId)  
     	      	
	
    SELECT
        @whereClause = '	vwSite.site_id = a.site_id
					and c.client_id = vwSite.client_id	
					and s.site_id = vwSite.site_id
					and accMap.sr_rc_contract_document_id is not null '

    IF @clientId > 0
        BEGIN
            SELECT
                @whereClause = @whereClause + ' and  c.client_id = '
                + STR(@clientId) 
        END
	
    IF @sites IS NOT NULL
        AND @sites > '0' 
        BEGIN
            SELECT
                @whereClause = @whereClause + '  and s.site_id in (' + @sites
                + ')'

        END

    IF @accounts IS NOT NULL
        AND @accounts > '0' 
        BEGIN

            SELECT
                @whereClause = @whereClause + '  and a.account_id in ('
                + @accounts + ')'

        END

    SELECT
        @SQLStatement = 'SELECT ' + @selectClause + ' FROM ' + @fromClause
        + ' WHERE ' + @whereClause + ' order by c.client_name asc '

    EXEC ( @SQLStatement )

END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_RC_DOCUMENTS_INFO_P] TO [CBMSApplication]
GO
