
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [DBO].[Account_Group_Sel_By_Account_Id]  
     
DESCRIPTION:

	To Get account group detail for Selected Account Id.

INPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
@Account_Id		INT						
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        

  
	EXEC Account_Group_Sel_By_Account_Id  101527
	
	EXEC Account_Group_Sel_By_Account_Id  364
	
    SELECT TOP 10 * FROM Account where account_group_id is not null
     
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR			PANDARINATH
NR			Narayana Reddy
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR			25-MAY-10	CREATED   
NR			2017-04-04	Added Total Rows for CPH project.  

*/  

CREATE PROCEDURE [dbo].[Account_Group_Sel_By_Account_Id] ( @Account_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
 
      SELECT
            ag.GROUP_BILLING_NUMBER
           ,v.VENDOR_NAME
           ,COUNT(1) AS Total_Rows
      FROM
            dbo.ACCOUNT acc
            JOIN dbo.ACCOUNT_GROUP ag
                  ON acc.ACCOUNT_GROUP_ID = ag.ACCOUNT_GROUP_ID
            JOIN dbo.VENDOR v
                  ON ag.VENDOR_ID = v.VENDOR_ID
      WHERE
            acc.Account_Id = @Account_Id
      GROUP BY
            ag.GROUP_BILLING_NUMBER
           ,v.VENDOR_NAME

END

;
GO

GRANT EXECUTE ON  [dbo].[Account_Group_Sel_By_Account_Id] TO [CBMSApplication]
GO
