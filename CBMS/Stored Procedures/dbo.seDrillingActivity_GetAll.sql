SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[seDrillingActivity_GetAll]
As
BEGIN
	set nocount on
	 select DrillingActivityId
				, ReportDate
				, Volume
		 from seDrillingActivity
 order by ReportDate desc
END
GO
GRANT EXECUTE ON  [dbo].[seDrillingActivity_GetAll] TO [CBMSApplication]
GO
