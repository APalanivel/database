
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******    
NAME:    
     
 dbo.Report_UnitCost_Quarterly_differential_Budget    
    
DESCRIPTION:    
 This report is to get unit Cost (Budget Variances)    
    
INPUT PARAMETERS:    
Name   DataType Default Description    
-------------------------------------------------------------------------------------------    
@Client_id  INT        
@Metric_Id  INT        
@Commodity_Id   INT        
@Budget_id  INT        
@Compare_Month_Id INT        
@Site_Status INT       
@Average_type INT     
    
OUTPUT PARAMETERS:    
Name   DataType  Default Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
-------------------------------------------------------------    
Report_UnitCost_Quarterly_differential_Budget 235,171,291,5139,40573,0,'Straight'     
Report_UnitCost_Quarterly_differential_Budget 235,142,291,5139,40573,0,'Weight'        
Report_UnitCost_Quarterly_differential_Budget 235,0,291,5139,40297,0        
Report_UnitCost_Quarterly_differential_Budget 235,0,291,1217,40297,0      
  
Report_UnitCost_Quarterly_differential_Budget 235,171,290,6487,40573,0,'Straight'    
Report_UnitCost_Quarterly_differential_Budget 235,171,290,6487,40573,0,'Weight'    
    
AUTHOR INITIALS:    
Initials  Name    
------------------------------------------------------------    
SSR    Sharad srivastava    
AKR             Ashok Kumar Raju  
  
MODIFICATIONS   
Initials Date  Modification    
------------------------------------------------------------    
 SSR  01/17/2011 Created    
 SKA  01/28/2011 Modified to show 0 in case the compare month having 0 value.    
 SSR  03/02/2011 Added parameter @Average_Type Changed logic according to parameter (Straight or weighted Average)    
 LEC        03/19/2012  Added Core.Client_Hier to account for changes in Core.Client_Attribute_Tracking which eliminated Client_ID  
 AKR        2012-10-01  Modifed the Code to select the Cost_Usage data directly from COst_Usage_Site_Dtl, instead from RPT_VARIANCE_COST_USAGE_SITE_% tables  
 AKR        2014-06-11  Modified Attribute tables to refer DV2 on the same instance.(Changes for CBMS RO)  
 AKR        2014-08-13  Modifed the Site_Id reference. 
*/    
CREATE PROCEDURE [dbo].[Report_UnitCost_Quarterly_differential_Budget]
      ( 
       @Client_id INT
      ,@Metric_Id INT
      ,@Commodity_Id INT
      ,@Budget_id INT
      ,@Compare_Month_Id INT
      ,@Site_Status INT
      ,@Average_type VARCHAR(50) )
AS 
BEGIN        
    
      SET NOCOUNT ON        
          
      DECLARE
            @ng_uom INT
           ,@ep_uom INT
           ,@USD_Unit INT
           ,@Compare_date DATE
           ,@Year_Num INT
           ,@month_num INT
           ,@Month_Name VARCHAR(20)
           ,@Entity_Name VARCHAR(50)
           ,@active_table VARCHAR(50)
           ,@SQL VARCHAR(MAX)
           ,@Account_type_id INT
           ,@currency_factor DECIMAL(18, 5) = 1
           ,@Base_Unit_id INT = 25
           ,@Total_Cost INT
           ,@Total_Usage INT
           ,@Service_Month DATE  
        
        
      CREATE TABLE #Compare_Month_UnitCost
            ( 
             Site_Id INT
            ,Site_Name VARCHAR(200)
            ,Usage DECIMAL(38, 6)
            ,TotalCost DECIMAL(38, 6) )    
              
      SELECT
            @Total_Cost = MAX(CASE WHEN bm.Bucket_Name = 'Total Cost' THEN bm.Bucket_Master_Id
                              END)
           ,@Total_Usage = MAX(CASE WHEN bm.Bucket_Name = 'Total Usage' THEN bm.Bucket_Master_Id
                               END)
      FROM
            dbo.Bucket_Master bm
            INNER JOIN dbo.Commodity com
                  ON bm.Commodity_Id = com.Commodity_Id
      WHERE
            bm.Commodity_Id = @Commodity_Id      
        
        
      SELECT
            @Service_Month = DATE_D
      FROM
            meta.DATE_DIM
      WHERE
            DATE_ID = @Compare_Month_Id  
          
        
        
      SELECT
            @Compare_date = dd.Date_D
           ,@Year_Num = dd.Year_Num
           ,@month_num = dd.Month_Num
      FROM
            meta.Date_Dim dd
      WHERE
            dd.date_id = @Compare_Month_Id        
        
      SELECT
            @Entity_Name = ent.ENTITY_NAME
      FROM
            dbo.ENTITY ent
      WHERE
            ent.ENTITY_ID = @Commodity_Id         
            
      SET @Month_Name = 'Month' + CAST(@Month_num AS CHAR)    
        
      SELECT
            @ng_uom = ent.Entity_id
      FROM
            dbo.ENTITY ent
      WHERE
            ent.ENTITY_NAME = 'MMBtu'
            AND ent.ENTITY_DESCRIPTION = 'Unit for Gas'    
              
      SELECT
            @ep_uom = ent.Entity_id
      FROM
            dbo.ENTITY ent
      WHERE
            ent.ENTITY_NAME = 'KWh'
            AND ent.ENTITY_DESCRIPTION = 'Unit for electricity'    
              
              
      
      SELECT
            @Account_type_id = ent.Entity_id
      FROM
            dbo.ENTITY ent
      WHERE
            ent.ENTITY_NAME = 'Utility'
            AND ent.ENTITY_DESCRIPTION = 'Account Type'    
            
            
      SELECT
            @USD_Unit = cu.CURRENCY_UNIT_ID
      FROM
            dbo.CURRENCY_UNIT cu
      WHERE
            cu.CURRENCY_UNIT_NAME = 'USD'  
        
          
      INSERT      INTO #Compare_Month_UnitCost
                  ( 
                   Site_Name
                  ,Site_id
                  ,Usage
                  ,TotalCost )
                  SELECT
                        ch.Site_name
                       ,Ch.Site_Id
                       ,ISNULL(MAX(CASE WHEN cusd.Bucket_Master_Id = @Total_Usage THEN CASE WHEN com.commodity_Name = 'Electric Power' THEN cusd.Bucket_Value * cucep.CONVERSION_FACTOR
                                                                                            WHEN com.commodity_Name = 'Natural Gas' THEN cusd.Bucket_Value * cucng.CONVERSION_FACTOR
                                                                                       END
                                   END), 0)
                       ,ISNULL(MAX(CASE WHEN cusd.Bucket_Master_Id = @Total_Cost THEN cusd.Bucket_Value * cuc.CONVERSION_FACTOR
                                   END), 0)
                  FROM
                        core.Client_Hier ch
                        LEFT JOIN dbo.Cost_Usage_Site_Dtl cusd
                              ON cusd.Client_Hier_Id = ch.Client_Hier_Id
                                 AND cusd.Service_Month = @Service_Month
                                 AND cusd.Bucket_Master_Id IN ( @Total_Cost, @Total_Usage )
                        LEFT JOIN dbo.Bucket_Master bm
                              ON cusd.Bucket_Master_Id = bm.Bucket_Master_Id
                        LEFT JOIN dbo.Commodity com
                              ON bm.Commodity_Id = com.Commodity_Id
                        LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cucep
                              ON cucep.BASE_UNIT_ID = cusd.UOM_Type_Id
                                 AND cucep.CONVERTED_UNIT_ID = @ep_uom
                        LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cucng
                              ON cucng.BASE_UNIT_ID = cusd.UOM_Type_Id
                                 AND cucng.CONVERTED_UNIT_ID = @ng_uom
                        LEFT JOIN dbo.CURRENCY_UNIT_CONVERSION cuc
                              ON cuc.BASE_UNIT_ID = cusd.CURRENCY_UNIT_ID
                                 AND cuc.CONVERTED_UNIT_ID = @USD_Unit
                                 AND cuc.CONVERSION_DATE = cusd.Service_Month
                                 AND cuc.CURRENCY_GROUP_ID = ch.Client_Currency_Group_Id
                  WHERE
                        ch.Client_Id = @Client_id
                        AND ch.Site_Id > 0
                  GROUP BY
                        ch.Site_name
                       ,Ch.Site_Id;  
      WITH  CTE_Final_All
              AS ( SELECT
                        Final.SITE_ID
                       ,Final.SITE_NAME
                       ,Final.month_Name
                       ,Final.Attribute_Value
                       ,Final.unit_cost
                       ,AVG(final.unit_cost) OVER ( PARTITION BY final.Site_id ) FY_Avg
                       ,SUM(final.tc) OVER ( PARTITION BY final.Site_id ) FY_TC
                       ,SUM(final.Usage) OVER ( PARTITION BY final.Site_id ) FY_Usage
                       ,SUM(final.tc) OVER ( PARTITION BY final.Site_id, final.Qtr_distribution ) Qtr_TC
                       ,SUM(final.Usage) OVER ( PARTITION BY final.Site_id, final.Qtr_distribution ) Qtr_Usage
                       ,SUM(final.unit_cost) OVER ( PARTITION BY final.Site_id, final.Qtr_distribution ) Qtr_Avg
                       ,( CASE SUM(cmuc.usage)
                            WHEN 0 THEN 0
                            ELSE SUM(cmuc.TotalCost) / ( SUM(cmuc.usage) * cconng_prv_month.conversion_factor )
                          END ) AS [Compare_Month_unit_cost]
                       ,Final.Qtr_distribution
                   FROM
                        ( SELECT
                              x.SITE_ID
                             ,x.SITE_NAME
                             ,cat.Attribute_Value
                             ,x.month_Name
                             ,x.budget_id
                             ,( CASE SUM(x.usage)
                                  WHEN 0 THEN 0
                                  ELSE SUM(x.total_cost) / ( SUM(x.usage) )
                                END ) AS [unit_cost]
                             ,x.Qtr_distribution
                             ,SUM(x.usage) Usage
                             ,SUM(x.Total_cost) Tc
                          FROM
                              ( SELECT
                                    ch.CLIENT_ID
                                   ,ch.SITE_ID
                                   ,ch.SITE_NAME
                                   ,MONTH(bd.month_identifier) AS [month_Name]
                                   ,CASE WHEN MONTH(bd.month_identifier) IN ( 1, 2, 3 )
                                              AND ( YEAR(bd.month_identifier) = YEAR(GETDATE()) ) THEN 'QTR1'
                                         WHEN MONTH(bd.month_identifier) IN ( 4, 5, 6 )
                                              AND ( YEAR(bd.month_identifier) = YEAR(GETDATE()) ) THEN 'QTR2'
                                         WHEN MONTH(bd.month_identifier) IN ( 7, 8, 9 )
                                              AND ( YEAR(bd.month_identifier) = YEAR(GETDATE()) ) THEN 'QTR3'
                                         WHEN MONTH(bd.month_identifier) IN ( 10, 11, 12 )
                                              AND ( YEAR(bd.month_identifier) = YEAR(GETDATE()) ) THEN 'QTR4'
                                    END Qtr_distribution
                                   ,acct.ACCOUNT_ID
                                   ,( ( ( ISNULL(bd.variable_value, 0) ) + ISNULL(bd.rates_tax_value, 0) + ISNULL(bd.sourcing_tax_value, 0) + ISNULL(bd.transportation_value, 0) + ISNULL(bd.transmission_value, 0) + ISNULL(bd.distribution_value, 0) + ISNULL(bd.other_bundled_value, 0) ) * ISNULL(bd.budget_usage, ISNULL(bu.volume, 0)) ) + ISNULL(bd.other_fixed_costs_value, 0) AS [total_cost]
                                   ,ISNULL(bd.budget_usage, ISNULL(bu.volume, 0)) usage
                                   ,b.budget_id
                                FROM
                                    core.Client_Hier ch
                                    JOIN dbo.address a
                                          ON a.address_parent_id = ch.site_id
                                    JOIN ( SELECT
                                                cha.Account_Id
                                               ,cha.Meter_Address_ID address_id
                                           FROM
                                                core.Client_Hier_Account cha
                                           GROUP BY
                                                cha.Account_Id
                                               ,cha.Meter_Address_ID ) AS acct
                                          ON acct.address_id = a.address_id
                                    JOIN dbo.budget_account ba
                                          ON ba.account_id = acct.account_id
                                             AND ba.is_deleted = 0
                                    JOIN dbo.budget b
                                          ON b.budget_id = ba.budget_id
                                    LEFT OUTER JOIN dbo.budget_details bd
                                          ON bd.budget_account_id = ba.budget_account_id
                                    LEFT OUTER JOIN dbo.budget_usage bu
                                          ON bu.account_id = ba.account_id
                                             AND bd.month_identifier = bu.month_identifier
                                             AND b.commodity_type_id = bu.commodity_type_id
                                WHERE
                                    ch.Site_Closed = 0
                                    AND ( @Site_Status = -1
                                          OR ch.Site_Not_Managed = @Site_Status )
                                    AND b.budget_id = @budget_id
                                    AND b.COMMODITY_TYPE_ID = @Commodity_Id
                                GROUP BY
                                    ch.CLIENT_ID
                                   ,ch.SITE_ID
                                   ,ch.SITE_NAME
                                   ,bd.month_identifier
                                   ,MONTH(bd.month_identifier)
                                   ,( ( ( ISNULL(bd.variable_value, 0) ) + ISNULL(bd.rates_tax_value, 0) + ISNULL(bd.sourcing_tax_value, 0) + ISNULL(bd.transportation_value, 0) + ISNULL(bd.transmission_value, 0) + ISNULL(bd.distribution_value, 0) + ISNULL(bd.other_bundled_value, 0) ) * ISNULL(bd.budget_usage, ISNULL(bu.volume, 0)) ) + ISNULL(bd.other_fixed_costs_value, 0)
                                   ,ISNULL(bd.budget_usage, ISNULL(bu.volume, 0))
                                   ,b.budget_id
                                   ,acct.ACCOUNT_ID ) x --new  
                              LEFT JOIN ( SELECT
                                                cat1.attribute_value
                                               ,cat1.client_attribute_id
                                               ,ca.client_id
                                               ,cat1.client_hier_id
                                               ,ch.site_id
                                          FROM
                                                dv2.core.client_attribute ca
                                                JOIN core.client_hier ch
                                                      ON ch.client_id = ca.client_id
                                                JOIN dv2.core.client_attribute_tracking cat1
                                                      ON cat1.client_attribute_id = ca.client_attribute_id
                                                         AND cat1.client_hier_id = ch.client_hier_id ) cat
                                    ON cat.site_id = x.site_id
                                       AND cat.client_id = x.client_id
                                       AND cat.client_attribute_id = @Metric_Id
                          GROUP BY
                              x.SITE_ID
                             ,x.SITE_NAME
                             ,cat.Attribute_Value
                             ,x.month_Name
                             ,x.budget_id
                             ,x.Qtr_distribution ) Final
                        JOIN #Compare_Month_UnitCost cmuc
                              ON cmuc.Site_Id = Final.SITE_ID
                        JOIN dbo.consumption_unit_conversion cconng_prv_month
                              ON ( cconng_prv_month.base_unit_id = @Base_Unit_id
                                   AND cconng_prv_month.converted_unit_id = @ng_uom )
                   GROUP BY
                        Final.SITE_ID
                       ,Final.SITE_NAME
                       ,Final.month_Name
                       ,Final.Attribute_Value
                       ,Final.unit_cost
                       ,cmuc.usage
                       ,cmuc.TotalCost
                       ,Final.Usage
                       ,Final.Tc
                       ,cconng_prv_month.conversion_factor
                       ,final.Qtr_distribution),
            CTE_Pvt_Fin_Avg
              AS ( SELECT
                        pvt_final.SITE_ID
                       ,pvt_final.SITE_NAME
                       ,pvt_final.Attribute_Value
                       ,pvt_final.[1]
                       ,pvt_final.[2]
                       ,pvt_final.[3]
                       ,pvt_final.[4]
                       ,pvt_final.[5]
                       ,pvt_final.[6]
                       ,pvt_final.[7]
                       ,pvt_final.[8]
                       ,pvt_final.[9]
                       ,pvt_final.[10]
                       ,pvt_final.[11]
                       ,pvt_final.[12]
                       ,pvt_final.FY_Avg
                       ,pvt_final.[Compare_Month_unit_cost]
                       ,pvt_final.FY_TC
                       ,pvt_final.FY_Usage
                   FROM
                        ( SELECT
                              a.SITE_ID
                             ,a.Site_Name
                             ,a.month_Name
                             ,a.Attribute_Value
                             ,a.Unit_Cost
                             ,FY_Avg
                             ,a.FY_TC
                             ,a.FY_Usage
                             ,a.Compare_Month_Unit_Cost
                          FROM
                              CTE_Final_All a ) pvt PIVOT ( SUM(unit_cost) FOR month_Name IN ( [1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12] ) ) pvt_final),
            CTE_pvt_Qtr_tC
              AS ( SELECT
                        pvt_final.SITE_ID
                       ,pvt_final.SITE_NAME
                       ,pvt_final.[QTR1] Qtr_Tc_1
                       ,pvt_final.[QTR2] Qtr_Tc_2
                       ,pvt_final.[QTR3] Qtr_Tc_3
                       ,pvt_final.[QTR4] Qtr_Tc_4
                   FROM
                        ( SELECT
                              a.SITE_ID
                             ,a.Site_Name
                             ,a.Qtr_distribution
                             ,a.Qtr_TC
                          FROM
                              CTE_Final_All a ) pvt PIVOT ( MAX(qtr_tc) FOR Qtr_distribution IN ( [QTR1], [QTR2], [QTR3], [QTR4] ) ) pvt_final),
            CTE_pvt_Qtr_Usage
              AS ( SELECT
                        pvt_final.SITE_ID
                       ,pvt_final.SITE_NAME
                       ,pvt_final.[QTR1] Qtr_Usage_1
                       ,pvt_final.[QTR2] Qtr_Usage_2
                       ,pvt_final.[QTR3] Qtr_Usage_3
                       ,pvt_final.[QTR4] Qtr_Usage_4
                   FROM
                        ( SELECT
                              a.SITE_ID
                             ,a.Site_Name
                             ,a.Qtr_distribution
                             ,a.Qtr_Usage
                          FROM
                              CTE_Final_All a ) pvt PIVOT ( MAX(Qtr_Usage) FOR Qtr_distribution IN ( [QTR1], [QTR2], [QTR3], [QTR4] ) ) pvt_final),
            CTE_Final_Query
              AS ( SELECT
                        cte_fin_a.SITE_ID
                       ,cte_fin_a.SITE_NAME
                       ,cte_fin_a.Attribute_Value
                       ,cte_avg.[1]
                       ,cte_avg.[2]
                       ,cte_avg.[3]
                       ,cte_avg.[4]
                       ,cte_avg.[5]
                       ,cte_avg.[6]
                       ,cte_avg.[7]
                       ,cte_avg.[8]
                       ,cte_avg.[9]
                       ,cte_avg.[10]
                       ,cte_avg.[11]
                       ,cte_avg.[12]
                       ,CASE WHEN @Average_type = 'Straight' THEN cte_avg.FY_Avg
                             ELSE ( CASE cte_avg.FY_Usage
                                      WHEN 0 THEN 0
                                      ELSE cte_avg.FY_TC
                                    END / CASE cte_avg.FY_Usage
                                            WHEN 0 THEN 1
                                            ELSE cte_avg.FY_Usage
                                          END )
                        END Average_Val
                       ,cte_avg.Compare_Month_unit_cost
                       ,( CASE cte_Usage.Qtr_Usage_1
                            WHEN 0 THEN 0
                            ELSE cte_Tc.Qtr_Tc_1
                          END / CASE cte_Usage.Qtr_Usage_1
                                  WHEN 0 THEN 1
                                  ELSE cte_Usage.Qtr_Usage_1
                                END ) Weight_Avg_1
                       ,( CASE cte_Usage.Qtr_Usage_2
                            WHEN 0 THEN 0
                            ELSE cte_Tc.Qtr_Tc_2
                          END / CASE cte_Usage.Qtr_Usage_2
                                  WHEN 0 THEN 1
                                  ELSE cte_Usage.Qtr_Usage_2
                                END ) Weight_Avg_2
                       ,( CASE cte_Usage.Qtr_Usage_3
                            WHEN 0 THEN 0
                            ELSE cte_Tc.Qtr_Tc_3
                          END / CASE cte_Usage.Qtr_Usage_3
                                  WHEN 0 THEN 1
                                  ELSE cte_Usage.Qtr_Usage_3
                                END ) Weight_Avg_3
                       ,( CASE cte_Usage.Qtr_Usage_4
                            WHEN 0 THEN 0
                            ELSE cte_Tc.Qtr_Tc_4
                          END / CASE cte_Usage.Qtr_Usage_4
                                  WHEN 0 THEN 1
                                  ELSE cte_Usage.Qtr_Usage_4
                                END ) Weight_Avg_4
                       ,( cte_avg.[1] + cte_avg.[2] + cte_avg.[3] ) Sum_Qtr1
                       ,( cte_avg.[4] + cte_avg.[5] + cte_avg.[6] ) Sum_Qtr2
                       ,( cte_avg.[7] + cte_avg.[8] + cte_avg.[9] ) Sum_Qtr3
                       ,( cte_avg.[10] + cte_avg.[11] + cte_avg.[12] ) Sum_Qtr4
                   FROM
                        CTE_Final_All cte_fin_a
                        JOIN CTE_Pvt_Fin_Avg cte_avg
                              ON cte_avg.SITE_ID = cte_fin_a.SITE_ID
                        JOIN CTE_pvt_Qtr_tC cte_Tc
                              ON cte_Tc.SITE_ID = cte_avg.SITE_ID
                        JOIN CTE_pvt_Qtr_Usage cte_Usage
                              ON cte_Usage.SITE_ID = cte_avg.SITE_ID
                   GROUP BY
                        cte_fin_a.SITE_ID
                       ,cte_fin_a.SITE_NAME
                       ,cte_fin_a.Attribute_Value
                       ,cte_avg.[1]
                       ,cte_avg.[2]
                       ,cte_avg.[3]
                       ,cte_avg.[4]
                       ,cte_avg.[5]
                       ,cte_avg.[6]
                       ,cte_avg.[7]
                       ,cte_avg.[8]
                       ,cte_avg.[9]
                       ,cte_avg.[10]
                       ,cte_avg.[11]
                       ,cte_avg.[12]
                       ,cte_avg.FY_Avg
                       ,cte_avg.FY_TC
                       ,cte_avg.FY_Usage
                       ,cte_avg.Compare_Month_unit_cost
                       ,cte_Tc.Qtr_Tc_1
                       ,cte_Tc.Qtr_Tc_2
                       ,cte_Tc.Qtr_Tc_3
                       ,cte_Tc.Qtr_Tc_4
                       ,cte_Tc.Qtr_Tc_1
                       ,cte_Tc.Qtr_Tc_2
                       ,cte_Tc.Qtr_Tc_3
                       ,cte_Tc.Qtr_Tc_4
                       ,cte_Usage.Qtr_Usage_1
                       ,cte_Usage.Qtr_Usage_2
                       ,cte_Usage.Qtr_Usage_3
                       ,cte_Usage.Qtr_Usage_4)
            SELECT
                  cte_fin_Qr.Site_Id
                 ,cte_fin_Qr.SITE_NAME
                 ,cte_fin_Qr.Attribute_Value
                 ,cte_fin_Qr.[1]
                 ,cte_fin_Qr.[2]
                 ,cte_fin_Qr.[3]
                 ,cte_fin_Qr.[4]
                 ,cte_fin_Qr.[5]
                 ,cte_fin_Qr.[6]
                 ,cte_fin_Qr.[7]
                 ,cte_fin_Qr.[8]
                 ,cte_fin_Qr.[9]
                 ,cte_fin_Qr.[10]
                 ,cte_fin_Qr.[11]
                 ,cte_fin_Qr.[12]
                 ,cte_fin_Qr.Average_Val
                 ,cte_fin_Qr.[Compare_Month_unit_cost]
                 ,CASE WHEN @Average_type = 'Straight' THEN CASE WHEN cte_fin_Qr.Sum_Qtr1 = 0 THEN 0
                                                                 ELSE cte_fin_Qr.Sum_Qtr1 / 3
                                                            END
                       ELSE cte_fin_Qr.Weight_Avg_1
                  END Qtr1
                 ,CASE WHEN @Average_type = 'Straight' THEN CASE WHEN cte_fin_Qr.Sum_Qtr2 = 0 THEN 0
                                                                 ELSE cte_fin_Qr.Sum_Qtr2 / 3
                                                            END
                       ELSE cte_fin_Qr.Weight_Avg_2
                  END Qtr2
                 ,CASE WHEN @Average_type = 'Straight' THEN CASE WHEN cte_fin_Qr.Sum_Qtr3 = 0 THEN 0
                                                                 ELSE cte_fin_Qr.Sum_Qtr3 / 3
                                                            END
                       ELSE cte_fin_Qr.Weight_Avg_3
                  END Qtr3
                 ,CASE WHEN @Average_type = 'Straight' THEN CASE WHEN cte_fin_Qr.Sum_Qtr4 = 0 THEN 0
                                                                 ELSE cte_fin_Qr.Sum_Qtr4 / 3
                                                            END
                       ELSE cte_fin_Qr.Weight_Avg_4
                  END Qtr4
                 ,CASE WHEN cte_fin_Qr.[Compare_Month_unit_cost] = 0 THEN 0
                       ELSE ( CASE WHEN @Average_type = 'Straight' THEN ( CASE WHEN ( ( CASE WHEN cte_fin_Qr.Sum_Qtr1 = 0 THEN 0
                                                                                             ELSE cte_fin_Qr.Sum_Qtr1 / 3
                                                                                        END ) - cte_fin_Qr.[Compare_Month_unit_cost] ) = 0 THEN 0
                                                                               ELSE ( ( CASE WHEN cte_fin_Qr.Sum_Qtr1 = 0 THEN 0
                                                                                             ELSE cte_fin_Qr.Sum_Qtr1 / 3
                                                                                        END ) - cte_fin_Qr.[Compare_Month_unit_cost] ) / cte_fin_Qr.[Compare_Month_unit_cost]
                                                                          END )
                                   ELSE ( cte_fin_Qr.Weight_Avg_1 - cte_fin_Qr.[Compare_Month_unit_cost] ) / cte_fin_Qr.[Compare_Month_unit_cost]
                              END )
                  END '%Change Qtr1'
                 ,CASE WHEN cte_fin_Qr.[Compare_Month_unit_cost] = 0 THEN 0
                       ELSE ( CASE WHEN @Average_type = 'Straight' THEN ( CASE WHEN ( ( CASE WHEN cte_fin_Qr.Sum_Qtr2 = 0 THEN 0
                                                                                             ELSE cte_fin_Qr.Sum_Qtr2 / 3
                                                                                        END ) - cte_fin_Qr.[Compare_Month_unit_cost] ) = 0 THEN 0
                                                                               ELSE ( ( CASE WHEN cte_fin_Qr.Sum_Qtr2 = 0 THEN 0
                                                                                             ELSE cte_fin_Qr.Sum_Qtr2 / 3
                                                                                        END ) - cte_fin_Qr.[Compare_Month_unit_cost] ) / cte_fin_Qr.[Compare_Month_unit_cost]
                                                                          END )
                                   ELSE ( cte_fin_Qr.Weight_Avg_2 - cte_fin_Qr.[Compare_Month_unit_cost] ) / cte_fin_Qr.[Compare_Month_unit_cost]
                              END )
                  END '%Change Qtr2'
                 ,CASE WHEN cte_fin_Qr.[Compare_Month_unit_cost] = 0 THEN 0
                       ELSE ( CASE WHEN @Average_type = 'Straight' THEN ( CASE WHEN ( ( CASE WHEN cte_fin_Qr.Sum_Qtr3 = 0 THEN 0
                                                                                             ELSE cte_fin_Qr.Sum_Qtr3 / 3
                                                                                        END ) - cte_fin_Qr.[Compare_Month_unit_cost] ) = 0 THEN 0
                                                                               ELSE ( ( CASE WHEN cte_fin_Qr.Sum_Qtr3 = 0 THEN 0
                                                                                             ELSE cte_fin_Qr.Sum_Qtr3 / 3
                                                                                        END ) - cte_fin_Qr.[Compare_Month_unit_cost] ) / cte_fin_Qr.[Compare_Month_unit_cost]
                                                                          END )
                                   ELSE ( cte_fin_Qr.Weight_Avg_3 - cte_fin_Qr.[Compare_Month_unit_cost] ) / cte_fin_Qr.[Compare_Month_unit_cost]
                              END )
                  END '%Change Qtr3'
                 ,CASE WHEN cte_fin_Qr.[Compare_Month_unit_cost] = 0 THEN 0
                       ELSE ( CASE WHEN @Average_type = 'Straight' THEN ( CASE WHEN ( ( CASE WHEN cte_fin_Qr.Sum_Qtr4 = 0 THEN 0
                                                                                             ELSE cte_fin_Qr.Sum_Qtr4 / 3
                                                                                        END ) - cte_fin_Qr.[Compare_Month_unit_cost] ) = 0 THEN 0
                                                                               ELSE ( ( CASE WHEN cte_fin_Qr.Sum_Qtr4 = 0 THEN 0
                                                                                             ELSE cte_fin_Qr.Sum_Qtr4 / 3
                                                                                        END ) - cte_fin_Qr.[Compare_Month_unit_cost] ) / cte_fin_Qr.[Compare_Month_unit_cost]
                                                                          END )
                                   ELSE ( cte_fin_Qr.Weight_Avg_4 - cte_fin_Qr.[Compare_Month_unit_cost] ) / cte_fin_Qr.[Compare_Month_unit_cost]
                              END )
                  END '%Change Qtr4'
            FROM
                  CTE_Final_Query cte_fin_Qr    
            
END;  
  
;
;
GO


GRANT EXECUTE ON  [dbo].[Report_UnitCost_Quarterly_differential_Budget] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_UnitCost_Quarterly_differential_Budget] TO [CBMSApplication]
GO
