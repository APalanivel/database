SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[dbo.sp_MSupd_dboRA_User_Monthly_Login_Count]
		@c1 int = NULL,
		@c2 date = NULL,
		@c3 int = NULL,
		@pkc1 int = NULL,
		@pkc2 date = NULL,
		@bitmap binary(1)
as
begin  
if (substring(@bitmap,1,1) & 1 = 1) or
 (substring(@bitmap,1,1) & 2 = 2)
begin 
update [dbo].[RA_User_Monthly_Login_Count] set
		[USER_INFO_ID] = case substring(@bitmap,1,1) & 1 when 1 then @c1 else [USER_INFO_ID] end,
		[Login_Month] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [Login_Month] end,
		[Login_Cnt] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [Login_Cnt] end
where [USER_INFO_ID] = @pkc1
  and [Login_Month] = @pkc2
if @@rowcount = 0
    if @@microsoftversion>0x07320000
        exec sp_MSreplraiserror 20598
end  
else
begin 
update [dbo].[RA_User_Monthly_Login_Count] set
		[Login_Cnt] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [Login_Cnt] end
where [USER_INFO_ID] = @pkc1
  and [Login_Month] = @pkc2
if @@rowcount = 0
    if @@microsoftversion>0x07320000
        exec sp_MSreplraiserror 20598
end 
end 
GO
