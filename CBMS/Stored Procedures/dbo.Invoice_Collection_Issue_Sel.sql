SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******                              
 NAME: dbo.Invoice_Collection_Issue_Sel                  
                              
 DESCRIPTION:                              
   To get the details of Invoice_Collection_Issue                     
                              
 INPUT PARAMETERS:                
                           
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
@Invoice_Collection_Issue_Id INT      
      
                                   
 OUTPUT PARAMETERS:                
                                 
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
                              
 USAGE EXAMPLES:                                  
---------------------------------------------------------------------------------------------------------------                                  
      
            exec [dbo].[Invoice_Collection_Issue_Sel] 199349                            
                             
 AUTHOR INITIALS:              
             
 Initials              Name              
---------------------------------------------------------------------------------------------------------------                            
 SP                    Sandeep Pigilam      
 RKV					Ravi Kumar Vegesna                
 NM						Nagaraju M                 
 MODIFICATIONS:            
                
 Initials              Date             Modification            
---------------------------------------------------------------------------------------------------------------            
 SP                    2016-11-21       Created for Invoice Tracking                     
 RKV                   2017-07-31       Maint-5616,Added new column Invoice_Collection_Issue_Grouped_Queue_Ids to the result set       
 RKV       2019-08-14  Added Next Action Date to the select list        
 RKV                   2019-09-16  Added two new columns Is_Automatic_Link,Issue_Link_Start_Date , Issue_Id 
 NM					2020-02-12	MAINT 9725:- Added filter condition  "sc.Code_value = 'Open' " with code table 
									to avoid closed Periods and fetch open Periods only                         
******/  
  
CREATE PROCEDURE [dbo].[Invoice_Collection_Issue_Sel]  
     (  
         @Invoice_Collection_Issue_Id INT  
     )  
AS  
    BEGIN  
        SET NOCOUNT ON;  
  
        SELECT  
            i.Created_User_Id  
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Created_User  
            , i.Created_Ts  
            , Invoice_Collection_Issue_Type_Cd  
            , Issue_Status_Cd  
            , Issue_Entity_Owner_Cd  
            , Issue_Owner_User_Id  
            , '' AS Event_Type  
            , '' AS Event_Desc  
            , Type_Of_Issue_Owner_Cd AS Contact_Level_Cd  
            , LEFT(icqi.Invoice_Collection_Issue_Grouped_Queue_Ids, LEN(icqi.Invoice_Collection_Issue_Grouped_Queue_Ids)  
                                                                    - 1) Invoice_Collection_Issue_Grouped_Queue_Ids  
            , i.Is_Blocker  
            , i.Blocker_Action_Date  
            , nad1.Next_Action_Dt Next_Action_Date  
            , ica.Issue_Link_Start_Date  
            , ica.Is_Automatic_Link  
            , i.Invoice_Collection_Issue_Log_Id  
            , i.Invoice_Collection_Activity_Id  
        FROM  
            Invoice_Collection_Issue_Log i  
            INNER JOIN dbo.Invoice_Collection_Activity ica  
                ON ica.Invoice_Collection_Activity_Id = i.Invoice_Collection_Activity_Id  
            LEFT OUTER JOIN (   SELECT  
                                    nad.Next_Action_Dt  
                                    , nad.Invoice_Collection_Activity_Id  
                                FROM    (   SELECT  
                                                icaadl.Next_Action_Dt  
                                                , icaadl.Invoice_Collection_Activity_Id  
                                                , ROW_NUMBER() OVER (PARTITION BY  
                                                                         Invoice_Collection_Activity_Id  
                                                                     ORDER BY  
                                                                         Invoice_Collection_Activity_Log_Id DESC) AS seqnum  
                                            FROM  
                                                dbo.Invoice_Collection_Activity_Log icaadl) nad  
                                WHERE  
                                    seqnum = 1) nad1  
                ON nad1.Invoice_Collection_Activity_Id = ica.Invoice_Collection_Activity_Id  
            LEFT JOIN dbo.USER_INFO ui  
                ON i.Created_User_Id = ui.USER_INFO_ID  
            CROSS APPLY (   SELECT  
                                CAST(icil.Invoice_Collection_Queue_Id AS VARCHAR(25)) + ','  
                            FROM  
                                dbo.Invoice_Collection_Issue_Log icil  
					-- MAINT 9725 Added Join and Filter condition to fetch only open periods Start--
                                INNER JOIN dbo.Invoice_Collection_Queue icq  
                                    ON icq.Invoice_Collection_Queue_Id = icil.Invoice_Collection_Queue_Id  
                                INNER JOIN Code sc  
                                    ON sc.Code_Id = icq.Status_Cd  
                            WHERE  
                                icil.Invoice_Collection_Activity_Id = i.Invoice_Collection_Activity_Id  
                                AND sc.Code_value = 'Open' 
					-- MAINT 9725 Added Join and Filter condition to fetch only open periods END-- 
                            FOR XML PATH('')) icqi(Invoice_Collection_Issue_Grouped_Queue_Ids)  
        WHERE  
            i.Invoice_Collection_Issue_Log_Id = @Invoice_Collection_Issue_Id;  
  
    END;  
    ;
GO

GRANT EXECUTE ON  [dbo].[Invoice_Collection_Issue_Sel] TO [CBMSApplication]
GO
