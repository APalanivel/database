SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/**
NAME:    
   
	dbo.RM_Client_Onboard_Sel_By_Client
   
 DESCRIPTION:     
   
	This procedure is to get client onboarded details
   
 INPUT PARAMETERS:    
	Name				DataType		 Default		 Description    
----------------------------------------------------------------------      
	@Client_Id	INT
   
 OUTPUT PARAMETERS:    
 Name				DataType		 Default		 Description    
----------------------------------------------------------------------

  
  USAGE EXAMPLES:    
------------------------------------------------------------  

  
	EXEC RM_Client_Onboard_Sel_By_Client 235
	   
	
  
AUTHOR INITIALS:    
	Initials	Name    
------------------------------------------------------------    
	RR			Raghu Reddy
    
 MODIFICATIONS     
	Initials	Date		Modification    
------------------------------------------------------------    
	RR			01-08-2018	Created For Global Risk Managemnet		


******/

CREATE PROCEDURE [dbo].[RM_Client_Onboard_Sel_By_Client]
    (
        @Client_Id INT
        , @Commodity_Id INT = NULL
        , @Country_Id INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Client_Client_Hier_Id INT;

        SELECT
            @Client_Client_Hier_Id = Client_Hier_Id
        FROM
            Core.Client_Hier
        WHERE
            Client_Id = @Client_Id
            AND Sitegroup_Id = 0;

        SELECT
            cho.RM_Client_Hier_Onboard_Id
            , cho.Commodity_Id
            , com.Commodity_Name
            , cho.COUNTRY_ID
            , con.COUNTRY_NAME
        FROM
            Trade.RM_Client_Hier_Onboard cho
            INNER JOIN dbo.Commodity com
                ON cho.Commodity_Id = com.Commodity_Id
            INNER JOIN dbo.COUNTRY con
                ON cho.COUNTRY_ID = con.COUNTRY_ID
        WHERE
            cho.Client_Hier_Id = @Client_Client_Hier_Id
        ORDER BY
            com.Commodity_Name
            , con.COUNTRY_NAME;

    END;

GO
GRANT EXECUTE ON  [dbo].[RM_Client_Onboard_Sel_By_Client] TO [CBMSApplication]
GO
