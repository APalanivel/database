SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE     PROCEDURE [dbo].[cbmsCuInvoiceDeterminant_UpdateServiceCodeMapping]
   (
     @MyAccountId int
   , @cu_invoice_id int
   , @commodity_type_id int
   , @ubm_service_type_id int
   , @ubm_service_code varchar(200)
   )
AS 
   BEGIN

      set nocount on

      update   cu_invoice_determinant 
      set      commodity_type_id = @commodity_type_id
             , ubm_service_type_id = @ubm_service_type_id
      where    cu_invoice_id = @cu_invoice_id
               and ubm_service_code = @ubm_service_code
               and ubm_service_type_id is null

      update   cu_invoice_charge 
      set      commodity_type_id = @commodity_type_id
             , ubm_service_type_id = @ubm_service_type_id
      where    cu_invoice_id = @cu_invoice_id
               and ubm_service_code = @ubm_service_code
               and ubm_service_type_id is null

--	set nocount off

      select   i.cu_invoice_id
             , isNull(x.det_count, 0) error_count
      from     cu_invoice i 
               left outer join ( select   i.cu_invoice_id
                                        , count(*) det_count
                                 from     cu_invoice i 
                                          join cu_invoice_determinant d
                                          
                                               on d.cu_invoice_id = i.cu_invoice_id
                                 where    i.cu_invoice_id = @cu_invoice_id
                                          and d.ubm_service_type_id is null
                                 group by i.cu_invoice_id
                               ) x
                  on x.cu_invoice_id = i.cu_invoice_id
      where    i.cu_invoice_id = @cu_invoice_id

   END
GO
GRANT EXECUTE ON  [dbo].[cbmsCuInvoiceDeterminant_UpdateServiceCodeMapping] TO [CBMSApplication]
GO
