SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:      
	dbo.Data_Quality_Test_Commodity_In_Active_Upd

DESCRIPTION:
		Created For Data Quality Tests - Global Level Config Page.
		
 INPUT PARAMETERS:      
	Name					DataType			Default				 Description      
-------------------------------------------------------------------------------------------------      
	
 OUTPUT PARAMETERS:     
	Name					DataType			Default				 Description      
-------------------------------------------------------------------------------------------------   
   
 USAGE EXAMPLES:      
-------------------------------------------------------------------------------------------------      

EXEC dbo.Data_Quality_Test_Commodity_In_Active_Upd
    @Data_Quality_Test_Commodity_Id = 2
    , @Is_Active = 0
    , @User_Info_Id = 49
	
 AUTHOR INITIALS:
 Initials	Name
-------------------------------------------------------------------------------------------------      
 NR			Narayana Reddy
 
 MODIFICATIONS
 Initials	     Date		     Modification
-------------------------------------------------------------------------------------------------      
 NR				2019-04-23		Created Data2.0 -  Data Quality Tests - Global Level Config Page.

******/

CREATE PROCEDURE [dbo].[Data_Quality_Test_Commodity_In_Active_Upd]
    (
          @Data_Quality_Test_Commodity_Id INT
        , @Is_Active BIT
        , @User_Info_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;


        UPDATE
            dqtc
        SET
            dqtc.Is_Active = @Is_Active
            , dqtc.Updated_User_Id = @User_Info_Id
            , dqtc.Last_Change_Ts = GETDATE()
        FROM
            dbo.Data_Quality_Test dqtc
        WHERE
            dqtc.Data_Quality_Test_Id = @Data_Quality_Test_Commodity_Id;


    END;



GO
GRANT EXECUTE ON  [dbo].[Data_Quality_Test_Commodity_In_Active_Upd] TO [CBMSApplication]
GO
