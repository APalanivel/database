SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- exec sp_executesql N'EXEC SR_SAD_DELETE_RC_DOCUMENT_P_test @P1, @P2 ', N'@P1 int ,@P2 int ', 127988, 2086
CREATE PROCEDURE dbo.SR_SAD_DELETE_RC_DOCUMENT_P
	@accountId INT,
	@rfpId INT
AS
BEGIN
  
	SET NOCOUNT ON
	
	DECLARE @entityId INT 
		, @rcContractDocId INT
		, @rcDocId INT
		, @rcContractDocumnetId INT
		, @imageId INT
		, @cnt INT

	SELECT @rcContractDocId = 0
		, @rcContractDocumnetId = 0

	SELECT @entityId = ENTITY_ID FROM dbo.Entity (NOLOCK) WHERE Entity_Name = 'RC for Commercial ' AND entity_type=100

	SELECT @rcContractDocId =  (SELECT rcDoc.sr_rc_contract_document_id
	FROM dbo.cbms_image cbmsImage 
		INNER JOIN dbo.sr_rc_contract_document rcDoc ON rcDoc.rc_image_id = cbmsImage.cbms_image_id
		INNER JOIN dbo.SR_RC_CONTRACT_DOCUMENT_ACCOUNTS_MAP accMap ON accMap.sr_rc_contract_document_id = rcDoc.sr_rc_contract_document_id		
	WHERE accMap.account_id = @accountId
		AND accMap.sr_rfp_id = @rfpId		
		AND cbmsImage.cbms_image_type_id = @entityId )

	SELECT @rcContractDocumnetId = ISNULL((SELECT rcDoc.sr_rc_contract_document_id
	FROM dbo.cbms_image cbmsImage
		INNER JOIN dbo.sr_rc_contract_document rcDoc ON rcDoc.contract_image_id = cbmsImage.cbms_image_id
		INNER JOIN dbo.SR_RC_CONTRACT_DOCUMENT_ACCOUNTS_MAP accMap ON accMap.sr_rc_contract_document_id = rcDoc.sr_rc_contract_document_id
	WHERE accMap.account_id = @accountId
		AND accMap.sr_rfp_id = @rfpId
		),0)

	SELECT @imageId = rc_image_id FROM dbo.sr_rc_contract_document WHERE sr_rc_contract_document_id = @rcContractDocId

	SELECT @cnt = COUNT(1) FROM dbo.sr_rc_contract_document_accounts_map WHERE sr_rc_contract_document_id = @rcContractDocId 

	
	IF (@cnt > 1 or @rcContractDocumnetId > 0 )
	 BEGIN
		/*delete from sr_rc_contract_document_accounts_map 
		where account_id = @accountId 
		and sr_rc_contract_document_id = @rcContractDocId*/

		UPDATE dbo.sr_rc_contract_document 
		SET	rc_image_id = NULL
		WHERE sr_rc_contract_document_id = @rcContractDocId

	 END
	ELSE IF (@cnt = 1)
	 BEGIN
		IF (@rcContractDocumnetId = 0 )
		 BEGIN
			
			DELETE FROM dbo.sr_rc_contract_document_accounts_map 
			WHERE account_id = @accountId 
				AND sr_rfp_id = @rfpId
			
			DELETE FROM dbo.sr_rc_contract_document WHERE rc_image_id = @imageId
			
			/*  commented by Jaya
			delete from cbms_image where cbms_image_id = @imageId */
		 END
		ELSE IF (@rcContractDocumnetId > 0 )
		 BEGIN
			UPDATE dbo.sr_rc_contract_document 
				SET	rc_image_id = NULL
			WHERE sr_rc_contract_document_id = @rcContractDocId
		 END
	 END

	UPDATE rfpChkList 
		SET rfpChkList.is_rc_completed = NULL
	FROM dbo.sr_rfp_checklist rfpChkList 
		INNER JOIN dbo.sr_rfp_account rfpAcct ON rfpAcct.sr_rfp_account_id = rfpChkList.sr_rfp_account_id
	WHERE rfpAcct.account_id = @accountId 
		AND rfpAcct.sr_rfp_id = @rfpId

END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_DELETE_RC_DOCUMENT_P] TO [CBMSApplication]
GO
