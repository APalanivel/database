SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------    
/********************************************************************************************************                          
NAME: dbo.User_Info_Last_Login_Inactive_Sel                  
                             
DESCRIPTION: Procedure to get all new users who did not log in after specific days.              
                      
INPUT PARAMETERS:                            
 Name    DataType    Default     Description                            
------------------------------------------------------------------------------------------                        
 Inactive_Days INT 0 Number of days user is inactive    
                        
OUTPUT PARAMETERS:                            
 Name    DataType    Default      Description                            
------------------------------------------------------------------------------------------                                  
                     
USAGE EXAMPLES:                            
------------------------------------------------------------------------------------------                         
    
   EXEC [User_Info_Last_Login_Inactive_Sel]    
    
AUTHOR INITIALS:                              
 Initials   Name                              
------------------------------------------------------------------------------------------                              
 HK     Harish Kurma    
                       
                       
MODIFICATIONS                               
 Initials   Date     Modification                              
------------------------------------------------------------------------------------------                              
 HK     04 Feb 2019   Created  
 HK     19 Feb 2019   Modified to exclude Single Sign on users         
                    
***********************************************************************************************************/      
CREATE PROCEDURE [dbo].[User_Info_Last_Login_Inactive_Sel]    
  (    
   @Inactive_Days INT    
  )     
AS    
BEGIN    
  SET NOCOUNT ON;    
    
  SELECT     
   ui.USER_INFO_ID,    
    ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Full_Name,    
    ui.EMAIL_ADDRESS    
  FROM     
   dbo.USER_INFO ui     
  LEFT JOIN     
   dbo.RA_User_Login_Ts ul ON ui.USER_INFO_ID = ul.USER_INFO_ID  
  LEFT JOIN  
   dbo.Single_Signon_User_Map ssm ON ui.USER_INFO_ID = ssm.Internal_User_ID    
  WHERE    
 ui.Created_Ts < (GETDATE()-30)    
   AND   
   ui.Created_Ts > '01-01-2019'  
   AND      
 ul.USER_INFO_ID IS NULL    
   AND  
 ssm.Internal_User_ID IS NULL  
   AND    
 ui.ACCESS_LEVEL = 1    
   AND    
 ui.last_Login_Email_Reminder_Ts IS NULL    
END  

GO
GRANT EXECUTE ON  [dbo].[User_Info_Last_Login_Inactive_Sel] TO [CBMSApplication]
GO
