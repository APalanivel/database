
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  

 NAME: dbo.cbmsUser_Search  
 
 DESCRIPTION:   

 INPUT PARAMETERS:  
 Name				DataType		Default Description  
------------------------------------------------------------
@username			varchar(30)		null
@last_name			varchar(40)		null
@first_name			varchar(40)		null
@is_history			bit				0
@access_level		VARCHAR(max)	NULL	Comma seperated value(0,1) , by default fetch all the types of users
@clientname			varchar(400)	null

 OUTPUT PARAMETERS:
 Name				DataType		Default Description
------------------------------------------------------------

 USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.cbmsUser_Search @username = 'megan'
	EXEC dbo.cbmsUser_Search
	EXEC dbo.CbmsUser_Search @ClientName = 'Outback Steakhouse'
	EXEC dbo.CbmsUser_Search @Access_Level= 1

	EXEC dbo.cbmsUser_Search
		@ClientName ='kraft'

	EXEC dbo.CbmsUser_Search @username='test', @Access_Level= '1'
	EXEC dbo.CbmsUser_Search @username='test', @Access_Level= '0'
	EXEC dbo.CbmsUser_Search @username='test', @Access_Level= '0,1'
	
 AUTHOR INITIALS:
 Initials	Name
------------------------------------------------------------
 HG			Harihara Suthan G

 MODIFICATIONS
 Initials	Date			Modification
------------------------------------------------------------
 HG			11/09/2010		Comments header added
							Pulling the passcode value from new USER_PASSCODE table as this column moved there as a part of DV_SV Password Expiration enhancement.
							Unused @myaccountid parameter removed
							Removed the user passcode column as it is used by the application

HG			01/13/2011		implemented the partial search for client name filter like other search column values.
HG			2014-06-01		RA Admin user management changes
								- Modified the input param @Access_Level to VARCHAR(max) to accept the multiple values as a comma seperated value
******/

CREATE PROCEDURE [dbo].[cbmsUser_Search]
      ( 
       @username VARCHAR(30) = NULL
      ,@last_name VARCHAR(40) = NULL
      ,@first_name VARCHAR(40) = NULL
      ,@is_history BIT = 0
      ,@access_level VARCHAR(MAX) = NULL
      ,@clientname VARCHAR(400) = NULL )
AS 
BEGIN

      SET NOCOUNT ON

      SET @username = '%' + @username + '%'
      SET @last_name = '%' + @last_name + '%'
      SET @first_name = '%' + @first_name + '%'
      SET @ClientName = '%' + @ClientName + '%'

      SELECT
            u.user_info_id
           ,u.username
           ,u.queue_id
           ,u.first_name
           ,u.middle_name
           ,u.last_name
           ,u.email_address
           ,u.is_history
           ,u.access_level
           ,u.client_id
           ,u.division_id
           ,u.site_id
           ,c.client_name
           ,d.Sitegroup_Name AS division_name
           ,s.site_name
           ,u.phone_number
      FROM
            dbo.USER_INFO u
            LEFT OUTER JOIN dbo.CLIENT c
                  ON c.client_id = u.client_id
            LEFT OUTER JOIN dbo.Sitegroup d
                  ON d.Sitegroup_Id = u.division_id
            LEFT OUTER JOIN dbo.Site s
                  ON s.site_id = u.site_id
      WHERE
            ( @username IS NULL
              OR u.username LIKE @username )
            AND ( @last_name IS NULL
                  OR u.last_name LIKE @last_name )
            AND ( @first_name IS NULL
                  OR u.first_name LIKE @first_name )
            AND ( u.is_history = @is_history )
            AND ( @access_level IS NULL
                  OR EXISTS ( SELECT
                                    1
                              FROM
                                    dbo.ufn_split(@access_level, ',') al
                              WHERE
                                    al.Segments = u.ACCESS_LEVEL ) )
            AND ( @ClientName IS NULL
                  OR c.client_name LIKE @ClientName )
      ORDER BY
            u.last_name
           ,u.first_name

END


;
GO

GRANT EXECUTE ON  [dbo].[cbmsUser_Search] TO [CBMSApplication]
GO
