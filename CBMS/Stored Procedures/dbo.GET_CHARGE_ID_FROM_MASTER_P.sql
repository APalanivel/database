SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.GET_CHARGE_ID_FROM_MASTER_P
	@charge_master_id INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT charge_id 
	FROM dbo.CHARGE 
	WHERE charge_master_id = @charge_master_id

END
GO
GRANT EXECUTE ON  [dbo].[GET_CHARGE_ID_FROM_MASTER_P] TO [CBMSApplication]
GO
