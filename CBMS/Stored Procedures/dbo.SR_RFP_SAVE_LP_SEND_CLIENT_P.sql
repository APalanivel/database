SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.SR_RFP_SAVE_LP_SEND_CLIENT_P
	@user_id varchar(10),
	@session_id varchar(20),
	@rfp_id int,
	@site_id int,
	@lp_xml text,
	@email_log_id int
	AS
	set nocount on
	declare @sr_rfp_site_lp_xml_id int

	insert into sr_rfp_site_lp_xml values(@rfp_id, @site_id, @lp_xml)
	select @sr_rfp_site_lp_xml_id = @@identity

	insert into sr_rfp_lp_send_client
		select 	summary.sr_account_group_id,
	       		summary.is_bid_group,
	       		@email_log_id,
	       		@sr_rfp_site_lp_xml_id
		from 	sr_rfp_lp_account_summary summary(nolock),
			sr_rfp_account rfp_account(nolock),
			account acct(nolock)
		where 	acct.site_id =  @site_id
			and rfp_account.sr_rfp_id = @rfp_id
			and rfp_account.account_id = acct.account_id
			and rfp_account.is_deleted = 0
			and summary.sr_account_group_id = rfp_account.sr_rfp_account_id
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_SAVE_LP_SEND_CLIENT_P] TO [CBMSApplication]
GO
