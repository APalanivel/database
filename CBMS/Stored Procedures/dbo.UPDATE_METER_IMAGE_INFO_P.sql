SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE dbo.UPDATE_METER_IMAGE_INFO_P
	@desc varchar(500),
	@meterID int,
	@cbmsImageID int
	AS
	begin
		set nocount on

		UPDATE METER_CBMS_IMAGE_MAP SET description = @desc 
		WHERE METER_ID = @meterID  AND CBMS_IMAGE_ID = @cbmsImageID	
	end
GO
GRANT EXECUTE ON  [dbo].[UPDATE_METER_IMAGE_INFO_P] TO [CBMSApplication]
GO
