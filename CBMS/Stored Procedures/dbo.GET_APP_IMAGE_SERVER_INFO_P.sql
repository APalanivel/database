
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
NAME:  
	dbo.SiteGroup_Sel_By_Client_Id  

DESCRIPTION:    
	Procedure used to fetch the Sitegroup details for the given client id .  
	
INPUT PARAMETERS:  
Name				DataType	Default Description  
------------------------------------------------------------  
@hostAddress		VARCHAR(200)
@hostName			VARCHAR(200)
OUTPUT PARAMETERS:  
Name				DataType	Default Description 
------------------------------------------------------------  
@App_Image_Server	VARCHAR(255)
@Xid				VARCHAR(50)

USAGE EXAMPLES:  
------------------------------------------------------------  

DECLARE @Img_Server VARCHAR(255), @O_Xid VARCHAR(50)

EXEC dbo.GET_APP_IMAGE_SERVER_INFO_P
       @hostAddress = '10.1.1.63'
     ,@hostName = '01summit-ias'
     ,@App_Image_Server = @Img_Server OUT
     ,@Xid = @O_Xid OUT
     
     SELECT @Img_SErver, @O_Xid

SELECT * FROM App_Profile aprof
            INNER JOIN dbo.APP_SERVER aserv
                  ON aserv.APP_PROFILE_ID = aprof.APP_PROFILE_ID

AUTHOR INITIALS:
Initials		Name
------------------------------------------------------------
HG				Harihara Suthan Ganeshan

MODIFICATIONS:
Initials		Date		Modification
------------------------------------------------------------
HG				2016-06-03  Created
******/
CREATE PROCEDURE [dbo].[GET_APP_IMAGE_SERVER_INFO_P]
      @hostAddress VARCHAR(200)
     ,@hostName VARCHAR(200)
     ,@App_Image_Server VARCHAR(255) OUT
     ,@Xid VARCHAR(50) OUT
AS
BEGIN

      SET NOCOUNT ON;
	
      SELECT
            @App_Image_Server = aprof.App_Image_server
      FROM
            dbo.APP_PROFILE aprof
            INNER JOIN dbo.APP_SERVER aserv
                  ON aserv.APP_PROFILE_ID = aprof.APP_PROFILE_ID
      WHERE
            aserv.APP_SERVER_IP = @hostAddress
            AND aserv.APP_SERVER_NAME = @hostName;

      SELECT
            @Xid = App_Config_Value
      FROM
            dbo.App_Config (NOLOCK)
      WHERE
            App_Config_Cd = 'Xid'
            AND Is_Active = 1;

      RETURN;	
 
END;
;
GO

GRANT EXECUTE ON  [dbo].[GET_APP_IMAGE_SERVER_INFO_P] TO [CBMSApplication]
GO
