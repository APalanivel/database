SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.Account_Invoice_Collection_Primary_Source_Sel_Invoice_Collection_Account_Config_Id            
                        
 DESCRIPTION:                        
			To get the details of invoice collection ACCOUNT level config                  
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@Invoice_Collection_Account_Config_Id	INT
                              
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 
 
  EXEC [dbo].[Account_Invoice_Collection_Primary_Source_Sel_Invoice_Collection_Account_Config_Id] 
      @Invoice_Collection_Account_Config_Id = '33063'
      
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 RKV					Ravi kumar vegesna 
 NR					    Narayana Reddy  
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 RKV					2019-08-16     Created.     
 NR						2020-06-08	   SE2017-963 BOT Process - Retuen Enroll_In_Bot flag from new table.            
                       
******/

CREATE PROCEDURE [dbo].[Account_Invoice_Collection_Primary_Source_Sel_Invoice_Collection_Account_Config_Id]
    (
        @Invoice_Collection_Account_Config_Id NVARCHAR(MAX)
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            aic.Account_Invoice_Collection_Source_Id
            , aic.Invoice_Collection_Account_Config_Id
            , aic.Invoice_Collection_Source_Cd
            , aic.Invoice_Source_Type_Cd
            , aic.Invoice_Source_Method_of_Contact_Cd
            , aic.Is_Primary
            , aic.Collection_Instruction
            , ub.UBM_Id
            , ub.UBM_Type_Cd
            , COALESCE(dt.URL, aicdfefsd.URL) URL
            , COALESCE(dt.Login_Name, aicdfefsd.Login_Name) Login_Name
            , COALESCE(dt.Passcode, aicdfefsd.Passcode) Passcode
            , COALESCE(dt.Instruction, aicdfefsd.Website_Instructions) Instruction
            , COALESCE(dt.Instruction_Document_Image_Id, aicdfefsd.Instruction_Document_Image_Id) Instruction_Document_Image_Id
            , ci.CBMS_DOC_ID
            , aicdfefsd.File_Source_Cd
            , aicdfefsd.Data_Feed_Type_Cd
            , aicdfefsd.Vendor_Id File_Vendor_Id
            , aicdfefsd.Generic_Key
            , aicdfeisd.Image_Source_Cd
            , aicdfeisd.Vendor_Id Image_Vendor_Id
            , aicdfeisd.URL Image_Url
            , aicdfeisd.Login_Name Image_Login_Name
            , aicdfeisd.Passcode Image_Passcode
            , aicdfeisd.Image_Load_Location
            , ibpicacm.Enroll_In_Bot
        FROM
            dbo.Account_Invoice_Collection_Source aic
            LEFT JOIN dbo.Account_Invoice_Collection_Online_Source_Dtl dt
                ON aic.Account_Invoice_Collection_Source_Id = dt.Account_Invoice_Collection_Source_Id
            LEFT JOIN dbo.Account_Invoice_Collection_UBM_Source_Dtl ub
                ON aic.Account_Invoice_Collection_Source_Id = ub.Account_Invoice_Collection_Source_Id
            LEFT OUTER JOIN dbo.Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl AS aicdfefsd
                ON aicdfefsd.Account_Invoice_Collection_Source_Id = aic.Account_Invoice_Collection_Source_Id
            LEFT OUTER JOIN dbo.Account_Invoice_Collection_Data_Feed_Etl_Image_Source_Dtl AS aicdfeisd
                ON aicdfeisd.Account_Invoice_Collection_Source_Id = aic.Account_Invoice_Collection_Source_Id
            LEFT JOIN dbo.cbms_image ci
                ON ci.CBMS_IMAGE_ID = COALESCE(
                                          dt.Instruction_Document_Image_Id, aicdfefsd.Instruction_Document_Image_Id)
            INNER JOIN dbo.ufn_split(@Invoice_Collection_Account_Config_Id, ',') us
                ON us.Segments = aic.Invoice_Collection_Account_Config_Id
            LEFT JOIN dbo.IC_Bots_Process_Invoice_Collection_Account_Config_Map ibpicacm
                ON ibpicacm.Invoice_Collection_Account_Config_Id = aic.Invoice_Collection_Account_Config_Id
        WHERE
            aic.Is_Primary = 1;


    END;




GO
GRANT EXECUTE ON  [dbo].[Account_Invoice_Collection_Primary_Source_Sel_Invoice_Collection_Account_Config_Id] TO [CBMSApplication]
GO
