SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




--select * from budget_detail_comments_owner_map where budget_id = 1452
--exec BUDGET_REPORT_SAVE_INTERNAL_COMMENTS_P '-1',1452,2596,'Site','Comments for site not comming'

CREATE    PROCEDURE dbo.BUDGET_REPORT_SAVE_INTERNAL_COMMENTS_P
	@user_id varchar(10),
	@budget_id int,
	@comments_key int,
	@owner_type varchar(30),
	@comments varchar(2000)
	AS
	begin
		set nocount on

		declare @budget_account_id int, @owner_type_id int
		
		if @owner_type = 'Division'
		begin
			select @budget_account_id = min(budget_account.budget_account_id)
			from
			division d 
			join site s on d.division_id = s.division_id
			and d.division_id = @comments_key
			join account utilacc on s.site_id = utilacc.site_id
			join budget_account on utilacc.account_id = budget_account.account_id
			and budget_account.budget_id = @budget_id
			set @owner_type_id = 701 --//Division owner type

		end
		else if @owner_type = 'Site'
		begin
			select @budget_account_id = min(budget_account.budget_account_id)
			from
			site s 
			join account utilacc on s.site_id = utilacc.site_id
			and s.site_id = @comments_key
			join budget_account on utilacc.account_id = budget_account.account_id
			and budget_account.budget_id = @budget_id
			set @owner_type_id = 702 --//Site owner type
		end
		else -- 'Account'
		begin
			select @budget_account_id = @comments_key
			set @owner_type_id = 1309 --//Account owner type
		end

		if @comments = '' or @comments is null
		begin
		delete from budget_detail_comments_owner_map where budget_account_id = @budget_account_id and owner_type_id = @owner_type_id 
		end
		else
		begin
		if 	(select count(budget_detail_comments_owner_map_id) 
			from budget_detail_comments_owner_map where budget_account_id = @budget_account_id and owner_type_id = @owner_type_id) > 0
		begin
				update budget_detail_comments_owner_map set budget_detail_comments = @comments
				where budget_account_id = @budget_account_id and owner_type_id = @owner_type_id
		end else
		begin
			insert into budget_detail_comments_owner_map (budget_detail_comments, budget_id, owner_id, owner_type_id, budget_account_id)
			values(@comments, @budget_id, @user_id, @owner_type_id, @budget_account_id)
		end
		end
		

	end






GO
GRANT EXECUTE ON  [dbo].[BUDGET_REPORT_SAVE_INTERNAL_COMMENTS_P] TO [CBMSApplication]
GO
