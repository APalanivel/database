SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_DEAL_TICKET_COMMENTS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@dealTicketId  	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE   PROCEDURE DBO.GET_DEAL_TICKET_COMMENTS_P

@userId varchar,
@sessionId varchar,
@dealTicketId int

AS
set nocount on
	SELECT 
	rmdtc.RM_DEAL_TICKET_COMMENTS_ID, 
	ui.USERNAME, 
	rmdtc.CREATION_DATE, 
	rmdtc.COMMENTS
FROM 
	RM_DEAL_TICKET_COMMENTS rmdtc, 
	USER_INFO ui
WHERE 
	rmdtc.RM_DEAL_TICKET_ID = @dealTicketId AND
	rmdtc.USER_INFO_ID = ui.USER_INFO_ID
GO
GRANT EXECUTE ON  [dbo].[GET_DEAL_TICKET_COMMENTS_P] TO [CBMSApplication]
GO
