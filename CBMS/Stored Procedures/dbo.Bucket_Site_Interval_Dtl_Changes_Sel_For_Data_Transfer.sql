SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                
                      
NAME: [DBO].[Bucket_Site_Interval_Dtl_Changes_Sel_For_Data_Transfer]                      
                      
DESCRIPTION:                       
      Select the changed Site Level data from Bucket_Site_Interval_Dtl table(using change tracking) to transfer            
      it to HUB                     
                            
INPUT PARAMETERS:                                
NAME		DATATYPE DEFAULT  DESCRIPTION                                
------------------------------------------------------------                                
@Min_CT_Ver BIGINT                
@Max_CT_Ver BIGINT                   
                          
OUTPUT PARAMETERS:                                
NAME   DATATYPE DEFAULT  DESCRIPTION                         
                             
------------------------------------------------------------                                
USAGE EXAMPLES:                                
------------------------------------------------------------                      
  
 DECLARE
      @Min_CT_Ver BIGINT
     ,@Max_CT_Ver BIGINT    
 SET @Min_CT_Ver = CHANGE_TRACKING_MIN_VALID_VERSION(object_id('dbo.Bucket_Site_Interval_Dtl'));  
 SELECT
      @Max_CT_Ver = CHANGE_TRACKING_CURRENT_VERSION()            
 EXEC dbo.Bucket_Site_Interval_Dtl_Changes_Sel_For_Data_Transfer 
      @Min_CT_Ver
     ,@Max_CT_Ver            
 EXEC dbo.Bucket_Site_Interval_Dtl_Changes_Sel_For_Data_Transfer1
      @Min_CT_Ver
     ,@Max_CT_Ver 

            
AUTHOR INITIALS:                                
INITIALS	NAME                                
------------------------------------------------------------                                
HG			Harihara Suthan G              
RKV			Ravi Kumar Vegesna  

MODIFICATIONS
INITIALS	DATE(YYYY-MM-DD)	MODIFICATION      
------------------------------------------------------------
RKV			2013-04-24			Created
HG			2013-07-01			Table name changed to Bucket_Site_Interval_Dtl in  NOT EXIST clause to exclude the manual deleted records if we have entery for the same records with other manual data source code
RKV         2018-09-26			MAINT-7780 Added NOT NULL filters for I and U Sys_Change_Operation
*/
CREATE PROCEDURE [dbo].[Bucket_Site_Interval_Dtl_Changes_Sel_For_Data_Transfer]
     (
         @Min_CT_Ver BIGINT
         , @Max_CT_Ver BIGINT
     )
AS
    BEGIN

        SET NOCOUNT ON;

        /*****************************************************************************************
   This is here entirely as a work around for SSIS and temporary tables.  This statement
   allows SSIS to be able to return the meta data about the stored procedure to map columns.
*****************************************************************************************/
        IF 1 = 2
            BEGIN

                SELECT
                    CAST(NULL AS INT) AS Client_Hier_Id
                    , CAST(NULL AS INT) AS Bucket_Master_Id
                    , CAST(NULL AS INT) AS Data_Source_Cd
                    , CAST(NULL AS DATE) AS Service_Start_Dt
                    , CAST(NULL AS DATE) AS Service_End_Dt
                    , CAST(NULL AS DECIMAL(28, 10)) AS Bucket_Daily_Avg_Value
                    , CAST(NULL AS INT) AS Uom_Type_Id
                    , CAST(NULL AS INT) AS Currency_Unit_Id
                    , CAST(NULL AS INT) AS Created_User_Id
                    , CAST(NULL AS INT) AS Updated_User_Id
                    , CAST(NULL AS DATETIME) AS Created_Ts
                    , CAST(NULL AS DATETIME) AS Last_Changed_Ts
                    , CAST(NULL AS CHAR(1)) AS Sys_Change_Operation;


            END;


        DECLARE
            @CBMS_Data_Source_Cd INT
            , @Invoice_Data_Source_Cd INT
            , @DE_Data_Source_Cd INT;

        SELECT
            @Invoice_Data_Source_Cd = MAX(CASE WHEN cd.Code_Value = 'Invoice' THEN cd.Code_Id
                                          END)
            , @CBMS_Data_Source_Cd = MAX(CASE WHEN cd.Code_Value = 'CBMS' THEN cd.Code_Id
                                         END)
            , @DE_Data_Source_Cd = MAX(CASE WHEN cd.Code_Value = 'DE' THEN cd.Code_Id
                                       END)
        FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code cd
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Std_Column_Name = 'Data_Source_Cd';

        CREATE TABLE #Bucket_Site_Interval_Dtl_Ct_Data
             (
                 Client_Hier_Id INT
                 , Bucket_Master_Id INT
                 , Data_Source_Cd INT
                 , Service_Start_Dt DATE
                 , Service_End_Dt DATE
                 , Sys_Change_Operation NCHAR(1)
             );

        INSERT INTO #Bucket_Site_Interval_Dtl_Ct_Data
             (
                 Client_Hier_Id
                 , Bucket_Master_Id
                 , Data_Source_Cd
                 , Service_Start_Dt
                 , Service_End_Dt
                 , Sys_Change_Operation
             )
        SELECT
            ct.Client_Hier_Id
            , ct.Bucket_Master_Id
            , ct.Data_Source_Cd
            , ct.Service_Start_Dt
            , ct.Service_End_Dt
            , CONVERT(CHAR(1), ct.SYS_CHANGE_OPERATION)
        FROM
            CHANGETABLE(CHANGES dbo.Bucket_Site_Interval_Dtl, @Min_CT_Ver) ct
        WHERE
            ct.SYS_CHANGE_VERSION <= @Max_CT_Ver;




        SELECT
            ct_data.Client_Hier_Id
            , ct_data.Bucket_Master_Id
            , ct_data.Data_Source_Cd
            , ct_data.Service_Start_Dt
            , ct_data.Service_End_Dt
            , bsid.Bucket_Daily_Avg_Value
            , bsid.Uom_Type_Id
            , bsid.Currency_Unit_Id
            , bsid.Created_User_Id
            , bsid.Updated_User_Id
            , bsid.Created_Ts
            , bsid.Last_Changed_Ts
            , ct_data.Sys_Change_Operation
        FROM
            #Bucket_Site_Interval_Dtl_Ct_Data ct_data
            LEFT OUTER JOIN dbo.Bucket_Site_Interval_Dtl bsid
                ON bsid.Client_Hier_Id = ct_data.Client_Hier_Id
                   AND  bsid.Bucket_Master_Id = ct_data.Bucket_Master_Id
                   AND  bsid.Data_Source_Cd = ct_data.Data_Source_Cd
                   AND  bsid.Service_End_Dt = ct_data.Service_End_Dt
                   AND  bsid.Service_Start_Dt = ct_data.Service_Start_Dt
        WHERE
            (   (   ct_data.Sys_Change_Operation IN ( 'I', 'U' )
                    AND ct_data.Data_Source_Cd IN ( @CBMS_Data_Source_Cd, @Invoice_Data_Source_Cd )
                    AND bsid.Bucket_Daily_Avg_Value IS NOT NULL
                    AND bsid.Created_User_Id IS NOT NULL
                    AND bsid.Updated_User_Id IS NOT NULL
                    AND bsid.Last_Changed_Ts IS NOT NULL
                    AND bsid.Created_Ts IS NOT NULL
                    AND bsid.Data_Source_Cd IS NOT NULL) -- Records inserted/updated by CBMS/Invoice source
                OR  (   ct_data.Sys_Change_Operation = 'D' -- All the deleted invoice sources 
                        AND ct_data.Data_Source_Cd = @Invoice_Data_Source_Cd)
                OR  (   ct_data.Sys_Change_Operation = 'D' -- Deleted manual records only if it is not present BAID
                        AND ct_data.Data_Source_Cd IN ( @CBMS_Data_Source_Cd, @DE_Data_Source_Cd )
                        AND NOT EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.Bucket_Site_Interval_Dtl bsid1
                                           WHERE
                                                bsid1.Client_Hier_Id = ct_data.Client_Hier_Id
                                                AND bsid1.Bucket_Master_Id = ct_data.Bucket_Master_Id
                                                AND bsid1.Data_Source_Cd IN ( @CBMS_Data_Source_Cd, @DE_Data_Source_Cd )
                                                AND bsid1.Service_Start_Dt = ct_data.Service_Start_Dt
                                                AND bsid1.Service_End_Dt = ct_data.Service_End_Dt)));

        DROP TABLE #Bucket_Site_Interval_Dtl_Ct_Data;

    END;
    ;

GO
GRANT EXECUTE ON  [dbo].[Bucket_Site_Interval_Dtl_Changes_Sel_For_Data_Transfer] TO [ETL_Execute]
GO
