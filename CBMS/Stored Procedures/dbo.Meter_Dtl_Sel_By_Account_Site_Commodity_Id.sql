SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 dbo.Meter_Dtl_Sel_By_Account_Site_Commodity_Id 
  
DESCRIPTION:  
 It is used to get Utility/Supplier account meter informaiton by given account and site.  
  
INPUT PARAMETERS:  
 Name					DataType			Default					Description  
----------------------------------------------------------------------------------------  
 @account_id			INT   
 @site_id				INT					NULL  
  
OUTPUT PARAMETERS:  
 Name					DataType			Default					Description  
----------------------------------------------------------------------------------------  
  
USAGE EXAMPLES:  
---------------------------------------------------------------------------------------- 

EXEC Meter_Dtl_Sel_By_Account_Site_Commodity_Id 1741124,502503,290

 
AUTHOR INITIALS:  
 Initials		Name  
----------------------------------------------------------------------------------------  
 NR				Narayana Reddy
 TRK			Ramakrishna Thummala   
MODIFICATIONS  
 Initials			Date				Modification  
----------------------------------------------------------------------------------------  
  NR				2019-10-03			Created For Add Contract.  
  TRK				2020-02-17			Added the Is_Consolidated_Billing column in select List.
******/
CREATE PROCEDURE [dbo].[Meter_Dtl_Sel_By_Account_Site_Commodity_Id]
     (
         @Account_Id INT
         , @Site_Id INT = NULL
         , @Commodity_Id INT = NULL
     )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Supplier_Account_Type_Id INT
            , @Utility_Account_Type_Id INT;

        SELECT
            @Supplier_Account_Type_Id = MAX(CASE WHEN at.ENTITY_NAME = 'Supplier' THEN at.ENTITY_ID
                                            END)
            , @Utility_Account_Type_Id = MAX(CASE WHEN at.ENTITY_NAME = 'Utility' THEN at.ENTITY_ID
                                             END)
        FROM
            dbo.ENTITY at
        WHERE
            at.ENTITY_NAME IN ( 'Utility', 'Supplier' )
            AND at.ENTITY_DESCRIPTION = 'Account Type';

       
	    SELECT
            cha.Meter_Id
            , CASE WHEN cha.Supplier_Contract_ID IS NULL THEN cha.Rate_Id
              END AS Rate_Id
            , CASE WHEN cha.Supplier_Contract_ID IS NULL THEN cha.Rate_Name
              END AS Rate_Name
            , cha.Commodity_Id AS Commodity_Type_Id
            , com.Commodity_Name AS Commodity_Type
            , cha.Account_Vendor_Id AS Utility_Id
            , CASE WHEN cha.Account_Type = 'Utility' THEN cha.Account_Vendor_Id
                  ELSE ucha.Account_Vendor_Id
              END AS Supplier_Id
            , cha.Account_Vendor_Name AS Utility_Name
            , CASE WHEN cha.Account_Type = 'Utility' THEN cha.Account_Vendor_Name
                  ELSE ucha.Account_Vendor_Name
              END AS Supplier_Name
            , cha.Account_Id AS Utility_Account_Id
            , cha.Account_Number AS Utility_Account_Number
            , cha.Meter_Address_ID AS Address_Id
            , cha.Meter_Address_Line_1 AS Address_Line1
            , cha.Meter_Address_Line_2 AS Address_Line2
            , ch.City
            , ch.Client_Hier_Id
            , ch.State_Id
            , ch.State_Name
            , cha.Meter_Number
            , ch.Site_Id
            , ch.Site_name
            , cha.Account_Type AS Entity_Name
            , (CASE WHEN cha.Account_Type = 'Supplier' THEN @Supplier_Account_Type_Id
                   WHEN cha.Account_Type = 'Utility' THEN @Utility_Account_Type_Id
               END) AS Account_Type_Id
            , cha.Rate_Id AS Supplier_RateId
            , cha.Rate_Name AS Supplier_RateName
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS SiteView_Name
            , CASE WHEN cha.Account_Type = 'SUPPLIER' THEN ucha.Display_Account_Number
                  ELSE cha.Display_Account_Number
              END AS Supp_Utility_Account_Number
            , ch.Client_Id
            , m.ACCOUNT_ID meter_account_id
			, CASE WHEN acb.Account_Id IS NOT NULL
                                   THEN 1
                             ELSE  0
                        END AS Is_Consolidated_Billing
        FROM
            Core.Client_Hier ch
            JOIN Core.Client_Hier_Account cha
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            LEFT JOIN(Core.Client_Hier_Account scha
                      JOIN Core.Client_Hier_Account ucha
                          ON ucha.Account_Id = scha.Account_Id)
                ON scha.Meter_Id = cha.Meter_Id
                   AND  cha.Account_Type = 'Supplier'
                   AND  ucha.Account_Type = 'Utility'
            LEFT JOIN dbo.METER m
                ON cha.Meter_Id = m.METER_ID
            JOIN dbo.Commodity com
                ON com.Commodity_Id = cha.Commodity_Id
			LEFT OUTER JOIN
                        dbo.Account_Consolidated_Billing_Vendor acb
                              ON acb.Account_Id = cha.Account_Id
        WHERE
            ch.Site_Not_Managed = 0
            AND cha.Account_Id = @Account_Id
            AND (   @Site_Id IS NULL
                    OR  ch.Site_Id = @Site_Id)
            AND (   @Commodity_Id IS NULL
                    OR  cha.Commodity_Id = @Commodity_Id)
        GROUP BY
            cha.Meter_Id
            , CASE WHEN cha.Supplier_Contract_ID IS NULL THEN cha.Rate_Id
              END
            , CASE WHEN cha.Supplier_Contract_ID IS NULL THEN cha.Rate_Name
              END
            , cha.Commodity_Id
            , com.Commodity_Name
            , cha.Account_Vendor_Id
            , CASE WHEN cha.Account_Type = 'Utility' THEN cha.Account_Vendor_Id
                  ELSE ucha.Account_Vendor_Id
              END
            , cha.Account_Vendor_Name
            , CASE WHEN cha.Account_Type = 'Utility' THEN cha.Account_Vendor_Name
                  ELSE ucha.Account_Vendor_Name
              END
            , cha.Account_Id
            , cha.Account_Number
            , cha.Meter_Address_ID
            , cha.Meter_Address_Line_1
            , cha.Meter_Address_Line_2
            , ch.City
            , ch.Client_Hier_Id
            , ch.State_Id
            , ch.State_Name
            , cha.Meter_Number
            , ch.Site_Id
            , ch.Site_name
            , cha.Account_Type
            , (CASE WHEN cha.Account_Type = 'Supplier' THEN @Supplier_Account_Type_Id
                   WHEN cha.Account_Type = 'Utility' THEN @Utility_Account_Type_Id
               END)
            , cha.Rate_Id
            , cha.Rate_Name
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
            , CASE WHEN cha.Account_Type = 'SUPPLIER' THEN ucha.Display_Account_Number
                  ELSE cha.Display_Account_Number
              END
            , ch.Client_Id
            , m.ACCOUNT_ID
			, acb.Account_Id;
    END;
    ;


GO
GRANT EXECUTE ON  [dbo].[Meter_Dtl_Sel_By_Account_Site_Commodity_Id] TO [CBMSApplication]
GO
