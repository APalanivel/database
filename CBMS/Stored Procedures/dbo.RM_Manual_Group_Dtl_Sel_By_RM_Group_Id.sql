SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
                         
 NAME: dbo.RM_Manual_Group_Dtl_Sel_By_RM_Group_Id                     
                          
 DESCRIPTION:      
		  To get the group of site Risk Management Group details

 INPUT PARAMETERS:      
                         
 Name                               DataType          Default       Description      
-------------------------------------------------------------------------------------
 @Client_Id							INT        
                          
 OUTPUT PARAMETERS:      
                               
 Name                               DataType          Default       Description      
-------------------------------------------------------------------------------------
                          
 USAGE EXAMPLES:                              
-------------------------------------------------------------------------------------
          
		  EXEC  dbo.RM_Manual_Group_Dtl_Sel_By_RM_Group_Id  339--,1,4
		  EXEC  dbo.RM_Manual_Group_Dtl_Sel_By_RM_Group_Id  383--,1,2

		  EXEC  dbo.RM_Manual_Group_Dtl_Sel_By_RM_Group_Id  340
		  EXEC  dbo.RM_Manual_Group_Dtl_Sel_By_RM_Group_Id  341

		  EXEC dbo.RM_Group_Sel_By_Client_Id 218


                         
 AUTHOR INITIALS:    
       
	Initials	Name      
-------------------------------------------------------------------------------------
	RR          Raghu Reddy                            
                           
 MODIFICATIONS:    
                           
	Initials    Date            Modification    
-------------------------------------------------------------------------------------
	RR          2018-08-09      Global risk management - Created                        
                         
******/



CREATE PROCEDURE [dbo].[RM_Manual_Group_Dtl_Sel_By_RM_Group_Id]
      ( 
       @RM_Group_Id INT
      ,@Start_Index INT = 1
      ,@End_Index INT = 2147483647 )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Group_Dtl_Tbl AS TABLE
            ( 
             RM_GROUP_ID INT
            ,GROUP_NAME VARCHAR(100)
            ,RM_Group_Type_Cd INT
            ,RM_Group_Type VARCHAR(25)
            ,RM_Group_Participant_Type_Cd INT
            ,RM_Group_Participant_Type VARCHAR(25)
            ,Participant_Id INT
            ,Participant VARCHAR(200)
            ,Participant_Type VARCHAR(25)
            ,Division_Name VARCHAR(200)
            ,Country_Name VARCHAR(200)
            ,Status VARCHAR(50)
            ,Sites_Count INT
            ,Row_Num INT IDENTITY(1, 1) )
            
      DECLARE
            @Total INT
           ,@Module_Cd INT
           
      SELECT
            @Module_Cd = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON cs.Codeset_Id = c.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Application Module'
            AND c.Code_Value = 'CBMSRiskManagement'
      
      
                        
      INSERT      INTO @Group_Dtl_Tbl
                  ( 
                   RM_GROUP_ID
                  ,GROUP_NAME
                  ,RM_Group_Type_Cd
                  ,RM_Group_Type
                  ,RM_Group_Participant_Type_Cd
                  ,RM_Group_Participant_Type
                  ,Participant_Id
                  ,Participant
                  ,Participant_Type
                  ,Division_Name
                  ,Country_Name
                  ,Status
                  ,Sites_Count )
                  SELECT
                        rg.Cbms_Sitegroup_Id AS RM_GROUP_ID
                       ,rg.GROUP_NAME
                       ,rg.Group_Type_Cd AS RM_Group_Type_Cd
                       ,grptyp.Code_Value AS RM_Group_Type
                       ,dtl.Group_Participant_Type_Cd AS RM_Group_Participant_Type_Cd
                       ,prtptyp.Code_Value AS RM_Group_Participant_Type
                       ,dtl.Group_Participant_Id AS Participant_Id
                       ,ch.Site_name AS Participant
                       ,hlcd.Code_Value AS Participant_Type
                       ,ch.Sitegroup_Name AS Division_Name
                       ,ch.Country_Name
                       ,CASE ch.Site_Not_Managed
                          WHEN 1 THEN 'Inactive'
                          WHEN 0 THEN 'Active'
                        END AS STATUS
                       ,NULL AS Sites_Count
                  FROM
                        dbo.Cbms_Sitegroup rg
                        INNER JOIN dbo.Cbms_Sitegroup_Participant dtl
                              ON rg.Cbms_Sitegroup_Id = dtl.Cbms_Sitegroup_Id
                        INNER JOIN dbo.Code grptyp
                              ON rg.Group_Type_Cd = grptyp.Code_Id
                        INNER JOIN dbo.Codeset grptypcs
                              ON grptyp.Codeset_Id = grptypcs.Codeset_Id
                        INNER JOIN dbo.Code prtptyp
                              ON dtl.Group_Participant_Type_Cd = prtptyp.Code_Id
                        INNER JOIN dbo.Codeset prtptypcs
                              ON prtptyp.Codeset_Id = prtptypcs.Codeset_Id
                        INNER JOIN Core.Client_Hier ch
                              ON ch.Site_Id = dtl.Group_Participant_Id
                        INNER JOIN dbo.Code hlcd
                              ON ch.Hier_level_Cd = hlcd.Code_Id
                  WHERE
                        rg.Cbms_Sitegroup_Id = @RM_Group_Id
                        AND grptypcs.Codeset_Name = 'Risk Management Groups'
                        AND grptyp.Code_Value = 'RM Manual Group'
                        AND prtptypcs.Codeset_Name = 'RM Group Participants'
                        AND prtptyp.Code_Value IN ( 'Site' )
                        AND ch.Site_Id > 0
                        
      SELECT
            @Total = MAX(Row_Num)
      FROM
            @Group_Dtl_Tbl
            
      SELECT
            RM_GROUP_ID
           ,GROUP_NAME
           ,RM_Group_Type_Cd
           ,RM_Group_Type
           ,RM_Group_Participant_Type_Cd
           ,RM_Group_Participant_Type
           ,Participant_Id
           ,Participant
           ,Participant_Type
           ,Division_Name
           ,Country_Name
           ,Status
           ,Sites_Count
           ,Row_Num
           ,@Total AS Total
      FROM
            @Group_Dtl_Tbl
      WHERE
            Row_Num BETWEEN @Start_Index AND @End_Index
                  


END;
GO
GRANT EXECUTE ON  [dbo].[RM_Manual_Group_Dtl_Sel_By_RM_Group_Id] TO [CBMSApplication]
GO
