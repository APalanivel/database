SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.DELTOOL_ADDRESS_UPDATEPRIMARY_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@addressId     	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

--exec DELTOOL_ADDRESS_UPDATEPRIMARY_P



CREATE   PROCEDURE DBO.DELTOOL_ADDRESS_UPDATEPRIMARY_P
	( @MyAccountId int
	, @addressId int
	)
AS
BEGIN
	declare @siteId int

	set nocount on

		select @siteId = address_parent_id
		from address
		where address_id = @addressId

	   	update address
		set is_primary_address = 0
		where address_parent_id = @siteid

		update address
		set is_primary_address = 1
		where address_id= @addressId

		update site
		set primary_address_id = @addressId
		where site_id = @siteId

--exec cbmsAddress_Get @MyAccountId, @siteId

END
GO
GRANT EXECUTE ON  [dbo].[DELTOOL_ADDRESS_UPDATEPRIMARY_P] TO [CBMSApplication]
GO
