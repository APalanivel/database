SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.ADD_HEDGE_P
	@currency_unit_id INT,
	@hedge_unit_type_id INT,
	@hedge_name VARCHAR(200),
	@deal_type_id INT,
	@hedge_transaction_date DATETIME,
	@transaction_by_type_id INT,
	@hedge_comments VARCHAR(4000),
	@contract_id INT
AS
BEGIN

	SET NOCOUNT ON

	INSERT INTO dbo.hedge(currency_unit_id,
		hedge_unit_type_id,
		hedge_name,
		deal_type_id,
		hedge_transaction_date,
		transaction_by_type_id,
		hedge_comments,
		contract_id
		)
	VALUES(@currency_unit_id,
		@hedge_unit_type_id,
		@hedge_name,
		@deal_type_id,
		@hedge_transaction_date,
		@transaction_by_type_id,
		@hedge_comments,
		@contract_id
		)
END
GO
GRANT EXECUTE ON  [dbo].[ADD_HEDGE_P] TO [CBMSApplication]
GO
