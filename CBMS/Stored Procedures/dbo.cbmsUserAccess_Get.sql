SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   Procedure dbo.cbmsUserAccess_Get
	( @MyAccountId int )
As
BEGIN
	set nocount on
	   select distinct pm.permission_name functionname
	     from user_info_group_info_map ugmap
	     join group_info_permission_info_map gpmap on gpmap.group_info_id = ugmap.group_info_id
	     join permission_info pm on pm.permission_info_id = gpmap.permission_info_id
	    where ugmap.user_info_id = @MyAccountId
	 order by pm.permission_name


END
GO
GRANT EXECUTE ON  [dbo].[cbmsUserAccess_Get] TO [CBMSApplication]
GO
