SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME: [dbo].[Report_DE_Get_InvoiceCollection_ContactsData]

DESCRIPTION:
		this procedure is used to get InvoiceCollection module Contacts data for all clients

INPUT PARAMETERS:
	NAME							DATATYPE	DEFAULT		DESCRIPTION
-----------------------------------------------------------------------

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION
-----------------------------------------------------------------------

USAGE EXAMPLES:
-----------------------------------------------------------------------
	exec Report_DE_Get_InvoiceCollection_ContactsData

AUTHOR INITIALS:
	INITIALS	NAME
-----------------------------------------------------------------------
	KVK			Vinay K

MODIFICATIONS:
	INITIALS	DATE			MODIFICATION
-----------------------------------------------------------------------
	KVK			09/10/2018		created, REPTMGR-94
*/

CREATE PROCEDURE [dbo].[Report_DE_Get_InvoiceCollection_ContactsData]
AS
BEGIN
    SET NOCOUNT ON;

    ;WITH Contacts_CTE
    AS (SELECT DISTINCT
            icaco.Invoice_Collection_Account_Config_Id,
            CAST(ci.First_Name AS VARCHAR(200)) + ' ' + CAST(ci.Last_Name AS VARCHAR(200)) + ' | '
            + ISNULL(ci.Email_Address, '') + ' | ' + ISNULL(ci.Phone_Number, '') Contact,
            cc.Code_Value [Contact Type],
            [Primary] = CASE
                            WHEN icaco.Is_Primary = 1 THEN
                                'Yes'
                            ELSE
                                'No'
                        END,
            vtp.ENTITY_NAME [Chased Vendor Type],
            v.VENDOR_NAME [Chased Vendor]
        FROM dbo.Invoice_Collection_Account_Contact icaco
            INNER JOIN dbo.Contact_Info ci
                ON ci.Contact_Info_Id = icaco.Contact_Info_Id
            INNER JOIN dbo.Code cc
                ON cc.Code_Id = ci.Contact_Level_Cd
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icac.Invoice_Collection_Account_Config_Id = icaco.Invoice_Collection_Account_Config_Id
            LEFT OUTER JOIN dbo.Vendor_Contact_Map vcm
                ON vcm.Contact_Info_Id = ci.Contact_Info_Id
            LEFT OUTER JOIN dbo.VENDOR v
                ON v.VENDOR_ID = vcm.VENDOR_ID
            LEFT OUTER JOIN dbo.ENTITY vtp
                ON vtp.ENTITY_ID = v.VENDOR_TYPE_ID
    --WHERE icaco.Is_Primary = 1--AND icaco.Invoice_Collection_Account_Config_Id = 21104
    )
    SELECT DISTINCT
        ch.Client_Name Client,
        ch.Site_name Site,
        ch.City City,
        ch.State_Name State,
        ch.Country_Name Country,
        cha.Display_Account_Number [Account Number],
        cha.Account_Id [Account ID],
        cha.Account_Vendor_Name [Account Vendor],
        cha.Account_Type [Account Type],
        cc.Contact [Contact name | email | phone#],
        cc.[Contact Type],
        [Primary] = cc.[Primary],
        cc.[Chased Vendor],
        cc.[Chased Vendor Type]
    FROM Core.Client_Hier_Account cha
        JOIN Core.Client_Hier ch
            ON ch.Client_Hier_Id = cha.Client_Hier_Id
        LEFT JOIN dbo.ENTITY cltyp
            ON cltyp.ENTITY_ID = ch.Client_Type_Id
        JOIN dbo.Invoice_Collection_Account_Config iac
            ON iac.Account_Id = cha.Account_Id
        LEFT OUTER JOIN Contacts_CTE cc
            ON cc.Invoice_Collection_Account_Config_Id = iac.Invoice_Collection_Account_Config_Id
    --AND cc.[Contact Type] = 'Account'
    --LEFT OUTER JOIN Contacts_CTE cc1 ON cc1.Invoice_Collection_Account_Config_Id = iac.Invoice_Collection_Account_Config_Id
    --AND cc1.[Contact Type] = 'Client'
    --LEFT OUTER JOIN Contacts_CTE cc2 ON cc2.Invoice_Collection_Account_Config_Id = iac.Invoice_Collection_Account_Config_Id
    --AND cc2.[Contact Type] = 'Vendor'
    WHERE cltyp.ENTITY_NAME != 'demo'
          AND ch.Client_Not_Managed = 0
          AND ch.Site_Not_Managed = 0
          AND cha.Account_Not_Managed = 0
    ORDER BY ch.Client_Name,
             ch.Site_name,
             cha.Display_Account_Number;
END;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Get_InvoiceCollection_ContactsData] TO [CBMSApplication]
GO
