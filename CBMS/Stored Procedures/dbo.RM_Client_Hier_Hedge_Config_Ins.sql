SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        dbo.RM_Client_Hier_Hedge_Config_Ins                      
                        
Description:                        
        To insert client hedge info and mapping with client  
                        
Input Parameters:                        
    Name        DataType        Default     Description                          
----------------------------------------------------------------------------------  
 @Client_Hier_Id      INT  
 @RM_Forecast_UOM_Type_Id   INT  
 @Config_Start_Dt     DATE  
 @Config_End_Dt      DATE  
 @Hedge_Type_Id      INT  
 @Max_Hedge_Pct      DECIMAL(28,12)  
 @RM_Risk_Tolerance_Category_Id  INT  
 @Risk_Manager_User_Info_Id   INT  
 @Contact_Info_Id     INT  
 @Include_In_Reports     BIT  
 @User_Info_Id      INT  
                        
 Output Parameters:                              
 Name            Datatype        Default  Description                              
------------------------------------------------------------------------------  
  
                      
Usage Examples:                            
------------------------------------------------------------------------------  
    
                       
 Author Initials:                        
    Initials    Name                        
------------------------------------------------------------------------------  
    PR          Pramod Reddy          
                         
 Modifications:                        
    Initials Date           Modification                        
------------------------------------------------------------------------------  
    PR   01-08-2018     Global Risk Management - Created                      
                       
******/
CREATE PROCEDURE [dbo].[RM_Client_Hier_Hedge_Config_Ins]
(
    @Client_Hier_Id INT,
    @Commodity_Id INT,
    @RM_Forecast_UOM_Type_Id INT,
    @Config_Start_Dt DATE,
    @Config_End_Dt DATE,
    @Hedge_Type_Id INT,
    @Max_Hedge_Pct DECIMAL(5, 2),
    @RM_Risk_Tolerance_Category_Id INT,
    @Risk_Manager_User_Info_Id INT,
    @Contact_Info_Id INT,
    @Include_In_Reports BIT,
    @User_Info_Id INT
)
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @RM_Client_Hier_Onboard_Id INT,
            @RM_Default_Config_Start_Dt DATE,
            @RM_Default_Config_End_Dt DATE;

    SELECT @RM_Default_Config_Start_Dt = CAST(App_Config_Value AS DATE)
    FROM dbo.App_Config
    WHERE App_Config_Cd = 'RM_Default_Config_Start_Dt'
          AND App_Id = -1;

    SELECT @RM_Default_Config_End_Dt = CAST(App_Config_Value AS DATE)
    FROM dbo.App_Config
    WHERE App_Config_Cd = 'RM_Default_Config_End_Dt'
          AND App_Id = -1;

    INSERT INTO Trade.RM_Client_Hier_Onboard
    (
        Client_Hier_Id,
        Country_Id,
        Commodity_Id,
        Created_User_Id,
        Created_Ts,
        Updated_User_Id,
        Last_Change_Ts,
        RM_Forecast_UOM_Type_Id
    )
    SELECT ch.Client_Hier_Id,
           ch.Country_Id,
           @Commodity_Id,
           @User_Info_Id,
           GETDATE(),
           @User_Info_Id,
           GETDATE(),
           @RM_Forecast_UOM_Type_Id
    FROM Core.Client_Hier ch
    WHERE ch.Client_Hier_Id = @Client_Hier_Id
          AND NOT EXISTS
    (
        SELECT 1
        FROM Trade.RM_Client_Hier_Onboard chob
        WHERE chob.Client_Hier_Id = ch.Client_Hier_Id
              AND chob.Commodity_Id = @Commodity_Id
    );

    SELECT @RM_Client_Hier_Onboard_Id = chob.RM_Client_Hier_Onboard_Id
    FROM Trade.RM_Client_Hier_Onboard chob
    WHERE chob.Client_Hier_Id = @Client_Hier_Id
          AND chob.Commodity_Id = @Commodity_Id;

    SELECT @Config_Start_Dt = DATEADD(dd, -DATEPART(dd, @Config_Start_Dt) + 1, @Config_Start_Dt);
    SELECT @Config_End_Dt = DATEADD(dd, -DATEPART(dd, @Config_End_Dt) + 1, @Config_End_Dt);

    INSERT INTO Trade.RM_Client_Hier_Hedge_Config
    (
        RM_Client_Hier_Onboard_Id,
        Config_Start_Dt,
        Config_End_Dt,
        --,RM_Forecast_UOM_Type_Id  
        Hedge_Type_Id,
        Max_Hedge_Pct,
        RM_Risk_Tolerance_Category_Id,
        Risk_Manager_User_Info_Id,
        Contact_Info_Id,
        Include_In_Reports,
        Is_Default_Config,
        Created_User_Id,
        Created_Ts,
        Last_Updated_By,
        Last_Updated_Ts
    )
    SELECT @RM_Client_Hier_Onboard_Id,
           ISNULL(@Config_Start_Dt, @RM_Default_Config_Start_Dt),
           ISNULL(@Config_End_Dt, @RM_Default_Config_End_Dt),
           --,@RM_Forecast_UOM_Type_Id  
           @Hedge_Type_Id,
           @Max_Hedge_Pct,
           @RM_Risk_Tolerance_Category_Id,
           @Risk_Manager_User_Info_Id,
           @Contact_Info_Id,
           @Include_In_Reports,
           0 AS Is_Default_Config,
           @User_Info_Id,
           GETDATE(),
           @User_Info_Id,
           GETDATE();



--EXEC dbo.RM_Forecast_Volume_Load   
--      @RM_Client_Hier_Onboard_Id = @RM_Client_Hier_Onboard_Id  




END;


GO
GRANT EXECUTE ON  [dbo].[RM_Client_Hier_Hedge_Config_Ins] TO [CBMSApplication]
GO
