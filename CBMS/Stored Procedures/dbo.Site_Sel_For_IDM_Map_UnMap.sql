
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [dbo].[Site_Sel_For_IDM_Map_UnMap]
     
DESCRIPTION: 
	
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@ClientId			INT
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        

    exec Site_Sel_For_IDM_Map_UnMap
	exec Site_Sel_For_IDM_Map_UnMap 11278

AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------
	KVK		Vinay K
	SS		Sani

MODIFICATIONS

INITIALS	DATE		MODIFICATION
------------------------------------------------------------          
	KVK		12/14/2014	created
	KVK		07/06/2016	modified to have weather station code in select list
	SS		2017/03/16	modified to have Time Zone in the select list
*/

CREATE PROCEDURE [dbo].[Site_Sel_For_IDM_Map_UnMap] ( @ClientId AS INT = NULL )
AS
BEGIN

      SET NOCOUNT ON;

      SELECT
            s.Client_ID AS [Client_Id]
           ,ch.Client_Hier_Id
           ,s.SITE_NAME AS [Site_Name]
           ,s.Is_IDM_Enabled
           ,s.IDM_Node_XId IDM_Node_Id
           ,s.Weather_Station_Code
		   ,ch.Time_Zone
      FROM
            dbo.SITE AS s
            JOIN Core.Client_Hier AS ch
                  ON ch.Site_Id = s.SITE_ID
      WHERE
            ( s.Client_ID = @ClientId
              OR @ClientId IS NULL )
            AND ( ( s.Is_IDM_Enabled = 1
                    AND s.IDM_Node_XId IS NULL )
                  OR ( s.Is_IDM_Enabled = 0
                       AND s.IDM_Node_XId > 0 ) );
   
END;

;
GO


GRANT EXECUTE ON  [dbo].[Site_Sel_For_IDM_Map_UnMap] TO [CBMSApplication]
GO
