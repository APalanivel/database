SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsCostUsageSiteAltFuel_GetMonthly

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@client_id     	int       	          	
	@division_id   	int       	null      	
	@site_id       	int       	null      	
	@alt_fuel_type_id	int       	null      	
	@unit_of_measure_type_id	int       	          	
	@currency_unit_id	int       	          	
	@report_year   	int       	          	
	@country_id    	int       	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE        procedure [dbo].[cbmsCostUsageSiteAltFuel_GetMonthly]
	( @MyAccountId int
	, @client_id int
	, @division_id int = null
	, @site_id int = null
	, @alt_fuel_type_id int = null
	, @unit_of_measure_type_id int
	, @currency_unit_id int
	, @report_year int
	, @country_id int = null
	)
AS
BEGIN

	set nocount on
	set ansi_warnings off

	  -- sproc driven
	  declare @session_uid uniqueidentifier
		, @begin_date datetime
		, @end_date datetime

	set @session_uid = newid()

	exec cbmsRpt_ServiceMonth_LoadForReportYear @MyAccountId, @session_uid, @report_year, @client_id 


	   select @begin_date = min(service_month)
		, @end_date = max(service_month)
	     from rpt_service_month
	    where session_uid = @session_uid

	set nocount off


	   select null cost_usage_site_alt_fuel_id
		, m.service_month

		, sum(x.total_cost) total_cost
		, sum(x.total_usage) total_usage

		, convert(decimal(32, 16), 0) unit_cost

		, min(x.cbms_image_id) cbms_image_id

	     from rpt_service_month m WITH (NOLOCK) 

  left outer join (
		   select a.cost_usage_site_alt_fuel_id
			, a.service_month
			, a.total_usage  * unitcon.conversion_factor 	total_usage
			, a.total_cost * cuc.conversion_factor 		total_cost
			, a.cbms_image_id cbms_image_id
		     from client c WITH (NOLOCK) 
		     join division d  WITH (NOLOCK) on d.client_id = c.client_id
		     join site s  WITH (NOLOCK) on s.division_id = d.division_id
		     join vwSiteName vws WITH (NOLOCK) on vws.site_id = s.site_id

		     join cost_usage_site_alt_fuel a WITH (NOLOCK) on a.site_id = s.site_id
		     join currency_unit_conversion cuc on (cuc.currency_group_id = c.currency_group_id and cuc.base_unit_id = a.currency_unit_id and cuc.conversion_date = a.service_month and cuc.converted_unit_id = @currency_unit_id)
		     join consumption_unit_conversion unitcon on (unitcon.base_unit_id = a.unit_of_measure_type_id and unitcon.converted_unit_id = @unit_of_measure_type_id)

		    where c.client_id 		= @client_id
		      and d.division_id 	= isNull(@division_id, d.division_id)
		      and s.site_id 		= isNull(@site_id, s.site_id)
		      and vws.country_id 	= isNull(@country_id, vws.country_id)
		      and a.alt_fuel_type_id 	= isNull(@alt_fuel_type_id, a.alt_fuel_type_id)		      and a.service_month between @begin_date and @end_date
		      and s.closed = 0
		      and s.not_managed = 0

		  ) x on x.service_month = m.service_month

	    where m.session_uid = @session_uid

	   group by m.service_month


	 order by m.service_month

	set nocount on
	exec cbmsRpt_ServiceMonth_RemoveForSession @MyAccountId, @session_uid
	set nocount off
	set ansi_warnings on


END
GO
GRANT EXECUTE ON  [dbo].[cbmsCostUsageSiteAltFuel_GetMonthly] TO [CBMSApplication]
GO
