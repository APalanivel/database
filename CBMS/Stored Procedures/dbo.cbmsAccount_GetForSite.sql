SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 dbo.cbmsAccount_GetForSite  
 DESCRIPTION:   
 
 INPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 @MyAccountId    int                     
 @site_id        int                     

 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  

 USAGE EXAMPLES:  
------------------------------------------------------------  
--exec cbmsAccount_GetForSite 10069, 16987 

exec cbmsAccount_GetForSite 49, 400017 

exec cbmsAccount_GetForSite 49, 400017
 AUTHOR INITIALS:  Initials Name  
------------------------------------------------------------  

 MODIFICATIONS   
 Initials		Date			Modification  
------------------------------------------------------------  
 9/22/2009						For adding Supplier Account start and end dates 
 DMR		   09/10/2010		Modified for Quoted_Identifier
 NR				2019-11-16		Add Contract -added Display account number.
 
******/
CREATE PROCEDURE [dbo].[cbmsAccount_GetForSite]
    (
        @MyAccountId INT
        , @site_id INT
    )
AS
    BEGIN

        SELECT
            cha.Account_Id
            , cha.Account_Number
            , cha.Display_Account_Number
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account cha
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
        WHERE
            ch.Site_Id = @site_id
        GROUP BY
            cha.Account_Id
            , cha.Account_Number
            , cha.Display_Account_Number
        ORDER BY
            cha.Account_Number;


    END;


GO
GRANT EXECUTE ON  [dbo].[cbmsAccount_GetForSite] TO [CBMSApplication]
GO
