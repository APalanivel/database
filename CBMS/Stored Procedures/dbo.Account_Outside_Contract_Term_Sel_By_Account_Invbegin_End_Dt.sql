SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********     
NAME:  dbo.Account_Outside_Contract_Term_Sel_By_Account_Invbegin_End_Dt    
   
DESCRIPTION:  Used to check supplier data for Out side contract term    
  
INPUT PARAMETERS:      
      Name              DataType          Default     Description      
-----------------------------------------------------------------------------------------------      
  @Account_Id			INT  
  @InvBeginDate			DATETIME  
  @InvEndDate			DATETIME     
      
OUTPUT PARAMETERS:      
      Name              DataType          Default     Description      
-----------------------------------------------------------------------------------------------      
      
USAGE EXAMPLES:    
   
 EXEC Account_Outside_Contract_Term_Sel_By_Account_Invbegin_End_Dt 35071,'2008-11-10','2009-12-01'  
 EXEC Account_Outside_Contract_Term_Sel_By_Account_Invbegin_End_Dt 105559,'2008-10-28','2010-10-31'  
 EXEC Account_Outside_Contract_Term_Sel_By_Account_Invbegin_End_Dt 135109,'2009-05-13','2011-04-30'  
   
-----------------------------------------------------------------------------------------------      
AUTHOR INITIALS:    
Initials				Modification    
-----------------------------------------------------------------------------------------------      
NR						 Narayana Reddy         
  
  
Initials		Date			Modification    
-----------------------------------------------------------------------------------------------      
NR				2020-01-21		MAINT - 9783 Created..  
HG				2020-04-02		Handling open ended supplier account dates.
NR				2020-06-10		SE2017-981 -Added Vendor details of current account & other account
******/
CREATE PROCEDURE [dbo].[Account_Outside_Contract_Term_Sel_By_Account_Invbegin_End_Dt]
    (
        @Account_Id INT
        , @InvBeginDate DATETIME
        , @InvEndDate DATETIME
        , @UseVendorId BIT = 0
    )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            suppacc2.ACCOUNT_ID
            , ISNULL(NULLIF(suppacc2.ACCOUNT_NUMBER, ''), 'Not Yet Assigned') AS ACCOUNT_NUMBER
            , suppacc1.VENDOR_ID AS Current_Account_Vendor_Id
            , suppacc2.VENDOR_ID AS Other_Account_Vendor_Id
        FROM
            dbo.SUPPLIER_ACCOUNT_METER_MAP samm1
            JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm2
                ON samm1.METER_ID = samm2.METER_ID
            LEFT OUTER JOIN dbo.CONTRACT con1
                ON samm1.Contract_ID = con1.CONTRACT_ID
            LEFT OUTER JOIN dbo.CONTRACT con2
                ON samm2.Contract_ID = con2.CONTRACT_ID
            INNER JOIN dbo.ACCOUNT suppacc1
                ON samm1.ACCOUNT_ID = suppacc1.ACCOUNT_ID
            INNER JOIN dbo.Supplier_Account_Config sac1
                ON sac1.Account_Id = suppacc1.ACCOUNT_ID
            INNER JOIN dbo.ACCOUNT suppacc2
                ON samm2.ACCOUNT_ID = suppacc2.ACCOUNT_ID
            INNER JOIN dbo.Supplier_Account_Config sac2
                ON sac2.Account_Id = suppacc2.ACCOUNT_ID
        WHERE
            samm1.ACCOUNT_ID = @Account_Id
            AND (   samm1.Contract_ID != samm2.Contract_ID
                    OR  (   samm1.Contract_ID = -1
                            AND samm2.Contract_ID = -1))
            AND samm1.ACCOUNT_ID != samm2.ACCOUNT_ID
            AND (   @UseVendorId = 0
                    OR  suppacc1.VENDOR_ID = suppacc2.VENDOR_ID)
            AND sac2.Supplier_Account_Begin_Dt <= @InvBeginDate
            AND ISNULL(sac2.Supplier_Account_End_Dt, '9999-12-31') >= @InvEndDate
        GROUP BY
            suppacc2.ACCOUNT_ID
            , ISNULL(NULLIF(suppacc2.ACCOUNT_NUMBER, ''), 'Not Yet Assigned')
            , suppacc1.VENDOR_ID
            , suppacc2.VENDOR_ID;

    END;

GO
GRANT EXECUTE ON  [dbo].[Account_Outside_Contract_Term_Sel_By_Account_Invbegin_End_Dt] TO [CBMSApplication]
GO
