SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          
NAME:   Change_Tracking_Current_Version_Sel       

    
DESCRIPTION:          
	select current DB Version id
      
INPUT PARAMETERS:          
Name				DataType		Default		Description          
------------------------------------------------------------          

                
OUTPUT PARAMETERS:          
Name			DataType	Default		Description          
------------------------------------------------------------ 

         
USAGE EXAMPLES:          
------------------------------------------------------------ 

EXEC Change_Tracking_Current_Version_Sel       
     
AUTHOR INITIALS:          
Initials	Name          
------------------------------------------------------------          
KVK			Vinay K
     
MODIFICATIONS           
Initials	Date	Modification          
------------------------------------------------------------          
	KVK		12/21/2014		created
*****/ 
CREATE PROCEDURE [dbo].[Change_Tracking_Current_Version_Sel]
AS
BEGIN
      SET NOCOUNT ON; 
      SELECT
            change_tracking_current_Version() CT_Current_Version_Id
END
;
GO
GRANT EXECUTE ON  [dbo].[Change_Tracking_Current_Version_Sel] TO [CBMSApplication]
GO
