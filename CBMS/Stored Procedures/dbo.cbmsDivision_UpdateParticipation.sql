SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE    PROCEDURE [dbo].[cbmsDivision_UpdateParticipation]
	( @user_info_id int
	, @division_id int
	, @not_managed bit = null
	)
AS
BEGIN

	set nocount on

	if @not_managed = 1
	begin

	  declare @current_managed bit

	   select @current_managed = isNull(not_managed, 0)
	     from division
	    where division_id = @division_id

		if @not_managed = 1 and @current_managed = 0
		begin

			exec cbmsInvoiceParticipationQueue_Save 
				  @user_info_id
				, 4 -- make division not managed
				, null
				, @division_id
				, null
				, null
				, null
				, 1


		end

	end

--	set nocount off

END
GO
GRANT EXECUTE ON  [dbo].[cbmsDivision_UpdateParticipation] TO [CBMSApplication]
GO
