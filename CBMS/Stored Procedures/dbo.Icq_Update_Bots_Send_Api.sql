SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
NAME:              
           
DESCRIPTION: GetVendorOnlineReadyToChaseRecordsList          
            
              
 INPUT PARAMETERS:              
 Name   DataType  Default Description              
------------------------------------------------------------              
           
 OUTPUT PARAMETERS:              
 Name   DataType  Default Description              
------------------------------------------------------------              
          
           
 USAGE EXAMPLES:              
------------------------------------------------------------              
    exec [dbo].[GetVendorOnlineReadyToChaseRecordsList]      
              
              
AUTHOR INITIALS:              
 Initials Name              
------------------------------------------------------------              
 RKV     Ravi Kumar Vegesna      
           
 MODIFICATIONS               
           
 Initials Date  Modification              
------------------------------------------------------------              
 RKV  2019-12-17 Created      
 RKV  2020-02-19  Status will be updated when the is_inovice_received flag is true      
          
******/  
  
CREATE PROCEDURE [dbo].[Icq_Update_Bots_Send_Api]  
     (  
         @Invoice_Collection_Queue_Id INT  
         , @Collection_Start_Dt DATE  
         , @Collection_End_Dt DATE  
         , @Received_Status_Updated_Dt DATE = NULL  
         , @Invoice_File_Name NVARCHAR(255) = NULL  
         , @Last_Change_Ts DATETIME = NULL  
         , @Download_Attempt BIT = 0  
         , @Next_Action_Dt DATE = NULL  
         , @Last_BOT_Attempt_Date DATE = NULL  
         , @User_Info_Id INT  
         , @Is_Invoice_Received BIT = 1  
     )  
AS  
    BEGIN  
  
        DECLARE  
            @tvp_Invoice_Collection_Queue tvp_Invoice_Collection_Queue  
            , @IC_Activity_Type_Cd INT  
            , @status_id INT  
            , @ICR_Queue_Type_Code INT;  
  
  
  
  
        SELECT  
            @ICR_Queue_Type_Code = c.Code_Id  
        FROM  
            dbo.Code c  
            INNER JOIN dbo.Codeset cs  
                ON cs.Codeset_Id = c.Codeset_Id  
        WHERE  
            c.Code_Value = 'ICR'  
            AND cs.Codeset_Name = 'Invoice Collection Type';  
  
  
        SELECT  
            @IC_Activity_Type_Cd = c.Code_Id  
        FROM  
            dbo.Code c  
            INNER JOIN dbo.Codeset cs  
                ON cs.Codeset_Id = c.Codeset_Id  
        WHERE  
            c.Code_Value = 'Attempt Download'  
            AND cs.Codeset_Name = 'IC Activity Type';  
  
        SELECT  
            @status_id = c.Code_Id  
        FROM  
            dbo.Code c  
            INNER JOIN dbo.Codeset cs  
                ON cs.Codeset_Id = c.Codeset_Id  
        WHERE  
            c.Code_Value = 'Received'  
            AND cs.Codeset_Name = 'ICR Status';  
  
        INSERT INTO @tvp_Invoice_Collection_Queue  
             (  
                 Invoice_Collection_Queue_Id  
             )  
        SELECT  @Invoice_Collection_Queue_Id;  
  
  
        IF @Download_Attempt = 1  
            BEGIN  
                EXEC Invoice_Collection_Activity_Log_Ins  
                    @tvp_Invoice_Collection_Queue  
                    , @IC_Activity_Type_Cd  
                    , NULL  
                    , @User_Info_Id;  
            END;  
  
        UPDATE  
            dbo.Invoice_Collection_Queue  
        SET  
            Status_Cd = @status_id  
            , Invoice_Collection_Queue_Type_Cd = @ICR_Queue_Type_Code  
        WHERE  
            Invoice_Collection_Queue_Id = @Invoice_Collection_Queue_Id  
            AND @Is_Invoice_Received = 1;  
  
        UPDATE  
            dbo.Invoice_Collection_Queue  
        SET  
            Collection_Start_Dt = @Collection_Start_Dt  
            , Collection_End_Dt = @Collection_End_Dt  
            , Received_Status_Updated_Dt = ISNULL(@Received_Status_Updated_Dt, Received_Status_Updated_Dt)  
            , Invoice_File_Name = ISNULL(@Invoice_File_Name, Invoice_File_Name)  
            , Last_Change_Ts =  GETDATE()
            , Next_Action_Dt = ISNULL(@Next_Action_Dt, Next_Action_Dt)  
            , Last_BOT_Attempt_Date = ISNULL(@Last_BOT_Attempt_Date, Last_BOT_Attempt_Date)  
            , Updated_User_Id = @User_Info_Id  
        WHERE  
            Invoice_Collection_Queue_Id = @Invoice_Collection_Queue_Id;  
  
    END;  
  
GO
GRANT EXECUTE ON  [dbo].[Icq_Update_Bots_Send_Api] TO [CBMSApplication]
GO
