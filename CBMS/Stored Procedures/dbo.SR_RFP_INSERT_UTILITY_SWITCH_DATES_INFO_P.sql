SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_INSERT_UTILITY_SWITCH_DATES_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@accountGroupId	int       	          	
	@isBidGroup    	bit       	          	
	@returnToTarrifDate	datetime  	          	
	@isSwitchRate  	bit       	          	
	@switchSupplierDate	datetime  	          	
	@isSwitchSupplier	bit       	          	
	@returnToTarrifTypeId	int       	          	
	@switchSupplierTypeId	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE     PROCEDURE DBO.SR_RFP_INSERT_UTILITY_SWITCH_DATES_INFO_P

@accountGroupId int,
@isBidGroup bit, 
@returnToTarrifDate datetime ,
@isSwitchRate bit ,
@switchSupplierDate datetime,
@isSwitchSupplier bit,
@returnToTarrifTypeId int,
@switchSupplierTypeId int



AS
set nocount on
INSERT INTO SR_RFP_UTILITY_SWITCH 
(SR_ACCOUNT_GROUP_ID,
IS_BID_GROUP,
RETURN_TO_TARIFF_DATE,
RETURN_TO_TARIFF_TYPE_ID,
IS_SWITCH_RATE_ESTIMATED,
UTILITY_SWITCH_SUPPLIER_DATE,
UTILITY_SWITCH_SUPPLIER_TYPE_ID,
IS_SWITCH_SUPPLIER_ESTIMATED
) 

VALUES ( @accountGroupId, 
@isBidGroup , 
@returnToTarrifDate,
@returnToTarrifTypeId,
@isSwitchRate,
@switchSupplierDate,
@switchSupplierTypeId,
@isSwitchSupplier
)

if(@isBidGroup = 0)
begin
	UPDATE SR_RFP_CHECKLIST set
	UTILITY_SWITCH_DEADLINE_DATE = @returnToTarrifDate,
	UTILITY_SWITCH_SUPPLIER_DATE = @switchSupplierDate
	WHERE SR_RFP_ACCOUNT_ID =  @accountGroupId 
end
else if(@isBidGroup > 0 )
begin
	UPDATE SR_RFP_CHECKLIST set
	UTILITY_SWITCH_DEADLINE_DATE = @returnToTarrifDate,
	UTILITY_SWITCH_SUPPLIER_DATE = @switchSupplierDate
	WHERE SR_RFP_ACCOUNT_ID   IN 
	(SELECT SR_RFP_ACCOUNT_ID FROM SR_RFP_ACCOUNT WHERE SR_RFP_BID_GROUP_ID = @accountGroupId)


end
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_INSERT_UTILITY_SWITCH_DATES_INFO_P] TO [CBMSApplication]
GO
