SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.DELTOOL_SITE_GET_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@site_id       	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

--EXEC DELTOOL_SITE_GET -1,16794


CREATE           PROCEDURE DBO.DELTOOL_SITE_GET_P
	( @MyAccountId int
	, @site_id int
	)
AS
BEGIN

	   select s.site_id
		, s.site_type_id
		, st.entity_name site_type
		, s.site_name
		, s.division_id
		, d.client_id
		, d.division_name
		, s.ubmsite_id
		, s.primary_address_id
		, s.site_reference_number
		, s.site_product_service
		, s.production_schedule
		, s.shutdown_schedule
		, s.is_alternate_power
		, s.is_alternate_gas
		, s.doing_business_as
		, s.naics_code
		, s.tax_number
		, s.duns_number
		, s.is_history
		, s.history_reason_type_id
		, hr.entity_name history_reason_type
		, vw.site_name site_label
		, c.client_name
	     from site s with (nolock)
  left outer join entity st with (nolock) on st.entity_id = s.site_type_id
	     join division d with (nolock) on d.division_id = s.division_id
  left outer join entity hr with (nolock) on hr.entity_id = s.history_reason_type_id
	     join vwSiteName vw with (nolock) on vw.site_id = s.site_id
	     join client c with (nolock) on c.client_id = d.client_id
	    where s.site_id = @site_id


END
GO
GRANT EXECUTE ON  [dbo].[DELTOOL_SITE_GET_P] TO [CBMSApplication]
GO
