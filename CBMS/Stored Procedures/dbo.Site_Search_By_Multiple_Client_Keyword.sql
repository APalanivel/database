SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:        
 dbo.Site_Search_By_Client_Keyword      
      
DESCRIPTION:    
    
   Selects the site name(combination city, state , address and actual sitename)  for the given client.    
     
INPUT PARAMETERS:    
 Name   DataType   Default   Description        
------------------------------------------------------------------------      
 @Client_id     varchar(max)    
 @division_id   INT     NULL    
 @Keyword  VARCHAR(100)     NULL    
 @Start_Index   INT     1    
 @End_Index     INT     2147483647    
 @Site_State_Id INT     NULL  
    
OUTPUT PARAMETERS:        
 Name   DataType  Default    Description        
------------------------------------------------------------------------      
    
USAGE EXAMPLES:    
------------------------------------------------------------------------      
    
 
 EXEC dbo.[Site_Search_By_Multiple_Client_Keyword] '235,10045',300,null,'1707,1722,1810',1,100    
     
     
Author Initials:            
 Initials  Name          
------------------------------------------------------------------------      
SLP			Sri Lakshmi Pallikonda
           
 Modifications :            
 Initials   Date   Modification            
------------------------------------------------------------------------      
 SLP		2019-10-23	Created
  
******/
CREATE PROCEDURE [dbo].[Site_Search_By_Multiple_Client_Keyword]
     (
         @Client_Id VARCHAR(MAX)
         , @division_id INT = NULL
         , @Keyword VARCHAR(100) = NULL
         , @Site_Ids VARCHAR(MAX) = NULL
         , @Start_Index INT = 1
         , @End_Index INT = 2147483647
         , @Site_State_Id INT = NULL
         , @Is_Invoice_Collection_Account_Config BIT = 0
     )
AS
    BEGIN

        SET NOCOUNT ON;



        WITH CTE_Site
        AS (
               SELECT   TOP (@End_Index)
                        ch.Site_Id
                        , ch.Client_Hier_Id
                        , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
                        , ch.Sitegroup_Id AS Division_Id
                        , ROW_NUMBER() OVER (ORDER BY
                                                 Site_name ASC) AS Row_Num
                        , ch.Site_Reference_Number
               FROM
                    Core.Client_Hier ch
                    LEFT JOIN Core.Client_Hier_Account cha
                        ON ch.Client_Hier_Id = cha.Client_Hier_Id
               WHERE
                    -- ch.Client_Id = @Client_Id  
                    (   @Client_Id IS NULL
                        OR  EXISTS (   SELECT
                                            1
                                       FROM
                                            dbo.ufn_split(@Client_Id, ',') us
                                       WHERE
                                            us.Segments = ch.Client_Id))
                    AND ch.Site_Id > 0
                    AND (   @division_id IS NULL
                            OR  ch.Sitegroup_Id = @division_id)
                    AND (   @Keyword IS NULL
                            OR  ch.Site_name LIKE '%' + @Keyword + '%'
                            OR  ch.State_Name LIKE '%' + @Keyword + '%'
                            OR  ch.City LIKE '%' + @Keyword + '%'
                            OR  ch.Site_Reference_Number LIKE '%' + @Keyword + '%')
                    AND (   @Site_Ids IS NULL
                            OR  EXISTS (SELECT  1 FROM  dbo.ufn_split(@Site_Ids, ',') x WHERE   x.Segments = ch.Site_Id))
                    AND (   @Site_State_Id IS NULL
                            OR  ch.State_Id = @Site_State_Id)
                    AND (   @Is_Invoice_Collection_Account_Config = 0
                            OR  EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.Invoice_Collection_Account_Config icac
                                           WHERE
                                                icac.Account_Id = cha.Account_Id
                                                AND @Is_Invoice_Collection_Account_Config = 1))
               GROUP BY
                   ch.Site_Id
                   , ch.Client_Hier_Id
                   , ch.City
                   , ch.State_Name
                   , ch.Site_name
                   , ch.Sitegroup_Id
                   , ch.Site_Reference_Number
           )
        SELECT
            cs.Client_Hier_Id
            , cs.Site_Id
            , cs.Site_name
            , cs.Division_Id
            , cs.Site_Reference_Number
            , cs.Row_Num
        FROM
            CTE_Site AS cs
        WHERE
            cs.Row_Num BETWEEN @Start_Index
                       AND     @End_Index
        ORDER BY
            cs.Row_Num;

    END;





    ;


GO
GRANT EXECUTE ON  [dbo].[Site_Search_By_Multiple_Client_Keyword] TO [CBMSApplication]
GO
