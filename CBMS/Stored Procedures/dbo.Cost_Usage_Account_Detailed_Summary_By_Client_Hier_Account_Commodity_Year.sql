SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
Name:    
 CBMS.dbo.Cost_Usage_Account_Detailed_Summary_By_Client_Hier_Account_Commodity_Year
   
 Description:    
	Used to fetch data for the new account level detailed report

 Input Parameters:
    Name			    DataType      Default Description
------------------------------------------------------------------------
    @Client_Hier_Id     INT  
    @Account_Id			INT
    @Commodity_Id	    INT
    @Report_Year	    smALLINT
    @uom_Type_Id	    INT  
    @currency_Unit_Id   INT  
    
   
 Output Parameters:    
 Name  Datatype  Default Description    
------------------------------------------------------------    
   
 Usage Examples:  
------------------------------------------------------------    
    EXEcuTE dbo.Cost_Usage_Account_Detailed_Summary_By_Client_Hier_Account_Commodity_Year 1131,113, 290, 2011, 12, 3
    EXEcuTE dbo.Cost_Usage_Account_Detailed_Summary_By_Client_Hier_Account_Commodity_Year 1131,112, 291, 2010, 25, 3

    EXEcuTE dbo.Cost_Usage_Account_Detailed_Summary_By_Client_Hier_Account_Commodity_Year 32199,157784, 290, 2007, 12, 3
    
    SELECT TOP 10 * FROM dbo.Cost_Usage_Account_Dtl WHERE bucket_master_id = 100993 AND year(service_month) = 2011

    SELECT TOP 10 * FROM dbo.Cost_Usage_Account_Dtl WHERE Bucket_master_id = 100991 AND data_Source_Cd = 100349
   
Author Initials:    
 Initials	Name  
------------------------------------------------------------  
 HG			Harihara Suthan G
		   
 Modifications :    
 Initials   Date	    Modification    
------------------------------------------------------------    
 HG			2012-06-25  Created
 HG			2012-06-27	Data_Source column added as wanted to differentiate the manual and invoice data in reports
 ******/
CREATE PROCEDURE dbo.Cost_Usage_Account_Detailed_Summary_By_Client_Hier_Account_Commodity_Year
      (
       @Client_Hier_Id INT
      ,@Account_Id INT
      ,@Commodity_Id INT
      ,@Report_Year SMALLINT
      ,@uom_Type_Id INT
      ,@currency_Unit_Id INT )
AS
BEGIN

      SET NOCOUNT ON  

      DECLARE
            @Calendar_Year_Start_Month DATE
           ,@currency_Group_Id INT
  
      DECLARE @Bucket_List TABLE
            ( 
             Bucket_Master_Id INT PRIMARY KEY CLUSTERED
            ,Bucket_Name VARCHAR(255)
            ,Default_uom_Type_Id INT
            ,Bucket_Type_cd INT )  

      DECLARE @cu_Data TABLE
            ( 
             Bucket_Master_ID INT
            ,Bucket_Name VARCHAR(200)
            ,Service_Month DATE
            ,Month_Number INT
            ,Bucket_Type VARCHAR(25)
            ,uom_Type_Id INT
            ,Bucket_Value DECIMAL(28, 10)
            ,Data_Source VARCHAR(25) )

      SELECT
            @currency_Group_id = ch.Client_currency_Group_Id
      FROM
            Core.Client_Hier ch
      WHERE
            ch.Client_Hier_Id = @Client_Hier_Id

      SET @Calendar_Year_Start_Month = CONVERT(DATE, '1/1/' + CONVERT(VARCHAR(4), @Report_Year))  

      INSERT      INTO @Bucket_List
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Default_uom_Type_Id
                  ,Bucket_Type_cd )
                  SELECT
                        BM.Bucket_Master_Id
                       ,BM.Bucket_Name
                       ,BM.Default_uom_Type_Id
                       ,BM.Bucket_Type_cd
                  FROM
                        dbo.Bucket_Master BM
                  WHERE
                        BM.Commodity_Id = @Commodity_Id
                        AND BM.Is_Shown_On_Account = 1
                        AND BM.Is_Active = 1 ;

      INSERT      INTO @cu_Data
                  ( 
                   Bucket_Master_ID
                  ,Bucket_Name
                  ,Service_Month
                  ,Month_Number
                  ,Bucket_Type
                  ,uom_Type_Id
                  ,Bucket_Value
                  ,Data_Source )
                  SELECT
                        BL.Bucket_Master_Id
                       ,BL.Bucket_Name
                       ,cuad.Service_Month
                       ,sm.Month_Num AS Month_Number
                       ,cd.Code_Value AS Bucket_Type
                       ,( CASE WHEN uomconv.Conversion_Factor = 0
                                    AND cd.Code_Value = 'Determinant' THEN cuad.uom_Type_Id
                               WHEN uomconv.Conversion_Factor != 0
                                    AND cd.Code_Value = 'Determinant'
                                    AND BL.Bucket_Name IN ( 'Demand', 'Billed Demand', 'Contract Demand', 'Reactive Demand' ) THEN BL.Default_uom_Type_Id
                               WHEN uomconv.Conversion_Factor != 0
                                    AND cd.Code_Value = 'Determinant'
                                    AND BL.Bucket_Name NOT IN ( 'Demand', 'Billed Demand', 'Contract Demand', 'Reactive Demand' ) THEN @uom_Type_Id
                          END ) AS uom_Type_Id
                       ,SUM(CASE WHEN cd.Code_Value = 'Charge' THEN cuad.Bucket_Value * curconv.Conversion_Factor
                                 WHEN cd.Code_Value = 'Determinant'
                                      AND uomconv.Conversion_Factor = 0 THEN cuad.Bucket_Value
                                 WHEN cd.Code_Value = 'Determinant'
                                      AND uomconv.Conversion_Factor != 0 THEN cuad.Bucket_Value * uomconv.Conversion_Factor
                                 ELSE 0
                            END) Bucket_Value
                       ,dsc.Code_Value
                  FROM
                        dbo.Cost_Usage_Account_Dtl cuad
                        INNER JOIN @Bucket_List BL
                              ON BL.Bucket_Master_Id = cuad.Bucket_Master_Id
                        INNER JOIN dbo.Code cd
                              ON cd.Code_Id = BL.Bucket_Type_cd
                        INNER JOIN dbo.Code dsc
                              ON dsc.Code_Id = cuad.Data_Source_Cd
                        INNER JOIN meta.Date_Dim sm
                              ON sm.Date_D = cuad.Service_Month
                        LEFT OUTER JOIN dbo.currency_Unit_Conversion curconv
                              ON curconv.Base_Unit_Id = cuad.currency_Unit_Id
                                 AND curconv.Conversion_Date = sm.Date_D
                                 AND curconv.currency_Group_Id = @currency_Group_Id
                                 AND curconv.Converted_Unit_Id = @currency_Unit_Id
                        LEFT OUTER JOIN dbo.Consumption_Unit_Conversion uomconv
                              ON uomconv.Base_Unit_Id = cuad.uom_Type_Id
                                 AND uomconv.Converted_Unit_Id = CASE WHEN BL.Bucket_Name IN ( 'Demand', 'Billed Demand', 'Contract Demand', 'Reactive Demand' ) THEN BL.Default_uom_Type_Id
                                                                      ELSE @uom_Type_Id
                                                                 END
                  WHERE
                        cuad.Account_Id = @Account_Id
                        AND cuad.Client_Hier_Id = @Client_Hier_Id
                        AND sm.Year_Num = @Report_Year
                  GROUP BY
                        BL.Bucket_Master_Id
                       ,BL.Bucket_Name
                       ,cuad.Service_Month
                       ,sm.Month_Num
                       ,cd.Code_Value
                       ,( CASE WHEN uomconv.Conversion_Factor = 0
                                    AND cd.Code_Value = 'Determinant' THEN cuad.uom_Type_Id
                               WHEN uomconv.Conversion_Factor != 0
                                    AND cd.Code_Value = 'Determinant'
                                    AND BL.Bucket_Name IN ( 'Demand', 'Billed Demand', 'Contract Demand', 'Reactive Demand' ) THEN BL.Default_uom_Type_Id
                               WHEN uomconv.Conversion_Factor != 0
                                    AND cd.Code_Value = 'Determinant'
                                    AND BL.Bucket_Name NOT IN ( 'Demand', 'Billed Demand', 'Contract Demand', 'Reactive Demand' ) THEN @uom_Type_Id
                          END )
                       ,dsc.Code_Value


      SELECT
            BL.Bucket_Name
           ,BL.Bucket_Master_Id
           ,BL.Bucket_Type_cd
           ,( CASE WHEN cuad.Bucket_Type = 'Charge' THEN cu.currency_Unit_Name
                   WHEN cuad.Bucket_Type = 'Determinant' THEN uom.Entity_Name
              END ) AS uom_Name
           ,MAX(CASE WHEN cuad.Month_Number = 1 THEN cuad.Bucket_Value
                END) AS Month1
           ,MAX(CASE WHEN cuad.Month_Number = 2 THEN cuad.Bucket_Value
                END) AS Month2
           ,MAX(CASE WHEN cuad.Month_Number = 3 THEN cuad.Bucket_Value
                END) AS Month3
           ,MAX(CASE WHEN cuad.Month_Number = 4 THEN cuad.Bucket_Value
                END) AS Month4
           ,MAX(CASE WHEN cuad.Month_Number = 5 THEN cuad.Bucket_Value
                END) AS Month5
           ,MAX(CASE WHEN cuad.Month_Number = 6 THEN cuad.Bucket_Value
                END) AS Month6
           ,MAX(CASE WHEN cuad.Month_Number = 7 THEN cuad.Bucket_Value
                END) AS Month7
           ,MAX(CASE WHEN cuad.Month_Number = 8 THEN cuad.Bucket_Value
                END) AS Month8
           ,MAX(CASE WHEN cuad.Month_Number = 9 THEN cuad.Bucket_Value
                END) AS Month9
           ,MAX(CASE WHEN cuad.Month_Number = 10 THEN cuad.Bucket_Value
                END) AS Month10
           ,MAX(CASE WHEN cuad.Month_Number = 11 THEN cuad.Bucket_Value
                END) AS Month11
           ,MAX(CASE WHEN cuad.Month_Number = 12 THEN cuad.Bucket_Value
                END) AS Month12
           ,SUM(cuad.Bucket_Value) AS Total
           ,MAX(CASE WHEN cuad.Month_Number = 1 THEN cuad.Data_Source
                END) AS Month1_Data_Source
           ,MAX(CASE WHEN cuad.Month_Number = 2 THEN cuad.Data_Source
                END) AS Month2_Data_Source
           ,MAX(CASE WHEN cuad.Month_Number = 3 THEN cuad.Data_Source
                END) AS Month3_Data_Source
           ,MAX(CASE WHEN cuad.Month_Number = 4 THEN cuad.Data_Source
                END) AS Month4_Data_Source
           ,MAX(CASE WHEN cuad.Month_Number = 5 THEN cuad.Data_Source
                END) AS Month5_Data_Source
           ,MAX(CASE WHEN cuad.Month_Number = 6 THEN cuad.Data_Source
                END) AS Month6_Data_Source
           ,MAX(CASE WHEN cuad.Month_Number = 7 THEN cuad.Data_Source
                END) AS Month7_Data_Source
           ,MAX(CASE WHEN cuad.Month_Number = 8 THEN cuad.Data_Source
                END) AS Month8_Data_Source
           ,MAX(CASE WHEN cuad.Month_Number = 9 THEN cuad.Data_Source
                END) AS Month9_Data_Source
           ,MAX(CASE WHEN cuad.Month_Number = 10 THEN cuad.Data_Source
                END) AS Month10_Data_Source
           ,MAX(CASE WHEN cuad.Month_Number = 11 THEN cuad.Data_Source
                END) AS Month11_Data_Source
           ,MAX(CASE WHEN cuad.Month_Number = 12 THEN cuad.Data_Source
                END) AS Month12_Data_Source
           ,@Calendar_Year_Start_Month AS Start_Month
      FROM
            @Bucket_List BL
            LEFT OUTER JOIN @cu_Data cuad
                  ON cuad.Bucket_Master_Id = BL.Bucket_Master_Id
            LEFT OUTER JOIN dbo.currency_Unit cu
                  ON cu.currency_Unit_Id = @currency_Unit_Id
            LEFT OUTER JOIN dbo.Entity uom
                  ON uom.Entity_Id = cuad.uom_Type_Id
      GROUP BY
            BL.Bucket_Name
           ,BL.Bucket_Master_Id
           ,BL.Bucket_Type_cd
           ,BL.Default_uom_Type_Id
           ,( CASE WHEN cuad.Bucket_Type = 'Charge' THEN cu.currency_Unit_Name
                   WHEN cuad.Bucket_Type = 'Determinant' THEN uom.Entity_Name
              END )
      ORDER BY
            BL.Bucket_Name

END
;
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Detailed_Summary_By_Client_Hier_Account_Commodity_Year] TO [CBMSApplication]
GO
