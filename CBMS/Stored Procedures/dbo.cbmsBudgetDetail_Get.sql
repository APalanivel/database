SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsBudgetDetail_Get

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@myaccountid   	int       	null      	
	@budget_detail_id	int       	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE    PROCEDURE [dbo].[cbmsBudgetDetail_Get]
	( @myaccountid int = null
	, @budget_detail_id int = null
	)
AS
BEGIN

	   select budget_detail_id
		, budget_id
		, account_id
		, date
		, usage
		, variable
		, multiplier
		, adder
		, transportation	
		, transmission
		, distribution
		, other_unit
		, other_fixed
		, commodity_type_id
	     from budget_detail
	    where budget_detail_id = @budget_detail_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsBudgetDetail_Get] TO [CBMSApplication]
GO
