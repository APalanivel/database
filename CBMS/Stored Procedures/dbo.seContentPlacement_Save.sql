SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure dbo.seContentPlacement_Save
	(
		@ContentPlacementId int = null
	,	@ContentPageId int
	,	@PlacementLabel varchar(50)
	)
As
BEGIN
	set nocount on

	declare @ThisId int


	set @ThisId = @ContentPlacementId
	
	if @ThisId is null
	begin
	
		select @ThisId = ContentPlacementId
		  from seContentPlacement
		 where ContentPageId = @ContentPageId 
		   and PlacementLabel = @PlacementLabel

	end

	if @ThisId is null
	begin

		insert into seContentPlacement
			( ContentPageId
			, PlacementLabel
			)
		values
			( @ContentPageId
			, @PlacementLabel
			)

		set @ThisId = @@IDENTITY

	end
	else
	begin
	
		update seContentPlacement
		   set ContentPageId = @ContentPageId
		     , PlacementLabel = @PlacementLabel
		 where ContentPlacementId = @ThisId
	
	end
	exec seContentPlacement_Get @ThisId

END
GO
GRANT EXECUTE ON  [dbo].[seContentPlacement_Save] TO [CBMSApplication]
GO
