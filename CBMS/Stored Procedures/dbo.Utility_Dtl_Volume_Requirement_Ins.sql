SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Utility_Dtl_Volume_Requirement_Ins

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	 @Commodity_Id				INT
     @Vendor_Id					INT 
     @Question_Commodity_Map_Id INT
     @Utility_Volume_Dsc		VARCHAR(30) = NULL
     @Utility_UOM_Cd			INT = NULL
     @Time_Period				VARCHAR(200) = NULL
     @Time_Req					VARCHAR(500) = NULL
     @Other_Req					VARCHAR(500) = NULL

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	
	EXEC Utility_Dtl_Volume_Requirement_Ins 291,30,12,'Utility_Volume_Dsc Test',1,'Annual','Time_Req Test','Other_Req Test'
	
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	SKA			01/12/2011	Created
******/

CREATE PROC dbo.Utility_Dtl_Volume_Requirement_Ins
      (
       @Commodity_Id INT
      ,@Vendor_Id INT
      ,@Question_Commodity_Map_Id INT
      ,@Utility_Volume_Dsc VARCHAR(30) = NULL
      ,@Utility_UOM_Cd INT = NULL
      ,@Time_Period VARCHAR(200) = NULL
      ,@Time_Req VARCHAR(500) = NULL
      ,@Other_Req VARCHAR(500) = NULL )
AS 
BEGIN
 
      SET nocount ON ;

      INSERT INTO
            Utility_Dtl_Volume_Requirement
            (
             Question_Commodity_Map_Id
            ,Vendor_Commodity_Map_Id
            ,Utility_Volume_Dsc
            ,Utility_UOM_Cd
            ,Time_Period
            ,Time_Req
            ,Other_Req )
            SELECT
                  @Question_Commodity_Map_Id
                 ,vcm.VENDOR_COMMODITY_MAP_ID
                 ,@Utility_Volume_Dsc
                 ,@Utility_UOM_Cd
                 ,@Time_Period
                 ,@Time_Req
                 ,@Other_Req
            FROM
                  dbo.VENDOR_COMMODITY_MAP vcm
            WHERE
                  vcm.vendor_id = @Vendor_Id
                  AND vcm.COMMODITY_TYPE_ID = @Commodity_Id
END                        
GO
GRANT EXECUTE ON  [dbo].[Utility_Dtl_Volume_Requirement_Ins] TO [CBMSApplication]
GO
