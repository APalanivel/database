SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
                    
NAME: [dbo].[Ec_Account_Group_Type_Exists]  
                      
DESCRIPTION:                      
    To Check weather Ec_Account_Group_Type exists or not                     
                      
INPUT PARAMETERS:      
                     
Name                              DataType            Default        Description      
---------------------------------------------------------------------------------------------------------------    
@State_Id       INT   
@Commodity_Id      INT  
@Group_Type_Name     NVARCHAR(60)  
  
                      
OUTPUT PARAMETERS:        
                        
Name                              DataType            Default        Description      
---------------------------------------------------------------------------------------------------------------    
                      
USAGE EXAMPLES:                          
---------------------------------------------------------------------------------------------------------------                            
              
  EXEC [dbo].[Ec_Account_Group_Type_Exists1] 124,290,'Group one55'  
     
                     
AUTHOR INITIALS:      
                    
Initials                Name      
---------------------------------------------------------------------------------------------------------------    
RKV                      Ravi kumar vegesna       
                       
MODIFICATIONS:     
                      
Initials                Date            Modification    
---------------------------------------------------------------------------------------------------------------    
 RKV                     2016-11-17      Created for Maint-4563  
                     
******/   
  
                 
CREATE PROCEDURE [dbo].[Ec_Account_Group_Type_Exists]
      ( 
       @State_Id INT
      ,@Commodity_Id INT
      ,@Group_Type_Name NVARCHAR(60) = NULL )
AS 
BEGIN                  
      SET NOCOUNT ON;       
                  
      DECLARE @Is_Ec_Account_Group_Type_Exists BIT = 0  
  
      SELECT
            @Is_Ec_Account_Group_Type_Exists = 1
      FROM
            dbo.Ec_Account_Group_Type eagt
      WHERE
            eagt.State_Id = @State_Id
            AND eagt.Commodity_Id = @Commodity_Id
            AND ( @Group_Type_Name IS NULL
                  OR eagt.Group_Type_Name = @Group_Type_Name )
              
      SELECT
            @Is_Ec_Account_Group_Type_Exists Ec_Account_Group_Type_Exists  
         
END;  
;


;
GO
GRANT EXECUTE ON  [dbo].[Ec_Account_Group_Type_Exists] TO [CBMSApplication]
GO
