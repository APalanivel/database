SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/******    
NAME:    Trade.CM_Dtls_Sel_By_Deal_Ticket
   
    
DESCRIPTION:   
   
  
    
INPUT PARAMETERS:    
	Name          DataType       Default        Description    
-----------------------------------------------------------------------------    
	@Deal_Ticket_Id		INT
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec Trade.CM_Dtls_Sel_By_Deal_Ticket 291
	Exec Trade.CM_Dtls_Sel_By_Deal_Ticket 298
	
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	RR          Raghu Reddy
    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	RR          2019-03-27	GRM - Created
     
    
******/

CREATE PROC [Trade].[CM_Dtls_Sel_By_Deal_Ticket] ( @Deal_Ticket_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;
      
      SELECT
            ui.FIRST_NAME
           ,ui.LAST_NAME
           ,ui.EMAIL_ADDRESS
      FROM
            dbo.USER_INFO ui
            INNER JOIN dbo.CLIENT_CEM_MAP ccm
                  ON ui.USER_INFO_ID = ccm.USER_INFO_ID
            INNER JOIN Trade.Deal_Ticket dt
                  ON ccm.CLIENT_ID = dt.Client_Id
      WHERE
            dt.Deal_Ticket_Id = @Deal_Ticket_Id
      
END;

GO
GRANT EXECUTE ON  [Trade].[CM_Dtls_Sel_By_Deal_Ticket] TO [CBMSApplication]
GO
