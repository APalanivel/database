
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
    
NAME: [DBO].[Cost_Usage_History_Del_For_Meter_Commodity]      
         
DESCRIPTION:     
    
 To Delete Cost/Usage history associated with the given Meter id and the commodity of it.    
 This procedure will be called directly from the application if the meter being deleted is the last meter of the     
 commodity in the utility account, to delete all the cost/usage associated specific to meter commodity    
     
INPUT PARAMETERS:    
NAME		DATATYPE DEFAULT  DESCRIPTION    
------------------------------------------------------------    
@Meter_Id	INT    

OUTPUT PARAMETERS:    
NAME   DATATYPE DEFAULT  DESCRIPTION    

------------------------------------------------------------    
USAGE EXAMPLES:    
------------------------------------------------------------    

 SELECT count(1) FROM dbo.Cost_Usage_Account_Dtl WHERE Account_Id = 440819 --> Meter_Id = 326032    
 SELECT count(1) FROM dbo.Bucket_Account_Interval_Dtl WHERE Account_Id = 440819
 BEGIN TRAN    
  EXEC Cost_Usage_History_Del_For_Meter_Commodity 326032    
 ROLLBACK TRAN    
     
 SELECT Account_Id,Meter_Id,Commodity_Id FROM core.Client_Hier_Account WHERE Account_Id = 105322    
 SELECT count(1) FROM dbo.Cost_Usage_Account_Dtl WHERE Account_Id = 105322
 SELECT count(1) FROM dbo.Bucket_Account_Interval_Dtl WHERE Account_Id = 105322   

 BEGIN TRAN    
  EXEC Cost_Usage_History_Del_For_Meter_Commodity 373273    
 ROLLBACK TRAN    

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
HG			Harihara Suthan G    
PNR			PANDARINATH    
BCH			Balaraju    
RR			Raghu Reddy    
AP			Athmaram Pabbathi    
RKV			Ravi Kumar Vegesna

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------    
HG			08/30/2010	Created    
BCH			2011-12-30  Deleted transactions (delete tool enhancement - Transactions are maintained in Application)    
RR			2012-01-02  Removed the condition to check SYSTEM_ROW status, as cost and usage data that comes from invoices(conatraint) will anyways be     
						  removed when invoices are removed. So the data left in this section after backing out invoices all will be manually entered     
						  data comes from UDE, which can be deleted as it is not constraint.    
RR			2012-01-24  Updating all values with zero specific to meter commodity, data socurce code as null and system row as 1 only if other commodity    
						  data source is already null. (Previously the rows are deleting only when system row is 1(invoice backed out) or all the values of    
						  the commodity are zero, in this case some rows are left behind and values are appearing again in application cost and usage if a     
						  new meter is added to the account of the same commodity)    
AP			2012-03-19  Following Changes are made for Addl Data    
						-- Removed the references of Cost_Usage, Cost_Usage_Comment_Map tables    
						-- Replaced dbo.Cost_Usage_Account_Dtl_Del_By_Cost_Usage_Account_Dtl_Id SP call with dbo.Cost_Usage_Account_Dtl_Del_By_Client_Hier_Account_Bucket_Service_Month    
						-- Used Temp tables with clustered indexes    
						-- Replaced base tables with CHA    
RR			2012-06-27	Replaced while exists(select and deleting processed row) for looping through each record in temp table with     
							row number column, iterating the loop till count reaches maximum row number value
RKV			2013-04-22	Added Logic to Delete Bucket_Account_Interval_Dtl history associated with the given Meter id and the commodity of it

*/
CREATE PROCEDURE dbo.Cost_Usage_History_Del_For_Meter_Commodity @Meter_Id INT
AS 
BEGIN

      SET NOCOUNT ON ;    
   
      CREATE TABLE #Cost_Usage_Account_Dtl_List
            ( 
             Client_Hier_Id INT
            ,Account_Id INT
            ,Bucket_Master_Id INT
            ,Service_Month DATE
            ,PRIMARY KEY CLUSTERED ( Client_Hier_Id, Account_Id, Bucket_Master_Id, Service_Month )
            ,Row_Num INT IDENTITY(1, 1) )   
                 
      CREATE TABLE #Bucket_Account_Interval_Dtl_List
            ( 
             Client_Hier_Id INT
            ,Account_Id INT
            ,Bucket_Master_Id INT
            ,Service_Start_Dt DATE
            ,Service_End_Dt DATE
            ,Data_Source_Cd INT
            ,PRIMARY KEY CLUSTERED ( Client_Hier_Id, Account_Id, Bucket_Master_Id, Service_Start_Dt, Service_End_Dt, Data_Source_Cd )
            ,Row_Num INT IDENTITY(1, 1) )   

      DECLARE
            @Client_Hier_Id INT
           ,@Service_Month DATE
           ,@Service_Start_Dt DATE
           ,@Service_End_Dt DATE
           ,@Data_Source_Cd INT
           ,@Bucket_Master_Id INT
           ,@Account_Id INT
           ,@Total_Row_Count INT
           ,@Row_Counter INT    

      INSERT      INTO #Cost_Usage_Account_Dtl_List
                  ( 
                   Client_Hier_Id
                  ,Account_Id
                  ,Bucket_Master_Id
                  ,Service_Month )
                  SELECT
                        cuad.Client_Hier_Id
                       ,cuad.Account_Id
                       ,cuad.Bucket_Master_Id
                       ,cuad.Service_Month
                  FROM
                        dbo.Cost_Usage_Account_Dtl cuad
                        INNER JOIN dbo.Bucket_Master bm
                              ON bm.Bucket_Master_Id = cuad.Bucket_Master_Id
                        INNER JOIN Core.Client_Hier_Account cha
                              ON cha.Account_Id = cuad.Account_Id
                                 AND cha.Client_Hier_Id = cuad.Client_Hier_Id
                                 AND cha.Commodity_Id = bm.Commodity_Id
                  WHERE
                        cha.Meter_Id = @Meter_Id
                  GROUP BY
                        cuad.Client_Hier_Id
                       ,cuad.Account_Id
                       ,cuad.Bucket_Master_Id
                       ,cuad.Service_Month    
                         
                         
      INSERT      INTO #Bucket_Account_Interval_Dtl_List
                  ( 
                   Client_Hier_Id
                  ,Account_Id
                  ,Bucket_Master_Id
                  ,Service_Start_Dt
                  ,Service_End_Dt
                  ,Data_Source_Cd )
                  SELECT
                        baid.Client_Hier_Id
                       ,baid.Account_Id
                       ,baid.Bucket_Master_Id
                       ,baid.Service_Start_Dt
                       ,baid.Service_End_Dt
                       ,baid.Data_Source_Cd
                  FROM
                        dbo.Bucket_Account_Interval_Dtl baid
                        INNER JOIN dbo.Bucket_Master bm
                              ON bm.Bucket_Master_Id = baid.Bucket_Master_Id
                        INNER JOIN Core.Client_Hier_Account cha
                              ON cha.Account_Id = baid.Account_Id
                                 AND cha.Client_Hier_Id = baid.Client_Hier_Id
                                 AND cha.Commodity_Id = bm.Commodity_Id
                  WHERE
                        cha.Meter_Id = @Meter_Id
                  GROUP BY
                        baid.Client_Hier_Id
                       ,baid.Account_Id
                       ,baid.Bucket_Master_Id
                       ,baid.Service_Start_Dt
                       ,baid.Service_End_Dt
                       ,baid.Data_Source_Cd    
  
                     
    
      BEGIN TRY    
    
--Loop for deleting records from Cost_Usage_Account_Dtl table    
            SELECT
                  @Total_Row_Count = max(Row_Num)
            FROM
                  #Cost_Usage_Account_Dtl_List    
            SET @Row_Counter = 1     
                
            WHILE ( @Row_Counter <= @Total_Row_Count ) 
                  BEGIN    
    
                        SELECT
                              @Client_Hier_Id = cuad.Client_Hier_Id
                             ,@Bucket_Master_Id = cuad.Bucket_Master_Id
                             ,@Service_Month = cuad.Service_Month
                             ,@Account_Id = cuad.Account_Id
                        FROM
                              #Cost_Usage_Account_Dtl_List cuad
                        WHERE
                              Row_Num = @Row_Counter      
    
                        EXECUTE dbo.Cost_Usage_Account_Dtl_DEL_By_Client_Hier_Account_Bucket_Service_Month 
                              @Client_Hier_Id = @Client_Hier_Id
                             ,@Account_Id = @Account_Id
                             ,@Bucket_Master_Id = @Bucket_Master_Id
                             ,@Service_Month = @Service_Month    
    
                        SET @Row_Counter = @Row_Counter + 1    
                  END                    

			--Loop for deleting records from Bucket_Account_Interval_Dtl table
            SELECT
                  @Total_Row_Count = max(Row_Num)
            FROM
                  #Bucket_Account_Interval_Dtl_List

            SET @Row_Counter = 1

            WHILE ( @Row_Counter <= @Total_Row_Count ) 
                  BEGIN

                        SELECT
                              @Client_Hier_Id = baid.Client_Hier_Id
                             ,@Account_Id = baid.Account_Id
                             ,@Bucket_Master_Id = baid.Bucket_Master_Id
                             ,@Service_Start_Dt = baid.Service_Start_Dt
                             ,@Service_End_Dt = baid.Service_End_Dt
                             ,@Data_Source_Cd = baid.Data_Source_Cd
                        FROM
                              #Bucket_Account_Interval_Dtl_List baid
                        WHERE
                              Row_Num = @Row_Counter     

                        EXECUTE dbo.Bucket_Account_Interval_Dtl_Del 
                              @Client_Hier_Id = @Client_Hier_Id
                             ,@Account_Id = @Account_Id
                             ,@Bucket_Master_Id = @Bucket_Master_Id
                             ,@Service_Start_Dt = @Service_Start_Dt
                             ,@Service_End_Dt = @Service_End_Dt
                             ,@Data_Source_Cd = @Data_Source_Cd  

                        SET @Row_Counter = @Row_Counter + 1

                  END
      END TRY

      BEGIN CATCH    
            EXEC dbo.usp_RethrowError
      END CATCH

      DROP TABLE #Cost_Usage_Account_Dtl_List  
      DROP TABLE #Bucket_Account_Interval_Dtl_List    

END ;

;
GO



GRANT EXECUTE ON  [dbo].[Cost_Usage_History_Del_For_Meter_Commodity] TO [CBMSApplication]
GO
