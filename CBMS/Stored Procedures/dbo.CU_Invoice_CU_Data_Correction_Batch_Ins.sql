SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.CU_Invoice_CU_Data_Correction_Batch_Ins
           
DESCRIPTION:             
			To insert invoices to be processed
			
INPUT PARAMETERS:            
	Name						DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Contract_Id				INT
    @Is_Notification_Required	BIT			1
    @Status_Cd					INT
    @User_Info_Id				INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
	EXEC dbo.CODE_SEL_BY_CodeSet_Name @CodeSet_Name = 'Request Status'
	
	SELECT TOP 10 * FROM dbo.CU_Invoice_CU_Data_Correction_Batch
            
	BEGIN TRANSACTION
		DECLARE @CU_Invoice_CU_Data_Correction_Batch_Id INT
		SELECT * FROM  dbo.CU_Invoice_CU_Data_Correction_Batch WHERE Contract_Id = 2 
		EXEC dbo.CU_Invoice_CU_Data_Correction_Batch_Ins 2,1,100429,16,@CU_Invoice_CU_Data_Correction_Batch_Id=@CU_Invoice_CU_Data_Correction_Batch_Id OUT
		SELECT * FROM  dbo.CU_Invoice_CU_Data_Correction_Batch WHERE Contract_Id = 2
		SELECT @CU_Invoice_CU_Data_Correction_Batch_Id
	ROLLBACK TRANSACTION
	
	
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-03-09	Contract placeholder - CP-77 Created
******/

CREATE PROCEDURE [dbo].[CU_Invoice_CU_Data_Correction_Batch_Ins]
      ( 
       @Contract_Id INT
      ,@Is_Notification_Required BIT = 1
      ,@Status_Cd INT
      ,@User_Info_Id INT
      ,@CU_Invoice_CU_Data_Correction_Batch_Id INT OUTPUT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      INSERT      INTO dbo.CU_Invoice_CU_Data_Correction_Batch
                  ( 
                   Contract_Id
                  ,Status_Cd
                  ,Is_Notification_Required
                  ,Requested_User_Id
                  ,Requested_Ts
                  ,Last_Change_Ts )
                  SELECT
                        @Contract_Id
                       ,@Status_Cd
                       ,@Is_Notification_Required
                       ,@User_Info_Id
                       ,GETDATE()
                       ,GETDATE()
                       
      SET @CU_Invoice_CU_Data_Correction_Batch_Id = @@IDENTITY
                  
                                          
      
      
END;

;
GO
GRANT EXECUTE ON  [dbo].[CU_Invoice_CU_Data_Correction_Batch_Ins] TO [CBMSApplication]
GO
