SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:  
	 dbo.Site_Dtl_Sel_By_Cu_Invoice_Site_Commodity_Id        
                
Description:                
		This sproc to get the site details based Invoice,site,commodity.        
                             
 Input Parameters:                
    Name						DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
	@Cu_Invoice_Id				INT
	@Site_Id					INT
	@Commodity_Id				INT

      
 Output Parameters:                      
    Name					  DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
----------------------------------------------------------------------------------------     
Select * from CU_INVOICE_SERVICE_MONTH where cu_invoice_Id =17359963 
select * from core.client_hier_Account  where account_id=557163
select * from core.client_hier where client_hier_id=293366

   Exec dbo.Site_Dtl_Sel_By_Cu_Invoice_Site_Commodity_Id   17359963 ,297164,null    
       
   
Author Initials:                
    Initials		Name                
----------------------------------------------------------------------------------------                  
	NR				Narayana Reddy                 
 Modifications:                
    Initials        Date			Modification                
----------------------------------------------------------------------------------------                  
    NR				2019-04-18		Created For data quality.           
               
******/


CREATE PROCEDURE [dbo].[Site_Dtl_Sel_By_Cu_Invoice_Site_Commodity_Id]
    (
        @Cu_Invoice_Id INT
        , @Site_Id INT
        , @Commodity_Id INT = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            ch.Client_Hier_Id
            , ch.Site_Id
            , ch.Site_name
            , ch.City
            , ch.State_Name
            , ch.Site_Address_Line1 + ' ' + ISNULL(ch.Site_Address_Line2, '') AS Address
            , cha.Account_Id
            , cha.Display_Account_Number
            , c.Commodity_Name
            , cha.Account_Vendor_Name
            , cha.Meter_Number
            , cha.Rate_Name
            , cha.Account_Type
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account cha
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN dbo.Commodity c
                ON c.Commodity_Id = cha.Commodity_Id
        WHERE
            ch.Site_Id > 0
            AND ch.Site_Id = @Site_Id
            AND (   @Commodity_Id IS NULL
                    OR  cha.Commodity_Id = @Commodity_Id)
            AND EXISTS (   SELECT
                                1
                           FROM
                                dbo.CU_INVOICE_SERVICE_MONTH cism
                           WHERE
                                cism.Account_ID = cha.Account_Id
                                AND cism.CU_INVOICE_ID = @Cu_Invoice_Id)
        GROUP BY
            ch.Client_Hier_Id
            , ch.Site_Id
            , ch.Site_name
            , ch.City
            , ch.State_Name
            , cha.Account_Id
            , cha.Display_Account_Number
            , c.Commodity_Name
            , cha.Account_Vendor_Name
            , cha.Meter_Number
            , cha.Rate_Name
            , ch.Site_Address_Line1
            , ch.Site_Address_Line2
            , cha.Account_Type
        ORDER BY
            ch.Site_name;


    END;


GO
GRANT EXECUTE ON  [dbo].[Site_Dtl_Sel_By_Cu_Invoice_Site_Commodity_Id] TO [CBMSApplication]
GO
