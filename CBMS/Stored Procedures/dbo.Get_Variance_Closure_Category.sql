SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*********     
NAME:    [dbo].[Get_Variance_Closure_Category]  
   
DESCRIPTION:  Used to select code value from code table using the code name    
  
INPUT PARAMETERS:      
      Name              DataType          Default     Description      
------------------------------------------------------------      
    
          
      
OUTPUT PARAMETERS:      
      Name              DataType          Default     Description      
------------------------------------------------------------      
      
USAGE EXAMPLES:     
  
 
 EXEC [dbo].[Get_Variance_Closure_Category]     
  
------------------------------------------------------------    
AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
TRK Ramakrishna Thummala  
AP Arunkumar Palanivel  
  
Initials Date  Modification    
------------------------------------------------------------    

AP Feb 19,2020 Created stored procedure to get variance closure category  
  
******/
CREATE PROCEDURE [dbo].[Get_Variance_Closure_Category]
    
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT DISTINCT c.Code_Id AS ID, c.code_dsc AS NAME FROM dbo.Variance_Closed_Reason_Category vr
JOIN dbo.code c 
ON vr.Closure_Category_Cd = c.Code_Id
WHERE vr.Is_Active =1
ORDER BY c.Code_Dsc
    END;

GO
GRANT EXECUTE ON  [dbo].[Get_Variance_Closure_Category] TO [CBMSApplication]
GO
