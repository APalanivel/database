SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/******              
Name:   dbo.Consumption_Level_Specific_Configuration_Parameters_Sel      
              
Description:              
			
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
  
                 
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
---------------------------------------------------------------------------------------- 
EXEC Consumption_Level_Specific_Configuration_Parameters_Sel 1

    
      
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna
	AP				Arunkumar Palanivel
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
	RKV             2020-01-09      Created for AVT.  
	Ap				feb 10,2020		Modified according to the new changes     
             
******/
CREATE PROCEDURE [dbo].[Consumption_Level_Specific_Configuration_Parameters_Sel]
      (
      @Commodity_Id INT )
AS
      BEGIN
            SET NOCOUNT ON;

            WITH pivot_data
            AS ( SELECT DISTINCT
                        c.Code_Value AS Category
                      , vcl.Consumption_Level_Desc AS [Average Consumption Level]
                      , vesp.Param_Name
                      , vcespv.Param_Value 
					  ,vcl.Variance_Consumption_Level_Id
					-- row_number() OVER (ORDER BY  vcl.Variance_Consumption_Level_Id DESC) AS rn
                 FROM   dbo.Variance_Consumption_Extraction_Service_Param_Value vcespv
                        JOIN
                        dbo.Variance_Extraction_Service_Param vesp
                              ON vesp.Variance_Extraction_Service_Param_Id = vcespv.Variance_Extraction_Service_Param_Id
                        JOIN
                        dbo.Variance_Consumption_Level vcl
                              ON vcl.Variance_Consumption_Level_Id = vcespv.Variance_Consumption_Level_Id
                        JOIN
                        Code c
                              ON c.Code_Id = vcespv.Variance_Category_Cd
                        JOIN
                        dbo.Commodity cm
                              ON cm.Commodity_Id = vcl.Commodity_Id
                 WHERE  cm.Commodity_Id = @Commodity_Id ),pivot_data_1 AS(
            SELECT      
                        p.Category
                      , [Average Consumption Level]  AS [Average_Consumption_Level]
                      , [Analogous_Account_Distance_value] AS [AA_Distance]
                      , [Analogous_Account_Max_Account_Required] AS [AA_Max_Accounts]
                      , [Analogous_Account_Min_Account_Required] AS [AA_Min_Accounts]
                      , [Error_Metric] AS [Error_Metric]
                      , [Linear_Regression_Sigma_value],
					  row_number() OVER (ORDER BY  Variance_Consumption_Level_Id asc) AS rn
            FROM        pivot_data
                  PIVOT
                        (     max(Param_Value)
                              FOR Param_Name IN (
                                    [Analogous_Account_Distance_value]
                                  , [Analogous_Account_Max_Account_Required]
                                  , [Analogous_Account_Min_Account_Required]
                                  , [Error_Metric]
                                  , [Linear_Regression_Sigma_value] )) AS p)

								  SELECT Category	
									,Average_Consumption_Level	 
									,AA_Distance	 
									,AA_Max_Accounts	 
									,AA_Min_Accounts	 
									,Error_Metric	 
									,Linear_Regression_Sigma_value 
 FROM pivot_data_1 
								  ORDER BY pivot_data_1.rn
			
				--ORDER BY p.rn	
				;

      END;
GO
GRANT EXECUTE ON  [dbo].[Consumption_Level_Specific_Configuration_Parameters_Sel] TO [CBMSApplication]
GO
