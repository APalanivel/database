SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Invoice_Participation_Del]  
     
DESCRIPTION: 
	It Deletes Invoice Participation for Selected Account and Site Id's Which are associated by account.
      
INPUT PARAMETERS:          
NAME				DATATYPE	DEFAULT		DESCRIPTION          
---------------------------------------------------------------          
@Account_Id			INT	
@Site_Id			INT
@Service_Month		DATETIME		
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        

	BEGIN TRAN
		EXEC Invoice_Participation_Del  289629, 97001, '12/1/2010'
	ROLLBACK TRAN

	SELECT TOP 10 * FROM invoice_Participation	ORDER BY ACCOUNT_ID DESC

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			27-MAY-10	CREATED

*/

CREATE PROCEDURE dbo.Invoice_Participation_Del
   (
      @Account_Id		INT
      ,@Site_Id			INT
      ,@Service_Month	DATETIME
   )
AS
BEGIN

    SET NOCOUNT ON;

    DELETE
   	FROM
		dbo.INVOICE_PARTICIPATION
	WHERE
		ACCOUNT_ID = @Account_Id
		AND SITE_ID = @Site_Id
		AND SERVICE_MONTH = @Service_Month

END
GO
GRANT EXECUTE ON  [dbo].[Invoice_Participation_Del] TO [CBMSApplication]
GO
