SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.Client_Service_Category_Commodity_Sel_By_Category_Id

DESCRIPTION:
 used to select the commodity_id and commodity_name from commodity table

INPUT PARAMETERS:
	Name						DataType		Default	Description
------------------------------------------------------------
	@Client_Service_Category_Id INT

	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

Exec Client_Service_Category_Commodity_Sel_By_Category_Id 111
Exec Client_Service_Category_Commodity_Sel_By_Category_Id 91

AUTHOR INITIALS:

	BCH			Balaraju
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
 BCH			2011-10-19  created

******/
CREATE PROCEDURE dbo.Client_Service_Category_Commodity_Sel_By_Category_Id
      ( 
       @Client_Service_Category_Id INT )
AS 
BEGIN

      SET NOCOUNT ON 
      SELECT
            cmd.Commodity_Id
           ,cmd.Commodity_Name
      FROM
            dbo.Client_Service_Category_Commodity cscc
            JOIN dbo.Commodity cmd
                  ON cmd.Commodity_Id = cscc.Commodity_id
      WHERE
            cscc.Sub_Client_Service_Category_Id IS NULL
            AND cscc.Client_Service_Category_Id = @Client_Service_Category_Id
       

END
GO
GRANT EXECUTE ON  [dbo].[Client_Service_Category_Commodity_Sel_By_Category_Id] TO [CBMSApplication]
GO
