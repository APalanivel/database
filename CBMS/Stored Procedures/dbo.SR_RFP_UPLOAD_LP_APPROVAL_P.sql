SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[SR_RFP_UPLOAD_LP_APPROVAL_P]
	@userId INT,
	@sessionId varchar(100),
	@cbmsImageId INT,  --added by Jaya
	--@cbmsImageDocId varchar(200),
	--@cbmsImage image ,
	--@contentType varchar(200),
	@rfpId INT,
	@dateUploaded DATETIME,
	@accountGroupId INT,
	@statusTypeId INT,
	@siteId INT
AS
BEGIN

	SET NOCOUNT ON
	
	DECLARE @pendingClientApprovalStatusId INT
		, @clientApprovedStatusId INT
		, @revPendingClientApprovalStatusId INT
		, @revClientApprovedStatusId INT
		, @rfpLPStatusTypeId INT
		, @entityId INT
		, @projectId INT
		, @DateNow DATETIME

	SELECT @pendingClientApprovalStatusId = entity_id FROM dbo.Entity(NOLOCK) WHERE Entity_Name = 'Pending Client Approval' AND Entity_Type = 1006

	SELECT @clientApprovedStatusId = entity_id from dbo.Entity(NOLOCK) WHERE Entity_Name = 'Client approved' AND Entity_Type = 1006
	  	
	SELECT @revPendingClientApprovalStatusId = entity_id FROM dbo.Entity(NOLOCK) WHERE Entity_Name = 'Revised - Pending client approval' AND Entity_Type = 1006

	SELECT @revClientApprovedStatusId = entity_id FROM dbo.Entity(NOLOCK) WHERE Entity_Name = 'Revised - Client approved' AND Entity_Type = 1006	  

	SELECT @rfpLPStatusTypeId = status_type_id FROM dbo.sr_rfp_lp_account_summary WHERE sr_account_group_id = @accountGroupId
	  
	IF (@rfpLPStatusTypeId = @pendingClientApprovalStatusId)
	 BEGIN
	 
		SELECT @statusTypeId = @clientApprovedStatusId
		
	 END
	ELSE IF (@rfpLPStatusTypeId = @clientApprovedStatusId OR @rfpLPStatusTypeId = @revPendingClientApprovalStatusId)
	 BEGIN
	 
		SELECT @statusTypeId = @revClientApprovedStatusId
	 
	 END

	SELECT @entityId = ENTITY_ID FROM dbo.Entity (NOLOCK) WHERE Entity_Name = 'Client Load Profiles' AND Entity_Type = 100  
	  
	/* commented by Jaya  
	 INSERT INTO CBMS_IMAGE (CBMS_IMAGE_TYPE_ID, CBMS_DOC_ID, CBMS_IMAGE, CONTENT_TYPE, DATE_IMAGED)   
	 VALUES (@entityId, @cbmsImageDocId, @cbmsImage, @contentType, getDate())  
	  
	declare @cbmsImageId INT  
	SELECT @cbmsImageId = (SELECT scope_identity())  */  
	  
	--added by Jaya for updating entityid  
	   
	UPDATE dbo.CBMS_IMAGE SET CBMS_IMAGE_TYPE_ID = @entityId WHERE CBMS_IMAGE_ID = @cbmsImageId
	  
	--IF (SELECT COUNT(1) FROM dbo.SR_RFP_LP_CLIENT_APPROVAL WHERE SR_ACCOUNT_GROUP_ID = @accountGroupId AND IS_BID_GROUP = 0 AND IS_GROUP_LP = 0) > 0
	-- begin
	
	SET @DateNow = GETDATE()
	
	UPDATE dbo.SR_RFP_LP_CLIENT_APPROVAL
		SET CBMS_IMAGE_ID = @cbmsImageId,
			APPROVE_DATE = @dateUploaded,  
			SITE_ID = @siteId,    
			UPLOADED_BY = @userId,  
			UPLOADED_DATE = @DateNow
	WHERE SR_ACCOUNT_GROUP_ID = @accountGroupId
		AND IS_BID_GROUP = 0
		AND IS_GROUP_LP = 0
		
	IF @@ROWCOUNT = 0 -- If no records updated by the previous query
	 BEGIN
	 
		INSERT INTO dbo.SR_RFP_LP_CLIENT_APPROVAL(SR_ACCOUNT_GROUP_ID
			, IS_BID_GROUP
			, CBMS_IMAGE_ID
			, APPROVE_DATE
			, SITE_ID
			, IS_GROUP_LP
			, UPLOADED_BY
			, UPLOADED_DATE)
		VALUES( @accountGroupId
			, 0 
			, @cbmsImageId
			, @dateUploaded
			, @siteId
			, 0
			, @userId
			, @DateNow)
	 END
	 
	IF (@statusTypeId > 0)
	 BEGIN

		UPDATE dbo.SR_RFP_LP_ACCOUNT_SUMMARY
			SET status_type_id = @statusTypeId
		WHERE sr_account_group_id = @accountGroupId

	 END
	  
	UPDATE rfpCheckList
		SET rfpCheckList.LP_APPROVED_DATE = GETDATE()
	FROM dbo.SR_RFP_CHECKLIST rfpCheckList
		INNER JOIN dbo.SR_RFP_ACCOUNT rfpAcct ON rfpAcct.SR_RFP_ACCOUNT_ID = rfpCheckList.SR_RFP_ACCOUNT_ID
	WHERE rfpAcct.SR_RFP_ACCOUNT_ID = @accountGroupId
		AND rfpAcct.SR_RFP_ID = @rfpId

	--added by prasad.  
	SELECT @projectId = SSO_PROJECT_ID FROM dbo.RFP_SITE_PROJECT WHERE RFP_ID = @rfpId AND SITE_ID = @siteid
	  
	--if (SELECT count(SSO_PROJECT_STEP_ID) from SSO_PROJECT_STEP  WHERE SSO_PROJECT_ID = @projectId AND STEP_NO = 2 )= 0  
	 -- begin  
	UPDATE dbo.SSO_PROJECT_STEP
		SET IS_ACTIVE = 0
		, IS_COMPLETE = 1
	WHERE SSO_PROJECT_ID = @projectId  
		AND STEP_NO = 2
	  
	  --end  

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_UPLOAD_LP_APPROVAL_P] TO [CBMSApplication]
GO
