SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
  dbo.IP_Processing_Invoice_Del    
    
DESCRIPTION:    
    
    
INPUT PARAMETERS:    
 Name                      DataType  Default Description    
------------------------------------------------------------    
  Processing_Cu_Invoice_Id   INT    
  User_Info_ID               INT                 
    
OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
     
    
AUTHOR INITIALS:    
 Initials Name    
------------------------------------------------------------    
 SP         SRINIVASARAO PATCHAVA    
    
MODIFICATIONS    
    
 Initials Date  Modification    
------------------------------------------------------------    
 SP         16/10/2018   CREATED    
 SP         16/10/2018   updated - removed userId - we need to remove older entry irrespective of user.
 HG			2019-09-13	MAINT-9302, Removed the User_Info_id filter from the delete statement as decided to delete all the records added for that invoice irrespective of the user  
******/    
CREATE PROCEDURE [dbo].[IP_Processing_Invoice_Del]    
      (     
       @Processing_Cu_Invoice_Id INT    
      ,@User_Info_ID INT )
AS
BEGIN     
      SET NOCOUNT ON    
    
      DELETE FROM    
            IP_Processing_Invoice    
      WHERE    
            Processing_Cu_Invoice_Id = @Processing_Cu_Invoice_Id    
                
END    

GO
GRANT EXECUTE ON  [dbo].[IP_Processing_Invoice_Del] TO [CBMSApplication]
GO
