
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
		[dbo].[Sitegroup_Sel_By_Owner_User_Info_Id]

DESCRIPTION:


INPUT PARAMETERS:
	Name            DataType        Default    Description
------------------------------------------------------------
    @User_Info_Id	INT
      
OUTPUT PARAMETERS:
	Name            DataType        Default    Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC dbo.Sitegroup_Sel_By_Owner_User_Info_Id 38216
	EXEC dbo.Sitegroup_Sel_By_Owner_User_Info_Id 119
	EXEC dbo.Sitegroup_Sel_By_Owner_User_Info_Id 49

AUTHOR INITIALS:
    Initials    Name
------------------------------------------------------------
	RR			Raghu Reddy
	NR			Narayana Reddy
	
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RR			2015-01-05	Created MAINT-3304 Move User to History CBMS enhancement
	NR			2016-30-05	MAINT-3974 - Added Sitegroup_Id in Output List.

******/


CREATE PROCEDURE [dbo].[Sitegroup_Sel_By_Owner_User_Info_Id] ( @User_Info_Id AS INT )
AS 
BEGIN
    
      SET NOCOUNT ON;
    
      SELECT
            chsg.Client_Name
           ,chsg.Sitegroup_Name
           ,cd.Code_Value
           ,chsg.Sitegroup_Id
      FROM
            Core.Client_Hier chsg
            INNER JOIN dbo.Code cd
                  ON chsg.Sitegroup_Type_Cd = cd.Code_Id
      WHERE
            chsg.Site_Id = 0
            AND chsg.Sitegroup_Owner_User_id = @User_Info_Id
    
END;

;
GO

GRANT EXECUTE ON  [dbo].[Sitegroup_Sel_By_Owner_User_Info_Id] TO [CBMSApplication]
GO
