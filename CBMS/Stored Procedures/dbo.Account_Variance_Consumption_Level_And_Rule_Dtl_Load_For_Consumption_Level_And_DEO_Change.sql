SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/******

 NAME: dbo.Account_Variance_Consumption_Level_And_Rule_Dtl_Load_For_Consumption_Level_And_DEO_Change

 DESCRIPTION: 
	This stored procedure gets the consumption level ids mapped to an account in a comma seperated values and load Consumption Level and Variance Rule dtl based on DEO flag

	This stored procedure will be called in the following scenarios
		- Whenever consumption level is added to an account
		- Consumption level is changed for an account
		- New consumption level is added to an account
		- DEO flag is changed for an account

 INPUT PARAMETERS:
 Name								DataType			Default			Description
---------------------------------------------------------------------------------------------------------------
 Account_Id							INT									Account_Id of account being edited
 @Variance_Consumption_Level_Ids	VARCHAR(max)						CSV of all the consumption levels currently mapped to the account

 OUTPUT PARAMETERS:

 Name								DataType			Default			Description
---------------------------------------------------------------------------------------------------------------

 USAGE EXAMPLES:
---------------------------------------------------------------------------------------------------------------

BEGIN TRAN
	EXEC dbo.Account_Variance_Consumption_Level_And_Rule_Dtl_Load_For_Consumption_Level_And_DEO_Change
		  @Account_Id = 1148793
		 ,@Variance_Consumption_Level_Ids = '5'
ROLLBACK

BEGIN TRAN

	EXEC dbo.Account_Variance_Consumption_Level_And_Rule_Dtl_Load_For_Consumption_Level_And_DEO_Change
		  @Account_Id = 18859
		 ,@Variance_Consumption_Level_Ids = '4,9'

ROLLBACK

 AUTHOR INITIALS:
	Initials	Name
---------------------------------------------------------------------------------------------------------------
	HG			Harihara Suthan G
	RR			Raghu Reddy
	AP			Arunkumar Palanivel
 
 MODIFICATIONS
	Initials	Date		Modification
---------------------------------------------------------------------------------------------------------------
	HG			2013-12-17	Created to fix the variance rule dtl issue for ulitity account
	SP			2016-11-10	Variance Enhancement Phase II,Removed condition is_data_entry_only flag to load data into Variance_Rule_Dtl_Account_Override
	RR			11-29-2016	VTE-58/50 Modified to insert all tests if Is_Data_Entry_Only is false otherwise loads only DEO tests 
	Ap			Feb 13,2020	Modofied procedure to exclude the addition of variance rule detail account override for AVT 
******/
CREATE PROCEDURE [dbo].[Account_Variance_Consumption_Level_And_Rule_Dtl_Load_For_Consumption_Level_And_DEO_Change]
      ( 
       @Account_Id INT
      ,@Variance_Consumption_Level_Ids VARCHAR(MAX) )
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE @Account_Is_Data_Entry_Only BIT

      DECLARE @Variance_Consumption_Level TABLE
            ( 
             Variance_Consumption_Level_Id INT PRIMARY KEY CLUSTERED )

      DECLARE @Account_Variance_Consumption_Level_And_Rule_Dtl TABLE
            ( 
             Account_Id INT
            ,Variance_Consumption_Level_Id INT
            ,Variance_Rule_Dtl_Id INT
            ,PRIMARY KEY CLUSTERED ( Account_Id, Variance_Consumption_Level_Id, Variance_Rule_Dtl_Id ) )

      INSERT      INTO @Variance_Consumption_Level
                  ( 
                   Variance_Consumption_Level_Id )
                  SELECT
                        Segments
                  FROM
                        dbo.ufn_split(@Variance_Consumption_Level_Ids, ',')

      SELECT
            @Account_Is_Data_Entry_Only = ISNULL(Is_Data_Entry_Only, 0)
      FROM
            dbo.Account
      WHERE
            Account_Id = @Account_Id

      BEGIN TRY
            BEGIN TRAN

		-- Inserts the new consumption levels added the account and deletes the consumption level which are not belongs to the account

            INSERT      INTO dbo.Account_Variance_Consumption_Level
                        ( 
                         ACCOUNT_ID
                        ,Variance_Consumption_Level_Id )
                        SELECT
                              @Account_Id AS Account_Id
                             ,Variance_Consumption_Level_Id
                        FROM
                              @Variance_Consumption_Level vcl
                        WHERE
                              NOT EXISTS ( SELECT
                                                1
                                           FROM
                                                dbo.Account_Variance_Consumption_Level avcl
                                           WHERE
                                                avcl.ACCOUNT_ID = @Account_Id
                                                AND avcl.Variance_Consumption_Level_Id = vcl.Variance_Consumption_Level_Id )

            DELETE
                  avcl
            FROM
                  dbo.Account_Variance_Consumption_Level avcl
            WHERE
                  ACCOUNT_ID = @Account_id
                  AND NOT EXISTS ( SELECT
                                    1
                                   FROM
                                    @Variance_Consumption_Level vcl
                                   WHERE
                                    vcl.Variance_Consumption_Level_Id = avcl.Variance_Consumption_Level_Id )

	-- Get all the available consumption level along with the rules



            INSERT      INTO @Account_Variance_Consumption_Level_And_Rule_Dtl
                        ( 
                         Account_Id
                        ,Variance_Consumption_Level_Id
                        ,Variance_Rule_Dtl_Id )
                        SELECT
                              avcl.ACCOUNT_ID
                             ,avcl.Variance_Consumption_Level_Id
                             ,vrd.Variance_Rule_Dtl_Id
                        FROM
                              dbo.Account_Variance_Consumption_Level avcl
                              INNER JOIN dbo.Variance_Rule_Dtl vrd
                                    ON vrd.Variance_Consumption_Level_Id = avcl.Variance_Consumption_Level_Id
                        WHERE
                              avcl.ACCOUNT_ID = @Account_Id
                              AND vrd.IS_Active = 1
                              AND ( @Account_Is_Data_Entry_Only = 0
                                    OR ( @Account_Is_Data_Entry_Only = 1
                                         AND vrd.Is_Data_Entry_Only = 1 ) )


	-- Insert the rules in to rule_dtl if rules of the available consumption level entries are not present already



 
            INSERT      INTO dbo.Variance_Rule_Dtl_Account_Override
                        ( 
                         Variance_Rule_Dtl_Id
                        ,ACCOUNT_ID )
                        SELECT
                              avclrdtl.Variance_Rule_Dtl_Id
                             ,avclrdtl.Account_Id
                        FROM
                              @Account_Variance_Consumption_Level_And_Rule_Dtl avclrdtl
                        WHERE
                              NOT EXISTS ( SELECT
                                                1
                                           FROM
                                                dbo.Variance_Rule_Dtl_Account_Override exist_vrdao
                                           WHERE
                                                exist_vrdao.ACCOUNT_ID = avclrdtl.Account_Id
                                                AND exist_vrdao.Variance_Rule_Dtl_Id = avclrdtl.Variance_Rule_Dtl_Id )
							AND   NOT EXISTS
                                                            (     SELECT
                                                                        1
                                                                  FROM  Core.Client_Hier_Account cha
                                                                        JOIN
                                                                        dbo.Account_Variance_Consumption_Level avcl1
                                                                              ON avcl1.ACCOUNT_ID = cha.Account_Id
                                                                        JOIN
                                                                        dbo.Variance_Consumption_Level vcl
                                                                              ON vcl.Variance_Consumption_Level_Id = avcl1.Variance_Consumption_Level_Id
                                                                                 AND vcl.Commodity_Id = cha.Commodity_Id
                                                                        JOIN
                                                                        dbo.Bucket_Master bm
                                                                              ON bm.Commodity_Id = vcl.Commodity_Id
                                                                        JOIN
                                                                        dbo.Variance_category_Bucket_Map vcbm
                                                                              ON vcbm.Bucket_master_Id = bm.Bucket_Master_Id
                                                                        JOIN
                                                                        dbo.Variance_Parameter vp
                                                                              ON vp.Variance_Category_Cd = vcbm.Variance_Category_Cd
                                                                        JOIN
                                                                        dbo.Variance_Parameter_Baseline_Map vpbm
                                                                              ON vpbm.Variance_Parameter_Id = vp.Variance_Parameter_Id
                                                                        JOIN
                                                                        dbo.Variance_Rule vr
                                                                              ON vr.Variance_Baseline_Id = vpbm.Variance_Baseline_Id
                                                                        JOIN
                                                                        dbo.Variance_Rule_Dtl vrd1
                                                                              ON vrd1.Variance_Rule_Id = vr.Variance_Rule_Id
                                                                        JOIN
                                                                        dbo.Variance_Parameter_Commodity_Map vpcm
                                                                              ON vpcm.Variance_Parameter_Id = vp.Variance_Parameter_Id
                                                                        JOIN
                                                                        dbo.Variance_Processing_Engine vpe
                                                                              ON vpe.Variance_Processing_Engine_Id = vpcm.variance_process_Engine_id
                                                                        JOIN
                                                                        dbo.Code vpcat
                                                                              ON vpcat.Code_Id = vpe.Variance_Engine_Cd
                                                                        JOIN
                                                                        dbo.Variance_Consumption_Extraction_Service_Param_Value vesp
                                                                              ON vesp.Variance_Consumption_Level_Id = vcl.Variance_Consumption_Level_Id
                                                                  WHERE cha.Account_Id = avclrdtl.Account_Id
                                                                        AND   cha.Account_Type = 'Utility'
                                                                        AND   vpcat.Code_Value = 'Advanced Variance Test'
                                                                        AND   avclrdtl.Variance_Rule_Dtl_Id = vrd1.Variance_Rule_Dtl_Id
																		AND vcl.Variance_Consumption_Level_Id = avclrdtl.Variance_Consumption_Level_Id  );
 
												 
	-- Delete the rules from override if it doesn't belongs to the current consumption level and DEO rules

            DELETE
                  vrdao
            FROM
                  dbo.Variance_Rule_Dtl_Account_Override vrdao
            WHERE
                  vrdao.ACCOUNT_ID = @Account_Id
                  AND NOT EXISTS ( SELECT
                                    1
                                   FROM
                                    @Account_Variance_Consumption_Level_And_Rule_Dtl avclrdtl
                                   WHERE
                                    avclrdtl.Variance_Rule_Dtl_Id = vrdao.Variance_Rule_Dtl_Id )

            COMMIT TRAN
      END TRY
      BEGIN CATCH
            IF @@TRANCOUNT > 0 
                  BEGIN
                        ROLLBACK TRAN

                        EXEC dbo.usp_RethrowError

                  END
      END CATCH

END;


;
GO

GRANT EXECUTE ON  [dbo].[Account_Variance_Consumption_Level_And_Rule_Dtl_Load_For_Consumption_Level_And_DEO_Change] TO [CBMSApplication]
GO
