SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******

NAME: [DBO].[Upd_Commodity_Emission_Last_Change_User]

DESCRIPTION:

	Used to update the emission last_change_user and last change timestamp.

INPUT PARAMETERS:
	NAME								DATATYPE	DEFAULT		DESCRIPTION
-----------------------------------------------------------------------
	@Commodity_Id						INT
	@Emission_Last_Change_By_User_Id	INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION
-----------------------------------------------------------------------

USAGE EXAMPLES:
-----------------------------------------------------------------------
	SELECT GETDATE()

	BEGIN TRAN
		EXEC dbo.Upd_Commodity_Emission_Last_Change_User @Commodity_Id = 290
			,@Emission_Last_Change_By_User_Id = 16
	ROLLBACK TRAN

AUTHOR INITIALS:
	INITIALS	NAME
-----------------------------------------------------------------------
	HG			Harihara Suthan G

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
-----------------------------------------------------------------------
	HG			2013-04-22	Created for manage services requirement

*/

CREATE PROCEDURE dbo.Upd_Commodity_Emission_Last_Change_User
      (
       @Commodity_Id INT
      ,@Emission_Last_Change_By_User_Id INT )
AS
BEGIN

      SET NOCOUNT ON

      UPDATE
            com
      SET
            com.Emission_Last_Change_By_User_Id = @Emission_Last_Change_By_User_Id
           ,com.Emission_Last_Change_Ts = getdate()
      FROM
            dbo.Commodity com
      WHERE
            com.Commodity_Id = @Commodity_Id

END
;
GO
GRANT EXECUTE ON  [dbo].[Upd_Commodity_Emission_Last_Change_User] TO [CBMSApplication]
GO
