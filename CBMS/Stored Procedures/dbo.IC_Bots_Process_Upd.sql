SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          
            
 NAME: dbo.IC_Bots_Process_Upd			
            
 DESCRIPTION:            
    To Update the Bots Process names    
            
 INPUT PARAMETERS:            
           
 Name                               DataType            Default      Description            
---------------------------------------------------------------------------------------------------------------          
@IC_Bots_Process_Id					INT
@IC_Bots_Process_Name				VARCHAR(200)
@IC_Bots_Process_Desc				NVARCHAR(MAX)		NULL
@User_Info_Id						INT  
     
             
 OUTPUT PARAMETERS:           
                  
 Name                               DataType            Default      Description            
---------------------------------------------------------------------------------------------------------------          
            
 USAGE EXAMPLES:                
---------------------------------------------------------------------------------------------------------------          
BEGIN TRAN;

select  * from  dbo.IC_Bots_Process where   IC_Bots_Process_Id = 1;

EXEC IC_Bots_Process_Upd
    @IC_Bots_Process_Id = 1
    , @IC_Bots_Process_Name = 'Hellow Welcome Bots For IC'
    , @IC_Bots_Process_Desc = 'Small enahancement changes - Bots Process for IC'
    , @User_Info_Id = 49;

select  * from  dbo.IC_Bots_Process where   IC_Bots_Process_Id = 1;

ROLLBACK TRAN;

 AUTHOR INITIALS:          
            
 Initials               Name            
---------------------------------------------------------------------------------------------------------------          
 NR                     Narayana Reddy              
             
 MODIFICATIONS:           
           
 Initials               Date            Modification          
---------------------------------------------------------------------------------------------------------------          
 NR                     2020-05-27      Created for SE2017-963          
           
******/


CREATE PROC [dbo].[IC_Bots_Process_Upd]
     (
         @IC_Bots_Process_Id INT
         , @IC_Bots_Process_Name VARCHAR(200)
         , @IC_Bots_Process_Desc NVARCHAR(MAX) = NULL
         , @User_Info_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;


        UPDATE
            ibp
        SET
            ibp.IC_Bots_Process_Name = @IC_Bots_Process_Name
            , ibp.IC_Bots_Process_Desc = @IC_Bots_Process_Desc
            , ibp.Updated_User_Id = @User_Info_Id
            , ibp.Last_Change_Ts = GETDATE()
        FROM
            dbo.IC_Bots_Process ibp
        WHERE
            ibp.IC_Bots_Process_Id = @IC_Bots_Process_Id;



    END;

GO
GRANT EXECUTE ON  [dbo].[IC_Bots_Process_Upd] TO [CBMSApplication]
GO
