SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/******              
Name:   dbo.Consumption_Level_Specific_Configuration_Error_Metric_sel      
              
Description:              
			
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
  
                 
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
---------------------------------------------------------------------------------------- 
EXEC Consumption_Level_Specific_Configuration_Error_Metric_sel 1

    
      
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna
	AP				Arunkumar Palanivel
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
	Ap				feb 13,2020		Modified according to the new changes     
             
******/
CREATE PROCEDURE [dbo].[Consumption_Level_Specific_Configuration_Error_Metric_sel]
     
AS
      BEGIN
            SET NOCOUNT ON;

           	SELECT  c.Code_Id , c.Code_Value FROM code C 
	JOIN dbo.codeset cs
	ON cs.Codeset_Id = C.Codeset_Id 
	WHERE cs.Codeset_Name ='Variance Error Metric' 
      END;
GO
GRANT EXECUTE ON  [dbo].[Consumption_Level_Specific_Configuration_Error_Metric_sel] TO [CBMSApplication]
GO
