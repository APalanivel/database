SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[cbmsAccount_MakeNotManagedForClient]
	( @MyAccountId int
	, @client_id int
	)
AS
BEGIN

	insert into do_not_track
		( user_info_id, account_id, reason_type_id, account_number
		, client_name, client_city, state_id, vendor_name, date_added
		)
	   select distinct @MyAccountId user_info_id
		, a.account_id
		, 314
		, isNull(a.account_number, 'Unspecified Supplier Account')
		, cl.client_name
		, ad.city
		, ad.state_id
		, v.vendor_name
		, getdate()
	     from client cl
	     join division d on d.client_id = cl.client_id
	     join site s on s.division_id = d.division_id
	     join vwAccountMeter vam on vam.site_id = s.site_id
	     join account a on a.account_id = vam.account_id
	     join address ad on ad.address_id = s.primary_address_id
	     join vendor v on v.vendor_id = a.vendor_id
  left outer join do_not_track dnt on dnt.account_id = a.account_id
	    where cl.client_id = @client_id
	      and dnt.do_not_track_id is null


END






















GO
GRANT EXECUTE ON  [dbo].[cbmsAccount_MakeNotManagedForClient] TO [CBMSApplication]
GO
