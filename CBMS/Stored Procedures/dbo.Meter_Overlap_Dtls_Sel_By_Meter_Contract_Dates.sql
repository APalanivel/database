SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Meter_Overlap_Dtls_Sel_By_Meter_Contract_Dates]
           
DESCRIPTION:             
			To get map/unmap countries and SR pricing products
			
INPUT PARAMETERS:            
	Name					DataType		Default		Description  
---------------------------------------------------------------------------------  
    @Meter_Id				VARCHAR(MAX)
    @Contract_Start_Date	DATETIME
    @Contract_End_Date		DATETIME


OUTPUT PARAMETERS:
	Name			DataType		Default		Description  
---------------------------------------------------------------------------------  


 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT a.* FROM dbo.SUPPLIER_ACCOUNT_METER_MAP a JOIN dbo.CONTRACT b ON a.Contract_ID = b.CONTRACT_ID 
	WHERE b.CONTRACT_TYPE_ID = 153 AND a.METER_ID IN (232669,228359,228369)
	ORDER BY a.METER_ID,a.METER_ASSOCIATION_DATE,a.METER_DISASSOCIATION_DATE
		EXEC  dbo.Meter_Overlap_Dtls_Sel_By_Meter_Contract_Dates '232669,228359,228369','2015-01-01','2015-12-31'
		EXEC  dbo.Meter_Overlap_Dtls_Sel_By_Meter_Contract_Dates '232669,228359,228369','2026-01-01','2026-12-31'
		EXEC  dbo.Meter_Overlap_Dtls_Sel_By_Meter_Contract_Dates '232669,228359,228369','2009-01-01','2013-12-31'
		EXEC  dbo.Meter_Overlap_Dtls_Sel_By_Meter_Contract_Dates '232669,228359,228369','2009-01-01','2014-12-31'
	
	SELECT a.* FROM dbo.SUPPLIER_ACCOUNT_METER_MAP a JOIN dbo.CONTRACT b ON a.Contract_ID = b.CONTRACT_ID 
	WHERE b.CONTRACT_TYPE_ID = 153 AND a.METER_ID IN (861466,862955,861185)
	ORDER BY a.METER_ID,a.METER_ASSOCIATION_DATE,a.METER_DISASSOCIATION_DATE
		EXEC  dbo.Meter_Overlap_Dtls_Sel_By_Meter_Contract_Dates '861466,862955,861185','2015-01-01','2015-12-31'
		EXEC  dbo.Meter_Overlap_Dtls_Sel_By_Meter_Contract_Dates '861466,862955,861185','2016-01-01','2016-12-31'
		EXEC  dbo.Meter_Overlap_Dtls_Sel_By_Meter_Contract_Dates '861466,862955,861185','2017-01-01','2017-12-31'
		EXEC  dbo.Meter_Overlap_Dtls_Sel_By_Meter_Contract_Dates '861466,862955,861185','2020-01-01','2020-12-31'
		
		EXEC  dbo.Meter_Overlap_Dtls_Sel_By_Meter_Contract_Dates '232669,228359,228369','2015-01-01','2015-12-31'
		EXEC  dbo.Meter_Overlap_Dtls_Sel_By_Meter_Contract_Dates '232669,228359,228369','2015-01-01','2015-12-31',106194
		EXEC  dbo.Meter_Overlap_Dtls_Sel_By_Meter_Contract_Dates '232669,228359,228369','2009-01-01','2013-12-31'
		EXEC  dbo.Meter_Overlap_Dtls_Sel_By_Meter_Contract_Dates '232669,228359,228369','2009-01-01','2013-12-31',52547
		EXEC  dbo.Meter_Overlap_Dtls_Sel_By_Meter_Contract_Dates '232669,228359,228369','2009-01-01','2013-12-31',89659
		
		EXEC dbo.Meter_Overlap_Dtls_Sel_By_Meter_Contract_Dates '9241,9642,9643,9644','12/31/2015','01/04/2017',126480 
		EXEC dbo.Meter_Overlap_Dtls_Sel_By_Meter_Contract_Dates '9241,9642,9643,9644','01/04/2017','01/31/2022',126479 



 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2015-10-06	Global Sourcing - Phase2 -Created
							Applied same logic used in exisitng script dbo.CONTRACT_CHECK_FOR_METER_OVERLAPPING_P, this existing script just
							returns a flag if any one meter in the list is overlapping, in the current enhancement need to display each meter details
	RR			2015-11-04	Global Sourcing - Phase2 - Failed to validate overlap if simultaneous contracs end and start dates are matching,
							old contract end date = new contract start date, allows users to create contracts with one day overlap
	RR			2015-12-08	Global Sourcing - Phase2 - GCS-412, reverted above change to allow one day overlap							
******/

CREATE PROCEDURE [dbo].[Meter_Overlap_Dtls_Sel_By_Meter_Contract_Dates]
      ( 
       @Meter_Id VARCHAR(MAX)
      ,@Contract_Start_Date DATETIME
      ,@Contract_End_Date DATETIME
      ,@Contract_ID INT = NULL )
AS 
BEGIN

      SET NOCOUNT ON;

      DECLARE @Meters_List TABLE
            ( 
             Meter_Id INT PRIMARY KEY
            ,METER_NUMBER VARCHAR(50) )
      DECLARE @Contract_Type_ID INT

      INSERT      INTO @Meters_List
                  ( 
                   Meter_id
                  ,METER_NUMBER )
                  SELECT
                        Segments
                       ,cha.METER_NUMBER
                  FROM
                        dbo.ufn_split(@Meter_Id, ',')
                        INNER JOIN Core.Client_Hier_Account cha
                              ON Segments = cha.Meter_Id
                  GROUP BY
                        Segments
                       ,cha.METER_NUMBER
      
      
      SELECT
            @Contract_Type_ID = ENTITY_ID
      FROM
            dbo.Entity
      WHERE
            ENTITY_DESCRIPTION = 'Contract Type'
            AND ENTITY_NAME = 'Supplier';
      
      WITH  Cte_Overlapped_Mtrs
              AS ( SELECT
                        mtrs.Meter_Id
                       ,map.METER_ASSOCIATION_DATE
                       ,map.METER_DISASSOCIATION_DATE
                       ,map.Contract_ID
                       ,con.ED_CONTRACT_NUMBER
                   FROM
                        dbo.CONTRACT con
                        INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP AS map
                              ON con.CONTRACT_ID = map.CONTRACT_ID
                        INNER JOIN @Meters_List mtrs
                              ON mtrs.Meter_Id = map.Meter_Id
                   WHERE
                        con.contract_type_id = @Contract_Type_ID
                        AND ( ( ( @Contract_Start_Date > map.Meter_Association_Date
                                  AND @Contract_Start_Date < map.Meter_Disassociation_Date )
                                OR ( @Contract_End_Date > map.Meter_Association_Date
                                     AND @Contract_End_Date < map.Meter_Disassociation_Date ) )
                              OR ( ( map.Meter_Association_Date > @Contract_Start_Date
                                     AND map.Meter_Association_Date < @Contract_End_Date )
                                   OR ( map.Meter_Disassociation_Date > @Contract_Start_Date
                                        AND map.Meter_Disassociation_Date < @Contract_End_Date ) )
                              OR ( map.Meter_Association_Date = @Contract_Start_Date
                                   AND map.Meter_Disassociation_Date = @Contract_End_Date ) )
                        AND ( @Contract_ID IS NULL
                              OR con.CONTRACT_ID <> @Contract_ID ))
            SELECT
                  ml.Meter_Id
                 ,case WHEN replace(left(cons.Contracts, len(cons.Contracts) - 1), '&amp;', '&') IS NOT NULL THEN 1
                       ELSE 0
                  END AS Overlapping
                 ,replace(left(cons.Contracts, len(cons.Contracts) - 1), '&amp;', '&') AS Contracts
                 ,ml.METER_NUMBER
            FROM
                  @Meters_List ml
                  CROSS APPLY ( SELECT
                                    cast(com.ED_CONTRACT_NUMBER AS VARCHAR(20)) + '(' + convert(VARCHAR(10), com.METER_ASSOCIATION_DATE, 101) + ',' + convert(VARCHAR(10), com.METER_DISASSOCIATION_DATE, 101) + '),'
                                FROM
                                    Cte_Overlapped_Mtrs com
                                WHERE
                                    ml.Meter_Id = com.Meter_Id
                                GROUP BY
                                    com.Meter_Id
                                   ,com.METER_ASSOCIATION_DATE
                                   ,com.METER_DISASSOCIATION_DATE
                                   ,com.Contract_ID
                                   ,com.ED_CONTRACT_NUMBER
                  FOR
                                XML PATH('') ) cons ( Contracts )

	

END;


;
GO
GRANT EXECUTE ON  [dbo].[Meter_Overlap_Dtls_Sel_By_Meter_Contract_Dates] TO [CBMSApplication]
GO
