SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_NYMEX_FORECAST_PRICE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@fromDate      	datetime  	          	
	@toDate        	datetime  	          	
	@asOfDate      	datetime  	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE     PROCEDURE dbo.GET_NYMEX_FORECAST_PRICE_P

@userId varchar(10),
@sessionId varchar(20),
@fromDate  Datetime,
@toDate  Datetime,
@asOfDate Datetime


as
set nocount on
	select a.forecast_as_of_date, a.forecast_to_date,
	b.aggressive_price aggressive, b.conservative_price conservative,
	 b.moderate_price moderate, 
	b.month_identifier identifier
	from RM_FORECAST a, RM_FORECAST_DETAILS b
	where a.RM_FORECAST_ID=b.RM_FORECAST_ID and  a.RM_FORECAST_ID=
	(
		select RM_FORECAST_ID from RM_FORECAST where 
		forecast_as_of_date=@asOfDate
	)
	and @fromDate<=month_identifier and  @toDate>=month_identifier
GO
GRANT EXECUTE ON  [dbo].[GET_NYMEX_FORECAST_PRICE_P] TO [CBMSApplication]
GO
