SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--EXEC SR_RFP_GET_MIN_DUE_DATE_P 1,1

--SELECT * FROM SR_RFP_SEND_SUPPLIER

--select * from SR_RFP_SOP_SHORTLIST

CREATE   PROCEDURE [dbo].[SR_RFP_GET_MIN_DUE_DATE_P] 

@userId varchar,
@sessionId varchar

AS
	set nocount on
declare @expStatusId int
declare @procStatusId int

select @expStatusId=(select entity_id from entity where entity_name='Expired' and entity_type=1029)
select @procStatusId=(select entity_id from entity where entity_name='Processing' and entity_type=1029)


SELECT MIN(DUE_DATE) MIN_DUE_DATE

FROM

(
	SELECT 
		MIN(SR_RFP_SEND_SUPPLIER.DUE_DATE) DUE_DATE
	FROM 
		SR_RFP_SEND_SUPPLIER 
	where
		bid_status_type_id not in(@expStatusId,@procStatusId)

UNION

	SELECT 
		MIN(SR_RFP_SOP_SHORTLIST.REVISED_DUE_DATE) DUE_DATE
	FROM 
		SR_RFP_SOP_SHORTLIST 
) AS X
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_MIN_DUE_DATE_P] TO [CBMSApplication]
GO
