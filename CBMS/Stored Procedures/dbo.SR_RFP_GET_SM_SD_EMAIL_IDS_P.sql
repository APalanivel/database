SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--select * from user_info
--exec SR_RFP_GET_SM_SD_EMAIL_IDS_P 1023,1

CREATE   PROCEDURE [dbo].[SR_RFP_GET_SM_SD_EMAIL_IDS_P] 

@userid varchar(10),
@session_id varchar(20)

AS
	set nocount on
select dbo.SR_RFP_FN_GET_SM_SD_EMAIL_IDS(@userid) SM_SD_EMAIL_ADDRESSES
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_SM_SD_EMAIL_IDS_P] TO [CBMSApplication]
GO
