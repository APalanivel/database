SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Suppllier_Utility_Account_Commodity_Invoice_Recalc_Type_Sel_By_Account_Id

DESCRIPTION:

INPUT PARAMETERS:
	Name						DataType		Default			Description
-----------------------------------------------------------------------------------------
    @Account_Id					INT					
    @Commodity_Id				INT

OUTPUT PARAMETERS:
	Name						DataType		Default			Description
-----------------------------------------------------------------------------------------
	
USAGE EXAMPLES:
-----------------------------------------------------------------------------------------
    
EXEC Suppllier_Utility_Account_Commodity_Invoice_Recalc_Type_Sel_By_Account_Id @Account_Id = 1527279


AUTHOR INITIALS:
	Initials	Name
-----------------------------------------------------------------------------------------
	NR			Narayana Reddy	
	
	
MODIFICATIONS
	Initials	Date			Modification
-----------------------------------------------------------------------------------------
	NR       	2020-06-11		Created For MAINT-10422 - Common sproc to get the all recalcs for supplier& conslodaited & Utility.

******/
CREATE PROCEDURE [dbo].[Suppllier_Utility_Account_Commodity_Invoice_Recalc_Type_Sel_By_Account_Id]
    (
        @Account_Id INT
        , @Commodity_Id INT = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;



        DECLARE @Source_cd INT;

        SELECT
            @Source_cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Valcon Source Type'
            AND c.Code_Value = 'Contract';

        SELECT
            acirt.Account_Commodity_Invoice_Recalc_Type_Id
            , acirt.Account_Id
            , com.Commodity_Id
            , c.Code_Id
            , c.Code_Value
            , acirt.Start_Dt
            , acirt.End_Dt
            , acirt.Determinant_Source_Cd
        FROM
            dbo.Account_Commodity_Invoice_Recalc_Type acirt
            INNER JOIN dbo.Commodity com
                ON acirt.Commodity_ID = com.Commodity_Id
            INNER JOIN dbo.Code c
                ON acirt.Invoice_Recalc_Type_Cd = c.Code_Id
        WHERE
            acirt.Account_Id = @Account_Id
            AND (   @Commodity_Id IS NULL
                    OR  acirt.Commodity_ID = @Commodity_Id)
        UNION

        -------------------------------Supplier Account Contract Level ---------------------------------------------	
        SELECT
            acirt.Account_Commodity_Invoice_Recalc_Type_Id
            , acirt.Account_Id
            , com.Commodity_Id
            , c.Code_Id
            , c.Code_Value
            , acirt.Start_Dt
            , acirt.End_Dt
            , acirt.Determinant_Source_Cd
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN dbo.Contract_Recalc_Config crc
                ON crc.Contract_Id = cha.Supplier_Contract_ID
            INNER JOIN dbo.Account_Commodity_Invoice_Recalc_Type acirt
                ON acirt.Account_Id = cha.Account_Id
                   AND  crc.Start_Dt = acirt.Start_Dt
                   AND  ISNULL(crc.End_Dt, '9999-12-31') = ISNULL(acirt.End_Dt, '9999-12-31')
            INNER JOIN dbo.Commodity com
                ON acirt.Commodity_ID = com.Commodity_Id
            INNER JOIN dbo.Code c
                ON crc.Supplier_Recalc_Type_Cd = c.Code_Id
        WHERE
            acirt.Account_Id = @Account_Id
            AND acirt.Source_Cd = @Source_cd
            AND (   @Commodity_Id IS NULL
                    OR  acirt.Commodity_ID = @Commodity_Id)
            AND crc.Is_Consolidated_Contract_Config = 0

        -------------------------------Consolidated Account Contract Level ---------------------------------------------	
        UNION
        SELECT
            acirt.Account_Commodity_Invoice_Recalc_Type_Id
            , acirt.Account_Id
            , com.Commodity_Id
            , c.Code_Id
            , c.Code_Value
            , acirt.Start_Dt
            , acirt.End_Dt
            , acirt.Determinant_Source_Cd
        FROM
            Core.Client_Hier_Account u_cha
            INNER JOIN Core.Client_Hier_Account s_cha
                ON s_cha.Client_Hier_Id = u_cha.Client_Hier_Id
                   AND  s_cha.Meter_Id = u_cha.Meter_Id
            INNER JOIN dbo.Contract_Recalc_Config crc
                ON crc.Contract_Id = s_cha.Supplier_Contract_ID
            INNER JOIN dbo.Account_Commodity_Invoice_Recalc_Type acirt
                ON acirt.Account_Id = u_cha.Account_Id
                   AND  crc.Start_Dt = acirt.Start_Dt
                   AND  ISNULL(crc.End_Dt, '9999-12-31') = ISNULL(acirt.End_Dt, '9999-12-31')
            INNER JOIN dbo.Commodity com
                ON acirt.Commodity_ID = com.Commodity_Id
            INNER JOIN dbo.Code c
                ON crc.Supplier_Recalc_Type_Cd = c.Code_Id
        WHERE
            acirt.Account_Id = @Account_Id
            AND acirt.Source_Cd = @Source_cd
            AND (   @Commodity_Id IS NULL
                    OR  acirt.Commodity_ID = @Commodity_Id)
            AND crc.Is_Consolidated_Contract_Config = 1
            AND u_cha.Account_Type = 'utility'
            AND s_cha.Account_Type = 'supplier'
        ORDER BY
            acirt.Start_Dt;


    END;

GO
GRANT EXECUTE ON  [dbo].[Suppllier_Utility_Account_Commodity_Invoice_Recalc_Type_Sel_By_Account_Id] TO [CBMSApplication]
GO
