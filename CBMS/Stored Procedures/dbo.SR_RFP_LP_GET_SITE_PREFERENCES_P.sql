
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 dbo.Locale_Notification_Template_Sel_By_Notification_Type    
 
DESCRIPTION:  

INPUT PARAMETERS:    
Name							DataType		Default			Description    
------------------------------------------------------------------------------    
@user_id						VARCHAR(10)
@session_id						VARCHAR(20)
@sr_supplier_contact_info_id	INT			
					

OUTPUT PARAMETERS:    
Name							DataType		Default			Description    
-----------------------------------------------------------------------------    
 
USAGE EXAMPLES:    
------------------------------------------------------------------------------    
    
EXEC dbo.SR_RFP_LP_GET_SITE_PREFERENCES_P 0,0,14108


AUTHOR INITIALS:    
Initials	Name    
------------------------------------------------------------------------------    
NR			Narayana Reddy			

MODIFICATIONS:
Initials	Date		Modification    
------------------------------------------------------------------------------    
NR			2016-07-26	GCS Phase-5b -Added LP_CONTACT_LAST_NAME.
						GCS-1255 -Last Name preferred
NR			2016-08-04	MAINT-4203 - Added lp_contact_first_name,LP_CONTACT_LAST_NAME columns.			
					    
******/

CREATE PROCEDURE [dbo].[SR_RFP_LP_GET_SITE_PREFERENCES_P]
      ( 
       @user_id VARCHAR(10)
      ,@session_id VARCHAR(20)
      ,@site_id INT )
AS 
BEGIN

      SET NOCOUNT ON 
      
      
      SELECT
            cli.client_name
           ,vwSite.site_name
           ,sit.is_preference_by_email
           ,sit.is_preference_by_dv
           ,sit.lp_contact_email_address
           ,dbo.SR_RFP_LP_FN_GET_ALL_CC_EMAIL(@site_id) AS lp_contact_cc
           ,sit.lp_contact_first_name + ' ' + sit.LP_CONTACT_LAST_NAME AS lp_contact_first_name
           ,info.email_address
           ,info.first_name
           ,info.last_name
           ,info.phone_number
      FROM
            site sit
            INNER JOIN vwSiteName vwSite
                  ON vwSite.site_id = sit.site_id
            INNER JOIN division div
                  ON div.division_id = sit.division_id
            INNER JOIN client cli
                  ON cli.client_id = div.client_id
            INNER JOIN client_cem_map map
                  ON map.client_id = cli.client_id
            INNER JOIN user_info info
                  ON info.user_info_id = map.user_info_id
      WHERE
            sit.site_id = @site_id
           
      
END





;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_LP_GET_SITE_PREFERENCES_P] TO [CBMSApplication]
GO
