
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 CBMS.dbo.Cost_Usage_Account_Dtl_Merge  
  
DESCRIPTION:  
  
 Used to insert the data in to Cost_Usage_Account_Dtl table  
   
INPUT PARAMETERS:  
    Name    DataType      Default Description  
------------------------------------------------------------  
    @Bucket_Master_Id INT  
    @Bucket_Value  DECIMAL(28,10)  
    @UOM_Type_Id  INT  
    @CURRENCY_UNIT_ID INT  
    @Created_By_Id  INT  
    @ACCOUNT_ID  INT  
    @Service_Month  DATE 
    @Created_Ts          DATETIME      NULL  
    @Updated_Ts  DATETIME      NULL  
    @Data_Source_Cd  INT  
    @Client_Hier_Id      INT  
  
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
begin tran  
EXEC dbo.Cost_Usage_Account_Dtl_Merge 100029,22.00,null,3,49,158072,'2019-12-01',null,null,100350, 3661  
  
rollback tran  
  
select top 2 * from  Cost_Usage_Account_Dtl where Client_Hier_Id = 3661 and Account_Id = 158072 and Service_Month = '2019-12-01'   
  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 SSR  Sharad Srivastava  
 HG  Harihara Suthan G  
 BCH  Balaraju  
 AP  Athmaram Pabbathi 
 SP	 Sandeep Pigilam 
  
MODIFICATIONS  
 Initials Date     Modification  
------------------------------------------------------------  
 SSR       03/04/2010   Created  
 HG  03/18/2010   return of SCOPE_IDENTITY() value removed as application not required this.  
 BCH  2011-10-18   Added @Data_Source_Cd input parameter and to stored in Cost_Usage_Account_Dtl table  
 AP  2012-03-22   Addnl Data Changes --Added @Client_Hier_Id input parameter  
 AKR 2012-06-12   changed @Service_Month data type from Datetime to Date
 SP	 2017-06-13	  Data Estimation, Added @Data_Type as Input.
******/  
CREATE PROCEDURE [dbo].[Cost_Usage_Account_Dtl_Merge]
      ( 
       @Bucket_Master_Id INT
      ,@Bucket_Value DECIMAL(28, 10)
      ,@UOM_Type_Id INT
      ,@CURRENCY_UNIT_ID INT
      ,@Created_By_Id INT
      ,@ACCOUNT_ID INT
      ,@Service_Month DATE
      ,@Created_Ts DATETIME = NULL
      ,@Updated_Ts DATETIME = NULL
      ,@Data_Source_Cd INT
      ,@Client_Hier_Id INT
      ,@Data_Type VARCHAR(25) = 'Actual' )
AS 
BEGIN  
      SET NOCOUNT ON  
      DECLARE @Data_Type_Cd INT
	
      SELECT
            @Data_Type_Cd = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Std_Column_Name = 'Data_Type_Cd'
            AND cs.Codeset_Name = 'Data_Type'
            AND c.Code_Value = ISNULL(@Data_Type,'Actual')
      
  
      MERGE INTO dbo.Cost_Usage_Account_Dtl tr_cuad
            USING 
                  ( SELECT
                        @Bucket_Master_Id AS Bucket_Master_Id
                       ,@Bucket_Value AS Bucket_Value
                       ,@UOM_Type_Id AS UOM_Type_Id
                       ,@CURRENCY_UNIT_ID AS CURRENCY_UNIT_ID
                       ,@Created_By_Id AS Created_By_Id
                       ,ISNULL(@Created_Ts, GETDATE()) AS Created_Ts
                       ,ISNULL(@Updated_Ts, GETDATE()) AS Updated_Ts
                       ,@ACCOUNT_ID AS ACCOUNT_ID
                       ,@Service_Month AS Service_Month
                       ,@Data_Source_Cd AS Data_Source_Cd
                       ,@Client_Hier_Id AS Client_Hier_Id
                       ,@Data_Type_Cd AS Data_Type_Cd ) AS sr_cuad
            ON ( tr_cuad.Bucket_Master_Id = sr_cuad.Bucket_Master_Id
                 AND tr_cuad.ACCOUNT_ID = sr_cuad.ACCOUNT_ID
                 AND tr_cuad.Service_Month = sr_cuad.Service_Month
                 AND tr_cuad.Client_Hier_Id = sr_cuad.Client_Hier_Id )
            WHEN MATCHED 
                  THEN  
  UPDATE            SET 
                        tr_cuad.Bucket_Master_Id = sr_cuad.Bucket_Master_Id
                       ,tr_cuad.Bucket_Value = sr_cuad.Bucket_Value
                       ,tr_cuad.UOM_Type_Id = sr_cuad.UOM_Type_Id
                       ,tr_cuad.CURRENCY_UNIT_ID = sr_cuad.CURRENCY_UNIT_ID
                       ,tr_cuad.Updated_By_Id = sr_cuad.Created_By_Id
                       ,tr_cuad.Updated_Ts = sr_cuad.Updated_Ts
                       ,tr_cuad.ACCOUNT_ID = sr_cuad.ACCOUNT_ID
                       ,tr_cuad.Data_Source_Cd = sr_cuad.Data_Source_Cd
                       ,tr_cuad.Data_Type_Cd = sr_cuad.Data_Type_Cd
            WHEN NOT MATCHED 
                  THEN  
  INSERT
                        ( 
                         Bucket_Master_Id
                        ,Bucket_Value
                        ,UOM_Type_Id
                        ,CURRENCY_UNIT_ID
                        ,Created_By_Id
                        ,Created_Ts
                        ,Updated_Ts
                        ,ACCOUNT_ID
                        ,Service_Month
                        ,Data_Source_Cd
                        ,Client_Hier_Id
                        ,Data_Type_Cd )
                    VALUES
                        ( 
                         sr_cuad.Bucket_Master_Id
                        ,sr_cuad.Bucket_Value
                        ,sr_cuad.UOM_Type_Id
                        ,sr_cuad.CURRENCY_UNIT_ID
                        ,sr_cuad.Created_By_Id
                        ,sr_cuad.Created_Ts
                        ,sr_cuad.Updated_Ts
                        ,sr_cuad.ACCOUNT_ID
                        ,sr_cuad.Service_Month
                        ,sr_cuad.Data_Source_Cd
                        ,sr_cuad.Client_Hier_Id
                        ,sr_cuad.Data_Type_Cd );  
  
END;

;
GO



GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Dtl_Merge] TO [CBMSApplication]
GO
