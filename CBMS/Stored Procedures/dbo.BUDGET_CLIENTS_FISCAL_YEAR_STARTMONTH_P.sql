SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


--exec BUDGET_CLIENTS_FISCAL_YEAR_STARTMONTH_P -1,-1,10027
CREATE   PROCEDURE dbo.BUDGET_CLIENTS_FISCAL_YEAR_STARTMONTH_P
@user_id varchar(10),
@session_id varchar(20),
@clientId int

AS
begin
set nocount on

		select	
 		case	entity_name 
		when 	'January' then 1
		when 	'February'then 2
		when 	'March'	  then 3
		when 	'April'	  then 4
		when 	'May' 	  then 5
		when 	'June' 	  then 6
		when 	'July' 	  then 7
		when 	'August'   then 8
		when 	'September'then 9
		when 	'October'  then 10
		when 	'November' then 11
		when 	'December' then 12
		end fiscalStartMonth
        from     entity
        where    entity_id=(select   fiscalyear_startmonth_type_id 
                            from     client
                            where    client_id=@clientId)

	
				
end
GO
GRANT EXECUTE ON  [dbo].[BUDGET_CLIENTS_FISCAL_YEAR_STARTMONTH_P] TO [CBMSApplication]
GO
