SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[cbmsCuInvoiceComment_Get]
	( @MyAccountId int
	, @cu_invoice_comment_id int
	)
AS
BEGIN

	   select com.cu_invoice_comment_id
		, com.cu_invoice_id
		, com.comment_by_id
		, com.comment_date
		, com.image_comment
	     from cu_invoice_comment com with (nolock)
	    where com.cu_invoice_comment_id = @cu_invoice_comment_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCuInvoiceComment_Get] TO [CBMSApplication]
GO
