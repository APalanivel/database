SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.Check_Client_Service_Category_Presence_In_Level3_Category_By_Client_Service_Category_Id


DESCRIPTION: 

	Used to check if the level 2 service category added to any level 3 categories

INPUT PARAMETERS:
      Name					DataType          Default     Description
------------------------------------------------------------------ 
	  @Client_Service_Category_Id   int

OUTPUT PARAMETERS:
      Name              DataType          Default     Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
DECLARE @Is_Added BIT
EXEC Check_Client_Service_Category_Presence_In_Level3_Category_By_Client_Service_Category_Id 
	22,@Is_Added OUT
	
	SELECT @Is_Added

SELECT * FROM Client_service_Category_Commodity

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan G

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	HG			11/10/2011	Created

******/
CREATE PROCEDURE dbo.Check_Client_Service_Category_Presence_In_Level3_Category_By_Client_Service_Category_Id
      ( 
       @Client_Service_Category_Id INT
      ,@Is_Service_Category_Added_To_Level3 BIT OUT )
AS 
BEGIN

      SET NOCOUNT ON
	  
      SET @Is_Service_Category_Added_To_Level3 = 0

      IF EXISTS ( SELECT
                        1
                  FROM
                        dbo.Client_Service_Category_Commodity cc
                  WHERE
                        cc.Sub_Client_Service_Category_id = @Client_Service_Category_Id ) 
            BEGIN
                  SET @Is_Service_Category_Added_To_Level3 = 1	 
            END

END
GO
GRANT EXECUTE ON  [dbo].[Check_Client_Service_Category_Presence_In_Level3_Category_By_Client_Service_Category_Id] TO [CBMSApplication]
GO
