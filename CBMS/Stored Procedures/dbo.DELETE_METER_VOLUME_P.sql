SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:    dbo.DELETE_METER_VOLUME_P 
     
      
DESCRIPTION:     
     
      
INPUT PARAMETERS:      
      Name          DataType       Default        Description      
-----------------------------------------------------------------------------      
   
    
      
OUTPUT PARAMETERS:    
      
	Name     DataType   Default  Description      
-----------------------------------------------------------------------------      
      
      
      
USAGE EXAMPLES:      
-----------------------------------------------------------------------------      
   

 
AUTHOR INITIALS:       
	Initials    Name  
-----------------------------------------------------------------------------         
	RR			Raghu Reddy
      
MODIFICATIONS       
	Initials    Date		Modification        
-----------------------------------------------------------------------------         
	RR			2020-04-30	Added header
							GRM - Contract volumes enhancement - Modified to delete data from dbo.Contract_Meter_Volume_Dtl
******/
CREATE PROCEDURE [dbo].[DELETE_METER_VOLUME_P]
    @contract_id INT
    , @meter_id INT
AS
    BEGIN

        SET NOCOUNT ON;

        DELETE
        cmvd
        FROM
            dbo.Contract_Meter_Volume_Dtl cmvd
            INNER JOIN dbo.CONTRACT_METER_VOLUME cmv
                ON cmv.CONTRACT_METER_VOLUME_ID = cmvd.CONTRACT_METER_VOLUME_ID
        WHERE
            cmv.CONTRACT_ID = @contract_id
            AND cmv.METER_ID = @meter_id;

        DELETE  FROM
        dbo.CONTRACT_METER_VOLUME
        WHERE
            CONTRACT_ID = @contract_id
            AND METER_ID = @meter_id;

    END;

GO
GRANT EXECUTE ON  [dbo].[DELETE_METER_VOLUME_P] TO [CBMSApplication]
GO
