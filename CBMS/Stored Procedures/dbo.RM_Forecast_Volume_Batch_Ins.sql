SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        dbo.RM_Forecast_Volume_Batch_Ins                    
                      
Description:                      
        To load forecast volumes for on-boarded sites
                      
Input Parameters:                      
    Name							DataType        Default     Description                        
--------------------------------------------------------------------------------
	@RM_Client_Hier_Onboard_Id		INT
    @Batch_Status_Cd				INT
    @User_Info_Id					INT
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	  
                    
Usage Examples:                          
--------------------------------------------------------------------------------
	Begin tran 
	 exec [dbo].[RM_Forecast_Volume_Batch_Ins] NULL,1,NULL,NULL,NULL
	Rollback tran 
		
		                     
 Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy   
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			2018-09-27     Global Risk Management - Created 
                     
******/
CREATE  PROCEDURE [dbo].[RM_Forecast_Volume_Batch_Ins]
     (
         @RM_Client_Hier_Onboard_Id INT = NULL
         , @Batch_Status_Cd INT
         , @User_Info_Id INT = NULL
         , @Client_Hier_Id INT = NULL
         , @Commodity_Id INT = NULL
     )
AS
    BEGIN
        SET NOCOUNT ON;

        --EXEC dbo.CODE_SEL_BY_CodeSet_Name 
        --      @CodeSet_Name = 'Batch Status'--, @Code_Value =''

        DECLARE
            @Start_Dt DATE
            , @End_Dt DATE
            , @Openendadate_Forecast_End_Dt DATE
            , @RM_Forecast_Volume_Batch_Id INT
            , @New_RM_Client_Hier_Onboard_Id INT;

        DECLARE @Current_Dt DATE = CAST(GETDATE() AS DATE);

        SELECT
            @Openendadate_Forecast_End_Dt = DATEADD(
                                                dd, -DATEPART(dd, @Current_Dt) + 1
                                                , DATEADD(
                                                      mm, -DATEPART(mm, DATEADD(yy, 6, @Current_Dt))
                                                      , DATEADD(yy, 6, @Current_Dt)));

        --SELECT @Openendadate_Forecast_End_Dt

        SELECT
            @New_RM_Client_Hier_Onboard_Id = chob.RM_Client_Hier_Onboard_Id
        FROM
            Trade.RM_Client_Hier_Onboard chob
            INNER JOIN Core.Client_Hier chsites
                ON chob.Client_Hier_Id = chsites.Client_Hier_Id
        WHERE
            chsites.Site_Id > 0
            AND
            --( ( ( @Client_Hier_Id IS NULL
            --        OR 
            chsites.Client_Hier_Id = @Client_Hier_Id;   --)
        --AND ( @Commodity_Id IS NULL
        --      OR chob.Commodity_Id = @Commodity_Id ) 
        --        )
        --      OR ( @RM_Client_Hier_Onboard_Id IS NULL
        --           OR chob.RM_Client_Hier_Onboard_Id = @RM_Client_Hier_Onboard_Id ) )
        --AND ISNULL(@RM_Client_Hier_Onboard_Id, @Client_Hier_Id) IS NOT NULL

        SELECT
            @New_RM_Client_Hier_Onboard_Id = chob.RM_Client_Hier_Onboard_Id
        FROM
            Trade.RM_Client_Hier_Onboard chob
            INNER JOIN Core.Client_Hier ch
                ON chob.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN Core.Client_Hier chsites
                ON ch.Client_Id = chsites.Client_Id
                   AND  chob.Country_Id = chsites.Country_Id
        WHERE
            ch.Sitegroup_Id = 0
            AND chsites.Site_Id > 0
            AND
            --( ( ( @Client_Hier_Id IS NULL
            --OR 
            chsites.Client_Hier_Id = @Client_Hier_Id -- )
            --AND ( @Commodity_Id IS NULL
            --  OR chob.Commodity_Id = @Commodity_Id )
            --)
            --      OR ( @RM_Client_Hier_Onboard_Id IS NULL
            --           OR chob.RM_Client_Hier_Onboard_Id = @RM_Client_Hier_Onboard_Id ) )
            --AND ISNULL(@RM_Client_Hier_Onboard_Id, @Client_Hier_Id) IS NOT NULL
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.RM_Client_Hier_Onboard siteonb
                               WHERE
                                    chsites.Client_Hier_Id = siteonb.Client_Hier_Id
                                    AND siteonb.Commodity_Id = chob.Commodity_Id);

        SELECT
            @RM_Client_Hier_Onboard_Id = ISNULL(@New_RM_Client_Hier_Onboard_Id, @RM_Client_Hier_Onboard_Id);

        SELECT
            @User_Info_Id = ISNULL(@User_Info_Id, Updated_User_Id)
        FROM
            Trade.RM_Client_Hier_Onboard
        WHERE
            RM_Client_Hier_Onboard_Id = @RM_Client_Hier_Onboard_Id;


        SELECT
            @Start_Dt = MIN(chhc.Config_Start_Dt)
            , @End_Dt = NULLIF(MAX(chhc.Config_End_Dt), '9999-12-31')
        FROM
            Trade.RM_Client_Hier_Hedge_Config chhc
            INNER JOIN dbo.ENTITY hdg
                ON chhc.Hedge_Type_Id = hdg.ENTITY_ID
        WHERE
            chhc.RM_Client_Hier_Onboard_Id = @RM_Client_Hier_Onboard_Id
            AND hdg.ENTITY_NAME <> 'Does Not Hedge'
            AND hdg.ENTITY_DESCRIPTION = 'HEDGE_TYPE';


        INSERT INTO Trade.RM_Forecast_Volume_Batch
             (
                 RM_Client_Hier_Onboard_Id
                 , Forecast_Start_Dt
                 , Forecast_End_Dt
                 , Current_Step
                 , Current_Step_Status_Cd
                 , Batch_Status_Cd
                 , Requested_User_Info_Id
                 , Created_Ts
                 , Last_Change_Ts
             )
        SELECT
            @RM_Client_Hier_Onboard_Id
            , @Start_Dt
            , ISNULL(@End_Dt, @Openendadate_Forecast_End_Dt)
            , 'Pending'
            , -1
            , @Batch_Status_Cd
            , @User_Info_Id
            , GETDATE()
            , GETDATE()
        WHERE
            @Start_Dt IS NOT NULL
            AND ISNULL(@RM_Client_Hier_Onboard_Id, @Client_Hier_Id) IS NOT NULL;

        SELECT  @RM_Forecast_Volume_Batch_Id = SCOPE_IDENTITY();

        SELECT  @RM_Forecast_Volume_Batch_Id AS RM_Forecast_Volume_Batch_Id;


    END;

GO
GRANT EXECUTE ON  [dbo].[RM_Forecast_Volume_Batch_Ins] TO [CBMSApplication]
GO
