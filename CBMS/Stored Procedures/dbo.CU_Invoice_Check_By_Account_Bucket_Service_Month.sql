SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME: 

	dbo.CU_Invoice_Check_By_Account_Bucket_Service_Month 

DESCRIPTION:

Used to Check the invoice and manual data presence by given account id and service month
This procedure will be called to know the what data source code should be updated in Site level or for the given account.

INPUT PARAMETERS:
      Name			DataType          Default     Description
------------------------------------------------------------------
      @Account_Id		INT
      @Service_Month	DATETIME
      @Bucket_Master_Id	INT

OUTPUT PARAMETERS:
      Name                DataType          Default     Description        
------------------------------------------------------------

    
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.CU_Invoice_Check_By_Account_Bucket_Service_Month  1, '2005-09-01', 100945
	EXEC dbo.CU_Invoice_Check_By_Account_Bucket_Service_Month  1, '2005-01-01', 100945

	SELECT TOP 10 * FROM Cost_usage WHERE EL_DATA_SOURCE_CD = 100348
	
	SELECT* FROM Code WHERE CodeSet_id = 119
    SELECT * FROM Bucket_Master WHERE Commodity_Id = 290

AUTHOR INITIALS:
 Initials	Name
------------------------------------------------------------
 AP		Athmaram Pabbathi

MODIFICATIONS
 Initials	Date		    Modification
------------------------------------------------------------
 AP		2012-01-31    Created

******/

CREATE PROCEDURE dbo.CU_Invoice_Check_By_Account_Bucket_Service_Month
      @Account_Id INT
     ,@Service_Month DATETIME
     ,@Bucket_Master_Id INT
AS 
BEGIN  
  
      SET NOCOUNT ON ;  
  
      DECLARE
            @Is_Invoice_Exist BIT = 0
           ,@Commodity_Id INT
      
      SELECT
            @Commodity_Id = bm.Commodity_Id
      FROM
            dbo.Bucket_Master bm
      WHERE
            bm.Bucket_Master_Id = @Bucket_Master_Id

      IF EXISTS ( SELECT
                        1
                  FROM
                        dbo.CU_INVOICE cui
                        JOIN dbo.CU_INVOICE_SERVICE_MONTH cusm
                              ON cui.CU_INVOICE_ID = cusm.CU_INVOICE_ID
                        JOIN dbo.CU_INVOICE_DETERMINANT cuid
                              ON cuid.CU_INVOICE_ID = cui.CU_INVOICE_ID
                  WHERE
                        cusm.ACCOUNT_ID = @Account_id
                        AND cusm.SERVICE_MONTH = @Service_Month
                        AND cuid.COMMODITY_TYPE_ID = @Commodity_Id
                        AND cui.IS_PROCESSED = 1
                        AND cui.IS_REPORTED = 1
                        AND cui.IS_DNT = 0
                        AND cui.IS_DUPLICATE = 0 )
            OR EXISTS ( SELECT
                              1
                        FROM
                              dbo.CU_INVOICE cui
                              JOIN dbo.CU_INVOICE_SERVICE_MONTH cusm
                                    ON cui.CU_INVOICE_ID = cusm.CU_INVOICE_ID
                              JOIN dbo.CU_Invoice_Charge cuic
                                    ON cuic.CU_INVOICE_ID = cui.CU_INVOICE_ID
                        WHERE
                              cusm.ACCOUNT_ID = @Account_id
                              AND cusm.SERVICE_MONTH = @Service_Month
                              AND cuic.COMMODITY_TYPE_ID = @Commodity_Id
                              AND cui.IS_PROCESSED = 1
                              AND cui.IS_REPORTED = 1
                              AND cui.IS_DNT = 0
                              AND cui.IS_DUPLICATE = 0 ) 
            BEGIN
                  SET @Is_Invoice_Exist = 1
            END

      
      SELECT
            @Is_Invoice_Exist Is_Invoice_Exist

END
GO
GRANT EXECUTE ON  [dbo].[CU_Invoice_Check_By_Account_Bucket_Service_Month] TO [CBMSApplication]
GO
