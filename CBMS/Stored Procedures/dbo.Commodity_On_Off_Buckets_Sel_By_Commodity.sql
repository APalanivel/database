SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
   dbo.Commodity_On_Off_Buckets_Sel_By_Commodity  
  
DESCRIPTION:    
		
  
INPUT PARAMETERS:    
	Name					DataType			Default			Description    
-------------------------------------------------------------------------------- 
	@User_Info_Id			INT

OUTPUT PARAMETERS:    
	Name				DataType		Default			Description    
--------------------------------------------------------------------------------  
	@Commodity_Id		INT
    
USAGE EXAMPLES:    
--------------------------------------------------------------------------------  

	EXEC dbo.Commodity_On_Off_Buckets_Sel_By_Commodity 290
	EXEC dbo.Commodity_On_Off_Buckets_Sel_By_Commodity 291
	EXEC dbo.Commodity_On_Off_Buckets_Sel_By_Commodity 67

AUTHOR INITIALS:    
	Initials	Name    
--------------------------------------------------------------------------------  
	RR			Raghu Reddy  
  
MODIFICATIONS     
	Initials	Date			Modification    
--------------------------------------------------------------------------------  
	RR			2020-03-31		GRM-US EP & Renewables - Created

******/

CREATE PROCEDURE [dbo].[Commodity_On_Off_Buckets_Sel_By_Commodity]
    (
        @Commodity_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            bm.Commodity_Id
            , bm.Bucket_Master_Id
            , CASE WHEN bm.Bucket_Name = 'On-Peak Usage' THEN 'On Peak'
                  WHEN bm.Bucket_Name = 'Off-Peak Usage' THEN 'Off Peak'
              END AS Contract_Bucket
        FROM
            dbo.Bucket_Master bm
        WHERE
            bm.Commodity_Id = @Commodity_Id
            AND bm.Bucket_Name IN ( 'On-Peak Usage', 'Off-Peak Usage' );

    END;

GO
GRANT EXECUTE ON  [dbo].[Commodity_On_Off_Buckets_Sel_By_Commodity] TO [CBMSApplication]
GO
