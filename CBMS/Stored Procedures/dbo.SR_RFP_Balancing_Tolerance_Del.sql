SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[SR_RFP_Balancing_Tolerance_Del]  
     
DESCRIPTION: 
	It Deletes SR RFP Balance Tolerance for Selected 
						SR RFP Balance Tolerance Id.
      
INPUT PARAMETERS:          
NAME							DATATYPE	DEFAULT		DESCRIPTION          
--------------------------------------------------------------------
@@SR_RFP_Balancing_Tolerance_Id	INT	

   
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------
  
	Begin Tran
		EXEC SR_RFP_Balancing_Tolerance_Del  744
	Rollback Tran
    
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR			PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR		    31-MAY-10	CREATED     

*/  

CREATE PROCEDURE dbo.SR_RFP_Balancing_Tolerance_Del
   (
    @SR_RFP_Balancing_Tolerance_Id INT
   )
AS 
BEGIN

    SET NOCOUNT ON;
   
    DELETE
   	FROM
		dbo.SR_RFP_BALANCING_TOLERANCE
	WHERE
		SR_RFP_BALANCING_TOLERANCE_ID = @SR_RFP_Balancing_Tolerance_Id

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_Balancing_Tolerance_Del] TO [CBMSApplication]
GO
