SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        dbo.RM_Deal_Ticket_Client_Workflow_Sel                      
                        
Description:                        
        To get deal ticket workflows  
                        
Input Parameters:                        
    Name       DataType        Default     Description                          
--------------------------------------------------------------------------------  
   
                        
 Output Parameters:                              
 Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
     
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
   
  EXEC dbo.RM_Deal_Ticket_Client_Workflow_Sel 11236,291,586,'162277'  
  EXEC dbo.RM_Deal_Ticket_Client_Workflow_Sel 11236,291,587,'162277'  
  EXEC dbo.RM_Deal_Ticket_Client_Workflow_Sel 11236,291,587,NULL,'41270'  
    
  EXEC dbo.RM_Deal_Ticket_Client_Workflow_Sel  @Client_Id = 11236,@Commodity_Id = 291,@Hedge_Type=586,@Contract_Id = '148361'  
  EXEC dbo.RM_Deal_Ticket_Client_Workflow_Sel  @Client_Id = 11236,@Commodity_Id = 291,@Hedge_Type=586,@Site_Str = '24401,24415'  
  EXEC dbo.RM_Deal_Ticket_Client_Workflow_Sel  @Client_Id = 11236,@Commodity_Id = 291,@Hedge_Type=586  
   ,@Site_Str = '24401,24415',@RM_Group_Id='730',@Division_Id='1928'  
    
    
    
                         
 Author Initials:                        
    Initials    Name                        
--------------------------------------------------------------------------------  
    RR          Raghu Reddy     
                         
 Modifications:                        
    Initials Date           Modification                        
--------------------------------------------------------------------------------  
    RR   2018-11-15     Global Risk Management - Created   
                       
******/
CREATE PROCEDURE [dbo].[RM_Deal_Ticket_Client_Workflow_Sel]
(
    @Client_Id INT,
    @Commodity_Id INT,
    @Hedge_Type INT,
    @Contract_Id VARCHAR(MAX) = NULL,
    @Site_Str VARCHAR(MAX) = NULL,
    @Division_Id VARCHAR(MAX) = NULL,
    @RM_Group_Id VARCHAR(MAX) = NULL
)
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @Client_Client_Hier_Id INT;

    SELECT @Client_Client_Hier_Id = Client_Hier_Id
    FROM Core.Client_Hier
    WHERE Client_Id = @Client_Id
          AND Sitegroup_Id = 0;

    CREATE TABLE #Configs
    (
        RM_Client_Hier_Onboard_Id INT
    );


    CREATE TABLE #RM_Group_Sites
    (
        [Site_Id] INT
    );

    INSERT INTO #RM_Group_Sites
    (
        Site_Id
    )
    SELECT ch.Site_Id
    FROM dbo.Cbms_Sitegroup_Participant rgd
        INNER JOIN dbo.Code grp
            ON grp.Code_Id = rgd.Group_Participant_Type_Cd
        INNER JOIN dbo.CONTRACT c
            ON c.CONTRACT_ID = rgd.Group_Participant_Id
        INNER JOIN Core.Client_Hier_Account chasupp
            ON chasupp.Supplier_Contract_ID = c.CONTRACT_ID
        INNER JOIN Core.Client_Hier_Account chautil
            ON chasupp.Meter_Id = chautil.Meter_Id
        INNER JOIN Core.Client_Hier ch
            ON ch.Client_Hier_Id = chasupp.Client_Hier_Id
    WHERE (
              EXISTS
    (
        SELECT 1
        FROM dbo.ufn_split(@RM_Group_Id, ',') con
        WHERE rgd.Cbms_Sitegroup_Id = CAST(Segments AS INT)
    )
              AND grp.Code_Value = 'Contract'
              AND chasupp.Account_Type = 'Supplier'
              AND chautil.Account_Type = 'Utility'
          );

    INSERT INTO #RM_Group_Sites
    (
        Site_Id
    )
    SELECT ch.Site_Id
    FROM dbo.Cbms_Sitegroup_Participant rgd
        INNER JOIN dbo.Code grp
            ON grp.Code_Id = rgd.Group_Participant_Type_Cd
        INNER JOIN Core.Client_Hier ch
            ON ch.Site_Id = rgd.Group_Participant_Id
    WHERE (
              EXISTS
    (
        SELECT 1
        FROM dbo.ufn_split(@RM_Group_Id, ',') con
        WHERE rgd.Cbms_Sitegroup_Id = CAST(Segments AS INT)
    )
              AND grp.Code_Value = 'Site'
          );

    INSERT INTO #RM_Group_Sites
    (
        Site_Id
    )
    SELECT CAST(Segments AS INT)
    FROM dbo.ufn_split(@Site_Str, ',') con;

    INSERT INTO #RM_Group_Sites
    (
        Site_Id
    )
    SELECT ch.Site_Id
    FROM Core.Client_Hier ch
    WHERE ch.Site_Id > 0
          AND (EXISTS
    (
        SELECT 1
        FROM dbo.ufn_split(@Division_Id, ',') con
        WHERE ch.Sitegroup_Id = CAST(Segments AS INT)
    )
              );


    INSERT INTO #Configs
    (
        RM_Client_Hier_Onboard_Id
    )
    SELECT chob.RM_Client_Hier_Onboard_Id
    FROM Core.Client_Hier ch
        INNER JOIN Core.Client_Hier chclient
            ON ch.Client_Id = chclient.Client_Id
        INNER JOIN Trade.RM_Client_Hier_Onboard chob
            ON chclient.Client_Hier_Id = chob.Client_Hier_Id
               AND ch.Country_Id = chob.Country_Id
    WHERE ch.Site_Id > 0
          AND ch.Client_Id = @Client_Id
          AND chclient.Sitegroup_Id = 0
          AND chob.Commodity_Id = @Commodity_Id
          AND
          (
              EXISTS
    (
        SELECT 1 FROM #RM_Group_Sites rgs WHERE rgs.Site_Id = ch.Site_Id
    )
              OR EXISTS
    (
        SELECT 1
        FROM Core.Client_Hier_Account cha
            INNER JOIN dbo.ufn_split(@Contract_Id, ',') con
                ON cha.Supplier_Contract_ID = CAST(Segments AS INT)
        WHERE cha.Client_Hier_Id = ch.Client_Hier_Id
    )
          );
    --AND ( @Site_Str IS NULL  
    --      OR EXISTS ( SELECT  
    --                        1  
    --                  FROM  
    --                        dbo.ufn_split(@Site_Str, ',')  
    --                  WHERE  
    --                        CAST(Segments AS INT) = ch.Site_Id ) )  
    --AND ( @Division_Id IS NULL  
    --      OR EXISTS ( SELECT  
    --                        1  
    --                  FROM  
    --                        dbo.ufn_split(@Division_Id, ',')  
    --                  WHERE  
    --                        CAST(Segments AS INT) = ch.Sitegroup_Id ) )  
    --AND ( @RM_Group_Id IS NULL  
    --      OR EXISTS ( SELECT  
    --                        1  
    --                  FROM  
    --                        #RM_Group_Sites rgs  
    --                  WHERE  
    --                        rgs.Site_Id = ch.Site_Id ) )  

    SELECT rcw.RM_Client_Workflow_Id,
           chob.Country_Id,
           con.COUNTRY_NAME,
           chob.Commodity_Id,
           com.Commodity_Name,
           rcw.Hedge_Type_Id,
           hdg.ENTITY_NAME AS Hedge_Type,
           rcw.Workflow_Id,
           wf.Workflow_Name,
           wf.Workflow_Description,
           ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Last_Updated_By,
           rcw.Last_Change_Ts
    FROM dbo.RM_Client_Workflow rcw
        INNER JOIN dbo.Workflow wf
            ON rcw.Workflow_Id = wf.Workflow_Id
        INNER JOIN Trade.RM_Client_Hier_Onboard chob
            ON rcw.RM_Client_Hier_Onboard_Id = chob.RM_Client_Hier_Onboard_Id
        INNER JOIN dbo.COUNTRY con
            ON chob.Country_Id = con.COUNTRY_ID
        INNER JOIN dbo.Commodity com
            ON chob.Commodity_Id = com.Commodity_Id
        INNER JOIN dbo.ENTITY hdg
            ON rcw.Hedge_Type_Id = hdg.ENTITY_ID
        INNER JOIN dbo.USER_INFO ui
            ON rcw.Updated_User_Id = ui.USER_INFO_ID
    WHERE chob.Client_Hier_Id = @Client_Client_Hier_Id
          AND rcw.Hedge_Type_Id = @Hedge_Type
          AND EXISTS
    (
        SELECT 1
        FROM #Configs onbid
        WHERE onbid.RM_Client_Hier_Onboard_Id = rcw.RM_Client_Hier_Onboard_Id
    );



END;

GO
GRANT EXECUTE ON  [dbo].[RM_Deal_Ticket_Client_Workflow_Sel] TO [CBMSApplication]
GO
