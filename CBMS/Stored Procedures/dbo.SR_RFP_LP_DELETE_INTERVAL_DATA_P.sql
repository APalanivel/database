SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	cbms_prod.dbo.SR_RFP_LP_DELETE_INTERVAL_DATA_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@user_id       	varchar(10)	          	
	@session_id    	varchar(20)	          	
	@sr_rfp_lp_interval_data_id	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DMR			Deana Ritter
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	       	7/20/2009	Autogenerated script
	       	08/09/2009	Removed linked server updates for SV
	       	
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_LP_DELETE_INTERVAL_DATA_P
	@user_id VARCHAR(10),
	@session_id VARCHAR(20),
	@sr_rfp_lp_interval_data_id INT
AS

BEGIN

	SET NOCOUNT ON
	
	DECLARE @Cbms_Image_Id INT

	SELECT @Cbms_Image_Id = Cbms_Image_Id FROM dbo.Sr_Rfp_Lp_Interval_Data (NOLOCK)
	WHERE Sr_Rfp_Lp_Interval_Data_Id = @Sr_Rfp_Lp_Interval_Data_Id

	DELETE dbo.Sr_Rfp_Lp_Interval_Data WHERE Sr_Rfp_Lp_Interval_Data_Id = @Sr_Rfp_Lp_Interval_Data_Id

	

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_LP_DELETE_INTERVAL_DATA_P] TO [CBMSApplication]
GO
