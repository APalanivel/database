SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********  
NAME:  dbo.TOU_Schedule_Term_Del  
  
DESCRIPTION:  
  
 Used to Delete Time of Usage Schedule Term information for selected TOU Term Id from all the child tables  
 and term table  
  
INPUT PARAMETERS:
      Name								DataType          Default     Description
------------------------------------------------------------
	  @Time_Of_Use_Schedule_Term_Id		INT
	  @User_Info_Id						INT
	  
OUTPUT PARAMETERS:
      Name								DataType          Default     Description
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
  
 select * FROM dbo.Time_Of_Use_Schedule_Term where Time_Of_Use_Schedule_Term_Id=251
 BEGIN TRAN  
  Exec dbo.TOU_Schedule_Term_Del 251,49
 select * FROM dbo.Time_Of_Use_Schedule_Term where Time_Of_Use_Schedule_Term_Id=251  
 ROLLBACK TRAN  
   
  
  
AUTHOR INITIALS:  
	Initials Name
------------------------------------------------------------
	BCH		Balaraju

	Initials Date		Modification
------------------------------------------------------------
	BCH		2012-07-09  Created
	
	
******/  
CREATE PROCEDURE dbo.TOU_Schedule_Term_Del
( 
 @Time_Of_Use_Schedule_Term_Id INT
,@User_Info_Id INT )
AS 
BEGIN  
      SET NOCOUNT ON ;  
     
      DECLARE
            @Season_Parent_Type_Id INT
           ,@LookUp_Value XML
           ,@User_Name VARCHAR(100)
           ,@Client_Name VARCHAR(200)
           ,@Current_Ts DATETIME  
     
      SELECT
            @Season_Parent_Type_Id = ENTITY_ID
      FROM
            dbo.ENTITY
      WHERE
            Entity_Name = 'Time Of Use Schedule Term'
            AND ENTITY_TYPE = 120  
  
      SELECT
            @User_Name = FIRST_NAME + space(1) + LAST_NAME
      FROM
            dbo.USER_INFO UI
      WHERE
            UI.USER_INFO_ID = @User_Info_Id
  
      BEGIN TRY  
            BEGIN TRAN  
              
            DELETE
                  TOUSTPD
            FROM
                  dbo.Time_Of_Use_Schedule_Term_Peak_Dtl TOUSTPD
                  JOIN dbo.Time_Of_Use_Schedule_Term_Peak TOUSTP
                        ON TOUSTPD.Time_Of_Use_Schedule_Term_Peak_Id = TOUSTP.Time_Of_Use_Schedule_Term_Peak_Id
            WHERE
                  TOUSTP.Time_Of_Use_Schedule_Term_Id = @Time_Of_Use_Schedule_Term_Id  
                  
              
            DELETE FROM
                  dbo.Time_Of_Use_Schedule_Term_Peak
            WHERE
                  Time_Of_Use_Schedule_Term_Id = @Time_Of_Use_Schedule_Term_Id  
   
            DELETE FROM
                  dbo.SEASON
            WHERE
                  SEASON_PARENT_ID = @Time_Of_Use_Schedule_Term_Id
                  AND SEASON_PARENT_TYPE_ID = @Season_Parent_Type_Id  
   
            DELETE FROM
                  dbo.Time_Of_Use_Schedule_Term_Holiday
            WHERE
                  Time_Of_Use_Schedule_Term_Id = @Time_Of_Use_Schedule_Term_Id  
  
            SELECT
                  @LookUp_Value = ( SELECT
                                          Time_Of_Use_Schedule_Id
                                         ,Start_Dt
                                         ,End_Dt
                                    FROM
                                          dbo.Time_Of_Use_Schedule_Term
                                    WHERE
                                          Time_Of_Use_Schedule_Term_Id = @Time_Of_Use_Schedule_Term_Id
                  FOR
                                    XML AUTO )  
  
            DELETE FROM
                  dbo.Time_Of_Use_Schedule_Term
            WHERE
                  Time_Of_Use_Schedule_Term_Id = @Time_Of_Use_Schedule_Term_Id  
  
            SELECT
                  @Current_Ts = getdate()  
       
            EXEC dbo.Application_Audit_Log_Ins NULL, 'Delete Time Of Use Schedule Term', -1, 'Time_Of_Use_Schedule_Term', @Lookup_Value, @User_Name, @Current_Ts    
  
            COMMIT TRAN  
      END TRY  
      BEGIN CATCH  
            IF @@TRANCOUNT > 0 
                  ROLLBACK TRAN  
            EXEC dbo.usp_RethrowError  
      END CATCH  
END  

;
GO
GRANT EXECUTE ON  [dbo].[TOU_Schedule_Term_Del] TO [CBMSApplication]
GO
