SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******      
NAME:      
su.Client_GWP_Factor_Ins_By_Default_Values    
  
DESCRIPTION:      
Used to save default values in Client_GWP_Factor table    
  
INPUT PARAMETERS:      
Name        DataType Default Description      
------------------------------------------------------------      
@ClientCommodityId INT    
        
OUTPUT PARAMETERS:      
Name        DataType Default Description      
------------------------------------------------------------      
USAGE EXAMPLES:      
------------------------------------------------------------    
  
EXEC su.Client_GWP_Factor_Ins_By_Default_Values  1  
  
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
GB   Geetansu Behera      
CMH  Chad Hattabaugh       
HG   Hari     
SKA  Shobhit Kr Agrawal    
  
MODIFICATIONS       
Initials Date		Modification      
------------------------------------------------------------      
GB					Created   
SKA		01-OCT-09	Resending and used the one single insert statement after creating the new table for default values.
SKA		02-OCT-09	Changed the table name inside the script
  
  
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [su].[Client_GWP_Factor_Ins_By_Default_Values]
 @Client_Id AS INT
AS 

BEGIN
    SET  NOCOUNT ON
    
    BEGIN TRY
        BEGIN TRAN
        
        DECLARE  @isExist AS INT = NULL
        
        SELECT @isExist = COUNT( 1 )
        FROM su.Client_GWP_Factor
        WHERE Client_Id = @Client_Id


		INSERT INTO su.Client_GWP_Factor(Client_id
                                         ,Emission_cd
                                         ,co2e_factor )
        SELECT @Client_Id,Emission_Cd,Co2e_factor
        FROM SU.GWP_Default
        WHERE ( @isExist = 0 OR @isExist IS NULL )
        
        
       COMMIT TRAN
    END TRY
            BEGIN CATCH
        ROLLBACK TRAN
	        EXEC usp_RethrowError
    END CATCH
END
GO
GRANT EXECUTE ON  [su].[Client_GWP_Factor_Ins_By_Default_Values] TO [CBMSApplication]
GO
