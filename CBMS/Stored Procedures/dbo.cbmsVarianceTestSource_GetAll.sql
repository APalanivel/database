SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[cbmsVarianceTestSource_GetAll]
	( @MyAccountId int
	)
AS
BEGIN
	set nocount on
	   select variance_test_source_id
		, source_label
	     from variance_test_source with (nolock)
	 order by source_label asc

END
GO
GRANT EXECUTE ON  [dbo].[cbmsVarianceTestSource_GetAll] TO [CBMSApplication]
GO
