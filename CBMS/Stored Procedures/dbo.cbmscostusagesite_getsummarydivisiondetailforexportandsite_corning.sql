
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
name:

 dbo.[cbmscostusagesite_getsummarydivisiondetailforexportandsite_corning]

 description:

	this procedure selects the comments associated to the report master id.

 input parameters:
	 name			datatype	default		description
------------------------------------------------------------
     @site_id		INT
     @begin_date	DATETIME
     @end_date		DATETIME


 output parameters:
	 name			datatype  default description
------------------------------------------------------------

  usage examples:
------------------------------------------------------------

set statistics profile on
	Exec [dbo].[cbmscostusagesite_getsummarydivisiondetailforexportandsite_corning] 1758, '2010-01-01', '2010-12-31'

author initials:
	 initials	name
------------------------------------------------------------
	 jr			jon ruel
	 mm			moses morales
	 dr			deana ritter
	 ska		shobhit kr agrawal
	 BCH		Balaraju
	 NR			Narayana Reddy

 modifications
	initials	date		modification
------------------------------------------------------------
	mm			2/24/2010	created from script of cbmscostusagesite_getsummarydivisiondetailforexportandsite_textron and added the
							original unit for electricity and natural gas along with 2 additional units for each.

	mm			10/17/2010	It was not converting the cost to USD for the international sites, so I just added  ?AND cuc.converted_unit_id = 3? to the join with the currency_unit_conversion table.

	BCH			2012-04-02  Used Cost_Usage_Site_Dtl and Core.Client_Hier_Id tables instead of Cost_Usage_Site.Removed client and site tables.
							and removed the part of the script and that tables references also.

							LEFT OUTER JOIN currency_unit_conversion cuc
								  ON cuc.base_unit_id = cu.currency_unit_id
									 AND cuc.converted_unit_id = cu.currency_unit_id
									 AND cuc.conversion_date = cu.service_month
									 AND cuc.currency_group_id = ch.Client_Currency_Group_Id
							LEFT OUTER JOIN consumption_unit_conversion elcon_default
								  ON elcon_default.base_unit_id = cu.UOM_Type_Id
									 AND elcon_default.converted_unit_id = cu.UOM_Type_Id
							LEFT OUTER JOIN consumption_unit_conversion ngcon_default
								  ON ngcon_default.base_unit_id = cu.UOM_Type_Id
									 AND ngcon_default.converted_unit_id = cu.UOM_Type_Id
	NR              2016-05-31		MAINT-3789 Replaced DV2-Commodity-Type(Read as '-' to '_') by Commodity.            


******/
CREATE PROCEDURE [dbo].[cbmscostusagesite_getsummarydivisiondetailforexportandsite_corning]
      ( 
       @site_id INT
      ,@begin_date DATETIME
      ,@end_date DATETIME )
AS 
BEGIN
      SET NOCOUNT ON;
	/*** the default unit for electric power ***/
      DECLARE
            @el_commodity_type_id INT
           ,@ng_commodity_type_id INT
           ,@ng2unitid INT
           ,@ng2unitname VARCHAR(25)
           ,@ng3unitid INT
           ,@ng3unitname VARCHAR(25)
           ,@el2unitid INT
           ,@el2unitname VARCHAR(25)
           ,@el3unitid INT
           ,@el3unitname VARCHAR(25)
           ,@kvarhunitID INT
           ,@kvarhunitname VARCHAR(25)
           ,@otherunitid INT
           ,@otherunitname VARCHAR(25)

      SELECT
            @el_commodity_type_id = c.Commodity_Id
      FROM
            dbo.Commodity c
      WHERE
            c.Commodity_Name = 'electric power'
            

	/*** the default unit for natural gas ***/
      SELECT
            @ng_commodity_type_id = c.Commodity_Id
      FROM
            dbo.Commodity c
      WHERE
            c.Commodity_Name = 'natural gas'

	/*** natural gas in mcf ***/
      
      SELECT
            @ng2unitid = entity_id
           ,@ng2unitname = ent.entity_name
      FROM
            dbo.entity ent
            JOIN dbo.Commodity comtype
                  ON comtype.UOM_Entity_Type = ent.entity_type
      WHERE
            ent.entity_name = 'mmbtu'
            AND comtype.Commodity_Name = 'natural gas'

    /*** natural gas in mmbtu ***/

      SELECT
            @ng3unitid = entity_id
           ,@ng3unitname = ent.entity_name
      FROM
            dbo.entity ent
            JOIN dbo.Commodity comtype
                  ON comtype.UOM_Entity_Type = ent.entity_type
      WHERE
            ent.entity_name = 'kwh'
            AND comtype.Commodity_Name = 'natural gas'

   	/*** electric power in mwh ***/

      SELECT
            @el2unitid = entity_id
           ,@el2unitname = ent.entity_name
      FROM
            dbo.entity ent
            JOIN dbo.Commodity comtype
                  ON comtype.UOM_Entity_Type = ent.entity_type
      WHERE
            ent.entity_name = 'kwh'
            AND comtype.Commodity_Name = 'electric power'

     /*** electric power in kvarh ***/

      SELECT
            @kvarhunitID = ENTITY_ID
           ,@kvarhunitname = ent.ENTITY_NAME
      FROM
            dbo.ENTITY ent
            JOIN dbo.Commodity comtype
                  ON comtype.UOM_Entity_Type = ent.ENTITY_TYPE
      WHERE
            ent.ENTITY_NAME = 'kvarh'
            AND comtype.Commodity_Name = 'Electric Power'


    /*** electric power in mmbtu ***/

      SELECT
            @el3unitid = entity_id
           ,@el3unitname = ent.entity_name
      FROM
            dbo.entity ent
            JOIN dbo.Commodity comtype
                  ON comtype.UOM_Entity_Type = ent.entity_type
      WHERE
            ent.entity_name = 'mmbtu'
            AND comtype.Commodity_Name = 'electric power'

    /*** electric power in other uom ***/
      SELECT
            @otherunitid = ENTITY_ID
           ,@otherunitname = ent.ENTITY_NAME
      FROM
            dbo.ENTITY ent
            JOIN dbo.Commodity comtype
                  ON comtype.UOM_Entity_Type = ent.ENTITY_TYPE
      WHERE
            ent.ENTITY_NAME = 'other'
            AND comtype.Commodity_Name = 'Electric Power'


      SELECT
            ch.site_id
           ,ch.site_name
           ,ISNULL(CASE WHEN bm.bucket_name = 'Total Cost'
                             AND com.commodity_name = 'Electric Power' THEN cu.bucket_value
                   END, 0) AS el_cost
           ,ISNULL(CASE WHEN bm.bucket_name = 'Total Usage'
                             AND com.commodity_name = 'Electric Power' THEN cu.bucket_value
                   END, 0) AS el_usage
           ,elunit_default.ubm_unit_of_measure_code AS el_unit_of_measure_type
           ,ISNULL(CASE WHEN bm.bucket_name = 'Total Usage'
                             AND com.commodity_name = 'Electric Power' THEN cu.bucket_value
                   END, 0) * ISNULL(elcon2.conversion_factor, 0) AS el_usage2
           ,@el2unitname AS el_unit_of_measure_type2
           ,ISNULL(CASE WHEN bm.bucket_name = 'Total Usage'
                             AND com.commodity_name = 'Electric Power' THEN cu.bucket_value
                   END, 0) * ISNULL(elcon3.conversion_factor, 0) AS el_usage3
           ,@el3unitname AS el_unit_of_measure_type3
           ,CASE WHEN ( CASE WHEN bm.bucket_name = 'Total Usage'
                                  AND com.commodity_name = 'Electric Power' THEN cu.bucket_value
                        END ) > 0 THEN CONVERT(DECIMAL(32, 16), CASE WHEN bm.bucket_name = 'Total Cost'
                                                                          AND com.commodity_name = 'Electric Power' THEN cu.bucket_value
                                                                END / CASE WHEN bm.bucket_name = 'Total Usage'
                                                                                AND com.commodity_name = 'Electric Power' THEN cu.bucket_value
                                                                      END)
                 ELSE CONVERT(DECIMAL(32, 16), 0)
            END AS el_unit_cost
           ,'electric power' AS el_commodity_type
           ,ISNULL(CASE WHEN bm.bucket_name = 'Total Cost'
                             AND com.commodity_name = 'Natural Gas' THEN cu.bucket_value
                   END, 0) AS ng_cost
           ,ISNULL(CASE WHEN bm.bucket_name = 'Total Usage'
                             AND com.commodity_name = 'Natural Gas' THEN cu.bucket_value
                   END, 0) AS ng_usage
           ,ngunit_default.ubm_unit_of_measure_code AS ng_unit_of_measure_type
           ,ISNULL(CASE WHEN bm.bucket_name = 'Total Usage'
                             AND com.commodity_name = 'Natural Gas' THEN cu.bucket_value
                   END, 0) * ISNULL(ngcon2.conversion_factor, 0) AS ng_usage2
           ,@ng2unitname AS ng_unit_of_measure_type2
           ,ISNULL(CASE WHEN bm.bucket_name = 'Total Usage'
                             AND com.commodity_name = 'Natural Gas' THEN cu.bucket_value
                   END, 0) * ISNULL(ngcon3.conversion_factor, 0) ng_usage3
           ,@ng3unitname ng_unit_of_measure_type3
           ,CASE WHEN ( ISNULL(CASE WHEN bm.bucket_name = 'Total Usage'
                                         AND com.commodity_name = 'Natural Gas' THEN cu.bucket_value
                               END, 0) * ISNULL(ngcon2.conversion_factor, 0) ) > 0 THEN CONVERT(DECIMAL(32, 16), ( ISNULL(CASE WHEN bm.bucket_name = 'Total Cost'
                                                                                                                                    AND com.commodity_name = 'Natural Gas' THEN cu.bucket_value
                                                                                                                          END, 0) ) / ( ISNULL(CASE WHEN bm.bucket_name = 'Total Usage'
                                                                                                                                                         AND com.commodity_name = 'Natural Gas' THEN cu.bucket_value
                                                                                                                                               END, 0) * ISNULL(ngcon2.conversion_factor, 0) ))
                 ELSE CONVERT(DECIMAL(32, 16), 0)
            END AS ng_unit_cost
           ,'natural gas' AS ng_commodity_type
           ,cu.service_month
      FROM
            Core.Client_Hier ch
            JOIN dbo.Cost_Usage_Site_Dtl cu
                  ON cu.Client_Hier_Id = ch.Client_Hier_Id
            JOIN dbo.Bucket_Master bm
                  ON cu.Bucket_Master_Id = bm.Bucket_Master_Id
            JOIN dbo.Commodity com
                  ON bm.Commodity_Id = com.Commodity_Id
            JOIN dbo.ubm_unit_of_measure_map elunit_default
                  ON elunit_default.commodity_type_id = @el_commodity_type_id
                     AND elunit_default.unit_of_measure_type_id = cu.UOM_Type_Id
                     AND elunit_default.ubm_unit_of_measure_map_id = @kvarhunitID
            JOIN dbo.ubm_unit_of_measure_map ngunit_default
                  ON ngunit_default.commodity_type_id = @ng_commodity_type_id
                     AND ngunit_default.unit_of_measure_type_id = cu.UOM_Type_Id
                     AND ngunit_default.ubm_unit_of_measure_map_id = @otherunitid
            LEFT JOIN dbo.consumption_unit_conversion ngcon2
                  ON ngcon2.base_unit_id = cu.UOM_Type_Id
                     AND ngcon2.converted_unit_id = @ng2unitid
            LEFT JOIN dbo.consumption_unit_conversion ngcon3
                  ON ngcon3.base_unit_id = cu.UOM_Type_Id
                     AND ngcon3.converted_unit_id = @ng3unitid
            LEFT JOIN dbo.consumption_unit_conversion elcon2
                  ON elcon2.base_unit_id = cu.UOM_Type_Id
                     AND elcon2.converted_unit_id = @el2unitid
            LEFT JOIN dbo.consumption_unit_conversion elcon3
                  ON elcon3.base_unit_id = cu.UOM_Type_Id
                     AND elcon3.converted_unit_id = @el3unitid
      WHERE
            ch.SITE_ID = @site_id
            AND cu.service_month BETWEEN @begin_date
                                 AND     @end_date
            AND ch.Site_NOT_MANAGED <> 1
      ORDER BY
            ch.site_name
END;

;
GO


GRANT EXECUTE ON  [dbo].[cbmscostusagesite_getsummarydivisiondetailforexportandsite_corning] TO [CBMSApplication]
GO
