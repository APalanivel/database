SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                    
NAME:                    
                    
  dbo.[Report_DE_Invoice_Audit_Tracker_By_Client]                
                     
 DESCRIPTION:  To fetch Invoice Event Details for Client\Clients                    
 INPUT PARAMETERS:                    
Name							DataType				Default					Description                    
---------------------------------------------------------------------------------------------                    
@Client_Id_List					VARCHAR(MAX)                    
@begindate						DATETIME                    
@enddate						DATETIME         
        
OUTPUT PARAMETERS:                    
Name							DataType				Default					Description                    
---------------------------------------------------------------------------------------------                    
                  
 USAGE EXAMPLES:                    
---------------------------------------------------------------------------------------------                    
                  
 EXEC dbo.[Report_DE_Invoice_Audit_Tracker_By_Client] '11682','2013-10-11','2013-10-15'                       
 EXEC dbo.[Report_DE_Invoice_Audit_Tracker_By_Client] '-1','2010-01-01','2010-02-01'                    
 EXEC dbo.[Report_DE_Invoice_Audit_Tracker_By_Client] '-1,11017','2010-01-01','2010-02-01'  
 
  EXEC dbo.[Report_DE_Invoice_Audit_Tracker_By_Client] '235','2019-01-01','2019-05-01'              
                   
 AUTHOR INITIALS:                    
Initials Name                    
---------------------------------------------------------------------------------------------                    
SP       Sandeep Pigilam
NR		 Narayana Reddy                   
                
MODIFICATIONS                     
Initials Date			Modification                    
---------------------------------------------------------------------------------------------                    
SP       2013-12-04		Created                 
AKR      2014-09-02		Modified the code to include Invoice Type             
AKR      2014-10-07		Modified the sproc for Failed Recalc for Utility Accounts   
NR		 2016-01-20		MAINT-4737 Replaced Invoice_Type_Cd with Meter_Read_Type. 
NR		 2018-04-04		Data2.0 -  Removed Watchlist_group_Info_Id and replaced with Config table.            
******/

CREATE PROCEDURE [dbo].[Report_DE_Invoice_Audit_Tracker_By_Client]
    (
        @Client_Id_List VARCHAR(MAX)
        , @begindate DATE
        , @enddate DATE
    )
AS
    BEGIN

        SET NOCOUNT ON;



        DECLARE @ClientIds TABLE
              (
                  Client_Id INT PRIMARY KEY
              );

        INSERT  @ClientIds SELECT   Segments FROM   dbo.ufn_split(@Client_Id_List, ',');


        CREATE TABLE #Accounts
             (
                 CU_INVOICE_ID INT
                 , Account_Number VARCHAR(50)
                 , Account_Id INT
                 , Vendor VARCHAR(200)
                 , Client_Name VARCHAR(200)
                 , Site VARCHAR(200)
                 , City VARCHAR(200)
                 , State VARCHAR(200)
                 , Country VARCHAR(200)
                 , Supplier_Account_Recalc_Type_Cd INT
                 , SERVICE_MONTH DATETIME
                 , Account_Type CHAR(8)
             );

        CREATE TABLE #Account_Invoice_Processing_Instruction
             (
                 Account_Id INT
                 , Processing_Instruction_Category_Cd INT
                 , Processing_Instruction NVARCHAR(MAX)
                 , Row_Num INT
             );

        CREATE TABLE #Account_Group_Name
             (
                 Account_Id INT
                 , GROUP_NAME VARCHAR(200)
                 , Row_Num INT
             );
        CREATE TABLE #CU_INVOICE_EVENT
             (
                 CU_INVOICE_ID INT NOT NULL
                 , EVENT_DATE DATETIME
                 , EVENT_BY_ID INT
                 , EVENT_DESCRIPTION VARCHAR(4000)
                 , UBM VARCHAR(200)
                 , IS_PROCESSED BIT
                 , IS_REPORTED BIT
                 , IS_MANUAL BIT
                 , CLIENT_ID INT
                 , Meter_Read_Type VARCHAR(25)
             );


        CREATE TABLE #Cu_Invoice_Event_Whole
             (
                 CU_INVOICE_ID INT
                 , EVENT_DATE DATETIME
                 , Flag BIT
             );

        DECLARE
            @Invoice_Processing INT
            , @Other_Processing INT
            , @Variance_Processing INT;

        SELECT
            @Invoice_Processing = MAX(CASE WHEN c.Code_Value = 'Invoice' THEN c.Code_Id
                                      END)
            , @Other_Processing = MAX(CASE WHEN c.Code_Value = 'Other' THEN c.Code_Id
                                      END)
            , @Variance_Processing = MAX(CASE WHEN c.Code_Value = 'Variance' THEN c.Code_Id
                                         END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            Codeset_Name = 'ProcessingInstruction';


        INSERT  #CU_INVOICE_EVENT
             (
                 CU_INVOICE_ID
                 , EVENT_DATE
                 , EVENT_BY_ID
                 , EVENT_DESCRIPTION
                 , UBM
                 , IS_PROCESSED
                 , IS_REPORTED
                 , IS_MANUAL
                 , CLIENT_ID
                 , Meter_Read_Type
             )
        SELECT
            CIE.CU_INVOICE_ID
            , CIE.EVENT_DATE
            , CIE.EVENT_BY_ID
            , CIE.EVENT_DESCRIPTION
            , UBM.UBM_NAME
            , CI.IS_PROCESSED
            , CI.IS_REPORTED
            , CI.IS_MANUAL
            , CI.CLIENT_ID
            , itc.Code_Value
        FROM
            dbo.CU_INVOICE_EVENT CIE
            JOIN dbo.CU_INVOICE CI
                ON CIE.CU_INVOICE_ID = CI.CU_INVOICE_ID
            LEFT JOIN dbo.UBM
                ON CI.UBM_ID = UBM.UBM_ID
            JOIN @ClientIds C
                ON CI.CLIENT_ID = C.Client_Id
            LEFT JOIN dbo.Code itc
                ON itc.Code_Id = CI.Meter_Read_Type_Cd
        WHERE
            CIE.EVENT_DATE BETWEEN @begindate
                           AND     @enddate
            AND CIE.EVENT_DESCRIPTION LIKE '%insert%'
            AND CI.IS_DNT = 0
        GROUP BY
            CIE.CU_INVOICE_ID
            , CIE.EVENT_DATE
            , CIE.EVENT_BY_ID
            , CIE.EVENT_DESCRIPTION
            , UBM.UBM_NAME
            , CI.IS_PROCESSED
            , CI.IS_REPORTED
            , CI.IS_MANUAL
            , CI.CLIENT_ID
            , itc.Code_Value;

        IF CHARINDEX('-1', @Client_Id_List, 1) > 0
            INSERT  #CU_INVOICE_EVENT
                 (
                     CU_INVOICE_ID
                     , EVENT_DATE
                     , EVENT_BY_ID
                     , EVENT_DESCRIPTION
                     , UBM
                     , IS_PROCESSED
                     , IS_REPORTED
                     , IS_MANUAL
                     , CLIENT_ID
                     , Meter_Read_Type
                 )
            SELECT
                CIE.CU_INVOICE_ID
                , CIE.EVENT_DATE
                , CIE.EVENT_BY_ID
                , CIE.EVENT_DESCRIPTION
                , UBM.UBM_NAME
                , CI.IS_PROCESSED
                , CI.IS_REPORTED
                , CI.IS_MANUAL
                , NULL AS CLIENT_ID
                , itc.Code_Value
            FROM
                dbo.CU_INVOICE_EVENT CIE
                JOIN dbo.CU_INVOICE CI
                    ON CIE.CU_INVOICE_ID = CI.CU_INVOICE_ID
                LEFT JOIN dbo.UBM
                    ON CI.UBM_ID = UBM.UBM_ID
                LEFT JOIN dbo.Code itc
                    ON itc.Code_Id = CI.Meter_Read_Type_Cd
            WHERE
                CI.CLIENT_ID IS NULL
                AND CIE.EVENT_DATE BETWEEN @begindate
                                   AND     @enddate
                AND CIE.EVENT_DESCRIPTION LIKE '%insert%'
                AND CI.IS_DNT = 0
            GROUP BY
                CIE.CU_INVOICE_ID
                , CIE.EVENT_DATE
                , CIE.EVENT_BY_ID
                , CIE.EVENT_DESCRIPTION
                , UBM.UBM_NAME
                , CI.IS_PROCESSED
                , CI.IS_REPORTED
                , CI.IS_MANUAL
                , itc.Code_Value;

        CREATE CLUSTERED INDEX IX_TCU_INVOICE_EVENT
            ON #CU_INVOICE_EVENT
        (
            CU_INVOICE_ID
        )   ;


        INSERT  #Cu_Invoice_Event_Whole
        SELECT
            CIE2.CU_INVOICE_ID
            , CIE2.EVENT_DATE
            , CASE WHEN CIE2.EVENT_DESCRIPTION = 'Invoice Closed via Manual Close/Stop Processing' THEN 1
                  ELSE 0
              END Flag
        FROM
            #CU_INVOICE_EVENT CIE
            JOIN dbo.CU_INVOICE_EVENT CIE2 WITH (INDEX = IX_CU_INVOICE_EVENT)
                ON CIE.CU_INVOICE_ID = CIE2.CU_INVOICE_ID
        WHERE
            (   CIE2.EVENT_DESCRIPTION LIKE '%passed resolve to month%'
                OR  CIE2.EVENT_DESCRIPTION LIKE '%resolved to month%'
                OR  CIE2.EVENT_DESCRIPTION = 'Invoice Closed via Manual Close/Stop Processing');

        CREATE CLUSTERED INDEX IX_TCU_INVOICE_EVENT_WHOLE
            ON #Cu_Invoice_Event_Whole
        (
            CU_INVOICE_ID
            , Flag
        )   ;

        INSERT INTO #Accounts
             (
                 CU_INVOICE_ID
                 , Account_Number
                 , Account_Id
                 , Vendor
                 , Client_Name
                 , Site
                 , City
                 , State
                 , Country
                 , Supplier_Account_Recalc_Type_Cd
                 , SERVICE_MONTH
                 , Account_Type
             )
        SELECT
            CIE.CU_INVOICE_ID
            , CHA.Account_Number
            , CHA.Account_Id
            , CHA.Account_Vendor_Name Vendor
            , CH.Client_Name
            , CH.Site_name Site
            , CH.City City
            , CH.State_Name State
            , CH.Country_Name Country
            , CHA.Supplier_Account_Recalc_Type_Cd
            , CISM.SERVICE_MONTH
            , CHA.Account_Type
        FROM
            #CU_INVOICE_EVENT CIE
            JOIN dbo.CU_INVOICE_SERVICE_MONTH CISM
                ON CIE.CU_INVOICE_ID = CISM.CU_INVOICE_ID
            JOIN Core.Client_Hier_Account CHA
                ON CISM.Account_ID = CHA.Account_Id
            JOIN Core.Client_Hier CH
                ON CH.Client_Hier_Id = CHA.Client_Hier_Id
        WHERE
            CH.Site_Id > 0
        GROUP BY
            CIE.CU_INVOICE_ID
            , CHA.Account_Number
            , CHA.Account_Vendor_Name
            , CH.Client_Name
            , CH.Site_name
            , CH.City
            , CH.State_Name
            , CH.Country_Name
            , CHA.Account_Id
            , CHA.Supplier_Account_Recalc_Type_Cd
            , CISM.SERVICE_MONTH
            , CHA.Account_Type;



        INSERT INTO #Account_Invoice_Processing_Instruction
             (
                 Account_Id
                 , Processing_Instruction_Category_Cd
                 , Processing_Instruction
                 , Row_Num
             )
        SELECT
            aipc.Account_Id
            , aipc.Processing_Instruction_Category_Cd
            , aipc.Processing_Instruction
            , DENSE_RANK() OVER (PARTITION BY
                                     aipc.Processing_Instruction_Category_Cd
                                 ORDER BY
                                     aipc.Last_Change_Ts DESC) AS Row_Num
        FROM
            #Accounts a
            INNER JOIN dbo.Account_Invoice_Processing_Config aipc
                ON aipc.Account_Id = a.Account_Id
        WHERE
            aipc.Is_Active = 1;




        INSERT INTO #Account_Group_Name
             (
                 Account_Id
                 , GROUP_NAME
                 , Row_Num
             )
        SELECT
            a.Account_Id
            , gi.GROUP_NAME
            , ROW_NUMBER() OVER (ORDER BY
                                     aipc.Last_Change_Ts DESC) AS Row_Num
        FROM
            #Accounts a
            INNER JOIN dbo.Account_Invoice_Processing_Config aipc
                ON aipc.Account_Id = a.Account_Id
            INNER JOIN dbo.GROUP_INFO gi
                ON gi.GROUP_INFO_ID = aipc.Watch_List_Group_Info_Id
        WHERE
            aipc.Watch_List_Group_Info_Id IS NOT NULL
            AND aipc.Is_Active = 1;



        WITH CTE_CUException
        AS (
               SELECT
                    eg.ENTITY_NAME Issue_Type
                    , ce.CU_INVOICE_ID
                    , ce.CU_EXCEPTION_ID
                    , et.EXCEPTION_TYPE
                    , CASE WHEN ced.IS_CLOSED = 1 THEN 'Closed'
                          ELSE es.ENTITY_NAME
                      END AS exception_status_type
                    , ced.OPENED_DATE
                    , cr.ENTITY_NAME closed_reason_type
                    , ced.CLOSED_BY_ID
                    , ui.FIRST_NAME + ' ' + ui.LAST_NAME closed_by
                    , ced.CLOSED_DATE
               FROM
                    #CU_INVOICE_EVENT CIE
                    JOIN CU_EXCEPTION ce
                        ON CIE.CU_INVOICE_ID = ce.CU_INVOICE_ID
                    JOIN CU_EXCEPTION_DETAIL ced
                        ON ced.CU_EXCEPTION_ID = ce.CU_EXCEPTION_ID
                    LEFT OUTER JOIN CU_EXCEPTION_TYPE et
                        ON et.CU_EXCEPTION_TYPE_ID = ced.EXCEPTION_TYPE_ID
                    LEFT OUTER JOIN ENTITY eg
                        ON eg.ENTITY_ID = et.EXCEPTION_GROUP_TYPE_ID
                    LEFT OUTER JOIN ENTITY es
                        ON es.ENTITY_ID = ced.EXCEPTION_STATUS_TYPE_ID
                    LEFT OUTER JOIN ENTITY cr
                        ON cr.ENTITY_ID = ced.CLOSED_REASON_TYPE_ID
                    LEFT OUTER JOIN USER_INFO ui
                        ON ui.USER_INFO_ID = ced.CLOSED_BY_ID
               GROUP BY
                   eg.ENTITY_NAME
                   , et.EXCEPTION_TYPE
                   , es.ENTITY_NAME
                   , ced.OPENED_DATE
                   , cr.ENTITY_NAME
                   , ced.CLOSED_BY_ID
                   , ui.FIRST_NAME + ' ' + ui.LAST_NAME
                   , ced.CLOSED_DATE
                   , ce.CU_INVOICE_ID
                   , ce.CU_EXCEPTION_ID
                   , ced.IS_CLOSED
           )
        SELECT
            CIE.CU_INVOICE_ID CuInvoiceID
            , ISNULL(CA.Client_Name, 'No Client') AS [Client Name]
            , CIE.EVENT_DATE InsertEventDate
            , CRC.EVENT_DATE AS ResolvedToMonthEventDate
            , CA.SERVICE_MONTH
            , CA.Account_Number AS [Account Number]
            , CA.Site
            , CA.City
            , CA.State
            , CA.Country
            , CA.Vendor
            , CIE.UBM
            , agn.GROUP_NAME
            , MAX(apiv.Processing_Instruction) Variance_Processing_Instrunctions
            , MAX(apii.Processing_Instruction) Invoice_Processing_Instrunctions
            , MAX(apio.Processing_Instruction) Other_Processing_Instrunctions
            , CE.Issue_Type
            , CASE WHEN CE.EXCEPTION_TYPE = 'Failed Recalc'
                        AND CA.Account_Type = 'Utility' THEN 'Failed - Utility Recalc'
                  WHEN CE.EXCEPTION_TYPE = 'Failed Recalc' THEN 'Failed - ' + rt.Code_Value
                  ELSE CE.EXCEPTION_TYPE
              END AS exception_type
            , CE.exception_status_type
            , CE.OPENED_DATE
            , CE.closed_reason_type
            , CE.CLOSED_BY_ID
            , CE.closed_by
            , CE.CLOSED_DATE
            , CIE.Meter_Read_Type
        FROM
            #CU_INVOICE_EVENT CIE
            JOIN dbo.USER_INFO UI1
                ON CIE.EVENT_BY_ID = UI1.USER_INFO_ID
            LEFT JOIN #Accounts CA
                ON CIE.CU_INVOICE_ID = CA.CU_INVOICE_ID
            LEFT JOIN #Cu_Invoice_Event_Whole CRC
                ON CIE.CU_INVOICE_ID = CRC.CU_INVOICE_ID
                   AND  CRC.Flag = 0
            LEFT JOIN #Account_Invoice_Processing_Instruction apii
                ON CA.Account_Id = apii.Account_Id
                   AND  apii.Row_Num = 1
                   AND  apii.Processing_Instruction_Category_Cd = @Invoice_Processing
            LEFT JOIN #Account_Invoice_Processing_Instruction apio
                ON CA.Account_Id = apio.Account_Id
                   AND  apio.Row_Num = 1
                   AND  apio.Processing_Instruction_Category_Cd = @Other_Processing
            LEFT JOIN #Account_Invoice_Processing_Instruction apiv
                ON CA.Account_Id = apiv.Account_Id
                   AND  apiv.Row_Num = 1
                   AND  apiv.Processing_Instruction_Category_Cd = @Variance_Processing
            LEFT JOIN CTE_CUException CE
                ON CIE.CU_INVOICE_ID = CE.CU_INVOICE_ID
            LEFT OUTER JOIN dbo.Code rt
                ON rt.Code_Id = CA.Supplier_Account_Recalc_Type_Cd
            LEFT OUTER JOIN #Account_Group_Name agn
                ON agn.Account_Id = CA.Account_Id
                   AND  agn.Row_Num = 1
        GROUP BY
            CIE.CU_INVOICE_ID
            , ISNULL(CA.Client_Name, 'No Client')
            , CIE.EVENT_DATE
            , CRC.EVENT_DATE
            , CA.SERVICE_MONTH
            , CA.Account_Number
            , CA.Site
            , CA.City
            , CA.State
            , CA.Country
            , CA.Vendor
            , CIE.UBM
            , CE.Issue_Type
            , CE.EXCEPTION_TYPE
            , CE.exception_status_type
            , CE.OPENED_DATE
            , CE.closed_reason_type
            , CE.CLOSED_BY_ID
            , CE.closed_by
            , CE.CLOSED_DATE
            , agn.GROUP_NAME
            , rt.Code_Value
            , CIE.Meter_Read_Type
            , CASE WHEN CE.EXCEPTION_TYPE = 'Failed Recalc'
                        AND CA.Account_Type = 'Utility' THEN 'Failed - Utility Recalc'
                  WHEN CE.EXCEPTION_TYPE = 'Failed Recalc' THEN 'Failed - ' + rt.Code_Value
                  ELSE CE.EXCEPTION_TYPE
              END
        ORDER BY
            [Client Name];


        DROP TABLE #Accounts;
        DROP TABLE #Account_Group_Name;
        DROP TABLE #Account_Invoice_Processing_Instruction;
        DROP TABLE #CU_INVOICE_EVENT;
        DROP TABLE #Cu_Invoice_Event_Whole;


    END;
    ;


    ;

GO




GRANT EXECUTE ON  [dbo].[Report_DE_Invoice_Audit_Tracker_By_Client] TO [CBMS_SSRS_Reports]
GO
