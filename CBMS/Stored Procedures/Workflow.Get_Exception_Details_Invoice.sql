SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




































             
/******                    
NAME:    [Workflow].[Get_Exception_Details_Invoice]                
DESCRIPTION: This is the master stored procedure created to display the result set                    
  the inputs will come with pipe delimeted if filter type is multi select                
------------------------------------------------------------                   
 INPUT PARAMETERS:                    
 Name   DataType  Default Description                    
 @MODULE_ID int                    
   ,@Workflow_Queue_Saved_Filter_Query_Id int                    
   ,@Logged_In_User int                       
   ,@Queue_id    varchar (max) =null  ---  User selected from drop down Id 49,1,2,3                    
   ,@Exception_Type   varchar (max) =null                     
   ,@Account_Number   varchar(500)  =null                     
   ,@Client     varchar(500)  =null                     
   ,@Site      varchar(500)  =null                     
   ,@Country     varchar(500)  =null                     
   ,@State      varchar(500)  =null                     
   ,@City      varchar(500)  =null                     
   ,@Commodity     varchar(500)  =null                     
   ,@Invoice_ID    varchar(500)  =null                     
   ,@Priority     varchar(500)  =null  --- Yes                    
   ,@Exception_Status     varchar(500)  =null                     
   ,@Comments     varchar(500)  =null                     
   ,@Start_Date_in_Queue  Datetime   =null                     
   ,@End_Date_in_Queue   Datetime   =null                     
   ,@Start_Date_in_CBMS  Datetime   =null                     
   ,@End_Date_in_CBMS   Datetime    =null                     
   ,@Data_Source    Varchar(100)  =null                     
   ,@Vendor     Varchar(100)  =null                     
   ,@Vendor_Type    varchar(100)  =null                     
   ,@Month      datetime   =null                     
   ,@Filename      Varchar(100)  =null                     
   ,@startindex    int=1                              
   ,@endindex     int=2147483647                      
   ,@ONLY_COUNT_NEEDED   BIT =null  -- if required only count then we need to pass value as 1 then it'll return the total record count                 
                   
------------------------------------------------------------                    
 OUTPUT PARAMETERS:                    
 Name   DataType  Default Description                    
 @TOTAL_COUNT INT OUTPUT,                  
 @PAGE_ROW_COUNT INT OUTPUT                  
------------------------------------------------------------                    
 USAGE EXAMPLES:                  
DECLARE       @return_value int,                  
             @TOTAL_COUNT INT,                  
    @PAGE_ROW_COUNT INT                   
                  
EXEC   @return_value = [Workflow].[Get_Exception_Details_Invoice]                  
             @MODULE_ID = 1, --- workflow module id                  
             @Workflow_Queue_Saved_Filter_Query_Id = 147, --                   
    @Logged_In_User =49,--user id done                  
             @Queue_id = '49',-- done  -- queue_id from user_info table comma separated                  
             @Exception_Type =null, -- done multiple exception type with comma separated                  
             @Account_Number =null, -- done                  
             @Client = null, -- done                  
             @Site = null, --done                  
             @Country = null, --multiple country id (1,4) comma separated done                  
             @State =null, --state id comma separated done                  
             @City = NULL, --done                   
             @Commodity =null,-- done                  
             @Invoice_ID = null, --invoice id string done                  
             @Priority = null, --- Yes/No/NULL done                  
             @exception_Status = null, --done                   
    @Comments =null, -- DONE                   
             @Start_Date_in_Queue = '9/20/2019', -- done                  
             @End_Date_in_Queue = '10/21/2019',-- done                  
           @Start_Date_in_CBMS = null,-- done                  
             @End_Date_in_CBMS = null,-- done                  
       @Data_Source = null, --done                  
             @Vendor = null, --done                  
    @Vendor_type = null, --done                  
@Month = null , --done                  
             @Filename =null, -- done                    
    @startindex =1,                    
  @endindex = 50,                  
             @ONLY_COUNT_NEEDED = 0,                  
             @TOTAL_COUNT = @TOTAL_COUNT OUTPUT,                  
    @PAGE_ROW_COUNT= @PAGE_ROW_COUNT  OUTPUT                  
                  
SELECT @TOTAL_COUNT as '@TOTAL_COUNT' ,   @PAGE_ROW_COUNT as '@PAGE_ROW_COUNT'                  
                  
SELECT 'Return Value' = @return_value                 
------------------------------------------------------------                    
AUTHOR INITIALS:                    
Initials Name                    
------------------------------------------------------------                    
AKP    Arunkumar Summit Energy                 
TRK    Ramakrishna  Summit Energy                
 MODIFICATIONS                     
 Initials Date   Modification                    
------------------------------------------------------------                    
 AKP   Aug 29,2019 Created                
 TRK  Sep-11-2019 Changed the Date_In_CBMS, Date_In_Queue as International date formate        
 TRK   15-Oct-2019 Updated the @Queue_id = '-2' to  @Queue_id = '-1,0' 
 AP		Oct 30 2019 Modified for Ticket D20-1420 Bulk route function allows me to route invoices without adding comment             
AP		OCt 31 Modified this SP to call global SP to remove unnecessary system logic.
AP		June 2,2020		Performance issue ticket https://summit.jira.com/browse/PRSUPPORT-3915. Changed sub query to outer apply
******/                  
CREATE PROCEDURE [Workflow].[Get_Exception_Details_Invoice]                    
    -- Add the parameters for the stored procedure here                       
    @MODULE_ID INT,                    
    @Workflow_Queue_Saved_Filter_Query_Id INT = NULL,                    
    @Logged_In_User INT,                    
    @Queue_id VARCHAR(MAX) = NULL,            ---  User selected from drop down Id 49,1,2,3                      
    @Exception_Type VARCHAR(MAX) = NULL,                    
    @Account_Number VARCHAR(500) = NULL,                    
    @Client VARCHAR(500) = NULL,                    
    @Site VARCHAR(500) = NULL,                    
    @Country VARCHAR(500) = NULL,                    
    @State VARCHAR(500) = NULL,                    
    @City VARCHAR(500) = NULL,                    
    @Commodity VARCHAR(500) = NULL,                    
    @Invoice_ID VARCHAR(500) = NULL,                    
    @Priority INT = NULL,                        --- Yes                      
    @Exception_Status INT = NULL,                    
    @Comments VARCHAR(500) = NULL,                    
    @Start_Date_in_Queue DATETIME = NULL,                    
    @End_Date_in_Queue DATETIME = NULL,                    
    @Start_Date_in_CBMS DATETIME = NULL,                    
    @End_Date_in_CBMS DATETIME = NULL,                    
    @Data_Source INT = NULL,                    
    @Vendor VARCHAR(100) = NULL,                    
    @Vendor_Type VARCHAR(100) = NULL,                    
    @Month DATETIME = NULL,                    
    @Filename VARCHAR(100) = NULL,                    
    @startindex INT = 1,                    
    @endindex INT = 2147483647,                    
    @ONLY_COUNT_NEEDED INT = NULL,                    
    @Sorting_Column_name VARCHAR(200) = NULL, --send workflow.workflow_queue_output_column -> Data_Source_Column_Name                    
    @Sorting_Type VARCHAR(10) = NULL,         --- send 1 or 0. 1 - Asc, 0 - Desc   
	@DOWNLOAD_ONLY INT = NULL,    
  @TOTAL_COUNT  INT OUTPUT,                   
    @PAGE_ROW_COUNT INT OUTPUT                    
   ,@Date_IN_QUEUE_COUNT INT OUTPUT                    
AS                    
BEGIN -- SET NOCOUNT ON added to prevent extra result sets from                       
    -- interfering with SELECT statements.                       
           
                    
    SET NOCOUNT ON;                    
                    
    DECLARE @PROC_NAME VARCHAR(100) = 'Get_Exception_Details_Invoice',                    
            @INPUT_PARAMS VARCHAR(1000),                    
            @ERROR_LINE INT,            
            @ERROR_MESSAGE VARCHAR(3000),                    
            @SQL NVARCHAR(MAX),      
   @SQL1 NVARCHAR(MAX),                
            @SQL2 NVARCHAR(MAX),     
   @SQL3 NVARCHAR(MAX),                  
            @Data_Source_Column_Name VARCHAR(100),  
   @Date_IN_QUEUE_COUNT_System INT,  
   @Date_IN_QUEUE_COUNT_User INT    ,
   @SQL4 NVARCHAR (MAX)   
   
   IF (@Start_Date_in_Queue is NULL AND @End_Date_in_Queue IS NOT null)
   begin
   
   SET @Start_Date_in_Queue = '2000-01-01 00:00:00'  
   end

    IF (@Start_Date_in_Queue is NOT NULL AND @End_Date_in_Queue IS  null)
	begin
   
   SET @End_Date_in_Queue =  CONVERT(VARCHAR(19), GETDATE(), 120) 
   END
   
      IF (@Start_Date_in_CBMS is NULL AND @End_Date_in_cbms IS NOT null)
   begin
   
   SET @Start_Date_in_CBMS = '2000-01-01 00:00:00'  
   end

    IF (@Start_Date_in_CBMS is NOT NULL AND @End_Date_in_cbms IS  null)
	begin
   
   SET @End_Date_in_cbms =  CONVERT(VARCHAR(19), GETDATE(), 120) 
   end
         
   set @End_Date_in_Queue = dateadd (d, 1, @End_Date_in_Queue )         
   set @End_Date_in_CBMS =dateadd (d,1 , @End_Date_in_CBMS )  
   
   ---FOR DOWNLOAD START AND END INDEX SHOULD NOT BE APPLICABLE---
   IF (@DOWNLOAD_ONLY=1)
   BEGIN
   SET @startindex     = 1                              
   SET @endindex      =2147483647    
   END        

     if (isnull(@Sorting_Column_name,'') ='')
   set @Sorting_Column_name  ='Inv Id'
   
   if (isnull(@Sorting_Type,'') ='')
   set @Sorting_Type ='asc'  
                    
    SET @Data_Source_Column_Name =                    
    (                    
        SELECT case when Data_Source_Column_Name  ='Data Source' then '[Data Source]'          
  else Data_Source_Column_Name          
  end                  
        FROM Workflow.Workflow_Queue_Output_Column                    
        WHERE Display_Column_Name = @Sorting_Column_name  
		AND Workflow_Queue_Id = @MODULE_ID                  
    );                    
          
        
                    
    BEGIN TRY                    
                    
        /* STEP 1 FORMAT THE INPUTS. IF INPUT IS COMMA SEPARATED, SPILT THE STRING USING BELOW FORMULA*/                    
                    
            
        CREATE TABLE #temp_user_info                    
        (                    
            User_info_id INT  ,
			user_type varchar (50)                  
        );                    
                    
        CREATE TABLE #temp_Exception_Type                    
        (                    
            exception_type INT                    
        );                    
                    
        IF (@Queue_id IS NOT NULL)                    
        BEGIN                    
                    
            INSERT #temp_user_info                    
            SELECT CAST(Segments AS INT) AS User_info_id  ,
			case when CAST(Segments AS INT)= -1 then 'private users'
			when    CAST(Segments AS INT)= 0 then 'Public Users'     
			when    CAST(Segments AS INT) not in (-1, 0 ) then 'Regular Users'    
			end           
            FROM dbo.ufn_split(@Queue_id, ',');                    
   END;
        
   
   IF EXISTS (SELECT 1 FROM #temp_user_info WHERE User_type = 'Private Users' OR
	User_type = 'Public Users' 	)
	AND NOT EXISTS 
	(SELECT 1 FROM #temp_user_info WHERE User_type = 'Regular Users')  
	AND (@Exception_Type IS NULL 
            and @Account_Number IS null 
            and @Client IS null 
            and @Site IS null 
            and @Country IS null 
            and @State IS NULL 
            and @City IS NULL 
            and @Commodity IS NULL  
            and @Invoice_ID IS  null 
            and @Priority IS  null 
            and @exception_Status is null 
			and @Comments	IS NULL 
            and @Start_Date_in_Queue IS  null 
            and @End_Date_in_Queue IS NULL 
            and @Start_Date_in_CBMS IS NULL 
            and @End_Date_in_CBMS IS NULL 
            and @Data_Source IS NULL 
            and @Vendor IS NULL 
			and @Vendor_type IS NULL 
            and @Month IS NULL 
            and @Filename IS NULL  )   
			
			BEGIN 
			
			EXEC    Workflow.Get_Exception_Details_Invoice_All_Open_Items
             @MODULE_ID = @MODULE_ID,  
             @Workflow_Queue_Saved_Filter_Query_Id = @Workflow_Queue_Saved_Filter_Query_Id, -- 
			 @Logged_In_User =@Logged_In_User, 
             @Queue_id =@Queue_id, 
           @startindex =@startindex,  
             @endindex = @endindex,
             @ONLY_COUNT_NEEDED = @ONLY_COUNT_NEEDED,
			  @Sorting_Column_name =@Sorting_Column_name,  
			     @DOWNLOAD_ONLY = @DOWNLOAD_ONLY ,
			@Sorting_Type =@Sorting_Type,
             @TOTAL_COUNT = @TOTAL_COUNT OUTPUT,		
			 @PAGE_ROW_COUNT= @PAGE_ROW_COUNT  OUTPUT,
			 @Date_IN_QUEUE_COUNT =  @Date_IN_QUEUE_COUNT output 
			  
			 RETURN 0 
			 --close the sp call
			
			END          
                    
        IF (@Exception_Type IS NOT NULL)                    
        BEGIN                    
                    
            INSERT #temp_Exception_Type                    
            SELECT CAST(Segments AS INT) AS exception_type                    
            FROM dbo.ufn_split(@Exception_Type, ',');                    
        END;                    
	                
                    
        CREATE TABLE #DEFAULTEXCEPTIONS                    
        (                    
            UserGroupId INT NULL,                    
            UserGroupName VARCHAR(300) NULL,                    
            usergroupcount INT NULL,                    
            systemgroupid INT NULL,                    
            SystemGroupCount INT NULL,                    
            ubmid INT NULL,                    
            ubmaccountcode VARCHAR(200) NULL,                    
            [RN] [BIGINT] NULL,                    
            [CU_INVOICE_ID] [INT] NULL,                    
            [ACCOUNT] [VARCHAR](MAX) NULL,                    
            [Account_Id] varchar(50) NULL,                    
            [EXCEPTION_TYPE] [VARCHAR](MAX) NULL,                    
            [EXCEPTION_STATUS_TYPE] [VARCHAR](MAX) NULL,                    
            [ASSIGNEE] [VARCHAR](MAX) NULL,                    
            [CLIENTID] INT NULL,                    
            [CLIENT] [VARCHAR](MAX) NULL,                    
            [COMMODITY] [VARCHAR](MAX) NULL,                    
            [DATA SOURCE] [VARCHAR](MAX) NULL,                    
			[DATE_IN_QUEUE] [DATETIME] NULL,                    
            [DATE_IN_CBMS] [DATETIME] NULL,                    
            [FILEID] INT NULL,                    
            [FILENAME] [VARCHAR](MAX) NULL,                    
            [SERVICE_MONTH] [VARCHAR](MAX) NULL,                    
            [SITE] [VARCHAR](MAX) NULL,                    
			[COUNTRY] [NVARCHAR](MAX) NULL,                    
            [STATE] [NVARCHAR](MAX) NULL,                    
            [VENDOR] [NVARCHAR](MAX) NULL,                    
            [VENDOR_TYPE] [NVARCHAR](MAX) NULL,                    
            [COMMENTS] VARCHAR(MAX) NULL,                    
            [is_priority] INT    NULL ,
			[priority_cd] numeric (10,0)                
        );                    
                    
                    
                    
                    
        CREATE TABLE #DEFAULTEXCEPTIONSUSERGROUP                    
        (            
            UserGroupId INT NULL,                    
            UserGroupName VARCHAR(300) NULL,                    
            usergroupcount INT NULL,                    
            systemgroupid INT NULL,                    
            SystemGroupCount INT NULL,                    
            ubmid INT NULL,                    
            ubmaccountcode VARCHAR(200) NULL,                    
            [RN] [BIGINT] NULL,                    
            [CU_INVOICE_ID] [INT] NULL,                    
            [ACCOUNT] [VARCHAR](MAX) NULL,                    
            [Account_Id] varchar(50) NULL,                    
            [EXCEPTION_TYPE] [VARCHAR](MAX) NULL,                    
            [EXCEPTION_STATUS_TYPE] [VARCHAR](MAX) NULL,                    
            [ASSIGNEE] [VARCHAR](MAX) NULL,                    
            [CLIENTID] INT NULL,                    
            [CLIENT] [VARCHAR](MAX) NULL,                    
            [COMMODITY] [VARCHAR](MAX) NULL,                    
            [DATA SOURCE] [VARCHAR](MAX) NULL,                    
            [DATE_IN_QUEUE] [DATETIME] NULL,                    
            [DATE_IN_CBMS] [DATETIME] NULL,                    
            [FILEID] INT NULL,                    
            [FILENAME] [VARCHAR](MAX) NULL,                    
            [SERVICE_MONTH] [VARCHAR](MAX) NULL,                    
            [SITE] [VARCHAR](MAX) NULL,                    
            [COUNTRY] [NVARCHAR](MAX) NULL,                    
            [STATE] [NVARCHAR](MAX) NULL,                    
            [VENDOR] [NVARCHAR](MAX) NULL,                    
            [VENDOR_TYPE] [NVARCHAR](MAX) NULL,                    
            [COMMENTS] VARCHAR(MAX) NULL,                    
            [is_priority] INT NULL,
			[priority_cd] numeric (10,0)                
  
        );    
		
		 CREATE TABLE #DEFAULTEXCEPTIONSUSERGROUP_DETAIL                    
        (            
            UserGroupId INT NULL,                    
            UserGroupName VARCHAR(300) NULL,                    
            usergroupcount INT NULL,                    
            systemgroupid INT NULL,                    
            SystemGroupCount INT NULL,                    
            ubmid INT NULL,                    
            ubmaccountcode VARCHAR(200) NULL,                    
            [RN] [BIGINT] NULL,                    
            [CU_INVOICE_ID] [INT] NULL,                    
            [ACCOUNT] [VARCHAR](MAX) NULL,                    
            [Account_Id] varchar(50) NULL,                    
            [EXCEPTION_TYPE] [VARCHAR](MAX) NULL,                    
            [EXCEPTION_STATUS_TYPE] [VARCHAR](MAX) NULL,                    
            [ASSIGNEE] [VARCHAR](MAX) NULL,                    
            [CLIENTID] INT NULL,                    
            [CLIENT] [VARCHAR](MAX) NULL,                    
            [COMMODITY] [VARCHAR](MAX) NULL,                    
            [DATA SOURCE] [VARCHAR](MAX) NULL,                    
            [DATE_IN_QUEUE] [DATETIME] NULL,                    
            [DATE_IN_CBMS] [DATETIME] NULL,                    
            [FILEID] INT NULL,                    
            [FILENAME] [VARCHAR](MAX) NULL,                    
            [SERVICE_MONTH] [VARCHAR](MAX) NULL,                    
            [SITE] [VARCHAR](MAX) NULL,                    
            [COUNTRY] [NVARCHAR](MAX) NULL,                    
            [STATE] [NVARCHAR](MAX) NULL,                    
            [VENDOR] [NVARCHAR](MAX) NULL,                    
            [VENDOR_TYPE] [NVARCHAR](MAX) NULL,                    
            [COMMENTS] VARCHAR(MAX) NULL,                    
            [is_priority] INT NULL ,
						[priority_cd] numeric (10,0)                
                     
        );                  
                    
        CREATE TABLE #DEFAULTEXCEPTIONSYSTEMGROUP                    
        (                    
            UserGroupId INT NULL,                    
            UserGroupName VARCHAR(300) NULL,                    
            usergroupcount INT NULL,                    
            systemgroupid INT NULL,                    
            SystemGroupCount INT NULL,                    
            ubmid INT NULL,                    
            ubmaccountcode VARCHAR(200) NULL,                    
            [RN] [BIGINT] NULL,                    
            [CU_INVOICE_ID] [INT] NULL,                    
            [ACCOUNT] [VARCHAR](MAX) NULL,                    
            [Account_Id] varchar(50) NULL,                    
            [EXCEPTION_TYPE] [VARCHAR](MAX) NULL,                    
            [EXCEPTION_STATUS_TYPE] [VARCHAR](MAX) NULL,                    
            [ASSIGNEE] [VARCHAR](MAX) NULL,                    
            [CLIENTID] INT NULL,                    
            [CLIENT] [VARCHAR](MAX) NULL,              
            [COMMODITY] [VARCHAR](MAX) NULL,                    
            [DATA SOURCE] [VARCHAR](MAX) NULL,                    
            [DATE_IN_QUEUE] [DATETIME] NULL,                    
            [DATE_IN_CBMS] [DATETIME] NULL,                                
			[FILEID] INT NULL,                    
            [FILENAME] [VARCHAR](MAX) NULL,                    
            [SERVICE_MONTH] [VARCHAR](MAX) NULL,                    
            [SITE] [VARCHAR](MAX) NULL,                    
            [COUNTRY] [NVARCHAR](MAX) NULL,                    
            [STATE] [NVARCHAR](MAX) NULL,                    
            [VENDOR] [NVARCHAR](MAX) NULL,     
            [VENDOR_TYPE] [NVARCHAR](MAX) NULL,                    
            [COMMENTS] VARCHAR(MAX) NULL,                    
            [is_priority] INT  NULL ,
			[priority_cd] numeric (10,0)                
              
        );      
		
		
		   CREATE TABLE #DEFAULTEXCEPTIONSYSTEMGROUP_DETAIL                    
        (                    
            UserGroupId INT NULL,                    
            UserGroupName VARCHAR(300) NULL,                    
            usergroupcount INT NULL,                    
            systemgroupid INT NULL,                    
            SystemGroupCount INT NULL,                    
            ubmid INT NULL,                    
            ubmaccountcode VARCHAR(200) NULL,                    
            [RN] [BIGINT] NULL,                    
            [CU_INVOICE_ID] [INT] NULL,                    
            [ACCOUNT] [VARCHAR](MAX) NULL,                    
            [Account_Id] varchar(50) NULL,                    
            [EXCEPTION_TYPE] [VARCHAR](MAX) NULL,                    
            [EXCEPTION_STATUS_TYPE] [VARCHAR](MAX) NULL,                    
            [ASSIGNEE] [VARCHAR](MAX) NULL,                    
            [CLIENTID] INT NULL,                    
            [CLIENT] [VARCHAR](MAX) NULL,              
            [COMMODITY] [VARCHAR](MAX) NULL,                    
            [DATA SOURCE] [VARCHAR](MAX) NULL,                    
            [DATE_IN_QUEUE] [DATETIME] NULL,                    
            [DATE_IN_CBMS] [DATETIME] NULL,                                
			[FILEID] INT NULL,                    
            [FILENAME] [VARCHAR](MAX) NULL,                    
            [SERVICE_MONTH] [VARCHAR](MAX) NULL,                    
            [SITE] [VARCHAR](MAX) NULL,                    
            [COUNTRY] [NVARCHAR](MAX) NULL,                    
            [STATE] [NVARCHAR](MAX) NULL,                    
            [VENDOR] [NVARCHAR](MAX) NULL,     
            [VENDOR_TYPE] [NVARCHAR](MAX) NULL,                    
            [COMMENTS] VARCHAR(MAX) NULL,                    
            [is_priority] INT  NULL ,
			[priority_cd] numeric (10,0)                
              
        );    
		              
                    
        DECLARE @DefaultExceptionsystemgrouptemp TABLE                    
        (                    
            systemgroupid INT IDENTITY (1,1) NOT NULL,                
			ubmid INT NULL,                    
            ubmaccountcode VARCHAR(200) NULL,                    
            exception_type VARCHAR(200) NULL,                    
            systemgrouptotalcount INT NULL                    
        );                    
                    
        DECLARE @DefaultExceptionusergrouptemp TABLE                    
        (                    
            usergroupid INT NULL,                    
            usergroupname VARCHAR(300) NULL,                    
            usergrouptotalcount INT NULL                    
        );                    
                    
        CREATE TABLE #DEFAULTEXCEPTIONSFINAL                    
        (                    
            UserGroupId INT NULL,                    
            UserGroupName VARCHAR(300) NULL,                    
            usergroupcount INT NULL,                    
            systemgroupid INT NULL,                    
            SystemGroupCount INT NULL,                    
            ubmid INT NULL,                    
            ubmaccountcode VARCHAR(200) NULL,                
            [CU_INVOICE_ID] INT NULL,                    
			[ACCOUNT] [VARCHAR](MAX) NULL,                    
            [Account_Id] varchar(50) NULL,                    
            [EXCEPTION_TYPE] [VARCHAR](MAX) NULL,                    
            [EXCEPTION_STATUS_TYPE] [VARCHAR](MAX) NULL,                    
            [ASSIGNEE] [VARCHAR](MAX) NULL,                    
            [CLIENTID] INT NULL,                    
            [CLIENT] [VARCHAR](MAX) NULL,                    
            [COMMODITY] [VARCHAR](MAX) NULL,                    
            [DATA SOURCE] [VARCHAR](MAX) NULL,                    
            [DATE_IN_QUEUE] [DATETIME] NULL,                    
            [DATE_IN_CBMS] [DATETIME] NULL,                    
            [FILEID] INT NULL,                    
            [FILENAME] [VARCHAR](MAX) NULL,                    
            [SERVICE_MONTH] [VARCHAR](MAX) NULL,                    
            [SITE] [VARCHAR](MAX) NULL,                    
            [COUNTRY] [NVARCHAR](MAX) NULL,                    
            [STATE] [NVARCHAR](MAX) NULL,                    
            [VENDOR] [NVARCHAR](MAX) NULL,                    
            [VENDOR_TYPE] [NVARCHAR](MAX) NULL,                    
            [COMMENTS] VARCHAR(MAX) NULL,                    
            [is_priority] INT  NULL ,
			[priority_cd] numeric (10,0)  
			                 
        );                    
                    
                    
        CREATE TABLE #DEFAULTEXCEPTIONS_Sorting                    
        (                    
            index_range INT NULL,                    
            UserGroupId INT NULL,                    
            UserGroupName VARCHAR(300) NULL,                    
            usergroupcount INT NULL,                    
            systemgroupid INT NULL,              
            SystemGroupCount INT NULL,       
            ubmid INT NULL,                    
            ubmaccountcode VARCHAR(200) NULL,                    
            [CU_INVOICE_ID] INT NULL,                    
            [ACCOUNT] [VARCHAR](MAX) NULL,                    
            [Account_Id] varchar(50) NULL,                    
            [EXCEPTION_TYPE] [VARCHAR](MAX) NULL,                    
            [EXCEPTION_STATUS_TYPE] [VARCHAR](MAX) NULL,                    
            [ASSIGNEE] [VARCHAR](MAX) NULL,                    
            [CLIENTID] INT NULL,                    
            [CLIENT] [VARCHAR](MAX) NULL,                    
            [COMMODITY] [VARCHAR](MAX) NULL,                    
            [DATA SOURCE] [VARCHAR](MAX) NULL,                    
            [DATE_IN_QUEUE] [DATETIME] NULL,                    
			[DATE_IN_CBMS] [DATETIME] NULL,                    
            [FILEID] INT NULL,                    
            [FILENAME] [VARCHAR](MAX) NULL,                    
            [SERVICE_MONTH] [VARCHAR](MAX) NULL,                    
            [SITE] [VARCHAR](MAX) NULL,                    
            [COUNTRY] [NVARCHAR](MAX) NULL,                    
            [STATE] [NVARCHAR](MAX) NULL,                    
            [VENDOR] [NVARCHAR](MAX) NULL,                    
            [VENDOR_TYPE] [NVARCHAR](MAX) NULL,                    
            [COMMENTS] VARCHAR(MAX) NULL,                    
            [is_priority] INT  NULL ,
			[priority_cd] numeric (10,0)                
                   
        );                    
                    
  
        SET @SQL1              
            = '   insert #DEFAULTEXCEPTIONS                      
    select                     
   NULL,                      
   null,                    
   null,                     
   null,                    
   null,                    
    CI.UBM_ID,                    
    CI.UBM_ACCOUNT_CODE,                    
     ROW_NUMBER ()OVER(PARTITION BY CUEX.CU_INVOICE_ID ORDER BY (select null)    ) AS RN,                       
   CUEX.CU_INVOICE_ID                      
    , ( CASE WHEN CUEX.EXCEPTION_TYPE = ' + '''' + 'WRONG ACCOUNT' + ''''                    
              + ' THEN NULL                      
             WHEN AG.ACCOUNT_GROUP_ID IS NULL                      
                   THEN COALESCE(CHA.Account_Number, CUEX.UBM_Account_Number,INV_LBL.ACCOUNT_NUMBER)                      
             ELSE COALESCE( AG.GROUP_BILLING_NUMBER,CHA.ACCOUNT_NUMBER, CUEX.UBM_ACCOUNT_NUMBER,INV_LBL.account_number)                           
        END ) AS ACCOUNT                 ,(CASE WHEN CUEX.EXCEPTION_TYPE = ' + '''' + 'WRONG ACCOUNT' + ''''                    
              + ' THEN NULL  
			  when    AG.ACCOUNT_GROUP_ID IS NOT NULL then '+''''+'MULTIPLE'+''''+           
        'ELSE COALESCE(cast(CHA.Account_Id as varchar), cast(CUEX.Account_Id as varchar))   
        END ) AS ACCOUNT_ID                     
    , CUEX.EXCEPTION_TYPE                      
    , CUEX.EXCEPTION_STATUS_TYPE                       
    , UI.FIRST_NAME + ' + '''' + ' ' + ''''                    
              + '+ UI.LAST_NAME ASSIGNEE                      
   , CASE WHEN CUEX.CLIENT_HIER_ID IS NOT NULL                      
                 THEN CH.Client_Id                      
           ELSE  CI.CLient_id                       
      END CLIENT_ID                      
    , CASE WHEN CUEX.CLIENT_HIER_ID IS NOT NULL                      
                 THEN CH.CLIENT_NAME                      
           ELSE  coalesce(CL.CLient_Name,CUEX.UBM_CLIENT_NAME , INV_LBL.CLIENT_NAME)                    
      END CLIENT                        
    , ISNULL(COM.COMMODITY_NAME , INV_LBL.COMMODITY) AS  COMMODITY                      
 , UBM.UBM_NAME [DATA SOURCE]                      
    , CUEX.DATE_IN_QUEUE                      
    , CUEX.DATE_IN_CBMS                      
 , CUEX.CBMS_IMAGE_ID [FILEID]                    
    , CUEX.CBMS_DOC_ID [FILENAME]                      
    , ( CASE WHEN CUEX.EXCEPTION_TYPE = ' + '''' + 'WRONG MONTH' + ''''                    
              + 'THEN NULL                      
             WHEN AG.ACCOUNT_GROUP_ID IS NULL and SM.SERVICE_MONTH is not null THEN CONVERT(VARCHAR(10),  SM.SERVICE_MONTH , 101)                      
             when CUEX.SERVICE_MONTH is not null then CUEX.SERVICE_MONTH                 
    else null  END ) AS SERVICE_MONTH                       
    , CASE WHEN CUEX.CLIENT_HIER_ID IS NOT NULL                      
                 THEN isnull(CH.SITE_NAME , inv_lbl.site_name)                     
           ELSE  isnull(CUEX.UBM_SITE_NAME  , inv_lbl.site_name)                    
      END [SITE]                      
    ,isnull( C.country_name, INV_LBL.COUNTRY) as country,                      
 isnull(S.state_name, INV_LBL.STATE_NAME) as state,                      
  CASE WHEN  CI.ACCOUNT_GROUP_ID IS NULL THEN isnull(CHA.Account_Vendor_Name  ,INV_LBL.VENDOR_NAME )                    
     WHEN CI.ACCOUNT_GROUP_ID IS NOT NULL THEN isnull(V.VENDOR_NAME  ,INV_LBL.VENDOR_NAME )                     
     ELSE INV_LBL.VENDOR_NAME                      
     end  AS VENDOR                       
 ,  CASE WHEN  CI.ACCOUNT_GROUP_ID IS NULL THEN isnull(AVT.ENTITY_NAME ,inv_lbl.vendor_type )                     
   WHEN CI.ACCOUNT_GROUP_ID IS NOT NULL THEN isnull(VT.ENTITY_NAME  ,inv_lbl.vendor_type )                     
     ELSE inv_lbl.vendor_type                      
     end AS VENDOR_TYPE                       
    , NULL,                    
 isnull(cia.is_priority ,0)    ,
 case when cia.is_priority is null or cia.is_priority=0
 then 9999999
 else cia.source_cd
 end             
FROM  DBO.CU_EXCEPTION_DENORM CUEX                      
  JOIN dbo.QUEUE q ON q.QUEUE_ID = CUEX.QUEUE_ID                      
                 INNER JOIN dbo.Entity qt ON qt.ENTITY_ID = q.QUEUE_TYPE_ID                       
   JOIN                      
      DBO.CU_INVOICE CI                      
            ON CI.CU_INVOICE_ID = CUEX.CU_INVOICE_ID 
			LEFT JOIN CLIENT CL 
			on CL.CLIENT_ID = CI.CLIENT_ID                    
      LEFT JOIN                      
      DBO.CU_INVOICE_SERVICE_MONTH SM                      
            ON SM.CU_INVOICE_ID = CUEX.CU_INVOICE_ID                      
               AND SM.ACCOUNT_ID = CUEX.ACCOUNT_ID                      
      LEFT OUTER JOIN                      
      DBO.USER_INFO UI                      
            ON UI.QUEUE_ID = CUEX.QUEUE_ID                      
      LEFT JOIN                      
      DBO.CU_EXCEPTION_TYPE CET                      
            ON CUEX.EXCEPTION_TYPE = CET.EXCEPTION_TYPE                      
   LEFT JOIN WORKFLOW.CU_INVOICE_ATTRIBUTE CIA                       
 ON  CIA.CU_INVOICE_ID = cuex.CU_INVOICE_ID                     
      LEFT JOIN                      
      DBO.ACCOUNT_GROUP AG                      
            ON AG.ACCOUNT_GROUP_ID = CI.ACCOUNT_GROUP_ID                      
      LEFT JOIN                      
      (DBO.VENDOR V                      
       INNER JOIN                      
       DBO.ENTITY VT           
             ON VT.ENTITY_ID = V.VENDOR_TYPE_ID)                      
            ON V.VENDOR_ID = AG.VENDOR_ID                      
                      
      LEFT JOIN                      
      CORE.CLIENT_HIER_ACCOUNT CHA                      
            ON CHA.CLIENT_HIER_ID = CUEX.CLIENT_HIER_ID                      
               AND CHA.ACCOUNT_ID = CUEX.ACCOUNT_ID                       
       LEFT JOIN                      
      (DBO.VENDOR AV                      
       INNER JOIN                      
       DBO.ENTITY AVT                      
             ON AVT.ENTITY_ID = AV.VENDOR_TYPE_ID)                      
            ON AV.VENDOR_ID = CHA.Account_Vendor_Id                      
      LEFT JOIN                      
 CORE.CLIENT_HIER CH                      
            ON CH.CLIENT_HIER_ID = CUEX.CLIENT_HIER_ID                       
 LEFT JOIN COUNTRY c                      
       ON CH.COUNTRY_ID = C.COUNTRY_ID                       
   LEFT JOIN state s                      
            ON CH.state_id = s.state_id                         
  LEFT JOIN                      
      DBO.COMMODITY COM                      
            ON COM.COMMODITY_ID = CHA.COMMODITY_ID                      
      LEFT JOIN                      
      DBO.CU_INVOICE_LABEL INV_LBL                      
            ON CUEX.CU_INVOICE_ID = INV_LBL.CU_INVOICE_ID                      
      LEFT JOIN                      
      DBO.UBM UBM                      
            ON UBM.UBM_ID = CI.UBM_ID                    
  LEFT JOIN dbo.Entity status_type ON status_type.ENTITY_name = cuex.EXCEPTION_STATUS_TYPE                     
  AND STATUS_TYPE.ENTITY_description =' + '''' + 'UBM Exception Status' + ''''                    
              + ' OUTER APPLY                      
      (     SELECT                      
                        UAC.ACCOUNT_VENDOR_NAME + ' + '''' + ', ' + ''''                    
              + 'FROM        CORE.CLIENT_HIER_ACCOUNT UAC                      
                      INNER JOIN                      
                        CORE.CLIENT_HIER_ACCOUNT SAC                      
                              ON SAC.METER_ID = UAC.METER_ID                      
            WHERE       UAC.ACCOUNT_TYPE = ' + '''' + 'UTILITY' + '''' + 'AND   SAC.ACCOUNT_TYPE = ' + ''''                    
              + 'SUPPLIER' + '''' + 'AND   CHA.ACCOUNT_TYPE = ' + '''' + 'SUPPLIER' + ''''                    
              + 'AND   SAC.ACCOUNT_ID = CHA.ACCOUNT_ID                      
            GROUP BY    UAC.ACCOUNT_VENDOR_NAME                      
            FOR XML PATH(' + '''' + ''''                    
              + ')) UV(VENDOR_NAME)                      
   WHERE  CUEX.EXCEPTION_TYPE <> ' + '''' + 'FAILED RECALC' + ''''                    
                
       
   set @sql2= CASE                    
                    WHEN @Account_Number IS NOT NULL THEN                    
                        ' and ( CASE WHEN CUEX.EXCEPTION_TYPE = ' + '''' + 'WRONG ACCOUNT' + ''''                    
                        + ' THEN NULL                      
             WHEN AG.ACCOUNT_GROUP_ID IS NULL                      
                   THEN COALESCE(CHA.Account_Number, CUEX.UBM_Account_Number,INV_LBL.ACCOUNT_NUMBER)                      
             ELSE  COALESCE( AG.GROUP_BILLING_NUMBER,CHA.ACCOUNT_NUMBER, CUEX.UBM_ACCOUNT_NUMBER,INV_LBL.account_number)                          
        END )   like'                                                             + '''%' + @Account_Number + '%'''                    
             ELSE                    
                        ''                    
                END + 
	 
	
	CASE WHEN  EXISTS (SELECT 1 FROM #temp_user_info WHERE User_type = 'Private Users')
				and exists (SELECT 1 FROM #temp_user_info WHERE User_type = 'Public Users')
						THEN ''

		 WHEN  EXISTS (SELECT 1 FROM #temp_user_info WHERE User_type = 'Private Users')
				 Then 'AND ( qt.[Entity_Name] = ' + '''' + 'Private' + ''''  + 
				 ' or  cuex.queue_id in (select user_info_id from #temp_user_info
				 where User_type ='+''''+'Regular Users'+''''+'))'

				 WHEN  EXISTS (SELECT 1 FROM #temp_user_info WHERE User_type = 'Public Users')
				 then 'AND ( qt.[Entity_Name] = ' + '''' + 'Public' + ''''  + 
				 ' or  cuex.queue_id in (select user_info_id from #temp_user_info
						where User_type ='+''''+'Regular Users'+''''+'))'
				
				when exists (select 1 from #temp_user_info WHERE User_type = 'Regular Users')
				Then 'and cuex.queue_id in (select user_info_id from #temp_user_info
				 where User_type ='+''''+'Regular Users'+''''+')'
				 else ''
				 end 
	               + 
			  CASE                    
             WHEN @Client IS NOT NULL THEN                    
                        'and CASE WHEN CUEX.CLIENT_HIER_ID IS NOT NULL                      
                 THEN CH.CLIENT_NAME                      
           ELSE  coalesce(CL.CLient_Name,CUEX.UBM_CLIENT_NAME , INV_LBL.CLIENT_NAME)                   
      END like ' + '''%' + @Client + '%'''                    
                    ELSE                    
                        ''                    
                END + CASE                    
                          WHEN @Commodity IS NOT NULL THEN                    
                              'and ISNULL(COM.COMMODITY_NAME , INV_LBL.COMMODITY) like  ' + '''%' + @Commodity + '%'''                    
                          ELSE                    
                              ''                    
                      END + CASE                    
                                WHEN @Data_Source IS NOT NULL AND @Data_Source = 0 THEN   
								'and CI.UBM_Invoice_id is null'
								WHEN @Data_Source IS NOT NULL AND @Data_Source <> 0 then                 
                                    'and ubm.UBM_ID=' + CAST(@Data_Source AS VARCHAR)                    
                                ELSE                    
                                    ''                    
                            END                    
             + CASE                    
                    WHEN @Start_Date_in_CBMS IS NOT NULL THEN                    
                        ' and cuex.Date_In_CBMS between ''' + CAST(@Start_Date_in_CBMS AS VARCHAR) + '''' + ' and '''                    
                        + CAST(@End_Date_in_CBMS AS VARCHAR) + ''''                    
                    ELSE                    
                        ''                    
                END                    
              + CASE                    
                    WHEN @Start_Date_in_Queue IS NOT NULL THEN                    
                        'and cuex.DATE_IN_QUEUE between ''' + CAST(@Start_Date_in_Queue AS VARCHAR) + '''' + ' and '''                    
                        + CAST(@End_Date_in_Queue AS VARCHAR) + ''''                    
                    ELSE                    
                        ''                    
                END + CASE             
                          WHEN @Filename IS NOT NULL THEN                       
         'and CUEX.CBMS_DOC_ID like ' + '''%' + @Filename + '%'''              
                          ELSE                    
                              ''                    
      END + CASE                    
                                WHEN @Invoice_ID IS NOT NULL THEN                    
                            'and cuex.CU_INVOICE_ID=' + @Invoice_ID                    
                                ELSE                    
                                    ''                    
                            END + CASE                    
                                      WHEN @Exception_Status IS NOT NULL THEN                    
                                          'and STATUS_TYPE.ENTITY_ID=' + CAST(@Exception_Status AS VARCHAR)                    
                                      ELSE                    
                                          ''                    
                                  END                    
              + CASE                    
                    WHEN @Month IS NOT NULL THEN                    
                        'and CASE WHEN CUEX.EXCEPTION_TYPE = ' + '''' + 'WRONG MONTH' + ''''                    
          + 'THEN NULL                      
             WHEN AG.ACCOUNT_GROUP_ID IS NULL and SM.SERVICE_MONTH is not null THEN CONVERT(VARCHAR(10),  SM.SERVICE_MONTH , 101)                      
             when CUEX.SERVICE_MONTH is not null then CUEX.SERVICE_MONTH    else null                    
    END'                    
                        + '=' + '''' + CONVERT(VARCHAR(10), @Month, 101) + ''''                    
                    ELSE                    
                        ''                    
                END                     
            + CASE                    
                    WHEN @Site IS NOT NULL THEN                    
                        'and CASE WHEN CUEX.CLIENT_HIER_ID IS NOT NULL                      
                 THEN isnull(CH.SITE_NAME , inv_lbl.site_name)                     
           ELSE  isnull(CUEX.UBM_SITE_NAME  , inv_lbl.site_name)                    
      END   like' + '''%' + @Site + '%'''                    
                    ELSE                    
                        ''                    
                END + CASE                    
                          WHEN @Exception_Type IS NOT NULL THEN                    
     'and cet.cu_exception_type_id in (select exception_type from #temp_exception_type)'                    
                          ELSE                    
                              ''                    
                      END + CASE                    
                                WHEN @City IS NOT NULL THEN                    
                                    'and ch.city=' + '''' + @City + ''''                    
                                ELSE                    
                                    ''                    
                            END + CASE                    
                                      WHEN   @Priority = 1 THEN        
                                          'and (CIA.Is_Priority =1)'             
            WHEN  @Priority = 0 THEN                    
                                          'and (CIA.Is_Priority is null or CIA.Is_Priority =0)'                   
                                      ELSE                    
                                          ''                    
                                  END                    
              + CASE                    
                    WHEN @Vendor IS NOT NULL THEN                    
                        'and   CASE WHEN  CI.ACCOUNT_GROUP_ID IS NULL THEN isnull(CHA.Account_Vendor_Name  ,INV_LBL.VENDOR_NAME )                    
     WHEN CI.ACCOUNT_GROUP_ID IS NOT NULL THEN isnull(V.VENDOR_NAME  ,INV_LBL.VENDOR_NAME )                     
     ELSE INV_LBL.VENDOR_NAME                      
     end like ' + '''%' + @Vendor + '%'''                    
                    ELSE                    
                        ''                    
                END                    
              + CASE                    
                    WHEN @Vendor_Type IS NOT NULL THEN                    
                        'and CASE WHEN  CI.ACCOUNT_GROUP_ID IS NULL THEN isnull(AVT.ENTITY_NAME ,inv_lbl.vendor_type )                     
   WHEN CI.ACCOUNT_GROUP_ID IS NOT NULL THEN isnull(VT.ENTITY_NAME  ,inv_lbl.vendor_type )                     
     ELSE inv_lbl.vendor_type                      
     end like' + '''%' + @Vendor_Type + '%'''                    
                    ELSE                    
                        ''                    
                END + CASE                    
                          WHEN @Country IS NOT NULL THEN                    
                              'and isnull( C.country_name, INV_LBL.COUNTRY) like ' + '''%' + @Country + '%'''                    
                          ELSE                    
                              ''                    
                      END + CASE                    
                                WHEN @State IS NOT NULL THEN                    
                                    'and isnull(S.state_name, INV_LBL.STATE_NAME) like ' + '''%' + @State + '%'''                    
                                ELSE                    
                                    ''                    
                            END;                    
           
		 
   set @sql = @sql1+ @sql2        
        
        EXECUTE sys.sp_executesql @SQL;                    
                    
        UPDATE DF                    
        SET DF.COMMODITY = CASE                    
                WHEN (               
                                    (                    
                                        SELECT COUNT(DISTINCT DF1.COMMODITY)                    
                                        FROM #DEFAULTEXCEPTIONS DF1                
                                        WHERE DF1.COMMODITY IS NOT NULL                    
                                              AND DF.CU_INVOICE_ID = DF1.CU_INVOICE_ID                    
                                    ) > 1                    
                                    ) THEN                    
                                   'MULTIPLE'                    
                               ELSE                    
                                   DF.COMMODITY                    
                           END,                    
            DF.SITE = CASE                    
                          WHEN (                    
                               (                    
                                   SELECT COUNT(DISTINCT DF1.SITE)                    
                                   FROM #DEFAULTEXCEPTIONS DF1                    
                                   WHERE DF1.SITE IS NOT NULL                    
                                         AND DF.CU_INVOICE_ID = DF1.CU_INVOICE_ID                    
                               ) > 1                    
                               ) THEN                    
                              'MULTIPLE'     
                          ELSE                    
                              DF.SITE                    
                      END,                    
            DF.ACCOUNT = CASE                    
                             WHEN (                    
                  (                    
                                      SELECT COUNT(DISTINCT DF1.ACCOUNT)                    
           FROM #DEFAULTEXCEPTIONS DF1                    
                                      WHERE DF1.ACCOUNT IS NOT NULL                    
                                            AND DF.CU_INVOICE_ID = DF1.CU_INVOICE_ID                    
                                  ) > 1                    
                                  ) THEN                    
                                 'MULTIPLE'                    
                             ELSE                    
                                 DF.ACCOUNT                    
                         END   ,
						                    
            DF.Account_Id = CASE                    
                             WHEN (                    
                  (                    
                                      SELECT COUNT(DISTINCT DF1.Account_Id)                    
           FROM #DEFAULTEXCEPTIONS DF1                    
                                      WHERE DF1.Account_Id IS NOT NULL                    
                                            AND DF.CU_INVOICE_ID = DF1.CU_INVOICE_ID                    
                                  ) > 1                    
                                  ) THEN                    
                                 'MULTIPLE'                    
                             ELSE                    
                                 DF.Account_Id                    
                         END                   
        FROM #DEFAULTEXCEPTIONS DF;                    
                    
                    
        DELETE FROM #DEFAULTEXCEPTIONS                    
        WHERE RN <> 1;                    
                    
        IF (@Comments IS NOT NULL)                    
        BEGIN                    
                    
            UPDATE DFG                    
            SET DFG.COMMENTS =                    
                (                    
                    SELECT TOP (1)                    
                           CUI.IMAGE_COMMENT                    
                    FROM dbo.CU_INVOICE_COMMENT CUI                    
                    WHERE CUI.CU_INVOICE_ID = DFG.CU_INVOICE_ID                    
                          AND CUI.IMAGE_COMMENT = @Comments                    
                    ORDER BY CUI.CU_INVOICE_COMMENT_ID DESC                    
                )                    
            FROM #DEFAULTEXCEPTIONS DFG;                    
                    
            DELETE FROM #DEFAULTEXCEPTIONS                    
            WHERE COMMENTS IS NULL;                    
                    
        END;                    
                    
        ELSE                    
        BEGIN                    
                    
            UPDATE DFG                    
            SET DFG.COMMENTS =                    
                (                    
                    SELECT TOP (1)                    
                           CUI.IMAGE_COMMENT                    
                    FROM dbo.CU_INVOICE_COMMENT CUI   
					WHERE CUI.CU_INVOICE_ID = DFG.CU_INVOICE_ID                    
                    ORDER BY CUI.CU_INVOICE_COMMENT_ID DESC                    
                )                    
            FROM #DEFAULTEXCEPTIONS DFG;                    
                    
        END;                    
                    
                    
              
 
                    
        ---STEP 1 UPDATE THE SYSTEM GROUP, GROUP THE INVOICES                      
                    
                    
        INSERT @DefaultExceptionsystemgrouptemp                    
        (                    
                               
            ubmid,                    
            ubmaccountcode,                    
            systemgrouptotalcount,                    
            exception_type                    
        )                    
 
    select   
     
	CI.UBM_ID,                   
   CI.UBM_ACCOUNT_CODE,
    count(distinct CI.CU_INVOICE_ID) as system_group_total_count,   
   'UNKNOWN ACCOUNT' AS exception_type    
     from dbo.CU_EXCEPTION_DENORM CUEX   
  join dbo.CU_INVOICE CI                      
     ON CI.CU_INVOICE_ID = CUEX.CU_INVOICE_ID   
     join #DEFAULTEXCEPTIONS DF   
     on DF.ubmid = CI.UBM_ID  
     and DF.ubmaccountcode =CI.UBM_ACCOUNT_CODE  
  where CUEX.EXCEPTION_TYPE ='unknown account'  
  and     CI.UBM_ID is not null                     
   and    CI.UBM_ACCOUNT_CODE is not null  
  group by     CI.UBM_ID,                    
   CI.UBM_ACCOUNT_CODE    
    HAVING COUNT(1) > 1;          
                    
        UPDATE DF                    
        SET 
		DF.systemgroupid = D.systemgroupid,                    
            DF.SystemGroupCount = D.systemgrouptotalcount                    
        FROM #DEFAULTEXCEPTIONS DF                    
            JOIN @DefaultExceptionsystemgrouptemp D                    
                ON DF.ubmid = D.ubmid                    
                   AND DF.ubmaccountcode = D.ubmaccountcode                    
                    AND DF.EXCEPTION_TYPE = D.exception_type;    
     

	  --STEP 3 MOVE SYSTEM GROUP TO SEPARATE TABLE                    
        INSERT INTO #DEFAULTEXCEPTIONSYSTEMGROUP_DETAIL                    
        (                    
            UserGroupId,                    
            UserGroupName,                    
            usergroupcount,                    
            systemgroupid,                    
			SystemGroupCount,                    
            ubmid,                    
            ubmaccountcode,                    
            RN,                    
            CU_INVOICE_ID,                    
            ACCOUNT,                    
            Account_Id,                    
            EXCEPTION_TYPE,                    
            EXCEPTION_STATUS_TYPE,                    
            ASSIGNEE,                    
            CLIENTID,                    
            CLIENT,                    
			COMMODITY,                    
            [DATA SOURCE],                    
            DATE_IN_QUEUE,                    
            DATE_IN_CBMS,                    
            FILEID,                    
            [FILENAME],                    
            SERVICE_MONTH,                    
            [SITE],                    
            COUNTRY,                    
            [STATE],                    
            VENDOR,                    
            VENDOR_TYPE,                    
            COMMENTS,                    
            is_priority   ,
	          [priority_cd]       
        )                    
 
SELECT NULL, 
       NULL, 
       NULL, 
       DF.systemgroupid, 
       DF.SystemGroupCount, 
       CI.UBM_ID, 
       CI.UBM_ACCOUNT_CODE, 
       Row_number () 
         OVER( 
           partition BY CUEX.CU_INVOICE_ID  
           ORDER BY (select null) )                  AS RN, 
       CUEX.CU_INVOICE_ID, 
       
  ( CASE 
           WHEN CUEX.EXCEPTION_TYPE = 'WRONG ACCOUNT' THEN NULL 
           WHEN AG.ACCOUNT_GROUP_ID IS NULL THEN 
           COALESCE(CHA.Account_Number, 
           CUEX.UBM_Account_Number, INV_LBL.ACCOUNT_NUMBER) 
           ELSE  COALESCE( AG.GROUP_BILLING_NUMBER,CHA.Account_Number, CUEX.UBM_Account_Number,INV_LBL.ACCOUNT_NUMBER)  
         END )                                       AS ACCOUNT, 
       ( CASE 
           WHEN CUEX.EXCEPTION_TYPE = 'WRONG ACCOUNT' THEN NULL 
		when    AG.ACCOUNT_GROUP_ID IS NOT NULL then  'MULTIPLE' 
         ELSE COALESCE(cast(CHA.Account_Id as varchar), cast(CUEX.Account_ID as varchar))  
         
         END )                                       AS ACCOUNT_ID,  
       CUEX.EXCEPTION_TYPE, 
       CUEX.EXCEPTION_STATUS_TYPE, 
       UI.FIRST_NAME + ' ' + UI.LAST_NAME            ASSIGNEE, 
       CASE 
         WHEN CUEX.Client_Hier_ID IS NOT NULL THEN CH.Client_Id 
         ELSE CI.CLIENT_ID 
       END                                           CLIENT_ID, 
       CASE 
         WHEN CUEX.Client_Hier_ID IS NOT NULL THEN CH.Client_Name 
         ELSE coalesce(CL.CLIENT_NAME,CUEX.UBM_Client_Name , INV_LBL.CLIENT_NAME)
       END                                           CLIENT, 
       Isnull(COM.Commodity_Name, INV_LBL.COMMODITY) AS COMMODITY, 
       UBM.UBM_NAME                                  [DATA SOURCE], 
       CUEX.DATE_IN_QUEUE, 
       CUEX.Date_In_CBMS, 
       CUEX.CBMS_IMAGE_ID                            [FILEID], 
       CUEX.CBMS_DOC_ID                              [FILENAME], 
       ( CASE 
           WHEN CUEX.EXCEPTION_TYPE = 'WRONG MONTH'THEN NULL 
           WHEN AG.ACCOUNT_GROUP_ID IS NULL 
                AND SM.SERVICE_MONTH IS NOT NULL THEN 
           CONVERT(VARCHAR(10), SM.SERVICE_MONTH, 101) 
           WHEN CUEX.SERVICE_MONTH IS NOT NULL THEN CUEX.SERVICE_MONTH 
           ELSE NULL 
         END )                                       AS SERVICE_MONTH, 
       CASE 
         WHEN CUEX.Client_Hier_ID IS NOT NULL THEN 
         Isnull(CH.Site_name, INV_LBL.Site_Name) 
         ELSE Isnull(CUEX.UBM_Site_Name, INV_LBL.Site_Name) 
       END                                           [SITE], 
       Isnull(c.COUNTRY_NAME, INV_LBL.COUNTRY)       AS country, 
       Isnull(s.STATE_NAME, INV_LBL.STATE_NAME)      AS state, 
       CASE 
         WHEN CI.ACCOUNT_GROUP_ID IS NULL THEN 
         Isnull(CHA.Account_Vendor_Name, INV_LBL.VENDOR_NAME) 
         WHEN CI.ACCOUNT_GROUP_ID IS NOT NULL THEN 
         Isnull(V.VENDOR_NAME, INV_LBL.VENDOR_NAME) 
         ELSE INV_LBL.VENDOR_NAME 
       END                                           AS VENDOR, 
       CASE 
         WHEN CI.ACCOUNT_GROUP_ID IS NULL THEN 
         Isnull(AVT.ENTITY_NAME, INV_LBL.VENDOR_TYPE) 
         WHEN CI.ACCOUNT_GROUP_ID IS NOT NULL THEN 
         Isnull(VT.ENTITY_NAME, INV_LBL.VENDOR_TYPE) 
         ELSE INV_LBL.VENDOR_TYPE 
       END                                           AS VENDOR_TYPE, 
       NULL, 
       Isnull(CIA.Is_Priority, 0),
	 case when CIA.Is_Priority is null or CIA.Is_Priority=0
 then 9999999
 else CIA.Source_Cd
 end 
FROM   dbo.CU_EXCEPTION_DENORM CUEX 
       JOIN dbo.QUEUE q 
         ON q.QUEUE_ID = CUEX.QUEUE_ID 
       INNER JOIN dbo.ENTITY qt 
               ON qt.ENTITY_ID = q.QUEUE_TYPE_ID 
       JOIN dbo.CU_INVOICE CI 
         ON CI.CU_INVOICE_ID = CUEX.CU_INVOICE_ID
		 join   #DEFAULTEXCEPTIONS DF 
		on	DF.ubmid = CI.UBM_ID                     
                   AND DF.ubmaccountcode = CI.UBM_ACCOUNT_CODE  
LEFT JOIN dbo.CLIENT CL 
			on CL.CLIENT_ID = CI.CLIENT_ID  
       LEFT JOIN dbo.CU_INVOICE_SERVICE_MONTH SM 
              ON SM.CU_INVOICE_ID = CUEX.CU_INVOICE_ID 
                 AND SM.Account_ID = CUEX.Account_ID 
       LEFT OUTER JOIN dbo.USER_INFO UI 
                    ON UI.QUEUE_ID = CUEX.QUEUE_ID 
       LEFT JOIN dbo.CU_EXCEPTION_TYPE CET 
              ON CUEX.EXCEPTION_TYPE = CET.EXCEPTION_TYPE 
       LEFT JOIN Workflow.Cu_Invoice_Attribute CIA 
              ON CIA.CU_INVOICE_ID = CUEX.CU_INVOICE_ID 
       LEFT JOIN dbo.ACCOUNT_GROUP AG 
              ON AG.ACCOUNT_GROUP_ID = CI.ACCOUNT_GROUP_ID 
       LEFT JOIN (dbo.VENDOR V 
                  INNER JOIN dbo.ENTITY VT 
                          ON VT.ENTITY_ID = V.VENDOR_TYPE_ID) 
              ON V.VENDOR_ID = AG.VENDOR_ID 
       LEFT JOIN Core.Client_Hier_Account CHA 
              ON CHA.Client_Hier_Id = CUEX.Client_Hier_ID 
                 AND CHA.Account_Id = CUEX.Account_ID 
       LEFT JOIN (dbo.VENDOR AV 
                  INNER JOIN dbo.ENTITY AVT 
                          ON AVT.ENTITY_ID = AV.VENDOR_TYPE_ID) 
              ON AV.VENDOR_ID = CHA.Account_Vendor_Id 
       LEFT JOIN Core.Client_Hier CH 
              ON CH.Client_Hier_Id = CUEX.Client_Hier_ID 
       LEFT JOIN dbo.COUNTRY c 
              ON CH.Country_Id = c.COUNTRY_ID 
       LEFT JOIN dbo.STATE s 
              ON CH.State_Id = s.STATE_ID 
       LEFT JOIN dbo.Commodity COM 
              ON COM.Commodity_Id = CHA.Commodity_Id 
       LEFT JOIN dbo.CU_INVOICE_LABEL INV_LBL 
              ON CUEX.CU_INVOICE_ID = INV_LBL.CU_INVOICE_ID 
       LEFT JOIN dbo.UBM UBM 
              ON UBM.UBM_ID = CI.UBM_ID 
       LEFT JOIN dbo.ENTITY status_type 
              ON status_type.ENTITY_NAME = CUEX.EXCEPTION_STATUS_TYPE 
                 AND status_type.ENTITY_DESCRIPTION = 'UBM Exception Status' 
       OUTER apply (SELECT UAC.Account_Vendor_Name + ', ' 
                    FROM   Core.Client_Hier_Account UAC 
                           INNER JOIN Core.Client_Hier_Account SAC 
                                   ON SAC.Meter_Id = UAC.Meter_Id 
                    WHERE  UAC.Account_Type = 'UTILITY' 
                           AND SAC.Account_Type = 'SUPPLIER' 
                           AND CHA.Account_Type = 'SUPPLIER' 
                           AND SAC.Account_Id = CHA.Account_Id 
                    GROUP  BY UAC.Account_Vendor_Name 
                    FOR xml path('')) UV(vendor_name) 
WHERE  CUEX.EXCEPTION_TYPE <> 'FAILED RECALC' 
and CUEX.EXCEPTION_TYPE = 'unknown account'  
and DF.systemgroupid is not null  
                
		  
		   UPDATE DF                    
        SET DF.COMMODITY = CASE                    
                WHEN (               
                                    (                    
                                        SELECT COUNT(DISTINCT DF1.COMMODITY)                    
                                        FROM #DEFAULTEXCEPTIONSYSTEMGROUP_DETAIL   DF1                
                                        WHERE DF1.COMMODITY IS NOT NULL                    
                                              AND DF.CU_INVOICE_ID = DF1.CU_INVOICE_ID                    
                                    ) > 1                    
                                    ) THEN                    
                                   'MULTIPLE'                    
                               ELSE                    
                                   DF.COMMODITY                    
                           END,                    
            DF.SITE = CASE                    
                          WHEN (                    
                               (                    
                                   SELECT COUNT(DISTINCT DF1.SITE)                    
                                   FROM #DEFAULTEXCEPTIONSYSTEMGROUP_DETAIL   DF1                    
                                   WHERE DF1.SITE IS NOT NULL                    
                                         AND DF.CU_INVOICE_ID = DF1.CU_INVOICE_ID                    
                               ) > 1                    
                               ) THEN                    
                              'MULTIPLE'     
                          ELSE                    
                              DF.SITE                    
                      END,                    
            DF.ACCOUNT = CASE                    
                             WHEN (                    
                  (                    
                                      SELECT COUNT(DISTINCT DF1.ACCOUNT)                    
           FROM #DEFAULTEXCEPTIONSYSTEMGROUP_DETAIL   DF1                    
                                      WHERE DF1.ACCOUNT IS NOT NULL                    
                                            AND DF.CU_INVOICE_ID = DF1.CU_INVOICE_ID                    
                                  ) > 1                    
                                  ) THEN                    
                                 'MULTIPLE'                    
                             ELSE                    
                                 DF.ACCOUNT                    
                         END                    
        FROM #DEFAULTEXCEPTIONSYSTEMGROUP_DETAIL   DF;   
		
		                 
                             
        DELETE FROM #DEFAULTEXCEPTIONSYSTEMGROUP_DETAIL                    
        WHERE RN <> 1;   
		
		             
        IF (@Comments IS NOT NULL)                    
        BEGIN                    
                    
            UPDATE DFG                    
            SET DFG.COMMENTS =                    
                (                    
                    SELECT TOP (1)                    
                           CUI.IMAGE_COMMENT                    
                    FROM dbo.CU_INVOICE_COMMENT CUI                    
                    WHERE CUI.CU_INVOICE_ID = DFG.CU_INVOICE_ID                    
                          AND CUI.IMAGE_COMMENT = @Comments                    
                    ORDER BY CUI.CU_INVOICE_COMMENT_ID DESC                    
                )                    
            FROM #DEFAULTEXCEPTIONSYSTEMGROUP_DETAIL DFG;                    
           
   
          
        END;                    
                    
        ELSE                    
        BEGIN                    
                    
            UPDATE DFG                    
            SET DFG.COMMENTS =                    
                (                    
                    SELECT TOP (1)                    
                           CUI.IMAGE_COMMENT                    
                    FROM dbo.CU_INVOICE_COMMENT CUI   
					WHERE CUI.CU_INVOICE_ID = DFG.CU_INVOICE_ID                    
                    ORDER BY CUI.CU_INVOICE_COMMENT_ID DESC                    
                )                    
            FROM #DEFAULTEXCEPTIONSYSTEMGROUP_DETAIL DFG;                    
                    
        END;                    
                   
		
	 

    
   --NEW TABLE IS CREATED TO KEEP THE DETAILED AND MASTER DATA
   INSERT INTO #DEFAULTEXCEPTIONSYSTEMGROUP
			( UserGroupId,                    
            UserGroupName,                    
            usergroupcount,                    
            systemgroupid,                    
			SystemGroupCount,                    
            ubmid,                    
            ubmaccountcode,                    
            RN,                    
            CU_INVOICE_ID,                    
            ACCOUNT,                    
            Account_Id,                    
            EXCEPTION_TYPE,                    
            EXCEPTION_STATUS_TYPE,                    
            ASSIGNEE,                    
            CLIENTID,                    
            CLIENT,                    
			COMMODITY,                    
            [DATA SOURCE],                    
            DATE_IN_QUEUE,                    
            DATE_IN_CBMS,                    
            FILEID,                    
            [FILENAME],                    
            SERVICE_MONTH,                    
            [SITE],                    
            COUNTRY,                    
            [STATE],                    
            VENDOR,                    
            VENDOR_TYPE,                    
            COMMENTS,                    
            is_priority ,
			[priority_cd] ) 

   SELECT  UserGroupId,                    
            UserGroupName,                    
            usergroupcount,                    
            systemgroupid,                    
			SystemGroupCount,                    
            ubmid,                    
            ubmaccountcode,                    
            ROW_NUMBER() OVER(  PARTITION BY systemgroupid ORDER BY  CU_INVOICE_ID asc  ),                    
            CU_INVOICE_ID,                    
            ACCOUNT,                    
            Account_Id,                    
            EXCEPTION_TYPE,                    
            EXCEPTION_STATUS_TYPE,                    
            ASSIGNEE,                    
            CLIENTID,                    
            CLIENT,                    
			COMMODITY,                    
            [DATA SOURCE],                    
            DATE_IN_QUEUE,                    
            DATE_IN_CBMS,                    
            FILEID,                    
            [FILENAME],                    
            SERVICE_MONTH,                    
            [SITE],                    
            COUNTRY,                    
            [STATE],                    
            VENDOR,                    
            VENDOR_TYPE,                    
            COMMENTS,                    
            is_priority,
			[priority_cd]   FROM #DEFAULTEXCEPTIONSYSTEMGROUP_DETAIL  
			
			DELETE FROM #DEFAULTEXCEPTIONSYSTEMGROUP 
			WHERE RN <> 1 
   
    -- this is to update total count . this has to include the total count of system grouping as well.   
      
                
                    
                       
                    
        --STEP 1 UPDATE THE USER GROUP, GROUP THE INVOICES                       
                    
        UPDATE DF                    
        SET DF.UserGroupId = wg.Workflow_Queue_Search_Filter_Group_Id,                    
            DF.UserGroupName = wg.Filter_Group_Name                    
        FROM #DEFAULTEXCEPTIONS DF                    
            JOIN Workflow.Workflow_Queue_Search_Filter_Group_Invoice_Map WM                    
                ON DF.CU_INVOICE_ID = WM.Cu_Invoice_Id                    
        JOIN Workflow.Workflow_Queue_Search_Filter_Group wg                    
                ON wg.Workflow_Queue_Search_Filter_Group_Id = WM.Workflow_Queue_Search_Filter_Group_Id                    
        WHERE wg.Workflow_Queue_Saved_Filter_Query_Id = @Workflow_Queue_Saved_Filter_Query_Id                    
              AND wg.Owner_User_Info_Id = @Logged_In_User                    
              AND DF.systemgroupid IS NULL;                    
                    
        INSERT INTO @DefaultExceptionusergrouptemp                    
        (                    
            usergroupid,                    
            usergroupname,                    
            usergrouptotalcount                    
        )                    
        SELECT df.UserGroupId,                    
               df.UserGroupName,                    
               COUNT(1)                    
        FROM #DEFAULTEXCEPTIONS df                    
        WHERE df.UserGroupId IS NOT NULL                    
        GROUP BY df.UserGroupId,                    
                 df.UserGroupName;                    
                    
        UPDATE DF                    
        SET DF.usergroupcount = D.usergrouptotalcount  ,
		DF.UserGroupId = D.usergroupid                 
        FROM #DEFAULTEXCEPTIONS DF                    
            JOIN @DefaultExceptionusergrouptemp D                    
                ON DF.UserGroupId = D.usergroupid                    
                   AND DF.UserGroupName = D.usergroupname;                    
                    
              --this delete is moved here to exclude system group invoices in user groups      
           	DELETE FROM #DEFAULTEXCEPTIONS                    
        WHERE systemgroupid IS NOT NULL;               
                    
                    
        --STEP 4 MOVE USER GROUP TO SEPARATE TABLE                    
        INSERT INTO #DEFAULTEXCEPTIONSUSERGROUP_DETAIL                    
        (                    
            UserGroupId,                    
            UserGroupName,                    
            usergroupcount,                    
            systemgroupid,                    
            SystemGroupCount,                    
            ubmid,                    
            ubmaccountcode,                    
            RN,                    
            CU_INVOICE_ID,                    
            ACCOUNT,                    
            Account_Id,                    
            EXCEPTION_TYPE,                    
            EXCEPTION_STATUS_TYPE,                    
            ASSIGNEE,                    
            CLIENTID,                    
            CLIENT,                    
            COMMODITY,                    
            [DATA SOURCE],                    
            DATE_IN_QUEUE,                    
            DATE_IN_CBMS,                    
            FILEID,                    
            [FILENAME],                    
            SERVICE_MONTH,                    
            [SITE],                    
            COUNTRY,                    
            [STATE],                    
            VENDOR,                    
            VENDOR_TYPE,                    
            COMMENTS,                    
            is_priority  ,
			[priority_cd]                  
        )                    
        SELECT df.UserGroupId,                    
               df.UserGroupName,                    
        df.usergroupcount,              
               df.systemgroupid,                    
               df.SystemGroupCount,                    
               df.ubmid,                    
               df.ubmaccountcode,                    
               ROW_NUMBER() OVER (PARTITION BY df.UserGroupId ORDER BY df.CU_INVOICE_ID),                    
               df.CU_INVOICE_ID,                    
               df.ACCOUNT,                    
               df.Account_Id,                    
               df.EXCEPTION_TYPE,                    
               df.EXCEPTION_STATUS_TYPE,                    
               df.ASSIGNEE,                    
               df.CLIENTID,                    
               df.CLIENT,                    
               df.COMMODITY,                    
               df.[DATA SOURCE],                                   df.DATE_IN_QUEUE,                    
               df.DATE_IN_CBMS,                    
               df.FILEID,                    
               df.FILENAME,                    
               df.SERVICE_MONTH,                    
               df.SITE,                    
               df.COUNTRY,                    
               df.STATE,                    
               df.VENDOR,                    
               df.VENDOR_TYPE,                    
               df.COMMENTS,                    
               df.is_priority ,
			   df.priority_cd                   
        FROM #DEFAULTEXCEPTIONS df                    
        WHERE df.UserGroupId IS NOT NULL;   
    
            
                    
        INSERT INTO  #DEFAULTEXCEPTIONSUSERGROUP                   
        SELECT * FROM #DEFAULTEXCEPTIONSUSERGROUP_DETAIL  
		WHERE RN = 1;                    
                    
        DELETE FROM #DEFAULTEXCEPTIONS                    
        WHERE UserGroupId IS NOT NULL;    
 
    
  SET @TOTAL_COUNT  =                    
        (                    
            (SELECT COUNT(1) FROM #DEFAULTEXCEPTIONS )  
						+		isnull((select COUNT(1) from #DEFAULTEXCEPTIONSYSTEMGROUP_DETAIL  ) ,0)  
						+	isnull ((select COUNT(1) from #DEFAULTEXCEPTIONSUSERGROUP_DETAIL),0) 
   )  
                      
                      
 
                     
        --IF ONLY COUNT IS NEEDED THEN RETURN THE SP, NO NEED TO REST OF THE OPERATION                    
                    
        IF (@ONLY_COUNT_NEEDED = 1)                    
            RETURN 0;                  
                    
       SET @SQL4=' INSERT INTO #DEFAULTEXCEPTIONSFINAL                    
        (                    
            UserGroupId,                    
            UserGroupName,                    
            usergroupcount,                    
            systemgroupid,                    
            SystemGroupCount,                    
            ubmid,                    
            ubmaccountcode,                    
            CU_INVOICE_ID,                    
      ACCOUNT,               
            Account_Id,                    
            EXCEPTION_TYPE,                    
            EXCEPTION_STATUS_TYPE,                    
            ASSIGNEE,                    
            CLIENTID,                    
            CLIENT,                    
            COMMODITY,                    
            [DATA SOURCE],                    
            DATE_IN_QUEUE,                    
            DATE_IN_CBMS,                    
            FILEID,                    
            FILENAME,                    
            SERVICE_MONTH,                    
            SITE,                    
            COUNTRY,                    
            STATE,                    
            VENDOR,                    
            VENDOR_TYPE,                    
            COMMENTS,                    
            is_priority ,
			[priority_cd]                   
        )                    
        SELECT UserGroupId,                    
               UserGroupName,                    
               usergroupcount,                    
   systemgroupid,                    
               SystemGroupCount,                    
               ubmid,                    
               ubmaccountcode,                    
               CU_INVOICE_ID,                    
               ACCOUNT,                    
               Account_Id,                    
               EXCEPTION_TYPE,                    
               EXCEPTION_STATUS_TYPE,                    
               ASSIGNEE,                    
               CLIENTID,                    
               CLIENT,                    
               COMMODITY,                    
               [DATA SOURCE],                    
               DATE_IN_QUEUE,                    
               DATE_IN_CBMS,                    
               FILEID,                    
               FILENAME,                    
               SERVICE_MONTH,                    
               SITE,                    
               COUNTRY,                    
               STATE,                    
               VENDOR,            
               VENDOR_TYPE,                    
               COMMENTS,                    
               is_priority ,
			   [priority_cd]                   
        FROM                    
        (                    
            SELECT UserGroupId,                    
                   UserGroupName,                 
                   usergroupcount,                    
                   systemgroupid,                    
                   SystemGroupCount,                    
                   ubmid,                    
                   ubmaccountcode,                    
                   CU_INVOICE_ID,                    
                   ACCOUNT,                    
                   Account_Id,                    
                   EXCEPTION_TYPE,                    
                   EXCEPTION_STATUS_TYPE,                    
                   ASSIGNEE,                    
                   CLIENTID,                    
                   CLIENT,                    
                   COMMODITY,                    
                   [DATA SOURCE],                    
                   DATE_IN_QUEUE,                    
                   DATE_IN_CBMS,                    
                   FILEID,                    
                   FILENAME,                    
                   SERVICE_MONTH,                    
                   SITE,                    
                   COUNTRY,                    
                   STATE,                    
                   VENDOR,                    
                   VENDOR_TYPE,                    
                   COMMENTS,                    
                   is_priority,
				   [priority_cd]                    
            FROM #DEFAULTEXCEPTIONS                    
            UNION                    
            SELECT UserGroupId,                    
                   UserGroupName,                    
                   usergroupcount,                    
                   systemgroupid,                    
                   SystemGroupCount,                    
                   ubmid,                    
                   ubmaccountcode,                    
                   CU_INVOICE_ID,                    
                   ACCOUNT,                    
                   Account_Id,                    
                   EXCEPTION_TYPE,                    
                   EXCEPTION_STATUS_TYPE,                    
                   ASSIGNEE,                    
                   CLIENTID,                    
                   CLIENT,                    
                   COMMODITY,                    
                   [DATA SOURCE],                    
                   DATE_IN_QUEUE,                    
                   DATE_IN_CBMS,             
                   FILEID,                    
                   FILENAME,                    
                   SERVICE_MONTH,                    
                   SITE,                    
                   COUNTRY,                    
              STATE,                    
                   VENDOR,                    
                   VENDOR_TYPE,                    
                   COMMENTS,                    
                   is_priority ,
				   [priority_cd]                   
            FROM '+
			CASE WHEN @DOWNLOAD_ONLY =1 THEN 
			 '#DEFAULTEXCEPTIONSYSTEMGROUP_DETAIL'
			 ELSE '#DEFAULTEXCEPTIONSYSTEMGROUP' 
			 END   +              
   ' UNION                    
            SELECT UserGroupId,                    
                   UserGroupName,                    
                   usergroupcount,                    
       systemgroupid,                    
                   SystemGroupCount,                    
                   ubmid,                    
                   ubmaccountcode,                    
                   CU_INVOICE_ID,                    
                   ACCOUNT,                    
                   Account_Id,                    
                   EXCEPTION_TYPE,                    
                   EXCEPTION_STATUS_TYPE,                    
                   ASSIGNEE,                    
                   CLIENTID,                    
                   CLIENT,                    
                   COMMODITY,                    
                   [DATA SOURCE],                    
            DATE_IN_QUEUE,                    
                   DATE_IN_CBMS,                    
                   FILEID,                    
                   FILENAME,                    
                   SERVICE_MONTH,                    
                   SITE,                    
                   COUNTRY,                    
                   STATE,                    
                   VENDOR,                    
                   VENDOR_TYPE,                    
                   COMMENTS,                    
                   is_priority ,
				   [priority_cd]                   
            FROM '+
			CASE WHEN @DOWNLOAD_ONLY =1 THEN 
			 '#DEFAULTEXCEPTIONSUSERGROUP_DETAIL'
			 ELSE '#DEFAULTEXCEPTIONSUSERGROUP' 
			 END    +                  
        ') A'                    
                
         EXECUTE sys.sp_executesql @SQL4;  
		                
        SET @SQL3                    
            = 'insert #DEFAULTEXCEPTIONS_Sorting                     
                    
   select row_number () over (order by UserGroupId desc, is_priority desc , priority_cd asc'                    
              + CASE                    
                    WHEN @Data_Source_Column_Name IS NOT NULL THEN                    
                        +',' + @Data_Source_Column_Name + ' '                    
                    ELSE                    
                        ''                    
                END + CASE                    
                          WHEN @Data_Source_Column_Name IS NOT NULL                    
                               AND @Sorting_Type IS NOT NULL THEN                    
                              @Sorting_Type                    
                          ELSE                    
                              ''                    
  END + '),                    
   * from #DEFAULTEXCEPTIONSFINAL';                    
                    
            
                    
        EXECUTE sys.sp_executesql @SQL3;                    
                    
                    
                    
        SET @PAGE_ROW_COUNT =                    
        (                    
            SELECT COUNT(1) FROM #DEFAULTEXCEPTIONSFINAL                    
        );                    
                    
        SELECT DE.CU_INVOICE_ID    AS [Inv Id],                    
               DE.DATE_IN_QUEUE   AS [Date in Queue],                    
      DE.DATE_IN_CBMS   AS [Date in CBMS],                    
               DE.ASSIGNEE     AS Assignee,                    
               DE.EXCEPTION_TYPE   AS [Exception Type],                    
  DE.EXCEPTION_STATUS_TYPE  AS [Exception Status],                    
               DE.[DATA SOURCE]    AS [Data Source],                    
               DE.CLIENT     AS [Client],                    
               DE.SITE     AS [Site],                    
               DE.COUNTRY     AS Country,                    
               DE.STATE     AS [State],                 
               DE.ACCOUNT     AS [Account no],                    
               DE.Account_Id    AS Account_Id,                    
               DE.VENDOR     AS Vendor,                    
               DE.VENDOR_TYPE    AS [Vendor Type],                    
               DE.COMMODITY     AS Commodity,                    
               DE.SERVICE_MONTH    AS [Month],                    
               DE.FILENAME    AS Filename,                
               DE.COMMENTS     AS Comments,                    
               DE.UserGroupId    AS UserGroupId,                    
               DE.UserGroupName    AS UserGroupName,                    
               DE.SystemGroupCount   AS SystemGroupCount,                    
               DE.CLIENTID     AS ClientID,                    
               DE.usergroupcount   AS usergroupcount,                    
               DE.systemgroupid    AS systemgroupid,                    
               DE.FILEID     AS FileID,                    
               DE.ubmid      AS UBMID,                    
               DE.ubmaccountcode   AS UBMAccountNumber ,                    
               isnull(DE.is_priority, 0) AS [Priority], 
      CASE WHEN Batch_status.Code_Value='Pending'   THEN  'Pending'  ELSE  NULL END AS [Batch Status],   
  CASE WHEN   Invoice_priority.Code_Value = 'Systempriorityclient' THEN 'System'          
  WHEN    Invoice_priority.Code_Value = 'Userprioritymanual' THEN 'User'  
  ELSE null        
  END AS Priority_Source          
        FROM #DEFAULTEXCEPTIONS_Sorting  DE   
		outer APPLY 
		(SELECT C.Code_Value  FROM Workflow.Cu_Invoice_Workflow_Action_Batch_Dtl CIWABD             
  INNER JOIN dbo.Code C ON CIWABD.Status_Cd=C.Code_Id           
  AND CIWABD.Cu_Invoice_Id =DE.CU_INVOICE_ID
  AND C.Code_Value ='pending' )     Batch_status 
  outer APPLY 
		(SELECT c.Code_Value FROM Workflow.Cu_Invoice_Attribute CU          
  JOIN dbo.Code c           
  ON CU.Source_Cd = c.Code_Id          
  AND CU.CU_INVOICE_ID = DE.CU_INVOICE_ID ) Invoice_priority   
        WHERE DE.index_range                    
        BETWEEN @startindex AND @endindex 
		ORDER BY DE.index_range   
                 
              
                 
              
                    
        DROP TABLE #DEFAULTEXCEPTIONSUSERGROUP;                    
        DROP TABLE #DEFAULTEXCEPTIONS;                    
        DROP TABLE #DEFAULTEXCEPTIONSFINAL;                    
        DROP TABLE #DEFAULTEXCEPTIONSYSTEMGROUP;                    
        DROP TABLE #temp_user_info;                    
        DROP TABLE #temp_Exception_Type;     
  DROP TABLE #DEFAULTEXCEPTIONS_Sorting;                   
                    
    END TRY                    
    BEGIN CATCH                    
                    
                    
        -- Entry made to the logging SP to capture the errors.                      
        SELECT @ERROR_LINE = error_line(),                    
               @ERROR_MESSAGE = error_message();                    
                    
        INSERT INTO dbo.StoredProc_Error_Log                    
        (                    
            StoredProc_Name,                    
            Error_Line,                    
            Error_message,                    
            Input_Params                    
        )                    
        VALUES                    
        (@PROC_NAME, @ERROR_LINE, @ERROR_MESSAGE, @INPUT_PARAMS);                    
                    
        EXEC dbo.usp_RethrowError @ERROR_MESSAGE;                    
          
    END CATCH;                    
END;                                                                                                                                                                                                                                                          
GO
GRANT EXECUTE ON  [Workflow].[Get_Exception_Details_Invoice] TO [CBMSApplication]
GO
