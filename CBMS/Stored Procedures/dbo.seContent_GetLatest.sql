SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure dbo.seContent_GetLatest
	( @ContentPlacementId int 
	, @ContentLocaleId int 
	)
As
BEGIN
	set nocount on
	
		declare @StatusId int
		
		set @StatusId = dbo.dmlLookup_GetId ('Content Status', 'Approved')
	
	 select c.ContentId
				, c.ContentLocaleId
				, c.ContentPlacementId
				, c.Version
				, c.CurrentStatusId
				, c.ContentText
		 from seContent c
		 join (select ContentLocaleId
								, ContentPlacementId
								, max(Version) Version
						 from seContent
						where CurrentStatusId = @StatusId
						  and ContentPlacementId = @ContentPlacementId
						  and ContentLocaleId = @ContentLocaleId
				 group by ContentLocaleId
								, ContentPlacementId
					) x on (c.ContentPlacementId = x.ContentPlacementId and c.ContentLocaleId = x.ContentLocaleId and c.Version = x.Version)
END
GO
GRANT EXECUTE ON  [dbo].[seContent_GetLatest] TO [CBMSApplication]
GO
