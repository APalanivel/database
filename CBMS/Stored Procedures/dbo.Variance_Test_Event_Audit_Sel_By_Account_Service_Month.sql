SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
Name:    
	dbo.Variance_Test_Event_Audit_Sel_By_Account_Service_Month  
   
 Description:    
	To select the data from Variance_Test_Event_Audit table

 Input Parameters:    
    Name						DataType      Default		Description    
------------------------------------------------------------------------    
    @Account_Id					INT   
    @Service_Month				DATE

 Output Parameters:
	Name						DataType      Default		Description
------------------------------------------------------------------------

 Usage Examples:  
------------------------------------------------------------------------


	EXEC Variance_Test_Event_Audit_Sel_By_Account_Service_Month
       @Account_Id = 6961
      ,@Service_Month = '2006-01-01'


 
 
 SELECT TOP 10 * FROM Variance_Log
    

Author Initials:
 Initials	Name  
------------------------------------------------------------  
 HG			Harihara Suthan G
   
 Modifications :    
 Initials		Date	    Modification    
------------------------------------------------------------    
 HG				2014-07-24	Created
 ******/
CREATE PROCEDURE dbo.Variance_Test_Event_Audit_Sel_By_Account_Service_Month
      ( 
       @Account_Id INT
      ,@Service_Month DATE )
AS 
BEGIN

      SET NOCOUNT ON
			
      SELECT
            a.Event_Ts
           ,ui.First_Name + SPACE(1) + ui.Last_Name AS Event_By
           ,et.Code_Dsc + ISNULL(SPACE(1) + run.First_Name + SPACE(1) + run.Last_Name, '') AS Event_Dsc
      FROM
            dbo.Variance_Test_Event_Audit a
            INNER JOIN dbo.User_Info ui
                  ON ui.USER_INFO_ID = a.Event_By_User_Id
            INNER JOIN dbo.Code et
                  ON et.Code_Id = a.Variance_Test_Event_Type_Cd
            LEFT OUTER JOIN dbo.User_Info run
                  ON run.User_Info_Id = a.Variance_Routed_To_User_Id
      WHERE
            a.Account_Id = @Account_Id
            AND a.Service_Month = @Service_Month                 

END;

;
GO
GRANT EXECUTE ON  [dbo].[Variance_Test_Event_Audit_Sel_By_Account_Service_Month] TO [CBMSApplication]
GO
