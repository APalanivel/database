SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:      
      
 dbo.Bucket_Site_Interval_Dtl_Sel      
      
DESCRIPTION:      
      
 This procedure is used to Select the data for given client hier id,bucket master id,account id,service start date and service end date      
       
      
INPUT PARAMETERS:      
   Name               DataType  Default     Description       
-------------------------------------------------------------------------------------      
  @Client_Hier_Id      INT      
  @Bucket_Master_Id    INT      
  @Service_Start_Dt    DATE      
  @Service_End_Dt      DATE  
  @Data_Source_Cd      INT      
      
      
OUTPUT PARAMETERS:      
 Name       DataType     Default     Description                  
-------------------------------------------------------------------------------------      
      
USAGE EXAMPLES:      
-------------------------------------------------------------------------------------      
 Example 1:- Starts Here  
   
 BEGIN TRAN      
      
 DECLARE @tvp tvp_Bucket_Site_Interval_Dtl        
 INSERT INTO @tvp values (100001,110,'1/1/2013','1/5/2013',100350,150,12,null)        
 EXEC  dbo.Bucket_Site_Interval_Dtl_Ins @tvp,49,null
 
 SELECT * FROM  Bucket_Site_Interval_Dtl WHERE  Client_Hier_Id = 100001 AND Bucket_Master_Id = 110 AND Service_Start_Dt = '1/1/2013'    
							AND Service_End_Dt = '1/5/2013' AND Data_Source_Cd  = 100350    
   
   
 EXEC dbo.Bucket_Site_Interval_Dtl_Sel 100001,110, '1/1/2013', '1/5/2013',100350      
   
 ROLLBACK TRAN  
      
 Example 1:- Ends Here     
   
   
      
AUTHOR INITIALS:      
 Initials  Name      
-------------------------------------------------------------------------------------      
  RKV      Ravi Kumar Vegesna      
                                    
MODIFICATIONS                  
 Initials  Date         Modification                  
-------------------------------------------------------------------------------------      
   RKV     2013-03-07   Created                          
******/
CREATE PROCEDURE dbo.Bucket_Site_Interval_Dtl_Sel
      (
       @Client_Hier_Id INT
      ,@Bucket_Master_Id INT
      ,@Service_Start_Dt DATE
      ,@Service_End_Dt DATE
      ,@Data_Source_Cd INT )
AS
BEGIN

      SET NOCOUNT ON ;      

      SELECT
            Client_Hier_Id
           ,Bucket_Master_Id
           ,Service_Start_Dt
           ,Service_End_Dt
           ,Bucket_Daily_Avg_Value
           ,Data_Source_Cd
           ,Uom_Type_Id
           ,Currency_Unit_Id
           ,Created_User_Id
           ,Updated_User_Id
           ,Created_Ts
           ,Last_Changed_Ts
      FROM
            dbo.Bucket_Site_Interval_Dtl
      WHERE
            Client_Hier_Id = @Client_Hier_Id
            AND Bucket_Master_Id = @Bucket_Master_Id
            AND Service_Start_Dt = @Service_Start_Dt
            AND Service_End_Dt = @Service_End_Dt
            AND Data_Source_Cd = @Data_Source_Cd      

END
;
GO
GRANT EXECUTE ON  [dbo].[Bucket_Site_Interval_Dtl_Sel] TO [CBMSApplication]
GO
