SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
dbo.Unit_Of_Measurement_Sel_By_Commodity_List

DESCRIPTION:

	Selects UOM for the given commodity list

INPUT PARAMETERS:
	Name 			DataType 	Default 	Description
------------------------------------------------------------
	@Commodity_List VARCHAR(MAX)

OUTPUT PARAMETERS:
	Name			DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Unit_Of_Measurement_Sel_By_Commodity_List 290
	EXEC dbo.Unit_Of_Measurement_Sel_By_Commodity_List '291'
	EXEC dbo.Unit_Of_Measurement_Sel_By_Commodity_List '290,291'
	EXEC dbo.Unit_Of_Measurement_Sel_By_Commodity_List '290,291,1384,66'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy


MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RR			2015-07-10	Global Sourcing - Created

******/
CREATE PROCEDURE [dbo].[Unit_Of_Measurement_Sel_By_Commodity_List]
      ( 
       @Commodity_List VARCHAR(MAX) )
AS 
BEGIN

      SET NOCOUNT ON

      SELECT
            com.Commodity_Id
           ,en.Entity_id Uom_Type_Id
           ,en.Entity_name Uom_Name
           ,com.Default_UOM_Entity_Type_Id
      FROM
            dbo.Commodity com
            INNER JOIN dbo.Entity en
                  ON en.Entity_type = com.UOM_Entity_Type
      WHERE
            EXISTS ( SELECT
                        1
                     FROM
                        dbo.ufn_split(@Commodity_List, ',') com_list
                     WHERE
                        com_list.Segments = com.Commodity_Id )
      ORDER BY
            com.Commodity_Id

END
;
GO
GRANT EXECUTE ON  [dbo].[Unit_Of_Measurement_Sel_By_Commodity_List] TO [CBMSApplication]
GO
