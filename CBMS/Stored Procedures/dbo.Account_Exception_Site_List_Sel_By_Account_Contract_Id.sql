SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:                  
      dbo.Account_Exception_Site_List_Sel_By_Account_Contract_Id               
                  
Description:                  
			Add Conract - When clieck on multiple link in account exception to show the list in popup.                  
                  
 Input Parameters:                  
 Name                                   DataType        Default        Description                    
-------------------------------------------------------------------------------------------------                   
 @Account_Id							INT
 @Contract_Id							INT

                  
 Output Parameters:                        
 Name                                    DataType        Default        Description                    
--------------------------------------------------------------------------------------------------                   
                  
 Usage Examples:                      
--------------------------------------------------------------------------------------------------



EXEC dbo.Account_Exception_Site_List_Sel_By_Account_Contract_Id
    @Account_Id = NULL
    , @Contract_Id = 166786


EXEC dbo.Account_Exception_Site_List_Sel_By_Account_Contract_Id
    @Account_Id = 1655652
    , @Contract_Id = null
        

             
 Author Initials:                  
  Initials        Name                  
--------------------------------------------------------------------------------------------------                   
  NR              Narayana Reddy    
                   
 Modifications:                  
  Initials        Date              Modification                  
--------------------------------------------------------------------------------------------------                   
  NR              2019-01-03		Created for Add Contract             
                 
******/


CREATE PROCEDURE [dbo].[Account_Exception_Site_List_Sel_By_Account_Contract_Id]
    (
        @Account_Id INT = NULL
        , @Contract_Id INT = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;


        CREATE TABLE #Site_List
             (
                 Site_Id INT
                 , Site_Name VARCHAR(200)
                 , Client_Id INT
                 , State_Id INT
             );


        INSERT INTO #Site_List
             (
                 Site_Id
                 , Site_Name
                 , Client_Id
                 , State_Id
             )
        SELECT
            ch.Site_Id
            , ch.Site_name
            , ch.Client_Id
            , ch.State_Id
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Client_Hier_Id = ch.Client_Hier_Id
        WHERE
            cha.Account_Id = @Account_Id
        GROUP BY
            ch.Site_Id
            , ch.Site_name
            , ch.Client_Id
            , ch.State_Id;



        INSERT INTO #Site_List
             (
                 Site_Id
                 , Site_Name
                 , Client_Id
                 , State_Id
             )
        SELECT
            ch.Site_Id
            , ch.Site_name
            , ch.Client_Id
            , ch.State_Id
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Client_Hier_Id = ch.Client_Hier_Id
        WHERE
            cha.Supplier_Contract_ID = @Contract_Id
            AND cha.Supplier_Contract_ID <> -1
        GROUP BY
            ch.Site_Id
            , ch.Site_name
            , ch.Client_Id
            , ch.State_Id;


        SELECT
            sl.Site_Id
            , sl.Site_Name
            , sl.Client_Id
            , sl.State_Id
        FROM
            #Site_List sl
        GROUP BY
          sl.Site_Id
            , sl.Site_Name
            , sl.Client_Id
            , sl.State_Id
        ORDER BY
            sl.Site_Name;

    END;




GO
GRANT EXECUTE ON  [dbo].[Account_Exception_Site_List_Sel_By_Account_Contract_Id] TO [CBMSApplication]
GO
