SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
  dbo.Cu_Invoice_Recalc_Response_Is_Hide_On_RA_Update  
  
DESCRIPTION:  
  BulkUpdate the flag Is_Hide_On_RA in table Cu_Invoice_Recalc_Response
  
INPUT PARAMETERS:  
 Name													DataType				Default		Description  
-----------------------------------------------------------------------------------------------------------  
 @Ec_Calc_Val_Id										INT
 @tvp_Cu_Invoice_Recalc_Response_Is_Hide_On_RA_Update	tvp_Cu_Invoice_Recalc_Response_Is_Hide_On_RA_Update  
  

OUTPUT PARAMETERS:  
 Name								DataType				Default		Description  
-------------------------------------------------------------------------------------  
  
USAGE EXAMPLES:  
-------------------------------------------------------------------------------------  
 BEGIN Transaction
 
 
 
 ROLLBACK Transaction 
  
  
AUTHOR INITIALS:  
 Initials	Name  
-------------------------------------------------------------------------------------  
 RKV		Ravi Kumar Vegesna  
   
MODIFICATIONS  
  
 Initials		Date			Modification  
-------------------------------------------------------------------------------------  
 RKV			2016-01-27		Created for AS400-PII 
            
              
******/  
CREATE PROCEDURE [dbo].[Cu_Invoice_Recalc_Response_Is_Hide_On_RA_Update]
      ( 
       @tvp_Cu_Invoice_Recalc_Response_Is_Hide_On_RA_Update AS tvp_Cu_Invoice_Recalc_Response_Is_Hide_On_RA_Update READONLY )
AS 
BEGIN  
      SET NOCOUNT ON;  
     
     
     
     
      UPDATE
            cirr
      SET   
            Is_Hidden_On_RA = tcirr.Is_Hidden_On_RA
      FROM
            dbo.Cu_Invoice_Recalc_Response cirr
            INNER JOIN @tvp_Cu_Invoice_Recalc_Response_Is_Hide_On_RA_Update tcirr
                  ON cirr.Cu_Invoice_Recalc_Response_Id = tcirr.Cu_Invoice_Recalc_Response_Id
   
        
      
       
END     
   





;
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Recalc_Response_Is_Hide_On_RA_Update] TO [CBMSApplication]
GO
