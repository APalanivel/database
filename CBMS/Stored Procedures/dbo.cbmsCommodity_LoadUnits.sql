
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:                  
        dbo.cbmsCommodity_LoadUnits                   
                  
Description:                  
		       
                  
 Input Parameters:                  
 Name                       DataType				Default        Description                    
------------------------------------------------------------------------------------  
 @MyAccountId				INT
 @MyClientId				INT
 @commodity_type_id			INT 				            
 			 
                  
 Output Parameters:                        
 Name                       DataType				Default        Description                    
------------------------------------------------------------------------------------  
                  
 Usage Examples:                      
------------------------------------------------------------------------------------             


EXEC dbo.cbmsCommodity_LoadUnits  49, 235, 290

EXEC dbo.cbmsCommodity_LoadUnits  49, 235, 291


    
 Author Initials:                  
  Initials        Name                  
------------------------------------------------------------------------------------             
  NR              Narayana Reddy   
                   
 Modifications:                  
  Initials        Date              Modification                  
------------------------------------------------------------------------------------             
  NR              2016-05-31		MAINT-3789 Added Header and Replaced DV2-Commodity-Type(Read as '-' to '_') by Commodity.            
                 
******/ 


CREATE   PROCEDURE [dbo].[cbmsCommodity_LoadUnits]
      ( 
       @MyAccountId INT
      ,@MyClientId INT
      ,@commodity_type_id INT )
AS 
BEGIN

      
      
      SELECT
            e.ENTITY_ID AS unit_of_measure_type_id
           ,e.ENTITY_NAME AS unit_of_measure_type
           ,CASE WHEN c.Default_UOM_Entity_Type_Id = e.ENTITY_ID THEN 1
                 ELSE 0
            END AS is_default
      FROM
            dbo.Commodity c
            INNER JOIN dbo.ENTITY e
                  ON e.ENTITY_TYPE = c.UOM_Entity_Type
      WHERE
            c.Commodity_Id = @commodity_type_id
      ORDER BY
            e.ENTITY_NAME DESC 
      
      

END







;
GO

GRANT EXECUTE ON  [dbo].[cbmsCommodity_LoadUnits] TO [CBMSApplication]
GO
