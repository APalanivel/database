SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[DMR_UPDATE_CU_DETAIL] (@CU_invoice_ID             Int)
as

Set NoCount On

Declare @UBM_Invoice_ID            Int,
        @UBM_ID                    Int,
        @ubm_Batch_master_log_id   Int,
        @ubm_account_id            Varchar(55),
        @ubm_vendor_id             VarChar(55),
        @Commodity_type_id         Int,
        @ubm_service_type_id       Int,
        @ubm_service_code          varchar(55)

Select @UBM_Account_ID          = UI.UBM_Account_ID,
       @ubm_vendor_id           = UI.UBM_Vendor_ID,
       @ubm_id                  = M.UBM_ID
From ubm_batch_master_log M
       inner join
     ubm_invoice UI on M.UBM_Batch_master_log_id = UI.UBM_Batch_Master_log_id
       inner join
     cu_invoice CI on CI.ubm_invoice_ID = UI.ubm_invoice_id
Where cu_invoice_ID = @CU_Invoice_ID

Select @ubm_id = ubm_id
From ubm_batch_master_log
Where ubm_batch_master_log_id = @ubm_batch_master_log_id


Select @Commodity_type_id    = M.Commodity_type_id,
       @ubm_Service_Type_ID  = M.UBM_Service_Type_ID,
       @UBM_service_code     = M.UBM_Service_Code
From ubm_cass_service C
       inner join
     ubm_service_Map M on C.Account_Service_Name = M.UBM_Service_Code
Where C.Account_ID = @UBM_Account_ID
  And C.Vendor_ID = @ubm_vendor_id
  And M.UBM_ID = @ubm_id
Group By M.Commodity_type_id,
         M.UBM_Service_Type_ID,
         M.UBM_Service_Code

Set NoCount Off

Update cu_invoice_charge
  Set Commodity_type_id = @Commodity_Type_ID,
      UBM_Service_Type_id = @UBM_Service_Type_ID,
      UBM_Service_Code    = @UBM_Service_Code
Where cu_invoice_id = @cu_invoice_id
  And commodity_type_id is null

Update cu_invoice_determinant
  Set Commodity_type_id = @Commodity_Type_ID,
      UBM_Service_Type_id = @UBM_Service_Type_ID,
      UBM_Service_Code    = @UBM_Service_Code
Where cu_invoice_id = @cu_invoice_id
  And commodity_type_id is null
GO
GRANT EXECUTE ON  [dbo].[DMR_UPDATE_CU_DETAIL] TO [CBMSApplication]
GO
