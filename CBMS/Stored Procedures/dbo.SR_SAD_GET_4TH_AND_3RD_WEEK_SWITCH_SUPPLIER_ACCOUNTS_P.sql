SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- exec SR_SAD_GET_4TH_AND_3RD_WEEK_SWITCH_SUPPLIER_ACCOUNTS_P '02/27/2006'

CREATE        PROCEDURE dbo.SR_SAD_GET_4TH_AND_3RD_WEEK_SWITCH_SUPPLIER_ACCOUNTS_P
	@batch_execution_date datetime
	AS
	set nocount on
	declare @tariff_type_id int, @na_type_id int, @switch_supplier_type_id int

	select @tariff_type_id = entity_id from entity(nolock) where entity_type = 1034 and entity_name = 'Returning to tariff'
	select @na_type_id = entity_id from entity(nolock) where entity_type = 1034 and entity_name = 'N/A'	
	select @switch_supplier_type_id = entity_id from entity(nolock) where entity_type = 1027 and entity_name = 'Switching Suppliers'	


	select 	utilacc.account_id,
		con.contract_id

	from 	sr_rfp_utility_switch switch(nolock),
		sr_rfp_account rfp_account(nolock),
		sr_rfp rfp(nolock),
		account utilacc(nolock)		
		left join SR_SAD_CONTRACT_VW contract_vw(nolock) on contract_vw.account_id = utilacc.account_id
		left join contract con(nolock) on con.contract_id = contract_vw.contract_id

		
	where	switch.utility_switch_supplier_date is not null
		and switch.utility_switch_supplier_date  between dateadd(dd, (2 * 7), @batch_execution_date) and dateadd(dd, ((4 * 7) - 1), @batch_execution_date)
		and (switch.return_to_tariff_type_id != @tariff_type_id or switch.return_to_tariff_type_id != @na_type_id or switch.return_to_tariff_type_id is null)
		and (switch.utility_switch_supplier_type_id = @switch_supplier_type_id or switch.utility_switch_supplier_type_id is null)
		and switch.change_notice_image_id is null
		and switch.is_bid_group = 0
		and rfp_account.sr_rfp_account_id = switch.sr_account_group_id
		and rfp_account.is_deleted = 0
		and rfp.sr_rfp_id = rfp_account.sr_rfp_id
		and utilacc.account_id = rfp_account.account_id
		--and con.commodity_type_id = rfp.commodity_type_id
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_4TH_AND_3RD_WEEK_SWITCH_SUPPLIER_ACCOUNTS_P] TO [CBMSApplication]
GO
