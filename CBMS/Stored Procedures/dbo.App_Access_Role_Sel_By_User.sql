SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
         
/******         
         
 NAME: dbo.App_Access_Role_Sel_By_User        
                
 DESCRIPTION:          
		  To get the roles for a given user info id.        
              
 INPUT PARAMETERS:          
         
 Name                               DataType       Default          Description          
---------------------------------------------------------------------------------------------------------------        
 @User_Info_Id                      INT        
          
 OUTPUT PARAMETERS:        
                 
 Name                               DataType       Default          Description          
---------------------------------------------------------------------------------------------------------------        
          
 USAGE EXAMPLES:              
---------------------------------------------------------------------------------------------------------------         
		 EXEC dbo.App_Access_Role_Sel_By_User 33486        
         
 AUTHOR INITIALS:          
        
 Initials               Name          
---------------------------------------------------------------------------------------------------------------        
 NR                     Narayana Reddy            
           
 MODIFICATIONS:          
         
 Initials               Date            Modification        
---------------------------------------------------------------------------------------------------------------        
 NR                     2013-11-25      Created for RA Admin user management        
         
******/          
CREATE PROCEDURE [dbo].[App_Access_Role_Sel_By_User] ( @User_Info_Id INT )
AS 
BEGIN  
      
      SET NOCOUNT ON; 
             
      SELECT
            car.Client_App_Access_Role_Id
           ,car.App_Access_Role_Name
           ,car.App_Access_Role_Dsc
           ,CASE WHEN c.Code_Value = 'Admin' THEN 1
                 WHEN c.Code_Value = 'Full Access (Non Admin)' THEN 1
                 ELSE 0
            END AS Is_Default_Role
      FROM
            dbo.Client_App_Access_Role car
            INNER JOIN dbo.User_Info_Client_App_Access_Role_Map ucarm
                  ON car.Client_App_Access_Role_Id = ucarm.Client_App_Access_Role_Id
            INNER JOIN dbo.Code c
                  ON c.Code_Id = car.App_Access_Role_Type_Cd
      WHERE
            ucarm.User_Info_Id = @User_Info_Id        
                    
END;

;
GO
GRANT EXECUTE ON  [dbo].[App_Access_Role_Sel_By_User] TO [CBMSApplication]
GO
