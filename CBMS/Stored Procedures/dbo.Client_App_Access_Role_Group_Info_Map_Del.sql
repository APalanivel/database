SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 /******    
                       
 NAME: dbo.Client_App_Access_Role_Group_Info_Map_Del                   
                        
 DESCRIPTION:    
		To delete colums from  Client App Access Role group info map table.                        
                        
 INPUT PARAMETERS:    
                       
 Name                               DataType          Default       Description    
---------------------------------------------------------------------------------------------------------------  
 @Client_App_Access_Role_Id         INT      
 @GROUP_INFO_ID                     INT
                      
 OUTPUT PARAMETERS:    
                             
 Name                               DataType          Default       Description    
---------------------------------------------------------------------------------------------------------------  
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------    
             
	 BEGIN TRAN      
	 SELECT * FROM dbo.Client_App_Access_Role_Group_Info_Map where Client_App_Access_Role_Id=4 and GROUP_INFO_ID=307               
	 EXEC dbo.Client_App_Access_Role_Group_Info_Map_Del @Client_App_Access_Role_Id=4,@GROUP_INFO_ID=307   
	 SELECT * FROM dbo.Client_App_Access_Role_Group_Info_Map where Client_App_Access_Role_Id=4 and GROUP_INFO_ID=307    
	 ROLLBACK               
                       
 AUTHOR INITIALS:  
     
 Initials               Name    
---------------------------------------------------------------------------------------------------------------  
 NR                     Narayana Reddy                          
                         
 MODIFICATIONS:  
                         
 Initials               Date            Modification  
---------------------------------------------------------------------------------------------------------------  
 NR                     2013-12-30      Created for RA Admin user management                      
                       
******/ 
 CREATE PROCEDURE dbo.Client_App_Access_Role_Group_Info_Map_Del
      ( 
       @Client_App_Access_Role_Id INT
      ,@GROUP_INFO_ID INT )
 AS 
 BEGIN                      
      SET NOCOUNT ON;            
               
      DELETE FROM
            dbo.Client_App_Access_Role_Group_Info_Map
      WHERE
            Client_App_Access_Role_Id = @Client_App_Access_Role_Id
            AND GROUP_INFO_ID = @GROUP_INFO_ID
                      
 END   


;
GO
GRANT EXECUTE ON  [dbo].[Client_App_Access_Role_Group_Info_Map_Del] TO [CBMSApplication]
GO
