SET NUMERIC_ROUNDABORT OFF 
GO

SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.cbmsSsoProject_Search

DESCRIPTION:


INPUT PARAMETERS:
	Name						DataType	Default	Description
------------------------------------------------------------
	@MyAccountId   				int       	          	
	@commodity_type_id			int       	null      	
	@project_status_type_id		int       	null      	
	@project_ownership_type_id	int       	null      	
	@client_id     				int       	null      	
	@division_id   				int       	null      	
	@site_id       				int       	null      	
	@is_active     				bit       	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.cbmsSsoProject_Search 
		  @MyAccountId = 49
		 ,@Client_Id = 10057

	EXEC dbo.cbmsSsoProject_Search 
		  @MyAccountId = 49
		 ,@Commodity_type_id = 833
		 ,@Client_Id = 10057

	EXEC dbo.cbmsSsoProject_Search 
		  @MyAccountId = 49
		 ,@Commodity_type_id = 832
		 ,@Client_Id = 10041

	EXEC dbo.cbmsSsoProject_Search 
		  @MyAccountId = 49
		 ,@Commodity_type_id = 832
		 ,@Client_Id = 10015
	
	EXEC dbo.cbmsSsoProject_Search 
		  @MyAccountId = 49
		 ,@Commodity_type_id = 832
		 ,@Client_Id = 10015
		,@project_status_type_id = 703
	
	EXEC dbo.cbmsSsoProject_Search 
		  @MyAccountId = 49
		 ,@Commodity_type_id = 832
		 ,@Client_Id = 10015
		,@project_status_type_id = 704

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CPE			Chaitanya Eshwar Panduga
	RR			RaghuReddy
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	CPE			03/28/2011	Replaced vwCbmsSSOProjectOwnerFlat with SSO_PROJECT_OWNER_MAP table. 
							Replaced vwSsoProject_LastActivity with the base tables.
	RR			07/01/2011	Modified to apply condition not_managed based on type Client_Not_Managed/ DIvision_Not_Managed/Site_Not_Managed,
							previously used to apply the condition always on Client_Not_Managed.
	HG			2014-08-11	MAINT-2998, Modified the logic to get the last activity based on the Activity_Date in SSO_PROJECT_ACTIVITY table. Earlier it was based on the SSO_PROJECT_ACTIVITY_Id.
							If we have multiple activities on the same day then the last entered for that date will be considered as last activity.
******/

CREATE PROCEDURE [dbo].[cbmsSsoProject_Search]
      ( 
       @MyAccountId INT
      ,@commodity_type_id INT = NULL
      ,@project_status_type_id INT = NULL
      ,@project_ownership_type_id INT = NULL
      ,@client_id INT = NULL
      ,@division_id INT = NULL
      ,@site_id INT = NULL
      ,@is_active BIT = NULL )
AS 
BEGIN

      SET NOCOUNT ON

      EXEC cbmsSecurity_GetClientAccess 
            @MyAccountId
           ,@client_id OUTPUT
           ,@division_id OUTPUT
           ,@site_id OUTPUT

      SELECT
            SP.SSO_PROJECT_ID
           ,acc.owner_id
           ,acc.owner_name
           ,acc.owner_sort
           ,SP.PROJECT_TITLE
           ,SP.PROJECT_DESCRIPTION
           ,SP.PROJECT_STATUS_TYPE_ID
           ,PST.ENTITY_NAME Project_Status_Type
           ,SP.COMMODITY_TYPE_ID
           ,COM.ENTITY_NAME commodity_type
           ,SP.PROJECTED_END_DATE
           ,SP.PROJECT_OWNERSHIP_TYPE_ID
           ,POT.ENTITY_NAME project_ownership_type
           ,SP.IS_URGENT
           ,acc.Client_Name
           ,ISNULL(CONVERT(VARCHAR(14), PA.ACTIVITY_DATE, 101), '') + ISNULL(' - ' + PA.ACTIVITY_DESCRIPTION, SP.PROJECT_DESCRIPTION) Last_Activity
      FROM
            dbo.SSO_PROJECT SP
            JOIN dbo.ENTITY PST
                  ON PST.ENTITY_ID = SP.PROJECT_STATUS_TYPE_ID
            JOIN ENTITY POT
                  ON POT.ENTITY_ID = SP.PROJECT_OWNERSHIP_TYPE_ID
            JOIN ( SELECT DISTINCT
                        SSO_PROJECT_ID
                       ,CASE CD.Code_Value
                          WHEN 'Corporate' THEN CH.Client_Id
                          WHEN 'Division' THEN CH.Sitegroup_Id
                          WHEN 'Site' THEN CH.Site_Id
                        END AS Owner_Id
                       ,CASE CD.Code_Value
                          WHEN 'Corporate' THEN CH.Client_Name
                          WHEN 'Division' THEN CH.Sitegroup_Name
                          WHEN 'Site' THEN RTRIM(CH.City) + ', ' + CH.State_Name + ' (' + CH.Site_name + ')'
                        END AS Owner_Name
                       ,Cd.Display_Seq AS Owner_Sort
                       ,CH.Client_Id
                       ,CH.Client_Name
                       ,CASE CD.Code_Value
                          WHEN 'Corporate' THEN CH.Client_Not_Managed
                          WHEN 'Division' THEN CH.Division_Not_Managed
                          WHEN 'Site' THEN CH.Site_Not_Managed
                        END AS Not_Managed
                   FROM
                        SSO_PROJECT_OWNER_MAP SPOM
                        INNER JOIN Core.Client_Hier CH
                              ON SPOM.Client_Hier_Id = CH.Client_Hier_Id
                        INNER JOIN dbo.Code Cd
                              ON CH.Hier_level_Cd = Cd.Code_Id
                   WHERE
                        ( @client_id IS NULL
                          OR @client_id = CH.Client_Id )
                        AND ( @division_id IS NULL
                              OR @division_id = CH.Sitegroup_Id )
                        AND ( @site_id IS NULL
                              OR @Site_Id = CH.Site_Id ) ) acc
                  ON acc.SSO_PROJECT_ID = SP.SSO_PROJECT_ID
            LEFT OUTER JOIN dbo.ENTITY COM
                  ON COM.ENTITY_ID = SP.COMMODITY_TYPE_ID
            LEFT OUTER JOIN ( SELECT
                                    SSO_PROJECT_ID
                                   ,MAX(ACTIVITY_DATE) Last_Activity_Dt
                              FROM
                                    dbo.SSO_PROJECT_ACTIVITY
                              GROUP BY
                                    SSO_PROJECT_ID ) prj_last_act_dt
                  ON SP.SSO_PROJECT_ID = prj_last_act_dt.SSO_PROJECT_ID
            LEFT OUTER JOIN ( SELECT
                                    SSO_PROJECT_ID
                                   ,ACTIVITY_DATE
                                   ,MAX(SSO_PROJECT_ACTIVITY_ID) Last_Activity_Id
                              FROM
                                    dbo.SSO_PROJECT_ACTIVITY
                              GROUP BY
                                    SSO_PROJECT_ID
                                   ,ACTIVITY_DATE ) prj_last_act_id
                  ON prj_last_act_id.SSO_PROJECT_ID = sp.SSO_PROJECT_ID
                     AND prj_last_act_id.ACTIVITY_DATE = prj_last_act_dt.Last_Activity_Dt
            LEFT OUTER JOIN dbo.SSO_PROJECT_ACTIVITY PA
                  ON PA.SSO_PROJECT_ID = prj_last_act_dt.SSO_PROJECT_ID
                     AND pa.SSO_PROJECT_ACTIVITY_ID = prj_last_act_id.Last_Activity_Id
      WHERE
            ( @commodity_type_id IS NULL
              OR @commodity_type_id = SP.COMMODITY_TYPE_ID )
            AND ( @project_status_type_id IS NULL
                  OR @project_status_type_id = SP.PROJECT_STATUS_TYPE_ID )
            AND ( @project_ownership_type_id IS NULL
                  OR @project_ownership_type_id = SP.PROJECT_OWNERSHIP_TYPE_ID )
            AND ( @is_active IS NULL
                  OR @is_active = acc.Not_Managed )
      GROUP BY
            SP.SSO_PROJECT_ID
           ,acc.owner_id
           ,acc.owner_name
           ,acc.owner_sort
           ,SP.PROJECT_TITLE
           ,SP.PROJECT_DESCRIPTION
           ,SP.PROJECT_STATUS_TYPE_ID
           ,PST.ENTITY_NAME
           ,SP.COMMODITY_TYPE_ID
           ,COM.ENTITY_NAME
           ,SP.PROJECTED_END_DATE
           ,SP.PROJECT_OWNERSHIP_TYPE_ID
           ,POT.ENTITY_NAME
           ,SP.IS_URGENT
           ,acc.Client_Name
           ,ISNULL(CONVERT(VARCHAR(14), PA.ACTIVITY_DATE, 101), '') + ISNULL(' - ' + PA.ACTIVITY_DESCRIPTION, SP.PROJECT_DESCRIPTION)
      ORDER BY
            SP.SSO_PROJECT_ID
           ,PST.ENTITY_NAME DESC
           ,SP.IS_URGENT DESC
           ,acc.owner_name
END



;
GO


GRANT EXECUTE ON  [dbo].[cbmsSsoProject_Search] TO [CBMSApplication]
GO
