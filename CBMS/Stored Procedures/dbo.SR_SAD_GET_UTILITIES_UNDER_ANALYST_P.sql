SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_UTILITIES_UNDER_ANALYST_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@analystId     	varchar(25)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	exec dbo.SR_SAD_GET_UTILITIES_UNDER_ANALYST_P '5837,16980'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	SSR			09/27/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on	        		        	
******/

CREATE PROCEDURE dbo.SR_SAD_GET_UTILITIES_UNDER_ANALYST_P @analystId VARCHAR(25)
AS
BEGIN

    SET NOCOUNT ON
    DECLARE @sql VARCHAR(1000)

	SELECT
		@sql = ' Select v.VENDOR_ID,	v.VENDOR_NAME  from VENDOR v,utility_analyst_map map ,	'
		+ ' utility_detail det, 	entity venType  where 	map.UTILITY_DETAIL_ID = DET.UTILITY_DETAIL_ID  '
		+ ' and v.VENDOR_ID = det.VENDOR_ID and v.vendor_type_id = venType.entity_id '
		+ ' and venType.entity_type = 155 and venType.entity_name = ''Utility'' '
		+ ' and ((map.gas_analyst_id in ( select IntValue from dbo.SR_SAD_RPT_FN_GET_CSV_TO_INT('''
		+ @analystId + ''') ) )'
		+ '  OR (map.power_analyst_id in ( select IntValue  from dbo.SR_SAD_RPT_FN_GET_CSV_TO_INT('''
		+ @analystId + '''))))' + ' order by v.vendor_name'

		PRINT @Sql
        EXEC ( @sql )

END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_UTILITIES_UNDER_ANALYST_P] TO [CBMSApplication]
GO
