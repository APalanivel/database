SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                    
                          
NAME: [dbo].[CU_Service_Month_Sel_By_Account_Data_Source]                            
                               
DESCRIPTION:                      
                      
 This procedure is used to get the cost/usage changes for the given account.                          
                                              
                      
INPUT PARAMETERS:                      
NAME			DATATYPE DEFAULT  DESCRIPTION                      
------------------------------------------------------------                                    
@Account_Id INT
      ,@Data_Source_Cd INT = 'DE' 
      

OUTPUT PARAMETERS:                      
NAME   DATATYPE DEFAULT  DESCRIPTION                      
                      
------------------------------------------------------------                      

USAGE EXAMPLES:                      
------------------------------------------------------------                      
  
   EXEC [dbo].[CU_Service_Month_Sel_By_Account_Data_Source]
    @Account_Id=1149343 
    ,@Data_Source_Cd=100349   
       
   EXEC dbo.CODE_SEL_BY_CodeSet_Name 
      @CodeSet_Name = 'DataSource'
     ,@Code_Value = 'DE'
         
        
    SELECT
          *
      FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                  ON cs.Codeset_id = cd.Codeset_Id
      WHERE
            cs.Std_Column_Name = 'Data_Source_Cd'
                     
 AUTHOR INITIALS:        
       
 Initials               Name        
--------------------------------------------------------------------------------------- 
 SP                     Sandeep Pigilam
                         
 MODIFICATIONS:      
       
 Initials               Date             Modification      
--------------------------------------------------------------------------------------- 
 SP                     2017-03-23      Created for Contract Placeholder.          
*/
CREATE PROCEDURE [dbo].[CU_Service_Month_Sel_By_Account_Data_Source]
      ( 
       @Account_Id INT
      ,@Data_Source_Cd INT )
AS 
BEGIN            
            
      SET NOCOUNT ON;                        
                      
      SELECT
            cu.Account_id
           ,cu.Service_Month
      FROM
            dbo.Cost_Usage_Account_Dtl cu
      WHERE
            cu.Data_Source_Cd = @Data_Source_Cd
            AND CU.ACCOUNT_ID = @Account_Id
      GROUP BY
            cu.Account_id
           ,cu.Service_Month 
END;
;
GO
GRANT EXECUTE ON  [dbo].[CU_Service_Month_Sel_By_Account_Data_Source] TO [CBMSApplication]
GO
