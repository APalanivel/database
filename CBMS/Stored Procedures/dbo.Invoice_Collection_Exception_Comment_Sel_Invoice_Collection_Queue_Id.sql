SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Invoice_Collection_Exception_Comment_Sel_Invoice_Collection_Queue_Id       
              
Description:              
			This sproc to get the Invoice Collection Exceptions for the given filters     
                           
 Input Parameters:              
    Name									DataType								Default			Description                
-----------------------------------------------------------------------------------------------------------------                
	@tvp_Invoice_Collection_Queue_Sel		tvp_Invoice_Collection_Queue_Sel                      
    @Tvp_Invoice_Collection_Sources         Tvp_Invoice_Collection_Sources
    @Start_Index							INT
    @End_Index								INT
    @Total_Count							INT
    
 Output Parameters:                    
    Name								DataType			Default			Description                
-----------------------------------------------------------------------------------------------------------------                
              
 Usage Examples:                  
-----------------------------------------------------------------------------------------------------------------                

 
 EXEC dbo.Invoice_Collection_Exception_Comment_Sel_Invoice_Collection_Queue_Id 919
      
     
				
   
              
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2017-02-01		Created For Invoice_Collection.         
             
******/ 
CREATE PROCEDURE [dbo].[Invoice_Collection_Exception_Comment_Sel_Invoice_Collection_Queue_Id]
      ( 
       @Invoice_Collection_Queue_Id VARCHAR(MAX) )
AS 
BEGIN

      SELECT
            Invoice_Collection_Queue_Id
           ,Comment_Desc
           ,Invoice_Collection_Exception_Comment_Id
           ,icec.Created_Ts
           ,ui.FIRST_NAME + ' ' + ui.LAST_NAME Username
           ,c.Code_Value AS Comment_Type
      FROM
            dbo.Invoice_Collection_Exception_Comment icec
            INNER JOIN dbo.USER_INFO ui
                  ON ui.USER_INFO_ID = icec.Created_User_Id
                  INNER JOIN dbo.Code c
                  ON icec.comment_type_cd=c.Code_Id
      WHERE
            Invoice_Collection_Queue_Id = @Invoice_Collection_Queue_Id
      
END

;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Exception_Comment_Sel_Invoice_Collection_Queue_Id] TO [CBMSApplication]
GO
