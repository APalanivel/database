SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO

CREATE  PROCEDURE dbo.SR_SAD_SAVE_SR_DT_VERIFICATION_DETAILS_P
@userId varchar(10),
@sessionId varchar(20),
@srDealTicketId int,
@isDataBase bit,
@contractEndDate datetime,
@notificationsRequirements varchar(100),
@contractNumber varchar(50),
@saName varchar(100)


AS
	set nocount on
IF (select count(1) from SR_DT_VERIFICATION where SR_DEAL_TICKET_ID = @srDealTicketId) = 0
BEGIN

	insert into SR_DT_VERIFICATION
		(SR_DEAL_TICKET_ID, IS_DATABASE, CONTRACT_END_DATE, NOTIFICATION_REQUIREMENTS, CONTRACT_NUMBER, SA_NAME)
	
	values	(@srDealTicketId, @isDataBase, @contractEndDate, @notificationsRequirements, @contractNumber, @saName)

END

ELSE

BEGIN


	update SR_DT_VERIFICATION set IS_DATABASE=@isDataBase, CONTRACT_END_DATE=@contractEndDate,
		NOTIFICATION_REQUIREMENTS=@notificationsRequirements, CONTRACT_NUMBER=@contractNumber,
		SA_NAME=@saName

	where
		SR_DEAL_TICKET_ID=@srDealTicketId




END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_SAVE_SR_DT_VERIFICATION_DETAILS_P] TO [CBMSApplication]
GO
