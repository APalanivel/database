SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
dbo.Report_Budgets_GetAllYearsForClientAndCommodity

DESCRIPTION:

This procedure is to fetch all Budget Names by Client and Commodity

INPUT PARAMETERS:
Name				DataType		Default	Description
----------------------------------------------------------
@Client_ID			INT
@Commodity_type_id  INT 

OUTPUT PARAMETERS:
Name			DataType		Default	Description
----------------------------------------------------------

USAGE EXAMPLES:
----------------------------------------------------------
EXEC Report_Budgets_GetAllYearsForClientAndCommodity 1043,290

AUTHOR INITIALS:
Initials	Name
----------------------------------------------------------
SSR			Sharad srivastava

MODIFICATIONS:
Initials	Date		Modification
----------------------------------------------------------
SSR        	01/10/2011	Created
******/  
CREATE PROCEDURE [dbo].[Report_Budgets_GetAllYearsForClientAndCommodity]
    @Client_id INT
  , @Commodity_type_id INT
AS 
    BEGIN  
    
        SET NOCOUNT ON 
        
        SELECT
            budget_id
          , budget_name
        FROM
            budget
        WHERE
            client_id = @client_id
            AND commodity_type_id = @commodity_type_id
        GROUP BY
            budget_id
          , budget_name
        ORDER BY
          budget_name 
  
    END


GO
GRANT EXECUTE ON  [dbo].[Report_Budgets_GetAllYearsForClientAndCommodity] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_Budgets_GetAllYearsForClientAndCommodity] TO [CBMSApplication]
GO
