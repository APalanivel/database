SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[SR_RFP_Term_Product_Map_Del]  
     
DESCRIPTION: 
	It Deletes SR RFP Term Product Map for Selected 
						SR RFP Term Product Map Id.
      
INPUT PARAMETERS:          
NAME								DATATYPE	DEFAULT		DESCRIPTION          
-------------------------------------------------------------------          
@SR_RFP_Term_Product_Map_Id			INT	

   
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------

	Begin Tran
		EXEC SR_RFP_Term_Product_Map_Del  819
	Rollback Tran

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR		    28-MAY-10	CREATED

*/

CREATE PROCEDURE dbo.SR_RFP_Term_Product_Map_Del
   (
    @SR_RFP_Term_Product_Map_Id INT
   )
AS
BEGIN

    SET NOCOUNT ON;

    DELETE
   	FROM
		dbo.SR_RFP_TERM_PRODUCT_MAP
	WHERE
		SR_RFP_TERM_PRODUCT_MAP_ID = @SR_RFP_Term_Product_Map_Id

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_Term_Product_Map_Del] TO [CBMSApplication]
GO
