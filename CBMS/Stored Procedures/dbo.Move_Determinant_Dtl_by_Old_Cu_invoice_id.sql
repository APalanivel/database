
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Move_Determinant_Dtl_by_Old_Cu_invoice_id

DESCRIPTION:
	This SP is to Copy All Records from below tables of a given cu_invoice_id with New Cu_invoice_id
Tables
	cu_invoice_determinant
	,cu_invoice_determinant_account
	
INPUT PARAMETERS:
	Name			   DataType		Default	Description
------------------------------------------------------------	     	          	
 @Old_cu_invoice_id 	INT       	          	
 @New_cu_invoice_id 	INT 
 @cu_determinant_id		INT       	          	
 @is_checked			BIT

	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

begin transaction
select * from CU_INVOICE_DETERMINANT where  CU_INVOICE_ID = 25534118
                        AND CU_INVOICE_DETERMINANT_ID = 67249201
	
	EXEC dbo.Move_Determinant_Dtl_by_Old_Cu_invoice_id 25534125,25534122,67249205,1
	
	select * from CU_INVOICE_DETERMINANT where  CU_INVOICE_ID = 25534122
	
                    
Rollback Transaction


AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------
SSR			Sharad Srivastava
RKV         Ravi kumar vegesna
	
MODIFICATIONS

Initials	Date		Modification
------------------------------------------------------------
SSR        	04/12/2010	Created
RKV         2015-06-25  For As400 the column EC_Invoice_Sub_Bucket_Master_Id is added 
******/

CREATE PROCEDURE [dbo].[Move_Determinant_Dtl_by_Old_Cu_invoice_id]
      @Old_cu_invoice_id INT
     ,@New_cu_invoice_id INT
     ,@cu_determinant_id INT
     ,@is_checked BIT
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE @DETEROUT_TBL TABLE
            ( 
             ID INT IDENTITY(1, 1)
            ,NEW_CU_INVOICE_ID INT
            ,OLD_CU_INVOICE_ID INT
            ,NEW_CU_INVOICE_DETERMINANT_ID INT )
		
      INSERT      INTO CU_INVOICE_DETERMINANT
                  ( 
                   CU_INVOICE_ID
                  ,COMMODITY_TYPE_ID
                  ,UBM_METER_NUMBER
                  ,UBM_SERVICE_TYPE_ID
                  ,UBM_SERVICE_CODE
                  ,Bucket_Master_Id
                  ,UBM_BUCKET_CODE
                  ,UNIT_OF_MEASURE_TYPE_ID
                  ,UBM_UNIT_OF_MEASURE_CODE
                  ,DETERMINANT_NAME
                  ,DETERMINANT_VALUE
                  ,UBM_INVOICE_DETAILS_ID
                  ,CU_DETERMINANT_NO
                  ,CU_DETERMINANT_CODE
                  ,EC_Invoice_Sub_Bucket_Master_Id )
      OUTPUT      inserted.CU_INVOICE_ID
                 ,@Old_cu_invoice_id
                 ,inserted.CU_INVOICE_DETERMINANT_ID
                  INTO @DETEROUT_TBL
                  SELECT
                        @New_cu_invoice_id
                       ,COMMODITY_TYPE_ID
                       ,UBM_METER_NUMBER
                       ,UBM_SERVICE_TYPE_ID
                       ,UBM_SERVICE_CODE
                       ,Bucket_Master_Id
                       ,UBM_BUCKET_CODE
                       ,UNIT_OF_MEASURE_TYPE_ID
                       ,UBM_UNIT_OF_MEASURE_CODE
                       ,DETERMINANT_NAME
                       ,CASE WHEN @is_checked = 0 THEN '0'
                             ELSE DETERMINANT_VALUE
                        END
                       ,UBM_INVOICE_DETAILS_ID
                       ,CU_DETERMINANT_NO
                       ,CU_DETERMINANT_CODE
                       ,EC_Invoice_Sub_Bucket_Master_Id
                  FROM
                        dbo.CU_INVOICE_DETERMINANT
                  WHERE
                        CU_INVOICE_ID = @Old_cu_invoice_id
                        AND CU_INVOICE_DETERMINANT_ID = @cu_determinant_id
	
      UPDATE
            CU_INVOICE_DETERMINANT
      SET   
            DETERMINANT_VALUE = CASE WHEN @is_checked = 1 THEN '0'
                                     ELSE DETERMINANT_VALUE
                                END
      WHERE
            CU_INVOICE_ID = @Old_cu_invoice_id
            AND CU_INVOICE_DETERMINANT_ID = @cu_determinant_id


      INSERT      INTO CU_INVOICE_DETERMINANT_ACCOUNT
                  ( 
                   CU_INVOICE_DETERMINANT_ID
                  ,ACCOUNT_ID
                  ,Determinant_Expression
                  ,Determinant_Value )
                  SELECT
                        NEW_CU_INVOICE_DETERMINANT_ID
                       ,ACCOUNT_ID
                       ,Determinant_Expression
                       ,CASE WHEN @is_checked = 0 THEN 0
                             ELSE Determinant_Value
                        END
                  FROM
                        dbo.CU_INVOICE_DETERMINANT_ACCOUNT
                        CROSS APPLY @DETEROUT_TBL
                  WHERE
                        CU_INVOICE_DETERMINANT_ID = @cu_determinant_id

      UPDATE
            CU_INVOICE_DETERMINANT_ACCOUNT
      SET   
            Determinant_Value = CASE WHEN @is_checked = 1 THEN 0
                                     ELSE Determinant_Value
                                END
      WHERE
            CU_INVOICE_DETERMINANT_ID = @cu_determinant_id

END


;
GO

GRANT EXECUTE ON  [dbo].[Move_Determinant_Dtl_by_Old_Cu_invoice_id] TO [CBMSApplication]
GO
