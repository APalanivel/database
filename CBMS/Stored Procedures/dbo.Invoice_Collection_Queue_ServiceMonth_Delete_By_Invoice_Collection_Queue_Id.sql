SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                                
                      
NAME: dbo.Invoice_Collection_Queue_ServiceMonth_Delete_By_Invoice_Collection_Queue_Id

DESCRIPTION:                       
	To delete the Invoice collection queue month map
                            
INPUT PARAMETERS:                                
NAME									DATATYPE DEFAULT  DESCRIPTION                                
------------------------------------------------------------------------------                                
@Invoice_Collection_Queue_Id			INT

OUTPUT PARAMETERS:                                
NAME   DATATYPE DEFAULT  DESCRIPTION                                                      
------------------------------------------------------------------------------

USAGE EXAMPLES:                                
------------------------------------------------------------------------------


AUTHOR INITIALS:                                
INITIALS	NAME                                
------------------------------------------------------------                                
PR			Pradip Rajput
                    
MODIFICATIONS                      
INITIALS	DATE(YYYY-MM-DD)	MODIFICATION                      
------------------------------------------------------------                      
PR			2018-10-02			Created
*/
CREATE PROCEDURE [dbo].[Invoice_Collection_Queue_ServiceMonth_Delete_By_Invoice_Collection_Queue_Id]
      (
       @Invoice_Collection_Queue_Id INT )
AS
BEGIN

      SET NOCOUNT ON;

      DELETE
            dbo.Invoice_Collection_Queue_Month_Map
      WHERE
            Invoice_Collection_Queue_Id = @Invoice_Collection_Queue_Id

END;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Queue_ServiceMonth_Delete_By_Invoice_Collection_Queue_Id] TO [CBMSApplication]
GO
