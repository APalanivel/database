SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME: dbo.GET_INSERT_ACCOUNT_INFO_P    
    
    
DESCRIPTION:     
   
INPUT PARAMETERS:        
      Name                             DataType          Default     Description        
---------------------------------------------------------------------------------        
	@vendorID							INT       
	@siteID								INT       
	@invoiceSourceID					INT       
	@accountNumber						VARCHAR(200)      
	@accountTypeID						INT       
	@contractStartDate					DATETIME      
	@contractEndDate					DATETIME      
	@serviceLevelId						INT      
	@dmWatchList						bit      
	@ratesWatchList						bit      
	@proInstructions					VARCHAR(6000)  
	@eligibilityDate					DATETIME  
	@classificationType					int  
	@userId								INT    
	@Supplier_Account_begin_dt			DATETIME      
	@Supplier_Account_end_dt			DATETIME  
	@Supplier_Account_Recalc_Type_Id	INT   
	@Analyst_Mapping_Cd					INT   
	@Is_Broker_Account					INT 			0
	@Not_Expected						BIT				0
    @Not_Managed						BIT				0
    @Not_Expected_Date					DATETIME		NULL
    @Not_Expected_By_Id					INT				NULL 
    @Is_Level2_Recalc_Validation_Required  BIT	
    @Is_Invoice_Posting_Blocked			BIT	
    @Alternate_Account_Number			NVARCHAR(200)				            
                               
OUTPUT PARAMETERS:             
      Name              DataType          Default     Description        
------------------------------------------------------------          
    
USAGE EXAMPLES:    
------------------------------------------------------------    
BEGIN TRAN
EXEC DBO.GET_INSERT_ACCOUNT_INFO_P
    @vendorID = 464
    , @siteID = 2711
    , @invoiceSourceID = 35
    , @accountNumber = 'Test Account Data'
    , @accountTypeID = 37
    , @contractStartDate = NULL
    , @contractEndDate = NULL
    , @serviceLevelId = 859
    , @eligibilityDate = NULL
    , @classificationType = NULL
    , @isDataEntryOnly = 1
    , @Supplier_Account_begin_dt = '1/1/2009'
    , @Supplier_Account_end_dt = '12/1/2009'
    , @Supplier_Account_Recalc_Type_Cd = 1
    , @Analyst_Mapping_Cd = 100530
    , @Is_Broker_Account = 0
    , @Not_Expected = 1
    , @Not_Managed = 1
    , @Not_Expected_Date = '07/06/2013'
    , @Not_Expected_By_Id = 49
   
    , @Is_Invoice_Posting_Blocked = 0
    , @Alternate_Account_Number = 'Alternate_Account_Number'


SELECT  TOP 1   * FROM  dbo.ACCOUNT ORDER BY ACCOUNT_ID DESC
ROLLBACK TRAN


AUTHOR INITIALS:    
 Initials	Name    
------------------------------------------------------------    
  DR		Deana Ritter    
  SM		Sushil Muppidi  
  HG		Hari  
  NK		Nageswara Rao Kosuri
  AKR		Ashok Kumar Raju
  RR		Raghu Reddy
  SP		Sandeep Pigilam
  NR		Narayana Reddy
  TRK		Ramakrishna Thummala
MODIFICATIONS    
 Initials	Date			Modification    
------------------------------------------------------------    
   DR		08/04/2009		Removed Linked Server Updates    
   SM		09/18/2009		Included supplier Account begindate and enddate as paramater  
   HG		10/26/2009		Removed If Loop used insert the values into Account table.  
   sm		10/30/2009		included one more parameter Supplier_Account_Recalc_Type_Id  
   HG		11/05/2009		Contract.Contract_Recalc_Type_id and Account.Supplier_Account_Recalc_Type_id column relation moved to Code/Codeset from Entity  
							Account.Supplier_Account_Recalc_Type_id renamed to Supplier_Account_Recalc_Type_Cd   
							@Supplier_Account_Recalc_Type_Id parameter renamed to @Supplier_Account_Recalc_Type_Cd  
   HG		11/07/2009		Classification_type_id column moved to Contract table from account as it is specific to contract  
							Removed @classificationType input param and the column removed from the insert into account statement
   NK		12/07/2009		Removed Hard coded Account Type
   AKR      2012-09-27      Added Analyst_Mapping_Cd as a part of POCO
   RR		2013-06-10		ENHANCE-38 Its an enhancement the user have given ability to make account not expected or not managed while creating
							the account, so added the script part to add the event to invoice_participation_queue if the account is marked as 
							not managed or not expected
   SP		2014-07-18		Data Operations Enhancements Phase III added Is_Level2_Recalc_Validation_Required,Is_Invoice_Posting_Blocked Columns in Insert statement.
   RR		2015-07-20      For Global CBMS Sourcing - Added @Alternate_Account_Number as input parameter.
   RKV      2015-09-21      Removed the parameter @Is_Level2_Recalc_Validation_Required as part of AS400-II 	
   SP		2016-11-24		Invoice tracking,Added  Invoice_Collection_Is_Chased,Invoice_Collection_Is_Chased_Changed AS INPUT PARAM.    
   NR		2017-03-07		Contract Place holder  - Removed  @consolidatedBillingPostedToUtility input parameter.
   NR		2017-03-07		IDR-25  -Added @Supplier_Account_Determinant_Source_Cd input parameter.
   NR		2019-04-03		Data2.0 - Removed WATCHLIST-GROUP-INFO-ID  column from Account Table.
   NR		2019-08-27		Add Contract - Saved Service Level type as Blank entity for Suppler accounts.		
   RKV		2019-12-30		D20-1762  Removed UBM Parameters
   TRK		2020-02-05  Added dbo.ACCOUNT_POSTING_BLOCKED_AUDIT_TRACKING insert for Audit purpose.
			
******/

CREATE PROCEDURE [dbo].[GET_INSERT_ACCOUNT_INFO_P]
     (
         @vendorID INT
         , @siteID INT
         , @invoiceSourceID INT
         , @accountNumber VARCHAR(200)
         , @accountTypeID INT
         , @contractStartDate DATETIME
         , @contractEndDate DATETIME
         , @serviceLevelId INT
         , @eligibilityDate DATETIME
         , @classificationType INT
         , @isDataEntryOnly INT
         , @Supplier_Account_begin_dt DATETIME
         , @Supplier_Account_end_dt DATETIME
         , @Supplier_Account_Recalc_Type_Cd INT
         , @Analyst_Mapping_Cd INT = NULL
         , @Not_Expected_Date DATETIME = NULL
         , @Not_Expected BIT = 0
         , @Not_Managed BIT = 0
         , @Not_Expected_By_Id INT = NULL
         , @Is_Broker_Account INT = 0
         , @Is_Invoice_Posting_Blocked BIT
         , @Alternate_Account_Number NVARCHAR(200) = NULL
         , @Invoice_Collection_Is_Chased VARCHAR = NULL
         , @Invoice_Collection_Is_Chased_Changed BIT = NULL
         , @User_Info_Id INT = NULL
         , @Supplier_Account_Determinant_Source_Cd INT = NULL
     )
AS
    BEGIN

        SET NOCOUNT ON;
        DECLARE
            @this_id INT
            , @AccountType INT
            , @Invoice_Collection_Is_Chased_Bit BIT
            , @Blank_Entity_Id INT
            , @Supplier_Account_Type_Id INT
			, @ACCOUNTID INT;

        SELECT
            @Blank_Entity_Id = e.ENTITY_ID
        FROM
            dbo.ENTITY e
        WHERE
            e.ENTITY_NAME = ' '
            AND e.ENTITY_TYPE = 708;

        SELECT
            @Supplier_Account_Type_Id = e.ENTITY_ID
        FROM
            dbo.ENTITY e
        WHERE
            e.ENTITY_NAME = 'Supplier'
            AND e.ENTITY_DESCRIPTION = 'Account Type';



        SELECT
            @AccountType = ENTITY_ID
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_TYPE = 111
            AND ENTITY_NAME = 'Utility';

        INSERT INTO dbo.ACCOUNT
             (
                 VENDOR_ID
                 , INVOICE_SOURCE_TYPE_ID
                 , ACCOUNT_NUMBER
                 , ACCOUNT_TYPE_ID
                 , NOT_EXPECTED
                 , NOT_MANAGED
                 , SITE_ID
                 , SERVICE_LEVEL_TYPE_ID
                 , ELIGIBILITY_DATE
                 , Is_Data_Entry_Only
                 , Supplier_Account_Begin_Dt
                 , Supplier_Account_End_Dt
                 , Supplier_Account_Recalc_Type_Cd
                 , Analyst_Mapping_Cd
                 , Is_Broker_Account
                 , NOT_EXPECTED_DATE
                 , NOT_EXPECTED_BY_ID
                 , Is_Invoice_Posting_Blocked
                 , Alternate_Account_Number
                 , Supplier_Account_Determinant_Source_Cd
             )
        VALUES
            (@vendorID
             , @invoiceSourceID
             , @accountNumber
             , @accountTypeID
             , @Not_Expected
             , @Not_Managed
             , @siteID
             , CASE WHEN @Supplier_Account_Type_Id = @accountTypeID THEN @Blank_Entity_Id
                   ELSE ISNULL(@serviceLevelId, @Blank_Entity_Id)
               END
             , @eligibilityDate
             , @isDataEntryOnly
             , @Supplier_Account_begin_dt
             , @Supplier_Account_end_dt
             , @Supplier_Account_Recalc_Type_Cd
             , @Analyst_Mapping_Cd
             , @Is_Broker_Account
             , @Not_Expected_Date
             , @Not_Expected_By_Id
             , @Is_Invoice_Posting_Blocked
             , @Alternate_Account_Number
             , @Supplier_Account_Determinant_Source_Cd);

        SET @this_id = SCOPE_IDENTITY();


       



        IF @accountTypeID = @AccountType
            BEGIN
                EXEC dbo.cbmsInvoiceParticipation_InsertUtilityAccount 93, @this_id, @siteID;

                IF @Not_Managed = 1
                    BEGIN
                        EXEC cbmsInvoiceParticipationQueue_Save
                            93
                            , 7 -- Make account not managed    
                            , NULL
                            , NULL
                            , NULL
                            , @this_id
                            , NULL
                            , 1;
                    END;

                IF @Not_Expected = 1
                    BEGIN
                        EXEC cbmsInvoiceParticipationQueue_Save
                            93
                            , 8 -- Make account not expected    
                            , NULL
                            , NULL
                            , NULL
                            , @this_id
                            , NULL
                            , 1;
                    END;
            END;

			SELECT  @ACCOUNTID = SCOPE_IDENTITY();
			
			IF @Is_Invoice_Posting_Blocked = 1
			BEGIN
				INSERT INTO dbo.ACCOUNT_POSTING_BLOCKED_AUDIT_TRACKING
				(
					ACCOUNT_ID,
					Created_User_Id,
					Created_Ts,
					Updated_User_Id,
					Last_Change_Ts
				)
				VALUES
				(   @ACCOUNTID,    -- ACCOUNT_ID - int
					@User_Info_Id, -- Created_User_Id - int
					GETDATE(),     -- Created_Ts - datetime
					@User_Info_Id, -- Updated_User_Id - int
					GETDATE()      -- Last_Change_Ts - datetime
					);
			END;


        SELECT
            @Invoice_Collection_Is_Chased_Bit = CAST(ISNULL(@Invoice_Collection_Is_Chased, 0) AS BIT)
            , @Invoice_Collection_Is_Chased_Changed = CAST(ISNULL(@Invoice_Collection_Is_Chased_Changed, 0) AS BIT);




        RETURN @this_id;

    END;






GO
GRANT EXECUTE ON  [dbo].[GET_INSERT_ACCOUNT_INFO_P] TO [CBMSApplication]
GO
