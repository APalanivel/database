SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                  
NAME:                                  
    dbo.Onboard_Site_Sel_By_Division_Sitegroup_Keyword_Search                                  
                                  
DESCRIPTION:                                  
   Site Search Based on Keyword(Site_name or Site_Address_Line1 or City or State_Name or ZipCode) with mandatory Security_Role_Id                          
    and optional @SiteGroupId_DivisionId  or @Client_Hier_Id                           
                                  
INPUT PARAMETERS:                                  
 Name      DataType  Default Description                                  
---------------------------------------------------------------  
 @SiteGroupId_DivisionId INT    NULL  
 @Keyword     VARCHAR(200) NULL  
 @StartIndex    INT    1  
 @EndIndex     INT    2147483647  
                            
                         
OUTPUT PARAMETERS:                                  
 Name   DataType  Default Description                                  
---------------------------------------------------------------      
  
EXEC dbo.Onboard_Site_Sel_By_Division_Sitegroup_Keyword_Search  
    @Client_Id = 235     
   
                               
 EXEC dbo.Onboard_Site_Sel_By_Division_Sitegroup_Keyword_Search @Client_Id = 10050,@Commodity_Id = 291  
 EXEC dbo.Onboard_Site_Sel_By_Division_Sitegroup_Keyword_Search @Client_Id = 11236,@Commodity_Id = 291,@RM_Group_Id=730  
 EXEC dbo.Onboard_Site_Sel_By_Division_Sitegroup_Keyword_Search @Client_Id = 11278,@Commodity_Id = 291  
 EXEC dbo.Onboard_Site_Sel_By_Division_Sitegroup_Keyword_Search @Client_Id = 11278,@Commodity_Id = 291,@SiteGroupId_DivisionId=12270905  
                  
   
USAGE EXAMPLES:  
------------------------------------------------------------  
  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 NR   Narayana Reddy  
                     
MODIFICATIONS  
  
 Initials Date  Modification  
------------------------------------------------------------  
 NR   2018-07-25  Created For Risk Managemnet.  
  
******/
CREATE PROCEDURE [dbo].[Onboard_Site_Sel_By_Division_Sitegroup_Keyword_Search]
(
    @Client_Id INT,
    @Commodity_Id INT,
    @SiteGroupId_DivisionId INT = NULL,
    @Keyword VARCHAR(200) = NULL,
    @StartIndex INT = 1,
    @EndIndex INT = 2147483647,
    @RM_Group_Id INT = NULL
)
AS
BEGIN

    SET NOCOUNT ON;



    CREATE TABLE #Filtered_Client_Hier
    (
        Client_Hier_Id INT PRIMARY KEY CLUSTERED
    );

    CREATE TABLE #RM_Group_Sites
    (
        [Site_Id] INT
    );


    DECLARE @Keyword_FT_Search VARCHAR(200),
            @SQL_Filtered_Site VARCHAR(MAX),
            @SQL_Paged_Site VARCHAR(MAX);

    SET @Keyword = NULLIF(@Keyword, '');
    SET @Keyword_FT_Search = '"*' + dbo.udf_StripNonAlphaNumerics(@Keyword) + '*"';

    SELECT @SiteGroupId_DivisionId = NULLIF(@SiteGroupId_DivisionId, 0);

    INSERT INTO #RM_Group_Sites
    (
        Site_Id
    )
    SELECT ch.Site_Id
    FROM dbo.Cbms_Sitegroup_Participant rgd
        INNER JOIN dbo.Code grp
            ON grp.Code_Id = rgd.Group_Participant_Type_Cd
        INNER JOIN dbo.CONTRACT c
            ON c.CONTRACT_ID = rgd.Group_Participant_Id
        INNER JOIN Core.Client_Hier_Account chasupp
            ON chasupp.Supplier_Contract_ID = c.CONTRACT_ID
        INNER JOIN Core.Client_Hier_Account chautil
            ON chasupp.Meter_Id = chautil.Meter_Id
        INNER JOIN Core.Client_Hier ch
            ON ch.Client_Hier_Id = chasupp.Client_Hier_Id
    WHERE (
              rgd.Cbms_Sitegroup_Id = @RM_Group_Id
              AND grp.Code_Value = 'Contract'
              AND chasupp.Account_Type = 'Supplier'
              AND chautil.Account_Type = 'Utility'
          );

    INSERT INTO #RM_Group_Sites
    (
        Site_Id
    )
    SELECT ch.Site_Id
    FROM dbo.Cbms_Sitegroup_Participant rgd
        INNER JOIN dbo.Code grp
            ON grp.Code_Id = rgd.Group_Participant_Type_Cd
        INNER JOIN Core.Client_Hier ch
            ON ch.Site_Id = rgd.Group_Participant_Id
    WHERE (
              rgd.Cbms_Sitegroup_Id = @RM_Group_Id
              AND grp.Code_Value = 'Site'
          );


    SET @SQL_Filtered_Site
        = '  
    INSERT INTO #Filtered_Client_Hier  
 (Client_Hier_Id)  
    SELECT  
                        ch.Client_Hier_Id  
                   FROM  
                        Core.Client_Hier ch   
                          
                   WHERE  
       ch.Client_Id = ' + CAST(@Client_Id AS VARCHAR(20))
          + '  
       AND (EXISTS ( SELECT  
             1  
             FROM  
             Trade.RM_Client_Hier_Onboard siteob  
             WHERE  
             siteob.Client_Hier_Id = ch.Client_Hier_Id  
             AND siteob.Commodity_Id = ' + CAST(@Commodity_Id AS VARCHAR(20))
          + ' )  
           OR EXISTS ( SELECT  
              1  
              FROM  
              Trade.RM_Client_Hier_Onboard clntob  
              INNER JOIN Core.Client_Hier clch  
                 ON clntob.Client_Hier_Id = clch.Client_Hier_Id  
              WHERE  
              clch.Sitegroup_Id = 0  
              AND clch.Client_Id = ch.Client_Id  
              AND clntob.Country_Id = ch.Country_Id  
              AND clntob.Commodity_Id = ' + CAST(@Commodity_Id AS VARCHAR(20))
          + ' ))  
        AND ch.site_id > 0   
        ';

    -- Apply Keyword Filter  
    SET @SQL_Filtered_Site
        = @SQL_Filtered_Site
          + CASE
                WHEN @Keyword IS NOT NULL THEN
                    ' AND ( CONTAINS (  
                                    ( ch.Site_Search_Combo, ch.Site_Search_FTSearch),' + '''' + @Keyword_FT_Search
                    + '''' + '))'
                ELSE
                    ''
            END;



    -- Apply Sitegroup Filter  
    SET @SQL_Filtered_Site
        = @SQL_Filtered_Site
          + CASE
                WHEN @SiteGroupId_DivisionId IS NOT NULL THEN
                    ' AND EXISTS ( SELECT  
                                                      1  
                                                FROM  
                                                      dbo.Sitegroup_Site ss  
                                                WHERE  
                                                      ss.Site_id = ch.Site_Id  
                                     AND ss.Sitegroup_id = ' + CAST(@SiteGroupId_DivisionId AS VARCHAR(20)) + ' )'
                ELSE
                    ''
            END;
    -- Apply RM group Filter                                               
    SET @SQL_Filtered_Site
        = @SQL_Filtered_Site
          + CASE
                WHEN @RM_Group_Id IS NOT NULL THEN
                    ' AND EXISTS   
                (SELECT  
                 1  
                FROM  
                 #RM_Group_Sites rgs  
                WHERE  
                 rgs.Site_Id = ch.Site_Id )'
                ELSE
                    ''
            END;




    EXEC (@SQL_Filtered_Site);

    SET @SQL_Paged_Site
        = 'WITH Cte_Paged_Site AS (   
         SELECT TOP(' + CAST(@EndIndex AS VARCHAR(20))
          + ')  
          ch.Client_Hier_Id  
          ,ch.Site_Id  
          ,ch.Site_Name  
          ,ch.Site_Address_Line1  
          ,ch.City  
          ,ch.State_Name  
          ,ch.ZipCode  
          ,ch.Country_Name  
          ,ch.Sitegroup_Name  
          ,CASE WHEN Ch.Site_Not_Managed=0 THEN ''Active'' ELSE ''InActive'' END AS Site_Not_Managed  
          ,ROW_NUMBER() OVER( ORDER BY ch.Site_name ) AS row_Num  
         FROM  
          #Filtered_Client_Hier fch  
          INNER JOIN core.Client_Hier ch  
           ON fch.Client_Hier_Id = ch.Client_Hier_Id  
        )  
        SELECT   
         ch.Client_Hier_Id  
         ,ch.Site_Id  
         ,ch.Site_Name  
         ,ch.Sitegroup_Name   
         ,ch.Site_Address_Line1  
         ,ch.City  
         ,ch.State_Name  
         ,ch.Country_Name  
         ,ch.ZipCode  
         ,ch.Site_Not_Managed  
         ,ch.Row_Num  
        FROM  
         Cte_Paged_Site ch  
        WHERE  
         Row_Num BETWEEN ' + CAST(@StartIndex AS VARCHAR(20)) + ' AND ' + CAST(@EndIndex AS VARCHAR(20))
          + '  
        ORDER BY  
         ch.Row_Num ';

    EXEC (@SQL_Paged_Site);

    DROP TABLE #Filtered_Client_Hier;

END;

GO
GRANT EXECUTE ON  [dbo].[Onboard_Site_Sel_By_Division_Sitegroup_Keyword_Search] TO [CBMSApplication]
GO
