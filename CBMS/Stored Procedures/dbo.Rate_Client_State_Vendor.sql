SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                 
/******                          
 NAME: dbo.Rate_Client_State_Vendor              
                          
 DESCRIPTION:                          
   To get the State_Id based on State_Name.                          
                          
 INPUT PARAMETERS:            
                       
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
@State_Name     VARCHAR(200)                       
                          
 OUTPUT PARAMETERS:            
                             
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
                          
 USAGE EXAMPLES:                              
---------------------------------------------------------------------------------------------------------------                              
   
     EXEC [dbo].[Rate_Client_State_Vendor]   
                         
 AUTHOR INITIALS:          
         
 Initials              Name          
---------------------------------------------------------------------------------------------------------------                        
 RKV                  Ravi Kumar Vegesna  
                           
 MODIFICATIONS:        
            
 Initials              Date             Modification        
---------------------------------------------------------------------------------------------------------------        
 RKV                   2017-05-23       Created as part of SE2017-147                  
                         
******/                   
                  
CREATE  PROCEDURE [dbo].[Rate_Client_State_Vendor]  
      (   
       @State_Id INT = NULL  
      ,@Account_Vendor_Id INT = NULL  
      ,@Client_Id INT = NULL
	  ,@Commodity_Id Int = Null )  
AS   
BEGIN                  
      SET NOCOUNT ON;       
        
        
      SELECT  
            cha.Rate_Id  
           ,cha.Rate_Name  
      FROM  
            core.Client_Hier_Account cha  
            INNER JOIN core.Client_Hier ch  
                  ON cha.Client_Hier_Id = ch.Client_Hier_Id  
      WHERE  
            ( @State_Id IS NULL  
              OR ch.State_Id = @State_Id )  
            AND ( @Account_Vendor_Id IS NULL  
                  OR cha.Account_Vendor_Id = @Account_Vendor_Id )  
            AND ( @Client_Id IS NULL  
                  OR ch.Client_Id = @Client_Id )  
			 AND ( @Commodity_Id IS NULL  
                  OR cha.Commodity_Id = @Commodity_Id )
      GROUP BY  
            cha.Rate_Id  
           ,cha.Rate_Name  
      ORDER BY  
            cha.Rate_Name          
                          
                         
END;  
  
;  
GO
GRANT EXECUTE ON  [dbo].[Rate_Client_State_Vendor] TO [CBMSApplication]
GO
