SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [dbo].[Cost_Usage_Dtl_Sel_By_Account_Id_For_Utility_And_Dependant_Supplier_Accounts]
     
DESCRIPTION:

	To Get CU detail for Selected Utility Account Id and dependant supplier accounts

INPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
@Account_Id		INT
@StartIndex		INT			1			
@EndIndex		INT			2147483647

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Cost_Usage_Dtl_Sel_By_Account_Id_For_Utility_And_Dependant_Supplier_Accounts 440819
	EXEC dbo.Cost_Usage_Dtl_Sel_By_Account_Id_For_Utility_And_Dependant_Supplier_Accounts 116867 ,1,25

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
CPE			Chaitanya Panduga Eswara

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------     
CPE			2014-02-01	Created
*/

CREATE PROCEDURE dbo.Cost_Usage_Dtl_Sel_By_Account_Id_For_Utility_And_Dependant_Supplier_Accounts
	  (
	   @Account_Id INT
	  ,@StartIndex INT = 1
	  ,@EndIndex INT = 2147483647
	  )
AS
BEGIN
	  SET NOCOUNT ON;

	  WITH	CTE_CU
			  AS ( SELECT
						A.ACCOUNT_ID
					   ,A.SERVICE_MONTH
					   ,A.Site_Id
					   ,A.Client_Hier_Id
					   ,A.Bucket_Value
					   ,A.Bucket_Name
					   ,A.ACCOUNT_NUMBER
					   ,A.Account_Type
					   ,ROW_NUMBER() OVER ( ORDER BY Order_Col, Service_Month ) Row_Num
					   ,COUNT(1) OVER ( ) Total_Rows
				   FROM
						( SELECT
							  cuad.ACCOUNT_ID
							 ,cuad.SERVICE_MONTH
							 ,ch.Site_Id
							 ,cuad.Client_Hier_Id
							 ,cuad.Bucket_Value
							 ,bm.Bucket_Name
							 ,a.ACCOUNT_NUMBER
							 ,1 AS Order_Col
							 ,'Account To Delete' AS Account_Type
						  FROM
							  dbo.Cost_Usage_Account_Dtl AS cuad
							  INNER JOIN dbo.ACCOUNT AS a
									ON a.ACCOUNT_ID = cuad.ACCOUNT_ID
							  INNER JOIN Core.Client_Hier ch
									ON ch.Client_Hier_Id = cuad.Client_Hier_Id
							  INNER JOIN Bucket_Master bm
									ON bm.Bucket_Master_Id = cuad.Bucket_Master_Id
						  WHERE
							  a.ACCOUNT_ID = @Account_Id
						  GROUP BY
							  cuad.ACCOUNT_ID
							 ,cuad.SERVICE_MONTH
							 ,ch.Site_Id
							 ,cuad.Client_Hier_Id
							 ,cuad.Bucket_Value
							 ,bm.Bucket_Name
							 ,a.ACCOUNT_NUMBER
						  UNION
						  SELECT
							  cuad.ACCOUNT_ID
							 ,cuad.SERVICE_MONTH
							 ,ch.Site_Id
							 ,cuad.Client_Hier_Id
							 ,cuad.Bucket_Value
							 ,bm.Bucket_Name
							 ,cha.ACCOUNT_NUMBER
							 ,2 AS Order_Col
							 ,'Associated Supplier Accounts' AS Account_Type
						  FROM
							  dbo.Cost_Usage_Account_Dtl AS cuad
							  INNER JOIN core.Client_Hier_Account AS cha
									ON cha.Account_Id = cuad.ACCOUNT_ID
							  INNER JOIN dbo.METER AS m
									ON m.METER_ID = cha.Meter_Id
							  INNER JOIN Core.Client_Hier ch
									ON ch.Client_Hier_Id = cha.Client_Hier_Id
							  INNER JOIN Bucket_Master bm
									ON bm.Bucket_Master_Id = cuad.Bucket_Master_Id
						  WHERE
							  m.ACCOUNT_ID = @Account_Id
							  AND cha.Account_Type = 'Supplier'
						  GROUP BY
							  cuad.ACCOUNT_ID
							 ,cuad.SERVICE_MONTH
							 ,ch.Site_Id
							 ,cuad.Client_Hier_Id
							 ,cuad.Bucket_Value
							 ,bm.Bucket_Name
							 ,cha.ACCOUNT_NUMBER ) A)
			SELECT
				  ACCOUNT_ID
				 ,SERVICE_MONTH
				 ,Site_Id
				 ,Client_Hier_Id
				 ,Bucket_Value
				 ,Bucket_Name
				 ,ACCOUNT_NUMBER
				 ,Account_Type
				 ,Row_Num
				 ,Total_Rows
			FROM
				  CTE_CU
			WHERE
				  Row_Num BETWEEN @StartIndex AND @EndIndex
                  
END
;
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Dtl_Sel_By_Account_Id_For_Utility_And_Dependant_Supplier_Accounts] TO [CBMSApplication]
GO
