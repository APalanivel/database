
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            

NAME: 
	[DBO].[Supplier_Account_Service_Month_Check_By_Account_Dates]    
       
DESCRIPTION: TO CHECK INVOICE IS GENERATED FOR SELECTED ACCOUNT ID  
        
INPUT PARAMETERS:            
	NAME		  DATATYPE	   DEFAULT  DESCRIPTION            
------------------------------------------------------------            
	@ContractID INT  
	@AccountID  VARCHAR(MAX)   
                  
OUTPUT PARAMETERS:            
NAME   DATATYPE DEFAULT  DESCRIPTION            
-------------------------------------------------------------------            

USAGE EXAMPLES:            
-------------------------------------------------------------------            

	EXEC dbo.Supplier_Account_Service_Month_Check_By_Account_Dates  79201,'2017-03-01','2017-04-01'
	EXEC dbo.Supplier_Account_Service_Month_Check_By_Account_Dates  79201,'2008-03-01','2017-04-01'
	EXEC dbo.Supplier_Account_Service_Month_Check_By_Account_Dates  79201,'2008-02-01','2017-04-01'
	
 
 
AUTHOR INITIALS:            
	INITIALS	NAME            
------------------------------------------------------------            
	RR			Raghu Reddy

MODIFICATIONS             
	INITIALS	DATE	    MODIFICATION            
------------------------------------------------------------            
	RR			2017-03-07	Contract placeholder CP-8 Created
	RR			2017-05-02	MAINT-5262 System not allowing to change supplier account dates even selecting date in the same month, issue
							with invoice and for begin date only, invoice service month date is always first day of the month and any other date 
							other than first day selected then the service month date is out side of it. To fix this resetting begin date(@Begin_Dt)
							to first day of the month
	
***********************/    
  
CREATE PROCEDURE [dbo].[Supplier_Account_Service_Month_Check_By_Account_Dates]
      ( 
       @AccountID INT
      ,@Begin_Dt DATE
      ,@End_Dt DATE )
AS 
BEGIN    

      SET NOCOUNT ON;
      
      DECLARE
            @Dates_Allowed BIT = 0
           ,@Min_Begin_Dt DATE
           ,@Max_End_Dt DATE
           
      DECLARE @Dates_Tbl AS TABLE
            ( 
             Min_Begin_Dt DATE
            ,Max_End_Dt DATE )
            
      --INSERT      INTO @Dates_Tbl
      --            ( 
      --             Min_Begin_Dt
      --            ,Max_End_Dt )
      --            SELECT
      --                  MIN(Service_Month)
      --                 ,MAX(Service_Month)
      --            FROM
      --                  dbo.Cost_Usage_Account_Dtl cuad
      --            WHERE
      --                  cuad.ACCOUNT_ID = @AccountID
      
      INSERT      INTO @Dates_Tbl
                  ( 
                   Min_Begin_Dt
                  ,Max_End_Dt )
                  SELECT
                        MIN(Begin_Dt)
                       ,MAX(End_Dt)
                  FROM
                        dbo.CU_INVOICE_SERVICE_MONTH
                  WHERE
                        Account_ID = @AccountID
                 
      SELECT
            @Min_Begin_Dt = MIN(Min_Begin_Dt)
           ,@Max_End_Dt = MAX(Max_End_Dt)
      FROM
            @Dates_Tbl
            
      SELECT
            @Begin_Dt = DATEADD(DD, -DATEPART(DD, @Begin_Dt) + 1, @Begin_Dt)
	  
      SELECT
            @Dates_Allowed = 1
      WHERE
            ( @End_Dt IS NULL
              AND @Begin_Dt <= @Min_Begin_Dt
              AND @Begin_Dt <= @Max_End_Dt )
            OR ( @End_Dt IS NOT NULL
                 AND @Begin_Dt <= @Min_Begin_Dt
                 AND @Begin_Dt <= @Max_End_Dt
                 AND @Min_Begin_Dt <= @End_Dt
                 AND @Max_End_Dt <= @End_Dt )
                 
      SELECT
            @Dates_Allowed AS Dates_Allowed
           ,@Min_Begin_Dt AS Invoice_Start_Dt
           ,@Max_End_Dt AS Invoice_End_Dt
            
END;

;
GO

GRANT EXECUTE ON  [dbo].[Supplier_Account_Service_Month_Check_By_Account_Dates] TO [CBMSApplication]
GO
