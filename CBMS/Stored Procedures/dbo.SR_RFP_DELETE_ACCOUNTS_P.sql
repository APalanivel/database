SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_DELETE_ACCOUNTS_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@accountId     int,
@rfpId         int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- Test delete
--exec [dbo].[SR_RFP_DELETE_ACCOUNTS_P] 
--@accountId = 10044,
--@rfpId = 22

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[SR_RFP_DELETE_ACCOUNTS_P] 

@accountId int,
@rfpId int
AS
	
set nocount on


declare @lpAccExist int
select @lpAccExist =  count(*) from SR_RFP_LOAD_PROFILE_SETUP  where SR_RFP_ACCOUNT_ID 
in (select sr_rfp_account_id from sr_rfp_account where account_id= @accountId and  sr_rfp_id= @rfpId)

delete from sr_rfp_checklist where sr_rfp_account_id in (select sr_rfp_account_id from sr_rfp_account
where account_id=@accountId and  sr_rfp_id=@rfpId)

delete from SR_RFP_ARCHIVE_METER_CONTRACTS where sr_rfp_account_meter_map_id in ( select sr_rfp_account_meter_map_id from sr_rfp_account_meter_map where sr_rfp_account_id in (select sr_rfp_account_id from sr_rfp_account
where account_id=@accountId and  sr_rfp_id=@rfpId))

delete from SR_RFP_ARCHIVE_METER where sr_rfp_account_meter_map_id in ( select sr_rfp_account_meter_map_id from sr_rfp_account_meter_map where sr_rfp_account_id in (select sr_rfp_account_id from sr_rfp_account
where account_id=@accountId and  sr_rfp_id=@rfpId))

delete from sr_rfp_account_meter_map where sr_rfp_account_id in (select sr_rfp_account_id from sr_rfp_account
where account_id=@accountId and  sr_rfp_id=@rfpId)


if(@lpAccExist > 0)
begin

	delete from SR_RFP_LP_DETERMINANT_VALUES where SR_RFP_LOAD_PROFILE_DETERMINANT_ID 
	in (select SR_RFP_LOAD_PROFILE_DETERMINANT_ID from  SR_RFP_LOAD_PROFILE_DETERMINANT where SR_RFP_LOAD_PROFILE_SETUP_ID
	in (select SR_RFP_LOAD_PROFILE_SETUP_ID from SR_RFP_LOAD_PROFILE_SETUP where SR_RFP_ACCOUNT_ID 
	in (select sr_rfp_account_id from sr_rfp_account where account_id= @accountId and  sr_rfp_id= @rfpId)))
	
	delete from SR_RFP_LOAD_PROFILE_DETERMINANT where SR_RFP_LOAD_PROFILE_SETUP_ID
	in (select SR_RFP_LOAD_PROFILE_SETUP_ID from SR_RFP_LOAD_PROFILE_SETUP where SR_RFP_ACCOUNT_ID 
	in (select sr_rfp_account_id from sr_rfp_account where account_id= @accountId and  sr_rfp_id= @rfpId))
	
	delete from SR_RFP_LOAD_PROFILE_SETUP  where SR_RFP_ACCOUNT_ID 
	in (select sr_rfp_account_id from sr_rfp_account where account_id= @accountId and  sr_rfp_id= @rfpId)
	
	delete from SR_RFP_LP_ACCOUNT_ADDITIONAL_ROW where SR_RFP_ACCOUNT_ID 
	in (select sr_rfp_account_id from sr_rfp_account where account_id= @accountId and  sr_rfp_id= @rfpId)
	
	delete from SR_RFP_LP_SEND_CLIENT where SR_ACCOUNT_GROUP_ID in (select sr_rfp_account_id from sr_rfp_account
	where account_id= @accountId and  sr_rfp_id= @rfpId)
	
	delete from SR_RFP_LP_INTERVAL_DATA where sr_rfp_account_id in (select sr_rfp_account_id from sr_rfp_account
	where account_id= @accountId and  sr_rfp_id= @rfpId)
	
	delete from SR_RFP_LP_COMMENT where sr_rfp_account_id in (select sr_rfp_account_id from sr_rfp_account
	where account_id= @accountId and  sr_rfp_id= @rfpId)
	
	delete from SR_RFP_LP_CLIENT_APPROVAL where SR_ACCOUNT_GROUP_ID 
	in (select sr_rfp_account_id from sr_rfp_account where  account_id= @accountId and sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
	and is_bid_group = 0
	
	delete from SR_RFP_LP_CLIENT_APPROVAL where SR_ACCOUNT_GROUP_ID 
	in (select sr_rfp_bid_group_id from sr_rfp_account where  account_id= @accountId and sr_rfp_id= @rfpId)
	and is_bid_group = 1
	
	delete from SR_RFP_LP_ACCOUNT_SUMMARY where SR_ACCOUNT_GROUP_ID 
	in (select sr_rfp_account_id from sr_rfp_account where account_id= @accountId and sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
	and is_bid_group = 0
	
	delete from SR_RFP_LP_ACCOUNT_SUMMARY where SR_ACCOUNT_GROUP_ID 
	in (select sr_rfp_bid_group_id from sr_rfp_account where account_id= @accountId and sr_rfp_id= @rfpId)
	and is_bid_group = 1
end

declare @termAccExistNoBidGroup int
declare @termAccExistIsBidGroup int

select @termAccExistNoBidGroup = count(*) from SR_RFP_ACCOUNT_TERM where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where account_id= @accountId and  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0

select @termAccExistIsBidGroup = count(*) from SR_RFP_ACCOUNT_TERM where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where account_id= @accountId and  sr_rfp_id= @rfpId)
and is_bid_group = 1

if(@termAccExistNoBidGroup > 0)
begin
	delete from sr_rfp_term_product_map where sr_rfp_account_term_id 
	in (select sr_rfp_account_term_id from SR_RFP_ACCOUNT_TERM where SR_ACCOUNT_GROUP_ID 
	in (select sr_rfp_bid_group_id from sr_rfp_account where account_id= @accountId and  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
	and is_bid_group = 0)

	delete from SR_RFP_ACCOUNT_TERM where SR_ACCOUNT_GROUP_ID 
	in (select sr_rfp_account_id from sr_rfp_account where account_id= @accountId and  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
	and is_bid_group = 0

end

if(@termAccExistIsBidGroup > 0)
begin
	delete from sr_rfp_term_product_map where sr_rfp_account_term_id 
	in (select sr_rfp_account_term_id from SR_RFP_ACCOUNT_TERM where SR_ACCOUNT_GROUP_ID 
	in (select sr_rfp_bid_group_id from sr_rfp_account where account_id= @accountId and  sr_rfp_id= @rfpId)
	and is_bid_group = 1)

	delete from SR_RFP_ACCOUNT_TERM where SR_ACCOUNT_GROUP_ID 
	in (select sr_rfp_bid_group_id from sr_rfp_account where account_id= @accountId and  sr_rfp_id= @rfpId)
	and is_bid_group = 1
end

delete from SR_RFP_CLOSURE where SR_ACCOUNT_GROUP_ID in (select sr_rfp_account_id from sr_rfp_account
where account_id = @accountId and  sr_rfp_id = @rfpId)

delete from SR_RFP_CONTRACT_ADMIN_INITIATIVES where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where account_id = @accountId and  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0

delete from SR_RFP_CONTRACT_ADMIN_INITIATIVES where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where account_id = @accountId and  sr_rfp_id= @rfpId)
and is_bid_group = 1

delete from SR_RFP_REASON_NOT_WINNING_MAP where SR_RFP_REASON_NOT_WINNING_ID
in (select SR_RFP_REASON_NOT_WINNING_ID from SR_RFP_REASON_NOT_WINNING where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where  account_id= @accountId and sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0)

delete from SR_RFP_REASON_NOT_WINNING_MAP where SR_RFP_REASON_NOT_WINNING_ID
in (select SR_RFP_REASON_NOT_WINNING_ID from SR_RFP_REASON_NOT_WINNING where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where  account_id= @accountId and sr_rfp_id= @rfpId)
and is_bid_group = 1)

delete from SR_RFP_REASON_NOT_WINNING where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where account_id= @accountId and  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0

delete from SR_RFP_REASON_NOT_WINNING where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where account_id= @accountId and  sr_rfp_id= @rfpId)
and is_bid_group = 1




declare @sendSuppAccExistNoBidGroup int
declare @sendSuppAccExistIsBidGroup int

select @sendSuppAccExistNoBidGroup = count(*) from SR_RFP_SEND_SUPPLIER where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where account_id= @accountId and  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0

select @sendSuppAccExistIsBidGroup = count(*) from SR_RFP_SEND_SUPPLIER where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where account_id= @accountId and  sr_rfp_id= @rfpId)
and is_bid_group = 1

if(@sendSuppAccExistNoBidGroup > 0)
begin
	delete from SR_RFP_SEND_SUPPLIER_LOG where SR_RFP_SEND_SUPPLIER_ID
	in (select SR_RFP_SEND_SUPPLIER_ID from SR_RFP_SEND_SUPPLIER where SR_ACCOUNT_GROUP_ID 
	in (select sr_rfp_account_id from sr_rfp_account where  account_id= @accountId and sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
	and is_bid_group = 0)
	
	delete from SR_RFP_SEND_SUPPLIER where SR_ACCOUNT_GROUP_ID 
	in (select sr_rfp_account_id from sr_rfp_account where account_id= @accountId and  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
	and is_bid_group = 0
end

if(@sendSuppAccExistIsBidGroup > 0)
begin
	delete from SR_RFP_SEND_SUPPLIER_LOG where SR_RFP_SEND_SUPPLIER_ID
	in (select SR_RFP_SEND_SUPPLIER_ID from SR_RFP_SEND_SUPPLIER where SR_ACCOUNT_GROUP_ID 
	in (select sr_rfp_bid_group_id from sr_rfp_account where account_id= @accountId and  sr_rfp_id= @rfpId)
	and is_bid_group = 1)
	
	delete from SR_RFP_SEND_SUPPLIER where SR_ACCOUNT_GROUP_ID 
	in (select sr_rfp_bid_group_id from sr_rfp_account where account_id= @accountId and  sr_rfp_id= @rfpId)
	and is_bid_group = 1
end



declare @sopAccExistNoBidGroup int
declare @sopAccExistIsBidGroup int

select @sopAccExistNoBidGroup = count(*) from SR_RFP_SOP_CLIENT_APPROVAL where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where account_id= @accountId and  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0

select @sopAccExistIsBidGroup = count(*) from SR_RFP_SOP_SUMMARY where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where account_id= @accountId and sr_rfp_id= @rfpId)
and is_bid_group = 1



if(@sopAccExistNoBidGroup > 0)
begin

	delete from sr_rfp_sop_shortlist_details where sr_rfp_sop_shortlist_id in 
	(select sr_rfp_sop_shortlist_id from sr_rfp_sop_shortlist where SR_ACCOUNT_GROUP_ID 
	in (select sr_rfp_account_id from sr_rfp_account where  account_id= @accountId and sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
	and is_bid_group = 0)
	
	delete from SR_RFP_SOP_SHORTLIST where SR_ACCOUNT_GROUP_ID 
	in (select sr_rfp_account_id from sr_rfp_account where account_id= @accountId and  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
	and is_bid_group = 0
	
	delete from SR_RFP_SOP_DETAILS where SR_RFP_SOP_ID
	in (select SR_RFP_SOP_ID from SR_RFP_SOP where SR_RFP_SOP_SUMMARY_ID
	in (select SR_RFP_SOP_SUMMARY_ID from SR_RFP_SOP_SUMMARY where SR_ACCOUNT_GROUP_ID 
	in (select sr_rfp_account_id from sr_rfp_account where account_id= @accountId and  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
	and is_bid_group = 0))
	
	delete from SR_RFP_SOP where SR_RFP_SOP_SUMMARY_ID
	in (select SR_RFP_SOP_SUMMARY_ID from SR_RFP_SOP_SUMMARY where SR_ACCOUNT_GROUP_ID 
	in (select sr_rfp_account_id from sr_rfp_account where  account_id= @accountId and sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
	and is_bid_group = 0)
	
	delete from SR_RFP_SOP_SUMMARY where SR_ACCOUNT_GROUP_ID 
	in (select sr_rfp_account_id from sr_rfp_account where account_id= @accountId and  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
	and is_bid_group = 0
	
	delete from SR_RFP_SOP_CLIENT_APPROVAL where SR_ACCOUNT_GROUP_ID 
	in (select sr_rfp_account_id from sr_rfp_account where account_id= @accountId and  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
	and is_bid_group = 0
	
	delete from SR_RFP_SOP_SMR where SR_ACCOUNT_GROUP_ID 
	in (select sr_rfp_account_id from sr_rfp_account where account_id= @accountId and  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
	and is_bid_group = 0
end

if(@sopAccExistIsBidGroup > 0)
begin

	delete from sr_rfp_sop_shortlist_details where sr_rfp_sop_shortlist_id in 
	(select sr_rfp_sop_shortlist_id from sr_rfp_sop_shortlist where SR_ACCOUNT_GROUP_ID 
	in (select sr_rfp_bid_group_id from sr_rfp_account where account_id= @accountId and sr_rfp_id= @rfpId) 
	and is_bid_group = 1)
	
	delete from SR_RFP_SOP_SHORTLIST where SR_ACCOUNT_GROUP_ID 
	in (select sr_rfp_bid_group_id from sr_rfp_account where account_id= @accountId and sr_rfp_id= @rfpId)
	and is_bid_group = 1
	
	delete from SR_RFP_SOP_DETAILS where SR_RFP_SOP_ID
	in (select SR_RFP_SOP_ID from SR_RFP_SOP where SR_RFP_SOP_SUMMARY_ID
	in (select SR_RFP_SOP_SUMMARY_ID from SR_RFP_SOP_SUMMARY where SR_ACCOUNT_GROUP_ID 
	in (select sr_rfp_bid_group_id from sr_rfp_account where account_id= @accountId and sr_rfp_id= @rfpId) and is_bid_group = 1))
	
	delete from SR_RFP_SOP where SR_RFP_SOP_SUMMARY_ID
	in (select SR_RFP_SOP_SUMMARY_ID from SR_RFP_SOP_SUMMARY where SR_ACCOUNT_GROUP_ID 
	in (select sr_rfp_bid_group_id from sr_rfp_account where account_id= @accountId and sr_rfp_id= @rfpId) 
	and is_bid_group = 1)
	
	delete from SR_RFP_SOP_SUMMARY where SR_ACCOUNT_GROUP_ID 
	in (select sr_rfp_bid_group_id from sr_rfp_account where account_id= @accountId and sr_rfp_id= @rfpId)
	and is_bid_group = 1
	
	delete from SR_RFP_SOP_CLIENT_APPROVAL where SR_ACCOUNT_GROUP_ID 
	in (select sr_rfp_bid_group_id from sr_rfp_account where account_id= @accountId and sr_rfp_id= @rfpId)
	and is_bid_group = 1
	
	delete from SR_RFP_SOP_SMR where SR_ACCOUNT_GROUP_ID 
	in (select sr_rfp_bid_group_id from sr_rfp_account where account_id= @accountId and  sr_rfp_id= @rfpId)
	and is_bid_group = 1
	
end

delete from SR_RFP_UTILITY_SWITCH where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where  account_id = @accountId and sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0

delete from SR_RFP_UTILITY_SWITCH where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where account_id = @accountId and sr_rfp_id= @rfpId)
and is_bid_group = 1

delete from SR_RFP_ARCHIVE_CHECKLIST where sr_rfp_account_id in (select sr_rfp_account_id from sr_rfp_account
where  sr_rfp_id=@rfpId)

delete from SR_RFP_ARCHIVE_CHECKLIST where sr_rfp_account_id in (select sr_rfp_account_id from sr_rfp_account
where account_id = @accountId and  sr_rfp_id = @rfpId)

delete from SR_RFP_ARCHIVE_ACCOUNT where sr_rfp_account_id in (select sr_rfp_account_id from sr_rfp_account
where account_id = @accountId and  sr_rfp_id = @rfpId)

delete SR_RFP_UPDATE_SUPPLIER where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where  account_id = @accountId and sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0

delete SR_RFP_UPDATE_SUPPLIER where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where account_id = @accountId and sr_rfp_id= @rfpId)
and is_bid_group = 1

delete SR_SW_RFP_NO_BID where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where  account_id = @accountId and sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0
 
delete SR_SW_RFP_NO_BID where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where account_id = @accountId and sr_rfp_id= @rfpId)
and is_bid_group = 1

delete from sr_rfp_account where account_id = @accountId and  sr_rfp_id = @rfpId
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_DELETE_ACCOUNTS_P] TO [CBMSApplication]
GO
