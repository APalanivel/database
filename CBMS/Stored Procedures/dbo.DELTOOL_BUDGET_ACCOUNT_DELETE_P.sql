SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




--sp_help BUDGET_account

--exec DELTOOL_BUDGET_ACCOUNT_DELETE_P 264

CREATE        PROCEDURE dbo.DELTOOL_BUDGET_ACCOUNT_DELETE_P	
	@accountId int,
	@error_out int OUTPUT
	AS
	begin
	set nocount on
	declare @error int
	set @error = 0
	
		
		delete from BUDGET_DETAIL_SNAP_SHOT where budget_account_id in(select budget_account_id from budget_account where ACCOUNT_ID = @accountId)
		set @error=@@error
		

		if(@error=0)
		begin
		delete from BUDGET_DETAIL_COMMENTS where budget_account_id in(select budget_account_id from budget_account where ACCOUNT_ID = @accountId)
		set @error=@@error
		end

		if(@error=0)
		begin
		delete from BUDGET_DETAILS where budget_account_id in(select budget_account_id from budget_account where ACCOUNT_ID = @accountId)
		set @error=@@error
		end
		--added by saritha for Bz18297
		if(@error=0)
		begin
		delete from BUDGET_DETAIL_COMMENTS_OWNER_MAP where budget_account_id in(select budget_account_id from budget_account where ACCOUNT_ID = @accountId)
		set @error=@@error
		end

		if(@error=0)
		begin
		delete from BUDGET_ACCOUNT where ACCOUNT_ID = @accountId
		set @error=@@error
		end
	
		if(@error=0)
		begin
		delete from BUDGET_NYMEX_FORECAST where ACCOUNT_ID = @accountId
		set @error=@@error
		end

		if(@error=0)
		begin
		delete from BUDGET_USAGE where  ACCOUNT_ID = @accountId
		set @error=@@error
		end
	
		return @error
	end













GO
GRANT EXECUTE ON  [dbo].[DELTOOL_BUDGET_ACCOUNT_DELETE_P] TO [CBMSApplication]
GO
