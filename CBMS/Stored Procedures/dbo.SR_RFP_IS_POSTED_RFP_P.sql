SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- exec DBO.SR_RFP_IS_POSTED_RFP_P  '-1', '-1', 2, 1, 5
CREATE PROCEDURE dbo.SR_RFP_IS_POSTED_RFP_P
	@user_id varchar(10),
	@session_id varchar(20),
	@account_group_id int,
	@is_bid_group bit,
	@contact_id int
	AS
	set nocount on	
	if @is_bid_group = 1
	begin
		select	en.entity_name as status
		from 	sr_rfp_send_supplier send_supp(nolock),
			sr_rfp_supplier_contact_vendor_map map(nolock),
			entity en(nolock),
			sr_rfp_account rfp_account(nolock)
		where	send_supp.sr_account_group_id = @account_group_id
			and rfp_account.sr_rfp_bid_group_id = send_supp.sr_account_group_id
			and rfp_account.is_deleted = 0
			and send_supp.is_bid_group = 1
			and en.entity_id = send_supp.status_type_id 	
			and map.sr_rfp_supplier_contact_vendor_map_id = send_supp.sr_rfp_supplier_contact_vendor_map_id
			and map.sr_supplier_contact_info_id = @contact_id
	end
	else
	begin
		select	en.entity_name as status
		from 	sr_rfp_send_supplier send_supp(nolock),
			sr_rfp_supplier_contact_vendor_map map(nolock),
			entity en(nolock),
			sr_rfp_account rfp_account(nolock)
		where	send_supp.sr_account_group_id = @account_group_id
			and rfp_account.sr_rfp_account_id = send_supp.sr_account_group_id
			and rfp_account.is_deleted = 0
			and en.entity_id = send_supp.status_type_id 	
			and map.sr_rfp_supplier_contact_vendor_map_id = send_supp.sr_rfp_supplier_contact_vendor_map_id
			and map.sr_supplier_contact_info_id = @contact_id

	end
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_IS_POSTED_RFP_P] TO [CBMSApplication]
GO
