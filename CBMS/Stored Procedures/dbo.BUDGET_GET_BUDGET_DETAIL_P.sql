SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO













-- exec BUDGET_GET_BUDGET_DETAIL_P '','',1
CREATE       PROCEDURE dbo.BUDGET_GET_BUDGET_DETAIL_P
	@user_id varchar(10),
	@session_id varchar(20),
	@budget_account_id int
	AS

	begin
	set nocount on

	select	budget_details.month_identifier,
		budget_details.nymex_forecast,
		budget_details.budget_usage as budget_usage,
		entity.entity_name as usage_type,
		budget_details.variable,
		budget_details.transportation,
		budget_details.transmission,
		budget_details.distribution,
		budget_details.other_bundled,
		budget_details.other_fixed_costs,
		budget_details.sourcing_tax,
		budget_details.rates_tax,
		budget_account.has_details_saved,
		budget_details.budget_detail_id,
		budget_details.is_manual_generation,
		budget_details.is_manual_sourcing_tax
 
	from 	budget_account 
		join budget_details on budget_details.budget_account_id = budget_account.budget_account_id
		and budget_account.budget_account_id = @budget_account_id
		left join entity on entity.entity_id = budget_details.budget_usage_type_id
	end
















GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_BUDGET_DETAIL_P] TO [CBMSApplication]
GO
