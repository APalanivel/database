SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--exec dbo.GET_CORPORATE_REPORTS_P '1','1',203

CREATE  PROCEDURE dbo.GET_CORPORATE_REPORTS_P

@userId varchar(10),
@sessionId varchar(20),
@clientId int


as
	set nocount on

select distinct list.REPORT_NAME REPORT_NAME from 
	rm_batch_configuration batch,
	REPORT_LIST list
 where 

	list.REPORT_LIST_ID in 
	( select report_list_id from rm_batch_configuration where RM_BATCH_CONFIGURATION_MASTER_ID=

		(SELECT max(batch.RM_BATCH_CONFIGURATION_MASTER_ID) FROM 
		RM_BATCH_CONFIGURATION batch
	
		)
		and client_id=@clientId and CORPORATE_REPORT=1
	)
		




/*	batch.RM_BATCH_CONFIGURATION_MASTER_ID=39
	(
		SELECT max(batch.RM_BATCH_CONFIGURATION_MASTER_ID) FROM 
		RM_BATCH_CONFIGURATION batch
		WHERE 

		batch.client_id=@clientId

	)
	and batch.CORPORATE_REPORT=1
	AND =batch.REPORT_LIST_ID*/
GO
GRANT EXECUTE ON  [dbo].[GET_CORPORATE_REPORTS_P] TO [CBMSApplication]
GO
