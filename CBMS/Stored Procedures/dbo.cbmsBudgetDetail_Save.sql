SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE       PROCEDURE [dbo].[cbmsBudgetDetail_Save]
	( @MyAccountID int = null
	, @budget_detail_id int = null
	, @budget_id int = null
	, @account_id int = null	
	, @date datetime = null
	, @usage decimal(18,10) = null
	, @variable decimal(18,10) = null
	, @multiplier decimal(18,10) = null
	, @adder decimal(18,10) = null
	, @transportation decimal(18,10) = null
	, @transmission decimal(18,10) = null
	, @distribution decimal(18,10) = null
	, @other_unit decimal(18,10) = null
	, @other_fixed decimal(18,10) = null
	, @commodity_type_id decimal(18,10) = null
	)
AS
BEGIN

	set nocount on

	  declare @this_id int

	      set @this_id = @budget_detail_id 

	if @this_id is null
	begin




		insert into budget_detail
			( budget_id
			, account_id
			, date
			, usage
			, variable
			, multiplier
			, adder
			, transportation	
			, transmission
			, distribution
			, other_unit
			, other_fixed
			, commodity_type_id
			)
		 values
			( @budget_id
			, @account_id
			, @date
			, @usage
			, @variable
			, @multiplier
			, @adder
			, @transportation	
			, @transmission
			, @distribution
			, @other_unit
			, @other_fixed
			, @commodity_type_id
			)

		set @this_id = @@IDENTITY
		select @@IDENTITY as budget_detail_id

	end
	else
	begin

		   update budget_detail
		      set budget_id = @budget_id 
			, account_id = @account_id
			, date = @date
			, usage = @usage
			, variable = @variable
			, multiplier = @multiplier 
			, adder = @adder 
			, transportation = @transportation 
			, transmission = @transmission 
			, distribution = @distribution 
			, other_unit = @other_unit 
			, other_fixed = @other_fixed 
			, commodity_type_id = @commodity_type_id 
		    where budget_detail_id = @this_id

	end

	set nocount off

	exec cbmsBudgetDetail_Get @this_id



END


SET QUOTED_IDENTIFIER ON
GO
GRANT EXECUTE ON  [dbo].[cbmsBudgetDetail_Save] TO [CBMSApplication]
GO
