SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:      
     [dbo].[Cost_Usage_Account_Dtl_Del_By_Contract_Id]       

DESCRIPTION:
	Created to delete the all the cost usage data associated with the supplier accounts of given contract id
	Used by Delete tool application

INPUT PARAMETERS:
Name            DataType          Default     Description
-----------------------------------------------------------
@Contract_Id	INT

OUTPUT PARAMETERS:
Name			DataType          Default     Description   
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------



	 SELECT DISTINCT
		 --samm.Contract_Id
		 cuad.Data_Source_Cd
	 FROM
		  dbo.Cost_Usage_Account_Dtl cuad
		  JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
				ON samm.ACCOUNT_ID = cuad.ACCOUNT_ID
	 WHERE
		  contract_Id = 22960
		  --WHERE Data_Source_Cd = 100348
		  
		  22960
			45582
			47823

	BEGIN TRAN
		exec dbo.Cost_Usage_Account_Dtl_Del_By_Contract_Id 22960
	ROLLBACK TRAN
	
	select cuad.Bucket_Value,samm.contract_id from dbo.Cost_Usage_Account_Dtl cuad JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm ON samm.ACCOUNT_ID = cuad.ACCOUNT_ID
	where contract_Id=22960

AUTHOR INITIALS:
 Initials					Name
------------------------------------------------------------
 HG							Harihara Suthan G

MODIFICATIONS:
 Initials    Date			Modification
------------------------------------------------------------
 HG			2012-05-28		Created

******/

CREATE PROCEDURE dbo.Cost_Usage_Account_Dtl_Del_By_Contract_Id ( @Contract_Id INT )
AS 
BEGIN

      SET NOCOUNT ON ;

      DECLARE @Supplier_Account_List TABLE
            ( 
             Account_Id INT PRIMARY KEY CLUSTERED )

      DECLARE @Account_Id INT             

	  INSERT INTO @Supplier_Account_List
	              ( Account_Id )
      SELECT
            samm.ACCOUNT_ID
      FROM
            dbo.Cost_Usage_Account_Dtl cuad
            JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                  ON samm.ACCOUNT_ID = cuad.ACCOUNT_ID
      WHERE
            samm.Contract_ID = @Contract_Id
      GROUP BY
            samm.ACCOUNT_ID


      WHILE EXISTS ( SELECT
                        1
                     FROM
                        @Supplier_Account_List ) 
            BEGIN  
  
                  SET @Account_Id = ( SELECT TOP 1
                                          Account_Id
                                      FROM
                                          @Supplier_Account_List )  
      
                  EXEC dbo.Cost_Usage_History_Del_For_Account 
                        @Account_id  
  
                  DELETE
                        @Supplier_Account_List
                  WHERE
                        Account_Id = @Account_Id  
  
            END  

END
;
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Dtl_Del_By_Contract_Id] TO [CBMSApplication]
GO
