
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[SR_RFP_ADD_UTILITY_LP_DEFAULT_SETUP_P]
           
DESCRIPTION:             
			To insert EP&NG determinant UOMs for load profile
			
INPUT PARAMETERS:            
	Name			DataType	Default		Description  
---------------------------------------------------------------------------------  
	@vendor_id		INT			

OUTPUT PARAMETERS:
	Name			DataType		Default		Description  
---------------------------------------------------------------------------------  


 USAGE EXAMPLES:
---------------------------------------------------------------------------------  

    EXEC dbo.SR_RFP_ADD_UTILITY_LP_DEFAULT_SETUP_P 
      27289


 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy
	NR			Narayana Reddy

 MODIFICATIONS:
	Initials	Date			Modification
------------------------------------------------------------
	RR			2015-08-06		Added Header
								Global Sourcing - Modified script to get vendor country and commodity specific UOM
	NR			2017-02-22		MAINT-4841 Added  Demand determinant name for Natural Gas.
								
******/
CREATE PROCEDURE [dbo].[SR_RFP_ADD_UTILITY_LP_DEFAULT_SETUP_P] ( @vendor_id INT )
AS 
BEGIN

      SET NOCOUNT ON;

      DECLARE
            @ng_commodity_type_id INT
           ,@el_commodity_type_id INT
           ,@month_selector_type_id INT
           ,@setup_id INT
           ,@ng_unit_type_id INT
           ,@el_unit_type_id INT
           ,@el_unit_type_id1 INT
           ,@ng_unit_type_id1 INT

	
      SELECT
            @ng_commodity_type_id = ENTITY_ID
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_TYPE = 157
            AND ENTITY_NAME = 'Natural Gas'
      
      SELECT
            @el_commodity_type_id = ENTITY_ID
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_TYPE = 157
            AND ENTITY_NAME = 'Electric Power'
      
      SELECT
            @month_selector_type_id = ENTITY_ID
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_TYPE = 1007
            AND ENTITY_NAME = 'System Bill Month'
      
      SELECT
            @el_unit_type_id = ccu.Default_Uom_Type_Id
      FROM
            dbo.Country_Commodity_Uom ccu
            INNER JOIN dbo.STATE stt
                  ON ccu.COUNTRY_ID = stt.COUNTRY_ID
            INNER JOIN dbo.VENDOR_STATE_MAP vsm
                  ON stt.STATE_ID = vsm.STATE_ID
      WHERE
            vsm.VENDOR_ID = @vendor_id
            AND ccu.Commodity_Id = @el_commodity_type_id
            
      SELECT
            @ng_unit_type_id = ccu.Default_Uom_Type_Id
      FROM
            dbo.Country_Commodity_Uom ccu
            INNER JOIN dbo.STATE stt
                  ON ccu.COUNTRY_ID = stt.COUNTRY_ID
            INNER JOIN dbo.VENDOR_STATE_MAP vsm
                  ON stt.STATE_ID = vsm.STATE_ID
      WHERE
            vsm.VENDOR_ID = @vendor_id
            AND ccu.Commodity_Id = @ng_commodity_type_id
      
      SELECT
            @el_unit_type_id1 = ENTITY_ID
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_TYPE = 101
            AND ENTITY_NAME = 'kW'


      SELECT
            @ng_unit_type_id1 = ENTITY_ID
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_TYPE = 102
            AND ENTITY_NAME = 'Dth'
            AND ENTITY_DESCRIPTION = 'Unit for Gas'
            



      IF ( SELECT
            COUNT(1)
           FROM
            dbo.VENDOR_COMMODITY_MAP
           WHERE
            VENDOR_ID = @vendor_id
            AND COMMODITY_TYPE_ID = @ng_commodity_type_id
            AND IS_HISTORY = 0 ) > 0 
            BEGIN
		--//For Natural Gas
                  IF ( SELECT
                        COUNT(SR_LOAD_PROFILE_DEFAULT_SETUP_ID)
                       FROM
                        dbo.SR_LOAD_PROFILE_DEFAULT_SETUP
                       WHERE
                        COMMODITY_TYPE_ID = @ng_commodity_type_id
                        AND VENDOR_ID = @vendor_id ) = 0 
                        BEGIN
                              INSERT      INTO dbo.SR_LOAD_PROFILE_DEFAULT_SETUP
                                          ( 
                                           COMMODITY_TYPE_ID
                                          ,VENDOR_ID
                                          ,MONTH_SELECTOR_TYPE_ID )
                              VALUES
                                          ( 
                                           @ng_commodity_type_id
                                          ,@vendor_id
                                          ,@month_selector_type_id )
                                          
                              SET @setup_id = @@IDENTITY
                              
                              INSERT      INTO dbo.SR_LOAD_PROFILE_DETERMINANT
                                          ( SR_LOAD_PROFILE_DEFAULT_SETUP_ID, DETERMINANT_NAME, DETERMINANT_UNIT_TYPE_ID, IS_CHECKED )
                              VALUES
                                          ( @setup_id, 'Total Usage', @ng_unit_type_id, 1 ),
                                        ------
                                          ( @setup_id, 'Demand', @ng_unit_type_id1, 0 )
                        END
            END

      IF ( SELECT
            COUNT(1)
           FROM
            dbo.VENDOR_COMMODITY_MAP
           WHERE
            VENDOR_ID = @vendor_id
            AND COMMODITY_TYPE_ID = @el_commodity_type_id
            AND IS_HISTORY = 0 ) > 0 
            BEGIN

		--//For Electric Power
                  IF ( SELECT
                        COUNT(SR_LOAD_PROFILE_DEFAULT_SETUP_ID)
                       FROM
                        dbo.SR_LOAD_PROFILE_DEFAULT_SETUP
                       WHERE
                        COMMODITY_TYPE_ID = @el_commodity_type_id
                        AND VENDOR_ID = @vendor_id ) = 0 
                        BEGIN
                              INSERT      INTO dbo.SR_LOAD_PROFILE_DEFAULT_SETUP
                                          ( 
                                           COMMODITY_TYPE_ID
                                          ,VENDOR_ID
                                          ,MONTH_SELECTOR_TYPE_ID )
                              VALUES
                                          ( 
                                           @el_commodity_type_id
                                          ,@vendor_id
                                          ,@month_selector_type_id )
                                          
                              SET @setup_id = @@IDENTITY
                              
                              INSERT      INTO dbo.SR_LOAD_PROFILE_DETERMINANT
                                          ( SR_LOAD_PROFILE_DEFAULT_SETUP_ID, DETERMINANT_NAME, DETERMINANT_UNIT_TYPE_ID, IS_CHECKED )
                              VALUES
                                          ( @setup_id, 'Total Usage', @el_unit_type_id, 1 )
                              ,           ( @setup_id, 'On Peak kWh', @el_unit_type_id, 0 )
                              ,           ( @setup_id, 'Off Peak kWh', @el_unit_type_id, 0 )
                              ,           ( @setup_id, 'Intermediate Peak kWh', @el_unit_type_id, 0 )
                              ,           ( @setup_id, 'Actual/On Peak kW', @el_unit_type_id1, 0 )
                              ,           ( @setup_id, 'Contract kW', @el_unit_type_id1, 0 )
                              ,           ( @setup_id, 'Billing kW', @el_unit_type_id1, 0 )
                        END
            END

END;



;
GO


GRANT EXECUTE ON  [dbo].[SR_RFP_ADD_UTILITY_LP_DEFAULT_SETUP_P] TO [CBMSApplication]
GO
