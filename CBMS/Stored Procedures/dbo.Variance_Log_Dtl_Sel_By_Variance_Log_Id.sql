SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

 NAME: dbo.Variance_Log_Dtl_Sel_By_Variance_Log_Id 

 DESCRIPTION: 
	
	  Get the all variance log details given by the Variance_Log_Id.
	  

 INPUT PARAMETERS:  
 Name								DataType			Default			Description  
---------------------------------------------------------------------------------------------------------------
 Variance_Log_Id                    VARCHAR(MAX)
 
 OUTPUT PARAMETERS:  

 Name								DataType			Default			Description
---------------------------------------------------------------------------------------------------------------
 
 USAGE EXAMPLES:  
---------------------------------------------------------------------------------------------------------------

  EXEC dbo.Variance_Log_Dtl_Sel_By_Variance_Log_Id '290870,8345701'
  

  
 AUTHOR INITIALS:  
 Initials				Name  
---------------------------------------------------------------------------------------------------------------
 NR                     NARAYANA REDDY					
  
 MODIFICATIONS   
 Initials				Date			Modification
---------------------------------------------------------------------------------------------------------------
 NR						2014-06-19		Created for Data operation phase-2.
 
******/
CREATE PROCEDURE dbo.Variance_Log_Dtl_Sel_By_Variance_Log_Id
      ( 
       @Variance_Log_Id VARCHAR(MAX) )
AS 
BEGIN
      SET NOCOUNT ON 
       
 
      SELECT
            vlog.Variance_log_id
           ,vlog.Account_Id
           ,vlog.Account_Number
           ,vlog.Service_month
           ,vlog.Commodity_id
           ,vlog.Variance_Status_Cd
      FROM
            dbo.Variance_Log vlog
            INNER JOIN dbo.ufn_split(@Variance_Log_Id, ',') ufn
                  ON ufn.Segments = vlog.Variance_Log_Id
    
            
END

;
GO
GRANT EXECUTE ON  [dbo].[Variance_Log_Dtl_Sel_By_Variance_Log_Id] TO [CBMSApplication]
GO
