SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME: dbo.Site_Info_GetForInvoice_SEL

DESCRIPTION:

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@cu_invoice_id    	int   

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC dbo.Site_Info_GetForInvoice_SEL 16046
	exec Site_Info_GetForInvoice_SEL  75207047

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	NK			Nageswara Rao Kosuri
	NR			Narayana Reeddy
	TRK			Ramakrishna Thummala  
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
		02/05/2010	Created       	
	SSR			03/18/2010	Removed Cu_invoice_account_map(Account_id) with cu_invoice_service_month(Account_id)
	SKA			04/19/2010	USed CTE Concept to get the Supplier & Utility Account
	SKA			06/23/2010	Add a join with Contract table to get Contract_Id and Number
	SSR			07/09/2010  Removed the logic of fetching data from SAMM table
							(( SAMM.meter_disassociation_date > Account.Supplier_Account_Begin_Dt  
								OR SAMM.meter_disassociation_date IS NULL ))
							(After contract enhancement application is populating both Meter_association_date and meter_disassociation_Date column supplier_Account_meter_map and removing the entries when ever is_history = 1)
	NR			2019-11-16	Add Contract - Added supplier dates condition.
	TRK			2020-02-17			Added the Is_Consolidated_Billing column in select List.
******/
CREATE PROCEDURE [dbo].[Site_Info_GetForInvoice_SEL]
    (
        @cu_invoice_id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        WITH CTE_Supplier_Account_Det
        AS (
               SELECT
                    cha.Supplier_Account_begin_Dt
                    , cha.Supplier_Account_End_Dt
                    , a.ACCOUNT_ID
                    , a.VENDOR_ID
                    , a.ACCOUNT_TYPE_ID
                    , a.ACCOUNT_NUMBER
                    , cha.Supplier_Account_Config_Id
               FROM
                    dbo.CU_INVOICE_SERVICE_MONTH cism
                    JOIN dbo.ACCOUNT AS a
                        ON cism.Account_ID = a.ACCOUNT_ID
                    JOIN Core.Client_Hier_Account cha
                        ON a.ACCOUNT_ID = cha.Account_Id
               WHERE
                    cism.CU_INVOICE_ID = @cu_invoice_id
                    AND (   cism.SERVICE_MONTH IS NULL
                            OR  ((   cha.Account_Type = 'Utility'
                                     OR (   cha.Account_Type = 'Supplier'
                                            AND (   cism.Begin_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                                                  AND     cha.Supplier_Account_End_Dt
                                                    OR  cism.End_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                                                    AND     cha.Supplier_Account_End_Dt
                                                    OR  cha.Supplier_Account_begin_Dt BETWEEN cism.Begin_Dt
                                                                                      AND     cism.End_Dt
                                                    OR  cha.Supplier_Account_End_Dt BETWEEN cism.Begin_Dt
                                                                                    AND     cism.End_Dt)))))
               GROUP BY
                   cha.Supplier_Account_begin_Dt
                   , cha.Supplier_Account_End_Dt
                   , a.ACCOUNT_ID
                   , a.VENDOR_ID
                   , a.ACCOUNT_TYPE_ID
                   , a.ACCOUNT_NUMBER
                   , cha.Supplier_Account_Config_Id
           )
             , CTE_Account_Det
        AS (
               SELECT
                    m.ACCOUNT_ID
                    , m.METER_ID
                    , cte1.Supplier_Account_begin_Dt
                    , cte1.Supplier_Account_End_Dt
                    , cte1.VENDOR_ID
                    , cte1.ACCOUNT_TYPE_ID
                    , cte1.ACCOUNT_NUMBER
                    , NULL contract_id
               FROM
                    dbo.METER m
                    JOIN CTE_Supplier_Account_Det cte1
                        ON cte1.ACCOUNT_ID = m.ACCOUNT_ID
               UNION
               SELECT
                    map.ACCOUNT_ID
                    , map.METER_ID
                    , cte2.Supplier_Account_begin_Dt
                    , cte2.Supplier_Account_End_Dt
                    , cte2.VENDOR_ID
                    , cte2.ACCOUNT_TYPE_ID
                    , cte2.ACCOUNT_NUMBER
                    , map.Contract_ID
               FROM
                    dbo.SUPPLIER_ACCOUNT_METER_MAP AS map
                    JOIN CTE_Supplier_Account_Det cte2
                        ON cte2.ACCOUNT_ID = map.ACCOUNT_ID
                           AND  cte2.Supplier_Account_Config_Id = map.Supplier_Account_Config_Id
           )
        SELECT
            cl.CLIENT_ID
            , cl.CLIENT_NAME
            , s.SITE_ID
            , RTRIM(ad.CITY) + ', ' + st.STATE_NAME + ' (' + s.SITE_NAME + ')' site_name
            , m.ADDRESS_ID
            , ad.ADDRESS_LINE1
            , ad.ADDRESS_LINE2
            , ad.CITY
            , st.STATE_ID
            , st.STATE_NAME
            , m.METER_ID
            , m.RATE_ID
            , m.METER_NUMBER
            , m.TAX_EXEMPT_STATUS
            , m.IS_HISTORY
            , x.ACCOUNT_TYPE_ID
            , act.ENTITY_NAME AS account_type
            , x.VENDOR_ID AS utility_id
            , v.VENDOR_NAME AS utility_name
            , r.RATE_NAME
            , r.COMMODITY_TYPE_ID
            , com.Commodity_Name commodity_type
            , x.ACCOUNT_ID
            , CASE WHEN act.ENTITY_NAME = 'Supplier'
                        AND x.ACCOUNT_NUMBER IS NULL THEN 'Not Yet Assigned'
                  ELSE x.ACCOUNT_NUMBER
              END AS account_number
            , x.Supplier_Account_begin_Dt AS Contract_Start_Date
            , x.Supplier_Account_End_Dt AS Contract_End_Date
            , con.CONTRACT_ID
            , con.ED_CONTRACT_NUMBER
			, CASE
               WHEN acb.Account_Id IS NOT NULL THEN
                   1
               ELSE
                   0
           END AS Is_Consolidated_Billing
        FROM
            CTE_Account_Det x
            JOIN dbo.METER m
                ON m.METER_ID = x.METER_ID
            JOIN dbo.ADDRESS ad
                ON ad.ADDRESS_ID = m.ADDRESS_ID
            JOIN dbo.SITE AS s
                ON s.SITE_ID = ad.ADDRESS_PARENT_ID
            JOIN dbo.CLIENT AS cl
                ON cl.CLIENT_ID = s.Client_ID
            JOIN dbo.VENDOR AS v
                ON v.VENDOR_ID = x.VENDOR_ID
            JOIN dbo.ENTITY AS act
                ON act.ENTITY_ID = x.ACCOUNT_TYPE_ID
            JOIN dbo.STATE st
                ON st.STATE_ID = ad.STATE_ID
            JOIN dbo.RATE r
                ON r.RATE_ID = m.RATE_ID
            JOIN dbo.Commodity AS com
                ON com.Commodity_Id = r.COMMODITY_TYPE_ID
            JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.Account_ID = x.ACCOUNT_ID
            LEFT JOIN dbo.CONTRACT con
                ON con.CONTRACT_ID = x.contract_id
			LEFT OUTER JOIN dbo.Account_Consolidated_Billing_Vendor acb
            ON acb.Account_Id = x.Account_Id
        GROUP BY
            cl.CLIENT_ID
            , cl.CLIENT_NAME
            , s.SITE_ID
            , ad.CITY
            , st.STATE_NAME
            , s.SITE_NAME
            , m.ADDRESS_ID
            , ad.ADDRESS_LINE1
            , ad.ADDRESS_LINE2
            , ad.CITY
            , st.STATE_ID
            , st.STATE_NAME
            , m.METER_ID
            , m.RATE_ID
            , m.METER_NUMBER
            , m.TAX_EXEMPT_STATUS
            , m.IS_HISTORY
            , x.ACCOUNT_TYPE_ID
            , act.ENTITY_NAME
            , x.VENDOR_ID
            , v.VENDOR_NAME
            , r.RATE_NAME
            , r.COMMODITY_TYPE_ID
            , com.Commodity_Name
            , x.ACCOUNT_ID
            , x.ACCOUNT_NUMBER
            , x.Supplier_Account_begin_Dt
            , x.Supplier_Account_End_Dt
            , con.CONTRACT_ID
            , con.ED_CONTRACT_NUMBER
			, acb.Account_Id;


    END;



GO
GRANT EXECUTE ON  [dbo].[Site_Info_GetForInvoice_SEL] TO [CBMSApplication]
GO
