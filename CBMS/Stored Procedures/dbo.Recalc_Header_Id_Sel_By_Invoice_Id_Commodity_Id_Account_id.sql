SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Recalc_Header_Id_Sel_By_Invoice_Id_Commodity_Id_Account_id          
              
Description:              
                    
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
     @Account_Id						INT
     @Commodity_Id						INT
     @Cu_Invoice_Id						INT
     
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
    
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                
	
	
	
    EXEC dbo.Recalc_Header_Id_Sel_By_Invoice_Id_Commodity_Id_Account_id
      @Account_Id =55236
     ,@Commodity_Id = 290
     ,@Cu_Invoice_Id = 24604392
    
     
	
	
	
             
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2015-08-07		Created For AS400-II.         
             
******/ 
CREATE  PROCEDURE [dbo].[Recalc_Header_Id_Sel_By_Invoice_Id_Commodity_Id_Account_id]
      ( 
       @Account_Id INT
      ,@Commodity_Id INT
      ,@Cu_Invoice_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 
		
     
      SELECT
            RECALC_HEADER_ID
      FROM
            dbo.RECALC_HEADER rh
      WHERE
            ACCOUNT_ID = @Account_Id
            AND Commodity_Id = @Commodity_Id
            AND CU_INVOICE_ID = @Cu_Invoice_Id 
            
      
      
            
END;

;
GO
GRANT EXECUTE ON  [dbo].[Recalc_Header_Id_Sel_By_Invoice_Id_Commodity_Id_Account_id] TO [CBMSApplication]
GO
