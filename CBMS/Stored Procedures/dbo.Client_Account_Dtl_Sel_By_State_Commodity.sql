SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:  
	 dbo.Client_Account_Dtl_Sel_By_State_Commodity        
                
Description:                
		This sproc to get the client account details based state commodity.        
                             
 Input Parameters:                
    Name						DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
	@State_Id					INT
	@Commodity_Id				INT

      
 Output Parameters:                      
    Name					  DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
----------------------------------------------------------------------------------------     

   Exec dbo.Client_Account_Dtl_Sel_By_State_Commodity   170,134,290    
       
   
Author Initials:                
    Initials		Name                
----------------------------------------------------------------------------------------                  
	NR				Narayana Reddy                 
 Modifications:                
    Initials        Date			Modification                
----------------------------------------------------------------------------------------                  
    NR				2016-11-15		Created For MAINT-4563.           
               
******/   
CREATE PROCEDURE [dbo].[Client_Account_Dtl_Sel_By_State_Commodity]
      ( 
       @Client_Id INT
      ,@State_Id INT
      ,@Commodity_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 

      SELECT
            ch.Client_Id
           ,ch.Client_Name
           ,ch.Site_Id
           ,ch.Site_name
           ,ch.City
           ,ch.State_Name
           ,cha.Account_Id
           ,cha.Account_Number
      FROM
            core.Client_Hier ch
            INNER JOIN core.Client_Hier_Account cha
                  ON ch.Client_Hier_Id = cha.Client_Hier_Id
      WHERE
            ch.Site_Id > 0
            AND cha.Account_Type = 'Utility'
            AND cha.Account_Not_Managed = 0
            AND ch.State_Id = @State_Id
            AND cha.Commodity_Id = @Commodity_Id
            AND ch.Client_Id = @Client_Id
      GROUP BY
            ch.Client_Id
           ,ch.Client_Name
           ,ch.Site_Id
           ,ch.Site_name
           ,ch.City
           ,ch.State_Name
           ,cha.Account_Id
           ,cha.Account_Number
      ORDER BY
            ch.Client_Name
           ,ch.Site_name
            
      
END
      



;
GO
GRANT EXECUTE ON  [dbo].[Client_Account_Dtl_Sel_By_State_Commodity] TO [CBMSApplication]
GO
