SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        dbo.RM_Deal_Ticket_Sel_Total_Hedged_Volumes                      
                        
Description:                        
        To get site's hedge configurations  
                        
Input Parameters:                        
    Name    DataType        Default     Description                          
--------------------------------------------------------------------------------  
 @Client_Id   INT  
    @Commodity_Id  INT  
    @Hedge_Type  INT  
    @Start_Dt DATE  
    @End_Dt  DATE  
    @Contract_Id  VARCHAR(MAX)  
    @Site_Id   INT    NULL  
                        
 Output Parameters:                              
 Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
     
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
DECLARE @Trade_tvp_Hedge_Contract_Sites AS Trade.tvp_Hedge_Contract_Sites
	INSERT INTO @Trade_tvp_Hedge_Contract_Sites
	VALUES  ( 155898, 2177 )
EXEC dbo.RM_Deal_Ticket_Sel_Total_Hedged_Volumes  @Client_Id = 1005,@Commodity_Id = 291  
	  ,@Start_Dt='2019-07-01',@End_Dt='2019-07-01',@Hedge_Type=586,@Uom_Id=25,@Index_Id=325,@Hedge_Mode_Type_Id=621
	  ,@Deal_Ticket_Frequency_Cd= 102744,@Trade_tvp_Hedge_Contract_Sites=@Trade_tvp_Hedge_Contract_Sites 

DECLARE @Trade_tvp_Hedge_Contract_Sites AS Trade.tvp_Hedge_Contract_Sites
		INSERT INTO @Trade_tvp_Hedge_Contract_Sites
		VALUES  ( 145427, 2372 ),( 145427, 2030 )
	EXEC dbo.RM_Deal_Ticket_Sel_Total_Hedged_Volumes  @Client_Id = 1005,@Commodity_Id = 291  
	  ,@Start_Dt='2019-01-01',@End_Dt='2019-12-01',@Hedge_Type=586,@Uom_Id=25,@Index_Id=325,@Hedge_Mode_Type_Id=621
	  ,@Deal_Ticket_Frequency_Cd= 102749
	  ,@Trade_tvp_Hedge_Contract_Sites=@Trade_tvp_Hedge_Contract_Sites 
  
Author Initials:                        
    Initials    Name                        
--------------------------------------------------------------------------------  
    RR          Raghu Reddy     
                         
 Modifications:                        
    Initials	Date		Modification                        
--------------------------------------------------------------------------------  
	RR          02-11-2018	Created GRM  
    HG			2019-08-02	MAINT-1481, Modified the query to pull the hedged volumes just based on site irrespective hedge type  
	RR			2019-09-05	GRM-1531 - Added back missing code lines assigns value to @Hedge_Contract_Sites_Cnt, code got deleted
							in previous fix MAINT-1481   
                       
******/
CREATE PROCEDURE [dbo].[RM_Deal_Ticket_Sel_Total_Hedged_Volumes]
     (
         @Client_Id INT
         , @Commodity_Id INT
         , @Hedge_Type INT
         , @Start_Dt DATE
         , @End_Dt DATE
         , @Contract_Id VARCHAR(MAX) = NULL
         , @Uom_Id INT = NULL
         , @Site_Str VARCHAR(MAX) = NULL
         , @Division_Id VARCHAR(MAX) = NULL
         , @RM_Group_Id VARCHAR(MAX) = NULL
         , @Hedge_Mode_Type_Id INT = NULL
         , @Index_Id INT = NULL
         , @Deal_Ticket_Frequency_Cd INT = NULL
         , @Trade_tvp_Hedge_Contract_Sites AS Trade.tvp_Hedge_Contract_Sites READONLY
     )
WITH RECOMPILE
AS
    BEGIN
        SET NOCOUNT ON;

        CREATE TABLE #Volumes
             (
                 Client_Hier_Id INT
                 , Account_Id INT
                 , Service_Month DATE
                 , Forecast_Volume DECIMAL(28, 0)
                 , Uom_Id INT
                 , Deal_Ticket_Id INT
             );

        DECLARE @Common_Uom_Id INT;

        DECLARE @Hedge_Type_Input VARCHAR(200);
        DECLARE @Hedge_Mode_Type VARCHAR(200);
        DECLARE @Hedge_Index VARCHAR(200);
        DECLARE
            @Deal_Ticket_Frequency VARCHAR(25)
            , @Hedge_Contract_Sites_Cnt INT;

        DECLARE @RM_Group_Sites INT;

        SELECT
            @Deal_Ticket_Frequency = Code_Value
        FROM
            dbo.Code
        WHERE
            Code_Id = @Deal_Ticket_Frequency_Cd;

        SELECT
            @Hedge_Type_Input = ENTITY_NAME
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_ID = @Hedge_Type;

        SELECT
            @Hedge_Contract_Sites_Cnt = COUNT(1)
        FROM
            @Trade_tvp_Hedge_Contract_Sites
        WHERE
            @Hedge_Type_Input = 'Physical';

        SELECT
            @Hedge_Mode_Type = ENTITY_NAME
        FROM
            dbo.ENTITY
        WHERE
            (   @Hedge_Mode_Type_Id IS NOT NULL
                AND ENTITY_ID = @Hedge_Mode_Type_Id)
            OR  (   @Hedge_Mode_Type_Id IS NULL
                    AND ENTITY_NAME = 'Index'
                    AND ENTITY_DESCRIPTION = 'HEDGE_MODE_TYPE');


        SELECT
            @Hedge_Index = pridx.ENTITY_NAME
        FROM
            dbo.ENTITY pridx
        WHERE
            pridx.ENTITY_ID = @Index_Id;


        CREATE TABLE #RM_Group_Sites
             (
                 Site_Id INT
             );

        INSERT INTO #RM_Group_Sites
             (
                 Site_Id
             )
        SELECT
            ch.Site_Id
        FROM
            dbo.Cbms_Sitegroup_Participant rgd
            INNER JOIN dbo.Code grp
                ON grp.Code_Id = rgd.Group_Participant_Type_Cd
            INNER JOIN dbo.CONTRACT c
                ON c.CONTRACT_ID = rgd.Group_Participant_Id
            INNER JOIN Core.Client_Hier_Account chasupp
                ON chasupp.Supplier_Contract_ID = c.CONTRACT_ID
            INNER JOIN Core.Client_Hier_Account chautil
                ON chasupp.Meter_Id = chautil.Meter_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = chasupp.Client_Hier_Id
        WHERE
            (   EXISTS (   SELECT
                                1
                           FROM
                                dbo.ufn_split(@RM_Group_Id, ',') con
                           WHERE
                                rgd.Cbms_Sitegroup_Id = CAST(Segments AS INT))
                AND grp.Code_Value = 'Contract'
                AND chasupp.Account_Type = 'Supplier'
                AND chautil.Account_Type = 'Utility');

        INSERT INTO #RM_Group_Sites
             (
                 Site_Id
             )
        SELECT
            ch.Site_Id
        FROM
            dbo.Cbms_Sitegroup_Participant rgd
            INNER JOIN dbo.Code grp
                ON grp.Code_Id = rgd.Group_Participant_Type_Cd
            INNER JOIN Core.Client_Hier ch
                ON ch.Site_Id = rgd.Group_Participant_Id
        WHERE
            (   EXISTS (   SELECT
                                1
                           FROM
                                dbo.ufn_split(@RM_Group_Id, ',') con
                           WHERE
                                rgd.Cbms_Sitegroup_Id = CAST(Segments AS INT))
                AND grp.Code_Value = 'Site');

        INSERT INTO #RM_Group_Sites
             (
                 Site_Id
             )
        SELECT  CAST(Segments AS INT)FROM   dbo.ufn_split(@Site_Str, ',') con;

        INSERT INTO #RM_Group_Sites
             (
                 Site_Id
             )
        SELECT
            ch.Site_Id
        FROM
            Core.Client_Hier ch
        WHERE
            ch.Site_Id > 0
            AND (EXISTS (   SELECT
                                1
                            FROM
                                dbo.ufn_split(@Division_Id, ',') con
                            WHERE
                                ch.Sitegroup_Id = CAST(Segments AS INT)));

        SELECT
            @RM_Group_Sites = NULLIF(COUNT(1), 0)
        FROM
            #RM_Group_Sites
        WHERE
            @Hedge_Type_Input = 'Financial';


        ----------------------To get site own config  
        INSERT INTO #Volumes
             (
                 Client_Hier_Id
                 , Account_Id
                 , Service_Month
                 , Forecast_Volume
                 , Uom_Id
                 , Deal_Ticket_Id
             )
        SELECT
            dtv.Client_Hier_Id
            , dtv.Account_Id
            , dtv.Deal_Month
            , (CASE WHEN frq.Code_Value = 'Monthly' THEN (CASE WHEN trdact.Code_Value = 'Buy' THEN dtv.Total_Volume
                                                              WHEN trdact.Code_Value = 'Sell' THEN -dtv.Total_Volume
                                                          END)
                   WHEN frq.Code_Value = 'Daily' THEN (CASE WHEN trdact.Code_Value = 'Buy' THEN dtv.Total_Volume
                                                           WHEN trdact.Code_Value = 'Sell' THEN -dtv.Total_Volume
                                                       END) * dd.DAYS_IN_MONTH_NUM
               END) * cuc.CONVERSION_FACTOR AS Forecast_Volume
            , ISNULL(chob.RM_Forecast_UOM_Type_Id, dtv.Uom_Type_Id) AS Uom_Id
            , dtv.Deal_Ticket_Id
        FROM
            Core.Client_Hier ch
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON ch.Client_Hier_Id = chob.Client_Hier_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY et
                ON et.ENTITY_ID = chhc.Hedge_Type_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtv
                ON ch.Client_Hier_Id = dtv.Client_Hier_Id
            INNER JOIN Trade.Deal_Ticket dt
                ON dtv.Deal_Ticket_Id = dt.Deal_Ticket_Id
                   AND  dt.Commodity_Id = @Commodity_Id
            INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = dtv.Uom_Type_Id
                   AND  cuc.CONVERTED_UNIT_ID = ISNULL(chob.RM_Forecast_UOM_Type_Id, dtv.Uom_Type_Id)
            INNER JOIN dbo.PRICE_INDEX pridx
                ON pridx.PRICE_INDEX_ID = dt.Price_Index_Id
            INNER JOIN dbo.ENTITY idx
                ON pridx.INDEX_ID = idx.ENTITY_ID
            INNER JOIN dbo.ENTITY hmt
                ON dt.Hedge_Mode_Type_Id = hmt.ENTITY_ID
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dtv.Client_Hier_Id = dtch.Client_Hier_Id
                   AND  dt.Deal_Ticket_Id = dtch.Deal_Ticket_Id
            INNER JOIN Trade.Trade_Price tp
                ON dtv.Trade_Price_Id = tp.Trade_Price_Id
            INNER JOIN dbo.Code trdact
                ON dt.Trade_Action_Type_Cd = trdact.Code_Id
            INNER JOIN dbo.Code frq
                ON dt.Deal_Ticket_Frequency_Cd = frq.Code_Id
            INNER JOIN meta.DATE_DIM dd
                ON dd.DATE_D = dtv.Deal_Month
        WHERE
            ch.Site_Id > 0
            AND ch.Client_Id = @Client_Id
            AND chob.Commodity_Id = @Commodity_Id
            AND (   @Start_Dt BETWEEN chhc.Config_Start_Dt
                              AND     chhc.Config_End_Dt
                    OR  @End_Dt BETWEEN chhc.Config_Start_Dt
                                AND     chhc.Config_End_Dt
                    OR  chhc.Config_Start_Dt BETWEEN @Start_Dt
                                             AND     @End_Dt
                    OR  chhc.Config_End_Dt BETWEEN @Start_Dt
                                           AND     @End_Dt)
            AND (   (   @Hedge_Type_Input = 'Physical'
                        AND et.ENTITY_NAME IN ( 'Physical', 'Physical & Financial' ))
                    OR  (   @Hedge_Type_Input = 'Financial'
                            AND et.ENTITY_NAME IN ( 'Financial', 'Physical & Financial' )))
            AND (   @RM_Group_Sites IS NULL
                    OR  EXISTS (SELECT  1 FROM  #RM_Group_Sites rgs WHERE   rgs.Site_Id = ch.Site_Id))
            AND (   @Hedge_Contract_Sites_Cnt = 0
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        Core.Client_Hier chhdg
                                        INNER JOIN @Trade_tvp_Hedge_Contract_Sites hcs
                                            ON hcs.Site_Id = chhdg.Site_Id
                                   WHERE
                                        chhdg.Client_Hier_Id = ch.Client_Hier_Id))
            AND dtv.Deal_Month BETWEEN @Start_Dt
                               AND     @End_Dt
            AND (   (   @Hedge_Mode_Type = 'Index'
                        AND @Hedge_Index = 'Nymex'
                        AND hmt.ENTITY_NAME = 'Index')
                    OR  (   @Hedge_Mode_Type = 'Basis'
                            AND hmt.ENTITY_NAME = 'Basis')
                    OR  (   @Hedge_Mode_Type = 'Index'
                            AND @Hedge_Index <> 'Nymex'
                            AND hmt.ENTITY_NAME = 'Index'))
            AND tp.Trade_Price IS NOT NULL
        GROUP BY
            dtv.Client_Hier_Id
            , dtv.Account_Id
            , dtv.Deal_Month
            , trdact.Code_Value
            , dtv.Total_Volume
            , cuc.CONVERSION_FACTOR
            , ISNULL(chob.RM_Forecast_UOM_Type_Id, dtv.Uom_Type_Id)
            , dtv.Deal_Ticket_Id
            , frq.Code_Value
            , dd.DAYS_IN_MONTH_NUM;



        ----------------------To get default config                         
        INSERT INTO #Volumes
             (
                 Client_Hier_Id
                 , Account_Id
                 , Service_Month
                 , Forecast_Volume
                 , Uom_Id
                 , Deal_Ticket_Id
             )
        SELECT
            dtv.Client_Hier_Id
            , dtv.Account_Id
            , dtv.Deal_Month
            , (CASE WHEN frq.Code_Value = 'Monthly' THEN (CASE WHEN trdact.Code_Value = 'Buy' THEN dtv.Total_Volume
                                                              WHEN trdact.Code_Value = 'Sell' THEN -dtv.Total_Volume
                                                          END)
                   WHEN frq.Code_Value = 'Daily' THEN (CASE WHEN trdact.Code_Value = 'Buy' THEN dtv.Total_Volume
                                                           WHEN trdact.Code_Value = 'Sell' THEN -dtv.Total_Volume
                                                       END) * dd.DAYS_IN_MONTH_NUM
               END) * cuc.CONVERSION_FACTOR AS Forecast_Volume
            , ISNULL(chob.RM_Forecast_UOM_Type_Id, dtv.Uom_Type_Id) AS Uom_Id
            , dtv.Deal_Ticket_Id
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier chclient
                ON ch.Client_Id = chclient.Client_Id
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON chclient.Client_Hier_Id = chob.Client_Hier_Id
                   AND  ch.Country_Id = chob.Country_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY et
                ON et.ENTITY_ID = chhc.Hedge_Type_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtv
                ON ch.Client_Hier_Id = dtv.Client_Hier_Id
            INNER JOIN Trade.Deal_Ticket dt
                ON dtv.Deal_Ticket_Id = dt.Deal_Ticket_Id
                   AND  dt.Commodity_Id = @Commodity_Id
            INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = dtv.Uom_Type_Id
                   AND  cuc.CONVERTED_UNIT_ID = ISNULL(chob.RM_Forecast_UOM_Type_Id, dtv.Uom_Type_Id)
            INNER JOIN dbo.PRICE_INDEX pridx
                ON pridx.PRICE_INDEX_ID = dt.Price_Index_Id
            INNER JOIN dbo.ENTITY idx
                ON pridx.INDEX_ID = idx.ENTITY_ID
            INNER JOIN dbo.ENTITY hmt
                ON dt.Hedge_Mode_Type_Id = hmt.ENTITY_ID
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dtv.Client_Hier_Id = dtch.Client_Hier_Id
                   AND  dt.Deal_Ticket_Id = dtch.Deal_Ticket_Id
            INNER JOIN Trade.Trade_Price tp
                ON dtv.Trade_Price_Id = tp.Trade_Price_Id
            INNER JOIN dbo.Code trdact
                ON dt.Trade_Action_Type_Cd = trdact.Code_Id
            INNER JOIN dbo.Code frq
                ON dt.Deal_Ticket_Frequency_Cd = frq.Code_Id
            INNER JOIN meta.DATE_DIM dd
                ON dd.DATE_D = dtv.Deal_Month
        WHERE
            ch.Site_Id > 0
            AND ch.Client_Id = @Client_Id
            AND chclient.Sitegroup_Id = 0
            AND chob.Commodity_Id = @Commodity_Id
            AND (   @Start_Dt BETWEEN chhc.Config_Start_Dt
                              AND     chhc.Config_End_Dt
                    OR  @End_Dt BETWEEN chhc.Config_Start_Dt
                                AND     chhc.Config_End_Dt
                    OR  chhc.Config_Start_Dt BETWEEN @Start_Dt
                                             AND     @End_Dt
                    OR  chhc.Config_End_Dt BETWEEN @Start_Dt
                                           AND     @End_Dt)
            AND (   (   @Hedge_Type_Input = 'Physical'
                        AND et.ENTITY_NAME IN ( 'Physical', 'Physical & Financial' ))
                    OR  (   @Hedge_Type_Input = 'Financial'
                            AND et.ENTITY_NAME IN ( 'Financial', 'Physical & Financial' )))
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.RM_Client_Hier_Onboard siteob
                               WHERE
                                    siteob.Client_Hier_Id = ch.Client_Hier_Id
                                    AND siteob.Commodity_Id = chob.Commodity_Id)
            AND (   @RM_Group_Sites IS NULL
                    OR  EXISTS (SELECT  1 FROM  #RM_Group_Sites rgs WHERE   rgs.Site_Id = ch.Site_Id))
            AND (   @Hedge_Contract_Sites_Cnt = 0
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        Core.Client_Hier chhdg
                                        INNER JOIN @Trade_tvp_Hedge_Contract_Sites hcs
                                            ON hcs.Site_Id = chhdg.Site_Id
                                   WHERE
                                        chhdg.Client_Hier_Id = ch.Client_Hier_Id))
            AND dtv.Deal_Month BETWEEN @Start_Dt
                               AND     @End_Dt
            AND (   (   @Hedge_Mode_Type = 'Index'
                        AND @Hedge_Index = 'Nymex'
                        AND hmt.ENTITY_NAME = 'Index')
                    OR  (   @Hedge_Mode_Type = 'Basis'
                            AND hmt.ENTITY_NAME = 'Basis')
                    OR  (   @Hedge_Mode_Type = 'Index'
                            AND @Hedge_Index <> 'Nymex'
                            AND hmt.ENTITY_NAME = 'Index'))
            AND tp.Trade_Price IS NOT NULL
        GROUP BY
            dtv.Client_Hier_Id
            , dtv.Account_Id
            , dtv.Deal_Month
            , trdact.Code_Value
            , dtv.Total_Volume
            , cuc.CONVERSION_FACTOR
            , ISNULL(chob.RM_Forecast_UOM_Type_Id, dtv.Uom_Type_Id)
            , dtv.Deal_Ticket_Id
            , frq.Code_Value
            , dd.DAYS_IN_MONTH_NUM;

        SELECT  @Common_Uom_Id = MAX(Uom_Id)FROM    #Volumes;

        SELECT
            dd.DATE_D AS Service_Month
            , CASE WHEN @Deal_Ticket_Frequency = 'Monthly' THEN
                       CAST(ISNULL(SUM(vol.Forecast_Volume * cuc.CONVERSION_FACTOR), 0) AS DECIMAL(28, 0))
                  WHEN @Deal_Ticket_Frequency = 'Daily' THEN
                      CAST(ISNULL(SUM(vol.Forecast_Volume * cuc.CONVERSION_FACTOR) / dd.DAYS_IN_MONTH_NUM, 0) AS DECIMAL(28, 0))
              END AS Volume_Hedged
        FROM
            meta.DATE_DIM dd
            LEFT JOIN #Volumes vol
                ON dd.DATE_D = vol.Service_Month
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = vol.Uom_Id
                   AND  cuc.CONVERTED_UNIT_ID = ISNULL(@Uom_Id, @Common_Uom_Id)
        WHERE
            dd.DATE_D BETWEEN @Start_Dt
                      AND     @End_Dt
        GROUP BY
            dd.DATE_D
            , dd.DAYS_IN_MONTH_NUM;

    END;











GO
GRANT EXECUTE ON  [dbo].[RM_Deal_Ticket_Sel_Total_Hedged_Volumes] TO [CBMSApplication]
GO
