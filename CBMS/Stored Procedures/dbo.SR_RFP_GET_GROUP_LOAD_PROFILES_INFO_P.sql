
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[SR_RFP_Comment_Ins]
           
DESCRIPTION:             
			
			
INPUT PARAMETERS:            
	Name					DataType		Default		Description  
---------------------------------------------------------------------------------  
	@SR_RFP_Id				INT
    @User_Info_Id			INT
    @Comment_Text			VARCHAR(MAX)


OUTPUT PARAMETERS:
	Name			DataType		Default		Description  
---------------------------------------------------------------------------------  


 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
	SELECT TOP 10 * FROM dbo.SR_RFP
	
	EXEC dbo.SR_RFP_GET_GROUP_LOAD_PROFILES_INFO_P 1,1,5
	EXEC dbo.SR_RFP_GET_GROUP_LOAD_PROFILES_INFO_P 1,1,13244
	EXEC dbo.SR_RFP_GET_GROUP_LOAD_PROFILES_INFO_P 1,1,13255
	EXEC dbo.SR_RFP_GET_GROUP_LOAD_PROFILES_INFO_P 1,1,13256
	

 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2015-10-15	Added Header
							Global Sourcing - Phase2 - Replaced three functiouns with cross apply
							Two functions dbo.SR_RFP_FN_GET_FILE_NAME, dbo.SR_RFP_FN_GET_UTILITIES_UNDER_SITE can be dropped,
								referrring in this sproc only, third one is dbo.SR_RFP_FN_GET_ACCOUNT_UNDER_SITE
							For UTILITIES field, function returns text as "Multiple Utilities" if the site associated to more 
								than one utility, modified to return all utilities
							Replaced base tables with client hier tables
							
******/


CREATE PROCEDURE [dbo].[SR_RFP_GET_GROUP_LOAD_PROFILES_INFO_P]
      @userId VARCHAR
     ,@sessionId VARCHAR
     ,@rfpId INT
AS 
BEGIN
      SET NOCOUNT ON;
      
      WITH  Cte_Site_Dtls
              AS ( SELECT
                        ch.CLIENT_ID
                       ,ch.CLIENT_NAME
                       ,ch.SITE_ID
                       ,rtrim(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' AS SITE_NAME
                       ,cha.Account_Number AS ACCOUNT_NUMBER
                       ,cha.Account_Vendor_Name AS UTILITIES
                       ,approval.CBMS_IMAGE_ID
                       ,ci.CBMS_DOC_ID
                       ,cha.Alternate_Account_Number
                   FROM
                        Core.Client_Hier ch
                        INNER JOIN core.Client_Hier_Account cha
                              ON ch.Client_Hier_Id = cha.Client_Hier_Id
                        INNER JOIN dbo.SR_RFP_ACCOUNT rfpacc
                              ON rfpacc.account_id = cha.account_id
                                 AND rfpacc.is_deleted = 0
                        LEFT JOIN SR_RFP_LP_CLIENT_APPROVAL approval
                              ON approval.SR_RFP_ID = rfpacc.SR_RFP_ID
                                 AND approval.IS_GROUP_LP = 1
                                 AND approval.SITE_ID = ch.SITE_ID
                        LEFT JOIN dbo.cbms_image ci
                              ON approval.CBMS_IMAGE_ID = ci.CBMS_IMAGE_ID
                   WHERE
                        rfpacc.SR_RFP_ID = @rfpId
                   GROUP BY
                        ch.CLIENT_ID
                       ,ch.CLIENT_NAME
                       ,ch.SITE_ID
                       ,rtrim(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')'
                       ,cha.Account_Number
                       ,cha.Account_Vendor_Name
                       ,approval.CBMS_IMAGE_ID
                       ,ci.CBMS_DOC_ID
                       ,cha.Alternate_Account_Number)
            SELECT
                  csd.CLIENT_ID
                 ,csd.CLIENT_NAME
                 ,csd.SITE_ID
                 ,csd.SITE_NAME
                 ,replace(left(accnum.numbrs, len(accnum.numbrs) - 1), '&amp;', '&') AS ACCOUNT_NUMBER
                 ,replace(left(accvndr.vndrs, len(accvndr.vndrs) - 1), '&amp;', '&') AS UTILITIES
                 ,csd.CBMS_IMAGE_ID
                 ,csd.CBMS_DOC_ID
                 ,case WHEN len(altaccnum.altnumbrs) > 1 THEN replace(left(altaccnum.altnumbrs, len(altaccnum.altnumbrs) - 1), '&amp;', '&')
                       ELSE NULL
                  END AS Alternate_Account_Number
            FROM
                  Cte_Site_Dtls csd
                  CROSS APPLY ( SELECT
                                    nums.ACCOUNT_NUMBER + ','
                                FROM
                                    Cte_Site_Dtls nums
                                WHERE
                                    csd.Site_Id = nums.Site_Id
                                GROUP BY
                                    nums.ACCOUNT_NUMBER
                  FOR
                                XML PATH('') ) accnum ( numbrs )
                  CROSS APPLY ( SELECT
                                    vndr.UTILITIES + ','
                                FROM
                                    Cte_Site_Dtls vndr
                                WHERE
                                    csd.Site_Id = vndr.Site_Id
                                GROUP BY
                                    vndr.UTILITIES
                  FOR
                                XML PATH('') ) accvndr ( vndrs )
                  CROSS APPLY ( SELECT
                                    alt.Alternate_Account_Number + ','
                                FROM
                                    Cte_Site_Dtls alt
                                WHERE
                                    csd.Site_Id = alt.Site_Id
                                GROUP BY
                                    alt.Alternate_Account_Number
                  FOR
                                XML PATH('') ) altaccnum ( altnumbrs )
            GROUP BY
                  csd.CLIENT_ID
                 ,csd.CLIENT_NAME
                 ,csd.SITE_ID
                 ,csd.SITE_NAME
                 ,replace(left(accnum.numbrs, len(accnum.numbrs) - 1), '&amp;', '&')
                 ,replace(left(accvndr.vndrs, len(accvndr.vndrs) - 1), '&amp;', '&')
                 ,csd.CBMS_IMAGE_ID
                 ,csd.CBMS_DOC_ID
                 ,case WHEN len(altaccnum.altnumbrs) > 1 THEN replace(left(altaccnum.altnumbrs, len(altaccnum.altnumbrs) - 1), '&amp;', '&')
                  END
            ORDER BY
                  replace(left(accvndr.vndrs, len(accvndr.vndrs) - 1), '&amp;', '&')
END

GO

GRANT EXECUTE ON  [dbo].[SR_RFP_GET_GROUP_LOAD_PROFILES_INFO_P] TO [CBMSApplication]
GO
