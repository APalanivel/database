SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        Trade.Deal_Ticket_Manage_Trade_Volume_Status                    
                      
Description:                      
        
                      
Input Parameters:                      
    Name				DataType        Default     Description                        
--------------------------------------------------------------------------------
	@Group_Name			VARCHAR(100)
    @Client_Id			INT
    @RM_Group_Type_Cd	INT
    @User_Info_Id		INT   
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	
                    
Usage Examples:                          
--------------------------------------------------------------------------------
	
	    
                     
 Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy        
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			2018-11-16     Global Risk Management - Created                    
                     
******/
CREATE PROCEDURE [Trade].[Deal_Ticket_Manage_Trade_Volume_Status]
    (
        @Deal_Ticket_Id INT
        , @Trade_tvp_Deal_Ticket_Client_Hier_Volume AS Trade.tvp_Deal_Ticket_Client_Hier_Volume READONLY
        , @Uom_Id INT
        , @User_Info_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Workflow_Status_Map_Id INT;

        DECLARE @Tbl_Deal_Ticket_Client_Hier_Volume AS TABLE
              (
                  Client_Hier_Id INT NOT NULL
                  , Account_Id INT NOT NULL
                  , Deal_Month DATE NOT NULL
                  , Total_Volume DECIMAL(24, 2) NOT NULL
                  , Contract_Id INT NULL
              );

        INSERT INTO @Tbl_Deal_Ticket_Client_Hier_Volume
             (
                 Client_Hier_Id
                 , Account_Id
                 , Deal_Month
                 , Total_Volume
                 , Contract_Id
             )
        SELECT
            dtv.Client_Hier_Id
            , dtv.Account_Id
            , dtv.Deal_Month
            , dtv.Total_Volume AS Total_Volume
            , dtv.Contract_Id
        FROM
            @Trade_tvp_Deal_Ticket_Client_Hier_Volume dtv
            INNER JOIN meta.DATE_DIM dd
                ON dd.DATE_D = dtv.Deal_Month;

        SELECT
            @Workflow_Status_Map_Id = dtchws.Workflow_Status_Map_Id
        FROM
            Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtchvd
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dtch.Deal_Ticket_Id = dtchvd.Deal_Ticket_Id
                   AND  dtch.Client_Hier_Id = dtchvd.Client_Hier_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status dtchws
                ON dtchws.Deal_Ticket_Client_Hier_Id = dtch.Deal_Ticket_Client_Hier_Id
        WHERE
            dtchvd.Deal_Ticket_Id = @Deal_Ticket_Id
            AND dtchws.Is_Active = 1
            AND (   dtchws.Trade_Month IS NULL
                    OR  dtchvd.Deal_Month = dtchws.Trade_Month)
            AND (   dtchws.Contract_Id IS NULL
                    OR  dtchvd.Contract_Id = dtchws.Contract_Id)
            AND dtchvd.Trade_Number IS NOT NULL
            AND EXISTS (   SELECT
                                1
                           FROM
                                @Trade_tvp_Deal_Ticket_Client_Hier_Volume ttdtchv
                           WHERE
                                dtchvd.Deal_Month = ttdtchv.Deal_Month
                                AND dtchvd.Contract_Id = ttdtchv.Contract_Id);

        INSERT INTO Trade.Deal_Ticket_Client_Hier_Workflow_Status
             (
                 Deal_Ticket_Client_Hier_Id
                 , Workflow_Status_Map_Id
                 , Workflow_Status_Comment
                 , Is_Active
                 , Last_Notification_Sent_Ts
                 , Created_Ts
                 , Updated_User_Id
                 , Last_Change_Ts
                 , CBMS_Image_Id
                 , Trade_Month
                 , Workflow_Task_Id
                 , Contract_Id
             )
        SELECT
            dtch.Deal_Ticket_Client_Hier_Id
            , @Workflow_Status_Map_Id
            , NULL
            , 1
            , NULL
            , GETDATE()
            , @User_Info_Id
            , GETDATE()
            , NULL
            , NULL
            , NULL
            , NULL
        FROM
            Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtchvd
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtchvd2
                ON dtchvd2.Deal_Ticket_Id = dtchvd.Deal_Ticket_Id
                   AND  dtchvd2.Deal_Month = dtchvd.Deal_Month
                   AND  dtchvd2.Contract_Id = dtchvd.Contract_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dtch.Deal_Ticket_Id = dtchvd2.Deal_Ticket_Id
                   AND  dtch.Client_Hier_Id = dtchvd2.Client_Hier_Id
        WHERE
            dtchvd.Deal_Ticket_Id = @Deal_Ticket_Id
            AND dtchvd.Trade_Number IS NOT NULL
            AND dtchvd2.Trade_Number IS NULL
            AND EXISTS (   SELECT
                                1
                           FROM
                                @Trade_tvp_Deal_Ticket_Client_Hier_Volume ttdtchv
                           WHERE
                                dtchvd.Deal_Month = ttdtchv.Deal_Month
                                AND dtchvd.Contract_Id = ttdtchv.Contract_Id)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.Deal_Ticket_Client_Hier_Workflow_Status dtchws
                               WHERE
                                    dtchws.Deal_Ticket_Client_Hier_Id = dtch.Deal_Ticket_Client_Hier_Id
                                    AND dtchws.Workflow_Status_Map_Id = @Workflow_Status_Map_Id
                                    AND dtchws.Is_Active = 1);

        UPDATE
            dtchvd2
        SET
            dtchvd2.Trade_Number = dtchvd.Trade_Number
            , dtchvd2.Trade_Executed_Ts = dtchvd.Trade_Executed_Ts
            , dtchvd2.Trade_Price_Id = dtchvd.Trade_Price_Id
        FROM
            Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtchvd
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtchvd2
                ON dtchvd2.Deal_Ticket_Id = dtchvd.Deal_Ticket_Id
                   AND  dtchvd2.Deal_Month = dtchvd.Deal_Month
                   AND  dtchvd2.Contract_Id = dtchvd.Contract_Id
        WHERE
            dtchvd.Deal_Ticket_Id = @Deal_Ticket_Id
            AND dtchvd.Trade_Number IS NOT NULL
            AND dtchvd2.Trade_Number IS NULL
            AND EXISTS (   SELECT
                                1
                           FROM
                                @Trade_tvp_Deal_Ticket_Client_Hier_Volume ttdtchv
                           WHERE
                                dtchvd.Deal_Month = ttdtchv.Deal_Month
                                AND dtchvd.Contract_Id = ttdtchv.Contract_Id);



    END;
GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Manage_Trade_Volume_Status] TO [CBMSApplication]
GO
