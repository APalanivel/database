SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  

NAME: dbo.SR_SAVE_USER_P  
  
DESCRIPTION:   
  
INPUT PARAMETERS:      
	Name															DataType        Default     Description      
---------------------------------------------------------------------------------------------------------------------      
	@userId														VARCHAR(10)
	@vendorTypeId												INT
	@vendorId													INT
	@privilegeId												INT
	@userName													VARCHAR(50)
	@password													VARCHAR(50)
	@firstName													VARCHAR(50)
	@middleName													VARCHAR(50)
	@lastName													VARCHAR(50)
	@emailAddress												VARCHAR(50)
	@workPhoneNumber											VARCHAR(50)
	@cellPhoneNumber											VARCHAR(50)
	@faxNumber													VARCHAR(50)
	@isRfpPreferenceWebsite										BIT
	@isRfpPreferenceEmail										BIT
	@isemailsent												BIT 
    @Country_Id													INT
    @Locale_Code												VARCHAR(10)
    @Termination_Contact_Email_Address							NVARCHAR(MAX)
    @Is_Default_Termination_Contact_Email_For_Vendor			BIT
    @Is_Default_Termination_Contact_Email_Applies_To_Existing	BIT
    @Registration_Contact_Email_Address							NVARCHAR(MAX)
    @Is_Default_Registration_Contact_Email_For_Vendor			BIT
    @Is_Default_Registration_Contact_Email_Applies_To_Existing	BIT
    @Comment_Text												VARCHAR(MAX)
                             
OUTPUT PARAMETERS:           
      Name						DataType          Default     Description      
------------------------------------------------------------      
	  @srSupplierContactInfoId	INT				  OUT  
  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
 BEGIN TRAN test
  
		EXEC dbo.SR_SAVE_USER_P
				 @userId					= 49  
				,@vendorTypeId				= 288    
				,@vendorId					= 822    
				,@privilegeId				= 1165     
				,@userName					= 'dvsvtest'    
				,@password					= '098F6BCD4621D373CADE4E832627B4F6'    
				,@firstName					= 'test first'    
				,@middleName				= NULL    
				,@lastName					= 'test last'
				,@emailAddress				= 'test@Ctepl.com'
				,@workPhoneNumber			= '980-670-4567'    
				,@cellPhoneNumber			= '980-670-4568'    
				,@faxNumber					= '980-670-4569'    
				,@isRfpPreferenceWebsite	= 0    
				,@isRfpPreferenceEmail		= 1    
				,@isemailsent				= 0    
				,@srSupplierContactInfoId	= 3  

ROLLBACK TRAN test

	BEGIN TRAN
		DECLARE @srSupplierContactInfoId INT
		SELECT * FROM dbo.USER_INFO WHERE USERNAME='GlobalSourcing'
		EXEC dbo.SR_SAVE_USER_P @userId=49,@vendorTypeId=288,@vendorId=822,@privilegeId=1165,@userName= 'GlobalSourcing',@password= '098F6BCD46'    
					,@firstName= 'Global',@middleName= NULL,@lastName= 'Sourcing',@emailAddress= 'test@Ctepl.com'
					,@workPhoneNumber= '888-888-8888',@cellPhoneNumber= '888-888-8888' ,@faxNumber= '888-888-8888'    
					,@isRfpPreferenceWebsite=0,@isRfpPreferenceEmail= 1,@isemailsent=0,@srSupplierContactInfoId	= @srSupplierContactInfoId OUT
					,@Country_Id = 10,@Locale_Code ='It-IT'
					,@Termination_Contact_Email_Address='test@Ctepl.com',@Is_Default_Termination_Contact_Email_For_Vendor=0
					,@Registration_Contact_Email_Address='test@Ctepl.com',@Is_Default_Registration_Contact_Email_For_Vendor=1
					,@Comment_Text='GlobalSourcing-Test-Comment'
		SELECT * FROM dbo.USER_INFO WHERE USERNAME='GlobalSourcing'
		SELECT * FROM dbo.sr_supplier_contact_info ssci left JOIN dbo.Comment com ON com.Comment_ID = ssci.Comment_ID
			WHERE SR_SUPPLIER_CONTACT_INFO_ID = @srSupplierContactInfoId
		SELECT * FROM dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP WHERE SR_SUPPLIER_CONTACT_INFO_ID = @srSupplierContactInfoId
	ROLLBACK TRAN
	
		
	BEGIN TRAN
		DECLARE @srSupplierContactInfoId INT
		SELECT * FROM dbo.USER_INFO WHERE USERNAME='GlobalSourcingPhase2'
		EXEC dbo.SR_SAVE_USER_P 49,288,822,1165,'GlobalSourcingPhase2','098F6BCD46','Global',NULL,'Sourcing','test@Ctepl.com'
					,'888-888-8888','888-888-8888','888-888-8888',0,1,0,@srSupplierContactInfoId OUT, 10,'It-IT'
					,'test@Ctepl.com',1,0,'test@Ctepl.com',1,0,'GlobalSourcingPhase2-Test-Comment'
		SELECT * FROM dbo.SR_SUPPLIER_CONTACT_INFO ssci left JOIN dbo.Comment com ON com.Comment_ID = ssci.Comment_ID
				INNER JOIN dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP scvm ON ssci.SR_SUPPLIER_CONTACT_INFO_ID = scvm.SR_SUPPLIER_CONTACT_INFO_ID
				 WHERE scvm.SR_SUPPLIER_CONTACT_INFO_ID = @srSupplierContactInfoId
		SELECT DISTINCT Termination_Contact_Email_Address,Is_Default_Termination_Contact_Email_For_Vendor
					,Registration_Contact_Email_Address,Is_Default_Registration_Contact_Email_For_Vendor
                    FROM dbo.SR_SUPPLIER_CONTACT_INFO ssci INNER JOIN dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP scvm 
					ON ssci.SR_SUPPLIER_CONTACT_INFO_ID = scvm.SR_SUPPLIER_CONTACT_INFO_ID
				 WHERE scvm.VENDOR_ID = 822
	ROLLBACK TRAN
	
	BEGIN TRAN
		DECLARE @srSupplierContactInfoId INT
		SELECT * FROM dbo.USER_INFO WHERE USERNAME='GlobalSourcingPhase2'
		EXEC dbo.SR_SAVE_USER_P 49,288,822,1165,'GlobalSourcingPhase2','098F6BCD46','Global',NULL,'Sourcing','test@Ctepl.com'
					,'888-888-8888','888-888-8888','888-888-8888',0,1,0,@srSupplierContactInfoId OUT, 10,'It-IT'
					,'test123@Ctepl.com',1,0,'test123@Ctepl.com',1,1,'GlobalSourcingPhase2-Test-Comment'
		SELECT * FROM dbo.SR_SUPPLIER_CONTACT_INFO ssci left JOIN dbo.Comment com ON com.Comment_ID = ssci.Comment_ID
				INNER JOIN dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP scvm ON ssci.SR_SUPPLIER_CONTACT_INFO_ID = scvm.SR_SUPPLIER_CONTACT_INFO_ID
				 WHERE scvm.SR_SUPPLIER_CONTACT_INFO_ID = @srSupplierContactInfoId
		SELECT DISTINCT Termination_Contact_Email_Address,Is_Default_Termination_Contact_Email_For_Vendor
					,Registration_Contact_Email_Address,Is_Default_Registration_Contact_Email_For_Vendor
                    FROM dbo.SR_SUPPLIER_CONTACT_INFO ssci INNER JOIN dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP scvm 
					ON ssci.SR_SUPPLIER_CONTACT_INFO_ID = scvm.SR_SUPPLIER_CONTACT_INFO_ID
				 WHERE scvm.VENDOR_ID = 822
	ROLLBACK TRAN
  
AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
  DR		Deana Ritter
  PNR		Pandarinath  
  HG		Harihara Suthan G
  RR		Raghu Reddy

MODIFICATIONS  
  
	Initials	Date		Modification  
------------------------------------------------------------  
	DR			08/04/2009  Removed Linked Server Updates  
	DMR			09/10/2010	Modified for Quoted_Identifier.
	PNR			11/01/2010	Added Insert statement for User_passcode table.(Proj Name : SV_DV_Password_Expiration) 
	HG			11/09/2010	Transaction statements added we have multiple table inserts
							Unused parameter @sessionId removed and all other unused variables declared with in the procedure got removed.
	RR			2015-07-08	Global sourcing - Added new input parameters, saving into dbo.sr_supplier_contact_info new fields 
	RR			2015-10-19	Global Sourcing - Phase2 - Added new input parameters and modified script to mark default contacts and updating
							existing contcts based on new input values
	RR			2018-06-25	Global Risk Management - Added new input parameter @Contact_Type_Cd
******/
CREATE PROCEDURE [dbo].[SR_SAVE_USER_P]
    (
        @userId VARCHAR(10)
        , @vendorTypeId INT
        , @vendorId INT
        , @privilegeId INT
        , @userName VARCHAR(50)
        , @password NVARCHAR(128)
        , @firstName VARCHAR(50)
        , @middleName VARCHAR(50)
        , @lastName VARCHAR(50)
        , @emailAddress VARCHAR(50)
        , @workPhoneNumber VARCHAR(50)
        , @cellPhoneNumber VARCHAR(50)
        , @faxNumber VARCHAR(50)
        , @isRfpPreferenceWebsite BIT
        , @isRfpPreferenceEmail BIT
        , @isemailsent BIT
        , @srSupplierContactInfoId INT OUT
        , @Country_Id INT
        , @Locale_Code VARCHAR(10)
        , @Termination_Contact_Email_Address NVARCHAR(MAX)
        , @Is_Default_Termination_Contact_Email_For_Vendor BIT = 0
        , @Is_Default_Termination_Contact_Email_Applies_To_Existing BIT = 0
        , @Registration_Contact_Email_Address NVARCHAR(MAX)
        , @Is_Default_Registration_Contact_Email_For_Vendor BIT = 0
        , @Is_Default_Registration_Contact_Email_Applies_To_Existing BIT = 0
        , @Comment_Text VARCHAR(MAX)
        , @Contact_Type_Cd VARCHAR(MAX) = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Active_From_Dt DATE
            , @group_info_id INT;

        DECLARE @queueId INT;
        DECLARE @userInfoId INT;
        DECLARE @emaildate DATETIME;
        DECLARE
            @Comment_Id INT
            , @Comment_Type_CD INT
            , @Comment_Dt DATE = GETDATE();

        DECLARE
            @Old_Termination_Contact_Email_Address NVARCHAR(MAX) = ''
            , @Old_Registration_Contact_Email_Address NVARCHAR(MAX) = '';

        SET @emaildate = NULL;

        SELECT
            @Comment_Type_CD = cd.Code_Id
        FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'CommentType'
            AND cd.Code_Value = 'UtilityComment';

        SELECT
            @Old_Termination_Contact_Email_Address = ssci.Termination_Contact_Email_Address
        FROM
            dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP scvm
            INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                ON scvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
        WHERE
            scvm.VENDOR_ID = @vendorId
            AND ssci.Is_Default_Termination_Contact_Email_For_Vendor = 1;

        SELECT
            @Old_Registration_Contact_Email_Address = ssci.Registration_Contact_Email_Address
        FROM
            dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP scvm
            INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                ON scvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
        WHERE
            scvm.VENDOR_ID = @vendorId
            AND ssci.Is_Default_Registration_Contact_Email_For_Vendor = 1;

        BEGIN TRY

            BEGIN TRAN;

            INSERT INTO dbo.QUEUE
                 (
                     QUEUE_TYPE_ID
                 )
            SELECT
                ENTITY_ID
            FROM
                dbo.ENTITY
            WHERE
                ENTITY_DESCRIPTION = 'Queue'
                AND ENTITY_NAME = 'Private';

            SELECT  @queueId = SCOPE_IDENTITY();

            IF @isemailsent = 1
                BEGIN

                    SET @emaildate = GETDATE();

                END;

            INSERT INTO dbo.USER_INFO
                 (
                     USERNAME
                     , QUEUE_ID
                     , FIRST_NAME
                     , MIDDLE_NAME
                     , LAST_NAME
                     , EMAIL_ADDRESS
                     , IS_HISTORY
                     , country_id
                     , locale_code
                 )
            VALUES
                (@userName
                 , @queueId
                 , @firstName
                 , @middleName
                 , @lastName
                 , @emailAddress
                 , 0
                 , @Country_Id
                 , @Locale_Code);

            SELECT  @userInfoId = SCOPE_IDENTITY();

            SET @Active_From_Dt = '1/1/1900';

            EXEC dbo.User_PassCode_Ins @userInfoId, @password, @Active_From_Dt, @userId;

            EXEC dbo.Comment_Ins
                @Comment_Type_CD
                , @userId
                , @Comment_Dt
                , @Comment_Text
                , @Comment_Id OUT;

            INSERT INTO dbo.SR_SUPPLIER_CONTACT_INFO
                 (
                     USER_INFO_ID
                     , WORK_PHONE
                     , CELL_PHONE
                     , FAX
                     , VENDOR_TYPE_ID
                     , PRIVILEGE_TYPE_ID
                     , IS_RFP_PREFERENCE_WEBSITE
                     , IS_RFP_PREFERENCE_EMAIL
                     , EMAIL_SENT_DATE
                     , Termination_Contact_Email_Address
                     , Is_Default_Termination_Contact_Email_For_Vendor
                     , Registration_Contact_Email_Address
                     , Is_Default_Registration_Contact_Email_For_Vendor
                     , Comment_Id
                 )
            VALUES
                (@userInfoId
                 , @workPhoneNumber
                 , @cellPhoneNumber
                 , @faxNumber
                 , @vendorTypeId
                 , @privilegeId
                 , @isRfpPreferenceWebsite
                 , @isRfpPreferenceEmail
                 , @emaildate
                 , @Termination_Contact_Email_Address
                 , 0
                 , @Registration_Contact_Email_Address
                 , 0
                 , @Comment_Id);

            SELECT  @srSupplierContactInfoId = SCOPE_IDENTITY();

            INSERT INTO dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP
                 (
                     SR_SUPPLIER_CONTACT_INFO_ID
                     , VENDOR_ID
                     , IS_PRIMARY
                 )
            VALUES
                (@srSupplierContactInfoId
                 , @vendorId
                 , 1);

            SELECT
                @group_info_id = GROUP_INFO_ID
            FROM
                dbo.GROUP_INFO
            WHERE
                GROUP_NAME = 'supplier';

            INSERT INTO dbo.USER_INFO_GROUP_INFO_MAP
                 (
                     GROUP_INFO_ID
                     , USER_INFO_ID
                 )
            SELECT  @group_info_id, @userInfoId;

            --> Marking defalt Termination contact to false of old contact if new/current contact marked as default
            UPDATE
                ssci
            SET
                ssci.Is_Default_Termination_Contact_Email_For_Vendor = 0
            FROM
                dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP scvm
                INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                    ON scvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
            WHERE
                scvm.VENDOR_ID = @vendorId
                AND scvm.SR_SUPPLIER_CONTACT_INFO_ID <> @srSupplierContactInfoId
                AND @Is_Default_Termination_Contact_Email_For_Vendor = 1
                AND @Old_Termination_Contact_Email_Address <> @Termination_Contact_Email_Address;

            --> Marking defalt Registration contact to false of old contact if new/current contact marked as default      
            UPDATE
                ssci
            SET
                ssci.Is_Default_Registration_Contact_Email_For_Vendor = 0
            FROM
                dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP scvm
                INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                    ON scvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
            WHERE
                scvm.VENDOR_ID = @vendorId
                AND scvm.SR_SUPPLIER_CONTACT_INFO_ID <> @srSupplierContactInfoId
                AND @Is_Default_Registration_Contact_Email_For_Vendor = 1
                AND @Old_Registration_Contact_Email_Address <> @Registration_Contact_Email_Address;

            --> Marking defalt Termination contact to true of new contact if new/current contact marked as default
            UPDATE
                ssci
            SET
                ssci.Is_Default_Termination_Contact_Email_For_Vendor = 1
            FROM
                dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP scvm
                INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                    ON scvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
            WHERE
                scvm.VENDOR_ID = @vendorId
                AND scvm.SR_SUPPLIER_CONTACT_INFO_ID = @srSupplierContactInfoId
                AND @Is_Default_Termination_Contact_Email_For_Vendor = 1
                AND @Old_Termination_Contact_Email_Address <> @Termination_Contact_Email_Address;

            --> Marking defalt Registration contact to true of new contact if new/current contact marked as default      
            UPDATE
                ssci
            SET
                ssci.Is_Default_Registration_Contact_Email_For_Vendor = 1
            FROM
                dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP scvm
                INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                    ON scvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
            WHERE
                scvm.VENDOR_ID = @vendorId
                AND scvm.SR_SUPPLIER_CONTACT_INFO_ID = @srSupplierContactInfoId
                AND @Is_Default_Registration_Contact_Email_For_Vendor = 1
                AND @Old_Registration_Contact_Email_Address <> @Registration_Contact_Email_Address;

            --> Updating existing contacts Termination mail as new/current contact marked as default and applies to all    
            UPDATE
                ssci
            SET
                ssci.Termination_Contact_Email_Address = @Termination_Contact_Email_Address
            FROM
                dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP scvm
                INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                    ON scvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
            WHERE
                scvm.VENDOR_ID = @vendorId
                AND @Is_Default_Termination_Contact_Email_Applies_To_Existing = 1;

            --> Updating existing contacts Termination mail as new/current contact marked as default and applies to all    
            UPDATE
                ssci
            SET
                ssci.Registration_Contact_Email_Address = @Registration_Contact_Email_Address
            FROM
                dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP scvm
                INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                    ON scvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
            WHERE
                scvm.VENDOR_ID = @vendorId
                AND @Is_Default_Registration_Contact_Email_Applies_To_Existing = 1;



            INSERT INTO dbo.Sr_Supplier_Contact_Type_Map
                 (
                     SR_SUPPLIER_CONTACT_INFO_ID
                     , Contact_Type_Cd
                 )
            SELECT
                @srSupplierContactInfoId
                , ufn.Segments
            FROM
                dbo.ufn_split(@Contact_Type_Cd, ',') ufn
            WHERE
                NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Sr_Supplier_Contact_Type_Map srss
                               WHERE
                                    srss.SR_SUPPLIER_CONTACT_INFO_ID = @srSupplierContactInfoId
                                    AND ufn.Segments = srss.Contact_Type_Cd);


            COMMIT TRAN;
        END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRAN;
                END;
            EXEC dbo.usp_RethrowError;
        END CATCH;

    END;

    ;



GO


GRANT EXECUTE ON  [dbo].[SR_SAVE_USER_P] TO [CBMSApplication]
GO
