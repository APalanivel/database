SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[P2P_i_dboSR_RFP_PRICING_SCOPE01793653639]
		@c1 int,
		@c2 bit,
		@c3 varchar(1000),
		@c4 bit,
		@c5 varchar(1000),
		@c6 bit,
		@c7 varchar(1000),
		@c8 bit,
		@c9 varchar(1000),
		@c10 bit,
		@c11 varchar(1000),
		@c12 bit,
		@c13 varchar(1000),
		@c14 varchar(1000),
		@c15 bit,
		@c16 varchar(1000),
		@c17 int,
		@c18 int,
		@c19 int,
		@c20 int,
		@c21 int,
		@c22 int,
		@c23 int,
		@c24 uniqueidentifier
	,@MSp2pPostVersion varbinary(32) 
as
begin  
begin try
	insert into [dbo].[SR_RFP_PRICING_SCOPE](
		[SR_RFP_PRICING_SCOPE_ID],
		[IS_PRICE_INCLUDE_LOSES],
		[LOSS_PERCENT],
		[IS_PRICE_INCLUDE_ANCILIARY_SERVICE],
		[ANCILIARY_SERVICE_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_CAPACITY_CHARGES],
		[CAPACITY_CHARGES_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_ISO_CHARGES],
		[ISO_CHARGES_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_TRANSMISSION],
		[TRANSMISSION_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_TAXES],
		[TAXES_ESTIMATED_COST],
		[OTHER_CHARGES],
		[IS_DEMAND_CAP_APPLICABLE],
		[CAP_FLOOR_PRICE],
		[PRICE_INCLUDE_LOSES_TYPE_ID],
		[PRICE_INCLUDE_ANCILIARY_SERVICE_TYPE_ID],
		[PRICE_INCLUDE_CAPACITY_CHARGES_TYPE_ID],
		[PRICE_INCLUDE_ISO_CHARGES_TYPE_ID],
		[PRICE_INCLUDE_TRANSMISSION_TYPE_ID],
		[PRICE_INCLUDE_TAXES_TYPE_ID],
		[DEMAND_CAP_APPLICABLE_TYPE_ID],
		[msrepl_tran_version],
		$sys_p2p_cd_id
	) values (
    @c1,
    @c2,
    @c3,
    @c4,
    @c5,
    @c6,
    @c7,
    @c8,
    @c9,
    @c10,
    @c11,
    @c12,
    @c13,
    @c14,
    @c15,
    @c16,
    @c17,
    @c18,
    @c19,
    @c20,
    @c21,
    @c22,
    @c23,
    @c24,
		@MSp2pPostVersion	) 
end try
begin catch
if @@error in (2627, 2601) 
begin  
	declare @cur_version varbinary(32) 
		,@conflict_type int = 5
		,@conflict_type_txt nvarchar(20) = N'Insert-Insert'
		,@reason_code int = 1
		,@reason_text nvarchar(720) = NULL
		,@is_on_disk_winner bit = 0
		,@is_incoming_winner bit = 0
		,@peer_id_current_node int = NULL
		,@peer_id_incoming int
		,@tranid_incoming nvarchar(40)
		,@peer_id_on_disk int
		,@tranid_on_disk nvarchar(40)
	select @peer_id_incoming = sys.fn_replvarbintoint(@MSp2pPostVersion)
		,@tranid_incoming = sys.fn_replp2pversiontotranid(@MSp2pPostVersion)
	select @cur_version = $sys_p2p_cd_id from [dbo].[SR_RFP_PRICING_SCOPE] 
where [SR_RFP_PRICING_SCOPE_ID] = @c1
	if @@rowcount = 0  
			exec sys.sp_replrethrow
	else 
	begin  
		select @peer_id_on_disk = sys.fn_replvarbintoint(@cur_version)
			,@tranid_on_disk = sys.fn_replp2pversiontotranid(@cur_version)
		if(@peer_id_incoming > @peer_id_on_disk)
			set @is_incoming_winner = 1
		else
			set @is_on_disk_winner = 1
	end  
		select @peer_id_current_node = p.originator_id from syspublications p join sysarticles a on a.pubid = p.pubid where a.objid = object_id(N'[dbo].[SR_RFP_PRICING_SCOPE]') and p.options & 0x1 = 0x1
	if (@peer_id_current_node is not null) 
	begin 
		if (@reason_text is NULL)
			if (@peer_id_incoming > @peer_id_on_disk)
			begin  
				select @reason_text = formatmessage(22825,@peer_id_incoming,@peer_id_on_disk,@peer_id_current_node)
			end  
			else  
			begin  
				select @reason_text = formatmessage(22824,@peer_id_incoming,@peer_id_on_disk,@peer_id_current_node)
			end  
		create table #change_id (change_id varbinary(8))
		insert [dbo].[conflict_dbo_SR_RFP_PRICING_SCOPE] (
		[SR_RFP_PRICING_SCOPE_ID],
		[IS_PRICE_INCLUDE_LOSES],
		[LOSS_PERCENT],
		[IS_PRICE_INCLUDE_ANCILIARY_SERVICE],
		[ANCILIARY_SERVICE_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_CAPACITY_CHARGES],
		[CAPACITY_CHARGES_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_ISO_CHARGES],
		[ISO_CHARGES_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_TRANSMISSION],
		[TRANSMISSION_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_TAXES],
		[TAXES_ESTIMATED_COST],
		[OTHER_CHARGES],
		[IS_DEMAND_CAP_APPLICABLE],
		[CAP_FLOOR_PRICE],
		[PRICE_INCLUDE_LOSES_TYPE_ID],
		[PRICE_INCLUDE_ANCILIARY_SERVICE_TYPE_ID],
		[PRICE_INCLUDE_CAPACITY_CHARGES_TYPE_ID],
		[PRICE_INCLUDE_ISO_CHARGES_TYPE_ID],
		[PRICE_INCLUDE_TRANSMISSION_TYPE_ID],
		[PRICE_INCLUDE_TAXES_TYPE_ID],
		[DEMAND_CAP_APPLICABLE_TYPE_ID],
		[msrepl_tran_version]
			,__$originator_id
			,__$origin_datasource
			,__$tranid
			,__$conflict_type
			,__$is_winner
			,__$reason_code
			,__$reason_text
			,__$update_bitmap
			,__$pre_version)
		output inserted.__$row_id into #change_id
		select 
    @c1,
    @c2,
    @c3,
    @c4,
    @c5,
    @c6,
    @c7,
    @c8,
    @c9,
    @c10,
    @c11,
    @c12,
    @c13,
    @c14,
    @c15,
    @c16,
    @c17,
    @c18,
    @c19,
    @c20,
    @c21,
    @c22,
    @c23,
    @c24			,@peer_id_current_node
			,@peer_id_incoming
			,@tranid_incoming
			,@conflict_type
			,@is_incoming_winner
			,@reason_code
			,@reason_text
			,NULL
			,NULL
		insert [dbo].[conflict_dbo_SR_RFP_PRICING_SCOPE] (

		[SR_RFP_PRICING_SCOPE_ID],
		[IS_PRICE_INCLUDE_LOSES],
		[LOSS_PERCENT],
		[IS_PRICE_INCLUDE_ANCILIARY_SERVICE],
		[ANCILIARY_SERVICE_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_CAPACITY_CHARGES],
		[CAPACITY_CHARGES_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_ISO_CHARGES],
		[ISO_CHARGES_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_TRANSMISSION],
		[TRANSMISSION_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_TAXES],
		[TAXES_ESTIMATED_COST],
		[OTHER_CHARGES],
		[IS_DEMAND_CAP_APPLICABLE],
		[CAP_FLOOR_PRICE],
		[PRICE_INCLUDE_LOSES_TYPE_ID],
		[PRICE_INCLUDE_ANCILIARY_SERVICE_TYPE_ID],
		[PRICE_INCLUDE_CAPACITY_CHARGES_TYPE_ID],
		[PRICE_INCLUDE_ISO_CHARGES_TYPE_ID],
		[PRICE_INCLUDE_TRANSMISSION_TYPE_ID],
		[PRICE_INCLUDE_TAXES_TYPE_ID],
		[DEMAND_CAP_APPLICABLE_TYPE_ID],
		[msrepl_tran_version]			,__$originator_id
			,__$origin_datasource
			,__$tranid
			,__$conflict_type
			,__$is_winner
			,__$reason_code
			,__$reason_text
			,__$pre_version
			,__$change_id)
		select 

		[SR_RFP_PRICING_SCOPE_ID],
		[IS_PRICE_INCLUDE_LOSES],
		[LOSS_PERCENT],
		[IS_PRICE_INCLUDE_ANCILIARY_SERVICE],
		[ANCILIARY_SERVICE_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_CAPACITY_CHARGES],
		[CAPACITY_CHARGES_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_ISO_CHARGES],
		[ISO_CHARGES_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_TRANSMISSION],
		[TRANSMISSION_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_TAXES],
		[TAXES_ESTIMATED_COST],
		[OTHER_CHARGES],
		[IS_DEMAND_CAP_APPLICABLE],
		[CAP_FLOOR_PRICE],
		[PRICE_INCLUDE_LOSES_TYPE_ID],
		[PRICE_INCLUDE_ANCILIARY_SERVICE_TYPE_ID],
		[PRICE_INCLUDE_CAPACITY_CHARGES_TYPE_ID],
		[PRICE_INCLUDE_ISO_CHARGES_TYPE_ID],
		[PRICE_INCLUDE_TRANSMISSION_TYPE_ID],
		[PRICE_INCLUDE_TAXES_TYPE_ID],
		[DEMAND_CAP_APPLICABLE_TYPE_ID],
		[msrepl_tran_version]			,@peer_id_current_node
			,@peer_id_on_disk
			,@tranid_on_disk
			,@conflict_type
			,@is_on_disk_winner
			,@reason_code
			,@reason_text
			,NULL
			,(select change_id from #change_id)
		from [dbo].[SR_RFP_PRICING_SCOPE] 

where [SR_RFP_PRICING_SCOPE_ID] = @c1
	end 
	if(@peer_id_incoming > @peer_id_on_disk)
	begin  
update [dbo].[SR_RFP_PRICING_SCOPE] set
		[IS_PRICE_INCLUDE_LOSES] = @c2,
		[LOSS_PERCENT] = @c3,
		[IS_PRICE_INCLUDE_ANCILIARY_SERVICE] = @c4,
		[ANCILIARY_SERVICE_ESTIMATED_COST] = @c5,
		[IS_PRICE_INCLUDE_CAPACITY_CHARGES] = @c6,
		[CAPACITY_CHARGES_ESTIMATED_COST] = @c7,
		[IS_PRICE_INCLUDE_ISO_CHARGES] = @c8,
		[ISO_CHARGES_ESTIMATED_COST] = @c9,
		[IS_PRICE_INCLUDE_TRANSMISSION] = @c10,
		[TRANSMISSION_ESTIMATED_COST] = @c11,
		[IS_PRICE_INCLUDE_TAXES] = @c12,
		[TAXES_ESTIMATED_COST] = @c13,
		[OTHER_CHARGES] = @c14,
		[IS_DEMAND_CAP_APPLICABLE] = @c15,
		[CAP_FLOOR_PRICE] = @c16,
		[PRICE_INCLUDE_LOSES_TYPE_ID] = @c17,
		[PRICE_INCLUDE_ANCILIARY_SERVICE_TYPE_ID] = @c18,
		[PRICE_INCLUDE_CAPACITY_CHARGES_TYPE_ID] = @c19,
		[PRICE_INCLUDE_ISO_CHARGES_TYPE_ID] = @c20,
		[PRICE_INCLUDE_TRANSMISSION_TYPE_ID] = @c21,
		[PRICE_INCLUDE_TAXES_TYPE_ID] = @c22,
		[DEMAND_CAP_APPLICABLE_TYPE_ID] = @c23,
		[msrepl_tran_version] = @c24		,$sys_p2p_cd_id = @MSp2pPostVersion

where [SR_RFP_PRICING_SCOPE_ID] = @c1
	end  
		if exists(select * from syspublications p join sysarticles a on a.pubid = p.pubid where a.objid = object_id(N'[dbo].[SR_RFP_PRICING_SCOPE]') and p.options & 0x10 = 0x10)
			raiserror(22815, 10, -1, @conflict_type_txt, @peer_id_current_node, @peer_id_incoming, @tranid_incoming, @peer_id_on_disk, @tranid_on_disk) with log
		else
			raiserror(22815, 16, -1, @conflict_type_txt, @peer_id_current_node, @peer_id_incoming, @tranid_incoming, @peer_id_on_disk, @tranid_on_disk) with log
end 
else  
	EXEC sys.sp_replrethrow
end catch 
end  
GO
