SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UBM_CASS_INSERT_MISSING_IMAGES_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@masterLogId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--select * from entity where entity_type = 656 

CREATE       PROCEDURE [dbo].[UBM_CASS_INSERT_MISSING_IMAGES_P] 
@masterLogId int

	AS
return
/*
	set nocount on
	DECLARE 
		@ubm_batch_master_log_id int, 
		@cbms_image_id int,
		@invoice_identifier varchar(50),
		@ubm_invoice_id int,
		@bar_code varchar(200)


		--select ubm_id from ubm_batch_master_log where ubm_batch_master_log_id=@masterLogId

DECLARE C_MISSING_IMAGE_INV CURSOR FAST_FORWARD

FOR

	select ubm_batch_master_log_id from ubm_batch_master_log

	where 
		--ubm_id = (select ubm_id from ubm where ubm_name='Cass')
		ubm_id=(select ubm_id from ubm_batch_master_log where ubm_batch_master_log_id=@masterLogId)
		and status_type_id in (884,889)
--removed and hardcoded values MM
--other option would be to store in variables and call once from entity prior to entering cursor
-- 	(select entity_id from entity 
-- 				where entity_type = 656 
-- 					and (entity_name='Missing Images' or entity_name = 'Orphan Barcodes'))

--added to exclude useless crap, needs to be further refined
		and start_date>=getdate()-730

	OPEN C_MISSING_IMAGE_INV
	
	FETCH NEXT FROM C_MISSING_IMAGE_INV INTO @ubm_batch_master_log_id
--print '@ubm_batch_master_log_id =  '+str(@ubm_batch_master_log_id)
	WHILE @@fetch_status =0
	BEGIN
		DECLARE C_MISSING_IMAGE_INV_DETAILS CURSOR FAST_FORWARD
		FOR
			select ubm_invoice_id,
			       invoice_identifier 	
			from
				ubm_invoice
				 
			where ubm_batch_master_log_id = @ubm_batch_master_log_id
			      and cbms_image_id is null	
	
		OPEN C_MISSING_IMAGE_INV_DETAILS
	
			FETCH NEXT FROM C_MISSING_IMAGE_INV_DETAILS INTO 
			@ubm_invoice_id, @invoice_identifier 
		 

			WHILE @@fetch_status =0
			BEGIN

				select @bar_code = bar_code from ubm_cass_bill(nolock)
                                where util_bill_header_id = @invoice_identifier 
				      and ubm_batch_master_log_id = @ubm_batch_master_log_id
				--print '@bar_code =  '+@bar_code

				select @cbms_image_id = MIN(cbms_image_id) from cbms_image(nolock)
				where cbms_doc_id in (@bar_code +'.tif', @bar_code +'.pdf', @bar_code +'.html', @bar_code +'.htm',@bar_code+'.jpg')
-- 				print '@cbms_image_id =  '+str(@cbms_image_id)

				if @cbms_image_id is not null
				begin

					update ubm_invoice set cbms_image_id = @cbms_image_id
					where   ubm_invoice_id =  @ubm_invoice_id 
				end
				
				FETCH NEXT FROM C_MISSING_IMAGE_INV_DETAILS INTO 
				@ubm_invoice_id, @invoice_identifier 
			END
		CLOSE C_MISSING_IMAGE_INV_DETAILS
		DEALLOCATE C_MISSING_IMAGE_INV_DETAILS

		IF (select count(ubm_invoice_id) 	
		   from ubm_invoice
		   where ubm_batch_master_log_id = @ubm_batch_master_log_id
			and cbms_image_id is null) = 0
		BEGIN
			--print 'Status is successful '	
			update ubm_batch_master_log set status_type_id = (select entity_id from entity 
				where entity_type = 656 
					and entity_name='Success')
			where ubm_batch_master_log_id = @ubm_batch_master_log_id

		END

	
	FETCH NEXT FROM C_MISSING_IMAGE_INV INTO @ubm_batch_master_log_id
	end
CLOSE C_MISSING_IMAGE_INV
DEALLOCATE C_MISSING_IMAGE_INV

*/
GO
GRANT EXECUTE ON  [dbo].[UBM_CASS_INSERT_MISSING_IMAGES_P] TO [CBMSApplication]
GO
