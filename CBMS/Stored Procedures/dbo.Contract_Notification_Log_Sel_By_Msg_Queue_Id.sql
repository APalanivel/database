SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** 

NAME:
		dbo.Contract_Notification_Log_Sel_By_Msg_Queue_Id 

DESCRIPTION: 
				To get the registration notifications log

INPUT PARAMETERS: 
Name						DataType		Default		Description 
----------------------------------------------------------------------
@Date						DATETIME
@Start_Index				INT
@End_Index					INT
@Total_Count				INT


OUTPUT PARAMETERS:   
Name						DataType	Default		Description 
---------------------------------------------------------------------- 

USAGE EXAMPLES:   
----------------------------------------------------------------------   

SELECT TOP 10  nmq.Notification_Msg_Queue_Id,flv.Contract_Id,flv.Meter_Start_End_Date,flv.Last_Change_Ts 
	FROM dbo.Contract_Notification_Log flv INNER JOIN dbo.Notification_Msg_Queue nmq 
			ON flv.Notification_Msg_Queue_Id = nmq.Notification_Msg_Queue_Id
	INNER JOIN dbo.Notification_Template nt ON nmq.Notification_Template_Id = nt.Notification_Template_Id
	INNER JOIN dbo.Code cd ON cd.Code_Id = nt.Notification_Type_Cd
    INNER JOIN dbo.Codeset cs ON cd.Codeset_Id = cd.Codeset_Id
WHERE cd.Code_Value IN ( 'Termination Analyst', 'Termination Vendor' ) AND cs.Codeset_Name = 'Notification Type'
	ORDER BY flv.Contract_Id,flv.Meter_Start_End_Date,flv.Last_Change_Ts

EXEC dbo.Contract_Notification_Log_Sel_By_Msg_Queue_Id 8139
EXEC dbo.Contract_Notification_Log_Sel_By_Msg_Queue_Id 8190
EXEC dbo.Contract_Notification_Log_Sel_By_Msg_Queue_Id 8140
EXEC dbo.Contract_Notification_Log_Sel_By_Msg_Queue_Id 8208


SELECT TOP 10  nmq.Notification_Msg_Queue_Id,flv.Contract_Id,flv.Meter_Start_End_Date,flv.Last_Change_Ts 
	FROM dbo.Contract_Notification_Log flv INNER JOIN dbo.Notification_Msg_Queue nmq 
			ON flv.Notification_Msg_Queue_Id = nmq.Notification_Msg_Queue_Id
	INNER JOIN dbo.Notification_Template nt ON nmq.Notification_Template_Id = nt.Notification_Template_Id
	INNER JOIN dbo.Code cd ON cd.Code_Id = nt.Notification_Type_Cd
    INNER JOIN dbo.Codeset cs ON cd.Codeset_Id = cd.Codeset_Id
WHERE cd.Code_Value IN ( 'Registration Notification' ) AND cs.Codeset_Name = 'Notification Type'
	ORDER BY flv.Contract_Id,flv.Meter_Start_End_Date,flv.Last_Change_Ts

EXEC dbo.Contract_Notification_Log_Sel_By_Msg_Queue_Id 8201
EXEC dbo.Contract_Notification_Log_Sel_By_Msg_Queue_Id 8149
EXEC dbo.Contract_Notification_Log_Sel_By_Msg_Queue_Id 8209
EXEC dbo.Contract_Notification_Log_Sel_By_Msg_Queue_Id 8179
EXEC dbo.Contract_Notification_Log_Sel_By_Msg_Queue_Id 8333



AUTHOR INITIALS:   
	Initials	Name   
----------------------------------------------------------------------   
	RR			Raghu Reddy


MODIFICATIONS:  
	Initials	Date		Modification 
-------------------------------------------------------------------------------   
	RR			2016-06-03  Created  GCS-988 GCS-Phase-5
	 	 		 
******/
CREATE PROCEDURE [dbo].[Contract_Notification_Log_Sel_By_Msg_Queue_Id]
      ( 
       @Notification_Msg_Queue_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
 
      DECLARE @Flow_Verification VARCHAR(25);
      
      SELECT
            @Flow_Verification = Code_Value
      FROM
            dbo.Notification_Msg_Queue nmq
            INNER JOIN dbo.Notification_Template nt
                  ON nmq.Notification_Template_Id = nt.Notification_Template_Id
            INNER JOIN dbo.Code cd
                  ON cd.Code_Id = nt.Notification_Type_Cd
            INNER JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cd.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Notification Type'
            AND nmq.Notification_Msg_Queue_Id = @Notification_Msg_Queue_Id;
		
      SELECT
            flv.Notification_Msg_Queue_Id
           ,flv.Last_Change_Ts
      FROM
            dbo.Contract_Notification_Log flv
            INNER JOIN dbo.Notification_Msg_Queue nmq
                  ON flv.Notification_Msg_Queue_Id = nmq.Notification_Msg_Queue_Id
            INNER JOIN dbo.Notification_Template nt
                  ON nmq.Notification_Template_Id = nt.Notification_Template_Id
            INNER JOIN dbo.Code cd
                  ON cd.Code_Id = nt.Notification_Type_Cd
            INNER JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cd.Codeset_Id
      WHERE
            ( ( @Flow_Verification = 'Registration Notification'
                AND cd.Code_Value = 'Registration Notification' )
              OR ( @Flow_Verification = 'Termination Vendor'
                   AND cd.Code_Value IN ( 'Termination Analyst', 'Termination Vendor' ) )
              OR ( @Flow_Verification = 'Termination Analyst'
                   AND cd.Code_Value IN ( 'Termination Analyst', 'Termination Vendor' ) ) )
            AND cs.Codeset_Name = 'Notification Type'
            AND EXISTS ( SELECT
                              1
                         FROM
                              dbo.Contract_Notification_Log flvlog
                         WHERE
                              flv.Contract_Id = flvlog.Contract_Id
                              AND flv.Meter_Term_Dt = flvlog.Meter_Term_Dt
                              AND flvlog.Notification_Msg_Queue_Id = @Notification_Msg_Queue_Id )
      GROUP BY
            flv.Notification_Msg_Queue_Id
           ,flv.Last_Change_Ts
      ORDER BY
            flv.Last_Change_Ts DESC;
  
END;



;
GO
GRANT EXECUTE ON  [dbo].[Contract_Notification_Log_Sel_By_Msg_Queue_Id] TO [CBMSApplication]
GO
