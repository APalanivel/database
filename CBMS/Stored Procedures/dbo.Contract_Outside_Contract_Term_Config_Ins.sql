SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Contract_Outside_Contract_Term_Config_Ins          
              
Description:              
        To insert Data into  table.              
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
     @Contract_Id						INT
     @Commodity_Id						INT
     @Config_Start_Dt							DATE
     @Config_End_Dt							DATE				NULL
     @Invoice_Recalc_Type_Cd			INT
     @User_Info_Id						INT    
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                
	
	BEGIN TRAN  

	SELECT * FROM Contract_Outside_Contract_Term_Config WHERE Contract_Id =1148520
    EXEC dbo.Contract_Outside_Contract_Term_Config_Ins 
      @Contract_Id =1148520
     ,@OCT_Parameter_Cd = 1
	 ,@OCT_Tolerance_Date_Cd = 1
	 ,@OCT_Dt_Range_Cd = 1
     ,@Config_Start_Dt = '2015-01-01'
     ,@Config_End_Dt= NULL   
     ,@User_Info_Id = 49
    
	SELECT * FROM Contract_Outside_Contract_Term_Config WHERE Contract_Id =1148520

	ROLLBACK TRAN             
             
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2019-06-26		Created For Add contract.   
             
******/
CREATE PROCEDURE [dbo].[Contract_Outside_Contract_Term_Config_Ins]
    (
        @Contract_Id INT
        , @Valcon_Account_Config_Type_Cd INT
        , @OCT_Parameter_Cd INT
        , @OCT_Tolerance_Date_Cd INT
        , @OCT_Dt_Range_Cd INT
        , @Config_Start_Dt DATE
        , @Config_End_Dt DATE = NULL
        , @User_Info_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Valcon_Contract_Config_Id INT;

        SELECT
            @Valcon_Contract_Config_Id = vcc.Valcon_Contract_Config_Id
        FROM
            dbo.Valcon_Contract_Config vcc
        WHERE
            vcc.Contract_Id = @Contract_Id
            AND vcc.Valcon_Account_Config_Type_Cd = @Valcon_Account_Config_Type_Cd;


        INSERT INTO dbo.Contract_Outside_Contract_Term_Config
             (
                 Valcon_Contract_Config_Id
                 , OCT_Parameter_Cd
                 , OCT_Tolerance_Date_Cd
                 , OCT_Dt_Range_Cd
                 , Config_Start_Dt
                 , Config_End_Dt
                 , Created_Ts
                 , Created_User_Id
                 , Updated_User_Id
                 , Last_Change_Ts
             )
        SELECT
            @Valcon_Contract_Config_Id
            , @OCT_Parameter_Cd
            , @OCT_Tolerance_Date_Cd
            , @OCT_Dt_Range_Cd
            , @Config_Start_Dt
            , @Config_End_Dt
            , GETDATE()
            , @User_Info_Id
            , @User_Info_Id
            , GETDATE()
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                dbo.Contract_Outside_Contract_Term_Config cocc
                           WHERE
                                cocc.Valcon_Contract_Config_Id = @Valcon_Contract_Config_Id
                                AND cocc.OCT_Parameter_Cd = @OCT_Parameter_Cd);


    END;


GO
GRANT EXECUTE ON  [dbo].[Contract_Outside_Contract_Term_Config_Ins] TO [CBMSApplication]
GO
