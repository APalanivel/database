
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_SOP_SHORTLIST_PRODUCT_TERMS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name					DataType	Default		Description
----------------------------------------------------------------
	@rfp_account_group_id	int       	          	
	@is_bid_group  			bit       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
----------------------------------------------------------------

USAGE EXAMPLES:
----------------------------------------------------------------

	EXEC dbo.SR_RFP_GET_SOP_SHORTLIST_PRODUCT_TERMS_P 12093844,0
	EXEC dbo.SR_RFP_GET_SOP_SHORTLIST_PRODUCT_TERMS_P 12093844,1
	EXEC dbo.SR_RFP_GET_SOP_SHORTLIST_PRODUCT_TERMS_P 12100468,0
	EXEC dbo.SR_RFP_GET_SOP_SHORTLIST_PRODUCT_TERMS_P 12100468,1
	EXEC dbo.SR_RFP_GET_SOP_SHORTLIST_PRODUCT_TERMS_P 12101775,0
	EXEC dbo.SR_RFP_GET_SOP_SHORTLIST_PRODUCT_TERMS_P 10010866,1
	EXEC dbo.SR_RFP_GET_SOP_SHORTLIST_PRODUCT_TERMS_P 10010906,1
	EXEC dbo.SR_RFP_GET_SOP_SHORTLIST_PRODUCT_TERMS_P 10011026,1

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2016-04-22	GCS-797 Added NO_OF_MONTHS to select list

******/

CREATE  PROCEDURE [dbo].[SR_RFP_GET_SOP_SHORTLIST_PRODUCT_TERMS_P]
      ( 
       @rfp_account_group_id INT
      ,@is_bid_group BIT )
AS 
BEGIN
      SET NOCOUNT ON



      SELECT
            ssci.SR_SUPPLIER_CONTACT_INFO_ID AS contactId
           ,spp.SR_PRICING_PRODUCT_ID AS productId
           ,spp.PRODUCT_NAME AS productName
           ,term.SR_RFP_ACCOUNT_TERM_ID AS accountTermId
           ,term.FROM_MONTH AS fromMonth
           ,term.TO_MONTH AS toMonth
           ,term.NO_OF_MONTHS AS NO_OF_MONTHS
      FROM
            dbo.SR_RFP_ACCOUNT_TERM term
            INNER JOIN dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                  ON term.SR_RFP_ACCOUNT_TERM_ID = tpm.SR_RFP_ACCOUNT_TERM_ID
            INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                  ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
            INNER JOIN dbo.SR_PRICING_PRODUCT spp
                  ON srsp.PRICING_PRODUCT_ID = spp.SR_PRICING_PRODUCT_ID
            INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                  ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
            INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                  ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
            INNER JOIN dbo.SR_RFP_SOP_SHORTLIST_DETAILS srssd
                  ON cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = srssd.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                     AND srsp.SR_RFP_SELECTED_PRODUCTS_ID = srssd.SR_RFP_SELECTED_PRODUCTS_ID
                     AND tpm.SR_RFP_ACCOUNT_TERM_ID = srssd.SR_RFP_ACCOUNT_TERM_ID
            INNER JOIN dbo.SR_RFP_SOP_SHORTLIST srss
                  ON srssd.SR_RFP_SOP_SHORTLIST_ID = srss.SR_RFP_SOP_SHORTLIST_ID
                     AND term.SR_ACCOUNT_GROUP_ID = srss.SR_ACCOUNT_GROUP_ID
                     AND term.IS_BID_GROUP = srss.IS_BID_GROUP
      WHERE
            srss.SR_ACCOUNT_GROUP_ID = @rfp_account_group_id
            AND srss.IS_BID_GROUP = @is_bid_group
            AND tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID IS NOT NULL
            AND srsp.IS_SYSTEM_PRODUCT = 1
      UNION
      SELECT
            ssci.SR_SUPPLIER_CONTACT_INFO_ID AS contactId
           ,srprp.SR_RFP_PRICING_PRODUCT_ID AS productId
           ,srprp.PRODUCT_NAME AS productName
           ,term.SR_RFP_ACCOUNT_TERM_ID AS accountTermId
           ,term.FROM_MONTH AS fromMonth
           ,term.TO_MONTH AS toMonth
           ,term.NO_OF_MONTHS AS NO_OF_MONTHS
      FROM
            dbo.SR_RFP_ACCOUNT_TERM term
            INNER JOIN dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                  ON term.SR_RFP_ACCOUNT_TERM_ID = tpm.SR_RFP_ACCOUNT_TERM_ID
            INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                  ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
            INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srprp
                  ON srsp.PRICING_PRODUCT_ID = srprp.SR_RFP_PRICING_PRODUCT_ID
            INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                  ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
            INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                  ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
            INNER JOIN dbo.SR_RFP_SOP_SHORTLIST_DETAILS srssd
                  ON cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = srssd.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                     AND srsp.SR_RFP_SELECTED_PRODUCTS_ID = srssd.SR_RFP_SELECTED_PRODUCTS_ID
                     AND tpm.SR_RFP_ACCOUNT_TERM_ID = srssd.SR_RFP_ACCOUNT_TERM_ID
            INNER JOIN dbo.SR_RFP_SOP_SHORTLIST srss
                  ON term.SR_ACCOUNT_GROUP_ID = srss.SR_ACCOUNT_GROUP_ID
                     AND term.IS_BID_GROUP = srss.IS_BID_GROUP
                     AND srssd.SR_RFP_SOP_SHORTLIST_ID = srss.SR_RFP_SOP_SHORTLIST_ID
      WHERE
            srss.SR_ACCOUNT_GROUP_ID = @rfp_account_group_id
            AND srss.IS_BID_GROUP = @is_bid_group
            AND tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID IS NOT NULL
            AND srsp.IS_SYSTEM_PRODUCT IS NULL
             
             
END 

;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_GET_SOP_SHORTLIST_PRODUCT_TERMS_P] TO [CBMSApplication]
GO
