SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_RISK_DOCUMENT_IMAGE_ID_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@documentLevelId	int       	          	
	@documentTypeId	int       	          	
	@siteOrDivisionId	int       	          	
	@clientId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE dbo.GET_RISK_DOCUMENT_IMAGE_ID_P
@userId varchar(10),
@sessionId varchar(20),
@documentLevelId int,
@documentTypeId int,
@siteOrDivisionId int,
@clientId int
AS
set nocount on
declare @entityName varchar(200)

select @entityName = entity_name from entity where entity_id = @documentLevelId

if(@entityName = 'Site')

begin

select cbms_image_id from RM_RISK_PLAN_PROFILE where document_type_id = @documentTypeId and
site_id = @siteOrDivisionId and client_id = @clientId and division_id is null 

end

else if (@entityName = 'Division')

begin

select cbms_image_id from RM_RISK_PLAN_PROFILE where document_type_id = @documentTypeId and
division_id = @siteOrDivisionId and client_id = @clientId and site_id is null

end

else if (@entityName = 'Corporate')

begin

select cbms_image_id from RM_RISK_PLAN_PROFILE
	 where document_type_id = @documentTypeId 
		and client_id = @clientId
		and site_id is null
		and division_id is null 

end
GO
GRANT EXECUTE ON  [dbo].[GET_RISK_DOCUMENT_IMAGE_ID_P] TO [CBMSApplication]
GO
