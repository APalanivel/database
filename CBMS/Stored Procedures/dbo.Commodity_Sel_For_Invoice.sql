SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******      
NAME:      
 dbo.Commodity_Sel_For_Invoice    
    
DESCRIPTION:
   
	Used to get all commodity associated with the given Invoice id from Determinant/ Charge table.
    
INPUT PARAMETERS:
Name			DataType  Default  Description
------------------------------------------------------------
@Cu_Invoice_Id	INT

OUTPUT PARAMETERS:
Name      DataType  Default  Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Commodity_Sel_For_Invoice	123645
	EXEC dbo.Commodity_Sel_For_Invoice	1168494
	
AUTHOR INITIALS :
Initials Name
------------------------------------------------------------
HG		Hari
PNR		Pandarinath

MODIFICATIONS
Initials	Date		Modification
------------------------------------------------------------
HG			04/01/2010	Created
VV			07/08/2010	added COMMODITY_NAME column
HG			08/10/2010	As all the commodities are not going to have determinant modified the script to get the commodity associated with the invoice from either invoice or determinant table.
						(Cable, Document Service,Fire Protection are the few commodities which will not have determinant)
						Application already taking care of -1 commodity_id fetched by this procedure.
******/

CREATE PROCEDURE dbo.Commodity_Sel_For_Invoice
(
	@cu_invoice_id INT
)
AS
BEGIN

	SET NOCOUNT ON;

	WITH Cte_Commodity_Type_Id_List
	AS
	(
		SELECT
			 Commodity_Type_id
			,UBM_SERVICE_CODE
		FROM
			dbo.CU_Invoice_Determinant
		WHERE
			CU_INVOICE_ID  = @cu_invoice_id

		UNION 
		
		SELECT
			 Commodity_Type_id
			,UBM_SERVICE_CODE
		FROM
			dbo.CU_Invoice_Charge
		WHERE
			CU_INVOICE_ID  = @cu_invoice_id
	)
	SELECT
		 ctil.Commodity_Type_id
		,ctil.Ubm_Service_Code
		,com.Commodity_Name
	FROM
		Cte_Commodity_Type_Id_List ctil
		JOIN dbo.Commodity com
			 ON ctil.COMMODITY_TYPE_ID = com.Commodity_Id

END
GO
GRANT EXECUTE ON  [dbo].[Commodity_Sel_For_Invoice] TO [CBMSApplication]
GO
