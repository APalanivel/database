SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_ADD_NEW_PRODUCT_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@userId varchar,
@sessionId varchar,
@delivery varchar(200),
@transServiceLevelId int,
@supplierNominationId int,
@supplierBalancingId int,
@summitComments varchar(4000),
@deliveryPowerId int,
@transServiceLevelPowerId int,
@rfpId int,
@accountGroupId int,
@isBidGroup int,
@productName varchar(200),
@startDate datetime,
@endDate datetime,
@productDescription varchar(200),
@subProductName varchar(200),
@supplierContactId int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- Test Insert new product into sr_rfp
--exec [dbo].[SR_RFP_ADD_NEW_PRODUCT_P] 
--@userId = -2,
--@sessionId = -2,
--@delivery = 'Test Data' ,
--@transServiceLevelId = 1079,
--@supplierNominationId = 1081,
--@supplierBalancingId =1081,
--@summitComments = 'Test Data',
--@deliveryPowerId =NULL,
--@transServiceLevelPowerId =NULL,
--@rfpId = 2047,
--@accountGroupId =169,
--@isBidGroup = 0,
--@productName = 'Test Data',
--@startDate ='2003-05-01 00:00:00.000',
--@endDate ='2003-08-01 00:00:00.000',
--@productDescription ='Test Data',
--@subProductName ='Test Data',
--@supplierContactId = NULL

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[SR_RFP_ADD_NEW_PRODUCT_P] 

@userId varchar,
@sessionId varchar,
@delivery varchar(200),
@transServiceLevelId int,
@supplierNominationId int,
@supplierBalancingId int,
@summitComments varchar(4000),
@deliveryPowerId int,
@transServiceLevelPowerId int,
@rfpId int,
@accountGroupId int,
@isBidGroup int,
@productName varchar(200),
@startDate datetime,
@endDate datetime,
@productDescription varchar(200),
@subProductName varchar(200),
@supplierContactId int

as
	
declare @noOfMonths int
	declare @summitBidReqId int
	declare @priceProductId int
	declare @accountTermId int
	declare @summitBidRequirement int
	declare @selectedProductsId int
	declare @rfpTermId int
	declare @isSubProduct int
	declare @rfpBidId int
	--declare @productTermMapId int

	declare @repl_noOfMonths int
	declare @repl_summitBidReqId int
	declare @repl_priceProductId int
	declare @repl_accountTermId int
	declare @repl_summitBidRequirement int
	declare @repl_selectedProductsId int
	declare @repl_rfpTermId int
	declare @repl_isSubProduct int
	declare @repl_rfpBidId int
	declare @repl_productTermMapId int


	SELECT @noOfMonths= DATEDIFF(month, @startDate, @endDate) + 1

	if @subProductName is not null
	begin
		set @isSubProduct=1
	end
	else
	begin
		set @isSubProduct=0
	end


-- for SR_RFP_BID_REQUIREMENTS


	INSERT INTO SR_RFP_BID_REQUIREMENTS 
	(
		DELIVERY_POINT,
		TRANSPORTATION_LEVEL_TYPE_ID,
		NOMINATION_TYPE_ID,
		BALANCING_TYPE_ID,
		COMMENTS,
		DELIVERY_POINT_POWER_TYPE_ID,
		SERVICE_LEVEL_POWER_TYPE_ID
	)  
	
	VALUES
	
	(
		@delivery,
		@transServiceLevelId,
		@supplierNominationId,
		@supplierBalancingId,
		@summitComments,
		@deliveryPowerId ,
		@transServiceLevelPowerId 
		
	)
	
	SELECT @summitBidReqId=SCOPE_IDENTITY()
	
-- for SR_RFP_PRICING_PRODUCT


	
	if((select count(*) from SR_RFP_PRICING_PRODUCT where PRODUCT_NAME=@productName and SR_RFP_ID=@rfpId)=0)

	begin
		insert into SR_RFP_PRICING_PRODUCT (SR_RFP_ID,PRODUCT_NAME,PRODUCT_DESCRIPTION)
		values
		(@rfpId,@productName,@productDescription)
		set @priceProductId=@@identity

	end

	else
	begin
		select @priceProductId = SR_RFP_PRICING_PRODUCT_ID from 
		SR_RFP_PRICING_PRODUCT 
		where PRODUCT_NAME=@productName and SR_RFP_ID=@rfpId

	end

-- for SR_RFP_SELECTED_PRODUCTS


	if((select count(*) from SR_RFP_SELECTED_PRODUCTS where PRICING_PRODUCT_ID=@priceProductId 
	and IS_SYSTEM_PRODUCT is null and SR_RFP_ID=@rfpId)=0)

	begin
		

		INSERT INTO SR_RFP_SELECTED_PRODUCTS 
		(
			SR_RFP_ID,
			PRICING_PRODUCT_ID,
			SR_RFP_BID_REQUIREMENTS_ID
		)
		VALUES
		(
			@rfpId,
			@priceProductId,
			@summitBidReqId
		)
		set @selectedProductsId=@@identity
	end
	else
	begin

		select @selectedProductsId = SR_RFP_SELECTED_PRODUCTS_ID 
		from SR_RFP_SELECTED_PRODUCTS 
		where PRICING_PRODUCT_ID=@priceProductId 
		and IS_SYSTEM_PRODUCT is null and SR_RFP_ID=@rfpId

	end


-- for SR_RFP_TERM

	if(	select count(*) from SR_RFP_TERM 
		where SR_RFP_TERM_ID = 
		
		(select SR_RFP_TERM_ID 
		from   SR_RFP_ACCOUNT_TERM 
		where 
		SR_ACCOUNT_GROUP_ID=@accountGroupId 
		and IS_BID_GROUP=@isBidGroup 
		and IS_SOP is null 
		and FROM_MONTH=@startDate 
		and TO_MONTH=@endDate 
		and NO_OF_MONTHS=@noOfMonths))=0

	begin

		insert into SR_RFP_TERM (SR_RFP_ID) values (@rfpId)
		set @rfpTermId=SCOPE_IDENTITY()

	end

	else
	begin
		
		select  @rfpTermId = SR_RFP_TERM_ID 
		from   SR_RFP_ACCOUNT_TERM 
		where 
		SR_ACCOUNT_GROUP_ID=@accountGroupId 
		and IS_BID_GROUP=@isBidGroup 
		and IS_SOP is null 
		and FROM_MONTH=@startDate 
		and TO_MONTH=@endDate 
		and NO_OF_MONTHS=@noOfMonths
	end

-- for SR_RFP_ACCOUNT_TERM


	
	if  (select count(*) from  SR_RFP_ACCOUNT_TERM 
		where 
		SR_ACCOUNT_GROUP_ID=@accountGroupId 
		and IS_BID_GROUP=@isBidGroup 
		and IS_SOP is null 
		and FROM_MONTH=@startDate 
		and TO_MONTH=@endDate 
		and NO_OF_MONTHS=@noOfMonths)=0
	begin
		insert into SR_RFP_ACCOUNT_TERM
		(SR_RFP_TERM_ID,SR_ACCOUNT_GROUP_ID,IS_BID_GROUP,IS_SOP,FROM_MONTH,TO_MONTH,NO_OF_MONTHS)
		values
		(@rfpTermId,@accountGroupId,@isBidGroup,null,@startDate,@endDate,@noOfMonths)
		set @accountTermId=SCOPE_IDENTITY()
	end

	else
	begin
		select @accountTermId= SR_RFP_ACCOUNT_TERM_ID from  SR_RFP_ACCOUNT_TERM 
				where 
				SR_ACCOUNT_GROUP_ID=@accountGroupId 
				and IS_BID_GROUP=@isBidGroup 
				and IS_SOP is null 
				and FROM_MONTH=@startDate 
				and TO_MONTH=@endDate 
				and NO_OF_MONTHS=@noOfMonths
	end

-- for SR_RFP_BID_ID


	insert into SR_RFP_BID
	(
		PRODUCT_NAME,
		SR_RFP_BID_REQUIREMENTS_ID,
		SR_RFP_SUPPLIER_PRICE_COMMENTS_ID,
		SR_RFP_SUPPLIER_SERVICE_ID,
		IS_SUB_PRODUCT
	)
	values
	(
		@subProductName,
		@summitBidReqId,
		null,
		null,
		@isSubProduct
	)

set @rfpBidId= @@identity

-- for SR_RFP_TERM_PRODUCT_MAP_ID

	insert into SR_RFP_TERM_PRODUCT_MAP
	(
		SR_RFP_ACCOUNT_TERM_ID,
		SR_RFP_SELECTED_PRODUCTS_ID,
		SR_RFP_BID_ID,
		SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID,
		BID_DATE
	)
	values
	(
		@accountTermId,
		@selectedProductsId,
		@rfpBidId,
		@supplierContactId,
		null
	)
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_ADD_NEW_PRODUCT_P] TO [CBMSApplication]
GO
