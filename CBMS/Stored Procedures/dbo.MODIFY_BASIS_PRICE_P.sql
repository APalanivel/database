SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.MODIFY_BASIS_PRICE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@indexId       	int       	          	
	@pricePointName	varchar(200)	          	
	@year          	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE dbo.MODIFY_BASIS_PRICE_P
	@userId VARCHAR(10),
	@sessionId VARCHAR(20),
	@indexId INT,
	@pricePointName VARCHAR(200),
	@year INT

AS
BEGIN

	SET NOCOUNT ON;

	SELECT CONVERT(VARCHAR(12), b.month_identifier, 101) AS month_identifier
		, basis_price
		, b.RM_FORECAST_ID
	FROM RM_FORECAST a JOIN RM_FORECAST_DETAILS b
			ON a.RM_FORECAST_ID=b.RM_FORECAST_ID
		JOIN RM_FORECAST c
			ON a.RM_FORECAST_ID=c.RM_FORECAST_ID
		JOIN price_index i
			ON i.price_index_id=c.price_index_id
				AND i.index_id=	@indexId 
				AND pricing_poINT=@pricePointName
	WHERE DATEPART(YEAR,b.month_identifier)>=@year

END
GO
GRANT EXECUTE ON  [dbo].[MODIFY_BASIS_PRICE_P] TO [CBMSApplication]
GO
