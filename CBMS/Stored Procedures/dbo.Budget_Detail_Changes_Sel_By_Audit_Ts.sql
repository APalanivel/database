SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME:	dbo.Budget_Detail_Changes_Sel_By_Audit_Ts

DESCRIPTION:
	Gets all changes made between the Audit_Start_Ts and Audit_End_Ts. 

INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@Audit_Start_Ts		DATETIME		Minimum Audit Start Time stamp
	@Audit_End_Ts		DATETIME        Maximum Audit End Time stamp

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	DECLARE @Cur_Ts DateTime = GETDATE()

	EXEC dbo.Budget_Detail_Changes_Sel_By_Audit_Ts '06/1/2010', @Cur_Ts

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:

	Initials	Date		Modification
------------------------------------------------------------
	PNR			06/10/2010	Created   

******/  

CREATE PROCEDURE dbo.Budget_Detail_Changes_Sel_By_Audit_Ts
(    
	 @Audit_Start_Ts     DATETIME    
	,@Audit_End_Ts       DATETIME  
)    
AS     
BEGIN     
	SET NOCOUNT ON;
	
	DECLARE @Budget_Account_Audit TABLE
	(
		Audit_Function		SMALLINT
		,Audit_Ts			DateTime
		,Budget_Account_Id	INT
		,Budget_Id			INT
		,Account_Id			INT
	)
	
	DECLARE @Budget_Details_Audit TABLE
	(
		 Audit_Function			SMALLINT
		,Audit_Ts			DateTime
		,Budget_Detail_Id		INT
		,Budget_Account_Id		INT
		,Month_Identifier		DATETIME
		,PRIMARY KEY CLUSTERED (Budget_Detail_Id)
	)

	DECLARE @Budget_Details TABLE
	(
		Budget_Id					INT
		,Account_Id					INT
		,Budget_Detail_Id			INT
		,Month_Identifier			DATETIME
		,COMMODITY_TYPE_ID			INT
		,Budget_Start_Year			INT
		,TRANSMISSION_VALUE			DECIMAL(32,16)
		,TRANSPORTATION_VALUE		DECIMAL(32,16)
		,VARIABLE_VALUE				DECIMAL(32,16)
		,DISTRIBUTION_VALUE			DECIMAL(32,16)
		,OTHER_BUNDLED_VALUE		DECIMAL(32,16)	
		,RATES_TAX_VALUE			DECIMAL(32,16)
		,SOURCING_TAX_VALUE			DECIMAL(32,16)
		,BUDGET_USAGE				DECIMAL(32,16)
		,OTHER_FIXED_COSTS_VALUE	DECIMAL(32,16)
		,Audit_Function				SMALLINT
		,Audit_Ts					DATETIME
		,Source_Table				VARCHAR(50)
		,Is_Deleted					BIT
	)	

	INSERT INTO @Budget_Details_Audit
	(
		 Budget_Detail_Id
		,Budget_Account_Id
		,Month_Identifier
		,Audit_Function
		,Audit_Ts
	)
	SELECT
		DtlAdt.Budget_Detail_Id
		,DtlAdt.Budget_Account_Id 
		,DtlAdt.Month_Identifier
		,DtlAdt.Audit_Function
		,DtlAdt.Audit_Ts
	 FROM
		 dbo.Budget_Details_Audit DtlAdt
		 JOIN
			(
			SELECT
				 Budget_Detail_Id
				,MAX(Audit_Ts) Max_Audit_Ts
			FROM
				dbo.Budget_Details_Audit
			WHERE
				Audit_Ts BETWEEN @Audit_Start_Ts
								AND @Audit_End_Ts
			GROUP BY
				Budget_Detail_Id
		 
		   ) maxadt
				ON maxadt.Budget_Detail_Id = DtlAdt.Budget_Detail_Id
					AND maxadt.Max_Audit_Ts = DtlAdt.Audit_Ts

	INSERT INTO @Budget_Account_Audit
	(
		 Audit_Function
		,Audit_Ts
		,Budget_Account_Id
		,Budget_Id
		,Account_Id
	)
	SELECT
		AccAdt.Audit_Function
		,AccAdt.Audit_Ts
		,AccAdt.Budget_Account_Id
		,AccAdt.Budget_Id 
		,AccAdt.Account_Id
	 FROM
		 dbo.Budget_Account_Audit AccAdt
		 JOIN 
			(
			SELECT
				 Budget_Account_Id
				,MAX(Audit_Ts) Max_Audit_Ts
			FROM
				dbo.Budget_Account_Audit
			WHERE
				Audit_Ts BETWEEN @Audit_Start_Ts
								AND @Audit_End_Ts
			GROUP BY
				Budget_Account_Id
		 
		   ) maxadt
				ON maxadt.Budget_Account_Id = AccAdt.Budget_Account_Id
					AND maxadt.Max_Audit_Ts = AccAdt.Audit_Ts


	INSERT INTO @Budget_Details
	SELECT
		b.Budget_Id
		,ba.Account_Id
		,DtlAdt.Budget_Detail_Id
		,DtlAdt.Month_Identifier
		,b.COMMODITY_TYPE_ID
		,b.Budget_Start_Year
		,ISNULL(bd.TRANSMISSION_VALUE,0) AS TRANSMISSION_VALUE
		,ISNULL(bd.TRANSPORTATION_VALUE,0) AS TRANSPORTATION_VALUE
		,ISNULL(bd.VARIABLE_VALUE,0) AS VARIABLE_VALUE
		,ISNULL(bd.DISTRIBUTION_VALUE,0) AS DISTRIBUTION_VALUE
		,ISNULL(bd.OTHER_BUNDLED_VALUE,0) AS OTHER_BUNDLED_VALUE
		,ISNULL(bd.RATES_TAX_VALUE,0) AS RATES_TAX_VALUE
		,ISNULL(bd.SOURCING_TAX_VALUE,0) AS SOURCING_TAX_VALUE
		,ISNULL(bd.BUDGET_USAGE,0) AS BUDGET_USAGE
		,ISNULL(bd.OTHER_FIXED_COSTS_VALUE,0) AS OTHER_FIXED_COSTS_VALUE
		,DtlAdt.Audit_Function
		,DtlAdt.Audit_Ts
		,'Budget_Details'
		,ba.IS_DELETED
	FROM
		@Budget_Details_Audit DtlAdt
		LEFT OUTER JOIN dbo.Budget_Details bd
		 	ON DtlAdt.Budget_Detail_Id = bd.BUDGET_DETAIL_ID
		JOIN dbo.Budget_Account ba
			ON ba.Budget_Account_Id = DtlAdt.Budget_Account_Id
		JOIN dbo.Budget b
			ON b.Budget_id = ba.BUDGET_ID
	WHERE
		b.IS_POSTED_TO_DV = 1

	INSERT INTO @Budget_Details
	SELECT DISTINCT
		AccAdt.Budget_Id
		,AccAdt.Account_Id
		,ISNULL(bd.Budget_Detail_Id, bda.Budget_Detail_Id) Budget_Detail_Id
		,ISNULL(bd.Month_Identifier, bda.Month_Identifier) Month_Identifier
		,b.COMMODITY_TYPE_ID
		,b.Budget_Start_Year
		,ISNULL(bd.TRANSMISSION_VALUE,0) AS TRANSMISSION_VALUE
		,ISNULL(bd.TRANSPORTATION_VALUE,0) AS TRANSPORTATION_VALUE
		,ISNULL(bd.VARIABLE_VALUE,0) AS VARIABLE_VALUE
		,ISNULL(bd.DISTRIBUTION_VALUE,0) AS DISTRIBUTION_VALUE
		,ISNULL(bd.OTHER_BUNDLED_VALUE,0) AS OTHER_BUNDLED_VALUE
		,ISNULL(bd.RATES_TAX_VALUE,0) AS RATES_TAX_VALUE
		,ISNULL(bd.SOURCING_TAX_VALUE,0) AS SOURCING_TAX_VALUE
		,ISNULL(bd.BUDGET_USAGE,0) AS BUDGET_USAGE
		,ISNULL(bd.OTHER_FIXED_COSTS_VALUE,0) AS OTHER_FIXED_COSTS_VALUE
		,AccAdt.Audit_Function
		,AccAdt.Audit_Ts
		,'Budget_Account'
		,ISNULL(ba.IS_DELETED, 0) IS_DELETED
	FROM
		@Budget_Account_Audit AccAdt
		LEFT OUTER JOIN dbo.Budget_Account ba
			ON ba.Budget_Account_Id = AccAdt.Budget_Account_Id
		LEFT JOIN dbo.Budget_Details bd
		 	ON bd.BUDGET_ACCOUNT_ID = AccAdt.Budget_Account_Id
		LEFT JOIN @Budget_Details_Audit bda
			ON bda.Budget_Account_Id = AccAdt.Budget_Account_Id	
		INNER JOIN dbo.Budget b
			ON b.Budget_id = AccAdt.Budget_Id
	WHERE
		b.IS_POSTED_TO_DV = 1

	SELECT
		bdgDtl.Budget_Id
		,bdgDtl.Account_Id
		,bdgDtl.Budget_Detail_Id
		,bdgDtl.Month_Identifier
		,bdgDtl.COMMODITY_TYPE_ID
		,bdgDtl.Budget_Start_Year
		,bdgDtl.TRANSMISSION_VALUE
		,bdgDtl.TRANSPORTATION_VALUE
		,bdgDtl.VARIABLE_VALUE
		,bdgDtl.DISTRIBUTION_VALUE
		,bdgDtl.OTHER_BUNDLED_VALUE
		,bdgDtl.RATES_TAX_VALUE
		,bdgDtl.SOURCING_TAX_VALUE
		,bdgDtl.BUDGET_USAGE
		,bdgDtl.OTHER_FIXED_COSTS_VALUE
		,bdgDtl.Audit_Function
		,bdgDtl.Source_Table
		,BdgDtl.Is_Deleted
	FROM
		@Budget_Details BdgDtl
		JOIN
			(
				SELECT
					Budget_Id
					,Budget_Detail_Id
					,Month_Identifier
					,MAX(Audit_Ts) Max_Audit_Ts
				FROM
					@Budget_Details
				GROUP BY
					Budget_id
					,Budget_Detail_Id
					,Month_Identifier			
			) MaxAdt
			ON MaxAdt.Budget_Id = BdgDtl.Budget_Id
				AND MaxAdt.Budget_Detail_Id = BdgDtl.Budget_Detail_Id
				AND MaxAdt.Month_Identifier = BdgDtl.Month_Identifier
				AND MaxAdt.Max_Audit_Ts = BdgDtl.Audit_Ts

END
GO
GRANT EXECUTE ON  [dbo].[Budget_Detail_Changes_Sel_By_Audit_Ts] TO [CBMSApplication]
GO
