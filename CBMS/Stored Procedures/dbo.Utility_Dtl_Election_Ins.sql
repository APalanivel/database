SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Utility_Dtl_Election_Ins

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	 @Commodity_Id				INT
     @Vendor_Id					INT 
     @Question_Commodity_Map_Id INT
     @Industrial_Flag_Cd		INT
     @Commercial_Flag_Cd		INT
     @Comment_ID				INT = NULL

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN
		EXEC Utility_Dtl_Election_Ins 290,30,'Election_Name Test','Enrollment_Period Test','Notification_Deadline Test','Other_Stipulation Test'
	ROLLBACK TRAN
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	SKA			01/12/2011	Created
******/

CREATE PROC dbo.Utility_Dtl_Election_Ins
      (
       @Commodity_Id INT
      ,@Vendor_Id INT
      ,@Election_Name VARCHAR(100)
      ,@Enrollment_Period VARCHAR(500) = NULL
      ,@Notification_Deadline VARCHAR(500) = NULL
      ,@Other_Stipulation VARCHAR(500) = NULL
      ,@Comment_ID INT = NULL )
AS 
BEGIN
 
      SET nocount ON ;

      INSERT INTO
            Utility_Dtl_Election
            (
             Vendor_Commodity_Map_Id
            ,Election_Name
            ,Enrollment_Period
            ,Notification_Deadline
            ,Other_Stipulation
            ,Comment_ID )
            SELECT
                  vcm.VENDOR_COMMODITY_MAP_ID
                 ,@Election_Name
                 ,@Enrollment_Period
                 ,@Notification_Deadline
                 ,@Other_Stipulation
                 ,@Comment_ID
            FROM
                  dbo.VENDOR_COMMODITY_MAP vcm
            WHERE
                  vcm.vendor_id = @Vendor_Id
                  AND vcm.COMMODITY_TYPE_ID = @Commodity_Id
END                        
GO
GRANT EXECUTE ON  [dbo].[Utility_Dtl_Election_Ins] TO [CBMSApplication]
GO
