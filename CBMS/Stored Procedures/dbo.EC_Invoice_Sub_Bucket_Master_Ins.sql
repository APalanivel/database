SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.EC_Invoice_Sub_Bucket_Master_Ins           
              
Description:              
        To insert Data to EC_Invoice_Sub_Bucket_Master table.              
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------  
	@State_Id							INT
    @Bucket_Master_Id					INT
    @tvp_EC_Invoice_Sub_Bucket_Master	dbo.tvp_EC_Invoice_Sub_Bucket_Master
    @User_Info_Id						INT              
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------  
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                
	DECLARE  @tvp_EC_Invoice_Sub_Bucket_Master_Dtl dbo.tvp_EC_Invoice_Sub_Bucket_Master
	INSERT @tvp_EC_Invoice_Sub_Bucket_Master_Dtl  
		SELECT NULL,'Test1' 
		UNION ALL  
		SELECT NULL,'Test2' 

	BEGIN TRAN  
	SELECT * FROM EC_Invoice_Sub_Bucket_Master WHERE Sub_Bucket_Name in ('Test1','Test2')
    EXEC dbo.EC_Invoice_Sub_Bucket_Master_Ins 
      @State_Id = 1
     ,@Bucket_Master_Id = 70
     ,@tvp_EC_Invoice_Sub_Bucket_Master=@tvp_EC_Invoice_Sub_Bucket_Master_Dtl
     ,@User_Info_Id = 100
	SELECT * FROM EC_Invoice_Sub_Bucket_Master WHERE Sub_Bucket_Name in ('Test1','Test2')
	ROLLBACK TRAN             
             
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy               
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2015-04-22		Created For AS400.         
             
******/ 
CREATE PROCEDURE [dbo].[EC_Invoice_Sub_Bucket_Master_Ins]
      ( 
       @State_Id INT
      ,@Bucket_Master_Id INT
      ,@tvp_EC_Invoice_Sub_Bucket_Master dbo.tvp_EC_Invoice_Sub_Bucket_Master READONLY
      ,@User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 
      
      INSERT      INTO dbo.EC_Invoice_Sub_Bucket_Master
                  ( 
                   State_Id
                  ,Bucket_Master_Id
                  ,Sub_Bucket_Name
                  ,Created_User_Id
                  ,Created_Ts
                  ,Updated_User_Id
                  ,Last_Change_Ts )
                  SELECT
                        @State_Id
                       ,@Bucket_Master_Id
                       ,sb.Sub_Bucket_Name
                       ,@User_Info_Id
                       ,GETDATE()
                       ,@User_Info_Id
                       ,GETDATE()
                  FROM
                        @tvp_EC_Invoice_Sub_Bucket_Master sb
                 
            
END


;
GO
GRANT EXECUTE ON  [dbo].[EC_Invoice_Sub_Bucket_Master_Ins] TO [CBMSApplication]
GO
