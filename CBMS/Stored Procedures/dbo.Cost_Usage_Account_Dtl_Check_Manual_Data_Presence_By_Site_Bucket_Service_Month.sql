
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME: 

	dbo.Cost_Usage_Account_Dtl_Check_Manual_Data_Presence_By_Site_Bucket_Service_Month

DESCRIPTION:

Used to Check the invoice and manual data presence by given account id and service month
This procedure will be called to know the what data source code should be updated in Site level or for the given account.

INPUT PARAMETERS:

      Name					DataType          Default     Description
------------------------------------------------------------------
      @Client_Hier_Id		INT
      @Service_Month		DATETIME
      @Bucket_Master_Id		INT

OUTPUT PARAMETERS:
      Name                DataType          Default     Description        
------------------------------------------------------------

    
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Cost_Usage_Account_Dtl_Check_Manual_Data_Presence_By_Site_Bucket_Service_Month 1229, 100154, '2010-01-01'
	EXEC dbo.Cost_Usage_Account_Dtl_Check_Manual_Data_Presence_By_Site_Bucket_Service_Month 1204, 100244, '2010-01-01'

	SELECT TOP 10 * FROM Cost_Usage_Account_Dtl WHERE DATA_SOURCE_CD = 100350

	SELECT* FROM Code WHERE CodeSet_id = 119

AUTHOR INITIALS:
 Initials	Name
------------------------------------------------------------
 HG			Harihara Suthan G

MODIFICATIONS

 Initials	Date		Modification
------------------------------------------------------------
 HG			11/08/2011	Created
 HG			2012-03-26	@Site_Id param changed to @Client_Hier_Id and the EXISTS clause used to filter the Accounts removed as the Client_Hier_Id column added in CUAD table itself.
******/

CREATE PROCEDURE dbo.Cost_Usage_Account_Dtl_Check_Manual_Data_Presence_By_Site_Bucket_Service_Month
      @Client_Hier_Id INT
     ,@Bucket_Master_Id INT
     ,@Service_Month DATETIME
AS 
BEGIN

      SET NOCOUNT ON ;

      SELECT
            max(case WHEN ds.Code_Value = 'CBMS'
                          OR ds.Code_Value = 'DE' THEN 1
                     WHEN ds.Code_Value = 'Invoice' THEN 0
                END) Has_Manual_Data
      FROM
            dbo.Cost_Usage_Account_Dtl cu
            INNER JOIN dbo.Bucket_Master bm
                  ON bm.Bucket_Master_Id = cu.Bucket_Master_Id
            LEFT OUTER JOIN dbo.Code ds
                  ON ds.Code_Id = cu.Data_Source_Cd
      WHERE
			cu.Client_Hier_Id = @Client_Hier_Id
            AND bm.Bucket_Master_Id = @Bucket_Master_Id
            AND cu.SERVICE_MONTH = @Service_Month

END
;
GO

GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Dtl_Check_Manual_Data_Presence_By_Site_Bucket_Service_Month] TO [CBMSApplication]
GO
