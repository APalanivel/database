SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
                           
 NAME:  [dbo].[Rate_Sel_By_Commodity_Id]                        
                            
 DESCRIPTION:        
				TO get the rates information .                           
                            
 INPUT PARAMETERS:        
                           
 Name                                   DataType        Default       Description        
---------------------------------------------------------------------------------------------------------------      
                         
 OUTPUT PARAMETERS:        
                                 
 Name                                   DataType        Default       Description        
---------------------------------------------------------------------------------------------------------------      
                            
 USAGE EXAMPLES:                                
---------------------------------------------------------------------------------------------------------------     
       
 Exec  dbo.Rate_Sel_By_Commodity_Id 290 ,3     
                           
 AUTHOR INITIALS:      
         
 Initials               Name        
---------------------------------------------------------------------------------------------------------------      
 SP                     Srinivas                              
                             
 MODIFICATIONS:      
                             
 Initials               Date            Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                     2019-06-10      Created .                          
                           
******/

CREATE PROCEDURE [dbo].[Rate_Sel_By_Commodity_Id]
    (
        @Commodity_Id INT
        , @Vendor_Id INT = NULL
    )
AS
    BEGIN



        SET NOCOUNT ON;


        SELECT
            r.RATE_ID
            , r.VENDOR_ID
            , r.COMMODITY_TYPE_ID
            , r.RATE_NAME
        FROM
            dbo.RATE r
        WHERE
            r.COMMODITY_TYPE_ID = @Commodity_Id
            AND (   @Vendor_Id IS NULL
                    OR  r.VENDOR_ID = @Vendor_Id)
        ORDER BY
            r.RATE_NAME;


    END;
GO
GRANT EXECUTE ON  [dbo].[Rate_Sel_By_Commodity_Id] TO [CBMSApplication]
GO
