SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
	Trade.Deal_Ticket_Volume_Sel_By_Deal_Ticket_Id
   
    
DESCRIPTION:   
	This procedure to get summary page details for volumes.
    
INPUT PARAMETERS:    
    Name			DataType       Default        Description    
-----------------------------------------------------------------------------    
	@Deal_Ticket_Id INT
  
  
    
OUTPUT PARAMETERS:  
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
    
	Exec Trade.Deal_Ticket_Volume_Sel_By_Deal_Ticket_Id 131937 
	Exec Trade.Deal_Ticket_Volume_Sel_By_Deal_Ticket_Id 298
	
	Exec Trade.Deal_Ticket_Volume_Sel_By_Deal_Ticket_Id 646,NULL,151196
    
AUTHOR INITIALS:     
	Initials	Name       
-----------------------------------------------------------------------------       
	SP          SrinivasaRao Patchava   
	RR			Raghu Reddy 
    
MODIFICATIONS     
	Initials	Date        Modification      
-----------------------------------------------------------------------------       
	SP          2018-11-21  Create sp to get summary page details for volumes  
	RR			2019-11-09	GRM-1487 Modified script to pull executed months only in the trade   
	RR			2020-03-23	GRM-1763 Modified to return Trade_Price in place of Client_Generated_Price as price for a client generated
							deal can be editable
******/

CREATE PROCEDURE [Trade].[Deal_Ticket_Volume_Sel_By_Deal_Ticket_Id]
    (
        @Deal_Ticket_Id INT
        , @Client_Hier_Id VARCHAR(MAX) = NULL
        , @Contract_Id INT = NULL
        , @Trade_Id INT = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;


        SELECT
            RIGHT(CONVERT(VARCHAR(100), dtchvd2.Deal_Month, 106), 8) AS [Month]
            , CAST(SUM(dtchvd2.Total_Volume * cuc.CONVERSION_FACTOR) AS DECIMAL(28, 0)) AS Volume
            , MAX(tp.Trade_Price) AS Client_Generated_Price
            , MAX(dthv.Trigger_Target_Price) AS Trigger_Target_Price
            , MAX(tp.Trade_Price) AS Trade_Price
            , cu.CURRENCY_UNIT_NAME
            , REPLACE(CONVERT(VARCHAR(20), MAX(dthv.Client_Generated_Fixed_Dt), 106), ' ', '-') AS Client_Generated_Fixed_Dt
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtchvd2
                ON dt.Deal_Ticket_Id = dtchvd2.Deal_Ticket_Id
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = dtchvd2.Uom_Type_Id
                   AND  cuc.CONVERTED_UNIT_ID = dt.Uom_Type_Id
            INNER JOIN Trade.Deal_Total_Hedge_Volume dthv
                ON dthv.Deal_Ticket_Id = dtchvd2.Deal_Ticket_Id
                   AND  dthv.Deal_Month = dtchvd2.Deal_Month
            LEFT JOIN Trade.Trade_Price tp
                ON tp.Trade_Price_Id = dtchvd2.Trade_Price_Id
            INNER JOIN dbo.CURRENCY_UNIT cu
                ON cu.CURRENCY_UNIT_ID = dt.Currency_Unit_Id
        WHERE
            dt.Deal_Ticket_Id = @Deal_Ticket_Id
            AND (   @Client_Hier_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Client_Hier_Id, ',') ch
                                   WHERE
                                        CAST(ch.Segments AS INT) = dtchvd2.Client_Hier_Id))
            AND (   @Contract_Id IS NULL
                    OR  dtchvd2.Contract_Id = @Contract_Id)
            AND (   (@Trade_Id IS NULL)
                    OR  (   dtchvd2.Trade_Number = @Trade_Id
                            AND tp.Trade_Price IS NOT NULL
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                Trade.Deal_Ticket_Client_Hier dtch
                                                INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                                                    ON chws.Deal_Ticket_Client_Hier_Id = dtch.Deal_Ticket_Client_Hier_Id
                                                INNER JOIN Trade.Workflow_Status_Map wsm
                                                    ON wsm.Workflow_Status_Map_Id = chws.Workflow_Status_Map_Id
                                                INNER JOIN Trade.Workflow_Status ws
                                                    ON ws.Workflow_Status_Id = wsm.Workflow_Status_Id
                                           WHERE
                                                dtch.Deal_Ticket_Id = @Deal_Ticket_Id
                                                AND dtch.Deal_Ticket_Id = dt.Deal_Ticket_Id
                                                AND dtchvd2.Client_Hier_Id = dtch.Client_Hier_Id
                                                AND dtchvd2.Deal_Month = chws.Trade_Month
                                                AND chws.Is_Active = 1
                                                AND ws.Workflow_Status_Name = 'Order Executed')))
        GROUP BY
            dtchvd2.Deal_Month
            , cu.CURRENCY_UNIT_NAME
        ORDER BY
            dtchvd2.Deal_Month;



    END;










GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Volume_Sel_By_Deal_Ticket_Id] TO [CBMSApplication]
GO
