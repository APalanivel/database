SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.[cbmsCurrencyUnit_DeleteCountryMappings]


DESCRIPTION: 


INPUT PARAMETERS:    
      Name                  DataType          Default     Description    
------------------------------------------------------------------    
	@MyAccountId               int
	@CurrencyUnitId            int
	@is_sv_rpl                 bit               0
        
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates
	   DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[cbmsCurrencyUnit_DeleteCountryMappings]
(
	@MyAccountId int
	,@CurrencyUnitId int
	,@is_sv_rpl bit = 0
)
AS


DELETE FROM 
			CURRENCY_UNIT_COUNTRY_MAP
WHERE 
			currency_unit_id = @CurrencyUnitId
GO
GRANT EXECUTE ON  [dbo].[cbmsCurrencyUnit_DeleteCountryMappings] TO [CBMSApplication]
GO
