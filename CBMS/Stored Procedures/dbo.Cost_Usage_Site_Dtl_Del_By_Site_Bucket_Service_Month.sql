SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.dbo.Cost_Usage_Site_Dtl_Del_By_Site_Bucket_Service_Month

DESCRIPTION:
	Used to delete data from Cost_Usage_Site_Dtl table based on the given Site and Bucket_id for the given service month
	
INPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	@Client_Hier_Id		INT
	@Bucket_Master_Id		INT	
	@Service_Month			DATE

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN
		EXEC dbo.Cost_Usage_Site_Dtl_Del_By_Site_Bucket_Service_Month  2586,100154,'2010-05-01'
	ROLLBACK TRAN

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG		Harihara Suthan G

MODIFICATIONS

	Initials	Date		   Modification
------------------------------------------------------------
	HG		09/07/2011   Created  
******/ 
  
CREATE PROCEDURE dbo.Cost_Usage_Site_Dtl_Del_By_Site_Bucket_Service_Month
      @Client_Hier_Id INT
     ,@Bucket_Master_id INT
     ,@Service_Month DATE
AS 
BEGIN

      SET NOCOUNT ON

      DELETE
            cus
      FROM
            dbo.Cost_Usage_Site_Dtl cus
      WHERE
            cus.Client_Hier_Id = @Client_Hier_Id
            AND cus.Bucket_Master_Id = @Bucket_Master_id
            AND cus.Service_Month = @Service_Month

END
;
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Site_Dtl_Del_By_Site_Bucket_Service_Month] TO [CBMSApplication]
GO
