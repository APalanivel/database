SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE           PROCEDURE DBO.RC_GET_RATE_COMPARISON_REQUIREMENT_P

@userId varchar(20),
@sessionId varchar(20),
@rateName varchar(100),
@rateId varchar(10),
@strRatename varchar(100)


AS
	set nocount on
	IF @strRatename <> ''
		select isnull(replace(rate.RATE_REQUIREMENTS,'"',''),'' ) RATE_REQUIREMENTS
		
		from RATE rate
			
		where rate.RATE_NAME like @strRatename and rate.RATE_REQUIREMENTS <> ''


	IF @rateName <> ''
		select rateComparison.COMPARED_RATES_WITH_IDS
		
		from RC_RATE_COMPARISON rateComparison
			
		where rateComparison.RC_RATE_COMPARISON_ID = @rateId

	IF @rateId <> ''
		select isnull(replace(rate.RATE_REQUIREMENTS,'"',''),'' ) RATE_REQUIREMENTS
		
		from RATE rate
			
		where rate.RATE_ID = @rateId
GO
GRANT EXECUTE ON  [dbo].[RC_GET_RATE_COMPARISON_REQUIREMENT_P] TO [CBMSApplication]
GO
