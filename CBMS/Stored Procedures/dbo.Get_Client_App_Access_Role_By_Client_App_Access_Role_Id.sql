SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
 NAME: [dbo].[Get_Client_App_Access_Role_By_Client_App_Access_Role_Id]      
      
 DESCRIPTION:        
			Get Client_App_Access_Role by Client_App_Access_Role_Id          
        
 INPUT PARAMETERS:        
       
 Name                                DataType            Default        Description        
---------------------------------------------------------------------------------------------------------------      
 @Client_App_Access_Role_Id             INT                
      
        
 OUTPUT PARAMETERS:              
 Name                                DataType            Default        Description        
---------------------------------------------------------------------------------------------------------------      
        
 USAGE EXAMPLES:            
---------------------------------------------------------------------------------------------------------------       
       
	 EXEC dbo.Get_Client_App_Access_Role_By_Client_App_Access_Role_Id @Client_App_Access_Role_Id= 8       
        
       
 AUTHOR INITIALS:       
       
 Initials               Name        
---------------------------------------------------------------------------------------------------------------      
 SP                     Sandeep Pigilam  
         
 MODIFICATIONS:       
        
 Initials               Date            Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                     2013-12-23      Created for RA Admin user management      
       
******/ 
  
CREATE PROCEDURE [dbo].[Get_Client_App_Access_Role_By_Client_App_Access_Role_Id]
      ( 
       @Client_App_Access_Role_Id INT )
AS 
BEGIN   
      SET NOCOUNT ON   
  
      SELECT
            caar.App_Access_Role_Name
           ,caar.App_Access_Role_Dsc
           ,caar.Client_App_Access_Role_Id
           ,c.Code_Value AS App_Access_Role_Type_Cd
      FROM
            dbo.Client_App_Access_Role caar
            INNER JOIN dbo.Code c
                  ON caar.App_Access_Role_Type_Cd = c.Code_Id
      WHERE
            caar.Client_App_Access_Role_Id = @Client_App_Access_Role_Id  
            
      
END;

;
GO
GRANT EXECUTE ON  [dbo].[Get_Client_App_Access_Role_By_Client_App_Access_Role_Id] TO [CBMSApplication]
GO
