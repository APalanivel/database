SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:    Trade.Deal_Ticket_Dtls_Sel_By_Trade_Id  
     
      
DESCRIPTION:     
     
  This procedure to get summary page details for deal ticket summery.  
      
INPUT PARAMETERS:      
      Name          DataType       Default        Description      
-----------------------------------------------------------------------------      
 @Deal_Ticket_Id   INT  
   
    
      
OUTPUT PARAMETERS:    
      
 Name     DataType   Default  Description      
-----------------------------------------------------------------------------      
      
      
      
USAGE EXAMPLES:      
-----------------------------------------------------------------------------      
   
 Exec Trade.Deal_Ticket_Dtls_Sel_By_Trade_Id  131253,16191  
 Exec Trade.Deal_Ticket_Dtls_Sel_By_Trade_Id  131089,16056
 Exec Trade.Deal_Ticket_Dtls_Sel_By_Trade_Id  131733,16619
         
AUTHOR INITIALS:       
 Initials    Name  
-----------------------------------------------------------------------------         
 SP          SrinivasaRao Patchava      
      
MODIFICATIONS       
	Initials    Date        Modification        
-----------------------------------------------------------------------------         
	SP          2018-11-21  Global Risk Management - Create sp to get summary page details for deal ticket summery 
	RR			2019-11-09	GRM-1487 Modified script to pull executed months only in the trade  
       
      
******/

CREATE PROCEDURE [Trade].[Deal_Ticket_Dtls_Sel_By_Trade_Id]
    (
        @Deal_Ticket_Id INT
        , @Trade_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Trade_CounterParty VARCHAR(MAX) = ''
            , @Trade_Supplier VARCHAR(MAX) = '';

        SELECT
            @Trade_CounterParty = @Trade_CounterParty + rmcp.COUNTERPARTY_NAME + ','
        FROM
            Trade.Deal_Ticket_Client_Hier_TXN_Status dtchts
            INNER JOIN dbo.RM_COUNTERPARTY rmcp
                ON dtchts.Cbms_Counterparty_Id = rmcp.RM_COUNTERPARTY_ID
        WHERE
            dtchts.Cbms_Trade_Number = @Trade_Id
        GROUP BY
            rmcp.COUNTERPARTY_NAME;

        SELECT
            @Trade_Supplier = @Trade_Supplier + rmcp.VENDOR_NAME + ','
        FROM
            Trade.Deal_Ticket_Client_Hier_TXN_Status dtchts
            INNER JOIN dbo.VENDOR rmcp
                ON dtchts.Cbms_Counterparty_Id = rmcp.VENDOR_ID
        WHERE
            dtchts.Cbms_Trade_Number = @Trade_Id
        GROUP BY
            rmcp.VENDOR_NAME;

        SELECT
            dt.Deal_Ticket_Id
            , dt.Deal_Ticket_Number
            , ch.Client_Name
            , c.Commodity_Name
            , ehed.ENTITY_NAME AS Hedge_Type
            , ta.Code_Value AS Trade_Action_Type
            , dtt.Code_Value AS Deal_Ticket_Type
            , idx.ENTITY_NAME AS Price_Index
            , prix.PRICING_POINT
            , cfrqcd.Code_Value AS Deal_Ticket_Frequency
            , eum.ENTITY_NAME AS Uom
            , CASE WHEN dt.Is_Client_Generated = 1 THEN 'Yes'
                  WHEN dt.Is_Client_Generated = 0 THEN 'No'
              END AS Client_Generated
            , applyprice.Code_Value AS Apply_Price
            , CASE WHEN dt.Is_Individual_Site_Pricing_Required = 1 THEN 'Yes'
                  WHEN dt.Is_Individual_Site_Pricing_Required = 0 THEN 'No'
              END AS Individual_Site_Pricing
            , cu.CURRENCY_UNIT_NAME AS Currency
            , CASE WHEN COUNT(DISTINCT chsite.Site_Id) > 1 THEN 'Multiple'
                  ELSE MAX(RTRIM(chsite.City) + ', ' + chsite.State_Name + ' (' + chsite.Site_name + ')')
              END AS Site_Name
            , SUBSTRING(DATENAME(MONTH, MIN(dtchvd.Deal_Month)), 1, 3) + ' '
              + CAST(DATEPART(YEAR, MIN(dtchvd.Deal_Month)) AS VARCHAR) AS Hedge_Start_Month
            , SUBSTRING(DATENAME(MONTH, MAX(dtchvd.Deal_Month)), 1, 3) + ' '
              + CAST(DATEPART(YEAR, MAX(dtchvd.Deal_Month)) AS VARCHAR) AS Hedge_End_Month
            , iniui.FIRST_NAME + ' ' + iniui.LAST_NAME AS Initiated_By
            , iniui.EMAIL_ADDRESS AS Initiator_Email_Address
            , currq.FIRST_NAME + ' ' + currq.LAST_NAME AS Currently_With
            , REPLACE(CONVERT(VARCHAR(20), dt.Created_Ts, 106), ' ', '-') AS Date_Initiated
            , ISNULL(dt.Is_Bid, 0) AS Is_Bid
            , e.ENTITY_NAME AS Hedge_Mode_Type
            , dt.Uom_Type_Id
            , dt.Hedge_Type_Cd
            , dt.Hedge_Mode_Type_Id
            , dt.Price_Index_Id
            , dt.Commodity_Id
            , CASE WHEN LEN(@Trade_CounterParty) > 1 THEN STUFF(@Trade_CounterParty, LEN(@Trade_CounterParty), 1, '')
                  WHEN LEN(@Trade_Supplier) > 1 THEN STUFF(@Trade_Supplier, LEN(@Trade_Supplier), 1, '')
                  ELSE NULL
              END Trade_Counterparty
            , dt.Client_Id
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtchvd
                ON dtchvd.Deal_Ticket_Id = dt.Deal_Ticket_Id
            INNER JOIN Trade.Trade_Price tp
                ON tp.Trade_Price_Id = dtchvd.Trade_Price_Id
            INNER JOIN Core.Client_Hier ch
                ON dt.Client_Id = ch.Client_Id
            INNER JOIN dbo.Commodity c
                ON c.Commodity_Id = dt.Commodity_Id
            INNER JOIN dbo.ENTITY ehed
                ON ehed.ENTITY_ID = dt.Hedge_Type_Cd
            INNER JOIN dbo.PRICE_INDEX prix
                ON prix.PRICE_INDEX_ID = dt.Price_Index_Id
            INNER JOIN dbo.ENTITY idx
                ON prix.INDEX_ID = idx.ENTITY_ID
            INNER JOIN dbo.Code cfrqcd
                ON cfrqcd.Code_Id = dt.Deal_Ticket_Frequency_Cd
            INNER JOIN dbo.ENTITY eum
                ON dt.Uom_Type_Id = eum.ENTITY_ID
            INNER JOIN dbo.Code applyprice
                ON applyprice.Code_Id = dt.Trade_Pricing_Option_Cd
            INNER JOIN dbo.Code ta
                ON ta.Code_Id = dt.Trade_Action_Type_Cd
            INNER JOIN dbo.Code dtt
                ON dtt.Code_Id = dt.Deal_Ticket_Type_Cd
            INNER JOIN dbo.Workflow wf
                ON dt.Workflow_Id = wf.Workflow_Id
            LEFT JOIN dbo.CURRENCY_UNIT cu
                ON cu.CURRENCY_UNIT_ID = dt.Currency_Unit_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dtch.Deal_Ticket_Id = dt.Deal_Ticket_Id
                   AND  dtchvd.Client_Hier_Id = dtch.Client_Hier_Id
            INNER JOIN Core.Client_Hier chsite
                ON chsite.Client_Id = dt.Client_Id
                   AND  chsite.Client_Hier_Id = dtch.Client_Hier_Id
            LEFT JOIN dbo.USER_INFO iniui
                ON iniui.USER_INFO_ID = dt.Created_User_Id
            LEFT JOIN dbo.USER_INFO currq
                ON currq.QUEUE_ID = dt.Queue_Id
            INNER JOIN dbo.ENTITY e
                ON e.ENTITY_ID = dt.Hedge_Mode_Type_Id
        WHERE
            dt.Deal_Ticket_Id = @Deal_Ticket_Id
            AND ch.Sitegroup_Id = 0
            AND dtchvd.Trade_Number = @Trade_Id
            AND tp.Trade_Price IS NOT NULL
            AND EXISTS (   SELECT
                                1
                           FROM
                                Trade.Deal_Ticket_Client_Hier dtch
                                INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                                    ON chws.Deal_Ticket_Client_Hier_Id = dtch.Deal_Ticket_Client_Hier_Id
                                INNER JOIN Trade.Workflow_Status_Map wsm
                                    ON wsm.Workflow_Status_Map_Id = chws.Workflow_Status_Map_Id
                                INNER JOIN Trade.Workflow_Status ws
                                    ON ws.Workflow_Status_Id = wsm.Workflow_Status_Id
                           WHERE
                                dtch.Deal_Ticket_Id = @Deal_Ticket_Id
                                AND dtch.Deal_Ticket_Id = dt.Deal_Ticket_Id
                                AND dtchvd.Client_Hier_Id = dtch.Client_Hier_Id
                                AND dtchvd.Deal_Month = chws.Trade_Month
                                AND chws.Is_Active = 1
                                AND ws.Workflow_Status_Name = 'Order Executed')
        GROUP BY
            dt.Deal_Ticket_Id
            , dt.Deal_Ticket_Number
            , ch.Client_Name
            , c.Commodity_Name
            , ehed.ENTITY_NAME
            , ta.Code_Value
            , dtt.Code_Value
            , idx.ENTITY_NAME
            , prix.PRICING_POINT
            , cfrqcd.Code_Value
            , eum.ENTITY_NAME
            , CASE WHEN dt.Is_Client_Generated = 1 THEN 'Yes'
                  WHEN dt.Is_Client_Generated = 0 THEN 'No'
              END
            , applyprice.Code_Value
            , CASE WHEN dt.Is_Individual_Site_Pricing_Required = 1 THEN 'Yes'
                  WHEN dt.Is_Individual_Site_Pricing_Required = 0 THEN 'No'
              END
            , wf.Workflow_Id
            , wf.Workflow_Name
            , wf.Workflow_Description
            , cu.CURRENCY_UNIT_NAME
            , iniui.FIRST_NAME + ' ' + iniui.LAST_NAME
            , iniui.EMAIL_ADDRESS
            , currq.FIRST_NAME + ' ' + currq.LAST_NAME
            , dt.Created_Ts
            , ISNULL(dt.Is_Bid, 0)
            , e.ENTITY_NAME
            , dt.Uom_Type_Id
            , dt.Hedge_Type_Cd
            , dt.Hedge_Mode_Type_Id
            , dt.Price_Index_Id
            , dt.Commodity_Id
            , dt.Client_Id;




    END;

GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Dtls_Sel_By_Trade_Id] TO [CBMSApplication]
GO
