SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********         
NAME:  dbo.Contract_Lock_Dtl_Upd        
       
DESCRIPTION:   
  
Used to Insert/update the data into dbo.Site_Commodity_Analyst_Merge table.  
      
INPUT PARAMETERS:          
      Name     DataType          Default     Description          
------------------------------------------------------------          
      @Contract_Lock_Dtl_Id INT  
   @Contract_Id   INT  
   @Start_Dt    DATE   
   @Expiration_Dt      DATE   
   @Nymex_Price   DECIMAL (28, 10)   
   @Percentage_Locked     DECIMAL (5, 2)   
          
          
OUTPUT PARAMETERS:          
      Name              DataType          Default     Description          
------------------------------------------------------------          
          
USAGE EXAMPLES:     
  
BEGIN TRAN  
select * from dbo.Contract where Contract_Id = 10871  
EXEC DBO.Contract_Lock_Dtl_Upd 10871,'2012-01-01','2012-12-01',100,200,300,50,15  
select * from dbo.Contract_Lock_Dtl where Contract_Id = 10871  
ROLLBACK TRAN  
  
  
BEGIN TRAN  
select * from dbo.Contract where Contract_Id = 10961  
EXEC DBO.Contract_Lock_Dtl_Upd 10871,'2012-01-01','2012-12-01',5,100,500,25,5  
select * from dbo.Contract_Lock_Dtl where Contract_Id = 10871  
ROLLBACK TRAN  
  
  
------------------------------------------------------------    
      
AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
AKR   Ashok Kumar Raju  
      
      
Initials Date  Modification        
------------------------------------------------------------        
AKR  2012-09-20  Created new sp (for POCO Project)  
******/  
  
CREATE PROCEDURE dbo.Contract_Lock_Dtl_Upd
      ( 
       @Contract_Lock_Dtl_Id INT
      ,@Start_Dt DATE
      ,@Expiration_Dt DATE
      ,@Nymex_Price DECIMAL(28, 10)
      ,@Percentage_Locked DECIMAL(5, 2) )
AS 
BEGIN  
      SET NOCOUNT ON ;  
        
      UPDATE
            cld
      SET   
            cld.Start_Dt = @Start_Dt
           ,cld.Expiration_Dt = @Expiration_Dt
           ,cld.Nymex_Price = @Nymex_Price
           ,cld.Percentage_Locked = @Percentage_Locked
      FROM
            dbo.Contract_Lock_Dtl cld
      WHERE
            cld.Contract_Lock_Dtl_Id = @Contract_Lock_Dtl_Id  
                    
END  
;
GO
GRANT EXECUTE ON  [dbo].[Contract_Lock_Dtl_Upd] TO [CBMSApplication]
GO
