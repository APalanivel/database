SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
NAME:  
	dbo.Code_Ins
 
DESCRIPTION:  
	Used to insert UOM in code table 
  
INPUT PARAMETERS:  
Name				DataType		Default Description  
------------------------------------------------------------  
@code_value			VARCHAR(25)
@code_dsc			VARCHAR(255)
@is_active			BIT				1
@is_default			BIT				0
@display_seq		INT				100
@std_column_name	VARCHAR(255)  


OUTPUT PARAMETERS:  
Name				DataType		Default Description  
------------------------------------------------------------  
USAGE EXAMPLES:  
------------------------------------------------------------

	EXEC dbo.Code_Ins 'Test2','Test2',1,0,200, 'UOM_CD'

AUTHOR INITIALS:  
Initials	Name  
------------------------------------------------------------  
GB			Geetansu Behera  
CMH			Chad Hattabaugh   
HG			Hari
SKA			Shobhit Kr Agrawal

 
MODIFICATIONS   
Initials	Date		Modification  
------------------------------------------------------------  
GB						created
SKA		26/08/2009		Modified as per coding standard (used @std_column_name as input parameter because it's inserting the value in codeset)

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[Code_Ins]  
	@code_value VARCHAR(25),
	@code_dsc VARCHAR(255),
	@is_active BIT = 1,
	@is_default BIT = 0,
	@display_seq INT = 100,
	@std_column_name VARCHAR(255)  
AS   

BEGIN  

	SET NOCOUNT ON

	INSERT INTO dbo.CODE(CODE_VALUE
		,CODESET_ID
		,CODE_DSC
		,IS_ACTIVE
		,IS_DEFAULT
		,DISPLAY_SEQ)
	SELECT
		@Code_Value
		,CodeSet_Id
		,@Code_dsc
		,@is_Active
		,@is_Default
		,@display_seq
	FROM dbo.CodeSet
	WHERE STD_COLUMN_NAME = @std_column_name

END
GO
GRANT EXECUTE ON  [dbo].[Code_Ins] TO [CBMSApplication]
GO
