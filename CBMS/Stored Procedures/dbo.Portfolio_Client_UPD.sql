SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********  
NAME: dbo.Portfolio_Client_UPD   

DESCRIPTION:     
Updates Portfolio Client Id column in Client Table, when a client is mapped to Portfolio Client.
The update happens only if the client is already not mapped to any portfolio or when the portfolio client is being updated to NULL.
	
INPUT PARAMETERS:    
Name		           DataType        Default         Description    
----------------------------------------------------------------    
@Client_Id               INT
@Portfolio_client_Id     INT            NULL     


OUTPUT PARAMETERS:    
Name   DataType  Default Description    
------------------------------------------------------------    

USAGE EXAMPLES:    
------------------------------------------------------------    
SELECT TOP 10 * FROM dbo.Client WHERE Portfolio_Client_Id IS NULL

BEGIN TRAN
DECLARE @oRowsAffected1 INT
-- Update portfolio client id to some client
EXEC dbo.Portfolio_Client_UPD 102, 108, @oRowsAffected1 OUTPUT
SELECT @oRowsAffected1
SELECT Portfolio_Client_Id, * FROM dbo.Client WHERE Client_Id = 102

DECLARE @oRowsAffected2 INT
-- Try updating the portfolio for the same client to a different client
EXEC dbo.Portfolio_Client_UPD 102, 109, @oRowsAffected2 OUTPUT
SELECT @oRowsAffected2
SELECT Portfolio_Client_Id, * FROM dbo.Client WHERE Client_Id = 102

DECLARE @oRowsAffected3 INT
--Update the portfolio client to NULL
EXEC dbo.Portfolio_Client_UPD 102, NULL, @oRowsAffected3 OUTPUT
SELECT @oRowsAffected3
SELECT Portfolio_Client_Id, * FROM dbo.Client WHERE Client_Id = 102

ROLLBACK

AUTHOR INITIALS:    
Initials	Name    
------------------------------------------------------------    
RK          Rajesh Kasoju
 
MODIFICATIONS     
Initials	       Date		       Modification    
------------------------------------------------------------    
 RK                07/03/2012        Created    
    
******/

CREATE PROCEDURE dbo.Portfolio_Client_UPD
      ( 
       @Client_Id INT
      ,@Portfolio_client_Id INT = NULL
      ,@oRowsAffected INT OUTPUT )
AS 
BEGIN 

      SET NOCOUNT ON ;

      UPDATE
            dbo.CLIENT
      SET   
            Portfolio_Client_Id = @Portfolio_Client_Id
      WHERE
            Client_Id = @Client_Id
            AND ( Portfolio_Client_Id IS NULL
                  OR @Portfolio_client_Id IS NULL )
      
      SET @oRowsAffected = @@ROWCOUNT
END
GO
GRANT EXECUTE ON  [dbo].[Portfolio_Client_UPD] TO [CBMSApplication]
GO
