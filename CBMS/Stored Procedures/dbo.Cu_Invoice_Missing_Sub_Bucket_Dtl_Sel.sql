SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
                        
 NAME: dbo.Cu_Invoice_Missing_Sub_Bucket_Dtl_Sel

 DESCRIPTION:      
		  To show the invoice details for which 

 INPUT PARAMETERS:      

 Name                               DataType          Default       Description      
---------------------------------------------------------------------------------------------------------------    
 @Cu_Invoice_Id				         INT        

 OUTPUT PARAMETERS:      
                               
 Name                               DataType          Default       Description      
---------------------------------------------------------------------------------------------------------------    

 USAGE EXAMPLES:                              
---------------------------------------------------------------------------------------------------------------      


EXEC dbo.Cu_Invoice_Missing_Sub_Bucket_Dtl_Sel @Cu_Invoice_Id = 93899252

EXEC dbo.Cu_Invoice_Missing_Sub_Bucket_Dtl_Sel @Cu_Invoice_Id = 93899253
EXEC dbo.Cu_Invoice_Missing_Sub_Bucket_Dtl_Sel @Cu_Invoice_Id = 93263356
EXEC dbo.Cu_Invoice_Missing_Sub_Bucket_Dtl_Sel @Cu_Invoice_Id = 93691181

EXEC dbo.Cu_Invoice_Missing_Sub_Bucket_Dtl_Sel @Cu_Invoice_Id = 93946164


SELECT TOP 100 * FROM CU_INVOICE_DETERMINANT WHERE Bucket_Master_Id IS NOT NULL AND Ubm_Sub_Bucket_Code iS NOT NULL AND EC_INVOICE_SUb_Bucket_Master_Id IS NULL ORDER BY CU_Invoice_Determinant_ID DESC
SELECT TOP 100 * FROM CU_INVOICE_Charge WHERE Bucket_Master_Id IS NOT NULL AND Ubm_Sub_Bucket_Code iS NOT NULL AND EC_INVOICE_SUb_Bucket_Master_Id IS NULL ORDER BY CU_Invoice_Charge_ID DESC


 AUTHOR INITIALS:    
       
 Initials                   Name      
---------------------------------------------------------------------------------------------------------------    
 HG							Harihara Suthan Ganesan

 MODIFICATIONS:    

 Initials               Date            Modification    
---------------------------------------------------------------------------------------------------------------    
 HG						2020-06-01		D20-1972, Created to show the details in new Sub bucket mapping page
******/
CREATE PROCEDURE [dbo].[Cu_Invoice_Missing_Sub_Bucket_Dtl_Sel]
(@Cu_Invoice_Id INT)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Missing_Sub_Bucket_Dtl TABLE
	(
		Commodity_Id		  INT NOT NULL
		, Bucket_Master_Id	  INT NOT NULL
		, Ubm_Bucket_Name	  VARCHAR(255)  NULL
		, Ubm_Sub_Bucket_Name NVARCHAR(255) NULL
		, Ubm_Uom_Name		  VARCHAR(255) NULL
		, Uom_Id			  INT NULL
	);

	INSERT INTO @Missing_Sub_Bucket_Dtl
		(
			Commodity_Id
			, Bucket_Master_Id
			, Ubm_Bucket_Name
			, Ubm_Sub_Bucket_Name
			, Ubm_Uom_Name
			, Uom_Id
		)
	SELECT
		d.COMMODITY_TYPE_ID
		, d.Bucket_Master_Id
		, d.UBM_BUCKET_CODE
		, d.Ubm_Sub_Bucket_Code
		, d.UBM_UNIT_OF_MEASURE_CODE
		, d.UNIT_OF_MEASURE_TYPE_ID
	FROM
		dbo.CU_INVOICE_DETERMINANT d
		INNER JOIN dbo.Bucket_Master bm
			ON bm.Bucket_Master_Id = d.Bucket_Master_Id
	WHERE
		d.CU_INVOICE_ID = @Cu_Invoice_Id
		AND d.Bucket_Master_Id IS NOT NULL
		AND d.Ubm_Sub_Bucket_Code IS NOT NULL
		AND d.EC_Invoice_Sub_Bucket_Master_Id IS NULL
	GROUP BY
		d.COMMODITY_TYPE_ID
		, d.Bucket_Master_Id
		, d.UBM_BUCKET_CODE
		, d.Ubm_Sub_Bucket_Code
		, d.UBM_UNIT_OF_MEASURE_CODE
		, d.UNIT_OF_MEASURE_TYPE_ID;

	INSERT INTO @Missing_Sub_Bucket_Dtl
		(
			Commodity_Id
			, Bucket_Master_Id
			, Ubm_Bucket_Name
			, Ubm_Sub_Bucket_Name
		)
	SELECT
		d.COMMODITY_TYPE_ID
		, d.Bucket_Master_Id
		, d.UBM_BUCKET_CODE
		, d.Ubm_Sub_Bucket_Code
	FROM
		dbo.CU_INVOICE_CHARGE d
		INNER JOIN dbo.Bucket_Master bm
			ON bm.Bucket_Master_Id = d.Bucket_Master_Id
	WHERE
		d.CU_INVOICE_ID = @Cu_Invoice_Id
		AND d.Bucket_Master_Id IS NOT NULL
		AND d.Ubm_Sub_Bucket_Code IS NOT NULL
		AND d.EC_Invoice_Sub_Bucket_Master_Id IS NULL
	GROUP BY
		d.COMMODITY_TYPE_ID
		, d.Bucket_Master_Id
		, d.UBM_BUCKET_CODE
		, d.Ubm_Sub_Bucket_Code;

	SELECT
		com.Commodity_Name
		, ms.Ubm_Bucket_Name
		, bm.Bucket_Name AS Cbms_Bucket_Name
		, bt.Code_Value AS Bucket_Type
		, ms.Ubm_Sub_Bucket_Name
		, ms.Ubm_Uom_Name
		, uom.ENTITY_NAME AS Cbms_Uom_Name
		, ms.Commodity_Id
		, ms.Bucket_Master_Id
		, ms.Uom_Id
	FROM
		@Missing_Sub_Bucket_Dtl ms
		INNER JOIN dbo.Bucket_Master bm
			ON bm.Bucket_Master_Id = ms.Bucket_Master_Id
		INNER JOIN dbo.Code bt
			ON bt.Code_Id = bm.Bucket_Type_Cd
		INNER JOIN dbo.Commodity com
			ON com.Commodity_Id = ms.Commodity_Id
		LEFT OUTER JOIN dbo.ENTITY uom
			ON uom.ENTITY_ID = ms.Uom_Id;

END;
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Missing_Sub_Bucket_Dtl_Sel] TO [CBMSApplication]
GO
