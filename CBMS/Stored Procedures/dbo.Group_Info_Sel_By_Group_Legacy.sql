SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 dbo.Group_Info_Sel_By_Group_Legacy  
  
DESCRIPTION:  
  To Get Group_Info_Id and Group_Name based on the Group_Legacy_Name_Cd_Value and call this with in the stored procedures 
  
 INPUT PARAMETERS:            
                       
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
@Group_Legacy_Name_Cd_Value  VARCHAR(25)  

  
  
 OUTPUT PARAMETERS:            
                             
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
  
 USAGE EXAMPLES:                              
---------------------------------------------------------------------------------------------------------------                              

      
EXEC dbo.Group_Info_Sel_By_Group_Legacy 
      @Group_Legacy_Name_Cd_Value = 'Regulated_Market'  
      

  
 AUTHOR INITIALS:          
         
 Initials              Name          
---------------------------------------------------------------------------------------------------------------                        
 SP					Sandeep Pigilam  
  
 MODIFICATIONS:        
            
 Initials              Date             Modification        
---------------------------------------------------------------------------------------------------------------        
  SP				2014-09-05		Created for Data Transition.  
  
******/  
  
CREATE PROCEDURE [dbo].[Group_Info_Sel_By_Group_Legacy]
      ( 
       @Group_Legacy_Name_Cd_Value VARCHAR(25) )
AS 
BEGIN  
      SET NOCOUNT ON    
          
      SELECT
            gi.GROUP_INFO_ID
           ,gi.GROUP_NAME
           ,c.Code_Value AS Legacy_Group_Name
      FROM
            dbo.GROUP_INFO gi
            INNER JOIN dbo.Code c
                  ON gi.Group_Legacy_Name_Cd = c.Code_Id
      WHERE
            c.Code_Value = @Group_Legacy_Name_Cd_Value

END 
;
GO
GRANT EXECUTE ON  [dbo].[Group_Info_Sel_By_Group_Legacy] TO [CBMSApplication]
GO
