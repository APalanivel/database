SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Budget_Details_INS

DESCRIPTION:
	Loads records corresponding to 12 months from the budget start date for each account under the budget

INPUT PARAMETERS:
		Name			DataType		Default	Description
------------------------------------------------------------
	@budgetId			VARCHAR(MAX)
	@Start_Dt			DATE
	@End_Dt				DATE
		
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
BEGIN TRAN
	EXEC Budget_Details_INS 8552, '2014-01-01', '2014-12-01'
	SELECT * FROM dbo.Budget_Details WHERE Budget_Account_Id IN (
	SELECT Budget_Account_Id FROM dbo.Budget_Account WHERE Budget_Id = 8552
	)
ROLLBACK

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
 CPE			2014-03-11	Created
							
******/
CREATE PROCEDURE dbo.Budget_Details_INS
      (
       @Budget_Id INT
      ,@Start_Dt DATE
      ,@End_Dt DATE )
AS 
BEGIN

      SET NOCOUNT ON

      INSERT      dbo.BUDGET_DETAILS
                  ( 
                   BUDGET_ACCOUNT_ID
                  ,MONTH_IDENTIFIER )
                  SELECT
                        ba.BUDGET_ACCOUNT_ID
                       ,dd.Date_D
                  FROM
                        dbo.BUDGET_ACCOUNT AS ba
                        CROSS JOIN meta.Date_Dim AS dd
                  WHERE
                        dd.Date_D BETWEEN @Start_Dt AND @End_Dt
                        AND ba.BUDGET_ID = @Budget_Id
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Budget_DetailS
                                         WHERE
                                          ba.BUDGET_ACCOUNT_ID = BUDGET_ACCOUNT_ID
                                          AND DD.Date_D = MONTH_IDENTIFIER )

END
;
GO
GRANT EXECUTE ON  [dbo].[Budget_Details_INS] TO [CBMSApplication]
GO
