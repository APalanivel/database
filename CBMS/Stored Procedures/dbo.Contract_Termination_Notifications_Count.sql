
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** 

NAME:
		dbo.Contract_Termination_Notifications_Count 

DESCRIPTION: 
				To get the termination notifications count

INPUT PARAMETERS: 
Name						DataType		Default		Description 
----------------------------------------------------------------------
@Date						DATETIME


OUTPUT PARAMETERS:   
Name						DataType	Default		Description 
---------------------------------------------------------------------- 

USAGE EXAMPLES:   
----------------------------------------------------------------------   

EXEC dbo.Contract_Termination_Notifications_Count NULL,'2016-06-10'
EXEC dbo.Contract_Termination_Notifications_Count NULL,'2016-06-11'
EXEC dbo.Contract_Termination_Notifications_Count NULL,'2016-06-12'
EXEC dbo.Contract_Termination_Notifications_Count NULL,'2016-06-13'
EXEC dbo.Contract_Termination_Notifications_Count 24991,'2016-06-12'
EXEC dbo.Contract_Termination_Notifications_Count 36119,'2016-06-12'
EXEC dbo.Contract_Termination_Notifications_Count 35697,'2016-06-30'



AUTHOR INITIALS:
	Initials	Name
----------------------------------------------------------------------
	RR			Raghu Reddy
	

MODIFICATIONS:  
	Initials	Date		Modification 
-------------------------------------------------------------------------------   
	RR			2016-06-03  Created  GCS-985 GCS-Phase-5
	RR			2016-07-21	GCS-1099 Performance tuning.
	 	 		 
******/

CREATE PROCEDURE [dbo].[Contract_Termination_Notifications_Count]
      ( 
       @Analyst_Id INT = NULL
      ,@Date DATETIME = NULL )
AS 
BEGIN 

      SET NOCOUNT ON;
      CREATE TABLE #Contract_Commodity
            ( 
             Contract_Id INT
            ,Site_Id INT
            ,Account_Id INT
            ,Account_Vendor_Id INT
            ,Meter_Disassociation_Dt DATE
            ,Client_Id INT
            ,Commodity_Id INT
            ,Client_Commodity_Id INT
            ,Analyst_Mapping_Cd INT );


      DECLARE
            @Total_Count AS INT
           ,@Not_Sent_To_Supp_Count INT;
		
      DECLARE @Tbl_Termination TABLE
            ( 
             Contract_Id INT
            ,Meter_Start_End_Date DATE
            ,Notification_Msg_Queue_Id INT );
            
      DECLARE @Cnt_Termination TABLE
            ( 
             Contract_Id INT
            ,Meter_Start_End_Date DATE
            ,Notification_Msg_Queue_Id INT );
            
      DECLARE
            @Default_Analyst INT
           ,@Custom_Analyst INT
           ,@Commodity_Source INT;

      SELECT
            @Default_Analyst = MAX(CASE WHEN c.Code_Value = 'Default' THEN c.Code_Id
                                   END)
           ,@Custom_Analyst = MAX(CASE WHEN c.Code_Value = 'Custom' THEN c.Code_Id
                                  END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Analyst Type';

      SELECT
            @Commodity_Source = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON cs.Codeset_Id = c.Codeset_Id
      WHERE
            cs.Codeset_Name = 'CommodityServiceSource'
            AND c.Code_Value = 'Invoice';       
            
      INSERT      INTO @Tbl_Termination
                  ( 
                   Contract_Id
                  ,Meter_Start_End_Date
                  ,Notification_Msg_Queue_Id )
                  SELECT
                        fvl.Contract_Id
                       ,fvl.Meter_Term_Dt AS Meter_Start_End_Date
                       ,MAX(fvl.Notification_Msg_Queue_Id) AS Notification_Msg_Queue_Id
                  FROM
                        dbo.Contract_Notification_Log fvl
                        INNER JOIN dbo.Notification_Msg_Queue nmq
                              ON fvl.Notification_Msg_Queue_Id = nmq.Notification_Msg_Queue_Id
                        INNER JOIN dbo.Notification_Template nt
                              ON nmq.Notification_Template_Id = nt.Notification_Template_Id
                        INNER JOIN dbo.Code cd
                              ON cd.Code_Id = nt.Notification_Type_Cd
                        INNER JOIN dbo.Codeset cs
                              ON cd.Codeset_Id = cd.Codeset_Id
                  WHERE
                        cd.Code_Value IN ( 'Termination Analyst', 'Termination Vendor' )
                        AND cs.Codeset_Name = 'Notification Type'
                        AND ( CONVERT(DATE, fvl.Last_Change_Ts, 101) BETWEEN CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD, -90, @Date), 101))
                                                                     AND     @Date )
                  GROUP BY
                        fvl.Contract_Id
                       ,fvl.Meter_Term_Dt;

      INSERT      INTO #Contract_Commodity
                  ( 
                   Contract_Id
                  ,Site_Id
                  ,Account_Id
                  ,Account_Vendor_Id
                  ,Meter_Disassociation_Dt
                  ,Client_Id
                  ,Commodity_Id
                  ,Analyst_Mapping_Cd )
                  SELECT
                        saam.Contract_ID
                       ,ch.Site_Id
                       ,ucha.Account_Id
                       ,ucha.Account_Vendor_Id
                       ,saam.METER_DISASSOCIATION_DATE
                       ,ch.Client_Id
                       ,ucha.Commodity_Id
                       ,COALESCE(ucha.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd)
                  FROM
                        @Tbl_Termination t
                        INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP saam
                              ON saam.Contract_ID = t.Contract_Id
                                 AND saam.METER_DISASSOCIATION_DATE = t.Meter_Start_End_Date
                        INNER JOIN Core.Client_Hier_Account ucha
                              ON ucha.Meter_Id = saam.METER_ID
                        INNER JOIN Core.Client_Hier ch
                              ON ch.Client_Hier_Id = ucha.Client_Hier_Id
                  WHERE
                        ucha.Account_Type = 'Utility'
                  GROUP BY
                        saam.Contract_ID
                       ,ch.Site_Id
                       ,ucha.Account_Id
                       ,ucha.Account_Vendor_Id
                       ,saam.METER_DISASSOCIATION_DATE
                       ,ch.Client_Id
                       ,ucha.Commodity_Id
                       ,COALESCE(ucha.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd);

      UPDATE
            con
      SET   
            Client_Commodity_Id = cc.Client_Commodity_Id
      FROM
            #Contract_Commodity con
            INNER JOIN Core.Client_Commodity cc
                  ON cc.Client_Id = con.Client_Id
                     AND cc.Commodity_Id = con.Commodity_Id
      WHERE
            cc.Commodity_Service_Cd = @Commodity_Source;

      INSERT      INTO @Cnt_Termination
                  ( 
                   Contract_Id
                  ,Meter_Start_End_Date
                  ,Notification_Msg_Queue_Id )
                  SELECT
                        x.Contract_Id
                       ,x.Meter_Start_End_Date
                       ,x.Notification_Msg_Queue_Id
                  FROM
                        ( SELECT
                              tt.Contract_Id
                             ,tt.Meter_Start_End_Date
                             ,tt.Notification_Msg_Queue_Id
                             ,CASE WHEN con.Analyst_Mapping_Cd = @Default_Analyst THEN vcam.Analyst_Id
                                   WHEN con.Analyst_Mapping_Cd = @Custom_Analyst THEN COALESCE(aca.Analyst_User_Info_Id, sca.Analyst_User_Info_Id, cca.Analyst_User_Info_Id)
                              END AS Analyst_Id
                          FROM
                              #Contract_Commodity con
                              INNER JOIN @Tbl_Termination tt
                                    ON tt.Contract_Id = con.Contract_Id
                                       AND tt.Meter_Start_End_Date = con.Meter_Disassociation_Dt
                              LEFT OUTER JOIN ( dbo.VENDOR_COMMODITY_MAP vcm
                                                INNER JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                                                      ON vcm.VENDOR_COMMODITY_MAP_ID = vcam.Vendor_Commodity_Map_Id )
                                                ON vcm.VENDOR_ID = con.Account_Vendor_Id
                                                   AND vcm.COMMODITY_TYPE_ID = con.Commodity_Id
                              LEFT JOIN dbo.Account_Commodity_Analyst aca
                                    ON aca.Account_Id = con.Account_Id
                                       AND aca.Commodity_Id = con.Commodity_Id
                              LEFT JOIN dbo.Site_Commodity_Analyst sca
                                    ON sca.Site_Id = con.Site_Id
                                       AND sca.Commodity_Id = con.Commodity_Id
                              LEFT JOIN dbo.Client_Commodity_Analyst cca
                                    ON cca.Client_Commodity_Id = con.Client_Commodity_Id
                          GROUP BY
                              tt.Contract_Id
                             ,tt.Meter_Start_End_Date
                             ,tt.Notification_Msg_Queue_Id
                             ,CASE WHEN con.Analyst_Mapping_Cd = @Default_Analyst THEN vcam.Analyst_Id
                                   WHEN con.Analyst_Mapping_Cd = @Custom_Analyst THEN COALESCE(aca.Analyst_User_Info_Id, sca.Analyst_User_Info_Id, cca.Analyst_User_Info_Id)
                              END ) x
                  WHERE
                        ( @Analyst_Id IS NULL
                          OR x.Analyst_Id = @Analyst_Id )
                  GROUP BY
                        x.Contract_Id
                       ,x.Meter_Start_End_Date
                       ,x.Notification_Msg_Queue_Id;

--/*

      SELECT
            @Total_Count = COUNT(1)
      FROM
            @Cnt_Termination;
           
      SELECT
            @Not_Sent_To_Supp_Count = COUNT(1)
      FROM
            @Cnt_Termination tt
            INNER JOIN dbo.Notification_Msg_Queue nmq
                  ON tt.Notification_Msg_Queue_Id = nmq.Notification_Msg_Queue_Id
            INNER JOIN dbo.Notification_Template nt
                  ON nmq.Notification_Template_Id = nt.Notification_Template_Id
            INNER JOIN dbo.Code cd
                  ON cd.Code_Id = nt.Notification_Type_Cd
            INNER JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cd.Codeset_Id
      WHERE
            cd.Code_Value = 'Termination Analyst'
            AND cs.Codeset_Name = 'Notification Type'
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              dbo.Contract_Notification_Log fvl
                              INNER JOIN dbo.Contract_Notification_Log_Meter_Dtl fvmd
                                    ON fvl.Contract_Notification_Log_Id = fvmd.Contract_Notification_Log_Id
                              INNER JOIN dbo.Notification_Msg_Queue nmq
                                    ON fvl.Notification_Msg_Queue_Id = nmq.Notification_Msg_Queue_Id
                              INNER JOIN dbo.Notification_Template nt
                                    ON nmq.Notification_Template_Id = nt.Notification_Template_Id
                              INNER JOIN dbo.Code cd
                                    ON cd.Code_Id = nt.Notification_Type_Cd
                              INNER JOIN dbo.Codeset cs
                                    ON cd.Codeset_Id = cd.Codeset_Id
                             WHERE
                              cs.Codeset_Name = 'Notification Type'
                              AND cd.Code_Value = 'Termination Vendor'
                              AND tt.Contract_Id = fvl.Contract_Id
                              AND tt.Meter_Start_End_Date = fvl.Meter_Term_Dt );

      SELECT
            @Total_Count AS Total_Count
           ,@Not_Sent_To_Supp_Count AS Not_Sent_To_Supp_Count;
   
--*/

      DROP TABLE #Contract_Commodity;
END;

;
GO

GRANT EXECUTE ON  [dbo].[Contract_Termination_Notifications_Count] TO [CBMSApplication]
GO
