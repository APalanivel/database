SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.Client_Hier_DMO_Config_Del
           
DESCRIPTION:             
			To delete DMO configurations
			
INPUT PARAMETERS:            
	Name						DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Client_Hier_DMO_Config_Id	INT
    @Client_Hier_Id				INT
    @User_Info_Id				INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT TOP 10 * FROM dbo.Client_Hier_DMO_Config
            
	BEGIN TRANSACTION
		DECLARE @Client_Hier_DMO_Config_Id INT = NULL
		
		SELECT * FROM  dbo.Client_Hier_DMO_Config 
			WHERE Client_Hier_Id =56 AND Commodity_Id = 290 AND DMO_Start_Dt= '2017-1-1' AND DMO_End_Dt='2017-12-31'
		EXEC dbo.Client_Hier_DMO_Config_Ins_Upd 56,290,'2017-1-1','2017-12-31',@Client_Hier_DMO_Config_Id,16
		SELECT * FROM  dbo.Client_Hier_DMO_Config 
			WHERE Client_Hier_Id =56 AND Commodity_Id = 290 AND DMO_Start_Dt= '2017-1-1' AND DMO_End_Dt='2017-12-31'
		EXEC dbo.Client_Hier_DMO_Config_Sel 56
		SELECT @Client_Hier_DMO_Config_Id = Client_Hier_DMO_Config_Id FROM  dbo.Client_Hier_DMO_Config 
			WHERE Client_Hier_Id =56 AND Commodity_Id = 290 AND DMO_Start_Dt= '2017-1-1' AND DMO_End_Dt='2017-12-31'
		SELECT @Client_Hier_DMO_Config_Id as NewRecord
		EXEC dbo.Client_Hier_DMO_Config_Del @Client_Hier_DMO_Config_Id,56,16
		SELECT * FROM  dbo.Client_Hier_DMO_Config 
			WHERE Client_Hier_Id =56 AND Commodity_Id = 290 AND DMO_Start_Dt= '2017-1-1' AND DMO_End_Dt='2017-12-31'
		EXEC dbo.Client_Hier_DMO_Config_Sel 56
	ROLLBACK TRANSACTION
	
	BEGIN TRANSACTION
		DECLARE @Client_Hier_DMO_Config_Id INT = NULL
		
		SELECT * FROM  dbo.Client_Hier_DMO_Config 
			WHERE Client_Hier_Id IN(56,5429) AND Commodity_Id = 290 AND DMO_Start_Dt= '2017-1-1' AND DMO_End_Dt='2017-12-31'
		EXEC dbo.Client_Hier_DMO_Config_Ins_Upd 56,290,'2017-1-1','2017-12-31',@Client_Hier_DMO_Config_Id,16
		SELECT * FROM  dbo.Client_Hier_DMO_Config 
			WHERE Client_Hier_Id IN(56,5429) AND Commodity_Id = 290 AND DMO_Start_Dt= '2017-1-1' AND DMO_End_Dt='2017-12-31'
		
		SELECT @Client_Hier_DMO_Config_Id = Client_Hier_DMO_Config_Id FROM  dbo.Client_Hier_DMO_Config 
			WHERE Client_Hier_Id =56 AND Commodity_Id = 290 AND DMO_Start_Dt= '2017-1-1' AND DMO_End_Dt='2017-12-31'
		SELECT @Client_Hier_DMO_Config_Id as NewRecord
		EXEC dbo.Client_Hier_DMO_Config_Del @Client_Hier_DMO_Config_Id,5429,16
		SELECT * FROM  dbo.Client_Hier_DMO_Config 
			WHERE Client_Hier_Id IN(56,5429) AND Commodity_Id = 290 AND DMO_Start_Dt= '2017-1-1' AND DMO_End_Dt='2017-12-31'
		EXEC dbo.Client_Hier_DMO_Config_Sel 56
		EXEC dbo.Client_Hier_DMO_Config_Sel 5429
		SELECT * FROM dbo.Client_Hier_Not_Applicable_DMO_Config WHERE Client_Hier_Id IN(56,5429)
	ROLLBACK TRANSACTION
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-01-20	Contract placeholder - CP-4 Created
******/

CREATE PROCEDURE [dbo].[Client_Hier_DMO_Config_Del]
      ( 
       @Client_Hier_DMO_Config_Id INT
      ,@Client_Hier_Id INT
      ,@User_Info_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE
            @Entity_Hier_Level_Cd INT
           ,@Config_Hier_Level_Cd INT
      
      SELECT
            @Entity_Hier_Level_Cd = Hier_level_Cd
      FROM
            Core.Client_Hier
      WHERE
            Client_Hier_Id = @Client_Hier_Id
            
      SELECT
            @Config_Hier_Level_Cd = ch.Hier_level_Cd
      FROM
            Core.Client_Hier ch
            INNER JOIN dbo.Client_Hier_DMO_Config chdc
                  ON ch.Client_Hier_Id = chdc.Client_Hier_Id
      WHERE
            chdc.Client_Hier_DMO_Config_Id = @Client_Hier_DMO_Config_Id
            
      INSERT      INTO dbo.Client_Hier_Not_Applicable_DMO_Config
                  ( 
                   Client_Hier_Id
                  ,Client_Hier_DMO_Config_Id
                  ,Created_User_Id
                  ,Created_Ts )
                  SELECT
                        @Client_Hier_Id
                       ,@Client_Hier_DMO_Config_Id
                       ,@User_Info_Id
                       ,GETDATE()
                  WHERE
                        @Entity_Hier_Level_Cd <> @Config_Hier_Level_Cd
      
      DELETE FROM
            dbo.Account_Not_Applicable_DMO_Config
      WHERE
            Client_Hier_DMO_Config_Id = @Client_Hier_DMO_Config_Id
            AND @Entity_Hier_Level_Cd = @Config_Hier_Level_Cd
      
      DELETE FROM
            dbo.Client_Hier_Not_Applicable_DMO_Config
      WHERE
            Client_Hier_DMO_Config_Id = @Client_Hier_DMO_Config_Id
            AND @Entity_Hier_Level_Cd = @Config_Hier_Level_Cd
            
      DELETE FROM
            dbo.Client_Hier_DMO_Config
      WHERE
            Client_Hier_DMO_Config_Id = @Client_Hier_DMO_Config_Id
            AND @Entity_Hier_Level_Cd = @Config_Hier_Level_Cd
      
      
END;
;


;
GO
GRANT EXECUTE ON  [dbo].[Client_Hier_DMO_Config_Del] TO [CBMSApplication]
GO
