
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
Name:  
 dbo.SR_RFP_UPLOAD_SOP_P
 
 Description:  
 			
 
 Input Parameters:  
	Name					DataType			Default					Description  
-------------------------------------------------------------------------------------   
	@accountBidGroupId		INT
	@isBidGroup				INT
	@uploadedById			INT
	@cbmsImageId			INT
 
 Output Parameters:  
	Name					DataType			Default					Description  
-------------------------------------------------------------------------------------   
 
	Usage Examples:
------------------------------------------------------------------------------------- 
	SELECT TOP 10 * FROM dbo.SR_RFP_SOP_SMR
	BEGIN TRAN
		SELECT * FROM dbo.SR_RFP_SOP_SMR WHERE SR_ACCOUNT_GROUP_ID = 39 AND IS_BID_GROUP = 1 AND CBMS_IMAGE_ID = 111000
		EXEC dbo.SR_RFP_UPLOAD_SOP_P 39,1,16,111000
		SELECT * FROM dbo.SR_RFP_SOP_SMR WHERE SR_ACCOUNT_GROUP_ID = 39 AND IS_BID_GROUP = 1 AND CBMS_IMAGE_ID = 111000
	ROLLBACK TRAN

Author Initials:  
	Initials	Name
-------------------------------------------------------------------------------------   
	RR			Raghu Reddy
 
 Modifications :  
	Initials		Date	Modification  
-------------------------------------------------------------------------------------   
	RR			2016-02-29	Global Sourcing Phase-3 GCS-432 - Removed input parameter @docType, no more SOP type docuement type, all
							the documents type will be SMR 
      
******/
CREATE PROCEDURE [dbo].[SR_RFP_UPLOAD_SOP_P]
      ( 
       @accountBidGroupId INT
      ,@isBidGroup INT
      ,@uploadedById INT
      ,@cbmsImageId INT )
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE
            @entityId INT
           ,@DateNow DATETIME

      SELECT
            @entityId = ENTITY_ID
      FROM
            dbo.Entity(NOLOCK)
      WHERE
            Entity_Name = 'SMR'
            AND Entity_Type = 100	

      IF @accountBidGroupId = 0 
            BEGIN

                  UPDATE
                        dbo.CBMS_IMAGE
                  SET   
                        CBMS_IMAGE_TYPE_ID = @entityId
                  WHERE
                        CBMS_IMAGE_ID = @cbmsImageId
		
	    
            END
      ELSE 
            BEGIN

                  INSERT      INTO dbo.SR_RFP_SOP_SMR
                              ( 
                               CBMS_IMAGE_ID
                              ,SR_ACCOUNT_GROUP_ID
                              ,IS_BID_GROUP
                              ,UPLOADED_DATE
                              ,UPLOADED_BY )
                  VALUES
                              ( 
                               @cbmsImageId
                              ,@accountBidGroupId
                              ,@isBidGroup
                              ,getdate()
                              ,@uploadedById )


                  SET @DateNow = getdate()
                  IF @isBidGroup = 0 
                        BEGIN

                              UPDATE
                                    dbo.SR_RFP_CHECKLIST
                              SET   
                                    SENT_RECO_DATE = @DateNow
                              WHERE
                                    SR_RFP_ACCOUNT_ID = @accountBidGroupId

                        END
                  ELSE 
                        IF @isBidGroup = 1 
                              BEGIN

                                    UPDATE
                                          rfpAcctList
                                    SET   
                                          rfpAcctList.SENT_RECO_DATE = @DateNow
                                    FROM
                                          dbo.SR_RFP_CHECKLIST rfpAcctList
                                          INNER JOIN dbo.SR_RFP_ACCOUNT rfpAcct
                                                ON rfpAcct.SR_RFP_ACCOUNT_ID = rfpAcctList.SR_RFP_ACCOUNT_ID
                                    WHERE
                                          rfpAcct.SR_RFP_BID_GROUP_ID = @accountBidGroupId

                              END

            END
  
END


;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_UPLOAD_SOP_P] TO [CBMSApplication]
GO
