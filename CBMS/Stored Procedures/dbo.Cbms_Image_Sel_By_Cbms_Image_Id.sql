SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/******
NAME:	dbo.Cbms_Image_Sel_By_Cbms_Image_Id

DESCRIPTION:
	Check if the Vendor name exists in RA

INPUT PARAMETERS:
	Name				DataType		Default				Description
------------------------------------------------------------
	@Cbms_Image_Id		INT


OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
Cbms_Image_Sel_By_Cbms_Image_Id 


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SS			Sani
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	SS			2017/02/03	Created
	
******/

CREATE PROCEDURE [dbo].[Cbms_Image_Sel_By_Cbms_Image_Id]
      ( 
       @Cbms_Image_Id INT
       )
AS 
BEGIN
      SET NOCOUNT ON;

SELECT CBMS_DOC_ID AS UpdFileName
FROM cbms_image
WHERE CBMS_IMAGE_ID = @Cbms_Image_Id
						
END						


;
GO
GRANT EXECUTE ON  [dbo].[Cbms_Image_Sel_By_Cbms_Image_Id] TO [CBMSApplication]
GO
