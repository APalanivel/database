SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE dbo.UPDATE_BATCH_CONFIGURATION_DETAILS_P


@userId varchar(10),
@sessionId varchar(20),
@masterId int,
@reportName varchar (200),
@siteId int,
@divisionId int,
@clientId int

as
	set nocount on
declare @batchConfigId int, @reportId int

select @reportId=report_list_id from report_list where 
	report_name =@reportName

select @batchConfigId=RM_BATCH_CONFIGURATION_ID from RM_BATCH_CONFIGURATION
where RM_BATCH_CONFIGURATION_MASTER_ID=@masterId and report_list_id=@reportId
	and client_id=@clientId



	insert into RM_BATCH_CONFIGURATION_DETAILS 
	(
		RM_BATCH_CONFIGURATION_ID,
		SITE_ID,
		DIVISION_ID
	)
	values
	(
		@batchConfigId,
		@siteId,
		@divisionId
	)
GO
GRANT EXECUTE ON  [dbo].[UPDATE_BATCH_CONFIGURATION_DETAILS_P] TO [CBMSApplication]
GO
