SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******    
Name:
	dbo.Cost_Usage_Account_Detailed_Summary_By_Client_Hier_Account_Commodity

 Description:
	Used to fetch data for the new account level detailed report

 Input Parameters:
    Name			    DataType      Default			Description
------------------------------------------------------------------------

    @Client_Hier_Id     INT
    @Account_Id			INT
    @Commodity_Id	    INT
    @Start_Dt			DATE
    @End_Dt				DATE
    @Uom_Type_Id	    INT
    @Currency_Unit_Id   INT

 Output Parameters:
	Name			    DataType      Default			Description
------------------------------------------------------------------------

 Usage Examples:
------------------------------------------------------------------------
DECLARE @Tvp Tvp_Bucket    
INSERT      INTO @Tvp
VALUES
            ( 70, 25 )     
,           ( 102517, 20 )
,           ( 101700, 1545 )  
,           ( 71, 25 ) 
,			(110,NULL)

EXEC dbo.Cost_Usage_Account_Detailed_Summary_By_Client_Hier_Account_Commodity 
      @Client_Hier_Id = 17654
     ,@Account_Id = 75926
     ,@Commodity_Id = 291
     ,@Start_Dt = '2011-01-01'
     ,@End_Dt = '2011-12-01'
     ,@Currency_Unit_Id = 3
     ,@Tvp_Bucket_Uom = @Tvp

DECLARE @Tvp Tvp_Bucket    
INSERT      INTO @Tvp
VALUES
            ( 70, 25 )     
,           ( 102517, 20 )
,           ( 101700, 1545 )  
,           ( 71, 25 ) 
EXEC dbo.Cost_Usage_Account_Detailed_Summary_By_Client_Hier_Account_Commodity 
      @Client_Hier_Id = 17654
     ,@Account_Id = 75926
     ,@Commodity_Id = 291
     ,@Start_Dt = '2014-01-01'
     ,@End_Dt = '2014-12-01'
     ,@Currency_Unit_Id = 3
     ,@Tvp_Bucket_Uom = @Tvp


DECLARE @Tvp Tvp_Bucket    
INSERT      INTO @Tvp
VALUES
            ( 100589, 1660 )     

			,
            ( 100598, NULL )
EXEC dbo.Cost_Usage_Account_Detailed_Summary_By_Client_Hier_Account_Commodity 
      @Client_Hier_Id = 17654
     ,@Account_Id = 339575
     ,@Commodity_Id = 100028
     ,@Start_Dt = '2014-01-01'
     ,@End_Dt = '2014-12-01'
     ,@Currency_Unit_Id = 3
     ,@Tvp_Bucket_Uom = @Tvp
     
     SELECT * FROM Commodity WHERE Commodity_id = 100028

Author Initials:
 Initials	Name  
------------------------------------------------------------------------
 HG			Harihara Suthan G
 NR			Narayana Reddy
		   
 Modifications :    
 Initials   Date	    Modification    
------------------------------------------------------------------------
 HG			2014-07-24  Created for data ops phase-3 enh
 NR			2015-07-29	MAINT-3533 Added Has_Failed_Variance flag column and it returned based on the records available 
						in Variance_Log table for the given account , commodity service month combination
						MAINT-3534 Added  a new column Reported_Invoice_Cnt  to  populated based on the number 
						of distinct reported and processed invoices for the given Account.
SP			2017-08-23	Data estimations,added Data_Type in select list.						

******/
CREATE PROCEDURE [dbo].[Cost_Usage_Account_Detailed_Summary_By_Client_Hier_Account_Commodity]
      (
       @Client_Hier_Id INT
      ,@Account_Id INT
      ,@Commodity_Id INT
      ,@Start_Dt DATE
      ,@End_Dt DATE
      ,@Currency_Unit_Id INT
      ,@Tvp_Bucket_Uom Tvp_Bucket READONLY )
AS
BEGIN

      SET NOCOUNT ON

      DECLARE @currency_Group_Id INT
  
      DECLARE @Bucket_List TABLE
            ( 
             Bucket_Master_Id INT PRIMARY KEY CLUSTERED
            ,Bucket_Name VARCHAR(255)
            ,Default_uom_Type_Id INT
            ,Bucket_Type_cd INT
            ,Uom_Type_Id INT )
            
                     
      DECLARE @Invoice_Count TABLE
            ( 
             Account_Id INT
            ,Service_Month DATE
            ,Reported_Invoice_Cnt INT )   

      SELECT
            @currency_Group_id = ch.Client_currency_Group_Id
      FROM
            Core.Client_Hier ch
      WHERE
            ch.Client_Hier_Id = @Client_Hier_Id

      INSERT      INTO @Bucket_List
                  ( 
					Bucket_Master_Id
                  ,Bucket_Name
                  ,Default_uom_Type_Id
                  ,Bucket_Type_cd
                  ,Uom_Type_Id )
                  SELECT
                        BM.Bucket_Master_Id
                       ,BM.Bucket_Name
                       ,BM.Default_uom_Type_Id
                       ,BM.Bucket_Type_cd
                       ,tvp.Uom_Id
                  FROM
                        dbo.Bucket_Master BM
                        INNER JOIN @Tvp_Bucket_Uom tvp
                              ON tvp.Bucket_Master_Id = BM.Bucket_Master_Id
                  WHERE
                        BM.Commodity_Id = @Commodity_Id
                        AND BM.Is_Shown_On_Account = 1
                        AND BM.Is_Active = 1;

      INSERT      INTO @Invoice_Count
                  ( 
                   Account_Id
                  ,Service_Month
                  ,Reported_Invoice_Cnt )
                  SELECT
                        cism.Account_Id
                       ,cism.SERVICE_MONTH
                       ,COUNT(DISTINCT cism.CU_INVOICE_ID)
                  FROM
                        dbo.CU_INVOICE_SERVICE_MONTH cism
                        INNER JOIN dbo.CU_INVOICE ci
                              ON cism.CU_INVOICE_ID = ci.CU_INVOICE_ID
                  WHERE
                        cism.Account_ID = @Account_Id
                        AND ci.IS_PROCESSED = 1
                        AND ci.IS_REPORTED = 1
                        AND ci.IS_DNT = 0
                        AND ci.IS_Duplicate = 0
                        AND cism.SERVICE_MONTH BETWEEN @Start_Dt AND @End_Dt
                  GROUP BY
                        cism.Account_Id
                       ,cism.SERVICE_MONTH

      SELECT
            bl.Bucket_Name
           ,bt.Code_Value AS Bucket_Type
           ,sm.Date_D AS Service_Month
           ,sm.Month_Num
           ,( CASE WHEN bt.Code_Value = 'Charge' THEN cu.currency_Unit_Name
                   WHEN bt.Code_Value = 'Determinant' THEN uom.Entity_Name
              END ) AS uom_Name
           ,( CASE WHEN bt.Code_Value = 'Charge' THEN cuad.Bucket_Value * isnull(curconv.Conversion_Factor, 0)
                   WHEN bt.Code_Value = 'Determinant' THEN cuad.Bucket_Value * isnull(uomconv.Conversion_Factor, 0)
                   ELSE 0
              END ) Bucket_Value
           ,cuad.Data_Source_Cd
           ,dsc.Code_Value AS Data_Source
           ,cast(ip.IS_EXPECTED AS INT) AS IP_Expected_Cnt
           ,cast(ip.IS_RECEIVED AS INT) AS IP_Received_Cnt
           ,bd.Billing_Start_Dt
           ,bd.Billing_End_Dt
           ,bd.Billing_Days
           ,CASE WHEN vl.Variance_Log_Id IS NOT NULL THEN 1
                 ELSE 0
            END AS Has_Failed_Variance
           ,isnull(ic.Reported_Invoice_Cnt, 0) AS Reported_Invoice_Cnt
           ,dtc.Code_Value AS Data_Type,
		  CASE WHEN exists (SELECT 1 FROM dbo.Variance_Account_Month_Anomalous_Status a WHERE a.Account_Id =@Account_Id 
				AND a.Service_Month = sm.DATE_D AND a.Is_Anomalous =1)
				THEN 1 
				ELSE 0
				END AS IS_anomalous
      FROM
            @Bucket_List BL
            INNER JOIN dbo.Code bt
                  ON bt.Code_Id = bl.Bucket_Type_cd
            CROSS JOIN meta.Date_Dim sm
            LEFT OUTER JOIN ( dbo.Cost_Usage_Account_Dtl cuad
                              INNER JOIN dbo.Code dsc
                                    ON dsc.Code_Id = cuad.Data_Source_Cd )
                              ON cuad.Bucket_Master_Id = bl.Bucket_Master_Id
                                 AND sm.Date_D = cuad.Service_Month
                                 AND cuad.Account_Id = @Account_Id
                                 AND cuad.Client_Hier_Id = @Client_Hier_Id
            LEFT OUTER JOIN dbo.currency_Unit_Conversion curconv
                  ON curconv.Base_Unit_Id = cuad.currency_Unit_Id
                     AND curconv.Conversion_Date = sm.Date_D
                     AND curconv.currency_Group_Id = @currency_Group_Id
                     AND curconv.Converted_Unit_Id = @currency_Unit_Id
            LEFT OUTER JOIN dbo.Consumption_Unit_Conversion uomconv
                  ON uomconv.Base_Unit_Id = cuad.uom_Type_Id
                     AND uomconv.Converted_Unit_Id = bl.Uom_Type_Id
            LEFT OUTER JOIN dbo.INVOICE_PARTICIPATION ip
                  ON ip.Client_Hier_Id = @Client_Hier_Id
                     AND ip.ACCOUNT_ID = @Account_Id
                     AND ip.SERVICE_MONTH = sm.DATE_D
            LEFT OUTER JOIN dbo.Cost_Usage_Account_Billing_Dtl bd
                  ON bd.Account_Id = @Account_Id
                     AND bd.Service_Month = sm.DATE_D
            LEFT OUTER JOIN dbo.currency_Unit cu
                  ON cu.currency_Unit_Id = @currency_Unit_Id
            LEFT OUTER JOIN dbo.Entity uom
                  ON uom.Entity_Id = bl.Uom_Type_Id
            LEFT OUTER JOIN dbo.Variance_Log vl
                  ON sm.Date_D = vl.Service_Month
                     AND vl.Account_ID = @Account_Id
                     AND vl.Commodity_ID = @Commodity_Id
                     AND vl.Partition_Key IS NULL
            LEFT OUTER JOIN @Invoice_Count ic
                  ON ic.SERVICE_MONTH = sm.DATE_D
            LEFT JOIN dbo.Code dtc
                  ON dtc.Code_Id = cuad.Data_Type_Cd
      WHERE
            sm.DATE_D BETWEEN @Start_Dt AND @End_Dt
      GROUP BY
            bl.Bucket_Name
           ,bt.Code_Value
           ,sm.Date_D
           ,sm.Month_Num
           ,( CASE WHEN bt.Code_Value = 'Charge' THEN cu.currency_Unit_Name
                   WHEN bt.Code_Value = 'Determinant' THEN uom.Entity_Name
              END )
           ,( CASE WHEN bt.Code_Value = 'Charge' THEN cuad.Bucket_Value * isnull(curconv.Conversion_Factor, 0)
                   WHEN bt.Code_Value = 'Determinant' THEN cuad.Bucket_Value * isnull(uomconv.Conversion_Factor, 0)
                   ELSE 0
              END )
           ,cuad.Data_Source_Cd
           ,dsc.Code_Value
           ,cast(ip.IS_EXPECTED AS INT)
           ,cast(ip.IS_RECEIVED AS INT)
           ,bd.Billing_Start_Dt
           ,bd.Billing_End_Dt
           ,bd.Billing_Days
           ,CASE WHEN vl.Variance_Log_Id IS NOT NULL THEN 1
                 ELSE 0
            END
           ,isnull(ic.Reported_Invoice_Cnt, 0)
           ,dtc.Code_Value
      ORDER BY
            bt.Code_Value
           ,bl.Bucket_Name
           ,Service_Month 

END
;
;
GO


GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Detailed_Summary_By_Client_Hier_Account_Commodity] TO [CBMSApplication]
GO
