SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  
/******  
NAME:  
 [DBO].[Invoice_Participation_Make_Not_Expected_UPD]  
  
DESCRIPTION:  
 UPDATES INVOICE_PARTICIPATION TABLE WHEN NOT EXPECTED IS CHECKED from UI.
  
INPUT PARAMETERS:  
NAME   DATATYPE DEFAULT  DESCRIPTION  
------------------------------------------------------------  
@ACCOUNT_ID  INT      
  
OUTPUT PARAMETERS:  
NAME   DATATYPE DEFAULT  DESCRIPTION  
------------------------------------------------------------  
  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
  
   
AUTHOR INITIALS:  
INITIALS NAME  
------------------------------------------------------------  
MGB   Bhaskaran Gopalakrishnan  
  
MODIFICATIONS  
INITIALS DATE  MODIFICATION  
------------------------------------------------------------  
MGB   12/10/2009 Created  
*/  
  
  
CREATE PROCEDURE dbo.Invoice_Participation_Make_Not_Expected_UPD
	@ACCOUNT_ID INT
AS
BEGIN

	SET NOCOUNT ON

	UPDATE dbo.INVOICE_PARTICIPATION
		SET IS_EXPECTED = 0
	WHERE
		ACCOUNT_ID = @ACCOUNT_ID

END
GO
GRANT EXECUTE ON  [dbo].[Invoice_Participation_Make_Not_Expected_UPD] TO [CBMSApplication]
GO
