SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*****
        
NAME: [dbo].[Report_DE_Budget_Info] 
    
DESCRIPTION:        
	used to get the details of vendors
       
INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
    
USAGE EXAMPLES:
------------------------------------------------------------
EXEC [dbo].[Report_DE_Budget_Info]'2015'
EXEC [dbo].[Report_DE_Budget_Info]'2015','3'
    
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
LEC		Lynn Cox
     
MAINTENANCE LOG:        
Initials	Date		Modification 
--- ---------- ----------------------------------------------        
  LEC    2016-09-08  Created for Reg markets managers to be able to see Budget details for all analysts' queues.
******/
CREATE PROCEDURE [dbo].[Report_DE_Budget_Info]
      (
       @Budget_Initiated_Year INT = NULL
      ,@Country_Id_List VARCHAR(MAX)
      ,@state_Id_List VARCHAR(MAX)
      ,@Budget_DueDate_Year INT = NULL
      ,@Budget_RatesDoneYear INT = NULL
      ,@Budget_SOURCINGDoneYear INT = NULL )
AS
BEGIN        
    
      SET NOCOUNT ON;    
      
  
      
      WITH  Budget_Level_Data_CTE
              AS ( SELECT DISTINCT
                        b.BUDGET_ID [BID]
                       ,b.DATE_INITIATED [Date budget initiated]
                       ,b.DUE_DATE [Date budget due]
                       ,ba.RATES_COMPLETED_DATE [Date rates completed]
                       ,ui.FIRST_NAME + ' ' + ui.LAST_NAME [Rates completed by]
                       ,ba.SOURCING_COMPLETED_DATE [Date sourcing completed]
                       ,ui1.FIRST_NAME + ' ' + ui1.LAST_NAME [Sourcing completed by]
                       ,ba.RATES_REVIEWED_DATE [Date budget reviewed]
                       ,[Original or Reforecast] = CASE WHEN ba.IS_REFORECAST = 1 THEN 'Reforecast'
                                                        ELSE 'Original'
                                                   END
                       ,[Cannot Perform] = CASE WHEN ba.CANNOT_PERFORMED_DATE IS NOT NULL THEN 'Y'
                                                ELSE 'N'
                                           END
                       ,ba.ACCOUNT_ID [AID]
                       ,b.COMMODITY_TYPE_ID [CID]
                       ,b.BUDGET_NAME [Budget]
                   FROM
                        dbo.BUDGET b
                        JOIN dbo.BUDGET_ACCOUNT ba
                              ON ba.BUDGET_ID = b.BUDGET_ID
                        LEFT JOIN dbo.USER_INFO ui
                              ON ui.USER_INFO_ID = ba.RATES_COMPLETED_BY
                        LEFT JOIN USER_INFO ui1
                              ON ui1.USER_INFO_ID = ba.SOURCING_COMPLETED_BY
                        INNER JOIN Code cd
                              ON cd.Code_Id = b.Analyst_Queue_Display_Cd
                   WHERE
                        cd.Code_Value != 'no' -- This is to only pull budgets that have been sent to an analyst queue
                        AND ( YEAR(b.DATE_INITIATED) = @Budget_Initiated_Year
                              OR @Budget_Initiated_Year IS NULL )
                        AND ( YEAR(b.DUE_DATE) = @Budget_DueDate_Year
                              OR @Budget_DueDate_Year IS NULL )
                        AND ( YEAR(ba.RATES_COMPLETED_DATE) = @Budget_RatesDoneYear
                              OR @Budget_RatesDoneYear IS NULL )
                        AND ( YEAR(ba.SOURCING_COMPLETED_DATE) = @Budget_SOURCINGDoneYear
                              OR @Budget_SOURCINGDoneYear IS NULL )),
            Client_CEM_List_CTE
              AS ( SELECT
                        c.CLIENT_NAME
                       ,LEFT(CEM_Names, LEN(CEM_Names) - 1) CEM
                       ,c.CLIENT_ID
                   FROM
                        dbo.CLIENT c
                        INNER JOIN dbo.ENTITY e
                              ON c.CLIENT_TYPE_ID = e.ENTITY_ID
                        CROSS APPLY ( SELECT
                                          ui.FIRST_NAME + ' ' + ui.LAST_NAME + ','
                                      FROM
                                          dbo.CLIENT_CEM_MAP cem
                                          INNER JOIN dbo.USER_INFO ui
                                                ON cem.USER_INFO_ID = ui.USER_INFO_ID
                                      WHERE
                                          cem.CLIENT_ID = c.CLIENT_ID
                                          AND ui.IS_HISTORY = 0
                                      ORDER BY
                                          ui.USERNAME
                        FOR
                                      XML PATH('') ) cem ( CEM_Names ))
            SELECT DISTINCT
                  bc.Budget
                 ,ch.Client_Name [Client]
                 ,clt.ENTITY_NAME [Client Type]
                 ,Sitegroup_Name [Division]
                 ,ch.Site_Id [Site ID]
                 ,cha.Display_Account_Number [Account]
                 ,e.ENTITY_NAME [Service Level]
                 ,ch.City [City]
                 ,ch.State_Name [State]
                 ,ch.Country_Name [Country]
                 ,ch.Region_Name [Region]
                 ,cha.Account_Vendor_Name [Utility]
                 ,com.Commodity_Name [Commodity]
                 ,vw.Tariff_Transport [Tariff or Transport]
                 ,dbo.BUDGET_FN_GET_RATES(cha.Account_Id, bc.CID) [Rate(s)]
                 ,bc.[Date budget initiated]
                 ,bc.[Date budget due]
                 ,bc.[Date rates completed]
                 ,bc.[Rates completed by]
                 ,bc.[Date sourcing completed]
                 ,bc.[Sourcing completed by]
                 ,bc.[Date budget reviewed]
                 ,bc.[Original or Reforecast]
                 ,bc.[Cannot Perform]
                 ,CAST(cc.CEM AS VARCHAR(300)) [Client Manager]
            FROM
                  Core.Client_Hier ch
                  JOIN Core.Client_Hier_Account cha
                        ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  JOIN ENTITY e
                        ON e.ENTITY_ID = cha.Account_Service_level_Cd
                  JOIN dbo.Commodity com
                        ON com.Commodity_Id = cha.Commodity_Id
                  JOIN Budget_Level_Data_CTE bc
                        ON bc.AID = cha.Account_Id
                           AND bc.CID = com.Commodity_Id
                  JOIN Client_CEM_List_CTE cc
                        ON cc.CLIENT_ID = ch.Client_Id
                  JOIN ENTITY clt
                        ON clt.ENTITY_ID = ch.Client_Type_Id
                  INNER JOIN BUDGET_TARIFF_TRANSPORT_VW vw
                        ON vw.Account_id = cha.Account_Id
                           AND vw.Commodity_Type_id = bc.CID
                           AND vw.Commodity_Type_id = cha.Commodity_Id
            WHERE
                  ( vw.Tariff_Transport = 'transport'
                    OR e.ENTITY_NAME IN ( 'A', 'B' ) )
                  AND ( ch.Country_Id IN ( SELECT
                                                Segments
                                           FROM
                                                dbo.ufn_split(@Country_Id_List, ',') ) )
                  AND ( ch.State_Id IN ( SELECT
                                          Segments
                                         FROM
                                          dbo.ufn_split(@state_Id_List, ',') ) );

END;
;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Budget_Info] TO [CBMSApplication]
GRANT EXECUTE ON  [dbo].[Report_DE_Budget_Info] TO [CBMSReports]
GO
