SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******    
NAME:    
 dbo.UBM_UPDATE_AVISTA_LOG_STATUS_FAILURE_P    
 DESCRIPTION:     
 INPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
 @status		varchar(200)	null      
 @reason		varchar(500)	null      
 @masterLogId	int				null  
 @failureReason varchar(max)	null 
                            
 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
 USAGE EXAMPLES:    
------------------------------------------------------------    
  
    
 AUTHOR INITIALS:    
 Initials Name    
------------------------------------------------------------    
 MODIFICATIONS  Initials Date  Modification    
------------------------------------------------------------              
 NKOSURI  14-Sep-2009 Modified to update detail failure reason     
                     
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[UBM_UPDATE_AVISTA_LOG_STATUS_FAILURE_P]
	@status VARCHAR(200),
	@reason VARCHAR(500),
	@masterLogId INT,
	@detailReason VARCHAR(MAX)
AS

BEGIN

	SET NOCOUNT ON
	
	DECLARE @statusTypeId INT

	SELECT @statusTypeId = entity_id FROM dbo.Entity WHERE Entity_name = @status AND entity_type = 656

	UPDATE dbo.Ubm_Batch_Master_Log
		SET Status_type_id = @statusTypeId,
			batch_failure_reason =  @reason,
			end_date = GETDATE(),
			detail_failure_reason = @detailReason
	WHERE
		ubm_batch_master_log_id = @masterLogId

END
GO
GRANT EXECUTE ON  [dbo].[UBM_UPDATE_AVISTA_LOG_STATUS_FAILURE_P] TO [CBMSApplication]
GO
