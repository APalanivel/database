
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_GET_PROFILE_VENDORS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@vendorTypeId  	int       	          	
	@vendorId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	
	EXEC dbo.SR_GET_PROFILE_VENDORS_P 1,1,288,0,NULL,NULL,NULL,1,25
	EXEC dbo.SR_GET_PROFILE_VENDORS_P 1,1,289,0,291,4,NULL,1,25
	EXEC dbo.SR_GET_PROFILE_VENDORS_P 1,1,289,0,291,1,NULL,1,100
	EXEC dbo.SR_GET_PROFILE_VENDORS_P 1,1,289,0,291,4,44,1,100
	EXEC dbo.SR_GET_PROFILE_VENDORS_P 1,1,0,0,291,4,NULL,1,100
	EXEC dbo.SR_GET_PROFILE_VENDORS_P 1,1,0,623,NULL,NULL,NULL,1,100
	EXEC dbo.SR_GET_PROFILE_VENDORS_P 1,1,0,779,NULL,NULL,NULL,1,100

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2015-07-10	Global Sourcing - Added new input optional parameters @Country_Id, @State_Id, @Commodity_Id
							Added state, countrty and commodity to select list
							Implemented pagination


******/
CREATE PROCEDURE [dbo].[SR_GET_PROFILE_VENDORS_P]
      ( 
       @userId VARCHAR
      ,@sessionId VARCHAR
      ,@vendorTypeId INT
      ,@vendorId INT
      ,@Commodity_Id INT = NULL
      ,@Country_Id INT = NULL
      ,@State_Id INT = NULL
      ,@StartIndex INT = 1
      ,@EndIndex INT = 2147483647 )
AS -- Modified for Bz 6620 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Total INT
      
      DECLARE @Tbl_Vendors TABLE
            ( 
             VENDOR_ID INT
            ,VENDOR_NAME VARCHAR(200)
            ,Vendor_Type VARCHAR(200)
            ,Commodity_Name VARCHAR(50)
            ,STATE_NAME VARCHAR(200)
            ,COUNTRY_NAME VARCHAR(200)
            ,Row_Num INT )
      
      INSERT      INTO @Tbl_Vendors
                  ( 
                   VENDOR_ID
                  ,VENDOR_NAME
                  ,Vendor_Type
                  ,Commodity_Name
                  ,STATE_NAME
                  ,COUNTRY_NAME
                  ,Row_Num )
                  SELECT
                        vndr.VENDOR_ID
                       ,vndr.VENDOR_NAME
                       ,vndr_type.ENTITY_NAME AS Vendor_Type
                       ,com.Commodity_Name
                       ,stt.STATE_NAME
                       ,con.COUNTRY_NAME
                       ,dense_rank() OVER ( ORDER BY vndr_type.ENTITY_NAME, vndr.VENDOR_NAME, vndr.VENDOR_ID )
                  FROM
                        dbo.VENDOR vndr
                        INNER JOIN dbo.ENTITY vndr_type
                              ON vndr.VENDOR_TYPE_ID = vndr_type.ENTITY_ID
                        LEFT JOIN dbo.VENDOR_STATE_MAP vsm
                              ON vndr.VENDOR_ID = vsm.VENDOR_ID
                        LEFT JOIN dbo.VENDOR_COMMODITY_MAP vcm
                              ON vcm.VENDOR_ID = vndr.VENDOR_ID
                        LEFT JOIN dbo.Commodity com
                              ON vcm.COMMODITY_TYPE_ID = com.Commodity_Id
                        LEFT JOIN dbo.STATE stt
                              ON vsm.STATE_ID = stt.STATE_ID
                        LEFT JOIN dbo.COUNTRY con
                              ON con.COUNTRY_ID = stt.COUNTRY_ID
                  WHERE
                        vndr.IS_HISTORY = 0
                        AND ( @vendorTypeId = 0
                              OR vndr.VENDOR_TYPE_ID = @vendorTypeId )
                        AND ( @vendorId = 0
                              OR vndr.VENDOR_ID = @vendorId )
                        AND ( @Commodity_Id IS NULL
                              OR vcm.COMMODITY_TYPE_ID = @Commodity_Id )
                        AND ( @State_Id IS NULL
                              OR vsm.STATE_ID = @State_Id )
                        AND ( @Country_Id IS NULL
                              OR stt.COUNTRY_ID = @Country_Id )
                  GROUP BY
                        vndr.VENDOR_ID
                       ,vndr.VENDOR_NAME
                       ,vndr_type.ENTITY_NAME
                       ,com.Commodity_Name
                       ,stt.STATE_NAME
                       ,con.COUNTRY_NAME
                       
      SELECT
            @Total = max(Row_Num)
      FROM
            @Tbl_Vendors
      SELECT
            vndr.VENDOR_ID
           ,vndr.VENDOR_NAME
           ,vndr.Vendor_Type
           ,case WHEN len(comm.Commodities) > 0 THEN left(comm.Commodities, len(comm.Commodities) - 1)
                 ELSE NULL
            END AS Commodity_Name
           ,case WHEN len(stts.States) > 0 THEN left(stts.States, len(stts.States) - 1)
                 ELSE NULL
            END AS STATE_NAME
           ,case WHEN len(contrs.Countries) > 0 THEN left(contrs.Countries, len(contrs.Countries) - 1)
                 ELSE NULL
            END AS COUNTRY_NAME
           ,Row_Num
           ,@Total AS Total
      FROM
            @Tbl_Vendors vndr
            CROSS APPLY ( SELECT
                              comdts.Commodity_Name + ', '
                          FROM
                              @Tbl_Vendors comdts
                          WHERE
                              vndr.VENDOR_ID = comdts.VENDOR_ID
                              AND vndr.VENDOR_NAME = comdts.VENDOR_NAME
                              AND vndr.Vendor_Type = comdts.Vendor_Type
                          GROUP BY
                              comdts.Commodity_Name
            FOR
                          XML PATH('') ) comm ( Commodities )
            CROSS APPLY ( SELECT
                              sts.STATE_NAME + ', '
                          FROM
                              @Tbl_Vendors sts
                          WHERE
                              vndr.VENDOR_ID = sts.VENDOR_ID
                              AND vndr.VENDOR_NAME = sts.VENDOR_NAME
                              AND vndr.Vendor_Type = sts.Vendor_Type
                          GROUP BY
                              sts.STATE_NAME
            FOR
                          XML PATH('') ) stts ( States )
            CROSS APPLY ( SELECT
                              cons.COUNTRY_NAME + ', '
                          FROM
                              @Tbl_Vendors cons
                          WHERE
                              vndr.VENDOR_ID = cons.VENDOR_ID
                              AND vndr.VENDOR_NAME = cons.VENDOR_NAME
                              AND vndr.Vendor_Type = cons.Vendor_Type
                          GROUP BY
                              cons.COUNTRY_NAME
            FOR
                          XML PATH('') ) contrs ( Countries )
      WHERE
            vndr.Row_Num BETWEEN @StartIndex AND @EndIndex
      GROUP BY
            vndr.VENDOR_ID
           ,vndr.VENDOR_NAME
           ,vndr.Vendor_Type
           ,case WHEN len(comm.Commodities) > 0 THEN left(comm.Commodities, len(comm.Commodities) - 1)
            END
           ,case WHEN len(stts.States) > 0 THEN left(stts.States, len(stts.States) - 1)
            END
           ,case WHEN len(contrs.Countries) > 0 THEN left(contrs.Countries, len(contrs.Countries) - 1)
            END
           ,vndr.Row_Num
      ORDER BY
            vndr.Row_Num	
END
      



;
GO

GRANT EXECUTE ON  [dbo].[SR_GET_PROFILE_VENDORS_P] TO [CBMSApplication]
GO
