
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
      
 NAME: dbo.cbmsUserDivisionMap_GetAvailableForUser    
      
 DESCRIPTION:      
   To get the UnMapped Divisions for given client_id and user_info_id.    
      
 INPUT PARAMETERS:      
     
 Name         DataType    Default   Description      
---------------------------------------------------------------------------------------------------------------    
 @MyAccountId                       INT     NOT NULL    
 @MyClientID                        INT                 NOT NULL    
       
 OUTPUT PARAMETERS:     
            
 Name         DataType    Default   Description      
---------------------------------------------------------------------------------------------------------------    
      
      
 USAGE EXAMPLES:          
---------------------------------------------------------------------------------------------------------------    
    
  Exec  dbo.cbmsUserDivisionMap_GetAssignedForUser 5323,10041    
  Exec  dbo.cbmsUserDivisionMap_GetAvailableForUser 5323,10041 
     
 AUTHOR INITIALS:    
      
 Initials    Name      
---------------------------------------------------------------------------------------------------------------    
  SP        Sandeep Pigilam
       
 MODIFICATIONS:     
     
 Initials    Date       Modification    
---------------------------------------------------------------------------------------------------------------    
  SP       2013-12-20    Added header.    
						 For RA-Admin Removed USER_DIVISION_MAP table and implemented Security Role.    
******/  
CREATE   PROCEDURE dbo.cbmsUserDivisionMap_GetAvailableForUser
      ( 
       @MyAccountId INT
      ,@MyClientID INT )
AS 
BEGIN    
      SET NOCOUNT ON     
    
      SELECT
            d.Sitegroup_Id AS division_id
           ,d.Sitegroup_Name AS division_name
      FROM
            core.Client_Hier d
      WHERE
            d.Site_Id = 0
            AND d.Sitegroup_Id > 0
            AND d.Sitegroup_Type_Name = 'Division'
            AND d.client_id = @MyClientId
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              dbo.User_Security_Role usr
                              INNER JOIN dbo.Security_Role_Client_Hier srch
                                    ON srch.Security_Role_Id = usr.Security_Role_Id
                             WHERE
                              usr.User_Info_Id = @MyAccountId
                              AND srch.Client_Hier_Id = d.Client_Hier_Id )
      ORDER BY
            d.Sitegroup_Name
    
    
END

;
GO

GRANT EXECUTE ON  [dbo].[cbmsUserDivisionMap_GetAvailableForUser] TO [CBMSApplication]
GO
