SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******      
NAME:      
	dbo.Site_CSM_By_Site_Client_Commodity
 
DESCRIPTION:      
	Used to get Client Source Mapping Data    
  
INPUT PARAMETERS:      
Name			      DataType		Default Description      
------------------------------------------------------------      
@ClientCommodityId		INT,          
@SiteGroupId			INT			= NULL,           
@SiteId					VARCHAR(200)= NULL,    
@SiteName				VARCHAR(9),    
@startIndex				INT,    
@endIndex				INT    
            
OUTPUT PARAMETERS:      
Name   DataType  Default Description      
------------------------------------------------------------      
USAGE EXAMPLES:      
------------------------------------------------------------    
 
	EXEC dbo.Site_CSM_By_Site_Client_Commodity 49,null,null,'Site_Name' ,'desc'
	
	EXEC dbo.Site_CSM_By_Site_Client_Commodity 49,null,null,'Sitegroup_Name' ,'Asc'
	
	
	
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
GB   Geetansu Behera      
CMH  Chad Hattabaugh       
HG   Hari     
 
MODIFICATIONS       
Initials Date		Modification      
------------------------------------------------------------      
GB					Created    
SS		28-07-2009	Added TotalRows
SS		09-01-2009	Removed Dynamic SQL Statement, unused variables - @SiteName, @SQL AND Unused Tables Core.Client_Commodity			
SKA					We have to use the case statement to remove the dynamic sql from code
SKA		06 Sep 09   Expand the Select statement and specified names instead of *					

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[Site_CSM_By_Site_Client_Commodity]          
	 @ClientCommodityId INT,          
	 @SiteGroupId INT = NULL,           
	 @SiteId VARCHAR(200)=NULL,  
	 @sortColumn VARCHAR(50)='Site_Name' ,  
	 @sortOrder	VARCHAR(10)='ASC',  
	 @startIndex INT =1,      
	 @endIndex INT = 2147483647 
             
AS          

BEGIN          
  
SET NOCOUNT ON  

  DECLARE @Sort VARCHAR(100)
  
  SET @Sort = @sortColumn +  @sortOrder 
  
  
  SELECT SITE_ID,Site_Name,STATE_ID,STATE_NAME,COUNTRY_ID,Sitegroup_Id,Sitegroup_Name,CLIENT_ID,CLIENT_NAME,NOT_MANAGED,Is_GHG_Reported,
	UOM_Cd,Client_Commodity_Detail_Id,Row_Num,TotalRows
  FROM 
  (
	SELECT s.SITE_ID,  
	   CASE WHEN COUNT(sg.client_id) > 1   
			THEN RTRIM(ad.city) + ', '+ st.state_name + ', ' + RTRIM(ad.address_line1)   
			ELSE RTRIM(ad.city) + ', '+ st.state_name 
	   END + ' (' + s.SITE_NAME + ')' AS Site_Name, 
       ad.STATE_ID,   
       st.STATE_NAME,   
       st.COUNTRY_ID,     
       sg.Sitegroup_Id,  
       sg.Sitegroup_Name ,   
       sg.CLIENT_ID,  
       '' AS CLIENT_NAME,      
       s.NOT_MANAGED,      
       ccd.Is_GHG_Reported ,     
       ccd.UOM_Cd,   
       ccd.Client_Commodity_Detail_Id,   
       Row_Num = ROW_NUMBER() OVER (ORDER BY CASE WHEN @Sort = 'Site_NameASC' THEN s.Site_Name END ASC, 
										     CASE WHEN @Sort = 'Site_NameDESC' THEN s.Site_Name END DESC,
											 CASE WHEN @Sort = 'Site_IdASC' THEN s.Site_Id END ASC,
											 CASE WHEN @Sort = 'Site_IdDESC' THEN s.Site_Id END DESC,
											 CASE WHEN @Sort = 'SiteGroup_NameASC' THEN sg.SiteGroup_Name END ASC, 
											 CASE WHEN @Sort = 'SiteGroup_NameDESC' THEN sg.SiteGroup_Name END DESC,
											 CASE WHEN @Sort = 'SiteGroup_IdASC' THEN sg.SiteGroup_Id END ASC,
											 CASE WHEN @Sort = 'SiteGroup_IdDESC' THEN sg.SiteGroup_Id END DESC,
											 CASE WHEN @Sort = 'Client_IdASC' THEN sg.CLIENT_ID END ASC, 
											 CASE WHEN @Sort = 'Client_IdDESC' THEN sg.CLIENT_ID END DESC
												
											),  
		TotalRows = COUNT(1) OVER()   
    FROM dbo.SITE s  
    INNER JOIN dbo.Sitegroup_site sgs WITH(NOLOCK) ON s.SITE_ID = sgs.SITE_ID  
    INNER JOIN dbo.SiteGroup sg WITH(NOLOCK) ON sg.SiteGroup_Id = sgs.SiteGroup_Id  
    INNER JOIN dbo.code cd WITH(NOLOCK) ON cd.CODE_ID = sg.SiteGroup_Type_Cd  
    INNER JOIN  dbo.ADDRESS ad WITH(NOLOCK) ON ad.ADDRESS_ID = s.PRIMARY_ADDRESS_ID   
    INNER JOIN  dbo.STATE st WITH(NOLOCK) ON st.STATE_ID = ad.STATE_ID   
    INNER JOIN Core.Client_Commodity_Detail ccd  WITH(NOLOCK) ON ccd.client_id = sg.client_id   
     AND ccd.sitegroup_id = sg.SiteGroup_Id  
     AND ccd.site_id = s.site_id 
			
    WHERE	((@SiteId IS NULL) OR (s.SITE_ID = @SiteId)) AND
			((@SitegroupId IS NULL) OR (sg.sitegroup_id = @SitegroupId)) AND
			ccd.Client_Commodity_Id = @ClientCommodityId 
     GROUP BY	s.SITE_ID, s.Site_Name, ad.STATE_ID, st.STATE_NAME, ad.city, ad.address_line1,  
				st.COUNTRY_ID, sg.Sitegroup_Id, sg.Sitegroup_Name, sg.CLIENT_ID, ccd.Is_GHG_Reported ,     
				ccd.UOM_Cd, s.NOT_MANAGED, ccd.Client_Commodity_Detail_Id,   
				ccd.CLIENT_ID
	 ) x  WHERE x.Row_Num BETWEEN @startIndex AND @endIndex
     
     
END
GO
GRANT EXECUTE ON  [dbo].[Site_CSM_By_Site_Client_Commodity] TO [CBMSApplication]
GO
