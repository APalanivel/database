SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[cbmsInvoiceParticipation_MakeNotExpectedForSite]
	( @MyAccountId int
	, @site_id int
	)
AS
BEGIN

   update invoice_participation
      set is_expected = 0
    where site_id = @site_id
      and is_received = 0

END




GO
GRANT EXECUTE ON  [dbo].[cbmsInvoiceParticipation_MakeNotExpectedForSite] TO [CBMSApplication]
GO
