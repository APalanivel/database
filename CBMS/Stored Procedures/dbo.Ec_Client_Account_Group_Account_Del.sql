SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   
		dbo.Ec_Client_Account_Group_Account_Del        
                
Description:                
		This sproc to remoevd the groups.        
                             
 Input Parameters:                
    Name							DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
	@Ec_Client_Account_Group_Id		INT
	
      
 Output Parameters:                      
    Name					  DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
----------------------------------------------------------------------------------------     
   BEGIN TRAN
  
  SELECT * FROM dbo.Ec_Client_Account_Group ecag WHERE Ec_Client_Account_Group_Id=38
  SELECT * FROM dbo.Ec_Client_Account_Group_Account ecaga WHERE Ec_Client_Account_Group_Id=38

   EXEC dbo.Ec_Client_Account_Group_Account_Del 
      @Ec_Client_Account_Group_Id = 38
   
   SELECT * FROM dbo.Ec_Client_Account_Group ecag WHERE Ec_Client_Account_Group_Id=38
   SELECT * FROM dbo.Ec_Client_Account_Group_Account ecaga WHERE Ec_Client_Account_Group_Id=38
   
   ROLLBACK TRAN
       
   
Author Initials:                
    Initials		Name                
----------------------------------------------------------------------------------------                  
	NR				Narayana Reddy                 
 Modifications:                
    Initials        Date			Modification                
----------------------------------------------------------------------------------------                  
    NR				2016-11-15		Created For MAINT-4563.           
               
******/   
CREATE PROCEDURE [dbo].[Ec_Client_Account_Group_Account_Del]
      ( 
       @Ec_Client_Account_Group_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 
      
      DELETE FROM
            dbo.Ec_Client_Account_Group_Account
      WHERE
            Ec_Client_Account_Group_Id = @Ec_Client_Account_Group_Id
            
      
      
      DELETE FROM
            dbo.Ec_Client_Account_Group
      WHERE
            Ec_Client_Account_Group_Id = @Ec_Client_Account_Group_Id
            
      
END

;
GO
GRANT EXECUTE ON  [dbo].[Ec_Client_Account_Group_Account_Del] TO [CBMSApplication]
GO
