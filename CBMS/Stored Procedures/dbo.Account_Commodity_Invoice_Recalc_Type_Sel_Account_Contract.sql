SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Account_Commodity_Invoice_Recalc_Type_Sel_Account_Contract

DESCRIPTION:

INPUT PARAMETERS:
	Name						DataType		Default			Description
-----------------------------------------------------------------------------------------
    @Account_Id					INT	
	@Contract_Id				INT				
    @Commodity_Id				INT

OUTPUT PARAMETERS:
	Name						DataType		Default			Description
-----------------------------------------------------------------------------------------
	
USAGE EXAMPLES:
-----------------------------------------------------------------------------------------
    
  EXEC dbo.Account_Commodity_Invoice_Recalc_Type_Sel_Account_Contract
      @Account_Id = 456911	
	  ,@Contract_Id = 1
	  ,@Commodity_Id= null

  EXEC dbo.Account_Commodity_Invoice_Recalc_Type_Sel_Account_Contract
      @Account_Id = 1527279	
	  ,@Contract_Id = 159815
	  ,@Commodity_Id= null


  EXEC dbo.Account_Commodity_Invoice_Recalc_Type_Sel_Account_Contract  
      @Account_Id = 1655697   
   ,@Contract_Id = 166628
   ,@Commodity_Id= 290  


  EXEC dbo.Account_Commodity_Invoice_Recalc_Type_Sel_Account_Contract
    @Account_Id = 1921329
    , @Contract_Id = 185258
    , @Commodity_Id = 290;


	EXEC dbo.Account_Commodity_Invoice_Recalc_Type_Sel_Account_Contract
    @Account_Id = 1921425
    , @Contract_Id = 185294
    , @Commodity_Id = 290;


AUTHOR INITIALS:
	Initials	Name
-----------------------------------------------------------------------------------------
	NR			Narayana Reddy	
	
	
MODIFICATIONS
	Initials	Date			Modification
-----------------------------------------------------------------------------------------
	NR       	2019-10-30		Created for Add Contract.
	NR			2020-05-27		MAINT-Contract Extension - Removed contract id filter on contract level recalc to show the recalc in  all extended contracts.
	 
******/

CREATE PROCEDURE [dbo].[Account_Commodity_Invoice_Recalc_Type_Sel_Account_Contract]
    (
        @Account_Id INT
        , @Contract_Id INT = NULL
        , @Commodity_Id INT = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Source_cd INT
            , @Supplier_Account_begin_Dt DATE
            , @Supplier_Account_End_Dt DATE;


        SELECT
            @Source_cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Valcon Source Type'
            AND c.Code_Value = 'Contract';


        SELECT
            @Supplier_Account_begin_Dt = cha.Supplier_Account_begin_Dt
            , @Supplier_Account_End_Dt = cha.Supplier_Account_End_Dt
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Account_Type = 'Supplier'
            AND cha.Account_Id = @Account_Id
            AND cha.Supplier_Contract_ID = @Contract_Id;

        SELECT
            acirt.Account_Commodity_Invoice_Recalc_Type_Id
            , acirt.Account_Id
            , com.Commodity_Id
            , com.Commodity_Name
            , c.Code_Id
            , c.Code_Value + '(' + CONVERT(VARCHAR, acirt.Start_Dt, 106) + ' - '
              + ISNULL(CONVERT(VARCHAR, acirt.End_Dt, 106), 'Open') + ')' AS Code_Value
            , acirt.Start_Dt
            , ISNULL(acirt.End_Dt, '9999-12-31') AS End_Dt
            , Source_Cd.Code_Value AS Determinant_Source
            , Determinant_Source_Cd
            , NULL AS Source_value
            , 0 AS Is_Current_Contract_Level_Recalc
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN dbo.Account_Commodity_Invoice_Recalc_Type acirt
                ON acirt.Account_Id = cha.Account_Id
                   AND  acirt.Commodity_ID = cha.Commodity_Id
            INNER JOIN dbo.Commodity com
                ON acirt.Commodity_ID = com.Commodity_Id
            INNER JOIN dbo.Code c
                ON acirt.Invoice_Recalc_Type_Cd = c.Code_Id
            LEFT JOIN dbo.Code Source_Cd
                ON acirt.Determinant_Source_Cd = Source_Cd.Code_Id
        WHERE
            acirt.Account_Id = @Account_Id
            AND (   @Contract_Id IS NULL
                    OR  cha.Supplier_Contract_ID = @Contract_Id)
            AND (   @Commodity_Id IS NULL
                    OR  acirt.Commodity_ID = @Commodity_Id)
            AND (   cha.Account_Type = 'Utility'
                    OR  (   cha.Account_Type = 'Supplier'
                            AND (   acirt.Start_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                                   AND     cha.Supplier_Account_End_Dt
                                    OR  ISNULL(acirt.End_Dt, '9999-12-31') BETWEEN cha.Supplier_Account_begin_Dt
                                                                           AND     cha.Supplier_Account_End_Dt
                                    OR  cha.Supplier_Account_begin_Dt BETWEEN acirt.Start_Dt
                                                                      AND     ISNULL(acirt.End_Dt, '9999-12-31')
                                    OR  cha.Supplier_Account_End_Dt BETWEEN acirt.Start_Dt
                                                                    AND     ISNULL(acirt.End_Dt, '9999-12-31'))))
        UNION
        -------------------------------Supplier Account - Source Contract Level ---------------------------------------------	

        SELECT
            acirt.Account_Commodity_Invoice_Recalc_Type_Id
            , acirt.Account_Id
            , com.Commodity_Id
            , com.Commodity_Name
            , C.Code_Id
            , C.Code_Value + '(' + CONVERT(VARCHAR, acirt.Start_Dt, 106) + ' - '
              + ISNULL(CONVERT(VARCHAR, acirt.End_Dt, 106), 'Open') + ')' AS Code_Value
            , acirt.Start_Dt
            , ISNULL(acirt.End_Dt, '9999-12-31') AS End_Dt
            , Source_Cd.Code_Value AS Determinant_Source
            , acirt.Determinant_Source_Cd
            , cdd.Code_Value AS Source_value
            , MIN(CASE WHEN acirt.Account_Commodity_Invoice_Recalc_Type_Id IS NOT NULL
                            AND acirt.Source_Cd IS NOT NULL
                            AND crc.Contract_Id = @Contract_Id THEN 1
                      ELSE 0
                  END) AS Is_Current_Contract_Level_Recalc
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN dbo.Contract_Recalc_Config crc
                ON crc.Contract_Id = cha.Supplier_Contract_ID
            INNER JOIN dbo.Account_Commodity_Invoice_Recalc_Type acirt
                ON acirt.Account_Id = cha.Account_Id
                   AND  acirt.Commodity_ID = cha.Commodity_Id
                   AND  crc.Start_Dt = acirt.Start_Dt
                   AND  ISNULL(crc.End_Dt, '9999-12-31') = ISNULL(acirt.End_Dt, '9999-12-31')
            INNER JOIN dbo.Commodity com
                ON acirt.Commodity_ID = com.Commodity_Id
            INNER JOIN dbo.Code C
                ON crc.Supplier_Recalc_Type_Cd = C.Code_Id
            LEFT JOIN dbo.Code Source_Cd
                ON acirt.Determinant_Source_Cd = Source_Cd.Code_Id
            LEFT JOIN dbo.Code cdd
                ON acirt.Source_Cd = cdd.Code_Id
        WHERE
            acirt.Account_Id = @Account_Id
            AND acirt.Source_Cd = @Source_cd
            AND (   @Commodity_Id IS NULL
                    OR  acirt.Commodity_ID = @Commodity_Id)
            AND crc.Is_Consolidated_Contract_Config = 0
            AND (   acirt.Start_Dt BETWEEN @Supplier_Account_begin_Dt
                                   AND     @Supplier_Account_End_Dt
                    OR  ISNULL(acirt.End_Dt, '9999-12-31') BETWEEN @Supplier_Account_begin_Dt
                                                           AND     @Supplier_Account_End_Dt
                    OR  @Supplier_Account_begin_Dt BETWEEN acirt.Start_Dt
                                                   AND     ISNULL(acirt.End_Dt, '9999-12-31')
                    OR  @Supplier_Account_End_Dt BETWEEN acirt.Start_Dt
                                                 AND     ISNULL(acirt.End_Dt, '9999-12-31'))
        GROUP BY
            acirt.Account_Commodity_Invoice_Recalc_Type_Id
            , acirt.Account_Id
            , com.Commodity_Id
            , com.Commodity_Name
            , C.Code_Id
            , C.Code_Value + '(' + CONVERT(VARCHAR, acirt.Start_Dt, 106) + ' - '
              + ISNULL(CONVERT(VARCHAR, acirt.End_Dt, 106), 'Open') + ')'
            , acirt.Start_Dt
            , acirt.End_Dt
            , Source_Cd.Code_Value
            , acirt.Determinant_Source_Cd
            , cdd.Code_Value
        ORDER BY
            acirt.Start_Dt;




    END;












GO
GRANT EXECUTE ON  [dbo].[Account_Commodity_Invoice_Recalc_Type_Sel_Account_Contract] TO [CBMSApplication]
GO
