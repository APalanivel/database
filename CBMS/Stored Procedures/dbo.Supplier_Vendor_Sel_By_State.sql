SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Supplier_Vendor_Sel_By_State_Sel_By_Country_Commodity]
           
DESCRIPTION:             
			To get map/unmap countries and SR pricing products
			
INPUT PARAMETERS:            
	Name					DataType		Default		Description  
---------------------------------------------------------------------------------  
    @Meter_Id				VARCHAR(MAX)
    @Contract_Start_Date	DATETIME
    @Contract_End_Date		DATETIME


OUTPUT PARAMETERS:
	Name			DataType		Default		Description  
---------------------------------------------------------------------------------  


 USAGE EXAMPLES:
---------------------------------------------------------------------------------  

		EXEC dbo.Supplier_Vendor_Sel_By_State 9
		EXEC dbo.Supplier_Vendor_Sel_By_State 45
		EXEC dbo.Supplier_Vendor_Sel_By_State 29
		EXEC dbo.Supplier_Vendor_Sel_By_State 59

		EXEC dbo.Supplier_Vendor_Sel_By_State 7

 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2015-10-06	Global Sourcing - Phase2 -Created
							
******/
CREATE PROCEDURE [dbo].[Supplier_Vendor_Sel_By_State] ( @State_Id INT )
AS
BEGIN

      SET NOCOUNT ON;
		/*
			When Sourcing users are created first mapping records created with the NULL state id in Sr_Supplier_Contact_Vendor_Map table and Is_Primary set to true thus it cant be used to filter by state.
			When the users are actually mapped to different states will have record in Sr_Supplier_Contact_Vendor_Map table with the actual state that user is mapped to thus doing self join on that table.
		*/
		
      SELECT
            vndr.VENDOR_ID
           ,vndr.VENDOR_NAME
      FROM
            dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP scvm
            INNER JOIN dbo.VENDOR vndr
                  ON vndr.VENDOR_ID = scvm.VENDOR_ID
            INNER JOIN dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP vm
                  ON vm.SR_SUPPLIER_CONTACT_INFO_ID = scvm.SR_SUPPLIER_CONTACT_INFO_ID
            INNER JOIN dbo.ENTITY typ
                  ON vndr.VENDOR_TYPE_ID = typ.ENTITY_ID
      WHERE
            scvm.IS_PRIMARY = 1
            AND typ.ENTITY_NAME = 'Supplier'
            AND vm.STATE_ID = @State_Id
      GROUP BY
            vndr.VENDOR_ID
           ,vndr.VENDOR_NAME;

END;
;

GO
GRANT EXECUTE ON  [dbo].[Supplier_Vendor_Sel_By_State] TO [CBMSApplication]
GO
