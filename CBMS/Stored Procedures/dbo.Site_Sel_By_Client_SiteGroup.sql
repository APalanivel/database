SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******    
NAME:    
    dbo.Site_Sel_By_Client_SiteGroup  
     
 DESCRIPTION:  
	Selects Site details for the given client and division_id/ sitegroup_id
    
 INPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
 @ClientId INT,    
 @DivisionId INT = NULL   
  
   OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
 USAGE EXAMPLES:    
------------------------------------------------------------    

dbo.Site_Sel_By_Client_SiteGroup   201, 29

    
    
AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
 HG   Harihara Suthan Ganeshan     
 NA   Nitesh Asthana  
 SS   Subhash Subramanyam  
 MA   Mohammed Abdul Aman   
 
 MODIFICATIONS     
 Initials Date			Modification    
------------------------------------------------------------    
 HG		  07/10/2009	Created    

 
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.Site_Sel_By_Client_SiteGroup    
	@ClientId INT,    
	@SiteGroup_Id INT = NULL  
AS

BEGIN

	SET NOCOUNT ON

	SELECT s.site_id, s.site_name
	FROM dbo.Site s
		JOIN dbo.SiteGroup_Site sgs ON sgs.Site_Id = s.Site_Id
		JOIN dbo.SiteGroup sg ON sg.SiteGroup_Id = s.division_Id
	WHERE sg.Client_id = @ClientId
		AND (@SiteGroup_Id IS NULL OR sg.SiteGroup_Id = @SiteGroup_Id)
	ORDER BY s.Site_Name

END
GO
GRANT EXECUTE ON  [dbo].[Site_Sel_By_Client_SiteGroup] TO [CBMSApplication]
GO
