
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
  
NAME: dbo.Cbms_Add_Division_P  
  
DESCRIPTION: Creates a divsion in teh SiteGroup table and adds Details to Divsion_Detail  
  
INPUT PARAMETERS:      
 Name							DataType          Default     Description      
------------------------------------------------------------------      
 @sbaTypeId                     int  
 @priceIndexId                  int  
 @clientId                      int  
 @termPreferredTypeId           int  
 @contractReviewerTypeid        int  
 @decisionMakerTypeid           int  
 @signatoryTypeId               int  
 @divisionName                  varchar(200)  
 @isInterestMinoritySuppliers   int  
 @isCorporateHedge              int  
 @naicsCode                     varchar(30)  
 @taxNumber                     varchar(30)  
 @dunsNumber                    varchar(30)  
 @isCorporateDivision           int  
 @triggerRights                 int  
 @miscComments                  varchar(4000)  
 @notManaged                    int  
 @contractingEntity             varchar(200)  
 @clientLegalStructure          varchar(4000)  
      
OUTPUT PARAMETERS:
 Name              DataType          Default     Description      
------------------------------------------------------------      
 @divisionId       int  
  
USAGE EXAMPLES:  
------------------------------------------------------------  

	EXEC dbo.Cbms_Add_Division_P  
		   @sbaTypeId					=	459  
		  ,@priceIndexId				=	464  
		  ,@clientId					=	1043   
		  ,@termPreferredTypeId			=	200  
		  ,@contractReviewerTypeid		=	203  
		  ,@decisionMakerTypeid			=	204  
		  ,@signatoryTypeId				=	206  
		  ,@divisionName				=	'Test Data'  
		  ,@isInterestMinoritySuppliers =	1  
		  ,@isCorporateHedge			=	0  
		  ,@naicsCode					=	null   
		  ,@taxNumber					=	'62-1867018'  
		  ,@dunsNumber					=	'11-266-2812'  
		  ,@isCorporateDivision			=	0  
		  ,@triggerRights				=	1  
		  ,@miscComments				=	'Test Data'  
		  ,@notManaged					=	0  
		  ,@contractingEntity			=	'Test Data'  
		  ,@clientLegalStructure		=	''  
		  ,Sitegroup_Reference_Number	=	Null	
		  ,@divisionId					=	NULL
  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 DR		  Deana Ritter
 PNR	  Pandarinath  
 TP		  Anoop
  
MODIFICATIONS  
  
 Initials	Date		Modification  
------------------------------------------------------------  
 DR			06/02/2009	Modified for replication to SV  
 CH			06/21/2009  Modified for Divsion table split with sitegroup  
 GH			07/16/2009	Is_Complete flag column added in the insert statement of Sitegroup.  
 DMR		09/10/2010	Modified for Quoted_Identifier
 PNR		11/19/2010	Removed Transaction part as part proj : Security Roles Administration.
 TP			06/18/2014	Added Sitegroup_Reference_Number column
******/  
CREATE PROCEDURE dbo.Cbms_Add_Division_P
      ( 
       @sbaTypeId INT
      ,@priceIndexId INT
      ,@clientId INT
      ,@termPreferredTypeId INT
      ,@contractReviewerTypeid INT
      ,@decisionMakerTypeid INT
      ,@signatoryTypeId INT
      ,@divisionName VARCHAR(200)
      ,@isInterestMinoritySuppliers INT
      ,@isCorporateHedge INT
      ,@naicsCode VARCHAR(30)
      ,@taxNumber VARCHAR(30)
      ,@dunsNumber VARCHAR(30)
      ,@isCorporateDivision INT
      ,@triggerRights INT
      ,@miscComments VARCHAR(4000)
      ,@notManaged INT
      ,@contractingEntity VARCHAR(200)
      ,@clientLegalStructure VARCHAR(4000)
      ,@divisionId INT OUT
      ,@Portfolio_Client_Hier_Reference_Number INT = NULL )
AS 
BEGIN  
	
      SET NOCOUNT ON;
	
      INSERT      dbo.SiteGroup
                  ( 
                   Sitegroup_Name
                  ,Sitegroup_Type_Cd
                  ,Client_Id
                  ,Is_Smart_Group
                  ,Is_Publish_To_DV
                  ,Owner_User_Id
                  ,Add_User_Id
                  ,Add_Dt
                  ,Mod_User_Id
                  ,Mod_Dt
                  ,Is_Complete
                  ,Portfolio_Client_Hier_Reference_Number )
                  SELECT
                        @divisionName
                       ,sgType.Code_Id
                       ,@clientId
                       ,0
                       ,1
                       ,ui.User_Info_Id
                       ,ui.User_Info_Id
                       ,getdate()
                       ,ui.user_Info_id
                       ,getdate()
                       ,1
                       ,@Portfolio_Client_Hier_Reference_Number
                  FROM
                        dbo.User_Info ui
                        CROSS JOIN dbo.Code sgType
                        INNER JOIN dbo.CodeSet cs
                              ON sgType.CodeSet_Id = cs.CodeSet_Id
                  WHERE
                        UserName = 'GlobalSiteGroup'
                        AND sgType.Code_Value = 'Division'
                        AND cs.CodeSet_Name = 'SiteGroup_Type'

      SET @DivisionId = scope_identity()  

      INSERT      INTO dbo.Division_Dtl
                  ( 
                   SiteGroup_Id
                  ,SBA_TYPE_ID
                  ,PRICE_INDEX_ID
                  ,CLIENT_ID
                  ,TERM_PREFERRED_TYPE_ID
                  ,CONTRACT_REVIEWER_TYPE_ID
                  ,DECISION_MAKER_TYPE_ID
                  ,SIGNATORY_TYPE_ID
                  ,IS_INTEREST_MINORITY_SUPPLIERS
                  ,IS_CORPORATE_HEDGE
                  ,NAICS_CODE
                  ,TAX_NUMBER
                  ,DUNS_NUMBER
                  ,IS_CORPORATE_DIVISION
                  ,TRIGGER_RIGHTS
                  ,MISC_COMMENTS
                  ,NOT_MANAGED
                  ,CONTRACTING_ENTITY
                  ,CLIENT_LEGAL_STRUCTURE )
      VALUES
                  ( 
                   @DivisionId
                  ,@sbaTypeId
                  ,@priceIndexId
                  ,@clientId
                  ,@termPreferredTypeId
                  ,@contractReviewerTypeid
                  ,@decisionMakerTypeid
                  ,@signatoryTypeId
                  ,@isInterestMinoritySuppliers
                  ,@isCorporateHedge
                  ,@naicsCode
                  ,@taxNumber
                  ,@dunsNumber
                  ,@isCorporateDivision
                  ,@triggerRights
                  ,@miscComments
                  ,@notManaged
                  ,@contractingEntity
                  ,@clientLegalStructure )  
END





;
GO

GRANT EXECUTE ON  [dbo].[Cbms_Add_Division_P] TO [CBMSApplication]
GO
