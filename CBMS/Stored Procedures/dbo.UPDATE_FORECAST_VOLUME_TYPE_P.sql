SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
CREATE  PROCEDURE dbo.UPDATE_FORECAST_VOLUME_TYPE_P
@userId varchar(10),
@sessionId varchar(20),
@siteId int

 AS

	set nocount on
	update rm_forecast_volume_details set volume_type_id =
		(select entity_id from entity where entity_name='Manual_Forecast')
	where volume_type_id =
		(select entity_id from entity where entity_name='Edit_Actual_Contract') and site_id=@siteId


	update rm_forecast_volume_details set volume_type_id =
		(select entity_id from entity where entity_name='Previous_Forecast')
	where volume_type_id =
		(select entity_id from entity where entity_name='Previous_Edit_Actual_Contract') and site_id=@siteId

GO
GRANT EXECUTE ON  [dbo].[UPDATE_FORECAST_VOLUME_TYPE_P] TO [CBMSApplication]
GO
