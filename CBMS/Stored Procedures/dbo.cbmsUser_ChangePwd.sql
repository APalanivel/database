SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	cbms_prod.dbo.cbmsUser_ChangePwd

DESCRIPTION:

	Used to change the password for CBMS Users

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	INT
	@user_info_id  	INT
	@passcode      	VARCHAR(128)

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	 EXEC dbo.cbmsUser_ChangePwd 1,49,N'098F6BCD4621D373CADE4E832627B4F6'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath

MODIFICATIONS :
	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script
	PNR			10/21/2010	Changed tablename from user_info to User_PassCode in Update Statement
							as well merged both sps code inside of current sp only instead calling both sps 'cbmsEntityAudit_Log','cbmsUser_Get' individually.
							(Project Name : DV_SV_Password_Expiration)
	PNR			11/01/2010	Passcode column fetched from User_Passcode table instead user_info.

******/

CREATE PROCEDURE dbo.cbmsUser_ChangePwd
(
      @MyAccountId	INT,
      @user_info_id INT,
      @passcode		NVARCHAR(128)
)
AS
BEGIN

	SET NOCOUNT ON

	DECLARE @table_id		INT
		, @Active_From_Dt	DATE = '1/1/1900'

	BEGIN TRY
		BEGIN TRAN
			
			EXEC dbo.User_PassCode_Ins @User_Info_Id, @PassCode, @Active_From_Dt, @MyAccountId

			EXEC dbo.cbmsEntityAudit_Log @MyAccountId, 'USER_INFO', @user_info_id, 'Edit'

			SELECT
				 ui.user_info_id
				,ui.username
				,ui.queue_id
				,@passcode PassCode
				,ui.first_name
				,ui.middle_name
				,ui.last_name
				,ui.first_name + SPACE(1) + ui.last_name full_name
				,ui.email_address
				,ui.is_history
				,ui.access_level
				,ui.client_id
				,ui.division_id
				,ui.site_id
				,ui.phone_number
				,ui.phone_number_ext
				,ui.cell_number
				,ui.fax_number
				,ui.cem_homepage
				,ui.locale_code
				,ui.currency_unit_id
				,ui.el_uom_id
				,ui.ng_uom_id
				,sol.ubm_id
				,sol.ubm_username
				,sol.ubm_password
				,sol.dsm_username
				,sol.dsm_password
				,ui.address_1
				,ui.address_2
				,ui.city
				,ui.state_id
				,ui.zipcode
				,ui.country_id
				,ui.queue_id
			FROM
				dbo.user_info ui
				LEFT JOIN dbo.sso_other_logins sol
						  ON sol.user_info_id = ui.user_info_id
			WHERE
				ui.user_info_id = @user_info_id

		COMMIT TRAN			
		
	END TRY
	BEGIN CATCH
	 IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRAN
			END
	 EXEC dbo.usp_RethrowError
	END CATCH

END
GO
GRANT EXECUTE ON  [dbo].[cbmsUser_ChangePwd] TO [CBMSApplication]
GO
