SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                              
 NAME: [dbo].[Utility_Detail_Analyst_Map_Save]                  
                               
 DESCRIPTION:                              
  To Insert or Update data of table utility_detail_analyst_map                   
                              
 INPUT PARAMETERS:                
                           
 Name					     DataType        Default       Description              
---------------------------------------------------------------------------------------------------------------            
@Utility_Detail_Analyst_Map_Tvp	Tvp_Utility_Detail_Analyst_Map READONLY   
                
                              
 OUTPUT PARAMETERS:                
                                 
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
                              
 USAGE EXAMPLES:                                  
--------------------------------------------------------------------------------------------------------------- 

--Example 1 for Update
BEGIN TRAN 

   EXEC [dbo].[Utility_Detail_Analyst_Map_Sel]       
      @Country_Id=4     
     ,@Vendor_Id=16101  
     ,@Queue =NULL    
     ,@Start_Index = 1      
     ,@End_Index = 25      
     ,@Total_Row_Count = 0 
                                     
      DECLARE @Utility_Detail_Analyst_Map_Tvp AS Tvp_Utility_Detail_Analyst_Map              
      INSERT      INTO @Utility_Detail_Analyst_Map_Tvp
                  ( [Utility_Detail_Id]
                  ,[Group_Info_Id]
                  ,[Analyst_Id] )
      VALUES
                  ( 14609
                  ,135
                  ,29574 ) 
    
      EXEC [dbo].[Utility_Detail_Analyst_Map_Save] 
            @Utility_Detail_Analyst_Map_Tvp  
            
   EXEC [dbo].[Utility_Detail_Analyst_Map_Sel]       
      @Country_Id=4     
     ,@Vendor_Id=16101  
     ,@Queue =NULL    
     ,@Start_Index = 1      
     ,@End_Index = 25      
     ,@Total_Row_Count = 0 
                 
ROLLBACK

            
                             
 AUTHOR INITIALS:              
             
 Initials              Name              
---------------------------------------------------------------------------------------------------------------                            
 SP                    Sandeep Pigilam                
                               
 MODIFICATIONS:            
                
 Initials              Date             Modification            
---------------------------------------------------------------------------------------------------------------            
 SP                    2014-02-24       Created                      
                             
******/   
CREATE PROCEDURE dbo.Utility_Detail_Analyst_Map_Save
      ( 
       @Utility_Detail_Analyst_Map_Tvp Tvp_Utility_Detail_Analyst_Map READONLY )
AS 
BEGIN

      SET NOCOUNT ON

	
	
      INSERT      INTO dbo.Utility_Detail_Analyst_Map
                  ( 
                   Utility_Detail_ID
                  ,Group_Info_ID
                  ,Analyst_ID )
                  SELECT
                        tvp.Utility_Detail_Id
                       ,tvp.Group_Info_Id
                       ,tvp.Analyst_Id
                  FROM
                        @Utility_Detail_Analyst_Map_Tvp tvp
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.Utility_Detail_Analyst_Map map
                                     WHERE
                                          map.Utility_Detail_ID = tvp.Utility_Detail_ID
                                          AND map.Group_Info_ID = tvp.Group_Info_ID )
                                   
      UPDATE
            map
      SET   
            map.Analyst_Id = tvp.Analyst_Id
      FROM
            dbo.Utility_Detail_Analyst_Map map
            INNER JOIN @Utility_Detail_Analyst_Map_Tvp tvp
                  ON map.Utility_Detail_ID = tvp.Utility_Detail_ID
                     AND map.Group_Info_ID = tvp.Group_Info_ID
                     AND ( ( map.Analyst_ID IS NULL
                             AND tvp.Analyst_Id IS NOT NULL )
                           OR ( map.Analyst_Id IS NOT NULL
                                AND tvp.Analyst_Id IS NULL )
                           OR map.Analyst_ID <> tvp.Analyst_Id )
                  
	
END



;
GO
GRANT EXECUTE ON  [dbo].[Utility_Detail_Analyst_Map_Save] TO [CBMSApplication]
GO
