SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Invoice_Collection_Issue_upd     
                
Description:                
   This sproc to get the attribute details for a Given id's.        
                             
 Input Parameters:                
    Name												DataType   Default   Description                  
----------------------------------------------------------------------------------------                  
    @Invoice_Collection_Activity_Id INT
         , @Is_Automatic_Link BIT = NULL
         , @Issue_Link_Start_Date DATE = NULL           
   
 Output Parameters:                      
    Name        DataType   Default   Description                  
----------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
----------------------------------------------------------------------------------------     
  BEGIN TRAN
  EXEC [Invoice_Collection_Activity_Upd] 
  ROLLBACK
  
  
       
     
Author Initials:                
    Initials  Name                
----------------------------------------------------------------------------------------                  
 RKV    Ravi Kumar Vegesna  
 Modifications:                
    Initials        Date   Modification                
----------------------------------------------------------------------------------------                  
    RKV    2019-09-29  Created .
    
               
******/

CREATE PROCEDURE [dbo].[Invoice_Collection_Activity_Upd]
     (
         @Invoice_Collection_Activity_Id INT
         , @Is_Automatic_Link BIT = NULL
         , @Issue_Link_Start_Date DATE = NULL
     )
AS
    BEGIN
        UPDATE
            dbo.Invoice_Collection_Activity
        SET
            Issue_Link_Start_Date = @Issue_Link_Start_Date
            , Is_Automatic_Link = ISNULL(@Is_Automatic_Link, 0)
        WHERE
            Invoice_Collection_Activity_Id = @Invoice_Collection_Activity_Id;
    END;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Activity_Upd] TO [CBMSApplication]
GO
