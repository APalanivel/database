SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
----------------------------------------------------------------------------------------------------------------------  
/******  
NAME:  
 [DBO].[ACCOUNT_DATES_UPD]  
  
DESCRIPTION:  
 UPDATES SUPPLIER ACCOUNT START/END DATES WITH CONTRACT START/END DATES  
  
  
INPUT PARAMETERS:  
NAME   DATATYPE DEFAULT  DESCRIPTION  
------------------------------------------------------------  
@CONTRACT_ID  INT  
@CONTRACT_START_DATE DATETIME  
@CONTRACT_END_DATE DATETIME  
@SUPPACCSTARTDATEFLAG   BIT  
@SUPPACCENDDATEFLAG     BIT  
  
OUTPUT PARAMETERS:  
NAME   DATATYPE DEFAULT  DESCRIPTION  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
  
 EXEC ACCOUNT_DATES_UPD  .....  
  
AUTHOR INITIALS:  
INITIALS NAME  
------------------------------------------------------------  
MGB  BHASKARAN GOPALAKRISHNAN  
  
MODIFICATIONS  
INITIALS DATE  MODIFICATION  
------------------------------------------------------------  
MGB  24-NOV-09 CREATED  
*/  
  
CREATE PROCEDURE dbo.ACCOUNT_DATES_UPD  
  @CONTRACT_ID INT,  
  @Supplier_Account_Begin_Dt DATETIME,  
  @Supplier_Account_End_Dt DATETIME,  
  @Is_Update_Begin_Dt  BIT,  
  @Is_Update_End_Dt   BIT  
AS  
BEGIN  
  
 SET NOCOUNT ON  
  
 UPDATE A  
  SET  
   A.SUPPLIER_ACCOUNT_BEGIN_DT = CASE @Is_Update_Begin_Dt   
            WHEN 1 THEN @Supplier_Account_Begin_Dt  
            ELSE A.Supplier_Account_Begin_Dt  
           END  
               
   , A.SUPPLIER_ACCOUNT_END_DT =  CASE @Is_Update_End_Dt   
            WHEN 1 THEN @Supplier_Account_End_Dt  
            ELSE A.Supplier_Account_End_Dt  
           END  
 FROM  
  DBO.ACCOUNT A  
  JOIN DBO.SUPPLIER_ACCOUNT_METER_MAP SAMM ON SAMM.ACCOUNT_ID = A.ACCOUNT_ID  
 WHERE  
  SAMM.CONTRACT_ID = @CONTRACT_ID  
  AND SAMM.IS_HISTORY = 0  
  
END
GO
GRANT EXECUTE ON  [dbo].[ACCOUNT_DATES_UPD] TO [CBMSApplication]
GO
