SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******************************************************************************************************    
NAME :	dbo.Service_Broker_Conversation_Handle_Sel
   
DESCRIPTION: 

 INPUT PARAMETERS:    

 Name             DataType               Default        Description    
--------------------------------------------------------------------      

 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
--------------------------------------------------------------------   

 
  USAGE EXAMPLES:    
--------------------------------------------------------------------    
	
	Service_Broker_Conversation_Handle_Sel
  
AUTHOR INITIALS:    
 Initials Name    
-------------------------------------------------------------------    

   
 MODIFICATIONS     
 Initials	Date			Modification    
--------------------------------------------------------------------    
 DSC		04/08/2014		Created
******************************************************************************************************/
CREATE PROCEDURE [dbo].[Service_Broker_Conversation_Handle_Sel]
      ( 
       @From_Service NVARCHAR(255)
      ,@Target_Service NVARCHAR(255)
      ,@Target_Contract NVARCHAR(255)
      ,@Conversation_Handle UNIQUEIDENTIFIER OUTPUT )
AS 
BEGIN    
    
      SET NOCOUNT ON;            

      SET @Conversation_Handle = NULL

      SELECT
            @Conversation_Handle = conversation_handle
      FROM
            Service_Broker_Conversation_Handler
      WHERE
            Target_Service = @Target_Service
            AND Target_Contract = @Target_Contract

      IF EXISTS ( SELECT
                        1
                  FROM
                        sys.conversation_endpoints
                  WHERE
                        state <> 'CO'
                        AND conversation_handle = @Conversation_Handle )
            OR NOT EXISTS ( SELECT
                              1
                            FROM
                              sys.conversation_endpoints
                            WHERE
                              conversation_handle = @Conversation_Handle ) 
            BEGIN
							
                  BEGIN DIALOG CONVERSATION @Conversation_Handle    
						FROM SERVICE @From_Service
						TO SERVICE @Target_Service    
						ON CONTRACT @Target_Contract; 
						
                  DELETE
                        dbo.Service_Broker_Conversation_Handler
                  WHERE
                        Target_Contract = @Target_Contract
                        AND Target_Service = @Target_Service

                  INSERT      INTO dbo.Service_Broker_Conversation_Handler
                              ( 
                               Target_Contract
                              ,Target_Service
                              ,[Conversation_Handle]
                              ,Last_Change_Ts )
                  VALUES
                              ( 
                               @Target_Contract
                              ,@Target_Service
                              ,@Conversation_Handle
                              ,getdate() )

            END

                              
      SELECT
            @Conversation_Handle = Conversation_Handle
      FROM
            dbo.Service_Broker_Conversation_Handler
      WHERE
            Target_Service = @Target_Service
            AND Target_Contract = @Target_Contract
		
		
END
;
GO
GRANT EXECUTE ON  [dbo].[Service_Broker_Conversation_Handle_Sel] TO [CBMSApplication]
GRANT EXECUTE ON  [dbo].[Service_Broker_Conversation_Handle_Sel] TO [sb_Execute]
GO
