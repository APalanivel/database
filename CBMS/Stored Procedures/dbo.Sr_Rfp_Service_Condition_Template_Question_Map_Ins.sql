SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Rfp_Service_Condition_Template_Question_Map_Ins]
           
DESCRIPTION:             
			To get service condition template details 
			
INPUT PARAMETERS:            
	Name							DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Selected_Pricing_Product_Id	INT
    @SR_RFP_ACCOUNT_TERM_ID			INT
    @User_Info_Id					INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
	SELECT top 10 * FROM dbo.Sr_Rfp_Service_Condition_Template_Question_Map rfpqtn 
	INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp ON rfpqtn.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
    INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
    INNER JOIN dbo.SR_PRICING_PRODUCT spp ON srpp.PRODUCT_NAME = spp.PRODUCT_NAME
    
BEGIN TRANSACTION
SELECT * FROM dbo.Sr_Rfp_Service_Condition_Template_Question_Map WHERE SR_RFP_SELECTED_PRODUCTS_ID = 1211417 AND SR_RFP_ACCOUNT_TERM_ID =109551
EXEC dbo.Sr_Rfp_Service_Condition_Template_Question_Map_Ins  1211417 ,109551,16 
SELECT * FROM dbo.Sr_Rfp_Service_Condition_Template_Question_Map WHERE SR_RFP_SELECTED_PRODUCTS_ID = 1211417 AND SR_RFP_ACCOUNT_TERM_ID =109551
ROLLBACK TRANSACTION
			
		
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
-----------------------------------------------------------------------
	RR			2016-03-02	Global Sourcing - Phase3 - GCS-478 Created
******/
CREATE PROCEDURE [dbo].[Sr_Rfp_Service_Condition_Template_Question_Map_Ins]
      ( 
       @Selected_Pricing_Product_Id INT
      ,@SR_RFP_ACCOUNT_TERM_ID INT
      ,@User_Info_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE
            @Country_Cnt INT
           ,@Sr_Service_Condition_Template_Id INT
      
      DECLARE @Acc_Country_Commodity AS TABLE
            ( 
             Country_Id INT
            ,Commodity_Id INT )
            
      DECLARE @Templates AS TABLE
            ( 
             Sr_Service_Condition_Template_Id INT
            ,Country_Id INT
            ,Cnt INT )
      
      INSERT      INTO @Acc_Country_Commodity
                  ( 
                   Country_Id
                  ,Commodity_Id )
                  SELECT
                        cha.Meter_Country_Id
                       ,rfp.COMMODITY_TYPE_ID
                  FROM
                        dbo.SR_RFP_ACCOUNT_TERM srat
                        INNER JOIN dbo.SR_RFP_ACCOUNT rfpacc
                              ON srat.SR_ACCOUNT_GROUP_ID = rfpacc.SR_RFP_ACCOUNT_ID
                        INNER JOIN dbo.SR_RFP rfp
                              ON rfpacc.SR_RFP_ID = rfp.SR_RFP_ID
                        INNER JOIN Core.Client_Hier_Account cha
                              ON rfpacc.ACCOUNT_ID = cha.Account_Id
                                 AND cha.Commodity_Id = rfp.COMMODITY_TYPE_ID
                  WHERE
                        srat.IS_BID_GROUP = 0
                        AND srat.SR_RFP_ACCOUNT_TERM_ID = @SR_RFP_ACCOUNT_TERM_ID
                  GROUP BY
                        cha.Meter_Country_Id
                       ,rfp.COMMODITY_TYPE_ID
                       
      INSERT      INTO @Acc_Country_Commodity
                  ( 
                   Country_Id
                  ,Commodity_Id )
                  SELECT
                        cha.Meter_Country_Id
                       ,rfp.COMMODITY_TYPE_ID
                  FROM
                        dbo.SR_RFP_ACCOUNT_TERM srat
                        INNER JOIN dbo.SR_RFP_ACCOUNT rfpacc
                              ON srat.SR_ACCOUNT_GROUP_ID = rfpacc.SR_RFP_BID_GROUP_ID
                        INNER JOIN dbo.SR_RFP rfp
                              ON rfpacc.SR_RFP_ID = rfp.SR_RFP_ID
                        INNER JOIN Core.Client_Hier_Account cha
                              ON rfpacc.ACCOUNT_ID = cha.Account_Id
                                 AND cha.Commodity_Id = rfp.COMMODITY_TYPE_ID
                  WHERE
                        srat.IS_BID_GROUP = 1
                        AND srat.SR_RFP_ACCOUNT_TERM_ID = @SR_RFP_ACCOUNT_TERM_ID
                  GROUP BY
                        cha.Meter_Country_Id
                       ,rfp.COMMODITY_TYPE_ID;
                       
      SELECT
            @Country_Cnt = count(DISTINCT Country_Id)
      FROM
            @Acc_Country_Commodity
                       
      INSERT      INTO @Templates
                  ( 
                   Sr_Service_Condition_Template_Id
                  ,Country_Id
                  ,Cnt )
                  SELECT
                        ssct.Sr_Service_Condition_Template_Id
                       ,sppcm.Country_Id
                       ,rank() OVER ( PARTITION BY ssct.Sr_Service_Condition_Template_Id ORDER BY sppcm.Country_Id ) AS Cnt
                  FROM
                        dbo.Sr_Service_Condition_Template ssct
                        INNER JOIN dbo.Sr_Service_Condition_Template_Product_Map ssctpm
                              ON ssct.Sr_Service_Condition_Template_Id = ssctpm.Sr_Service_Condition_Template_Id
                        INNER JOIN dbo.SR_Pricing_Product_Country_Map sppcm
                              ON ssctpm.SR_Pricing_Product_Country_Map_Id = sppcm.SR_Pricing_Product_Country_Map_Id
                        INNER JOIN dbo.SR_PRICING_PRODUCT spp
                              ON sppcm.SR_Pricing_Product_Id = spp.SR_PRICING_PRODUCT_ID
                  WHERE
                        EXISTS ( SELECT
                                    1
                                 FROM
                                    dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                    INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                          ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                 WHERE
                                    srsp.SR_RFP_SELECTED_PRODUCTS_ID = @Selected_Pricing_Product_Id
                                    AND spp.PRODUCT_NAME = srpp.PRODUCT_NAME )
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          @Acc_Country_Commodity acc
                                     WHERE
                                          spp.COMMODITY_TYPE_ID = acc.Commodity_Id
                                          AND sppcm.Country_Id = acc.Country_Id )
                                          
      SELECT
            @Sr_Service_Condition_Template_Id = tmplts.Sr_Service_Condition_Template_Id
      FROM
            @Templates tmplts
      WHERE
            Cnt = @Country_Cnt
      GROUP BY
            tmplts.Sr_Service_Condition_Template_Id
            
      SELECT
            @Sr_Service_Condition_Template_Id = isnull(@Sr_Service_Condition_Template_Id, srpp.Sr_Service_Condition_Template_Id)
      FROM
            dbo.SR_RFP_SELECTED_PRODUCTS srsp
            INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                  ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
      WHERE
            srsp.SR_RFP_SELECTED_PRODUCTS_ID = @Selected_Pricing_Product_Id
            
         
      DELETE FROM
            dbo.Sr_Rfp_Service_Condition_Template_Question_Map
      WHERE
            SR_RFP_ACCOUNT_TERM_ID = @SR_RFP_ACCOUNT_TERM_ID
            AND SR_RFP_SELECTED_PRODUCTS_ID = @Selected_Pricing_Product_Id
                       
      INSERT      INTO dbo.Sr_Rfp_Service_Condition_Template_Question_Map
                  ( 
                   Sr_Service_Condition_Template_Question_Map_Id
                  ,Created_User_Id
                  ,Created_Ts
                  ,Updated_User_Id
                  ,Last_Change_Ts
                  ,SR_RFP_ACCOUNT_TERM_ID
                  ,SR_RFP_SELECTED_PRODUCTS_ID
                  ,Sr_Service_Condition_Category_Id
                  ,Category_Display_Seq
                  ,Sr_Service_Condition_Question_Id
                  ,Question_Display_Seq
                  ,Is_Post_To_Supplier )
                  SELECT
                        ssctqm.Sr_Service_Condition_Template_Question_Map_Id
                       ,@User_Info_Id
                       ,getdate()
                       ,@User_Info_Id
                       ,getdate()
                       ,@SR_RFP_ACCOUNT_TERM_ID
                       ,@Selected_Pricing_Product_Id
                       ,ssctcm.Sr_Service_Condition_Category_Id
                       ,ssctcm.Display_Seq AS Category_Display_Seq
                       ,ssctqm.Sr_Service_Condition_Question_Id
                       ,ssctqm.Display_Seq AS Question_Display_Seq
                       ,1 AS Is_Post_To_Supplier
                  FROM
                        dbo.Sr_Service_Condition_Template_Question_Map ssctqm
                        INNER JOIN dbo.Sr_Service_Condition_Template_Category_Map ssctcm
                              ON ssctqm.Sr_Service_Condition_Template_Category_Map_Id = ssctcm.Sr_Service_Condition_Template_Category_Map_Id
                  WHERE
                        ssctcm.Sr_Service_Condition_Template_Id = @Sr_Service_Condition_Template_Id
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Sr_Rfp_Service_Condition_Template_Question_Map rfptqm
                                         WHERE
                                          rfptqm.SR_RFP_SELECTED_PRODUCTS_ID = @Selected_Pricing_Product_Id
                                          AND rfptqm.SR_RFP_ACCOUNT_TERM_ID = @SR_RFP_ACCOUNT_TERM_ID
                                          AND rfptqm.Sr_Service_Condition_Question_Id = ssctqm.Sr_Service_Condition_Question_Id )
            
            
END;
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Service_Condition_Template_Question_Map_Ins] TO [CBMSApplication]
GO
