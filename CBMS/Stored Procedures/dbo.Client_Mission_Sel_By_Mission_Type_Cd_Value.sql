SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******    
NAME:    
	dbo.Client_Mission_Sel_By_Mission_Type_Cd_Value    

DESCRIPTION:    
	Used to get data for Client Mission from CLIENT_MISSION , CODE , CODESET table  

INPUT PARAMETERS:    
Name				DataType		Default Description    
------------------------------------------------------------    
 @Client_Id			INT     
 @Code_Value		VARCHAR(100)
 @Std_Column_Name	VARCHAR(255) 'Mission_Type_Cd' 
          
OUTPUT PARAMETERS:    
Name				DataType		Default Description  
------------------------------------------------------------    
USAGE EXAMPLES:    
------------------------------------------------------------  

	EXEC dbo.Client_Mission_Sel_By_Mission_Type_Cd_Value 10069,'Constant'


AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
GB   Geetansu Behera    
CMH  Chad Hattabaugh     
HG   Hari   

MODIFICATIONS     
Initials	Date		Modification    
------------------------------------------------------------    
GB							Created  
SKA			17-Jul-2009		Modified as per coding standards
SS			06-SEP-2009		SP Renamed from Client_Mission_Sel_By_Code_Value to Client_Mission_Sel_By_Mission_Type_Cd_Value
							Removed the Parameter @Std_Column_Name and Included as Hardcoded Filter 
CMH			09/15/2009		Chagned @code_Value to @Mission-Type_Cd
							Removed Codeset link

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE  PROCEDURE [dbo].[Client_Mission_Sel_By_Mission_Type_Cd_Value]
      @Client_Id INT
     ,@Mission_Type_Name VARCHAR(25)
AS 

BEGIN    
 
      SET NOCOUNT ON ;    
     
      SELECT
            cm.CLIENT_ID
           ,cm.MISSION_TYPE_CD
           ,cm.MISSION_STATEMENT
      FROM
            dbo.CLIENT_MISSION cm
            INNER JOIN dbo.CODE cd ON cd.CODE_ID = cm.MISSION_TYPE_CD
      WHERE
            CLIENT_ID = @Client_Id
            AND cd.CODE_VALUE = @Mission_Type_Name    
    
         
END
GO
GRANT EXECUTE ON  [dbo].[Client_Mission_Sel_By_Mission_Type_Cd_Value] TO [CBMSApplication]
GO
