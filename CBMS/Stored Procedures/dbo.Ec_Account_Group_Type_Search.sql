SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Ec_Account_Group_Type_Search        
                
Description:                
		This sproc to get the cGroup types based country and state and commodity.        
                             
 Input Parameters:                
    Name						DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
	@Country_Id					INT  
	@State_Id					INT
	@Commodity_Id				INT
      
 Output Parameters:                      
    Name					  DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
----------------------------------------------------------------------------------------     
  
   Exec dbo.Ec_Account_Group_Type_Search
   
   Exec dbo.Ec_Account_Group_Type_Search 98,236,290

     
       
   
Author Initials:                
    Initials		Name                
----------------------------------------------------------------------------------------                  
	NR				Narayana Reddy                 
 Modifications:                
    Initials        Date			Modification                
----------------------------------------------------------------------------------------                  
    NR				2016-11-15		Created For MAINT-4563.           
               
******/   
CREATE PROCEDURE [dbo].[Ec_Account_Group_Type_Search]
      ( 
       @Country_Id INT = NULL
      ,@State_Id INT = NULL
      ,@Commodity_Id INT = NULL
      ,@Start_Index INT = 1
      ,@End_Index INT = 2147483647
      ,@Total_Count INT = 0 )
AS 
BEGIN
      SET NOCOUNT ON;
      
      IF @Total_Count = 0 
            BEGIN 
            
                  SELECT
                        @Total_Count = COUNT(1) OVER ( )
                  FROM
                        dbo.Ec_Account_Group_Type eagt
                        INNER JOIN dbo.STATE s
                              ON eagt.State_Id = s.STATE_ID
                        INNER JOIN dbo.COUNTRY c
                              ON s.COUNTRY_ID = c.COUNTRY_ID
                        INNER JOIN dbo.Commodity com
                              ON com.Commodity_Id = eagt.Commodity_Id
                  WHERE
                        ( @State_Id IS NULL
                          OR eagt.State_Id = @State_Id )
                        AND ( @Commodity_Id IS NULL
                              OR eagt.Commodity_Id = @Commodity_Id )
                        AND ( @Country_Id IS NULL
                              OR s.COUNTRY_ID = @Country_Id )
            
            END;
      
      
      WITH  Cte_Acc_Group
              AS ( SELECT TOP ( @End_Index )
                        eagt.Ec_Account_Group_Type_Id
                       ,eagt.Group_Type_Name
                       ,c.COUNTRY_ID
                       ,c.COUNTRY_NAME
                       ,s.STATE_NAME
                       ,com.Commodity_Name
                       ,com.Commodity_Id
                       ,s.STATE_ID
                       ,ROW_NUMBER() OVER ( PARTITION BY c.COUNTRY_NAME, s.STATE_NAME, com.Commodity_Name ORDER BY c.COUNTRY_NAME, eagt.Group_Type_Name ) Grp_Num
                       ,ROW_NUMBER() OVER ( ORDER BY c.COUNTRY_NAME, s.STATE_NAME, com.Commodity_Name ) Row_Num
                   FROM
                        dbo.Ec_Account_Group_Type eagt
                        INNER JOIN dbo.STATE s
                              ON eagt.State_Id = s.STATE_ID
                        INNER JOIN dbo.COUNTRY c
                              ON s.COUNTRY_ID = c.COUNTRY_ID
                        INNER JOIN dbo.Commodity com
                              ON com.Commodity_Id = eagt.Commodity_Id
                   WHERE
                        ( @State_Id IS NULL
                          OR eagt.State_Id = @State_Id )
                        AND ( @Commodity_Id IS NULL
                              OR eagt.Commodity_Id = @Commodity_Id )
                        AND ( @Country_Id IS NULL
                              OR s.COUNTRY_ID = @Country_Id ))
            SELECT
                  cag.Ec_Account_Group_Type_Id
                 ,cag.Group_Type_Name
                 ,cag.COUNTRY_ID
                 ,cag.COUNTRY_NAME
                 ,cag.STATE_NAME
                 ,cag.Commodity_Name
                 ,cag.Commodity_Id
                 ,cag.STATE_ID
                 ,CASE WHEN ecv.EC_Calc_Val_Id IS NULL THEN 0
                       ELSE 1
                  END Is_Used_In_Calc_Val
                 ,Grp_Num
                 ,Row_Num
                 ,@Total_Count Total_Rows
            FROM
                  Cte_Acc_Group cag
                  LEFT OUTER JOIN dbo.EC_Calc_Val ecv
                        ON cag.Ec_Account_Group_Type_Id = ecv.Ec_Account_Group_Type_Id
            WHERE
                  cag.Row_Num BETWEEN @Start_Index AND @End_Index
            GROUP BY
                  cag.Ec_Account_Group_Type_Id
                 ,cag.Group_Type_Name
                 ,cag.COUNTRY_ID
                 ,cag.COUNTRY_NAME
                 ,cag.STATE_NAME
                 ,cag.Commodity_Name
                 ,cag.Commodity_Id
                 ,cag.STATE_ID
                 ,CASE WHEN ecv.EC_Calc_Val_Id IS NULL THEN 0
                       ELSE 1
                  END
                 ,Grp_Num
                 ,Row_Num
            ORDER BY
                  Row_Num
           
END
      




;
GO
GRANT EXECUTE ON  [dbo].[Ec_Account_Group_Type_Search] TO [CBMSApplication]
GO
