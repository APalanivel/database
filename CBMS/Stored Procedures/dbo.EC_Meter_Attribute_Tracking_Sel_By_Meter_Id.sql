SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:   dbo.EC_Meter_Attribute_Tracking_Sel_By_Meter_Id                   
                          
Description:                          
   This sproc get the attribute tracking  details for the given meter id.                   
                          
 Input Parameters:                          
    Name        DataType   Default   Description                            
----------------------------------------------------------------------------------------                            
    @Meter_Id       INT            
                
 Output Parameters:                                
    Name        DataType   Default   Description                            
----------------------------------------------------------------------------------------                            
                          
 Usage Examples:                              
----------------------------------------------------------------------------------------               
            
   Exec dbo.EC_Meter_Attribute_Tracking_Sel_By_Meter_Id 804129            
               
   Exec dbo.EC_Meter_Attribute_Tracking_Sel_By_Meter_Id 804130            
               
             
Author Initials:                          
    Initials  Name                          
----------------------------------------------------------------------------------------                            
 NR    Narayana Reddy    
 SC    Sreenivasulu Cheerala                       
 Modifications:                          
    Initials        Date   Modification                          
----------------------------------------------------------------------------------------                            
    NR    2015-04-22  Created For AS400.
	SC		2020-05-13		Added addtional columns like Vendor type and Meter attribute types.         
******/
CREATE PROCEDURE [dbo].[EC_Meter_Attribute_Tracking_Sel_By_Meter_Id]
    (
        @Meter_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            emat.EC_Meter_Attribute_Tracking_Id
            , ema.EC_Meter_Attribute_Id
            , ema.EC_Meter_Attribute_Name
            , emat.EC_Meter_Attribute_Value
            , c.Code_Value AS Attribute_Type
            , emat.Start_Dt
            , emat.End_Dt
            , emat.Meter_Id
            , ema.Vendor_Type_Cd
            , emat.Meter_Attribute_Type_Cd
            , cd.Code_Value Meter_Attribute_Name
            , cdd.Code_Value Vendor_Type_Value
        FROM
            dbo.EC_Meter_Attribute ema
            INNER JOIN dbo.EC_Meter_Attribute_Tracking emat
                ON ema.EC_Meter_Attribute_Id = emat.EC_Meter_Attribute_Id
            INNER JOIN Code c
                ON c.Code_Id = ema.Attribute_Type_Cd
            LEFT JOIN dbo.Code cd
                ON cd.Code_Id = emat.Meter_Attribute_Type_Cd
            LEFT JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
            LEFT JOIN dbo.Code cdd
                ON cdd.Code_Id = ema.Vendor_Type_Cd
        WHERE
            emat.Meter_Id = @Meter_Id
            AND cs.Codeset_Name = 'MeterAttributeType'
            AND cd.Code_Value = 'Actual'
        GROUP BY
            emat.EC_Meter_Attribute_Tracking_Id
            , ema.EC_Meter_Attribute_Id
            , ema.EC_Meter_Attribute_Name
            , emat.EC_Meter_Attribute_Value
            , c.Code_Value
            , emat.Start_Dt
            , emat.End_Dt
            , emat.Meter_Id
            , ema.Vendor_Type_Cd
            , emat.Meter_Attribute_Type_Cd
            , cd.Code_Value
            , cdd.Code_Value;

    END;
GO
GRANT EXECUTE ON  [dbo].[EC_Meter_Attribute_Tracking_Sel_By_Meter_Id] TO [CBMSApplication]
GO
