SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Manual_Site_Group_Sel_By_Site_Id]  
     
DESCRIPTION: 
	To Get Manual Site Group Detail For given Site Id and which are having is_smart_group = 0 in sitegroup table.
      
INPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
	@Site_Id		INT			  		
               
OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        

	EXEC dbo.Manual_Site_Group_Sel_By_Site_Id 18983
	
	EXEC dbo.Manual_Site_Group_Sel_By_Site_Id 22111
	
AUTHOR INITIALS:          
	INITIALS	NAME          
------------------------------------------------------------          
	PNR			PANDARINATH
          
MODIFICATIONS:           
	INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
	PNR			25-JUN-10	CREATED   
  
*/

CREATE PROCEDURE dbo.Manual_Site_Group_Sel_By_Site_Id
    (
       @Site_Id		INT
    )
AS
BEGIN

    SET NOCOUNT ON;

  	SELECT
		 sg.Sitegroup_Name
		,cd.Code_Value Group_Type
		,ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME UserName
	FROM
		dbo.Sitegroup sg
		JOIN dbo.Sitegroup_Site sgs
			 ON sgs.Sitegroup_id = sg.Sitegroup_Id
		JOIN dbo.Code cd
			 ON cd.Code_Id = sg.Sitegroup_Type_Cd
		JOIN dbo.USER_INFO ui
			 ON sg.Add_User_Id = ui.USER_INFO_ID
	WHERE
		sgs.Site_id = @Site_Id
		AND sg.Is_Smart_Group = 0
		AND sg.Is_Complete  = 1
		AND cd.Code_Value IN ('User','Global')

END
GO
GRANT EXECUTE ON  [dbo].[Manual_Site_Group_Sel_By_Site_Id] TO [CBMSApplication]
GO
