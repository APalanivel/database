SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	cbms_prod.dbo.cbmsUbmInvoice_Get

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId	Int
	@ubm_invoice_id	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC cbmsUbmInvoice_Get 49, 2029962
	
	SELECT * FROM sys.procedures where name ='cbmsUbmInvoice_Get'
	sp_helptext cbmsUbmInvoice_Get
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan G
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script
	HG			06/15/2010	Amount_Due column added for the Duplicate test enhancement.

******/

CREATE PROCEDURE dbo.cbmsUbmInvoice_Get
	(
	@MyAccountId INT
	,@Ubm_Invoice_Id INT
	)
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT 
		ml.ubm_id
		, ubm.ubm_name
		, i.ubm_invoice_id
		, i.invoice_identifier
		, i.cbms_image_id
		, i.ubm_account_id
		, i.ubm_account_number
		, i.ubm_vendor_id
		, i.ubm_vendor_name
		, i.ubm_client_id ubm_client_code
		, i.ubm_client_name
		, i.currency ubm_currency_code
		, MIN(det.service_start_date) service_start_date
		, MAX(det.service_end_date) service_end_date
		, MAX(det.billing_days) billing_days
		, i.current_charges
		, i.AMOUNT_DUE
	FROM
		dbo.UBM_BATCH_MASTER_LOG ml
		INNER JOIN dbo.UBM_INVOICE i
			ON i.ubm_batch_master_log_id = ml.ubm_batch_master_log_id
		JOIN dbo.UBM
			ON ubm.ubm_id = ml.ubm_id
		LEFT OUTER JOIN dbo.UBM_INVOICE_DETAILS det
			ON det.ubm_invoice_id = i.ubm_invoice_id
	WHERE
		i.ubm_invoice_id = @ubm_invoice_id
	GROUP BY
		ml.ubm_id
		, ubm.ubm_name
		, i.ubm_invoice_id
		, i.invoice_identifier
		, i.cbms_image_id
		, i.ubm_account_id
		, i.ubm_account_number
		, i.ubm_vendor_id
		, i.ubm_vendor_name
		, i.ubm_client_id
		, i.ubm_client_name
		, i.currency
		, i.current_charges
		, i.AMOUNT_DUE

END
GO
GRANT EXECUTE ON  [dbo].[cbmsUbmInvoice_Get] TO [CBMSApplication]
GO
