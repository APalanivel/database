SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
                           
 NAME:  dbo.IC_Bots_Process_Invoice_Collection_Account_Config_Map_Sel_By_IC_Account_Config_Id                     
                            
 DESCRIPTION:        
				TO get the Bot  information for IC Data .                            
                            
 INPUT PARAMETERS:        
                           
 Name                                   DataType        Default       Description        
------------------------------------------------------------------------------------   
@Invoice_Collection_Account_Config_Id	INT
		                          
 OUTPUT PARAMETERS:        
                                 
 Name                                   DataType        Default       Description        
------------------------------------------------------------------------------------   
                            
 USAGE EXAMPLES:                                
------------------------------------------------------------------------------------   
 

 Exec dbo.IC_Bots_Process_Invoice_Collection_Account_Config_Map_Sel_By_IC_Account_Config_Id 1   
 
                           
 AUTHOR INITIALS:      
         
 Initials               Name        
------------------------------------------------------------------------------------   
 NR                     Narayana Reddy                              
                             
 MODIFICATIONS:      
                             
 Initials               Date            Modification      
------------------------------------------------------------------------------------   
 NR                     2020-06-03      Created for SE2017-963 BOT Process.                          
                           
******/

CREATE  PROC [dbo].[IC_Bots_Process_Invoice_Collection_Account_Config_Map_Sel_By_IC_Account_Config_Id]
     (
         @Invoice_Collection_Account_Config_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;



        SELECT
            ibpicacm.Invoice_Collection_Account_Config_Id
            , ibpicacm.Enroll_In_Bot
            , ibpicacm.IC_Bots_Process_Id
            , ibp.IC_Bots_Process_Name
        FROM
            dbo.IC_Bots_Process_Invoice_Collection_Account_Config_Map ibpicacm
            LEFT JOIN dbo.IC_Bots_Process ibp
                ON ibp.IC_Bots_Process_Id = ibpicacm.IC_Bots_Process_Id
        WHERE
            ibpicacm.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id;



    END;

GO
GRANT EXECUTE ON  [dbo].[IC_Bots_Process_Invoice_Collection_Account_Config_Map_Sel_By_IC_Account_Config_Id] TO [CBMSApplication]
GO
