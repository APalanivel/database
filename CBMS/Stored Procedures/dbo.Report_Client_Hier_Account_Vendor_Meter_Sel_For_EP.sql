
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                 
                 
NAME: Report_Client_Hier_Account_Vendor_Meter_Sel_For_NG
    
DESCRIPTION:          
    
   This proceduere will return meter and vendor details of NG commodity and meter country 4 except vendor name like Landlord

    
INPUT PARAMETERS:    
NAME   DATATYPE DEFAULT  DESCRIPTION    
------------------------------------------------------------     
    
OUTPUT PARAMETERS:    
NAME     DATATYPE DEFAULT  DESCRIPTION    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
    exec dbo.Report_Client_Hier_Account_Vendor_Meter_Sel_For_NG
AUTHOR INITIALS:    
INITIALS	NAME    
------------------------------------------------------------
 KVK		Vinay k

MODIFICATIONS    
INITIALS	DATE		MODIFICATION    
------------------------------------------------------------
	KVK		4/6/2016		created(requested by Ian Lawrence & Lynn Cox)
	KVK		5/11/2016		included CANADA
    
*/ 
CREATE PROCEDURE [dbo].[Report_Client_Hier_Account_Vendor_Meter_Sel_For_EP]
AS
BEGIN
      SET NOCOUNT ON;
      WITH  details_cte
              AS ( SELECT
                        cha.Meter_City
                       ,cha.Meter_Country_Name
                       ,cha.Meter_State_Name
                       ,cha.Account_Vendor_Name
                       ,count(cha.Account_Vendor_Name) AS VendorRecords
                   FROM
                        CBMS.Core.Client_Hier_Account cha
                   WHERE
                        cha.Commodity_Id = 290
                        AND cha.Account_Type = 'Utility'
                        AND cha.Meter_Country_Id IN (1, 4)
                        AND cha.Account_Vendor_Name NOT LIKE 'Landlord%'
                   GROUP BY
                        cha.Meter_City
                       ,Account_Vendor_Name
                       ,cha.Meter_State_Name
                       ,Meter_Country_Name),
            maxdata_cte
              AS ( SELECT
                        max(dc.VendorRecords) maxvr
                       ,dc.Meter_City
                       ,dc.Meter_State_Name
                   FROM
                        details_cte dc
                   GROUP BY
                        dc.Meter_City
                       ,dc.Meter_State_Name)
            SELECT
                  dc.Meter_City AS City
                 ,dc.Meter_State_Name AS State_Province
                 ,dc.Meter_Country_Name AS Country
                 ,count(dc2.Account_Vendor_Name) AS [EP Utilities]
                 ,dc.Account_Vendor_Name AS [Primary EP Utility]
                 ,'(' + cast(dc.VendorRecords AS VARCHAR(5)) + ' of ' + cast(sum(dc2.VendorRecords) AS VARCHAR(5)) + ')' AS [Primary Match Count]
                 ,round(cast(dc.VendorRecords AS DECIMAL(6, 2)) / cast(sum(dc2.VendorRecords) AS DECIMAL(6, 2)), 2) AS [Primary Match %]
            FROM
                  maxdata_cte mx
                  JOIN details_cte dc
                        ON dc.Meter_City = mx.Meter_City
                           AND dc.Meter_State_Name = mx.Meter_State_Name
                           AND dc.VendorRecords = mx.maxvr
                  INNER JOIN details_cte dc2
                        ON dc2.Meter_City = mx.Meter_City
                           AND mx.Meter_State_Name = dc2.Meter_State_Name
--where dc.Meter_City = 'Anderson'
GROUP BY
                  dc.Meter_City
                 ,dc.Meter_State_Name
                 ,dc.Account_Vendor_Name
                 ,dc.VendorRecords
                 ,dc.Meter_Country_Name;
END;


;
GO

GRANT EXECUTE ON  [dbo].[Report_Client_Hier_Account_Vendor_Meter_Sel_For_EP] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_Client_Hier_Account_Vendor_Meter_Sel_For_EP] TO [CBMSApplication]
GO
