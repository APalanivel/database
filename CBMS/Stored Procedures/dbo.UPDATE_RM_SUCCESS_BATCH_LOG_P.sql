SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.UPDATE_RM_SUCCESS_BATCH_LOG_P

@userId varchar(10),
@sessionId varchar(10),
@batchIdentity int

as
	set nocount on
update RM_REPORT_BATCH_LOG set LOG_TIME=GETDATE() where RM_REPORT_ID=@batchIdentity
GO
GRANT EXECUTE ON  [dbo].[UPDATE_RM_SUCCESS_BATCH_LOG_P] TO [CBMSApplication]
GO
