SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   Procedure dbo.seStorage_GetForYear

(
		@Year varchar(4)
)

As
BEGIN
	set nocount on
	 select StorageId
				, ReportDate
				, WeekEnding
				, WeekOfYear
				, Volume
				, Change
		 from seStorage
			where year(reportdate) = @Year
 order by ReportDate desc
END
GO
GRANT EXECUTE ON  [dbo].[seStorage_GetForYear] TO [CBMSApplication]
GO
