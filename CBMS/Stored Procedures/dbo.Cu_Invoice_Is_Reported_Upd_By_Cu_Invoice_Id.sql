SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
            
Name:   
	dbo.Cu_Invoice_Is_Reported_Upd_By_Cu_Invoice_Id        
              
Description:              
			To update Cu_invoice is reported flag.
              
 Input Parameters:              
    Name					DataType		Default		Description                
---------------------------------------------------------------------------------
	@Cu_Invoice_Id				INT
	@Is_Reported				BIT
	@User_Info_Id				INT
    
    
Output Parameters:                    
    Name					DataType		Default		Description                
---------------------------------------------------------------------------------
              
Usage Examples:                  
---------------------------------------------------------------------------------

BEGIN TRAN
SELECT  * FROM  dbo.Cu_Invoice ac WHERE ac.Cu_Invoice_Id = 15591

EXEC dbo.Cu_Invoice_Is_Reported_Upd_By_Cu_Invoice_Id 15591, 53755, 291

SELECT  * FROM  dbo.Cu_Invoice ac WHERE ac.Cu_Invoice_Id = 15591
    


ROLLBACK TRAN
	
	
   
	
Author Initials:              
    Initials	Name              
---------------------------------------------------------------------------------
	NR			Narayana Reddy
 
Modifications:              
	Initials    Date		Modification              
---------------------------------------------------------------------------------
    NR			2018-11-01	Created for D20-327.         
             
******/

CREATE PROCEDURE [dbo].[Cu_Invoice_Is_Reported_Upd_By_Cu_Invoice_Id]
     (
         @Cu_Invoice_Id INT
         , @Is_Reported BIT = 0
         , @User_Info_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;

        UPDATE
            ci
        SET
            ci.IS_REPORTED = @Is_Reported
            , ci.UPDATED_BY_ID = @User_Info_Id
            , ci.UPDATED_DATE = GETDATE()
        FROM
            dbo.CU_INVOICE ci
        WHERE
            ci.CU_INVOICE_ID = @Cu_Invoice_Id;


    END;
    ;


GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Is_Reported_Upd_By_Cu_Invoice_Id] TO [CBMSApplication]
GO
