SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.GET_CEMS_UNDER_TEAM_P 
@userId varchar,
@sessionId varchar,
@teamId int
as
select	rcem.CEM_USER_ID,
	userinfo.LAST_NAME,
	userinfo.FIRST_NAME

from	ENTITY entity,
	RM_CEM_TEAM rcem,
	USER_INFO userinfo

WHERE	entity.ENTITY_ID=rcem.CEM_TEAM_TYPE_ID AND
	rcem.CEM_USER_ID=userinfo.USER_INFO_ID  AND
	entity.ENTITY_ID=@teamId order by LAST_NAME
GO
GRANT EXECUTE ON  [dbo].[GET_CEMS_UNDER_TEAM_P] TO [CBMSApplication]
GO
