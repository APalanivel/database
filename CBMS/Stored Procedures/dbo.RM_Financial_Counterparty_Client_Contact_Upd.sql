SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/**

NAME: dbo.RM_Financial_Counterparty_Client_Contact_Upd

    
DESCRIPTION:    

      To check duplicate client contact


INPUT PARAMETERS:    
     NAME					DATATYPE		DEFAULT           DESCRIPTION    
------------------------------------------------------------------------    
	@Client_Id				INT		

                    

OUTPUT PARAMETERS:              
      NAME              DATATYPE    DEFAULT           DESCRIPTION       
-------------------------------------------------------------------------              


USAGE EXAMPLES:              
-------------------------------------------------------------------------              
	BEGIN TRANSACTION
		SELECT TOP 5 * FROM dbo.Client_Contact_Map a INNER JOIN dbo.Contact_Info b ON a.Contact_Info_Id = b.Contact_Info_Id
			WHERE a.CLIENT_ID  = 10003 ORDER BY b.Last_Change_Ts DESC
		EXEC dbo.RM_Financial_Counterparty_Client_Contact_Upd @Client_Id=10003,@First_Name='Test',@Last_Name='DB',@Email_Address='rvintha@ctepl.com'
		,@User_Info_Id = 16,@Contact_Info_Id = 13228,@Phone_Number = '007'
		SELECT TOP 5 * FROM dbo.Client_Contact_Map a INNER JOIN dbo.Contact_Info b ON a.Contact_Info_Id = b.Contact_Info_Id
			WHERE a.CLIENT_ID  = 10003 ORDER BY b.Last_Change_Ts DESC
	ROLLBACK TRANSACTION

	 

AUTHOR INITIALS:              
	INITIALS	NAME              
------------------------------------------------------------              
    RR			Raghu Reddy


MODIFICATIONS:    
	INITIALS	DATE		MODIFICATION              
------------------------------------------------------------              
	RR 			31-07-2018	Created - Global Risk Management

**/ 
CREATE PROCEDURE [dbo].[RM_Financial_Counterparty_Client_Contact_Upd]
      ( 
       @Client_Id INT
      ,@Counterparty_Id INT
      ,@First_Name NVARCHAR(60)
      ,@Last_Name NVARCHAR(60)
      ,@Email_Address NVARCHAR(150)
      ,@Phone_Number NVARCHAR(60) = NULL
      ,@Mobile_Number NVARCHAR(60) = NULL
      ,@Fax_Number NVARCHAR(60) = NULL
      ,@User_Info_Id INT
      ,@Contact_Info_Id INT )
AS 
BEGIN
	
      SET NOCOUNT ON;
         
      UPDATE
            ci
      SET   
            ci.First_Name = @First_Name
           ,ci.Last_Name = @Last_Name
           ,ci.Email_Address = @Email_Address
           ,ci.Phone_Number = @Phone_Number
           ,ci.Fax_Number = @Fax_Number
           ,ci.Mobile_Number = @Mobile_Number
           ,ci.Last_Change_Ts = GETDATE()
           ,ci.Updated_User_Id = @User_Info_Id
      FROM
            Trade.Financial_Counterparty_Client_Contact_Map ccm
            INNER JOIN dbo.Contact_Info ci
                  ON ccm.Contact_Info_Id = ci.Contact_Info_Id
      WHERE
            ccm.CLIENT_ID = @Client_Id
            AND ccm.RM_COUNTERPARTY_ID = @Counterparty_Id
            AND ccm.Contact_Info_Id = @Contact_Info_Id
     
				

END;
GO
GRANT EXECUTE ON  [dbo].[RM_Financial_Counterparty_Client_Contact_Upd] TO [CBMSApplication]
GO
