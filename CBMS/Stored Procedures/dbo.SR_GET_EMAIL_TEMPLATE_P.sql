SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--EXEC SR_GET_EMAIL_TEMPLATE_P '1', '1', 'Email-018-SWProfile_SA_SupplierContact'

CREATE    PROCEDURE [dbo].[SR_GET_EMAIL_TEMPLATE_P] 

@emailName varchar(100)

AS


BEGIN
	set nocount on
	select subject, content from sr_email_template, entity
	where entity.entity_name = @emailName and entity.entity_type = 1033
	and entity.entity_id = sr_email_template.email_type_id

END
GO
GRANT EXECUTE ON  [dbo].[SR_GET_EMAIL_TEMPLATE_P] TO [CBMSApplication]
GO
