SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Utility_Dtl_Volume_Requirement_Sel

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Vendor_ID				INT
	@Commodity_Id			INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
 EXEC Utility_Dtl_Volume_Requirement_Sel 30, 290
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	SKA			01/12/2011	Created
******/

CREATE PROC [dbo].[Utility_Dtl_Volume_Requirement_Sel]
      ( 
       @Vendor_ID INT
      ,@Commodity_Id INT )
AS 
BEGIN
      SET NOCOUNT ON ; 
	
      SELECT
            utl_vol.Utility_Dtl_Volume_Requirement_Id
           ,qcm.Question_Commodity_Map_Id
           ,usq.Question_Label
           ,utl_vol.Utility_Volume_Dsc
           ,utl_vol.Utility_UOM_Cd
           ,CD.Code_Value AS UOM_Name
           ,utl_vol.Time_Period
           ,utl_vol.Time_Req
           ,utl_vol.Other_Req
      FROM
            dbo.Question_Commodity_Map qcm
            INNER JOIN dbo.Utility_Summary_Question usq
                  ON usq.Utility_Summary_Question_Id = qcm.Utility_Summary_Question_Id
            INNER JOIN dbo.Utility_Summary_Section uss
                  ON uss.Utility_Summary_Section_Id = usq.Utility_Summary_Section_Id
            INNER JOIN VENDOR_COMMODITY_MAP vcm
                  ON vcm.COMMODITY_TYPE_ID = qcm.Commodity_Id
            LEFT JOIN dbo.Utility_Dtl_Volume_Requirement utl_vol
                  ON vcm.VENDOR_COMMODITY_MAP_ID = utl_vol.VENDOR_COMMODITY_MAP_ID
                     AND qcm.Question_Commodity_Map_Id = utl_vol.Question_Commodity_Map_Id
            LEFT JOIN dbo.Code CD
                  ON CD.Code_Id = utl_vol.Utility_UOM_Cd
      WHERE
            vcm.VENDOR_ID = @Vendor_ID
            AND vcm.COMMODITY_TYPE_ID = @Commodity_Id
            AND usq.Is_Active = 1
            AND uss.Section_Label = 'Volume Requirement to transport'
      ORDER BY
            usq.Display_Order
END
GO
GRANT EXECUTE ON  [dbo].[Utility_Dtl_Volume_Requirement_Sel] TO [CBMSApplication]
GO
