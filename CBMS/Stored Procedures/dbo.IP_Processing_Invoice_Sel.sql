SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******  
NAME:  
  dbo.IP_Processing_Invoice_Sel  
  
DESCRIPTION:  
  
  
INPUT PARAMETERS:  
 Name       DataType  Default Description  
------------------------------------------------------------  
  Processing_Cu_Invoice_Id   INT  
  User_Info_ID            INT               
  
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
  
  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 SP         SRINIVASARAO PATCHAVA  
  
MODIFICATIONS  
  
 Initials Date  Modification  
------------------------------------------------------------  
 SP         16/10/2018   CREATED  
  
******/  
CREATE PROCEDURE [dbo].[IP_Processing_Invoice_Sel]
      ( 
       @Processing_Cu_Invoice_Id INT
      ,@User_Info_ID INT )
AS 
BEGIN  
      SET NOCOUNT ON;  
  
      SELECT
            Processing_Cu_Invoice_Id
           ,Cu_Invoice_Id
           ,Account_Id
           ,Service_Month
           ,Is_Reported
           ,Is_Previously_Reported
           ,User_Info_ID
           ,Last_Updated_Ts
      FROM
            dbo.IP_Processing_Invoice
      WHERE
            Processing_Cu_Invoice_Id = @Processing_Cu_Invoice_Id
            AND User_Info_ID = @User_Info_ID;  
  
  
END;  
                
GO
GRANT EXECUTE ON  [dbo].[IP_Processing_Invoice_Sel] TO [CBMSApplication]
GO
