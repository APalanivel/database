SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
            
/******              
NAME:              
 dbo.Report_Contract_Meter_Volume_details_test              
               
DESCRIPTION:              
 This Procedure is to get all Contract Meter Volumes              
               
INPUT PARAMETERS:              
Name    DataType  Default Description              
-------------------------------------------------------------------------------------------              
@ContractExpiration DATETIME             
@Default_UOM_Id  INT              
@frequency_type  VARCHAR(200)            
@fromDate DATE          
@toDate DATE            
              
              
OUTPUT PARAMETERS:              
Name   DataType Default Description              
------------------------------------------------------------              
              
USAGE EXAMPLES:              
-------------------------------------------------------------              
Report_DE_Contract_Meter_Volume_details_By_Client '1011',25,'Monthly','01/01/2012','12/01/2012'            
Report_DE_Contract_Meter_Volume_details_By_Client '11610',25,'Monthly','01/01/2012','02/01/2012'              
Report_DE_Contract_Meter_Volume_details_By_Client 10003,25,'Monthly','11/01/2011' ,'12/01/2011'              
Report_DE_Contract_Meter_Volume_details_By_Client 10003,25,'Monthly','2012-06-31','2012-07-31'              
               
AUTHOR INITIALS:              
Initials Name              
------------------------------------------------------------              
AKR      Ashok Kumar Raju    
    
 MODIFICATIONS Initials Date  Modification              
------------------------------------------------------------              
 AKR   2013-10-21  Created    
 AKR   2014-03-18  Modifed the query, to sum ContractMeterVolume data  
*/              
          
CREATE PROCEDURE dbo.Report_DE_Contract_Meter_Volume_details_By_Client      
      (       
       @ClientId INT      
      ,@Default_UOM_Id INT      
      ,@frequency_type VARCHAR(7) = 'Monthly'      
      ,@fromDate DATE      
      ,@toDate DATE )      
AS       
BEGIN                
                    
      SET NOCOUNT ON                   
                
      SELECT      
            sit.Site_Id      
           ,cmv.MONTH_IDENTIFIER      
           ,SUM(CASE WHEN freq_type.entity_name = 'Daily'      
                      AND @frequency_type = 'Monthly' THEN ( ( cmv.VOLUME * dd.Days_In_Month_Num ) * cuc.CONVERSION_FACTOR )      
                 WHEN freq_type.entity_name IN ( 'Month', '30-day Cycle' )      
                      AND @frequency_type = 'Daily' THEN ( ( cmv.VOLUME / dd.Days_In_Month_Num ) * cuc.CONVERSION_FACTOR )      
                 WHEN freq_type.entity_name = CASE WHEN @frequency_type = 'Monthly' THEN 'Month'      
                                                   ELSE @frequency_type      
                                              END THEN ( cmv.VOLUME * cuc.CONVERSION_FACTOR )      
                 ELSE NULL      
            END) AS ContractMeterVolume      
      FROM      
            dbo.CONTRACT con      
            JOIN dbo.supplier_account_meter_map map      
                  ON map.contract_id = con.contract_id      
            JOIN dbo.ACCOUNT suppacc      
                  ON suppacc.account_id = map.account_id      
            JOIN Core.Client_Hier_Account ca      
                  ON ca.Meter_Id = map.METER_ID      
                     AND ca.Account_Type = 'Utility'      
            JOIN Core.Client_Hier ch      
                  ON ch.Client_Hier_Id = ca.Client_Hier_Id      
            JOIN dbo.SITE sit      
                  ON sit.SITE_ID = ch.Site_Id      
            JOIN dbo.Commodity cm      
                  ON cm.Commodity_Id = con.COMMODITY_TYPE_ID      
                     AND cm.Commodity_Name = 'Natural Gas'      
            JOIN dbo.ENTITY S_type      
                  ON S_type.ENTITY_ID = sit.SITE_TYPE_ID      
            LEFT JOIN dbo.CONTRACT_METER_VOLUME cmv      
                  ON cmv.CONTRACT_ID = con.CONTRACT_ID      
                     AND cmv.METER_ID = ca.Meter_Id      
            LEFT JOIN dbo.ENTITY freq_type    
                  ON freq_type.ENTITY_ID = cmv.FREQUENCY_TYPE_ID      
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc      
                  ON cuc.BASE_UNIT_ID = cmv.UNIT_TYPE_ID      
                     AND cuc.CONVERTED_UNIT_ID = @Default_UOM_Id      
            LEFT JOIN dbo.ENTITY ent_uofm      
                  ON ent_uofm.ENTITY_ID = cmv.UNIT_TYPE_ID      
                     AND ent_uofm.ENTITY_DESCRIPTION IN ( 'Unit for Gas' )      
            INNER JOIN meta.Date_Dim dd      
                  ON dd.Date_D = cmv.MONTH_IDENTIFIER      
      WHERE      
            map.is_history <> 1      
            AND CH.CLIENT_ID = @ClientId      
            AND cmv.MONTH_IDENTIFIER BETWEEN @fromDate      
                                     AND     @toDate      
            AND ca.Account_Not_Managed = 0      
            AND ch.Site_Not_Managed = 0      
            AND freq_type.entity_name IS NOT NULL      
            AND cmv.VOLUME <> 0      
      GROUP BY      
            sit.Site_Id      
           ,cmv.MONTH_IDENTIFIER      
            
             
END     
;
;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Contract_Meter_Volume_details_By_Client] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_Contract_Meter_Volume_details_By_Client] TO [CBMSApplication]
GO
