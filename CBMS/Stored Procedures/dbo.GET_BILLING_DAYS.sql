SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_BILLING_DAYS

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@ACCOUNT_TYPE  	varchar(10)	          	
	@ACCOUNT_ID    	int       	          	
	@START_DATE    	datetime  	          	
	@END_DATE      	datetime  	          	
	@RATE_ID       	datetime  	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE PROCEDURE dbo.GET_BILLING_DAYS
(
@ACCOUNT_TYPE VARCHAR(10),
@ACCOUNT_ID INT,
@START_DATE DATETIME,
@END_DATE DATETIME,
@RATE_ID DATETIME
)

AS
set nocount on
BEGIN
DECLARE @RATE_DATE DATETIME
DECLARE @DATEDIFF1 INT

SELECT @DATEDIFF1 =  DATEDIFF(DAY,@START_DATE,@END_DATE) 
IF @START_DATE <>  NULL
	SET @RATE_DATE = @START_DATE
ELSE IF @END_DATE <> NULL
	SET @RATE_DATE = @END_DATE
ELSE 
	SET @RATE_DATE = NULL

IF @ACCOUNT_TYPE =  'Supplier' --Supplier Account
BEGIN 
	SELECT @DATEDIFF1 + 1
END

ELSE  IF  @RATE_DATE = NULL--Utility Account
	SELECT  @DATEDIFF1 + 0
ELSE  IF @RATE_ID = NULL                                           --Utility Account
SELECT @DATEDIFF1 + (
	select max(isNull(rs.billing_days_adjustment, 0)) billing_days_adjustment
	from account a
	join vwAccountMeter vam on vam.account_id = a.account_id
	join meter m on m.meter_id = vam.meter_id
	join rate r on r.rate_id = m.rate_id
	left outer join rate_schedule rs on rs.rate_id = r.rate_id and rs.rs_start_date <= @RATE_DATE and rs.rs_end_date >= @RATE_DATE
	where a.account_id = @account_id
)

ELSE                                           --Utility Account
SELECT @DATEDIFF1 + 1



END
GO
GRANT EXECUTE ON  [dbo].[GET_BILLING_DAYS] TO [CBMSApplication]
GO
