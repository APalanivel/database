SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:		Change_Control_Process_Table_Change_Info

DESCRIPTION:
	Activated by Change_Control_Queue, it processes messages
	of //Change_Control/Message/Table_Change_Info


INPUT PARAMETERS:
	Name						DataType		Default	Description
------------------------------------------------------------
	@Message					XML					-- Message associated to the conversation
	,@Conversation_Handle		UNIQUEIDENTIFIER	-- Paramter exists for consistancy only	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
 -- Procedure is only executed by Change_Control_Receive_Table_Changes
 when a message type is Recieved

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CMH		Chad Hattabaugh
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	CMH			11/04/2011	Created
******/
CREATE PROCEDURE [dbo].[Change_Control_Process_Table_Change_Info]
(	@Message				XML
	,@Conversation_Handle	UNIQUEIDENTIFIER  ) -- Paramter exists for consistancy only
AS 
BEGIN 
	SET NOCOUNT ON; 
	
	DECLARE 
		@Table_Name		VARCHAR(255)
		,@Change_Count	INT
		,@Ct_Message XML 

	-- Get the Table name and Change Count from the message 
	SELECT 						
		@Table_Name = cng.ch.value('Table_Name[1]', 'Varchar(255)') 
		,@Change_Count = cng.ch.value('Change_Count[1]', 'int')
	FROM 
		@Message.nodes('/Table_Changes/Table_Change_Info') cng(ch)

	-- Update teh thresholds for the table name
	UPDATE dbo.Change_Control_Threshold 
	SET Current_Change_Count = Current_Change_Count + @Change_Count
	WHERE Table_Name = @Table_Name
	
	-- Create a message to pass 
	SET @Ct_Message = '<Check_Threshold><Table_Name>' + @Table_Name + '</Table_Name></Check_Threshold>'

	;SEND ON CONVERSATION @Conversation_Handle MESSAGE TYPE [//Change_Control/Message/Check_Threshold] (@Ct_Message)
	;END CONVERSATION @Conversation_Handle
END

GO
GRANT EXECUTE ON  [dbo].[Change_Control_Process_Table_Change_Info] TO [CBMSApplication]
GO
