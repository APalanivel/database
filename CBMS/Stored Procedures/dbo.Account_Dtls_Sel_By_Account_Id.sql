SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
	dbo.Account_Dtls_Sel_By_Account_Id  

DESCRIPTION:   


INPUT PARAMETERS:  
	Name			DataType			Default			Description  
-----------------------------------------------------------------------  
	@Account_Id     INT                     

OUTPUT PARAMETERS:  
	Name			DataType			Default			Description  
-----------------------------------------------------------------------  


USAGE EXAMPLES:  
-----------------------------------------------------------------------  

	
	EXEC dbo.Account_Dtls_Sel_By_Account_Id 1149261
	
	EXEC dbo.Account_Dtls_Sel_By_Account_Id 1148289

	USE CBMS

AUTHOR INITIALS:  
	Initials	Name  
-----------------------------------------------------------------------  
	NR			Narayana Reddy
 
MODIFICATIONS:
	Initials	Date			Modification  
-----------------------------------------------------------------------  
	NR			2017-03-16		Created For Contract Placeholder.
	NR			2019-03-01		Watch List - Removed Acc Processing instrcution table.
	NR			2019-04-03		Data2.0 - Removed WATCHLIST-GROUP-INFO-ID  column from Account Table.

******/

CREATE PROCEDURE [dbo].[Account_Dtls_Sel_By_Account_Id]
    (
        @Account_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;


        DECLARE @Group_Legacy_Name TABLE
              (
                  GROUP_INFO_ID INT
                  , GROUP_NAME VARCHAR(200)
                  , Legacy_Group_Name VARCHAR(200)
              );

        INSERT INTO @Group_Legacy_Name
             (
                 GROUP_INFO_ID
                 , GROUP_NAME
                 , Legacy_Group_Name
             )
        EXEC dbo.Group_Info_Sel_By_Group_Legacy
            @Group_Legacy_Name_Cd_Value = 'Regulated_Market';
        WITH Cte_Account_Meter
        AS (
               SELECT
                    a.ACCOUNT_ID
                    , m.METER_ID
                    , a.ACCOUNT_TYPE_ID
                    , a.ACCOUNT_NUMBER
                    , a.ACCOUNT_GROUP_ID
                    , a.SITE_ID
                    , a.VENDOR_ID
                    , NULL AS Contract_ID
                    , NULL AS Supplier_Account_Begin_Dt
                    , NULL AS Supplier_Account_End_Dt
                    , a.INVOICE_SOURCE_TYPE_ID
                    , a.Supplier_Account_Recalc_Type_Cd
                    , a.SERVICE_LEVEL_TYPE_ID
                    , m.PURCHASE_METHOD_TYPE_ID
                    , m.ADDRESS_ID
                    , a.IS_HISTORY
                    , a.IS_IN_INVOICE_CHECKLIST
                    , a.Is_Data_Entry_Only
                    , cd.Code_Value Recalc_Type
                    , com.Commodity_Name
                    , com.Commodity_Id
               FROM
                    dbo.METER m
                    INNER JOIN dbo.ACCOUNT a
                        ON a.ACCOUNT_ID = m.ACCOUNT_ID
                    INNER JOIN dbo.RATE r
                        ON m.RATE_ID = r.RATE_ID
                    INNER JOIN dbo.Commodity com
                        ON r.COMMODITY_TYPE_ID = com.Commodity_Id
                    LEFT OUTER JOIN dbo.Account_Commodity_Invoice_Recalc_Type acirt
                        ON acirt.Account_Id = a.ACCOUNT_ID
                           AND  acirt.Commodity_ID = r.COMMODITY_TYPE_ID
                    LEFT OUTER JOIN dbo.Code cd
                        ON acirt.Invoice_Recalc_Type_Cd = cd.Code_Id
               WHERE
                    a.ACCOUNT_ID = @Account_Id
               UNION
               SELECT
                    map.ACCOUNT_ID
                    , map.METER_ID
                    , a.ACCOUNT_TYPE_ID
                    , a.ACCOUNT_NUMBER
                    , a.ACCOUNT_GROUP_ID
                    , uacc.SITE_ID
                    , a.VENDOR_ID
                    , map.Contract_ID
                    , a.Supplier_Account_Begin_Dt
                    , a.Supplier_Account_End_Dt
                    , a.INVOICE_SOURCE_TYPE_ID
                    , a.Supplier_Account_Recalc_Type_Cd
                    , a.SERVICE_LEVEL_TYPE_ID
                    , m.PURCHASE_METHOD_TYPE_ID
                    , m.ADDRESS_ID
                    , a.IS_HISTORY
                    , a.IS_IN_INVOICE_CHECKLIST
                    , a.Is_Data_Entry_Only
                    , cd.Code_Value Recalc_Type
                    , com.Commodity_Name
                    , com.Commodity_Id
               FROM
                    dbo.SUPPLIER_ACCOUNT_METER_MAP AS map
                    JOIN dbo.ACCOUNT a
                        ON a.ACCOUNT_ID = map.ACCOUNT_ID
                    JOIN dbo.METER m
                        ON m.METER_ID = map.METER_ID
                    JOIN dbo.ACCOUNT uacc
                        ON uacc.ACCOUNT_ID = m.ACCOUNT_ID
                    INNER JOIN dbo.RATE r
                        ON m.RATE_ID = r.RATE_ID
                    INNER JOIN dbo.Commodity com
                        ON r.COMMODITY_TYPE_ID = com.Commodity_Id
                    LEFT OUTER JOIN dbo.Account_Commodity_Invoice_Recalc_Type acirt
                        ON acirt.Account_Id = m.ACCOUNT_ID
                           AND  acirt.Commodity_ID = r.COMMODITY_TYPE_ID
                    LEFT OUTER JOIN dbo.Code cd
                        ON acirt.Invoice_Recalc_Type_Cd = cd.Code_Id
               WHERE
                    (   map.METER_DISASSOCIATION_DATE > a.Supplier_Account_Begin_Dt
                        OR  map.METER_DISASSOCIATION_DATE IS NULL)
                    AND a.ACCOUNT_ID = @Account_Id
           )
        SELECT
            cteacc.ACCOUNT_ID
            , cteacc.ACCOUNT_GROUP_ID
            , group_billing_number = CASE WHEN cteacc.ACCOUNT_GROUP_ID IS NULL THEN 'Not part of Group Bill'
                                         ELSE ISNULL(ag.GROUP_BILLING_NUMBER, 'Unspecified Group Number')
                                     END
            , cl.CLIENT_ID
            , cl.CLIENT_NAME
            , s.DIVISION_ID
            , cl.UBM_SERVICE_ID
            , ISNULL(us.UBM_SERVICE_NAME, 'None') AS ubm_service_name
            , cteacc.VENDOR_ID
            , v.VENDOR_NAME
            , s.SITE_ID
            , cteacc.INVOICE_SOURCE_TYPE_ID
            , ist.ENTITY_NAME AS invoice_source_type
            , cteacc.SERVICE_LEVEL_TYPE_ID
            , sl.ENTITY_NAME AS service_level_type
            , ISNULL(cteacc.ACCOUNT_NUMBER, 'Not Yet Assigned') AS account_number
            , cteacc.ACCOUNT_TYPE_ID
            , act.ENTITY_NAME AS account_type
            , cteacc.IS_HISTORY
            , cteacc.IS_IN_INVOICE_CHECKLIST
            , ui.USER_INFO_ID AS analyst_id
            , ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME AS analyst
            , ui.QUEUE_ID
            , con.CONTRACT_ID
            , con.ED_CONTRACT_NUMBER
            , cteacc.Supplier_Account_Begin_Dt
            , cteacc.Supplier_Account_End_Dt
            , cteacc.Supplier_Account_Recalc_Type_Cd
            , CASE WHEN act.ENTITY_NAME = 'Utility' THEN 'Utility Recalc'
                  ELSE crt.Code_Dsc
              END AS contract_recalc_type
            , uir.USER_INFO_ID AS region_mgr_id
            , cteacc.Is_Data_Entry_Only
            , mmap.Contract_ID
            , mmap.METER_ID
            , con.CONTRACT_TYPE_ID
            , cteacc.Recalc_Type
            , cteacc.Commodity_Name
            , cteacc.Commodity_Id
        FROM
            Cte_Account_Meter cteacc
            JOIN ADDRESS ad
                ON cteacc.ADDRESS_ID = ad.ADDRESS_ID
            JOIN dbo.SITE AS s
                ON s.SITE_ID = ad.ADDRESS_PARENT_ID
            JOIN dbo.CLIENT AS cl
                ON cl.CLIENT_ID = s.Client_ID
            JOIN dbo.VENDOR AS v
                ON v.VENDOR_ID = cteacc.VENDOR_ID
            JOIN dbo.ENTITY AS act
                ON act.ENTITY_ID = cteacc.ACCOUNT_TYPE_ID
            LEFT OUTER JOIN dbo.ACCOUNT_GROUP AS ag
                ON ag.ACCOUNT_GROUP_ID = cteacc.ACCOUNT_GROUP_ID
            LEFT OUTER JOIN dbo.ENTITY AS ist
                ON ist.ENTITY_ID = cteacc.INVOICE_SOURCE_TYPE_ID
            LEFT OUTER JOIN dbo.UBM_SERVICE AS us
                ON us.UBM_SERVICE_ID = cl.UBM_SERVICE_ID
            LEFT OUTER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP AS mmap
                ON mmap.ACCOUNT_ID = cteacc.ACCOUNT_ID
                   AND  mmap.METER_ID = cteacc.METER_ID
            LEFT OUTER JOIN dbo.CONTRACT AS con
                ON con.CONTRACT_ID = mmap.Contract_ID
            LEFT OUTER JOIN dbo.Code AS crt
                ON crt.Code_Id = cteacc.Supplier_Account_Recalc_Type_Cd
            LEFT OUTER JOIN dbo.ENTITY AS sl
                ON sl.ENTITY_ID = cteacc.SERVICE_LEVEL_TYPE_ID
            LEFT OUTER JOIN dbo.ADDRESS AS addr
                ON addr.ADDRESS_ID = s.PRIMARY_ADDRESS_ID
            LEFT OUTER JOIN dbo.STATE AS st
                ON st.STATE_ID = addr.STATE_ID
            LEFT OUTER JOIN dbo.REGION AS reg
                ON reg.REGION_ID = st.REGION_ID
            LEFT OUTER JOIN dbo.REGION_MANAGER_MAP AS rmm
                ON rmm.REGION_ID = reg.REGION_ID
            LEFT OUTER JOIN dbo.USER_INFO AS uir
                ON uir.USER_INFO_ID = rmm.USER_INFO_ID
            LEFT JOIN(dbo.UTILITY_DETAIL ud
                      JOIN Utility_Detail_Analyst_Map udmap
                          ON udmap.Utility_Detail_ID = ud.UTILITY_DETAIL_ID
                      JOIN GROUP_INFO gi
                          ON gi.GROUP_INFO_ID = udmap.Group_Info_ID
                      INNER JOIN @Group_Legacy_Name gil
                          ON gi.GROUP_INFO_ID = gil.GROUP_INFO_ID)
                ON ud.VENDOR_ID = cteacc.VENDOR_ID
            LEFT OUTER JOIN USER_INFO ui
                ON ui.USER_INFO_ID = udmap.Analyst_ID
        GROUP BY
            cteacc.ACCOUNT_ID
            , cteacc.ACCOUNT_GROUP_ID
            , group_billing_number
            , cl.CLIENT_ID
            , cl.CLIENT_NAME
            , s.DIVISION_ID
            , cl.UBM_SERVICE_ID
            , ISNULL(us.UBM_SERVICE_NAME, 'None')
            , cteacc.VENDOR_ID
            , v.VENDOR_NAME
            , s.SITE_ID
            , cteacc.INVOICE_SOURCE_TYPE_ID
            , ist.ENTITY_NAME
            , cteacc.SERVICE_LEVEL_TYPE_ID
            , sl.ENTITY_NAME
            , ISNULL(cteacc.ACCOUNT_NUMBER, 'Not Yet Assigned')
            , cteacc.ACCOUNT_TYPE_ID
            , act.ENTITY_NAME
            , cteacc.IS_HISTORY
            , cteacc.IS_IN_INVOICE_CHECKLIST
            , ui.USER_INFO_ID
            , ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME
            , ui.QUEUE_ID
            , con.CONTRACT_ID
            , con.ED_CONTRACT_NUMBER
            , cteacc.Supplier_Account_Begin_Dt
            , cteacc.Supplier_Account_End_Dt
            , cteacc.Supplier_Account_Recalc_Type_Cd
            , crt.Code_Dsc
            , uir.USER_INFO_ID
            , cteacc.Is_Data_Entry_Only
            , mmap.Contract_ID
            , mmap.METER_ID
            , con.CONTRACT_TYPE_ID
            , cteacc.Recalc_Type
            , cteacc.Commodity_Name
            , cteacc.Commodity_Id;

    END;
    ;
    ;



GO
GRANT EXECUTE ON  [dbo].[Account_Dtls_Sel_By_Account_Id] TO [CBMSApplication]
GO
