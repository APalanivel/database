SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE       PROCEDURE [dbo].[cbmsVendor_GetByType]
	( @MyAccountId int 
	, @vendor_type_id int = null
	, @state_id int = null
	, @country_id int = null
	, @region_id int = null
	)
AS
BEGIN
	set nocount on
	   select distinct v.vendor_id
		, v.vendor_name
	     from vendor v
  left outer join vendor_state_map vs on vs.vendor_id = v.vendor_id
  left outer join state st on st.state_id = vs.state_id
	    where ((v.vendor_type_id = isNull(@vendor_type_id, v.vendor_type_id)) or (v.vendor_type_id is null and @vendor_type_id is null))
	      and ((st.country_id = isNull(@country_id, st.country_id)) or (st.country_id is null and @country_id is null))
	      and ((st.region_id = isNull(@region_id, st.region_id)) or (st.region_id is null and @region_id is null))
   	      and ((st.state_id = isNull(@state_id, st.state_id)) or (st.state_id is null and @state_id is null))
	 order by v.vendor_name

END
GO
GRANT EXECUTE ON  [dbo].[cbmsVendor_GetByType] TO [CBMSApplication]
GO
