SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GET_SITE_IMAGE_P]
	@SiteId INT
AS
BEGIN

  SET NOCOUNT ON

	SELECT c.cbms_doc_id
		, c.CBMS_IMAGE_ID
	FROM dbo.Site s   
		INNER JOIN dbo.Cbms_Image c ON c.Cbms_Image_Id = s.Cbms_Image_Id
	WHERE s.Site_Id = @SiteId

END
GO
GRANT EXECUTE ON  [dbo].[GET_SITE_IMAGE_P] TO [CBMSApplication]
GO
