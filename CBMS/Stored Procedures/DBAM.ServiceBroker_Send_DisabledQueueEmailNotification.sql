SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [DBAM].[ServiceBroker_Send_DisabledQueueEmailNotification]
      WITH EXECUTE AS OWNER
AS
DECLARE @ch UNIQUEIDENTIFIER;
DECLARE @messagetypename NVARCHAR(256);
DECLARE @messagebody XML;
DECLARE @queueName VARCHAR(100);
DECLARE @emailTo VARCHAR(500);
DECLARE @Profile sysname;
	
	
	
IF @@Servername LIKE '01DW%'
      BEGIN
            SELECT
                  @emailTo = email_address
            FROM
                  msdb.dbo.sysoperators
            WHERE
                  name = 'DBA Team';
      END;
ELSE
      BEGIN
            SELECT TOP 1
                  @emailTo = pager_address + ',' + email_address
            FROM
                  msdb.dbo.sysoperators
            WHERE
                  name = 'DBA Team';
      END;

SELECT TOP 1
      @Profile = name
FROM
      msdb.dbo.sysmail_profile
ORDER BY
      CASE WHEN name = 'CriticalEvents' THEN 1
           ELSE 2
      END;

WHILE ( 1 = 1 )
      BEGIN
            BEGIN TRY
                  BEGIN TRANSACTION;

                  WAITFOR (
				RECEIVE TOP(1)
					@ch = Conversation_Handle,
					@messagetypename = message_type_name,
					@messagebody = cast(MESSAGE_Body AS XML)
				FROM dbam_DisabledQueueNotificationQueue
			), TIMEOUT 60000;

                  IF ( @@ROWCOUNT = 0 )
                        BEGIN
                              ROLLBACK TRANSACTION;
                              BREAK;
                        END;

                  IF ( @messagetypename = 'http://schemas.microsoft.com/SQL/Notifications/EventNotification' )
                        BEGIN
                              SET @queueName = 'Disabled queue: ' + @messagebody.value('/EVENT_INSTANCE[1]/ObjectName[1]', 'VARCHAR(100)');
				
                              EXEC msdb.dbo.sp_send_dbmail
                                    @profile_name = @Profile
                                   ,@recipients = @emailTo
                                   ,@body = @queueName
                                   ,@subject = @queueName;
				
                        END;
			
                  IF ( @messagetypename = 'http://schemas.microsoft.com/SQL/ServiceBroker/Error' )
                        BEGIN
                              DECLARE @errorcode INT;
                              DECLARE @errormessage NVARCHAR(3000);
                  -- Extract the error information from the sent message
                              SET @errorcode = ( SELECT
                                                      @messagebody.value(N'declare namespace brokerns="http://schemas.microsoft.com/SQL/ServiceBroker/Error"; 
                        (/brokerns:Error/brokerns:Code)[1]', 'int') );
                              SET @errormessage = ( SELECT
                                                      @messagebody.value('declare namespace brokerns="http://schemas.microsoft.com/SQL/ServiceBroker/Error";
                        (/brokerns:Error/brokerns:Description)[1]', 'nvarchar(3000)') );

                  -- Log the error

                  -- End the conversation on the initiator's side
                              END CONVERSATION @ch;
                        END;


                  IF ( @messagetypename = 'http://schemas.microsoft.com/SQL/ServiceBroker/EndDialog' )
                        BEGIN
				-- End the conversation
                              END CONVERSATION @ch;
                        END;

                  COMMIT TRANSACTION;
            END TRY
            BEGIN CATCH
                  ROLLBACK TRANSACTION;
                  DECLARE @ErrorNum INT;
                  DECLARE @ErrorMsg NVARCHAR(3000);
                  SELECT
                        @ErrorNum = error_number()
                       ,@ErrorMsg = error_message();
			-- log the error
                  BREAK;
            END CATCH;
      END;
GO
