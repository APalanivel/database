SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.COMMENT_UPD

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	 @COMMENT_ID   INT
	 @COMMENT_TEXT   VARCHAR(MAX)  

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN
		EXEC COMMENT_UPD 8, 'TEST'
	ROLLBACK TRAN
	
	SELECT * FROM Comment WHERE COMMENT_ID = 8
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	SKA			01/28/2011	Created
******/

CREATE PROCEDURE dbo.COMMENT_UPD
      @COMMENT_ID INT
     ,@COMMENT_TEXT VARCHAR(MAX)
AS 
BEGIN  
      SET nocount ON ;  

      UPDATE
            dbo.COMMENT
      SET   
            COMMENT_TEXT = @COMMENT_TEXT
      WHERE
            COMMENT_ID = @COMMENT_ID
END 

GO
GRANT EXECUTE ON  [dbo].[COMMENT_UPD] TO [CBMSApplication]
GO
