SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE dbo.GET_PARENT_CHARGE_MASTER_P
	@parentMasterId INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT cm.charge_master_id,
		cm.charge_display_id, 
		cm.charge_parent_id, 
		charge_parent_type_id, 
		charge_type_id 
	FROM dbo.charge_master cm
	WHERE charge_master_id = @parentMasterId

END
GO
GRANT EXECUTE ON  [dbo].[GET_PARENT_CHARGE_MASTER_P] TO [CBMSApplication]
GO
