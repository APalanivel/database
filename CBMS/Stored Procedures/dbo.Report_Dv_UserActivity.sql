
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 dbo.Report_Dv_UserActivity
 
 DESCRIPTION:   
 
 This procedure is to fetch all user\users of Client\Clients from Session Info and Sesssion Activity  

INPUT PARAMETERS:  
Name			DataType	Default Description  
----------------------------------------------------------  
@@Client_ID			INT  
@UserID_List		VARCHAR(MAX)  
@Access_Begin_date  DATETIME
@Access_End_Date	DATETIME

OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
----------------------------------------------------------  

USAGE EXAMPLES:  
----------------------------------------------------------  

Exec dbo.Report_Dv_UserActivity 1043,'204,205','12/01/2007','07/20/2010'
Exec dbo.Report_Dv_UserActivity 1043,'204,205,206,207'
Exec dbo.Report_Dv_UserActivity 1043,'206,207'

AUTHOR INITIALS:  

 Initials	Name  
----------------------------------------------------------  
 SSR		Sharad srivastava  
 
 MODIFICATIONS:  
 Initials	Date		 Modification  
----------------------------------------------------------  
 SSR         06/14/2010  Created  
 SSR		 07/16/2010  Removed WildCard search condition from Front of IP address.
 
TO DO:
	Need to uncomment Email Address filter condition while moving it to production .

******/    
CREATE PROC [dbo].[Report_Dv_UserActivity]
    @Client_ID INT
  , @UserID_List VARCHAR(MAX)
  , @Access_Begin_date DATETIME
  , @Access_End_Date DATETIME
AS 
    BEGIN  
  
        SET NOCOUNT ON ;
        
        WITH    CTE_User_info
                  AS ( SELECT
                        ui.user_info_id
                      , ui.username
                      , ui.first_name
                      , ui.last_name
                      , ui.email_address
                      , cl.client_id
                      , cl.client_name
                       FROM
                        dbo.user_info ui
                        JOIN dbo.CLIENT cl
                            ON cl.client_id = ui.client_id
                        JOIN dbo.ufn_split(@UserID_List, ',') ufn_User_tmp
                            ON ufn_User_tmp.Segments = CAST(ui.USER_INFO_ID AS VARCHAR)
                       WHERE
                        cl.CLIENT_ID = @Client_ID
                        AND ui.is_history = 0
                        AND ui.access_level = 1  
       --AND ui.email_address NOT LIKE '%summitenergy%'  
                        
                     )
            SELECT
                user_inf.username
              , user_inf.first_name
              , user_inf.last_name
              , user_inf.email_address
              , user_inf.client_name
              , x.Cnt [Count of Logins]
              , x.Maxdate [Last Login]
            FROM
                CTE_User_info user_inf
                LEFT JOIN ( SELECT
                                COUNT(DISTINCT si.session_info_id) Cnt
                              , MAX(sa.access_date) Maxdate
                              , si.USER_INFO_ID
                            FROM
                                dbo.DV2_Session_Info si
                                JOIN dbo.DV2_session_activity sa
                                    ON sa.SESSION_INFO_ID = si.SESSION_INFO_ID
                            WHERE
                                sa.ACCESS_DATE BETWEEN @Access_Begin_date
                                               AND     @Access_End_Date
                                and si.session_info_id not in (select session_info_id from dv2_session_activity where page_url like '%/rider.aspx%')
                                AND si.ip_address NOT LIKE '10.%'
                                AND si.ip_address NOT LIKE '172.16%'
                                AND si.ip_address NOT LIKE '172.17%'
                                AND si.ip_address NOT LIKE '172.18%'
                                AND si.ip_address NOT LIKE '172.19%'
                                AND si.ip_address NOT LIKE '172.20%'
                                AND si.ip_address NOT LIKE '172.21%'
                                AND si.ip_address NOT LIKE '172.22%'
                                AND si.ip_address NOT LIKE '172.23%'
                                AND si.ip_address NOT LIKE '172.24%'
                                AND si.ip_address NOT LIKE '172.25%'
                                AND si.ip_address NOT LIKE '172.26%'
                                AND si.ip_address NOT LIKE '172.27%'
                                AND si.ip_address NOT LIKE '172.28%'
                                AND si.ip_address NOT LIKE '172.29%'
                                AND si.ip_address NOT LIKE '172.30%'
                                AND si.ip_address NOT LIKE '172.31%'
                                AND si.ip_address NOT LIKE '192.0.8%'
                                AND si.ip_address NOT LIKE '192.168.%'
                            GROUP BY
                                si.USER_INFO_ID
                          ) x
                    ON x.USER_INFO_ID = user_inf.USER_INFO_ID
            GROUP BY
                user_inf.username
              , user_inf.first_name
              , user_inf.last_name
              , user_inf.email_address
              , user_inf.client_name
              , x.Cnt
              , x.Maxdate  
    END


GO
GRANT EXECUTE ON  [dbo].[Report_Dv_UserActivity] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_Dv_UserActivity] TO [CBMSApplication]
GO
