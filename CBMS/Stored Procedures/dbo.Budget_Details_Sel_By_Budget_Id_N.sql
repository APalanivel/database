SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
    
/******                              
NAME:                              
 [dbo].[Budget_Details_Sel_By_Budget_Id]                              
                              
DESCRIPTION:                              
 This procedure gets details of all accounts associated with a particular budget id                              
                                         
INPUT PARAMETERS:                              
 Name              DataType          Default     Description                              
----------------------------------------------------------------                              
   @Budget_Id   int                              
                                   
OUTPUT PARAMETERS:                              
 Name              DataType          Default     Description                              
----------------------------------------------------------------                              
                              
USAGE EXAMPLES:                              
------------------------------------------------------------                              
 EXEC dbo.Budget_Details_Sel_By_Budget_Id 8037                     
                                       
AUTHOR INITIALS:                              
Initials    Name        
------------------------------------------------------------                              
  VN     Varada Nair      
  RK     Raghu Kalvapudi                              
                                               
MODIFICATIONS:                              
 Initials      Date           Modification                              
------------------------------------------------------------        
   VN  07-March-2013  Created.                           
   VN  1-04-2013      Modified to get value of b.IS_POSTED_TO_DV                          
   VN  16-04-2013     Modified to get budget usage from previous years if not present in  for                     
   RK  05/06/2013     Added code to calculate Variable and Sourcing Tax.           
   VN  05/13/2013     Added condition to get values from details for EP with Tariff account.        
   RK  05/13/2013     Modified Queries to remove Views and fix JIRA issues (COMMBGT- 135,133).       
   VN  05/13/2013     Changed hard coded values to variables.    
   RK  05/14/2013     Modified CASE statement to address missing scenarios in calculation of variable and transportation.     
   RK  05/15/2013     Rewrote query to remove WHILE Loop ( which was causing Performance Issues).
   RK  05/16/2013     Rewrote Total usage calculation for perforamance.
   RK  05/20/2013     Added Logic to fix JIRA issues COMMBGT - 116/132 .
   RK  05/23/2013     Made changes as per Review comments.
   RK  06/19/2013     fix for MAINT-1935 (Is_Posted_To_DV).                   
******/                              
                              
CREATE PROCEDURE [dbo].[Budget_Details_Sel_By_Budget_Id_N] ( @Budget_Id INT )
AS 
BEGIN                              
                              
      SET NOCOUNT ON                          
                               
      DECLARE
            @service_level_type_id INT
           ,@service_level_type INT
           ,@EP_Commodity_Id INT
           ,@Commodity_Id INT
           ,@Is_Posted_to_DV BIT
           ,@EP_UOM_Id INT
           ,@NG_UOM_Id INT
           ,@Budget_UOM_Id INT
           ,@Currency_Id INT
           ,@Currency_Group_Id INT
           ,@accounttype INT
           ,@Suppliertype INT
           ,@Bucket_Master_Id INT
           ,@Start_Dt DATE
           ,@End_Dt DATE
           ,@CU_Start_Dt DATE
           ,@CU_End_Dt DATE 
                
      SELECT
            @service_level_type_id = e.ENTITY_ID
      FROM
            dbo.ENTITY e
      WHERE
            e.ENTITY_NAME = 'C'                        
                    
                    
      SELECT
            @service_level_type = e.ENTITY_ID
      FROM
            dbo.ENTITY e
      WHERE
            e.ENTITY_NAME = 'D'                        
                   
                   
      SELECT
            @EP_Commodity_Id = e.ENTITY_ID
      FROM
            dbo.ENTITY e
      WHERE
            e.ENTITY_TYPE = 157
            AND e.ENTITY_NAME = 'Electric Power'                   
         
      SELECT
            @accounttype = ety.ENTITY_ID
      FROM
            dbo.ENTITY ety
      WHERE
            ety.ENTITY_TYPE = 111
            AND ety.ENTITY_NAME = 'Utility'              
  
  
      SELECT
            @Suppliertype = ety.ENTITY_ID
      FROM
            dbo.ENTITY ety
      WHERE
            ety.ENTITY_TYPE = 129
            AND ety.ENTITY_NAME = 'Supplier'   
   
                     
      SELECT
            @Commodity_Id = b.COMMODITY_TYPE_ID
           ,@Is_Posted_to_DV = b.IS_POSTED_TO_DV
      FROM
            dbo.BUDGET b
      WHERE
            b.BUDGET_ID = @Budget_Id        
    
      SELECT
            @EP_UOM_Id = e.ENTITY_ID
      FROM
            dbo.ENTITY e
      WHERE
            e.ENTITY_NAME = 'KWH'
            AND e.ENTITY_TYPE = 101               
              
              
      SELECT
            @NG_UOM_Id = e.ENTITY_ID
      FROM
            dbo.ENTITY e
      WHERE
            e.ENTITY_NAME = 'MMBtu'
            AND e.ENTITY_TYPE = 102               
              
      SELECT
            @Bucket_Master_Id = bm.Bucket_Master_Id
      FROM
            dbo.Bucket_Master AS bm
      WHERE
            bm.Commodity_Id = @commodity_id
            AND bm.Bucket_Name = 'Total Usage'
 
      SELECT
            @Budget_UOM_Id = case WHEN @Commodity_Id = @EP_Commodity_Id THEN @EP_UOM_Id
                                  ELSE @NG_UOM_Id
                             END
 
              
      SELECT
            @Currency_Id = cu.CURRENCY_UNIT_ID
      FROM
            dbo.CURRENCY_UNIT cu
      WHERE
            cu.CURRENCY_UNIT_NAME = 'USD'              
              
      SELECT
            @Currency_Group_Id = ch.Client_Currency_Group_Id
      FROM
            core.Client_Hier ch
            INNER JOIN dbo.BUDGET b
                  ON ch.Client_Id = b.CLIENT_ID
                     AND ch.Sitegroup_Id = 0                           
                                 
                            
      SELECT
            @Start_Dt = max(bd.MONTH_IDENTIFIER)
      FROM
            dbo.BUDGET_DETAILS bd
            INNER JOIN dbo.BUDGET_ACCOUNT ba
                  ON bd.BUDGET_ACCOUNT_ID = ba.BUDGET_ACCOUNT_ID
            INNER JOIN dbo.BUDGET b
                  ON ba.BUDGET_ID = b.BUDGET_ID
      WHERE
            ba.BUDGET_ID = @Budget_Id
            AND ba.IS_DELETED = 0
            AND bd.MONTH_IDENTIFIER <= case b.BUDGET_START_MONTH
                                         WHEN 1 THEN cast(cast(b.BUDGET_START_YEAR AS VARCHAR) + '-' + cast(b.BUDGET_START_MONTH AS VARCHAR) + '-01' AS DATE)
                                         ELSE cast(cast(b.BUDGET_START_YEAR - 1 AS VARCHAR) + '-' + cast(b.BUDGET_START_MONTH AS VARCHAR) + '-01' AS DATE)
                                       END                      
      SELECT
            @End_Dt = dateadd(mm, 11, @Start_Dt);                       
    
      SELECT
            @CU_Start_Dt = dateadd(yy, -3, @Start_Dt)
           ,@CU_End_Dt = dateadd(yy, -1, @End_Dt)
    
      DECLARE @Budget_Accounts TABLE ( Account_Id INT )
       
      INSERT      INTO @Budget_Accounts
                  ( 
                   Account_Id )
                  SELECT
                        ba.ACCOUNT_ID
                  FROM
                        dbo.BUDGET_ACCOUNT ba
                  WHERE
                        ba.BUDGET_ID = @Budget_Id
                        AND ba.IS_DELETED = 0;
      WITH  Account_Rates
              AS ( SELECT
                        ba1.Account_Id
                       ,stuff(x.Rate_Name, 1, 2, '') AS Rate_Name
                   FROM
                        @Budget_Accounts ba1
                        CROSS APPLY ( SELECT
                                          ', ' + Rate_Name
                                      FROM
                                          dbo.RATE r
                                          INNER JOIN dbo.METER m
                                                ON r.RATE_ID = m.RATE_ID
                                          INNER JOIN @Budget_Accounts ba2
                                                ON m.ACCOUNT_ID = ba2.Account_Id
                                      WHERE
                                          r.COMMODITY_TYPE_ID = @Commodity_Id
                                          AND ba2.Account_Id = ba1.Account_Id
                                      GROUP BY
                                          ba2.Account_Id
                                         ,r.RATE_NAME
                        FOR
                                      XML PATH('') ) x ( Rate_Name ))
            SELECT
                  b.IS_POSTED_TO_DV
                 ,b.BUDGET_ID
                 ,ba.ACCOUNT_ID
                 ,cha.ACCOUNT_NUMBER
                 ,cha.Account_Vendor_Id AS VENDOR_ID
                 ,case WHEN max(cha1.Account_Id) IS NOT NULL THEN 'Transport'
                       ELSE 'Tariff'
                  END AS tariff_transport
                 ,ch.Client_Id
                 ,ch.Client_Name
                 ,ar.RATE_NAME
                 ,ch.Site_Id
                 ,rtrim(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' AS Site_Name
                 ,cte.ENTITY_ID
                 ,cte.ENTITY_NAME
                 ,b.BUDGET_NAME
                 ,ch.Country_Name
                 ,cha.Account_Vendor_Name AS VENDOR_NAME
                 ,b.budget_start_year AS start_year
                 ,b.budget_start_month AS start_month
                 ,ba.budget_account_id
                 ,bdc.DISTRIBUTION_COMMENTS
                 ,bdc.OTHER_BUNDLED_COMMENTS
                 ,bdc.OTHER_FIXED_COSTS_COMMENTS
                 ,bdc.RATES_TAX_COMMENTS
                 ,bdc.SOURCING_TAX_COMMENTS
                 ,bdc.TRANSMISSION_COMMENTS
                 ,bdc.TRANSPORTATION_COMMENTS
                 ,bdc.VARIABLE_COMMENTS
                 ,ba.SOURCING_COMPLETED_DATE
                 ,ba.SOURCING_COMPLETED_BY
                 ,ba.RATES_COMPLETED_DATE
                 ,ba.RATES_COMPLETED_BY
                 ,ba.RATES_REVIEWED_DATE
                 ,ba.RATES_REVIEWED_BY
            FROM
                  dbo.BUDGET b
                  INNER JOIN dbo.BUDGET_ACCOUNT ba
                        ON ba.BUDGET_ID = b.BUDGET_ID
                  INNER JOIN Core.Client_Hier_Account AS cha
                        ON ba.ACCOUNT_ID = cha.Account_Id
                           AND b.COMMODITY_TYPE_ID = cha.Commodity_Id
                  LEFT  JOIN dbo.BUDGET_DETAIL_COMMENTS bdc
                        ON bdc.BUDGET_ACCOUNT_ID = ba.BUDGET_ACCOUNT_ID
                  INNER JOIN dbo.ENTITY sle
                        ON sle.ENTITY_ID = cha.Account_Service_level_Cd
                  INNER JOIN Core.Client_Hier ch
                        ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  LEFT JOIN Account_Rates ar
                        ON ba.ACCOUNT_ID = ar.ACCOUNT_ID
                  LEFT JOIN ( Core.Client_Hier_Account AS cha1
                              INNER JOIN dbo.[CONTRACT] AS c
                                    ON cha1.Supplier_Contract_ID = c.CONTRACT_ID
                                       AND c.contract_end_date >= getdate()
                                       AND cha1.Account_Type = 'Supplier'
                              INNER JOIN dbo.Entity conType
                                    ON conType.Entity_id = c.contract_type_id
                                       AND ConType.Entity_Name = 'Supplier' )
                              ON cha.Meter_Id = cha1.Meter_Id
                  INNER JOIN dbo.ENTITY cte
                        ON cte.ENTITY_ID = b.COMMODITY_TYPE_ID
            WHERE
                  b.budget_id = @Budget_Id
                  AND ba.IS_DELETED = 0
            GROUP BY
                  b.IS_POSTED_TO_DV
                 ,b.BUDGET_ID
                 ,ba.ACCOUNT_ID
                 ,cha.ACCOUNT_NUMBER
                 ,cha.Account_Vendor_Id
                 ,ch.Client_Id
                 ,ch.Client_Name
                 ,ch.Site_Id
                 ,ch.city
                 ,ch.state_name
                 ,ch.site_name
                 ,cte.ENTITY_ID
                 ,cte.ENTITY_NAME
                 ,b.BUDGET_NAME
                 ,ar.RATE_NAME
                 ,ch.Country_Name
                 ,cha.Account_Vendor_Name
                 ,b.budget_start_year
                 ,b.budget_start_month
                 ,ba.budget_account_id
                 ,bdc.DISTRIBUTION_COMMENTS
                 ,bdc.OTHER_BUNDLED_COMMENTS
                 ,bdc.OTHER_FIXED_COSTS_COMMENTS
                 ,bdc.RATES_TAX_COMMENTS
                 ,bdc.SOURCING_TAX_COMMENTS
                 ,bdc.TRANSMISSION_COMMENTS
                 ,bdc.TRANSPORTATION_COMMENTS
                 ,bdc.VARIABLE_COMMENTS
                 ,ba.SOURCING_COMPLETED_DATE
                 ,ba.SOURCING_COMPLETED_BY
                 ,ba.RATES_COMPLETED_DATE
                 ,ba.RATES_COMPLETED_BY
                 ,ba.RATES_REVIEWED_DATE
                 ,ba.RATES_REVIEWED_BY                 
      
               
      SELECT
            ba.BUDGET_ID
           ,ba.BUDGET_ACCOUNT_ID
           ,ba.ACCOUNT_ID
           ,bd.MONTH_IDENTIFIER
           ,@Commodity_Id commodity_type_id
           ,ba.HAS_DETAILS_SAVED
           ,cha.Account_Service_level_Cd SERVICE_LEVEL_TYPE_ID
           ,bd.variable
           ,bd.transmission
           ,bd.transportation
           ,case WHEN max(cha1.Account_Id) IS NOT NULL THEN 'Transport'
                 ELSE 'Tariff'
            END AS tariff_transport
           ,bd.distribution
           ,bd.other_bundled
           ,bd.other_fixed_costs
           ,bd.sourcing_tax
           ,bd.rates_tax
           ,bd.is_manual_generation
           ,bd.is_manual_sourcing_tax
      INTO
            #Temp_Budget_Details
      FROM
            dbo.BUDGET_ACCOUNT ba
            INNER JOIN dbo.BUDGET_DETAILS bd
                  ON ba.BUDGET_ACCOUNT_ID = bd.BUDGET_ACCOUNT_ID
                     AND bd.MONTH_IDENTIFIER BETWEEN @Start_Dt AND @End_Dt
            INNER JOIN Core.Client_Hier_Account cha
                  ON ba.ACCOUNT_ID = cha.Account_Id
                     AND cha.Commodity_Id = @Commodity_Id
            LEFT JOIN ( Core.Client_Hier_Account AS cha1
                        INNER JOIN dbo.CONTRACT AS c
                              ON cha1.Supplier_Contract_ID = c.CONTRACT_ID
                                 AND c.contract_end_date >= getdate()
                                 AND cha1.Account_Type = 'Supplier'
                        INNER JOIN dbo.Entity conType
                              ON conType.Entity_id = c.contract_type_id
                                 AND ConType.Entity_Name = 'Supplier' )
                        ON cha.Meter_Id = cha1.Meter_Id
      WHERE
            ba.budget_id = @Budget_Id
            AND ba.Is_Deleted = 0
      GROUP BY
            ba.BUDGET_ID
           ,ba.BUDGET_ACCOUNT_ID
           ,ba.ACCOUNT_ID
           ,bd.MONTH_IDENTIFIER
           ,ba.HAS_DETAILS_SAVED
           ,cha.Account_Service_level_Cd
           ,bd.variable
           ,bd.transmission
           ,bd.transportation
           ,bd.distribution
           ,bd.other_bundled
           ,bd.other_fixed_costs
           ,bd.sourcing_tax
           ,bd.rates_tax
           ,bd.is_manual_generation
           ,bd.is_manual_sourcing_tax    
    
      SELECT
            c.CONTRACT_ID
           ,c.CONTRACT_START_DATE
           ,c.CONTRACT_END_DATE
           ,c.COMMODITY_TYPE_ID
           ,ua.ACCOUNT_ID
      INTO
            #Temp_Contracts
      FROM
            @Budget_Accounts ba
            INNER JOIN dbo.ACCOUNT ua
                  ON ba.ACCOUNT_ID = ua.ACCOUNT_ID
            INNER JOIN dbo.METER m
                  ON ua.ACCOUNT_ID = m.ACCOUNT_ID
            INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                  ON m.METER_ID = samm.METER_ID
            INNER JOIN dbo.ACCOUNT sa
                  ON samm.ACCOUNT_ID = sa.ACCOUNT_ID
            INNER JOIN dbo.[CONTRACT] c
                  ON c.CONTRACT_ID = samm.Contract_ID
      WHERE
            ua.ACCOUNT_TYPE_ID = @accounttype
            AND samm.IS_HISTORY = 0
            AND c.CONTRACT_TYPE_ID = @Suppliertype
            AND c.COMMODITY_TYPE_ID = @commodity_id
      GROUP BY
            c.CONTRACT_ID
           ,c.CONTRACT_START_DATE
           ,c.CONTRACT_END_DATE
           ,c.COMMODITY_TYPE_ID
           ,ua.ACCOUNT_ID    
          
  
   
      DECLARE @Account_Contracts TABLE
            ( 
             Account_Id INT
            ,Contract_Id INT ) 
            

     
      INSERT      INTO @Account_Contracts
                  ( 
                   Account_Id
                  ,Contract_Id )
                  SELECT
                        tbd.account_id
                       ,cast(case WHEN tbd.tariff_transport = 'Transport' THEN tc.contract_id
                                  ELSE NULL
                             END AS VARCHAR) Contract_Ids
                  FROM
                        #Temp_Budget_Details tbd
                        INNER JOIN dbo.#Temp_Contracts tc
                              ON tbd.ACCOUNT_ID = tc.ACCOUNT_ID
                  WHERE
                        ( tbd.month_identifier BETWEEN dateadd(dd, -( day(tc.contract_start_date) - 1 ), tc.contract_start_date)
                                               AND     tc.contract_end_date
                          OR ( ( tbd.month_identifier NOT BETWEEN dateadd(dd, -( day(tc.contract_start_date) - 1 ), tc.contract_start_date)
                                                      AND         tc.contract_end_date )
                               AND ( tc.contract_start_date < @start_dt )
                               AND ( tc.contract_end_date = ( SELECT
                                                                  max(tc.contract_end_date)
                                                              FROM
                                                                  #Temp_Contracts tc
                                                              WHERE
                                                                  tc.Account_Id = tbd.account_id
                                                                  AND tc.commodity_type_id = tbd.commodity_type_id ) ) ) )
                  GROUP BY
                        tbd.account_id
                       ,tc.contract_id
                       ,tbd.tariff_transport
    
    
      SELECT
            ba.ACCOUNT_ID
           ,x.contract_id ContractIDs
      FROM
            @Budget_Accounts ba
            LEFT JOIN ( SELECT
                              account_id
                             ,stuff(D.contract_id, 1, 2, '') contract_id
                        FROM
                              @Account_Contracts ac1
                              CROSS APPLY ( SELECT
                                                ', ' + cast(ac2.contract_id AS VARCHAR(20))
                                            FROM
                                                @Account_Contracts ac2
                                            WHERE
                                                ac1.account_id = ac2.account_id
                              FOR
                                            XML PATH('') ) D ( contract_id ) ) x
                  ON x.account_id = ba.account_id
      GROUP BY
            ba.ACCOUNT_ID
           ,x.contract_id
      ORDER BY
            ba.Account_Id

                      
       
      DECLARE @Forecast_Contracts_Data TABLE
            ( 
             Forecast_Contract_Id INT NOT NULL
                                      IDENTITY(1, 1)
            ,Account_Id INT
            ,Contract_Id INT
            ,Contract_Start_Dt DATE
            ,Contract_End_dt DATE
            ,Contract_Adder DECIMAL(32, 16)
            ,Contract_Multiplier DECIMAL(32, 16)
            ,Contract_Tax DECIMAL(32, 16)
            ,Contract_Market_Id INT
            ,Contract_Fuel DECIMAL(32, 16)
            ,Default_Adder DECIMAL(32, 16)
            ,Default_Multiplier DECIMAL(32, 16)
            ,Default_Market_Id INT
            ,Default_Tax DECIMAL(32, 16)
            ,Defualt_Fuel DECIMAL(32, 16) )


      INSERT      INTO @Forecast_Contracts_Data
                  ( 
                   Account_Id
                  ,Contract_Id
                  ,Contract_Start_Dt
                  ,Contract_End_dt
                  ,Contract_Adder
                  ,Contract_Multiplier
                  ,Contract_Tax
                  ,Contract_Market_Id
                  ,Contract_Fuel
                  ,Default_Adder
                  ,Default_Multiplier
                  ,Default_Market_Id
                  ,Default_Tax
                  ,Defualt_Fuel )
                  SELECT
                        tbd.Account_Id
                       ,tc.Contract_Id
                       ,tc.CONTRACT_START_DATE
                       ,tc.CONTRACT_END_DATE
                       ,bcbd.ADDER AS Contract_Adder
                       ,bcbd.MULTIPLIER AS Contract_Multiplier
                       ,bcbd.TAX AS Contract_Tax
                       ,bcbd.MARKET_ID AS Contract_Market_Id
                       ,bcbd.FUEL AS Contract_Fuel
                       ,bcd.ADDER AS Default_Adder
                       ,bcd.MULTIPLIER AS Default_Multiplier
                       ,bcd.MARKET_ID AS Default_Market_Id
                       ,bcd.TAX AS Default_Tax
                       ,bcd.FUEL AS Defualt_Fuel
                  FROM
                        #Temp_Budget_Details tbd
                        LEFT JOIN #Temp_Contracts tc
                              ON tbd.ACCOUNT_ID = tc.Account_Id
                                 AND tbd.month_identifier BETWEEN dateadd(dd, -( day(tc.contract_start_date) - 1 ), tc.contract_start_date)
                                                          AND     tc.contract_end_date
                        LEFT JOIN BUDGET_CONTRACT_BUDGET bcb
                              ON bcb.CONTRACT_ID = tc.contract_id
                        LEFT JOIN BUDGET_CONTRACT_BUDGET_MONTHS bcbm
                              ON bcbm.BUDGET_CONTRACT_BUDGET_ID = bcb.BUDGET_CONTRACT_BUDGET_ID
                                 AND bcbm.MONTH_IDENTIFIER = tbd.MONTH_IDENTIFIER
                                 AND bcbm.MONTH_IDENTIFIER BETWEEN @Start_Dt
                                                           AND     @End_Dt
                        LEFT JOIN dbo.BUDGET_CONTRACT_BUDGET_DETAIL bcbd
                              ON bcbd.BUDGET_CONTRACT_BUDGET_MONTH_ID = bcbm.BUDGET_CONTRACT_BUDGET_MONTH_ID
                        LEFT JOIN dbo.BUDGET_CONTRACT_DEFAULTS bcd
                              ON bcb.BUDGET_CONTRACT_BUDGET_ID = bcd.BUDGET_CONTRACT_BUDGET_ID
                  WHERE
                        tc.Contract_Id IS NOT NULL
                  GROUP BY
                        tbd.Account_Id
                       ,tc.Contract_Id
                       ,tc.CONTRACT_START_DATE
                       ,tc.CONTRACT_END_DATE
                       ,bcbd.ADDER
                       ,bcbd.MULTIPLIER
                       ,bcbd.TAX
                       ,bcbd.MARKET_ID
                       ,bcbd.FUEL
                       ,bcd.ADDER
                       ,bcd.MULTIPLIER
                       ,bcd.MARKET_ID
                       ,bcd.TAX
                       ,bcd.FUEL
                  ORDER BY
                        tbd.Account_Id
                       ,CONTRACT_START_DATE
 
 
      DECLARE @Forecast_Contracts TABLE
            ( 
             Account_Id INT
            ,Contract_Id INT
            ,Null_Contract_Id INT
            ,Contract_St_Dt DATE
            ,Contract_End_Dt DATE )
      
      
      INSERT      INTO @Forecast_Contracts
                  ( 
                   Account_Id
                  ,Contract_Id
                  ,Null_Contract_Id
                  ,Contract_St_Dt
                  ,Contract_End_Dt )
                  SELECT
                        fc.Account_Id
                       ,fc.Contract_Id
                       ,fc1.Contract_Id AS Null_Contract_Id
                       ,fc.Contract_Start_Dt
                       ,fc.Contract_End_dt
                  FROM
                        @Forecast_Contracts_Data fc
                        INNER JOIN ( SELECT
                                          Account_Id
                                         ,Forecast_Contract_Id
                                         ,Contract_Id
                                     FROM
                                          @Forecast_Contracts_Data
                                     WHERE
                                          Contract_Adder IS NULL
                                          AND Contract_Multiplier IS NULL
                                          AND Contract_Tax IS NULL
                                          AND Contract_Market_Id IS NULL
                                          AND Contract_Fuel IS NULL ) fc1
                              ON fc.Account_Id = fc1.Account_Id
                  WHERE
                        fc.Forecast_Contract_Id = ( SELECT
                                                      max(Forecast_Contract_Id)
                                                    FROM
                                                      @Forecast_Contracts_Data fc2
                                                    WHERE
                                                      fc2.Forecast_Contract_Id < fc1.Forecast_Contract_Id )
                        AND ( fc.Contract_Adder IS NOT NULL
                              OR fc.Contract_Market_Id IS NOT NULL
                              OR fc.Contract_Multiplier IS NOT NULL
                              OR fc.Contract_Tax IS NOT NULL )                     
                   
       
      DECLARE @Current_Budget_Contracts TABLE
            ( 
             Account_Id INT
            ,Contract_Id INT )
 
      INSERT      INTO @Current_Budget_Contracts
                  ( 
                   Account_Id
                  ,Contract_Id )
                  SELECT
                        Account_Id
                       ,Contract_Id
                  FROM
                        @Forecast_Contracts_Data fcd
                  WHERE
                        ( fcd.Contract_Adder IS NOT NULL
                          OR fcd.Contract_Market_Id IS NOT NULL
                          OR fcd.Contract_Multiplier IS NOT NULL
                          OR fcd.Contract_Tax IS NOT NULL )  
       
                   
      CREATE TABLE #Budget_Details
            ( 
             Budget_Id INT
            ,Budget_Account_Id INT NOT NULL
            ,Account_Id INT NOT NULL
            ,Month_Identifier DATE NOT NULL
            ,variable VARCHAR(300)
            ,TRANSPORTATION VARCHAR(300)
            ,TRANSMISSION VARCHAR(300)
            ,DISTRIBUTION VARCHAR(300)
            ,OTHER_BUNDLED VARCHAR(300)
            ,OTHER_FIXED_COSTS VARCHAR(300)
            ,SOURCING_TAX VARCHAR(300)
            ,RATES_TAX VARCHAR(300)
            ,IS_MANUAL_GENERATION BIT
            ,IS_MANUAL_SOURCING_TAX BIT
            ,Volume DECIMAL(32, 16)
            ,Nymex_Price DECIMAL(32, 16) )                          
                                    
      DECLARE @Temp_Budgets1 TABLE
            ( 
             BUDGET_ID INT
            ,BUDGET_ACCOUNT_ID INT
            ,ACCOUNT_ID INT
            ,MONTH_IDENTIFIER DATETIME
            ,VOLUME DECIMAL(32, 16)
            ,Is_Nymex_Forecast BIT
            ,INDEX_DETAIL_VALUE DECIMAL(32, 16)
            ,Volume_Pct DECIMAL(32, 16)
            ,MULTIPLIER DECIMAL(32, 16)
            ,ADDER DECIMAL(32, 16)
            ,Nymex_Value DECIMAL(32, 16)
            ,SERVICE_LEVEL_TYPE_ID INT
            ,BUDGET_CONTRACT_BUDGET_ID INT
            ,Market_Id INT
            ,CONTRACT_START_DATE DATETIME
            ,CONTRACT_END_DATE DATETIME
            ,commodity_type_id INT
            ,contract_id INT
            ,variable_actual VARCHAR(300)
            ,variable VARCHAR(300)
            ,Fuel DECIMAL(32, 16)
            ,tax DECIMAL(32, 16)
            ,transmission VARCHAR(300)
            ,transportation VARCHAR(300)
            ,Tariff_Transport VARCHAR(300)
            ,distribution VARCHAR(300)
            ,other_bundled VARCHAR(300)
            ,other_fixed_costs VARCHAR(300)
            ,sourcing_tax VARCHAR(300)
            ,rates_tax VARCHAR(300)
            ,is_manual_generation BIT
            ,is_manual_sourcing_tax BIT )  
      
      
      
      CREATE TABLE #Temp_Budgets
            ( 
             BUDGET_ID INT
            ,BUDGET_ACCOUNT_ID INT
            ,ACCOUNT_ID INT
            ,MONTH_IDENTIFIER DATETIME
            ,VOLUME DECIMAL(32, 16)
            ,Is_Nymex_Forecast BIT
            ,INDEX_DETAIL_VALUE DECIMAL(32, 16)
            ,Volume_Pct DECIMAL(32, 16)
            ,MULTIPLIER DECIMAL(32, 16)
            ,ADDER DECIMAL(32, 16)
            ,Nymex_Value DECIMAL(32, 16)
            ,SERVICE_LEVEL_TYPE_ID INT
            ,Market_Id INT
            ,CONTRACT_START_DATE DATETIME
            ,CONTRACT_END_DATE DATETIME
            ,commodity_type_id INT
            ,contract_id INT
            ,variable_actual VARCHAR(300)
            ,variable VARCHAR(300)
            ,Fuel DECIMAL(32, 16)
            ,tax DECIMAL(32, 16)
            ,transmission VARCHAR(300)
            ,transportation VARCHAR(300)
            ,Tariff_Transport VARCHAR(300)
            ,distribution VARCHAR(300)
            ,other_bundled VARCHAR(300)
            ,other_fixed_costs VARCHAR(300)
            ,sourcing_tax VARCHAR(300)
            ,rates_tax VARCHAR(300)
            ,is_manual_generation BIT
            ,is_manual_sourcing_tax BIT )              
                   
      
           
      INSERT      INTO @Temp_Budgets1
                  ( 
                   BUDGET_ID
                  ,BUDGET_ACCOUNT_ID
                  ,ACCOUNT_ID
                  ,MONTH_IDENTIFIER
                  ,VOLUME
                  ,Is_Nymex_Forecast
                  ,INDEX_DETAIL_VALUE
                  ,Volume_Pct
                  ,MULTIPLIER
                  ,ADDER
                  ,Nymex_Value
                  ,SERVICE_LEVEL_TYPE_ID
                  ,BUDGET_CONTRACT_BUDGET_ID
                  ,Market_Id
                  ,CONTRACT_START_DATE
                  ,CONTRACT_END_DATE
                  ,commodity_type_id
                  ,contract_id
                  ,variable_actual
                  ,variable
                  ,Fuel
                  ,tax
                  ,transmission
                  ,transportation
                  ,Tariff_Transport
                  ,distribution
                  ,other_bundled
                  ,other_fixed_costs
                  ,sourcing_tax
                  ,rates_tax
                  ,is_manual_generation
                  ,is_manual_sourcing_tax )
                  SELECT
                        tbd.BUDGET_ID
                       ,tbd.BUDGET_ACCOUNT_ID
                       ,tbd.ACCOUNT_ID
                       ,tbd.MONTH_IDENTIFIER
                       ,1 AS VOLUME
                       ,bcbd.Is_Nymex_Forecast
                       ,cid.INDEX_DETAIL_VALUE
                       ,bcbd.VOLUME AS Volume_Pct
                       ,bcbd.MULTIPLIER
                       ,( bcbd.ADDER * curc.CONVERSION_FACTOR ) / nullif(conc.CONVERSION_FACTOR, 0) AS ADDER
                       ,1 AS Nymex_Value
                       ,tbd.SERVICE_LEVEL_TYPE_ID
                       ,bcb.BUDGET_CONTRACT_BUDGET_ID
                       ,bcbd.Market_Id
                       ,case WHEN tbd.tariff_transport = 'Transport' THEN tc.CONTRACT_START_DATE
                             ELSE NULL
                        END
                       ,case WHEN tbd.tariff_transport = 'Transport' THEN tc.CONTRACT_END_DATE
                             ELSE NULL
                        END
                       ,tbd.commodity_type_id
                       ,case WHEN tbd.tariff_transport = 'Transport' THEN tc.contract_id
                             ELSE NULL
                        END
                       ,tbd.variable AS variable_actual
                       ,tbd.variable
                       ,bcbd.Fuel
                       ,bcbd.tax
                       ,tbd.transmission
                       ,tbd.transportation
                       ,tbd.Tariff_Transport
                       ,tbd.distribution
                       ,tbd.other_bundled
                       ,tbd.other_fixed_costs
                       ,tbd.sourcing_tax
                       ,tbd.rates_tax
                       ,tbd.is_manual_generation
                       ,tbd.is_manual_sourcing_tax
                  FROM
                        #Temp_Budget_Details tbd
                        LEFT JOIN #Temp_Contracts tc
                              ON tbd.ACCOUNT_ID = tc.Account_Id
                                 AND tbd.month_identifier BETWEEN dateadd(dd, -( day(tc.contract_start_date) - 1 ), tc.contract_start_date)
                                                          AND     tc.contract_end_date
                        LEFT JOIN BUDGET_CONTRACT_BUDGET bcb
                              ON bcb.CONTRACT_ID = tc.contract_id
                        LEFT JOIN BUDGET_CONTRACT_BUDGET_MONTHS bcbm
                              ON bcbm.BUDGET_CONTRACT_BUDGET_ID = bcb.BUDGET_CONTRACT_BUDGET_ID
                                 AND bcbm.MONTH_IDENTIFIER = tbd.MONTH_IDENTIFIER
                                 AND bcbm.MONTH_IDENTIFIER BETWEEN @Start_Dt
                                                           AND     @End_Dt
                        LEFT JOIN dbo.BUDGET_CONTRACT_BUDGET_DETAIL bcbd
                              ON bcbd.BUDGET_CONTRACT_BUDGET_MONTH_ID = bcbm.BUDGET_CONTRACT_BUDGET_MONTH_ID
                        LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION conc
                              ON conc.BASE_UNIT_ID = bcbd.VOLUME_UNIT_TYPE_ID
                                 AND conc.CONVERTED_UNIT_ID = case WHEN tbd.commodity_type_id = @EP_Commodity_Id THEN @EP_UOM_Id
                                                                   ELSE @NG_UOM_Id
                                                              END
                        LEFT JOIN dbo.CURRENCY_UNIT_CONVERSION curc
                              ON curc.BASE_UNIT_ID = bcbd.CURRENCY_UNIT_ID
                                 AND curc.CONVERTED_UNIT_ID = @Currency_Id
                                 AND curc.CURRENCY_GROUP_ID = @Currency_Group_Id
                                 AND curc.CONVERSION_DATE = tbd.Month_Identifier
                        LEFT JOIN dbo.CLEARPORT_INDEX_MONTHS cim
                              ON cim.clearport_index_id = bcbd.market_id
                                 AND cim.clearport_index_month = bcbm.MONTH_IDENTIFIER
                        LEFT JOIN dbo.CLEARPORT_INDEX_DETAIL cid
                              ON cid.clearport_index_month_id = cim.clearport_index_month_id
                                 AND cid.index_detail_date = ( SELECT
                                                                  max(index_detail_date)
                                                               FROM
                                                                  dbo.CLEARPORT_INDEX_DETAIL
                                                               WHERE
                                                                  clearport_index_month_id = cid.clearport_index_month_id );
      SELECT
            tbd.BUDGET_ID
           ,tbd.BUDGET_ACCOUNT_ID
           ,tbd.ACCOUNT_ID
           ,tbd.MONTH_IDENTIFIER
           ,1 AS VOLUME
           ,( bcd.Is_Nymex_Forecast )
           ,avg(cid.INDEX_DETAIL_VALUE) OVER ( PARTITION BY tbd.Account_Id, cim.CLEARPORT_INDEX_ID ) INDEX_DETAIL_VALUE
           ,bcd.VOLUME AS Volume_Pct
           ,bcd.MULTIPLIER
           ,( bcd.ADDER * curc.CONVERSION_FACTOR ) / nullif(conc.CONVERSION_FACTOR, 0) AS ADDER
           ,1 AS Nymex_Value
           ,tbd.SERVICE_LEVEL_TYPE_ID
           ,bcb.BUDGET_CONTRACT_BUDGET_ID
           ,bcd.Market_Id
           ,case WHEN tbd.tariff_transport = 'Transport' THEN tc.CONTRACT_START_DATE
                 ELSE NULL
            END CONTRACT_START_DATE
           ,case WHEN tbd.tariff_transport = 'Transport' THEN tc.CONTRACT_END_DATE
                 ELSE NULL
            END CONTRACT_END_DATE
           ,tbd.commodity_type_id
           ,case WHEN tbd.tariff_transport = 'Transport' THEN tc.contract_id
                 ELSE NULL
            END contract_id
           ,tbd.variable AS variable_actual
           ,tbd.variable
           ,bcd.Fuel
           ,bcd.tax
           ,tbd.transmission
           ,tbd.transportation
           ,tbd.Tariff_Transport
           ,tbd.distribution
           ,tbd.other_bundled
           ,tbd.other_fixed_costs
           ,tbd.sourcing_tax
           ,tbd.rates_tax
           ,tbd.is_manual_generation
           ,tbd.is_manual_sourcing_tax
      INTO
            #Future_Budget_Data
      FROM
            #Temp_Budget_Details tbd
            LEFT JOIN #Temp_Contracts tc
                  ON tbd.ACCOUNT_ID = tc.Account_Id
                     AND ( ( tbd.month_identifier NOT BETWEEN dateadd(dd, -( day(tc.contract_start_date) - 1 ), tc.contract_start_date)
                                                  AND         tc.contract_end_date )
                           AND ( tc.contract_end_date = ( SELECT
                                                            max(tc.contract_end_date)
                                                          FROM
                                                            #Temp_Contracts tc
                                                          WHERE
                                                            tc.Account_Id = tbd.account_id
                                                            AND tc.commodity_type_id = tbd.commodity_type_id ) )
                           OR tc.Contract_Id IN ( SELECT
                                                      fc.Contract_id
                                                  FROM
                                                      @Forecast_Contracts fc
                                                  WHERE
                                                      tc.Account_Id = fc.Account_Id ) )
            LEFT JOIN dbo.BUDGET_CONTRACT_BUDGET bcb
                  ON bcb.CONTRACT_ID = tc.contract_id
            LEFT JOIN dbo.BUDGET_CONTRACT_DEFAULTS bcd
                  ON bcd.BUDGET_CONTRACT_BUDGET_ID = bcb.BUDGET_CONTRACT_BUDGET_ID
            LEFT JOIN dbo.CLEARPORT_INDEX_MONTHS cim
                  ON cim.CLEARPORT_INDEX_ID = bcd.MARKET_ID
                     AND cim.CLEARPORT_INDEX_MONTH BETWEEN dateadd(mm, 1, dateadd(dd, -( day(tc.contract_end_date) - 1 ), tc.contract_end_date))
                                                   AND     dateadd(mm, 12, tc.contract_end_date)
            LEFT JOIN dbo.CLEARPORT_INDEX_DETAIL cid
                  ON cid.clearport_index_month_id = cim.clearport_index_month_id
                     AND cid.index_detail_date = ( SELECT
                                                      max(index_detail_date)
                                                   FROM
                                                      dbo.CLEARPORT_INDEX_DETAIL
                                                   WHERE
                                                      clearport_index_month_id = cid.clearport_index_month_id )
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION conc
                  ON conc.BASE_UNIT_ID = bcd.VOLUME_UNIT_TYPE_ID
                     AND conc.CONVERTED_UNIT_ID = case WHEN tbd.commodity_type_id = @EP_Commodity_Id THEN @EP_UOM_Id
                                                       ELSE @NG_UOM_Id
                                                  END
            LEFT JOIN dbo.CURRENCY_UNIT_CONVERSION curc
                  ON curc.BASE_UNIT_ID = bcd.CURRENCY_UNIT_ID
                     AND curc.CONVERTED_UNIT_ID = @Currency_Id
                     AND curc.CURRENCY_GROUP_ID = @Currency_Group_Id
                     AND curc.CONVERSION_DATE = tbd.Month_Identifier
                     
                     
      SELECT
            BUDGET_ID
           ,BUDGET_ACCOUNT_ID
           ,ACCOUNT_ID
           ,MONTH_IDENTIFIER
           ,VOLUME
           ,Is_Nymex_Forecast
           ,INDEX_DETAIL_VALUE
           ,Volume_Pct
           ,MULTIPLIER
           ,ADDER
           ,Nymex_Value
           ,SERVICE_LEVEL_TYPE_ID
           ,BUDGET_CONTRACT_BUDGET_ID
           ,Market_Id
           ,CONTRACT_START_DATE
           ,CONTRACT_END_DATE
           ,commodity_type_id
           ,contract_id
           ,variable_actual
           ,variable
           ,Fuel
           ,tax
           ,transmission
           ,transportation
           ,Tariff_Transport
           ,distribution
           ,other_bundled
           ,other_fixed_costs
           ,sourcing_tax
           ,rates_tax
           ,is_manual_generation
           ,is_manual_sourcing_tax
           ,count(1) OVER ( PARTITION BY account_id, month_identifier, contract_start_date ) AS Count_Contract
      INTO
            #Future_Budget_Grouped
      FROM
            #Future_Budget_Data
      GROUP BY
            BUDGET_ID
           ,BUDGET_ACCOUNT_ID
           ,ACCOUNT_ID
           ,MONTH_IDENTIFIER
           ,VOLUME
           ,Is_Nymex_Forecast
           ,INDEX_DETAIL_VALUE
           ,Volume_Pct
           ,MULTIPLIER
           ,ADDER
           ,Nymex_Value
           ,SERVICE_LEVEL_TYPE_ID
           ,BUDGET_CONTRACT_BUDGET_ID
           ,Market_Id
           ,CONTRACT_START_DATE
           ,CONTRACT_END_DATE
           ,commodity_type_id
           ,contract_id
           ,variable_actual
           ,variable
           ,Fuel
           ,tax
           ,transmission
           ,transportation
           ,Tariff_Transport
           ,distribution
           ,other_bundled
           ,other_fixed_costs
           ,sourcing_tax
           ,rates_tax
           ,is_manual_generation
           ,is_manual_sourcing_tax
           
           
      SELECT
            tb.BUDGET_ID
           ,tb.BUDGET_ACCOUNT_ID
           ,tb.ACCOUNT_ID
           ,tb.MONTH_IDENTIFIER
           ,1 AS Volume
           ,case WHEN tb.contract_id = cbc.Contract_Id THEN tb.Is_Nymex_Forecast
                 ELSE fbg.Is_Nymex_Forecast
            END Is_Nymex_Forecast
           ,case WHEN tb.contract_id = cbc.Contract_Id THEN tb.INDEX_DETAIL_VALUE
                 ELSE fbg.INDEX_DETAIL_VALUE
            END INDEX_DETAIL_VALUE
           ,case WHEN tb.contract_id = cbc.Contract_Id THEN tb.Volume_Pct
                 ELSE fbg.Volume_Pct
            END Volume_Pct
           ,case WHEN tb.contract_id = cbc.Contract_Id THEN tb.MULTIPLIER
                 ELSE fbg.MULTIPLIER
            END MULTIPLIER
           ,case WHEN tb.contract_id = cbc.Contract_Id THEN tb.Contract_Id
                 ELSE fbg.Contract_Id
            END Contract_Id
           ,case WHEN tb.contract_id = cbc.Contract_Id THEN tb.CONTRACT_START_DATE
                 ELSE fbg.CONTRACT_START_DATE
            END CONTRACT_START_DATE
           ,case WHEN tb.contract_id = cbc.Contract_Id THEN tb.CONTRACT_END_DATE
                 ELSE fbg.CONTRACT_END_DATE
            END CONTRACT_END_DATE
           ,case WHEN tb.contract_id = cbc.Contract_Id THEN tb.ADDER
                 ELSE fbg.ADDER
            END ADDER
           ,1 AS Nymex_Value
           ,tb.SERVICE_LEVEL_TYPE_ID
           ,case WHEN tb.contract_id = cbc.Contract_Id THEN tb.Market_Id
                 ELSE fbg.Market_Id
            END Market_Id
           ,tb.commodity_type_id
           ,tb.variable_actual
           ,tb.variable
           ,case WHEN tb.contract_id = cbc.Contract_Id THEN tb.Fuel
                 ELSE fbg.Fuel
            END Fuel
           ,case WHEN tb.contract_id = cbc.Contract_Id THEN tb.tax
                 ELSE fbg.tax
            END tax
           ,tb.transmission
           ,tb.transportation
           ,tb.Tariff_Transport
           ,tb.distribution
           ,tb.other_bundled
           ,tb.other_fixed_costs
           ,tb.sourcing_tax
           ,tb.rates_tax
           ,tb.is_manual_generation
           ,tb.is_manual_sourcing_tax
           ,row_number() OVER ( PARTITION BY tb.account_id, tb.month_identifier, case WHEN tb.contract_id = cbc.Contract_Id THEN tb.Contract_Id
                                                                                      ELSE fbg.Contract_Id
                                                                                 END ORDER BY case WHEN tb.contract_id = cbc.Contract_Id THEN 1
                                                                                                   ELSE 2
                                                                                              END ) AS Row_Num
           ,case WHEN tb.contract_id = cbc.Contract_Id THEN x.Contract_Count
                 ELSE fbg.Count_Contract
            END Contract_Count
      INTO
            #Future_Budget_Contracts
      FROM
            @temp_budgets1 tb
            LEFT JOIN #Future_Budget_Grouped fbg
                  ON tb.ACCOUNT_ID = fbg.ACCOUNT_ID
                     AND tb.Month_Identifier = fbg.Month_Identifier
            LEFT JOIN @Current_Budget_Contracts cbc
                  ON cbc.account_id = tb.account_id
                     AND cbc.contract_id = tb.contract_id
            LEFT JOIN ( SELECT
                              account_id
                             ,contract_id
                             ,count(1) AS Contract_Count
                        FROM
                              @Current_Budget_Contracts
                        GROUP BY
                              account_id
                             ,contract_id ) x
                  ON x.account_id = tb.account_id
                     AND x.contract_id = TB.contract_id
                     
      INSERT      INTO #Temp_Budgets
                  ( 
                   BUDGET_ID
                  ,BUDGET_ACCOUNT_ID
                  ,ACCOUNT_ID
                  ,MONTH_IDENTIFIER
                  ,VOLUME
                  ,Is_Nymex_Forecast
                  ,INDEX_DETAIL_VALUE
                  ,Volume_Pct
                  ,MULTIPLIER
                  ,ADDER
                  ,Nymex_Value
                  ,SERVICE_LEVEL_TYPE_ID
                  ,Market_Id
                  ,CONTRACT_START_DATE
                  ,CONTRACT_END_DATE
                  ,commodity_type_id
                  ,contract_id
                  ,variable_actual
                  ,variable
                  ,Fuel
                  ,tax
                  ,transmission
                  ,transportation
                  ,Tariff_Transport
                  ,distribution
                  ,other_bundled
                  ,other_fixed_costs
                  ,sourcing_tax
                  ,rates_tax
                  ,is_manual_generation
                  ,is_manual_sourcing_tax )
                  SELECT
                        BUDGET_ID
                       ,BUDGET_ACCOUNT_ID
                       ,ACCOUNT_ID
                       ,MONTH_IDENTIFIER
                       ,VOLUME
                       ,Is_Nymex_Forecast
                       ,INDEX_DETAIL_VALUE
                       ,Volume_Pct
                       ,MULTIPLIER
                       ,ADDER
                       ,Nymex_Value
                       ,SERVICE_LEVEL_TYPE_ID
                       ,Market_Id
                       ,CONTRACT_START_DATE
                       ,CONTRACT_END_DATE
                       ,commodity_type_id
                       ,contract_id
                       ,variable_actual
                       ,variable
                       ,Fuel
                       ,tax
                       ,transmission
                       ,transportation
                       ,Tariff_Transport
                       ,distribution
                       ,other_bundled
                       ,other_fixed_costs
                       ,sourcing_tax
                       ,rates_tax
                       ,is_manual_generation
                       ,is_manual_sourcing_tax
                  FROM
                        #Future_Budget_Contracts
                  WHERE
                        Row_Num <= Contract_Count
                  GROUP BY
                        BUDGET_ID
                       ,BUDGET_ACCOUNT_ID
                       ,ACCOUNT_ID
                       ,MONTH_IDENTIFIER
                       ,VOLUME
                       ,Is_Nymex_Forecast
                       ,INDEX_DETAIL_VALUE
                       ,Volume_Pct
                       ,MULTIPLIER
                       ,ADDER
                       ,Nymex_Value
                       ,SERVICE_LEVEL_TYPE_ID
                       ,Market_Id
                       ,CONTRACT_START_DATE
                       ,CONTRACT_END_DATE
                       ,commodity_type_id
                       ,contract_id
                       ,variable_actual
                       ,variable
                       ,Fuel
                       ,tax
                       ,transmission
                       ,transportation
                       ,Tariff_Transport
                       ,distribution
                       ,other_bundled
                       ,other_fixed_costs
                       ,sourcing_tax
                       ,rates_tax
                       ,is_manual_generation
                       ,is_manual_sourcing_tax
                       ,Contract_Count
                  ORDER BY
                        Month_Identifier

         
   --Calculate variable,Transportation.          
              
      UPDATE
            tb
      SET   
            VARIABLE = case WHEN ( tb.is_manual_generation = 1
                                   OR @Is_Posted_to_DV = 1 ) THEN variable
                            ELSE case WHEN tb.commodity_type_id = @EP_Commodity_Id THEN case WHEN Tariff_Transport = 'Tariff' THEN variable
                                                                                             ELSE case WHEN isnull(IS_NYMEX_FORECAST, 0) = 0 THEN case WHEN ( INDEX_DETAIL_VALUE IS NULL
                                                                                                                                                              AND adder IS NULL ) THEN NULL
                                                                                                                                                       ELSE convert(VARCHAR(100), round(( isnull(Volume_Pct, 100) / 100 ) * ( ( isnull(INDEX_DETAIL_VALUE, 0) / 1000 ) * multiplier + ( (isnull(adder, 0)) ) ), 3))
                                                                                                                                                  END
                                                                                                       ELSE convert(VARCHAR(100), round(( isnull(Volume_Pct, 100) / 100 ) * ( ( coalesce(bnf.Price, rf.MODERATE_PRICE, 0) / 1000 ) * multiplier + ( (isnull(adder, 0)) ) ), 3))
                                                                                                  END
                                                                                        END
                                      ELSE case WHEN Tariff_Transport = 'Tariff' THEN case WHEN service_level_type_id IN ( @service_level_type_id, @service_level_type ) THEN variable
                                                                                           ELSE isnull(variable, 'A1')
                                                                                      END
                                                ELSE isnull(variable, 'A1')
                                           END
                                 END
                       END
           ,TRANSPORTATION = case WHEN ( tb.is_manual_generation = 1
                                         OR @Is_Posted_to_DV = 1 ) THEN TRANSPORTATION
                                  ELSE case WHEN tb.commodity_type_id = @EP_Commodity_Id THEN transportation
                                            ELSE case WHEN Tariff_Transport = 'Tariff' THEN transportation
                                                      ELSE case WHEN ( INDEX_DETAIL_VALUE IS NULL
                                                                       AND adder IS NULL ) THEN NULL
                                                                ELSE convert(VARCHAR(100), round(( isnull(Volume_Pct, 100) / 100 ) * ( ( ( coalesce(bnf.Price, rf.MODERATE_PRICE, 0) + ( isnull(INDEX_DETAIL_VALUE, 0) ) ) * isnull(( Fuel / ( 100 - Fuel ) ), 0) ) + ( isnull(INDEX_DETAIL_VALUE, 0) + ( isnull(adder, 0) ) ) ), 3))
                                                           END
                                                 END
                                       END
                             END
           ,NYMEX_VALUE = isnull(bnf.Price, rf.MODERATE_PRICE)
      FROM
            #Temp_Budgets tb
            LEFT JOIN dbo.BUDGET_NYMEX_FORECAST bnf
                  ON bnf.ACCOUNT_ID = tb.Account_Id
                     AND tb.Month_Identifier = bnf.MONTH_IDENTIFIER
            LEFT JOIN ( SELECT
                              rfd.MONTH_IDENTIFIER
                             ,rfd.MODERATE_PRICE
                        FROM
                              dbo.RM_FORECAST_DETAILS AS rfd
                              INNER JOIN dbo.RM_FORECAST AS rf
                                    ON rf.RM_FORECAST_ID = rfd.RM_FORECAST_ID
                                       AND rf.FORECAST_AS_OF_DATE = ( SELECT
                                                                        max(FORECAST_FROM_DATE)
                                                                      FROM
                                                                        dbo.RM_FORECAST AS rf
                                                                        INNER JOIN dbo.ENTITY AS en
                                                                              ON rf.FORECAST_TYPE_ID = en.ENTITY_ID
                                                                      WHERE
                                                                        en.ENTITY_NAME = 'Nymex' ) ) rf
                  ON rf.MONTH_IDENTIFIER = tb.Month_Identifier             
          
          
--- Get Variable as Null if Unit cost Exists in two Contracts for the same Month          
          
   
      UPDATE
            tbm
      SET   
            variable = case WHEN x.Commodity_Id = @EP_Commodity_Id THEN case WHEN isnull(tbm.is_manual_generation, 0) = 0 THEN x.variable
                                                                             ELSE tbm.variable
                                                                        END
                            ELSE tbm.variable
                       END
           ,transportation = case WHEN x.Commodity_Id = @EP_Commodity_Id THEN tbm.transportation
                                  ELSE case WHEN isnull(tbm.is_manual_generation, 0) = 0 THEN x.variable
                                            ELSE tbm.transportation
                                       END
                             END
      FROM
            #Temp_Budgets tbm
            INNER JOIN ( SELECT
                              case WHEN max(tb1.duplicate_month_ct) = 2 THEN NULL
                                   ELSE convert(VARCHAR(100), max(variable))
                              END variable
                             ,case WHEN max(tb1.duplicate_month_ct) = 2 THEN NULL
                                   ELSE convert(VARCHAR(100), max(transportation))
                              END transportation
                             ,commodity_type_id AS Commodity_Id
                             ,tb.budget_account_id
                             ,tb.month_identifier
                         FROM
                              #Temp_Budgets tb
                              INNER JOIN ( SELECT
                                                budget_account_id
                                               ,month_identifier
                                               ,count(1) AS duplicate_month_ct
                                           FROM
                                                #Temp_Budgets
                                           WHERE
                                                ( variable IS NOT NULL
                                                  OR transportation IS NOT NULL )
                                           GROUP BY
                                                budget_account_id
                                               ,market_id
                                               ,month_identifier
                                           HAVING
                                                count(1) = 2 ) tb1
                                    ON tb.budget_account_id = tb1.budget_account_id
                                       AND tb.month_identifier = tb1.month_identifier
                         GROUP BY
                              commodity_type_id
                             ,tb.budget_account_id
                             ,tb.month_identifier ) x
                  ON tbm.Budget_account_id = x.budget_account_id
                     AND tbm.month_identifier = x.month_identifier               
              
              
    --- Calculate Sourcing Tax based on variable/Transportation.          
                      
      UPDATE
            tb
      SET   
            sourcing_tax = case WHEN ( is_manual_sourcing_tax = 1
                                       OR @Is_Posted_to_DV = 1 ) THEN sourcing_tax
                                ELSE case WHEN isnumeric(isnull(sourcing_tax, 0)) = 0 THEN sourcing_tax
                                          ELSE case WHEN commodity_type_id = @EP_Commodity_Id THEN case WHEN isnumeric(variable) = 0 THEN sourcing_tax
                                                                                                        ELSE convert(VARCHAR(100), round(( round(variable, 3) * isnull(tax, 0) ) / 100, 3))
                                                                                                   END
                                                    ELSE case WHEN isnumeric(transportation) = 0 THEN sourcing_tax
                                                              ELSE convert(VARCHAR(100), ( round(isnull(( transportation * tax / 100 ), 0) + isnull(( NYMEX_VALUE * tax / 100 ), 0), 3) ))
                                                         END
                                               END
                                     END
                           END
      FROM
            #Temp_Budgets tb;              
      WITH  Budget_Details
              AS ( SELECT
                        Budget_Id
                       ,Budget_Account_Id
                       ,Account_Id
                       ,Month_Identifier
                       ,isnumeric(variable) AS Is_variable_Numeric
                       ,case WHEN isnumeric(variable) = 1 THEN convert(DECIMAL(32, 6), variable)
                        END variable_decimal
                       ,case WHEN isnumeric(variable) = 0 THEN variable
                        END Variable
                       ,isnumeric(TRANSPORTATION) AS Is_TRANSPORTATION_Numeric
                       ,case WHEN isnumeric(TRANSPORTATION) = 1 THEN convert(DECIMAL(32, 6), TRANSPORTATION)
                        END TRANSPORTATION_Decimal
                       ,case WHEN isnumeric(TRANSPORTATION) = 0 THEN TRANSPORTATION
                        END TRANSPORTATION
                       ,TRANSMISSION
                       ,DISTRIBUTION
                       ,OTHER_BUNDLED
                       ,OTHER_FIXED_COSTS
                       ,isnumeric(SOURCING_TAX) AS Is_SOURCING_TAX_Numeric
                       ,case WHEN isnumeric(SOURCING_TAX) = 1 THEN convert(DECIMAL(32, 6), SOURCING_TAX)
                        END SOURCING_TAX_Decimal
                       ,case WHEN isnumeric(SOURCING_TAX) = 0 THEN SOURCING_TAX
                        END SOURCING_TAX
                       ,RATES_TAX
                       ,IS_MANUAL_GENERATION
                       ,IS_MANUAL_SOURCING_TAX
                   FROM
                        #Temp_Budgets)
            INSERT      INTO #Budget_Details
                        ( 
                         Budget_Id
                        ,Budget_Account_Id
                        ,Account_Id
                        ,Month_Identifier
                        ,variable
                        ,TRANSPORTATION
                        ,TRANSMISSION
                        ,DISTRIBUTION
                        ,OTHER_BUNDLED
                        ,OTHER_FIXED_COSTS
                        ,SOURCING_TAX
                        ,RATES_TAX
                        ,IS_MANUAL_GENERATION
                        ,IS_MANUAL_SOURCING_TAX )
                        SELECT
                              Budget_Id
                             ,Budget_Account_Id
                             ,Account_Id
                             ,Month_Identifier
                             ,case WHEN max(Is_variable_Numeric) = 1 THEN case WHEN isnull(is_manual_generation, 0) = 0 THEN convert(VARCHAR(50), sum(variable_decimal))
                                                                               ELSE convert(VARCHAR(50), max(variable_decimal))
                                                                          END
                                   ELSE convert(VARCHAR(50), max(variable))
                              END variable
                             ,case WHEN max(Is_TRANSPORTATION_Numeric) = 1 THEN case WHEN isnull(is_manual_generation, 0) = 0 THEN convert(VARCHAR(50), sum(TRANSPORTATION_Decimal))
                                                                                     ELSE convert(VARCHAR(50), max(TRANSPORTATION_Decimal))
                                                                                END
                                   ELSE max(TRANSPORTATION)
                              END TRANSPORTATION
                             ,TRANSMISSION
                             ,DISTRIBUTION
                             ,OTHER_BUNDLED
                             ,OTHER_FIXED_COSTS
                             ,case WHEN max(Is_SOURCING_TAX_Numeric) = 1 THEN case WHEN ( is_manual_sourcing_tax = 1 ) THEN convert(VARCHAR(50), max(SOURCING_TAX_Decimal))
                                                                                   ELSE convert(VARCHAR(50), sum(SOURCING_TAX_Decimal))
                                                                              END
                                   ELSE max(SOURCING_TAX)
                              END SOURCING_TAX
                             ,RATES_TAX
                             ,IS_MANUAL_GENERATION
                             ,IS_MANUAL_SOURCING_TAX
                        FROM
                              Budget_Details
                        GROUP BY
                              Budget_Id
                             ,Budget_Account_Id
                             ,Account_Id
                             ,Month_Identifier
                             ,TRANSMISSION
                             ,DISTRIBUTION
                             ,OTHER_BUNDLED
                             ,OTHER_FIXED_COSTS
                             ,RATES_TAX
                             ,IS_MANUAL_GENERATION
                             ,IS_MANUAL_SOURCING_TAX              
                     
                        
  ---- Update usage from Budget_Usage for all the accounts and month identifier                            
                                      
;
      --WITH  Historic_Cost_Usage
      --        AS ( 
			  
			  SELECT
                        cuad.ACCOUNT_ID
                       ,cuad.Service_Month
                       ,round(cuad.Bucket_Value * cuc.Conversion_Factor, 0) Bucket_Value
					   INTO #Historic_Cost_Usage
                   FROM
                        dbo.Cost_Usage_Account_Dtl AS cuad
                        INNER JOIN ( SELECT
                                          cua.Account_Id
                                         ,month(cua.Service_Month) [smonth]
                                         ,max(cua.Service_Month) Service_Month
                                     FROM
                                          #Temp_Budget_Details tbd
                                          INNER JOIN dbo.Cost_Usage_Account_Dtl cua
                                                ON cua.ACCOUNT_ID = tbd.Account_Id
                                     WHERE
                                          cua.Bucket_Master_Id = @Bucket_Master_Id
                                          AND cua.Service_Month BETWEEN @CU_Start_Dt
                                                                AND     @CU_End_Dt
                                     GROUP BY
                                          cua.Account_Id
                                         ,month(cua.Service_Month) ) cu_acc
                              ON cuad.ACCOUNT_ID = cu_acc.ACCOUNT_ID
                                 AND cuad.Service_Month = cu_acc.Service_Month
                        INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                              ON cuc.BASE_UNIT_ID = cuad.UOM_Type_Id
                                 AND cuc.CONVERTED_UNIT_ID = @Budget_UOM_Id
                   WHERE
                        cuad.Bucket_Master_Id = @Bucket_Master_Id
						--)
            UPDATE
                  bd
            SET   
                  Volume = coalesce(bu.VOLUME * cnuc.CONVERSION_FACTOR, hcu.Bucket_Value)
            FROM
                  #Budget_Details bd
                  LEFT JOIN dbo.BUDGET_USAGE bu
                        ON bd.Account_Id = bu.ACCOUNT_ID
                           AND bd.Month_Identifier = bu.MONTH_IDENTIFIER
                  LEFT JOIN #Historic_Cost_Usage hcu
                        ON hcu.ACCOUNT_ID = bd.ACCOUNT_ID
                           AND month(hcu.Service_Month) = month(bd.Month_Identifier)
                  LEFT JOIN dbo.Consumption_Unit_Conversion cnuc
                        ON cnuc.Base_Unit_ID = bu.VOLUME_UNIT_TYPE_ID
                           AND cnuc.Converted_Unit_ID = @Budget_UOM_Id                             
                                             
              
  --    -- Update usage from Budget_Usage for all the accounts and month identifier for 1 year back                    
                                         
      SELECT
            Budget_Account_Id
           ,Account_Id
           ,Month_Identifier
           ,variable
           ,TRANSPORTATION
           ,TRANSMISSION
           ,DISTRIBUTION
           ,OTHER_BUNDLED
           ,OTHER_FIXED_COSTS
           ,SOURCING_TAX
           ,RATES_TAX
           ,IS_MANUAL_GENERATION
           ,IS_MANUAL_SOURCING_TAX
           ,isnull(convert(DECIMAL(32, 0), round(Volume, 0)), 0) Volume
           ,convert(DECIMAL(32, 3), round(Nymex_Price, 3)) Nymex_Price
      FROM
            #Budget_Details
      ORDER BY
            Account_Id
           ,Month_Identifier       
           
END;
;
GO
