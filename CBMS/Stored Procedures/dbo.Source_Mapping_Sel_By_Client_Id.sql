SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/******    
NAME:    
	dbo.Source_Mapping_Sel_By_Client_Id  

DESCRIPTION:    
	Used to get UOM Conversion from COMMODITY_UOM_CONVERSION table  

INPUT PARAMETERS:    
Name      DataType Default Description    
------------------------------------------------------------    
@Client_Id INT  
          
OUTPUT PARAMETERS:    
Name      DataType Default Description    
------------------------------------------------------------    
USAGE EXAMPLES:    
------------------------------------------------------------  

	EXEC dbo.Source_Mapping_Sel_By_Client_Id 10044
	EXEC dbo.Source_Mapping_Sel_By_Client_Id 10032
	EXEC dbo.Source_Mapping_Sel_By_Client_Id 170
	EXEC dbo.Source_Mapping_Sel_By_Client_Id 235
	
AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
GB   Geetansu Behera    
CMH  Chad Hattabaugh     
HG   Hari   
SKA  Shobhit Kr Agrawal  
SS	 Subhash Subramanyam

MODIFICATIONS     
Initials Date		Modification
------------------------------------------------------------
GB					created
SS    05-JUL-09		Converted Correlated Subquery to Explicit derive Join
SKA   16-JUL-09		Modified as per coding Standard
SKA   17-JUL-09		Modified parameter @ClientId to @Client_Id
SKA   27-JUL-09		Removed the CAST(cd2.CODE_VALUE AS VARCHAR(50) in Select clause
					Removed un-needed aliases
SKA	  24 Aug 09		Added WHERE ccd1.UOM_Cd>0 condition 	
SKA	  26 Aug 09		Replaced Drived table and put the straight join with Client_Commodity_Detail table
SKA	  22 sep 09		As per the functionality re design the script and used CTE for same.
SKA   01/19/2010	Referred Commodity_Service_Cd from Core.Client_commodity to get the sustainability commodities		
HG	  04/17/2010	Rewrote the query to remove UNION ALL based on UOM_CD
					Code table join moved out side the CTE
DMR	  09/10/2010    Modified for Quoted_Identifier
SKA	  10/26/2010    Removed ccd1.UOM_Cd from first select as we are showing multiple in case we have more then one units(Bug #21097)


******/
CREATE  PROCEDURE [dbo].[Source_Mapping_Sel_By_Client_Id]
	@Client_Id INT
AS

BEGIN

	SET NOCOUNT ON;

	WITH CTE_Commodity AS
	(
		SELECT
			cc.CLIENT_COMMODITY_ID 
			,cc.COMMODITY_ID
			,cm.COMMODITY_NAME
			,cc.CLIENT_ID
			,cc.SCOPE_CD  
			,cc.UOM_Cd
			,cc.FREQUENCY_CD
			,cc.HIER_LEVEL_CD
			,COUNT(DISTINCT ccd1.UOM_Cd) Uom_Cd_Count
		FROM
			core.CLIENT_COMMODITY cc
			INNER JOIN dbo.COMMODITY cm
				ON cc.COMMODITY_ID = cm.COMMODITY_ID
			INNER JOIN dbo.Code cd5
				ON cd5.Code_Id = cc.Commodity_Service_Cd
			LEFT JOIN Core.CLIENT_COMMODITY_DETAIL ccd1
				ON cc.CLIENT_COMMODITY_ID = ccd1.CLIENT_COMMODITY_ID
		WHERE
			cc.CLIENT_ID= @Client_Id
			AND cd5.Code_Value = 'GHG'
		GROUP BY
			cc.CLIENT_COMMODITY_ID
			,cc.COMMODITY_ID
			,cm.COMMODITY_NAME
			,cc.CLIENT_ID
			,cc.SCOPE_CD  
			,cc.UOM_Cd
			,cc.FREQUENCY_CD
			,cc.HIER_LEVEL_CD
	)
	SELECT
		cc.CLIENT_COMMODITY_ID
		,cc.COMMODITY_ID
		,cc.COMMODITY_NAME
		,cc.CLIENT_ID
		,cc.Scope_Cd
		,scd.Code_Value scope_name
		,cc.UOM_Cd
		,
		(	CASE
				WHEN cc.Uom_Cd_Count > 1 THEN 'Multiple'
				ELSE ucd.Code_Value
			END
		) AS 'uom_name'
		,cc.FREQUENCY_CD
		,fcd.Code_Value frequency_name
		,cc.Hier_Level_Cd
		,hlcd.Code_Value hier_level_name
	FROM
		CTE_Commodity cc
		INNER JOIN dbo.CODE scd
			ON scd.CODE_ID = cc.SCOPE_CD
		INNER JOIN dbo.CODE ucd
			ON ucd.CODE_ID = cc.UOM_Cd
		INNER JOIN dbo.CODE fcd
			ON fcd.CODE_ID = cc.FREQUENCY_CD
		INNER JOIN dbo.CODE hlcd
			ON hlcd.CODE_ID = cc.HIER_LEVEL_CD

END

GO
GRANT EXECUTE ON  [dbo].[Source_Mapping_Sel_By_Client_Id] TO [CBMSApplication]
GO
