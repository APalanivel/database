SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.Supplier_Account_Config_Exists_Sel_By_Account_Id_Config_Id
           
DESCRIPTION:             
			To get DMO supplier accounts associated to a client
			
INPUT PARAMETERS:            
	Name							DataType			Default			Description  
--------------------------------------------------------------------------------------------  
	@Supplier_Account_Id			INT
    @Supplier_Account_Config_Id		INT					NULL
   

OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	          
	EXEC dbo.Supplier_Account_Config_Exists_Sel_By_Account_Id_Config_Id 1197550,507644
	
	Select * from Supplier_Account_Config where account_Id = 1197550
		
	
	EXEC dbo.Supplier_Account_Config_Exists_Sel_By_Account_Id_Config_Id 1655261,62500
	
	Select * from Supplier_Account_Config where account_Id = 1655261
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	NR			Narayana Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	NR			2020-01-16	MAINT-9734 - Created
******/

CREATE PROCEDURE [dbo].[Supplier_Account_Config_Exists_Sel_By_Account_Id_Config_Id]
    (
        @Account_Id INT
        , @Supplier_Account_Config_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Is_Another_Config_Associated_To_Account BIT = 0;


        SELECT
            @Is_Another_Config_Associated_To_Account = 1
        FROM
            dbo.Supplier_Account_Config sac
            INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                ON samm.Supplier_Account_Config_Id = sac.Supplier_Account_Config_Id
                   AND  samm.ACCOUNT_ID = sac.Account_Id
        WHERE
            sac.Supplier_Account_Config_Id <> @Supplier_Account_Config_Id
            AND sac.Account_Id = @Account_Id;


        SELECT
            @Is_Another_Config_Associated_To_Account AS Is_Another_Config_Associated_To_Account;

    END;


GO
GRANT EXECUTE ON  [dbo].[Supplier_Account_Config_Exists_Sel_By_Account_Id_Config_Id] TO [CBMSApplication]
GO
