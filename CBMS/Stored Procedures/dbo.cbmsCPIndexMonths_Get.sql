SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsCPIndexMonths_Get

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@clearport_index_month_id	int       	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE    PROCEDURE [dbo].[cbmsCPIndexMonths_Get]
	( @clearport_index_month_id int = null
	)
AS
BEGIN

	   select clearport_index_month_id
		, clearport_index_month	
		, clearport_index_id	
	     from clearport_index_months with (nolock)
	    where clearport_index_month_id = @clearport_index_month_id


END
GO
GRANT EXECUTE ON  [dbo].[cbmsCPIndexMonths_Get] TO [CBMSApplication]
GO
