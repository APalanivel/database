SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME: [DBO].[SR_RFP_Miscellaneous_Power_Del]

DESCRIPTION:
	It Deletes SR RFP Miscellaneous Power for Selected
						SR RFP Miscellaneous Power Id.

INPUT PARAMETERS:
NAME							DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
@SR_RFP_Miscellaneous_Power_Id	INT

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  
	Begin Tran
		EXEC SR_RFP_Miscellaneous_Power_Del  2350
	Rollback Tran
    
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR			PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR		    31-MAY-10	CREATED     

*/  

CREATE PROCEDURE dbo.SR_RFP_Miscellaneous_Power_Del
   (
    @SR_RFP_Miscellaneous_Power_Id INT
   )
AS
BEGIN

    SET NOCOUNT ON;

    DELETE
   	FROM
		dbo.SR_RFP_MISCELLANEOUS_POWER
	WHERE
		SR_RFP_MISCELLANEOUS_POWER_ID = @SR_RFP_Miscellaneous_Power_Id

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_Miscellaneous_Power_Del] TO [CBMSApplication]
GO
