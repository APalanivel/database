SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE dbo.GET_BASELOADS_P
	@contractID INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT DISTINCT bl.baseload_id
		, baseload_name
	FROM dbo.load_profile_specification lps INNER JOIN dbo.baseload_details bld ON bld.load_profile_specification_id = lps.load_profile_specification_id
		INNER JOIN baseload bl ON bl.baseload_id = bld.baseload_id
	WHERE lps.contract_id = @contractID
			  
END
GO
GRANT EXECUTE ON  [dbo].[GET_BASELOADS_P] TO [CBMSApplication]
GO
