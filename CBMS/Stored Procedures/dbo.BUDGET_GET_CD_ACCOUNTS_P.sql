SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










--exec BUDGET_GET_CD_ACCOUNTS_P 16
CREATE     PROCEDURE dbo.BUDGET_GET_CD_ACCOUNTS_P
	@budgetId int
	AS
	begin
		set nocount on

		select 	bacc.budget_account_id 
		from 	budget_account bacc, account a,budget_tariff_account_vw tariff		
		where 	bacc.budget_id = @budgetId
			and bacc.account_id = a.account_id
			and tariff.account_id = bacc.account_id	
			and a.service_level_type_id in(861,1024)
			
		
		

	end


















GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_CD_ACCOUNTS_P] TO [CBMSApplication]
GO
