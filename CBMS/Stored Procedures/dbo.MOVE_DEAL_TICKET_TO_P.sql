SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.MOVE_DEAL_TICKET_TO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@queueId       	int       	          	
	@groupName     	varchar(200)	          	
	@dealTicketId  	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE   PROCEDURE DBO.MOVE_DEAL_TICKET_TO_P 

@userId varchar(10),
@sessionId varchar(20),
@queueId int,  
@groupName varchar(200), 
@dealTicketId int

AS
set nocount on
-- claim deal ticket
IF @queueId IS NULL AND @groupName IS NULL 
	BEGIN
		select @queueId = QUEUE_ID FROM USER_INFO WHERE USER_INFO_ID = @userId 
	END
--move deal ticket to a group
ELSE
	BEGIN
		IF @groupName IS NOT NULL
			BEGIN
				select @queueId = QUEUE_ID FROM GROUP_INFO WHERE GROUP_NAME = @groupName 
			END
	END

UPDATE 
	RM_DEAL_TICKET 
SET 
	QUEUE_ID = @queueId 
WHERE
	RM_DEAL_TICKET_ID = @dealTicketId
GO
GRANT EXECUTE ON  [dbo].[MOVE_DEAL_TICKET_TO_P] TO [CBMSApplication]
GO
