SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
--------------------------------------------------------------------------    
  
/******                                
Name:   dbo.Cu_Invoice_Standing_Data_Exception_Open_InProgress                        
                                
Description:                                
   SP to check if exception is Open/In Progress for an Invoice  
                                             
 Input Parameters:                                
    Name      DataType    Default   Description                                  
-----------------------   ----------------  -------------------------------------------------                              
@Cu_Invoice_Id     INT                          
                   
 Output Parameters:                                      
    Name        DataType   Default   Description                                  
----------------------------------------------------------------------------------------                                  
                                
 Usage Examples:                                    
----------------------------------------------------------------------------------------                     
                  
 EXEC Cu_Invoice_Standing_Data_Exception_Open_InProgress 29507                  
                 
                 
Author Initials:                                
    Initials    Name                                
--------------    --------------------------------------------------------------------------                                  
   
 HKT      Harish Kumar Tirumandyam                 
                
                 
Modifications:                                
    Initials   Date    Modification                                
---------------   ------    -------------------------------------------------------------------                                  
 HKT     2020-04-28   Created for Data Purple            
******/  
CREATE PROCEDURE [dbo].[Cu_Invoice_Standing_Data_Exception_Open_InProgress]  
     (  
         @Cu_Invoice_Id INT  
     )  
AS  
    BEGIN  
        SET NOCOUNT ON;  
  
        DECLARE @Is_Open BIT = 0;  
  
        SELECT  
            @Is_Open = 1  
        FROM  
            dbo.Cu_Invoice_Standing_Data_Exception AS CISDE  
            JOIN [dbo].[Code] AS [cde]  
                ON CISDE.[Exception_Status_Cd] = [cde].[Code_Id]  
        WHERE  
            CISDE.Cu_Invoice_Id = @Cu_Invoice_Id  
            AND Code_Value IN ( 'New','In Progress', 'Open' );  
  
       SELECT @Is_Open  AS Is_Open
    END;  
  
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Standing_Data_Exception_Open_InProgress] TO [CBMSApplication]
GO
