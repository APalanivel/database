SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE    PROCEDURE [dbo].[cbmsSsoMis_RemoveStatesForPublication]
	( @MyAccountId int
	, @sso_publication_id int
	)
AS
BEGIN

	set nocount on

	  delete sso_publications_state_map
	   where sso_publication_id = @sso_publication_id

--	exec cbmsPermissionInfo_GetMy @MyAccountId, @group_info_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoMis_RemoveStatesForPublication] TO [CBMSApplication]
GO
