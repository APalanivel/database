
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 
 dbo.cbmsCostUsageSite_GetSummaryDivisionDetailForExportAndSite_Lilly
 
 DESCRIPTION:   
 
	This procedure selects the comments associated to the report master id.
 
 INPUT PARAMETERS:  
 Name				DataType		Default				Description  
------------------------------------------------------------------------------------    
 @site_id			int
 @begin_date		datetime
 @end_date			datetime
	

 OUTPUT PARAMETERS:  
 Name				DataType		Default				Description  
------------------------------------------------------------------------------------    

  USAGE EXAMPLES:  
------------------------------------------------------------------------------------    

set Statistics Profile on 
	Exec [dbo].[cbmsCostUsageSite_GetSummaryDivisionDetailForExportAndSite] 24051, '1/1/2009', '12/31/2009'
	Exec [dbo].[cbmsCostUsageSite_GetSummaryDivisionDetailForExportAndSite_SKA] 24051, '1/1/2009', '12/31/2009'
	EXEC cbmsCostUsageSite_GetSummaryDivisionDetailForExportAndSite 528, '01/JAN/09', '31/DEC/09'

AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 JR			Jon Ruel
 MM			Moses Morales
 DR			Deana Ritter
 SKA		Shobhit Kr Agrawal
 NR			Narayana Reddy
 
 MODIFICATIONS   
 Initials		Date			Modification  
------------------------------------------------------------  
NR              2016-05-31		MAINT-3789 Replaced DV2-Commodity-Type(Read as '-' to '_') by Commodity.            

******/

CREATE PROCEDURE [dbo].[cbmsCostUsageSite_GetSummaryDivisionDetailForExportAndSite_Lilly]
      ( 
       @site_id INT
      ,@begin_date DATETIME
      ,@end_date DATETIME )
AS 
BEGIN
   
	  -- Get Unit_Type_ID for Natural Gas
	  
      DECLARE
            @ngunitID INT
           ,@ngunitname VARCHAR(25)
           
      SELECT
            @ngunitID = ENTITY_ID
           ,@ngunitname = ent.ENTITY_NAME
      FROM
            ENTITY ent
            INNER JOIN dbo.Commodity comtype
                  ON comtype.UOM_Entity_Type = ent.ENTITY_TYPE
      WHERE
            ent.ENTITY_NAME = 'MMBtu'
            AND comtype.Commodity_Name = 'Natural Gas'
            
      -- Get Unit_Type_ID for Steam
      DECLARE
            @steamunitID INT
           ,@steamunitname VARCHAR(25)
      SELECT
            @steamunitID = ENTITY_ID
           ,@steamunitname = ent.ENTITY_NAME
      FROM
            ENTITY ent
            INNER JOIN dbo.Commodity comtype
                  ON comtype.UOM_Entity_Type = ent.ENTITY_TYPE
      WHERE
            ent.ENTITY_NAME = 'MMBtu'
            AND comtype.Commodity_Name = 'Steam'
            
      -- Get Unit_Type_ID for Chilled Water
      DECLARE
            @cwunitID INT
           ,@cwunitname VARCHAR(25)
      SELECT
            @cwunitID = ENTITY_ID
           ,@cwunitname = ent.ENTITY_NAME
      FROM
            ENTITY ent
            INNER JOIN dbo.Commodity comtype
                  ON comtype.UOM_Entity_Type = ent.ENTITY_TYPE
      WHERE
            ent.ENTITY_NAME = 'Ton hours'
            AND comtype.Commodity_Name = 'Chilled Water'
            
               
      DECLARE @EL_COMMODITY_TYPE_ID INT
      
      SELECT
            @EL_COMMODITY_TYPE_ID = Commodity_Id
      FROM
            dbo.Commodity c
      WHERE
            Commodity_Name = 'Electric Power'
	 
      SELECT
            s.site_id
           ,s.site_name
           ,ISNULL(cu.el_usage, 0) * ISNULL(elcon.conversion_factor, 0) el_usage
           ,'Electric Power' AS el_commodity_type
           ,elunit.UBM_UNIT_OF_MEASURE_CODE el_unit_of_measure_type
           ,ISNULL(cu.ng_usage, 0) * ISNULL(ngcon.conversion_factor, 0) ng_usage
           ,@ngunitname ng_unit_of_measure_type
           ,'Natural Gas' AS ng_commodity_type
           ,ISNULL(alt.steam_usage, 0) * ISNULL(steamcon.conversion_factor, 0) steam_usage
           ,@steamunitname steam_unit_of_measure_type
           ,'Steam' AS steam_commodity_type
           ,ISNULL(alt.cw_usage, 0) * ISNULL(cwcon.conversion_factor, 0) cw_usage
           ,@cwunitname cw_unit_of_measure_type
           ,'Chilled Water' AS cw_commodity_type
           ,cu.service_month
      FROM
            client c
            JOIN site s
                  ON c.client_id = s.client_id
            JOIN cost_usage_site cu
                  ON ( cu.site_id = s.site_id
                       AND cu.service_month BETWEEN @begin_date
                                            AND     @end_date
                       AND cu.is_default = 1 )
            LEFT OUTER JOIN ( SELECT
                                    cusd.SITE_ID
                                   ,cusd.Service_Month
                                   ,SUM(CASE WHEN c.Commodity_Id = 1423 THEN cusd.Bucket_Value
                                             ELSE 0
                                        END) AS steam_usage
                                   ,SUM(CASE WHEN c.Commodity_Id = 1423 THEN cusd.UOM_Type_Id
                                             ELSE 0
                                        END) AS steam_unit_of_measure_type_id
                                   ,SUM(CASE WHEN c.Commodity_Id = 1503 THEN cusd.Bucket_Value
                                             ELSE 0
                                        END) AS cw_usage
                                   ,SUM(CASE WHEN c.Commodity_Id = 1503 THEN cusd.UOM_Type_Id
                                             ELSE 0
                                        END) AS cw_unit_of_measure_type_id
                              FROM
                                    Cost_Usage_Site_Dtl cusd
                                    INNER JOIN Bucket_Master bm
                                          ON cusd.Bucket_Master_Id = bm.Bucket_Master_Id
                                    INNER JOIN Commodity c
                                          ON c.Commodity_Id = bm.Commodity_Id
                              WHERE
                                    Bucket_Name = 'Volume'
                                    AND c.Commodity_Id IN ( 1503, 1423 )
                                    AND cusd.SITE_ID = @site_id
                              GROUP BY
                                    SITE_ID
                                   ,Service_Month ) alt
                  ON alt.SITE_ID = s.site_id
                     AND alt.Service_Month = cu.SERVICE_MONTH
            JOIN dbo.UBM_UNIT_OF_MEASURE_MAP elunit
                  ON elunit.COMMODITY_TYPE_ID = @EL_COMMODITY_TYPE_ID
                     AND elunit.UNIT_OF_MEASURE_TYPE_ID = cu.EL_UNIT_OF_MEASURE_TYPE_ID
                     AND elunit.UBM_UNIT_OF_MEASURE_MAP_ID = 10
            LEFT OUTER JOIN currency_unit_conversion cuc
                  ON cuc.base_unit_id = cu.currency_unit_id
                     AND cuc.converted_unit_id = cu.currency_unit_id
                     AND cuc.conversion_date = cu.service_month
                     AND cuc.currency_group_id = c.currency_group_id
            LEFT OUTER JOIN consumption_unit_conversion elcon
                  ON elcon.base_unit_id = cu.el_unit_of_measure_type_id
                     AND elcon.converted_unit_id = cu.EL_UNIT_OF_MEASURE_TYPE_ID
            LEFT OUTER JOIN consumption_unit_conversion ngcon
                  ON ngcon.base_unit_id = cu.ng_unit_of_measure_type_id
                     AND ngcon.converted_unit_id = @ngunitID
            LEFT OUTER JOIN consumption_unit_conversion steamcon
                  ON steamcon.base_unit_id = alt.steam_unit_of_measure_type_id
                     AND steamcon.converted_unit_id = @steamunitID
            LEFT OUTER JOIN consumption_unit_conversion cwcon
                  ON cwcon.base_unit_id = alt.cw_unit_of_measure_type_id
                     AND cwcon.converted_unit_id = @cwunitID
      WHERE
            s.SITE_ID = @site_id
            AND s.NOT_MANAGED <> 1
      ORDER BY
            s.site_name


END;

;
GO

GRANT EXECUTE ON  [dbo].[cbmsCostUsageSite_GetSummaryDivisionDetailForExportAndSite_Lilly] TO [CBMSApplication]
GO
