SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/***  
NAME :	dbo.Get_User_Email_Dtl_Sel_By_User_Info_Id 
   
DESCRIPTION: 

	This procedure used to get the new users which are created .
   
 INPUT PARAMETERS:    
 Name				DataType				Default      Description    
--------------------------------------------------------------------      
@User_Info_Id		INT
    
 OUTPUT PARAMETERS:    
 Name				DataType				Default      Description    
--------------------------------------------------------------------   
   
  USAGE EXAMPLES:    
--------------------------------------------------------------------    

	EXEC Get_User_Email_Dtl_Sel_By_User_Info_Id 2
  
AUTHOR INITIALS:    
 Initials		Name    
-------------------------------------------------------------------     
 NR				Narayana Reddy
 
  
 MODIFICATIONS
 Initials		Date		Modification
--------------------------------------------------------------------
  NR			2017-03-22	Created For MAINT-4824.
  
***/
CREATE PROCEDURE [dbo].[Get_User_Email_Dtl_Sel_By_User_Info_Id] ( @User_Info_Id INT )
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE
            @Notification_Template_Id INT
           ,@Msg_Delivery_Status_Cd INT
         

      SELECT
            @Msg_Delivery_Status_Cd = c.Code_Id
      FROM
            dbo.Code AS c
            INNER JOIN dbo.Codeset AS cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            c.Code_Value = 'Pending'
            AND cs.Codeset_Name = 'Message Delivery Status'

      SELECT
            @Notification_Template_Id = nt.Notification_Template_Id
      FROM
            dbo.Notification_Template nt
            INNER JOIN dbo.Code AS c
                  ON c.Code_Id = nt.Notification_Type_Cd
      WHERE
            c.Code_Value = 'New User Confirmation'

      SELECT
            ui.user_info_id
           ,ui.access_level
           ,ui.FIRST_NAME + SPACE(1) + LAST_NAME AS Full_Name
           ,ui.username
           ,ui.EMAIL_ADDRESS
           ,mq.Email_Subject
           ,mq.Email_Body
           ,md.Notification_Msg_Dtl_Id
      FROM
            dbo.USER_INFO ui
            INNER JOIN dbo.Notification_Msg_Dtl md
                  ON md.Recipient_User_Id = ui.USER_INFO_ID
            INNER JOIN dbo.Notification_Msg_Queue mq
                  ON mq.Notification_Msg_Queue_Id = md.Notification_Msg_Queue_Id
      WHERE
            ui.USER_INFO_ID = @User_Info_Id
            AND md.Msg_Delivery_Status_Cd = @Msg_Delivery_Status_Cd
            AND mq.Notification_Template_Id = @Notification_Template_Id
            AND ui.Access_Level = 1
            AND ui.Is_Demo_User = 0
            AND ui.Is_history = 0
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              dbo.Single_Signon_User_Map um
                             WHERE
                              um.Internal_User_ID = ui.USER_INFO_ID )

END

;
GO
GRANT EXECUTE ON  [dbo].[Get_User_Email_Dtl_Sel_By_User_Info_Id] TO [CBMSApplication]
GO
