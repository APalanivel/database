SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_SAVED_DT_SUPPLIER_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@srDealTicketId	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--select * from SR_DT_SUPPLY_INFO where SR_DEAL_TICKET_ID=794 
--select * from SR_DEAL_TICKET_VOLUME_DETAILS where SR_DEAL_TICKET_ID=708 


CREATE    PROCEDURE dbo.SR_SAD_GET_SAVED_DT_SUPPLIER_DETAILS_P
@srDealTicketId int

AS
set nocount on
	select 
		VENDOR_ID, PRICE, IS_APPROVED, COMMENTS 
	from 
		SR_DT_SUPPLY_INFO
	
	where 
		SR_DEAL_TICKET_ID=@srDealTicketId
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_SAVED_DT_SUPPLIER_DETAILS_P] TO [CBMSApplication]
GO
