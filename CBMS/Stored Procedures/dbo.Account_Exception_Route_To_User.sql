SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Account_Exception_Route_To_User       
              
Description:              
			This sproc route the queue id updated in the Account_Exception table.      
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------            
    @Routed_To_User_Queue				INT					
    @Account_Exception_Ids				INT	
    @User_Info_Id						INT				
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   
    BEGIN TRAN
    EXEC dbo.Account_Exception_Route_To_User 
      49
     ,'1,2'
     ,49
    ROLLBACK TRAN

    BEGIN TRAN
    EXEC dbo.Account_Exception_Route_To_User 
      49
     ,'10,12,15,18'
     ,49
    ROLLBACK TRAN

   
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy               
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2015-04-22		Created For AS400.         
             
******/
CREATE PROCEDURE [dbo].[Account_Exception_Route_To_User]
      ( 
       @Routed_To_User_Queue INT
      ,@Account_Exception_Ids VARCHAR(MAX)
      ,@User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 
      
      DECLARE @Account_Exception TABLE
            ( 
             Account_Exception_Id INT )
                 
     
      INSERT      @Account_Exception
                  ( 
                   Account_Exception_Id )
                  SELECT
                        us.Segments
                  FROM
                        dbo.ufn_split(@Account_Exception_Ids, ',') AS us
                  GROUP BY
                        us.Segments
            
            
      UPDATE
            ae
      SET   
            ae.Queue_Id = @Routed_To_User_Queue
           ,ae.Updated_User_Id = @User_Info_Id
           ,ae.Last_Change_Ts = GETDATE()
      FROM
            dbo.Account_Exception ae
            INNER JOIN @Account_Exception aes
                  ON ae.Account_Exception_Id = aes.Account_Exception_Id
     
END;



;
GO
GRANT EXECUTE ON  [dbo].[Account_Exception_Route_To_User] TO [CBMSApplication]
GO
