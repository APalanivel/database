
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].cbmsCostUsageSite_GetTotalCostForClient  

DESCRIPTION: 
	
      
INPUT PARAMETERS:          
    NAME			    DATATYPE	 DEFAULT	   DESCRIPTION         
--------------------------------------------------------------------
    @Client_ID		    INT
    @Division_ID	    INT		 NULL
    @Site_ID		    INT		 NULL
    @Begin_Month	    DATETIME
    @End_Month		    DATETIME
    @Bucket_List	    dbo.Cost_Usage_Bucket READONLY
      
OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
DECLARE @Bucket_List1 AS Cost_Usage_Bucket

INSERT INTO @Bucket_List1
VALUES (290, 'Total Usage', 'Determinant', 12)
    ,(290, 'Total Cost', 'Charge', 3)

EXECUTE [dbo].[cbmsCostUsageSite_GetTotalCostForClient] 10069, NULL, NULL, '01/01/2005', '12/31/2005', @Bucket_List1

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	AP		 Athmaram Pabbathi

MODIFICATIONS:
    INITIALS	DATE		   MODIFICATION
------------------------------------------------------------
    AP		08/17/2011   Removed unused Parameter @MyAccountID and Removed Cost_Usage_Site & Invoice_Participation_Site tables and used Cost_Usage_Site_Dtl, 
					   Invoice_Participation, Bucket_Master, Commodity tables and modified the script to use CTE
    AP		08/22/2011    Added qualifiers to all objects with the owner name.
    AP		08/24/2011   Removed base tables from CTE_Client_Dtl and used Client_Hier table and removed NOLOCK statements
    AP		08/31/2011   Added new parameters for @Commodity_Name, @Bucket_Master_ID, @UOM_Type_ID & @Currency_Unit_Id and modified the logic to get the data
					   when multiple commodities & buckets selected.
    AP		03/29/2012   Modified joins to use Client_Hier_Id on CUSD and in temp tables
    AP		04/10/2012   Replaced @Bucket_Name, @Commodity_Name, @Currency_Unit_Id, @UOM_Type_Id parameters with @Bucket_List tvp paramater
					   Replaced @Site_Id with @Site_Client_Hier_ID parameter
*/

CREATE       PROCEDURE [dbo].[cbmsCostUsageSite_GetTotalCostForClient]
      ( 
       @Client_ID INT
      ,@Division_ID INT = NULL
      ,@Site_Client_Hier_ID INT = NULL
      ,@Begin_Month DATETIME
      ,@End_Month DATETIME
      ,@Bucket_List AS dbo.Cost_Usage_Bucket READONLY )
AS 
BEGIN
      SET NOCOUNT ON

      CREATE TABLE #Client_Dtl
            ( 
             Client_Hier_Id INT PRIMARY KEY CLUSTERED
            ,Site_Id INT
            ,Currency_Group_Id INT )
            
      CREATE TABLE #Invoice_Participation_Site
            ( 
             Client_Hier_Id INT
            ,Service_Month DATE
            ,Is_Published SMALLINT )

      CREATE INDEX IDX_Invoice_Participation_Site ON #Invoice_Participation_Site(Client_Hier_Id, Service_Month)


      INSERT      INTO #Client_Dtl
                  ( 
                   Client_Hier_Id
                  ,Site_Id
                  ,Currency_Group_Id )
                  SELECT
                        CH.Client_Hier_Id
                       ,CH.Site_ID
                       ,CH.Client_Currency_Group_ID AS Currency_Group_ID
                  FROM
                        Core.Client_Hier CH
                  WHERE
                        CH.Client_id = @Client_ID
                        AND ( @Division_ID IS NULL
                              OR CH.Sitegroup_Id = @Division_ID )
                        AND ( @Site_Client_Hier_ID IS NULL
                              OR CH.Client_Hier_Id = @Site_Client_Hier_ID )
                        AND CH.Site_Closed = 0
                        AND CH.Site_Not_Managed = 0
                        AND CH.Site_ID > 0
                  GROUP BY
                        CH.Client_Hier_Id
                       ,CH.Site_ID
                       ,CH.Client_Currency_Group_ID
                       

      INSERT      INTO #Invoice_Participation_Site
                  ( 
                   Client_Hier_Id
                  ,Service_Month
                  ,Is_Published )
                  SELECT
                        cha.Client_Hier_Id
                       ,IP.Service_Month
                       ,( case WHEN sum(cast(IP.Is_Received AS INT)) > 0 THEN 1
                               ELSE 0
                          END ) AS Is_Published
                  FROM
                        dbo.Invoice_Participation IP
                        INNER JOIN ( SELECT
                                          cha.Account_Id
                                         ,cha.Client_Hier_Id
                                         ,cd.Site_Id
                                     FROM
                                          Core.Client_Hier_Account cha
                                          INNER JOIN @Bucket_List bl
                                                ON bl.Commodity_Id = cha.Commodity_Id
                                          INNER JOIN #Client_Dtl CD
                                                ON CD.Client_Hier_Id = cha.Client_Hier_Id
                                     GROUP BY
                                          cha.Account_Id
                                         ,cha.Client_Hier_Id
                                         ,cd.Site_Id ) CHA
                              ON CHA.Account_ID = IP.Account_ID
                                 AND cha.Site_Id = ip.Site_Id
                  WHERE
                        IP.Service_Month BETWEEN @Begin_Month
                                         AND     @End_Month
                  GROUP BY
                        cha.Client_Hier_Id
                       ,IP.Service_Month
                       

      SELECT
            COM.Commodity_Name
           ,BM.Bucket_Name
           ,CD.Code_Value
           ,sum(case WHEN CD.Code_Value = 'Charge' THEN isnull(CUSD.Bucket_Value * CurConv.Conversion_Factor, 0)
                     ELSE isnull(CUSD.Bucket_Value * UOM.Conversion_Factor, 0)
                END) AS Bucket_Value
      FROM
            #Client_Dtl C
            INNER JOIN dbo.Cost_Usage_Site_Dtl CUSD
                  ON CUSD.Client_Hier_Id = C.Client_Hier_Id
            INNER JOIN dbo.Bucket_Master BM
                  ON BM.Bucket_Master_Id = CUSD.Bucket_Master_Id
            INNER JOIN dbo.Code CD
                  ON CD.Code_Id = BM.Bucket_Type_Cd
            INNER JOIN @Bucket_List bl
                  ON bl.Bucket_Name = bm.Bucket_Name
                     AND bl.Bucket_Type = cd.Code_Value
                     AND bl.Commodity_Id = bm.Commodity_Id
            INNER JOIN dbo.Commodity COM
                  ON COM.Commodity_Id = BM.Commodity_Id
            LEFT OUTER JOIN dbo.Currency_Unit_Conversion CurConv
                  ON CurConv.Base_Unit_ID = CUSD.Currency_Unit_ID
                     AND CurConv.Conversion_Date = CUSD.Service_Month
                     AND CurConv.Currency_Group_ID = C.Currency_Group_ID
                     AND CurConv.Converted_Unit_ID = bl.Bucket_Uom_Id
                     AND bl.Bucket_Type = 'Charge'
            LEFT OUTER JOIN dbo.Consumption_Unit_Conversion UOM
                  ON UOM.Base_Unit_ID = CUSD.UOM_Type_Id
                     AND UOM.Converted_Unit_ID = bl.Bucket_Uom_Id
                     AND bl.Bucket_Type = 'Determinant'
            LEFT OUTER JOIN #Invoice_Participation_Site IPS
                  ON IPS.Client_Hier_Id = CUSD.Client_Hier_Id
                     AND IPS.Service_Month = CUSD.Service_Month
      WHERE
            CUSD.Service_Month BETWEEN @Begin_Month
                               AND     @End_Month
            AND ( IPS.Is_Published = 1
                  OR IPS.Is_Published IS NULL )
      GROUP BY
            COM.Commodity_Name
           ,BM.Bucket_Name
           ,CD.Code_Value
      ORDER BY
            Bucket_Value DESC 

      DROP TABLE #Client_Dtl
      DROP TABLE #Invoice_Participation_Site


END
;
GO

GRANT EXECUTE ON  [dbo].[cbmsCostUsageSite_GetTotalCostForClient] TO [CBMSApplication]
GO
