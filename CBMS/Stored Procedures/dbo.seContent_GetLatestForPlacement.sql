SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure dbo.seContent_GetLatestForPlacement
	( @ContentPlacementId int 
	, @ContentLocaleId int
	)
As
BEGIN
	set nocount on
	
	select c.ContentId
				, c.ContentLocaleId
				, c.ContentPlacementId
				, c.Version
				, c.CurrentStatusId
				, c.ContentText
		 from seContent c
		 join (select ContentLocaleId
								, ContentPlacementId
								, max(Version) Version
						 from seContent
						where ContentPlacementId = @ContentPlacementId
						  and ContentLocaleId = @ContentLocaleId
				 group by ContentLocaleId
								, ContentPlacementId
					) x on (c.ContentPlacementId = x.ContentPlacementId and c.ContentLocaleId = x.ContentLocaleId and c.Version = x.Version)
END
GO
GRANT EXECUTE ON  [dbo].[seContent_GetLatestForPlacement] TO [CBMSApplication]
GO
