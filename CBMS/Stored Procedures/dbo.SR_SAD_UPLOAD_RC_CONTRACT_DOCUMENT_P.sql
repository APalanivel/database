SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[SR_SAD_UPLOAD_RC_CONTRACT_DOCUMENT_P]
	@userId INT,
	@sessionId VARCHAR(100),
	--@cbmsImageDocId varchar(200),
	--@cbmsImage image ,
	--@contentType varchar(200),
	@rfpId INT,
	@cbmsImageId INT,  -- added by Jaya
	@accountId INT
AS
BEGIN
	
	SET NOCOUNT ON
	
	DECLARE @entityId INT
		, @contractDocId INT
		, @rcContractDocId INT
		, @rcDocId INT
		, @cnt INT
    
	SELECT @cnt = COUNT(1) FROM dbo.SR_RC_CONTRACT_DOCUMENT_ACCOUNTS_MAP WHERE account_id = @accountId AND sr_rfp_id = @rfpId
    
	SELECT @entityId = ENTITY_ID FROM dbo.ENTITY(NOLOCK) WHERE Entity_Name = 'Contract Review' AND Entity_Type = 100
    
	SELECT @contractDocId = rcDoc.sr_rc_contract_document_id
	FROM dbo.cbms_image cbmsImage 
		INNER JOIN dbo.sr_rc_contract_document rcDoc ON rcDoc.contract_image_id = cbmsImage.cbms_image_id 
		INNER JOIN dbo.SR_RC_CONTRACT_DOCUMENT_ACCOUNTS_MAP accMap ON accMap.sr_rc_contract_document_id = rcDoc.sr_rc_contract_document_id
	WHERE accMap.account_id = @accountId
		AND accMap.sr_rfp_id = @rfpId 
		AND cbmsImage.cbms_image_type_id = @entityId
    
	SELECT @rcDocId = ISNULL(rcDoc.sr_rc_contract_document_id, 0)
	FROM dbo.cbms_image cbmsImage
		INNER JOIN dbo.sr_rc_contract_document rcDoc ON rcDoc.rc_image_id = cbmsImage.cbms_image_id
		INNER JOIN dbo.SR_RC_CONTRACT_DOCUMENT_ACCOUNTS_MAP accMap ON accMap.sr_rc_contract_document_id = rcDoc.sr_rc_contract_document_id
	WHERE accMap.account_id = @accountId
		AND accMap.sr_rfp_id = @rfpId    

	IF (@contractDocId > 0 AND @cnt > 0 AND @rcDocId = 0)
	 BEGIN

		/*INSERT INTO CBMS_IMAGE (CBMS_IMAGE_TYPE_ID, CBMS_DOC_ID, CBMS_IMAGE, CONTENT_TYPE, DATE_IMAGED)     
		VALUES (@entityId, @cbmsImageDocId, @cbmsImage, @contentType, getDate())     
    
     
		select @cbmsImageId = @@Identity  */    
    
		--added by Jaya for updating entityid    
     
		UPDATE dbo.CBMS_IMAGE SET CBMS_IMAGE_TYPE_ID = @entityId WHERE CBMS_IMAGE_ID = @cbmsImageId

		UPDATE dbo.SR_RC_CONTRACT_DOCUMENT
			SET CONTRACT_IMAGE_ID = @cbmsImageId,
				UPLOADED_BY = @userId,
				UPLOADED_DATE = GETDATE()
		WHERE SR_RC_CONTRACT_DOCUMENT_ID = @contractDocId
    
	 END
	ELSE IF ( @cnt = 0 )
	 BEGIN
	 
		/*INSERT INTO CBMS_IMAGE (CBMS_IMAGE_TYPE_ID, CBMS_DOC_ID, CBMS_IMAGE, CONTENT_TYPE, DATE_IMAGED)     
		VALUES (@entityId, @cbmsImageDocId, @cbmsImage, @contentType, getDate())    
    
     
		select @cbmsImageId = (select @@Identity)  */    
     
		--added by Jaya for updating entityid    
     
		UPDATE dbo.CBMS_IMAGE SET CBMS_IMAGE_TYPE_ID = @entityId WHERE CBMS_IMAGE_ID = @cbmsImageId
    
		INSERT INTO dbo.SR_RC_CONTRACT_DOCUMENT (CONTRACT_IMAGE_ID,UPLOADED_BY,UPLOADED_DATE)
		VALUES (@cbmsImageId , @userId, GETDATE())
    
		SELECT @rcContractDocId = SCOPE_IDENTITY() -- @@identity    
    
		INSERT INTO dbo.SR_RC_CONTRACT_DOCUMENT_ACCOUNTS_MAP (SR_RC_CONTRACT_DOCUMENT_ID,ACCOUNT_ID,SR_RFP_ID)     
		VALUES ( @rcContractDocId, @accountId, @rfpId )

	 END
	ELSE IF (@cnt > 0 AND @rcDocId > 0)
	 BEGIN

		/*INSERT INTO CBMS_IMAGE (CBMS_IMAGE_TYPE_ID, CBMS_DOC_ID, CBMS_IMAGE, CONTENT_TYPE, DATE_IMAGED)     
		VALUES (@entityId, @cbmsImageDocId, @cbmsImage, @contentType, getDate())    

		select @cbmsImageId = (select @@Identity)  */    

		--added by Jaya for updating entityid

		UPDATE dbo.CBMS_IMAGE SET CBMS_IMAGE_TYPE_ID = @entityId WHERE CBMS_IMAGE_ID = @cbmsImageId    

		UPDATE dbo.SR_RC_CONTRACT_DOCUMENT     
			SET CONTRACT_IMAGE_ID = @cbmsImageId,
				UPLOADED_BY = @userId,    
				UPLOADED_DATE = GETDATE()
		WHERE  SR_RC_CONTRACT_DOCUMENT_ID = @rcDocId

	 END    
    
	UPDATE rfpCheckList
		SET is_contract_reviewed = 1
	FROM dbo.sr_rfp_checklist rfpCheckList
		INNER JOIN dbo.sr_rfp_account rfpAcct ON rfpAcct.sr_rfp_account_id = rfpCheckList.sr_rfp_account_id
	WHERE rfpAcct.account_id = @accountId
		AND sr_rfp_id = @rfpId

END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_UPLOAD_RC_CONTRACT_DOCUMENT_P] TO [CBMSApplication]
GO
