SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.GET_MARKET_PRICE_INDEXES_P
AS
BEGIN
SELECT  market_price.market_price_index_id,
	market_price.market_price_index_name 
FROM    market_price_index market_price
END


GO
GRANT EXECUTE ON  [dbo].[GET_MARKET_PRICE_INDEXES_P] TO [CBMSApplication]
GO
