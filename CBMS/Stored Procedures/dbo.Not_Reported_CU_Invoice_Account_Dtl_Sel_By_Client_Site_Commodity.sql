
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
    
/******    
NAME:    
 CBMS.dbo.Not_Reported_CU_Invoice_Account_Dtl_Sel_By_Client_Site_Commodity    
 DESCRIPTION:     
 As we are removing the C&U data if the invoice is marked as not reported user should have ability to see that invoice details to again mark it as reported.    
 this procedure used to select not reported invoice account details based on the given input.   
    
 INPUT PARAMETERS:    
 Name				DataType	Default		Description
------------------------------------------------------------
 @Client_Hier_Id	INT
 @Account_Id		INT			NULL
 @Commodity_Id		INT
 @Begin_Dt			DATE
 @End_Dt			DATE

 OUTPUT PARAMETERS:
 Name   DataType  Default Description
------------------------------------------------------------
 USAGE EXAMPLES:
------------------------------------------------------------       

	EXEC dbo.Not_Reported_CU_Invoice_Account_Dtl_Sel_By_Client_Site_Commodity  1154,NULL,290,'2/1/2005','2/1/2005'    
	EXEC dbo.Not_Reported_CU_Invoice_Account_Dtl_Sel_By_Client_Site_Commodity  5560,null,290,'2/1/2005','2/1/2005'       
	EXEC dbo.Not_Reported_CU_Invoice_Account_Dtl_Sel_By_Client_Site_Commodity  1154,10698,290,'2/1/2005','2/1/2005'    
  
  AUTHOR INITIALS:    
 Initials	Name    
------------------------------------------------------------    
 HG			Hari    
 PNR		Pandarinath    
 SKA		Shobhit      
 AP			Athmaram Pabbathi  
   
 MODIFICATIONS     
  Initials  Date		Modification    
------------------------------------------------------------    
 HG			04/26/2010  Created    
						Volume, Utility_Cost, Marketer_Cost and Total_Usage column added as the result set of this procedure is will be merged with result set of Cost_Usage_Account_Dtl_Sel_By_Client_Account_Commodity.    
 HG			05/13/2010  GROUP BY clause added as we will have multiple entries for the same invoice in determinant.    
 SKA		05/21/2010  Refere Account_Id from CU_INVOICE_SERVICE_MONTH instead of CU_INVOICE    
 HG			08/10/2010  Procedure filtering the commodity only based on determinant, as we can have services with charges but no determinant modified the commodity filter to based on either Cu_Invoice_Determinant/ Cu_Invoice_Charge    
 BCH		10/18/2011  Removed for finding utility and supplier accounts with old tables at CTE_ACCOUNT_LIST, used  core.client_hier_account and dbo.SUPPLIER_ACCOUNT_METER_MAP tables for finding combination of utility and supplier accounts  
						Used to finding the  utility and supplier account_type_id's from variables  
 AP			03/20/2012  Addnl Data Changes  
							-- Replaced @Site_Id parameter with @Client_Hier_Id  
							-- removed unused parameter @Client_Id   
							-- removed base tables and used CHA & CH  
 AKR		06/08/2012  Modified the Order By Clause
 HG			2012-08-06	Modified the query to return NULL value for Volume, Utility_Cost, Marketer_Cost and Total_Usage columns as we wanted to differentiate between NULL and 0
******/   
CREATE PROCEDURE dbo.Not_Reported_CU_Invoice_Account_Dtl_Sel_By_Client_Site_Commodity
      ( 
       @Client_Hier_Id INT
      ,@Account_Id INT = NULL
      ,@Commodity_Id INT
      ,@Begin_Dt DATE
      ,@End_Dt DATE )
AS 
BEGIN    
      SET NOCOUNT ON ;    
      DECLARE
            @Utility_Account_Type_id INT
           ,@Supplier_Account_Type_id INT   
             
      DECLARE @Invoice_Dtl TABLE
            ( 
             ACCOUNT_ID INT
            ,ACCOUNT_NUMBER VARCHAR(50)
            ,ACCOUNT_TYPE_ID INT
            ,Account_Type VARCHAR(30)
            ,VENDOR_ID INT
            ,VENDOR_NAME VARCHAR(200)
            ,Service_Month DATE
            ,Cu_Invoice_Id INT
            ,PRIMARY KEY CLUSTERED ( Cu_Invoice_Id, Account_Id, Service_Month ) )   
    
      SELECT
            @Utility_Account_Type_id = ENTITY_ID
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_DESCRIPTION = 'Account Type'
            AND ENTITY_NAME = 'Utility'    
      SELECT
            @Supplier_Account_Type_id = ENTITY_ID
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_DESCRIPTION = 'Account Type'
            AND ENTITY_NAME = 'Supplier' ;  
              
  
      WITH  Cte_Account_List
              AS ( SELECT
                        cha.Account_Vendor_Id AS Vendor_Id
                       ,cha.Account_Id
                       ,( case WHEN cha.Account_Type = 'Utility' THEN @Utility_Account_Type_Id
                               ELSE @Supplier_Account_Type_Id
                          END ) AS Account_Type_Id
                       ,ltrim(rtrim(cha.Account_Type)) AS Account_Type
                       ,cha.Display_Account_Number AS ACCOUNT_NUMBER
                       ,ch.Site_Id
                       ,cha.Account_Vendor_Name AS Vendor_Name
                   FROM
                        Core.Client_Hier ch
                        JOIN Core.Client_Hier_Account cha
                              ON ch.Client_Hier_Id = cha.Client_Hier_Id
                   WHERE
                        ( @Account_ID IS NULL
                          OR cha.ACCOUNT_ID = @Account_ID )
                        AND ch.Client_Hier_Id = @Client_Hier_Id
                        AND cha.Commodity_Id = @Commodity_Id
                   GROUP BY
                        cha.Account_Vendor_Id
                       ,cha.Account_Id
                       ,( case WHEN cha.Account_Type = 'Utility' THEN @Utility_Account_Type_Id
                               ELSE @Supplier_Account_Type_Id
                          END )
                       ,ltrim(rtrim(cha.Account_Type))
                       ,cha.Display_Account_Number
                       ,ch.Site_Id
                       ,cha.Account_Vendor_Name)
            INSERT      INTO @Invoice_Dtl
                        ( 
                         Account_Id
                        ,Account_Number
                        ,Account_Type_Id
                        ,Account_Type
                        ,Vendor_Id
                        ,Vendor_Name
                        ,Service_Month
                        ,Cu_Invoice_Id )
                        SELECT
                              ism.Account_Id
                             ,acc.Account_Number
                             ,acc.Account_Type_Id
                             ,acc.Account_Type
                             ,acc.Vendor_Id
                             ,acc.Vendor_Name
                             ,ism.Service_Month
                             ,cu.Cu_Invoice_Id
                        FROM
                              dbo.CU_Invoice cu
                              JOIN dbo.CU_Invoice_Service_Month ism
                                    ON ism.Cu_Invoice_Id = cu.Cu_Invoice_Id
                              JOIN Cte_Account_List acc
                                    ON acc.Account_Id = ism.Account_Id
                        WHERE
                              ism.Service_Month BETWEEN @Begin_Dt AND @End_Dt
                              AND cu.Is_Reported = 0
                        GROUP BY
                              ism.Account_Id
                             ,acc.Account_Number
                             ,acc.Account_Type_Id
                             ,acc.Account_Type
                             ,acc.Vendor_Id
                             ,acc.Vendor_Name
                             ,ism.Service_Month
                             ,cu.Cu_Invoice_Id ;  
    
      WITH  Cte_Determinant_Charge_Invoice
              AS ( SELECT
                        inv.Cu_Invoice_Id
                   FROM
                        dbo.CU_Invoice_Determinant det
                        JOIN @Invoice_Dtl inv
                              ON det.Cu_Invoice_Id = inv.Cu_Invoice_Id
                   WHERE
                        det.Commodity_Type_Id = @Commodity_Id
                   UNION
                   SELECT
                        inv.Cu_Invoice_Id
                   FROM
                        dbo.CU_Invoice_Charge chg
                        JOIN @Invoice_Dtl inv
                              ON chg.Cu_Invoice_Id = inv.Cu_Invoice_Id
                   WHERE
                        chg.Commodity_Type_Id = @Commodity_Id )
            SELECT
                  inv.Account_Id
                 ,inv.Account_Number
                 ,inv.Account_Type_Id
                 ,inv.Account_Type
                 ,inv.Vendor_Id
                 ,inv.Vendor_Name
                 ,inv.Service_Month
                 ,inv.Cu_Invoice_Id
                 ,NULL AS Volume
                 ,NULL AS Utility_Cost
                 ,NULL AS Marketer_Cost
                 ,NULL AS Total_Cost
                 ,NULL AS Unit_Cost
            FROM
                  @Invoice_Dtl inv
                  JOIN Cte_Determinant_Charge_Invoice Det_Chg_Inv
                        ON Det_Chg_Inv.Cu_Invoice_Id = inv.Cu_Invoice_Id
            ORDER BY
                  inv.Account_Type DESC
                 ,inv.Account_Number ASC
                 ,inv.Vendor_Name                     
   
END    

;
GO


GRANT EXECUTE ON  [dbo].[Not_Reported_CU_Invoice_Account_Dtl_Sel_By_Client_Site_Commodity] TO [CBMSApplication]
GO
