SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
                         
 NAME: dbo.RM_CBMS_TXN_Deal_Ticket_Trade_Data_Load_By_DT                     
                          
 DESCRIPTION:      
		  To load TXN trade data from stage table to CBMS tables

 INPUT PARAMETERS:      
                         
	Name                   DataType          Default       Description      
-------------------------------------------------------------------------------------
                          
 OUTPUT PARAMETERS:      
                               
	Name                   DataType          Default       Description      
-------------------------------------------------------------------------------------
                          
 USAGE EXAMPLES:                              
-------------------------------------------------------------------------------------
          
		  EXEC  dbo.RM_CBMS_TXN_Deal_Ticket_Trade_Data_Load_By_DT 671
		  
		  EXEC  dbo.RM_CBMS_TXN_Deal_Ticket_Trade_Data_Load_By_DT 677
		  
		  EXEC  dbo.RM_CBMS_TXN_Deal_Ticket_Trade_Data_Load_By_DT 683
		  
		  EXEC  dbo.RM_CBMS_TXN_Deal_Ticket_Trade_Data_Load_By_DT 131459
	 
                         
 AUTHOR INITIALS:    
       
	Initials	Name      
-------------------------------------------------------------------------------------
	Raghu       Raghu Reddy                            
                           
 MODIFICATIONS:    
                           
	Initials    Date            Modification    
-------------------------------------------------------------------------------------
	RR          2013-03-15      Created for GRM                        
                         
******/
--27337

CREATE PROCEDURE [dbo].[RM_CBMS_TXN_Deal_Ticket_Trade_Data_Load_By_DT]
      ( 
       @Deal_Ticket_Id INT = NULL )
AS 
BEGIN

      SET NOCOUNT ON;
      
      INSERT      INTO Trade.Trade_Price
                  ( 
                   Deal_Tickets_Price_XId
                  ,Trade_Price
                  ,Mid_Price
                  ,Market_Price
                  ,Created_Ts
                  ,Last_Change_Ts )
                  SELECT
                        deal_tickets_price_id
                       ,trade_price
                       ,mid_price
                       ,market_price
                       ,GETDATE()
                       ,GETDATE()
                  FROM
                        Trade.Deal_Tickets sdt --cdc.fn_cdc_get_net_changes_Trade_Deal_Tickets(@Start_LSN, @End_LSN, N'all') sdt--
                  WHERE
                        ( @Deal_Ticket_Id IS NULL
                          OR sdt.cbms_deal_ticket_id = @Deal_Ticket_Id )
                        AND deal_tickets_price_id IS NOT NULL
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          Trade.Trade_Price ttp
                                         WHERE
                                          sdt.deal_tickets_price_id = ttp.Deal_Tickets_Price_XId )
                  GROUP BY
                        deal_tickets_price_id
                       ,trade_price
                       ,mid_price
                       ,market_price;
                                          
                                          
      UPDATE
            chws
      SET   
            chws.Is_Active = 0
           ,chws.Last_Change_Ts = GETDATE()
      FROM
            Trade.Deal_Tickets sdt --cdc.fn_cdc_get_net_changes_Trade_Deal_Tickets(@Start_LSN, @End_LSN, N'all') sdt --
            INNER JOIN Trade.Deal_Ticket dt
                  ON sdt.cbms_deal_ticket_id = dt.Deal_Ticket_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                  ON sdt.cbms_site_client_hier_id = dtch.Client_Hier_Id
                     AND dt.Deal_Ticket_Id = dtch.Deal_Ticket_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                  ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
      WHERE
            ( chws.Trade_Month IS NULL
              OR sdt.period_from_date = chws.Trade_Month )
            AND ( chws.Contract_Id IS NULL
                  OR sdt.cbms_contract_id = chws.Contract_Id )
            AND ( @Deal_Ticket_Id IS NULL
                  OR sdt.cbms_deal_ticket_id = @Deal_Ticket_Id )

      INSERT      INTO Trade.Deal_Ticket_Client_Hier_Workflow_Status
                  ( 
                   Deal_Ticket_Client_Hier_Id
                  ,Workflow_Status_Map_Id
                  ,Workflow_Status_Comment
                  ,Is_Active
                  ,Last_Notification_Sent_Ts
                  ,Created_Ts
                  ,Updated_User_Id
                  ,Last_Change_Ts
                  ,CBMS_Image_Id
                  ,Trade_Month
                  ,Contract_Id
                  ,Workflow_Task_Id )
                  SELECT
                        dtch.Deal_Ticket_Client_Hier_Id
                       ,wsm.Workflow_Status_Map_Id
                       ,sdt.comment
                       ,1
                       ,NULL
                       ,GETDATE()
                       ,16
                       ,GETDATE()
                       ,NULL
                       ,sdt.period_from_date
                       ,sdt.cbms_contract_id
                       ,NULL
                  FROM
                        Trade.Deal_Tickets sdt -- cdc.fn_cdc_get_net_changes_Trade_Deal_Tickets(@Start_LSN, @End_LSN, N'all') sdt --
                        INNER JOIN Trade.Deal_Ticket dt
                              ON sdt.cbms_deal_ticket_id = dt.Deal_Ticket_Id
                        INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                              ON sdt.cbms_site_client_hier_id = dtch.Client_Hier_Id
                                 AND dt.Deal_Ticket_Id = dtch.Deal_Ticket_Id
                        INNER JOIN Trade.Workflow_Status ws
                              ON ws.Workflow_Status_Name = sdt.status
                        INNER JOIN Trade.Workflow_Status_Map wsm
                              ON ws.Workflow_Status_Id = wsm.Workflow_Status_Id
                                 AND wsm.Workflow_Id = dt.Workflow_Id
                  WHERE
                        ( @Deal_Ticket_Id IS NULL
                          OR sdt.cbms_deal_ticket_id = @Deal_Ticket_Id )
                  GROUP BY
                        dtch.Deal_Ticket_Client_Hier_Id
                       ,wsm.Workflow_Status_Map_Id
                       ,sdt.comment
                       ,sdt.period_from_date
                       ,sdt.cbms_contract_id
                       
              
 	 	 	 	 	 	
      INSERT      INTO Trade.Deal_Ticket_Client_Hier_TXN_Status
                  ( 
                   Deal_Ticket_Client_Hier_Workflow_Status_Id
                  ,Cbms_Trade_Number
                  ,Date_Data_Amended
                  ,Transaction_Status
                  ,Trade_Price_Id
                  ,Cbms_Counterparty_Id
                  ,Id
                  ,Assignee
                  ,Created_Ts
                  ,Last_Change_Ts )
                  SELECT
                        dtchws.Deal_Ticket_Client_Hier_Workflow_Status_Id
                       ,MAX(sdt.cbms_trade_number)
                       ,MAX(sdt.date_data_amended)
                       ,MAX(sdt.status)
                       ,MAX(ttp.Trade_Price_Id)
                       ,MAX(sdt.cbms_counterparty_id)
                       ,MAX(sdt.id)
                       ,MAX(sdt.assignee)
                       ,GETDATE()
                       ,GETDATE()
                  FROM
                        Trade.Deal_Tickets sdt --cdc.fn_cdc_get_net_changes_Trade_Deal_Tickets(@Start_LSN, @End_LSN, N'all') sdt --
                        INNER JOIN Trade.Deal_Ticket dt
                              ON sdt.cbms_deal_ticket_id = dt.Deal_Ticket_Id
                        INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                              ON sdt.cbms_site_client_hier_id = dtch.Client_Hier_Id
                                 AND dt.Deal_Ticket_Id = dtch.Deal_Ticket_Id
                        INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status dtchws
                              ON dtch.Deal_Ticket_Client_Hier_Id = dtchws.Deal_Ticket_Client_Hier_Id
                                 AND sdt.period_from_date = dtchws.Trade_Month
                                 AND ISNULL(sdt.cbms_contract_id, -1) = ISNULL(dtchws.Contract_Id, -1)
                        LEFT JOIN Trade.Trade_Price ttp
                              ON ttp.Deal_Tickets_Price_XId = sdt.deal_tickets_price_id
                  WHERE
                        ( @Deal_Ticket_Id IS NULL
                          OR sdt.cbms_deal_ticket_id = @Deal_Ticket_Id )
                        AND dtchws.Is_Active = 1
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          Trade.Deal_Ticket_Client_Hier_TXN_Status txnsts
                                         WHERE
                                          dtchws.Deal_Ticket_Client_Hier_Workflow_Status_Id = txnsts.Deal_Ticket_Client_Hier_Workflow_Status_Id )
                  GROUP BY
                        Deal_Ticket_Client_Hier_Workflow_Status_Id
                       --,sdt.cbms_trade_number
                       --,sdt.date_data_amended
                       --,sdt.status
                       --,ttp.Trade_Price_Id
                       --,sdt.cbms_counterparty_id
                       --,sdt.id
                       --,sdt.assignee
                       
                       
      

END


GO
GRANT EXECUTE ON  [dbo].[RM_CBMS_TXN_Deal_Ticket_Trade_Data_Load_By_DT] TO [CBMSApplication]
GO
