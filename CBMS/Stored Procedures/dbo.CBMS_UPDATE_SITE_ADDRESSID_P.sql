SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.[CBMS_UPDATE_SITE_ADDRESSID_P]


DESCRIPTION: Updates information for a site

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------------    
@PRIMARY_ADDRESS_ID     int
@site_id                int
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    

USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR		07/06/2009	modified for the sitegroup division table split and implementation of SV replication 
 DMR		  09/10/2010 Modified for Quoted_Identifier
	      	
******/
CREATE PROCEDURE dbo.CBMS_UPDATE_SITE_ADDRESSID_P
@PRIMARY_ADDRESS_ID int,
@site_id int

AS



update site SET PRIMARY_ADDRESS_ID = @PRIMARY_ADDRESS_ID  where site_id = @site_id
GO
GRANT EXECUTE ON  [dbo].[CBMS_UPDATE_SITE_ADDRESSID_P] TO [CBMSApplication]
GO
