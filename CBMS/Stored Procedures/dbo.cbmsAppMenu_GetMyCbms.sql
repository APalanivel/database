SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  procedure [dbo].[cbmsAppMenu_GetMyCbms]
	( @MyAccountId int
	, @menu_profile_id int
	, @menu_level int
	, @parent_menu_id int = null
	)
AS
BEGIN

	   select distinct m.app_menu_id
		, m.app_menu_profile_id
		, m.permission_info_id
		, m.display_text
		, m.menu_description
		, m.target_server
		, m.target_action
		, m.menu_level
		, m.display_order
		, m.parent_menu_id
		, m.app_module_id
	     from app_menu m
	     join group_info_permission_info_map gpmap on gpmap.permission_info_id = m.permission_info_id
             join user_info_group_info_map ugmap on ugmap.group_info_id = gpmap.group_info_id
	    where ugmap.user_info_id = @MyAccountId
	      and m.app_menu_profile_id = @menu_profile_id
	      and m.menu_level = @menu_level
	      and (m.parent_menu_id = @parent_menu_id or (m.parent_menu_id is null and @parent_menu_id is null))
	 order by m.display_order

END
GO
GRANT EXECUTE ON  [dbo].[cbmsAppMenu_GetMyCbms] TO [CBMSApplication]
GO
