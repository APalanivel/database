SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   procedure [dbo].[cbmsLookup_GetAll]
AS
BEGIN


	   select entity_type LookupTypeId
		, entity_description LookupTypeName
		, entity_id LookupId
		, entity_name LookupName
		, 0 SortOrder
	     from entity
	 order by entity_type
		, display_order
		, entity_name

END
GO
GRANT EXECUTE ON  [dbo].[cbmsLookup_GetAll] TO [CBMSApplication]
GO
