SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Invoice_Collection_Final_Review_Log_Status_Comments_Upd       
              
Description:              
			This sproc is used to update the status of the Final_Review.      
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
@Invoice_Collection_Activity_Id 		INT					
                           
                    
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

   Exec dbo.Invoice_Collection_Final_Review_Log_Status_Comments_Upd 114,'Test_For_Close'
   
   
   
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna               
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2019-08-20		Created For Invoice_Collection.    .         
             
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Final_Review_Log_Status_Comments_Upd]
      ( 
       @Invoice_Collection_Final_Review_Log_Id INT
      ,@Invoice_Final_Review_Comment NVARCHAR(MAX) = NULL )
AS 
BEGIN
      SET NOCOUNT ON 
      
      DECLARE @IC_Final_Review_Close_Status_Cd INT
      
      SELECT
            @IC_Final_Review_Close_Status_Cd = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON cs.Codeset_Id = c.Codeset_Id
      WHERE
            cs.Std_Column_Name = 'Invoice_Collection_Final_Review_Status_Cd'
            AND c.Code_Value = 'Close' 
      
      
      UPDATE
            dbo.Invoice_Collection_Final_Review_Log
      SET   
            Invoice_Final_Review_Comment = @Invoice_Final_Review_Comment
           ,Status_Cd = @IC_Final_Review_Close_Status_Cd
      WHERE
            Invoice_Collection_Final_Review_Log_Id = @Invoice_Collection_Final_Review_Log_Id
      

      

      UPDATE
            icq
      SET   
            Is_Locked = 1,
			icq.Last_Change_Ts = GETDATE()
      FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN dbo.Invoice_Collection_Final_Review_Log_Queue_Map icclqm
                  ON icq.Invoice_Collection_Queue_Id = icclqm.Invoice_Collection_Queue_Id
      WHERE
            icclqm.Invoice_Collection_Final_Review_Log_Id = @Invoice_Collection_Final_Review_Log_Id
	

END;

;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Final_Review_Log_Status_Comments_Upd] TO [CBMSApplication]
GO
