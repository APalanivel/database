SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********   
NAME:  dbo.Master_Variance_Test_SEL_Baseline_By_Parameter  
 
DESCRIPTION:  Used to load all the baselines by parameter

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
@Parameter_Id			int     
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:   
	Master_Variance_Test_SEL_Baseline_By_Parameter 4
------------------------------------------------------------  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
NK   Nageswara Rao Kosuri         


Initials Date  Modification  
------------------------------------------------------------  
NK	10/13/2009  Created


 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.Master_Variance_Test_SEL_Baseline_By_Parameter  
 @Parameter_Id int
 
AS  

BEGIN  
   
 SET NOCOUNT ON;  
   
 SELECT map.Variance_Baseline_Id,  
  cd.Code_Dsc  
 FROM dbo.CODE cd  
  INNER JOIN dbo.Codeset cs ON cs.Codeset_Id = cd.CodeSet_Id 
  INNER JOIN dbo.Variance_Parameter_Baseline_Map map ON map.Baseline_Cd = cd.Code_Id   
 WHERE cs.CodeSet_Name = 'VarianceBaseline'       
  AND Is_Active = 1  
  AND map.Variance_Parameter_Id = @Parameter_Id
 ORDER BY cd.Display_seq  
    
  
END
GO
GRANT EXECUTE ON  [dbo].[Master_Variance_Test_SEL_Baseline_By_Parameter] TO [CBMSApplication]
GO
