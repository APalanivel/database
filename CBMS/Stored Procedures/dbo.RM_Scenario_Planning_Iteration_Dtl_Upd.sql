SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                   
/******                        
 NAME: dbo.RM_Scenario_Planning_Iteration_Dtl_Upd            
                        
 DESCRIPTION:                        
			To UPDATE data into RM_Scenario_Planning_Iteration_Dtl table.                        
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@RM_Scenario_Report_Id INT
,@Iteration_Num INT
,@Service_Month DATE
,@Scenario_Input_Name_Cd INT
,@Scenario_Input_Value DECIMAL(32, 16)
,@Updated_User_Id INT						              
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
BEGIN TRAN 
EXEC dbo.RM_Scenario_Planning_Iteration_Dtl_Upd 
      @RM_Scenario_Planning_Iteration_ID = 1
      ,@Service_Month='2014-02-01'
     ,@Scenario_Input_Name_Cd = 100721
     ,@Scenario_Input_Value = 20
     ,@Created_User_Id = 49   
SELECT * FROM RM_Scenario_Planning_Iteration_Dtl WHERE RM_Scenario_Planning_Iteration_ID=1

ROLLBACK
        
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2014-10-01       Created                
                       
******/                
          
CREATE PROCEDURE [dbo].[RM_Scenario_Planning_Iteration_Dtl_Upd]
      ( 
       @RM_Scenario_Report_Id INT
      ,@Iteration_Num INT
      ,@Service_Month DATE
      ,@Scenario_Input_Name_Cd INT
      ,@Scenario_Input_Value DECIMAL(32, 16)
      ,@Updated_User_Id INT )
AS 
BEGIN                
      SET NOCOUNT ON;
      DECLARE @RM_Scenario_Planning_Iteration_Id INT
      SELECT
            @RM_Scenario_Planning_Iteration_Id = RM_Scenario_Planning_Iteration_Id
      FROM
            dbo.RM_Scenario_Planning_Iteration
      WHERE
            RM_Scenario_Report_Id = @RM_Scenario_Report_Id
            AND Iteration_Num = @Iteration_Num
      
      UPDATE
            dbo.RM_Scenario_Planning_Iteration_Dtl
      SET   
            Service_Month = @Service_Month
           ,[Scenario_Input_Name_Cd] = @Scenario_Input_Name_Cd
           ,Scenario_Input_Value = @Scenario_Input_Value
           ,Updated_User_Id = @Updated_User_Id
           ,Last_Change_Ts = GETDATE()
      WHERE
            RM_Scenario_Planning_Iteration_Id = @RM_Scenario_Planning_Iteration_Id
     
                  
                                                                   
                                 
                       
END 
;
GO
GRANT EXECUTE ON  [dbo].[RM_Scenario_Planning_Iteration_Dtl_Upd] TO [CBMSApplication]
GO
