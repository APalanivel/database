SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.GET_BASE_LOAD_VOLUME_P
	@contract_id INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT bd.baseload_volume,
		LPS.LOAD_PROFILE_SPECIFICATION_ID,
		bd.baseload_id,
		DATENAME(MONTH,LPS.month_identifier),
		DATENAME(YEAR,LPS.month_identifier)
	FROM dbo.baseload_details bd INNER JOIN dbo.load_profile_specification LPS ON LPS.LOAD_PROFILE_SPECIFICATION_ID = bd.LOAD_PROFILE_SPECIFICATION_ID
		INNER JOIN dbo.contract c ON c.contract_id = LPS.contract_id
	WHERE c.contract_Id=@contract_id
	ORDER BY bd.baseload_id
		, LPS.month_identifier

END
GO
GRANT EXECUTE ON  [dbo].[GET_BASE_LOAD_VOLUME_P] TO [CBMSApplication]
GO
