SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
select * from clearport_index NGX Empress Transport

select cd.index_detail_date
		, cd.index_detail_value
		, cm.clearport_index_month
		, ci.clearport_index
	     from clearport_index_detail cd  with (nolock)
    	      join clearport_index_months cm with (nolock) on cm.clearport_index_month_id = cd.clearport_index_month_id
  	      join clearport_index ci with (nolock) on ci.clearport_index_id = cm.clearport_index_id 
   	     where ci.clearport_index = 'CIG ROCKIES'


select cd.index_detail_date
		, cd.index_detail_value
		, cm.clearport_index_month
		, ci.clearport_index
	     from clearport_index_detail cd  with (nolock)
    	      join clearport_index_months cm with (nolock) on cm.clearport_index_month_id = cd.clearport_index_month_id
  	      join clearport_index ci with (nolock) on ci.clearport_index_id = cm.clearport_index_id 
   	     where ci.clearport_index = 'NGX Empress Transport'

*/
CREATE   PROCEDURE [dbo].[cbmsCPIndexDetail_UpdateForCurrencyConversion] 
	( @clearport_index varchar(200) = null
	, @index_detail_date datetime = null
	)
AS

BEGIN
	
	update clearport_index_detail 
		set index_detail_value = (select (cd.index_detail_value / 1.055056) * cuc.conversion_factor
						from clearport_index_detail cd  with (nolock)
					    	      join clearport_index_months cm with (nolock) on cm.clearport_index_month_id = cd.clearport_index_month_id
					  	      join clearport_index ci with (nolock) on ci.clearport_index_id = cm.clearport_index_id 
					   left outer join currency_unit_conversion cuc on cuc.base_unit_id = 1 and cuc.converted_unit_id = 3 and cuc.conversion_date = clearport_index_month and currency_group_id = 3
					   	     where cd.clearport_index_detail_id = cd2.clearport_indeX_detail_id
					 )
	     from clearport_index_detail cd2  with (nolock)
    	      join clearport_index_months cm2 with (nolock) on cm2.clearport_index_month_id = cd2.clearport_index_month_id
  	      join clearport_index ci2 with (nolock) on ci2.clearport_index_id = cm2.clearport_index_id 
   	     where ci2.clearport_index = @clearport_index
		and day(index_detail_date) = day(@index_detail_date)
		and month(index_detail_date) = month(@index_detail_date)
		and year(index_detail_date) = year(@index_detail_date)




END
GO
GRANT EXECUTE ON  [dbo].[cbmsCPIndexDetail_UpdateForCurrencyConversion] TO [CBMSApplication]
GO
