SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.GET_USER_INFO_LIST_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@userInfoId    	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.GET_USER_INFO_LIST_P -1,-1,49
	EXEC dbo.GET_USER_INFO_LIST_P -1,-1,0


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/28/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on
	
******/

CREATE PROCEDURE dbo.GET_USER_INFO_LIST_P
	@userId		VARCHAR(10),
	@sessionId	VARCHAR(20), 
	@userInfoId INT
AS
BEGIN
	
	SET NOCOUNT ON ;
	
	DECLARE @whereClause VARCHAR(1000)
	DECLARE @SQLStatement VARCHAR(8000)

	SELECT @whereClause = ''  
	SELECT @SQLStatement = ' SELECT USER_INFO_ID, QUEUE_ID, USERNAME, FIRST_NAME, ' + 
				' MIDDLE_NAME, LAST_NAME, ' +  
				' EMAIL_ADDRESS  FROM USER_INFO  ' 

	IF @userInfoId > 0 
		BEGIN
			SELECT @whereClause = @whereClause + ' WHERE USER_INFO_ID = ' + STR(@userInfoId)
		END

	SELECT @SQLStatement = @SQLStatement + @whereClause + ' ORDER BY FIRST_NAME ' 

	EXEC (@SQLStatement)
	
END
GO
GRANT EXECUTE ON  [dbo].[GET_USER_INFO_LIST_P] TO [CBMSApplication]
GO
