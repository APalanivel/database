SET NUMERIC_ROUNDABORT OFF 
GO

SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
 NAME:  
	cbms_prod.dbo.UBM_INSERT_CASS_EFFECTIVE_INVOICE_DATA_P
 
 DESCRIPTION:  

 INPUT PARAMETERS:
 Name						DataType		Default Description
----------------------------------------------------------------------------
 
 @ubm_batch_master_log_id	INT
 @isQuarterlyDump			BIT
 @isImage					BIT
 
 OUTPUT PARAMETERS:
 Name				DataType			Default Description
------------------------------------------------------------
 @missed_bar_code   VARCHAR(8000)				OUTPUT
 @orphan_bar_code   VARCHAR(8000)				OUTPUT

 USAGE EXAMPLES:  
------------------------------------------------------------  

   SELECT * FROM UBM WHERE UBM_ID IN (7,9)
   
   EXEC dbo.UBM_INSERT_CASS_EFFECTIVE_INVOICE_DATA_P 12,12,8791,0,1
   EXEC dbo.UBM_INSERT_CASS_EFFECTIVE_INVOICE_DATA_P 12,12,9106,0,1

 AUTHOR INITIALS:
 Initials			Name  
------------------------------------------------------------  
 PNR				Pandarinath   
 NR					Narayana Reddy
 
 MODIFICATIONS:
 Initials	Date		Modification
------------------------------------------------------------  
 PNR		09/07/2010	Added .doc,.bmp and .fax extensions in the query which used to get cbms image based on barcode.  
 PNR		01/12/2010	Included ".docx" extension as one of the ubm invoice extension in the query which used to get cbms_image_id.
 PNR		04/25/2011	MAINT-552 fixes the issues with code to return the @Missed_bar_code and @orphan_bar_code output param value
							-- Added WHERE clause condition in the select query which appends value in @missed_bar_code and @orphan_bar_code variable based on @Cbms_Image_Id value instead of checking it in CASE condition.
 HG			2014-09-25	Modified for UBM Rename Prokarma to Schneider Electric and addition of new UBM Prokarma
 HG			2014-10-15	Modified the logic for PK and SE to load the Ubm_Cass_Meter_Actuals.Determinant_Name value to Ubm_invoice_meter_details.Determinant_Name which was added part of PK rename project
 HG			2016-02-15	Modified to populate the Ubm_Sub_Bucket_Code column in UBM_INVOICE_METER_DETAILS from ubm_cass_meter_actuals table for AS400 Sub bucket enhancement
 NR			2018-11-21	D20-16 - Prokarma then it always saves NULL as currency in existing system. Instead we should use the Country name saved in UBM_CASS_VENDOR table and use that. 
******/
CREATE PROCEDURE [dbo].[UBM_INSERT_CASS_EFFECTIVE_INVOICE_DATA_P]
    (
        @missed_bar_code VARCHAR(8000) OUTPUT
        , @orphan_bar_code VARCHAR(8000) OUTPUT
        , @ubm_batch_master_log_id INT
        , @isQuarterlyDump BIT
        , @isImage BIT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @ubm_Name VARCHAR(200)
            , @util_bill_header_id VARCHAR(60)
            , @cbms_image_id INT
            , @bar_code VARCHAR(200)
            , @Prokarma_Ubm_Id INT;

        SELECT  @missed_bar_code = '', @orphan_bar_code = '';

        SELECT
            @ubm_Name = u.UBM_NAME
        FROM
            dbo.UBM_BATCH_MASTER_LOG ubm_log
            JOIN dbo.UBM u
                ON u.UBM_ID = ubm_log.UBM_ID
        WHERE
            ubm_log.UBM_BATCH_MASTER_LOG_ID = @ubm_batch_master_log_id;


        SELECT
            @Prokarma_Ubm_Id = u.UBM_ID
        FROM
            dbo.UBM u
        WHERE
            u.UBM_NAME = 'Prokarma';

        INSERT INTO dbo.UBM_INVOICE
             (
                 UBM_BATCH_MASTER_LOG_ID
                 , INVOICE_IDENTIFIER
                 , CBMS_IMAGE_ID
                 , UBM_ACCOUNT_ID
                 , UBM_ACCOUNT_NUMBER
                 , UBM_VENDOR_ID
                 , UBM_VENDOR_NAME
                 , UBM_CLIENT_NAME
                 , BILL_ADDRESS_LINE1
                 , BILL_ADDRESS_LINE2
                 , BILL_ADDRESS_LINE3
                 , AMOUNT_DUE
                 , PREVIOUS_BALANCE
                 , PAYMENT_DUE_DATE
                 , CITY
                 , STATE
                 , ZIP_CODE
                 , VENDOR_BILLING_DATE
                 , IS_PROCESSED
                 , IS_QUARTERLY
                 , UBM_CLIENT_ID
                 , CURRENCY
             )
        SELECT  DISTINCT
                @ubm_batch_master_log_id
                , bill.UTIL_BILL_HEADER_ID
                , NULL cbms_image_id
                , bill.ACCOUNT_ID
                , bill.REF_ACCOUNT_NO
                , bill.VENDOR_ID
                , vendor.VENDOR_NAME
                , customer.SUBCUSTOMER_NAME
                , accnt.ADDRESS_LINE1
                , accnt.ADDRESS_LINE2
                , accnt.ADDRESS_LINE3
                , bill.AMOUNT_DUE
                , bill.PREVIOUS_BALANCE
                , bill.PAYMENT_DUE_DATE
                , accnt.CITY
                , accnt.STATE_CODE
                , accnt.ZIPCODE
                , bill.VENDOR_BILLING_DATE
                , 0 is_processed
                , @isQuarterlyDump
                , customer.SUBCUSTOMER_ID
                , CASE WHEN @ubm_Name IN ( 'Schneider Electric', 'AS400' ) THEN NULL
                      WHEN @ubm_Name = 'Prokarma' THEN (CASE WHEN vendor.COUNTRY = 'USA' THEN 'USD'
                                                            WHEN vendor.COUNTRY = 'Canada' THEN 'CAD'
                                                            ELSE ISNULL(cu.CURRENCY_UNIT_NAME, vendor.COUNTRY)
                                                        END)
                      ELSE (CASE WHEN vendor.COUNTRY = 'CAN' THEN 'CAN'
                                ELSE 'USD'
                            END)
                  END AS currency
        FROM
            dbo.UBM_CASS_BILL bill
            LEFT OUTER JOIN (   SELECT
                                    ACCOUNT_ID
                                    , MAX(UBM_BATCH_MASTER_LOG_ID) ubm_batch_master_log_id
                                FROM
                                    dbo.UBM_CASS_ACCOUNT
                                GROUP BY
                                    ACCOUNT_ID) a
                ON a.ACCOUNT_ID = bill.ACCOUNT_ID
            LEFT OUTER JOIN dbo.UBM_CASS_ACCOUNT accnt
                ON accnt.ACCOUNT_ID = a.ACCOUNT_ID
                   AND  accnt.UBM_BATCH_MASTER_LOG_ID = a.ubm_batch_master_log_id
            LEFT OUTER JOIN dbo.UBM_CASS_VENDOR vendor
                ON vendor.VENDOR_ID = bill.VENDOR_ID
                   AND  vendor.UBM_BATCH_MASTER_LOG_ID = bill.UBM_BATCH_MASTER_LOG_ID
            LEFT OUTER JOIN dbo.UBM_CASS_CUSTOMER customer
                ON customer.SUBCUSTOMER_ID = accnt.SUBCUSTOMER_ID
                   AND  customer.UBM_BATCH_MASTER_LOG_ID = bill.UBM_BATCH_MASTER_LOG_ID
            LEFT OUTER JOIN(dbo.UBM_CURRENCY_MAP ucm
                            INNER JOIN dbo.CURRENCY_UNIT cu
                                ON cu.CURRENCY_UNIT_ID = ucm.CURRENCY_UNIT_ID)
                ON ucm.UBM_CURRENCY_CODE = vendor.COUNTRY
                   AND  ucm.UBM_ID = @Prokarma_Ubm_Id
        WHERE
            bill.UBM_BATCH_MASTER_LOG_ID = @ubm_batch_master_log_id;

        INSERT INTO dbo.UBM_INVOICE_DETAILS
             (
                 UBM_INVOICE_ID
                 , SERVICE_START_DATE
                 , SERVICE_END_DATE
                 , BILLING_DAYS
                 , ITEM_CODE
                 , ITEM_TYPE_DESCRIPTION
                 , ITEM_NAME
                 , ITEM_AMOUNT
                 , ITEM_QUANTITY
                 , UNIT_OF_MEASURE_CODE
                 , ESTIMATE_FLAG
             )
        SELECT
            i.UBM_INVOICE_ID
            , charges.BILL_SERVICE_PERIOD_START_DATE
            , charges.BILL_SERVICE_PERIOD_END_DATE
            , DATEDIFF(DAY, charges.BILL_SERVICE_PERIOD_START_DATE, charges.BILL_SERVICE_PERIOD_END_DATE)
            , srvc.ACCOUNT_SERVICE_NAME
            , charges.CHARGE_CATEGORY_CODE
            , charges.CHARGE_CATEGORY_NAME
            , charges.BILL_CHARGE_AMOUNT
            , charges.BILL_QUANTITY
            , charges.UOM_NAME
            , charges.ESTIMATE_FLAG
        FROM
            dbo.UBM_INVOICE i
            JOIN dbo.UBM_CASS_BILL_CHARGES charges
                ON charges.UTIL_BILL_HEADER_ID = i.INVOICE_IDENTIFIER
                   AND  charges.UBM_BATCH_MASTER_LOG_ID = i.UBM_BATCH_MASTER_LOG_ID
            LEFT OUTER JOIN (   SELECT
                                    ACCOUNT_SERVICE_ID
                                    , MAX(UBM_BATCH_MASTER_LOG_ID) ubm_batch_master_log_id
                                FROM
                                    dbo.UBM_CASS_SERVICE
                                GROUP BY
                                    ACCOUNT_SERVICE_ID) s
                ON s.ACCOUNT_SERVICE_ID = charges.ACCOUNT_SERVICE_ID
            LEFT OUTER JOIN dbo.UBM_CASS_SERVICE srvc
                ON srvc.ACCOUNT_SERVICE_ID = charges.ACCOUNT_SERVICE_ID
                   AND  srvc.UBM_BATCH_MASTER_LOG_ID = s.ubm_batch_master_log_id
        WHERE
            i.UBM_BATCH_MASTER_LOG_ID = @ubm_batch_master_log_id;

        INSERT INTO dbo.UBM_INVOICE_METER_DETAILS
             (
                 UBM_INVOICE_ID
                 , UBM_METER_NUMBER
                 , UBM_RATE_NAME
                 , DEMAND_QUANTITY
                 , DEMAND_UNIT_OF_MEASURE_CODE
                 , USAGE_QUANTITY
                 , USAGE_UNIT_OF_MEASURE_CODE
                 , METER_LAST_READING
                 , METER_CURRENT_READING
                 , POWER_FACTOR
                 , ITEM_TYPE_DESCRIPTION
                 , ITEM_CODE
                 , DETERMINANT_NAME
                 , Ubm_Sub_Bucket_Code
             )
        SELECT
            i.UBM_INVOICE_ID
            , a.REF_METER_NO
            , a.RATE_CODE
            , a.DEMAND_QUANTITY
            , a.DEMAND_UOM
            , a.USAGE_QUANTITY
            , a.USAGE_UOM
            , a.METER_LAST_READING
            , a.METER_CURRENT_READING
            , a.POWER_FACTOR
            , item_code = CASE WHEN l.UBM_ID = 2 THEN
                                   CASE WHEN a.DEMAND_QUANTITY != 0 THEN
                                            'DEMAND-' + ISNULL(a.READING_TYPE_CODE, srvc.ACCOUNT_SERVICE_NAME)
                                       ELSE 'USAGE-' + ISNULL(a.READING_TYPE_CODE, srvc.ACCOUNT_SERVICE_NAME)
                                   END
                              ELSE a.READING_TYPE_DESC
                          END
            , srvc.ACCOUNT_SERVICE_NAME
            , CASE WHEN @ubm_Name IN ( 'Schneider Electric', 'Prokarma' ) THEN a.Determinant_Name
                  ELSE NULL
              END
            , a.Ubm_Sub_Bucket_Code -- Only the Schneider Electric and Urjanet will have this bucket populated for now
        FROM
            dbo.UBM_INVOICE i
            JOIN dbo.UBM_BATCH_MASTER_LOG l
                ON l.UBM_BATCH_MASTER_LOG_ID = i.UBM_BATCH_MASTER_LOG_ID
            JOIN dbo.UBM_CASS_METER_ACTUALS a
                ON a.UTIL_BILL_HEADER_ID = i.INVOICE_IDENTIFIER
                   AND  a.UBM_BATCH_MASTER_LOG_ID = i.UBM_BATCH_MASTER_LOG_ID
            LEFT OUTER JOIN (   SELECT
                                    ACCOUNT_SERVICE_ID
                                    , MAX(UBM_BATCH_MASTER_LOG_ID) ubm_batch_master_log_id
                                FROM
                                    dbo.UBM_CASS_SERVICE
                                GROUP BY
                                    ACCOUNT_SERVICE_ID) s
                ON s.ACCOUNT_SERVICE_ID = a.ACCOUNT_SERVICE_ID
            LEFT OUTER JOIN dbo.UBM_CASS_SERVICE srvc
                ON srvc.ACCOUNT_SERVICE_ID = a.ACCOUNT_SERVICE_ID
                   AND  srvc.UBM_BATCH_MASTER_LOG_ID = s.ubm_batch_master_log_id
        WHERE
            i.UBM_BATCH_MASTER_LOG_ID = @ubm_batch_master_log_id;

        DECLARE curInvoice CURSOR FAST_FORWARD FOR
            SELECT
                UTIL_BILL_HEADER_ID
                , BAR_CODE
            FROM
                dbo.UBM_CASS_BILL
            WHERE
                UBM_BATCH_MASTER_LOG_ID = @ubm_batch_master_log_id
            GROUP BY
                UTIL_BILL_HEADER_ID
                , BAR_CODE;

        OPEN curInvoice;
        FETCH NEXT FROM curInvoice
        INTO
            @util_bill_header_id
            , @bar_code;
        WHILE @@FETCH_STATUS = 0
            BEGIN

                IF @bar_code = '99999999999'
                    BEGIN
                        SELECT
                            @orphan_bar_code = ISNULL(@orphan_bar_code, '') + ', ' + ISNULL(@util_bill_header_id, '');
                    END;
                ELSE
                    BEGIN
                        SELECT
                            @cbms_image_id = MIN(CBMS_IMAGE_ID)
                        FROM
                            dbo.cbms_image
                        WHERE
                            CBMS_DOC_ID IN ( @bar_code + '.tif', @bar_code + '.pdf', @bar_code + '.html'
                                             , @bar_code + '.htm', @bar_code + '.jpg', @bar_code + '.tiff'
                                             , @bar_code + '.mdi', @bar_code + '.fax', @bar_code + '.doc'
                                             , @bar_code + '.bmp', @bar_code + '.docx' );

                        IF @cbms_image_id IS NOT NULL
                            BEGIN
                                UPDATE
                                    dbo.UBM_INVOICE
                                SET
                                    CBMS_IMAGE_ID = @cbms_image_id
                                WHERE
                                    UBM_BATCH_MASTER_LOG_ID = @ubm_batch_master_log_id
                                    AND INVOICE_IDENTIFIER = @util_bill_header_id;
                            END;

                        SELECT
                            @missed_bar_code = CASE WHEN @isImage = 1 THEN
                                                        ISNULL(@missed_bar_code, '') + ', ' + ISNULL(@bar_code, '')
                                               END
                        WHERE
                            @cbms_image_id IS NULL;
                    END;

                FETCH NEXT FROM curInvoice
                INTO
                    @util_bill_header_id
                    , @bar_code;
            END;

        CLOSE curInvoice;
        DEALLOCATE curInvoice;

        SELECT
            @orphan_bar_code = SUBSTRING(@orphan_bar_code, 3, LEN(@orphan_bar_code));
        SELECT
            @missed_bar_code = SUBSTRING(@missed_bar_code, 3, LEN(@missed_bar_code));

        SELECT
            @missed_bar_code = CASE WHEN @missed_bar_code = '' THEN '0'
                                   ELSE @missed_bar_code
                               END;

        SELECT
            @orphan_bar_code = CASE WHEN @orphan_bar_code = '' THEN '0'
                                   ELSE @orphan_bar_code
                               END;

        RETURN;

    END;





GO



GRANT EXECUTE ON  dbo.UBM_INSERT_CASS_EFFECTIVE_INVOICE_DATA_P TO CBMSApplication
GO