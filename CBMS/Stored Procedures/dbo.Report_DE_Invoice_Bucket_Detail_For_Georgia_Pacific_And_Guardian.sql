SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******        
  
 NAME: dbo.Report_DE_Invoice_Bucket_Detail_For_Georgia_Pacific_And_Guardian                       
  
 DESCRIPTION:        
    To get the invocie details.                         
  
 INPUT PARAMETERS:        
                           
 Name                               DataType          Default       Description        
---------------------------------------------------------------------------------------------------------------      
 @Client_Name      VARCHAR(200)  
 @Start_Dt       DATETIME   NULL  
 @End_Dt       DATETIME   NULL            
                   
 OUTPUT PARAMETERS:        
                                 
 Name                               DataType          Default       Description        
---------------------------------------------------------------------------------------------------------------      
                            
 USAGE EXAMPLES:                                
---------------------------------------------------------------------------------------------------------------   
   
 BEGIN TRAN  
  SELECT * FROM REPTMGR.dbo.Client_Report_Config  
  EXEC Report_DE_Invoice_Bucket_Detail  
    @Client_Name = 'Royal Philips|Philips Lighting|New Lumileds'  
  SELECT * FROM REPTMGR.dbo.Client_Report_Config   
 ROLLBACK TRAN        
  
 BEGIN TRAN  
  SELECT * FROM REPTMGR.dbo.Client_Report_Config  
        EXEC Report_DE_Invoice_Bucket_Detail  
            @Client_Name = 'Philips'  
            ,@Sitegroup_Name = '1'  
           ,@Start_Dt = '2014-03-01'  
           ,@End_Dt = '2014-09-01'  
  SELECT * FROM REPTMGR.dbo.Client_Report_Config   
 ROLLBACK TRAN        
  
 SELECT * FROM Client WHERE Client_Name LIKE 'Georgia%LLC%' AND Sitegroup_Id = 0  
 SELECT * FROM Client WHERE Client_Name LIKE 'Guardian%ind%' AND Sitegroup_Id = 0  
  
 AUTHOR INITIALS:      
         
 Initials                   Name        
---------------------------------------------------------------------------------------------------------------      
 HG       Harihara Suthan                             
                             
 MODIFICATIONS:      
                             
 Initials               Date            Modification      
---------------------------------------------------------------------------------------------------------------      
 HG                     2019-12-08      Created for Georgia Pacific and Guardian data export  
******/  
CREATE PROCEDURE [dbo].[Report_DE_Invoice_Bucket_Detail_For_Georgia_Pacific_And_Guardian]  
      (  
      @Client_Name    VARCHAR(MAX) = 'Georgia-Pacific LLC'  
    , @Sitegroup_Name VARCHAR(MAX) = NULL  
    , @Start_Dt       DATE         = NULL  
    , @End_Dt         DATE         = NULL 
	, @RollOverMonths  INT		   = NULL
	)  
AS  
      BEGIN  
  
            SET NOCOUNT ON;  
  
		  IF @RollOverMonths > 0
			BEGIN
				SELECT @End_Dt = CONVERT(DATE, DATEADD(mm, -1, DATEADD(dd, -DAY(GETDATE()) + 1, GETDATE())));
				SELECT @Start_Dt = DATEADD(mm, - (@RollOverMonths - 1), @End_Dt);
			END;

            CREATE TABLE #Client_Accounts  
                  ( Account_Id INT PRIMARY KEY CLUSTERED );  
  
  
            CREATE TABLE #Client_Invoice  
                  ( CU_Invoice_Id    INT PRIMARY KEY CLUSTERED  
                  , Currency_Unit_Id INT  
                  , Currency_Unit    VARCHAR(200));  
  
            CREATE TABLE #Invoice_Charges_Determinants_Dtl  
                  ( Account_Id                   INT  
                  , Service_Month                DATE  
                  , Bucket                       VARCHAR(255)  
                  , Name                         VARCHAR(200)  
                  , Value                        VARCHAR(50)  
                  , Type                         VARCHAR(50)  
                  , Code                         VARCHAR(10)  
                  , Commodity                    VARCHAR(255)  
                  , Unit                         VARCHAR(200)  
                  , Cu_Invoice_Id                INT  
                  , Commodity_Type_Id            INT  
                  , Begin_Date                   DATE  
                  , End_Date                     DATE  
                  , Determinant_Charge_Unique_Id INT  
                  , Uom_Id                       INT         NULL  
 , Min_Invoice_Service_Month    DATE );  
            CREATE NONCLUSTERED INDEX #ix_#Invoice_Charges_Determinants_Dtl  
            ON #Invoice_Charges_Determinants_Dtl (  
                  Account_Id  
                , Commodity_Type_Id );  
  
            DECLARE @Client_List TABLE  
                  ( Client_Id INT );  
  
            DECLARE @Sitegroup_List TABLE  
                  ( Sitegroup_Id INT );  
  
            DECLARE  
                  @Last_Run_Ts          DATETIME  
                , @Current_Ts           DATETIME  
                , @USD_Currency_Unit_Id INT  
                , @Currency_Group_Id    INT;  
  
  
            SELECT  
                  @USD_Currency_Unit_Id = CURRENCY_UNIT_ID  
            FROM  dbo.CURRENCY_UNIT  
            WHERE CURRENCY_UNIT_NAME = 'USD';  
  
            SELECT  
                  @Currency_Group_Id = CURRENCY_GROUP_ID  
            FROM  dbo.CLIENT  
            WHERE CLIENT_NAME = @Client_Name;  
  
            SET @End_Dt = isnull(@End_Dt, cast(dateadd(DAY, -day(getdate()) + 1, cast(getdate() AS DATE)) AS DATETIME));  
            SET @Start_Dt = isnull(@Start_Dt, dateadd(MONTH, -13, @End_Dt));  
  
  
            INSERT INTO @Client_List (  
                                           Client_Id  
                                     )  
                        SELECT  
                                    cl.CLIENT_ID  
                        FROM        dbo.ufn_split(@Client_Name, '|') cn  
                                    INNER JOIN  
                                    dbo.CLIENT cl  
                                          ON cl.CLIENT_NAME = cn.Segments  
                        GROUP BY    cl.CLIENT_ID;  
  
            INSERT INTO @Sitegroup_List (  
                                              Sitegroup_Id  
                                        )  
                        SELECT  
                              sg.Sitegroup_Id  
                        FROM  dbo.ufn_split(@Sitegroup_Name, '|') sn  
                              INNER JOIN  
                              Core.Client_Hier sg  
                                    ON sg.Sitegroup_Name = sn.Segments  
                        WHERE sg.Site_Id = 0  
                              AND   sg.Sitegroup_Id > 0  
                              AND   EXISTS  
                              (     SELECT  
                                          1  
                                    FROM  @Client_List cl  
                                    WHERE cl.Client_Id = sg.Client_Id );  
  
  
            INSERT INTO #Client_Accounts (  
                                               Account_Id  
                                         )  
                        SELECT  
                                    cha.Account_Id  
                        FROM        Core.Client_Hier ch  
                                    INNER JOIN  
                                    Core.Client_Hier_Account cha  
                                          ON cha.Client_Hier_Id = ch.Client_Hier_Id  
                        WHERE       EXISTS  
                              (     SELECT  
                                          1  
                                    FROM  @Client_List cl  
                                    WHERE cl.Client_Id = ch.Client_Id )  
                                    AND  
                                          (     @Sitegroup_Name IS NULL  
                                                OR    EXISTS  
                              (     SELECT  
                                          1  
                                    FROM  dbo.Sitegroup_Site sgs  
                                          INNER JOIN  
                                          @Sitegroup_List sl  
                                                ON sl.Sitegroup_Id = sgs.Sitegroup_id  
                                    WHERE sgs.Site_id = ch.Site_Id ))  
                                    AND   ch.Site_Not_Managed = 0  
     AND   cha.Account_Not_Managed = 0  
                        GROUP BY    cha.Account_Id;  
  
            INSERT INTO #Client_Invoice (  
                                              CU_Invoice_Id  
                                            , Currency_Unit_Id  
                                            , Currency_Unit  
                                        )  
                        SELECT  
                                    cism.CU_INVOICE_ID  
                                  , cu.CURRENCY_UNIT_ID  
                                  , isnull(cu.CURRENCY_UNIT_NAME, 'USD')  
                        FROM        #Client_Accounts ca  
                                    INNER JOIN  
                                    dbo.CU_INVOICE_SERVICE_MONTH cism  
                                          ON cism.Account_ID = ca.Account_Id  
                                    INNER JOIN  
                                    dbo.CU_INVOICE i  
                                          ON i.CU_INVOICE_ID = cism.CU_INVOICE_ID  
                                    LEFT JOIN  
                                    dbo.CURRENCY_UNIT cu  
                                          ON cu.CURRENCY_UNIT_ID = i.CURRENCY_UNIT_ID  
                        WHERE       cism.SERVICE_MONTH  
                                    BETWEEN @Start_Dt AND @End_Dt  
                                    AND   i.IS_DNT = 0  
                                    AND   i.IS_DUPLICATE = 0  
                                    AND   i.IS_REPORTED = 1  
                                    AND   i.IS_PROCESSED = 1  
                        GROUP BY    cism.CU_INVOICE_ID  
                                  , cu.CURRENCY_UNIT_ID  
                                  , isnull(cu.CURRENCY_UNIT_NAME, 'USD');  
  
            INSERT INTO #Invoice_Charges_Determinants_Dtl (  
                                                                Account_Id  
                                                              , Service_Month  
                                                              , Bucket  
                                                              , Name  
                                                              , Value  
                                                              , Type  
                                                              , Code  
                                                              , Commodity  
                                                              , Unit  
                                                              , Cu_Invoice_Id  
                                                              , Commodity_Type_Id  
                                                              , Begin_Date  
                                                              , End_Date  
                                                              , Determinant_Charge_Unique_Id  
                                                              --, Min_Invoice_Service_Month  
                                                          )  
                        SELECT  
                                    cuica.ACCOUNT_ID  
                                  , cuism.SERVICE_MONTH  
                                  , bm.Bucket_Name  
                                  , cuic.CHARGE_NAME  
                                  , ( CASE WHEN nullif(cuica.Charge_Expression, '') IS NULL  
                                                 THEN ( convert(DECIMAL(28, 10)  
                                                              , replace(replace(replace(replace(replace(replace(( CASE WHEN ( patindex('%[0-9]-', cuic.CHARGE_VALUE)) > 0  
                                                                                                                             THEN stuff(cuic.CHARGE_VALUE, patindex('%[0-9]-', cuic.CHARGE_VALUE) + 1, 1, '')  
                                                                                                                       ELSE  cuic.CHARGE_VALUE  
                 END )  
                                                                                                              , ','  
                                                                                                              , '')  
                                                                                                      , '0-'  
                                                                                                      , 0)  
                                                                                              , '+'  
                                                                                              , '')  
                                                                                      , '$'  
                                                                                      , '')  
                                                                              , ')'  
                                                                              , '')  
                                                                      , '('  
                                                                      , '')))  
                                           ELSE  cuica.Charge_Value  
                                      END )/smcount.count_Service_Month AS VALUE  
                                  , 'Charge'  
                                  , cuic.CU_DETERMINANT_CODE  
                                  , com.Commodity_Name  
                                  , cui.Currency_Unit  
                                  , cuic.CU_INVOICE_ID  
                                  , cuic.COMMODITY_TYPE_ID  
                                  , cuism.Begin_Dt  
                                  , cuism.End_Dt  
                                  , cuic.CU_INVOICE_CHARGE_ID  
                                  --, min(cuism.SERVICE_MONTH)  
                        FROM        #Client_Invoice cui  
                                    INNER JOIN  
                                    dbo.CU_INVOICE_SERVICE_MONTH cuism  
                                          ON cui.CU_Invoice_Id = cuism.CU_INVOICE_ID  
                                    INNER JOIN  
                                    dbo.CU_INVOICE_CHARGE cuic  
                                          ON cuic.CU_INVOICE_ID = cui.CU_Invoice_Id  
                                    INNER JOIN  
                                    dbo.CU_INVOICE_CHARGE_ACCOUNT cuica  
                                          ON cuica.CU_INVOICE_CHARGE_ID = cuic.CU_INVOICE_CHARGE_ID  
                                             AND cuica.ACCOUNT_ID = cuism.Account_ID  
                                    INNER JOIN  
                                    dbo.Bucket_Master bm  
                                          ON bm.Bucket_Master_Id = cuic.Bucket_Master_Id  
                                    INNER JOIN  
                                    dbo.Commodity com  
                                          ON com.Commodity_Id = cuic.COMMODITY_TYPE_ID  
                                    LEFT OUTER JOIN  
                                          (     SELECT  
                                                            CU_INVOICE_ID,count(DISTINCT SERVICE_MONTH) count_Service_Month  
                                                FROM        dbo.CU_INVOICE_SERVICE_MONTH  
                                                GROUP BY    CU_INVOICE_ID ) smcount  
              ON cui.CU_Invoice_Id = smcount.Cu_Invoice_Id  
                        WHERE       cuism.SERVICE_MONTH  
                        BETWEEN     @Start_Dt AND @End_Dt  
                        --GROUP BY    cuica.ACCOUNT_ID  
                        --          , bm.Bucket_Name  
                        --          , cuic.CHARGE_NAME  
                        --          , cuic.CHARGE_VALUE  
                        --          , cuic.CU_DETERMINANT_CODE  
                        --          , com.Commodity_Name  
              --          , cui.Currency_Unit  
                        --          , cuic.CU_INVOICE_ID  
                        --          , cuic.COMMODITY_TYPE_ID  
                        --          , cuism.Begin_Dt  
                        --          , cuism.End_Dt  
                        --          , CASE WHEN smcount.Cu_Invoice_Id IS NOT NULL  
                        --                       THEN left(sm.Service_Month, len(sm.Service_Month) - 1)  
                        --                 ELSE  convert(VARCHAR(10), cuism.SERVICE_MONTH, 101)   
                        --            END  
                        --          , CASE WHEN nullif(cuica.Charge_Expression, '') IS NULL  
                        --                       THEN ( convert(DECIMAL(28, 10)  
                        --                                    , replace(replace(replace(replace(replace(replace(( CASE WHEN ( patindex('%[0-9]-', cuic.CHARGE_VALUE)) > 0  
                        --                                                                                                   THEN stuff(cuic.CHARGE_VALUE, patindex('%[0-9]-', cuic.CHARGE_VALUE) + 1, 1, '')  
                        --                                                                                             ELSE  cuic.CHARGE_VALUE  
                        --                                                                                        END )  
                        --                                                                                    , ','  
                        --                                                                                    , '')  
                        --                                                                            , '0-'  
                        --                                                                            , 0)  
                        --                                                                    , '+'  
                        --                                                                    , '')  
                        --                                                            , '$'  
                        --                                                            , '')  
                        --                                                    , ')'  
                        --                                                    , '')  
                        --                                            , '('  
                        --                                            , '')))  
                        --                 ELSE  cuica.Charge_Value  
                        --            END  
                        --          , cuic.CU_INVOICE_CHARGE_ID;  
  
            INSERT INTO #Invoice_Charges_Determinants_Dtl (  
                                                                Account_Id  
                                                              , Service_Month  
                 , Bucket  
                                                              , Name  
                                                              , Value  
                                                              , Type  
                                                              , Code  
                                                              , Commodity  
                                                              , Unit  
                                                              , Cu_Invoice_Id  
                                                              , Commodity_Type_Id  
                                                              , Begin_Date  
                                                              , End_Date  
                                                              , Determinant_Charge_Unique_Id  
                                                              , Uom_Id  
                                                              --, Min_Invoice_Service_Month  
                                                          )  
                        SELECT  
                                    cuica.ACCOUNT_ID  
                                  , cuism.SERVICE_MONTH  
                                  , bm.Bucket_Name AS Bucket  
                                  , cuic.DETERMINANT_NAME Name  
                                  , ( CASE WHEN nullif(cuica.Determinant_Expression, '') IS NULL  
                                                 THEN ( convert(DECIMAL(28, 10)  
                                                              , replace(replace(replace(replace(replace(replace(( CASE WHEN ( patindex('%[0-9]-', cuic.DETERMINANT_VALUE)) > 0  
                                                                                                                             THEN stuff(cuic.DETERMINANT_VALUE, patindex('%[0-9]-', cuic.DETERMINANT_VALUE) + 1, 1, '')  
                                                                                                                       ELSE  cuic.DETERMINANT_VALUE  
                                                                                                                  END )  
                                                                                                              , ','  
                                                                                                              , '')  
                                                                                                      , '0-'  
                                                                                                      , 0)  
                                                                                              , '+'  
                                                                                              , '')  
                                                                                      , '$'  
                                                                                      , '')  
                                                                              , ')'  
                                                                              , '')  
                                                                      , '('  
                                                                      , '')))  
                                           ELSE  cuica.Determinant_Value  
                                      END )/smcount.count_Service_month AS VALUE  
                                  , 'Determinant' AS Type  
                                  , cuic.CU_DETERMINANT_CODE Code  
          , com.Commodity_Name Commodity  
                                  , e.ENTITY_NAME Unit  
                                  , cuic.CU_INVOICE_ID  
                                  , cuic.COMMODITY_TYPE_ID  
                                  , cuism.Begin_Dt  
                                  , cuism.End_Dt  
                                  , cuic.CU_INVOICE_DETERMINANT_ID  
                                  , cuic.UNIT_OF_MEASURE_TYPE_ID  
                                  --, min(cuism.SERVICE_MONTH)  
                        FROM        #Client_Invoice cui  
                                    INNER JOIN  
                                    dbo.CU_INVOICE_SERVICE_MONTH cuism  
                                          ON cui.CU_Invoice_Id = cuism.CU_INVOICE_ID  
                                    LEFT OUTER JOIN  
                                          (     SELECT  
                                                            CU_INVOICE_ID,count(DISTINCT SERVICE_MONTH) count_Service_month  
                                                FROM        dbo.CU_INVOICE_SERVICE_MONTH  
                                                GROUP BY    CU_INVOICE_ID ) smcount  
                                                ON cui.CU_Invoice_Id = smcount.Cu_Invoice_Id  
                                    INNER JOIN  
                                    dbo.CU_INVOICE_DETERMINANT cuic  
                                          ON cuic.CU_INVOICE_ID = cui.CU_Invoice_Id  
                                    INNER JOIN  
                                    dbo.CU_INVOICE_DETERMINANT_ACCOUNT cuica  
                                          ON cuica.CU_INVOICE_DETERMINANT_ID = cuic.CU_INVOICE_DETERMINANT_ID  
                                             AND cuica.ACCOUNT_ID = cuism.Account_ID  
                                    INNER JOIN  
                                    dbo.Bucket_Master bm  
                                          ON bm.Bucket_Master_Id = cuic.Bucket_Master_Id  
                                    INNER JOIN  
                                    dbo.Commodity com  
                                          ON com.Commodity_Id = cuic.COMMODITY_TYPE_ID  
                                    LEFT JOIN  
                                    dbo.ENTITY e  
                                          ON e.ENTITY_ID = cuic.UNIT_OF_MEASURE_TYPE_ID  
                        WHERE       cuism.SERVICE_MONTH  
                        BETWEEN     @Start_Dt AND @End_Dt  
                       -- GROUP BY    cuica.ACCOUNT_ID  
                       --           , bm.Bucket_Name  
                       --           , cuic.DETERMINANT_NAME  
                       --           , cuic.DETERMINANT_VALUE  
                       --           , cuic.CU_DETERMINANT_CODE  
                       --           , com.Commodity_Name  
                       --           , e.ENTITY_NAME  
                       --           , cuic.CU_INVOICE_ID  
                       --           , cuic.COMMODITY_TYPE_ID  
                       --           , cuism.Begin_Dt  
                       --           , cuism.End_Dt  
                       --           , CASE WHEN smcount.Cu_Invoice_Id IS NOT NULL  
                       --                        THEN left(sm.Service_Month, len(sm.Service_Month) - 1)  
                       --                  ELSE  convert(VARCHAR(10), cuism.SERVICE_MONTH, 101)   
                       --END  
                       --           , CASE WHEN nullif(cuica.Determinant_Expression, '') IS NULL  
                       --                        THEN ( convert(DECIMAL(28, 10)  
                       --                                     , replace(replace(replace(replace(replace(replace(( CASE WHEN ( patindex('%[0-9]-', cuic.DETERMINANT_VALUE)) > 0  
                       --                                                                                                    THEN stuff(cuic.DETERMINANT_VALUE, patindex('%[0-9]-', cuic.DETERMINANT_VALUE) + 1, 1, '')  
                       --                                                                                              ELSE  cuic.DETERMINANT_VALUE  
                       --                                                                                         END )  
                       --                                                                                     , ','  
                       --                                                                                     , '')  
                       --                                                                             , '0-'  
                       --                                                                             , 0)  
                       --                                                                     , '+'  
                       --                                                                     , '')  
                       --                                                             , '$'  
                       --                                                             , '')  
                       --                                                     , ')'  
                       --                                                     , '')  
                       --                                             , '('  
                     --                                             , '')))  
                       --                  ELSE  cuica.Determinant_Value  
                       --             END  
                       --           , cuic.CU_INVOICE_DETERMINANT_ID  
                       --           , cuic.UNIT_OF_MEASURE_TYPE_ID;  
  
            SELECT  
                        ch.Client_Name Client  
                      , ch.Sitegroup_Name Division  
                      , ca.Client_Hier_Id AS SiteClientHierId  
                      , ch.Site_name Site  
                      , ch.Site_Address_Line1 [Primary Address]  
                      , ch.Site_Address_Line2 [Address 2]  
                      , ch.City City  
                      , ch.State_Name State  
                      , ch.Country_Name AS Country  
                      , ca.Account_Vendor_Name Vendor  
                      , ca.Account_Id AS AccountId  
                      , ca.Account_Number [Account number]  
                      , ca.Account_Type [Account Type]  
                      , ca.Commodity_Id AS ServiceId  
                      , icd.Commodity  
                      , icd.Cu_Invoice_Id [Invoice Id]  
                      , icd.Service_Month AS [Service Month]  
                      , CONVERT(VARCHAR(10), icd.Begin_Date, 101) [Invoice begin date]  
                      , CONVERT(VARCHAR(10), icd.End_Date, 101) [Invoice end date]  
                      , icd.Type  
                      , icd.Bucket  
                      , icd.Name  
                      , CASE WHEN icd.Type = 'Charge'  
                                   THEN icd.Unit  
                        END AS [Invoice Currency]  
                      , CASE WHEN icd.Type = 'Charge'  
                                   THEN icd.Value  
                        END AS [Value In Invoice Currency]  
                      , CASE WHEN icd.Type = 'Charge'  
                                   THEN icd.Value * cuc.CONVERSION_FACTOR  
                        END AS [Value In USD]  
                      , CASE WHEN icd.Type = 'Determinant'  
                                   THEN icd.Unit  
                        END AS [Invoice UOM]  
                      , CASE WHEN icd.Type = 'Determinant'  
                                   THEN icd.Value  
                        END AS [Value In Invoice UOM]  
            FROM        Core.Client_Hier_Account ca  
                        INNER JOIN  
                        Core.Client_Hier ch  
                              ON ch.Client_Hier_Id = ca.Client_Hier_Id  
                        INNER JOIN  
                        #Invoice_Charges_Determinants_Dtl icd  
                              ON icd.Account_Id = ca.Account_Id  
                                 AND icd.Commodity_Type_Id = ca.Commodity_Id  
                        INNER JOIN  
                        #Client_Invoice ci  
                              ON ci.CU_Invoice_Id = icd.Cu_Invoice_Id  
                        LEFT OUTER JOIN  
                        dbo.CURRENCY_UNIT_CONVERSION cuc  
                              ON cuc.BASE_UNIT_ID = ci.Currency_Unit_Id  
                                 AND cuc.CONVERTED_UNIT_ID = @USD_Currency_Unit_Id  
                                 AND cuc.CONVERSION_DATE = icd.Service_Month  
                                 AND cuc.CURRENCY_GROUP_ID = @Currency_Group_Id  
                                 AND icd.Type = 'Charge'  
                        LEFT OUTER JOIN  
                        dbo.CONSUMPTION_UNIT_CONVERSION cc  
                              ON cc.BASE_UNIT_ID = icd.Uom_Id  
                                 AND cc.CONVERTED_UNIT_ID = icd.Uom_Id  
                                 AND icd.Type = 'Determinant'  
            WHERE       EXISTS  
                  (     SELECT  
                              1  
                        FROM  @Client_List cl  
                        WHERE cl.Client_Id = ch.Client_Id )  
            GROUP BY    ch.Client_Name  
                      , ch.Sitegroup_Name  
                      , ca.Client_Hier_Id  
                      , ch.Site_name  
                      , ch.Site_Address_Line1  
                      , ch.Site_Address_Line2  
                      , ch.City  
                      , ch.State_Name  
                      , ch.Country_Name  
                      , ca.Account_Vendor_Name  
                      , ca.Account_Id  
                      , ca.Account_Number  
                      , ca.Account_Type  
                      , ca.Commodity_Id  
                      , icd.Commodity  
                      , icd.Cu_Invoice_Id  
                      , icd.Service_Month  
                      , CONVERT(VARCHAR(10), icd.Begin_Date, 101)   
                      , CONVERT(VARCHAR(10), icd.End_Date, 101)  
                      , icd.Type  
                      , icd.Bucket  
                      , icd.Name  
                      , icd.Determinant_Charge_Unique_Id  
                      , CASE WHEN icd.Type = 'Charge'  
                                   THEN icd.Unit  
                        END  
                      , CASE WHEN icd.Type = 'Charge'  
                                   THEN icd.Value  
                        END  
                      , CASE WHEN icd.Type = 'Charge'  
                                   THEN icd.Value * cuc.CONVERSION_FACTOR  
                        END  
                      , CASE WHEN icd.Type = 'Determinant'  
                                   THEN icd.Unit  
                        END  
                      , CASE WHEN icd.Type = 'Determinant'  
                                   THEN icd.Value  
                        END  
            ORDER BY    ch.Client_Name  
                      , ch.Site_name  
                      , ca.Account_Number  
                      , [Service Month];  
  
            DROP TABLE #Client_Accounts;  
            DROP TABLE #Client_Invoice;  
            DROP TABLE #Invoice_Charges_Determinants_Dtl;  
  
      END;  
  
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Invoice_Bucket_Detail_For_Georgia_Pacific_And_Guardian] TO [CBMSApplication]
GO
