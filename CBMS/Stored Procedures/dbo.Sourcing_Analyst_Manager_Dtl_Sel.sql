SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
NAME:   [dbo].[Sourcing_Analyst_Manager_Dtl_Sel]  
             
DESCRIPTION:               
   To get Sourcing Analyst and Manager details  
     
INPUT PARAMETERS:              
 Name   DataType Default  Description    
---------------------------------------------------------------------------------    
 @Country_Id  INT   NULL  
    @Commodity_Id INT   NULL  
    @StartIndex  INT   1  
    @EndIndex  INT   2147483647  
  
  
OUTPUT PARAMETERS:  
 Name   DataType  Default  Description    
---------------------------------------------------------------------------------    
  
  
 USAGE EXAMPLES:  
---------------------------------------------------------------------------------    
  
 EXEC dbo.Sourcing_Analyst_Manager_Dtl_Sel  
 EXEC dbo.Sourcing_Analyst_Manager_Dtl_Sel NULL,-1  
 EXEC dbo.Sourcing_Analyst_Manager_Dtl_Sel NULL,54  
 EXEC dbo.Sourcing_Analyst_Manager_Dtl_Sel 22442  
  
  
 AUTHOR INITIALS:              
 Initials Name              
-------------------------------------------------------------              
 RR   Raghu Reddy  
  
 MODIFICATIONS:  
 Initials Date   Modification  
------------------------------------------------------------  
 RR   2015-07-20  Global Sourcing - Created  
          
******/  
CREATE PROCEDURE [dbo].[Sourcing_Analyst_Manager_Dtl_Sel]  
      (   
       @Sourcing_Analyst_Id INT = NULL  
      ,@Sourcing_Manager_Id INT = NULL  
      ,@StartIndex INT = 1  
      ,@EndIndex INT = 2147483647 )  
AS   
BEGIN  
  
      SET NOCOUNT ON;  
      WITH  Cte_SA_SM  
              AS ( SELECT  
                        saui.USER_INFO_ID AS Sourcing_Analyst_Id  
                       ,saui.FIRST_NAME + ' ' + saui.LAST_NAME AS Sourcing_Analyst_Name  
                       ,sasm.SOURCING_MANAGER_ID AS Sourcing_Manager_Id  
                       ,smui.FIRST_NAME + ' ' + smui.LAST_NAME AS Sourcing_Manager_Name  
                       ,Row_Num = row_number() OVER ( ORDER BY smui.FIRST_NAME, saui.FIRST_NAME )  
                       ,Total = count(1) OVER ( )  
                   FROM  
                        dbo.USER_INFO saui  
                        INNER JOIN dbo.USER_INFO_GROUP_INFO_MAP uigim  
                              ON saui.USER_INFO_ID = uigim.USER_INFO_ID  
                        INNER JOIN dbo.GROUP_INFO gi  
                              ON uigim.GROUP_INFO_ID = gi.GROUP_INFO_ID  
                        LEFT JOIN dbo.SR_SA_SM_MAP sasm  
                              ON sasm.SOURCING_ANALYST_ID = saui.USER_INFO_ID  
                        LEFT JOIN dbo.USER_INFO smui  
                              ON sasm.SOURCING_MANAGER_ID = smui.USER_INFO_ID  
                   WHERE  
                        gi.GROUP_NAME = 'supply'  
                        AND saui.IS_HISTORY = 0  
                        AND ( @Sourcing_Analyst_Id IS NULL  
                              OR saui.USER_INFO_ID = @Sourcing_Analyst_Id )  
                        AND ( @Sourcing_Manager_Id IS NULL  
                              OR ( @Sourcing_Manager_Id = -1  
                                   AND NOT EXISTS ( SELECT  
                                                      1  
                                                    FROM  
                                                      dbo.SR_SA_SM_MAP sasm  
                                                    WHERE  
                                                      sasm.SOURCING_ANALYST_ID = saui.USER_INFO_ID ) )  
                              OR sasm.SOURCING_MANAGER_ID = @Sourcing_Manager_Id ))  
            SELECT  
                  css.Sourcing_Analyst_Id  
                 ,css.Sourcing_Analyst_Name  
                 ,css.Sourcing_Manager_Id  
                 ,css.Sourcing_Manager_Name  
                 ,css.Row_Num  
                 ,css.Total  
            FROM  
                  Cte_SA_SM css  
            WHERE  
                  css.Row_Num BETWEEN @StartIndex AND @EndIndex  
              
                           
END;  
;
GO
GRANT EXECUTE ON  [dbo].[Sourcing_Analyst_Manager_Dtl_Sel] TO [CBMSApplication]
GO
