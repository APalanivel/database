SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_DEAL_INFORMATION_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@entityTypeId  	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE dbo.SR_SAD_GET_DEAL_INFORMATION_DETAILS_P
@userId varchar(10),
@sessionId varchar(20),
@entityTypeId int
AS
set nocount on


select ENTITY_ID, ENTITY_NAME from ENTITY where ENTITY_TYPE = @entityTypeId   order by ENTITY_ID
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_DEAL_INFORMATION_DETAILS_P] TO [CBMSApplication]
GO
