SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******        
NAME:        
		dbo.SiteGroup_CSM_By_SiteGroup_Client_Commodity

DESCRIPTION:      
		Used to get the details for Client_Commodity_Detail table.           
	
INPUT PARAMETERS:      
Name			      DataType		Default Description      
------------------------------------------------------------      
@Client_Commodity_Id	INT,          
@SiteGroup_Id			INT			= NULL,           
@@sitegroup_type_name	VARCHAR(15)= 'GLOBAL',    
@startIndex				INT,    
@endIndex				INT    
            
OUTPUT PARAMETERS:      
Name   DataType  Default Description      
------------------------------------------------------------      

USAGE EXAMPLES:          
------------------------------------------------------------      
EXEC dbo.SiteGroup_CSM_By_SiteGroup_Client_Commodity 203028, NULL, 'SiteGroup_Id', 'ASC','Division'
   
   
AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
GB   Geetansu Behera        
CMH  Chad Hattabaugh         
HG   Hari       
SKA  Shobhit Kr Agrawal  
   
MODIFICATIONS         
Initials Date		Modification        
------------------------------------------------------------        
GB					Created      
SKA		20-Jul-2009 Modified as per coding standard
SKA		28-Jul-2009 Added TotalRows in select clause
GB		20-Aug-09	delete one line if else condition as this is returning single row, requirement  is to get all division or sitegroup
SKA		24 Aug 09	Modfied the where clause in the script
SKA		01 Sep 09   Removed Dynamic Sql Statement
					We have to use the case statement to remove the dynamic sql from code
SKA		06 Sep 09   Expand the Select statement and specified names instead of *					
SKA		09-SEP-09	Added one more condition in inner join condition (sg.Client_Id = ccd.CLIENT_ID)
  
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SiteGroup_CSM_By_SiteGroup_Client_Commodity
  @Client_Commodity_Id INT,              
  @SiteGroup_Id INT = NULL,               
  @sortColumn varchar(50)='SiteGroup_Name' ,    
  @sortOrder varchar(10)='ASC',    
  @sitegroup_type_name VARCHAR(15) = 'Global',    
  @startIndex INT =1,        
  @endIndex INT = 2147483647     
                  
AS              

BEGIN              
    
 SET NOCOUNT ON;              
       

	DECLARE @Sort VARCHAR(100)  
	
	SET @Sort = @sortColumn + @sortOrder   
	
	SELECT SITE_ID,Site_Name,Sitegroup_Id,Sitegroup_Name,CLIENT_ID,CLIENT_NAME,Is_GHG_Reported,UOM_Cd,NOT_MANAGED,Client_Commodity_Detail_Id,
	Row_Num,TotalRows
	FROM 
	(
		SELECT	'' AS SITE_ID,      
				'' AS Site_Name,      
				sg.Sitegroup_Id,      
				sg.Sitegroup_Name,      
				'' AS CLIENT_ID,        
				'' AS CLIENT_NAME,       
				ccd.Is_GHG_Reported ,       
				ccd.UOM_Cd,      
				'' AS NOT_MANAGED,       
				ccd.Client_Commodity_Detail_Id,      
				Row_Num = ROW_NUMBER() OVER (ORDER BY CASE WHEN @Sort = 'SiteGroup_NameASC' THEN sg.SiteGroup_Name END ASC, 
													  CASE WHEN @Sort = 'SiteGroup_NameDESC' THEN sg.SiteGroup_Name END DESC,
													  CASE WHEN @Sort = 'SiteGroup_IdASC' THEN sg.SiteGroup_Id END ASC,
													  CASE WHEN @Sort = 'SiteGroup_IdDESC' THEN sg.SiteGroup_Id END DESC
											),  
				TotalRows = COUNT(1) OVER()     
		FROM Core.Client_Commodity_Detail ccd      
		INNER JOIN Core.Client_Commodity cc        
			ON cc.Client_commodity_id = ccd.client_commodity_id       
		INNER JOIN CLIENT c        
			ON ccd.client_id=c.client_id       
		INNER JOIN Sitegroup sg       
			ON sg.Sitegroup_Id  = ccd.Sitegroup_id   
				AND sg.Client_Id = ccd.CLIENT_ID
		INNER JOIN CODE cd       
			ON cd.CODE_ID = sg.Sitegroup_Type_Cd      
		INNER JOIN CODESET cs      
			ON cd.CODESET_ID = cs.CODESET_ID       
		WHERE       
			cc.Client_commodity_id = @Client_Commodity_Id 
			AND cd.Code_Value = @sitegroup_type_name  
			AND cs.STD_COLUMN_NAME = 'Sitegroup_type_cd'
			AND ((@SiteGroup_Id IS NULL) OR (ccd.sitegroup_id = @sitegroup_id))
	) x	WHERE x.Row_Num BETWEEN @startIndex AND @endIndex 	

	
	 
 END
GO
GRANT EXECUTE ON  [dbo].[SiteGroup_CSM_By_SiteGroup_Client_Commodity] TO [CBMSApplication]
GO
