SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
NAME:  
	dbo.SiteGroup_Sel_By_Client_Id  

DESCRIPTION:    
	Procedure used to fetch the Sitegroup details for the given client id .  
	
INPUT PARAMETERS:  
Name				DataType	Default Description  
------------------------------------------------------------  
@Client_Id			INT  ,   
@Code_Value			VARCHAR(25)
@STD_Column_Name	VARCHAR(25)	'Sitegroup_type_cd'

OUTPUT PARAMETERS:  
Name				DataType	Default Description  
------------------------------------------------------------  

USAGE EXAMPLES:  
------------------------------------------------------------  
EXEC dbo.SiteGroup_Sel_By_Client_Id 10027, NULL,'Division'

Test AT&T
EXEC dbo.SiteGroup_Sel_By_Client_Id 11552, NULL,NULL,1,100


AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
HG		Harihara Suthan Ganeshan     
NA		Nitesh Asthana  
SS		Subhash Subramanyam  
MA		Mohammed Aman    

MODIFICATIONS:
Initials Date  Modification  
------------------------------------------------------------  
HG   06/12/2009  Created  
SS   06/23/2009  Added Is_SmartGroup Column  
SS   07/05/2009  Add a column Is_Locked for MA  
SS   07/16/2009  Modified as per coding Standard
SKA  09/06/2009	 Removed @STD_Column_Name variable and hard code the value in Filter clause
SKA	 12/27/2009	 Added pagination in the script
   
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[SiteGroup_Sel_By_Client_Id]
      (
       @Client_Id INT
      ,@SiteGroupId INT = NULL
      ,@Sitegroup_Type_Name VARCHAR(25) = NULL
      ,@StartIndex INT = 1
      ,@endIndex INT = 2147483647 )
AS 

BEGIN

      SET NOCOUNT ON ;

      WITH  Cte_SiteGroup_Sel_By_Client_Id
              AS ( SELECT
                        sg.SiteGroup_Id
                       ,sg.SiteGroup_Name
                       ,sg.Is_Smart_Group
                       ,GroupType = ( CASE WHEN sg.Is_Smart_Group = 0 THEN 'Manual Group'
                                           ELSE 'Smart Group'
                                      END )
                       ,sg.Is_Locked
                       ,SiteGroup_Type_Name = cd.Code_Value
                       ,Total = COUNT(1) OVER ( )
                                               ,Row_Num = ROW_NUMBER() OVER ( ORDER BY sg.SiteGroup_Name )
                   FROM
                                                dbo.SiteGroup sg
                                                INNER JOIN dbo.CODE cd
                                                      ON cd.CODE_ID = sg.Sitegroup_Type_Cd
                                                INNER JOIN dbo.CODESET cs
                                                      ON cs.CODESET_ID = cd.CODESET_ID
                   WHERE
                                                sg.Client_Id = @Client_Id
                                                AND ( @Sitegroup_Type_Name IS NULL
                                                      OR cd.CODE_VALUE = @Sitegroup_Type_Name )
                                                AND ( @SiteGroupId IS NULL
                                                      OR sg.Sitegroup_Id = @SiteGroupId )
                                                AND ( sg.Is_Complete = 1
                                                      OR sg.Is_Edited = 1 ))
            SELECT
                  SiteGroup_Id
                 ,SiteGroup_Name
                 ,Is_Smart_Group
                 ,GroupType
                 ,Is_Locked
                 ,SiteGroup_Type_Name
                 ,Total
            FROM
                  Cte_SiteGroup_Sel_By_Client_Id
            WHERE
                  Row_Num BETWEEN @StartIndex AND @endIndex

END
GO
GRANT EXECUTE ON  [dbo].[SiteGroup_Sel_By_Client_Id] TO [CBMSApplication]
GO
