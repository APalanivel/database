SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        dbo.RM_Sites_Client_Contact_Dtls_Sel_By_Contract                      
                        
Description:                        
        To get site's hedge configurations  
                        
Input Parameters:                        
    Name    DataType        Default     Description                          
--------------------------------------------------------------------------------  
 @Client_Id   INT  
    @Commodity_Id  INT  
    @Hedge_Type  INT  
    @Start_Dt DATE  
    @End_Dt  DATE  
    @Contract_Id  VARCHAR(MAX)  
    @Site_Id   INT    NULL  
                        
 Output Parameters:                              
 Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
     
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
 EXEC dbo.RM_Client_Hier_Hedge_Config_Sel 11236  
  
 EXEC dbo.Contract_Sel_By_Commodity_Hedge_Type_Period @Client_Id = 11236,@Commodity_Id = 291  
   ,@Start_Dt='2015-01-01',@End_Dt='2017-12-30',@Hedge_Type=586  
    
 EXEC dbo.RM_Sites_Client_Contact_Dtls_Sel_By_Contract  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2015-01-01',@End_Dt='2017-12-30',@Hedge_Type=586,@Contract_Id = 162277  
 EXEC dbo.RM_Sites_Client_Contact_Dtls_Sel_By_Contract  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2015-01-01',@End_Dt='2017-12-30',@Hedge_Type=586,@Site_Str = '624280,41241'  
   
 EXEC dbo.RM_Sites_Client_Contact_Dtls_Sel_By_Contract  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2015-01-01',@End_Dt='2017-12-30',@Hedge_Type=586,@Contract_Id = 162278  
 EXEC dbo.RM_Sites_Client_Contact_Dtls_Sel_By_Contract  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2015-01-01',@End_Dt='2017-12-30',@Hedge_Type=586,@Site_Str = '41270'  
  
 SELECT ch.* FROM Core.Client_Hier_Account cha INNER JOIN Core.Client_Hier ch ON cha.Client_Hier_Id = ch.Client_Hier_Id  
  WHERE cha.Supplier_Contract_ID = 162277  
 SELECT ch.* FROM Core.Client_Hier_Account cha INNER JOIN Core.Client_Hier ch ON cha.Client_Hier_Id = ch.Client_Hier_Id  
  WHERE cha.Supplier_Contract_ID = 162278  
    
 EXEC dbo.RM_Sites_Client_Contact_Dtls_Sel_By_Contract  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2019-01-01',@End_Dt='2019-01-01',@Hedge_Type=586,@Contract_Id = '148361'  
 EXEC dbo.RM_Sites_Client_Contact_Dtls_Sel_By_Contract  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2019-01-01',@End_Dt='2019-01-01',@Hedge_Type=586,@Site_Str = '24401,24415'  
 EXEC dbo.RM_Sites_Client_Contact_Dtls_Sel_By_Contract  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2019-01-01',@End_Dt='2019-01-01',@Hedge_Type=586,@Site_Str = '24401,24415',@RM_Group_Id='730',@Division_Id='1928'  
  
Author Initials:                        
    Initials    Name                        
--------------------------------------------------------------------------------  
    RR          Raghu Reddy     
                         
 Modifications:                        
    Initials Date           Modification                        
--------------------------------------------------------------------------------  
 PR          02-11-2018    Created GRM  
                       
******/
CREATE PROCEDURE [dbo].[RM_Sites_Client_Contact_Dtls_Sel_By_Contract]
(
    @Client_Id INT,
    @Commodity_Id INT,
    @Hedge_Type INT,
    @Start_Dt DATE,
    @End_Dt DATE,
    @Contract_Id VARCHAR(MAX) = NULL,
    @Site_Id INT = NULL,
    @Site_Str VARCHAR(MAX) = NULL,
    @Division_Id VARCHAR(MAX) = NULL,
    @RM_Group_Id VARCHAR(MAX) = NULL
)
AS
BEGIN
    SET NOCOUNT ON;

    CREATE TABLE #Configs
    (
        Client_Id INT,
        Client_Hier_Id INT,
        Site_Id INT,
        Site_name VARCHAR(200),
        Client_Contact_Info_Id INT,
        Client_Contact VARCHAR(100),
        Site_Not_Managed BIT,
        Last_Updated_Ts DATETIME
    );

    DECLARE @Hedge_Type_Input VARCHAR(200);

    SELECT @Hedge_Type_Input = ENTITY_NAME
    FROM dbo.ENTITY
    WHERE ENTITY_ID = @Hedge_Type;

    CREATE TABLE #RM_Group_Sites
    (
        [Site_Id] INT
    );

    INSERT INTO #RM_Group_Sites
    (
        Site_Id
    )
    SELECT ch.Site_Id
    FROM dbo.Cbms_Sitegroup_Participant rgd
        INNER JOIN dbo.Code grp
            ON grp.Code_Id = rgd.Group_Participant_Type_Cd
        INNER JOIN dbo.CONTRACT c
            ON c.CONTRACT_ID = rgd.Group_Participant_Id
        INNER JOIN Core.Client_Hier_Account chasupp
            ON chasupp.Supplier_Contract_ID = c.CONTRACT_ID
        INNER JOIN Core.Client_Hier_Account chautil
            ON chasupp.Meter_Id = chautil.Meter_Id
        INNER JOIN Core.Client_Hier ch
            ON ch.Client_Hier_Id = chasupp.Client_Hier_Id
    WHERE (
              EXISTS
    (
        SELECT 1
        FROM dbo.ufn_split(@RM_Group_Id, ',') con
        WHERE rgd.Cbms_Sitegroup_Id = CAST(Segments AS INT)
    )
              AND grp.Code_Value = 'Contract'
              AND chasupp.Account_Type = 'Supplier'
              AND chautil.Account_Type = 'Utility'
          );

    INSERT INTO #RM_Group_Sites
    (
        Site_Id
    )
    SELECT ch.Site_Id
    FROM dbo.Cbms_Sitegroup_Participant rgd
        INNER JOIN dbo.Code grp
            ON grp.Code_Id = rgd.Group_Participant_Type_Cd
        INNER JOIN Core.Client_Hier ch
            ON ch.Site_Id = rgd.Group_Participant_Id
    WHERE (
              EXISTS
    (
        SELECT 1
        FROM dbo.ufn_split(@RM_Group_Id, ',') con
        WHERE rgd.Cbms_Sitegroup_Id = CAST(Segments AS INT)
    )
              AND grp.Code_Value = 'Site'
          );

    INSERT INTO #RM_Group_Sites
    (
        Site_Id
    )
    SELECT CAST(Segments AS INT)
    FROM dbo.ufn_split(@Site_Str, ',') con;

    INSERT INTO #RM_Group_Sites
    (
        Site_Id
    )
    SELECT ch.Site_Id
    FROM Core.Client_Hier ch
    WHERE ch.Site_Id > 0
          AND (EXISTS
    (
        SELECT 1
        FROM dbo.ufn_split(@Division_Id, ',') con
        WHERE ch.Sitegroup_Id = CAST(Segments AS INT)
    )
              );

    ----------------------To get site own config  
    INSERT INTO #Configs
    (
        Client_Id,
        Client_Hier_Id,
        Site_Id,
        Site_name,
        Client_Contact_Info_Id,
        Client_Contact,
        Site_Not_Managed,
        Last_Updated_Ts
    )
    SELECT ch.Client_Id,
           ch.Client_Hier_Id,
           ch.Site_Id,
           RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name,
           chhc.Contact_Info_Id AS Client_Contact_Info_Id,
           ci.First_Name + ' ' + ci.Last_Name AS Client_Contact,
           ch.Site_Not_Managed,
           chhc.Last_Updated_Ts
    FROM Core.Client_Hier ch
        INNER JOIN Trade.RM_Client_Hier_Onboard chob
            ON ch.Client_Hier_Id = chob.Client_Hier_Id
        INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
            ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
        INNER JOIN dbo.ENTITY et
            ON et.ENTITY_ID = chhc.Hedge_Type_Id
        LEFT JOIN(dbo.Contact_Info ci
        INNER JOIN dbo.Code cd
            ON ci.Contact_Type_Cd = cd.Code_Id
        INNER JOIN dbo.Codeset cs
            ON cd.Codeset_Id = cs.Codeset_Id
               AND cs.Codeset_Name = 'ContactType'
               AND cd.Code_Value = 'RM Client Contact')
            ON chhc.Contact_Info_Id = ci.Contact_Info_Id
    WHERE ch.Site_Id > 0
          AND ch.Client_Id = @Client_Id
          AND chob.Commodity_Id = @Commodity_Id
          AND
          (
              @Start_Dt
          BETWEEN chhc.Config_Start_Dt AND chhc.Config_End_Dt
              OR @End_Dt
          BETWEEN chhc.Config_Start_Dt AND chhc.Config_End_Dt
              OR chhc.Config_Start_Dt
          BETWEEN @Start_Dt AND @End_Dt
              OR chhc.Config_End_Dt
          BETWEEN @Start_Dt AND @End_Dt
          )
          AND
          (
              (
                  @Hedge_Type_Input = 'Physical'
                  AND et.ENTITY_NAME IN ( 'Physical', 'Physical & Financial' )
              )
              OR
              (
                  @Hedge_Type_Input = 'Financial'
                  AND et.ENTITY_NAME IN ( 'Financial', 'Physical & Financial' )
              )
          )
          AND
          (
              @Site_Id IS NULL
              OR ch.Site_Id = @Site_Id
          )
          AND
          (
              EXISTS
    (
        SELECT 1 FROM #RM_Group_Sites rgs WHERE rgs.Site_Id = ch.Site_Id
    )
              OR EXISTS
    (
        SELECT 1
        FROM Core.Client_Hier_Account cha
            INNER JOIN dbo.ufn_split(@Contract_Id, ',') con
                ON cha.Supplier_Contract_ID = CAST(Segments AS INT)
        WHERE cha.Client_Hier_Id = ch.Client_Hier_Id
    )
          );
    --AND ( @Site_Str IS NULL  
    --      OR EXISTS ( SELECT  
    --                        1  
    --                  FROM  
    --                        dbo.ufn_split(@Site_Str, ',')  
    --                  WHERE  
    --                        CAST(Segments AS INT) = ch.Site_Id ) )  
    --AND ( @Division_Id IS NULL  
    --      OR EXISTS ( SELECT  
    --                        1  
    --                  FROM  
    --                        dbo.ufn_split(@Division_Id, ',')  
    --                  WHERE  
    --                        CAST(Segments AS INT) = ch.Sitegroup_Id ) )  
    --AND ( @RM_Group_Id IS NULL  
    --      OR EXISTS ( SELECT  
    --                        1  
    --                  FROM  
    --                        #RM_Group_Sites rgs  
    --                  WHERE  
    --                        rgs.Site_Id = ch.Site_Id ) )  



    ----------------------To get default config                         
    INSERT INTO #Configs
    (
        Client_Id,
        Client_Hier_Id,
        Site_Id,
        Site_name,
        Client_Contact_Info_Id,
        Client_Contact,
        Site_Not_Managed,
        Last_Updated_Ts
    )
    SELECT ch.Client_Id,
           ch.Client_Hier_Id,
           ch.Site_Id,
           RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name,
           chhc.Contact_Info_Id AS Client_Contact_Info_Id,
           ci.First_Name + ' ' + ci.Last_Name AS Client_Contact,
           ch.Site_Not_Managed,
           chhc.Last_Updated_Ts
    FROM Core.Client_Hier ch
        INNER JOIN Core.Client_Hier chclient
            ON ch.Client_Id = chclient.Client_Id
        INNER JOIN Trade.RM_Client_Hier_Onboard chob
            ON chclient.Client_Hier_Id = chob.Client_Hier_Id
               AND ch.Country_Id = chob.Country_Id
        INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
            ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
        INNER JOIN dbo.ENTITY et
            ON et.ENTITY_ID = chhc.Hedge_Type_Id
        LEFT JOIN(dbo.Contact_Info ci
        INNER JOIN dbo.Code cd
            ON ci.Contact_Type_Cd = cd.Code_Id
        INNER JOIN dbo.Codeset cs
            ON cd.Codeset_Id = cs.Codeset_Id
               AND cs.Codeset_Name = 'ContactType'
               AND cd.Code_Value = 'RM Client Contact')
            ON chhc.Contact_Info_Id = ci.Contact_Info_Id
    WHERE ch.Site_Id > 0
          AND ch.Client_Id = @Client_Id
          AND chclient.Sitegroup_Id = 0
          AND chob.Commodity_Id = @Commodity_Id
          AND
          (
              @Start_Dt
          BETWEEN chhc.Config_Start_Dt AND chhc.Config_End_Dt
              OR @End_Dt
          BETWEEN chhc.Config_Start_Dt AND chhc.Config_End_Dt
              OR chhc.Config_Start_Dt
          BETWEEN @Start_Dt AND @End_Dt
              OR chhc.Config_End_Dt
          BETWEEN @Start_Dt AND @End_Dt
          )
          AND
          (
              (
                  @Hedge_Type_Input = 'Physical'
                  AND et.ENTITY_NAME IN ( 'Physical', 'Physical & Financial' )
              )
              OR
              (
                  @Hedge_Type_Input = 'Financial'
                  AND et.ENTITY_NAME IN ( 'Financial', 'Physical & Financial' )
              )
          )
          AND
          (
              @Site_Id IS NULL
              OR ch.Site_Id = @Site_Id
          )
          AND NOT EXISTS
    (
        SELECT 1
        FROM Trade.RM_Client_Hier_Onboard siteob
        WHERE siteob.Client_Hier_Id = ch.Client_Hier_Id
              AND siteob.Commodity_Id = chob.Commodity_Id
    )
          AND
          (
              EXISTS
    (
        SELECT 1 FROM #RM_Group_Sites rgs WHERE rgs.Site_Id = ch.Site_Id
    )
              OR EXISTS
    (
        SELECT 1
        FROM Core.Client_Hier_Account cha
            INNER JOIN dbo.ufn_split(@Contract_Id, ',') con
                ON cha.Supplier_Contract_ID = CAST(Segments AS INT)
        WHERE cha.Client_Hier_Id = ch.Client_Hier_Id
    )
          );
    --AND ( @Site_Str IS NULL  
    --      OR EXISTS ( SELECT  
    --                        1  
    --                  FROM  
    --                        dbo.ufn_split(@Site_Str, ',')  
    --                  WHERE  
    --                        CAST(Segments AS INT) = ch.Site_Id ) )  
    --AND ( @Division_Id IS NULL  
    --      OR EXISTS ( SELECT  
    --                        1  
    --                  FROM  
    --                        dbo.ufn_split(@Division_Id, ',')  
    --                  WHERE  
    --                        CAST(Segments AS INT) = ch.Sitegroup_Id ) )  
    --AND ( @RM_Group_Id IS NULL  
    --      OR EXISTS ( SELECT  
    --                        1  
    --                  FROM  
    --                        #RM_Group_Sites rgs  
    --                  WHERE  
    --                        rgs.Site_Id = ch.Site_Id ) )  


    SELECT c1.Client_Id,
           c1.Client_Hier_Id,
           c1.Site_Id,
           c1.Site_name,
           c1.Client_Contact_Info_Id,
           c1.Client_Contact,
           c1.Site_Not_Managed,
           c1.Last_Updated_Ts
    FROM #Configs c1
        INNER JOIN
        (
            SELECT c2.Site_Id,
                   MAX(c2.Last_Updated_Ts) AS Last_Updated_Ts
            FROM #Configs c2
            GROUP BY c2.Site_Id
        ) c3
            ON c1.Site_Id = c3.Site_Id
               AND c1.Last_Updated_Ts = c3.Last_Updated_Ts;

END;
GO
GRANT EXECUTE ON  [dbo].[RM_Sites_Client_Contact_Dtls_Sel_By_Contract] TO [CBMSApplication]
GO
