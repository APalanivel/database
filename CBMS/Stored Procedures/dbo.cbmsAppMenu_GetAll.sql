SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsAppMenu_GetAll

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@app_menu_profile_id	int       	          	
	@menu_level    	int       	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE   procedure [dbo].[cbmsAppMenu_GetAll]
	( @MyAccountId int
	, @app_menu_profile_id int
	, @menu_level int = null
	)
AS
BEGIN

	   select m.app_menu_id
		, m.app_menu_profile_id
		, m.permission_info_id
		, m.display_text
		, m.menu_description
		, m.target_server
		, m.target_action
		, m.menu_level
		, m.display_order
		, m.parent_menu_id
		, m.app_module_id
		, isNull(m2.display_text,m.display_text) parent_display_text
		, cast((case when (select count(*) from app_menu x where x.parent_menu_id = m.app_menu_id) > 0 then 1 else 0 end) as bit) as is_parent
	     from app_menu m
  left outer join app_menu m2 on m2.app_menu_id = m.parent_menu_id
	    where m.menu_level = isNull(@menu_level, m.menu_level)
	      and m.app_menu_profile_id = @app_menu_profile_id
	 order by isNull(m2.display_order, m.display_order)
		, m.menu_level
		, m.display_order
		, isNull(m2.display_text, m.display_text)

END
GO
GRANT EXECUTE ON  [dbo].[cbmsAppMenu_GetAll] TO [CBMSApplication]
GO
