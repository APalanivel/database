SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	cbms_prod.dbo.CBMS_GET_ROLLOUT_EMAIL_USER_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@user_id       	varchar(10)	          	
	@session_id    	varchar(20)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script
 DMR		  09/10/2010 Modified for Quoted_Identifier
	        	
******/
CREATE PROCEDURE dbo.CBMS_GET_ROLLOUT_EMAIL_USER_INFO_P
	@user_id varchar(10),
	@session_id varchar(20)
	AS
	
SET NOCOUNT ON
select 	first_name+' '+last_name+'<'+email_address+'>' as email_address
from 	user_info(nolock)
		
where 	user_info_id = @user_id
GO
GRANT EXECUTE ON  [dbo].[CBMS_GET_ROLLOUT_EMAIL_USER_INFO_P] TO [CBMSApplication]
GO
