SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE dbo.SR_RFP_LP_DEFAULT_SETUP_SCRIPT_P
	AS
	set nocount on
	declare @ng_commodity_type_id int, 
		@el_commodity_type_id int, 
		@vendor_id int,
		@month_selector_type_id int,
		@setup_id int,
		@ng_unit_type_id int,
		@el_unit_type_id int,
		@el_unit_type_id1 int

	
	select @ng_commodity_type_id = entity_id from entity(nolock) where entity_type = 157 and entity_name = 'Natural Gas'
	select @el_commodity_type_id = entity_id from entity(nolock) where entity_type = 157 and entity_name = 'Electric Power'
	select @month_selector_type_id = entity_id from entity(nolock) where entity_type = 1007 and entity_name = 'System Bill Month'
	select  @ng_unit_type_id = entity_id from entity(nolock) where entity_type = 102 and entity_name = 'MMBtu'
	select  @el_unit_type_id = entity_id from entity(nolock) where entity_type = 101 and entity_name = 'kWh'
	select  @el_unit_type_id1 = entity_id from entity(nolock) where entity_type = 101 and entity_name = 'kW'

	DECLARE C_LP_SCRIPT CURSOR FAST_FORWARD
	FOR
		select vendor_id from vendor where vendor_type_id = (select entity_id from entity where entity_name = 'Utility' and entity_type = 155)
	OPEN C_LP_SCRIPT
	
	FETCH NEXT FROM C_LP_SCRIPT INTO @vendor_id
	WHILE (@@fetch_status <> -1)
	BEGIN
		IF (@@fetch_status <> -2)
		BEGIN

	if (select count(1) from vendor_commodity_map 
		where vendor_id = @vendor_id 
		and commodity_type_id = @ng_commodity_type_id  and is_history = 0) > 0
	begin
		--//For Natural Gas
		if (select count(sr_load_profile_default_setup_id) from sr_load_profile_default_setup 
			where commodity_type_id = @ng_commodity_type_id and vendor_id = @vendor_id ) = 0
		begin
			insert into sr_load_profile_default_setup 
			(commodity_type_id, vendor_id, month_selector_type_id)
			values (@ng_commodity_type_id, @vendor_id, @month_selector_type_id)
			SET @setup_id = @@IDENTITY
			insert into sr_load_profile_determinant values(@setup_id, 'Total Usage', @ng_unit_type_id, 1)
		end
	end
		
	if (select count(1) from vendor_commodity_map 
		where vendor_id = @vendor_id 
		and commodity_type_id = @el_commodity_type_id  and is_history = 0) > 0
	begin

		--//For Electric Power
		if (select count(sr_load_profile_default_setup_id) from sr_load_profile_default_setup 
			where commodity_type_id = @el_commodity_type_id and vendor_id = @vendor_id ) = 0
		begin
			insert into sr_load_profile_default_setup 
			(commodity_type_id, vendor_id, month_selector_type_id)
			values (@el_commodity_type_id, @vendor_id, @month_selector_type_id)
			SET @setup_id = @@IDENTITY
			insert into sr_load_profile_determinant values(@setup_id, 'Total Usage', @el_unit_type_id, 1)
			insert into sr_load_profile_determinant values(@setup_id, 'On Peak kWh', @el_unit_type_id, 0)
			insert into sr_load_profile_determinant values(@setup_id, 'Off Peak kWh', @el_unit_type_id, 0)
			insert into sr_load_profile_determinant values(@setup_id, 'Intermediate Peak kWh', @el_unit_type_id, 0)
			insert into sr_load_profile_determinant values(@setup_id, 'Actual/On Peak kW', @el_unit_type_id1, 0)
			insert into sr_load_profile_determinant values(@setup_id, 'Contract kW', @el_unit_type_id1, 0)
			insert into sr_load_profile_determinant values(@setup_id, 'Billing kW', @el_unit_type_id1, 0)
		end
	end

		END
		FETCH NEXT FROM C_LP_SCRIPT INTO @vendor_id
	END
	CLOSE C_LP_SCRIPT
	DEALLOCATE C_LP_SCRIPT
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_LP_DEFAULT_SETUP_SCRIPT_P] TO [CBMSApplication]
GO
