SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/********
NAME : [dbo].[Variance_Test_Event_Audit_Queue_ErrorMessage_Processing]  
       
DESCRIPTION: 
				This Procedure is used to Re-process the error message in Variance_Test_Event_Audit_Queue    
   
 INPUT PARAMETERS:        
 Name			DataType               Default			Description        
-------------------------------------------------------------------------------------------------------  
  

 OUTPUT PARAMETERS:        
 Name			DataType				Default			Description        
-------------------------------------------------------------------------------------------------------    
  

 USAGE EXAMPLES:        
-------------------------------------------------------------------------------------------------------  
EXEC [dbo].[Variance_Test_Event_Audit_Queue_ErrorMessage_Processing]  
  
AUTHOR INITIALS:        
 Initials   Name        
-------------------------------------------------------------------------------------------------------  
 RR			Raghu Reddy
 
 MODIFICATIONS
 Initials   Date		Modification
-------------------------------------------------------------------------------------------------------
 RR			2014-09-17	Created MAINT-3040 Moving non-process dependent auditing to queued update to help performance

*******/
CREATE PROCEDURE [dbo].[Variance_Test_Event_Audit_Queue_ErrorMessage_Processing]
AS 
BEGIN

      SET NOCOUNT ON;

      CREATE TABLE #Variance_Test_Event_Audit_Queue_ErrorMessage
            ( 
             Message_Id INT IDENTITY(1, 1)
            ,Conversation_Group_Id UNIQUEIDENTIFIER
            ,[Conversation_Handle] UNIQUEIDENTIFIER
            ,MESSAGE_Body XML
            ,Message_Sequence_Number INT
            ,Message_Type VARCHAR(255)
            ,Message_Received_Ts DATETIME )

      DECLARE
            @id1 INT = 1
           ,@id2 INT = 1
           ,@Conversation_Group_Id UNIQUEIDENTIFIER
           ,@Conversation_Handle UNIQUEIDENTIFIER
           ,@Old_Conversation_Handle UNIQUEIDENTIFIER
           ,@Message_Body XML
           ,@Message_Type VARCHAR(255)
           ,@Idoc INT
           ,@Message_Received_Ts DATETIME
           ,@Batch_Id BIGINT   
                               
      INSERT      INTO #Variance_Test_Event_Audit_Queue_ErrorMessage
                  ( 
                   Conversation_Group_Id
                  ,Conversation_Handle
                  ,MESSAGE_Body
                  ,Message_Sequence_Number
                  ,Message_Type
                  ,Message_Received_Ts )
                  SELECT
                        [Conversation_Group_Id]
                       ,[Conversation_Handle]
                       ,convert(XML, MESSAGE_Body)
                       ,Message_Sequence_Number
                       ,Message_Type
                       ,Message_Received_Ts
                  FROM
                        dbo.Service_Broker_Poison_Message
                  WHERE
                        Queue_Name = 'Variance_Test_Event_Audit_Queue'
                        AND MESSAGE_Body IS NOT NULL
                  ORDER BY
                        Message_Received_Ts
                       ,Conversation_Group_Id
                       ,Conversation_Handle
                       ,Message_Sequence_Number 

      SELECT
            @id2 = @@ROWCOUNT    
               
      WHILE ( @id2 >= @id1 ) 
            BEGIN  

                  BEGIN TRY   

                        BEGIN TRANSACTION  

                        SELECT
                              @Conversation_Group_Id = Conversation_Group_Id
                             ,@Old_Conversation_Handle = Conversation_Handle
                             ,@Message_Type = Message_Type
                             ,@Message_Body = MESSAGE_Body
                             ,@Message_Received_Ts = Message_Received_Ts
                        FROM
                              #Variance_Test_Event_Audit_Queue_ErrorMessage
                        WHERE
                              Message_Id = @id1  
       
        
                        IF EXISTS ( SELECT
                                          1
                                    FROM
                                          sys.conversation_endpoints
                                    WHERE
                                          conversation_handle = @Old_Conversation_Handle
                                          AND state != 'CD' )  
								--end old conversation  
                              END CONVERSATION @Old_Conversation_Handle--Error Msg Conversation

                        SET @Conversation_Handle = NULL;
                        BEGIN DIALOG CONVERSATION @Conversation_Handle -- New Conversation
						FROM SERVICE [//Change_Control/Service/CBMS/Variance_Test_Event_Audit]
						TO SERVICE '//Change_Control/Service/CBMS/Variance_Test_Event_Audit'
						ON CONTRACT [//Change_Control/Contract/Variance_Test_Event_Audit];

                        IF ( @Message_Type = '//Change_Control/Message/Variance_Test_Event_Audit' ) 
                              BEGIN
                                    SEND ON CONVERSATION @Conversation_Handle MESSAGE TYPE [//Change_Control/Message/Variance_Test_Event_Audit] (@Message_Body);
                                    SET @id1 = @id1 + 1
                              END  
                               
                        END CONVERSATION @Conversation_Handle  
                       
                       -- delete error message from poison table  
                        DELETE FROM
                              dbo.Service_Broker_Poison_Message
                        WHERE
                              Queue_Name = 'Variance_Test_Event_Audit_Queue'
                              AND Conversation_Group_Id = @Conversation_Group_Id
                              AND Conversation_Handle = @Old_Conversation_Handle  

                        COMMIT TRANSACTION  

                  END TRY  
                  BEGIN CATCH  

                        DECLARE @errormessage NVARCHAR(4000) = error_message()  

                        IF @@TRANCOUNT > 1 
                              ROLLBACK TRANSACTION  

                        RAISERROR(60001, 16, 1, 'SP:Variance_Test_Event_Audit_Queue_ErrorMessage_Processing', @errormessage)  

                  END CATCH  

            END  

END;
GO
