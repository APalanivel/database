SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.UPDATE_LPS_P
	@priceIndexID INT,
	@lpsFixedPrice DECIMAL(32,16),
	@lpsUnitTypeID INT,
	@lpsFreqTypeID INT,
	@lpSpecificationID INT
AS
BEGIN

	SET NOCOUNT ON

	UPDATE dbo.LOAD_PROFILE_SPECIFICATION
	SET PRICE_INDEX_ID = @priceIndexID,
		LPS_FIXED_PRICE = @lpsFixedPrice, 
		LPS_UNIT_TYPE_ID = @lpsUnitTypeID,
		LPS_FREQUENCY_TYPE_ID = @lpsFreqTypeID 
	WHERE load_profile_specification_id = @lpSpecificationID

END
GO
GRANT EXECUTE ON  [dbo].[UPDATE_LPS_P] TO [CBMSApplication]
GO
