SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
                           
 NAME:  dbo.Account_Exception_Cu_Invoice_Map_Sel_By_Cu_Invoice_Id                 
                            
 DESCRIPTION:        
		load the filers by default when we click on SDE link in Invoice tracking page .                            
                            
 INPUT PARAMETERS:        
                           
 Name                            DataType        Default       Description        
-----------------------------------------------------------------------------------
 @Cu_Invoice_Id					  INT          
                         
 OUTPUT PARAMETERS:        
                                 
 Name                            DataType        Default       Description        
-----------------------------------------------------------------------------------
                            
 USAGE EXAMPLES:                                
-----------------------------------------------------------------------------------

Exec dbo.Account_Exception_Cu_Invoice_Map_Sel_By_Cu_Invoice_Id 75254547    
     
Exec dbo.Account_Exception_Cu_Invoice_Map_Sel_By_Cu_Invoice_Id 69045505    
                           
 AUTHOR INITIALS:      
         
 Initials               Name        
-----------------------------------------------------------------------------------
 NR                     Narayana Reddy                              
                             
 MODIFICATIONS:      
                             
 Initials               Date            Modification      
-----------------------------------------------------------------------------------
 NR                     2014-01-01      Created for MAINT-9733 -Add Contract.                          
                           
******/

CREATE PROCEDURE [dbo].[Account_Exception_Cu_Invoice_Map_Sel_By_Cu_Invoice_Id]
    (
        @Cu_Invoice_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @New_Exception_Status_Cd INT
            , @In_Progress_Exception_Status_Cd INT;


        SELECT
            @New_Exception_Status_Cd = MAX(CASE WHEN c.Code_Value = 'New' THEN c.Code_Id
                                           END)
            , @In_Progress_Exception_Status_Cd = MAX(CASE WHEN c.Code_Value = 'In Progress' THEN c.Code_Id
                                                     END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Status'
            AND cs.Std_Column_Name = 'Exception_Status_Cd'
            AND c.Code_Value IN ( 'New', 'In Progress' );



        SELECT
            ae.Queue_Id
            , ae.Exception_Type_Cd
            , aeci.Cu_Invoice_Id
            , c.Code_Value AS Exception_Type
        FROM
            dbo.Account_Exception ae
            INNER JOIN dbo.Code c
                ON ae.Exception_Type_Cd = c.Code_Id
            INNER JOIN dbo.Account_Exception_Cu_Invoice_Map aeci
                ON ae.Account_Exception_Id = aeci.Account_Exception_Id
        WHERE
            aeci.Cu_Invoice_Id = @Cu_Invoice_Id
            AND aeci.Status_Cd IN ( @New_Exception_Status_Cd, @In_Progress_Exception_Status_Cd )
            AND ae.Exception_Status_Cd IN ( @New_Exception_Status_Cd, @In_Progress_Exception_Status_Cd )
        GROUP BY
            ae.Queue_Id
            , ae.Exception_Type_Cd
            , aeci.Cu_Invoice_Id
            , c.Code_Value
        ORDER BY
            CASE WHEN c.Code_Value = 'Missing Contract' THEN 1
                ELSE 2
            END;




    END;

GO
GRANT EXECUTE ON  [dbo].[Account_Exception_Cu_Invoice_Map_Sel_By_Cu_Invoice_Id] TO [CBMSApplication]
GO
