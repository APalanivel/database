SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
 NAME: dbo.Cu_Exception_Is_Closed_Upd_By_Invoice_Account_Commodity              
                          
 DESCRIPTION:    
 To update the IS_Closed Flag based on input.  
                          
 INPUT PARAMETERS:            
                       
 Name       DataType         Default       Description          
-------------------------------------------------------------------------------       
 @Cu_Invoice_Id                INT  
 @Account_Id       INT  
 @Commodity_Id       INT  
 @User_Info_Id       INT  
                 
                          
 OUTPUT PARAMETERS:            
                             
 Name                        DataType         Default       Description          
-------------------------------------------------------------------------------       
                          
 USAGE EXAMPLES:                              
-------------------------------------------------------------------------------    
BEGIN TRAN     
 SELECT  * FROM dbo.CU_EXCEPTION ce WHERE CU_INVOICE_ID = 1  
  
         EXEC dbo.Cu_Exception_Is_Closed_Upd_By_Invoice_Account_Commodity   
            @Cu_Invoice_Id = 1  
           ,@Account_Id = 1  
           ,@Commodity_Id = 1  
           ,@User_Info_Id = 1  
             
  SELECT  * FROM dbo.CU_EXCEPTION ce WHERE CU_INVOICE_ID = 1  
    
  ROLLBACK TRAN       
           
             
                         
 AUTHOR INITIALS:          
         
 Initials              Name          
-------------------------------------------------------------------------------       
 NR                    Narayana Reddy            
                           
 MODIFICATIONS:        
            
 Initials              Date             Modification        
-------------------------------------------------------------------------------       
 NR                    2017-11-03        Created for Nov - 2017 Hotfix.  
                         
******/
CREATE PROCEDURE [dbo].[Cu_Exception_Is_Closed_Upd_By_Invoice_Account_Commodity]
     (
         @Cu_Invoice_Id INT
         , @Account_Id INT
         , @Commodity_Id INT
         , @User_Info_Id INT
         , @Closed_Reason_Type VARCHAR(200) = 'Invoice Corrected'
     )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Closed_Reason_Type_Id INT;

        SELECT
            @Closed_Reason_Type_Id = ENTITY_ID
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_NAME = 'Invoice is correct/differential is acceptable'
            AND ENTITY_TYPE = 102657;

        DELETE
        ced
        FROM
            dbo.CU_EXCEPTION_DENORM ced
            INNER JOIN dbo.CU_EXCEPTION ce
                ON ced.CU_INVOICE_ID = ce.CU_INVOICE_ID
            INNER JOIN dbo.CU_EXCEPTION_DETAIL cedt
                ON cedt.CU_EXCEPTION_ID = ce.CU_EXCEPTION_ID
                   AND  cedt.Account_ID = ced.Account_ID
                   AND  cedt.Commodity_Id = ced.Commodity_Id
        WHERE
            ced.CU_INVOICE_ID = @Cu_Invoice_Id
            AND ced.Account_ID = @Account_Id
            AND ced.Commodity_Id = @Commodity_Id
            AND ced.EXCEPTION_TYPE = 'Failed Recalc'
            AND cedt.IS_CLOSED = 0;

        UPDATE
            ced
        SET
            ced.IS_CLOSED = 1
            , ced.CLOSED_BY_ID = @User_Info_Id
            , ced.CLOSED_DATE = GETDATE()
            , ced.CLOSED_REASON_TYPE_ID = @Closed_Reason_Type_Id
        FROM
            dbo.CU_EXCEPTION_DETAIL ced
            INNER JOIN dbo.CU_EXCEPTION_TYPE cet
                ON ced.EXCEPTION_TYPE_ID = cet.CU_EXCEPTION_TYPE_ID
            INNER JOIN dbo.CU_EXCEPTION ce
                ON ced.CU_EXCEPTION_ID = ce.CU_EXCEPTION_ID
        WHERE
            ce.CU_INVOICE_ID = @Cu_Invoice_Id
            AND ced.Account_ID = @Account_Id
            AND ced.Commodity_Id = @Commodity_Id
            AND ced.IS_CLOSED = 0
            AND cet.EXCEPTION_TYPE = 'Failed Recalc';

        UPDATE
            ce
        SET
            ce.IS_CLOSED = 1
            , ce.CLOSED_DATE = GETDATE()
        FROM
            dbo.CU_EXCEPTION ce
        WHERE
            ce.CU_INVOICE_ID = @Cu_Invoice_Id
            AND ce.IS_CLOSED = 0
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.CU_EXCEPTION_DETAIL ced
                                    INNER JOIN dbo.CU_EXCEPTION_TYPE cet
                                        ON ced.EXCEPTION_TYPE_ID = cet.CU_EXCEPTION_TYPE_ID
                               WHERE
                                    ce.CU_EXCEPTION_ID = ced.CU_EXCEPTION_ID
                                    AND ced.IS_CLOSED = 0);


    END;
    ;
GO
GRANT EXECUTE ON  [dbo].[Cu_Exception_Is_Closed_Upd_By_Invoice_Account_Commodity] TO [CBMSApplication]
GO
