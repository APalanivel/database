SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.BUDGET_DETAILS_BULK_INSERT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@fileName      	varchar(100)	          	
	@server        	varchar(100)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath
	HG			Harihara Suthan G

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			9/23/2010	Double quotes used to annotate text replaced by Single quote to set	quoted identifier on
	HG			10/13/2010	EXEC statement was missed out while changing the script for QI added it back.
        	
******/

CREATE PROCEDURE [dbo].[BUDGET_DETAILS_BULK_INSERT_P]
	@fileName VARCHAR(100),
	@server VARCHAR(100)
AS
BEGIN

	DECLARE @bulkInsert VARCHAR(8000)

	SET @bulkInsert =
		' BULK INSERT BUDGET_DETAILS '+
		' FROM '+'''\\'+ @server +'\TEMP\' + @fileName + '''  '+ 
		' WITH( MAXERRORS=0, FIRE_TRIGGERS, CHECK_CONSTRAINTS, FORMATFILE ='+'''C:\sourcing_fmt\budget_details.fmt'')'

	EXEC (@bulkInsert)

END
GO
GRANT EXECUTE ON  [dbo].[BUDGET_DETAILS_BULK_INSERT_P] TO [CBMSApplication]
GO
