SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_SAVE_DEAL_TICKET_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@srDealTicketId	int       	          	
	@srDealTypeId  	int       	          	
	@srDealTypeOtherValue	varchar(200)	          	
	@srNewContractNumber	varchar(200)	          	
	@srDealStatusTypeId	int       	          	
	@srCounterParty	int       	          	
	@srContact     	varchar(300)	          	
	@srOfficePhone 	varchar(200)	          	
	@srCellPhone   	varchar(200)	          	
	@srActionTypeId	int       	          	
	@srActionTypeOtherValue	varchar(200)	          	
	@srVolumeUnitTypeId	int       	          	
	@srCommodityPriceTypeId	int       	          	
	@srPricingReuqestTypeId	int       	          	
	@srCurrencyUnitId	int       	          	
	@srWithWhom    	varchar(200)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE dbo.SR_SAD_SAVE_DEAL_TICKET_DETAILS_P
@userId varchar(10),
@sessionId varchar(20),
@srDealTicketId int,
@srDealTypeId int,
@srDealTypeOtherValue varchar(200),
@srNewContractNumber varchar(200),
@srDealStatusTypeId int,
@srCounterParty int,
@srContact varchar(300),
@srOfficePhone varchar(200),
@srCellPhone varchar(200),
@srActionTypeId int,
@srActionTypeOtherValue varchar(200),
@srVolumeUnitTypeId int,
@srCommodityPriceTypeId int,
@srPricingReuqestTypeId int,
@srCurrencyUnitId int,
@srWithWhom varchar(200)


AS
set nocount on
IF (select count(1) from SR_DEAL_TICKET_DETAILS where SR_DEAL_TICKET_ID = @srDealTicketId) = 0
	BEGIN

		insert into SR_DEAL_TICKET_DETAILS
			(SR_DEAL_TICKET_ID, DEAL_TYPE_ID, DEAL_TYPE_OTHER_VALUE, NEW_CONTRACT_NUMBER,
			DEAL_STATUS_TYPE_ID, COUNTER_PARTY, CONTACT, OFFICE_PHONE, CELL_PHONE, ACTION_TYPE_ID, 
			ACTION_OTHER_VALUE, VOLUME_UNIT_TYPE_ID, COMMODITY_PRICE_POINT_TYPE_ID, PRICING_REQUEST_TYPE_ID, 
			CURRENCY_UNIT_ID, WITH_WHOM)
		
		values	(@srDealTicketId ,
			@srDealTypeId ,
			@srDealTypeOtherValue,
			@srNewContractNumber,
			@srDealStatusTypeId,
			@srCounterParty,
			@srContact,
			@srOfficePhone,
			@srCellPhone,
			@srActionTypeId,
			@srActionTypeOtherValue,
			@srVolumeUnitTypeId,
			@srCommodityPriceTypeId,
			@srPricingReuqestTypeId,
			@srCurrencyUnitId,
			@srWithWhom)
	END
ELSE
	BEGIN

		update SR_DEAL_TICKET_DETAILS set DEAL_TYPE_ID=@srDealTypeId, 
			DEAL_TYPE_OTHER_VALUE=@srDealTypeOtherValue,
			NEW_CONTRACT_NUMBER=@srNewContractNumber, DEAL_STATUS_TYPE_ID=@srDealStatusTypeId,
			COUNTER_PARTY=@srCounterParty, CONTACT=@srContact, OFFICE_PHONE=@srOfficePhone,
			CELL_PHONE=@srCellPhone, ACTION_TYPE_ID=@srActionTypeId, 
			ACTION_OTHER_VALUE=@srActionTypeOtherValue,
			VOLUME_UNIT_TYPE_ID=@srVolumeUnitTypeId, COMMODITY_PRICE_POINT_TYPE_ID=@srCommodityPriceTypeId,
			PRICING_REQUEST_TYPE_ID=@srPricingReuqestTypeId, CURRENCY_UNIT_ID=@srCurrencyUnitId,
			WITH_WHOM=@srWithWhom 
		where
			SR_DEAL_TICKET_ID=@srDealTicketId

	END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_SAVE_DEAL_TICKET_DETAILS_P] TO [CBMSApplication]
GO
