SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
/******
NAME:		Change_Control_Receive_Messages_For_Cbms_Image_Transfer_Queue


DESCRIPTION:
	automaitcially activates when a record is recieved on the Cbms_Image_Transfer_Queue
	and manages the changes based on the Message Type


INPUT PARAMETERS:
	Name						DataType		Default	Description
------------------------------------------------------------


OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
 -- Procedure is only executed on Cbms_Image_Transfer_Queue

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DSC			Kaushik
	
MODIFICATIONS

	Initials	Date			Modification
------------------------------------------------------------
	DSC			10/01/2014		Created
******/
CREATE PROCEDURE [dbo].[Change_Control_Receive_Messages_For_Cbms_Image_Transfer_Queue]
AS
BEGIN 
	SET NOCOUNT ON; 

	DECLARE 
		@Param_Definition NVARCHAR(100) = N'@Message XML, @Conversation_Handle UNIQUEIDENTIFIER' -- footprint of message procedures
		,@CG_Iterator BIT = 1 -- Flag used to iterate through conversation Groups
			
	DECLARE @Message TABLE -- Table used to store failed messages for a conversation_Group
	(	Message_Sequence_Number		INT
		,Conversation_Handle		UNIQUEIDENTIFIER			
		,SERVICE_NAME				Varchar(255)
		,CONTRACT_NAME				Varchar(255)
		,Message_Type				Varchar(255)
		,Message_Body				Varbinary(max)	)

	WHILE ( @Cg_Iterator = 1 )
	BEGIN
		DECLARE @Conversation_Group_Id UNIQUEIDENTIFIER = NULL; 

		GET CONVERSATION GROUP @Conversation_Group_Id	-- get a conversation group to process
		FROM Cbms_Image_Transfer_Queue

		-- Inserting Message Details in Service_Broker_Message_Monitor table  
		INSERT      INTO Service_Broker_Message_Monitor  
				  (   
				   Queue_Name  
				  ,Message_Type  
				  ,Message_Received_Ts  
				  ,Message_Sequence_Number    
				  ,[Conversation_Group_Id]  
				  ,[Conversation_Handle]  
				  ,[Service_name]  
				  ,Contract_name  
				  ,Error_Text  
				  ,Message_Body  
				  ,Message_Status 
				  ,Message_Completed_Ts
				  ,Message_Batch_Count)  
		SELECT  
				  'Cbms_Image_Transfer_Queue' AS Queue_Name  
				  ,Message_Type_name  
				  ,getdate() AS Message_Received_Ts  
				  ,Message_Sequence_number    
				  ,[Conversation_Group_Id]  
				  ,[Conversation_Handle]  
				  ,[Service_Name]  
				  ,[Service_Contract_Name]  
				  ,'' AS [error_message]  
				  ,[Message_Body]  
				  ,'In Progress' 
				  ,NULL AS Message_Completed_Ts
				  ,NULL AS Message_Batch_Count	 
      FROM  
				Cbms_Image_Transfer_Queue WITH ( NOLOCK )  
      WHERE  
				Conversation_Group_Id = @Conversation_Group_Id  


		
		IF @Conversation_Group_Id IS NOT NULL
		BEGIN
			BEGIN TRY 
				BEGIN TRANSACTION
					DECLARE @M_Iterator INT = 1
					 
					WHILE ( @M_Iterator  = 1  )		-- Cycle through all messages in CG
					BEGIN
						DECLARE 
							@I_Conversation_Handle UNIQUEIDENTIFIER = NULL		-- Incmming message handler 
							,@I_Message_Type sysname = NULL						-- Incomming Message Type
							,@I_Message VARCHAR(max) = NULL 					-- Incomming Message
							,@Stmt NVARCHAR(2000) = NULL						-- Statement to execute
							,@Message_Type_Exists BIT = 0 						-- Flag to validate tha the message type exists
							,@I_Sequence_Number	INT								-- To update record in msg_monitor table.
						
						SAVE TRANSACTION savepoint					-- Set individual save points for each message
						
						;RECEIVE TOP (1)
							@I_Conversation_Handle = Conversation_Handle
							,@I_Message_Type = Message_Type_Name
							,@I_Message = Message_Body	
						FROM 
							Cbms_Image_Transfer_Queue
						WHERE 
							Conversation_Group_Id = @Conversation_Group_Id

						IF @I_Conversation_Handle IS NOT NULL -- Process message 
						BEGIN
							-- Get the statement to run for this message type 
							SELECT 
								@Message_Type_Exists = 1 
								,@Stmt = N'EXEC ' + PROCEDURE_NAME + ' @Message, @Conversation_Handle'
							FROM 
								Service_Broker_Message_Procedure
							WHERE
								Queue_Name = 'Cbms_Image_Transfer_Queue' 
								AND Message_Type = @I_Message_Type

							IF @Message_Type_Exists = 1  -- Execute procedure for known the message type
							BEGIN 
								EXEC sp_EXECUTESQL 
									@Statement = @Stmt
									,@params = @Param_Definition
									,@Message = @I_message
									,@Conversation_Handle = @I_Conversation_Handle

								-- Update the Processed Message to Completed status  
								UPDATE dbo.Service_Broker_Message_Monitor  
								   SET	Message_Status = 'Completed'  
									  ,	Message_Completed_Ts = getdate()
								WHERE Queue_Name = 'Cbms_Image_Transfer_Queue'  
									  AND Conversation_Group_Id = @Conversation_Group_Id  
									  AND Message_Type = @I_Message_Type  
									  AND Message_Sequence_Number = @I_Sequence_Number	


							END ELSE BEGIN 
								RAISERROR ('Unknown message Type: %s', 16, 1, @I_Message_Type)	-- if the message type is not known then thow an error
							END
						END ELSE BEGIN															-- Quit processes when no message was recieved
							SET @M_Iterator = 0 
						END
					END
				COMMIT TRANSACTION		-- Commit the successfull messages in the Conversation Group
			END TRY	
			BEGIN CATCH
				DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE()

				IF ( XACT_STATE() <> -1 )
				BEGIN
					ROLLBACK TRANSACTION savePoint -- Rollback the active transaction to the last save 
					COMMIT TRANSACTION	-- Commit any messages that executed successfully
				END 
				ELSE
				BEGIN
					ROLLBACK TRANSACTION -- Rollback complete trnasaction if it is not possible to roll back to the save point
				END
				
				-- Receive all messages and insert them into the Poison message table	
				;RECEIVE 
					Message_Sequence_Number
					,Conversation_Handle
					,SERVICE_NAME
					,Service_Contract_Name
					,Message_Type_Name
					,Message_Body
				FROM 
					Cbms_Image_Transfer_Queue
				INTO @message
				WHERE 
					Conversation_Group_Id = @Conversation_Group_Id

				INSERT Service_Broker_Poison_Message
						( Queue_Name
						,Message_Received_Ts
						,Conversation_Group_Id
						,Message_Sequence_Number
						,Conversation_Handle
						,SERVICE_NAME
						,CONTRACT_NAME
						,Message_Type
						,Error_text
						,MESSAGE_Body )
				SELECT
					'Cbms_Image_Transfer_Queue'
					,getdate()
					,@Conversation_Group_Id
					,Message_Sequence_Number
					,Conversation_Handle
					,SERVICE_NAME
					,CONTRACT_NAME
					,Message_Type
					,@ErrorMessage
					,message_Body
				FROM @Message 


	       -- Update the Processed Message to Error status  
				UPDATE dbo.Service_Broker_Message_Monitor  
				   SET Message_Status = 'Error'  
					 , Message_Completed_Ts = getdate()
				WHERE Queue_Name = 'Cbms_Image_Transfer_Queue'  
						AND Conversation_Group_Id = @Conversation_Group_Id  
						AND Message_Type IN (SELECT Message_Type FROM @message)  
						AND Message_Sequence_Number = @I_Sequence_Number

				RAISERROR(60001, 16, 1, 'Cbms_Image_Transfer_Queue', @errormessage) -- Raise Standard Error 
			END CATCH
		END ELSE BEGIN
			SET @Cg_Iterator = 0 
		END
	END
END

GO
GRANT EXECUTE ON  [dbo].[Change_Control_Receive_Messages_For_Cbms_Image_Transfer_Queue] TO [sb_Execute]
GO
