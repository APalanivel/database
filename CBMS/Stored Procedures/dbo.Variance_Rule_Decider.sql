SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                              
NAME: Budget_Exception_List_get                             
                              
DESCRIPTION: Select all  Distinct Budget Execeptions from  Budget.MileStone_Process table for     
   Used At :- Budget---> Exception Mapping Page                            
                               
                              
INPUT PARAMETERS:                              
  Name             DataType  Default Description                              
------------------------------------------------------------                              
                                
OUTPUT PARAMETERS:                              
 Name          DataType  Default Description                              
------------------------------------------------------------                              
                              
USAGE EXAMPLES:                              
------------------------------------------------------------    
                      
                              
AUTHOR INITIALS:                              
 Initials Name                              
------------------------------------------------------------                              
AP			Arunkumar Palanivel                       
MODIFICATIONS                              
                              
 Initials Date  Modification                              
------------------------------------------------------------                              
AP	Jan 9 2020 THIS PROCEDURE IS CREATED TO FIND WHETHER THE ACCOUNT CAN USE RULE BASED VARIANCE TEST(LEGACY) OR ADVANCED VARIANCE TESTING (AVT).

The current AVT applies for EP,NG,Water for Total cost, Usage Categories.                       
                                     
******/
CREATE PROCEDURE [dbo].[Variance_Rule_Decider]
      (
      @commodity_id INT )
AS
      BEGIN
            SET NOCOUNT ON;


            SELECT      DISTINCT
                        C.Code_Value
                      , variance_code.Code_Value
            FROM        dbo.Variance_Parameter_Commodity_Map VPCM
                        JOIN
                        dbo.Variance_Parameter VP
                              ON VP.Variance_Parameter_Id = VPCM.Variance_Parameter_Id
                        JOIN
                        Code C
                              ON C.Code_Id = VP.Variance_Category_Cd
                        JOIN
                        dbo.Variance_Processing_Engine VPE
                              ON VPE.Variance_Processing_Engine_Id = VPCM.variance_process_Engine_id
                        JOIN
                        Code variance_code
                              ON variance_code.Code_Id = VPE.Variance_Engine_Cd
            WHERE       VPCM.Commodity_Id = @commodity_id;
      END;
GO
GRANT EXECUTE ON  [dbo].[Variance_Rule_Decider] TO [CBMSApplication]
GO
