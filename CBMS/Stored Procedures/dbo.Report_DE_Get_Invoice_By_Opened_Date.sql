SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
 NAME:  dbo.Report_DE_Get_Invoice_By_Opened_Date         
        
 DESCRIPTION:          
             
          
 INPUT PARAMETERS:          
         
 Name                         DataType            Default        Description          
---------------------------------------------------------------------------------------------------------------        
 @Client					  Varchar(200)
 @Year                        INT  
 @Month                       INT                             
        
          
 OUTPUT PARAMETERS:                
 Name                         DataType            Default        Description          
---------------------------------------------------------------------------------------------------------------        
          
 USAGE EXAMPLES:              
---------------------------------------------------------------------------------------------------------------         
     EXEC   dbo.Report_DE_Get_Invoice_By_Opened_Date NULL,2019,1,'Open'  
      
         
 AUTHOR INITIALS:         
         
 Initials               Name          
---------------------------------------------------------------------------------------------------------------        
 AKR                    Ashok Kumar Raju
 ABK					Aditya Bharadwaj
           
 MODIFICATIONS:         
          
 Initials               Date            Modification        
---------------------------------------------------------------------------------------------------------------        
 AKR                    2014-04-21      Created for SSRS reports    
 AKR					2014-10-07		Modified the sproc for Failed Recalc for Utility Accounts
 AB						2019-03-08		Modified the sproc. Added ClientName parameter to both sproc and SSRS report (REPTMGR-106)           
******/

CREATE PROCEDURE [dbo].[Report_DE_Get_Invoice_By_Opened_Date]
    (
        @ClientName VARCHAR(200) = NULL,
        @Year       INT,
        @Month      INT,
        @Date_Type  VARCHAR(20)
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Begin_Dt DATE,
            @End_Dt   DATE;

        SELECT
            @Begin_Dt = CAST(@Month AS VARCHAR) + '/1/' + CAST(@Year AS VARCHAR),
            @End_Dt   = DATEADD(dd, -1, DATEADD(mm, 1, @Begin_Dt));

        SELECT
                cue.CU_INVOICE_ID      [Invoice ID],
                ch.Client_Name         [Client],
                cue.DATE_IN_QUEUE      [Invoice In Queue Date],
                ca.Account_Number      [Account Number],
                ch.Site_name           [Site],
                ch.City                [City],
                ch.State_Name          [State],
                ch.Country_Name        [Country],
                ca.Account_Vendor_Name [Vendor],
                u.UBM_NAME             [UBM],
                grp.ENTITY_NAME        [Issue Type],
                CASE
                    WHEN cuet.EXCEPTION_TYPE = 'Failed Recalc'
                         AND ca.Account_Type = 'Utility'
                        THEN
                        'Failed - Utility Recalc'
                    WHEN cuet.EXCEPTION_TYPE = 'Failed Recalc'
                        THEN
                        'Failed - ' + rt.Code_Value
                    ELSE
                        cuet.EXCEPTION_TYPE
                END                    AS exception_type,
                -- ,cuet.exception_type [Exception Type]  
                [Exception Status]     = CASE
                                             WHEN cued.IS_CLOSED = 1
                                                 THEN
                                                 'Closed'
                                             ELSE
                                                 stat.ENTITY_NAME
                                         END,
                --,stat.entity_name  
                cued.OPENED_DATE       [Date Exception Opened],
                cued.CLOSED_DATE       [Date Exception Closed],
                [Exception Closed By]  = ui.FIRST_NAME + ' ' + ui.LAST_NAME,
                [Closed Reason]        = reas.ENTITY_NAME,
                q.queue_name           [Queue]
        FROM
                dbo.CU_EXCEPTION             cue
            JOIN
                dbo.CU_EXCEPTION_DETAIL      cued
                    ON cued.CU_EXCEPTION_ID = cue.CU_EXCEPTION_ID
            JOIN
                dbo.CU_EXCEPTION_TYPE        cuet
                    ON cuet.CU_EXCEPTION_TYPE_ID = cued.EXCEPTION_TYPE_ID
            LEFT JOIN
                dbo.USER_INFO                ui
                    ON ui.USER_INFO_ID = cued.CLOSED_BY_ID
            LEFT JOIN
                dbo.USER_INFO_GROUP_INFO_MAP ugim
                    ON ugim.USER_INFO_ID = ui.USER_INFO_ID
            LEFT JOIN
                dbo.ENTITY                   grp
                    ON grp.ENTITY_ID = cuet.EXCEPTION_GROUP_TYPE_ID
            JOIN
                dbo.ENTITY                   stat
                    ON stat.ENTITY_ID = cued.EXCEPTION_STATUS_TYPE_ID
            LEFT JOIN
                dbo.ENTITY                   reas
                    ON reas.ENTITY_ID = cued.CLOSED_REASON_TYPE_ID
            LEFT JOIN
                dbo.CU_INVOICE_SERVICE_MONTH cuism
                    ON cuism.CU_INVOICE_ID = cue.CU_INVOICE_ID
            LEFT JOIN
                Core.Client_Hier_Account     ca
                    ON ca.Account_Id = cuism.Account_ID
            JOIN
                dbo.CU_INVOICE               cui
                    ON cui.CU_INVOICE_ID = cue.CU_INVOICE_ID
            LEFT JOIN
                Core.Client_Hier             ch
                    ON ch.Client_Hier_Id = ca.Client_Hier_Id
            LEFT JOIN
                dbo.UBM                      u
                    ON u.UBM_ID = cui.UBM_ID
            LEFT JOIN
                dbo.vwCbmsQueueName          q
                    ON q.queue_id = cue.QUEUE_ID
            LEFT OUTER JOIN
                dbo.Code                     rt
                    ON rt.Code_Id = ca.Supplier_Account_Recalc_Type_Cd
        WHERE
                (
                    ch.Client_Name = @ClientName
                    OR @ClientName IS NULL
                )
                AND
                    (
                        @Date_Type = 'Open'
                        AND cued.OPENED_DATE
                BETWEEN @Begin_Dt AND @End_Dt
                    )
                OR
                    (
                        @Date_Type = 'Close'
                        AND cued.CLOSED_DATE
                BETWEEN @Begin_Dt AND @End_Dt
                    )
        GROUP BY
                cue.CU_INVOICE_ID,
                ch.Client_Name,
                cue.DATE_IN_QUEUE,
                ca.Account_Number,
                ch.Site_name,
                ch.City,
                ch.State_Name,
                ch.Country_Name,
                ca.Account_Vendor_Name,
                u.UBM_NAME,
                grp.ENTITY_NAME,
                CASE
                    WHEN cuet.EXCEPTION_TYPE = 'Failed Recalc'
                         AND ca.Account_Type = 'Utility'
                        THEN
                        'Failed - Utility Recalc'
                    WHEN cuet.EXCEPTION_TYPE = 'Failed Recalc'
                        THEN
                        'Failed - ' + rt.Code_Value
                    ELSE
                        cuet.EXCEPTION_TYPE
                END,
                CASE
                    WHEN cued.IS_CLOSED = 1
                        THEN
                        'Closed'
                    ELSE
                        stat.ENTITY_NAME
                END,
                --,stat.entity_name  
                cued.OPENED_DATE,
                cued.CLOSED_DATE,
                ui.FIRST_NAME,
                ui.LAST_NAME,
                reas.ENTITY_NAME,
                q.queue_name
        ORDER BY
                cue.CU_INVOICE_ID,
                cued.OPENED_DATE;
    END;
    ;

GO

GRANT EXECUTE ON  [dbo].[Report_DE_Get_Invoice_By_Opened_Date] TO [CBMS_SSRS_Reports]
GO
