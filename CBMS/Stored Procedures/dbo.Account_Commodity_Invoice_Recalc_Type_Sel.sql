SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Account_Commodity_Invoice_Recalc_Type_Sel

DESCRIPTION:

INPUT PARAMETERS:
	Name						DataType		Default			Description
-----------------------------------------------------------------------------------------
    @Account_Id					INT					
    @Commodity_Id				INT

OUTPUT PARAMETERS:
	Name						DataType		Default			Description
-----------------------------------------------------------------------------------------
	
USAGE EXAMPLES:
-----------------------------------------------------------------------------------------
    
EXEC [dbo].[Account_Commodity_Invoice_Recalc_Type_Sel] @Account_Id = 1527279


EXEC [dbo].[Account_Commodity_Invoice_Recalc_Type_Sel]
    @Account_Id = 1527279
  

EXEC [dbo].[Account_Commodity_Invoice_Recalc_Type_Sel]
    @Account_Id = 1527279
    , @Contract_Id = 159815


select * from Account_Commodity_Invoice_Recalc_Type where account_id=1527279 
select * from Contract_Recalc_Config where contract_id =159815 
select * from Account_Commodity_Invoice_Recalc_Type where account_id=1527279 




AUTHOR INITIALS:
	Initials	Name
-----------------------------------------------------------------------------------------
	RKV			Ravi Kumar Vegesna
	NR			Narayana Reddy	
	
	
MODIFICATIONS
	Initials	Date			Modification
-----------------------------------------------------------------------------------------
	RKV       	2015-08-10		Created for AS400-II
	NR			2018-02-08		Interval Data Recalcs - IDR-5 - Added Determinant_Source in Result set. 
	NR			2019-08-08		Add Contract -  Added Is_Active flag.
    HG			2019-12-12		Modified to return the account level configuration as long as the start/end date overlaps with the contract start / end date.
								Right now Contract level date changes are not flowing down to account level , this is a temporary fix inorder to show the old configuration which was copied from account.
	NR			2020-01-22		MAINT-9643 -
								1)	Removed above Modification temporary fix and change back to join on dates.
									We implemneted when Contract level date changes and immediatly it will effect in account level.
								2)	SET @Contract_Id is null in sproc even app pass the Contract_Id , Because we need to show that configauration in 
									All extended contracts if any recalc is open end date.

******/
CREATE PROCEDURE [dbo].[Account_Commodity_Invoice_Recalc_Type_Sel]
    (
        @Account_Id INT
        , @Commodity_Id INT = NULL
        , @Contract_Id INT = NULL
        , @Is_Consolidated_Contract_Config BIT = 0
    )
AS
    BEGIN
        SET NOCOUNT ON;

        /* 
	Note: 
	
	SET @Contract_Id is null in sproc even app pass the Contract_Id , Because we need to show that configauration in 
		All extended contracts if any recalc is open end date.

	*/

        IF (@Contract_Id IS NOT NULL)
            BEGIN

                SET @Contract_Id = NULL;

            END;

        DECLARE @Source_cd INT;

        SELECT
            @Source_cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Valcon Source Type'
            AND c.Code_Value = 'Contract';

        SELECT
            acirt.Account_Commodity_Invoice_Recalc_Type_Id
            , acirt.Account_Id
            , com.Commodity_Id
            , com.Commodity_Name
            , c.Code_Id
            , c.Code_Value
            , acirt.Start_Dt
            , acirt.End_Dt
            , Source_Cd.Code_Value AS Determinant_Source
            , Determinant_Source_Cd
            , NULL AS Source_Cd
        FROM
            dbo.Account_Commodity_Invoice_Recalc_Type acirt
            INNER JOIN dbo.Commodity com
                ON acirt.Commodity_ID = com.Commodity_Id
            INNER JOIN dbo.Code c
                ON acirt.Invoice_Recalc_Type_Cd = c.Code_Id
            LEFT JOIN dbo.Code Source_Cd
                ON acirt.Determinant_Source_Cd = Source_Cd.Code_Id
        WHERE
            acirt.Account_Id = @Account_Id
            AND (   @Commodity_Id IS NULL
                    OR  acirt.Commodity_ID = @Commodity_Id)
        UNION

        -------------------------------Supplier Account Contract Level ---------------------------------------------	
        SELECT
            acirt.Account_Commodity_Invoice_Recalc_Type_Id
            , acirt.Account_Id
            , com.Commodity_Id
            , com.Commodity_Name
            , c.Code_Id
            , c.Code_Value
            , acirt.Start_Dt
            , acirt.End_Dt
            , Source_Cd.Code_Value AS Determinant_Source
            , acirt.Determinant_Source_Cd
            , @Source_cd AS Source_Cd
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN dbo.Contract_Recalc_Config crc
                ON crc.Contract_Id = cha.Supplier_Contract_ID
            INNER JOIN dbo.Account_Commodity_Invoice_Recalc_Type acirt
                ON acirt.Account_Id = cha.Account_Id
                   AND  crc.Start_Dt = acirt.Start_Dt
                   AND  ISNULL(crc.End_Dt, '9999-12-31') = ISNULL(acirt.End_Dt, '9999-12-31')

            --AND  (   acirt.Start_Dt BETWEEN crc.Start_Dt
            --                        AND     ISNULL(crc.End_Dt, '9999-12-31')
            --         OR  ISNULL(acirt.End_Dt, '9999-12-31') BETWEEN crc.Start_Dt
            --                                                AND     ISNULL(crc.End_Dt, '9999-12-31'))

            INNER JOIN dbo.Commodity com
                ON acirt.Commodity_ID = com.Commodity_Id
            INNER JOIN dbo.Code c
                ON crc.Supplier_Recalc_Type_Cd = c.Code_Id
            LEFT JOIN dbo.Code Source_Cd
                ON acirt.Determinant_Source_Cd = Source_Cd.Code_Id
        WHERE
            acirt.Account_Id = @Account_Id
            AND (   @Contract_Id IS NULL
                    OR  crc.Contract_Id = @Contract_Id)
            AND acirt.Source_Cd = @Source_cd
            AND (   @Commodity_Id IS NULL
                    OR  acirt.Commodity_ID = @Commodity_Id)
            AND crc.Is_Consolidated_Contract_Config = 0

        -------------------------------Consolidated Account Contract Level ---------------------------------------------	
        UNION
        SELECT
            acirt.Account_Commodity_Invoice_Recalc_Type_Id
            , acirt.Account_Id
            , com.Commodity_Id
            , com.Commodity_Name
            , c.Code_Id
            , c.Code_Value
            , acirt.Start_Dt
            , acirt.End_Dt
            , Source_Cd.Code_Value AS Determinant_Source
            , acirt.Determinant_Source_Cd
            , @Source_cd AS Source_Cd
        FROM
            Core.Client_Hier_Account u_cha
            INNER JOIN Core.Client_Hier_Account s_cha
                ON s_cha.Client_Hier_Id = u_cha.Client_Hier_Id
                   AND  s_cha.Meter_Id = u_cha.Meter_Id
            INNER JOIN dbo.Contract_Recalc_Config crc
                ON crc.Contract_Id = s_cha.Supplier_Contract_ID
            INNER JOIN dbo.Account_Commodity_Invoice_Recalc_Type acirt
                ON acirt.Account_Id = u_cha.Account_Id
                   AND  crc.Start_Dt = acirt.Start_Dt
                   AND  ISNULL(crc.End_Dt, '9999-12-31') = ISNULL(acirt.End_Dt, '9999-12-31')

            --AND  (   acirt.Start_Dt BETWEEN crc.Start_Dt
            --                        AND     ISNULL(crc.End_Dt, '9999-12-31')
            --         OR  ISNULL(acirt.End_Dt, '9999-12-31') BETWEEN crc.Start_Dt
            --                                                AND     ISNULL(crc.End_Dt, '9999-12-31'))


            INNER JOIN dbo.Commodity com
                ON acirt.Commodity_ID = com.Commodity_Id
            INNER JOIN dbo.Code c
                ON crc.Supplier_Recalc_Type_Cd = c.Code_Id
            LEFT JOIN dbo.Code Source_Cd
                ON acirt.Determinant_Source_Cd = Source_Cd.Code_Id
        WHERE
            acirt.Account_Id = @Account_Id
            AND (   @Contract_Id IS NULL
                    OR  crc.Contract_Id = @Contract_Id)
            AND acirt.Source_Cd = @Source_cd
            AND (   @Commodity_Id IS NULL
                    OR  acirt.Commodity_ID = @Commodity_Id)
            AND crc.Is_Consolidated_Contract_Config = 1
            AND u_cha.Account_Type = 'utility'
            AND s_cha.Account_Type = 'supplier'
        ORDER BY
            acirt.Start_Dt;


    END;














GO
GRANT EXECUTE ON  [dbo].[Account_Commodity_Invoice_Recalc_Type_Sel] TO [CBMSApplication]
GO
