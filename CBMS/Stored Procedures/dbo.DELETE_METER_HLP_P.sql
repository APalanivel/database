SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE   PROCEDURE dbo.DELETE_METER_HLP_P
	@meterId int
	as
	begin
		set nocount on

		delete  historic_load_profile where meter_id = @meterId

	end
GO
GRANT EXECUTE ON  [dbo].[DELETE_METER_HLP_P] TO [CBMSApplication]
GO
