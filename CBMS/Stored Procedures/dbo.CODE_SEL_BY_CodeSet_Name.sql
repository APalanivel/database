SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********   
NAME:  core.Code_Sel_By_CodeSet_Name  
 
DESCRIPTION:  Used to select code value from code table using the code name  

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
@CodeSet_Name			VARCHAR(25)   
        
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:   

	EXEC CODE_SEL_BY_CodeSet_Name 'HierLevel'
	EXEC CODE_SEL_BY_CodeSet_Name 'Frequency', 'Monthly'
	EXEC CODE_SEL_BY_CodeSet_Name 'Contract_Classification'

------------------------------------------------------------  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
SS   Subhash Subramanyam         
HG   Harihara Suthan Ganeshan     
MA   Mohammed Aman
SKA	 Shobhit Kr Agrawal

Initials Date  Modification  
------------------------------------------------------------  
SKA	07/07/2009  Modofied this SP as per Coding standards
HG	07/07/2009	Is_Active flag validation added and Order by display sequence added.
HG	11/7/2009	Added Code_dsc in select clause
				updated Usage comment header with the new examples.
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.CODE_SEL_BY_CodeSet_Name
	@CodeSet_Name VARCHAR(25),
	@Code_Value VARCHAR(25) = NULL
AS

BEGIN
	
	SET NOCOUNT ON;
	
	SELECT
		cd.Code_Id
		,cd.Code_Value
		,cd.Code_Dsc		
	FROM
		dbo.CODE cd
		INNER JOIN dbo.Codeset cs 
			ON cs.Codeset_Id = cd.CodeSet_Id
	WHERE cs.CodeSet_Name = @CodeSet_Name 
		AND ((@Code_Value IS NULL)
			OR (cd.CODE_VALUE = @Code_Value))
		AND Is_Active = 1
	ORDER BY
		cd.Display_seq
		

END
GO
GRANT EXECUTE ON  [dbo].[CODE_SEL_BY_CodeSet_Name] TO [CBMSApplication]
GO
