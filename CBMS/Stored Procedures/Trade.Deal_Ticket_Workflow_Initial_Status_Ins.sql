SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    Trade.Deal_Ticket_Workflow_Initial_Status_Ins
   
    
DESCRIPTION:   
   
  This procedure to get summary page details for deal ticket summery.
    
INPUT PARAMETERS:    
      Name          DataType       Default        Description    
-----------------------------------------------------------------------------    
	@Deal_Ticket_Id   INT
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec Trade.Deal_Ticket_Workflow_Initial_Status_Ins  195
	Exec Trade.Deal_Ticket_Workflow_Initial_Status_Ins  198
	Exec Trade.Deal_Ticket_Workflow_Initial_Status_Ins  212
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	SP          SrinivasaRao Patchava    
    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	SP          2018-11-21  Global Risk Management - Create sp to get summary page details for deal ticket summery
	RR			2020-02-28	GRM-1768 - Modified Deal_Volume_Above_Contract_Volume calculation as the daily frequency deal
							tickets volumes multiplying twice with number of days of the respective month
     
    
******/

CREATE PROCEDURE [Trade].[Deal_Ticket_Workflow_Initial_Status_Ins]
    (
        @Deal_Ticket_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @WorkflowId INT
            , @Workflow_Status_Map_Id INT
            , @Workflow_Status NVARCHAR(255)
            , @QUEUE_ID INT
            , @Deal_Volume_Above_Contract_Volume BIT = 0
            , @Workflow_Name NVARCHAR(255)
            , @Is_Client_Generated BIT
            , @Deal_Ticket_Frequency VARCHAR(25)
            , @Uom_Id INT;

        CREATE TABLE #DT_Hedged_Volumes
             (
                 Client_Hier_Id INT
                 , Account_Id INT
                 , Deal_Month DATE
                 , Hedged_Volume DECIMAL(28, 0)
                 , Deal_Ticket_Id INT
                 , Contract_Id INT
             );

        CREATE TABLE #Hedged_Volumes
             (
                 Client_Hier_Id INT
                 , Account_Id INT
                 , Deal_Month DATE
                 , Hedged_Volume DECIMAL(28, 0)
                 , Contract_Id INT
             );

        SELECT
            @Is_Client_Generated = dt.Is_Client_Generated
            , @Deal_Ticket_Frequency = frq.Code_Value
            , @WorkflowId = dt.Workflow_Id
            , @Uom_Id = dt.Uom_Type_Id
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN dbo.Code frq
                ON frq.Code_Id = dt.Deal_Ticket_Frequency_Cd
        WHERE
            dt.Deal_Ticket_Id = @Deal_Ticket_Id;

        SELECT
            @Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
            , @Workflow_Name = wf.Workflow_Name
        FROM
            dbo.Workflow wf
            INNER JOIN Trade.Workflow_Status_Map wsm
                ON wf.Workflow_Id = wsm.Workflow_Id
            INNER JOIN Trade.Workflow_Status ws
                ON wsm.Workflow_Status_Id = ws.Workflow_Status_Id
        WHERE
            wsm.Workflow_Id = @WorkflowId
            AND ws.Workflow_Status_Name = 'DT Created';

        INSERT INTO #DT_Hedged_Volumes
             (
                 Client_Hier_Id
                 , Account_Id
                 , Deal_Month
                 , Hedged_Volume
                 , Deal_Ticket_Id
                 , Contract_Id
             )
        SELECT
            dtv.Client_Hier_Id
            , dtv.Account_Id
            , dtv.Deal_Month
            , CASE WHEN frq.Code_Value = 'Daily' THEN MAX((CASE WHEN trdact.Code_Value = 'Buy' THEN dtv.Total_Volume
                                                               WHEN trdact.Code_Value = 'Sell' THEN -dtv.Total_Volume
                                                           END) * dtcuc.CONVERSION_FACTOR) * dd.DAYS_IN_MONTH_NUM
                  ELSE MAX((CASE WHEN trdact.Code_Value = 'Buy' THEN dtv.Total_Volume
                                WHEN trdact.Code_Value = 'Sell' THEN -dtv.Total_Volume
                            END) * dtcuc.CONVERSION_FACTOR)
              END AS Hedged_Volume
            , dtv.Deal_Ticket_Id
            , dtv.Contract_Id
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtv
                ON dtv.Deal_Ticket_Id = dt.Deal_Ticket_Id
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION dtcuc
                ON dtcuc.BASE_UNIT_ID = dtv.Uom_Type_Id
                   AND  dtcuc.CONVERTED_UNIT_ID = @Uom_Id
            INNER JOIN Trade.Trade_Price tp
                ON dtv.Trade_Price_Id = tp.Trade_Price_Id
            INNER JOIN dbo.Code trdact
                ON dt.Trade_Action_Type_Cd = trdact.Code_Id
            INNER JOIN dbo.Code frq
                ON dt.Deal_Ticket_Frequency_Cd = frq.Code_Id
            INNER JOIN meta.DATE_DIM dd
                ON dd.DATE_D = dtv.Deal_Month
        WHERE
            tp.Trade_Price IS NOT NULL
            AND EXISTS (   SELECT
                                1
                           FROM
                                Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtchvd
                                INNER JOIN Trade.Deal_Ticket dt2
                                    ON dt2.Deal_Ticket_Id = dtchvd.Deal_Ticket_Id
                           WHERE
                                dt2.Deal_Ticket_Id = @Deal_Ticket_Id
                                AND dtv.Client_Hier_Id = dtchvd.Client_Hier_Id
                                AND dtv.Account_Id = dtchvd.Account_Id
                                AND dtv.Contract_Id = dtchvd.Contract_Id
                                AND dtv.Deal_Month = dtchvd.Deal_Month
                                AND dt.Trade_Action_Type_Cd = dt2.Trade_Action_Type_Cd
                                AND dt.Hedge_Mode_Type_Id = dt2.Hedge_Mode_Type_Id
                                AND dtv.Deal_Ticket_Id <> dtchvd.Deal_Ticket_Id)
        GROUP BY
            dtv.Client_Hier_Id
            , dtv.Account_Id
            , dtv.Deal_Month
            , dtv.Deal_Ticket_Id
            , frq.Code_Value
            , dd.DAYS_IN_MONTH_NUM
            , dtv.Contract_Id;

        INSERT INTO #Hedged_Volumes
             (
                 Client_Hier_Id
                 , Account_Id
                 , Deal_Month
                 , Hedged_Volume
                 , Contract_Id
             )
        SELECT
            Client_Hier_Id
            , Account_Id
            , Deal_Month
            , SUM(ISNULL(Hedged_Volume, 0))
            , Contract_Id
        FROM
            #DT_Hedged_Volumes dhv
        GROUP BY
            Client_Hier_Id
            , Account_Id
            , Deal_Month
            , Contract_Id;


        INSERT INTO Trade.Deal_Ticket_Client_Hier_Workflow_Status
             (
                 Deal_Ticket_Client_Hier_Id
                 , Workflow_Status_Map_Id
                 , Workflow_Status_Comment
                 , Is_Active
                 , Last_Notification_Sent_Ts
                 , Created_Ts
                 , Updated_User_Id
                 , Last_Change_Ts
             )
        SELECT
            dtch.Deal_Ticket_Client_Hier_Id
            , @Workflow_Status_Map_Id
            , NULL  --'New deal ticket created'
            , 1
            , GETDATE()
            , GETDATE()
            , dt.Updated_User_Id
            , GETDATE()
        FROM
            Trade.Deal_Ticket_Client_Hier dtch
            INNER JOIN Trade.Deal_Ticket dt
                ON dtch.Deal_Ticket_Id = dt.Deal_Ticket_Id
        WHERE
            dt.Deal_Ticket_Id = @Deal_Ticket_Id;


        SELECT
            @Deal_Volume_Above_Contract_Volume = 1
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl currdtvol
                ON dt.Deal_Ticket_Id = currdtvol.Deal_Ticket_Id
            LEFT JOIN #Hedged_Volumes hdgddtvol
                ON hdgddtvol.Account_Id = currdtvol.Account_Id
                   AND  hdgddtvol.Client_Hier_Id = currdtvol.Client_Hier_Id
                   AND  hdgddtvol.Contract_Id = currdtvol.Contract_Id
                   AND  hdgddtvol.Deal_Month = currdtvol.Deal_Month
            INNER JOIN Core.Client_Hier_Account cha
                ON currdtvol.Client_Hier_Id = cha.Client_Hier_Id
                   AND  currdtvol.Account_Id = cha.Account_Id
                   AND  cha.Commodity_Id = dt.Commodity_Id
            INNER JOIN dbo.CONTRACT_METER_VOLUME cmv
                ON cha.Meter_Id = cmv.METER_ID
                   AND  cmv.MONTH_IDENTIFIER = currdtvol.Deal_Month
                   AND  cmv.CONTRACT_ID = currdtvol.Contract_Id
            INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cmcuc
                ON cmcuc.BASE_UNIT_ID = cmv.UNIT_TYPE_ID
                   AND  cmcuc.CONVERTED_UNIT_ID = dt.Uom_Type_Id
            INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION chcuc
                ON chcuc.BASE_UNIT_ID = currdtvol.Uom_Type_Id
                   AND  chcuc.CONVERTED_UNIT_ID = dt.Uom_Type_Id
            INNER JOIN dbo.ENTITY frq
                ON cmv.FREQUENCY_TYPE_ID = frq.ENTITY_ID
            INNER JOIN meta.DATE_DIM dd
                ON dd.DATE_D = cmv.MONTH_IDENTIFIER
        WHERE
            dt.Deal_Ticket_Id = @Deal_Ticket_Id
            AND @Workflow_Name IN ( 'Workflow 1', 'Workflow 2' )
        GROUP BY
            currdtvol.Client_Hier_Id
            , currdtvol.Deal_Month
            , currdtvol.Account_Id
            , cmv.CONTRACT_ID
            , chcuc.CONVERSION_FACTOR
            , cmcuc.CONVERSION_FACTOR
            , dd.DAYS_IN_MONTH_NUM
            , frq.ENTITY_NAME
        HAVING
            (MAX((CASE WHEN @Deal_Ticket_Frequency = 'Daily' THEN
                 (currdtvol.Total_Volume) * chcuc.CONVERSION_FACTOR * dd.DAYS_IN_MONTH_NUM
                      ELSE (currdtvol.Total_Volume) * chcuc.CONVERSION_FACTOR
                  END)) + ISNULL(MAX(hdgddtvol.Hedged_Volume), 0)) > SUM((CASE WHEN frq.ENTITY_NAME = 'Daily' THEN
                                                                                   cmv.VOLUME * cmcuc.CONVERSION_FACTOR
                                                                                   * dd.DAYS_IN_MONTH_NUM
                                                                              ELSE cmv.VOLUME * cmcuc.CONVERSION_FACTOR
                                                                          END));

        SELECT
            @Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
        FROM
            dbo.Workflow wf
            INNER JOIN Trade.Workflow_Status_Map wsm
                ON wf.Workflow_Id = wsm.Workflow_Id
            INNER JOIN Trade.Workflow_Status ws
                ON wsm.Workflow_Status_Id = ws.Workflow_Status_Id
        WHERE
            wsm.Workflow_Id = @WorkflowId
            AND (   (   @Is_Client_Generated = 0
                        AND (   (   wf.Workflow_Name = 'Workflow 1'
                                    AND ws.Workflow_Status_Name = 'Pending Internal Approval'
                                    AND @Deal_Volume_Above_Contract_Volume = 1)
                                OR  (   wf.Workflow_Name = 'Workflow 1'
                                        AND ws.Workflow_Status_Name = 'Pending Client Approval'
                                        AND @Deal_Volume_Above_Contract_Volume = 0)
                                OR  (   wf.Workflow_Name = 'Workflow 2'
                                        AND ws.Workflow_Status_Name = 'Pending Internal Approval'
                                        AND @Deal_Volume_Above_Contract_Volume = 1)
                                OR  (   wf.Workflow_Name = 'Workflow 2'
                                        AND ws.Workflow_Status_Name = 'Ready to Trade'
                                        AND @Deal_Volume_Above_Contract_Volume = 0)
                                OR  (   wf.Workflow_Name = 'Workflow 3'
                                        AND ws.Workflow_Status_Name = 'Pending Internal Approval')
                                OR  (   wf.Workflow_Name = 'Workflow 4'
                                        AND ws.Workflow_Status_Name = 'Pending Internal Approval')))
                    OR  (   @Is_Client_Generated = 1
                            AND ws.Workflow_Status_Name = 'Closed'));

        UPDATE
            chws
        SET
            chws.Is_Active = 0
        FROM
            Trade.Deal_Ticket_Client_Hier dtch
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
        WHERE
            dtch.Deal_Ticket_Id = @Deal_Ticket_Id;

        INSERT INTO Trade.Deal_Ticket_Client_Hier_Workflow_Status
             (
                 Deal_Ticket_Client_Hier_Id
                 , Workflow_Status_Map_Id
                 , Workflow_Status_Comment
                 , Is_Active
                 , Last_Notification_Sent_Ts
                 , Created_Ts
                 , Updated_User_Id
                 , Last_Change_Ts
             )
        SELECT
            dtch.Deal_Ticket_Client_Hier_Id
            , @Workflow_Status_Map_Id
            , NULL  -- 'Initial status'
            , 1
            , GETDATE()
            , GETDATE()
            , dt.Updated_User_Id
            , GETDATE()
        FROM
            Trade.Deal_Ticket_Client_Hier dtch
            INNER JOIN Trade.Deal_Ticket dt
                ON dtch.Deal_Ticket_Id = dt.Deal_Ticket_Id
        WHERE
            dt.Deal_Ticket_Id = @Deal_Ticket_Id;

        SELECT
            @Workflow_Status = ws.Workflow_Status_Name
        FROM
            Trade.Workflow_Status ws
            INNER JOIN Trade.Workflow_Status_Map wsm
                ON ws.Workflow_Status_Id = wsm.Workflow_Status_Id
        WHERE
            wsm.Workflow_Status_Map_Id = @Workflow_Status_Map_Id;

        SELECT
            @QUEUE_ID = q.QUEUE_ID
        FROM
            dbo.QUEUE q
        WHERE
            EXISTS (   SELECT
                            1
                       FROM
                            dbo.GROUP_INFO gi
                       WHERE
                            gi.GROUP_NAME = 'RM Deal Ticket Approver'
                            AND @Workflow_Status = 'Pending Internal Approval'
                            AND gi.QUEUE_ID = q.QUEUE_ID)
            OR  EXISTS (   SELECT
                                1
                           FROM
                                dbo.USER_INFO ui
                                INNER JOIN Trade.Deal_Ticket dt
                                    ON ui.USER_INFO_ID = dt.Created_User_Id
                           WHERE
                                dt.Deal_Ticket_Id = @Deal_Ticket_Id
                                AND @Workflow_Status <> 'Pending Internal Approval'
                                AND ui.QUEUE_ID = q.QUEUE_ID);

        UPDATE
            Trade.Deal_Ticket
        SET
            Queue_Id = @QUEUE_ID
        WHERE
            Deal_Ticket_Id = @Deal_Ticket_Id;

        EXEC Trade.Deal_Ticket_Last_Updated_Ins @Deal_Ticket_Id;


    END;











GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Workflow_Initial_Status_Ins] TO [CBMSApplication]
GO
