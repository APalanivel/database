SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
	 dbo.Account_Number_Exists_Sel_By_Account_Number_Vendor_Meter    
    
DESCRIPTION:    
    
 Rolling_Meters.
     
    
INPUT PARAMETERS:    
 Name					DataType	 Default		Description    
--------------------------------------------------------------------    
 
    
OUTPUT PARAMETERS:    
 Name					DataType	 Default		Description    
--------------------------------------------------------------------    
    
USAGE EXAMPLES:    
--------------------------------------------------------------------    


EXEC Account_Number_Exists_Sel_By_Account_Number_Vendor_Meter
    @Account_Number = '2622184EPR3425148'
    , @Meter_Id = '51191'
    , @Account_Vendor_Id = 2382;



 Select * from Core.client_hier_Account where meter_Id  =51191

    
AUTHOR INITIALS:    
	Initials		Name    
--------------------------------------------------------------------    
    NR				Narayana Reddy       
    
MODIFICATIONS    
    
 Initials		Date			Modification    
--------------------------------------------------------------------    
 NR				2019-05-30		Created For - Add Contract Project.
  
******/

CREATE PROCEDURE [dbo].[Account_Number_Exists_Sel_By_Account_Number_Vendor_Meter]
     (
         @Account_Number VARCHAR(50)
         , @Meter_Id VARCHAR(MAX)
         , @Account_Vendor_Id INT
     )
AS
    SET NOCOUNT ON;



    BEGIN


        DECLARE @Same_Account_Number_Exists BIT = 0;


        SELECT
            @Same_Account_Number_Exists = 1
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Account_Type = 'Supplier'
            AND cha.Account_Vendor_Id = @Account_Vendor_Id
            AND cha.Account_Number = @Account_Number
            AND EXISTS (   SELECT
                                1
                           FROM
                                dbo.ufn_split(@Meter_Id, ',') us
                           WHERE
                                us.Segments = cha.Meter_Id);


        SELECT  @Same_Account_Number_Exists AS Same_Account_Number_Exists;

    END;

GO
GRANT EXECUTE ON  [dbo].[Account_Number_Exists_Sel_By_Account_Number_Vendor_Meter] TO [CBMSApplication]
GO
