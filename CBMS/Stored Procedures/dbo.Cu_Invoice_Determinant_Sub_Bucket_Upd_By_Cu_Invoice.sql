SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
                           
 NAME: dbo.Cu_Invoice_Determinant_Sub_Bucket_Upd_By_Cu_Invoice                       
                            
 DESCRIPTION:        
    To Update the sub bucket info in Cu_invoice_Determinent table.                          
                            
 INPUT PARAMETERS:        
                           
 Name                               DataType          Default       Description        
---------------------------------------------------------------------------------------------------------------      
 @Cu_Invoice_Id             INT          
                            
 OUTPUT PARAMETERS:        
                                 
 Name                               DataType          Default       Description        
---------------------------------------------------------------------------------------------------------------      
                            
 USAGE EXAMPLES:                                
---------------------------------------------------------------------------------------------------------------        
                 
  BEGIN TRAN          
  SELECT * FROM dbo.CU_INVOICE_DETERMINANT cid WHERE CU_INVOICE_ID =22907303  
  EXEC dbo.Cu_Invoice_Determinant_Sub_Bucket_Upd_By_Cu_Invoice @Cu_Invoice_Id=22907303      
  SELECT * FROM dbo.CU_INVOICE_DETERMINANT cid WHERE CU_INVOICE_ID =22907303  
  ROLLBACK     
                     
                           
 AUTHOR INITIALS:      
         
 Initials                   Name        
---------------------------------------------------------------------------------------------------------------      
 NR                     Narayana Reddy                              
                             
 MODIFICATIONS:      
                             
 Initials               Date            Modification      
---------------------------------------------------------------------------------------------------------------      
 NR                     2013-12-30      Created for SE2017-226.                          
 HG      2020-05-31  D20-1972, Modified the procedure to not update the sub bucket when auto mapping logic is not defined and more than one sub bucket defined for the State / Bucket combination  
******/  
CREATE  PROCEDURE [dbo].[Cu_Invoice_Determinant_Sub_Bucket_Upd_By_Cu_Invoice]  
(@Cu_Invoice_Id INT)  
AS  
BEGIN  
 SET NOCOUNT ON;  
  
 DECLARE @Ubm_Id INT;  
  
  
 CREATE TABLE #Bucket_State_Commodity_Dtl  
 (  
  State_Id     INT  
  , Bucket_Master_Id INT  
  , Commodity_Id    INT  
  , Uom_Id     INT  
 );  
  
  
 CREATE TABLE #Ec_Sub_Invoice_Bucket_Dtl_Uom_Map  
 (  
  EC_Invoice_Sub_Bucket_Master_Id INT  
  , Bucket_Master_Id    INT  
  , Commodity_Id     INT  
  , Uom_Id      INT  
 );  
  
 CREATE TABLE #Ec_Sub_Invoice_Bucket_Dtl  
 (  
  EC_Invoice_Sub_Bucket_Master_Id INT  
  , Bucket_Master_Id    INT  
  , Commodity_Id     INT  
 );  
  
 SELECT @Ubm_Id = ci.UBM_ID FROM dbo.CU_INVOICE ci WHERE ci.CU_INVOICE_ID = @Cu_Invoice_Id;  
  
 INSERT INTO #Bucket_State_Commodity_Dtl  
  (  
   State_Id  
   , Bucket_Master_Id  
   , Commodity_Id  
   , Uom_Id  
  )  
 SELECT  
  cha.Meter_State_Id  
  , cid.Bucket_Master_Id  
  , cha.Commodity_Id  
  , cid.UNIT_OF_MEASURE_TYPE_ID  
 FROM  
  dbo.CU_INVOICE_DETERMINANT cid  
  INNER JOIN dbo.CU_INVOICE_DETERMINANT_ACCOUNT da  
   ON da.CU_INVOICE_DETERMINANT_ID = cid.CU_INVOICE_DETERMINANT_ID  
  INNER JOIN Core.Client_Hier_Account cha  
   ON cha.Account_Id = da.ACCOUNT_ID AND cha.Commodity_Id = cid.COMMODITY_TYPE_ID  
 WHERE  
  cid.CU_INVOICE_ID = @Cu_Invoice_Id  
  AND cid.EC_Invoice_Sub_Bucket_Master_Id IS NULL  
  AND cid.Bucket_Master_Id IS NOT NULL  
 GROUP BY  
  cha.Meter_State_Id  
  , cid.Bucket_Master_Id  
  , cha.Commodity_Id  
  , cid.UNIT_OF_MEASURE_TYPE_ID;  
  
 INSERT INTO #Ec_Sub_Invoice_Bucket_Dtl_Uom_Map  
  (  
   EC_Invoice_Sub_Bucket_Master_Id  
   , Bucket_Master_Id  
   , Commodity_Id  
   , Uom_Id  
  )  
 SELECT  
  eisbm.EC_Invoice_Sub_Bucket_Master_Id  
  , eisbm.Bucket_Master_Id  
  , bm.Commodity_Id  
  , bst.Uom_Id  
 FROM  
  dbo.EC_Invoice_Sub_Bucket_Master eisbm  
  INNER JOIN dbo.Bucket_Master bm  
   ON eisbm.Bucket_Master_Id = bm.Bucket_Master_Id  
  INNER JOIN #Bucket_State_Commodity_Dtl bst  
   ON eisbm.Bucket_Master_Id = bst.Bucket_Master_Id  
      AND eisbm.State_Id = bst.State_Id  
      AND bst.Commodity_Id = bm.Commodity_Id  
 WHERE  
  EXISTS (  
       SELECT  
        1  
       FROM  
        dbo.EC_Invoice_Sub_Bucket_UBM_Uom_Map um_mp  
       WHERE  
        um_mp.EC_Invoice_Sub_Bucket_Master_Id = eisbm.EC_Invoice_Sub_Bucket_Master_Id  
        AND um_mp.Uom_Id = ISNULL(bst.Uom_Id, -1)  
        AND um_mp.Ubm_Id = @Ubm_Id  
      )  
 GROUP BY  
  eisbm.EC_Invoice_Sub_Bucket_Master_Id
  ,eisbm.Bucket_Master_Id  
  , bm.Commodity_Id  
  , bst.Uom_Id;  
  
  
 INSERT INTO #Ec_Sub_Invoice_Bucket_Dtl  
  (  
   EC_Invoice_Sub_Bucket_Master_Id  
   , Bucket_Master_Id  
   , Commodity_Id  
  )  
 SELECT  
  final.EC_Invoice_Sub_Bucket_Master_Id  
  , final.Bucket_Master_Id  
  , final.Commodity_Id  
 FROM(  
   SELECT  
    MIN(eisbm.EC_Invoice_Sub_Bucket_Master_Id) EC_Invoice_Sub_Bucket_Master_Id  
    , eisbm.Bucket_Master_Id  
    , bm.Commodity_Id  
    , bst.Uom_Id  
   FROM  
    dbo.EC_Invoice_Sub_Bucket_Master eisbm  
    INNER JOIN dbo.Bucket_Master bm  
     ON eisbm.Bucket_Master_Id = bm.Bucket_Master_Id  
    INNER JOIN #Bucket_State_Commodity_Dtl bst  
     ON eisbm.Bucket_Master_Id = bst.Bucket_Master_Id  
        AND eisbm.State_Id = bst.State_Id  
        AND bst.Commodity_Id = bm.Commodity_Id  
   GROUP BY  
    eisbm.Bucket_Master_Id  
    , bm.Commodity_Id  
    , bst.Uom_Id  
   HAVING  
    COUNT(DISTINCT eisbm.EC_Invoice_Sub_Bucket_Master_Id) = 1 -- Added to get the subbucket only when one to one mapping exists  
  ) AS final  
 WHERE  
  NOT EXISTS (  
        SELECT  
         1  
        FROM  
         #Ec_Sub_Invoice_Bucket_Dtl_Uom_Map ec  
        WHERE  
         ec.EC_Invoice_Sub_Bucket_Master_Id = final.EC_Invoice_Sub_Bucket_Master_Id  
         AND ec.Bucket_Master_Id = final.Bucket_Master_Id  
         AND ec.Commodity_Id = final.Commodity_Id  
         AND ec.Uom_Id = ISNULL(final.Uom_Id, -1)  
       )  
 GROUP BY  
  final.EC_Invoice_Sub_Bucket_Master_Id  
  , final.Bucket_Master_Id  
  , final.Commodity_Id;  
  
 BEGIN TRY  
  BEGIN TRANSACTION;  
  
  
  UPDATE  
   cid  
  SET  
   cid.EC_Invoice_Sub_Bucket_Master_Id = esibd.EC_Invoice_Sub_Bucket_Master_Id  
  FROM  
   dbo.CU_INVOICE_DETERMINANT cid  
   INNER JOIN #Ec_Sub_Invoice_Bucket_Dtl_Uom_Map esibd  
    ON cid.Bucket_Master_Id = esibd.Bucket_Master_Id  
       AND esibd.Commodity_Id = cid.COMMODITY_TYPE_ID  
       AND esibd.Uom_Id = cid.UNIT_OF_MEASURE_TYPE_ID  
  WHERE  
   cid.CU_INVOICE_ID = @Cu_Invoice_Id AND cid.EC_Invoice_Sub_Bucket_Master_Id IS NULL;  
  
  UPDATE  
   cid  
  SET  
   cid.EC_Invoice_Sub_Bucket_Master_Id = esibd.EC_Invoice_Sub_Bucket_Master_Id  
  FROM  
   dbo.CU_INVOICE_DETERMINANT cid  
   INNER JOIN #Ec_Sub_Invoice_Bucket_Dtl esibd  
    ON cid.Bucket_Master_Id = esibd.Bucket_Master_Id AND esibd.Commodity_Id = cid.COMMODITY_TYPE_ID  
  WHERE  
   cid.CU_INVOICE_ID = @Cu_Invoice_Id AND cid.EC_Invoice_Sub_Bucket_Master_Id IS NULL;  
  
  
  COMMIT TRANSACTION;  
  
 END TRY  
 BEGIN CATCH  
  IF @@TRANCOUNT > 0  
   ROLLBACK TRAN;  
  EXEC dbo.usp_RethrowError;  
  
 END CATCH;  
 DROP TABLE  
  #Bucket_State_Commodity_Dtl  
  , #Ec_Sub_Invoice_Bucket_Dtl  
  , #Ec_Sub_Invoice_Bucket_Dtl_Uom_Map;  
END;
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Determinant_Sub_Bucket_Upd_By_Cu_Invoice] TO [CBMSApplication]
GO
