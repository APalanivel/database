SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.GET_PREVIOUS_DATE_P
@userId varchar,
@sessionId varchar,
@executionDate varchar(12)

as
	set nocount on
	select	distinct CONVERT(Varchar(12),BATCH_EXECUTION_DATE-1, 101)  DATE_VALUE
	from	RM_NYMEX_DATA
	
	where BATCH_EXECUTION_DATE=CONVERT(Varchar(12),@executionDate, 101)
GO
GRANT EXECUTE ON  [dbo].[GET_PREVIOUS_DATE_P] TO [CBMSApplication]
GO
