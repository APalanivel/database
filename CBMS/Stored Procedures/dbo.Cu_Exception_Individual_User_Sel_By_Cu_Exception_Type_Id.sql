SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.Cu_Exception_Individual_User_Sel_By_Cu_Exception_Type_Id

DESCRIPTION:

INPUT PARAMETERS:
	Name							DataType		Default					Description
---------------------------------------------------------------------------------------------
	@cu_exception_type_id			INT
   

OUTPUT PARAMETERS:
	Name							DataType		Default					Description
---------------------------------------------------------------------------------------------
	
USAGE EXAMPLES:
---------------------------------------------------------------------------------------------

EXEC dbo.Cu_Exception_Individual_User_Sel_By_Cu_Exception_Type_Id
    @cu_exception_type_id =1
   

AUTHOR INITIALS:
	Initials	Name
---------------------------------------------------------------------------------------------
	NR			Narayana Reddy
	
MODIFICATIONS
	Initials	Date			Modification
---------------------------------------------------------------------------------------------
	NR       	2018-10-04		D20-17 -  Created.

******/

CREATE PROCEDURE [dbo].[Cu_Exception_Individual_User_Sel_By_Cu_Exception_Type_Id]
    (
        @Cu_Exception_Type_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Individual_User_Group_Info_Id INT;

        SELECT
            @Individual_User_Group_Info_Id = gi.GROUP_INFO_ID
        FROM
            dbo.GROUP_INFO gi
        WHERE
            gi.GROUP_NAME = 'Individual User';

        SELECT
            cet.CU_EXCEPTION_TYPE_ID
            , cet.EXCEPTION_TYPE
            , cet.Managed_By_User_Info_Id
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Username
            , ui.QUEUE_ID
        FROM
            dbo.CU_EXCEPTION_TYPE cet
            INNER JOIN dbo.USER_INFO ui
                ON cet.Managed_By_User_Info_Id = ui.USER_INFO_ID
        WHERE
            cet.MANAGED_BY_GROUP_INFO_ID = @Individual_User_Group_Info_Id
            AND cet.CU_EXCEPTION_TYPE_ID = @Cu_Exception_Type_Id;

    END;


GO
GRANT EXECUTE ON  [dbo].[Cu_Exception_Individual_User_Sel_By_Cu_Exception_Type_Id] TO [CBMSApplication]
GO
