
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
	dbo.cbmsCuExceptionDetail_Save
	
DESCRIPTION:

INPUT PARAMETERS:
	Name			DataType		Default	Description
-------------------------------------------------------------------------------------------
	@MyAccountId			int  
    @cu_exception_detail_id int			 null  
    @cu_exception_id		int  
    @exception_type_id		int  
    @exception_status_type_id int  
    @is_closed				bit			 0  
    @closed_reason_type_id	int 		 null  
    @account_id				int			 null  
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------
USE CBMS_TK3
	EXEC cbmsCuExceptionDetail_Save 49,NULL,235648,54,54,0,45,NULL

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SSR			Sharad Srivastava
	RKV			Ravi Kumar Vegesna

MODIFICATIONS

Initials	Date		Modification
------------------------------------------------------------
		   07/21/2008   Autogenrated Script
 SSR	   04/29/2010   Added @account_id as input parameter for GB
						Converted to merge Statement
 SKA	   06/07/2010	Added isnull condition for Account_ID	
 DMR	   08/20/2012	Modified Output clause to Input to Table variable to handle adding of triggers to the table.
 RKV       2015-10-30   added optional parameter @Commodity_id as part of AS400-PII				
*/  
CREATE PROCEDURE [dbo].[cbmsCuExceptionDetail_Save]
      ( 
       @MyAccountId INT
      ,@cu_exception_detail_id INT = NULL
      ,@cu_exception_id INT
      ,@exception_type_id INT
      ,@exception_status_type_id INT
      ,@is_closed BIT = 0
      ,@closed_reason_type_id INT = NULL
      ,@account_id INT = NULL
      ,@Commodity_id INT = NULL )
AS 
BEGIN  
  
      SET NOCOUNT ON;
      DECLARE @OutTable TABLE
            ( 
             cu_exception_detail_id INT NULL
            ,cu_exception_id INT NULL
            ,exception_type_id INT NULL
            ,exception_status_type_id INT NULL
            ,opened_date DATETIME2 NULL
            ,is_closed BIT NOT NULL
                           DEFAULT 0
            ,closed_reason_type_id INT NULL
            ,closed_by_id INT NULL
            ,closed_date DATETIME NULL
            ,account_id INT NULL
            ,Commodity_id INT NULL )
  
      MERGE INTO cu_exception_detail AS Tr_ced
            USING 
                  ( SELECT
                        @cu_exception_detail_id AS cu_exception_detail_id
                       ,@cu_exception_id AS cu_exception_id
                       ,@exception_type_id AS exception_type_id
                       ,@exception_status_type_id AS exception_status_type_id
                       ,@is_closed AS is_closed
                       ,@closed_reason_type_id AS closed_reason_type_id
                       ,@account_id AS account_id
                       ,@MyAccountId AS MyAccountId
                       ,@Commodity_id AS Commodity_id ) Src
            ON ( Tr_ced.cu_exception_id = Src.cu_exception_id
                 AND Tr_ced.exception_type_id = Src.exception_type_id
                 AND ISNULL(tr_ced.account_id, 0) = ISNULL(Src.account_id, 0) 
                 AND ISNULL(tr_ced.Commodity_Id, 0) = ISNULL(Src.Commodity_Id, 0))
            WHEN NOT MATCHED 
                  THEN 
		    INSERT
                        ( 
                         cu_exception_id
                        ,exception_type_id
                        ,exception_status_type_id
                        ,opened_date
                        ,is_closed
                        ,closed_reason_type_id
                        ,closed_by_id
                        ,closed_date
                        ,account_id
                        ,Commodity_Id )
                    VALUES
                        ( 
                         Src.cu_exception_id
                        ,Src.exception_type_id
                        ,Src.exception_status_type_id
                        ,GETDATE()
                        ,Src.is_closed
                        ,Src.closed_reason_type_id
                        ,CASE WHEN Src.is_closed = 1 THEN Src.myAccountId
                              ELSE NULL
                         END
                        ,CASE WHEN Src.is_closed = 1 THEN GETDATE()
                              ELSE NULL
                         END
                        ,Src.account_id
                        ,src.Commodity_id )
            WHEN MATCHED 
                  THEN 
	        UPDATE  SET 
                        Tr_ced.exception_status_type_id = Src.exception_status_type_id
                       ,Tr_ced.is_closed = Src.is_closed
                       ,Tr_ced.closed_reason_type_id = Src.closed_reason_type_id
                       ,Tr_ced.closed_by_id = CASE WHEN Src.is_closed = 1 THEN ISNULL(Tr_ced.closed_by_id, Src.MyAccountId)
                                                   ELSE NULL
                                              END
                       ,Tr_ced.closed_date = CASE WHEN Src.is_closed = 1 THEN ISNULL(Tr_ced.closed_date, GETDATE())
                                                  ELSE NULL
                                             END
                       ,Tr_ced.account_id = Src.account_id
                       ,Tr_ced.Commodity_id = src.Commodity_id
            OUTPUT
                  Inserted.cu_exception_detail_id
                 ,Inserted.cu_exception_id
                 ,Inserted.exception_type_id
                 ,Inserted.exception_status_type_id
                 ,Inserted.opened_date
                 ,Inserted.is_closed
                 ,Inserted.closed_reason_type_id
                 ,Inserted.closed_by_id
                 ,Inserted.closed_date
                 ,Inserted.account_id
                 ,INSERTED.Commodity_id
                  INTO @OutTable
                        ( cu_exception_detail_id, cu_exception_id, exception_type_id, exception_status_type_id, opened_date, is_closed, closed_reason_type_id, closed_by_id, closed_date, account_id, Commodity_id );
                 
      SELECT
            cu_exception_detail_id
           ,cu_exception_id
           ,exception_type_id
           ,exception_status_type_id
           ,opened_date
           ,is_closed
           ,closed_reason_type_id
           ,closed_by_id
           ,closed_date
           ,account_id
           ,Commodity_id
      FROM
            @OutTable
  
END;
;
GO


GRANT EXECUTE ON  [dbo].[cbmsCuExceptionDetail_Save] TO [CBMSApplication]
GO
