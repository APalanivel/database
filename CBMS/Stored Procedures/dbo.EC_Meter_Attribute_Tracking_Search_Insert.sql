SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                                                
NAME: dbo.EC_Meter_Attribute_Tracking_Search_insert                                                                
                                                                 
                                                                
DESCRIPTION:                                                                
       This sproc is to get the Meter and attribute tracking details for the given search criteria                                                          
                                   
                                                                
INPUT PARAMETERS:                                                                
 Name     DataType Default   Description                                                                
------------------------------------------------------------------------------------                                                                
 @Country_Id   INT                  
 @State_Id    INT                  
 @Commodity_Id   INT                  
 @Client_Id    INT = NULL                  
 @Sitegroup_Id   INT = NULL                  
 @Site_Client_Hier_Id INT = NULL                  
 @zipcode    VARCHAR(20) = NULL                  
 @EC_Meter_Attribute_Id_1 INT = NULL                 
 @EC_Meter_Attribute_Value NVARCHAR(255) = NULL                  
 @EC_Meter_Attribute_Id_2 INT                  
 @Start_Index     INT = 1                  
 @End_Index      INT = 2147483647                  
 @Total_Count     INT = 0                  
 @Site_Status     BIT = NULL                  
 @Vendor_Id      INT = NULL                  
 @Contract_Number    VARCHAR(150) = NULL                  
 @Vendor_Type_Cd   INT = NULL                  
 @Meter_Attribute_Type_Cd INT = NULL                  
 @Tarrif_Rate     INT = NULL                  
 @Is_Primary     BIT = NULL                         
                         
                                                                
OUTPUT PARAMETERS:                                                                
 Name       DataType  Default   Description                                                                
------------------------------------------------------------------------------------                                                                
                                                              
                                                                
USAGE EXAMPLES:                                                                
------------------------------------------------------------------------------------                                                                
                                                         
EXEC dbo.EC_Meter_Attribute_Tracking_Search_insert                         
      @Country_Id = 27                        
     ,@State_Id = 124                        
     ,@Commodity_Id = 290                        
     ,@EC_Meter_Attribute_Id_2 = 46                        
     ,@EC_Meter_Attribute_Id_1 = 47                      
                            
                             
EXEC dbo.EC_Meter_Attribute_Tracking_Search_insert                        
      @Country_Id = 27                        
     ,@State_Id = 124                        
     ,@Commodity_Id = 290                        
     ,@EC_Meter_Attribute_Id_2 = 7                        
                             
EXEC dbo.EC_Meter_Attribute_Tracking_Search_insert                         
      @Country_Id = 27                        
     ,@State_Id = 124                        
     ,@Commodity_Id = 290                        
     ,@EC_Meter_Attribute_Id_2 = 38                        
     ,@Start_Index = 1                        
     ,@End_Index = 3000                      
     ,@Total_Count=0                                            
                                                                
AUTHOR INITIALS:                        
 Initials  Name                                                                
------------------------------------------------------------------------------------                                                                
 RKV        Ravi Kumar Vegesna                        
 NR   Narayana Reddy                      
                                                                 
MODIFICATIONS                                                                
                                                                
 Initials Date  Modification                                                                
------------------------------------------------------------------------------------                                                               
 RKV   2015-05-19  Created For As400.                       
 RKV   2016-09-13  MAINT-4274,Added two Columns Attribute_Searched,Current_Value to the result set.                        
 NR    2017-04-27  Added  @Site_Status ,@Vendor_Id as optional parameter.                      
 SLP   2020-01-15  Added Filters @Vendor_Type_Cd, @Meter_Attribute_Type_Cd ,@Tarrif_Rate ,@Is_Primary              
                       
                        
******/
CREATE PROCEDURE [dbo].[EC_Meter_Attribute_Tracking_Search_Insert]
     (
         @Country_Id INT
         , @State_Id INT
         , @Commodity_Id INT
         , @Client_Id INT = NULL
         , @Sitegroup_Id INT = NULL
         , @Site_Client_Hier_Id INT = NULL
         , @zipcode VARCHAR(20) = NULL
         , @EC_Meter_Attribute_Id_1 INT = NULL
         , @EC_Meter_Attribute_Value NVARCHAR(255) = NULL
         , @EC_Meter_Attribute_Id_2 INT
         , @Start_Index INT = 1
         , @End_Index INT = 2147483647
         , @Total_Count INT = 0
         , @Site_Status BIT = NULL
         , @Vendor_Id INT = NULL
         , @Contract_Number VARCHAR(150) = NULL
         , @Vendor_Type_Cd INT = NULL
         , @Meter_Attribute_Type_Cd INT = NULL
         , @Tarrif_Rate INT = NULL
         , @Is_Primary BIT = NULL
     )
AS
    BEGIN

        SET NOCOUNT ON;

        SET @Contract_Number = '%' + @Contract_Number + '%';


        DECLARE
            @EC_Meter_Attribute_Name NVARCHAR(400)
            , @Attribute_Type VARCHAR(25)
            , @sitegroup_Name VARCHAR(200)
            , @Attribute_Searched NVARCHAR(200)
            -- , @Attribute_Value_Type INT                      
            , @Vendor_Type_Value VARCHAR(25) = NULL
            , @Meter_Attribute_Type_Value VARCHAR(25) = NULL
            , @Meter_Attribute_Type_Value_Update VARCHAR(25) = NULL
            , @Vendor_Type_Value_Update VARCHAR(255) = NULL
            , @Vendor_Type_Update VARCHAR(25);




        SELECT
            @Vendor_Type_Value = Code_Value
        FROM
            Code
        WHERE
            Code_Id = @Vendor_Type_Cd;

        SELECT
            @Vendor_Type_Value = c.Code_Value
        FROM
            dbo.EC_Meter_Attribute AS ema
            JOIN Code c
                ON c.Code_Id = ema.Vendor_Type_Cd
        WHERE
            ema.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_1;

        SELECT
            @Meter_Attribute_Type_Value = Code_Value
        FROM
            Code
        WHERE
            Code_Id = @Meter_Attribute_Type_Cd;

        CREATE TABLE #EC_Meter_Attribute_Tracking_Search
             (
                 EC_Meter_Attribute_Id INT
                 , Meter_Id INT
                 , EC_Meter_Attribute_Value NVARCHAR(400)
                 , EC_Meter_Attribute_Name NVARCHAR(400)
                 , Attribute_Type VARCHAR(25)
                 , Start_Dt DATE
                 , End_Dt DATE
                 , Vendor_Type_Cd INT
                 , Vendor_Type_Value VARCHAR(25)
                 , Meter_Attribute_Type_Cd INT
                 , Meter_Attribute_Type_Value VARCHAR(25)
             );

        INSERT INTO #EC_Meter_Attribute_Tracking_Search
             (
                 EC_Meter_Attribute_Id
                 , Meter_Id
                 , EC_Meter_Attribute_Value
                 , EC_Meter_Attribute_Name
                 , Attribute_Type
                 , Start_Dt
                 , End_Dt
                 , Vendor_Type_Cd
                 , Vendor_Type_Value
                 , Meter_Attribute_Type_Cd
                 , Meter_Attribute_Type_Value
             )
        SELECT
            x.EC_Meter_Attribute_Id
            , x.Meter_Id
            , x.EC_Meter_Attribute_Value
            , x.EC_Meter_Attribute_Name
            , x.Code_Value
            , x.Start_Dt
            , x.End_Dt
            , x.Vendor_Type_Cd
            , x.Vendor_Type_Value
            , x.Meter_Attribute_Type_Cd
            , x.Meter_Attribute_Type_Value
        FROM    (   SELECT
                        emae.EC_Meter_Attribute_Id
                        , emat.Meter_Id
                        , emat.EC_Meter_Attribute_Value
                        , emae.EC_Meter_Attribute_Name
                        , cd.Code_Value
                        , emat.Start_Dt
                        , emat.End_Dt
                        , emae.Vendor_Type_Cd
                        , cdd.Code_Value Vendor_Type_Value
                        , emat.Meter_Attribute_Type_Cd
                        , co.Code_Value Meter_Attribute_Type_Value
                        , ROW_NUMBER() OVER (PARTITION BY
                                                 emat.Meter_Id
                                             ORDER BY
                                                 emat.Start_Dt DESC) AS Row_Num
                    FROM
                        dbo.EC_Meter_Attribute emae
                        INNER JOIN dbo.EC_Meter_Attribute_Tracking emat
                            ON emae.EC_Meter_Attribute_Id = emat.EC_Meter_Attribute_Id
                        INNER JOIN dbo.Code cd
                            ON cd.Code_Id = emae.Attribute_Type_Cd
                        LEFT JOIN dbo.Code cdd
                            ON cdd.Code_Id = emae.Vendor_Type_Cd
                        LEFT JOIN Code co
                            ON co.Code_Id = emat.Meter_Attribute_Type_Cd
                    WHERE
                        (   @EC_Meter_Attribute_Id_1 IS NULL
                            OR emae.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_1)
                        AND (   @Meter_Attribute_Type_Cd IS NULL
                                OR emat.Meter_Attribute_Type_Cd = @Meter_Attribute_Type_Cd)
                        AND (   @Vendor_Type_Cd IS NULL
                                OR emae.Vendor_Type_Cd = @Vendor_Type_Cd)
                        AND (   @EC_Meter_Attribute_Value IS NULL
                                OR emat.EC_Meter_Attribute_Value = @EC_Meter_Attribute_Value)) x
        WHERE
            x.Row_Num = 1;


        --SELECT  *  FROM #EC_Meter_Attribute_Tracking_Search        


        SELECT
            @EC_Meter_Attribute_Name = EC_Meter_Attribute_Name
            , @Attribute_Type = cd.Code_Value
            , @Vendor_Type_Value_Update = c.Code_Value
        FROM
            dbo.EC_Meter_Attribute emae
            INNER JOIN dbo.Code cd
                ON cd.Code_Id = emae.Attribute_Type_Cd
            LEFT OUTER JOIN dbo.Code c
                ON c.Code_Id = emae.Vendor_Type_Cd
        WHERE
            EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_2;

        SELECT
            @sitegroup_Name = s.Sitegroup_Name
        FROM
            dbo.Sitegroup s
        WHERE
            Sitegroup_Id = @Sitegroup_Id;

        SELECT
            @Attribute_Searched = ema.EC_Meter_Attribute_Name
        FROM
            dbo.EC_Meter_Attribute ema
        WHERE
            ema.State_Id = @State_Id
            AND ema.Commodity_Id = @Commodity_Id
            AND ema.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_1;



        IF @Total_Count = 0
            BEGIN
                SELECT
                    @Total_Count = COUNT(1) OVER ()
                FROM
                    Core.Client_Hier_Account cha
                    INNER JOIN Core.Client_Hier ch
                        ON cha.Client_Hier_Id = ch.Client_Hier_Id
                    LEFT OUTER JOIN #EC_Meter_Attribute_Tracking_Search emat
                        ON cha.Meter_Id = emat.Meter_Id
                    LEFT OUTER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                        ON cha.Meter_Id = samm.METER_ID
                    LEFT OUTER JOIN dbo.CONTRACT c
                        ON samm.Contract_ID = c.CONTRACT_ID
                    LEFT JOIN Budget.Account_Commodity_Primary_Meter acpm
                        ON acpm.Account_Id = cha.Account_Id
                           AND  acpm.Commodity_Id = cha.Commodity_Id
                           AND  cha.Meter_Id = acpm.Meter_Id
                WHERE
                    ch.Country_Id = @Country_Id
                    AND ch.State_Id = @State_Id
                    AND cha.Commodity_Id = @Commodity_Id
                    AND (   @zipcode IS NULL
                            OR  cha.Meter_ZipCode = @zipcode)
                    AND (   @EC_Meter_Attribute_Id_1 IS NULL
                            OR  (   @EC_Meter_Attribute_Id_1 IS NOT NULL
                                    AND @EC_Meter_Attribute_Value IS NULL
                                    AND NOT EXISTS (   SELECT
                                                            1
                                                       FROM
                                                            #EC_Meter_Attribute_Tracking_Search ea
                                                       WHERE
                                                            ea.Meter_Id = cha.Meter_Id
                                                            AND ea.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_1))
                            OR  (   @EC_Meter_Attribute_Id_1 IS NOT NULL
                                    AND @EC_Meter_Attribute_Value IS NOT NULL
                                    AND EXISTS (   SELECT
                                                        1
                                                   FROM
                                                        #EC_Meter_Attribute_Tracking_Search eam
                                                   WHERE
                                                        eam.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_1
                                                        AND eam.EC_Meter_Attribute_Value = @EC_Meter_Attribute_Value
                                                        AND emat.EC_Meter_Attribute_Id = eam.EC_Meter_Attribute_Id
                                                        AND emat.EC_Meter_Attribute_Value = eam.EC_Meter_Attribute_Value)))
                    AND (   @Client_Id IS NULL
                            OR  ch.Client_Id = @Client_Id)
                    AND (   @Tarrif_Rate IS NULL
                            OR  cha.Rate_Id = @Tarrif_Rate)
                    AND (   @Site_Client_Hier_Id IS NULL
                            OR  ch.Client_Hier_Id = @Site_Client_Hier_Id)
                    AND cha.Account_Type = 'Utility'
                    AND EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.Sitegroup_Site sgs
                                        INNER JOIN dbo.Sitegroup sg
                                            ON sgs.Sitegroup_id = sg.Sitegroup_Id
                                   WHERE
                                        ch.Site_Id = sgs.Site_id
                                        AND (   @Sitegroup_Id IS NULL
                                                OR  sgs.Sitegroup_id = @Sitegroup_Id))
                    AND (   @Site_Status IS NULL
                            OR  ch.Site_Not_Managed = @Site_Status)
                    AND (   @Vendor_Id IS NULL
                            OR  cha.Account_Vendor_Id = @Vendor_Id)
                    AND (   @Contract_Number IS NULL
                            OR  c.ED_CONTRACT_NUMBER LIKE @Contract_Number)
                    AND (   @Is_Primary IS NULL
                            OR  (   @Is_Primary = 1
                                    AND acpm.Meter_Id IS NOT NULL)
                            OR  (   @Is_Primary = 0
                                    AND acpm.Meter_Id IS NULL))
                GROUP BY
                    cha.Meter_State_Id
                    , cha.Meter_State_Name
                    , ch.Client_Id
                    , ch.Client_Name
                    , ISNULL(@Sitegroup_Id, ch.Sitegroup_Id)
                    , ISNULL(@sitegroup_Name, ch.Sitegroup_Name)
                    , ch.Client_Hier_Id
                    , ch.Site_Id
                    , ch.Site_name
                    , cha.Meter_Id
                    , cha.Meter_Number
                    , emat.EC_Meter_Attribute_Name
                    , emat.EC_Meter_Attribute_Value
                    , cha.Display_Account_Number
                    , cha.Account_Vendor_Id
                    , cha.Account_Vendor_Name
                    , emat.Vendor_Type_Cd
                    , emat.Vendor_Type_Value
                    , emat.Meter_Attribute_Type_Cd
                    , emat.Meter_Attribute_Type_Value
                    , cha.Rate_Name
                    , CASE WHEN acpm.Meter_Id IS NOT NULL THEN 1
                          ELSE 0
                      END;



            END;

        WITH Cte_Attribute_Tracking
        AS (
               SELECT   TOP (@End_Index)
                        cha.Meter_State_Id
                        , cha.Meter_State_Name
                        , ch.Client_Id
                        , ch.Client_Name
                        , ISNULL(@Sitegroup_Id, ch.Sitegroup_Id) Sitegroup_id
                        , ISNULL(@sitegroup_Name, ch.Sitegroup_Name) Sitegroup_Name
                        , ch.Client_Hier_Id
                        , ch.Site_Id
                        , ch.Site_name
                        , cha.Meter_Id
                        , cha.Meter_Number
                        , CASE WHEN @EC_Meter_Attribute_Id_1 IS NOT NULL
                                    AND @EC_Meter_Attribute_Value IS NULL THEN @Attribute_Searched
                              ELSE emat.EC_Meter_Attribute_Name
                          END AS Attribute_Searched
                        , emat.EC_Meter_Attribute_Value Current_Value
                        , cha.Display_Account_Number Account_Number
                        , @EC_Meter_Attribute_Id_2 EC_Meter_Attribute_Id
                        , @EC_Meter_Attribute_Name EC_Meter_Attribute_Name
                        , @Attribute_Type AS Meter_Attribute_Type
                        , '' AS EC_Meter_Attribute_Value
                        , '' AS Start_Dt
                        , '' AS End_Dt
                        , '' AS Vendor_Type_Value_Update
                        , '' AS Meter_Attribute_Type_Update
                        , cha.Account_Vendor_Id
                        , cha.Account_Vendor_Name
                        , emat.Vendor_Type_Cd
                        , emat.Vendor_Type_Value AS Vendor_Type_Value_Search
                        , cha.Rate_Name Tariff_Rate
                        , CASE WHEN acpm.Meter_Id IS NOT NULL THEN 1
                              ELSE 0
                          END AS Is_Primary
                        , ROW_NUMBER() OVER (ORDER BY
                                                 cha.Meter_Number) AS Row_Num
               FROM
                    Core.Client_Hier_Account cha
                    INNER JOIN Core.Client_Hier ch
                        ON cha.Client_Hier_Id = ch.Client_Hier_Id
                    LEFT OUTER JOIN #EC_Meter_Attribute_Tracking_Search emat
                        ON cha.Meter_Id = emat.Meter_Id
                    LEFT OUTER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                        ON cha.Meter_Id = samm.METER_ID
                    LEFT OUTER JOIN dbo.CONTRACT c
                        ON samm.Contract_ID = c.CONTRACT_ID
                    LEFT JOIN Budget.Account_Commodity_Primary_Meter acpm
                        ON acpm.Account_Id = cha.Account_Id
                           AND  acpm.Commodity_Id = cha.Commodity_Id
                           AND  cha.Meter_Id = acpm.Meter_Id
               WHERE
                    ch.Country_Id = @Country_Id
                    AND ch.State_Id = @State_Id
                    AND cha.Commodity_Id = @Commodity_Id
                    AND (   @zipcode IS NULL
                            OR  cha.Meter_ZipCode = @zipcode)
                    AND (   @EC_Meter_Attribute_Id_1 IS NULL
                            OR  (   @EC_Meter_Attribute_Id_1 IS NOT NULL
                                    AND @EC_Meter_Attribute_Value IS NULL
                                    AND NOT EXISTS (   SELECT
                                                            1
                                                       FROM
                                                            #EC_Meter_Attribute_Tracking_Search ea
                                                       WHERE
                                                            ea.Meter_Id = cha.Meter_Id
                                                            AND ea.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_1))
                            OR  (   @EC_Meter_Attribute_Id_1 IS NOT NULL
                                    AND @EC_Meter_Attribute_Value IS NOT NULL
                                    AND EXISTS (   SELECT
                                                        1
                                                   FROM
                                                        #EC_Meter_Attribute_Tracking_Search eam
                                                   WHERE
                                                        eam.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_1
                                                        AND eam.EC_Meter_Attribute_Value = @EC_Meter_Attribute_Value
                                                        AND emat.EC_Meter_Attribute_Id = eam.EC_Meter_Attribute_Id
                                                        AND emat.EC_Meter_Attribute_Value = eam.EC_Meter_Attribute_Value)))
                    AND (   @Client_Id IS NULL
                            OR  ch.Client_Id = @Client_Id)
                    AND (   @Tarrif_Rate IS NULL
                            OR  cha.Rate_Id = @Tarrif_Rate)
                    AND (   @Site_Client_Hier_Id IS NULL
                            OR  ch.Client_Hier_Id = @Site_Client_Hier_Id)
                    AND cha.Account_Type = 'Utility'
                    AND EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.Sitegroup_Site sgs
                                        INNER JOIN dbo.Sitegroup sg
                                            ON sgs.Sitegroup_id = sg.Sitegroup_Id
                                   WHERE
                                        ch.Site_Id = sgs.Site_id
                                        AND (   @Sitegroup_Id IS NULL
                                                OR  sgs.Sitegroup_id = @Sitegroup_Id))
                    AND (   @Site_Status IS NULL
                            OR  ch.Site_Not_Managed = @Site_Status)
                    AND (   @Vendor_Id IS NULL
                            OR  cha.Account_Vendor_Id = @Vendor_Id)
                    AND (   @Contract_Number IS NULL
                            OR  c.ED_CONTRACT_NUMBER LIKE @Contract_Number)
                    AND (   @Is_Primary IS NULL
                            OR  (   @Is_Primary = 1
                                    AND acpm.Meter_Id IS NOT NULL)
                            OR  (   @Is_Primary = 0
                                    AND acpm.Meter_Id IS NULL))
               GROUP BY
                   cha.Meter_State_Id
                   , cha.Meter_State_Name
                   , ch.Client_Id
                   , ch.Client_Name
                   , ISNULL(@Sitegroup_Id, ch.Sitegroup_Id)
                   , ISNULL(@sitegroup_Name, ch.Sitegroup_Name)
                   , ch.Client_Hier_Id
                   , ch.Site_Id
                   , ch.Site_name
                   , cha.Meter_Id
                   , cha.Meter_Number
                   , emat.EC_Meter_Attribute_Name
                   , emat.EC_Meter_Attribute_Value
                   , cha.Display_Account_Number
                   , cha.Account_Vendor_Id
                   , cha.Account_Vendor_Name
                   , emat.Vendor_Type_Cd
                   , emat.Vendor_Type_Value
                   , cha.Rate_Name
                   , CASE WHEN acpm.Meter_Id IS NOT NULL THEN 1
                         ELSE 0
                     END
           )
        SELECT
            cat.Meter_State_Id
            , cat.Meter_State_Name
            , cat.Client_Id
            , cat.Client_Name
            , cat.Sitegroup_id
            , cat.Sitegroup_Name
            , cat.Client_Hier_Id
            , cat.Site_Id
            , cat.Site_name
            , cat.Meter_Id
            , cat.Meter_Number
            , cat.Attribute_Searched
            , cat.Current_Value
            , cat.Account_Number
            , cat.EC_Meter_Attribute_Id
            , cat.EC_Meter_Attribute_Name
            , cat.Meter_Attribute_Type
            , cat.EC_Meter_Attribute_Value
            , cat.Start_Dt
            , cat.End_Dt
            , cat.Row_Num
            , @Total_Count AS Total_Rows
            , cat.Account_Vendor_Id
            , cat.Account_Vendor_Name
            , ISNULL(@Vendor_Type_Cd, cat.Vendor_Type_Cd) Vendor_Type_Cd
            , ISNULL(@Vendor_Type_Value, cat.Vendor_Type_Value_Search) Vendor_Type_Value
            , cat.Tariff_Rate
            , cat.Is_Primary
            , cat.Meter_Attribute_Type_Update
            , ISNULL(@Vendor_Type_Value_Update, cat.Vendor_Type_Value_Update) Vendor_Type_Value_Update
        FROM
            Cte_Attribute_Tracking cat
        WHERE
            cat.Row_Num BETWEEN @Start_Index
                        AND     @End_Index
        ORDER BY
            Row_Num;
        DROP TABLE #EC_Meter_Attribute_Tracking_Search;
    END;
GO


GRANT EXECUTE ON  [dbo].[EC_Meter_Attribute_Tracking_Search_Insert] TO [CBMSApplication]
GO
