SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.Supplier_Account_Config_Dates_Exists

DESCRIPTION:

INPUT PARAMETERS:
Name								DataType		Default			Description
-------------------------------------------------------------------------------
 @Supplier_Account_Config_Id		INT					
 
 
OUTPUT PARAMETERS:
Name								DataType		Default			Description
-------------------------------------------------------------------------------
	
USAGE EXAMPLES:
-------------------------------------------------------------------------------

Select  top 10 * from Supplier_Account_Config where contract_Id = -1 order by supplier_account_config_ID desc 

exec Supplier_Account_Config_Dates_Exists
    @Account_Id = 1921016
    , @Supplier_Account_Begin_Dt = '2019-11-01'
    , @Supplier_Account_End_Dt = null


exec Supplier_Account_Config_Dates_Exists
    @Account_Id = 1921016
    , @Supplier_Account_Begin_Dt = '2020-11-01'
    , @Supplier_Account_End_Dt = null

AUTHOR INITIALS:
	Initials	Name
-------------------------------------------------------------------------------
	NR			Narayana Reddy	
	
MODIFICATIONS
	Initials	Date			Modification
-------------------------------------------------------------------------------
    NR				2019-06-26		Created For Add contract.   

******/

CREATE PROCEDURE [dbo].[Supplier_Account_Config_Dates_Exists]
    (
        @Account_Id INT
        , @Supplier_Account_Begin_Dt DATE
        , @Supplier_Account_End_Dt DATE
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Is_Overlap_Missing_Contract_Exists BIT = 0;


        SELECT
            @Is_Overlap_Missing_Contract_Exists = 1
        FROM
            dbo.Supplier_Account_Config sac
            INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                ON samm.Supplier_Account_Config_Id = sac.Supplier_Account_Config_Id
        WHERE
            sac.Account_Id = @Account_Id
            AND sac.Contract_Id = -1
            AND (   sac.Supplier_Account_Begin_Dt BETWEEN @Supplier_Account_Begin_Dt
                                                  AND     ISNULL(@Supplier_Account_End_Dt, '9999-12-31')
                    OR  ISNULL(sac.Supplier_Account_End_Dt, '9999-12-31') BETWEEN @Supplier_Account_Begin_Dt
                                                                          AND     ISNULL(
                                                                                      @Supplier_Account_End_Dt
                                                                                      , '9999-12-31')
                    OR  @Supplier_Account_Begin_Dt BETWEEN sac.Supplier_Account_Begin_Dt
                                                   AND     ISNULL(sac.Supplier_Account_End_Dt, '9999-12-31')
                    OR  ISNULL(@Supplier_Account_End_Dt, '9999-12-31') BETWEEN sac.Supplier_Account_Begin_Dt
                                                                       AND     ISNULL(
                                                                                   sac.Supplier_Account_End_Dt
                                                                                   , '9999-12-31'));

        SELECT
            @Is_Overlap_Missing_Contract_Exists AS Is_Overlap_Missing_Contract_Exists;

    END;


GO
GRANT EXECUTE ON  [dbo].[Supplier_Account_Config_Dates_Exists] TO [CBMSApplication]
GO
