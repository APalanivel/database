SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UBM_UPDATE_AVISTA_BATCH_LOG_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@totalActualImages	int       	          	
	@totalActualImageBytes	int       	          	
	@consolidationId	varchar(50)	          	
	@zipFileName   	varchar(200)	          	
	@masterLogId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	@EXPECTED_RECORDS	int       	          	
	@EXPECTED_DOLLARS	decimal(32,16)	          	
	@EXPECTED_IMAGES	int       	          	
	@ACTUAL_RECORDS	int       	          	
	@ACTUAL_DOLLARS	decimal(32,16)	          	
	@UNKNOWN_CLIENT_STATUS	int       	          	
	@CLIENT        	int       	          	

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE          PROCEDURE dbo.UBM_UPDATE_AVISTA_BATCH_LOG_P
	@totalActualImages int,
	@totalActualImageBytes int,
	@consolidationId varchar(50),
	@zipFileName varchar(200),
	@masterLogId int,
	@EXPECTED_RECORDS INT OUTPUT,
	@EXPECTED_DOLLARS DECIMAL(32,16) OUTPUT,
	@EXPECTED_IMAGES INT OUTPUT,
	@ACTUAL_RECORDS INT OUTPUT,
	@ACTUAL_DOLLARS DECIMAL(32,16)OUTPUT,
	@UNKNOWN_CLIENT_STATUS INT OUTPUT,
	@CLIENT INT OUTPUT
	AS
set nocount on
		DECLARE @CLIENT_NAME VARCHAR(200),
			@CLIENT_ID INT,
			@NO_OF_RECORDS INT,
			@TOTAL_EXPECTED_RECORDS INT,
			@TOTAL_EXPECTED_DOLLARS DECIMAL(32,16),
			@TOTAL_EXPECTED_IMAGES INT,
			@TOTAL_EXPECTED_IMAGE_BYTES INT,
			@TOTAL_ACTUAL_RECORDS INT,
			@TOTAL_ACTUAL_DOLLARS DECIMAL(32,16),
			@UBM_BATCH_LOG_ID INT

		SELECT 
			@CLIENT_NAME = CLIENT_NAME, 
			@CLIENT_ID = CLIENT_ID,
			@TOTAL_EXPECTED_RECORDS = SUM (TOTAL_EXPECTED_RECORDS),
			@TOTAL_EXPECTED_DOLLARS = SUM (TOTAL_EXPECTED_DOLLARS),
			@TOTAL_EXPECTED_IMAGES  = SUM (TOTAL_EXPECTED_IMAGES),
			@TOTAL_EXPECTED_IMAGE_BYTES = SUM (TOTAL_EXPECTED_BYTES)

		FROM UBM_AVISTA_CONTROL

		WHERE   CONSOLIDATION_ID = @consolidationId
				AND UBM_BATCH_MASTER_LOG_ID =@masterLogId
		GROUP BY CLIENT_NAME, CLIENT_ID


		SELECT 
			@TOTAL_ACTUAL_RECORDS = COUNT(BILL_ID),
			@TOTAL_ACTUAL_DOLLARS = SUM(SERVICE_AMOUNT) 
		FROM 
			UBM_AVISTA_FEED 

		WHERE 	CONSOLIDATION_ID = @consolidationId
				AND UBM_BATCH_MASTER_LOG_ID = @masterLogId


		select 
			@NO_OF_RECORDS = count(ubm_feed_frequency_id) 
		from 
			ubm_feed_frequency 
		where 
			ubm_client_id = @CLIENT_ID
			and ubm_id=1

--			and ubm_id=(select ubm_id from ubm where ubm_name='Avista')

		IF @NO_OF_RECORDS > 0
			BEGIN
				SELECT @UNKNOWN_CLIENT_STATUS = 0
				INSERT INTO UBM_BATCH_LOG (UBM_BATCH_MASTER_LOG_ID, CLIENT_NAME, CONSOLIDATION_ID, ZIP_FILE_NAME, CLIENT_ID)
				VALUES(@masterLogId, @CLIENT_NAME, @consolidationId, @zipFileName, @CLIENT_ID)	
			END
		ELSE

			BEGIN --/* CASS_TABLE_NAME  is used for storing UNKNOWN Client ID*/
				SELECT @UNKNOWN_CLIENT_STATUS = 1
				INSERT INTO UBM_BATCH_LOG (UBM_BATCH_MASTER_LOG_ID, CLIENT_NAME, CASS_TABLE_NAME, CONSOLIDATION_ID, ZIP_FILE_NAME, CLIENT_ID)
				VALUES(@masterLogId, @CLIENT_NAME, @CLIENT_ID, @consolidationId, @zipFileName, @CLIENT_ID)	
			END
--		SELECT @UBM_BATCH_LOG_ID = @@identity
		SELECT @UBM_BATCH_LOG_ID = scope_identity()
		INSERT INTO UBM_BATCH_LOG_DETAILS (
				UBM_BATCH_LOG_ID, 
				EXPECTED_DATA_RECORDS,
				ACTUAL_DATA_RECORDS,
				EXPECTED_IMAGE_RECORDS,
				ACTUAL_IMAGE_RECORDS,
				EXPECTED_DOLLAR_AMOUNT,
				ACTUAL_DOLLAR_AMOUNT,
				ACTUAL_IMAGE_BYTES,
				EXPECTED_IMAGE_BYTES
				)
			VALUES(	
				@UBM_BATCH_LOG_ID, 
				@TOTAL_EXPECTED_RECORDS,
				@TOTAL_ACTUAL_RECORDS,
				@TOTAL_EXPECTED_IMAGES,
				@totalActualImages,
				@TOTAL_EXPECTED_DOLLARS,
				@TOTAL_ACTUAL_DOLLARS,
				@totalActualImageBytes,
				@TOTAL_EXPECTED_IMAGE_BYTES
				)	

	SELECT 	@expected_records = @total_expected_records,
		@EXPECTED_DOLLARS = @TOTAL_EXPECTED_DOLLARS,
		@EXPECTED_IMAGES  = @TOTAL_EXPECTED_IMAGES,
		@ACTUAL_RECORDS   = @TOTAL_ACTUAL_RECORDS,
		@ACTUAL_DOLLARS   = @TOTAL_ACTUAL_DOLLARS,
		@CLIENT = @CLIENT_ID
-- 	SELECT @EXPECTED_DOLLARS = @TOTAL_EXPECTED_DOLLARS
-- 	SELECT @EXPECTED_IMAGES  = @TOTAL_EXPECTED_IMAGES
-- 	SELECT @ACTUAL_RECORDS   = @TOTAL_ACTUAL_RECORDS
-- 	SELECT @ACTUAL_DOLLARS   = @TOTAL_ACTUAL_DOLLARS
-- 	SELECT @CLIENT = @CLIENT_ID
	/*RETURN @EXPECTED_RECORDS;
	RETURN @EXPECTED_DOLLARS;
	RETURN @EXPECTED_IMAGES;
	RETURN @ACTUAL_RECORDS;
	RETURN @ACTUAL_DOLLARS;*/


RETURN
GO
GRANT EXECUTE ON  [dbo].[UBM_UPDATE_AVISTA_BATCH_LOG_P] TO [CBMSApplication]
GO
