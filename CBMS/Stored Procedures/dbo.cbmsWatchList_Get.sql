SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[cbmsWatchList_Get]
	( @MyAccountId int
	, @watch_list_id int
	)
AS
BEGIN
	set nocount on
	   select watch_list_id
		, user_info_id
		, account_id
		, comments
	     from watch_list
	    where watch_list_id = @watch_list_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsWatchList_Get] TO [CBMSApplication]
GO
