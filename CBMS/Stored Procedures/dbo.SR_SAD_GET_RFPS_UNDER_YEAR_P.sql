SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_RFPS_UNDER_YEAR_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@year          	int       	          	
	@analystId     	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE dbo.SR_SAD_GET_RFPS_UNDER_YEAR_P 
@year int,
@analystId int

as
set nocount on
select 	distinct rfp.sr_rfp_id 

from 	sr_rfp rfp, 
	sr_rfp_account rfpAccount , 
	sr_rfp_checklist checklist

where 	rfp.sr_rfp_id = rfpAccount.sr_rfp_id
	and rfpAccount.sr_rfp_account_id = checklist.sr_rfp_account_id
	and rfpAccount.is_deleted = 0
	and year(checklist.rfp_initiated_date) = @year
	and rfp.initiated_by = 	@analystId

order by rfp.sr_rfp_id
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_RFPS_UNDER_YEAR_P] TO [CBMSApplication]
GO
