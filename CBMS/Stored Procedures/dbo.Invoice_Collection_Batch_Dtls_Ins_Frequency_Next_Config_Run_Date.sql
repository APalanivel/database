SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                    
Name:   [dbo].[Invoice_Collection_Batch_Dtls_Ins_Frequency_Next_Config_Run_Date]             
                    
Description:                    
   This sproc is used to fill the ICQ Batch Table.      
       
  [__$operation] Identifies the Data Changes and it can be one of the following.    
    1 = delete    
       2 = insert    
    3 = update (captured column values are those before the update operation)    
    4 = update (captured column values are those after the update operation)          
    
 Input Parameters:                    
    Name     DataType   Default     Description                      
--------------------------------------------------------------------------------------     
                     
 Output Parameters:                          
    Name     DataType   Default     Description                      
--------------------------------------------------------------------------------------                      
    
 Usage Examples:                        
--------------------------------------------------------------------------------------         
  BEGIN TRAN    
 exec dbo.Invoice_Collection_Batch_Dtls_Ins_Frequency_Next_Config_Run_Date    
  ROLLBACK    
    
Author Initials:                    
    Initials Name                    
--------------------------------------------------------------------------------------                      
 RKV   Ravi Kumar Vegesna    

 Modifications:                    
    Initials        Date  Modification                    
--------------------------------------------------------------------------------------                      
    RKV    2020-03-13  Created For Invoice_Collection.    
     
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Batch_Dtls_Ins_Frequency_Next_Config_Run_Date]
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Record_Cnt INT = 1
            , @Status_Cd INT
            , @Upload_Batch_Id INT = NULL
           
            
            , @ICR_Batch_Type_Cd INT
            , @Batch_Size INT
            , @In_Progress_Status_Cd INT;





        SELECT
            @Batch_Size = CAST(App_Config_Value AS INT)
        FROM
            dbo.App_Config ac
        WHERE
            App_Config_Cd = 'IC_Batch_Size';



       

        SELECT
            @ICR_Batch_Type_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'ICBatchType'
            AND c.Code_Value = 'Freq Next Config Run Dt';



        SELECT
            @Status_Cd = Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Std_Column_Name = 'Invoice_Collection_Queue_Batch_Status_Cd'
            AND c.Code_Value = 'Pending';

        SELECT
            @In_Progress_Status_Cd = Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Std_Column_Name = 'Invoice_Collection_Queue_Batch_Status_Cd'
            AND c.Code_Value = 'In Progress';



        CREATE TABLE #Invoice_Collection_Batch_Dtl
             (
                 Invoice_Collection_Account_Config_Id INT NOT NULL
                 , Collection_Start_Dt DATE NOT NULL
                 , Collection_End_Dt DATE NOT NULL
                 , Row_Num INT IDENTITY(1, 1)
             );



        
        INSERT INTO #Invoice_Collection_Batch_Dtl
             (
                 Invoice_Collection_Account_Config_Id
                 , Collection_Start_Dt
                 , Collection_End_Dt
             )
        SELECT
            icac.Invoice_Collection_Account_Config_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt
        FROM
            dbo.Account_Invoice_Collection_Frequency aicf
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON aicf.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
        WHERE
            CAST(aicf.Next_Config_Run_Date AS date) = CAST(getdate() AS date)
            
        GROUP BY
            icac.Invoice_Collection_Account_Config_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt;




        DELETE
        icbd
        FROM
            #Invoice_Collection_Batch_Dtl icbd
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icac.Invoice_Collection_Account_Config_Id = icbd.Invoice_Collection_Account_Config_Id
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = icac.Account_Id
        WHERE
            cha.Account_Not_Managed = 1;

        BEGIN TRY
            BEGIN TRAN;

            WHILE (@Record_Cnt < (SELECT    MAX(Row_Num)FROM    #Invoice_Collection_Batch_Dtl))
                BEGIN

                    SELECT  @Upload_Batch_Id = NULL;
                    INSERT INTO dbo.Invoice_Collection_Batch
                         (
                             Invoice_Collection_Batch_Type_Cd
                             , Status_Cd
                         )
                    SELECT
                        @ICR_Batch_Type_Cd
                        , @Status_Cd
                    WHERE
                        EXISTS (SELECT  TOP 1   * FROM  #Invoice_Collection_Batch_Dtl);

                    SELECT
                        @Upload_Batch_Id = SCOPE_IDENTITY()
                    WHERE
                        EXISTS (SELECT  TOP 1   * FROM  #Invoice_Collection_Batch_Dtl);

                    INSERT INTO dbo.Invoice_Collection_Batch_Dtl
                         (
                             Invoice_Collection_Account_Config_Id
                             , Invoice_Collection_Batch_Id
                             , Collection_Start_Dt
                             , Collection_End_Dt
                             , Status_Cd
                         )
                    SELECT
                        Invoice_Collection_Account_Config_Id
                        , @Upload_Batch_Id
                        , MAX(Collection_Start_Dt)
                        , MAX(Collection_End_Dt)
                        , @Status_Cd
                    FROM
                        #Invoice_Collection_Batch_Dtl icbd
                    WHERE
                        @Upload_Batch_Id IS NOT NULL
                        AND Row_Num BETWEEN @Record_Cnt
                                    AND     @Record_Cnt + (CAST(@Batch_Size AS INT) - 1)
                        AND NOT EXISTS (   SELECT
                                                1
                                           FROM
                                                Invoice_Collection_Batch_Dtl icbd1
                                           WHERE
                                                icbd1.Invoice_Collection_Account_Config_Id = icbd.Invoice_Collection_Account_Config_Id
                                                AND icbd1.Status_Cd IN ( @Status_Cd, @In_Progress_Status_Cd )
                                                AND icbd1.Invoice_Collection_Batch_Id = @Upload_Batch_Id)
                    GROUP BY
                        Invoice_Collection_Account_Config_Id;

                    SET @Record_Cnt = @Record_Cnt + CAST(@Batch_Size AS INT);
                END;




            DROP TABLE #Invoice_Collection_Batch_Dtl;


            COMMIT TRAN;
        END TRY
        BEGIN CATCH

            IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRAN;
                END;

            EXEC dbo.usp_RethrowError;

        END CATCH;

    END;




    ;


GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Batch_Dtls_Ins_Frequency_Next_Config_Run_Date] TO [CBMSApplication]
GO
