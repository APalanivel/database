SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/******************************************************************************************************    
NAME :	dbo.[Get_EnergyType_Details] 
   
DESCRIPTION: 
To get the Energy Type  details for EPNG and Others

INPUT PARAMETERS:      
Name             DataType               Default        Description      
--------------------------------------------------------------------   
Energy Type			VARCHAR

OUTPUT PARAMETERS:      
Name   DataType  Default Description      
--------------------------------------------------------------------      

USAGE EXAMPLES:    
--------------------------------------------------------------------    
EXEC Get_EnergyType_Details 11231,'EP/NG'

AUTHOR INITIALS:    
Initials	Name    
-------------------------------------------------------------------    
BM			BIJU MOHAN
RT			ROMY TOMAS
   
MODIFICATIONS     
Initials	Date		Modification    
--------------------------------------------------------------------    
RT			02/28/2011	Removed Client table from query
******************************************************************************************************/

CREATE PROCEDURE [dbo].[Get_EnergyType_Details]

(
		@EnergyType AS VARCHAR(200)
)
AS
BEGIN
	
	IF @EnergyType = 'EP/NG' 
	BEGIN
	
		SELECT 
			   ENTITY_ID	AS EP_UOM_ID 
		,	   ENTITY_NAME	AS EP_UOM 
		FROM 
			 ENTITY 
		WHERE ENTITY_TYPE=101
		
		SELECT
			   ENTITY_ID	AS NG_UOM_ID 
		,	   ENTITY_NAME	AS NG_UOM
		FROM
			 ENTITY
		WHERE ENTITY_TYPE=102
		
	END
	ELSE
	BEGIN	
		
		SELECT 
				E.ENTITY_ID AS OTHER_UOM_ID
		,		E.ENTITY_NAME AS OTHER_UOM
		FROM
			dbo.Commodity C 
			INNER JOIN dbo.Entity E 
			ON E.Entity_type = C.UOM_Entity_Type 
		WHERE C.Commodity_Name	=	@EnergyType
		
	END
END
		




GO
GRANT EXECUTE ON  [dbo].[Get_EnergyType_Details] TO [CBMSApplication]
GO
