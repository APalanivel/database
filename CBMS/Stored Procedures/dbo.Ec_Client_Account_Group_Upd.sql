SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Group_Type_Client_Dtl_Sel_By_State_Commodity_Group_Id        
                
Description:                
		This sproc to get the client details based state commodity.        
                             
 Input Parameters:                
    Name						DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
	@State_Id					INT
	@Commodity_Id				INT
	@Ec_Account_Group_Type_Id   INT
	@Start_Dt					DATE
	@End_Dt						DATE
      
 Output Parameters:                      
    Name					  DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
----------------------------------------------------------------------------------------     

  BEGIN TRANSACTION

SELECT * FROM dbo.Ec_Client_Account_Group ecag
WHERE Ec_Client_Account_Group_Id = 5


SELECT * FROM dbo.Ec_Client_Account_Group_Account ecaga
WHERE Ec_Client_Account_Group_Id = 5

EXEC dbo.Ec_Client_Account_Group_upd 
      @Client_Id = 11236
     ,@Ec_Client_Account_Group_Id=5
     ,@Start_Dt = '2013-11-01'
     ,@End_Dt = '2013-12-01'
     ,@Account_Id = 688875
     ,@User_Info_Id = 49

SELECT * FROM dbo.Ec_Client_Account_Group ecag
WHERE Ec_Client_Account_Group_Id = 5

SELECT * FROM dbo.Ec_Client_Account_Group_Account ecaga
WHERE Ec_Client_Account_Group_Id = 5 

ROLLBACK TRANSACTION
     
   
Author Initials:                
    Initials		Name                
----------------------------------------------------------------------------------------                  
	NR				Narayana Reddy                 
 Modifications:                
    Initials        Date			Modification                
----------------------------------------------------------------------------------------                  
    NR				2016-11-15		Created For MAINT-4563.           
               
******/    
CREATE PROCEDURE [dbo].[Ec_Client_Account_Group_Upd]
      ( 
       @Client_Id INT
      ,@Ec_Client_Account_Group_Id INT
      ,@Start_Dt DATE = NULL
      ,@End_Dt DATE = NULL
      ,@Account_Id NVARCHAR(MAX)
      ,@User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON
      
      UPDATE
            dbo.Ec_Client_Account_Group
      SET   
            Start_Dt = @Start_Dt
           ,End_Dt = @End_Dt
           ,Updated_User_Id = @User_Info_Id
           ,Last_Change_Ts = GETDATE()
      WHERE
            Ec_Client_Account_Group_Id = @Ec_Client_Account_Group_Id     
     
      
      
      DELETE FROM
            Ec_Client_Account_Group_Account
      WHERE
            Ec_Client_Account_Group_Id = @Ec_Client_Account_Group_Id
      
      INSERT      INTO dbo.Ec_Client_Account_Group_Account
                  ( 
                   Ec_Client_Account_Group_Id
                  ,Account_Id
                  ,Created_User_Id
                  ,Created_Ts )
                  SELECT
                        @Ec_Client_Account_Group_Id
                       ,us.Segments
                       ,@User_Info_Id
                       ,GETDATE()
                  FROM
                        dbo.ufn_split(@Account_Id, ',') us
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          Ec_Client_Account_Group_Account ecaga
                                     WHERE
                                          ecaga.Ec_Client_Account_Group_Id = @Ec_Client_Account_Group_Id
                                          AND ecaga.Account_Id = us.Segments )
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.Ec_Client_Account_Group ecag
                                     WHERE
                                          ecag.Ec_Client_Account_Group_Id = @Ec_Client_Account_Group_Id )                  
               
END

;
GO
GRANT EXECUTE ON  [dbo].[Ec_Client_Account_Group_Upd] TO [CBMSApplication]
GO
