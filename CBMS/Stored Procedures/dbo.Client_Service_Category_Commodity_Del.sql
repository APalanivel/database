SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.Client_Service_Category_Commodity_Del


DESCRIPTION: 

	Used to Delete data from the Client_Service_Category_Commodity table.

INPUT PARAMETERS:    
      Name					DataType          Default     Description    
------------------------------------------------------------------    
	  @Client_Service_Category_Id   int

OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
begin tran
Exec Client_Service_Category_Del  17
rollback tran
select * from Client_Service_Category where Client_Service_Category_Id = 17


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan G

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	HG			11/10/2011	Created

******/
CREATE PROCEDURE dbo.Client_Service_Category_Commodity_Del
      (
       @Client_Service_Category_Id INT )
AS
BEGIN

      SET NOCOUNT ON

      DELETE
            dbo.Client_Service_Category_Commodity
      WHERE
            Client_Service_Category_Id = @Client_Service_Category_Id

END
GO
GRANT EXECUTE ON  [dbo].[Client_Service_Category_Commodity_Del] TO [CBMSApplication]
GO
