SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    Trade.Deal_Ticket_Internal_Approver_Dtl_Sel_By_Deal_Ticket
   
    
DESCRIPTION:   
   
    
INPUT PARAMETERS:    
      Name          DataType       Default        Description    
-----------------------------------------------------------------------------    
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec Trade.Deal_Ticket_Internal_Approval_Pending_Sel  
	Exec Trade.Deal_Ticket_Internal_Approver_Dtl_Sel_By_Deal_Ticket '333,561' 
	
	
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	RR          Raghu Reddy

    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	RR          2019-03-28	GRM Proejct.


     
    
******/

CREATE PROC [Trade].[Deal_Ticket_Internal_Approver_Dtl_Sel_By_Deal_Ticket]
    (
        @Deal_Ticket_Id VARCHAR(MAX) = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            dt.Deal_Ticket_Id
            , ISNULL(
                  CASE WHEN LEN(dir.Dir_EMAIL_ADDRESS) > 0 THEN
                           LEFT(dir.Dir_EMAIL_ADDRESS, LEN(dir.Dir_EMAIL_ADDRESS) - 1)
                      ELSE dir.Dir_EMAIL_ADDRESS
                  END, CASE WHEN LEN(ui.EMAIL_ADDRESS) > 0 THEN LEFT(ui.EMAIL_ADDRESS, LEN(ui.EMAIL_ADDRESS) - 1)
                           ELSE ui.EMAIL_ADDRESS
                       END) AS EMAIL_ADDRESS
            , ch.Client_Name
            , ui2.EMAIL_ADDRESS AS Initiator_Email
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dtch.Deal_Ticket_Id = dt.Deal_Ticket_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
            INNER JOIN Trade.Workflow_Status_Map wsm
                ON chws.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
            INNER JOIN Trade.Workflow_Status ws
                ON wsm.Workflow_Status_Id = ws.Workflow_Status_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Id = dt.Client_Id
                   AND  ch.Client_Hier_Id = dtch.Client_Hier_Id
            CROSS APPLY
        (   SELECT
                ui.EMAIL_ADDRESS + ','
            FROM
                dbo.USER_INFO ui
            WHERE
                dt.Queue_Id = ui.QUEUE_ID
            GROUP BY
                ui.EMAIL_ADDRESS
            FOR XML PATH('')) ui(EMAIL_ADDRESS)
            CROSS APPLY
        (   SELECT
                regdir.EMAIL_ADDRESS + ','
            FROM
                dbo.Geographic_Region_Permission_Info_Map grp
                INNER JOIN dbo.GROUP_INFO_PERMISSION_INFO_MAP gipi
                    ON grp.Permission_Info_Id = gipi.PERMISSION_INFO_ID
                INNER JOIN dbo.USER_INFO_GROUP_INFO_MAP uigi
                    ON gipi.GROUP_INFO_ID = uigi.GROUP_INFO_ID
                INNER JOIN dbo.Geographic_Region_Country_Map grc
                    ON grp.Geographic_Region_Id = grc.Geographic_Region_Id
                INNER JOIN dbo.USER_INFO regdir
                    ON uigi.USER_INFO_ID = regdir.USER_INFO_ID
            WHERE
                ch.Country_Id = grc.COUNTRY_ID
            GROUP BY
                regdir.EMAIL_ADDRESS
            FOR XML PATH('')) dir(Dir_EMAIL_ADDRESS)
            LEFT JOIN dbo.USER_INFO ui2
                ON dt.Created_User_Id = ui2.USER_INFO_ID
        WHERE
            chws.Is_Active = 1
            AND ws.Workflow_Status_Name = 'Pending Internal Approval'
            AND (   @Deal_Ticket_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Deal_Ticket_Id, ',') us
                                   WHERE
                                        us.Segments = dt.Deal_Ticket_Id))
        GROUP BY
            dt.Deal_Ticket_Id
            , ISNULL(
                  CASE WHEN LEN(dir.Dir_EMAIL_ADDRESS) > 0 THEN
                           LEFT(dir.Dir_EMAIL_ADDRESS, LEN(dir.Dir_EMAIL_ADDRESS) - 1)
                      ELSE dir.Dir_EMAIL_ADDRESS
                  END, CASE WHEN LEN(ui.EMAIL_ADDRESS) > 0 THEN LEFT(ui.EMAIL_ADDRESS, LEN(ui.EMAIL_ADDRESS) - 1)
                           ELSE ui.EMAIL_ADDRESS
                       END)
            , ch.Client_Name
            , ui2.EMAIL_ADDRESS;

    END;





GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Internal_Approver_Dtl_Sel_By_Deal_Ticket] TO [CBMSApplication]
GO
