SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[cbmsCuInvoiceServiceMonth_Get]
	( @MyAccountId int
	, @cu_invoice_service_month_id int
	)
AS
BEGIN

	   select cs.cu_invoice_service_month_id
		, cs.cu_invoice_id
		, cs.service_month
	     from cu_invoice_service_month cs with (nolock)
	    where cs.cu_invoice_service_month_id = @cu_invoice_service_month_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCuInvoiceServiceMonth_Get] TO [CBMSApplication]
GO
