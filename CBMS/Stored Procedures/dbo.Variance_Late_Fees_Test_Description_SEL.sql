
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********       
NAME:  dbo.Variance_Late_Fees_Test_Description_SEL      
     
DESCRIPTION:  Used to select variance Late fees test description      
    
INPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
    @Account_Id			INT
            
        
OUTPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:       
    
	EXEC dbo.Variance_Late_Fees_Test_Description_SEL  12345   
  
     
    
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
NK  Nageswara Rao Kosuri
    
Initials	Date		Modification      
------------------------------------------------------------      
NK			01/28/2010  created    
HG			2016-12-08	Added the account_id join on VRDAO table
******/      
    
CREATE PROCEDURE [dbo].[Variance_Late_Fees_Test_Description_SEL] @Account_Id INT
AS
BEGIN

      SET NOCOUNT ON;    
     
      SELECT
		DISTINCT
            cd.Code_Value AS Category
           ,ISNULL(vrdac.Test_Description, vrd.Test_Description) AS Test_Description
      FROM
            dbo.Variance_Rule_Dtl vrd
            INNER JOIN dbo.Variance_Rule vr
                  ON vrd.Variance_Rule_Id = vr.Variance_Rule_Id
            INNER JOIN dbo.Variance_Test_Master vt
                  ON vt.Variance_Test_Id = vr.Variance_Test_Id
            INNER JOIN dbo.Variance_Test_Commodity_Map map
                  ON map.Variance_Test_Id = vt.Variance_Test_Id
            INNER JOIN dbo.Variance_Consumption_Level vcl
                  ON vcl.Variance_Consumption_Level_Id = vrd.Variance_Consumption_Level_Id
                     AND vcl.Commodity_Id = map.Commodity_Id
            INNER JOIN dbo.Account_Variance_Consumption_Level avcl
                  ON avcl.Variance_Consumption_Level_Id = vcl.Variance_Consumption_Level_Id
            INNER JOIN dbo.ACCOUNT acc
                  ON acc.ACCOUNT_ID = avcl.ACCOUNT_ID
                     AND vrd.Is_Data_Entry_Only = ISNULL(acc.Is_Data_Entry_Only, 0)
            INNER JOIN dbo.Variance_Parameter_Baseline_Map vpbmap
                  ON vr.Variance_Baseline_Id = vpbmap.Variance_Baseline_Id
            INNER JOIN dbo.Variance_Parameter vp
                  ON vp.Variance_Parameter_Id = vpbmap.Variance_Parameter_Id
            INNER JOIN dbo.Code cd
                  ON cd.Code_Id = vp.Variance_Category_Cd
            LEFT JOIN dbo.Variance_Rule_Dtl_Account_Override vrdac
                  ON vrdac.Variance_Rule_Dtl_Id = vrd.Variance_Rule_Dtl_Id
                     AND vrdac.ACCOUNT_ID = avcl.ACCOUNT_ID
      WHERE
            avcl.ACCOUNT_ID = @Account_Id
            AND cd.Code_Value = 'Late Fees'
            AND vrd.IS_Active = 1;

END;

;
GO

GRANT EXECUTE ON  [dbo].[Variance_Late_Fees_Test_Description_SEL] TO [CBMSApplication]
GO
