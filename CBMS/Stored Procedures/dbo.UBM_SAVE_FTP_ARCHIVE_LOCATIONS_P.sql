SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE     PROCEDURE dbo.UBM_SAVE_FTP_ARCHIVE_LOCATIONS_P 
@userId varchar,
@sessionId varchar,
@locationName varchar (100),
@entityId int
as

	set nocount on


UPDATE ENTITY SET ENTITY_NAME =@locationName where entity_id=@entityId
GO
GRANT EXECUTE ON  [dbo].[UBM_SAVE_FTP_ARCHIVE_LOCATIONS_P] TO [CBMSApplication]
GO
