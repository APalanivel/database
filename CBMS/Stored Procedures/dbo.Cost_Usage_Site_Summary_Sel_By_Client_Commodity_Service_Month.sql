
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
	dbo.Cost_Usage_Site_Summary_Sel_By_Client_Commodity_Service_Month
	
 DESCRIPTION:
 INPUT PARAMETERS:  
	Name				DataType  Default Description  
------------------------------------------------------------  
	@Client_Id		INT
	@Commodity_id		INT
	@Begin_Dt			DATETIME
	@End_Dt			DATETIME
	@Currency_unit_id	INT
	@UOM_Id			INT
	@Site_Not_Managed	INT			0
	@country_id		INT			NULL	
	 
 OUTPUT PARAMETERS:
 Name   DataType  Default Description
------------------------------------------------------------

 USAGE EXAMPLES:
------------------------------------------------------------
	EXEC dbo.Cost_Usage_Site_Summary_Sel_By_Client_Commodity_Service_Month 10044,67,'01/01/2009','12/1/2009',39,1563,NULL
	EXEC dbo.Cost_Usage_Site_Summary_Sel_By_Client_Commodity_Service_Month 235,67,'01/01/2010','12/1/2010',3,1409,NULL


AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 SSR		Sharad srivastava
 HG			Harihara Suthan G
 AP			Athmaram Pabbathi
 BCH	    Balaraju Chalumuri
 RR			Raghu Reddy
 
 MODIFICATIONS   
 Initials	Date		Modification  
------------------------------------------------------------  
 SSR		02/08/2010  Created	
 HG			03/04/2010  Comma was not added after Site_Name column in the select from CTE added it.
 SSR		03/11/2010  Unit_Cost Column with Decimal(32,16) has been added in Result set.	
 SKA		03/12/2010  Used Left join for Cost_Usage_Site_Dtl instead of Inner join
 SSR		03/12/2010  Changed default value of @Site_Not_Managed 	from 0 to NULL
 HG			03/18/2010  Unnecessary Common table expression removed.
 SSR		03/22/2010  row_type column added in select Clause
 HG			04/06/2010  Sorting order modified first Division then by Site.
 AP			09/12/2011  Removed hardcoded buckets and used table variable, Code table and used dbo.Cost_usage_Bucket_Sel_By_Commodity SP to load buckets.						
 BCH		2012-03-22  Removed site,sitegroup,address and state tables instead core.client_hier table.
						  Replaced s.site_id=cus.site_id with s.Client_Hier_Id = cus.Client_Hier_Id
 AP			2012-03-26  Removed multiple CTEs and Added Client_Hier_Id in the result result  
 RR			2012-07-09	Removed the script replcing null values with zeros for Total_Cost, Volume, Unit_Cost
******/
CREATE PROCEDURE dbo.Cost_Usage_Site_Summary_Sel_By_Client_Commodity_Service_Month
      ( 
       @Client_Id INT
      ,@Commodity_id INT
      ,@Begin_Dt DATETIME
      ,@End_Dt DATETIME
      ,@Currency_unit_id INT
      ,@UOM_Id INT
      ,@Site_Not_Managed INT = NULL
      ,@country_id INT = NULL )
AS 
BEGIN  
      SET NOCOUNT ON

      DECLARE @Cost_Usage_Bucket_Id TABLE
            ( 
             Bucket_Master_Id INT PRIMARY KEY CLUSTERED
            ,Bucket_Name VARCHAR(200)
            ,Bucket_Type VARCHAR(25) )


      INSERT      INTO @Cost_Usage_Bucket_Id
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Bucket_Type )
                  EXEC dbo.Cost_usage_Bucket_Sel_By_Commodity 
                        @Commodity_id = @Commodity_Id ;
                        
      WITH  Cte_Site_IP_Dtl
              AS ( SELECT
                        ch.client_hier_id
                       ,ch.Client_Currency_Group_Id AS Currency_Group_Id
                       ,ip.Service_Month
                       ,rtrim(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' Site_Name
                       ,ch.Sitegroup_ID AS Division_Id
                       ,ch.Sitegroup_Name Division_Name
                       ,case WHEN ch.site_not_managed = 1 THEN 'Inactive'
                             ELSE 'Active'
                        END Active
                       ,max(case WHEN ip.Is_Expected = 1
                                      AND ip.Is_Received = 1 THEN 1
                                 ELSE 0
                            END) Is_Complete
                       ,max(convert(INT, ip.Is_Received)) Is_published
                       ,max(case WHEN ip.Recalc_Under_Review = 1
                                      OR ip.Variance_Under_Review = 1 THEN 1
                                 ELSE 0
                            END) Is_Under_Review
                   FROM
                        core.client_hier ch
                        LEFT OUTER JOIN dbo.Invoice_Participation ip
                              ON ch.Site_ID = ip.Site_ID
                                 AND ip.Service_Month BETWEEN @Begin_Dt AND @End_Dt
                   WHERE
                        ch.Client_id = @Client_Id
                        AND ch.Site_Id > 0
                        AND ( @Site_Not_Managed IS NULL
                              OR ch.Site_Not_Managed = @Site_Not_Managed )
                        AND ( @Country_Id IS NULL
                              OR ch.Country_Id = @Country_Id )
                   GROUP BY
                        ch.Client_Hier_Id
                       ,ch.Sitegroup_ID
                       ,ch.Sitegroup_Name
                       ,ch.City
                       ,ch.State_Name
                       ,ch.Site_Name
                       ,ch.Site_Not_Managed
                       ,ch.Client_Currency_Group_Id
                       ,ip.Service_Month)
            SELECT
                  0 AS Row_Type
                 ,s.Client_Hier_Id
                 ,s.Site_Name
                 ,s.Active
                 ,s.Division_ID
                 ,s.Division_Name
                 ,sum(case WHEN CUB.Bucket_Type = 'Charge' THEN cus.Bucket_Value * cc.Conversion_Factor
                      END) AS Total_Cost
                 ,sum(case WHEN CUB.Bucket_Type = 'Determinant' THEN cus.Bucket_Value * uc.Conversion_Factor
                      END) AS Volume
                 ,sum(case WHEN CUB.Bucket_Type = 'Charge' THEN cus.Bucket_Value * cc.Conversion_Factor
                      END) / nullif(sum(case WHEN CUB.Bucket_Type = 'Determinant' THEN cus.Bucket_Value * uc.Conversion_Factor
                                        END), 0) AS Unit_Cost
                 ,isnull(max(s.Is_Complete), 0) AS Is_Complete
                 ,isnull(max(s.Is_published), 0) AS Is_published
                 ,isnull(max(s.Is_Under_Review), 0) AS Is_Under_Review
            FROM
                  Cte_Site_IP_Dtl s
                  LEFT JOIN ( dbo.Cost_Usage_Site_Dtl cus
                              JOIN @Cost_Usage_Bucket_Id CUB
                                    ON CUB.Bucket_Master_Id = cus.Bucket_Master_Id )
                              ON s.Client_Hier_Id = cus.Client_Hier_Id
                                 AND s.Service_Month = cus.Service_Month
                                 AND cus.Service_Month BETWEEN @Begin_Dt AND @End_Dt
                  LEFT JOIN dbo.Consumption_Unit_Conversion uc
                        ON uc.Base_Unit_ID = cus.UOM_Type_Id
                           AND uc.Converted_Unit_ID = @UOM_Id
                  LEFT JOIN dbo.Currency_Unit_Conversion cc
                        ON cc.currency_group_id = s.Currency_Group_Id
                           AND cc.Base_Unit_Id = cus.Currency_Unit_ID
                           AND cc.Converted_Unit_Id = @Currency_Unit_Id
                           AND cc.Conversion_Date = cus.Service_month
            GROUP BY
                  s.Client_Hier_Id
                 ,s.DIVISION_ID
                 ,s.Division_Name
                 ,s.site_name
                 ,s.Active
            ORDER BY
                  s.Division_Name
                 ,s.Site_Name
END
;
GO


GRANT EXECUTE ON  [dbo].[Cost_Usage_Site_Summary_Sel_By_Client_Commodity_Service_Month] TO [CBMSApplication]
GO
