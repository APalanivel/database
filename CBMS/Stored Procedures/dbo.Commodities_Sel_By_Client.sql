SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
NAME:  
	dbo.Commodities_Sel_By_Client

DESCRIPTION:  
	Used to get all commodity as per Client_ID & @Commodity_Service_Type

INPUT PARAMETERS:  
Name      DataType		Default		Description  
------------------------------------------------------------  
  @Client_Id INT
  @Commodity_Service_Type VARCHAR(25)       
       
OUTPUT PARAMETERS:  
Name      DataType		Default		Description  
------------------------------------------------------------  

USAGE EXAMPLES:  
------------------------------------------------------------
EXEC dbo.Commodities_Sel_By_Client 108,'Invoice'
EXEC dbo.Commodities_Sel_By_Client 218,'Invoice'
	
AUTHOR INITIALS:  
Initials	Name  
------------------------------------------------------------  
SKA			Shobhit Kr Agrawal

MODIFICATIONS   
Initials	Date		Modification  
------------------------------------------------------------  
SKA						created
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.Commodities_Sel_By_Client
   (    
    @Client_Id INT
   ,@Commodity_Service_Type VARCHAR(25)  
   )    
 AS        

BEGIN        
         
 SET NOCOUNT ON;        

	SELECT 
		cc.commodity_id   
		,com.Commodity_Name    
	FROM 
		Core.Client_Commodity cc  
	INNER JOIN dbo.Commodity com  
		ON cc.Commodity_Id = com.Commodity_Id
	INNER JOIN Code cd
		ON cd.Code_Id = cc.Commodity_Service_Cd
	WHERE 
		cc.CLIENT_ID = @Client_Id
		AND cd.Code_Value = @Commodity_Service_Type            
END
GO
GRANT EXECUTE ON  [dbo].[Commodities_Sel_By_Client] TO [CBMSApplication]
GO
