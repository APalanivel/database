SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.GET_HEDGE_TYPES_P
@userId varchar,
@sessionId varchar
as
	set nocount on
	select	entity.ENTITY_ID,
		entity.ENTITY_NAME
	
	from	ENTITY entity
		
	
	where	entity.ENTITY_TYPE=273 AND
		entity.ENTITY_NAME NOT LIKE 'does not hedge'
GO
GRANT EXECUTE ON  [dbo].[GET_HEDGE_TYPES_P] TO [CBMSApplication]
GO
