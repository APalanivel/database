SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE    PROCEDURE [dbo].[UPDATE_FORECAST_CONTRACT_VOLUMES_P]
   @userId varchar(10)
 , @sessionId varchar(20)
 , @siteId int
 ,
--@asOfDate datetime,
   @clientId int
AS 
   set nocount on
   declare @cnt int
    , @contractVolumeId int
    , @editContractVolumeId int
    , @previousContractVolumeId int
    , @previousEditContractVolumeId int
    , @rm_forecast_volume_id int
    , @site_id int
    , @division_id int
    , @volume int
    , @month_identifier datetime
    , @volume_type_id int
    , @maxAsOfDate datetime
    , @forecast_year int

   select   @contractVolumeId = entity_id
   from     entity 
   where    entity_name = 'Actual_Contract'
            and entity_type = 285
   select   @editContractVolumeId = entity_id
   from     entity 
   where    entity_name = 'Editable_Actual_Contract'
            and entity_type = 285
   select   @previousContractVolumeId = entity_id
   from     entity 
   where    entity_name = 'Previous_Contract'
            and entity_type = 285
   select   @previousEditContractVolumeId = entity_id
   from     entity 
   where    entity_name = 'Previous_Editable_Actual_Contract'
            and entity_type = 285

   update   rm_forecast_volume_details 
   set      volume_type_id = @editContractVolumeId
   where    site_id = @siteId
            and volume_type_id = @contractVolumeId
            and rm_forecast_volume_id in (
            select   rm_forecast_volume_id
            from     rm_forecast_volume 
            where    client_id = @clientId ) 

   update   rm_forecast_volume_details 
   set      volume_type_id = @previousEditContractVolumeId
   where    site_id = @siteId
            and volume_type_id = @previousContractVolumeId
            and rm_forecast_volume_id in (
            select   rm_forecast_volume_id
            from     rm_forecast_volume  
            where    client_id = @clientId ) 

GO
GRANT EXECUTE ON  [dbo].[UPDATE_FORECAST_CONTRACT_VOLUMES_P] TO [CBMSApplication]
GO
