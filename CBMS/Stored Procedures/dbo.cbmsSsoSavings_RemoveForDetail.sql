SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsSsoSavings_RemoveForDetail

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@sso_savings_id	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE   PROCEDURE [dbo].[cbmsSsoSavings_RemoveForDetail]
	( @MyAccountId int
	, @sso_savings_id int
	)
AS
BEGIN

	set nocount on

	  delete sso_savings_detail
	   where sso_savings_id = @sso_savings_id


END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoSavings_RemoveForDetail] TO [CBMSApplication]
GO
