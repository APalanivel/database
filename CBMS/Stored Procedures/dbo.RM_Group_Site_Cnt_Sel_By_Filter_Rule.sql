SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:      
   dbo.RM_Group_Site_Cnt_Sel_By_Filter_Rule  
           
 DESCRIPTION:             
       to insert values for user report site table given by the filter rule.  
           
 INPUT PARAMETERS:            
 Name						DataType				Default				Description            
 ---------------------------------------------------------------------------------              
 @Rule_Condition			VARCHAR(25)  
 @Client_ID					INT        
 @tvp_Sites_To_Risk_Management_Group			tvp_Account_Site_Filter    
           
 OUTPUT PARAMETERS:            
 Name    DataType       Default          Description            
 ---------------------------------------------------------------------------------              
           
 USAGE EXAMPLES:            
 ---------------------------------------------------------------------------------        
       
DECLARE @tvp_Sites_To_RM_Group tvp_Sites_To_RM_Group
INSERT INTO @tvp_Sites_To_RM_Group VALUES ('Supplier', '=', '16208')
Exec dbo.RM_Group_Site_Cnt_Sel_By_Filter_Rule 'All', 235, @tvp_Sites_To_RM_Group
          
 AUTHOR INITIALS:          
 Initials      Name          
 ---------------------------------------------------------------------------------              
 PR			   Pramod Reddy
           
 MODIFICATIONS          
 Initials		 Date      Modification          
 ---------------------------------------------------------------------------------              
 PR				27-07-2018		Created For Risk managemnet Site Count.		
								
******/

CREATE PROCEDURE [dbo].[RM_Group_Site_Cnt_Sel_By_Filter_Rule]
    (
        @Rule_Condition VARCHAR(25)
        , @Client_Id INT
        , @tvp_Sites_To_RM_Group dbo.tvp_Sites_To_RM_Group READONLY
    )
AS
    BEGIN

        SET NOCOUNT ON;
        CREATE TABLE #SiteList
             (
                 Site_id INT
             );
        DECLARE
            @SQl_Stmt VARCHAR(MAX)
            , @Wstmt VARCHAR(MAX) = ''
            , @Gstmt VARCHAR(4000) = ''
            , @Condition VARCHAR(25);



        IF @Rule_Condition = 'All'
            BEGIN
                SET @Condition = 'AND';
            END;
        ELSE
            BEGIN
                SET @Condition = 'OR';
            END;

        SELECT
            @Wstmt = @Wstmt
                     + CASE WHEN Filter_Name = 'Country' THEN 'ch.Country_Id'
                           WHEN Filter_Name = 'State' THEN 'ch.State_Id'
                           WHEN Filter_Name = 'Supplier' THEN
                               '(cha.Account_type = ''Supplier'' and cha.Account_Vendor_Id'
                           WHEN Filter_Name = 'Utility' THEN
                               '(cha.Account_type = ''Utility'' and cha.Account_Vendor_Id'
                           WHEN Filter_Name = 'Site Status' THEN 'ch.Site_Not_Managed'
                       END + ' ' + Filter_Condition + ' '
                     + CASE WHEN Filter_Name = 'Supplier'
                                 OR Filter_Name = 'Utility' THEN Filter_Value + ')'
                           ELSE Filter_Value
                       END + ' ' + @Condition + ' '
        FROM
            @tvp_Sites_To_RM_Group;


        IF @Wstmt <> ''
            SET @Wstmt = LEFT(@Wstmt, LEN(@Wstmt) - 3) + ')';

        SET @Gstmt = 'GROUP BY ch.Site_Id';

        SET @SQl_Stmt = 'INSERT #SiteList( Site_Id)  
			SELECT
				ch.Site_Id
			FROM
				core.Client_Hier ch ';

        IF EXISTS (   SELECT
                            1
                      FROM
                            @tvp_Sites_To_RM_Group tsf
                      WHERE
                            tsf.Filter_Name IN ( 'Supplier', 'Utility' ))
            BEGIN
                SET @SQl_Stmt = @SQl_Stmt
                                + '  
        LEFT JOIN core.Client_Hier_Account cha    
        ON ch.Client_Hier_Id = cha.Client_Hier_Id';
            END;

        SET @SQl_Stmt = @SQl_Stmt + '  
       WHERE  ( ch.Client_Id = ' + CAST(@Client_Id AS VARCHAR) + '   
      AND ch.Site_Id > 0)   
     '  ;

        SET @SQl_Stmt = @SQl_Stmt + CASE WHEN @Wstmt <> '' THEN 'AND (' + @Wstmt
                                        ELSE ''
                                    END + ' GROUP BY ch.Site_Id';



        EXEC (@SQl_Stmt);



        SELECT
            
             COUNT(1) AS Site_Count
        FROM
            #SiteList sl
     

        DROP TABLE #SiteList;

    END;
    ;


GO
GRANT EXECUTE ON  [dbo].[RM_Group_Site_Cnt_Sel_By_Filter_Rule] TO [CBMSApplication]
GO
