SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        dbo.RM_Client_Comment_Ins                    
                      
Description:                      
        
                      
Input Parameters:                      
    Name				DataType    Default     Description                        
--------------------------------------------------------------------------------
	@Client_Id			INT
    @Comment_Id			INT
    @Cbms_Image_Id		INT			NULL
    @User_Info_Id		INT
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	  
                    
Usage Examples:                          
--------------------------------------------------------------------------------
	DECLARE @Comment_Dt DATE = GETDATE()
	DECLARE @Comment_Id INT
	EXEC dbo.Comment_Ins  @Comment_Type_CD = 102693, @Comment_User_Info_Id = 16
     , @Comment_Dt =  @Comment_Dt, @Comment_Text = 'Client onboarded'
     ,@Comment_Id = @Comment_Id OUT
     
     SELECT @Comment_Id --39872451,39872452
	EXEC dbo.RM_Client_Comment_Ins 11236, 39872451,NULL,16
	EXEC dbo.RM_Client_Comment_Ins 11236, 39872452,NULL,16
		                     
 Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy   
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			2019-01-28     Global Risk Management - Created 
                     
******/
CREATE PROCEDURE [dbo].[RM_Client_Comment_Ins]
      ( 
       @Client_Id INT
      ,@Comment_Id INT
      ,@Cbms_Image_Id INT = NULL
      ,@User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;
      
      INSERT      INTO dbo.RM_Client_Comment
                  ( 
                   Client_Id
                  ,Comment_Id
                  ,Cbms_Image_Id
                  ,Created_User_Id
                  ,Created_Ts
                  ,Updated_User_Id
                  ,Last_Change_Ts )
      VALUES
                  ( 
                   @Client_Id
                  ,@Comment_Id
                  ,@Cbms_Image_Id
                  ,@User_Info_Id
                  ,GETDATE()
                  ,@User_Info_Id
                  ,GETDATE() )
                  
END;
GO
GRANT EXECUTE ON  [dbo].[RM_Client_Comment_Ins] TO [CBMSApplication]
GO
