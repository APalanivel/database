SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Contract_GetExpirationStats_By_SecurityRole

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Security_Role_ID    	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
USE CBMS
EXEC dbo.Contract_GetExpirationStats_By_SecurityRole 99

EXEC dbo.Contract_GetExpirationStats_By_SecurityRole 121 --(Widget)
EXEC dbo.Contract_Search_By_SecurityRole 121,NULL,NULL,0,NULL,NULL,NULL,'4/6/2015',NULL,0 --(Detailed)

EXEC dbo.Contract_GetExpirationStats_By_SecurityRole 616 --(Widget)
EXEC dbo.Contract_Search_By_SecurityRole 616,NULL,NULL,0,NULL,NULL,NULL,'4/6/2015',NULL,0 --(Detailed)


AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 NR			Narayana Reddy
 
 MODIFICATIONS   
 Initials	Date		Modification  
------------------------------------------------------------  
 NR			2020-02-11		MAINT-9861 - Moved Sproc From DV to CBMS.	

******/

CREATE PROCEDURE [dbo].[Contract_GetExpirationStats_By_SecurityRole]
     (
         @Security_Role_ID INT
     )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @today DATETIME
            , @90day DATETIME
            , @180day DATETIME
            , @1year DATETIME;

        SET @today = GETDATE();
        SET @90day = DATEADD(mm, 3, @today);
        SET @180day = DATEADD(mm, 6, @today);
        SET @1year = DATEADD(yy, 1, @today);

        WITH CTE_Account_Details
        AS (
               SELECT
                    ch.Site_Id
                    , c.CONTRACT_ID
                    , c.CONTRACT_END_DATE
               FROM
                    dbo.Security_Role_Client_Hier srch
                    INNER JOIN Core.Client_Hier_Account cha
                        ON cha.Client_Hier_Id = srch.Client_Hier_Id
                    INNER JOIN Core.Client_Hier ch
                        ON ch.Client_Hier_Id = cha.Client_Hier_Id
                    INNER JOIN dbo.CONTRACT c
                        ON c.CONTRACT_ID = cha.Supplier_Contract_ID
               WHERE
                    srch.Security_Role_Id = @Security_Role_ID
                    AND ch.Site_Not_Managed = 0
                    AND ch.Site_Id > 0
                    AND cha.Account_Type = 'Supplier'
                    AND c.CONTRACT_END_DATE > @today
               GROUP BY
                   ch.Site_Id
                   , c.CONTRACT_ID
                   , c.CONTRACT_END_DATE
           )
        SELECT
            '90day' = SUM(CASE WHEN cad.CONTRACT_END_DATE BETWEEN @today
                                                          AND     @90day THEN 1
                              ELSE 0
                          END)
            , '180day' = SUM(CASE WHEN cad.CONTRACT_END_DATE BETWEEN @90day
                                                             AND     @180day THEN 1
                                 ELSE 0
                             END)
            , '1year' = SUM(CASE WHEN cad.CONTRACT_END_DATE BETWEEN @180day
                                                            AND     @1year THEN 1
                                ELSE 0
                            END)
            , 'morethan1year' = SUM(CASE WHEN cad.CONTRACT_END_DATE > @1year THEN 1
                                        ELSE 0
                                    END)
        FROM
            CTE_Account_Details cad;

    END;


GO
GRANT EXECUTE ON  [dbo].[Contract_GetExpirationStats_By_SecurityRole] TO [CBMSApplication]
GO
