
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.State_Sel_By_Client_Id_State_Name            
                        
 DESCRIPTION:                        
			To get the State_Id based on State_Name.                        
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@State_Name					VARCHAR(200)                     
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 
     EXEC [dbo].[State_Sel_By_Client_Id_State_Name] @State_Name = 'I'
		,@Account_Vendor_Id= 640  
		
	EXEC State_Sel_By_Client_Id_State_Name @State_Name = 'a'
                       
 AUTHOR INITIALS:        
       
 Initials             Name        
---------------------------------------------------------------------------------------------------------------                      
 RKV                  Ravi Kumar Vegesna
 NR					  Narayana Reddy
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 RKV                   2017-05-23       Created as part of SE2017-147.
 NR					   2017-03-08       MAINT-5514 - Chnaged State name order by clause DESC to ASC.         
                       
******/                 
            
CREATE  PROCEDURE [dbo].[State_Sel_By_Client_Id_State_Name]
      ( 
       @State_Name VARCHAR(200) = NULL
      ,@Client_Id INT = NULL )
AS 
BEGIN                
      SET NOCOUNT ON;     
      
      
      SELECT
            ch.State_Id
           ,ch.State_Name
      FROM
            core.Client_Hier_Account cha
            INNER JOIN core.Client_Hier ch
                  ON cha.Client_Hier_Id = ch.Client_Hier_Id
      WHERE
            ( @State_Name IS NULL
              OR ch.State_Name LIKE '%' + @State_Name + '%' )
            AND ( @Client_Id IS NULL
                  OR ch.Client_Id = @Client_Id )
      GROUP BY
            ch.State_Id
           ,ch.State_Name
      ORDER BY
            ch.State_Name        
                        
                       
END;

;
GO

GRANT EXECUTE ON  [dbo].[State_Sel_By_Client_Id_State_Name] TO [CBMSApplication]
GO
