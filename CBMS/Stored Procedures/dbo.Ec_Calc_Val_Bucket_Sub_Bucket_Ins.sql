SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
  dbo.Ec_Calc_Val_Invoice_Sub_Bucket_Insert  
  
DESCRIPTION:  
  
  
INPUT PARAMETERS:  
 Name								DataType				Default		Description  
-------------------------------------------------------------------------------------  
 @Ec_Calc_Val_Id					INT
 @Ec_Calc_Val_Invoice_Sub_Bucket	tvp_Ec_Calc_Val_Invoice_Sub_Bucket  
  

OUTPUT PARAMETERS:  
 Name								DataType				Default		Description  
-------------------------------------------------------------------------------------  
  
USAGE EXAMPLES:  
-------------------------------------------------------------------------------------  
 BEGIN Transaction
 
 select * from  Ec_Calc_Val_Bucket_Map where Ec_Calc_val_Id = 13 
 select * from  Ec_Calc_Val_Bucket_Sub_Bucket_Map  where Ec_Calc_Val_Bucket_Map_Id in ( select Ec_Calc_Val_Bucket_Map_Id from  Ec_Calc_Val_Bucket_Map where Ec_Calc_val_Id = 13)
   
 DECLARE @Ec_Calc_Val_Bucket_Sub_Bucket AS tvp_Ec_Calc_Val_Bucket_Sub_Bucket   
 insert into  @Ec_Calc_Val_Bucket_Sub_Bucket values (329,null),(330,27),(330,26)  
 exec Ec_Calc_Val_Bucket_Sub_Bucket_Ins 13,@Ec_Calc_Val_Bucket_Sub_Bucket,49   
  
 select * from  Ec_Calc_Val_Bucket_Map where Ec_Calc_val_Id = 13 
 select * from  Ec_Calc_Val_Bucket_Sub_Bucket_Map  where Ec_Calc_Val_Bucket_Map_Id in ( select Ec_Calc_Val_Bucket_Map_Id from  Ec_Calc_Val_Bucket_Map where Ec_Calc_val_Id = 13 )
 
 ROLLBACK Transaction 
  
  
AUTHOR INITIALS:  
 Initials	Name  
-------------------------------------------------------------------------------------  
 RKV		Ravi Kumar Vegesna  
   
MODIFICATIONS  
  
 Initials		Date			Modification  
-------------------------------------------------------------------------------------  
 RKV			2015-10-12		Created for AS400-PII 
            
              
******/  
CREATE PROCEDURE [dbo].[Ec_Calc_Val_Bucket_Sub_Bucket_Ins]
      ( 
       @Ec_Calc_Val_Id INT
      ,@Ec_Calc_Val_Bucket_Sub_Bucket AS tvp_Ec_Calc_Val_Bucket_Sub_Bucket READONLY
      ,@Created_User_Id INT )
AS 
BEGIN  
      SET NOCOUNT ON;  
     
     
      INSERT      INTO dbo.Ec_Calc_Val_Bucket_Map
                  ( 
                   Ec_Calc_Val_Id
                  ,Bucket_Master_Id
                  ,Created_User_Id )
                  SELECT
                        @Ec_Calc_Val_Id
                       ,ecvbsb.Bucket_Master_Id
                       ,@Created_User_Id
                  FROM
                        @Ec_Calc_Val_Bucket_Sub_Bucket ecvbsb
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.Ec_Calc_Val_Bucket_Map ecvbm
                                     WHERE
                                          ecvbm.Bucket_Master_Id = ecvbsb.Bucket_Master_Id
                                          AND ecvbm.Ec_Calc_Val_Id = @Ec_Calc_Val_Id )
                  GROUP BY
                        ecvbsb.Bucket_Master_Id    
        
   
        
      DELETE
            ecvbsbm
      FROM
            dbo.Ec_Calc_Val_Bucket_Sub_Bucket_Map ecvbsbm
            INNER JOIN dbo.Ec_Calc_Val_Bucket_Map ecvbm
                  ON ecvbsbm.Ec_Calc_Val_Bucket_Map_Id = ecvbm.Ec_Calc_Val_Bucket_Map_Id
                     AND ecvbm.Ec_Calc_Val_Id = @Ec_Calc_Val_Id
                     
      DELETE
            ecvbp
      FROM
            dbo.Ec_Calc_Val_Bucket_Map ecvbp
      WHERE
            NOT EXISTS ( SELECT
                              1
                         FROM
                              @Ec_Calc_Val_Bucket_Sub_Bucket ecbsb
                         WHERE
                              ecvbp.Bucket_Master_Id = ecbsb.Bucket_Master_Id )
            AND ecvbp.Ec_Calc_Val_Id = @Ec_Calc_Val_Id
      
      INSERT      INTO dbo.Ec_Calc_Val_Bucket_Sub_Bucket_Map
                  ( 
                   Ec_Calc_Val_Bucket_Map_Id
                  ,EC_Invoice_Sub_Bucket_Master_Id
                  ,Created_User_Id )
                  SELECT
                        ecvbm.Ec_Calc_Val_Bucket_Map_Id
                       ,ecvbsb.[EC_Invoice_Sub_Bucket_Master_Id]
                       ,@Created_User_Id
                  FROM
                        @Ec_Calc_Val_Bucket_Sub_Bucket ecvbsb
                        INNER JOIN dbo.Ec_Calc_Val_Bucket_Map ecvbm
                              ON ecvbm.Bucket_Master_Id = ecvbsb.Bucket_Master_Id
                                 AND ecvbm.Ec_Calc_Val_Id = @Ec_Calc_Val_Id
                  WHERE
                        ecvbsb.EC_Invoice_Sub_Bucket_Master_Id IS NOT NULL
       
END     
   

;
GO
GRANT EXECUTE ON  [dbo].[Ec_Calc_Val_Bucket_Sub_Bucket_Ins] TO [CBMSApplication]
GO
