SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME: dbo.[cbmsRate_Delete]    
    
    
DESCRIPTION:     
    
INPUT PARAMETERS:        
     Name                 DataType          Default     Description        
------------------------------------------------------------------        
  @MyAccountId    INT    
  @RateId     INT        
        
OUTPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
    
USAGE EXAMPLES:    
------------------------------------------------------------    
 BEGIN TRAN    
  EXEC dbo.CbmsRate_Delete 49, 66    
 ROLLBACK TRAN     
    
 BEGIN TRAN    
  EXEC dbo.CbmsRate_Delete 49, 2599    
      
  SELECT * FROM Application_Audit_Log WHERE table_Name = 'Rate'    
 ROLLBACK TRAN      
     
 SELECT * FROM dbo.rate where rate_id = 2599    
      
AUTHOR INITIALS:    
 Initials Name    
------------------------------------------------------------    
 DR		Deana Ritter    
 PNR	Pandarinath    
 GK		Gopi Konga    
 SC		Sreenivasulu Cheerala   
MODIFICATIONS    
    
 Initials Date   Modification    
------------------------------------------------------------    
   DR	08/04/2009		Removed Linked ServeUpdates    
   PNR  09/01/2010		Modified script as per delete tool standards and script added to the log the deleted entry in to Application_Audit_log table.    
   GK	05/05/2011      MAINT-488 fixes the code to delete rate history from addtional_charge and Charge_Master_Sign _Map ids.    
   SC	2020-05-11	    Removed Rate info from Budget Rate Default Meter Attributes.
******/
CREATE PROCEDURE [dbo].[cbmsRate_Delete]
    (
        @MyAccountId INT
        , @RateId INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Application_Name VARCHAR(30) = 'Delete Rate'
            , @Audit_Function SMALLINT = -1
            , @User_Name VARCHAR(81)
            , @Current_Ts DATETIME
            , @Table_Name VARCHAR(10) = 'RATE'
            , @Lookup_Value XML;

        DECLARE
            @Rate_Schedule_Id INT
            , @Entity_Id INT
            , @Bd_Parent_Type_Id INT
            , @Charge_Parent_Type_Id INT
            , @Additional_Charge_Id INT
            , @Charge_Master_Id INT;

        DECLARE @rate_schedule_list TABLE
              (
                  Rate_schedule_id INT
              );
        DECLARE @billing_determinant_list TABLE
              (
                  Billing_Determinant_Id INT
              );
        DECLARE @Billing_Determinant_Id INT;
        DECLARE @billing_determinant_master_list TABLE
              (
                  Billing_Determinant_Master_Id INT
              );
        DECLARE @Billing_Determinant_Master_Id INT;
        DECLARE @charge_master_list TABLE
              (
                  Charge_Master_Id INT
              );
        DECLARE @Additional_Charge_Id_List TABLE
              (
                  Additional_Charge_Id INT
              );

        SELECT
            @Entity_Id = ENTITY_ID
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_NAME = 'UTILITY_TABLE'
            AND ENTITY_DESCRIPTION = 'Table_Type';

        SELECT
            @Bd_Parent_Type_Id = ENTITY_ID
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_DESCRIPTION = 'Billing Determinant Charge Parent'
            AND ENTITY_NAME = 'Rate_Schedule';

        SELECT
            @Charge_Parent_Type_Id = ENTITY_ID
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_DESCRIPTION = 'Billing Determinant Charge Parent'
            AND ENTITY_NAME = 'Rate';


        SET @Lookup_Value = (   SELECT
                                    RATE.RATE_ID
                                    , RATE.RATE_NAME
                                    , VENDOR.VENDOR_NAME
                                    , Commodity.Commodity_Name
                                FROM
                                    dbo.VENDOR
                                    JOIN dbo.RATE
                                        ON RATE.VENDOR_ID = VENDOR.VENDOR_ID
                                    JOIN dbo.Commodity
                                        ON Commodity.Commodity_Id = RATE.COMMODITY_TYPE_ID
                                WHERE
                                    RATE.RATE_ID = @RateId
                                FOR XML RAW);

        SELECT
            @User_Name = FIRST_NAME + SPACE(1) + LAST_NAME
        FROM
            dbo.USER_INFO
        WHERE
            USER_INFO_ID = @MyAccountId;

        INSERT INTO @rate_schedule_list
             (
                 Rate_schedule_id
             )
        SELECT  RATE_SCHEDULE_ID FROM   dbo.RATE_SCHEDULE WHERE RATE_ID = @RateId;

        INSERT INTO @billing_determinant_list
             (
                 Billing_Determinant_Id
             )
        SELECT
            BILLING_DETERMINANT_ID
        FROM
            dbo.BILLING_DETERMINANT
        WHERE
            BD_PARENT_ID = @RateId
            AND BD_PARENT_TYPE_ID = @Bd_Parent_Type_Id;

        INSERT INTO @billing_determinant_master_list
             (
                 Billing_Determinant_Master_Id
             )
        SELECT
            BILLING_DETERMINANT_MASTER_ID
        FROM
            dbo.BILLING_DETERMINANT_MASTER
        WHERE
            BD_PARENT_ID = @RateId
            AND BD_PARENT_TYPE_ID = @Charge_Parent_Type_Id;

        INSERT INTO @charge_master_list
             (
                 Charge_Master_Id
             )
        SELECT
            CHARGE_MASTER_ID
        FROM
            dbo.CHARGE_MASTER
        WHERE
            CHARGE_PARENT_ID = @RateId
            AND CHARGE_PARENT_TYPE_ID = @Charge_Parent_Type_Id;

        INSERT INTO @Additional_Charge_Id_List
             (
                 Additional_Charge_Id
             )
        SELECT
            ac.ADDITIONAL_CHARGE_ID
        FROM
            dbo.ADDITIONAL_CHARGE ac
            INNER JOIN @charge_master_list cml
                ON cml.Charge_Master_Id = ac.CHARGE_MASTER_ID;

        BEGIN TRY
            BEGIN TRAN;
            ---delete rate from Rate default meter attributes  
            DELETE  FROM Budget.Rate_Default_Meter_Attribute WHERE  RATE_ID = @RateId;

            WHILE EXISTS (SELECT    1 FROM  @rate_schedule_list)
                BEGIN

                    SET @Rate_Schedule_Id = (SELECT TOP 1   Rate_schedule_id FROM   @rate_schedule_list);

                    EXEC dbo.cbmsRateSchedule_Delete @Rate_Schedule_Id;

                    DELETE  FROM @rate_schedule_list WHERE  Rate_schedule_id = @Rate_Schedule_Id;
                END;

            WHILE EXISTS (SELECT    1 FROM  @billing_determinant_list)
                BEGIN

                    SET @Billing_Determinant_Id = (SELECT   TOP 1   Billing_Determinant_Id FROM @billing_determinant_list);

                    EXEC dbo.Billing_Determinant_Del @Billing_Determinant_Id;

                    DELETE  FROM
                    @billing_determinant_list
                    WHERE
                        Billing_Determinant_Id = @Billing_Determinant_Id;

                END;

            WHILE EXISTS (SELECT    1 FROM  @billing_determinant_master_list)
                BEGIN

                    SET @Billing_Determinant_Master_Id = (   SELECT TOP 1
                                                                    Billing_Determinant_Master_Id
                                                             FROM
                                                                    @billing_determinant_master_list);

                    EXEC dbo.Billing_Determinant_Master_Del @Billing_Determinant_Master_Id;

                    DELETE  FROM
                    @billing_determinant_master_list
                    WHERE
                        Billing_Determinant_Master_Id = @Billing_Determinant_Master_Id;

                END;


            WHILE EXISTS (SELECT    1 FROM  @Additional_Charge_Id_List)
                BEGIN
                    SET @Additional_Charge_Id = (SELECT TOP 1   Additional_Charge_Id FROM   @Additional_Charge_Id_List);
                    EXEC Additional_Charge_Del @Additional_Charge_Id;

                    DELETE  FROM
                    @Additional_Charge_Id_List
                    WHERE
                        Additional_Charge_Id = @Additional_Charge_Id;
                END;


            WHILE EXISTS (SELECT    1 FROM  @charge_master_list)
                BEGIN
                    SET @Charge_Master_Id = (SELECT TOP 1   Charge_Master_Id FROM   @charge_master_list);

                    EXEC Charge_Master_Sign_Map_Del @Charge_Master_Id;
                    EXEC dbo.Charge_Master_Del @Charge_Master_Id;

                    DELETE  FROM @charge_master_list WHERE  Charge_Master_Id = @Charge_Master_Id;
                END;


            EXEC dbo.Entity_Audit_Del_By_Entity_Id_Entity_Identifier @Entity_Id, @RateId;

            EXEC dbo.Rate_Del @RateId;

            SET @Current_Ts = GETDATE();
            EXEC dbo.Application_Audit_Log_Ins
                NULL
                , @Application_Name
                , @Audit_Function
                , @Table_Name
                , @Lookup_Value
                , @User_Name
                , @Current_Ts;

            COMMIT TRAN;
        END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRAN;
                END;

            EXEC usp_RethrowError;
        END CATCH;
    END;
GO
GRANT EXECUTE ON  [dbo].[cbmsRate_Delete] TO [CBMSApplication]
GO
