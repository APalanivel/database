SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Invoice_Collection_Queue_Exists_Account_Config_Id         
                
Description:                
   This sproc is used to fill the ICQ Batch Table.        
                             
 Input Parameters:                
    Name										DataType   Default   Description                  
--------------------------------------------------------------------------------------                  
                         
   
 Output Parameters:                      
    Name        DataType   Default   Description                  
--------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
--------------------------------------------------------------------------------------     
  exec dbo.Invoice_Collection_Queue_Exists_Account_Config_Id 536,'2015/01/01','2015/11/30'
    
Author Initials:                
    Initials  Name                
--------------------------------------------------------------------------------------                  
 RKV    Ravi Kumar Vegesna  
 Modifications:                
    Initials        Date   Modification                
--------------------------------------------------------------------------------------                  
    RKV    2017-02-03  Created For Invoice_Collection.           
               
******/   
CREATE PROCEDURE [dbo].[Invoice_Collection_Queue_Exists_Account_Config_Id]
      ( 
       @Invoice_Collection_Account_Config_Id INT
      ,@Invoice_Collection_Service_Start_Dt DATE
      ,@Invoice_Collection_Service_End_Dt DATE )
AS 
BEGIN  
      SET NOCOUNT ON;  
    
 
      DECLARE @ICQ_Exists BIT = 0
      SELECT
            @ICQ_Exists = 1
      FROM
            dbo.Invoice_Collection_Chase_Log iccl
            INNER JOIN dbo.Invoice_Collection_Chase_Log_Queue_Map icclqm
                  ON iccl.Invoice_Collection_Chase_Log_Id = icclqm.Invoice_Collection_Chase_Log_Id
            INNER JOIN dbo.Invoice_Collection_Queue icq
                  ON icclqm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                  ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN dbo.Invoice_Collection_Queue_Month_Map icqmm
                  ON icq.Invoice_Collection_Queue_Id = icqmm.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Account_Invoice_Collection_Month aicm
                  ON icqmm.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id
            INNER JOIN dbo.Code sc
                  ON sc.Code_Id = iccl.Status_Cd
      WHERE
            sc.Code_Value = 'Close'
            AND icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
            AND @ICQ_Exists = 0
            AND ( aicm.Service_Month < @Invoice_Collection_Service_Start_Dt
                  OR aicm.Service_Month > @Invoice_Collection_Service_End_Dt ) 

	    	
      SELECT
            @ICQ_Exists = 1
      FROM
            dbo.Invoice_Collection_Issue_Log icil
            INNER JOIN dbo.Invoice_Collection_Queue icq
                  ON icil.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                  ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN dbo.Invoice_Collection_Queue_Month_Map icqmm
                  ON icq.Invoice_Collection_Queue_Id = icqmm.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Account_Invoice_Collection_Month aicm
                  ON icqmm.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id
            INNER JOIN dbo.Code isc
                  ON isc.Code_Id = icil.Issue_Status_Cd
      WHERE
            isc.Code_Value = 'Close'
            AND icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
            AND @ICQ_Exists = 0
            AND ( aicm.Service_Month < @Invoice_Collection_Service_Start_Dt
                  OR aicm.Service_Month > @Invoice_Collection_Service_End_Dt ) 	
	
      SELECT
            @ICQ_Exists = 1
      FROM
            dbo.Invoice_Collection_Exception_Comment icec
            INNER JOIN dbo.Invoice_Collection_Queue icq
                  ON icec.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                  ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN dbo.Invoice_Collection_Queue_Month_Map icqmm
                  ON icq.Invoice_Collection_Queue_Id = icqmm.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Account_Invoice_Collection_Month aicm
                  ON icqmm.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id
      WHERE
            icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
            AND @ICQ_Exists = 0
            AND ( aicm.Service_Month < @Invoice_Collection_Service_Start_Dt
                  OR aicm.Service_Month > @Invoice_Collection_Service_End_Dt ) 
		
		
      SELECT
            @ICQ_Exists = 1
      FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                  ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN dbo.Code icqtc
                  ON icqtc.Code_Id = icq.Invoice_Collection_Queue_Type_Cd
            INNER JOIN dbo.Invoice_Collection_Queue_Month_Map icqmm
                  ON icq.Invoice_Collection_Queue_Id = icqmm.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Account_Invoice_Collection_Month aicm
                  ON icqmm.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id
            INNER JOIN dbo.Code sc
                  ON sc.Code_Id = icq.Status_Cd
      WHERE
            icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
            AND icqtc.Code_Value = 'ICR'
            AND sc.Code_Value = 'Received'
            AND @ICQ_Exists = 0
            AND ( aicm.Service_Month < @Invoice_Collection_Service_Start_Dt
                  OR aicm.Service_Month > @Invoice_Collection_Service_End_Dt ) 
      
      SELECT
            @ICQ_Exists = 1
      FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                  ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN dbo.Invoice_Collection_Queue_Month_Map icqmm
                  ON icq.Invoice_Collection_Queue_Id = icqmm.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Account_Invoice_Collection_Month aicm
                  ON icqmm.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id
            INNER JOIN dbo.Code icqtc
                  ON icqtc.Code_Id = icq.Invoice_Collection_Queue_Type_Cd
            INNER JOIN dbo.Code sc
                  ON sc.Code_Id = icq.Status_Cd
      WHERE
            icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
            AND icqtc.Code_Value = 'ICE'
            AND sc.Code_Value = 'Processed'
            AND @ICQ_Exists = 0
            AND ( aicm.Service_Month < @Invoice_Collection_Service_Start_Dt
                  OR aicm.Service_Month > @Invoice_Collection_Service_End_Dt )    
                  
                  
      
		
      SELECT
            @ICQ_Exists ICQ_Exists


END;

;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Queue_Exists_Account_Config_Id] TO [CBMSApplication]
GO
