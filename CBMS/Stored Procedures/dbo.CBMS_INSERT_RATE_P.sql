SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******        
NAME: dbo.[CBMS_INSERT_RATE_P]        
        
        
DESCRIPTION: Inserts data into the RATE Table.        
        
INPUT PARAMETERS:            
      Name              DataType          Default     Description            
------------------------------------------------------------------            
@vendor_id              int        
@commodity_type_id      int        
@rate_name              varchar(200)        
@rate_requirements      varchar(4000)        
            
            
OUTPUT PARAMETERS:            
      Name              DataType          Default     Description            
------------------------------------------------------------            
@rate_id                int         
        
        
USAGE EXAMPLES:        
------------------------------------------------------------        
--exec [dbo].[CBMS_INSERT_RATE_P]         
--@vendor_id = 3051,        
--@commodity_type_id = 290,        
--@rate_name = 'Test Data',        
--@rate_requirements = NULL,        
--@rate_id = NULL        
        
        
AUTHOR INITIALS:        
 Initials Name        
------------------------------------------------------------        
  DR Deana Ritter        
  SC Sreenivasulu Cheerala       
MODIFICATIONS        
        
 Initials Date  Modification        
------------------------------------------------------------        
 DR     08/04/2009		Removed Linked Server Updates        
 DMR    09/10/2010		Modified for Quoted_Identifier  
 SC     2020-05-15		Added @Budget_Calc_Type_Id as a input Parameter.    
******/  
CREATE PROCEDURE [dbo].[CBMS_INSERT_RATE_P]  
    @vendor_id INT  
    , @commodity_type_id INT  
    , @rate_name VARCHAR(200)  
    , @rate_requirements VARCHAR(4000)  
    , @rate_id INT OUTPUT  
    , @Budget_Calc_Type_Id INT  
AS  
    INSERT INTO RATE  
         (  
             VENDOR_ID  
             , COMMODITY_TYPE_ID  
             , RATE_NAME  
             , RATE_REQUIREMENTS  
             , Budget_Calc_Type_Cd  
         )  
    VALUES  
        (@vendor_id  
         , @commodity_type_id  
         , @rate_name  
         , @rate_requirements  
         , @Budget_Calc_Type_Id);  
    SELECT  @rate_id = SCOPE_IDENTITY();  
  
    RETURN;
GO
GRANT EXECUTE ON  [dbo].[CBMS_INSERT_RATE_P] TO [CBMSApplication]
GO
