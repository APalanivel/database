SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Sr_Rfp_Account_Meter_Map_Del]  
     
DESCRIPTION:

	It Deletes SR RFP Account Meter Map for the given Sr_Rfp_Account_Meter_Map_Id						

INPUT PARAMETERS:
NAME							DATATYPE	DEFAULT		DESCRIPTION
-------------------------------------------------------------------
@Sr_Rfp_Account_Meter_Map_Id	INT
   
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------

	Begin Tran
		EXEC Sr_Rfp_Account_Meter_Map_Del  18569
	Rollback Tran
	
	SELECT TOP 100 * FROM SR_RFP_ACCOUNT_METER_MAP m
	WHERE NOT EXISTS(SELECT 1 FROM SR_RFP_ARCHIVE_METER a WHERE a.Sr_Rfp_Account_Meter_Map_Id = m.Sr_Rfp_Account_Meter_Map_Id)
	AND NOT EXISTS(SELECT 1 FROM SR_RFP_ARCHIVE_METER_Contracts a WHERE a.Sr_Rfp_Account_Meter_Map_Id = m.Sr_Rfp_Account_Meter_Map_Id)

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR		    04-JUNE-10	CREATED

*/

CREATE PROCEDURE dbo.Sr_Rfp_Account_Meter_Map_Del
   (
    @Sr_Rfp_Account_Meter_Map_Id INT
   )
AS
BEGIN

    SET NOCOUNT ON;

    DELETE
		dbo.SR_RFP_Account_Meter_Map
	WHERE
		SR_RFP_ACCOUNT_METER_MAP_ID = @Sr_Rfp_Account_Meter_Map_Id

END
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Account_Meter_Map_Del] TO [CBMSApplication]
GO
