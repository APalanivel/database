SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Budget_Account_Del]  
     
DESCRIPTION: 
	It Deletes Budget Account for Selected Budget Account Id.
      
INPUT PARAMETERS:          
NAME				DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Budget_Account_Id	INT				
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------
	
	BEGIN TRAN
		EXEC Budget_Account_Del 20192
	ROLLBACK TRAN
	
	SELECT TOP 10
		*
	FROM
		Budget_Account ba
	WHERE
		Not EXISTS(SELECT 1 FROM Budget_Detail_Comments bdc WHERE bdc.Budget_Account_id = ba.Budget_Account_id)
		AND Not EXISTS(SELECT 1 FROM Budget_Detail_Snap_Shot bdc WHERE bdc.Budget_Account_id = ba.Budget_Account_id)
		AND Not EXISTS(SELECT 1 FROM Budget_Details bdc WHERE bdc.Budget_Account_id = ba.Budget_Account_id)
	
    
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR			PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR			26-MAY-10	CREATED     

*/  

CREATE PROCEDURE dbo.Budget_Account_Del
    (
      @Budget_Account_Id INT
    )
AS 
BEGIN

    SET NOCOUNT ON ;
   
	DELETE
		dbo.BUDGET_ACCOUNT
	WHERE
		BUDGET_ACCOUNT_ID = @Budget_Account_Id

END
GO
GRANT EXECUTE ON  [dbo].[Budget_Account_Del] TO [CBMSApplication]
GO
