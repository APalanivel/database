SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                        
 NAME: dbo.ValCol_Setup_Exist_Sel_By_Account_Id_Begin_End_Dt            
                        
 DESCRIPTION:                        
			To get Supplier Account Details                       
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
------------------------------------------------------------------------------     
@Account_Id						INT
@Start_Dt						Date
@End_Dt							Date			                      
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
------------------------------------------------------------------------------     
                        
 USAGE EXAMPLES:                            
------------------------------------------------------------------------------     
 
EXEC dbo.ValCol_Setup_Exist_Sel_By_Account_Id_Begin_End_Dt
    @Account_Id = 123
    , @Start_Dt = '2019-01-01'
    , @End_Dt ='2019-01-01'

    

  
                     
 AUTHOR INITIALS:        
       
 Initials              Name        
------------------------------------------------------------------------------     
 NR						Narayana Reddy
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
------------------------------------------------------------------------------     
 NR                    2019-05-29       Created for - Add Contract.
                       
******/
CREATE PROCEDURE [dbo].[ValCol_Setup_Exist_Sel_By_Account_Id_Begin_End_Dt]
     (
         @Account_Id INT
         , @Start_Dt DATE
         , @End_Dt DATE
     )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Is_ValCal_Setup_Exists BIT = 0;


        SELECT
            @Is_ValCal_Setup_Exists = 1
        FROM
            dbo.Account_Level_Supplier_Recalc_Type alsrt
        WHERE
            alsrt.Account_Id = @Account_Id
            AND (   @Start_Dt BETWEEN alsrt.Start_Dt
                              AND     alsrt.End_Dt
                    OR  @End_Dt BETWEEN alsrt.Start_Dt
                                AND     alsrt.End_Dt
                    OR  alsrt.Start_Dt BETWEEN @Start_Dt
                                       AND     @End_Dt
                    OR  alsrt.End_Dt BETWEEN @Start_Dt
                                     AND     @End_Dt);


        SELECT  @Is_ValCal_Setup_Exists AS Is_ValCal_Setup_Exists;


    END;
GO
GRANT EXECUTE ON  [dbo].[ValCol_Setup_Exist_Sel_By_Account_Id_Begin_End_Dt] TO [CBMSApplication]
GO
