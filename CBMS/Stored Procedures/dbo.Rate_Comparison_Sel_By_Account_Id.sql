SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Rate_Comparison_Sel_By_Account_Id]  
     
DESCRIPTION: 
	To Get Rate Comparison Details for Selected Account Id.
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Account_Id		INT						
@StartIndex		INT			1			
@EndIndex		INT			2147483647
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        

	EXEC Rate_Comparison_Sel_By_Account_Id 88450
	
	EXEC Rate_Comparison_Sel_By_Account_Id 35475

     
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR			PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR			25-MAY-10	CREATED     
*/  

CREATE PROCEDURE dbo.Rate_Comparison_Sel_By_Account_Id
(
	@Account_Id		INT
	,@StartIndex	INT		= 1
	,@EndIndex		INT		= 2147483647
)
AS
BEGIN

	SET NOCOUNT ON;

	WITH Cte_Rate_Comparison_List AS
	(
		SELECT
			 RATE_COMPARISON_NAME
			,CREATION_DATE
			,RC_RATE_COMPARISON_ID
			,ROW_NUMBER()OVER(ORDER BY RC_RATE_COMPARISON_ID) Row_Num
			,COUNT(1) OVER() Total_Rows
		FROM
			dbo.RC_RATE_COMPARISON
		WHERE
			Account_Id = @Account_Id
	)
	SELECT
		RATE_COMPARISON_NAME
		,CREATION_DATE
		,Total_Rows
	FROM
		Cte_Rate_Comparison_List
	WHERE
		Row_Num BETWEEN @StartIndex AND @EndIndex

END
GO
GRANT EXECUTE ON  [dbo].[Rate_Comparison_Sel_By_Account_Id] TO [CBMSApplication]
GO
