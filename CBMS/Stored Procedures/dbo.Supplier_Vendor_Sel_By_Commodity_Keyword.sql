SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.Supplier_Vendor_Sel_By_Commodity_Keyword
           
DESCRIPTION:             
			To get vendors by service
			
INPUT PARAMETERS:            
	Name				DataType		Default		Description  
---------------------------------------------------------------------------------  
	@Commodity_Id		INT
    @Keyword			VARCHAR(200)	NULL


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT TOP 10 * FROM dbo.VENDOR_COMMODITY_MAP
	
           
	EXEC dbo.Supplier_Vendor_Sel_By_Commodity_Keyword 290,'Constellation'
	
		
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-02-14	Contract placeholder - CP-8 Created
******/

CREATE PROCEDURE [dbo].[Supplier_Vendor_Sel_By_Commodity_Keyword]
      ( 
       @Commodity_Id INT
      ,@Keyword VARCHAR(200) = NULL )
AS 
BEGIN

      SET NOCOUNT ON;
      
      SELECT
            vndr.VENDOR_ID
           ,vndr.VENDOR_NAME
      FROM
            dbo.VENDOR_COMMODITY_MAP vcm
            INNER JOIN dbo.VENDOR vndr
                  ON vcm.VENDOR_ID = vndr.VENDOR_ID
            INNER JOIN dbo.ENTITY ventyp
                  ON vndr.VENDOR_TYPE_ID = ventyp.ENTITY_ID
      WHERE
            vndr.IS_HISTORY = 0
            AND vcm.COMMODITY_TYPE_ID = @Commodity_Id
            AND ventyp.ENTITY_DESCRIPTION = 'Vendor'
            AND ventyp.ENTITY_NAME = 'Supplier'
            AND ( @Keyword IS NULL
                  OR vndr.VENDOR_NAME LIKE +'%' + @Keyword + '%' )
      
END;
;
GO
GRANT EXECUTE ON  [dbo].[Supplier_Vendor_Sel_By_Commodity_Keyword] TO [CBMSApplication]
GO
