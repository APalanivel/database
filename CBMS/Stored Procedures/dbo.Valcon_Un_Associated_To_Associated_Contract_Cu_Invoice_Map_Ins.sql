SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
                         
 NAME: dbo.Valcon_Un_Associated_To_Associated_Contract_Cu_Invoice_Map_Ins                     
                          
 DESCRIPTION:      
		  To Map the   contract and invoice data for associated.                        
                          
 INPUT PARAMETERS:      
                         
 Name                               DataType          Default       Description      
----------------------------------------------------------------------------------
                          
 OUTPUT PARAMETERS:      
                               
 Name                               DataType          Default       Description      
----------------------------------------------------------------------------------
                          
 USAGE EXAMPLES:                              
----------------------------------------------------------------------------------
               
	 BEGIN TRAN 
	        
	 SELECT * FROM dbo.Contract_Cu_Invoice_Map where  Contract_Id = 1   
	                
	 EXEC dbo.Valcon_Un_Associated_To_Associated_Contract_Cu_Invoice_Map_Ins
	      @Contract_Id =1
        , @Account_List  = '1'
		, @Is_Consolidated_Account = 1
        , @User_Info_Id =49  

	 SELECT * FROM dbo.Contract_Cu_Invoice_Map where  Contract_Id = 1   
	   
	 ROLLBACK   
			                
                         
 AUTHOR INITIALS:    
       
 Initials                   Name      
----------------------------------------------------------------------------------
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
----------------------------------------------------------------------------------
 NR                     2013-12-30      Created for - Add contract.  
 NR						2020-01-29		MAINT-9782 - Added IS_PROCESSED flag in Temp table.    
 NR						2020-04-29		MAINT-10157 - Removed contract filter on map table for un associated list to if invoice is mapped any other contract.
                   
                         
******/
CREATE PROCEDURE [dbo].[Valcon_Un_Associated_To_Associated_Contract_Cu_Invoice_Map_Ins]
    (
        @Contract_Id INT
        , @Account_List NVARCHAR(MAX)
        , @Is_Consolidated_Account BIT
        , @User_Info_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;
        CREATE TABLE #Account_List
             (
                 Id INT IDENTITY(1, 1)
                 , Account_Id INT
             );


        CREATE TABLE #Un_Associated_Invoice_List
             (
                 Id INT IDENTITY(1, 1)
                 , Cu_Invoice_Id INT
                 , Account_Id INT
                 , CBMS_IMAGE_ID INT
                 , Begin_Dt DATE
                 , End_Dt DATE
                 , IS_PROCESSED BIT
             );
        DECLARE
            @Max_Account_Cnt INT
            , @Account_Counter INT = 1
            , @Account_Id INT
            , @Max_Un_Associated_Cnt INT
            , @Counter INT = 1
            , @Outside_Contract_Term_Id INT
            , @New_Status_Cd INT
            , @Inprogress_Status_Cd INT
            , @Invoice_Recalc_Process_Batch_Id INT
            , @Pending_Status_Cd INT
            , @Error_Status_Cd INT
            , @Missing_Contract_Type_Id INT;

        SELECT
            @Pending_Status_Cd = MAX(CASE WHEN c.Code_Value = 'Pending' THEN c.Code_Id
                                         ELSE 0
                                     END)
            , @Error_Status_Cd = MAX(CASE WHEN c.Code_Value = 'Error' THEN c.Code_Id
                                         ELSE 0
                                     END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Request Status'
            AND c.Code_Value IN ( 'Pending', 'Error' );

        SELECT
            @Outside_Contract_Term_Id = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Type'
            AND cs.Std_Column_Name = 'Exception_Type_Cd'
            AND c.Code_Value = 'Outside Contract Term';


        SELECT
            @Missing_Contract_Type_Id = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Type'
            AND cs.Std_Column_Name = 'Exception_Type_Cd'
            AND c.Code_Value = 'Missing Contract';


        SELECT
            @New_Status_Cd = MAX(CASE WHEN c.Code_Value = 'New' THEN c.Code_Id
                                 END)
            , @Inprogress_Status_Cd = MAX(CASE WHEN c.Code_Value = 'In Progress' THEN c.Code_Id
                                          END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Status'
            AND cs.Std_Column_Name = 'Exception_Status_Cd'
            AND c.Code_Value IN ( 'New', 'In Progress' );

        INSERT INTO #Account_List
             (
                 Account_Id
             )
        SELECT
            us.Segments
        FROM
            dbo.ufn_split(@Account_List, ',') us
        GROUP BY
            us.Segments;

        SELECT  @Max_Account_Cnt = MAX(Id)FROM  #Account_List;

        WHILE (@Account_Counter <= @Max_Account_Cnt)
            BEGIN

                SELECT
                    @Account_Id = al.Account_Id
                FROM
                    #Account_List al
                WHERE
                    al.Id = @Account_Counter;

                IF (@Is_Consolidated_Account = 0)
                    BEGIN
                        INSERT INTO #Un_Associated_Invoice_List
                             (
                                 Cu_Invoice_Id
                                 , Account_Id
                                 , CBMS_IMAGE_ID
                                 , Begin_Dt
                                 , End_Dt
                                 , IS_PROCESSED
                             )
                        EXEC dbo.Associate_Dis_Associate_Invoice_Sel_By_Contract_Id_Account_Id
                            @Contract_Id = @Contract_Id
                            , @Account_Id = @Account_Id
                            , @Is_Associated_Invoice = 0
                            , @Is_Dis_Associated_Invoice = 1;

                    END;

                IF (@Is_Consolidated_Account = 1)
                    BEGIN
                        INSERT INTO #Un_Associated_Invoice_List
                             (
                                 Cu_Invoice_Id
                                 , Account_Id
                                 , CBMS_IMAGE_ID
                                 , Begin_Dt
                                 , End_Dt
                                 , IS_PROCESSED
                             )
                        EXEC dbo.Associate_Dis_Associate_Invoice_Sel_By_Contract_Id_Account_Id_For_Consolidated
                            @Contract_Id = @Contract_Id
                            , @Account_Id = @Account_Id
                            , @Is_Associated_Invoice = 0
                            , @Is_Dis_Associated_Invoice = 1;

                    END;

                SET @Account_Counter = @Account_Counter + 1;

            END;
        BEGIN TRY
            BEGIN TRAN;

            -- If atleast one invoice to be moved to associated bucket then load the data in to Batch table.
            IF EXISTS (   SELECT    TOP 1
                                    ua.Cu_Invoice_Id
                          FROM
                                #Un_Associated_Invoice_List ua
                          WHERE
                                NOT EXISTS (   SELECT
                                                    1
                                               FROM
                                                    dbo.Contract_Cu_Invoice_Map cu
                                               WHERE
                                                    cu.Contract_Id = @Contract_Id
                                                    AND cu.Cu_Invoice_Id = ua.Cu_Invoice_Id
                                                    AND cu.Account_Id = ua.Account_Id)
                                AND NOT EXISTS (   SELECT
                                                        1
                                                   FROM
                                                        dbo.Account_Exception_Cu_Invoice_Map aeci
                                                        INNER JOIN dbo.Account_Exception ae
                                                            ON aeci.Account_Exception_Id = ae.Account_Exception_Id
                                                   WHERE
                                                        ae.Account_Id = ua.Account_Id
                                                        AND aeci.Cu_Invoice_Id = ua.Cu_Invoice_Id
                                                        AND ae.Exception_Type_Cd IN ( @Outside_Contract_Term_Id
                                                                                      , @Missing_Contract_Type_Id )
                                                        AND ae.Exception_Status_Cd IN ( @New_Status_Cd
                                                                                        , @Inprogress_Status_Cd )
                                                        AND aeci.Status_Cd IN ( @New_Status_Cd, @Inprogress_Status_Cd )))
                BEGIN
                    INSERT INTO dbo.Invoice_Recalc_Process_Batch
                         (
                             Status_Cd
                             , Created_User_Id
                             , Created_Ts
                             , Updated_User_Id
                             , Last_Change_Ts
                         )
                    VALUES
                        (@Pending_Status_Cd
                         , @User_Info_Id
                         , GETDATE()
                         , @User_Info_Id
                         , GETDATE());

                    SET @Invoice_Recalc_Process_Batch_Id = SCOPE_IDENTITY();
                END;

            SELECT  @Max_Un_Associated_Cnt = MAX(Id)FROM    #Un_Associated_Invoice_List;

            WHILE (@Counter <= @Max_Un_Associated_Cnt)
                BEGIN
                    INSERT INTO dbo.Contract_Cu_Invoice_Map
                         (
                             Contract_Id
                             , Cu_Invoice_Id
                             , Account_Id
                             , Created_User_Id
                             , Created_Ts
                         )
                    SELECT
                        @Contract_Id
                        , ua.Cu_Invoice_Id
                        , ua.Account_Id
                        , @User_Info_Id
                        , GETDATE()
                    FROM
                        #Un_Associated_Invoice_List ua
                    WHERE
                        ua.Id BETWEEN @Counter
                              AND     @Counter + 499
                        AND NOT EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.Contract_Cu_Invoice_Map cu
                                           WHERE
                                                cu.Contract_Id = @Contract_Id
                                                AND cu.Cu_Invoice_Id = ua.Cu_Invoice_Id
                                                AND cu.Account_Id = ua.Account_Id)
                        AND NOT EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.Account_Exception_Cu_Invoice_Map aeci
                                                INNER JOIN dbo.Account_Exception ae
                                                    ON aeci.Account_Exception_Id = ae.Account_Exception_Id
                                           WHERE
                                                ae.Account_Id = ua.Account_Id
                                                AND aeci.Cu_Invoice_Id = ua.Cu_Invoice_Id
                                                AND ae.Exception_Type_Cd IN ( @Outside_Contract_Term_Id
                                                                              , @Missing_Contract_Type_Id )
                                                AND ae.Exception_Status_Cd IN ( @New_Status_Cd, @Inprogress_Status_Cd )
                                                AND aeci.Status_Cd IN ( @New_Status_Cd, @Inprogress_Status_Cd ))
                    GROUP BY
                        ua.Cu_Invoice_Id
                        , ua.Account_Id;

                    INSERT INTO dbo.Invoice_Recalc_Process_Batch_Dtl
                         (
                             Invoice_Recalc_Process_Batch_Id
                             , Cu_Invoice_Id
                             , Status_Cd
                             , Created_User_Id
                             , Created_Ts
                             , Updated_User_Id
                             , Last_Change_Ts
                         )
                    SELECT
                        @Invoice_Recalc_Process_Batch_Id AS Invoice_Recalc_Process_Batch_Id
                        , ua.Cu_Invoice_Id
                        , @Pending_Status_Cd
                        , @User_Info_Id
                        , GETDATE()
                        , @User_Info_Id
                        , GETDATE()
                    FROM
                        #Un_Associated_Invoice_List ua
                    WHERE
                        ua.Id BETWEEN @Counter
                              AND     @Counter + 499
                        AND @Invoice_Recalc_Process_Batch_Id IS NOT NULL
                        AND NOT EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.Account_Exception_Cu_Invoice_Map aeci
                                                INNER JOIN dbo.Account_Exception ae
                                                    ON aeci.Account_Exception_Id = ae.Account_Exception_Id
                                           WHERE
                                                ae.Account_Id = ua.Account_Id
                                                AND aeci.Cu_Invoice_Id = ua.Cu_Invoice_Id
                                                AND ae.Exception_Type_Cd IN ( @Outside_Contract_Term_Id
                                                                              , @Missing_Contract_Type_Id )
                                                AND ae.Exception_Status_Cd IN ( @New_Status_Cd, @Inprogress_Status_Cd )
                                                AND aeci.Status_Cd IN ( @New_Status_Cd, @Inprogress_Status_Cd ))
                        AND NOT EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.Invoice_Recalc_Process_Batch_Dtl ir
                                           WHERE
                                                ir.Invoice_Recalc_Process_Batch_Id = @Invoice_Recalc_Process_Batch_Id
                                                AND ir.Cu_Invoice_Id = ua.Cu_Invoice_Id)
                        AND EXISTS (   SELECT
                                            1
                                       FROM
                                            dbo.CU_INVOICE ci
                                       WHERE
                                            ci.CU_INVOICE_ID = ua.Cu_Invoice_Id
                                            AND ci.IS_REPORTED = 1
                                            AND ci.IS_PROCESSED = 1)
                    GROUP BY
                        ua.Cu_Invoice_Id;

                    SET @Counter = @Counter + 500;

                END;
            COMMIT TRAN;
            SELECT  @Invoice_Recalc_Process_Batch_Id AS Invoice_Recalc_Process_Batch_Id;
        END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRAN;
                END;
            EXEC dbo.usp_RethrowError @CustomMessage = '';  -- varchar(2500)

        END CATCH;
    END;










GO
GRANT EXECUTE ON  [dbo].[Valcon_Un_Associated_To_Associated_Contract_Cu_Invoice_Map_Ins] TO [CBMSApplication]
GO
