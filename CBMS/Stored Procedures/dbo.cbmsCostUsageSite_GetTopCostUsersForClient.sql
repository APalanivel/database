SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsCostUsageSite_GetTopCostUsersForClient

DESCRIPTION:
		
INPUT PARAMETERS:
	Name				   DataType	    Default	Description
------------------------------------------------------------
      @Client_ID		   INT
      @Division_ID		   INT		    NULL
      @Begin_Month		   DATETIME
      @End_Month		   DATETIME 
      @Commodity_Id		   INT
      @Bucket_Master_ID	   INT
      @UOM_Type_Id		   INT		    NULL
      @Currency_Unit_Id	   INT		    NULL
      
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
    EXECUTE dbo.cbmsCostUsageSite_GetTopCostUsersForClient 10069, NULL, '01/01/2011', '12/01/2011', 291, 101034, 25, null


AUTHOR INITIALS:
    Initials	 Name
------------------------------------------------------------
    AP		 Athmaram Pabbathi
    
MODIFICATIONS
	Initials	Date		   Modification
------------------------------------------------------------
	AP		2011-08-29   Created
	AP		2012-03-30   Added @Top_Count INT = 5 parameter and removed hardcoded value in the final select
******/

CREATE        PROCEDURE [dbo].[cbmsCostUsageSite_GetTopCostUsersForClient]
      ( 
       @Client_ID INT
      ,@Division_ID INT = NULL
      ,@Begin_Month DATETIME
      ,@End_Month DATETIME
      ,@Commodity_Id INT
      ,@Bucket_Master_ID INT
      ,@UOM_Type_Id INT = NULL
      ,@Currency_Unit_Id INT = NULL
      ,@Top_Count INT = 5 )
AS 
BEGIN
      SET NOCOUNT ON ;
    
      CREATE TABLE #Client_Dtl
            ( 
             Client_Hier_Id INT
            ,Site_Id INT
            ,Site_Name VARCHAR(200)
            ,Currency_Group_Id INT
            ,PRIMARY KEY CLUSTERED ( Client_Hier_Id, Site_Id ) )
    
      CREATE TABLE #Cost_Usage_Site_Dtl
            ( 
             Client_Hier_Id INT
            ,Service_Month DATE
            ,Bucket_Value DECIMAL(32, 16)
            ,PRIMARY KEY CLUSTERED ( Client_Hier_Id, Service_Month ) )

      CREATE TABLE #Invoice_Participation_Site
            ( 
             Client_Hier_Id INT
            ,Service_Month DATE
            ,Is_Published INT
            ,PRIMARY KEY CLUSTERED ( Client_Hier_Id, Service_Month ) )


      INSERT      INTO #Client_Dtl
                  ( 
                   Client_Hier_Id
                  ,Site_Id
                  ,Site_Name
                  ,Currency_Group_Id )
                  SELECT
                        ch.Client_Hier_Id
                       ,CH.Site_ID
                       ,CH.Site_Name
                       ,CH.Client_Currency_Group_ID
                  FROM
                        Core.Client_Hier CH
                  WHERE
                        CH.Client_ID = @Client_ID
                        AND ( @Division_ID IS NULL
                              OR CH.SiteGroup_Id = @Division_ID )
                        AND CH.Site_Closed = 0
                        AND CH.Site_Not_Managed = 0
                        AND CH.Site_ID > 0
                       
      INSERT      INTO #Cost_Usage_Site_Dtl
                  ( 
                   Client_Hier_Id
                  ,Service_Month
                  ,Bucket_Value )
                  SELECT
                        CUSD.Client_Hier_Id
                       ,CUSD.Service_Month
                       ,sum(case WHEN CD.Code_Value = 'Charge' THEN isnull(CUSD.Bucket_Value * CurConv.Conversion_Factor, 0)
                                 ELSE isnull(CUSD.Bucket_Value * UOM.Conversion_Factor, 0)
                            END) AS Bucket_Value
                  FROM
                        dbo.Cost_Usage_Site_Dtl CUSD
                        INNER JOIN #Client_Dtl C
                              ON C.Client_Hier_Id = CUSD.Client_Hier_Id
                        INNER JOIN dbo.Bucket_Master BM
                              ON BM.Bucket_Master_Id = CUSD.Bucket_Master_Id
                        INNER JOIN dbo.Code CD
                              ON CD.Code_Id = BM.Bucket_Type_Cd
                        LEFT OUTER JOIN dbo.Currency_Unit_Conversion CurConv
                              ON CurConv.Base_Unit_ID = CUSD.Currency_Unit_ID
                                 AND CurConv.Conversion_Date = CUSD.Service_Month
                                 AND CurConv.Currency_Group_ID = C.Currency_Group_ID
                                 AND CurConv.Converted_Unit_ID = @Currency_Unit_Id
                        LEFT OUTER JOIN dbo.Consumption_Unit_Conversion UOM
                              ON UOM.Base_Unit_ID = CUSD.UOM_Type_Id
                                 AND UOM.Converted_Unit_ID = @UOM_Type_Id
                  WHERE
                        BM.Commodity_Id = @Commodity_Id
                        AND BM.Bucket_Master_ID = @Bucket_Master_ID
                        AND BM.Is_Active = 1
                        AND CUSD.Service_Month BETWEEN @Begin_Month
                                               AND     @End_Month
                  GROUP BY
                        CUSD.Client_Hier_Id
                       ,CUSD.Service_Month

      INSERT      INTO #Invoice_Participation_Site
                  ( 
                   Client_Hier_Id
                  ,Service_Month
                  ,Is_Published )
                  SELECT
                        cha.Client_Hier_Id
                       ,IP.Service_Month
                       ,( case WHEN sum(cast(IP.Is_Received AS INT)) > 0 THEN 1
                               ELSE 0
                          END ) AS Is_Published
                  FROM
                        dbo.Invoice_Participation IP
                        INNER JOIN ( SELECT
                                          cd.Client_Hier_Id
                                         ,cd.Site_Id
                                         ,cha.Account_Id
                                     FROM
                                          #Client_Dtl cd
                                          INNER JOIN Core.Client_Hier_Account cha
                                                ON cha.Client_Hier_Id = cd.Client_Hier_Id
                                     WHERE
                                          cha.Commodity_Id = @Commodity_Id
                                     GROUP BY
                                          cd.Client_Hier_Id
                                         ,cd.Site_Id
                                         ,Account_Id ) CHA
                              ON CHA.Account_ID = IP.Account_ID
                                 AND cha.Site_Id = ip.Site_Id
                  WHERE
                        IP.Service_Month BETWEEN @Begin_Month
                                         AND     @End_Month
                  GROUP BY
                        cha.Client_Hier_Id
                       ,IP.Service_Month
                       
                       

      SELECT TOP ( @Top_Count )
            CD.Site_ID
           ,CD.Site_Name
           ,sum(CUSD.Bucket_Value) Bucket_Value
      FROM
            #Client_Dtl CD
            LEFT OUTER JOIN #Cost_Usage_Site_Dtl CUSD
                  ON CUSD.Client_Hier_Id = CD.Client_Hier_Id
            LEFT OUTER JOIN #Invoice_Participation_Site IPS
                  ON IPS.Client_Hier_Id = CD.Client_Hier_Id
                     AND IPS.Service_Month = CUSD.Service_Month
      WHERE
            ( IPS.Is_Published = 1
              OR IPS.Is_Published IS NULL )
      GROUP BY
            CD.Site_ID
           ,CD.Site_Name
      ORDER BY
            Bucket_Value DESC
            
END
;
GO
GRANT EXECUTE ON  [dbo].[cbmsCostUsageSite_GetTopCostUsersForClient] TO [CBMSApplication]
GO
