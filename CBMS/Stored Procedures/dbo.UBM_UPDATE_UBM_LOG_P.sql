SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UBM_UPDATE_UBM_LOG_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@tableName     	varchar(60)	          	
	@masterLogId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE    PROCEDURE dbo.UBM_UPDATE_UBM_LOG_P
@userId varchar(10),
@sessionId varchar(20),
@tableName varchar(60),
@masterLogId int
AS
set nocount on
insert into 
		UBM_BATCH_LOG with (rowlock)(UBM_BATCH_MASTER_LOG_ID,CASS_TABLE_NAME)
	values	
		(@masterLogId, @tableName)
GO
GRANT EXECUTE ON  [dbo].[UBM_UPDATE_UBM_LOG_P] TO [CBMSApplication]
GO
