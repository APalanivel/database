SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsContract_GetRfpChecklist

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@contract_number	varchar(50)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE   procedure [dbo].[cbmsContract_GetRfpChecklist]
(
	@MyAccountId int
	,@contract_number varchar(50)
)
as
/*
	select src.sr_rfp_checklist_id 
	from sr_rfp_checklist src
	join contract c on c.contract_id = src.new_contract_id
	where c.ed_contract_number = @contract_number

	*/

	select rfp.sr_rfp_id
	from sr_rfp rfp
	join sr_rfp_account racct on racct.sr_rfp_id = rfp.sr_rfp_id
	join sr_rfp_checklist rcheck on rcheck.sr_rfp_account_id = racct.sr_rfp_account_id
	join contract c on c.contract_id = rcheck.new_contract_id
	where c.ed_contract_number = @contract_number
GO
GRANT EXECUTE ON  [dbo].[cbmsContract_GetRfpChecklist] TO [CBMSApplication]
GO
