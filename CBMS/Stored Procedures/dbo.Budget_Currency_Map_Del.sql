SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Budget_Currency_Map_Del]  
     
DESCRIPTION: 
	To Delete Budget Currency Map for selected Budget Currency Map Id.
      
INPUT PARAMETERS:          
NAME						DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Budget_Currency_Map_Id		INT			
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN
		EXEC Budget_Currency_Map_Del  18607
	ROLLBACK 

	SELECT *
	FROM	
		dbo.BUDGET_CURRENCY_MAP 
		
AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			08-JUNE-10	CREATED     
*/  

CREATE PROCEDURE dbo.Budget_Currency_Map_Del
    (
      @Budget_Currency_Map_Id INT
    )
AS 
BEGIN

    SET NOCOUNT ON ;
   
    DELETE 
	FROM	
		dbo.BUDGET_CURRENCY_MAP 
	WHERE 
		BUDGET_CURRENCY_MAP_ID = @Budget_Currency_Map_Id
			
END
GO
GRANT EXECUTE ON  [dbo].[Budget_Currency_Map_Del] TO [CBMSApplication]
GO
