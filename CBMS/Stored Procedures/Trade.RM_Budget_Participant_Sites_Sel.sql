SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.RM_Budget_Participant_Sites_Sel                        
                          
Description:                          
        To get market price and forecast pirce if a selected index   
                          
Input Parameters:                          
    Name    DataType        Default     Description                            
--------------------------------------------------------------------------------    
	@Index_Id   INT    
    @Start_Dt	Date    
	@End_Dt		Date
                          
 Output Parameters:                                
	Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
Usage Examples:                              
--------------------------------------------------------------------------------    
	SELECT * FROM dbo.ENTITY e WHERE e.ENTITY_TYPE=272

	EXEC Trade.RM_Budget_Participant_Sites_Sel  '2019-01-01','2019-12-01'  
    SP_RECOMPILE  'Trade.RM_Budget_Participant_Sites_Sel'
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR			2019-11-29	RM-Budgets Enahancement - Created
	RR			2020-02-13	GRM-1730 Modified @Site_Id input to varchar to accept maulitple site ids as comma seperated string                
******/
CREATE PROCEDURE [Trade].[RM_Budget_Participant_Sites_Sel]
    (
        @Client_Id INT
        , @Commodity_Id INT
        , @Index_Id INT
        , @Country_Id VARCHAR(MAX) = NULL
        , @Start_Dt DATE
        , @End_Dt DATE
        , @Participant_Sites VARCHAR(20)
        , @Division_Id INT = NULL
        , @Site_Id VARCHAR(MAX) = NULL
        , @RM_Group_Id INT = NULL
    )
WITH RECOMPILE
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Index VARCHAR(200)
            , @Trade_Index_Id INT;

        DECLARE
            @All_Sites BIT = 0
            , @Hedgable_Sites BIT = 0
            , @Reported_Sites BIT = 0;

        CREATE TABLE #RM_Group_Sites
             (
                 [Site_Id] INT
             );

        SELECT  @All_Sites = 1 WHERE @Participant_Sites = 'All Sites';
        SELECT  @Hedgable_Sites = 1 WHERE   @Participant_Sites = 'Hedgeable Sites';
        SELECT  @Reported_Sites = 1 WHERE   @Participant_Sites = 'Reported Sites';

        SELECT  @Index = e.ENTITY_NAME FROM dbo.ENTITY e WHERE  e.ENTITY_ID = @Index_Id;

        SELECT
            @Trade_Index_Id = e.ENTITY_ID
        FROM
            dbo.ENTITY e
        WHERE
            e.ENTITY_TYPE = 165
            AND (   (   e.ENTITY_NAME = 'Nymex'
                        AND @Index = 'NYMEX')
                    OR  (   e.ENTITY_NAME = 'CGPR'
                            AND @Index = 'AECO'));


        INSERT INTO #RM_Group_Sites
             (
                 Site_Id
             )
        SELECT
            ch.Site_Id
        FROM
            dbo.Cbms_Sitegroup_Participant rgd
            INNER JOIN dbo.Code grp
                ON grp.Code_Id = rgd.Group_Participant_Type_Cd
            INNER JOIN dbo.CONTRACT c
                ON c.CONTRACT_ID = rgd.Group_Participant_Id
            INNER JOIN Core.Client_Hier_Account chasupp
                ON chasupp.Supplier_Contract_ID = c.CONTRACT_ID
            INNER JOIN Core.Client_Hier_Account chautil
                ON chasupp.Meter_Id = chautil.Meter_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = chasupp.Client_Hier_Id
        WHERE
            (   rgd.Cbms_Sitegroup_Id = @RM_Group_Id
                AND grp.Code_Value = 'Contract'
                AND chasupp.Account_Type = 'Supplier'
                AND chautil.Account_Type = 'Utility');

        INSERT INTO #RM_Group_Sites
             (
                 Site_Id
             )
        SELECT
            ch.Site_Id
        FROM
            dbo.Cbms_Sitegroup_Participant rgd
            INNER JOIN dbo.Code grp
                ON grp.Code_Id = rgd.Group_Participant_Type_Cd
            INNER JOIN Core.Client_Hier ch
                ON ch.Site_Id = rgd.Group_Participant_Id
        WHERE
            (   rgd.Cbms_Sitegroup_Id = @RM_Group_Id
                AND grp.Code_Value = 'Site');


        SELECT
            ch.Client_Hier_Id
        FROM
            Core.Client_Hier ch
        WHERE
            ch.Client_Id = @Client_Id
            AND (   @Division_Id IS NULL
                    OR  ch.Sitegroup_Id = @Division_Id)
            AND (   @Site_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Site_Id, ',') us
                                   WHERE
                                        CAST(us.Segments AS INT) = ch.Site_Id))
            AND (   @Country_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Country_Id, ',')
                                   WHERE
                                        CAST(Segments AS INT) = ch.Country_Id))
            AND ch.Site_Id > 0
            AND (   @All_Sites = 1
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        Trade.RM_Client_Hier_Onboard siteob
                                        INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                                            ON siteob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
                                        INNER JOIN dbo.ENTITY et
                                            ON et.ENTITY_ID = chhc.Hedge_Type_Id
                                   WHERE
                                        siteob.Client_Hier_Id = ch.Client_Hier_Id
                                        AND siteob.Commodity_Id = @Commodity_Id
                                        AND @All_Sites = 0
                                        AND (   @Hedgable_Sites = 0
                                                OR  et.ENTITY_NAME IN ( 'Physical', 'Financial', 'Physical & Financial' ))
                                        AND (   @Reported_Sites = 0
                                                OR  chhc.Include_In_Reports = 1)
                                        AND (   chhc.Config_Start_Dt BETWEEN @Start_Dt
                                                                     AND     @End_Dt
                                                OR  chhc.Config_End_Dt BETWEEN @Start_Dt
                                                                       AND     @End_Dt
                                                OR  @Start_Dt BETWEEN chhc.Config_Start_Dt
                                                              AND     chhc.Config_End_Dt
                                                OR  @End_Dt BETWEEN chhc.Config_Start_Dt
                                                            AND     chhc.Config_End_Dt))
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        Trade.RM_Client_Hier_Onboard clntob
                                        INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                                            ON clntob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
                                        INNER JOIN dbo.ENTITY et
                                            ON et.ENTITY_ID = chhc.Hedge_Type_Id
                                        INNER JOIN Core.Client_Hier clch
                                            ON clntob.Client_Hier_Id = clch.Client_Hier_Id
                                   WHERE
                                        clch.Sitegroup_Id = 0
                                        AND clch.Client_Id = ch.Client_Id
                                        AND clntob.Country_Id = ch.Country_Id
                                        AND clntob.Commodity_Id = @Commodity_Id
                                        AND @All_Sites = 0
                                        AND (   @Hedgable_Sites = 0
                                                OR  et.ENTITY_NAME IN ( 'Physical', 'Financial', 'Physical & Financial' ))
                                        AND (   @Reported_Sites = 0
                                                OR  chhc.Include_In_Reports = 1)
                                        AND (   chhc.Config_Start_Dt BETWEEN @Start_Dt
                                                                     AND     @End_Dt
                                                OR  chhc.Config_End_Dt BETWEEN @Start_Dt
                                                                       AND     @End_Dt
                                                OR  @Start_Dt BETWEEN chhc.Config_Start_Dt
                                                              AND     chhc.Config_End_Dt
                                                OR  @End_Dt BETWEEN chhc.Config_Start_Dt
                                                            AND     chhc.Config_End_Dt)
                                        AND NOT EXISTS (   SELECT
                                                                1
                                                           FROM
                                                                Trade.RM_Client_Hier_Onboard siteob1
                                                           WHERE
                                                                siteob1.Client_Hier_Id = ch.Client_Hier_Id
                                                                AND siteob1.Commodity_Id = @Commodity_Id)))
            AND (   @RM_Group_Id IS NULL
                    OR  EXISTS (SELECT  1 FROM  #RM_Group_Sites rgs WHERE   rgs.Site_Id = ch.Site_Id))
        GROUP BY
            ch.Client_Hier_Id;


        DROP TABLE #RM_Group_Sites;

    END;

GO
GRANT EXECUTE ON  [Trade].[RM_Budget_Participant_Sites_Sel] TO [CBMSApplication]
GO
