SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********        
NAME:  [dbo].[Report_DE_cbms_CuInvoice_Get_Manual_For_ProKarma_Upload4]         
         
DESCRIPTION:        
 procedure is used for data extracts via DTS packages        
        
INPUT PARAMETERS:            
      Name     DataType          Default     DescriptiON            
------------------------------------------------------------            
     
        
OUTPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------            
            
USAGE EXAMPLES:           
        
 EXEC [dbo].[Report_DE_cbms_CuInvoice_Get_Manual_For_ProKarma_Upload4]       
         
------------------------------------------------------------          
AUTHOR INITIALS:          
Initials Name          
------------------------------------------------------------                
HG       Harihara Suthan G        
DMR      Deana Ritter        
PD       Padmanava Debnath      
AKR      Ashok Kumar Raju
RKV      Ravi Kumar Vegesna    
         
Initials  Date      ModificatiON          
------------------------------------------------------------          
         07/20/2009 Created        
 HG      03/23/2010 Bucket_type_id column renamed to Bucket_Master_id in Cu_Invoice_Charge and Cu_Invoice_Determinant table.        
                    Entity table reference to get Bucket_name replaced by Bucket_Master table        
                    Entity table reference to get Commodity_name replaced by Commodity table.        
 DMR    08/20/2010  Removed Meter table from Account CTE as this was generating multiple results unnecessarily.  This cause invoices to be        
                    incorrectly reported as group accounts.        
 PD     11/02/2010  Added WHERE clause in the CTE to filter out AT&T data      
 DMR    06/20/2011  Removed join to Invoice Charge account in pulling base invoice as we now receive determinants with no charges.       
                    Per Erin Dierking 6/20/2011.      
 AKR    2012-05-01  Cloned from cbmsCuInvoice_GetManualForProKarmaUpload4. Removed Unused Parameters.
 RKV    2014-01-07	Maint - 2357 & 2358 Added column Meter_Number and appended the start and end dates for supplier accounts to the account numbers. 
 HG		2019-09-30	PRSUPPORT-2996, converting the values to the column data width instead of leaving it to default 30 characters.
******/
CREATE PROCEDURE [dbo].[Report_DE_cbms_CuInvoice_Get_Manual_For_ProKarma_Upload4]
AS
      BEGIN

            SET NOCOUNT ON;
            SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
            DECLARE @session_uid UNIQUEIDENTIFIER;
            SET @session_uid = newid ();

            DECLARE @ATT_Id INT;

            SELECT
                  @ATT_Id = c.CLIENT_ID
            FROM  dbo.CLIENT c
            WHERE c.CLIENT_NAME = 'AT&T Services, Inc.';

            --Meta data for SSIS  
            IF 1 = 2
                  BEGIN
                        SELECT
                              cast(NULL AS INT) AS [Client Id]
                            , cast(NULL AS VARCHAR(200)) AS Client
                            , cast(NULL AS INT) AS [Site Id]
                            , cast(NULL AS VARCHAR(200)) AS Site
                            , cast(NULL AS VARCHAR(200)) AS State
                            , cast(NULL AS INT) AS [Account Id]
                            , cast(NULL AS VARCHAR(500)) AS [Account Number]
                            , cast(NULL AS INT) AS [Vendor Id]
                            , cast(NULL AS VARCHAR(200)) AS [Vendor Name]
                            , cast(NULL AS VARCHAR(8)) AS [Account Type]
                            , cast(NULL AS VARCHAR(50)) AS [Meter Number]
                            , cast(NULL AS INT) AS [Invoice Id]
                            , cast(NULL AS VARCHAR(20)) AS [Service Month]
                            , cast(NULL AS VARCHAR(200)) AS Currency
                            , cast(NULL AS VARCHAR(200)) AS [Item Type]
                            , cast(NULL AS VARCHAR(200)) AS [Commodity Type]
                            , cast(NULL AS VARCHAR(200)) AS [Item Name]
                            , cast(NULL AS VARCHAR(200)) AS [Item Code]
                            , cast(NULL AS VARCHAR(200)) AS [Unit of Measure]
                            , cast(NULL AS VARCHAR(200)) AS [Item Value]
                            , cast(NULL AS VARCHAR(81)) AS [Updated By];
                  END;

            CREATE TABLE #Account_Details
                  ( Client_Id      INT
                  , Client_Name    VARCHAR(200)
                  , Site_id        INT
                  , Site_name      VARCHAR(200)
                  , State_Name     VARCHAR(200)
                  , account_id     INT
                  , Account_Type   VARCHAR(8)
                  , account_number VARCHAR(500)
                  , Vendor_Id      INT
                  , Vendor_Name    VARCHAR(200)
                  , Meter_Number   VARCHAR(50));

            INSERT INTO dbo.RPT_ACCOUNT_CU_INVOICE (
                                                         SESSION_UID
                                                       , ACCOUNT_ID
                                                       , CU_INVOICE_ID
                                                   )
                        SELECT
                                    @session_uid
                                  , sm.Account_ID
                                  , max(cu.CU_INVOICE_ID) cu_invoice_id
                        FROM        CU_INVOICE cu
                                    JOIN
                                    CU_INVOICE_SERVICE_MONTH sm
                                          ON sm.CU_INVOICE_ID = cu.CU_INVOICE_ID
                                    JOIN
                                          (     SELECT
                                                            sm.Account_ID
                                                          , max(cu.UPDATED_DATE) updated_date
                                                FROM        CU_INVOICE cu
                                                            JOIN
                                                            CU_INVOICE_SERVICE_MONTH sm
                                                                  ON sm.CU_INVOICE_ID = cu.CU_INVOICE_ID
                                                WHERE       cu.IS_PROCESSED = 1
                                                            AND
                                                                  (     cu.UBM_ID IS NULL
                                                                        OR    cu.UBM_ID IN (
                                                                                    1
                                                                                  , 2))
                                                GROUP BY    sm.Account_ID ) x
                                                ON x.Account_ID = sm.Account_ID
                                                   AND x.updated_date = cu.UPDATED_DATE
                        WHERE       cu.IS_PROCESSED = 1
                                    AND
                                          (     cu.UBM_ID IS NULL
                                                OR    cu.UBM_ID IN (
                                                            1
                                                          , 2))
                                    AND   cu.UPDATED_DATE >= getdate() - 181
                        GROUP BY    sm.Account_ID;

            INSERT INTO RPT_CU_INVOICE_DETAIL (
                                                    SESSION_UID
                                                  , ITEM_TYPE
                                                  , CU_INVOICE_ID
                                                  , COMMODITY_TYPE
                                                  , ITEM_NAME
                                                  , ITEM_CODE
                                                  , UNIT_OF_MEASURE
                                                  , ITEM_VALUE
                                              )
                        SELECT
                              @session_uid
                            , 'Determinant' item_type
                            , d.CU_INVOICE_ID
                            , c.Commodity_Name commodity_type
                            , d.DETERMINANT_NAME item_name
                            , bm.Bucket_Name item_code
                            , u.ENTITY_NAME unit_of_measure
                            , d.DETERMINANT_VALUE item_value
                        FROM  RPT_ACCOUNT_CU_INVOICE cu
                              JOIN
                              CU_INVOICE_DETERMINANT d
                                    ON cu.CU_INVOICE_ID = d.CU_INVOICE_ID
                              JOIN
                              Commodity c
                                    ON c.Commodity_Id = d.COMMODITY_TYPE_ID
                              JOIN
                              Bucket_Master bm
                                    ON bm.Bucket_Master_Id = d.Bucket_Master_Id
                              JOIN
                              ENTITY u
                                    ON u.ENTITY_ID = d.UNIT_OF_MEASURE_TYPE_ID
                        WHERE cu.SESSION_UID = @session_uid
                              AND   d.DETERMINANT_VALUE IS NOT NULL;

            INSERT INTO RPT_CU_INVOICE_DETAIL (
                                                    SESSION_UID
                                                  , ITEM_TYPE
                                                  , CU_INVOICE_ID
                                                  , COMMODITY_TYPE
                                                  , ITEM_NAME
                                                  , ITEM_CODE
                                                  , UNIT_OF_MEASURE
                                                  , ITEM_VALUE
                                              )
                        SELECT
                              @session_uid
                            , 'Charge' item_type
                            , d.CU_INVOICE_ID
                            , c.Commodity_Name commodity_type
                            , d.CHARGE_NAME item_name
                            , bm.Bucket_Name item_code
                            , NULL unit_of_measure
                            , d.CHARGE_VALUE item_value
                        FROM  dbo.RPT_ACCOUNT_CU_INVOICE cu
                              JOIN
                              dbo.CU_INVOICE_CHARGE d
                                    ON d.CU_INVOICE_ID = cu.CU_INVOICE_ID
                              JOIN
                              dbo.Commodity c
                                    ON c.Commodity_Id = d.COMMODITY_TYPE_ID
                              JOIN
                              dbo.Bucket_Master bm
                                    ON bm.Bucket_Master_Id = d.Bucket_Master_Id
                        WHERE cu.SESSION_UID = @session_uid
                              AND   d.CHARGE_VALUE IS NOT NULL;
            INSERT INTO #Account_Details (
                                               Client_Id
                                             , Client_Name
                                             , Site_id
                                             , Site_name
                                             , State_Name
                                             , account_id
                                             , Account_Type
                                             , account_number
                                             , Vendor_Id
                                             , Vendor_Name
                                             , Meter_Number
                                         )
                        SELECT
                              Client_Id
                            , Client_Name
                            , Site_Id
                            , Site_name
                            , State_Name
                            , Account_Id
                            , Account_Type
                            , account_number
                            , Vendor_Id
                            , Vendor_Name
                            , Meter_Number
                        FROM
                              (     SELECT
                                                ch.Client_Id
                                              , ch.Client_Name
                                              , ch.Site_Id
                                              , ch.Site_name
                                              , cha.Meter_State_Name State_Name
                                              , cha.Account_Id
                                              , cha.Account_Type
                                              , cha.Display_Account_Number account_number
                                              , cha.Account_Vendor_Id Vendor_Id
                                              , cha.Account_Vendor_Name Vendor_Name
                                              , cha.Meter_Number
                                    FROM        Core.Client_Hier_Account cha
                                                INNER JOIN
                                                Core.Client_Hier ch
                                                      ON cha.Client_Hier_Id = ch.Client_Hier_Id
                                                INNER JOIN
                                                ENTITY e
                                                      ON e.ENTITY_NAME = cha.Account_Type
                                                         AND e.ENTITY_DESCRIPTION = 'Account Type'
                                    WHERE       ch.Client_Id != @ATT_Id
                                                AND   cha.Account_Not_Managed = 0
                                                AND   cha.Account_Type = 'Utility'
                                                AND   ch.Client_Not_Managed = 0
                                    GROUP BY    ch.Client_Id
                                              , ch.Client_Name
                                              , ch.Site_Id
                                              , ch.Site_name
                                              , cha.Meter_State_Name
                                              , cha.Account_Id
                                              , cha.Account_Type
                                              , cha.Display_Account_Number
                                              , cha.Account_Vendor_Id
                                              , cha.Account_Vendor_Name
                                              , cha.Meter_Number
                                    UNION ALL
                                    SELECT
                                                ch.Client_Id
                                              , ch.Client_Name
                                              , ch.Site_Id
                                              , ch.Site_name
                                              , cha.Meter_State_Name State_Name
                                              , cha.Account_Id
                                              , cha.Account_Type
                                              , cha.Display_Account_Number account_number
                                              , cha.Account_Vendor_Id Vendor_Id
                                              , cha.Account_Vendor_Name Vendor_Name
                                              , cha.Meter_Number
                                    FROM        Core.Client_Hier_Account cha
                                                INNER JOIN
                                                Core.Client_Hier ch
                                                      ON cha.Client_Hier_Id = ch.Client_Hier_Id
                                                INNER JOIN
                                                dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                                                      ON cha.Account_Id = samm.ACCOUNT_ID
                                    WHERE       ch.Client_Id != 11231
                                                AND   cha.Account_Not_Managed = 0
                                                AND   samm.IS_HISTORY = 0
                                                AND   cha.Account_Type = 'Supplier'
                                                AND   ch.Client_Not_Managed = 0
                                    GROUP BY    ch.Client_Id
                                              , ch.Client_Name
                                              , ch.Site_Id
                                              , ch.Site_name
                                              , cha.Meter_State_Name
                                              , cha.Account_Id
                                              , cha.Account_Type
                                              , cha.Display_Account_Number
                                              , cha.Account_Vendor_Id
                                              , cha.Account_Vendor_Name
                                              , cha.Meter_Number ) k;

            CREATE CLUSTERED INDEX ix_Account_Details_Account_Id
            ON #Account_Details ( account_id );

            SELECT
                        vam.Client_Id AS [Client Id]
                      , vam.Client_Name AS Client
                      , vam.Site_id AS [Site Id]
                      , vam.Site_name AS Site
                      , vam.State_Name AS State
                      , vam.account_id AS [Account Id]
                      , vam.account_number AS [Account Number]
                      , vam.Vendor_Id AS [Vendor Id]
                      , vam.Vendor_Name AS [Vendor Name]
                      , vam.Account_Type AS [Account Type]
                      , vam.Meter_Number
                      , cu.CU_INVOICE_ID AS [Invoice Id]
                      , convert(VARCHAR, sm.SERVICE_MONTH, 101) AS [Service Month]
                      , cur.CURRENCY_UNIT_NAME AS Currency
                      , isnull(convert(VARCHAR(200), x.ITEM_TYPE), '') AS [Item Type]
                      , isnull(convert(VARCHAR(200), x.COMMODITY_TYPE), '') AS [Commodity Type]
                      , isnull(convert(VARCHAR(200), x.ITEM_NAME), '') AS [Item Name]
                      , isnull(convert(VARCHAR(200), x.ITEM_CODE), '') AS [Item Code]
                      , isnull(convert(VARCHAR(200), x.UNIT_OF_MEASURE), '') AS [Unit of Measure]
                      , isnull(convert(VARCHAR(200), x.ITEM_VALUE), '') AS [Item Value]
                      , isnull(convert(VARCHAR(81), ui.FIRST_NAME + space(1) + ui.LAST_NAME), '') AS [Updated By]
            FROM        dbo.RPT_ACCOUNT_CU_INVOICE cu
                        JOIN
                        #Account_Details vam
                              ON vam.account_id = cu.ACCOUNT_ID
                        JOIN
                        CU_INVOICE i
                              ON i.CU_INVOICE_ID = cu.CU_INVOICE_ID
                        JOIN
                        CURRENCY_UNIT cur
                              ON cur.CURRENCY_UNIT_ID = i.CURRENCY_UNIT_ID
                        JOIN
                        USER_INFO ui
                              ON ui.USER_INFO_ID = i.UPDATED_BY_ID
                        JOIN
                        CU_INVOICE_SERVICE_MONTH sm
                              ON sm.CU_INVOICE_ID = cu.CU_INVOICE_ID
                        JOIN
                        RPT_CU_INVOICE_DETAIL x
                              ON x.CU_INVOICE_ID = cu.CU_INVOICE_ID
            WHERE       cu.SESSION_UID = @session_uid
                        AND   x.SESSION_UID = @session_uid
            GROUP BY    vam.Client_Id
                      , vam.Client_Name
                      , vam.Site_id
                      , vam.Site_name
                      , vam.State_Name
                      , vam.account_id
                      , vam.account_number
                      , vam.Vendor_Id
                      , vam.Vendor_Name
                      , vam.Account_Type
                      , vam.Meter_Number
                      , cu.CU_INVOICE_ID
                      , convert(VARCHAR, sm.SERVICE_MONTH, 101)
                      , cur.CURRENCY_UNIT_NAME
                      , isnull(convert(VARCHAR(200), x.ITEM_TYPE), '')
                      , isnull(convert(VARCHAR(200), x.COMMODITY_TYPE), '')
                      , isnull(convert(VARCHAR(200), x.ITEM_NAME), '')
                      , isnull(convert(VARCHAR(200), x.ITEM_CODE), '')
                      , isnull(convert(VARCHAR(200), x.UNIT_OF_MEASURE), '')
                      , isnull(convert(VARCHAR(200), x.ITEM_VALUE), '')
                      , isnull(convert(VARCHAR(81), ui.FIRST_NAME + space(1) + ui.LAST_NAME), '')
            ORDER BY    vam.Client_Name ASC
                      , vam.Site_name
                      , vam.account_number
                      , isnull(convert(VARCHAR(200), x.ITEM_TYPE), '') DESC
                      , isnull(convert(VARCHAR(200), x.COMMODITY_TYPE), '') ASC
                      , isnull(convert(VARCHAR(200), x.ITEM_NAME), '');

            DELETE      RPT_ACCOUNT_CU_INVOICE
            WHERE       SESSION_UID = @session_uid;

            DELETE      RPT_CU_INVOICE_DETAIL
            WHERE       SESSION_UID = @session_uid;

      END;

      ;

GO

GRANT EXECUTE ON  [dbo].[Report_DE_cbms_CuInvoice_Get_Manual_For_ProKarma_Upload4] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_cbms_CuInvoice_Get_Manual_For_ProKarma_Upload4] TO [CBMSApplication]
GO
