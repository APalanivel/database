SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE DBO.RC_UPDATE_RATE_COMPARISON_RESULT_P 

@userId int,
@sessionId varchar(20),
@rateComparisonId int,
@resultId int

AS
	set nocount on
	update RC_RATE_COMPARISON
	
	set RESULT_TYPE_ID=@resultId
	
	where RC_RATE_COMPARISON_ID=@rateComparisonId
GO
GRANT EXECUTE ON  [dbo].[RC_UPDATE_RATE_COMPARISON_RESULT_P] TO [CBMSApplication]
GO
