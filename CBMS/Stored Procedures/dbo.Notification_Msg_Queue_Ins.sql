SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
NAME:   [dbo].[Notification_Message_Queue_Ins]  
             
DESCRIPTION:               
			To create a message queue
             
INPUT PARAMETERS:              
Name							DataType		Default		Description    
------------------------------------------------------------------------    
@Notification_Type				NVARCHAR(50)  
@Data_Upload_Request_Name		NVARCHAR(200)	NULL  
@Requested_User_Id				INT  
@Email_Subject					NVARCHAR(200)   
@Email_Body						INT				NULL  
  
 OUTPUT PARAMETERS:              
Name							DataType		Default		Description    
------------------------------------------------------------------------    
@Notification_Msg_Queue_Id		INT 

 USAGE EXAMPLES:              
------------------------------------------------------------------------

BEGIN TRAN  
	DECLARE @New_Notification_Msg_Queue_Id INT  

	EXEC dbo.Notification_Msg_Queue_Ins   
		@Notification_Type = 'New User Email'  
		,@Data_Upload_Request_Name = NULL  
		,@Requested_User_Id = 16
		,@Email_Subject = 'RA Admin New User Created'  
		,@Email_Body = 'RA Admin New User Created'
		,@Notification_Msg_Queue_Id = @New_Notification_Msg_Queue_Id OUT
	SELECT @New_Notification_Msg_Queue_Id  
	SELECT * FROM dbo.Notification_Msg_Queue WHERE Notification_Msg_Queue_Id = @New_Notification_Msg_Queue_Id 
ROLLBACK  
  
 AUTHOR INITIALS:              
	Initials	Name              
------------------------------------------------------------------------
	RR			Raghu Reddy  
             
 MODIFICATIONS:               
	Initials	Date		Modification              
------------------------------------------------------------------------  
	RR			2014-01-23  Created

******/
CREATE PROCEDURE [dbo].[Notification_Msg_Queue_Ins]
      ( 
       @Notification_Type VARCHAR(25)
      ,@Data_Upload_Request_Name VARCHAR(200) = NULL
      ,@Requested_User_Id INT
      ,@Email_Subject NVARCHAR(200)
      ,@Email_Body NVARCHAR(MAX) = NULL
      ,@Notification_Msg_Queue_Id INT OUT )
AS 
BEGIN  
      SET NOCOUNT ON;

      DECLARE
            @Notification_Template_Id INT
           ,@Message_Pending_Status_Cd INT
           ,@Data_Upload_Request_Type_Id INT

      SELECT
            @Notification_Template_Id = nt.Notification_Template_Id
      FROM
            dbo.Notification_Template nt
            INNER JOIN dbo.Code AS cd
                  ON cd.Code_Id = nt.Notification_Type_Cd
            INNER JOIN dbo.Codeset cs
                  ON cs.Codeset_Id = cd.Codeset_Id
      WHERE
            cd.Code_Value = @Notification_Type
            AND cs.Codeset_Name = 'Notification Type'

      SELECT
            @Data_Upload_Request_Type_Id = uprq.Data_Upload_Request_Type_Id
      FROM
            dbo.Data_Upload_Request_Type AS uprq
      WHERE
            uprq.Request_Name = @Data_Upload_Request_Name

      BEGIN TRY  
            BEGIN TRAN 
            INSERT      INTO dbo.Notification_Msg_Queue
                        ( 
                         Notification_Template_Id
                        ,Requested_User_Id
                        ,Data_Upload_Request_Type_Id
                        ,Email_Subject
                        ,Email_Body
                        ,Created_Ts )
            VALUES
                        ( 
                         @Notification_Template_Id
                        ,@Requested_User_Id
                        ,@Data_Upload_Request_Type_Id
                        ,@Email_Subject
                        ,@Email_Body
                        ,GETDATE() ) 
    
            SELECT
                  @Notification_Msg_Queue_Id = SCOPE_IDENTITY()  

				COMMIT  
      END TRY  
      BEGIN CATCH  
            IF @@TRANCOUNT > 0 
                  ROLLBACK  
            EXEC dbo.usp_RethrowError   
   
      END CATCH  
   
END

;
GO
GRANT EXECUTE ON  [dbo].[Notification_Msg_Queue_Ins] TO [CBMSApplication]
GO
