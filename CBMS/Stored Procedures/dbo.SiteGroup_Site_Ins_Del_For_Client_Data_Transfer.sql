SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******************************************************************************************************
NAME : dbo.[SiteGroup_Site_Ins_Upd_For_Client_Data_Transfer]

DESCRIPTION: This procedure used to insert sitegroup mapping for a site 

 INPUT PARAMETERS:      

 Name			DataType		Default			Description      
--------------------------------------------------------------------        

 @Message		XML   

 OUTPUT PARAMETERS:      

 Name   DataType  Default Description      
--------------------------------------------------------------------      
  	
  USAGE EXAMPLES:
--------------------------------------------------------------------  
BEGIN TRAN
	DECLARE @tvp_Site_Ids Site_Ids

	INSERT INTO @tvp_Site_Ids(Site_Id)
	values(22225),(729238)
	   
	EXEC [SiteGroup_Site_Ins_Del_For_Client_Data_Transfer]
		@To_Sitegroup_CH_Id = 761821 
		,@tvp_Site_Ids =  @tvp_Site_Ids
	SELECT * FROM Sitegroup_Site  ss
	inner join core.client_hier ch on ch.sitegroup_id = ss.sitegroup_id
	WHERE ch.Client_hier_id = 761821
ROLLBACK TRAN
		
AUTHOR INITIALS:      

 Initials		Name      
-------------------------------------------------------------------       
 MSV			Muhamed Shahid V

 MODIFICATIONS  

 Initials		Date			Modification  
--------------------------------------------------------------------  
 MSV			18 Jul 2019		Created 	
 
*****************************************************************************************************/
CREATE PROCEDURE [dbo].[SiteGroup_Site_Ins_Del_For_Client_Data_Transfer]
	(
		@To_Sitegroup_CH_Id INT,
		@tvp_Site_Ids Site_Ids READONLY
	)
AS
BEGIN
	
    --SET NOCOUNT ON
	
	BEGIN TRY
		BEGIN TRAN;

		DECLARE @Sitegroup_Id INT

		SELECT @Sitegroup_Id = Sitegroup_Id
		FROM Core.Client_Hier ch
		WHERE ch.Client_Hier_Id = @To_Sitegroup_CH_Id

		INSERT INTO Sitegroup_Site
		(
			Sitegroup_id,
			Site_id,
			Last_Change_Ts
		)
		SELECT	@Sitegroup_Id,
				ts.Site_Id,
				GETDATE()
		FROM @tvp_Site_Ids ts
		WHERE NOT EXISTS (	SELECT 1 
							FROM Sitegroup_Site ss 
							WHERE ss.Sitegroup_id = @Sitegroup_Id
							AND ss.Site_Id = ts.Site_Id)
		
		DELETE ss
		FROM Sitegroup_Site ss
		INNER JOIN Core.Client_Hier ch ON ch.Sitegroup_Id = ss.Sitegroup_id
		LEFT JOIN @tvp_Site_Ids ts ON ts.Site_Id = ss.Site_Id
		WHERE ss.Sitegroup_id = @Sitegroup_Id
		AND ts.Site_Id IS NULL
		AND ch.Hier_level_Cd = 100017 -- Site Group 

		COMMIT TRAN;
	END TRY
	BEGIN CATCH

		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRAN;
		END;

		EXEC dbo.usp_RethrowError;
	END CATCH;
END;

GO
GRANT EXECUTE ON  [dbo].[SiteGroup_Site_Ins_Del_For_Client_Data_Transfer] TO [CBMSApplication]
GO
