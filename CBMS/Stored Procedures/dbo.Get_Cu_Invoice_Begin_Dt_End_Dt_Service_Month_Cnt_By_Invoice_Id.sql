SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Get_Cu_Invoice_Begin_Dt_End_Dt_Service_Month_Cnt_By_Invoice_Id]

DESCRIPTION:
	To get the mininum begin date , maximum end date and distinct service month count of invoices based on the given input.
	Used in CU aggregation page.

INPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------
@Cu_Invoice_Id	INT
,@Account_Id	INT         NULL

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------  
	EXEC dbo.Get_Cu_Invoice_Begin_Dt_End_Dt_Service_Month_Cnt_By_Invoice_Id 3075909
	EXEC dbo.Get_Cu_Invoice_Begin_Dt_End_Dt_Service_Month_Cnt_By_Invoice_Id 7530478
	EXEC dbo.Get_Cu_Invoice_Begin_Dt_End_Dt_Service_Month_Cnt_By_Invoice_Id 7530478, 158109

	
	SELECT TOP 10 a.Cu_Invoice_Id FROM Cu_Invoice_Service_Month a JOIN cu_Invoice_Service_Month b ON b.Cu_Invoice_Id = a.Cu_Invoice_Id and a.Account_id <> b.Account_id
	WHERE a.Service_Month iS NOT NULL
	--Group by a.Cu_Invoice_Id HAVING COUNT(1) =2 
	ORDER BY a.CU_INVOICE_ID DESC
	
AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------          
HG		Harihara Suthan G

MODIFICATIONS
INITIALS	DATE		    MODIFICATION
------------------------------------------------------------          
HG		08/08/2011    Created for Additional data enhancement requirement

*/

CREATE PROCEDURE dbo.Get_Cu_Invoice_Begin_Dt_End_Dt_Service_Month_Cnt_By_Invoice_Id
      @Cu_Invoice_Id INT
     ,@Account_Id INT = NULL
AS 
BEGIN

      SET NOCOUNT ON

      SELECT
            min(sm.Begin_Dt) AS Min_Begin_Dt
           ,max(sm.End_Dt) AS Max_End_Dt
           ,count(DISTINCT Service_Month) AS Service_Month_Cnt
      FROM
            dbo.CU_INVOICE_SERVICE_MONTH sm
      WHERE
            sm.CU_INVOICE_ID = @Cu_Invoice_Id
            AND ( @Account_Id IS NULL
                  OR sm.Account_ID = @Account_Id )

END
;
GO
GRANT EXECUTE ON  [dbo].[Get_Cu_Invoice_Begin_Dt_End_Dt_Service_Month_Cnt_By_Invoice_Id] TO [CBMSApplication]
GO
