SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********                                                      
NAME: dbo.CalcVal_Sel_By_Cu_Invoice_Id_Account_Id_Commodity_Id                                                       
                                                    
DESCRIPTION:                                                         
                                                    
 To get the CalcVals based on the cu_invoice_id, account_id,Commodity_Id.                                                    
                                                     
INPUT PARAMETERS:                                                        
Name     DataType Default   Description                                                        
-------------------------------------------------------------------------                                                        
@Cu_Invoice_Id     INT                                                         
@Account_Id     INT                                                          
@Commodity_Id    INT                                                         
                                                    
OUTPUT PARAMETERS:                                                        
Name     DataType Default   Description                                                        
-------------------------------------------------------------------------                                                        
                                                    
USAGE EXAMPLES:                                                        
------------------------------------------------------------                                                        
                                                    
EXEC CalcVal_Sel_By_Cu_Invoice_Id_Account_Id_Commodity_Id 23957559,54239,291                                                    
                                                    
EXEC CalcVal_Sel_By_Cu_Invoice_Id_Account_Id_Commodity_Id 34837119,687143,290                                                    
                                                    
EXEC dbo.CalcVal_Sel_By_Cu_Invoice_Id_Account_Id_Commodity_Id 69966502,1532543,290                                                    
                                                      
                                                    
  SELECT * FROM CU_INVOIcE_SERVICE_MONTH WHERE CU_INVOICE_ID = 69966502                                                   
                                                  
AUTHOR INITIALS:                                                        
Initials Name                                                        
------------------------------------------------------------                                                        
RKV   Ravi Kumar Vegesna                                                    
NR   Narayana Reddy                                                    
VPIDAPARTHI Venkata Sriram Pavan Kumar Pidaparthi                                
VRV Venkata Redy Vanga                          
NM Nagaraju Muppa                                                            
SC	Sreenivasulu Cheerala
MODIFICATIONS                                                         
Initials Date  Modification                                                        
------------------------------------------------------------                                                        
RKV   2015-10-08 Created                                                    
RKV         2016-09-15  MAINT 4275  Added Federal Tax and Green Certificate charges to the result set                                                    
RKV         2016-09-15  MAINT 4193  Added Calc_Type and Is_Value_Locked to the result set                                                    
RKV         2016-11-18  MAINT 4563  Added Account_Group_Accounts.                                                    
RKV   2017-01-03 MAINT-4788  CalcVals - Start Date to tie in with meter attribute setting                                                    
HG   2017-02-17 if determinant values are same for the two differnt invoices then it is excluded in group by , modified the logic which inserts the data in to #Ec_Calc_Val_Determinant_Charge_Values_With_Uom to take the sum of calc value.          
HG   2017-02-20 MAINT-4952 Instead of summing up the determinant values while insert the value in to #Ec_Calc_Val_Determinant_Charge_Values_With_Uom,considering the determinant/charge id to keep the same value for different month                          
  
    
     
         
HG  2018-03-14 Pam Data Interval- added Locked_Calc_Value and new slots.                  
RKV  2018-08-13 Maint-7623 Modified the bucket_Masters logic based on account.                               
RKV  2018-10-03 D20-173 - Added New Billed usage total  buckets as priority logic.                                                  
VPIDAPARTHI 2019-10-03  SE2017-456 - Change code for CalcVals to always pull from the associated utility account                                                    
HG  2019-10-11 Reverted SE2017-456 changes are it was not working for all the country                                
VRV 2019-11-12  SE2017-875 - Changed code for CalcVals to the new functionlaity                          
NM  2020-03-20 Maint-9721 Added commodity join to get the default UOM_Name instade of getting NULL                                            
NM  2020-03-23 Maint-9551 Update as NULL if calc_value is zero to calculate MIN and AVG value currectly            
NM  2020-03-26 MAINT-10063 Added EC_Calc_Val table join to get updated calcval UOM_Name.      
NM  2020-04-24 MAINT-10063 Removed hard coded  UOM Names logic for demand and usage      
NM  2020-04-28 MAINT-10131  Added Sub_Bucket filter to consider sub bucket calc values if sub buckets not configured then it will show all determent values.      
SC  2020-04-10  B20-1331 - Added new input parameter @Attribute_Type Actual or Forecast                    
******/

CREATE PROCEDURE [dbo].[CalcVal_Sel_By_Cu_Invoice_Id_Account_Id_Commodity_Id]
    (
        @Cu_Invoice_Id INT
        , @Account_Id INT
        , @Commodity_Id INT
        , @Attribute_Type VARCHAR(20) = 'Actual'
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @state_Id INT
            , @Client_Id INT
            , @Sql_Str NVARCHAR(4000)
            , @Total_Row_cnt INT
            , @Row_Num INT = 1
            , @Service_Month DATE
            , @Default_Uom_Id INT
            , @Default_Currency_Id INT
            , @Default_Currency_Unit_Name VARCHAR(255)
            , @Start_Service_Month DATE
            , @End_Service_Month DATE
            , @Ec_Calc_Val_Id INT
            , @Calc_Value_Name NVARCHAR(255)
            , @charge_names VARCHAR(MAX)
            , @GreenCertificateYTD_Calc_Val_Name NVARCHAR(200) = N'GreenCertificateYTD'
            , @Client_Currency_Group_Id INT
            , @Determinant_Source VARCHAR(25)
            , @Cu_Invoice_Account_Commodity_Id INT
            , @Multiple_Option_Value_Selection VARCHAR(10)
            , @Multiple_Option_No_Of_Month INT
            , @Multiple_Option_Total_Aggregation VARCHAR(10)
            , @ORDER VARCHAR(10)
            , @Adjusted_Billing_Days INT
            , @Service_Period_Start_Date_Adjustment SMALLINT
            , @Service_Period_End_Date_Adjustment SMALLINT
            , @AccountType CHAR(8)
            , @Attribute_Type_Cd INT;



        DECLARE @Invoice_Bucket_Master_Ids TABLE
              (
                  Bucket_Master_Id INT NOT NULL
                  , Cu_Invoice_Id INT NOT NULL
                  , Account_Id INT NOT NULL
                  , SERVICE_MONTH DATETIME
              );

        DECLARE @Invoice_Ids_AdjustingBilling_Days TABLE
              (
                  Ec_Calc_Val_Id INT
                  , Cu_Invoice_Id INT NOT NULL
                  , Account_Id INT NOT NULL
                  , SERVICE_MONTH DATETIME
                  , Begin_Dt DATE
                  , End_Dt DATE
              );

        DECLARE @Ec_Calc_Val_Bucket_Master_Ids TABLE
              (
                  Ec_Calc_Val_Bucket_Map_Id INT NOT NULL
                  , Ec_Calc_Val_Id INT NOT NULL
                  , Bucket_Master_Id INT NOT NULL
                  , Cu_Invoice_Id INT NOT NULL
                  , Account_Id INT NOT NULL
                  , SERVICE_MONTH DATETIME
              );

        DECLARE @Ec_Calc_vals_Comments TABLE
              (
                  EC_Calc_Val_Id INT
                  , Calc_Value_Name NVARCHAR(255)
                  , Comment_Text VARCHAR(MAX)
              );

        CREATE TABLE dbo.#EC_Calc_Val
             (
                 sno INT IDENTITY(1, 1)
                 , EC_Calc_Val_Id INT
                 , Calc_Value_Name NVARCHAR(255)
                 , Starting_Period_Cd INT
                 , Starting_Period_Operator_Cd INT
                 , Starting_Period_Operand SMALLINT
                 , End_Period_Cd INT
                 , End_Period_Operator_Cd INT
                 , End_Period_Operand SMALLINT
                 , Aggregation_Cd INT
                 , Ec_Account_Group_Type_Id INT
                 , Start_Dt_Reference_Ec_Meter_Attribute_Id INT
                 , Start_Dt_Meter_Attribute_Precedence_Cd INT
                 , Aggregation_Val VARCHAR(50)
                 , Monthly_settings_Cd INT
                 , Monthly_Setting_Start_Month_Num INT
                 , Monthly_Setting_End_Month_Num INT
                 , Multiple_Option_Value_Selection_Cd INT
                 , Multiple_Option_No_Of_Month INT
                 , Multiple_Option_Total_Aggregation_Cd INT
                 , Calc_Val_Type_Cd INT
                 , Uom_Cd INT
                 , Supplier_Account_Source_Type_Cd INT
             ); ---Newly added Code                                                 

        CREATE TABLE #Ec_Calc_Val_Id_Service_Months
             (
                 Ec_Calc_Val_Id INT
                 , Start_Service_Month DATE
                 , End_Service_Month DATE
             );

        CREATE TABLE #Ec_Calc_Val_Determinant_Charge_Values
             (
                 Ec_Calc_Val_Id INT
                 , Calc_Value DECIMAL(32, 18)
                 , Uom_ID INT
                 , Bucket_Master_Id INT
                 , Cu_Invoice_Determinant_Charge_Id INT
                 , Is_Value_Locked BIT
                 , SERVICE_MONTH DATETIME
             );

        CREATE TABLE #Ec_Calc_Val_Determinant_Charge_Values_With_Uom
             (
                 Ec_Calc_Val_Id INT
                 , Calc_Value DECIMAL(32, 18)
                 , Uom_Name VARCHAR(200)
                 , Is_Value_Locked BIT
                 , SERVICE_MONTH DATETIME
             );

        CREATE TABLE #Ec_Calc_Val_Determinant_Charge_Values_With_Uom_Multiple
             (
                 Ec_Calc_Val_Id INT
                 , Calc_Value DECIMAL(32, 18)
                 , Uom_Name VARCHAR(200)
                 , Is_Value_Locked BIT
                 , SERVICE_MONTH DATETIME
                 , Multiple_Option_Value_Selection VARCHAR(10)
                 , Multiple_Option_No_Of_Month INT
                 , Multiple_Option_Total_Aggregation VARCHAR(10)
             );

        CREATE TABLE #Ec_Calc_Val_Adjusting_BillingDays
             (
                 Ec_Calc_Val_Id INT
                 , Account_Id INT
                 , SERVICE_MONTH DATE
                 , Begin_Dt DATE
                 , End_Dt DATE
                 , Billing_Days INT
             );

        CREATE TABLE #Ec_Calc_Val_Final_Result
             (
                 Ec_Calc_Val_Id INT
                 , Calc_Value_Name NVARCHAR(255)
                 , Calc_Value DECIMAL(32, 18)
                 , Locked_Calc_Value DECIMAL(32, 18)
                 , Uom_Name VARCHAR(200)
                 , Is_Value_Locked BIT
             );

        CREATE TABLE #Eligible_Calc_Val
             (
                 Ec_Calc_Val_Id INT
                 , Is_Eligible BIT
             );

        CREATE TABLE #Ec_Account_Group_Accounts
             (
                 Ec_Calc_Val_Id INT
                 , Account_Id INT
             );

        DECLARE @Calc_Val_Type TABLE
              (
                  Ec_Calc_Val_Id INT
                  , Calc_Type VARCHAR(50)
              );

        DECLARE @GreenCertificate TABLE
              (
                  Calc_Val_Name NVARCHAR(255)
                  , Charge_Name VARCHAR(250)
              );

        DECLARE @Recalc_Header TABLE
              (
                  Recalc_Header_Id INT
                  , Cu_Invoice_ID INT
                  , Ec_Calc_Val_Id INT
                  , SERVICE_MONTH DATETIME
                  , PRIMARY KEY CLUSTERED
                    (
                        Recalc_Header_Id
                        , Cu_Invoice_ID
                        , Ec_Calc_Val_Id
                        , SERVICE_MONTH
                    )
              );

        CREATE TABLE #Monthly_Settings
             (
                 Ec_Calc_Val_Id INT
                 , MONTH_NO INT
                 , MONTH_NAME VARCHAR(10)
             );

        DECLARE @Ec_Calc_vals_Meter_Attributes_Adjusted_Billing_Days TABLE
              (
                  Account_Id INT
                  , Meter_Id INT
                  , EC_Meter_Attribute_Id INT
                  , EC_Meter_Attribute_Name NVARCHAR(100)
                  , EC_Meter_Attribute_Value NVARCHAR(50)
              );
        SELECT
            @Attribute_Type_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            c.Code_Value = @Attribute_Type
            AND cs.Codeset_Name = 'MeterAttributeType';


        SELECT
            @AccountType = cha.Account_Type
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Account_Id = @Account_Id;

        SELECT
            @Determinant_Source = (   SELECT    TOP 1
                                                ds.Code_Value
                                      FROM
                                            dbo.Account_Commodity_Invoice_Recalc_Type acirt
                                            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                                                ON acirt.Account_Id = cism.Account_ID
                                            INNER JOIN dbo.Code c
                                                ON c.Code_Id = acirt.Invoice_Recalc_Type_Cd
                                            LEFT JOIN dbo.Code ds
                                                ON ds.Code_Id = acirt.Invoice_Recalc_Type_Cd
                                      WHERE
                                            cism.CU_INVOICE_ID = @Cu_Invoice_Id
                                            AND cism.SERVICE_MONTH BETWEEN acirt.Start_Dt
                                                                   AND     ISNULL(acirt.End_Dt, '2099-12-01')
                                            AND c.Code_Value <> 'No Recalc'
                                            AND acirt.Invoice_Recalc_Type_Cd IS NOT NULL
                                      ORDER BY
                                          cism.SERVICE_MONTH DESC);

        SELECT
            @charge_names = ac.App_Config_Value
        FROM
            dbo.App_Config ac
        WHERE
            ac.App_Config_Cd = 'GreenCertificateYTD';

        INSERT INTO @GreenCertificate
             (
                 Calc_Val_Name
                 , Charge_Name
             )
        SELECT
            'GreenCertificateYTD'
            , us.Segments
        FROM
            dbo.ufn_split(@charge_names, '|') us;

        SELECT
            @Default_Currency_Id = COALESCE(ci.CURRENCY_UNIT_ID, cucm.CURRENCY_UNIT_ID)
            , @Client_Currency_Group_Id = ch.Client_Currency_Group_Id
        FROM
            dbo.CU_INVOICE ci
            LEFT OUTER JOIN(dbo.CU_INVOICE_SERVICE_MONTH cism
                            INNER JOIN Core.Client_Hier_Account cha
                                ON cha.Account_Id = cism.Account_ID
                            INNER JOIN Core.Client_Hier ch
                                ON ch.Client_Hier_Id = cha.Client_Hier_Id
                            INNER JOIN dbo.CURRENCY_UNIT_COUNTRY_MAP cucm
                                ON ch.Country_Id = cucm.COUNTRY_ID)
                ON ci.CU_INVOICE_ID = cism.CU_INVOICE_ID
                   AND  cism.Account_ID = @Account_Id
        WHERE
            ci.CU_INVOICE_ID = @Cu_Invoice_Id;


        SELECT
            @Default_Currency_Unit_Name = cu.CURRENCY_UNIT_NAME
        FROM
            dbo.CURRENCY_UNIT cu
        WHERE
            cu.CURRENCY_UNIT_ID = @Default_Currency_Id;

        SELECT
            @state_Id = cha.Meter_State_Id
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Account_Id = @Account_Id
            AND cha.Commodity_Id = @Commodity_Id;

        SELECT
            @Client_Id = ch.Client_Id
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN Core.Client_Hier ch
                ON cha.Client_Hier_Id = ch.Client_Hier_Id
        WHERE
            cha.Account_Id = @Account_Id
            AND cha.Commodity_Id = @Commodity_Id;

        SELECT
            @Service_Month = MAX(cism.SERVICE_MONTH)
        FROM
            dbo.CU_INVOICE_SERVICE_MONTH cism
        WHERE
            cism.CU_INVOICE_ID = @Cu_Invoice_Id
            AND cism.Account_ID = @Account_Id;


        SELECT
            @Cu_Invoice_Account_Commodity_Id = ciac.Cu_Invoice_Account_Commodity_Id
        FROM
            dbo.Cu_Invoice_Account_Commodity ciac
        WHERE
            ciac.Cu_Invoice_Id = @Cu_Invoice_Id
            AND ciac.Account_Id = @Account_Id
            AND ciac.Commodity_Id = @Commodity_Id;


        INSERT INTO #Eligible_Calc_Val
             (
                 Ec_Calc_Val_Id
                 , Is_Eligible
             )
        SELECT
            x.EC_Calc_Val_Id
            , CASE WHEN MAX(x.EC_Meter_Attribute_Id) IS NULL THEN 1
                  WHEN MAX(x.EC_Meter_Attribute_Id) IS NOT NULL
                       AND  MIN(x.Has_Calc_Value_Matched_With_AcccountMeter) <> 0 THEN 1
                  ELSE 0
              END
        FROM    (   SELECT
                        ecvma.EC_Calc_Val_Id
                        , ecvma.EC_Meter_Attribute_Id
                        , ecvma.EC_Meter_Attribute_Value
                        , CASE WHEN MAX(emat.EC_Meter_Attribute_Id) IS NOT NULL THEN 1
                              ELSE 0
                          END Has_Calc_Value_Matched_With_AcccountMeter
                    FROM
                        dbo.EC_Calc_Val ecv
                        LEFT OUTER JOIN dbo.EC_Calc_Val_Meter_Attribute ecvma
                            ON ecvma.EC_Calc_Val_Id = ecv.EC_Calc_Val_Id
                        LEFT JOIN(dbo.EC_Meter_Attribute_Tracking emat
                                  INNER JOIN Core.Client_Hier_Account cha
                                      ON cha.Meter_Id = emat.Meter_Id
                                         AND   emat.Meter_Attribute_Type_Cd = @Attribute_Type_Cd)
                            ON ecvma.EC_Meter_Attribute_Id = emat.EC_Meter_Attribute_Id
                               AND ecvma.EC_Meter_Attribute_Value = emat.EC_Meter_Attribute_Value
                               AND cha.Account_Id = @Account_Id
                               AND cha.Commodity_Id = @Commodity_Id
                               AND @Service_Month BETWEEN ISNULL(emat.Start_Dt, '1900-01-01')
                                                  AND     ISNULL(emat.End_Dt, '2099-01-01')
                    WHERE
                        ecv.State_Id = @state_Id
                        AND ecv.Commodity_Id = @Commodity_Id
                    GROUP BY
                        ecvma.EC_Meter_Attribute_Value
                        , ecvma.EC_Meter_Attribute_Id
                        , ecvma.EC_Calc_Val_Id) x
        GROUP BY
            x.EC_Calc_Val_Id
        UNION ALL
        SELECT
            ecv.EC_Calc_Val_Id
            , Is_Eligible = 1
        FROM
            dbo.EC_Calc_Val ecv
            LEFT OUTER JOIN dbo.EC_Calc_Val_Meter_Attribute ecvma
                ON ecvma.EC_Calc_Val_Id = ecv.EC_Calc_Val_Id
        WHERE
            ecv.State_Id = @state_Id
            AND ecv.Commodity_Id = @Commodity_Id
            AND ecvma.EC_Meter_Attribute_Id IS NULL;

        --SELECT * FROM #Eligible_Calc_Val                                                

        INSERT INTO #EC_Calc_Val
             (
                 EC_Calc_Val_Id
                 , Calc_Value_Name
                 , Starting_Period_Cd
                 , Starting_Period_Operator_Cd
                 , Starting_Period_Operand
                 , End_Period_Cd
                 , End_Period_Operator_Cd
                 , End_Period_Operand
                 , Aggregation_Cd
                 , Ec_Account_Group_Type_Id
                 , Start_Dt_Reference_Ec_Meter_Attribute_Id
                 , Start_Dt_Meter_Attribute_Precedence_Cd
                 , Aggregation_Val
                 , Monthly_settings_Cd
                 , Monthly_Setting_Start_Month_Num
                 , Monthly_Setting_End_Month_Num
                 , Multiple_Option_Value_Selection_Cd
                 , Multiple_Option_No_Of_Month
                 , Multiple_Option_Total_Aggregation_Cd
                 , Calc_Val_Type_Cd
                 , Uom_Cd
                 , Supplier_Account_Source_Type_Cd
             )
        SELECT
            ecv.EC_Calc_Val_Id
            , ecv.Calc_Value_Name
            , ecv.Starting_Period_Cd
            , ecv.Starting_Period_Operator_Cd
            , ecv.Starting_Period_Operand
            , ecv.End_Period_Cd
            , ecv.End_Period_Operator_Cd
            , ecv.End_Period_Operand
            , ecv.Aggregation_Cd
            , ecv.Ec_Account_Group_Type_Id
            , ecv.Start_Dt_Reference_Ec_Meter_Attribute_Id
            , ecv.Start_Dt_Meter_Attribute_Precedence_Cd
            , c.Code_Value
            , ecv.Monthly_settings_Cd
            , ecv.Monthly_Setting_Start_Month_Num
            , ecv.Monthly_Setting_End_Month_Num
            , ecv.Multiple_Option_Value_Selection_Cd
            , ecv.Multiple_Option_No_Of_Month
            , ecv.Multiple_Option_Total_Aggregation_Cd
            , ecv.Calc_Val_Type_Cd
            , ecv.Uom_Cd
            , ecv.Supplier_Account_Source_Type_Cd
        FROM
            dbo.EC_Calc_Val ecv
            JOIN dbo.Code c
                ON ecv.Aggregation_Cd = c.Code_Id   ---Join condition Newly added Code                                                
        WHERE
            ecv.State_Id = @state_Id
            AND ecv.Commodity_Id = @Commodity_Id
            AND EXISTS (   SELECT
                                1
                           FROM
                                #Eligible_Calc_Val ec
                           WHERE
                                ec.Ec_Calc_Val_Id = ecv.EC_Calc_Val_Id
                                AND ec.Is_Eligible = 1)
        GROUP BY
            ecv.EC_Calc_Val_Id
            , ecv.Calc_Value_Name
            , ecv.Starting_Period_Cd
            , ecv.Starting_Period_Operator_Cd
            , ecv.Starting_Period_Operand
            , ecv.End_Period_Cd
            , ecv.End_Period_Operator_Cd
            , ecv.End_Period_Operand
            , ecv.Aggregation_Cd
            , ecv.Ec_Account_Group_Type_Id
            , ecv.Start_Dt_Reference_Ec_Meter_Attribute_Id
            , ecv.Start_Dt_Meter_Attribute_Precedence_Cd
            , c.Code_Value
            , ecv.Monthly_settings_Cd
            , ecv.Monthly_Setting_Start_Month_Num
            , ecv.Monthly_Setting_End_Month_Num
            , ecv.Multiple_Option_Value_Selection_Cd
            , ecv.Multiple_Option_No_Of_Month
            , ecv.Multiple_Option_Total_Aggregation_Cd
            , ecv.Calc_Val_Type_Cd
            , ecv.Uom_Cd
            , ecv.Supplier_Account_Source_Type_Cd;

        --SELECT * FROM #EC_Calc_Val                                                
        INSERT INTO #Ec_Calc_Val_Final_Result
             (
                 Ec_Calc_Val_Id
                 , Calc_Value_Name
                 , Locked_Calc_Value
                 , Uom_Name
                 , Is_Value_Locked
             )
        SELECT
            ciacecv.Ec_Calc_Val_Id
            , ecv.Calc_Value_Name
            , ciacecv.Calc_Value
            , ISNULL(uom_type.ENTITY_NAME, Charge_type.CURRENCY_UNIT_NAME)
            , 1
        FROM
            dbo.Cu_Invoice_Account_Commodity ciac
            INNER JOIN dbo.Cu_Invoice_Account_Commodity_EC_Calc_Value ciacecv
                ON ciac.Cu_Invoice_Account_Commodity_Id = ciacecv.Cu_Invoice_Account_Commodity_Id
            INNER JOIN #EC_Calc_Val ecv
                ON ecv.EC_Calc_Val_Id = ciacecv.Ec_Calc_Val_Id
            LEFT OUTER JOIN dbo.ENTITY uom_type
                ON uom_type.ENTITY_ID = ciacecv.Uom_Id
            LEFT OUTER JOIN dbo.CURRENCY_UNIT Charge_type
                ON Charge_type.CURRENCY_UNIT_ID = ciacecv.Currency_Unit_Id
        WHERE
            ciac.Account_Id = @Account_Id
            AND ciac.Commodity_Id = @Commodity_Id
            AND ciac.Cu_Invoice_Id = @Cu_Invoice_Id
            AND ciacecv.Is_Value_Locked = 1;



        INSERT INTO @Ec_Calc_vals_Comments
             (
                 EC_Calc_Val_Id
                 , Calc_Value_Name
                 , Comment_Text
             )
        SELECT
            ecv.EC_Calc_Val_Id
            , ecv.Calc_Value_Name
            , ISNULL(c.Comment_Text, '')
        FROM
            #EC_Calc_Val ecv
            LEFT OUTER JOIN(dbo.Cu_Invoice_Account_Commodity_EC_Calc_Value ciacecv
                            INNER JOIN dbo.Cu_Invoice_Account_Commodity ciac
                                ON ciac.Cu_Invoice_Account_Commodity_Id = ciacecv.Cu_Invoice_Account_Commodity_Id
                                   AND  ciac.Cu_Invoice_Id = @Cu_Invoice_Id
                                   AND  ciac.Account_Id = @Account_Id
                                   AND  ciac.Commodity_Id = @Commodity_Id
                            INNER JOIN dbo.Comment c
                                ON c.Comment_ID = ciacecv.Comment_Id)
                ON ciacecv.Ec_Calc_Val_Id = ecv.EC_Calc_Val_Id
        GROUP BY
            ecv.EC_Calc_Val_Id
            , ecv.Calc_Value_Name
            , c.Comment_Text;



        IF @Determinant_Source = 'Invoice'
            BEGIN

                DELETE
                ecv
                FROM
                    #EC_Calc_Val ecv
                    INNER JOIN #Ec_Calc_Val_Final_Result ecvfr
                        ON ecvfr.Ec_Calc_Val_Id = ecv.EC_Calc_Val_Id;
            END;

        INSERT INTO @Calc_Val_Type
             (
                 Ec_Calc_Val_Id
                 , Calc_Type
             )
        SELECT
            ecv.EC_Calc_Val_Id
            , MAX(CASE WHEN (bt.Code_Value = 'Charge')
                            OR  (   ecv.Calc_Value_Name = @GreenCertificateYTD_Calc_Val_Name
                                    AND bt.Code_Value IS NULL) THEN 'Recalc Charge'
                      ELSE 'Invoice Determinant'
                  END)
        FROM
            @Ec_Calc_vals_Comments ecv
            LEFT OUTER JOIN(dbo.Ec_Calc_Val_Bucket_Map ecvbm
                            INNER JOIN dbo.Bucket_Master bm
                                ON bm.Bucket_Master_Id = ecvbm.Bucket_Master_Id
                            INNER JOIN dbo.Code bt
                                ON bt.Code_Id = bm.Bucket_Type_Cd)
                ON ecv.EC_Calc_Val_Id = ecvbm.Ec_Calc_Val_Id
        GROUP BY
            ecv.EC_Calc_Val_Id;



        SELECT  @Total_Row_cnt = MAX(sno)FROM   #EC_Calc_Val;

        WHILE @Total_Row_cnt >= @Row_Num
            BEGIN



                SELECT
                    @Sql_Str = N'INSERT INTO #Ec_Calc_Val_Id_Service_Months                                                    
             SELECT                                                    
              EC_Calc_Val_Id,' + CASE WHEN ssm.Period_UDF IS NOT NULL THEN
                                          'DATEADD(m,' + spo.Code_Value + ''
                                          + CAST(ecv.Starting_Period_Operand AS VARCHAR) + ',dbo.' + ssm.Period_UDF
                                          + '(''' + CAST(@Service_Month AS VARCHAR) + '''))'
                                     WHEN ssm.Period_UDF IS NULL THEN
                                         'DATEADD(m,' + spo.Code_Value + ''
                                         + CAST(ecv.Starting_Period_Operand AS VARCHAR) + ','''
                                         + CAST(@Service_Month AS VARCHAR) + ''')'
                                 END + N','
                               + CASE WHEN esm.Period_UDF IS NOT NULL THEN
                                          'DATEADD(m,' + epo.Code_Value + '' + CAST(ecv.End_Period_Operand AS VARCHAR)
                                          + ',dbo.' + esm.Period_UDF + '(''' + CAST(@Service_Month AS VARCHAR) + '''))'
                                     WHEN esm.Period_UDF IS NULL THEN
                                         'DATEADD(m,' + epo.Code_Value + '' + CAST(ecv.End_Period_Operand AS VARCHAR)
                                         + ',''' + CAST(@Service_Month AS VARCHAR) + ''')'
                                 END
                               + N'                                                     
                                                    FROM                                                     
              #Ec_Calc_Val                                          
             WHERE                                                     
              sno = ' + CAST(@Row_Num AS VARCHAR)
                FROM
                    #EC_Calc_Val ecv
                    INNER JOIN dbo.Code spo
                        ON spo.Code_Id = ecv.Starting_Period_Operator_Cd
                    INNER JOIN dbo.Code epo
                        ON epo.Code_Id = ecv.End_Period_Operator_Cd
                    LEFT OUTER JOIN dbo.Ec_Calc_Val_Period_UDF ssm
                        ON ssm.Period_Cd = ecv.Starting_Period_Cd
                    LEFT OUTER JOIN dbo.Ec_Calc_Val_Period_UDF esm
                        ON esm.Period_Cd = ecv.End_Period_Cd
                WHERE
                    ecv.sno = @Row_Num;

                EXECUTE (@Sql_Str);



                SELECT
                    @End_Service_Month = ecvism.End_Service_Month
                    , @Ec_Calc_Val_Id = ecv.EC_Calc_Val_Id
                    , @Calc_Value_Name = ecv.Calc_Value_Name
                FROM
                    #Ec_Calc_Val_Id_Service_Months ecvism
                    INNER JOIN #EC_Calc_Val ecv
                        ON ecvism.Ec_Calc_Val_Id = ecv.EC_Calc_Val_Id;

                SELECT
                    @Start_Service_Month = ecvism.Start_Service_Month
                FROM
                    #Ec_Calc_Val_Id_Service_Months ecvism
                    INNER JOIN #EC_Calc_Val ecv
                        ON ecvism.Ec_Calc_Val_Id = ecv.EC_Calc_Val_Id
                WHERE
                    ecv.Start_Dt_Reference_Ec_Meter_Attribute_Id IS NULL;

                IF EXISTS (   SELECT
                                    1
                              FROM
                                    #EC_Calc_Val ecv
                                    LEFT JOIN Code c
                                        ON ecv.Supplier_Account_Source_Type_Cd = c.Code_Id
                              WHERE
                                    (   c.Code_Value = 'Utility'
                                        OR  c.Code_Value IS NULL)
                                    AND ecv.sno = @Row_Num
                                    AND @AccountType = 'Supplier')
                    BEGIN
                        INSERT INTO #Ec_Account_Group_Accounts
                             (
                                 Ec_Calc_Val_Id
                                 , Account_Id
                             )
                        SELECT
                            ecv.EC_Calc_Val_Id
                            , UCha.Account_Id
                        FROM
                            #EC_Calc_Val ecv
                            CROSS JOIN(Core.Client_Hier_Account SCha
                                       INNER JOIN Core.Client_Hier_Account UCha
                                           ON SCha.Meter_Id = UCha.Meter_Id
                                              AND   UCha.Commodity_Id = SCha.Commodity_Id
                                              AND   SCha.Account_Id = @Account_Id
                                              AND   SCha.Commodity_Id = @Commodity_Id
                                              AND   UCha.Account_Type = 'Utility'
                                              AND   SCha.Account_Type = 'Supplier')
                        WHERE
                            ecv.sno = @Row_Num
                        GROUP BY
                            ecv.EC_Calc_Val_Id
                            , UCha.Account_Id;
                    END;

                ELSE
                    BEGIN

                        INSERT INTO #Ec_Account_Group_Accounts
                             (
                                 Ec_Calc_Val_Id
                                 , Account_Id
                             )
                        SELECT
                            ecv.EC_Calc_Val_Id
                            , @Account_Id
                        FROM
                            #EC_Calc_Val ecv
                        WHERE
                            NOT EXISTS (   SELECT
                                                1
                                           FROM
                                                #Ec_Account_Group_Accounts eaga
                                           WHERE
                                                eaga.Ec_Calc_Val_Id = ecv.EC_Calc_Val_Id
                                                AND eaga.Account_Id = @Account_Id)
                            AND ecv.sno = @Row_Num;

                    END;

                INSERT INTO #Ec_Account_Group_Accounts
                     (
                         Ec_Calc_Val_Id
                         , Account_Id
                     )
                SELECT
                    ecv.EC_Calc_Val_Id
                    , ecaga.Account_Id
                FROM
                    #EC_Calc_Val ecv
                    INNER JOIN #Ec_Calc_Val_Id_Service_Months ecsm
                        ON ecsm.Ec_Calc_Val_Id = ecv.EC_Calc_Val_Id
                    INNER JOIN dbo.Ec_Account_Group_Type eagt
                        ON eagt.Ec_Account_Group_Type_Id = ecv.Ec_Account_Group_Type_Id
                    INNER JOIN dbo.Ec_Client_Account_Group ecag
                        ON ecag.Ec_Account_Group_Type_Id = eagt.Ec_Account_Group_Type_Id
                    INNER JOIN dbo.Ec_Client_Account_Group_Account ecaga
                        ON ecaga.Ec_Client_Account_Group_Id = ecag.Ec_Client_Account_Group_Id
                WHERE
                    @Service_Month BETWEEN ecag.Start_Dt
                                   AND     ISNULL(ecag.End_Dt, '2999-12-01')
                    AND NOT EXISTS (   SELECT
                                            1
                                       FROM
                                            #Ec_Account_Group_Accounts eaga
                                       WHERE
                                            eaga.Ec_Calc_Val_Id = ecv.EC_Calc_Val_Id
                                            AND eaga.Account_Id = ecaga.Account_Id)
                    AND ecv.sno = @Row_Num
                    AND ecag.Client_Id = @Client_Id
                    -- To ensure that account is part of the group account , added to exists clause so that all other accounts of the group account is considered                                                  
                    AND EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.Ec_Client_Account_Group_Account ga
                                   WHERE
                                        ga.Account_Id = @Account_Id
                                        AND ga.Ec_Client_Account_Group_Id = ecaga.Ec_Client_Account_Group_Id);



                SELECT
                    @Start_Service_Month = CASE WHEN sdmac.Code_Value = 'Most Recent' THEN MAX(emat.Start_Dt)
                                               ELSE MIN(emat.Start_Dt)
                                           END
                FROM
                    #Ec_Calc_Val_Id_Service_Months ecvism
                    INNER JOIN #EC_Calc_Val ecv
                        ON ecv.EC_Calc_Val_Id = ecvism.Ec_Calc_Val_Id
                    INNER JOIN dbo.EC_Meter_Attribute_Tracking emat
                        ON emat.EC_Meter_Attribute_Id = ecv.Start_Dt_Reference_Ec_Meter_Attribute_Id
                           AND  emat.Start_Dt < @Service_Month
                    INNER JOIN #Ec_Account_Group_Accounts eaga
                        ON eaga.Ec_Calc_Val_Id = ecvism.Ec_Calc_Val_Id
                    INNER JOIN Core.Client_Hier_Account cha
                        ON cha.Account_Id = eaga.Account_Id
                           AND  cha.Meter_Id = emat.Meter_Id
                    INNER JOIN dbo.Code sdmac
                        ON sdmac.Code_Id = ecv.Start_Dt_Meter_Attribute_Precedence_Cd
                WHERE
                    ecv.Start_Dt_Reference_Ec_Meter_Attribute_Id IS NOT NULL
                    AND emat.Meter_Attribute_Type_Cd = @Attribute_Type_Cd
                GROUP BY
                    sdmac.Code_Value;


                IF EXISTS (   SELECT
                                    1
                              FROM
                                    #EC_Calc_Val
                              WHERE
                                    EC_Calc_Val_Id = @Ec_Calc_Val_Id
                                    AND Monthly_Setting_Start_Month_Num > Monthly_Setting_End_Month_Num)
                    BEGIN

                        INSERT INTO #Monthly_Settings
                             (
                                 Ec_Calc_Val_Id
                                 , MONTH_NO
                                 , MONTH_NAME
                             )
                        SELECT
                            Ec_Calc_Val_Id = @Ec_Calc_Val_Id
                            , MONTH_NUM
                            , MONTH_NAME
                        FROM
                            meta.DATE_DIM
                        WHERE
                            MONTH_NUM >= (   SELECT
                                                    MONTH_NUM
                                             FROM
                                                    meta.DATE_DIM
                                             WHERE
                                                 YEAR_NUM = 1900
                                                 AND MONTH_NUM = (   SELECT
                                                                            Monthly_Setting_Start_Month_Num
                                                                     FROM
                                                                            dbo.EC_Calc_Val
                                                                     WHERE
                                                                         EC_Calc_Val_Id = @Ec_Calc_Val_Id))
                            AND YEAR_NUM = 1900
                        UNION
                        SELECT
                            Ec_Calc_Val_Id = @Ec_Calc_Val_Id
                            , MONTH_NUM
                            , MONTH_NAME
                        FROM
                            meta.DATE_DIM
                        WHERE
                            MONTH_NUM <= (   SELECT
                                                    MONTH_NUM
                                             FROM
                                                    meta.DATE_DIM
                                             WHERE
                                                 YEAR_NUM = 1900
                                                 AND MONTH_NUM = (   SELECT
                                                                            Monthly_Setting_End_Month_Num
                                                                     FROM
                                                                            dbo.EC_Calc_Val
                                                                     WHERE
                                                                         EC_Calc_Val_Id = @Ec_Calc_Val_Id))
                            AND YEAR_NUM = 1900;


                    END;

                ELSE
                    BEGIN

                        INSERT INTO #Monthly_Settings
                             (
                                 Ec_Calc_Val_Id
                                 , MONTH_NO
                                 , MONTH_NAME
                             )
                        SELECT
                            Ec_Calc_Val_Id = @Ec_Calc_Val_Id
                            , MONTH_NUM
                            , MONTH_NAME
                        FROM
                            meta.DATE_DIM
                        WHERE
                            MONTH_NUM >= (   SELECT
                                                    MONTH_NUM
                                             FROM
                                                    meta.DATE_DIM
                                             WHERE
                                                 YEAR_NUM = 1900
                                                 AND MONTH_NUM = (   SELECT
                                                                            Monthly_Setting_Start_Month_Num
                                                                     FROM
                                                                            dbo.EC_Calc_Val
                                                                     WHERE
                                                                         EC_Calc_Val_Id = @Ec_Calc_Val_Id))
                            AND MONTH_NUM <= (   SELECT
                                                        MONTH_NUM
                                                 FROM
                                                        meta.DATE_DIM
                                                 WHERE
                                                     YEAR_NUM = 1900
                                                     AND MONTH_NUM = (   SELECT
                                                                                Monthly_Setting_End_Month_Num
                                                                         FROM
                                                                                dbo.EC_Calc_Val
                                                                         WHERE
                                                                             EC_Calc_Val_Id = @Ec_Calc_Val_Id))
                            AND YEAR_NUM = 1900;


                    END;


                IF EXISTS (   SELECT
                                    1
                              FROM
                                    #EC_Calc_Val ecv
                                    LEFT JOIN dbo.Code c
                                        ON ecv.Calc_Val_Type_Cd = c.Code_Id
                              WHERE
                                    c.Code_Value <> 'Adjusted Billing Days'
                                    OR  ecv.Calc_Val_Type_Cd IS NULL)
                    BEGIN




                        INSERT INTO @Invoice_Bucket_Master_Ids
                             (
                                 Bucket_Master_Id
                                 , Cu_Invoice_Id
                                 , Account_Id
                                 , SERVICE_MONTH
                             )
                        SELECT
                            bm.Bucket_Master_Id
                            , cuid.CU_INVOICE_ID
                            , cism.Account_ID
                            , cism.SERVICE_MONTH
                        FROM
                            dbo.CU_INVOICE_SERVICE_MONTH cism
                            INNER JOIN dbo.CU_INVOICE inv
                                ON inv.CU_INVOICE_ID = cism.CU_INVOICE_ID
                            INNER JOIN dbo.CU_INVOICE_DETERMINANT cuid
                                ON cism.CU_INVOICE_ID = cuid.CU_INVOICE_ID
                            INNER JOIN dbo.CU_INVOICE_DETERMINANT_ACCOUNT cuida
                                ON cuid.CU_INVOICE_DETERMINANT_ID = cuida.CU_INVOICE_DETERMINANT_ID
                                   AND  cism.Account_ID = cuida.ACCOUNT_ID
                            INNER JOIN dbo.Bucket_Master bm
                                ON bm.Bucket_Master_Id = cuid.Bucket_Master_Id
                            INNER JOIN #Ec_Account_Group_Accounts eaga
                                ON cuida.ACCOUNT_ID = eaga.Account_Id
                        WHERE
                            cism.SERVICE_MONTH BETWEEN @Start_Service_Month
                                               AND     @End_Service_Month
                            AND cuid.COMMODITY_TYPE_ID = @Commodity_Id
                            AND (   inv.CU_INVOICE_ID = @Cu_Invoice_Id
                                    OR  (   inv.IS_REPORTED = 1
                                            AND inv.IS_PROCESSED = 1))  --AND MOnth(cism.SERVICE_MONTH )                         
                        --IN (SELECT MONTH_NO FROM #Monthly_Settings)                                                

                        GROUP BY
                            bm.Bucket_Master_Id
                            , cuid.CU_INVOICE_ID
                            , cism.Account_ID
                            , SERVICE_MONTH;


                        INSERT INTO @Ec_Calc_Val_Bucket_Master_Ids
                             (
                                 Ec_Calc_Val_Bucket_Map_Id
                                 , Ec_Calc_Val_Id
                                 , Bucket_Master_Id
                                 , Cu_Invoice_Id
                                 , Account_Id
                                 , SERVICE_MONTH
                             )
                        SELECT
                            ecvbm.Ec_Calc_Val_Bucket_Map_Id
                            , ecvbm.Ec_Calc_Val_Id
                            , ecvbm.Bucket_Master_Id
                            , ibmi.Cu_Invoice_Id
                            , ibmi.Account_Id
                            , ibmi.SERVICE_MONTH
                        FROM
                            dbo.Ec_Calc_Val_Bucket_Map ecvbm
                            CROSS JOIN (   SELECT
                                                ibmi.Cu_Invoice_Id
                                                , ibmi.Account_Id
                                                , SERVICE_MONTH
                                           FROM
                                                @Invoice_Bucket_Master_Ids ibmi
                                           GROUP BY
                                               ibmi.Cu_Invoice_Id
                                               , ibmi.Account_Id
                                               , SERVICE_MONTH) ibmi
                        WHERE
                            ecvbm.Ec_Calc_Val_Id = @Ec_Calc_Val_Id;



                        INSERT INTO @Ec_Calc_Val_Bucket_Master_Ids
                             (
                                 Ec_Calc_Val_Bucket_Map_Id
                                 , Ec_Calc_Val_Id
                                 , Bucket_Master_Id
                                 , Cu_Invoice_Id
                                 , Account_Id
                                 , SERVICE_MONTH
                             )
                        SELECT
                            -1
                            , ecv.EC_Calc_Val_Id
                            , -1
                            , ibmi.Cu_Invoice_Id
                            , ibmi.Account_Id
                            , ibmi.SERVICE_MONTH
                        FROM
                            #EC_Calc_Val ecv
                            INNER JOIN dbo.App_Config ac
                                ON ecv.Calc_Value_Name = ac.App_Config_Cd
                            CROSS JOIN (   SELECT
                                                ibmi.Cu_Invoice_Id
                                                , ibmi.Account_Id
                                                , SERVICE_MONTH
                                           FROM
                                                @Invoice_Bucket_Master_Ids ibmi
                                           GROUP BY
                                               ibmi.Cu_Invoice_Id
                                               , ibmi.Account_Id
                                               , SERVICE_MONTH) ibmi
                        WHERE
                            NOT EXISTS (   SELECT
                                                1
                                           FROM
                                                @Ec_Calc_Val_Bucket_Master_Ids ecvbm
                                           WHERE
                                                ecvbm.Ec_Calc_Val_Id = ecv.EC_Calc_Val_Id)
                            AND ecv.EC_Calc_Val_Id = @Ec_Calc_Val_Id;



                        /*Delete the records of other related buckets if the 'Usage - Total', 'Billed Usage - Total'  , 'Total Volume' is selected  */

                        DELETE
                        ecv_ob
                        FROM
                            @Ec_Calc_Val_Bucket_Master_Ids ecv_ob
                            INNER JOIN @Ec_Calc_Val_Bucket_Master_Ids ecv
                                ON ecv.Ec_Calc_Val_Id = ecv_ob.Ec_Calc_Val_Id
                                   AND  ecv.Account_Id = ecv_ob.Account_Id
                                   AND  ecv.Cu_Invoice_Id = ecv_ob.Cu_Invoice_Id
                        WHERE
                            EXISTS (   SELECT
                                            1
                                       FROM
                                            dbo.Bucket_Master bm
                                            INNER JOIN @Ec_Calc_Val_Bucket_Master_Ids ec
                                                ON ec.Bucket_Master_Id = bm.Bucket_Master_Id
                                       WHERE
                                            bm.Bucket_Master_Id = ecv_ob.Bucket_Master_Id
                                            AND ec.Account_Id = ecv_ob.Account_Id
                                            AND ec.Cu_Invoice_Id = ecv_ob.Cu_Invoice_Id
                                            AND bm.Bucket_Name IN ( 'Usage - Mid-Peak', 'Usage - Miscellaneous'
                                                                    , 'Usage - Off-Peak', 'Usage - On-Peak'
                                                                    , 'Usage - Other-Peak', 'Baseload Volume'
                                                                    , 'Cashout Volume', 'Miscellaneous Volume'
                                                                    , 'Swing Volume', 'Fuel Volume'
                                                                    , 'Billed Usage - Off-Peak'
                                                                    , 'Billed Usage - Mid-Peak'
                                                                    , 'Billed Usage - Miscellaneous'
                                                                    , 'Billed Usage - Other-Peak'
                                                                    , 'Billed Usage - On-Peak'
                                                                    , 'Billed  Baseload Volume'
                                                                    , 'Billed Cashout Volume', 'Billed Swing Volume'
                                                                    , 'Billed Fuel Volume'
                                                                    , 'Billed Miscellaneous Volume' ))
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.Bucket_Master bm
                                                INNER JOIN @Ec_Calc_Val_Bucket_Master_Ids ec
                                                    ON ec.Bucket_Master_Id = bm.Bucket_Master_Id
                                           WHERE
                                                bm.Bucket_Master_Id = ecv.Bucket_Master_Id
                                                AND ec.Account_Id = ecv.Account_Id
                                                AND ec.Cu_Invoice_Id = ecv.Cu_Invoice_Id
                                                AND bm.Bucket_Name IN ( 'Usage - Total', 'Billed Usage - Total'
                                                                        , 'Total Volume', 'Billed Total Volume' ));



                        /*In the above delete all the buckets will remain if the bucket 'Usage - Total' and  'Billed Usage - Total' is not selected,                                                    
        now we will check for Total usage child buckets, if it is selected then we will delete the Billed Usage child buckets */

                        DELETE
                        ecv_ob
                        FROM
                            @Ec_Calc_Val_Bucket_Master_Ids ecv_ob
                            INNER JOIN @Ec_Calc_Val_Bucket_Master_Ids ecv
                                ON ecv.Ec_Calc_Val_Id = ecv_ob.Ec_Calc_Val_Id
                                   AND  ecv.Account_Id = ecv_ob.Account_Id
                                   AND  ecv.Cu_Invoice_Id = ecv_ob.Cu_Invoice_Id
                        WHERE
                            EXISTS (   SELECT
                                            1
                                       FROM
                                            dbo.Bucket_Master bm
                                            INNER JOIN @Ec_Calc_Val_Bucket_Master_Ids ec
                                                ON ec.Bucket_Master_Id = bm.Bucket_Master_Id
                                       WHERE
                                            bm.Bucket_Master_Id = ecv_ob.Bucket_Master_Id
                                            AND ec.Account_Id = ecv_ob.Account_Id
                                            AND ec.Cu_Invoice_Id = ecv_ob.Cu_Invoice_Id
                                            AND bm.Bucket_Name IN ( 'Billed Usage - Off-Peak'
                                                                    , 'Billed Usage - Mid-Peak'
                                                                    , 'Billed Usage - Miscellaneous'
                                                                    , 'Billed Usage - Other-Peak'
                                                                    , 'Billed Usage - On-Peak'
                                                                    , 'Billed  Baseload Volume'
                                                                    , 'Billed Cashout Volume', 'Billed Swing Volume'
                                                                    , 'Billed Fuel Volume'
                                                                    , 'Billed Miscellaneous Volume' ))
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.Bucket_Master bm
                                                INNER JOIN @Ec_Calc_Val_Bucket_Master_Ids ec
                                                    ON ec.Bucket_Master_Id = bm.Bucket_Master_Id
                                           WHERE
                                                bm.Bucket_Master_Id = ecv.Bucket_Master_Id
                                                AND ec.Account_Id = ecv.Account_Id
                                                AND ec.Cu_Invoice_Id = ecv.Cu_Invoice_Id
                                                AND bm.Bucket_Name IN ( 'Usage - Mid-Peak', 'Usage - Miscellaneous'
                                                                        , 'Usage - Off-Peak', 'Usage - On-Peak'
                                                                        , 'Usage - Other-Peak', 'Baseload Volume'
                                                                        , 'Cashout Volume', 'Miscellaneous Volume'
                                                                        , 'Swing Volume', 'Fuel Volume' ));

                        /* 'Usage - Total' and  'Billed Usage - Total' is  selected  and  Usage -Total have invoice data. So deleted                               
 Billed Usage - Total bucket */




                        DELETE
                        ecv
                        FROM
                            @Ec_Calc_Val_Bucket_Master_Ids ecv
                            INNER JOIN dbo.Bucket_Master bm
                                ON bm.Bucket_Master_Id = ecv.Bucket_Master_Id
                        WHERE
                            bm.Bucket_Name IN ( 'Billed Usage - Total', 'Billed Total Volume' )
                            AND ecv.Ec_Calc_Val_Id = @Ec_Calc_Val_Id
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                @Ec_Calc_Val_Bucket_Master_Ids ecvbm
                                                INNER JOIN dbo.Bucket_Master bm1
                                                    ON ecvbm.Bucket_Master_Id = bm1.Bucket_Master_Id
                                           WHERE
                                                bm1.Bucket_Name IN ( 'Usage - Total', 'Total Volume' )
                                                AND ecv.Cu_Invoice_Id = ecvbm.Cu_Invoice_Id
                                                AND ecv.Account_Id = ecvbm.Account_Id
                                                AND ecvbm.Ec_Calc_Val_Id = @Ec_Calc_Val_Id)
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                @Ec_Calc_Val_Bucket_Master_Ids ecvbm
                                                INNER JOIN dbo.Bucket_Master bm1
                                                    ON ecvbm.Bucket_Master_Id = bm1.Bucket_Master_Id
                                           WHERE
                                                bm1.Bucket_Name IN ( 'Billed Usage - Total', 'Billed Total Volume' )
                                                AND ecv.Cu_Invoice_Id = ecvbm.Cu_Invoice_Id
                                                AND ecv.Account_Id = ecvbm.Account_Id
                                                AND ecvbm.Ec_Calc_Val_Id = @Ec_Calc_Val_Id)
                            AND (EXISTS (   SELECT
                                                1
                                            FROM
                                                @Invoice_Bucket_Master_Ids ecvbm
                                                INNER JOIN dbo.Bucket_Master bm1
                                                    ON ecvbm.Bucket_Master_Id = bm1.Bucket_Master_Id
                                            WHERE
                                                bm1.Bucket_Name IN ( 'Usage - Total', 'Total Volume'
                                                                     , 'Usage - Mid-Peak', 'Usage - Miscellaneous'
                                                                     , 'Usage - Off-Peak', 'Usage - On-Peak'
                                                                     , 'Usage - Other-Peak', 'Baseload Volume'
                                                                     , 'Cashout Volume', 'Miscellaneous Volume'
                                                                     , 'Swing Volume', 'Fuel Volume' )
                                                AND ecv.Cu_Invoice_Id = ecvbm.Cu_Invoice_Id
                                                AND ecv.Account_Id = ecvbm.Account_Id))
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.EC_Calc_Val ecv
                                                INNER JOIN dbo.Code c
                                                    ON c.Code_Id = ecv.Aggregation_Cd
                                           WHERE
                                                c.Code_Value IN ( 'SUM', 'Multiple' )
                                                AND ecv.EC_Calc_Val_Id = @Ec_Calc_Val_Id);




                        /* 'Usage - Total' and  'Billed Usage - Total' is  selected  and  Usage -Total doesn't have have invoice data but Billed Usage - Total have invoice data                                                    
  . So deleted  'Usage - Total' bucket */


                        DELETE
                        ecv
                        FROM
                            @Ec_Calc_Val_Bucket_Master_Ids ecv
                            INNER JOIN dbo.Bucket_Master bm
                                ON bm.Bucket_Master_Id = ecv.Bucket_Master_Id
                        WHERE
                            bm.Bucket_Name IN ( 'Usage - Total', 'Total Volume' )
                            AND ecv.Ec_Calc_Val_Id = @Ec_Calc_Val_Id
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                @Ec_Calc_Val_Bucket_Master_Ids ecvbm
                                                INNER JOIN dbo.Bucket_Master bm1
                                                    ON ecvbm.Bucket_Master_Id = bm1.Bucket_Master_Id
                                           WHERE
                                                bm1.Bucket_Name IN ( 'Usage - Total', 'Total Volume' )
                                                AND ecv.Cu_Invoice_Id = ecvbm.Cu_Invoice_Id
                                                AND ecv.Account_Id = ecvbm.Account_Id
                                                AND ecvbm.Ec_Calc_Val_Id = @Ec_Calc_Val_Id)
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                @Ec_Calc_Val_Bucket_Master_Ids ecvbm
                                                INNER JOIN dbo.Bucket_Master bm1
                                                    ON ecvbm.Bucket_Master_Id = bm1.Bucket_Master_Id
                                           WHERE
                                                bm1.Bucket_Name IN ( 'Billed Usage - Total', 'Billed Total Volume' )
                                                AND ecv.Cu_Invoice_Id = ecvbm.Cu_Invoice_Id
                                                AND ecv.Account_Id = ecvbm.Account_Id
                                                AND ecvbm.Ec_Calc_Val_Id = @Ec_Calc_Val_Id)
                            AND (EXISTS (   SELECT
                                                1
                                            FROM
                                                @Invoice_Bucket_Master_Ids ecvbm
                                                INNER JOIN dbo.Bucket_Master bm1
                                                    ON ecvbm.Bucket_Master_Id = bm1.Bucket_Master_Id
                                            WHERE
                                                bm1.Bucket_Name IN ( 'Billed Usage - Total', 'Billed Total Volume' )
                                                AND ecv.Cu_Invoice_Id = ecvbm.Cu_Invoice_Id
                                                AND ecv.Account_Id = ecvbm.Account_Id))
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.EC_Calc_Val ecv
                                                INNER JOIN dbo.Code c
                                                    ON c.Code_Id = ecv.Aggregation_Cd
                                           WHERE
                                                c.Code_Value IN ( 'SUM', 'Multiple' )
                                                AND ecv.EC_Calc_Val_Id = @Ec_Calc_Val_Id);


                        /* If the calc val is defined with the bucket of "Usage - Total" and any of the invoice between the selected dates does not                                          
   have "Usage - Total"  for that invoice it should get the sum of all these available buckets 'Usage - Mid-Peak'                                                    
   ,'Usage - Miscellaneous','Usage - Off-Peak','Usage - On-Peak', 'Usage - Other-Peak' buckets if it exists */

                        INSERT INTO @Ec_Calc_Val_Bucket_Master_Ids
                             (
                                 Ec_Calc_Val_Bucket_Map_Id
                                 , Ec_Calc_Val_Id
                                 , Bucket_Master_Id
                                 , Cu_Invoice_Id
                                 , Account_Id
                                 , SERVICE_MONTH
                             )
                        SELECT
                            -1
                            , @Ec_Calc_Val_Id
                            , bm.Bucket_Master_Id
                            , ibmi.Cu_Invoice_Id
                            , ibmi.Account_Id
                            , ibmi.SERVICE_MONTH
                        FROM
                            dbo.Bucket_Master bm
                            CROSS JOIN (   SELECT
                                                ibmi.Cu_Invoice_Id
                                                , ibmi.Account_Id
                                                , SERVICE_MONTH
                                           FROM
                                                @Invoice_Bucket_Master_Ids ibmi
                                           GROUP BY
                                               ibmi.Cu_Invoice_Id
                                               , ibmi.Account_Id
                                               , SERVICE_MONTH) ibmi
                        WHERE
                            bm.Bucket_Name IN ( 'Usage - Mid-Peak', 'Usage - Miscellaneous', 'Usage - Off-Peak'
                                                , 'Usage - On-Peak', 'Usage - Other-Peak', 'Baseload Volume'
                                                , 'Cashout Volume', 'Miscellaneous Volume', 'Swing Volume'
                                                , 'Fuel Volume' )
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                @Ec_Calc_Val_Bucket_Master_Ids ecvbm
                                                INNER JOIN dbo.Bucket_Master bm1
                                                    ON ecvbm.Bucket_Master_Id = bm1.Bucket_Master_Id
                                           WHERE
                                                bm1.Bucket_Name IN ( 'Usage - Total', 'Total Volume' )
                                                AND ibmi.Cu_Invoice_Id = ecvbm.Cu_Invoice_Id
                                                AND ibmi.Account_Id = ecvbm.Account_Id
                                                AND ecvbm.Ec_Calc_Val_Id = @Ec_Calc_Val_Id)
                            AND NOT EXISTS (   SELECT
                                                    1
                                               FROM
                                                    @Ec_Calc_Val_Bucket_Master_Ids ecvbm
                                                    INNER JOIN dbo.Bucket_Master bm1
                                                        ON ecvbm.Bucket_Master_Id = bm1.Bucket_Master_Id
                                               WHERE
                                                    bm1.Bucket_Name IN ( 'Billed Usage - Total', 'Billed Total Volume' )
                                                    AND ibmi.Cu_Invoice_Id = ecvbm.Cu_Invoice_Id
                                                    AND ibmi.Account_Id = ecvbm.Account_Id
                                                    AND ecvbm.Ec_Calc_Val_Id = @Ec_Calc_Val_Id)
                            AND (NOT EXISTS (   SELECT
                                                    1
                                                FROM
                                                    @Invoice_Bucket_Master_Ids ecvbm
                                                    INNER JOIN dbo.Bucket_Master bm1
                                                        ON ecvbm.Bucket_Master_Id = bm1.Bucket_Master_Id
                                                WHERE
                                                    bm1.Bucket_Name IN ( 'Usage - Total', 'Total Volume' )
                                                    AND ibmi.Cu_Invoice_Id = ecvbm.Cu_Invoice_Id
                                                    AND ibmi.Account_Id = ecvbm.Account_Id))
                            AND (EXISTS (   SELECT
                                                1
                                            FROM
                                                @Invoice_Bucket_Master_Ids ecvbm
                                                INNER JOIN dbo.Bucket_Master bm1
                                                    ON ecvbm.Bucket_Master_Id = bm1.Bucket_Master_Id
                                            WHERE
                                                bm1.Bucket_Name IN ( 'Usage - Mid-Peak', 'Usage - Miscellaneous'
                                                                     , 'Usage - Off-Peak', 'Usage - On-Peak'
                                                                     , 'Usage - Other-Peak', 'Baseload Volume'
                                                                     , 'Cashout Volume', 'Miscellaneous Volume'
                                                                     , 'Swing Volume', 'Fuel Volume' )
                                                AND ibmi.Cu_Invoice_Id = ecvbm.Cu_Invoice_Id
                                                AND ibmi.Account_Id = ecvbm.Account_Id))
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.EC_Calc_Val ecv
                                                INNER JOIN dbo.Code c
                                                    ON c.Code_Id = ecv.Aggregation_Cd
                                           WHERE
                                                c.Code_Value IN ( 'SUM', 'Multiple' )
                                                AND ecv.EC_Calc_Val_Id = @Ec_Calc_Val_Id);






                        /* If the calc val is defined with the bucket of "Usage - Total" ,'Billed Usage - Total'  and any of the invoice between the selected dates does not                                                     
   have "Usage - Total"  for that invoice it should get the sum of all these available buckets 'Usage - Mid-Peak'                                                    
   ,'Usage - Miscellaneous','Usage - Off-Peak','Usage - On-Peak', 'Usage - Other-Peak' buckets if it exists */
                        INSERT INTO @Ec_Calc_Val_Bucket_Master_Ids
                             (
                                 Ec_Calc_Val_Bucket_Map_Id
                                 , Ec_Calc_Val_Id
                                 , Bucket_Master_Id
                                 , Cu_Invoice_Id
                                 , Account_Id
                                 , SERVICE_MONTH
                             )
                        SELECT
                            -1
                            , @Ec_Calc_Val_Id
                            , bm.Bucket_Master_Id
                            , ibmi.Cu_Invoice_Id
                            , ibmi.Account_Id
                            , ibmi.SERVICE_MONTH
                        FROM
                            dbo.Bucket_Master bm
                            CROSS JOIN (   SELECT
                                                ibmi.Cu_Invoice_Id
                                                , ibmi.Account_Id
                                                , SERVICE_MONTH
                                           FROM
                                                @Invoice_Bucket_Master_Ids ibmi
                                           GROUP BY
                                               ibmi.Cu_Invoice_Id
                                               , ibmi.Account_Id
                                               , SERVICE_MONTH) ibmi
                        WHERE
                            bm.Bucket_Name IN ( 'Usage - Mid-Peak', 'Usage - Miscellaneous', 'Usage - Off-Peak'
                                                , 'Usage - On-Peak', 'Usage - Other-Peak', 'Baseload Volume'
                                                , 'Cashout Volume', 'Miscellaneous Volume', 'Swing Volume'
                                                , 'Fuel Volume' )
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                @Ec_Calc_Val_Bucket_Master_Ids ecvbm
                                                INNER JOIN dbo.Bucket_Master bm1
                                                    ON ecvbm.Bucket_Master_Id = bm1.Bucket_Master_Id
                                           WHERE
                                                bm1.Bucket_Name IN ( 'Usage - Total', 'Total Volume' )
                                                AND ibmi.Cu_Invoice_Id = ecvbm.Cu_Invoice_Id
                                                AND ibmi.Account_Id = ecvbm.Account_Id
                                                AND ecvbm.Ec_Calc_Val_Id = @Ec_Calc_Val_Id)
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                @Ec_Calc_Val_Bucket_Master_Ids ecvbm
                                                INNER JOIN dbo.Bucket_Master bm1
                                                    ON ecvbm.Bucket_Master_Id = bm1.Bucket_Master_Id
                                           WHERE
                                                bm1.Bucket_Name IN ( 'Billed Usage - Total', 'Billed Total Volume' )
                                                AND ibmi.Cu_Invoice_Id = ecvbm.Cu_Invoice_Id
                                                AND ibmi.Account_Id = ecvbm.Account_Id
                                                AND ecvbm.Ec_Calc_Val_Id = @Ec_Calc_Val_Id)
                            AND (NOT EXISTS (   SELECT
                                                    1
                                                FROM
                                                    @Invoice_Bucket_Master_Ids ecvbm
                                                    INNER JOIN dbo.Bucket_Master bm1
                                                        ON ecvbm.Bucket_Master_Id = bm1.Bucket_Master_Id
                                                WHERE
                                                    bm1.Bucket_Name IN ( 'Usage - Total', 'Total Volume' )
                                                    AND ibmi.Cu_Invoice_Id = ecvbm.Cu_Invoice_Id
                                                    AND ibmi.Account_Id = ecvbm.Account_Id))
                            AND (EXISTS (   SELECT
                                                1
                                            FROM
                                                @Invoice_Bucket_Master_Ids ecvbm
                                                INNER JOIN dbo.Bucket_Master bm1
                                                    ON ecvbm.Bucket_Master_Id = bm1.Bucket_Master_Id
                                            WHERE
                                                bm1.Bucket_Name IN ( 'Usage - Mid-Peak', 'Usage - Miscellaneous'
                                                                     , 'Usage - Off-Peak', 'Usage - On-Peak'
                                                                     , 'Usage - Other-Peak', 'Baseload Volume'
                                                                     , 'Cashout Volume', 'Miscellaneous Volume'
                                                                     , 'Swing Volume', 'Fuel Volume' )
                                                AND ibmi.Cu_Invoice_Id = ecvbm.Cu_Invoice_Id
                                                AND ibmi.Account_Id = ecvbm.Account_Id))
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.EC_Calc_Val ecv
                                                INNER JOIN dbo.Code c
                                                    ON c.Code_Id = ecv.Aggregation_Cd
                                           WHERE
                                                c.Code_Value IN ( 'SUM', 'Multiple' )
                                                AND ecv.EC_Calc_Val_Id = @Ec_Calc_Val_Id);


                        /* If the calc val is defined with the bucket of "Usage - Total" ,'Billed Usage - Total'  and any of the invoice between the selected dates does not                                                     
   have 'Usage - Total', 'Billed Usage - Total', 'Usage - Mid-Peak', 'Usage - Miscellaneous', 'Usage - Off-Peak', 'Usage - On-Peak'  , 'Usage - Other-Peak'                                                    
  then should sum of all  'Billed Usage - Off-Peak', 'Billed Usage - Mid-Peak' , 'Billed Usage - Miscellaneous', 'Billed Usage - Other-Peak' Buckets                                                    
  */


                        INSERT INTO @Ec_Calc_Val_Bucket_Master_Ids
                             (
                                 Ec_Calc_Val_Bucket_Map_Id
                                 , Ec_Calc_Val_Id
                                 , Bucket_Master_Id
                                 , Cu_Invoice_Id
                                 , Account_Id
                                 , SERVICE_MONTH
                             )
                        SELECT
                            -1
                            , @Ec_Calc_Val_Id
                            , bm.Bucket_Master_Id
                            , ibmi.Cu_Invoice_Id
                            , ibmi.Account_Id
                            , ibmi.SERVICE_MONTH
                        FROM
                            dbo.Bucket_Master bm
                            CROSS JOIN (   SELECT
                                                ibmi.Cu_Invoice_Id
                                                , ibmi.Account_Id
                                                , SERVICE_MONTH
                                           FROM
                                                @Invoice_Bucket_Master_Ids ibmi
                                           GROUP BY
                                               ibmi.Cu_Invoice_Id
                                               , ibmi.Account_Id
                                               , SERVICE_MONTH) ibmi
                        WHERE
                            bm.Bucket_Name IN ( 'Billed Usage - Off-Peak', 'Billed Usage - Mid-Peak'
                                                , 'Billed Usage - Miscellaneous', 'Billed Usage - Other-Peak'
                                                , 'Billed Usage - On-Peak', 'Billed  Baseload Volume'
                                                , 'Billed Cashout Volume', 'Billed Swing Volume', 'Billed Fuel Volume'
                                                , 'Billed Miscellaneous Volume' )
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                @Ec_Calc_Val_Bucket_Master_Ids ecvbm
                                                INNER JOIN dbo.Bucket_Master bm1
                                                    ON ecvbm.Bucket_Master_Id = bm1.Bucket_Master_Id
                                           WHERE
                                                bm1.Bucket_Name IN ( 'Usage - Total', 'Total Volume' )
                                                AND ibmi.Cu_Invoice_Id = ecvbm.Cu_Invoice_Id
                                                AND ibmi.Account_Id = ecvbm.Account_Id
                                                AND ecvbm.Ec_Calc_Val_Id = @Ec_Calc_Val_Id)
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                @Ec_Calc_Val_Bucket_Master_Ids ecvbm
                                                INNER JOIN dbo.Bucket_Master bm1
                                                    ON ecvbm.Bucket_Master_Id = bm1.Bucket_Master_Id
                                           WHERE
                                                bm1.Bucket_Name IN ( 'Billed Usage - Total', 'Billed Total Volume' )
                                                AND ibmi.Cu_Invoice_Id = ecvbm.Cu_Invoice_Id
                                                AND ibmi.Account_Id = ecvbm.Account_Id
                                                AND ecvbm.Ec_Calc_Val_Id = @Ec_Calc_Val_Id)
                            AND (NOT EXISTS (   SELECT
                                                    1
                                                FROM
                                                    @Invoice_Bucket_Master_Ids ecvbm
                                                    INNER JOIN dbo.Bucket_Master bm1
                                                        ON ecvbm.Bucket_Master_Id = bm1.Bucket_Master_Id
                                                WHERE
                                                    bm1.Bucket_Name IN ( 'Usage - Total', 'Billed Usage - Total'
                                                                         , 'Total Volume', 'Billed Total Volume'
                                                                         , 'Usage - Mid-Peak', 'Usage - Miscellaneous'
                                                                         , 'Usage - Off-Peak', 'Usage - On-Peak'
                                                                         , 'Usage - Other-Peak', 'Baseload Volume'
                                                                         , 'Cashout Volume', 'Miscellaneous Volume'
                                                                         , 'Swing Volume', 'Fuel Volume' )
                                                    AND ibmi.Cu_Invoice_Id = ecvbm.Cu_Invoice_Id
                                                    AND ibmi.Account_Id = ecvbm.Account_Id))
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.EC_Calc_Val ecv
                                                INNER JOIN dbo.Code c
                                                    ON c.Code_Id = ecv.Aggregation_Cd
                                           WHERE
                                                c.Code_Value IN ( 'SUM', 'Multiple' )
                                                AND ecv.EC_Calc_Val_Id = @Ec_Calc_Val_Id);


                        /* If the calc val is defined with the bucket of 'Billed Usage - Total'  and doesn't have invoice data for bucket of                                                     
 Billed Usage - Total then it should sum of  billed child buckets*/

                        INSERT INTO @Ec_Calc_Val_Bucket_Master_Ids
                             (
                                 Ec_Calc_Val_Bucket_Map_Id
                                 , Ec_Calc_Val_Id
                                 , Bucket_Master_Id
                                 , Cu_Invoice_Id
                                 , Account_Id
                                 , SERVICE_MONTH
                             )
                        SELECT
                            -1
                            , @Ec_Calc_Val_Id
                            , bm.Bucket_Master_Id
                            , ibmi.Cu_Invoice_Id
                            , ibmi.Account_Id
                            , ibmi.SERVICE_MONTH
                        FROM
                            dbo.Bucket_Master bm
                            CROSS JOIN (   SELECT
                                                ibmi.Cu_Invoice_Id
                                                , ibmi.Account_Id
                                                , SERVICE_MONTH
                                           FROM
                                                @Invoice_Bucket_Master_Ids ibmi
                                           GROUP BY
                                               ibmi.Cu_Invoice_Id
                                               , ibmi.Account_Id
                                               , SERVICE_MONTH) ibmi
                        WHERE
                            bm.Bucket_Name IN ( 'Billed Usage - Off-Peak', 'Billed Usage - Mid-Peak'
                                                , 'Billed Usage - Miscellaneous', 'Billed Usage - Other-Peak'
                                                , 'Billed Usage - On-Peak', 'Billed  Baseload Volume'
                                                , 'Billed Cashout Volume', 'Billed Swing Volume', 'Billed Fuel Volume'
                                                , 'Billed Miscellaneous Volume' )
                            AND NOT EXISTS (   SELECT
                                                    1
                                               FROM
                                                    @Ec_Calc_Val_Bucket_Master_Ids ecvbm
                                                    INNER JOIN dbo.Bucket_Master bm1
                                                        ON ecvbm.Bucket_Master_Id = bm1.Bucket_Master_Id
                                               WHERE
                                                    bm1.Bucket_Name IN ( 'Usage - Total', 'Total Volume' )
                                                    AND ibmi.Cu_Invoice_Id = ecvbm.Cu_Invoice_Id
                                                    AND ibmi.Account_Id = ecvbm.Account_Id
                                                    AND ecvbm.Ec_Calc_Val_Id = @Ec_Calc_Val_Id)
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                @Ec_Calc_Val_Bucket_Master_Ids ecvbm
                                                INNER JOIN dbo.Bucket_Master bm1
                                                    ON ecvbm.Bucket_Master_Id = bm1.Bucket_Master_Id
                                           WHERE
                                                bm1.Bucket_Name IN ( 'Billed Usage - Total', 'Billed Total Volume' )
                                                AND ibmi.Cu_Invoice_Id = ecvbm.Cu_Invoice_Id
                                                AND ibmi.Account_Id = ecvbm.Account_Id
                                                AND ecvbm.Ec_Calc_Val_Id = @Ec_Calc_Val_Id)
                            AND (NOT EXISTS (   SELECT
                                                    1
                                                FROM
                                                    @Invoice_Bucket_Master_Ids ecvbm
                                                    INNER JOIN dbo.Bucket_Master bm1
                                                        ON ecvbm.Bucket_Master_Id = bm1.Bucket_Master_Id
                                                WHERE
                                                    bm1.Bucket_Name IN ( 'Billed Usage - Total', 'Billed Total Volume' )
                                                    AND ibmi.Cu_Invoice_Id = ecvbm.Cu_Invoice_Id
                                                    AND ibmi.Account_Id = ecvbm.Account_Id))
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.EC_Calc_Val ecv
                                                INNER JOIN dbo.Code c
                                                    ON c.Code_Id = ecv.Aggregation_Cd
                                           WHERE
                                                c.Code_Value IN ( 'SUM', 'Multiple' )
                                                AND ecv.EC_Calc_Val_Id = @Ec_Calc_Val_Id);

                        /* If the calc val is defined with the bucket of "Total Volume" and any of the invoice between the selected dates does not have                                                     
                    "Total Volume" bucket, for that invoice it should get the sum of all these available buckets 'Baseload Volume','Cashout Volume'                                                    
        ,'Miscellaneous Volume','Swing Volume' buckets */

                        INSERT INTO @Ec_Calc_Val_Bucket_Master_Ids
                             (
                                 Ec_Calc_Val_Bucket_Map_Id
                                 , Ec_Calc_Val_Id
                                 , Bucket_Master_Id
                                 , Cu_Invoice_Id
                                 , Account_Id
                                 , SERVICE_MONTH
                             )
                        SELECT
                            -1
                            , @Ec_Calc_Val_Id
                            , bm.Bucket_Master_Id
                            , ibmi.Cu_Invoice_Id
                            , ibmi.Account_Id
                            , ibmi.SERVICE_MONTH
                        FROM
                            dbo.Bucket_Master bm
                            CROSS JOIN (   SELECT
                                                ibmi.Cu_Invoice_Id
                                                , ibmi.Account_Id
                                                , ibmi.SERVICE_MONTH
                                           FROM
                                                @Invoice_Bucket_Master_Ids ibmi
                                           GROUP BY
                                               ibmi.Cu_Invoice_Id
                                               , ibmi.Account_Id
                                               , ibmi.SERVICE_MONTH) ibmi
                        WHERE
                            bm.Bucket_Name IN ( 'Baseload Volume', 'Cashout Volume', 'Miscellaneous Volume'
                                                , 'Swing Volume', 'Fuel Volume' )
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                @Ec_Calc_Val_Bucket_Master_Ids ecvbm
                                                INNER JOIN dbo.Bucket_Master bm1
                                                    ON ecvbm.Bucket_Master_Id = bm1.Bucket_Master_Id
                                           WHERE
                                                bm1.Bucket_Name = 'Total Volume'
                                                AND ibmi.Cu_Invoice_Id = ecvbm.Cu_Invoice_Id
                                                AND ibmi.Account_Id = ecvbm.Account_Id
                                                AND ecvbm.Ec_Calc_Val_Id = @Ec_Calc_Val_Id)
                            AND (NOT EXISTS (   SELECT
                                                    1
                                                FROM
                                                    @Invoice_Bucket_Master_Ids ecvbm
                                                    INNER JOIN dbo.Bucket_Master bm1
                                                        ON ecvbm.Bucket_Master_Id = bm1.Bucket_Master_Id
                                                WHERE
                                                    bm1.Bucket_Name = 'Total Volume'
                                                    AND ibmi.Account_Id = ecvbm.Account_Id
                                                    AND ibmi.Cu_Invoice_Id = ecvbm.Cu_Invoice_Id))
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.EC_Calc_Val ecv
                                                INNER JOIN dbo.Code c
                                                    ON c.Code_Id = ecv.Aggregation_Cd
                                           WHERE
                                                c.Code_Value IN ( 'SUM', 'Multiple' )
                                                AND ecv.EC_Calc_Val_Id = @Ec_Calc_Val_Id);



                        INSERT INTO #Ec_Calc_Val_Determinant_Charge_Values
                             (
                                 Ec_Calc_Val_Id
                                 , Calc_Value
                                 , Uom_ID
                                 , Bucket_Master_Id
                                 , Cu_Invoice_Determinant_Charge_Id
                                 , SERVICE_MONTH
                             )
                        SELECT
                            ecsm.Ec_Calc_Val_Id
                            , CASE WHEN cuida.Determinant_Value IS NULL THEN
                                       CASE WHEN (   ISNUMERIC(cuid.DETERMINANT_VALUE) = 1
                                                     OR   cuid.DETERMINANT_VALUE IS NULL) THEN
                                                CAST(ISNULL(
                                                         (REPLACE(REPLACE(cuid.DETERMINANT_VALUE, ',', ''), ' ', ''))
                                                         , 0) AS DECIMAL(32, 18))
                                           WHEN (   ISNUMERIC(cuid.DETERMINANT_VALUE) = 0
                                                    AND   cuid.DETERMINANT_VALUE IS NOT NULL) THEN 0
                                       END
                                  ELSE cuida.Determinant_Value
                              END DeterminantValue
                            , cuid.UNIT_OF_MEASURE_TYPE_ID
                            , ecvbm.Bucket_Master_Id
                            , cuid.CU_INVOICE_DETERMINANT_ID
                            , ecvbm.SERVICE_MONTH
                        FROM
                            dbo.CU_INVOICE_SERVICE_MONTH cism
                            INNER JOIN dbo.CU_INVOICE_DETERMINANT cuid
                                ON cism.CU_INVOICE_ID = cuid.CU_INVOICE_ID
                            INNER JOIN dbo.CU_INVOICE_DETERMINANT_ACCOUNT cuida
                                ON cuid.CU_INVOICE_DETERMINANT_ID = cuida.CU_INVOICE_DETERMINANT_ID
                                   AND  cism.Account_ID = cuida.ACCOUNT_ID
                            INNER JOIN dbo.CU_INVOICE ci
                                ON cism.CU_INVOICE_ID = ci.CU_INVOICE_ID
                            INNER JOIN @Ec_Calc_Val_Bucket_Master_Ids ecvbm
                                ON cuid.Bucket_Master_Id = ecvbm.Bucket_Master_Id
                                   AND  cuid.CU_INVOICE_ID = ecvbm.Cu_Invoice_Id
                                   AND  cuida.ACCOUNT_ID = ecvbm.Account_Id
                            INNER JOIN #EC_Calc_Val ecv
                                ON ecv.EC_Calc_Val_Id = ecvbm.Ec_Calc_Val_Id
                            INNER JOIN #Ec_Calc_Val_Id_Service_Months ecsm
                                ON ecsm.Ec_Calc_Val_Id = ecv.EC_Calc_Val_Id
                            INNER JOIN #Ec_Account_Group_Accounts eaga
                                ON eaga.Account_Id = cuida.ACCOUNT_ID
                                   AND  eaga.Ec_Calc_Val_Id = ecv.EC_Calc_Val_Id
                            INNER JOIN dbo.Bucket_Master bm
                                ON bm.Bucket_Master_Id = ecvbm.Bucket_Master_Id
                            LEFT OUTER JOIN dbo.Cu_Invoice_Account_Commodity ciac
                                ON ciac.Account_Id = cuida.ACCOUNT_ID
                                   AND  ciac.Cu_Invoice_Id = cuid.CU_INVOICE_ID
                                   AND  ciac.Commodity_Id = cuid.COMMODITY_TYPE_ID
                            LEFT OUTER JOIN dbo.Cu_Invoice_Account_Commodity_EC_Calc_Value ciacec
                                ON ciacec.Cu_Invoice_Account_Commodity_Id = ciac.Cu_Invoice_Account_Commodity_Id
                        WHERE
                            cism.SERVICE_MONTH BETWEEN @Start_Service_Month
                                               AND     @End_Service_Month
                            AND cuid.COMMODITY_TYPE_ID = @Commodity_Id
                            AND (   ci.CU_INVOICE_ID = @Cu_Invoice_Id
                                    OR  (   ci.IS_REPORTED = 1
                                            AND ci.IS_PROCESSED = 1))
                            AND ci.IS_DNT = 0
                            AND ci.IS_DUPLICATE = 0
                            /*** MAINT-10131 change start *****/
                            AND (   NOT EXISTS (   SELECT
                                                        1
                                                   FROM
                                                        dbo.Ec_Calc_Val_Bucket_Map ecvbm
                                                        INNER JOIN dbo.Ec_Calc_Val_Bucket_Sub_Bucket_Map ecvbsbm
                                                            ON ecvbsbm.Ec_Calc_Val_Bucket_Map_Id = ecvbm.Ec_Calc_Val_Bucket_Map_Id
                                                   WHERE
                                                        ecvbm.Ec_Calc_Val_Id = ecv.EC_Calc_Val_Id)
                                    OR  EXISTS (   SELECT
                                                        1
                                                   FROM
                                                        dbo.Ec_Calc_Val_Bucket_Map ecvbm
                                                        INNER JOIN dbo.Ec_Calc_Val_Bucket_Sub_Bucket_Map ecvbsbm
                                                            ON ecvbsbm.Ec_Calc_Val_Bucket_Map_Id = ecvbm.Ec_Calc_Val_Bucket_Map_Id
                                                   WHERE
                                                        ecvbm.Ec_Calc_Val_Id = ecv.EC_Calc_Val_Id
                                                        AND ISNULL(cuid.EC_Invoice_Sub_Bucket_Master_Id, -1) = ISNULL(
                                                                                                                   ecvbsbm.EC_Invoice_Sub_Bucket_Master_Id
                                                                                                                   , -1))); /*** MAINT-10131 change end *****/


                        IF EXISTS (   SELECT
                                            1
                                      FROM
                                            @Calc_Val_Type
                                      WHERE
                                            Ec_Calc_Val_Id = @Ec_Calc_Val_Id
                                            AND Calc_Type = 'Recalc Charge')
                            BEGIN
                                DELETE  FROM @Recalc_Header;

                                INSERT INTO @Recalc_Header
                                     (
                                         Recalc_Header_Id
                                         , Cu_Invoice_ID
                                         , Ec_Calc_Val_Id
                                         , SERVICE_MONTH
                                     )
                                SELECT
                                    rh.RECALC_HEADER_ID
                                    , rh.CU_INVOICE_ID
                                    , eaga.Ec_Calc_Val_Id
                                    , sm.SERVICE_MONTH
                                FROM
                                    #Ec_Account_Group_Accounts eaga
                                    INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH sm
                                        ON sm.Account_ID = eaga.Account_Id
                                    INNER JOIN dbo.CU_INVOICE ci
                                        ON ci.CU_INVOICE_ID = sm.CU_INVOICE_ID
                                    INNER JOIN dbo.RECALC_HEADER rh
                                        ON rh.CU_INVOICE_ID = sm.CU_INVOICE_ID
                                           AND  rh.ACCOUNT_ID = eaga.Account_Id
                                WHERE
                                    rh.Commodity_Id = @Commodity_Id
                                    AND (   ci.CU_INVOICE_ID = @Cu_Invoice_Id
                                            OR  (   ci.IS_REPORTED = 1
                                                    AND ci.IS_PROCESSED = 1))
                                    AND ci.IS_DNT = 0
                                    AND ci.IS_DUPLICATE = 0
                                    AND sm.SERVICE_MONTH BETWEEN @Start_Service_Month
                                                         AND     @End_Service_Month
                                GROUP BY
                                    rh.RECALC_HEADER_ID
                                    , rh.CU_INVOICE_ID
                                    , eaga.Ec_Calc_Val_Id
                                    , sm.SERVICE_MONTH;



                                -- getting the values of 'Federal Tax' charges from Cu_Invoice_Recalc_Response table                                                 

                                INSERT INTO #Ec_Calc_Val_Determinant_Charge_Values
                                     (
                                         Ec_Calc_Val_Id
                                         , Calc_Value
                                         , Uom_ID
                                         , Bucket_Master_Id
                                         , Cu_Invoice_Determinant_Charge_Id
                                         , SERVICE_MONTH
                                     )
                                SELECT
                                    ecsm.Ec_Calc_Val_Id
                                    , SUM(cirr.Net_Amount * cuc.CONVERSION_FACTOR)
                                    , @Default_Currency_Id
                                    , ecvbm.Bucket_Master_Id
                                    , NULL
                                    , ecvbm.SERVICE_MONTH
                                FROM
                                    @Recalc_Header rh
                                    INNER JOIN dbo.Cu_Invoice_Recalc_Response cirr
                                        ON cirr.Recalc_Header_Id = rh.Recalc_Header_Id
                                    INNER JOIN @Ec_Calc_Val_Bucket_Master_Ids ecvbm
                                        ON cirr.Bucket_Master_Id = ecvbm.Bucket_Master_Id
                                           AND  rh.Cu_Invoice_ID = ecvbm.Cu_Invoice_Id
                                    INNER JOIN #EC_Calc_Val ecv
                                        ON ecv.EC_Calc_Val_Id = ecvbm.Ec_Calc_Val_Id
                                           AND  ecv.EC_Calc_Val_Id = rh.Ec_Calc_Val_Id
                                    INNER JOIN #Ec_Calc_Val_Id_Service_Months ecsm
                                        ON ecsm.Ec_Calc_Val_Id = ecv.EC_Calc_Val_Id
                                    INNER JOIN dbo.Bucket_Master bm
                                        ON bm.Bucket_Master_Id = ecvbm.Bucket_Master_Id
                                    INNER JOIN @Calc_Val_Type cv
                                        ON cv.Ec_Calc_Val_Id = ecsm.Ec_Calc_Val_Id
                                    INNER JOIN dbo.CURRENCY_UNIT_CONVERSION cuc
                                        ON cuc.BASE_UNIT_ID = cirr.Net_Amt_Currency_Unit_Id
                                           AND  cuc.CONVERTED_UNIT_ID = @Default_Currency_Id
                                           AND  cuc.CONVERSION_DATE = @Service_Month -- Max Service month of the invoice being recalculated                                                    
                                           AND  cuc.CURRENCY_GROUP_ID = @Client_Currency_Group_Id
                                WHERE
                                    cv.Calc_Type = 'Recalc Charge'
                                GROUP BY
                                    ecsm.Ec_Calc_Val_Id
                                    , ecvbm.Bucket_Master_Id
                                    , ecvbm.SERVICE_MONTH;

                                ---- getting the values of Green Certificate from Cu_Invoice_Recalc_Response table                                            

                                INSERT INTO #Ec_Calc_Val_Determinant_Charge_Values
                                     (
                                         Ec_Calc_Val_Id
                                         , Calc_Value
                                         , Uom_ID
                                         , Bucket_Master_Id
                                         , Cu_Invoice_Determinant_Charge_Id
                                         , SERVICE_MONTH
                                     )
                                SELECT
                                    ecsm.Ec_Calc_Val_Id
                                    , SUM(cirr.Net_Amount)
                                    , @Default_Currency_Id
                                    , -1
                                    , NULL
                                    , ecvbm.SERVICE_MONTH
                                FROM
                                    @Recalc_Header rh
                                    INNER JOIN dbo.Cu_Invoice_Recalc_Response cirr
                                        ON cirr.Recalc_Header_Id = rh.Recalc_Header_Id
                                    INNER JOIN @Ec_Calc_Val_Bucket_Master_Ids ecvbm
                                        ON rh.Cu_Invoice_ID = ecvbm.Cu_Invoice_Id
                                    INNER JOIN #EC_Calc_Val ecv
                                        ON ecv.EC_Calc_Val_Id = ecvbm.Ec_Calc_Val_Id
                                           AND  ecv.EC_Calc_Val_Id = rh.Ec_Calc_Val_Id
                                    INNER JOIN #Ec_Calc_Val_Id_Service_Months ecsm
                                        ON ecsm.Ec_Calc_Val_Id = ecv.EC_Calc_Val_Id
                                    INNER JOIN @GreenCertificate gc
                                        ON gc.Calc_Val_Name = ecv.Calc_Value_Name
                                           AND  cirr.Charge_Name = gc.Charge_Name
                                    INNER JOIN @Calc_Val_Type cvt
                                        ON cvt.Ec_Calc_Val_Id = ecv.EC_Calc_Val_Id
                                    INNER JOIN dbo.CURRENCY_UNIT_CONVERSION cuc
                                        ON cuc.BASE_UNIT_ID = cirr.Net_Amt_Currency_Unit_Id
                                           AND  cuc.CONVERTED_UNIT_ID = @Default_Currency_Id
                                           AND  cuc.CONVERSION_DATE = @Service_Month -- Max Service month of the invoice being recalculated                                                    
                                           AND  cuc.CURRENCY_GROUP_ID = @Client_Currency_Group_Id
                                WHERE
                                    ecvbm.Bucket_Master_Id = -1
                                    AND cvt.Calc_Type = 'Recalc Charge'
                                GROUP BY
                                    ecsm.Ec_Calc_Val_Id
                                    , ecvbm.SERVICE_MONTH;
                            END;



                        DELETE
                        #Ec_Calc_Val_Determinant_Charge_Values
                        FROM
                            #Ec_Calc_Val_Determinant_Charge_Values ecvdcv
                            INNER JOIN #EC_Calc_Val ecv
                                ON ecvdcv.Ec_Calc_Val_Id = ecv.EC_Calc_Val_Id
                        WHERE
                            MONTH(ecvdcv.SERVICE_MONTH) NOT IN ( SELECT MONTH_NO FROM   #Monthly_Settings )
                            AND (   ecv.Monthly_Setting_Start_Month_Num IS NOT NULL
                                    AND Monthly_Setting_End_Month_Num IS NOT NULL);




                        SELECT
                            @Default_Uom_Id = CASE WHEN ecv.Uom_Cd IS NOT NULL THEN ecv.Uom_Cd
                                                  ELSE MAX(ecdcv.Uom_ID)
                                              END
                        FROM
                            #Ec_Calc_Val_Determinant_Charge_Values ecdcv
                            LEFT JOIN #EC_Calc_Val ecv
                                ON ecdcv.Ec_Calc_Val_Id = ecv.EC_Calc_Val_Id
                        GROUP BY
                            ecv.Uom_Cd;


                        INSERT INTO #Ec_Calc_Val_Determinant_Charge_Values_With_Uom
                             (
                                 Ec_Calc_Val_Id
                                 , Calc_Value
                                 , Uom_Name
                                 , SERVICE_MONTH
                             )
                        SELECT
                            ecvdcv.Ec_Calc_Val_Id
                            , (CASE WHEN cvt.Calc_Type = 'Recalc Charge' THEN ecvdcv.Calc_Value
                                   ELSE ecvdcv.Calc_Value * ccon.CONVERSION_FACTOR
                               END) Calc_Value
                            , CASE WHEN cvt.Calc_Type = 'Recalc Charge' THEN cu.CURRENCY_UNIT_NAME
                                  ELSE e.ENTITY_NAME
                              END ENTITY_NAME
                            , ecvdcv.SERVICE_MONTH
                        FROM
                            #Ec_Calc_Val_Determinant_Charge_Values ecvdcv
                            INNER JOIN @Calc_Val_Type cvt
                                ON ecvdcv.Ec_Calc_Val_Id = cvt.Ec_Calc_Val_Id
                            LEFT OUTER JOIN dbo.CONSUMPTION_UNIT_CONVERSION ccon
                                ON ccon.BASE_UNIT_ID = ecvdcv.Uom_ID
                                   AND  ccon.CONVERTED_UNIT_ID = @Default_Uom_Id
                            LEFT OUTER JOIN dbo.ENTITY e
                                ON e.ENTITY_ID = @Default_Uom_Id
                            LEFT OUTER JOIN dbo.CURRENCY_UNIT cu
                                ON cu.CURRENCY_UNIT_ID = @Default_Currency_Id
                        GROUP BY
                            ecvdcv.Ec_Calc_Val_Id
                            , CASE WHEN cvt.Calc_Type = 'Recalc Charge' THEN cu.CURRENCY_UNIT_NAME
                                  ELSE e.ENTITY_NAME
                              END
                            , (CASE WHEN cvt.Calc_Type = 'Recalc Charge' THEN ecvdcv.Calc_Value
                                   ELSE ecvdcv.Calc_Value * ccon.CONVERSION_FACTOR
                               END)
                            , ecvdcv.Cu_Invoice_Determinant_Charge_Id
                            , ecvdcv.SERVICE_MONTH;

                        SELECT
                            @Sql_Str = N'MERGE INTO #Ec_Calc_Val_Final_Result tgt                                                    
            USING(                                                    
             SELECT                                                    
              ecv.Ec_Calc_Val_Id                                                    
              ,ecv.Calc_Value_Name                                                    
              ,Calc_Value=' + c.Code_Value
/***** MAINT-9551 added NULLIF to update NULL if calc_value is zero to calculate MIN and AVG value currectly **********/
+                           N'(NULLIF(ecdc.Calc_Value,0))                                                     
              ,ecdc.Uom_Name Uom_Name                                                    
              ,0 Is_Value_Locked                       
      FROM                                   
              #EC_Calc_Val ecv                                                     
              INNER JOIN #Ec_Calc_Val_Determinant_Charge_Values_With_Uom ecdc                                                     
               ON ecdc.EC_Calc_Val_Id = ecv.EC_Calc_Val_Id                                                   
       GROUP BY                                                     
       ecv.Ec_Calc_Val_Id,ecv.Calc_Value_Name,ecdc.Uom_Name                                                    
            )src                                                    
             ON tgt.EC_Calc_Val_Id = src.EC_Calc_Val_Id                                        
   WHEN NOT MATCHED THEN INSERT (Ec_Calc_Val_Id                                                    
               ,Calc_Value_Name                                                    
               ,Calc_Value                                
               ,Uom_Name                                                    
               ,Is_Value_Locked)                                               
               VALUES(src.Ec_Calc_Val_Id                                                    
               ,src.Calc_Value_Name                                                    
               ,src.Calc_Value                                                    
               ,src.Uom_Name                                                    
               ,src.Is_Value_Locked)                                                    
           WHEN MATCHED THEN UPDATE SET Calc_Value = src.Calc_Value;                                                    
           '
                        FROM
                            #EC_Calc_Val ecv
                            INNER JOIN dbo.Code c
                                ON c.Code_Id = ecv.Aggregation_Cd
                        WHERE
                            ecv.sno = @Row_Num
                            AND c.Code_Value <> 'Multiple';

                        EXECUTE (@Sql_Str);


                        INSERT INTO #Ec_Calc_Val_Determinant_Charge_Values_With_Uom_Multiple
                             (
                                 Ec_Calc_Val_Id
                                 , Calc_Value
                                 , Uom_Name
                                 , Is_Value_Locked
                                 , SERVICE_MONTH
                                 , Multiple_Option_Value_Selection
                                 , Multiple_Option_No_Of_Month
                                 , Multiple_Option_Total_Aggregation
                             )
                        SELECT
                            ecvdcvwu.Ec_Calc_Val_Id
                            , SUM(ecvdcvwu.Calc_Value)
                            , ecvdcvwu.Uom_Name
                            , ecvdcvwu.Is_Value_Locked
                            , ecvdcvwu.SERVICE_MONTH
                            , c1.Code_Value
                            , ecv.Multiple_Option_No_Of_Month
                            , c2.Code_Value
                        FROM
                            #Ec_Calc_Val_Determinant_Charge_Values_With_Uom ecvdcvwu
                            INNER JOIN #EC_Calc_Val ecv
                                ON ecvdcvwu.Ec_Calc_Val_Id = ecv.EC_Calc_Val_Id
                            INNER JOIN dbo.Code c
                                ON c.Code_Id = ecv.Aggregation_Cd
                                   AND  c.Code_Value = 'Multiple'
                            LEFT JOIN dbo.Code c1
                                ON c1.Code_Id = ecv.Multiple_Option_Value_Selection_Cd
                            LEFT JOIN dbo.Code c2
                                ON c2.Code_Id = ecv.Multiple_Option_Total_Aggregation_Cd
                        GROUP BY
                            ecvdcvwu.Ec_Calc_Val_Id
                            , ecvdcvwu.Uom_Name
                            , ecvdcvwu.Is_Value_Locked
                            , ecvdcvwu.SERVICE_MONTH
                            , c1.Code_Value
                            , ecv.Multiple_Option_No_Of_Month
                            , c2.Code_Value;


                        SELECT
                            @Multiple_Option_Value_Selection = (   SELECT   DISTINCT
                                                                            Multiple_Option_Value_Selection
                                                                   FROM
                                                                        #Ec_Calc_Val_Determinant_Charge_Values_With_Uom_Multiple);
                        SELECT
                            @Multiple_Option_No_Of_Month = (   SELECT   DISTINCT
                                                                        Multiple_Option_No_Of_Month
                                                               FROM
                                                                    #Ec_Calc_Val_Determinant_Charge_Values_With_Uom_Multiple);
                        SELECT
                            @Multiple_Option_Total_Aggregation = (   SELECT DISTINCT
                                                                            Multiple_Option_Total_Aggregation
                                                                     FROM
                                                                            #Ec_Calc_Val_Determinant_Charge_Values_With_Uom_Multiple);



                        SET @ORDER = (   SELECT
                                                CASE WHEN @Multiple_Option_Value_Selection = 'MIN' THEN 'ASC'
                                                    WHEN @Multiple_Option_Value_Selection = 'MAX' THEN 'DESC'
                                                END);
                        SELECT
                            @Sql_Str = N'                                                
;WITH CTE AS (SELECT EC_CALC_VAL_ID,CALC_VALUE,UOM_NAME,IS_VALUE_LOCKED,SERVICE_MONTH, ROW_NUMBER() OVER(ORDER BY CALC_VALUE '
                                       + @ORDER
                                       + N') AS ROWNUMBER                                                 
FROM #EC_CALC_VAL_DETERMINANT_CHARGE_VALUES_WITH_UOM_MULTIPLE                                                
)                                                
MERGE INTO #Ec_Calc_Val_Final_Result tgt                                                    
            USING(                                                    
             SELECT                                                    
              ecv.Ec_Calc_Val_Id                                                    
              ,ecv.Calc_Value_Name                                                    
              ,CASE WHEN ''' + @Multiple_Option_Total_Aggregation + N''' =''AVG'' THEN' + N' '
                                       + @Multiple_Option_Total_Aggregation + N'(CALC_VALUE) ELSE' + N' '
                                       + @Multiple_Option_Total_Aggregation
                                       + N'(CALC_VALUE) END AS CALC_VALUE                                                    
              ,ecdc.Uom_Name Uom_Name                              
              ,0 Is_Value_Locked                                                
             FROM                                                     
              #EC_Calc_Val ecv                                                     
              INNER JOIN CTE ecdc                                                     
               ON ecdc.EC_Calc_Val_Id = ecv.EC_Calc_Val_Id                                                   
WHERE  ROWNUMBER<=' +       CAST(@Multiple_Option_No_Of_Month AS VARCHAR(10))
                                       + N'                                                 
             GROUP BY                                                     
              ecv.Ec_Calc_Val_Id,ecv.Calc_Value_Name,ecdc.Uom_Name        
            )src                                                    
             ON tgt.EC_Calc_Val_Id = src.EC_Calc_Val_Id                                                    
           WHEN NOT MATCHED THEN INSERT (Ec_Calc_Val_Id                                                    
               ,Calc_Value_Name                                                    
               ,Calc_Value                                                    
      ,Uom_Name                                                    
               ,Is_Value_Locked)                                                     
               VALUES(src.Ec_Calc_Val_Id                                                    
               ,src.Calc_Value_Name                                          
               ,src.Calc_Value                                                    
               ,src.Uom_Name                
   ,src.Is_Value_Locked)                                                    
           WHEN MATCHED THEN UPDATE SET Calc_Value = src.Calc_Value;'
                        FROM
                            #EC_Calc_Val ecv
                            INNER JOIN dbo.Code c
                                ON c.Code_Id = ecv.Aggregation_Cd
                        WHERE
                            ecv.sno = @Row_Num;

                        EXECUTE (@Sql_Str);

                        DELETE  FROM #Ec_Calc_Val_Id_Service_Months;

                        DELETE  FROM #Ec_Calc_Val_Determinant_Charge_Values;

                        DELETE  FROM #Ec_Calc_Val_Determinant_Charge_Values_With_Uom;

                        DELETE  FROM #Ec_Calc_Val_Determinant_Charge_Values_With_Uom_Multiple;

                        DELETE  FROM @Invoice_Bucket_Master_Ids;

                        DELETE  FROM @Ec_Calc_Val_Bucket_Master_Ids;



                        SET @Row_Num = @Row_Num + 1;
                    END;

                IF EXISTS (   SELECT
                                    1
                              FROM
                                    #EC_Calc_Val ecv
                                    LEFT JOIN dbo.Code c
                                        ON ecv.Calc_Val_Type_Cd = c.Code_Id
                              WHERE
                                    c.Code_Value = 'Adjusted Billing Days'
                                    AND ecv.EC_Calc_Val_Id = @Ec_Calc_Val_Id)
                    BEGIN


                        INSERT INTO @Invoice_Ids_AdjustingBilling_Days
                             (
                                 Ec_Calc_Val_Id
                                 , Cu_Invoice_Id
                                 , Account_Id
                                 , SERVICE_MONTH
                                 , Begin_Dt
                                 , End_Dt
                             )
                        SELECT
                            @Ec_Calc_Val_Id
                            , cuid.CU_INVOICE_ID
                            , cism.Account_ID
                            , cism.SERVICE_MONTH
                            , cism.Begin_Dt
                            , cism.End_Dt
                        FROM
                            dbo.CU_INVOICE_SERVICE_MONTH cism
                            INNER JOIN dbo.CU_INVOICE inv
                                ON inv.CU_INVOICE_ID = cism.CU_INVOICE_ID
                            INNER JOIN dbo.CU_INVOICE_DETERMINANT cuid
                                ON cism.CU_INVOICE_ID = cuid.CU_INVOICE_ID
                            INNER JOIN dbo.CU_INVOICE_DETERMINANT_ACCOUNT cuida
                                ON cuid.CU_INVOICE_DETERMINANT_ID = cuida.CU_INVOICE_DETERMINANT_ID
                                   AND  cism.Account_ID = cuida.ACCOUNT_ID
                            INNER JOIN #Ec_Account_Group_Accounts eaga
                                ON cuida.ACCOUNT_ID = eaga.Account_Id
                        WHERE
                            cism.SERVICE_MONTH BETWEEN @Start_Service_Month
                                               AND     @End_Service_Month
                            AND cuid.COMMODITY_TYPE_ID = @Commodity_Id
                            AND (   inv.CU_INVOICE_ID = @Cu_Invoice_Id
                                    OR  (   inv.IS_REPORTED = 1
                                            AND inv.IS_PROCESSED = 1))
                        GROUP BY
                            cuid.CU_INVOICE_ID
                            , cism.Account_ID
                            , SERVICE_MONTH
                            , Begin_Dt
                            , End_Dt;

                        DELETE
                        @Invoice_Ids_AdjustingBilling_Days
                        FROM
                            @Invoice_Ids_AdjustingBilling_Days ecvdcv
                            INNER JOIN #EC_Calc_Val ecv
                                ON ecvdcv.Ec_Calc_Val_Id = ecv.EC_Calc_Val_Id
                        WHERE
                            MONTH(ecvdcv.SERVICE_MONTH) NOT IN ( SELECT MONTH_NO FROM   #Monthly_Settings )
                            AND (   ecv.Monthly_Setting_Start_Month_Num IS NOT NULL
                                    AND Monthly_Setting_End_Month_Num IS NOT NULL);


                        INSERT INTO @Ec_Calc_vals_Meter_Attributes_Adjusted_Billing_Days
                        SELECT
                            cha.Account_Id
                            , cha.Meter_Id
                            , emat.EC_Meter_Attribute_Id
                            , ema.EC_Meter_Attribute_Name
                            , EC_Meter_Attribute_Value
                        FROM
                            Core.Client_Hier_Account cha
                            JOIN dbo.EC_Meter_Attribute_Tracking emat
                                ON cha.Meter_Id = emat.Meter_Id
                                   AND  emat.Meter_Attribute_Type_Cd = @Attribute_Type_Cd
                            LEFT JOIN dbo.EC_Meter_Attribute ema
                                ON emat.EC_Meter_Attribute_Id = ema.EC_Meter_Attribute_Id
                        WHERE
                            EC_Meter_Attribute_Name LIKE '%Service Period% %Date Adjustment%'
                            AND Account_Id = @Account_Id;

                        SELECT
                            @Service_Period_Start_Date_Adjustment = CASE WHEN EC_Meter_Attribute_Name = 'Service Period Start Date Adjustment'
                                                                              AND   EC_Meter_Attribute_Value LIKE '%Back%' THEN
                                                                             -1
                                                                        WHEN EC_Meter_Attribute_Name = 'Service Period Start Date Adjustment'
                                                                             AND EC_Meter_Attribute_Value LIKE '%forward%' THEN
                                                                            1
                                                                        ELSE 0
                                                                    END
                        FROM
                            @Ec_Calc_vals_Meter_Attributes_Adjusted_Billing_Days;

                        SELECT
                            @Service_Period_End_Date_Adjustment = CASE WHEN EC_Meter_Attribute_Name = 'Service Period End Date Adjustment'
                                                                            AND EC_Meter_Attribute_Value LIKE '%Back%' THEN
                                                                           -1
                                                                      WHEN EC_Meter_Attribute_Name = 'Service Period End Date Adjustment'
                                                                           AND  EC_Meter_Attribute_Value LIKE '%forward%' THEN
                                                                          1
                                                                      ELSE 0
                                                                  END
                        FROM
                            @Ec_Calc_vals_Meter_Attributes_Adjusted_Billing_Days;

                        INSERT INTO #Ec_Calc_Val_Adjusting_BillingDays
                             (
                                 Ec_Calc_Val_Id
                                 , Account_Id
                                 , SERVICE_MONTH
                                 , Begin_Dt
                                 , End_Dt
                                 , Billing_Days
                             )
                        SELECT
                            Ec_Calc_Val_Id
                            , Account_Id
                            , SERVICE_MONTH
                            , MIN(Begin_Dt) AS Begin_Dt
                            , MAX(End_Dt) AS End_Dt
                            , DATEDIFF(
                                  DAY, DATEADD(DAY, ISNULL(@Service_Period_Start_Date_Adjustment, 0), MIN(Begin_Dt))
                                  , DATEADD(DAY, ISNULL(@Service_Period_End_Date_Adjustment, 0), MAX(End_Dt))) + 1 AS Billing_Days
                        FROM
                            @Invoice_Ids_AdjustingBilling_Days
                        GROUP BY
                            Account_Id
                            , SERVICE_MONTH
                            , Ec_Calc_Val_Id;

                        --SELECT * FROM #Ec_Calc_Val_Adjusting_BillingDays                                                



                        SELECT
                            @Sql_Str = N'MERGE INTO #Ec_Calc_Val_Final_Result tgt                                                    
            USING(                                      
             SELECT                                                    
              ecv.Ec_Calc_Val_Id                                     
              ,ecv.Calc_Value_Name                                                    
              ,Calc_Value=' + c.Code_Value
                                       + N'(ecdc.Billing_Days)                                                    
              ,Uom_Name=''Days''                                                    
              ,0 Is_Value_Locked                                                    
             FROM                                                     
              #EC_Calc_Val ecv                                                     
              INNER JOIN #Ec_Calc_Val_Adjusting_BillingDays ecdc                                                     
               ON ecdc.EC_Calc_Val_Id = ecv.EC_Calc_Val_Id                                                   
             GROUP BY                                                     
              ecv.Ec_Calc_Val_Id,ecv.Calc_Value_Name                                                  
            )src                                                    
             ON tgt.EC_Calc_Val_Id = src.EC_Calc_Val_Id                                                    
           WHEN NOT MATCHED THEN INSERT (Ec_Calc_Val_Id                                                    
               ,Calc_Value_Name                                                    
               ,Calc_Value                      
               ,Uom_Name                                                    
               ,Is_Value_Locked)                                                     
               VALUES(src.Ec_Calc_Val_Id                                                    
               ,src.Calc_Value_Name                                                    
               ,src.Calc_Value                             
               ,src.Uom_Name                                                    
               ,src.Is_Value_Locked)                                                    
           WHEN MATCHED THEN UPDATE SET Calc_Value = src.Calc_Value; '
                        FROM
                            #EC_Calc_Val ecv
                            INNER JOIN dbo.Code c
                                ON c.Code_Id = ecv.Aggregation_Cd
                        WHERE
                            c.Code_Value <> 'Multiple'
                            AND ecv.EC_Calc_Val_Id = @Ec_Calc_Val_Id;

                        EXECUTE (@Sql_Str);

                        DELETE  FROM #Ec_Calc_Val_Adjusting_BillingDays;

                    --DELETE FROM #Ec_Account_Group_Accounts;                                                

                    END;

                DELETE  FROM #Monthly_Settings;

                DELETE  FROM #Ec_Account_Group_Accounts;

            END;


        SELECT
            ecv.EC_Calc_Val_Id
            , ecv.Calc_Value_Name
            , ISNULL(ecvfr.Calc_Value, 0) Calc_Value
            , ecvfr.Locked_Calc_Value
            , CASE WHEN cvt.Calc_Type = 'Recalc Charge' THEN COALESCE(ecvfr.Uom_Name, @Default_Currency_Unit_Name)
                  ELSE COALESCE(ecvfr.Uom_Name, e.ENTITY_NAME, ce.ENTITY_NAME)
              END uom_Name
            , cvt.Calc_Type
            , ISNULL(ecvfr.Is_Value_Locked, 0) Is_Value_Locked
            , ecv.Comment_Text
            , ciacecv.Expected_Slots
            , ciacecv.Actual_Slots
            , ciacecv.Missing_Slots
            , ciacecv.Edited_Slots
        FROM
            @Ec_Calc_vals_Comments ecv
            INNER JOIN @Calc_Val_Type cvt
                ON ecv.EC_Calc_Val_Id = cvt.Ec_Calc_Val_Id
            /*-- Maint 10063 Added EC_Calc_Val join to get updated calcval UOM_Name  START**/
            LEFT OUTER JOIN dbo.EC_Calc_Val ECV1
                ON ECV1.EC_Calc_Val_Id = ecv.EC_Calc_Val_Id
            LEFT OUTER JOIN dbo.ENTITY e
                ON e.ENTITY_ID = ECV1.Uom_Cd
            /*-- Maint 10063 Added EC_Calc_Val join to get updated calcval UOM_Name  END**/
            LEFT JOIN dbo.Ec_Calc_Val_Bucket_Map ecvbm
                ON ecvbm.Ec_Calc_Val_Id = ecv.EC_Calc_Val_Id
            LEFT JOIN dbo.Bucket_Master bm
                ON ecvbm.Bucket_Master_Id = bm.Bucket_Master_Id
            /*-- Maint 9721 added commodity join to get default UOM_Name instade of getting NULL  START**/
            LEFT OUTER JOIN dbo.Commodity c
                ON c.Commodity_Id = bm.Commodity_Id
            LEFT OUTER JOIN dbo.ENTITY ce
                ON ce.ENTITY_ID = c.Default_UOM_Entity_Type_Id
            LEFT OUTER JOIN #Ec_Calc_Val_Final_Result ecvfr
                ON ecv.EC_Calc_Val_Id = ecvfr.Ec_Calc_Val_Id
            LEFT OUTER JOIN dbo.Cu_Invoice_Account_Commodity_EC_Calc_Value ciacecv
                ON ciacecv.Ec_Calc_Val_Id = ecv.EC_Calc_Val_Id
                   AND  ciacecv.Cu_Invoice_Account_Commodity_Id = @Cu_Invoice_Account_Commodity_Id
        GROUP BY
            ecv.EC_Calc_Val_Id
            , ecv.Calc_Value_Name
            , ecvfr.Calc_Value
            , ecvfr.Locked_Calc_Value
            , ecvfr.Uom_Name
            , cvt.Calc_Type
            , ecvfr.Is_Value_Locked
            , ecv.Comment_Text
            , ciacecv.Expected_Slots
            , ciacecv.Actual_Slots
            , ciacecv.Missing_Slots
            , ciacecv.Edited_Slots
            , e.ENTITY_NAME
            , ce.ENTITY_NAME;



        DROP TABLE #EC_Calc_Val;
        DROP TABLE #Ec_Calc_Val_Id_Service_Months;
        DROP TABLE #Ec_Calc_Val_Determinant_Charge_Values;
        DROP TABLE #Ec_Calc_Val_Final_Result;
        DROP TABLE #Ec_Calc_Val_Determinant_Charge_Values_With_Uom;
        DROP TABLE #Ec_Account_Group_Accounts;
        DROP TABLE #Eligible_Calc_Val;
        DROP TABLE #Ec_Calc_Val_Determinant_Charge_Values_With_Uom_Multiple;
        DROP TABLE #Ec_Calc_Val_Adjusting_BillingDays;
        DROP TABLE #Monthly_Settings;

    END;
GO




GRANT EXECUTE ON  [dbo].[CalcVal_Sel_By_Cu_Invoice_Id_Account_Id_Commodity_Id] TO [CBMSApplication]
GO
