
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*=============================================================
Name:
    CBMS.dbo.BUDGET_GET_EL_HISTORICAL_DATA_P
    
Description:
    
    
Input Parameters:
	Name					DataType	Default		Description
---------------------------------------------------------------
	@Budget_Account_ID		INT
	@From_Month				DATETIME
	@To_Month				DATETIME


OUTPUT PARAMETERS:
	Name			DataType		Default		Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC dbo.BUDGET_GET_EL_HISTORICAL_DATA_P 377822, '2011-01-01', '2011-12-01'
	EXEC dbo.BUDGET_GET_EL_HISTORICAL_DATA_P 378226, '2010-01-01', '2010-12-01'
	EXEC dbo.BUDGET_GET_EL_HISTORICAL_DATA_P 447447, '2009-01-01', '2009-12-01'
	EXEC dbo.BUDGET_GET_EL_HISTORICAL_DATA_P 354512, '2009-01-01', '2009-12-01'
	

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
    RK			Rajesh Kasoju
    RR			Raghu Reddy

MODIFICATIONS:
	Initials	Date		Modification
----------------------------------------------------------------
	RK			07/26/2011  Removed @User_ID & @Session_ID parameters and 
							Modified the SP logic and Remove Cost_Usage table & use Cost_Usage_Account_Dtl, Bucket_Master, Commodity tables
	RR			2012-06-08	Added usage examples
							
							
=============================================================*/

CREATE PROCEDURE [dbo].[BUDGET_GET_EL_HISTORICAL_DATA_P]
      ( 
       @Budget_Account_ID INT
      ,@From_Month DATETIME
      ,@To_Month DATETIME )
AS 
BEGIN
      SET NOCOUNT ON
	
      DECLARE
            @Currency_Group_ID INT
           ,@Cur_Converted_Unit_ID INT
           ,@Cons_Converted_Unit_ID INT
           ,@Commodity_Id INT
           ,@Account_Id INT
           
	        	        
      SELECT
            @Cons_Converted_Unit_ID = uom.Entity_ID
           ,@Commodity_Id = com.Commodity_Id
      FROM
            dbo.Commodity COM
            INNER JOIN dbo.ENTITY UOM
                  ON UOM.Entity_Type = COM.UOM_Entity_Type
      WHERE
            COM.Commodity_Name = 'Electric Power'
            AND UOM.Entity_Name = 'kWh'

      SELECT TOP 1
            @Account_Id = ba.Account_ID
      FROM
            dbo.Budget_Account ba
      WHERE
            ba.Budget_Account_ID = @Budget_Account_ID 
                                                
      SELECT
            @Cur_Converted_Unit_ID = cu.Currency_Unit_Id
      FROM
            dbo.Currency_Unit cu
      WHERE
            cu.Currency_Unit_Name = 'USD'
 
      SELECT
            @Currency_Group_ID = cg.Currency_Group_ID
      FROM
            dbo.Currency_Group cg
      WHERE
            cg.Currency_Group_Name = 'Standard' ;
   
          
      WITH  CTE_Max_Currency_Conv_Date
              AS ( SELECT
                        max(cuc.Conversion_Date) AS Max_Conversion_Date
                       ,cuc.Base_Unit_Id
                       ,cuc.Converted_Unit_Id
                       ,cuc.Currency_Group_Id
                   FROM
                        dbo.Currency_Unit_Conversion cuc
                   WHERE
                        cuc.Converted_Unit_ID = @Cur_Converted_Unit_ID
                        AND cuc.Currency_Group_ID = @Currency_Group_ID
                   GROUP BY
                        cuc.Base_Unit_Id
                       ,cuc.Converted_Unit_Id
                       ,cuc.Currency_Group_Id),
            CTE_Cost_Usage_Account_Info
              AS ( SELECT
                        CUAD.Service_Month AS ServiceMonth
                       ,isnull(( case WHEN BM.Bucket_Name = 'Total Cost' THEN CUAD.Bucket_Value * CurConv.Conversion_Factor
                                 END ), 0) AS Total_Cost
                       ,isnull(( case WHEN BM.Bucket_Name = 'Taxes' THEN CUAD.Bucket_Value * CurConv.Conversion_Factor
                                 END ), 0) AS Tax
                       ,isnull(( case WHEN BM.Bucket_Name = 'Total Usage' THEN CUAD.Bucket_Value * CNUC.Conversion_Factor
                                 END ), 0) AS Usage_Value
                   FROM
                        dbo.Cost_Usage_Account_Dtl CUAD
                        INNER JOIN dbo.Bucket_Master BM
                              ON BM.Bucket_Master_Id = CUAD.Bucket_Master_Id
                        LEFT OUTER JOIN dbo.Consumption_Unit_Conversion CNUC
                              ON CNUC.Base_Unit_ID = CUAD.UOM_Type_Id
                                 AND CNUC.Converted_Unit_ID = @Cons_Converted_Unit_ID
		--Currency Unit Converstion		
                        LEFT OUTER JOIN ( dbo.Currency_Unit_Conversion CurConv
                                          INNER JOIN CTE_Max_Currency_Conv_Date max_date
                                                ON CurConv.Base_Unit_Id = max_date.Base_Unit_Id
                                                   AND CurConv.Converted_Unit_ID = max_date.Converted_Unit_ID
                                                   AND CurConv.Currency_Group_ID = max_date.Currency_Group_ID
                                                   AND CurConv.Conversion_Date = max_date.Max_Conversion_Date )
                                          ON CurConv.Base_Unit_ID = CUAD.Currency_Unit_ID
                                             AND CurConv.Converted_Unit_ID = @Cur_Converted_Unit_ID
                                             AND CurConv.Currency_Group_ID = @Currency_Group_ID
                   WHERE
                        CUAD.Account_ID = @Account_Id
                        AND BM.Bucket_Name IN ( 'Total Cost', 'Total Usage', 'Taxes' )
                        AND CUAD.Service_Month BETWEEN @From_Month
                                               AND     @To_Month
                        AND bm.Commodity_Id = @Commodity_Id)
            SELECT
                  cua.ServiceMonth
                 ,sum(CUA.Total_Cost) AS Total_Cost
                 ,sum(CUA.Tax) AS Tax
                 ,sum(CUA.Usage_Value) AS Usage_Value
            FROM
                  CTE_Cost_Usage_Account_Info CUA
            GROUP BY
                  cua.ServiceMonth
		
END 
;
GO

GRANT EXECUTE ON  [dbo].[BUDGET_GET_EL_HISTORICAL_DATA_P] TO [CBMSApplication]
GO
