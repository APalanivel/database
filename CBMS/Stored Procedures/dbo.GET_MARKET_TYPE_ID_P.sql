SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_MARKET_TYPE_ID_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@rmMarketName  	varchar(200)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--exec GET_MARKET_TYPE_ID_P -1,-1,'West Texas Intermediate Crude Oil'

CREATE       PROCEDURE dbo.GET_MARKET_TYPE_ID_P
@userId varchar(10),
@sessionId varchar(20),
@rmMarketName varchar(200)

AS
begin
	set nocount on
	select 	rm_market_id
	
	from 	RM_MARKET
	where rm_market_name = @rmMarketName
	
end
GO
GRANT EXECUTE ON  [dbo].[GET_MARKET_TYPE_ID_P] TO [CBMSApplication]
GO
