
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
   dbo.GET_USER_ID_OF_CEM_P  
  
DESCRIPTION:    
 To get the User_Info_Id and Queue id for the given Username.  
  
INPUT PARAMETERS:    
Name					DataType			Default				Description    
--------------------------------------------------------------------------------  
@USERNAME				VARCHAR(30)  
         
OUTPUT PARAMETERS:    
Name					DataType			Default				Description    
--------------------------------------------------------------------------------  
  
USAGE EXAMPLES:
--------------------------------------------------------------------------------
 
    EXEC dbo.GET_USER_ID_OF_CEM_P
      @USERNAME = 'psingh'
     
     
   
AUTHOR INITIALS:    
Initials	Name    
--------------------------------------------------------------------------------
NR			Narayana Reddy

MODIFICATIONS
Initials		Date			Modification
--------------------------------------------------------------------------------
NR				2016-09-12		MAINT-4114(4326) Added header and Queue id in output list.
******/

CREATE PROCEDURE [dbo].[GET_USER_ID_OF_CEM_P] ( @USERNAME VARCHAR(30) )
AS 
BEGIN

      SET NOCOUNT ON

      SELECT
            USER_INFO_ID
           ,QUEUE_ID
      FROM
            dbo.USER_INFO
      WHERE
            USERNAME = @USERNAME

END

;
GO

GRANT EXECUTE ON  [dbo].[GET_USER_ID_OF_CEM_P] TO [CBMSApplication]
GO
