SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
      
/******                        
 NAME: dbo.Vendor_Sel_By_Invoice_Collection_Contact            
                        
 DESCRIPTION:                        
			To get the details of Vendor      
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                              
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            


 
  EXEC [dbo].[Vendor_Sel_By_Invoice_Collection_Contact] 

                    
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2016-12-19       Created for Invoice Tracking               
                       
******/
CREATE PROCEDURE [dbo].[Vendor_Sel_By_Invoice_Collection_Contact]
AS 
BEGIN                
      SET NOCOUNT ON; 
      SELECT
            v.VENDOR_ID
           ,V.VENDOR_NAME
      FROM
            dbo.VENDOR v
            INNER JOIN dbo.ENTITY e
                  ON e.ENTITY_ID = v.VENDOR_TYPE_ID
      WHERE
            ENTITY_DESCRIPTION = 'vendor'
            AND ENTITY_NAME = 'supplier'
            AND v.IS_HISTORY = 0
            AND EXISTS ( SELECT
                              1
                         FROM
                              dbo.Vendor_Contact vc
                         WHERE
                              vc.VENDOR_ID = v.VENDOR_ID )

END
;
GO
GRANT EXECUTE ON  [dbo].[Vendor_Sel_By_Invoice_Collection_Contact] TO [CBMSApplication]
GO
