SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Utility_Dtl_Service_Level_Upd

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	 @Utility_Dtl_Service_Level_Id	INT
     @Industrial_Flag_Cd			INT
     @Commercial_Flag_Cd			INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN
	EXEC Utility_Dtl_Service_Level_Upd 1,100307,100306
	ROLLBACK TRAN
	
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	SKA			01/12/2011	Created
******/

CREATE PROC dbo.Utility_Dtl_Service_Level_Upd
      (
       @Utility_Dtl_Service_Level_Id INT
      ,@Industrial_Flag_Cd INT
      ,@Commercial_Flag_Cd INT )
AS 
BEGIN
 
      SET nocount ON ;

      UPDATE
            Utility_Dtl_Service_Level
      SET   
            Industrial_Flag_Cd = @Industrial_Flag_Cd
           ,Commercial_Flag_Cd = @Commercial_Flag_Cd
      WHERE
            Utility_Dtl_Service_Level_Id = @Utility_Dtl_Service_Level_Id
END                        
GO
GRANT EXECUTE ON  [dbo].[Utility_Dtl_Service_Level_Upd] TO [CBMSApplication]
GO
