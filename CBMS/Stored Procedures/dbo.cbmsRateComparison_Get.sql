SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	cbms_prod.dbo.cbmsRateComparison_Get

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@rc_rate_comparison_id	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC cbmsRateComparison_Get 9214
	EXEC cbmsRateComparison_Get 9137

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script
	SSR			03/29/2010	Added table Owner name and Removed Entity(Entity_id) with Commodity(commodity_id)	        	
							Replaced View vwsiteName with the base tables.
	DMR			09/10/2010	Modified for Quoted_Identifier
	HG			2020-01-16	RC change, returning the Manually uploaded image id if system generated image id is not there.						
******/
CREATE PROCEDURE [dbo].[cbmsRateComparison_Get]
    (@rc_rate_comparison_id INT)
AS
BEGIN

    SET NOCOUNT ON;

    SELECT
        rc.RC_RATE_COMPARISON_ID
       ,rc.CLIENT_ID
       ,rc.SITE_ID
       ,RTRIM(ad.CITY) + ', ' + st.STATE_NAME + ' (' + s.SITE_NAME + ')' Site_Name
       ,rc.CREATION_DATE
       ,r.COMMODITY_TYPE_ID
       ,c.Commodity_Name commodity_type
       ,a.ACCOUNT_NUMBER
       ,ISNULL(rc.CBMS_IMAGE_ID, rc.RATE_COMPARISON_DOCUMENT_IMAGE_ID) AS Cbms_Image_Id
    FROM
        dbo.RC_RATE_COMPARISON rc
        JOIN dbo.SITE s
            ON s.SITE_ID = rc.SITE_ID
        JOIN dbo.RATE r
            ON r.RATE_ID = rc.RATE_ID
        JOIN dbo.Commodity c
            ON c.Commodity_Id = r.COMMODITY_TYPE_ID
        JOIN dbo.ACCOUNT a
            ON a.ACCOUNT_ID = rc.ACCOUNT_ID
        JOIN dbo.ADDRESS ad
            ON ad.ADDRESS_ID = s.PRIMARY_ADDRESS_ID
        JOIN dbo.STATE st
            ON st.STATE_ID = ad.STATE_ID
    WHERE
        rc.RC_RATE_COMPARISON_ID = @rc_rate_comparison_id;

END;
GO
GRANT EXECUTE ON  [dbo].[cbmsRateComparison_Get] TO [CBMSApplication]
GO
