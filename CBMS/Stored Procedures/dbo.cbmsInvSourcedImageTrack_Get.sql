SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[cbmsInvSourcedImageTrack_Get]
	( @MyAccountId int
	, @inv_sourced_image_track_id int
	)
AS
BEGIN
	set nocount on
	   select inv_sourced_image_track_id
		, inv_sourced_image_id
		, original_filename
		, prokarma_filename
		, is_received
		, cu_invoice_id
		, track_comments
	     from inv_sourced_image_track
	    where inv_sourced_image_track_id = @inv_sourced_image_track_id
END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvSourcedImageTrack_Get] TO [CBMSApplication]
GO
