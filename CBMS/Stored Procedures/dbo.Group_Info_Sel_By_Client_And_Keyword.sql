SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
 NAME: [dbo].[Group_Info_Sel_By_Client_And_Keyword]    
                    
 DESCRIPTION:                      
      To select the columns for cilent group information map table given the client id with optional Keyword.                    
                      
 INPUT PARAMETERS:                      
                     
 Name                              DataType        Default       Description                      
-------------------------------------------------------------------------------------------------------------                    
 @Client_Id                        INT      NULL               
 @keyword                          NVARCHAR(200)   NULL  
 @Start_Index						INT   1          
 @End_Index							INT   2147483647  
 @Total_Row_Count					INT	  0
              
                      
 OUTPUT PARAMETERS:                            
 Name                              DataType        Default       Description                      
-------------------------------------------------------------------------------------------------------------                    
                      
 USAGE EXAMPLES:                          
-------------------------------------------------------------------------------------------------------------                     
                     
    EXEC Group_Info_Sel_By_Client_And_Keyword 
      @Client_Id = 235
     ,@Start_Index = 1
     ,@End_Index = 1000
     ,@Total_Row_Count = 0                   
     

   EXEC Group_Info_Sel_By_Client_And_Keyword 
      @Client_Id = 218
     ,@Start_Index = 1
     ,@End_Index = 1000
     ,@Total_Row_Count = 0         

   EXEC Group_Info_Sel_By_Client_And_Keyword 
      @Client_Id = 11231
     ,@Start_Index = 1
     ,@End_Index = 1000
     ,@Total_Row_Count = 0  
 
   EXEC Group_Info_Sel_By_Client_And_Keyword   
      @Client_Id = 235  
     ,@Start_Index = 1  
     ,@End_Index = 1000  
     ,@Total_Row_Count = 0

 AUTHOR INITIALS:                     
                     
 Initials               Name                      
-------------------------------------------------------------------------------------------------------------                    
 NR                     Narayana Reddy                        
                       
 MODIFICATIONS:                     
                      
 Initials               Date            Modification
-------------------------------------------------------------------------------------------------------------
 NR                     2013-11-25      Created for RA Admin user management

******/
CREATE PROCEDURE [dbo].[Group_Info_Sel_By_Client_And_Keyword]
      ( 
       @Client_Id INT
      ,@keyword NVARCHAR(200) = NULL
      ,@Start_Index INT = 1
      ,@End_Index INT = 2147483647
      ,@Total_Row_Count INT = 0 )
AS 
BEGIN             
      
      SET NOCOUNT ON; 

      IF ( @Total_Row_Count = 0 ) 
            BEGIN

                  SELECT
                        @Total_Row_Count = COUNT(1)
                  FROM
                        Client_Group_Info_Map cgim
                        INNER JOIN dbo.group_info gf
                              ON cgim.Group_Info_Id = gf.GROUP_INFO_ID
                  WHERE
                        Client_Id = @Client_Id
                        AND ( @keyword IS NULL
                              OR gf.group_name LIKE '%' + @keyword + '%'
                              OR gf.GROUP_DESCRIPTION LIKE '%' + @keyword + '%' )
            END;
      WITH  cte_Group_Info_Sel
              AS ( SELECT TOP ( @End_Index )
                        gic.Group_Info_Category_Id
                       ,gic.Category_Name
                       ,gf.GROUP_NAME
                       ,gf.GROUP_DESCRIPTION
                       ,gf.Group_Info_Id
                       ,gic.Sort_Order
                       ,gf.Is_Required_For_RA_Login
                       ,cd.Code_Value AS App_Access_Role_Type_Cd
                       ,COUNT(users.User_Info_Id) AS Users_Count
                       ,gf.Is_Fee_Module
                       ,ROW_NUMBER() OVER ( ORDER BY gic.Sort_Order ) Row_Num
                   FROM
                        dbo.Group_Info gf
                        INNER JOIN dbo.Group_Info_Category gic
                              ON gic.Group_Info_Category_Id = gf.Group_Info_Category_Id
                        INNER JOIN dbo.Client_Group_Info_Map cgm
                              ON gf.GROUP_INFO_ID = cgm.Group_Info_Id
                                 AND cgm.Client_Id = @Client_Id
                        LEFT OUTER JOIN ( dbo.Group_Info_App_Access_Role_Type_Cd gcd
                                          INNER JOIN dbo.Code cd
                                                ON gcd.App_Access_Role_Type_Cd = cd.Code_Id )
                                          ON cgm.Group_Info_Id = gcd.Group_Info_Id
                        LEFT OUTER JOIN ( SELECT
                                                map.group_info_id
                                               ,map.User_Info_Id
                                          FROM
                                                dbo.User_Info_Group_Info_Map map
                                                INNER JOIN dbo.USER_INFO ui
                                                      ON map.User_Info_Id = ui.USER_INFO_ID
                                                         AND ui.ACCESS_LEVEL = 1
                                                         AND ui.IS_HISTORY = 0
                                          WHERE
                                                ui.CLIENT_ID = @CLIENT_ID
                                          UNION
                                          SELECT
                                                gim.GROUP_INFO_ID
                                               ,urm.User_Info_Id
                                          FROM
                                                dbo.Client_App_Access_Role_Group_Info_Map gim
                                                INNER JOIN dbo.Client_App_Access_Role ar
                                                      ON gim.Client_App_Access_Role_Id = ar.Client_App_Access_Role_Id
                                                INNER JOIN dbo.User_Info_Client_App_Access_Role_Map urm
                                                      ON ar.Client_App_Access_Role_Id = urm.Client_App_Access_Role_Id
                                                INNER JOIN dbo.User_Info ui
                                                      ON urm.User_Info_Id = ui.User_Info_Id
                                          WHERE
                                                ar.Client_Id = @Client_id
                                                AND ui.IS_HISTORY = 0
                                                AND ui.IS_DEMO_USER = 0
                                          GROUP BY
                                                gim.GROUP_INFO_ID
                                               ,urm.User_Info_Id ) users
                              ON cgm.group_info_id = users.group_info_id
                   WHERE
                        ( @keyword IS NULL
                          OR gf.group_name LIKE '%' + @keyword + '%'
                          OR gf.GROUP_DESCRIPTION LIKE '%' + @keyword + '%' )
                   GROUP BY
                        gic.Group_Info_Category_Id
                       ,gic.Category_Name
                       ,gf.GROUP_NAME
                       ,gf.GROUP_DESCRIPTION
                       ,gf.Group_Info_Id
                       ,gic.Sort_Order
                       ,gf.Is_Required_For_RA_Login
                       ,cd.Code_Value
                       ,gf.Is_Fee_Module)
            SELECT
                  Group_Info_Category_Id
                 ,Category_Name
                 ,GROUP_NAME
                 ,GROUP_DESCRIPTION
                 ,Group_Info_Id
                 ,Sort_Order
                 ,Is_Required_For_RA_Login
                 ,App_Access_Role_Type_Cd
                 ,Users_Count
                 ,Is_Fee_Module
                 ,@Total_Row_Count AS Total_Row_Count
            FROM
                  cte_Group_Info_Sel csa
            WHERE
                  csa.Row_Num BETWEEN @Start_Index AND @End_Index
            ORDER BY
                  csa.Row_Num

END

;
GO
GRANT EXECUTE ON  [dbo].[Group_Info_Sel_By_Client_And_Keyword] TO [CBMSApplication]
GO
