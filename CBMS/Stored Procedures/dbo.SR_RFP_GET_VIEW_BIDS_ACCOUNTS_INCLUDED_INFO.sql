
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********    
NAME: dbo.SR_RFP_GET_VIEW_BIDS_ACCOUNTS_INCLUDED_INFO     
  
DESCRIPTION:       
  
   
   
INPUT PARAMETERS:      
Name           DataType  Default      Description      
------------------------------------------------------------      
@userId         VARCHAR
@sessionId      VARCHAR
@accountGroupId INT
@isBidId        INT 
  
  
OUTPUT PARAMETERS:      
Name           DataType  Default      Description      
------------------------------------------------------------      
  
USAGE EXAMPLES:      
------------------------------------------------------------      
  
EXEC dbo.SR_RFP_GET_VIEW_BIDS_ACCOUNTS_INCLUDED_INFO 1,1,10007889,1  
EXEC dbo.SR_RFP_GET_VIEW_BIDS_ACCOUNTS_INCLUDED_INFO 1,1,10007289,1  
EXEC dbo.SR_RFP_GET_VIEW_BIDS_ACCOUNTS_INCLUDED_INFO 1,1,12020969,0  
EXEC dbo.SR_RFP_GET_VIEW_BIDS_ACCOUNTS_INCLUDED_INFO 1,1,12057339,0  
  
  
AUTHOR INITIALS:      
	Initials	Name      
------------------------------------------------------------      
	RR			Raghu Reddy  
	NR			Narayana Reddy
  
   
MODIFICATIONS       
	Initials	Date		Modification      
------------------------------------------------------------      
	RR			2014-04-15	Added description header  
							MAINT-2696 Added Utility and client name in select list, replaced base tables with client hier  .
	NR			2014-1-03   Added the new column Site_Reference_Number in select list.
	RR			2016-03-10	Global Sourcing - Phase3 - GCS-527 Added Alternate_Account_Number to select list

******/  
  
CREATE  PROCEDURE [dbo].[SR_RFP_GET_VIEW_BIDS_ACCOUNTS_INCLUDED_INFO]
      ( 
       @userId VARCHAR
      ,@sessionId VARCHAR
      ,@accountGroupId INT
      ,@isBidId INT )
AS 
SET NOCOUNT ON;

DECLARE @Bid_Status_Closed INT 

SELECT
      @Bid_Status_Closed = ENTITY_ID
FROM
      dbo.ENTITY
WHERE
      ENTITY_NAME = 'closed'
      AND ENTITY_TYPE = 1029
      
BEGIN  
      IF @isBidId = 1 
            BEGIN  
  
                  SELECT
                        ch.Site_name
                       ,cha.Account_Number
                       ,cha.Meter_Address_Line_1 + ' ' + cha.Meter_Address_Line_2 AS ADDRESS
                       ,cha.Account_Vendor_Name AS UTILITY_NAME
                       ,ch.Client_Name
                       ,ch.Site_Reference_Number
                       ,cha.Alternate_Account_Number
                  FROM
                        dbo.SR_RFP_ACCOUNT sr_rfp
                        JOIN Core.Client_Hier_Account cha
                              ON sr_rfp.ACCOUNT_ID = cha.Account_Id
                        JOIN Core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        sr_rfp.SR_RFP_BID_GROUP_ID = @accountGroupId
                        AND sr_rfp.IS_DELETED = 0
                        AND sr_rfp.BID_STATUS_TYPE_ID != @Bid_Status_Closed
                        AND cha.Account_Type = 'Utility'
                  UNION
                  SELECT
                        arc_acc.site_name
                       ,arc_acc.account_number
                       ,arc_meter.METER_ADDRESS ADDRESS
                       ,arc_acc.UTILITY_NAME
                       ,arc_acc.CLIENT_NAME
                       ,ch.Site_Reference_Number
                       ,cha.Alternate_Account_Number
                  FROM
                        dbo.SR_RFP_ACCOUNT sr_rfp
                        JOIN sr_rfp_archive_account arc_acc
                              ON arc_acc.sr_rfp_account_id = sr_rfp.sr_rfp_account_id
                        JOIN SR_RFP_ACCOUNT_METER_MAP met_map
                              ON met_map.sr_rfp_account_id = arc_acc.sr_rfp_account_id
                        JOIN SR_RFP_ARCHIVE_METER arc_meter
                              ON arc_meter.SR_RFP_ACCOUNT_METER_MAP_ID = met_map.SR_RFP_ACCOUNT_METER_MAP_ID
                        INNER JOIN core.Client_Hier_Account cha
                              ON sr_rfp.ACCOUNT_ID = cha.Account_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        sr_rfp.SR_RFP_BID_GROUP_ID = @accountGroupId
                        AND sr_rfp.IS_DELETED = 0
                        AND sr_rfp.BID_STATUS_TYPE_ID = @Bid_Status_Closed 
                      
            END  
  
      ELSE 
            IF @isBidId = 0 
                  BEGIN  
  
                        SELECT
                              ch.Site_name
                             ,cha.Account_Number
                             ,cha.Meter_Address_Line_1 + ' ' + cha.Meter_Address_Line_2 AS ADDRESS
                             ,cha.Account_Vendor_Name AS UTILITY_NAME
                             ,ch.Client_Name
                             ,ch.Site_Reference_Number
                             ,cha.Alternate_Account_Number
                        FROM
                              dbo.SR_RFP_ACCOUNT sr_rfp
                              JOIN Core.Client_Hier_Account cha
                                    ON sr_rfp.ACCOUNT_ID = cha.Account_Id
                              JOIN Core.Client_Hier ch
                                    ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        WHERE
                              sr_rfp.SR_RFP_ACCOUNT_ID = @accountGroupId
                              AND sr_rfp.IS_DELETED = 0
                              AND cha.Account_Type = 'Utility'
                        UNION
                        SELECT
                              arc_acc.SITE_NAME
                             ,arc_acc.ACCOUNT_NUMBER
                             ,arc_meter.METER_ADDRESS ADDRESS
                             ,arc_acc.UTILITY_NAME
                             ,arc_acc.CLIENT_NAME
                             ,ch.Site_Reference_Number
                             ,cha.Alternate_Account_Number
                        FROM
                              dbo.SR_RFP_ACCOUNT sr_rfp
                              JOIN dbo.SR_RFP_ARCHIVE_ACCOUNT arc_acc
                                    ON arc_acc.SR_RFP_ACCOUNT_ID = sr_rfp.SR_RFP_ACCOUNT_ID
                              JOIN dbo.SR_RFP_ACCOUNT_METER_MAP met_map
                                    ON met_map.SR_RFP_ACCOUNT_ID = arc_acc.SR_RFP_ACCOUNT_ID
                              JOIN dbo.SR_RFP_ARCHIVE_METER arc_meter
                                    ON arc_meter.SR_RFP_ACCOUNT_METER_MAP_ID = met_map.SR_RFP_ACCOUNT_METER_MAP_ID
                              INNER JOIN core.Client_Hier_Account cha
                                    ON sr_rfp.ACCOUNT_ID = cha.Account_Id
                              INNER JOIN core.Client_Hier ch
                                    ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        WHERE
                              sr_rfp.SR_RFP_ACCOUNT_ID = @accountGroupId
                              AND sr_rfp.IS_DELETED = 0
                              AND sr_rfp.BID_STATUS_TYPE_ID = @Bid_Status_Closed  
                  END  
END;




;

;
GO




GRANT EXECUTE ON  [dbo].[SR_RFP_GET_VIEW_BIDS_ACCOUNTS_INCLUDED_INFO] TO [CBMSApplication]
GO
