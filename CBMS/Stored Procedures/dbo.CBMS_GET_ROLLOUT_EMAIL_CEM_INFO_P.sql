SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE  PROCEDURE dbo.CBMS_GET_ROLLOUT_EMAIL_CEM_INFO_P
	@user_id varchar(10),
	@session_id varchar(20),
	@site_id int
	AS

	select 	cli.client_name,
		vwSite.site_name,
		cem.first_name+' '+cem.last_name+'<'+cem.email_address+'>' as cem_email_address
			

	from 	vwSiteName vwSite(nolock),
		client cli(nolock),
		client_cem_map map(nolock),
		user_info cem(nolock) 
	where	vwSite.site_id = @site_id
		and cli.client_id = vwSite.client_id
		and map.client_id = cli.client_id
		and cem.user_info_id = map.user_info_id







GO
GRANT EXECUTE ON  [dbo].[CBMS_GET_ROLLOUT_EMAIL_CEM_INFO_P] TO [CBMSApplication]
GO
