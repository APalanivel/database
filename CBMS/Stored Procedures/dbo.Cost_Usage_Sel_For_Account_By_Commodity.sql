SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/******
NAME:
	CBMS.dbo.Cost_Usage_Sel_For_Account_By_Commodity

DESCRIPTION:
		
		Used to select cost usage data for the given account and commodity
		As application doesn't have Client_id information on this page getting the currency group id in a variable and using it in main query.
		
INPUT PARAMETERS:
	Name					DataType		Default	Description
---------------------------------------------------------------
	@Account_Id				int
	@Commodity_id			int
	@Begin_Dt				Date
	@End_Dt					Date
	@Uom_Type_Id			INT
	@Currency_Unit_Id		INT
    @Is_Required_For_Variance_Test BIT
    @Client_Hier_ID			INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Cost_Usage_Sel_For_Account_By_Commodity 127,67,'2010-01-01','2010-12-01',3,1,2586

	 EXEC Cost_Usage_Sel_For_Account_By_Commodity 
			  @Account_Id = 209976
			 ,@Client_Hier_Id = 53868
			 ,@Begin_Dt = '1/1/2011'
			 ,@End_Dt = '12/1/2011'
			 ,@Currency_Unit_Id = 3
			 ,@Commodity_id = 290

	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hari
	SSR			Sharad Srivastava
    AP			Athmaram Pabbathi
    SP			Sandeep Pigilam
     
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG			02/05/2010  Created
	SSR			03/04/2010  Removed Input parameter @Client_ID and joined Account & SUPPLIER_ACCOUNT_METER_MAP to get currency group id.
	SSR			04/14/2010  Replace LEFT JOIN of cost_usage_Account_dtl into INNER JOIN with Bucket_master
	HG			04/29/2010  Unit Cost added as one of the row with other bucket values as per the variance page requirement
	DMR			09/10/2010  Modified for Quoted_Identifier
	HG			09/09/2011  Modifed for the additional data requirement
								- Hardcoded Bucket name used to find the unit cost logic replaced by Calling a new procedure Cost_usage_Bucket_Sel_By_Commodity to get the respective bucket id for total cost/ voume and total cost
								- Removed @Uom_Type_id parameter and used the default uom saved in the bucket master for all Determinant type buckets
								- Modified filter Is_Show_On_Account = 1 to Is_Required_For_Variance_Test = 1 to fetch only the buckets required for variance test (added @Is_Required_For_Variance_Test as a parameter and defaulted the value to 1)
	AP			09/12/2011  Removed CTE and used Table variable instead.
	HG			2012-03-22	Client_Hier_id param added as this column added in CUAD table.
	HG			2012-07-09	MAINT-1355, modified to fix the Unit cost calculation to consider only Total Usage/ Volume determinant
								--UNION changed to UNION ALL as it is always going to return unique set of values
	AKR			2013-07-16  Modified for Detailed Variance, Added UOM_Id and UOM_Name
	SP			2016-13-10	Variance Test Enhancements,Used CURRENCY_UNIT_COUNTRY_MAP for default currency_id and Country_Commodity_Uom for default uom_id. 	
	RR			28-11-2016	VTE-60 Removed @Client_Hier_ID filter on dbo.Cost_Usage_Account_Dtl table as total bucket values are not returning if the
							supplier account associated with multiple sites						
******/
CREATE PROCEDURE [dbo].[Cost_Usage_Sel_For_Account_By_Commodity]
      @Account_Id INT
     ,@Commodity_id INT
     ,@Begin_Dt DATE
     ,@End_Dt DATE
     ,@Currency_Unit_Id INT
     ,@Is_Required_For_Variance_Test BIT = 1
     ,@Client_Hier_ID INT
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Cost_Usage_Bucket_Id TABLE
            ( 
             Bucket_Master_Id INT
            ,Bucket_Name VARCHAR(200)
            ,Bucket_Type VARCHAR(25) )

      DECLARE @Cost_Usage_Account_Dtl TABLE
            ( 
             Service_Month DATE
            ,Account_Id INT
            ,Bucket_Type VARCHAR(25)
            ,Bucket_Name VARCHAR(255)
            ,Bucket_Value DECIMAL(28, 10)
            ,Total_Cost DECIMAL(28, 10)
            ,Volume DECIMAL(28, 10)
            ,UOM_Id INT
            ,UOM_Name VARCHAR(200) )

      DECLARE
            @Currency_Group_Id INT
           ,@Currency_Name VARCHAR(200)
           ,@Usage_Bucket_Stream_Cd INT
           ,@Country_Id INT
           ,@Country_Default_Uom_Type_Id INT           
      

      SELECT
            @Usage_Bucket_Stream_Cd = cd.Code_Id
      FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code cd
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'BucketStream'
            AND cd.Code_Value = 'Usage';
            
      SELECT TOP 1
            @Currency_Group_Id = ch.Client_Currency_Group_Id
           ,@Country_Id = ch.Country_Id
      FROM
            core.Client_Hier ch
      WHERE
            ch.Client_Hier_id = @Client_Hier_Id

      SELECT
            @Country_Default_Uom_Type_Id = ccu.Default_Uom_Type_Id
      FROM
            dbo.Country_Commodity_Uom ccu
      WHERE
            ccu.Commodity_Id = @Commodity_Id
            AND ccu.COUNTRY_ID = @Country_Id
                        

      SELECT
            @Currency_Unit_Id = MAX(cucm.CURRENCY_UNIT_ID)
      FROM
            dbo.CURRENCY_UNIT_COUNTRY_MAP cucm
      WHERE
            cucm.Country_Id = @Country_Id            
            
      SELECT
            @Currency_Name = cu.CURRENCY_UNIT_NAME
      FROM
            dbo.CURRENCY_UNIT cu
      WHERE
            cu.CURRENCY_UNIT_ID = @Currency_Unit_Id      

      INSERT      INTO @Cost_Usage_Bucket_Id
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Bucket_Type )
                  EXEC dbo.Cost_usage_Bucket_Sel_By_Commodity 
                        @Commodity_id = @Commodity_Id;

      INSERT      INTO @Cost_Usage_Account_Dtl
                  ( 
                   Service_Month
                  ,Account_Id
                  ,Bucket_Type
                  ,Bucket_Name
                  ,Bucket_Value
                  ,Total_Cost
                  ,Volume
                  ,UOM_Id
                  ,UOM_Name )
                  SELECT
                        cua.Service_Month
                       ,@Account_Id ACCOUNT_ID
                       ,bkt_cd.Code_Value Bucket_Type
                       ,bm.Bucket_Name
                       ,sum(cua.Bucket_Value) * ( CASE WHEN bkt_cd.Code_Value = 'Charge' THEN cc.Conversion_Factor
                                                       WHEN bkt_cd.Code_Value = 'Determinant' THEN uc.Conversion_Factor
                                                  END ) Bucket_Value
                       ,( CASE WHEN cua.Bucket_Master_Id = cub.Bucket_Master_Id
                                    AND cub.Bucket_Type = 'Charge' THEN sum(cua.Bucket_Value) * cc.Conversion_Factor
                               ELSE 0
                          END ) AS Total_Cost
                       ,( CASE WHEN cua.Bucket_Master_Id = cub.Bucket_Master_Id
                                    AND cub.Bucket_Type = 'Determinant' THEN sum(cua.Bucket_Value) * uc.Conversion_Factor
                               ELSE 0
                          END ) AS Volume
                       ,CASE WHEN bkt_cd.Code_Value = 'Charge' THEN @Currency_Unit_Id
                             ELSE ( CASE WHEN bm.Bucket_Stream_Cd = @Usage_Bucket_Stream_Cd THEN @Country_Default_Uom_Type_Id
                                         ELSE bm.Default_Uom_Type_Id
                                    END )
                        END UOM_Id
                       ,CASE WHEN bkt_cd.Code_Value = 'Charge' THEN @Currency_Name
                             ELSE en.ENTITY_NAME
                        END UOM_Name
                  FROM
                        dbo.Cost_Usage_Account_Dtl cua
                        INNER JOIN dbo.Bucket_Master bm
                              ON bm.Bucket_Master_Id = cua.Bucket_Master_Id
                        INNER JOIN dbo.Code bkt_cd
                              ON bkt_cd.Code_Id = bm.Bucket_Type_Cd
                        LEFT OUTER JOIN @Cost_Usage_Bucket_Id cub
                              ON cub.Bucket_Master_Id = cua.Bucket_Master_Id
                        LEFT OUTER JOIN dbo.consumption_unit_conversion uc
                              ON uc.Base_Unit_ID = cua.UOM_Type_Id
                                 AND uc.Converted_Unit_ID = ( CASE WHEN bm.Bucket_Stream_Cd = @Usage_Bucket_Stream_Cd THEN @Country_Default_Uom_Type_Id
                                                                   ELSE bm.Default_Uom_Type_Id
                                                              END )
                        LEFT OUTER JOIN dbo.ENTITY en
                              ON en.ENTITY_ID = ( CASE WHEN bm.Bucket_Stream_Cd = @Usage_Bucket_Stream_Cd THEN @Country_Default_Uom_Type_Id
                                                       ELSE bm.Default_Uom_Type_Id
                                                  END )
                        LEFT OUTER JOIN dbo.Currency_Unit_Conversion cc
                              ON ( cc.Currency_Group_Id = @Currency_Group_Id
                                   AND cc.Base_Unit_ID = cua.Currency_Unit_ID
                                   AND cc.converted_unit_id = @Currency_Unit_ID
                                   AND cc.conversion_date = cua.Service_month )
                  WHERE
                        cua.Account_ID = @Account_Id
                        --AND cua.Client_Hier_Id = @Client_Hier_Id
                        AND cua.Service_Month BETWEEN @Begin_Dt AND @End_Dt
                        AND bm.Commodity_Id = @Commodity_id
                        AND bm.Is_Required_For_Variance_Test = @Is_Required_For_Variance_Test
						--AND NOT EXISTS (SELECT 1 FROM CORE.Client_Hier_Account CHA
						--				JOIN dbo.Cost_Usage_Account_Dtl CUAD
						--				ON CUAD.ACCOUNT_ID = CHA.Account_Id
						--				AND CUAD.Client_Hier_ID = CHA.Client_Hier_Id 
						--				JOIN dbo.Variance_category_Bucket_Map vcbm
						--					ON vcbm.Bucket_master_Id = cuaD.Bucket_Master_Id
						--					WHERE CHA.Account_Type ='UTILITY'
						--					AND CUAD.ACCOUNT_ID = @Account_Id
						--					AND CUAD.Bucket_Master_Id = CUA.Bucket_Master_Id
						--					AND cua.Service_Month BETWEEN @Begin_Dt AND @End_Dt)
                  GROUP BY
                        cua.Service_Month
                       ,bkt_cd.Code_Value
                       ,bm.Bucket_Name
                       ,uc.Conversion_Factor
                       ,cc.Conversion_Factor
                       ,cua.Bucket_Master_Id
                       ,cub.Bucket_Master_Id
                       ,cub.Bucket_Type
                       ,bm.Bucket_Stream_Cd
                       ,bm.Default_Uom_Type_Id
                       ,en.ENTITY_NAME


      SELECT
            cua.Account_Id
           ,cua.Service_Month
           ,cua.Bucket_Type
           ,cua.Bucket_Name
           ,cua.Bucket_Value
           ,cua.UOM_Id
           ,cua.UOM_Name
      FROM
            @Cost_Usage_Account_Dtl cua
      UNION ALL
      SELECT
            Account_id
           ,Service_Month
           ,'Charge' Bucket_Type
           ,'Unit Cost' Bucket_Name
           ,sum(Total_Cost) / nullif(sum(Volume), 0) Bucket_Value
           ,@Currency_Unit_Id UOM_Id
           ,@Currency_Name UOM_Name
      FROM
            @Cost_Usage_Account_Dtl cu
      WHERE
            cu.Bucket_Name IN ( 'Total Cost', 'Total Usage', 'Volume' )
      GROUP BY
            Account_id
           ,Service_Month


END;
;




;
GO





GRANT EXECUTE ON  [dbo].[Cost_Usage_Sel_For_Account_By_Commodity] TO [CBMSApplication]
GO
