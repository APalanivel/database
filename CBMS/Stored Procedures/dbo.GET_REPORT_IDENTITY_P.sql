SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.GET_REPORT_IDENTITY_P
@userId varchar,
@sessionId varchar,
@batchId int,
@clientId int,
@divisionId int,
@siteId int,
@volumeId int,
@volumeValue int,
@currencyId int,
@currencyValue int,
@status int,
@reportListId int,
@yearId int,
@yearValue int,
@groupId int

as
	set nocount on
if @status=1

	select RM_REPORT_ID from RM_REPORT_FILTER_DATA 

	where	INPUT_PARAMETER=@volumeValue AND
		INPUT_PARAMETER_TYPE_ID=@volumeId AND

		RM_REPORT_ID IN(select	RM_REPORT_ID
				from	RM_REPORT_BATCH_LOG
				where	RM_REPORT_BATCH_ID=@batchId AND
					CLIENT_ID=@clientId AND
					SITE_ID IS NULL AND
					DIVISION_ID IS NULL
				) AND

		RM_REPORT_ID IN(select RM_REPORT_ID from RM_REPORT_FILTER_DATA 
				where	INPUT_PARAMETER=@currencyValue AND
					INPUT_PARAMETER_TYPE_ID=@currencyId AND
					RM_REPORT_ID IN(select	RM_REPORT_ID
							from	RM_REPORT_BATCH_LOG
							where	RM_REPORT_BATCH_ID=@batchId AND
								CLIENT_ID=@clientId AND
								SITE_ID IS NULL AND
								DIVISION_ID IS NULL
							)
				) AND

		RM_REPORT_ID IN(select RM_REPORT_ID from RM_REPORT_FILTER_DATA 
				where	INPUT_PARAMETER=@yearValue AND
					INPUT_PARAMETER_TYPE_ID=@yearId AND
					RM_REPORT_ID IN(select	RM_REPORT_ID
							from	RM_REPORT_BATCH_LOG
							where	RM_REPORT_BATCH_ID=@batchId AND
								CLIENT_ID=@clientId AND
								SITE_ID IS NULL AND
								DIVISION_ID IS NULL
							)
				) AND
		
		RM_REPORT_ID IN(select	RM_REPORT_ID
				from	RM_REPORT
				where	REPORT_LIST_ID=@reportListId
				) 

else if @status=2

	select RM_REPORT_ID from RM_REPORT_FILTER_DATA 

	where	INPUT_PARAMETER=@volumeValue AND
		INPUT_PARAMETER_TYPE_ID=@volumeId AND

		RM_REPORT_ID IN(select	RM_REPORT_ID
				from	RM_REPORT_BATCH_LOG
				where	RM_REPORT_BATCH_ID=@batchId AND
					CLIENT_ID=@clientId AND
					DIVISION_ID =@divisionId 
				) AND

		RM_REPORT_ID IN(select RM_REPORT_ID from RM_REPORT_FILTER_DATA 
				where	INPUT_PARAMETER=@currencyValue AND
					INPUT_PARAMETER_TYPE_ID=@currencyId AND
					RM_REPORT_ID IN(select	RM_REPORT_ID
							from	RM_REPORT_BATCH_LOG
							where	RM_REPORT_BATCH_ID=@batchId AND
								CLIENT_ID=@clientId AND
								DIVISION_ID =@divisionId 	
							)
				) AND

		RM_REPORT_ID IN(select RM_REPORT_ID from RM_REPORT_FILTER_DATA 
				where	INPUT_PARAMETER=@yearValue AND
					INPUT_PARAMETER_TYPE_ID=@yearId AND
					RM_REPORT_ID IN(select	RM_REPORT_ID
							from	RM_REPORT_BATCH_LOG
							where	RM_REPORT_BATCH_ID=@batchId AND
								CLIENT_ID=@clientId AND								
								DIVISION_ID =@divisionId
							)
				) AND

		RM_REPORT_ID IN(select	RM_REPORT_ID
				from	RM_REPORT
				where	REPORT_LIST_ID=@reportListId
				) 

else if @status=3

	select RM_REPORT_ID from RM_REPORT_FILTER_DATA 

	where	INPUT_PARAMETER=@volumeValue AND
		INPUT_PARAMETER_TYPE_ID=@volumeId AND

		RM_REPORT_ID IN(select	RM_REPORT_ID
				from	RM_REPORT_BATCH_LOG
				where	RM_REPORT_BATCH_ID=@batchId AND
					CLIENT_ID=@clientId AND
					SITE_ID =@siteId 
				) AND

		RM_REPORT_ID IN(select RM_REPORT_ID from RM_REPORT_FILTER_DATA 
				where	INPUT_PARAMETER=@currencyValue AND
					INPUT_PARAMETER_TYPE_ID=@currencyId AND
					RM_REPORT_ID IN(select	RM_REPORT_ID
							from	RM_REPORT_BATCH_LOG
							where	RM_REPORT_BATCH_ID=@batchId AND
								CLIENT_ID=@clientId AND
								SITE_ID =@siteId 	
							)
				) AND

		RM_REPORT_ID IN(select RM_REPORT_ID from RM_REPORT_FILTER_DATA 
				where	INPUT_PARAMETER=@yearValue AND
					INPUT_PARAMETER_TYPE_ID=@yearId AND
					RM_REPORT_ID IN(select	RM_REPORT_ID
							from	RM_REPORT_BATCH_LOG
							where	RM_REPORT_BATCH_ID=@batchId AND
								CLIENT_ID=@clientId AND								
								SITE_ID =@siteId 
							)
				) AND

		RM_REPORT_ID IN(select	RM_REPORT_ID
				from	RM_REPORT
				where	REPORT_LIST_ID=@reportListId
				) 



else if @status=4

	select RM_REPORT_ID from RM_REPORT_FILTER_DATA 

	where	INPUT_PARAMETER=@volumeValue AND
		INPUT_PARAMETER_TYPE_ID=@volumeId AND

		RM_REPORT_ID IN(select	RM_REPORT_ID
				from	RM_REPORT_BATCH_LOG
				where	RM_REPORT_BATCH_ID=@batchId AND
					CLIENT_ID=@clientId AND
					RM_GROUP_ID =@groupId 
				) AND

		RM_REPORT_ID IN(select RM_REPORT_ID from RM_REPORT_FILTER_DATA 
				where	INPUT_PARAMETER=@currencyValue AND
					INPUT_PARAMETER_TYPE_ID=@currencyId AND
					RM_REPORT_ID IN(select	RM_REPORT_ID
							from	RM_REPORT_BATCH_LOG
							where	RM_REPORT_BATCH_ID=@batchId AND
								CLIENT_ID=@clientId AND
								RM_GROUP_ID =@groupId 	
							)
				) AND

		RM_REPORT_ID IN(select RM_REPORT_ID from RM_REPORT_FILTER_DATA 
				where	INPUT_PARAMETER=@yearValue AND
					INPUT_PARAMETER_TYPE_ID=@yearId AND
					RM_REPORT_ID IN(select	RM_REPORT_ID
							from	RM_REPORT_BATCH_LOG
							where	RM_REPORT_BATCH_ID=@batchId AND
								CLIENT_ID=@clientId AND								
								RM_GROUP_ID =@groupId
							)
				) AND

		RM_REPORT_ID IN(select	RM_REPORT_ID
				from	RM_REPORT
				where	REPORT_LIST_ID=@reportListId
				)
GO
GRANT EXECUTE ON  [dbo].[GET_REPORT_IDENTITY_P] TO [CBMSApplication]
GO
