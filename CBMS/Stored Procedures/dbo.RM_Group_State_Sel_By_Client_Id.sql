SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
   
 dbo.RM_Group_State_Sel_By_Client_Id
   
 DESCRIPTION:     
   
 This procedure gets all the Supplier name associated to the user.  
   
 INPUT PARAMETERS:    
 Name				DataType		 Default		 Description    
----------------------------------------------------------------------      
 @Client_Id	INT
   
 OUTPUT PARAMETERS:    
 Name				DataType		 Default		 Description    
----------------------------------------------------------------------

  
  USAGE EXAMPLES:    
------------------------------------------------------------  

  
	EXEC RM_Group_State_Sel_By_Client_Id 235 
	   
	
  
AUTHOR INITIALS:    
 Initials		Name    
------------------------------------------------------------    
NR				Narayana Reddy
    
 MODIFICATIONS     
 Initials			Date			Modification    
------------------------------------------------------------    
   NR				25-07-2018		Created For Risk managemnet.		


******/


CREATE PROCEDURE [dbo].[RM_Group_State_Sel_By_Client_Id]
    (
        @Client_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            'State' AS Filter_Name
            , CH.State_Id AS Filter_Value
            , CH.State_Name AS Display_Filter_Name
        FROM
            Core.Client_Hier CH
        WHERE
            CH.Client_Id = @Client_Id
            AND CH.Site_Id > 0
        GROUP BY
            CH.State_Id
            , CH.State_Name
        ORDER BY
            CH.State_Name;

    END;


GO
GRANT EXECUTE ON  [dbo].[RM_Group_State_Sel_By_Client_Id] TO [CBMSApplication]
GO
