SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE dbo.DELETE_CSA_DISPLAY_DETAILS_P
	@user_info_id int
	AS
	begin
		set nocount on
	
		delete CSA_HOMEPAGE_CLIENTS_DISPLAY_LIST 
		where USER_INFO_ID = @user_info_id

	end


GO
GRANT EXECUTE ON  [dbo].[DELETE_CSA_DISPLAY_DETAILS_P] TO [CBMSApplication]
GO
