SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



create         PROCEDURE [dbo].[cbmsUBM_GetByUser]
	( @user_info_id int
	)
AS
BEGIN  

         select ubm.ubm_id
		,ubm_name
	     from ubm ubm	     
		join ubm_service us on us.ubm_id = ubm.ubm_id
	     	join client c on c.ubm_service_id = us.ubm_service_id
		join user_info ui on ui.client_id = c.client_id	     
 	    where ui.user_info_id = @user_info_id


END

GO
GRANT EXECUTE ON  [dbo].[cbmsUBM_GetByUser] TO [CBMSApplication]
GO
