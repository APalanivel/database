SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	 dbo.Report_DE_Site_Utility_Account_Dtl_Sel_For_ATT 
		
DESCRIPTION:
	Query to Get Site Accoutns Details of ATT
	
INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
   Site_Name        VARCHAR
   Site_ID          INT
   Division_Name    VARCHAR
   STATE_NAME       VARCHAR
   REGION_NAME      VARCHAR
   Account_Number   INT
   Vendor_Name      VARCHAR
   METER_NUMBER     INT
   rate_Name        VARCHAR
   username         VARCHAR
   modified_date    DATE
   
   
USAGE EXAMPLES:
------------------------------------------------------------
EXEC dbo.Report_DE_Site_Utility_Account_Dtl_Sel_For_ATT 



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	AC         AJAY CHEJARLA   

MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	AC        02/13/2013   Created
	
******/
CREATE PROCEDURE [dbo].[Report_DE_Site_Utility_Account_Dtl_Sel_For_ATT] 
AS
BEGIN
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
    
	SELECT
		  s.Site_Name [Site]
		 ,S.Site_ID [SiteID]
		 ,d.Division_Name [Division]
		 ,st.STATE_NAME [State]
		 ,reg.REGION_NAME [Region]
		 ,a.Account_Number [AccountNumber]
		 ,v.Vendor_Name [Vendor]
		 ,m.METER_NUMBER [Meter]
		 ,r.rate_Name [Rate]
		 ,ui.username [AccountCreatedBy]
		 ,ea.modified_date [AccountCreatedDate]
	FROM
		  dbo.Site s
		  JOIN Division d
				ON d.Division_Id = s.DIVISION_ID
		  JOIN ACCOUNT a
				ON a.SITE_ID = s.SITE_ID
		  JOIN VENDOR v
				ON v.VENDOR_ID = a.VENDOR_ID
		  JOIN METER m
				ON m.ACCOUNT_ID = a.ACCOUNT_ID
		  JOIN RATE r
				ON r.RATE_ID = m.RATE_ID
		  JOIN ENTITY_AUDIT ea
				ON ea.ENTITY_IDENTIFIER = a.ACCOUNT_ID
				   AND ea.ENTITY_ID = 491
				   AND ea.AUDIT_TYPE = 1
		  JOIN user_info ui
				ON ui.user_info_id = ea.user_info_id
		  JOIN ADDRESS addr
				ON addr.ADDRESS_ID = m.ADDRESS_ID
		  JOIN STATE st
				ON st.STATE_ID = addr.STATE_ID
		  JOIN REGION reg
				ON reg.REGION_ID = st.REGION_ID
	WHERE
		  s.Client_ID = 11231
		  AND ea.modified_date > getdate() - 14
	GROUP BY
		  s.Site_Name
		 ,d.Division_Name
		 ,S.Site_ID
		 ,a.Account_Number
		 ,v.Vendor_Name
		 ,m.METER_NUMBER
		 ,r.rate_Name
		 ,ui.username
		 ,ea.modified_date
		 ,REGION_NAME
		 ,st.STATE_NAME

END

;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Site_Utility_Account_Dtl_Sel_For_ATT] TO [CBMS_SSRS_Reports]
GO
