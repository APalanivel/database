SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[cbmsSSOClientInfo_GetCEMUserName]
	( @MyAccountId int
	, @client_id int = null
	)
AS
BEGIN

	 select c.client_id
		, u.username
		, u.first_name + ' ' + u.last_name as full_name
		, u.email_address
	   from client_cem_map c
	   join user_info u on u.user_info_id = c.user_info_id
	  where c.client_id = @client_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSSOClientInfo_GetCEMUserName] TO [CBMSApplication]
GO
