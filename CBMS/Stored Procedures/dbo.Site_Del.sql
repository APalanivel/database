SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Site_Del]  

DESCRIPTION: It Deletes Site for Given Site Id.     
      
INPUT PARAMETERS:          
	NAME		DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Site_Id	INT		
    
OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
	   EXEC Site_Del 193262
  Rollback Tran

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			28-JUN-10	CREATED

*/

CREATE PROCEDURE dbo.Site_Del
    (
       @Site_Id	INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.Site 
	WHERE
		Site_Id = @Site_Id
END
GO
GRANT EXECUTE ON  [dbo].[Site_Del] TO [CBMSApplication]
GO
