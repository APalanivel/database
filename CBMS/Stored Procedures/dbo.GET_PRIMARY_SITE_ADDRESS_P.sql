SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--exec dbo.GET_PRIMARY_SITE_ADDRESS_P 33,33

CREATE      PROCEDURE dbo.GET_PRIMARY_SITE_ADDRESS_P
	@address_parent_id int,
	@site_id int
	AS
	begin
		set nocount on

		select  s.STATE_name,
			a.ADDRESS_LINE1,
			a.ADDRESS_LINE2,
			a.CITY  
		from    site join address a on a.ADDRESS_ID = site.PRIMARY_ADDRESS_ID 
			and site.site_id = @site_id
			join state s on s.state_id = a.state_id  
			and a.ADDRESS_PARENT_ID = @address_parent_id 
		
	end
GO
GRANT EXECUTE ON  [dbo].[GET_PRIMARY_SITE_ADDRESS_P] TO [CBMSApplication]
GO
