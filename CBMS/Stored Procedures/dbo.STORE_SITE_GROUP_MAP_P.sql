SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.STORE_SITE_GROUP_MAP_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@siteId        	int       	          	
	@groupId       	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE [dbo].[STORE_SITE_GROUP_MAP_P] 

@siteId integer,
@groupId integer
AS
set nocount on

IF (select count(1) from RM_GROUP_SITE_MAP where  rm_group_id = @groupId and site_id = @siteId) = 0

BEGIN

 insert into RM_GROUP_SITE_MAP(rm_group_id,site_id) values (@groupId,@siteId)

END
GO
GRANT EXECUTE ON  [dbo].[STORE_SITE_GROUP_MAP_P] TO [CBMSApplication]
GO
