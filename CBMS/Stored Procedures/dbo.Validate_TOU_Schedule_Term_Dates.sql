SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********
NAME:  dbo.Validate_TOU_Schedule_Term_Dates

DESCRIPTION:  

	Validates the passed in start and end dates for a term.
	The passed in start and end dates should not overlap with the dates of any other term within the schedule.
	The SP will return the start date of the term which has dates that are overlapping with the passed in dates.
	If this SP doesn't return any records, it means the passed in dates are valid.

INPUT PARAMETERS:
      Name								DataType		Default     Description
-----------------------------------------------------------------------------------
	  @Time_Of_Use_Schedule_Id			INT
	  @Time_Of_Use_Schedule_Term_Id		INT 
      @Start_Dt							DATETIME
      @End_Dt							DATETIME		NULL
				

OUTPUT PARAMETERS:
      Name								DataType          Default     Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
	EXEC dbo.Validate_TOU_Schedule_Term_Dates 9, 0, '2012-08-01', '2030-12-31'	-- Insert mode
	EXEC dbo.Validate_TOU_Schedule_Term_Dates 9, 76, '2012-08-01', '2030-12-31'	-- Edit mode

AUTHOR INITIALS:
	Initials Name
------------------------------------------------------------
	CPE		 Chaitanya Panduga Eshwar


	Initials	Date			Modification
------------------------------------------------------------
	CPE			2012-07-31		Created

******/
CREATE PROCEDURE dbo.Validate_TOU_Schedule_Term_Dates
( 
 @Time_Of_Use_Schedule_Id INT
,@Time_Of_Use_Schedule_Term_Id INT
,@Start_Dt DATETIME
,@End_Dt DATETIME = NULL )
AS 
BEGIN
      SET NOCOUNT ON
	
      SELECT
            @End_Dt = ISNULL(@End_Dt, '2099-12-31')
	
      SELECT
            TOUST.Start_Dt
      FROM
            dbo.Time_Of_Use_Schedule_Term TOUST
      WHERE
            TOUST.Time_Of_Use_Schedule_Id = @Time_Of_Use_Schedule_Id
            AND TOUST.Time_Of_Use_Schedule_Term_Id <> @Time_Of_Use_Schedule_Term_Id
            AND ( ( TOUST.End_Dt <> '2099-12-31'
                    AND ( @Start_Dt BETWEEN TOUST.Start_Dt AND TOUST.End_Dt
                          OR @End_Dt BETWEEN TOUST.Start_Dt AND TOUST.End_Dt
                          OR ( @Start_Dt <= TOUST.Start_Dt
                               AND @End_Dt >= TOUST.End_Dt ) ) )
                  OR ( TOUST.End_Dt = '2099-12-31'
                       AND ( TOUST.Start_Dt BETWEEN @Start_Dt AND @End_Dt ) ) )
END


;
GO
GRANT EXECUTE ON  [dbo].[Validate_TOU_Schedule_Term_Dates] TO [CBMSApplication]
GO
