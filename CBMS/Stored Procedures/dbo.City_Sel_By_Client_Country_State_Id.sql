SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
                         
 NAME: dbo.City_Sel_By_Client_Country_State_Id                     
                          
 DESCRIPTION:      
		Get the     cities.                
                          
 INPUT PARAMETERS:      
                         
 Name                               DataType          Default       Description      
---------------------------------------------------------------------------------------------------------------    
 @Sup_Account_Id			INT        
                          
 OUTPUT PARAMETERS:      
                               
 Name                               DataType          Default       Description      
---------------------------------------------------------------------------------------------------------------    
                          
 USAGE EXAMPLES:                              
---------------------------------------------------------------------------------------------------------------      
               
EXEC dbo.City_Sel_By_Client_Country_State_Id @Account_Id = 1628

                         
 AUTHOR INITIALS:    
       
 Initials                   Name      
---------------------------------------------------------------------------------------------------------------    
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
---------------------------------------------------------------------------------------------------------------    
 NR                     2019-06-13      Created for ADD Contract.                        
                         
******/
CREATE PROCEDURE [dbo].[City_Sel_By_Client_Country_State_Id]
    (
        @Account_Id INT = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;


        SELECT
            ch.Client_Id
            , ch.Client_Name
            , ch.Site_Id
            , ch.Site_name
            , cha.Account_Number
            , cha.Account_Vendor_Name
            , ch.Country_Id
            , ch.Country_Name
            , ch.State_Name
            , ch.State_Id
            , ch.City
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Client_Hier_Id = ch.Client_Hier_Id
        WHERE
            cha.Account_Id = @Account_Id
        GROUP BY
            ch.Client_Id
            , ch.Client_Name
            , ch.Site_Id
            , ch.Site_name
            , cha.Account_Number
            , cha.Account_Vendor_Name
            , ch.Country_Id
            , ch.Country_Name
            , ch.State_Name
            , ch.State_Id
            , ch.City

    END;



GO
GRANT EXECUTE ON  [dbo].[City_Sel_By_Client_Country_State_Id] TO [CBMSApplication]
GO
