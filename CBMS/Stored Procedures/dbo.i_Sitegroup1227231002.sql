SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[i_Sitegroup1227231002]
		@c1 int,
		@c2 varchar(200),
		@c3 int,
		@c4 int,
		@c5 bit,
		@c6 bit,
		@c7 int,
		@c8 int,
		@c9 datetime,
		@c10 int,
		@c11 datetime,
		@c12 bit,
		@c13 bit,
		@c14 bit,
		@c15 datetime,
		@c16 int
	,@MSp2pPostVersion varbinary(32) 
as
begin  
begin try
	insert into [dbo].[Sitegroup](
		[Sitegroup_Id],
		[Sitegroup_Name],
		[Sitegroup_Type_Cd],
		[Client_Id],
		[Is_Smart_Group],
		[Is_Publish_To_DV],
		[Owner_User_Id],
		[Add_User_Id],
		[Add_Dt],
		[Mod_User_Id],
		[Mod_Dt],
		[Is_Complete],
		[Is_Locked],
		[Is_Edited],
		[Last_Smartgroup_Refresh_dt],
		[Portfolio_Client_Hier_Reference_Number],
		$sys_p2p_cd_id
	) values (
    @c1,
    @c2,
    @c3,
    @c4,
    @c5,
    @c6,
    @c7,
    @c8,
    @c9,
    @c10,
    @c11,
    @c12,
    @c13,
    @c14,
    @c15,
    @c16,
		@MSp2pPostVersion	) 
end try
begin catch
if @@error in (2627, 2601) 
begin  
	declare @cur_version varbinary(32) 
		,@conflict_type int = 5
		,@conflict_type_txt nvarchar(20) = N'Insert-Insert'
		,@reason_code int = 1
		,@reason_text nvarchar(720) = NULL
		,@is_on_disk_winner bit = 0
		,@is_incoming_winner bit = 0
		,@peer_id_current_node int = NULL
		,@peer_id_incoming int
		,@tranid_incoming nvarchar(40)
		,@peer_id_on_disk int
		,@tranid_on_disk nvarchar(40)
	select @peer_id_incoming = sys.fn_replvarbintoint(@MSp2pPostVersion)
		,@tranid_incoming = sys.fn_replp2pversiontotranid(@MSp2pPostVersion)
	select @cur_version = $sys_p2p_cd_id from [dbo].[Sitegroup] 
where [Sitegroup_Id] = @c1
	if @@rowcount = 0  
			exec sys.sp_replrethrow
	else 
	begin  
		select @peer_id_on_disk = sys.fn_replvarbintoint(@cur_version)
			,@tranid_on_disk = sys.fn_replp2pversiontotranid(@cur_version)
		if(@peer_id_incoming > @peer_id_on_disk)
			set @is_incoming_winner = 1
		else
			set @is_on_disk_winner = 1
	end  
		select @peer_id_current_node = p.originator_id from syspublications p join sysarticles a on a.pubid = p.pubid where a.objid = object_id(N'[dbo].[Sitegroup]') and p.options & 0x1 = 0x1
	if (@peer_id_current_node is not null) 
	begin 
		if (@reason_text is NULL)
			if (@peer_id_incoming > @peer_id_on_disk)
			begin  
				select @reason_text = formatmessage(22825,@peer_id_incoming,@peer_id_on_disk,@peer_id_current_node)
			end  
			else  
			begin  
				select @reason_text = formatmessage(22824,@peer_id_incoming,@peer_id_on_disk,@peer_id_current_node)
			end  
		create table #change_id (change_id varbinary(8))
		insert [dbo].[conflict_dbo_Sitegroup] (
		[Sitegroup_Id],
		[Sitegroup_Name],
		[Sitegroup_Type_Cd],
		[Client_Id],
		[Is_Smart_Group],
		[Is_Publish_To_DV],
		[Owner_User_Id],
		[Add_User_Id],
		[Add_Dt],
		[Mod_User_Id],
		[Mod_Dt],
		[Is_Complete],
		[Is_Locked],
		[Is_Edited],
		[Last_Smartgroup_Refresh_dt],
		[Portfolio_Client_Hier_Reference_Number]
			,__$originator_id
			,__$origin_datasource
			,__$tranid
			,__$conflict_type
			,__$is_winner
			,__$reason_code
			,__$reason_text
			,__$update_bitmap
			,__$pre_version)
		output inserted.__$row_id into #change_id
		select 
    @c1,
    @c2,
    @c3,
    @c4,
    @c5,
    @c6,
    @c7,
    @c8,
    @c9,
    @c10,
    @c11,
    @c12,
    @c13,
    @c14,
    @c15,
    @c16			,@peer_id_current_node
			,@peer_id_incoming
			,@tranid_incoming
			,@conflict_type
			,@is_incoming_winner
			,@reason_code
			,@reason_text
			,NULL
			,NULL
		insert [dbo].[conflict_dbo_Sitegroup] (

		[Sitegroup_Id],
		[Sitegroup_Name],
		[Sitegroup_Type_Cd],
		[Client_Id],
		[Is_Smart_Group],
		[Is_Publish_To_DV],
		[Owner_User_Id],
		[Add_User_Id],
		[Add_Dt],
		[Mod_User_Id],
		[Mod_Dt],
		[Is_Complete],
		[Is_Locked],
		[Is_Edited],
		[Last_Smartgroup_Refresh_dt],
		[Portfolio_Client_Hier_Reference_Number]			,__$originator_id
			,__$origin_datasource
			,__$tranid
			,__$conflict_type
			,__$is_winner
			,__$reason_code
			,__$reason_text
			,__$pre_version
			,__$change_id)
		select 

		[Sitegroup_Id],
		[Sitegroup_Name],
		[Sitegroup_Type_Cd],
		[Client_Id],
		[Is_Smart_Group],
		[Is_Publish_To_DV],
		[Owner_User_Id],
		[Add_User_Id],
		[Add_Dt],
		[Mod_User_Id],
		[Mod_Dt],
		[Is_Complete],
		[Is_Locked],
		[Is_Edited],
		[Last_Smartgroup_Refresh_dt],
		[Portfolio_Client_Hier_Reference_Number]			,@peer_id_current_node
			,@peer_id_on_disk
			,@tranid_on_disk
			,@conflict_type
			,@is_on_disk_winner
			,@reason_code
			,@reason_text
			,NULL
			,(select change_id from #change_id)
		from [dbo].[Sitegroup] 

where [Sitegroup_Id] = @c1
	end 
	if(@peer_id_incoming > @peer_id_on_disk)
	begin  
update [dbo].[Sitegroup] set
		[Sitegroup_Name] = @c2,
		[Sitegroup_Type_Cd] = @c3,
		[Client_Id] = @c4,
		[Is_Smart_Group] = @c5,
		[Is_Publish_To_DV] = @c6,
		[Owner_User_Id] = @c7,
		[Add_User_Id] = @c8,
		[Add_Dt] = @c9,
		[Mod_User_Id] = @c10,
		[Mod_Dt] = @c11,
		[Is_Complete] = @c12,
		[Is_Locked] = @c13,
		[Is_Edited] = @c14,
		[Last_Smartgroup_Refresh_dt] = @c15,
		[Portfolio_Client_Hier_Reference_Number] = @c16		,$sys_p2p_cd_id = @MSp2pPostVersion

where [Sitegroup_Id] = @c1
	end  
		if exists(select * from syspublications p join sysarticles a on a.pubid = p.pubid where a.objid = object_id(N'[dbo].[Sitegroup]') and p.options & 0x10 = 0x10)
			raiserror(22815, 10, -1, @conflict_type_txt, @peer_id_current_node, @peer_id_incoming, @tranid_incoming, @peer_id_on_disk, @tranid_on_disk) with log
		else
			raiserror(22815, 16, -1, @conflict_type_txt, @peer_id_current_node, @peer_id_incoming, @tranid_incoming, @peer_id_on_disk, @tranid_on_disk) with log
end 
else  
	EXEC sys.sp_replrethrow
end catch 
end  
GO
