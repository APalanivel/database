SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: dbo.[Account_History_Sel_By_Account_Id]  
     
DESCRIPTION:

	To select all the account history information for the given account id.
	Instead of calling individual procedures application will call this procedure to get all the informations required for the page at once.

INPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
@Account_Id		INT						
@StartIndex		INT			1
@EndIndex		INT			2147483647

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------  

	EXEC dbo.Account_History_Sel_By_Account_Id  30008
	
	EXEC dbo.Account_History_Sel_By_Account_Id  34970, 1,25
	EXEC dbo.Account_History_Sel_By_Account_Id  1269921, 1,25
	EXEC dbo.Account_History_Sel_By_Account_Id  36126, 1,25

AUTHOR INITIALS:
INITIALS	NAME          
------------------------------------------------------------          
HG			Harihara Suthan G

MODIFICATIONS
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------          
	HG			03-JUN-10	CREATED
	RR			2017-02-17	Contract placeholder - CP-54 Added script dbo.DMO_Supplier_Account_Meter_Dtls_Sel_By_Utility_Account to 
							get asscoiated DMO supplier accounts
	RR			2019-05-28	GRM -  Modified to get deal tickets associated to account
*/

CREATE PROCEDURE [dbo].[Account_History_Sel_By_Account_Id]
      ( 
       @Account_Id INT
      ,@StartIndex INT = 1
      ,@EndIndex INT = 2147483647 )
AS 
BEGIN

      SET NOCOUNT ON;

      EXEC dbo.Account_Information_Sel_By_Account_Id 
            @Account_Id
      EXEC dbo.Meter_Information_Sel_By_Account_Id 
            @Account_Id
           ,@StartIndex
           ,@EndIndex
      EXEC dbo.CU_Invoice_Dtl_Sel_By_Account_Id 
            @Account_Id
           ,@StartIndex
           ,@EndIndex
      EXEC dbo.Cost_Usage_Service_Month_Sel_By_Account_Id 
            @Account_Id
           ,@StartIndex
           ,@EndIndex
      EXEC dbo.Sr_Rfp_Id_Sel_By_Account_Id 
            @Account_Id
           ,@StartIndex
           ,@EndIndex
      EXEC dbo.Rate_Comparison_Sel_By_Account_Id 
            @Account_Id
           ,@StartIndex
           ,@EndIndex
      EXEC dbo.Budget_Id_Sel_By_Account_Id 
            @Account_Id
           ,@StartIndex
           ,@EndIndex
      EXEC dbo.Contract_Sel_By_Account_Id 
            @Account_Id
           ,@StartIndex
           ,@EndIndex
      EXEC dbo.Meter_Tax_Exemption_Document_Dtl_Sel_By_Account_Id 
            @Account_Id
           ,@StartIndex
           ,@EndIndex
      EXEC dbo.Account_Group_Sel_By_Account_Id 
            @Account_Id
      EXEC dbo.Audit_Log_Sel_By_Account_Id 
            @Account_Id
           ,@StartIndex
           ,@EndIndex
      EXEC dbo.DMO_Supplier_Account_Meter_Dtls_Sel_By_Utility_Account 
            @Account_Id
           ,@StartIndex
           ,@EndIndex
      EXEC dbo.Invoice_Collection_Period_Sel_By_Account_Id 
            @Account_Id
           ,@StartIndex
           ,@EndIndex
           
      EXEC Trade.Deal_Ticket_Sel_By_Account 
            @Account_Id
           ,@StartIndex
           ,@EndIndex

END;

GO

GRANT EXECUTE ON  [dbo].[Account_History_Sel_By_Account_Id] TO [CBMSApplication]
GO
