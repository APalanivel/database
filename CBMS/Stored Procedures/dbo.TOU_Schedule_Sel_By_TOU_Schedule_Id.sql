SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********
NAME:  dbo.TOU_Schedule_Sel_By_TOU_Schedule_Id

DESCRIPTION:  

Used to select TOU Schedule details by given Time_Of_Use_Schedule_Id

INPUT PARAMETERS:
      Name								DataType          Default     Description
------------------------------------------------------------
	@Time_Of_Use_Schedule_Id			INT
				

OUTPUT PARAMETERS:
      Name								DataType          Default     Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------

	Exec dbo.TOU_Schedule_Sel_By_TOU_Schedule_Id 14


AUTHOR INITIALS:
	Initials Name
------------------------------------------------------------
	BCH		 Balaraju


	Initials Date		 Modification
------------------------------------------------------------
	BCH		 2012-07-06  Created

******/
CREATE PROCEDURE dbo.TOU_Schedule_Sel_By_TOU_Schedule_Id
    (
      @Time_Of_Use_Schedule_Id INT 
    )
AS 
    BEGIN

        SET NOCOUNT ON ;

        SELECT  TOUS.Time_Of_Use_Schedule_Id ,
                TOUS.Vendor_Id ,
                VEN.VENDOR_NAME ,
                TOUS.Schedule_Name ,
                s.COUNTRY_ID ,
                s.STATE_ID ,
                s.STATE_NAME ,
                COM.Commodity_Id ,
                COM.Commodity_Name ,
                COMT.Comment_Id ,
                COMT.Comment_Text
        FROM    dbo.Time_Of_Use_Schedule TOUS
                JOIN dbo.VENDOR VEN ON VEN.VENDOR_ID = TOUS.Vendor_Id
                JOIN dbo.Commodity COM ON COM.Commodity_Id = TOUS.Commodity_Id
                LEFT JOIN ( dbo.VENDOR_STATE_MAP vsm
                            JOIN dbo.STATE s ON vsm.STATE_ID = s.STATE_ID
                          ) ON vsm.VENDOR_ID = ven.VENDOR_ID
                LEFT JOIN dbo.Comment COMT ON TOUS.Comment_ID = COMT.Comment_ID
        WHERE   TOUS.Time_Of_Use_Schedule_Id = @Time_Of_Use_Schedule_Id
    END 

;
GO
GRANT EXECUTE ON  [dbo].[TOU_Schedule_Sel_By_TOU_Schedule_Id] TO [CBMSApplication]
GO
