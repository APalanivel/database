SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:            
[Budget].[Budget_Master_Sel_User_Info_Id]           
            
DESCRIPTION:            
   User is move to history to know the user is assigned with any of the budget as default queue assignee.            
            
INPUT PARAMETERS:            
 Name       DataType  Default     Description            
---------------------------------------------------------------------------------------------            
 @User_Info_Id     INT            
               
            
OUTPUT PARAMETERS:            
 Name       DataType  Default     Description            
---------------------------------------------------------------------------------------------            
             
USAGE EXAMPLES:            
---------------------------------------------------------------------------------------------            
          
EXEC [Budget].[Budget_Master_Sel_User_Info_Id]           
    @User_Info_Id =34404          
            
AUTHOR INITIALS:            
 Initials Name            
---------------------------------------------------------------------------------------------                  
 SC   Sreenivasulu Cheerala            
  
MODIFICATIONS            
 Initials Date   Modification            
---------------------------------------------------------------------------------------------            
SC   2020-04-02   added Budget 2.0 exception list in the result set.        
******/  
CREATE PROCEDURE [Budget].[Budget_Master_Sel_User_Info_Id]  
     (  
         @User_Info_Id INT  
     )  
AS  
    BEGIN  
        SET NOCOUNT ON;  
  
        SELECT  
            bm.Budget_Master_Id Budget_Id  
        FROM  
            Budget.Budget_Master bm  
            INNER JOIN dbo.USER_INFO ui  
                ON ui.USER_INFO_ID = bm.Default_Queue_Assignee  
        WHERE  
            bm.Default_Queue_Assignee = @User_Info_Id;  
    END;  
  

  
GO
GRANT EXECUTE ON  [Budget].[Budget_Master_Sel_User_Info_Id] TO [CBMSApplication]
GO
