
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	dbo.SR_RFP_SAVE_TERM_P

DESCRIPTION: 


INPUT PARAMETERS:    
	Name                        DataType        Default     Description    
---------------------------------------------------------------------------------    
	@rfp_id						INT
    @term_id					INT
    @account_group_id			INT
    @is_bid_group				BIT
    @from_month					DATETIME
    @to_month					DATETIME
    @no_of_months				INT
    @Is_Term_Date_Specific		BIT				0
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
BEGIN TRANSACTION
	SELECT * FROM dbo.SR_RFP a WHERE a.SR_RFP_ID = 13132
	SELECT * FROM dbo.SR_RFP_ACCOUNT_TERM a WHERE a.SR_RFP_TERM_ID = 1215079
	EXEC dbo.SR_RFP_SAVE_TERM_P 13132,1215079,10011865,1,'2018-01-01 00:00:00.000','2018-06-01 00:00:00.000',6,0
	SELECT * FROM dbo.SR_RFP a WHERE a.SR_RFP_ID = 13132
	SELECT * FROM dbo.SR_RFP_ACCOUNT_TERM a WHERE a.SR_RFP_TERM_ID = 1215079
ROLLBACK TRANSACTION

BEGIN TRANSACTION
	SELECT * FROM dbo.SR_RFP a WHERE a.SR_RFP_ID = 13132
	SELECT * FROM dbo.SR_RFP_ACCOUNT_TERM a WHERE a.SR_RFP_TERM_ID = 1215079
	EXEC dbo.SR_RFP_SAVE_TERM_P 13132,1215079,10011865,1,'2018-01-01 00:00:00.000','2018-06-01 00:00:00.000',6,1
	SELECT * FROM dbo.SR_RFP a WHERE a.SR_RFP_ID = 13132
	SELECT * FROM dbo.SR_RFP_ACCOUNT_TERM a WHERE a.SR_RFP_TERM_ID = 1215079
ROLLBACK TRANSACTION


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DR			Deana Ritter
	RR			Raghu Reddy

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	DR			08/04/2009	Removed Linked Server Updates
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2016-02-11	Global Sourcing - Phase3 - GCS-469 Added new input parameter Is_Term_Date_Specific


******/
CREATE PROCEDURE [dbo].[SR_RFP_SAVE_TERM_P]
      ( 
       @rfp_id INT
      ,@term_id INT
      ,@account_group_id INT
      ,@is_bid_group BIT
      ,@from_month DATETIME
      ,@to_month DATETIME
      ,@no_of_months INT
      ,@Is_Term_Date_Specific BIT = 0 )
AS 
BEGIN
      SET NOCOUNT ON;
      DECLARE @sr_account_group_id INT 
      DECLARE @sr_rfp_account_term_id INT 
		
	
      IF @is_bid_group = 1 
            BEGIN
                  SET @sr_account_group_id = @account_group_id
            END
      ELSE 
            BEGIN
                  SELECT
                        @sr_account_group_id = sr_rfp_account_id
                  FROM
                        dbo.SR_RFP_ACCOUNT
                  WHERE
                        account_id = @account_group_id
                        AND sr_rfp_id = @rfp_id
                        AND sr_rfp_bid_group_id IS NULL
                        AND is_deleted = 0
            END
            
            
      UPDATE
            dbo.SR_RFP
      SET   
            Is_Term_Date_Specific = @Is_Term_Date_Specific
      WHERE
            SR_RFP_ID = @rfp_id
            AND Is_Term_Date_Specific = 0  --> Once set to true should not be changed

      IF ( SELECT
            count(sr_rfp_account_term_id)
           FROM
            dbo.SR_RFP_ACCOUNT_TERM
           WHERE
            sr_rfp_term_id = @term_id
            AND sr_account_group_id = @sr_account_group_id
            AND is_bid_group = @is_bid_group ) > 0 
            BEGIN
                  UPDATE
                        sr_rfp_account_term
                  SET   
                        from_month = @from_month
                       ,to_month = @to_month
                       ,no_of_months = @no_of_months
                  WHERE
                        sr_rfp_term_id = @term_id
                        AND sr_account_group_id = @sr_account_group_id
                        AND is_bid_group = @is_bid_group
			
            END
      ELSE 
            BEGIN
                  INSERT      INTO dbo.SR_RFP_ACCOUNT_TERM
                              ( 
                               sr_rfp_term_id
                              ,sr_account_group_id
                              ,is_bid_group
                              ,from_month
                              ,to_month
                              ,no_of_months )
                  VALUES
                              ( 
                               @term_id
                              ,@sr_account_group_id
                              ,@is_bid_group
                              ,@from_month
                              ,@to_month
                              ,@no_of_months )	
			
                  SELECT
                        @sr_rfp_account_term_id = scope_identity()
            END
END
;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_SAVE_TERM_P] TO [CBMSApplication]
GO
