SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.Vendor_Commodity_Analyst_Map_Upd

DESCRIPTION:

	Used to update the vendor_commodity_Analyst_Map table for the given Vendor_Commodity_Analyst_map_id
	
INPUT PARAMETERS:
	Name							DataType		Default	Description
------------------------------------------------------------
	@Vendor_Commodity_Analyst_Map_Id INT
	@Analyst_Id						 INT 
	@Vendor_Commodity_Map_Id		 INT 	 

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Vendor_Commodity_Analyst_Map_Upd 2154,49,4539
	
	SELECT * FROM Vendor_Commodity_Analyst_Map
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SSR			Sharad Srivastava
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	SSR	       	02/10/2010	Created
******/
CREATE PROCEDURE dbo.Vendor_Commodity_Analyst_Map_Upd
	@Vendor_Commodity_Analyst_Map_Id INT
   ,@Analyst_Id						 INT 
   ,@Vendor_Commodity_Map_Id		 INT 	 
	
AS
BEGIN
	
	SET NOCOUNT ON

	UPDATE
		dbo.Vendor_Commodity_Analyst_Map
		SET Analyst_Id = @Analyst_Id
		   ,Vendor_Commodity_Map_Id = @Vendor_Commodity_Map_Id
	WHERE
		Vendor_Commodity_Analyst_Map_Id = @Vendor_Commodity_Analyst_Map_Id
		
END
GO
GRANT EXECUTE ON  [dbo].[Vendor_Commodity_Analyst_Map_Upd] TO [CBMSApplication]
GO
