SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********   
NAME:  dbo.Account_Variance_Consumption_Level_Rules_UPD  
 
DESCRIPTION:  Used to update account variance consumption level into variance_rule_dtl_account_override table when utility/supplier accounts modified

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
@Old_Variance_Consumption_Level_Id  int
@New_Variance_Consumption_Level_Id  int
@Account_id						int
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:  
	
	EXEC dbo.Account_Variance_Consumption_Level_Rules_UPD 7,8,12345
	EXEC dbo.Account_Variance_Consumption_Level_Rules_UPD 13,13,1095348

------------------------------------------------------------  
AUTHOR INITIALS:  
	Initials	Name  
------------------------------------------------------------  
	NK			Nageswara Rao Kosuri 
	RR			Raghu Reddy  
	AP			Arunkumar Palanivel      


	Initials	Date		Modification  
------------------------------------------------------------  
	NK			11/11/2009  Created
	NK			01/07/2009  Removed Join Account_Variance_Consumption_Level when inserting into variance_rule_dtl_account_override table
	NK			01/08/2009	Added Account_id filter in where cluase of Delete statement
							and added Is_Active = 1 filter in where clause of Insert Select statement
	NK			02/16/2011	Added Account_Deo filter in the INSERT statement to fix #22221
	RR			11-29-2016	VTE-58/50 Modified to insert all tests if Is_Data_Entry_Only is false otherwise loads only DEO tests 
	Ap			feb 24,2020	Modified procedure to exclude variance test at account lebel for AVT
******/

CREATE PROCEDURE [dbo].[Account_Variance_Consumption_Level_Rules_UPD]
      @Old_Variance_Consumption_Level_Id INT
     ,@New_Variance_Consumption_Level_Id INT
     ,@Account_id INT
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Account_Deo BIT

      DELETE FROM
            dbo.variance_rule_dtl_account_override
      WHERE
            variance_rule_dtl_id IN ( SELECT
                                          vrdtl.variance_rule_dtl_id
                                      FROM
                                          dbo.variance_rule_dtl vrdtl
                                          JOIN dbo.Account_Variance_Consumption_Level avcl
                                                ON avcl.Variance_Consumption_Level_Id = vrdtl.Variance_Consumption_Level_Id
                                      WHERE
                                          avcl.variance_consumption_level_id = @Old_Variance_Consumption_Level_Id
                                          AND avcl.account_id = @Account_id )
            AND account_id = @Account_id
	
      SELECT
            @Account_Deo = isnull(Is_Data_Entry_only, 0)
      FROM
            dbo.Account
      WHERE
            Account_Id = @Account_id

      INSERT      INTO dbo.Variance_Rule_Dtl_Account_Override
                  ( 
                   Variance_Rule_Dtl_Id
                  ,ACCOUNT_ID )
                  SELECT
                        vrdtl.variance_rule_dtl_id
                       ,@Account_id
                  FROM
                        dbo.Variance_rule_dtl vrdtl
                        INNER JOIN dbo.Account acc
                              ON acc.Account_Id = @Account_id
                  WHERE
                        vrdtl.Variance_Consumption_Level_Id = @New_Variance_Consumption_Level_Id
                        AND ( @Account_Deo = 0
                              OR ( @Account_Deo = 1
                                   AND vrdtl.Is_Data_Entry_Only = 1 ) )
                        AND vrdtl.Is_Active = 1
						   AND   NOT EXISTS
                                                            (     SELECT
                                                                        1
                                                                  FROM  Core.Client_Hier_Account cha
                                                                        JOIN
                                                                        dbo.Account_Variance_Consumption_Level avcl1
                                                                              ON avcl1.ACCOUNT_ID = cha.Account_Id
                                                                        JOIN
                                                                        dbo.Variance_Consumption_Level vcl
                                                                              ON vcl.Variance_Consumption_Level_Id = avcl1.Variance_Consumption_Level_Id
                                                                                 AND vcl.Commodity_Id = cha.Commodity_Id
                                                                        JOIN
                                                                        dbo.Bucket_Master bm
                                                                              ON bm.Commodity_Id = vcl.Commodity_Id
                                                                        JOIN
                                                                        dbo.Variance_category_Bucket_Map vcbm
                                                                              ON vcbm.Bucket_master_Id = bm.Bucket_Master_Id
                                                                        JOIN
                                                                        dbo.Variance_Parameter vp
                                                                              ON vp.Variance_Category_Cd = vcbm.Variance_Category_Cd
                                                                        JOIN
                                                                        dbo.Variance_Parameter_Baseline_Map vpbm
                                                                              ON vpbm.Variance_Parameter_Id = vp.Variance_Parameter_Id
                                                                        JOIN
                                                                        dbo.Variance_Rule vr
                                                                              ON vr.Variance_Baseline_Id = vpbm.Variance_Baseline_Id
                                                                        JOIN
                                                                        dbo.Variance_Rule_Dtl vrd1
                                                                              ON vrd1.Variance_Rule_Id = vr.Variance_Rule_Id
                                                                        JOIN
                                                                        dbo.Variance_Parameter_Commodity_Map vpcm
                                                                              ON vpcm.Variance_Parameter_Id = vp.Variance_Parameter_Id
                                                                        JOIN
                                                                        dbo.Variance_Processing_Engine vpe
                                                                              ON vpe.Variance_Processing_Engine_Id = vpcm.variance_process_Engine_id
                                                                        JOIN
                                                                        dbo.Code vpcat
                                                                              ON vpcat.Code_Id = vpe.Variance_Engine_Cd
                                                                        JOIN
                                                                        dbo.Variance_Consumption_Extraction_Service_Param_Value vesp
                                                                              ON vesp.Variance_Consumption_Level_Id = vcl.Variance_Consumption_Level_Id
                                                                  WHERE cha.Account_Id = @Account_id
                                                                        AND   cha.Account_Type = 'Utility'
                                                                        AND   vpcat.Code_Value = 'Advanced Variance Test'
                                                                        AND   vrdtl.Variance_Rule_Dtl_Id = vrd1.Variance_Rule_Dtl_Id
																		AND vcl.Variance_Consumption_Level_Id = @New_Variance_Consumption_Level_Id );

END

;
GO

GRANT EXECUTE ON  [dbo].[Account_Variance_Consumption_Level_Rules_UPD] TO [CBMSApplication]
GO
