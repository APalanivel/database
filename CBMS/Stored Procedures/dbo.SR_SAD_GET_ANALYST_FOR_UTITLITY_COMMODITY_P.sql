
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_ANALYST_FOR_UTITLITY_COMMODITY_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default			Description
-------------------------------------------------------------------------
	@sessionId     	int       	          	
	@userId        	int       	          	
	@utilityId     	int       	          	
	@commodityId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default			Description
--------------------------------------------------------------------------

USAGE EXAMPLES:
--------------------------------------------------------------------------

EXEC dbo.SR_SAD_GET_ANALYST_FOR_UTITLITY_COMMODITY_P -1,-1,40,290

EXEC dbo.SR_SAD_GET_ANALYST_FOR_UTITLITY_COMMODITY_P -1,-1,57,291

AUTHOR INITIALS:
	Initials		Name
--------------------------------------------------------------------------
	NR				Narayana Reddy

MODIFICATIONS

	Initials	Date		Modification
--------------------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR			09/10/2010	Modified for Quoted_Identifier
 NR				2015-09-14	MAINT-3067 Removed legacy view Utility-Analyst-Map(read "-" as "_")

******/
CREATE PROCEDURE [dbo].[SR_SAD_GET_ANALYST_FOR_UTITLITY_COMMODITY_P]
      ( 
       @sessionId INT
      ,@userId INT
      ,@utilityId INT
      ,@commodityId INT )
AS 
SET NOCOUNT ON
BEGIN
            
      SELECT
            ui.user_info_id
           ,ui.FIRST_NAME + ' ' + ui.LAST_NAME USER_INFO_NAME
      FROM
            dbo.UTILITY_DETAIL ud
            INNER JOIN dbo.VENDOR_COMMODITY_MAP vcm
                  ON ud.VENDOR_ID = vcm.VENDOR_ID
            INNER JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                  ON vcm.VENDOR_COMMODITY_MAP_ID = vcam.Vendor_Commodity_Map_Id
            INNER JOIN dbo.USER_INFO ui
                  ON ui.USER_INFO_ID = vcam.Analyst_Id
      WHERE
            ud.VENDOR_ID = @utilityId
            AND vcm.COMMODITY_TYPE_ID = @commodityId

END

;
GO

GRANT EXECUTE ON  [dbo].[SR_SAD_GET_ANALYST_FOR_UTITLITY_COMMODITY_P] TO [CBMSApplication]
GO
