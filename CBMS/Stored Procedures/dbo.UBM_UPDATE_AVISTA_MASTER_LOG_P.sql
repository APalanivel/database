
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.UBM_UPDATE_AVISTA_MASTER_LOG_P

DESCRIPTION:


INPUT PARAMETERS:
	Name						DataType		Default	Description
------------------------------------------------------------
	 @status					VARCHAR(200)
     @expectedData				INT
     @actualData				INT
     @expectedImages			INT
     @actualImages				INT
     @expectedAmount			DECIMAL
     @actualAmount				DECIMAL
     @masterLogId				INT
     @Batch_Failure_Reason		VARCHAR(500)	NULL
     @Detail__Failure_Reason	VARCHAR(MAX)	NULL      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
RKV				Ravi Kumar Vegesna


MODIFICATIONS

Initials	Date		Modification
------------------------------------------------------------
RKV			2015-09-18   Comments added and added two input parameteres as part of Ecova Changes
 
******/
CREATE  PROCEDURE [dbo].[UBM_UPDATE_AVISTA_MASTER_LOG_P]
      @status VARCHAR(200)
     ,@expectedData INT
     ,@actualData INT
     ,@expectedImages INT
     ,@actualImages INT
     ,@expectedAmount DECIMAL
     ,@actualAmount DECIMAL
     ,@masterLogId INT
     ,@Batch_Failure_Reason VARCHAR(500) = NULL
     ,@Detail_Failure_Reason VARCHAR(MAX) = NULL
AS 
SET nocount ON
DECLARE @statusTypeId INT

SELECT
      @statusTypeId = ( SELECT
                              entity_id
                        FROM
                              entity
                        WHERE
                              entity_name = @status
                              AND entity_type = 656 )

UPDATE
      ubm_batch_master_log
SET   
      status_type_id = @statusTypeId
     ,end_date = GETDATE()
     ,expected_data_records = @expectedData
     ,actual_data_records = @actualData
     ,expected_image_records = @expectedImages
     ,actual_image_records = @actualImages
     ,expected_dollar_amount = @expectedAmount
     ,actual_dollar_amount = @actualAmount
     ,Batch_Failure_Reason = CASE WHEN @Batch_Failure_Reason IS NULL THEN Batch_Failure_Reason
                                  ELSE @Batch_Failure_Reason
                             END
     ,Detail_Failure_Reason = CASE WHEN @Detail_Failure_Reason IS NULL THEN Detail_Failure_Reason
                                   ELSE @Detail_Failure_Reason
                              END
WHERE
      ubm_batch_master_log_id = @masterLogId
;
GO

GRANT EXECUTE ON  [dbo].[UBM_UPDATE_AVISTA_MASTER_LOG_P] TO [CBMSApplication]
GO
