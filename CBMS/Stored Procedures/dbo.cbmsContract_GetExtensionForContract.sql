SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE  PROCEDURE [dbo].[cbmsContract_GetExtensionForContract]
	( @MyAccountId int
	, @contract_id int
	)
AS
BEGIN

	   select c.contract_id
		, c.ed_contract_number
	     from contract c
	    where c.base_contract_id = @contract_id 

END




GO
GRANT EXECUTE ON  [dbo].[cbmsContract_GetExtensionForContract] TO [CBMSApplication]
GO
