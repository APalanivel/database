SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE dbo.BUDGET_CREATE_REVISED_BUDGET_DETAILS_COMMENTS_OWNER_P
@user_id varchar(10),
@session_id varchar(20),
@budgetId int,
@revisedBudgetId int 



AS
begin
set nocount on



	insert into budget_detail_comments_owner_map(	
				BUDGET_DETAIL_COMMENTS,
				BUDGET_ID,
				BUDGET_DETAIL_ID,
				OWNER_ID,
				OWNER_TYPE_ID)

      			select 	BUDGET_DETAIL_COMMENTS,
				@revisedBudgetId,
				BUDGET_DETAIL_ID,
				OWNER_ID,
				OWNER_TYPE_ID
               		 from  	budget_detail_comments_owner_map
              	 	 where 	budget_id=@budgetId
	

	
				
end
GO
GRANT EXECUTE ON  [dbo].[BUDGET_CREATE_REVISED_BUDGET_DETAILS_COMMENTS_OWNER_P] TO [CBMSApplication]
GO
