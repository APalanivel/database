SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.Default_UOM_Sel_By_Account_Id

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@intAccount_ID   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
EXEC Default_UOM_Sel_By_Meter_Id @intMeter_ID = 84048


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PRG			Prasanna R Gachinmani
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  PRG      	17/04/2019	Initial Development
******/

CREATE  PROC [dbo].[Default_UOM_Sel_By_Meter_Id]
     (
         @intMeter_ID INT

     )
AS
    BEGIN

        DECLARE @Commodity_Id INT;
        DECLARE @Uom_Names TABLE
              (
                  Uom_Id INT
                  , Uom_Name VARCHAR(200)
              );

        SELECT
            @Commodity_Id = cha.Commodity_Id
        FROM
            Core.Client_Hier_Account AS cha
        WHERE
            cha.Meter_Id = @intMeter_ID;


        INSERT INTO @Uom_Names
             (
                 Uom_Id,Uom_Name
             )
        SELECT
            e.ENTITY_ID
            , e.ENTITY_NAME AS Uom_Name
        FROM
            dbo.Bucket_Master_UOM_Map bmum
            INNER JOIN dbo.Bucket_Master bm
                ON bmum.Bucket_Master_Id = bm.Bucket_Master_Id
            INNER JOIN dbo.ENTITY e
                ON e.ENTITY_ID = bmum.Uom_Type_Id
        WHERE
            bm.Commodity_Id = @Commodity_Id
            AND bm.Bucket_Name NOT IN ( 'Total Usage', 'Volume' )
        GROUP BY
            e.ENTITY_ID
            , e.ENTITY_NAME;
        INSERT INTO @Uom_Names
             (
                 Uom_Id,Uom_Name
             )
        SELECT
            e.ENTITY_ID
            , e.ENTITY_NAME AS Uom_Name
        FROM
            dbo.Commodity c
            INNER JOIN dbo.Bucket_Master bm
                ON c.Commodity_Id = bm.Commodity_Id
            INNER JOIN dbo.ENTITY e
                ON e.ENTITY_TYPE = c.UOM_Entity_Type
        WHERE
            bm.Commodity_Id = @Commodity_Id
            AND bm.Bucket_Name IN ( 'Total Usage', 'Volume' )
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    @Uom_Names AS un
                               WHERE
                                    un.Uom_Name = e.ENTITY_NAME
                                    AND un.Uom_Id = e.ENTITY_ID)
        GROUP BY
            e.ENTITY_ID
            , e.ENTITY_NAME;
        SELECT  * FROM  @Uom_Names AS un;

    END;
GO
GRANT EXECUTE ON  [dbo].[Default_UOM_Sel_By_Meter_Id] TO [CBMSApplication]
GO
