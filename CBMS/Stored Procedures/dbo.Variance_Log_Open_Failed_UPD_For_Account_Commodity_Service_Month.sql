SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







/*********     
NAME:  dbo.Variance_Log_Open_Failed_UPD_For_Account_Commodity_Service_Month    
   
DESCRIPTION:  
    Used to close all open failed variance tests exist for given Account, Commodity and service month when save & submit happens for an invoice  
  
INPUT PARAMETERS:      
      Name								DataType          Default     Description      
---------------------------------------------------------------------------------      
     @Account_Id						Int
     @Commodity_id						Int					NULL
     @Service_Month						Date	
     @User_Info_Id						Int
	 @Variance_Closed_Reason_Cd_Value	VARCHAR(25)			'Other'
     @Variance_Comment_Text				VARCHAR(MAX)		'Resubmitting invoice'

OUTPUT PARAMETERS:
      Name              DataType          Default     Description
----------------------------------------------------------------------------------- 
      
USAGE EXAMPLES:     

BEGIN TRAN  
    EXEC dbo.Variance_Log_Open_Failed_UPD_For_Account_Commodity_Service_Month 125493, 290, '2010-06-01',49

    SELECT
	   VLog.*
    FROM
	    dbo.Variance_Log VLog
	    JOIN dbo.USER_INFO ui
		    ON ui.USER_INFO_ID = 49
    WHERE
	    vlog.Account_ID = 125493
	    AND VLog.Commodity_ID = 290
	    AND VLog.Service_Month = '2010-06-01'
	    AND vlog.Is_Failure = 1
ROLLBACK TRAN


BEGIN TRAN

	SELECT * FROM Variance_Log WHERE Closed_Dt IS NULL and Account_id = 682175 AND Service_Month = '2014-08-01' AND Is_Failure = 1

EXEC Variance_Log_Open_Failed_UPD_For_Account_Commodity_Service_Month 
      @User_Info_Id = 33290
     ,@Account_Id = 682175
     ,@Commodity_Id = 290
     ,@Service_Month = '2014-08-01'
     ,@Variance_Closed_Reason_Cd_Value = NULL
     ,@Variance_Comment_Text = NULL
     
     SELECT * FROM Variance_Log WHERE Closed_Dt IS NULL and Account_id = 682175 AND Service_Month = '2014-08-01' AND Is_Failure = 1 -- Should not return any records

ROLLBACK TRAN

---------------------------------------------------------------
AUTHOR INITIALS:
Initials	Name
---------------------------------------------------------------
HG			Harihara Suthan G
CPE			Chaitanya Panduga Eshwar
AP			Arunkumar Palanivel

Initials	Date		    Modification
---------------------------------------------------------------
HG			03/10/2010		Created
HG			03/26/2010		Variance_Comment_Ins_For_Account_Commodity procedure called to insert the comment as 'Resubmitting invoice'
HG			09/06/2011		Removed the @Cost_Usage_Id parameter from the nested stored procedure Variance_Closed_Reason_INS call
CPE			2012-06-05		Loops through each commodity
HG			2014-06-20		Data-Ops Phase-II enh changes, Variance closed reason and comment made as a input parameter so that this sproc can be used anywhere and application can pass values to it.
HG			2014-08-29		Modified to update the Variance_Log irrespective of Commodity_id passed by the application and save the Comment_Map and Closed Reason of Variance with any one of the affected commodity in Variance_Log table.
AP			feb 19,2020		Modified procedure to update anomalous month data entr whenever we re run the variance
AP			Feb 20,2020		Modified procedure to update reason code in variance closed reason table
******/

CREATE PROCEDURE [dbo].[Variance_Log_Open_Failed_UPD_For_Account_Commodity_Service_Month]
      ( 
       @Account_Id INT
      ,@Commodity_id INT = NULL
      ,@Service_Month DATE
      ,@User_Info_Id INT
      ,@Variance_Closed_Reason_Cd_Value VARCHAR(25) = 'Other'	  
	  ,@Variance_Comment_Text NVARCHAR(MAX) ='Resubmitting invoice'
	)

	 
AS 
BEGIN

      SET NOCOUNT ON;
      DECLARE @Var_Commodity_Ids as TABLE ( Commodity_Id INT )
      DECLARE
           -- @Reason_Cd INT
           @Closed_Status_Cd INT
           ,@Row_Id INT = 1
           ,@iCommodity INT
		   ,@Variance_Closed_reason_Cd INT

		   DECLARE @tvp_Category_id AS TVP_Category_Ids  
	
      SET @Variance_Comment_Text = ISNULL(@Variance_Comment_Text, 'Resubmitting invoice')

	 
      --SET @Variance_Closed_Reason_Cd_Value = ISNULL(@Variance_Closed_Reason_Cd_Value, 'Other')
	  SET  @Variance_Closed_reason_Cd =	ISNULL(@Variance_Closed_reason_Cd,
								 (SELECT vcrc.Closed_Reason_Id FROM dbo.Variance_Closed_Reason_Category vcrc
	  JOIN dbo.code c 
	  ON vcrc.Closure_Category_Cd = c.Code_Id
	  WHERE c.Code_Dsc ='Other'
	  AND vcrc.Is_Active =1 ))

      --SELECT
      --      @Reason_Cd = cd.Code_Id
      --FROM
      --      dbo.Code cd
      --      JOIN dbo.CodeSet cs
      --            ON cs.Codeset_Id = cd.Codeset_Id
      --WHERE
      --      cs.Codeset_Name = 'VarianceClosedReason'
      --      AND cd.Code_Dsc = @Variance_Closed_Reason_Cd_Value

      SELECT
            @Closed_Status_Cd = cd.Code_Id
      FROM
            dbo.Code cd
            INNER JOIN dbo.CodeSet cs
                  ON cs.Codeset_Id = cd.Codeset_Id
      WHERE
            cs.Codeset_Name = 'VarianceStatus'
            AND cd.Code_Dsc = 'Closed'

      BEGIN TRY
            BEGIN TRAN

		

		--take the list of categories to make an entry in variance code reason category map table

		IF NOT EXISTS (SELECT 1 FROM @tvp_Category_id )
		begin
		INSERT @tvp_Category_id (
		                                   Category_name
		                             )
			SELECT DISTINCT c.Code_Value FROM
                  dbo.Variance_Log VLog 
				  JOIN dbo.code c 
			ON VLog.Category_Name = c.Code_Value
			JOIN dbo.Codeset cs
			ON cs.Codeset_Id = c.Codeset_Id
			WHERE cs.Codeset_name ='VarianceCategory'
            AND  vlog.Account_ID = @Account_Id
                  AND VLog.Service_Month = @Service_Month
                  AND vlog.Closed_Dt IS NULL
                  AND vlog.Is_Failure = 1
		end
            UPDATE
                  VLog
            SET   
                  VLog.Closed_Dt = GETDATE()
                 ,VLog.Closed_By_User_Info_ID = @User_Info_Id
                 ,VLog.Closed_By_User_Name = ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME
                 ,VLog.Variance_Status_Cd = @Closed_Status_Cd
                 ,VLog.Variance_Status_Desc = 'Closed'
            OUTPUT
                  inserted.Commodity_ID
                  INTO @var_Commodity_Ids
            FROM
                  dbo.Variance_Log VLog
                  INNER JOIN dbo.USER_INFO ui
                        ON ui.USER_INFO_ID = @User_Info_Id
            WHERE
                  vlog.Account_ID = @Account_Id
                  AND VLog.Service_Month = @Service_Month
                  AND vlog.Closed_Dt IS NULL
                  AND vlog.Is_Failure = 1

            SELECT TOP 1
                  @iCommodity = Commodity_Id
            FROM
                  @Var_Commodity_Ids
                  
            SET @iCommodity = isnull(@iCommodity, @Commodity_Id)
			
			IF @iCommodity IS NOT NULL
			BEGIN

				EXEC dbo.Variance_Closed_Reason_INS
				      @Reason_Id = @Variance_Closed_reason_Cd                -- int
				    , @Account_Id = @Account_Id              -- int
				    , @Service_Month = @Service_Month -- date
				    , @User_id = @User_Info_Id                  -- int
				    , @Category_id = @tvp_Category_id           -- TVP_Category_Ids new parameter will be used for AVT

					/*Currently in the existing Review Variance page, when a user clicks "Re-Run Variance Tests" button, both the comment log and the Audit Tracking tab 
					are updated. This is shown in the image below: comment log is updated with status of "Resubmitting Invoice", and Audit Tracking is updated several 
					times, showing variance test was re-run and the outcome. As a part of our changes, the new comment section will no longer be updated in this scenario.
					 When variance tests are re-run, we will not show "Resubmitting Invoice" within the comment log. 
					 Audit Tracking will continue to show these necessary status updates.*/

					--EXEC dbo.Variance_Comment_Ins_For_Account_Commodity
					--      @Account_Id = @Account_Id               -- int
					--    , @Commodity_Id = @Commodity_id            -- int
					--    , @Service_Month = @Service_Month -- date
					--    , @User_Info_Id = @User_Info_Id             -- int
					--    , @Internal_Comment_Text = @Variance_Comment_Text  -- nvarchar(max)
					--    , @external_comment_text = NULL  -- nvarchar(max)
					--    , @TVP_Comments = @tvp_Category_id          -- TVP_Category_Ids
					


					 --AP: whenever re run the variance test need to update anomalous month entry
				EXEC dbo.Variance_Anomalous_Month_Insert_Update
				      @Account_id = @Account_Id               -- int
				    , @Service_Month = @Service_Month-- date
				    , @Is_Anomalous = 0          -- bit
				    , @User_id = @User_Info_Id                  -- int
				
			END

            COMMIT TRAN

      END TRY
      BEGIN CATCH

            ROLLBACK TRAN
		
            EXEC dbo.usp_RethrowError
		
      END CATCH

END;

;

;
GO




GRANT EXECUTE ON  [dbo].[Variance_Log_Open_Failed_UPD_For_Account_Commodity_Service_Month] TO [CBMSApplication]
GO
