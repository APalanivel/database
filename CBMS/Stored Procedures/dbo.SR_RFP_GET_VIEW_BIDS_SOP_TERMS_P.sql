SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[SR_RFP_GET_VIEW_BIDS_SOP_TERMS_P] 

@userId varchar,
@sessionId varchar,
@accountGroupId int,
@bidGroupId int

AS
	set nocount on
		SELECT 
			
			dbo.SR_RFP_FN_GET_ACCOUNT_TERMS(@accountGroupId,@bidGroupId) AS ALL_ACCOUNT_TERMS_ID,
			dbo.SR_RFP_FN_GET_SOP_ACCOUNT_TERMS(@accountGroupId,@bidGroupId) AS ALL_SOP_ACCOUNT_TERMS_ID
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_VIEW_BIDS_SOP_TERMS_P] TO [CBMSApplication]
GO
