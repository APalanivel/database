SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE  PROCEDURE [dbo].[cbmsInvoiceParticipation_GetAllSiteMonthsForClient2]
	( @MyAccountId int
	, @client_id int
	)
AS
BEGIN


   select distinct ip.site_id
	, ip.service_month
from 
 	 invoice_participation2 ip 
    where client_id = @client_id

END




GO
GRANT EXECUTE ON  [dbo].[cbmsInvoiceParticipation_GetAllSiteMonthsForClient2] TO [CBMSApplication]
GO
