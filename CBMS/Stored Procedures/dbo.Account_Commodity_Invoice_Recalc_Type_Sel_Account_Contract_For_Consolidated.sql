SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Account_Commodity_Invoice_Recalc_Type_Sel_Account_Contract_For_Consolidated

DESCRIPTION:

INPUT PARAMETERS:
	Name						DataType		Default			Description
-----------------------------------------------------------------------------------------
    @Account_Id					INT	
	@Contract_Id				INT				
    @Commodity_Id				INT

OUTPUT PARAMETERS:
	Name						DataType		Default			Description
-----------------------------------------------------------------------------------------
	
USAGE EXAMPLES:
-----------------------------------------------------------------------------------------
    
 select * from Account_Commodity_Invoice_Recalc_Type where account_id=1646248

exec Account_Commodity_Invoice_Recalc_Type_Sel_Account_Contract_For_Consolidated
    1646248
    , 166787
    , 290
exec Account_Commodity_Invoice_Recalc_Type_Sel_Account_Contract_For_Consolidated 37761,166795,290 

AUTHOR INITIALS:
	Initials	Name
-----------------------------------------------------------------------------------------
	NR			Narayana Reddy	
	
	
MODIFICATIONS
	Initials	Date			Modification
-----------------------------------------------------------------------------------------
	NR       	2019-10-30		Created for Add Contract.
	NR			2020-05-27		MAINT-Contract Extension - Removed contract id filter on contract level recalc to show the recalc in  all extended contracts.

	 
******/

CREATE PROCEDURE [dbo].[Account_Commodity_Invoice_Recalc_Type_Sel_Account_Contract_For_Consolidated]
    (
        @Account_Id INT
        , @Contract_Id INT = NULL
        , @Commodity_Id INT = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Source_cd INT
            , @Con_Start_Dt DATE
            , @Con_End_Dt DATE;



        SELECT
            @Con_Start_Dt = c.CONTRACT_START_DATE
            , @Con_End_Dt = c.CONTRACT_END_DATE
        FROM
            dbo.CONTRACT c
        WHERE
            c.CONTRACT_ID = @Contract_Id;


        SELECT
            @Source_cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Valcon Source Type'
            AND c.Code_Value = 'Contract';

        SELECT
            acirt.Account_Commodity_Invoice_Recalc_Type_Id
            , acirt.Account_Id
            , com.Commodity_Id
            , com.Commodity_Name
            , c.Code_Id
            , c.Code_Value + '(' + CONVERT(VARCHAR, acirt.Start_Dt, 106) + ' - '
              + ISNULL(CONVERT(VARCHAR, acirt.End_Dt, 106), 'Open') + ')' AS Code_Value
            , acirt.Start_Dt
            , ISNULL(acirt.End_Dt, '9999-12-31') AS End_Dt
            , Source_Cd.Code_Value AS Determinant_Source
            , Determinant_Source_Cd
            , NULL AS Source_value
            , 0 AS Is_Current_Contract_Level_Recalc
        FROM
            Core.Client_Hier_Account u_cha
            INNER JOIN Core.Client_Hier_Account s_cha
                ON s_cha.Client_Hier_Id = u_cha.Client_Hier_Id
                   AND  s_cha.Meter_Id = u_cha.Meter_Id
            INNER JOIN dbo.Account_Commodity_Invoice_Recalc_Type acirt
                ON acirt.Account_Id = u_cha.Account_Id
                   AND  acirt.Commodity_ID = u_cha.Commodity_Id
            INNER JOIN dbo.Commodity com
                ON acirt.Commodity_ID = com.Commodity_Id
            INNER JOIN dbo.Code c
                ON acirt.Invoice_Recalc_Type_Cd = c.Code_Id
            LEFT JOIN dbo.Code Source_Cd
                ON acirt.Determinant_Source_Cd = Source_Cd.Code_Id
        WHERE
            acirt.Account_Id = @Account_Id
            AND (   @Contract_Id IS NULL
                    OR  s_cha.Supplier_Contract_ID = @Contract_Id)
            AND (   @Commodity_Id IS NULL
                    OR  acirt.Commodity_ID = @Commodity_Id)
            AND u_cha.Account_Type = 'Utility'
            AND s_cha.Account_Type = 'Supplier'
            AND (   acirt.Start_Dt BETWEEN @Con_Start_Dt
                                   AND     @Con_End_Dt
                    OR  ISNULL(acirt.End_Dt, '9999-12-31') BETWEEN @Con_Start_Dt
                                                           AND     @Con_End_Dt
                    OR  @Con_Start_Dt BETWEEN acirt.Start_Dt
                                      AND     ISNULL(acirt.End_Dt, '9999-12-31')
                    OR  @Con_End_Dt BETWEEN acirt.Start_Dt
                                    AND     ISNULL(acirt.End_Dt, '9999-12-31'))
        UNION

        -------------------------------Consolidated Account- Source Contract Level ---------------------------------------------	

        SELECT
            acirt.Account_Commodity_Invoice_Recalc_Type_Id
            , acirt.Account_Id
            , com.Commodity_Id
            , com.Commodity_Name
            , c.Code_Id
            , c.Code_Value + '(' + CONVERT(VARCHAR, acirt.Start_Dt, 106) + ' - '
              + ISNULL(CONVERT(VARCHAR, acirt.End_Dt, 106), 'Open') + ')' AS Code_Value
            , acirt.Start_Dt
            , ISNULL(acirt.End_Dt, '9999-12-31') AS End_Dt
            , Source_Cd.Code_Value AS Determinant_Source
            , acirt.Determinant_Source_Cd
            , cdd.Code_Value AS Source_value
            , CASE WHEN acirt.Account_Commodity_Invoice_Recalc_Type_Id IS NOT NULL
                        AND acirt.Source_Cd IS NOT NULL
                        AND crc.Contract_Id = @Contract_Id THEN 1
                  ELSE 0
              END AS Is_Current_Contract_Level_Recalc
        FROM
            Core.Client_Hier_Account u_cha
            INNER JOIN Core.Client_Hier_Account s_cha
                ON s_cha.Client_Hier_Id = u_cha.Client_Hier_Id
                   AND  s_cha.Meter_Id = u_cha.Meter_Id
            INNER JOIN dbo.Contract_Recalc_Config crc
                ON crc.Contract_Id = s_cha.Supplier_Contract_ID
            INNER JOIN dbo.Account_Commodity_Invoice_Recalc_Type acirt
                ON acirt.Account_Id = u_cha.Account_Id
                   AND  acirt.Commodity_ID = u_cha.Commodity_Id
                   AND  crc.Start_Dt = acirt.Start_Dt
                   AND  ISNULL(crc.End_Dt, '9999-12-31') = ISNULL(acirt.End_Dt, '9999-12-31')
            INNER JOIN dbo.Commodity com
                ON acirt.Commodity_ID = com.Commodity_Id
            INNER JOIN dbo.Code c
                ON crc.Supplier_Recalc_Type_Cd = c.Code_Id
            LEFT JOIN dbo.Code Source_Cd
                ON acirt.Determinant_Source_Cd = Source_Cd.Code_Id
            LEFT JOIN dbo.Code cdd
                ON acirt.Source_Cd = cdd.Code_Id
        WHERE
            acirt.Account_Id = @Account_Id
            AND acirt.Source_Cd = @Source_cd
            AND (   @Commodity_Id IS NULL
                    OR  acirt.Commodity_ID = @Commodity_Id)
            AND crc.Is_Consolidated_Contract_Config = 1
            AND u_cha.Account_Type = 'Utility'
            AND s_cha.Account_Type = 'Supplier'
            AND (   acirt.Start_Dt BETWEEN @Con_Start_Dt
                                   AND     @Con_End_Dt
                    OR  ISNULL(acirt.End_Dt, '9999-12-31') BETWEEN @Con_Start_Dt
                                                           AND     @Con_End_Dt
                    OR  @Con_Start_Dt BETWEEN acirt.Start_Dt
                                      AND     ISNULL(acirt.End_Dt, '9999-12-31')
                    OR  @Con_End_Dt BETWEEN acirt.Start_Dt
                                    AND     ISNULL(acirt.End_Dt, '9999-12-31'))
        ORDER BY
            acirt.Start_Dt;




    END;














GO
GRANT EXECUTE ON  [dbo].[Account_Commodity_Invoice_Recalc_Type_Sel_Account_Contract_For_Consolidated] TO [CBMSApplication]
GO
