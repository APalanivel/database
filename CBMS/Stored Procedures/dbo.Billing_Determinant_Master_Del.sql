SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Billing_Determinant_Master_Del]  

DESCRIPTION: It Deletes Billing Determinant Master for Selected Billing Determinant Master Id.     
      
INPUT PARAMETERS:          
	NAME								DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Billing_Determinant_Master_Id		INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
	EXEC Billing_Determinant_Master_Del 3901
  Rollback Tran

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			23-JUN-10	CREATED

*/

CREATE PROCEDURE dbo.Billing_Determinant_Master_Del
    (
      @Billing_Determinant_Master_Id INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.Billing_Determinant_Master 
	WHERE
		Billing_Determinant_Master_Id = @Billing_Determinant_Master_Id
END
GO
GRANT EXECUTE ON  [dbo].[Billing_Determinant_Master_Del] TO [CBMSApplication]
GO
