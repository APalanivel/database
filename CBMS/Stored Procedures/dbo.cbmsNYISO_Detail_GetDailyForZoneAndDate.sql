SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO




--
CREATE  PROCEDURE [dbo].[cbmsNYISO_Detail_GetDailyForZoneAndDate]  
	( @zone_id int
	, @date datetime
	, @pricing_type varchar(50)
	)
AS
BEGIN
	   select zd.date
		, zd.lbmp
	     from ny_iso_detail zd
	    where zone_id = @zone_id
	      and convert(varchar(10),date,101) = convert(varchar(10),@date,101)
	      and pricing_type = @pricing_type
    	  order by convert(varchar(10),date,101) 
		

END
GO
GRANT EXECUTE ON  [dbo].[cbmsNYISO_Detail_GetDailyForZoneAndDate] TO [CBMSApplication]
GO
