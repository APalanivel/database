SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        dbo.RM_Deal_Ticket_Client_Generated_Workflow_Sel                      
                        
Description:                        
        To get deal ticket workflows  
                        
Input Parameters:                        
    Name       DataType        Default     Description                          
--------------------------------------------------------------------------------  
   
                        
 Output Parameters:                              
 Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
     
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
   
  EXEC dbo.RM_Deal_Ticket_Client_Generated_Workflow_Sel 11236,291,586,'162277'  
  EXEC dbo.RM_Deal_Ticket_Client_Generated_Workflow_Sel 11236,291,587,'162277'  
  EXEC dbo.RM_Deal_Ticket_Client_Generated_Workflow_Sel 11236,291,587,NULL,'41270'  
    
  EXEC dbo.RM_Deal_Ticket_Client_Generated_Workflow_Sel  @Client_Id = 11236,@Commodity_Id = 291,@Hedge_Type=586,@Contract_Id = '148361'  
  EXEC dbo.RM_Deal_Ticket_Client_Generated_Workflow_Sel  @Client_Id = 11236,@Commodity_Id = 291,@Hedge_Type=586,@Site_Str = '24401,24415'  
  EXEC dbo.RM_Deal_Ticket_Client_Generated_Workflow_Sel  @Client_Id = 11236,@Commodity_Id = 291,@Hedge_Type=586  
   ,@Site_Str = '24401,24415',@RM_Group_Id='730',@Division_Id='1928'  
    
    
    
                         
 Author Initials:                        
    Initials    Name                        
--------------------------------------------------------------------------------  
    RR          Raghu Reddy     
                         
 Modifications:                        
    Initials Date           Modification                        
--------------------------------------------------------------------------------  
    RR   2018-11-15     Global Risk Management - Created   
                       
******/
CREATE PROCEDURE [dbo].[RM_Deal_Ticket_Client_Generated_Workflow_Sel]
AS 
BEGIN
      SET NOCOUNT ON;

      SELECT
            wf.Workflow_Id
           ,wf.Workflow_Name
           ,wf.Workflow_Description
      FROM
            dbo.Workflow wf
      WHERE
            wf.Workflow_Name = 'Workflow 5'


END;
GO
GRANT EXECUTE ON  [dbo].[RM_Deal_Ticket_Client_Generated_Workflow_Sel] TO [CBMSApplication]
GO
