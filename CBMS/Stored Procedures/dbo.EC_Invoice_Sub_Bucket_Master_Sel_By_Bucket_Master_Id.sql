SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.EC_Invoice_Sub_Bucket_Master_Sel_By_Bucket_Master_Id       
              
Description:              
			This sproc to get the bucket details details for a Given id.      
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
    @Bucket_Master_Id					INT	
    
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

   Exec dbo.EC_Invoice_Sub_Bucket_Master_Sel_By_Bucket_Master_Id 849,124,'Elia - Demand - Off-Peak'
   Exec dbo.EC_Invoice_Sub_Bucket_Master_Sel_By_Bucket_Master_Id 849,124
   
   Exec dbo.EC_Invoice_Sub_Bucket_Master_Sel_By_Bucket_Master_Id 70       
	
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy               
	RKV             Ravi Kumar Vegesna
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2015-04-22		Created For AS400. 
    RKV				2016-02-18		AS Part of AS400-PII Added Functionality to check whether any sub bucket is already mapped with the 
									given ubm_Sub_Bucket_code or not,If not will give all the sub_buckets based on the bucket_master	and state 
									if it is mapped give only the related sub_bucket
             
******/

CREATE PROCEDURE [dbo].[EC_Invoice_Sub_Bucket_Master_Sel_By_Bucket_Master_Id]
    (
        @Bucket_Master_Id INT
        , @State_Id INT
        , @ubm_Sub_Bucket_Code VARCHAR(250) = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;


        DECLARE @Sub_Bucket_Master_sel_By_Bucket_Master_State TABLE
              (
                  EC_Invoice_Sub_Bucket_Master_Id INT
                  , Bucket_Master_Id INT
                  , Sub_Bucket_Name VARCHAR(250)
                  , State_Id INT
              );

        INSERT INTO @Sub_Bucket_Master_sel_By_Bucket_Master_State
             (
                 EC_Invoice_Sub_Bucket_Master_Id
                 , Bucket_Master_Id
                 , Sub_Bucket_Name
                 , State_Id
             )
        SELECT
            ubdeisbm.EC_Invoice_Sub_Bucket_Master_Id
            , ubdm.Bucket_Master_Id
            , eisbm.Sub_Bucket_Name
            , eisbm.State_Id
        FROM
            UBM_BUCKET_DETERMINANT_MAP ubdm
            INNER JOIN dbo.Ubm_Bucket_Determinant_Ec_Invoice_Sub_Bucket_Map ubdeisbm
                ON ubdm.UBM_BUCKET_DETERMINANT_MAP_ID = ubdeisbm.Ubm_Bucket_Determinant_Map_Id
            INNER JOIN dbo.EC_Invoice_Sub_Bucket_Master eisbm
                ON ubdeisbm.EC_Invoice_Sub_Bucket_Master_Id = eisbm.EC_Invoice_Sub_Bucket_Master_Id
        WHERE
            ubdm.Bucket_Master_Id = @Bucket_Master_Id
            AND ubdeisbm.Ubm_Sub_Bucket_Code = @ubm_Sub_Bucket_Code
            AND eisbm.State_Id = @State_Id
            AND @ubm_Sub_Bucket_Code IS NOT NULL;


        --INSERT INTO @Sub_Bucket_Master_sel_By_Bucket_Master_State
        --     (
        --         EC_Invoice_Sub_Bucket_Master_Id
        --         , Bucket_Master_Id
        --         , Sub_Bucket_Name
        --         , State_Id
        --     )
        --SELECT
        --    cid.EC_Invoice_Sub_Bucket_Master_Id
        --    , cid.Bucket_Master_Id
        --    , eisbm.Sub_Bucket_Name
        --    , eisbm.State_Id
        --FROM
        --    dbo.CU_INVOICE_DETERMINANT cid
        --    INNER JOIN dbo.EC_Invoice_Sub_Bucket_Master eisbm
        --        ON cid.EC_Invoice_Sub_Bucket_Master_Id = eisbm.EC_Invoice_Sub_Bucket_Master_Id
        --WHERE
        --    cid.Bucket_Master_Id = @Bucket_Master_Id
        --    AND cid.Ubm_Sub_Bucket_Code = @ubm_Sub_Bucket_Code
        --    AND eisbm.State_Id = @State_Id
        --    AND @ubm_Sub_Bucket_Code IS NOT NULL
        --    AND NOT EXISTS (SELECT  1 FROM  @Sub_Bucket_Master_sel_By_Bucket_Master_State)
        --GROUP BY
        --    cid.EC_Invoice_Sub_Bucket_Master_Id
        --    , cid.Bucket_Master_Id
        --    , eisbm.Sub_Bucket_Name
        --    , eisbm.State_Id;



        INSERT INTO @Sub_Bucket_Master_sel_By_Bucket_Master_State
             (
                 EC_Invoice_Sub_Bucket_Master_Id
                 , Bucket_Master_Id
                 , Sub_Bucket_Name
                 , State_Id
             )
        SELECT
            eisbm.EC_Invoice_Sub_Bucket_Master_Id
            , eisbm.Bucket_Master_Id
            , eisbm.Sub_Bucket_Name
            , eisbm.State_Id
        FROM
            dbo.EC_Invoice_Sub_Bucket_Master eisbm
        WHERE
            eisbm.Bucket_Master_Id = @Bucket_Master_Id
            AND eisbm.State_Id = @State_Id
            AND NOT EXISTS (SELECT  1 FROM  @Sub_Bucket_Master_sel_By_Bucket_Master_State)
        GROUP BY
            eisbm.EC_Invoice_Sub_Bucket_Master_Id
            , eisbm.Bucket_Master_Id
            , eisbm.Sub_Bucket_Name
            , eisbm.State_Id;


        SELECT
            EC_Invoice_Sub_Bucket_Master_Id
            , Bucket_Master_Id
            , Sub_Bucket_Name
            , State_Id
        FROM
            @Sub_Bucket_Master_sel_By_Bucket_Master_State;


    END;
    ;

GO

GRANT EXECUTE ON  [dbo].[EC_Invoice_Sub_Bucket_Master_Sel_By_Bucket_Master_Id] TO [CBMSApplication]
GO
