SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.RM_GET_DEAL_TICKET_COUNTER_PARTY_INFORMATION_P
@userid VARCHAR(10),
@sessionid VARCHAR(20),
@dealticketid INT
AS
	set nocount on
	SELECT counterparty_name, counterparty_contact_name 
	FROM	RM_DEAL_TICKET_COUNTER_PARTY dtcontact 
		JOIN rm_counterparty rmcontact on dtcontact.rm_counterparty_id = rmcontact.rm_counterparty_id
		AND dtcontact.rm_deal_ticket_id = @dealticketid
		AND rmcontact.is_history = 0
GO
GRANT EXECUTE ON  [dbo].[RM_GET_DEAL_TICKET_COUNTER_PARTY_INFORMATION_P] TO [CBMSApplication]
GO
