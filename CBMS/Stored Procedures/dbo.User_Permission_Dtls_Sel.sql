SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        dbo.User_Permission_Dtls_Sel                    
                      
Description:                      
        To get user permissions, exisitng script dbo.GET_PERMISSIONS_FOR_USER_P has @userName as input
                      
Input Parameters:                      
    Name				DataType        Default     Description                        
--------------------------------------------------------------------------------
	@User_Info_Id			INT
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	  
                    
Usage Examples:                          
--------------------------------------------------------------------------------
	
		EXEC dbo.User_Permission_Dtls_Sel 49
                     
 Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy        
                       
 Modifications:                      
    Initials	Date        Modification                      
--------------------------------------------------------------------------------
    RR			01-08-2018  Exisitng script dbo.GET_PERMISSIONS_FOR_USER_P has @userName as input
							Global Risk Management - Created                    
                     
******/       
CREATE PROCEDURE [dbo].[User_Permission_Dtls_Sel] ( @User_Info_Id INT )
AS 
BEGIN      
      SET NOCOUNT ON;  
      
      SELECT
            pinfo.PERMISSION_INFO_ID
           ,pinfo.PERMISSION_NAME
           ,gipi.GROUP_INFO_ID
           ,gi.GROUP_NAME
      FROM
            dbo.PERMISSION_INFO pinfo
            INNER JOIN dbo.GROUP_INFO_PERMISSION_INFO_MAP gipi
                  ON pinfo.PERMISSION_INFO_ID = gipi.PERMISSION_INFO_ID
            INNER JOIN dbo.GROUP_INFO gi
                  ON gipi.GROUP_INFO_ID = gi.GROUP_INFO_ID
            INNER JOIN dbo.USER_INFO_GROUP_INFO_MAP uigi
                  ON gipi.GROUP_INFO_ID = uigi.GROUP_INFO_ID
            INNER JOIN dbo.USER_INFO ui
                  ON uigi.USER_INFO_ID = ui.USER_INFO_ID
      WHERE
            ui.USER_INFO_ID = @User_Info_Id
      GROUP BY
            pinfo.PERMISSION_INFO_ID
           ,pinfo.PERMISSION_NAME
           ,gipi.GROUP_INFO_ID
           ,gi.GROUP_NAME
      
      
   
END;
GO
GRANT EXECUTE ON  [dbo].[User_Permission_Dtls_Sel] TO [CBMSApplication]
GO
