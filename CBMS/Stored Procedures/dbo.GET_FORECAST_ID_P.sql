SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_FORECAST_ID_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@indexId       	int       	          	
	@pricePointName	varchar(200)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--exec dbo.GET_FORECAST_ID_P -1,-1,322,'CenterPoINT Energy - East (formerly Reliant - East)'

CREATE PROCEDURE dbo.GET_FORECAST_ID_P
	@userId VARCHAR(10),
	@sessionId VARCHAR(20),
	@indexId INT,
	@pricePointName VARCHAR(200)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT RM_FORECAST_ID
	FROM RM_FORECAST f JOIN price_index i
		ON f.price_index_id=i.price_index_id
			AND i.index_id=	@indexId
			AND i.pricing_point=@pricePointName

END
GO
GRANT EXECUTE ON  [dbo].[GET_FORECAST_ID_P] TO [CBMSApplication]
GO
