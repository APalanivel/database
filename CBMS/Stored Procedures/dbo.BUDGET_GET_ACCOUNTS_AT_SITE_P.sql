SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--exec BUDGET_GET_ACCOUNTS_AT_SITE_P -1,-1,10016,10412,291


CREATE         PROCEDURE dbo.BUDGET_GET_ACCOUNTS_AT_SITE_P
	@user_id varchar(10),
	@session_id varchar(20),
	@clientId int,
	@siteId int,
	@commodity_type_id int
	AS
	begin
	set nocount on

	select	cli.client_id,
		cli.client_name,
		div.division_id, 
		div.division_name,
		sit.site_id, 
		vwSite.site_name,
		utilacc.account_id, 
		utilacc.account_number,
		utility.vendor_name as utility_name,
		service_level.entity_name as service_level_char,
		budget_tariff_transport_vw.Tariff_Transport as tariff_transport,
                commodity.entity_name as commodityName


	from client cli, 
             division div, 
              site sit, 
              vwSiteName vwSite,
              account utilacc,
              vendor utility,
             -- vendor_commodity_map vendormap,
              budget_account_commodity_vw account_commodity_vw,
              budget_tariff_transport_vw,
              entity service_level ,
              entity commodity



	where cli.client_id =@clientId
	      and div.client_id = cli.client_id 
	      and sit.division_id = div.division_id 
	      and sit.site_id = @siteId
              and sit.closed=0
	      and vwSite.site_id = sit.site_id  
	      and utilacc.site_id = vwSite.site_id 
	      and utilacc.not_managed = 0
	      and utility.vendor_id = utilacc.vendor_id 
             -- and utility.vendor_id = vendormap.vendor_id
             -- and vendormap.commodity_type_id=@commodity_type_id
              and account_commodity_vw.account_id = utilacc.account_id
              and account_commodity_vw.commodity_type_id=@commodity_type_id
              and budget_tariff_transport_vw.account_id = utilacc.account_id
	      and budget_tariff_transport_vw.commodity_type_id=@commodity_type_id
	      and service_level.entity_id = utilacc.service_level_type_id 
              and commodity.entity_id=@commodity_type_id
         order by div.division_name,vwSite.site_name
	end
GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_ACCOUNTS_AT_SITE_P] TO [CBMSApplication]
GO
