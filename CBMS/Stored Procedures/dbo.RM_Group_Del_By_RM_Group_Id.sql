SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
                         
 NAME: dbo.RM_Group_Del_By_RM_Group_Id                     
                          
 DESCRIPTION:      
		  To get the manage Risk Management Groups.

 INPUT PARAMETERS:      
                         
 Name								    DataType          Default       Description      
-------------------------------------------------------------------------------------
 @@RM_Group_Id							INT 
 @User_Info_Id							INT
                          
 OUTPUT PARAMETERS:      
                               
 Name                               DataType          Default       Description      
-------------------------------------------------------------------------------------
                          
 USAGE EXAMPLES:                              
-------------------------------------------------------------------------------------

BEGIN TRAN

SELECT * FROM dbo.Cbms_Sitegroup rg WHERE rg.Cbms_Sitegroup_Id = 278
SELECT * FROM dbo.Cbms_Sitegroup_Participant rg WHERE rg.Cbms_Sitegroup_Id = 278
SELECT * FROM Application_Audit_Log WHERE Table_Name ='RM_GROUP'

EXEC dbo.RM_Group_Del_By_RM_Group_Id 278,49

SELECT * FROM dbo.Cbms_Sitegroup rg WHERE rg.Cbms_Sitegroup_Id = 278
SELECT * FROM dbo.Cbms_Sitegroup_Participant rg WHERE rg.Cbms_Sitegroup_Id = 278
SELECT * FROM Application_Audit_Log WHERE Table_Name ='RM_GROUP'

ROLLBACK TRAN

                         
 AUTHOR INITIALS:    
       
 Initials                   Name      
-------------------------------------------------------------------------------------
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
-------------------------------------------------------------------------------------
 NR                     2018-07-19      Created for GRM-51.                        
                         
******/


CREATE PROCEDURE [dbo].[RM_Group_Del_By_RM_Group_Id]
      ( 
       @RM_Group_Id INT
      ,@User_Info_Id INT
      ,@Application_Name VARCHAR(30) = NULL )
AS 
BEGIN

      SET NOCOUNT ON;

      DECLARE
            @Client_Name VARCHAR(200)
           ,@RM_Group_Name VARCHAR(200)
           ,@Audit_Function SMALLINT = 1
           ,@Lookup_Value XML
           ,@Current_Ts DATETIME = GETDATE()
           ,@User_Name VARCHAR(81);

      SELECT
            @Application_Name = ISNULL(@Application_Name, 'RM Group Delete')
            
            
      SELECT
            @User_Name = ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME
      FROM
            dbo.USER_INFO ui
      WHERE
            ui.USER_INFO_ID = @User_Info_Id;
      SELECT
            @Client_Name = ch.Client_Name
           ,@RM_Group_Name = rg.GROUP_NAME
      FROM
            dbo.Cbms_Sitegroup rg
            INNER JOIN Core.Client_Hier ch
                  ON ch.Client_Id = rg.CLIENT_ID
      WHERE
            ch.Sitegroup_Id = 0
            AND ch.Site_Id = 0
            AND rg.Cbms_Sitegroup_Id = @RM_Group_Id;

      SET @Lookup_Value = ( SELECT
                              @RM_Group_Id RM_Group_Id
                             ,@RM_Group_Name RM_Group_Name
            FOR
                            XML RAW );

     
      DELETE
            rgd
      FROM
            dbo.Cbms_Sitegroup_Participant rgd
      WHERE
            rgd.Cbms_Sitegroup_Id = @RM_Group_Id;
      DELETE
            rg
      FROM
            dbo.Cbms_Sitegroup rg
      WHERE
            rg.Cbms_Sitegroup_Id = @RM_Group_Id;


      EXEC dbo.Application_Audit_Log_Ins 
            @Client_Name
           ,@Application_Name
           ,@Audit_Function
           ,'RM_GROUP'
           ,@Lookup_Value
           ,@User_Name
           ,@Current_Ts;


END;




GO
GRANT EXECUTE ON  [dbo].[RM_Group_Del_By_RM_Group_Id] TO [CBMSApplication]
GO
