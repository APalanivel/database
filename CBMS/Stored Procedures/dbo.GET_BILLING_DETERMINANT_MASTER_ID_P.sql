SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE dbo.GET_BILLING_DETERMINANT_MASTER_ID_P
	@bdDisplayId CHAR(4),
	@parentId INT,
	@parentType	INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT billing_determinant_master_id
	FROM dbo.billing_determinant_master 
	WHERE bd_display_id = @bdDisplayId
		AND bd_parent_id = @parentId 
	    AND bd_parent_type_id = @parentType  

END
GO
GRANT EXECUTE ON  [dbo].[GET_BILLING_DETERMINANT_MASTER_ID_P] TO [CBMSApplication]
GO
