SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
			dbo.EP_NG_Commodity_Sel

DESCRIPTION:  
			Used to get the Ep and Ng commodities.

INPUT PARAMETERS:  
Name			DataType				Default				Description  
--------------------------------------------------------------------------------
       
OUTPUT PARAMETERS:  
Name			DataType				Default				Description  
--------------------------------------------------------------------------------

USAGE EXAMPLES:  
--------------------------------------------------------------------------------

	EXEC dbo.EP_NG_Commodity_Sel
	
	
AUTHOR INITIALS:  
Initials		Name  
--------------------------------------------------------------------------------
NR				Narayana Reddy

MODIFICATIONS   
Initials		Date			Modification  
--------------------------------------------------------------------------------
NR				2015-05-26		Created for AS400.

******/
CREATE PROCEDURE [dbo].[EP_NG_Commodity_Sel]
AS 
BEGIN
      SET NOCOUNT ON 
      SELECT
            c.Commodity_Id
           ,c.Commodity_Name
      FROM
            dbo.Commodity c
      WHERE
            c.Commodity_Name IN ( 'Electric Power', 'Natural Gas' )
END

;
GO
GRANT EXECUTE ON  [dbo].[EP_NG_Commodity_Sel] TO [CBMSApplication]
GO
