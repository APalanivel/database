SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    Trade.Deal_Ticket_Trade_Executed_Deal_Ticket_Sel_For_ALert
   
    
DESCRIPTION:   
   
    
INPUT PARAMETERS:    
      Name          DataType       Default        Description    
-----------------------------------------------------------------------------    
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec Trade.Deal_Ticket_Trade_Executed_Deal_Ticket_Sel_For_ALert  

	
	'Trade.Deal_Ticket_Trade_Executed_Deal_Ticket_Sel_For_ALert'
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	RR          Raghu Reddy

    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	RR          2019-05-22	GRM Proejct.

******/
CREATE PROC [Trade].[Deal_Ticket_Trade_Executed_Deal_Ticket_Sel_For_ALert]
      (
      @Deal_Ticket_Id VARCHAR(MAX) = NULL )
AS
      BEGIN
            SET NOCOUNT ON;

            SELECT
                        dt.Deal_Ticket_Id
                      , ' ' AS Risk_Manager
                      , ' ' AS Risk_Manager_Email
                      , ' ' AS Risk_Manager_Phone
                      , CASE WHEN len(CQ.CurrentQ_Email) > 0
                                   THEN left(CQ.CurrentQ_Email, len(CQ.CurrentQ_Email) - 1)
                        END AS Currently_With_Email
                      , CASE WHEN len(CM.CM_Email) > 0
                                   THEN left(CM.CM_Email, len(CM.CM_Email) - 1)
                        END AS CM_Email
                      , ch.Client_Name
                      , CASE WHEN count(DISTINCT ch.Site_Id) > 1
                                   THEN 'Multiple'
                             ELSE  max(rtrim(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')')
                        END AS Site_Name
                      , trd.Cbms_Trade_Number
                      , chws.Workflow_Status_Map_Id
                      , CASE WHEN dttyp.ENTITY_NAME = 'Physical'
                                   THEN cha.VENDOR_NAME
                             WHEN dttyp.ENTITY_NAME = 'Financial'
                                   THEN cp.COUNTERPARTY_NAME
                        END AS Counterparty_Supplier
                      , CASE WHEN len(CMName.CM_Name) > 0
                                   THEN left(CMName.CM_Name, len(CMName.CM_Name) - 1)
                        END AS CM_Name
                      , ui2.EMAIL_ADDRESS AS Initiator_Email
                      , ui2.FIRST_NAME + ' ' + ui2.LAST_NAME AS Initiator_Name
            FROM        Trade.Deal_Ticket dt
                        INNER JOIN
                        dbo.ENTITY dttyp
                              ON dt.Hedge_Type_Cd = dttyp.ENTITY_ID
                        INNER JOIN
                        Trade.Deal_Ticket_Client_Hier dtch
                              ON dtch.Deal_Ticket_Id = dt.Deal_Ticket_Id
                        INNER JOIN
                        Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                              ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
                        INNER JOIN
                        Core.Client_Hier ch
                              ON ch.Client_Id = dt.Client_Id
                                 AND ch.Client_Hier_Id = dtch.Client_Hier_Id
                        INNER JOIN
                        Trade.Deal_Ticket_Client_Hier_TXN_Status trd
                              ON chws.Deal_Ticket_Client_Hier_Workflow_Status_Id = trd.Deal_Ticket_Client_Hier_Workflow_Status_Id
                        INNER JOIN
                        Trade.Trade_Price tp
                              ON trd.Trade_Price_Id = tp.Trade_Price_Id
                        CROSS APPLY
                  (     SELECT
                              ui.EMAIL_ADDRESS + ','
                        FROM  dbo.USER_INFO ui
                        WHERE dt.Queue_Id = ui.QUEUE_ID
                        FOR XML PATH('')) CQ(CurrentQ_Email)
                        CROSS APPLY
                  (     SELECT
                              ui.EMAIL_ADDRESS + ','
                        FROM  dbo.USER_INFO ui
                              INNER JOIN
                              dbo.CLIENT_CEM_MAP ccm
                                    ON ui.USER_INFO_ID = ccm.USER_INFO_ID
                        WHERE ccm.CLIENT_ID = dt.Client_Id
                        FOR XML PATH('')) CM(CM_Email)
                        CROSS APPLY
                  (     SELECT
                                    ui.FIRST_NAME + ' ' + ui.LAST_NAME + ', '
                        FROM        dbo.USER_INFO ui
                                    INNER JOIN
                                    dbo.CLIENT_CEM_MAP ccm
                                          ON ui.USER_INFO_ID = ccm.USER_INFO_ID
                        WHERE       ccm.CLIENT_ID = dt.Client_Id
                        GROUP BY    ui.FIRST_NAME + ' ' + ui.LAST_NAME
                        FOR XML PATH('')) CMName(CM_Name)
                        LEFT JOIN
                        dbo.VENDOR cha
                              ON cha.VENDOR_ID = trd.Cbms_Counterparty_Id
                                 AND dttyp.ENTITY_NAME = 'Physical'
                        LEFT JOIN
                        dbo.RM_COUNTERPARTY cp
                              ON cp.RM_COUNTERPARTY_ID = trd.Cbms_Counterparty_Id
                                 AND dttyp.ENTITY_NAME = 'Financial'
                        LEFT JOIN
                        dbo.USER_INFO ui2
                              ON dt.Created_User_Id = ui2.USER_INFO_ID
            WHERE       chws.Is_Active = 1
                        AND   EXISTS
                  (     SELECT
                              1
                        FROM  Trade.Workflow_Status_Map wsm
                              INNER JOIN
                              Trade.Workflow_Status ws
                                    ON wsm.Workflow_Status_Id = ws.Workflow_Status_Id
                        WHERE chws.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
                              AND   ws.Workflow_Status_Name = 'Order Executed' )
                        AND   tp.Trade_Price IS NOT NULL
                        AND
                              (     @Deal_Ticket_Id IS NULL
                                    OR    EXISTS
                  (     SELECT
                              1
                        FROM  dbo.ufn_split(@Deal_Ticket_Id, ',') us
                        WHERE us.Segments = dt.Deal_Ticket_Id ))
                        AND   chws.Last_Notification_Sent_Ts IS NULL
            GROUP BY    dt.Deal_Ticket_Id
                      , CASE WHEN len(CQ.CurrentQ_Email) > 0
                                   THEN left(CQ.CurrentQ_Email, len(CQ.CurrentQ_Email) - 1)
                        END
                      , CASE WHEN len(CM.CM_Email) > 0
                                   THEN left(CM.CM_Email, len(CM.CM_Email) - 1)
                        END
                      , ch.Client_Name
                      , trd.Cbms_Trade_Number
                      , chws.Workflow_Status_Map_Id
                      , CASE WHEN dttyp.ENTITY_NAME = 'Physical'
                                   THEN cha.VENDOR_NAME
                             WHEN dttyp.ENTITY_NAME = 'Financial'
                                   THEN cp.COUNTERPARTY_NAME
                        END
                      , CASE WHEN len(CMName.CM_Name) > 0
                                   THEN left(CMName.CM_Name, len(CMName.CM_Name) - 1)
                        END
                      , ui2.EMAIL_ADDRESS
                      , ui2.FIRST_NAME
                      , ui2.LAST_NAME;

      END;
GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Trade_Executed_Deal_Ticket_Sel_For_ALert] TO [CBMSApplication]
GO
