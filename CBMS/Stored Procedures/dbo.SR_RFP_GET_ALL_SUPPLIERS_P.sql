SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--exec dbo.SR_RFP_GET_ALL_SUPPLIERS_P 1,1

--select * from entity where entity_id=289

-- select * from vendor_commodity_map

CREATE PROCEDURE dbo.SR_RFP_GET_ALL_SUPPLIERS_P 
@userId varchar,
@sessionId varchar

as
	set nocount on
declare @supplierTypeId int

select @supplierTypeId=(select entity_id from entity where entity_type=155
 and entity_name='Supplier')

Select	v.VENDOR_ID,
	v.VENDOR_NAME 

from 	VENDOR v
	

where 	v.VENDOR_TYPE_ID = @supplierTypeId
	
order by v.vendor_name
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_ALL_SUPPLIERS_P] TO [CBMSApplication]
GO
