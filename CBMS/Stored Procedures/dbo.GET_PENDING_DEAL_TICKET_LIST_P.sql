SET NUMERIC_ROUNDABORT OFF
GO

SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO

/******
NAME:
	CBMS.dbo.GET_PENDING_DEAL_TICKET_LIST_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@clientId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.GET_PENDING_DEAL_TICKET_LIST_P NULL,NULL,235

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			04/13/2011	MAINT-359 -- fixes the problem with the code to get DEAL_TRANSACTION_TYPE column as blank string incase of nulll value.
							as well removed subquery at where clause which was written in from clause.
	HG			04/20/2011	Replaced Division and vwSitename by Sitegroup and Core.Client_Hier table
								Left join on sitegroup can't be replaced by core.Client_Hier table as we have records where Division_id saved as NULL with valid site_id.

								unused @userId and @sessionid paramter removed
******/

CREATE PROCEDURE dbo.GET_PENDING_DEAL_TICKET_LIST_P
    @clientId	INT
AS 
BEGIN

    SET NOCOUNT ON ;

    SELECT
		 RM_DEAL_TICKET_ID
		,DEAL_TICKET_NUMBER
		,CLIENT_NAME
		,SITE_NAME
		,Division_Name
		,DEAL_TYPE
		,DEAL_INITIATED_DATE
		,HEDGE_START_MONTH
		,HEDGE_END_MONTH
		,HEDGE_LEVEL_TYPE
		,DEAL_STATUS
		,DEAL_TRANSACTION_TYPE
		,GROUP_NAME
    FROM    
    (
			SELECT    
				rmdt.RM_DEAL_TICKET_ID,
				rmdt.DEAL_TICKET_NUMBER,
				c.CLIENT_NAME,
				RTRIM(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' SITE_NAME,
				sg.Sitegroup_Name Division_Name,
				e1.ENTITY_NAME AS DEAL_TYPE,
				rmdt.DEAL_INITIATED_DATE,
				rmdt.HEDGE_START_MONTH,
				rmdt.HEDGE_END_MONTH,
				e2.ENTITY_NAME AS HEDGE_LEVEL_TYPE,
				CASE e3.ENTITY_NAME
				  WHEN 'New' THEN 'New'
				  WHEN 'Client Approved' THEN 'Client Approved'
				  WHEN 'Place Bid' THEN 'Place Bid'
				  WHEN 'Manage Bid' THEN 'Manage Bid'
				  WHEN 'Order Placed' THEN 'Order Placed'
				  WHEN 'Order Executed' THEN 'Order Executed'
				  WHEN 'Potentially Fixed'
				  THEN 'Potentially Fixed'
				  WHEN 'Closed' THEN 'Closed'
				  WHEN 'Cancelled' THEN 'Cancelled'
				  WHEN 'Marked To Cancel' THEN 'Marked To Cancel'
				  WHEN 'Expired' THEN 'Expired'
				  ELSE 
					rmdts.Entity_Name
				END AS DEAL_STATUS,
				ISNULL(trans.ENTITY_NAME,'') AS DEAL_TRANSACTION_TYPE,
				rmgroup.GROUP_NAME
		  FROM      
				dbo.Client c
				JOIN dbo.RM_DEAL_TICKET rmdt
					 ON rmdt.CLIENT_ID = c.client_id
				LEFT JOIN 
					(
						SELECT 
							CASE COUNT(1) 
								  WHEN 1 THEN 'Order Executed'
								  WHEN 2 THEN 'Closed'
							END AS Entity_Name
							,rmdts.rm_deal_ticket_id
						 FROM
							dbo.rm_deal_ticket_transaction_status rmdts
							JOIN dbo.entity ent
								 ON ent.ENTITY_ID = rmdts.deal_transaction_status_type_id
						 WHERE  
							ent.Entity_name IN ('Client Confirmation','Counterparty Confirmation' )
						GROUP BY
							rmdts.rm_deal_ticket_id
					 ) AS rmdts
						 ON rmdts.RM_DEAL_TICKET_ID = rmdt.rm_deal_ticket_id

				LEFT JOIN dbo.Sitegroup sg						  
						  ON ( rmdt.DIVISION_ID = sg.Sitegroup_id )

				LEFT JOIN Core.Client_Hier ch
						  ON ( ch.SITE_ID = rmdt.SITE_ID )
				LEFT JOIN dbo.ENTITY trans 
						  ON ( rmdt.DEAL_TRANSACTION_TYPE_ID = trans.ENTITY_ID )
				LEFT JOIN dbo.RM_GROUP rmgroup 
						  ON ( rmdt.RM_GROUP_ID = rmgroup.RM_GROUP_ID )
				JOIN dbo.ENTITY e1 
					 ON ( rmdt.DEAL_TYPE_ID = e1.ENTITY_ID )
				JOIN dbo.ENTITY e2 
					 ON ( rmdt.HEDGE_LEVEL_TYPE_ID = e2.ENTITY_ID )
				JOIN dbo.RM_DEAL_TICKET_TRANSACTION_STATUS rmdtts 
					 ON ( rmdt.RM_DEAL_TICKET_ID = rmdtts.RM_DEAL_TICKET_ID )
				JOIN dbo.ENTITY e3
					 ON rmdtts.DEAL_TRANSACTION_STATUS_TYPE_ID = e3.ENTITY_ID	 
				JOIN 
					(
						SELECT
							 RM_DEAL_TICKET_ID
							,MAX(DEAL_TRANSACTION_DATE) DEAL_TRANSACTION_DATE
						FROM    
							dbo.RM_DEAL_TICKET_TRANSACTION_STATUS
						GROUP BY
							RM_DEAL_TICKET_ID
					) AS RMDTT_STATUS
						 ON RMDTT_STATUS.RM_DEAL_TICKET_ID = rmdt.RM_DEAL_TICKET_ID
		  WHERE			 
				c.CLIENT_ID = @clientId
				AND rmdtts.DEAL_TRANSACTION_DATE = RMDTT_STATUS.DEAL_TRANSACTION_DATE
		) A
	WHERE   
		A.DEAL_STATUS NOT IN ( 'Closed', 'Cancelled', 'Expired' )
		AND A.DEAL_TRANSACTION_TYPE NOT IN ( 'Triggers placed and hit' )
	ORDER BY 
		A.DEAL_INITIATED_DATE DESC
		
END
GO

GRANT EXECUTE ON  [dbo].[GET_PENDING_DEAL_TICKET_LIST_P] TO [CBMSApplication]
GO
GO