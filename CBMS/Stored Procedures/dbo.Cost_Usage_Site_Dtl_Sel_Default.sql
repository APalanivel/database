
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.Cost_Usage_Site_Dtl_Sel_Default

DESCRIPTION:

	Used to Select the data from Cost_Usage_Account_Dtl table for the given Site_id , period and commodity
	
INPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	@Site_Id			INT
	@Commodity_id		INT
	@Begin_Dt			DATE
	@End_Dt				DATE


OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Cost_Usage_Site_Dtl_Sel_Default 289850,1374,'2011-01-01','2011-12-31'
	EXEC dbo.Cost_Usage_Site_Dtl_Sel_Default 193597,67,'2012-01-01','2012-01-31'
	EXEC dbo.Cost_Usage_Site_Dtl_Sel_Default 289850,290,'2012-01-01','2012-12-31'
	EXEC dbo.Cost_Usage_Site_Dtl_Sel_Default 193597,291,'2012-01-01','2012-12-31'	
		
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hari
	SKA			Shobhit Kumar Agrawal
	BCH			Balaraju
	RR			Raghu Reddy

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	SKA        	03/04/2010	Created
	HG			03/05/2010	Cbms_Image_Id column added in the Select clause.
							Order by clause added to sort the records based on bucket type and Bucket name.
	HG			03/17/2010	@Commodity_Id made as mandatory as application always looks data for the specific commodity.							
							Cbms_Image_Id column removed from Cost_Usage_Site_Dtl table and moved to Site_Commodity_Image, selection of that column modified accordingly.
    BCH			2012-03-16  I have modified to Client_hier_id parameter instead of site_id.and used core.client_hier table.
    BCH			2013-06-07	ENHANCEMENT-68, Added Last_Updated_Date and Last_Updated_By columns in output
    RR			2013-11-18	MAINT-2325 Added  data source code and uom name in select list
******/
CREATE PROCEDURE dbo.Cost_Usage_Site_Dtl_Sel_Default
      ( 
       @Client_Hier_Id INT
      ,@Commodity_id INT
      ,@Begin_Dt DATE
      ,@End_Dt DATE )
AS 
BEGIN  
      SET NOCOUNT ON  
      SELECT
            ch.Client_Hier_Id
           ,cusd.Service_Month
           ,cusd.UOM_Type_Id
           ,cusd.CURRENCY_UNIT_ID
           ,bkt_cd.Code_Value Bucket_Type
           ,bm.Bucket_Name
           ,cusd.Bucket_Value
           ,cusd.Cost_Usage_Site_Dtl_Id
           ,cusd.Bucket_Master_Id
           ,bm.Commodity_Id
           ,sci.CBMS_IMAGE_ID
           ,cusd.Updated_Ts AS Last_Updated_Date
           ,case WHEN cusd.Updated_By_Id IS NULL THEN crusr.FIRST_NAME + space(1) + crusr.LAST_NAME
                 ELSE modusr.FIRST_NAME + space(1) + modusr.LAST_NAME
            END AS Last_Updated_By
           ,dscd.Code_Value Data_Source_Code
           ,uom.ENTITY_NAME UOM
      FROM
            dbo.Cost_Usage_Site_Dtl cusd
            INNER JOIN dbo.Bucket_Master bm
                  ON bm.Bucket_Master_Id = cusd.Bucket_Master_Id
            INNER JOIN dbo.Code bkt_cd
                  ON bkt_cd.Code_Id = bm.Bucket_Type_Cd
            INNER JOIN Core.Client_Hier ch
                  ON ch.Client_Hier_Id = cusd.Client_Hier_Id
            INNER JOIN dbo.User_Info crusr
                  ON crusr.USER_INFO_ID = cusd.Created_By_Id
            LEFT OUTER JOIN dbo.Site_Commodity_Image sci
                  ON sci.Site_id = ch.SITE_ID
                     AND sci.Commodity_Id = bm.Commodity_Id
                     AND sci.Service_Month = cusd.Service_Month
            LEFT OUTER JOIN dbo.USER_INFO modusr
                  ON modusr.USER_INFO_ID = cusd.Updated_By_Id
            LEFT OUTER JOIN dbo.Code dscd
                  ON cusd.Data_Source_Cd = dscd.Code_Id
            LEFT OUTER JOIN dbo.ENTITY uom
                  ON cusd.UOM_Type_Id = uom.ENTITY_ID
      WHERE
            cusd.Client_Hier_Id = @Client_Hier_Id
            AND ( bm.Commodity_Id = @Commodity_id )
            AND cusd.Service_Month BETWEEN @Begin_Dt AND @End_Dt
      ORDER BY
            cusd.Service_Month
           ,bkt_cd.Code_Value DESC
           ,bm.Bucket_Name

END;
;
GO




GRANT EXECUTE ON  [dbo].[Cost_Usage_Site_Dtl_Sel_Default] TO [CBMSApplication]
GO
