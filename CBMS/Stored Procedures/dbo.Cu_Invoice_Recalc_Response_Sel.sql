SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:   dbo.Cu_Invoice_Recalc_Response_Sel             
                  
Description:                  
                         
                  
 Input Parameters:                  
    Name    DataType Default   Description                    
--------------------------------------------------------------------          
  @Cu_Invoice_Id  INT     
  @Account_Id   INT    
  @Commodity_Id   INT    
            
 Output Parameters:                        
    Name    DataType Default   Description                    
--------------------------------------------------------------------          
       
                  
 Usage Examples:                      
--------------------------------------------------------------------          
     
     
 EXEC Cu_Invoice_Recalc_Response_Sel 35238716,54239,291    
 EXEC Cu_Invoice_Recalc_Response_Sel 31460832, 55236, 290 
 exec Cu_Invoice_Recalc_Response_Sel 79439981,1698645,291

    
    
     
                 
Author Initials:                  
 Initials  Name                  
--------------------------------------------------------------------          
 RKV   Ravi Kumar Vegesna    
 NR    Narayana Reddy  
 SP    Srinivas Patchava
   
 Modifications:    
                 
 Initials         Date   Modification                  
--------------------------------------------------------------------          
 RKV     2015-10-16 Created For AS400-II.    
 NR      2017-03-16 MAINT-4845 - Added Is_Valid_Charge column in Output List.  
 NR      2017-10-17 SE2017-263 - return  the Charge_GUID,Is_Locked,Is_Send_To_Sys,Charge_Comment_Id new columns. 
 SP      2019-11-05 MAINT-9496 - added to Calculator_Working in order by clause.  
  
              
                 
******/
CREATE PROCEDURE [dbo].[Cu_Invoice_Recalc_Response_Sel]
    (
        @Cu_Invoice_Id INT
        , @Account_Id INT
        , @Commodity_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;



        SELECT
            Charge_Name
            , Bucket_Name
            , Determinant_Unit
            , Determinant_Value
            , Rate_Unit
            , Rate_Currency
            , Rate_Amount
            , Net_Amount
            , Calculator_Working
            , cirr.Cu_Invoice_Recalc_Response_Id
            , cirr.Recalc_Header_Id
            , cirr.Is_Hidden_On_RA
            , c.Code_Value Recalc_Response_Source
            , c.Code_Id Recalc_Response_Source_cd
            , cirr.Created_Ts
            , cirr.Last_Change_Ts
            , cirr.Is_Valid_Charge
            , cirr.Charge_GUID
            , cirr.Is_Locked
            , cirr.Is_Send_To_Sys
            , cirr.Charge_Comment_Id
            , cent.Comment_Text
            , curr.SYMBOL currency_symbol
        FROM
            dbo.Cu_Invoice_Recalc_Response cirr
            INNER JOIN dbo.RECALC_HEADER rh
                ON cirr.Recalc_Header_Id = rh.RECALC_HEADER_ID
            INNER JOIN dbo.Bucket_Master bm
                ON cirr.Bucket_Master_Id = bm.Bucket_Master_Id
            INNER JOIN dbo.Code c
                ON c.Code_Id = cirr.Recalc_Response_Source_Cd
            LEFT OUTER JOIN dbo.Comment cent
                ON cent.Comment_ID = cirr.Charge_Comment_Id
            LEFT OUTER JOIN dbo.CURRENCY_UNIT curr
                ON cirr.Net_Amt_Currency_Unit_Id = curr.CURRENCY_UNIT_ID
        WHERE
            rh.CU_INVOICE_ID = @Cu_Invoice_Id
            AND rh.Commodity_Id = @Commodity_Id
            AND rh.ACCOUNT_ID = @Account_Id
        ORDER BY
            Bucket_Name
            , cirr.Calculator_Working;


    END;


    ;
    ;


GO


GRANT EXECUTE ON  [dbo].[Cu_Invoice_Recalc_Response_Sel] TO [CBMSApplication]
GO
