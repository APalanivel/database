SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
CBMS.dbo.Report_DE_Utility_Details_EP_All  
  
        
DESCRIPTION:   INPUT PARAMETERS:    
Name    DataType  Default Description    
------------------------------------------------------------    
  
  
OUTPUT PARAMETERS:    
Name   DataType  Default Description    
------------------------------------------------------------    
  
  
USAGE EXAMPLES:    
------------------------------------------------------------    
EXEC dbo.Report_DE_Utility_Details_EP_All  
  
AUTHOR INITIALS:    
Initials   Name    
-----------------------------------------------------------    
AKR       Ashok Kumar Raju  
  
MODIFICATIONS     
Initials Date  Modification    
------------------------------------------------------------    
 AKR 2014-12-08  Create the SP  
******/    
    
CREATE PROCEDURE [dbo].[Report_DE_Utility_Details_EP_All]
AS 
BEGIN              
      SET NOCOUNT ON;     
        
      DECLARE
            @Commodity_Id_EP INT
           ,@Commodity_Id_NG INT
           ,@Vendor_Type_Id INT  
              
      SELECT
            @Commodity_Id_EP = c.Commodity_Id
      FROM
            dbo.Commodity c
      WHERE
            c.Commodity_Name = 'Electric Power'  
              
      SELECT
            @Commodity_Id_NG = c.Commodity_Id
      FROM
            dbo.Commodity c
      WHERE
            c.Commodity_Name = 'Natural Gas'        
              
      SELECT
            @Vendor_Type_Id = e.Entity_Id
      FROM
            dbo.ENTITY e
      WHERE
            e.ENTITY_NAME = 'Utility'
            AND e.ENTITY_DESCRIPTION = 'Vendor'  
              
    
        
      SELECT
            Vendor_Name [Utility]
           ,STATE_NAME [State]
           ,COUNTRY_NAME [Country]
           ,[Electric Power] = CASE WHEN v.VENDOR_ID IN ( SELECT
                                                            vendor_id
                                                          FROM
                                                            VENDOR_COMMODITY_MAP
                                                          WHERE
                                                            Commodity_type_Id = @Commodity_Id_EP ) THEN 'Yes'
                                    ELSE 'No'
                               END
           ,[Natural Gas] = CASE WHEN v.VENDOR_ID IN ( SELECT
                                                            VENDOR_id
                                                       FROM
                                                            VENDOR_COMMODITY_MAP
                                                       WHERE
                                                            Commodity_type_Id = @Commodity_Id_NG ) THEN 'Yes'
                                 ELSE 'No'
                            END
           ,c.Code_Value [EP Ind Option To Transport]
           ,c1.Code_Value [EP Com Option To Transport]
           ,CAST(comm.Comment_Text AS VARCHAR(4000)) [EP Option to Transport Comments]
           ,MAX(CASE WHEN q.Question_Label = 'Industrial' THEN q.Utility_Volume_Dsc
                END) [EP Ind Volume Requirement]
           ,MAX(CASE WHEN q.Question_Label = 'Industrial' THEN q.Code_Value
                END) [EP Ind UOM]
           ,MAX(CASE WHEN q.Question_Label = 'Industrial' THEN q.Time_Period
                END) [EP Ind Time Period]
           ,MAX(CASE WHEN q.Question_Label = 'Industrial' THEN q.Time_Req
                END) [EP Ind Time Requirements]
           ,MAX(CASE WHEN q.Question_Label = 'Industrial' THEN q.Other_Req
                END) [EP Ind Other Requirements or stipulations to transport]
           ,MAX(CASE WHEN q.Question_Label = 'Commercial' THEN q.Utility_Volume_Dsc
                END) [EP Com Volume Requirement]
           ,MAX(CASE WHEN q.Question_Label = 'Commercial' THEN q.Code_Value
                END) [EP Com UOM]
           ,MAX(CASE WHEN q.Question_Label = 'Commercial' THEN q.Time_Period
                END) [EP Com Time Period]
           ,MAX(CASE WHEN q.Question_Label = 'Commercial' THEN q.Time_Req
                END) [EP Com Time Requirements]
           ,MAX(CASE WHEN q.Question_Label = 'Commercial' THEN q.Other_Req
                END) [EP Com Other Requirements or stipulations to transport]
           ,MAX(CASE WHEN q.Question_Label = 'Utility Restrictions' THEN q.Utility_Volume_Dsc
                END) [EP Utility Restrictions Volume Requirement]
           ,MAX(CASE WHEN q.Question_Label = 'Utility Restrictions' THEN q.Code_Value
                END) [EP Utility Restrictions UOM]
           ,MAX(CASE WHEN q.Question_Label = 'Utility Restrictions' THEN q.Time_Period
                END) [EP Utility Restrictions Time Period]
           ,MAX(CASE WHEN q.Question_Label = 'Utility Restrictions' THEN q.Time_Req
                END) [EP Utility Restrictions Time Requirements]
           ,MAX(CASE WHEN q.Question_Label = 'Utility Restrictions' THEN q.Other_Req
                END) [EP Utility Restrictions Other Requirements or stipulations to transport]
           ,MAX(CASE WHEN q.Question_Label = 'Supplier Restrictions' THEN q.Utility_Volume_Dsc
                END) [EP Supplier Restrictions Volume Requirement]
           ,MAX(CASE WHEN q.Question_Label = 'Supplier Restrictions' THEN q.Code_Value
                END) [EP Supplier Restrictions UOM]
           ,MAX(CASE WHEN q.Question_Label = 'Supplier Restrictions' THEN q.Time_Period
                END) [EP Supplier Restrictions Time Period]
           ,MAX(CASE WHEN q.Question_Label = 'Supplier Restrictions' THEN q.Time_Req
                END) [EP Supplier Restrictions Time Requirements]
           ,MAX(CASE WHEN q.Question_Label = 'Supplier Restrictions' THEN q.Other_Req
                END) [EP Supplier Restrictions Other Requirements or stipulations to transport]
           ,MAX(CASE WHEN q4.Question_Label = 'Is telemetering required to transport?' THEN q4.[Ind Telemetering]
                END) [EP Ind Telemetering Required]
           ,MAX(CASE WHEN q4.Question_Label = 'Is telemetering required to transport?' THEN q4.[Com Telemetering]
                END) [EP Com Telemetering Required]
           ,MAX(CASE WHEN q4.Question_Label = 'Is telemetering required to transport?' THEN CAST(q4.[Telemetering Comments] AS VARCHAR(4000))
                END) [EP Telemetering Required Comments]
           ,MAX(CASE WHEN q4.Question_Label = 'Is there an additional cost?' THEN q4.[Ind Telemetering]
                END) [EP Ind Telemetering Additional Cost]
           ,MAX(CASE WHEN q4.Question_Label = 'Is there an additional cost?' THEN q4.[Com Telemetering]
                END) [EP Com Telemetering Additional Cost]
           ,MAX(CASE WHEN q4.Question_Label = 'Is there an additional cost?' THEN CAST(q4.[Telemetering Comments] AS VARCHAR(4000))
                END) [EP Telemetering Additional Cost Comments]
           ,MAX(CASE WHEN q4.Question_Label = 'How are meter readings obtained? (website, LDC software, etc.)' THEN CAST(q4.[Telemetering Comments] AS VARCHAR(4000))
                END) [EP Telemetering Meter Read Comments]
           ,MAX(CASE WHEN q4.Question_Label = 'Online bill information availability?' THEN q4.[Ind Telemetering]
                END) [EP Ind Telemetering Online Bill]
           ,MAX(CASE WHEN q4.Question_Label = 'Online bill information availability?' THEN q4.[Com Telemetering]
                END) [EP Com Telemetering Online Bill]
           ,MAX(CASE WHEN q4.Question_Label = 'Online bill information availability?' THEN CAST(q4.[Telemetering Comments] AS VARCHAR(4000))
                END) [EP Telemetering Online Bill Comments]
           ,MAX(CASE WHEN q8.Question_Label = 'Does utility offer firm & interruptible transport rates?' THEN q8.[Industrial]
                END) AS [EP Ind Firm & Int Transport]
           ,MAX(CASE WHEN q8.Question_Label = 'Does utility offer firm & interruptible transport rates?' THEN q8.[Commercial]
                END) AS [EP Com Firm & Int Transport]
           ,MAX(CASE WHEN q8.Question_Label = 'Does utility offer firm & interruptible transport rates?' THEN CAST(q8.[Comments] AS VARCHAR(4000))
                END) [EP Firm & Int Transport Comments]
           ,MAX(CASE WHEN q8.Question_Label = 'What are penalties if customer does not interrupt when called?' THEN CAST(q8.[Comments] AS VARCHAR(4000))
                END) [EP Penalties Comments]
           ,MAX(CASE WHEN q8.Question_Label = 'Is cogen required for interruptible transport?' THEN q8.[Industrial]
                END) AS [EP Ind Int Cogen Requirement]
           ,MAX(CASE WHEN q8.Question_Label = 'Is cogen required for interruptible transport?' THEN q8.[Commercial]
                END) AS [EP Com Int Cogen Requirement]
           ,MAX(CASE WHEN q8.Question_Label = 'Is cogen required for interruptible transport?' THEN CAST(q8.[Comments] AS VARCHAR(4000))
                END) [EP Int Cogen Requirement Comments]
           ,MAX(CASE WHEN q8.Question_Label = 'Does utility allow customers to switch between tariff and transport on a monthly basis?' THEN q8.[Industrial]
                END) AS [EP Ind Monthly Switching]
           ,MAX(CASE WHEN q8.Question_Label = 'Does utility allow customers to switch between tariff and transport on a monthly basis?' THEN q8.[Commercial]
                END) AS [EP Com Monthly Switching]
           ,MAX(CASE WHEN q8.Question_Label = 'Does utility allow customers to switch between tariff and transport on a monthly basis?' THEN CAST(q8.[Comments] AS VARCHAR(4000))
                END) [EP Monthly Switching Comments]
           ,MAX(CASE WHEN q8.Question_Label = 'If this is rate specific, what rate schedule applies to this option?' THEN CAST(q8.[Comments] AS VARCHAR(4000))
                END) [EP Monthly Switching Rate Schedule Comments]
           ,MAX(CASE WHEN q8.Question_Label = 'Ability to hedge through the utility?' THEN q8.[Industrial]
                END) AS [EP Ind Hedge Ability]
           ,MAX(CASE WHEN q8.Question_Label = 'Ability to hedge through the utility?' THEN q8.[Commercial]
                END) AS [EP Com Hedge Ability]
           ,MAX(CASE WHEN q8.Question_Label = 'Ability to hedge through the utility?' THEN CAST(q8.[Comments] AS VARCHAR(4000))
                END) [EP Hedge Ability Comments]
           ,MAX(CASE WHEN q14.Question_Label = 'Nominations' THEN CAST(q14.[Comments] AS VARCHAR(4000))
                END) [EP Nominations Comments]
           ,MAX(CASE WHEN q14.Question_Label = 'Rate Changes' THEN CAST(q14.[Comments] AS VARCHAR(4000))
                END) [EP Rate Changes Comments]
           ,MAX(CASE WHEN q14.Question_Label = 'Marketer Changes' THEN CAST(q14.[Comments] AS VARCHAR(4000))
                END) [EP Marketer Changes Comments]
           ,MAX(CASE WHEN q14.Question_Label = 'Contract Changes' THEN CAST(q14.[Comments] AS VARCHAR(4000))
                END) [EP Contract Changes Comments]
      FROM
            dbo.VENDOR v
            JOIN dbo.VENDOR_STATE_MAP vsm
                  ON vsm.VENDOR_ID = v.VENDOR_ID
            JOIN dbo.STATE s
                  ON S.STATE_ID = vsm.STATE_ID
            JOIN dbo.COUNTRY ctry
                  ON ctry.COUNTRY_ID = s.COUNTRY_ID
            JOIN dbo.VENDOR_COMMODITY_MAP vcm
                  ON vcm.VENDOR_ID = v.VENDOR_ID
            JOIN dbo.Commodity com
                  ON com.Commodity_Id = vcm.COMMODITY_TYPE_ID
            LEFT JOIN dbo.Utility_Dtl_Transport udt
                  ON udt.Vendor_Commodity_Map_Id = vcm.VENDOR_COMMODITY_MAP_ID
            LEFT JOIN dbo.Code c
                  ON c.Code_Id = udt.Industrial_Flag_Cd
            LEFT JOIN dbo.Code c1
                  ON c1.Code_Id = udt.Commercial_Flag_Cd
            LEFT JOIN dbo.Comment comm
                  ON comm.Comment_ID = udt.Comment_ID  
  --Industrial  
            LEFT JOIN ( SELECT
                              uv.Vendor_Commodity_Map_Id
                             ,usq.Question_Label
                             ,uv.Other_Req
                             ,uv.Utility_Volume_Dsc
                             ,c1.Code_Value
                             ,qcm.Commodity_Id
                             ,uv.Time_Period
                             ,uv.Time_Req
                        FROM
                              dbo.Utility_Dtl_Volume_Requirement uv
                              LEFT JOIN dbo.Code c1
                                    ON c1.Code_Id = uv.Utility_UOM_Cd
                              JOIN dbo.Question_Commodity_Map qcm
                                    ON qcm.Question_Commodity_Map_Id = uv.Question_Commodity_Map_Id
                              JOIN dbo.Utility_Summary_Question usq
                                    ON usq.Utility_Summary_Question_Id = qcm.Utility_Summary_Question_Id
                        WHERE
                              usq.Question_Label IN ( 'industrial', 'Commercial', 'Supplier Restrictions', 'Utility Restrictions' ) ) q
                  ON q.Vendor_Commodity_Map_Id = vcm.VENDOR_COMMODITY_MAP_ID
                     AND q.Commodity_Id = vcm.COMMODITY_TYPE_ID
            LEFT JOIN ( SELECT
                              ut.Vendor_Commodity_Map_Id
                             ,usq.Question_Label
                             ,qcm.Commodity_Id
                             ,c.Code_Value [Com Telemetering]
                             ,c1.Code_Value [Ind Telemetering]
                             ,comm.Comment_Text [Telemetering Comments]
                        FROM
                              dbo.Utility_Dtl_Telemetering ut
                              JOIN dbo.Question_Commodity_Map qcm
                                    ON qcm.Question_Commodity_Map_Id = ut.Question_Commodity_Map_Id
                              JOIN dbo.Utility_Summary_Question usq
                                    ON usq.Utility_Summary_Question_Id = qcm.Utility_Summary_Question_Id
                              JOIN dbo.Code c
                                    ON c.Code_Id = ut.Commercial_Flag_Cd
                              JOIN dbo.Code c1
                                    ON c1.Code_Id = ut.Industrial_Flag_Cd
                              LEFT JOIN dbo.Comment comm
                                    ON comm.Comment_ID = ut.Comment_ID
                        WHERE
                              usq.Question_Label IN ( 'Is telemetering required to transport?', 'Is there an additional cost?', 'How are meter readings obtained? (website, LDC software, etc.)', 'Online bill information availability?' ) ) q4
                  ON q4.Vendor_Commodity_Map_Id = vcm.VENDOR_COMMODITY_MAP_ID
                     AND q4.Commodity_Id = vcm.COMMODITY_TYPE_ID
            LEFT JOIN ( SELECT
                              ul.Vendor_Commodity_Map_Id
                             ,usq.Question_Label
                             ,qcm.Commodity_Id
                             ,c.Code_Value [Commercial]
                             ,c1.Code_Value [Industrial]
                             ,comm.Comment_Text [Comments]
                        FROM
                              dbo.Utility_Dtl_Service_Level ul
                              JOIN dbo.Question_Commodity_Map qcm
                                    ON qcm.Question_Commodity_Map_Id = ul.Question_Commodity_Map_Id
                              JOIN dbo.Utility_Summary_Question usq
                                    ON usq.Utility_Summary_Question_Id = qcm.Utility_Summary_Question_Id
                              JOIN dbo.Code c
                                    ON c.Code_Id = ul.Commercial_Flag_Cd
                              JOIN dbo.Code c1
                                    ON c1.Code_Id = ul.Industrial_Flag_Cd
                              LEFT JOIN dbo.Comment comm
                                    ON comm.Comment_ID = ul.Comment_ID
                        WHERE
                              usq.Question_Label IN ( 'Does utility offer firm & interruptible transport rates?', 'What are penalties if customer does not interrupt when called?', 'Is cogen required for interruptible transport?', 'Does utility allow customers to switch between tariff and transport on a monthly basis?', 'If this is rate specific, what rate schedule applies to this option?', 'Ability to hedge through the utility?' ) ) q8
                  ON q8.Vendor_Commodity_Map_Id = vcm.VENDOR_COMMODITY_MAP_ID
                     AND q8.Commodity_Id = vcm.COMMODITY_TYPE_ID
            LEFT JOIN ( SELECT
                              un.Vendor_Commodity_Map_Id
                             ,usq.Question_Label
                             ,qcm.Commodity_Id
                             ,comm.Comment_Text [Comments]
                        FROM
                              dbo.Utility_Dtl_Notification un
                              JOIN dbo.Question_Commodity_Map qcm
                                    ON qcm.Question_Commodity_Map_Id = un.Question_Commodity_Map_Id
                              JOIN dbo.Utility_Summary_Question usq
                                    ON usq.Utility_Summary_Question_Id = qcm.Utility_Summary_Question_Id
                              LEFT JOIN dbo.Comment comm
                                    ON comm.Comment_ID = un.Comment_ID
                        WHERE
                              usq.Question_Label IN ( 'Nominations', 'Rate Changes', 'Marketer Changes', 'Contract Changes' ) ) q14
                  ON q14.Vendor_Commodity_Map_Id = vcm.VENDOR_COMMODITY_MAP_ID
                     AND q14.Commodity_Id = vcm.COMMODITY_TYPE_ID
      WHERE
            vcm.COMMODITY_TYPE_ID = @Commodity_Id_EP
            AND v.VENDOR_TYPE_ID = @Vendor_Type_Id
      GROUP BY
            Vendor_Name
           ,STATE_NAME
           ,COUNTRY_NAME
           ,v.VENDOR_ID
           ,Comm.Comment_Text
           ,c.Code_Value
           ,c1.Code_Value
      ORDER BY
            Utility
           ,[State]
           ,COUNTRY  
      
       
END;

;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Utility_Details_EP_All] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_Utility_Details_EP_All] TO [CBMSReports]
GO
