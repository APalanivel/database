SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Rfp_Cloned_Dtls_Sel_By_Sr_Rfp_Id]
           
DESCRIPTION:             
			To get base RFP(s) of the cloned RFP

INPUT PARAMETERS:            
	Name				DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Sr_Rfp_Id		INT
    

OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  


USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
	BEGIN TRANSACTION
		SELECT * FROM dbo.Sr_Rfp WHERE SR_RFP_ID = 9999
		EXEC dbo.Sr_Rfp_Base_Sr_Rfp_Id_Upd  9999,999
		SELECT * FROM dbo.Sr_Rfp WHERE SR_RFP_ID = 9999
		EXEC dbo.Sr_Rfp_Cloned_Dtls_Sel_By_Sr_Rfp_Id  9999
	ROLLBACK TRANSACTION
	
	BEGIN TRANSACTION
		EXEC dbo.Sr_Rfp_Base_Sr_Rfp_Id_Upd  99,9
		EXEC dbo.Sr_Rfp_Base_Sr_Rfp_Id_Upd  999,99
		EXEC dbo.Sr_Rfp_Base_Sr_Rfp_Id_Upd  9999,999
		EXEC dbo.Sr_Rfp_Cloned_Dtls_Sel_By_Sr_Rfp_Id  9
		EXEC dbo.Sr_Rfp_Cloned_Dtls_Sel_By_Sr_Rfp_Id  99
		EXEC dbo.Sr_Rfp_Cloned_Dtls_Sel_By_Sr_Rfp_Id  999
		EXEC dbo.Sr_Rfp_Cloned_Dtls_Sel_By_Sr_Rfp_Id  9999
	ROLLBACK TRANSACTION
	
		
AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-04-05	Global Sourcing - Phase3 - GCS-521 Created
******/
CREATE PROCEDURE [dbo].[Sr_Rfp_Cloned_Dtls_Sel_By_Sr_Rfp_Id] ( @Sr_Rfp_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;

      DECLARE
            @Child_Sum INT
           ,@Child_Count INT

      DECLARE @Rfp_Oder TABLE
            ( 
             SR_RFP_ID INT
            ,Base_Sr_Rfp_Id INT
            ,Ranking INT );
      WITH  Cte_Rfp_Parent ( Sr_Rfp_Id, Base_Sr_Rfp_Id, Ranking, Last_Child )
              AS ( SELECT
                        SR_RFP_ID
                       ,Base_Sr_Rfp_Id
                       ,1 AS Ranking
                       ,SR_RFP_ID AS Last_Child
                   FROM
                        dbo.SR_RFP
                   WHERE
                        SR_RFP_ID = @Sr_Rfp_Id
                   UNION ALL
                   SELECT
                        base.SR_RFP_ID
                       ,base.Base_Sr_Rfp_Id
                       ,clone.Ranking + 1 AS Ranking
                       ,base.SR_RFP_ID AS Last_Child
                   FROM
                        dbo.SR_RFP base
                        INNER JOIN Cte_Rfp_Parent clone
                              ON clone.Base_Sr_Rfp_Id = base.SR_RFP_ID )
            INSERT      INTO @Rfp_Oder
                        ( 
                         SR_RFP_ID
                        ,Base_Sr_Rfp_Id
                        ,Ranking )
                        SELECT
                              SR_RFP_ID
                             ,Base_Sr_Rfp_Id
                             ,Ranking
                        FROM
                              Cte_Rfp_Parent
            
      SELECT
            @Child_Sum = min(Ranking) + max(Ranking)
           ,@Child_Count = count(SR_RFP_ID)
      FROM
            @Rfp_Oder


      UPDATE
            @Rfp_Oder
      SET   
            Ranking = @Child_Sum - Ranking;

      WITH  Cte_Rfp_Child ( Sr_Rfp_Id, Base_Sr_Rfp_Id, Ranking, Last_Child )
              AS ( SELECT
                        SR_RFP_ID
                       ,Base_Sr_Rfp_Id
                       ,1 AS Ranking
                       ,SR_RFP_ID AS Last_Child
                   FROM
                        dbo.SR_RFP
                   WHERE
                        Base_Sr_Rfp_Id = @Sr_Rfp_Id
                   UNION ALL
                   SELECT
                        base.SR_RFP_ID
                       ,base.Base_Sr_Rfp_Id
                       ,clone.Ranking + 1 AS Ranking
                       ,base.SR_RFP_ID AS Last_Child
                   FROM
                        dbo.SR_RFP base
                        INNER JOIN Cte_Rfp_Child clone
                              ON base.Base_Sr_Rfp_Id = clone.SR_RFP_ID )
            INSERT      INTO @Rfp_Oder
                        ( 
                         SR_RFP_ID
                        ,Base_Sr_Rfp_Id
                        ,Ranking )
                        SELECT
                              SR_RFP_ID
                             ,Base_Sr_Rfp_Id
                             ,Ranking + @Child_Count AS Ranking
                        FROM
                              Cte_Rfp_Child
  
               
      SELECT
            Ranking AS Rfp_Round
           ,SR_RFP_ID
           ,Base_Sr_Rfp_Id
      FROM
            @Rfp_Oder
      ORDER BY
            Ranking ASC
            
END;



;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Cloned_Dtls_Sel_By_Sr_Rfp_Id] TO [CBMSApplication]
GO
