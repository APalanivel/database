SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:    Trade.Deal_Ticket_Dtls_Sel_By_Deal_Ticket_Id  
     
      
DESCRIPTION:     
     
  This procedure to get summary page details for deal ticket summery.  
      
INPUT PARAMETERS:      
      Name          DataType       Default        Description      
-----------------------------------------------------------------------------      
 @Deal_Ticket_Id   INT  
   
    
      
OUTPUT PARAMETERS:    
      
 Name     DataType   Default  Description      
-----------------------------------------------------------------------------      
      
      
      
USAGE EXAMPLES:      
-----------------------------------------------------------------------------      
   
 Exec Trade.Deal_Ticket_Dtls_Sel_By_Deal_Ticket_Id  646  
 Exec Trade.Deal_Ticket_Dtls_Sel_By_Deal_Ticket_Id  198  
 Exec Trade.Deal_Ticket_Dtls_Sel_By_Deal_Ticket_Id  212  
 Exec Trade.Deal_Ticket_Dtls_Sel_By_Deal_Ticket_Id  131861  
 Exec Trade.Deal_Ticket_Dtls_Sel_By_Deal_Ticket_Id  131477
         
AUTHOR INITIALS:       
 Initials    Name  
-----------------------------------------------------------------------------         
 SP          SrinivasaRao Patchava      
      
MODIFICATIONS       
 Initials    Date        Modification        
-----------------------------------------------------------------------------         
 SP          2018-11-21  Global Risk Management - Create sp to get summary page details for deal ticket summery  
       
      
******/

CREATE PROCEDURE [Trade].[Deal_Ticket_Dtls_Sel_By_Deal_Ticket_Id]
    (
        @Deal_Ticket_Id INT
        , @Client_Hier_Id VARCHAR(MAX) = NULL
        , @Trade_Id INT = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Country VARCHAR(MAX) = ''
            , @CounterParty VARCHAR(MAX) = ''
            , @Supplier VARCHAR(MAX) = ''
            , @Status VARCHAR(200) = ''
            , @Tasks VARCHAR(200) = ''
            , @Site_Count VARCHAR(200) = ''
            , @Trade_CounterParty VARCHAR(MAX) = ''
            , @Trade_Supplier VARCHAR(MAX) = '';



        SELECT
            @Country = @Country + ch.Country_Name + ','
        FROM
            Core.Client_Hier ch
            INNER JOIN Trade.Deal_Ticket_Client_Hier vdtl
                ON ch.Client_Hier_Id = vdtl.Client_Hier_Id
        WHERE
            vdtl.Deal_Ticket_Id = @Deal_Ticket_Id
        GROUP BY
            ch.Country_Name;




        SELECT
            @CounterParty = @CounterParty + rmcp.COUNTERPARTY_NAME + '|'
        FROM
            Trade.Deal_Ticket_Financial_Counter_Party_Contact dtcpc
            INNER JOIN Trade.Financial_Counterparty_Client_Contact_Map cccm
                ON dtcpc.Financial_Counterparty_Client_Contact_Map_Id = cccm.Financial_Counterparty_Client_Contact_Map_Id
            INNER JOIN dbo.RM_COUNTERPARTY rmcp
                ON cccm.RM_COUNTERPARTY_ID = rmcp.RM_COUNTERPARTY_ID
        WHERE
            dtcpc.Deal_Ticket_Id = @Deal_Ticket_Id
        GROUP BY
            rmcp.COUNTERPARTY_NAME;


        SELECT
            @Supplier = @Supplier + cha.Account_Vendor_Name + '|'
        FROM
            Trade.Deal_Ticket_Contract dtc
            INNER JOIN Core.Client_Hier_Account cha
                ON dtc.CONTRACT_ID = cha.Supplier_Contract_ID
        WHERE
            dtc.Deal_Ticket_Id = @Deal_Ticket_Id
        GROUP BY
            cha.Account_Vendor_Name;

        SELECT
            @Trade_CounterParty = @Trade_CounterParty + rmcp.COUNTERPARTY_NAME + ','
        FROM
            Trade.Deal_Ticket_Client_Hier_TXN_Status dtchts
            INNER JOIN dbo.RM_COUNTERPARTY rmcp
                ON dtchts.Cbms_Counterparty_Id = rmcp.RM_COUNTERPARTY_ID
        WHERE
            dtchts.Cbms_Trade_Number = @Trade_Id
        GROUP BY
            rmcp.COUNTERPARTY_NAME;


        SELECT
            @Trade_Supplier = @Trade_Supplier + rmcp.VENDOR_NAME + ','
        FROM
            Trade.Deal_Ticket_Client_Hier_TXN_Status dtchts
            INNER JOIN dbo.VENDOR rmcp
                ON dtchts.Cbms_Counterparty_Id = rmcp.VENDOR_ID
        WHERE
            dtchts.Cbms_Trade_Number = @Trade_Id
        GROUP BY
            rmcp.VENDOR_NAME;

        --SELECT
        --      @Status = @Status + ws.Workflow_Status_Name + ', '
        --     ,@Site_Count = CAST(COUNT(DISTINCT dtch.Deal_Ticket_Client_Hier_Id) AS VARCHAR(20))
        --FROM
        --      Trade.Deal_Ticket_Client_Hier dtch
        --      INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
        --            ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
        --      INNER JOIN Trade.Workflow_Status_Map wsm
        --            ON chws.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
        --   INNER JOIN Trade.Workflow_Status ws
        --            ON wsm.Workflow_Status_Id = ws.Workflow_Status_Id
        --WHERE
        --      dtch.Deal_Ticket_Id = @Deal_Ticket_Id
        --      AND chws.Is_Active = 1
        --      AND ( @Client_Hier_Id IS NULL
        --            OR EXISTS ( SELECT
        --                              1
        --                        FROM
        --                              dbo.ufn_split(@Client_Hier_Id, ',')
        --                        WHERE
        --                              CAST(Segments AS INT) = dtch.Client_Hier_Id ) )
        --GROUP BY
        --      ws.Workflow_Status_Name;

        --SELECT
        --      @Tasks = @Tasks + tsk.Task_Name + ' | '
        --FROM
        --      Trade.Deal_Ticket_Client_Hier dtch
        --      INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
        --            ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
        --      INNER JOIN Trade.Workflow_Status_Map wsm
        --            ON chws.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
        --      INNER JOIN Trade.Workflow_Task_Status_Map tsm
        --            ON tsm.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
        --      INNER JOIN Trade.Workflow_Task tsk
        --            ON tsm.Workflow_Task_Id = tsk.Workflow_Task_Id
        --WHERE
        --      dtch.Deal_Ticket_Id = @Deal_Ticket_Id
        --      AND chws.Is_Active = 1
        --GROUP BY
        --      tsk.Task_Name;

        SELECT
            dt.Deal_Ticket_Id
            , ISNULL(STUFF(@Status, LEN(@Status), 1, ''), '') AS Deal_Ticket_Status
            , dt.Deal_Ticket_Number
            , ch.Client_Name
            , STUFF(@Country, LEN(@Country), 1, '') AS Country_name
            , c.Commodity_Name
            , ehed.ENTITY_NAME AS Hedge_Type
            , ta.Code_Value + ' ' + dtt.Code_Value AS Trade_Action_Transaction
            , idx.ENTITY_NAME AS Price_Index
            , prix.PRICING_POINT
            , cfrqcd.Code_Value AS Deal_Ticket_Frequency
            , eum.ENTITY_NAME AS Uom
            , CASE WHEN dt.Is_Client_Generated = 1 THEN 'Yes'
                  WHEN dt.Is_Client_Generated = 0 THEN 'No'
              END AS Client_Generated
            , applyprice.Code_Value AS Apply_Price
            , CASE WHEN dt.Is_Individual_Site_Pricing_Required = 1 THEN 'Yes'
                  WHEN dt.Is_Individual_Site_Pricing_Required = 0 THEN 'No'
              END AS Individual_Site_Pricing
            , wf.Workflow_Id
            , wf.Workflow_Name
            , wf.Workflow_Description
            , cu.CURRENCY_UNIT_NAME AS Currency
            , CASE WHEN LEN(@CounterParty) > 1 THEN STUFF(@CounterParty, LEN(@CounterParty), 1, '')
                  WHEN LEN(@Supplier) > 1 THEN STUFF(@Supplier, LEN(@Supplier), 1, '')
                  ELSE NULL
              END Supplier_Counterparty
            , ISNULL(STUFF(@Tasks, LEN(@Tasks), 1, ''), 'Tasks') AS Tasks
            , @Site_Count AS Site_Count
            , CASE WHEN COUNT(DISTINCT chsite.Site_Id) > 1 THEN 'Multiple'
                  ELSE MAX(RTRIM(chsite.City) + ', ' + chsite.State_Name + ' (' + chsite.Site_name + ')')
              END AS Site_Name
            , SUBSTRING(DATENAME(MONTH, dt.Hedge_Start_Dt), 1, 3) + ' '
              + CAST(DATEPART(YEAR, dt.Hedge_Start_Dt) AS VARCHAR) AS Hedge_Start_Month
            , SUBSTRING(DATENAME(MONTH, dt.Hedge_End_Dt), 1, 3) + ' '
              + CAST(DATEPART(YEAR, dt.Hedge_End_Dt) AS VARCHAR) AS Hedge_End_Month
            , iniui.FIRST_NAME + ' ' + iniui.LAST_NAME AS Initiated_By
            , iniui.EMAIL_ADDRESS AS Initiator_Email_Address
            , currq.FIRST_NAME + ' ' + currq.LAST_NAME AS Currently_With
            , REPLACE(CONVERT(VARCHAR(20), dt.Created_Ts, 106), ' ', '-') AS Date_Initiated
            , ISNULL(dt.Is_Bid, 0) AS Is_Bid
            , e.ENTITY_NAME AS Hedge_Mode_Type
            , c2.Code_Value AS Trade_Action_Type
            , c3.Code_Value AS Hedge_Allocation_Type
            , c4.Code_Value AS Deal_Ticket_Type
            , dt.Uom_Type_Id
            , dt.Hedge_Type_Cd
            , dt.Hedge_Mode_Type_Id
            , dt.Price_Index_Id
            , dt.Commodity_Id
            , CASE WHEN rdt.RM_DEAL_TICKET_ID IS NOT NULL THEN 1
                  ELSE 0
              END AS Is_Old_Deal_Ticket
            , CASE WHEN LEN(@Trade_CounterParty) > 1 THEN STUFF(@Trade_CounterParty, LEN(@Trade_CounterParty), 1, '')
                  WHEN LEN(@Trade_Supplier) > 1 THEN STUFF(@Trade_Supplier, LEN(@Trade_Supplier), 1, '')
                  ELSE NULL
              END Trade_Counterparty
            , dt.Client_Id
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN Core.Client_Hier ch
                ON dt.Client_Id = ch.Client_Id
            INNER JOIN dbo.Commodity c
                ON c.Commodity_Id = dt.Commodity_Id
            INNER JOIN dbo.ENTITY ehed
                ON ehed.ENTITY_ID = dt.Hedge_Type_Cd
            LEFT JOIN dbo.Code cstus
                ON cstus.Code_Id = dt.Deal_Status_Cd
            INNER JOIN dbo.PRICE_INDEX prix
                ON prix.PRICE_INDEX_ID = dt.Price_Index_Id
            INNER JOIN dbo.ENTITY idx
                ON prix.INDEX_ID = idx.ENTITY_ID
            INNER JOIN dbo.Code cfrqcd
                ON cfrqcd.Code_Id = dt.Deal_Ticket_Frequency_Cd
            INNER JOIN dbo.ENTITY eum
                ON dt.Uom_Type_Id = eum.ENTITY_ID
            INNER JOIN dbo.Code applyprice
                ON applyprice.Code_Id = dt.Trade_Pricing_Option_Cd
            INNER JOIN dbo.Code ta
                ON ta.Code_Id = dt.Trade_Action_Type_Cd
            INNER JOIN dbo.Code dtt
                ON dtt.Code_Id = dt.Deal_Ticket_Type_Cd
            INNER JOIN dbo.Workflow wf
                ON dt.Workflow_Id = wf.Workflow_Id
            LEFT JOIN dbo.CURRENCY_UNIT cu
                ON cu.CURRENCY_UNIT_ID = dt.Currency_Unit_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dtch.Deal_Ticket_Id = dt.Deal_Ticket_Id
            INNER JOIN Core.Client_Hier chsite
                ON chsite.Client_Id = dt.Client_Id
                   AND  chsite.Client_Hier_Id = dtch.Client_Hier_Id
            LEFT JOIN dbo.USER_INFO iniui
                ON iniui.USER_INFO_ID = dt.Created_User_Id
            LEFT JOIN dbo.USER_INFO currq
                ON currq.QUEUE_ID = dt.Queue_Id
            INNER JOIN dbo.ENTITY e
                ON e.ENTITY_ID = dt.Hedge_Mode_Type_Id
            INNER JOIN dbo.Code c2
                ON c2.Code_Id = dt.Trade_Action_Type_Cd
            INNER JOIN dbo.Code c3
                ON dt.Hedge_Allocation_Type_Cd = c3.Code_Id
            INNER JOIN dbo.Code c4
                ON dt.Deal_Ticket_Type_Cd = c4.Code_Id
            LEFT JOIN dbo.RM_DEAL_TICKET rdt
                ON rdt.RM_DEAL_TICKET_ID = dt.Deal_Ticket_Id
        WHERE
            dt.Deal_Ticket_Id = @Deal_Ticket_Id
            AND ch.Sitegroup_Id = 0
            AND (   @Client_Hier_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Client_Hier_Id, ',')
                                   WHERE
                                        CAST(Segments AS INT) = dtch.Client_Hier_Id))
        GROUP BY
            dt.Deal_Ticket_Id
            , dt.Deal_Ticket_Number
            , ch.Client_Name
            , c.Commodity_Name
            , ehed.ENTITY_NAME
            , ta.Code_Value + ' ' + dtt.Code_Value
            , idx.ENTITY_NAME
            , prix.PRICING_POINT
            , cfrqcd.Code_Value
            , eum.ENTITY_NAME
            , CASE WHEN dt.Is_Client_Generated = 1 THEN 'Yes'
                  WHEN dt.Is_Client_Generated = 0 THEN 'No'
              END
            , applyprice.Code_Value
            , CASE WHEN dt.Is_Individual_Site_Pricing_Required = 1 THEN 'Yes'
                  WHEN dt.Is_Individual_Site_Pricing_Required = 0 THEN 'No'
              END
            , wf.Workflow_Id
            , wf.Workflow_Name
            , wf.Workflow_Description
            , cu.CURRENCY_UNIT_NAME
            , dt.Hedge_Start_Dt
            , dt.Hedge_End_Dt
            , iniui.FIRST_NAME + ' ' + iniui.LAST_NAME
            , iniui.EMAIL_ADDRESS
            , currq.FIRST_NAME + ' ' + currq.LAST_NAME
            , dt.Created_Ts
            , ISNULL(dt.Is_Bid, 0)
            , e.ENTITY_NAME
            , c2.Code_Value
            , c3.Code_Value
            , c4.Code_Value
            , dt.Uom_Type_Id
            , dt.Hedge_Type_Cd
            , dt.Hedge_Mode_Type_Id
            , dt.Price_Index_Id
            , dt.Commodity_Id
            , CASE WHEN rdt.RM_DEAL_TICKET_ID IS NOT NULL THEN 1
                  ELSE 0
              END
            , dt.Client_Id;




    END;















GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Dtls_Sel_By_Deal_Ticket_Id] TO [CBMSApplication]
GO
