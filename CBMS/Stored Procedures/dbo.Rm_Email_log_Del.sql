SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: DBO.Rm_Email_log_Del  

DESCRIPTION: It Deletes Rm Email log for Selected Rm Email Log Id.
      
INPUT PARAMETERS:          
	NAME					DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Rm_Email_Log_Id		INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN
		EXEC dbo.rm_email_log_del  13683
	ROLLBACK TRAN
	
	SELECT
		Rm_Email_Log_Id
	FROM
		dbo.rm_email_log
	WHERE
		rm_deal_ticket_id = 106703

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			31-AUG-10	CREATED

*/

CREATE PROCEDURE dbo.Rm_Email_log_Del
    (
      @Rm_Email_Log_Id	INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.Rm_Email_log
	WHERE
		Rm_Email_Log_Id = @Rm_Email_Log_Id
		
END
GO
GRANT EXECUTE ON  [dbo].[Rm_Email_log_Del] TO [CBMSApplication]
GO
