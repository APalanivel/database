SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE [dbo].[cbmsLookup_GetByTypeAndName]
	( @MyAccountId int
	, @entity_type int
	, @entity_name varchar(200)
	)
AS
BEGIN

	   select entity_id
		, entity_name
		, entity_type
		, entity_description
	     from entity
	    where entity_type = @entity_type
	      and entity_name = @entity_name


END
GO
GRANT EXECUTE ON  [dbo].[cbmsLookup_GetByTypeAndName] TO [CBMSApplication]
GO
