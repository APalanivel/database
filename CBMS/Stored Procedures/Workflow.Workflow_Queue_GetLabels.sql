SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
    
    
/******            
NAME:    [Workflow].[Workflow_Queue_GetLabels]        
DESCRIPTION:  The Stored Procedure will Return the Lable Information  from dbo.cu_invoice_label      
------------------------------------------------------------           
 INPUT PARAMETERS:            
 Name   DataType  Default Description          
 @MyAccountId int            
 @cu_invoice_id int          
------------------------------------------------------------            
 OUTPUT PARAMETERS:            
 Name   DataType  Default Description            
------------------------------------------------------------            
 USAGE EXAMPLES:             
 EXEC Workflow.Workflow_Queue_GetLabels @MyAccountId = 0,  -- int      
                                        @cu_invoice_id = 0 -- int      
------------------------------------------------------------            
AUTHOR INITIALS:            
Initials Name            
------------------------------------------------------------            
 AKP Arun Kumnar  Summit Energy         
         
 MODIFICATIONS             
 Initials Date   Modification            
------------------------------------------------------------            
 AKP SEP-2019 Created        
        
******/        
CREATE    PROCEDURE [Workflow].[Workflow_Queue_GetLabels]                        
 ( @MyAccountId int                        
 , @cu_invoice_id int                        
 )                        
AS                        
BEGIN                        
                        
  SELECT                 
   cl.cu_invoice_label_id                        
  ,cl.cu_invoice_id                        
  ,cl.account_number  AS [Account Number]                      
  ,cl.client_name  AS [Client]                                   
  ,cl.state_name  AS [State]                      
  ,cl.vendor_name  AS [Vendor]                
  ,cl.COUNTRY AS [Country]                
  ,cl.COMMODITY AS [Commodity]                           
  ,cl.VENDOR_TYPE AS [Vendor Type]          
  ,cl.Site_Name AS [Site]                
 FROM cu_invoice_label cl WITH (NOLOCK)                        
 WHERE cl.cu_invoice_id = @cu_invoice_id                        
                        
END             
GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_GetLabels] TO [CBMSApplication]
GO
