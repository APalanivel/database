SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.SR_RFP_LP_GET_INTERVAL_DATA_P
	@user_id varchar(10),
	@session_id varchar(20),
	@sr_rfp_account_id int
	AS
	set nocount on
	select 	ird.sr_rfp_lp_interval_data_id,
		ird.interval_name,
		cbms_img.cbms_doc_id,
		cbms_img.cbms_image_id

	from 	sr_rfp_lp_interval_data ird(nolock),
		cbms_image cbms_img(nolock),
		sr_rfp_account rfp_account(nolock)
	where	ird.sr_rfp_account_id = @sr_rfp_account_id
		and rfp_account.sr_rfp_account_id = ird.sr_rfp_account_id
		and rfp_account.is_deleted = 0
		and ird.cbms_image_id =  cbms_img.cbms_image_id
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_LP_GET_INTERVAL_DATA_P] TO [CBMSApplication]
GO
