SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:   [Workflow].[Workflow_Queue_Load_SearchFilter]   
DESCRIPTION: To Retrive the Search Filter values   
      
 INPUT PARAMETERS:      
 Name			DataType  Default Description    
 @MODULE_ID		INT
------------------------------------------------------------      
   
 OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
  USAGE EXAMPLES:    EXEC [Workflow].[Workflow_Queue_Load_SearchFilter] @MODULE_ID=1  
------------------------------------------------------------      
 AUTHOR INITIALS:      
 Initials Name      
------------------------------------------------------------      
TRK   Ramakrishna Thummala Summit Energy   
   
 MODIFICATIONS       
   
 Initials Date   Modification      
-----------------------------------------------------------      
 SEP-2019  Ramakrishna Thummala Initial Creation  
 ***********/     
CREATE PROC [Workflow].[Workflow_Queue_Load_SearchFilter]                     
   (                      
  @MODULE_ID INT                      
   )                      
AS                      
BEGIN                      
                     
 SELECT                                 
 qst.Workflow_Queue_Search_Filter_Id AS SearchFilters_Id,                    
 sf.Filter_Name AS Control_Name,                    
 c.Code_Dsc AS Control_Type,                    
 qst.Display_Seq AS List_Order,                     
 qst.Is_Default AS Default_Display,          
 s.App_Data_Load_Service_Method AS ServiceMethod,                    
 qst.Is_Default AS IsFixed,                    
 qst.Is_Smart_Search_Filter AS IsSmartSearch,           
 qst.Data_Source_Query AS [DATA_SOURCE],                 
 qst.CSS AS CSSClass ,           
 qst.CanDropdownSearchFilter AS canDropdownSearchFilter              
 FROM  [Workflow].workflow_queue_search_filter qst            
 LEFT JOIN [Workflow].app_data_load_service s ON qst.app_data_load_service_id = s.app_data_load_service_id          
 INNER JOIN [Workflow].search_filter sf ON  qst.search_filter_id = sf.search_filter_id          
 INNER JOIN dbo.code c ON qst.filter_type_cd = c.Code_Id          
  WHERE qst.Workflow_Queue_Id = @MODULE_ID                    
  AND qst.IS_ACTIVE = 1                    
 ORDER BY qst.Display_Seq ASC               
                
END;          
          
                      
        
            
GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_Load_SearchFilter] TO [CBMSApplication]
GO
