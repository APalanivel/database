SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.BUDGET_GET_SERVICE_LEVEL_LIST_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

--exec dbo.BUDGET_GET_SERVICE_LEVEL_LIST_P '' ,''
--select * from entity where entity_name like 'A'
--select * from entity where entity_type=708

CREATE	PROCEDURE dbo.BUDGET_GET_SERVICE_LEVEL_LIST_P
	
	AS
begin
		set nocount on
		
		select	entity_id ,
			entity_name 
		
		from	entity 
		where entity_type=708


		end
GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_SERVICE_LEVEL_LIST_P] TO [CBMSApplication]
GO
