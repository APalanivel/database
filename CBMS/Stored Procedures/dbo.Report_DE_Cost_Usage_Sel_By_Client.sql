
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
NAME:   Report_DE_Cost_Usage_Sel_By_Client           
    
        
DESCRIPTION:              
 Gets a list of missing invoices based on invoice participation     
          
INPUT PARAMETERS:              
Name			DataType			Default				Description              
----------------------------------------------------------------------------              
@Client_Id		INT  
@Start_Dt		DATE  
@End_Dt			DATE  
                    
OUTPUT PARAMETERS:              
Name			DataType			Default				Description              
----------------------------------------------------------------------------              
    
             
USAGE EXAMPLES:              
----------------------------------------------------------------------------              
    
  
EXEC Report_DE_Cost_Usage_Sel_By_Client 
      235
     ,'2012-01-01'
     ,'2013-01-01'      
    
EXEC dbo.Report_DE_Cost_Usage_Sel_By_Client   
      11472  
     ,'2012-01-01'  
     ,'2012-03-01'    
    
AUTHOR INITIALS:              
Initials		Name              
----------------------------------------------------------------------------              
AKR				Ashok Kumar Raju  
NR				Narayana Reddy
         
MODIFICATIONS               
Initials	Date			Modification              
----------------------------------------------------------------------------              
AKR			2013-12-02		Created  
NR			2016-08-30		REPTMGR-66 -Default UOM in the temp table is changed VARCHAR(10) to VARCHAR(100).
*****/    
CREATE PROCEDURE [dbo].[Report_DE_Cost_Usage_Sel_By_Client]
      ( 
       @Client_List INT
      ,@Start_Dt DATE
      ,@End_Dt DATE )
AS 
BEGIN            
            
      CREATE TABLE #Account_List
            ( 
             Account_Id INT
            ,Service_Month DATE
            ,Commodity_Id INT
            ,Billing_Data VARCHAR(20)
            ,Billing_data_Type VARCHAR(20)
            ,Data VARCHAR(10) )            
             
      DECLARE
            @Client_Currency_Group_Id INT
           ,@Currency_Unit_Id INT
      
      
      SELECT
            @Currency_Unit_Id = cu.Currency_Unit_Id
      FROM
            dbo.Currency_Unit cu
      WHERE
            currency_Unit_Name = 'USD'            
            
      SELECT
            @Client_Currency_Group_Id = ch.Client_Currency_Group_Id
      FROM
            core.Client_Hier ch
      WHERE
            Client_Id = @Client_List
            AND Site_Id = 0
            AND Sitegroup_Id = 0;             
               
      CREATE TABLE #CU_Data
            ( 
             Client_Name VARCHAR(200)
            ,Sitegroup_Name VARCHAR(200)
            ,Site_name VARCHAR(200)
            ,City VARCHAR(200)
            ,State_Name VARCHAR(200)
            ,Country_Name VARCHAR(200)
            ,Account_Vendor_Name VARCHAR(200)
            ,Display_Account_Number VARCHAR(500)
            ,Account_Id INT
            ,Account_Type CHAR(8)
            ,Consumption_Level_Desc VARCHAR(100)
            ,[DEO Status] VARCHAR(3)
            ,[Bucket Type] VARCHAR(20)
            ,Bucket_Master_Id INT
            ,Bucket_Name VARCHAR(255)
            ,Client_Hier_Id INT
            ,Service_Month DATE
            ,Commodity_Name VARCHAR(50)
            ,Commodity_Id INT
            ,[Converted Bucket Value] VARCHAR(40)
            ,[Default UOM] VARCHAR(100)
            ,Sort_Order INT
            ,Sort_Type INT );          
     
      INSERT      INTO #Account_List
                  ( 
                   Account_Id
                  ,Service_Month
                  ,Commodity_Id
                  ,Billing_Data
                  ,Billing_data_Type
                  ,Data )
                  SELECT
                        Account_Id
                       ,Service_Month
                       ,Commodity_Id
                       ,'Billing Data'
                       ,BillingType
                       ,BillingData
                  FROM
                        ( SELECT
                              cha.Account_Id
                             ,cubd.Service_Month
                             ,cha.Commodity_Id
                             ,CONVERT(VARCHAR(10), ISNULL(cubd.Billing_Start_Dt, dd.FIRST_DAY_OF_MONTH_D), 101) AS Billing_Start_Dt
                             ,CONVERT(VARCHAR(10), ISNULL(cubd.Billing_End_Dt, dd.LAST_DAY_OF_MONTH_D), 101) AS Billing_End_Dt
                             ,CAST(ISNULL(cubd.Billing_Days, dd.DAYS_IN_MONTH_NUM) AS VARCHAR(10)) AS Billing_Days
                          FROM
                              core.Client_Hier ch
                              JOIN core.Client_Hier_Account cha
                                    ON ch.Client_Hier_Id = cha.Client_Hier_Id
                              JOIN dbo.Cost_Usage_Account_Billing_Dtl cubd
                                    ON cubd.ACCOUNT_ID = cha.Account_Id
                              JOIN meta.DATE_DIM dd
                                    ON dd.DATE_D = cubd.Service_Month
                          WHERE
                              ch.Client_Id = @Client_List
                              AND cubd.SERVICE_MONTH BETWEEN @Start_Dt AND @End_Dt
                          GROUP BY
                              cha.Account_Id
                             ,cubd.Service_Month
                             ,cha.Commodity_Id
                             ,CONVERT(VARCHAR(10), ISNULL(cubd.Billing_Start_Dt, dd.FIRST_DAY_OF_MONTH_D), 101)
                             ,CONVERT(VARCHAR(10), ISNULL(cubd.Billing_End_Dt, dd.LAST_DAY_OF_MONTH_D), 101)
                             ,CAST(ISNULL(cubd.Billing_Days, dd.DAYS_IN_MONTH_NUM) AS VARCHAR(10)) ) up UNPIVOT           
                       ( BillingData FOR BillingType IN ( Billing_Start_Dt, Billing_End_Dt, Billing_Days ) )          
                       AS unpvt          
         
            
      CREATE CLUSTERED INDEX ix_cu_#AL ON #Account_List(Account_Id,Service_Month,Commodity_Id)          
            
         
      CREATE CLUSTERED INDEX ix_index_cu ON #CU_Data(Account_Id,Service_Month,Commodity_Id)          
              
      INSERT      INTO #CU_Data
                  ( 
                   Client_Name
                  ,Sitegroup_Name
                  ,Site_name
                  ,City
                  ,State_Name
                  ,Country_Name
                  ,Account_Vendor_Name
                  ,Display_Account_Number
                  ,Account_Id
                  ,Account_Type
                  ,Consumption_Level_Desc
                  ,[DEO Status]
                  ,[Bucket Type]
                  ,Bucket_Master_Id
                  ,Bucket_Name
                  ,Client_Hier_Id
                  ,Service_Month
                  ,Commodity_Name
                  ,Commodity_Id
                  ,[Converted Bucket Value]
                  ,[Default UOM]
                  ,Sort_Order
                  ,Sort_Type )
                  SELECT
                        Ch.Client_Name [Client Name]
                       ,Ch.Sitegroup_Name [Division]
                       ,Ch.Site_name [Site]
                       ,ch.City [City]
                       ,ch.State_Name [State]
                       ,ch.Country_Name [Country]
                       ,ca.Account_Vendor_Name [Vendor]
                       ,ca.Display_Account_Number [Account #]
                       ,ca.Account_Id [Account ID]
                       ,ca.Account_Type [Account Type]
                       ,vcl.Consumption_Level_Desc [Consumption Level]
                       ,[DEO Status] = CASE WHEN ca.Account_Is_Data_Entry_Only = 1 THEN 'DEO'
                                            ELSE 'No'
                                       END
                       ,[Bucket Type] = CASE WHEN c.Code_Value = 'Determinant' THEN 'Volume'
                                             ELSE 'Cost'
                                        END
                       ,bm.Bucket_Master_Id [BMID]
                       ,bm.Bucket_Name [Bucket]
                       ,cuad.Client_Hier_Id [CHID]
                       ,cuad.Service_Month [Month]
                       ,com.Commodity_Name [Commodity]
                       ,com.Commodity_Id [Commodity ID]
                       ,CAST(CASE WHEN c.Code_Value = 'Determinant' THEN CAST(cuad.Bucket_Value * cuc.Conversion_Factor AS DECIMAL(32, 2))
                                  WHEN c.Code_Value = 'Charge' THEN CAST(cuad.Bucket_Value * cuconv.Conversion_Factor AS DECIMAL(32, 2))
                             END AS VARCHAR(40)) AS [Converted Bucket Value]
                       ,[Default UOM] = CASE WHEN c.code_value = 'Charge' THEN 'USD'
                                             ELSE e1.ENTITY_NAME
                                        END
                       ,bm.Sort_Order
                       ,CASE WHEN c.Code_Value = 'Determinant' THEN 2
                             ELSE 3
                        END Sort_Type
                  FROM
                        core.Client_Hier ch
                        INNER JOIN core.Client_Hier_Account ca
                              ON ch.Client_Hier_Id = ca.Client_Hier_Id
                        INNER JOIN Cost_Usage_Account_Dtl cuad
                              ON ca.Account_Id = cuad.ACCOUNT_ID
                                 AND ca.Client_Hier_Id = cuad.Client_Hier_Id
                        JOIN Bucket_Master bm
                              ON bm.Bucket_Master_Id = cuad.Bucket_Master_Id
                        JOIN Commodity com
                              ON com.Commodity_Id = bm.Commodity_Id
                        INNER JOIN ( SELECT
                                          ACCOUNT_ID
                                         ,Commodity_Id
                                         ,Service_Month
                                     FROM
                                          #Account_List
                                     GROUP BY
                                          ACCOUNT_ID
                                         ,Commodity_Id
                                         ,Service_Month ) al
                              ON cuad.ACCOUNT_ID = al.Account_Id
                                 AND cuad.Service_Month = al.Service_Month
                                 AND com.Commodity_Id = al.Commodity_Id
                        JOIN Code c
                              ON c.Code_Id = bm.Bucket_Type_Cd
                        JOIN Account_Variance_Consumption_Level avcl
                              ON avcl.ACCOUNT_ID = ca.Account_Id
                        JOIN Variance_Consumption_Level vcl
                              ON vcl.Commodity_Id = com.Commodity_Id
                                 AND vcl.Variance_Consumption_Level_Id = avcl.Variance_Consumption_Level_Id
                        LEFT JOIN ENTITY e1
                              ON e1.ENTITY_ID = bm.Default_Uom_Type_Id
                        LEFT OUTER JOIN dbo.Consumption_Unit_Conversion cuc
                              ON cuc.Base_Unit_Id = cuad.Uom_Type_id
                                 AND cuc.Converted_Unit_Id = bm.Default_Uom_Type_Id
                        LEFT OUTER JOIN dbo.Currency_Unit_Conversion cuconv
                              ON cuconv.Base_Unit_Id = cuad.Currency_Unit_Id
                                 AND cuconv.Conversion_Date = cuad.Service_Month
                                 AND cuconv.Currency_Group_Id = @Client_Currency_Group_Id
                                 AND cuconv.Converted_Unit_Id = @Currency_Unit_Id -- usd                                                     
                  GROUP BY
                        Ch.Client_Name
                       ,Ch.Sitegroup_Name
                       ,Ch.Site_name
                       ,ch.City
                       ,ch.State_Name
                       ,ch.Country_Name
                       ,ca.Account_Vendor_Name
                       ,ca.Display_Account_Number
                       ,ca.Account_Id
                       ,ca.Account_Type
                       ,vcl.Consumption_Level_Desc
                       ,ca.Account_Is_Data_Entry_Only
                       ,c.Code_Value
                       ,bm.Bucket_Master_Id
                       ,bm.Bucket_Name
                       ,cuad.Client_Hier_Id
                       ,cuad.Service_Month
                       ,com.Commodity_Name
                       ,com.Commodity_Id
                       ,c.Code_Value
                       ,cuad.Bucket_Value
                       ,cuc.Conversion_Factor
                       ,cuconv.Conversion_Factor
                       ,e1.ENTITY_NAME
                       ,bm.Sort_Order          
               
      INSERT      INTO #CU_Data
                  ( 
                   Client_Name
                  ,Sitegroup_Name
                  ,Site_name
                  ,City
                  ,State_Name
                  ,Country_Name
                  ,Account_Vendor_Name
                  ,Display_Account_Number
                  ,Account_Id
                  ,Account_Type
                  ,Consumption_Level_Desc
                  ,[DEO Status]
                  ,[Bucket Type]
                  ,Bucket_Master_Id
                  ,Bucket_Name
                  ,Client_Hier_Id
                  ,Service_Month
                  ,Commodity_Name
                  ,Commodity_Id
                  ,[Converted Bucket Value]
                  ,[Default UOM]
                  ,Sort_Order
                  ,Sort_Type )
                  SELECT
                        cd.Client_Name
                       ,cd.Sitegroup_Name
                       ,cd.Site_name
                       ,cd.City
                       ,cd.State_Name
                       ,cd.Country_Name
                       ,cd.Account_Vendor_Name
                       ,cd.Display_Account_Number
                       ,cd.Account_Id
                       ,cd.Account_Type
                       ,Consumption_Level_Desc
                       ,[DEO Status]
                       ,cubd.Billing_Data
                       ,NULL
                       ,CASE WHEN cubd.Billing_data_Type = 'Billing_Days' THEN 'Billing Days'
                             WHEN cubd.Billing_data_Type = 'Billing_End_Dt' THEN 'Billing End Date'
                             WHEN cubd.Billing_data_Type = 'Billing_Start_Dt' THEN 'Billing Start Date'
                        END
                       ,cd.Client_Hier_Id
                       ,cubd.Service_Month
                       ,Commodity_Name
                       ,cd.Commodity_Id
                       ,cubd.Data
                       ,NULL
                       ,CASE WHEN cubd.Billing_data_Type = 'Billing_Days' THEN 3
                             WHEN cubd.Billing_data_Type = 'Billing_End_Dt' THEN 2
                             WHEN cubd.Billing_data_Type = 'Billing_Start_Dt' THEN 1
                        END
                       ,1
                  FROM
                        #CU_Data cd
                        INNER JOIN #Account_List cubd
                              ON cubd.ACCOUNT_ID = cd.Account_Id
                                 AND cd.Commodity_Id = cubd.commodity_id
                                 AND cd.service_Month = cubd.service_month
                  GROUP BY
                        cd.Client_Name
                       ,cd.Sitegroup_Name
                       ,cd.Site_name
                       ,cd.City
                       ,cd.State_Name
                       ,cd.Country_Name
                       ,cd.Account_Vendor_Name
                       ,cd.Display_Account_Number
                       ,cd.Account_Id
                       ,cd.Account_Type
                       ,Consumption_Level_Desc
                       ,[DEO Status]
                       ,cubd.Billing_Data
                       ,cubd.Billing_data_Type
                       ,cd.Client_Hier_Id
                       ,cubd.Service_Month
                       ,Commodity_Name
                       ,cd.Commodity_Id
                       ,cubd.Data    
              
              
      SELECT
            Client_Name [Client Name]
           ,Sitegroup_Name [Division]
           ,Site_name [Site]
           ,City
           ,State_Name [State]
           ,Country_Name [Country]
           ,Account_Vendor_Name [Vendor]
           ,Display_Account_Number [Account #]
           ,Account_Id [Account ID]
           ,Account_Type [Account Type]
           ,Consumption_Level_Desc [Consumption Level]
           ,[DEO Status]
           ,[Bucket Type]
           ,Bucket_Master_Id [BMID]
           ,Bucket_Name [Bucket]
           ,Client_Hier_Id [CHID]
           ,Service_Month [Month]
           ,Commodity_Name [Commodity]
           ,Commodity_Id [Commodity ID]
           ,[Converted Bucket Value]
           ,[Default UOM]
           ,Sort_Order
           ,Sort_Type
      FROM
            #CU_Data
      ORDER BY
            [Division]
           ,[Site]
           ,[Country]
           ,[State]
           ,City
           ,[Account #]
           ,[Commodity]
           ,Sort_Type
           ,Sort_Order        
                      
              
END;

;
GO

GRANT EXECUTE ON  [dbo].[Report_DE_Cost_Usage_Sel_By_Client] TO [CBMS_SSRS_Reports]
GO
