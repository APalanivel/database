
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	cbms_prod.dbo.cbmsCuInvoice_GetWithFailedVariance

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	 cbmsCuInvoice_GetWithFailedVariance 
 
AUTHOR INITIALS:
    Initials	 Name
------------------------------------------------------------
    AP		 Athmaram Pabbathi
    
MODIFICATIONS
    Initials	 Date	   Modification
------------------------------------------------------------
    PD		 02/01/2010  Modified the Join with the old variance_test_log table to the new Variance_Log table
    SKA		 03/22/2010  Replaced Account_id reference from cu_invoice with cu_invoice_service_month
    AP		 2012-04-11  Addnl Data Changes
						  -- Replaced Cost_Usage table with CUAD & CH
						  
******/

CREATE PROCEDURE [dbo].[cbmsCuInvoice_GetWithFailedVariance]
AS 
BEGIN

      SET NOCOUNT ON

      SELECT
            cism.CU_Invoice_Id
      FROM
            dbo.Variance_Log vl
            INNER JOIN Core.Client_Hier ch
                  ON ch.Site_Id = vl.Site_Id
            INNER JOIN dbo.Cost_Usage_Account_Dtl cuad
                  ON cuad.Account_ID = vl.Account_Id
                     AND cuad.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.cu_invoice_service_month cism
                  ON cism.Account_ID = cuad.Account_Id
                     AND cism.Service_Month = cuad.Service_Month
      WHERE
            vl.Is_Failure = 1
            AND vl.Closed_Dt IS NULL
      GROUP BY
            cism.CU_Invoice_Id
END
;
GO

GRANT EXECUTE ON  [dbo].[cbmsCuInvoice_GetWithFailedVariance] TO [CBMSApplication]
GO
