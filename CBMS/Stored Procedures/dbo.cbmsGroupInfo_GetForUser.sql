SET NUMERIC_ROUNDABORT OFF 
GO

SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	dbo.[cbmsGroupInfo_GetForUser]

DESCRIPTION: 

INPUT PARAMETERS:    
      Name                  DataType          Default     Description    
------------------------------------------------------------------    
	@user_info_id              int
        
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.cbmsGroupInfo_GetForUser 49
	EXEC dbo.cbmsGroupInfo_GetForUser 162
	EXEC dbo.cbmsGroupInfo_GetForUser 409

	SELECT * FROM user_Info WHERE Access_Level = 1 And is_history = 0

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		HG		Harihara Suthan G
		RR		Raghu Reddy

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG			04/19/2011	Added Comments header
							Removed unused parameter @MyAccountid as dependent stored has been modified as a part of DV group revamp.
	HG			04/27/2011	Based on the Script review comment replaced the subselect query with LEFT JOIN which used to get the user specific group.
	RR			2013-06-06	ENHANCE-60 Added Is_Fee_Module column in select list
	HG			2014-03-04	RA Admin enh, added filter condition to not show the Is_Required_For_RA_Login groups if it is mapped to the roles assigned to the users
******/

CREATE PROCEDURE [dbo].[cbmsGroupInfo_GetForUser] ( @user_info_id INT )
AS 
BEGIN

      SET NOCOUNT ON

      SELECT
            gi.group_info_id
           ,gi.group_name
           ,gi.group_description
           ,is_checked = case WHEN map.group_info_id IS NULL THEN 0
                              ELSE 1
                         END
           ,gi.Is_Fee_Module
      FROM
            dbo.group_info gi
            LEFT OUTER JOIN dbo.User_Info_Group_Info_Map map
                  ON map.group_info_id = gi.group_info_id
                     AND map.User_Info_Id = @User_info_id
      WHERE
            ( gi.Is_Required_For_RA_Login = 0
              OR NOT EXISTS ( SELECT
                                    1
                              FROM
                                    dbo.User_Info_Client_App_Access_Role_Map urm
                                    INNER JOIN dbo.Client_App_Access_Role_Group_Info_Map argm
                                          ON argm.Client_App_Access_Role_Id = urm.Client_App_Access_Role_Id
                              WHERE
                                    urm.User_Info_Id = @user_info_id
                                    AND argm.GROUP_INFO_ID = gi.GROUP_INFO_ID ) )
      ORDER BY
            gi.group_name ASC

END;

;
GO



GRANT EXECUTE ON  [dbo].[cbmsGroupInfo_GetForUser] TO [CBMSApplication]
GO