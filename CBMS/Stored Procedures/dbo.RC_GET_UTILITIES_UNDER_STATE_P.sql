SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE dbo.RC_GET_UTILITIES_UNDER_STATE_P 
@userId varchar,
@sessionId varchar,
@stateId int

as
	set nocount on
	Select	v.VENDOR_ID,
		v.VENDOR_NAME 
	
	from 	VENDOR v,
		VENDOR_STATE_MAP vsm ,
		entity venType 
	
	where 	vsm.STATE_ID = @stateId  and
		v.VENDOR_ID = vsm.VENDOR_ID and 
		v.vendor_type_id = venType.entity_id and
		venType.entity_name = 'Utility'
	
	order by v.vendor_name
GO
GRANT EXECUTE ON  [dbo].[RC_GET_UTILITIES_UNDER_STATE_P] TO [CBMSApplication]
GO
