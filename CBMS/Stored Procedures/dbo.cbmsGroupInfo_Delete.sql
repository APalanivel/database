SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<K. Horton>
-- Create date: <12-2-2009>
-- Description:	<Deletes From GroupInfo>
-- =============================================

CREATE      procedure [dbo].[cbmsGroupInfo_Delete]
	( @group_info_id int
		)
AS
BEGIN
	set nocount on
	
	delete group_info where group_info_id = @group_info_id

END

GO
GRANT EXECUTE ON  [dbo].[cbmsGroupInfo_Delete] TO [CBMSApplication]
GO
