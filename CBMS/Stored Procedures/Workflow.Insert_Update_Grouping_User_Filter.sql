SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

       
        
/******              
NAME:    [Workflow].[Insert_Update_Grouping_User_Filter]          
DESCRIPTION: THIS STORED PROCEDURE IS CREATED TO INSERT OR UPDATE THE GROUPING          
          
If output column id is empty, then code will delete the user column preferences.          
------------------------------------------------------------             
 INPUT PARAMETERS:              
 Name   DataType  Default Description             
 @userID   INT ,          
  @Workflow_Queue_Saved_Filter_Query_id int,          
  @Group_Id_List varchar(max)        
  @Group_Name varchar (max),          
  @invoice_List varchar(max)           
------------------------------------------------------------              
 OUTPUT PARAMETERS:              
 Name   DataType  Default Description              
------------------------------------------------------------              
 USAGE EXAMPLES:              
------------------------------------------------------------              
AUTHOR INITIALS:              
Initials Name              
------------------------------------------------------------              
AKP   ARUNKUMAR PALANIVEL Summit Energy           
           
 MODIFICATIONS               
 Initials Date   Modification              
------------------------------------------------------------              
 AKP    SEP 5,2019 Created          
 CPK	Oct-2019  added event log logic
******/        
       
CREATE PROCEDURE [Workflow].[Insert_Update_Grouping_User_Filter]        
    -- Add the parameters for the stored procedure here          
        
    @MODULE_ID INT,        
    @Workflow_Queue_Saved_Filter_Query_Id INT = NULL,        
    @Logged_In_User INT,        
    @Queue_id VARCHAR(MAX) = NULL,     ---  User selected from drop down Id 49,1,2,3                  
    @Exception_Type VARCHAR(MAX) = NULL,        
    @Account_Number VARCHAR(500) = NULL,        
    @Client VARCHAR(500) = NULL,        
    @Site VARCHAR(500) = NULL,        
    @Country VARCHAR(500) = NULL,        
    @State VARCHAR(500) = NULL,        
    @City VARCHAR(500) = NULL,        
    @Commodity VARCHAR(500) = NULL,        
    @Invoice_ID VARCHAR(500) = NULL,        
    @Priority INT = 0,                 --- Yes                  
    @Exception_Status INT = NULL,        
    @Comments VARCHAR(500) = NULL,        
    @Start_Date_in_Queue DATETIME = NULL,        
    @End_Date_in_Queue DATETIME = NULL,        
    @Start_Date_in_CBMS DATETIME = NULL,        
    @End_Date_in_CBMS DATETIME = NULL,        
    @Data_Source INT = NULL,        
    @Vendor VARCHAR(100) = NULL,        
    @Vendor_Type VARCHAR(100) = NULL,        
    @Month DATETIME = NULL,        
    @Filename VARCHAR(100) = NULL,        
    @invoice_List VARCHAR(MAX) = NULL, ---- if new invoice needs to be added send those invoices         
    @User_Group_Id_List VARCHAR(MAX)=NULL,        
    @Invoice_List_TVP Invoice_List READONLY,        
    @Group_Name VARCHAR(MAX) = NULL    --- new group name        
AS        
BEGIN -- SET NOCOUNT ON added to prevent extra result sets from             
    -- interfering with SELECT statements.             
    DECLARE @Proc_name VARCHAR(100) = 'Insert_Update_Grouping_User_Filter',        
            @Input_Params VARCHAR(1000),        
            @Error_Line INT,        
            @Error_Message VARCHAR(3000),        
            @Workflow_Queue_Search_Filter_Group_Id INT;        
        
    DECLARE @invoice_table TABLE        
    (        
        Invoice_id INT        
    );        
        
    SELECT @Input_Params        
        = '@User ID:' + CAST(@Logged_In_User AS VARCHAR) + '@Workflow_Queue_Saved_Filter_Query_id:'        
          + CAST(@Workflow_Queue_Saved_Filter_Query_Id AS VARCHAR) + '@invoice_List:' + ISNULL(@invoice_List, '');        
    SET NOCOUNT ON;        
        
        
        
    BEGIN TRY        
        
        
        
        --get the list of invoices        
        
        INSERT @invoice_table        
        EXEC [Workflow].[Get_List_Of_Invoices] @MODULE_ID,        
                        @Workflow_Queue_Saved_Filter_Query_Id,        
                                               @Logged_In_User,        
                                               @Queue_id,        
                                               @Exception_Type,        
                                               @Account_Number,        
                                               @Client,        
                                               @Site,        
                                               @Country,        
                                               @State,     --state id comma separated done        
                                               @City,      --done         
                                               @Commodity, -- done        
                                               @Invoice_ID,        
                                               @Priority,        
                                               @Exception_Status,        
                                               @Comments,        
                                               @Start_Date_in_Queue,        
                                               @End_Date_in_Queue,        
                                               @Start_Date_in_CBMS,        
                                               @End_Date_in_CBMS,        
                                               @Data_Source,        
                                               @Vendor,        
                                               @Vendor_Type,        
                                               @Month,        
                                               @Filename,        
                                               @invoice_List,        
                                               @User_Group_Id_List,        
                                               @Invoice_List_TVP;         
--DELETE THE EXISTING ENTRIES        
           DELETE WQSFIM1        
        FROM Workflow.Workflow_Queue_Search_Filter_Group_Invoice_Map WQSFIM1        
        WHERE WQSFIM1.Workflow_Queue_Search_Filter_Group_Id IN        
              (        
                  SELECT WQSFG.Workflow_Queue_Search_Filter_Group_Id        
                  FROM Workflow.Workflow_Queue_Search_Filter_Group WQSFG        
      JOIN Workflow.Workflow_Queue_Search_Filter_Group_Invoice_Map wqsfim        
      ON wqsfg.Workflow_Queue_Search_Filter_Group_Id = wqsfim.Workflow_Queue_Search_Filter_Group_Id        
                      JOIN @invoice_table O        
                          ON O.Invoice_id = WQSFIM.Cu_Invoice_Id        
                  WHERE WQSFG.Owner_User_Info_Id = @Logged_In_User        
                        AND WQSFG.Workflow_Queue_Saved_Filter_Query_Id = @Workflow_Queue_Saved_Filter_Query_Id        
              );        
        
        
        
        
        DELETE WQSFG        
        FROM Workflow.Workflow_Queue_Search_Filter_Group WQSFG        
            LEFT JOIN Workflow.Workflow_Queue_Search_Filter_Group_Invoice_Map WQSFIM        
                ON WQSFG.Workflow_Queue_Search_Filter_Group_Id = WQSFIM.Workflow_Queue_Search_Filter_Group_Id        
        WHERE WQSFG.Owner_User_Info_Id = @Logged_In_User        
              AND WQSFG.Workflow_Queue_Saved_Filter_Query_Id = @Workflow_Queue_Saved_Filter_Query_Id        
              AND WQSFIM.Cu_Invoice_Id IS NULL;        
        
        IF (@Group_Name IS NOT NULL)        
        BEGIN        
        
            IF NOT EXISTS        
            (        
                SELECT 1        
                FROM Workflow.Workflow_Queue_Search_Filter_Group        
                WHERE Workflow_Queue_Saved_Filter_Query_Id = @Workflow_Queue_Saved_Filter_Query_Id        
                      AND Filter_Group_Name = @Group_Name        
                      AND Owner_User_Info_Id = @Logged_In_User        
            )        
            BEGIN        
                INSERT INTO Workflow.Workflow_Queue_Search_Filter_Group        
           (        
                    Workflow_Queue_Saved_Filter_Query_Id,        
                    Filter_Group_Name,        
                    Owner_User_Info_Id,        
                    Created_User_Id,        
                    Created_Ts,        
                    Updated_User_Id,        
                    Last_Change_Ts        
                )        
                VALUES        
                (@Workflow_Queue_Saved_Filter_Query_Id, @Group_Name, @Logged_In_User, @Logged_In_User, GETDATE(),        
                 @Logged_In_User, GETDATE());        
        
            END;        
        
            SET @Workflow_Queue_Search_Filter_Group_Id =        
            (        
      SELECT Workflow_Queue_Search_Filter_Group_Id        
                FROM Workflow.Workflow_Queue_Search_Filter_Group        
                WHERE Workflow_Queue_Saved_Filter_Query_Id = @Workflow_Queue_Saved_Filter_Query_Id        
                      AND Filter_Group_Name = @Group_Name        
                      AND Owner_User_Info_Id = @Logged_In_User        
    );         
        
            IF (@Workflow_Queue_Search_Filter_Group_Id IS NOT NULL)        
            BEGIN        
            
        
                INSERT INTO Workflow.Workflow_Queue_Search_Filter_Group_Invoice_Map        
                (        
                    Workflow_Queue_Search_Filter_Group_Id,        
                    Cu_Invoice_Id,        
                    Created_User_Id,        
                    Created_Ts        
                )        
                SELECT @Workflow_Queue_Search_Filter_Group_Id,        
                       Invoice_id,        
                       @Logged_In_User,        
                       getdate()        
                FROM @invoice_table;        
				
				INSERT INTO dbo.cu_invoice_event          
					(          
					   cu_invoice_id  
							,event_date  
							,event_by_id  
							,event_description         
					)       
					SELECT           
					 AC.Invoice_id AS CU_INVOICE_ID          
					,getdate() AS event_date          
					,@Logged_In_User AS event_by_id         
					,'Group Action Performed' AS event_description          
					FROM @invoice_table AS AC
        
            END;         
        END;         
        
    --DELETE THE ENTRIES WHICH WAS DEFAULT EARLIER AND NOW USER DID NOT WANT TOR           
    END TRY        
    BEGIN CATCH        
        -- Entry made to the logging SP to capture the errors.            
        SELECT @Error_Line = error_line(),        
               @Error_Message = error_message();        
        
        INSERT INTO StoredProc_Error_Log        
        (        
            StoredProc_Name,        
            Error_Line,        
            Error_message,        
            Input_Params        
        )        
        VALUES        
        (@Proc_name, @Error_Line, @Error_Message, @Input_Params);        
        
        EXEC [dbo].[usp_RethrowError] @Error_Message;        
    END CATCH;        
END;                                               
GO
GRANT EXECUTE ON  [Workflow].[Insert_Update_Grouping_User_Filter] TO [CBMSApplication]
GO
