SET NUMERIC_ROUNDABORT OFF 
GO

SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

 NAME:  
	dbo.BUDGET_UPDATE_BUDGET_ACCOUNT_DETAILS_P
 
 DESCRIPTION:
 
 INPUT PARAMETERS:  
 Name						DataType	  Default Description  
------------------------------------------------------------  
 @budget_account_id			INT
 @month_identifier			DATETIME
 @variable					VARCHAR(300)
 @variable_value			DECIMAL(32,16)
 @transportation			VARCHAR(300)
 @transportation_value		DECIMAL(32,16)
 @transmission				VARCHAR(300)
 @transmission_value		DECIMAL(32,16)
 @distribution				VARCHAR(300)
 @distribution_value		DECIMAL(32,16)
 @other_bundled				VARCHAR(300)
 @other_bundled_value		DECIMAL(32,16)
 @other_fixed_costs			VARCHAR(300)
 @other_fixed_costs_value	DECIMAL(32,16)
 @sourcing_tax				VARCHAR(300)
 @sourcing_tax_value		DECIMAL(32,16)
 @rates_tax					VARCHAR(300)
 @rates_tax_value			DECIMAL(32,16)
 @manual_gen_status			BIT
 @manual_tax_status			BIT

 
 OUTPUT PARAMETERS:  

 Name   DataType  Default Description  
------------------------------------------------------------  
 USAGE EXAMPLES:
------------------------------------------------------------  
  BEGIN TRAN
  
	  EXEC dbo.BUDGET_UPDATE_BUDGET_ACCOUNT_DETAILS_P
			356850
		   ,'2011-01-01'
		   ,'5.82*c3'
		   ,9.05243
		   ,'((((((a1/.95-a1)*0.14)*c1)/1.055056)*(31/1500))*(((1.3*c1)/1.055056)*(326/1500))+(((1.2*c1)/1.055056)*(1143/1500)))*((1500*31)/a2)'
		   ,1.86514
		   ,null
		   ,null
		   ,'.41*c3'
		   ,0.63771
		   ,null
		   ,null
		   ,null
		   ,null
		   ,null
		   ,null
		   ,null
		   ,null
		   ,true
		   ,false
	ROLLBACK TRAN	  
 AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 PNR		Pandarinath
 HG			Harihara Suthan G

 MODIFICATIONS:
 Initials Date			Modification
------------------------------------------------------------  
 PNR	  03/31/2011	MAINT-548 fixes code to save the full formula entered for tranportation
								Extended param length upto 300 and increased the actual column width for the below mentioned columns
									Trasportation, Variable, Transmission, Distribution, Other_bundled, Other_fixed_costs, Sourcing_tax and Rates_Tax.
							Replaced the Count(1) >0  with If Exists which used to find the record existense.
 HG		  04/25/2011	MAINT-548 increased the parameter width to VARCHAR(300) for the below mentioned columns as the column width already increased
										Variable, Transmission, Distribution, Other_bundled, Other_fixed_costs, Sourcing_tax and Rates_Tax.

******/

CREATE PROCEDURE dbo.BUDGET_UPDATE_BUDGET_ACCOUNT_DETAILS_P
	 @budget_account_id			INT
    ,@month_identifier			DATETIME
    ,@variable					VARCHAR(300)
    ,@variable_value			DECIMAL(32,16)
    ,@transportation			VARCHAR(300)
    ,@transportation_value		DECIMAL(32,16)
    ,@transmission				VARCHAR(300)
    ,@transmission_value		DECIMAL(32,16)
    ,@distribution				VARCHAR(300)
    ,@distribution_value		DECIMAL(32,16)
    ,@other_bundled				VARCHAR(300)
    ,@other_bundled_value		DECIMAL(32,16)
    ,@other_fixed_costs			VARCHAR(300)
    ,@other_fixed_costs_value	DECIMAL(32,16)
    ,@sourcing_tax				VARCHAR(300)
    ,@sourcing_tax_value		DECIMAL(32,16)
    ,@rates_tax					VARCHAR(300)
    ,@rates_tax_value			DECIMAL(32,16)
    ,@manual_gen_status			BIT
    ,@manual_tax_status			BIT
AS
BEGIN

	SET NOCOUNT ON

    IF EXISTS (
				 SELECT 
					1
				 FROM   
					dbo.budget_details
				 WHERE  
					budget_account_id = @budget_account_id
					AND month_identifier = @month_identifier
       )
         BEGIN  
  
            UPDATE  
				dbo.budget_details
            SET     
				variable				= @variable,
                variable_value			= @variable_value,
                transportation			= @transportation,
                transportation_value	= @transportation_value,
                transmission			= @transmission,
                transmission_value		= @transmission_value,
                distribution			= @distribution,
                distribution_value		= @distribution_value,
                other_bundled			= @other_bundled,
                other_bundled_value		= @other_bundled_value,
                other_fixed_costs		= @other_fixed_costs,
                other_fixed_costs_value = @other_fixed_costs_value,
                sourcing_tax			= @sourcing_tax,
                sourcing_tax_value		= @sourcing_tax_value,
                rates_tax				= @rates_tax,
                rates_tax_value			= @rates_tax_value,
                is_manual_generation	= @manual_gen_status,
                is_manual_sourcing_tax	= @manual_tax_status
            WHERE   
				budget_account_id		= @budget_account_id
                AND month_identifier	= @month_identifier
        END
    ELSE 
        BEGIN  
            INSERT  INTO dbo.budget_details
                    (
                      budget_account_id,
                      month_identifier,
                      variable,
                      variable_value,
                      transportation,
                      transportation_value,
                      transmission,
                      transmission_value,
                      distribution,
                      distribution_value,
                      other_bundled,
                      other_bundled_value,
                      other_fixed_costs,
                      other_fixed_costs_value,
                      sourcing_tax,
                      sourcing_tax_value,
                      rates_tax,
                      rates_tax_value,
                      is_manual_generation,
                      is_manual_sourcing_tax
                    )
            VALUES  (
                      @budget_account_id,
                      @month_identifier,
                      @variable,
                      @variable_value,
                      @transportation,
                      @transportation_value,
                      @transmission,
                      @transmission_value,
                      @distribution,
                      @distribution_value,
                      @other_bundled,
                      @other_bundled_value,
                      @other_fixed_costs,
                      @other_fixed_costs_value,
                      @sourcing_tax,
                      @sourcing_tax_value,
                      @rates_tax,
                      @rates_tax_value,
                      @manual_gen_status,
                      @manual_tax_status
                    )   
		END
END
GO

GRANT EXECUTE ON  [dbo].[BUDGET_UPDATE_BUDGET_ACCOUNT_DETAILS_P] TO [CBMSApplication]
GO
GO