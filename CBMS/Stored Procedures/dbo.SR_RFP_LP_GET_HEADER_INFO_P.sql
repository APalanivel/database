
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.SR_RFP_LP_GET_HEADER_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------         	
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES
------------------------------------------------------------

	EXEC dbo.SR_RFP_LP_GET_HEADER_INFO_P 1,1,258733,3870
	EXEC dbo.SR_RFP_LP_GET_HEADER_INFO_P 1,1,258812	,3702
	EXEC dbo.SR_RFP_LP_GET_HEADER_INFO_P 1,1,33785,2335
	EXEC dbo.SR_RFP_LP_GET_HEADER_INFO_P 1,1,39788,	3290
	
	
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	NR			Narayaan Reddy
	
	
MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	NR			2014-10-29	Added header.
							Added the Site_Reference_Number in select list.
								
******/

CREATE  PROCEDURE [dbo].[SR_RFP_LP_GET_HEADER_INFO_P]
      @user_id VARCHAR(10)
     ,@session_id VARCHAR(20)
     ,@site_id INT
     ,@rfp_id INT
AS 
BEGIN 
      SET NOCOUNT ON
      SELECT
            ch.Client_Name
           ,ch.Site_name
           ,en.entity_name AS status
           ,count(cha.Meter_Id) AS total_meters
           ,ch.Site_Reference_Number
      FROM
            dbo.SR_RFP_ACCOUNT rfp_account
            INNER JOIN sr_rfp_lp_account_summary summary
                  ON summary.sr_account_group_id = rfp_account.sr_rfp_account_id
            INNER JOIN entity en
                  ON en.entity_id = summary.status_type_id
            INNER JOIN core.Client_Hier_Account cha
                  ON cha.Account_Id = rfp_account.account_id
            INNER JOIN Core.Client_Hier ch
                  ON ch.Client_Hier_Id = cha.Client_Hier_Id
      WHERE
            rfp_account.sr_rfp_id = @rfp_id
            AND rfp_account.is_deleted = 0
            AND ch.site_id = @site_id
      GROUP BY
            ch.Client_Name
           ,ch.Site_name
           ,en.entity_name
           ,ch.Site_Reference_Number
END


;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_LP_GET_HEADER_INFO_P] TO [CBMSApplication]
GO
