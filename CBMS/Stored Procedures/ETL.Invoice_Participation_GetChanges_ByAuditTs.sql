SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          
NAME:   etl.Invoice_Participation_GetChanges_ByAuditTs       

    
DESCRIPTION:  
	Gets changes from Invoice_participation that are between the 
	specified time stamps         
	
      
INPUT PARAMETERS:          
Name				DataType	Default		Description          
------------------------------------------------------------          
@Audit_Start_Ts		DATETIME
@Audit_End_Ts		DATETIME	
                
OUTPUT PARAMETERS:          
Name			DataType	Default		Description          
------------------------------------------------------------ 

         
USAGE EXAMPLES:          
------------------------------------------------------------ 

EXEC etl.IP_GetChanges_ByAuditTs  '01/01/2009', '12/31/2010'

     
AUTHOR INITIALS:          
Initials	Name          
------------------------------------------------------------          
CMH			Chad Hattabaugh  
     
     
MODIFICATIONS           
Initials	Date	Modification          
------------------------------------------------------------          
CMH			10/04/2010	Created
*****/ 
CREATE PROCEDURE etl.Invoice_Participation_GetChanges_ByAuditTs
(	@Audit_Start_Ts		DATETIME
	,@Audit_End_Ts		DATETIME			)
AS 
BEGIN

	SET NOCOUNT ON; 


	WITH cte_Audit
	AS (	SELECT 
				max(Audit_Ts) as Max_Audit_Ts 
				,Account_ID
				,Site_Id
				,Service_Month
			FROM 
				INVOICE_PARTICIPATION_Audit adt
			WHERE 
				Audit_Ts between @Audit_Start_Ts AND @Audit_End_Ts
			GROUP BY 
				Account_ID
				,Site_Id
				,Service_Month )
	SELECT
		adt.Site_Id
		,adt.Account_ID
		,adt.Service_Month
		,adt.Audit_Ts
		,ip.cbms_image_id
		,min(adt.Audit_Function)		as Audit_Function
		,isnull(ip.IS_EXPECTED, 0)		as Is_Expected
		,isnull(ip.IS_RECEIVED, 0)		as Is_Received
	FROM 
		cte_Audit cte
		INNER JOIN Invoice_Participation_Audit adt 
			ON cte.Site_Id = adt.Site_Id
				AND cte.Account_ID = adt.Account_ID
				AND cte.Service_Month = adt.Service_Month
				AND cte.Max_Audit_Ts = adt.Audit_Ts
		LEFT JOIN INVOICE_PARTICIPATION ip 
			ON adt.Account_ID = ip.ACCOUNT_ID 
				AND adt.Site_Id = ip.SITE_ID
				AND adt.Service_Month = ip.SERVICE_MONTH
	GROUP BY 
		adt.Site_Id
		,adt.Account_ID
		,adt.Service_Month
		,adt.Audit_Ts
		,ip.cbms_image_id
		,ip.IS_EXPECTED
		,ip.IS_RECEIVED
END




GO
GRANT EXECUTE ON  [ETL].[Invoice_Participation_GetChanges_ByAuditTs] TO [CBMSApplication]
GRANT EXECUTE ON  [ETL].[Invoice_Participation_GetChanges_ByAuditTs] TO [ETL_Execute]
GO
