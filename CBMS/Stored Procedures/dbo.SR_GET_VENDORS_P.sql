SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_GET_VENDORS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@vendorTypeId  	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--exec [dbo].[SR_GET_VENDORS_P] '1', '1', 288

CREATE    PROCEDURE [dbo].[SR_GET_VENDORS_P] 

@userId varchar,
@sessionId varchar,
@vendorTypeId int
as
set nocount on
-- if (@vendorTypeId > 0)
if (@vendorTypeId <> 0)  -- Modified for Bz 6620 
begin
SELECT 
VENDOR_ID, VENDOR_NAME
FROM 
VENDOR
WHERE IS_HISTORY = 0 and vendor_type_id = @vendorTypeId
ORDER BY VENDOR_NAME	
end

else

begin
SELECT 
VENDOR_ID, VENDOR_NAME
FROM 
VENDOR
WHERE IS_HISTORY = 0
ORDER BY VENDOR_NAME	
end
GO
GRANT EXECUTE ON  [dbo].[SR_GET_VENDORS_P] TO [CBMSApplication]
GO
