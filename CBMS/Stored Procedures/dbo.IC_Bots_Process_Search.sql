SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          
            
 NAME: dbo.IC_Bots_Process_Search			
            
 DESCRIPTION:            
    To search the Bots Process names    
            
 INPUT PARAMETERS:            
           
 Name                               DataType            Default      Description            
---------------------------------------------------------------------------------------------------------------          
@IC_Bots_Process_Name				VARCHAR(200)
     
             
 OUTPUT PARAMETERS:           
                  
 Name                               DataType            Default      Description            
---------------------------------------------------------------------------------------------------------------          
            
 USAGE EXAMPLES:                
---------------------------------------------------------------------------------------------------------------          


EXEC dbo.IC_Bots_Process_Search
 

EXEC dbo.IC_Bots_Process_Search
    @IC_Bots_Process_Name = 'cbms'

EXEC dbo.IC_Bots_Process_Search
@Sort_Column ='IC_Bots_Process_Desc'

   

   
 AUTHOR INITIALS:          
            
 Initials               Name            
---------------------------------------------------------------------------------------------------------------          
 NR                     Narayana Reddy              
             
 MODIFICATIONS:           
           
 Initials               Date            Modification          
---------------------------------------------------------------------------------------------------------------          
 NR                     2020-05-27      Created for SE2017-963          
           
******/


CREATE PROCEDURE [dbo].[IC_Bots_Process_Search]
    (
        @IC_Bots_Process_Name VARCHAR(200) = NULL
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
        , @Sort_Column VARCHAR(50) = 'IC_Bots_Process_Name'
        , @Sort_Order VARCHAR(15) = 'ASC'
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @SQL_Statement NVARCHAR(MAX)
            , @Where VARCHAR(MAX);

        SET @Where = CHAR(10) + ' WHERE 1=1';

        IF @IC_Bots_Process_Name IS NOT NULL
            BEGIN
                SET @Where = @Where + CHAR(10) + ' And  ibp.IC_Bots_Process_Name LIKE ''%' + @IC_Bots_Process_Name
                             + '%''';
            END;




        SELECT
            @SQL_Statement = N'    ;WITH Cte_IC_Bots_Process
        AS (
               SELECT
                    ibp.IC_Bots_Process_Id
                    , ibp.IC_Bots_Process_Name
                    , ibp.IC_Bots_Process_Desc
                    , COUNT(1) OVER () AS Total_Row
                    , ROW_NUMBER() OVER(ORDER BY ' + @Sort_Column + N' ' + @Sort_Order + N')'
                             + N'  AS Row_Num
               FROM
                    dbo.IC_Bots_Process ibp
               ' + @Where + N'

               GROUP BY
                   ibp.IC_Bots_Process_Id
                   , ibp.IC_Bots_Process_Name
                   , ibp.IC_Bots_Process_Desc
           )
        SELECT
            cte_ibp.IC_Bots_Process_Id
            , cte_ibp.IC_Bots_Process_Name
            , cte_ibp.IC_Bots_Process_Desc
            , cte_ibp.Total_Row
            , cte_ibp.Row_Num
        FROM
            Cte_IC_Bots_Process cte_ibp
        WHERE
            cte_ibp.Row_Num BETWEEN ' + CAST(@Start_Index AS VARCHAR(MAX)) + N' AND '
                             + CAST(@End_Index AS VARCHAR(MAX)) + N' 
        ORDER BY
            cte_ibp.Row_Num';

        EXEC (@SQL_Statement);


    END;


GO
GRANT EXECUTE ON  [dbo].[IC_Bots_Process_Search] TO [CBMSApplication]
GO
