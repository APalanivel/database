SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
                         
 NAME: dbo.Contract_Cu_Invoice_Map_Del                     
                          
 DESCRIPTION:      

		  To remove the contract from Invoice

 INPUT PARAMETERS:      
                         
 Name                               DataType          Default       Description      
----------------------------------------------------------------------------------
 @Cu_Invoice_Id						INT
 @Account_Id						INT					NULL
 @Contract_Id						INT					NULL
             
 OUTPUT PARAMETERS:      
                               
 Name                               DataType          Default       Description      
----------------------------------------------------------------------------------
                          
 USAGE EXAMPLES:                              
----------------------------------------------------------------------------------
               
	
			                
                         
 AUTHOR INITIALS:    
       
 Initials                   Name      
----------------------------------------------------------------------------------
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
----------------------------------------------------------------------------------
 NR                     2019-11-15      Created for - Add contract.                        
                         
******/
CREATE PROCEDURE [dbo].[Contract_Cu_Invoice_Map_Del]
      (
      @Cu_Invoice_Id INT
    , @Account_Id    INT = NULL
    , @Contract_Id   INT = NULL )
AS
      BEGIN

            SET NOCOUNT ON;

            DELETE      sac
            FROM        dbo.Contract_Cu_Invoice_Map sac
            WHERE       sac.Cu_Invoice_Id = @Cu_Invoice_Id
                        AND
                              (     @Contract_Id IS NULL
                                    OR    sac.Contract_Id = @Contract_Id )
                        AND
                              (     @Account_Id IS NULL
                                    OR    sac.Account_Id = @Account_Id );


      END;
GO
GRANT EXECUTE ON  [dbo].[Contract_Cu_Invoice_Map_Del] TO [CBMSApplication]
GO
