SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[u_Sitegroup455942349]
		@c1 int = NULL,
		@c2 varchar(200) = NULL,
		@c3 int = NULL,
		@c4 int = NULL,
		@c5 bit = NULL,
		@c6 bit = NULL,
		@c7 int = NULL,
		@c8 int = NULL,
		@c9 datetime = NULL,
		@c10 int = NULL,
		@c11 datetime = NULL,
		@c12 bit = NULL,
		@c13 bit = NULL,
		@c14 bit = NULL,
		@c15 datetime = NULL,
		@c16 int = NULL,
		@pkc1 int = NULL,
		@bitmap binary(2),
		@MSp2pPreVersion varbinary(32) , @MSp2pPostVersion varbinary(32) 

as
begin  
update [dbo].[Sitegroup] set
		[Sitegroup_Name] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [Sitegroup_Name] end,
		[Sitegroup_Type_Cd] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [Sitegroup_Type_Cd] end,
		[Client_Id] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [Client_Id] end,
		[Is_Smart_Group] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [Is_Smart_Group] end,
		[Is_Publish_To_DV] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [Is_Publish_To_DV] end,
		[Owner_User_Id] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [Owner_User_Id] end,
		[Add_User_Id] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [Add_User_Id] end,
		[Add_Dt] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [Add_Dt] end,
		[Mod_User_Id] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [Mod_User_Id] end,
		[Mod_Dt] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [Mod_Dt] end,
		[Is_Complete] = case substring(@bitmap,2,1) & 8 when 8 then @c12 else [Is_Complete] end,
		[Is_Locked] = case substring(@bitmap,2,1) & 16 when 16 then @c13 else [Is_Locked] end,
		[Is_Edited] = case substring(@bitmap,2,1) & 32 when 32 then @c14 else [Is_Edited] end,
		[Last_Smartgroup_Refresh_dt] = case substring(@bitmap,2,1) & 64 when 64 then @c15 else [Last_Smartgroup_Refresh_dt] end,
		[Portfolio_Client_Hier_Reference_Number] = case substring(@bitmap,2,1) & 128 when 128 then @c16 else [Portfolio_Client_Hier_Reference_Number] end		,$sys_p2p_cd_id = @MSp2pPostVersion

where [Sitegroup_Id] = @pkc1
		and ($sys_p2p_cd_id = @MSp2pPreVersion or $sys_p2p_cd_id is null)
if @@rowcount = 0
begin  
	declare @cur_version varbinary(32) 
		,@conflict_type int = 1
		,@conflict_type_txt nvarchar(20) = N'Update-Update'
		,@reason_code int = 1
		,@reason_text nvarchar(720) = NULL
		,@is_on_disk_winner bit = 0
		,@is_incoming_winner bit = 0
		,@peer_id_current_node int = NULL
		,@peer_id_incoming int
		,@tranid_incoming nvarchar(40)
		,@peer_id_on_disk int
		,@tranid_on_disk nvarchar(40)
	select @peer_id_incoming = sys.fn_replvarbintoint(@MSp2pPostVersion)
		,@tranid_incoming = sys.fn_replp2pversiontotranid(@MSp2pPostVersion)
	select @cur_version = $sys_p2p_cd_id from [dbo].[Sitegroup] 
where [Sitegroup_Id] = @pkc1
	if @@rowcount = 0  
			select @conflict_type = 3, @conflict_type_txt = N'Update-Delete', @is_on_disk_winner = 1, @reason_text = formatmessage(22823), @reason_code = 0
	else 
	begin  
		select @peer_id_on_disk = sys.fn_replvarbintoint(@cur_version)
			,@tranid_on_disk = sys.fn_replp2pversiontotranid(@cur_version)
		if(@peer_id_incoming > @peer_id_on_disk)
			set @is_incoming_winner = 1
		else
			set @is_on_disk_winner = 1
	end  
		select @peer_id_current_node = p.originator_id from syspublications p join sysarticles a on a.pubid = p.pubid where a.objid = object_id(N'[dbo].[Sitegroup]') and p.options & 0x1 = 0x1
	if (@peer_id_current_node is not null) 
	begin 
		if (@reason_text is NULL)
			if (@peer_id_incoming > @peer_id_on_disk)
			begin  
				select @reason_text = formatmessage(22822,@peer_id_incoming,@peer_id_on_disk,@peer_id_current_node)
			end  
			else  
			begin  
				select @reason_text = formatmessage(22821,@peer_id_incoming,@peer_id_on_disk,@peer_id_current_node)
			end  
		create table #change_id (change_id varbinary(8))
		insert [dbo].[conflict_dbo_Sitegroup] (
		[Sitegroup_Id],
		[Sitegroup_Name],
		[Sitegroup_Type_Cd],
		[Client_Id],
		[Is_Smart_Group],
		[Is_Publish_To_DV],
		[Owner_User_Id],
		[Add_User_Id],
		[Add_Dt],
		[Mod_User_Id],
		[Mod_Dt],
		[Is_Complete],
		[Is_Locked],
		[Is_Edited],
		[Last_Smartgroup_Refresh_dt],
		[Portfolio_Client_Hier_Reference_Number]
			,__$originator_id
			,__$origin_datasource
			,__$tranid
			,__$conflict_type
			,__$is_winner
			,__$reason_code
			,__$reason_text
			,__$update_bitmap
			,__$pre_version)
		output inserted.__$row_id into #change_id
		select 
    @pkc1,
    @c2,
    @c3,
    @c4,
    @c5,
    @c6,
    @c7,
    @c8,
    @c9,
    @c10,
    @c11,
    @c12,
    @c13,
    @c14,
    @c15,
    @c16			,@peer_id_current_node
			,@peer_id_incoming
			,@tranid_incoming
			,@conflict_type
			,@is_incoming_winner
			,@reason_code
			,@reason_text
			,@bitmap
			,@MSp2pPreVersion
		insert [dbo].[conflict_dbo_Sitegroup] (

		[Sitegroup_Id],
		[Sitegroup_Name],
		[Sitegroup_Type_Cd],
		[Client_Id],
		[Is_Smart_Group],
		[Is_Publish_To_DV],
		[Owner_User_Id],
		[Add_User_Id],
		[Add_Dt],
		[Mod_User_Id],
		[Mod_Dt],
		[Is_Complete],
		[Is_Locked],
		[Is_Edited],
		[Last_Smartgroup_Refresh_dt],
		[Portfolio_Client_Hier_Reference_Number]			,__$originator_id
			,__$origin_datasource
			,__$tranid
			,__$conflict_type
			,__$is_winner
			,__$reason_code
			,__$reason_text
			,__$pre_version
			,__$change_id)
		select 

		[Sitegroup_Id],
		[Sitegroup_Name],
		[Sitegroup_Type_Cd],
		[Client_Id],
		[Is_Smart_Group],
		[Is_Publish_To_DV],
		[Owner_User_Id],
		[Add_User_Id],
		[Add_Dt],
		[Mod_User_Id],
		[Mod_Dt],
		[Is_Complete],
		[Is_Locked],
		[Is_Edited],
		[Last_Smartgroup_Refresh_dt],
		[Portfolio_Client_Hier_Reference_Number]			,@peer_id_current_node
			,@peer_id_on_disk
			,@tranid_on_disk
			,@conflict_type
			,@is_on_disk_winner
			,@reason_code
			,@reason_text
			,NULL
			,(select change_id from #change_id)
		from [dbo].[Sitegroup] 

where [Sitegroup_Id] = @pkc1
	end 
	if(@peer_id_incoming > @peer_id_on_disk)
	begin  
update [dbo].[Sitegroup] set
		[Sitegroup_Name] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [Sitegroup_Name] end,
		[Sitegroup_Type_Cd] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [Sitegroup_Type_Cd] end,
		[Client_Id] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [Client_Id] end,
		[Is_Smart_Group] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [Is_Smart_Group] end,
		[Is_Publish_To_DV] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [Is_Publish_To_DV] end,
		[Owner_User_Id] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [Owner_User_Id] end,
		[Add_User_Id] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [Add_User_Id] end,
		[Add_Dt] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [Add_Dt] end,
		[Mod_User_Id] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [Mod_User_Id] end,
		[Mod_Dt] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [Mod_Dt] end,
		[Is_Complete] = case substring(@bitmap,2,1) & 8 when 8 then @c12 else [Is_Complete] end,
		[Is_Locked] = case substring(@bitmap,2,1) & 16 when 16 then @c13 else [Is_Locked] end,
		[Is_Edited] = case substring(@bitmap,2,1) & 32 when 32 then @c14 else [Is_Edited] end,
		[Last_Smartgroup_Refresh_dt] = case substring(@bitmap,2,1) & 64 when 64 then @c15 else [Last_Smartgroup_Refresh_dt] end,
		[Portfolio_Client_Hier_Reference_Number] = case substring(@bitmap,2,1) & 128 when 128 then @c16 else [Portfolio_Client_Hier_Reference_Number] end		,$sys_p2p_cd_id = @MSp2pPostVersion

where [Sitegroup_Id] = @pkc1
	end  
		if exists(select * from syspublications p join sysarticles a on a.pubid = p.pubid where a.objid = object_id(N'[dbo].[Sitegroup]') and p.options & 0x10 = 0x10)
			raiserror(22815, 10, -1, @conflict_type_txt, @peer_id_current_node, @peer_id_incoming, @tranid_incoming, @peer_id_on_disk, @tranid_on_disk) with log
		else
			raiserror(22815, 16, -1, @conflict_type_txt, @peer_id_current_node, @peer_id_incoming, @tranid_incoming, @peer_id_on_disk, @tranid_on_disk) with log
end 
end 
GO
