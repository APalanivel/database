SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_IS_LP_EMAIL_SENT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@rfp_id        	int       	          	
	@site_id       	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--SELECT * FROM sr_rfp_account where sr_rfp_account_id = 564
--SELECT * FROM sr_rfp_lp_send_client
--SELECT * FROM account where account_id = 13579
--dbo.SR_RFP_IS_LP_EMAIL_SENT_P 292, 10445

CREATE PROCEDURE dbo.SR_RFP_IS_LP_EMAIL_SENT_P
	@rfp_id int,
	@site_id int
	AS
set nocount on
	select 	COUNT(email_log.sr_rfp_email_log_id)as lp_count
	from 	sr_rfp_account rfp_account(nolock),
		sr_rfp_lp_send_client send(nolock),
		site s(nolock), 
		account a(nolock),
		sr_rfp_email_log email_log(nolock) 
		 
	where	rfp_account.sr_rfp_id = @rfp_id
		and send.sr_account_group_id = rfp_account.sr_rfp_account_id
		and rfp_account.is_deleted = 0
		and a.account_id = rfp_account.account_id
		and s.site_id = a.site_id
		and s.site_id = @site_id
		and send.sr_rfp_email_log_id = email_log.sr_rfp_email_log_id
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_IS_LP_EMAIL_SENT_P] TO [CBMSApplication]
GO
