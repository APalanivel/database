SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******               
                           
 NAME: [dbo].[Account_Invoice_Collection_Frequency_And_Expected_Dtl_Merge]                            
                              
 DESCRIPTION:                              
   To insert or update or delete data in Account_Invoice_Collection_Frequency_And_Expected_Dtl_Merge  
                              
 INPUT PARAMETERS:              
                             
 Name                            DataType           Default       Description              
---------------------------------------------------------------------------------------------------------------            
@User_Report_Id      INT  
@tvp_Invoice_Collection_Frequency_And_Expected_Dtl       tvp_Invoice_Collection_Frequency_And_Expected_Dtl   READONLY                  
                              
 OUTPUT PARAMETERS:                   
                              
 Name                            DataType            Default      Description              
---------------------------------------------------------------------------------------------------------------            
                              
 USAGE EXAMPLES:              
---------------------------------------------------------------------------------------------------------------            
  
--INSERT  
  
BEGIN TRAN  
DECLARE @tvp_Invoice_Collection_Frequency_And_Expected_Dtl tvp_Invoice_Collection_Frequency_And_Expected_Dtl;    
    
INSERT      INTO @tvp_Invoice_Collection_Frequency_And_Expected_Dtl(  
[Seq_No] ,  
[Is_Primary]  ,  
[Invoice_Frequency_Cd] ,  
[Invoice_Frequency_Pattern_Cd,  
[Expected_Invoice_Raised_Day] ,  
[Expected_Invoice_Received_Day],  
[Expected_Invoice_Raised_Month],  
[Expected_Invoice_Received_Month] ,  
[Expected_Invoice_Raised_Day_Lag] ,  
[Expected_Invoice_Received_Day_Lag]  
 )  
VALUES  
            ( 1, 0,NULL,NULL,1,1,1,1,1,1,1 )  
            ,( 2, 1,NULL,NULL,1,1,1,1,1,1,1 )  
            ,( 3, 2,NULL,NULL,1,1,1,1,1,1,1)  
             ,( 4, 3,NULL,NULL,1,1,1,1,1,1,1' );    
              
SELECT * FROM [Account_Invoice_Collection_Frequency] icli WHERE icl.Invoice_Collection_Account_Config_Id=2  
  
EXEC [dbo].[Account_Invoice_Collection_Frequency_And_Expected_Dtl_Merge]   
      @Invoice_Collection_Account_Config_Id = 2  
     ,@tvp_Invoice_Collection_Frequency_And_Expected_Dtl=@tvp_Invoice_Collection_Frequency_And_Expected_Dtl  
     ,@User_Info_Id =49  
       
SELECT * FROM [Account_Invoice_Collection_Frequency] icli WHERE icl.Invoice_Collection_Account_Config_Id=2  
ROLLBACK  
  
  
--UPDATE  
  
BEGIN TRAN  
DECLARE @tvp_Invoice_Collection_Frequency_And_Expected_Dtl tvp_Invoice_Collection_Frequency_And_Expected_Dtl;    
    
INSERT      INTO @tvp_Invoice_Collection_Frequency_And_Expected_Dtl  
(  
[Seq_No] ,  
[Is_Primary]  ,  
[Invoice_Frequency_Cd] ,  
[Invoice_Frequency_Pattern_Cd,  
[Expected_Invoice_Raised_Day] ,  
[Expected_Invoice_Received_Day],  
[Expected_Invoice_Raised_Month],  
[Expected_Invoice_Received_Month] ,  
[Expected_Invoice_Raised_Day_Lag] ,  
[Expected_Invoice_Received_Day_Lag]  
)  
VALUES  
            ( 1, 0,NULL,NULL,1,1,1,1,1,1,1 )  
            ,( 2, 1,NULL,NULL,1,1,1,1,1,1,1 )  
            
SELECT * FROM [Account_Invoice_Collection_Frequency] icli WHERE icl.Invoice_Collection_Account_Config_Id=2  
  
EXEC [dbo].[Account_Invoice_Collection_Frequency_And_Expected_Dtl_Merge]   
      @Invoice_Collection_Account_Config_Id = 6  
     ,@tvp_Invoice_Collection_Frequency_And_Expected_Dtl=@tvp_Invoice_Collection_Frequency_And_Expected_Dtl  
     ,@User_Info_Id =49  
       
SELECT * FROM [Account_Invoice_Collection_Frequency] icli WHERE icl.Invoice_Collection_Account_Config_Id=2  
ROLLBACK  
  
                             
 AUTHOR INITIALS:              
             
 Initials               Name              
---------------------------------------------------------------------------------------------------------------            
 SP                     Sandeep Pigilam                
                            
 MODIFICATIONS:            
             
 Initials               Date             Modification            
---------------------------------------------------------------------------------------------------------------            
 SP                    2016-11-17       Created for Invoice Tracking Phase I.                        
 RKV				   2019-10-18		Added two new columns Custom_Days, Custom_Invoice_Received_Day                           
******/

CREATE PROCEDURE [dbo].[Account_Invoice_Collection_Frequency_And_Expected_Dtl_Merge]
     (
         @Invoice_Collection_Account_Config_Id INT
         , @tvp_Invoice_Collection_Frequency_And_Expected_Dtl tvp_Invoice_Collection_Frequency_And_Expected_Dtl READONLY
         , @User_Info_Id INT
         , @Preserve_Existing_Value_When_Null BIT = 0
     )
AS
    BEGIN  
        SET NOCOUNT ON;  
  
        DECLARE @FrequencyPattern_CustomDays_Cd INT;  
  
        SELECT  
            @FrequencyPattern_CustomDays_Cd = c.Code_Id  
        FROM  
            Code c  
            INNER JOIN dbo.Codeset cs  
                ON cs.Codeset_Id = c.Codeset_Id  
        WHERE  
            cs.Codeset_Name = 'FrequencyPattern'  
            AND c.Code_Value = 'Custom Days';  
  
  
        MERGE dbo.Account_Invoice_Collection_Frequency AS tgt  
        USING (   SELECT  
                        @Invoice_Collection_Account_Config_Id AS Invoice_Collection_Account_Config_Id  
                        , tvp.Seq_No  
                        , tvp.Is_Primary  
                        , CASE WHEN @Preserve_Existing_Value_When_Null = 1 THEN  
                                   ISNULL(tvp.Invoice_Frequency_Cd, icf.Invoice_Frequency_Cd)  
                              ELSE tvp.Invoice_Frequency_Cd  
                          END AS Invoice_Frequency_Cd  
                        , CASE WHEN @Preserve_Existing_Value_When_Null = 1 THEN  
                                   ISNULL(tvp.Invoice_Frequency_Pattern_Cd, icf.Invoice_Frequency_Pattern_Cd)  
                              ELSE tvp.Invoice_Frequency_Pattern_Cd  
                          END AS Invoice_Frequency_Pattern_Cd  
                        , CASE WHEN @Preserve_Existing_Value_When_Null = 1  
                                    AND tvp.Invoice_Frequency_Pattern_Cd <> @FrequencyPattern_CustomDays_Cd THEN  
                                   NULLIF(ISNULL(tvp.Expected_Invoice_Raised_Day, icf.Expected_Invoice_Raised_Day), 0)  
                              ELSE NULLIF(tvp.Expected_Invoice_Raised_Day, 0)  
                          END AS Expected_Invoice_Raised_Day  
                        , CASE WHEN @Preserve_Existing_Value_When_Null = 1  
                                    AND tvp.Invoice_Frequency_Pattern_Cd <> @FrequencyPattern_CustomDays_Cd THEN  
                                   NULLIF(ISNULL(tvp.Expected_Invoice_Received_Day, icf.Expected_Invoice_Received_Day), 0)  
                              ELSE NULLIF(tvp.Expected_Invoice_Received_Day, 0)  
                          END AS Expected_Invoice_Received_Day  
                        , CASE WHEN @Preserve_Existing_Value_When_Null = 1  
                                    AND tvp.Invoice_Frequency_Pattern_Cd <> @FrequencyPattern_CustomDays_Cd THEN  
                                   NULLIF(ISNULL(tvp.Expected_Invoice_Raised_Month, icf.Expected_Invoice_Raised_Month), 0)  
                              ELSE NULLIF(tvp.Expected_Invoice_Raised_Month, 0)  
                          END AS Expected_Invoice_Raised_Month  
                        , CASE WHEN @Preserve_Existing_Value_When_Null = 1  
                                    AND tvp.Invoice_Frequency_Pattern_Cd <> @FrequencyPattern_CustomDays_Cd THEN  
                                   NULLIF(ISNULL(  
                                              tvp.Expected_Invoice_Received_Month, icf.Expected_Invoice_Received_Month), 0)  
                              ELSE NULLIF(tvp.Expected_Invoice_Received_Month, 0)  
                          END AS Expected_Invoice_Received_Month  
                        , CASE WHEN @Preserve_Existing_Value_When_Null = 1  
                                    AND tvp.Invoice_Frequency_Pattern_Cd <> @FrequencyPattern_CustomDays_Cd THEN  
                                   ISNULL(tvp.Expected_Invoice_Raised_Day_Lag, icf.Expected_Invoice_Raised_Day_Lag)  
                              ELSE tvp.Expected_Invoice_Raised_Day_Lag  
                          END AS Expected_Invoice_Raised_Day_Lag  
                        , CASE WHEN @Preserve_Existing_Value_When_Null = 1  
                                    AND tvp.Invoice_Frequency_Pattern_Cd <> @FrequencyPattern_CustomDays_Cd THEN  
                                   ISNULL(tvp.Expected_Invoice_Received_Day_Lag, icf.Expected_Invoice_Received_Day_Lag)  
                              ELSE tvp.Expected_Invoice_Received_Day_Lag  
                          END AS Expected_Invoice_Received_Day_Lag  
                        , CASE WHEN @Preserve_Existing_Value_When_Null = 1 THEN ISNULL(icf.Last_Change_Ts, GETDATE())  
                              ELSE GETDATE()  
                          END AS TS  
                        , CASE WHEN @Preserve_Existing_Value_When_Null = 1 THEN  
                                   ISNULL(icf.Updated_User_Id, @User_Info_Id)  
                              ELSE @User_Info_Id  
                          END AS User_Info_Id  
                        , CASE WHEN @Preserve_Existing_Value_When_Null = 1  AND tvp.Invoice_Frequency_Pattern_Cd = @FrequencyPattern_CustomDays_Cd THEN  
                                   NULLIF(ISNULL(tvp.Custom_Days, icf.Custom_Days), 0)  
                              ELSE NULLIF(tvp.Custom_Days, 0)  
                          END AS Custom_Days  
                        , CASE WHEN @Preserve_Existing_Value_When_Null = 1 AND tvp.Invoice_Frequency_Pattern_Cd = @FrequencyPattern_CustomDays_Cd THEN  
                                   NULLIF(ISNULL(tvp.Custom_Invoice_Received_Day, icf.Custom_Invoice_Received_Day), 0)  
                              ELSE NULLIF(tvp.Custom_Invoice_Received_Day, 0)  
                          END AS Custom_Invoice_Received_Day  
                  FROM  
                        @tvp_Invoice_Collection_Frequency_And_Expected_Dtl tvp  
                        LEFT JOIN dbo.Account_Invoice_Collection_Frequency icf  
                            ON tvp.Seq_No = icf.Seq_No  
                               AND  icf.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id  
                               AND  @Preserve_Existing_Value_When_Null = 1) AS src  
        ON src.Invoice_Collection_Account_Config_Id = tgt.Invoice_Collection_Account_Config_Id  
           AND  src.Seq_No = tgt.Seq_No  
        WHEN NOT MATCHED BY TARGET THEN INSERT (Invoice_Collection_Account_Config_Id  
                                                , Seq_No  
                                                , Is_Primary  
                                                , Invoice_Frequency_Cd  
                                                , Invoice_Frequency_Pattern_Cd  
                                                , Expected_Invoice_Raised_Day  
                                                , Expected_Invoice_Received_Day  
                                                , Expected_Invoice_Raised_Month  
                                                , Expected_Invoice_Received_Month  
                                                , Expected_Invoice_Raised_Day_Lag  
                                                , Expected_Invoice_Received_Day_Lag  
                                                , Created_User_Id  
                                                , Created_Ts  
                                                , Updated_User_Id  
                                                , Last_Change_Ts  
            , Custom_Days  
                                                , Custom_Invoice_Received_Day)  
                                        VALUES  
                                            (src.Invoice_Collection_Account_Config_Id  
                                             , src.Seq_No  
                                             , src.Is_Primary  
                                             , src.Invoice_Frequency_Cd  
                                             , src.Invoice_Frequency_Pattern_Cd  
                                             , src.Expected_Invoice_Raised_Day  
                                             , src.Expected_Invoice_Received_Day  
                                             , src.Expected_Invoice_Raised_Month  
                                             , src.Expected_Invoice_Received_Month  
                                             , src.Expected_Invoice_Raised_Day_Lag  
                                             , src.Expected_Invoice_Received_Day_Lag  
                                             , src.User_Info_Id  
                                             , src.TS  
                                             , src.User_Info_Id  
                                             , src.TS  
                                             , src.Custom_Days  
                                             , src.Custom_Invoice_Received_Day)  
        WHEN NOT MATCHED BY SOURCE AND Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id THEN  
            DELETE  
        WHEN MATCHED THEN UPDATE SET  
                              tgt.Invoice_Frequency_Cd = src.Invoice_Frequency_Cd  
                              , Invoice_Frequency_Pattern_Cd = src.Invoice_Frequency_Pattern_Cd  
                              , Expected_Invoice_Raised_Day = src.Expected_Invoice_Raised_Day  
                              , Expected_Invoice_Received_Day = src.Expected_Invoice_Received_Day  
                              , Expected_Invoice_Raised_Month = src.Expected_Invoice_Raised_Month  
                              , Expected_Invoice_Received_Month = src.Expected_Invoice_Received_Month  
                              , Expected_Invoice_Raised_Day_Lag = src.Expected_Invoice_Raised_Day_Lag  
                              , Expected_Invoice_Received_Day_Lag = src.Expected_Invoice_Received_Day_Lag  
                              , tgt.Updated_User_Id = src.User_Info_Id  
                              , tgt.Last_Change_Ts = src.TS  
                              , tgt.Custom_Days = src.Custom_Days  
                              , tgt.Custom_Invoice_Received_Day = src.Custom_Invoice_Received_Day;  
  
  
  
  
  
  
    END    ;
GO
GRANT EXECUTE ON  [dbo].[Account_Invoice_Collection_Frequency_And_Expected_Dtl_Merge] TO [CBMSApplication]
GO
