SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE proc [dbo].[cbmsSourcingGraph_Save]
(
	@MyAccountId int
	,@sourcing_graph_id int = null
	,@source_type_id int
	,@pricing_point_id int
	,@strip_type_id int
	,@from_date datetime = null
	,@to_date datetime = null
	,@single_date datetime = null
	,@position int
)
as

if @position is null
	set @position = 0

if exists(select * from sourcing_graph where user_info_id = @MyAccountId and position = @position)
begin
	update sourcing_graph
	set source_type_id = @source_type_id
	,pricing_point_id = @pricing_point_id
	,strip_type_id = @strip_type_id
	,from_date = @from_date
	,to_date = @to_date
	,single_date = @single_date
	where user_info_id = @MyAccountId
	and position = @position
end

else
begin
	insert sourcing_graph
	(
		source_type_id
		,pricing_point_id
		,strip_type_id
		,from_date
		,to_date
		,single_date
		,position
		,user_info_id
	)
	values
	(
		@source_type_id
		,@pricing_point_id
		,@strip_type_id
		,@from_date
		,@to_date
		,@single_date
		,@position
		,@MyAccountId
	)
end
GO
GRANT EXECUTE ON  [dbo].[cbmsSourcingGraph_Save] TO [CBMSApplication]
GO
