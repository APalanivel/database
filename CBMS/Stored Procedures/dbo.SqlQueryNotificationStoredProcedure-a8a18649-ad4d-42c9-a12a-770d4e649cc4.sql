SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SqlQueryNotificationStoredProcedure-a8a18649-ad4d-42c9-a12a-770d4e649cc4] AS BEGIN BEGIN TRANSACTION; RECEIVE TOP(0) conversation_handle FROM [SqlQueryNotificationService-a8a18649-ad4d-42c9-a12a-770d4e649cc4]; IF (SELECT COUNT(*) FROM [SqlQueryNotificationService-a8a18649-ad4d-42c9-a12a-770d4e649cc4] WHERE message_type_name = 'http://schemas.microsoft.com/SQL/ServiceBroker/DialogTimer') > 0 BEGIN if ((SELECT COUNT(*) FROM sys.services WHERE name = 'SqlQueryNotificationService-a8a18649-ad4d-42c9-a12a-770d4e649cc4') > 0)   DROP SERVICE [SqlQueryNotificationService-a8a18649-ad4d-42c9-a12a-770d4e649cc4]; if (OBJECT_ID('SqlQueryNotificationService-a8a18649-ad4d-42c9-a12a-770d4e649cc4', 'SQ') IS NOT NULL)   DROP QUEUE [SqlQueryNotificationService-a8a18649-ad4d-42c9-a12a-770d4e649cc4]; DROP PROCEDURE [SqlQueryNotificationStoredProcedure-a8a18649-ad4d-42c9-a12a-770d4e649cc4]; END COMMIT TRANSACTION; END
GO
