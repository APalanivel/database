SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sourcing_Manager_Dtl_Sel]
           
DESCRIPTION:             
			To get Sourcing Managers
			
INPUT PARAMETERS:            
	Name			DataType	Default		Description  
---------------------------------------------------------------------------------  


OUTPUT PARAMETERS:
	Name			DataType		Default		Description  
---------------------------------------------------------------------------------  


 USAGE EXAMPLES:
---------------------------------------------------------------------------------  

	EXEC dbo.Sourcing_Manager_Dtl_Sel


 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date			Modification
------------------------------------------------------------
	RR			2015-07-24		Global Sourcing - Created
								
******/
CREATE PROCEDURE [dbo].[Sourcing_Manager_Dtl_Sel]
AS 
BEGIN

      SET NOCOUNT ON;
      
      WITH  Cte_SM
              AS ( SELECT
                        smui.USER_INFO_ID AS Sourcing_Manager_Id
                       ,smui.FIRST_NAME + ' ' + smui.LAST_NAME AS Sourcing_Manager_Name
                       ,gi.GROUP_NAME
                   FROM
                        dbo.USER_INFO smui
                        INNER JOIN dbo.USER_INFO_GROUP_INFO_MAP uigim
                              ON smui.USER_INFO_ID = uigim.USER_INFO_ID
                        INNER JOIN dbo.GROUP_INFO gi
                              ON uigim.GROUP_INFO_ID = gi.GROUP_INFO_ID
                   WHERE
                        gi.GROUP_NAME IN ( 'supply', 'sourcing.manager' )
                        AND smui.IS_HISTORY = 0
                   GROUP BY
                        smui.USER_INFO_ID
                       ,smui.FIRST_NAME + ' ' + smui.LAST_NAME
                       ,gi.GROUP_NAME)
            SELECT
                  Sourcing_Manager_Id
                 ,Sourcing_Manager_Name
            FROM
                  Cte_SM
            GROUP BY
                  Sourcing_Manager_Id
                 ,Sourcing_Manager_Name
            HAVING
                  count(GROUP_NAME) = 2
            
                         
END;
;
GO
GRANT EXECUTE ON  [dbo].[Sourcing_Manager_Dtl_Sel] TO [CBMSApplication]
GO
