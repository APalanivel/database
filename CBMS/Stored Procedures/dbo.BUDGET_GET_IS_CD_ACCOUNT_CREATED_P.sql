SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









--select * from state
--exec BUDGET_GET_IS_CD_ACCOUNT_CREATED_P 452,43
CREATE       PROCEDURE dbo.BUDGET_GET_IS_CD_ACCOUNT_CREATED_P
	@budgetId int
       
	AS
	begin
		set nocount on

		select count(budget_account_id)
		from budget_account bacc
		where cd_create_multiplier is not null 
		and bacc.budget_id =@budgetId
                
                
                
	end




















GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_IS_CD_ACCOUNT_CREATED_P] TO [CBMSApplication]
GO
