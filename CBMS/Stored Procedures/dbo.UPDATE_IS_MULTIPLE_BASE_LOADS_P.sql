SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.UPDATE_IS_MULTIPLE_BASE_LOADS_P
	@is_only_one_baseload BIT,
	@contract_id INT
AS
BEGIN

	SET NOCOUNT ON

	UPDATE dbo.[contract]
	SET IS_ONLY_ONE_BASELOAD = @is_only_one_baseload 
	WHERE contract_id = @contract_id

END
GO
GRANT EXECUTE ON  [dbo].[UPDATE_IS_MULTIPLE_BASE_LOADS_P] TO [CBMSApplication]
GO
