SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




--exec BUDGET_CREATE_REVISED_BUDGET_CONVERSIONS_P -1,-1,1278,1279

CREATE    PROCEDURE dbo.BUDGET_CREATE_REVISED_BUDGET_CONVERSIONS_P
@user_id varchar(10),
@session_id varchar(20),
@budgetId int,
@revisedBudgetId int 



AS
begin
set nocount on



	insert into budget_currency_map(	
				BUDGET_ID,
				CURRENCY_UNIT_ID,
				CONVERSION_FACTOR)

      			select 	@revisedBudgetId,
				CURRENCY_UNIT_ID,
				CONVERSION_FACTOR
               		 from  	budget_currency_map
              	 	 where 	budget_id=@budgetId
	

	
				
end














GO
GRANT EXECUTE ON  [dbo].[BUDGET_CREATE_REVISED_BUDGET_CONVERSIONS_P] TO [CBMSApplication]
GO
