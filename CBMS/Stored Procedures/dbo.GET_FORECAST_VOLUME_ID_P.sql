SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_FORECAST_VOLUME_ID_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(50)	          	
	@clientId      	int       	          	
	@forecast_year 	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE    PROCEDURE dbo.GET_FORECAST_VOLUME_ID_P
@userId varchar(10),
@sessionId varchar(50),
@clientId int,
@forecast_year int
--@forecast_as_of_date datetime
 AS
set nocount on
select   rm_forecast_volume_id from rm_forecast_volume (NOLOCK)
	where  forecast_year=@forecast_year 
		--and forecast_as_of_date=@forecast_as_of_date 
		and client_id = @clientId
GO
GRANT EXECUTE ON  [dbo].[GET_FORECAST_VOLUME_ID_P] TO [CBMSApplication]
GO
