SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_APPROVED_BUDGET_REPORT_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@clientId      	int       	          	
	@divisionId    	int       	          	
	@siteId        	int       	          	
	@startDate     	datetime  	          	
	@endDate       	datetime  	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--exec GET_APPROVED_BUDGET_REPORT_DETAILS_P '1','1',387,-1,-1,'09/01/2004','09/01/2004'



CREATE        PROCEDURE dbo.GET_APPROVED_BUDGET_REPORT_DETAILS_P
-- this procedure is for approved budget report  by suresh Reddy 
@userId varchar(10),
@sessionId varchar(20),
@clientId int,
@divisionId int,
@siteId int,
@startDate Datetime,
@endDate Datetime

--select * from RM_TARGET_BUDGET

AS
set nocount on
IF @siteId = -1 and @divisionId > 0 

	SELECT 
		budget.BUDGET_NAME BUDGET_NAME  ,cli.CLIENT_NAME CLIENT_NAME, cli.CLIENT_ID CLIENT_ID,
		div.DIVISION_ID DIVISION_ID, sit.SITE_ID SITE_ID,sit.site_name SITE_NAME,
		target.MONTH_IDENTIFIER MONTH_IDENTIFIER,
		target.BUDGET_TARGET BUDGET_TARGET
	FROM 
		RM_TARGET_BUDGET target,
		client cli,	
		RM_BUDGET budget 
		left join division div on budget.division_id = div.division_id 
		left join vwsitename sit on budget.site_id = sit.site_id 
	WHERE   
		cli.CLIENT_ID=budget.CLIENT_ID
		AND budget.CLIENT_ID=@clientId
		AND budget.DIVISION_ID=@divisionId 
		AND budget.IS_BUDGET_APPROVED=1
		AND 
		(
			(	budget.BUDGET_START_MONTH >= @startDate 
				AND  budget.BUDGET_START_MONTH <=@endDate) 
			or 

			(budget.BUDGET_START_MONTH >= @startDate
			and  budget.BUDGET_START_MONTH <=@endDate)
		)
		AND 
		(
			target.MONTH_IDENTIFIER >= @startDate 
			AND  target.MONTH_IDENTIFIER <=@endDate
		)
		and budget.rm_budget_id=target.rm_budget_id


--		and budget.SITE_ID=@siteId
ELSE
IF @divisionId = -1 and  @siteId > 0

	SELECT 
		budget.BUDGET_NAME BUDGET_NAME  ,cli.CLIENT_NAME CLIENT_NAME, cli.CLIENT_ID CLIENT_ID,
		div.DIVISION_ID DIVISION_ID, sit.SITE_ID SITE_ID,sit.site_name SITE_NAME,target.MONTH_IDENTIFIER MONTH_IDENTIFIER,
		target.BUDGET_TARGET BUDGET_TARGET
	FROM 
		RM_TARGET_BUDGET target,
		client cli,	
		RM_BUDGET budget 
		left join division div on budget.division_id = div.division_id 
		left join vwsitename sit on budget.site_id = sit.site_id 
	WHERE   
		cli.CLIENT_ID=budget.CLIENT_ID
		AND budget.CLIENT_ID=@clientId
		--AND budget.DIVISION_ID=117 
		and budget.site_id = @siteId
		AND budget.IS_BUDGET_APPROVED=1
		AND 
		(
			(	budget.BUDGET_START_MONTH >= @startDate 
				AND  budget.BUDGET_START_MONTH <=@endDate) 
			or 

			(budget.BUDGET_START_MONTH >= @startDate
			and  budget.BUDGET_START_MONTH <=@endDate)
		)
		AND 
		(
			target.MONTH_IDENTIFIER >= @startDate 
			AND  target.MONTH_IDENTIFIER <=@endDate
		)
		and budget.rm_budget_id=target.rm_budget_id


ELSE
IF @divisionId=-1 AND @siteId=-1
	SELECT 
		budget.BUDGET_NAME BUDGET_NAME  ,cli.CLIENT_NAME CLIENT_NAME, cli.CLIENT_ID CLIENT_ID,
		div.DIVISION_ID DIVISION_ID, sit.SITE_ID SITE_ID,sit.SITE_NAME SITE_NAME,target.MONTH_IDENTIFIER MONTH_IDENTIFIER,
		target.BUDGET_TARGET BUDGET_TARGET
	FROM 
		RM_TARGET_BUDGET target,
		client cli,	
		RM_BUDGET budget 
		left join division div on budget.division_id = div.division_id 
		left join vwsitename sit on budget.site_id = sit.site_id 
	WHERE   
		cli.CLIENT_ID=budget.CLIENT_ID
		AND budget.CLIENT_ID=@clientId
		AND budget.IS_BUDGET_APPROVED=1
		AND 
		(
			(	budget.BUDGET_START_MONTH >= @startDate 
				AND  budget.BUDGET_START_MONTH <=@endDate) 
			or 

			(budget.BUDGET_START_MONTH >= @startDate
			and  budget.BUDGET_START_MONTH <=@endDate)
		)
		and budget.rm_budget_id=target.rm_budget_id


ELSE
IF @divisionId  > 0 and  @siteId > 0

	SELECT 
		budget.BUDGET_NAME BUDGET_NAME  ,cli.CLIENT_NAME CLIENT_NAME, cli.CLIENT_ID CLIENT_ID,
		div.DIVISION_ID DIVISION_ID, sit.SITE_ID SITE_ID,sit.SITE_NAME SITE_NAME,target.MONTH_IDENTIFIER MONTH_IDENTIFIER,
		target.BUDGET_TARGET BUDGET_TARGET
	FROM 
		RM_TARGET_BUDGET target,
		client cli,	
		RM_BUDGET budget 
		left join division div on budget.division_id = div.division_id 
		left join vwsitename sit on budget.site_id = sit.site_id 
	WHERE   
		cli.CLIENT_ID=budget.CLIENT_ID
		AND budget.CLIENT_ID=@clientId

		--AND budget.DIVISION_ID=117 
		and budget.site_id = @siteId
		AND budget.IS_BUDGET_APPROVED=1
		AND 
		(
			(	budget.BUDGET_START_MONTH >= @startDate 
				AND  budget.BUDGET_START_MONTH <=@endDate) 
			or 

			(budget.BUDGET_START_MONTH >= @startDate
			and  budget.BUDGET_START_MONTH <=@endDate)
		)
		AND 
		(
			target.MONTH_IDENTIFIER >= @startDate 
			AND  target.MONTH_IDENTIFIER <=@endDate
		)
		and budget.rm_budget_id=target.rm_budget_id
GO
GRANT EXECUTE ON  [dbo].[GET_APPROVED_BUDGET_REPORT_DETAILS_P] TO [CBMSApplication]
GO
