SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 /******                        
Name:  dbo.EC_Calc_Value_Name_Exist                
                        
Description:                        
   This sproc checks if the calc value naem  is already present for the Given calc iud.                
            If already exists then return 1 else 0.                          
                        
 Input Parameters:                        
    Name        DataType   Default   Description                          
----------------------------------------------------------------------------------------                          
    @Calc_Value_Name     NVARCHAR(250)          
    @EC_Calc_Val_Id      INT      NULL          
              
 Output Parameters:                              
    Name        DataType   Default   Description                          
----------------------------------------------------------------------------------------                          
                        
 Usage Examples:                            
----------------------------------------------------------------------------------------             
          
EXEC dbo.EC_Calc_Value_Name_Exist           
     @Calc_Value_Name= 'CAVmd'          
     ,@State_Id=25          
     ,@Commodity_Id=290          
     ,@Ec_Calc_Val_Id=35          
     ,@EC_Meter_Attribute_Id=27          
     ,@EC_Meter_Attribute_Value='MD2'          
          
          
EXEC dbo.EC_Calc_Value_Name_Exist           
     @Calc_Value_Name= 'CAVmd'          
     ,@State_Id=25          
     ,@Commodity_Id=290          
     ,@Ec_Calc_Val_Id= Null          
     ,@EC_Meter_Attribute_Id=27          
     ,@EC_Meter_Attribute_Value='MD2'          
              
           
Author Initials:                        
    Initials  Name                        
----------------------------------------------------------------------------------------                          
 NR      Narayana Reddy           
 RKV     Ravi Kumar Vegesna  
 VRV     Venkata Reddy Vanga           
                        
Modifications:                        
    Initials        Date   Modification                        
----------------------------------------------------------------------------------------                          
    NR    2015-04-22  Created For AS400.                   
    RKV    2015-11-04  Added Parameters Commodity_Id,state_id as Part of AS400-PII   
 VRV     2020-01-08 as part of MAINT-9566 removed @EC_Meter_Attribute_Id,@EC_Meter_Attribute_Value parameters and   
         added tvp to inserti multiple meter attributes to get duplicate calc vals having multiple meter attributes.        
                          
******/           
CREATE PROCEDURE [dbo].[EC_Calc_Value_Name_Exist]              
      (               
       @Calc_Value_Name NVARCHAR(255)              
      ,@State_Id INT              
      ,@Commodity_Id INT              
      ,@Ec_Calc_Val_Id INT = NULL              
      ,@EC_Calc_Val_Meter_Attribute_Value tvp_EC_Calc_Val_Meter_Attribute_Value READONLY )              
AS               
BEGIN              
      SET NOCOUNT ON         
      
   DECLARE @Count int,      
            @SQL nvarchar(Max);      
         
    CREATE  TABLE #EC_Calc_Val_Meter_Attribute_Value (EC_Meter_Attribute_Id int,      
                                                      EC_Meter_Attribute_Value varchar(50));      
         
   INSERT INTO   #EC_Calc_Val_Meter_Attribute_Value      
   SELECT * FROM   @EC_Calc_Val_Meter_Attribute_Value WHERE ISNULL(NULLIF(EC_Meter_Attribute_Value,''),NULL) IS not null ;       
      
   SET  @Count = (SELECT Count(*) FROM   #EC_Calc_Val_Meter_Attribute_Value);      
        
    
IF @Count>=1    
    
BEGIN    
                 
SELECT  DISTINCT          
            ecv.EC_Calc_Val_Id            
                  
           FROM dbo.EC_Calc_Val ecv JOIN dbo.EC_Calc_Val_Meter_Attribute ecvma       
ON ecv.EC_Calc_Val_Id=ecvma.EC_Calc_Val_Id JOIN #EC_Calc_Val_Meter_Attribute_Value ecvmav ON       
ecvma.EC_Meter_Attribute_Id=ecvmav.EC_Meter_Attribute_Id AND ecvma.EC_Meter_Attribute_Value=ecvmav.EC_Meter_Attribute_Value       
      WHERE            
            ecv.Commodity_Id =@Commodity_Id          
            AND ecv.State_Id = @State_Id            
            AND ecv.Calc_Value_Name = LTRIM(RTRIM(@Calc_Value_Name))          
            AND ( @Ec_Calc_Val_Id IS NULL            
                  OR ecv.EC_Calc_Val_Id <> @Ec_Calc_Val_Id )    
   AND ecv.EC_Calc_Val_Id IN ( SELECT EC_Calc_Val_Id FROM (    
 SELECT CounT(*) AS Count,ecv.EC_Calc_Val_Id   FROM dbo.EC_Calc_Val ecv  JOIN dbo.EC_Calc_Val_Meter_Attribute ecvma     
 join #EC_Calc_Val_Meter_Attribute_Value ecvmav ON       
ecvma.EC_Meter_Attribute_Id=ecvmav.EC_Meter_Attribute_Id AND ecvma.EC_Meter_Attribute_Value=ecvmav.EC_Meter_Attribute_Value     
   ON ecv.EC_Calc_Val_Id=ecvma.EC_Calc_Val_Id     
   WHERE                
            ecv.Commodity_Id = @Commodity_Id          
            AND ecv.State_Id = @State_Id           
            AND ecv.Calc_Value_Name = LTRIM(RTRIM(@Calc_Value_Name))            
            AND ( @Ec_Calc_Val_Id IS NULL            
                  OR ecv.EC_Calc_Val_Id <> @Ec_Calc_Val_Id )     
      GROUP BY ecv.EC_Calc_Val_Id )x WHERE x.Count=@Count)    
AND ecv.EC_Calc_Val_Id IN ( SELECT x.EC_Calc_Val_Id from    
 (SELECT CounT(*) AS Count,ecv.EC_Calc_Val_Id FROM dbo.EC_Calc_Val ecv  JOIN dbo.EC_Calc_Val_Meter_Attribute ecvma     
   ON ecv.EC_Calc_Val_Id=ecvma.EC_Calc_Val_Id     
   WHERE                
            ecv.Commodity_Id = @Commodity_Id          
            AND ecv.State_Id = @State_Id           
            AND ecv.Calc_Value_Name = LTRIM(RTRIM(@Calc_Value_Name))            
            AND ( @Ec_Calc_Val_Id IS NULL            
                  OR ecv.EC_Calc_Val_Id <> @Ec_Calc_Val_Id )     
      GROUP BY ecv.EC_Calc_Val_Id) x WHERE x.Count=@Count )    
    
    
  END;    
ELSE     
    
BEGIN     
    
 SELECT            
            ecv.EC_Calc_Val_Id            
      FROM            
            dbo.EC_Calc_Val ecv            
      WHERE       
            ecv.Commodity_Id = @Commodity_Id            
            AND ecv.State_Id = @State_Id            
            AND ecv.Calc_Value_Name = LTRIM(RTRIM(@Calc_Value_Name))            
            AND ( @Ec_Calc_Val_Id IS NULL            
                  OR ecv.EC_Calc_Val_Id <> @Ec_Calc_Val_Id )      
   AND  ecv.EC_Calc_Val_Id    NOT IN (SELECT  EC_Calc_Val_Id FROM dbo.EC_Calc_Val_Meter_Attribute)           
                 
    
END;    
      
      
DROP TABLE #EC_Calc_Val_Meter_Attribute_Value              
                          
                               
END;            
              
              
              
              
              
; 
GO

GRANT EXECUTE ON  [dbo].[EC_Calc_Value_Name_Exist] TO [CBMSApplication]
GO
