
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
NAME:  
	dbo.Cost_Usage_Account_Dtl_Sel_For_Supplier_Account_By_Commodity_Service_Month

DESCRIPTION:  

INPUT PARAMETERS:  
Name      DataType		   Default		Description  
------------------------------------------------------------  
	@Site_Client_Hier_Id	int
	@account_id				int  
	@Commodity_id			int
	@Begin_Dt				Date
	@End_Dt					Date
	@Currency_Unit_Id		int      

OUTPUT PARAMETERS:
Name      DataType		Default		Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	 EXEC Cost_Usage_Account_Dtl_Sel_For_Supplier_Account_By_Commodity_Service_Month
			  @Account_Id = 367352
			 ,@Site_Client_Hier_Id = 1131
			 ,@Begin_Dt = '1/1/2012'
			 ,@End_Dt = '12/1/2012'
			 ,@Currency_Unit_Id = 3
			 ,@Commodity_id = 291

	 EXEC Cost_Usage_Account_Dtl_Sel_For_Supplier_Account_By_Commodity_Service_Month 
			  @Account_Id = 452845
			 ,@Site_Client_Hier_Id = 1130
			 ,@Begin_Dt = '1/1/2012'
			 ,@End_Dt = '12/1/2012'
			 ,@Currency_Unit_Id = 3
			 ,@Commodity_id = 291

 EXEC Cost_Usage_Account_Dtl_Sel_For_Supplier_Account_By_Commodity_Service_Month
			  @Account_Id = 439139
			 ,@Site_Client_Hier_Id = 1131
			 ,@Begin_Dt = '1/1/2012'
			 ,@End_Dt = '12/1/2012'
			 ,@Currency_Unit_Id = 3
			 ,@Commodity_id = 290

	SELECT TOP 10
		  cuad.*
	FROM
		  dbo.Cost_Usage_Account_Dtl cuad
		  JOIN core.Client_Hier_Account cha
				ON cha.Account_id = cuad.Account_id
	WHERE
		  cha.Account_Type = 'Supplier'
		  AND cuad.Service_Month = '01-01-2012'
		  AND cuad.Bucket_Master_Id = 100991

AUTHOR INITIALS:
Initials	Name  
------------------------------------------------------------  
SSR			Sharad Srivastava
HG			Harihara Suthan G
AP			Athmaram Pabbathi
SP			Sandeep Pigilam

MODIFICATIONS   
Initials	Date	Modification
------------------------------------------------------------
SSR		02/08/2010  Created
HG		04/06/2010  vwAccountmeter replaced by base tables, unnecessary Account table join removed from the main query.
						Isnull function used to get the conversion factor removed and used bucket type to get the conversion factors.						
HG		04/29/2010  Unit Cost added as one of the row with other bucket values as per the variance page requirement						
DMR		09/10/2010  Modified for Quoted_Identifier
AP		09/09/2011  Following changes made as apart of Addl Data Prj
					   - Removed UOM_Type_Id parameter and used Default_UOM_Type_Id from Bucket_Master table
					   - Replaced CTE_Account with Subquery on Client_Hier_Account table
					   - Replaced CTE_Cu_Account_Dtl with @Cost_Usage_Account_Dtl table variable
					   - Removed hardcoded buckets and used table variable & dbo.Cost_usage_Bucket_Sel_By_Commodity to load buckets.
AP		 03/27/2012 Added @Site_Client_Hier_Id as a parameter; removed unused parameter @Client_Id and modified SELECT used for currency_group_id 
HG		 2012-07-09	MAINT-1355, modified to fix the Unit cost calculation to consider only Total Usage/ Volume determinant
								--UNION changed to UNION ALL as it is always going to return unique set of values
HG		2013-07-19	Uom_Id and Uom_Name add to the out put as required by Detailed Variance Enhancement
SP		 2016-13-10	Variance Test Enhancements,Used CURRENCY_UNIT_COUNTRY_MAP for default currency_id and Country_Commodity_Uom for default uom_id. 	

******/
CREATE PROCEDURE [dbo].[Cost_Usage_Account_Dtl_Sel_For_Supplier_Account_By_Commodity_Service_Month]
      ( 
       @Site_Client_Hier_Id INT
      ,@Account_Id INT
      ,@Commodity_Id INT
      ,@Begin_Dt DATE
      ,@End_Dt DATE
      ,@Currency_Unit_Id INT )
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE
            @Currency_Group_Id INT
           ,@Currency_Name VARCHAR(200)
           ,@Usage_Bucket_Stream_Cd INT
           ,@Country_Id INT
           ,@Country_Default_Uom_Type_Id INT           

      DECLARE @Cost_Usage_Account_Dtl TABLE
            ( 
             Service_Month DATE
            ,Account_Id INT
            ,Bucket_Type VARCHAR(25)
            ,Bucket_Name VARCHAR(255)
            ,Bucket_Value DECIMAL(28, 10)
            ,Total_Cost DECIMAL(28, 10)
            ,Volume DECIMAL(28, 10)
            ,UOM_Id INT
            ,UOM_Name VARCHAR(200) )


      SELECT
            @Usage_Bucket_Stream_Cd = cd.Code_Id
      FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code cd
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'BucketStream'
            AND cd.Code_Value = 'Usage';
            
      SELECT
            @Currency_Group_Id = ch.Client_Currency_Group_Id
           ,@Country_Id = ch.Country_Id
      FROM
            Core.Client_Hier ch
      WHERE
            ch.Client_Hier_Id = @Site_Client_Hier_Id


      SELECT
            @Currency_Unit_Id = MAX(cucm.CURRENCY_UNIT_ID)
      FROM
            dbo.CURRENCY_UNIT_COUNTRY_MAP cucm
      WHERE
            cucm.Country_Id = @Country_Id
	  
      SELECT
            @Currency_Name = cu.CURRENCY_UNIT_NAME
      FROM
            dbo.CURRENCY_UNIT cu
      WHERE
            cu.CURRENCY_UNIT_ID = @Currency_Unit_Id   

      SELECT
            @Country_Default_Uom_Type_Id = ccu.Default_Uom_Type_Id
      FROM
            dbo.Country_Commodity_Uom ccu
      WHERE
            ccu.Commodity_Id = @Commodity_Id
            AND ccu.COUNTRY_ID = @Country_Id
                        

      INSERT      INTO @Cost_Usage_Account_Dtl
                  ( 
                   Service_Month
                  ,Account_Id
                  ,Bucket_Type
                  ,Bucket_Name
                  ,Bucket_Value
                  ,Total_Cost
                  ,Volume
                  ,UOM_Id
                  ,UOM_Name )
                  SELECT
                        cuad.Service_Month
                       ,cuad.Account_ID
                       ,bkt_cd.Code_Value AS Bucket_Type
                       ,bm.Bucket_Name
                       ,cuad.Bucket_Value * ( CASE WHEN bkt_cd.Code_Value = 'Charge' THEN cc.Conversion_Factor
                                                   WHEN bkt_Cd.Code_Value = 'Determinant' THEN uc.Conversion_Factor
                                              END ) Bucket_Value
                       ,( CASE WHEN bkt_cd.Code_Value = 'Charge' THEN cuad.Bucket_Value * cc.Conversion_Factor
                          END ) AS Total_Cost
                       ,( CASE WHEN bkt_Cd.Code_Value = 'Determinant' THEN cuad.Bucket_Value * uc.Conversion_Factor
                          END ) AS Volume
                       ,CASE WHEN bkt_cd.Code_Value = 'Charge' THEN @Currency_Unit_Id
                             ELSE ( CASE WHEN bm.Bucket_Stream_Cd = @Usage_Bucket_Stream_Cd THEN @Country_Default_Uom_Type_Id
                                         ELSE bm.Default_Uom_Type_Id
                                    END )
                        END UOM_Id
                       ,CASE WHEN bkt_cd.Code_Value = 'Charge' THEN @Currency_Name
                             ELSE en.ENTITY_NAME
                        END UOM_Name
                  FROM
                        dbo.Cost_Usage_Account_Dtl cuad
                        INNER JOIN dbo.Bucket_master bm
                              ON bm.Bucket_Master_Id = cuad.Bucket_Master_Id
                        INNER JOIN dbo.Code bkt_cd
                              ON bkt_cd.Code_Id = bm.Bucket_Type_Cd
                        INNER JOIN ( SELECT
                                          ua.Account_Id
                                         ,ua.Client_Hier_Id
                                     FROM
                                          Core.Client_Hier_Account sa
                                          INNER JOIN core.Client_Hier_Account ua
                                                ON sa.Meter_Id = ua.Meter_Id
                                     WHERE
                                          sa.Account_Id = @Account_Id
                                          AND sa.Client_Hier_Id = @Site_Client_Hier_Id
                                          AND sa.Commodity_Id = @Commodity_Id
                                          AND sa.Account_Type = 'Supplier'
                                          AND ua.Account_Type = 'Utility'
                                          AND ( sa.Supplier_Meter_Disassociation_Date > sa.Supplier_Account_begin_Dt
                                                OR sa.Supplier_Meter_Disassociation_Date IS NULL )
                                     GROUP BY
                                          ua.Account_Id
                                         ,ua.Client_Hier_Id ) uam
                              ON cuad.account_id = uam.account_id
                                 AND cuad.Client_Hier_Id = uam.Client_Hier_Id
                        LEFT OUTER JOIN dbo.consumption_unit_conversion uc
                              ON uc.Base_Unit_ID = cuad.UOM_Type_Id
                                 AND uc.Converted_Unit_ID = ( CASE WHEN bm.Bucket_Stream_Cd = @Usage_Bucket_Stream_Cd THEN @Country_Default_Uom_Type_Id
                                                                   ELSE bm.Default_Uom_Type_Id
                                                              END )
                        LEFT OUTER JOIN dbo.Currency_Unit_Conversion cc
                              ON cc.Currency_Group_Id = @Currency_Group_Id
                                 AND cc.Base_Unit_Id = cuad.CURRENCY_UNIT_ID
                                 AND cc.Converted_Unit_Id = @Currency_Unit_Id
                                 AND cc.Conversion_Date = cuad.Service_Month
                        LEFT OUTER JOIN dbo.ENTITY en
                              ON en.ENTITY_ID = ( CASE WHEN bm.Bucket_Stream_Cd = @Usage_Bucket_Stream_Cd THEN @Country_Default_Uom_Type_Id
                                                       ELSE bm.Default_Uom_Type_Id
                                                  END )
                  WHERE
                        bm.Commodity_Id = @Commodity_id
                        AND bm.Is_Required_For_Variance_Test = 1
                        AND cuad.Service_Month BETWEEN @Begin_Dt AND @End_Dt

      SELECT
            cuad.Service_Month
           ,cuad.Account_ID
           ,cuad.Bucket_Type
           ,cuad.Bucket_Name
           ,cuad.Bucket_Value
           ,cuad.Uom_Id
           ,cuad.Uom_Name
      FROM
            @Cost_Usage_Account_Dtl cuad
      UNION ALL
      SELECT
            cuad.Service_Month
           ,cuad.Account_ID
           ,'Charge' Bucket_Type
           ,'Unit Cost' Bucket_Name
           ,SUM(cuad.Total_Cost) / NULLIF(SUM(cuad.Volume), 0) AS Bucket_Value
           ,@Currency_Unit_Id AS Uom_Id
           ,@Currency_Name AS Uom_Name
      FROM
            @Cost_Usage_Account_Dtl cuad
      WHERE
            cuad.Bucket_Name IN ( 'Total Cost', 'Total Usage', 'Volume' )
      GROUP BY
            cuad.Account_Id
           ,cuad.Service_Month
      ORDER BY
            cuad.Service_Month
           ,cuad.Bucket_Type DESC
           ,cuad.Bucket_Name
END;
;


;
GO




GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Dtl_Sel_For_Supplier_Account_By_Commodity_Service_Month] TO [CBMSApplication]
GO
