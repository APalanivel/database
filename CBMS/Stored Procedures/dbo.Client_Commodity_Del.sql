SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******    
NAME:    
dbo.Client_Commodity_Del  

DESCRIPTION:    
Used to delete Client_Commodity_detail and Client_Commodity table  

INPUT PARAMETERS:    
Name			     DataType Default Description    
------------------------------------------------------------    
@Client_Commodity_Id	INT  
      
OUTPUT PARAMETERS:    
Name			     DataType Default Description    
------------------------------------------------------------    
USAGE EXAMPLES:    
------------------------------------------------------------  

EXEC dbo.Client_Commodity_Del  1

AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
GB   Geetansu Behera    
CMH  Chad Hattabaugh     
HG   Hari   
SKA  Shobhit Kr Agrawal  

MODIFICATIONS     
Initials Date  Modification    
------------------------------------------------------------    
GB				Created  
SKA   26-AUG-09 Modified  REMOVED SUB QUERY AND USED INNER JOIN
SKA   06-SEP-09 Added two more delete statement for GHG_Tracking_SUM & GHG_Tracking

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE  PROCEDURE [dbo].[Client_Commodity_Del]  
    @Client_Commodity_Id INT   
AS  

BEGIN  
SET NOCOUNT ON  
	BEGIN TRY
		BEGIN TRAN
		
			DELETE ghgsum
			FROM
				Su.GHG_Tracking_SUM ghgsum
			INNER JOIN
				Core.Client_Commodity cc
			ON cc.Client_Id = ghgsum.Client_Id
				AND cc.Commodity_Id = ghgsum.Commodity_Id
				AND cc.Scope_Cd = ghgsum.Scope_Cd
			WHERE cc.Client_Commodity_Id = @Client_Commodity_Id
			
			DELETE ghg
			FROM
				Su.GHG_Tracking ghg
			INNER JOIN
				Core.Client_Commodity_Detail ccd
			ON ccd.Client_Commodity_Detail_Id = ghg.Client_Commodity_Detail_Id
			WHERE ccd.Client_Commodity_Id = @Client_Commodity_Id

			DELETE  cdef
			FROM Su.Client_Commodity_Detail_Emission_Factor cdef
			INNER JOIN
				Core.Client_Commodity_detail ccd
			ON ccd.client_commodity_detail_id = cdef.client_commodity_detail_id
			WHERE Client_Commodity_Id = @Client_Commodity_Id

			 
			 DELETE Core.Client_Commodity_detail   
			 WHERE Client_Commodity_Id = @Client_Commodity_Id   
			  
			 DELETE Core.Client_Commodity   
			 WHERE Client_Commodity_Id = @Client_Commodity_Id   
			 
		COMMIT TRAN  
	END TRY
	
	BEGIN CATCH
		ROLLBACK TRAN
		EXEC usp_RethrowError	
	END CATCH

END
GO
GRANT EXECUTE ON  [dbo].[Client_Commodity_Del] TO [CBMSApplication]
GO
