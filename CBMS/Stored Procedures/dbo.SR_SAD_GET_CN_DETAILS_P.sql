SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_CN_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@srDealTicketId	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--exec dbo.SR_SAD_GET_CN_DETAILS_P 824
--select * from SR_DT_TRANSPORT_TERMS_CONDITIONS where sr_deal_ticket_id = 824

CREATE  PROCEDURE dbo.SR_SAD_GET_CN_DETAILS_P

@srDealTicketId int

AS
set nocount on
select 
	sddp.DELIVERY_POINT_NAME, sddp.PRICING_METHODOLOGY,
	sdttc.TRANSPORTATION_TYPE_ID, sdttc.SUPPLY_TYPE_ID, sdttc.INCLUSIVE_OF_FUEL_TYPE_ID


from
	SR_DT_DELIVERY_POINT sddp, SR_DT_TRANSPORT_TERMS_CONDITIONS sdttc, SR_DEAL_TICKET sdt

where
	sdttc.SR_DEAL_TICKET_ID = sdt.SR_DEAL_TICKET_ID and
	sddp.SR_DEAL_TICKET_ID = sdt.SR_DEAL_TICKET_ID and
	sdt.SR_DEAL_TICKET_ID = @srDealTicketId

group by sddp.DELIVERY_POINT_NAME, sddp.PRICING_METHODOLOGY,
	 sdttc.TRANSPORTATION_TYPE_ID, sdttc.SUPPLY_TYPE_ID, sdttc.INCLUSIVE_OF_FUEL_TYPE_ID
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_CN_DETAILS_P] TO [CBMSApplication]
GO
