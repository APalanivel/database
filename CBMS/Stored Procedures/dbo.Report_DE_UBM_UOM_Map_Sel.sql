SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********       

NAME:
    dbo.Report_DE_UBM_UOM_Map_Sel
       
DESCRIPTION:          
			
      
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
--------------------------------------------------------------------------  
          
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
--------------------------------------------------------------------------  
          
 USAGE EXAMPLES:                            
--------------------------------------------------------------------------  

 EXEC dbo.Report_DE_UBM_UOM_Map_Sel
      
 AUTHOR INITIALS:        
       
 Initials              Name        
--------------------------------------------------------------------------  
 AKR                   Ashok Kumar Raju        

  
 MODIFICATIONS:      
          
 Initials              Date             Modification      
--------------------------------------------------------------------------  
 AKR                   2014-10-21      Created.
*********/

CREATE PROCEDURE [dbo].[Report_DE_UBM_UOM_Map_Sel]
AS 
BEGIN
      SET NOCOUNT ON 
      
---UOM Mappings

      SELECT
            UBM_NAME
           ,Commodity_Name AS COMMODITY_NAME
           ,UBM_UNIT_OF_MEASURE_CODE AS UBM_UOM
           ,ENTITY_NAME AS CBMS_UOM
      FROM
            UBM_UNIT_OF_MEASURE_MAP uum
            JOIN ENTITY e
                  ON e.ENTITY_ID = uum.UNIT_OF_MEASURE_TYPE_ID
            JOIN Commodity co
                  ON co.Commodity_Id = uum.COMMODITY_TYPE_ID
            JOIN UBM u
                  ON u.UBM_ID = uum.UBM_ID
      ORDER BY
            UBM_NAME
           ,ENTITY_NAME
END

;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_UBM_UOM_Map_Sel] TO [CBMS_SSRS_Reports]
GO
