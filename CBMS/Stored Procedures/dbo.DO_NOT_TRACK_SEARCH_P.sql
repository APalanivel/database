SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.DO_NOT_TRACK_SEARCH_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@accountNumber 	varchar(200)	NULL      	
	@clientName    	varchar(200)	NULL      	
	@city          	varchar(200)	NULL      	
	@vendorName    	varchar(200)	NULL      	
	@stateId       	int       		NULL      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.DO_NOT_TRACK_SEARCH_P
     @clientName = 'darden'

	EXEC dbo.DO_NOT_TRACK_SEARCH_P
     @vendorName = 'Mobile Gas'



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan Ganesan
	NR			Narayana Reddy

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier

	HG			2013-12-09	MAINT-2419, @Client_Name param width changed to 200 as Client_Name column width on DO_NOT_TRACK increased to VARCHAR(200) to match with Client.Client_Name
	NR			2018-10-31  D20-339 - Added @stateId paraametr in where function insted of join and left join relaced with inner on STATE table.
******/

CREATE PROCEDURE [dbo].[DO_NOT_TRACK_SEARCH_P]
    @accountNumber VARCHAR(200) = NULL
    , @clientName VARCHAR(200) = NULL
    , @city VARCHAR(200) = NULL
    , @vendorName VARCHAR(200) = NULL
    , @stateId INT = NULL
AS
    BEGIN


        SET NOCOUNT ON;

        SET @accountNumber = '%' + @accountNumber + '%';
        SET @clientName = '%' + @clientName + '%';
        SET @city = '%' + @city + '%';
        SET @vendorName = '%' + @vendorName + '%';

        SELECT
            DNT.DO_NOT_TRACK_ID
            , U.USERNAME
            , DNT.CLIENT_NAME
            , DNT.ACCOUNT_ID
            , E.ENTITY_NAME
            , DNT.CLIENT_CITY
            , st.STATE_NAME
            , DNT.VENDOR_NAME
            , DNT.DATE_ADDED
            , DNT.ACCOUNT_NUMBER
            , DNT.STATE_ID
        FROM
            dbo.DO_NOT_TRACK DNT
            INNER JOIN dbo.STATE st
                ON DNT.STATE_ID = st.STATE_ID
            INNER JOIN dbo.ENTITY E
                ON E.ENTITY_ID = DNT.REASON_TYPE_ID
            INNER JOIN dbo.USER_INFO U
                ON U.USER_INFO_ID = DNT.USER_INFO_ID
        WHERE
            (   @accountNumber IS NULL
                OR  DNT.ACCOUNT_NUMBER LIKE @accountNumber)
            AND (   @clientName IS NULL
                    OR  DNT.CLIENT_NAME LIKE @clientName)
            AND (   @city IS NULL
                    OR  DNT.CLIENT_CITY LIKE @city)
            AND (   @vendorName IS NULL
                    OR  DNT.VENDOR_NAME LIKE @vendorName)
            AND (   @stateId IS NULL
                    OR  DNT.STATE_ID = @stateId);


    END;





GO

GRANT EXECUTE ON  [dbo].[DO_NOT_TRACK_SEARCH_P] TO [CBMSApplication]
GO
