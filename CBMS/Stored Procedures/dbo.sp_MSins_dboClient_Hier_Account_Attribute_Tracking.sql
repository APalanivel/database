SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[sp_MSins_dboClient_Hier_Account_Attribute_Tracking]
    @c1 int,
    @c2 int,
    @c3 int,
    @c4 int,
    @c5 int,
    @c6 date,
    @c7 date,
    @c8 nvarchar(255),
    @c9 int,
    @c10 datetime,
    @c11 int,
    @c12 datetime
as
begin  
	insert into [dbo].[Client_Hier_Account_Attribute_Tracking](
		[Client_Hier_Account_Attribute_Tracking_Id],
		[Client_Hier_Id],
		[Account_Id],
		[Commodity_Id],
		[Client_Attribute_Id],
		[Start_Dt],
		[End_Dt],
		[Attribute_Value],
		[Created_User_Id],
		[Created_Ts],
		[Updated_User_Id],
		[Last_Change_Ts]
	) values (
    @c1,
    @c2,
    @c3,
    @c4,
    @c5,
    @c6,
    @c7,
    @c8,
    @c9,
    @c10,
    @c11,
    @c12	) 
end  
GO
