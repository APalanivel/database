SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE dbo.ADD_CBMS_IMAGE_ID_P
	@cbmsImageID int,
	@siteID int
	AS
	begin
		set nocount on

		update	site 
		set	cbms_image_id = @cbmsImageID 
		where	site_id = @siteID

	end


GO
GRANT EXECUTE ON  [dbo].[ADD_CBMS_IMAGE_ID_P] TO [CBMSApplication]
GO
