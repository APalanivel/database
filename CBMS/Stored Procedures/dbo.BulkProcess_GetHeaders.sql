SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
NAME:    [[BulkProcess_GetHeaders]]          
DESCRIPTION:             
------------------------------------------------------------               
 INPUT PARAMETERS:                
 Name   DataType  Default Description                
 @@@Batch_ID            
------------------------------------------------------------                
 OUTPUT PARAMETERS:                
 Name   DataType  Default Description                
------------------------------------------------------------                
 USAGE EXAMPLES:                
------------------------------------------------------------                
AUTHOR INITIALS:                
Initials Name                
------------------------------------------------------------                
PK   Prasan Summit Energy             
             
 MODIFICATIONS                 
 Initials Date   Modification                
------------------------------------------------------------                
          
******/
CREATE PROCEDURE [dbo].[BulkProcess_GetHeaders]
    (@Batch_ID INT)
AS
BEGIN

    SET NOCOUNT ON;

    DECLARE @ProccessId INT;


    SELECT @ProccessId = Xl_Bulk_Data_Process_Id
    FROM
        LOGDB.dbo.[XL_Bulk_Invoice_Process_Batch]
    WHERE
        Xl_Bulk_Invoice_Process_Batch_Id = @Batch_ID;

    SELECT
        c.Code_Value [Process_Name]
       ,Layout_Name
       ,Start_Index
       ,Cell_Padding
    FROM
        dbo.Xl_Layout AS xl
        INNER JOIN dbo.Xl_Bulk_Data_Process dp
            ON xl.Xl_Bulk_Data_Process_Id = dp.Xl_Bulk_Data_Process_Id
        INNER JOIN Code c
            ON c.Code_Id = dp.Xl_Process_Name_Cd
    WHERE
        xl.Xl_Bulk_Data_Process_Id = @ProccessId;


    SELECT Column_Name
    FROM (
             SELECT
                 xlc.Column_Name
                ,xl.Display_Seq
             FROM
                 dbo.Xl_Layout_Column AS xl
                 INNER JOIN dbo.Xl_Column AS xlc
                     ON xl.Xl_Column_Id = xlc.Xl_Column_Id
                 INNER JOIN dbo.Xl_Layout AS xla
                     ON xl.Xl_Layout_Id = xla.Xl_Layout_Id
                 INNER JOIN dbo.Xl_Bulk_Data_Process dp
                     ON dp.Xl_Bulk_Data_Process_Id = xla.Xl_Bulk_Data_Process_Id
                 INNER JOIN Code c
                     ON c.Code_Id = dp.Xl_Process_Name_Cd
             WHERE
                 dp.Xl_Bulk_Data_Process_Id = @ProccessId
             UNION ALL
             SELECT
                 'Status'
                ,100
         ) A
    ORDER BY Display_Seq;

END;
GO
GRANT EXECUTE ON  [dbo].[BulkProcess_GetHeaders] TO [CBMSApplication]
GO
