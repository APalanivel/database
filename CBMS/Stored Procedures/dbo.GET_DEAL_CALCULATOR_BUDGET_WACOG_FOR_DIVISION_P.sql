SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- EXEC GET_DEAL_CALCULATOR_BUDGET_WACOG_FOR_DIVISION_P 1,1,10069,568,'01/01/2008','12/01/2008',2008,2008  
-- EXEC GET_DEAL_CALCULATOR_BUDGET_WACOG_FOR_DIVISION_P 1,1,1043,105,'1-1-2005','12-1-2005',2005,2005  
  
--EXEC GET_DEAL_CALCULATOR_BUDGET_WACOG_FOR_DIVISION_P 1,1,10063,511,'1-1-2007','12-1-2007',2007,2007  
CREATE PROCEDURE dbo.GET_DEAL_CALCULATOR_BUDGET_WACOG_FOR_DIVISION_P
 @userId VARCHAR(10),  
 @sessionId VARCHAR(20),  
 @clientId INT,  
 @divisionId INT,  
 @startDate DATETIME,  
 @endDate DATETIME,  
 @startYear INT,  
 @endYear INT    
AS  
BEGIN  
  
 --FOR DIVISION  
 SET NOCOUNT ON  
  
 DECLARE @Forecast_As_Of_Date_Startyear DATETIME  
  , @Forecast_As_Of_Date_Endyear DATETIME  
  , @InsertedCount INT  
  
 DECLARE @temp_table TABLE(priceVolume DECIMAL(32,16), totalVolume DECIMAL(32,16), BudgetWACOGPRICE DECIMAL(32,16), dealTicketMonth INT, dealTicketYear INT, MONTH_IDENTIFIER DATETIME)  
 DECLARE @ResultSet TABLE(priceVolume DECIMAL(32,16), VOLUME DECIMAL(32,16), FORECAST_ID INT, SITEID INT, TARGETPRICE DECIMAL(32,16), TARGETID INT  
  , DEALMONTHYEAR DATETIME, MI2 DATETIME, MONTH_IDENTIFIER VARCHAR(12))  
  
 DECLARE @tmpResultSet TABLE(priceVolume DECIMAL(32,16), VOLUME DECIMAL(32,16), FORECAST_ID INT, SITEID INT, TARGETPRICE DECIMAL(32,16), TARGETID INT  
  , DEALMONTHYEAR DATETIME, MI2 DATETIME, MONTH_IDENTIFIER VARCHAR(12))  
  
 DECLARE @tblbase TABLE (RM_TARGET_BUDGET_ID INT,DIVISION_ID INT, Site_ID INT, MONTH_IDENTIFIER DATETIME, BUDGET_TARGET DECIMAL(32,16))  
 DECLARE @tblIDColln TABLE (RM_TARGET_BUDGET_ID INT,DIVISION_ID INT, Site_ID INT, MONTH_IDENTIFIER DATETIME, BUDGET_TARGET DECIMAL(32,16))  
 DECLARE @tblIDColln1 TABLE (RM_TARGET_BUDGET_ID INT,DIVISION_ID INT, Site_ID INT, MONTH_IDENTIFIER DATETIME, BUDGET_TARGET DECIMAL(32,16))  
   
 INSERT INTO @tblBase  
 SELECT DISTINCT RM_TARGET_BUDGET_ID  
  , rm_bdg.DIVISION_ID  
  , rm_bdg.Site_ID  
  , RM_TARGET_BUDGET.MONTH_IDENTIFIER  
  , RM_TARGET_BUDGET.BUDGET_TARGET  
 FROM dbo.RM_TARGET_BUDGET  
  JOIN dbo.RM_BUDGET rm_bdg ON rm_bdg.RM_BUDGET_ID = RM_TARGET_BUDGET.RM_BUDGET_ID  
  JOIN dbo.RM_BUDGET rm_bdg_in ON rm_bdg_in.RM_BUDGET_ID = rm_bdg.RM_BUDGET_ID  
 WHERE rm_bdg.CLIENT_ID = @clientId  
  AND rm_bdg.IS_BUDGET_APPROVED = 1  
  AND RM_TARGET_BUDGET.MONTH_IDENTIFIER BETWEEN CONVERT(VARCHAR(12), @startDate, 101) AND CONVERT(VARCHAR(12), @endDate, 101)  
  AND (  
    (CONVERT(VARCHAR(12),@startDate, 101) BETWEEN  rm_bdg_in.BUDGET_START_MONTH AND rm_bdg_in.BUDGET_END_MONTH)  
    OR (CONVERT(VARCHAR(12), @endDate, 101) BETWEEN rm_bdg_in.BUDGET_START_MONTH AND rm_bdg_in.BUDGET_END_MONTH)  
    OR (rm_bdg_in.BUDGET_START_MONTH BETWEEN CONVERT(VARCHAR(12),@startDate, 101) AND CONVERT(VARCHAR(12),@endDate, 101))  
    OR (rm_bdg_in.BUDGET_END_MONTH BETWEEN CONVERT(VARCHAR(12),@startDate, 101) AND CONVERT(VARCHAR(12),@endDate, 101))  
   )  
  
 INSERT INTO @tblIDColln  
 SELECT RM_TARGET_BUDGET_ID  
  , DIVISION_ID  
  , Site_ID  
  , MONTH_IDENTIFIER  
  , BUDGET_TARGET  
 FROM @tblBase  
 WHERE DIVISION_ID = @divisionId  
  AND SITE_ID IS NULL  
  
 INSERT INTO @tblIDColln1  
 SELECT b.RM_TARGET_BUDGET_ID  
  , b.DIVISION_ID  
  , b.Site_ID  
  , b.MONTH_IDENTIFIER  
  , b.BUDGET_TARGET  
 FROM @tblBase b  
  JOIN site s ON s.Site_ID = b.SITE_ID  
 WHERE s.division_id = @divisionId  
  AND b.SITE_ID IS NOT NULL  
  
 SELECT @Forecast_As_Of_Date_Startyear = MAX(FORECAST_AS_OF_DATE) FROM dbo.RM_FORECAST_VOLUME WHERE FORECAST_YEAR = @startYear AND CLIENT_ID=@clientId  
 SELECT @Forecast_As_Of_Date_Endyear = MAX(FORECAST_AS_OF_DATE) FROM dbo.RM_FORECAST_VOLUME WHERE FORECAST_YEAR = @endYear AND CLIENT_ID=@clientId  
  
 --FOR DIVISION  
  
 INSERT INTO @ResultSet  
 SELECT volumedetails.VOLUME * tblTgtBdg.BUDGET_TARGET priceVolume,  
  volumedetails.VOLUME VOLUME,  
  volumedetails.RM_FORECAST_VOLUME_DETAILS_ID FORECAST_ID,  
  volumedetails.SITE_ID SITEID,  
  tblTgtBdg.BUDGET_TARGET TARGETPRICE,  
  tblTgtBdg.RM_TARGET_BUDGET_ID TARGETID,  
  volumedetails.MONTH_IDENTIFIER DEALMONTHYEAR,  
  tblTgtBdg.MONTH_IDENTIFIER MI2,  
  CONVERT (VARCHAR(12), tblTgtBdg.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER  
 FROM dbo.RM_FORECAST_VOLUME_DETAILS volumedetails  
  JOIN dbo.RM_FORECAST_VOLUME volume ON volume.RM_FORECAST_VOLUME_ID = volumedetails.RM_FORECAST_VOLUME_ID  
  JOIN dbo.RM_BUDGET_SITE_MAP ON RM_BUDGET_SITE_MAP.SITE_ID = volumedetails.site_id  
  
--   JOIN dbo.RM_TARGET_BUDGET ON RM_TARGET_BUDGET.MONTH_IDENTIFIER = volumedetails.MONTH_IDENTIFIER  
--   JOIN dbo.RM_BUDGET ON RM_BUDGET.rm_budget_id = RM_TARGET_BUDGET.rm_budget_id  
  
  JOIN @tblIDColln tblTgtBdg ON --tblTgtBdg.RM_TARGET_BUDGET_ID = RM_TARGET_BUDGET.RM_TARGET_BUDGET_ID  
    tblTgtBdg.MONTH_IDENTIFIER = volumedetails.MONTH_IDENTIFIER  
  
  JOIN dbo.Site sit ON sit.Site_ID = volumedetails.SITE_ID  
   AND sit.Division_ID = tblTgtBdg.Division_ID  
--   JOIN @tblIDColln tblDivision ON tblDivision.DIVISION_ID = sit.DIVISION_ID -- As this query is not Working kept subquery in where clause  
  
  LEFT OUTER JOIN @tblIDColln1 tblSite ON tblSite.SITE_ID = volumedetails.SITE_ID  
 WHERE volume.FORECAST_AS_OF_DATE IN (@Forecast_As_Of_Date_Startyear, @Forecast_As_Of_Date_Endyear)  
  AND volume.CLIENT_ID = @clientId  
  AND volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(VARCHAR(12), @startDate, 101) AND CONVERT(VARCHAR(12), @endDate, 101)  
--   AND sit.DIVISION_ID IN (SELECT DIVISION_ID FROM @tblIDColln)  
  AND tblSite.SITE_ID IS NULL -- to Fetch the records not exist in tblSite (NOT IN)  
     
 --client level  
 IF @@ROWCOUNT = 0 -- If no rows inserted in to temp table  
  BEGIN  
  
  INSERT INTO @ResultSet  
  SELECT volumedetails.VOLUME*rm_trg_bdg.BUDGET_TARGET priceVolume,  
   volumedetails.VOLUME VOLUME,  
   volumedetails.RM_FORECAST_VOLUME_DETAILS_ID FORECAST_ID,  
   volumedetails.SITE_ID SITEID,  
   rm_trg_bdg.BUDGET_TARGET TARGETPRICE,  
   rm_trg_bdg.RM_TARGET_BUDGET_ID TARGETID,  
   volumedetails.MONTH_IDENTIFIER DEALMONTHYEAR,  
   rm_trg_bdg.MONTH_IDENTIFIER MI2,  
   CONVERT(VARCHAR(12), rm_trg_bdg.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER  
  FROM dbo.RM_FORECAST_VOLUME_DETAILS volumedetails  
   JOIN dbo.RM_FORECAST_VOLUME volume ON volume.RM_FORECAST_VOLUME_ID = volumedetails.RM_FORECAST_VOLUME_ID  
   JOIN dbo.RM_BUDGET_SITE_MAP ON RM_BUDGET_SITE_MAP.SITE_ID = volumedetails.site_id  
  
--   JOIN dbo.RM_TARGET_BUDGET ON RM_TARGET_BUDGET.MONTH_IDENTIFIER = volumedetails.MONTH_IDENTIFIER  
--   JOIN dbo.RM_BUDGET ON RM_BUDGET.rm_budget_id = RM_TARGET_BUDGET.rm_budget_id  
     
--   JOIN @tblIDColln2 rm_trg_bdg ON rm_trg_bdg.RM_TARGET_BUDGET_id = RM_TARGET_BUDGET.RM_TARGET_BUDGET_ID  
  
   JOIN @tblBase rm_trg_bdg ON rm_trg_bdg.MONTH_IDENTIFIER = volumedetails.MONTH_IDENTIFIER  
      
   JOIN dbo.SITE sit ON sit.SITE_ID = volumedetails.SITE_ID  
   JOIN dbo.DIVISION div ON div.DIVISION_ID = sit.DIVISION_ID  
   --JOIN RM_BUDGET_SITE_MAP rm_bdg_siteMap ON rm_bdg_siteMap.SITE_ID != sit.SITE_ID -- is it required ??  
  
  WHERE volume.CLIENT_ID=@clientId  
   AND volume.FORECAST_AS_OF_DATE IN(@Forecast_As_Of_Date_Startyear, @Forecast_As_Of_Date_Endyear)  
   AND volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(VARCHAR(12), @startDate, 101) AND CONVERT(VARCHAR(12), @endDate, 101)  
   AND div.CLIENT_ID = @clientId     
   AND rm_trg_bdg.DIVISION_ID IS NULL  
   AND rm_trg_bdg.SITE_ID IS NULL  
  
  SET @InsertedCount = @@ROWCOUNT  
  
 END  
  
 --for divisions and sites    
 IF @InsertedCount = 0  
  BEGIN  
  
  INSERT INTO @tmpResultSet  
  SELECT volumedetails.VOLUME*tbl_rm_trg_bdg.BUDGET_TARGET priceVolume,  
   volumedetails.VOLUME VOLUME,  
   volumedetails.RM_FORECAST_VOLUME_DETAILS_ID FORECAST_ID,  
   volumedetails.SITE_ID SITEID,  
   tbl_rm_trg_bdg.BUDGET_TARGET TARGETPRICE,  
   tbl_rm_trg_bdg.RM_TARGET_BUDGET_ID TARGETID,  
   volumedetails.MONTH_IDENTIFIER DEALMONTHYEAR,  
   tbl_rm_trg_bdg.MONTH_IDENTIFIER MI2,  
   CONVERT (VARCHAR(12), tbl_rm_trg_bdg.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER  
  FROM dbo.RM_FORECAST_VOLUME_DETAILS volumedetails  
   JOIN dbo.RM_FORECAST_VOLUME volume ON volume.RM_FORECAST_VOLUME_ID = volumedetails.RM_FORECAST_VOLUME_ID  
   JOIN dbo.RM_BUDGET_SITE_MAP ON RM_BUDGET_SITE_MAP.SITE_ID = volumedetails.site_id  
  
--   JOIN dbo.RM_TARGET_BUDGET ON RM_TARGET_BUDGET.MONTH_IDENTIFIER = volumedetails.MONTH_IDENTIFIER  
--   JOIN dbo.RM_BUDGET ON RM_BUDGET.rm_budget_id = RM_TARGET_BUDGET.rm_budget_id     
   
--   JOIN @tblIDColln tbl_rm_trg_bdg ON tbl_rm_trg_bdg.RM_TARGET_BUDGET_ID = RM_TARGET_BUDGET.RM_TARGET_BUDGET_ID  
   JOIN @tblIDColln tbl_rm_trg_bdg ON tbl_rm_trg_bdg.MONTH_IDENTIFIER = volumedetails.MONTH_IDENTIFIER  
  
   JOIN dbo.Site sit ON sit.Site_ID = volumedetails.SITE_ID  
    AND sit.DIVISION_ID = tbl_rm_trg_bdg.DIVISION_ID  
  
   LEFT OUTER JOIN @tblIDColln1 tblSite ON tblSite.SITE_ID = volumedetails.SITE_ID  
  WHERE volume.CLIENT_ID=@clientId  
   AND volume.FORECAST_AS_OF_DATE IN(@Forecast_As_Of_Date_Startyear, @Forecast_As_Of_Date_Endyear)  
   AND volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(VARCHAR(12), @startDate, 101) AND CONVERT(VARCHAR(12), @endDate, 101)  
--   AND sit.DIVISION_ID IN (SELECT DIVISION_ID FROM @tblIDColln)  
   AND tblSite.Site_ID IS NULL  
  
  --FOR SITE    
  INSERT INTO @tmpResultSet  
  SELECT volumedetails.VOLUME*tbl_rm_trg_bdg.BUDGET_TARGET priceVolume,  
   volumedetails.VOLUME VOLUME,  
   volumedetails.RM_FORECAST_VOLUME_DETAILS_ID FORECAST_ID,  
   volumedetails.SITE_ID SITEID,  
   tbl_rm_trg_bdg.BUDGET_TARGET TARGETPRICE,  
   tbl_rm_trg_bdg.RM_TARGET_BUDGET_ID TARGETID,  
   volumedetails.MONTH_IDENTIFIER DEALMONTHYEAR,  
   tbl_rm_trg_bdg.MONTH_IDENTIFIER MI2,  
   CONVERT (VARCHAR(12), tbl_rm_trg_bdg.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER  
  FROM dbo.RM_FORECAST_VOLUME_DETAILS volumedetails  
   JOIN dbo.RM_FORECAST_VOLUME volume ON volume.RM_FORECAST_VOLUME_ID = volumedetails.RM_FORECAST_VOLUME_ID  
   JOIN dbo.RM_BUDGET_SITE_MAP ON RM_BUDGET_SITE_MAP.SITE_ID = volumedetails.site_id  
  
--   JOIN dbo.RM_TARGET_BUDGET ON RM_TARGET_BUDGET.MONTH_IDENTIFIER = volumedetails.MONTH_IDENTIFIER  
--   JOIN dbo.RM_BUDGET ON RM_BUDGET.rm_budget_id = RM_TARGET_BUDGET.rm_budget_id  
--    AND RM_BUDGET.SITE_ID = volumedetails.site_id  
  
--   JOIN @tblIDColln1 tbl_rm_trg_bdg ON tbl_rm_trg_bdg.RM_TARGET_BUDGET_ID = RM_TARGET_BUDGET.RM_TARGET_BUDGET_ID  
   JOIN @tblIDColln1 tbl_rm_trg_bdg ON tbl_rm_trg_bdg.MONTH_IDENTIFIER = volumedetails.MONTH_IDENTIFIER  
    AND tbl_rm_trg_bdg.SITE_ID = volumedetails.site_id  
  
   JOIN @tblIDColln1 tblSite ON tblSite.Site_ID = volumedetails.SITE_ID  
  WHERE volume.CLIENT_ID=@clientId  
   AND volume.FORECAST_AS_OF_DATE IN(@Forecast_As_Of_Date_Startyear, @Forecast_As_Of_Date_Endyear)  
   AND volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(VARCHAR(12), @startDate, 101) AND CONVERT(VARCHAR(12), @endDate, 101)  
    
  INSERT INTO @ResultSet  
  SELECT DISTINCT priceVolume, VOLUME, FORECAST_ID, SITEID, TARGETPRICE  
   , TARGETID, DEALMONTHYEAR, MI2, MONTH_IDENTIFIER  
  FROM @tmpResultSet  
    
 END  
 -----END OF QUERY HERE-----  
  
 SELECT SUM(a.priceVolume) AS priceVolume ,  
  SUM(a.VOLUME) AS totalVolume ,  
  CASE SUM(a.VOLUME)  
   WHEN 0 THEN 0.0  
   ELSE SUM(a.priceVolume)/SUM(a.VOLUME)  
  END AS BudgetWACOGPRICE,  
  DATEPART(MONTH,a.DEALMONTHYEAR) dealTicketMonth,  
  DATEPART(YEAR,a.DEALMONTHYEAR) dealTicketYear,  
  a.MONTH_IDENTIFIER   
 FROM @ResultSet a  
 GROUP BY DATEPART(MONTH,A.DEALMONTHYEAR)  
  , DATEPART(YEAR,A.DEALMONTHYEAR)  
  , a.MONTH_IDENTIFIER  
  
  
END  
GO
GRANT EXECUTE ON  [dbo].[GET_DEAL_CALCULATOR_BUDGET_WACOG_FOR_DIVISION_P] TO [CBMSApplication]
GO
