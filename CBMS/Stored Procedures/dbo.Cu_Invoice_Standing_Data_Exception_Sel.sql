SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
                    
Name:           
 dbo.[Cu_Invoice_Standing_Data_Exception_Sel]               
                      
Description:                      
   To insert Invoice Exception      
                      
 Input Parameters:                      
 Name							DataType		Default				Description                        
-------------------				-----------		----------			-----------------------------------------------------        
@Cu_Invoice_Id INT
@Exception_Type_Cd INT
@Account_Id INT 
@Commodity_Id INT 
@User_Info_Id   INT  = NULL      
            
Output Parameters:                            
 Name      DataType   Default     Description                        
---------------------------------------------------------------------------------------------        
                      
Usage Examples:                          
---------------------------------------------------------------------------------------------    
     
	EXEC Cu_Invoice_Standing_Data_Exception_Sel 93011324
      
         
Author Initials:                      
 Initials			Name                      
-------------		--------------------------------------------------------------------------------        
HKT					Harish Kumar Tirumandyam               
         
Modifications:                      
Initials		Date				Modification                      
------------	----------------	-----------------------------------------------------------------        
 HKT			2020-04-15			Created For Data Purple      
         
******/
CREATE PROCEDURE [dbo].[Cu_Invoice_Standing_Data_Exception_Sel]
     (
         @Cu_Invoice_Id INT
     )
AS
    BEGIN
        SELECT
            cisde.Cu_Invoice_Id,cisded.Account_Id,cisded.Commodity_Id,et.Code_Dsc Exception_Type,et.Code_Value Exception_Code_val,et.Code_Id Exception_Type_Cd
        FROM
            dbo.Cu_Invoice_Standing_Data_Exception cisde
            INNER JOIN dbo.Cu_Invoice_Standing_Data_Exception_Dtl cisded
                ON cisded.Cu_Invoice_Standing_Data_Exception_Id = cisde.Cu_Invoice_Standing_Data_Exception_Id
			INNER JOIN dbo.Code c
			ON c.Code_Id = cisde.Exception_Status_Cd
			INNER JOIN dbo.Code et
			ON et.Code_Id = cisde.Exception_Type_Cd
        WHERE
            cisde.Cu_Invoice_Id = @Cu_Invoice_Id
			AND c.Code_Value <> 'Closed';
    END;

GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Standing_Data_Exception_Sel] TO [CBMSApplication]
GO
