SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.EC_Contract_Attribute_Ins           
              
Description:              
        To insert Data to EC_Contract_Attribute table.              
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
    @State_Id							INT
    @Commodity_Id						INT
    @EC_Contract_Attribute_Name			NVARCHAR(255)
    @Attribute_Type_Cd					INT
    @User_Info_Id						INT    
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
    @EC_Contract_Attribute_Id			INT
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                
	DECLARE  @EC_Contract_Attribute_Id_Out INT 
	BEGIN TRAN  
	SELECT * FROM EC_Contract_Attribute WHERE EC_Contract_Attribute_Name='Test_AS400_Inserted_1'
    EXEC dbo.EC_Contract_Attribute_Ins 
      @State_Id = 2
     ,@Commodity_Id = 291
     ,@EC_Contract_Attribute_Name = 'Test_AS400_Inserted_1'
     ,@Attribute_Type_Cd = 100026
     ,@User_Info_Id = 49
     ,@EC_Contract_Attribute_Id = @EC_Contract_Attribute_Id_Out OUTPUT
	SELECT @EC_Contract_Attribute_Id_Out AS EC_Contract_Attribute_Id 
	SELECT * FROM EC_Contract_Attribute WHERE EC_Contract_Attribute_Name='Test_AS400_Inserted_1'
	ROLLBACK TRAN             
             
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy               
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2015-04-22		Created For AS400.         
             
******/ 
CREATE PROCEDURE [dbo].[EC_Contract_Attribute_Ins]
      ( 
       @State_Id INT
      ,@Commodity_Id INT
      ,@EC_Contract_Attribute_Name NVARCHAR(255)
      ,@Attribute_Type_Cd INT
      ,@User_Info_Id INT
      ,@EC_Contract_Attribute_Id INT OUTPUT )
AS 
BEGIN
      SET NOCOUNT ON 


      INSERT      INTO dbo.EC_Contract_Attribute
                  ( 
                   State_Id
                  ,Commodity_Id
                  ,EC_Contract_Attribute_Name
                  ,Attribute_Type_Cd
                  ,Created_User_Id
                  ,Created_Ts
                  ,Updated_User_Id
                  ,Last_Change_Ts )
      VALUES
                  ( 
                   @State_Id
                  ,@Commodity_Id
                  ,@EC_Contract_Attribute_Name
                  ,@Attribute_Type_Cd
                  ,@User_Info_Id
                  ,GETDATE()
                  ,@User_Info_Id
                  ,GETDATE() )
                  
      SET @EC_Contract_Attribute_Id = SCOPE_IDENTITY()
            
END




;
GO
GRANT EXECUTE ON  [dbo].[EC_Contract_Attribute_Ins] TO [CBMSApplication]
GO
