SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_UTILITIES_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@RFP_ID        	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE    PROCEDURE dbo.SR_RFP_GET_UTILITIES_INFO_P
@RFP_ID INT

AS
set nocount on
select distinct
	v.vendor_id,
	v.vendor_name	
	
from 
	
	sr_rfp_account rfp_account,
	client c, 
	division d,
	site s, 
	account a,
	vendor v
	 
where 
	rfp_account.sr_rfp_id = @RFP_ID	
	and a.account_id = rfp_account.account_id
	and rfp_account.is_deleted = 0
	and v.vendor_id = a.vendor_id	
	and s.site_id = a.site_id
	and d.division_id = s.division_id
	and c.client_id = d.client_id

order by v.vendor_name
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_UTILITIES_INFO_P] TO [CBMSApplication]
GO
