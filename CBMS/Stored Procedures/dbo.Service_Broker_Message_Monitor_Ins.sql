SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:		Service_Broker_Message_Monitor_Ins

DESCRIPTION:
	Activated by dbo.Service_Broker_Message_Monitor_Queue, it processes messages
	of //Change_Control/Message/SB_Message_Monitor_Ins
	
	Procedure Only Responds with a message of type of //Change_Control/Message/SB_Message_Monitor_Ins


INPUT PARAMETERS:
	Name						DataType		Default	Description
------------------------------------------------------------
	@Message					XML						-- Exists for Consistancy. message type validates to null 
	,@Conversation_Handle		UNIQUEIDENTIFIER		-- Conversation that sent the message

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
 -- Procedure is only executed by 
XML Structure:
<Message_Monitor_Ins>
	<Queue_Name></Queue_Name>
	<Message_Type></Message_Type>
	<Message_Received_Ts></Message_Received_Ts>
	<Message_Sequence_Number></Message_Sequence_Number>
	<Conversation_Group_Id></Conversation_Group_Id>
	<Conversation_Handle></Conversation_Handle>
	<Service_name></Service_name>
	<Contract_name></ontract_name>
	<Message_Body></Message_Body>
	<Message_Status></Message_Status>
</Message_Monitor_Ins>

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DSC			Kaushik
	
MODIFICATIONS

	Initials	Date			Modification
------------------------------------------------------------
	DSC			06/03/2014		Created
******/
CREATE PROCEDURE [dbo].[Service_Broker_Message_Monitor_Ins]
      ( 
       @Message XML
      ,@Conversation_Handle UNIQUEIDENTIFIER )
AS 
BEGIN
      SET NOCOUNT ON; 
	
      INSERT      INTO dbo.Service_Broker_Message_Monitor
                  ( 
                   Queue_Name
                  ,Message_Type
                  ,Message_Received_Ts
                  ,Message_Sequence_Number
                  ,Conversation_Group_Id
                  ,Conversation_Handle
                  ,Service_name
                  ,Contract_name
                  ,Message_Body
                  ,Message_Status )
                  SELECT
                        cng.ch.value('Queue_Name[1]', 'varchar(200)')
                       ,cng.ch.value('Message_Type[1]', 'varchar(200)')
                       ,cng.ch.value('Message_Received_Ts[1]', 'datetime')
                       ,cng.ch.value('Message_Sequence_Number[1]', 'int')
                       ,cng.ch.value('Conversation_Group_Id[1]', 'uniqueidentifier')
                       ,cng.ch.value('Conversation_Handle[1]', 'uniqueidentifier')
                       ,cng.ch.value('Service_name[1]', 'varchar(200)')
                       ,cng.ch.value('Contract_name[1]', 'varchar(200)')
                       ,cng.ch.value('Message_Body[1]', 'varchar(max)')
                       ,cng.ch.value('Message_Status[1]', 'varchar(200)')
                  FROM
                        @Message.nodes('/Message_Monitor_Ins') cng ( ch );
            
            ;
      END CONVERSATION @Conversation_Handle
	
END
;
GO
GRANT EXECUTE ON  [dbo].[Service_Broker_Message_Monitor_Ins] TO [ETL_Execute]
GRANT EXECUTE ON  [dbo].[Service_Broker_Message_Monitor_Ins] TO [sb_Execute]
GO
