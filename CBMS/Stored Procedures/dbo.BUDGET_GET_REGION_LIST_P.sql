SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.BUDGET_GET_REGION_LIST_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE         PROCEDURE dbo.BUDGET_GET_REGION_LIST_P
@userId varchar(10),
@sessionId varchar(20)


AS
begin
set nocount on
select region_id , region_name
from region
order by region_name
end
GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_REGION_LIST_P] TO [CBMSApplication]
GO
