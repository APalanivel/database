
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
	dbo.Report_UtilityAccounts_Rate_sel_by_Utility
	
DESCRIPTION:
	This Procedure is to get All the rates by input parameter Vendor
	
INPUT PARAMETERS:
Name			DataType		Default	Description
-------------------------------------------------------------------------------------------
	
OUTPUT PARAMETERS:
Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------
Report_UtilityAccounts_Rate_sel_by_Utility  4655
	
AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------
SSR			Sharad srivastava
AKR         Ashok Kumar Raju

MODIFICATIONS
Initials	Date		Modification
------------------------------------------------------------
 SSR	   09/05/2010	Created
 AKR       08/29/2011   Modified the sp to return the Rate_ID Rather than ROW_NUMBER() OVER ( ORDER BY r.rate_Name )
*/
CREATE PROC [dbo].[Report_UtilityAccounts_Rate_sel_by_Utility] @Utility INT
AS 
    BEGIN           
          
        SET NOCOUNT ON ;    
          
        SELECT
            0 ID
          , ' None' RATE_NAME
        UNION
        SELECT
            r.RATE_ID
          , r.RATE_NAME
        FROM
            dbo.RATE r
        WHERE
            r.VENDOR_ID = @Utility
       
    END 


GO
GRANT EXECUTE ON  [dbo].[Report_UtilityAccounts_Rate_sel_by_Utility] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_UtilityAccounts_Rate_sel_by_Utility] TO [CBMSApplication]
GO
