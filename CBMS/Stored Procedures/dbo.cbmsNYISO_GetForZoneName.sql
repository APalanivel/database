SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsNYISO_GetForZoneName

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@zone_name     	varchar(200)	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

create   PROCEDURE [dbo].[cbmsNYISO_GetForZoneName]

(
	@zone_name as varchar(200) = null
)
AS
BEGIN

	   select zone_id	
		, zone_name
		, zone_sheet_name
	     from ny_iso
	    where zone_name = @zone_name


END
GO
GRANT EXECUTE ON  [dbo].[cbmsNYISO_GetForZoneName] TO [CBMSApplication]
GO
