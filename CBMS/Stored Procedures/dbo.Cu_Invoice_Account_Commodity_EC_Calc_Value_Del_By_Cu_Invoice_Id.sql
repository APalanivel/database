SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
NAME:   [dbo].[Cu_Invoice_Account_Commodity_EC_Calc_Value_Del_By_Cu_Invoice_Id]    
               
DESCRIPTION:                 
To delete the records from the table Cu_Invoice_Account_Commodity_EC_Calc_Value_Del_By_Cu_Invoice_Id when the Invoice is Backed Out.
               
INPUT PARAMETERS:                
Name					DataType		Default		 Description      
------------------------------------------------------------------------      
@Cu_Invoice_Id			INT                       

      
 OUTPUT PARAMETERS:                
Name					DataType		Default		 Description      
------------------------------------------------------------------------    
             
 USAGE EXAMPLES:                
------------------------------------------------------------------------                     

BEGIN TRAN
SELECT
            *
      FROM
            dbo.Cu_Invoice_Account_Commodity ciac
            INNER JOIN dbo.Cu_Invoice_Account_Commodity_EC_Calc_Value ciacecv
                  ON ciac.Cu_Invoice_Account_Commodity_Id = ciacecv.Cu_Invoice_Account_Commodity_Id
      WHERE
            ciac.Cu_Invoice_Id = 23712332


EXEC dbo.[Cu_Invoice_Account_Commodity_EC_Calc_Value_Del_By_Cu_Invoice_Id] 
      @Cu_Invoice_Id = 23712332
     
   SELECT
            *
      FROM
            dbo.Cu_Invoice_Account_Commodity ciac
            INNER JOIN dbo.Cu_Invoice_Account_Commodity_EC_Calc_Value ciacecv
                  ON ciac.Cu_Invoice_Account_Commodity_Id = ciacecv.Cu_Invoice_Account_Commodity_Id
      WHERE
            ciac.Cu_Invoice_Id = 23712332
           
Rollback TRAN
	

 AUTHOR INITIALS:             
 Initials		Name
------------------------------------------------------------------------
 RKV			Ravi kumar vegesna

 moDIFICATIONS:                 
 Initials		Date			Modification                
------------------------------------------------------------------------    
 RKV			2016-04-01      Created for AS400 Enhancement.
 
******/
CREATE PROCEDURE [dbo].[Cu_Invoice_Account_Commodity_EC_Calc_Value_Del_By_Cu_Invoice_Id] ( @Cu_Invoice_Id INT )
AS 
BEGIN     
    
      SET NOCOUNT ON;   

      
      
      DELETE
            ciacecv
      FROM
            dbo.Cu_Invoice_Account_Commodity ciac
            INNER JOIN dbo.Cu_Invoice_Account_Commodity_EC_Calc_Value ciacecv
                  ON ciac.Cu_Invoice_Account_Commodity_Id = ciacecv.Cu_Invoice_Account_Commodity_Id
      WHERE
            ciac.Cu_Invoice_Id = @Cu_Invoice_Id

END
;
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Account_Commodity_EC_Calc_Value_Del_By_Cu_Invoice_Id] TO [CBMSApplication]
GO
