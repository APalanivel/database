SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        [Trade].[Trade_Site_Status_Price_Dtls_Sel]   
                        
Description:                        
          
                        
Input Parameters:                        
    Name			DataType        Default     Description                          
--------------------------------------------------------------------------------  
	@Trade_Number INT
                        
Output Parameters:                              
	Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
   
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
 
       Exec [Trade].[Trade_Site_Status_Price_Dtls_Sel] 825,1,25
	   Exec [Trade].[Trade_Site_Status_Price_Dtls_Sel] 16712
	   Exec [Trade].[Trade_Site_Status_Price_Dtls_Sel] 16711
	   Exec [Trade].[Trade_Site_Status_Price_Dtls_Sel] 16710
	   Exec [Trade].[Trade_Site_Status_Price_Dtls_Sel] 16708
                       
 Author Initials:                        
    Initials    Name                        
--------------------------------------------------------------------------------  
    RR          Raghu Reddy          
                         
 Modifications:                        
    Initials Date           Modification                        
--------------------------------------------------------------------------------  
    RR   2019-03-22     Global Risk Management - Created                      
                       
******/
CREATE PROCEDURE [Trade].[Trade_Site_Status_Price_Dtls_Sel]
    (
        @Trade_Number INT
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
    )
AS
    BEGIN
        SET NOCOUNT ON;
        WITH Cte_Trade
        AS (
               SELECT
                    @Trade_Number AS Trade_Number
                    , REPLACE(CONVERT(VARCHAR(20), dtchvd.Deal_Month, 106), ' ', '-') AS Deal_Month
                    , ttp.Trade_Price
                    , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
                    , ch.Client_Hier_Id
                    , SUM(dtchvd.Total_Volume) AS Total_Volume
                    , ws.Workflow_Status_Name
                    , dtchvd.Trade_Executed_Ts
                    , e.ENTITY_NAME AS Hedge_Type
                    , ROW_NUMBER() OVER (ORDER BY
                                             RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
                                             , dtchvd.Deal_Month) AS Row_Num
               FROM
                    Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtchvd
                    LEFT JOIN Trade.Trade_Price ttp
                        ON dtchvd.Trade_Price_Id = ttp.Trade_Price_Id
                    INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                        ON dtch.Client_Hier_Id = dtchvd.Client_Hier_Id
                           AND  dtch.Deal_Ticket_Id = dtchvd.Deal_Ticket_Id
                    INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status dtchws
                        ON dtchws.Deal_Ticket_Client_Hier_Id = dtch.Deal_Ticket_Client_Hier_Id
                           AND  dtchws.Is_Active = 1
                    INNER JOIN Trade.Workflow_Status_Map wsm
                        ON wsm.Workflow_Status_Map_Id = dtchws.Workflow_Status_Map_Id
                    INNER JOIN Trade.Workflow_Status ws
                        ON ws.Workflow_Status_Id = wsm.Workflow_Status_Id
                    INNER JOIN Core.Client_Hier ch
                        ON ch.Client_Hier_Id = dtchvd.Client_Hier_Id
                    INNER JOIN Trade.Deal_Ticket dt2
                        ON dt2.Deal_Ticket_Id = dtchvd.Deal_Ticket_Id
                    INNER JOIN dbo.ENTITY e
                        ON dt2.Hedge_Type_Cd = e.ENTITY_ID
               WHERE
                    dtchvd.Trade_Number = @Trade_Number
                    AND (   dtchws.Trade_Month IS NULL
                            OR  dtchws.Trade_Month = dtchvd.Deal_Month)
                    AND (   dtchws.Contract_Id IS NULL
                            OR  ISNULL(dtchws.Contract_Id, -1) = ISNULL(dtchvd.Contract_Id, -1))
               GROUP BY
                   dtchvd.Deal_Month
                   , ttp.Trade_Price
                   , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
                   , ch.Client_Hier_Id
                   , ws.Workflow_Status_Name
                   , dtchvd.Trade_Executed_Ts
                   , e.ENTITY_NAME
           )
        SELECT
            ct.Trade_Number
            , ct.Deal_Month
            , ct.Site_name
            , ct.Total_Volume AS Total_Volume
            , ct.Workflow_Status_Name AS [Status]
            , ct.Trade_Price
            , MAX(ISNULL(v.VENDOR_NAME, rc.COUNTERPARTY_NAME)) AS Counter_Party
            , MAX(dtchts.Assignee) AS Risk_Manager
            , REPLACE(CONVERT(VARCHAR(20), MAX(dtchts1.Date_Data_Amended), 106), ' ', '-') AS Order_Placed
            , REPLACE(CONVERT(VARCHAR(20), ct.Trade_Executed_Ts, 106), ' ', '-') AS Date_Executed
            , ct.Row_Num
            , tot.Total
            , ct.Hedge_Type
            , MAX(dtchts.Cbms_Counterparty_Id) AS Cbms_Counterparty_Id
        FROM
            Cte_Trade ct
            LEFT JOIN Trade.Deal_Ticket_Client_Hier_TXN_Status dtchts1
                ON ct.Trade_Number = dtchts1.Cbms_Trade_Number
                   AND  dtchts1.Transaction_Status IN ( 'Order placed' )
            LEFT JOIN Trade.Deal_Ticket_Client_Hier_TXN_Status dtchts
                ON ct.Trade_Number = dtchts.Cbms_Trade_Number
            LEFT JOIN dbo.VENDOR v
                ON v.VENDOR_ID = dtchts.Cbms_Counterparty_Id
                   AND  ct.Hedge_Type = 'Physical'
            LEFT JOIN dbo.RM_COUNTERPARTY rc
                ON rc.RM_COUNTERPARTY_ID = dtchts.Cbms_Counterparty_Id
                   AND  ct.Hedge_Type = 'Financial'
            CROSS JOIN
            (   SELECT
                    MAX(Row_Num) AS Total
                FROM
                    Cte_Trade) tot
        WHERE
            ct.Row_Num BETWEEN @Start_Index
                       AND     @End_Index
        GROUP BY
            ct.Trade_Number
            , ct.Deal_Month
            , ct.Site_name
            , ct.Total_Volume
            , ct.Workflow_Status_Name
            , ct.Trade_Price
            , REPLACE(CONVERT(VARCHAR(20), ct.Trade_Executed_Ts, 106), ' ', '-')
            , ct.Row_Num
            , tot.Total
            , ct.Hedge_Type
        ORDER BY
            ct.Row_Num;

    END;

GO
GRANT EXECUTE ON  [Trade].[Trade_Site_Status_Price_Dtls_Sel] TO [CBMSApplication]
GO
