
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
    
NAME:     
   [dbo].[Cu_Invoice_Recalc_Response_Last_Change_Ts_Upd_By_Invoice_Account_Commodity]    
         
DESCRIPTION:     
   When User close the recalc exception then updating the Last_Change_Ts in 
   Cu_invoice_Recalc_Response table for that invoice/Account/commodity  combination.    
          
INPUT PARAMETERS:            
NAME				DATATYPE			DEFAULT    DESCRIPTION    
------------------------------------------------------------------------------    
 @Cu_Invoice_Id		INT
 @Account_Id		INT
 @Commodity_Id		INT           
    
OUTPUT PARAMETERS:    
NAME				DATATYPE			DEFAULT    DESCRIPTION    
------------------------------------------------------------------------------    
USAGE EXAMPLES:    
------------------------------------------------------------------------------    

   BEGIN TRAN 
   
   EXEC [dbo].[Cu_Invoice_Recalc_Response_Last_Change_Ts_Upd_By_Invoice_Account_Commodity]
      @Cu_Invoice_Id = 29942697
     ,@Account_Id = 1013117
     ,@Commodity_Id = 291  
       
   ROLLBACK TRAN
      
AUTHOR INITIALS:              
INITIALS	NAME              
------------------------------------------------------------------------------    
RKV			Ravi Kumar Vegesna	
              
MODIFICATIONS               
INITIALS		DATE			MODIFICATION              
------------------------------------------------------------------------------    
RKV			    2016-07-21		Created For MAINT-4156.    
*/      

CREATE PROCEDURE [dbo].[Cu_Invoice_Recalc_Response_Last_Change_Ts_Upd_By_Invoice_Account_Commodity]
      ( 
       @Cu_Invoice_Id INT
      ,@Account_Id INT
      ,@Commodity_Id INT )
AS 
BEGIN

      SET NOCOUNT ON; 
      
      UPDATE
            cirr
      SET   
            cirr.Last_Change_Ts = cirr.Last_Change_Ts
      FROM
            dbo.Cu_Invoice_Recalc_Response cirr
            INNER JOIN dbo.RECALC_HEADER rh
                  ON cirr.Recalc_Header_Id = rh.RECALC_HEADER_ID
      WHERE
            rh.CU_INVOICE_ID = @Cu_Invoice_Id
            AND rh.ACCOUNT_ID = @Account_Id
            AND rh.Commodity_Id = @Commodity_Id
            
END

;
;
GO

GRANT EXECUTE ON  [dbo].[Cu_Invoice_Recalc_Response_Last_Change_Ts_Upd_By_Invoice_Account_Commodity] TO [CBMSApplication]
GO
