SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
NAME:          
 dbo.Vendor_Name_Search_By_Keyword_VendorType_ClientID        
        
DESCRIPTION:     
   Vendor name Smart search implemented.    
      
INPUT PARAMETERS:      
 Name    DataType   Default   Description          
------------------------------------------------------------------------        
@Keyword  VARCHAR(200)  NULL    
@Vendor_Type_Id  INT     NULL    
@Client_Id       INT  NULL  
      
OUTPUT PARAMETERS:          
 Name    DataType   Default   Description          
------------------------------------------------------------------------        
      
USAGE EXAMPLES:      
------------------------------------------------------------------------        
      
 EXEC dbo.[Vendor_Name_Search_By_Keyword_VendorType_ClientID] 'Blue','Supplier'  ,235  
   
     
       
       
Author Initials:              
 Initials  Name            
------------------------------------------------------------------------        
 SLP  Sri Lakshmi Pallikonda  
             
 Modifications :              
 Initials   Date   Modification              
------------------------------------------------------------------------        
 SLP     2019-09-29 Create to Search Vendors based upon the Vendor Type, ClientId   
      & Keyword  
    
******/
CREATE PROCEDURE [dbo].[Vendor_Name_Search_By_Keyword_VendorType_ClientID]
     (
         @Keyword VARCHAR(200) = NULL
         , @Vendor_Type VARCHAR(200) = NULL
         , @Client_Id INT = NULL
     )
AS
    BEGIN
        SET NOCOUNT ON;

        CREATE TABLE #Vendor_Dtls
             (
                 Vendor_Id INT
                 , Vendor_Name VARCHAR(200)
             );

        INSERT INTO #Vendor_Dtls
             (
                 Vendor_Id
                 , Vendor_Name
             )
        SELECT
            vndr.VENDOR_ID
            , vndr.VENDOR_NAME
        FROM
            dbo.VENDOR vndr
            INNER JOIN dbo.ENTITY ventyp
                ON vndr.VENDOR_TYPE_ID = ventyp.ENTITY_ID
        WHERE
            @Client_Id IS NULL
            AND vndr.IS_HISTORY = 0
            AND (   @Keyword IS NULL
                    OR  vndr.VENDOR_NAME LIKE +'%' + @Keyword + '%')
            AND (   @Vendor_Type IS NULL
                    OR  ventyp.ENTITY_NAME = @Vendor_Type)
        GROUP BY
            vndr.VENDOR_ID
            , vndr.VENDOR_NAME;



        INSERT INTO #Vendor_Dtls
             (
                 Vendor_Id
                 , Vendor_Name
             )
        SELECT
            cha.Account_Vendor_Id
            , cha.Account_Vendor_Name
        FROM
            Core.Client_Hier ch
            JOIN Core.Client_Hier_Account cha
                ON cha.Client_Hier_Id = ch.Client_Hier_Id
            
        --INNER JOIN dbo.VENDOR vndr
        --    ON vcm.VENDOR_ID = vndr.VENDOR_ID
        --INNER JOIN dbo.ENTITY ventyp
        --    ON vndr.VENDOR_TYPE_ID = ventyp.ENTITY_ID
        WHERE
            @Client_Id IS NOT NULL
            AND ch.Client_Id = @Client_Id
            AND (   @Keyword IS NULL
                    OR  cha.Account_Vendor_Name LIKE +'%' + @Keyword + '%')
            AND (   @Vendor_Type IS NULL
                    OR  cha.Account_Type = @Vendor_Type)
        GROUP BY
            cha.Account_Vendor_Id
            , cha.Account_Vendor_Name;
   
   
   SELECT * FROM #Vendor_Dtls vd
   DROP TABLE #Vendor_Dtls
   
    END;
GO
GRANT EXECUTE ON  [dbo].[Vendor_Name_Search_By_Keyword_VendorType_ClientID] TO [CBMSApplication]
GO
