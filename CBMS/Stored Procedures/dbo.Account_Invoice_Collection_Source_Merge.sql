SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******             
                         
 NAME: [dbo].[Account_Invoice_Collection_Source_Merge]                          
                            
 DESCRIPTION:                            
			To insert or update or delete data in Account_Invoice_Collection_Source_Merge
                            
 INPUT PARAMETERS:            
                           
 Name                            DataType           Default       Description            
---------------------------------------------------------------------------------------------------------------          
@User_Report_Id						INT
@tvp_Account_Invoice_Collection_Source							tvp_Account_Invoice_Collection_Source		 READONLY                
                            
 OUTPUT PARAMETERS:                 
                            
 Name                            DataType            Default      Description            
---------------------------------------------------------------------------------------------------------------          
                            
 USAGE EXAMPLES:            
---------------------------------------------------------------------------------------------------------------          
--EXAMPLE 1

BEGIN TRAN
     
SELECT * FROM Account_Invoice_Collection_Source  WHERE Invoice_Collection_Account_Config_Id=186
SELECT * FROM dbo.Account_Invoice_Collection_Online_Source_Dtl where Account_Invoice_Collection_Source_Id in (SELECT Account_Invoice_Collection_Source_Id FROM Account_Invoice_Collection_Source  WHERE Invoice_Collection_Account_Config_Id=186)
SELECT * FROM dbo.Account_Invoice_Collection_UBM_Source_Dtl where Account_Invoice_Collection_Source_Id in (SELECT Account_Invoice_Collection_Source_Id FROM Account_Invoice_Collection_Source  WHERE Invoice_Collection_Account_Config_Id=186)

DECLARE @tvp_Account_Invoice_Collection_Source tvp_Account_Invoice_Collection_Source;    
    
INSERT      INTO @tvp_Account_Invoice_Collection_Source  
(  
[Invoice_Collection_Source_Cd]  
,[Invoice_Source_Type_Cd]  
,[Is_Primary]   
,[Invoice_Source_Method_of_Contact_Cd]   
,[URL]   
,[Login_Name]  
,[Passcode]   
,[Instruction]   
,[Instruction_Document_Image_Id]   
,[UBM_Id]  
,[UBM_Type_Cd]  
 )  
VALUES  
            ( 102288, 102292,0,0,'www.Prokarma.com','Test123456787978','Test123456','Test',89906856,9,102296 )  
            ,( 102289, 102292,0,NULL,'www.energydomain.com','cteplJan17', 'ctepFeb17','Vendor online',89906857,NULL,NULL )  
            ,( 102289, 102293,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL) 
             ,( 102289, 102297,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL )  
            ,( 102287, 102292,0,NULL,'www.eaton.com','Test1234Client','bsclientTest1234','Account_Invoice_Collec',89906858,NULL,NULL)  
             ,( 102287, 102291,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL )  
            ,( 102287, 102290,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)   
            ;    
              
  
EXEC [dbo].[Account_Invoice_Collection_Source_Merge]   
      @Invoice_Collection_Account_Config_Id = 186  
     ,@tvp_Account_Invoice_Collection_Source=@tvp_Account_Invoice_Collection_Source  
     ,@User_Info_Id =49  

SELECT * FROM Account_Invoice_Collection_Source  WHERE Invoice_Collection_Account_Config_Id=186
SELECT * FROM dbo.Account_Invoice_Collection_Online_Source_Dtl where Account_Invoice_Collection_Source_Id in (SELECT Account_Invoice_Collection_Source_Id FROM Account_Invoice_Collection_Source  WHERE Invoice_Collection_Account_Config_Id=186)
SELECT * FROM dbo.Account_Invoice_Collection_UBM_Source_Dtl where Account_Invoice_Collection_Source_Id in (SELECT Account_Invoice_Collection_Source_Id FROM Account_Invoice_Collection_Source  WHERE Invoice_Collection_Account_Config_Id=186)

     
ROLLBACK

--EXAMPLE 2

BEGIN TRAN

DECLARE @tvp_Account_Invoice_Collection_Source tvp_Account_Invoice_Collection_Source;  
  
INSERT      INTO @tvp_Account_Invoice_Collection_Source
(
[Invoice_Collection_Source_Cd]
,[Invoice_Source_Type_Cd]
,[Is_Primary] 
,[Invoice_Source_Method_of_Contact_Cd] 
,[URL] 
,[Login_Name]
,[Passcode] 
,[Instruction] 
,[Instruction_Document_Image_Id] 
,[UBM_Id]
,[UBM_Type_Cd]
 )
VALUES
            ( 102288, 102292,1,NULL,'Url1','Login_Name','Password','Instructions',123,NULL,NULL )            ;  
            
SELECT * FROM Account_Invoice_Collection_Source  WHERE Invoice_Collection_Account_Config_Id=41
SELECT * FROM dbo.Account_Invoice_Collection_Online_Source_Dtl where Account_Invoice_Collection_Source_Id in (SELECT Account_Invoice_Collection_Source_Id FROM Account_Invoice_Collection_Source  WHERE Invoice_Collection_Account_Config_Id=41)
SELECT * FROM dbo.Account_Invoice_Collection_UBM_Source_Dtl where Account_Invoice_Collection_Source_Id in (SELECT Account_Invoice_Collection_Source_Id FROM Account_Invoice_Collection_Source  WHERE Invoice_Collection_Account_Config_Id=41)

EXEC [dbo].[Account_Invoice_Collection_Source_Merge] 
      @Invoice_Collection_Account_Config_Id = 41
    ,@tvp_Account_Invoice_Collection_Source=@tvp_Account_Invoice_Collection_Source
     ,@User_Info_Id =49
     
SELECT * FROM Account_Invoice_Collection_Source  WHERE Invoice_Collection_Account_Config_Id=41
SELECT * FROM dbo.Account_Invoice_Collection_Online_Source_Dtl where Account_Invoice_Collection_Source_Id in (SELECT Account_Invoice_Collection_Source_Id FROM Account_Invoice_Collection_Source  WHERE Invoice_Collection_Account_Config_Id=41)
SELECT * FROM dbo.Account_Invoice_Collection_UBM_Source_Dtl where Account_Invoice_Collection_Source_Id in (SELECT Account_Invoice_Collection_Source_Id FROM Account_Invoice_Collection_Source  WHERE Invoice_Collection_Account_Config_Id=41)



ROLLBACK



                           
 AUTHOR INITIALS:            
           
 Initials               Name            
---------------------------------------------------------------------------------------------------------------          
 SP                     Sandeep Pigilam              
                             
 MODIFICATIONS:          
           
 Initials               Date             Modification          
---------------------------------------------------------------------------------------------------------------          
 SP						2016-11-17       Created for Invoice Tracking Phase I.   
 NR						2017-09-04		 MAINT-5638 - changed the URL data length to NVARCHAR(max) in both TVP and 
										Account_Invoice_Collection_Online_Source_Dtl Table and temp tables defined with in the save object.                      
 RKV                    2019-06-06      Added two merge statements aas part of IC Project.    
 NR						2020-06-09		SE2017-963 - Bot Process - Removed Enroll_In_Bot coulmn .                     
******/
CREATE PROCEDURE [dbo].[Account_Invoice_Collection_Source_Merge]
    (
        @Invoice_Collection_Account_Config_Id INT
        , @tvp_Account_Invoice_Collection_Source tvp_Account_Invoice_Collection_Source READONLY
        , @User_Info_Id INT
        , @Preserve_Existing_Value_When_Null BIT = 0
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Invoice_Source_Type_Online INT
            , @Invoice_Source_Type_Data_Feed_ETL INT
            , @Invoice_Source_Type_Data_Feed_Patner INT
            , @Account_Invoice_Collection_Source_Id INT;

        SELECT
            @Invoice_Source_Type_Online = MAX(CASE WHEN c.Code_Value = 'Online' THEN c.Code_Id
                                              END)
            , @Invoice_Source_Type_Data_Feed_ETL = MAX(CASE WHEN c.Code_Value = 'ETL' THEN c.Code_Id
                                                       END)
            , @Invoice_Source_Type_Data_Feed_Patner = MAX(CASE WHEN c.Code_Value = 'Partner' THEN c.Code_Id
                                                          END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Std_Column_Name = 'Invoice_Source_Type_Cd'
            AND c.Code_Value IN ( 'Online', 'ETL', 'Partner' );




        DECLARE @Temp_Account_Invoice_Collection_Source TABLE
              (
                  Invoice_Collection_Source_Cd INT
                  , Invoice_Source_Type_Cd INT
                  , Is_Primary INT
                  , Invoice_Source_Method_of_Contact_Cd INT
                  , URL NVARCHAR(MAX)
                  , Login_Name NVARCHAR(255)
                  , Passcode NVARCHAR(255)
                  , Instruction NVARCHAR(500)
                  , Instruction_Document_Image_Id INT
                  , UBM_Id INT
                  , UBM_Type_Cd INT
                  , Account_Invoice_Collection_Source_Id INT
                  , Collection_Instruction NVARCHAR(MAX)
                  , File_Source_Cd INT NULL
                  , File_Vendor_Id INT NULL
                  , Generic_Key NVARCHAR(255) NULL
                  , Image_Source_Cd INT NULL
                  , Image_Vendor_Id INT NULL
                  , Image_URL NVARCHAR(MAX) NULL
                  , Image_Login_Name NVARCHAR(255) NULL
                  , Image_Passcode NVARCHAR(255) NULL
                  , Image_Load_Location NVARCHAR(255) NULL
              );

        INSERT INTO @Temp_Account_Invoice_Collection_Source
             (
                 Invoice_Collection_Source_Cd
                 , Invoice_Source_Type_Cd
                 , Is_Primary
                 , Invoice_Source_Method_of_Contact_Cd
                 , URL
                 , Login_Name
                 , Passcode
                 , Instruction
                 , Instruction_Document_Image_Id
                 , UBM_Id
                 , UBM_Type_Cd
                 , Collection_Instruction
                 , File_Source_Cd
                 , File_Vendor_Id
                 , Generic_Key
                 , Image_Source_Cd
                 , Image_Vendor_Id
                 , Image_URL
                 , Image_Login_Name
                 , Image_Passcode
                 , Image_Load_Location
             )
        SELECT
            tvp.Invoice_Collection_Source_Cd
            , tvp.Invoice_Source_Type_Cd
            , CASE WHEN @Preserve_Existing_Value_When_Null = 1 THEN ISNULL(tvp.Is_Primary, aic.Is_Primary)
                  ELSE tvp.Is_Primary
              END AS Is_Primary
            , CASE WHEN @Preserve_Existing_Value_When_Null = 1 THEN
                       ISNULL(tvp.Invoice_Source_Method_of_Contact_Cd, aic.Invoice_Source_Method_of_Contact_Cd)
                  ELSE tvp.Invoice_Source_Method_of_Contact_Cd
              END AS Invoice_Source_Method_of_Contact_Cd
            , CASE WHEN @Preserve_Existing_Value_When_Null = 1 THEN ISNULL(tvp.URL, oaic.URL)
                  ELSE tvp.URL
              END AS URL
            , CASE WHEN @Preserve_Existing_Value_When_Null = 1 THEN ISNULL(tvp.Login_Name, oaic.Login_Name)
                  ELSE tvp.Login_Name
              END AS Login_Name
            , CASE WHEN @Preserve_Existing_Value_When_Null = 1 THEN ISNULL(tvp.Passcode, oaic.Passcode)
                  ELSE tvp.Passcode
              END AS Passcode
            , CASE WHEN @Preserve_Existing_Value_When_Null = 1 THEN ISNULL(tvp.Instruction, oaic.Instruction)
                  ELSE tvp.Instruction
              END AS Instruction
            , tvp.Instruction_Document_Image_Id AS Instruction_Document_Image_Id
            , CASE WHEN @Preserve_Existing_Value_When_Null = 1 THEN ISNULL(tvp.UBM_Id, uaic.UBM_Id)
                  ELSE tvp.UBM_Id
              END AS UBM_Id
            , CASE WHEN @Preserve_Existing_Value_When_Null = 1 THEN ISNULL(tvp.UBM_Type_Cd, uaic.UBM_Type_Cd)
                  ELSE tvp.UBM_Type_Cd
              END AS UBM_Type_Cd
            , CASE WHEN @Preserve_Existing_Value_When_Null = 1 THEN
                       ISNULL(tvp.Collection_Instruction, aic.Collection_Instruction)
                  ELSE tvp.Collection_Instruction
              END AS Collection_Instruction
            , tvp.File_Source_Cd AS File_Source_Cd
            , tvp.File_Vendor_Id AS File_Vendor_Id
            , tvp.Generic_Key AS Generic_Key
            , tvp.Image_Source_Cd AS Image_Source_Cd
            , tvp.Image_Vendor_Id AS Image_Vendor_Id
            , tvp.Image_URL AS Image_URL
            , tvp.Image_Login_Name AS Image_Login_Name
            , tvp.Image_Passcode AS Image_Passcode
            , tvp.Image_Load_Location AS Image_Load_Location
        FROM
            @tvp_Account_Invoice_Collection_Source tvp
            LEFT JOIN dbo.Account_Invoice_Collection_Source aic
                ON aic.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
                   AND  aic.Invoice_Collection_Source_Cd = tvp.Invoice_Collection_Source_Cd
                   AND  aic.Invoice_Source_Type_Cd = tvp.Invoice_Source_Type_Cd
                   AND  @Preserve_Existing_Value_When_Null = 1
            LEFT JOIN dbo.Account_Invoice_Collection_Online_Source_Dtl oaic
                ON oaic.Account_Invoice_Collection_Source_Id = aic.Account_Invoice_Collection_Source_Id
            LEFT JOIN dbo.Account_Invoice_Collection_UBM_Source_Dtl uaic
                ON uaic.Account_Invoice_Collection_Source_Id = aic.Account_Invoice_Collection_Source_Id
            LEFT OUTER JOIN dbo.Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl AS efsd
                ON efsd.Account_Invoice_Collection_Source_Id = aic.Account_Invoice_Collection_Source_Id
            LEFT OUTER JOIN dbo.Account_Invoice_Collection_Data_Feed_Etl_Image_Source_Dtl AS eisd
                ON eisd.Account_Invoice_Collection_Source_Id = aic.Account_Invoice_Collection_Source_Id;





        MERGE dbo.Account_Invoice_Collection_Source AS tgt
        USING (   SELECT
                        @Invoice_Collection_Account_Config_Id AS Invoice_Collection_Account_Config_Id
                        , tvp.Invoice_Collection_Source_Cd
                        , tvp.Invoice_Source_Type_Cd
                        , tvp.Is_Primary
                        , tvp.Invoice_Source_Method_of_Contact_Cd
                        , tvp.Collection_Instruction
                  FROM
                        @Temp_Account_Invoice_Collection_Source tvp) AS src
        ON src.Invoice_Collection_Account_Config_Id = tgt.Invoice_Collection_Account_Config_Id
           AND  src.Invoice_Collection_Source_Cd = tgt.Invoice_Collection_Source_Cd
           AND  src.Invoice_Source_Type_Cd = tgt.Invoice_Source_Type_Cd
        WHEN NOT MATCHED BY TARGET THEN INSERT (Invoice_Collection_Account_Config_Id
                                                , Invoice_Collection_Source_Cd
                                                , Invoice_Source_Type_Cd
                                                , Is_Primary
                                                , Invoice_Source_Method_of_Contact_Cd
                                                , Created_User_Id
                                                , Created_Ts
                                                , Updated_User_Id
                                                , Last_Change_Ts
                                                , Collection_Instruction)
                                        VALUES
                                            (src.Invoice_Collection_Account_Config_Id
                                             , src.Invoice_Collection_Source_Cd
                                             , src.Invoice_Source_Type_Cd
                                             , src.Is_Primary
                                             , src.Invoice_Source_Method_of_Contact_Cd
                                             , @User_Info_Id
                                             , GETDATE()
                                             , @User_Info_Id
                                             , GETDATE()
                                             , src.Collection_Instruction)
        WHEN MATCHED THEN UPDATE SET
                              Is_Primary = src.Is_Primary
                              , Invoice_Source_Method_of_Contact_Cd = src.Invoice_Source_Method_of_Contact_Cd
                              , Collection_Instruction = src.Collection_Instruction
                              , Updated_User_Id = @User_Info_Id
                              , Last_Change_Ts = GETDATE();


        UPDATE
            temp
        SET
            temp.Account_Invoice_Collection_Source_Id = aic.Account_Invoice_Collection_Source_Id
        FROM
            @Temp_Account_Invoice_Collection_Source temp
            INNER JOIN Account_Invoice_Collection_Source aic
                ON temp.Invoice_Collection_Source_Cd = aic.Invoice_Collection_Source_Cd
                   AND  temp.Invoice_Source_Type_Cd = aic.Invoice_Source_Type_Cd
        WHERE
            aic.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id;




        SELECT
            @Account_Invoice_Collection_Source_Id = Account_Invoice_Collection_Source_Id
        FROM
            @Temp_Account_Invoice_Collection_Source temp
        WHERE
            temp.Invoice_Source_Type_Cd = @Invoice_Source_Type_Data_Feed_ETL;


        --Online merge,Patner

        MERGE dbo.Account_Invoice_Collection_Online_Source_Dtl AS tgt
        USING (   SELECT
                        tvp.Account_Invoice_Collection_Source_Id
                        , tvp.URL
                        , tvp.Login_Name
                        , tvp.Passcode
                        , tvp.Instruction
                        , tvp.Instruction_Document_Image_Id
                        , tvp.Invoice_Source_Type_Cd
                  FROM
                        @Temp_Account_Invoice_Collection_Source tvp
                  WHERE
                        tvp.Invoice_Source_Type_Cd IN ( @Invoice_Source_Type_Online
                                                        , @Invoice_Source_Type_Data_Feed_Patner )) AS src
        ON src.Account_Invoice_Collection_Source_Id = tgt.Account_Invoice_Collection_Source_Id
        WHEN NOT MATCHED BY TARGET AND src.Invoice_Source_Type_Cd IN ( @Invoice_Source_Type_Online
                                                                       , @Invoice_Source_Type_Data_Feed_Patner )
                                       AND  (   src.URL IS NOT NULL
                                                OR  src.Login_Name IS NOT NULL
                                                OR  src.Passcode IS NOT NULL
                                                OR  src.Instruction IS NOT NULL
                                                OR  src.Instruction_Document_Image_Id IS NOT NULL) THEN
            INSERT (Account_Invoice_Collection_Source_Id
                    , URL
                    , Login_Name
                    , Passcode
                    , Instruction
                    , Instruction_Document_Image_Id
                    , Created_User_Id
                    , Created_Ts
                    , Updated_User_Id
                    , Last_Change_Ts)
            VALUES
                (src.Account_Invoice_Collection_Source_Id
                 , src.URL
                 , src.Login_Name
                 , src.Passcode
                 , src.Instruction
                 , src.Instruction_Document_Image_Id
                 , @User_Info_Id
                 , GETDATE()
                 , @User_Info_Id
                 , GETDATE())
        WHEN MATCHED AND src.Invoice_Source_Type_Cd IN ( @Invoice_Source_Type_Online
                                                         , @Invoice_Source_Type_Data_Feed_Patner )
                         AND (   ISNULL(tgt.URL, '') <> ISNULL(src.URL, '')
                                 OR ISNULL(tgt.Login_Name, '') <> ISNULL(src.Login_Name, '')
                                 OR ISNULL(tgt.Passcode, '') <> ISNULL(src.Passcode, '')
                                 OR ISNULL(tgt.Instruction, '') <> ISNULL(src.Instruction, '')
                                 OR ISNULL(tgt.Instruction_Document_Image_Id, -1) <> ISNULL(
                                                                                         src.Instruction_Document_Image_Id
                                                                                         , -1)) THEN
            UPDATE SET
                URL = src.URL
                , Login_Name = src.Login_Name
                , Passcode = src.Passcode
                , Instruction = src.Instruction
                , Instruction_Document_Image_Id = src.Instruction_Document_Image_Id
                , Updated_User_Id = CASE WHEN @Preserve_Existing_Value_When_Null = 1 THEN Updated_User_Id
                                        ELSE @User_Info_Id
                                    END
                , Last_Change_Ts = CASE WHEN @Preserve_Existing_Value_When_Null = 1 THEN Last_Change_Ts
                                       ELSE GETDATE()
                                   END;



        --UBM Source Merge

        MERGE dbo.Account_Invoice_Collection_UBM_Source_Dtl AS tgt
        USING (   SELECT
                        tvp.Account_Invoice_Collection_Source_Id
                        , tvp.UBM_Id
                        , tvp.UBM_Type_Cd
                  FROM
                        @Temp_Account_Invoice_Collection_Source tvp
                  GROUP BY
                      tvp.Account_Invoice_Collection_Source_Id
                      , tvp.UBM_Id
                      , tvp.UBM_Type_Cd) AS src
        ON src.Account_Invoice_Collection_Source_Id = tgt.Account_Invoice_Collection_Source_Id
        WHEN NOT MATCHED BY TARGET AND (   src.UBM_Id IS NOT NULL
                                           OR   src.UBM_Type_Cd IS NOT NULL) THEN
            INSERT (Account_Invoice_Collection_Source_Id
                    , UBM_Id
                    , UBM_Type_Cd
                    , Created_User_Id
                    , Created_Ts
                    , Updated_User_Id
                    , Last_Change_Ts)
            VALUES
                (src.Account_Invoice_Collection_Source_Id
                 , src.UBM_Id
                 , src.UBM_Type_Cd
                 , @User_Info_Id
                 , GETDATE()
                 , @User_Info_Id
                 , GETDATE())
        WHEN MATCHED AND (   ISNULL(tgt.UBM_Id, '') <> ISNULL(src.UBM_Id, '')
                             OR ISNULL(tgt.UBM_Type_Cd, '') <> ISNULL(src.UBM_Type_Cd, '')) THEN
            UPDATE SET
                UBM_Id = src.UBM_Id
                , UBM_Type_Cd = src.UBM_Type_Cd
                , Updated_User_Id = CASE WHEN @Preserve_Existing_Value_When_Null = 1 THEN Updated_User_Id
                                        ELSE @User_Info_Id
                                    END
                , Last_Change_Ts = CASE WHEN @Preserve_Existing_Value_When_Null = 1 THEN Last_Change_Ts
                                       ELSE GETDATE()
                                   END;








        --Data Feed ETL file Merge
        MERGE dbo.Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl AS tgt
        USING (   SELECT
                        tvp.Account_Invoice_Collection_Source_Id
                        , tvp.File_Source_Cd
                        , tvp.UBM_Type_Cd
                        , tvp.File_Vendor_Id
                        , tvp.URL
                        , tvp.Login_Name
                        , tvp.Passcode
                        , tvp.Generic_Key
                        , tvp.Instruction
                        , tvp.Instruction_Document_Image_Id
                        , tvp.Invoice_Source_Type_Cd
                  FROM
                        @Temp_Account_Invoice_Collection_Source tvp) AS src
        ON src.Account_Invoice_Collection_Source_Id = tgt.Account_Invoice_Collection_Source_Id
        WHEN NOT MATCHED BY TARGET AND (src.Invoice_Source_Type_Cd = @Invoice_Source_Type_Data_Feed_ETL) THEN
            INSERT (Account_Invoice_Collection_Source_Id
                    , File_Source_Cd
                    , Data_Feed_Type_Cd
                    , Vendor_Id
                    , URL
                    , Login_Name
                    , Passcode
                    , Generic_Key
                    , Website_Instructions
                    , Instruction_Document_Image_Id
                    , Created_User_Id
                    , Created_Ts
                    , Updated_User_Id
                    , Last_Change_Ts)
            VALUES
                (src.Account_Invoice_Collection_Source_Id
                 , src.File_Source_Cd
                 , src.UBM_Type_Cd
                 , src.File_Vendor_Id
                 , src.URL
                 , src.Login_Name
                 , src.Passcode
                 , src.Generic_Key
                 , src.Instruction
                 , src.Instruction_Document_Image_Id
                 , @User_Info_Id
                 , GETDATE()
                 , @User_Info_Id
                 , GETDATE())
        WHEN MATCHED AND (src.Invoice_Source_Type_Cd = @Invoice_Source_Type_Data_Feed_ETL)
                         AND (   ISNULL(tgt.URL, '') <> ISNULL(src.URL, '')
                                 OR ISNULL(tgt.Login_Name, '') <> ISNULL(src.Login_Name, '')
                                 OR ISNULL(tgt.Vendor_Id, '') <> ISNULL(src.File_Vendor_Id, '')
                                 OR ISNULL(tgt.Passcode, '') <> ISNULL(src.Passcode, '')
                                 OR ISNULL(tgt.Website_Instructions, '') <> ISNULL(src.Instruction, '')
                                 OR ISNULL(tgt.File_Source_Cd, '') <> ISNULL(src.File_Source_Cd, '')
                                 OR ISNULL(tgt.Data_Feed_Type_Cd, '') <> ISNULL(src.UBM_Type_Cd, '')
                                 OR ISNULL(tgt.Generic_Key, '') <> ISNULL(src.Generic_Key, '')
                                 OR ISNULL(tgt.Instruction_Document_Image_Id, -1) <> ISNULL(
                                                                                         src.Instruction_Document_Image_Id
                                                                                         , -1)) THEN
            UPDATE SET
                URL = src.URL
                , File_Source_Cd = src.File_Source_Cd
                , Data_Feed_Type_Cd = src.UBM_Type_Cd
                , Vendor_Id = src.File_Vendor_Id
                , Login_Name = src.Login_Name
                , Passcode = src.Passcode
                , tgt.Generic_Key = src.Generic_Key
                , tgt.Website_Instructions = src.Instruction
                , Instruction_Document_Image_Id = src.Instruction_Document_Image_Id
                , Updated_User_Id = CASE WHEN @Preserve_Existing_Value_When_Null = 1 THEN Updated_User_Id
                                        ELSE @User_Info_Id
                                    END
                , Last_Change_Ts = CASE WHEN @Preserve_Existing_Value_When_Null = 1 THEN Last_Change_Ts
                                       ELSE GETDATE()
                                   END;




        --Data Feed ETL Image Merge
        MERGE dbo.Account_Invoice_Collection_Data_Feed_Etl_Image_Source_Dtl AS tgt
        USING (   SELECT
                        tvp.Account_Invoice_Collection_Source_Id
                        , tvp.Image_Source_Cd
                        , tvp.Image_Vendor_Id
                        , tvp.Image_URL
                        , tvp.Image_Login_Name
                        , tvp.Image_Passcode
                        , tvp.Image_Load_Location
                        , tvp.Invoice_Source_Type_Cd
                  FROM
                        @Temp_Account_Invoice_Collection_Source tvp) AS src
        ON src.Account_Invoice_Collection_Source_Id = tgt.Account_Invoice_Collection_Source_Id
        WHEN NOT MATCHED BY TARGET AND (src.Invoice_Source_Type_Cd = @Invoice_Source_Type_Data_Feed_ETL)
                                       AND  (   src.Image_URL IS NOT NULL
                                                OR  src.Image_Source_Cd IS NOT NULL
                                                OR  src.Image_Vendor_Id IS NOT NULL
                                                OR  src.Image_Load_Location IS NOT NULL
                                                OR  src.Image_Vendor_Id IS NOT NULL
                                                OR  src.Image_Passcode IS NOT NULL) THEN
            INSERT (Account_Invoice_Collection_Source_Id
                    , Image_Source_Cd
                    , Vendor_Id
                    , URL
                    , Login_Name
                    , Passcode
                    , Image_Load_Location
                    , Created_User_Id
                    , Created_Ts
                    , Updated_User_Id
                    , Last_Change_Ts)
            VALUES
                (src.Account_Invoice_Collection_Source_Id
                 , src.Image_Source_Cd
                 , src.Image_Vendor_Id
                 , src.Image_URL
                 , src.Image_Login_Name
                 , src.Image_Passcode
                 , src.Image_Load_Location
                 , @User_Info_Id
                 , GETDATE()
                 , @User_Info_Id
                 , GETDATE())
        WHEN MATCHED AND (src.Invoice_Source_Type_Cd = @Invoice_Source_Type_Data_Feed_ETL)
                         AND (   ISNULL(tgt.URL, '') <> ISNULL(src.Image_URL, '')
                                 OR ISNULL(tgt.Login_Name, '') <> ISNULL(src.Image_Login_Name, '')
                                 OR ISNULL(tgt.Image_Source_Cd, '') <> ISNULL(src.Image_Source_Cd, '')
                                 OR ISNULL(tgt.Vendor_Id, '') <> ISNULL(src.Image_Vendor_Id, '')
                                 OR ISNULL(tgt.Passcode, '') <> ISNULL(src.Image_Passcode, '')
                                 OR ISNULL(tgt.Image_Load_Location, '') <> ISNULL(src.Image_Load_Location, '')) THEN
            UPDATE SET
                URL = src.Image_URL
                , Login_Name = src.Image_Login_Name
                , Image_Source_Cd = src.Image_Source_Cd
                , tgt.Vendor_Id = src.Image_Vendor_Id
                , Passcode = src.Image_Passcode
                , tgt.Image_Load_Location = src.Image_Load_Location
                , Updated_User_Id = CASE WHEN @Preserve_Existing_Value_When_Null = 1 THEN Updated_User_Id
                                        ELSE @User_Info_Id
                                    END
                , Last_Change_Ts = CASE WHEN @Preserve_Existing_Value_When_Null = 1 THEN Last_Change_Ts
                                       ELSE GETDATE()
                                   END;




    END;
GO

GRANT EXECUTE ON  [dbo].[Account_Invoice_Collection_Source_Merge] TO [CBMSApplication]
GO
