SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********   
NAME:  dbo.Account_Variance_Consumption_Level_Rules_INS  
 
DESCRIPTION:  Used to insert account variance consumption level into variance_rule_dtl_account_override table for newly created utility/supplier accounts

INPUT PARAMETERS:    
Name							DataType          Default     Description
------------------------------------------------------------
@Variance_Consumption_Level_Id  int
@Account_id						int
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:  
	
	Account_Variance_Consumption_Level_Rules_INS 7
	EXEC dbo.Account_Variance_Consumption_Level_Rules_INS 1095348

------------------------------------------------------------  
AUTHOR INITIALS:  
	Initials	Name  
------------------------------------------------------------  
	NK			Nageswara Rao Kosuri  
	RR			Raghu Reddy
	AP			Arunkumar Palanivel       


	Initials	Date		Modification  
------------------------------------------------------------  
	NK			11/11/2009  Created
	NK			1/22/2010	Script added to delete the existing account dtls from Variance_Rule_Dtl_Account_Override table
							and insert the data based on the Is_Data_entry_Only column in account table.
	HG			04/14/2010	@Variance_Consumption_Level_Id input parameter removed
							As we have all Variance_Consumption_Level entries belongs to the account in Account_Variance_Consumption_Level
							so application doesn't require to pass all the variance_consumption_level_id associated with the account.
	RR			11-28-2016	VTE-58/50 Modified to insert all tests if Is_Data_Entry_Only is false otherwise loads only DEO tests 
	Ap			feb 24,2020	Modified procedure to exclude variance test at account lebel for AVT
******/

CREATE PROCEDURE [dbo].[Account_Variance_Consumption_Level_Rules_INS] @Account_id INT
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Account_Deo BIT

      DELETE
            dbo.Variance_Rule_Dtl_Account_Override
      WHERE
            ACCOUNT_ID = @Account_id
            
      SELECT
            @Account_Deo = isnull(Is_Data_Entry_only, 0)
      FROM
            dbo.Account
      WHERE
            Account_Id = @Account_id

      INSERT      INTO dbo.Variance_Rule_Dtl_Account_Override
                  ( 
                   Variance_Rule_Dtl_Id
                  ,ACCOUNT_ID )
                  SELECT
                        vrdtl.variance_rule_dtl_id
                       ,@Account_id
                  FROM
                        dbo.variance_rule_dtl vrdtl
                        INNER JOIN dbo.Account_Variance_Consumption_Level avcl
                              ON avcl.Variance_Consumption_Level_Id = vrdtl.Variance_Consumption_Level_Id
                        INNER JOIN dbo.Account acc
                              ON acc.ACCOUNT_ID = avcl.ACCOUNT_ID
                  WHERE
                        avcl.account_id = @Account_id
                        AND vrdtl.IS_Active = 1
                        AND ( @Account_Deo = 0
                              OR ( @Account_Deo = 1
                                   AND vrdtl.Is_Data_Entry_Only = 1 ) )
								    AND   NOT EXISTS
                                                            (     SELECT
                                                                        1
                                                                  FROM  Core.Client_Hier_Account cha
                                                                        JOIN
                                                                        dbo.Account_Variance_Consumption_Level avcl1
                                                                              ON avcl1.ACCOUNT_ID = cha.Account_Id
                                                                        JOIN
                                                                        dbo.Variance_Consumption_Level vcl
                                                                              ON vcl.Variance_Consumption_Level_Id = avcl1.Variance_Consumption_Level_Id
                                                                                 AND vcl.Commodity_Id = cha.Commodity_Id
                                                                        JOIN
                                                                        dbo.Bucket_Master bm
                                                                              ON bm.Commodity_Id = vcl.Commodity_Id
                                                                        JOIN
                                                                        dbo.Variance_category_Bucket_Map vcbm
                                                                              ON vcbm.Bucket_master_Id = bm.Bucket_Master_Id
                                                                        JOIN
                                                                        dbo.Variance_Parameter vp
                                                                              ON vp.Variance_Category_Cd = vcbm.Variance_Category_Cd
                                                                        JOIN
                                                                        dbo.Variance_Parameter_Baseline_Map vpbm
                                                                              ON vpbm.Variance_Parameter_Id = vp.Variance_Parameter_Id
                                                                        JOIN
                                                                        dbo.Variance_Rule vr
                                                                              ON vr.Variance_Baseline_Id = vpbm.Variance_Baseline_Id
                                                                        JOIN
                                                                        dbo.Variance_Rule_Dtl vrd1
                                                                              ON vrd1.Variance_Rule_Id = vr.Variance_Rule_Id
                                                                        JOIN
                                                                        dbo.Variance_Parameter_Commodity_Map vpcm
                                                                              ON vpcm.Variance_Parameter_Id = vp.Variance_Parameter_Id
                                                                        JOIN
                                                                        dbo.Variance_Processing_Engine vpe
                                                                              ON vpe.Variance_Processing_Engine_Id = vpcm.variance_process_Engine_id
                                                                        JOIN
                                                                        dbo.Code vpcat
                                                                              ON vpcat.Code_Id = vpe.Variance_Engine_Cd
                                                                        JOIN
                                                                        dbo.Variance_Consumption_Extraction_Service_Param_Value vesp
                                                                              ON vesp.Variance_Consumption_Level_Id = vcl.Variance_Consumption_Level_Id
                                                                  WHERE cha.Account_Id = @Account_id
                                                                        AND   cha.Account_Type = 'Utility'
                                                                        AND   vpcat.Code_Value = 'Advanced Variance Test'
                                                                        AND   vrdtl.Variance_Rule_Dtl_Id = vrd1.Variance_Rule_Dtl_Id
																		AND vcl.Variance_Consumption_Level_Id = avcl.Variance_Consumption_Level_Id  );

END


;
GO

GRANT EXECUTE ON  [dbo].[Account_Variance_Consumption_Level_Rules_INS] TO [CBMSApplication]
GO
