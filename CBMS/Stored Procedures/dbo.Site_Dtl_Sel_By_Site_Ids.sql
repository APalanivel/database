
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
Name:        
    [dbo].[Site_Dtl_Sel_By_Site_Ids]      
       
Description:        
    Used for returning the list of sites for the given filter criteria.      
       
Input Parameters:        
    Name			 DataType		Default		 Description        
------------------------------------------------------------------------        
   @Site_Ids		 VARCHAR(MAX)      
       
Output Parameters:        
    Name			 DataType		Default		 Description        
------------------------------------------------------------------------        
       
Usage Examples:      
------------------------------------------------------------------------        
    SELECT
      *
    FROM
      Core.Client_Hier
    WHERE
      Client_Id = 235
      AND Site_Id > 0
    
    EXECUTE dbo.Site_Dtl_Sel_By_Site_Ids 
      '1899,10979,2379'
       
Author Initials:        
	Initials			Name      
------------------------------------------------------------------------  
	RR					Raghu Reddy
	RKV                 Ravi Kumar Vegesna
       
 Modifications :        
	Initials		Date			 Modification        
------------------------------------------------------------------------  
	RR				2014-11-19		 Created for Site Reference number Enhancement.
	RKV             2017-03-24       Added Client_Hier_Id as part of Invoice Collection Project.
     

******/      
CREATE PROCEDURE [dbo].[Site_Dtl_Sel_By_Site_Ids] ( @Site_Ids VARCHAR(MAX) )
AS 
BEGIN        
        
      SET NOCOUNT ON        
              
      SELECT
            ch.Site_Id
           ,RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_Name + ')' AS Site_name
           ,ch.Site_Reference_Number
           ,ch.Client_Hier_Id
      FROM
            ufn_split(@Site_Ids, ',') sites
            INNER JOIN Core.Client_Hier ch
                  ON ch.Site_Id = sites.Segments
END   
;
GO

GRANT EXECUTE ON  [dbo].[Site_Dtl_Sel_By_Site_Ids] TO [CBMSApplication]
GO
