
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	
	dbo.Client_Commodity_Sel_By_Client_Commodity_Service_Level

DESCRIPTION:

Used to select the commodity,hier level code,bucket aggresstion and allow for de

INPUT PARAMETERS:
	Name						DataType		Default		Description
------------------------------------------------------------------------------------
	@Client_id 					int
	@Commodity_Service_Value	VARCHAR(25)					Invoice/GHG
	@Hier_Level_Cd_Value		VARCHAR(25)					Account/Site

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC Client_Commodity_Sel_By_Client_Commodity_Service_Level 10861,'Invoice'
	EXEC Client_Commodity_Sel_By_Client_Commodity_Service_Level 11231,'Invoice'
	EXEC Client_Commodity_Sel_By_Client_Commodity_Service_Level 235,'Invoice'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan G
	BCH			Balaraju
	RR			Raghu Reddy
	SP			Sandeep Pigilam

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG        	1/28/2010	Created
	HG			02/11/2010	Bucket Aggregation cd added in the select clause
	BCH			10/19/2011  Added Allow_For_DE column in the select list
	HG			2012-06-06	Sort order modified to return EP/NG always at first followed by other services in alphabet order
	RR			2013-06-18  Added Variance_Effective_Dt in select
	SP			2016-11-09	Variance Enhancement Phase II VTE-15,removed columns @Variance_Effective_Dt
	
******/

CREATE PROCEDURE [dbo].[Client_Commodity_Sel_By_Client_Commodity_Service_Level]
      @Client_id INT
     ,@Commodity_Service_Value VARCHAR(25)
     ,@Hier_Level_Cd_Value VARCHAR(25) = NULL
AS 
BEGIN

      SET NOCOUNT ON;

      SELECT
            cc.Client_Commodity_Id
           ,cc.Commodity_Id
           ,com.Commodity_Name
           ,cc.Hier_Level_Cd
           ,heir_level.Code_Value Hier_level_Cd_Value
           ,com.Default_UOM_Entity_Type_Id
           ,bkt_agg_rule.Code_Value Bucket_Aggregation_Rule
           ,com.UOM_Entity_Type
           ,cc.Allow_For_DE
           --,cc.Variance_Effective_Dt
      FROM
            Core.Client_Commodity cc
            JOIN dbo.Commodity com
                  ON com.Commodity_id = cc.Commodity_id
            JOIN dbo.CODE heir_level
                  ON cc.hier_level_cd = heir_level.code_id
            JOIN dbo.CODE service_type
                  ON cc.commodity_service_cd = service_type.code_id
            JOIN dbo.Code lvl
                  ON lvl.Code_Id = cc.Hier_Level_Cd
            JOIN dbo.Code bkt_agg_rule
                  ON bkt_agg_rule.Code_Id = com.Bucket_Aggregation_Cd
      WHERE
            cc.Client_Id = @Client_id
            AND service_type.Code_Value = @Commodity_Service_Value
            AND ( @Hier_Level_Cd_Value IS NULL
                  OR lvl.Code_Value = @Hier_Level_Cd_Value )
      ORDER BY
            ( CASE WHEN com.Commodity_Name = 'Electric Power' THEN 1
                   WHEN com.Commodity_Name = 'Natural Gas' THEN 2
                   ELSE 3
              END ) ASC
           ,com.Commodity_Name ASC

END;
;

;
GO




GRANT EXECUTE ON  [dbo].[Client_Commodity_Sel_By_Client_Commodity_Service_Level] TO [CBMSApplication]
GO
