SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Supplier_Account_Dtl_Exists_Sel_By_Meters_Account_Id

DESCRIPTION:

INPUT PARAMETERS:
Name								DataType		Default			Description
-------------------------------------------------------------------------------
 @Supplier_Account_Config_Id		INT					
 
 
OUTPUT PARAMETERS:
Name								DataType		Default			Description
-------------------------------------------------------------------------------
	
USAGE EXAMPLES:
-------------------------------------------------------------------------------
USE CBMS

EXEC Supplier_Account_Dtl_Exists_Sel_By_Meters_Account_Id
  @Meter_Id = 1227203

EXEC Supplier_Account_Dtl_Exists_Sel_By_Meters_Account_Id 88858 

AUTHOR INITIALS:
	Initials	Name
-------------------------------------------------------------------------------
	NR			Narayana Reddy	
	
MODIFICATIONS
	Initials	Date			Modification
-------------------------------------------------------------------------------
    NR				2019-06-26		Created For Add contract.   
	NR				2020-01-20		 MAINT-9656 - To get the all details associated to the meter.
	NR				2020-02-07		 Add Contract- Overlap dates - Added Order by Supplier_Account_begin_Dt.

******/

CREATE PROCEDURE [dbo].[Supplier_Account_Dtl_Exists_Sel_By_Meters_Account_Id]
    (
        @Meter_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            cha.Account_Type
            , cha.Account_Id
            , cha.Account_Number
            , cha.Meter_Id
            , cha.Supplier_Account_begin_Dt
            , cha.Supplier_Account_End_Dt
            , cha.Supplier_Meter_Association_Date
            , cha.Supplier_Meter_Disassociation_Date
            , cha.Supplier_Contract_ID
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Account_Type = 'Supplier'
            AND cha.Meter_Id = @Meter_Id
        GROUP BY
            cha.Account_Type
            , cha.Account_Id
            , cha.Account_Number
            , cha.Meter_Id
            , cha.Supplier_Account_begin_Dt
            , cha.Supplier_Account_End_Dt
            , cha.Supplier_Meter_Association_Date
            , cha.Supplier_Meter_Disassociation_Date
            , cha.Supplier_Contract_ID
        ORDER BY
            cha.Supplier_Account_begin_Dt;



    END;



GO
GRANT EXECUTE ON  [dbo].[Supplier_Account_Dtl_Exists_Sel_By_Meters_Account_Id] TO [CBMSApplication]
GO
