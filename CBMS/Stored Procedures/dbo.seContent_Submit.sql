SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   Procedure dbo.seContent_Submit
	(
		@ContentId int
	, @AccountId int
	)
As
BEGIN

	set nocount on

		declare @StatusId int
		set @StatusId = dbo.dmlLookup_GetId ('Content Status', 'Submitted')

	exec seContent_ChangeStatus @ContentId, @StatusId, @AccountId

END
GO
GRANT EXECUTE ON  [dbo].[seContent_Submit] TO [CBMSApplication]
GO
