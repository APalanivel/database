SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                          
 NAME: dbo.Invoice_Collection_Account_Config_Sel              
                          
 DESCRIPTION:                          
   To get the details of invoice collection account level config                    
                          
 INPUT PARAMETERS:            
                       
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
@Account_Id        INT  
@Is_Chase_Activated        INT  
@Invoice_Collection_Account_Config_Id INT  NULL   
                                
 OUTPUT PARAMETERS:            
                             
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
                          
 USAGE EXAMPLES:                              
---------------------------------------------------------------------------------------------------------------                              
      
  
  EXEC dbo.Invoice_Collection_Account_Config_Sel   
      @Account_Id = 998681  
     ,@Is_Chase_Activated = 1  
        
  EXEC dbo.Invoice_Collection_Account_Config_Sel   
      @Invoice_Collection_Account_Config_Id=1  
        
        
  EXEC dbo.Invoice_Collection_Account_Config_Sel   
      @Account_Id = 1342210  
      ,@Invoice_Collection_Service_Start_Dt DATE = '2016-01-01'  
      ,@Invoice_Collection_Service_End_Dt DATE = '2016-01-01'    
        
                        
 AUTHOR INITIALS:          
         
 Initials              Name          
---------------------------------------------------------------------------------------------------------------                        
 SP                    Sandeep Pigilam            
 RKV				   Ravi kumar vegesna
 SLP				   Sri Lakshmi Pallikonda
                           
 MODIFICATIONS:        
            
 Initials              Date             Modification        
---------------------------------------------------------------------------------------------------------------        
 SP                    2016-12-08       Created   for Invoice tracking               
 RKV               2017-11-27           Added New Column Invoice_Collection_Owner_User_Id  to the result set            
 RKV               2019-05-24			Added new column Service_Type_Cd 
 SLP			   2019-10-16			Added Group_name as new column in the select
                        
******/

CREATE PROCEDURE [dbo].[Invoice_Collection_Account_Config_Sel]
     (
         @Account_Id INT = NULL
         , @Is_Chase_Activated INT = NULL
         , @Invoice_Collection_Account_Config_Id INT = NULL
         , @Invoice_Collection_Service_Start_Dt DATE = NULL
         , @Invoice_Collection_Service_End_Dt DATE = NULL
         , @Is_Manual BIT = NULL
     )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            icac.Invoice_Collection_Account_Config_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt
            , icac.Is_Chase_Activated
            , icac.Chase_Status_Updated_User_Id
            , icac.Chase_Status_Lat_Updated_Ts
            , icac.Invoice_Collection_Officer_User_Id
            , icac.Chase_Priority_Cd
            , icac.Invoice_Pattern_Cd
            , icac.Additional_Invoices_Per_Period
            , icac.Invoice_Collection_Alternative_Account_Number
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS UserName
            , uic.FIRST_NAME + ' ' + uic.LAST_NAME AS Chase_Status_Updated_User
            , cpc.Code_Value Chase_Priority
            , ipc.Code_Value Invoice_Pattern
            , icsc.Code_Value Primary_Invoice_Source
            , icac.Invoice_Collection_Owner_User_Id
            , icac.Service_Type_Cd
            , istc.Code_Value Primary_Invoice_Source_Type
            , icacg.Invoice_Collection_Account_Config_Group_name AS IC_Account_Config_Group_name
        FROM
            dbo.Invoice_Collection_Account_Config icac
            LEFT JOIN dbo.USER_INFO ui
                ON icac.Invoice_Collection_Officer_User_Id = ui.USER_INFO_ID
            LEFT JOIN dbo.USER_INFO uic
                ON icac.Chase_Status_Updated_User_Id = uic.USER_INFO_ID
            LEFT OUTER JOIN dbo.Code cpc
                ON cpc.Code_Id = icac.Chase_Priority_Cd
            LEFT OUTER JOIN dbo.Code ipc
                ON cpc.Code_Id = icac.Invoice_Pattern_Cd
            LEFT OUTER JOIN dbo.Account_Invoice_Collection_Source aics
                ON icac.Invoice_Collection_Account_Config_Id = aics.Invoice_Collection_Account_Config_Id
                   AND  Is_Primary = 1
            LEFT OUTER JOIN dbo.Code icsc
                ON icsc.Code_Id = aics.Invoice_Collection_Source_Cd
            LEFT OUTER JOIN dbo.Code istc
                ON istc.Code_Id = aics.Invoice_Source_Type_Cd
            LEFT OUTER JOIN Invoice_Collection_Account_Config_Group_Map icacm
                ON icacm.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            LEFT OUTER JOIN Invoice_Collection_Account_Config_Group icacg
                ON icacg.Invoice_Collection_Account_Config_Group_ID = icacm.Invoice_Collection_Account_Config_Group_Id
        WHERE
            (   (   icac.Account_Id = @Account_Id
                    AND (   @Is_Chase_Activated IS NULL
                            OR  icac.Is_Chase_Activated = @Is_Chase_Activated))
                OR  icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id)
            AND (   @Invoice_Collection_Service_Start_Dt IS NULL
                    OR  (   icac.Invoice_Collection_Account_Config_Id <> ISNULL(
                                                                             @Invoice_Collection_Account_Config_Id, 0)
                            AND (   Invoice_Collection_Service_Start_Dt BETWEEN @Invoice_Collection_Service_Start_Dt
                                                                        AND     @Invoice_Collection_Service_End_Dt
                                    OR  Invoice_Collection_Service_End_Dt BETWEEN @Invoice_Collection_Service_End_Dt
                                                                          AND     @Invoice_Collection_Service_End_Dt
                                    OR  @Invoice_Collection_Service_Start_Dt BETWEEN Invoice_Collection_Service_Start_Dt
                                                                             AND     Invoice_Collection_Service_End_Dt
                                    OR  @Invoice_Collection_Service_End_Dt BETWEEN Invoice_Collection_Service_End_Dt
                                                                           AND     Invoice_Collection_Service_End_Dt)))
        ORDER BY
            icac.Invoice_Collection_Service_End_Dt DESC;


    END;

    ;







GO

GRANT EXECUTE ON  [dbo].[Invoice_Collection_Account_Config_Sel] TO [CBMSApplication]
GO
