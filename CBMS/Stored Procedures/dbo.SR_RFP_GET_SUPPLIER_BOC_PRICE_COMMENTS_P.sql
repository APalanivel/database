
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
   
 dbo.[SR_RFP_GET_SUPPLIER_BOC_PRICE_COMMENTS_P]  
  
DESCRIPTION:  
     Gets the SR_RFP_SUPPLIER_PRICE_COMMENTS details
  
INPUT PARAMETERS:  
Name			DataType	Default Description  
-------------------------------------------------------------------------------------------  
 @userId       VARCHAR
 @sessionId    VARCHAR
 @bidId        INT
    
OUTPUT PARAMETERS:  
Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
-------------------------------------------------------------  
	EXEC dbo.SR_RFP_GET_SUPPLIER_BOC_PRICE_COMMENTS_P 1,1,931  
	EXEC dbo.SR_RFP_GET_SUPPLIER_BOC_PRICE_COMMENTS_P  1,1,1452893
	EXEC dbo.SR_RFP_GET_SUPPLIER_BOC_PRICE_COMMENTS_P  1,1,1452896
  
AUTHOR INITIALS:  
	Initials	Name  
------------------------------------------------------------  
	AKR         Ashok Kumar Raju
	RR			Raghu Reddy

MODIFICATIONS	
	Initials	Date		Modification  
------------------------------------------------------------  
	AKR         2012-10-10  Modifed the Code to include broker_included_type_id column as a part of POCO
							Modified to Standards
	RR			2016-04-20	GCS-758 Added Pricing_Comments to select list
*/  
CREATE PROCEDURE [dbo].[SR_RFP_GET_SUPPLIER_BOC_PRICE_COMMENTS_P]
      ( 
       @userId VARCHAR
      ,@sessionId VARCHAR
      ,@bidId INT )
AS 
BEGIN      
  
      SET NOCOUNT ON 
  
      SELECT
            srsp.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
           ,srsp.IS_CREDIT_APPROVAL
           ,srsp.NO_CREDIT_COMMENTS
           ,srsp.PRICE_RESPONSE_COMMENTS
           ,srsp.Broker_Included_Type_Id
           ,srsp.Pricing_Comments
      FROM
            dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS srsp
            INNER JOIN dbo.SR_RFP_BID srb
                  ON srsp.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID = srb.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
      WHERE
            srb.SR_RFP_BID_ID = @bidId

END;

;
GO


GRANT EXECUTE ON  [dbo].[SR_RFP_GET_SUPPLIER_BOC_PRICE_COMMENTS_P] TO [CBMSApplication]
GO
