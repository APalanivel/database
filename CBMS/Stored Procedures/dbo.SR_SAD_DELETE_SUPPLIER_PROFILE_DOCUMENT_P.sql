SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_SAD_DELETE_SUPPLIER_PROFILE_DOCUMENT_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@cbmsImageId      int,
@vendorId         int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
--exec dbo.SR_SAD_DELETE_SUPPLIER_PROFILE_DOCUMENT_P 228021,650

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_SAD_DELETE_SUPPLIER_PROFILE_DOCUMENT_P

@cbmsImageId int,
@vendorId int

AS
	
set nocount on
	
	declare @count int 
	
	select @count = count(cbms_image_id)
		from sr_supplier_profile_documents join sr_supplier_profile
		on sr_supplier_profile_documents.sr_supplier_profile_id = sr_supplier_profile.sr_supplier_profile_id
		where sr_supplier_profile.vendor_id = @vendorId 

IF (@count>1)
   BEGIN
      DELETE 
	      SR_SUPPLIER_PROFILE_DOCUMENTS
      WHERE 
	      cbms_image_id = @cbmsImageId
   END
ELSE 
   BEGIN

	   DELETE 
		   SR_SUPPLIER_PROFILE_DOCUMENTS 
	   WHERE 
		   cbms_image_id = @cbmsImageId

	   UPDATE 
		   SR_RFP_CHECKLIST 
	   SET
		   IS_SUPPLIER_DOCS = NULL	
	   WHERE 
		   NEW_SUPPLIER_ID IN (SELECT DISTINCT 
								   V.VENDOR_ID 
							   FROM 	
								   VENDOR V, SR_RFP_CLOSURE cl
     						   WHERE 
     							   CL.VENDOR_ID = V.VENDOR_ID AND
								   V.VENDOR_ID = @vendorId
							   )
   END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_DELETE_SUPPLIER_PROFILE_DOCUMENT_P] TO [CBMSApplication]
GO
