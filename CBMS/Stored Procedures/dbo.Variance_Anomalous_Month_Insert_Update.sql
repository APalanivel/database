SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*********   
NAME:  
    dbo.Variance_Anomalous_Month_Insert_Update  
 
DESCRIPTION:  
    Used to insert log details into variance_log table

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
@Account_id INT,
	  @Service_Month date,
	  @Is_Anomalous BIT,
	  @User_id INT
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:   
------------------------------------------------------------    
	dbo.Variance_Anomalous_Month_Insert_Update
	
AUTHOR INITIALS:

Initials Name
------------------------------------------------------------
AP		Arunkumar Palanivel

Initials Date		Modification  
------------------------------------------------------------  
AP		Feb 19,2020		Procedure is created to insert or update anomalous month entry	
******/

CREATE PROCEDURE [dbo].[Variance_Anomalous_Month_Insert_Update]
      (
      @Account_id    INT
    , @Service_Month DATE
    , @Is_Anomalous  BIT
    , @User_id       INT )
AS
      BEGIN

            SET NOCOUNT ON;
            SET XACT_ABORT ON;

			DECLARE @Event_Type_Name VARCHAR(100)
			SET @Event_Type_Name ='Month marked as anomalous' 
			
			 --SELECT @Event_Type_Cd=
				--				cd.code_id
				--				FROM
				--				dbo.Codeset cs
				--				INNER JOIN dbo.Code cd
				--				ON cd.Codeset_Id = cs.Codeset_Id
				--				WHERE
				--				cs.Std_Column_Name = 'Variance_Test_Event_Type_Cd'
				--				AND cd.Code_Value =  'Month marked as anomalous'
				 

            IF NOT EXISTS
                  (     SELECT
                              1
                        FROM  dbo.Variance_Account_Month_Anomalous_Status vra
                        WHERE vra.Account_Id = @Account_id
                              AND   vra.Service_Month = @Service_Month )
							  AND @Is_Anomalous =1
                  BEGIN

                        INSERT INTO dbo.Variance_Account_Month_Anomalous_Status (
                                                                                      Account_Id
                                                                                    , Service_Month
                                                                                    , Is_Anomalous
                                                                                    , Created_User_Id
                                                                                    , Created_Ts
                                                                                )
                        VALUES
                             ( @Account_id      -- Account_Id - int
                             , @Service_Month   -- Service_Month - date
                             , @Is_Anomalous    -- Is_Anomalous - bit
                             , @User_id         -- Created_User_Id - int
                             , getdate()        -- Created_Ts - datetime
                              );

                  END;

            ELSE
                  BEGIN

                        UPDATE
                              vam
                        SET
                              vam.Updated_User_Id = @User_id
                            , vam.Updated_Ts = getdate()
                            , vam.Is_Anomalous = @Is_Anomalous
                        FROM  dbo.Variance_Account_Month_Anomalous_Status vam
						WHERE vam.Account_Id = @Account_id
						AND vam.Service_Month =@Service_Month;

                  END;


				IF (@Is_Anomalous =1)

				BEGIN
                
				exec [dbo].[Variance_Test_Event_Audit_Ins]

				@Account_Id = @Account_id
				,@Service_Month = @Service_Month
				,@Event_Type_Name  =@Event_Type_Name --logic is mentioned below
				,@Variance_Routed_To_Queue_Id  =null
				,@Event_By_User_Id =@User_id  
end



           -- COMMIT;

            RETURN;

      END;
GO
GRANT EXECUTE ON  [dbo].[Variance_Anomalous_Month_Insert_Update] TO [CBMSApplication]
GO
