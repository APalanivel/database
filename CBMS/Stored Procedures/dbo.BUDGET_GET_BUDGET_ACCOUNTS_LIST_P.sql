SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





--exec BUDGET_GET_BUDGET_ACCOUNTS_LIST_P 264

CREATE    PROCEDURE dbo.BUDGET_GET_BUDGET_ACCOUNTS_LIST_P	
	@budgetId int
	AS
	begin
	set nocount on

	select 	budget_account_id 
	from 	budget_account
	where 	budget_id = @budgetId


	end







GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_BUDGET_ACCOUNTS_LIST_P] TO [CBMSApplication]
GO
