SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE   PROCEDURE dbo.GET_CORPORATE_DIVISION_ID_P
	@client_id int
	AS
	begin
		set nocount on

		select	division_id 
		from	division 	
		where	client_id = @client_id 
			and IS_CORPORATE_DIVISION = 1 

	end



GO
GRANT EXECUTE ON  [dbo].[GET_CORPORATE_DIVISION_ID_P] TO [CBMSApplication]
GO
