SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	 dbo.GET_CONTRACT_VOLUMES_FOR_UPDATE_FORECAST_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@contractId		INTEGER
	@clientId		INTEGER       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

exec dbo.GET_CONTRACT_VOLUMES_FOR_UPDATE_FORECAST_P  44625,110

exec dbo.GET_CONTRACT_VOLUMES_FOR_UPDATE_FORECAST_P  10956,119

exec dbo.GET_CONTRACT_VOLUMES_FOR_UPDATE_FORECAST_P  11006,206

exec dbo.GET_CONTRACT_VOLUMES_FOR_UPDATE_FORECAST_P  11428, 124

exec dbo.GET_CONTRACT_VOLUMES_FOR_UPDATE_FORECAST_P  11155, 170

EXEC GET_CONTRACT_VOLUMES_FOR_UPDATE_FORECAST_P 54924,10003 
EXEC GET_CONTRACT_VOLUMES_FOR_UPDATE_FORECAST_P 82210,10056


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SS			Subhash Subramanyam
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	04/28/2009	Autogenerated script
	 SS       	08/31/2009	Used contract_id to join contract table and supplier_account_meter_map instead of account_id, Removed group by as we do not have aggregation on any columns
	 SSR		08/31/2010  DISTINCT clause added in the SELECT clause filtered the meters having the same volume with in a site, done the following changes to fix it
							Added meter_id column to get details for all meters of contracts.
							Removed hard coded values ,Used CTE to get SiteIds,
							division and VwsiteName replaced by the base tables.
							unused @userId & @sessionId parameters removed.
	SKA			10/11/2010	Added the join with contract table in second query to get the volumn only for NaturalGas instead of all.							
******/
CREATE PROCEDURE dbo.GET_CONTRACT_VOLUMES_FOR_UPDATE_FORECAST_P
	@contractId AS INTEGER
  , @clientId AS INTEGER
AS
BEGIN

        SET  NOCOUNT ON

        DECLARE
            @NG_Commodity_ID INT
          , @Hedge_ID INT

        SELECT
            @NG_Commodity_ID = Commodity_Id
        FROM
            dbo.Commodity
        WHERE
            Commodity_name = 'Natural Gas'
          
        SELECT
            @Hedge_ID = ent.entity_id
        FROM
            dbo.entity ent
        WHERE
            ent.ENTITY_DESCRIPTION = 'HEDGE_TYPE'
            AND entity_name = 'Does not hedge' ;


        WITH	CTE_SiteId_Contract
                  AS (

						SELECT
							s.site_id
							,RTRIM(ad.city) + ', ' + st.state_name + ' ('+ s.site_name + ')' Site_Name
							,s.DIVISION_ID
							,s.Client_ID
						FROM
							dbo.Supplier_Account_meter_map map
							JOIN CONTRACT con
								ON con.CONTRACT_ID = map.Contract_ID
							JOIN METER m
								ON map.METER_ID = m.METER_ID
							JOIN ACCOUNT a
								ON a.ACCOUNT_ID = m.ACCOUNT_ID
							JOIN SITE s
								ON s.SITE_ID = a.SITE_ID
							JOIN ADDRESS ad
								ON ad.ADDRESS_ID = s.PRIMARY_ADDRESS_ID
							JOIN STATE st
								ON st.STATE_ID = ad.STATE_ID
						WHERE
							con.contract_id = @contractId
							AND con.commodity_type_id = @NG_Commodity_ID
						GROUP BY
							s.site_id
							,s.DIVISION_ID
							,s.Client_ID
							,ad.city
							,st.state_name
							,s.site_name
                     )
			SELECT
				SUM( isnull(vol.volume,0) * conversion.CONVERSION_FACTOR ) AS volume
				, cte_site.site_id
				, cte_site.Site_Name
				, cte_site.DIVISION_ID
				, vol.month_identifier
				, frequencyType.entity_name
				, hedge.VOLUME_UNITS_TYPE_ID
            FROM
                CTE_SiteId_Contract cte_site
                JOIN dbo.RM_ONBOARD_HEDGE_SETUP AS hedge
                    ON hedge.site_id = cte_site.site_id
                JOIN dbo.client AS cli
                    ON cte_site.client_id = cli.client_id
                JOIN dbo.account AS utilacc
                    ON cte_site.site_id = utilacc.site_id
                JOIN dbo.meter AS met
                    ON met.account_id = utilacc.account_id
                JOIN dbo.supplier_account_meter_map AS map
                    ON map.meter_id = met.meter_id
                JOIN dbo.CONTRACT con
					ON con.CONTRACT_ID = map.Contract_ID 
                JOIN dbo.contract_meter_volume AS vol
                    ON map.contract_id = vol.contract_id
                       AND map.meter_id = vol.meter_id
                JOIN dbo.CONSUMPTION_UNIT_CONVERSION AS conversion
                    ON conversion.converted_unit_id = hedge.VOLUME_UNITS_TYPE_ID
                       AND conversion.base_unit_id = vol.UNIT_TYPE_ID
                JOIN dbo.entity AS frequencyType
                    ON frequencyType.entity_id = vol.FREQUENCY_TYPE_ID
            WHERE
                cli.client_id = @clientId
                AND ( hedge.volume_units_type_id <> @Hedge_ID)
                AND vol.METER_ID IS NOT NULL
                AND con.commodity_type_id = @NG_Commodity_ID
            GROUP BY				
				cte_site.site_id
				,cte_site.Site_Name
				,cte_site.site_name
				,cte_site.DIVISION_ID
				,vol.month_identifier
				,frequencyType.entity_name
				,hedge.VOLUME_UNITS_TYPE_ID

END

GO
GRANT EXECUTE ON  [dbo].[GET_CONTRACT_VOLUMES_FOR_UPDATE_FORECAST_P] TO [CBMSApplication]
GO
