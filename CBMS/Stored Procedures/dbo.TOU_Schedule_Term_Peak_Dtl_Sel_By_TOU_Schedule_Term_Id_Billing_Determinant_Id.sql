SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.TOU_Schedule_Term_Peak_Dtl_Sel_By_TOU_Schedule_Term_Id_Billing_Determinant_Id

DESCRIPTION:
	Selects the peaks associated to the schedule and the billing determinant.

INPUT PARAMETERS:
	Name									DataType		Default	Description
-------------------------------------------------------------------------------
	@Time_Of_Use_Schedule_Term_Id			INT
	@Billing_Determinant_Id					INT				NULL

OUTPUT PARAMETERS:
	Name									DataType		Default	Description
-------------------------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------------------------
					
	Exec dbo.TOU_Schedule_Term_Peak_Dtl_Sel_By_TOU_Schedule_Term_Id_Billing_Determinant_Id 14, 495474
	Exec dbo.TOU_Schedule_Term_Peak_Dtl_Sel_By_TOU_Schedule_Term_Id_Billing_Determinant_Id 402, 496193
	Exec dbo.TOU_Schedule_Term_Peak_Dtl_Sel_By_TOU_Schedule_Term_Id_Billing_Determinant_Id 382, 496189


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------------------------
	CPE			Chaitanya Panduga Eshwar
	BCH			Balaraju Chalumuri

MODIFICATIONS

	Initials	Date			Modification
------------------------------------------------------------------------------
	CPE			2012-07-25		Created
	BCH			2012-09-06		Given working examples.
******/ 
CREATE PROCEDURE dbo.TOU_Schedule_Term_Peak_Dtl_Sel_By_TOU_Schedule_Term_Id_Billing_Determinant_Id
      ( 
       @Time_Of_Use_Schedule_Term_Id INT
      ,@Billing_Determinant_Id INT = NULL )
AS 
BEGIN
	
      SET NOCOUNT ON

      SELECT
            SEA.SEASON_NAME
           ,TOUSTP.Time_Of_Use_Schedule_Term_Peak_Id
           ,TOUSTP.Peak_Name AS Peak_Name
           ,BD.BILLING_DETERMINANT_ID
           ,BD.BILLING_DETERMINANT_NAME
      FROM
            dbo.Time_Of_Use_Schedule_Term_Peak TOUSTP
            LEFT JOIN ( dbo.Time_Of_Use_Schedule_Term_Peak_Billing_Determinant TOUSTPBD
                        JOIN dbo.BILLING_DETERMINANT BD
                              ON TOUSTPBD.BILLING_DETERMINANT_ID = BD.BILLING_DETERMINANT_ID )
                        ON TOUSTP.Time_Of_Use_Schedule_Term_Peak_Id = TOUSTPBD.Time_Of_Use_Schedule_Term_Peak_Id
                           AND TOUSTPBD.BILLING_DETERMINANT_ID = @Billing_Determinant_Id
            LEFT JOIN dbo.SEASON SEA
                  ON TOUSTP.SEASON_ID = SEA.SEASON_ID
      WHERE
            TOUSTP.Time_Of_Use_Schedule_Term_Id = @Time_Of_Use_Schedule_Term_Id
END


;
GO
GRANT EXECUTE ON  [dbo].[TOU_Schedule_Term_Peak_Dtl_Sel_By_TOU_Schedule_Term_Id_Billing_Determinant_Id] TO [CBMSApplication]
GO
