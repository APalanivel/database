
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
                  
NAME: [dbo].[Utility_Account_Interval_Data_Presence_Status_Per_Commodity_Sel_By_Client_Hier_Id]  
  
DESCRIPTION:                        
          SP is used to get the Utility Account Interval Data Presence Status Per Commodity based on the Client_Hier_Id      
  
INPUT PARAMETERS:                        
NAME                        DATATYPE          DEFAULT        DESCRIPTION                        
------------------------------------------------------------                                       
@Site_Client_Hier_Id        INT                    
@MinDate     DATE              
@MaxDate                    DATE                            
  
OUTPUT PARAMETERS:                                          
NAME   DATATYPE DEFAULT  DESCRIPTION  
------------------------------------------------------------  
  
  
USAGE EXAMPLES:                                          
------------------------------------------------------------                                  
  
    EXEC Utility_Account_Interval_Data_Presence_Status_Per_Commodity_Sel_By_Client_Hier_Id  1131, '2011-03-01', '2011-03-31'      
    EXEC Utility_Account_Interval_Data_Presence_Status_Per_Commodity_Sel_By_Client_Hier_Id  36765, '2011-11-01', '2011-11-30'       
  
AUTHOR INITIALS:                                          
INITIALS  NAME                                          
------------------------------------------------------------                                          
 PKY      Pavan Kumar Yadalam  
 RR		  Raghu Reddy
  
MODIFICATIONS  
INITIALS	DATE(YYYY-MM-DD)  MODIFICATION  
------------------------------------------------------------          
PKY			2013-05-02        Created  
RR			2013-10-07		  MAINT-2300 Logic to apply input dates is modified, old logic failed to return intersecting service periods 
								with input dates (Old logic cuad.Service_Start_Dt >= @MinDate AND cuad.Service_End_Dt <= @MaxDate)
*/    
    
CREATE PROCEDURE dbo.Utility_Account_Interval_Data_Presence_Status_Per_Commodity_Sel_By_Client_Hier_Id
      ( 
       @Site_Client_Hier_Id INT
      ,@MinDate DATE
      ,@MaxDate DATE )
AS 
BEGIN  
           
      SET NOCOUNT ON  
  
      SELECT
            com.Commodity_Id
           ,com.Commodity_Name
      FROM
            dbo.Bucket_Account_Interval_Dtl cuad
            INNER JOIN dbo.Bucket_Master bm
                  ON bm.Bucket_Master_Id = cuad.Bucket_Master_Id
            INNER JOIN dbo.Commodity com
                  ON com.Commodity_Id = bm.Commodity_Id
            INNER JOIN Core.Client_Hier_Account ucha
                  ON ucha.Client_Hier_Id = cuad.Client_Hier_Id
                     AND ucha.Account_Id = cuad.Account_Id
      WHERE
            ucha.Client_Hier_Id = @Site_Client_Hier_Id
            AND ( ( @MinDate BETWEEN cuad.Service_Start_Dt
                             AND     cuad.Service_End_Dt )
                  OR ( @MaxDate BETWEEN cuad.Service_Start_Dt
                                AND     cuad.Service_End_Dt ) )
            AND ucha.Account_Type = 'Utility'
      GROUP BY
            com.Commodity_Id
           ,com.Commodity_Name  
  
END 

;
GO

GRANT EXECUTE ON  [dbo].[Utility_Account_Interval_Data_Presence_Status_Per_Commodity_Sel_By_Client_Hier_Id] TO [CBMSApplication]
GO
