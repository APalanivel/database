SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Utility_Dtl_Service_Level_Ins

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	 @Commodity_Id				INT
     @Vendor_Id					INT 
     @Question_Commodity_Map_Id INT
     @Industrial_Flag_Cd		INT
     @Commercial_Flag_Cd		INT
     @Comment_ID				INT = NULL

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN
	EXEC Utility_Dtl_Service_Level_Ins 290,30,25,100306,100307
	ROLLBACK TRAN
	
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	SKA			01/12/2011	Created
******/

CREATE PROC dbo.Utility_Dtl_Service_Level_Ins
      (
       @Commodity_Id INT
      ,@Vendor_Id INT
      ,@Question_Commodity_Map_Id INT
      ,@Industrial_Flag_Cd INT
      ,@Commercial_Flag_Cd INT
      ,@Comment_ID INT = NULL
      )
AS 
BEGIN
 
      SET nocount ON ;

      INSERT INTO
            Utility_Dtl_Service_Level
            (
             Question_Commodity_Map_Id
            ,Vendor_Commodity_Map_Id
            ,Industrial_Flag_Cd
            ,Commercial_Flag_Cd
            ,Comment_ID )
            SELECT
                  @Question_Commodity_Map_Id
                 ,vcm.VENDOR_COMMODITY_MAP_ID
                 ,@Industrial_Flag_Cd
                 ,@Commercial_Flag_Cd
                 ,@Comment_ID
            FROM
                  dbo.VENDOR_COMMODITY_MAP vcm
            WHERE
                  vcm.vendor_id = @Vendor_Id
                  AND vcm.COMMODITY_TYPE_ID = @Commodity_Id
END                        
GO
GRANT EXECUTE ON  [dbo].[Utility_Dtl_Service_Level_Ins] TO [CBMSApplication]
GO
