SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:        
	dbo.City_Site_List_By_Contract_Id        
	
 DESCRIPTION:         
 
   This Procedure is to get all Site_ids and City based on Contract_id.And more than one Site_id if there for a city then it will 
   be comma seperated
   	
 INPUT PARAMETERS:        
 Name			DataType	Default Description        
------------------------------------------------------------        
 @contractId	VARCHAR(150)      
      
OUTPUT PARAMETERS:        
Name	DataType  Default Description        
------------------------------------------------------------        

 USAGE EXAMPLES:        
------------------------------------------------------------
        
	EXEC dbo.City_Site_List_By_Contract_Id	26046
	EXEC SITEID_CITIES_SEL_BY_CONTRACT_ID 26046
	
 AUTHOR INITIALS:        
 Initials	Name        
------------------------------------------------------------        
 SSR		Sharad Srivastava
 
 MODIFICATIONS:        
 Initials	Date			Modification        
------------------------------------------------------------        
  SSR		04/21/2010		Created    
  
******/        
CREATE PROCEDURE dbo.City_Site_List_By_Contract_Id
	@Contract_Id	INT
AS
BEGIN        

    SET NOCOUNT ON;

	WITH Cte_City_Site_List	AS
	(
		SELECT
			addr.City
			,sit.Site_Id
		FROM 
			dbo.SUPPLIER_ACCOUNT_METER_MAP samm
			INNER JOIN dbo.METER MET
				ON MET.METER_ID = samm.METER_ID        
			INNER JOIN dbo.ACCOUNT uacc
				ON uacc.ACCOUNT_ID = MET.ACCOUNT_ID     
			INNER JOIN dbo.SITE sit
				ON SIT.SITE_ID = uacc.SITE_ID
			INNER JOIN dbo.ADDRESS addr
				ON addr.ADDRESS_ID = sit.PRIMARY_ADDRESS_ID
		WHERE
			samm.CONTRACT_ID = @Contract_Id
	)
	SELECT
		csl.CITY
		,LEFT(ST.ST_LIST, LEN(ST.ST_LIST) - 1) AS SITE_ID
	FROM
		Cte_City_Site_List csl
		CROSS APPLY (
					SELECT
						CONVERT(VARCHAR,sl.SITE_ID) + ', '
					FROM
						Cte_City_Site_List sl
					WHERE
						sl.CITY = csl.CITY
					GROUP BY
						sl.SITE_ID
					FOR XML PATH('')
					) ST(ST_LIST)
		GROUP BY
			CITY
			,ST.ST_LIST
	
END 
GO
GRANT EXECUTE ON  [dbo].[City_Site_List_By_Contract_Id] TO [CBMSApplication]
GO
