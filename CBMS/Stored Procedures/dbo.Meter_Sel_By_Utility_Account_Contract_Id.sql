SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.Meter_Sel_By_Utility_Account_Contract_Id

DESCRIPTION:

INPUT PARAMETERS:
	Name					DataType		Default	Description
---------------------------------------------------------------
	@Contract_Id			INT
	@Account_Id				INT 

OUTPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	
USAGE EXAMPLES:
------------------------------------------------------------

	

	EXEC Meter_Sel_By_Utility_Account_Contract_Id 1


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	NR			Narayaana Reddy
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	NR       	2019-07-18	Created for Add - contract

******/
CREATE PROCEDURE [dbo].[Meter_Sel_By_Utility_Account_Contract_Id]
     (
         @Contract_Id INT
         , @Account_Id INT
     )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            uti_cha.Meter_Id
            , uti_cha.Meter_Number
        FROM
            Core.Client_Hier_Account uti_cha
            INNER JOIN Core.Client_Hier_Account sup_cha
                ON uti_cha.Client_Hier_Id = sup_cha.Client_Hier_Id
                   AND  uti_cha.Meter_Id = sup_cha.Meter_Id
        WHERE
            uti_cha.Account_Type = 'utility'
            AND sup_cha.Account_Type = 'Supplier'
            AND sup_cha.Supplier_Contract_ID = @Contract_Id
            AND uti_cha.Account_Id = @Account_Id
        GROUP BY
            uti_cha.Meter_Id
            , uti_cha.Meter_Number;

    END;


GO
GRANT EXECUTE ON  [dbo].[Meter_Sel_By_Utility_Account_Contract_Id] TO [CBMSApplication]
GO
