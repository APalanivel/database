
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********  
NAME: 
		dbo.BUDGET_GET_CONTRACTS_P   

DESCRIPTION:     

	
INPUT PARAMETERS:    
Name					DataType	Default		Description    
------------------------------------------------------------    
@userId					VARCHAR(10)
@sessionId				VARCHAR(20)
@budget_account_id		INT
@budget_start_month		DATETIME
@budget_end_month		DATETIME  


OUTPUT PARAMETERS:    
Name   DataType  Default Description    
------------------------------------------------------------    

USAGE EXAMPLES:    
------------------------------------------------------------    

	EXEC dbo.BUDGET_GET_CONTRACTS_P 112,'',539894,'2013-12-01','2014-11-01'
	EXEC dbo.BUDGET_GET_CONTRACTS_P '','',286221,'2013-12-01','2014-11-01'
	EXEC dbo.BUDGET_GET_CONTRACTS_P '','',531931,'2011-11-01','2012-12-01'
	
	SELECT * FROM dbo.budget_contract_vw WHERE account_id=35877 AND contract_id = 93097
	SELECT * FROM dbo.budget_contract_vw WHERE account_id=75491 AND contract_id = 97491
	SELECT * FROM dbo.budget_contract_vw WHERE account_id=16594 AND contract_id = 83217 


AUTHOR INITIALS:    
Initials	Name    
------------------------------------------------------------    
RR			Raghu Reddy
 
MODIFICATIONS     
Initials	Date		Modification    
------------------------------------------------------------    
RR			2013-07-05	Added description header
						Maint-2041 Added group by to avoid duplicates, contract will have entries for each supplier account, returning
						duplicate records if the contract have multiple supplier accounts associated with an utility account
						dbo.budget_contract_vw view is replaced with Client_Hier_Account and contract tables
    
******/
CREATE PROCEDURE dbo.BUDGET_GET_CONTRACTS_P
      ( 
       @userId VARCHAR(10)
      ,@sessionId VARCHAR(20)
      ,@budget_account_id INT
      ,@budget_start_month DATETIME
      ,@budget_end_month DATETIME )
AS 
BEGIN
      SET NOCOUNT ON ;
      
      DECLARE @temp TABLE
            ( 
             [contract_id] [int]
            ,[ed_contract_number] VARCHAR(150)
            ,[type_of_contract] BIT )
      
      INSERT      INTO @temp
                  ( 
                   contract_id
                  ,ed_contract_number
                  ,type_of_contract )
                  SELECT
                        con.contract_id
                       ,con.ed_contract_number
                       ,1 AS type_of_contract	--1 represents current budget period contract
                  FROM
                        dbo.budget bdg
                        INNER JOIN dbo.budget_account bgdacc
                              ON bdg.budget_id = bgdacc.budget_id
                        INNER JOIN Core.Client_Hier_Account utility
                              ON utility.Account_Id = bgdacc.ACCOUNT_ID
                                 AND bdg.COMMODITY_TYPE_ID = utility.Commodity_Id
                        INNER JOIN Core.Client_Hier_Account supp
                              ON utility.Meter_Id = supp.Meter_Id
                        INNER JOIN dbo.CONTRACT con
                              ON con.CONTRACT_ID = supp.Supplier_Contract_ID
                                 AND con.CONTRACT_TYPE_ID = 153
                  WHERE
                        bgdacc.budget_account_id = @budget_account_id
                        AND con.contract_start_date <= ( dateadd(month, 1, @budget_end_month) - 1 )
                        AND con.contract_end_date >= @budget_start_month
                  GROUP BY
                        con.contract_id
                       ,con.ed_contract_number
		
      IF ( SELECT
            count(contract_id)
           FROM
            @temp ) = 0 
            BEGIN
                  SELECT
                        con.contract_id
                       ,con.ed_contract_number
                       ,0 AS type_of_contract	--0 represents most recent contract in previous budget
                  FROM
                        dbo.budget bgd
                        INNER JOIN dbo.budget_account bdgacc
                              ON bgd.budget_id = bdgacc.budget_id
                        INNER JOIN Core.Client_Hier_Account utility
                              ON utility.Account_Id = bdgacc.ACCOUNT_ID
                                 AND bgd.COMMODITY_TYPE_ID = utility.Commodity_Id
                        INNER JOIN Core.Client_Hier_Account supp
                              ON utility.Meter_Id = supp.Meter_Id
                        INNER JOIN dbo.CONTRACT con
                              ON con.CONTRACT_ID = supp.Supplier_Contract_ID
                                 AND con.CONTRACT_TYPE_ID = 153
                  WHERE
                        bdgacc.budget_account_id = @budget_account_id
                        AND con.contract_start_date < @budget_start_month
                        AND con.contract_end_date = ( SELECT
                                                            max(con1.contract_end_date)
                                                      FROM
                                                            Core.Client_Hier_Account util
                                                            INNER JOIN Core.Client_Hier_Account supp
                                                                  ON util.Meter_Id = supp.Meter_Id
                                                            INNER JOIN dbo.CONTRACT con1
                                                                  ON con1.CONTRACT_ID = supp.Supplier_Contract_ID
                                                                     AND con1.CONTRACT_TYPE_ID = 153
                                                      WHERE
                                                            util.account_id = bdgacc.account_id
                                                            AND util.Commodity_Id = bgd.commodity_type_id )
                  GROUP BY
                        con.contract_id
                       ,con.ed_contract_number
            END
      ELSE 
            BEGIN
                  SELECT
                        contract_id
                       ,ed_contract_number
                       ,type_of_contract
                  FROM
                        @temp
            END

END
;
GO

GRANT EXECUTE ON  [dbo].[BUDGET_GET_CONTRACTS_P] TO [CBMSApplication]
GO
