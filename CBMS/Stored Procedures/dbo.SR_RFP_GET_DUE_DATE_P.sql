
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******               
                           
NAME:  dbo.SR_RFP_GET_DUE_DATE_P                        
                              
DESCRIPTION:  
			
                               
                              
INPUT PARAMETERS:              
	Name				DataType			Default					Description              
-----------------------------------------------------------------------------------  
	@user_id			VARCHAR(10)
    @session_id			VARCHAR(20)
    @account_group_id	INT
    @is_bid_group		BIT
                                          
OUTPUT PARAMETERS:                   
 Name					DataType			Default					Description              
-----------------------------------------------------------------------------------  
                              

USAGE EXAMPLES:              
-----------------------------------------------------------------------------------  
  
	EXEC dbo.SR_RFP_GET_DUE_DATE_P 1,1,12103458,0
	EXEC dbo.SR_RFP_GET_DUE_DATE_P 1,1,10011924,1
  
  
 AUTHOR INITIALS:              
             
	Initials	Name              
-----------------------------------------------------------------------------------  
	RR			Raghu Reddy                                    
                               
 MODIFICATIONS:            
             
	Initials	Date        Modification            
-----------------------------------------------------------------------------------  
	RR			2016-04-15	GCS-681 Added time zone detailed columns
                             
******/ 
CREATE PROCEDURE [dbo].[SR_RFP_GET_DUE_DATE_P]
      ( 
       @user_id VARCHAR(10)
      ,@session_id VARCHAR(20)
      ,@account_group_id INT
      ,@is_bid_group BIT )
AS 
BEGIN
      SET NOCOUNT ON;
      
      DECLARE
            @Due_Date DATETIME
           ,@Due_Dt_By_Timezone DATETIME
           ,@Due_Dt_Time_Zone NVARCHAR(50)
           ,@Due_Dt_Time_Zone_Code NVARCHAR(50)
           ,@Due_Dt_GMT NVARCHAR(50)
      
      SELECT
            @Due_Date = max(due_date)
      FROM
            dbo.SR_RFP_SEND_SUPPLIER
      WHERE
            sr_account_group_id = @account_group_id
            AND is_bid_group = @is_bid_group
            
      SELECT
            @Due_Dt_By_Timezone = srss.Due_Dt_By_Timezone
           ,@Due_Dt_Time_Zone = tz.Time_Zone
           ,@Due_Dt_Time_Zone_Code = tz.Time_Zone_Code
           ,@Due_Dt_GMT = tz.GMT
      FROM
            dbo.SR_RFP_SEND_SUPPLIER srss
            INNER JOIN dbo.Time_Zone tz
                  ON srss.Time_Zone_Id = tz.Time_Zone_Id
      WHERE
            srss.sr_account_group_id = @account_group_id
            AND srss.is_bid_group = @is_bid_group
            AND srss.DUE_DATE = @Due_Date
            
            
            
      SELECT
            @Due_Date AS due_date
           ,@Due_Dt_By_Timezone AS Due_Dt_By_Timezone
           ,@Due_Dt_Time_Zone AS Due_Dt_Time_Zone
           ,@Due_Dt_Time_Zone_Code AS Due_Dt_Time_Zone_Code
           ,@Due_Dt_GMT AS Due_Dt_GMT
END

;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_GET_DUE_DATE_P] TO [CBMSApplication]
GO
