SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_USER_LIST_FOR_VENDOR

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@username      	varchar(20)	          	
	@lastname      	varchar(20)	          	
	@firstname     	varchar(20)	          	
	@vendorid      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	exec dbo.SR_SAD_GET_USER_LIST_FOR_VENDOR 'a','','r',0

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	SSR			09/27/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on	        		        	
******/
CREATE PROCEDURE dbo.SR_SAD_GET_USER_LIST_FOR_VENDOR
    @username VARCHAR(20)
  , @lastname VARCHAR(20)
  , @firstname VARCHAR(20)
  , @vendorid INT
AS 
    BEGIN 

        SET nocount ON
        DECLARE @selectClause VARCHAR(7000)
        DECLARE @fromClause VARCHAR(7000)
        DECLARE @whereClause VARCHAR(7000)
        DECLARE @sqlStatement VARCHAR(8000)
	
	--basic selectClause
        SELECT
            @selectClause = ' select map.sr_supplier_contact_info_id,v.vendor_name,u.username,u.last_name,u.first_name,u.email_adDress,dbo.SR_SAD_FN_GET_STATE_NAMES_FOR_CONTACT (map.sr_supplier_contact_info_id) as state'

	--basic fromClause
        SELECT
            @fromClause = ' from user_info u,vendor v,sr_supplier_contact_vendor_map map,sr_supplier_contact_info supcont '
	
	--basic whereClause
        SELECT
            @whereClause = ' where	map.vendor_id = v.vendor_id and map.sr_supplier_contact_info_id = supcont.sr_supplier_contact_info_id and supcont.user_info_id = u.user_info_id	'
	
        IF ( @vendorid <> 0 ) 
            BEGIN
                SELECT
                    @whereClause = @whereClause + ' AND map.vendor_id = '
                    + STR(@vendorid) 		
            END

        IF ( @username IS NOT NULL
             AND @username <> '' ) 
            BEGIN
                SELECT
                    @whereClause = @whereClause + 'AND u.username like ''%'
                    + @username + '%''' 	
            END
	
        IF ( @lastname IS NOT NULL
             AND @lastname <> '' ) 
            BEGIN
                SELECT
                    @whereClause = @whereClause + 'AND u.last_name like ''%'
                    + @lastname + '%''' 	
            END
	
        IF ( @firstname IS NOT NULL
             AND @firstname <> '' ) 
            BEGIN
                SELECT
                    @whereClause = @whereClause + 'AND u.first_name like ''%'
                    + @firstname + '%''' 	
            END
	
	
        SELECT
            @sqlStatement = @selectClause + @fromClause + @whereClause
	
        EXEC ( @sqlStatement )

END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_USER_LIST_FOR_VENDOR] TO [CBMSApplication]
GO
