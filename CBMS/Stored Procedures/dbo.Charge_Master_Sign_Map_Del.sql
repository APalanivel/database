SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Charge_Master_Sign_Map_Del]  

DESCRIPTION: It Deletes Charge Master Sign Map for Selected Charge Master Id.     
      
INPUT PARAMETERS:          
	NAME				DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Charge_Master_Id	INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
	EXEC Charge_Master_Sign_Map_Del 2675
  Rollback Tran

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			17-JUN-10	CREATED

*/

CREATE PROCEDURE dbo.Charge_Master_Sign_Map_Del
    (
      @Charge_Master_Id INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.CHARGE_MASTER_SIGN_MAP
	WHERE
		CHARGE_MASTER_ID = @Charge_Master_Id

END
GO
GRANT EXECUTE ON  [dbo].[Charge_Master_Sign_Map_Del] TO [CBMSApplication]
GO
