SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
                         
 NAME: dbo.Service_Desk_Dates_Validation_Sel_By_Account_Id                     
                          
 DESCRIPTION: To raise the Valcal Exception
       
		              
                          
 INPUT PARAMETERS:      
                         
 Name                  DataType          Default       Description      
--------------------------------------------------------------------
 @Account_Id			INT        
                          
 OUTPUT PARAMETERS:      
                               
 Name                  DataType          Default       Description      
-------------------------------------------------------------------------------------                            
 USAGE EXAMPLES:                              
-------------------------------------------------------------------------------------                            

SELECT
    cha.Supplier_Account_begin_Dt
    , cha.Supplier_Account_End_Dt
    , cha.Supplier_Contract_ID
FROM
    Core.Client_Hier_Account cha
WHERE
    cha.Account_Id = 1741085

EXEC dbo.Service_Desk_Dates_Validation_Sel_By_Account_Id
    1741085
    , '2019-07-04'
    , '2019-08-05'
    
	                   
 AUTHOR INITIALS:    
       
 Initials                   Name      
-------------------------------------------------------------------------------------  
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
-------------------------------------------------------------------------------------  
 NR                     2019-06-13      Created for ADD Contract.                        
                         
******/



CREATE PROCEDURE [dbo].[Service_Desk_Dates_Validation_Sel_By_Account_Id]
    (
        @Account_Id INT
        , @Exception_Begin_Dt DATE
        , @Exception_End_Dt DATE
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Is_Service_Desk_Dates_Overlap BIT = 0
            , @Is_Already_Service_Desk_Dates_Exists BIT = 0;


        SELECT
            @Is_Already_Service_Desk_Dates_Exists = 1
        FROM
            dbo.Account_Exception ae
        WHERE
            ae.Account_Id = @Account_Id
            AND (   @Exception_Begin_Dt BETWEEN ae.Exception_Begin_Dt
                                        AND     ae.Exception_End_Dt
                    OR  @Exception_End_Dt BETWEEN ae.Exception_Begin_Dt
                                          AND     ae.Exception_End_Dt
                    OR  ae.Exception_Begin_Dt BETWEEN @Exception_Begin_Dt
                                              AND     @Exception_End_Dt
                    OR  ae.Exception_End_Dt BETWEEN @Exception_Begin_Dt
                                            AND     @Exception_End_Dt);

        SELECT
            @Is_Service_Desk_Dates_Overlap = 1
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Account_Id = @Account_Id
            AND cha.Supplier_Contract_ID = -1
            AND (   @Exception_Begin_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                        AND     cha.Supplier_Account_End_Dt
                    OR  @Exception_End_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                          AND     cha.Supplier_Account_End_Dt
                    OR  cha.Supplier_Account_begin_Dt BETWEEN @Exception_Begin_Dt
                                                      AND     @Exception_End_Dt
                    OR  cha.Supplier_Account_End_Dt BETWEEN @Exception_Begin_Dt
                                                    AND     @Exception_End_Dt);


        SELECT
            @Is_Service_Desk_Dates_Overlap AS Is_Service_Desk_Dates_Overlap
            , @Is_Already_Service_Desk_Dates_Exists AS Is_Already_Service_Desk_Dates_Exists;

    END;


GO
GRANT EXECUTE ON  [dbo].[Service_Desk_Dates_Validation_Sel_By_Account_Id] TO [CBMSApplication]
GO
