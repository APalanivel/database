
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
            
/******            
NAME: dbo.Report_Invoice_Determinant_ElectricPower_Excel            
             
            
DESCRIPTION:            
           To Get Pivoted data for determinants for Electric Power for AT&T or AllNonAT&T Clients.    
           This stored procedure is only used as a Data Source from Excel.    
    
            
INPUT PARAMETERS:            
 Name          DataType           Default Description            
------------------------------------------------------------            
 @client_id    BIT (1 for client = AT&T and 0 for AllNOnAT&T clients)                           
 @StartDate    Date                            
 @EndDate      Date                            
            
OUTPUT PARAMETERS:            
 Name   DataType  Default Description            
------------------------------------------------------------            
    
            
USAGE EXAMPLES:             
------------------------------------------------------------            
EXEC dbo.Report_Invoice_Determinant_ElectricPower_Excel 1,'8/1/2010','11/1/2010'            
EXEC dbo.Report_Invoice_Determinant_ElectricPower_Excel 0,'1/1/2010','12/1/2010'     
            
AUTHOR INITIALS:            
Initials        Name            
------------------------------------------------------------            
AKR             Ashok Kumar Raju    
             
MODIFICATIONS            
            
 Initials Date  Modification            
------------------------------------------------------------            
AKR      07/12/2011   sp creation          
AKR      09/07/2011   Added the join condition on CU_Invoice. so, the data is filtered on Is_Reported = 1 and Is_DNT = 0 and Is_Duplicate = 0   
AKR      09/29/2011   Added the Grant Statement
             
******/            
            
CREATE PROCEDURE dbo.Report_Invoice_Determinant_ElectricPower_Excel    
      (     
       @Client_IdO INT    
      ,@StartDateO DATETIME    
      ,@EndDateO DATETIME )    
AS     
BEGIN    
      SET NOCOUNT ON    
    
      DECLARE @SQLString NVARCHAR(MAX) = ''    
      DECLARE @Months VARCHAR(MAX) = ''                        
      DECLARE @Months_m VARCHAR(MAX) = ''   
       
        
     
      SELECT              
            @Months = @Months + '[' + convert(VARCHAR(20), dd.date_d, 110) + '],'              
      FROM              
            meta.Date_Dim  dd            
      WHERE              
            dd.date_d BETWEEN @StartDateO AND @EndDateO              
      ORDER BY              
            dd.Date_D    
              
      SELECT              
            @Months_m = @Months_m + 'CASE WHEN [' + convert(VARCHAR(20), dd.date_d, 110) + '] =-1000 THEN ''FALSE''     
                                         WHEN [DeterminantUOM] = ''Months'' and      
                        [' + convert(VARCHAR(20), dd.date_d, 110) + '] = 0 THEN ''Unable to Calculate''       
                        ELSE ' + 'CAST ([' + convert(VARCHAR(20), dd.date_d, 110) + '] AS VARCHAR(40)) END ' + '''[' + CAST (convert(VARCHAR(10), date_d, 110) AS VARCHAR(20)) + ']'','  
      FROM              
            meta.Date_Dim  dd             
      WHERE              
            dd.date_d BETWEEN @StartDateO AND @EndDateO              
      ORDER BY              
            dd.Date_D                   
                        
                        
                
                        
                                  
      SELECT              
            @Months = stuff(@Months, len(@Months), 1, '')                     
      SELECT              
            @Months_m = stuff(@Months_m, len(@Months_m), 1, '')   
     
    
      SELECT    
            @SQLString = '    
    
 DECLARE @Commodity_ID INT    
    
         
 CREATE TABLE #Client_ID    
      (     
       Client_ID INT PRIMARY KEY )        
                 
                 
   IF @Client_Id = 1     
            INSERT      INTO #Client_ID    
                        SELECT    
                              cl.Client_ID    
                        FROM    
                              dbo.CLIENT cl   
                        WHERE    
                              cl.CLIENT_NAME = ''AT&T Services, Inc.''                
    ELSE     
            INSERT      INTO #Client_ID    
                        SELECT    
                              cl.Client_ID    
                        FROM    
                              dbo.CLIENT  cl  
                        WHERE    
                              cl.CLIENT_NAME <> ''AT&T Services, Inc.'';    
    
    
    
      
SELECT    
      @Commodity_ID = com.Commodity_Id    
FROM    
      dbo.Commodity  com   
WHERE    
      com.Commodity_Name = ''Electric Power'' ;    
    
    
    
  WITH  Meter_Rate_Counts    
        AS ( SELECT    
                  CH.Region_Name [Region]    
                 ,CH.State_Name [State]    
                 ,CH.Client_Name [Client Name]    
                 ,CH.Site_name [Site Name]    
                 ,CHA.Account_Vendor_Name [Utility]    
                 ,CHA.Account_Number [Utility Account Number]    
                 ,CHA.Account_Id [AccountID]    
                 ,[Meter Number] = CASE WHEN COUNT(CHA.Meter_Id) = 1 THEN MAX(CHA.Meter_Number)    
                                        ELSE ''Multiple''    
                                   END    
                 ,[Rate Name] = CASE WHEN COUNT(DISTINCT CHA.Rate_Id) = 1 THEN MAX(CHA.Rate_Name)    
                                     ELSE ''Multiple''    
                                END    
             FROM    
                  core.Client_Hier_Account CHA    
                  JOIN Core.Client_Hier CH    
                     ON CHA.Client_Hier_Id = CH.Client_Hier_Id    
                     AND cha.Account_Type = ''Utility''    
                  join #Client_ID cid on ch.client_id = cid.client_ID    
             WHERE    
                  CHA.Commodity_Id = @Commodity_ID    
                  AND CH.Country_Name IN ( ''usa'', ''canada'', ''mexico'', ''puerto rico'' )    
             GROUP BY    
                  CH.Region_Name    
                 ,CH.State_Name    
                 ,CH.Client_Name    
                 ,CH.Site_name    
                 ,CHA.Account_Vendor_Name    
                 ,CHA.Account_Number    
                 ,CHA.Account_Id)    
                     
                  SELECT      
                   [Region]      
                 ,[State]      
                 ,[Client Name]      
                 ,[Site Name]      
                 ,[Utility]      
                 ,[Utility Account Number]      
                 ,[Rate Name]      
                 ,[Meter Number]      
                 ,[AccountID]      
                 ,[CU_INVOICE_ID]    
                  ,[Commodity Name]    
                 ,[Determinant Bucket]      
                 ,[DeterminantUOM]      
                 ,[Service Month]      
                 ,DeterminantValue = case WHEN DeterminantUOM IN ( ''%'', ''kw'', ''mw'', ''kvar'', ''kva'' ) THEN max(DeterminantValue)      
                                          ELSE sum(DeterminantValue)      
                                     END      
            INTO      
                  #Temp_For_Pivot      
            FROM      
                  ( SELECT      
                        mc.[Region]      
                       ,mc.[State]      
                       ,mc.[Client Name]      
                       ,mc.[Site Name]      
                       ,mc.[Utility]      
                       ,mc.[Utility Account Number]      
                       ,mc.[Rate Name]      
                       ,mc.[Meter Number]      
                       ,mc.[AccountID]    
                       ,cuid.[CU_INVOICE_ID]      
                       ,com.commodity_Name [Commodity Name]    
                       ,bm.Bucket_Name [Determinant Bucket]      
                       ,e.ENTITY_NAME [DeterminantUOM]      
                       ,cuism.SERVICE_MONTH [Service Month]      
                       ,CASE WHEN (isnumeric(DETERMINANT_VALUE) = 1 OR DETERMINANT_VALUE IS NULL) THEN    
                       cast(isnull(( replace(replace(determinant_value, '','', ''''), '' '', '''') ), 0) AS DECIMAL(32, 18))               
                           WHEN  (isnumeric(DETERMINANT_VALUE) = 0 AND DETERMINANT_VALUE IS NOT NULL) THEN -1000  END   [DeterminantValue]   
                    FROM      
                        Meter_Rate_Counts mc      
                        JOIN dbo.CU_INVOICE_SERVICE_MONTH cuism      
                              ON cuism.Account_ID = mc.AccountId      
                        JOIN dbo.CU_INVOICE_DETERMINANT cuid      
                              ON cuid.CU_INVOICE_ID = cuism.CU_INVOICE_ID      
                                 AND cuid.COMMODITY_TYPE_ID = @Commodity_ID      
                        JOIN dbo.Commodity com    
                              ON com.commodity_ID = @Commodity_ID    
                        LEFT JOIN dbo.Bucket_Master bm      
                              ON bm.Bucket_Master_Id = cuid.Bucket_Master_Id      
                        LEFT JOIN dbo.ENTITY e      
                              ON e.ENTITY_ID = cuid.UNIT_OF_MEASURE_TYPE_ID      
                    WHERE      
                        SERVICE_MONTH BETWEEN @StartDate AND @EndDate ) x      
            GROUP BY      
                  [Region]      
                 ,[State]      
                 ,[Client Name]      
                 ,[Site Name]      
                 ,[Utility]      
                 ,[Utility Account Number]      
                 ,[Rate Name]      
                 ,[Meter Number]      
                 ,[AccountID]      
                 ,[CU_INVOICE_ID]      
                 ,[Commodity Name]    
                 ,[Determinant Bucket]      
                 ,[DeterminantUOM]      
                 ,[Service Month]              
                     
                     
           ; WITH       
            CTE_Months_Invoice_Count      
      AS      
            ( SELECT      
                  cuism.Account_ID      
                 ,cuism.Service_Month      
                 ,cuism.CU_INVOICE_ID      
                 ,count(cuism.CU_INVOICE_ID) OVER ( PARTITION BY cuism.ACCOUNT_ID, cuism.SERVICE_MONTH ) AS Invoice_Count      
                 ,count(cuism.SERVICE_MONTH) OVER ( PARTITION BY cuism.CU_INVOICE_ID, cuism.ACCOUNT_ID ) AS Month_Count      
              FROM      
                  dbo.CU_INVOICE_SERVICE_MONTH cuism      
                  INNER JOIN core.Client_Hier_Account cha      
                        ON cuism.Account_ID = cha.Account_Id      
                  INNER JOIN core.Client_Hier ch      
                        ON cha.Client_Hier_Id = ch.Client_Hier_Id      
              WHERE      
                  ch.Client_Id IN ( SELECT      
                                          Client_ID      
                                    FROM      
                                          #Client_ID )      
                  AND cuism.SERVICE_MONTH BETWEEN @StartDate AND @EndDate       
                  AND cha.commodity_id = @Commodity_ID              
                  AND ch.Country_Name IN ( ''usa'', ''canada'', ''mexico'', ''puerto rico'' )
                  GROUP BY 
                   cuism.Account_ID      
                 ,cuism.Service_Month      
                 ,cuism.CU_INVOICE_ID  )      
                                        
                      
                             
            INSERT INTO #Temp_For_Pivot       
                            
              SELECT        
                    DISTINCT [Region]              
                   ,[State]              
                   ,[Client Name]              
                   ,[Site Name]              
                   ,[Utility]              
                   ,[Utility Account Number]              
                   ,[Rate Name]              
                   ,[Meter Number]              
                   ,[AccountID]              
                   ,0         
                   ,[Commodity Name]            
                   , [Determinant Bucket]              
                   ,[DeterminantUOM]              
                   , [Service Month]         
                   ,CASE WHEN MAX(Invoice_Count) = 1 THEN MAX(Month_Count)      
     WHEN MAX(Invoice_Count) > 1 AND MAX(Month_Count) = MIN(Month_Count) THEN MAX(Month_Count)      
     ELSE 0      
        END   [Billing Days]        
                  FROM        
                    (  SELECT      
                         DISTINCT temp.[Region]              
                       ,temp.[State]              
                       ,temp.[Client Name]              
                       ,temp.[Site Name]              
                       ,temp.[Utility]              
                       ,temp.[Utility Account Number]              
                       ,temp.[Rate Name]              
                       ,temp.[Meter Number]              
                       ,temp.[AccountID]              
                       ,temp.CU_INVOICE_ID            
                       ,temp.[Commodity Name]            
                       ,''Count of Months'' [Determinant Bucket]              
                       ,''Months'' [DeterminantUOM]              
                       ,temp.[SERVICE MONTH] [Service Month]       
                       ,cuser.Invoice_Count      
                       ,cuser.Month_Count        
                       FROM        
                       #Temp_For_Pivot  temp          
                       INNER JOIN  CTE_Months_Invoice_Count cuser        
                         ON temp.CU_INVOICE_ID = cuser.CU_INVOICE_ID AND cuser.Account_ID = temp.[AccountID]        
                        AND temp.[SERVICE MONTH] = cuser.service_month      
                        INNER JOIN dbo.CU_INVOICE cuin
                        ON cuin.CU_INVOICE_ID = temp.CU_INVOICE_ID
                        WHERE  cuin.Is_Reported = 1 and cuin.Is_DNT = 0 and cuin.Is_Duplicate = 0
                       ) k        
                      GROUP BY        
                         [Region]              
                       ,[State]              
                       ,[Client Name]              
                       ,[Site Name]              
                       ,[Utility]              
                       ,[Utility Account Number]              
                       ,[Rate Name]              
                       ,[Meter Number]              
                       ,[AccountID]              
                       ,[Commodity Name]            
                       , [Determinant Bucket]              
                       ,[DeterminantUOM]              
                       , [Service Month]       
                     
              
                     
       
     SELECT                 
    [Region]                
      ,[State]                
      ,[Client Name]                
      ,[Site Name]          
      ,[Utility]                
      ,[Utility Account Number]                
      ,[Rate Name]                
      ,[Meter Number]                 
      ,[AccountID]                
      ,[Commodity Name]            
      ,[Determinant Bucket]        
      ,[DeterminantUOM]                
      ,' + @Months_m + '                
                         
      FROM                 
     (                
     SELECT                 
    [Region]                
      ,[State]                
      ,[Client Name]                
      ,[Site Name]                
      ,[Utility]                
      ,[Utility Account Number]                
      ,[Meter Number]                 
      ,[AccountID]                
      ,[Commodity Name]             
      ,[Determinant Bucket]       
      ,[DeterminantUOM]                
      ,[Service Month]                
      ,[Rate Name]                
      ,DeterminantValue  
      FROM #Temp_For_Pivot) tem   
     PIVOT (SUM([DeterminantValue])                
     FOR [Service Month] IN (' + @Months + ')) AS pvt
      '       
    
    
    
    
      EXEC sp_executesql     
            @SQLString    
           ,N'@Client_Id INT, @StartDate DATETIME, @EndDate DATETIME '    
           ,@Client_Id = @Client_IdO    
           ,@StartDate = @StartDateO    
           ,@EndDate = @EndDateO    
              
              
END




GO
GRANT EXECUTE ON  [dbo].[Report_Invoice_Determinant_ElectricPower_Excel] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_Invoice_Determinant_ElectricPower_Excel] TO [CBMSApplication]
GO
