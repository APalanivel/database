SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******        
NAME:        
 cbms_prod.dbo.Contract_Meter_Sel_By_ContrcatID         
        
DESCRIPTION:        
        to get the count of the meter which are associated to that account for every meter
        show contract start/end date in case meter ASSOCIATION/SSOCIATION are null
        
INPUT PARAMETERS:        
 Name     DataType  Default Description        
------------------------------------------------------------        
@contract_id	INT
@meter_id		VARCHAR(MAX) 
        
OUTPUT PARAMETERS:        
 Name   DataType  Default Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:        
------------------------------------------------------------        
	EXEC Contract_Meter_Sel_By_ContrcatID @contract_id = 12048,@meter_id = '183,321,953'
	EXEC METERS_FOR_CONTRACT_Sel @contract_id = 12048,@meter_id = '183,321,953'

	select * from SUPPLIER_ACCOUNT_METER_MAP where Contract_ID = 79512
	exec Contract_Meter_Sel_By_ContrcatID 79512, '144540'
	
	EXEC Contract_Meter_Sel_By_ContrcatID @contract_id = 85003,@meter_id = '102774,97021,96987,102897,102741,102905'
	SELECT * FROM dbo.SUPPLIER_ACCOUNT_METER_MAP WHERE Contract_ID =85003
	
	EXEC Contract_Meter_Sel_By_ContrcatID @contract_id = 88911,@meter_id = '25825,25826'
	SELECT * FROM dbo.SUPPLIER_ACCOUNT_METER_MAP WHERE  Contract_ID =88911


        
AUTHOR INITIALS:        
 Initials Name        
------------------------------------------------------------        
SKA		Shobhit Kumar Agrawal
RR		Raghu Reddy

MODIFICATIONS                
 Initials  Date   Modification        
------------------------------------------------------------        
SKA		06/10/2010  Created   
SKA		06/11/2010	Added partition by clause for Row_Number 
RR		2012-01-02	Added ACCOUNT_ID in final select clause as a part of delete tool enhancement
RR		2018-03-19	MAINT-6969 This script used for supplier account history cleanup on last/all meter(s) moved/delete based on Meter_Count 
					column if the value is = 1, this is working fine if the account is having only one meter and as it is last but failing
					if there more than one meter. Actually this script should return remaning based on moving meters provided in input. Added
					new column Remaining_Meter_Count and cleanup will be called for each respective account having this field value is zero.

******/     

CREATE PROCEDURE [dbo].[Contract_Meter_Sel_By_ContrcatID]
      @contract_id INT
     ,@meter_id VARCHAR(MAX)
AS 
BEGIN
      SET NOCOUNT ON;    
	
      WITH  CTE_SupMeter_Det
              AS ( SELECT
                        samm.ACCOUNT_ID
                       ,Row_Num = COUNT(DISTINCT samm.METER_ID)
                       ,samm.Contract_ID
                   FROM
                        dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                   WHERE
                        samm.Contract_ID = @contract_id
                   GROUP BY
                        samm.ACCOUNT_ID
                       ,samm.Contract_ID)
            SELECT
                  NULL AS METER_ID
                 ,a.ACCOUNT_NUMBER
                 ,cont.CONTRACT_START_DATE AS ASSOCIATION_DATE
                 ,cont.CONTRACT_END_DATE AS DISASSOCIATION_DATE
                 ,cte1.Row_Num AS Meter_Count
                 ,a.ACCOUNT_ID
                 ,cte1.Row_Num - remamtrs.Mtrs_Moving AS Remaining_Meter_Count
            FROM
                  CTE_SupMeter_Det cte1
                  INNER JOIN dbo.ACCOUNT a
                        ON a.account_id = cte1.ACCOUNT_ID
                  INNER JOIN dbo.CONTRACT cont
                        ON cont.CONTRACT_ID = cte1.Contract_ID
                  INNER JOIN ( SELECT
                                    samm1.ACCOUNT_ID
                                   ,COUNT(DISTINCT samm1.METER_ID) Mtrs_Moving
                               FROM
                                    ufn_Split(@meter_id, ',') segm
                                    INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm1
                                          ON CAST(segm.Segments AS INT) = samm1.METER_ID
                               WHERE
                                    samm1.Contract_ID = @contract_id
                               GROUP BY
                                    samm1.ACCOUNT_ID ) remamtrs
                        ON cte1.ACCOUNT_ID = remamtrs.ACCOUNT_ID

END
GO

GRANT EXECUTE ON  [dbo].[Contract_Meter_Sel_By_ContrcatID] TO [CBMSApplication]
GO
