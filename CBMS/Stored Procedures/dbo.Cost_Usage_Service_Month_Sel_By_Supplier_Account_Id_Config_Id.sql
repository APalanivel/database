SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [DBO].[Cost_Usage_Service_Month_Sel_By_Supplier_Account_Id_Config_Id]  
     
DESCRIPTION:

	To Get Service Month of Cost / Usage for Selected Account Id from both Cost_Usage and
	Cost_Usage_Account_Dtl

INPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
@Account_Id		INT
@StartIndex		INT			1			
@EndIndex		INT			2147483647

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------


EXEC dbo.Cost_Usage_Service_Month_Sel_By_Supplier_Account_Id_Config_Id 1197550,507644, 1,25


AUTHOR INITIALS:

INITIALS	    NAME
------------------------------------------------------------
NR				Narayana Reddy


MODIFICATIONS
INITIALS	DATE		    MODIFICATION
------------------------------------------------------------     
NR			2020-01-16	MAINT-9734 - Created - Added @Supplier_Account_Config_Id new parameter.
*/

CREATE PROCEDURE [dbo].[Cost_Usage_Service_Month_Sel_By_Supplier_Account_Id_Config_Id]
    (
        @Supplier_Account_Id INT
        , @Supplier_Account_Config_Id INT
        , @StartIndex INT = 1
        , @EndIndex INT = 2147483647
    )
AS
    BEGIN
        SET NOCOUNT ON;

        WITH Cte_Service_Month_List
        AS (
               SELECT
                    CUAD.Service_Month
                    , ROW_NUMBER() OVER (ORDER BY
                                             Service_Month) Row_Num
                    , COUNT(1) OVER () Total_Rows
               FROM
                    dbo.Cost_Usage_Account_Dtl CUAD
                    INNER JOIN Core.Client_Hier_Account cha
                        ON cha.Account_Id = CUAD.ACCOUNT_ID
               WHERE
                    cha.Account_Id = @Supplier_Account_Id
                    AND cha.Supplier_Account_Config_Id = @Supplier_Account_Config_Id
                    AND CUAD.Service_Month BETWEEN cha.Supplier_Account_begin_Dt
                                           AND     cha.Supplier_Account_End_Dt
               GROUP BY
                   CUAD.Service_Month
           )
        SELECT
            CSML.Service_Month
            , CSML.Total_Rows
        FROM
            Cte_Service_Month_List CSML
        WHERE
            CSML.Row_Num BETWEEN @StartIndex
                         AND     @EndIndex;

    END;

GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Service_Month_Sel_By_Supplier_Account_Id_Config_Id] TO [CBMSApplication]
GO
