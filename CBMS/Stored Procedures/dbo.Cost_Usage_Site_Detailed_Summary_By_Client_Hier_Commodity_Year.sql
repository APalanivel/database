SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
Name:    
 CbmS.dbo.Cost_Usage_Site_Detailed_Summary_By_Client_Hier_Commodity_Year  
   
 Description:    
	Used to fetch the data for site level detailed bucket report

 Input Parameters:    
    Name				DataType      Default Description    
------------------------------------------------------------------------    
    @Client_Hier_Id     INT  
    @Commodity_Id		INT  
    @Report_Year		smALLINT  
    @uom_Type_Id		INT  
    @currency_Unit_Id   INT

 Output Parameters:    
 Name  Datatype  Default Description    
------------------------------------------------------------    
   
 Usage Examples:  
------------------------------------------------------------    

    EXEC dbo.Cost_Usage_Site_Detailed_Summary_By_Client_Hier_Commodity_Year 17654, 290, 2011, 12, 3
    EXEC dbo.Cost_Usage_Site_Detailed_Summary_By_Client_Hier_Commodity_Year 5459, 291, 2010, 25, 3 
    EXEC dbo.Cost_Usage_Site_Detailed_Summary_By_Client_Hier_Commodity_Year 5459, 290, 2010, 12, 3  
    
    EXEC dbo.Cost_Usage_Site_Detailed_Summary_By_Client_Hier_Commodity_Year 32199, 290, 2007, 12, 3  
    

Author Initials:
 Initials	Name  
------------------------------------------------------------  
 HG			Harihara Suthan G
   
 Modifications :    
 Initials		Date	    Modification    
------------------------------------------------------------    
 HG				2012-06-25	Created
 HG				2012-06-27	Data_Source column added as wanted to differentiate manual and invoice data in reports
 ******/
CREATE PROCEDURE dbo.Cost_Usage_Site_Detailed_Summary_By_Client_Hier_Commodity_Year
      ( 
       @Client_Hier_Id INT
      ,@Commodity_Id INT
      ,@Report_Year SMALLINT
      ,@uom_Type_Id INT
      ,@currency_Unit_Id INT )
AS 
BEGIN  
  
      SET NOCOUNT ON  
  
      DECLARE
            @Calendar_Year_Start_Month DATE
           ,@currency_Group_Id INT
  
      DECLARE @Bucket_List TABLE
            ( 
             Bucket_Master_Id INT PRIMARY KEY CLUSTERED
            ,Bucket_Name VARCHAR(255)
            ,uom_Type_Id INT
            ,Bucket_Type_cd INT )  
  
      DECLARE @Cu_Data TABLE
            ( 
             Bucket_Master_ID INT
            ,Bucket_Name VARCHAR(200)
            ,Service_Month DATE
            ,Month_Number INT
            ,Bucket_Type VARCHAR(25)
            ,uom_Type_Id INT
            ,Bucket_Value DECIMAL(28, 10) 
            ,Data_Source VARCHAR(25))              

      SELECT
            @currency_Group_id = ch.Client_currency_Group_Id
      FROM
            Core.Client_Hier ch
      WHERE
            ch.Client_Hier_Id = @Client_Hier_Id

      SET @Calendar_Year_Start_Month = CONVERT(DATE, '1/1/' + CONVERT(VARCHAR(4), @Report_Year))

      INSERT      INTO @Bucket_List
                  (
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,uom_Type_Id
                  ,Bucket_Type_cd )
                  SELECT
                        bm.Bucket_Master_Id
                       ,bm.Bucket_Name
                       ,CASE WHEN bm.Bucket_Name IN ( 'Demand', 'Billed Demand', 'Contract Demand', 'Reactive Demand' ) THEN bm.Default_uom_Type_Id
                             ELSE @uom_Type_Id
                        END
                       ,bm.Bucket_Type_cd
                  FROM
                        CbmS.dbo.Bucket_Master bm
                  WHERE
                        bm.Commodity_Id = @Commodity_Id
                        AND bm.Is_Shown_on_Site = 1
                        AND bm.Is_Active = 1 ;  
  
      INSERT      INTO @Cu_Data
                  ( 
                   Bucket_Master_ID
                  ,Bucket_Name
                  ,Service_Month
                  ,Month_Number
                  ,Bucket_Type
                  ,uom_Type_Id
                  ,Bucket_Value 
                  ,Data_Source)
                  SELECT
                        bl.Bucket_Master_Id
                       ,bl.Bucket_Name
                       ,sm.Date_D
                       ,sm.Month_Num
                       ,cd.Code_Value
                       ,( CASE WHEN uomconv.Conversion_Factor = 0
                                    AND cd.Code_Value = 'Determinant' THEN cusd.uom_Type_Id
                               WHEN uomconv.Conversion_Factor != 0
                                    AND cd.Code_Value = 'Determinant' THEN bl.uom_Type_Id
                          END ) AS uom_Type_Id
                       ,SUM(CASE WHEN cd.Code_Value = 'Charge' THEN cusd.Bucket_Value * curconv.Conversion_Factor
                                 WHEN cd.Code_Value = 'Determinant'
                                      AND uomconv.Conversion_Factor != 0 THEN cusd.Bucket_Value * uomconv.Conversion_Factor
                                 WHEN cd.Code_Value = 'Determinant'
                                      AND uomconv.Conversion_Factor = 0 THEN cusd.Bucket_Value
                                 ELSE 0
                            END) Bucket_Value
						,dsc.Code_Value                            
                  FROM
                        dbo.Cost_Usage_Site_Dtl cusd
                        INNER JOIN @Bucket_List bl
                              ON bl.Bucket_Master_Id = cusd.Bucket_Master_Id
                        INNER JOIN dbo.Code cd
                              ON cd.Code_Id = bl.Bucket_Type_cd
						INNER JOIN dbo.Code dsc
							ON dsc.Code_Id = cusd.Data_Source_Cd                              
                        INNER JOIN meta.Date_Dim sm
                              ON sm.Date_D = cusd.Service_Month
                        LEFT OUTER JOIN dbo.currency_Unit_Conversion curconv
                              ON curconv.Base_Unit_Id = cusd.currency_Unit_Id
                                 AND curconv.Conversion_Date = sm.DATE_D
                                 AND curconv.currency_Group_Id = @currency_Group_Id
                                 AND curconv.Converted_Unit_Id = @currency_Unit_Id
                        LEFT OUTER JOIN CbmS.dbo.Consumption_Unit_Conversion uomconv
                              ON uomconv.Base_Unit_Id = cusd.uom_Type_Id
                                 AND uomconv.Converted_Unit_Id = bl.uom_Type_Id
                  WHERE
                        cusd.Client_Hier_Id = @Client_Hier_Id
                        AND sm.Year_Num = @Report_Year
                  GROUP BY
                        bl.Bucket_Master_Id
                       ,bl.Bucket_Name
                       ,sm.Date_D
                       ,sm.Month_Num
                       ,cd.Code_Value
                       ,( CASE WHEN uomconv.Conversion_Factor = 0
                                    AND cd.Code_Value = 'Determinant' THEN cusd.uom_Type_Id
                               WHEN uomconv.Conversion_Factor != 0
                                    AND cd.Code_Value = 'Determinant' THEN bl.uom_Type_Id
                          END )  
                       ,dsc.Code_Value
  
  
      SELECT
            bl.Bucket_Name
           ,bl.Bucket_Master_Id
           ,bl.Bucket_Type_cd
           ,( CASE WHEN cusd.Bucket_Type = 'Charge' THEN cu.currency_Unit_Name
                   WHEN cusd.Bucket_Type = 'Determinant' THEN uom.Entity_Name
              END ) AS uom_Name
           ,MAX(CASE WHEN cusd.Month_Number = 1 THEN cusd.Bucket_Value
                END) AS Month1
           ,MAX(CASE WHEN cusd.Month_Number = 2 THEN cusd.Bucket_Value
                END) AS Month2
           ,MAX(CASE WHEN cusd.Month_Number = 3 THEN cusd.Bucket_Value
                END) AS Month3
           ,MAX(CASE WHEN cusd.Month_Number = 4 THEN cusd.Bucket_Value
                END) AS Month4
           ,MAX(CASE WHEN cusd.Month_Number = 5 THEN cusd.Bucket_Value
                END) AS Month5
           ,MAX(CASE WHEN cusd.Month_Number = 6 THEN cusd.Bucket_Value
                END) AS Month6
           ,MAX(CASE WHEN cusd.Month_Number = 7 THEN cusd.Bucket_Value
                END) AS Month7
           ,MAX(CASE WHEN cusd.Month_Number = 8 THEN cusd.Bucket_Value
                END) AS Month8
           ,MAX(CASE WHEN cusd.Month_Number = 9 THEN cusd.Bucket_Value
                END) AS Month9
           ,MAX(CASE WHEN cusd.Month_Number = 10 THEN cusd.Bucket_Value
                END) AS Month10
           ,MAX(CASE WHEN cusd.Month_Number = 11 THEN cusd.Bucket_Value
                END) AS Month11
           ,MAX(CASE WHEN cusd.Month_Number = 12 THEN cusd.Bucket_Value
                END) AS Month12
           ,SUM(cusd.Bucket_Value) AS Total
           ,MAX(CASE WHEN cusd.Month_Number = 1 THEN cusd.Data_Source
                END) AS Month1_Data_Source
           ,MAX(CASE WHEN cusd.Month_Number = 2 THEN cusd.Data_Source
                END) AS Month2_Data_Source
           ,MAX(CASE WHEN cusd.Month_Number = 3 THEN cusd.Data_Source
                END) AS Month3_Data_Source
           ,MAX(CASE WHEN cusd.Month_Number = 4 THEN cusd.Data_Source
                END) AS Month4_Data_Source
           ,MAX(CASE WHEN cusd.Month_Number = 5 THEN cusd.Data_Source
                END) AS Month5_Data_Source
           ,MAX(CASE WHEN cusd.Month_Number = 6 THEN cusd.Data_Source
                END) AS Month6_Data_Source
           ,MAX(CASE WHEN cusd.Month_Number = 7 THEN cusd.Data_Source
                END) AS Month7_Data_Source
           ,MAX(CASE WHEN cusd.Month_Number = 8 THEN cusd.Data_Source
                END) AS Month8_Data_Source
           ,MAX(CASE WHEN cusd.Month_Number = 9 THEN cusd.Data_Source
                END) AS Month9_Data_Source
           ,MAX(CASE WHEN cusd.Month_Number = 10 THEN cusd.Data_Source
                END) AS Month10_Data_Source
           ,MAX(CASE WHEN cusd.Month_Number = 11 THEN cusd.Data_Source
                END) AS Month11_Data_Source
           ,MAX(CASE WHEN cusd.Month_Number = 12 THEN cusd.Data_Source
                END) AS Month12_Data_Source           
           ,@Calendar_Year_Start_Month AS Start_Month
      FROM
            @Bucket_List bl
            LEFT OUTER JOIN @Cu_Data cusd
                  ON cusd.Bucket_Master_Id = bl.Bucket_Master_Id
            LEFT OUTER JOIN CbmS.dbo.currency_Unit cu
                  ON cu.currency_Unit_Id = @currency_Unit_Id
            LEFT OUTER JOIN CbmS.dbo.Entity uom
                  ON uom.Entity_Id = cusd.uom_Type_Id
      GROUP BY
            bl.Bucket_Name
           ,bl.Bucket_Master_Id
           ,bl.Bucket_Type_cd
           ,( CASE WHEN cusd.Bucket_Type = 'Charge' THEN cu.currency_Unit_Name
                   WHEN cusd.Bucket_Type = 'Determinant' THEN uom.Entity_Name
              END )
      ORDER BY
            bl.Bucket_Name  
  
END
;
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Site_Detailed_Summary_By_Client_Hier_Commodity_Year] TO [CBMSApplication]
GO
