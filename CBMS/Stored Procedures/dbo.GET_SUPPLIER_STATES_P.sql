SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE dbo.GET_SUPPLIER_STATES_P
	@vendorId int
AS
BEGIN

	SET NOCOUNT ON

	SELECT map.state_id,
		s.state_name
	FROM dbo.vendor_state_map map INNER JOIN dbo.state s ON s.state_id = map.state_id
	WHERE map.vendor_id = @vendorId

END
GO
GRANT EXECUTE ON  [dbo].[GET_SUPPLIER_STATES_P] TO [CBMSApplication]
GO
