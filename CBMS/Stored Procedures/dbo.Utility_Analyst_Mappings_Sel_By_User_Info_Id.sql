SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
		dbo.Utility_Analyst_Mappings_Sel_By_User_Info_Id
        
DESCRIPTION:
                                                  
                                                  
INPUT PARAMETERS:
	Name            DataType        Default    Description
------------------------------------------------------------
    @User_Info_Id	INT
    
      
OUTPUT PARAMETERS:
	Name            DataType        Default    Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	
	EXEC DBO.Utility_Analyst_Mappings_Sel_By_User_Info_Id 5150
	EXEC DBO.Utility_Analyst_Mappings_Sel_By_User_Info_Id 14832
	EXEC DBO.Utility_Analyst_Mappings_Sel_By_User_Info_Id 43984

SELECT * FROM dbo.Utility_Detail_Analyst_Map	

AUTHOR INITIALS:
    Initials    Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RR			2015-01-05	Created MAINT-3304 Move User to History CBMS enhancement
	          
******/

CREATE PROCEDURE [dbo].[Utility_Analyst_Mappings_Sel_By_User_Info_Id] ( @User_Info_Id AS INT )
AS 
BEGIN
                                    
               
      SET NOCOUNT ON
      SELECT
            con.COUNTRY_NAME
           ,st.state_name
           ,vndr.VENDOR_NAME
           ,gii.GROUP_NAME AS [QUEUE]
      FROM
            dbo.Utility_Detail_Analyst_Map udam
            INNER JOIN dbo.UTILITY_DETAIL ud
                  ON ud.UTILITY_DETAIL_ID = udam.Utility_Detail_ID
            INNER JOIN dbo.VENDOR vndr
                  ON vndr.VENDOR_ID = ud.VENDOR_ID
            INNER JOIN dbo.VENDOR_STATE_MAP vsm
                  ON vsm.VENDOR_ID = vndr.VENDOR_ID
            INNER JOIN dbo.STATE st
                  ON st.STATE_ID = vsm.STATE_ID
            INNER JOIN dbo.COUNTRY con
                  ON con.COUNTRY_ID = st.COUNTRY_ID
            INNER JOIN GROUP_INFO gii
                  ON gii.GROUP_INFO_ID = udam.Group_Info_ID
            INNER JOIN QUEUE q
                  ON q.QUEUE_ID = gii.QUEUE_ID
      WHERE
            udam.Analyst_ID = @User_Info_Id
                                                      
            --AND QUEUE_TYPE_ID = 34
      ORDER BY
            gii.GROUP_NAME
END

;
GO
GRANT EXECUTE ON  [dbo].[Utility_Analyst_Mappings_Sel_By_User_Info_Id] TO [CBMSApplication]
GO
