SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME:
		dbo.Cost_Usage_Transfer_Queue_Upd_By_Queue_Id
		
DESCRIPTION:
			To update status bit Is_Aggregated_To_Site/Is_Variance_Tested of Stage.Cost_Usage_Transfer_Queue
      
INPUT PARAMETERS:          
	NAME							DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------------------          
	@Is_Aggregated_To_Site			BIT			NULL
    @Is_Variance_Tested				BIT			NULL
    @Cost_Usage_Transfer_Queue_Id	INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------------------          

USAGE EXAMPLES:          
-------------------------------------------------------------------------  
	Select * from Stage.Cost_Usage_Transfer_Queue

Select * from Stage.Cost_Usage_Transfer_Queue WHERE Cost_Usage_Transfer_Queue_Id = 100001
BEGIN TRANSACTION
	EXEC dbo.Cost_Usage_Transfer_Queue_Upd_By_Queue_Id @Is_Aggregated_To_Site=1,@Is_Variance_Tested=1,@Cost_Usage_Transfer_Queue_Id=100001
	Select * from Stage.Cost_Usage_Transfer_Queue WHERE Cost_Usage_Transfer_Queue_Id = 100001
ROLLBACK TRANSACTION
	
     
AUTHOR INITIALS:          
	INITIALS	NAME          
------------------------------------------------------------          
	RR			Raghu Reddy

MODIFICATIONS
	INITIALS	DATE		MODIFICATION
-----------------------------------------------------------
	RR			2013-05-03	Created
*/

CREATE PROCEDURE dbo.Cost_Usage_Transfer_Queue_Upd_By_Queue_Id
      ( 
       @Is_Aggregated_To_Site BIT = NULL
      ,@Is_Variance_Tested BIT = NULL
      ,@Cost_Usage_Transfer_Queue_Id INT )
AS 
BEGIN
      SET NOCOUNT ON ;
		
      UPDATE
            que
      SET   
            que.Is_Aggregated_To_Site = ISNULL(@Is_Aggregated_To_Site, que.Is_Aggregated_To_Site)
           ,que.Is_Variance_Tested = ISNULL(@Is_Variance_Tested, que.Is_Variance_Tested)
      FROM
            Stage.Cost_Usage_Transfer_Queue que
      WHERE
            que.Cost_Usage_Transfer_Queue_Id = @Cost_Usage_Transfer_Queue_Id
      
END
;
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Transfer_Queue_Upd_By_Queue_Id] TO [CBMSApplication]
GO
