SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        dbo.RM_Client_Workflow_Sel                    
                      
Description:                      
        To get deal ticket workflows
                      
Input Parameters:                      
    Name							DataType        Default     Description                        
--------------------------------------------------------------------------------
	
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	  
                    
Usage Examples:                          
--------------------------------------------------------------------------------
	
		EXEC dbo.RM_Client_Workflow_Sel 235
		EXEC dbo.RM_Client_Workflow_Sel 235,1
		EXEC dbo.RM_Client_Workflow_Sel 11236
		SELECT * FROM dbo.RM_Client_Deal_Ticket_Work_Flow_Map ORDER BY Hedge_Type_Id
		                     
 Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy   
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			2018-09-12     Global Risk Management - Created 
                     
******/
CREATE PROCEDURE [dbo].[RM_Client_Workflow_Sel]
      ( 
       @Client_Id INT
      ,@RM_Client_Workflow_Id INT = NULL )
AS 
BEGIN
      SET NOCOUNT ON;
      
      DECLARE @Client_Client_Hier_Id INT
      
      SELECT
            @Client_Client_Hier_Id = Client_Hier_Id
      FROM
            Core.Client_Hier
      WHERE
            Client_Id = @Client_Id
            AND Sitegroup_Id = 0
      
      SELECT
            rcw.RM_Client_Workflow_Id
           ,chob.Country_Id
           ,con.COUNTRY_NAME
           ,chob.Commodity_Id
           ,com.Commodity_Name
           ,rcw.Hedge_Type_Id
           ,hdg.ENTITY_NAME AS Hedge_Type
           ,rcw.Workflow_Id
           ,wf.Workflow_Name
           ,wf.Workflow_Description
           ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Last_Updated_By
           ,rcw.Last_Change_Ts
      FROM
            dbo.RM_Client_Workflow rcw
            INNER JOIN dbo.Workflow wf
                  ON rcw.Workflow_Id = wf.Workflow_Id
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                  ON rcw.RM_Client_Hier_Onboard_Id = chob.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.COUNTRY con
                  ON chob.Country_Id = con.COUNTRY_ID
            INNER JOIN dbo.Commodity com
                  ON chob.Commodity_Id = com.Commodity_Id
            INNER JOIN dbo.ENTITY hdg
                  ON rcw.Hedge_Type_Id = hdg.ENTITY_ID
            INNER JOIN dbo.USER_INFO ui
                  ON rcw.Updated_User_Id = ui.USER_INFO_ID
      WHERE
            chob.Client_Hier_Id = @Client_Client_Hier_Id
            AND ( @RM_Client_Workflow_Id IS NULL
                  OR rcw.RM_Client_Workflow_Id = @RM_Client_Workflow_Id )
      
     
END;

GO
GRANT EXECUTE ON  [dbo].[RM_Client_Workflow_Sel] TO [CBMSApplication]
GO
