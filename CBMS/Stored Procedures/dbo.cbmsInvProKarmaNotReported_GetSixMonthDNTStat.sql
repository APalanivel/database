SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[cbmsInvProKarmaNotReported_GetSixMonthDNTStat]
	 ( @MyAccountId int )
AS
BEGIN
	set nocount on

	declare @start_date datetime
	set @start_date = dateadd(m, -6, convert(datetime, convert(varchar, month(getdate())) + '/1/' + convert(varchar, year(getdate()))))

	set nocount off

	   select month(b.batch_date) month_no
		, datename(mm, b.batch_date) month_name
		, count(distinct r.barcode) image_count 
	     from inv_prokarma_not_reported_batch b 
	     join inv_prokarma_not_reported r on r.inv_prokarma_not_reported_batch_id = b.inv_prokarma_not_reported_batch_id 
	    where b.batch_date >= @start_date
	      and r.remarks like '%DNT%'

	 group by month(b.batch_date) 
		, datename(mm, b.batch_date) 
	 order by month(b.batch_date) 

END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvProKarmaNotReported_GetSixMonthDNTStat] TO [CBMSApplication]
GO
