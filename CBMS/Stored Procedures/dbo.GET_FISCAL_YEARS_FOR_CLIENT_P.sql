SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.GET_FISCAL_YEARS_FOR_CLIENT_P
@userId varchar,
@sessionId varchar,
@reportDate varchar(12),
@clientId int,
@reportName varchar(50)


as	
	set nocount on
select distinct filter.INPUT_PARAMETER

from	REPORT_LIST list,
	RM_REPORT report,
	RM_REPORT_BATCH_LOG batch,
	RM_REPORT_FILTER_DATA filter

where	list.REPORT_NAME like @reportName AND
	list.REPORT_LIST_ID=report.REPORT_LIST_ID AND
	report.RM_REPORT_ID=batch.RM_REPORT_ID AND
	CONVERT(Varchar(12),batch.LOG_TIME, 101)=@reportDate AND
	batch.CLIENT_ID=@clientId AND
	batch.RM_REPORT_ID=filter.RM_REPORT_ID AND
	filter.INPUT_PARAMETER_TYPE_ID=( select ENTITY_ID
				  	 from	ENTITY
					 where  ENTITY_TYPE=503 AND
					  	ENTITY_NAME like 'Year'
					)

order by filter.INPUT_PARAMETER
GO
GRANT EXECUTE ON  [dbo].[GET_FISCAL_YEARS_FOR_CLIENT_P] TO [CBMSApplication]
GO
