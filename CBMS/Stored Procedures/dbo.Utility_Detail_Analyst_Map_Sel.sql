SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                              
 NAME: dbo.Utility_Detail_Analyst_Map_Sel                  
                               
 DESCRIPTION:                              
			Search of all utility Vendors in the system.               
                              
 INPUT PARAMETERS:                
                           
 Name                        DataType         Default       Description              
--------------------------------------------------------------------------------------------------------------- 
@Country_Id						INT				 NULL			           
@State_Id						INT				 NULL
@Commodity_Type_Id				INT				 NULL
@Analyst_Id						INT				 NULL
@Is_Analyst_Mapped				INT				 -1
@Vendor_Id						INT				 NULL
@Queue							INT				 NULL
@Start_Index					INT				 1
@End_Index						INT				 2147483647
@Total_Row_Count				INT				 0                
@QueueType					VARCHAR(25)			'InvoiceProcessingTeam'
                              
 OUTPUT PARAMETERS:                
                                 
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
                              
 USAGE EXAMPLES:                                  
---------------------------------------------------------------------------------------------------------------                                  


--MAPPED 0(no analyst mapped Vendors) 
 EXEC [dbo].[Utility_Detail_Analyst_Map_Sel]       
      @Country_Id=4     
     ,@Vendor_Id=NULL  
     ,@Queue =135 
     ,@Is_Analyst_Mapped=0
     ,@Analyst_Id= NULL
 
 
 
--MAPPED 1(analyst mapped Vendor for given analystid)
 EXEC [dbo].[Utility_Detail_Analyst_Map_Sel]       
      @Country_Id=4     
     ,@Vendor_Id=NULL  
     ,@Queue =135 
     ,@Is_Analyst_Mapped=1
     ,@Analyst_Id= 29574           
 
 
     
--MAPPED 1(analyst mapped Vendors but not  particular analyst as @Analyst_Id is given as  NULL, takes all analyst mapped Vendors)
 EXEC [dbo].[Utility_Detail_Analyst_Map_Sel]       
      @Country_Id=4     
     ,@Vendor_Id=19001  
     ,@Queue =NULL 
     ,@Is_Analyst_Mapped=1
     ,@Analyst_Id= NULL



--MAPPED -1(All Vendors irrespective of mapped or non mapped analyst,if analystid is given it takes that particular analyst mapped Vendors)
 EXEC [dbo].[Utility_Detail_Analyst_Map_Sel]       
      @Country_Id=4     
     ,@Vendor_Id=19001  
     ,@Queue =NULL 
     ,@Is_Analyst_Mapped=-1
     ,@Analyst_Id= 29574
     ,@Start_Index  = 1
     ,@End_Index = 100
     ,@Total_Row_Count = 100



--MAPPED -1(All Vendors irrespective of mapped or non mapped analyst,if analystid NULL it takes all the Vendors)      
 EXEC [dbo].[Utility_Detail_Analyst_Map_Sel]       
      @Country_Id=4     
     ,@Vendor_Id=19001  
     ,@Queue =NULL 
     ,@Is_Analyst_Mapped=-1
     ,@Analyst_Id= NULL
     ,@Start_Index  = 1
     ,@End_Index = 100
     ,@Total_Row_Count = 100  
------------------------------------------


--IF Vendor id is given and queue is not given then gets all that queue related Vendors for the given Country
 EXEC [dbo].[Utility_Detail_Analyst_Map_Sel]       
      @Country_Id=4     
     ,@Vendor_Id=19001  
     ,@Queue =NULL 
     ,@Is_Analyst_Mapped=-1
     ,@Analyst_Id= NULL
     ,@Start_Index  = 1
     ,@End_Index = 100
     ,@Total_Row_Count = 100   



--IF queue  is given and Vendor is not given then gets all that Vendors related records for the given Country
 EXEC [dbo].[Utility_Detail_Analyst_Map_Sel]       
      @Country_Id=4     
     ,@Vendor_Id=NULL  
     ,@Queue =135 
     ,@Is_Analyst_Mapped=-1
     ,@Analyst_Id= NULL
     ,@Start_Index  = 1
     ,@End_Index = 100
     ,@Total_Row_Count = 100   




--IF queue  is given and Vendor is not given then gets all that Vendors related records for the given Country AND State
 EXEC [dbo].[Utility_Detail_Analyst_Map_Sel]  
      @Country_Id=4 
     ,@State_Id=46     
     ,@Vendor_Id=NULL  
     ,@Queue =135 
     ,@Is_Analyst_Mapped=-1
     ,@Analyst_Id= NULL
     ,@Start_Index  = 1
     ,@End_Index = 100
     ,@Total_Row_Count = 100            

EXEC Utility_Detail_Analyst_Map_Sel @Queue = 133
              
EXEC Utility_Detail_Analyst_Map_Sel @Queue = 379
                             
 AUTHOR INITIALS:              
             
 Initials              Name              
---------------------------------------------------------------------------------------------------------------                            
 SP                    Sandeep Pigilam                
                               
 MODIFICATIONS:            
                
 Initials              Date             Modification            
---------------------------------------------------------------------------------------------------------------            
 SP                    2014-02-24			Created                      
                             
******/   
CREATE PROCEDURE dbo.Utility_Detail_Analyst_Map_Sel
      ( 
       @State_Id INT = NULL
      ,@Country_Id INT = NULL
      ,@Commodity_Type_Id INT = NULL
      ,@Analyst_Id INT = NULL
      ,@Is_Analyst_Mapped INT = -1
      ,@Vendor_Id INT = NULL
      ,@Queue INT = NULL
      ,@Start_Index INT = 1
      ,@End_Index INT = 2147483647
      ,@Total_Row_Count INT = 0
      ,@QueueType VARCHAR(25) = 'InvoiceProcessingTeam' )
AS 
BEGIN  
      SET NOCOUNT ON
      
      DECLARE @Group_List TABLE
            ( 
             Group_Info_Id INT
            ,Group_Name VARCHAR(200) )

      INSERT      INTO @Group_List
                  ( 
                   Group_Info_Id
                  ,Group_Name )
                  SELECT
                        gi.Group_Info_Id
                       ,gi.Group_Name
                  FROM
                        dbo.GROUP_INFO gi
                        INNER JOIN dbo.QUEUE q
                              ON q.queue_id = gi.QUEUE_ID
                        INNER JOIN dbo.ENTITY qt
                              ON qt.Entity_Id = q.QUEUE_TYPE_ID
                        INNER JOIN dbo.Codeset cs
                              ON cs.Codeset_Id = q.QUEUE_TYPE_ID
                  WHERE
                        ( @Queue IS NULL
                          OR gi.GROUP_INFO_ID = @Queue )
                        AND qt.Entity_Description = 'Invoice Source'
                        AND cs.Codeset_Name = @QueueType

      IF ( @Total_Row_Count = 0 ) 
            BEGIN
      
                  SELECT
                        @Total_Row_Count = COUNT(DISTINCT v.Vendor_Id)
                  FROM
                        dbo.Vendor v
                        INNER JOIN dbo.Entity vt
                              ON vt.Entity_Id = v.Vendor_Type_Id
                        INNER JOIN dbo.Vendor_State_Map stmap
                              ON stmap.Vendor_Id = v.Vendor_Id
                        INNER JOIN dbo.State st
                              ON st.State_id = stmap.State_id
                        INNER JOIN dbo.Country cou
                              ON cou.Country_Id = st.Country_Id
                        INNER JOIN dbo.Utility_Detail ud
                              ON ud.Vendor_Id = v.Vendor_Id
                        CROSS JOIN @Group_List gl
                        LEFT OUTER JOIN dbo.Utility_Detail_Analyst_Map map
                              ON map.Utility_Detail_id = ud.Utility_Detail_id
                                 AND map.Group_Info_ID = gl.Group_Info_Id
                        LEFT OUTER JOIN dbo.User_Info ui
                              ON ui.User_Info_id = map.Analyst_ID
                  WHERE
                        ( @Country_Id IS NULL
                          OR st.Country_Id = @Country_Id )
                        AND ( @State_Id IS NULL
                              OR st.State_ID = @State_Id )
                        AND ( ( @Is_Analyst_Mapped = -1
                                AND ( @Analyst_Id IS NULL
                                      OR map.Analyst_ID = @Analyst_Id ) )
                              OR ( @Is_Analyst_Mapped = 1
                                   AND ( map.Analyst_ID = @Analyst_Id
                                         OR ( map.Analyst_ID IS NOT NULL
                                              AND @Analyst_Id IS NULL ) ) )
                              OR ( @Is_Analyst_Mapped = 0
                                   AND map.Analyst_ID IS NULL
                                   AND @Analyst_ID IS NULL ) )
                        AND ( @Commodity_Type_Id IS NULL
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Vendor_COMMODITY_MAP cmap
                                          WHERE
                                                cmap.Vendor_Id = v.Vendor_Id
                                                AND cmap.COMMODITY_TYPE_ID = @Commodity_Type_Id ) )
                        AND ( @Vendor_Id IS NULL
                              OR v.Vendor_Id = @Vendor_Id )
      
            END;
      WITH  Cte_Utility_Detail_Analyst_Map_Sel
              AS ( SELECT TOP ( @End_Index )
                        v.Vendor_Id
                       ,v.Vendor_Name
                       ,map.Analyst_ID User_Info_id
                       ,ui.First_Name + SPACE(1) + ui.Last_Name username
                       ,LEFT(temp.Commodity_Name, LEN(temp.Commodity_Name) - 1) AS Commodity_Name
                       ,st.State_Name
                       ,st.State_Id
                       ,ud.Utility_Detail_Id
                       ,gl.Group_Info_Id
                       ,gl.Group_Name
                       ,cou.Country_Name
                       ,DENSE_RANK()OVER (ORDER BY v.Vendor_Name,st.State_ID) AS Row_Num
                   FROM
                        dbo.Vendor v
                        INNER JOIN dbo.Vendor_State_Map stmap
                              ON stmap.Vendor_Id = v.Vendor_Id
                        INNER JOIN dbo.State st
                              ON st.State_id = stmap.State_id
                        INNER JOIN dbo.Country cou
                              ON cou.Country_Id = st.Country_Id
                        INNER JOIN dbo.Utility_Detail ud
                              ON ud.Vendor_Id = v.Vendor_Id
                        CROSS JOIN @Group_List gl
                        LEFT OUTER JOIN dbo.Utility_Detail_Analyst_Map map
                              ON map.Utility_Detail_id = ud.Utility_Detail_id
                                 AND map.Group_Info_ID = gl.Group_Info_Id
                        LEFT OUTER JOIN dbo.User_Info ui
                              ON ui.User_Info_id = map.Analyst_ID
                        CROSS APPLY ( SELECT
                                          CAST(c.Commodity_Name AS VARCHAR(MAX)) + ','
                                      FROM
                                          dbo.Commodity c
                                          INNER JOIN dbo.Vendor_COMMODITY_MAP vc
                                                ON c.Commodity_Id = vc.COMMODITY_TYPE_ID
                                      WHERE
                                          vc.Vendor_Id = v.Vendor_Id
                                      ORDER BY
                                          ( CASE WHEN c.Commodity_Name = 'Electric Power' THEN 1
                                                 WHEN c.Commodity_Name = 'Natural Gas' THEN 2
                                                 ELSE 3
                                            END )
                                ,c.Commodity_Name
                        FOR
                                      XML PATH('') ) temp ( Commodity_Name )
                   WHERE
                        ( @Country_Id IS NULL
                          OR st.Country_Id = @Country_Id )
                        AND ( @State_Id IS NULL
                              OR st.State_ID = @State_Id )
                        AND ( ( @Is_Analyst_Mapped = -1
                                AND ( @Analyst_Id IS NULL
                                      OR map.Analyst_ID = @Analyst_Id ) )
                              OR ( @Is_Analyst_Mapped = 1
                                   AND ( map.Analyst_ID = @Analyst_Id
                                         OR ( map.Analyst_ID IS NOT NULL
                                              AND @Analyst_Id IS NULL ) ) )
                              OR ( @Is_Analyst_Mapped = 0
                                   AND map.Analyst_ID IS NULL
                                   AND @Analyst_ID IS NULL ) )
                        AND ( @Commodity_Type_Id IS NULL
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Vendor_COMMODITY_MAP cmap
                                          WHERE
                                                cmap.Vendor_Id = v.Vendor_Id
                                                AND cmap.COMMODITY_TYPE_ID = @Commodity_Type_Id ) )
                        AND ( @Vendor_Id IS NULL
                              OR v.Vendor_Id = @Vendor_Id )
                       )
            SELECT
                  Vendor_Id
                 ,Vendor_Name
                 ,User_Info_id
                 ,UserName
                 ,Commodity_Name
                 ,State_Name
                 ,State_ID
                 ,Utility_Detail_ID
                 ,Group_Info_Id
                 ,Group_Name
                 ,Country_Name
                 ,@Total_Row_Count AS Total_Row_Count
                 ,Row_Num
            FROM
                  Cte_Utility_Detail_Analyst_Map_Sel
            WHERE
                  Row_Num BETWEEN @Start_Index AND @End_Index
            ORDER BY
                  Row_Num
                  ,Group_Info_Id                               
   
END
;
GO
GRANT EXECUTE ON  [dbo].[Utility_Detail_Analyst_Map_Sel] TO [CBMSApplication]
GO
