SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_DELETE_ACCOUNT_TERM_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
	@rfp_id              int,
	@rfp_term_id         int,
	@account_group_id    int,
	@is_bid_group        bit
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- Delete entry for test
--EXEC dbo.SR_RFP_DELETE_ACCOUNT_TERM_P
--	@rfp_id =199,
--	@rfp_term_id = 4861,
--	@account_group_id = 3853,
--	@is_bid_group = 1

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_DELETE_ACCOUNT_TERM_P
	@rfp_id int,
	@rfp_term_id int,
	@account_group_id int,
	@is_bid_group bit
	AS	
	
SET NOCOUNT ON
	
	
	declare @sr_account_group_id int	
	if @is_bid_group = 1
	begin
		set @sr_account_group_id = @account_group_id
	end
	else
	begin
		select @sr_account_group_id = sr_rfp_account_id from sr_rfp_account(nolock) where account_id = @account_group_id and sr_rfp_id = @rfp_id and sr_rfp_bid_group_id is null
	end

	delete sr_rfp_term_product_map where sr_rfp_account_term_id = (select sr_rfp_account_term_id from sr_rfp_account_term where sr_rfp_term_id = @rfp_term_id 
	and sr_account_group_id = @sr_account_group_id and is_bid_group = @is_bid_group)

	delete sr_rfp_account_term where sr_rfp_term_id = @rfp_term_id 
	and sr_account_group_id = @sr_account_group_id and is_bid_group = @is_bid_group
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_DELETE_ACCOUNT_TERM_P] TO [CBMSApplication]
GO
