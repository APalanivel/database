SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 
 dbo.cbmsCuExceptionDetail_GetForCuInvoiceMissingData
 
 DESCRIPTION:   
 
 
 INPUT PARAMETERS:  
 Name							DataType				Default			Description  
-------------------------------------------------------------------------------------------    
 @MyAccountId					INT
 @cu_invoice_id					INT

 
 Name							DataType				Default			Description  
-------------------------------------------------------------------------------------------    

  USAGE EXAMPLES:  
-------------------------------------------------------------------------------------------    


	EXEC dbo.cbmsCuExceptionDetail_GetForCuInvoiceMissingData
    @MyAccountId = 49        -- int
    , @cu_invoice_id = 74432806    -- int
	


AUTHOR INITIALS:  
 Initials		Name  
-------------------------------------------------------------------------------------------    
 NR				Narayana Reddy

 MODIFICATIONS   
 Initials	Date		Modification  
-------------------------------------------------------------------------------------------    
 NR			2018-08-30	D20-60 - Added Header.
******/

CREATE PROCEDURE [dbo].[cbmsCuExceptionDetail_GetForCuInvoiceMissingData]
     (
         @MyAccountId INT
         , @cu_invoice_id INT
     )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @missing_data_type_id INT;
        DECLARE @mapping_type_id INT;



        SELECT
            @missing_data_type_id = ENTITY_ID
        FROM
            dbo.ENTITY WITH (NOLOCK)
        WHERE
            ENTITY_TYPE = 712
            AND ENTITY_NAME = 'Missing Data';




        SELECT
            ced.CU_EXCEPTION_DETAIL_ID
            , ced.CU_EXCEPTION_ID
            , eg.ENTITY_ID exception_group_type_id
            , eg.ENTITY_NAME exception_group_type
            , ced.EXCEPTION_TYPE_ID
            , et.EXCEPTION_TYPE
            , ced.EXCEPTION_STATUS_TYPE_ID
            , es.ENTITY_NAME exception_status_type
            , ced.OPENED_DATE
            , ced.IS_CLOSED
            , ced.CLOSED_REASON_TYPE_ID
            , cr.ENTITY_NAME closed_reason_type
            , ced.CLOSED_BY_ID
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME closed_by
            , ced.CLOSED_DATE
        FROM
            dbo.CU_EXCEPTION ce WITH (NOLOCK)
            JOIN dbo.CU_EXCEPTION_DETAIL ced WITH (NOLOCK)
                ON ced.CU_EXCEPTION_ID = ce.CU_EXCEPTION_ID
            LEFT OUTER JOIN dbo.CU_EXCEPTION_TYPE et WITH (NOLOCK)
                ON et.CU_EXCEPTION_TYPE_ID = ced.EXCEPTION_TYPE_ID
            LEFT OUTER JOIN dbo.ENTITY eg WITH (NOLOCK)
                ON eg.ENTITY_ID = et.EXCEPTION_GROUP_TYPE_ID
            LEFT OUTER JOIN dbo.ENTITY es WITH (NOLOCK)
                ON es.ENTITY_ID = ced.EXCEPTION_STATUS_TYPE_ID
            LEFT OUTER JOIN dbo.ENTITY cr WITH (NOLOCK)
                ON cr.ENTITY_ID = ced.CLOSED_REASON_TYPE_ID
            LEFT OUTER JOIN dbo.USER_INFO ui WITH (NOLOCK)
                ON ui.USER_INFO_ID = ced.CLOSED_BY_ID
        WHERE
            ce.CU_INVOICE_ID = @cu_invoice_id
            AND et.EXCEPTION_GROUP_TYPE_ID = @missing_data_type_id
        ORDER BY
            ced.IS_CLOSED
            , ced.OPENED_DATE DESC;

    END;

GO
GRANT EXECUTE ON  [dbo].[cbmsCuExceptionDetail_GetForCuInvoiceMissingData] TO [CBMSApplication]
GO
