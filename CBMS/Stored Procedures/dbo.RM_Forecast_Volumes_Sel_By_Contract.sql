SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********     
NAME:    
   [dbo].[RM_Forecast_Volumes_Sel_By_Contract]    

DESCRIPTION:    


INPUT PARAMETERS:    
Name							DataType		Default		Description    
-------------------------------------------------------------------------
 @Contract_Id									INT

OUTPUT PARAMETERS:    
	Name				DataType  Default Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------

SELECT MIN(Service_Month),MAX(Service_Month),Client_Hier_Id FROM dbo.RM_Client_Hier_Forecast_Volume WHERE Client_Hier_Id in(18551,18552,18553)
GROUP BY Client_Hier_Id

SELECT b.CONTRACT_START_DATE,b.CONTRACT_END_DATE,b.CONTRACT_ID FROM Core.Client_Hier_Account a 
	INNER JOIN dbo.CONTRACT b ON a.Supplier_Contract_ID = b.CONTRACT_ID 
	WHERE a.Client_Hier_Id in(18551,18552,18553)

EXEC [dbo].[RM_Forecast_Volumes_Sel_By_Contract]   162279


AUTHOR INITIALS:    
	Initials	Name    
------------------------------------------------------------    
	RR			Raghu Reddy


MODIFICATIONS     
	Initials	Date			Modification    
------------------------------------------------------------    
	RR			2018-09-30		SP Created

******/

CREATE PROCEDURE [dbo].[RM_Forecast_Volumes_Sel_By_Contract]
    (
        @Contract_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Max_Fill_Date DATE = DATEADD(
                                          mm, 72 - DATEPART(mm, GETDATE())
                                          , DATEADD(dd, -DATEPART(dd, GETDATE()) + 1, GETDATE()));



        DECLARE @Tbl_Forecast AS TABLE
              (
                  Client_Hier_Id INT
                  , Site_name VARCHAR(800)
                  , Account_Id INT
                  , Service_Month DATE
                  , Commodity_Id INT
                  , Forecast_Volume DECIMAL(28, 0)
                  , Volume_Source_Cd INT
                  , Volume_Source VARCHAR(25)
              );

        INSERT INTO @Tbl_Forecast
             (
                 Client_Hier_Id
                 , Site_name
                 , Account_Id
                 , Service_Month
                 , Commodity_Id
                 , Forecast_Volume
                 , Volume_Source_Cd
                 , Volume_Source
             )
        SELECT
            cha.Client_Hier_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
            , cha.Account_Id
            , chfv.Service_Month
            , chfv.Commodity_Id
            , avf.Forecast_Volume
            , avf.Volume_Source_Cd
            , cd.Code_Value AS Volume_Source
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account cha
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN Core.Client_Hier_Account supp
                ON cha.Meter_Id = supp.Meter_Id
            INNER JOIN dbo.CONTRACT con
                ON supp.Supplier_Contract_ID = con.CONTRACT_ID
            INNER JOIN Trade.RM_Client_Hier_Forecast_Volume chfv
                ON chfv.Client_Hier_Id = cha.Client_Hier_Id
                   AND  chfv.Commodity_Id = con.COMMODITY_TYPE_ID
                   AND  (   chfv.Service_Month >= DATEADD(
                                                      dd, -DATEPART(dd, con.CONTRACT_START_DATE) + 1
                                                      , con.CONTRACT_START_DATE)
                            OR  chfv.Service_Month >= con.CONTRACT_END_DATE)
            LEFT JOIN Trade.RM_Account_Forecast_Volume avf
                ON avf.Account_Id = cha.Account_Id
                   AND  avf.Client_Hier_Id = cha.Client_Hier_Id
                   AND  con.COMMODITY_TYPE_ID = avf.Commodity_Id
                   AND  avf.Service_Month = chfv.Service_Month
            LEFT JOIN dbo.Code cd
                ON avf.Volume_Source_Cd = cd.Code_Id
        WHERE
            cha.Account_Type = 'Utility'
            AND supp.Account_Type = 'Supplier'
            AND con.CONTRACT_ID = @Contract_Id;

        INSERT INTO @Tbl_Forecast
             (
                 Client_Hier_Id
                 , Site_name
                 , Account_Id
                 , Service_Month
                 , Commodity_Id
                 , Forecast_Volume
                 , Volume_Source_Cd
                 , Volume_Source
             )
        SELECT
            cha.Client_Hier_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
            , cha.Account_Id
            , dd.DATE_D AS Service_Month
            , cha.Commodity_Id
            , NULL AS Forecast_Volume
            , NULL AS Volume_Source_Cd
            , NULL AS Volume_Source
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account cha
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN Core.Client_Hier_Account supp
                ON cha.Meter_Id = supp.Meter_Id
            INNER JOIN dbo.CONTRACT con
                ON supp.Supplier_Contract_ID = con.CONTRACT_ID
            CROSS JOIN meta.DATE_DIM dd
        WHERE
            (   dd.DATE_D >= DATEADD(dd, -DATEPART(dd, con.CONTRACT_START_DATE) + 1, con.CONTRACT_START_DATE)
                AND (   dd.DATE_D <= @Max_Fill_Date
                        OR  dd.DATE_D <= con.CONTRACT_END_DATE))
            AND cha.Account_Type = 'Utility'
            AND supp.Account_Type = 'Supplier'
            AND con.CONTRACT_ID = @Contract_Id
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    @Tbl_Forecast tf
                               WHERE
                                    tf.Client_Hier_Id = cha.Client_Hier_Id
                                    AND tf.Account_Id = cha.Account_Id
                                    AND tf.Service_Month = dd.DATE_D);

        SELECT
            Client_Hier_Id
            , Site_name
            , Account_Id
            , Service_Month
            , Commodity_Id
            , Forecast_Volume
            , Volume_Source_Cd
            , Volume_Source
        FROM
            @Tbl_Forecast tf
        ORDER BY
            Client_Hier_Id
            , Account_Id
            , Service_Month;
    END;






GO
GRANT EXECUTE ON  [dbo].[RM_Forecast_Volumes_Sel_By_Contract] TO [CBMSApplication]
GO
