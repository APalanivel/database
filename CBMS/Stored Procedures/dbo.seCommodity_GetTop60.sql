SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   Procedure dbo.seCommodity_GetTop60
AS
BEGIN
	set nocount on
declare @Today datetime
set @Today = convert(varchar,month(getdate())) + '/' + convert(varchar,day(getdate())) + '/' + convert(varchar,year(getdate()))


	 select top 60 CommodityId
				, Symbol
				, Label
				, TradingMonth
				, ExpirationDate
	   from seCommodity
	  where isNull(ExpirationDate, @Today + 1) >= @Today
 order by TradingMonth asc

END
GO
GRANT EXECUTE ON  [dbo].[seCommodity_GetTop60] TO [CBMSApplication]
GO
