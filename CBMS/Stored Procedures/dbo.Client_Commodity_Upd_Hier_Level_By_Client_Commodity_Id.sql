SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
	dbo.Client_Commodity_Upd_Hier_Level_By_Client_Commodity_Id

DESCRIPTION:

Used to update the Core.Client_Commodity table by given commodity,hier level code and allow for de.
	
INPUT PARAMETERS:
	Name									DataType		Default		Description
------------------------------------------------------------------------------------
	@Client_Commodity_id					INT
	@Hier_Level_Cd							INT
	@Allow_For_DE							BIT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:

------------------------------------------------------------
select  * from core.client_commodity where Client_Id = 13764 AND SCope_Cd is null

begin tran
	select  * from core.client_commodity where client_commodity_id = 10307
	SELECT * FROM dbo.Client_Commodity_Variance_Test_Config ccv WHERE ccv.Client_Commodity_Id=10307
    EXEC dbo.Client_Commodity_Upd_Hier_Level_By_Client_Commodity_Id 
      @Client_Commodity_id = 10307
     ,@Hier_Level_Cd = 100273--100016
     ,@Allow_For_DE = 1
     ,@User_Info_Id = 49
	select  * from core.client_commodity where client_commodity_id = 10307
	SELECT * FROM dbo.Client_Commodity_Variance_Test_Config ccv WHERE ccv.Client_Commodity_Id=10307
rollback tran

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan G
	BCH			Balaraju
	SP			Sandeep Pigilam

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG        	2/4/2010	Created

	DMR		    9/10/2010	Modified for Quoted_Identifier
	BCH			2011-10-21	Add @Allow_For_DE input parameter and updated in to Core.Client_Commodity table
	BCH			2012-02-08  Added Last_Change_Ts as getdate() in update clause
	BCH		    2013-06-10	Enhancement-40, Added Variance_Effective_Dt, Variance_Effective_Dt_Updated_User_Id, Variance_Effective_Dt_Last_Change_Ts input parameters
	SP			2016-11-09	Variance Enhancement Phase II VTE-15,Added Insert for Client_Commodity_Variance_Test_Config table.
							removed columns @Variance_Effective_Dt,@Variance_Effective_Dt_Updated_User_Id,@Variance_Effective_Dt_Last_Change_Ts from Client_Commodity.
	
******/ 
CREATE PROCEDURE [dbo].[Client_Commodity_Upd_Hier_Level_By_Client_Commodity_Id]
      ( 
       @Client_Commodity_id INT
      ,@Hier_Level_Cd INT
      ,@Allow_For_DE BIT
      ,@User_Info_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;

      DECLARE
            @Is_Hier_Level_Or_Allow_For_DE_Modified BIT = 0
           ,@Is_Variance_Effective_Dt_Modified BIT = 0
           ,@Hier_Level VARCHAR(30)
           ,@Variance_Start_Date DATE;
           
      SELECT
            @Variance_Start_Date = ap.App_Config_Value
      FROM
            dbo.App_Config ap
      WHERE
            ap.App_Config_Cd = 'Variance_Start_Date'
                 
      SELECT
            @Hier_Level = Code_Value
      FROM
            dbo.Code
      WHERE
            Code_Id = @Hier_Level_Cd;

      SELECT
            @Is_Hier_Level_Or_Allow_For_DE_Modified = CASE WHEN ( Hier_Level_Cd <> @Hier_Level_Cd
                                                                  OR Allow_for_DE <> @Allow_For_DE ) THEN 1
                                                           ELSE 0
                                                      END
      FROM
            Core.Client_Commodity cc
            LEFT JOIN dbo.Code c
                  ON cc.Hier_Level_Cd = c.Code_Id
      WHERE
            Client_Commodity_Id = @Client_Commodity_id;

      

                   
      MERGE dbo.Client_Commodity_Variance_Test_Config AS tgt
            USING 
                  ( SELECT
                        cc.Client_Commodity_Id
                       ,@Variance_Start_Date AS Start_Dt
                       ,NULL End_Dt
                       ,vt.Code_Id AS Variance_Test_Type_Cd
                       ,GETDATE() AS Created_Ts
                       ,@User_Info_Id AS Created_User_Id
                       ,@User_Info_Id AS Updated_User_Id
                       ,GETDATE() AS Last_Change_Ts
                    FROM
                        Core.Client_Commodity cc
                        INNER JOIN dbo.Code hl
                              ON hl.Code_Id = cc.Hier_Level_Cd
                        INNER JOIN dbo.Code csc
                              ON csc.Code_Id = cc.Commodity_Service_Cd
                        INNER JOIN dbo.Codeset cs
                              ON cs.Codeset_Name = 'VarianceTestType'
                        INNER JOIN dbo.Code vt
                              ON vt.Codeset_Id = cs.Codeset_Id
                                 AND vt.Code_Value = 'Full Variance Testing'
                    WHERE
                        cc.Client_Commodity_Id = @Client_Commodity_id
                        AND csc.Code_Value = 'Invoice' ) AS src
            ON src.Client_Commodity_Id = tgt.Client_Commodity_Id
            WHEN NOT MATCHED AND @Hier_Level = 'Account'
                  THEN
            INSERT
                        ( 
                         Client_Commodity_Id
                        ,Start_Dt
                        ,End_Dt
                        ,Variance_Test_Type_Cd
                        ,Created_Ts
                        ,Created_User_Id
                        ,Updated_User_Id
                        ,Last_Change_Ts )
                    VALUES
                        ( 
                         src.Client_Commodity_Id
                        ,src.Start_Dt
                        ,src.End_Dt
                        ,src.Variance_Test_Type_Cd
                        ,src.Created_Ts
                        ,src.Created_User_Id
                        ,src.Updated_User_Id
                        ,src.Last_Change_Ts )
            WHEN MATCHED AND @Hier_Level = 'Site'
                  THEN
            DELETE;

END;


;
GO



GRANT EXECUTE ON  [dbo].[Client_Commodity_Upd_Hier_Level_By_Client_Commodity_Id] TO [CBMSApplication]
GO
