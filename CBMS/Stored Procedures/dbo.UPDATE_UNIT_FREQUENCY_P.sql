SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.UPDATE_UNIT_FREQUENCY_P
	@lps_unit_type_id INT,
	@lps_frequency_type_id INT,
	@month_identifier DATETIME,
	@contract_id INT
AS
BEGIN

	SET NOCOUNT ON

	/*
	UPDATE load_profile_specification 
		SET lps_unit_type_id = @lps_unit_type_id,
			lps_frequency_type_id= @lps_frequency_type_id 
	WHERE month_identifier = @month_identifier 
		AND contract_id = @contract_id 
		AND lps_type_id =( SELECT entity_id FROM entity WHERE entity_type = 142 AND entity_name = 'Baseload')
	*/

	UPDATE dbo.load_profile_specification 
		SET lps_unit_type_id = @lps_unit_type_id,
			lps_frequency_type_id= @lps_frequency_type_id
	FROM Entity e 
	WHERE lps_type_id = e.entity_id
		AND e.entity_name = 'Baseload' AND e.entity_type = 142
		AND month_identifier = @month_identifier 
		AND contract_id = @contract_id

END
GO
GRANT EXECUTE ON  [dbo].[UPDATE_UNIT_FREQUENCY_P] TO [CBMSApplication]
GO
