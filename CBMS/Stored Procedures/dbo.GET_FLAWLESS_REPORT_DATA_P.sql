SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	cbms_prod.dbo.GET_FLAWLESS_REPORT_DATA_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId			VARCHAR,
	@sessionId		VARCHAR,
	@reportStatusId INT,
	@costToSummit	INT,
	@flawless		INT,
	@fromDate		VARCHAR(12),
	@toDate			VARCHAR(12)         	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	GET_FLAWLESS_REPORT_DATA_P 49, 1, 1, 1 , 1, '1/1/2029','1/1/2029'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hari

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	SKA		23 JUL 2009	REMOVED DIVISION VIEW AND ADDED SITEGROUP

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[GET_FLAWLESS_REPORT_DATA_P] 
@userId varchar,
@sessionId varchar,
@reportStatusId int,
@costToSummit int,
@flawless int,
@fromDate Varchar(12),
@toDate Varchar(12)

AS

BEGIN

	SET NOCOUNT ON

	if  @reportStatusId=1 

		select	
		   CONVERT (Varchar(12), MAX(dealtransaction.DEAL_TRANSACTION_DATE), 101)  DEAL_TRANSACTION_DATE,
			deal.RM_DEAL_TICKET_ID,
			CONVERT (Varchar(12), deal.HEDGE_START_MONTH, 113) HEDGE_START_MONTH,
			CONVERT (Varchar(12), deal.HEDGE_END_MONTH, 113) HEDGE_END_MONTH,
			isnull(client.CLIENT_NAME,'') CLIENT_NAME,	
			isnull(site.SITE_NAME,'0') SITE_NAME,
			isnull(sg.Sitegroup_Name,'0') DIVISION_NAME,	
			CASE rflaw.IS_MISTAKES_DETECTED 	
				WHEN 0 THEN 'N'
				WHEN 1 THEN 'Y'
				ELSE ''
				END AS MISTAKES_DETECTED,	
			CAST(rflaw.COST_TO_SUMMIT as decimal(20,2) ) COST_TO_SUMMIT,
			CASE  rcounter.COUNTERPARTY_NAME 
				  WHEN isnull(rcounter.COUNTERPARTY_NAME,'') THEN rcounter.COUNTERPARTY_NAME 
				  ELSE isnull(vendor.VENDOR_NAME,'')
				  END  COUNTERPARTY_NAME		
		from	RM_FLAWLESS_EXECUTION rflaw
			INNER JOIN RM_DEAL_TICKET deal
				ON deal.RM_DEAL_TICKET_ID=rflaw.RM_DEAL_TICKET_ID
			INNER JOIN RM_DEAL_TICKET_TRANSACTION_STATUS dealtransaction
				ON deal.RM_DEAL_TICKET_ID=dealtransaction.RM_DEAL_TICKET_ID
			INNER JOIN CLIENT client
				ON deal.CLIENT_ID=client.CLIENT_ID
			LEFT OUTER JOIN RM_COUNTERPARTY rcounter
				ON deal.RM_COUNTERPARTY_ID=rcounter.RM_COUNTERPARTY_ID
			LEFT OUTER JOIN SITE site
				ON deal.SITE_ID = site.SITE_ID
			LEFT OUTER JOIN Sitegroup sg 
				ON deal.DIVISION_ID = sg.Sitegroup_Id
			LEFT OUTER JOIN VENDOR vendor
				ON deal.VENDOR_ID = vendor.VENDOR_ID
		GROUP BY 
		    deal.RM_DEAL_TICKET_ID
		   ,deal.HEDGE_START_MONTH
		   ,deal.HEDGE_END_MONTH
		   ,client.CLIENT_NAME
		   ,site.SITE_NAME
		   ,sg.Sitegroup_Name
		   ,rcounter.COUNTERPARTY_NAME
		   ,rflaw.IS_MISTAKES_DETECTED
		   ,rflaw.COST_TO_SUMMIT
		   ,vendor.VENDOR_NAME

		UNION	

		select	isnull(CONVERT (Varchar(12), MAX(dealtransaction.DEAL_TRANSACTION_DATE), 101),'')  DEAL_TRANSACTION_DATE,
			deal.RM_DEAL_TICKET_ID,
			CONVERT (Varchar(12), deal.HEDGE_START_MONTH, 113) HEDGE_START_MONTH,
			CONVERT (Varchar(12), deal.HEDGE_END_MONTH, 113) HEDGE_END_MONTH,
			isnull(client.CLIENT_NAME,'') CLIENT_NAME,	
			isnull(site.SITE_NAME,'0') SITE_NAME,
			isnull(sg.Sitegroup_Name,'0') DIVISION_NAME,	
 			'Y' MISTAKES_DETECTED,	
			CAST(0.0 as decimal(20,2) ) COST_TO_SUMMIT,
			CASE  rcounter.COUNTERPARTY_NAME 
				  WHEN isnull(rcounter.COUNTERPARTY_NAME,'') THEN rcounter.COUNTERPARTY_NAME 
				  ELSE isnull(vendor.VENDOR_NAME,'')
				  END  COUNTERPARTY_NAME		
		from RM_DEAL_TICKET_TRANSACTION_STATUS dealtransaction
			INNER JOIN RM_DEAL_TICKET deal
				ON deal.RM_DEAL_TICKET_ID=dealtransaction.RM_DEAL_TICKET_ID
			LEFT OUTER JOIN CLIENT client
				ON deal.CLIENT_ID = client.CLIENT_ID
			LEFT OUTER JOIN RM_COUNTERPARTY rcounter
				ON deal.RM_COUNTERPARTY_ID = rcounter.RM_COUNTERPARTY_ID
			LEFT OUTER JOIN SITE site
				ON deal.SITE_ID = site.SITE_ID
			LEFT OUTER JOIN Sitegroup sg 
				ON deal.DIVISION_ID=sg.Sitegroup_Id
			LEFT OUTER JOIN VENDOR vendor
				ON deal.VENDOR_ID=vendor.VENDOR_ID
		where deal.RM_DEAL_TICKET_ID NOT IN(select distinct RM_DEAL_TICKET_ID from RM_FLAWLESS_EXECUTION)
		GROUP BY 
		    deal.RM_DEAL_TICKET_ID
		   ,deal.HEDGE_START_MONTH
		   ,deal.HEDGE_END_MONTH
		   ,client.CLIENT_NAME
		   ,site.SITE_NAME
		   ,sg.Sitegroup_Name
		   ,rcounter.COUNTERPARTY_NAME
		   ,vendor.VENDOR_NAME
  	   order by  DEAL_TRANSACTION_DATE desc
  	            ,CLIENT_NAME


	ELSE IF @reportStatusId=2 

		select	CONVERT (Varchar(12), MAX(dealtransaction.DEAL_TRANSACTION_DATE), 101)  DEAL_TRANSACTION_DATE,
			deal.RM_DEAL_TICKET_ID,
			CONVERT (Varchar(12), deal.HEDGE_START_MONTH, 113) HEDGE_START_MONTH,
			CONVERT (Varchar(12), deal.HEDGE_END_MONTH, 113) HEDGE_END_MONTH,
			isnull(client.CLIENT_NAME,'') CLIENT_NAME,	
			isnull(site.SITE_NAME,'0') SITE_NAME,
			isnull(sg.Sitegroup_Name,'0') DIVISION_NAME,	
			CASE rflaw.IS_MISTAKES_DETECTED 	
				WHEN 0 THEN 'N'
				WHEN 1 THEN 'Y'
				ELSE ''
				END AS MISTAKES_DETECTED,	
			CAST(rflaw.COST_TO_SUMMIT as decimal(20,2) ) COST_TO_SUMMIT,
			CASE  rcounter.COUNTERPARTY_NAME 

				  WHEN isnull(rcounter.COUNTERPARTY_NAME,'') THEN rcounter.COUNTERPARTY_NAME 
				  ELSE isnull(vendor.VENDOR_NAME,'')
				  END  COUNTERPARTY_NAME		
		from	RM_FLAWLESS_EXECUTION rflaw
			INNER JOIN RM_DEAL_TICKET deal
				ON deal.RM_DEAL_TICKET_ID=rflaw.RM_DEAL_TICKET_ID
			INNER JOIN RM_DEAL_TICKET_TRANSACTION_STATUS dealtransaction
				ON deal.RM_DEAL_TICKET_ID=dealtransaction.RM_DEAL_TICKET_ID
			LEFT OUTER JOIN CLIENT client
				ON deal.CLIENT_ID=client.CLIENT_ID
			LEFT OUTER JOIN RM_COUNTERPARTY rcounter
				ON deal.RM_COUNTERPARTY_ID=rcounter.RM_COUNTERPARTY_ID
			LEFT OUTER JOIN SITE site
				ON deal.SITE_ID = site.SITE_ID
			LEFT OUTER JOIN Sitegroup sg
				ON deal.DIVISION_ID=sg.Sitegroup_Id
			LEFT OUTER JOIN VENDOR vendoR
				ON deal.VENDOR_ID=vendor.VENDOR_ID
		where	rflaw.COST_TO_SUMMIT<=0	
		GROUP BY deal.RM_DEAL_TICKET_ID,deal.HEDGE_START_MONTH,deal.HEDGE_END_MONTH,client.CLIENT_NAME,site.SITE_NAME,
			sg.Sitegroup_Name,rcounter.COUNTERPARTY_NAME,rflaw.IS_MISTAKES_DETECTED,rflaw.COST_TO_SUMMIT,vendor.VENDOR_NAME

		UNION	

		select	isnull(CONVERT (Varchar(12), MAX(dealtransaction.DEAL_TRANSACTION_DATE), 101),'')  DEAL_TRANSACTION_DATE,
			deal.RM_DEAL_TICKET_ID,
			CONVERT (Varchar(12), deal.HEDGE_START_MONTH, 113) HEDGE_START_MONTH,
			CONVERT (Varchar(12), deal.HEDGE_END_MONTH, 113) HEDGE_END_MONTH,
			isnull(client.CLIENT_NAME,'') CLIENT_NAME,	
			isnull(site.SITE_NAME,'0') SITE_NAME,
			isnull(sg.Sitegroup_Name,'0') DIVISION_NAME,	
 			'Y' MISTAKES_DETECTED,	
			CAST(0.0 as decimal(20,2) ) COST_TO_SUMMIT,
			CASE  rcounter.COUNTERPARTY_NAME 
				  WHEN isnull(rcounter.COUNTERPARTY_NAME,'') THEN rcounter.COUNTERPARTY_NAME 
				  ELSE isnull(vendor.VENDOR_NAME,'')
				  END  COUNTERPARTY_NAME		
		
		from RM_DEAL_TICKET_TRANSACTION_STATUS dealtransaction
			INNER JOIN RM_DEAL_TICKET deal
				ON deal.RM_DEAL_TICKET_ID=dealtransaction.RM_DEAL_TICKET_ID
			LEFT OUTER JOIN CLIENT client
				ON deal.CLIENT_ID=client.CLIENT_ID
			LEFT OUTER JOIN RM_COUNTERPARTY rcounter
				ON deal.RM_COUNTERPARTY_ID=rcounter.RM_COUNTERPARTY_ID
			LEFT OUTER JOIN SITE site
				ON deal.SITE_ID=site.SITE_ID
			LEFT OUTER JOIN Sitegroup sg
				ON deal.DIVISION_ID=sg.Sitegroup_Id
			LEFT OUTER JOIN VENDOR vendor
				ON deal.VENDOR_ID=vendor.VENDOR_ID
		where deal.RM_DEAL_TICKET_ID NOT IN(select distinct RM_DEAL_TICKET_ID from RM_FLAWLESS_EXECUTION)
		GROUP BY deal.RM_DEAL_TICKET_ID,deal.HEDGE_START_MONTH,deal.HEDGE_END_MONTH,client.CLIENT_NAME,site.SITE_NAME,
			sg.Sitegroup_Name,rcounter.COUNTERPARTY_NAME,vendor.VENDOR_NAME
		order by DEAL_TRANSACTION_DATE desc,CLIENT_NAME


	ELSE IF @reportStatusId=3 

		select	CONVERT (Varchar(12), MAX(dealtransaction.DEAL_TRANSACTION_DATE), 101)  DEAL_TRANSACTION_DATE,
			deal.RM_DEAL_TICKET_ID,
			CONVERT (Varchar(12), deal.HEDGE_START_MONTH, 113) HEDGE_START_MONTH,
			CONVERT (Varchar(12), deal.HEDGE_END_MONTH, 113) HEDGE_END_MONTH,
			isnull(client.CLIENT_NAME,'') CLIENT_NAME,	
			isnull(site.SITE_NAME,'0') SITE_NAME,
			isnull(sg.Sitegroup_Name,'0') DIVISION_NAME,	
			CASE rflaw.IS_MISTAKES_DETECTED 	
				WHEN 0 THEN 'N'
				WHEN 1 THEN 'Y'
				ELSE ''
				END AS MISTAKES_DETECTED,	
			CAST(rflaw.COST_TO_SUMMIT as decimal(20,2) ) COST_TO_SUMMIT,
			CASE  rcounter.COUNTERPARTY_NAME 
				  WHEN isnull(rcounter.COUNTERPARTY_NAME,'') THEN rcounter.COUNTERPARTY_NAME 
				  ELSE isnull(vendor.VENDOR_NAME,'')
				  END  COUNTERPARTY_NAME		
		from	RM_FLAWLESS_EXECUTION rflaw
			INNER JOIN RM_DEAL_TICKET deal
				ON deal.RM_DEAL_TICKET_ID=rflaw.RM_DEAL_TICKET_ID
			INNER JOIN RM_DEAL_TICKET_TRANSACTION_STATUS dealtransaction
				ON deal.RM_DEAL_TICKET_ID=dealtransaction.RM_DEAL_TICKET_ID
			LEFT OUTER JOIN CLIENT client
				ON deal.CLIENT_ID=client.CLIENT_ID
			LEFT OUTER JOIN RM_COUNTERPARTY rcounter
				ON deal.RM_COUNTERPARTY_ID=rcounter.RM_COUNTERPARTY_ID
			LEFT OUTER JOIN SITE site
				ON deal.SITE_ID=site.SITE_ID
			LEFT OUTER JOIN Sitegroup sg
				ON deal.DIVISION_ID=sg.Sitegroup_Id
			LEFT OUTER JOIN VENDOR vendor
				ON deal.VENDOR_ID=vendor.VENDOR_ID
		
		where rflaw.COST_TO_SUMMIT>0	
		GROUP BY deal.RM_DEAL_TICKET_ID,deal.HEDGE_START_MONTH,deal.HEDGE_END_MONTH,client.CLIENT_NAME,site.SITE_NAME,
			sg.Sitegroup_Name,rcounter.COUNTERPARTY_NAME,rflaw.IS_MISTAKES_DETECTED,rflaw.COST_TO_SUMMIT,vendor.VENDOR_NAME
		order by DEAL_TRANSACTION_DATE desc,CLIENT_NAME


	ELSE IF @reportStatusId=4 

		select	CONVERT (Varchar(12), MAX(dealtransaction.DEAL_TRANSACTION_DATE), 101)  DEAL_TRANSACTION_DATE,
			deal.RM_DEAL_TICKET_ID,
			CONVERT (Varchar(12), deal.HEDGE_START_MONTH, 113) HEDGE_START_MONTH,
			CONVERT (Varchar(12), deal.HEDGE_END_MONTH, 113) HEDGE_END_MONTH,
			isnull(client.CLIENT_NAME,'') CLIENT_NAME,	
			isnull(site.SITE_NAME,'0') SITE_NAME,
			isnull(sg.Sitegroup_Name,'0') DIVISION_NAME,	
			CASE rflaw.IS_MISTAKES_DETECTED 	
				WHEN 0 THEN 'N'
				WHEN 1 THEN 'Y'
				ELSE ''
				END AS MISTAKES_DETECTED,	
			CAST(rflaw.COST_TO_SUMMIT as decimal(20,2) ) COST_TO_SUMMIT,
			CASE  rcounter.COUNTERPARTY_NAME 
				  WHEN isnull(rcounter.COUNTERPARTY_NAME,'') THEN rcounter.COUNTERPARTY_NAME 
				  ELSE isnull(vendor.VENDOR_NAME,'')
				  END  COUNTERPARTY_NAME		
		from RM_FLAWLESS_EXECUTION rflaw
			INNER JOIN RM_DEAL_TICKET deal
				ON deal.RM_DEAL_TICKET_ID=rflaw.RM_DEAL_TICKET_ID	
			INNER JOIN RM_DEAL_TICKET_TRANSACTION_STATUS dealtransaction
				ON deal.RM_DEAL_TICKET_ID=dealtransaction.RM_DEAL_TICKET_ID
			LEFT OUTER JOIN CLIENT client
				ON deal.CLIENT_ID=client.CLIENT_ID
			LEFT OUTER JOIN RM_COUNTERPARTY rcounter
				ON deal.RM_COUNTERPARTY_ID=rcounter.RM_COUNTERPARTY_ID
			LEFT OUTER JOIN SITE site
				ON deal.SITE_ID=site.SITE_ID
			LEFT OUTER JOIN Sitegroup sg
				ON deal.DIVISION_ID=sg.Sitegroup_Id
			LEFT OUTER JOIN VENDOR vendor
				ON deal.VENDOR_ID=vendor.VENDOR_ID
		where rflaw.IS_MISTAKES_DETECTED=@flawless
		GROUP BY deal.RM_DEAL_TICKET_ID,deal.HEDGE_START_MONTH,deal.HEDGE_END_MONTH,client.CLIENT_NAME,site.SITE_NAME,
			sg.Sitegroup_Name,rcounter.COUNTERPARTY_NAME,rflaw.IS_MISTAKES_DETECTED,rflaw.COST_TO_SUMMIT,vendor.VENDOR_NAME

		UNION	

		select	isnull(CONVERT (Varchar(12), MAX(dealtransaction.DEAL_TRANSACTION_DATE), 101),'')  DEAL_TRANSACTION_DATE,
			deal.RM_DEAL_TICKET_ID,
			CONVERT (Varchar(12), deal.HEDGE_START_MONTH, 113) HEDGE_START_MONTH,
			CONVERT (Varchar(12), deal.HEDGE_END_MONTH, 113) HEDGE_END_MONTH,
			isnull(client.CLIENT_NAME,'') CLIENT_NAME,	
			isnull(site.SITE_NAME,'0') SITE_NAME,
			isnull(sg.Sitegroup_Name,'0') DIVISION_NAME,	
 			'Y' MISTAKES_DETECTED,	
			CAST(0.0 as decimal(20,2) ) COST_TO_SUMMIT,
			CASE  rcounter.COUNTERPARTY_NAME 
				  WHEN isnull(rcounter.COUNTERPARTY_NAME,'') THEN rcounter.COUNTERPARTY_NAME 
				  ELSE isnull(vendor.VENDOR_NAME,'')
				  END  COUNTERPARTY_NAME		
		from RM_DEAL_TICKET_TRANSACTION_STATUS dealtransaction
			INNER JOIN RM_DEAL_TICKET deal
				ON deal.RM_DEAL_TICKET_ID=dealtransaction.RM_DEAL_TICKET_ID
			LEFT OUTER JOIN CLIENT client
				ON deal.CLIENT_ID = client.CLIENT_ID
			LEFT OUTER JOIN RM_COUNTERPARTY rcounter
				ON deal.RM_COUNTERPARTY_ID=rcounter.RM_COUNTERPARTY_ID
			LEFT OUTER JOIN SITE site
				ON deal.SITE_ID=site.SITE_ID
			LEFT OUTER JOIN Sitegroup sg
				ON deal.DIVISION_ID=sg.Sitegroup_Id
			LEFT OUTER JOIN VENDOR vendor
				ON deal.VENDOR_ID=vendor.VENDOR_ID
		where deal.RM_DEAL_TICKET_ID NOT IN(select distinct RM_DEAL_TICKET_ID from RM_FLAWLESS_EXECUTION)
		GROUP BY deal.RM_DEAL_TICKET_ID,deal.HEDGE_START_MONTH,deal.HEDGE_END_MONTH,client.CLIENT_NAME,site.SITE_NAME,
			sg.Sitegroup_Name,rcounter.COUNTERPARTY_NAME,vendor.VENDOR_NAME
		order by DEAL_TRANSACTION_DATE desc,CLIENT_NAME


	ELSE IF @reportStatusId=5 

		select	CONVERT (Varchar(12), MAX(dealtransaction.DEAL_TRANSACTION_DATE), 101)  DEAL_TRANSACTION_DATE,
			deal.RM_DEAL_TICKET_ID,
			CONVERT (Varchar(12), deal.HEDGE_START_MONTH, 113) HEDGE_START_MONTH,
			CONVERT (Varchar(12), deal.HEDGE_END_MONTH, 113) HEDGE_END_MONTH,
			isnull(client.CLIENT_NAME,'') CLIENT_NAME,	
			isnull(site.SITE_NAME,'0') SITE_NAME,
			isnull(sg.Sitegroup_Name,'0') DIVISION_NAME,	
			CASE rflaw.IS_MISTAKES_DETECTED 	
				WHEN 0 THEN 'N'
				WHEN 1 THEN 'Y'
				ELSE ''
				END AS MISTAKES_DETECTED,	
			CAST(rflaw.COST_TO_SUMMIT as decimal(20,2) ) COST_TO_SUMMIT,

			CASE  rcounter.COUNTERPARTY_NAME 
				  WHEN isnull(rcounter.COUNTERPARTY_NAME,'') THEN rcounter.COUNTERPARTY_NAME 
				  ELSE isnull(vendor.VENDOR_NAME,'')
				  END  COUNTERPARTY_NAME		
		from RM_FLAWLESS_EXECUTION rflaw
			INNER JOIN RM_DEAL_TICKET deal
				ON deal.RM_DEAL_TICKET_ID = rflaw.RM_DEAL_TICKET_ID
			INNER JOIN RM_DEAL_TICKET_TRANSACTION_STATUS dealtransaction
				ON deal.RM_DEAL_TICKET_ID=dealtransaction.RM_DEAL_TICKET_ID
			LEFT OUTER JOIN CLIENT client
				ON deal.CLIENT_ID=client.CLIENT_ID
			LEFT OUTER JOIN RM_COUNTERPARTY rcounter
				ON deal.RM_COUNTERPARTY_ID=rcounter.RM_COUNTERPARTY_ID
			LEFT OUTER JOIN SITE site
				ON deal.SITE_ID = site.SITE_ID
			LEFT OUTER JOIN Sitegroup sg
				ON deal.DIVISION_ID=sg.Sitegroup_Id
			LEFT OUTER JOIN VENDOR vendor
				ON deal.VENDOR_ID=vendor.VENDOR_ID
		where rflaw.IS_MISTAKES_DETECTED=@flawless
		GROUP BY deal.RM_DEAL_TICKET_ID,deal.HEDGE_START_MONTH,deal.HEDGE_END_MONTH,client.CLIENT_NAME,site.SITE_NAME,
			sg.Sitegroup_Name,rcounter.COUNTERPARTY_NAME,rflaw.IS_MISTAKES_DETECTED,rflaw.COST_TO_SUMMIT,vendor.VENDOR_NAME
		order by DEAL_TRANSACTION_DATE desc, CLIENT_NAME

	ELSE IF @reportStatusId=6 

		select	CONVERT (Varchar(12), MAX(dealtransaction.DEAL_TRANSACTION_DATE), 101)  DEAL_TRANSACTION_DATE,
			deal.RM_DEAL_TICKET_ID,
			CONVERT (Varchar(12), deal.HEDGE_START_MONTH, 113) HEDGE_START_MONTH,
			CONVERT (Varchar(12), deal.HEDGE_END_MONTH, 113) HEDGE_END_MONTH,
			isnull(client.CLIENT_NAME,'') CLIENT_NAME,	
			isnull(site.SITE_NAME,'0') SITE_NAME,
			isnull(sg.Sitegroup_Name,'0') DIVISION_NAME,				
			CASE rflaw.IS_MISTAKES_DETECTED 	
				WHEN 0 THEN 'N'
				WHEN 1 THEN 'Y'
				ELSE ''
				END AS MISTAKES_DETECTED,	
			CAST(rflaw.COST_TO_SUMMIT as decimal(20,2) ) COST_TO_SUMMIT,
			CASE  rcounter.COUNTERPARTY_NAME 
				  WHEN isnull(rcounter.COUNTERPARTY_NAME,'') THEN rcounter.COUNTERPARTY_NAME 
				  ELSE isnull(vendor.VENDOR_NAME,'')
				  END  COUNTERPARTY_NAME		
		from RM_FLAWLESS_EXECUTION rflaw
			INNER JOIN RM_DEAL_TICKET deal
				ON deal.RM_DEAL_TICKET_ID=rflaw.RM_DEAL_TICKET_ID
			INNER JOIN RM_DEAL_TICKET_TRANSACTION_STATUS dealtransaction
				ON deal.RM_DEAL_TICKET_ID=dealtransaction.RM_DEAL_TICKET_ID
			LEFT OUTER JOIN CLIENT client
				ON deal.CLIENT_ID=client.CLIENT_ID
			LEFT OUTER JOIN RM_COUNTERPARTY rcounter
				ON deal.RM_COUNTERPARTY_ID=rcounter.RM_COUNTERPARTY_ID
			LEFT OUTER JOIN SITE site
				ON deal.SITE_ID=site.SITE_ID
			LEFT OUTER JOIN Sitegroup sg
				ON deal.DIVISION_ID=sg.Sitegroup_Id
			LEFT OUTER JOIN VENDOR vendor
				ON deal.VENDOR_ID=vendor.VENDOR_ID
		where CONVERT(Varchar(12), dealtransaction.DEAL_TRANSACTION_DATE, 101) between  @fromDate  AND  @toDate
		GROUP BY deal.RM_DEAL_TICKET_ID,deal.HEDGE_START_MONTH,deal.HEDGE_END_MONTH,client.CLIENT_NAME,site.SITE_NAME,
			sg.Sitegroup_Name,rcounter.COUNTERPARTY_NAME,rflaw.IS_MISTAKES_DETECTED,rflaw.COST_TO_SUMMIT,vendor.VENDOR_NAME

		UNION	

		select	isnull(CONVERT (Varchar(12), MAX(dealtransaction.DEAL_TRANSACTION_DATE), 101),'')  DEAL_TRANSACTION_DATE,
			deal.RM_DEAL_TICKET_ID,
			CONVERT (Varchar(12), deal.HEDGE_START_MONTH, 113) HEDGE_START_MONTH,
			CONVERT (Varchar(12), deal.HEDGE_END_MONTH, 113) HEDGE_END_MONTH,
			isnull(client.CLIENT_NAME,'') CLIENT_NAME,	
			isnull(site.SITE_NAME,'0') SITE_NAME,
			isnull(sg.Sitegroup_Name,'0') DIVISION_NAME,	
 			'Y' MISTAKES_DETECTED,	
			CAST(0.0 as decimal(20,2) ) COST_TO_SUMMIT,
			CASE  rcounter.COUNTERPARTY_NAME 
				  WHEN isnull(rcounter.COUNTERPARTY_NAME,'') THEN rcounter.COUNTERPARTY_NAME 
				  ELSE isnull(vendor.VENDOR_NAME,'')
				  END  COUNTERPARTY_NAME		
		from RM_DEAL_TICKET_TRANSACTION_STATUS dealtransaction
			INNER JOIN RM_DEAL_TICKET deal
				ON deal.RM_DEAL_TICKET_ID=dealtransaction.RM_DEAL_TICKET_ID
			LEFT OUTER JOIN CLIENT client
				ON deal.CLIENT_ID=client.CLIENT_ID
			LEFT OUTER JOIN RM_COUNTERPARTY rcounter
				ON deal.RM_COUNTERPARTY_ID=rcounter.RM_COUNTERPARTY_ID
			LEFT OUTER JOIN SITE site
				ON deal.SITE_ID=site.SITE_ID
			LEFT OUTER JOIN Sitegroup sg
				ON deal.DIVISION_ID=sg.Sitegroup_Id
			LEFT OUTER JOIN VENDOR vendor
				ON deal.VENDOR_ID=vendor.VENDOR_ID
		where	CONVERT(Varchar(12), dealtransaction.DEAL_TRANSACTION_DATE, 101) between  @fromDate  AND  @toDate
			AND deal.RM_DEAL_TICKET_ID NOT IN(select distinct RM_DEAL_TICKET_ID from RM_FLAWLESS_EXECUTION)
		GROUP BY deal.RM_DEAL_TICKET_ID,deal.HEDGE_START_MONTH,deal.HEDGE_END_MONTH,client.CLIENT_NAME,site.SITE_NAME,
			sg.Sitegroup_Name,rcounter.COUNTERPARTY_NAME,vendor.VENDOR_NAME
		order by DEAL_TRANSACTION_DATE desc,CLIENT_NAME


	ELSE IF @reportStatusId=7 

		select	CONVERT (Varchar(12), MAX(dealtransaction.DEAL_TRANSACTION_DATE), 101)  DEAL_TRANSACTION_DATE,
			deal.RM_DEAL_TICKET_ID,
			CONVERT (Varchar(12), deal.HEDGE_START_MONTH, 113) HEDGE_START_MONTH,
			CONVERT (Varchar(12), deal.HEDGE_END_MONTH, 113) HEDGE_END_MONTH,
			isnull(client.CLIENT_NAME,'') CLIENT_NAME,	
			isnull(site.SITE_NAME,'0') SITE_NAME,
			isnull(sg.Sitegroup_Name,'0') DIVISION_NAME,				
			CASE rflaw.IS_MISTAKES_DETECTED 	
				WHEN 0 THEN 'N'
				WHEN 1 THEN 'Y'
				ELSE ''
				END AS MISTAKES_DETECTED,	
			CAST(rflaw.COST_TO_SUMMIT as decimal(20,2) ) COST_TO_SUMMIT,
			CASE  rcounter.COUNTERPARTY_NAME 
				  WHEN isnull(rcounter.COUNTERPARTY_NAME,'') THEN rcounter.COUNTERPARTY_NAME 
				  ELSE isnull(vendor.VENDOR_NAME,'')
				  END  COUNTERPARTY_NAME		
		from	RM_FLAWLESS_EXECUTION rflaw
			INNER JOIN RM_DEAL_TICKET deal
				ON deal.RM_DEAL_TICKET_ID=rflaw.RM_DEAL_TICKET_ID
			INNER JOIN RM_DEAL_TICKET_TRANSACTION_STATUS dealtransaction
				ON deal.RM_DEAL_TICKET_ID=dealtransaction.RM_DEAL_TICKET_ID
			LEFT OUTER JOIN CLIENT client
				ON deal.CLIENT_ID=client.CLIENT_ID
			LEFT OUTER JOIN RM_COUNTERPARTY rcounter
				ON deal.RM_COUNTERPARTY_ID=rcounter.RM_COUNTERPARTY_ID
			LEFT OUTER JOIN SITE site
				ON deal.SITE_ID=site.SITE_ID
			LEFT OUTER JOIN Sitegroup sg
				ON deal.DIVISION_ID=sg.Sitegroup_Id
			LEFT OUTER JOIN VENDOR vendor
				ON deal.VENDOR_ID=vendor.VENDOR_ID
		
		where rflaw.COST_TO_SUMMIT<=0 
			AND	CONVERT(Varchar(12), dealtransaction.DEAL_TRANSACTION_DATE, 101) between  @fromDate  AND  @toDate 
		GROUP BY deal.RM_DEAL_TICKET_ID,deal.HEDGE_START_MONTH,deal.HEDGE_END_MONTH,client.CLIENT_NAME,site.SITE_NAME,
			sg.Sitegroup_Name,rcounter.COUNTERPARTY_NAME,rflaw.IS_MISTAKES_DETECTED,rflaw.COST_TO_SUMMIT,vendor.VENDOR_NAME

		UNION	

		select	isnull(CONVERT (Varchar(12), MAX(dealtransaction.DEAL_TRANSACTION_DATE), 101),'')  DEAL_TRANSACTION_DATE,
			deal.RM_DEAL_TICKET_ID,
			CONVERT (Varchar(12), deal.HEDGE_START_MONTH, 113) HEDGE_START_MONTH,
			CONVERT (Varchar(12), deal.HEDGE_END_MONTH, 113) HEDGE_END_MONTH,
			isnull(client.CLIENT_NAME,'') CLIENT_NAME,	
			isnull(site.SITE_NAME,'0') SITE_NAME,
			isnull(sg.Sitegroup_Name,'0') DIVISION_NAME,	
 			'Y' MISTAKES_DETECTED,	
			CAST(0.0 as decimal(20,2) ) COST_TO_SUMMIT,
			CASE  rcounter.COUNTERPARTY_NAME 
				  WHEN isnull(rcounter.COUNTERPARTY_NAME,'') THEN rcounter.COUNTERPARTY_NAME 
				  ELSE isnull(vendor.VENDOR_NAME,'')
				  END  COUNTERPARTY_NAME		
		from RM_DEAL_TICKET_TRANSACTION_STATUS dealtransaction
			INNER JOIN RM_DEAL_TICKET deal
				ON deal.RM_DEAL_TICKET_ID=dealtransaction.RM_DEAL_TICKET_ID
			LEFT OUTER JOIN CLIENT client
				ON deal.CLIENT_ID=client.CLIENT_ID
			LEFT OUTER JOIN RM_COUNTERPARTY rcounter
				ON deal.RM_COUNTERPARTY_ID=rcounter.RM_COUNTERPARTY_ID
			LEFT OUTER JOIN SITE site
				ON deal.SITE_ID=site.SITE_ID
			LEFT OUTER JOIN Sitegroup sg
				ON deal.DIVISION_ID=sg.Sitegroup_Id
			LEFT OUTER JOIN VENDOR vendor
				ON deal.VENDOR_ID=vendor.VENDOR_ID
		where deal.RM_DEAL_TICKET_ID NOT IN(select distinct RM_DEAL_TICKET_ID from RM_FLAWLESS_EXECUTION) 
			AND	CONVERT(Varchar(12), dealtransaction.DEAL_TRANSACTION_DATE, 101) between  @fromDate  AND  @toDate 
		GROUP BY deal.RM_DEAL_TICKET_ID,deal.HEDGE_START_MONTH,deal.HEDGE_END_MONTH,client.CLIENT_NAME,site.SITE_NAME,
			sg.Sitegroup_Name,rcounter.COUNTERPARTY_NAME,vendor.VENDOR_NAME

		order by DEAL_TRANSACTION_DATE desc,CLIENT_NAME


	ELSE IF @reportStatusId=8 

		select	CONVERT (Varchar(12), MAX(dealtransaction.DEAL_TRANSACTION_DATE), 101)  DEAL_TRANSACTION_DATE,
			deal.RM_DEAL_TICKET_ID,
			CONVERT (Varchar(12), deal.HEDGE_START_MONTH, 113) HEDGE_START_MONTH,
			CONVERT (Varchar(12), deal.HEDGE_END_MONTH, 113) HEDGE_END_MONTH,
			isnull(client.CLIENT_NAME,'') CLIENT_NAME,	
			isnull(site.SITE_NAME,'0') SITE_NAME,
			isnull(sg.Sitegroup_Name,'0') DIVISION_NAME,				
			CASE rflaw.IS_MISTAKES_DETECTED 	
				WHEN 0 THEN 'N'
				WHEN 1 THEN 'Y'
				ELSE ''
				END AS MISTAKES_DETECTED,	
			CAST(rflaw.COST_TO_SUMMIT as decimal(20,2) ) COST_TO_SUMMIT,
			CASE  rcounter.COUNTERPARTY_NAME 
				  WHEN isnull(rcounter.COUNTERPARTY_NAME,'') THEN rcounter.COUNTERPARTY_NAME 
				  ELSE isnull(vendor.VENDOR_NAME,'')
				  END  COUNTERPARTY_NAME		
		from	RM_FLAWLESS_EXECUTION rflaw
			INNER JOIN RM_DEAL_TICKET deal
				ON deal.RM_DEAL_TICKET_ID=rflaw.RM_DEAL_TICKET_ID
			INNER JOIN RM_DEAL_TICKET_TRANSACTION_STATUS dealtransaction
				ON deal.RM_DEAL_TICKET_ID=dealtransaction.RM_DEAL_TICKET_ID
			LEFT OUTER JOIN CLIENT client
				ON deal.CLIENT_ID=client.CLIENT_ID
			LEFT OUTER JOIN RM_COUNTERPARTY rcounter
				ON deal.RM_COUNTERPARTY_ID=rcounter.RM_COUNTERPARTY_ID
			LEFT OUTER JOIN SITE site
				ON deal.SITE_ID=site.SITE_ID
			LEFT OUTER JOIN Sitegroup sg
				ON deal.DIVISION_ID=sg.Sitegroup_Id
			LEFT OUTER JOIN VENDOR vendor
				ON deal.VENDOR_ID=vendor.VENDOR_ID
		where rflaw.COST_TO_SUMMIT >0 
			AND	CONVERT(Varchar(12), dealtransaction.DEAL_TRANSACTION_DATE, 101) between  @fromDate  AND  @toDate 
		GROUP BY deal.RM_DEAL_TICKET_ID,deal.HEDGE_START_MONTH,deal.HEDGE_END_MONTH,client.CLIENT_NAME,site.SITE_NAME,
			sg.Sitegroup_Name,rcounter.COUNTERPARTY_NAME,rflaw.IS_MISTAKES_DETECTED,rflaw.COST_TO_SUMMIT,vendor.VENDOR_NAME
		order by DEAL_TRANSACTION_DATE desc,CLIENT_NAME

	ELSE IF @reportStatusId=9 

		select	CONVERT (Varchar(12), MAX(dealtransaction.DEAL_TRANSACTION_DATE), 101)  DEAL_TRANSACTION_DATE,
			deal.RM_DEAL_TICKET_ID,
			CONVERT (Varchar(12), deal.HEDGE_START_MONTH, 113) HEDGE_START_MONTH,
			CONVERT (Varchar(12), deal.HEDGE_END_MONTH, 113) HEDGE_END_MONTH,
			isnull(client.CLIENT_NAME,'') CLIENT_NAME,	
			isnull(site.SITE_NAME,'0') SITE_NAME,
			isnull(sg.Sitegroup_Name,'0') DIVISION_NAME,				
			CASE rflaw.IS_MISTAKES_DETECTED 	
				WHEN 0 THEN 'N'
				WHEN 1 THEN 'Y'
				ELSE ''
				END AS MISTAKES_DETECTED,	
			CAST(rflaw.COST_TO_SUMMIT as decimal(20,2) ) COST_TO_SUMMIT,
			CASE  rcounter.COUNTERPARTY_NAME 
				  WHEN isnull(rcounter.COUNTERPARTY_NAME,'') THEN rcounter.COUNTERPARTY_NAME 
				  ELSE isnull(vendor.VENDOR_NAME,'')
				  END  COUNTERPARTY_NAME		
		from RM_FLAWLESS_EXECUTION rflaw
			INNER JOIN RM_DEAL_TICKET deal
				ON deal.RM_DEAL_TICKET_ID=rflaw.RM_DEAL_TICKET_ID
			INNER JOIN RM_DEAL_TICKET_TRANSACTION_STATUS dealtransaction
				ON deal.RM_DEAL_TICKET_ID=dealtransaction.RM_DEAL_TICKET_ID
			LEFT OUTER JOIN CLIENT client
				ON deal.CLIENT_ID=client.CLIENT_ID
			LEFT OUTER JOIN RM_COUNTERPARTY rcounter
				ON deal.RM_COUNTERPARTY_ID=rcounter.RM_COUNTERPARTY_ID
			LEFT OUTER JOIN SITE site
				ON deal.SITE_ID=site.SITE_ID
			LEFT OUTER JOIN Sitegroup sg
				ON deal.DIVISION_ID=sg.Sitegroup_Id
			LEFT OUTER JOIN VENDOR vendor
				ON deal.VENDOR_ID=vendor.VENDOR_ID
		
		where rflaw.IS_MISTAKES_DETECTED=@flawless 
			AND	CONVERT(Varchar(12), dealtransaction.DEAL_TRANSACTION_DATE, 101) between  @fromDate  AND  @toDate 
		GROUP BY deal.RM_DEAL_TICKET_ID,deal.HEDGE_START_MONTH,deal.HEDGE_END_MONTH,client.CLIENT_NAME,site.SITE_NAME,
			sg.Sitegroup_Name,rcounter.COUNTERPARTY_NAME,rflaw.IS_MISTAKES_DETECTED,rflaw.COST_TO_SUMMIT,vendor.VENDOR_NAME

		UNION	

		select	isnull(CONVERT (Varchar(12), MAX(dealtransaction.DEAL_TRANSACTION_DATE), 101),'')  DEAL_TRANSACTION_DATE,
			deal.RM_DEAL_TICKET_ID,
			CONVERT (Varchar(12), deal.HEDGE_START_MONTH, 113) HEDGE_START_MONTH,
			CONVERT (Varchar(12), deal.HEDGE_END_MONTH, 113) HEDGE_END_MONTH,
			isnull(client.CLIENT_NAME,'') CLIENT_NAME,	
			isnull(site.SITE_NAME,'0') SITE_NAME,
			isnull(sg.Sitegroup_Name,'0') DIVISION_NAME,	
 			'Y' MISTAKES_DETECTED,	
			CAST(0.0 as decimal(20,2) ) COST_TO_SUMMIT,
			CASE  rcounter.COUNTERPARTY_NAME 
				  WHEN isnull(rcounter.COUNTERPARTY_NAME,'') THEN rcounter.COUNTERPARTY_NAME 
				  ELSE isnull(vendor.VENDOR_NAME,'')
				  END  COUNTERPARTY_NAME		
		
		from RM_DEAL_TICKET_TRANSACTION_STATUS dealtransaction
			INNER JOIN RM_DEAL_TICKET deal
				ON deal.RM_DEAL_TICKET_ID=dealtransaction.RM_DEAL_TICKET_ID
			LEFT OUTER JOIN CLIENT client
				ON deal.CLIENT_ID=client.CLIENT_ID
			LEFT OUTER JOIN RM_COUNTERPARTY rcounter
				ON deal.RM_COUNTERPARTY_ID=rcounter.RM_COUNTERPARTY_ID
			LEFT OUTER JOIN SITE site
				ON deal.SITE_ID=site.SITE_ID
			LEFT OUTER JOIN Sitegroup sg
				ON deal.DIVISION_ID=sg.Sitegroup_Id
			LEFT OUTER JOIN VENDOR vendor
				ON deal.VENDOR_ID=vendor.VENDOR_ID
		
		where deal.RM_DEAL_TICKET_ID NOT IN(select distinct RM_DEAL_TICKET_ID from RM_FLAWLESS_EXECUTION) 
		GROUP BY deal.RM_DEAL_TICKET_ID,deal.HEDGE_START_MONTH,deal.HEDGE_END_MONTH,client.CLIENT_NAME,site.SITE_NAME,
			sg.Sitegroup_Name,rcounter.COUNTERPARTY_NAME,vendor.VENDOR_NAME
		order by DEAL_TRANSACTION_DATE desc,CLIENT_NAME


	ELSE IF @reportStatusId=10 

		select	CONVERT (Varchar(12), MAX(dealtransaction.DEAL_TRANSACTION_DATE), 101)  DEAL_TRANSACTION_DATE,
			deal.RM_DEAL_TICKET_ID,
			CONVERT (Varchar(12), deal.HEDGE_START_MONTH, 113) HEDGE_START_MONTH,
			CONVERT (Varchar(12), deal.HEDGE_END_MONTH, 113) HEDGE_END_MONTH,
			isnull(client.CLIENT_NAME,'') CLIENT_NAME,	
			isnull(site.SITE_NAME,'0') SITE_NAME,
			isnull(sg.Sitegroup_Name,'0') DIVISION_NAME,				
			CASE rflaw.IS_MISTAKES_DETECTED 	
				WHEN 0 THEN 'N'
				WHEN 1 THEN 'Y'
				ELSE ''
				END AS MISTAKES_DETECTED,	
			CAST(rflaw.COST_TO_SUMMIT as decimal(20,2) ) COST_TO_SUMMIT,
			CASE  rcounter.COUNTERPARTY_NAME 
				  WHEN isnull(rcounter.COUNTERPARTY_NAME,'') THEN rcounter.COUNTERPARTY_NAME 
				  ELSE isnull(vendor.VENDOR_NAME,'')
				  END  COUNTERPARTY_NAME		

		from RM_FLAWLESS_EXECUTION rflaw
			INNER JOIN RM_DEAL_TICKET deal
				ON deal.RM_DEAL_TICKET_ID=rflaw.RM_DEAL_TICKET_ID
			INNER JOIN RM_DEAL_TICKET_TRANSACTION_STATUS dealtransaction
				ON deal.RM_DEAL_TICKET_ID=dealtransaction.RM_DEAL_TICKET_ID
			LEFT OUTER JOIN CLIENT client
				ON deal.CLIENT_ID=client.CLIENT_ID
			LEFT OUTER JOIN RM_COUNTERPARTY rcounter
				ON deal.RM_COUNTERPARTY_ID=rcounter.RM_COUNTERPARTY_ID
			LEFT OUTER JOIN SITE site
				ON deal.SITE_ID=site.SITE_ID
			LEFT OUTER JOIN Sitegroup sg
				ON deal.DIVISION_ID=sg.Sitegroup_Id
			LEFT OUTER JOIN VENDOR vendor
				ON deal.VENDOR_ID=vendor.VENDOR_ID
		
		where rflaw.IS_MISTAKES_DETECTED=@flawless 
			AND CONVERT(Varchar(12), dealtransaction.DEAL_TRANSACTION_DATE, 101) between  @fromDate  AND  @toDate 
		GROUP BY deal.RM_DEAL_TICKET_ID,deal.HEDGE_START_MONTH,deal.HEDGE_END_MONTH,client.CLIENT_NAME,site.SITE_NAME,
			sg.Sitegroup_Name,rcounter.COUNTERPARTY_NAME,rflaw.IS_MISTAKES_DETECTED,rflaw.COST_TO_SUMMIT,vendor.VENDOR_NAME
		order by DEAL_TRANSACTION_DATE desc,CLIENT_NAME


	ELSE IF @reportStatusId=11 

		select	CONVERT (Varchar(12), MAX(dealtransaction.DEAL_TRANSACTION_DATE), 101)  DEAL_TRANSACTION_DATE,
			deal.RM_DEAL_TICKET_ID,
			CONVERT (Varchar(12), deal.HEDGE_START_MONTH, 113) HEDGE_START_MONTH,
			CONVERT (Varchar(12), deal.HEDGE_END_MONTH, 113) HEDGE_END_MONTH,
			isnull(client.CLIENT_NAME,'') CLIENT_NAME,	
			isnull(site.SITE_NAME,'0') SITE_NAME,
			isnull(sg.Sitegroup_Name,'0') DIVISION_NAME,	
			CASE rflaw.IS_MISTAKES_DETECTED 	
				WHEN 0 THEN 'N'
				WHEN 1 THEN 'Y'
				ELSE ''
				END AS MISTAKES_DETECTED,	
			CAST(rflaw.COST_TO_SUMMIT as decimal(20,2) ) COST_TO_SUMMIT,
			CASE  rcounter.COUNTERPARTY_NAME 
				  WHEN isnull(rcounter.COUNTERPARTY_NAME,'') THEN rcounter.COUNTERPARTY_NAME 
				  ELSE isnull(vendor.VENDOR_NAME,'')
				  END  COUNTERPARTY_NAME		
		from RM_FLAWLESS_EXECUTION rflaw
			INNER JOIN RM_DEAL_TICKET deal
				ON deal.RM_DEAL_TICKET_ID=rflaw.RM_DEAL_TICKET_ID
			INNER JOIN RM_DEAL_TICKET_TRANSACTION_STATUS dealtransaction
				ON deal.RM_DEAL_TICKET_ID=dealtransaction.RM_DEAL_TICKET_ID
			LEFT OUTER JOIN CLIENT client
				ON deal.CLIENT_ID=client.CLIENT_ID
			LEFT OUTER JOIN RM_COUNTERPARTY rcounter
				ON deal.RM_COUNTERPARTY_ID=rcounter.RM_COUNTERPARTY_ID
			LEFT OUTER JOIN SITE site
				ON deal.SITE_ID=site.SITE_ID
			LEFT OUTER JOIN Sitegroup sg
				ON deal.DIVISION_ID=sg.Sitegroup_Id
			LEFT OUTER JOIN VENDOR vendor
				ON deal.VENDOR_ID=vendor.VENDOR_ID
		
		where rflaw.IS_MISTAKES_DETECTED=@flawless 
			AND	rflaw.COST_TO_SUMMIT<=0		
		GROUP BY deal.RM_DEAL_TICKET_ID,deal.HEDGE_START_MONTH,deal.HEDGE_END_MONTH,client.CLIENT_NAME,site.SITE_NAME,
			sg.Sitegroup_Name,rcounter.COUNTERPARTY_NAME,rflaw.IS_MISTAKES_DETECTED,rflaw.COST_TO_SUMMIT,vendor.VENDOR_NAME
		order by DEAL_TRANSACTION_DATE desc,CLIENT_NAME


	ELSE IF @reportStatusId=12 

		select	CONVERT (Varchar(12), MAX(dealtransaction.DEAL_TRANSACTION_DATE), 101)  DEAL_TRANSACTION_DATE,
			deal.RM_DEAL_TICKET_ID,
			CONVERT (Varchar(12), deal.HEDGE_START_MONTH, 113) HEDGE_START_MONTH,
			CONVERT (Varchar(12), deal.HEDGE_END_MONTH, 113) HEDGE_END_MONTH,
			isnull(client.CLIENT_NAME,'') CLIENT_NAME,	
			isnull(site.SITE_NAME,'0') SITE_NAME,
			isnull(sg.Sitegroup_Name,'0') DIVISION_NAME,	
			CASE rflaw.IS_MISTAKES_DETECTED 	
				WHEN 0 THEN 'N'
				WHEN 1 THEN 'Y'
				ELSE ''
				END AS MISTAKES_DETECTED,	
			CAST(rflaw.COST_TO_SUMMIT as decimal(20,2) ) COST_TO_SUMMIT,
			CASE  rcounter.COUNTERPARTY_NAME 
				  WHEN isnull(rcounter.COUNTERPARTY_NAME,'') THEN rcounter.COUNTERPARTY_NAME 
				  ELSE isnull(vendor.VENDOR_NAME,'')
				  END  COUNTERPARTY_NAME		
		from RM_FLAWLESS_EXECUTION rflaw
			INNER JOIN RM_DEAL_TICKET deal
				ON deal.RM_DEAL_TICKET_ID=rflaw.RM_DEAL_TICKET_ID
			INNER JOIN RM_DEAL_TICKET_TRANSACTION_STATUS dealtransaction
				ON deal.RM_DEAL_TICKET_ID=dealtransaction.RM_DEAL_TICKET_ID
			LEFT OUTER JOIN CLIENT client
				ON deal.CLIENT_ID=client.CLIENT_ID
			LEFT OUTER JOIN RM_COUNTERPARTY rcounter
				ON deal.RM_COUNTERPARTY_ID=rcounter.RM_COUNTERPARTY_ID
			LEFT OUTER JOIN SITE site
				ON deal.SITE_ID=site.SITE_ID
			LEFT OUTER JOIN Sitegroup sg
				ON deal.DIVISION_ID=sg.Sitegroup_Id
			LEFT OUTER JOIN VENDOR vendor
				ON deal.VENDOR_ID=vendor.VENDOR_ID
		
		where rflaw.IS_MISTAKES_DETECTED=@flawless 
			AND rflaw.COST_TO_SUMMIT>0		
		GROUP BY deal.RM_DEAL_TICKET_ID,deal.HEDGE_START_MONTH,deal.HEDGE_END_MONTH,client.CLIENT_NAME,site.SITE_NAME,
			sg.Sitegroup_Name,rcounter.COUNTERPARTY_NAME,rflaw.IS_MISTAKES_DETECTED,rflaw.COST_TO_SUMMIT,vendor.VENDOR_NAME
		order by DEAL_TRANSACTION_DATE desc,CLIENT_NAME



	ELSE IF @reportStatusId=13 

		select	CONVERT (Varchar(12), MAX(dealtransaction.DEAL_TRANSACTION_DATE), 101)  DEAL_TRANSACTION_DATE,
			deal.RM_DEAL_TICKET_ID,
			CONVERT (Varchar(12), deal.HEDGE_START_MONTH, 113) HEDGE_START_MONTH,
			CONVERT (Varchar(12), deal.HEDGE_END_MONTH, 113) HEDGE_END_MONTH,
			isnull(client.CLIENT_NAME,'') CLIENT_NAME,	
			isnull(site.SITE_NAME,'0') SITE_NAME,
			isnull(sg.Sitegroup_Name,'0') DIVISION_NAME,	
			CASE rflaw.IS_MISTAKES_DETECTED 	
				WHEN 0 THEN 'N'
				WHEN 1 THEN 'Y'
				ELSE ''
				END AS MISTAKES_DETECTED,	
			CAST(rflaw.COST_TO_SUMMIT as decimal(20,2) ) COST_TO_SUMMIT,
			CASE  rcounter.COUNTERPARTY_NAME 
				  WHEN isnull(rcounter.COUNTERPARTY_NAME,'') THEN rcounter.COUNTERPARTY_NAME 
				  ELSE isnull(vendor.VENDOR_NAME,'')
				  END  COUNTERPARTY_NAME		
		from RM_FLAWLESS_EXECUTION rflaw
			INNER JOIN RM_DEAL_TICKET deal
				ON deal.RM_DEAL_TICKET_ID=rflaw.RM_DEAL_TICKET_ID
			INNER JOIN RM_DEAL_TICKET_TRANSACTION_STATUS dealtransaction
				ON deal.RM_DEAL_TICKET_ID=dealtransaction.RM_DEAL_TICKET_ID
			LEFT OUTER JOIN CLIENT client
				ON deal.CLIENT_ID=client.CLIENT_ID
			LEFT OUTER JOIN RM_COUNTERPARTY rcounter
				ON deal.RM_COUNTERPARTY_ID=rcounter.RM_COUNTERPARTY_ID
			LEFT OUTER JOIN SITE site
				ON deal.SITE_ID=site.SITE_ID
			LEFT OUTER JOIN Sitegroup sg
				ON deal.DIVISION_ID=sg.Sitegroup_Id
			LEFT OUTER JOIN VENDOR vendor
				ON deal.VENDOR_ID=vendor.VENDOR_ID
		where rflaw.IS_MISTAKES_DETECTED=@flawless AND
			rflaw.COST_TO_SUMMIT<=0		
		GROUP BY deal.RM_DEAL_TICKET_ID,deal.HEDGE_START_MONTH,deal.HEDGE_END_MONTH,client.CLIENT_NAME,site.SITE_NAME,
			sg.Sitegroup_Name,rcounter.COUNTERPARTY_NAME,rflaw.IS_MISTAKES_DETECTED,rflaw.COST_TO_SUMMIT,vendor.VENDOR_NAME

		UNION	

		select	isnull(CONVERT (Varchar(12), MAX(dealtransaction.DEAL_TRANSACTION_DATE), 101),'')  DEAL_TRANSACTION_DATE,
			deal.RM_DEAL_TICKET_ID,
			CONVERT (Varchar(12), deal.HEDGE_START_MONTH, 113) HEDGE_START_MONTH,
			CONVERT (Varchar(12), deal.HEDGE_END_MONTH, 113) HEDGE_END_MONTH,
			isnull(client.CLIENT_NAME,'') CLIENT_NAME,	
			isnull(site.SITE_NAME,'0') SITE_NAME,
			isnull(sg.Sitegroup_Name,'0') DIVISION_NAME,	
 			'Y' MISTAKES_DETECTED,	
			CAST(0.0 as decimal(20,2) ) COST_TO_SUMMIT,
			CASE  rcounter.COUNTERPARTY_NAME 
				  WHEN isnull(rcounter.COUNTERPARTY_NAME,'') THEN rcounter.COUNTERPARTY_NAME 
				  ELSE isnull(vendor.VENDOR_NAME,'')
				  END  COUNTERPARTY_NAME		
		from RM_DEAL_TICKET_TRANSACTION_STATUS dealtransaction
			INNER JOIN RM_DEAL_TICKET deal
				ON deal.RM_DEAL_TICKET_ID=dealtransaction.RM_DEAL_TICKET_ID
			LEFT OUTER JOIN CLIENT client
				ON deal.CLIENT_ID=client.CLIENT_ID
			LEFT OUTER JOIN RM_COUNTERPARTY rcounter
				ON deal.RM_COUNTERPARTY_ID=rcounter.RM_COUNTERPARTY_ID
			LEFT OUTER JOIN SITE site
				ON deal.SITE_ID=site.SITE_ID
			LEFT OUTER JOIN Sitegroup sg
				ON deal.DIVISION_ID=sg.Sitegroup_Id
			LEFT OUTER JOIN VENDOR vendor
				ON deal.VENDOR_ID=vendor.VENDOR_ID
		where deal.RM_DEAL_TICKET_ID NOT IN(select distinct RM_DEAL_TICKET_ID from RM_FLAWLESS_EXECUTION)
		GROUP BY deal.RM_DEAL_TICKET_ID,deal.HEDGE_START_MONTH,deal.HEDGE_END_MONTH,client.CLIENT_NAME,site.SITE_NAME,
			sg.Sitegroup_Name,rcounter.COUNTERPARTY_NAME,vendor.VENDOR_NAME
		order by DEAL_TRANSACTION_DATE desc,CLIENT_NAME


	ELSE IF @reportStatusId=14 

		select	CONVERT (Varchar(12), MAX(dealtransaction.DEAL_TRANSACTION_DATE), 101)  DEAL_TRANSACTION_DATE,
			deal.RM_DEAL_TICKET_ID,
			CONVERT (Varchar(12), deal.HEDGE_START_MONTH, 113) HEDGE_START_MONTH,
			CONVERT (Varchar(12), deal.HEDGE_END_MONTH, 113) HEDGE_END_MONTH,
			isnull(client.CLIENT_NAME,'') CLIENT_NAME,	
			isnull(site.SITE_NAME,'0') SITE_NAME,
			isnull(sg.Sitegroup_Name,'0') DIVISION_NAME,	
			CASE rflaw.IS_MISTAKES_DETECTED 	
				WHEN 0 THEN 'N'
				WHEN 1 THEN 'Y'
				ELSE ''
				END AS MISTAKES_DETECTED,	
			CAST(rflaw.COST_TO_SUMMIT as decimal(20,2) ) COST_TO_SUMMIT,
			CASE  rcounter.COUNTERPARTY_NAME 
				  WHEN isnull(rcounter.COUNTERPARTY_NAME,'') THEN rcounter.COUNTERPARTY_NAME 
				  ELSE isnull(vendor.VENDOR_NAME,'')
				  END  COUNTERPARTY_NAME		
		from	RM_FLAWLESS_EXECUTION rflaw
			INNER JOIN RM_DEAL_TICKET deal
				ON deal.RM_DEAL_TICKET_ID=rflaw.RM_DEAL_TICKET_ID
			INNER JOIN RM_DEAL_TICKET_TRANSACTION_STATUS dealtransaction
				ON deal.RM_DEAL_TICKET_ID=dealtransaction.RM_DEAL_TICKET_ID	
			LEFT OUTER JOIN CLIENT client
				ON deal.CLIENT_ID=client.CLIENT_ID
			LEFT OUTER JOIN RM_COUNTERPARTY rcounter
				ON deal.RM_COUNTERPARTY_ID=rcounter.RM_COUNTERPARTY_ID
			LEFT OUTER JOIN SITE site
				ON deal.SITE_ID=site.SITE_ID
			LEFT OUTER JOIN Sitegroup sg
				ON deal.DIVISION_ID=sg.Sitegroup_Id
			LEFT OUTER JOIN VENDOR vendor
				ON deal.VENDOR_ID=vendor.VENDOR_ID
		where rflaw.IS_MISTAKES_DETECTED=@flawless 
			AND	rflaw.COST_TO_SUMMIT<=0	
			AND	CONVERT(Varchar(12), dealtransaction.DEAL_TRANSACTION_DATE, 101) between  @fromDate  AND  @toDate 
		GROUP BY deal.RM_DEAL_TICKET_ID,deal.HEDGE_START_MONTH,deal.HEDGE_END_MONTH,client.CLIENT_NAME,site.SITE_NAME,
			sg.Sitegroup_Name,rcounter.COUNTERPARTY_NAME,rflaw.IS_MISTAKES_DETECTED,rflaw.COST_TO_SUMMIT,vendor.VENDOR_NAME
		order by DEAL_TRANSACTION_DATE desc,CLIENT_NAME


	ELSE IF @reportStatusId=15 

		select	CONVERT (Varchar(12), MAX(dealtransaction.DEAL_TRANSACTION_DATE), 101)  DEAL_TRANSACTION_DATE,
			deal.RM_DEAL_TICKET_ID,
			CONVERT (Varchar(12), deal.HEDGE_START_MONTH, 113) HEDGE_START_MONTH,
			CONVERT (Varchar(12), deal.HEDGE_END_MONTH, 113) HEDGE_END_MONTH,
			isnull(client.CLIENT_NAME,'') CLIENT_NAME,	
			isnull(site.SITE_NAME,'0') SITE_NAME,
			isnull(sg.Sitegroup_Name,'0') DIVISION_NAME,	
			CASE rflaw.IS_MISTAKES_DETECTED 	
				WHEN 0 THEN 'N'
				WHEN 1 THEN 'Y'

				ELSE ''

				END AS MISTAKES_DETECTED,	
			CAST(rflaw.COST_TO_SUMMIT as decimal(20,2) ) COST_TO_SUMMIT,
			CASE  rcounter.COUNTERPARTY_NAME 
				  WHEN isnull(rcounter.COUNTERPARTY_NAME,'') THEN rcounter.COUNTERPARTY_NAME 
				  ELSE isnull(vendor.VENDOR_NAME,'')
				  END  COUNTERPARTY_NAME		

		from RM_FLAWLESS_EXECUTION rflaw
			INNER JOIN RM_DEAL_TICKET deal
				ON deal.RM_DEAL_TICKET_ID=rflaw.RM_DEAL_TICKET_ID
			INNER JOIN RM_DEAL_TICKET_TRANSACTION_STATUS dealtransaction
				ON deal.RM_DEAL_TICKET_ID=dealtransaction.RM_DEAL_TICKET_ID
			LEFT OUTER JOIN CLIENT client
				ON deal.CLIENT_ID=client.CLIENT_ID
			LEFT OUTER JOIN RM_COUNTERPARTY rcounter
				ON deal.RM_COUNTERPARTY_ID=rcounter.RM_COUNTERPARTY_ID
			LEFT OUTER JOIN SITE site
				ON deal.SITE_ID=site.SITE_ID
			LEFT OUTER JOIN Sitegroup sg
				ON deal.DIVISION_ID=sg.Sitegroup_Id
			LEFT OUTER JOIN VENDOR vendor
				ON deal.VENDOR_ID=vendor.VENDOR_ID
		
		where rflaw.IS_MISTAKES_DETECTED=@flawless 
			AND rflaw.COST_TO_SUMMIT>0	
			AND	CONVERT(Varchar(12), dealtransaction.DEAL_TRANSACTION_DATE, 101) between  @fromDate  AND  @toDate 
		GROUP BY deal.RM_DEAL_TICKET_ID,deal.HEDGE_START_MONTH,deal.HEDGE_END_MONTH,client.CLIENT_NAME,site.SITE_NAME,
			sg.Sitegroup_Name,rcounter.COUNTERPARTY_NAME,rflaw.IS_MISTAKES_DETECTED,rflaw.COST_TO_SUMMIT,vendor.VENDOR_NAME
		order by DEAL_TRANSACTION_DATE desc,CLIENT_NAME



	ELSE IF @reportStatusId=16 

		select	CONVERT (Varchar(12), MAX(dealtransaction.DEAL_TRANSACTION_DATE), 101)  DEAL_TRANSACTION_DATE,
			deal.RM_DEAL_TICKET_ID,
			CONVERT (Varchar(12), deal.HEDGE_START_MONTH, 113) HEDGE_START_MONTH,
			CONVERT (Varchar(12), deal.HEDGE_END_MONTH, 113) HEDGE_END_MONTH,
			isnull(client.CLIENT_NAME,'') CLIENT_NAME,	
			isnull(site.SITE_NAME,'0') SITE_NAME,
			isnull(sg.Sitegroup_Name,'0') DIVISION_NAME,	
			CASE rflaw.IS_MISTAKES_DETECTED 	
				WHEN 0 THEN 'N'
				WHEN 1 THEN 'Y'
				ELSE ''
				END AS MISTAKES_DETECTED,	
			CAST(rflaw.COST_TO_SUMMIT as decimal(20,2) ) COST_TO_SUMMIT,
			CASE  rcounter.COUNTERPARTY_NAME 
				  WHEN isnull(rcounter.COUNTERPARTY_NAME,'') THEN rcounter.COUNTERPARTY_NAME 
				  ELSE isnull(vendor.VENDOR_NAME,'')
				  END  COUNTERPARTY_NAME		

		
		from	RM_FLAWLESS_EXECUTION rflaw
			INNER JOIN RM_DEAL_TICKET deal
				ON deal.RM_DEAL_TICKET_ID=rflaw.RM_DEAL_TICKET_ID
			INNER JOIN RM_DEAL_TICKET_TRANSACTION_STATUS dealtransaction
				ON deal.RM_DEAL_TICKET_ID=dealtransaction.RM_DEAL_TICKET_ID
			LEFT OUTER JOIN CLIENT client
				ON deal.CLIENT_ID=client.CLIENT_ID
			LEFT OUTER JOIN RM_COUNTERPARTY rcounter
				ON deal.RM_COUNTERPARTY_ID=rcounter.RM_COUNTERPARTY_ID
			LEFT OUTER JOIN SITE site
				ON deal.SITE_ID=site.SITE_ID
			LEFT OUTER JOIN Sitegroup sg
				ON deal.DIVISION_ID=sg.Sitegroup_Id
			LEFT OUTER JOIN VENDOR vendor
				ON deal.VENDOR_ID=vendor.VENDOR_ID
		
		where rflaw.IS_MISTAKES_DETECTED=@flawless 
			AND	rflaw.COST_TO_SUMMIT<=0	
			AND	CONVERT(Varchar(12), dealtransaction.DEAL_TRANSACTION_DATE, 101) between  @fromDate  AND  @toDate 	
		GROUP BY deal.RM_DEAL_TICKET_ID,deal.HEDGE_START_MONTH,deal.HEDGE_END_MONTH,client.CLIENT_NAME,site.SITE_NAME,
			sg.Sitegroup_Name,rcounter.COUNTERPARTY_NAME,rflaw.IS_MISTAKES_DETECTED,rflaw.COST_TO_SUMMIT,vendor.VENDOR_NAME

		UNION	

		select	isnull(CONVERT (Varchar(12), MAX(dealtransaction.DEAL_TRANSACTION_DATE), 101),'')  DEAL_TRANSACTION_DATE,
			deal.RM_DEAL_TICKET_ID,
			CONVERT (Varchar(12), deal.HEDGE_START_MONTH, 113) HEDGE_START_MONTH,
			CONVERT (Varchar(12), deal.HEDGE_END_MONTH, 113) HEDGE_END_MONTH,
			isnull(client.CLIENT_NAME,'') CLIENT_NAME,	
			isnull(site.SITE_NAME,'0') SITE_NAME,
			isnull(sg.Sitegroup_Name,'0') DIVISION_NAME,	
 			'Y' MISTAKES_DETECTED,	
			CAST(0.0 as decimal(20,2) ) COST_TO_SUMMIT,
			CASE  rcounter.COUNTERPARTY_NAME 
				  WHEN isnull(rcounter.COUNTERPARTY_NAME,'') THEN rcounter.COUNTERPARTY_NAME 
				  ELSE isnull(vendor.VENDOR_NAME,'')
				  END  COUNTERPARTY_NAME		
		
		from RM_DEAL_TICKET_TRANSACTION_STATUS dealtransaction
			INNER JOIN RM_DEAL_TICKET deal
				ON deal.RM_DEAL_TICKET_ID=dealtransaction.RM_DEAL_TICKET_ID
			LEFT OUTER JOIN CLIENT client
				ON deal.CLIENT_ID=client.CLIENT_ID
			LEFT OUTER JOIN RM_COUNTERPARTY rcounter
				ON deal.RM_COUNTERPARTY_ID=rcounter.RM_COUNTERPARTY_ID
			LEFT OUTER JOIN SITE site
				ON deal.SITE_ID=site.SITE_ID
			LEFT OUTER JOIN Sitegroup sg
				ON deal.DIVISION_ID=sg.Sitegroup_Id
			LEFT OUTER JOIN VENDOR vendor
				ON deal.VENDOR_ID=vendor.VENDOR_ID
		where deal.RM_DEAL_TICKET_ID NOT IN(select distinct RM_DEAL_TICKET_ID from RM_FLAWLESS_EXECUTION) AND
			CONVERT(Varchar(12), dealtransaction.DEAL_TRANSACTION_DATE, 101) between  @fromDate  AND  @toDate 
		GROUP BY deal.RM_DEAL_TICKET_ID,deal.HEDGE_START_MONTH,deal.HEDGE_END_MONTH,client.CLIENT_NAME,site.SITE_NAME,sg.Sitegroup_Name,rcounter.COUNTERPARTY_NAME,vendor.VENDOR_NAME

		order by DEAL_TRANSACTION_DATE desc,CLIENT_NAME

END

GO
GRANT EXECUTE ON  [dbo].[GET_FLAWLESS_REPORT_DATA_P] TO [CBMSApplication]
GO
