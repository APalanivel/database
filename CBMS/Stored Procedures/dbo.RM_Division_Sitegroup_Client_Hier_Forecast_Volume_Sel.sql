SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/**  
NAME:      
     
 dbo.RM_Division_Sitegroup_Client_Hier_Forecast_Volume_Sel  
     
 DESCRIPTION:       
     
 This procedure is to get client onboarded details  
     
 INPUT PARAMETERS:      
 Name    DataType   Default   Description      
----------------------------------------------------------------------        
 @Client_Id   INT  
    @Commodity_Id  INT  
    @Country_Id   VARCHAR(MAX) NULL  
    @Start_Dt   DATE  
    @End_Dt    DATE   NULL  
    @Site_Not_Managed INT    NULL  
    @Sitegroup_Id  INT    NULL  
    @Site_Id   VARCHAR(MAX) NULL  
     
 OUTPUT PARAMETERS:      
 Name    DataType   Default   Description      
----------------------------------------------------------------------  
  
    
  USAGE EXAMPLES:      
------------------------------------------------------------    
  
    
 EXEC RM_Division_Sitegroup_Client_Hier_Forecast_Volume_Sel 235,291,NULL,'2016-01-01','2016-12-01',NULL,NULL,NULL  
 EXEC RM_Division_Sitegroup_Client_Hier_Forecast_Volume_Sel 11236,291,2,'2018-01-01','2018-12-01',NULL,NULL,NULL  
 EXEC RM_Division_Sitegroup_Client_Hier_Forecast_Volume_Sel 12240,291,NULL,'2017-01-01','2017-12-01',NULL,NULL,NULL  
   
    
AUTHOR INITIALS:      
 Initials Name      
------------------------------------------------------------      
 RR   Raghu Reddy  
      
 MODIFICATIONS       
 Initials Date  Modification      
------------------------------------------------------------      
 RR   2018-09-06 Created For Global Risk Managemnet    
  
  
******/

CREATE PROCEDURE [dbo].[RM_Division_Sitegroup_Client_Hier_Forecast_Volume_Sel]
    (
        @Client_Id INT
        , @Commodity_Id INT
        , @Country_Id VARCHAR(MAX) = NULL
        , @Start_Dt DATE
        , @End_Dt DATE = NULL
        , @Site_Not_Managed INT = NULL
        , @Sitegroup_Id INT = NULL
        , @Site_Id VARCHAR(MAX) = NULL
        , @RM_Group_Id INT = NULL
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Total_Cnt INT;


        DECLARE @Tbl_Config AS TABLE
              (
                  RM_Client_Hier_Onboard_Id INT
                  , RM_Forecast_UOM_Type_Id INT
                  , Last_Updated_Ts DATETIME
              );

        DECLARE
            @RM_Forecast_UOM_Type_Id INT
            , @Last_Updated_Ts DATETIME;

        CREATE TABLE #Tbl_Site_HedgeType_Uom
             (
                 Client_Hier_Id INT
                 , Forecast_Uom INT
                 , HedgeType VARCHAR(200)
                 , Service_Month DATE
             );

        DECLARE @Tbl_RM_Group_Sites AS TABLE
              (
                  Site_Id INT
                  , Client_Id INT
                  , Sitegroup_Id INT
                  , Site_Name VARCHAR(200)
              );

        INSERT INTO @Tbl_RM_Group_Sites
             (
                 Site_Id
                 , Client_Id
                 , Sitegroup_Id
                 , Site_Name
             )
        EXEC dbo.Cbms_Sitegroup_Sites_Sel_By_Cbms_Sitegroup_Id
            @Cbms_Sitegroup_Id = @RM_Group_Id;


        INSERT INTO @Tbl_Config
             (
                 RM_Client_Hier_Onboard_Id
                 , RM_Forecast_UOM_Type_Id
                 , Last_Updated_Ts
             )
        SELECT
            chhc.RM_Client_Hier_Onboard_Id
            , chob.RM_Forecast_UOM_Type_Id
            , MAX(ISNULL(chhc.Last_Updated_Ts, chhc.Created_Ts)) AS Last_Updated_Ts
        FROM
            Trade.RM_Client_Hier_Onboard chob
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN Core.Client_Hier ch
                ON chob.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.ENTITY ht
                ON chhc.Hedge_Type_Id = ht.ENTITY_ID
        WHERE
            ch.Client_Id = @Client_Id
            AND chob.Commodity_Id = @Commodity_Id
            AND ht.ENTITY_NAME <> 'Does Not Hedge'
        GROUP BY
            chhc.RM_Client_Hier_Onboard_Id
            , chob.RM_Forecast_UOM_Type_Id;

        SELECT  @Last_Updated_Ts = MAX(Last_Updated_Ts)FROM @Tbl_Config;

        SELECT
            @RM_Forecast_UOM_Type_Id = cc.RM_Forecast_UOM_Type_Id
        FROM
            @Tbl_Config cc
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON cc.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
        WHERE
            ISNULL(chhc.Last_Updated_Ts, chhc.Created_Ts) = @Last_Updated_Ts;


        INSERT INTO #Tbl_Site_HedgeType_Uom
             (
                 Client_Hier_Id
                 , Forecast_Uom
                 , HedgeType
                 , Service_Month
             )
        SELECT
            ch.Client_Hier_Id
            , MAX(chob.RM_Forecast_UOM_Type_Id)
            , MIN(ht.ENTITY_NAME)
            , dd.DATE_D
        FROM
            Core.Client_Hier ch
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON ch.Client_Hier_Id = chob.Client_Hier_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY ht
                ON chhc.Hedge_Type_Id = ht.ENTITY_ID
            CROSS JOIN meta.DATE_DIM dd
        WHERE
            ch.Client_Id = @Client_Id
            AND ht.ENTITY_NAME <> 'Does Not Hedge'
            AND chob.Commodity_Id = @Commodity_Id
            AND (   @Site_Not_Managed IS NULL
                    OR  ch.Site_Not_Managed = @Site_Not_Managed)
            AND (   @Sitegroup_Id IS NULL
                    OR  ch.Sitegroup_Id = @Sitegroup_Id)
            AND dd.DATE_D BETWEEN chhc.Config_Start_Dt
                          AND     chhc.Config_End_Dt
            AND dd.DATE_D BETWEEN @Start_Dt
                          AND     @End_Dt
            AND (   @RM_Group_Id IS NULL
                    OR  EXISTS (SELECT  1 FROM  @Tbl_RM_Group_Sites rgs WHERE   rgs.Site_Id = ch.Site_Id))
        GROUP BY
            ch.Client_Hier_Id
            , dd.DATE_D;

        INSERT INTO #Tbl_Site_HedgeType_Uom
             (
                 Client_Hier_Id
                 , Forecast_Uom
                 , HedgeType
                 , Service_Month
             )
        SELECT
            ch.Client_Hier_Id
            , MAX(chob.RM_Forecast_UOM_Type_Id)
            , MIN(ht.ENTITY_NAME)
            , dd.DATE_D
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier chcl
                ON ch.Client_Id = chcl.Client_Id
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON chcl.Client_Hier_Id = chob.Client_Hier_Id
                   AND  ch.Country_Id = chob.Country_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY ht
                ON chhc.Hedge_Type_Id = ht.ENTITY_ID
            CROSS JOIN meta.DATE_DIM dd
        WHERE
            ch.Client_Id = @Client_Id
            AND ht.ENTITY_NAME <> 'Does Not Hedge'
            AND chcl.Sitegroup_Id = 0
            AND chob.Commodity_Id = @Commodity_Id
            AND (   @Site_Not_Managed IS NULL
                    OR  ch.Site_Not_Managed = @Site_Not_Managed)
            AND (   @Sitegroup_Id IS NULL
                    OR  ch.Sitegroup_Id = @Sitegroup_Id)
            AND dd.DATE_D BETWEEN chhc.Config_Start_Dt
                          AND     chhc.Config_End_Dt
            AND dd.DATE_D BETWEEN @Start_Dt
                          AND     @End_Dt
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.RM_Client_Hier_Onboard siteonb
                               WHERE
                                    siteonb.Client_Hier_Id = ch.Client_Hier_Id
                                    AND siteonb.Commodity_Id = chob.Commodity_Id)
            AND (   @RM_Group_Id IS NULL
                    OR  EXISTS (SELECT  1 FROM  @Tbl_RM_Group_Sites rgs WHERE   rgs.Site_Id = ch.Site_Id))
        GROUP BY
            ch.Client_Hier_Id
            , dd.DATE_D;


        SELECT
            ch2.Client_Hier_Id
            , ch.Sitegroup_Name
            , vol.Service_Month
            , CAST(SUM(vol.Forecast_Volume * cuc.CONVERSION_FACTOR) AS DECIMAL(28, 0)) AS Forecast_Volume
            , ent.ENTITY_NAME AS UOM
            , DENSE_RANK() OVER (ORDER BY
                                     ch.Sitegroup_Name) AS Row_Num
        INTO
            #Sg_Dtl
        FROM
            Trade.RM_Client_Hier_Forecast_Volume vol
            INNER JOIN #Tbl_Site_HedgeType_Uom tshtu
                ON tshtu.Client_Hier_Id = vol.Client_Hier_Id
                   AND  tshtu.Service_Month = vol.Service_Month
            INNER JOIN dbo.ENTITY ent
                ON ent.ENTITY_ID = @RM_Forecast_UOM_Type_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = vol.Client_Hier_Id
            INNER JOIN Core.Client_Hier ch2
                ON ch2.Sitegroup_Id = ch.Sitegroup_Id
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = vol.Uom_Id
                   AND  cuc.CONVERTED_UNIT_ID = @RM_Forecast_UOM_Type_Id
        WHERE
            ch.Client_Id = @Client_Id
            AND vol.Commodity_Id = @Commodity_Id
            AND vol.Service_Month BETWEEN @Start_Dt
                                  AND     @End_Dt
            AND ch2.Sitegroup_Id > 0
            AND ch2.Site_Id = 0
        GROUP BY
            ch2.Client_Hier_Id
            , ch.Sitegroup_Name
            , vol.Service_Month
            , ent.ENTITY_NAME;


        SELECT  @Total_Cnt = MAX(sd.Row_Num)FROM    #Sg_Dtl sd;
        SELECT
            ct.Client_Hier_Id
            , ct.Sitegroup_Name
            , ct.Service_Month
            , ct.Forecast_Volume
            , ct.UOM
            , @Total_Cnt AS Total_Cnt
            , ct.Row_Num
        FROM
            #Sg_Dtl ct
        WHERE
            ct.Row_Num BETWEEN @Start_Index
                       AND     @End_Index
        ORDER BY
            ct.Row_Num;

        DROP TABLE #Sg_Dtl;


    END;





GO
GRANT EXECUTE ON  [dbo].[RM_Division_Sitegroup_Client_Hier_Forecast_Volume_Sel] TO [CBMSApplication]
GO
