SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BulkProcess_UpdateBatch]      
    (      
        @batch_id INT      
        , @Status_Cd INT          
    )      
AS      
    BEGIN      
        SET NOCOUNT ON;      
      
      
        UPDATE      
            [lxbipbd]      
        SET      
            [lxbipbd].[Status_Cd] = @Status_Cd                  
        FROM      
            [LOGDB].[dbo].[XL_Bulk_Invoice_Process_Batch] AS [lxbipbd]      
        WHERE      
            [lxbipbd].[XL_Bulk_Invoice_Process_Batch_Id] = @batch_id;      
      
      
    END; 
GO
GRANT EXECUTE ON  [dbo].[BulkProcess_UpdateBatch] TO [CBMSApplication]
GO
