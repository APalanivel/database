SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******  

NAME:  
 dbo.cbmsSSOSavings_GetTotalByCatForDivision  
  
DESCRIPTION:
  
 INPUT PARAMETERS:  
 Name				DataType  Default Description  
------------------------------------------------------------  
 @division_id		INT                      
  
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
 USAGE EXAMPLES:  
 ------------------------------------------------------------  
  
 EXEC dbo.cbmsSSOSavings_GetTotalByCatForDivision 2819
 
 EXEC dbo.cbmsSSOSavings_GetTotalByCatForDivision 1900
   
 AUTHOR INITIALS:
 Initials	Name
------------------------------------------------------------
 PNR		Pandarinath
  
 MODIFICATIONS :
 Initials Date		 Modification  
------------------------------------------------------------  
 PNR	  04/05/2011 Replaced vwCbmsSSOSavingsOwnerFlat with SSO_SAVINGS_OWNER_MAP table.
 
******/  


CREATE PROCEDURE dbo.cbmsSSOSavings_GetTotalByCatForDivision
( 
 @division_id INT )
AS 
BEGIN  

      SET NOCOUNT ON ;
	
      SELECT
            s.savings_category_type_id
           ,SUM(DISTINCT s.total_estimated_savings) AS total_estimated_savings
           ,@division_id AS division_id
           ,e.entity_name AS savings_category_type
      FROM
            dbo.sso_savings s
            JOIN dbo.entity e
                  ON s.savings_category_type_id = e.entity_id
      WHERE
            EXISTS ( SELECT
                        1
                     FROM
                        dbo.SSO_SAVINGS_OWNER_MAP ssom
                        JOIN core.Client_Hier ch
                              ON ch.Client_Hier_Id = ssom.Client_Hier_Id
                     WHERE
                        ch.Sitegroup_Id = @division_id
                        AND s.sso_savings_id = ssom.sso_savings_id )
      GROUP BY
            s.savings_category_type_id
           ,e.entity_name
            
END  

GO
GRANT EXECUTE ON  [dbo].[cbmsSSOSavings_GetTotalByCatForDivision] TO [CBMSApplication]
GO
GO