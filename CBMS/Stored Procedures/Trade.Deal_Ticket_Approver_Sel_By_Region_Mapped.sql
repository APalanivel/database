SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    Trade.Deal_Ticket_Approver_Sel_By_Region_Mapped
   
    
DESCRIPTION:   
   
  This procedure to get summary page details Deal Ticket Approval Queue.
    
INPUT PARAMETERS:    
      Name          DataType       Default        Description    
-----------------------------------------------------------------------------    
	 @Client_Id		INT
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec Trade.Deal_Ticket_Approver_Sel_By_Region_Mapped 112
	Exec Trade.Deal_Ticket_Approver_Sel_By_Region_Mapped 49
	Exec Trade.Deal_Ticket_Approver_Sel_By_Region_Mapped 8510
	
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	NR          Narayana Reddy
    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	NR          2019-02-10	GRM Proejct.
     
    
******/

CREATE PROC [Trade].[Deal_Ticket_Approver_Sel_By_Region_Mapped] ( @User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;
      
      SELECT
            aprui.USER_INFO_ID
           ,aprui.FIRST_NAME + ' ' + aprui.LAST_NAME AS NAME
           ,aprui.USERNAME
      FROM
            dbo.USER_INFO aprui
            INNER JOIN dbo.USER_INFO_GROUP_INFO_MAP apruigi
                  ON aprui.USER_INFO_ID = apruigi.USER_INFO_ID
            INNER JOIN dbo.GROUP_INFO_PERMISSION_INFO_MAP aprgipi
                  ON apruigi.GROUP_INFO_ID = aprgipi.GROUP_INFO_ID
      WHERE
            EXISTS ( SELECT
                        1
                     FROM
                        dbo.Geographic_Region_Permission_Info_Map grp
                        INNER JOIN dbo.GROUP_INFO_PERMISSION_INFO_MAP gipi
                              ON grp.Permission_Info_Id = gipi.PERMISSION_INFO_ID
                        INNER JOIN dbo.USER_INFO_GROUP_INFO_MAP uigi
                              ON gipi.GROUP_INFO_ID = uigi.GROUP_INFO_ID
                     WHERE
                        uigi.USER_INFO_ID = @User_Info_Id
                        AND aprgipi.PERMISSION_INFO_ID = grp.Permission_Info_Id )
            AND aprui.USER_INFO_ID <> @User_Info_Id
      GROUP BY
            aprui.USER_INFO_ID
           ,aprui.FIRST_NAME + ' ' + aprui.LAST_NAME
           ,aprui.USERNAME
                                   
END;

GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Approver_Sel_By_Region_Mapped] TO [CBMSApplication]
GO
