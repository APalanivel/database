SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE  PROCEDURE [dbo].[cbmsInvSourceExcludeFolder_GetForInvSource]
	( @MyAccountId int
	, @inv_source_id int
	)
AS
BEGIN

	   select inv_source_exclude_folder_id
		, inv_source_id
		, exclude_folder_name
	     from inv_source_exclude_folder with (nolock)
	    where inv_source_id = @inv_source_id


END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvSourceExcludeFolder_GetForInvSource] TO [CBMSApplication]
GO
