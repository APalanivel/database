SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/********* 
NAME:  dbo.Account_Dtl_By_Account_Group_Id
   
DESCRIPTION:  This will fetch all the accounts based on account_group_Id  
  
INPUT PARAMETERS:  
  Name					DataType  Default Description  
------------------------------------------------------------  
  @account_group_id		INT

OUTPUT PARAMETERS:  
  Name  DataType  Default Description  
------------------------------------------------------------  
USAGE EXAMPLES: 
	Account_Dtl_By_Account_Group_Id 14717  
	GO
	Account_Dtl_By_Account_Group_Id 2593  
------------------------------------------------------------
AUTHOR INITIALS:
Initials Name
------------------------------------------------------------
NK   Nageswara Rao Kosuri
SKA  Shobhit Kumar Agrawal 
SSR	 Sharad Srivastava	  
  
Initials Date  Modification
------------------------------------------------------------
ska		12/17/2009  Created  
SSR		04/20/2010	Added ISNULL condition for Account_numbers						
SSR		07/09/2010  Removed the logic of data reterival from SAMM table
							(( map.meter_disassociation_date > a.Supplier_Account_Begin_Dt  
								OR map.meter_disassociation_date IS NULL ))
							(After contract enhancement application is populating both Meter_association_date and meter_disassociation_Date column supplier_Account_meter_map and removing the entries when ever is_history = 1)

HG		2017-03-23  CPH, Supplier account end date is returned as 9999-12-31 if it is null
NR		2020-05-03	MAINT-9936 - To get the Supplier dates from COnfig table with Min & Max value.

******/
CREATE PROCEDURE [dbo].[Account_Dtl_By_Account_Group_Id]
    (
        @account_group_id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        WITH CTE_AccountGrp_Det
        AS (
               SELECT
                    m.ACCOUNT_ID
                    , m.METER_ID
                    , a.ACCOUNT_NUMBER
                    , a.Supplier_Account_Begin_Dt
                    , a.Supplier_Account_End_Dt
                    , a.NOT_MANAGED
                    , a.ACCOUNT_TYPE_ID
                    , a.VENDOR_ID
               FROM
                    dbo.METER m
                    JOIN dbo.ACCOUNT AS a
                        ON m.ACCOUNT_ID = a.ACCOUNT_ID
               WHERE
                    a.ACCOUNT_GROUP_ID = @account_group_id
               UNION
               SELECT
                    map.ACCOUNT_ID
                    , map.METER_ID
                    , a.ACCOUNT_NUMBER
                    , MIN(sac.Supplier_Account_Begin_Dt) AS Supplier_Account_Begin_Dt
                    , MAX(ISNULL(a.Supplier_Account_End_Dt, '9999-12-31')) AS Supplier_Account_End_Dt
                    , a.NOT_MANAGED
                    , a.ACCOUNT_TYPE_ID
                    , a.VENDOR_ID
               FROM
                    dbo.SUPPLIER_ACCOUNT_METER_MAP AS map
                    INNER JOIN dbo.Supplier_Account_Config sac
                        ON sac.Supplier_Account_Config_Id = map.Supplier_Account_Config_Id
                           AND  sac.Account_Id = map.ACCOUNT_ID
                    JOIN dbo.ACCOUNT a
                        ON a.ACCOUNT_ID = map.ACCOUNT_ID
               WHERE
                    a.ACCOUNT_GROUP_ID = @account_group_id
               GROUP BY
                   map.ACCOUNT_ID
                   , map.METER_ID
                   , a.ACCOUNT_NUMBER
                   , a.NOT_MANAGED
                   , a.ACCOUNT_TYPE_ID
                   , a.VENDOR_ID
           )
        SELECT
            cl.CLIENT_ID
            , cl.CLIENT_NAME
            , s.SITE_ID
            , RTRIM(ad.CITY) + ', ' + st.STATE_NAME + ' (' + s.SITE_NAME + ')' Site_Name
            , a.ACCOUNT_TYPE_ID
            , act.ENTITY_NAME AS account_type
            , a.VENDOR_ID
            , v.VENDOR_NAME
            , r.COMMODITY_TYPE_ID
            , com.Commodity_Name
            , a.ACCOUNT_ID
            , ISNULL(a.ACCOUNT_NUMBER, 'Not yet Assigned') account_number
            , a.Supplier_Account_Begin_Dt
            , a.Supplier_Account_End_Dt
            , a.NOT_MANAGED
        FROM
            CTE_AccountGrp_Det a
            JOIN dbo.METER M
                ON M.METER_ID = a.METER_ID
            JOIN dbo.ADDRESS ad
                ON ad.ADDRESS_ID = M.ADDRESS_ID
            JOIN dbo.SITE s
                ON s.SITE_ID = ad.ADDRESS_PARENT_ID
            JOIN dbo.STATE st
                ON st.STATE_ID = ad.STATE_ID
            JOIN dbo.CLIENT AS cl
                ON cl.CLIENT_ID = s.Client_ID
            JOIN dbo.RATE r
                ON r.RATE_ID = M.RATE_ID
            JOIN dbo.VENDOR AS v
                ON v.VENDOR_ID = a.VENDOR_ID
            JOIN dbo.ENTITY AS act
                ON act.ENTITY_ID = a.ACCOUNT_TYPE_ID
            JOIN dbo.Commodity AS com
                ON com.Commodity_Id = r.COMMODITY_TYPE_ID
        GROUP BY
            cl.CLIENT_ID
            , cl.CLIENT_NAME
            , s.SITE_ID
            , ad.CITY
            , st.STATE_NAME
            , s.SITE_NAME
            , a.ACCOUNT_TYPE_ID
            , act.ENTITY_NAME
            , a.VENDOR_ID
            , v.VENDOR_NAME
            , r.COMMODITY_TYPE_ID
            , com.Commodity_Name
            , a.ACCOUNT_ID
            , a.ACCOUNT_NUMBER
            , a.Supplier_Account_Begin_Dt
            , a.Supplier_Account_End_Dt
            , a.NOT_MANAGED
        ORDER BY
            a.ACCOUNT_ID;

    END;


GO

GRANT EXECUTE ON  [dbo].[Account_Dtl_By_Account_Group_Id] TO [CBMSApplication]
GO
