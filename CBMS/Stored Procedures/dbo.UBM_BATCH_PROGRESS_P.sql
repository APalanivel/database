SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--exec dbo.UBM_BATCH_PROGRESS_P 173, 'Cass'
CREATE  PROCEDURE dbo.UBM_BATCH_PROGRESS_P
	@ubm_batch_master_log_id int,
	@ubm_name varchar(100),
	@progress varchar(4000) OUTPUT
	AS
	set nocount on
	DECLARE @EXPECTED_RECORDS INT,
	@ACTUAL_RECORDS INT,
	@NO_OF_INVOICES INT,
	@TOTAL_INVOICES INT

	if @ubm_name = 'Avista'
	begin
		select @EXPECTED_RECORDS = sum(total_expected_records) 
		from ubm_avista_control(nolock)  
		where ubm_batch_master_log_id = @ubm_batch_master_log_id
		
		IF @EXPECTED_RECORDS IS NULL OR @EXPECTED_RECORDS = 0
			BEGIN
				select @progress = 'Data feed insertion is started into Avista temp tables'
			END
		ELSE 
			BEGIN
		 		SELECT @NO_OF_INVOICES = COUNT(UBM_INVOICE_ID) FROM UBM_INVOICE(nolock) 
				WHERE ubm_batch_master_log_id = @ubm_batch_master_log_id
		
				IF @NO_OF_INVOICES IS NULL OR @NO_OF_INVOICES = 0
					BEGIN
		 				select @ACTUAL_RECORDS = count(bill_id)from ubm_avista_feed (nolock) 
						   where ubm_batch_master_log_id =@ubm_batch_master_log_id
						
						IF @ACTUAL_RECORDS = @EXPECTED_RECORDS
							BEGIN
								select @progress = 'Invoice images insertion is going on'
							END
						ELSE
							BEGIN
								select @progress = str(@ACTUAL_RECORDS)+'/'+str(@EXPECTED_RECORDS)+'records are inserted into temp tables'
							END
					END
				ELSE
					BEGIN
						select	@TOTAL_INVOICES = count(distinct feed.bill_id) 
						from 	ubm_avista_feed feed(nolock) 
						where 	feed.ubm_batch_master_log_id = @ubm_batch_master_log_id
						if @NO_OF_INVOICES > @TOTAL_INVOICES or @NO_OF_INVOICES = @TOTAL_INVOICES
							begin
								select @progress = 'Invoice process has been completed'
							end
						else
							begin
								select @progress = str(@NO_OF_INVOICES)+'/'+str(@TOTAL_INVOICES)+'invoices are processed'
							end
					END
			END
	end
	else if @ubm_name = 'Cass'
	begin

 		SELECT @NO_OF_INVOICES = COUNT(UBM_INVOICE_ID) FROM UBM_INVOICE(nolock) 
		WHERE ubm_batch_master_log_id = @ubm_batch_master_log_id

		IF @NO_OF_INVOICES IS NULL OR @NO_OF_INVOICES = 0
			BEGIN
				select @progress = 'Data feed insertion is in process into Cass temp tables'
			END
		ELSE 
			BEGIN
				select @TOTAL_INVOICES = count(util_bill_header_id) from ubm_cass_bill(nolock)
				where ubm_batch_master_log_id = @ubm_batch_master_log_id

				if @NO_OF_INVOICES > @TOTAL_INVOICES or @NO_OF_INVOICES = @TOTAL_INVOICES
					begin
						--select @progress = 'Invoice process has been completed'
						select @progress = str(@NO_OF_INVOICES)+'/'+str(@TOTAL_INVOICES)+'invoices are processed'
					end
				else
					begin
						select @progress = str(@NO_OF_INVOICES)+'/'+str(@TOTAL_INVOICES)+'invoices are processed'
					end

			END

	end
			
IF @progress is null
	select @progress = '-1'	

RETURN
GO
GRANT EXECUTE ON  [dbo].[UBM_BATCH_PROGRESS_P] TO [CBMSApplication]
GO
