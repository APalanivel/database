
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******  
NAME:  
 dbo.SR_SAD_GET_RETURN_TO_TARIFF_ACCOUNTS_P  
  
DESCRIPTION:  
  
  
INPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 @batch_execution_date DATETIME  
 @no_of_weeks          INT   
  
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
 exec SR_SAD_GET_SWITCH_SUPPLIER_ACCOUNTS_P '2012-11-01', 1  
                     
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 AKR   Ashok Kumar Raju  
  
  
MODIFICATIONS  
  
 Initials Date  Modification  
-----------------------------------------------------------  
 AKR   2012-11-15  Modified the script to use the Analyst Mapping Code  
                   Modified to Standards  
   
******/   

CREATE  PROCEDURE dbo.SR_SAD_GET_SWITCH_SUPPLIER_ACCOUNTS_P
      ( 
       @batch_execution_date DATETIME
      ,@no_of_weeks INT )
AS 
BEGIN
      SET NOCOUNT ON ;

      DECLARE
            @tariff_type_id INT
           ,@na_type_id INT
           ,@switch_supplier_type_id INT
           ,@duration_type VARCHAR(20)
           ,@Start_Date DATETIME
           ,@End_Date DATETIME
           ,@EP_commodity_type_id INT
           ,@NG_commodity_type_id INT
           ,@Default_Analyst INT
           ,@Custom_Analyst INT
           ,@Utility INT  
  
      SELECT
            @tariff_type_id = e.entity_id
      FROM
            dbo.ENTITY e
      WHERE
            e.entity_type = 1034
            AND e.entity_name = 'Returning to tariff'  
      SELECT
            @na_type_id = e.entity_id
      FROM
            dbo.ENTITY e
      WHERE
            e.entity_type = 1034
            AND e.entity_name = 'N/A'   
      SELECT
            @switch_supplier_type_id = e.entity_id
      FROM
            dbo.ENTITY e
      WHERE
            e.entity_type = 1027
            AND e.entity_name = 'Switching Suppliers'   
  

      SET @duration_type = case WHEN @no_of_weeks = 1 THEN 'Current Week'
                                ELSE '2nd Week'
                           END  
  
      SET @Start_Date = case WHEN @no_of_weeks = 1 THEN @batch_execution_date
                             ELSE dateadd(dd, ( 1 * 7 ), @batch_execution_date)
                        END  
      SET @End_Date = case WHEN @no_of_weeks = 1 THEN dateadd(dd, ( ( @no_of_weeks * 7 ) - 1 ), @batch_execution_date)
                           ELSE dateadd(dd, ( ( @no_of_weeks * 7 ) - 1 ), @batch_execution_date)
                      END  
                    
      SELECT
            @EP_commodity_type_id = max(case WHEN com.Commodity_Name = 'Electric Power' THEN com.Commodity_Id
                                        END)
           ,@NG_commodity_type_id = max(case WHEN com.Commodity_Name = 'Natural Gas' THEN com.Commodity_Id
                                        END)
      FROM
            dbo.Commodity com
      WHERE
            com.Commodity_Name IN ( 'Electric Power', 'Natural Gas' )    
        
      SELECT
            @Default_Analyst = max(case WHEN c.Code_Value = 'Default' THEN c.Code_Id
                                   END)
           ,@Custom_Analyst = max(case WHEN c.Code_Value = 'Custom' THEN c.Code_Id
                                  END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Analyst Type'  
            
            
      DECLARE @Account_List TABLE
            ( 
             Client_Name VARCHAR(200)
            ,Site_Name VARCHAR(500)
            ,Account_Id INT
            ,Account_Number VARCHAR(50)
            ,contract_id INT
            ,Analyst_Mapping_Cd INT
            ,Account_Vendor_Id INT
            ,Client_Id INT
            ,Site_Id INT
            ,utility_switch_supplier_date DATETIME
            ,Commodity_Type_Id INT )  
             
             
             
      INSERT      INTO @Account_List
                  ( 
                   Client_Name
                  ,Site_Name
                  ,Account_Id
                  ,Account_Number
                  ,contract_id
                  ,Analyst_Mapping_Cd
                  ,Account_Vendor_Id
                  ,Client_Id
                  ,Site_Id
                  ,utility_switch_supplier_date
                  ,Commodity_Type_Id )
                  SELECT
                        ch.Client_Name
                       ,ch.Site_name
                       ,utilacc.ACCOUNT_ID
                       ,utilacc.ACCOUNT_NUMBER
                       ,DBO.SR_SAD_FN_GET_CONTRACT_ID_OF_ACCOUNT(utilacc.account_id, convert(DATETIME, convert(VARCHAR(10), getdate(), 101))) AS contract_id
                       ,coalesce(utilacc.Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) Analyst_Mapping_Cd
                       ,utilacc.VENDOR_ID
                       ,ch.Client_Id
                       ,ch.Site_Id
                       ,switch.utility_switch_supplier_date
                       ,rfp.COMMODITY_TYPE_ID
                  FROM
                        dbo.SR_RFP_UTILITY_SWITCH switch
                        INNER JOIN dbo.SR_RFP_ACCOUNT rfp_account
                              ON rfp_account.sr_rfp_account_id = switch.sr_account_group_id
                        INNER JOIN dbo.SR_RFP rfp
                              ON rfp.sr_rfp_id = rfp_account.sr_rfp_id
                        INNER JOIN dbo.ACCOUNT utilacc
                              ON utilacc.account_id = rfp_account.account_id
                        INNER JOIN core.Client_Hier ch
                              ON utilacc.SITE_ID = ch.Site_Id
                  WHERE
                        switch.utility_switch_supplier_date IS NOT NULL
                        AND switch.utility_switch_supplier_date BETWEEN @Start_Date
                                                                AND     @End_Date
                        AND ( switch.return_to_tariff_type_id != @tariff_type_id
                              OR switch.return_to_tariff_type_id != @na_type_id
                              OR switch.return_to_tariff_type_id IS NULL )
                        AND ( switch.utility_switch_supplier_type_id = @switch_supplier_type_id
                              OR switch.utility_switch_supplier_type_id IS NULL )
                        AND switch.change_notice_image_id IS NULL
                        AND switch.is_bid_group = 0
                        AND rfp_account.is_deleted = 0
                        AND rfp.COMMODITY_TYPE_ID IN ( @EP_commodity_type_id, @NG_commodity_type_id )
                  GROUP BY
                        ch.client_name
                       ,ch.city
                       ,ch.State_Name
                       ,ch.Site_Name
                       ,utilacc.account_id
                       ,utilacc.account_number
                       ,utilacc.Analyst_Mapping_Cd
                       ,ch.Site_Analyst_Mapping_Cd
                       ,ch.Client_Analyst_Mapping_Cd
                       ,utilacc.VENDOR_ID
                       ,ch.Client_Id
                       ,ch.Site_Id
                       ,switch.utility_switch_supplier_date
                       ,rfp.COMMODITY_TYPE_ID ;
      WITH  cte_Account_User
              AS ( SELECT
                        acc.Account_Vendor_Id
                       ,acc.client_name
                       ,acc.site_name
                       ,acc.account_id
                       ,acc.account_number
                       ,acc.contract_id
                       ,acc.utility_switch_supplier_date
                       ,case WHEN acc.Analyst_Mapping_Cd = @Default_Analyst THEN convert(VARCHAR, ui.user_info_id) + ':' + ui.first_name + ':' + ui.email_address
                             WHEN acc.Analyst_Mapping_Cd = @Custom_Analyst THEN coalesce(convert(VARCHAR, aui.user_info_id) + ':' + aui.first_name + ':' + aui.email_address, convert(VARCHAR, sui.user_info_id) + ':' + sui.first_name + ':' + sui.email_address, convert(VARCHAR, cui.user_info_id) + ':' + cui.first_name + ':' + cui.email_address)
                        END AS sa_email_info
                   FROM
                        @Account_List acc
                        INNER JOIN dbo.VENDOR_COMMODITY_MAP vcm
                              ON acc.Account_Vendor_Id = vcm.VENDOR_ID
                                 AND acc.COMMODITY_TYPE_ID = vcm.COMMODITY_TYPE_ID
                        INNER JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                              ON vcm.VENDOR_COMMODITY_MAP_ID = vcam.Vendor_Commodity_Map_Id
                        LEFT JOIN dbo.USER_INFO ui
                              ON vcam.Analyst_Id = ui.USER_INFO_ID
                        INNER JOIN core.Client_Commodity ccc
                              ON ccc.Client_Id = acc.Client_Id
                                 AND ccc.Commodity_Id = acc.COMMODITY_TYPE_ID
                        LEFT JOIN dbo.Account_Commodity_Analyst aca
                              ON aca.Account_Id = acc.Account_Id
                                 AND aca.Commodity_Id = acc.COMMODITY_TYPE_ID
                        LEFT JOIN dbo.USER_INFO aui
                              ON aca.Analyst_User_Info_Id = aui.USER_INFO_ID
                        LEFT JOIN dbo.SITE_Commodity_Analyst sca
                              ON sca.Site_Id = acc.Site_Id
                                 AND sca.Commodity_Id = acc.COMMODITY_TYPE_ID
                        LEFT JOIN dbo.USER_INFO sui
                              ON sca.Analyst_User_Info_Id = sui.USER_INFO_ID
                        LEFT JOIN dbo.Client_Commodity_Analyst cca
                              ON ccc.Client_Commodity_Id = cca.Client_Commodity_Id
                        LEFT JOIN dbo.USER_INFO cui
                              ON cca.Analyst_User_Info_Id = cui.USER_INFO_ID)
            SELECT
                  cau.Account_Vendor_Id Vendor_Id
                 ,cau.account_id
                 ,cau.account_number
                 ,cau.contract_id
                 ,cau.utility_switch_supplier_date
                 ,cau.site_name
                 ,cau.client_name
                 ,cau.sa_email_info
                 ,@duration_type AS duration_type
            FROM
                  cte_Account_User cau
            GROUP BY
                  cau.Account_Vendor_Id
                 ,cau.client_name
                 ,cau.site_name
                 ,cau.account_id
                 ,cau.account_number
                 ,cau.contract_id
                 ,cau.utility_switch_supplier_date
                 ,cau.sa_email_info  
        
END
;
GO

GRANT EXECUTE ON  [dbo].[SR_SAD_GET_SWITCH_SUPPLIER_ACCOUNTS_P] TO [CBMSApplication]
GO
