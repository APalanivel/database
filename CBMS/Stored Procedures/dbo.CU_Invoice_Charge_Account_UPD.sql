SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********       
NAME:  dbo.CU_Invoice_Charge_Account_UPD 
     
DESCRIPTION:  
	This object is used update the existing record during stage-3 process for CU_INVOICE_CHARGE_ACCOUNT
    
INPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
	@Account_Id			INT
    @CU_Invoice_Id		INT  
        
        
OUTPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:   
    
	EXEC dbo.CU_Invoice_Charge_Account_UPD 446513, 6953750
    EXEC dbo.CU_Invoice_Charge_Account_UPD 446513, 6957572

	
 
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
SKA		Shobhit Kumar Agrawal            
HG		Harihara Suthan G    

Initials Date			Modification
------------------------------------------------------------
SKA		 06/07/2010		Created
HG		 06/15/2011		MAINT-658 fixed the code to update the account_id only for the charge commodity mapped with the Account.
******/      

CREATE PROCEDURE dbo.CU_Invoice_Charge_Account_UPD
      @Account_Id		AS INT
     ,@CU_Invoice_Id	AS INT
AS
BEGIN

      SET  NOCOUNT ON

      DECLARE @Not_Applicable_Commodity_Id INT
      
      SELECT
            @Not_Applicable_Commodity_Id = Commodity_id
      FROM
            dbo.Commodity
      WHERE
            Commodity_Name = 'Not Applicable'

      UPDATE
            cica
      SET
            cica.ACCOUNT_ID = @Account_Id
      FROM
            dbo.CU_INVOICE_CHARGE cic
            INNER JOIN dbo.CU_INVOICE_CHARGE_ACCOUNT cica ON cica.CU_INVOICE_CHARGE_ID = cic.CU_INVOICE_CHARGE_ID
      WHERE
            cic.CU_INVOICE_ID = @CU_Invoice_Id
            AND ( cic.COMMODITY_TYPE_ID = @Not_Applicable_Commodity_Id	-- Taxes normally saved with Commodity_type_id -1 as it is common to all the services of the invoices
                  OR EXISTS ( SELECT
                                    1
                              FROM
                                    core.Client_Hier_Account cha
                              WHERE
                                    cha.Account_id = @Account_Id
                                    AND cha.Commodity_Id = cic.Commodity_Type_id ) )

END
GO
GRANT EXECUTE ON  [dbo].[CU_Invoice_Charge_Account_UPD] TO [CBMSApplication]
GO