SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/******  
NAME:  
 dbo.Report_Contract_Meter_Volume_details  
   
DESCRIPTION:  
 This Procedure is to get all Contract Meter Volumes  
   
INPUT PARAMETERS:  
Name    DataType  Default Description  
-------------------------------------------------------------------------------------------  
@Client_id_List  VARCHAR(MAX)  
@division_id_List VARCHAR(MAX)  
@country_id_List VARCHAR(MAX)  
@region_Id_List  VARCHAR(MAX)
@ContractExpiration DATETIME 
@Commodity_id  INT  
@utility_Id_List VARCHAR(MAX)  
@supplier_Id_List VARCHAR(MAX)  
@Contract_type_List VARCHAR(100)  
@Account_status  INT  
@Site_Status  INT  
@Default_UOM_Id  INT  
@frequency_type  VARCHAR(200)  
  
  
OUTPUT PARAMETERS:  
Name   DataType Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
-------------------------------------------------------------  
Report_Contract_Meter_Volume_details '201','62','4','5','02/01/2011',291,'534','822','153',0,0,25,'Monthly'    
Report_Contract_Meter_Volume_details '201','62',4,5,'02/01/2011',291,534,822,'153',0,0,25,'Daily'    
Report_Contract_Meter_Volume_details 10003,513,4,3,'12/01/2011',291,63,663,153,0,0,25,'Monthly'    
Report_Contract_Meter_Volume_details 10003,514,4,4,'2012-07-31',291,1073,822,153,0,0,25,'Monthly'   
   
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
SSR		Sharad srivastava  
GK		Gopi Konga
CPE		Chaitanya Panduga Eshwar
 MODIFICATIONS Initials Date  Modification  
------------------------------------------------------------  
 SSR			03/10/2011		Created  
 GK				05/10/2011		REPTMGR-18: Removed the parameters @from_Date and @to_Date and added added parameter @ContractExpiration
 CPE			05/11/2011		REPTMGR-19: Added extra filter on JOIN with Contract_Meter_Volume to avoid duplicates.
 CPE			05/23/2011		REPTMGR-20:	Meter_Id replaced with Meter_Number
 CPE			11/09/2011		Added a filter on Account_Type = 'Utility' to avoid Supplier accounts associated to the meter
*/  
CREATE PROC dbo.Report_Contract_Meter_Volume_details
      @Client_id_List VARCHAR(MAX)
     ,@division_id_List VARCHAR(MAX)
     ,@country_id_List VARCHAR(MAX)
     ,@region_Id_List VARCHAR(MAX)
     ,@ContractExpiration DATETIME
     ,@Commodity_id INT
     ,@utility_Id_List VARCHAR(MAX)
     ,@supplier_Id_List VARCHAR(MAX)
     ,@Contract_type_List VARCHAR(100)
     ,@Account_status INT
     ,@Site_Status INT
     ,@Default_UOM_Id INT
     ,@frequency_type VARCHAR(200)
AS 
BEGIN    
        
      SET NOCOUNT ON       
    
      SELECT
            ch.Client_Name
           ,ch.Sitegroup_Name
           ,sit.Site_name
           ,S_type.ENTITY_NAME [Site Type]
           ,sit.Site_Id
           ,CASE WHEN sit.NOT_MANAGED = 1 THEN 'No'
                 ELSE 'Yes'
            END AS Site_status
           ,CASE WHEN sit.CLOSED = 1 THEN 'Yes'
                 ELSE 'No'
            END CLOSED
           ,ch.Site_Address_Line1
           ,ch.City
           ,ch.State_Name
           ,ch.Country_Name
           ,ch.Region_Name
           ,ca.Account_Vendor_Name
           ,ca.Account_Number
           ,CASE WHEN ca.Account_Not_Managed = 1 THEN 'No'
                 ELSE 'Yes'
            END AS account_status
           ,ca.Meter_Number
           ,supplier.VENDOR_NAME [supplier Vendor Name]
           ,con.ED_CONTRACT_NUMBER [Contract Number]
           ,con.CONTRACT_START_DATE
           ,con.CONTRACT_END_DATE
           ,con.CONTRACT_PRICING_SUMMARY
           ,cmv.MONTH_IDENTIFIER
           ,cmv.VOLUME
           ,cm.Commodity_Name
           ,ent_uofm.ENTITY_NAME [Unit of measurement]
           ,freq_type.ENTITY_NAME [Frequency]
           ,[Converted Volume (SelectedUofM/SelectedFrequency)] = CASE WHEN freq_type.entity_name = 'Daily'
                                                                            AND @frequency_type = 'Monthly' THEN ( ( cmv.VOLUME * dd.Days_In_Month_Num ) * cuc.CONVERSION_FACTOR )
                                                                       WHEN freq_type.entity_name IN ( 'Month', '30-day Cycle' )
                                                                            AND @frequency_type = 'Daily' THEN ( ( cmv.VOLUME / dd.Days_In_Month_Num ) * cuc.CONVERSION_FACTOR )
                                                                       WHEN freq_type.entity_name = CASE WHEN @frequency_type = 'Monthly' THEN 'Month'
                                                                                                         ELSE @frequency_type
                                                                                                    END THEN ( cmv.VOLUME * cuc.CONVERSION_FACTOR )
                                                                       ELSE NULL
                                                                  END
      FROM
            dbo.CONTRACT con
            JOIN dbo.supplier_account_meter_map map
                  ON map.contract_id = con.contract_id
            JOIN dbo.ACCOUNT suppacc
                  ON suppacc.account_id = map.account_id
            JOIN dbo.vendor supplier
                  ON supplier.vendor_id = suppacc.vendor_id
            JOIN Core.Client_Hier_Account ca
                  ON ca.Meter_Id = map.METER_ID
                  AND ca.Account_Type = 'Utility'
            JOIN Core.Client_Hier ch
                  ON ch.Client_Hier_Id = ca.Client_Hier_Id
            JOIN dbo.SITE sit
                  ON sit.SITE_ID = ch.Site_Id
            JOIN dbo.Commodity cm
                  ON cm.Commodity_Id = con.COMMODITY_TYPE_ID
            JOIN dbo.entity ventype
                  ON ventype.entity_id = supplier.vendor_type_id
            JOIN dbo.ENTITY S_type
                  ON S_type.ENTITY_ID = sit.SITE_TYPE_ID
            JOIN dbo.ufn_split(@country_id_List, ',') ufn_Country_tmp
                  ON CAST(ufn_Country_tmp.Segments AS INT) = ch.Country_id
            JOIN dbo.ufn_split(@region_Id_List, ',') ufn_Region_tmp
                  ON CAST(ufn_Region_tmp.Segments AS INT) = ch.Region_id
            JOIN dbo.ufn_split(@Client_id_List, ',') ufn_Client_tmp
                  ON CAST(ufn_Client_tmp.Segments AS INT) = ch.CLIENT_ID
            JOIN dbo.ufn_split(@utility_Id_List, ',') ufn_utility_tmp
                  ON CAST(ufn_utility_tmp.Segments AS INT) = ca.Account_Vendor_Id
            JOIN dbo.ufn_split(@Contract_type_List, ',') ufn_Contract_tmp
                  ON CAST(ufn_Contract_tmp.Segments AS INT) = con.CONTRACT_TYPE_ID
            JOIN dbo.ufn_split(@supplier_Id_List, ',') ufn_supplier_tmp
                  ON CAST(ufn_supplier_tmp.Segments AS INT) = supplier.VENDOR_ID
            JOIN dbo.ufn_split(@division_id_List, ',') ufn_division_tmp
                  ON CAST(ufn_division_tmp.Segments AS INT) = sit.DIVISION_ID
            LEFT JOIN dbo.CONTRACT_METER_VOLUME cmv
                  ON cmv.CONTRACT_ID = con.CONTRACT_ID
                  AND cmv.METER_ID = ca.Meter_Id
            LEFT JOIN dbo.ENTITY freq_type
                  ON freq_type.ENTITY_ID = cmv.FREQUENCY_TYPE_ID
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                  ON cuc.BASE_UNIT_ID = cmv.UNIT_TYPE_ID
                     AND cuc.CONVERTED_UNIT_ID = @Default_UOM_Id
            LEFT JOIN dbo.ENTITY ent_uofm
                  ON ent_uofm.ENTITY_ID = cmv.UNIT_TYPE_ID
                     AND ent_uofm.ENTITY_DESCRIPTION IN ( 'Unit for Gas', 'Unit for electricity' )
            LEFT JOIN meta.Date_Dim dd
                  ON dd.Date_D = cmv.MONTH_IDENTIFIER
      WHERE
            map.is_history <> 1
            And con.contract_end_date >= @ContractExpiration
            AND con.COMMODITY_TYPE_ID = @Commodity_id
            AND ( ( @Account_Status = -1 )
                  OR ( ca.Account_Not_Managed = @Account_Status ) )
            AND ( ( @Site_Status = -1 )
                  OR ( ch.Site_Not_Managed = @Site_Status ) )
      GROUP BY
            ch.Client_Name
           ,ch.Sitegroup_Name
           ,sit.Site_name
           ,S_type.ENTITY_NAME
           ,sit.Site_Id
           ,sit.NOT_MANAGED
           ,sit.CLOSED
           ,ch.Site_Address_Line1
           ,ch.City
           ,ch.State_Name
           ,ch.Country_Name
           ,ch.Region_Name
           ,ca.Account_Vendor_Name
           ,ca.Account_Number
           ,ca.Account_Not_Managed
           ,ca.Meter_Number
           ,supplier.VENDOR_NAME
           ,con.ED_CONTRACT_NUMBER
           ,con.CONTRACT_START_DATE
           ,con.CONTRACT_END_DATE
           ,con.CONTRACT_PRICING_SUMMARY
           ,cmv.MONTH_IDENTIFIER
           ,cmv.VOLUME
           ,cm.Commodity_Name
           ,ent_uofm.ENTITY_NAME
           ,freq_type.ENTITY_NAME
           ,cuc.CONVERSION_FACTOR
           ,dd.Days_In_Month_Num    
END


GO
GRANT EXECUTE ON  [dbo].[Report_Contract_Meter_Volume_details] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_Contract_Meter_Volume_details] TO [CBMSApplication]
GO
