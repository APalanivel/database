SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_USER_NAME_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userInfoId    	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE dbo.SR_SAD_GET_USER_NAME_P
@userInfoId int

AS
set nocount on

select 	userInfo.user_info_id ,	
	userInfo.FIRST_NAME+' '+userInfo.LAST_NAME USER_INFO_NAME

from 	
	user_info userInfo

where 
	userInfo.USER_INFO_ID = @userInfoId
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_USER_NAME_P] TO [CBMSApplication]
GO
