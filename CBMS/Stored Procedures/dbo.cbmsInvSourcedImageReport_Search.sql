SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsInvSourcedImageReport_Search

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@start_date    	datetime  	null      	
	@end_date      	datetime  	null      	
	@status_type_id	int       	null      	
	@inv_source_id 	int       	null      	
	@original_filename	varchar(200)	null      	
	@inv_source_keyword	varchar(1000)	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE          PROCEDURE [dbo].[cbmsInvSourcedImageReport_Search]
	( @MyAccountId int
	, @start_date datetime = null
	, @end_date datetime = null
	, @status_type_id int = null
	, @inv_source_id int = null
	, @original_filename varchar(200) = null
	, @inv_source_keyword varchar(1000) = null
	)
AS
BEGIN
	declare @Where varchar(2000)

	set nocount on

	if @start_date is null set @start_date = '1/1/2005'
	if @end_date is null set @end_date = '1/1/' + convert(varchar, year(getdate()) + 1)

	print convert(varchar, @start_date, 101)
	print @end_date


	set @Where = + 'where (sent_date between ''' + convert(varchar, @start_date, 101) + ''' and ''' + convert(varchar, @end_date, 101) + ''' or received_date between ''' + convert(varchar, @start_date, 101) + ''' and ''' + convert(varchar, @end_date, 101) + ''')'

	if @status_type_id is not null
	begin
		set @Where = @Where + ' and status_type_id = ' + convert(varchar, @status_type_id)
	end

	if @inv_source_id is not null
	begin
		set @Where = @Where + ' and inv_source_id = ' + convert(varchar, @inv_source_id)
	end

	if @original_filename is not null
	begin
		set @original_filename = replace(@original_filename, '''', '''''')
		set @Where = @Where + ' and original_filename like ''%' + @original_filename + '%'''
	end

	if @inv_source_keyword is not null
	begin
		set @inv_source_keyword = replace(@inv_source_keyword, '''', '''''')
		set @Where = @Where + ' and inv_source_keyword like ''%' + @inv_source_keyword + '%'''
	end

	print @where

--	set nocount off

	exec(	'   select inv_sourced_image_report_id ' +
		'	, inv_sourced_image_track_id ' +
		'	, inv_sourced_image_id ' +
		'	, inv_source_id ' +
		'	, inv_source_label ' +
		'	, inv_source_keyword ' +
		'	, original_filename ' +
		'	, prokarma_filename ' +
		'	, archive_path ' +
		'	, sent_date ' +
		'	, received_date ' +
		'	, status_type ' +
		'	, account_number ' +
		'	, cbms_image_id ' +
		'	, ubm_batch_master_log_id ' +
		'	, exception_comments ' +
		'     from inv_sourced_image_report with (nolock) ' + @Where
		)


END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvSourcedImageReport_Search] TO [CBMSApplication]
GO
