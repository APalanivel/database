SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.DELETE_CHARGE_MASTER_SIGN_MAP_P
	@CHARGE_MASTER_ID INT
AS
BEGIN

	SET NOCOUNT ON

	DELETE 
	FROM dbo.CHARGE_MASTER_SIGN_MAP  
	WHERE CHARGE_MASTER_ID = @CHARGE_MASTER_ID 

END
GO
GRANT EXECUTE ON  [dbo].[DELETE_CHARGE_MASTER_SIGN_MAP_P] TO [CBMSApplication]
GO
