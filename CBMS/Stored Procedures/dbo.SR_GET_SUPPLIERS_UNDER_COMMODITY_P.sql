SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--exec SR_GET_SUPPLIERS_UNDER_COMMODITY_P 290
--SELECT * FROM ENTITY WHERE ENTITY_ID=290

CREATE    PROCEDURE dbo.SR_GET_SUPPLIERS_UNDER_COMMODITY_P
	@commodityId  varchar(5)
AS
	set nocount on
		select distinct v.vendor_id,
	v.vendor_name

	from entity e,
	vendor v,
	vendor_commodity_map vcm
	where
	e.entity_id = v.vendor_type_id 
	and e.entity_name='Supplier'
	--and e.entity_type=115
	and vcm.vendor_id=v.vendor_id
	and vcm.commodity_type_id = @commodityId


	order by v.vendor_name
--select * from vendor_commodity_map
GO
GRANT EXECUTE ON  [dbo].[SR_GET_SUPPLIERS_UNDER_COMMODITY_P] TO [CBMSApplication]
GO
