
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                
/******                        
 NAME: dbo.[cbmsCuInvoiceComment_GetForCuInvoice]            
                        
 DESCRIPTION:                        
			To get cu_invoice_comment details.                        
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
 @Client_Id                  INT              
 @Group_Info_Id              VARCHAR(MAX)       
 @Assigned_User_Id           INT                      
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 EXEC [dbo].[cbmsCuInvoiceComment_GetForCuInvoice] 
      @MyAccountId = 49
     ,@cu_invoice_id = 2754
          
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2014-04-28       Header Added.For Project Operations Enhancement 4.1.4,Added new column User_Info_Name  in Select List              
                       
******/ 

CREATE  PROCEDURE [dbo].[cbmsCuInvoiceComment_GetForCuInvoice]
      ( 
       @MyAccountId INT
      ,@cu_invoice_id INT )
AS 
BEGIN

      SELECT
            com.cu_invoice_comment_id
           ,com.cu_invoice_id
           ,com.comment_by_id
           ,ui.username
           ,com.comment_date
           ,com.image_comment
           ,ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME AS User_Info_Name
      FROM
            cu_invoice_comment com
            JOIN user_info ui
                  ON ui.user_info_id = com.comment_by_id
      WHERE
            com.cu_invoice_id = @cu_invoice_id
      ORDER BY
            com.comment_date

END

;
GO

GRANT EXECUTE ON  [dbo].[cbmsCuInvoiceComment_GetForCuInvoice] TO [CBMSApplication]
GO
