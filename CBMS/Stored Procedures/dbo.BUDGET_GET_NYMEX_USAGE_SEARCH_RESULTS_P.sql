SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO











-- exec BUDGET_GET_NYMEX_USAGE_SEARCH_RESULTS_P -1,-1,123,0,0,25,'01/01/2005','12/31/2005',1
-- exec BUDGET_GET_NYMEX_USAGE_SEARCH_RESULTS_P -1,-1,12020,0,102,25,'01/01/2008','12/31/2008',3
 
CREATE                  PROCEDURE [dbo].[BUDGET_GET_NYMEX_USAGE_SEARCH_RESULTS_P]
	@user_id varchar(10),
	@session_id varchar(20),	
	@clientId int,
	@divisionId int,
	@siteId int,		
	@unitTypeId int,
	@fromDate datetime,
	@toDate datetime
	
	

	
AS
begin
set nocount on

	declare @selectClause varchar(8000)
	declare @fromClause varchar(8000)
	declare @whereClause varchar(8000)	
	declare @SQLStatement varchar(8000)

	
select @selectClause = " site.site_id, "
			+" site.site_name as SiteName, "
			+" utilacc.account_id as accountId, "
			+" utilacc.account_number as accountNumber, "
			+" utility.vendor_name as utility_name, "
			+" bu.month_identifier, "
			+" bu.volume "

select @fromClause =" account utilacc
			join vwSiteName site on site.site_id = utilacc.site_id
			join client cli on cli.client_id = site.client_id
			join vendor utility on utility.vendor_id = utilacc.vendor_id
			left join budget_usage bu on
			bu.account_id = utilacc.account_id
			AND bu.month_identifier between '" + CONVERT(Varchar(12), @fromDate, 101) + "'
			AND '"+ CONVERT(Varchar(12), @toDate, 101)  + "'
			left join entity on entity.entity_id = bu.volume_unit_type_id
			and bu.volume_unit_type_id = "+ str(@unitTypeId)

		select @whereClause = " cli.client_id = "+str(@clientId)
			
			
		
	IF @divisionId > 0 
	BEGIN	
		select @whereClause = @whereClause + " and  site.division_id = " + str(@divisionId)		
	END 

	IF @siteId > 0 
	BEGIN
		select @whereClause = @whereClause + " and  site.site_id = " + str(@siteId) 		
	END
	
	
	

	
	select @SQLStatement =	"SELECT " 
				+ @selectClause  
				+ " FROM "  
				+ @fromClause 
				+ " WHERE " 
				+ @whereClause 
	

	--PRINT @SQLStatement

	EXEC(@SQLStatement)
	
end









GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_NYMEX_USAGE_SEARCH_RESULTS_P] TO [CBMSApplication]
GO
