SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
              
Name:     
 dbo.Account_Exception_Ins         
                
Description:                
   To insert Calc Val configuration  
                
 Input Parameters:                
 Name						DataType			Default					Description                  
---------------------------------------------------------------------------------------------  
 @Exception_Type_Cd			INT  
 @Account_Id				INT  
 @Meter_Id					INT  
 @User_Info_Id				INT  
      
Output Parameters:                      
 Name						DataType			Default					Description                  
---------------------------------------------------------------------------------------------  
                
Usage Examples:                    
---------------------------------------------------------------------------------------------  
  
 EXEC dbo.CODE_SEL_BY_CodeSet_Name @CodeSet_Name = 'Exception Type', @Code_Value ='Missing Meter Attribute' -->Exception Type  
  
 SELECT TOP 10 cha.Account_Id,cha.Meter_Id,cha.Meter_Number FROM core.Client_Hier_Account cha   
  WHERE cha.Account_Type = 'Utility' AND cha.Meter_Country_Name ='Belgium'  
  AND NOT EXISTS(SELECT 1 FROM dbo.Account_Exception ae WHERE ae.Account_Id=cha.Account_Id AND ae.Meter_Id = cha.Meter_Id)  
   
 BEGIN TRANSACTION  
  SELECT * FROM dbo.Account_Exception WHERE Account_Id =57050 AND Meter_Id =38814  
  EXEC dbo.Account_Exception_Ins  100995,38814,16  
  SELECT * FROM dbo.Account_Exception WHERE Account_Id =57050 AND Meter_Id =38814  
 ROLLBACK TRANSACTION  
   
 BEGIN TRANSACTION  
  SELECT * FROM dbo.Account_Exception WHERE Account_Id =1148199 AND Exception_Type_Cd = 102032--Meter_Id =38815  
  --EXEC dbo.Account_Exception_Ins  100995,38815,16  
  EXEC dbo.Account_Exception_Ins 102032,null,49,1148199,290
  SELECT * FROM dbo.Account_Exception WHERE Account_Id =1148199 AND Exception_Type_Cd = 102032--Meter_Id =38815  
 ROLLBACK TRANSACTION  
  
  
   BEGIN TRANSACTION  
  SELECT * FROM dbo.Account_Exception WHERE Account_Id =1590271 AND Exception_Type_Cd = 102756 and Meter_Id =495956  

  EXEC dbo.Account_Exception_Ins  
		@Exception_Type_Cd =102756
        , @Meter_Id  = 495956
        , @User_Info_Id =49
        , @Account_Id  = 1590271
        , @Commodity_Id  = -1
        
  SELECT * FROM dbo.Account_Exception WHERE Account_Id =1590271 AND Exception_Type_Cd = 102756 and Meter_Id =495956  
 ROLLBACK TRANSACTION  
 
 
EXEC dbo.Account_Exception_Ins
    @Exception_Type_Cd = 102759
    , @Meter_Id = 338054
    , @User_Info_Id = 49
    , @Account_Id = 1741077
    , @Commodity_Id = 290
	

  SELECT * FROM dbo.Account_Exception WHERE Account_Id =1741077

   
Author Initials:                
 Initials		Name                
---------------------------------------------------------------------------------------------  
 RR				Raghu Reddy    
 RKV			Ravi Kumar Vegesna   
 NR				Narayana Reddy          
   
Modifications:                
Initials		Date				Modification                
---------------------------------------------------------------------------------------------  
 RR				2015-05-13			Created For AS400.           
 RKV			2015-08-06			Added New Column Commodity_Id as Part Of AS400-II.
 NR				2019-05-30			Add Contract Project - Added New exceptions.   
 NR				2020-06-16			SE2017-981- Added Overlapping Sup Account exception. 
******/
CREATE  PROCEDURE [dbo].[Account_Exception_Ins]
      (
      @Exception_Type_Cd     INT
    , @Meter_Id              INT  = -1
    , @User_Info_Id          INT
    , @Account_Id            INT  = -1
    , @Commodity_Id          INT  = -1
    , @Invoice_Service_Month DATE = NULL
    , @Contract_Id           INT  = NULL
    , @Cu_Invoice_Id         INT  = NULL )
AS
      BEGIN
            SET NOCOUNT ON;

            DECLARE
                  @Exception_Status_Cd       INT
                , @Queue_User_Info_Id        INT
                , @Group_Legacy_Name_Cd      INT
                , @Is_Exception_Date_Overlap BIT = 0
           , @Client_Id                 INT
                , @Account_Exception_Id      INT;

            SELECT
                  @Meter_Id = -1
            WHERE @Meter_Id IS NULL;
            SELECT
                  @Contract_Id = -1
            WHERE @Contract_Id IS NULL;

            SELECT
                  @Account_Id = Account_Id
                , @Commodity_Id = Commodity_Id
            FROM  Core.Client_Hier_Account cha
            WHERE Meter_Id = @Meter_Id
                  AND   Account_Type = 'Utility'
                  AND   @Meter_Id <> -1
                  AND   @Account_Id = -1;



            SELECT
                  @Exception_Status_Cd = cd.Code_Id
            FROM  dbo.Code cd
                  JOIN
                  dbo.Codeset cs
                        ON cd.Codeset_Id = cs.Codeset_Id
            WHERE cs.Codeset_Name = 'Exception Status'
                  AND   cd.Code_Value = 'New';

            SELECT
                  @Group_Legacy_Name_Cd = cd.Code_Id
            FROM  dbo.Code cd
                  JOIN
                  dbo.Codeset cs
                        ON cd.Codeset_Id = cs.Codeset_Id
            WHERE cs.Codeset_Name = 'Group_Legacy_Name'
                  AND   cd.Code_Value = 'Standing Data Team';



            SELECT
                  @Is_Exception_Date_Overlap = 1
            FROM  dbo.Account_Exception ae
                  INNER JOIN
                  dbo.Code c
                        ON ae.Exception_Type_Cd = c.Code_Id
            WHERE ae.Account_Id = @Account_Id
                  AND   ae.Exception_Type_Cd = @Exception_Type_Cd
                  AND   c.Code_Value = 'Missing Contract'
                  AND   @Invoice_Service_Month
                  BETWEEN ae.Exception_Begin_Dt AND ae.Exception_End_Dt
                  AND   @Invoice_Service_Month IS NOT NULL;


            SELECT
                  @Client_Id = ch.Client_Id
            FROM  Core.Client_Hier ch
                  INNER JOIN
                  Core.Client_Hier_Account cha
                        ON cha.Client_Hier_Id = ch.Client_Hier_Id
            WHERE cha.Account_Id = @Account_Id;


            IF EXISTS
                  (     SELECT
                              1
                        FROM  dbo.Code c
                              INNER JOIN
                              dbo.Codeset cs
                                    ON cs.Codeset_Id = c.Codeset_Id
                        WHERE cs.Codeset_Name = 'Exception Type'
                              AND   cs.Std_Column_Name = 'Exception_Type_Cd'
                              AND   c.Code_Id = @Exception_Type_Cd
                              AND   c.Code_Value IN (
                                          'Missing IC Data'
                                        , 'Missing Meter Attribute'
                                        , 'Missing Recalc Type'))
                  BEGIN


                        SELECT
                              @Queue_User_Info_Id = isnull(ui.USER_INFO_ID, rmm.USER_INFO_ID)
                        FROM  Core.Client_Hier_Account cha
                              INNER JOIN
                              dbo.UTILITY_DETAIL ud
                                    ON ud.VENDOR_ID = cha.Account_Vendor_Id
                              LEFT JOIN
                              (dbo.Utility_Detail_Analyst_Map map
                               INNER JOIN
                               dbo.GROUP_INFO gi
                                     ON gi.GROUP_INFO_ID = map.Group_Info_ID
                               INNER JOIN
                               dbo.USER_INFO ui
                                     ON ui.USER_INFO_ID = map.Analyst_ID)
                                    ON map.Utility_Detail_ID = ud.UTILITY_DETAIL_ID
                                       AND gi.Group_Legacy_Name_Cd = @Group_Legacy_Name_Cd
                              LEFT OUTER JOIN
                              dbo.STATE st
                                    ON st.STATE_ID = cha.Meter_State_Id
                              LEFT OUTER JOIN
                              dbo.REGION reg
                                    ON reg.REGION_ID = st.REGION_ID
                              LEFT OUTER JOIN
                              dbo.REGION_MANAGER_MAP rmm
                                    ON rmm.REGION_ID = reg.REGION_ID
                        WHERE cha.Account_Id = @Account_Id
                              AND
                                    (     cha.Meter_Id = @Meter_Id
                                          OR    @Meter_Id = -1 );

                  END;



            IF EXISTS
                  (     SELECT
                              1
                        FROM  dbo.Code c
                              INNER JOIN
                              dbo.Codeset cs
                                    ON cs.Codeset_Id = c.Codeset_Id
                        WHERE cs.Codeset_Name = 'Exception Type'
                              AND   cs.Std_Column_Name = 'Exception_Type_Cd'
                              AND   c.Code_Id = @Exception_Type_Cd
                              AND   c.Code_Value = 'Missing Contract' )
                  BEGIN



                        SELECT
                              @Queue_User_Info_Id = ceam.Analyst_Id
                        FROM  dbo.Client_Exception_Analyst_Map ceam
                        WHERE ceam.Client_Id = @Client_Id
                              AND   ceam.Exception_Type_Cd = @Exception_Type_Cd;



                  END;


            IF EXISTS
                  (     SELECT
                              1
                        FROM  dbo.Code c
                              INNER JOIN
                              dbo.Codeset cs
                                    ON cs.Codeset_Id = c.Codeset_Id
                        WHERE cs.Codeset_Name = 'Exception Type'
                              AND   cs.Std_Column_Name = 'Exception_Type_Cd'
                              AND   c.Code_Id = @Exception_Type_Cd
                              AND   c.Code_Value IN ('Outside Contract Term','Overlapping Sup Account'))
                  BEGIN



                        SELECT
                              @Queue_User_Info_Id = ui.USER_INFO_ID
                        FROM  Core.Client_Hier_Account utlity_cha
                              INNER JOIN
                              Core.Client_Hier_Account Sup_cha
                                    ON utlity_cha.Client_Hier_Id = Sup_cha.Client_Hier_Id
                                       AND utlity_cha.Meter_Id = Sup_cha.Meter_Id
                              INNER JOIN
                              dbo.UTILITY_DETAIL ud
                                    ON ud.VENDOR_ID = utlity_cha.Account_Vendor_Id
                              LEFT JOIN
                              (dbo.Utility_Detail_Analyst_Map map
                               INNER JOIN
                               dbo.GROUP_INFO gi
                                     ON gi.GROUP_INFO_ID = map.Group_Info_ID
                               INNER JOIN
                               dbo.USER_INFO ui
                                     ON ui.USER_INFO_ID = map.Analyst_ID)
                                    ON map.Utility_Detail_ID = ud.UTILITY_DETAIL_ID
                                       AND gi.GROUP_NAME = 'IAV Analyst Team'
                        WHERE Sup_cha.Account_Id = @Account_Id
                              AND   Sup_cha.Account_Type = 'Supplier'
                              AND   utlity_cha.Account_Type = 'Utility';


                        SELECT
                              @Queue_User_Info_Id = ui.USER_INFO_ID
                        FROM  Core.Client_Hier_Account cha
                              INNER JOIN
                              dbo.UTILITY_DETAIL ud
                        ON ud.VENDOR_ID = cha.Account_Vendor_Id
                              LEFT JOIN
                              (dbo.Utility_Detail_Analyst_Map map
                               INNER JOIN
                               dbo.GROUP_INFO gi
                                     ON gi.GROUP_INFO_ID = map.Group_Info_ID
                               INNER JOIN
                               dbo.USER_INFO ui
                                     ON ui.USER_INFO_ID = map.Analyst_ID)
                                    ON map.Utility_Detail_ID = ud.UTILITY_DETAIL_ID
                                       AND gi.GROUP_NAME = 'IAV Analyst Team'
                        WHERE cha.Account_Type = 'Utility'
                              AND   cha.Account_Id = @Account_Id;


                  END;



            SELECT
                  @Queue_User_Info_Id = rmm.USER_INFO_ID
            FROM  Core.Client_Hier_Account cha
                  INNER JOIN
                  dbo.STATE st
                        ON st.STATE_ID = cha.Meter_State_Id
                  INNER JOIN
                  dbo.REGION reg
                        ON reg.REGION_ID = st.REGION_ID
                  INNER JOIN
                  dbo.REGION_MANAGER_MAP rmm
                        ON rmm.REGION_ID = reg.REGION_ID
            WHERE cha.Account_Id = @Account_Id
                  AND   @Queue_User_Info_Id IS NULL;

            INSERT INTO dbo.Account_Exception (
                                                    Account_Id
                                                  , Meter_Id
                                                  , Exception_Type_Cd
                                                  , Queue_Id
                                                  , Exception_Status_Cd
                                                  , Exception_By_User_Id
                                                  , Exception_Created_Ts
                                                  , Last_Change_Ts
                                                  , Updated_User_Id
                                                  , Commodity_Id
                                                  , Contract_Id
                                              )
                        SELECT
                              @Account_Id AS Account_Id
                            , @Meter_Id AS Meter_Id
                            , @Exception_Type_Cd AS Exception_Type_Cd
                            , ui.QUEUE_ID
                            , @Exception_Status_Cd AS Exception_Status_Cd
                            , @User_Info_Id AS Exception_By_User_Id
                            , getdate() AS Exception_Created_Ts
                            , getdate() AS Last_Change_Ts
                            , @User_Info_Id AS Updated_User_Id
                            , @Commodity_Id AS Commodity_Id
                            , isnull(@Contract_Id, -1)
                        FROM  dbo.USER_INFO ui
                        WHERE ui.USER_INFO_ID = @Queue_User_Info_Id
                              AND   @Is_Exception_Date_Overlap = 0
                              AND   NOT EXISTS
                              (     SELECT
                                          1
                                    FROM  dbo.Account_Exception ae
                                    WHERE ae.Account_Id = @Account_Id
                                          AND   ae.Exception_Type_Cd = @Exception_Type_Cd
                                          AND   ae.Exception_Status_Cd = @Exception_Status_Cd
                                          AND   isnull(ae.Contract_Id, -1) = isnull(@Contract_Id, -1));


            SELECT
                  @Account_Exception_Id = scope_identity();

            SELECT
                  @Account_Exception_Id = ae.Account_Exception_Id
            FROM  dbo.Account_Exception ae
            WHERE ae.Account_Id = @Account_Id
                  AND   ae.Exception_Type_Cd = @Exception_Type_Cd
                  AND   ae.Exception_Status_Cd = @Exception_Status_Cd
                  AND   isnull(ae.Contract_Id, -1) = isnull(@Contract_Id, -1)
                  AND   @Account_Exception_Id IS NULL;

            INSERT INTO dbo.Account_Exception_Cu_Invoice_Map (
                                                                   Account_Exception_Id
                                                                 , Cu_Invoice_Id
                                                                 , Status_Cd
                                                                 , Created_User_Id
                                                                 , Created_Ts
                                                                 , Updated_User_Id
                                                                 , Last_Change_Ts
                                                             )
                        SELECT
                              @Account_Exception_Id
                            , @Cu_Invoice_Id
                            , @Exception_Status_Cd
                            , @User_Info_Id
                            , getdate()
                            , @User_Info_Id
                            , getdate()
                        WHERE @Account_Exception_Id IS NOT NULL
                              AND   @Cu_Invoice_Id IS NOT NULL
                              AND   NOT EXISTS
                              (     SELECT
                                          1
                                    FROM  dbo.Account_Exception_Cu_Invoice_Map aec
                                    WHERE aec.Account_Exception_Id = @Account_Exception_Id
                                          AND   aec.Cu_Invoice_Id = @Cu_Invoice_Id );

      END;





















GO

GRANT EXECUTE ON  [dbo].[Account_Exception_Ins] TO [CBMSApplication]
GO
