SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[UPDATE_METER_VOLUME_P]
           
DESCRIPTION:             

			
INPUT PARAMETERS:            
	Name					DataType	Default		Description  
---------------------------------------------------------------------------------  
	


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	

		
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2020-05-09	Added header
							GRMUER-81 - Added new optional input @Is_Edited, update chnaged to merge
							GRMUER-128 - Modified merge update to update volume and frequency
	
******/
CREATE PROCEDURE [dbo].[UPDATE_METER_VOLUME_P]
    (
        @volume DECIMAL(32, 16)
        , @unit_type_id INT
        , @frequency_type_id INT
        , @contract_id INT
        , @meter_id INT
        , @month_identifier DATETIME
        , @Is_Edited BIT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        MERGE INTO dbo.CONTRACT_METER_VOLUME tgt
        USING
        (   SELECT
                @volume AS Volume
                , @contract_id AS Contract_id
                , @meter_id AS Meter_id
                , @month_identifier AS Month_identifier
                , ISNULL(@Is_Edited, 0) AS Is_Edited) src
        ON tgt.CONTRACT_ID = src.Contract_id
           AND  tgt.METER_ID = src.Meter_id
           AND  tgt.MONTH_IDENTIFIER = src.Month_identifier
        WHEN MATCHED THEN UPDATE SET
                              tgt.VOLUME = src.Volume
                              , tgt.Is_Edited = src.Is_Edited
                              , tgt.UNIT_TYPE_ID = @unit_type_id
                              , tgt.FREQUENCY_TYPE_ID = @frequency_type_id
        WHEN NOT MATCHED THEN INSERT (METER_ID
                                      , CONTRACT_ID
                                      , VOLUME
                                      , MONTH_IDENTIFIER
                                      , UNIT_TYPE_ID
                                      , FREQUENCY_TYPE_ID
                                      , Is_Edited)
                              VALUES
                                  (src.Meter_id
                                   , src.Contract_id
                                   , src.Volume
                                   , src.Month_identifier
                                   , @unit_type_id
                                   , @frequency_type_id
                                   , src.Is_Edited);

    --UPDATE
    --    dbo.CONTRACT_METER_VOLUME
    --SET
    --    VOLUME = @volume
    --    , UNIT_TYPE_ID = @unit_type_id
    --    , FREQUENCY_TYPE_ID = @frequency_type_id
    --    , Is_Edited = ISNULL(@Is_Edited, 0)
    --WHERE
    --    CONTRACT_ID = @contract_id
    --    AND METER_ID = @meter_id
    --    AND MONTH_IDENTIFIER = @month_identifier;

    END;

GO
GRANT EXECUTE ON  [dbo].[UPDATE_METER_VOLUME_P] TO [CBMSApplication]
GO
