SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Baseload_Details_Del]  

DESCRIPTION: It Deletes Baseload Details for Selected Baseload Detail Id.     
      
INPUT PARAMETERS:          
	NAME					DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Baseload_Details_Id	INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
	EXEC Baseload_Details_Del 2180
  Rollback Tran

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			17-JUN-10	CREATED

*/

CREATE PROCEDURE dbo.Baseload_Details_Del
    (
      @Baseload_Details_Id INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.BASELOAD_DETAILS
	WHERE
		BASELOAD_DETAILS_ID = @Baseload_Details_Id

END
GO
GRANT EXECUTE ON  [dbo].[Baseload_Details_Del] TO [CBMSApplication]
GO
