
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME: [DBO].[Get_Reported_Cu_Invoice_Begin_Dt_End_Dt_Service_Month_Cnt_By_Account_Id]

DESCRIPTION:
	To get the mininum begin date , maximum end date and distinct service month count of invoices based on the given input.
	Used in CU aggregation page.

INPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
@Account_Id	INT
@Service_Month	DATE

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
	EXEC dbo.Get_Reported_Cu_Invoice_Begin_Dt_End_Dt_Service_Month_Cnt_By_Account_Id 73231,3075909,'1/1/2008'
	EXEC dbo.Get_Reported_Cu_Invoice_Begin_Dt_End_Dt_Service_Month_Cnt_By_Account_Id 73231,3075909,'2/1/2008',0
	EXEC dbo.Get_Reported_Cu_Invoice_Begin_Dt_End_Dt_Service_Month_Cnt_By_Account_Id 73231,0,'2/1/2008',0
	EXEC dbo.Get_Reported_Cu_Invoice_Begin_Dt_End_Dt_Service_Month_Cnt_By_Account_Id 468655, 10282158, '2012-12-01'

	SELECT * FROM cu_Invoice_Service_Month WHERE Account_id = 73231

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------          
HG			Harihara Suthan G
RR			Raghu Reddy

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------          
HG			08/09/2011  Created for Additional data requirement
RR			2013-09-17	MAINT-2136 The script output data used by the application for aggregation and input invoice is including
						 always, but it should not. Added @Include_Current_Invoice parameter to determine, it is 1(true) if the invoice 
						 is new, 0(false) if the invoice is backed off


*/
CREATE PROCEDURE dbo.Get_Reported_Cu_Invoice_Begin_Dt_End_Dt_Service_Month_Cnt_By_Account_Id
      @Account_Id INT
     ,@Cu_Invoice_Id INT
     ,@Service_Month DATE
     ,@Include_Current_Invoice BIT = 1
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE
            @Min_Begin_Dt DATE
           ,@Max_End_Dt DATE
           ,@Min_Service_Month DATE
           ,@Max_Service_Month DATE  
  
      SELECT
            @Min_Begin_Dt = min(sm.Begin_Dt)
           ,@Max_End_Dt = max(sm.End_Dt)
           ,@Min_Service_Month = min(sm1.SERVICE_MONTH)
           ,@Max_Service_Month = max(sm1.SERVICE_MONTH)
      FROM
            dbo.CU_INVOICE_SERVICE_MONTH sm
            INNER JOIN dbo.Cu_Invoice i
                  ON i.Cu_Invoice_Id = sm.Cu_Invoice_Id
            INNER JOIN dbo.CU_Invoice_Service_Month sm1
                  ON sm1.Cu_Invoice_Id = sm.Cu_Invoice_Id
                     AND sm1.Account_ID = sm.Account_ID
      WHERE
            sm.Account_id = @Account_id
            AND @Service_Month BETWEEN sm.Service_Month
                               AND     sm.Service_Month
            AND ( ( @Include_Current_Invoice = 1
                    AND ( i.Cu_Invoice_Id = @Cu_Invoice_id
                          OR i.Is_Reported = 1 ) )
                  OR ( @Include_Current_Invoice = 0
                       AND i.Cu_Invoice_Id != @Cu_Invoice_id
                       AND i.Is_Reported = 1 ) )

   
    
      SELECT
            @Min_Begin_Dt AS Min_Begin_Dt
           ,@Max_End_Dt AS Max_End_Dt
           ,case WHEN @Max_Service_Month < @Min_Service_Month THEN 1
                 ELSE count(DISTINCT sm.Service_Month)
            END AS Service_Month_Cnt
      FROM
            dbo.CU_INVOICE_SERVICE_MONTH sm
            INNER JOIN dbo.Cu_Invoice i
                  ON i.Cu_Invoice_Id = sm.Cu_Invoice_Id
      WHERE
            sm.Account_id = @Account_id
            AND sm.Service_Month BETWEEN @Min_Service_Month
                                 AND     @Max_Service_Month
            AND ( @Include_Current_Invoice = 1
                  AND ( ( i.Cu_Invoice_Id = @Cu_Invoice_id
                        OR i.Is_Reported = 1 ) )
                  OR ( @Include_Current_Invoice = 0
                       AND i.Cu_Invoice_Id != @Cu_Invoice_id
                       AND i.Is_Reported = 1 ) )


END;
GO

GRANT EXECUTE ON  [dbo].[Get_Reported_Cu_Invoice_Begin_Dt_End_Dt_Service_Month_Cnt_By_Account_Id] TO [CBMSApplication]
GO
