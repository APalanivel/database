SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE  PROCEDURE [dbo].[cbmsInvoiceParticipation_GetAllSiteMonthsForSite]
	( @MyAccountId int
	, @site_id int
	)
AS
BEGIN


   select distinct ip.site_id
	, ip.service_month
     from invoice_participation ip
    where ip.site_id = @site_id

END



GO
GRANT EXECUTE ON  [dbo].[cbmsInvoiceParticipation_GetAllSiteMonthsForSite] TO [CBMSApplication]
GO
