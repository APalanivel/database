SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 /******      
                         
 NAME: dbo.Category_Bucket_Aggregation_Rule_Sel_By_Country_State_Aggregation_Level                     
                          
 DESCRIPTION:      
		  To get the formula details for the given state and aggrigation level.                    
                          
 INPUT PARAMETERS:      
                         
 Name                               DataType          Default       Description      
-----------------------------------------------------------------------------------    
  @Country_Id						INT 
  @State_Id							INT
  @Aggregation_Level				VARCHAR(25)
                                
 OUTPUT PARAMETERS:      
                               
 Name                               DataType          Default       Description      
-----------------------------------------------------------------------------------    
                          
 USAGE EXAMPLES:                              
-----------------------------------------------------------------------------------    
               
 EXEC dbo.Category_Bucket_Aggregation_Rule_Sel_By_Country_State_Aggregation_Level 27,124,'Invoice'   
	
			                
                         
 AUTHOR INITIALS:    
       
 Initials                   Name      
-----------------------------------------------------------------------------------    
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
-----------------------------------------------------------------------------------    
 NR                     2016-10-04      Created for MAINT-4352.                        
                         
******/ 

 CREATE PROCEDURE [dbo].[Category_Bucket_Aggregation_Rule_Sel_By_Country_State_Aggregation_Level]
      ( 
       @Country_Id INT
      ,@State_Id INT
      ,@Aggregation_Level VARCHAR(25) )
 AS 
 BEGIN 
      SET NOCOUNT ON 


      SELECT
            Country_State_Category_Bucket_Aggregation_Rule_Id
           ,Category_Bucket_Master_Id
           ,Country_Id
           ,State_Id
           ,Priority_Order
           ,CU_Aggregation_Level_Cd
           ,Numerator_Formula
           ,Is_Aggregate_Category_Bucket
           ,Is_Aggregate_When_All_Component_Is_Non_Zero
      FROM
            dbo.Country_State_Category_Bucket_Aggregation_Rule cscbar
            INNER JOIN dbo.Code c
                  ON c.Code_Id = cscbar.CU_Aggregation_Level_Cd
      WHERE
            cscbar.Country_Id = @Country_Id
            AND cscbar.State_Id = @State_Id
            AND c.Code_Value = @Aggregation_Level
      
 END

;
GO
GRANT EXECUTE ON  [dbo].[Category_Bucket_Aggregation_Rule_Sel_By_Country_State_Aggregation_Level] TO [CBMSApplication]
GO
