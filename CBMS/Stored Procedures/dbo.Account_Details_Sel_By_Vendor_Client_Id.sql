
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 CBMS.dbo.Account_Details_Sel_By_Vendor_Id    
    
DESCRIPTION:    
    
 Used to Select all Account details associated with the given vendor_Id     
     
    
INPUT PARAMETERS:    
 Name					DataType	 Default		Description    
--------------------------------------------------------------------    
 @Vendor_Id				INT			 NULL
 @Client_Id				INT			 NULL
 @Site_Id				INT			 NULL
 @Commodity_Id			INT			 NULL
 @Account_Type			CHAR(8)		 NULL  
 
    
OUTPUT PARAMETERS:    
 Name					DataType	 Default		Description    
--------------------------------------------------------------------    
    
USAGE EXAMPLES:    
--------------------------------------------------------------------    
    
 EXEC dbo.Account_Details_Sel_By_Vendor_Client_Id NULL,Null,226
 EXEC dbo.Account_Details_Sel_By_Vendor_Client_Id Null,12327,NULL
     
    
     
    
AUTHOR INITIALS:    
	Initials		Name    
--------------------------------------------------------------------    
    RKV				Ravi Kumar Vegesna 
    NR				Narayana Reddy       
    
MODIFICATIONS    
    
 Initials		Date			Modification    
--------------------------------------------------------------------    
 RKV			2017-01-20		Created for Invoice Collection 
 NR				2017-07-05		MAINT-5519 - Incresed Account_Number column in #Vendor_Account_Dtls table.
  
******/    

CREATE  PROCEDURE [dbo].[Account_Details_Sel_By_Vendor_Client_Id]
      ( 
       @Vendor_Id INT = NULL
      ,@Client_Id INT = NULL
      ,@Site_Id INT = NULL
      ,@Commodity_Id INT = NULL
      ,@Account_Type CHAR(8) = NULL )
AS 
BEGIN        
      SET NOCOUNT ON        
        
      DECLARE @Contact_Level_Vendor INT

      SELECT
            @Contact_Level_Vendor = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'ContactLevel'
            AND c.Code_Value = 'Vendor'
      
 
      CREATE TABLE #Vendor_Account_Dtls
            ( 
             Account_Id INT
            ,Account_Number VARCHAR(500)
            ,Client_Name VARCHAR(200)
            ,Account_Vendor_Type CHAR(8)
            ,Account_Vendor_Id INT
            ,Account_Not_Managed BIT )
      
  --Collecting the Utility Account Vendors    
    
      INSERT      INTO #Vendor_Account_Dtls
                  ( 
                   Account_Id
                  ,Account_Number
                  ,Client_Name
                  ,Account_Vendor_Type
                  ,Account_Vendor_Id
                  ,Account_Not_Managed )
                  SELECT
                        ucha.Account_ID
                       ,ucha.Display_Account_Number
                       ,ch.Client_Name
                       ,Account_Vendor_Type = CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                                                        AND asbv1.Account_Id IS NOT NULL THEN scha.Account_Type
                                                   WHEN e.ENTITY_NAME IS NOT NULL THEN e.ENTITY_NAME
                                                   WHEN icav.VENDOR_NAME IS NOT NULL
                                                        AND asbv1.Account_Id IS NOT NULL THEN 'Supplier'
                                                   ELSE ucha.Account_Type
                                              END
                       ,Account_Vendor_Id = CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                                                      AND asbv1.Account_Id IS NOT NULL THEN scha.Account_Vendor_Id
                                                 WHEN e.ENTITY_NAME IS NOT NULL THEN v.VENDOR_ID
                                                 WHEN icav.VENDOR_NAME IS NOT NULL
                                                      AND asbv1.Account_Id IS NOT NULL THEN icav.VENDOR_ID
                                                 ELSE ucha.Account_Vendor_Id
                                            END
                       ,ucha.Account_Not_Managed
                  FROM
                        dbo.Invoice_Collection_Account_Config icac
                        INNER JOIN core.Client_Hier_Account ucha
                              ON icac.Account_Id = ucha.Account_Id
                        INNER JOIN core.Client_Hier ch
                              ON ucha.Client_Hier_Id = ch.Client_Hier_Id
                        LEFT OUTER JOIN core.Client_Hier_Account scha
                              ON ucha.Meter_Id = scha.Meter_Id
                                 AND scha.Account_Type = 'Supplier'
                                 AND icac.Invoice_Collection_Service_End_Dt BETWEEN scha.Supplier_Account_begin_Dt
                                                                            AND     scha.Supplier_Account_End_Dt
                        LEFT OUTER JOIN ( dbo.Account_Consolidated_Billing_Vendor asbv
                                          INNER JOIN dbo.ENTITY e
                                                ON asbv.Invoice_Vendor_Type_Id = e.ENTITY_ID
                                                   AND e.ENTITY_NAME = 'Supplier'
                                          LEFT OUTER JOIN dbo.VENDOR v
                                                ON v.VENDOR_ID = asbv.Supplier_Vendor_Id )
                                          ON asbv.Account_Id = ucha.Account_Id
                                             AND icac.Invoice_Collection_Service_End_Dt BETWEEN asbv.Billing_Start_Dt
                                                                                        AND     asbv.Billing_End_Dt
                        LEFT OUTER JOIN ( dbo.Account_Consolidated_Billing_Vendor asbv1
                                          INNER JOIN dbo.ENTITY e1
                                                ON asbv1.Invoice_Vendor_Type_Id = e1.ENTITY_ID
                                                   AND e1.ENTITY_NAME = 'Supplier'
                                          LEFT OUTER JOIN dbo.VENDOR v1
                                                ON v1.VENDOR_ID = asbv1.Supplier_Vendor_Id )
                                          ON asbv1.Account_Id = ucha.Account_Id
                        LEFT OUTER JOIN ( dbo.Invoice_Collection_Account_Contact icc
                                          INNER JOIN Contact_Info ci
                                                ON ci.Contact_Info_Id = icc.Contact_Info_Id
                                                   AND icc.Is_Primary = 1
                                                   AND ci.Contact_Level_Cd = @Contact_Level_Vendor
                                          INNER JOIN dbo.Vendor_Contact_Map vcm
                                                ON ci.Contact_Info_Id = vcm.Contact_Info_Id
                                          INNER JOIN dbo.VENDOR icav
                                                ON icav.VENDOR_ID = vcm.Vendor_Id )
                                          ON icc.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                  WHERE
                        ucha.Account_Type = 'Utility'
                        AND ( @Client_Id IS NULL
                              OR ch.Client_Id = @Client_Id )
                        AND ( @Site_Id IS NULL
                              OR ch.Site_Id = @Site_Id )
                        AND ( @Commodity_Id IS NULL
                              OR ucha.Commodity_Id = @Commodity_Id )
                        AND icac.Is_Chase_Activated = 1
                  GROUP BY
                        CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                                  AND asbv1.Account_Id IS NOT NULL THEN scha.Account_Type
                             WHEN e.ENTITY_NAME IS NOT NULL THEN e.ENTITY_NAME
                             WHEN icav.VENDOR_NAME IS NOT NULL
                                  AND asbv1.Account_Id IS NOT NULL THEN 'Supplier'
                             ELSE ucha.Account_Type
                        END
                       ,CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                                  AND asbv1.Account_Id IS NOT NULL THEN scha.Account_Vendor_Id
                             WHEN e.ENTITY_NAME IS NOT NULL THEN v.VENDOR_ID
                             WHEN icav.VENDOR_NAME IS NOT NULL
                                  AND asbv1.Account_Id IS NOT NULL THEN icav.VENDOR_ID
                             ELSE ucha.Account_Vendor_Id
                        END
                       ,ucha.Account_ID
                       ,ucha.Display_Account_Number
                       ,ch.Client_Name
                       ,ucha.Account_Not_Managed    
      
    
    --Collecting the Supplier Account Vendors   
    
      INSERT      INTO #Vendor_Account_Dtls
                  ( 
                   Account_Id
                  ,Account_Number
                  ,Client_Name
                  ,Account_Vendor_Type
                  ,Account_Vendor_Id
                  ,Account_Not_Managed )
                  SELECT
                        cha.Account_ID
                       ,cha.Display_Account_Number
                       ,ch.Client_Name
                       ,cha.Account_Type
                       ,cha.Account_Vendor_Id
                       ,cha.Account_Not_Managed
                  FROM
                        Core.Client_Hier ch
                        INNER JOIN core.Client_Hier_Account cha
                              ON ch.Client_Hier_Id = cha.Client_Hier_Id
                        INNER JOIN dbo.Invoice_Collection_Account_Config icac
                              ON cha.Account_Id = icac.Account_Id
                  WHERE
                        ( @Client_Id IS NULL
                          OR ch.Client_Id = @Client_Id )
                        AND ( @Site_Id IS NULL
                              OR ch.Site_Id = @Site_Id )
                        AND ( @Commodity_Id IS NULL
                              OR cha.Commodity_Id = @Commodity_Id )
                        AND cha.Account_Type = 'Supplier'
                        AND icac.Is_Chase_Activated = 1
                  GROUP BY
                        cha.Account_ID
                       ,cha.Display_Account_Number
                       ,ch.Client_Name
                       ,cha.Account_Type
                       ,cha.Account_Vendor_Id
                       ,cha.Account_Not_Managed
      
      
      SELECT
            vad.Account_Number
           ,vad.Account_Id
           ,vad.Client_Name
           ,vad.Account_Not_Managed AS Not_Managed
      FROM
            #Vendor_Account_Dtls vad
      WHERE
            ( @Vendor_Id IS NULL
              OR vad.Account_Vendor_Id = @Vendor_Id )
            AND ( @Account_Type IS NULL
                  OR vad.Account_Vendor_Type = @Account_Type )
      GROUP BY
            vad.Account_Number
           ,vad.Account_Id
           ,vad.Client_Name
           ,vad.Account_Not_Managed
           
                
        
END;    
;   
            


 



;
GO

GRANT EXECUTE ON  [dbo].[Account_Details_Sel_By_Vendor_Client_Id] TO [CBMSApplication]
GO
