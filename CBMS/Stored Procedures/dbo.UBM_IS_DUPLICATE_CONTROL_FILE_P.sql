
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/*********    
NAME: dbo.UBM_IS_DUPLICATE_CONTROL_FILE_P  
  
DESCRIPTION:       
  
   
INPUT PARAMETERS:      
Name			    DataType		Default		Description      
------------------------------------------------------------      
@userId				VARCHAR(10)
@sessionId			VARCHAR(20)
@consolidationid	VARCHAR(50)
@masterLogId		INT  
  
OUTPUT PARAMETERS:      
Name			    DataType		Default		Description      
------------------------------------------------------------      
  
USAGE EXAMPLES:      
------------------------------------------------------------      
  
EXEC dbo.UBM_IS_DUPLICATE_CONTROL_FILE_P '','',214123,303  
EXEC dbo.UBM_IS_DUPLICATE_CONTROL_FILE_P -1,-1,1000458,15123  
EXEC dbo.UBM_IS_DUPLICATE_CONTROL_FILE_P -1,-1,999999,15123  
  
  
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
RR   Raghu Reddy  
  
   
MODIFICATIONS       
Initials	Date		Modification      
------------------------------------------------------------      
RR			2014-04-15	Added description header  
						Changed inptu parameter @consolidationid datatype INT to VARCHAR(50) as per table field type  
						Removed table dbo.UBM_AVISTA_CONTROL as not using  
      
******/  
  
CREATE PROCEDURE dbo.UBM_IS_DUPLICATE_CONTROL_FILE_P
      ( 
       @userId VARCHAR(10)
      ,@sessionId VARCHAR(20)
      ,@consolidationid VARCHAR(50)
      ,@masterLogId INT )
AS
BEGIN

	SET NOCOUNT ON  
	  
	SELECT
		  uac.UBM_BATCH_MASTER_LOG_ID MASTER_LOG_ID
		 ,ufm.FEED_FILE_NAME FILE_NAMES
	FROM
		  dbo.UBM_AVISTA_CONTROL uac
		  JOIN dbo.UBM_FEED_FILE_MAP ufm
				ON ufm.UBM_BATCH_MASTER_LOG_ID = uac.UBM_BATCH_MASTER_LOG_ID
	WHERE
		  uac.CONSOLIDATION_ID = @consolidationid
		  AND uac.UBM_BATCH_MASTER_LOG_ID <> @masterLogId
	GROUP BY
		  uac.UBM_BATCH_MASTER_LOG_ID
		 ,ufm.FEED_FILE_NAME
	ORDER BY
		  uac.UBM_BATCH_MASTER_LOG_ID DESC  
	  
END
;
GO

GRANT EXECUTE ON  [dbo].[UBM_IS_DUPLICATE_CONTROL_FILE_P] TO [CBMSApplication]
GO
