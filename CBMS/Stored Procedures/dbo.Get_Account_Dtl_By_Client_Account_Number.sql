SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
  
NAME:   
   [dbo].[Get_Account_Dtl_By_Client_Account_Number]    
       
DESCRIPTION:   
			To Get Account Information for selected Account Number.  
        
INPUT PARAMETERS:          
NAME				DATATYPE			DEFAULT				DESCRIPTION  
------------------------------------------------------------------------------  
@Account_Number		VARCHAR         
  
OUTPUT PARAMETERS:  
NAME				DATATYPE			DEFAULT				DESCRIPTION  
------------------------------------------------------------------------------  
USAGE EXAMPLES:  
------------------------------------------------------------------------------  
  
 EXEC dbo.Get_Account_Dtl_By_Client_Account_Number  '541448800000000710'  
  
 EXEC dbo.Get_Account_Dtl_By_Client_Account_Number  '0617000048026'  
   
  
AUTHOR INITIALS:            
INITIALS		NAME            
------------------------------------------------------------------------------  
NR				 Narayana Reddy  
            
MODIFICATIONS             
INITIALS		DATE			 MODIFICATION            
------------------------------------------------------------------------------  
NR				2016-05-07		Created For Data Full load.  
*/    
  
CREATE PROCEDURE [dbo].[Get_Account_Dtl_By_Client_Account_Number]
      ( 
       @Account_Number VARCHAR(50)
      ,@Commodity_Id INT = NULL )
AS 
BEGIN  
  
      SET NOCOUNT ON;  
  
      SELECT
            cha.Client_Hier_Id
           ,cha.Account_Id
           ,cha.Commodity_Id
           ,ch.Client_Id
      FROM
            core.Client_Hier_Account cha
            INNER JOIN core.Client_Hier ch
                  ON cha.Client_Hier_Id = ch.Client_Hier_Id
      WHERE
            cha.Account_Number = @Account_Number
            AND ( @Commodity_Id IS NULL
                  OR cha.Commodity_Id = @Commodity_Id )  
  
END;  

;
GO
GRANT EXECUTE ON  [dbo].[Get_Account_Dtl_By_Client_Account_Number] TO [CBMSApplication]
GO
