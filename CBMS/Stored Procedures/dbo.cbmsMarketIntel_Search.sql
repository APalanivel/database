SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE    PROCEDURE [dbo].[cbmsMarketIntel_Search]
	( @MyAccountId int
	, @full_text_keyword varchar(200) = null
	)
AS
BEGIN

	   /*select distinct mi.market_intel_id
		, mi.publish_date
		, mi.market_intel_title
		, mi.market_intel
		, commodity_type = dbo.MARKET_INTEL_COMMODITIES(mi.market_intel_id)
		, state_name = dbo.MARKET_INTEL_STATES(mi.market_intel_id)
	     from market_intel mi
	     where mi.market_intel like isNull(@full_text_keyword, '%%')
	     order by mi.publish_date desc*/

 select mi.market_intel_id
		, mi.publish_date
		, mi.market_intel_title
		, commodity_type = dbo.MARKET_INTEL_COMMODITIES(mi.market_intel_id)
		, state_name = dbo.MARKET_INTEL_STATES(mi.market_intel_id)
	     from market_intel mi

	     where mi.market_intel like isNull(@full_text_keyword, '%%')

	     group by mi.market_intel_id
		, mi.publish_date
		, mi.market_intel_title
		
	     order by mi.publish_date desc

END
 

GO
GRANT EXECUTE ON  [dbo].[cbmsMarketIntel_Search] TO [CBMSApplication]
GO
