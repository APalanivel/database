SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********       
NAME:  dbo.CU_Invoice_Determinant_Account_Merge 
     
     
DESCRIPTION:  This will insert data into CU_Invoice_Determinant_Account table
    
INPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
@CU_Invoice_Determinant_Id	int
@Account_Id					int
@Determinant_Expression		varchar		NULL
@Determinant_Value			decimal     NULL
        
        
OUTPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:   
     
	CU_Invoice_Determinant_Account_Merge 7102632, 49,'A2*0.3',15000
    
    
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
NK   Nageswara Rao Kosuri             
SKA	Shobhit Kumar Agrawal    
    
Initials	Date		Modification      
------------------------------------------------------------      
NK		12/24/2009	Created    
NK		01/18/2010  Modified to add update statement	    
SKA		03/30/2010	Used Merge instead of Insert and update statement

******/      

CREATE PROCEDURE dbo.CU_Invoice_Determinant_Account_Merge      
	 @CU_Invoice_Determinant_Id INT
	,@Account_Id INT
	,@Determinant_Expression VARCHAR(200) = NULL
	,@Determinant_Value DECIMAL(16,4) = NULL    

AS      
BEGIN      
SET NOCOUNT ON     

	MERGE INTO CU_Invoice_Determinant_Account AS tgt
	USING
		( SELECT
			@Determinant_Expression AS Determinant_Expression
			,@Determinant_Value AS Determinant_Value
			,@CU_Invoice_Determinant_Id AS CU_Invoice_Determinant_Id
			,@Account_Id AS Account_Id
		) AS src
	ON tgt.CU_INVOICE_DETERMINANT_ID = src.CU_Invoice_Determinant_Id
		AND tgt.ACCOUNT_ID =  src.Account_Id
	WHEN MATCHED
	THEN UPDATE
		SET
			Determinant_Expression = src.Determinant_Expression
			,Determinant_Value = src.Determinant_Value
	WHEN NOT MATCHED
	THEN INSERT
			(CU_INVOICE_DETERMINANT_ID
			,ACCOUNT_ID
			,Determinant_Expression
			,Determinant_Value
			)
		VALUES
			(src.CU_Invoice_Determinant_Id
			,src.Account_Id
			,src.Determinant_Expression
			,src.Determinant_Value
			);
END    


    


GO
GRANT EXECUTE ON  [dbo].[CU_Invoice_Determinant_Account_Merge] TO [CBMSApplication]
GO
