SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Contract_Recalc_Config_Upd

DESCRIPTION:

INPUT PARAMETERS:
	Name										DataType		Default		Description
-------------------------------------------------------------------------------------------
	@Contract_Recalc_Config_id	INT
    @Start_Dt									DATE
    @End_Dt										DATE
   @Supplier_Recalc_Type_Cd					INT
    @User_Info_Id								INT

OUTPUT PARAMETERS:
	Name										DataType		Default		Description
-------------------------------------------------------------------------------------------
	
USAGE EXAMPLES:
-------------------------------------------------------------------------------------------
 

	BEGIN TRANSACTION
	
      EXEC Contract_Recalc_Config_Upd 
            ,1
            ,290
           ,'2015-01-01'
           ,'2015-01-01'
           ,102029
           ,49
           ,102536
           
	SELECT * FROM Contract_Recalc_Config WHERE Account_Id=1148520  
			
	ROLLBACK TRANSACTION


AUTHOR INITIALS:
	Initials	Name
-----------------------------------------------------------------------------------------
	NR			Narayana Reddy	
	
	
MODIFICATIONS
	Initials	Date			Modification
-----------------------------------------------------------------------------------------
    NR				2019-06-26		Created For Add contract.   

******/


CREATE PROCEDURE [dbo].[Contract_Recalc_Config_Upd]
    (
        @Contract_Recalc_Config_Id INT
        , @Commodity_Id INT
        , @Start_Dt DATE
        , @End_Dt DATE
        , @Supplier_Recalc_Type_Cd INT
        , @User_Info_Id INT
        , @Determinant_Source_Cd INT
    )
AS
    BEGIN

        SET NOCOUNT ON;




        UPDATE
            acirt
        SET
            Determinant_Source_Cd = @Determinant_Source_Cd
            , acirt.Commodity_Id = @Commodity_Id
            , Start_Dt = @Start_Dt
            , End_Dt = @End_Dt
            , Supplier_Recalc_Type_Cd = @Supplier_Recalc_Type_Cd
            , Updated_User_Id = @User_Info_Id
            , Last_Change_Ts = GETDATE()
        FROM
            dbo.Contract_Recalc_Config acirt
        WHERE
            acirt.Contract_Recalc_Config_Id = @Contract_Recalc_Config_Id;




    END;
    ;
GO
GRANT EXECUTE ON  [dbo].[Contract_Recalc_Config_Upd] TO [CBMSApplication]
GO
