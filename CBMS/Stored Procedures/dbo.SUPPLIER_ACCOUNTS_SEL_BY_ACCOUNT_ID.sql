SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
NAME:          
	[DBO].[SUPPLIER_ACCOUNTS_SEL_BY_ACCOUNT_ID]  
     
DESCRIPTION:          
	TO GET THE SUPPLIER_ACCOUNT DETAILS FOR THE SELECTED CONTRACT ID
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@ACCOUNT_ID		INT

                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        
USE [CBMS_TK2]
  
	EXEC SUPPLIER_ACCOUNTS_SEL_BY_ACCOUNT_ID  22558 	
	EXEC SUPPLIER_ACCOUNTS_SEL_BY_ACCOUNT_ID  284254

     
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
DMR		Deana Ritter
PNR		PnadariNath
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR			05/19/2010	CREATED     
NR			209-07-03		Add Contract - Changed Inner to left on Entity.SERVICE_LEVEL_TYPE_ID. 


 
*/

CREATE PROCEDURE [dbo].[SUPPLIER_ACCOUNTS_SEL_BY_ACCOUNT_ID]
    (
        @ACCOUNT_ID INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            m.METER_NUMBER METER_NUMBER
            , utilacc.ACCOUNT_NUMBER UTIL_ACCOUNT_NUMBER
        FROM
            dbo.SUPPLIER_ACCOUNT_METER_MAP map
            JOIN dbo.ACCOUNT acc
                ON acc.ACCOUNT_ID = map.ACCOUNT_ID
            JOIN dbo.METER m
                ON map.METER_ID = m.METER_ID
            JOIN dbo.ACCOUNT utilacc
                ON m.ACCOUNT_ID = utilacc.ACCOUNT_ID
            LEFT JOIN dbo.ENTITY ent
                ON acc.SERVICE_LEVEL_TYPE_ID = ent.ENTITY_ID
        WHERE
            map.ACCOUNT_ID = @ACCOUNT_ID
            AND map.IS_HISTORY = 0;

    END;

GO
GRANT EXECUTE ON  [dbo].[SUPPLIER_ACCOUNTS_SEL_BY_ACCOUNT_ID] TO [CBMSApplication]
GO
