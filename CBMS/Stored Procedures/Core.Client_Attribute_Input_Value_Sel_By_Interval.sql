SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********  
NAME: Core.Client_Attribute_Input_Value_Sel_By_Interval    

DESCRIPTION:     

	Selects input value screen data for the given client based on the year stamp.
	
INPUT PARAMETERS:    
Name					DataType  Default Description    
------------------------------------------------------------    
@MyClientId				INT     
@YearStamp				CHAR(6)     
@Interval				VARCHAR(15)    


OUTPUT PARAMETERS:    
Name   DataType  Default Description    
------------------------------------------------------------    

USAGE EXAMPLES:    
------------------------------------------------------------    

Core.Client_Attribute_Input_Value_Sel_By_Interval 10027,'FY2008', 'Quarterly'
Core.Client_Attribute_Input_Value_Sel_By_Interval 10027,'FY2008', 'Monthly'
Core.Client_Attribute_Input_Value_Sel_By_Interval 10069,'FY2008', 'Semi Annual'


AUTHOR INITIALS:    
Initials	Name    
------------------------------------------------------------    
SS   Subhash Subramanyam         
HG   Harihara Suthan Ganeshan     
MA   Mohammed Aman
SKA	 Shobhit Kr Agrawal
 
MODIFICATIONS     
Initials	Date		Modification    
------------------------------------------------------------    
 SS     06/23/2009  Created    
 SKA	07/07/2009  Modofied this SP as per Coding standards
 SKA	07/15/2009  Modofied colums in SP as per the changes in UDF 'ufn_IntervalsForYear'
 DMR		  09/10/2010 Modified for Quoted_Identifier
     
    
******/
CREATE PROCEDURE Core.Client_Attribute_Input_Value_Sel_By_Interval      
		@MyClientId INT,       
		@YearStamp CHAR(6),    
		@Interval  VARCHAR(15)    
AS    

BEGIN      
SET NOCOUNT ON   
   
	SELECT Client_Id,
	   Interval_ID,     
	   Interval_Name,     
	   Start_Dt,     
	   End_Dt    
   FROM dbo.ufn_IntervalsForYear(@MyClientId, @YearStamp, @Interval)    

END
GO
GRANT EXECUTE ON  [Core].[Client_Attribute_Input_Value_Sel_By_Interval] TO [CBMSApplication]
GO
