SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.DV2GetIPDeltaUpdates

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

--Mark McCallon
--11-19-07
--delta update for dv2_ip
--
create procedure dbo.DV2GetIPDeltaUpdates
as
begin
set nocount on

UPDATE [dbo].[DV2_IP]
SET [DIVISION_ID]=b.DIVISION_ID, [SITE_ID]=b.SITE_ID, [SITE_NOT_MANAGED]=b.SITE_NOT_MANAGED, [VENDOR_TYPE_ID]=b.VENDOR_TYPE_ID, 
[VENDOR_ID]=b.VENDOR_ID, [ACCOUNT_ID]=b.ACCOUNT_ID, [COMMODITY_TYPE_ID]=b.COMMODITY_TYPE_ID, [COUNTRY_ID]=b.COUNTRY_ID, 
[STATE_ID]=b.STATE_ID, [SERVICE_MONTH_ID]=b.SERVICE_MONTH_ID, [IS_EXPECTED]=b.IS_EXPECTED, [IS_RECEIVED]=b.IS_RECEIVED, 
[INVOICE_COUNT]=b.INVOICE_COUNT, [CBMS_IMAGE_ID]=b.CBMS_IMAGE_ID
from tempdb.dbo.dv2_ip_delta b
inner join dbo.dv2_ip a
on a.site_id=b.site_id and a.account_id=b.account_id and a.commodity_type_id=b.commodity_type_id and
a.service_month_id=b.service_month_id
where a.division_id<>b.division_id or
a.site_id<>b.site_id or
a.site_not_managed<>b.site_not_managed or
a.vendor_type_id<>b.vendor_type_id or
a.vendor_id<>b.vendor_id or
a.account_id<>b.account_id or
a.commodity_type_id<>b.commodity_type_id or
a.country_id<>b.country_id or
a.state_id<>b.state_id or
a.service_month_id<>b.service_month_id or
a.is_expected<>b.is_expected or
a.is_received<>b.is_received or
a.invoice_count<>b.invoice_count or
a.cbms_image_id<>b.cbms_image_id

end
GO
GRANT EXECUTE ON  [dbo].[DV2GetIPDeltaUpdates] TO [CBMSApplication]
GO
