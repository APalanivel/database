SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                             
/******                                    
 NAME: dbo.Invoice_Collection_Queue_Dtls_Sel_Invoice_Collection_Account_Config_Id                        
                                    
 DESCRIPTION:                                    
   To get the details of invoice collection Queue for the given account_config                     
                                    
 INPUT PARAMETERS:                      
                                 
 Name                        DataType         Default       Description                    
---------------------------------------------------------------------------------------------------------------                  
@Invoice_Collection_Account_Config_Id INT  NULL             
                                          
 OUTPUT PARAMETERS:                      
                                       
 Name                        DataType         Default       Description                    
---------------------------------------------------------------------------------------------------------------                  
                                    
 USAGE EXAMPLES:                                        
---------------------------------------------------------------------------------------------------------------                                        
                
            
  EXEC dbo.Invoice_Collection_Queue_Dtls_Sel_Invoice_Collection_Account_Config_Id             
      @Invoice_Collection_Account_Config_Id = 342            
                  
            
                                   
 AUTHOR INITIALS:                    
                   
 Initials              Name                    
---------------------------------------------------------------------------------------------------------------                                  
 RKV                  Ravi Kumar Vegesna            
                                     
 MODIFICATIONS:                  
                      
 Initials              Date             Modification                  
---------------------------------------------------------------------------------------------------------------                  
    RKV    2017-01-25  Created For Invoice_Collection.                     
                                   
******/                             
                            
CREATE PROCEDURE [dbo].[Invoice_Collection_Queue_Dtls_Sel_Invoice_Collection_Account_Config_Id]
      ( 
       @Invoice_Collection_Account_Config_Id INT
      ,@is_Manual BIT = NULL )
AS 
BEGIN                            
      SET NOCOUNT ON;                
                 
                 
      SELECT
            icq.Invoice_Collection_Queue_Id
           ,icq.Collection_Start_Dt
           ,icq.Collection_End_Dt
           ,icq.Commodity_Id
           ,icq.Is_Locked
           ,icq.Is_Manual
           ,icqt.Code_Value Collection_Queue_Type
           ,icqt.Code_Id Collection_Queue_Type_Cd
           ,icq.Status_Cd
           ,icq.Invoice_Collection_Exception_Type_Cd
           ,aicm.Account_Invoice_Collection_Month_Id
           ,aicm.Service_Month
           ,aicm.Seq_No
           ,icq.Received_Status_Updated_Dt
           ,CASE WHEN cpc.Code_Value = 'Day Issued' THEN 'High'
                 ELSE 'Normal'
            END PRIORITY
      FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                  ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN dbo.Code sc
                  ON sc.Code_Id = icq.Status_Cd
            INNER JOIN dbo.Code cpc
                  ON cpc.Code_Id = icac.Chase_Priority_Cd
            LEFT  JOIN dbo.Code icqt
                  ON icqt.Code_Id = icq.Invoice_Collection_Queue_Type_Cd
            LEFT JOIN dbo.Invoice_Collection_Queue_Month_Map icqmm
                  ON icqmm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            LEFT JOIN dbo.Account_Invoice_Collection_Month aicm
                  ON aicm.Account_Invoice_Collection_Month_Id = icqmm.Account_Invoice_Collection_Month_Id
      WHERE
            icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
            AND ( @is_Manual IS NULL
                  OR icq.Is_Manual = @is_Manual )
            AND sc.Code_Value NOT IN ( 'Archived' )  
                        
                        
END;

;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Queue_Dtls_Sel_Invoice_Collection_Account_Config_Id] TO [CBMSApplication]
GO
