
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.Commodity_Sel_For_Account

DESCRIPTION:

	Used to Select all Commodity associated with the given Account_Id (Supplier / Utility)
	Procedure will be used by the Edit Cost_Usage page to show the account speicific Commodities tab.

INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@Account_Id			int						(Supplier / Utility)

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Commodity_Sel_For_Account 157854
	EXEC dbo.Commodity_Sel_For_Account 158070
	EXEC dbo.Commodity_Sel_For_Account 1152421

	SELECT TOP 100 * FROM Account WHERE Account_Type_id = 38 Order By Account_Id DESC

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hari
	SKA			Shobhit Kumar Agrawal
	RR			Raghu Reddy

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG        	03/05/2010	Created
	HG			03/15/2010	Commodity name and default_uom_type_id column added in the select clause.
	RR			2016-02-11	Global Sourcing - Phase3 - GCS-471 Added Geographic UOMs
******/

CREATE PROCEDURE [dbo].[Commodity_Sel_For_Account] ( @Account_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
	
      SELECT
            cha.Commodity_Id
           ,comm.Commodity_Name
           ,comm.Default_UOM_Entity_Type_Id
           ,dfltuom.ENTITY_NAME AS Uom_Name
           ,country_uom.ENTITY_ID AS Country_Default_UOM_Entity_Type_Id
           ,country_uom.ENTITY_NAME AS Country_Default_UOM
           ,cucm.CURRENCY_UNIT_ID
           ,cu.CURRENCY_UNIT_NAME
      FROM
            Core.Client_Hier_Account cha
            INNER JOIN dbo.Commodity comm
                  ON cha.Commodity_Id = comm.Commodity_Id
            INNER JOIN dbo.ENTITY dfltuom
                  ON dfltuom.ENTITY_ID = comm.Default_UOM_Entity_Type_Id
            LEFT JOIN dbo.Country_Commodity_Uom ccu
                  ON cha.Commodity_Id = ccu.Commodity_Id
                     AND cha.Meter_Country_Id = ccu.COUNTRY_ID
            LEFT JOIN dbo.ENTITY country_uom
                  ON country_uom.ENTITY_ID = ccu.Default_Uom_Type_Id
            LEFT JOIN dbo.CURRENCY_UNIT_COUNTRY_MAP cucm
                  ON cha.Meter_Country_Id = cucm.COUNTRY_ID
            LEFT JOIN dbo.CURRENCY_UNIT cu
                  ON cucm.CURRENCY_UNIT_ID = cu.CURRENCY_UNIT_ID
      WHERE
            cha.Account_Id = @Account_Id
      GROUP BY
            cha.Commodity_Id
           ,comm.Commodity_Name
           ,comm.Default_UOM_Entity_Type_Id
           ,dfltuom.ENTITY_NAME
           ,country_uom.ENTITY_ID
           ,country_uom.ENTITY_NAME
           ,cucm.CURRENCY_UNIT_ID
           ,cu.CURRENCY_UNIT_NAME
      ORDER BY
            comm.Commodity_Name

END

;
GO

GRANT EXECUTE ON  [dbo].[Commodity_Sel_For_Account] TO [CBMSApplication]
GO
