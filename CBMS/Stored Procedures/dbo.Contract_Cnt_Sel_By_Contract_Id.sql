SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
NAME:  [dbo].Contract_Cnt_Sel_By_Contract_Id  
               
DESCRIPTION:   Get Contact count based on contract_Id               
	    
               
INPUT PARAMETERS:                
Name					DataType		Default		 Description      
------------------------------------------------------------------------      
@Contract_ID			INT                       
   
      
 OUTPUT PARAMETERS:                
Name					DataType		Default		 Description      
------------------------------------------------------------------------    
             
 USAGE EXAMPLES:                
------------------------------------------------------------------------                     
[dbo].Contract_Cnt_Sel_By_Contract_Id 143685
    

 AUTHOR INITIALS:             
 Initials		Name
------------------------------------------------------------------------
 SP				Srinivas Patchava

 moDIFICATIONS:                 
 Initials		Date			Modification                
------------------------------------------------------------------------    
 SP				2019-07-23      Created for SE2017-752

******/
CREATE PROCEDURE [dbo].[Contract_Cnt_Sel_By_Contract_Id]
     (
         @Contract_ID INT
     )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            COUNT(c.CONTRACT_ID) AS Contract_Cnt
        FROM
            dbo.CONTRACT c
        WHERE
            c.ED_CONTRACT_NUMBER LIKE CAST(@Contract_ID AS VARCHAR(50)) + '%';

    END;
GO
GRANT EXECUTE ON  [dbo].[Contract_Cnt_Sel_By_Contract_Id] TO [CBMSApplication]
GO
