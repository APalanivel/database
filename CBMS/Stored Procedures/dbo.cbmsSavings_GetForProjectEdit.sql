SET NUMERIC_ROUNDABORT OFF 
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******
NAME:
		dbo.cbmsSavings_GetForProjectEdit

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@sso_project_id	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
EXEC dbo.cbmsSavings_GetForProjectEdit  202
EXEC dbo.cbmsSavings_GetForProjectEdit  686
EXEC dbo.cbmsSavings_GetForProjectEdit  65
EXEC dbo.cbmsSavings_GetForProjectEdit  129

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CPE			Chaitanya Panduga Eshwar
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	CPE			03/28/2011	Replaced vwCbmsSsoProjectOwnerFlat and vwCbmsSsoSavingsOwnerFlat views with
							SSO_PROJECT_OWNER_MAP and SSO_SAVINGS_OWNER_MAP tables respectively
							Used dbo.Commodity instead of dbo.Entity Com. Removed the filter on entity descriptions.
	        				
******/

CREATE PROCEDURE dbo.cbmsSavings_GetForProjectEdit
( 
 @sso_project_id int )
AS 
BEGIN
      SELECT DISTINCT
            is_checked = CASE WHEN MAP.SSO_SAVINGS_ID IS NULL THEN 0
                              ELSE 1
                         END
           ,SSOM.SSO_SAVINGS_ID
           ,SS.SAVINGS_TITLE
           ,SS.SAVINGS_DESCRIPTION
           ,CU.CURRENCY_UNIT_NAME
           ,SS.TOTAL_ESTIMATED_SAVINGS
           ,SCT.ENTITY_NAME savings_category_type
           ,SS.START_DATE
           ,SS.END_DATE
      FROM
            dbo.SSO_PROJECT_OWNER_MAP SPOM
            JOIN dbo.SSO_SAVINGS_OWNER_MAP SSOM
                  ON SPOM.Client_Hier_Id = SSOM.Client_Hier_Id
            JOIN dbo.SSO_PROJECT SP
                  ON SPOM.sso_project_id = SP.SSO_PROJECT_ID
            JOIN dbo.ENTITY PC
                  ON PC.ENTITY_ID = SP.COMMODITY_TYPE_ID
            JOIN dbo.Commodity Com
                  ON Com.Commodity_Name = PC.ENTITY_NAME
            JOIN dbo.SSO_SAVINGS SS
                  ON SS.SSO_SAVINGS_ID = SSOM.SSO_SAVINGS_ID
                     AND SS.COMMODITY_TYPE_ID = Com.Commodity_Id
            JOIN dbo.CURRENCY_UNIT CU
                  ON CU.CURRENCY_UNIT_ID = SS.CURRENCY_UNIT_ID
            JOIN dbo.ENTITY SCT
                  ON SS.SAVINGS_CATEGORY_TYPE_ID = SCT.ENTITY_ID
            LEFT JOIN dbo.SSO_PROJECT_SAVINGS_MAP MAP
                  on MAP.SSO_PROJECT_ID = SPOM.SSO_PROJECT_ID
                     and MAP.SSO_SAVINGS_ID = SS.SSO_SAVINGS_ID
      WHERE
            SPOM.SSO_PROJECT_ID = @sso_project_id
END


GO
GRANT EXECUTE ON  [dbo].[cbmsSavings_GetForProjectEdit] TO [CBMSApplication]
GO
