
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
   
/*****
        
NAME: dbo.Report_DE_SOP_Data_P      
    
DESCRIPTION:        
	Used to get a distinct ordered by list from multiple tables.  The data in each column IS NOT
	related to each other.  Each column is a distinct list from the appropriate table.  This sproc
	populates the SOPS table which is then export via Tidal to create the SOPS Distinct data spreasheet.
	This spreadsheet is used to populate drop downs within Excel macros.
       
INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
    
USAGE EXAMPLES:
------------------------------------------------------------
EXEC Report_DE_SOP_Data_P      
    
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
AKR   Ashok Kumar Raju
     
MAINTENANCE LOG:        
Initials	Date		Modification 
--- ---------- ----------------------------------------------        
  AKR    2012-05-01  Cloned from sproc SOP_Data_P
  DMR	 09/10/2012  Modified to return in order of BogusNum.  Altered WHERE clause to exclude
                     ONLY those columns where ALL columns are null.  Tried using RowNum instead
                     of temp table and performance was unacceptable.  Reverted back to Temp Table 
                     update.

******/

CREATE PROCEDURE dbo.Report_DE_SOP_Data_P
      WITH EXECUTE AS SELF
AS 
BEGIN  
/**************************************************************************************  
  This statement eliminates a lot of useless chatter from the procedures   
***************************************************************************************/  
  
      SET nocount ON  
      
      DECLARE
            @RowCount BIGINT = 0
           ,@RowCntHold BIGINT = 0
           ,@MaxRowCount BIGINT = 1048576
           ,@RowCntErrorMsg VARCHAR(25) = 'Rows Exceeded 1,048,576'
  
/**************************************************************************************  
   Truncate Existing SOPS Table  
***************************************************************************************/  
      TRUNCATE TABLE SOPS  
  
/**************************************************************************************  
 Creating a work table to hold our result sets.  The identity column is necessary so  
 that we can join back to the SOPS table.  This is a temporary table rather than the  
 preferred table variable because we need to be able to truncate it.  A table variable  
 can not be truncated.  
***************************************************************************************/  
  
      CREATE TABLE #tmp1
            ( 
             BogusNum INT IDENTITY(1, 1)
                          PRIMARY KEY
            ,FieldValue VARCHAR(255) )  
  
  
/**************************************************************************************  
 Pre populating the data for the maximum number of rows available in Excel.  We'll be  
 checking that after each step.  We don need these numbers to be pre-populated.  If   
 we used an identity column and populated as we went along, then we run into issues   
 if one column contains more records than the previous and we wouldn't be able to join   
 back.  Then we'd be back in the multiple union queries.  
***************************************************************************************/  
      DECLARE @cntr INT  
      SET @cntr = 1  
  

      INSERT      INTO SOPS
                  ( BogusNum )
      VALUES
                  ( 1 )


      WHILE @cntr < 1048575 
            BEGIN  
                  INSERT      SOPS
                              ( 
                               BogusNum )
                              SELECT
                                    @cntr + BogusNum
                              FROM
                                    dbo.SOPS
                              WHERE
                                    @cntr + BogusNum < @MaxRowCount
                      
                  SELECT
                        @cntr = max(BogusNum)
                  FROM
                        dbo.SOPS
                
            END  

  
/**************************************************************************************  
 Selecting our First Column.  We insert into the temp table for the unique Client Name  
 ordering by client name so that it's in alphabetical order.  
***************************************************************************************/  
      INSERT      #tmp1
                  ( 
                   FieldValue )
                  SELECT
                        c.Client_Name
                  FROM
                        dbo.Client c
                  WHERE
                        c.Not_Managed <> '1'
                  GROUP BY
                        c.CLIENT_NAME
                  ORDER BY
                        c.Client_Name  
  
/**************************************************************************************  
 Now that we have the data, let's update the work table to the appropriate column.  
 The only thing that will change from field to field is the Set <column_name>.  We join  
 back to the work table on the generated number.  Basically, the generated number is  
 nothing more than the Excel rownumber.  
***************************************************************************************/  
      UPDATE
            s
      SET   
            ClientName = t.FieldValue
      FROM
            #tmp1 t
            INNER JOIN dbo.SOPS S
                  ON T.BogusNum = S.BogusNum  
      SET @RowCntHold = @@RowCount
      IF ( @RowCntHold BETWEEN @RowCount AND @MaxRowCount ) 
            SET @RowCount = @RowCntHold           
  
      IF @RowCntHold > @MaxRowCount 
            RAISERROR (@RowCntErrorMsg,16,1)

/**************************************************************************************  
 We truncate the work table for a few reasons.    
   1. It's easier on the system to truncate a table   
   2. Truncate resets the identity counter of the BogusNumber back to 1  
***************************************************************************************/  
      TRUNCATE TABLE #tmp1  
  
/**************************************************************************************  
 Now repeat the following 3 statements for each of the remaining columns.  
***************************************************************************************/  
--City  
      INSERT      #tmp1
                  ( 
                   FieldValue )
                  SELECT
                        ad.City
                  FROM
                        dbo.Address ad
                  GROUP BY
                        ad.CITY
                  ORDER BY
                        ad.City  
  
      UPDATE
            s
      SET   
            City = t.FieldValue
      FROM
            #tmp1 t
            INNER JOIN SOPS S
                  ON T.BogusNum = S.BogusNum  
      
      SET @RowCntHold = @@RowCount
      IF ( @RowCntHold BETWEEN @RowCount AND @MaxRowCount ) 
            SET @RowCount = @RowCntHold           
  
      IF @RowCntHold > @MaxRowCount 
            RAISERROR (@RowCntErrorMsg,16,1)


      TRUNCATE TABLE #tmp1  
  
--State_Province  
      INSERT      #tmp1
                  ( 
                   FieldValue )
                  SELECT
                        s.State_name
                  FROM
                        dbo.State s
                  GROUP BY
                        s.STATE_NAME
                  ORDER BY
                        State_Name  
  
      UPDATE
            s
      SET   
            State_Province = t.FieldValue
      FROM
            #tmp1 t
            INNER JOIN SOPS S
                  ON T.BogusNum = S.BogusNum  
                  
      SET @RowCntHold = @@RowCount
      IF ( @RowCntHold BETWEEN @RowCount AND @MaxRowCount ) 
            SET @RowCount = @RowCntHold           
  
      IF @RowCntHold > @MaxRowCount 
            RAISERROR (@RowCntErrorMsg,16,1)

      TRUNCATE TABLE #tmp1  
  
--SiteName  
      INSERT      #tmp1
                  ( 
                   FieldValue )
                  SELECT
                        s.Site_Name
                  FROM
                        dbo.Site s
                  WHERE
                        s.Not_Managed <> '1'
                  GROUP BY
                        s.Site_Name
                  ORDER BY
                        s.Site_name  
  
      UPDATE
            s
      SET   
            SiteName = t.FieldValue
      FROM
            #tmp1 t
            INNER JOIN dbo.SOPS S
                  ON T.BogusNum = S.BogusNum  
  
      SET @RowCntHold = @@RowCount
      IF ( @RowCntHold BETWEEN @RowCount AND @MaxRowCount ) 
            SET @RowCount = @RowCntHold           
  
      IF @RowCntHold > @MaxRowCount 
            RAISERROR (@RowCntErrorMsg,16,1)

      TRUNCATE TABLE #tmp1  
  
  
--SiteType  
      INSERT      #tmp1
                  ( 
                   FieldValue )
                  SELECT DISTINCT
                        Sty.entity_name
                  FROM
                        dbo.Site s
                        JOIN dbo.entity sty
                              ON sty.entity_id = s.site_type_id
                  ORDER BY
                        sty.entity_name  
  
      UPDATE
            s
      SET   
            SiteType = t.FieldValue
      FROM
            #tmp1 t
            INNER JOIN dbo.SOPS S
                  ON T.BogusNum = S.BogusNum  
  
      SET @RowCntHold = @@RowCount
      IF ( @RowCntHold BETWEEN @RowCount AND @MaxRowCount ) 
            SET @RowCount = @RowCntHold           
  
      IF @RowCntHold > @MaxRowCount 
            RAISERROR (@RowCntErrorMsg,16,1)

      TRUNCATE TABLE #tmp1  
  
--CommodityType  
      INSERT      #tmp1
                  ( 
                   FieldValue )
                  SELECT
                        comm.entity_name
                  FROM
                        dbo.Entity comm
                  WHERE
                        comm.entity_type = '157'
                  GROUP BY
                        comm.entity_name
                  ORDER BY
                        comm.entity_name  
  
      UPDATE
            s
      SET   
            CommodityType = t.FieldValue
      FROM
            #tmp1 t
            INNER JOIN dbo.SOPS S
                  ON T.BogusNum = S.BogusNum  
  
  
      SET @RowCntHold = @@RowCount
      IF ( @RowCntHold BETWEEN @RowCount AND @MaxRowCount ) 
            SET @RowCount = @RowCntHold           
  
      IF @RowCntHold > @MaxRowCount 
            RAISERROR (@RowCntErrorMsg,16,1)

      TRUNCATE TABLE #tmp1  
  
  
--Country  
      INSERT      #tmp1
                  ( 
                   FieldValue )
                  SELECT
                        Ctry.Country_Name
                  FROM
                        dbo.Country Ctry
                  GROUP BY
                        Ctry.COUNTRY_NAME
                  ORDER BY
                        Ctry.Country_Name  
  
      UPDATE
            s
      SET   
            Country = t.FieldValue
      FROM
            #tmp1 t
            INNER JOIN dbo.SOPS S
                  ON T.BogusNum = S.BogusNum  

  
      SET @RowCntHold = @@RowCount
      IF ( @RowCntHold BETWEEN @RowCount AND @MaxRowCount ) 
            SET @RowCount = @RowCntHold           
  
      IF @RowCntHold > @MaxRowCount 
            RAISERROR (@RowCntErrorMsg,16,1)


      TRUNCATE TABLE #tmp1  
  
--Region  
      INSERT      #tmp1
                  ( 
                   FieldValue )
                  SELECT
                        r.Region_Name
                  FROM
                        dbo.Region r
                  GROUP BY
                        r.REGION_NAME
                  ORDER BY
                        r.Region_Name  
  
      UPDATE
            s
      SET   
            Region = t.FieldValue
      FROM
            #tmp1 t
            INNER JOIN SOPS S
                  ON T.BogusNum = S.BogusNum  

  
      SET @RowCntHold = @@RowCount
      IF ( @RowCntHold BETWEEN @RowCount AND @MaxRowCount ) 
            SET @RowCount = @RowCntHold           
  
      IF @RowCntHold > @MaxRowCount 
            RAISERROR (@RowCntErrorMsg,16,1)


      TRUNCATE TABLE #tmp1  
  
--AccountNumber  
      INSERT      #tmp1
                  ( 
                   FieldValue )
                  SELECT
                        ltrim(rtrim(acct.Account_Number))
                  FROM
                        dbo.Account acct
                  WHERE
                        acct.Account_Type_Id = '38'
                        AND acct.Not_Managed <> '1'
                  GROUP BY
                        acct.ACCOUNT_NUMBER
                  ORDER BY
                        acct.Account_Number  
  
      UPDATE
            s
      SET   
            AccountNumber = t.FieldValue
      FROM
            #tmp1 t
            INNER JOIN SOPS S
                  ON T.BogusNum = S.BogusNum  
 
  
      SET @RowCntHold = @@RowCount
      IF ( @RowCntHold BETWEEN @RowCount AND @MaxRowCount ) 
            SET @RowCount = @RowCntHold           
  
      IF @RowCntHold > @MaxRowCount 
            RAISERROR (@RowCntErrorMsg,16,1)
 

      TRUNCATE TABLE #tmp1  
  
--UtilityAccountServiceLevel  
      INSERT      #tmp1
                  ( 
                   FieldValue )
                  SELECT
                        USrv.Entity_Name
                  FROM
                        dbo.Entity USrv
                  WHERE
                        USrv.Entity_Type = '708'
                  GROUP BY
                        USrv.Entity_Name
                  ORDER BY
                        USrv.Entity_Name  
  
      UPDATE
            s
      SET   
            UtilityAccountServiceLevel = t.FieldValue
      FROM
            #tmp1 t
            INNER JOIN dbo.SOPS S
                  ON T.BogusNum = S.BogusNum  
  
      SET @RowCntHold = @@RowCount
      IF ( @RowCntHold BETWEEN @RowCount AND @MaxRowCount ) 
            SET @RowCount = @RowCntHold           
  
      IF @RowCntHold > @MaxRowCount 
            RAISERROR (@RowCntErrorMsg,16,1)


      TRUNCATE TABLE #tmp1  
  
  
--Utility  
      INSERT      #tmp1
                  ( 
                   FieldValue )
                  SELECT
                        v.Vendor_Name
                  FROM
                        dbo.Vendor v
                  WHERE
                        v.Vendor_Type_ID = '289'
                  GROUP BY
                        v.Vendor_Name
                  ORDER BY
                        Vendor_Name  
  
  
      UPDATE
            s
      SET   
            Utility = t.FieldValue
      FROM
            #tmp1 t
            INNER JOIN dbo.SOPS S
                  ON T.BogusNum = S.BogusNum  
  
      SET @RowCntHold = @@RowCount
      IF ( @RowCntHold BETWEEN @RowCount AND @MaxRowCount ) 
            SET @RowCount = @RowCntHold           
  
      IF @RowCntHold > @MaxRowCount 
            RAISERROR (@RowCntErrorMsg,16,1)


      TRUNCATE TABLE #tmp1  
  
  
--ContractNumber  
      INSERT      #tmp1
                  ( 
                   FieldValue )
                  SELECT
                        c.Ed_Contract_Number
                  FROM
                        dbo.Contract c
                  GROUP BY
                        c.Ed_Contract_Number
                  ORDER BY
                        c.Ed_Contract_Number  
  
  
      UPDATE
            s
      SET   
            ContractNumber = t.FieldValue
      FROM
            #tmp1 t
            INNER JOIN SOPS S
                  ON T.BogusNum = S.BogusNum  
  
      SET @RowCntHold = @@RowCount
      IF ( @RowCntHold BETWEEN @RowCount AND @MaxRowCount ) 
            SET @RowCount = @RowCntHold           
  
      IF @RowCntHold > @MaxRowCount 
            RAISERROR (@RowCntErrorMsg,16,1)

      TRUNCATE TABLE #tmp1  
  
--ContractType  
  
      INSERT      #tmp1
                  ( 
                   FieldValue )
                  SELECT
                        Cty.Entity_Name
                  FROM
                        dbo.Entity Cty
                  WHERE
                        Cty.Entity_Type = '129'
                  GROUP BY
                        Cty.Entity_Name
                  ORDER BY
                        Cty.Entity_Name  
  
  
      UPDATE
            s
      SET   
            ContractType = t.FieldValue
      FROM
            #tmp1 t
            INNER JOIN dbo.SOPS S
                  ON T.BogusNum = S.BogusNum  
  
      SET @RowCntHold = @@RowCount
      IF ( @RowCntHold BETWEEN @RowCount AND @MaxRowCount ) 
            SET @RowCount = @RowCntHold           
  
      IF @RowCntHold > @MaxRowCount 
            RAISERROR (@RowCntErrorMsg,16,1)
 

      TRUNCATE TABLE #tmp1  
  
--ContractedVendor  
      INSERT      #tmp1
                  ( 
                   FieldValue )
                  SELECT
                        V1.Vendor_Name
                  FROM
                        dbo.Vendor V1
                        JOIN dbo.Account acct1
                              ON acct1.Vendor_id = V1.Vendor_Id
                  WHERE
                        acct1.Account_Type_ID = '37'
                  GROUP BY
                        V1.Vendor_Name
                  ORDER BY
                        v1.Vendor_Name  
  
      UPDATE
            s
      SET   
            ContractedVendor = t.FieldValue
      FROM
            #tmp1 t
            INNER JOIN SOPS S
                  ON T.BogusNum = S.BogusNum  
  
      SET @RowCntHold = @@RowCount
      IF ( @RowCntHold BETWEEN @RowCount AND @MaxRowCount ) 
            SET @RowCount = @RowCntHold           
  
      IF @RowCntHold > @MaxRowCount 
            RAISERROR (@RowCntErrorMsg,16,1)
 

      TRUNCATE TABLE #tmp1  
  
--ContractStartDate  
      INSERT      #tmp1
                  ( 
                   FieldValue )
                  SELECT
                        c.Contract_Start_Date
                  FROM
                        dbo.Contract c
                  GROUP BY
                        c.Contract_Start_Date
                  ORDER BY
                        c.Contract_Start_Date  
  
      UPDATE
            s
      SET   
            ContractStartDate = t.FieldValue
      FROM
            #tmp1 t
            INNER JOIN dbo.SOPS S
                  ON T.BogusNum = S.BogusNum  
  
      SET @RowCntHold = @@RowCount
      IF ( @RowCntHold BETWEEN @RowCount AND @MaxRowCount ) 
            SET @RowCount = @RowCntHold           
  
      IF @RowCntHold > @MaxRowCount 
            RAISERROR (@RowCntErrorMsg,16,1)


      TRUNCATE TABLE #tmp1  
  
--ContractEndDate  
      INSERT      #tmp1
                  ( 
                   FieldValue )
                  SELECT
                        c.Contract_End_Date
                  FROM
                        dbo.Contract c
                  GROUP BY
                        c.Contract_End_Date
                  ORDER BY
                        c.Contract_End_Date  
  
      UPDATE
            s
      SET   
            ContractEndDate = t.FieldValue
      FROM
            #tmp1 t
            INNER JOIN dbo.SOPS S
                  ON T.BogusNum = S.BogusNum  
  
      SET @RowCntHold = @@RowCount
      IF ( @RowCntHold BETWEEN @RowCount AND @MaxRowCount ) 
            SET @RowCount = @RowCntHold           
  
      IF @RowCntHold > @MaxRowCount 
            RAISERROR (@RowCntErrorMsg,16,1)


      TRUNCATE TABLE #tmp1  
  
  
--SupplierAccountNumber  
      INSERT      #tmp1
                  ( 
                   FieldValue )
                  SELECT
                        Acct2.Account_Number
                  FROM
                        dbo.Account acct2
                  WHERE
                        acct2.Account_Type_Id = '37'
                  GROUP BY
                        Acct2.Account_Number
                  ORDER BY
                        Account_Number  
  
      UPDATE
            s
      SET   
            SupplierAccountNumber = t.FieldValue
      FROM
            #tmp1 t
            INNER JOIN SOPS S
                  ON T.BogusNum = S.BogusNum  
  
      SET @RowCntHold = @@RowCount
      IF ( @RowCntHold BETWEEN @RowCount AND @MaxRowCount ) 
            SET @RowCount = @RowCntHold           
  
      IF @RowCntHold > @MaxRowCount 
            RAISERROR (@RowCntErrorMsg,16,1)


      TRUNCATE TABLE #tmp1  
  
--SupplierAccountServiceLevel  
  
      INSERT      #tmp1
                  ( 
                   FieldValue )
                  SELECT
                        ssrv.Entity_Name
                  FROM
                        dbo.Entity ssrv
                  WHERE
                        ssrv.Entity_Type = '708'
                  GROUP BY
                        ssrv.Entity_Name
                  ORDER BY
                        ssrv.Entity_Name  
  
      UPDATE
            s
      SET   
            SupplierAccountServiceLevel = t.FieldValue
      FROM
            #tmp1 t
            INNER JOIN SOPS S
                  ON T.BogusNum = S.BogusNum  
 
      SET @RowCntHold = @@RowCount
      IF ( @RowCntHold BETWEEN @RowCount AND @MaxRowCount ) 
            SET @RowCount = @RowCntHold           
  
      IF @RowCntHold > @MaxRowCount 
            RAISERROR (@RowCntErrorMsg,16,1)


      TRUNCATE TABLE #tmp1  
  
--RecalculationType  
      INSERT      #tmp1
                  ( 
                   FieldValue )
                  SELECT
                        cr.Entity_Name
                  FROM
                        dbo.Entity cr
                  WHERE
                        cr.Entity_Type = '301'
                  GROUP BY
                        cr.Entity_Name
                  ORDER BY
                        cr.Entity_Name  
  
      UPDATE
            s
      SET   
            RecalculationType = t.FieldValue
      FROM
            #tmp1 t
            INNER JOIN dbo.SOPS S
                  ON T.BogusNum = S.BogusNum  
  
      SET @RowCntHold = @@RowCount
      IF ( @RowCntHold BETWEEN @RowCount AND @MaxRowCount ) 
            SET @RowCount = @RowCntHold           
  
      IF @RowCntHold > @MaxRowCount 
            RAISERROR (@RowCntErrorMsg,16,1)


      TRUNCATE TABLE #tmp1  
  
  
  
--GroupBillStatus --   
  
      UPDATE
            SOPS
      SET   
            GroupBillStatus = 'Yes'
      WHERE
            BogusNum = 1  
  
      UPDATE
            SOPS
      SET   
            GroupBillStatus = 'No'
      WHERE
            BogusNum = 2  

      TRUNCATE TABLE #tmp1  
  
--This needs to just be 'Yes' or 'No'  
  
/**************************************************************************************  
Drop the Temporary Table.  This should be the last step before you return your results  
***************************************************************************************/  
      DROP TABLE #tmp1  
  
/**************************************************************************************  
***************************************************************************************/  
  
      SELECT TOP ( @RowCount )
            ClientName
           ,City
           ,State_Province
           ,SiteName
           ,SiteID
           ,ClientNotManaged
           ,SiteNotManaged
           ,SiteClosed
           ,SiteType
           ,CommodityType
           ,AddressLine1
           ,AddressLine2
           ,ZipCode
           ,Country
           ,Region
           ,AccountNumber
           ,UtilityAccountServiceLevel
           ,AccountInvoiceNotExpected
           ,AccountNotManaged
           ,Utility
           ,MeterNumber
           ,ContractID
           ,BaseContractID
           ,ContractNumber
           ,ContractType
           ,ContractedVendor
           ,ContractStartDate
           ,ContractEndDate
           ,Contract_Pricing_Summary
           ,ContractComments
           ,Currency
           ,SupplierAccountNumber
           ,SupplierAccountServiceLevel
           ,RecalculationType
           ,Rate
           ,PurchaseMethod
           ,FullRequirements
           ,GroupBillStatus
      FROM
            dbo.SOPS so
      ORDER BY
            BogusNum  
END;
;

GO
GRANT EXECUTE ON  [dbo].[Report_DE_SOP_Data_P] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_SOP_Data_P] TO [CBMSApplication]
GO
