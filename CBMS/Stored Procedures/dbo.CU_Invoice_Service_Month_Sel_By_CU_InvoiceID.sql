SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********       
NAME:  dbo.CU_Invoice_Service_Month_Sel_By_CU_InvoiceID      
     
DESCRIPTION:  
    
INPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
	@cu_invoice_Id		int
	    
OUTPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
USAGE EXAMPLES:   

	EXEC CU_Invoice_Service_Month_Sel_By_CU_InvoiceID 24113939
		EXEC CU_Invoice_Service_Month_Sel_By_CU_InvoiceID_1 24113939
	
	
	EXEC CU_Invoice_Service_Month_Sel_By_CU_InvoiceID 131153
	
	
	EXEC CU_Invoice_Service_Month_Sel_By_CU_InvoiceID 1148523
	
	
	EXEC CU_Invoice_Service_Month_Sel_By_CU_InvoiceID 75254431

AUTHOR INITIALS:      
Initials  Name      
------------------------------------------------------------      
SSR		  Sharad Srivastava
SP		  Sandeep Pigilam
RKV       Ravi Kumar vegesna
	    
Initials Date  Modification      
------------------------------------------------------------      
SSR		02/05/2010	Created
		02/09/2010	Added Supplier Account With begin Date and End Date
SSR		03/12/2010  Added table Owner Name 	
SKA		03/17/2010	Removed the reference of CU_Invoice_Account_Map table and used CU_INVOICE_SERVICE_MONTH
					Added Group by Clause 
SSR		04/20/2010	Added ISNULL condition for Account_Number in select Clause	
SSR		05/24/2010	Added Account_type Column 		
SP		2014-08-12	Data Operations Enhancement Phase III,Added Is_Level2_Recalc_Validation_Required column	.
RKV     2015-06-25  Added State_Id,used core.client_hier_account table instead of Account table as part of AS400
RKV     2015-08-27  Added Reclac_Type and removed Is_Level2_Recalc_Validation_Required as part of AS400 PII 
RKV     2018-10-15  D20-257,Added Filter to get the recalc types between Start_Date and End_Date for the given invoice service month 
NR		2019-12-30	MAINT-9684 - Added Account Number , Supplier dates in below stored procedure.
******/

CREATE PROCEDURE [dbo].[CU_Invoice_Service_Month_Sel_By_CU_InvoiceID]
    @cu_invoice_Id INT
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            cha.Account_Id
            , cha.Display_Account_Number AS ACCOUNT_NUMBER
            , inv.ACCOUNT_GROUP_ID
            , ent.ENTITY_NAME
            , ent.ENTITY_ID AS ACCOUNT_TYPE_ID
            , cha.Meter_State_Id AS State_Id
            , cd.Code_Value Recalc_Type
            , cha.Account_Number AS Inv_Account_Number
            , cha.Supplier_Account_begin_Dt
            , cha.Supplier_Account_End_Dt
        FROM
            dbo.CU_INVOICE inv
            JOIN dbo.CU_INVOICE_SERVICE_MONTH cusm
                ON cusm.CU_INVOICE_ID = inv.CU_INVOICE_ID
            JOIN Core.Client_Hier_Account cha
                ON cusm.Account_ID = cha.Account_Id
            JOIN dbo.ENTITY ent
                ON ent.ENTITY_NAME = cha.Account_Type
                   AND  ent.ENTITY_DESCRIPTION = 'Account Type'
            LEFT OUTER JOIN dbo.Account_Commodity_Invoice_Recalc_Type acirt
                ON cha.Account_Id = acirt.Account_Id
                   AND  cha.Commodity_Id = acirt.Commodity_ID
                   AND  cusm.SERVICE_MONTH BETWEEN acirt.Start_Dt
                                           AND     ISNULL(acirt.End_Dt, '2099-01-01')
            LEFT OUTER JOIN dbo.Code cd
                ON acirt.Invoice_Recalc_Type_Cd = cd.Code_Id
        WHERE
            inv.CU_INVOICE_ID = @cu_invoice_Id
            AND (   cusm.SERVICE_MONTH IS NULL
                    OR  (   cha.Account_Type = 'Utility'
                            OR  (   cha.Account_Type = 'Supplier'
                                    AND (   cusm.Begin_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                                          AND     cha.Supplier_Account_End_Dt
                                            OR  cusm.End_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                                            AND     cha.Supplier_Account_End_Dt
                                            OR  cha.Supplier_Account_begin_Dt BETWEEN cusm.Begin_Dt
                                                                              AND     cusm.End_Dt
                                            OR  cha.Supplier_Account_End_Dt BETWEEN cusm.Begin_Dt
                                                                            AND     cusm.End_Dt))))
        GROUP BY
            cha.Account_Id
            , cha.Display_Account_Number
            , inv.ACCOUNT_GROUP_ID
            , ent.ENTITY_NAME
            , ent.ENTITY_ID
            , cha.Meter_State_Id
            , cd.Code_Value
            , cha.Account_Number
            , cha.Supplier_Account_begin_Dt
            , cha.Supplier_Account_End_Dt;
    END;

    ;

    ;



GO



GRANT EXECUTE ON  [dbo].[CU_Invoice_Service_Month_Sel_By_CU_InvoiceID] TO [CBMSApplication]
GO
