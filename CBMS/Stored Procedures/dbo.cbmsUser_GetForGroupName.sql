SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.cbmsUser_GetForGroupName            
                        
 DESCRIPTION:                        
			This will get the users based on groups assiged to them.
			                  
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@MyAccountId					INT
@group_name						VARCHAR(200)	 NULL
@group_names					VARCHAR(MAX)	 NULL
                            
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 
  exec cbmsUser_GetForGroupName 
      @MyAccountId=49
     ,@group_names='Standing Data Team'


 exec cbmsUser_GetForGroupName 
      @MyAccountId=49
     ,@group_names='Standing Data Team,Standing Data Team - Invoice Collection'  

exec cbmsUser_GetForGroupName
    @MyAccountId = 49
    , @group_names = 'Standing Data Team,Standing Data Team - Invoice Collection'
	,@User_Name_Str ='raj'
     
       SELECT top 1 ch.Client_Id FROM core.Client_Hier ch WHERE ch.Client_Name LIKE 'A J Rorato'                    
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2017-02-16       Header added for Invoice Tracking.   
										@Group_Names as additional input param with comma seperated groups.    
NR						2019-06-11		Add Contract - Added smart search.										
										
										       
                  
******/
CREATE PROCEDURE [dbo].[cbmsUser_GetForGroupName]
    (
        @MyAccountId INT
        , @group_name VARCHAR(200) = NULL
        , @group_names VARCHAR(MAX) = NULL
        , @User_Name_Str VARCHAR(200) = NULL
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
    )
AS
    BEGIN
        SET NOCOUNT ON;
        WITH Cte_Users
        AS (
               SELECT
                    ui.USER_INFO_ID
                    , ui.USERNAME
                    , ui.QUEUE_ID
                    , ui.FIRST_NAME
                    , ui.MIDDLE_NAME
                    , ui.LAST_NAME
                    , ui.EMAIL_ADDRESS
                    , ui.IS_HISTORY
                    , ui.FIRST_NAME + ' ' + ui.LAST_NAME full_name
                    , ui.ACCESS_LEVEL
                    , ui.CLIENT_ID
                    , ui.DIVISION_ID
                    , ui.SITE_ID
                    , ROW_NUMBER() OVER (ORDER BY
                                             ui.FIRST_NAME + ' ' + ui.LAST_NAME) Row_Num
                    , COUNT(1) OVER () AS Total_Row_Count
               FROM
                    dbo.GROUP_INFO gi
                    JOIN dbo.USER_INFO_GROUP_INFO_MAP map
                        ON map.GROUP_INFO_ID = gi.GROUP_INFO_ID
                    JOIN dbo.USER_INFO ui
                        ON ui.USER_INFO_ID = map.USER_INFO_ID
               WHERE
                    (   (   @group_name IS NOT NULL
                            AND gi.GROUP_NAME = @group_name)
                        OR  (   @group_names IS NOT NULL
                                AND EXISTS (   SELECT
                                                    1
                                               FROM
                                                    dbo.ufn_split(@group_names, ',') uf
                                               WHERE
                                                    uf.Segments = gi.GROUP_NAME)))
                    AND ui.IS_HISTORY != 1
                    AND (   @User_Name_Str IS NULL
                            OR  ui.FIRST_NAME LIKE '%' + @User_Name_Str + '%'
                            OR  ui.LAST_NAME LIKE '%' + @User_Name_Str + '%')
               GROUP BY
                   ui.USER_INFO_ID
                   , ui.USERNAME
                   , ui.QUEUE_ID
                   , ui.FIRST_NAME
                   , ui.MIDDLE_NAME
                   , ui.LAST_NAME
                   , ui.EMAIL_ADDRESS
                   , ui.IS_HISTORY
                   , ui.FIRST_NAME + ' ' + ui.LAST_NAME
                   , ui.ACCESS_LEVEL
                   , ui.CLIENT_ID
                   , ui.DIVISION_ID
                   , ui.SITE_ID
           )
        SELECT
            cu.USER_INFO_ID
            , cu.USERNAME
            , cu.QUEUE_ID
            , cu.FIRST_NAME
            , cu.MIDDLE_NAME
            , cu.LAST_NAME
            , cu.EMAIL_ADDRESS
            , cu.IS_HISTORY
            , cu.full_name
            , cu.ACCESS_LEVEL
            , cu.CLIENT_ID
            , cu.DIVISION_ID
            , cu.SITE_ID
            , cu.Row_Num
            , cu.Total_Row_Count
        FROM
            Cte_Users cu
        WHERE
            cu.Row_Num BETWEEN @Start_Index
                       AND     @End_Index
        ORDER BY
            cu.Row_Num;

    END;




    ;

GO

GRANT EXECUTE ON  [dbo].[cbmsUser_GetForGroupName] TO [CBMSApplication]
GO
