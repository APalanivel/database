SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_TOTAL_SITES_FOR_GROUP_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@groupId       	int       	          	
	@clientId      	int       	          	
	@hedgeTypeId   	int       	          	
	@hedgeLevelId  	int       	          	
	@dealticketId  	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--exec GET_TOTAL_SITES_FOR_GROUP_P -1,-1,41,217,587,842

CREATE  PROCEDURE [dbo].[GET_TOTAL_SITES_FOR_GROUP_P] 
@userId varchar(10),
@sessionId varchar(20),
@groupId integer,
@clientId integer,
@hedgeTypeId integer,
@hedgeLevelId integer,
@dealticketId integer

AS
set nocount on
if(@dealticketId = 0 or @dealticketId is null )

begin
SELECT s.SITE_ID,vw.site_name
	FROM SITE s,vwSiteName vw
	where s.site_id=vw.site_id and
	s.SITE_ID IN
			(
		SELECT 
			RM_ONBOARD_HEDGE_SETUP.SITE_ID 
		FROM 
			RM_ONBOARD_CLIENT,
			RM_ONBOARD_HEDGE_SETUP 
		WHERE
			RM_ONBOARD_CLIENT.CLIENT_ID=@clientId AND
			RM_ONBOARD_HEDGE_SETUP.RM_ONBOARD_CLIENT_ID=RM_ONBOARD_CLIENT.RM_ONBOARD_CLIENT_ID AND
			RM_ONBOARD_HEDGE_SETUP.HEDGE_TYPE_ID=@hedgeTypeId and
			RM_ONBOARD_HEDGE_SETUP.RM_GROUP_ID=@groupId AND
			RM_ONBOARD_HEDGE_SETUP.HEDGE_LEVEL_TYPE_ID IN
			((select entity_id from entity where entity_type=262 and entity_name='CORPORATE'),@hedgeLevelId))
		
order by vw.site_name

end

else 
begin
SELECT s.SITE_ID,vw.site_name
	FROM SITE s,vwSiteName vw
	where s.site_id=vw.site_id and
	s.SITE_ID IN
			(
		SELECT 
			RM_ONBOARD_HEDGE_SETUP.SITE_ID 
		FROM 
			RM_ONBOARD_CLIENT,
			RM_ONBOARD_HEDGE_SETUP 
		WHERE
			RM_ONBOARD_CLIENT.CLIENT_ID=@clientId AND
			RM_ONBOARD_HEDGE_SETUP.RM_ONBOARD_CLIENT_ID=RM_ONBOARD_CLIENT.RM_ONBOARD_CLIENT_ID AND
			RM_ONBOARD_HEDGE_SETUP.HEDGE_TYPE_ID=@hedgeTypeId and
			RM_ONBOARD_HEDGE_SETUP.RM_GROUP_ID=@groupId AND
			RM_ONBOARD_HEDGE_SETUP.HEDGE_LEVEL_TYPE_ID IN
			((select entity_id from entity where entity_type=262 and entity_name='CORPORATE'),@hedgeLevelId)
		
		UNION
		
		select
			distinct rm_deal_ticket_volume_details.site_id 
		from 	
			rm_deal_ticket_volume_details,rm_deal_ticket_details,rm_deal_ticket,RM_ONBOARD_CLIENT,
			RM_ONBOARD_HEDGE_SETUP
		where  
			rm_deal_ticket_volume_details.rm_deal_ticket_details_id = rm_deal_ticket_details.rm_deal_ticket_details_id
			and rm_deal_ticket_details.rm_deal_ticket_id= rm_deal_ticket.rm_deal_ticket_id 
			and rm_deal_ticket.rm_deal_ticket_id = @dealticketId
			and rm_deal_ticket.client_id = RM_ONBOARD_CLIENT.CLIENT_ID and 
			RM_ONBOARD_CLIENT.CLIENT_ID=@clientId AND
			rm_deal_ticket_volume_details.site_id = RM_ONBOARD_HEDGE_SETUP.SITE_ID AND 
			RM_ONBOARD_CLIENT.RM_ONBOARD_CLIENT_ID=RM_ONBOARD_HEDGE_SETUP.RM_ONBOARD_CLIENT_ID AND
			--RM_ONBOARD_HEDGE_SETUP.HEDGE_TYPE_ID=@hedgeTypeId and 
			RM_ONBOARD_HEDGE_SETUP.RM_GROUP_ID=@groupId 
	
			)
order by vw.site_name

end
GO
GRANT EXECUTE ON  [dbo].[GET_TOTAL_SITES_FOR_GROUP_P] TO [CBMSApplication]
GO
