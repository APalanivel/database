
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [DBO].[Bucket_Master_Sel_High_Level_Determinents_By_RateId]

DESCRIPTION:

	To get the determiinant details
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@RateId			INT
@Start_Dt		DATE
@End_Dt			DATE


OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------  

	EXEC Bucket_Master_Sel_High_Level_Determinents_By_RateId 35788,'01-01-2007','12-01-2012'
     
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
AL			Ajeesh Lonachan
RK          Raghu Kalvapudi

MODIFICATIONS
INITIALS	DATE		MODIFICATION
-----------------------------------------------------------
AL			2012-07-18	Created for Rate Engine API
RK          2014-10-02  Added UOM_Name and UOM_Id to Output.
*/
CREATE PROCEDURE [dbo].[Bucket_Master_Sel_High_Level_Determinents_By_RateId]
      (
       @RateId INT
      ,@Start_Dt DATE
      ,@End_Dt DATE )
AS
BEGIN
      SET NOCOUNT ON;
 
      WITH  RateSchedule
              AS ( SELECT
                        RS.RATE_SCHEDULE_ID
                   FROM
                        dbo.RATE_SCHEDULE RS
                   WHERE
                        RS.RATE_ID = @RateId
                        AND ( ( @Start_Dt <= RS.RS_END_DATE
                                AND @End_Dt > = Rs.RS_START_DATE )
                              OR ( RS.RS_START_DATE BETWEEN @Start_Dt AND @End_Dt )
                              OR ( RS.RS_START_DATE < @Start_Dt
                                   AND RS.RS_END_DATE IS NULL ) ))
            SELECT
                  [CHARGE_NAME] AS Name
                 ,bm.External_Reference_Key AS KeyName
                 ,'input' AS IntervalOrInput
                 ,c.CURRENCY_UNIT_ID AS UOM_Id
                 ,cu.CURRENCY_UNIT_NAME AS UOM_Name
            FROM
                  dbo.CHARGE c
                  LEFT JOIN dbo.CURRENCY_UNIT cu
                        ON cu.CURRENCY_UNIT_ID = c.CURRENCY_UNIT_ID
                  INNER JOIN dbo.ENTITY e
                        ON e.ENTITY_ID = c.CHARGE_PARENT_TYPE_ID
                  INNER JOIN dbo.entity ct
                        ON ct.ENTITY_ID = c.CHARGE_CALCULATION_TYPE_ID
                  INNER JOIN dbo.Bucket_Master bm
                        ON c.Charge_Bucket_Master_Id = bm.Bucket_Master_Id
            WHERE
                  c.CHARGE_PARENT_ID IN ( SELECT
                                                RATE_SCHEDULE_ID
                                          FROM
                                                RateSchedule )
                  AND e.ENTITY_name = 'Rate Schedule'
                  AND e.ENTITY_TYPE = 120
                  AND ct.ENTITY_NAME IN ( '$Var', '%Var', '$Unknown' )
            UNION ALL
            SELECT
                  x.Name
                 ,x.KeyName
                 ,x.IntervalOrInput
                 ,x.UOM_Id
                 ,E.ENTITY_NAME AS UOM_Name
            FROM
                  ( SELECT
                        case WHEN cd.Code_Value = 'input' THEN BILLING_DETERMINANT_NAME
                             ELSE ''
                        END AS Name
                       ,case WHEN cd.Code_Value = 'input' THEN bm.External_Reference_Key
                             ELSE ER.Code_Value
                        END AS KeyName
                       ,case WHEN cd.Code_Value = 'input' THEN cd.Code_Value
                             ELSE 'interval'
                        END AS IntervalOrInput
                       ,max(bd.BD_UNIT_TYPE_ID) AS UOM_Id
                    FROM
                        dbo.BILLING_DETERMINANT bd
                        INNER JOIN dbo.entity e
                              ON e.ENTITY_ID = bd.BD_PARENT_TYPE_ID
                        INNER JOIN dbo.Bucket_Master bm
                              ON bd.bd_Bucket_Master_Id = bm.Bucket_Master_Id
                        INNER JOIN dbo.Code cd
                              ON cd.Code_Id = bd.Interval_Manipulation_Logic_Cd
                        INNER JOIN dbo.Code er
                              ON er.Code_Id = bm.Bucket_Stream_Cd
                    WHERE
                        bd.BD_PARENT_ID IN ( SELECT
                                                RATE_SCHEDULE_ID
                                             FROM
                                                RateSchedule )
                        AND e.ENTITY_name = 'Rate Schedule'
                        AND e.ENTITY_TYPE = 120
                    GROUP BY
                        case WHEN cd.Code_Value = 'input' THEN BILLING_DETERMINANT_NAME
                             ELSE ''
                        END
                       ,case WHEN cd.Code_Value = 'input' THEN bm.External_Reference_Key
                             ELSE ER.Code_Value
                        END
                       ,case WHEN cd.Code_Value = 'input' THEN cd.Code_Value
                             ELSE 'interval'
                        END ) x
                  LEFT JOIN dbo.ENTITY E
                        ON e.ENTITY_ID = x.UOM_Id

      SELECT
            R.RATE_ID
           ,R.RATE_NAME
      FROM
            dbo.RATE R
      WHERE
            RATE_ID = @RateId 		

END
;
GO

GRANT EXECUTE ON  [dbo].[Bucket_Master_Sel_High_Level_Determinents_By_RateId] TO [CBMSApplication]
GO
