
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********       

NAME:
    dbo.RC_GET_STATES_UNDER_CLIENT_P 
       
DESCRIPTION:          
			
      
 INPUT PARAMETERS:          
                     
 Name                   DataType         Default       Description        
----------------------------------------------------------------------------------
 @userId				VARCHAR(10)
 @sessionId				VARCHAR(10)
 @clientId				INT
                
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
----------------------------------------------------------------------------------    
          
 USAGE EXAMPLES:                            
----------------------------------------------------------------------------------    

 EXEC dbo.RC_GET_STATES_UNDER_CLIENT_P 0,0,110
 
 EXEC dbo.RC_GET_STATES_UNDER_CLIENT_P 0,0,235

 EXEC dbo.RC_GET_STATES_UNDER_CLIENT_P 0,0,11235
 
 EXEC dbo.RC_GET_STATES_UNDER_CLIENT_P 0,0,12032

    
 AUTHOR INITIALS:        
       
 Initials              Name        
----------------------------------------------------------------------------------    
 NR					   Narayana Reddy                  

  
 MODIFICATIONS:      
          
 Initials              Date             Modification      
----------------------------------------------------------------------------------    
 NR                   2014-12-29		MAINT-3291 Added header and Replaced base tables with client hier table.
*********/


CREATE PROCEDURE [dbo].[RC_GET_STATES_UNDER_CLIENT_P]
      ( 
       @userId VARCHAR(10)
      ,@sessionId VARCHAR(10)
      ,@clientId INT )
AS 
BEGIN
      SET NOCOUNT ON 
      SELECT
            Ch.State_Name
           ,Ch.State_Id
      FROM
            Core.Client_Hier Ch
      WHERE
            Ch.Client_Id = @clientId
            AND Ch.State_Id IS NOT NULL
      GROUP BY
            Ch.State_Name
           ,Ch.State_Id
END

;
GO


GRANT EXECUTE ON  [dbo].[RC_GET_STATES_UNDER_CLIENT_P] TO [CBMSApplication]
GO
