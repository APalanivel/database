SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_DEALTICKET_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@clientId      	int       	          	
	@hedgeModeId   	int       	          	
	@hedgeModeText 	varchar(20)	          	
	@startDate     	datetime  	          	
	@endDate       	datetime  	          	
	@dealTicketId  	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--select * from entity where entity_type=263

--EXEC GET_DEALTICKET_DETAILS_P 1,1,10002,550,'Nymex','2006-04-01','2006-10-01',102912
--EXEC GET_DEALTICKET_DETAILS_P 1,1,228,535,'Basis','1-1-2004','12-1-2005'
--EXEC GET_DEALTICKET_DETAILS_P 1,1,228,605,'Index','1-1-2005','12-1-2005'

--EXEC GET_DEALTICKET_DETAILS_P 1,1,361,596,'Nymex','1-1-2005','3-1-2005',100040
--EXEC GET_DEALTICKET_DETAILS_P 1,1,10026,535,'Basis','1-1-2004','12-1-2005'
--EXEC GET_DEALTICKET_DETAILS_P 1,1,10026,605,'Index','1-1-2004','12-1-2005'

--select * from entity where entity_id=60

--select * from rm_deal_ticket where client_id=381

--select * from RM_DEAL_TICKET_DETAILS where RM_DEAL_TICKET_DETAILS_ID=100070

--select * from rm_deal_ticket where rm_deal_ticket_id=100070

CREATE    PROCEDURE dbo.GET_DEALTICKET_DETAILS_P
@userId varchar(10),
@sessionId varchar(20),
@clientId integer,
@hedgeModeId integer,
@hedgeModeText varchar(20),
@startDate datetime,
@endDate datetime,
@dealTicketId int--Only for Edit Re-Allocation(For Edit Screen)

AS
set nocount on

if @hedgeModeText='Nymex' OR  @hedgeModeText='Basis'



			BEGIN

				(SELECT 
					distinct RM_DEAL_TICKET.RM_DEAL_TICKET_ID,
					ent.ENTITY_NAME,
					ent1.ENTITY_NAME,
					ent2.ENTITY_NAME,
					ent3.ENTITY_NAME,
					ent4.ENTITY_NAME,
					RM_DEAL_TICKET.HEDGE_START_MONTH,
					RM_DEAL_TICKET.HEDGE_END_MONTH,
					RM_DEAL_TICKET.SITE_ID /*Added BZ4352*/
					--ent5.ENTITY_NAME
					
	
					
				FROM 
					RM_DEAL_TICKET ,
					RM_DEAL_TICKET_DETAILS,
					ENTITY ent,
					ENTITY ent1,
					ENTITY ent2,
					ENTITY ent3,
					ENTITY ent4
					
					
					
					
				WHERE	
					--RM_DEAL_TICKET.RM_DEAL_TICKET_ID=@dealTicketId AND  /* BZ 5789 */
					RM_DEAL_TICKET.CLIENT_ID=@clientId AND
					RM_DEAL_TICKET.HEDGE_MODE_TYPE_ID IN(@hedgeModeId)AND
					ent.ENTITY_ID=RM_DEAL_TICKET.PRICING_REQUEST_TYPE_ID and
					ent1.ENTITY_ID=RM_DEAL_TICKET.DEAL_TYPE_ID AND
					ent2.ENTITY_ID=RM_DEAL_TICKET.ACTION_REQUIRED_TYPE_ID AND
					ent3.ENTITY_ID=RM_DEAL_TICKET.FREQUENCY_TYPE_ID and
					ent4.entity_id in (@hedgeModeId) and
					RM_DEAL_TICKET_DETAILS.RM_DEAL_TICKET_ID=RM_DEAL_TICKET.RM_DEAL_TICKET_ID AND
					RM_DEAL_TICKET_DETAILS.month_identifier between @startDate and @endDate)
	
				UNION
	
	
				SELECT 
					distinct RM_DEAL_TICKET.RM_DEAL_TICKET_ID,
					ent.ENTITY_NAME,
					ent1.ENTITY_NAME,
					ent2.ENTITY_NAME,
					ent3.ENTITY_NAME,
					ent4.ENTITY_NAME,
					RM_DEAL_TICKET.HEDGE_START_MONTH,
					RM_DEAL_TICKET.HEDGE_END_MONTH,
					RM_DEAL_TICKET.SITE_ID /*Added BZ4352*/
					--ent5.ENTITY_NAME
	
					
				FROM 
					RM_DEAL_TICKET,
					RM_DEAL_TICKET_DETAILS,
					ENTITY ent,
					ENTITY ent1,
					ENTITY ent2,
					ENTITY ent3,
					ENTITY ent4
					
					
				WHERE
					--RM_DEAL_TICKET.RM_DEAL_TICKET_ID=@dealTicketId AND /* BZ 5789 */
					RM_DEAL_TICKET.CLIENT_ID=@clientId AND
					RM_DEAL_TICKET.HEDGE_MODE_TYPE_ID IN(SELECT entity_id from entity where entity_name='Index' and entity_type=263)AND
					ent.ENTITY_ID=RM_DEAL_TICKET.PRICING_REQUEST_TYPE_ID AND
					ent1.ENTITY_ID=RM_DEAL_TICKET.DEAL_TYPE_ID AND
					ent2.ENTITY_ID=RM_DEAL_TICKET.ACTION_REQUIRED_TYPE_ID AND
					ent3.ENTITY_ID=RM_DEAL_TICKET.FREQUENCY_TYPE_ID and
					ent4.entity_id in (SELECT entity_id from entity where entity_name='Index' and entity_type=263) and
					RM_DEAL_TICKET_DETAILS.RM_DEAL_TICKET_ID=RM_DEAL_TICKET.RM_DEAL_TICKET_ID AND
					RM_DEAL_TICKET_DETAILS.month_identifier between @startDate and @endDate 

				UNION

				(SELECT 
					DISTINCT RM_DEAL_TICKET.RM_DEAL_TICKET_ID,
					ent.ENTITY_NAME,
					ent1.ENTITY_NAME,
					ent2.ENTITY_NAME,
					ent3.ENTITY_NAME,
					ent4.ENTITY_NAME,
					RM_DEAL_TICKET.HEDGE_START_MONTH,
					RM_DEAL_TICKET.HEDGE_END_MONTH,
					RM_DEAL_TICKET.SITE_ID /*Added BZ4352*/
					

					
				FROM 
					RM_DEAL_TICKET,
					RM_DEAL_TICKET_DETAILS,
					ENTITY ent,
					ENTITY ent1,
					ENTITY ent2,
					ENTITY ent3,
					ENTITY ent4
					
				WHERE
					RM_DEAL_TICKET.RM_DEAL_TICKET_ID=@dealTicketId AND
					ent.ENTITY_ID=RM_DEAL_TICKET.PRICING_REQUEST_TYPE_ID AND
					ent1.ENTITY_ID=RM_DEAL_TICKET.DEAL_TYPE_ID AND
					ent2.ENTITY_ID=RM_DEAL_TICKET.ACTION_REQUIRED_TYPE_ID AND
					ent3.ENTITY_ID=RM_DEAL_TICKET.FREQUENCY_TYPE_ID and
					ent4.entity_id = RM_DEAL_TICKET.HEDGE_MODE_TYPE_ID AND
					RM_DEAL_TICKET_DETAILS.RM_DEAL_TICKET_ID=RM_DEAL_TICKET.RM_DEAL_TICKET_ID AND
					RM_DEAL_TICKET_DETAILS.month_identifier between @startDate and @endDate )
			END

else if(@hedgeModeText='Index')

			BEGIN

				(SELECT 

					distinct RM_DEAL_TICKET.RM_DEAL_TICKET_ID,
					ent.ENTITY_NAME,
					ent1.ENTITY_NAME,
					ent2.ENTITY_NAME,
					ent3.ENTITY_NAME,
					ent4.ENTITY_NAME,
					RM_DEAL_TICKET.HEDGE_START_MONTH,
					RM_DEAL_TICKET.HEDGE_END_MONTH,
					RM_DEAL_TICKET.SITE_ID /*Added BZ4352*/
				
				FROM 
					RM_DEAL_TICKET,
					RM_DEAL_TICKET_DETAILS,
					ENTITY ent,
					ENTITY ent1,
					ENTITY ent2,
					ENTITY ent3,
					ENTITY ent4
				WHERE
					RM_DEAL_TICKET.CLIENT_ID=@clientId AND
					
					RM_DEAL_TICKET.HEDGE_MODE_TYPE_ID IN (@hedgeModeId)
					AND
					ent.ENTITY_ID=RM_DEAL_TICKET.PRICING_REQUEST_TYPE_ID and
					ent1.ENTITY_ID=RM_DEAL_TICKET.DEAL_TYPE_ID AND
					ent2.ENTITY_ID=RM_DEAL_TICKET.ACTION_REQUIRED_TYPE_ID AND
					ent3.ENTITY_ID=RM_DEAL_TICKET.FREQUENCY_TYPE_ID and
					ent4.entity_id=@hedgeModeId and
					RM_DEAL_TICKET_DETAILS.RM_DEAL_TICKET_ID=RM_DEAL_TICKET.RM_DEAL_TICKET_ID AND
					RM_DEAL_TICKET_DETAILS.month_identifier between @startDate and @endDate)

				union

					SELECT 
					distinct RM_DEAL_TICKET.RM_DEAL_TICKET_ID,
					ent.ENTITY_NAME,
					ent1.ENTITY_NAME,
					ent2.ENTITY_NAME,
					ent3.ENTITY_NAME,
					ent4.ENTITY_NAME,
					RM_DEAL_TICKET.HEDGE_START_MONTH,
					RM_DEAL_TICKET.HEDGE_END_MONTH,
					RM_DEAL_TICKET.SITE_ID /*Added BZ4352*/
					
				FROM 
					RM_DEAL_TICKET,
					RM_DEAL_TICKET_DETAILS,
					ENTITY ent,
					ENTITY ent1,
					ENTITY ent2,
					ENTITY ent3,
					ENTITY ent4
					
					
				WHERE
					RM_DEAL_TICKET.CLIENT_ID=@clientId AND
					RM_DEAL_TICKET.HEDGE_MODE_TYPE_ID IN(SELECT entity_id from entity where entity_name='Nymex' and entity_type=263)
					AND
					ent.ENTITY_ID=RM_DEAL_TICKET.PRICING_REQUEST_TYPE_ID and
					ent1.ENTITY_ID=RM_DEAL_TICKET.DEAL_TYPE_ID AND
					ent2.ENTITY_ID=RM_DEAL_TICKET.ACTION_REQUIRED_TYPE_ID AND
					ent3.ENTITY_ID=RM_DEAL_TICKET.FREQUENCY_TYPE_ID and
					ent4.entity_id in (SELECT entity_id from entity where entity_name='Nymex' and entity_type=263) and
					RM_DEAL_TICKET_DETAILS.RM_DEAL_TICKET_ID=RM_DEAL_TICKET.RM_DEAL_TICKET_ID AND
					RM_DEAL_TICKET_DETAILS.month_identifier between @startDate and @endDate


				union

					SELECT 
					distinct RM_DEAL_TICKET.RM_DEAL_TICKET_ID,
					ent.ENTITY_NAME,
					ent1.ENTITY_NAME,
					ent2.ENTITY_NAME,
					ent3.ENTITY_NAME,
					ent4.ENTITY_NAME,
					RM_DEAL_TICKET.HEDGE_START_MONTH,
					RM_DEAL_TICKET.HEDGE_END_MONTH,
					RM_DEAL_TICKET.SITE_ID /*Added BZ4352*/
					--ent5.ENTITY_NAME
					

					
				FROM 
					RM_DEAL_TICKET,
					RM_DEAL_TICKET_DETAILS,
					ENTITY ent,
					ENTITY ent1,
					ENTITY ent2,
					ENTITY ent3,
					ENTITY ent4
					
				WHERE
					RM_DEAL_TICKET.CLIENT_ID=@clientId AND
					RM_DEAL_TICKET.HEDGE_MODE_TYPE_ID IN(SELECT entity_id from entity where entity_name='Basis' and entity_type=263)
					AND
					ent.ENTITY_ID=RM_DEAL_TICKET.PRICING_REQUEST_TYPE_ID and
					ent1.ENTITY_ID=RM_DEAL_TICKET.DEAL_TYPE_ID AND
					ent2.ENTITY_ID=RM_DEAL_TICKET.ACTION_REQUIRED_TYPE_ID AND
					ent3.ENTITY_ID=RM_DEAL_TICKET.FREQUENCY_TYPE_ID and
					ent4.entity_id in (SELECT entity_id from entity where entity_name='Basis' and entity_type=263) and
					RM_DEAL_TICKET_DETAILS.RM_DEAL_TICKET_ID=RM_DEAL_TICKET.RM_DEAL_TICKET_ID AND
					RM_DEAL_TICKET_DETAILS.month_identifier between @startDate and @endDate 

				UNION

				(SELECT 

					DISTINCT RM_DEAL_TICKET.RM_DEAL_TICKET_ID,
					ent.ENTITY_NAME,
					ent1.ENTITY_NAME,
					ent2.ENTITY_NAME,
					ent3.ENTITY_NAME,
					ent4.ENTITY_NAME,
					RM_DEAL_TICKET.HEDGE_START_MONTH,
					RM_DEAL_TICKET.HEDGE_END_MONTH,
					RM_DEAL_TICKET.SITE_ID /*Added BZ4352*/
					

					
				FROM 
					RM_DEAL_TICKET,
					RM_DEAL_TICKET_DETAILS,
					ENTITY ent,
					ENTITY ent1,
					ENTITY ent2,
					ENTITY ent3,
					ENTITY ent4
					
				WHERE
					RM_DEAL_TICKET.RM_DEAL_TICKET_ID=@dealTicketId AND
					ent.ENTITY_ID=RM_DEAL_TICKET.PRICING_REQUEST_TYPE_ID AND
					ent1.ENTITY_ID=RM_DEAL_TICKET.DEAL_TYPE_ID AND
					ent2.ENTITY_ID=RM_DEAL_TICKET.ACTION_REQUIRED_TYPE_ID AND
					ent3.ENTITY_ID=RM_DEAL_TICKET.FREQUENCY_TYPE_ID AND
					ent4.entity_id = RM_DEAL_TICKET.HEDGE_MODE_TYPE_ID AND
					RM_DEAL_TICKET_DETAILS.RM_DEAL_TICKET_ID=RM_DEAL_TICKET.RM_DEAL_TICKET_ID AND
					RM_DEAL_TICKET_DETAILS.month_identifier between @startDate and @endDate)
			END

GO
GRANT EXECUTE ON  [dbo].[GET_DEALTICKET_DETAILS_P] TO [CBMSApplication]
GO
