SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        Trade.Deal_Ticket_Participant_Contracts_Sel                      
                        
Description:                        
        
                        
Input Parameters:                        
	Name				DataType        Default     Description                          
--------------------------------------------------------------------------------  
	@Deal_Ticket_Id		INT  
                        
 Output Parameters:                              
 Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
     
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
 
	EXEC Trade.Deal_Ticket_Participant_Contracts_Sel 646  
  
Author Initials:                        
    Initials    Name                        
--------------------------------------------------------------------------------  
	RR          Raghu Reddy     
                         
 Modifications:                        
    Initials	Date		Modification                        
--------------------------------------------------------------------------------  
	RR          2019-04-25	Created GRM  
                       
******/
CREATE PROCEDURE [Trade].[Deal_Ticket_Participant_Contracts_Sel] ( @Deal_Ticket_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;

      SELECT
            con.CONTRACT_ID
           ,con.ED_CONTRACT_NUMBER + '(' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_START_DATE, 106), ' ', '-') + ' - ' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_END_DATE, 106), ' ', '-') + ')/' + suppacc.Account_Vendor_Name AS Contract_Vendor
      FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account cha
                  ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN Core.Client_Hier_Account suppacc
                  ON cha.Meter_Id = suppacc.Meter_Id
            INNER JOIN dbo.CONTRACT con
                  ON con.CONTRACT_ID = suppacc.Supplier_Contract_ID
            INNER JOIN Trade.Deal_Ticket_Filter_Participant dtptp
                  ON dtptp.Participant_Id = con.CONTRACT_ID
            INNER JOIN Trade.Deal_Ticket dt
                  ON dtptp.Deal_Ticket_Id = dt.Deal_Ticket_Id
                     AND ch.Client_Id = dt.Client_Id
      WHERE
            ch.Site_Id > 0
            AND cha.Account_Type = 'Utility'
            AND suppacc.Account_Type = 'Supplier'
            AND dt.Deal_Ticket_Id = @Deal_Ticket_Id
      GROUP BY
            con.CONTRACT_ID
           ,con.ED_CONTRACT_NUMBER + '(' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_START_DATE, 106), ' ', '-') + ' - ' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_END_DATE, 106), ' ', '-') + ')/' + suppacc.Account_Vendor_Name


END;
GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Participant_Contracts_Sel] TO [CBMSApplication]
GO
