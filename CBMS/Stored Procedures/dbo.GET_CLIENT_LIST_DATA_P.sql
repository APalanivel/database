SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE dbo.GET_CLIENT_LIST_DATA_P 
@userId varchar,
@sessionId varchar,
@displayStatusId int,
@cemTeamId int,
@cemUserId int

as
	set nocount on
if  @displayStatusId=1 

	select	 distinct entity.ENTITY_NAME,
		userinfo.LAST_NAME,
		userinfo.FIRST_NAME,
		client.CLIENT_NAME CLIENT_NAME, client.CLIENT_ID CLIENT_ID
	
	from	ENTITY entity,
		RM_CEM_TEAM rcem,
		USER_INFO userinfo,
		CLIENT_CEM_MAP cmap,
		CLIENT 	client	
	
	
	WHERE	entity.ENTITY_ID=rcem.CEM_TEAM_TYPE_ID AND
		rcem.CEM_USER_ID=userinfo.USER_INFO_ID  AND
		rcem.CEM_USER_ID=cmap.USER_INFO_ID AND 
		cmap.CLIENT_ID=client.CLIENT_ID 
	
	order by ENTITY_NAME,LAST_NAME,CLIENT_NAME


ELSE IF @displayStatusId=2 

	select	 distinct entity.ENTITY_NAME,
		userinfo.LAST_NAME,
		userinfo.FIRST_NAME,
		client.CLIENT_NAME CLIENT_NAME,client.CLIENT_ID CLIENT_ID
 	
	from	ENTITY entity,
		RM_CEM_TEAM rcem,
		USER_INFO userinfo,
		CLIENT_CEM_MAP cmap,
		CLIENT 	client	
	
	
	WHERE	entity.ENTITY_ID=rcem.CEM_TEAM_TYPE_ID AND
		rcem.CEM_USER_ID=userinfo.USER_INFO_ID  AND
		rcem.CEM_USER_ID=cmap.USER_INFO_ID AND 
		cmap.CLIENT_ID=client.CLIENT_ID AND
		entity.ENTITY_ID=@cemTeamId
	
	order by ENTITY_NAME,LAST_NAME,CLIENT_NAME

ELSE IF @displayStatusId=3 

	select	 distinct entity.ENTITY_NAME,
		userinfo.LAST_NAME,
		userinfo.FIRST_NAME,
		client.CLIENT_NAME,client.CLIENT_ID
	
	from	ENTITY entity,
		RM_CEM_TEAM rcem,
		USER_INFO userinfo,
		CLIENT_CEM_MAP cmap,
		CLIENT 	client	
	
	
	WHERE	entity.ENTITY_ID=rcem.CEM_TEAM_TYPE_ID AND
		rcem.CEM_USER_ID=userinfo.USER_INFO_ID  AND
		rcem.CEM_USER_ID=cmap.USER_INFO_ID AND 
		cmap.CLIENT_ID=client.CLIENT_ID AND
		rcem.CEM_USER_ID=@cemUserId
	
	order by ENTITY_NAME,LAST_NAME,CLIENT_NAME

ELSE IF @displayStatusId=4 

	select	 distinct entity.ENTITY_NAME,
		userinfo.LAST_NAME,
		userinfo.FIRST_NAME,
		client.CLIENT_NAME,client.CLIENT_ID
	
	from	ENTITY entity,
		RM_CEM_TEAM rcem,
		USER_INFO userinfo,
		CLIENT_CEM_MAP cmap,
		CLIENT 	client	
	
	
	WHERE	entity.ENTITY_ID=rcem.CEM_TEAM_TYPE_ID AND
		rcem.CEM_USER_ID=userinfo.USER_INFO_ID  AND
		rcem.CEM_USER_ID=cmap.USER_INFO_ID AND 
		cmap.CLIENT_ID=client.CLIENT_ID AND
		entity.ENTITY_ID=@cemTeamId AND
		rcem.CEM_USER_ID=@cemUserId

	order by ENTITY_NAME,LAST_NAME,CLIENT_NAME
GO
GRANT EXECUTE ON  [dbo].[GET_CLIENT_LIST_DATA_P] TO [CBMSApplication]
GO
