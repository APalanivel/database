SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
		[dbo].[Sourcing_Analyst_Mappings_Sel_By_User_Info_Id]

DESCRIPTION:

INPUT PARAMETERS:
	Name            DataType        Default    Description
------------------------------------------------------------
    @User_Info_Id	INT
      
      
OUTPUT PARAMETERS:
	Name            DataType        Default    Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	SELECT * FROM dbo.Vendor_Commodity_Analyst_Map
	EXEC dbo.Sourcing_Analyst_Mappings_Sel_By_User_Info_Id 21461
	EXEC dbo.Sourcing_Analyst_Mappings_Sel_By_User_Info_Id 26957
	

AUTHOR INITIALS:
    Initials    Name
------------------------------------------------------------
	RR			Raghu Reddy
	
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RR			2015-01-05	 Created MAINT-3304 Move User to History CBMS enhancement

******/

CREATE PROCEDURE [dbo].[Sourcing_Analyst_Mappings_Sel_By_User_Info_Id] ( @User_Info_Id AS INT )
AS 
BEGIN
    
      SET NOCOUNT ON
    
    
      SELECT
            con.COUNTRY_NAME
           ,st.STATE_NAME
           ,vndr.VENDOR_NAME
           ,com.Commodity_Name
      FROM
            dbo.Vendor_Commodity_Analyst_Map vcam
            INNER JOIN dbo.VENDOR_COMMODITY_MAP vcm
                  ON vcm.VENDOR_COMMODITY_MAP_ID = vcam.Vendor_Commodity_Map_Id
            INNER JOIN dbo.VENDOR vndr
                  ON vndr.VENDOR_ID = vcm.VENDOR_ID
            INNER JOIN dbo.Commodity com
                  ON com.Commodity_Id = vcm.COMMODITY_TYPE_ID
            INNER JOIN dbo.VENDOR_STATE_MAP vsm
                  ON vsm.VENDOR_ID = vndr.VENDOR_ID
            INNER JOIN dbo.STATE st
                  ON st.STATE_ID = vsm.STATE_ID
            INNER JOIN dbo.COUNTRY con
                  ON con.COUNTRY_ID = st.COUNTRY_ID
      WHERE
            vcam.Analyst_ID = @User_Info_Id
            
END

;
GO
GRANT EXECUTE ON  [dbo].[Sourcing_Analyst_Mappings_Sel_By_User_Info_Id] TO [CBMSApplication]
GO
