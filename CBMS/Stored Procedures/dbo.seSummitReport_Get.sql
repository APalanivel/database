SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE    Procedure dbo.seSummitReport_Get
	(
		@ReportId int
	)
As
BEGIN
	set nocount on
	 select r.ReportId
				, r.ReportTypeId
				, r.ReportDate
				, r.Headline
				, r.DirFilename
				, r.StatusId
				, r.ContentTypeId
				, c.TypeLabel
				, c.TypeCode
				, r.HideFromDemo
				, r.ClientName
				, r.cbms_image_id
		from seSummitReport r
		 join seContentType c on r.ContentTypeId = c.ContentTypeId
		where ReportId = @ReportId
END
GO
GRANT EXECUTE ON  [dbo].[seSummitReport_Get] TO [CBMSApplication]
GO
