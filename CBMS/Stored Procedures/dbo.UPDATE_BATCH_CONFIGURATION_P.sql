SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--exec dbo.UPDATE_BATCH_CONFIGURATION_P '1','1',26,'Hedge allocation Report',0,399
--select * from report_list
CREATE    PROCEDURE dbo.UPDATE_BATCH_CONFIGURATION_P

@userId varchar(10),
@sessionId varchar(20),
@masterId int,
@reportName varchar(200),
@corporateReport int,
@clientId int


as
	set nocount on
declare @reportId int, @newClient int

select @reportId=report_list_id from report_list where 
	report_name =@reportName

select @newClient=count(*) from RM_BATCH_CONFIGURATION 
	where RM_BATCH_CONFIGURATION_MASTER_ID=@masterId and
		client_id=@clientId


-- here 6 is the count of batch reports if no. of  reports 
-- change that this no. will also change

if @newClient=6

	update  RM_BATCH_CONFIGURATION set 
			CORPORATE_REPORT=@corporateReport
	where 	REPORT_LIST_ID=@reportId
		and RM_BATCH_CONFIGURATION_MASTER_ID=@masterId
		and client_id=@clientId
else
	insert into RM_BATCH_CONFIGURATION 
		(RM_BATCH_CONFIGURATION_MASTER_ID,
			client_id,report_list_id,corporate_report)values
		(@masterId,@clientId,@reportId,@corporateReport)

GO
GRANT EXECUTE ON  [dbo].[UPDATE_BATCH_CONFIGURATION_P] TO [CBMSApplication]
GO
