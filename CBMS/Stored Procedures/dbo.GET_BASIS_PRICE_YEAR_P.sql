SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_BASIS_PRICE_YEAR_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@forecastType  	int       	          	
	@forecastTypeName	varchar(200)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE   PROCEDURE dbo.GET_BASIS_PRICE_YEAR_P

@userId varchar(10),
@sessionId varchar(20),
@forecastType int, 
@forecastTypeName varchar(200)

AS
select datepart(YEAR,min(month_identifier)) year from 
rm_forecast_details where rm_forecast_id in
(
	select rm_forecast_id from rm_forecast
	where forecast_type_id=
	(
		select ENTITY_ID FROM ENTITY WHERE ENTITY_TYPE=@forecastType AND 
		ENTITY_NAME=@forecastTypeName
	)
)
GO
GRANT EXECUTE ON  [dbo].[GET_BASIS_PRICE_YEAR_P] TO [CBMSApplication]
GO
