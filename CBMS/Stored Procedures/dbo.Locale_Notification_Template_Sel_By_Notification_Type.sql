SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 dbo.Locale_Notification_Template_Sel_By_Notification_Type    
 
DESCRIPTION:  

INPUT PARAMETERS:    
Name						DataType	Default			Description    
----------------------------------------------------------------    
@Contract_Id				INT
@Notification_Msg_Queue_Id	INT				

OUTPUT PARAMETERS:    
Name					DataType	Default			Description    
----------------------------------------------------------------    
 
USAGE EXAMPLES:    
----------------------------------------------------------------    
    
SELECT
      rf.Resource_File_Name
     ,rk.Resource_Key
     ,cd.Code_Value
     ,cd.Code_Id
     ,cd.Code_Dsc
     ,lv.Language_Cd
     ,lv.Locale_Value
FROM
      Resource_CS.dbo.Resource_File rf
      INNER JOIN Resource_CS.dbo.Resource_File_Key rfk
            ON rf.Resource_File_Id = rfk.Resource_File_Id
      INNER JOIN Resource_CS.dbo.Resource_Key rk
            ON rfk.Resource_Key_Id = rk.Resource_Key_Id
      INNER JOIN Resource_CS.dbo.Resource_Key_Locale_Value lv
            ON rfk.Resource_File_Key_Id = lv.Resource_File_Key_Id
      INNER JOIN dbo.Code cd
            ON lv.Language_Cd = cd.Code_Id
WHERE
      rf.Resource_File_Name = 'CbmsSourcingNotification'
      
EXEC dbo.Locale_Notification_Template_Sel_By_Notification_Type 'Termination Vendor'
EXEC dbo.Locale_Notification_Template_Sel_By_Notification_Type 'Termination Vendor',100465
EXEC dbo.Locale_Notification_Template_Sel_By_Notification_Type 'Registration Notification'
EXEC dbo.Locale_Notification_Template_Sel_By_Notification_Type 'Registration Notification',100465


EXEC dbo.Locale_Notification_Template_Sel_By_Notification_Type 'Termination Vendor'
EXEC dbo.Notification_Template_Sel_By_Notification_Type 'Termination Vendor'

EXEC dbo.SR_GET_EMAIL_TEMPLATE_P 'Email-038-LP-EP-EmailRequest-More75_CEM_LPContact'
EXEC dbo.Locale_Notification_Template_Sel_By_Notification_Type 'Email-038-LP-EP-EmailRequest-More75_CEM_LPContact'


AUTHOR INITIALS:    
Initials	Name    
----------------------------------------------------------------    
RR			Raghu Reddy

MODIFICATIONS:
Initials	Date		Modification    
----------------------------------------------------------------    
RR			2016-07-13	GCS - 5b - Created					
						
    
******/    
    
CREATE PROCEDURE [dbo].[Locale_Notification_Template_Sel_By_Notification_Type]
      ( 
       @Notification_Type VARCHAR(100)
      ,@Locale_Cd INT = NULL )
AS 
BEGIN    
    
      SET NOCOUNT ON; 
      
      DECLARE
            @Locale_Email_Subject NVARCHAR(MAX)
           ,@Locale_Email_Body NVARCHAR(MAX);
      WITH  Cte_Subject
              AS ( SELECT
                        lv.Language_Cd
                       ,lv.Locale_Value
                   FROM
                        Resource_CS.dbo.Resource_File rf
                        INNER JOIN Resource_CS.dbo.Resource_File_Key rfk
                              ON rf.Resource_File_Id = rfk.Resource_File_Id
                        INNER JOIN Resource_CS.dbo.Resource_Key rk
                              ON rfk.Resource_Key_Id = rk.Resource_Key_Id
                        INNER JOIN Resource_CS.dbo.Resource_Key_Locale_Value lv
                              ON rfk.Resource_File_Key_Id = lv.Resource_File_Key_Id
                        INNER JOIN dbo.Code cd
                              ON lv.Language_Cd = cd.Code_Id
                   WHERE
                        rf.Resource_File_Name = 'CbmsSourcingNotification'
                        AND rk.Resource_Key = 'CbmsSourcingNotification' + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@Notification_Type, SPACE(1), SPACE(0)), '-', SPACE(0)), ')', SPACE(0)), '(', SPACE(0)), '_', SPACE(0)) + 'Subject'
                        AND ( @Locale_Cd IS NULL
                              OR lv.Language_Cd = @Locale_Cd )),
            Cte_Body
              AS ( SELECT
                        lv.Language_Cd
                       ,lv.Locale_Value
                   FROM
                        Resource_CS.dbo.Resource_File rf
                        INNER JOIN Resource_CS.dbo.Resource_File_Key rfk
                              ON rf.Resource_File_Id = rfk.Resource_File_Id
                        INNER JOIN Resource_CS.dbo.Resource_Key rk
                              ON rfk.Resource_Key_Id = rk.Resource_Key_Id
                        INNER JOIN Resource_CS.dbo.Resource_Key_Locale_Value lv
                              ON rfk.Resource_File_Key_Id = lv.Resource_File_Key_Id
                        INNER JOIN dbo.Code cd
                              ON lv.Language_Cd = cd.Code_Id
                   WHERE
                        rf.Resource_File_Name = 'CbmsSourcingNotification'
                        AND rk.Resource_Key = 'CbmsSourcingNotification' + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@Notification_Type, SPACE(1), SPACE(0)), '-', SPACE(0)), ')', SPACE(0)), '(', SPACE(0)), '_', SPACE(0)) + 'Body'
                        AND ( @Locale_Cd IS NULL
                              OR lv.Language_Cd = @Locale_Cd ))
            SELECT
                  cd.Code_Id AS Locale_Cd
                 ,CD.Code_Value AS Locale
                 ,ctes.Locale_Value AS Locale_Email_Subject
                 ,cteb.Locale_Value AS Locale_Email_Body
            FROM
                  dbo.Code cd
                  INNER JOIN dbo.Codeset cs
                        ON cd.Codeset_Id = cs.Codeset_Id
                  LEFT JOIN Cte_Subject ctes
                        ON ctes.Language_Cd = cd.Code_Id
                  LEFT JOIN Cte_Body cteb
                        ON cteb.Language_Cd = cd.Code_Id
            WHERE
                  cs.Codeset_Name = 'LocalizationLanguage'
                  AND ( @Locale_Cd IS NULL
                        OR cd.Code_Id = @Locale_Cd )
      
      
        

END;

;
GO
GRANT EXECUTE ON  [dbo].[Locale_Notification_Template_Sel_By_Notification_Type] TO [CBMSApplication]
GO
