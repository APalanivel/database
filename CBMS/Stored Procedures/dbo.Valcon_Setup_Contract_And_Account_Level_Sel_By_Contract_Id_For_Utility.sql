SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
                         
 NAME: dbo.Valcon_Setup_Contract_And_Account_Level_Sel_By_Contract_Id_For_Utility                     
                          
 DESCRIPTION:      
		Get the Contract & Account Level OCT rules.                   
                          
 INPUT PARAMETERS:      
                         
 Name                               DataType          Default       Description      
-------------------------------------------------------------------------------------  
 @Contract_Id						INT    

 OUTPUT PARAMETERS:      
                               
 Name                               DataType          Default       Description      
-------------------------------------------------------------------------------------                            
 USAGE EXAMPLES:                              
-------------------------------------------------------------------------------------                 

select * from Account_Commodity_Invoice_Recalc_Type where account_id=333493
select * from contract where contract_id=159815

EXEC dbo.Valcon_Setup_Contract_And_Account_Level_Sel_By_Contract_Id_For_Utility
    @Contract_Id = 172894
                 

EXEC dbo.Valcon_Setup_Contract_And_Account_Level_Sel_By_Contract_Id_For_Utility
    @Contract_Id = 167450

 EXEC  Valcon_Setup_Contract_And_Account_Level_Sel_By_Contract_Id_For_Utility 172986

 exec Valcon_Setup_Contract_And_Account_Level_Sel_By_Contract_Id_For_Utility 159815

                         
 AUTHOR INITIALS:    
       
 Initials                   Name      
-------------------------------------------------------------------------------------  
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
-------------------------------------------------------------------------------------  
 NR                     2019-06-13      Created for ADD Contract.                        
                         
******/
CREATE PROCEDURE [dbo].[Valcon_Setup_Contract_And_Account_Level_Sel_By_Contract_Id_For_Utility]
    (
        @Contract_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        CREATE TABLE #Client_Hier_Account
             (
                 Account_Id INT
                 , Account_Number VARCHAR(50)
                 , Meter_Id INT
                 , Contract_Start_Date DATE
                 , Contract_End_Date DATE
                 , Client_Id INT
                 , SIte_ID INT
             );


        INSERT INTO #Client_Hier_Account
             (
                 Account_Id
                 , Account_Number
                 , Meter_Id
                 , Contract_Start_Date
                 , Contract_End_Date
                 , Client_Id
                 , SIte_ID
             )
        SELECT
            utility_cha.Account_Id
            , utility_cha.Account_Number
            , utility_cha.Meter_Id
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE
            , ch.Client_Id
            , ch.Site_Id
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account utility_cha
                ON utility_cha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN Core.Client_Hier_Account Supplier_Cha
                ON Supplier_Cha.Meter_Id = utility_cha.Meter_Id
            INNER JOIN dbo.CONTRACT c
                ON Supplier_Cha.Supplier_Contract_ID = c.CONTRACT_ID
        WHERE
            Supplier_Cha.Supplier_Contract_ID = @Contract_Id
            AND utility_cha.Account_Type = 'Utility'
            AND Supplier_Cha.Account_Type = 'Supplier'
        GROUP BY
            utility_cha.Account_Id
            , utility_cha.Account_Number
            , utility_cha.Meter_Id
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE
            , ch.Client_Id
            , ch.Site_Id;




        SELECT
            'Account Level' AS Configuration_Level
            , cha.Account_Number
            , cha.Account_Id
            , COUNT(DISTINCT cha.Meter_Id) AS Meter
            , CASE WHEN LEN(uerc.Invoice_Recalc_Type) > 1 THEN
                       LEFT(uerc.Invoice_Recalc_Type, LEN(uerc.Invoice_Recalc_Type) - 1)
                  ELSE uerc.Invoice_Recalc_Type
              END AS Recalc_Type
            , cha.Client_Id
            , cha.SIte_ID
        FROM
            #Client_Hier_Account cha
            CROSS APPLY
        (   SELECT
                CAST(clsrt_Valu.Invoice_Recalc_Type_Cd AS VARCHAR(100)) + '|' + Sup_Recalc_valu.Code_Value + ','
            FROM
                dbo.Account_Commodity_Invoice_Recalc_Type clsrt_Valu
                INNER JOIN dbo.Code Sup_Recalc_valu
                    ON Sup_Recalc_valu.Code_Id = clsrt_Valu.Invoice_Recalc_Type_Cd
            WHERE
                clsrt_Valu.Account_Id = cha.Account_Id
                AND (   cha.Contract_Start_Date BETWEEN clsrt_Valu.Start_Dt
                                                AND     ISNULL(clsrt_Valu.End_Dt, '2099-12-31')
                        OR  cha.Contract_End_Date BETWEEN clsrt_Valu.Start_Dt
                                                  AND     ISNULL(clsrt_Valu.End_Dt, '2099-12-31')
                        OR  clsrt_Valu.Start_Dt BETWEEN cha.Contract_Start_Date
                                                AND     cha.Contract_End_Date
                        OR  ISNULL(clsrt_Valu.End_Dt, '2099-12-31') BETWEEN cha.Contract_Start_Date
                                                                    AND     cha.Contract_End_Date)
            GROUP BY
                Sup_Recalc_valu.Code_Value
                , clsrt_Valu.Invoice_Recalc_Type_Cd
            FOR XML PATH('')) uerc(Invoice_Recalc_Type)
        GROUP BY
            cha.Account_Number
            , cha.Account_Id
            , CASE WHEN LEN(uerc.Invoice_Recalc_Type) > 1 THEN
                       LEFT(uerc.Invoice_Recalc_Type, LEN(uerc.Invoice_Recalc_Type) - 1)
                  ELSE uerc.Invoice_Recalc_Type
              END
            , cha.Client_Id
            , cha.SIte_ID;


    END;




GO
GRANT EXECUTE ON  [dbo].[Valcon_Setup_Contract_And_Account_Level_Sel_By_Contract_Id_For_Utility] TO [CBMSApplication]
GO
