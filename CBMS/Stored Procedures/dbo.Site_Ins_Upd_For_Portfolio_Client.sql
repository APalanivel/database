
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME:  dbo.Site_Ins_Upd_For_Portfolio_Client      

DESCRIPTION:          

          

INPUT PARAMETERS:          

 Name					DataType			Default		Description          
 -------------------------------------------------------------------------------------------------------    
  @Message				XML								XML string of changes to the client heir table          

 ,@Conversation_Handle  UNIQUEIDENTIFER					Conversation Handle that sent the message           

          

OUTPUT PARAMETERS:          

 Name      DataType  Default Description          
 ------------------------------------------------------------------------------------------------------          

          

 USAGE EXAMPLES:          
 ------------------------------------------------------------------------------------------------------    

 This sp should be executed from SQL Server Service Broker   
          

 AUTHOR INITIALS:          

 Initials	Name          
 ------------------------------------------------------------------------------------------------------    
 TP			Anoop
 KVK		VInay K


MODIFICATIONS          

 Initials		Date			Modification          
------------------------------------------------------------------------------------------------------          
 TP				2014-06-23		Created   
 KVK			2014-10-09		While updating SIte the audit type should be 2
******/ 
CREATE PROCEDURE [dbo].[Site_Ins_Upd_For_Portfolio_Client]
      (
       @Message XML
      ,@Conversation_Handle UNIQUEIDENTIFIER )
AS
BEGIN

      SET NOCOUNT ON


      DECLARE
            @Portfolio_Client_Id INT
           ,@Division_Id INT
           ,@Site_Id INT
           ,@Site_name VARCHAR(200)
           ,@Opcode CHAR(1)
           ,@Portfolio_Client_Site_Id INT
           ,@Client_Id INT
           ,@User_Info_Id INT
           ,@Portfolio_Client_Hier_Reference_Number INT 

		   
      SELECT
            @Site_Id = cng.ch.value('Site_Id[1]', 'INT')
           ,@Client_Id = cng.ch.value('Client_Id[1]', 'INT')
           ,@Opcode = cng.ch.value('Op_Code[1]', 'VARCHAR')
      FROM
            @Message.nodes('/Site_Info/Site') cng ( ch ) 

      SELECT
            @Portfolio_Client_Hier_Reference_Number = ch.Client_Hier_Id
           ,@Portfolio_Client_Id = ch.Portfolio_Client_Id
      FROM
            core.Client_Hier ch
      WHERE
            ch.Site_Id = @Site_Id

      SELECT
            @User_Info_Id = ui.User_Info_Id
      FROM
            dbo.USER_INFO AS ui
      WHERE
            ui.username = 'conversion'


      DECLARE @Portfolio_Client_Ids TABLE
            (
             Portfolio_Client_Id INT )

      INSERT      INTO @Portfolio_Client_Ids
                  ( Portfolio_Client_Id )
                  SELECT
                        us.Segments
                  FROM
                        dbo.App_Config AS ac
                        CROSS APPLY dbo.ufn_split(ac.App_Config_Value, ',') AS us
                  WHERE
                        ac.App_Config_Cd = 'Portfolio_ClientHier_Management'



 
      IF EXISTS ( SELECT
                        1
                  FROM
                        @Portfolio_Client_Ids pci
                  WHERE
                        pci.Portfolio_Client_Id = @Portfolio_Client_Id )
            BEGIN


                  SELECT
                        @Division_Id = s.Sitegroup_Id
                  FROM
                        dbo.Sitegroup AS s
                  WHERE
                        s.Client_Id = @Portfolio_Client_Id
                        AND s.Portfolio_Client_Hier_Reference_Number = ( SELECT
                                                                              ch.Client_Hier_Id
                                                                         FROM
                                                                              core.Client_Hier ch
                                                                         WHERE
                                                                              ch.Client_Id = @Client_Id
                                                                              AND ch.Sitegroup_Id = 0 )


                  SELECT
                        @Portfolio_Client_Site_Id = s.SITE_ID
                  FROM
                        dbo.SITE AS s
                  WHERE
                        s.Client_ID = @Portfolio_Client_Id
                        AND s.DIVISION_ID = @Division_Id
                        AND s.Portfolio_Client_Hier_Reference_Number = @Portfolio_Client_Hier_Reference_Number
                      

                  DECLARE
                        @SITE_TYPE_ID INT
                       ,@UBMSITE_ID VARCHAR(30)
                       ,@SITE_PRODUCT_SERVICE VARCHAR(2000)
                       ,@PRODUCTION_SCHEDULE VARCHAR(2000)
                       ,@SHUTDOWN_SCHEDULE VARCHAR(2000)
                       ,@IS_ALTERNATE_POWER BIT
                       ,@IS_ALTERNATE_GAS BIT
                       ,@DOING_BUSINESS_AS VARCHAR(200)
                       ,@NAICS_CODE VARCHAR(30)
                       ,@TAX_NUMBER VARCHAR(30)
                       ,@DUNS_NUMBER VARCHAR(30)
                       ,@CLOSED BIT
                       ,@Closed_By_Id INTEGER
                       ,@closed_date DATETIME
                       ,@NOT_MANAGED BIT
                       ,@CONTRACTING_ENTITY VARCHAR(200)
                       ,@CLIENT_LEGAL_STRUCTURE VARCHAR(4000)
                       ,@firstName VARCHAR(50)
                       ,@lastName VARCHAR(50)
                       ,@contactEmailId VARCHAR(50)
                       ,@emailCC VARCHAR(400)
                       ,@emailApproval BIT
                       ,@dbViewApproval BIT
                       ,@Analyst_Mapping_Cd INT
                       ,@PRIMARY_ADDRESS_ID INT
                       ,@Weather_Station_Cd VARCHAR(10)
                       ,@Is_Analyst_Mapping_Cd_Changed BIT
                       ,@site_reference_number VARCHAR(30)


                  SELECT
                        @Site_Name = SITE_NAME
                       ,@SITE_TYPE_ID = SITE_TYPE_ID
                       ,@UBMSITE_ID = UBMSITE_ID
                       ,@SITE_PRODUCT_SERVICE = SITE_PRODUCT_SERVICE
                       ,@PRODUCTION_SCHEDULE = PRODUCTION_SCHEDULE
                       ,@SHUTDOWN_SCHEDULE = SHUTDOWN_SCHEDULE
                       ,@IS_ALTERNATE_POWER = IS_ALTERNATE_POWER
                       ,@IS_ALTERNATE_GAS = IS_ALTERNATE_GAS
                       ,@DOING_BUSINESS_AS = DOING_BUSINESS_AS
                       ,@NAICS_CODE = NAICS_CODE
                       ,@TAX_NUMBER = TAX_NUMBER
                       ,@DUNS_NUMBER = DUNS_NUMBER
                       ,@CLOSED = CLOSED
                       ,@Closed_By_Id = CLOSED_BY_ID
                       ,@closed_date = CLOSED_DATE
                       ,@NOT_MANAGED = NOT_MANAGED
                       ,@CONTRACTING_ENTITY = CONTRACTING_ENTITY
                       ,@CLIENT_LEGAL_STRUCTURE = CLIENT_LEGAL_STRUCTURE
                       ,@firstName = LP_CONTACT_FIRST_NAME
                       ,@lastName = LP_CONTACT_LAST_NAME
                       ,@contactEmailId = LP_CONTACT_EMAIL_ADDRESS
                       ,@emailCC = LP_CONTACT_CC
                       ,@emailApproval = IS_PREFERENCE_BY_EMAIL
                       ,@dbViewApproval = IS_PREFERENCE_BY_DV
                       ,@Analyst_Mapping_Cd = Analyst_Mapping_Cd
                       ,@PRIMARY_ADDRESS_ID = PRIMARY_ADDRESS_ID
                       ,@Weather_Station_Cd = Weather_Station_Code
                       ,@site_reference_number = SITE_REFERENCE_NUMBER
                  FROM
                        dbo.SITE
                  WHERE
                        SITE_ID = @Site_Id	

				-- Variables to add address for new site.

                  DECLARE
                        @addressTypeId INT
                       ,@stateId INT
                       ,@addressLine1 VARCHAR(200)
                       ,@addressLine2 VARCHAR(200)
                       ,@city VARCHAR(80)
                       ,@zipcode VARCHAR(20)
                       ,@addressParentId INT
                       ,@addressParentTypeId INT
                       ,@latitude DECIMAL(32, 16)
                       ,@longitude DECIMAL(32, 16)
                       ,@addressId INT
                       ,@Is_System_Generated_Geocode BIT 

		--,@addressParentId this Variable is loaded with duplcate Site_Id

                  SELECT
                        @addressTypeId = a.ADDRESS_TYPE_ID
                       ,@stateId = a.STATE_ID
                       ,@addressLine1 = a.ADDRESS_LINE1
                       ,@addressLine2 = a.ADDRESS_LINE2
                       ,@city = a.city
                       ,@zipcode = a.zipcode
                       ,@addressParentTypeId = a.ADDRESS_PARENT_TYPE_ID
                       ,@latitude = a.GEO_LAT
                       ,@longitude = a.GEO_LONG
                       ,@addressId = a.ADDRESS_ID
                       ,@Is_System_Generated_Geocode = a.Is_System_Generated_Geocode
                  FROM
                        dbo.ADDRESS a
                        INNER JOIN site s
                              ON s.PRIMARY_ADDRESS_ID = a.ADDRESS_ID
                  WHERE
                        s.SITE_ID = @site_Id


                  IF ( @Portfolio_Client_Site_Id IS NULL )
                        BEGIN


                                    
                              EXEC dbo.CBMS_ADD_SITE_P
                                    @SITE_TYPE_ID = @SITE_TYPE_ID
                                   ,@SITE_NAME = @Site_Name
                                   ,@DIVISION_ID = @Division_Id
                                   ,@UBMSITE_ID = @UBMSITE_ID
                                   ,@SITE_REFERENCE_NUMBER = @site_reference_number
                                   ,@SITE_PRODUCT_SERVICE = @SITE_PRODUCT_SERVICE
                                   ,@PRODUCTION_SCHEDULE = @PRODUCTION_SCHEDULE
                                   ,@SHUTDOWN_SCHEDULE = @SHUTDOWN_SCHEDULE
                                   ,@IS_ALTERNATE_POWER = @IS_ALTERNATE_POWER
                                   ,@IS_ALTERNATE_GAS = @IS_ALTERNATE_GAS
                                   ,@DOING_BUSINESS_AS = @DOING_BUSINESS_AS
                                   ,@NAICS_CODE = @NAICS_CODE
                                   ,@TAX_NUMBER = @TAX_NUMBER
                                   ,@DUNS_NUMBER = @DUNS_NUMBER
                                   ,@CLOSED = @CLOSED
                                   ,@NOT_MANAGED = @NOT_MANAGED
                                   ,@CONTRACTING_ENTITY = @CONTRACTING_ENTITY
                                   ,@CLIENT_LEGAL_STRUCTURE = @CLIENT_LEGAL_STRUCTURE
                                   ,@firstName = @firstName
                                   ,@lastName = @lastName
                                   ,@contactEmailId = @contactEmailId
                                   ,@emailCC = @emailCC
                                   ,@emailApproval = @emailApproval
                                   ,@dbViewApproval = @dbViewApproval
                                   ,@Client_Id = @Portfolio_Client_Id
                                   ,@Analyst_Mapping_Cd = @Analyst_Mapping_Cd
                                   ,@siteId = @Portfolio_Client_Site_Id OUTPUT
                                   ,@Portfolio_Client_Hier_Reference_Number = @Portfolio_Client_Hier_Reference_Number



                              EXEC CBMS_ADD_SITE_ADDRESS_P
                                    @addressTypeId
                                   ,@stateId
                                   ,@addressLine1
                                   ,@addressLine2
                                   ,@city
                                   ,@zipcode
                                   ,@Portfolio_Client_Site_Id
                                   ,@addressParentTypeId
                                   ,@latitude
                                   ,@longitude
                                   ,@addressId OUT
                                   ,@Is_System_Generated_Geocode
		
                              EXECUTE CBMS_UPDATE_SITE_ADDRESSID_P
                                    @addressId
                                   ,@Portfolio_Client_Site_Id
		

                              EXEC dbo.ADD_ENTITY_AUDIT_ITEM_P
                                    @entity_identifier = @Portfolio_Client_Site_Id
                                   ,@user_info_id = @User_Info_Id
                                   ,@audit_type = 1
                                   ,@entity_name = 'SITE_TABLE'
                                   ,@entity_type = 500

								--o	Site_Upd_Weather_Station_Cd ? I will findout more about this 

                              EXEC dbo.Site_Upd_Weather_Station_Cd
                                    @Site_Id = @Portfolio_Client_Site_Id
                                   ,@Weather_Station_Cd = @Weather_Station_Cd

                              EXEC dbo.Add_Site_To_Roles
                                    @Site_Id = @Portfolio_Client_Site_Id

                        END 
                  ELSE
                        BEGIN


                              UPDATE
                                    a
                              SET
                                    a.ADDRESS_TYPE_ID = a2.ADDRESS_TYPE_ID
                                   ,a.STATE_ID = a2.STATE_ID
                                   ,a.ADDRESS_LINE1 = a2.ADDRESS_LINE1
                                   ,a.ADDRESS_LINE2 = a2.ADDRESS_LINE2
                                   ,a.CITY = a2.CITY
                                   ,a.ZIPCODE = a2.ZIPCODE
                                   ,a.PHONE_NUMBER = a2.PHONE_NUMBER
                                   ,a.IS_PRIMARY_ADDRESS = a2.IS_PRIMARY_ADDRESS
                                   ,a.ADDRESS_PARENT_ID = s.site_Id
                                   ,a.ADDRESS_PARENT_TYPE_ID = a2.ADDRESS_PARENT_TYPE_ID
                                   ,a.IS_HISTORY = a2.IS_HISTORY
                                   ,a.GEO_LAT = a2.GEO_LAT
                                   ,a.GEO_LONG = a2.GEO_LONG
                                   ,a.SQUARE_FOOTAGE = a2.SQUARE_FOOTAGE
                                   ,a.Is_System_Generated_Geocode = a2.Is_System_Generated_Geocode
                              FROM
                                    site s
                                    INNER JOIN dbo.ADDRESS a
                                          ON A.ADDRESS_ID = S.PRIMARY_ADDRESS_ID
                                    INNER JOIN core.Client_Hier ch
                                          ON ch.Client_Hier_Id = @Portfolio_Client_Hier_Reference_Number
                                    INNER JOIN site s2
                                          ON s2.site_Id = ch.Site_Id
                                    INNER JOIN dbo.ADDRESS a2
                                          ON a2.ADDRESS_ID = s2.PRIMARY_ADDRESS_ID
                              WHERE
                                    s.site_id = @Portfolio_Client_Site_Id



                              SET @Is_Analyst_Mapping_Cd_Changed = 0

                              SELECT
                                    @Is_Analyst_Mapping_Cd_Changed = 1
                              FROM
                                    dbo.SITE s
                                    INNER JOIN dbo.SITE s1
                                          ON s1.Site_Id = @Portfolio_Client_Site_Id
                                             AND s.Site_Id = @Site_Id
                                             AND s.Analyst_Mapping_Cd <> s1.Analyst_Mapping_Cd


            

                              EXEC dbo.GET_UPDATE_SITE_INFO_P
                                    @site_type_id = @Site_Type_Id
                                   ,@site_name = @Site_Name
                                   ,@division_id = @Division_Id
                                   ,@ubmsite_id = @ubmsite_id
                                   ,@site_reference_number = @site_reference_number
                                   ,@site_product_service = @site_product_service
                                   ,@production_schedule = @production_schedule
                                   ,@shutdown_schedule = @shutdown_schedule
                                   ,@is_alternate_power = @is_alternate_power
                                   ,@is_alternate_gas = @is_alternate_gas
                                   ,@doing_business_as = @doing_business_as
                                   ,@naics_code = @naics_code
                                   ,@tax_number = @tax_number
                                   ,@duns_number = @duns_number
                                   ,@closed = @closed
                                   ,@closed_date = @closed_date
                                   ,@not_managed = @not_managed
                                   ,@closed_by_id = @closed_by_id
                                   ,@user_info_id = @user_info_id
                                   ,@contracting_entity = @contracting_entity
                                   ,@client_legal_structure = @client_legal_structure
                                   ,@firstName = @firstName
                                   ,@lastName = @lastName
                                   ,@contactEmailId = @contactEmailId
                                   ,@emailCC = @emailCC
                                   ,@emailApproval = @emailApproval
                                   ,@dbViewApproval = @dbViewApproval
                                   ,@site_id = @Portfolio_Client_Site_Id
                                   ,@Analyst_Mapping_Cd = @Analyst_Mapping_Cd
                                   ,@Is_Analyst_Mapping_Cd_Changed = @Is_Analyst_Mapping_Cd_Changed


                              EXEC dbo.ADD_ENTITY_AUDIT_ITEM_P
                                    @entity_identifier = @Portfolio_Client_Site_Id
                                   ,@user_info_id = @User_Info_Id
                                   ,@audit_type = 2
                                   ,@entity_name = 'SITE_TABLE'
                                   ,@entity_type = 500

                              EXEC dbo.Site_Upd_Weather_Station_Cd
                                    @Site_Id = @Portfolio_Client_Site_Id
                                   ,@Weather_Station_Cd = @Weather_Station_Cd

                 
                        END      

                 
			   
            END

      END CONVERSATION @Conversation_Handle;

END
;
GO

GRANT EXECUTE ON  [dbo].[Site_Ins_Upd_For_Portfolio_Client] TO [CBMSApplication]
GO
