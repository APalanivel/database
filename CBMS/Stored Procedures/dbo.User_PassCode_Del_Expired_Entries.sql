SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

 NAME: DV2.dbo.User_PassCode_Del_Expired_Entries

 DESCRIPTION:

	Used to delete the expired user pass code entries from User_Passcode table.

	This procedure will pull the reuse limit set in the client(Dv users) table or Appconfig(SV users)
	and verify with the number of entries in User_PassCode table for the given user and if the limit exceeds the config values then it will remove it.

 INPUT PARAMETERS:
 Name				DataType		Default Description
------------------------------------------------------------
 @User_Info_Id		INT
 
 OUTPUT PARAMETERS:
 Name   DataType  Default Description
------------------------------------------------------------
 
 USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN		
		EXEC dbo.User_PassCode_Del_Expired_Entries 49
	ROLLBACK TRAN
 
 AUTHOR INITIALS:
 Initials	Name 
------------------------------------------------------------
 HG			Harihara Suthan G
 PNR		Pandarinath
 
 MODIFICATIONS   
 Initials	Date			Modification
------------------------------------------------------------  
 HG			11/1/2010		Created for the requirement of DV/SV password enhancement project.
 PNR		12/16/2010		@User_PassCode_Reuse_Limit variable taken into isnull function to control null value.
 HG			3/1/2011		SET Statement used for default value of @User_PassCode_Reuse_Limit removed.
							Changed the logic of getting @User_PassCode_Reuse_Limit for demo users.		
							Added Is_Active, Start_Dt and End_Dt filters on App_Config table.
 HG			4/1/2011		Using Client_Hier table to get the user_passcode_reuse_limit as client table goaway from DV
******/

CREATE PROCEDURE dbo.User_PassCode_Del_Expired_Entries
(
	 @User_Info_Id		INT
)
AS
BEGIN

    SET NOCOUNT ON ;

	DECLARE @User_PassCode_Reuse_Limit	SMALLINT;

	SELECT
		@User_PassCode_Reuse_Limit = 
									CASE
										WHEN ui.ACCESS_LEVEL = 1 AND ui.Is_Demo_User = 0 THEN ch.User_PassCode_Reuse_Limit
										WHEN ui.ACCESS_LEVEL IS NULL THEN ac.App_Config_Value
										ELSE 1											-- CBMS Users / DV demo users
									END
	FROM
		dbo.User_Info ui
		INNER JOIN dbo.App_Config ac
			ON ac.App_Config_Cd = 'SV_User_PassCode_Reuse_Limit'
		LEFT OUTER JOIN core.Client_Hier ch
			ON ch.Client_Id = ui.Client_Id
	WHERE
		ui.User_Info_Id = @User_Info_Id
		AND ac.Is_Active = 1
		AND ac.Start_Dt= '1900-01-01 00:00:00.000'
		AND ac.End_Dt = '2099-12-31 00:00:00.000'
	GROUP BY
		ui.Access_Level
		,ui.Is_Demo_User
		,ch.User_PassCode_Reuse_Limit
		,ac.App_Config_Value;		

	WITH Cte_Top_N_User_PassCode AS
	(

		SELECT TOP (@User_PassCode_Reuse_Limit)
			User_Info_Id
			,User_PassCode_Id
		FROM
			dbo.User_PassCode up
		WHERE
			User_Info_Id = @User_Info_Id
		ORDER BY
			up.Last_Change_Ts DESC
	)
	DELETE
		up
	FROM
		dbo.User_PassCode up
		LEFT OUTER JOIN Cte_Top_N_User_PassCode TopN
			ON TopN.User_Info_Id = up.User_Info_Id
				AND TopN.User_PassCode_Id = up.User_PassCode_Id
	WHERE
		up.User_Info_Id = @User_Info_Id
		AND TopN.User_PassCode_Id IS NULL

END
GO
GRANT EXECUTE ON  [dbo].[User_PassCode_Del_Expired_Entries] TO [CBMSApplication]
GO
