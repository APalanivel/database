SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME: dbo.Get_Division_By_Site

DESCRIPTION: 
	To get the division details of given site's client hier id

INPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------  

	EXEC dbo.Get_Division_By_Site

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
HG			Harihara Suthan G

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
HG			2012-03-22	Created

*/

CREATE PROCEDURE dbo.Get_Division_By_Site
      @Site_Client_Hier_Id INT
AS 
BEGIN

      SET NOCOUNT ON ;

      SELECT
            ch.Sitegroup_Id
           ,ch.Sitegroup_Name
           ,ch.Client_Id
           ,ch.Client_Name
      FROM
            core.Client_Hier ch
      WHERE
            ch.Client_Hier_Id = @Site_Client_Hier_Id

END
;
GO
GRANT EXECUTE ON  [dbo].[Get_Division_By_Site] TO [CBMSApplication]
GO
