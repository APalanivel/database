SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.RC_GET_REVIEWER_NAMES_LIST_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(10)	          	
	@reviewerGroup 	varchar(50)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE dbo.RC_GET_REVIEWER_NAMES_LIST_P
@userId varchar(10),
@sessionId varchar(10),
@reviewerGroup varchar(50)
as
set nocount on
	SELECT      USER_INFO.USER_INFO_ID ,USER_INFO.FIRST_NAME+' '+USER_INFO.LAST_NAME USER_INFO_NAME 
	FROM         USER_INFO INNER JOIN
	                      USER_INFO_GROUP_INFO_MAP ON USER_INFO.USER_INFO_ID = USER_INFO_GROUP_INFO_MAP.USER_INFO_ID INNER JOIN
	                      GROUP_INFO ON USER_INFO_GROUP_INFO_MAP.GROUP_INFO_ID = GROUP_INFO.GROUP_INFO_ID
	WHERE     (GROUP_INFO.GROUP_NAME =@reviewerGroup)
	ORDER BY USER_INFO.FIRST_NAME+' '+USER_INFO.LAST_NAME
GO
GRANT EXECUTE ON  [dbo].[RC_GET_REVIEWER_NAMES_LIST_P] TO [CBMSApplication]
GO
