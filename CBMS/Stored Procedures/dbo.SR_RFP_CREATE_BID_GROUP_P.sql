SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_CREATE_BID_GROUP_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@groupName varchar(200),
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    
@sr_rfp_bid_group_id int out


USAGE EXAMPLES:
------------------------------------------------------------
---- exec dbo.SR_RFP_CREATE_BID_GROUP_P 'Maaza',0 

---- Test ID at SV for the insert done
--SELECT * FROM SR_RFP_BID_GROUP WHERE SR_RFP_BID_GROUP_ID = (SELECT MAX(SR_RFP_BID_GROUP_ID) FROM SR_RFP_BID_GROUP)

---- remove the entry
--DELETE FROM SR_RFP_BID_GROUP WHERE SR_RFP_BID_GROUP_ID = (SELECT MAX(SR_RFP_BID_GROUP_ID) FROM SR_RFP_BID_GROUP)AND GROUP_NAME ='Maaza'



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_CREATE_BID_GROUP_P

@groupName varchar(200),
@sr_rfp_bid_group_id int out

	AS
	
set nocount on


	INSERT INTO SR_RFP_BID_GROUP (GROUP_NAME) 
	VALUES(@groupName)
	
	Select @sr_rfp_bid_group_id = scope_identity()

RETURN
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_CREATE_BID_GROUP_P] TO [CBMSApplication]
GO
