SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_CREATE_RFP_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@commodityTypeId int,
@analystId int,
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    
@sr_rfp_id int out


USAGE EXAMPLES:
------------------------------------------------------------
--exec dbo.SR_RFP_CREATE_RFP_P 290, 49, null

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_CREATE_RFP_P

@commodityTypeId int,
@analystId int,
@sr_rfp_id int out


as
	
set nocount on
	
	DECLARE @entityId int
	SELECT @entityId = (select entity_id from entity where entity_type=1004 and entity_name='new')
	
	INSERT INTO SR_RFP(RFP_STATUS_TYPE_ID,COMMODITY_TYPE_ID,INITIATED_BY)
	VALUES(@entityId,@commodityTypeId,@analystId)

   Select @sr_rfp_id=scope_identity()

	RETURN
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_CREATE_RFP_P] TO [CBMSApplication]
GO
