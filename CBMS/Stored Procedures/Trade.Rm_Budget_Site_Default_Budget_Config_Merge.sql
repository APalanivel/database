SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.Rm_Budget_Site_Default_Budget_Config_Merge                        
                          
Description:                          
        To get market price and forecast pirce if a selected index   
                          
Input Parameters:                          
    Name    DataType        Default     Description                            
--------------------------------------------------------------------------------    
	@Index_Id   INT    
    @Start_Dt	Date    
	@End_Dt		Date
                          
 Output Parameters:                                
	Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
Usage Examples:                              
--------------------------------------------------------------------------------    
	SELECT * FROM dbo.ENTITY e WHERE e.ENTITY_TYPE=272

	EXEC Trade.Rm_Budget_Site_Default_Budget_Config_Merge 584,1005,291,NULL,'2020-04-01','2020-05-01','All Sites'

    
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR			2019-11-29	RM-Budgets Enahancement - Created
	                
******/
CREATE PROCEDURE [Trade].[Rm_Budget_Site_Default_Budget_Config_Merge]
    (
        @Client_Hier_Id INT
        , @Rm_Budget_Id INT
        , @Start_Dt DATE
        , @End_Dt DATE
        , @User_Info_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        UPDATE
            rbsdbc
        SET
            rbsdbc.Rm_Budget_Id = @Rm_Budget_Id
            , rbsdbc.Last_Updated_By = @User_Info_Id
            , rbsdbc.Last_Change_Ts = GETDATE()
        FROM
            Trade.Rm_Budget_Site_Default_Budget_Config rbsdbc
        WHERE
            rbsdbc.Client_Hier_Id = @Client_Hier_Id
            AND rbsdbc.Start_Dt >= @Start_Dt
            AND rbsdbc.End_Dt >= @Start_Dt
            AND rbsdbc.Start_Dt <= @End_Dt
            AND rbsdbc.End_Dt <= @End_Dt;

        INSERT INTO Trade.Rm_Budget_Site_Default_Budget_Config
             (
                 Client_Hier_Id
                 , Rm_Budget_Id
                 , Start_Dt
                 , End_Dt
                 , Created_User_Id
                 , Created_Ts
                 , Last_Updated_By
                 , Last_Change_Ts
             )
        SELECT
            @Client_Hier_Id
            , @Rm_Budget_Id
            , @Start_Dt
            , @End_Dt
            , @User_Info_Id
            , GETDATE()
            , @User_Info_Id
            , GETDATE()
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                Trade.Rm_Budget_Site_Default_Budget_Config rbsdbc
                           WHERE
                                rbsdbc.Client_Hier_Id = @Client_Hier_Id
                                AND (   rbsdbc.Start_Dt BETWEEN @Start_Dt
                                                        AND     @End_Dt
                                        OR  rbsdbc.End_Dt BETWEEN @Start_Dt
                                                          AND     @End_Dt
                                        OR  @Start_Dt BETWEEN rbsdbc.Start_Dt
                                                      AND     rbsdbc.End_Dt
                                        OR  @End_Dt BETWEEN rbsdbc.Start_Dt
                                                    AND     rbsdbc.End_Dt));



    END;
GO
GRANT EXECUTE ON  [Trade].[Rm_Budget_Site_Default_Budget_Config_Merge] TO [CBMSApplication]
GO
