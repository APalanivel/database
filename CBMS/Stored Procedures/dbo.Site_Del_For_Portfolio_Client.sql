SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME:  Site_Del_For_Portfolio_Client      

DESCRIPTION:          

          

INPUT PARAMETERS:          

 Name					DataType			Default		Description          
 -------------------------------------------------------------------------------------------------------    
  @Message				XML								XML string of changes to the client heir table          

 ,@Conversation_Handle  UNIQUEIDENTIFER					Conversation Handle that sent the message           

          

OUTPUT PARAMETERS:          

 Name      DataType  Default Description          
 ------------------------------------------------------------------------------------------------------          

          

 USAGE EXAMPLES:          
 ------------------------------------------------------------------------------------------------------    

 This sp should be executed from SQL Server Service Broker   
          

 AUTHOR INITIALS:          

 Initials	Name          
 ------------------------------------------------------------------------------------------------------    
 TP			Anoop

 
MODIFICATIONS          

 Initials		Date			Modification          
------------------------------------------------------------------------------------------------------          
 TP				2014-06-23		Created   

******/       

CREATE PROCEDURE dbo.Site_Del_For_Portfolio_Client
      (
       @Message XML
      ,@Conversation_Handle UNIQUEIDENTIFIER )
AS
BEGIN

      SET NOCOUNT ON

      DECLARE
            @Portfolio_Client_Id INT
           ,@Division_Id INT
           ,@Site_Ch_Id INT
           ,@Site_Id INT
           ,@Opcode CHAR(1)
           ,@User_Info_Id INT
           ,@Client_Id INT
           ,@Client_Portfolio_Client_Id INT  
		   
	--two cases, 
		--Site table direct Delete XMl will have op_code,Site_Id & Client_Id 
		--Client UnMap from portfolio scenario,op_code_Site_Id & Portfolio_Client_Id
      SELECT
            @Opcode = cng.ch.value('Op_Code[1]', 'VARCHAR')
           ,@Site_Ch_Id = cng.ch.value('Client_Hier_Id[1]', 'INT')
           ,@Client_Id = cng.ch.value('Client_Id[1]', 'INT')
           ,@Portfolio_Client_Id = cng.ch.value('Portfolio_Client_Id[1]', 'INT')
      FROM
            @Message.nodes('/Site_Info/Site') cng ( ch ) 


      DECLARE @Portfolio_Client_Ids TABLE
            (
             Portfolio_Client_Id INT )

      INSERT      INTO @Portfolio_Client_Ids
                  ( Portfolio_Client_Id )
                  SELECT
                        us.Segments
                  FROM
                        dbo.App_Config AS ac
                        CROSS APPLY dbo.ufn_split(ac.App_Config_Value, ',') AS us
                  WHERE
                        ac.App_Config_Cd = 'Portfolio_ClientHier_Management'

      SELECT
            @Client_Portfolio_Client_Id = c.Portfolio_Client_Id
      FROM
            dbo.CLIENT AS c
      WHERE
            c.CLIENT_ID = @Client_Id

      SELECT
            @User_Info_Id = ui.USER_INFO_ID
      FROM
            dbo.USER_INFO AS ui
      WHERE
            ui.USERNAME = 'conversion'


      IF EXISTS ( SELECT
                        1
                  FROM
                        @Portfolio_Client_Ids pc
                  WHERE
                        pc.Portfolio_Client_Id = @Portfolio_Client_Id )
            BEGIN

				
			-- delete duplicate site when original site is unmapped in Client Table
                  SELECT
                        @Site_Id = s.SITE_ID
                  FROM
              dbo.SITE AS s
                  WHERE
                        s.Portfolio_Client_Hier_Reference_Number = @Site_Ch_Id

                  IF @Site_Id IS NOT NULL
                        BEGIN
                              EXEC dbo.Delete_Site_From_Roles
                                    @Site_Id
                        		
                              EXEC dbo.Site_History_Del_By_Site_Id
                                    @Site_Id
                                   ,@User_Info_Id 
									  

									  
									  
                        END
            END

                
				
      IF EXISTS ( SELECT
                        1
                  FROM
                        @Portfolio_Client_Ids pc
                  WHERE
                        pc.Portfolio_Client_Id = @Client_Portfolio_Client_Id )
            BEGIN
  		

			-- delete duplicate site when original site is deleted.
                  SELECT
                        @Site_Id = s.SITE_ID
                  FROM
                        dbo.SITE AS s
                  WHERE
                        s.Portfolio_Client_Hier_Reference_Number = @Site_Ch_Id
                 
                  IF @Site_Id IS NOT NULL
                        BEGIN
                              EXEC dbo.Delete_Site_From_Roles
                                    @Site_Id
                        		
                              EXEC dbo.Site_History_Del_By_Site_Id
                                    @Site_Id
                                   ,@User_Info_Id 

                        END 

            END;


      END CONVERSATION @Conversation_Handle
END











;
GO
GRANT EXECUTE ON  [dbo].[Site_Del_For_Portfolio_Client] TO [CBMSApplication]
GO
