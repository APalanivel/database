SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	cbms_prod.dbo.cbmsUbmInvoiceDeterminant_GetProkarmaNotMapped

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	
	EXEC dbo.cbmsUbmInvoiceDeterminant_GetProkarmaNotMapped

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
SSR				Sharad Srivastava
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script
SSR				01/28//2010	Replaced Bucket_type_id with bucket_master_id
SSR				02/24/2010	Removed NOLOCK HINT
 DMR		  09/10/2010 Modified for Quoted_Identifier 
******/
CREATE PROCEDURE dbo.cbmsUbmInvoiceDeterminant_GetProkarmaNotMapped
AS

BEGIN

	SET NOCOUNT ON

	SELECT 
		i.ubm_invoice_id
		, d.ubm_invoice_meter_details_id
		, sm.commodity_type_id 		commodity_type_id
		, sm.ubm_service_type_id 	ubm_service_type_id
		, d.item_code 			ubm_service_code
		, dm.bucket_master_id		bucket_type_id
		, d.item_type_description	ubm_bucket_code
		, um.unit_of_measure_type_id	unit_of_measure_type_id
		, d.usage_unit_of_measure_code	ubm_unit_of_measure_code
		, 'determinant_name' = CASE WHEN d.ubm_meter_number IS NOT NULL THEN 'Meter ' + d.ubm_meter_number ELSE 'Unspecified Meter' END
		, d.usage_quantity 		determinant_value
	FROM
		dbo.ubm_invoice i 
		JOIN dbo.ubm_batch_master_log ml 
			ON ml.ubm_batch_master_log_id = i.ubm_batch_master_log_id
		JOIN dbo.ubm_invoice_meter_details d 
			ON d.ubm_invoice_id = i.ubm_invoice_id
		LEFT OUTER JOIN dbo.ubm_service_map sm 
			ON sm.ubm_id = ml.ubm_id AND sm.ubm_service_code = d.item_code
		LEFT OUTER JOIN dbo.ubm_bucket_determinant_map dm 
			ON dm.ubm_id = ml.ubm_id AND dm.ubm_bucket_code = d.item_type_description AND dm.commodity_type_id = sm.commodity_type_id
		LEFT OUTER JOIN dbo.ubm_unit_of_measure_map um 
			ON um.ubm_id = ml.ubm_id AND um.ubm_unit_of_measure_code = CASE WHEN d.demand_quantity != 0 THEN d.demand_unit_of_measure_code ELSE d.usage_unit_of_measure_code END 
				AND um.commodity_type_id = sm.commodity_type_id
		JOIN dbo.UBM u
			ON u.UBM_ID = ml.UBM_ID
	WHERE
		i.is_processed = 0
		AND u.UBM_NAME = 'Prokarma'
		AND (CASE WHEN d.demand_quantity != 0 THEN d.demand_unit_of_measure_code ELSE d.usage_unit_of_measure_code END) IS NOT NULL
		AND (dm.bucket_master_id IS NULL OR um.unit_of_measure_type_id IS NULL OR sm.ubm_service_type_id IS NULL)

END
GO
GRANT EXECUTE ON  [dbo].[cbmsUbmInvoiceDeterminant_GetProkarmaNotMapped] TO [CBMSApplication]
GO
