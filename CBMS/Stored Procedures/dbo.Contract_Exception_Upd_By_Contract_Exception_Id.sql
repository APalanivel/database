SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
                         
 NAME: dbo.Contract_Exception_Upd_By_Contract_Exception_Id           
                          
 DESCRIPTION:      
		Update the contract exception status.        
                          
 INPUT PARAMETERS:      
                         
 Name                               DataType          Default       Description      
-------------------------------------------------------------------------------------  
@Contract_Exception_Id				INT
@Exception_Status_Cd				INT
@User_Info_Id						INT
		 
		                           
 OUTPUT PARAMETERS:      
                               
 Name                               DataType          Default       Description      
-------------------------------------------------------------------------------------                            
 USAGE EXAMPLES:                              
-------------------------------------------------------------------------------------                 
	EXEC dbo.CODE_SEL_BY_CodeSet_Name
    @CodeSet_Name = 'Exception Status'

EXEC dbo.Missing_Contract_Image_Exception_Exists_Sel_By_Contract_Id
     @Contract_Id = 172958


 AUTHOR INITIALS:    
       
 Initials                   Name      
-------------------------------------------------------------------------------------  
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
-------------------------------------------------------------------------------------  
 NR                     2019-06-13      Created for ADD Contract.                        
                         
******/

CREATE PROCEDURE [dbo].[Contract_Exception_Upd_By_Contract_Exception_Id]
     (
         @Contract_Exception_Id INT
         , @Exception_Status_Cd INT
         , @User_Info_Id INT
     )
AS
    BEGIN

        SET NOCOUNT ON;

        UPDATE
            ce
        SET
            ce.Exception_Status_Cd = @Exception_Status_Cd
            , ce.Updated_User_Id = @User_Info_Id
            , ce.Last_Change_Ts = GETDATE()
        FROM
            dbo.Contract_Exception ce
        WHERE
            ce.Contract_Exception_Id = @Contract_Exception_Id;




    END;

GO
GRANT EXECUTE ON  [dbo].[Contract_Exception_Upd_By_Contract_Exception_Id] TO [CBMSApplication]
GO
