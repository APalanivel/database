
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/****** 
 NAME:  dbo.UBM_GET_BATCHLOG_DETAILS_P
 
 DESCRIPTION: 
 
 INPUT PARAMETERS:  
 Name			Type	Default		Description  
-----------------------------------------------------------------------------------------  


 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
----------------------------------------------------------------------------------------- 

 USAGE EXAMPLES:  
----------------------------------------------------------------------------------------- 

 EXEC [dbo].[UBM_GET_BATCHLOG_DETAILS_P] 49,12,2356,'Urjanet'

 AUTHOR INITIALS:  
 Initials	Name  
-----------------------------------------------------------------------------------------
 AJ         Ajay Chejarla
 LD			Lalith

 MODIFICATIONS   
 Initials	Date			Modification  
 -----------------------------------------------------------------------------------------  	
 AJ			09-18-2012		Updated to have New UBM in IF Condition.
 LD			02-07-2013		Updated to inlcude ?EncoreUK? and ?EncoreGermany? UBM?S
 HG			2014-09-25		Modified for UBM Rename Prokarma to Schneider Electric and addition of new UBM Prokarma									    
******/
  
CREATE PROCEDURE [dbo].[UBM_GET_BATCHLOG_DETAILS_P]
      ( 
       @userId VARCHAR(20)
      ,@sessionId VARCHAR(20)
      ,@masterId INT
      ,@ubmName VARCHAR(200) )
AS 
BEGIN

      SET NOCOUNT ON
	
      IF ( @ubmName = 'Cass'
           OR @ubmName = 'Prokarma'
           OR @ubmName = 'Schneider Electric'
           OR @ubmName = 'AS400'
           OR @ubmName = 'Urjanet'
           OR @ubmName = 'EncoreUK'
           OR @ubmName = 'EncoreGermany'
           OR @ubmName = 'CES' ) 
            BEGIN

                  SELECT
                        ubmlog.CASS_TABLE_NAME NAMES
                       ,details.EXPECTED_DATA_RECORDS
                       ,details.ACTUAL_DATA_RECORDS
                       ,details.EXPECTED_IMAGE_RECORDS
                       ,details.ACTUAL_IMAGE_RECORDS
                       ,details.EXPECTED_DOLLAR_AMOUNT
                       ,details.ACTUAL_DOLLAR_AMOUNT
                       ,'-1' UNKNOWN_CLIENT
                       ,'-1' TOTAL_BYTES
                       ,'-1' ACTUAL_BYTES
                       ,'-1' ZIP_FILE_NAME
                       ,'-1' REASON
                       ,'-1' CONSOLIDATION_ID
                       ,'-1' CLIENT_ID
                  FROM
                        dbo.UBM_BATCH_LOG ubmlog
                        INNER JOIN dbo.UBM_BATCH_LOG_DETAILS details
                              ON details.UBM_batch_LOG_ID = ubmlog.UBM_batch_LOG_ID
                        INNER JOIN dbo.UBM_BATCH_MASTER_LOG masterlog
                              ON masterlog.UBM_BATCH_MASTER_LOG_ID = ubmlog.UBM_BATCH_MASTER_LOG_ID
                        INNER JOIN dbo.UBM ubm
                              ON ubm.UBM_ID = masterlog.UBM_ID
                  WHERE
                        ubmlog.UBM_BATCH_MASTER_LOG_ID = @masterId
                        AND masterlog.UBM_BATCH_MASTER_LOG_ID = @masterId

            END
      ELSE --// For AVISTA  
            BEGIN
	 
                  SELECT
                        ubmlog.CLIENT_NAME NAMES
                       ,details.EXPECTED_DATA_RECORDS
                       ,details.ACTUAL_DATA_RECORDS
                       ,details.EXPECTED_IMAGE_RECORDS
                       ,details.ACTUAL_IMAGE_RECORDS
                       ,details.EXPECTED_DOLLAR_AMOUNT
                       ,details.ACTUAL_DOLLAR_AMOUNT
                       ,ubmlog.CASS_TABLE_NAME UNKNOWN_CLIENT
                       ,details.EXPECTED_IMAGE_BYTES TOTAL_BYTES
                       ,details.ACTUAL_IMAGE_BYTES ACTUAL_BYTES
                       ,ubmlog.ZIP_FILE_NAME ZIP_FILE_NAME
                       ,ubmlog.FAILURE_REASON REASON
                       ,ubmlog.CONSOLIDATION_ID CONSOLIDATION_ID
                       ,ubmlog.CLIENT_ID CLIENT_ID
                  FROM
                        dbo.UBM_BATCH_LOG ubmlog
                        LEFT JOIN dbo.UBM_BATCH_LOG_DETAILS details
                              ON details.UBM_BATCH_LOG_ID = ubmlog.UBM_BATCH_LOG_ID
                        INNER JOIN dbo.UBM_BATCH_MASTER_LOG masterlog
                              ON masterlog.UBM_BATCH_MASTER_LOG_ID = ubmlog.UBM_BATCH_MASTER_LOG_ID
                        INNER JOIN dbo.UBM ubm
                              ON ubm.UBM_ID = masterlog.UBM_ID
                  WHERE
                        masterlog.UBM_BATCH_MASTER_LOG_ID = @masterId
                        AND ubmlog.UBM_BATCH_MASTER_LOG_ID = @masterId

            END

END


;
GO



GRANT EXECUTE ON  [dbo].[UBM_GET_BATCHLOG_DETAILS_P] TO [CBMSApplication]
GO
