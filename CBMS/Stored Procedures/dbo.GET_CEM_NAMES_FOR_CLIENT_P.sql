SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_CEM_NAMES_FOR_CLIENT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@clientId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE  PROCEDURE dbo.GET_CEM_NAMES_FOR_CLIENT_P
@userId varchar(10),
@sessionId varchar(20),
@clientId int

AS
select 	first_name+' '+last_name

from 	client_cem_map map,
	user_info userInfo

where 	map.client_id = @clientId
	and map.user_info_id = userInfo.user_info_id
order by last_name
GO
GRANT EXECUTE ON  [dbo].[GET_CEM_NAMES_FOR_CLIENT_P] TO [CBMSApplication]
GO
