SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE  procedure [dbo].[cbmsAppMenu_Get]
	( @MyAccountId int
	, @menu_id int
	)
AS
BEGIN

	   select m.app_menu_id
		, m.app_menu_profile_id
		, m.permission_info_id
		, m.display_text
		, m.menu_description
		, m.target_server
		, m.target_action
		, m.menu_level
		, m.display_order
		, m.parent_menu_id
		, m.app_module_id
	     from app_menu m
	    where m.app_menu_id = @menu_id
END




















GO
GRANT EXECUTE ON  [dbo].[cbmsAppMenu_Get] TO [CBMSApplication]
GO
