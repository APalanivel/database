SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- exec SR_RFP_DELETE_SWITCH_UTILITY_FLOW_VERIFICATION_P 18282, 0, 1658 
  
CREATE PROCEDURE [dbo].[SR_RFP_DELETE_SWITCH_UTILITY_FLOW_VERIFICATION_P]
	@srRFPAccountId INT,
	@isBidGroup INT,
	@rfpId INT
AS

BEGIN
	
	SET NOCOUNT ON

	--DECLARE @cbmsImageId INT

	--SELECT @cbmsImageId = flow_verification_image_id
	--FROM dbo.Sr_Rfp_Utility_Switch (NOLOCK)
	--WHERE sr_account_group_id = @srRFPAccountId
	--	AND is_bId_group = @isBidGroup

	--DECLARE @changeNotificationImageId int
	--SELECT @changeNotificationImageId =  isNull((select change_notice_image_id from  sr_rfp_utility_switch where sr_account_group_id = @srRFPAccountId and is_bId_group = @isBidGroup ) ,0)

	UPDATE dbo.SR_RFP_UTILITY_SWITCH
	SET FLOW_VERIFICATION_IMAGE_ID = NULL
	WHERE SR_ACCOUNT_GROUP_ID = @srRFPAccountId
		AND is_bId_group = @isBidGroup

	--declare @entityId int  
	--select @entityId = (  
	--SELECT ENTITY_ID FROM ENTITY where entity_name='Flow Verification' and entity_type=100)  

	--DELETE FROM dbo.Cbms_Image
	--WHERE cbms_image_id = @cbmsImageId

	IF (@isBidGroup = 0 )
	 BEGIN
	 
		UPDATE dbo.SR_RFP_CHECKLIST
			SET IS_VERIFY_FLOW = NULL
		WHERE SR_RFP_ACCOUNT_ID =  @srRFPAccountId
	
	 END
	ELSE IF (@isBidGroup > 0 )  
	 BEGIN
	 
		UPDATE rfpCheckList
			SET rfpCheckList.IS_VERIFY_FLOW = NULL
		FROM dbo.SR_RFP_CHECKLIST rfpCheckList
			INNER JOIN dbo.SR_RFP_ACCOUNT rfpAcct ON rfpAcct.SR_RFP_ACCOUNT_ID = rfpCheckList.SR_RFP_ACCOUNT_ID
		WHERE rfpAcct.SR_RFP_BID_GROUP_ID = @srRFPAccountId

	 END

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_DELETE_SWITCH_UTILITY_FLOW_VERIFICATION_P] TO [CBMSApplication]
GO
