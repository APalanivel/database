SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_SYSTEM_CHECKLIST_ACCOUNT_STATUS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE dbo.SR_SAD_GET_SYSTEM_CHECKLIST_ACCOUNT_STATUS_P 

as
set nocount on
select 	ENTITY_ID , ENTITY_NAME

from 	ENTITY

where 	ENTITY_TYPE = 1048 AND ENTITY_DESCRIPTION='SYSTEM CHECKLIST ACCOUNT STATUS'
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_SYSTEM_CHECKLIST_ACCOUNT_STATUS_P] TO [CBMSApplication]
GO
