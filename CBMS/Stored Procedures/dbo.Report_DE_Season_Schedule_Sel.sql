SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
NAME:                
                
  dbo.[Report_DE_Season_Schedule_Sel]            
                 
 DESCRIPTION:  To fetch Season and schedule data  
 INPUT PARAMETERS:                
Name    DataType  Default  Description                
------------------------------------------------------------                
    
    
OUTPUT PARAMETERS:                
Name    DataType  Default  Description                
------------------------------------------------------------      
              
 USAGE EXAMPLES:                
------------------------------------------------------------               
              
 EXEC dbo.[Report_DE_Season_Schedule_Sel]  
               
 AUTHOR INITIALS:                
Initials Name                
-----------------------------------------------------------                
AKR      Ashok Kumar Raju  
            
MODIFICATIONS                 
Initials Date  Modification                
------------------------------------------------------------                
AKR      2014-10-14   Created             
  
******/                
                
CREATE PROCEDURE [dbo].[Report_DE_Season_Schedule_Sel]
AS 
BEGIN                
                
      SET NOCOUNT ON;              
      SELECT
            v.VENDOR_NAME [Vendor]
           ,ts.Schedule_Name [Schedule]
           ,com.Commodity_Name [Commodity]
           ,CONVERT(VARCHAR(2000), comm.Comment_Text) [Comment]
           ,s.SEASON_NAME [Season]
           ,[Start Date] = CONVERT(VARCHAR(10), MONTH(season_from_date)) + '/' + CONVERT(VARCHAR(10), DAY(season_from_date))
           ,[End Date] = CONVERT(VARCHAR(10), MONTH(season_to_date)) + '/' + CONVERT(VARCHAR(10), DAY(season_to_date))
      FROM
            dbo.vendor v
            JOIN dbo.Time_Of_Use_Schedule ts
                  ON ts.VENDOR_ID = v.VENDOR_ID
            LEFT JOIN dbo.Comment comm
                  ON comm.Comment_ID = ts.Comment_ID
            LEFT JOIN dbo.Commodity com
                  ON com.Commodity_Id = ts.Commodity_Id
            LEFT JOIN dbo.Time_Of_Use_Schedule_Term tst
                  ON tst.Time_Of_Use_Schedule_Id = ts.Time_Of_Use_Schedule_Id
            LEFT JOIN dbo.Time_Of_Use_Schedule_Term_Peak tstp
                  ON tstp.Time_Of_Use_Schedule_Term_Id = tst.Time_Of_Use_Schedule_Term_Id
            LEFT JOIN dbo.SEASON s
                  ON s.SEASON_ID = tstp.SEASON_ID
      GROUP BY
            v.VENDOR_NAME
           ,ts.Schedule_Name
           ,com.Commodity_Name
           ,comm.Comment_Text
           ,s.SEASON_NAME
           ,season_from_date
           ,season_to_date
      ORDER BY
            v.VENDOR_NAME
           ,season

               
END;    


;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Season_Schedule_Sel] TO [CBMS_SSRS_Reports]
GO
