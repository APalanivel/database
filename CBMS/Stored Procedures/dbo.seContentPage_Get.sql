SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure dbo.seContentPage_Get
	(
		@ContentPageId int
	)
As
BEGIN
	set nocount on
	 select ContentPageId
				, PageLabel
		 from seContentPage
		where ContentPageId = @ContentPageId

END
GO
GRANT EXECUTE ON  [dbo].[seContentPage_Get] TO [CBMSApplication]
GO
