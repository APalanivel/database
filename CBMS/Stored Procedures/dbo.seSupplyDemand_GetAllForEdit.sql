SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   Procedure dbo.seSupplyDemand_GetAllForEdit
	( @DataTypeId int = 1 
	, @ReportYear char(4) = null
	, @ReportMonth char(2) = null
	)
As
BEGIN
	set nocount on
		 select SupplyDemandId
			, DataTypeId
			, ReportYear
			, ReportMonth
			, MonthName = case DataTypeId When '1' then datename(month, ReportMonth + '/1/2006') else null End
			, UnAccountedFor
			, Consumption
			, NetImports
			, StorageWithdrawal
			, DryProduction
			, SupplementalProduction
			, PlantFuel
			, Vehicles
			, Industrial
			, Commercial
			, Residential
			, Transportation
			, PowerGeneration 
	     from seSupplyDemand
			where DataTypeId = @DataTypeId
				and ReportYear = isNull(@ReportYear, ReportYear)
				and ReportMonth = isNull(@ReportMonth, ReportMonth)
	 order by ReportYear desc
					, ReportMonth desc
END
GO
GRANT EXECUTE ON  [dbo].[seSupplyDemand_GetAllForEdit] TO [CBMSApplication]
GO
