SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:      
  dbo.Contract_Dtls_Sel_By_Account_Cu_Invoice_Commodity_For_Single_Contract

DESCRIPTION:      
  to get the contract_dtls for the given utility and supplier account    
      
INPUT PARAMETERS:      
 Name					DataType				Default				Description      
-------------------------------------------------------------------------------------    
 @Account_Id			INT    
 @Cu_Invoice_Id			INT    
 @commodity_Id			INT    
    
OUTPUT PARAMETERS:      
 Name					DataType				Default				Description      
-------------------------------------------------------------------------------------    
      
USAGE EXAMPLES:      
-------------------------------------------------------------------------------------      
exec dbo.Contract_Dtls_Sel_By_Account_Id_Cu_Invoice_Id_Commodity_Id_For_Single_Contract 206098,18604132,290    
    
exec dbo.Contract_Dtls_Sel_By_Account_Cu_Invoice_Commodity_For_Single_Contract 194774,16517207,290     
    
exec dbo.Contract_Dtls_Sel_By_Account_Cu_Invoice_Commodity_For_Single_Contract 67861,13505316,291    
    
exec dbo.Contract_Dtls_Sel_By_Account_Cu_Invoice_Commodity_For_Single_Contract 258880,25773598,290    
  
exec dbo.Contract_Dtls_Sel_By_Account_Cu_Invoice_Commodity_For_Single_Contract 55262,32227387,290  
  
exec dbo.Contract_Dtls_Sel_By_Account_Cu_Invoice_Commodity_For_Single_Contract 76404,32412669,291  
  
  exec dbo.Contract_Dtls_Sel_By_Account_Cu_Invoice_Commodity_For_Single_Contract
      @Account_Id = 1271032
     ,@Cu_Invoice_Id = 54395711
     ,@commodity_Id = 290
      
      
AUTHOR INITIALS:      
 Initials		Name      
-------------------------------------------------------------------------------------      
 RKV			Ravi Kumar Vegesna      
       
MODIFICATIONS      
      
 Initials	Date		Modification      
-------------------------------------------------------------------------------------      
 RKV		2019-06-28	SE2017-724	- Created. 

 ******/

CREATE PROCEDURE [dbo].[Contract_Dtls_Sel_By_Account_Cu_Invoice_Commodity_For_Single_Contract]
     (
         @Account_Id INT
         , @Cu_Invoice_Id INT
         , @commodity_Id INT = NULL
     )
AS
    BEGIN
        SET NOCOUNT ON;


        CREATE TABLE #Contract_Dtls
             (
                 CONTRACT_ID INT
                 , ED_CONTRACT_NUMBER VARCHAR(150)
                 , CONTRACT_START_DATE DATETIME
                 , CONTRACT_END_DATE DATETIME
                 , Meter_State_Id INT
                 , Invoice_Begin_Dt DATE
                 , Invoice_End_Dt DATE
                 , Contract_Recalc_Begin_Dt DATE
                 , Contract_Recalc_End_Dt DATE
                 , Updated_User VARCHAR(80)
                 , Last_Change_Ts DATETIME
             );


        DECLARE @Contract_Id TABLE
              (
                  Contract_Id INT
                  , Account_Id INT
              );


        INSERT INTO @Contract_Id
             (
                 Contract_Id
                 , Account_Id
             )
        SELECT
            scha.Supplier_Contract_ID
            , scha.Account_Id
        FROM
            Core.Client_Hier_Account ucha
            INNER JOIN Core.Client_Hier_Account scha
                ON ucha.Meter_Id = scha.Meter_Id
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.Account_ID = ucha.Account_Id
        WHERE
            ucha.Account_Type = 'Utility'
            AND scha.Account_Type = 'Supplier'
            AND scha.Supplier_Contract_ID <> -1
            AND ucha.Account_Id = @Account_Id
            AND cism.CU_INVOICE_ID = @Cu_Invoice_Id
            AND (   (cism.Begin_Dt BETWEEN scha.Supplier_Meter_Association_Date
                                   AND     scha.Supplier_Meter_Disassociation_Date)
                    OR  (cism.End_Dt BETWEEN scha.Supplier_Meter_Association_Date
                                     AND     scha.Supplier_Meter_Disassociation_Date))
            AND (   @commodity_Id IS NULL
                    OR  ucha.Commodity_Id = @commodity_Id)
        GROUP BY
            scha.Supplier_Contract_ID
            , scha.Account_Id;

        INSERT INTO @Contract_Id
             (
                 Contract_Id
                 , Account_Id
             )
        SELECT
            scha.Supplier_Contract_ID
            , scha.Account_Id
        FROM
            Core.Client_Hier_Account scha
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.Account_ID = scha.Account_Id
        WHERE
            scha.Account_Type = 'Supplier'
            AND scha.Account_Id = @Account_Id
            AND cism.CU_INVOICE_ID = @Cu_Invoice_Id
            AND (   (cism.Begin_Dt BETWEEN scha.Supplier_Meter_Association_Date
                                   AND     scha.Supplier_Meter_Disassociation_Date)
                    OR  (cism.End_Dt BETWEEN scha.Supplier_Meter_Association_Date
                                     AND     scha.Supplier_Meter_Disassociation_Date))
            AND (   @commodity_Id IS NULL
                    OR  scha.Commodity_Id = @commodity_Id)
            AND scha.Supplier_Contract_ID <> -1
            AND NOT EXISTS (SELECT  1 FROM  @Contract_Id c WHERE c.Contract_Id = scha.Supplier_Contract_ID)
        GROUP BY
            scha.Supplier_Contract_ID
            , scha.Account_Id;




        INSERT INTO #Contract_Dtls
             (
                 CONTRACT_ID
                 , ED_CONTRACT_NUMBER
                 , CONTRACT_START_DATE
                 , CONTRACT_END_DATE
                 , Meter_State_Id
                 , Invoice_Begin_Dt
                 , Invoice_End_Dt
                 , Contract_Recalc_Begin_Dt
                 , Contract_Recalc_End_Dt
                 , Updated_User
                 , Last_Change_Ts
             )
        SELECT
            c.CONTRACT_ID
            , c.ED_CONTRACT_NUMBER
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE
            , cha.Meter_State_Id
            , cism.Begin_Dt AS Invoice_Begin_Dt
            , cism.End_Dt AS Invoice_End_Dt
            , ciacrc.Contract_Recalc_Begin_Dt
            , ciacrc.Contract_Recalc_End_Dt
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Updated_User
            , ciacrc.Last_Change_Ts
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN Core.Client_Hier_Account chat
                ON chat.Meter_Id = cha.Meter_Id
            INNER JOIN dbo.CONTRACT c
                ON chat.Supplier_Contract_ID = c.CONTRACT_ID
            INNER JOIN @Contract_Id e
                ON e.Contract_Id = c.CONTRACT_ID
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.Account_ID = cha.Account_Id
            LEFT OUTER JOIN(dbo.Cu_Invoice_Account_Commodity ciac
                            INNER JOIN dbo.Cu_Invoice_Account_Commodity_Recalc_Contract ciacrc
                                ON ciac.Cu_Invoice_Account_Commodity_Id = ciacrc.Cu_Invoice_Account_Commodity_Id)
                ON cism.CU_INVOICE_ID = ciac.Cu_Invoice_Id
                   AND  cism.Account_ID = ciac.Account_Id
                   AND  ciac.Commodity_Id = cha.Commodity_Id
                   AND  c.CONTRACT_ID = ciacrc.Contract_Id
            LEFT JOIN dbo.USER_INFO ui
                ON ui.USER_INFO_ID = ciacrc.Updated_User_Id
        WHERE
            (   cha.Account_Id = @Account_Id
                OR  chat.Account_Id = @Account_Id)
            AND (   (cism.Begin_Dt BETWEEN chat.Supplier_Meter_Association_Date
                                   AND     chat.Supplier_Meter_Disassociation_Date)
                    OR  (cism.End_Dt BETWEEN chat.Supplier_Meter_Association_Date
                                     AND     chat.Supplier_Meter_Disassociation_Date))
            AND cism.CU_INVOICE_ID = @Cu_Invoice_Id
            AND (   @commodity_Id IS NULL
                    OR  cha.Commodity_Id = @commodity_Id)
        GROUP BY
            c.CONTRACT_ID
            , c.ED_CONTRACT_NUMBER
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE
            , cha.Meter_State_Id
            , cism.Begin_Dt
            , cism.End_Dt
            , ciacrc.Contract_Recalc_Begin_Dt
            , ciacrc.Contract_Recalc_End_Dt
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME
            , ciacrc.Last_Change_Ts;


        SELECT
            CONTRACT_ID
            , ED_CONTRACT_NUMBER
            , CONTRACT_START_DATE
            , CONTRACT_END_DATE
            , Meter_State_Id
            , Invoice_Begin_Dt
            , Invoice_End_Dt
            , Contract_Recalc_Begin_Dt
            , Contract_Recalc_End_Dt
            , Updated_User
            , Last_Change_Ts
        FROM
            #Contract_Dtls;
        DROP TABLE #Contract_Dtls;



    END;

GO
GRANT EXECUTE ON  [dbo].[Contract_Dtls_Sel_By_Account_Cu_Invoice_Commodity_For_Single_Contract] TO [CBMSApplication]
GO
