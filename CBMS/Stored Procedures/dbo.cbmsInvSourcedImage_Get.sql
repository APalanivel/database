SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE          PROCEDURE [dbo].[cbmsInvSourcedImage_Get]
	( @MyAccountId int 
	, @inv_sourced_image_id int
	)
AS
BEGIN

	   select i.inv_sourced_image_id
		, i.inv_sourced_image_batch_id
		, i.inv_source_id
		, l.inv_source_label
		, i.original_filename
		, i.cbms_filename
		, i.archive_path
		, i.keyword
	     from inv_sourced_image i with (nolock)
	     join inv_source l on l.inv_source_id = i.inv_source_id
	    where i.inv_sourced_image_id = @inv_sourced_image_id


END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvSourcedImage_Get] TO [CBMSApplication]
GO
