SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
NAME:              
 [DBO].[DEFAULT_SUPPLIER_ACCOUNT_DETAILS_SEL]      
         
DESCRIPTION: 
             
 WHEN ADDING A NEW SUPPLIER ACCOUNT IT HAS TO POPULATE SUPPLIER ACCOUNT INFORMATION 
 FOR WHICH SUPPLIER_ACCOUNT_IS_DEFAULT_SETTING = 1    
          
INPUT PARAMETERS:              
NAME					 DATATYPE				DEFAULT			DESCRIPTION              
-----------------------------------------------------------------------------------              
@CONTRACT_ID			 INT    
    
                    
OUTPUT PARAMETERS:              
NAME					 DATATYPE				DEFAULT			DESCRIPTION              
-----------------------------------------------------------------------------------     
         
USAGE EXAMPLES:              
-----------------------------------------------------------------------------------              
      
 EXEC DEFAULT_SUPPLIER_ACCOUNT_DETAILS_SEL  69429
 EXEC DEFAULT_SUPPLIER_ACCOUNT_DETAILS_SEL  56709
         
AUTHOR INITIALS:              
INITIALS		NAME              
-----------------------------------------------------------------------------------              
MGB				BHASKARAN GOPALAKRISHNAN      
HG				Hari    
NK				Nageswara Rao Kosuri  
SP				Sandeep Pigilam    
NR				Narayana Reddy        

MODIFICATIONS               
INITIALS			DATE			MODIFICATION              
-----------------------------------------------------------------------------------              
MGB					21-AUG-09		CREATED  
MGB					03-Nov-09		included RA_WATCH_LIST while selecting records  
HG					11/05/2009		Contract.Contract_Recalc_Type_id and Account.Supplier_Account_Recalc_Type_id column relation moved to Code/Codeset from Entity  
									Account.Supplier_Account_Recalc_Type_id renamed to Supplier_Account_Recalc_Type_Cd  
NK					11/17/2009		Added watchlist group  
NK					12/07/2009		Removed Unused Joins and commented Code  
MGB					06/11/2010		Added ACCOUNT_VARIANCE_CONSUMPTION_LEVEL join to get Variance_Consumption_Level_Id
SP					2014-08-05		Data Operations Enhancement Phase III,Added Is_Invoice_Posting_Blocked in Select list.	
NR					2019-04-03		Data2.0 - Removed WATCHLIST-GROUP-INFO-ID  column from Account Table.
RKV					2019-12-30		D20-1762  Removed UBM Columns
	
*/

CREATE PROCEDURE [dbo].[DEFAULT_SUPPLIER_ACCOUNT_DETAILS_SEL]
     (
         @CONTRACTID INT
     )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            CON.CONTRACT_ID
            , ACC.ACCOUNT_ID
            , CONVERT(VARCHAR(10), CON.CONTRACT_START_DATE, 101) CONTRACT_START_DATE
            , CONVERT(VARCHAR(10), CON.CONTRACT_END_DATE, 101) CONTRACT_END_DATE
            , NOT_EXPECTED
            , NOT_MANAGED
            , ACC.SERVICE_LEVEL_TYPE_ID
            , ACC.INVOICE_SOURCE_TYPE_ID
            , CONVERT(VARCHAR(10), ACC.NOT_EXPECTED_DATE, 101) not_expected_date
            , ACC.Supplier_Account_Recalc_Type_Cd
            , ACC.Is_Data_Entry_Only
            , vcl.Variance_Consumption_Level_Id
            , ACC.Is_Invoice_Posting_Blocked
        FROM
            dbo.CONTRACT CON
            JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP SAM
                ON CON.CONTRACT_ID = SAM.Contract_ID
            JOIN dbo.ACCOUNT ACC
                ON ACC.ACCOUNT_ID = SAM.ACCOUNT_ID
            JOIN dbo.Account_Variance_Consumption_Level vcl
                ON SAM.ACCOUNT_ID = vcl.ACCOUNT_ID
        WHERE
            ACC.Supplier_Account_Is_Default_Setting = 1
            AND CON.CONTRACT_ID = @CONTRACTID
        GROUP BY
            CON.CONTRACT_ID
            , ACC.ACCOUNT_ID
            , CON.CONTRACT_START_DATE
            , CON.CONTRACT_END_DATE
            , NOT_EXPECTED
            , NOT_MANAGED
            , ACC.SERVICE_LEVEL_TYPE_ID
            , ACC.INVOICE_SOURCE_TYPE_ID
            , ACC.NOT_EXPECTED_DATE
            , ACC.Supplier_Account_Recalc_Type_Cd
            , ACC.Is_Data_Entry_Only
            , vcl.Variance_Consumption_Level_Id
            , ACC.Is_Invoice_Posting_Blocked;
    END;

GO

GRANT EXECUTE ON  [dbo].[DEFAULT_SUPPLIER_ACCOUNT_DETAILS_SEL] TO [CBMSApplication]
GO
