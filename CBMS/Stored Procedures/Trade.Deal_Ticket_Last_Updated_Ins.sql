SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        Trade.Deal_Ticket_Last_Updated_Ins
                        
Description:                        
       To make a new entry whenever a new deal ticket created or status updated
                        
Input Parameters:                        
    Name				DataType        Default     Description                          
--------------------------------------------------------------------------------  
	@Deal_Ticket_Id  
                        
Output Parameters:                              
	Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
     
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
	BEGIN TRANSACTION
		SELECT * FROM Trade.Deal_Ticket_Last_Updated WHERE Deal_Ticket_Id = 1
		EXEC Trade.Deal_Ticket_Last_Updated_Ins 1
		SELECT * FROM Trade.Deal_Ticket_Last_Updated WHERE Deal_Ticket_Id = 1
		EXEC Trade.Deal_Ticket_Last_Updated_Ins 1
		SELECT * FROM Trade.Deal_Ticket_Last_Updated WHERE Deal_Ticket_Id = 1
	ROLLBACK TRANSACTION

  
Author Initials:            
    Initials    Name                        
--------------------------------------------------------------------------------  
    RR          Raghu Reddy     
                         
 Modifications:                        
    Initials	Date           Modification                        
--------------------------------------------------------------------------------  
	RR          2019-04-09    Created GRM  
                       
******/
CREATE PROCEDURE [Trade].[Deal_Ticket_Last_Updated_Ins] ( @Deal_Ticket_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;

      
      INSERT      INTO Trade.Deal_Ticket_Last_Updated
                  ( Deal_Ticket_Id, Last_Change_Ts )
                  SELECT
                        @Deal_Ticket_Id
                       ,GETDATE()
                       

      

END;
GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Last_Updated_Ins] TO [CBMSApplication]
GO
