SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE proc dbo.GET_PRICE_POINT_VALIDATION_P
@userId varchar(10),
@sessionId varchar(20),
@clientId int,
@divisionId int,
@siteId int,
@priceIndexId int,
@DCStartDate datetime,
@DCEndDate datetime

AS
	set nocount on
	DECLARE	@fiscalStringMonth varchar(200)
	DECLARE	@fiscalIntMonth int
	DECLARE	@fiscalStartDate varchar(200)
	DECLARE	@fiscalEndDate varchar(200)
	DECLARE	@DCStartMonth int
	DECLARE	@DCStartYear varchar(10)
	
	DECLARE	@selectClause varchar(200)
	DECLARE	@fromClause varchar(200)
	DECLARE	@whereClause varchar(200)
	


select @fiscalStringMonth=entity.ENTITY_NAME from CLIENT client,RM_ONBOARD_CLIENT onboard,ENTITY entity where client.CLIENT_ID=onboard.CLIENT_ID AND client.FISCALYEAR_STARTMONTH_TYPE_ID=entity.ENTITY_ID and client.CLIENT_ID=+str(@clientId)
select @DCStartYear=datePart(yyyy,@DCStartDate)--2005
--print '@DCStartYear '+@DCStartYear
select @fiscalStringMonth= '1-'+@fiscalStringMonth+'-'+@DCStartYear
select @fiscalIntMonth=datePart(mm,@fiscalStringMonth)
--print @fiscalMonth
select @DCStartMonth=datePart(mm,@DCStartDate)


--print @hedgeEndDate


if @fiscalIntMonth>@DCStartMonth
begin
	select @fiscalStartDate=DATEADD(year, -1, @fiscalStringMonth)
	select @fiscalEndDate=DATEADD(month, 12, @fiscalStartDate)
	select @fiscalEndDate=DATEADD(day, -1, @fiscalEndDate)
	--print '@fiscalStartDate '+@fiscalStartDate
	--print '@fiscalEndDate '+@fiscalEndDate
	
end
else if @fiscalIntMonth<=@DCStartMonth
begin
	select @fiscalStartDate=@fiscalStringMonth
	select @fiscalEndDate=DATEADD(month, 12, @fiscalStringMonth)
	select @fiscalEndDate=DATEADD(day, -1, @fiscalEndDate)
	--select @fiscalEndDate=@DCStartDate
	--print @fiscalStartDate
	--print '@fiscalEndDate1 '+@fiscalEndDate
	
	
end



if @clientId>0 and @divisionId<0 and @siteId<0 

		if (select count(*) from rm_deal_ticket where 
		(hedge_start_month between @fiscalStartDate and @fiscalEndDate or
		hedge_end_month between @fiscalStartDate and @fiscalEndDate)and
		client_id=@clientId and
		division_id is null and
		site_id is null)>0

begin

--print 'inside client '
	
Select
	distinct price_index_id 
From 
	rm_deal_ticket
Where
	@priceIndexId not in
	(
		select distinct price_index_id from rm_deal_ticket where 
		(hedge_start_month between @fiscalStartDate and @fiscalEndDate or
		hedge_end_month between @fiscalStartDate and @fiscalEndDate)and
		client_id=@clientId and
		division_id is null and
		site_id is null
	)and
	(
		hedge_start_month between @fiscalStartDate and @fiscalEndDate or
		hedge_end_month between @fiscalStartDate and @fiscalEndDate
	)and
		client_id=@clientId and
		division_id is null and
		site_id is null
	

end
else
begin
	--print 'inside client1 '
	select price_index_id from price_index where price_index_id=-1
end

else if @clientId>0 and @divisionId>0 and @siteId<0 

 
		if(select count(*) from rm_deal_ticket where 
		(hedge_start_month between @fiscalStartDate and @fiscalEndDate or
		hedge_end_month between @fiscalStartDate and @fiscalEndDate)and
		client_id=@clientId and 
		division_Id=@divisionId and
		site_id is null)>0
begin
--print 'inside div '

Select
	distinct price_index_id 
From 
	rm_deal_ticket
Where
	@priceIndexId not in
	(
		select distinct price_index_id from rm_deal_ticket where 
		(hedge_start_month between @fiscalStartDate and @fiscalEndDate or
		hedge_end_month between @fiscalStartDate and @fiscalEndDate)and
		client_id=@clientId and 
		division_Id=@divisionId and
		site_id is null
	)and (hedge_start_month between @fiscalStartDate and @fiscalEndDate or
		hedge_end_month between @fiscalStartDate and @fiscalEndDate)and
		client_id=@clientId and 
		division_Id=@divisionId and
		site_id is null
	

end
else
begin
	--print 'inside div1 '
	select price_index_id from price_index where price_index_id=-1
end

else if @clientId>0 and @divisionId<0 and @siteId>0 	
		
		if (select count(*) from rm_deal_ticket where 

		(hedge_start_month between @fiscalStartDate and @fiscalEndDate or
		hedge_end_month between @fiscalStartDate and @fiscalEndDate)and
		client_id=@clientId and 
		division_id is null and
		site_Id=@siteId)>0
begin

--print 'inside site '
Select
	distinct price_index_id 
From 
	rm_deal_ticket
Where
	@priceIndexId not in
	(
		select distinct price_index_id from rm_deal_ticket where 
		(hedge_start_month between @fiscalStartDate and @fiscalEndDate or
		hedge_end_month between @fiscalStartDate and @fiscalEndDate)and
		client_id=@clientId and 
		division_id is null and
		site_Id=@siteId
	) and(hedge_start_month between @fiscalStartDate and @fiscalEndDate or hedge_end_month between @fiscalStartDate and @fiscalEndDate)and
		client_id=@clientId and 
		division_id is null and
		site_Id=@siteId
end

else
begin
	--print 'inside site 1'
	select price_index_id from price_index where price_index_id=-1
end
GO
GRANT EXECUTE ON  [dbo].[GET_PRICE_POINT_VALIDATION_P] TO [CBMSApplication]
GO
