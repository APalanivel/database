SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********   
NAME:  
    dbo.Variance_Log_INS  
 
DESCRIPTION:  
    Used to insert log details into variance_log table

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
	@Summit_Variance_Log  SUMMIT_VARIANCE_LOG 
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:   
------------------------------------------------------------    
	dbo.Variance_Log_INS
	
AUTHOR INITIALS:

Initials Name
------------------------------------------------------------
NK		Nageswara Rao Kosuri
AP		Athmaram Pabbathi
SP		Sandeep Pigilam

Initials Date		Modification  
------------------------------------------------------------  
NK	   11/03/2009	Created
AP	   08/17/2011	Removed COST_USAGE_ID column as part of ADLDT changes.
					Commented code removed
SP	   2014-04-25	Data Operations Enhancements,Added	Baseline_Cd,Baseline_Desc,Baseline_Service_Month in Variance_Log Insert script.	
SP	   2014-07-16	Data Operations Enhancements Phase III,Added Baseline_Account_Id in Variance_Log Insert script.	
AP		Feb 5, 2020	Modofied variance log to capture Advanced variance testing		
******/

CREATE PROCEDURE [dbo].[Variance_Log_INS]
      (
      @Summit_Variance_Log AS             SUMMIT_VARIANCE_LOG             READONLY
    , @TVP_Variance_History_Data_Point AS TVP_Variance_History_Data_Point READONLY )
AS
      BEGIN

            SET NOCOUNT ON;
            SET XACT_ABORT ON;

            DECLARE
                  @VarainceStatusCd   INT
                , @VarianceStatusDesc VARCHAR(50)
                , @cost_usage_id      INT;

            SELECT
                  @VarainceStatusCd = cd.Code_Id
            FROM  dbo.Code cd
                  JOIN
                  dbo.Codeset cset
                        ON cset.Codeset_Id = cd.Codeset_Id
            WHERE Codeset_Name = 'VarianceStatus'
                  AND   cd.Code_Value = 'In Progress';

            SET @VarianceStatusDesc = 'In Progress';

            BEGIN TRAN;


            --For AVT we need to make entry only for currrent service month
            INSERT INTO dbo.Variance_Log (
                                               Client_ID
                                             , Client_Name
                                             , Site_ID
                                             , Site_Name
                                             , State_ID
                                             , State_Name
                                             , Account_ID
                                             , Account_Number
                                             , Service_Month
                                             , Queue_Id
                                             , Queue_Name
                                             , Queue_Dt
                                             , Is_Failure
                                             , UOM_Cd
                                             , UOM_Label
                                             , Execution_Dt
                                             , Current_Value
                                             , Baseline_Value
                                             , Low_Tolerance
                                             , High_Tolerance
                                             , Test_Description
                                             , Category_Name
                                             , Commodity_ID
                                             , Commodity_Desc
                                             , Consumtion_Level_Desc
                                             , Variance_Status_Cd
                                             , Variance_Status_Desc
                                             , City
                                             , Vendor_name
                                             , Vendor_ID
                                             , Is_Inclusive
                                             , Is_Multiple_Condition
                                             , Ref_Variance_Log_Id
                                             , Baseline_Cd
                                             , Baseline_Desc
                                             , Baseline_Service_Month
                                             , Baseline_Account_Id
                                             , AVT_R2_Score
                                         )
                        SELECT
                              SVL.Client_ID
                            , SVL.Client_Name
                            , SVL.Site_ID
                            , SVL.Site_Name
                            , SVL.State_ID
                            , SVL.State_Name
                            , SVL.Account_ID
                            , SVL.Account_Number
                            , SVL.Service_Month
                            , SVL.Queue_Id
                            , SVL.Queue_Name
                            , getdate()
                            , SVL.Is_Failure
                            , SVL.UOM_Cd
                            , SVL.UOM_Label
                            , getdate()
                            , SVL.Current_Value
                            , SVL.Baseline_Value
                            , SVL.Low_Tolerance
                            , SVL.High_Tolerance
                            , SVL.Test_Description
                            , SVL.Category_Name
                            , SVL.Commodity_ID
                            , SVL.Commodity_Desc
                            , SVL.Consumtion_Level_Desc
                            , @VarainceStatusCd
                            , @VarianceStatusDesc
                            , SVL.City
                            , SVL.Vendor_name
                            , SVL.Vendor_ID
                            , SVL.Is_Inclusive
                            , SVL.Is_Multiple_Condition
                            , SVL.Ref_Variance_Log_Id
                            , Baseline_Cd
                            , Baseline_Desc
                            , Baseline_Service_Month
                            , Baseline_Account_Id
                            , SVL.AVT_R2_Score
                        FROM  @Summit_Variance_Log SVL;

            IF EXISTS
                  (     SELECT
                              1
                        FROM  @TVP_Variance_History_Data_Point )
                  BEGIN

                        INSERT INTO dbo.Variance_Historical_Data_Point (
                                                                             Variance_Log_Id
                                                                           , Service_Month
                                                                           , Baseline_Value
                                                                           , High_Tolerance
                                                                           , Low_Tolerance
                                                                           , Created_Ts
                                                                       )
                                    SELECT
                                          (     SELECT      TOP ( 1 )
                                                            VL.Variance_Log_Id
                                                FROM        Variance_Log VL
                                                WHERE       tl.Account_ID = VL.Account_ID
                                                            AND   tl.Commodity_ID = VL.Commodity_ID
                                                            AND   VL.Category_Name = tl.Category_Name
                                                ORDER BY    VL.Variance_Log_Id DESC ) -- this sub query is to make sure we are taking the latest log id
                                        , Service_Month
                                        , Baseline_Value
                                        , High_Tolerance
                                        , Low_Tolerance
                                        , getdate()
                                    FROM  @TVP_Variance_History_Data_Point TL;


                  END;


            COMMIT;

            RETURN;

      END;

GO



GRANT EXECUTE ON  [dbo].[Variance_Log_INS] TO [CBMSApplication]
GO
