SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UPDATE_FORECAST_VOLUMES_P

DESCRIPTION:


INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@userId        		varchar(10)	          	
	@sessionId     		varchar(20)	          	
	@clientId      		int       	          	
	@divisionId    		int       	          	
	@siteId        		int       	          	
	@forecastYear  		int       	          	
	@monthIdentifier	datetime  	          	
	@volume        		decimal(18,0)	          	
	@volumeTypeName		varchar(200)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE [dbo].[UPDATE_FORECAST_VOLUMES_P]
      ( 
       @userId VARCHAR(10)
      ,@sessionId VARCHAR(20)
      ,@clientId INT
      ,@divisionId INT
      ,@siteId INT
      ,@forecastYear INT
      ,@monthIdentifier DATETIME
      ,
--@asOfDate datetime,
       @volume DECIMAL
      ,@volumeTypeName VARCHAR(200)
--@siteUnitId int
       )
AS 
SET NOCOUNT	ON;

DECLARE
      @volumeId INT
     ,@count INT
     ,@cnt INT
     ,@volumeTypeId INT

SELECT
      @volumeTypeId = entity_id
FROM
      entity
WHERE
      entity_type = 285
      AND entity_name = @volumeTypeName


/*select @cnt = count(1) from rm_forecast_volume 
	where  forecast_year=@forecastYear 
		--and forecast_as_of_date=@asOfDate 
		and client_id = @clientId
if(@cnt = 0 )

begin
insert into rm_forecast_volume (client_id, forecast_year, forecast_as_of_date) 
	values(@clientId, @forecastYear, getdate())
end
*/


SELECT
      @forecastYear = ISNULL(@forecastYear, CAST(YEAR(@monthIdentifier) AS INT))

SELECT
      @volumeId = rm_forecast_volume_id
FROM
      rm_forecast_volume
WHERE
      forecast_year = @forecastYear 
		--and forecast_as_of_date=@asOfDate 
      AND client_id = @clientId

SELECT
      @count = COUNT(1)
FROM
      rm_forecast_volume_details details
     ,site sit
WHERE
      details.site_id = sit.site_id
      AND sit.site_id = @siteId
      AND details.month_identifier = @monthIdentifier
      AND details.rm_forecast_volume_id = @volumeId

IF ( @count > 0 ) 
      UPDATE
            rm_forecast_volume_details
      SET   
            volume = @volume
           ,volume_type_id = @volumeTypeId
	--, volume_units_type_id = @siteUnitId
      WHERE
            site_id = @siteId
            AND month_identifier = @monthIdentifier
            AND rm_forecast_volume_id = @volumeId
ELSE 
      INSERT      INTO rm_forecast_volume_details
                  ( 
                   rm_forecast_volume_id
                  ,site_id
                  ,division_id
                  ,month_identifier
                  ,volume
                  ,volume_type_id )--, volume_units_type_id) 
      VALUES
                  ( 
                   @volumeId
                  ,@siteId
                  ,@divisionId
                  ,@monthIdentifier
                  ,@volume
                  ,@volumeTypeId )--, @siteUnitId)

GO
GRANT EXECUTE ON  [dbo].[UPDATE_FORECAST_VOLUMES_P] TO [CBMSApplication]
GO
