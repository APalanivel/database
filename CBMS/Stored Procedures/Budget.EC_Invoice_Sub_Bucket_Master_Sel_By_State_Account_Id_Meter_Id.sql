SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   [Budget].[EC_Invoice_Sub_Bucket_Master_Sel_By_State_Account_Id_Meter_Id]       
              
Description:              
			This sproc to get the bucket details details for a Given id.      
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
	@State_Id							INT
    @Account_Id							INT	
    @Meter_Id							INT	
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

   

   
   Exec [Budget].[EC_Invoice_Sub_Bucket_Master_Sel_By_State_Account_Id_Meter_Id] 124,55291,38245      
	
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna               
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2019-06-05		Created For Calculation Tester.         
    
******/

CREATE PROCEDURE [Budget].[EC_Invoice_Sub_Bucket_Master_Sel_By_State_Account_Id_Meter_Id]
    (
        @Account_Id INT
        , @Meter_Id INT
		,@State_Id INT = NULL 
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Commodity_Id INT;

        SELECT
            @Commodity_Id = cha.Commodity_Id
        FROM
            Core.Client_Hier_Account AS cha
        WHERE
            cha.Account_Id = @Account_Id
            AND cha.Meter_Id = @Meter_Id;

		SELECT
            @State_Id = ch.State_Id
        FROM
            Core.Client_Hier_Account AS cha
            INNER JOIN Core.Client_Hier AS ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
        WHERE
            cha.Account_Id = @Account_Id
            AND cha.Meter_Id = @Meter_Id
            AND @State_Id IS NULL
        GROUP BY
            ch.State_Id;


        SELECT
            eisbm.EC_Invoice_Sub_Bucket_Master_Id
			,eisbm.Sub_Bucket_Name
			,bm.Bucket_Master_Id
        FROM
            dbo.EC_Invoice_Sub_Bucket_Master eisbm
            INNER JOIN dbo.Bucket_Master bm
                ON bm.Bucket_Master_Id = eisbm.Bucket_Master_Id
            INNER JOIN dbo.Code btc
                ON btc.Code_Id = bm.Bucket_Type_Cd
            INNER JOIN dbo.STATE s
                ON s.STATE_ID = eisbm.State_Id
            LEFT OUTER JOIN dbo.Ec_Calc_Val_Bucket_Sub_Bucket_Map ecvbsbm
                ON eisbm.EC_Invoice_Sub_Bucket_Master_Id = ecvbsbm.EC_Invoice_Sub_Bucket_Master_Id
        WHERE
            eisbm.State_Id = @State_Id
            AND bm.Commodity_Id = @Commodity_Id
            AND btc.Code_Value = 'Determinant'
        GROUP BY
            eisbm.EC_Invoice_Sub_Bucket_Master_Id
			,eisbm.Sub_Bucket_Name
			,bm.Bucket_Master_Id;


    END;

    ;
    ;

GO
GRANT EXECUTE ON  [Budget].[EC_Invoice_Sub_Bucket_Master_Sel_By_State_Account_Id_Meter_Id] TO [CBMSApplication]
GO
