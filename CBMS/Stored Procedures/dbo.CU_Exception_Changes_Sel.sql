SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	 dbo.CU_Exception_Changes_Sel

DESCRIPTION:
			This procedure is used to select changed records between two versions from CU_EXCEPTION.

INPUT PARAMETERS:
	Name						DataType		Default	Description
------------------------------------------------------------
@Last_Version_Id				BIGINT			Beginnng Change tracking Version Id (From Persistent Variables) 			
@Current_Version_Id				BIGINT			Current Change tracking Version Id

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
It will execute only through the replication replacement SSIS package.

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RK			Raghu Kalvapudi
	
MODIFICATIONS
	Initials	Date			Modification
------------------------------------------------------------
	RK			08/01/2013		Created
******/

CREATE PROCEDURE dbo.CU_Exception_Changes_Sel
      ( 
       @Last_Version_Id BIGINT
      ,@Current_Version_Id BIGINT )
AS 
BEGIN
      SET NOCOUNT ON;

      SELECT
            convert(CHAR(1), cue_ct.SYS_CHANGE_OPERATION) Op_Code
           ,cue_ct.CU_EXCEPTION_ID
           ,cue.CU_INVOICE_ID
           ,cue.QUEUE_ID
           ,cue.IS_CLOSED
           ,cue.OPENED_DATE
           ,cue.CLOSED_DATE
           ,cue.DATE_IN_QUEUE
           ,cue.ROUTED_REASON_TYPE_ID
      FROM
            CHANGETABLE(CHANGES dbo.CU_EXCEPTION, @Last_Version_Id) cue_ct
            LEFT JOIN dbo.CU_EXCEPTION cue
                  ON cue.CU_EXCEPTION_ID = cue_ct.CU_EXCEPTION_ID
      WHERE
            cue_ct.SYS_Change_Version <= @Current_Version_Id



END

;
GO
GRANT EXECUTE ON  [dbo].[CU_Exception_Changes_Sel] TO [ETL_Execute]
GO
