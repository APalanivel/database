SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
NAME: RM_Group_Search_Contracts              
              
DESCRIPTION:              
This procedure is modified as part of Contract enhancement where we have implemented pagination concept as well as               
Concatenation of few columns.              
Here User  has also ability to chose the Sorting and Order of same.(Utility,City,Site_id can not be used as Sorting Columns)              
               
              
INPUT PARAMETERS:              
 Name    DataType  Default   Description              
-----------------------------------------------------------------              
 @clientId   INT    NULL              
 @clientName  VARCHAR(200) NULL                     
 @stateId   VARCHAR(MAX)  NULL                     
 @contractNumber VARCHAR(150) NULL                     
 @city    VARCHAR(200) NULL                     
 @accountNumber  VARCHAR(50)  NULL                     
 @meterNumber  VARCHAR(50)  NULL                     
 @vendorTypeId  INT    NULL                     
 @supplierName  VARCHAR(200) NULL                     
 @utilityName  VARCHAR(200) NULL                     
 @commodityId  INT    NULL                 
 @Sortindex   varchar(20)  'Asc'   Asc/Desc              
 @SortColumn  Varchar(200) 'contract_id' contract_id/ed_contract_number/client_name/vendor_type              
             /supplier/commodity/contract_start_date/contract_end_date              
 @StartIndex  INT    1              
 @EndIndex   INT    2147483647  
 @AnalystId   INT    NULL                  
              
OUTPUT PARAMETERS:              
 Name   DataType  Default Description              
------------------------------------------------------------              
                   
             
                  
USAGE EXAMPLES:              
------------------------------------------------------------              
              
 EXEC RM_Group_Search_Contracts  @StartIndex = 1, @EndIndex = 200, @ClientId = 11231,@IsFilterByContractStartDate =1 , @IsFilterByContractEndDate = 0 , @FromDate = '1/1/2011' ,@ToDate = '6/1/2014'              
 EXEC RM_Group_Search_Contracts  @StartIndex = 1, @EndIndex = 200, @ClientId = 11231,@IsFilterByContractStartDate =1 , @IsFilterByContractEndDate = 0 , @FromDate = '1/1/2011' ,@ToDate = '6/1/2012',@SortColumn='State_Name',@Sortindex='desc'  
 EXEC RM_Group_Search_Contracts  @StartIndex = 1, @EndIndex = 200, @ClientId = 235,@IsFilterByContractStartDate =1 , @IsFilterByContractEndDate = 0 , @SortColumn='CONTRACT_START_DATE',@Sortindex='desc',@city='Ville',@stateId='10,16'  
 EXEC RM_Group_Search_Contracts  @StartIndex = 1, @EndIndex = 200, @ClientId = 235,@IsFilterByContractStartDate =1 , @IsFilterByContractEndDate = 0 , @SortColumn='CONTRACT_START_DATE',@Sortindex='desc',@stateId='35'  
 EXEC RM_Group_Search_Contracts  @StartIndex = 1, @EndIndex = 200, @ClientId = 235,@IsFilterByContractStartDate =1 , @IsFilterByContractEndDate = 0 , @SortColumn='CONTRACT_START_DATE',@Sortindex='desc',@AnalystId=18900,@stateId='13'  
 EXEC RM_Group_Search_Contracts   
      @StartIndex = 1  
     ,@EndIndex = 200  
     ,@ClientId = 235  
     ,@IsFilterByContractStartDate = 1  
     ,@IsFilterByContractEndDate = 0  
     ,@SortColumn = 'CONTRACT_START_DATE'  
     ,@Sortindex = 'desc'  
     ,@AnalystId = 34773   
  
 EXEC RM_Group_Search_Contracts   
    NULL  
   ,NULL  
   ,NULL  
   ,NULL  
   ,NULL  
   ,NULL  
   ,NULL  
   ,NULL  
   ,NULL  
   ,NULL  
   ,NULL  
   ,NULL  
   ,NULL  
   ,1  
   ,0  
   ,1  
   ,'2014-01-01 00:00:00'  
   ,'2015-12-31 00:00:00'  
   ,1  
   ,200  
   ,NULL  
  
EXEC RM_Group_Search_Contracts  
      @stateId = N'15'  
     ,@utilityName = N'exe'  
     ,@CommodityId = 290  
     ,@IsFilterByContractStartDate = 0  
     ,@IsFilterByContractEndDate = 0  
     ,@StartIndex = 1  
     ,@EndIndex = 200  
     ,@AnalystId = 22442  
       
 EXEC RM_Group_Search_Contracts  @StartIndex = 1, @EndIndex = 200, @ClientId = 235  
 EXEC RM_Group_Search_Contracts  @StartIndex = 1, @EndIndex = 200, @ClientId = 235, @Country_Id = 1  
 EXEC RM_Group_Search_Contracts  @StartIndex = 1, @EndIndex = 200, @ClientId = 235, @Country_Id = 4  
 EXEC RM_Group_Search_Contracts  @StartIndex = 1, @EndIndex = 200, @ClientId = 235, @Alternate_Account_Number = '0909'  
 EXEC RM_Group_Search_Contracts  @StartIndex = 1, @EndIndex = 200, @ClientId = 235, @Contract_Classification_Cd = 1326  
 EXEC RM_Group_Search_Contracts @StartIndex = 1, @EndIndex = 200, @contractNumber=126860  
 EXEC RM_Group_Search_Contracts @StartIndex = 1, @EndIndex = 200, @Utility_Vendor_Id=1574  
 EXEC RM_Group_Search_Contracts @StartIndex = 1, @EndIndex = 200, @Supplier_Vendor_Id=3137  
 EXEC RM_Group_Search_Contracts @contractNumber =  '126485-0001'  
 EXEC RM_Group_Search_Contracts  @ClientId = 10081 ,@IsFilterByContractStartDate = 1, @FromDate = '2019-01-01' , @ToDate = '2020-12-01'    
 EXEC RM_Group_Search_Contracts  @ClientId = 10081 ,@IsFilterByContractEndDate = 1, @FromDate = '2020-01-01' , @ToDate = '2020-12-01'   
 EXEC RM_Group_Search_Contracts  @ClientId = 10081 ,@IsFilterByContractStartDate = 1,@IsFilterByContractEndDate = 1, @FromDate = '2019-01-01' , @ToDate = '2020-12-01'    
  
AUTHOR INITIALS:              
 Initials Name              
------------------------------------------------------------              
 SS   Subhash Subramanyam              
 HG   Hari  
 RR   Raghu Reddy  
 DMR  Deana Ritter        
 NR   Narayana Reddy
 PRV  Pramod Reddy V        
              
MODIFICATIONS:            
 Initials Date  Modification              
------------------------------------------------------------              
   07/20/2009 Autogenerated script      
  SS  09/15/2009 Replaced account_id reference with contract_id to join account and supplier_account_meter_map              
  SS  10/06/2009 Concatenated relevant columns into comma separated values by contract_id              
  HG  11/11/2009 Modified the Query to get the city list of a contract(value derived from the base table).             
  HG  11/16/2009 Is_History filter condition added in the query which used to get city list for the contract.              
  HG  12/09/2009 Order by clause(ed_contract_number) moved to the select from table variable.              
      Division table join eliminated as we have client_id in Site table itself              
      Entity table joined used to find the commodity name substituted by Commodity table.              
  HG  01/05/2010 City Column added in the order by clause.               
  SSR  04/23/2010  USED CTE to generate Row_number for pagination               
      USED Cross Apply to concatenate few columns (Comma seperated) as per Reqd.               
      Converted Static to dynamic as user has ability to choose Sorting on Columns                     
      unused @Client_id parameter removed.                     
  SKA  04/26/2010 Added @Client_id parameter back as it is using by application.              
  SSR  06/01/2010 Added vendor_type in Case statement for Sorting and Changed commdity Column name to Commodity_name              
  SKA  06/09/2010 Added the State_Name column in the select clause.              
  HG  06/12/2010 Added code to place single quote after and before the account_number and meter_number parameters in WHERE clause.              
  SKA  08/19/2010 Added the Site Name column in select clause              
  DMR  09/10/2010 Modified for Quoted_Identifier            
  AKR  07/09/2011 Script modified TO implement the CONTRACT search enhancement functionality requirement  
      -- All the parameters made as non mandatory  
      -- Script modified to return image mapped to contract  
  RR  08/12/2011 Added the State_Name column in the columns collection   
  RR  08/26/2011 Added sorting on State_Name   
  RR  09/19/2011 MAINT-813 Modified city fliter WITH LIKE search  
  RR  21/09/2011 Prepopulated the ORDER By column string and appended it TO the main query to remove the SELECT CASE used in ORDER BY clause  
  RR  2014-05-23 MAINT-2766 Contract search enhancement modified @stateId to accept multiple state ids as comma seperated string and added  
      new input paremeter @AnalystId for sourcing analyst  
  HG  2014-06-11 MAINT-2867, Since ALL the vendors NOT going TO have analysts mapped changed the INNER JOIN ON Vendor_Commodity_Analyst_Map TO OUTER JOIN  
  DMR  12/8/2014 Removed Transaction Isolation Level statements.  Disabled Database level snap shot isolation.  
  HG  2014-12-26 MAINT-3236, CROSS APPLY query USED TO GET the comma seperated utility analysts name is simplified TO gain the performance.  
       - INDEX created ON Supplier_Contract_Id COLUMN in Client_Hier_Account TABLE.  
  DMR  2014-02-24 MAINT-3386 Modified to use Full text indexing. Keeping to dynamic SQL as OR condition in the WHERE conflicted with   
           Full text indexing not meeting performance standards (subseconds to minutes) and due to dynamic sort.  
      Implementation with Infrastructure release on 3/1/2015.       
  HG  2015-03-03 FT INDEX post production release fix, Vendory_Type replaced with Account_Type changed it back TO RETURN Vendor_Type  
      Keeping the Dynamic query as we have dynamic sorting and lot of optional parameters passed TO the sproc.  
  RR  2015-09-08 Global Sourcing - Phase2 - Replaced ',' with '^' in utility string concatenation as the application showing  
       multiple link and listing the vendors in a pop-up, vendors having comma in name creating issue while splitting.   
      Added input optional parameters Country_Id, Alternate_Account_Number, Meter_Type_Cd, Contract_Classification_Cd,   
       Contract_Product_Type_Cd, Utility_Vendor_Id, Supplier_Vendor_Id  
      Added Account_Number, Alternate_Account_Number, Meter_Number, Meter_Type, Country_Name to select clause  
      Removed the limit TO SELECT TOP 10 records OF utilities, cbms_Image_ids, site_ids, Analysts, Account_Numbers,   
       Alternate_Account_Numbers, Meter_Numbers in CROSS APPLY, application modified to show ALL records IN a popup  
      Analyst logic modifed TO look FOR custom analyst also.  
  NR  2016-02-03 MAINT-3783 Changed the join on dbo.Vendor_Commodity_Analyst_Map from INNER to LEFT.
  PRV 2019-06-26 Added Account_Id, Sitegroup_Id Columns for GRM requirement.  
******/
CREATE PROCEDURE [dbo].[RM_Group_Search_Contracts]
(
    @clientId INT = NULL,
    @stateId VARCHAR(MAX) = NULL,
    @contractNumber VARCHAR(150) = NULL,
    @city VARCHAR(200) = NULL,
    @accountNumber VARCHAR(50) = NULL,
    @meterNumber VARCHAR(50) = NULL,
    @vendorTypeId INT = NULL,
    @supplierName VARCHAR(200) = NULL,
    @utilityName VARCHAR(200) = NULL,
    @commodityId INT = NULL,
    @Sortindex VARCHAR(20) = NULL,
    @SortColumn VARCHAR(200) = NULL,
    @Region VARCHAR(200) = NULL,
    @ContractStatus INT = 1,
    @IsFilterByContractStartDate BIT = NULL,
    @IsFilterByContractEndDate BIT = NULL,
    @FromDate DATE = NULL,
    @ToDate DATE = NULL,
    @StartIndex INT = 1,
    @EndIndex INT = 2147483647,
    @AnalystId INT = NULL,
    @debug BIT = 0,
    @Country_Id INT = NULL,
    @Alternate_Account_Number NVARCHAR(400) = NULL,
    @Meter_Type_Cd INT = NULL,
    @Contract_Classification_Cd INT = NULL,
    @Contract_Product_Type_Cd INT = NULL,
    @Utility_Vendor_Id INT = NULL,
    @Supplier_Vendor_Id INT = NULL
)
AS
BEGIN

    SET NOCOUNT ON;

    DECLARE @Account_Number_Search VARCHAR(200) = '"*' + dbo.udf_StripNonAlphaNumerics(@accountNumber) + '*"',
            @Contract_Number_Search VARCHAR(200) = '"*' + dbo.udf_StripNonAlphaNumerics(@contractNumber) + '*"',
            @Meter_Number_Search VARCHAR(200) = '"*' + dbo.udf_StripNonAlphaNumerics(@meterNumber) + '*"',
            @City_Search VARCHAR(200) = '"*' + dbo.udf_StripNonAlphaNumerics(@city) + '*"',
            @Supplier_Name_Search VARCHAR(200) = '"*' + dbo.udf_StripNonAlphaNumerics(@supplierName) + '*"',
            @Utility_Name_Search VARCHAR(200) = '"*' + dbo.udf_StripNonAlphaNumerics(@utilityName) + '*"',
            @Top_Count INT,
            @Total INT = 0,
            @Default_Analyst INT,
            @Custom_Analyst INT,
            @RM_Group_Participant_Type_Cd INT;


    SELECT @RM_Group_Participant_Type_Cd = cd.Code_Id
    FROM dbo.Code cd
        INNER JOIN dbo.Codeset cs
            ON cd.Codeset_Id = cs.Codeset_Id
    WHERE cs.Codeset_Name = 'RM Group Participants'
          AND cd.Code_Value = 'Contract';

    IF @EndIndex = 2147483647
    BEGIN
        SET @Top_Count = 2147483647;
    END;
    ELSE
    BEGIN
        SET @Top_Count = @StartIndex + @EndIndex;
    END;

    /*Setting SortColumn for Contract Id as it is in Present in Contract and supplier_account_meter_map*/
    CREATE TABLE #Contracts
    (
        CONTRACT_ID INT,
        ED_CONTRACT_NUMBER VARCHAR(150),
        Client_Name VARCHAR(200),
        vendor_type CHAR(8),
        Supplier VARCHAR(200),
        Commodity_Name VARCHAR(50),
        Commodity_Id INT,
        CONTRACT_START_DATE DATETIME,
        CONTRACT_END_DATE DATETIME,
        Total INT NULL,
        Row_Num INT IDENTITY(1, 1),
        Client_Id INT
    );

    CREATE TABLE #Vendor_Type
    (
        Entity_Id INT,
        Entity_Name VARCHAR(200)
    );

    CREATE TABLE #Contract_Results
    (
        CONTRACT_ID INT,
        City VARCHAR(200),
        Site_Id INT,
        Site_name VARCHAR(200),
        Utility VARCHAR(200),
        Utility_Id INT,
        Commodity_Id INT,
        Account_Number VARCHAR(50),
        Alternate_Account_Number NVARCHAR(400),
        Meter_Number VARCHAR(50),
        Meter_Type VARCHAR(25),
        State_Name VARCHAR(200),
        Country_Name VARCHAR(200),
        Analyst_Name VARCHAR(100),
        State_Id INT,
        Account_Id INT,
        Sitegroup_Id INT
    );

    INSERT INTO #Vendor_Type
    (
        Entity_Id,
        Entity_Name
    )
    SELECT ENTITY_ID,
           ENTITY_NAME
    FROM dbo.ENTITY
    WHERE ENTITY_DESCRIPTION = 'Vendor';

    DECLARE @Contract_Results1 TABLE
    (
        CONTRACT_ID INT,
        City VARCHAR(MAX),
        Site_Id VARCHAR(MAX),
        Site_name VARCHAR(MAX),
        Utility VARCHAR(MAX),
        Cbms_Image_ID VARCHAR(MAX),
        Analyst VARCHAR(MAX),
        Account_Number VARCHAR(MAX),
        Alternate_Account_Number NVARCHAR(MAX),
        Meter_Number VARCHAR(MAX),
        Meter_Type VARCHAR(MAX),
        State_Name VARCHAR(MAX),
        Country_Name VARCHAR(MAX),
        State_Ids VARCHAR(MAX),
        Account_Id VARCHAR(MAX),
        Sitegroup_Id VARCHAR(MAX)
    );

    SELECT @Default_Analyst = MAX(   CASE
                                         WHEN c.Code_Value = 'Default' THEN
                                             c.Code_Id
                                     END
                                 ),
           @Custom_Analyst = MAX(   CASE
                                        WHEN c.Code_Value = 'Custom' THEN
                                            c.Code_Id
                                    END
                                )
    FROM dbo.Code c
        INNER JOIN dbo.Codeset cs
            ON c.Codeset_Id = cs.Codeset_Id
    WHERE cs.Codeset_Name = 'Analyst Type';

    DECLARE @SQL VARCHAR(MAX) = '';

    DECLARE @Orderby_Column VARCHAR(100);

    SELECT @SortColumn = CASE
                             WHEN @SortColumn IS NULL THEN
                                 'Client_Name asc, contract_end_date desc'
                             ELSE
                                 @SortColumn + ' ' + @Sortindex
                         END;

    SELECT @Orderby_Column = CASE @SortColumn
                                 WHEN 'Client_Name asc, contract_end_date desc' THEN
                                     'ch.Client_Name ASC, CONVERT(VARCHAR(10), con.CONTRACT_END_DATE, 112) DESC'
                                 WHEN 'contract_id asc' THEN
                                     'CONVERT(varchar, con.Contract_Id) ASC'
                                 WHEN 'vendor_name asc' THEN
                                     'CHASupp.Account_Vendor_Name ASC'
                                 WHEN 'vendor_type asc' THEN
                                     'VenType.Entity_Name ASC'
                                 WHEN 'ED_CONTRACT_NUMBER asc' THEN
                                     'con.ED_CONTRACT_NUMBER ASC'
                                 WHEN 'Commodity_Name asc' THEN
                                     'com.Commodity_Name ASC'
                                 WHEN 'CONTRACT_START_DATE asc' THEN
                                     'CONVERT(VARCHAR(10), con.CONTRACT_START_DATE, 112) ASC'
                                 WHEN 'CONTRACT_END_DATE asc' THEN
                                     'CONVERT(VARCHAR(10), con.CONTRACT_END_DATE, 112) ASC'
                                 WHEN 'State_Name asc' THEN
                                     'CH.State_Name ASC'
                                 WHEN 'contract_id desc' THEN
                                     'CONVERT(varchar, con.Contract_Id) DESC'
                                 WHEN 'vendor_name desc' THEN
                                     'CHASupp.Account_Vendor_Name DESC'
                                 WHEN 'vendor_type desc' THEN
                                     'CHASupp.Account_Type DESC'
                                 WHEN 'ED_CONTRACT_NUMBER desc' THEN
                                     'con.ED_CONTRACT_NUMBER DESC'
                                 WHEN 'Commodity_Name desc' THEN
                                     'com.Commodity_Name DESC'
                                 WHEN 'CONTRACT_START_DATE desc' THEN
                                     'CONVERT(VARCHAR(10), con.CONTRACT_START_DATE, 112) DESC'
                                 WHEN 'CONTRACT_END_DATE desc' THEN
                                     'CONVERT(VARCHAR(10), con.CONTRACT_END_DATE, 112) DESC'
                                 WHEN 'State_Name desc' THEN
                                     'CH.State_Name DESC'
                             END;

    SET @SQL
        = ' INSERT      INTO #Contracts  
                        ( CONTRACT_ID  
                        ,ED_CONTRACT_NUMBER  
                        ,Client_Name  
                        ,vendor_type  
                        ,Supplier  
                        ,Commodity_Name  
      ,Commodity_Id  
                        ,CONTRACT_START_DATE  
                        ,CONTRACT_END_DATE  
                        ,Client_Id  
                        )  
                  SELECT con.CONTRACT_ID              
      ,con.ED_CONTRACT_NUMBER              
      ,ch.Client_Name              
      ,VenType.Entity_Name AS vendor_type              
      ,CHASupp.Account_Vendor_Name AS Supplier              
      ,com.Commodity_Name     
      ,Com.Commodity_Id           
      ,con.CONTRACT_START_DATE              
      ,con.CONTRACT_END_DATE  
      ,ch.Client_Id               
     FROM              
      Core.Client_Hier CH              
      JOIN Core.Client_Hier_Account CHAUti              
       ON CHAUti.Client_Hier_Id = CH.Client_Hier_Id              
      JOIN Core.Client_Hier_Account CHASupp              
       ON CHASupp.Meter_Id = CHAUti.Meter_Id              
      JOIN dbo.CONTRACT CON              
       ON CON.CONTRACT_ID = CHASupp.Supplier_Contract_ID  
      JOIN #Vendor_Type VenType  
       ON CHASupp.Account_Vendor_Type_ID = VenType.Entity_Id  
      JOIN dbo.Commodity COM              
       ON CON.COMMODITY_TYPE_ID = COM.Commodity_Id  
      JOIN dbo.VENDOR_COMMODITY_MAP vcm  
       ON vcm.VENDOR_ID = CHAUti.Account_Vendor_Id  
       AND vcm.COMMODITY_TYPE_ID = CON.COMMODITY_TYPE_ID  
         JOIN dbo.VENDOR_COMMODITY_MAP suppven  
       ON suppven.VENDOR_ID = CHASupp.Account_Vendor_Id  
       AND suppven.COMMODITY_TYPE_ID = CON.COMMODITY_TYPE_ID  
      LEFT OUTER JOIN dbo.Vendor_Commodity_Analyst_Map vcam  
       ON vcam.Vendor_Commodity_Map_Id = vcm.VENDOR_COMMODITY_MAP_ID  
      LEFT JOIN dbo.Code mtr_typ  
                            ON CHAUti.Meter_Type_Cd = mtr_typ.Code_Id  
                        LEFT JOIN dbo.ENTITY cc  
       ON CON.Contract_Classification_Cd = cc.ENTITY_ID  
      LEFT JOIN dbo.Code cp  
       ON CON.Contract_Product_Type_Cd = cp.Code_Id  
      INNER JOIN core.Client_Commodity ccc  
                              ON ccc.Client_Id = ch.Client_Id  
                                 AND ccc.Commodity_Id = CHAUti.Commodity_Id  
                        LEFT JOIN dbo.Account_Commodity_Analyst aca  
                              ON aca.Account_Id = CHAUti.Account_Id  
                                 AND aca.Commodity_Id = CHAUti.Commodity_Id  
      LEFT JOIN dbo.SITE_Commodity_Analyst sca  
                              ON sca.Site_Id = ch.Site_Id  
                                 AND sca.Commodity_Id = CHAUti.Commodity_Id  
      LEFT JOIN dbo.Client_Commodity_Analyst cca  
                              ON ccc.Client_Commodity_Id = cca.Client_Commodity_Id  
                   WHERE              
                         CHAUti.Account_Type = ''Utility''              
       AND CHASupp.Account_Type = ''Supplier''';

    SET @SQL = @SQL + CASE
                          WHEN @clientId IS NOT NULL THEN
                              ' AND CH.Client_Id = ' + STR(@clientId)
                          ELSE
                              ''
                      END;
    SET @SQL = @SQL + CASE
                          WHEN @stateId IS NOT NULL THEN
                              ' AND CH.State_Id in (' + (@stateId) + ')'
                          ELSE
                              ''
                      END;
    SET @SQL
        = @SQL
          + CASE
                WHEN @contractNumber IS NOT NULL THEN
                    ' And CONTAINS((CON.ED_CONTRACT_NUMBER_FTSearch,CON.ED_CONTRACT_NUMBER_FTSearch),'''
                    + @Contract_Number_Search + ''')'
                ELSE
                    ''
            END;
    SET @SQL = @SQL + CASE
                          WHEN @city IS NOT NULL THEN
                              ' And CONTAINS((ch.city_FTSearch, ch.City),''' + @City_Search + ''')'
                          ELSE
                              ''
                      END;
    SET @SQL
        = @SQL
          + CASE
                WHEN @accountNumber IS NOT NULL THEN
                    ' And CONTAINS((CHAUti.Account_Number_FTSearch,CHAUti.Account_Number),''' + @Account_Number_Search
                    + ''')'
                ELSE
                    ''
            END;
    SET @SQL
        = @SQL
          + CASE
                WHEN @meterNumber IS NOT NULL THEN
                    ' And CONTAINS((CHASupp.Meter_Number_FTSearch,CHASupp.Meter_Number,CHASupp.Meter_Number_Search, CHASupp.Meter_Number),'''
                    + @Meter_Number_Search + ''')'
                ELSE
                    ''
            END;
    SET @SQL = @SQL + CASE
                          WHEN @vendorTypeId IS NOT NULL THEN
                              ' AND CHASupp.Account_Vendor_Type_ID = ' + STR(@vendorTypeId)
                          ELSE
                              ''
                      END;
    SET @SQL
        = @SQL
          + CASE
                WHEN @supplierName IS NOT NULL THEN
                    ' And CONTAINS((CHASupp.Account_Vendor_Name_FTSearch,CHASupp.Account_Vendor_Name),'''
                    + @Supplier_Name_Search + ''')'
                ELSE
                    ''
            END;
    SET @SQL
        = @SQL
          + CASE
                WHEN @utilityName IS NOT NULL THEN
                    ' And CONTAINS((CHAUti.Account_Vendor_Name_FTSearch,CHAUti.Account_Vendor_Name),'''
                    + @Utility_Name_Search + ''')'
                ELSE
                    ''
            END;
    SET @SQL = @SQL + CASE
                          WHEN @commodityId IS NOT NULL THEN
                              ' AND CON.COMMODITY_TYPE_ID = ' + STR(@commodityId)
                          ELSE
                              ''
                      END;
    SET @SQL = @SQL + CASE
                          WHEN @Region IS NOT NULL THEN
                              ' AND CH.Region_ID IN (' + @Region + ')'
                          ELSE
                              ''
                      END;
    SET @SQL
        = @SQL
          + CASE
                WHEN @IsFilterByContractStartDate = 1
                     AND @FromDate IS NOT NULL THEN
                    ' AND con.CONTRACT_START_DATE BETWEEN ''' + CONVERT(VARCHAR(10), @FromDate) + ''' AND '''
                    + CONVERT(VARCHAR(10), @ToDate) + ''''
                ELSE
                    ''
            END;
    SET @SQL
        = @SQL
          + CASE
                WHEN @IsFilterByContractEndDate = 1
                     AND @ToDate IS NOT NULL THEN
                    ' AND con.CONTRACT_END_DATE BETWEEN ''' + CONVERT(VARCHAR(10), @FromDate) + ''' AND '''
                    + CONVERT(VARCHAR(10), @ToDate) + ''''
                ELSE
                    ''
            END;
    SET @SQL = @SQL + CASE
                          WHEN @ContractStatus = 1 THEN
                              ' AND con.CONTRACT_END_DATE > ''' + CONVERT(VARCHAR(10), GETDATE(), 120) + ''''
                          WHEN @ContractStatus = 0 THEN
                              ' AND con.CONTRACT_END_DATE < ''' + CONVERT(VARCHAR(10), GETDATE(), 120) + ''''
                          ELSE
                              ''
                      END;
    SET @SQL
        = @SQL
          + CASE
                WHEN @AnalystId IS NOT NULL THEN
                    ' AND case WHEN coalesce(CHAUti.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) = '
                    + STR(@Custom_Analyst)
                    + ' THEN coalesce(aca.Analyst_User_Info_Id, sca
.Analyst_User_Info_Id, cca.Analyst_User_Info_Id)  
                                                             ELSE vcam.Analyst_ID  
                                                        END = ' + STR(@AnalystId)
                ELSE
                    ''
            END;
    SET @SQL = @SQL + CASE
                          WHEN @Country_Id IS NOT NULL THEN
                              ' AND CH.Country_Id = ' + STR(@Country_Id)
                          ELSE
                              ''
                      END;
    SET @SQL = @SQL + CASE
                          WHEN @Alternate_Account_Number IS NOT NULL THEN
                              ' AND CHAUti.Alternate_Account_Number like ''%' + @Alternate_Account_Number + '%'''
                          ELSE
                              ''
                      END;
    SET @SQL = @SQL + CASE
                          WHEN @Meter_Type_Cd IS NOT NULL THEN
                              ' AND CHAUti.Meter_Type_Cd = ' + STR(@Meter_Type_Cd)
                          ELSE
                              ''
                      END;
    SET @SQL = @SQL + CASE
                          WHEN @Contract_Classification_Cd IS NOT NULL THEN
                              ' AND CON.Contract_Classification_Cd = ' + STR(@Contract_Classification_Cd)
                          ELSE
                              ''
                      END;
    SET @SQL = @SQL + CASE
                          WHEN @Contract_Product_Type_Cd IS NOT NULL THEN
                              ' AND CON.Contract_Product_Type_Cd = ' + STR(@Contract_Product_Type_Cd)
                          ELSE
                              ''
                      END;
    SET @SQL = @SQL + CASE
                          WHEN @Utility_Vendor_Id IS NOT NULL THEN
                              ' AND CHAUti.Account_Vendor_Id = ' + STR(@Utility_Vendor_Id)
                          ELSE
                              ''
                      END;
    SET @SQL = @SQL + CASE
                          WHEN @Supplier_Vendor_Id IS NOT NULL THEN
                              ' AND CHASupp.Account_Vendor_Id = ' + STR(@Supplier_Vendor_Id)
                          ELSE
                              ''
                      END;

    SET @SQL
        = @SQL
          + '               
      GROUP BY         
       con.CONTRACT_ID              
       ,con.ED_CONTRACT_NUMBER              
       ,ch.Client_Name              
       ,VenType.Entity_Name  
       ,CHASupp.Account_Vendor_Name              
       ,com.Commodity_Name    
       ,Com.Commodity_Id            
       ,con.CONTRACT_START_DATE              
       ,con.CONTRACT_END_DATE   
       ,ch.Client_Id    
                 ';
    SET @SQL = @SQL + 'Order By ' + @Orderby_Column;

    IF @debug = 1
    BEGIN
        PRINT (@SQL);
    END;

    EXEC (@SQL);

    SELECT @Total = MAX(Row_Num)
    FROM #Contracts;

    UPDATE #Contracts
    SET Total = @Total;


    INSERT #Contract_Results
    (
        CONTRACT_ID,
        City,
        Site_Id,
        Site_name,
        Utility,
        Utility_Id,
        Commodity_Id,
        Account_Number,
        Alternate_Account_Number,
        Meter_Number,
        Meter_Type,
        State_Name,
        Country_Name,
        Analyst_Name,
        State_Id,
        Account_Id,
        Sitegroup_Id
    )
    SELECT CON.CONTRACT_ID,
           CH.City,
           CH.Site_Id,
           CH.Site_name,
           CHAUti.Account_Vendor_Name AS Utility,
           CHAUti.Account_Vendor_Id,
           CON.Commodity_Id,
           CHAUti.Display_Account_Number,
           CHAUti.Alternate_Account_Number,
           CHAUti.Meter_Number,
           mtr_typ.Code_Value,
           CH.State_Name,
           CH.Country_Name,
           ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME,
           CH.State_Id,
           CHAUti.Account_Id,
           CH.Sitegroup_Id
    FROM Core.Client_Hier CH
        INNER JOIN Core.Client_Hier_Account CHAUti
            ON CHAUti.Client_Hier_Id = CH.Client_Hier_Id
        INNER JOIN Core.Client_Hier_Account CHASupp
            ON CHASupp.Meter_Id = CHAUti.Meter_Id
        INNER JOIN #Contracts CON
            ON CON.CONTRACT_ID = CHASupp.Supplier_Contract_ID
        LEFT JOIN dbo.Code mtr_typ
            ON CHAUti.Meter_Type_Cd = mtr_typ.Code_Id
        INNER JOIN dbo.VENDOR_COMMODITY_MAP vcm
            ON vcm.VENDOR_ID = CHAUti.Account_Vendor_Id
               AND vcm.COMMODITY_TYPE_ID = CON.Commodity_Id
        LEFT JOIN dbo.Vendor_Commodity_Analyst_Map vcam
            ON vcam.Vendor_Commodity_Map_Id = vcm.VENDOR_COMMODITY_MAP_ID
        INNER JOIN Core.Client_Commodity ccc
            ON ccc.Client_Id = CH.Client_Id
               AND ccc.Commodity_Id = CHAUti.Commodity_Id
        LEFT JOIN dbo.Account_Commodity_Analyst aca
            ON aca.Account_Id = CHAUti.Account_Id
               AND aca.Commodity_Id = CHAUti.Commodity_Id
        LEFT JOIN dbo.Site_Commodity_Analyst sca
            ON sca.Site_Id = CH.Site_Id
               AND sca.Commodity_Id = CHAUti.Commodity_Id
        LEFT JOIN dbo.Client_Commodity_Analyst cca
            ON ccc.Client_Commodity_Id = cca.Client_Commodity_Id
        LEFT JOIN dbo.USER_INFO ui
            ON ui.USER_INFO_ID = CASE
                                     WHEN COALESCE(
                                                      CHAUti.Account_Analyst_Mapping_Cd,
                                                      CH.Site_Analyst_Mapping_Cd,
                                                      CH.Client_Analyst_Mapping_Cd
                                                  ) = @Custom_Analyst THEN
                                         COALESCE(
                                                     aca.Analyst_User_Info_Id,
                                                     sca.Analyst_User_Info_Id,
                                                     cca.Analyst_User_Info_Id
                                                 )
                                     ELSE
                                         vcam.Analyst_Id
                                 END
    WHERE CHAUti.Account_Type = 'Utility'
          AND CHASupp.Account_Type = 'Supplier'
    GROUP BY CON.CONTRACT_ID,
             CH.Site_Id,
             CH.Site_name,
             CH.City,
             CHAUti.Account_Vendor_Name,
             CHAUti.Account_Vendor_Id,
             CON.Commodity_Id,
             CHAUti.Display_Account_Number,
             CHAUti.Alternate_Account_Number,
             CHAUti.Meter_Number,
             mtr_typ.Code_Value,
             CH.State_Name,
             CH.Country_Name,
             ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME,
             CH.State_Id,
             CHAUti.Account_Id,
             CH.Sitegroup_Id;


    CREATE CLUSTERED INDEX IX_TContract_Results__Contract_Id
    ON #Contract_Results (CONTRACT_ID);

    IF @debug = 1
    BEGIN
        SELECT '#Contracts',
               *
        FROM #Contracts;
        SELECT '#Contract_Results',
               *
        FROM #Contract_Results;
    END;

    IF @debug = 1
    BEGIN
        SELECT '#Analysts',
               *
        FROM #Contracts;
    END;


    INSERT INTO @Contract_Results1
    (
        CONTRACT_ID,
        City,
        Site_Id,
        Site_name,
        Utility,
        Cbms_Image_ID,
        Analyst,
        Account_Number,
        Alternate_Account_Number,
        Meter_Number,
        Meter_Type,
        State_Name,
        Country_Name,
        State_Ids,
        Account_Id,
        Sitegroup_Id
    )
    SELECT cr.CONTRACT_ID,
           city = REPLACE(LEFT(a.ad_list, LEN(a.ad_list) - 1), '&amp;', '&'),
           site_id = LEFT(s_id.s_list, LEN(s_id.s_list) - 1),
           site_name = REPLACE(LEFT(s_name.sname_list, LEN(s_name.sname_list) - 1), '&amp;', '&'),
           utility = REPLACE(LEFT(ut.ut_list, LEN(ut.ut_list) - 1), '&amp;', '&'),
           cbms_Image_id = LEFT(I_ID.I_List, LEN(I_ID.I_List) - 1),
           --,Analyst = replace(left(ua.ua_list, len(ua.ua_list) - 1), '&amp;', '&')  
           Analyst = REPLACE(LEFT(Analyst.Analyst_list, LEN(Analyst.Analyst_list) - 1), '&amp;', '&'),
           Account_Number = REPLACE(LEFT(accnum.accnum_list, LEN(accnum.accnum_list) - 1), '&amp;', '&'),
           Alternate_Account_Number = REPLACE(
                                                 LEFT(altaccnum.altaccnum_list, LEN(altaccnum.altaccnum_list) - 1),
                                                 '&amp;',
                                                 '&'
                                             ),
           Meter_Number = REPLACE(LEFT(mtrnum.mtrnum_list, LEN(mtrnum.mtrnum_list) - 1), '&amp;', '&'),
           Meter_Type = REPLACE(LEFT(mtrtyp.mtrtyp_list, LEN(mtrtyp.mtrtyp_list) - 1), '&amp;', '&'),
           State_Name = REPLACE(LEFT(stts.stts_list, LEN(stts.stts_list) - 1), '&amp;', '&'),
           Country_Name = REPLACE(LEFT(contrs.contrs_list, LEN(contrs.contrs_list) - 1), '&amp;', '&'),
           State_Ids = REPLACE(LEFT(sttids.sttids_list, LEN(sttids.sttids_list) - 1), '&amp;', '&'),
           Account_Id = REPLACE(LEFT(Acc_id.Acc_list, LEN(Acc_id.Acc_list) - 1), '&amp;', '&'),
           Sitegroup_Id = REPLACE(LEFT(SG_id.SG_list, LEN(SG_id.SG_list) - 1), '&amp;', '&')
    FROM #Contracts cr
        CROSS APPLY
    (
        SELECT CRI1.Utility + '| '
        FROM #Contract_Results CRI1
        WHERE cr.CONTRACT_ID = CRI1.CONTRACT_ID
              AND CRI1.Utility IS NOT NULL
        GROUP BY CRI1.Utility
        FOR XML PATH('')
    ) ut(ut_list)
        CROSS APPLY
    (
        SELECT CRI2.City + '| '
        FROM #Contract_Results CRI2
        WHERE cr.CONTRACT_ID = CRI2.CONTRACT_ID
              AND CRI2.City IS NOT NULL
        GROUP BY CRI2.City
        FOR XML PATH('')
    ) a(ad_list)
        CROSS APPLY
    (
        SELECT CONVERT(VARCHAR(15), CRI3.Site_Id) + '| '
        FROM #Contract_Results CRI3
        WHERE cr.CONTRACT_ID = CRI3.CONTRACT_ID
              AND CRI3.Site_Id IS NOT NULL
        GROUP BY CRI3.Site_Id
        FOR XML PATH('')
    ) s_id(s_list)
        CROSS APPLY
    (
        SELECT CRI4.Site_name + '| '
        FROM #Contract_Results CRI4
        WHERE cr.CONTRACT_ID = CRI4.CONTRACT_ID
              AND CRI4.Site_name IS NOT NULL
        GROUP BY CRI4.Site_name
        FOR XML PATH('')
    ) s_name(sname_list)
        CROSS APPLY
    (
        SELECT CONVERT(VARCHAR(100), CRI5.CBMS_IMAGE_ID) + '| '
        FROM dbo.CONTRACT_CBMS_IMAGE_MAP CRI5
        WHERE cr.CONTRACT_ID = CRI5.CONTRACT_ID
              AND CRI5.CBMS_IMAGE_ID IS NOT NULL
        GROUP BY CRI5.CBMS_IMAGE_ID
        FOR XML PATH('')
    ) I_ID(I_List)
        CROSS APPLY
    (
        SELECT cr_accnum.Account_Number + '| '
        FROM #Contract_Results cr_accnum
        WHERE cr.CONTRACT_ID = cr_accnum.CONTRACT_ID
              AND cr_accnum.Account_Number IS NOT NULL
        GROUP BY cr_accnum.Account_Number
        FOR XML PATH('')
    ) accnum(accnum_list)
        CROSS APPLY
    (
        SELECT cr_altaccnum.Alternate_Account_Number + '| '
        FROM #Contract_Results cr_altaccnum
        WHERE cr.CONTRACT_ID = cr_altaccnum.CONTRACT_ID
              AND cr_altaccnum.Alternate_Account_Number IS NOT NULL
        GROUP BY cr_altaccnum.Alternate_Account_Number
        FOR XML PATH('')
    ) altaccnum(altaccnum_list)
        CROSS APPLY
    (
        SELECT cr_mtrnum.Meter_Number + '| '
        FROM #Contract_Results cr_mtrnum
        WHERE cr.CONTRACT_ID = cr_mtrnum.CONTRACT_ID
              AND cr_mtrnum.Meter_Number IS NOT NULL
        GROUP BY cr_mtrnum.Meter_Number
        FOR XML PATH('')
    ) mtrnum(mtrnum_list)
        CROSS APPLY
    (
        SELECT cr_mtrtyp.Meter_Type + '| '
        FROM #Contract_Results cr_mtrtyp
        WHERE cr.CONTRACT_ID = cr_mtrtyp.CONTRACT_ID
              AND cr_mtrtyp.Meter_Type IS NOT NULL
        GROUP BY cr_mtrtyp.Meter_Type
        FOR XML PATH('')
    ) mtrtyp(mtrtyp_list)
        CROSS APPLY
    (
        SELECT stts.State_Name + '| '
        FROM #Contract_Results stts
        WHERE cr.CONTRACT_ID = stts.CONTRACT_ID
              AND stts.State_Name IS NOT NULL
        GROUP BY stts.State_Name
        FOR XML PATH('')
    ) stts(stts_list)
        CROSS APPLY
    (
        SELECT contrs.Country_Name + '| '
        FROM #Contract_Results contrs
        WHERE cr.CONTRACT_ID = contrs.CONTRACT_ID
              AND contrs.Country_Name IS NOT NULL
        GROUP BY contrs.Country_Name
        FOR XML PATH('')
    ) contrs(contrs_list)
        CROSS APPLY
    (
        SELECT Analyst.Analyst_Name + '| '
        FROM #Contract_Results Analyst
        WHERE cr.CONTRACT_ID = Analyst.CONTRACT_ID
              AND Analyst.Analyst_Name IS NOT NULL
        GROUP BY Analyst.Analyst_Name
        FOR XML PATH('')
    ) Analyst(Analyst_list)
        CROSS APPLY
    (
        SELECT CONVERT(VARCHAR(15), sttids.State_Id) + '| '
        FROM #Contract_Results sttids
        WHERE cr.CONTRACT_ID = sttids.CONTRACT_ID
              AND sttids.State_Id IS NOT NULL
        GROUP BY sttids.State_Id
        FOR XML PATH('')
    ) sttids(sttids_list)
        CROSS APPLY
    (
        SELECT CONVERT(VARCHAR(15), CRI3.Account_Id) + '| '
        FROM #Contract_Results CRI3
        WHERE cr.CONTRACT_ID = CRI3.CONTRACT_ID
              AND CRI3.Account_Id IS NOT NULL
        GROUP BY CRI3.Account_Id
        FOR XML PATH('')
    ) Acc_id(Acc_list)
        CROSS APPLY
    (
        SELECT CONVERT(VARCHAR(15), CRI3.Sitegroup_Id) + '| '
        FROM #Contract_Results CRI3
        WHERE cr.CONTRACT_ID = CRI3.CONTRACT_ID
              AND CRI3.Sitegroup_Id IS NOT NULL
        GROUP BY CRI3.Sitegroup_Id
        FOR XML PATH('')
    ) SG_id(SG_list);


    SELECT CR.CONTRACT_ID,
           CR.ED_CONTRACT_NUMBER,
           CR.Client_Name,
           cr1.State_Name,
           cr1.City,
           CR.vendor_type,
           CR.Supplier,
           cr1.Utility,
           CR.Commodity_Name,
           cr1.Analyst,
           CR.CONTRACT_START_DATE,
           CR.CONTRACT_END_DATE,
           cr1.Site_Id,
           cr1.Site_name,
           cr1.Cbms_Image_ID,
           CR.Total,
           cr1.Account_Number,
           cr1.Alternate_Account_Number,
           cr1.Meter_Number,
           cr1.Meter_Type,
           cr1.Country_Name,
           CR.Commodity_Id,
           CR.Client_Id,
           cr1.State_Ids,
           cr1.Account_Id,
           cr1.Sitegroup_Id,
           MAX(dtl.Cbms_Sitegroup_Id) AS RM_Group_Id
    FROM #Contracts CR
        INNER JOIN @Contract_Results1 cr1
            ON cr1.CONTRACT_ID = CR.CONTRACT_ID
        LEFT JOIN dbo.Cbms_Sitegroup_Participant dtl
            ON CR.CONTRACT_ID = dtl.Group_Participant_Id
               AND dtl.Group_Participant_Type_Cd = @RM_Group_Participant_Type_Cd
    WHERE CR.Row_Num
    BETWEEN @StartIndex AND @EndIndex
    GROUP BY CR.CONTRACT_ID,
             CR.ED_CONTRACT_NUMBER,
             CR.Client_Name,
             cr1.State_Name,
             cr1.City,
             CR.vendor_type,
             CR.Supplier,
             cr1.Utility,
             CR.Commodity_Name,
             cr1.Analyst,
             CR.CONTRACT_START_DATE,
             CR.CONTRACT_END_DATE,
             cr1.Site_Id,
             cr1.Site_name,
             cr1.Cbms_Image_ID,
             CR.Total,
             cr1.Account_Number,
             cr1.Alternate_Account_Number,
             cr1.Meter_Number,
             cr1.Meter_Type,
             cr1.Country_Name,
             CR.Commodity_Id,
             CR.Client_Id,
             cr1.State_Ids,
             cr1.Account_Id,
             cr1.Sitegroup_Id,
             CR.Row_Num
    ORDER BY CR.Row_Num;

    DROP TABLE #Contracts;
    DROP TABLE #Contract_Results;
    DROP TABLE #Vendor_Type;

END;

GO
GRANT EXECUTE ON  [dbo].[RM_Group_Search_Contracts] TO [CBMSApplication]
GO
