SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********   
NAME:  dbo.Master_Variance_Test_SAVE  
 
DESCRIPTION:  Used to save all the Master level data  

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
	@is_Inclusive			bit
	@is_Multiple_Condition	bit
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:   

	
------------------------------------------------------------  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
NK   Nageswara Rao Kosuri         


Initials Date  Modification  
------------------------------------------------------------  
NK	10/13/2009  Created


 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.Master_Variance_Test_SAVE
	@is_Inclusive bit,
	@is_Multiple_Condition bit
AS

BEGIN
	
	SET NOCOUNT ON;
	 
	 INSERT INTO Variance_Test_Master(
		Is_Inclusive,
		Is_Multiple_Condition
		)    
	values(@is_Inclusive
		,@is_Multiple_Condition)
	

	SELECT SCOPE_IDENTITY() as Variance_Test_Id	
END
GO
GRANT EXECUTE ON  [dbo].[Master_Variance_Test_SAVE] TO [CBMSApplication]
GO
