SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_SAVE_DEAL_TICKET_RESPONSIBLE_FOR_BALANCING_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@srDealTicketId	int       	          	
	@utilityTypeId 	int       	          	
	@pipelineTypeId	int       	          	
	@deliveryPointTypeId	int       	          	
	@cityGateValue 	varchar(200)	          	
	@otherValue    	varchar(200)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE     PROCEDURE dbo.SR_SAD_SAVE_DEAL_TICKET_RESPONSIBLE_FOR_BALANCING_DETAILS_P
@userId varchar(10),
@sessionId varchar(20),
@srDealTicketId int,
@utilityTypeId int,
@pipelineTypeId int,
@deliveryPointTypeId int,
@cityGateValue varchar(200),
@otherValue varchar(200)

--select * from SR_DT_RESPONSIBLE_FOR_BALANCING where SR_DEAL_TICKET_ID=919

AS
set nocount on
IF (select count(1) from SR_DT_RESPONSIBLE_FOR_BALANCING where SR_DEAL_TICKET_ID = @srDealTicketId) = 0
BEGIN

	insert into SR_DT_RESPONSIBLE_FOR_BALANCING
		(SR_DEAL_TICKET_ID, UTILITY_TYPE_ID, PIPELINE_TYPE_ID, DELIVERY_POINT_TYPE_ID, 
		CITYGATE_VALUE, OTHER_VALUE)
	
	values	(@srDealTicketId, @utilityTypeId, @pipelineTypeId, @deliveryPointTypeId, @cityGateValue, @otherValue)

END

ELSE

BEGIN


	update SR_DT_RESPONSIBLE_FOR_BALANCING set UTILITY_TYPE_ID=@utilityTypeId, PIPELINE_TYPE_ID=@pipelineTypeId,
		DELIVERY_POINT_TYPE_ID=@deliveryPointTypeId, CITYGATE_VALUE=@cityGateValue,
		OTHER_VALUE=@otherValue

	where 
		SR_DEAL_TICKET_ID=@srDealTicketId

END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_SAVE_DEAL_TICKET_RESPONSIBLE_FOR_BALANCING_DETAILS_P] TO [CBMSApplication]
GO
