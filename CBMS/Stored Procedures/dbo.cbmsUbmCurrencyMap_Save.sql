SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[cbmsUbmCurrencyMap_Save]
	( @MyAccountId int
	, @ubm_currency_map_id int = null
	, @ubm_id int
	, @ubm_currency_code varchar(200)
	, @currency_id int
	)
AS
BEGIN

	declare @this_id int

	set nocount on

	set @this_id = @ubm_currency_map_id

	if @this_id is null
	begin

	   select @this_id = ubm_currency_map_id
	     from ubm_currency_map
	    where ubm_id = @ubm_id
	      and ubm_currency_code = @ubm_currency_code
	      and currency_unit_id = @currency_id

	end

	if @this_id is null
	begin

		insert into ubm_currency_map
			( ubm_id
			, ubm_currency_code
			, currency_unit_id
			)
		values
			( @ubm_id
			, @ubm_currency_code
			, @currency_id
			)

		set @this_id = @@IDENTITY


	end

	exec cbmsUbmCurrencyMap_Get @MyAccountId, @this_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsUbmCurrencyMap_Save] TO [CBMSApplication]
GO
