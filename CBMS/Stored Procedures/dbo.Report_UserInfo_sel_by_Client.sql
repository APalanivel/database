SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
	dbo.Report_UserInfo_sel_by_Client
	
DESCRIPTION:
	This report is to get All the User ID  by Client Filter
	
INPUT PARAMETERS:
Name			DataType		Default	Description
-------------------------------------------------------------------------------------------
@CLIENT_LIST	VARCHAR(MAX)
@User_LIST		VARCHAR(MAX)
@IS_history		INT
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------
Report_UserInfo_sel_by_Client '10092'
	
AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------
SSR			Sharad srivastava

MODIFICATIONS
Initials	Date		Modification
------------------------------------------------------------
 SSR	   07/28/2010	Created
*/
CREATE PROC [dbo].[Report_UserInfo_sel_by_Client]
    @CLIENT_LIST VARCHAR(MAX)
AS 
    BEGIN   
  
        SET NOCOUNT ON   
          
        SELECT
            ufo.USER_INFO_ID
          , ufo.FIRST_NAME + SPACE(1) + ufo.LAST_NAME + ' - ' + ' ( '
            + ufo.USERNAME + ' ) ' AS NAME
        FROM
            dbo.USER_INFO ufo
            JOIN dbo.ufn_split(@CLIENT_LIST, ',') ufn_User_tmp
                ON CAST(ufn_User_tmp.Segments AS INT)= ufo.CLIENT_ID
        GROUP BY
            ufo.USER_INFO_ID
          , ufo.USERNAME
          , ufo.FIRST_NAME
          , ufo.LAST_NAME
        ORDER BY
            NAME    
                                         
  
    END


GO
GRANT EXECUTE ON  [dbo].[Report_UserInfo_sel_by_Client] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_UserInfo_sel_by_Client] TO [CBMSApplication]
GO
