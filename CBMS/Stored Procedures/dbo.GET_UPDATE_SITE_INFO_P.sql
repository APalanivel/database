
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******  
NAME: dbo.GET_UPDATE_SITE_INFO_P  
  
  
DESCRIPTION: Updates information for a divsion  
  
INPUT PARAMETERS:      
      Name                             DataType          Default     Description      
---------------------------------------------------------------------------------      
@site_type_id        INTEGER  
@site_name         VARCHAR(200)  
@division_id        INTEGER  
@ubmsite_id        VARCHAR(30)  
@site_reference_number      VARCHAR(30)  
@site_product_service      VARCHAR(2000)  
@production_schedule      VARCHAR(2000)  
@shutdown_schedule       VARCHAR(2000)  
@is_alternate_power      BIT  
@is_alternate_gas       BIT  
@doing_business_as       VARCHAR(200)  
@naics_code        VARCHAR(30)  
@tax_number        VARCHAR(30)  
@duns_number        VARCHAR(30)  
@closed         BIT  
@closed_date        DATETIME  
@not_managed        BIT  
@closed_by_id        INTEGER  
@user_info_id        INTEGER  
@contracting_entity      VARCHAR(200)  
@client_legal_structure    VARCHAR(4000)  
@firstName         VARCHAR(50)  
@lastName         VARCHAR(50)  
@contactEmailId       VARCHAR(50)  
@emailCC         VARCHAR(400)  
@emailApproval        BIT  
@dbViewApproval       BIT  
@site_id         INTEGER  
@Analyst_Mapping_Cd     INT   
@Is_Analyst_Mapping_Cd_Changed  BIT  
                             
                             
OUTPUT PARAMETERS:           
      Name              DataType          Default     Description      
------------------------------------------------------------      
  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
exec DBO.GET_UPDATE_SITE_INFO_P
@site_type_id = 250,  
@site_name = 'Westborough, MA',  
@division_id =62 ,  
@ubmsite_id ='' ,  
@site_reference_number ='' ,  
@site_product_service ='',  
@production_schedule ='',  
@shutdown_schedule ='',   
@is_alternate_power =0,  
@is_alternate_gas =0,  
@doing_business_as ='ABB Power T&D Co',  -- Original value  'ABB Power T&D Co'  
@naics_code ='',  
@tax_number ='750948250',   
@duns_number ='008012148',  
@closed =1,  
@closed_date ='2007-03-25 00:00:00.000',  
@not_managed =0,  
@closed_by_id =55,  
@user_info_id =55,  
@contracting_entity ='AZZ incorporated',  
@client_legal_structure ='AZZ incorporated (AZZ) is publicly traded (NYSE: AZZ).  The company has two business segments: electrical and industrial products and galvanizing services.  AZZ?s headquarters are located in Fort Worth, TX, and consolidated finan
cial statements are available at www.azzincorporated.com.',  
@firstName ='Dana',  
@lastName ='Perry',  
@contactEmailId ='jscott@summitenergy.com',  
@emailCC ='jscott@summitenergy.com',  
@emailApproval =1,  
@dbViewApproval =0,  
@site_id = 29,  
@Analyst_Mapping_Cd = NULL,  
@Is_Analyst_Mapping_Cd_Changed = 0  
  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
DR  Deana Ritter  
AKR     Ashok Kumar Raju  
  
MODIFICATIONS  
  
Initials Date  Modification  
------------------------------------------------------------  
DR     08/04/2009    Removed Linked Server Updates  
DMR    09/10/2010    Modified for Quoted_Identifier  
AKR    2012-09-21      Modified the Code to update Analyst Mapping code in Site and Account Table  
  
******/  
CREATE PROCEDURE DBO.GET_UPDATE_SITE_INFO_P  
      (   
       @site_type_id INTEGER  
      ,@site_name VARCHAR(200)  
      ,@division_id INTEGER  
      ,@ubmsite_id VARCHAR(30)  
      ,@site_reference_number VARCHAR(30)  
      ,@site_product_service VARCHAR(2000)  
      ,@production_schedule VARCHAR(2000)  
      ,@shutdown_schedule VARCHAR(2000)  
      ,@is_alternate_power BIT  
      ,@is_alternate_gas BIT  
      ,@doing_business_as VARCHAR(200)  
      ,@naics_code VARCHAR(30)  
      ,@tax_number VARCHAR(30)  
      ,@duns_number VARCHAR(30)  
      ,@closed BIT  
      ,@closed_date DATETIME  
      ,@not_managed BIT  
      ,@closed_by_id INTEGER  
      ,@user_info_id INTEGER  
      ,@contracting_entity VARCHAR(200)  
      ,@client_legal_structure VARCHAR(4000)  
      ,@firstName VARCHAR(50)  
      ,@lastName VARCHAR(50)  
      ,@contactEmailId VARCHAR(50)  
      ,@emailCC VARCHAR(400)  
      ,@emailApproval BIT  
      ,@dbViewApproval BIT  
      ,@site_id INTEGER  
      ,@Analyst_Mapping_Cd INT  
      ,@Is_Analyst_Mapping_Cd_Changed BIT )  
AS   
BEGIN
SET NOCOUNT ON  
  
  
EXEC cbmsSite_UpdateParticipation   
      93  
     ,@site_id  
     ,@closed  
     ,@closed_date  
     ,@not_managed   
   
  
UPDATE  
      SITE  
SET     
      SITE_TYPE_ID = @site_type_id  
     ,SITE_NAME = @site_name  
     ,DIVISION_ID = @division_id  
     ,UBMSITE_ID = @ubmsite_id  
     ,SITE_REFERENCE_NUMBER = @site_reference_number  
     ,SITE_PRODUCT_SERVICE = @site_product_service  
     ,PRODUCTION_SCHEDULE = @production_schedule  
     ,SHUTDOWN_SCHEDULE = @shutdown_schedule  
     ,IS_ALTERNATE_POWER = @is_alternate_power  
     ,IS_ALTERNATE_GAS = @is_alternate_gas  
     ,DOING_BUSINESS_AS = @doing_business_as  
     ,NAICS_CODE = @naics_code  
     ,TAX_NUMBER = @tax_number  
     ,DUNS_NUMBER = @duns_number  
     ,CLOSED = @closed  
     ,CLOSED_DATE = @closed_date  
     ,NOT_MANAGED = @not_managed  
     ,CLOSED_BY_ID = @closed_by_id  
     ,USER_INFO_ID = @user_info_id  
     ,CONTRACTING_ENTITY = @contracting_entity  
     ,CLIENT_LEGAL_STRUCTURE = @client_legal_structure  
     ,LP_CONTACT_FIRST_NAME = @firstName  
     ,LP_CONTACT_LAST_NAME = @lastName  
     ,LP_CONTACT_EMAIL_ADDRESS = @contactEmailId  
     ,LP_CONTACT_CC = @emailCC  
     ,IS_PREFERENCE_BY_EMAIL = @emailApproval  
     ,IS_PREFERENCE_BY_DV = @dbViewApproval  
     ,Analyst_Mapping_Cd = @Analyst_Mapping_Cd  
WHERE  
      site_id = @site_id  
     
UPDATE  
      acc  
SET     
      acc.Analyst_Mapping_Cd = NULL  
FROM  
      dbo.ACCOUNT acc  
WHERE  
      acc.Site_Id = @site_id  
      AND @Is_Analyst_Mapping_Cd_Changed = 1  
      AND acc.Analyst_Mapping_Cd IS NOT NULL  
  
UPDATE  
      USER_INFO  
SET     
      DIVISION_ID = @division_id  
WHERE  
      site_id = @site_id  
      
END
;
GO

GRANT EXECUTE ON  [dbo].[GET_UPDATE_SITE_INFO_P] TO [CBMSApplication]
GO
