SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.VC_Extraction_Service_Variance_Consumption_Level_By_Commodity_Id      
              
Description:              
			
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
  
                 
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
---------------------------------------------------------------------------------------- 
EXEC VC_Extraction_Service_Variance_Consumption_Level_By_Commodity_Id 

    
      
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
	RKV             2020-01-09      Created for AVT.       
             
******/
CREATE PROCEDURE [dbo].[VC_Extraction_Service_Variance_Consumption_Level_By_Commodity_Id]
     (
         @Commodity_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            VCL.Variance_Consumption_Level_Id
            , VCL.Consumption_Level_Desc
        FROM
            Variance_Consumption_Extraction_Service_Param_Value VCESPV
            INNER JOIN Variance_Consumption_Level VCL
                ON VCESPV.Variance_Consumption_Level_Id = VCL.Variance_Consumption_Level_Id
            INNER JOIN Commodity C
                ON VCL.Commodity_Id = C.Commodity_Id
        WHERE
            C.Commodity_Id = @Commodity_Id
        GROUP BY
            VCL.Variance_Consumption_Level_Id
            , VCL.Consumption_Level_Desc
        ORDER BY
            VCL.Variance_Consumption_Level_Id;
    END;
GO
GRANT EXECUTE ON  [dbo].[VC_Extraction_Service_Variance_Consumption_Level_By_Commodity_Id] TO [CBMSApplication]
GO
