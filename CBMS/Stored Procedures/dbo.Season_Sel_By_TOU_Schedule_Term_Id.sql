SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********
NAME:  dbo.Season_Sel_By_TOU_Schedule_Term_Id

DESCRIPTION:  

Used to select Season details by given Time_Of_Use_Schedule_Term_Id
Returns a flag if a peak is configured with each season

INPUT PARAMETERS:
      Name								DataType          Default     Description
------------------------------------------------------------
	@Time_Of_Use_Schedule_Term_Id		INT
				

OUTPUT PARAMETERS:
      Name								DataType          Default     Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------

	Exec dbo.Season_Sel_By_TOU_Schedule_Term_Id 9


AUTHOR INITIALS:
	Initials Name
------------------------------------------------------------
	BCH		Balaraju


	Initials Date		Modification
------------------------------------------------------------
	BCH		2012-07-12  Created

******/
CREATE PROCEDURE dbo.Season_Sel_By_TOU_Schedule_Term_Id
( 
 @Time_Of_Use_Schedule_Term_Id INT )
AS 
BEGIN
      SET NOCOUNT ON ;

      SELECT
            ssn.SEASON_ID
           ,ssn.SEASON_NAME
           ,ssn.SEASON_FROM_DATE
           ,ssn.SEASON_TO_DATE
           ,CASE WHEN EXISTS ( SELECT
                                    1
                               FROM
                                    dbo.Time_of_Use_Schedule_Term_Peak TOUSTP
                               WHERE
                                    ssn.SEASON_ID = TOUSTP.SEASON_ID ) THEN 1
                 ELSE 0
            END AS Peak_Exists
      FROM
            dbo.SEASON ssn
            JOIN dbo.ENTITY ent
                  ON ent.ENTITY_ID = ssn.SEASON_PARENT_TYPE_ID
      WHERE
            ssn.SEASON_PARENT_ID = @Time_Of_Use_Schedule_Term_Id
            AND ent.ENTITY_NAME = 'Time Of Use Schedule Term'
            AND ent.ENTITY_TYPE = 120
END
;
GO
GRANT EXECUTE ON  [dbo].[Season_Sel_By_TOU_Schedule_Term_Id] TO [CBMSApplication]
GO
