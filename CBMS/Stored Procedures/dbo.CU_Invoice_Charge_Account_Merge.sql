SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********       
NAME:  dbo.CU_Invoice_Charge_Account_Merge 
     
DESCRIPTION:  This will insert data into CU_Invoice_Charge_Account table
    
INPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
	@Account_Id				int
	@Charge_Expression		varchar
	@Charge_Value			varchar
	@CU_Invoice_Charge_Id	int     
        
        
OUTPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:   
    
 EXEC CU_Invoice_Charge_Account_Merge 31662,'A2*0.3',123,5568853    
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
NK   Nageswara Rao Kosuri             
    
    
Initials Date  Modification      
------------------------------------------------------------      
NK		 12/24/2009  Created    
NK		 01/18/2010  Modified to add Update statement    
SKA	     03/30/2010	 Use Merge instead of Update & Insert statement			
******/      

CREATE PROCEDURE dbo.CU_Invoice_Charge_Account_Merge
      @Account_Id AS INT
     ,@Charge_Expression AS VARCHAR(200) = NULL
     ,@Charge_Value AS DECIMAL(16, 4) = NULL
     ,@CU_Invoice_Charge_Id AS INT
AS 
BEGIN
      SET  NOCOUNT ON
    
      MERGE INTO CU_Invoice_Charge_Account As tgt USING ( SELECT
                                                            @CU_Invoice_Charge_Id AS CU_INVOICE_CHARGE_ID
                                                           ,@Account_Id AS Account_Id
                                                           ,@Charge_Value AS Charge_Value
                                                           ,@Charge_Expression AS Charge_Expression ) AS src 
    ON ( tgt.CU_INVOICE_CHARGE_ID = src.CU_INVOICE_CHARGE_ID )
            AND ( tgt.ACCOUNT_ID = src.ACCOUNT_ID ) WHEN MATCHED THEN UPDATE
      SET   
            Charge_Expression = src.Charge_Expression
           ,Charge_Value = src.Charge_Value WHEN NOT MATCHED THEN INSERT ( ACCOUNT_ID, Charge_Expression, Charge_Value, CU_INVOICE_CHARGE_ID )
      VALUES
            (
             src.ACCOUNT_ID
            ,src.Charge_Expression
            ,src.Charge_Value
            ,src.CU_INVOICE_CHARGE_ID ) ;
				  
END
GO
GRANT EXECUTE ON  [dbo].[CU_Invoice_Charge_Account_Merge] TO [CBMSApplication]
GO
