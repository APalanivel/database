SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE dbo.GET_BASELOAD_DETAILS_P
	@contractID INT,
	@entityType INT,
	@entityName VARCHAR(200),
	@baseloadID INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT 	lps.load_profile_specification_id,
		lps.price_index_id,
		deal.entity_id,
		deal.ENTITY_NAME deal_type,
		price.pricing_point,
		lps.contract_id,
		lps.lps_type_id, 
		typ.entity_name lps_type,
		lps.month_identifier,
		lps.lps_fixed_price, 
		lps.lps_unit_type_id,
		unit.ENTITY_NAME unit,
		lps.lps_frequency_type_id,
		FREQ.ENTITY_NAME FREQUENCY,
		lps.lps_expression,
		bld.baseload_details_id
		, bld.baseload_volume
		, bld.baseload_low_tolerance
		, bld.baseload_high_tolerance
	FROM dbo.load_profile_specification lps INNER JOIN dbo.entity typ ON typ.entity_id = lps.lps_type_id
		INNER JOIN dbo.price_index price ON price.price_index_id = lps.price_index_id
		INNER JOIN dbo.entity deal ON deal.entity_id = price.index_id
		INNER JOIN dbo.ENTITY FREQ ON FREQ.ENTITY_ID = lps.LPS_FREQUENCY_TYPE_ID
		INNER JOIN dbo.ENTITY unit ON UNIT.ENTITY_ID = lps.LPS_UNIT_TYPE_ID
		INNER JOIN dbo.baseload_details bld ON bld.load_profile_specification_id = lps.load_profile_specification_id			
		INNER JOIN baseload bl ON  bld.baseload_id = bl.baseload_id
	WHERE typ.entity_type = @entityType
		AND typ.entity_name = @entityName
		AND lps.contract_id = @contractID
		AND bld.baseload_id = @baseloadID

END
GO
GRANT EXECUTE ON  [dbo].[GET_BASELOAD_DETAILS_P] TO [CBMSApplication]
GO
