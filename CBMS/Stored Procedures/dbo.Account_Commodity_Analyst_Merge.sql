SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********         
NAME:  dbo.Account_Commodity_Analyst_Merge        
       
DESCRIPTION:   
  
Used to Insert/update the data into dbo.Account_Commodity_Analyst_Merge table.  
      
INPUT PARAMETERS:          
      Name     DataType          Default     Description          
------------------------------------------------------------          
@Account_Commodity_Analyst Account_Commodity_Analyst_Type  
          
          
OUTPUT PARAMETERS:          
      Name              DataType          Default     Description          
------------------------------------------------------------          
          
USAGE EXAMPLES:     
  
BEGIN TRAN  
select * from Account_Commodity_Analyst where account_id=1606 and commodity_id=290  
DECLARE @Account_Commodity_Analyst AS Account_Commodity_Analyst_Type  
INSERT into @Account_Commodity_Analyst  
values(1606,290,NULL)  
EXEC DBO.Account_Commodity_Analyst_Merge @Account_Commodity_Analyst  
select * from Account_Commodity_Analyst where account_id=1606 and commodity_id=290  
ROLLBACK TRAN  
  
  
SELECT top 10  account_id,commodity_id FROM  core.client_hier_account   
WHERE account_not_managed=0 and commodity_id=291 group by account_id,commodity_id order by account_id desc  
  
Select * from Account_Commodity_Analyst  where Account_Id = 1606 and Commodity_Id =290
------------------------------------------------------------    
      
AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
BCH   Balaraju  Chalumuri  
      
      
Initials Date   Modification        
------------------------------------------------------------        
BCH   2012-09-17  Created new sp (for POCO Project)  
  
******/  
CREATE PROCEDURE dbo.Account_Commodity_Analyst_Merge
      ( 
       @Account_Commodity_Analyst AS Account_Commodity_Analyst_Type READONLY )
AS 
BEGIN  
      SET NOCOUNT ON ;  
        
      MERGE dbo.Account_Commodity_Analyst AS Tgt
            USING 
                  ( SELECT
                        aca.Account_Id AS Account_Id
                       ,aca.Commodity_Id AS Commodity_Id
                       ,aca.Analyst_User_Info_Id AS Analyst_User_Info_Id
                    FROM
                        @Account_Commodity_Analyst aca ) AS Src
            ON Tgt.Account_Id = Src.Account_Id
                  AND Tgt.Commodity_Id = Src.Commodity_Id
            WHEN MATCHED AND Src.Analyst_User_Info_Id IS NOT NULL
                  THEN   
                  UPDATE
                    SET 
                        Tgt.Analyst_User_Info_Id = Src.Analyst_User_Info_Id
            WHEN MATCHED AND Src.Analyst_User_Info_Id IS NULL
                  THEN 
                     DELETE
            WHEN NOT MATCHED AND Src.Analyst_User_Info_Id IS NOT NULL
                  THEN   
                       INSERT
                        ( 
                         Account_Id
                        ,Commodity_Id
                        ,Analyst_User_Info_Id )
                    VALUES
                        ( 
                         Src.Account_Id
                        ,Src.Commodity_Id
                        ,Src.Analyst_User_Info_Id ) ;  
                    
END  

;
GO
GRANT EXECUTE ON  [dbo].[Account_Commodity_Analyst_Merge] TO [CBMSApplication]
GO
