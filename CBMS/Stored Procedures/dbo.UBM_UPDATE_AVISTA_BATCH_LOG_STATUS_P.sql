SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE   PROCEDURE [dbo].[UBM_UPDATE_AVISTA_BATCH_LOG_STATUS_P]
   @consolidation_id varchar(50)
 , @client_id int
 , @batch_master_log_id int
 , @status varchar(500)
AS 
   set nocount on	
   declare @client_name varchar(100)

   select   @client_name = client_name
   from     ubm_avista_control
   where    UBM_BATCH_MASTER_LOG_ID = @batch_master_log_id
            and client_id = @client_id
            and consolidation_id = @consolidation_id


   update   UBM_BATCH_LOG
   set      FAILURE_REASON = @status
   where    UBM_BATCH_MASTER_LOG_ID = @batch_master_log_id
            and CONSOLIDATION_ID = @consolidation_id
            and client_name = @client_name
GO
GRANT EXECUTE ON  [dbo].[UBM_UPDATE_AVISTA_BATCH_LOG_STATUS_P] TO [CBMSApplication]
GO
