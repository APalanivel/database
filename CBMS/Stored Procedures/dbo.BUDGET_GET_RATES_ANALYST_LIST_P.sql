
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.BUDGET_GET_RATES_ANALYST_LIST_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	RR			2017-06-01	SE2017-26 - Modified script to include users with "Budget Reviewer" group
******/

CREATE         PROCEDURE [dbo].[BUDGET_GET_RATES_ANALYST_LIST_P]
      ( 
       @userId VARCHAR(10)
      ,@sessionId VARCHAR(20) )
AS 
BEGIN
      SET NOCOUNT ON;
      
      SELECT
            userInfo.user_info_id
           ,userInfo.FIRST_NAME + ' ' + userInfo.LAST_NAME USER_INFO_NAME
      FROM
            user_info userInfo
            INNER JOIN user_info_group_info_map map
                  ON userInfo.USER_INFO_ID = map.USER_INFO_ID
            INNER JOIN group_info groupInfo
                  ON map.GROUP_INFO_ID = groupInfo.GROUP_INFO_ID
      WHERE
            groupInfo.group_name IN ( 'operations', 'Budget Reviewer' )
      GROUP BY
            userInfo.user_info_id
           ,userInfo.FIRST_NAME + ' ' + userInfo.LAST_NAME
      ORDER BY
            userInfo.FIRST_NAME + ' ' + userInfo.LAST_NAME
END

;
GO

GRANT EXECUTE ON  [dbo].[BUDGET_GET_RATES_ANALYST_LIST_P] TO [CBMSApplication]
GO
