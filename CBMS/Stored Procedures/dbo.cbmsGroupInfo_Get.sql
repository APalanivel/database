SET NUMERIC_ROUNDABORT OFF 
GO

SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/******

NAME: dbo.cbmsGroupInfo_Get
  
DESCRIPTION:   
  
INPUT PARAMETERS:      
 Name			DataType			Default     Description      
------------------------------------------------------------------      
 @group_info_id INT

OUTPUT PARAMETERS:
Name            DataType          Default     Description      
------------------------------------------------------------      
  
USAGE EXAMPLES:  
------------------------------------------------------------  

	EXEC dbo.cbmsGroupInfo_Get 138

	EXEC dbo.cbmsGroupInfo_Get 1
	
	SELECT * FROM Group_Info
  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
PNR		 Pandarinath
RR		 Raghu Reddy
HG		 Harihara Suthan G
SP       Sandeep Pigilam          

  
MODIFICATIONS :
Initials	Date		  Modification
------------------------------------------------------------
PNR			04/12/2011	  Added Group_Info_Category_Id column to the select statement as per the DV group revamp requirement
							Removed unused parameter @MyAccountId parameter
RR			2013-06-06	  ENHANCE-60 Added Is_Fee_Module column in select list
HG			2014-02-04	  Is_Required_For_RA_Login,App_Acces_Role_Type_Cd columns added as output as per RA UM enhancement
SP          2014-08-26    For Data Transition,Added new column Is_Shown_On_Watch_List in Select list.

******/

CREATE PROCEDURE [dbo].[cbmsGroupInfo_Get] ( @group_info_id INT )
AS 
BEGIN

      SET NOCOUNT ON;

      SELECT
            gi.group_info_id
           ,gi.queue_id
           ,gi.group_name
           ,gi.group_description
           ,gi.Group_Info_Category_Id
           ,gic.Category_Name
           ,gic.APP_MENU_PROFILE_ID
           ,amp.APP_MENU_PROFILE_NAME
           ,gi.Is_Fee_Module
           ,gi.Is_Required_For_RA_Login
           ,rt.App_Access_Role_Type_Cd
           ,gi.Is_Shown_On_Watch_List
      FROM
            dbo.group_info gi
            INNER JOIN dbo.Group_Info_Category gic
                  ON gic.Group_Info_Category_Id = gi.Group_Info_Category_Id
            INNER JOIN dbo.App_Menu_Profile amp
                  ON amp.APP_MENU_PROFILE_ID = gic.App_Menu_Profile_Id
            LEFT OUTER JOIN dbo.Group_Info_App_Access_Role_Type_Cd rt
                  ON rt.Group_Info_Id = gi.Group_Info_Id
      WHERE
            gi.group_info_id = @group_info_id  

END;

;

;
GO




GRANT EXECUTE ON  [dbo].[cbmsGroupInfo_Get] TO [CBMSApplication]
GO
GO