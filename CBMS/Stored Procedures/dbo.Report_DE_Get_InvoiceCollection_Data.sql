SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******

NAME: [dbo].[Report_DE_Get_InvoiceCollection_Data]

DESCRIPTION:
		this procedure is used to get InvoiceCollection module data for all clients

INPUT PARAMETERS:
	NAME							DATATYPE	DEFAULT		DESCRIPTION
-----------------------------------------------------------------------

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION
-----------------------------------------------------------------------

USAGE EXAMPLES:
-----------------------------------------------------------------------
	
	--NOTE: this procedure only runs on ReadOnly Copy server because its using DVDEHUB db tables
	use cbms exec Report_DE_Get_InvoiceCollection_Data

AUTHOR INITIALS:
	INITIALS	NAME
-----------------------------------------------------------------------
	KVK			Vinay K
	RKV         Ravi Kumar Vegesna
	NR			Narayana Reddy

MODIFICATIONS:
	INITIALS	DATE			MODIFICATION
-----------------------------------------------------------------------
	KVK			09/10/2018		created, REPTMGR-94
	RKV         2020-04-24      PRSUPPORT-3579, Added Enroll_In_Bot to the result set
	NR			2020-06-08		SE2017-963 BOT Process - Retuen Enroll_In_Bot flag from new table.            

*/
CREATE PROCEDURE [dbo].[Report_DE_Get_InvoiceCollection_Data]
AS
    BEGIN
        SET NOCOUNT ON;
        WITH IC_SetUp_CTE
        AS (
               SELECT   DISTINCT
                        cat.Account_Id
                        , cat.Last_Change_Ts [Date IC Collection Turned On]
                        , ui.FIRST_NAME + ' ' + ui.LAST_NAME [IC Collection Turned On By]
                        , cat.Attribute_Value Value
               FROM
                    DVDEHub.dbo.Client_Hier_Account_Attribute_Tracking cat
                    INNER JOIN DVDEHub.Core.Client_Attribute ca
                        ON ca.Client_Attribute_id = cat.Client_Attribute_Id
                    LEFT JOIN dbo.USER_INFO ui
                        ON ui.USER_INFO_ID = cat.Updated_User_Id
               WHERE
                    ca.Attribute_Name = 'Invoice_Collection_Is_Chased'
                    AND ca.Is_Active = 1
           )
             , Recent_Vendor_CTE
        AS (
               SELECT
                    cbv.Account_Consolidated_Billing_Vendor_Id
                    , CASE WHEN iv.ENTITY_NAME = 'Utility' THEN cha.Account_Vendor_Name
                          WHEN iv.ENTITY_NAME = 'Supplier' THEN ISNULL(ven.VENDOR_NAME, 'Contracted Vendor')
                      END AS Vendor_Name
                    , cbv.Billing_Start_Dt
                    , BillingEndDt = CASE WHEN cbv.Billing_End_Dt IS NULL THEN 'Unspecified'
                                         ELSE CAST(cbv.Billing_End_Dt AS VARCHAR(20))
                                     END
                    , 0 AS Recent_By
                    , cha.Account_Id
               FROM
                    dbo.Account_Consolidated_Billing_Vendor cbv
                    INNER JOIN Core.Client_Hier_Account cha
                        ON cbv.Account_Id = cha.Account_Id
                    INNER JOIN dbo.ENTITY iv
                        ON cbv.Invoice_Vendor_Type_Id = iv.ENTITY_ID
                    LEFT JOIN dbo.VENDOR ven
                        ON cbv.Supplier_Vendor_Id = ven.VENDOR_ID
                    INNER JOIN dbo.USER_INFO ui
                        ON cbv.Updated_User_Id = ui.USER_INFO_ID
               WHERE
                    --cbv.Account_Id = 1138841
                    --AND 
                    cha.Account_Type = 'Utility'
               --AND ( ( cbv.Billing_End_Dt IS NOT NULL
               --        AND @Current_Date BETWEEN cbv.Billing_Start_Dt
               --                          AND     cbv.Billing_End_Dt )
               --      OR ( cbv.Billing_End_Dt IS NULL
               --           AND @Current_Date >= cbv.Billing_Start_Dt ) )
               GROUP BY
                   cbv.Account_Consolidated_Billing_Vendor_Id
                   , CASE WHEN iv.ENTITY_NAME = 'Utility' THEN cha.Account_Vendor_Name
                         WHEN iv.ENTITY_NAME = 'Supplier' THEN ISNULL(ven.VENDOR_NAME, 'Contracted Vendor')
                     END
                   , cbv.Billing_Start_Dt
                   , cbv.Billing_End_Dt
                   , cha.Account_Id
           )
             , Client_Invoice_Collection_Setup_CTE
        AS (
               SELECT   DISTINCT
                        iccc.Client_Id
                        , iccc.Invoice_Collection_Service_Start_Dt
                        , iccc.Invoice_Collection_Service_End_Dt
               FROM
                    dbo.Invoice_Collection_Client_Config iccc
               WHERE
                    iccc.Is_Active = 1
           )
                --Invoice frequency
             , IC_Frequency_CTE
        AS (
               SELECT   DISTINCT
                        icac.Invoice_Collection_Account_Config_Id
                        , icac.Account_Id AID
                        , aicf.Is_Primary [Primary]
                        , aicf.Seq_No Sequence
                        , IFc.Code_Value [Invoice Frequency]
                        , ifp.Code_Value [Frequency Pattern]
                        , aicf.Expected_Invoice_Raised_Day [Expected vendor invoice issued day]
                        , aicf.Expected_Invoice_Raised_Month [Expected vendor invoice issued month]
                        , aicf.Expected_Invoice_Raised_Day_Lag [Expected vendor invoice issued lag]
                        , aicf.Expected_Invoice_Received_Day [Expected invoice Received day]
                        , aicf.Expected_Invoice_Received_Month [Expected invoice Received month]
                        , aicf.Expected_Invoice_Received_Day_Lag [Expected invoice Received lag]
                        , cha.Display_Account_Number
                        , ch.Client_Name
                        , ch.Site_name
               FROM
                    dbo.Invoice_Collection_Account_Config icac
                    JOIN dbo.Account_Invoice_Collection_Frequency aicf
                        ON aicf.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                    INNER JOIN dbo.Code IFc
                        ON IFc.Code_Id = aicf.Invoice_Frequency_Cd -- not null
                    LEFT OUTER JOIN dbo.Code ifp
                        ON ifp.Code_Id = aicf.Invoice_Frequency_Pattern_Cd
                    JOIN Core.Client_Hier_Account cha
                        ON cha.Account_Id = icac.Account_Id
                    JOIN Core.Client_Hier ch
                        ON ch.Client_Hier_Id = cha.Client_Hier_Id
           --WHERE icac.Invoice_Collection_Account_Config_Id = 21104
           )
             , PrimaryInvoiceCollectionSource_CTE
        AS (
               SELECT   DISTINCT
                        icac.Invoice_Collection_Account_Config_Id
                        , [Invoice Collection Source] = CASE WHEN icsource.Code_Value = 'ubm' THEN 'UBM'
                                                            ELSE icsource.Code_Value + ' | ' + invsourcetyp.Code_Value
                                                        END
                        , u.UBM_NAME UBM
                        , uc.Code_Value [UBM Type]
                        , aicd.URL
                        , aicd.Login_Name Login
                        , ibpicacm.Enroll_In_Bot
               FROM
                    dbo.Invoice_Collection_Account_Config icac
                    JOIN dbo.Account_Invoice_Collection_Source aics
                        ON aics.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                    JOIN dbo.Code icsource
                        ON icsource.Code_Id = aics.Invoice_Collection_Source_Cd
                    JOIN dbo.Code invsourcetyp
                        ON invsourcetyp.Code_Id = aics.Invoice_Source_Type_Cd
                    LEFT OUTER JOIN dbo.Account_Invoice_Collection_Online_Source_Dtl aicd
                        ON aicd.Account_Invoice_Collection_Source_Id = aics.Account_Invoice_Collection_Source_Id
                    LEFT OUTER JOIN dbo.Account_Invoice_Collection_UBM_Source_Dtl aicu
                        ON aicu.Account_Invoice_Collection_Source_Id = aics.Account_Invoice_Collection_Source_Id
                    LEFT OUTER JOIN dbo.Account_Invoice_Collection_Data_Feed_Etl_Image_Source_Dtl aicdfeisd
                        ON aicdfeisd.Account_Invoice_Collection_Source_Id = aics.Account_Invoice_Collection_Source_Id
                    LEFT OUTER JOIN dbo.UBM u
                        ON u.UBM_ID = aicu.UBM_Id
                    LEFT OUTER JOIN dbo.Code uc
                        ON uc.Code_Id = aicu.UBM_Type_Cd
                    LEFT JOIN dbo.IC_Bots_Process_Invoice_Collection_Account_Config_Map ibpicacm
                        ON ibpicacm.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
               WHERE
                    aics.Is_Primary = 1
           )
             , Contacts_CTE
        AS (
               SELECT   DISTINCT
                        icaco.Invoice_Collection_Account_Config_Id
                        , CAST(ci.First_Name AS VARCHAR(200)) + ' ' + CAST(ci.Last_Name AS VARCHAR(200)) + ' | '
                          + ISNULL(ci.Email_Address, '') + ' | ' + ISNULL(ci.Phone_Number, '') Contact
                        , cc.Code_Value [Contact Type]
                        , [Primary] = CASE WHEN icaco.Is_Primary = 1 THEN 'Yes'
                                          ELSE 'No'
                                      END
                        , vtp.ENTITY_NAME [Chased Vendor Type]
                        , v.VENDOR_NAME [Chased Vendor]
               FROM
                    dbo.Invoice_Collection_Account_Contact icaco
                    INNER JOIN dbo.Contact_Info ci
                        ON ci.Contact_Info_Id = icaco.Contact_Info_Id
                    INNER JOIN dbo.Code cc
                        ON cc.Code_Id = ci.Contact_Level_Cd
                    INNER JOIN dbo.Invoice_Collection_Account_Config icac
                        ON icac.Invoice_Collection_Account_Config_Id = icaco.Invoice_Collection_Account_Config_Id
                    LEFT OUTER JOIN dbo.Vendor_Contact_Map vcm
                        ON vcm.Contact_Info_Id = ci.Contact_Info_Id
                    LEFT OUTER JOIN dbo.VENDOR v
                        ON v.VENDOR_ID = vcm.VENDOR_ID
                    LEFT OUTER JOIN dbo.ENTITY vtp
                        ON vtp.ENTITY_ID = v.VENDOR_TYPE_ID
               WHERE
                    icaco.Is_Primary = 1
           --and icac.account_id = 1247854
           --AND icaco.Invoice_Collection_Account_Config_Id = 21104
           )
        SELECT  DISTINCT
                ch.Client_Name Client
                , [Client Invoice Collection Setup] = CAST(DAY(cic.Invoice_Collection_Service_Start_Dt) AS VARCHAR(2))
                                                      + '-'
                                                      + LEFT(DATENAME(MONTH, (cic.Invoice_Collection_Service_Start_Dt)), 3)
                                                      + '-'
                                                      + CAST(YEAR(cic.Invoice_Collection_Service_Start_Dt) AS VARCHAR(4))
                                                      + ' to '
                                                      + CAST(DAY(cic.Invoice_Collection_Service_End_Dt) AS VARCHAR(2))
                                                      + '-'
                                                      + LEFT(DATENAME(MONTH, (cic.Invoice_Collection_Service_End_Dt)), 3)
                                                      + '-'
                                                      + CAST(YEAR(cic.Invoice_Collection_Service_End_Dt) AS VARCHAR(4))
                , ch.Site_name Site
                , ch.City City
                , ch.State_Name State
                , ch.Country_Name Country
                , [IC Turned On?] = CASE WHEN isc.Account_Id IS NULL THEN NULL
                                        WHEN isc.Account_Id IS NOT NULL
                                             AND isc.Value = 0 THEN 'No'
                                        WHEN isc.Account_Id IS NOT NULL
                                             AND isc.Value = 1 THEN 'Yes'
                                        ELSE 'Not Sure'
                                    END
                , isc.[Date IC Collection Turned On] [Date IC Turned On]
                , isc.[IC Collection Turned On By] [Name of person who turned on IC]
                , [IC Active] = CASE WHEN iac.Is_Chase_Activated = '1' THEN 'Yes'
                                    ELSE 'No'
                                END
                , iac.Chase_Status_Lat_Updated_Ts [Date IC Activated]
                , csuid.FIRST_NAME + ' ' + csuid.LAST_NAME [Name of person who activated IC]
                , iac.Invoice_Collection_Alternative_Account_Number [IC Alternative Account Number]
                , cha.Display_Account_Number [Account Number]
                , cha.Account_Vendor_Name [Account Vendor]
                , cha.Account_Type [Account Type]
                , [Invoice Collection Start Dt] = CAST(DAY(iac.Invoice_Collection_Service_Start_Dt) AS VARCHAR(2)) + '-'
                                                  + LEFT(DATENAME(MONTH, (iac.Invoice_Collection_Service_Start_Dt)), 3)
                                                  + '-' + CAST(YEAR(iac.Invoice_Collection_Service_Start_Dt) AS VARCHAR(4))
                , [Invoice Collection End Dt] = CAST(DAY(iac.Invoice_Collection_Service_End_Dt) AS VARCHAR(2)) + '-'
                                                + LEFT(DATENAME(MONTH, (iac.Invoice_Collection_Service_End_Dt)), 3) + '-'
                                                + CAST(YEAR(iac.Invoice_Collection_Service_End_Dt) AS VARCHAR(4))
                , icownid.FIRST_NAME + ' ' + icownid.LAST_NAME [Invoice Collection Owner]
                , icoff.FIRST_NAME + ' ' + icoff.LAST_NAME [Invoice Collection Officer]
                , chspr.Code_Value [Chase Priority]
                , invpat.Code_Value [Invoice Pattern]
                , ISNULL(iac.Additional_Invoices_Per_Period, 0) [Number of additional invoices per period]
                --,[Consolidated Billing Posted To Utility] = CASE WHEN cha.Account_CONSOLIDATED_BILLING_POSTED_TO_UTILITY = '1' THEN 'Yes' ELSE 'No' END
                --,[Most Recent Invoice Vendor] = Vendor_name + ' '+ CAST (rc.Billing_Start_Dt AS VARCHAR(20))+ ' - '+ cast (rc.BillingEndDt AS VARCHAR(20))
                , [Primary Invoice Frequency] = ic.[Invoice Frequency]
                , [Primary Invoice Frequency Pattern] = ic.[Frequency Pattern]
                , [Primary Invoice Expected Issued Day] = ic.[Expected vendor invoice issued day]
                , [Primary Invoice Expected Issued Month] = ic.[Expected vendor invoice issued month]
                , [Primary Invoice Expected Issued Lag] = ic.[Expected vendor invoice issued lag]
                , [Primary Invoice Received Issued Day] = ic.[Expected invoice Received day]
                , [Primary Invoice Received Issued Month] = ic.[Expected invoice Received month]
                , [Primary Invoice Received Issued Lag] = ic.[Expected invoice Received lag]
                , pc.[Invoice Collection Source]
                , cc2.[Chased Vendor]
                , cc2.[Chased Vendor Type]
                , pc.UBM
                , pc.[UBM Type]
                , pc.URL
                , pc.Login
                , cc.Contact [Account contact name | email | phone#]
                , cc1.Contact [Client contact | email | phone#]
                , cc2.Contact [Vendor contact | email | phone#]
                , [Additional Invoice1 Frequency] = ic1.[Invoice Frequency]
                , [Additional Invoice1 Frequency Pattern] = ic1.[Frequency Pattern]
                , [Additional Invoice1 Expected Issued Day] = ic1.[Expected vendor invoice issued day]
                , [Additional Invoice1 Expected Issued Month] = ic1.[Expected vendor invoice issued month]
                , [Additional Invoice1 Expected Issued Lag] = ic1.[Expected vendor invoice issued lag]
                , [Additional Invoice1 Received Issued Day] = ic1.[Expected invoice Received day]
                , [Additional Invoice1 Received Issued Month] = ic1.[Expected invoice Received month]
                , [Additional Invoice1 Received Issued Lag] = ic1.[Expected invoice Received lag]
                , [Additional Invoice2 Frequency] = ic2.[Invoice Frequency]
                , [Additional Invoice2 Frequency Pattern] = ic2.[Frequency Pattern]
                , [Additional Invoice2 Expected Issued Day] = ic2.[Expected vendor invoice issued day]
                , [Additional Invoice2 Expected Issued Month] = ic2.[Expected vendor invoice issued month]
                , [Additional Invoice2 Expected Issued Lag] = ic2.[Expected vendor invoice issued lag]
                , [Additional Invoice2 Received Issued Day] = ic2.[Expected invoice Received day]
                , [Additional Invoice2 Received Issued Month] = ic2.[Expected invoice Received month]
                , [Additional Invoice2 Received Issued Lag] = ic2.[Expected invoice Received lag]
                , [Additional Invoice3 Frequency] = ic3.[Invoice Frequency]
                , [Additional Invoice3 Frequency Pattern] = ic3.[Frequency Pattern]
                , [Additional Invoice3 Expected Issued Day] = ic3.[Expected vendor invoice issued day]
                , [Additional Invoice3 Expected Issued Month] = ic3.[Expected vendor invoice issued month]
                , [Additional Invoice3 Expected Issued Lag] = ic3.[Expected vendor invoice issued lag]
                , [Additional Invoice3 Received Issued Day] = ic3.[Expected invoice Received day]
                , [Additional Invoice3 Received Issued Month] = ic3.[Expected invoice Received month]
                , [Additional Invoice3 Received Issued Lag] = ic3.[Expected invoice Received lag]
                , [Additional Invoice4 Frequency] = ic4.[Invoice Frequency]
                , [Additional Invoice4 Frequency Pattern] = ic4.[Frequency Pattern]
                , [Additional Invoice4 Expected Issued Day] = ic4.[Expected vendor invoice issued day]
                , [Additional Invoice4 Expected Issued Month] = ic4.[Expected vendor invoice issued month]
                , [Additional Invoice4 Expected Issued Lag] = ic4.[Expected vendor invoice issued lag]
                , [Additional Invoice4 Received Issued Day] = ic4.[Expected invoice Received day]
                , [Additional Invoice4 Received Issued Month] = ic4.[Expected invoice Received month]
                , [Additional Invoice4 Received Issued Lag] = ic4.[Expected invoice Received lag]
                , [Enroll In Bot] = CASE WHEN pc.Enroll_In_Bot = 1 THEN 'Yes'
                                        ELSE 'No'
                                    END
        FROM
            Core.Client_Hier_Account cha
            JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            JOIN dbo.Invoice_Collection_Account_Config iac
                ON iac.Account_Id = cha.Account_Id
            LEFT JOIN dbo.USER_INFO csuid
                ON csuid.USER_INFO_ID = iac.Chase_Status_Updated_User_Id
            LEFT JOIN dbo.USER_INFO icownid
                ON icownid.USER_INFO_ID = iac.Invoice_Collection_Owner_User_Id
            LEFT JOIN dbo.USER_INFO icoff
                ON icoff.USER_INFO_ID = iac.Invoice_Collection_Officer_User_Id
            LEFT JOIN dbo.Code chspr
                ON chspr.Code_Id = iac.Chase_Priority_Cd
            LEFT JOIN Client_Invoice_Collection_Setup_CTE cic
                ON cic.Client_Id = ch.Client_Id
            LEFT JOIN dbo.Code invpat
                ON invpat.Code_Id = iac.Invoice_Pattern_Cd
            LEFT JOIN Recent_Vendor_CTE rc
                ON rc.Account_Id = cha.Account_Id
            JOIN dbo.ENTITY cltyp
                ON cltyp.ENTITY_ID = ch.Client_Type_Id
            LEFT OUTER JOIN IC_Frequency_CTE ic
                ON ic.Invoice_Collection_Account_Config_Id = iac.Invoice_Collection_Account_Config_Id
                   AND  ic.[Primary] = 1
            LEFT OUTER JOIN IC_Frequency_CTE ic1
                ON ic1.Invoice_Collection_Account_Config_Id = iac.Invoice_Collection_Account_Config_Id
                   AND  ic1.[Primary] = 0
                   AND  ic1.Sequence = 1
            LEFT OUTER JOIN IC_Frequency_CTE ic2
                ON ic2.Invoice_Collection_Account_Config_Id = iac.Invoice_Collection_Account_Config_Id
                   AND  ic2.[Primary] = 0
                   AND  ic2.Sequence = 2
            LEFT OUTER JOIN IC_Frequency_CTE ic3
                ON ic3.Invoice_Collection_Account_Config_Id = iac.Invoice_Collection_Account_Config_Id
                   AND  ic3.[Primary] = 0
                   AND  ic3.Sequence = 3
            LEFT OUTER JOIN IC_Frequency_CTE ic4
                ON ic4.Invoice_Collection_Account_Config_Id = iac.Invoice_Collection_Account_Config_Id
                   AND  ic4.[Primary] = 0
                   AND  ic4.Sequence = 4
            LEFT OUTER JOIN PrimaryInvoiceCollectionSource_CTE pc
                ON pc.Invoice_Collection_Account_Config_Id = iac.Invoice_Collection_Account_Config_Id
            LEFT OUTER JOIN Contacts_CTE cc
                ON cc.Invoice_Collection_Account_Config_Id = iac.Invoice_Collection_Account_Config_Id
                   AND  cc.[Contact Type] = 'Account'
            LEFT OUTER JOIN Contacts_CTE cc1
                ON cc1.Invoice_Collection_Account_Config_Id = iac.Invoice_Collection_Account_Config_Id
                   AND  cc1.[Contact Type] = 'Client'
            LEFT OUTER JOIN Contacts_CTE cc2
                ON cc2.Invoice_Collection_Account_Config_Id = iac.Invoice_Collection_Account_Config_Id
                   AND  cc2.[Contact Type] = 'Vendor'
            LEFT OUTER JOIN IC_SetUp_CTE isc
                ON isc.Account_Id = cha.Account_Id
        WHERE
            cltyp.ENTITY_NAME != 'demo'
            AND ch.Client_Not_Managed = 0
            AND ch.Site_Not_Managed = 0
            AND cha.Account_Not_Managed = 0;
    --and cha.account_number = 'DE0003992355400000000000030025885'
    END;


GO
GRANT EXECUTE ON  [dbo].[Report_DE_Get_InvoiceCollection_Data] TO [CBMSApplication]
GO
