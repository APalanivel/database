SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: dbo.Division_History_Sel_By_Division_Id  
     
DESCRIPTION:

	To Select all the division history information for the given Division id.
	Instead of calling individual procedures application will call this procedure 
	to get all the informations required for the page at once.
	
	Order of the execution should not be changed as application logic is based on this.

INPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
	@Division_Id	INT						
	@StartIndex		INT			1
	@EndIndex		INT			2147483647

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	
	EXEC dbo.Division_History_Sel_By_Division_Id  2225,1,15
	EXEC dbo.Division_History_Sel_By_Division_Id  2222,1,10
	EXEC dbo.Division_History_Sel_By_Division_Id  22,1,10

AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------          
	PNR			PANDARINATH
	HG			Harihara Suthan G
	RR			Raghu Reddy
	CPE			Chaitanya Panduga Eshwar
	
MODIFICATIONS
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			17-AUG-10	CREATED
	HG			9/27/2010	Added a new parameter (Is_History) value as 0 for the procedure User_Sel_By_Division_Id to get only the active users.
	RR			08/05/2011	Added new procedure RM_DEAL_TICKET_Sel_By_Division_Id to get deal ticket details for the division MAINT-787
	RR			2011-12-13	Part of delete tool enhancement, removed the procedure "dbo.Client_Attribute_Sel_By_Sitegroup_Id" used to get the 
							client attributes of the given division as the client attribute tables moved to DVDEHub
	CPE			2012-04-06	Removed the call to GHG_Sources_Sel_By_Sitegroup_Id as Client_Commodity_Detail is moved to Hub
*/

CREATE PROCEDURE dbo.Division_History_Sel_By_Division_Id
( 
 @Division_Id INT
,@StartIndex INT = 1
,@EndIndex INT = 2147483647 )
AS 
BEGIN

      SET NOCOUNT ON ;

      DECLARE @Owner_Type VARCHAR(10) = 'Division'

      EXEC dbo.Get_Division_Details_p @Division_Id
      EXEC dbo.Site_Dtl_Sel_By_Division_Id @Division_Id, @StartIndex, @EndIndex
      EXEC dbo.Sso_Savings_Detail_Sel_By_Owner_Id_Type @Division_Id, @Owner_Type, @StartIndex, @EndIndex
      EXEC dbo.Sso_Document_Sel_By_Owner_Id_Type @Division_Id, @Owner_Type, @StartIndex, @EndIndex
      EXEC dbo.Sso_Project_Sel_By_Owner_Id_Type @Division_Id, @Owner_Type, @StartIndex, @EndIndex
      EXEC dbo.User_Sel_By_Division_Id @Division_Id, 0, @StartIndex, @EndIndex
      EXEC dbo.RM_DEAL_TICKET_Sel_By_Division_Id @Division_Id, @StartIndex, @EndIndex

END


;
GO


GRANT EXECUTE ON  [dbo].[Division_History_Sel_By_Division_Id] TO [CBMSApplication]
GO
