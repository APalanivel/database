SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE dbo.GET_SETTLED_MONTH_P

@userId varchar(10),
@sessionId varchar(20)
--by suresh reddy 

as
	set nocount on
	declare @cmonth int
	
	select @cmonth= datepart(MONTH,getdate()) 
	
	select SETTLEMENT_DATE from rm_nymex_settlement
	where  datepart(MONTH,month_identifier)  =(@cmonth+1) and 
	datepart(year,month_identifier)=datepart(year,getdate())
GO
GRANT EXECUTE ON  [dbo].[GET_SETTLED_MONTH_P] TO [CBMSApplication]
GO
