SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  procedure [dbo].[cbmsLookup_GetByType]
	( @MyAccountId int 
	, @entity_type int 
	)
AS
BEGIN

	   select entity_id
		, entity_name
		, entity_type
		, entity_description
	     from entity
	    where entity_type = @entity_type
	 order by display_order asc
		, entity_name 
END
GO
GRANT EXECUTE ON  [dbo].[cbmsLookup_GetByType] TO [CBMSApplication]
GO
