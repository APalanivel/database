SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Client_Commodity_Variance_Test_Config_Del

DESCRIPTION:

INPUT PARAMETERS:
	Name										DataType		Default	Description
---------------------------------------------------------------
@Client_Commodity_Variance_Test_Config_Id		INT
@User_Info_Id									INT

OUTPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	
USAGE EXAMPLES:
------------------------------------------------------------

	BEGIN TRANSACTION
	SELECT * FROM Client_Commodity_Variance_Test_Config WHERE Account_Id=1	
	  EXEC Client_Commodity_Variance_Test_Config_Del @Client_Commodity_Variance_Test_Config_Id=1
	SELECT * FROM Client_Commodity_Variance_Test_Config WHERE Account_Id=1  		
	ROLLBACK TRANSACTION


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SP			Sandeep Pigilam
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	SP       	2016-11-01	Created for Variance Test Enhancements Phase II.

******/

CREATE PROCEDURE [dbo].[Client_Commodity_Variance_Test_Config_Del]
      ( 
       @Client_Commodity_Variance_Test_Config_Id INT
      ,@User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 
      
      
      DELETE
            acirt
      FROM
            dbo.Client_Commodity_Variance_Test_Config acirt
      WHERE
            acirt.Client_Commodity_Variance_Test_Config_Id = @Client_Commodity_Variance_Test_Config_Id
      
            
END;
;
;
GO
GRANT EXECUTE ON  [dbo].[Client_Commodity_Variance_Test_Config_Del] TO [CBMSApplication]
GO
