SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_DT_SEARCH_QUEUE_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@user_id       	varchar(10)	          	
	@commodity_type_id	int       	          	
	@region_id     	int       	          	
	@client_id     	int       	          	
	@site_id       	int       	          	
	@account_id    	int       	          	
	@deal_status_id	int       	          	
	@from_home_page	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
exec dbo.SR_SAD_GET_DT_SEARCH_QUEUE_DETAILS_P 9,290,4,0,0,285641,1,10 

exec dbo.SR_SAD_GET_DT_SEARCH_QUEUE_DETAILS_P 9,290,0,0,0,0,0,0 

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	SSR			09/27/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on	        		        	
******/
CREATE PROCEDURE dbo.SR_SAD_GET_DT_SEARCH_QUEUE_DETAILS_P
    @user_id VARCHAR(10)
  , @commodity_type_id INT
  , @region_id INT
  , @client_id INT
  , @site_id INT
  , @account_id INT
  , @deal_status_id INT
  , @from_home_page INT
AS
BEGIN

    SET NOCOUNT ON
    DECLARE @selectClause VARCHAR(1000)
    DECLARE @fromClause VARCHAR(1000)
    DECLARE @whereClause VARCHAR(1000)
    DECLARE @SQLStatement VARCHAR(8000)
    DECLARE @groupClause VARCHAR(1000)
	
    SELECT
        @selectClause = 'sdt.SR_DEAL_TICKET_ID, cl.CLIENT_ID, cl.CLIENT_NAME, '
        + 'DBO.SR_SAD_FN_GET_DEALTICKET_ACCOUNTS(sdt.SR_DEAL_TICKET_ID) AS account_number,'
        + ' dbo.SR_SAD_FN_GET_DEALTICKET_SITE(sdt.SR_DEAL_TICKET_ID) as SITE_NAME, '
        + ' e1.ENTITY_NAME as '
        + ' DEAL_TYPE_NAME, e2.ENTITY_NAME as COMMODITY_TYPE_NAME, '
        + ' sdt.FROM_DATE, sdt.TO_DATE, ui.USERNAME as USER_NAME, '
        + ' e3.ENTITY_NAME as DEAL_TICKET_STATUS,  '
        + ' dbo.SR_SAD_FN_GET_DEALTICKET_REGIONS(sdt.SR_DEAL_TICKET_ID) as REGION_NAME, '
        + ' volTermType.entity_name as vol_term_type '
    SELECT
        @fromClause = ' SR_DEAL_TICKET sdt, Client cl, '
        + ' SR_DEAL_TICKET_ACCOUNT_MAP sdtam, SITE st, '
        + ' ACCOUNT acc, SR_DEAL_TICKET_DETAILS sdtd, '
        + ' ENTITY e1, ENTITY e2, ENTITY e3, USER_INFO ui, DIVISION di, REGION rg, '
        + ' STATE state, ADDRESS address, Entity volTermType ' 
     	      	
	
    SELECT
        @whereClause = ' sdtam.ACCOUNT_ID = acc.account_id and '
        + ' st.site_id = acc.site_id and '
        + ' di.division_id = st.division_id and '
        + ' cl.client_id = di.client_id and '
        + ' sdt.volume_term_type_id = volTermType.entity_id and '
        + ' sdt.SR_DEAL_TICKET_ID = sdtam.SR_DEAL_TICKET_ID and '
        + ' sdtam.ACCOUNT_ID = acc.ACCOUNT_ID and '
        + ' sdtd.SR_DEAL_TICKET_ID = sdt.SR_DEAL_TICKET_ID and '
        + ' sdtd.DEAL_TYPE_ID = e1.ENTITY_ID and '
        + ' e2.ENTITY_ID = sdt.COMMODITY_TYPE_ID and '
        + ' ui.USER_INFO_ID = sdtd.WITH_WHOM and '
        + ' sdtd.DEAL_STATUS_TYPE_ID = e3.ENTITY_ID and '
        + ' st.primary_address_id = address.address_id and '
        + ' address.state_id = state.state_id and '
        + ' state.region_id = rg.region_id '

    SELECT
        @groupClause = ' sdt.SR_DEAL_TICKET_ID, cl.CLIENT_ID, cl.CLIENT_NAME, '
        + ' e1.ENTITY_NAME , e2.ENTITY_NAME , sdt.FROM_DATE, '
        + ' sdt.TO_DATE, ui.USERNAME, e3.ENTITY_NAME,  volTermType.entity_name ' 
    IF ( @user_id > 0 ) 
        BEGIN
            SELECT
                @whereClause = @whereClause + ' and ui.USER_INFO_ID = '
                + STR(@user_id)
        END 

    IF ( @commodity_type_id > 0 ) 
        BEGIN
            SELECT
                @whereClause = @whereClause + ' and e2.ENTITY_ID = '
                + STR(@commodity_type_id)
        END 


    IF ( @region_id > 0 ) 
        BEGIN
            SELECT
                @whereClause = @whereClause + ' and rg.REGION_ID = '
                + STR(@region_id)
        END
		

    IF ( @client_id > 0 ) 
        BEGIN
            SELECT
                @whereClause = @whereClause + ' and  cl.CLIENT_ID = '
                + STR(@client_id) 
        END


    IF ( @site_id > 0 ) 
        BEGIN
            SELECT
                @whereClause = @whereClause + ' and  st.SITE_ID = '
                + STR(@site_id) 
        END

    IF ( @account_id > 0 ) 
        BEGIN
            SELECT
                @whereClause = @whereClause + ' and  acc.ACCOUNT_ID = '
                + STR(@account_id) 
        END

    IF ( @deal_status_id > 0 ) 
        BEGIN
            SELECT
                @whereClause = @whereClause + ' and  e3.ENTITY_ID = '
                + STR(@deal_status_id) 
        END

    IF ( @from_home_page = 2 ) 
        BEGIN
            SELECT
                @whereClause = @whereClause
                + ' and e3.ENTITY_NAME not like ''Closed/Cancelled''  ' 
        END


    SELECT
        @SQLStatement = 'SELECT ' + @selectClause + ' FROM ' + @fromClause
        + ' WHERE ' + @whereClause + ' group by ' + @groupClause

    
    EXEC ( @SQLStatement )
    PRINT @SQLStatement

END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_DT_SEARCH_QUEUE_DETAILS_P] TO [CBMSApplication]
GO
