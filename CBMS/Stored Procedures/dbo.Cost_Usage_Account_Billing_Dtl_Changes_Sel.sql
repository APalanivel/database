SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	 dbo.Cost_Usage_Account_Billing_Dtl_Changes_Sel

DESCRIPTION:
			This procedure is used to select changed records between two versions from Cost_Usage_Account_Billing_Dtl.

INPUT PARAMETERS:
	Name						DataType		Default	Description
------------------------------------------------------------
@Last_Version_Id				BIGINT			Beginnng Change tracking Version Id (From Change_Control_Threshold) 			
@Current_Version_Id				BIGINT			Current Change tracking Version Id

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
It will execute only through the replication replacement SSIS package.

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RK			Raghu Kalvapudi
	
MODIFICATIONS
	Initials	Date			Modification
------------------------------------------------------------
	RK			01/22/2014		Created
	HG			2018-10-02		MAINT-7847, Modified the code to return the Inserted/Updated records only if it exists in the table. Due to the timing issue at times the insert/updated records are deleted immediately then this sproc is returning NULL values for the mandatory columns with the change operation of Insert/update.
******/
CREATE PROCEDURE [dbo].[Cost_Usage_Account_Billing_Dtl_Changes_Sel]
      (
       @Last_Version_Id BIGINT
      ,@Current_Version_Id BIGINT )
AS
BEGIN

      SET NOCOUNT ON;

/*****************************************************************************************
   This is here entirely as a work around for SSIS and temporary tables.  This statement
   allows SSIS to be able to return the meta data about the stored procedure to map columns.
*****************************************************************************************/

      IF 1 = 2
            BEGIN

                  SELECT
                        CONVERT(CHAR(1), NULL) Op_Code
                       ,CAST(NULL AS INT) AS Cost_Usage_Account_Billing_Dtl_Id
                       ,CAST(NULL AS INT) AS Account_Id
                       ,CAST(NULL AS DATE) AS Service_Month
                       ,CAST(NULL AS DATE) AS Billing_Start_Dt
                       ,CAST(NULL AS DATE) AS Billing_End_Dt
                       ,CAST(NULL AS INT) AS Billing_Days
            END

      CREATE TABLE #Cost_Usage_Account_Billing_Dtl_Changes
            (
             Cost_Usage_Account_Billing_Dtl_Id INT NOT NULL
            ,Op_Code CHAR(1) )

      INSERT      INTO #Cost_Usage_Account_Billing_Dtl_Changes
                  (Cost_Usage_Account_Billing_Dtl_Id
                  ,Op_Code )
                  SELECT
                        cuabd_ct.Cost_Usage_Account_Billing_Dtl_Id
                       ,CONVERT(CHAR(1), cuabd_ct.SYS_CHANGE_OPERATION)
                  FROM
                        CHANGETABLE(CHANGES dbo.Cost_Usage_Account_Billing_Dtl, @Last_Version_Id) cuabd_ct
                  WHERE
                        cuabd_ct.SYS_Change_Version <= @Current_Version_Id
            
      SELECT
            cuabd_ct.Op_Code
           ,cuabd_ct.Cost_Usage_Account_Billing_Dtl_Id
           ,cuabd.Account_Id
           ,cuabd.Service_Month
           ,cuabd.Billing_Start_Dt
           ,cuabd.Billing_End_Dt
           ,cuabd.Billing_Days
      FROM
            #Cost_Usage_Account_Billing_Dtl_Changes cuabd_ct
            LEFT JOIN dbo.Cost_Usage_Account_Billing_Dtl cuabd
                  ON cuabd.Cost_Usage_Account_Billing_Dtl_Id = cuabd_ct.Cost_Usage_Account_Billing_Dtl_Id
      WHERE
            ( cuabd_ct.Op_Code = 'D'
              OR cuabd.Cost_Usage_Account_Billing_Dtl_Id IS NOT NULL )
              
      DROP TABLE #Cost_Usage_Account_Billing_Dtl_Changes

END;

GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Billing_Dtl_Changes_Sel] TO [ETL_Execute]
GO
