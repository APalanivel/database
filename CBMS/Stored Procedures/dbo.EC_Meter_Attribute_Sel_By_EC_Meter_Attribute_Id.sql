SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
  
/******                  
Name:   dbo.EC_Meter_Attribute_Sel_By_EC_Meter_Attribute_Id           
                  
Description:                  
   This sproc to get the attribute details for a Given id.          
                               
 Input Parameters:                  
    Name        DataType   Default   Description                    
----------------------------------------------------------------------------------------                    
 @EC_Meter_Attribute_Id    INT    
        
 Output Parameters:                        
    Name        DataType   Default   Description                    
----------------------------------------------------------------------------------------                    
                  
 Usage Examples:                      
----------------------------------------------------------------------------------------       
    
   Exec dbo.EC_Meter_Attribute_Sel_By_EC_Meter_Attribute_Id 16    
       
   Exec dbo.EC_Meter_Attribute_Sel_By_EC_Meter_Attribute_Id  7           
     
Author Initials:                  
    Initials  Name                  
----------------------------------------------------------------------------------------                    
 NR    Narayana Reddy                   
 RKV             Ravi Kumar Vegesna    
 SLP	Sri Lakshimi Pallikonda.    
 Modifications:                  
    Initials        Date   Modification                  
----------------------------------------------------------------------------------------                    
    NR    2015-04-22  Created For AS400.     
    RKV    2016-11-02      Maint-4317 Added New column Attribute_Use_Cd,Attribute_Use to output           
    SLP    2020-04-10	 Added Meter Attribute data in select.     
                 
******/     
    
CREATE PROCEDURE [dbo].[EC_Meter_Attribute_Sel_By_EC_Meter_Attribute_Id]    
      (     
       @EC_Meter_Attribute_Id INT )    
AS     
BEGIN    
      SET NOCOUNT ON     
                
      SELECT    
            ema.EC_Meter_Attribute_Id    
           ,ema.State_Id    
           ,ema.Commodity_Id    
           ,ema.EC_Meter_Attribute_Name    
           ,ema.Attribute_Type_Cd    
           ,ema.Is_Used_In_Calc_Vals    
           ,s.COUNTRY_ID    
           ,s.STATE_ID    
           ,s.STATE_NAME    
           ,ema.Attribute_Use_Cd    
           ,au.Code_Value Attribute_Use    
     ,vcd.Code_Value Vendor_Type_Value  
	 ,ema.Vendor_Type_Cd Vendor_Type_Cd
  
      FROM    
            dbo.EC_Meter_Attribute ema    
            INNER JOIN dbo.STATE s    
                  ON s.STATE_ID = ema.State_Id    
            INNER JOIN code au    
                  ON au.Code_Id = ema.Attribute_Use_Cd    
   LEFT JOIN code vcd  
    ON vcd.Code_Id=ema.Vendor_Type_Cd  
      WHERE    
            ema.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id    
                    
END;    
    
    
;
GO

GRANT EXECUTE ON  [dbo].[EC_Meter_Attribute_Sel_By_EC_Meter_Attribute_Id] TO [CBMSApplication]
GO
