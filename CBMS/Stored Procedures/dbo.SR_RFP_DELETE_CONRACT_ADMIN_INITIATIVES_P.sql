SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- SR_RFP_DELETE_CONRACT_ADMIN_INITIATIVES_P 2000561, 17977, 0
CREATE PROCEDURE [dbo].[SR_RFP_DELETE_CONRACT_ADMIN_INITIATIVES_P]
	@cbmsImageId INT,
	@srAccountGroupId INT,
	@isBidGroup INT
AS
BEGIN

	SET NOCOUNT ON

	DELETE FROM dbo.SR_RFP_CONTRACT_ADMIN_INITIATIVES   
    WHERE SR_ACCOUNT_GROUP_ID = @srAccountGroupId
		AND cbms_image_id = @cbmsImageId
		AND IS_BID_GROUP = @isBidGroup   

	--DELETE FROM dbo.Cbms_Image   
	--WHERE cbms_image_id = @cbmsImageId

	IF (@isBidGroup = 0)
	 BEGIN

		 UPDATE dbo.SR_RFP_CHECKLIST
			SET IS_CONTRACT_ADMIN_INITIATED = 0
		 WHERE SR_RFP_ACCOUNT_ID = @srAccountGroupId
	 END
	ELSE IF (@isBidGroup > 0 )
	 BEGIN

		UPDATE rfpCheckList
			SET rfpCheckList.IS_CONTRACT_ADMIN_INITIATED = 0
		FROM dbo.SR_RFP_CHECKLIST rfpCheckList
			INNER JOIN dbo.SR_RFP_ACCOUNT rfpAcct ON rfpAcct.SR_RFP_ACCOUNT_ID = rfpCheckList.SR_RFP_ACCOUNT_ID
		WHERE rfpAcct.SR_RFP_BID_GROUP_ID = @srAccountGroupId

	 END
END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_DELETE_CONRACT_ADMIN_INITIATIVES_P] TO [CBMSApplication]
GO
