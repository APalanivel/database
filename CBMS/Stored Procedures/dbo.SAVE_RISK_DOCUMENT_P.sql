SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SAVE_RISK_DOCUMENT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@documentLevelId	int       	          	
	@documentTypeId	int       	          	
	@siteOrDivisionId	int       	          	
	@cbmsImageId   	int       	          	
	@clientId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE dbo.SAVE_RISK_DOCUMENT_P
@userId varchar(10),
@sessionId varchar(20),
@documentLevelId int,
@documentTypeId int,
@siteOrDivisionId int,
@cbmsImageId int,
@clientId int
AS
set nocount on
declare @entityName varchar(200)

select @entityName = entity_name from entity where entity_id = @documentLevelId

if(@entityName = 'Site')

begin

INSERT INTO RM_RISK_PLAN_PROFILE
	(DOCUMENT_TYPE_ID,CBMS_IMAGE_ID, DOCUMENT_LEVEL_TYPE_ID,SITE_ID,CLIENT_ID)
VALUES
            (@documentTypeId, @cbmsImageId, @documentLevelId, @siteOrDivisionId, @clientId)

end

else if (@entityName = 'Division')

begin

INSERT INTO RM_RISK_PLAN_PROFILE
	(DOCUMENT_TYPE_ID,CBMS_IMAGE_ID,DIVISION_ID,DOCUMENT_LEVEL_TYPE_ID,CLIENT_ID)
VALUES
            (@documentTypeId, @cbmsImageId, @siteOrDivisionId, @documentLevelId,  @clientId)

end

else if (@entityName = 'Corporate')

begin

INSERT INTO RM_RISK_PLAN_PROFILE
	(DOCUMENT_TYPE_ID,CBMS_IMAGE_ID,DOCUMENT_LEVEL_TYPE_ID,CLIENT_ID)
VALUES
            (@documentTypeId, @cbmsImageId, @documentLevelId, @clientId)

end
GO
GRANT EXECUTE ON  [dbo].[SAVE_RISK_DOCUMENT_P] TO [CBMSApplication]
GO
