SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:		Transmission_End_Dialog

DESCRIPTION:
	Activated by Cbms_Contract_Attribute_Transmission_Queue, it processes messages
	of http://schemas.microsoft.com/SQL/ServiceBroker/EndDialog
	
	Procedure Ends conversation 


INPUT PARAMETERS:
	Name						DataType		Default	Description
------------------------------------------------------------
	@Message					XML						-- Exists for Consistancy. message type validates to null 
	@Conversation_Handle		UNIQUEIDENTIFIER		-- Conversation that sent the message

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	

USAGE EXAMPLES:
------------------------------------------------------------
 
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hariharasuthan Ganesan

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG			2015-12-24	Created
******/
CREATE PROCEDURE [dbo].[Transmission_End_Dialog]
      (
       @Message XML
      ,@Conversation_Handle UNIQUEIDENTIFIER
      ,@Message_Batch_Count INT = 0 OUTPUT )
AS
BEGIN
      SET NOCOUNT ON;
	
      END CONVERSATION @Conversation_Handle;

      SET @Message_Batch_Count = 0; --Dummy output parameter

END;
;
GO
GRANT EXECUTE ON  [dbo].[Transmission_End_Dialog] TO [sb_CBMS_Service]
GRANT EXECUTE ON  [dbo].[Transmission_End_Dialog] TO [sb_Sys_Service]
GO
