SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.Invoice_Data_Quality_Failed_Test_Details_Sel_By_Cu_Invoice_Id        
		   
DESCRIPTION:    
  Used to select Invoice data quality failed test dtls for the given account , commodity and service month    
      
 INPUT PARAMETERS:          
 Name						DataType					Default					Description          
-------------------------------------------------------------------------------------------------          
 @Account_Id				INT    
 @Commodity_Id				INT							NULL    
 @Service_Month				DATE    
     
 OUTPUT PARAMETERS:         
 Name						DataType					Default					Description          
-------------------------------------------------------------------------------------------------     
     
 USAGE EXAMPLES:          
-------------------------------------------------------------------------------------------------   

       
 EXEC dbo.Invoice_Data_Quality_Failed_Test_Details_Sel_By_Cu_Invoice_Id 34054 ,@Is_Show_Latest_Failed_Tests = 1  
 EXEC dbo.Invoice_Data_Quality_Failed_Test_Details_Sel_By_Cu_Invoice_Id 34054  
    
    
     
 AUTHOR INITIALS:    
 Initials		Name    
-------------------------------------------------------------------------------------------------          
 NR				Narayana Reddy    
     
 MODIFICATIONS    
 Initials		 Date       Modification    
------------------------------------------------------------    
 NR				2019-04-23  Created Data2.0 -  Data quality.    
    
******/

CREATE PROCEDURE [dbo].[Invoice_Data_Quality_Failed_Test_Details_Sel_By_Cu_Invoice_Id]
    (
        @Cu_Invoice_Id INT
        , @Is_Show_Latest_Failed_Tests BIT = 0
    )
AS
    BEGIN

        SET NOCOUNT ON;



        SELECT
            idql.Invoice_Data_Quality_Log_Id
            , idql.Cu_Invoice_Id
            , LEFT(Sites.Site_Name, LEN(Sites.Site_Name) - 3) AS Site_Name
            , cha.Account_Number
            , idql.Account_ID
            , idql.Commodity_ID
            , c.Commodity_Name
            , idql.Service_Month
            , idql.Execution_Group_No
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Run_By
            , idql.Execution_Ts
            , idql.Current_Value
            , idql.Baseline_Value
            , idql.Data_Category
            , idql.Test_Description
            , e.ENTITY_NAME AS Uom_Name
            , ee.ENTITY_NAME AS Baseline_Uom_Name
            , cu.CURRENCY_UNIT_NAME
        FROM
            LOGDB.dbo.Invoice_Data_Quality_Log idql
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = idql.Account_ID
            INNER JOIN dbo.Commodity c
                ON c.Commodity_Id = idql.Commodity_ID
            CROSS APPLY (SELECT (   SELECT
                                        CAST(ch_Site.Site_name AS VARCHAR(MAX)) + '|@$'
                                    FROM
                                        Core.Client_Hier ch_Site
                                        INNER JOIN Core.Client_Hier_Account cha_Acc
                                            ON cha_Acc.Client_Hier_Id = ch_Site.Client_Hier_Id
                                    WHERE
                                        cha_Acc.Account_Id = cha.Account_Id
                                    GROUP BY
                                        ch_Site.Site_name
                                        , ch_Site.Site_Id
                                    FOR XML PATH(''), TYPE).value('.', 'VARCHAR(MAX)') AS Site_Name) Sites
            LEFT JOIN dbo.ENTITY e
                ON e.ENTITY_ID = idql.UOM_Id
            LEFT JOIN dbo.ENTITY ee
                ON ee.ENTITY_ID = idql.Baseline_UOM_Id
            LEFT JOIN dbo.CURRENCY_UNIT cu
                ON cu.CURRENCY_UNIT_ID = idql.Currency_Unit_Id
            LEFT JOIN dbo.USER_INFO ui
                ON ui.USER_INFO_ID = idql.Execution_User_Info_Id
        WHERE
            idql.Cu_Invoice_Id = @Cu_Invoice_Id
            AND (   (   @Is_Show_Latest_Failed_Tests = 1
                        AND idql.Is_Closed = 0
                        AND idql.Is_Failure = 1)
                    OR  (   @Is_Show_Latest_Failed_Tests = 0
                            AND idql.Is_Failure = 1))
        GROUP BY
            idql.Invoice_Data_Quality_Log_Id
            , idql.Cu_Invoice_Id
            , LEFT(Sites.Site_Name, LEN(Sites.Site_Name) - 3)
            , cha.Account_Number
            , idql.Account_ID
            , idql.Commodity_ID
            , c.Commodity_Name
            , idql.Service_Month
            , idql.Execution_Ts
            , idql.Execution_Group_No
            , idql.Current_Value
            , idql.Baseline_Value
            , idql.Data_Category
            , idql.Test_Description
            , e.ENTITY_NAME
            , ee.ENTITY_NAME
            , cu.CURRENCY_UNIT_NAME
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME
        ORDER BY
            idql.Execution_Group_No DESC;



    END;




GO
GRANT EXECUTE ON  [dbo].[Invoice_Data_Quality_Failed_Test_Details_Sel_By_Cu_Invoice_Id] TO [CBMSApplication]
GO
