SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE  PROCEDURE dbo.GET_CURRENCY_CONVERSION_P
@userId varchar(10),
@sessionId varchar(10)

as
	set nocount on	
	select	CURRENCY_UNIT_ID,
		CURRENCY_UNIT_NAME
	
	from	CURRENCY_UNIT
	order by currency_unit_name
GO
GRANT EXECUTE ON  [dbo].[GET_CURRENCY_CONVERSION_P] TO [CBMSApplication]
GO
