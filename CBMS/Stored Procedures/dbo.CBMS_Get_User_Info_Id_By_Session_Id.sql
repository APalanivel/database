SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.CBMS_Get_User_Info_Id_By_Session_Id

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@session_id    	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
PR				Pradip Rajput
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
PR	        	9/24/2018	Created
 


******/
CREATE PROCEDURE [dbo].[CBMS_Get_User_Info_Id_By_Session_Id]
    (
        @session_id INT
    )
AS
    BEGIN



        SELECT
            SESSION_INFO_ID
            , USER_INFO_ID
        FROM
            dbo.SESSION_INFO
        WHERE
            SESSION_INFO_ID = @session_id;



    END;


GO
GRANT EXECUTE ON  [dbo].[CBMS_Get_User_Info_Id_By_Session_Id] TO [CBMSApplication]
GO
