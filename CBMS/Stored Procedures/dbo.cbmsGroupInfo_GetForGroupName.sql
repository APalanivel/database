SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [dbo].[cbmsGroupInfo_GetForGroupName]
	( @MyAccountId int
	, @group_name varchar(200)
	)
AS
BEGIN

	   select group_info_id
		, group_name
		from GROUP_INFO
	            where GROUP_NAME = @group_name

END
GO
GRANT EXECUTE ON  [dbo].[cbmsGroupInfo_GetForGroupName] TO [CBMSApplication]
GO
