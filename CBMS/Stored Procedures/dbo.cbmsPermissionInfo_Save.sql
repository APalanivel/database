SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.[cbmsPermissionInfo_Save]


DESCRIPTION: 

INPUT PARAMETERS:    
      Name                  DataType          Default     Description    
------------------------------------------------------------------    
	@MyAccountId               int
	@permission_info_id        int               null
	@permission_name           varchar(200)
	@permission_description    varchar(500)      null
        
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
--exec cbmsPermissionInfo_Save
--	@MyAccountId = 1
--	, @permission_info_id = null
--	, @permission_name = 'Test Data'
--	, @permission_description =  'Test Data'


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates
	   DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[cbmsPermissionInfo_Save]
	( @MyAccountId int
	, @permission_info_id int = null
	, @permission_name varchar(200)
	, @permission_description varchar(500) = null
	)
AS

BEGIN

	set nocount on

	declare @ThisId int
	set @ThisId = @permission_info_id

	if @ThisId is null
	begin


		insert into permission_info
			( permission_name, permission_description )
		values
			( @permission_name, @permission_description )
	
		set @ThisId = scope_identity()
	end
	else
	begin
	
	   update permission_info
	      set permission_name = @permission_name
		, permission_description = @permission_description
	    where permission_info_id = @ThisId
	end
	
	exec cbmsPermissionInfo_Get @MyAccountId, @ThisId

END
GO
GRANT EXECUTE ON  [dbo].[cbmsPermissionInfo_Save] TO [CBMSApplication]
GO
