SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******    
NAME:    
      dbo.Account_Commodity_Analyst_Merge_All  
     
 DESCRIPTION:     
      Created to to Inset data when a Auto Fill is selected on Analyst Mapping Page Account Level  
        
 INPUT PARAMETERS:    
 Name   DataType Default Description    
------------------------------------------------------------    
 @Client_Id  INT  
 @Sitegroup_Id  INT   NULL  
 @Site_Id  INT   NULL  
 @Account_Id INT   NULL  
 @Commodity_Id INT  
 @Analyst_User_Info_Id  INT    
   
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 USAGE EXAMPLES:    
------------------------------------------------------------  
  
 EXEC dbo.Account_Commodity_Analyst_Merge_All 235,NULL,1706,NULL,290,31443  
 EXEC dbo.Account_Commodity_Analyst_Merge_All 235,NULL,NULL,NULL,290,155  
  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------   
 AKR  Ashok Kumar Raju  
   
  
 MODIFICATIONS     
 Initials Date  Modification  
------------------------------------------------------------  
 AKR        2012-09-26  Created for POCO  
  
******/  
  
CREATE  PROCEDURE dbo.Account_Commodity_Analyst_Merge_All
      ( 
       @Client_Id INT
      ,@Sitegroup_Id INT = NULL
      ,@Site_Id INT = NULL
      ,@Account_Id INT = NULL
      ,@Commodity_Id INT
      ,@Analyst_User_Info_Id INT )
AS 
BEGIN  
      SET NOCOUNT ON  
        
        
      DECLARE @Commodity_Service_Cd INT  
        
      CREATE TABLE #Site_Accounts
            ( 
             Account_Id INT
            ,Site_Analyst_User_Info_Id INT )  
        
      SELECT
            @Commodity_Service_Cd = Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'CommodityServiceSource'
            AND c.Code_Value = 'Invoice'  
  
      INSERT      INTO #Site_Accounts
                  ( 
                   Account_Id
                  ,Site_Analyst_User_Info_Id )
                  SELECT
                        cha.Account_Id
                       ,coalesce(sca.Analyst_User_Info_Id, ca.Analyst_User_Info_Id) Site_Analyst_User_Info_Id
                  FROM
                        Core.Client_Hier ch
                        INNER JOIN Core.Client_Commodity cc
                              ON ch.Client_Id = cc.Client_Id
                                 AND cc.Commodity_Id = @Commodity_Id
                                 AND cc.Commodity_Service_Cd = @Commodity_Service_Cd
                        INNER JOIN Core.Client_Hier ss
                              ON ch.Sitegroup_Id = ss.Sitegroup_id
                                 AND ss.site_id > 0
                        INNER JOIN core.Client_Hier_Account cha
                              ON ss.Client_Hier_Id = cha.Client_Hier_Id
                        LEFT JOIN dbo.Site_Commodity_Analyst sca
                              ON ss.Site_id = sca.Site_Id
                                 AND sca.Commodity_Id = @Commodity_Id
                        LEFT JOIN dbo.Client_Commodity_Analyst ca
                              ON ca.Client_Commodity_Id = cc.Client_Commodity_Id
                  WHERE
                        ( ch.CLIENT_ID = @client_id )
                        AND ( @Sitegroup_Id IS NULL
                              OR ch.Sitegroup_Id = @Sitegroup_Id )
                        AND ( @Site_Id IS NULL
                              OR ss.Site_Id = @Site_Id )
                        AND ( @Account_Id IS NULL
                              OR cha.Account_Id = @Account_Id )
                        AND cha.Account_Type = 'Utility'
                        AND ch.Site_Id = 0
                        AND ch.Sitegroup_Id > 0
                  GROUP BY
                        cha.Account_Id
                       ,sca.Analyst_User_Info_Id
                       ,ca.Analyst_User_Info_Id   
      BEGIN TRY                
            BEGIN TRAN                     
  
            DELETE
                  aca
            FROM
                  dbo.Account_Commodity_Analyst aca
                  INNER JOIN #Site_Accounts sa
                        ON aca.Account_Id = sa.Account_Id
                           AND Commodity_Id = @Commodity_Id
                           AND ( @Analyst_User_Info_Id IS NULL
                                 OR sa.Site_Analyst_User_Info_Id = @Analyst_User_Info_Id )  
         
            IF ( @Analyst_User_Info_Id IS NOT NULL ) 
                  BEGIN
                        MERGE INTO dbo.Account_Commodity_Analyst AS tgt
                              USING 
                                    ( SELECT
                                          Account_Id
                                         ,Site_Analyst_User_Info_Id
                                         ,@Analyst_User_Info_Id Account_Analyst_User_Info_Id
                                      FROM
                                          #Site_Accounts ) src
                              ON src.Account_Id = tgt.Account_Id
                                    AND tgt.Commodity_Id = @Commodity_Id
                              WHEN MATCHED 
                                    THEN  
       UPDATE                           SET
                                          tgt.Analyst_User_Info_Id = src.Account_Analyst_User_Info_Id
                              WHEN NOT MATCHED AND @Analyst_User_Info_Id != isnull(src.Site_Analyst_User_Info_Id, 0)
                                    THEN   
         INSERT
                                          ( 
                                           Account_Id
                                          ,Commodity_Id
                                          ,Analyst_User_Info_Id )
                                        VALUES
                                          ( 
                                           src.Account_Id
                                          ,@Commodity_Id
                                          ,src.Account_Analyst_User_Info_Id ) ;  
                  END
            COMMIT TRAN                
      END TRY                
      BEGIN CATCH                
            IF @@TRANCOUNT > 0 
                  ROLLBACK TRAN                
                
            EXEC dbo.usp_RethrowError                
                
      END CATCH                
               
      
END  
;
GO
GRANT EXECUTE ON  [dbo].[Account_Commodity_Analyst_Merge_All] TO [CBMSApplication]
GO
