SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/******  
NAME:  
 
 dbo.Cbms_Image_Location_SEL_By_Image_Drive
 
 DESCRIPTION:   
 
 Inserts the BillImageURL into the CBMS.CBMS_IMAGE table for the 
 given Ubm_Batch_Master_Log_Id.
 
 INPUT PARAMETERS:  
 Name			DataType	Default		Description  
------------------------------------------------------------    
		@Image_Drive VARCHAR(255)

 
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  USAGE EXAMPLES:  
------------------------------------------------------------  

	EXEC dbo.Cbms_Image_Location_SEL_By_Image_Drive '\\ses.int\docs_prod\Pool01\'

AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 GP			Garrett Page    10/17/2009

 MODIFICATIONS   
 Initials	Date		Modification  
------------------------------------------------------------  			    
******/


CREATE PROCEDURE [dbo].[Cbms_Image_Location_SEL_By_Image_Drive]
(
		@Image_Drive VARCHAR(255)
)
AS
BEGIN

	SET NOCOUNT ON
	
	SELECT CBMS_Image_Location_Id
	FROM CBMS_Image_Location
	WHERE Image_Drive = @Image_Drive

END
GO
GRANT EXECUTE ON  [dbo].[Cbms_Image_Location_SEL_By_Image_Drive] TO [CBMSApplication]
GO
