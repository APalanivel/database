SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.GET_PERMISSIONS_FOR_USER_P
@userName varchar(30) 
AS
begin
set nocount on
	SELECT DISTINCT permission_name 
	       FROM permission_info (nolock)
		    INNER JOIN group_info_permission_info_map (nolock) on group_info_permission_info_map.permission_info_id = permission_info.permission_info_id
		    INNER JOIN user_info_group_info_map (nolock) on user_info_group_info_map.group_info_id = group_info_permission_info_map.group_info_id
		    INNER JOIN user_info (nolock) on user_info.user_info_id = user_info_group_info_map.user_info_id
		WHERE user_info.username = @userName 
end

GO
GRANT EXECUTE ON  [dbo].[GET_PERMISSIONS_FOR_USER_P] TO [CBMSApplication]
GO
