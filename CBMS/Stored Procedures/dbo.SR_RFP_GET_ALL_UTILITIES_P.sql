SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE dbo.SR_RFP_GET_ALL_UTILITIES_P 
@userId varchar,
@sessionId varchar

as
	set nocount on
select	v.VENDOR_ID,
	v.VENDOR_NAME 

from 	VENDOR v,
	entity venType 

where 	v.vendor_type_id = venType.entity_id and
	venType.entity_name = 'Utility'

order by v.vendor_name
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_ALL_UTILITIES_P] TO [CBMSApplication]
GO
