SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********       
NAME:      
   [dbo].[RM_Client_Hier_Forecast_Volume_Upd]      
  
DESCRIPTION:      
  This procedure used to update the RM_Client_Hier_Forecast_Volume table from TVP    
  
INPUT PARAMETERS:      
   
Name           DataType Default Description      
------------------------------------------------------------------------------------------     
 @Commodity_Id         INT  
 @Updated_User_Id        INT  
 @tvp_RM_Client_Hier_Forecast_Volume   tvp_RM_Client_Hier_Forecast_Volume           
  
OUTPUT PARAMETERS:      
Name    DataType  Default Description      
------------------------------------------------------------      
      
USAGE EXAMPLES:      
------------------------------------------------------------      
 BEGIN TRAN   
 DECLARE @tvp_RM_Client_Hier_Forecast_Volume tvp_RM_Client_Hier_Forecast_Volume  
 INSERT INTO @tvp_RM_Client_Hier_Forecast_Volume VALUES (268382, '2018-02-01', '5000',25,'CEMManual')  
 Exec [dbo].[RM_Client_Hier_Forecast_Volume_Upd]  
  @Commodity_Id = 291, @Updated_User_Id = 49  
  , @tvp_RM_Client_Hier_Forecast_Volume = @tvp_RM_Client_Hier_Forecast_Volume  
 ROLLBACK TRAN  
  
  
 EXEC RM_Site_Client_Hier_Forecast_Volume_Sel 1005,291,NULL,'2018-01-01','2018-12-01',NULL,NULL,'273766'  
  
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
PRV   Pramod Reddy V  
  
  
MODIFICATIONS       
 Initials Date   Modification      
------------------------------------------------------------      
PRV   09-06-2018  SP Created  
  
******/

CREATE PROCEDURE [dbo].[RM_Client_Hier_Forecast_Volume_Upd]
(
    @Commodity_Id INT,
    @Updated_User_Id INT,
    @tvp_RM_Client_Hier_Forecast_Volume AS tvp_RM_Client_Hier_Forecast_Volume READONLY
)
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @Data_Change_Level_Cd INT;


    DECLARE @Forecast_Volume TABLE
    (
        Old_Sum_Forecast_Volume DECIMAL(28, 0),
        New_Forecast_Volume DECIMAL(28, 12),
        Client_Hier_Id INT,
        Service_Month DATE,
        Volume_Source_Cd INT,
        Uom_Id INT
    );

    SELECT @Data_Change_Level_Cd = cd.Code_Id
    FROM dbo.Code cd
        INNER JOIN dbo.Codeset cs
            ON cd.Codeset_Id = cs.Codeset_Id
    WHERE cs.Codeset_Name = 'Data Change Level'
          AND cd.Code_Value = 'Site';

    INSERT INTO Trade.RM_Forecast_Volume_Audit
    (
        Client_Hier_Id,
        Account_Id,
        Commodity_Id,
        Service_Month,
        Previous_Forecast_Volume,
        Previous_Uom_Id,
        Previous_Volume_Source_Cd,
        Previous_Data_Change_Level_Cd,
        Updated_User_Id,
        Last_Change_Ts
    )
    SELECT CHF.Client_Hier_Id,
           ISNULL(AF.Account_Id, -1) AS Account_Id,
           CHF.Commodity_Id,
           CHF.Service_Month,
           ISNULL(AF.Forecast_Volume, CHF.Forecast_Volume),
           CHF.Uom_Id,
           ISNULL(AF.Volume_Source_Cd, CHF.Volume_Source_Cd),
           ISNULL(AF.Data_Change_Level_Cd, CHF.Data_Change_Level_Cd),
           @Updated_User_Id,
           GETDATE()
    FROM [Trade].[RM_Client_Hier_Forecast_Volume] CHF
        INNER JOIN @tvp_RM_Client_Hier_Forecast_Volume TVP
            ON CHF.Client_Hier_Id = TVP.Client_Hier_Id
               AND CHF.Service_Month = TVP.Service_Month
        INNER JOIN dbo.Code c
            ON TVP.Volume_Source_Cd = c.Code_Value
        LEFT JOIN [Trade].[RM_Account_Forecast_Volume] AF
            ON AF.Client_Hier_Id = CHF.Client_Hier_Id
               AND AF.Service_Month = CHF.Service_Month
               AND AF.Commodity_Id = CHF.Commodity_Id
    WHERE CHF.Commodity_Id = @Commodity_Id;

    --UPDATE  
    --    CHF  
    --SET  
    --    CHF.Forecast_Volume = TVP.Forecast_Volume  
    --    , CHF.Uom_Id = TVP.Uom_Id  
    --    , CHF.Volume_Source_Cd = c.Code_Id  
    --    , CHF.Updated_User_Id = @Updated_User_Id  
    --    , CHF.Last_Change_Ts = GETDATE()  
    --    , CHF.Data_Change_Level_Cd = @Data_Change_Level_Cd  
    --FROM  
    --    [Trade].[RM_Client_Hier_Forecast_Volume] CHF  
    --    INNER JOIN @tvp_RM_Client_Hier_Forecast_Volume TVP  
    --        ON CHF.Client_Hier_Id = TVP.Client_Hier_Id  
    --           AND  CHF.Service_Month = TVP.Service_Month  
    --    INNER JOIN dbo.Code c  
    --        ON TVP.Volume_Source_Cd = c.Code_Value  
    --WHERE  
    --    CHF.Commodity_Id = @Commodity_Id;  

    MERGE Trade.RM_Client_Hier_Forecast_Volume AS tgt
    USING
    (
        SELECT cf.Forecast_Volume,
               cf.Uom_Id,
               c.Code_Id,
               cf.Client_Hier_Id,
               cf.Service_Month,
               @Commodity_Id AS Commodity_Id
        FROM @tvp_RM_Client_Hier_Forecast_Volume cf
            INNER JOIN dbo.Code c
                ON cf.Volume_Source_Cd = c.Code_Value
    ) src
    ON tgt.Client_Hier_Id = src.Client_Hier_Id
       AND tgt.Service_Month = src.Service_Month
       AND tgt.Commodity_Id = src.Commodity_Id
    WHEN MATCHED THEN
        UPDATE SET tgt.Forecast_Volume = src.Forecast_Volume,
                   tgt.Uom_Id = src.Uom_Id,
                   tgt.Volume_Source_Cd = src.Code_Id,
                   tgt.Updated_User_Id = @Updated_User_Id,
                   tgt.Last_Change_Ts = GETDATE(),
                   tgt.Data_Change_Level_Cd = @Data_Change_Level_Cd
    WHEN NOT MATCHED THEN
        INSERT
        (
            Client_Hier_Id,
            Commodity_Id,
            Service_Month,
            Forecast_Volume,
            Uom_Id,
            Volume_Source_Cd,
            Data_Change_Level_Cd,
            Created_User_Id,
            Created_Ts,
            Updated_User_Id,
            Last_Change_Ts
        )
        VALUES
        (src.Client_Hier_Id, @Commodity_Id, src.Service_Month, src.Forecast_Volume, src.Uom_Id, src.Code_Id,
         @Data_Change_Level_Cd, @Updated_User_Id, GETDATE(), @Updated_User_Id, GETDATE());




    INSERT INTO @Forecast_Volume
    (
        Old_Sum_Forecast_Volume,
        New_Forecast_Volume,
        Client_Hier_Id,
        Service_Month,
        Volume_Source_Cd,
        Uom_Id
    )
    SELECT SUM(RM.Forecast_Volume) AS Old_Sum_Forecast_Volume,
           TVP.Forecast_Volume AS New_Forecast_Volume,
           RM.Client_Hier_Id,
           RM.Service_Month,
           MIN(c.Code_Id) AS Volume_Source_Cd,
           TVP.Uom_Id
    FROM @tvp_RM_Client_Hier_Forecast_Volume TVP
        INNER JOIN [Trade].[RM_Account_Forecast_Volume] RM
            ON TVP.Client_Hier_Id = RM.Client_Hier_Id
               AND TVP.Service_Month = RM.Service_Month
        INNER JOIN dbo.Code c
            ON TVP.Volume_Source_Cd = c.Code_Value
    GROUP BY RM.Client_Hier_Id,
             RM.Service_Month,
             TVP.Forecast_Volume,
             TVP.Uom_Id;

    --;
    --WITH Cte_Account_Forecast
    --AS (SELECT SUM(RM.Forecast_Volume) AS Old_Sum_Forecast_Volume,
    --           TVP.Forecast_Volume AS New_Forecast_Volume,
    --           RM.Client_Hier_Id,
    --           RM.Service_Month,
    --           MIN(c.Code_Id) AS Volume_Source_Cd,
    --           TVP.Uom_Id
    --    FROM @tvp_RM_Client_Hier_Forecast_Volume TVP
    --        INNER JOIN [Trade].[RM_Account_Forecast_Volume] RM
    --            ON TVP.Client_Hier_Id = RM.Client_Hier_Id
    --               AND TVP.Service_Month = RM.Service_Month
    --        INNER JOIN dbo.Code c
    --            ON TVP.Volume_Source_Cd = c.Code_Value
    --    GROUP BY RM.Client_Hier_Id,
    --             RM.Service_Month,
    --             TVP.Forecast_Volume,
    --             TVP.Uom_Id)

    IF EXISTS (SELECT 1 FROM @Forecast_Volume)
    BEGIN
        UPDATE AF
        SET AF.Forecast_Volume = ISNULL(
                                           CAF.New_Forecast_Volume
                                           * ((AF.Forecast_Volume / NULLIF(CAF.Old_Sum_Forecast_Volume, 0)) * 100)
                                           / 100,
                                           0
                                       ),
            AF.Uom_Id = CAF.Uom_Id,
            AF.Volume_Source_Cd = CAF.Volume_Source_Cd,
            AF.Updated_User_Id = @Updated_User_Id,
            AF.Last_Change_Ts = GETDATE(),
            AF.Data_Change_Level_Cd = @Data_Change_Level_Cd
        FROM [Trade].[RM_Account_Forecast_Volume] AF
            INNER JOIN @Forecast_Volume CAF
                ON AF.Client_Hier_Id = CAF.Client_Hier_Id
                   AND AF.Service_Month = CAF.Service_Month
        WHERE AF.Commodity_Id = @Commodity_Id;
    END;

    ELSE
    BEGIN

        ;WITH CTE_Forecast_Volume
         AS (SELECT TVP.Forecast_Volume,
                    CHA.Account_Id,
                    CHA.Client_Hier_Id,
                    TVP.Service_Month,
                    TVP.Uom_Id,
                    TVP.Volume_Source_Cd,
                    COUNT(DISTINCT CHA.Account_Id) AS Account_Cnt
             FROM @tvp_RM_Client_Hier_Forecast_Volume TVP
                 INNER JOIN Core.Client_Hier_Account CHA
                     ON CHA.Client_Hier_Id = TVP.Client_Hier_Id
             WHERE CHA.Account_Type = 'Utility'
                   AND CHA.Commodity_Id = @Commodity_Id
             GROUP BY CHA.Account_Id,
                      CHA.Client_Hier_Id,
                      TVP.Forecast_Volume,
                      TVP.Service_Month,
                      TVP.Uom_Id,
                      TVP.Volume_Source_Cd)
        INSERT INTO [Trade].[RM_Account_Forecast_Volume]
        (
            [Client_Hier_Id],
            [Account_Id],
            [Commodity_Id],
            [Service_Month],
            [Forecast_Volume],
            [Uom_Id],
            [Volume_Source_Cd],
            [Data_Change_Level_Cd],
            [Created_User_Id]
        )
        SELECT CTE.Client_Hier_Id,
               CTE.Account_Id,
               @Commodity_Id,
               CTE.Service_Month,
               CTE.Forecast_Volume / CTE.Account_Cnt,
               CTE.Uom_Id,
               c.Code_Id,
               @Data_Change_Level_Cd,
               @Updated_User_Id
        FROM CTE_Forecast_Volume CTE
            INNER JOIN dbo.Code c
                ON CTE.Volume_Source_Cd = c.Code_Value;


    END;

    UPDATE audit
    SET Current_Forecast_Volume = ISNULL(AF.Forecast_Volume, CHF.Forecast_Volume),
        Current_Uom_Id = CHF.Uom_Id,
        Current_Volume_Source_Cd = ISNULL(AF.Volume_Source_Cd, CHF.Volume_Source_Cd),
        Current_Data_Change_Level_Cd = ISNULL(AF.Data_Change_Level_Cd, CHF.Data_Change_Level_Cd),
        Updated_User_Id = @Updated_User_Id,
        Last_Change_Ts = GETDATE()
    FROM Trade.RM_Forecast_Volume_Audit audit
        INNER JOIN [Trade].[RM_Client_Hier_Forecast_Volume] CHF
            ON audit.Client_Hier_Id = CHF.Client_Hier_Id
               AND audit.Commodity_Id = CHF.Commodity_Id
               AND audit.Service_Month = CHF.Service_Month
        INNER JOIN @tvp_RM_Client_Hier_Forecast_Volume TVP
            ON CHF.Client_Hier_Id = TVP.Client_Hier_Id
               AND CHF.Service_Month = TVP.Service_Month
        INNER JOIN dbo.Code c
            ON TVP.Volume_Source_Cd = c.Code_Value
        LEFT JOIN [Trade].[RM_Account_Forecast_Volume] AF
            ON AF.Client_Hier_Id = CHF.Client_Hier_Id
               AND AF.Service_Month = CHF.Service_Month
               AND AF.Commodity_Id = CHF.Commodity_Id
               AND AF.Account_Id = audit.Account_Id
    WHERE CHF.Commodity_Id = @Commodity_Id;


END;




GO
GRANT EXECUTE ON  [dbo].[RM_Client_Hier_Forecast_Volume_Upd] TO [CBMSApplication]
GO
