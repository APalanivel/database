SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.Report_DE_IC_Chase_For_Bots            
                        
 DESCRIPTION:                        
			This will allow the managers to gain a better understanding of the scope of officers and their allocations of  chase.
			                  
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                       
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            


   EXEC [dbo].[Report_DE_IC_Chase_For_Bots] 
      
       
                    
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
  
 RKV                   Ravi Kumar Vegesna     
   
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 
 RKV                   2020-06-25       Created
                
               
                       
******/
CREATE PROCEDURE [dbo].[Report_DE_IC_Chase_For_Bots]
AS
    BEGIN
        SET NOCOUNT ON;

        IF (1 = 2)
            SELECT
                1 AS IC_Bots_Process_Name
                , 1 AS IC_Bots_Process_Id
                , 1 AS Client_Name
                , 1 AS Client_Id
                , 1 AS Country_Name
                , 1 AS Country_Id
                , 1 AS Site_Name
                , 1 AS Site_Id
                , 1 AS Display_Account_Number
                , 1 AS Account_Id
                , 1 AS Commodity_Name
                , 1 AS Commodity_Id
                , 1 AS Invoice_Collection_Alternative_Account_Number
                , 1 AS Invoice_Collection_officer
                , 1 AS URL
                , 1 AS Username
                , 1 AS Frequency
                , 1 AS Expected_Day;



        SELECT
            ibp.IC_Bots_Process_Name
            , ibp.IC_Bots_Process_Id
            , ch.Client_Name
            , ch.Client_Id
            , ch.Country_Name
            , ch.Country_Id
            , ch.Site_Name
            , ch.Site_Id
            , cha.Display_Account_Number
            , cha.Account_Id
            , c.Commodity_Name
            , cha.Commodity_Id
            , icac.Invoice_Collection_Alternative_Account_Number
            , ico.FIRST_NAME + '' + ico.LAST_NAME Invoice_Collection_officer
            , CASE WHEN aicosd.URL IS NOT NULL THEN aicosd.URL
                  WHEN aicdfeisd.URL IS NOT NULL THEN aicdfeisd.URL
                  WHEN aicdfefsd.URL IS NOT NULL THEN aicdfefsd.URL
                  ELSE ''
              END URL
            , CASE WHEN aicosd.URL IS NOT NULL THEN aicosd.Login_Name
                  WHEN aicdfeisd.URL IS NOT NULL THEN aicdfeisd.Login_Name
                  WHEN aicdfefsd.URL IS NOT NULL THEN aicdfefsd.Login_Name
                  ELSE ''
              END username
            , fc.Code_Value Frequency
            , ISNULL(aicf.Expected_Invoice_Received_Day, aicf.Expected_Invoice_Raised_Day) Expected_Day
        FROM
            dbo.Invoice_Collection_Account_Config icac
            INNER JOIN dbo.IC_Bots_Process_Invoice_Collection_Account_Config_Map ibpicacm
                ON ibpicacm.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            LEFT OUTER JOIN dbo.IC_Bots_Process ibp
                ON ibp.IC_Bots_Process_Id = ibpicacm.IC_Bots_Process_Id
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = icac.Account_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN dbo.Commodity c
                ON c.Commodity_Id = cha.Commodity_Id
            INNER JOIN USER_INFO ico
                ON ico.USER_INFO_ID = icac.Invoice_Collection_Officer_User_Id
            INNER JOIN dbo.Account_Invoice_Collection_Source aics
                ON aics.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                   AND  aics.Is_Primary = 1
            LEFT OUTER JOIN dbo.Account_Invoice_Collection_Online_Source_Dtl aicosd
                ON aicosd.Account_Invoice_Collection_Source_Id = aics.Account_Invoice_Collection_Source_Id
            LEFT OUTER JOIN dbo.Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl aicdfefsd
                ON aicdfefsd.Account_Invoice_Collection_Source_Id = aics.Account_Invoice_Collection_Source_Id
            LEFT OUTER JOIN dbo.Account_Invoice_Collection_Data_Feed_Etl_Image_Source_Dtl aicdfeisd
                ON aicdfeisd.Account_Invoice_Collection_Source_Id = aics.Account_Invoice_Collection_Source_Id
            LEFT OUTER JOIN dbo.Account_Invoice_Collection_Frequency aicf
                ON aicf.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                   AND  aicf.Is_Primary = 1
            LEFT OUTER JOIN Code fc
                ON aicf.Invoice_Frequency_Cd = fc.Code_Id
        WHERE
            ibpicacm.Enroll_In_Bot = 1
			AND cha.Account_Not_Managed = 0
        GROUP BY
            ibp.IC_Bots_Process_Name
            , ibp.IC_Bots_Process_Id
            , ch.Client_Name
            , ch.Client_Id
            , ch.Site_name
            , ch.Site_Id
            , cha.Display_Account_Number
            , cha.Account_Id
            , icac.Invoice_Collection_Alternative_Account_Number
            , ico.FIRST_NAME + '' + ico.LAST_NAME
            , CASE WHEN aicosd.URL IS NOT NULL THEN aicosd.URL
                  WHEN aicdfeisd.URL IS NOT NULL THEN aicdfeisd.URL
                  WHEN aicdfefsd.URL IS NOT NULL THEN aicdfefsd.URL
                  ELSE ''
              END
            , CASE WHEN aicosd.URL IS NOT NULL THEN aicosd.Login_Name
                  WHEN aicdfeisd.URL IS NOT NULL THEN aicdfeisd.Login_Name
                  WHEN aicdfefsd.URL IS NOT NULL THEN aicdfefsd.Login_Name
                  ELSE ''
              END
            , fc.Code_Value
            , ISNULL(aicf.Expected_Invoice_Received_Day, aicf.Expected_Invoice_Raised_Day)
            , cha.Commodity_Id
            , c.Commodity_Name
            , ch.Country_Id
            , ch.Country_Name;





    END;


    ;

GO
GRANT EXECUTE ON  [dbo].[Report_DE_IC_Chase_For_Bots] TO [CBMS_SSRS_Reports]
GO
