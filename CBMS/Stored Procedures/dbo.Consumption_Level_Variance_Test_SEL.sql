
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********        
NAME:  dbo.Consumption_Level_Variance_Test_SEL        
         
DESCRIPTION:  Used to select all the consumption level data        
        
INPUT PARAMETERS:        
      Name              DataType         Default     Description        
------------------------------------------------------------         
@Commodity_Id   INT        
@Variance_Category_Id INT        
@Consumption_Level_Id INT     NULL        
@is_DEO     BIT     NULL        
        
OUTPUT PARAMETERS:        
      Name              DataType         Default     Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:        
        
exec dbo.Consumption_Level_Variance_Test_SEL 290,100174,1,1 -- demand category        
exec dbo.Consumption_Level_Variance_Test_SEL 290,100175,1,1 -- usage category        
exec dbo.Consumption_Level_Variance_Test_SEL 290,100176,1,1 -- cost category        
exec dbo.Consumption_Level_Variance_Test_SEL 290,100177,1,1 -- Taxes category        
        
exec CODE_SEL_BY_CodeSet_Name 'VarianceCategory'        
        
        
------------------------------------------------------------          
AUTHOR INITIALS:        
        
Initials Name        
------------------------------------------------------------        
NK   Nageswara Rao Kosuri        
BCH  Balaraju Chalumuri        
        
        
Initials Date  Modification        
------------------------------------------------------------        
NK  10/12/2009  Created        
BCH  2013-05-02  Added @Variance_Category_Cd input parameter for category based filter to Varience Test page Enhancement.        
        
******/        
CREATE PROCEDURE dbo.Consumption_Level_Variance_Test_SEL
      ( 
       @Commodity_Id INT
      ,@Variance_Category_Cd INT = NULL
      ,@Consumption_Level_Id INT = NULL
      ,@is_DEO BIT = NULL )
AS 
BEGIN        
        
      SET NOCOUNT ON ;        
      SELECT
            vt.variance_test_id
           ,vr.Variance_Rule_Id
           ,vrdtl.Variance_Rule_Dtl_Id
           ,vt.is_inclusive
           ,vt.is_multiple_condition
           ,vrdtl.IS_Active
           ,vrdtl.Is_Data_Entry_Only
           ,cd.code_value AS Category
           ,vp.Parameter
           ,cdo.code_value AS Operator
           ,vr.Positive_Negative
           ,cdb.code_Dsc AS BaseLine
           ,vrdtl.Default_Tolerance AS Tollerance
           ,vrdtl.test_description AS test_description
           ,COALESCE(vcl.VARIANCE_CONSUMPTION_LEVEL_ID, vrdtl.VARIANCE_CONSUMPTION_LEVEL_ID) AS Variance_Consumption_Level
      FROM
            dbo.Variance_Test_Master vt
            INNER JOIN dbo.Variance_Test_Commodity_Map cmap
                  ON cmap.Variance_Test_id = vt.Variance_test_id
            INNER JOIN dbo.Variance_Rule vr
                  ON vr.Variance_Test_id = cmap.Variance_test_id
            LEFT JOIN ( dbo.Variance_Rule_Dtl vrdtl
                        JOIN dbo.Variance_Consumption_Level vcl
                              ON vcl.Variance_Consumption_Level_id = vrdtl.Variance_Consumption_Level_id
                                 AND vcl.VARIANCE_CONSUMPTION_LEVEL_ID = @Consumption_Level_Id )
                        ON vrdtl.Variance_Rule_Id = vr.Variance_Rule_Id
                           AND ( ( @is_DEO IS NULL )
                                 OR ( vrdtl.Is_Data_Entry_Only = @is_DEO ) )
            INNER JOIN dbo.Variance_Parameter_Baseline_Map vpbmap
                  ON vr.Variance_Baseline_id = vpbmap.Variance_Baseline_id
            INNER JOIN dbo.Variance_Parameter vp
                  ON vp.Variance_Parameter_id = vpbmap.Variance_Parameter_id
            INNER JOIN dbo.Code cd
                  ON cd.code_id = vp.Variance_Category_cd
            INNER JOIN dbo.Code cdo
                  ON cdo.code_id = vr.Operator_cd
            INNER JOIN dbo.Code cdb
                  ON cdb.code_id = vpbmap.Baseline_cd
      WHERE
            cmap.COMMODITY_ID = @Commodity_Id
            AND ( @Variance_Category_Cd IS NULL
                  OR vp.Variance_Category_Cd = @Variance_Category_Cd )
      ORDER BY
            vt.variance_test_id
           ,vr.Variance_Rule_Id         
          
END 

;
GO

GRANT EXECUTE ON  [dbo].[Consumption_Level_Variance_Test_SEL] TO [CBMSApplication]
GO
