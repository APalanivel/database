SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.CREATE_DEAL_TICKET_HEDGE_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@dealTicketId  	int       	          	
	@hedgeName     	varchar(200)	          	
	@hedgeDealType 	int       	          	
	@hedgeDealName 	varchar(200)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	@hedgeId       	int       	          	

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

--exec DBO.CREATE_DEAL_TICKET_HEDGE_INFO_P '-1', '-1', 103364, '103364', 175, 'NYMEX', 4

CREATE       PROCEDURE DBO.CREATE_DEAL_TICKET_HEDGE_INFO_P 

@userId varchar(10),
@sessionId varchar(20),
@dealTicketId int, 
@hedgeName varchar(200), 
@hedgeDealType int, 
@hedgeDealName varchar(200), 
@hedgeId int output

AS
set nocount on
	DECLARE @currencyUnitId int, @contractId int, @hedgeUnitTypeId int, @dealTypeId int
	DECLARE @orderConfirmationDate datetime, @isInputByClient bit
	DECLARE @transactionByTypeId int 
	DECLARE @hedgeIdCnt int
	
	SELECT 	@currencyUnitId = CURRENCY_TYPE_ID, 
		@contractId = CONTRACT_ID, 
		@hedgeUnitTypeId = UNIT_TYPE_ID, 
		@orderConfirmationDate = VERBAL_CONFIRMATION_DATE, 
		@isInputByClient = IS_INPUT_BY_CLIENT
	FROM 	RM_DEAL_TICKET 
	WHERE 	RM_DEAL_TICKET_ID = @dealTicketId

	-- added line to fix BZ4637
	IF @orderConfirmationDate is null 
		BEGIN
			select @orderConfirmationDate = getDate()
		END
	
	SELECT 	@dealTypeId = ENTITY_ID FROM ENTITY 
	WHERE 	ENTITY_TYPE = @hedgeDealType AND ENTITY_NAME = @hedgeDealName	

	IF @isInputByClient > 0 
		BEGIN
			SELECT @transactionByTypeId = ENTITY_ID FROM ENTITY 
			WHERE ENTITY_TYPE = 163 AND ENTITY_NAME = 'Client'
		END
	ELSE
		BEGIN
			SELECT @transactionByTypeId = ENTITY_ID FROM ENTITY 
			WHERE ENTITY_TYPE = 163 AND ENTITY_NAME = 'Summit'
		END
	
	SELECT @hedgeIdCnt = COUNT(1) FROM HEDGE 
	WHERE CONTRACT_ID = @contractId AND HEDGE_NAME = @hedgeName
	
	--if hedge table already has entry related to the contract and DT,
	--then just fetch the hedge ID
	IF @hedgeIdCnt <> 0
		BEGIN
			--print ' in the IF ' + @hedgeIdCnt
			SELECT 	@hedgeId = HEDGE_ID FROM HEDGE 
			WHERE 	CONTRACT_ID = @contractId AND 
				HEDGE_NAME  = @hedgeName


		END
	ELSE
		BEGIN
			print ' in the ELSE'

			INSERT INTO HEDGE 
				(CURRENCY_UNIT_ID, CONTRACT_ID, HEDGE_UNIT_TYPE_ID, HEDGE_NAME, DEAL_TYPE_ID, 
				HEDGE_TRANSACTION_DATE, TRANSACTION_BY_TYPE_ID)
			VALUES
				(@currencyUnitId, @contractId, @hedgeUnitTypeId, @hedgeName, @dealTypeId, 
				@orderConfirmationDate, @transactionByTypeId)
			SELECT @hedgeId =  @@identity



		END
	
	--return all output parameters
	RETURN
GO
GRANT EXECUTE ON  [dbo].[CREATE_DEAL_TICKET_HEDGE_INFO_P] TO [CBMSApplication]
GO
