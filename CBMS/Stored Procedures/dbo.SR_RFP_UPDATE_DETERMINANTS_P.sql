SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.SR_RFP_UPDATE_DETERMINANTS_P
	@user_id varchar(10),
	@session_id varchar(20),
	@commodity_type_id int,
	@vendor_id int,
	@determinant_id int,
	@determinant varchar(100),
	@determinant_unit_type_id int,
	@is_checked bit
	
	AS
	set nocount on
declare @setup_id int

select	@setup_id = sr_load_profile_default_setup_id 
from 	sr_load_profile_default_setup(nolock) 
where 	vendor_id = @vendor_id  
	and commodity_type_id = @commodity_type_id

if(@determinant_id > 0)
begin
	update	sr_load_profile_determinant 
	set 	determinant_name = @determinant,
		determinant_unit_type_id = @determinant_unit_type_id,
		is_checked = @is_checked
	where 	sr_load_profile_determinant_id = @determinant_id
		and sr_load_profile_default_setup_id = @setup_id
end
else
begin
	insert into sr_load_profile_determinant (sr_load_profile_default_setup_id, determinant_name, determinant_unit_type_id, is_checked)
					values(@setup_id, @determinant, @determinant_unit_type_id, @is_checked)  
								
end
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_UPDATE_DETERMINANTS_P] TO [CBMSApplication]
GO
