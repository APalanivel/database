SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:    [Workflow].[Get_Exception_Details_User_Preferenced_Columns]  
DESCRIPTION: it will return the User Preferenced Columns list in Gird   
------------------------------------------------------------     
 INPUT PARAMETERS:      
 Name  DataType  Default Description      
 @User_id int,    
 @Module_Id int  
------------------------------------------------------------      
 OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
 USAGE EXAMPLES:      
 DECLARE @return_value int    
    
EXEC @return_value = [Workflow].[Get_Exception_Details_User_Preferenced_Columns]    
  @User_id = 49,    
  @Module_Id = 1    
    
SELECT 'Return Value' = @return_value    
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials Name      
AKP   Arunkumar  
------------------------------------------------------------      
AKP   Arunkumar Summit Energy   
   
 MODIFICATIONS       
 Initials Date   Modification      
------------------------------------------------------------      
 AKR    Aug 29,2019 Created  
  
******/  
CREATE PROCEDURE [Workflow].[Get_Exception_Details_User_Preferenced_Columns]           
  -- Add the parameters for the stored procedure here           
   @User_id int,          
   @Module_Id int          
              
          
AS           
  BEGIN -- SET NOCOUNT ON added to prevent extra result sets from           
      -- interfering with SELECT statements.           
          
          
   SET NOCOUNT ON           
          
      DECLARE @Proc_name     VARCHAR(100) = 'Get_Exception_Details_User_Preferenced_Columns',           
              @Input_Params  VARCHAR (1000),           
              @Error_Line    INT,           
              @Error_Message VARCHAR(3000),          
     @sql nvarchar(max)           
          
     BEGIN TRY           

          
     ;with cte   as (select           
       wqoc.Workflow_Queue_Output_Column_Id ,          
        wqupc.Is_Active          
      from   WORKFLOW.Workflow_Queue_Output_Column WQOC          
    left join  workflow.Workflow_Queue_User_Preference_Column WQUPC          
       on WQUPC.Workflow_Queue_Output_Column_Id = WQOC.Workflow_Queue_Output_Column_Id           
       and   WQUPC.User_Info_Id = @User_id              
    WHERE WQOC.Workflow_Queue_Id =@Module_Id           
    and wqoc.is_fixed =0       
 AND WQOC.Is_Visible_To_User <> 0),          
    cte1 as (          
          
  select wqo.Workflow_Queue_Output_Column_Id ,           
  wqo.Display_Column_Name,          
  wqo.HyperLink_URL,          
  wqo.is_default,          
  wqo.Header_CSS,          
  wqo.Row_CSS,          
  wqo.Allow_Sorting,      
  wqo.Target_Server,     
  wqo.Is_Visible_To_User,     
  wqo.Grid_Label,   
  wqo.Data_Format,    
  0 as PermenantColumn,           
  case when cte.Is_Active =1 then 1          
  else 0 end AS IsSelected ,          
  wqo.Display_Order,      
  (SELECT c.Code_Dsc FROM dbo.Code AS C  WHERE c.Code_Id = wqo.link_type_cd) AS Link_Type FROM cte          
  join workflow.Workflow_Queue_Output_Column wqo           
  on cte.Workflow_Queue_Output_Column_Id = wqo.Workflow_Queue_Output_Column_Id       
  WHERE WQO.Is_Visible_To_User <> 0         
          
  union          
          
  select wqo.Workflow_Queue_Output_Column_Id ,           
  wqo.Display_Column_Name,          
  wqo.HyperLink_URL,          
  wqo.is_default,          
  wqo.Header_CSS,          
  wqo.Row_CSS,         
    wqo.Allow_Sorting,     
 wqo.Target_Server,    
 wqo.Is_Visible_To_User,    
 wqo.Grid_Label,  
  wqo.Data_Format,        
  1 as PermenantColumn,           
  0 IsSelected ,          
  wqo.Display_Order,      
   (SELECT c.Code_Dsc FROM dbo.Code AS C         
WHERE c.Code_Id = wqo.link_type_cd) AS Link_Type from  workflow.Workflow_Queue_Output_Column wqo           
  where is_fixed = 1           
  and wqo.Workflow_Queue_Id = @Module_Id      
  AND    WQO.Is_Visible_To_User <> 0  )          
          
  select Workflow_Queue_Output_Column_Id ,           
 Display_Column_Name,          
 HyperLink_URL,          
 is_default,          
 Header_CSS,          
 Row_CSS,         
 Allow_Sorting,    
 Target_Server,       
 Is_Visible_To_User,      
 Grid_Label,  
 Data_Format,     
 PermenantColumn,           
    IsSelected,      
 cte1.Link_Type  
   from cte1           
  order by cte1.Display_Order           
            
       END try           
          
      BEGIN catch           
          
          
   -- Entry made to the logging SP to capture the errors.          
          SELECT @ERROR_LINE = Error_line(),           
                 @ERROR_MESSAGE = Error_message()           
          
          INSERT INTO storedproc_error_log           
                      (storedproc_name,           
                       error_line,           
                       error_message,           
                       input_params)           
          VALUES      ( @PROC_NAME,           
                        @ERROR_LINE,           
            @ERROR_MESSAGE,           
                        @INPUT_PARAMS )           
          
      return -99          
      END catch     
  END           
                   
         
  
GO
GRANT EXECUTE ON  [Workflow].[Get_Exception_Details_User_Preferenced_Columns] TO [CBMSApplication]
GO
