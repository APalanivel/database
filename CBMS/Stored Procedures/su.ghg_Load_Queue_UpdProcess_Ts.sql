SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	su.ghg_Load_Queue_UpdProcess_Ts


DESCRIPTION:
	Updates the ghg_Load_Queue.Process_ts for the ghg_Load_Queue_Id

INPUT PARAMETERS:
	Name						DataType		Default	Description
------------------------------------------------------------
    @ghg_Load_Queue_Id 	    Varcahr(25)					-- Record to update
    ,@Process_Ts			DateTime	 = null			-- value to update. If it is null then the current timestamp is used

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
DECLARE 
	@in_Client_Commodity_Id	INT
	,@in_CBMS_Image_Id INT
	,@ghg_Load_Queue_Id INT
	,@in_ghg_Load_Queue_Id INT

SELECT top 1 @in_Client_Commodity_Id = Client_Commodity_Id FROM Core.Client_Commodity
SELECT TOP 1 @in_CBMS_Image_Id = CBMS_Image_Id FROM cbms_Image
BEGIN TRANSACTION 


	EXEC su.ghg_Load_Queue_ins 
		@Client_Commodity_id		= @in_Client_Commodity_Id
		,@Load_Source_CodeValue		= 'NightlyBatch'
		,@Add_User_Info_Id			= -1	
		,@CBMS_Image_Id				= NULL
		,@out_GHG_Load_Queue_Id		= @GHG_Load_Queue_Id OUT
		
	EXEC su.ghg_Load_Queue_ins 
		@Client_Commodity_id		= @in_Client_Commodity_Id
		,@Load_Source_CodeValue		= 'ClientUpload'
		,@Add_User_Info_Id			= -1	
		,@CBMS_Image_Id				= @in_CBMS_Image_Id
		,@out_GHG_Load_Queue_Id		= @GHG_Load_Queue_Id OUT
		

	EXEC su.ghg_Load_Queue_ins 
		@Client_Commodity_id		= @in_Client_Commodity_Id
		,@Load_Source_CodeValue		= 'ClientOnboard'
		,@Add_User_Info_Id			= -1	
		,@CBMS_Image_Id				= NULL
		,@out_GHG_Load_Queue_Id		= @GHG_Load_Queue_Id OUT

	
	SELECT top 1  
		@in_ghg_Load_Queue_Id = ghg_Load_Queue_id 
	FROM su.ghg_Load_Queue
	WHERE Process_Ts is null
	ORDER BY Add_Ts
	
	EXEC su.ghg_Load_Queue_UpdProcess_Ts
		@ghg_Load_Queue_Id  = @in_ghg_Load_Queue_Id
		
	Select * 
	FROM su.ghg_Load_Queue
	WHERE Process_Ts is not null
	
	SELECT top 1  
		@in_ghg_Load_Queue_Id = ghg_Load_Queue_id 
	FROM su.ghg_Load_Queue
	WHERE Process_Ts is null
	ORDER BY Add_Ts
		
	EXEC su.ghg_Load_Queue_UpdProcess_Ts
		@ghg_Load_Queue_Id  = @in_ghg_Load_Queue_Id
		,@Process_Ts = '01/01/2000' 
		
	Select * 
	FROM su.ghg_Load_Queue
	WHERE Process_Ts is not null

ROLLBACK TRANSACTION 

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------


MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
Create Procedure su.ghg_Load_Queue_UpdProcess_Ts
(	
	@ghg_Load_Queue_Id 	    Varchar(25)					-- Record to update
    ,@Process_Ts			DateTime	 = null			-- value to update. If it is null then the current timestamp is used
)
AS 

BEGIN 
	SET NOCOUNT ON 
	
	UPDATE 
		su.ghg_Load_Queue
	SET 
		Process_ts = isnull(@Process_Ts, GetDAte())
	WHERE 
		ghg_Load_Queue_Id = @ghg_Load_Queue_Id

END

GO
GRANT EXECUTE ON  [su].[ghg_Load_Queue_UpdProcess_Ts] TO [CBMSApplication]
GO
