SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Report_AltServices_Missing_Determinant
	
DESCRIPTION:
	Finding invoices for accounts that have alternate fuel commodities where there is no determinant for that alternate commodity.
	
INPUT PARAMETERS:
Name				DataType		Default	Description
-------------------------------------------------------------------------------------------
 @CLIENT_Id_LIST	VARCHAR(MAX)
 @Start_date		DATETIME
 @End_date			DATETIME
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------
Report_AltServices_Missing_Determinant '10092,10069,11231','01/01/2008','08/01/2010'
	
AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------
SSR			Sharad srivastava

MODIFICATIONS
Initials	Date		Modification
------------------------------------------------------------
 SSR	   08/10/2010	Created
*/
CREATE PROC [dbo].[Report_AltServices_Missing_Determinant]
    @CLIENT_Id_LIST VARCHAR(MAX)
  , @Start_date DATETIME
  , @End_date DATETIME
AS 
    BEGIN

        SET NOCOUNT ON


        DECLARE @Tmp_tbl AS TABLE
            (
              CLIENT_ID INT
            , Client_NAME VARCHAR(200)
            , SiteName VARCHAR(200)
            , Account_id INT
            , AccountNumber VARCHAR(50)
            , Commodity_ID INT
            , Commodity_name VARCHAR(200)
            , State_Name VARCHAR(200)
            , NumberCommodities INT
            )
        INSERT INTO
            @Tmp_tbl
            SELECT
                c.Client_ID
              , c.CLIENT_NAME
              , s.site_name
              , a.ACCOUNT_ID
              , a.Account_number
              , r.commodity_type_id
              , com.Commodity_name
              , STATE_NAME
              , COUNT(1) OVER ( PARTITION BY a.Account_id )
            FROM
                CLIENT c
                JOIN dbo.ufn_split(@CLIENT_Id_LIST, ',') ufn_User_tmp
                    ON CAST(ufn_User_tmp.Segments AS INT) = c.CLIENT_ID
                JOIN SITE s
                    ON c.CLIENT_ID = s.Client_ID
                JOIN account a
                    ON s.SITE_ID = a.SITE_ID
                JOIN meter m
                    ON m.account_id = a.account_id
                JOIN rate r
                    ON r.rate_id = m.rate_id
                JOIN ADDRESS addr
                    ON addr.ADDRESS_ID = s.PRIMARY_ADDRESS_ID
                JOIN STATE st
                    ON st.STATE_ID = addr.STATE_ID
                JOIN Commodity com
                    ON com.Commodity_Id = r.COMMODITY_TYPE_ID
        SELECT
            tbl.CLIENT_NAME [Client]
          , tbl.SiteName [Site Name]
          , cism.cu_invoice_id [CU Invoice ID]
          , cism.SERVICE_MONTH [Service Month]
          , tbl.AccountNumber [Account Number]
          , tbl.Commodity_Name [Commodity]
          , tbl.STATE_NAME [State]
          , Charges = CASE WHEN cic.CU_INVOICE_ID IS NOT NULL THEN 'Yes'
                           ELSE 'No'
                      END
          , tbl.NumberCommodities
        FROM
            @Tmp_tbl tbl
            JOIN CU_INVOICE_SERVICE_MONTH cism
                ON cism.Account_ID = tbl.Account_id
            JOIN cu_invoice cui
                ON cui.CU_INVOICE_ID = cism.CU_INVOICE_ID
            JOIN CU_INVOICE_EVENT cie_inst
                ON cie_inst.CU_INVOICE_ID = cui.CU_INVOICE_ID
            LEFT JOIN CU_INVOICE_EVENT cie_savesubmit
                ON cie_savesubmit.CU_INVOICE_ID = cui.CU_INVOICE_ID
                   AND cie_savesubmit.EVENT_DESCRIPTION = 'Invoice Saved (Save and Submit)'
            LEFT JOIN ( cu_invoice_charge cic
                        JOIN cu_invoice_charge_account cica
                            ON cica.CU_INVOICE_CHARGE_ID = cic.CU_INVOICE_CHARGE_ID )
                        ON cic.CU_INVOICE_ID = cui.CU_INVOICE_ID
                           AND cic.COMMODITY_TYPE_ID = tbl.Commodity_ID
            LEFT JOIN CU_INVOICE_DETERMINANT cid
                ON cid.CU_INVOICE_ID = cism.CU_INVOICE_ID
                   AND cid.COMMODITY_TYPE_ID = tbl.Commodity_ID
        WHERE
            cui.is_duplicate = 0
            AND cid.CU_INVOICE_ID IS NULL
            AND tbl.Commodity_name NOT IN ( 'Electric Power', 'Natural Gas' )
            AND cie_inst.EVENT_DESCRIPTION LIKE '%inserted%'
            AND cie_inst.EVENT_DATE BETWEEN @Start_date
                                    AND     @end_date  ---- '2010-05-14'
            AND cie_savesubmit.CU_INVOICE_ID IS NULL
   
    
    END


GO
GRANT EXECUTE ON  [dbo].[Report_AltServices_Missing_Determinant] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_AltServices_Missing_Determinant] TO [CBMSApplication]
GO
