SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/**

NAME: dbo.RM_Client_Contact_Sel_By_Client

    
DESCRIPTION:    

      To get client contact details


INPUT PARAMETERS:    
     NAME					DATATYPE		DEFAULT           DESCRIPTION    
------------------------------------------------------------------------    
	@Client_Id				INT		

                    

OUTPUT PARAMETERS:              
      NAME              DATATYPE    DEFAULT           DESCRIPTION       
-------------------------------------------------------------------------              


USAGE EXAMPLES:              
-------------------------------------------------------------------------              
	EXEC dbo.RM_Client_Contact_Sel_By_Client 235
	EXEC dbo.RM_Client_Contact_Sel_By_Client 11236

	 

AUTHOR INITIALS:              
	INITIALS	NAME              
------------------------------------------------------------              
    RR			Raghu Reddy


MODIFICATIONS:    
	INITIALS	DATE		MODIFICATION              
------------------------------------------------------------              
	RR 			31-07-2018	Created - Global Risk Management

**/ 
CREATE PROCEDURE [dbo].[RM_Client_Contact_Sel_By_Client]
      ( 
       @Client_Id INT = NULL
      ,@Deal_Ticket_Id INT = NULL )
AS 
BEGIN
	
      SET NOCOUNT ON;   
      
      SELECT
            @Client_Id = Client_Id
      FROM
            Trade.Deal_Ticket
      WHERE
            @Deal_Ticket_Id IS NOT NULL
            AND Deal_Ticket_Id = @Deal_Ticket_Id

      SELECT
            ccm.Contact_Info_Id
           ,ci.First_Name + ' ' + ci.Last_Name AS NAME
           ,ci.Email_Address
      FROM
            dbo.Client_Contact_Map ccm
            INNER JOIN dbo.Contact_Info ci
                  ON ccm.Contact_Info_Id = ci.Contact_Info_Id
            INNER JOIN dbo.Code cd
                  ON ci.Contact_Type_Cd = cd.Code_Id
            INNER JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'ContactType'
            AND cd.Code_Value = 'RM Client Contact'
            AND ccm.CLIENT_ID = @Client_Id
     
				

END;


GO
GRANT EXECUTE ON  [dbo].[RM_Client_Contact_Sel_By_Client] TO [CBMSApplication]
GO
