SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE dbo.CREATE_CONFIGURATION_MASTER_ID_P

@userId varchar(10),
@sessionId varchar(20)


as
begin
	set nocount on
	DECLARE @currentDate datetime
	SELECT @currentDate=GETDATE()
	
	insert into RM_BATCH_CONFIGURATION_MASTER (creation_date)
	values(@currentDate)
end
GO
GRANT EXECUTE ON  [dbo].[CREATE_CONFIGURATION_MASTER_ID_P] TO [CBMSApplication]
GO
