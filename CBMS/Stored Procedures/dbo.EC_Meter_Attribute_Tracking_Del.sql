
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.EC_Meter_Attribute_Tracking_Del           
              
Description:              
        To delete Data to EC_Meter_Attribute_Tracking table.              
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
    @EC_Meter_Attribute_Tracking_Id		INT
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                
	Declare @EC_Meter_Attribute_Tracking_Id_Input int 
	BEGIN TRAN  
	SELECT * FROM dbo.EC_Meter_Attribute_Tracking WHERE EC_Meter_Attribute_Value='test' and meter_id=1
	EXEC dbo.EC_Meter_Attribute_Tracking_Ins 
       @Meter_Id =1
      ,@EC_Meter_Attribute_Id =2
      ,@Start_Dt ='2015-05-06'
      ,@End_Dt ='2015-06-06'
      ,@EC_Meter_Attribute_Value ='test'
      ,@User_Info_Id =100
      SELECT * FROM dbo.EC_Meter_Attribute_Tracking WHERE EC_Meter_Attribute_Value='test' and meter_id=1
      
	SELECT @EC_Meter_Attribute_Tracking_Id_Input=EC_Meter_Attribute_Tracking_Id FROM dbo.EC_Meter_Attribute_Tracking WHERE EC_Meter_Attribute_Value='test' and meter_id=1
	select @EC_Meter_Attribute_Tracking_Id_Input 
			
	EXEC dbo.EC_Meter_Attribute_Tracking_Del 
      @EC_Meter_Attribute_Tracking_Id = @EC_Meter_Attribute_Tracking_Id_Input
    
	SELECT * FROM EC_Meter_Attribute_Tracking WHERE EC_Meter_Attribute_Tracking_Id=@EC_Meter_Attribute_Tracking_Id_Input
	ROLLBACK TRAN                      
             
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy               
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2015-04-22		Created For AS400.         
             
******/ 
CREATE PROCEDURE [dbo].[EC_Meter_Attribute_Tracking_Del]
      ( 
       @EC_Meter_Attribute_Tracking_Id INT
      ,@User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 
      
      DECLARE @Account_Id INT
           
      
      
     
      SELECT
            @Account_Id = cha.Account_id
      FROM
            core.Client_Hier_Account cha
            INNER JOIN dbo.EC_Meter_Attribute_Tracking emat
                  ON cha.Meter_Id = emat.Meter_Id
      WHERE
            cha.Account_Type = 'Utility'
            AND EC_Meter_Attribute_Tracking_Id = @EC_Meter_Attribute_Tracking_Id
     
      EXEC dbo.ADD_ENTITY_AUDIT_ITEM_P 
            @entity_identifier = @Account_Id
           ,@user_info_id = @User_Info_Id
           ,@audit_type = 3
           ,@entity_name = 'ACCOUNT_TABLE'
           ,@entity_type = 500 
      
      DELETE
            emat
      FROM
            dbo.EC_Meter_Attribute_Tracking emat
      WHERE
            emat.EC_Meter_Attribute_Tracking_Id = @EC_Meter_Attribute_Tracking_Id
      
            
END;

;
GO

GRANT EXECUTE ON  [dbo].[EC_Meter_Attribute_Tracking_Del] TO [CBMSApplication]
GO
