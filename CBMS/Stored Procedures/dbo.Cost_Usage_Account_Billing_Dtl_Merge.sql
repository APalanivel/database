SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
NAME:   
    dbo.Cost_Usage_Account_Billing_Dtl_Merge       

DESCRIPTION:  
	This procedure merge the data in to Cost_Usage_Account_Billing_Dtl based on the given account and service month.
      
INPUT PARAMETERS:          
Name				DataType	Default	Description          
------------------------------------------------------------          
@Account_Id			INT
@Service_Month		DATE
@Billing_Start_Dt	DATE
@Billing_End_Dt		DATE
@Billing_Days		INT

OUTPUT PARAMETERS:          
Name			DataType	Default		Description          
------------------------------------------------------------ 

         
USAGE EXAMPLES:          
------------------------------------------------------------ 
begin transaction

	Exec dbo.Cost_Usage_Account_Billing_Dtl_Merge 
		161031,	'2012-06-01',	'2012-06-01', '2012-06-30',	30

rollback transaction

     

AUTHOR INITIALS:          
Initials	Name          
------------------------------------------------------------          
AP			Athmaram Pabbathi
BCH			Balaraju     
HG			Harihara Suthan G
     
MODIFICATIONS           
Initials	Date		    Modification          
------------------------------------------------------------          
AP			09/14/2011		Created
BCH			2012-04-16		Removed @Commodity_Id parameter.
HG			2012-04-19		Usage example updated
*****/
CREATE PROCEDURE dbo.Cost_Usage_Account_Billing_Dtl_Merge
      ( 
       @Account_Id INT
      ,@Service_Month DATE
      ,@Billing_Start_Dt DATE
      ,@Billing_End_Dt DATE
      ,@Billing_Days INT )
AS 
BEGIN      
      SET NOCOUNT ON ;     

      MERGE INTO dbo.Cost_Usage_Account_Billing_Dtl AS tgt
            USING 
                  ( SELECT
                        @Account_Id AS Account_Id
                       ,@Service_Month AS Service_Month
                       ,@Billing_Start_Dt AS Billing_Start_Dt
                       ,@Billing_End_Dt AS Billing_End_Dt
                       ,@Billing_Days AS Billing_Days ) AS src
            ON tgt.Account_Id = src.Account_Id
                  AND tgt.Service_Month = src.Service_Month
            WHEN MATCHED 
                  THEN UPDATE
                    SET 
                        Billing_Start_Dt = src.Billing_Start_Dt
                       ,Billing_End_Dt = src.Billing_End_Dt
                       ,Billing_Days = src.Billing_Days
            WHEN NOT MATCHED 
                  THEN INSERT
                        ( 
                         Account_Id
                        ,Service_Month
                        ,Billing_Start_Dt
                        ,Billing_End_Dt
                        ,Billing_Days )
                    VALUES
                        ( 
                         src.Account_Id
                        ,src.Service_Month
                        ,src.Billing_Start_Dt
                        ,src.Billing_End_Dt
                        ,src.Billing_Days ) ;
END  

;
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Billing_Dtl_Merge] TO [CBMSApplication]
GO
