SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



--exec dbo.BUDGET_UPDATE_CURRENCY_CONVERSION_FACTOR_P -1,-1,1492,'CAN',3

CREATE       PROCEDURE dbo.BUDGET_UPDATE_CURRENCY_CONVERSION_FACTOR_P
	@user_id varchar(10),
	@session_id varchar(20),
	@budget_id int,
	@currency_symbol varchar(10),
	@conversion_factor decimal(32,16)
	
	AS

	begin
	set nocount on
	declare @unit_id int
	select @unit_id=currency_unit_id from currency_unit where symbol=@currency_symbol 
	declare @record_exists int
	select	@record_exists=count(budget_id) 
	from  budget_currency_map
	where budget_id=@budget_id
	and 
	currency_unit_id=@unit_id
	if(@record_exists = 0)
	begin
		insert into budget_currency_map(budget_id,currency_unit_id,conversion_factor) 
		values( @budget_id,
			@unit_id,
			@conversion_factor);
	end
	else
	begin
		update budget_currency_map
			set --currency_unit_id=@unit_id,
			    conversion_factor=@conversion_factor
			where budget_id=@budget_id and currency_unit_id=@unit_id
	end
	end
			













GO
GRANT EXECUTE ON  [dbo].[BUDGET_UPDATE_CURRENCY_CONVERSION_FACTOR_P] TO [CBMSApplication]
GO
