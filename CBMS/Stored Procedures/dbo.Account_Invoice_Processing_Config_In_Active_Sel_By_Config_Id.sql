SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: dbo.Account_Invoice_Processing_Config_In_Active_Sel_By_Config_Id
     
DESCRIPTION: 
	To Delete Account Processing Instruction configs  for selected config.
      
INPUT PARAMETERS:          
NAME										DATATYPE			DEFAULT			DESCRIPTION          
------------------------------------------------------------------------------------------------------          
@Account_Invoice_Processing_Config_Id		INT				
                
OUTPUT PARAMETERS:          
NAME										DATATYPE			DEFAULT			DESCRIPTION          
------------------------------------------------------------------------------------------------------  
        
USAGE EXAMPLES:          
------------------------------------------------------------------------------------------------------  
  
	BEGIN TRAN

	SELECT * FROM Account_Invoice_Processing_Config WHERE Account_Invoice_Processing_Config_Id =    4 
	SELECT * FROM Account_Invoice_Processing_Config_Log WHERE Account_Invoice_Processing_Config_Id =    4 

		EXEC Account_Invoice_Processing_Config_In_Active_Sel_By_Config_Id  4,49

	SELECT * FROM Account_Invoice_Processing_Config WHERE Account_Invoice_Processing_Config_Id =    4
	SELECT * FROM Account_Invoice_Processing_Config_Log WHERE Account_Invoice_Processing_Config_Id =    4 
 

	ROLLBACK TRAN
	
	
	
	 
	
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------------------------------------------------  
NR			Narayana Reddy
          
MODIFICATIONS           
INITIALS	DATE			MODIFICATION          
------------------------------------------------------------------------------------------------------  
NR			2019-03-01		WatchList - Created	

*/

CREATE PROCEDURE [dbo].[Account_Invoice_Processing_Config_In_Active_Sel_By_Config_Id]
    (
        @Account_Invoice_Processing_Config_Id INT
        , @User_Info_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Is_Watch_List BIT = 0
            , @Field_Name VARCHAR(255)
            , @Instruction_Category VARCHAR(25);



        SELECT
            @Is_Watch_List = 1
        FROM
            Account_Invoice_Processing_Config aipc
        WHERE
            aipc.Account_Invoice_Processing_Config_Id = @Account_Invoice_Processing_Config_Id
            AND aipc.Watch_List_Group_Info_Id IS NOT NULL;



        SELECT
            @Instruction_Category = Cd.Code_Value
        FROM
            dbo.Account_Invoice_Processing_Config aipc
            INNER JOIN dbo.Code Cd
                ON Cd.Code_Id = aipc.Processing_Instruction_Category_Cd
        WHERE
            aipc.Account_Invoice_Processing_Config_Id = @Account_Invoice_Processing_Config_Id;



        SET @Field_Name = CASE WHEN @Is_Watch_List = 1 THEN
                                   @Instruction_Category + N' ' + N'Processing Instructions And Watch List Group'
                              ELSE @Instruction_Category + N' ' + N'Processing Instructions'
                          END;
        BEGIN TRY
            BEGIN TRAN;

            UPDATE
                aipc
            SET
                aipc.Is_Active = 0
                , aipc.Updated_User_Id = @User_Info_Id
                , aipc.Last_Change_Ts = GETDATE()
            FROM
                dbo.Account_Invoice_Processing_Config aipc
            WHERE
                aipc.Account_Invoice_Processing_Config_Id = @Account_Invoice_Processing_Config_Id;


            EXEC dbo.Account_Invoice_Processing_Config_Log_Ins
                @Account_Invoice_Processing_Config_Id = @Account_Invoice_Processing_Config_Id
                , @Field_Name = @Field_Name
                , @Change_Type = 'Deleted'
                , @Previous_Value = NULL
                , @Current_Value = NULL
                , @Is_Updated_Using_Apply_All = 0
                , @Event_By_User_Id = @User_Info_Id;

            COMMIT TRAN;
        END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0
                ROLLBACK TRAN;

            EXEC dbo.usp_RethrowError;

        END CATCH;


    END;



GO
GRANT EXECUTE ON  [dbo].[Account_Invoice_Processing_Config_In_Active_Sel_By_Config_Id] TO [CBMSApplication]
GO
