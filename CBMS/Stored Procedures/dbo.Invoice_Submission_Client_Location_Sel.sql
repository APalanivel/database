SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******************************************************************************************************                        
NAME: dbo.Invoice_Submission_Client_Location_Sel
    
DESCRIPTION:    
    
      Select record from Invoice_Submission_Client_Location_Sel
    
INPUT PARAMETERS:    
      NAME                                      DATATYPE    DEFAULT           DESCRIPTION    
------------------------------------------------------------------------    

      
OUTPUT PARAMETERS:              
      NAME              DATATYPE    DEFAULT           DESCRIPTION 
-----------------------------------------------------------      



------------------------------------------------------------              
USAGE EXAMPLES:              
------------------------------------------------------------            

	Exec Invoice_Submission_Client_Location_Sel
         
AUTHOR INITIALS:              
      INITIALS    NAME              
------------------------------------------------------------              
		HK			Harish Kurma
		      
MODIFICATIONS:    
      INITIALS    DATE			MODIFICATION              
------------------------------------------------------------              
      HK		  10 29 2018	Created
******************************************************************************************************/

CREATE PROCEDURE [dbo].[Invoice_Submission_Client_Location_Sel] 
	(
		@Client_Id INT
	)
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
		isc.Invoice_Submission_Client_Location_Id,
		isc.Client_Id,
		isc.Location_Path,
		isc.Location_Config,
		c.Code_Dsc AS Location_Type,
		isc.User_Name,
		isc.Pass_Code
	FROM
		dbo.Invoice_Submission_Client_Location isc
	JOIN dbo.Code c ON isc.Location_Type_Cd = c.Code_Id
	JOIN dbo.Codeset cs ON c.Codeset_Id = cs.Codeset_Id 
	WHERE
		isc.Client_Id = @Client_Id AND cs.codeset_Name = 'InvoiceSubmissionLocation'
END
GO
GRANT EXECUTE ON  [dbo].[Invoice_Submission_Client_Location_Sel] TO [CBMSApplication]
GO
