SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
 NAME:  dbo.Report_DE_Get_Open_Invoices_By_ClientName              
              
 DESCRIPTION:                
                   
                
 INPUT PARAMETERS:                
               
 Name                         DataType            Default        Description                
---------------------------------------------------------------------------------------------------------------              
 @ClientName      Varchar(200)      
                
 OUTPUT PARAMETERS:                      
 Name                         DataType            Default        Description                
---------------------------------------------------------------------------------------------------------------              
                
 USAGE EXAMPLES:                    
---------------------------------------------------------------------------------------------------------------               
     EXEC   dbo.Report_DE_Get_Open_Invoices_By_ClientName 'marriott'        
            
               
 AUTHOR INITIALS:               
               
 Initials               Name                
---------------------------------------------------------------------------------------------------------------              
 ABK     Aditya Bharadwaj  K  
                 
 MODIFICATIONS:               
                
 Initials               Date            Modification              
---------------------------------------------------------------------------------------------------------------              
 ABK     2019-04-19     Created new SP(REPTMGR-99)    
******/    
    
CREATE PROCEDURE [dbo].[Report_DE_Get_Open_Invoices_By_ClientName]    
    (@ClientName VARCHAR(200))    
AS    
    BEGIN    
    
        SET NOCOUNT ON;    
    
        IF OBJECT_ID('#temp') IS NOT NULL    
            DROP TABLE #temp;    
    
        SELECT  DISTINCT    
                cu.CU_INVOICE_ID                                                          AS CU_INVOICE_ID,    
                ci.CBMS_DOC_ID                                                            AS File_Name,    
                NULL                                                                      AS Insert_Date,    
                q.queue_name                                                              AS QUEUE,    
                CASE    
                    WHEN cuex.QUEUE_ID = '116'    
                        THEN    
                        'Public Queue'    
                    ELSE    
                        dtm.team_name    
                END                                                                       AS Team,    
                cuex.EXCEPTION_TYPE                                                       AS EXCEPTION_TYPE,    
                CASE    
                    WHEN    
                        (    
                            ubm.UBM_NAME = 'Ecova Basic'    
                            OR ubm.UBM_NAME = 'Avista'    
                        )    
                        THEN    
                        'Engie'    
                    ELSE    
                        'Internal Data-Entry'    
                END                                                                       AS Source,    
                ISNULL(ISNULL(a.Account_Number, l.ACCOUNT_NUMBER), cu.UBM_ACCOUNT_NUMBER) AS Account_Number,    
                ISNULL(ISNULL(chc.State_Name, l.STATE_NAME), cu.UBM_STATE_CODE)           AS State_Name,    
                CASE    
                    WHEN et.EXCEPTION_TYPE = 'Mapping Required - Client Code Not Mapped'    
                        THEN    
                        COALESCE(u.UBM_CLIENT_NAME, l.CLIENT_NAME, ch.CLIENT_NAME)    
                    WHEN cism.Account_ID IS NULL    
                        THEN    
                        COALESCE(ch.CLIENT_NAME, u.UBM_CLIENT_NAME, l.CLIENT_NAME)    
                    ELSE    
                        ISNULL(chc.Client_Name, l.CLIENT_NAME)    
                END    AS Client_Name,    
                ISNULL(ISNULL(a.Account_Vendor_Name, l.VENDOR_NAME), cu.UBM_VENDOR_CODE)  AS Vendor_name    
        INTO    
                #temp    
        FROM    
                dbo.CU_EXCEPTION_DENORM      cuex    
            INNER JOIN    
                dbo.CU_INVOICE               cu    
                    ON cuex.CU_INVOICE_ID = cu.CU_INVOICE_ID    
            INNER JOIN    
                dbo.cbms_image               ci    
                    ON ci.CBMS_IMAGE_ID = cu.CBMS_IMAGE_ID    
            LEFT OUTER JOIN    
                dbo.CU_INVOICE_SERVICE_MONTH cism    
                    ON cism.CU_INVOICE_ID = cu.CU_INVOICE_ID    
            LEFT OUTER JOIN    
                Core.Client_Hier_Account     a    
                    ON a.Account_Id = cism.Account_ID    
            LEFT OUTER JOIN    
                Core.Client_Hier             chc    
                    ON chc.Client_Hier_Id = a.Client_Hier_Id    
            LEFT OUTER JOIN    
                dbo.CLIENT                   ch    
                    ON cu.CLIENT_ID = ch.CLIENT_ID    
            LEFT OUTER JOIN    
                dbo.CU_EXCEPTION             ce    
                    ON ce.CU_INVOICE_ID = cu.CU_INVOICE_ID    
            JOIN    
                dbo.CU_EXCEPTION_DETAIL      ed    
                    ON ed.CU_EXCEPTION_ID = ce.CU_EXCEPTION_ID    
            JOIN    
                dbo.CU_EXCEPTION_TYPE        et    
                    ON et.CU_EXCEPTION_TYPE_ID = ed.EXCEPTION_TYPE_ID    
            LEFT OUTER JOIN    
                dbo.UBM_INVOICE              u    
                    ON u.UBM_INVOICE_ID = cu.UBM_INVOICE_ID    
            LEFT OUTER JOIN    
                dbo.CU_INVOICE_LABEL         l    
                    ON l.CU_INVOICE_ID = ce.CU_INVOICE_ID    
            LEFT OUTER JOIN    
                dbo.vwCbmsQueueName          q    
                    ON q.queue_id = ce.QUEUE_ID    
            LEFT JOIN    
                dbo.USER_INFO                ui    
                    ON ui.QUEUE_ID = cuex.QUEUE_ID    
            LEFT JOIN    
                REPTMGR.dbo.data_team_map_JMM        dtm    
                    ON dtm.USER_INFO_ID = ui.USER_INFO_ID    
            LEFT JOIN    
                dbo.UBM                      ubm    
                    ON ubm.UBM_ID = cu.UBM_ID    
        WHERE    
                (CASE    
                     WHEN et.EXCEPTION_TYPE = 'Mapping Required - Client Code Not Mapped'    
                         THEN    
                         COALESCE(u.UBM_CLIENT_NAME, l.CLIENT_NAME, ch.CLIENT_NAME)    
                     WHEN cism.Account_ID IS NULL    
                         THEN    
                         COALESCE(ch.CLIENT_NAME, u.UBM_CLIENT_NAME, l.CLIENT_NAME)    
                     ELSE    
                         ISNULL(chc.Client_Name, l.CLIENT_NAME)    
                 END    
                ) LIKE '%' + @ClientName + '%'    
                AND cu.IS_PROCESSED = 0    
                AND cu.IS_DNT = 0    
                AND cu.IS_DUPLICATE = 0;    
    
    
        SELECT    
                t.CU_INVOICE_ID,    
                t.File_Name,    
                CAST(cie.EVENT_DATE AS DATE) AS Insert_Date,    
                t.QUEUE,    
                t.Team,    
                t.EXCEPTION_TYPE,    
                t.Source,    
                t.Account_Number,    
                t.State_Name,    
                t.Client_Name,    
                t.Vendor_name    
        FROM    
                #temp                t    
            LEFT JOIN    
                dbo.CU_INVOICE_EVENT cie    
                    ON cie.CU_INVOICE_ID = t.CU_INVOICE_ID    
        WHERE    
                cie.EVENT_DESCRIPTION LIKE '%INSERTED%'    
                OR cie.EVENT_DESCRIPTION LIKE '%COPIED FROM%';    
    
    END;    
    
    
    
    
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Get_Open_Invoices_By_ClientName] TO [CBMS_SSRS_Reports]
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Get_Open_Invoices_By_ClientName] TO [CBMSApplication]
GO
