SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE dbo.SELECT_RATE_ID_NAMES_P
	@vendorId INT,
	@commodityTypeId INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT rate_id,
	   rate_name 
	FROM dbo.rate   
	WHERE vendor_id = @vendorId 
		AND commodity_type_id = @commodityTypeId  
	ORDER BY rate_name

END




GO
GRANT EXECUTE ON  [dbo].[SELECT_RATE_ID_NAMES_P] TO [CBMSApplication]
GO
