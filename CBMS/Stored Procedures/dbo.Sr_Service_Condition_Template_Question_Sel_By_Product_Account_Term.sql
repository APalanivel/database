
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Service_Condition_Template_Question_Sel_By_Product_Account_Term]
           
DESCRIPTION:             
			To get service condition template details 
			
INPUT PARAMETERS:            
	Name								DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Sr_Service_Condition_Template_Id	INT
    @Language_CD						INT			NULL
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
	EXEC dbo.Sr_Service_Condition_Template_Question_Sel_By_Product_Account_Term  1211471 ,109834 
	EXEC dbo.Sr_Service_Condition_Template_Question_Sel_By_Product_Account_Term  1211874 ,109977 
	EXEC dbo.Sr_Service_Condition_Template_Question_Sel_By_Product_Account_Term  1211875 ,109977
	EXEC dbo.Sr_Service_Condition_Template_Question_Sel_By_Product_Account_Term  1211685, 110635
	
	EXEC dbo.Sr_Service_Condition_Template_Question_Sel_By_Product_Account_Term 1212164,112285,100158
	EXEC dbo.Sr_Service_Condition_Template_Question_Sel_By_Product_Account_Term 1212164,112286,100158
	
	EXEC dbo.Sr_Service_Condition_Template_Question_Sel_By_Product_Account_Term 1218315,141411,100158
	EXEC dbo.Sr_Service_Condition_Template_Question_Sel_By_Product_Account_Term 1218315,141412,100158
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-03-02	Global Sourcing - Phase3 - GCS-478 Created
	RR			2017-11-29	MAINT-6008 Modified to load response options in default English(en-US, same as question and comment labels) if the 
							user selected locale values are not available
******/
CREATE PROCEDURE [dbo].[Sr_Service_Condition_Template_Question_Sel_By_Product_Account_Term]
      ( 
       @Selected_Pricing_Product_Id INT
      ,@SR_RFP_ACCOUNT_TERM_ID INT
      ,@Language_CD INT = NULL )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Acc_Country_Commodity AS TABLE
            ( 
             Country_Id INT
            ,Commodity_Id INT )
      
      SELECT
            @Language_CD = ISNULL(@Language_CD, cd.Code_Id)
      FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cd.Code_Value = 'en-US'
            AND cs.Codeset_Name = 'LocalizationLanguage';
            
            
      WITH  Cte_Qtns
              AS ( SELECT
                        sscc.Sr_Service_Condition_Category_Id
                       ,ISNULL(sscclv.Category_Name_Locale_Value, sscc.Category_Name) AS Category_Name_Locale_Value
                       ,ISNULL(sscqlv.Question_Label_Locale_Value, sscq.Question_Label) AS Question_Label_Locale_Value
                       ,sscq.Is_Comment_Shown
                       ,ISNULL(sscqlv.Comment_Label_Locale_Value, sscq.Comment_Label) AS Comment_Label_Locale_Value
                       ,cd.Code_Value AS Response_Type
                       ,rfptqm.Category_Display_Seq
                       ,rfptqm.Question_Display_Seq
                       ,rfptqm.Sr_Service_Condition_Question_Id
                       ,rfptqm.Is_Post_To_Supplier
                       ,rfptqm.Sr_Rfp_Service_Condition_Template_Question_Map_Id
                       ,sscq.Is_Response_Required
                   FROM
                        dbo.Sr_Rfp_Service_Condition_Template_Question_Map rfptqm
                        INNER JOIN dbo.Sr_Service_Condition_Question sscq
                              ON rfptqm.Sr_Service_Condition_Question_Id = sscq.Sr_Service_Condition_Question_Id
                        INNER JOIN dbo.Code cd
                              ON cd.Code_Id = sscq.Response_Type_Cd
                        LEFT JOIN dbo.Sr_Service_Condition_Question_Locale_Value sscqlv
                              ON sscq.Sr_Service_Condition_Question_Id = sscqlv.Sr_Service_Condition_Question_Id
                                 AND sscqlv.Language_Cd = @Language_CD
                        INNER JOIN dbo.Sr_Service_Condition_Category sscc
                              ON rfptqm.Sr_Service_Condition_Category_Id = sscc.Sr_Service_Condition_Category_Id
                        LEFT JOIN dbo.Sr_Service_Condition_Category_Locale_Value sscclv
                              ON sscc.Sr_Service_Condition_Category_Id = sscclv.Sr_Service_Condition_Category_Id
                                 AND sscclv.Language_Cd = @Language_CD
                   WHERE
                        rfptqm.SR_RFP_ACCOUNT_TERM_ID = @SR_RFP_ACCOUNT_TERM_ID
                        AND rfptqm.SR_RFP_SELECTED_PRODUCTS_ID = @Selected_Pricing_Product_Id),
            Cte_SSCQ
              AS ( SELECT
                        NULL AS Sr_Service_Condition_Template_Id
                       ,NULL AS Template_Name
                       ,NULL AS Is_Required
                       ,qtns.Sr_Service_Condition_Category_Id
                       ,qtns.Category_Name_Locale_Value
                       ,NULL AS Sr_Service_Condition_Template_Question_Map_Id
                       ,qtns.Question_Label_Locale_Value
                       ,qtns.Is_Comment_Shown
                       ,qtns.Comment_Label_Locale_Value
                       ,qtns.Response_Type
                       ,qtns.Category_Display_Seq
                       ,qtns.Question_Display_Seq
                       ,qtns.Sr_Service_Condition_Question_Id
                       ,ISNULL(Response_Controls_Cnt, 1) AS Response_Controls_Cnt
                       ,LEFT(resp.Options, LEN(resp.Options) - 1) AS Response_Options
                       ,'Response_Option_Id^Option_Value^Is_Comment_Required^Display_Seq' AS Response_Options_Field_Dtls
                       ,NULL AS RFP_Sr_Service_Condition_Template_Question_Map_Id
                       ,'' AS Response_Text_Value
                       ,'' AS Comment
                       ,qtns.Is_Response_Required
                       ,qtns.Is_Post_To_Supplier
                   FROM
                        Cte_Qtns qtns
                        LEFT JOIN ( SELECT
                                          COUNT(sscro.Sr_Service_Condition_Response_Option_Id) AS Response_Controls_Cnt
                                         ,sscro.Sr_Service_Condition_Question_Id
                                    FROM
                                          dbo.Sr_Service_Condition_Response_Option sscro
                                    GROUP BY
                                          sscro.Sr_Service_Condition_Question_Id ) Cnt
                              ON qtns.Sr_Service_Condition_Question_Id = Cnt.Sr_Service_Condition_Question_Id
                        CROSS APPLY ( SELECT
                                          CAST(sscro.Sr_Service_Condition_Response_Option_Id AS VARCHAR(10)) + '^' + ISNULL(sscrlv.Option_Locale_Value, sscro.Option_Value) + '^' + CAST(sscro.Is_Comment_Required AS VARCHAR(10)) + '^' + CAST(sscro.Display_Seq AS VARCHAR(10)) + ','
                                      FROM
                                          dbo.Sr_Service_Condition_Response_Option sscro
                                          LEFT JOIN dbo.Sr_Service_Condition_Response_Option_Locale_Value sscrlv
                                                ON sscro.Sr_Service_Condition_Response_Option_Id = sscrlv.Sr_Service_Condition_Response_Option_Id
                                                   AND sscrlv.Language_Cd = @Language_CD
                                      WHERE
                                          sscro.Sr_Service_Condition_Question_Id = qtns.Sr_Service_Condition_Question_Id
                                      GROUP BY
                                          CAST(sscro.Sr_Service_Condition_Response_Option_Id AS VARCHAR(10)) + '^' + ISNULL(sscrlv.Option_Locale_Value, sscro.Option_Value) + '^' + CAST(sscro.Is_Comment_Required AS VARCHAR(10)) + '^' + CAST(sscro.Display_Seq AS VARCHAR(10))
                                         ,sscro.Display_Seq
                                      ORDER BY
                                          sscro.Display_Seq
                        FOR
                                      XML PATH('') ) resp ( Options ))
            SELECT
                  Sr_Service_Condition_Template_Id
                 ,Template_Name
                 ,Is_Required
                 ,Sr_Service_Condition_Category_Id
                 ,Category_Name_Locale_Value
                 ,Sr_Service_Condition_Template_Question_Map_Id
                 ,Question_Label_Locale_Value
                 ,Is_Comment_Shown
                 ,Comment_Label_Locale_Value
                 ,Response_Type
                 ,Category_Display_Seq
                 ,Question_Display_Seq
                 ,Sr_Service_Condition_Question_Id
                 ,Response_Controls_Cnt
                 ,Response_Options
                 ,Response_Options_Field_Dtls
                 ,RFP_Sr_Service_Condition_Template_Question_Map_Id
                 ,Response_Text_Value
                 ,Comment
                 ,Is_Response_Required
                 ,Is_Post_To_Supplier
                 ,Controls.Cnt AS Controls_Count
            FROM
                  Cte_SSCQ sscq
                  CROSS APPLY ( SELECT
                                    MAX(Response_Controls_Cnt) + MAX(CAST(Is_Comment_Shown AS INT)) AS Cnt
                                FROM
                                    Cte_SSCQ ) Controls
            
END;




;
GO


GRANT EXECUTE ON  [dbo].[Sr_Service_Condition_Template_Question_Sel_By_Product_Account_Term] TO [CBMSApplication]
GO
