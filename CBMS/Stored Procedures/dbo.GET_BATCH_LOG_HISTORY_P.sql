SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_BATCH_LOG_HISTORY_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@batchId       	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE   PROCEDURE dbo.GET_BATCH_LOG_HISTORY_P 
@userId varchar(10),
@sessionId varchar(20),
@batchId int
AS
IF @batchId > 0

begin
	select 
		batch.rm_report_batch_id,
		count(batchLog.rm_report_batch_log_id)created_reports, 
		batch.batch_start_time, 
		batch.batch_end_time
	from 
		rm_report_batch_log batchLog, rm_report_batch batch
	where 
		batchLog.rm_report_batch_id = batch.rm_report_batch_id
		and batch.rm_report_batch_id = @batchId
	group by 
		batch.rm_report_batch_id,
		batch.batch_start_time, 
		batch.batch_end_time

	order by batch.batch_start_time desc
end

ELSE

begin 
	select 
		batch.rm_report_batch_id,
		count(batchLog.rm_report_batch_log_id)created_reports, 
		batch.batch_start_time, 
		batch.batch_end_time
	from 
		rm_report_batch_log batchLog, rm_report_batch batch
	where 
		batchLog.rm_report_batch_id = batch.rm_report_batch_id
	group by 
		batch.rm_report_batch_id,
		batch.batch_start_time, 
		batch.batch_end_time

	order by batch.batch_start_time desc

end
GO
GRANT EXECUTE ON  [dbo].[GET_BATCH_LOG_HISTORY_P] TO [CBMSApplication]
GO
