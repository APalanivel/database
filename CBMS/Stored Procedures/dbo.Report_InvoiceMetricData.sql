SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


    


CREATE Proc [dbo].[Report_InvoiceMetricData]
/*********        
NAME:        
  dbo.Report_InvoiceMetricData       
  
DESCRIPTION:   
	Used to pull Metric reporting data for Jeff Floyd/Adam McKinney's team
	Shows invoices that were inserted during specified period of time
	Shows how long to Close/Resolve to Month
	Shows any exceptions   
	Non Client Specific   
  
INPUT PARAMETERS:      
      Name              DataType          Default     Description      
------------------------------------------------------------      
@begindate				datetime      
@enddate				datetime     


OUTPUT PARAMETERS:
      Name              DataType          Default     Description
------------------------------------------------------------ 

USAGE EXAMPLES:
------------------------------------------------------------
Stress
exec dbo.Report_InvoiceMetricData '2010-01-16', '2010-02-01'-- 27 seconds
exec dbo.Report_InvoiceMetricData '2010-01-01', '2010-01-16' --22 seconds

AUTHOR INITIALS:
 
Initials Name
------------------------------------------------------------
LC Lynn Cox

Initials Date  Modification        
------------------------------------------------------------   
*/ 

(
@begindate datetime
,@enddate datetime
)

As 
Begin
Set NoCount On;


Declare @InvInserted table
(Cu_Invoice_ID INT Primary Key (Cu_Invoice_Id)
,InsertEventDate datetime
,Inserted varchar(4000)
,UserName varchar(30)
,Is_Processed bit 
,Is_Reported bit
,Is_Manual bit)

Insert @InvInserted 
(Cu_Invoice_Id
,InsertEventDate
,Inserted
,UserName
,Is_Processed
,Is_Reported
,Is_Manual
)
Select 
e.Cu_Invoice_Id
,e.event_date InsertEventDate
,e.event_description Inserted
,ui.username UserName
,is_processed IsProcessed
,is_reported IsReported
,is_manual IsManual
from CU_INVOICE_EVENT e
	join Cu_Invoice cui on cui.cu_Invoice_id = e.cu_Invoice_Id
	join user_info ui on ui.user_info_id = e.event_by_id
	where 
	--e.event_date between '2010-01-16' and '2010-02-01'
	--and 
	e.event_date between @begindate and @enddate
	AND e.event_description like '%insert%'
	and cui.is_dnt <> '1'
	and cui.is_duplicate <>'1'
group by e.Cu_Invoice_Id
,e.event_date
,e.event_description
,ui.username
,is_processed
,is_reported
,is_manual;

With
cte_ResToMonth
  as
(
select cue.cu_invoice_id 
			 ,event_description ResolvedToMonth
			 ,event_date ResolvedToMonthEventDate
		from cu_invoice_event cue 
		where (cue.event_description like '%passed resolve to month%'or
cue.event_description like '%resolved to month%')
group by cue.cu_invoice_id,event_description,event_date

)
,
cte_InvClosed as
(
select distinct cue.cu_invoice_id 
			,event_description ClosedManual
			,event_date ClosedManualDate
		from  cu_invoice_event cue 
		where event_description ='Invoice Closed via Manual Close/Stop Processing'
		group by cue.cu_invoice_id,event_description,event_date
)

	Select
	 ii.Cu_Invoice_Id CuInvoiceID
	,InsertEventDate
	,Inserted
	,UserName
	,ResolvedToMonth
	,ResolvedToMonthEventDate
	,Is_Processed IsProcessed
	,Is_Reported IsReported
	,Is_Manual IsManual
	,ClosedManual
	,ClosedManualDate
	,cue.opened_date OverallExceptionOpen
	,cue.closed_date ExceptionClosed
	,cue.is_closed IsClosed
	,cue.queue_id Queue_ID

	 from @InvInserted	ii
	 left join cte_ResToMonth rtm on rtm.Cu_Invoice_Id = ii.Cu_Invoice_Id
	 left join cte_InvClosed ic on ic.Cu_Invoice_Id = ii.Cu_Invoice_Id
	 left join cu_exception cue on cue.Cu_Invoice_Id = ii.Cu_Invoice_Id
end


GO
GRANT EXECUTE ON  [dbo].[Report_InvoiceMetricData] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_InvoiceMetricData] TO [CBMSApplication]
GO
