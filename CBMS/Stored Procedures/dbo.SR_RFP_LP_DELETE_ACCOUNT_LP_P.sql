SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_LP_DELETE_ACCOUNT_LP_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
	@rfp_id int,
	@sr_rfp_account_id int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- Delete the rows
--exec dbo.SR_RFP_LP_DELETE_ACCOUNT_LP_P
--	@rfp_id = null,
--	@sr_rfp_account_id = 20825

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_LP_DELETE_ACCOUNT_LP_P
	@rfp_id int,
	@sr_rfp_account_id int
	AS	
	
SET NOCOUNT ON

	DELETE sr_rfp_lp_determinant_values 
	
	FROM 	sr_rfp_lp_determinant_values val,
	     	sr_rfp_load_profile_determinant det,
			sr_rfp_load_profile_setup setup
	WHERE 	setup.sr_rfp_account_id = @sr_rfp_account_id
	      	and det.sr_rfp_load_profile_setup_id = setup.sr_rfp_load_profile_setup_id
		and val.sr_rfp_load_profile_determinant_id = det.sr_rfp_load_profile_determinant_id
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_LP_DELETE_ACCOUNT_LP_P] TO [CBMSApplication]
GO
