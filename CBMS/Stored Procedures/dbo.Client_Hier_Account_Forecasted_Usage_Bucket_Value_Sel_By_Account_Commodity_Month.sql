
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	dbo.Client_Hier_Account_Forecasted_Usage_Bucket_Value_Sel_By_Account_Commodity_Month

DESCRIPTION:

			This sproc is to get the forecast usage aggregation for the given account , commodity and month.

				- If data not available for the given month take it from previous year ( Goes back upto 5 years)
				- All the bucket data comes from same month
				- If more than one line item there in the invoice for the same bucket with different sub bucket then this sproc just gets any one.
				- If baseline invoice just has charges but no usage then this sproc doesn't rerturn any data.
				- Only the Non adjustement bill type invoices will be considered to calculate forecasted usage

INPUT PARAMETERS:
	Name						DataType			Default		Description
---------------------------------------------------------------------------
@Client_Hier_Id					INT								Client_Hier_Id of the account ( for now only the utility account can be forecasted)
@Account_Id						INT								Account id for which the forecast calculation is needed
@Commodity_Id					INT								Commodity of the forecast config
@Forecast_Month					tvp_Forecast_Month				Forecast and source month

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
---------------------------------------------------------------------------

USAGE EXAMPLES:
---------------------------------------------------------------------------

DECLARE @Forecast_Month_input tvp_Forecast_Month;

INSERT      INTO @Forecast_Month_input
VALUES
            ( '2016-01-01', '2013-01-01' )

EXEC Client_Hier_Account_Forecasted_Usage_Bucket_Value_Sel_By_Account_Commodity_Month
      @Client_Hier_Id = 22860
     ,@Account_Id = 128529
     ,@Commodity_Id = 290
      ,@Forecast_Month = @Forecast_Month_input
GO

---------------------------------------------------------------------------

DECLARE @Forecast_Month_input tvp_Forecast_Month;
INSERT      INTO @Forecast_Month_input
VALUES
            ( '2016-01-01', '2015-01-01' )
             ,
            ( '2016-02-01', '2015-02-01' )

EXEC Client_Hier_Account_Forecasted_Usage_Bucket_Value_Sel_By_Account_Commodity_Month
      @Client_Hier_Id = 13965
     ,@Account_Id = 53303
     ,@Commodity_Id = 290
     ,@Forecast_Month = @Forecast_Month_input
GO

	--------------------------------------------------------------------------------

DECLARE @Forecast_Month_input tvp_Forecast_Month;
INSERT      INTO @Forecast_Month_input
VALUES
            ( '2016-01-01', '2012-01-01' )
EXEC Client_Hier_Account_Forecasted_Usage_Bucket_Value_Sel_By_Account_Commodity_Month
	  @Client_Hier_Id = 13965
      ,@Account_Id = 53303
     ,@Commodity_Id = 290
     ,@Forecast_Month = @Forecast_Month_input
			
			------------------------------
GO	
DECLARE @Forecast_Month_input tvp_Forecast_Month;
INSERT      INTO @Forecast_Month_input
VALUES
            ( '2017-03-01', '2016-03-01' )
EXEC Client_Hier_Account_Forecasted_Usage_Bucket_Value_Sel_By_Account_Commodity_Month
      @Account_ID = 1150049
     ,@Client_Hier_Id = 509817
     ,@Commodity_id = 290
     ,@Forecast_Month = @Forecast_Month_input

---------------------------------------------------------------------------------

declare @p4 dbo.tvp_Forecast_Month
insert into @p4 values(N'01-01-2016',N'01-01-2015')
insert into @p4 values(N'02-01-2016',N'02-01-2015')
insert into @p4 values(N'03-01-2016',N'03-01-2015')
insert into @p4 values(N'04-01-2016',N'04-01-2015')
insert into @p4 values(N'05-01-2016',N'05-01-2015')
insert into @p4 values(N'06-01-2016',N'06-01-2015')
insert into @p4 values(N'07-01-2016',N'07-01-2015')
insert into @p4 values(N'08-01-2016',N'08-01-2015')
insert into @p4 values(N'09-01-2016',N'09-01-2015')
insert into @p4 values(N'10-01-2016',N'10-01-2015')
insert into @p4 values(N'11-01-2016',N'11-01-2015')
insert into @p4 values(N'12-01-2016',N'12-01-2015')
insert into @p4 values(N'01-01-2017',N'01-01-2016')
insert into @p4 values(N'02-01-2017',N'02-01-2016')
insert into @p4 values(N'03-01-2017',N'03-01-2016')
insert into @p4 values(N'04-01-2017',N'04-01-2016')
insert into @p4 values(N'05-01-2017',N'05-01-2016')
insert into @p4 values(N'06-01-2017',N'06-01-2016')
insert into @p4 values(N'07-01-2017',N'07-01-2016')
insert into @p4 values(N'08-01-2017',N'08-01-2016')
insert into @p4 values(N'09-01-2017',N'09-01-2016')
insert into @p4 values(N'10-01-2017',N'10-01-2016')
insert into @p4 values(N'11-01-2017',N'11-01-2016')
insert into @p4 values(N'12-01-2017',N'12-01-2016')
insert into @p4 values(N'01-01-2018',N'01-01-2017')
insert into @p4 values(N'02-01-2018',N'02-01-2017')
insert into @p4 values(N'03-01-2018',N'03-01-2017')
insert into @p4 values(N'04-01-2018',N'04-01-2017')
insert into @p4 values(N'05-01-2018',N'05-01-2017')
insert into @p4 values(N'06-01-2018',N'06-01-2017')
insert into @p4 values(N'07-01-2018',N'07-01-2017')
insert into @p4 values(N'08-01-2018',N'08-01-2017')
insert into @p4 values(N'09-01-2018',N'09-01-2017')
insert into @p4 values(N'10-01-2018',N'10-01-2017')
insert into @p4 values(N'11-01-2018',N'11-01-2017')
insert into @p4 values(N'12-01-2018',N'12-01-2017')
SELECT * FROM @p4

EXEC dbo.Client_Hier_Account_Forecasted_Usage_Bucket_Value_Sel_By_Account_Commodity_Month
      @Client_Hier_Id = 32176
     ,@Account_Id = 159845
     ,@Commodity_Id = 290
     ,@Forecast_Month = @p4;   

AUTHOR INITIALS:
	Initials	Name
-------------------------------------------------------------------
	HG			Harihara Suthan Ganesan

MODIFICATIONS
	Initials	Date			Modification
-------------------------------------------------------------------
	HG			2016-02-29		AS400/CIP Integration - Created to get the forecast usage
	HG			2017-01-01		MAINT-4737, modified to exclude the invoices with the type of "Adjustmen"
******/
CREATE PROCEDURE [dbo].[Client_Hier_Account_Forecasted_Usage_Bucket_Value_Sel_By_Account_Commodity_Month]
      (
       @Client_Hier_Id INT
      ,@Account_Id INT
      ,@Commodity_Id INT
      ,@Forecast_Month AS tvp_Forecast_Month READONLY )
AS
BEGIN

      SET NOCOUNT ON;

      CREATE TABLE #Invoice_Determinant
            (
             Forecast_Month DATE
            ,Cu_Invoice_Id INT
            ,Cu_Invoice_Determinant_Id INT
            ,Bucket_Master_Id INT
            ,Sub_Bucket_Master_Id INT
            ,Bucket_Value DECIMAL(28, 10)
            ,Uom_Type_Id INT
            ,Actual_Source_Month DATE
            ,No_Of_Months INT
            ,Last_Change_Ts DATETIME );

      CREATE TABLE #Bucket_Converted_Uom
            (
             Ec_Invoice_Sub_Bucket_Master_Id INT
            ,Converted_Uom_Id INT );

      CREATE TABLE #Forecasted_Usage
            (
             Forecast_Month DATE
            ,Actual_Service_Month DATE
            ,Bucket_Master_Id INT
            ,Ec_Invoice_Sub_Bucket_Master_Id INT
            ,Bucket_Value DECIMAL(28, 10)
            ,Uom_Type_Id INT
            ,Last_Change_Ts DATE );

      DECLARE @Bucket_Aggregation_Type TABLE
            (
             Bucket_Master_Id INT
            ,Aggregation_Type VARCHAR(25)
            ,Row_Num INT IDENTITY(1, 1) );

      DECLARE @Invoice_Period TABLE
            (
             Forecast_Month DATE
            ,Start_Service_Month DATE
            ,End_Service_Month DATE );

      DECLARE @Forecasted_Determinant TABLE
            (
             Bucket_Master_Id INT
            ,Sub_Bucket_Master_Id INT
            ,Bucket_Value DECIMAL(28, 10)
            ,Uom_Type_Id INT );

      CREATE TABLE #Invoice_List
            (
             Forecast_Month DATE
            ,Cu_Invoice_Id INT
            ,Service_Month DATE
            ,Row_Num INT NULL );
      CREATE CLUSTERED INDEX ix_#IL ON #Invoice_List( Cu_Invoice_Id, Row_Num);

      
      DECLARE
            @Sql_Forecast_Aggregation VARCHAR(MAX)
           ,@Total_Rows INT
           ,@Current_Row_Num INT = 1
           ,@Aggregation_Type VARCHAR(25)
           ,@Bucket_Master_Id INT
           ,@Invoice_Data_Source_Cd INT
           ,@Invoiced_Data_Type_Cd INT
           ,@Forecast_Data_Type_Cd INT
           ,@NG_Commodity_Type_Id INT
           ,@Default_Bucket_Master_Id INT
           ,@Adjustment_Invoice_Type_Cd INT;
           
      SELECT
            @Adjustment_Invoice_Type_Cd = cd.Code_Id
      FROM
            dbo.Codeset cs
            JOIN dbo.Code cd
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Std_Column_Name = 'Invoice_Type_Cd'
            AND cs.Codeset_Name = 'Invoice Type'
            AND cd.Code_Value = 'Adjustment';

      SELECT TOP 1
            @Default_Bucket_Master_Id = bm.Bucket_Master_Id
      FROM
            Core.Client_Hier_Account cha
            INNER JOIN dbo.Bucket_Master bm
                  ON cha.Commodity_Id = bm.Commodity_Id
            INNER JOIN Code c
                  ON bm.Bucket_Type_Cd = c.Code_Id
      WHERE
            bm.Commodity_Id = @Commodity_Id
            AND Account_Id = @Account_Id
            AND Client_Hier_Id = @Client_Hier_Id
            AND bm.Is_Shown_On_Invoice = 1
            AND c.Code_Value = 'Determinant';    
      
      SELECT
            @Invoice_Data_Source_Cd = c.Code_Id
      FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code c
                  ON cs.Codeset_Id = c.Codeset_Id
      WHERE
            Std_Column_Name = 'Data_Source_Cd'
            AND Code_Value = 'Invoice';

      SELECT
            @NG_Commodity_Type_Id = Commodity_Id
      FROM
            dbo.Commodity
      WHERE
            Commodity_Name = 'Natural Gas';

      SELECT
            @Invoiced_Data_Type_Cd = MAX(CASE WHEN c.Code_Value = 'Invoiced' THEN c.Code_Id
                                         END)
           ,@Forecast_Data_Type_Cd = MAX(CASE WHEN c.Code_Value = 'Forecast' THEN c.Code_Id
                                         END)
      FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code c
                  ON cs.Codeset_Id = c.Codeset_Id
      WHERE
            Std_Column_Name = 'Data_Type_Cd'
            AND Code_Value IN ( 'Invoiced', 'Forecast' );
            
      
 		-- Go back to 5 years to get the start period and end period will be the source month
      INSERT      INTO @Invoice_Period
                  ( Forecast_Month
                  ,Start_Service_Month
                  ,End_Service_Month )
                  SELECT
                        Forecast_Month
                       ,DATEADD(YEAR, -4, Source_Month) AS Start_Service_Month
                       ,Source_Month AS End_Service_Month
                  FROM
                        @Forecast_Month;

		
		-- Get all the invoices available for the month to be forecasted by applying the date range to be considered.
      INSERT      INTO #Invoice_List
                  ( Forecast_Month
                  ,Cu_Invoice_Id
                  ,Service_Month )
                  SELECT
                        p.Forecast_Month
                       ,ci.CU_INVOICE_ID
                       ,sm.SERVICE_MONTH
                  FROM
                        dbo.CU_INVOICE_SERVICE_MONTH sm
                        INNER JOIN dbo.CU_INVOICE ci
                              ON ci.CU_INVOICE_ID = sm.CU_INVOICE_ID
                        INNER JOIN @Invoice_Period p
                              ON ( (( sm.SERVICE_MONTH BETWEEN p.Start_Service_Month
                                                       AND     p.End_Service_Month
                                      AND MONTH(sm.SERVICE_MONTH) = MONTH(p.Forecast_Month) )
                                   OR sm.SERVICE_MONTH = p.Forecast_Month ) )
                  WHERE
                        ci.IS_PROCESSED = 1
                        AND ci.IS_REPORTED = 1
                        AND ci.IS_DNT = 0
                        AND ci.IS_DUPLICATE = 0
                        AND sm.Account_ID = @Account_Id
                        AND ISNULL(ci.Invoice_Type_Cd, 0) != @Adjustment_Invoice_Type_Cd
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.CU_INVOICE_DETERMINANT cid
                                          INNER JOIN dbo.CU_INVOICE_DETERMINANT_ACCOUNT da
                                                ON da.CU_INVOICE_DETERMINANT_ID = cid.CU_INVOICE_DETERMINANT_ID
                                     WHERE
                                          cid.CU_INVOICE_ID = ci.CU_INVOICE_ID
                                          AND da.ACCOUNT_ID = sm.Account_ID
                                          AND cid.COMMODITY_TYPE_ID = @Commodity_Id )
                  GROUP BY
                        p.Forecast_Month
                       ,ci.CU_INVOICE_ID
                       ,sm.SERVICE_MONTH;

      INSERT      INTO #Invoice_List
                  ( Forecast_Month
                  ,Cu_Invoice_Id
                  ,Service_Month )
                  SELECT
                        p.Forecast_Month
                       ,ci.CU_INVOICE_ID
                       ,sm.SERVICE_MONTH
                  FROM
                        dbo.CU_INVOICE_SERVICE_MONTH sm
                        INNER JOIN dbo.CU_INVOICE ci
                              ON ci.CU_INVOICE_ID = sm.CU_INVOICE_ID
                        INNER JOIN dbo.CU_INVOICE_CHARGE cic
                              ON cic.CU_INVOICE_ID = ci.CU_INVOICE_ID
                        INNER JOIN @Invoice_Period p
                              ON ( (( sm.SERVICE_MONTH BETWEEN p.Start_Service_Month
                                                       AND     p.End_Service_Month
                                      AND MONTH(sm.SERVICE_MONTH) = MONTH(p.Forecast_Month) )
                                   OR sm.SERVICE_MONTH = p.Forecast_Month ) )
                  WHERE
                        ci.IS_PROCESSED = 1
                        AND ci.IS_REPORTED = 1
                        AND ci.IS_DNT = 0
                        AND ci.IS_DUPLICATE = 0
                        AND sm.Account_ID = @Account_Id
                        AND ISNULL(ci.Invoice_Type_Cd, 0) != @Adjustment_Invoice_Type_Cd
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.CU_INVOICE_CHARGE cic
                                          INNER JOIN dbo.CU_INVOICE_CHARGE_ACCOUNT ca
                                                ON ca.CU_INVOICE_CHARGE_ID = cic.CU_INVOICE_CHARGE_ID
                                     WHERE
                                          cic.CU_INVOICE_ID = ci.CU_INVOICE_ID
                                          AND ca.ACCOUNT_ID = sm.Account_ID
                                          AND cic.COMMODITY_TYPE_ID = @Commodity_Id )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          #Invoice_List il
                                         WHERE
                                          il.Forecast_Month = p.Forecast_Month
                                          AND il.Cu_Invoice_Id = ci.CU_INVOICE_ID
                                          AND il.Service_Month = sm.SERVICE_MONTH )
                  GROUP BY
                        p.Forecast_Month
                       ,ci.CU_INVOICE_ID
                       ,sm.SERVICE_MONTH;

      WITH  Cte_Invoice_Row_Num
              AS ( SELECT
                        il.Forecast_Month
                       ,Cu_Invoice_Id
                       ,Service_Month
                       ,DENSE_RANK() OVER ( PARTITION BY il.Forecast_Month ORDER BY Service_Month DESC ) Row_Num	-- Using Dense rank so that if more than one invoice recieved for a single month both are considered.
                   FROM
                        #Invoice_List il)
            UPDATE
                  il
            SET
                  il.Row_Num = rn.Row_Num
            FROM
                  #Invoice_List il
                  INNER JOIN Cte_Invoice_Row_Num rn
                        ON rn.Forecast_Month = il.Forecast_Month
                           AND rn.Cu_Invoice_Id = il.Cu_Invoice_Id
                           AND rn.Service_Month = il.Service_Month;

      INSERT      INTO #Invoice_Determinant
                  ( Forecast_Month
                  ,Cu_Invoice_Id
                  ,Cu_Invoice_Determinant_Id
                  ,Bucket_Master_Id
                  ,Sub_Bucket_Master_Id
                  ,Bucket_Value
                  ,Uom_Type_Id
                  ,Actual_Source_Month
                  ,No_Of_Months
                  ,Last_Change_Ts )
                  SELECT
                        il.Forecast_Month
                       ,cid.CU_INVOICE_ID
                       ,cid.CU_INVOICE_DETERMINANT_ID
                       ,cid.Bucket_Master_Id
                       ,cid.EC_Invoice_Sub_Bucket_Master_Id
                       ,( CASE WHEN NULLIF(da.Determinant_Expression, '') IS NULL THEN ( CONVERT(DECIMAL(28, 10), REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(( CASE WHEN ( PATINDEX('%[0-9]-', cid.DETERMINANT_VALUE) ) > 0 THEN STUFF(cid.DETERMINANT_VALUE, PATINDEX('%[0-9]-', cid.DETERMINANT_VALUE) + 1, 1, '')
                                                                                                                                                                         ELSE cid.DETERMINANT_VALUE
                                                                                                                                                                    END ), ',', ''), '0-', 0), '+', ''), '$', ''), ')', ''), '(', '')) )
                               ELSE da.Determinant_Value
                          END ) Actual_Bucket_Value
                       ,cid.UNIT_OF_MEASURE_TYPE_ID
                       ,il.Service_Month
                       ,mc.Month_Cnt
                       ,ci.UPDATED_DATE
                  FROM
                        dbo.CU_INVOICE_DETERMINANT cid
                        INNER JOIN dbo.CU_INVOICE_DETERMINANT_ACCOUNT da
                              ON da.CU_INVOICE_DETERMINANT_ID = cid.CU_INVOICE_DETERMINANT_ID
                        INNER JOIN dbo.CU_INVOICE ci
                              ON ci.CU_INVOICE_ID = cid.CU_INVOICE_ID
                        INNER JOIN #Invoice_List il
                              ON il.Cu_Invoice_Id = cid.CU_INVOICE_ID
                                 AND il.Row_Num = 1
                                 -- Considering the first available month invoices
                        OUTER APPLY ( SELECT
                                          COUNT(DISTINCT sm.SERVICE_MONTH)
                                      FROM
                                          dbo.CU_INVOICE_SERVICE_MONTH sm
                                      WHERE
                                          sm.Account_ID = @Account_Id
                                          AND sm.CU_INVOICE_ID = cid.CU_INVOICE_ID ) mc ( Month_Cnt )
                  WHERE
                        da.ACCOUNT_ID = @Account_Id
                        AND cid.COMMODITY_TYPE_ID = @Commodity_Id
                        AND cid.EC_Invoice_Sub_Bucket_Master_Id IS NOT NULL
                  GROUP BY
                        il.Forecast_Month
                       ,cid.CU_INVOICE_ID
                       ,cid.CU_INVOICE_DETERMINANT_ID
                       ,cid.Bucket_Master_Id
                       ,cid.EC_Invoice_Sub_Bucket_Master_Id
                       ,( CASE WHEN NULLIF(da.Determinant_Expression, '') IS NULL THEN ( CONVERT(DECIMAL(28, 10), REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(( CASE WHEN ( PATINDEX('%[0-9]-', cid.DETERMINANT_VALUE) ) > 0 THEN STUFF(cid.DETERMINANT_VALUE, PATINDEX('%[0-9]-', cid.DETERMINANT_VALUE) + 1, 1, '')
                                                                                                                                                                         ELSE cid.DETERMINANT_VALUE
                                                                                                                                                                    END ), ',', ''), '0-', 0), '+', ''), '$', ''), ')', ''), '(', '')) )
                               ELSE da.Determinant_Value
                          END )
                       ,cid.UNIT_OF_MEASURE_TYPE_ID
                       ,il.Service_Month
                       ,mc.Month_Cnt
                       ,ci.UPDATED_DATE;

	-- Get the first uom received for that bucket and convert all the remaining line item of that bucket to that unit
      WITH  Cte_Bucket_Uom
              AS ( SELECT
                        id.Sub_Bucket_Master_Id
                       ,id.Uom_Type_Id
                       ,ROW_NUMBER() OVER ( PARTITION BY id.Sub_Bucket_Master_Id ORDER BY id.Cu_Invoice_Determinant_Id ) AS Row_Num
                   FROM
                        #Invoice_Determinant id)
            INSERT      INTO #Bucket_Converted_Uom
                        ( Ec_Invoice_Sub_Bucket_Master_Id
                        ,Converted_Uom_Id )
                        SELECT
                              bu.Sub_Bucket_Master_Id
                             ,bu.Uom_Type_Id
                        FROM
                              Cte_Bucket_Uom bu
                        WHERE
                              bu.Row_Num = 1;

      INSERT      INTO @Bucket_Aggregation_Type
                  ( Bucket_Master_Id
                  ,Aggregation_Type )
                  SELECT
                        chld.Bucket_Master_Id
                       ,CASE WHEN at.Code_Value = 'Recent' THEN 'MAX'
                             ELSE at.Code_Value
                        END AS Aggregation_Type
                  FROM
                        dbo.Bucket_Category_Rule bcr
                        INNER JOIN dbo.Bucket_Master cat
                              ON cat.Bucket_Master_Id = bcr.Category_Bucket_Master_Id
                        INNER JOIN dbo.Bucket_Master chld
                              ON chld.Bucket_Master_Id = bcr.Child_Bucket_Master_Id
                        INNER JOIN dbo.Code at
                              ON at.Code_Id = bcr.Aggregation_Type_CD
                        INNER JOIN dbo.Code lvl
                              ON lvl.Code_Id = bcr.CU_Aggregation_Level_Cd
                        INNER JOIN dbo.Code bt
                              ON bt.Code_Id = chld.Bucket_Type_Cd
                  WHERE
                        chld.Commodity_Id = @Commodity_Id
                        AND chld.Is_Shown_On_Invoice = 1
                        AND lvl.Code_Value = 'Invoice'
                        AND bt.Code_Value = 'Determinant'
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          #Invoice_Determinant id
                                     WHERE
                                          id.Bucket_Master_Id = chld.Bucket_Master_Id )
                  GROUP BY
                        chld.Bucket_Master_Id
                       ,CASE WHEN at.Code_Value = 'Recent' THEN 'MAX'
                             ELSE at.Code_Value
                        END;

		-- To add the buckets which are not aggregated to any other category ( Other )
      INSERT      INTO @Bucket_Aggregation_Type
                  ( Bucket_Master_Id
                  ,Aggregation_Type )
                  SELECT
                        bm.Bucket_Master_Id
                       ,'SUM'
                  FROM
                        dbo.Bucket_Master bm
                        INNER JOIN dbo.Code bt
                              ON bt.Code_Id = bm.Bucket_Type_Cd
                  WHERE
                        bt.Code_Value = 'Determinant'
                        AND bm.Is_Shown_On_Invoice = 1
                        AND bm.Commodity_Id = @Commodity_Id
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          #Invoice_Determinant id
                                     WHERE
                                          id.Bucket_Master_Id = bm.Bucket_Master_Id )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          @Bucket_Aggregation_Type bat
                                         WHERE
                                          bat.Bucket_Master_Id = bm.Bucket_Master_Id );

      SELECT
            @Total_Rows = MAX(Row_Num)
      FROM
            @Bucket_Aggregation_Type;

      BEGIN TRY

            WHILE @Current_Row_Num <= @Total_Rows
                  BEGIN

                        SELECT
                              @Bucket_Master_Id = Bucket_Master_Id
                             ,@Aggregation_Type = bat.Aggregation_Type
                        FROM
                              @Bucket_Aggregation_Type bat
                        WHERE
                              bat.Row_Num = @Current_Row_Num;
		
                        SET @Sql_Forecast_Aggregation = '
			INSERT INTO #Forecasted_Usage
             ( Forecast_Month
             ,Actual_Service_Month
             ,Bucket_Master_Id
             ,Ec_Invoice_Sub_Bucket_Master_Id
             ,Bucket_Value
             ,Uom_Type_Id
             ,Last_Change_Ts )                  
                  SELECT
                        id.Forecast_Month
                       ,id.Actual_Source_Month
                       ,id.Bucket_Master_Id
                       ,id.Sub_Bucket_Master_Id
                       ,' + @Aggregation_Type + '( (( id.Bucket_Value / id.No_Of_Months ) * cuc.CONVERSION_FACTOR) ) AS Bucket_Value
                       ,uom.Converted_Uom_Id
                       ,MAX(id.Last_Change_Ts) AS Last_Change_Ts
                  FROM
                        #Invoice_Determinant id
                        INNER JOIN #Bucket_Converted_Uom uom
                              ON uom.Ec_Invoice_Sub_Bucket_Master_Id = id.Sub_Bucket_Master_Id
                        INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                              ON cuc.BASE_UNIT_ID = id.Uom_Type_Id
                          AND cuc.CONVERTED_UNIT_ID = uom.Converted_Uom_Id
				  WHERE
						id.Bucket_Master_Id = ' + CAST(@Bucket_Master_Id AS VARCHAR(20)) + '
                  GROUP BY
                        id.Forecast_Month
                       ,id.Actual_Source_Month
                       ,id.Bucket_Master_Id
                       ,id.Sub_Bucket_Master_Id
                       ,uom.Converted_Uom_Id';		

                        EXEC (@Sql_Forecast_Aggregation);

                        SET @Current_Row_Num = @Current_Row_Num + 1;

                  END;
      END TRY

      BEGIN CATCH
            EXEC dbo.usp_RethrowError
                  @CustomMessage = ''; -- varchar(2500)
      END CATCH;

      
      INSERT      INTO #Forecasted_Usage
                  ( Forecast_Month
                  ,Actual_Service_Month
                  ,Bucket_Master_Id
                  ,Ec_Invoice_Sub_Bucket_Master_Id
                  ,Bucket_Value
                  ,Uom_Type_Id
                  ,Last_Change_Ts )
                  SELECT
                        fm.Forecast_Month
                       ,NULL
                       ,@Default_Bucket_Master_Id
                       ,-1
                       ,NULL
                       ,NULL
                       ,GETDATE()
                  FROM
                        @Forecast_Month fm
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          #Forecasted_Usage fu
                                     WHERE
                                          fu.Forecast_Month = fm.Forecast_Month );
                        
                       
     
      
      SELECT
            @Client_Hier_Id AS Client_Hier_Id
           ,@Account_Id AS Account_Id
           ,bm.Bucket_Master_Id
           ,fu.Forecast_Month AS Service_Month
           ,-1 AS CU_Invoice_Recalc_Response_Id
           ,-1 AS Calculated_Charge_Name_Id
           ,Bucket_Value
           ,Actual_Service_Month
           ,0 AS Is_Locked
           ,0 AS Is_Manual_Override
           ,fu.Uom_Type_Id
           ,NULL AS CURRENCY_UNIT_ID
           ,fu.Ec_Invoice_Sub_Bucket_Master_Id
           ,CASE WHEN fu.Forecast_Month = Actual_Service_Month THEN @Invoiced_Data_Type_Cd
                 ELSE @Forecast_Data_Type_Cd
            END AS Data_Type_Cd
           ,@Invoice_Data_Source_Cd AS Data_Source_Cd
           ,fu.Last_Change_Ts
           ,bm.Bucket_Name
           ,e.ENTITY_NAME AS Uom
      FROM
            #Forecasted_Usage fu
            INNER JOIN dbo.Bucket_Master bm
                  ON fu.Bucket_Master_Id = bm.Bucket_Master_Id
            LEFT OUTER JOIN dbo.ENTITY e
                  ON e.ENTITY_ID = fu.Uom_Type_Id
      ORDER BY
            Service_Month
           ,Bucket_Master_Id;

      DROP TABLE #Forecasted_Usage;
      DROP TABLE #Invoice_Determinant;
      DROP TABLE #Bucket_Converted_Uom;

END;
;
GO

GRANT EXECUTE ON  [dbo].[Client_Hier_Account_Forecasted_Usage_Bucket_Value_Sel_By_Account_Commodity_Month] TO [CBMSApplication]
GO
