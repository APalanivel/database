SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--SELECT * FROM budget_detail_comments where budget_account_id in(select budget_account_id from budget_account where budget_id=590)


CREATE   PROCEDURE dbo.BUDGET_CREATE_REVISED_BUDGET_DETAILS_COMMENTS_P
@user_id varchar(10),
@session_id varchar(20),
@budgetId int,
@revisedBudgetId int 



AS
begin
set nocount on



	insert into budget_detail_comments(	
				bdc.BUDGET_ACCOUNT_ID,
                                VARIABLE_COMMENTS,
                                IS_VARIABLE_ON_DV,
                                TRANSPORTATION_COMMENTS,
                                IS_TRANSPORTATION_ON_DV,
                                DISTRIBUTION_COMMENTS,
                                IS_DISTRIBUTION_ON_DV,
                                TRANSMISSION_COMMENTS,
                                IS_TRANSMISSION_ON_DV,
                                OTHER_BUNDLED_COMMENTS,
                                IS_OTHER_BUNDLED_ON_DV,
                                OTHER_FIXED_COSTS_COMMENTS,
                                IS_OTHER_FIXED_COSTS_ON_DV,
                                SOURCING_TAX_COMMENTS,
                                IS_SOURCING_TAX_ON_DV,
                                RATES_TAX_COMMENTS,
                                IS_RATES_TAX_ON_DV
				)
                       select
               (select budget_account_id from budget_account where budget_id = @revisedBudgetId and account_id =bacc.account_id) as budget_account_id,
      			
                                VARIABLE_COMMENTS,
                                IS_VARIABLE_ON_DV,
                                TRANSPORTATION_COMMENTS,
                                IS_TRANSPORTATION_ON_DV,
                                DISTRIBUTION_COMMENTS,
                                IS_DISTRIBUTION_ON_DV,
                                TRANSMISSION_COMMENTS,
                                IS_TRANSMISSION_ON_DV,
                                OTHER_BUNDLED_COMMENTS,
                                IS_OTHER_BUNDLED_ON_DV,
                                OTHER_FIXED_COSTS_COMMENTS,
                                IS_OTHER_FIXED_COSTS_ON_DV,
                                SOURCING_TAX_COMMENTS,
                                IS_SOURCING_TAX_ON_DV,
                                RATES_TAX_COMMENTS,
                                IS_RATES_TAX_ON_DV
               		 from  	budget_detail_comments bdc,budget_account bacc
              	 	 where 	bacc.budget_id=@budgetId
                                and bdc.budget_account_id=bacc.budget_account_id
                                and bacc.is_deleted = 0
	                 
	
				
end
GO
GRANT EXECUTE ON  [dbo].[BUDGET_CREATE_REVISED_BUDGET_DETAILS_COMMENTS_P] TO [CBMSApplication]
GO
