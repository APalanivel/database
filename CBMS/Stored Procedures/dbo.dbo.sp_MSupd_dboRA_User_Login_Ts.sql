SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[dbo.sp_MSupd_dboRA_User_Login_Ts]
		@c1 int = NULL,
		@c2 int = NULL,
		@c3 datetime = NULL,
		@pkc1 int = NULL,
		@bitmap binary(1)
as
begin  
update [dbo].[RA_User_Login_Ts] set
		[USER_INFO_ID] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [USER_INFO_ID] end,
		[Login_Ts] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [Login_Ts] end
where [User_Login_Ts_Id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
        exec sp_MSreplraiserror 20598
end 
GO
