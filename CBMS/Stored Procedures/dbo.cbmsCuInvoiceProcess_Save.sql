SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:  
	 dbo.cbmsCuInvoiceProcess_Save        
                
Description:                
		       
                             
 Input Parameters:                
    Name						DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
	@MyAccountId				INT
    @cu_invoice_process_id		INT					 NULL
    @cu_invoice_id				INT
    @cu_invoice_created			BIT = 0
    @service_mapping			BIT = 0
    @resolve_to_account			BIT = 0
    @cu_duplicate_test			BIT = 0
    @dm_watch_list				BIT = 0
    @required_data				BIT = 0
    @data_mapped				BIT = 0
    @ra_watch_list				BIT = 0
    @resolve_to_month			BIT = 0
    @contract_recalc			BIT = 0
    @invoice_aggregate			BIT = 0
    @variance_test				BIT = 0
    @Is_Data_Quality_Test		BIT = 0

      
 Output Parameters:                      
    Name					  DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
----------------------------------------------------------------------------------------     

 
       
   
Author Initials:                
    Initials		Name                
----------------------------------------------------------------------------------------                  
	NR				Narayana Reddy                 
 Modifications:                
    Initials        Date			Modification                
----------------------------------------------------------------------------------------                  
    NR				2016-11-15		Added Header and @Is_Data_Quality_Test column .           
               
******/

CREATE PROCEDURE [dbo].[cbmsCuInvoiceProcess_Save]
    (
        @MyAccountId INT
        , @cu_invoice_process_id INT = NULL
        , @cu_invoice_id INT
        , @cu_invoice_created BIT = 0
        , @service_mapping BIT = 0
        , @resolve_to_account BIT = 0
        , @cu_duplicate_test BIT = 0
        , @dm_watch_list BIT = 0
        , @required_data BIT = 0
        , @data_mapped BIT = 0
        , @ra_watch_list BIT = 0
        , @resolve_to_month BIT = 0
        , @contract_recalc BIT = 0
        , @invoice_aggregate BIT = 0
        , @variance_test BIT = 0
        , @Is_Data_Quality_Test BIT = 0
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @this_id INT;

        SET @this_id = @cu_invoice_process_id;

        IF @this_id IS NULL
            BEGIN

                SELECT
                    @this_id = CU_INVOICE_PROCESS_ID
                FROM
                    CU_INVOICE_PROCESS
                WHERE
                    CU_INVOICE_ID = @cu_invoice_id;

            END;

        IF @this_id IS NULL
            BEGIN

                INSERT INTO CU_INVOICE_PROCESS
                     (
                         CU_INVOICE_ID
                         , CU_INVOICE_CREATED
                         , SERVICE_MAPPING
                         , RESOLVE_TO_ACCOUNT
                         , CU_DUPLICATE_TEST
                         , DM_WATCH_LIST
                         , REQUIRED_DATA
                         , DATA_MAPPED
                         , RA_WATCH_LIST
                         , RESOLVE_TO_MONTH
                         , CONTRACT_RECALC
                         , INVOICE_AGGREGATE
                         , VARIANCE_TEST
                         , Is_Data_Quality_Test
                     )
                VALUES
                    (@cu_invoice_id
                     , @cu_invoice_created
                     , @service_mapping
                     , @resolve_to_account
                     , @cu_duplicate_test
                     , 1
                     , @required_data
                     , @data_mapped
                     , @ra_watch_list
                     , @resolve_to_month
                     , @contract_recalc
                     , @invoice_aggregate
                     , @variance_test
                     , @Is_Data_Quality_Test);

                SET @this_id = @@IDENTITY;

            END;
        ELSE
            BEGIN

                UPDATE
                    CU_INVOICE_PROCESS
                SET
                    CU_INVOICE_CREATED = @cu_invoice_created
                    , SERVICE_MAPPING = @service_mapping
                    , RESOLVE_TO_ACCOUNT = @resolve_to_account
                    , CU_DUPLICATE_TEST = @cu_duplicate_test
                    , DM_WATCH_LIST = @dm_watch_list
                    , REQUIRED_DATA = @required_data
                    , DATA_MAPPED = @data_mapped
                    , RA_WATCH_LIST = @ra_watch_list
                    , RESOLVE_TO_MONTH = @resolve_to_month
                    , CONTRACT_RECALC = @contract_recalc
                    , INVOICE_AGGREGATE = @invoice_aggregate
                    , VARIANCE_TEST = @variance_test
                    , Is_Data_Quality_Test = @Is_Data_Quality_Test
                WHERE
                    CU_INVOICE_PROCESS_ID = @this_id;

            END;

        --	SET NOCOUNT OFF

        EXEC cbmsCuInvoiceProcess_Get @MyAccountId, @this_id;

    END;

GO
GRANT EXECUTE ON  [dbo].[cbmsCuInvoiceProcess_Save] TO [CBMSApplication]
GO
