SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Ec_Account_Group_Type_Country_Sel        
                
Description:                
		This sproc to get the Group types based country and state and commodity.        
                             
 Input Parameters:                
    Name						DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
      
 Output Parameters:                      
    Name					  DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
----------------------------------------------------------------------------------------     
   
   Exec dbo.Ec_Account_Group_Type_Country_Sel      
       
   
Author Initials:                
    Initials		Name                
----------------------------------------------------------------------------------------                  
	NR				Narayana Reddy                 
 Modifications:                
    Initials        Date			Modification                
----------------------------------------------------------------------------------------                  
    NR				2016-11-15		Created For MAINT-4563.           
               
******/   
CREATE PROCEDURE [dbo].[Ec_Account_Group_Type_Country_Sel]
AS 
BEGIN
      SET NOCOUNT ON 

      SELECT
            c.COUNTRY_ID
           ,c.COUNTRY_NAME
      FROM
            dbo.Ec_Account_Group_Type eagt
            INNER JOIN dbo.STATE s
                  ON eagt.State_Id = s.STATE_ID
            INNER JOIN dbo.COUNTRY c
                  ON s.COUNTRY_ID = c.COUNTRY_ID
      GROUP BY
            c.COUNTRY_ID
           ,c.COUNTRY_NAME
      ORDER BY
            c.COUNTRY_NAME
                  
                
                  
END
      

;
GO
GRANT EXECUTE ON  [dbo].[Ec_Account_Group_Type_Country_Sel] TO [CBMSApplication]
GO
