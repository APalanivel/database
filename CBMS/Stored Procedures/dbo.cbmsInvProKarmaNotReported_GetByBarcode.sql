SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE     PROCEDURE [dbo].[cbmsInvProKarmaNotReported_GetByBarcode]
	( @MyAccountId int 
	, @barcode varchar(200) = null
	)
AS
BEGIN

	   select distinct r.inv_prokarma_not_reported_id
		, r.inv_prokarma_not_reported_batch_id
		, r.source_file
		, r.inv_sourced_image_id
		, r.client_name
		, r.city_state
		, r.vendor_name
		, r.account_number
		, r.bill_month
		, r.barcode
		, r.bill_status
		, r.remarks
		, b.batch_date received_date
	     from inv_prokarma_not_reported r
	     join inv_prokarma_not_reported_batch b on b.inv_prokarma_not_reported_batch_id = r.inv_prokarma_not_reported_batch_id
	    where r.barcode = @barcode
	 order by b.batch_date asc


END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvProKarmaNotReported_GetByBarcode] TO [CBMSApplication]
GO
