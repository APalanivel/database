SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsGetVarianceBudget2

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

--Mark McCallon
--12-13-06
--pulled Jon's data mart code into a sproc
--populates rpt_variance_cost_usage_site_2

CREATE PROCEDURE dbo.cbmsGetVarianceBudget2

 AS
set nocount on
begin
-- Declare the variables to store the values returned by FETCH.

-- Declare the variables to store the values returned by FETCH.
Delete from RPT_variance_cost_usage_site_2

DECLARE @client_id int

DECLARE variance_cursor CURSOR FOR
SELECT client_id FROM client
ORDER BY client_id

OPEN variance_cursor

-- Perform the first fetch and store the values in variables.
-- Note: The variables are in the same order as the columns
-- in the SELECT statement. 

FETCH NEXT FROM variance_cursor
INTO @client_id

-- Check @@FETCH_STATUS to see if there are any more rows to fetch.
WHILE @@FETCH_STATUS = 0
BEGIN



	declare @intWorkingYear int

	set @intWorkingYear = year(getdate()) - 2
	
	  WHILE @intworkingyear <= year(getdate()) + 1
  	   BEGIN


		insert into rpt_variance_cost_usage_site_2
		exec cbmsRpt_Variance_CostUsageSite_GetDetailElForBudget 128, 3,12,25,@intworkingyear,@client_id


		insert into rpt_variance_cost_usage_site_2
		exec cbmsRpt_Variance_CostUsageSite_GetDetailNGForBudget 128, 3,12,25,@intworkingyear,@client_id

		set @intworkingyear = @intworkingyear + 1	
	   END
	


   -- This is executed as long as the previous fetch succeeds.
   FETCH NEXT FROM variance_cursor
   INTO @client_id
END

CLOSE variance_cursor
DEALLOCATE variance_cursor


update RPT_VARIANCE_COST_USAGE_SITE_FLAG set 
active_table = 'RPT_VARIANCE_COST_USAGE_SITE_2'

return
end
GO
GRANT EXECUTE ON  [dbo].[cbmsGetVarianceBudget2] TO [CBMSApplication]
GO
