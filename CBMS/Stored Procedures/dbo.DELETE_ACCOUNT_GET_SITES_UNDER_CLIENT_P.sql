SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.DELETE_ACCOUNT_GET_SITES_UNDER_CLIENT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@clientId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

--select * from VWSITENAME
--select site_name from VWSITENAME where site_id = 566
--select site_name from site where site_id = 566


CREATE     PROCEDURE dbo.DELETE_ACCOUNT_GET_SITES_UNDER_CLIENT_P 
	@clientId int
	as
if(@clientId = 0)
	begin

		select	SITE_ID,
			SITE_NAME
		from	VWSITENAME viewSite
		

	end
	else 
	begin 
		select	site.SITE_ID,
			viewSite.SITE_NAME
	
		from	SITE site,	
			DIVISION division ,
			VWSITENAME viewSite,
			client c

		where 	c.CLIENT_ID= @clientId 
		AND site.DIVISION_ID=division.DIVISION_ID 	
		and site.SITE_ID = viewSite.SITE_ID
		--and site.closed=0 and site.not_managed=0
		and division.client_id = c.client_id

		order by viewSite.SITE_NAME

	end
GO
GRANT EXECUTE ON  [dbo].[DELETE_ACCOUNT_GET_SITES_UNDER_CLIENT_P] TO [CBMSApplication]
GO
