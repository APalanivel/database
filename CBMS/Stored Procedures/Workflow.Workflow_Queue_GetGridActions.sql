SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

  
    
/******          
NAME:   [Workflow].[Workflow_Queue_GetGridActions]       
DESCRIPTION:       
------------------------------------------------------------         
 INPUT PARAMETERS:          
 Name   DataType  Default Description          
 @User_Info_Id  INT               
 @Module_Id     INT      
------------------------------------------------------------          
 OUTPUT PARAMETERS:          
 Name   DataType  Default Description          
------------------------------------------------------------          
 USAGE EXAMPLES:  EXEC [Workflow].[Workflow_Queue_GetGridActions] 0,1      
------------------------------------------------------------          
AUTHOR INITIALS:          
Initials Name          
------------------------------------------------------------          
TRK   Ramakrishna Summit Energy       
       
 MODIFICATIONS           
 Initials Date   Modification          
------------------------------------------------------------          
 TRK    SEP-2019  Created      
      
******/ 
     
CREATE PROCEDURE [Workflow].[Workflow_Queue_GetGridActions]               
(                    
 @User_Info_Id INT   = NULL ,                  
 @Module_Id INT = NULL                   
)                    
AS                    
BEGIN               
           
        
    
 SELECT [Name],[Id] FROM    
(    
 SELECT DISTINCT       
 C.Code_Value [Name],          
 WQA.Workflow_Queue_Action_Id [Id]       
 FROM   group_info GI         
 JOIN user_info_group_info_map UIG ON GI.group_info_id = UIG.group_info_id         
 JOIN user_info U ON U.user_info_id = UIG.user_info_id         
 JOIN group_info_permission_info_map GIPM ON GIPM.group_info_id = GI.group_info_id         
 JOIN permission_info P WITH (NOLOCK) ON P.permission_info_id = GIPM.permission_info_id      
 JOIN   Workflow.Workflow_Queue_Action  WQA ON WQA.PERMISSION_INFO_ID=P.PERMISSION_INFO_ID    
 JOIN dbo.Code AS C ON C.Code_Id=WQA.Workflow_Queue_Action_Cd    
 WHERE  
 U.USER_INFO_ID=@User_Info_Id     
 AND WQA.Workflow_Queue_Id = @Module_Id  AND C.Is_Active = 1    
 UNION ALL    
 SELECT           
  c.Code_Value [Name],          
  WQA.Workflow_Queue_Action_Id [Id]           
 FROM Workflow.Workflow_Queue_Action AS WQA          
 INNER JOIN dbo.Code AS C ON C.Code_Id=WQA.Workflow_Queue_Action_Cd           
 WHERE WQA.Workflow_Queue_Id = @Module_Id          
 AND Is_Active = 1  AND WQA.PERMISSION_INFO_ID =-1   
 ) X     
 ORDER BY [Id]    
              
END                                                
GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_GetGridActions] TO [CBMSApplication]
GO
