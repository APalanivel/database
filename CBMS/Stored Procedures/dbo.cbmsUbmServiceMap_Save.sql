SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE    PROCEDURE [dbo].[cbmsUbmServiceMap_Save]
	( @MyAccountId int
	, @ubm_service_map_id int = null
	, @ubm_id int
	, @ubm_service_code varchar(200)
	, @ubm_service_type_id int
	, @commodity_type_id int = null
	)
AS
BEGIN

	declare @this_id int

	set nocount on

	set @this_id = @ubm_service_map_id

	if @this_id is null
	begin

	   select @this_id = ubm_service_map_id
	     from ubm_service_map
	    where ubm_id = @ubm_id
	      and ubm_service_code = @ubm_service_code
	      and ubm_service_type_id = @ubm_service_type_id

	end

	if @this_id is null
	begin

		insert into ubm_service_map
			( ubm_id
			, ubm_service_code
			, ubm_service_type_id
			, commodity_type_id
			)
		values
			( @ubm_id
			, @ubm_service_code
			, @ubm_service_type_id
			, @commodity_type_id
			)

		set @this_id = @@IDENTITY


	end

	exec cbmsUbmServiceMap_Get @MyAccountId, @this_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsUbmServiceMap_Save] TO [CBMSApplication]
GO
