SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_MARKET_PRICE_POINT_SEARCH_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@pricePointName		varchar(40)	          	
	@marketPricePoint	int       	null      	
	@marketIndex   		int       	null      	
	@fieldName     		varchar(80)	          	
	@sortState     		int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.GET_MARKET_PRICE_POINT_SEARCH_DETAILS_P 'SOCAL INDEX',15,1,'marketPricePoint',1

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/27/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on
	
******/
CREATE PROCEDURE dbo.GET_MARKET_PRICE_POINT_SEARCH_DETAILS_P
@pricePointName		VARCHAR(40),
@marketPricePoint	INT = NULL,
@marketIndex		INT = NULL,	
@fieldName			VARCHAR(80),
@sortState			INT
AS
BEGIN
	DECLARE @fieldNameSort VARCHAR(80)
	DECLARE @selectClause1 VARCHAR(8000)
	DECLARE @fromClause1 VARCHAR(8000)
	DECLARE @SQLStatement1 VARCHAR(8000)

	IF @fieldName != NULL
	BEGIN
		IF @fieldName = 'marketPricePoint' 
		BEGIN
			SELECT @fieldNameSort = 'price_point.market_price_point'
		END
		IF @fieldName = 'marketPriceIndex' 
		BEGIN
			SELECT @fieldNameSort = 'price_index.market_price_index_name'
		END
		IF @fieldName = 'shortName' 
		BEGIN
			SELECT @fieldNameSort = 'price_point.market_price_point_short_name'
		END
		IF @fieldName = 'showOnDv' 
		BEGIN
			SELECT @fieldNameSort = 'price_point.is_show_on_dv'
		END
		
	END

	SELECT @selectClause1 = 'price_point.market_price_index_id,
		price_index.market_price_index_name,
		price_point.market_price_point_id,
		price_point.market_price_point,
		price_point.market_price_point_short_name as short_name,
		price_point.is_show_on_dv ' 

	SELECT @fromClause1 =  ' dbo.market_price_point price_point
		join dbo.market_price_index price_index on price_index.market_price_index_id = price_point.market_price_index_id
		'
	IF @pricePointName IS NOT NULL 
	BEGIN
		SELECT @fromClause1 = @fromClause1 + ' and price_point.market_price_point like '+''''+@pricePointName+'%'+''''
	END
	IF @marketIndex > 0 
	BEGIN
		SELECT @fromClause1 = @fromClause1 + ' and price_point.market_price_index_id = ' + STR(@marketIndex) 
	END
	IF @marketPricePoint > 0 
	BEGIN
		SELECT @fromClause1 = @fromClause1 + ' and price_point.market_price_point_id = ' + STR(@marketPricePoint) 
	END
	
	IF @fieldNameSort != NULL 
	BEGIN
		IF(@sortState = 1)
		BEGIN	
			SELECT @SQLStatement1 =	'SELECT ' 
					+ @selectClause1  
					+ ' FROM '  
					+ @fromClause1 
					+ ' ORDER BY  '
					+ @fieldNameSort 
					+' ASC'
		END
		ELSE IF(@sortState = -1)
		BEGIN
			SELECT @SQLStatement1 =	'SELECT ' 
					+ @selectClause1  
					+ ' FROM '  
					+ @fromClause1 
					+ ' ORDER BY  '
					+ @fieldNameSort 
					+' DESC'
		END
	END
	ELSE
	BEGIN

		SELECT @SQLStatement1 =	'SELECT ' 
				+ @selectClause1  
				+ ' FROM '  
				+ @fromClause1

	END
	
	EXEC(@SQLStatement1)
END
GO
GRANT EXECUTE ON  [dbo].[GET_MARKET_PRICE_POINT_SEARCH_DETAILS_P] TO [CBMSApplication]
GO
