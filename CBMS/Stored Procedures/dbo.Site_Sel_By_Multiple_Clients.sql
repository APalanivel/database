SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:        
 dbo.Site_Sel_By_Multiple_Clients      
      
DESCRIPTION:    
    
   Selects the site name(combination city, state , address and actual sitename)  for the given client.    
     
INPUT PARAMETERS:    
 Name    DataType  Default   Description        
------------------------------------------------------------------------------        
 @Client_id    INT    
 @Keyword   VARCHAR(100)  NULL    
 @StartRecordNumber  INT   1    
 @EndRecordNumber  INT          2147483647    
    
    
OUTPUT PARAMETERS:        
 Name    DataType  Default   Description        
------------------------------------------------------------------------------        
    
USAGE EXAMPLES:    
------------------------------------------------------------------------------        
    
EXEC dbo.Site_Sel_By_Multiple_Clients '11552,235','bu'

  
  
AUTHOR INITIALS:    
    
Initials  Name     
------------------------------------------------------------    
 
SLP	   Sri Lakshmi Pallikonda
      
MODIFICATIONS      
       
Initials Date  Modification        
------------------------------------------------------------        
   
SLP  2019-09-27 Created
  
******/
CREATE PROCEDURE [dbo].[Site_Sel_By_Multiple_Clients]
    (
        @Client_Id VARCHAR(MAX)
        , @Keyword VARCHAR(100) = NULL
        , @StartRecordNumber INT = 1
        , @EndRecordNumber INT = 2147483647
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @State_Id INT
            , @Old_Client_Hier_Id INT
            , @Account_Number VARCHAR(50)
            , @Total_Cnt INT;


        WITH CTE_Site
        AS (
               SELECT   TOP (@EndRecordNumber)
                        ch.Site_Id
                        , ch.Site_name
                        , ch.Client_Hier_Id
                        --, RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
                        , ch.Sitegroup_Id AS Division_Id
                        , ROW_NUMBER() OVER (ORDER BY
                                                 Site_name ASC) AS Record_Number
                        , ch.Site_Reference_Number
               FROM
                    Core.Client_Hier ch
                    LEFT JOIN Core.Client_Hier_Account AS cha
                        ON cha.Client_Hier_Id = ch.Client_Hier_Id
               WHERE
                    --ch.Client_Id = @Client_Id

                    (   @Client_Id IS NULL
                        OR  EXISTS (   SELECT
                                            1
                                       FROM
                                            dbo.ufn_split(@Client_Id, ',') us
                                       WHERE
                                            us.Segments = ch.Client_Id))
                    AND ch.Site_Id > 0
                    --AND (   @Site_Managed IS NULL
                    --        OR  ch.Site_Not_Managed = @Site_Managed)
                    --AND (   @division_id IS NULL
                    --        OR  ch.Sitegroup_Id = @division_id)
                    --AND (   @Old_Client_Hier_Id IS NULL
                    --        OR  (   ch.Client_Hier_Id <> @Old_Client_Hier_Id
                    --                AND ch.State_Id = @State_Id))
                    AND (   @Keyword IS NULL
                            OR  ch.Site_name LIKE '%' + @Keyword + '%')
               --AND (   @Site_Reference_Number IS NULL
               --        OR  ch.Site_Reference_Number = @Site_Reference_Number)
               --AND (   @Rate_Id IS NULL
               --        OR  cha.Rate_Id = @Rate_Id)
               --AND (   @Site_State_Id IS NULL
               --        OR  ch.State_Id = @Site_State_Id)
               GROUP BY
                   ch.Site_Id
                   , ch.Client_Hier_Id
                    --, RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
                   , ch.Sitegroup_Id
                   , ch.Site_name
                   , ch.Site_Reference_Number
           )
        SELECT
            cs.Client_Hier_Id
            , cs.Site_Id
            , cs.Site_name
            , cs.Division_Id
            , cs.Site_Reference_Number
        FROM
            CTE_Site AS cs
        WHERE
            cs.Record_Number BETWEEN @StartRecordNumber
                             AND     @EndRecordNumber
        ORDER BY
            cs.Site_name;

    END;

    ;

GO
GRANT EXECUTE ON  [dbo].[Site_Sel_By_Multiple_Clients] TO [CBMSApplication]
GO
