
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME: [dbo].[Site_IDM_Queue_Sel]
     
DESCRIPTION: 
	procedure select idm queue sites
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
 
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        

	 exec Site_IDM_Queue_Sel

AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------
	KVK		Vinay K

MODIFICATIONS

INITIALS	DATE		MODIFICATION
------------------------------------------------------------          
	KVK		12/14/2014	created
	KVK		05/1/2017	Modified to have Node ID exists
*/
CREATE PROCEDURE [dbo].[Site_IDM_Queue_Sel]
AS
BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
            BEGIN TRANSACTION;
            UPDATE
                  siq
            SET
                  siq.Last_Change_Ts = '1900-01-01'
            FROM
                  dbo.Site_IDM_Queue siq
                  JOIN Core.Client_Hier ch
                        ON siq.Client_Hier_Id = ch.Client_Hier_Id
            WHERE
                  ch.IDM_Node_XId IS NOT NULL;


            SELECT
                  ch.Client_Hier_Id
                 ,ch.Client_Id
            FROM
                  dbo.Site_IDM_Queue siq
                  JOIN Core.Client_Hier ch
                        ON siq.Client_Hier_Id = ch.Client_Hier_Id
            WHERE
                  ch.IDM_Node_XId IS NOT NULL;

            COMMIT TRANSACTION;
      END TRY
      BEGIN CATCH 
            IF @@TRANCOUNT > 0
                  ROLLBACK TRANSACTION;

            EXEC dbo.usp_RethrowError
                  @CustomMessage = ''; 
      END CATCH;
END;
;
GO

GRANT EXECUTE ON  [dbo].[Site_IDM_Queue_Sel] TO [CBMSApplication]
GO
