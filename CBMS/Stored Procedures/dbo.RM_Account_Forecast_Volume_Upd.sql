SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********     
NAME:    
   [dbo].[RM_Account_Forecast_Volume_Upd]    

DESCRIPTION:    
	 This procedure used to update the RM_Account_Forecast_Volume table from TVP  

INPUT PARAMETERS:    
 
Name											DataType	Default Description    
------------------------------------------------------------------------------------------   
 @Commodity_Id									INT
 @Updated_User_Id								INT
 @tvp_RM_Account_Forecast_Volume		tvp_RM_Account_Forecast_Volume	        

OUTPUT PARAMETERS:    
Name				DataType  Default Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
	BEGIN TRAN	
		DECLARE @tvp_RM_Account_Forecast_Volume tvp_RM_Account_Forecast_Volume
		INSERT      INTO @tvp_RM_Account_Forecast_Volume
		VALUES
					( 18553, 338505, '2018-02-01', '13556', 25, 'CEMManual' ),
					( 18553, 235412, '2018-02-01', '14444', 25, 'CEMManual' )
		EXEC [dbo].[RM_Account_Forecast_Volume_Upd] 
			  @Commodity_Id = 291
			 ,@Updated_User_Id = 49
			 ,@tvp_RM_Account_Forecast_Volume = @tvp_RM_Account_Forecast_Volume
		EXEC RM_Site_Client_Hier_Forecast_Volume_Sel 11236,291,2,'2018-02-01','2018-02-01',NULL,NULL,'41270'
		EXEC dbo.RM_Account_Forecast_Volume_Sel 11236,291,2,'2018-02-01','2018-02-01',NULL,NULL,'41270'
	ROLLBACK TRAN
	
	BEGIN TRAN	
		DECLARE @tvp_RM_Account_Forecast_Volume tvp_RM_Account_Forecast_Volume
		INSERT      INTO @tvp_RM_Account_Forecast_Volume
		VALUES
					( 11593, 60830, '2018-10-01', '500', 25, 'ContractEditedByCEM' )
		EXEC [dbo].[RM_Account_Forecast_Volume_Upd] 
			  @Commodity_Id = 291
			 ,@Updated_User_Id = 49
			 ,@tvp_RM_Account_Forecast_Volume = @tvp_RM_Account_Forecast_Volume
		EXEC RM_Site_Client_Hier_Forecast_Volume_Sel 10050,291,NULL,'2018-10-01','2018-10-01',NULL,NULL,'15341'
		EXEC dbo.RM_Account_Forecast_Volume_Sel 10050,291,NULL,'2018-10-01','2018-10-01',NULL,NULL,'15341'
	ROLLBACK TRAN

AUTHOR INITIALS:    
Initials	Name    
------------------------------------------------------------    
PRV			Pramod Reddy V


MODIFICATIONS     
 Initials	Date			Modification    
------------------------------------------------------------    
PRV			09-07-2018		SP Created

******/

CREATE PROCEDURE [dbo].[RM_Account_Forecast_Volume_Upd]
      ( 
       @Commodity_Id INT
      ,@Updated_User_Id INT
      ,@tvp_RM_Account_Forecast_Volume AS tvp_RM_Account_Forecast_Volume READONLY )
AS 
BEGIN

      SET NOCOUNT ON;

      DECLARE @Data_Change_Level_Cd INT;

      SELECT
            @Data_Change_Level_Cd = cd.Code_Id
      FROM
            dbo.Code cd
            INNER JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Data Change Level'
            AND cd.Code_Value = 'Account';

      INSERT      INTO Trade.RM_Forecast_Volume_Audit
                  ( 
                   Client_Hier_Id
                  ,Account_Id
                  ,Commodity_Id
                  ,Service_Month
                  ,Previous_Forecast_Volume
                  ,Current_Forecast_Volume
                  ,Previous_Uom_Id
                  ,Current_Uom_Id
                  ,Previous_Volume_Source_Cd
                  ,Current_Volume_Source_Cd
                  ,Previous_Data_Change_Level_Cd
                  ,Current_Data_Change_Level_Cd
                  ,Updated_User_Id
                  ,Last_Change_Ts )
                  SELECT
                        CHF.Client_Hier_Id
                       ,CHF.Account_Id
                       ,CHF.Commodity_Id
                       ,CHF.Service_Month
                       ,CHF.Forecast_Volume
                       ,TVP.Forecast_Volume
                       ,CHF.Uom_Id
                       ,TVP.Uom_Id
                       ,CHF.Volume_Source_Cd
                       ,c.Code_Id
                       ,CHF.Data_Change_Level_Cd
                       ,@Data_Change_Level_Cd
                       ,@Updated_User_Id
                       ,GETDATE()
                  FROM
                        [Trade].[RM_Account_Forecast_Volume] CHF
                        INNER JOIN @tvp_RM_Account_Forecast_Volume TVP
                              ON CHF.Client_Hier_Id = TVP.Client_Hier_Id
                                 AND CHF.Service_Month = TVP.Service_Month
                                 AND CHF.Account_Id = TVP.Account_Id
                        INNER JOIN dbo.Code c
                              ON TVP.Volume_Source_Cd = c.Code_Value
                  WHERE
                        CHF.Commodity_Id = @Commodity_Id;


        --UPDATE
        --    CHF
        --SET
        --    CHF.Forecast_Volume = TVP.Forecast_Volume
        --    , CHF.Uom_Id = TVP.Uom_Id
        --    , CHF.Volume_Source_Cd = c.Code_Id
        --    , CHF.Updated_User_Id = @Updated_User_Id
        --    , CHF.Last_Change_Ts = GETDATE()
        --    , CHF.Data_Change_Level_Cd = @Data_Change_Level_Cd
        --FROM
        --    [Trade].[RM_Account_Forecast_Volume] CHF
        --    INNER JOIN @tvp_RM_Account_Forecast_Volume TVP
        --        ON CHF.Client_Hier_Id = TVP.Client_Hier_Id
        --           AND  CHF.Service_Month = TVP.Service_Month
        --           AND  CHF.Account_Id = TVP.Account_Id
        --    INNER JOIN dbo.Code c
        --        ON TVP.Volume_Source_Cd = c.Code_Value
        --WHERE
        --    CHF.Commodity_Id = @Commodity_Id;


      MERGE Trade.RM_Account_Forecast_Volume AS tgt
            USING 
                  ( SELECT
                        TVP.Client_Hier_Id
                       ,TVP.Service_Month
                       ,TVP.Account_Id
                       ,@Commodity_Id AS Commodity_Id
                       ,TVP.Forecast_Volume
                       ,TVP.Uom_Id
                       ,c.Code_Id
                    FROM
                        @tvp_RM_Account_Forecast_Volume TVP
                        INNER JOIN dbo.Code c
                              ON TVP.Volume_Source_Cd = c.Code_Value ) src
            ON tgt.Client_Hier_Id = src.Client_Hier_Id
                  AND tgt.Service_Month = src.Service_Month
                  AND tgt.Account_Id = src.Account_Id
                  AND tgt.Commodity_Id = src.Commodity_Id
            WHEN MATCHED 
                  THEN UPDATE
                    SET 
                        tgt.Forecast_Volume = src.Forecast_Volume
                       ,tgt.Uom_Id = src.Uom_Id
                       ,tgt.Volume_Source_Cd = src.Code_Id
                       ,tgt.Updated_User_Id = @Updated_User_Id
                       ,tgt.Last_Change_Ts = GETDATE()
                       ,tgt.Data_Change_Level_Cd = @Data_Change_Level_Cd
            WHEN NOT MATCHED 
                  THEN INSERT
                        ( 
                         Client_Hier_Id
                        ,Account_Id
                        ,Commodity_Id
                        ,Service_Month
                        ,Forecast_Volume
                        ,Uom_Id
                        ,Volume_Source_Cd
                        ,Data_Change_Level_Cd
                        ,Created_User_Id
                        ,Created_Ts
                        ,Updated_User_Id
                        ,Last_Change_Ts )
                    VALUES
                        ( 
                         src.Client_Hier_Id
                        ,src.Account_Id
                        ,@Commodity_Id
                        ,src.Service_Month
                        ,src.Forecast_Volume
                        ,src.Uom_Id
                        ,src.Code_Id
                        ,@Data_Change_Level_Cd
                        ,@Updated_User_Id
                        ,GETDATE()
                        ,@Updated_User_Id
                        ,GETDATE() );



      WITH  Cte_Forecast_Volume
              AS ( SELECT
                        SUM(( cuc.CONVERSION_FACTOR * CHF.Forecast_Volume )) AS Forecast_Volume
                       ,CHF.Client_Hier_Id AS Client_Hier_Id
                       ,CHF.Service_Month AS Service_Month
                       ,ISNULL(MIN(CASE WHEN CHF.Volume_Source_Cd > 0 THEN CHF.Volume_Source_Cd
                                   END), 0) AS Volume_Source_Cd
                       ,TVP.Uom_Id
                   FROM
                        [Trade].[RM_Account_Forecast_Volume] CHF
                        INNER JOIN ( SELECT
                                          Client_Hier_Id
                                         ,Service_Month
                                         ,Uom_Id
                                     FROM
                                          @tvp_RM_Account_Forecast_Volume
                                     GROUP BY
                                          Client_Hier_Id
                                         ,Service_Month
                                         ,Uom_Id ) TVP
                              ON CHF.Client_Hier_Id = TVP.Client_Hier_Id
                                 AND CHF.Service_Month = TVP.Service_Month
                        LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                              ON cuc.BASE_UNIT_ID = CHF.Uom_Id
                                 AND cuc.CONVERTED_UNIT_ID = TVP.Uom_Id
                   WHERE
                        CHF.Commodity_Id = @Commodity_Id
                   GROUP BY
                        CHF.Client_Hier_Id
                       ,CHF.Service_Month
                       ,TVP.Uom_Id)
            --UPDATE
        --    CHF
        --SET
        --    CHF.Forecast_Volume = CFV.Forecast_Volume
        --    , CHF.Uom_Id = CFV.Uom_Id
        --    , CHF.Volume_Source_Cd = CFV.Volume_Source_Cd
        --    , CHF.Updated_User_Id = @Updated_User_Id
        --    , CHF.Last_Change_Ts = GETDATE()
        --    , CHF.Data_Change_Level_Cd = @Data_Change_Level_Cd
        --FROM
        --    [Trade].[RM_Client_Hier_Forecast_Volume] CHF
        --    INNER JOIN Cte_Forecast_Volume CFV
        --        ON CHF.Client_Hier_Id = CFV.Client_Hier_Id
        --           AND  CHF.Service_Month = CFV.Service_Month
        --WHERE
        --    CHF.Commodity_Id = @Commodity_Id;




        MERGE Trade.RM_Client_Hier_Forecast_Volume AS tgt
            USING 
                  ( SELECT
                        cf.Forecast_Volume
                       ,cf.Uom_Id
                       ,cf.Volume_Source_Cd
                       ,cf.Client_Hier_Id
                       ,cf.Service_Month
                       ,@Commodity_Id AS Commodity_Id
                    FROM
                        Cte_Forecast_Volume cf ) src
            ON tgt.Client_Hier_Id = src.Client_Hier_Id
                  AND tgt.Service_Month = src.Service_Month
                  AND tgt.Commodity_Id = src.Commodity_Id
            WHEN MATCHED 
                  THEN UPDATE
                    SET 
                        tgt.Forecast_Volume = src.Forecast_Volume
                       ,tgt.Uom_Id = src.Uom_Id
                       ,tgt.Volume_Source_Cd = src.Volume_Source_Cd
                       ,tgt.Updated_User_Id = @Updated_User_Id
                       ,tgt.Last_Change_Ts = GETDATE()
                       ,tgt.Data_Change_Level_Cd = @Data_Change_Level_Cd
            WHEN NOT MATCHED 
                  THEN INSERT
                        ( 
                         Client_Hier_Id
                        ,Commodity_Id
                        ,Service_Month
                        ,Forecast_Volume
                        ,Uom_Id
                        ,Volume_Source_Cd
                        ,Data_Change_Level_Cd
                        ,Created_User_Id
                        ,Created_Ts
                        ,Updated_User_Id
                        ,Last_Change_Ts )
                    VALUES
                        ( 
                         src.Client_Hier_Id
                        ,@Commodity_Id
                        ,src.Service_Month
                        ,src.Forecast_Volume
                        ,src.Uom_Id
                        ,src.Volume_Source_Cd
                        ,@Data_Change_Level_Cd
                        ,@Updated_User_Id
                        ,GETDATE()
                        ,@Updated_User_Id
                        ,GETDATE() ) ;

END;






GO
GRANT EXECUTE ON  [dbo].[RM_Account_Forecast_Volume_Upd] TO [CBMSApplication]
GO
