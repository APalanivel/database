SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Service_Condition_Template_Sel_By_Sr_Rfp]
           
DESCRIPTION:             
			To get service condition templates matching RFP commodity and accounts geography 
			
INPUT PARAMETERS:            
	Name		DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Sr_Rfp_Id	INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	EXEC dbo.Sr_Service_Condition_Template_Sel_By_Sr_Rfp  13258
	EXEC dbo.Sr_Service_Condition_Template_Sel_By_Sr_Rfp  13245
	EXEC dbo.Sr_Service_Condition_Template_Sel_By_Sr_Rfp  13263
	EXEC dbo.Sr_Service_Condition_Template_Sel_By_Sr_Rfp  13299
	EXEC dbo.Sr_Service_Condition_Template_Sel_By_Sr_Rfp  13268
	EXEC dbo.Sr_Service_Condition_Template_Sel_By_Sr_Rfp  13254
	EXEC dbo.Sr_Service_Condition_Template_Sel_By_Sr_Rfp  13298
	EXEC dbo.Sr_Service_Condition_Template_Sel_By_Sr_Rfp  13273
			
		
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-03-28	Global Sourcing - Phase3 - GCS-564 Created

******/
CREATE PROCEDURE [dbo].[Sr_Service_Condition_Template_Sel_By_Sr_Rfp] ( @Sr_Rfp_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Country_Cnt INT
      
      DECLARE @Acc_Country_Commodity AS TABLE
            ( 
             Country_Id INT
            ,Commodity_Id INT
            ,Id INT IDENTITY(1, 1) )
            
      DECLARE @Templates AS TABLE
            ( 
             Sr_Service_Condition_Template_Id INT
            ,Template_Name NVARCHAR(255)
            ,Country_Id INT
            ,Cnt INT )
      
      INSERT      INTO @Acc_Country_Commodity
                  ( 
                   Country_Id
                  ,Commodity_Id )
                  SELECT
                        cha.Meter_Country_Id
                       ,rfp.COMMODITY_TYPE_ID
                  FROM
                        dbo.SR_RFP_ACCOUNT rfpacc
                        INNER JOIN dbo.SR_RFP rfp
                              ON rfpacc.SR_RFP_ID = rfp.SR_RFP_ID
                        INNER JOIN Core.Client_Hier_Account cha
                              ON rfpacc.ACCOUNT_ID = cha.Account_Id
                                 AND cha.Commodity_Id = rfp.COMMODITY_TYPE_ID
                  WHERE
                        rfp.SR_RFP_ID = @SR_RFP_ID
                  GROUP BY
                        cha.Meter_Country_Id
                       ,rfp.COMMODITY_TYPE_ID
                       
      SELECT
            @Country_Cnt = count(DISTINCT Country_Id)
      FROM
            @Acc_Country_Commodity
                       
                      
      INSERT      INTO @Templates
                  ( 
                   Sr_Service_Condition_Template_Id
                  ,Template_Name
                  ,Country_Id
                  ,Cnt )
                  SELECT
                        ssct.Sr_Service_Condition_Template_Id
                       ,ssct.Template_Name
                       ,sppcm.Country_Id
                       ,rank() OVER ( PARTITION BY ssct.Sr_Service_Condition_Template_Id ORDER BY sppcm.Country_Id ) AS Cnt
                  FROM
                        dbo.Sr_Service_Condition_Template ssct
                        INNER JOIN dbo.Sr_Service_Condition_Template_Product_Map ssctpm
                              ON ssct.Sr_Service_Condition_Template_Id = ssctpm.Sr_Service_Condition_Template_Id
                        INNER JOIN dbo.SR_Pricing_Product_Country_Map sppcm
                              ON ssctpm.SR_Pricing_Product_Country_Map_Id = sppcm.SR_Pricing_Product_Country_Map_Id
                        INNER JOIN dbo.SR_PRICING_PRODUCT spp
                              ON sppcm.SR_Pricing_Product_Id = spp.SR_PRICING_PRODUCT_ID
                  WHERE
                        EXISTS ( SELECT
                                    1
                                 FROM
                                    @Acc_Country_Commodity acc
                                 WHERE
                                    spp.COMMODITY_TYPE_ID = acc.Commodity_Id
                                    AND sppcm.Country_Id = acc.Country_Id )
                  GROUP BY
                        ssct.Sr_Service_Condition_Template_Id
                       ,ssct.Template_Name
                       ,sppcm.Country_Id
                       
                       
     
      SELECT
            tmplts.Sr_Service_Condition_Template_Id
           ,tmplts.Template_Name
      FROM
            @Templates tmplts
      WHERE
            Cnt = @Country_Cnt
      GROUP BY
            tmplts.Sr_Service_Condition_Template_Id
           ,tmplts.Template_Name
                
                  
            
END;
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Service_Condition_Template_Sel_By_Sr_Rfp] TO [CBMSApplication]
GO
