SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.GET_ALL_CHARGES_FOR_MONTH_P 
	@rate_id INT,  
	@rs_start_date DATETIME,  
	@rs_end_date DATETIME,  
	@entity_type INT,  
	@entity_name VARCHAR(200)  
AS  
BEGIN  
  
	SET NOCOUNT ON  
  
	SELECT cm.charge_master_id  
		, cm.charge_type_id  
		, chType.entity_name  
		, cm.charge_display_id  
	FROM dbo.rate_schedule rs INNER JOIN dbo.charge_master cm  ON cm.charge_parent_id = rs.rate_id  
		INNER JOIN dbo.entity chType ON chType.entity_id = cm.charge_type_id  
		INNER JOIN dbo.entity parent ON parent.entity_id = cm.charge_parent_type_id  
	WHERE ( rs.rate_id = @rate_id 
		AND ( (rs.rs_start_date <= @rs_start_date AND rs.rs_end_date >= @rs_end_date) 
				OR (rs.rs_start_date <= @rs_end_date  and rs.rs_end_date >= @rs_end_date)     
				OR (rs.rs_start_date >= @rs_start_date AND rs.rs_end_date <= @rs_end_date)  
				OR (rs.rs_start_date <= @rs_start_date AND rs.rs_end_date >= @rs_start_date)  
				OR (rs.rs_start_date <= @rs_start_date AND rs.rs_end_date IS NULL)  
			)    
		  )
	AND parent.entity_type = @entity_type
	AND parent.entity_name = @entity_name  
   
END
GO
GRANT EXECUTE ON  [dbo].[GET_ALL_CHARGES_FOR_MONTH_P] TO [CBMSApplication]
GO
