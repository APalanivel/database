SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE  PROCEDURE [dbo].[cbmsInvoiceParticipation_GetAllSiteMonthsForClient]
	( @MyAccountId int
	, @client_id int
	)
AS
BEGIN


   select distinct ip.site_id
	, ip.service_month
     from client cl with (nolock)
     join division d with (nolock) on d.client_id = cl.client_id
     join site s with (nolock) on s.division_id = d.division_id
     join invoice_participation ip on ip.site_id = s.site_id
    where cl.client_id = @client_id

END



GO
GRANT EXECUTE ON  [dbo].[cbmsInvoiceParticipation_GetAllSiteMonthsForClient] TO [CBMSApplication]
GO
