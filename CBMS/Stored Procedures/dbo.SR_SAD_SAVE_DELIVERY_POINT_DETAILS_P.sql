SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
--select * from SR_DT_DELIVERY_POINT where SR_DEAL_TICKET_ID=721


CREATE     PROCEDURE dbo.SR_SAD_SAVE_DELIVERY_POINT_DETAILS_P
@userId varchar(10),
@sessionId varchar(20),
@srDealTicketId int,
@deliveryPointId int,
@deliveryPointName varchar(400),
@pricingMethodology varchar(500)


AS
	set nocount on
IF (select count(1) from SR_DT_DELIVERY_POINT where SR_DEAL_TICKET_ID = @srDealTicketId and
    DELIVERY_POINT_NUMBER = @deliveryPointId ) = 0

BEGIN	
	insert into SR_DT_DELIVERY_POINT
		(SR_DEAL_TICKET_ID, DELIVERY_POINT_NUMBER, DELIVERY_POINT_NAME, PRICING_METHODOLOGY)
	
	values	(@srDealTicketId, @deliveryPointId, @deliveryPointName, @pricingMethodology)
END

ELSE

BEGIN

	update SR_DT_DELIVERY_POINT set DELIVERY_POINT_NUMBER=@deliveryPointId, DELIVERY_POINT_NAME=@deliveryPointName,
		PRICING_METHODOLOGY=@pricingMethodology
	where 
		SR_DEAL_TICKET_ID=@srDealTicketId


END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_SAVE_DELIVERY_POINT_DETAILS_P] TO [CBMSApplication]
GO
