SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE                                          procedure 
[dbo].[cbmsBudgetDetail_SearchDetails]

(
	  @MyAccountId int
	, @client_id int = null
	, @division_id int = null
	, @site_id int = null
	, @year int = null
	, @commodity_type_id int = null
	, @currency_unit_id int = null
	, @view_type varchar(20) = null
	, @ng_unit_of_measure_type_id int = null
	, @budget_name varchar(400) = null
)
as




declare @currency_factor decimal (18,5)
declare @currency_unit_name varchar(10)
declare @budget_id int

if @year is null
  set @year = datepart(yyyy,getdate())






select top 1 @budget_id = budget_id 
from budget
 where client_id = @client_id
   and budget_start_year = @year
   and commodity_type_id = @commodity_type_id
   and budget_name = isnull(@budget_name, budget_name)
   order by budget_date desc



select @currency_unit_name = currency_unit_name from currency_unit 
 where currency_unit_id = @currency_unit_id


--select @currency_unit_name
if @currency_unit_name = 'USD' 
  begin
    set @currency_factor = 1
  end
  else
  begin
    select @currency_factor = conversion_factor 
      from budget_currency_map 
     where budget_id = @budget_id
		 and currency_unit_id = @currency_unit_id
	--select @currency_factor
	if @currency_factor is null
		begin
		select  @currency_factor  = conversion_factor from currency_unit_conversion 
			where base_unit_id = @currency_unit_id	
			and converted_unit_id = 3
			and conversion_date = '1/1/' + cast(@year as varchar(4))
			and Currency_Group_Id = (select currency_group_id from client where client_id = @client_id)		
		end
  end

if @ng_unit_of_measure_type_id is null 
  set @ng_unit_of_measure_type_id = 25

-------------------------------------------------------------------
-------------------------------------------------------------------

-------------------------------------------------------------------
-------------------------------------------------------------------
--CLIENT VIEW
if @division_id is null and @site_id is null and @client_id is not null
  begin
	select    x.division_name
		, x.division_id
		, x.month
		, x.budget_id
		, sum(x.usage) * cconng.conversion_factor usage
		, sum(x.total_cost) / @currency_factor total_cost
		, sum(x.other_unit) / @currency_factor other_unit
		, sum(x.other_fixed) / @currency_factor other_fixed
		, ((sum(x.commodity_generation) / @currency_factor) / CASE @view_type WHEN 'Unit_Cost' THEN case sum(x.usage) WHEN 0 THEN 1 ELSE sum(x.usage) END  ELSE 1 END ) / CASE @view_type WHEN 'Unit_Cost' THEN cconng.conversion_factor else 1 end commodity_generation
		, (case sum(x.usage) when 0 then 0 else sum(x.total_cost) / (sum(x.usage) * cconng.conversion_factor) end ) / @currency_factor as [unit_cost]
		, ((sum(x.transmission) / @currency_factor )  / CASE @view_type WHEN 'Unit_Cost' THEN case sum(x.usage) WHEN 0 THEN 1 ELSE sum(x.usage) END  ELSE 1 END ) / CASE @view_type WHEN 'Unit_Cost' THEN cconng.conversion_factor else 1 end transmission
		, ((sum(x.distribution) / @currency_factor)  / CASE @view_type WHEN 'Unit_Cost' THEN case sum(x.usage) WHEN 0 THEN 1 ELSE sum(x.usage) END  ELSE 1 END ) / CASE @view_type WHEN 'Unit_Cost' THEN cconng.conversion_factor else 1 end distribution
		, ((sum(x.transportation) / @currency_factor)   / CASE @view_type WHEN 'Unit_Cost' THEN case sum(x.usage) WHEN 0 THEN 1 ELSE sum(x.usage) END  ELSE 1 END ) / CASE @view_type WHEN 'Unit_Cost' THEN cconng.conversion_factor else 1 end transportation
		, ((sum(x.other_cost) / @currency_factor )    / CASE @view_type WHEN 'Unit_Cost' THEN case sum(x.usage) WHEN 0 THEN 1 ELSE sum(x.usage) END  ELSE 1 END ) / CASE @view_type WHEN 'Unit_Cost' THEN cconng.conversion_factor else 1 end other_cost
		, ((sum(x.taxes) / @currency_factor )    / CASE @view_type WHEN 'Unit_Cost' THEN case sum(x.usage) WHEN 0 THEN 1 ELSE sum(x.usage) END  ELSE 1 END ) / CASE @view_type WHEN 'Unit_Cost' THEN cconng.conversion_factor else 1 end taxes
		, 'budget_account_id' = ''
	     from(
	 	select  d.division_id
			, d.division_name
			, convert(varchar(7), bd.month_identifier, 111) as [month]
			, isnull(variable_value,0)   *  isnull(budget_usage, isnull(bu.volume,0)) as commodity_generation
			, ((isnull(variable_value,0) + isnull(rates_tax_value,0) + isnull(sourcing_tax_value,0) + isnull(transportation_value,0) + isnull(transmission_value,0) + isnull(distribution_value,0) + isnull(other_bundled_value,0)) * isnull(budget_usage, isnull(bu.volume,0))) + isnull(other_fixed_costs_value,0) as [total_cost]
			, ((isnull(other_bundled_value,0) * isnull(budget_usage, isnull(bu.volume,0))) + isnull(other_fixed_costs_value,0)) as other_cost --/ CASE @view_type WHEN 'Unit_Cost' THEN case usage WHEN 0 THEN 1 ELSE usage END  ELSE 1 END  as other_cost
			, isnull(budget_usage, isnull(bu.volume,0)) usage
			, isnull(transmission_value,0)  *  isnull(budget_usage, isnull(bu.volume,0))   as transmission 
			, isnull(distribution_value,0) *  isnull(budget_usage, isnull(bu.volume,0)) as distribution 
			, isnull(transportation_value,0) *  isnull(budget_usage, isnull(bu.volume,0)) as transportation
			, b.budget_id
			, isnull(other_bundled_value,0) other_unit
			, isnull(other_fixed_costs_value,0) other_fixed
			, (isnull(rates_tax_value,0) + isnull(sourcing_tax_value,0)) * isnull(budget_usage, isnull(bu.volume,0)) as taxes
	    	    from client c
   		    join division d on (d.client_id = c.client_id and d.not_managed = 0 and d.division_id = coalesce(@division_id, d.division_id))
		    join site s on (s.division_id = d.division_id and s.not_managed = 0 and s.closed = 0 and s.site_id = coalesce(@site_id, s.site_id))
		    join address a on a.address_parent_id = s.site_id
		
		    join (
			select acct.account_id, m.address_id			
			  from account acct
			  join meter m on m.account_id = acct.account_id
			where acct.not_managed = 0
			and acct.account_type_id = 38
		
			union
		
			select acct.account_id,  m.address_id
			from meter m
			join supplier_account_meter_map samm on samm.meter_id = m.meter_id
			join account acct on acct.account_id = samm.account_id
			where acct.not_managed = 0
			) as acct on acct.address_id = a.address_id
		join budget_account ba on ba.account_id = acct.account_id and ba.is_deleted = 0
     left outer join budget_details bd on bd.budget_account_id = ba.budget_account_id
		join budget b on b.budget_id = ba.budget_id
     left outer join budget_usage bu on (bu.account_id = ba.account_id)  and (bd.month_identifier = bu.month_identifier) and (b.commodity_type_id = bu.commodity_type_id)
		where c.client_id = coalesce(@client_id, c.client_id)
		  and b.budget_start_year = coalesce(@year, b.budget_start_year)
		  and b.commodity_type_id = coalesce(@commodity_type_id, b.commodity_type_id)
		  and b.budget_id = @budget_id
		  --and (bd.budget_usage is not null or bu.volume is not null)
	group by 
		  d.division_id
		, d.division_name
		, convert(varchar(7), bd.month_identifier, 111)
		,(isnull(variable_value,0))   *   isnull(budget_usage, isnull(bu.volume,0))
		,((isnull(variable_value,0) + isnull(rates_tax_value,0) + isnull(sourcing_tax_value,0) + isnull(transportation_value,0) + isnull(transmission_value,0) + isnull(distribution_value,0) + isnull(other_bundled_value,0)) *  isnull(budget_usage, isnull(bu.volume,0))) + isnull(other_fixed_costs_value,0) 
		, ((isnull(other_bundled_value,0) *  isnull(budget_usage, isnull(bu.volume,0))) + isnull(other_fixed_costs_value,0))
		,  isnull(budget_usage, isnull(bu.volume,0))
		, bu.volume
		, isnull(transmission_value,0)  *   isnull(budget_usage, isnull(bu.volume,0))   
		, isnull(distribution_value,0) *   isnull(budget_usage, isnull(bu.volume,0)) 
		, isnull(transportation_value,0) *   isnull(budget_usage, isnull(bu.volume,0)) 
		, b.budget_id
		, isnull(other_bundled_value,0)
		, isnull(other_fixed_costs_value,0)
		--	, isnull(rates_tax_value,0) + isnull(sourcing_tax_value,0) * isnull(budget_usage, isnull(bu.volume,0)) 
		, (isnull(rates_tax_value,0) + isnull(sourcing_tax_value,0)) * isnull(budget_usage, isnull(bu.volume,0))

		) x
	    join consumption_unit_conversion cconng on (cconng.base_unit_id = 25 and cconng.converted_unit_id = @ng_unit_of_measure_type_id) 
	group by x.division_id
		, x.division_name
		, x.month
		, x.budget_id
		, cconng.conversion_factor
	order by  x.division_name
		, x.month
  end
-------------------------------------------------------------------
-------------------------------------------------------------------

-------------------------------------------------------------------
-------------------------------------------------------------------
--DIVISION VIEW
if @site_id is null and @division_id is not null
  begin

	select 
		x.site_name
		, x.site_id
		, x.month
		, x.budget_id
		, sum(x.usage) * cconng.conversion_factor usage
		, sum(x.total_cost) / @currency_factor total_cost
		, sum(x.other_unit) / @currency_factor other_unit
		, sum(x.other_fixed) / @currency_factor other_fixed
		, (case sum(x.usage) when 0 then 0 else sum(x.total_cost) / (sum(x.usage) * cconng.conversion_factor) end ) / @currency_factor as [unit_cost]
		, ((sum(x.commodity_generation) / @currency_factor) / CASE @view_type WHEN 'Unit_Cost' THEN case sum(x.usage) WHEN 0 THEN 1 ELSE sum(x.usage) END  ELSE 1 END) / CASE @view_type WHEN 'Unit_Cost' THEN cconng.conversion_factor else 1 end commodity_generation
		, ((sum(x.transmission) / @currency_factor )  / CASE @view_type WHEN 'Unit_Cost' THEN case sum(x.usage) WHEN 0 THEN 1 ELSE sum(x.usage) END  ELSE 1 END ) / CASE @view_type WHEN 'Unit_Cost' THEN cconng.conversion_factor else 1 end transmission
		, ((sum(x.distribution) / @currency_factor)  / CASE @view_type WHEN 'Unit_Cost' THEN case sum(x.usage) WHEN 0 THEN 1 ELSE sum(x.usage) END  ELSE 1 END ) / CASE @view_type WHEN 'Unit_Cost' THEN cconng.conversion_factor else 1 end distribution
		, ((sum(x.transportation) / @currency_factor)   / CASE @view_type WHEN 'Unit_Cost' THEN case sum(x.usage) WHEN 0 THEN 1 ELSE sum(x.usage) END  ELSE 1 END ) / CASE @view_type WHEN 'Unit_Cost' THEN cconng.conversion_factor else 1 end transportation
		, ((sum(x.other_cost) / @currency_factor)   / CASE @view_type WHEN 'Unit_Cost' THEN case sum(x.usage) WHEN 0 THEN 1 ELSE sum(x.usage) END  ELSE 1 END ) / CASE @view_type WHEN 'Unit_Cost' THEN cconng.conversion_factor else 1 end other_cost
		, ((sum(x.taxes) / @currency_factor )    / CASE @view_type WHEN 'Unit_Cost' THEN case sum(x.usage) WHEN 0 THEN 1 ELSE sum(x.usage) END  ELSE 1 END ) / CASE @view_type WHEN 'Unit_Cost' THEN cconng.conversion_factor else 1 end taxes
		, 'budget_account_id' = ''
	from(

		select  s.site_name
			, s.site_id
			, acct.account_id
			, convert(varchar(7), bd.month_identifier, 111) as [month]
			,(isnull(variable_value,0))   *   isnull(budget_usage, isnull(bu.volume,0)) as commodity_generation
			,(((isnull(variable_value,0)) + isnull(rates_tax_value,0) + isnull(sourcing_tax_value,0) + isnull(transportation_value,0) + isnull(transmission_value,0) + isnull(distribution_value,0) + isnull(other_bundled_value,0)) *  isnull(budget_usage, isnull(bu.volume,0))) + isnull(other_fixed_costs_value,0) as [total_cost]
			, ((isnull(other_bundled_value,0) *  isnull(budget_usage, isnull(bu.volume,0))) + isnull(other_fixed_costs_value,0)) as other_cost --/ CASE @view_type WHEN 'Unit_Cost' THEN case usage WHEN 0 THEN 1 ELSE usage END  ELSE 1 END  as other_cost
			, isnull(budget_usage, isnull(bu.volume,0)) usage
			, isnull(transmission_value,0)  *   isnull(budget_usage, isnull(bu.volume,0))   as transmission 
			, isnull(distribution_value,0) *   isnull(budget_usage, isnull(bu.volume,0)) as distribution 
			, isnull(transportation_value,0) *   isnull(budget_usage, isnull(bu.volume,0)) as transportation
			, b.budget_id
			, isnull(other_bundled_value,0) other_unit
			, isnull(other_fixed_costs_value,0) other_fixed
			, (isnull(rates_tax_value,0) + isnull(sourcing_tax_value,0)) * isnull(budget_usage, isnull(bu.volume,0)) as taxes

		     from client c
		     join division d on (d.client_id = c.client_id and d.not_managed = 0 and d.division_id = coalesce(@division_id, d.division_id))		     join site s on (s.division_id = d.division_id and s.not_managed = 0 and s.closed = 0 and s.site_id = coalesce(@site_id, s.site_id))
		     join address a on a.address_parent_id = s.site_id
		     join	(select acct.account_id, m.address_id			
				from account acct
				join meter m on m.account_id = acct.account_id
				where acct.not_managed = 0
				and acct.account_type_id = 38
			
				union
			
				select acct.account_id,  m.address_id
				from meter m
				join supplier_account_meter_map samm on samm.meter_id = m.meter_id
				join account acct on acct.account_id = samm.account_id
				where acct.not_managed = 0
				) as acct on acct.address_id = a.address_id
		join budget_account ba on ba.account_id = acct.account_id  and ba.is_deleted = 0
     left outer join budget_details bd on bd.budget_account_id = ba.budget_account_id
     		join budget b on b.budget_id = ba.budget_id
     left outer join budget_usage bu on (bu.account_id = ba.account_id)  and (bd.month_identifier = bu.month_identifier) and (b.commodity_type_id = bu.commodity_type_id)
		    where c.client_id = coalesce(@client_id, c.client_id)
		      and b.budget_start_year = coalesce(@year, b.budget_start_year)
		      and b.commodity_type_id = coalesce(@commodity_type_id, b.commodity_type_id)
		  and b.budget_id = @budget_id
		-- and (bd.budget_usage is not null or bu.volume is not null)
		 group by 
			s.site_name
			, s.site_id
		,convert(varchar(7), bd.month_identifier, 111) 
		,(isnull(variable_value,0))   *   isnull(budget_usage, isnull(bu.volume,0))
		,(((isnull(variable_value,0)) + isnull(rates_tax_value,0) + isnull(sourcing_tax_value,0) + isnull(transportation_value,0) + isnull(transmission_value,0) + isnull(distribution_value,0) + isnull(other_bundled_value,0)) *  isnull(budget_usage, isnull(bu.volume,0))) + isnull(other_fixed_costs_value,0) 
			, ((isnull(other_bundled_value,0) *  isnull(budget_usage, isnull(bu.volume,0))) + isnull(other_fixed_costs_value,0))
			,  isnull(budget_usage, isnull(bu.volume,0))
			, isnull(transmission_value,0)  *   isnull(budget_usage, isnull(bu.volume,0))   
			, isnull(distribution_value,0) *   isnull(budget_usage, isnull(bu.volume,0)) 
			, isnull(transportation_value,0) *   isnull(budget_usage, isnull(bu.volume,0)) 
			, b.budget_id
			, isnull(other_bundled_value,0)
			, isnull(other_fixed_costs_value,0)
			, acct.account_id
			--, isnull(rates_tax_value,0) + isnull(sourcing_tax_value,0) * isnull(budget_usage, isnull(bu.volume,0)) 
			, (isnull(rates_tax_value,0) + isnull(sourcing_tax_value,0)) * isnull(budget_usage, isnull(bu.volume,0))


			
		) x
	    join consumption_unit_conversion cconng on (cconng.base_unit_id = 25 and cconng.converted_unit_id = @ng_unit_of_measure_type_id) 	
	group by x.site_id
		, x.month
		, x.site_name
		, x.budget_id
		, cconng.conversion_factor
	order by  x.site_name
		, x.month

  end
-------------------------------------------------------------------
-------------------------------------------------------------------

--select * from account where account_number like '%00034-36002%'
--select * from budget_Detail where account_id = 22301

-------------------------------------------------------------------
-------------------------------------------------------------------
--SITE VIEW
if @site_id is not null
  begin

	select 
		  x.account_number 		, x.account_id
		, x.month
		, x.budget_id
		, sum(x.usage) * x.conversion_factor usage
		, sum(x.commodity_generation) / @currency_factor commodity_generation
		, sum(x.total_cost) / @currency_factor total_cost
		, sum(x.transmission) / @currency_factor transmission
		, sum(x.distribution) / @currency_factor distribution
		, sum(x.transportation) / @currency_factor transportation
		, sum(x.other_unit) / @currency_factor other_unit
		, sum(x.other_fixed) / @currency_factor other_fixed
		, x.account_number_exact
		, (((sum(x.other_unit) * sum(x.usage))  + sum(x.other_fixed)) / CASE @view_type WHEN 'Unit_Cost' THEN CASE sum(x.usage) WHEN 0 THEN 1 ELSE sum(x.usage) END  ELSE 1 END  ) / @currency_factor as [other_cost]
		, (case sum(x.usage) when 0 then 0 else sum(x.total_cost) / (sum(x.usage) * x.conversion_factor) end ) / @currency_factor as [unit_cost]
		, sum(x.taxes) / @currency_factor taxes
		, x.budget_account_id

	from(

		select  v.vendor_name + ', ' + acct.account_number + ', ' + a.address_line1 + ', ' + a.city + ', ' + st.state_name + ' ' + a.zipcode  as account_number
			, acct.account_id
			,convert(varchar(7), bd.month_identifier, 111) as month
			,((isnull(variable_value,0) + isnull(rates_tax_value,0) + isnull(sourcing_tax_value,0) + isnull(transportation_value,0) + isnull(transmission_value,0) + isnull(distribution_value,0) + isnull(other_bundled_value,0)) *   isnull(budget_usage, isnull(bu.volume,0))) + isnull(other_fixed_costs_value,0) as [total_cost]
			,(((isnull(variable_value,0) + isnull(rates_tax_value,0) + isnull(sourcing_tax_value,0) + isnull(transportation_value,0) + isnull(transmission_value,0) + isnull(distribution_value,0) + isnull(other_bundled_value,0)) *  isnull(budget_usage, isnull(bu.volume,0))) + isnull(other_fixed_costs_value,0))  / CASE  isnull(budget_usage, isnull(bu.volume,0)) WHEN 0 THEN 1 ELSE  isnull(budget_usage, isnull(bu.volume,0)) END [unit_cost]
			, isnull(budget_usage,isnull(bu.volume,0)) usage
			, CASE @view_type WHEN 'Unit_Cost' THEN isnull(variable_value,0)  / cconng.conversion_factor ELSE isnull(variable_value,0) *  isnull(budget_usage, isnull(bu.volume,0)) END as commodity_generation
			, CASE @view_type WHEN 'Unit_Cost' THEN isnull(transmission_value,0) / cconng.conversion_factor ELSE isnull(transmission_value,0) *  isnull(budget_usage, isnull(bu.volume,0)) END as transmission
			, CASE @view_type WHEN 'Unit_Cost' THEN isnull(distribution_value,0) / cconng.conversion_factor ELSE isnull(distribution_value,0) *  isnull(budget_usage, isnull(bu.volume,0)) END as distribution
			, CASE @view_type WHEN 'Unit_Cost' THEN isnull(transportation_value,0) / cconng.conversion_factor ELSE isnull(transportation_value,0) *  isnull(budget_usage, isnull(bu.volume,0)) END as transportation 
			, b.budget_id
			, isnull(other_bundled_value,0) other_unit
			, isnull(other_fixed_costs_value,0) other_fixed
			, acct.account_number as account_number_exact
			, cconng.conversion_factor
--			, isnull(rates_tax_value,0) + isnull(sourcing_tax_value,0) taxes
			, CASE @view_type WHEN 'Unit_Cost' THEN (isnull(isnull(rates_tax_value,0) + isnull(sourcing_tax_value,0) ,0)) / cconng.conversion_factor ELSE (isnull(isnull(rates_tax_value,0) + isnull(sourcing_tax_value,0) ,0)) *  isnull(budget_usage, isnull(bu.volume,0)) END as taxes 
			, ba.budget_account_id
		from client c
		join division d on (d.client_id = c.client_id and d.not_managed = 0 and d.division_id = coalesce(@division_id, d.division_id))
		join site s on (s.division_id = d.division_id and s.not_managed = 0 and s.closed = 0 and s.site_id = coalesce(@site_id, s.site_id))
		join address a on a.address_parent_id = s.site_id
		join state st on st.state_id = a.state_id

		join	(select acct.account_id, m.address_id, acct.account_number, acct.vendor_id--, a.address_line1, a.city, a.state			
			from account acct
			join meter m on m.account_id = acct.account_id
		--	join address a on m.address_id = a.address_id
		--	join state st
			where acct.not_managed = 0
			and acct.account_type_id = 38
			union
		
			select acct.account_id,  m.address_id, acct.account_number, acct.vendor_id
			from meter m
			join supplier_account_meter_map samm on samm.meter_id = m.meter_id
			join account acct on acct.account_id = samm.account_id
			where acct.not_managed = 0
			) as acct on acct.address_id = a.address_id
		join budget_account ba on ba.account_id = acct.account_id  and ba.is_deleted = 0
     left outer join budget_details bd on bd.budget_account_id = ba.budget_account_id
		join budget b on b.budget_id = ba.budget_id
		join vendor v on v.vendor_id = acct.vendor_id
		join consumption_unit_conversion cconng on (cconng.base_unit_id = 25 and cconng.converted_unit_id = @ng_unit_of_measure_type_id) 
     left outer join budget_usage bu on (bu.account_id = ba.account_id) and (bd.month_identifier = bu.month_identifier) and (b.commodity_type_id = bu.commodity_type_id)
 		where c.client_id = coalesce(@client_id, c.client_id)
		  and b.budget_start_year = coalesce(@year, b.budget_start_year)
		  and b.commodity_type_id = coalesce(@commodity_type_id, b.commodity_type_id)
		  and b.budget_id = @budget_id
		 --and (bd.budget_usage is not null or bu.volume is not null)
		--group by d.division_id, convert(varchar(7), bd.date, 111)
		--order by d.division_id--convert(varchar(7), bd.date, 111)
		group by 
			v.vendor_name + ', ' + acct.account_number + ', ' + a.address_line1 + ', ' + a.city + ', ' + st.state_name + ' ' + a.zipcode  
			, acct.account_id
			,convert(varchar(7), bd.month_identifier, 111) 
			,((isnull(variable_value,0) + isnull(rates_tax_value,0) + isnull(sourcing_tax_value,0) + isnull(transportation_value,0) + isnull(transmission_value,0) + isnull(distribution_value,0) + isnull(other_bundled_value,0)) *  isnull(budget_usage, isnull(bu.volume,0))) + isnull(other_fixed_costs_value,0) 
			,(((isnull(variable_value,0) + isnull(rates_tax_value,0) + isnull(sourcing_tax_value,0) +  isnull(transportation_value,0) + isnull(transmission_value,0) + isnull(distribution_value,0) + isnull(other_bundled_value,0)) *  isnull(budget_usage, isnull(bu.volume,0))) + isnull(other_fixed_costs_value,0))  / CASE  isnull(budget_usage, isnull(bu.volume,0)) WHEN 0 THEN 1 ELSE  isnull(budget_usage, isnull(bu.volume,0)) END 
			, isnull(budget_usage,isnull(bu.volume,0)) 			, CASE @view_type WHEN 'Unit_Cost' THEN isnull(variable_value,0) / cconng.conversion_factor ELSE isnull(variable_value,0) *  isnull(budget_usage, isnull(bu.volume,0)) END 
			, CASE @view_type WHEN 'Unit_Cost' THEN isnull(transmission_value,0) / cconng.conversion_factor ELSE isnull(transmission_value,0) *  isnull(budget_usage, isnull(bu.volume,0)) END 
			, CASE @view_type WHEN 'Unit_Cost' THEN isnull(distribution_value,0) / cconng.conversion_factor ELSE isnull(distribution_value,0) *  isnull(budget_usage, isnull(bu.volume,0)) END 
			, CASE @view_type WHEN 'Unit_Cost' THEN isnull(transportation_value,0) / cconng.conversion_factor ELSE isnull(transportation_value,0) *  isnull(budget_usage, isnull(bu.volume,0)) END 
			, b.budget_id
			, isnull(other_bundled_value,0)
			, isnull(other_fixed_costs_value,0)
			, acct.account_number 
			, cconng.conversion_factor
			--, isnull(rates_tax_value,0) + isnull(sourcing_tax_value,0)
			, CASE @view_type WHEN 'Unit_Cost' THEN (isnull(isnull(rates_tax_value,0) + isnull(sourcing_tax_value,0) ,0)) / cconng.conversion_factor ELSE (isnull(isnull(rates_tax_value,0) + isnull(sourcing_tax_value,0) ,0)) *  isnull(budget_usage, isnull(bu.volume,0)) END 
			, ba.budget_account_id
		) x
	group by x.account_id
		, x.month
		, x.account_number
		, x.budget_id
		, x.account_number_exact
		, x.conversion_factor
		, x.budget_account_id
	order by  x.account_number
		, x.account_id
		, x.month
	


end
-------------------------------------------------------------------
-------------------------------------------------------------------

















































GO
GRANT EXECUTE ON  [dbo].[cbmsBudgetDetail_SearchDetails] TO [CBMSApplication]
GO
