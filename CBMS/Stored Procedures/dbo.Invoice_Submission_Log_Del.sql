SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******************************************************************************************************                        
NAME: dbo.Invoice_Submission_Log_Del
    
DESCRIPTION:    
    
      Deletes record from Invoice_Submission_Log
    
INPUT PARAMETERS:    
      NAME                                      DATATYPE    DEFAULT           DESCRIPTION    
------------------------------------------------------------------------    

      
OUTPUT PARAMETERS:              
      NAME              DATATYPE    DEFAULT           DESCRIPTION 
-----------------------------------------------------------      



------------------------------------------------------------              
USAGE EXAMPLES:              
------------------------------------------------------------            

	Exec Invoice_Submission_Log_Del
         
AUTHOR INITIALS:              
      INITIALS    NAME              
------------------------------------------------------------              
		HK			Harish Kurma
		      
MODIFICATIONS:    
      INITIALS    DATE			MODIFICATION              
------------------------------------------------------------              
      HK		  10 29 2018	Created
******************************************************************************************************/

CREATE PROCEDURE [dbo].[Invoice_Submission_Log_Del] 
	(   
       @Invoice_Submission_Log_Id INT  
     )  
AS
BEGIN

	SET NOCOUNT ON;

    DELETE FROM  
            Invoice_Submission_Log  
      WHERE  
           Invoice_Submission_Log_Id = @Invoice_Submission_Log_Id  
              
END
GO
GRANT EXECUTE ON  [dbo].[Invoice_Submission_Log_Del] TO [CBMSApplication]
GO
