SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_DT_MAPPED_ACCOUNT_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@srDealTicketId	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE     PROCEDURE dbo.SR_SAD_GET_DT_MAPPED_ACCOUNT_DETAILS_P
@srDealTicketId int
AS
set nocount on

select 
	acc.ACCOUNT_ID, acc.ACCOUNT_NUMBER, st.SITE_ID, st.SITE_NAME

from 
	SR_DEAL_TICKET_ACCOUNT_MAP sdtam, ACCOUNT acc, site st

where
sdtam.ACCOUNT_ID = acc.ACCOUNT_ID and
st.SITE_ID = acc.SITE_ID and
sdtam.SR_DEAL_TICKET_ID = @srDealTicketId
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_DT_MAPPED_ACCOUNT_DETAILS_P] TO [CBMSApplication]
GO
