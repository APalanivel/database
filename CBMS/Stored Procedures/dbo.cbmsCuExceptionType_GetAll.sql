SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsCuExceptionType_GetAll

DESCRIPTION:

INPUT PARAMETERS:
	Name			DataType		Default				Description
--------------------------------------------------------------------------------
	@MyAccountId   	INT       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default				Description
--------------------------------------------------------------------------------

USAGE EXAMPLES:
--------------------------------------------------------------------------------

EXEC dbo.cbmsCuExceptionType_GetAll 49

AUTHOR INITIALS:
	Initials			Name
--------------------------------------------------------------------------------
	NR					Narayana Reddy

MODIFICATIONS

	Initials	Date		Modification
--------------------------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	NR			14-08-2018	DATA2.0 - Added IS_Active Filter condition.
	NR			2018-10-08	DATA2.0 - Phase-2 - Added Individual_User_Info_Id,Username columns.
******/


CREATE PROCEDURE [dbo].[cbmsCuExceptionType_GetAll]
    (
        @MyAccountId INT
    )
AS
    BEGIN


        SELECT
            cet.CU_EXCEPTION_TYPE_ID
            , cet.EXCEPTION_TYPE
            , cet.EXCEPTION_GROUP_TYPE_ID
            , cet.MANAGED_BY_GROUP_INFO_ID
            , cet.STOP_PROCESSING
            , cet.IS_ROUTED_REASON
            , cet.SORT_ORDER
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Username
            , cet.Managed_By_User_Info_Id
        FROM
            dbo.CU_EXCEPTION_TYPE cet WITH (NOLOCK)
            LEFT JOIN dbo.USER_INFO ui
                ON ui.USER_INFO_ID = cet.Managed_By_User_Info_Id
        WHERE
            cet.Is_Active = 1
        ORDER BY
            cet.EXCEPTION_TYPE;



    END;




GO
GRANT EXECUTE ON  [dbo].[cbmsCuExceptionType_GetAll] TO [CBMSApplication]
GO
