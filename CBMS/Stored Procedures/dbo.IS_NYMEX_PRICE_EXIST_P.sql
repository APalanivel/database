SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE dbo.IS_NYMEX_PRICE_EXIST_P 
@userId varchar(10),
@sessionId varchar(20),
@dateOfNymexPrice datetime

as
	set nocount on
	select	COUNT(RM_NYMEX_DATA_ID)
		
	from	RM_NYMEX_DATA
	
	where BATCH_EXECUTION_DATE = @dateOfNymexPrice
GO
GRANT EXECUTE ON  [dbo].[IS_NYMEX_PRICE_EXIST_P] TO [CBMSApplication]
GO
