SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[cbmsInvoiceParticipation_GetForAccountSiteMonth]
	( @MyAccountId int
	, @account_id int
	, @site_id int
	, @service_month datetime
	)
AS
BEGIN

	   select ip.invoice_participation_id
		, ip.account_id
		, ip.site_id
		, ip.service_month
		, ip.is_expected
		, ip.is_received
		, ip.recalc_under_review
		, ip.variance_under_review
	     from invoice_participation ip WITH (NOLOCK) 
	    where ip.account_id = @account_id
	      and ip.site_id = @site_id
	      and ip.service_month = @service_month

END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvoiceParticipation_GetForAccountSiteMonth] TO [CBMSApplication]
GO
