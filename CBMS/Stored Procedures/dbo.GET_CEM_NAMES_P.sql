SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE dbo.GET_CEM_NAMES_P 
@userId varchar,
@sessionId varchar
as

select	userinfo.USER_INFO_ID CEM_USER_ID ,
	userinfo.LAST_NAME,
	userinfo.FIRST_NAME

from	RM_CEM_TEAM rcem,
	USER_INFO userinfo

where	 rcem.CEM_USER_ID=userinfo.USER_INFO_ID 

UNION 

select	userinfo.USER_INFO_ID,
	userinfo.LAST_NAME,
	userinfo.FIRST_NAME

from	RM_CEM_TEAM rcem,
	USER_INFO userinfo

where	rcem.CEM_PRACTICE_HEAD_ID=userinfo.USER_INFO_ID 

order by LAST_NAME
GO
GRANT EXECUTE ON  [dbo].[GET_CEM_NAMES_P] TO [CBMSApplication]
GO
