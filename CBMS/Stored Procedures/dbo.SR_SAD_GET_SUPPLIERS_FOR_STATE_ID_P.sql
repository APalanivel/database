SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_SUPPLIERS_FOR_STATE_ID_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@stateId       	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

-- select * from state where state_name = 'AZ'
-- exec  dbo.SR_SAD_GET_SUPPLIERS_FOR_STATE_ID_P '-1','-1','4'
-- select * from entity where entity_id = 288

CREATE    PROCEDURE dbo.SR_SAD_GET_SUPPLIERS_FOR_STATE_ID_P(
@userId varchar,
@sessionId varchar,
@stateId int

) AS
set nocount on
select v.vendor_id, v.vendor_name 
from vendor v, 
vendor_state_map vsm 
where 
vsm.vendor_id=v.vendor_id and vsm.state_id= @stateId
and v.vendor_type_id=288

order by v.vendor_name
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_SUPPLIERS_FOR_STATE_ID_P] TO [CBMSApplication]
GO
