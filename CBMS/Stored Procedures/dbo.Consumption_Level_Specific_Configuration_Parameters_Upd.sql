SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******              
Name:   dbo.Consumption_Level_Specific_Configuration_Parameters_Upd      
              
Description:              
			
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
  @Variance_Consumption_Level_Id		INT
  @Variance_Extraction_Service_Param_Id INT
  @Param_Value							NVARCHAR(255)
  @Uom_Cd								INT
  @User_Info_Id							INT
                 
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
---------------------------------------------------------------------------------------- 
EXEC Consumption_Level_Specific_Configuration_Parameters_Upd 

    
      
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna
	Ap				Arunkumar Palanivel
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
	RKV             2020-01-09      Created for AVT. 
	AP				Feb 11 2020		Modified procedure based on the new UI screen changes	 
             
******/
CREATE PROCEDURE [dbo].[Consumption_Level_Specific_Configuration_Parameters_Upd]
      (
      @Commodity_id                    INT
    , @TVP_Consumption_Level_Params AS TVP_Consumption_Level_Params READONLY
    , @User_Info_Id                    INT )
AS
      BEGIN
            SET NOCOUNT ON;


            DECLARE @Variance_consumption_Extraction_Service_param_value TABLE
                  ( [Category]                  [VARCHAR](50)  NULL
                  , [Average_Consumption_Level] [VARCHAR](200) NULL
                  , Param_name                  VARCHAR(50)
                  , Param_value                 VARCHAR(10));

            INSERT INTO @Variance_consumption_Extraction_Service_param_value
                        --unpivot the value and update it
                        SELECT
                              unpvt.Category
                            , unpvt.Average_Consumption_Level
                            , CASE unpvt.Param_name
                                   WHEN 'AA_Distance'
                                         THEN 'Analogous_Account_Distance_value'
                                   WHEN 'AA_Max_Accounts'
                                         THEN 'Analogous_Account_Max_Account_Required'
                                   WHEN 'AA_Min_Accounts'
                                         THEN 'Analogous_Account_Min_Account_Required'
                                   WHEN 'Error_Metric'
                                         THEN 'Error_Metric'
                                   WHEN 'Linear_Regression_Sigma_Value'
                                         THEN 'Linear_Regression_Sigma_value'
                              END AS Param_Name
                            , unpvt.Param_value
                        FROM
                              (     SELECT
                                          *
                                    FROM  @TVP_Consumption_Level_Params ) p
                              UNPIVOT
                                    (     Param_value
                                          FOR Param_name IN (
                                                AA_Distance
                                              , AA_Max_Accounts
                                              , AA_Min_Accounts
                                              , Error_Metric
                                              , Linear_Regression_Sigma_Value )) AS unpvt;


											  SELECT * FROM @Variance_consumption_Extraction_Service_param_value 

            UPDATE
                  vcespv
            SET
                  vcespv.Last_Change_Ts = getdate()
                , vcespv.Param_Value = T.Param_value
                , vcespv.Updated_User_Id = @User_Info_Id
            FROM  dbo.Variance_Consumption_Extraction_Service_Param_Value vcespv
                  JOIN
                  dbo.Variance_Extraction_Service_Param vesp
                        ON vesp.Variance_Extraction_Service_Param_Id = vcespv.Variance_Extraction_Service_Param_Id
                  JOIN
                  dbo.Variance_Consumption_Level vcl
                        ON vcl.Variance_Consumption_Level_Id = vcespv.Variance_Consumption_Level_Id
                  JOIN
                  dbo.Code c
                        ON c.Code_Id = vcespv.Variance_Category_Cd
                  JOIN
                  dbo.Commodity cm
                        ON cm.Commodity_Id = vcl.Commodity_Id
                  JOIN
                  @Variance_consumption_Extraction_Service_param_value T
                        ON T.Average_Consumption_Level = vcl.Consumption_Level_Desc
            WHERE vcl.Commodity_Id = @Commodity_id
                  AND   T.Category = c.Code_Value
                  AND   T.Param_name = vesp.Param_Name;

      END;
GO
GRANT EXECUTE ON  [dbo].[Consumption_Level_Specific_Configuration_Parameters_Upd] TO [CBMSApplication]
GO
