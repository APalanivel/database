SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_DEAL_TICKET_REALLOCATION_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@dealTicketId  	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE           PROCEDURE dbo.GET_DEAL_TICKET_REALLOCATION_INFO_P 

@userId varchar(10),
@sessionId varchar(20),
@dealTicketId int 

AS
set nocount on
	SELECT 
	rmdt.RM_DEAL_TICKET_ID, 
	rmdt.CLIENT_ID, 
	rmdt.DIVISION_ID, 
	rmdt.SITE_ID,
	vw.SITE_NAME,
	rmdt.HEDGE_TYPE_ID, 
	e.ENTITY_NAME AS HEDGE_TYPE, 
	rmdt.HEDGE_LEVEL_TYPE_ID, 
	e1.ENTITY_NAME AS HEDGE_LEVEL_TYPE, 
	rmdt.HEDGE_MODE_TYPE_ID, 
	e2.ENTITY_NAME AS HEDGE_MODE_TYPE_TEXT, 
	e3.ENTITY_NAME AS ALLOCATION_TYPE_TEXT,
	e3.ENTITY_ID AS ALLOCATION_TYPE_ID,
	rmdtd.MONTH_IDENTIFIER, 
	rmdtd.TOTAL_VOLUME, 
	month(rmdtd.MONTH_IDENTIFIER) AS DEAL_MONTH, 
	year(rmdtd.MONTH_IDENTIFIER) AS DEAL_YEAR,
	rmdt.UNIT_TYPE_ID,
	e4.entity_name UNIT,
	rmdt.CURRENCY_TYPE_ID,
	currency.CURRENCY_UNIT_NAME CURRENCY,
	e5.entity_name FREQUENCY_TEXT,
	rmdt.RM_GROUP_ID,
	currency.CURRENCY_UNIT_NAME,
	e6.ENTITY_NAME AS PRICING_POINT_TYPE,
	pin.PRICING_POINT,
	e7.ENTITY_NAME AS INDEX_NAME  
	
FROM 
	RM_DEAL_TICKET rmdt
	LEFT JOIN vwSiteName vw ON (rmdt.SITE_ID = vw.SITE_ID), 
	RM_DEAL_TICKET_DETAILS rmdtd,  
	ENTITY e, 
	ENTITY e1, 
	ENTITY e2,
	ENTITY e3,
	ENTITY e4,
	ENTITY e5,
	CURRENCY_UNIT currency,
	ENTITY e6,
	PRICE_INDEX pin,
	ENTITY e7

WHERE 
	rmdt.RM_DEAL_TICKET_ID = @dealTicketId AND 
	rmdt.RM_DEAL_TICKET_ID = rmdtd.RM_DEAL_TICKET_ID AND 
	rmdt.HEDGE_TYPE_ID = e.ENTITY_ID AND 
	rmdt.HEDGE_LEVEL_TYPE_ID = e1.ENTITY_ID AND 
	rmdt.HEDGE_MODE_TYPE_ID = e2.ENTITY_ID and
	rmdt.ALLOCATION_TYPE_ID = e3.ENTITY_ID and
	rmdt.UNIT_TYPE_ID  = e4.ENTITY_ID and
	currency.CURRENCY_UNIT_ID=rmdt.CURRENCY_TYPE_ID and
	rmdt.frequency_type_id=e5.entity_id AND
	rmdt.PRICING_REQUEST_TYPE_ID = e6.ENTITY_ID AND
	rmdt.PRICE_INDEX_ID = pin.PRICE_INDEX_ID AND
	pin.INDEX_ID = e7.ENTITY_ID
	

ORDER BY rmdtd.MONTH_IDENTIFIER
GO
GRANT EXECUTE ON  [dbo].[GET_DEAL_TICKET_REALLOCATION_INFO_P] TO [CBMSApplication]
GO
