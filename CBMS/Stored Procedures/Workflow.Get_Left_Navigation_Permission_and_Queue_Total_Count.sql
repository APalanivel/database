SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


     
/******                  
NAME:    [Workflow].[Get_Left_Navigation_Permission_and_Queue_Total_Count]              
DESCRIPTION:  it'll Return the Left Navigation Codunt details for each Modules              
------------------------------------------------------------                 
 INPUT PARAMETERS:                  
 Name   DataType  Default Description                  
 @userID   VARCHAR(MAX),                   
  @moduleID INT                  
------------------------------------------------------------                  
 OUTPUT PARAMETERS:                  
 Name   DataType  Default Description                  
------------------------------------------------------------                  
 USAGE EXAMPLES:                  
 EXEC workflow.Get_Left_Navigation_Permission_and_Queue_Total_Count @userID   =49, @moduleID =3    
 EXEC workflow.Get_Left_Navigation_Permission_and_Queue_Total_Count @userID   =49, @moduleID =5           
------------------------------------------------------------                  
AUTHOR INITIALS:                  
Initials Name                  
------------------------------------------------------------                  
TRK   Ramakrishna Thummala Summit Energy               
               
 MODIFICATIONS                   
 Initials Date   Modification                  
------------------------------------------------------------                  
 TRK    Sep-2019  Created              
              
******/              
             
CREATE PROCEDURE [Workflow].[Get_Left_Navigation_Permission_and_Queue_Total_Count]                      
(                       
  @userID   VARCHAR(MAX),                       
  @moduleID INT                 
)                      
AS                      
BEGIN                   
                  
DECLARE @queue_id_input INT ,                  
  @priority_input INT ,                  
  @TOTAL_COUNT INT,                      
  @Workflow_Sub_Queue_Name NVARCHAR(255) ,                  
  @PAGE_ROW_COUNT INT,              
  @Date_IN_QUEUE_COUNT INT      
      
                  
SET @queue_id_input  = (SELECT queue_id FROM user_info WHERE user_info_id =@userid )                     
                   
 CREATE TABLE #Left_Navigation_Permissions                      
  (                      
   [ID] INT,    
   QID INT ,                  
   [PARENTID] INT,                      
   [Workflow_Sub_Queue_Name] NVARCHAR(255) NOT NULL,                       
   [Workflow_Queue_Id] INT NOT NULL,                      
   [PERMISSION_NAME] VARCHAR(200),                      
   [TOTAL_COUNT] INT,                      
   CSS NVARCHAR(255),                      
   Is_Default_View BIT,                      
   Is_Saved_Filter INT                      
  )                      
                      
 ;WITH Left_Navigation_Permissions AS (                  
 SELECT  DISTINCT                  
   WQS.workflow_sub_queue_id        AS ID,   
   WQS.Workflow_Sub_Queue_Id        AS QID,                    
   WQS.parent_workflow_sub_queue_id AS PARENTID,                       
   WQS.workflow_sub_queue_name,                        
   WQS.workflow_queue_id,                        
   NULL AS TOTAL_COUNT,                      
   WQS.CSS,                      
   WQS.Is_Default_View,                      
   0 AS Is_Saved_Filter                  
 FROM workflow.workflow_sub_queue WQS                  
 INNER JOIN permission_info P WITH (NOLOCK) ON WQS.permission_info_id = P.permission_info_id                  
 INNER JOIN group_info_permission_info_map AS GIPM WITH (NOLOCK) ON P.permission_info_id = GIPM.permission_info_id                  
 INNER JOIN group_info GI ON GIPM.group_info_id = GI.group_info_id                  
 INNER JOIN user_info_group_info_map UIG ON GI.group_info_id = UIG.group_info_id                   
 INNER JOIN user_info U ON U.user_info_id = UIG.user_info_id                  
 WHERE WQS.Workflow_Queue_Id = @moduleID AND U.USER_INFO_ID=@userID                  
 UNION ALL                  
 SELECT  DISTINCT                   
   WQS.workflow_sub_queue_id        AS ID, 
   WQS.Parent_Workflow_Sub_Queue_Id AS QID,                      
   WQS.parent_workflow_sub_queue_id AS PARENTID,      
   WQS.workflow_sub_queue_name,                        
   WQS.workflow_queue_id,                        
   NULL AS TOTAL_COUNT,                      
   WQS.CSS,                      
   WQS.Is_Default_View,                      
   0 AS Is_Saved_Filter                     
 FROM  workflow.workflow_sub_queue WQS WITH(NOLOCK)                   
 WHERE  WQS.Workflow_Queue_Id = @moduleID   AND WQS.Permission_Info_Id = -1                 
 UNION ALL                  
    SELECT                       
 NULL AS ID, 
 NULL AS QID,                      
 NULL AS parentid,                                           
 WQSFQ.Saved_Filter_Name AS workflow_sub_queue_name,                       
 WSQ.Workflow_Queue_Id,                       
 NULL AS TOTAL_COUNT,                      
 NULL AS CSS,                      
 NULL AS Is_Default_View,                      
 1 AS Is_Saved_Filter                      
 FROM Workflow.Workflow_Queue_Saved_Filter_Query AS WQSFQ    WITH(NOLOCK)   
 INNER JOIN  Workflow.Workflow_Sub_Queue AS WSQ ON WQSFQ.Workflow_Sub_Queue_Id = WSQ.Workflow_Sub_Queue_Id                 

 AND      
  WSQ.Workflow_Sub_Queue_Name='Saved Filters'                      
 WHERE WQSFQ.Created_User_Id=@userID   AND WSQ.Workflow_Queue_Id=@moduleID      
 AND WQSFQ.Expiration_Dt >= CONVERT(char(10), GetDate(),126)                   
 )                  
 SELECT  DISTINCT                  
	 ID  
	,ID AS QID                     
    ,CASE WHEN A.[PARENTID] IS NULL THEN WQSFQ.Workflow_Sub_Queue_Id                      
     ELSE A.[PARENTID] END [PARENTID]                       
    ,workflow_sub_queue_name                       
    ,workflow_queue_id                        
    ,TOTAL_COUNT                      
    ,CSS                      
    ,Is_Default_View                      
    ,Is_Saved_Filter                      
    INTO #Temp_ResultSet                       
    FROM Left_Navigation_Permissions A                      
    LEFT JOIN workflow.Workflow_Queue_Saved_Filter_Query AS WQSFQ WITH(NOLOCK) ON A.[Workflow_Sub_Queue_Name]=WQSFQ.Saved_Filter_Name                      
    ORDER BY ID;                 
      
      
 DECLARE @TEMP TABLE       
 (      
 RN INT IDENTITY (1,1),      
 ID INT,
 QID INT,      
 [PARENTID] INT,      
 workflow_sub_queue_name NVARCHAR(255),      
 workflow_queue_id INT,      
 TOTAL_COUNT INT,      
 CSS NVARCHAR(255),      
 Is_Default_View INT,      
 Is_Saved_Filter INT      
 )      
 INSERT INTO @TEMP
 (
     ID,
     QID,
     PARENTID,
     workflow_sub_queue_name,
     workflow_queue_id,
     TOTAL_COUNT,
     CSS,
     Is_Default_View,
     Is_Saved_Filter
 )
 
 SELECT       
  ID  AS ID 
  ,QID AS QID   
 ,[PARENTID]  AS   PARENTID          
 ,workflow_sub_queue_name    AS workflow_sub_queue_name                   
    ,workflow_queue_id        AS workflow_queue_id                
    ,TOTAL_COUNT    AS TOTAL_COUNT                  
    ,CSS      AS CSS
    ,ISNULL(Is_Default_View,0)AS Is_Default_View                     
    ,Is_Saved_Filter     AS Is_Saved_Filter  
       
 FROM #Temp_ResultSet WHERE ID IS NOT NULL ORDER BY ID ASC       
      
      
 INSERT INTO @TEMP
 (
     ID,
     QID,
     PARENTID,
     workflow_sub_queue_name,
     workflow_queue_id,
     TOTAL_COUNT,
     CSS,
     Is_Default_View,
     Is_Saved_Filter
 )
 
 SELECT       
	 ID  AS ID
	 ,QID AS QID
    ,[PARENTID]   AS [PARENTID]           
 ,workflow_sub_queue_name   AS workflow_sub_queue_name                     
    ,workflow_queue_id        AS workflow_queue_id                
    ,TOTAL_COUNT     AS TOTAL_COUNT                 
    ,CSS     AS CSS 
    ,ISNULL(Is_Default_View,0) AS Is_Default_View                     
    ,Is_Saved_Filter   AS Is_Saved_Filter   
                       
    FROM #Temp_ResultSet WITH(NOLOCK)  WHERE ID IS NULL                       
    ORDER BY workflow_sub_queue_name       
      
 INSERT INTO #Left_Navigation_Permissions                      
    (        
    ID , 
	QID,                  
    PARENTID,                      
    Workflow_Sub_Queue_Name,                       
    Workflow_Queue_Id,                       
    TOTAL_COUNT,                      
    CSS,                      
    Is_Default_View,                      
    Is_Saved_Filter       
 )      
 SELECT       
	 RN AS ID  
	,QID    AS QID
  
	,PARENTID       
	,workflow_sub_queue_name                       
    ,workflow_queue_id                        
    ,TOTAL_COUNT                      
    ,CSS      
    ,ISNULL(Is_Default_View,0)Is_Default_View                     
    ,Is_Saved_Filter      
       
      
 FROM @TEMP      
      
               
 DECLARE @Get_Left_Navigation_Permission_and_Queue_Total_Count TABLE                       
  (                      
  ID INT IDENTITY(1,1),                      
  Total_Count INT ,                      
  Workflow_Sub_Queue_Name  NVARCHAR(255)                      
  )                      
                      
                 
   INSERT INTO @Get_Left_Navigation_Permission_and_Queue_Total_Count               
   (              
   Workflow_Sub_Queue_Name ,              
   Total_Count              
   )                  
SELECT 'All My Exceptions',count (DISTINCT CUD.CU_invoice_id) FROM dbo.CU_EXCEPTION_DENORM CUD 
WHERE cud.QUEUE_ID =@queue_id_input 
AND cud.EXCEPTION_TYPE <> 'FAILED RECALC'

   INSERT INTO @Get_Left_Navigation_Permission_and_Queue_Total_Count               
   (              
   Workflow_Sub_Queue_Name ,              
   Total_Count              
   )                  
SELECT 'My Priority Queue', count (DISTINCT CUD.CU_invoice_id) FROM dbo.CU_EXCEPTION_DENORM CUD 
JOIN Workflow.Cu_Invoice_Attribute CUI
ON CUD.CU_INVOICE_ID = CUI.CU_INVOICE_ID
WHERE cud.QUEUE_ID= @queue_id_input
AND cui.Is_Priority =1 
AND cud.EXCEPTION_TYPE <> 'FAILED RECALC'                  
                             
 
 -- this is the new procedure created to get the total count       
IF EXISTS (SELECT 1 FROM #Left_Navigation_Permissions WHERE Workflow_Sub_Queue_Name ='All Open Exceptions')      
 BEGIN      
 INSERT INTO @Get_Left_Navigation_Permission_and_Queue_Total_Count          
  (              
   Workflow_Sub_Queue_Name ,              
   Total_Count              
   )         
   EXEC [Workflow].[Workflow_Queue_Exceptions_Global_Total_Count]       
   END                 
                      
                      
 UPDATE LNP2 SET LNP2.TOTAL_COUNT= GLNPAQTC.Total_Count                      
 FROM #Left_Navigation_Permissions AS LNP2                       
INNER JOIN @Get_Left_Navigation_Permission_and_Queue_Total_Count AS GLNPAQTC ON LNP2.Workflow_Sub_Queue_Name=GLNPAQTC.Workflow_Sub_Queue_Name                      
     
 SELECT     
  Y.ID,
  Y.QID,    
  Y.PARENTID,    
  Y.SF_ID,    
  Y.Workflow_Sub_Queue_Name,    
  Y.Workflow_Queue_Id,    
  Y.PERMISSION_NAME,    
  Y.TOTAL_COUNT,    
  Y.CSS,    
  Y.Is_Default_View,    
  Y.Is_Saved_Filter,    
  Y.IS_Temporary_Filter     
 FROM (    
 SELECT DISTINCT     
  X.ID, 
  CASE WHEN X.QID IS NULL THEN WQSFQ.Workflow_Queue_Saved_Filter_Query_Id 
  ELSE X.QID END AS QID,   
  X.PARENTID,    
  X.Workflow_Sub_Queue_Name,    
  X.Workflow_Queue_Id,    
  X.PERMISSION_NAME,    
  X.TOTAL_COUNT,    
  X.CSS,    
  X.Is_Default_View,    
  X.Is_Saved_Filter,    
  WQSFQ.Workflow_Queue_Saved_Filter_Query_Id AS SF_ID,    
  cast(CASE WHEN   WQSFQ.Saved_Filter_Type_Cd IS NOT NULL AND C.Code_Value='TempUserDefinedFilter' THEN 1 ELSE 0 END AS BIT) AS IS_Temporary_Filter,    
  row_number() OVER (PARTITION BY X.Workflow_Sub_Queue_Name ORDER BY X.ID) AS RN    
     
 FROM (                
 SELECT                       
   LNP.ID   
  ,LNP.QID                   
  ,LNP.[PARENTID]                           
  ,LNP.workflow_sub_queue_name                       
  ,LNP.workflow_queue_id                       
  ,LNP.PERMISSION_NAME                       
  ,isnull(LNP.TOTAL_COUNT,0) AS TOTAL_COUNT                      
  ,LNP.CSS                      
  ,LNP.Is_Default_View                      
  ,cast(LNP.Is_Saved_Filter AS BIT) AS Is_Saved_Filter                           
    
 FROM #Left_Navigation_Permissions AS LNP  WITH(NOLOCK)                     
 )X     
 LEFT JOIN Workflow.Workflow_Queue_Saved_Filter_Query AS WQSFQ ON X.Workflow_Sub_Queue_Name=WQSFQ.Saved_Filter_Name  
 LEFT JOIN DBO.Code C ON C.Code_ID= WQSFQ.Saved_Filter_Type_Cd      
 )Y WHERE RN=1    
     
  DROP TABLE #Left_Navigation_Permissions                         
 DROP TABLE #Temp_ResultSet                      
 --DROP TABLE #Workflow_Queue_Saved_Filter_Query_Id                      
                      
END                                                                                                                                                                                                                                                                                                                                                   
GO
GRANT EXECUTE ON  [Workflow].[Get_Left_Navigation_Permission_and_Queue_Total_Count] TO [CBMSApplication]
GO
