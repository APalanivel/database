SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	cbms_prod.dbo.cbmsAccountAudit_Save

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@AccountAuditId	int       	null      	
	@AccountAuditBatchId	int       	          	
	@AccountId     	int       	          	
	@AuditStatusTypeId	int       	          	
	@MonthToReview 	varchar(50)	null      	
	@AuditIssue    	varchar(4000)	null      	
	@ResponseImageId	int       	null      	
	@ClosedDate    	datetime  	null      	
	@ClosedReasonTypeId	int       	null      	
	@AuditComment  	varchar(4000)	null      	
	@AuditorId     	int       	          	
	@EventTrack    	int       	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	
	EXEC dbo.cbmsAccountAudit_Save
		@AccountAuditId = 10000
		,@AccountAuditBatchId = 300
		,@AccountId = 10673
		,@AuditStatusTypeId = 1251
		,@MonthToReview = 'Feb 2005'
		,@AuditIssue = 'I''m not sure how cost/usage is reported on this account, but the unit cost in this month seems very high.'
		,@ResponseImageId = 360868
		,@ClosedDate = '2006-07-06 09:31:09.200'
		,@ClosedReasonTypeId = 1049
		,@AuditorId = 5558
		,@EventTrack = NULL

	SELECT * FROM Account_AUdit WHEre Account_Audit_Id = 10000
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan G

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script
	HG			4/27/2010	script modified to remove the unused @myaccount_id parameter from the calling procedure cbmsAccountAudit_Get and this procedure.
							unused @EventDate parameter removed from this procedure.
							Using MERGE in place Insert/update.
 DMR		  09/10/2010 Modified for Quoted_Identifier

******/
CREATE PROC dbo.cbmsAccountAudit_Save
	(@AccountAuditId INT = NULL
	, @AccountAuditBatchId INT
	, @AccountId INT
	, @AuditStatusTypeId INT
	, @MonthToReview VARCHAR(50) = NULL
	, @AuditIssue VARCHAR(4000) = NULL
	, @ResponseImageId INT = NULL
	, @ClosedDate DATETIME = NULL
	, @ClosedReasonTypeId INT = NULL
	, @AuditComment VARCHAR(4000) = NULL
	, @AuditorId INT
	, @EventDate DATETIME = NULL
	, @EventTrack INT = NULL
	)
AS

BEGIN

	SET NOCOUNT ON

	DECLARE @Tbl_Account_Audit_Id TABLE (Account_Audit_id INT)
	DECLARE @Is_Update BIT

	SET @Is_Update =(
						CASE
							WHEN @AccountAuditId IS NULL THEN 1
							ELSE 0
						END
					 )

	MERGE INTO dbo.Account_Audit aa
		USING
			(
				SELECT
					@AccountAuditId Account_Audit_Id
			) src
		ON
			src.Account_Audit_Id = aa.Account_Audit_Id
	WHEN MATCHED THEN
		UPDATE
			SET account_audit_batch_id = @AccountAuditBatchId
				, account_id = @AccountId
				, audit_status_type_id = @AuditStatusTypeId
				, month_to_review = @MonthToReview
				, audit_issue = @AuditIssue
				, response_image_id = @ResponseImageId
				, closed_date = @ClosedDate
				, closed_reason_type_id = @ClosedReasonTypeId
				, audit_comment = @AuditComment
				, auditor_id = @AuditorId
	WHEN NOT MATCHED THEN
		INSERT
			( account_audit_batch_id
			, account_id
			, audit_status_type_id
			, month_to_review
			, audit_issue
			, response_image_id
			, closed_date
			, closed_reason_type_id
			, audit_comment
			, auditor_id
			)
		VALUES
			( @AccountAuditBatchId
			, @AccountId
			, @AuditStatusTypeId
			, @MonthToReview
			, @AuditIssue
			, @ResponseImageId
			, @ClosedDate
			, @ClosedReasonTypeId
			, @AuditComment
			, @AuditorId
			)
	OUTPUT inserted.Account_Audit_Id INTO @Tbl_Account_Audit_Id
	;

	SELECT @AccountAuditId = Account_Audit_Id FROM @Tbl_Account_Audit_Id

	IF ISNULL(@EventTrack, 1) != 0
		BEGIN

			INSERT INTO dbo.Account_Audit_Tracking
				( account_audit_id
				, audit_status_type_id
				, event_date
				, closed_reason_type_id
				)
			VALUES
				( 
				@AccountAuditId
				, @AuditStatusTypeId
				, GETDATE()
				, @ClosedReasonTypeId
				)
		END				

	EXEC dbo.cbmsAccountAudit_Get @AccountAuditId

END
GO
GRANT EXECUTE ON  [dbo].[cbmsAccountAudit_Save] TO [CBMSApplication]
GO
