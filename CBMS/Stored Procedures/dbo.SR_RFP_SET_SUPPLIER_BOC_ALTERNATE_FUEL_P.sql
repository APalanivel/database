SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_SET_SUPPLIER_BOC_ALTERNATE_FUEL_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@fuelId int,
@buyerLiquidateTypeId varchar(10),
@comments varchar(4000),
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    
@fuelIdentityId int outPUT


USAGE EXAMPLES:
------------------------------------------------------------
---- Check the entry before and after update of records at SV
--select * from sv..sr_rfp_alternate_fuel where sr_rfp_alternate_fuel_id = (select max(sr_rfp_alternate_fuel_id) from sv..sr_rfp_alternate_fuel)

---- Test update an entry
--exec [dbo].[SR_RFP_SET_SUPPLIER_BOC_ALTERNATE_FUEL_P] 
--@fuelId = 194738,
--@buyerLiquidateTypeId = NULL,
--@comments 'Test Data'  -- Original Value = NULL,
--@fuelIdentityId = NULL


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[SR_RFP_SET_SUPPLIER_BOC_ALTERNATE_FUEL_P] 

@fuelId int,
@buyerLiquidateTypeId varchar(10),
@comments varchar(4000),
@fuelIdentityId int outPUT

as
	
set nocount on

if @fuelId=0

BEGIN
	
	INSERT INTO SR_RFP_ALTERNATE_FUEL
	(
		BUYER_LIQUIDATE_TYPE_ID,
		COMMENTS
	)
	VALUES
	(
		@buyerLiquidateTypeId,
		@comments				
	)
			
	SELECT @fuelIdentityId=SCOPE_IDENTITY()
		
END

ELSE IF @fuelId>0
BEGIN

	UPDATE 
		SR_RFP_ALTERNATE_FUEL 
	SET
		BUYER_LIQUIDATE_TYPE_ID=@buyerLiquidateTypeId,
		COMMENTS=@comments
	WHERE
		SR_RFP_ALTERNATE_FUEL_ID=@fuelId

	SELECT @fuelIdentityId=@FuelID
END

RETURN
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_SET_SUPPLIER_BOC_ALTERNATE_FUEL_P] TO [CBMSApplication]
GO
