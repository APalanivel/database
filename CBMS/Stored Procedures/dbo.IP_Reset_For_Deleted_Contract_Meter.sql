SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
  
NAME: [dbo].[IP_Reset_For_Deleted_Contract_Meter]  
  
DESCRIPTION:   
  
 This procedure used to reset the Invoice_participation and Invoice_participation_Site table if the disassociated meter   
 from contract is the last one in the supplier account site.  
   
 Supplier Account Meter  Site  
 ---------------- -----  ----  
 Sup_Acct-1   Meter-1  Site-1  
      Meter-2  Site-1  
        
      Meter-3  Site-2  
  
 If meter-2 is disassociated from the Supplier account no issues as Meter-1 is still present in that account for "Site-1"  
 If Meter-3 is disassociated from the account then Site-2 is no where related to this account and the history of the same should be removed.  
  
INPUT PARAMETERS:  
 NAME   DATATYPE DEFAULT  DESCRIPTION  
------------------------------------------------------------  
 @Contract_Id INT  
 @Meter_Id  VARCHAR(max)   Comma seperated meter_id which are removed from the contract.  
  
OUTPUT PARAMETERS:  
 NAME   DATATYPE DEFAULT  DESCRIPTION  
  
------------------------------------------------------------  
USAGE EXAMPLES:  
------------------------------------------------------------  
 2017-03-06 --> Failing scenario  
 --------------------------------  
 Meter Supplieraccount  Site  
 --------------------------------  
 1  1     1  
 2  1     1  
 3  2     2  
 4  2     2  
 -------------------------------  
 When meters 1,2&3 are removed from contract, script not resetting IP for site 1 and supplier account 1. No meter from the site 1 is   
 associated to contract, so this site and account(1,1) IP should be removed from IP account level and site level should be re-aggregated  
   
   
 EXEC dbo.IP_Reset_For_Deleted_Contract_Meter  
 17232, 1574727  
  
AUTHOR INITIALS:  
 INITIALS NAME  
------------------------------------------------------------  
 HG   Harihara Suthan G 
 PS   Patchava srinivasarao 
  
MODIFICATIONS:  
 INITIALS	 DATE			MODIFICATION  
------------------------------------------------------------  
 HG			10/07/2010		CREATED to fix bug #21328  
 HG			10/26/2010		Modified the logic in the query which used to insert the data in to @Invoice_Participation_List  
							Script added to remove the entries from Invoice participation Queue as well as if the meter deletion happends   
						    from the contract even before the IP batch triggered based on the entries available in this table IP batch will   
							still populate the data for the sites which are not associated to the account at present.  
 RR			2017-03-06		Contract palaceholder - Script failed to reset IP when all meters of a site and one/few meter(s) of a other site   
							removed from contract at a time, explained in usage example(2017-03-06 --> Failing scenario)  
							To fix this script should first get remained meters and respective supplier accounts and sites  
 PS         2019-02-05      Added the group clause in @Invoice_Participation_List table

*/

CREATE PROCEDURE [dbo].[IP_Reset_For_Deleted_Contract_Meter]
    @Contract_Id INT
    , @Meter_Id VARCHAR(MAX)
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Account_id INT
            , @Site_Id INT
            , @Service_Month DATE;

        DECLARE @Deleted_Meter_List TABLE
              (
                  Meter_Id INT
                  , Supplier_Account_Id INT
                  , Site_Id INT
                  , PRIMARY KEY CLUSTERED
                    (
                        Meter_Id
                        , Supplier_Account_Id
                        , Site_Id
                    )
              );
        DECLARE @Contract_Meter_List TABLE
              (
                  Meter_Id INT
                  , Supplier_Account_Id INT
                  , Site_Id INT
                  , PRIMARY KEY CLUSTERED
                    (
                        Meter_Id
                        , Supplier_Account_Id
                        , Site_Id
                    )
              );
        DECLARE @Remaining_Meter_List TABLE
              (
                  Meter_Id INT
                  , Supplier_Account_Id INT
                  , Site_Id INT
                  , PRIMARY KEY CLUSTERED
                    (
                        Meter_Id
                        , Supplier_Account_Id
                        , Site_Id
                    )
              );

        DECLARE @Invoice_Participation_List TABLE
              (
                  Account_Id INT
                  , Site_Id INT
                  , Service_Month DATETIME
                  , PRIMARY KEY CLUSTERED
                    (
                        Site_Id
                        , Service_Month
                        , Account_Id
                    )
              );

        INSERT INTO @Contract_Meter_List
             (
                 Meter_Id
                 , Supplier_Account_Id
                 , Site_Id
             )
        SELECT
            cha.Meter_Id
            , cha.Account_Id
            , ch.Site_Id
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
        WHERE
            cha.Supplier_Contract_ID = @Contract_Id
        GROUP BY
            cha.Meter_Id
            , cha.Account_Id
            , ch.Site_Id;

        INSERT INTO @Deleted_Meter_List
             (
                 Meter_Id
                 , Supplier_Account_Id
                 , Site_Id
             )
        SELECT
            segm.Segments
            , cml.Supplier_Account_Id
            , cml.Site_Id
        FROM
            dbo.ufn_split(@Meter_Id, ',') segm
            JOIN @Contract_Meter_List cml
                ON cml.Meter_Id = CAST(segm.Segments AS INT);

        INSERT INTO @Remaining_Meter_List
             (
                 Meter_Id
                 , Supplier_Account_Id
                 , Site_Id
             )
        SELECT
            cm.Meter_Id
            , cm.Supplier_Account_Id
            , cm.Site_Id
        FROM
            @Contract_Meter_List cm
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                @Deleted_Meter_List dm
                           WHERE
                                cm.Meter_Id = dm.Meter_Id
                                AND cm.Supplier_Account_Id = dm.Supplier_Account_Id
                                AND dm.Site_Id = dm.Site_Id);

        INSERT INTO @Invoice_Participation_List
             (
                 Account_Id
                 , Site_Id
                 , Service_Month
             )
        SELECT
            ip.ACCOUNT_ID
            , d.Site_Id
            , ip.SERVICE_MONTH
        FROM
            dbo.INVOICE_PARTICIPATION ip
            INNER JOIN @Deleted_Meter_List d
                ON ip.ACCOUNT_ID = d.Supplier_Account_Id
                   AND  ip.Site_Id = d.Site_Id
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                @Remaining_Meter_List rml
                           WHERE
                                d.Site_Id = rml.Site_Id
                                AND d.Supplier_Account_Id = rml.Supplier_Account_Id)
        GROUP BY
            ip.ACCOUNT_ID
            , d.Site_Id
            , ip.SERVICE_MONTH;

        BEGIN TRY
            BEGIN TRAN;

            DELETE
            ipq
            FROM
                dbo.INVOICE_PARTICIPATION_QUEUE ipq
                INNER JOIN @Invoice_Participation_List ip
                    ON ip.Account_Id = ipq.ACCOUNT_ID
                       AND  ip.Site_Id = ipq.SITE_ID;

            WHILE EXISTS (SELECT    1 FROM  @Invoice_Participation_List)
                BEGIN

                    SELECT  TOP 1
                            @Account_id = Account_Id
                            , @Site_Id = Site_Id
                            , @Service_Month = Service_Month
                    FROM
                        @Invoice_Participation_List;

                    EXEC dbo.Invoice_Participation_Del @Account_id, @Site_Id, @Service_Month;

                    EXEC dbo.cbmsInvoiceParticipationSite_Save-1, @Site_Id, @Service_Month; -- To reset Site level invoice participation.  

                    DELETE
                    @Invoice_Participation_List
                    WHERE
                        Account_Id = @Account_id
                        AND Site_Id = @Site_Id
                        AND Service_Month = @Service_Month;

                END;
            COMMIT TRAN;
        END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRAN;
                END;

            EXEC dbo.usp_RethrowError;

        END CATCH;

    END;
GO

GRANT EXECUTE ON  [dbo].[IP_Reset_For_Deleted_Contract_Meter] TO [CBMSApplication]
GO
