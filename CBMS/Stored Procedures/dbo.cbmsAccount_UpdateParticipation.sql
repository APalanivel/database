SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE     PROCEDURE [dbo].[cbmsAccount_UpdateParticipation]
	( @user_info_id int
	, @account_id int
	, @not_expected bit = null
	, @not_expected_date datetime = null
	, @not_managed bit = null
	)
AS
BEGIN

	set nocount on

	if @not_expected = 1 or @not_managed = 1
	begin

	  declare @current_managed bit
		, @current_expected bit

	   select @current_managed = isNull(not_managed, 0)
		, @current_expected = isNull(not_expected, 0)
	     from account
	    where account_id = @account_id

		if @not_managed = 1 and @current_managed = 0
		begin

			exec cbmsInvoiceParticipationQueue_Save 
				  @user_info_id
				, 7 -- Make account not managed
				, null
				, null
				, null
				, @account_id
				, null
				, 1

		end

		if @not_expected = 1 and @current_expected = 0
		begin

			exec cbmsInvoiceParticipationQueue_Save 
				  @user_info_id
				, 8 -- Make account not expected
				, null
				, null
				, null
				, @account_id
				, null
				, 1

		end
	end

END






















GO
GRANT EXECUTE ON  [dbo].[cbmsAccount_UpdateParticipation] TO [CBMSApplication]
GO
