SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE procedure [dbo].[cbmsGroupInfo_GetMy]
	( @MyAccountId int
	)
AS
BEGIN

	   select gi.group_info_id
		, gi.group_name
		, gi.group_description
	     from user_info_group_info_map map
	     join group_info gi on gi.group_info_id = map.group_info_id 
	    where map.user_info_id = @MyAccountId

END
GO
GRANT EXECUTE ON  [dbo].[cbmsGroupInfo_GetMy] TO [CBMSApplication]
GO
