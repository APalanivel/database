SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:   [Invoice_Collection_Queue_Merge_For_Batch_Tool]    
                  
Description:                  
   This sproc is used in batch process to fill Invoice Collection Queue    
                               
 Input Parameters:                  
    Name        DataType   Default   Description                    
----------------------------------------------------------------------------------------                    
                      
                        
     
 Output Parameters:                        
    Name        DataType   Default   Description                    
----------------------------------------------------------------------------------------                    
                  
 Usage Examples:                      
----------------------------------------------------------------------------------------       
    
   Exec [Invoice_Collection_Queue_Merge_For_Batch_Tool]    
       
       
       
Author Initials:                  
    Initials	Name                  
----------------------------------------------------------------------------------------                    
 RKV             Ravi Kumar Vegesna    
 SP              Srinivas Patchava       
 SLP			 Sri Lakshmi Pallikonda  
               
 Modifications:                  
 Initials        Date           Modification
----------------------------------------------------------------------------------------
 RKV             2016-12-27     Created For Invoice_Collection.    
 RKV			 2019-08-26     Maint-9061 Modified the sp to change to close the issues if the status cd is 'Resolved,Recevied,Excluded'  
 SLP			 2019-09-20		Modified SP to include auto linking of issues.  
 HG				 2020-01-09		Commenting Issue Link logic to check the performance and run the tool in parallel  
 RKV             2020-01-28     uncommented the Issue Link Logic 
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Queue_Merge_For_Batch_Tool]
(
    @Invoice_Collection_Queue_Id INT = NULL,
    @Invoice_Collection_Account_Config_Id INT,
    @Commodity_Id INT = -1,
    @Invoice_Collection_Queue_Type_Cd INT,
    @Invoice_Collection_Exception_Type_Cd INT = NULL,
    @Collection_Start_Dt DATE,
    @Collection_End_Dt DATE,
    @Is_Manual BIT = 0,
    @Invoice_Request_Type_Cd INT = NULL,
    @Status_Cd INT,
    @Is_Locked BIT,
    @Received_Status_Updated_Dt DATE = NULL,
    @Invoice_File_Name NVARCHAR(255) = NULL,
    @Account_Invoice_Collection_Month_Id NVARCHAR(MAX),
    @Event_Desc NVARCHAR(MAX),
    @User_Info_Id INT
)
AS
BEGIN

    SET NOCOUNT ON;
    DECLARE @tvp_Invoice_Collection_Queue_Month AS tvp_Invoice_Collection_Queue_Month;
    DECLARE @tvp_Invoice_Collection_Queue_Account_Config_Id_Sel AS tvp_Invoice_Collection_Queue_Account_Config_Id_Sel;

    DECLARE @Close_Code_Id INT;


    SELECT @Close_Code_Id = Code_Id
    FROM Code C1
        INNER JOIN Codeset cs
            ON cs.Codeset_Id = C1.Codeset_Id
    WHERE C1.Code_Value = 'Close'
          AND cs.Codeset_Name = 'IC Chase Status';

    CREATE TABLE #Inserted_Invoice_Collection_Queue
    (
        Invoice_Collection_Queue_Id INT,
        Invoice_Collection_Account_Config_Id INT,
        Commodity_Id INT,
        Invoice_Collection_Queue_Type_Cd INT,
        Invoice_Collection_Exception_Type_Cd INT,
        Collection_Start_Dt DATE,
        Collection_End_Dt DATE,
        Is_Manual BIT,
        Invoice_Request_Type_Cd INT,
        Status_Cd INT,
        Is_Locked INT,
        Received_Status_Updated_Dt DATE
    );
    BEGIN TRY
        BEGIN TRAN;

        MERGE INTO dbo.Invoice_Collection_Queue tgt
        USING
        (
            SELECT @Invoice_Collection_Queue_Id Invoice_Collection_Queue_Id,
                   @Invoice_Collection_Account_Config_Id Invoice_Collection_Account_Config_Id,
                   ISNULL(@Commodity_Id, -1) AS Commodity_Id,
                   @Invoice_Collection_Queue_Type_Cd AS Invoice_Collection_Queue_Type_Cd,
                   @Invoice_Collection_Exception_Type_Cd AS Invoice_Collection_Exception_Type_Cd,
                   @Collection_Start_Dt AS Collection_Start_Dt,
                   @Collection_End_Dt AS Collection_End_Dt,
                   @Is_Manual AS Is_Manual,
                   @Invoice_Request_Type_Cd AS Invoice_Request_Type_Cd,
                   @Status_Cd AS Status_Cd,
                   ISNULL(@Is_Locked, 0) Is_Locked,
                   @Received_Status_Updated_Dt AS Received_Status_Updated_Dt,
                   @Invoice_File_Name Invoice_File_Name,
                   @User_Info_Id AS Created_User_Id,
                   @User_Info_Id AS Updated_User_Id
        ) src
        ON src.Invoice_Collection_Queue_Id = tgt.Invoice_Collection_Queue_Id
        WHEN NOT MATCHED THEN
            INSERT
            (
                Invoice_Collection_Account_Config_Id,
                Commodity_Id,
                Invoice_Collection_Queue_Type_Cd,
                Invoice_Collection_Exception_Type_Cd,
                Collection_Start_Dt,
                Collection_End_Dt,
                Is_Manual,
                Invoice_Request_Type_Cd,
                Status_Cd,
                Is_Locked,
                Received_Status_Updated_Dt,
                Invoice_File_Name,
                Created_User_Id,
                Updated_User_Id
            )
            VALUES
            (src.Invoice_Collection_Account_Config_Id, src.Commodity_Id, src.Invoice_Collection_Queue_Type_Cd,
             src.Invoice_Collection_Exception_Type_Cd, src.Collection_Start_Dt, src.Collection_End_Dt, src.Is_Manual,
             src.Invoice_Request_Type_Cd, src.Status_Cd, src.Is_Locked, src.Received_Status_Updated_Dt,
             Invoice_File_Name, src.Created_User_Id, src.Updated_User_Id)
        WHEN MATCHED THEN
            UPDATE SET tgt.Invoice_Collection_Queue_Type_Cd = src.Invoice_Collection_Queue_Type_Cd,
                       tgt.Invoice_Collection_Exception_Type_Cd = ISNULL(
                                                                            src.Invoice_Collection_Exception_Type_Cd,
                                                                            tgt.Invoice_Collection_Exception_Type_Cd
                                                                        ),
                       tgt.Collection_Start_Dt = src.Collection_Start_Dt,
                       tgt.Collection_End_Dt = src.Collection_End_Dt,
                       tgt.Is_Manual = src.Is_Manual,
                       tgt.Invoice_Request_Type_Cd = ISNULL(src.Invoice_Request_Type_Cd, tgt.Invoice_Request_Type_Cd),
                       tgt.Status_Cd = src.Status_Cd,
                       tgt.Is_Locked = src.Is_Locked,
                       tgt.Received_Status_Updated_Dt = ISNULL(
                                                                  src.Received_Status_Updated_Dt,
                                                                  tgt.Received_Status_Updated_Dt
                                                              ),
                       tgt.Invoice_File_Name = ISNULL(src.Invoice_File_Name, tgt.Invoice_File_Name)
        OUTPUT Inserted.Invoice_Collection_Queue_Id,
               Inserted.Invoice_Collection_Account_Config_Id,
               Inserted.Commodity_Id,
               Inserted.Invoice_Collection_Queue_Type_Cd,
               Inserted.Invoice_Collection_Exception_Type_Cd,
               Inserted.Collection_Start_Dt,
               Inserted.Collection_End_Dt,
               Inserted.Is_Manual,
               Inserted.Invoice_Request_Type_Cd,
               Inserted.Status_Cd,
               Inserted.Is_Locked,
               Inserted.Received_Status_Updated_Dt
        INTO #Inserted_Invoice_Collection_Queue;



        INSERT INTO @tvp_Invoice_Collection_Queue_Account_Config_Id_Sel
        (
            Invoice_Collection_Queue_Id,
            Invoice_Collection_Account_Config_Id
        )
        SELECT Invoice_Collection_Queue_Id,
               Invoice_Collection_Account_Config_Id
        FROM #Inserted_Invoice_Collection_Queue;

		
        EXEC dbo.Invoice_Collection_Issue_Link_Auto 
					@tvp_Invoice_Collection_Queue_Account_Config_Id_Sel,
                    @User_Info_Id;
	   

        INSERT INTO @tvp_Invoice_Collection_Queue_Month
        (
            Invoice_Collection_Queue_Id,
            Account_Invoice_Collection_Month_Id
        )
        SELECT icq.Invoice_Collection_Queue_Id,
               aicm.Account_Invoice_Collection_Month_Id
        FROM #Inserted_Invoice_Collection_Queue icq
            OUTER APPLY
        (
            SELECT Segments
            FROM dbo.ufn_split(@Account_Invoice_Collection_Month_Id, ',')
        ) aicm(Account_Invoice_Collection_Month_Id)
        WHERE @Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
              AND @Invoice_Collection_Queue_Type_Cd = icq.Invoice_Collection_Queue_Type_Cd
              AND ISNULL(@Invoice_Collection_Exception_Type_Cd, 0) = ISNULL(icq.Invoice_Collection_Exception_Type_Cd, 0)
              AND ISNULL(@Commodity_Id, -1) = ISNULL(icq.Commodity_Id, -1)
              AND @Collection_Start_Dt = icq.Collection_Start_Dt
              AND @Collection_End_Dt = icq.Collection_End_Dt
              AND ISNULL(@Invoice_Request_Type_Cd, 0) = ISNULL(icq.Invoice_Request_Type_Cd, 0)
              AND ISNULL(@Is_Locked, 0) = ISNULL(icq.Is_Locked, 0)
              AND ISNULL(@Received_Status_Updated_Dt, '1900-01-01') = ISNULL(
                                                                                icq.Received_Status_Updated_Dt,
                                                                                '1900-01-01'
                                                                            )
              AND @Status_Cd = icq.Status_Cd
              AND @Is_Manual = icq.Is_Manual;

        EXEC dbo.Invoice_Collection_Queue_Month_Map_Merge @tvp_Invoice_Collection_Queue_Month;


        INSERT INTO dbo.Invoice_Collection_Queue_Event
        (
            Invoice_Collection_Queue_Id,
            Event_Desc,
            Event_By_User_Id
        )
        SELECT icq.Invoice_Collection_Queue_Id,
               @Event_Desc,
               @User_Info_Id
        FROM #Inserted_Invoice_Collection_Queue icq
        WHERE @Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
              AND @Invoice_Collection_Queue_Type_Cd = icq.Invoice_Collection_Queue_Type_Cd
              AND ISNULL(@Invoice_Collection_Exception_Type_Cd, 0) = ISNULL(icq.Invoice_Collection_Exception_Type_Cd, 0)
              AND ISNULL(@Commodity_Id, -1) = ISNULL(icq.Commodity_Id, -1)
              AND @Collection_Start_Dt = icq.Collection_Start_Dt
              AND @Collection_End_Dt = icq.Collection_End_Dt
              AND ISNULL(@Invoice_Request_Type_Cd, 0) = ISNULL(icq.Invoice_Request_Type_Cd, 0)
              AND ISNULL(@Is_Locked, 0) = ISNULL(icq.Is_Locked, 0)
              AND ISNULL(@Received_Status_Updated_Dt, '1900-01-01') = ISNULL(
                                                                                icq.Received_Status_Updated_Dt,
                                                                                '1900-01-01'
                                                                            )
              AND @Status_Cd = icq.Status_Cd
              AND @Is_Manual = icq.Is_Manual;
        ------------------------------------------------------------------    





        UPDATE icil
        SET icil.Issue_Status_Cd = @Close_Code_Id,
            icil.Updated_User_Id = @User_Info_Id,
            icil.Last_Change_Ts = GETDATE()
        FROM dbo.Invoice_Collection_Issue_Log icil
            INNER JOIN dbo.Invoice_Collection_Queue icq
                ON icq.Invoice_Collection_Queue_Id = icil.Invoice_Collection_Queue_Id
        WHERE EXISTS
        (
            SELECT 1
            FROM Code c
            WHERE Code_Id = @Status_Cd
                  AND icq.Status_Cd = c.Code_Id
                  AND c.Code_Value IN ( 'Resolved', 'Received', 'Excluded' )
        )
              AND icq.Invoice_Collection_Queue_Id = @Invoice_Collection_Queue_Id;





        COMMIT TRAN;
    END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0
        BEGIN
            ROLLBACK;
        END;
        EXEC dbo.usp_RethrowError;

    END CATCH;

    DROP TABLE #Inserted_Invoice_Collection_Queue;

END;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Queue_Merge_For_Batch_Tool] TO [CBMSApplication]
GO
