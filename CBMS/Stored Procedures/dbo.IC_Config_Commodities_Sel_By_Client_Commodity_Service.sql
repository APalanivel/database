SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******      
NAME:      
 dbo.IC_Config_Commodities_Sel_By_Client_Commodity_Service    
    
DESCRIPTION:      
 Used to get all commodity as per Client_ID & @Commodity_Service_Type    
    
INPUT PARAMETERS:      
Name      DataType  Default  Description      
------------------------------------------------------------      
  @Client_Id INT    
  @Commodity_Service_Type VARCHAR(25)           
           
OUTPUT PARAMETERS:      
Name      DataType  Default  Description      
------------------------------------------------------------      
    
USAGE EXAMPLES:      
------------------------------------------------------------    
EXEC dbo.IC_Config_Commodities_Sel_By_Client_Commodity_Service 108,'Invoice'    
EXEC dbo.IC_Config_Commodities_Sel_By_Client_Commodity_Service 218,'Invoice'    
     
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
AKR     Ashok Kumar Raju  
    
MODIFICATIONS       
Initials Date  Modification      
------------------------------------------------------------      
AKR      created, to show the commodities in specific Order.  
    
******/    
CREATE PROCEDURE [dbo].[IC_Config_Commodities_Sel_By_Client_Commodity_Service]
      ( 
       @Client_Id INT
      ,@Site_Id INT = NULL )
AS 
BEGIN            
             
      SET NOCOUNT ON;            
    
      SELECT
            cha.commodity_id
           ,com.Commodity_Name
      FROM
            Core.Client_Hier ch
            INNER JOIN core.Client_Hier_Account cha
                  ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN dbo.Commodity com
                  ON cha.Commodity_Id = com.Commodity_Id
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                  ON icac.Account_Id = cha.Account_Id
      WHERE
            ch.CLIENT_ID = @Client_Id
            AND ( @Site_Id IS NULL
                  OR ch.Site_Id = @Site_Id )
       Group By 
            cha.commodity_id
           ,com.Commodity_Name
                           
END;


;
GO
GRANT EXECUTE ON  [dbo].[IC_Config_Commodities_Sel_By_Client_Commodity_Service] TO [CBMSApplication]
GO
