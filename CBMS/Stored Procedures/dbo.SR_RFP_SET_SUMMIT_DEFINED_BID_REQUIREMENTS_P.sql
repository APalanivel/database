SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_SET_SUMMIT_DEFINED_BID_REQUIREMENTS_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@delivery varchar(200),
@transServiceLevelId int,
@supplierNominationId int,
@supplierBalancingId int,
@summitComments varchar(4000),
@deliveryPowerId int,
@transServiceLevelPowerId int,
@rfpId int,
@priceProductId int,
@isSystemProduct int,
@bidId int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
-- Test insert an entry
--exec [dbo].[SR_RFP_SET_SUMMIT_DEFINED_BID_REQUIREMENTS_P] 
--@delivery = 'CityGate',
--@transServiceLevelId = 1077,
--@supplierNominationId = 1081,
--@supplierBalancingId = 1081,
--@summitComments = 'Test Data',
--@deliveryPowerId = NULL,
--@transServiceLevelPowerId = NULL,
--@rfpId = 2028,
--@priceProductId = 7359,
--@isSystemProduct = 0,
--@bidId = 0

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[SR_RFP_SET_SUMMIT_DEFINED_BID_REQUIREMENTS_P] 

@delivery varchar(200),
@transServiceLevelId int,
@supplierNominationId int,
@supplierBalancingId int,
@summitComments varchar(4000),
@deliveryPowerId int,
@transServiceLevelPowerId int,
@rfpId int,
@priceProductId int,
@isSystemProduct int,
@bidId int

as
	
set nocount on
	
declare @sr_rfp_bid_req_id int

if @bidId=0

BEGIN

		INSERT INTO SR_RFP_BID_REQUIREMENTS 
		(
			DELIVERY_POINT,
			TRANSPORTATION_LEVEL_TYPE_ID,
			NOMINATION_TYPE_ID,
			BALANCING_TYPE_ID,
			COMMENTS,
			DELIVERY_POINT_POWER_TYPE_ID,
			SERVICE_LEVEL_POWER_TYPE_ID
		)  
		
		VALUES
		
		(
			@delivery,
			@transServiceLevelId,
			@supplierNominationId,
			@supplierBalancingId,
			@summitComments,
			@deliveryPowerId ,
			@transServiceLevelPowerId 
		)
		
		SELECT @sr_rfp_bid_req_id=SCOPE_IDENTITY()
		
     	if @isSystemProduct=0
		
		BEGIN
	
			UPDATE  SR_RFP_SELECTED_PRODUCTS 
				
			SET 
				SR_RFP_BID_REQUIREMENTS_ID=@sr_rfp_bid_req_id 
			WHERE 
				PRICING_PRODUCT_ID=@priceProductId AND
				IS_SYSTEM_PRODUCT is null AND
				SR_RFP_ID=@rfpId
			
		END

     else if @isSystemProduct=1
	BEGIN

		UPDATE  SR_RFP_SELECTED_PRODUCTS 
		
		SET 
			SR_RFP_BID_REQUIREMENTS_ID=@sr_rfp_bid_req_id 
		WHERE 
			PRICING_PRODUCT_ID=@priceProductId AND
			IS_SYSTEM_PRODUCT=@isSystemProduct AND 
			SR_RFP_ID=@rfpId

	END

	EXEC SR_RFP_UPDATE_PLACE_BID_SUMMIT_BOC_P @delivery,@transServiceLevelId,@supplierNominationId,@supplierBalancingId,@summitComments,@deliveryPowerId,@transServiceLevelPowerId,@rfpId,@priceProductId,@isSystemProduct,@sr_rfp_bid_req_id

END

else if @bidId>0

BEGIN

		UPDATE  SR_RFP_BID_REQUIREMENTS 
		
		SET 
			DELIVERY_POINT=@delivery,
			TRANSPORTATION_LEVEL_TYPE_ID=@transServiceLevelId,
			NOMINATION_TYPE_ID=@supplierNominationId,
			BALANCING_TYPE_ID=@supplierBalancingId,
			COMMENTS=@summitComments,
			DELIVERY_POINT_POWER_TYPE_ID=@deliveryPowerId ,

			SERVICE_LEVEL_POWER_TYPE_ID=@transServiceLevelPowerId
		WHERE 
			SR_RFP_BID_REQUIREMENTS_ID=@bidId
	
		EXEC SR_RFP_UPDATE_PLACE_BID_SUMMIT_BOC_P @delivery,@transServiceLevelId,@supplierNominationId,@supplierBalancingId,@summitComments,@deliveryPowerId,@transServiceLevelPowerId,@rfpId,@priceProductId,@isSystemProduct,@bidId
			

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_SET_SUMMIT_DEFINED_BID_REQUIREMENTS_P] TO [CBMSApplication]
GO
