
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	dbo.SR_RFP_SAVE_CLOSURE_ACCOUNTS_DETAILS_P

DESCRIPTION: 


INPUT PARAMETERS:    
		Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
		@accountGroupId					INT
		@rfpId							INT
		@actionId						INT
		@vendorId						INT
		@contractNumber					VARCHAR(150)
		@benchMarkIndex					VARCHAR(50)
		@benchMarkFixedWhole			VARCHAR(50)
		@savings						VARCHAR(50)
		@savingsComments				VARCHAR(4000)
		@isAwardedEmail					BIT
		@supplierContactId				INT
		@generalComments				VARCHAR(4000)
		@benchmarkComments				VARCHAR(3900)
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
--EXEC dbo.SR_RFP_SAVE_CLOSURE_ACCOUNTS_DETAILS_P
--@accountGroupId int ,
--@rfpId int,
--@actionId int,
--@vendorId int,
----@contractId int,  removed for Bz7036
--@contractNumber varchar(150), -- added for Bz7036
--@benchMarkIndex varchar(50),
--@benchMarkFixedWhole varchar(50),
--@savings varchar(50),
--@savingsComments varchar(4000),
--@isAwardedEmail bit,
--@supplierContactId int,--,
--@generalComments varchar(4000),--added for
--@benchmarkComments varchar(3900)

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DR			Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	DR			08/04/2009	Removed Linked Server Updates
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2016-04-26	GCS-724 Updating cloned accounts also as closed

******/
CREATE PROCEDURE [dbo].[SR_RFP_SAVE_CLOSURE_ACCOUNTS_DETAILS_P]
      ( 
       @accountGroupId INT
      ,@rfpId INT
      ,@actionId INT
      ,@vendorId INT
      ,@contractNumber VARCHAR(150)
      , -- added for Bz7036
       @benchMarkIndex VARCHAR(50)
      ,@benchMarkFixedWhole VARCHAR(50)
      ,@savings VARCHAR(50)
      ,@savingsComments VARCHAR(4000)
      ,@isAwardedEmail BIT
      ,@supplierContactId INT
      ,@generalComments VARCHAR(4000)
      ,@benchmarkComments VARCHAR(3900) )
AS 
BEGIN
	
      DECLARE
            @account_bid_status_id INT
           ,@contractId INT

      DECLARE @sr_rfp_closure_id INT 
      
      DECLARE
            @contractStartDate DATETIME
           ,@contractEndDate DATETIME
           ,@actionName VARCHAR(200)
           ,@isSummitApproved BIT
           ,@count INT
           
      DECLARE @sr_account_group_id INT
           
      DECLARE
            @award_action_id INT
           ,@not_award_action_id INT
           ,@closed_action_id INT
           ,@contact_vendor_id INT
           ,@contact_map_id INT

-- start newly added for supplier website
      SELECT
            @account_bid_status_id = entity_id
      FROM
            entity
      WHERE
            entity_type = 1029
            AND entity_name = 'Closed'


      UPDATE
            SR_RFP_ACCOUNT
      SET   
            BID_STATUS_TYPE_ID = @account_bid_status_id
      WHERE
            SR_RFP_ACCOUNT_ID = @accountGroupId
            AND sr_rfp_id = @rfpId
	
-- end newly added for supplier website
-- added for Bz7036
      SELECT
            @contractId = contract_id
      FROM
            contract
      WHERE
            ed_contract_number = @contractNumber

      IF ( SELECT
            count(SR_RFP_CLOSURE_ID)
           FROM
            SR_RFP_CLOSURE
           WHERE
            SR_ACCOUNT_GROUP_ID = @accountGroupId ) = 0 
            BEGIN
		
                  INSERT      INTO SR_RFP_CLOSURE
                              ( 
                               SR_ACCOUNT_GROUP_ID
                              ,IS_BID_GROUP
                              ,CLOSE_ACTION_TYPE_ID
                              ,VENDOR_ID
                              ,CONTRACT_ID
                              ,BENCHMARK_INDEX_ADDER
                              ,BENCHMARK_FIXED_WHOLESALE
                              ,SAVINGS
                              ,SAVINGS_COMMENTS
                              ,IS_AWARDED_EMAIL_SENT
                              ,CLOSE_DATE
                              ,SR_SUPPLIER_CONTACT_INFO_ID
                              ,--,
                               GENERAL_COMMENTS
                              ,BENCHMARK_COMMENTS )
                  VALUES
                              ( 
                               @accountGroupId
                              ,0
                              ,@actionId
                              ,@vendorId
                              ,@contractId
                              ,@benchMarkIndex
                              ,@benchMarkFixedWhole
                              ,@savings
                              ,@savingsComments
                              ,@isAwardedEmail
                              ,getdate()
                              ,@supplierContactId
                              ,--,
                               @generalComments
                              ,@benchmarkComments )	

                  SELECT
                        @sr_rfp_closure_id = scope_identity()

            END
      ELSE 
            BEGIN	
	
                  UPDATE
                        SR_RFP_CLOSURE
                  SET   
                        CLOSE_ACTION_TYPE_ID = @actionId
                       ,VENDOR_ID = @vendorId
                       ,CONTRACT_ID = @contractId
                       ,BENCHMARK_INDEX_ADDER = @benchMarkIndex
                       ,BENCHMARK_FIXED_WHOLESALE = @benchMarkFixedWhole
                       ,SAVINGS = @savings
                       ,SAVINGS_COMMENTS = @savingsComments
                       ,IS_AWARDED_EMAIL_SENT = @isAwardedEmail
                       ,SR_SUPPLIER_CONTACT_INFO_ID = @supplierContactId
                       ,--,
                        GENERAL_COMMENTS = @generalComments
                       ,BENCHMARK_COMMENTS = @benchmarkComments
                  WHERE
                        SR_ACCOUNT_GROUP_ID = @accountGroupId	

            END

      

      SELECT
            @actionName = ( SELECT
                              ENTITY_NAME
                            FROM
                              entity
                            WHERE
                              entity_id = @actionId )
      SELECT
            @contractStartDate = ( SELECT
                                    CONTRACT_START_DATE
                                   FROM
                                    contract
                                   WHERE
                                    contract_id = @contractId )
      SELECT
            @contractEndDate = ( SELECT
                                    CONTRACT_END_DATE
                                 FROM
                                    contract
                                 WHERE
                                    contract_id = @contractId )
      SELECT
            @isSummitApproved = dbo.SR_RFP_FN_GET_IS_SUMMIT_APPROVED(@vendorId)
      SELECT
            @count = count(cbms_image_id)
      FROM
            sr_supplier_profile_documents
            JOIN sr_supplier_profile
                  ON sr_supplier_profile_documents.sr_supplier_profile_id = sr_supplier_profile.sr_supplier_profile_id
      WHERE
            sr_supplier_profile.vendor_id = @vendorId 
	
      IF ( @actionName <> 'Closed - Awarded' ) 
            BEGIN	
			
                  UPDATE
                        SR_RFP_CHECKLIST
                  SET   
                        NEW_CONTRACT_ID = @contractId
                       ,CONTRACT_START_DATE = @contractStartDate
                       ,CONTRACT_END_DATE = @contractEndDate
                       ,SA_SAVINGS = @savings
                       ,IS_AWARDED_EMAIL_SENT = @isAwardedEmail
                  WHERE
                        SR_RFP_ACCOUNT_ID = ( SELECT
                                                SR_RFP_ACCOUNT_ID
                                              FROM
                                                SR_RFP_ACCOUNT
                                              WHERE
                                                SR_RFP_ACCOUNT_ID = @accountGroupId
                                                AND is_deleted = 0
                                                AND SR_RFP_ID = @rfpId
                                                AND is_deleted = 0 )
			
            END
      ELSE 
            BEGIN
			
                  UPDATE
                        SR_RFP_CHECKLIST
                  SET   
                        NEW_CONTRACT_ID = @contractId
                       ,CONTRACT_START_DATE = @contractStartDate
                       ,CONTRACT_END_DATE = @contractEndDate
                       ,NEW_SUPPLIER_ID = @vendorId
                       ,SA_SAVINGS = @savings
                       ,IS_AWARDED_EMAIL_SENT = @isAwardedEmail
                  WHERE
                        SR_RFP_ACCOUNT_ID = ( SELECT
                                                SR_RFP_ACCOUNT_ID
                                              FROM
                                                SR_RFP_ACCOUNT
                                              WHERE
                                                SR_RFP_ACCOUNT_ID = @accountGroupId
                                                AND SR_RFP_ID = @rfpId
                                                AND is_deleted = 0 )

                  IF ( @isSummitApproved = 1 ) 
                        BEGIN
                              UPDATE
                                    SR_RFP_CHECKLIST
                              SET   
                                    IS_SUPPLIER_CREDIT = 1
                              WHERE
                                    NEW_SUPPLIER_ID IN ( SELECT DISTINCT
                                                            V.VENDOR_ID
                                                         FROM
                                                            VENDOR V
                                                           ,SR_RFP_CLOSURE cl
                                                         WHERE
                                                            CL.VENDOR_ID = V.VENDOR_ID
                                                            AND V.VENDOR_ID = @vendorId )
                        END 
                  IF ( @count > 1
                       OR @count = 1 ) 
                        BEGIN
                              UPDATE
                                    SR_RFP_CHECKLIST
                              SET   
                                    IS_SUPPLIER_DOCS = 1
                              WHERE
                                    NEW_SUPPLIER_ID IN ( SELECT DISTINCT
                                                            V.VENDOR_ID
                                                         FROM
                                                            VENDOR V
                                                           ,SR_RFP_CLOSURE cl
                                                         WHERE
                                                            CL.VENDOR_ID = V.VENDOR_ID
                                                            AND V.VENDOR_ID = @vendorId )
                        END 


            END

      

      SELECT
            @sr_account_group_id = isnull(( SELECT
                                                SR_RFP_BID_GROUP_ID
                                            FROM
                                                SR_RFP_ACCOUNT
                                            WHERE
                                                SR_RFP_ACCOUNT_ID = @accountGroupId
                                                AND sr_rfp_id = @rfpId
                                                AND is_deleted = 0 ), 0)

----// Changing the logic on 09/13/2006
-- if close_action_type is 'Closed - Awarded' then only for selected suppliers bis status is  'Closed Awarded' else 'Closed Not Awarded'
      SELECT
            @award_action_id = entity_id
      FROM
            entity
      WHERE
            entity_type = 1029
            AND entity_name = 'Closed Awarded'
      SELECT
            @not_award_action_id = entity_id
      FROM
            entity
      WHERE
            entity_type = 1029
            AND entity_name = 'Closed Not Awarded'
      SELECT
            @closed_action_id = entity_id
      FROM
            entity
      WHERE
            entity_type = 1029
            AND entity_name = 'Closed'


      IF ( @sr_account_group_id > 0 ) 
            BEGIN
	
                  IF dbo.SR_RFP_FN_GET_BID_GROUP_STATUS(@sr_account_group_id) = 'Closed' 
                        BEGIN
                              IF @actionName = 'Closed - Awarded' 
                                    BEGIN
                                          SELECT
                                                @contact_map_id = sr_rfp_supplier_contact_vendor_map_id
                                          FROM
                                                sr_rfp_supplier_contact_vendor_map
                                          WHERE
                                                sr_rfp_id = @rfpId
                                                AND vendor_id = @vendorId
                                                AND sr_supplier_contact_info_id = @supplierContactId
											
                                          UPDATE
                                                sr_rfp_send_supplier
                                          SET   
                                                bid_status_type_id = @not_award_action_id
                                          WHERE
                                                sr_account_group_id = @sr_account_group_id
                                                AND is_bid_group = 1
                                                AND sr_rfp_supplier_contact_vendor_map_id <> @contact_map_id

                                          UPDATE
                                                sr_rfp_send_supplier
                                          SET   
                                                bid_status_type_id = @award_action_id
                                          WHERE
                                                sr_account_group_id = @sr_account_group_id
                                                AND is_bid_group = 1
                                                AND sr_rfp_supplier_contact_vendor_map_id = @contact_map_id

                                    END
                              ELSE 
                                    IF ( @actionName = 'Closed - No Action' )
                                          OR ( @actionName = 'Closed - Return to Distributor' )
                                          OR ( @actionName = 'Closed - Cloned' ) 
                                          BEGIN
                                                UPDATE
                                                      sr_rfp_send_supplier
                                                SET   
                                                      bid_status_type_id = @closed_action_id
                                                WHERE
                                                      sr_account_group_id = @sr_account_group_id
                                                      AND is_bid_group = 1
			
                                          END

                        END
            END
-- not a bid group
      ELSE 
            BEGIN
                  IF @actionName = 'Closed - Awarded' 
                        BEGIN
                              SELECT
                                    @contact_map_id = sr_rfp_supplier_contact_vendor_map_id
                              FROM
                                    sr_rfp_supplier_contact_vendor_map
                              WHERE
                                    sr_rfp_id = @rfpId
                                    AND vendor_id = @vendorId
                                    AND sr_supplier_contact_info_id = @supplierContactId
										
                              UPDATE
                                    sr_rfp_send_supplier
                              SET   
                                    bid_status_type_id = @not_award_action_id
                              WHERE
                                    sr_account_group_id = @accountGroupId
                                    AND is_bid_group = 0
                                    AND sr_rfp_supplier_contact_vendor_map_id <> @contact_map_id

                              UPDATE
                                    sr_rfp_send_supplier
                              SET   
                                    bid_status_type_id = @award_action_id
                              WHERE
                                    sr_account_group_id = @accountGroupId
                                    AND is_bid_group = 0
                                    AND sr_rfp_supplier_contact_vendor_map_id = @contact_map_id
		
                        END
                  ELSE 
                        IF ( @actionName = 'Closed - No Action' )
                              OR ( @actionName = 'Closed - Return to Distributor' )
                              OR ( @actionName = 'Closed - Cloned' ) 
                              BEGIN
                                    UPDATE
                                          sr_rfp_send_supplier
                                    SET   
                                          bid_status_type_id = @closed_action_id
                                    WHERE
                                          sr_account_group_id = @accountGroupId
                                          AND is_bid_group = 0
				
                              END


            END

END
;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_SAVE_CLOSURE_ACCOUNTS_DETAILS_P] TO [CBMSApplication]
GO
