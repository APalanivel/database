
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******            

NAME: [DBO].[Check_Service_Month_Sel]    
       
DESCRIPTION: TO CHECK INVOICE IS GENERATED FOR SELECTED CONTRACTID AND ACCOUNT ID  
        
INPUT PARAMETERS:            
NAME		  DATATYPE	   DEFAULT  DESCRIPTION            
------------------------------------------------------------            
@ContractID INT  
@AccountID  VARCHAR(MAX)   
                  
OUTPUT PARAMETERS:            
NAME   DATATYPE DEFAULT  DESCRIPTION            
-------------------------------------------------------------------            
USAGE EXAMPLES:            
-------------------------------------------------------------------            
 USE [CBMS]
    
 EXEC dbo.CHECK_SERVICE_MONTH_SEL  11603, '46377'
 GO  
 EXEC dbo.CHECK_SERVICE_MONTH_SEL  78228, '166184'
 GO  
 EXEC dbo.CHECK_SERVICE_MONTH_SEL  79201,'241869,241906,'         
 GO 
 
 
AUTHOR INITIALS:            
INITIALS	  NAME            
------------------------------------------------------------            
MGB		  BHASKARAN GOPALAKRISHNAN    
SS		  Subhash Subramanyam      
PNR		  Pandarinath  
AP		  Athmaram Pabbathi

MODIFICATIONS             
INITIALS	  DATE	    MODIFICATION            
------------------------------------------------------------            
MGB		  21-AUG-09   CREATED      
SS		  08-OCT-09   Replaced Contract's Account_id with Supplier_Account_Meter_Map's Account_id.   
PNR		  31-May-10   Changed Account Id datatype from int to varchar(max) for accepting comma seperated 
			 		  values in string format and also query rewritten by using table valued function to 
					  seperate comma seperated string.
SKA		  06/03/2010  Removed the logic of Count and used if exists clause				  
SKA		  06/09/2010  Used cast feature for Segments column of ufn_Split as comparing with integer value
AP		  08/01/2011  Replaced Cost_Usage table with Cost_Usage_Account_Dtl table
AP		  03/26/2012  Replace SAMM & Contract tables with CHA
***********************/    
  
CREATE PROCEDURE [dbo].[Check_Service_Month_Sel]
      ( 
       @ContractID INT
      ,@AccountID VARCHAR(MAX) )
AS 
BEGIN    

      SET NOCOUNT ON ;
      DECLARE @Contract_Id_Status BIT = 0
	  
      IF EXISTS ( SELECT
                        1
                  FROM
                        dbo.Cost_Usage_Account_Dtl CUAD
                        INNER JOIN Core.Client_Hier_Account cha
                              ON cha.Client_Hier_Id = cuad.Client_Hier_Id
                                 AND cha.Account_Id = cuad.Account_Id
                        INNER JOIN dbo.ufn_split(@AccountID, ',') AS UA
                              ON cha.Account_ID = cast (UA.Segments AS INT)
                  WHERE
                        cha.Supplier_Contract_ID = @ContractID ) 
            SET @Contract_Id_Status = 1
            
      SELECT
            @Contract_Id_Status AS Contract_Id_Status       
         
END    
;
GO

GRANT EXECUTE ON  [dbo].[CHECK_SERVICE_MONTH_SEL] TO [CBMSApplication]
GO
