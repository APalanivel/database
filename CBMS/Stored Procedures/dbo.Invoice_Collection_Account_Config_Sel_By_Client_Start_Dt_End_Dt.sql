SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******              
Name:   dbo.Invoice_Collection_Account_Config_Sel_By_Client_Start_Dt_End_Dt       
              
Description:              
			This sproc to update Invoice_Collection_Queue
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
@Client_Id									INT = NULL

@Site_Id									INT = NULL
@Account_Id									INT = NULL
      
@Invoice_Collection_Service_Start_Dt		DATE
@Invoice_Collection_Service_End_Dt			DATE  
                 
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
 @Is_Config_Exists						 BIT
              
 Usage Examples:                  
---------------------------------------------------------------------------------------- 

DECLARE @Is_Config_Exists BIT=0  
 
EXEC dbo.Invoice_Collection_Account_Config_Sel_By_Client_Start_Dt_End_Dt 
      @Client_Id = 14525
     ,@Invoice_Collection_Service_Start_Dt = '2016-01-01'
     ,@Invoice_Collection_Service_End_Dt='2017-12-01'
     ,@Is_Config_Exists=@Is_Config_Exists out

SELECT @Is_Config_Exists AS Is_Config_Exists     

Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	SP				Sandeep Pigilam
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    SP				2017-01-18		Created For Invoice_Collection.         
             
******/ 	
CREATE PROCEDURE [dbo].[Invoice_Collection_Account_Config_Sel_By_Client_Start_Dt_End_Dt]
      ( 
       @Client_Id INT = NULL
      ,@Site_Id INT = NULL
      ,@Division_Id INT = NULL
      ,@Account_Id INT = NULL
      ,@Invoice_Collection_Service_Start_Dt DATE = NULL
      ,@Invoice_Collection_Service_End_Dt DATE = NULL
      ,@Is_Config_Exists BIT OUT )
AS 
BEGIN
      SET NOCOUNT ON 
      
      SELECT TOP 1
            @Is_Config_Exists = 1
      FROM
            dbo.Invoice_Collection_Account_Config ica
            INNER JOIN core.Client_Hier_Account cha
                  ON ica.Account_Id = cha.Account_Id
            INNER JOIN Core.Client_Hier ch
                  ON cha.Client_Hier_Id = ch.Client_Hier_Id
      WHERE
            ( ch.Client_Id = @Client_Id
              OR ch.Site_Id = @Site_Id
              OR cha.Account_Id = @Account_Id
              OR ch.Sitegroup_Id = @Division_Id )
            AND ( @Invoice_Collection_Service_Start_Dt IS NULL
                  OR EXISTS ( SELECT
                                    1
                              FROM
                                    dbo.Invoice_Collection_Account_Config ica
                              WHERE
                                    ica.account_id = cha.account_id
                                    AND ( @Invoice_Collection_Service_Start_Dt > Invoice_Collection_Service_Start_Dt
                                          OR @Invoice_Collection_Service_End_Dt < Invoice_Collection_Service_End_Dt ) ) )

    
END;


;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Account_Config_Sel_By_Client_Start_Dt_End_Dt] TO [CBMSApplication]
GO
