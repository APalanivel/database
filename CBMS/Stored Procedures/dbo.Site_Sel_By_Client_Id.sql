
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
       
      
/******        
Name:        
    dbo.Site_Sel_By_Client_Id       
       
Description:        
    Used for returning the list of sites for the given filter criteria.      
       
Input Parameters:        
    Name       DataType  Default Description        
------------------------------------------------------------------------        
    @Client_Id      INT          
    @Sites   Varchar(200)   NULL      
    @splitSign   Varchar(10)          
       
Output Parameters:        
 Name  Datatype  Default Description        
------------------------------------------------------------        
       
Usage Examples:      
------------------------------------------------------------        
    EXECUTE dbo.Site_Sel_By_Client_Id        
      @Client_Id = 170      
     ,@Sites = '3080,2084,0422,456'      
     ,@splitSign = ','       
       
Author Initials:        
 Initials   Name      
------------------------------------------------------------      
 AS		Arun Skaria      
 JR		Jishnu Radhakrishnan
       
 Modifications :        
 Initials Date   Modification        
------------------------------------------------------------        
  AS	13-Mar-2014		Created    
  JR	02-Oct-2014		Moved Client_id Validation to JOIN clause
******/      
CREATE PROCEDURE [dbo].[Site_Sel_By_Client_Id]  
      (  
       @Client_Id INT  
      ,@Sites VARCHAR(max) = NULL  
      ,@splitSign VARCHAR(10) = ',' )  
AS  
BEGIN        
        
   SET NOCOUNT ON        
              
      SELECT  
            s.SITE_ID  
           ,segments AS SITE_REFERENCE_NUMBER  
      FROM  
            ufn_split(@Sites, @splitSign) sites  
            LEFT JOIN dbo.SITE s  
                  ON (sites.segments = s.SITE_REFERENCE_NUMBER  
					 AND s.Client_ID = @Client_Id)
      WHERE  
            sites.segments != ''  
END   
GO

GRANT EXECUTE ON  [dbo].[Site_Sel_By_Client_Id] TO [CBMSApplication]
GO
