SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/**

NAME: dbo.RM_Financial_Counterparty_Client_Contact_Dtls_Sel_By_Client

    
DESCRIPTION:    

      To get client contact details


INPUT PARAMETERS:    
     NAME					DATATYPE		DEFAULT           DESCRIPTION    
------------------------------------------------------------------------    
	@Client_Id				INT		

                    

OUTPUT PARAMETERS:              
      NAME              DATATYPE    DEFAULT           DESCRIPTION       
-------------------------------------------------------------------------              


USAGE EXAMPLES:              
-------------------------------------------------------------------------              
	EXEC dbo.RM_Financial_Counterparty_Client_Contact_Dtls_Sel_By_Client 235
	EXEC dbo.RM_Financial_Counterparty_Client_Contact_Dtls_Sel_By_Client @Client_Id=10003,@Counterparty_Id=1,@Contact_Info_Id=13254

	 

AUTHOR INITIALS:              
	INITIALS	NAME              
------------------------------------------------------------              
    RR			Raghu Reddy


MODIFICATIONS:    
	INITIALS	DATE		MODIFICATION              
------------------------------------------------------------              
	RR 			31-07-2018	Created - Global Risk Management
	RR 			2019-10-24	GRM-1572 - Added search input @Keyword

**/
CREATE PROCEDURE [dbo].[RM_Financial_Counterparty_Client_Contact_Dtls_Sel_By_Client]
     (
         @Client_Id INT
         , @Start_Index INT = 1
         , @End_Index INT = 2147483647
         , @Sort_Key VARCHAR(50) = NULL
         , @Sort_Order VARCHAR(50) = NULL
         , @Counterparty_Id VARCHAR(MAX) = NULL
         , @Contact_Info_Id VARCHAR(MAX) = NULL
         , @Keyword VARCHAR(200) = NULL
     )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Tbl_CounterpartyContacts AS TABLE
              (
                  Counterparty_Id INT
                  , Counterparty_Name VARCHAR(200)
                  , Contact_Info_Id INT
                  , First_Name NVARCHAR(60)
                  , Last_Name NVARCHAR(60)
                  , Email_Address NVARCHAR(150)
                  , Phone_Number NVARCHAR(60)
                  , Mobile_Number NVARCHAR(20)
                  , Fax_Number NVARCHAR(60)
                  , Last_Updated_By NVARCHAR(200)
                  , Row_Num INT
                  , Created_Ts DATETIME
              );

        DECLARE @Total INT;

        SELECT
            @Sort_Order = ISNULL(@Sort_Order, 'Desc')
            , @Sort_Key = ISNULL(@Sort_Key, 'lastUpdatedBy');



        INSERT INTO @Tbl_CounterpartyContacts
             (
                 Counterparty_Id
                 , Counterparty_Name
                 , Contact_Info_Id
                 , First_Name
                 , Last_Name
                 , Email_Address
                 , Phone_Number
                 , Mobile_Number
                 , Fax_Number
                 , Last_Updated_By
                 , Row_Num
                 , Created_Ts
             )
        SELECT
            rc.RM_COUNTERPARTY_ID
            , rc.COUNTERPARTY_NAME
            , ccm.Contact_Info_Id
            , ci.First_Name
            , ci.Last_Name
            , ci.Email_Address
            , ci.Phone_Number
            , ci.Mobile_Number
            , ci.Fax_Number
            , REPLACE(CONVERT(VARCHAR(20), ci.Last_Change_Ts, 106), ' ', '-') + ' at '
              + LTRIM(RIGHT(CONVERT(VARCHAR(20), ci.Last_Change_Ts, 100), 7)) + ' EST by ' + ui.FIRST_NAME + ' '
              + ui.LAST_NAME AS Last_Updated_BY
            , CASE WHEN @Sort_Order = 'Asc' THEN
                       ROW_NUMBER() OVER (ORDER BY
                                              CASE WHEN @Sort_Key = 'lastUpdatedBy' THEN ci.Last_Change_Ts
                                              END ASC
                                              , CASE WHEN @Sort_Key = 'counterParty' THEN rc.COUNTERPARTY_NAME
                                                    WHEN @Sort_Key = 'firstName' THEN ci.First_Name
                                                    WHEN @Sort_Key = 'lastName' THEN ci.Last_Name
                                                    WHEN @Sort_Key = 'emailAddress' THEN ci.Email_Address
                                                    WHEN @Sort_Key = 'phoneNumber' THEN ci.Phone_Number
                                                    WHEN @Sort_Key = 'mobileNumber' THEN ci.Mobile_Number
                                                    WHEN @Sort_Key = 'faxNumber' THEN ci.Fax_Number
                                                END ASC)
                  WHEN @Sort_Order = 'Desc' THEN
                      ROW_NUMBER() OVER (ORDER BY
                                             CASE WHEN @Sort_Key = 'lastUpdatedBy' THEN ci.Last_Change_Ts
                                             END DESC
                                             , CASE WHEN @Sort_Key = 'counterParty' THEN rc.COUNTERPARTY_NAME
                                                   WHEN @Sort_Key = 'firstName' THEN ci.First_Name
                                                   WHEN @Sort_Key = 'lastName' THEN ci.Last_Name
                                                   WHEN @Sort_Key = 'emailAddress' THEN ci.Email_Address
                                                   WHEN @Sort_Key = 'phoneNumber' THEN ci.Phone_Number
                                                   WHEN @Sort_Key = 'mobileNumber' THEN ci.Mobile_Number
                                                   WHEN @Sort_Key = 'faxNumber' THEN ci.Fax_Number
                                               END DESC)
              END
            , ci.Created_Ts
        FROM
            Trade.Financial_Counterparty_Client_Contact_Map ccm
            INNER JOIN dbo.RM_COUNTERPARTY rc
                ON ccm.RM_COUNTERPARTY_ID = rc.RM_COUNTERPARTY_ID
            INNER JOIN dbo.Contact_Info ci
                ON ccm.Contact_Info_Id = ci.Contact_Info_Id
            INNER JOIN dbo.Code cd
                ON ci.Contact_Type_Cd = cd.Code_Id
            INNER JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
            INNER JOIN dbo.USER_INFO ui
                ON ci.Updated_User_Id = ui.USER_INFO_ID
        WHERE
            cs.Codeset_Name = 'ContactType'
            AND cd.Code_Value = 'RM Counterparty Contact'
            AND ccm.Client_Id = @Client_Id
            AND (   @Counterparty_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Counterparty_Id, ',')
                                   WHERE
                                        CAST(Segments AS INT) = ccm.RM_COUNTERPARTY_ID))
            AND (   @Contact_Info_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Contact_Info_Id, ',')
                                   WHERE
                                        CAST(Segments AS INT) = ccm.Contact_Info_Id))
            AND (   @Keyword IS NULL
                    OR  (   rc.COUNTERPARTY_NAME LIKE '%' + @Keyword + '%'
                            OR  ci.First_Name LIKE '%' + @Keyword + '%'
                            OR  ci.Last_Name LIKE '%' + @Keyword + '%'
                            OR  ci.Email_Address LIKE '%' + @Keyword + '%'));

        SELECT  @Total = MAX(Row_Num)FROM   @Tbl_CounterpartyContacts;

        SELECT
            Counterparty_Id
            , Counterparty_Name
            , Contact_Info_Id
            , First_Name
            , Last_Name
            , Email_Address
            , Phone_Number
            , Mobile_Number
            , Fax_Number
            , Last_Updated_By
            , Row_Num
            , @Total AS Total
            , Created_Ts
        FROM
            @Tbl_CounterpartyContacts
        WHERE
            Row_Num BETWEEN @Start_Index
                    AND     @End_Index
        ORDER BY
            Row_Num;
    END;

GO
GRANT EXECUTE ON  [dbo].[RM_Financial_Counterparty_Client_Contact_Dtls_Sel_By_Client] TO [CBMSApplication]
GO
