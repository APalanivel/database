SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UPDATE_SITE_FIELDS_OF_DIVISION_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@division_id   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--exec dbo.UPDATE_SITE_FIELDS_OF_DIVISION_P 79
CREATE       PROCEDURE dbo.UPDATE_SITE_FIELDS_OF_DIVISION_P 

@division_id int

AS
set nocount on
DECLARE @site_id int

		--for Site
			--print 'TO test the division id : ' + str(@division_id)

			DECLARE SITE_DATA CURSOR FAST_FORWARD
			FOR
			SELECT DISTINCT SITE_ID FROM SITE where DIVISION_ID = @division_id
			
			OPEN SITE_DATA
			FETCH NEXT FROM SITE_DATA INTO @site_id
			WHILE (@@fetch_status <> -1)
			BEGIN

				declare @comparingString as varchar(400)
				declare @imageId as int

				--For CONTRACTING_ENTITY
				select @comparingString = (select CONTRACTING_ENTITY from SITE where SITE_ID = @site_id)
				--IF ((select CONTRACTING_ENTITY from SITE where SITE_ID = @site_id) IS NULL) --OR (LTRIM(select CONTRACTING_ENTITY from SITE where SITE_ID = @site_id) = '')
				IF (@comparingString IS NULL OR LTRIM(@comparingString) = '')
				BEGIN
					update site set CONTRACTING_ENTITY = 
					(select CONTRACTING_ENTITY from DIVISION where DIVISION_ID = @division_id) 
					where SITE_ID = @site_id and DIVISION_ID = @division_id
				END

				--For CLIENT_LEGAL_STRUCTURE
				select @comparingString = (select CLIENT_LEGAL_STRUCTURE from SITE where SITE_ID = @site_id)
				--IF ((select CLIENT_LEGAL_STRUCTURE from SITE where SITE_ID = @site_id) IS NULL)
				IF (@comparingString IS NULL OR LTRIM(@comparingString) = '')
				BEGIN
					update site set CLIENT_LEGAL_STRUCTURE = 
					(select CLIENT_LEGAL_STRUCTURE from DIVISION where DIVISION_ID = @division_id) 
					where SITE_ID = @site_id and DIVISION_ID = @division_id
				END

				--For LEGAL_STRUCTURE_IMAGE_ID
				select @imageId = (select LEGAL_STRUCTURE_CBMS_IMAGE_ID from SITE where SITE_ID = @site_id)
				--IF ((select LEGAL_STRUCTURE_CBMS_IMAGE_ID from SITE where SITE_ID = @site_id) IS NULL)
				IF (@imageId IS NULL OR @imageId = 0)
				BEGIN
					update site set LEGAL_STRUCTURE_CBMS_IMAGE_ID = 
					(select CBMS_IMAGE_ID from DIVISION where DIVISION_ID = @division_id) 
					where SITE_ID = @site_id and DIVISION_ID = @division_id
				END

				--For FEIN_NUMBER
				select @comparingString = (select TAX_NUMBER from SITE where SITE_ID = @site_id)
				--IF ((select TAX_NUMBER from SITE where SITE_ID = @site_id) IS NULL)
				IF (@comparingString IS NULL OR LTRIM(@comparingString) = '')
				BEGIN
					update site set TAX_NUMBER = 
					(select TAX_NUMBER from DIVISION where DIVISION_ID = @division_id) 
					where SITE_ID = @site_id and DIVISION_ID = @division_id
				END

				--For DUNS_NUMBER
				select @comparingString = (select DUNS_NUMBER from SITE where SITE_ID = @site_id)
				--IF ((select DUNS_NUMBER from SITE where SITE_ID = @site_id) IS NULL)
				IF (@comparingString IS NULL OR LTRIM(@comparingString) = '')
				BEGIN
					update site set DUNS_NUMBER = 
					(select DUNS_NUMBER from DIVISION where DIVISION_ID = @division_id) 
					where SITE_ID = @site_id and DIVISION_ID = @division_id
				END

			FETCH NEXT FROM SITE_DATA INTO @site_id
			END
			CLOSE SITE_DATA
			DEALLOCATE SITE_DATA
		--for Site
GO
GRANT EXECUTE ON  [dbo].[UPDATE_SITE_FIELDS_OF_DIVISION_P] TO [CBMSApplication]
GO
