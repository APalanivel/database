SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.INSERT_CEA_DISPLAY_DATA_P
	@user_info_id int,
	@client_id int,
	@client_display_order int
	AS
	begin
	set nocount on
	INSERT into CEA_HOMEPAGE_CLIENTS_DISPLAY_LIST 
		    (USER_INFO_ID, 
		     CLIENT_ID, 
		     CLIENT_DISPLAY_ORDER) 
	values 	    (@user_info_id, 
		     @client_id,
		     @client_display_order)
	end
GO
GRANT EXECUTE ON  [dbo].[INSERT_CEA_DISPLAY_DATA_P] TO [CBMSApplication]
GO
