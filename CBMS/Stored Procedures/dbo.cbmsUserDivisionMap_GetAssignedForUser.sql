
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
  
 NAME: [dbo].[cbmsUserDivisionMap_GetAssignedForUser]
  
 DESCRIPTION:  
			To get the count of sites from given client_id and user_info_id.
  
 INPUT PARAMETERS:  
 
 Name								DataType			Default			Description  
---------------------------------------------------------------------------------------------------------------
 @MyAccountId                       INT		            NOT NULL
 @MyClientID                        INT                 NOT NULL
   
 OUTPUT PARAMETERS: 
        
 Name								DataType			Default			Description  
---------------------------------------------------------------------------------------------------------------
  
  
 USAGE EXAMPLES:      
---------------------------------------------------------------------------------------------------------------

		Exec  dbo.cbmsUserDivisionMap_GetAssignedForUser 39151,110
 
 AUTHOR INITIALS:
  
 Initials				Name  
---------------------------------------------------------------------------------------------------------------
  NR                    Narayana Reddy    
   
 MODIFICATIONS: 
 
 Initials				Date			 Modification
---------------------------------------------------------------------------------------------------------------
  NR                    2013-12-20        Added header.
                                          For RA-Admin added for Site_Count column.
******/    

CREATE  PROCEDURE [dbo].[cbmsUserDivisionMap_GetAssignedForUser]
      ( 
       @MyAccountId INT
      ,@MyClientID INT )
AS 
BEGIN  
-- Assigned Divisions  
  
      SET NOCOUNT ON  
      SELECT
            ch.Sitegroup_Id AS Division_Id
           ,ch.Sitegroup_Name AS Division_Name
           ,COUNT(sgs.Site_Id) AS Site_Count
      FROM
            dbo.Security_Role sr
            INNER JOIN dbo.User_Security_Role usr
                  ON sr.Security_Role_Id = usr.Security_Role_Id
            INNER JOIN dbo.Security_Role_Client_Hier srch
                  ON srch.Security_Role_Id = sr.Security_Role_Id
            INNER JOIN core.Client_Hier ch
                  ON ch.Client_Hier_Id = srch.Client_Hier_Id
                     AND ch.Site_Id = 0
            LEFT JOIN dbo.SiteGroup_Site sgs
                  ON ch.Sitegroup_Id = sgs.Sitegroup_Id
      WHERE
            usr.User_Info_Id = @MyAccountId
            AND sr.Client_Id = @MyClientID
            AND ch.Sitegroup_Type_Name = 'Division'
      GROUP BY
            ch.Sitegroup_Id
           ,ch.Sitegroup_Name
      ORDER BY
            ch.Sitegroup_Name
END



;
GO

GRANT EXECUTE ON  [dbo].[cbmsUserDivisionMap_GetAssignedForUser] TO [CBMSApplication]
GO
