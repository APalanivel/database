SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.[CBMS_ADD_VENDOR_COMMODITY_P]


DESCRIPTION: Inserts data into the VENDOR_COMMODITY_MAP table.

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------------    
@vendor_id              int,
@ommodity_id            int
    
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
   EXEC [dbo].[CBMS_ADD_VENDOR_COMMODITY_P]  @vendor_id = 3051, @ommodity_id = 290


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates
 DMR		  09/10/2010 Modified for Quoted_Identifier
	  

******/
CREATE PROCEDURE [dbo].[CBMS_ADD_VENDOR_COMMODITY_P] 
@vendor_id int,
@ommodity_id int

AS



INSERT INTO VENDOR_COMMODITY_MAP (
									VENDOR_ID,
									COMMODITY_TYPE_ID,
									IS_HISTORY
								) 
						VALUES (
									@vendor_id, 
									@ommodity_id, 
									0
								)
GO
GRANT EXECUTE ON  [dbo].[CBMS_ADD_VENDOR_COMMODITY_P] TO [CBMSApplication]
GO
