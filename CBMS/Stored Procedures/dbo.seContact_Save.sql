SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[seContact_Save]
	(
		@ContactName varchar(255)
	, @EmailAddress varchar(100)
	, @PhoneNumber varchar(25)
	, @Subject varchar(255)
	, @MessageText text
	)
As
BEGIN

	set nocount on

	declare @ThisId int

	insert seContact
		( SubmitDate
		,	ContactName
		, EmailAddress
		, PhoneNumber
		, Subject
		, MessageText
		)
		values
		( getdate()
		,	@ContactName
		, @EmailAddress
		, @PhoneNumber
		, @Subject
		, @MessageText
		)

	set @ThisId = @@IDENTITY
	
	set nocount off
	
	exec seContact_Get @ThisId

END
GO
GRANT EXECUTE ON  [dbo].[seContact_Save] TO [CBMSApplication]
GO
