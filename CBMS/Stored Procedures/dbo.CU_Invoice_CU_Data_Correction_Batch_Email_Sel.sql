SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.CU_Invoice_CU_Data_Correction_Batch_Email_Sel            
                        
 DESCRIPTION:                        
			To get the batch level contract based users based on permissions.                
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@Client_Id								INT
@Is_Active								INT
@Invoice_Collection_Client_Config_Id	INT		NULL 
                              
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
      
   EXEC CU_Invoice_CU_Data_Correction_Batch_Email_Sel 
      @CU_Invoice_CU_Data_Correction_Batch_Id = 9
     
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2017-03-21       Created                
                       
******/                 
                
CREATE PROCEDURE [dbo].[CU_Invoice_CU_Data_Correction_Batch_Email_Sel]
      ( 
       @CU_Invoice_CU_Data_Correction_Batch_Id INT
      ,@PERMISSION_NAME VARCHAR(200) = 'invoice.emailDMObackout' )
AS 
BEGIN                
      SET NOCOUNT ON;    


      DECLARE
            @Variance_Exception_Notice_Contact_Email VARCHAR(MAX)
           ,@EMAIL_ADDRESS VARCHAR(MAX)= ''
           ,@CEM_Email VARCHAR(MAX)= ''
      
      SELECT
            @Variance_Exception_Notice_Contact_Email = c.Variance_Exception_Notice_Contact_Email
      FROM
            dbo.CU_Invoice_CU_Data_Correction_Batch cicdcb --ON cha.Supplier_Contract_ID = cicdcb.Contract_Id
            INNER JOIN dbo.CU_Invoice_CU_Data_Correction_Batch_Dtl cicdtl
                  ON cicdcb.CU_Invoice_CU_Data_Correction_Batch_Id = cicdtl.CU_Invoice_CU_Data_Correction_Batch_Id
            INNER JOIN core.Client_Hier_Account cha
                  ON cicdtl.From_Account_Id = cha.Account_Id
            INNER JOIN core.Client_Hier ch
                  ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN dbo.CLIENT c
                  ON ch.Client_Id = c.CLIENT_ID
      WHERE
            cicdcb.CU_Invoice_CU_Data_Correction_Batch_Id = @CU_Invoice_CU_Data_Correction_Batch_Id
      
      SELECT
            @EMAIL_ADDRESS = @EMAIL_ADDRESS + ui.EMAIL_ADDRESS + ','
      FROM
            dbo.USER_INFO ui
            INNER JOIN dbo.USER_INFO_GROUP_INFO_MAP uigi
                  ON ui.USER_INFO_ID = uigi.USER_INFO_ID
            INNER JOIN dbo.GROUP_INFO_PERMISSION_INFO_MAP gipi
                  ON uigi.GROUP_INFO_ID = gipi.GROUP_INFO_ID
            INNER JOIN dbo.PERMISSION_INFO p
                  ON gipi.PERMISSION_INFO_ID = p.PERMISSION_INFO_ID
      WHERE
            p.PERMISSION_NAME = @PERMISSION_NAME    
        
      SELECT
            @EMAIL_ADDRESS = LEFT(@EMAIL_ADDRESS, LEN(@EMAIL_ADDRESS) - 1)
      WHERE
            LEN(@EMAIL_ADDRESS) > 1
            
      SELECT
            @CEM_Email = @CEM_Email + ui.EMAIL_ADDRESS + ','
      FROM
            dbo.CLIENT_CEM_MAP ccm
            INNER JOIN dbo.USER_INFO ui
                  ON ccm.USER_INFO_ID = ui.USER_INFO_ID
            INNER JOIN core.Client_Hier ch
                  ON ccm.CLIENT_ID = ch.Client_Id
            INNER JOIN core.Client_Hier_Account cha
                  ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN dbo.CU_Invoice_CU_Data_Correction_Batch_Dtl cicdtl
                  ON cicdtl.From_Account_Id = cha.Account_Id
      WHERE
            cicdtl.CU_Invoice_CU_Data_Correction_Batch_Id = @CU_Invoice_CU_Data_Correction_Batch_Id
      GROUP BY
            ui.EMAIL_ADDRESS
            
      SELECT
            @CEM_Email = LEFT(@CEM_Email, LEN(@CEM_Email) - 1)
      WHERE
            LEN(@CEM_Email) > 1
            
            
      SELECT
            @Variance_Exception_Notice_Contact_Email AS Variance_Exception_Notice_Contact_Email
           ,@EMAIL_ADDRESS AS EMAIL_ADDRESS
           ,@CEM_Email AS CEM_Email     
      
END 


;
GO
GRANT EXECUTE ON  [dbo].[CU_Invoice_CU_Data_Correction_Batch_Email_Sel] TO [CBMSApplication]
GO
