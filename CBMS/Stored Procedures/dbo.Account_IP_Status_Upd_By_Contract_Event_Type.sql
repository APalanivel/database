
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
    dbo.Account_IP_Status_Upd_By_Contract_Event_Type  
 
DESCRIPTION:   
    

INPUT PARAMETERS:  
Name					DataType		Default		Description  
------------------------------------------------------------  
@User_Info_Id			INT
@Contract_id			INT
@User_Info_Id			INT
@IP_Event_Type_Name		VARCHAR(50)

   
OUTPUT PARAMETERS:  
Name				DataType  Default Description  
------------------------------------------------------------  

USAGE EXAMPLES:  
------------------------------------------------------------  
BEGIN TRANSACTION
	SELECT b.* FROM core.Client_Hier_Account a JOIN	dbo.invoice_participation_queue b ON a.Account_Id = b.ACCOUNT_ID
		WHERE  b.INVOICE_PARTICIPATION_BATCH_ID IS NULL AND a.Supplier_Contract_ID=114419
	EXEC dbo.Account_IP_Status_Upd_By_Contract_Event_Type 16, 114419, 'MakeAccountNotManaged'
	SELECT b.* FROM core.Client_Hier_Account a JOIN	dbo.invoice_participation_queue b ON a.Account_Id = b.ACCOUNT_ID
		WHERE b.INVOICE_PARTICIPATION_BATCH_ID IS NULL AND a.Supplier_Contract_ID=114419
ROLLBACK TRANSACTION

BEGIN TRANSACTION
	SELECT b.* FROM core.Client_Hier_Account a JOIN	dbo.invoice_participation_queue b ON a.Account_Id = b.ACCOUNT_ID
		WHERE  b.INVOICE_PARTICIPATION_BATCH_ID IS NULL AND a.Supplier_Contract_ID=114419
	EXEC dbo.Account_IP_Status_Upd_By_Contract_Event_Type 16, 114419, 'MakeAccountNotExpected'
	SELECT b.* FROM core.Client_Hier_Account a JOIN	dbo.invoice_participation_queue b ON a.Account_Id = b.ACCOUNT_ID
		WHERE b.INVOICE_PARTICIPATION_BATCH_ID IS NULL AND a.Supplier_Contract_ID=114419
ROLLBACK TRANSACTION


AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------
RR			Raghu Reddy
 
Initials	Date		Modification
------------------------------------------------------------
RR			2014-06-09  Created MAINT-2874
RR			2017-02-03	Updated events list(17-21), 21-New

******/
CREATE PROCEDURE [dbo].[Account_IP_Status_Upd_By_Contract_Event_Type]
      ( 
       @User_Info_Id INT
      ,@Contract_id INT
      ,@IP_Event_Type_Name VARCHAR(50) )
AS 
BEGIN

      SET NOCOUNT ON;

/*
Events:
            InsertUtilityAccount = 1
            InsertSupplierAccount = 2
            MakeClientNotManaged = 3
            MakeDivisionNotManaged = 4
            MakeSiteNotManaged = 5
            MakeSiteClosed = 6
            MakeAccountNotManaged = 7
            MakeAccountNotExpected = 8
            MakeIpExpected = 9
            MakeIpNotExpected = 10
            MakeIpReceived = 11
            MakeIpNotReceived = 12
            MakeIpVarianceUnderReview = 13
            MakeIpVarianceNotUnderReview = 14
            MakeIpRecalcUnderReview = 15
            MakeIpRecalcNotUnderReview = 16
            MakeUtilityAccountManaged = 17
			MakeUtilityAccountExpected = 18
			MakeSupplierAccountManaged = 19
			MakeSupplierAccountExpected = 20
			InsertDMOSupplierAccount = 21
			UpdateDMOSupplierAccount = 22
*/

      DECLARE
            @IP_Queue_Event_Type INT
           ,@Account_Id INT
      
      DECLARE @Acc_Tbl TABLE ( Account_Id INT )

      SELECT
            @IP_Queue_Event_Type = CASE WHEN @IP_Event_Type_Name = 'MakeAccountNotManaged' THEN 7
                                        WHEN @IP_Event_Type_Name = 'MakeAccountNotExpected' THEN 8
                                   END

      INSERT      INTO @Acc_Tbl
                  ( 
                   Account_Id )
                  SELECT
                        cha.Account_Id
                  FROM
                        Core.Client_Hier_Account cha
                  WHERE
                        cha.Supplier_Contract_ID = @Contract_id
                  GROUP BY
                        cha.Account_Id
                        
      WHILE EXISTS ( SELECT
                        Account_Id
                     FROM
                        @Acc_Tbl ) 
            BEGIN

                  SELECT TOP 1
                        @Account_Id = Account_Id
                  FROM
                        @Acc_Tbl
                  
                  EXEC dbo.cbmsInvoiceParticipationQueue_Save 
                        @User_Info_Id
                       ,@IP_Queue_Event_Type
                       ,NULL
                       ,NULL
                       ,NULL
                       ,@Account_Id
                       ,NULL
                       ,1 
                        

                  DELETE
                        @Acc_Tbl
                  WHERE
                        Account_Id = @Account_Id

            END

END;

;
GO

GRANT EXECUTE ON  [dbo].[Account_IP_Status_Upd_By_Contract_Event_Type] TO [CBMSApplication]
GO
