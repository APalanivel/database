SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*********     
NAME:  dbo.Consumption_Level_Variance_Test_INS    
   
DESCRIPTION:  Used to insert all the consumption level test details    
  
INPUT PARAMETERS:      
      Name              DataType          Default     Description      
------------------------------------------------------------      
@Variance_Rule_Id   Int   
@Consumption_Level_Id  int  
@is_Data_Entry_Only   bit  
@is_Active     bit  
@Default_Tolerance   decimal(16,4)  
@Test_Description   varchar(255)      
      
OUTPUT PARAMETERS:      
      Name              DataType          Default     Description      
------------------------------------------------------------      
      
USAGE EXAMPLES:     
  
   
------------------------------------------------------------    
AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
NK   Nageswara Rao Kosuri 
AP	Arunkumar Palanivel          
  
  
Initials Date  Modification    
------------------------------------------------------------    
NK 10/13/2009  Created  
HG 01/06/2010 Combined If statement into single Insert statement with Case in the Values clause
PD 01/12/2010 Changed NULL to 0 for the bit field, Is_Data_Entry_Only  
AP		June 11,2020  https://summit.jira.com/browse/SE2017-996 Tax exclusion
  
******/  
  
CREATE PROCEDURE [dbo].[Consumption_Level_Variance_Test_INS]  
 @Variance_Rule_Id INT,  
 @Consumption_Level_Id INT,  
 @is_Data_Entry_Only BIT,  
 @is_Active BIT,  
 @Default_Tolerance DECIMAL(16,4),  
 @Test_Description VARCHAR(255)  
          
AS  
BEGIN  
   
 SET NOCOUNT ON;  
    
 DECLARE @variance_Rule_Dtl_Id INT   
  
 INSERT INTO dbo.Variance_Rule_Dtl(Variance_Rule_Id  
  ,Variance_Consumption_Level_Id  
  ,Is_Data_Entry_Only  
  ,IS_Active  
  ,Default_Tolerance  
  ,Test_Description)  
 VALUES(@Variance_Rule_Id  
  ,@Consumption_Level_Id  
  ,CASE @is_Data_Entry_Only  
   WHEN 1 THEN 1  
   ELSE 0  
   END  
  ,@is_Active   
  ,@Default_Tolerance   
  ,@Test_Description)  
  
 SELECT @variance_Rule_Dtl_Id = scope_identity()  
  
 INSERT INTO dbo.Variance_Rule_Dtl_Account_Override  
  (Variance_Rule_Dtl_Id  
  ,ACCOUNT_ID  
  )  
  
 SELECT  
  @variance_Rule_Dtl_Id  
  , avcl.ACCOUNT_ID  
 FROM  
  dbo.Account_Variance_Consumption_Level  avcl
 WHERE  
  avcl.Variance_Consumption_Level_Id = @Consumption_Level_Id  
  AND   NOT EXISTS
                              (     SELECT
                                          1
                                    FROM  dbo.Variance_Rule vr
									JOIN dbo.Variance_Rule_Dtl vrd
									ON vrd.Variance_Rule_Id = vr.Variance_Rule_Id
                                          INNER JOIN
                                          dbo.Variance_Parameter_Baseline_Map vpbmap
                                                ON vr.Variance_Baseline_Id = vpbmap.Variance_Baseline_Id
                                          INNER JOIN
                                          dbo.Variance_Parameter vp
                                                ON vp.Variance_Parameter_Id = vpbmap.Variance_Parameter_Id
                                          INNER JOIN
                                          dbo.Code cd
                                                ON cd.Code_Id = vp.Variance_Category_Cd
                                          INNER JOIN
                                          dbo.Code cdb
                                                ON cdb.Code_Id = vpbmap.Baseline_Cd
                                          JOIN
                                          Core.Client_Hier_Account cha
                                                ON cha.Account_Id = avcl.ACCOUNT_ID
                                          JOIN
                                          dbo.Variance_Rules_Exclusion vre
                                                ON vre.Country_id = cha.Meter_Country_Id
                                                   AND vre.Baseline_Cd = cdb.Code_Id
                                                   AND vre.Category_id = cd.Code_Id
                                    WHERE vrd.Variance_Rule_Dtl_Id = @variance_Rule_Dtl_Id )
  
  
END  
GO
GRANT EXECUTE ON  [dbo].[Consumption_Level_Variance_Test_INS] TO [CBMSApplication]
GO
