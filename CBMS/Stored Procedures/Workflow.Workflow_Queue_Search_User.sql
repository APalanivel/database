SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                    
 NAME: [Workflow].[Workflow_Queue_Search_User]                        
                                    
 DESCRIPTION:This will get the users based on groups assiged to them.            
                                 
INPUT PARAMETERS:                      
                                 
 Name                        DataType         Default       Description                    
---------------------------------------------------------------------------------------------------------------                  
@MyAccountId = 0,  -- int
@group_names = '', -- varchar(max)
@Key_Word = '',    -- varchar(200)
@Start_Index = 0,  -- int
@End_Index = 0     -- int         
                                        
 OUTPUT PARAMETERS:                      
                                       
 Name                        DataType         Default       Description                    
---------------------------------------------------------------------------------------------------------------                  
                                    
 USAGE EXAMPLES:                                        
---------------------------------------------------------------------------------------------------------------                                        
 EXEC Workflow.Workflow_Queue_Search_User @MyAccountId = NULL,  -- int
                                         @group_names  = NULL,  -- varchar(max)
                                         @Key_Word = '',    -- varchar(200)
                                         @Start_Index = 0,  -- int
                                         @End_Index = 0     -- int   
                 
SELECT top 1 ch.Client_Id FROM core.Client_Hier ch WHERE ch.Client_Name LIKE 'A J Rorato'                                
 AUTHOR INITIALS:                    
                   
 Initials              Name                    
---------------------------------------------------------------------------------------------------------------                                  
 SP                    Sandeep Pigilam                      
 TRK				   Ramakrishna                                     
 MODIFICATIONS:                  
                      
 Initials              Date             Modification                  
---------------------------------------------------------------------------------------------------------------                  
 SP                    2017-02-16       Header added for Invoice Tracking.               
          @Group_Names as additional input param with comma seperated groups.         
TRK   AUG-2019 Added Pagination                 
TRK   SEP-2019 Migrated to Workflow Schema.                              
******/
 
            
CREATE PROCEDURE [Workflow].[Workflow_Queue_Search_User]           
(              
 @QUEUE_ID INT   = NULL ,            
 @group_names VARCHAR(MAX) = NULL ,        
 @Key_Word varchar(200) = NULL,        
 @Start_Index INT = 1,        
 @End_Index INT = 2147483647            
)              
AS              
BEGIN              
              
   IF @group_names IS NULL            
   SET @group_names  = 'Invoice Processors'            
        
   SELECT CQU.QUEUE_ID as USER_INFO_ID,              
   CQU.Full_Name        
 FROM(        
   SELECT              
            ui.QUEUE_ID              
           ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Full_Name           
     ,ROW_NUMBER() OVER (ORDER BY ui.FIRST_NAME + ' ' + ui.LAST_NAME )  AS Row_Num          
      FROM  dbo.GROUP_INFO gi              
      JOIN dbo.USER_INFO_GROUP_INFO_MAP map  ON map.GROUP_INFO_ID = gi.GROUP_INFO_ID              
      JOIN dbo.USER_INFO ui  ON ui.USER_INFO_ID = map.USER_INFO_ID              
      WHERE  ( @group_names IS NOT NULL  AND EXISTS ( SELECT  1 FROM  dbo.ufn_split(@group_names, ',') uf WHERE uf.Segments = gi.GROUP_NAME ) )             
      AND ui.IS_HISTORY != 1         
	  AND ui.FIRST_NAME + ' ' + ui.LAST_NAME LIKE '%' + isNull(@Key_Word, (ui.FIRST_NAME + ' ' + ui.LAST_NAME) ) + '%' 
	  AND ui.QUEUE_ID NOT IN ( @QUEUE_ID)       
      )CQU WHERE         
      Row_Num BETWEEN @Start_Index  AND   @End_Index    ORDER BY Full_Name        
          
         
END    
      
  
GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_Search_User] TO [CBMSApplication]
GO
