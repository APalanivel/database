SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
Name:    
 App_Management.dbo.Cost_Usage_Site_EP_NG_Summary_Sel_For_Data_Compare  
   
 Description:    
   
 Input Parameters:    
    Name				DataType      Default Description    
-------------------------------- ----------------------------------------    
    @Site_Id			INT  
    @Commodity_Id		INT  
    @Report_Year		SMALLINT  
    @UOM_Type_Id		INT  
    @Currency_Unit_Id   INT  
   
 Output Parameters:    
 Name					Datatype	  Default Description    
--------------------------------- ---------------------------    
   
 Usage Examples:  
------------------------------------------------------------    
    EXEC dbo.Cost_Usage_Site_EP_NG_Summary_Sel_For_Data_Compare 545, 290, 2010, 12, 3  
    EXEC dbo.Cost_Usage_Site_EP_NG_Summary_Sel_For_Data_Compare 545, 291, 2010, 25, 3  
	 exec Cost_Usage_Site_EP_NG_Summary_Sel_For_Data_Compare
	13274,291,2007,25,3
 
  
Author Initials:    
 Initials   Name  
------------------------------------------------------------  
 AP			Athmaram Pabbathi  
 BCH		Balaraju
   
 Modifications :    
 Initials   Date	    Modification    
------------------------------------------------------------    
 AP		  10/04/2011  Created   
 AP		  10/11/2011  Added Start_Month column in the result set  
 AP		  11/16/2011  Added Bucket_Type_Cd column in the result set  
 AP		  2011-12-15  Modified the SP to use Calendar Year instead of Fiscal Year
 BCH	  2012-04-12  Modified the sp to get Uom_Name.		
 ******/
CREATE   PROCEDURE dbo.Cost_Usage_Site_EP_NG_Summary_Sel_For_Data_Compare
      ( 
       @Site_Id INT
      ,@Commodity_Id INT
      ,@Report_Year SMALLINT
      ,@UOM_Type_Id INT
      ,@Currency_Unit_Id INT )
AS 
BEGIN  
  
      SET NOCOUNT ON  
        
      DECLARE
            @Begin_Date DATETIME
           ,@End_Date DATETIME
           ,@Calendar_Year_Start_Month DATE
           ,@Client_Id INT
           ,@Currency_Group_Id INT
           ,@Commodity_Name VARCHAR(200)
           ,@Currency_Unit_Name VARCHAR(200)
  
      DECLARE @Service_Month TABLE
            ( 
             Service_Month DATE
            ,Month_Number SMALLINT )  
  
      DECLARE @Bucket_List TABLE
            ( 
             Bucket_Master_Id INT PRIMARY KEY CLUSTERED
            ,Bucket_Name VARCHAR(255)
            ,Default_UOM_Type_Id INT
            ,Bucket_Type VARCHAR(25)
            ,Bucket_Type_Cd INT )  
  
      SELECT
            @Client_Id = CH.Client_Id
           ,@Currency_Group_id = CH.Client_Currency_Group_Id
      FROM
            CBMS.Core.Client_Hier CH
      WHERE
            CH.Site_Id = @Site_Id
  
      SELECT
            @Commodity_Name = COM.Commodity_Name
      FROM
            CBMS.dbo.Commodity COM
      WHERE
            COM.Commodity_Id = @Commodity_Id  
            
      SELECT
            @Currency_Unit_Name = cu.Currency_Unit_Name
      FROM
            dbo.Currency_Unit cu
      WHERE
            cu.Currency_Unit_Id = @Currency_Unit_Id
              
/* Load the Months into table variable*/  
      INSERT      INTO @Service_Month
                  ( 
                   Service_Month
                  ,Month_Number )
                  SELECT
                        dd.Date_D AS Service_Month
                       ,row_number() OVER ( ORDER BY dd.Date_D ) Month_Number
                  FROM
                        CBMS.meta.Date_Dim dd
                  WHERE
                        dd.Year_Num = @Report_Year  
  
      SET @Calendar_Year_Start_Month = convert(DATE, '1/1/' + convert(VARCHAR(4), @Report_Year))  
  
      INSERT      INTO @Bucket_List
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Default_UOM_Type_Id
                  ,Bucket_Type
                  ,Bucket_Type_Cd )
                  SELECT
                        BM.Bucket_Master_Id
                       ,BM.Bucket_Name
                       ,BM.Default_Uom_Type_Id
                       ,bt.Code_Value
                     ,BM.Bucket_Type_Cd
                  FROM
                        CBMS.dbo.Bucket_Master BM
                        JOIN dbo.Commodity com
                              ON com.Commodity_Id = bm.Commodity_Id
                        JOIN dbo.Code bt
                              ON bt.Code_Id = bm.Bucket_Type_Cd
                  WHERE
                        BM.Commodity_Id = @Commodity_Id
                        AND ( ( com.Commodity_Name = 'Electric Power'
                                AND ( ( bt.Code_Value = 'Determinant'
                                        AND bm.Bucket_Name IN ( 'Total Usage', 'On-Peak Usage', 'Off-Peak Usage', 'Other-Peak Usage', 'Demand', 'Billed Demand', 'Contract Demand' ) )
                                      OR ( bt.Code_Value = 'Charge'
                                           AND bm.Bucket_Name IN ( 'Total Cost', 'Taxes', 'Marketer Cost', 'Utility Cost' ) ) ) )
                              OR ( com.Commodity_Name = 'Natural Gas'
                                   AND ( bt.Code_Value = 'Determinant'
                                         AND bm.Bucket_Name = 'Total Usage' )
                                   OR ( bt.Code_Value = 'Charge'
                                        AND bm.Bucket_Name IN ( 'Total Cost', 'Taxes', 'Marketer Cost', 'Utility Cost' ) ) ) )
                        AND BM.Is_Active = 1 ;  
  
      IF @Commodity_Name = 'Electric Power' 
            BEGIN  
                  WITH  CTE_Cost_Usage_Site
                          AS ( SELECT
                                    unpvt.Category AS Bucket_Name
                                   ,unpvt.Value AS Bucket_Value
                                   ,unpvt.EL_UNIT_OF_MEASURE_TYPE_ID
                                   ,unpvt.Service_Month
                                   ,unpvt.Site_Id
                                   ,unpvt.Month_Number
                                   ,( case WHEN unpvt.Category IN ( 'Total Usage', 'On-Peak Usage', 'Off-Peak Usage', 'Other-Peak Usage', 'Demand' ) THEN 'Determinant'
                                           WHEN unpvt.Category IN ( 'Marketer Cost', 'Utility Cost', 'Total Cost', 'Taxes' ) THEN 'Charge'
                                      END ) AS Bucket_Type
                               FROM
                                    ( SELECT
                                          CUS.Site_Id
                                         ,SM.Service_Month
                                         ,SM.Month_Number
                                         ,cus.EL_UNIT_OF_MEASURE_TYPE_ID
                                         ,CUS.EL_Usage * UOMConv.Conversion_Factor AS [Total Usage]
                                         ,CUS.EL_On_Peak_Usage * UOMConv.Conversion_Factor AS [On-Peak Usage]
                                         ,CUS.EL_Off_Peak_Usage * UOMConv.Conversion_Factor AS [Off-Peak Usage]
                                         ,CUS.EL_Int_Peak_Usage * UOMConv.Conversion_Factor AS [Other-Peak Usage]
                                         ,CUS.EL_Demand * UOMConv.Conversion_Factor AS [Demand]
                                         ,CUS.EL_Marketer_Cost * CurConv.Conversion_Factor AS [Marketer Cost]
                                         ,CUS.EL_Utility_Cost * CurConv.Conversion_Factor AS [Utility Cost]
                                         ,CUS.EL_Cost * CurConv.Conversion_Factor AS [Total Cost]
                                         ,CUS.EL_Tax * CurConv.Conversion_Factor AS [Taxes]
                                      FROM
                                          CBMS.dbo.Cost_Usage_Site CUS
                                          INNER JOIN @Service_Month SM
                                                ON SM.Service_Month = CUS.Service_Month
                                          INNER JOIN CBMS.dbo.Currency_Unit_Conversion CurConv
     ON CurConv.Base_Unit_Id = CUS.Currency_Unit_Id
                                                   AND CurConv.Conversion_Date = SM.Service_Month
                                                   AND CurConv.Currency_Group_Id = @Currency_Group_Id
                                                   AND CurConv.Converted_Unit_Id = @Currency_Unit_Id
                                          INNER JOIN CBMS.dbo.Consumption_Unit_Conversion UOMConv
                                                ON UOMConv.Base_Unit_Id = CUS.EL_Unit_Of_Measure_Type_Id
                                                   AND UOMConv.Converted_Unit_Id = @UOM_Type_Id
                                      WHERE
                                          CUS.Site_Id = @Site_Id
                                          AND CUS.System_Row = 0 ) UNP UNPIVOT ( Value FOR Category IN ( [Total Usage], [On-Peak Usage], [Off-Peak Usage], [Other-Peak Usage], [Demand], [Marketer Cost], [Utility Cost], [Total Cost], [Taxes] ) ) AS unpvt)
                        SELECT
                              BL.Bucket_Name
                             ,BL.Bucket_Master_Id
                             ,BL.Bucket_Type_Cd
                             ,case WHEN cus.Bucket_Type = 'Determinant'
                                        AND BL.Bucket_Name = 'Demand' THEN 'KW'
                                   WHEN CUS.Bucket_Type = 'Determinant'
                                        AND BL.Bucket_Name <> 'Demand' THEN Entity_Type.ENTITY_NAME
                                   WHEN CUS.Bucket_Type = 'Charge' THEN @Currency_Unit_Name
                                   ELSE NULL
                              END AS Uom_Name
                             ,max(case WHEN CUS.Month_Number = 1 THEN CUS.Bucket_Value
                                  END) AS Month1
                             ,max(case WHEN CUS.Month_Number = 2 THEN CUS.Bucket_Value
                                  END) AS Month2
                             ,max(case WHEN CUS.Month_Number = 3 THEN CUS.Bucket_Value
                                  END) AS Month3
                             ,max(case WHEN CUS.Month_Number = 4 THEN CUS.Bucket_Value
                                  END) AS Month4
                             ,max(case WHEN CUS.Month_Number = 5 THEN CUS.Bucket_Value
                                  END) AS Month5
                             ,max(case WHEN CUS.Month_Number = 6 THEN CUS.Bucket_Value
                                  END) AS Month6
                             ,max(case WHEN CUS.Month_Number = 7 THEN CUS.Bucket_Value
                                  END) AS Month7
                             ,max(case WHEN CUS.Month_Number = 8 THEN CUS.Bucket_Value
                                  END) AS Month8
                             ,max(case WHEN CUS.Month_Number = 9 THEN CUS.Bucket_Value
                                  END) AS Month9
                             ,max(case WHEN CUS.Month_Number = 10 THEN CUS.Bucket_Value
                                  END) AS Month10
                             ,max(case WHEN CUS.Month_Number = 11 THEN CUS.Bucket_Value
                                  END) AS Month11
                             ,max(case WHEN CUS.Month_Number = 12 THEN CUS.Bucket_Value
                                  END) AS Month12
                             ,sum(CUS.Bucket_Value) AS Total
                             ,@Calendar_Year_Start_Month AS Start_Month
                        FROM
                              @Bucket_List BL
                              LEFT OUTER JOIN CTE_Cost_Usage_Site CUS
                                    ON CUS.Bucket_Name = BL.Bucket_Name
                                       AND CUS.Bucket_Type = bl.Bucket_Type
                              LEFT OUTER JOIN dbo.ENTITY Entity_Type
                                    ON Entity_Type.ENTITY_Id = CUS.EL_UNIT_OF_MEASURE_TYPE_ID
 GROUP BY
                              BL.Bucket_Name
                             ,BL.Bucket_Master_Id
                             ,BL.Bucket_Type_Cd
                             ,case WHEN cus.Bucket_Type = 'Determinant'
                                        AND BL.Bucket_Name = 'Demand' THEN 'KW'
                                   WHEN CUS.Bucket_Type = 'Determinant'
                                        AND BL.Bucket_Name <> 'Demand' THEN Entity_Type.ENTITY_NAME
                                   WHEN CUS.Bucket_Type = 'Charge' THEN @Currency_Unit_Name
                                   ELSE NULL
                              END
                        ORDER BY
                              BL.Bucket_Name  
            END  
      ELSE 
            IF @Commodity_Name = 'Natural Gas' 
                  BEGIN  
                        WITH  CTE_Cost_Usage_Site
                                AS ( SELECT
                                          unpvt.Category AS Bucket_Name
                                         ,unpvt.Value AS Bucket_Value
                                         ,unpvt.Service_Month
                                         ,unpvt.Site_Id
                                         ,unpvt.NG_UNIT_OF_MEASURE_TYPE_ID
                                         ,unpvt.Month_Number
                                         ,( case WHEN unpvt.Category = 'Total Usage' THEN 'Determinant'
                                                 WHEN unpvt.Category IN ( 'Marketer Cost', 'Utility Cost', 'Total Cost', 'Taxes' ) THEN 'Charge'
                                            END ) AS Bucket_Type
                                     FROM
                                          ( SELECT
                                                CUS.Site_Id
                                               ,SM.Service_Month
                                               ,SM.Month_Number
                                               ,CUS.NG_UNIT_OF_MEASURE_TYPE_ID
                                               ,CUS.NG_Usage * UOMConv.Conversion_Factor AS [Total Usage]
                                               ,CUS.NG_Marketer_Cost * CurConv.Conversion_Factor AS [Marketer Cost]
                                               ,CUS.NG_Utility_Cost * CurConv.Conversion_Factor AS [Utility Cost]
                                               ,CUS.NG_Cost * CurConv.Conversion_Factor AS [Total Cost]
                                               ,CUS.NG_Tax * CurConv.Conversion_Factor AS [Taxes]
                                            FROM
                                                CBMS.dbo.Cost_Usage_Site CUS
                                                INNER JOIN @Service_Month SM
                                                      ON SM.Service_Month = CUS.Service_Month
                                                INNER JOIN CBMS.dbo.Currency_Unit_Conversion CurConv
                                                      ON CurConv.Base_Unit_Id = CUS.Currency_Unit_Id
                                                         AND CurConv.Conversion_Date = SM.Service_Month
                                                         AND CurConv.Currency_Group_Id = @Currency_Group_Id
                                                         AND CurConv.Converted_Unit_Id = @Currency_Unit_Id
                                                INNER JOIN CBMS.dbo.Consumption_Unit_Conversion UOMConv
                                                      ON UOMConv.Base_Unit_Id = CUS.NG_Unit_Of_Measure_Type_Id
                                                         AND UOMConv.Converted_Unit_Id = @UOM_Type_Id
                                            WHERE
                                                CUS.Site_Id = @Site_Id
                                                AND CUS.System_Row = 0 ) UNP UNPIVOT ( Value FOR Category IN ( [Total Usage], [Marketer Cost], [Utility Cost], [Total Cost], [Taxes] ) ) AS unpvt)
                              SELECT
                                    BL.Bucket_Name
                                   ,BL.Bucket_Master_Id
                                   ,BL.Bucket_Type_Cd
                                   ,case WHEN CUS.Bucket_Type = 'CHARGE' THEN @Currency_Unit_Name
                                         ELSE Entity_Type.ENTITY_NAME
                                    END AS Uom_Name
                                   ,max(case WHEN CUS.Month_Number = 1 THEN CUS.Bucket_Value
                                        END) AS Month1
                                   ,max(case WHEN CUS.Month_Number = 2 THEN CUS.Bucket_Value
                                        END) AS Month2
                                   ,max(case WHEN CUS.Month_Number = 3 THEN CUS.Bucket_Value
                                        END) AS Month3
                                   ,max(case WHEN CUS.Month_Number = 4 THEN CUS.Bucket_Value
                                        END) AS Month4
                                   ,max(case WHEN CUS.Month_Number = 5 THEN CUS.Bucket_Value
                                        END) AS Month5
                                   ,max(case WHEN CUS.Month_Number = 6 THEN CUS.Bucket_Value
                                        END) AS Month6
                                   ,max(case WHEN CUS.Month_Number = 7 THEN CUS.Bucket_Value
                                        END) AS Month7
                                   ,max(case WHEN CUS.Month_Number = 8 THEN CUS.Bucket_Value
                                        END) AS Month8
                                   ,max(case WHEN CUS.Month_Number = 9 THEN CUS.Bucket_Value
                                        END) AS Month9
                                   ,max(case WHEN CUS.Month_Number = 10 THEN CUS.Bucket_Value
                                        END) AS Month10
                                   ,max(case WHEN CUS.Month_Number = 11 THEN CUS.Bucket_Value
                                        END) AS Month11
                                   ,max(case WHEN CUS.Month_Number = 12 THEN CUS.Bucket_Value
                                        END) AS Month12
                                   ,sum(CUS.Bucket_Value) AS Total
                                   ,@Calendar_Year_Start_Month AS Start_Month
                              FROM
                                    @Bucket_List BL
                                    LEFT OUTER JOIN CTE_Cost_Usage_Site CUS
                                          ON CUS.Bucket_Name = BL.Bucket_Name
                                             AND CUS.Bucket_Type = bl.Bucket_Type
                                    LEFT OUTER JOIN dbo.ENTITY Entity_Type
                                          ON Entity_Type.ENTITY_Id = CUS.NG_UNIT_OF_MEASURE_TYPE_ID
                              GROUP BY
                                    BL.Bucket_Name
                                   ,BL.Bucket_Master_Id
                                   ,BL.Bucket_Type_Cd
                                   ,case WHEN CUS.Bucket_Type = 'CHARGE' THEN @Currency_Unit_Name
                                         ELSE Entity_Type.ENTITY_NAME
                                    END
                              ORDER BY
                                    BL.Bucket_Name
                  END
  
END


;
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Site_EP_NG_Summary_Sel_For_Data_Compare] TO [CBMSApplication]
GO
