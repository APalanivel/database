SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  Procedure dbo.seStorage_GetTwoYears
As
BEGIN
	set nocount on
	create table #Weeks (WeekOfYear int, IncludeYear int, SortOrder int)
	
	declare @CurrentWeek int
	declare @InsertedWeeks int
	declare @Year1 int
	declare @Year2 int
	declare @Count int
	declare @WorkingDate datetime
	select @WorkingDate = max(ReportDate)
	  from seStorage
	  
	--set @WorkingDate = '01/07/2000'
	select @CurrentWeek = WeekOfYear
	  from seStorage
	 where ReportDate = @WorkingDate
	
	set @Year1 = datepart(yy, @WorkingDate)
	set @Year2 = @Year1 - 1
	
	set @InsertedWeeks = 1
	
	while @InsertedWeeks <= 8
	begin
		insert into #Weeks (WeekOfYear, IncludeYear, SortOrder) values (@CurrentWeek, @Year1, @InsertedWeeks)
		insert into #Weeks (WeekOfYear, IncludeYear, SortOrder) values (@CurrentWeek, @Year2, @InsertedWeeks)
		set @CurrentWeek = @CurrentWeek - 1
		set @InsertedWeeks = @InsertedWeeks + 1
		if @CurrentWeek = 0
		begin
			set @Year1 = @Year1 - 1
			set @Year2 = @Year1 - 1
			set @CurrentWeek = 53
			select @Count = count(StorageId)
			  from seStorage
			 where datepart(wk, ReportDate) = @CurrentWeek
			   and (datepart(yy, ReportDate) = @Year1 or datepart(yy, ReportDate) = @Year2)
			if @Count = 0
			begin
				set @CurrentWeek = 52
			end
		end
	
	end
	
	 select s.StorageId
				, s.ReportDate
				, s.WeekEnding
				, s.Volume
				, s.Change
				, x.WeekOfYear
				, x.SortOrder
	   from seStorage s
	   join #Weeks x on s.WeekOfYear = x.WeekOfYear and datepart(yy, s.ReportDate) = x.IncludeYear
 order by x.SortOrder asc
			  , s.ReportDate desc
	
	drop table #Weeks
END
GO
GRANT EXECUTE ON  [dbo].[seStorage_GetTwoYears] TO [CBMSApplication]
GO
