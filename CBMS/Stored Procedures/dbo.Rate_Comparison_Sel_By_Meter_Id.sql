SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Rate_Comparison_Sel_By_Meter_Id]  
     
DESCRIPTION: 
	To Get Rate Comparison Information for Selected Meter Id with pagination.
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Meter_Id		INT						
@Start_Index	INT			1
@End_Index		INT			2147483647		
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC Rate_Comparison_Sel_By_Meter_Id  558,1,2	
	EXEC Rate_Comparison_Sel_By_Meter_Id  60052,1,5

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR		PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			03-JUNE-10	CREATED

*/

CREATE PROCEDURE dbo.Rate_Comparison_Sel_By_Meter_Id
(
	 @Meter_Id		INT
	,@Start_Index	INT = 1
	,@End_Index		INT = 2147483647
)
AS
BEGIN
	
	SET NOCOUNT ON;

	WITH Cte_RRC
	AS
	 (
	  SELECT
			RATE_COMPARISON_NAME
			,CREATION_DATE
			,Row_Num = ROW_NUMBER() OVER (ORDER BY RATE_COMPARISON_NAME)
		    ,Total = COUNT(1) OVER ()
	  FROM
			dbo.RC_RATE_COMPARISON
	  WHERE
			METER_ID = @Meter_Id
	 )
	SELECT
		rrc.RATE_COMPARISON_NAME
		,rrc.CREATION_DATE
		,rrc.Total
	FROM
		Cte_RRC rrc
	WHERE
		rrc.Row_Num BETWEEN @Start_Index AND @End_Index

END
GO
GRANT EXECUTE ON  [dbo].[Rate_Comparison_Sel_By_Meter_Id] TO [CBMSApplication]
GO
