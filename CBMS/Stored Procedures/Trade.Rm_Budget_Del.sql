SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.Rm_Budget_Del                        
                          
Description:                          
        To get market price and forecast pirce if a selected index   
                          
Input Parameters:                          
    Name    DataType        Default     Description                            
--------------------------------------------------------------------------------    
	@Index_Id   INT    
    @Start_Dt	Date    
	@End_Dt		Date
                          
 Output Parameters:                                
	Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
Usage Examples:                              
--------------------------------------------------------------------------------    
	SELECT * FROM dbo.ENTITY e WHERE e.ENTITY_TYPE=272

	EXEC Trade.Rm_Budget_Del  100
    
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR			2019-12-23	RM-Budgets Enahancement - Created
	                
******/
CREATE PROCEDURE [Trade].[Rm_Budget_Del]
    (
        @Rm_Budget_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;


        DELETE  FROM Trade.Rm_Budget_Comment WHERE  Rm_Budget_Id = @Rm_Budget_Id;

        DELETE  FROM
        Trade.Rm_Budget_Current_Hedge_Position_Dtl
        WHERE
            Rm_Budget_Id = @Rm_Budget_Id;

        DELETE  FROM
        Trade.Rm_Budget_Site_Default_Budget_Config
        WHERE
            Rm_Budget_Id = @Rm_Budget_Id;

        DELETE  FROM Trade.Rm_Budget_Price_Dtl WHERE Rm_Budget_Id = @Rm_Budget_Id;

        DELETE  FROM
        Trade.Rm_Budget_Volume_Price_Dtl
        WHERE
            Rm_Budget_Id = @Rm_Budget_Id;

        DELETE  FROM Trade.Rm_Budget_Participant WHERE  Rm_Budget_Id = @Rm_Budget_Id;

		DELETE  FROM Trade.Rm_Budget_Filter_Participant WHERE  Rm_Budget_Id = @Rm_Budget_Id;

        DELETE  FROM Trade.Rm_Budget WHERE  Rm_Budget_Id = @Rm_Budget_Id;

    END;

GO
GRANT EXECUTE ON  [Trade].[Rm_Budget_Del] TO [CBMSApplication]
GO
