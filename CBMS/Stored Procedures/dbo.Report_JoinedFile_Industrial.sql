SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********          
NAME:          
  dbo.Report_JoinedFile_Industrial      
    
DESCRIPTION:     
 Client Info, Accounts, Meters, Contract, Utility , Rate data  
    
    
INPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
 region_name  
  
  
OUTPUT PARAMETERS:  
      Name              DataType          Default     Description  
------------------------------------------------------------   
  
USAGE EXAMPLES:  
------------------------------------------------------------  
Stress /Prod  
exec Report_JoinedFile_Industrial 'se' --Southeast Region  
exec Report_JoinedFile_Industrial 'ne' --Northeast Region  
exec Report_JoinedFile_Industrial   -- All Regions (almost 2 minutes to run 149,304 records)  
   
  
AUTHOR INITIALS:  
   
Initials Name  
------------------------------------------------------------  
LC   Lynn Cox  
PS      Patchava Srinivasarao  
  
  
Initials  Date    Modification          
------------------------------------------------------------     
PS        2018-08-22  Added new Meter_Id column  
*/   
CREATE PROC [dbo].[Report_JoinedFile_Industrial]
      ( 
       @region_name VARCHAR(10) = NULL )
AS 
BEGIN  
      SET NoCount ON;   
--Utility Accounts  
      WITH  UtilityAccts_CTE
              AS ( SELECT
                        Account_Number
                       ,svl.entity_Name UtilityAccountServiceLevel
                       ,ea.MODIFIED_DATE AccountCreated
                       ,AccountInvoiceNotExpected = ISNULL(CASE a.not_expected
                                                             WHEN '1' THEN 'Yes'
                                                             ELSE 'No'
                                                           END, 'Null')
                       ,AccountNotManaged = ISNULL(CASE a.not_managed
                                                     WHEN '1' THEN 'Yes'
                                                     ELSE 'No'
                                                   END, 'Null')
                       ,istyp.ENTITY_NAME UtilityAccountInvoiceSource
                       ,v.VENDOR_NAME Utility
                       ,comm.Commodity_Name CommodityType
                       ,r.RATE_NAME Rate
                       ,'NotApplicable' PurchaseMethod
                       ,a.SITE_ID
                       ,m.ADDRESS_ID
                       ,m.METER_NUMBER MeterNumber
                       ,m.METER_ID
                       ,ui.username AccountCreatedBy
                       ,vcl.Consumption_Level_Desc
                       ,IsDataEntryOnly = ISNULL(CASE a.Is_Data_Entry_Only
                                                   WHEN '1' THEN 'Yes'
                                                   ELSE 'No'
                                                 END, 'Null')
                   FROM
                        dbo.Account a
                        INNER JOIN dbo.VENDOR v
                              ON v.VENDOR_ID = a.VENDOR_ID
                        INNER JOIN dbo.Entity svl
                              ON svl.entity_id = a.service_level_Type_Id
                        INNER JOIN dbo.ENTITY_AUDIT ea
                              ON ea.ENTITY_IDENTIFIER = a.account_ID
                                 AND ea.ENTITY_ID = 491
                                 AND ea.AUDIT_TYPE = 1
                        INNER JOIN dbo.user_info ui
                              ON ui.USER_INFO_ID = ea.USER_INFO_ID
                        LEFT JOIN dbo.ENTITY istyp
                              ON istyp.ENTITY_ID = a.INVOICE_SOURCE_TYPE_ID
                        INNER JOIN dbo.METER m
                              ON m.ACCOUNT_ID = a.ACCOUNT_ID
                        INNER JOIN dbo.RATE r
                              ON r.RATE_ID = m.RATE_ID
                        INNER JOIN dbo.Commodity comm
                              ON comm.commodity_ID = r.COMMODITY_TYPE_ID
                        INNER JOIN Account_Variance_Consumption_Level av
                              ON av.ACCOUNT_ID = a.ACCOUNT_ID
                        INNER JOIN Variance_Consumption_Level vcl
                              ON vcl.Variance_Consumption_Level_Id = av.Variance_Consumption_Level_Id
                                 AND vcl.Commodity_Id = r.COMMODITY_TYPE_ID),  
  
--Contracts    
            Contracts_CTE
              AS ( SELECT
                        c.Contract_Id ContractId
                       ,c.BASE_CONTRACT_ID BaseContractId
                       ,c.ED_CONTRACT_NUMBER ContractNumber
                       ,ContractPricingStatus = CASE WHEN lps.CONTRACT_ID IS NULL THEN 'NotBuilt'
                                                     ELSE 'Built'
                                                END
                       ,ContractType = CASE WHEN c.CONTRACT_TYPE_ID = '154' THEN 'Utility'
                                            ELSE 'Supplier'
                                       END
                       ,v.VENDOR_NAME ContractedVendor
                       ,c.CONTRACT_START_DATE ContractStartDate
                       ,c.CONTRACT_END_DATE ContractEndDate
                       ,c.NOTIFICATION_DAYS NotificationDays
                       ,NotificationDate = contract_end_date - ( Notification_Days + 15 )
                       ,ContractTriggerRights = ISNULL(CASE is_contract_trigger_rights
                                                         WHEN '1' THEN 'Yes'
                                                         ELSE 'No'
                                                       END, 'null')
                       ,FullRequirements = ISNULL(CASE is_contract_full_requirement
                                                    WHEN '1' THEN 'Yes'
                                                    ELSE 'No'
                                                  END, 'null')
                       ,CONTRACT_PRICING_SUMMARY
                       ,CONTRACT_COMMENTS ContractComments
                       ,cu.CURRENCY_UNIT_NAME Currency
                       ,e1.ENTITY_NAME RenewalType
                       ,a.ACCOUNT_NUMBER SupplierAccountNumber
                       ,e2.ENTITY_NAME SupplierAccountServiceLevel
                       ,e3.ENTITY_NAME SupplierAccountInvoiceSource
                       ,ui.USERNAME ContractCreatedBy
                       ,ea.MODIFIED_DATE ContractCreatedDate
                       ,m.METER_ID  
    --*/  
                   FROM
                        dbo.Contract c
                        INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                              ON samm.Contract_ID = c.CONTRACT_ID
                        INNER JOIN dbo.Account a
                              ON a.ACCOUNT_ID = samm.ACCOUNT_ID
                        INNER JOIN dbo.METER m
                              ON m.meter_ID = samm.meter_ID
                        INNER JOIN dbo.VENDOR v
                              ON v.VENDOR_ID = a.VENDOR_ID
                        INNER JOIN dbo.CURRENCY_UNIT cu
                              ON cu.CURRENCY_UNIT_ID = c.CURRENCY_UNIT_ID
                        INNER JOIN dbo.ENTITY e1
                              ON e1.ENTITY_ID = c.RENEWAL_TYPE_ID
                        INNER JOIN dbo.ENTITY e2
                              ON e2.ENTITY_ID = a.SERVICE_LEVEL_TYPE_ID
                        LEFT JOIN dbo.ENTITY e3
                              ON e3.ENTITY_ID = a.INVOICE_SOURCE_TYPE_ID
                        INNER JOIN dbo.ENTITY_AUDIT ea
                              ON ea.ENTITY_IDENTIFIER = c.CONTRACT_ID
                                 AND ea.ENTITY_ID = 494
                                 AND ea.AUDIT_TYPE = '1'
                        INNER JOIN dbo.USER_INFO ui
                              ON ui.USER_INFO_ID = ea.USER_INFO_ID
                        LEFT JOIN dbo.LOAD_PROFILE_SPECIFICATION lps
                              ON lps.CONTRACT_ID = c.CONTRACT_ID
                        JOIN ( SELECT
                                    METER_ID
                                   ,CONTRACT_TYPE_ID
                                   ,CONTRACT_END_DATE ced
                               FROM
                                    CONTRACT c
                                    JOIN SUPPLIER_ACCOUNT_METER_MAP samm
                                          ON samm.Contract_ID = c.CONTRACT_ID
                               WHERE
                                    c.CONTRACT_TYPE_ID = 153
                                    AND c.CONTRACT_END_DATE > '2011-12-31'
                                    AND samm.IS_HISTORY = 0
                               UNION
                               SELECT
                                    METER_ID
                                   ,CONTRACT_TYPE_ID
                                   ,CONTRACT_END_DATE ced
                               FROM
                                    CONTRACT c
                                    JOIN SUPPLIER_ACCOUNT_METER_MAP samm
                                          ON samm.Contract_ID = c.CONTRACT_ID
                               WHERE
                                    c.CONTRACT_TYPE_ID = 154
                                    AND samm.IS_HISTORY = 0 ) x
                              ON x.METER_ID = samm.METER_ID
                                 AND x.CED = c.CONTRACT_END_DATE
                                 AND x.CONTRACT_TYPE_ID = c.CONTRACT_TYPE_ID
                   GROUP BY
                        c.Contract_Id
                       ,c.BASE_CONTRACT_ID
                       ,c.ED_CONTRACT_NUMBER
                       ,v.VENDOR_NAME
                       ,c.CONTRACT_START_DATE
                       ,c.CONTRACT_END_DATE
                       ,c.NOTIFICATION_DAYS
                       ,CONTRACT_PRICING_SUMMARY
                       ,CONTRACT_COMMENTS
                       ,cu.CURRENCY_UNIT_NAME
                       ,e1.ENTITY_NAME
                       ,a.ACCOUNT_NUMBER
                       ,e2.ENTITY_NAME
                       ,e3.ENTITY_NAME
                       ,ui.USERNAME
                       ,ea.MODIFIED_DATE
                       ,m.METER_ID
                       ,c.CONTRACT_TYPE_ID
                       ,lps.CONTRACT_ID
                       ,IS_CONTRACT_FULL_REQUIREMENT
                       ,IS_CONTRACT_TRIGGER_RIGHTS)
            --SiteList  
SELECT
      Client_Name [ClientName]
     ,c.CLIENT_ID [ClientID]
     ,ClientNotManaged = ISNULL(CASE c.not_managed
                                  WHEN '1' THEN 'Yes'
                                  ELSE 'No'
                                END, 'Null')
     ,Division_Name [DivisionName]
     ,s.Division_Id [DivisionID]
     ,DivisionNotManaged = ISNULL(CASE d.not_managed
                                    WHEN '1' THEN 'Yes'
                                    ELSE 'No'
                                  END, 'Null')
     ,Site_Name [SiteName]
     ,s.Site_Id [SiteID]
     ,ea.MODIFIED_DATE SiteCreated
     ,s.SITE_REFERENCE_NUMBER [SiteRef.]
     ,rg.GROUP_NAME SiteRMGroup
     ,SiteNotManaged = ISNULL(CASE s.not_managed
                                WHEN '1' THEN 'Yes'
                                ELSE 'No'
                              END, 'Null')
     ,SiteClosed = ISNULL(CASE s.closed
                            WHEN '1' THEN 'Yes'
                            ELSE 'No'
                          END, 'Null')
     ,sty.ENTITY_NAME SiteType
     ,addr.ADDRESS_LINE1 [AddressLine1]
     ,addr.ADDRESS_LINE2 [AddressLine2]
     ,addr.CITY City
     ,st.STATE_NAME [State_Province]
     ,addr.ZIPCODE [ZipCode]
     ,Ctry.COUNTRY_NAME [Country]
     ,reg.REGION_NAme [Region]
     ,uacte.ACCOUNT_NUMBER [AccountNumber]
     ,uacte.UtilityAccountServiceLevel
     ,uacte.AccountCreated
     ,uacte.AccountInvoiceNotExpected
     ,uacte.AccountNotManaged
     ,uacte.UtilityAccountInvoiceSource
     ,uacte.Utility
     ,uacte.CommodityType
     ,uacte.Rate
     ,uacte.MeterNumber
     ,uacte.PurchaseMethod
     ,cc.ContractID
     ,BaseContractId
     ,cc.ContractNumber
     ,cc.ContractPricingStatus
     ,cc.ContractType
     ,cc.ContractedVendor
     ,cc.ContractStartDate
     ,cc.ContractEndDate
     ,NotificationDays
     ,NotificationDate
     ,ContractTriggerRights
     ,FullRequirements
     ,Contract_Pricing_Summary
     ,ContractComments
     ,Currency
     ,RenewalType
     ,SupplierAccountNumber
     ,SupplierAccountServiceLevel
     ,SupplierAccountInvoiceSource
     ,ContractCreatedBy
     ,ContractCreatedDate
     ,uacte.AccountCreatedBy
     ,Consumption_Level_Desc
     ,IsDataEntryOnly  
    --,addr.ADDRESS_ID  
     ,uacte.Meter_Id AS Meter_Id
FROM
      dbo.Site s
      JOIN dbo.Division d
            ON d.Division_Id = s.Division_Id
      JOIN dbo.Client c
            ON c.Client_Id = s.Client_Id
      JOIN dbo.Address addr
            ON addr.Address_Parent_Id = s.Site_Id
      JOIN dbo.State st
            ON st.State_Id = addr.State_Id
      JOIN dbo.Country Ctry
            ON Ctry.Country_Id = st.Country_Id
      JOIN dbo.Region reg
            ON reg.Region_Id = st.Region_Id
      JOIN dbo.ENTITY_AUDIT ea
            ON ea.ENTITY_IDENTIFIER = s.SITE_ID
               AND ea.ENTITY_ID = 506
               AND ea.AUDIT_TYPE = 1
      LEFT JOIN dbo.RM_GROUP_SITE_MAP rgm
            ON rgm.SITE_ID = s.SITE_ID
      LEFT JOIN dbo.RM_GROUP rg
            ON rg.RM_GROUP_ID = rgm.rm_group_id
      JOIN dbo.ENTITY sty
            ON sty.ENTITY_ID = s.SITE_TYPE_ID
      LEFT JOIN UtilityAccts_CTE uacte
            ON uacte.SITE_ID = s.SITE_ID
               AND uacte.ADDRESS_ID = addr.ADDRESS_ID
      LEFT JOIN Contracts_CTE cc
            ON cc.meter_id = uacte.meter_id
WHERE
      ( REG.REGION_NAME = @region_name
        OR @region_name IS NULL )
      AND c.CLIENT_ID != 11231
      AND c.CLIENT_TYPE_ID NOT IN ( 1455, 141 )
      AND s.NOT_MANAGED != 1
GROUP BY
      Client_Name
     ,c.CLIENT_ID
     ,c.not_managed
     ,Division_Name
     ,s.Division_Id
     ,d.not_managed
     ,Site_Name
     ,s.Site_Id
     ,ea.MODIFIED_DATE
     ,s.SITE_REFERENCE_NUMBER
     ,rg.GROUP_NAME
     ,s.not_managed
     ,s.closed
     ,sty.ENTITY_NAME
     ,addr.ADDRESS_LINE1
     ,addr.ADDRESS_LINE2
     ,addr.CITY
     ,st.STATE_NAME
     ,addr.ZIPCODE
     ,Ctry.COUNTRY_NAME
     ,reg.REGION_NAME
     ,uacte.ACCOUNT_NUMBER
     ,uacte.UtilityAccountServiceLevel
     ,uacte.AccountCreated
     ,uacte.AccountInvoiceNotExpected
     ,uacte.AccountNotManaged
     ,uacte.UtilityAccountInvoiceSource
     ,uacte.Utility
     ,uacte.CommodityType
     ,uacte.Rate
     ,uacte.MeterNumber
     ,uacte.PurchaseMethod
     ,cc.ContractID
     ,BaseContractId
     ,cc.ContractNumber
     ,cc.ContractPricingStatus
     ,cc.ContractType
     ,cc.ContractedVendor
     ,cc.ContractStartDate
     ,cc.ContractEndDate
     ,NotificationDays
     ,NotificationDate
     ,ContractTriggerRights
     ,FullRequirements
     ,Contract_Pricing_Summary
     ,ContractComments
     ,Currency
     ,RenewalType
     ,SupplierAccountNumber
     ,SupplierAccountServiceLevel
     ,SupplierAccountInvoiceSource
     ,ContractCreatedBy
     ,ContractCreatedDate
     ,addr.ADDRESS_ID
     ,uacte.AccountCreatedBy
     ,Consumption_Level_Desc
     ,IsDataEntryOnly
     ,uacte.Meter_Id  
END   
   
GO
GRANT EXECUTE ON  [dbo].[Report_JoinedFile_Industrial] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_JoinedFile_Industrial] TO [CBMSApplication]
GO
