SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Budget_Setup_Completed_Budget_Sel_For_Contract]  
     
DESCRIPTION: 
	To Get Budget Information for Selected Contract Id and to return only which budget accounts are having is_deleted = False 
	and record present in Budget_Contract_Budget for the given contract as all the budget set up completed contract will have entries in this table.
	As per requirement application should show only the budgets if the budget set up has been done and the budgets which are coming under the contract start date and end date.
      
INPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
	@Contract_Id	INT						
                
OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        
  
	EXEC Budget_Setup_Completed_Budget_Sel_For_Contract 30197
	
	EXEC Budget_Setup_Completed_Budget_Sel_For_Contract 31798
	 
AUTHOR INITIALS:          
	INITIALS	NAME          
------------------------------------------------------------          
	PNR			PANDARINATH
          
MODIFICATIONS:           
	INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
	PNR			16-JUNE-10		CREATED  
	PNR			08-JULY-10		Modified Added Group by Clause to Statement to filter duplicated budget Id's 
								and also added Order By Clause to Appear Budget Ids in Order.
	HG			07/23/2010		Renamed to Budget_Setup_Completed_Budget_Sel_For_Contract From Budget_Sel_By_Contract_Id to 
								Existence check on Budget_Contract_Budget table replaced by Join.
*/
					 
CREATE PROCEDURE dbo.Budget_Setup_Completed_Budget_Sel_For_Contract
(
	 @Contract_Id	INT						
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT 
		 b.BUDGET_ID
		,b.BUDGET_START_YEAR
		,b.IS_POSTED_TO_DV Posted
		,ent.ENTITY_NAME Budget_Status
	FROM
		dbo.Contract con 
		JOIN dbo.BUDGET_CONTRACT_BUDGET bcb
			ON bcb.CONTRACT_ID = con.CONTRACT_ID
		JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
			ON con.CONTRACT_ID = samm.Contract_ID
		JOIN dbo.METER m
			 ON samm.METER_ID = m.METER_ID
		JOIN dbo.BUDGET_ACCOUNT ba
			 ON ba.ACCOUNT_ID = m.ACCOUNT_ID
		JOIN dbo.BUDGET b
			 ON b.BUDGET_ID = ba.BUDGET_ID
		JOIN dbo.ENTITY ent
			 ON b.STATUS_TYPE_ID = ent.ENTITY_ID		
	WHERE 
		con.Contract_ID = @Contract_Id
		AND ba.IS_DELETED = 0
		AND b.BUDGET_START_YEAR BETWEEN YEAR(con.CONTRACT_START_DATE) AND YEAR(con.CONTRACT_END_DATE)
	GROUP BY
		 b.BUDGET_ID
		,b.BUDGET_START_YEAR
		,b.IS_POSTED_TO_DV
		,ent.ENTITY_NAME
	ORDER BY
		b.BUDGET_ID

END
GO
GRANT EXECUTE ON  [dbo].[Budget_Setup_Completed_Budget_Sel_For_Contract] TO [CBMSApplication]
GO
