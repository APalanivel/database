SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Invoice_Collection_DC_History_By_Invoice_Collection_Queue       
              
Description:              
			This sproc is to get the previously Final_Reviewed values in Invoice Collection Queue .      
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
	 @Invoice_Collection_Queue_Id		 INT
     @User_Info_Id						 INT                    
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

   Exec dbo.Invoice_Collection_DC_History_By_Invoice_Collection_Queue '36316,14',16
   
  
   
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2019-08-20		Created For Invoice_Collection.         
             
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_DC_History_By_Invoice_Collection_Queue]
    (
        @Invoice_Collection_Queue_Id VARCHAR(MAX)
        , @User_Info_Id INT
        , @Is_DC_Today BIT = NULL
        , @DC_From_Date DATE = NULL
        , @DC_To_Date DATE = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            CASE WHEN LEN(DC.Period_to_DC) > 0 THEN LEFT(DC.Period_to_DC, LEN(DC.Period_to_DC) - 1)
                ELSE DC.Period_to_DC
            END AS Period_DC
            , ical.Created_Ts Last_DC_Date
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME DC_By
            , ical.Invoice_Collection_Activity_Log_Id
        FROM
            dbo.Invoice_Collection_Activity_Log_Queue_Map icalqm
            INNER JOIN dbo.Invoice_Collection_Activity_Log ical
                ON icalqm.Invoice_Collection_Activity_Log_Id = ical.Invoice_Collection_Activity_Log_Id
            INNER JOIN dbo.Invoice_Collection_Queue icq
                ON icq.Invoice_Collection_Queue_Id = icalqm.Invoice_Collection_Queue_Id
            CROSS APPLY
        (   SELECT
                CAST(icalqm1.Collection_Start_Dt AS VARCHAR(12)) + '#' + CAST(icalqm1.Collection_End_Dt AS VARCHAR(12))
                + ','
            FROM
                dbo.Invoice_Collection_Activity_Log_Queue_Map icalqm1
                INNER JOIN dbo.Invoice_Collection_Activity_Log ical1
                    ON icalqm1.Invoice_Collection_Activity_Log_Id = ical1.Invoice_Collection_Activity_Log_Id
                INNER JOIN dbo.ufn_split(@Invoice_Collection_Queue_Id, ',') ufn
                    ON ufn.Segments = icalqm1.Invoice_Collection_Queue_Id
            WHERE
                ical1.Invoice_Collection_Activity_Log_Id = ical.Invoice_Collection_Activity_Log_Id
            GROUP BY
                icalqm1.Collection_Start_Dt
                , icalqm1.Collection_End_Dt
            FOR XML PATH('')) DC(Period_to_DC)
            INNER JOIN dbo.ufn_split(@Invoice_Collection_Queue_Id, ',') ufn
                ON ufn.Segments = icq.Invoice_Collection_Queue_Id
            INNER JOIN dbo.USER_INFO ui
                ON ui.USER_INFO_ID = ical.Created_User_Id
			INNER JOIN dbo.Invoice_Collection_Activity ica
			ON ica.Invoice_Collection_Activity_Id = ical.Invoice_Collection_Activity_Id
			INNER JOIN dbo.Code c
			ON c.Code_Id = ica.Invoice_Collection_Activity_Type_Cd
        WHERE
		    c.Code_Value = 'Attempt Download'
            AND (   @Is_DC_Today IS NULL
                AND (   (   @DC_From_Date IS NULL
                            OR  ical.Created_Ts >= @DC_From_Date)
                        AND (   @DC_To_Date IS NULL
                                OR  ical.Created_Ts <= @DC_To_Date))
                OR  (   @Is_DC_Today = 1
                        AND CAST(ical.Created_Ts AS DATE) = CAST(GETDATE() AS DATE)))
        GROUP BY
            CASE WHEN LEN(DC.Period_to_DC) > 0 THEN LEFT(DC.Period_to_DC, LEN(DC.Period_to_DC) - 1)
                ELSE DC.Period_to_DC
            END
            , ical.Created_Ts
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME
            , ical.Invoice_Collection_Activity_Log_Id
        ORDER BY
            ical.Created_Ts DESC;



    END;







    ;



    ;

    ;



GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_DC_History_By_Invoice_Collection_Queue] TO [CBMSApplication]
GO
