SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Sr_Rfp_Archive_Meter_Del]  
     
DESCRIPTION: 
	It Deletes SR RFP Archive Meter for Selected Sr RFP Archive Meter Id.
      
INPUT PARAMETERS:          
NAME							DATATYPE	DEFAULT		DESCRIPTION          
-------------------------------------------------------------------          
@Sr_Rfp_Archive_Meter_Del_Id	INT	

   
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------
  
	Begin Tran
		EXEC Sr_Rfp_Archive_Meter_Del  23957
	Rollback Tran
    
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR		PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR		27-MAY-10		CREATED     

*/  

CREATE PROCEDURE dbo.Sr_Rfp_Archive_Meter_Del
   (
    @Sr_Rfp_Archive_Meter_Id INT
   )
AS 
BEGIN

    SET NOCOUNT ON;
   
    DELETE
		dbo.SR_RFP_ARCHIVE_METER
	WHERE
		SR_RFP_ARCHIVE_METER_ID = @SR_RFP_Archive_Meter_Id
	
END
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Archive_Meter_Del] TO [CBMSApplication]
GO
