SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******

 NAME: 
			dbo.Inv_Sourced_Duplicate_Image_Sel  
			
 DESCRIPTION:
			To get the values from dbo.Inv_Sourced_Duplicate_Image . 

 INPUT PARAMETERS:  
      Name								DataType			Default			Description  
------------------------------------------------------------------------------------------------
	  @Processed_Start_Dt				DATE				 NULL
      @Processed_End_Dt					DATE				 NULL
      @Inv_Source_Id					INT					 NULL
      @Keyword							VARCHAR(200)		 NULL
      
 OUTPUT PARAMETERS:  
      Name								DataType			Default			Description  
------------------------------------------------------------------------------------------------
 
 USAGE EXAMPLES:  
------------------------------------------------------------------------------------------------
      EXEC dbo.Inv_Sourced_Duplicate_Image_Sel 
      
      EXEC dbo.Inv_Sourced_Duplicate_Image_Sel 
            @Inv_Source_Id = '6,1'
            
      EXEC dbo.Inv_Sourced_Duplicate_Image_Sel 
           @Keyword = '\ar'
           
      EXEC dbo.Inv_Sourced_Duplicate_Image_Sel 
            @Processed_Start_Dt = '2014-06-17'
           ,@Processed_End_Dt = '2014-08-17'
           
      EXEC dbo.Inv_Sourced_Duplicate_Image_Sel 
            @Processed_Start_Dt = '2014-06-17'
           ,@Processed_End_Dt = '2014-08-17'
           ,@Inv_Source_Id = '6,1'
           ,@Keyword = '\Ubm\Internal\Manual\Archive'
    
      

 AUTHOR INITIALS:  
 Initials				Name  
------------------------------------------------------------------------------------------------
 AV						Arjun Varma
 
 MODIFICATIONS   
 Initials				Date			Modification
------------------------------------------------------------------------------------------------
 AV						2014-07-17		Created for Invoice scraping image hash enh
 
******/
CREATE PROCEDURE [dbo].[Inv_Sourced_Duplicate_Image_Sel]
      ( 
       @Processed_Start_Dt DATE = NULL
      ,@Processed_End_Dt DATE = NULL
      ,@Inv_Source_Id VARCHAR(MAX) = NULL
      ,@Keyword VARCHAR(200) = NULL )
AS 
BEGIN
      SET NOCOUNT ON;
      
      DECLARE @Inv_Sourced_Duplicate_Image_List TABLE ( Inv_Source_Id INT )
      
      
      INSERT      INTO @Inv_Sourced_Duplicate_Image_List
                  ( 
                   Inv_Source_Id )
                  SELECT
                        x.Segments
                  FROM
                        dbo.ufn_split(@Inv_Source_Id, ',') x 
   	
      SELECT
            isdi.Image_File_Name AS Duplicate_Image_File_Name
           ,ise.SOURCE_FOLDER_PATH AS Duplicate_Image_Source_Folder
           ,isdi.Keyword
           ,isdi.Processed_Ts AS Duplicate_Image_Processed_Ts
           ,isi.CBMS_FILENAME AS Original_File_Name
           ,isce.SOURCE_FOLDER_PATH AS Original_File_Name_Source_Folder
           ,isce.ARCHIVE_FOLDER_PATH AS Original_File_Archive_Folder
           ,isib.BATCH_DATE AS Original_File_Batch_Ts
      FROM
            dbo.Inv_Sourced_Duplicate_Image isdi
            INNER JOIN dbo.INV_SOURCED_IMAGE isi
                  ON isdi.Original_Inv_Sourced_Image_Id = isi.INV_SOURCED_IMAGE_ID
            INNER JOIN dbo.INV_SOURCE ise
                  ON ise.INV_SOURCE_ID = isdi.Duplicate_Image_Inv_Source_Id
            INNER JOIN dbo.INV_SOURCE isce
                  ON isi.INV_SOURCE_ID = isce.INV_SOURCE_ID
            INNER  JOIN @Inv_Sourced_Duplicate_Image_List idl
                  ON isdi.Duplicate_Image_Inv_Source_Id = idl.Inv_Source_Id
            INNER JOIN dbo.INV_SOURCED_IMAGE_BATCH isib
                  ON isib.INV_SOURCED_IMAGE_BATCH_ID = isi.INV_SOURCED_IMAGE_BATCH_ID
      WHERE
            ( ( @Processed_Start_Dt IS NULL
                AND @Processed_End_Dt IS NULL )
              OR ( CAST(isdi.Processed_Ts AS DATE) BETWEEN @Processed_Start_Dt
                                                   AND     @Processed_End_Dt ) )
            AND ( @Keyword IS NULL
                  OR isdi.Keyword LIKE '%' + @Keyword + '%' )
 
END






;
GO
GRANT EXECUTE ON  [dbo].[Inv_Sourced_Duplicate_Image_Sel] TO [CBMSApplication]
GO
