SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:    [Workflow].[GetGridColumnData]  
DESCRIPTION:   
------------------------------------------------------------     
 INPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
@MODULE_ID  INT  
  
  
 OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
 USAGE EXAMPLES:    EXEC [Workflow].[GetGridColumnData] @MODULE_ID=1  
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
TRK   Ramakrishna Thummala Summit Energy   
   
 MODIFICATIONS       
 Initials Date   Modification      
------------------------------------------------------------      
 TRK    2019-07-03 Created  
  
******/          
               
CREATE PROC [Workflow].[GetGridColumnData]                  
   (                  
  @MODULE_ID INT                  
   )                  
AS                  
BEGIN      
    

 select       
   Workflow_Queue_Id AS Id    
  ,Display_Column_Name AS Display_Name    
  ,Is_Default AS IsDefault    
  ,Is_fixed as PermenantColumn    
  ,Header_CSS AS HeaderCSS    
  ,Row_CSS AS RowCSS    
  ,HyperLink_URL AS HyperLink    
  ,case when is_Fixed =1 then 0    
  else 1    
  end as is_selected    
     
 FROM Workflow.Workflow_queue_output_column       
 WHERE Workflow_Queue_Id = @MODULE_ID    
 AND Is_Visible_To_User <> 0         
 ORDER BY Display_order     
     
           
END;                

GO
GRANT EXECUTE ON  [Workflow].[GetGridColumnData] TO [CBMSApplication]
GO
