SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME: [DBO].[Supplier_Account_History_Del_By_Account_Id]    
       
DESCRIPTION:  
  
 To Delete all the history associated with the supplier account  
  
INPUT PARAMETERS:            
 NAME				DATATYPE			DEFAULT				DESCRIPTION            
-----------------------------------------------------------------------------            
 @Account_Id		INT  
 @User_Info_Id		INT  
  
OUTPUT PARAMETERS:  
NAME   DATATYPE DEFAULT  DESCRIPTION  
-----------------------------------------------------------------------------            
  
USAGE EXAMPLES:  
-----------------------------------------------------------------------------            
   
 SELECT * FROM dbo.SUPPLIER_ACCOUNT_METER_MAP  
                    
 BEGIN TRAN      
  SELECT * FROM dbo.SUPPLIER_ACCOUNT_METER_MAP where  Account_Id = 652012  
  EXEC dbo.Supplier_Account_History_Del_By_Account_Id 652012,49  
        SELECT * FROM dbo.SUPPLIER_ACCOUNT_METER_MAP where  Account_Id = 652012  
        SELECT TOP 5* FROM dbo.Application_Audit_Log   
   WHERE Application_Name ='Delete Supplier Account' ORDER BY Execution_Ts DESC  
 ROLLBACK TRAN    
       
   
BEGIN TRAN      
	   SELECT * FROM dbo.Cu_Invoice_Account_Commodity_Comment where   Account_Id = 1338934 
		SELECT * FROM dbo.Cu_Invoice_Account_Commodity_Comment_Recalc_Status_Type_Map where  Cu_Invoice_Account_Commodity_Comment_Id = 670188  
		EXEC dbo.[Supplier_Account_History_Del_By_Account_Id] 1338934,16  
        SELECT * FROM dbo.Cu_Invoice_Account_Commodity_Comment where  Account_Id = 1338934  
		SELECT * FROM dbo.Cu_Invoice_Account_Commodity_Comment_Recalc_Status_Type_Map where  Cu_Invoice_Account_Commodity_Comment_Id = 670188 
 ROLLBACK TRAN   
 
        
AUTHOR INITIALS:  
 INITIALS NAME  
-----------------------------------------------------------------------------            
 RR   Raghu Reddy
 ABK  Aditya Bharadwaj  
   
  
MODIFICATIONS  
 INITIALS DATE  MODIFICATION  
-----------------------------------------------------------------------------            
 RR   2017-02-23 Contract PLaceholder CP-54 Created  
       Reference script dbo.Utility_Account_History_Del_By_Account_Id   
 ABK	2019-06-14	SE2017-757:   Added code to delete data from Cu_Invoice_Account_Commodity_Comment_Recalc_Status_Type_Map table  
 NR		2019-12-18	Add Contract - Removed Account_Outside_Contract_Term_Config data when account deleted.
 NR		2020-01-16	MAINT-9734 - Deleted the Configuration also.  
  
*/

CREATE PROCEDURE [dbo].[Supplier_Account_History_Del_By_Account_Id]
    (
        @Account_Id INT
        , @User_Info_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Application_Name VARCHAR(30) = 'Delete Supplier Account'
            , @Audit_Function SMALLINT = -1
            , @User_Name VARCHAR(81)
            , @Client_Name VARCHAR(200)
            , @Current_Ts DATETIME
            , @Table_Name VARCHAR(10) = 'ACCOUNT'
            , @Lookup_Value XML
            , @Total_Row_Count INT
            , @Row_Counter INT
            , @Account_Exception_Id INT;

        DECLARE
            @Meter_Id INT
            , @Account_Entity_Id INT
            , @Commodity_Id INT
            , @Account_Commodity_Invoice_Recalc_Type_Id INT;

        DECLARE @Account_Exception TABLE
              (
                  Account_Exception_Id INT PRIMARY KEY CLUSTERED
                  , Row_Num INT IDENTITY(1, 1)
              );


        DECLARE @Account_Commodity_Invoice_Recalc_Type TABLE
              (
                  Account_Commodity_Invoice_Recalc_Type_Id INT PRIMARY KEY CLUSTERED
                  , Row_Num INT IDENTITY(1, 1)
              );

        DECLARE @Supplier_Account_Meter_Map TABLE
              (
                  Meter_Id INT
                  , Row_Num INT IDENTITY(1, 1)
              );

        SET @Lookup_Value = (   SELECT
                                    acc.ACCOUNT_NUMBER
                                    , acc.ACCOUNT_ID
                                    , mtr.METER_NUMBER
                                FROM
                                    dbo.ACCOUNT acc
                                    INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP sam
                                        ON acc.ACCOUNT_ID = sam.ACCOUNT_ID
                                    INNER JOIN dbo.METER mtr
                                        ON sam.METER_ID = mtr.METER_ID
                                WHERE
                                    acc.ACCOUNT_ID = @Account_Id
                                FOR XML AUTO);


        SELECT
            @Account_Entity_Id = ENTITY_ID
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_NAME = 'ACCOUNT_TABLE'
            AND ENTITY_DESCRIPTION = 'Table_Type';

        SELECT
            @Client_Name = CLIENT_NAME
        FROM
            dbo.ACCOUNT acc
            JOIN dbo.SITE s
                ON s.SITE_ID = acc.SITE_ID
            JOIN dbo.CLIENT cl
                ON cl.CLIENT_ID = s.Client_ID
        WHERE
            acc.ACCOUNT_ID = @Account_Id;

        SELECT
            @User_Name = FIRST_NAME + SPACE(1) + LAST_NAME
        FROM
            dbo.USER_INFO
        WHERE
            USER_INFO_ID = @User_Info_Id;

        INSERT INTO @Account_Exception
             (
                 Account_Exception_Id
             )
        SELECT
            ae.Account_Exception_Id
        FROM
            dbo.Account_Exception ae
        WHERE
            ae.Account_Id = @Account_Id;

        INSERT INTO @Account_Commodity_Invoice_Recalc_Type
             (
                 Account_Commodity_Invoice_Recalc_Type_Id
             )
        SELECT
            Account_Commodity_Invoice_Recalc_Type_Id
        FROM
            dbo.Account_Commodity_Invoice_Recalc_Type acirt
        WHERE
            Account_Id = @Account_Id;

        INSERT INTO @Supplier_Account_Meter_Map
             (
                 Meter_Id
             )
        SELECT
            METER_ID
        FROM
            dbo.SUPPLIER_ACCOUNT_METER_MAP
        WHERE
            ACCOUNT_ID = @Account_Id;

        BEGIN TRY

            SELECT  @Total_Row_Count = MAX(Row_Num)FROM @Account_Exception;

            SET @Row_Counter = 1;
            WHILE (@Row_Counter <= @Total_Row_Count)
                BEGIN
                    SELECT
                        @Account_Exception_Id = Account_Exception_Id
                    FROM
                        @Account_Exception
                    WHERE
                        Row_Num = @Row_Counter;

                    DELETE
                    dbo.Account_Exception
                    WHERE
                        Account_Exception_Id = @Account_Exception_Id;

                    SET @Row_Counter = @Row_Counter + 1;
                END;


            SELECT
                @Total_Row_Count = MAX(Row_Num)
            FROM
                @Account_Commodity_Invoice_Recalc_Type;
            SET @Row_Counter = 1;
            WHILE (@Row_Counter <= @Total_Row_Count)
                BEGIN

                    SELECT
                        @Account_Commodity_Invoice_Recalc_Type_Id = Account_Commodity_Invoice_Recalc_Type_Id
                    FROM
                        @Account_Commodity_Invoice_Recalc_Type
                    WHERE
                        Row_Num = @Row_Counter;

                    EXEC dbo.Account_Commodity_Invoice_Recalc_Type_Del
                        @Account_Commodity_Invoice_Recalc_Type_Id
                        , @User_Info_Id;

                    SET @Row_Counter = @Row_Counter + 1;

                END;

            SELECT  @Total_Row_Count = MAX(Row_Num)FROM @Supplier_Account_Meter_Map;

            SET @Row_Counter = 1;
            WHILE (@Row_Counter <= @Total_Row_Count)
                BEGIN
                    SELECT
                        @Meter_Id = Meter_Id
                    FROM
                        @Supplier_Account_Meter_Map
                    WHERE
                        Row_Num = @Row_Counter;

                    EXEC dbo.Supplier_Account_Meter_Map_Del
                        @Account_Id = @Account_Id
                        , @Meter_Id = @Meter_Id;

                    SET @Row_Counter = @Row_Counter + 1;
                END;

            -- Cost/Usage History delete for account  
            EXEC dbo.Cost_Usage_History_Del_For_Account @Account_Id;

            EXEC dbo.Invoice_Participation_History_Del_For_Account @Account_Id;

            EXEC dbo.Account_Other_History_Del_By_Account_Id @Account_Id;

            EXEC dbo.Entity_Audit_Del_By_Entity_Id_Entity_Identifier
                @Account_Entity_Id
                , @Account_Id;

            EXEC dbo.Account_Comment_Del @Account_Id;

            DELETE
            ciaccrstm
            FROM
                dbo.Cu_Invoice_Account_Commodity_Comment ciacc
                INNER JOIN dbo.Cu_Invoice_Account_Commodity_Comment_Recalc_Status_Type_Map ciaccrstm
                    ON ciaccrstm.Cu_Invoice_Account_Commodity_Comment_Id = ciacc.Cu_Invoice_Account_Commodity_Comment_Id
            WHERE
                Account_Id = @Account_Id;

            DELETE
            ciacc
            FROM
                dbo.Cu_Invoice_Account_Commodity_Comment ciacc
            WHERE
                Account_Id = @Account_Id;

            --Invoice Collection Setup  
            EXEC dbo.Account_Inovice_Collection_History_Del_By_Account_Id
                @Account_Id = @Account_Id;

            DELETE
            aoctc
            FROM
                dbo.Account_Outside_Contract_Term_Config aoctc
            WHERE
                aoctc.Account_Id = @Account_Id;


            DELETE
            sac
            FROM
                dbo.Supplier_Account_Config sac
            WHERE
                sac.Account_Id = @Account_Id;


            EXEC dbo.Account_Del @Account_Id;

            SET @Current_Ts = GETDATE();

            EXEC dbo.Application_Audit_Log_Ins
                @Client_Name
                , @Application_Name
                , @Audit_Function
                , @Table_Name
                , @Lookup_Value
                , @User_Name
                , @Current_Ts;


        END TRY
        BEGIN CATCH
            EXEC dbo.usp_RethrowError;
        END CATCH;

    END;
    ;

    ;


GO
GRANT EXECUTE ON  [dbo].[Supplier_Account_History_Del_By_Account_Id] TO [CBMSApplication]
GO
