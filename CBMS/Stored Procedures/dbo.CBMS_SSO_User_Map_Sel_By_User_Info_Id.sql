SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        dbo.CBMS_SSO_User_Map_Sel_By_User_Info_Id                      
                        
Description:                        
        
                        
Input Parameters:                        
    Name			DataType        Default     Description                          
--------------------------------------------------------------------------------  
	@App_Name		NVARCHAR(255)
    @User_Info_Id	INT
    @SSO_XUser_Id	NVARCHAR(255)
                        
 Output Parameters:                              
 Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
     
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
 
	EXEC dbo.CBMS_SSO_User_Map_Sel_By_User_Info_Id  
  
Author Initials:                        
    Initials    Name                        
--------------------------------------------------------------------------------  
    RR          Raghu Reddy     
                         
 Modifications:                        
    Initials	Date        Modification                        
--------------------------------------------------------------------------------  
	RR          21-02-2019  Created GRM  
                       
******/
CREATE PROCEDURE [dbo].[CBMS_SSO_User_Map_Sel_By_User_Info_Id] ( @User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;
      
      SELECT
            csum.User_Info_Id
           ,csum.SSO_XUser_Id
           ,csa.App_Name
      FROM
            dbo.CBMS_SSO_User_Map csum
            INNER JOIN dbo.CBMS_SSO_App csa
                  ON csum.CBMS_SSO_App_Id = csa.CBMS_SSO_App_Id
      WHERE
            csum.User_Info_Id = @User_Info_Id

END;
GO
GRANT EXECUTE ON  [dbo].[CBMS_SSO_User_Map_Sel_By_User_Info_Id] TO [CBMSApplication]
GO
