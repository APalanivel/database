SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsSsoPriceIndex_GetAdditionalIndexes

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE   PROCEDURE [dbo].[cbmsSsoPriceIndex_GetAdditionalIndexes]

	( @MyAccountId int
	)
AS
BEGIN

        declare @begin_date datetime
              , @end_date datetime
              , @month1 datetime
              , @month2 datetime
              , @month3 datetime
              , @month4 datetime
              , @month5 datetime
              , @month6 datetime

               select @end_date = max(index_month)
                 from price_index_value
		where price_index_id in (271, 196, 200, 201)

               set @month6 = @end_date
               set @month5 = dateadd(m, -1, @month6)
               set @month4 = dateadd(m, -1, @month5)
               set @month3 = dateadd(m, -1, @month4)
               set @month2 = dateadd(m, -1, @month3)
               set @month1 = dateadd(m, -1, @month2)
               set @begin_date = @month1

               select v.price_index_id
                    , pr.pricing_point
                    , 'month6_label' = @month6 
                    , 'month5_label' = @month5 
                    , 'month4_label' = @month4 
                    , 'month3_label' = @month3 
                    , 'month2_label' = @month2 
                    , 'month1_label' = @month1 
                    , 'month1' = max(case when v.index_month = @month1 then v.index_value else 0 end)
                    , 'month2' = max(case when v.index_month = @month2 then v.index_value else 0 end)
                    , 'month3' = max(case when v.index_month = @month3 then v.index_value else 0 end)
                    , 'month4' = max(case when v.index_month = @month4 then v.index_value else 0 end)
                    , 'month5' = max(case when v.index_month = @month5 then v.index_value else 0 end)
                    , 'month6' = max(case when v.index_month = @month6 then v.index_value else 0 end)
                 from price_index_value v 
                 join price_index pr on pr.price_index_id = v.price_index_id
                where v.index_month between @begin_Date and @end_date
                  and v.price_index_id in (271, 196, 200, 201)
             group by v.price_index_id
                    , pr.pricing_point
             order by pr.pricing_point
END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoPriceIndex_GetAdditionalIndexes] TO [CBMSApplication]
GO
