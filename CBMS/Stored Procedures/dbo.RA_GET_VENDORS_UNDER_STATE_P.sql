SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE dbo.RA_GET_VENDORS_UNDER_STATE_P 
@userId varchar,
@sessionId varchar,
@stateId int

as
set nocount on
	Select	vendor.VENDOR_ID,
		vendor.VENDOR_NAME 
	
	from 	VENDOR vendor,
		VENDOR_STATE_MAP vendorState
	
	where	vendor.VENDOR_ID=vendorState.VENDOR_ID AND
		vendorState.STATE_ID=@stateId
	
	order by VENDOR_NAME
GO
GRANT EXECUTE ON  [dbo].[RA_GET_VENDORS_UNDER_STATE_P] TO [CBMSApplication]
GO
