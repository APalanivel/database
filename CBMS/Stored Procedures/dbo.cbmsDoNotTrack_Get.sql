SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  procedure [dbo].[cbmsDoNotTrack_Get]
	( @MyAccountId int
	, @do_not_track_id int
	)
AS
BEGIN

	   select do_not_track_id
		, user_info_id
		, account_id
		, ubm_id
		, ubm_account_code
		, reason_type_id
		, account_number
		, client_name
		, client_city
		, state_id
		, vendor_name
		, date_added
	     from do_not_track
	    where do_not_track_id = @do_not_track_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsDoNotTrack_Get] TO [CBMSApplication]
GO
