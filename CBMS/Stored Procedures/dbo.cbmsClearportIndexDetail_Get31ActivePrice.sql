SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE    PROCEDURE [dbo].[cbmsClearportIndexDetail_Get31ActivePrice] 
	( @MyAccountId int 
	, @clearport_index_id_1 int
	, @clearport_index_id_2 int
	, @clearport_index_id_3 int
	, @clearport_index_id_1_alias varchar(100)
	, @clearport_index_id_2_alias varchar(100)
	, @clearport_index_id_3_alias varchar(100)
	)
AS
BEGIN

    select  convert(varchar(2),month(d.index_detail_date)) + '/' + convert(varchar(2),day(d.index_detail_date)) [Date] --convert(varchar(10),d.index_detail_date,101) [Date]
	  , d.index_detail_value Price
	  , i.clearport_index as [Index]
	  , 'Name' = @clearport_index_id_1_alias
	  , d.index_detail_date DetailDate
      from  clearport_index_detail d
      join  clearport_index_months m on m.clearport_index_month_id = d.clearport_index_month_id
 
      join (
	     select top 31 d.index_detail_date 
	   , min(m.clearport_index_month) min_month
	       from clearport_index_detail d
	       join clearport_index_months m on m.clearport_index_month_id = d.clearport_index_month_id
	      where m.clearport_index_id = @clearport_index_id_1
	   group by d.index_detail_date 
		order by d.index_detail_date desc
		) x on x.index_detail_date = d.index_detail_date and x.min_month = m.clearport_index_month
      join clearport_index i on i.clearport_index_id = m.clearport_index_id
     where m.clearport_index_id = @clearport_index_id_1
  
 
union all

select  convert(varchar(2),month(d.index_detail_date)) + '/' + convert(varchar(2),day(d.index_detail_date))  [Date]--convert(varchar(10),d.index_detail_date,101) [Date]
	  , d.index_detail_value Price
	  , i.clearport_index  as [Index]
	  , 'Name' = @clearport_index_id_2_alias
	  , d.index_detail_date DetailDate
      from clearport_index_detail d
      join clearport_index_months m on m.clearport_index_month_id = d.clearport_index_month_id
 
      join (
	     select top 31 d.index_detail_date 
	   , min(m.clearport_index_month) min_month
	       from clearport_index_detail d
	       join clearport_index_months m on m.clearport_index_month_id = d.clearport_index_month_id
	      where m.clearport_index_id = @clearport_index_id_2
	   group by d.index_detail_date 
		order by d.index_detail_date desc
    ) x on x.index_detail_date = d.index_detail_date and x.min_month = m.clearport_index_month
       join clearport_index i on i.clearport_index_id = m.clearport_index_id

     where m.clearport_index_id = @clearport_index_id_2
  

union all

  select  convert(varchar(2),month(d.index_detail_date)) + '/' + convert(varchar(2),day(d.index_detail_date))  [Date]--convert(varchar(10),d.index_detail_date,101) [Date]
	  , d.index_detail_value Price
	  , i.clearport_index  as [Index]
	  , 'Name' = @clearport_index_id_3_alias
	  , d.index_detail_date DetailDate
      from clearport_index_detail d
      join clearport_index_months m on m.clearport_index_month_id = d.clearport_index_month_id
 
      join (
	     select top 31 d.index_detail_date 
	   , min(m.clearport_index_month) min_month
	       from clearport_index_detail d
	       join clearport_index_months m on m.clearport_index_month_id = d.clearport_index_month_id
	      where m.clearport_index_id = @clearport_index_id_3
	   group by d.index_detail_date 
		order by d.index_detail_date desc
    ) x on x.index_detail_date = d.index_detail_date and x.min_month = m.clearport_index_month
       join clearport_index i on i.clearport_index_id = m.clearport_index_id

     where m.clearport_index_id = @clearport_index_id_3



union all


select top 31  convert(varchar(2),month(y.Date)) + '/' + convert(varchar(2),day(y.Date)) [Date]--convert(varchar(10),y.Date,101) [Date]
		, y.Price
		, ''  as [Index]
		, 'NYMEX' as Name
	  	, y.Date DetailDate
	from (select top 31 x.last_trade_date [Date]
		, x.ClosePrice  Price
		, ''  as [Index]
		, 'NYMEX' as Name
	from
		(select distinct(last_trade_date)
		, 'ClosePrice' = (select close_price from rm_nymex_data where rm_nymex_data_id = (select min(rm_nymex_data_id) from rm_nymex_data where last_trade_date = rm.last_trade_date))
		from rm_nymex_data rm ) x
		order by x.last_trade_date desc) y

 
  order by [Name], DetailDate desc


END



GO
GRANT EXECUTE ON  [dbo].[cbmsClearportIndexDetail_Get31ActivePrice] TO [CBMSApplication]
GO
