SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.SR_UPDATE_MASTER_BATCH_LOG_P
	@batch_end_time datetime,
	@master_batch_log_id int
	AS
	set nocount on
	update sr_batch_master_log set batch_end_date = @batch_end_time
	where sr_batch_master_log_id = @master_batch_log_id
GO
GRANT EXECUTE ON  [dbo].[SR_UPDATE_MASTER_BATCH_LOG_P] TO [CBMSApplication]
GO
