SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.EC_Invoice_Sub_Bucket_Master_Upd           
              
Description:              
        To update Data to EC_Invoice_Sub_Bucket_Master table.              
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------  
	@EC_Invoice_Sub_Bucket_Master_Id	INT               
	@State_Id							INT
    @Bucket_Master_Id					INT
    @tvp_EC_Invoice_Sub_Bucket_Master	tvp_EC_Invoice_Sub_Bucket_Master
    @User_Info_Id						INT              
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------  
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                
	DECLARE  @tvp_EC_Invoice_Sub_Bucket_Master_Dtl dbo.tvp_EC_Invoice_Sub_Bucket_Master
		INSERT @tvp_EC_Invoice_Sub_Bucket_Master_Dtl  
		SELECT 5,'Test1' 
		UNION ALL  
		SELECT 6,'Test2' 
    BEGIN TRAN 	
    SELECT * FROM dbo.EC_Invoice_Sub_Bucket_Master eisbm WHERE EC_Invoice_Sub_Bucket_Master_Id in (5,6)
    EXEC dbo.EC_Invoice_Sub_Bucket_Master_Upd 
	  @State_Id = 1
     ,@Bucket_Master_Id = 70
     ,@tvp_EC_Invoice_Sub_Bucket_Master = @tvp_EC_Invoice_Sub_Bucket_Master_Dtl
     ,@User_Info_Id = 49
    SELECT * FROM dbo.EC_Invoice_Sub_Bucket_Master eisbm WHERE EC_Invoice_Sub_Bucket_Master_Id in (5,6)
    ROLLBACK TRAN             
             
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy               
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2015-04-22		Created For AS400.         
             
******/ 
CREATE PROCEDURE [dbo].[EC_Invoice_Sub_Bucket_Master_Upd]
      ( 
       @State_Id INT
      ,@Bucket_Master_Id INT
      ,@tvp_EC_Invoice_Sub_Bucket_Master dbo.tvp_EC_Invoice_Sub_Bucket_Master READONLY
      ,@User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON
      
      BEGIN TRY        
            BEGIN TRAN
      
            UPDATE
                  eisbm
            SET   
                  State_Id = @State_Id
                 ,Bucket_Master_Id = @Bucket_Master_Id
                 ,Sub_Bucket_Name = sbn.Sub_Bucket_Name
                 ,Updated_User_Id = @User_Info_Id
                 ,Last_Change_Ts = GETDATE()
            FROM
                  @tvp_EC_Invoice_Sub_Bucket_Master sbn
                  INNER JOIN dbo.EC_Invoice_Sub_Bucket_Master eisbm
                        ON eisbm.EC_Invoice_Sub_Bucket_Master_Id = sbn.EC_Invoice_Sub_Bucket_Master_Id
            WHERE
                  sbn.EC_Invoice_Sub_Bucket_Master_Id IS NOT NULL      

            INSERT      INTO dbo.EC_Invoice_Sub_Bucket_Master
                        ( 
                         State_Id
                        ,Bucket_Master_Id
                        ,Sub_Bucket_Name
                        ,Created_User_Id
                        ,Created_Ts
                        ,Updated_User_Id
                        ,Last_Change_Ts )
                        SELECT
                              @State_Id
                             ,@Bucket_Master_Id
                             ,sb.Sub_Bucket_Name
                             ,@User_Info_Id
                             ,GETDATE()
                             ,@User_Info_Id
                             ,GETDATE()
                        FROM
                              @tvp_EC_Invoice_Sub_Bucket_Master sb
                        WHERE
                              EC_Invoice_Sub_Bucket_Master_Id IS NULL 
                        
            COMMIT TRAN        
      END TRY        
      BEGIN CATCH        
            IF @@ROWCOUNT > 0 
                  ROLLBACK TRAN   
                 
            EXEC usp_RethrowError        
      END CATCH    

END



;
GO
GRANT EXECUTE ON  [dbo].[EC_Invoice_Sub_Bucket_Master_Upd] TO [CBMSApplication]
GO
