SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE dbo.UPDATE_RATE_SCHEDULE_P
	@billDaysAdj smallINT,
	@rsGrandTotalExpression VARCHAR(1000),
	@rateScheduleId INT
AS
BEGIN

	SET NOCOUNT ON

	UPDATE dbo.rate_schedule 
		SET billing_days_adjustment = @billDaysAdj ,
			rs_grand_total_expression = @rsGrandTotalExpression
	WHERE rate_schedule_id = @rateScheduleId

END
GO
GRANT EXECUTE ON  [dbo].[UPDATE_RATE_SCHEDULE_P] TO [CBMSApplication]
GO
