SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE dbo.GET_SITE_PRIMARY_ADDRESS_ID_P
	@site_id int
	AS 
	begin
		set nocount on

		select primary_address_id from site where site_id = @site_id

	end
GO
GRANT EXECUTE ON  [dbo].[GET_SITE_PRIMARY_ADDRESS_ID_P] TO [CBMSApplication]
GO
