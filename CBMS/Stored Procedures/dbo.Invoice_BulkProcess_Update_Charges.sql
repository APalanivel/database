SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                    
/******                                                
NAME:    [[Invoice_BulkProcess_Update_Charges]]                                          
DESCRIPTION:                                             
------------------------------------------------------------                                               
 INPUT PARAMETERS: @BATCH_ID INT ,                                          
 -----------------------------------------------------------          
 unit test case                       
[Invoice_BulkProcess_Update_Charges] 4917          
[Invoice_BulkProcess_Update_Charges] 4739          
[Invoice_BulkProcess_Update_Charges] 4738          
[Invoice_BulkProcess_Update_Charges] 4735          
[Invoice_BulkProcess_Update_Charges] 4734                                 
------------------------------------------------------------                                                
AUTHOR INITIALS:                                                
Initials Name                                                
------------------------------------------------------------                                                
PK   Prasan kumar                          
                                             
 MODIFICATIONS                                                 
 Initials  Date  Modification                                                
------------------------------------------------------------                                                
 PK   10-06-2020  1) sub bucket logic has changes to take directly (not distinct clause) min value  
       2) error msg has changed     
       3) adding new auto subbucket name to excel email attachement   
       4) removed auto mapping status code                      
******/                      
CREATE PROCEDURE [dbo].[Invoice_BulkProcess_Update_Charges]                      
    (                      
        @batch_detail_id INT                      
    )                      
AS                      
    BEGIN                      
                      
        SET NOCOUNT ON;                      
                      
        DECLARE                      
            @statusfail INT                      
            , @statuspass INT                      
            , @statusinprogress INT                        
            , @Current_Bucket_Name VARCHAR(255)                      
            , @New_Bucket_Name VARCHAR(255)                      
            , @Current_Sub_Bucket_Name VARCHAR(255)                      
            , @New_Sub_Bucket_Name VARCHAR(255)       
            , @Bucket_code VARCHAR(10)                      
            , @user_id INT                      
            , @New_bucket_id INT                      
            , @New_sub_bucket_id INT                      
            , @Invoice_id INT                      
            , @IsUpdatable BIT = 0          
   , @IsAutoSubBucketPerformed BIT = 0        
                  
   ,@Commodity_type_id INT      
   ,@state_id INT;      
           
        CREATE TABLE #EC_Invoice_Sub_Bucket_Master      
  (         
   Sub_Bucket_Master_Id INT      
   ,Sub_Bucket_Name varchar(255)      
         
  );        
                      
        BEGIN TRY                      
                     
            SELECT                      
                @user_id = [b].[Requested_User_Id]                      
                , @Bucket_code = [b_detail].[Bucket_Code]                      
                , @Current_Bucket_Name = [b_detail].[Current_Bucket_Name]                      
                , @New_Bucket_Name = [b_detail].[New_Bucket_Name]                      
                , @Current_Sub_Bucket_Name = [b_detail].[Current_Sub_Bucket_Name]                      
                , @New_Sub_Bucket_Name = isnull([b_detail].[New_Sub_Bucket_Name],'')                     
                , @Invoice_id = [b_detail].[Cu_Invoice_Id]                      
            FROM                      
                [LOGDB].[dbo].[XL_Bulk_Invoice_Process_Batch_Dtl] AS [b_detail]                      
                , [LOGDB].[dbo].[XL_Bulk_Invoice_Process_Batch] AS [b]                      
            WHERE                      
                [b_detail].[XL_Bulk_Data_Process_Batch_Dtl_Id] = @batch_detail_id                      
                AND [b_detail].[XL_Bulk_Invoice_Process_Batch_Id] = [b].[XL_Bulk_Invoice_Process_Batch_Id];                      
                      
            SELECT                      
                @statusinprogress = MAX(CASE WHEN [cd].[Code_Value] = 'In Progress' THEN [cd].[Code_Id]     END)                      
                , @statuspass = MAX(CASE WHEN [cd].[Code_Value] = 'Completed' THEN [cd].[Code_Id]      END)                      
                , @statusfail = MAX(CASE WHEN [cd].[Code_Value] = 'Error' THEN [cd].[Code_Id]      END)                          
            FROM                      
                [dbo].[Code] AS [cd]                      
    INNER JOIN [dbo].[Codeset] AS [cs]                      
                    ON [cs].[Codeset_Id] = [cd].[Codeset_Id]                      
   WHERE                      
                [cd].[Is_Active] = 1                      
                AND [cs].[Codeset_Name] = 'Batch Status';                      
                      
                  
                      
   UPDATE                      
                [LOGDB].[dbo].[XL_Bulk_Invoice_Process_Batch_Dtl]                      
            SET                      
                [Xl_Bulk_Invoice_Process_Batch_Dtl].[Status_Cd] = @statusinprogress                      
            WHERE                      
                [Xl_Bulk_Invoice_Process_Batch_Dtl].[Xl_Bulk_Data_Process_Batch_Dtl_Id] = @batch_detail_id;                      
                      
                      
            SELECT                      
     @Current_Bucket_Name = MAX([bm].[Bucket_Name])                      
                , @Current_Sub_Bucket_Name = MAX([sb].[Sub_Bucket_Name])                   
    , @Commodity_type_id = MAX([ic].COMMODITY_TYPE_ID)                      
            FROM                      
                [dbo].[CU_INVOICE_CHARGE] AS [ic]                      
                INNER JOIN [dbo].[Bucket_Master] AS [bm]                      
                    ON [bm].[Bucket_Master_Id] = [ic].[Bucket_Master_Id]                      
                LEFT JOIN [dbo].[EC_Invoice_Sub_Bucket_Master] AS [sb]                      
                    ON [sb].[EC_Invoice_Sub_Bucket_Master_Id] = [ic].[EC_Invoice_Sub_Bucket_Master_Id]                      
            WHERE                      
                [ic].[CU_INVOICE_ID] = @Invoice_id                      
                AND [ic].[CU_DETERMINANT_CODE] = @Bucket_code;                      
                      
                      
            SELECT                      
                @New_bucket_id = [bm].[Bucket_Master_Id]                      
            FROM   [dbo].[Bucket_Master] AS [bm]                    
                INNER JOIN [dbo].[Code] AS [c]                      
                    ON [bm].[Bucket_Type_Cd] = [c].[Code_Id]                      
            WHERE                      
                [c].[Code_Value] = 'Charge'                      
                AND [bm].[Is_Shown_On_Invoice] = 1                      
                AND [bm].[Commodity_Id] = @Commodity_type_id                      
                AND [bm].[Bucket_Name] = @New_Bucket_Name;                      
                      
                    
   --------------- sub bucket logic-----------                  
            -- declare  at top and temp tble shld drop at end      
      
  SELECT TOP 1 @state_id = [cha].[Meter_State_Id]                    
                     FROM  [dbo].[CU_INVOICE_SERVICE_MONTH] AS [sm]                   
                         INNER JOIN [Core].[Client_Hier_Account] AS [cha]                    
                             ON [cha].[Account_Id] = [sm].[Account_ID]    
                     WHERE                    
                         [sm].[CU_INVOICE_ID] = @Invoice_id        
        
      
   INSERT INTO #EC_Invoice_Sub_Bucket_Master      
   (      
    Sub_Bucket_Master_Id       
    ,Sub_Bucket_Name       
   )      
   SELECT                  
    EC_Invoice_Sub_Bucket_Master_Id,      
    Sub_Bucket_Name                
   FROM                  
    [dbo].[EC_Invoice_Sub_Bucket_Master] AS [sb]                 
   WHERE  [sb].[Bucket_Master_Id] = @New_bucket_id                  
     AND [sb].[State_Id] = @state_id      
                  
           
  SELECT @New_sub_bucket_id = Sub_Bucket_Master_Id      
  FROM  #EC_Invoice_Sub_Bucket_Master       
  WHERE Sub_Bucket_Name = @New_Sub_Bucket_Name      
                  
      
  IF  @New_sub_bucket_id IS NULL                                    
  BEGIN      
           
	SELECT @New_sub_bucket_id = Sub_Bucket_Master_Id      
	FROM  #EC_Invoice_Sub_Bucket_Master       
	WHERE Sub_Bucket_Name = @New_Bucket_Name
          
	SELECT @IsAutoSubBucketPerformed = CASE WHEN @New_sub_bucket_id IS NOT NULL THEN 1 ELSE 0 END   
  END
  
  IF   @New_sub_bucket_id IS NULL  
  BEGIN        
     
	SELECT @New_sub_bucket_id = MIN(Sub_Bucket_Master_Id)  
	FROM  #EC_Invoice_Sub_Bucket_Master   
	--WHERE @New_sub_bucket_id IS NULL         
	--HAVING    
	-- COUNT(DISTINCT Sub_Bucket_Master_Id) = 1; -- Added to get the subbucket only when one to one mapping exists   
	       
	SELECT @IsAutoSubBucketPerformed = CASE WHEN @New_sub_bucket_id IS NOT NULL THEN 1 ELSE 0 END                
  END   
   
  ----------------end -----------------                     
                             
                      
                      
           IF @New_bucket_id IS NOT NULL                    
   AND  @New_sub_bucket_id IS NOT NULL                    
                SET @IsUpdatable = 1;                    
           ELSE IF @New_bucket_id IS NOT NULL          
    AND @New_Sub_Bucket_Name = ''         
    AND NOT EXISTS (         
      SELECT 1                     
       FROM #EC_Invoice_Sub_Bucket_Master          
     )              
                SET @IsUpdatable = 1;                    
                      
          
            UPDATE                      
                [ic]                      
            SET                      
                [ic].[Bucket_Master_Id] = @New_bucket_id                      
                , [ic].[EC_Invoice_Sub_Bucket_Master_Id] = @New_sub_bucket_id                      
            FROM                      
                [dbo].[CU_INVOICE_CHARGE] AS [ic]                      
            WHERE                      
                [ic].[CU_INVOICE_ID] = @Invoice_id                      
                AND [ic].[CU_DETERMINANT_CODE] = @Bucket_code                      
				AND @IsUpdatable = 1;                      
                     
  
   UPDATE    [LOGDB].[dbo].[XL_Bulk_Invoice_Process_Batch_Dtl]   
   SET New_Sub_Bucket_Name = ( SELECT Sub_Bucket_Name               
          FROM   #EC_Invoice_Sub_Bucket_Master              
          WHERE   Sub_Bucket_Master_Id =  @New_sub_bucket_id )  
   WHERE Xl_Bulk_Data_Process_Batch_Dtl_Id = @batch_detail_id  
   AND @IsUpdatable = 1  
   AND @IsAutoSubBucketPerformed = 1        
           
   --------- error status and history log ---------        
   UPDATE                      
                [LOGDB].[dbo].[XL_Bulk_Invoice_Process_Batch_Dtl]                      
            SET                      
                Status_Cd = CASE WHEN @IsUpdatable = 1 THEN @statuspass               
       ELSE @statusfail                      
       END                      
       , Error_Msg = CASE WHEN @New_bucket_id IS NULL THEN  'Fail. Incorrect Bucket Name Entered.'                                 
          WHEN @New_sub_bucket_id IS NULL                   
          AND @New_Sub_Bucket_Name <> ''         
          AND NOT EXISTS (         
            SELECT 1                     
             FROM #EC_Invoice_Sub_Bucket_Master          
           )  THEN                      
          'Fail. Incorrect Sub-Bucket Name Entered.'          
             
          END                      
            WHERE                      
                Xl_Bulk_Data_Process_Batch_Dtl_Id = @batch_detail_id;        
        
            SELECT   @New_Sub_Bucket_Name = Sub_Bucket_Name               
   FROM   #EC_Invoice_Sub_Bucket_Master              
   WHERE   Sub_Bucket_Master_Id =  @New_sub_bucket_id        
              
            INSERT INTO [dbo].[CU_INVOICE_CHANGE_LOG]                      
                 (                      
                     [CU_INVOICE_ID]                
                     , [CHANGE_TYPE_ID]                      
                     , [FIELD_TYPE_ID]                      
                     , [FIELD_NAME]                      
                     , [PREVIOUS_VALUE]                      
                     , [CURRENT_VALUE]                      
                     , [CHANGE_DATE]                      
                     , [Bucket_Number]                      
                     , [Updated_User_Id]                      
                 )                      
            SELECT                      
                [bucket_values].[cu_invoice_id]                      
                , [bucket_values].[change_type_id]                      
                , [bucket_values].[field_type_id]            
                , [bucket_values].[field_name]                      
                , [bucket_values].[previous_value]                      
                , [bucket_values].[current_value]                      
                , [bucket_values].[change_date]                      
                , [bucket_values].[Bucket_Number]                      
                , [bucket_values].[Updated_User_Id]                      
            FROM                      
            (   VALUES (@Invoice_id, 932, 933, 'Charge Bucket', @Current_Bucket_Name, @New_Bucket_Name, GETDATE()                      
                        , @Bucket_code, @user_id)                      
                       , (@Invoice_id, 932, 933, 'Charge Sub Bucket', @Current_Sub_Bucket_Name, isnull(@New_Sub_Bucket_Name,'')                
                          , GETDATE(), @Bucket_code, @user_id)) AS [bucket_values] ([cu_invoice_id], [change_type_id]                      
                                                                                    , [field_type_id], [field_name]                      
                                                                                    , [previous_value], [current_value]                      
                                                                                    , [change_date], [Bucket_Number]                      
                                                                                    , [Updated_User_Id])                      
            WHERE                      
                @IsUpdatable = 1                      
                AND [bucket_values].[previous_value] <> [bucket_values].[current_value];                      
                      
            --------- END ---------                      
                      
             IF OBJECT_ID('tempdb.dbo.#EC_Invoice_Sub_Bucket_Master', 'U') IS NOT NULL      
    DROP TABLE #EC_Invoice_Sub_Bucket_Master;         
           
        END TRY                      
        BEGIN CATCH                      
                      
            DECLARE                      
                @Error_Line INT                      
                , @Error_Message VARCHAR(3000);                      
            -- Entry made to the logging SP to capture the errors.                                          
            SELECT  @Error_Line = ERROR_LINE(), @Error_Message = ERROR_MESSAGE();                      
                      
            INSERT INTO [dbo].[StoredProc_Error_Log]                      
                 (                      
                     [StoredProc_Name]                      
                     , [Error_Line]                      
                     , [Error_message]                      
                     , [Input_Params]                      
                 )                      
            VALUES                     
                ('[Invoice_BulkProcess_Update_Charges]'                      
                 , @Error_Line                      
                 , @Error_Message                      
                 , 'Batch detail id: ' + @batch_detail_id);                      
                      
            EXEC [dbo].[usp_RethrowError] @Error_Message;                      
        END CATCH;                      
    END; 
GO
GRANT EXECUTE ON  [dbo].[Invoice_BulkProcess_Update_Charges] TO [CBMSApplication]
GO
