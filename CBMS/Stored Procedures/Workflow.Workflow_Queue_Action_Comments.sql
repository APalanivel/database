SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

    
    
/******        
NAME:    [Workflow].[Workflow_Queue_Action_Comments]    
DESCRIPTION:     
------------------------------------------------------------       
 INPUT PARAMETERS:        
 Name   DataType  Default Description      
 @MODULE_ID INT,    
    @Workflow_Queue_Saved_Filter_Query_Id INT = NULL,    
    @Logged_In_User INT,    
    @Queue_id VARCHAR(MAX) = NULL,     ---  User selected from drop down Id 49,1,2,3              
    @Exception_Type VARCHAR(MAX) = NULL,    
    @Account_Number VARCHAR(500) = NULL,    
    @Client VARCHAR(500) = NULL,    
    @Site VARCHAR(500) = NULL,    
    @Country VARCHAR(500) = NULL,    
    @State VARCHAR(500) = NULL,    
    @City VARCHAR(500) = NULL,    
    @Commodity VARCHAR(500) = NULL,    
    @Invoice_ID VARCHAR(500) = NULL,    
    @Priority INT = 0,                 --- Yes              
    @Exception_Status INT = NULL,    
    @Start_Date_in_Queue DATETIME = NULL,    
    @End_Date_in_Queue DATETIME = NULL,    
    @Start_Date_in_CBMS DATETIME = NULL,    
    @End_Date_in_CBMS DATETIME = NULL,    
    @Data_Source INT = NULL,    
    @Vendor VARCHAR(100) = NULL,    
    @Vendor_Type VARCHAR(100) = NULL,    
    @Month DATETIME = NULL,    
    @Filename VARCHAR(100) = NULL,    
    @invoice_List VARCHAR(MAX) = NULL, ---- if new invoice needs to be added send those invoices     
    @User_Group_Id_List VARCHAR(MAX),    
    @Invoice_List_TVP Invoice_List READONLY,    
    @Comments  VARCHAR(MAX) = NULL           
------------------------------------------------------------        
 OUTPUT PARAMETERS:        
 Name   DataType  Default Description        
------------------------------------------------------------        
 USAGE EXAMPLES:        
 /*        
 EXEC [Workflow].[Workflow_Queue_Action_Comments_New] 80209191,49,'Testingq2'        
 SELECT * FROM dbo.cu_invoice_comment WHERE CU_INVOICE_ID=80209191        
 DELETE FROM dbo.cu_invoice_comment WHERE CU_INVOICE_ID=76187343        
         
 */     
------------------------------------------------------------        
AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
TRK   Ramakrishna Thummala Summit Energy     
     
 MODIFICATIONS         
 Initials Date   Modification        
------------------------------------------------------------        
 TRK    Sep-2019  Created    
 CPK	Oct-2019  added event log logic
******/    
CREATE PROCEDURE [Workflow].[Workflow_Queue_Action_Comments]         
 (    
 @MODULE_ID INT,    
    @Workflow_Queue_Saved_Filter_Query_Id INT = NULL,    
    @Logged_In_User INT,    
    @Queue_id VARCHAR(MAX) = NULL,     ---  User selected from drop down Id 49,1,2,3              
    @Exception_Type VARCHAR(MAX) = NULL,    
    @Account_Number VARCHAR(500) = NULL,    
    @Client VARCHAR(500) = NULL,    
    @Site VARCHAR(500) = NULL,    
    @Country VARCHAR(500) = NULL,    
    @State VARCHAR(500) = NULL,    
    @City VARCHAR(500) = NULL,    
    @Commodity VARCHAR(500) = NULL,    
    @Invoice_ID VARCHAR(500) = NULL,    
    @Priority INT = 0,                 --- Yes              
    @Exception_Status INT = NULL,    
    @Start_Date_in_Queue DATETIME = NULL,    
    @End_Date_in_Queue DATETIME = NULL,    
    @Start_Date_in_CBMS DATETIME = NULL,    
    @End_Date_in_CBMS DATETIME = NULL,    
    @Data_Source INT = NULL,    
    @Vendor VARCHAR(100) = NULL,    
    @Vendor_Type VARCHAR(100) = NULL,    
    @Month DATETIME = NULL,    
    @Filename VARCHAR(100) = NULL,    
    @invoice_List VARCHAR(MAX) = NULL, ---- if new invoice needs to be added send those invoices     
    @User_Group_Id_List VARCHAR(MAX),    
    @Invoice_List_TVP Invoice_List READONLY,    
    @Comments  VARCHAR(MAX) = NULL         
 )          
AS          
BEGIN          
      -- SET NOCOUNT ON added to prevent extra result sets from         
    -- interfering with SELECT statements.         
    DECLARE @Proc_name VARCHAR(100) = 'Workflow_Queue_Action_Comments',    
            @Input_Params VARCHAR(1000),    
            @Error_Line INT,    
            @Error_Message VARCHAR(3000),    
            @Workflow_Queue_Search_Filter_Group_Id INT;    
    
    DECLARE @invoice_table TABLE    
    (    
        Invoice_id INT    
    );    
    
    SELECT @Input_Params    
        = '@User ID:' + CAST(@Logged_In_User AS VARCHAR) + '@Workflow_Queue_Saved_Filter_Query_id:'    
          + CAST(@Workflow_Queue_Saved_Filter_Query_Id AS VARCHAR) + '@invoice_List:' + ISNULL(@invoice_List, '');    
    SET NOCOUNT ON;    
    
 BEGIN TRY    
    
        --get the list of invoices    
    
        INSERT @invoice_table    
        EXEC [Workflow].[Get_List_Of_Invoices] @MODULE_ID,    
                                               @Workflow_Queue_Saved_Filter_Query_Id,    
                                               @Logged_In_User,    
                                               @Queue_id,    
                                               @Exception_Type,    
                                               @Account_Number,    
                                               @Client,    
                                               @Site,    
                                               @Country,    
                                               @State,     
                                               @City,           
											   @Commodity,     
                                               @Invoice_ID,    
                                               @Priority,    
                                               @Exception_Status,    
                                               NULL,    
                                               @Start_Date_in_Queue,    
                                               @End_Date_in_Queue,    
                                               @Start_Date_in_CBMS,    
                                               @End_Date_in_CBMS,    
                                               @Data_Source,    
                                               @Vendor,    
                                               @Vendor_Type,    
                                               @Month,    
                                               @Filename,    
                                               @invoice_List,    
                                               @User_Group_Id_List,    
                                               @Invoice_List_TVP;    
    
    
        
   INSERT INTO dbo.CU_INVOICE_COMMENT          
   (          
       CU_INVOICE_ID,          
       COMMENT_BY_ID,          
       COMMENT_DATE,          
       IMAGE_COMMENT          
   )          
           
   SELECT           
    AC.Invoice_id AS CU_INVOICE_ID          
   ,@Logged_In_User AS COMMENT_BY_ID          
   ,getdate() AS COMMENT_DATE          
   ,@Comments AS IMAGE_COMMENT          
   FROM @invoice_table AS AC     
   

   
   INSERT INTO dbo.cu_invoice_event          
   (          
      cu_invoice_id  
		,event_date  
		,event_by_id  
		,event_description         
   )       
   SELECT           
    AC.Invoice_id AS CU_INVOICE_ID          
   ,getdate() AS event_date          
   ,@Logged_In_User AS event_by_id         
   ,'Invoice Commented' AS event_description          
   FROM @invoice_table AS AC        

           
 END TRY    
    BEGIN CATCH    
        -- Entry made to the logging SP to capture the errors.        
        SELECT @Error_Line = error_line(),    
               @Error_Message = error_message();    
    
        INSERT INTO StoredProc_Error_Log    
        (    
            StoredProc_Name,    
            Error_Line,    
            Error_message,    
            Input_Params    
        )    
        VALUES    
        (@Proc_name, @Error_Line, @Error_Message, @Input_Params);    
    
        EXEC [dbo].[usp_RethrowError] @Error_Message;    
    END CATCH;    
END;       
  
GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_Action_Comments] TO [CBMSApplication]
GO
