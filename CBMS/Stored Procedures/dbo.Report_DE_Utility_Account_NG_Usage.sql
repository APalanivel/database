
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:

	dbo.Report_DE_Utility_Account_NG_Usage

DESCRIPTION:       
      
 INPUT PARAMETERS:      
 Name		DataType		Default			Description       
------------------------------------------------------------      

 OUTPUT PARAMETERS:
 Name		DataType		Default			Description
------------------------------------------------------------

  USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Report_DE_Utility_Account_NG_Usage

AUTHOR INITIALS:
 Initials	Name
------------------------------------------------------------      
 AKR		Ashok Kumar Raju

 MODIFICATIONS
 Initials	Date		Modification
------------------------------------------------------------
 AKR		2014-10-31	Cloned from lec_UtilityAccountNGUsageCommercial_2008    
 AKR		2014-11-27	Filtered data for USA
 KVK		12/03/2014	replace new line and carraige return with empty
 HG			2015-07-06	Modified the query to get the account details separately then join with CU tables to get the data as CHA join would duplicate the data if multiple meters are there for that account
 HG			2015-08-07	DDCR-1, modified the query to include canadian accounts
******/
CREATE PROCEDURE [dbo].[Report_DE_Utility_Account_NG_Usage]
AS 
BEGIN

      DECLARE
            @Start_Date DATE
           ,@End_Date DATE
           ,@NG_Commodity_Id INT
           ,@NG_Total_Usage_Bucket_Master_Id INT
           ,@NG_Mmbtu_Unit_Id INT;

      CREATE TABLE #Client_Hier_Account
            ( 
             Client_Hier_Id INT
            ,Site_Id INT
            ,Site_Name VARCHAR(200)
            ,Division_Id INT
            ,Division VARCHAR(200)
            ,Client_Id INT
            ,Client VARCHAR(200)
            ,Region VARCHAR(200)
            ,State_Name VARCHAR(200)
            ,Account_Id INT
            ,Account_Number VARCHAR(200)
            ,Vendor_Id INT
            ,Vendor VARCHAR(200)
            ,PRIMARY KEY CLUSTERED ( Client_Hier_Id, Account_Id ) );

      SELECT
            @NG_Commodity_Id = Commodity_Id
      FROM
            dbo.Commodity
      WHERE
            Commodity_Name = 'Natural Gas';

      SELECT
            @NG_Total_Usage_Bucket_Master_Id = bm.Bucket_Master_Id
      FROM
            dbo.Bucket_Master bm
            INNER JOIN dbo.Code bt
                  ON bt.Code_Id = bm.Bucket_Type_Cd
      WHERE
            bm.Commodity_Id = @NG_Commodity_Id
            AND bm.Bucket_Name = 'Total Usage'
            AND bt.Code_Value = 'Determinant';
            
      SELECT
            @NG_Mmbtu_Unit_Id = Entity_Id
      FROM
            dbo.ENTITY mmbtu
      WHERE
            mmbtu.ENTITY_NAME = 'mmbtu'
            AND mmbtu.ENTITY_DESCRIPTION = 'Unit for Gas';
		
      SET @Start_Date = '01-01-' + CAST(DATEPART(yy, GETDATE()) - 2 AS VARCHAR(4));
      SET @End_Date = '12-01-' + CAST(DATEPART(yy, GETDATE()) AS VARCHAR(4));

      INSERT      INTO #Client_Hier_Account
                  ( 
                   Client_Hier_Id
                  ,Account_Id
                  ,Account_Number
                  ,Vendor
                  ,Site_Name
                  ,Division
                  ,Client
                  ,Region
                  ,State_Name
                  ,Vendor_Id
                  ,Site_Id
                  ,Division_Id
                  ,Client_Id )
                  SELECT
                        cha.Client_Hier_Id
                       ,cha.Account_id
                       ,cha.Account_Number
                       ,cha.Account_Vendor_Name
                       ,ch.Site_Name
                       ,ch.Sitegroup_Name
                       ,ch.Client_Name
                       ,ch.Region_Name
                       ,ch.State_Name
                       ,cha.Account_Vendor_Id
                       ,ch.Site_Id
                       ,ch.Sitegroup_Id
                       ,ch.Client_Id
                  FROM
                        Core.Client_Hier_Account cha
                        INNER JOIN Core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        cha.Commodity_Id = @NG_Commodity_Id
                        AND cha.Account_Type = 'Utility'
                        AND ch.Site_Type_Name IN ( 'Industrial', 'Commercial' )
                        AND ch.Client_name != 'AT&T Services, Inc.'
                        AND ch.Site_Not_Managed = 0
                        AND cha.Account_Not_Managed = 0
                        AND ch.Country_Name IN ( 'USA', 'Canada')
                  GROUP BY
                        cha.Client_Hier_Id
                       ,cha.Account_id
                       ,cha.Account_Number
                       ,cha.Account_Vendor_Name
                       ,ch.Site_Name
                       ,ch.Sitegroup_Name
                       ,ch.Client_Name
                       ,ch.Region_Name
                       ,ch.State_Name
                       ,cha.Account_Vendor_Id
                       ,ch.Site_Id
                       ,ch.Sitegroup_Id
                       ,ch.Client_Id;
   
      SELECT
            cha.Account_Number AS Account
           ,cha.Account_Id
           ,cha.Vendor
           ,cha.Vendor_Id
           ,cha.Site_Name AS [Site]
           ,cha.Site_Id
           ,cha.Division
           ,cha.Division_Id
           ,cha.Client
           ,cha.Client_Id
           ,cha.Region
           ,cha.State_Name AS [State]
           ,( CAST(cu.Bucket_Value AS DECIMAL(32, 16)) * cuc.conversion_factor ) AS ConvertedUsage
           ,CONVERT(VARCHAR(3), DATENAME(MONTH, cu.service_month), 100) + '-' + RIGHT(DATEPART(yy, cu.SERVICE_MONTH), 2) AS service_month
      FROM
            #Client_Hier_Account cha
            LEFT OUTER JOIN dbo.Cost_Usage_Account_Dtl cu
                  ON cu.Account_Id = cha.Account_Id
                     AND cu.Client_Hier_Id = cha.Client_Hier_Id
                     AND cu.Bucket_Master_Id = @NG_Total_Usage_Bucket_Master_Id
                     AND cu.Service_Month BETWEEN @Start_Date
                                          AND     @End_Date
            LEFT OUTER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                  ON cuc.BASE_UNIT_ID = cu.UOM_Type_Id
                     AND cuc.CONVERTED_UNIT_ID = @NG_Mmbtu_Unit_Id;
    
      DROP TABLE #Client_Hier_Account;

END;
;
GO


GRANT EXECUTE ON  [dbo].[Report_DE_Utility_Account_NG_Usage] TO [CBMS_SSRS_Reports]
GO
