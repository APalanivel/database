SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Invoice_Collection_Exception_Type_User_Info_Map_Sel

DESCRIPTION:

INPUT PARAMETERS:
	Name							DataType		Default					Description
---------------------------------------------------------------------------------------------


OUTPUT PARAMETERS:
	Name							DataType		Default					Description
---------------------------------------------------------------------------------------------
	
USAGE EXAMPLES:
---------------------------------------------------------------------------------------------

exec Invoice_Collection_Exception_Type_User_Info_Map_Sel

AUTHOR INITIALS:

	Initials	Name
---------------------------------------------------------------------------------------------
	SP          Srinivas Patchava
	
MODIFICATIONS
	Initials	Date			Modification
---------------------------------------------------------------------------------------------
	SP       	2019-07-05		Created for  SE2017-729 Mapping of IC Exceptions by Type

******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Exception_Type_User_Info_Map_Sel]
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            uimap.Invoice_Collection_Exception_Type_User_Info_Map_Id
            , uimap.Invoice_Collection_Exception_Type_Cd
            , c.Code_Value AS Exception_Type
            , uimap.User_Info_Id AS Assigned_User_Id
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Assigned_User
            , uimap.Last_Change_Ts
            , uii.FIRST_NAME + ' ' + uii.LAST_NAME AS Last_Updated_By
        FROM
            Invoice_Collection_Exception_Type_User_Info_Map uimap
            INNER JOIN dbo.Code c
                ON c.Code_Id = uimap.Invoice_Collection_Exception_Type_Cd
            LEFT JOIN dbo.USER_INFO ui
                ON ui.USER_INFO_ID = uimap.User_Info_Id
            INNER JOIN dbo.USER_INFO uii
                ON uii.USER_INFO_ID = uimap.Updated_User_Id
        ORDER BY
            c.Display_Seq;


    END;

GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Exception_Type_User_Info_Map_Sel] TO [CBMSApplication]
GO
