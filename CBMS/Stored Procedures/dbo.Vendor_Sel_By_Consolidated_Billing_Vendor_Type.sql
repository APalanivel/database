
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.Vendor_Sel_By_Consolidated_Billing_Vendor_Type            
                        
 DESCRIPTION:                        
			To get the details of invoice collection Client level config                  
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
 @Contact_Info_Id					INT
,@Contact_Level_Cd					INT
,@Contact_Type_Cd					INT
,@Client_Id							INT
,@Vendor_Id							INT
,@Is_Active							INT
                              
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            


 
--Utility with nothing on consolidated vendor type
  EXEC [dbo].[Vendor_Sel_By_Consolidated_Billing_Vendor_Type] 
	   @Account_Id = 1269921
	   
--Supplier with nothing on consolidated vendor type
  EXEC [dbo].[Vendor_Sel_By_Consolidated_Billing_Vendor_Type] 
	   @Account_Id = 1342228

--Utility with Supplier on consolidated vendor type
  EXEC [dbo].[Vendor_Sel_By_Consolidated_Billing_Vendor_Type] 
	   @Account_Id = 1342214
                    
 --Utility with Supplier on consolidated vendor type
  EXEC [dbo].[Vendor_Sel_By_Consolidated_Billing_Vendor_Type] 
	   @Account_Id = 6045
	   ,@Invoice_Collection_Account_Config_End_Dt='2003-04-01'
	                         
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2016-12-19       Created for Invoice Tracking       
 SP					   2017-07-25		Hot Fix,MAINT-5575 removed consolidated billing check for supplier account.        
                       
******/  
CREATE PROCEDURE [dbo].[Vendor_Sel_By_Consolidated_Billing_Vendor_Type]
      ( 
       @Account_Id INT
      ,@Invoice_Collection_Account_Config_Id INT = NULL
      ,@Invoice_Collection_Account_Config_End_Dt DATE = NULL )
AS 
BEGIN                
      SET NOCOUNT ON;  
      DECLARE
            @Vendor_Type_Id INT
           ,@Vendor_Name VARCHAR(200)
           ,@Account_Type CHAR(8)
           ,@Is_Consolidated_billing BIT= 0
           ,@Consolidated_Billing_Vendor_Type VARCHAR(25)
           ,@Utility_Id INT
           ,@Supplier_Id INT
           ,@Account_Supplier_Vendor_Id INT


      SELECT
            @Utility_Id = entity_id
      FROM
            dbo.ENTITY E
      WHERE
            entity_description = 'Vendor'
            AND E.ENTITY_NAME = 'Utility'
      
      SELECT
            @Supplier_Id = entity_id
      FROM
            dbo.ENTITY E
      WHERE
            entity_description = 'Vendor'
            AND E.ENTITY_NAME = 'Supplier'



      
	  
      SELECT
            @Account_Type = cha.Account_Type
           ,@Vendor_Name = cha.Account_Vendor_Name
           ,@Vendor_Type_Id = CHA.Account_Vendor_Id
           ,@Consolidated_Billing_Vendor_Type = 'Utility'
      FROM
            core.Client_Hier_Account cha
            INNER JOIN core.Client_Hier ch
                  ON cha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.ACCOUNT a
                  ON cha.Account_Id = a.ACCOUNT_ID
      WHERE
            cha.Account_Id = @Account_Id
            
            
     
            
       
      --If there are no contacts for the config then treat vendor type as supplier 
      SELECT
            @Consolidated_Billing_Vendor_Type = 'Supplier'
      WHERE
            NOT EXISTS ( SELECT
                              1
                         FROM
                              dbo.Invoice_Collection_Account_Contact ica
                              INNER JOIN dbo.Invoice_Collection_Account_Config icac
                                    ON ica.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                         WHERE
                              icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id )
            
     
      --irrespective of timeperiod if any one is supplier then we enable vendor type dropdown in app   
      SELECT TOP 1
            @Consolidated_Billing_Vendor_Type = e.ENTITY_NAME
      FROM
            dbo.Account_Consolidated_Billing_Vendor asbv
            INNER JOIN dbo.ENTITY e
                  ON asbv.Invoice_Vendor_Type_Id = e.ENTITY_ID
      WHERE
            asbv.Account_Id = @Account_Id
            AND e.ENTITY_NAME = 'Supplier' 
            
    
     
    
     -- To highlight the vendor_name if the account is utility and conso vendor type as supplier.      
      SELECT TOP 1
            @Account_Supplier_Vendor_Id = cha1.Account_Vendor_Id
      FROM
            core.Client_Hier ch
            INNER JOIN core.Client_Hier_Account cha
                  ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN core.Client_Hier_Account cha1
                  ON cha.Meter_Id = cha1.Meter_Id
      WHERE
            cha.Account_Type = 'utility'
            AND @Account_Type = 'Utility'
            AND cha1.Account_Type = 'Supplier'
            AND cha.Account_Id = @Account_Id
            AND @Consolidated_Billing_Vendor_Type = 'Supplier'
            AND ( @Invoice_Collection_Account_Config_End_Dt IS NULL
                  OR @Invoice_Collection_Account_Config_End_Dt BETWEEN cha1.Supplier_Account_begin_Dt
                                                               AND     cha1.Supplier_Account_End_Dt )
            AND EXISTS ( SELECT
                              1
                         FROM
                              dbo.Vendor_Contact_Map vc
                         WHERE
                              vc.VENDOR_ID = cha1.Account_Vendor_Id )
      GROUP BY
            cha1.Account_Vendor_Id
             
     
      
      
      --If account has any record with consolidated billing then we will consider it as yes in IC account config vendor contacts      
      
      SELECT TOP 1
            @Account_Supplier_Vendor_Id = asbv.Supplier_Vendor_Id
      FROM
            dbo.Account_Consolidated_Billing_Vendor asbv
      WHERE
            asbv.Account_Id = @Account_Id
            AND @Account_Supplier_Vendor_Id IS NULL
            AND EXISTS ( SELECT
                              1
                         FROM
                              dbo.Vendor_Contact_Map vc
                         WHERE
                              vc.VENDOR_ID = asbv.Supplier_Vendor_Id )
      
      SELECT TOP 1
            @Is_Consolidated_billing = 1
      FROM
            dbo.Account_Consolidated_Billing_Vendor asbv
      WHERE
            asbv.Account_Id = @Account_Id
            
            
      DECLARE @Output TABLE
            ( 
             Vendor_Id INT
            ,Vendor_Name VARCHAR(200)
            ,Vendor_Type_Id INT
            ,Vendor_Type VARCHAR(25) )
            
    

      INSERT      INTO @Output
                  ( 
                   Vendor_Id
                  ,Vendor_Name
                  ,Vendor_Type_Id
                  ,Vendor_Type )
                  SELECT
                        @Vendor_Type_Id AS Vendor_Id
                       ,@Vendor_Name
                       ,@Utility_Id AS Vendor_Type_Id
                       ,@Account_Type AS Vendor_Type
                  WHERE
                        ISNULL(@Consolidated_Billing_Vendor_Type, 'Utility') = 'Utility'
                        AND @Account_Type = 'Utility'
    


      INSERT      INTO @Output
                  ( 
                   Vendor_Id
                  ,Vendor_Name
                  ,Vendor_Type_Id
                  ,Vendor_Type )
                  SELECT
                        v.VENDOR_ID
                       ,V.VENDOR_NAME
                       ,@Utility_Id AS Vendor_Type_Id
                       ,@Account_Type AS Vendor_Type
                  FROM
                        dbo.VENDOR v
                        INNER JOIN dbo.ENTITY e
                              ON e.ENTITY_ID = v.VENDOR_TYPE_ID
                  WHERE
                        ISNULL(@Consolidated_Billing_Vendor_Type, 'Utility') = 'Supplier'
                        AND @Account_Type = 'Utility'
                        AND ENTITY_DESCRIPTION = 'vendor'
                        AND ENTITY_NAME = 'supplier'
                        AND v.IS_HISTORY = 0
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.Vendor_Contact_Map vc
                                     WHERE
                                          vc.VENDOR_ID = v.VENDOR_ID )
		  
      

      INSERT      INTO @Output
                  ( 
                   Vendor_Id
                  ,Vendor_Name
                  ,Vendor_Type_Id
                  ,Vendor_Type )
                  SELECT
                        @Vendor_Type_Id AS Vendor_Id
                       ,@Vendor_Name
                       ,@Supplier_Id AS Vendor_Type_Id
                       ,@Account_Type AS Vendor_Type
                  WHERE
                        @Account_Type = 'Supplier'
               
   
      SELECT
            Vendor_Id
           ,Vendor_Name
           ,Vendor_Type_Id
           ,Vendor_Type
           ,ISNULL(@Consolidated_Billing_Vendor_Type, CASE WHEN @Account_Type = 'Utility' THEN 'Utility'
                                                           WHEN @Account_Type = 'Supplier' THEN 'Supplier'
                                                      END) AS Consolidated_Billing_Vendor_Type
           ,@Is_Consolidated_billing AS Is_Consolidated_billing
           ,@Account_Type AS Account_Type
           ,@Vendor_Name AS Account_Actual_Vendor_Name
           ,@Vendor_Type_Id AS Account_Actual_Vendor_Type_Id
           ,CASE WHEN Vendor_Id = @Account_Supplier_Vendor_Id THEN 1
                 ELSE 0
            END AS Is_Supplier_Vendor
      FROM
            @Output
      ORDER BY
            Vendor_Name
END;



;
GO

GRANT EXECUTE ON  [dbo].[Vendor_Sel_By_Consolidated_Billing_Vendor_Type] TO [CBMSApplication]
GO
