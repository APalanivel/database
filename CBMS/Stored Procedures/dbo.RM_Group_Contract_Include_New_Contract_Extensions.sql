SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        dbo.RM_Group_Contract_Include_New_Contract_Extensions                    
                      
Description:                      
        To add new contract to group if its base contract is in a group with add include extensions as true
                      
Input Parameters:                      
    Name				DataType        Default     Description                        
--------------------------------------------------------------------------------
	@Contract_Id		INT
    @User_Info_Id		INT
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------

                    
Usage Examples:                          
--------------------------------------------------------------------------------
	
	     
                     
 Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy        
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			2018-23-07     Global Risk Management - Created                    
                     
******/       
CREATE PROCEDURE [dbo].[RM_Group_Contract_Include_New_Contract_Extensions]
      ( 
       @Contract_Id INT
      ,@User_Info_Id INT )
AS 
BEGIN      
      SET NOCOUNT ON;
      
      INSERT      INTO dbo.Cbms_Sitegroup_Participant
                  ( 
                   Cbms_Sitegroup_Id
                  ,Group_Participant_Id
                  ,Group_Participant_Type_Cd
                  ,Include_New_Contract_Extensions
                  ,Created_User_Id
                  ,Created_Ts
                  ,Updated_User_Id
                  ,Last_Change_Ts )
                  SELECT
                        rmg.Cbms_Sitegroup_Id
                       ,@Contract_Id
                       ,dtl.Group_Participant_Type_Cd
                       ,0
                       ,@User_Info_Id
                       ,GETDATE()
                       ,@User_Info_Id
                       ,GETDATE()
                  FROM
                        dbo.Cbms_Sitegroup rmg
                        INNER JOIN dbo.Cbms_Sitegroup_Participant dtl
                              ON rmg.Cbms_Sitegroup_Id = dtl.Cbms_Sitegroup_Id
                        INNER JOIN dbo.Code cdgrp
                              ON rmg.Group_Type_Cd = cdgrp.Code_Id
                        INNER JOIN dbo.Codeset csgrp
                              ON csgrp.Codeset_Id = cdgrp.Codeset_Id
                        INNER JOIN dbo.Code cdprtp
                              ON dtl.Group_Participant_Type_Cd = cdprtp.Code_Id
                        INNER JOIN dbo.Codeset csprtp
                              ON cdprtp.Codeset_Id = cdprtp.Codeset_Id
                        INNER JOIN dbo.CONTRACT newcon
                              ON dtl.Group_Participant_Id = newcon.BASE_CONTRACT_ID
                  WHERE
                        csgrp.Codeset_Name = 'Risk Management Groups'
                        AND cdgrp.Code_Value = 'Contract'
                        AND csprtp.Codeset_Name = 'RM Group Participants'
                        AND cdgrp.Code_Value = 'Contract'
                        AND dtl.Include_New_Contract_Extensions = 1
                        AND newcon.CONTRACT_ID = @Contract_Id
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Cbms_Sitegroup_Participant old
                                         WHERE
                                          old.Cbms_Sitegroup_Id = dtl.Cbms_Sitegroup_Id
                                          AND old.Group_Participant_Type_Cd = dtl.Group_Participant_Type_Cd
                                          AND old.Group_Participant_Id = @Contract_Id )
                  GROUP BY
                        rmg.Cbms_Sitegroup_Id
                       ,dtl.Group_Participant_Type_Cd
      
      
           
END;
GO
GRANT EXECUTE ON  [dbo].[RM_Group_Contract_Include_New_Contract_Extensions] TO [CBMSApplication]
GO
