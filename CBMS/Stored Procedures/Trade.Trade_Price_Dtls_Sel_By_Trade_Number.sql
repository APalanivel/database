SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        [Trade].[Trade_Price_Dtls_Sel_By_Trade_Number]   
                        
Description:                        
          
                        
Input Parameters:                        
    Name			DataType        Default     Description                          
--------------------------------------------------------------------------------  
	@Trade_Number INT
                        
Output Parameters:                              
	Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
   
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
 
       Exec [Trade].[Trade_Price_Dtls_Sel_By_Trade_Number] 825,1,25
	   Exec [Trade].[Trade_Price_Dtls_Sel_By_Trade_Number] 1000
	   Exec [Trade].[Trade_Price_Dtls_Sel_By_Trade_Number] 1589
                       
 Author Initials:                        
    Initials    Name                        
--------------------------------------------------------------------------------  
    RR          Raghu Reddy          
                         
 Modifications:                        
    Initials Date           Modification                        
--------------------------------------------------------------------------------  
    RR   2019-03-22     Global Risk Management - Created                      
                       
******/
CREATE PROCEDURE [Trade].[Trade_Price_Dtls_Sel_By_Trade_Number]
    (
        @Trade_Number INT
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
    )
AS
    BEGIN
        SET NOCOUNT ON;
        WITH Cte_Trade
        AS (
               SELECT
                    REPLACE(CONVERT(VARCHAR(20), dtchvd.Deal_Month, 106), ' ', '-') AS Deal_Month
                    , ttp.Trade_Price
                    , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
                    , ROW_NUMBER() OVER (ORDER BY
                                             RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')') AS Row_Num
               FROM
                    Trade.Trade_Price ttp
                    INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtchvd
                        ON dtchvd.Trade_Price_Id = ttp.Trade_Price_Id
                    INNER JOIN Core.Client_Hier ch
                        ON ch.Client_Hier_Id = dtchvd.Client_Hier_Id
               WHERE
                    dtchvd.Trade_Number = @Trade_Number
               GROUP BY
                   dtchvd.Deal_Month
                   , ttp.Trade_Price
                   , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
           )
        SELECT
            ct.Deal_Month
            , ct.Trade_Price
            , ct.Site_name
            , ct.Row_Num
            , tot.Total
        FROM
            Cte_Trade ct
            CROSS JOIN
            (   SELECT
                    MAX(Row_Num) AS Total
                FROM
                    Cte_Trade) tot
        WHERE
            ct.Row_Num BETWEEN @Start_Index
                       AND     @End_Index
        ORDER BY
            ct.Row_Num;

    END;

GO
GRANT EXECUTE ON  [Trade].[Trade_Price_Dtls_Sel_By_Trade_Number] TO [CBMSApplication]
GO
