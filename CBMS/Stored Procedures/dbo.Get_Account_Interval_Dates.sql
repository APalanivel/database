
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                 
                 
NAME: Get_Account_Interval_dates    
    
DESCRIPTION:          
    
 To get cu service dates received for the given site account and date range    
    
INPUT PARAMETERS:    
NAME   DATATYPE DEFAULT  DESCRIPTION    
------------------------------------------------------------    
@Account_Id  INT    
@Site_Client_Hier_Id INT    
@Min_Dt   DATE    
@Max_Dt   DATE    
    
OUTPUT PARAMETERS:    
NAME     DATATYPE DEFAULT  DESCRIPTION    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
    
    EXEC dbo.Get_Account_Interval_Dates     
      @Site_Client_Hier_Id = 1130    
     ,@Account_Id = 452845    
     ,@Min_Dt = '2012-01-01'    
     ,@Max_Dt = '2012-01-31'    
      
    EXEC dbo.Get_Account_Interval_Dates 676558,12584,'2013-01-15','2013-01-25' 
    EXEC dbo.Get_Account_Interval_Dates 676557,12584,'2013-01-01','2013-01-20'
    
AUTHOR INITIALS:    
INITIALS	NAME    
------------------------------------------------------------    
PKY			Pavan K Yadalam  
RR			Raghu Reddy  
    
MODIFICATIONS    
INITIALS	DATE		MODIFICATION    
------------------------------------------------------------       
PKY			2013-03-14	Created for Calendarization requirement  
RR			2013-10-24	MAINT-2184 Logic to apply input dates is modified, old logic failed to return intersecting service periods 
						with input dates
    
*/    
CREATE PROCEDURE dbo.Get_Account_Interval_Dates
      ( 
       @Account_Id INT
      ,@Site_Client_Hier_Id INT
      ,@Min_Dt DATE
      ,@Max_Dt DATE )
AS 
BEGIN    
    
      SET NOCOUNT ON    
    
      SELECT
            bai.Account_Id
           ,bai.Bucket_Master_Id
           ,bai.Service_Start_Dt
           ,bai.Service_End_Dt
      FROM
            dbo.Bucket_Account_Interval_Dtl bai
            INNER JOIN dbo.Code c
                  ON c.Code_Id = bai.Data_Source_Cd
      WHERE
            bai.Client_Hier_Id = @Site_Client_Hier_Id
            AND ( bai.Service_Start_Dt BETWEEN @Min_Dt AND @Max_Dt
                  OR bai.Service_End_Dt BETWEEN @Min_Dt AND @Max_Dt
                  OR @Min_Dt BETWEEN bai.Service_Start_Dt
                             AND     bai.Service_End_Dt
                  OR @Max_Dt BETWEEN bai.Service_Start_Dt
                             AND     bai.Service_End_Dt )
            AND EXISTS ( SELECT
                              1
                         FROM
                              Core.Client_Hier_Account cha
                              INNER JOIN dbo.Bucket_Master bm
                                    ON bm.Commodity_Id = cha.Commodity_Id
                         WHERE
                              cha.Account_Id = @Account_Id
                              AND bai.Bucket_Master_Id = bm.Bucket_Master_Id )
            AND c.Code_Value = 'Invoice'    
    
END 

;
GO

GRANT EXECUTE ON  [dbo].[Get_Account_Interval_Dates] TO [CBMSApplication]
GO
