SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsUser_GetByUsername

DESCRIPTION:

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@username      	varchar(50)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	 EXEC dbo.cbmsUser_GetByUsername 'megan'
	 EXEC dbo.cbmsUser_GetByUsername 'dblock'
	 EXEC dbo.cbmsUser_GetByUsername 'vangundy'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			10/19/2010	Removed @MyAccountId param it was not used any where entire the code.
	PNR			11/01/2010	Selected last saved passcode value for PassCode column from user_passcode table instead user_info.
	PNR			11/15/2010	Removed Passcode column from select list because of not used this column in entire pwdreminder.aspx.vb page.
	
******/

CREATE PROCEDURE dbo.cbmsUser_GetByUsername
    (
      @username VARCHAR(30)
    )
AS
BEGIN

	SET NOCOUNT ON;

    SELECT
		 user_info_id
        ,username
        ,queue_id
        ,first_name
        ,middle_name
        ,last_name
        ,email_address
        ,is_history
        ,access_level
        ,client_id
        ,division_id
        ,site_id
        ,phone_number
        ,phone_number_ext
        ,cell_number
        ,fax_number
        ,cem_homepage
    FROM
		dbo.user_info
    WHERE
		USERNAME = @username

END
GO
GRANT EXECUTE ON  [dbo].[cbmsUser_GetByUsername] TO [CBMSApplication]
GO
