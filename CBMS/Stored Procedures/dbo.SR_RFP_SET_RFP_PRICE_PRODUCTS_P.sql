
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	dbo.SR_RFP_SET_RFP_PRICE_PRODUCTS_P

DESCRIPTION: 


INPUT PARAMETERS:    
    Name                DataType          Default     Description    
---------------------------------------------------------------------------------    
	@userId				varchar
	@sessionId			varchar
	@rfpId				int
	@productName		varchar(100)
	@productDescription varchar(4000)
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRANSACTION
		SELECT * FROM dbo.SR_RFP_PRICING_PRODUCT WHERE SR_RFP_ID = 13299 AND PRODUCT_NAME = 'Test GCS'
			EXEC dbo.SR_RFP_SET_RFP_PRICE_PRODUCTS_P 1,1,13299,'Test GCS','Test GCS',15
		SELECT * FROM dbo.SR_RFP_PRICING_PRODUCT WHERE SR_RFP_ID = 13299 AND PRODUCT_NAME = 'Test GCS'
	ROLLBACK TRANSACTION

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DR			Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	DR			08/04/2009	Removed Linked Server Updates
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2016-03-28	Global Sourcing - Phase3 - GCS-564 Added input parameter @Sr_Service_Condition_Template_Id

******/
CREATE PROCEDURE [dbo].[SR_RFP_SET_RFP_PRICE_PRODUCTS_P]
      ( 
       @userId VARCHAR
      ,@sessionId VARCHAR
      ,@rfpId INT
      ,@productName VARCHAR(100)
      ,@productDescription VARCHAR(4000)
      ,@Sr_Service_Condition_Template_Id INT = NULL )
AS 
BEGIN
      SET NOCOUNT ON


      INSERT      INTO dbo.SR_RFP_PRICING_PRODUCT
                  ( 
                   SR_RFP_ID
                  ,PRODUCT_NAME
                  ,PRODUCT_DESCRIPTION
                  ,Sr_Service_Condition_Template_Id )
      VALUES
                  ( 
                   @rfpId
                  ,@productName
                  ,@productDescription
                  ,@Sr_Service_Condition_Template_Id )

END
;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_SET_RFP_PRICE_PRODUCTS_P] TO [CBMSApplication]
GO
