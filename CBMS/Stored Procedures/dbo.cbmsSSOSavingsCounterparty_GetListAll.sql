SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[cbmsSSOSavingsCounterparty_GetListAll]

	( @MyAccountId int

	)
AS
BEGIN

	   select 
		rm_counterparty_id
		, counterparty_name
	     from rm_counterparty


END
GO
GRANT EXECUTE ON  [dbo].[cbmsSSOSavingsCounterparty_GetListAll] TO [CBMSApplication]
GO
