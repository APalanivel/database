SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_STATES_UNDER_CLIENT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@clientId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE dbo.SR_SAD_GET_STATES_UNDER_CLIENT_P 
@clientId int

as
set nocount on
select	distinct st.state_id,
	st.state_name 

from	address ad, 
	division  d , 
	client c, 
	site s , 
	state st

where	s.division_id = d.division_id
	and ad.address_id = s.primary_address_id
	and st.state_id = ad.state_id 
	and d.client_id = c.client_id	
	and c.client_id = @clientId
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_STATES_UNDER_CLIENT_P] TO [CBMSApplication]
GO
