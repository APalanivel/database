SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.Cost_Usage_Sel_Supplier_Account_For_Utility_Account_By_Service_Month
DESCRIPTION:

	This procedure used to fetch Supplier account for the given utility account filtered by cost_usage service month.

INPUT PARAMETERS:
Name			DataType		Default		Description
------------------------------------------------------------
@account_id		INT							Utility Account
@Begin_Dt		DATE
@End_Dt			DATE

OUTPUT PARAMETERS:
Name      DataType		Default		Description
------------------------------------------------------------  

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC Cost_Usage_Sel_Supplier_Account_For_Utility_Account_By_Service_Month 158059,'01/01/2010','1/01/2010'
	EXEC Cost_Usage_Sel_Supplier_Account_For_Utility_Account_By_Service_Month 158110,'02/01/2010','2/01/2010'
	EXEC Cost_Usage_Sel_Supplier_Account_For_Utility_Account_By_Service_Month 158091,'2/1/2010','2/1/2010'

AUTHOR INITIALS:
Initials	Name  
------------------------------------------------------------  
SSR			Sharad Srivastava
HG			Harihara Suthan G

MODIFICATIONS   
Initials	Date		Modification  
------------------------------------------------------------  
SSR			02/08/2010	Created
HG			04/07/2010	Supplier_Account_Meter_Map table join removed as we are not pulling any data from it.
HG			04/15/2010	Procedure renamed from Cost_Usage_Supplier_Account_Sel_By_Account_Service_Month

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.Cost_Usage_Sel_Supplier_Account_For_Utility_Account_By_Service_Month
(
 	 @account_id	INT
	,@Begin_Dt		DATE
	,@End_Dt		DATE
 )
AS

BEGIN

	SET NOCOUNT ON

	SELECT
		samm.Account_id
		,uacc.Site_id
	FROM
		dbo.Account uacc
		JOIN dbo.METER um
			ON um.ACCOUNT_ID = uacc.ACCOUNT_ID
		JOIN dbo.Cost_Usage_Account_Dtl cuad
			ON cuad.account_id = um.account_id
		JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
			ON samm.METER_ID = um.METER_ID
	WHERE
		uacc.account_id = @account_id
		AND cuad.service_month BETWEEN @Begin_Dt AND @End_Dt
	GROUP BY
		samm.Account_id
		,uacc.Site_id
	
END
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Sel_Supplier_Account_For_Utility_Account_By_Service_Month] TO [CBMSApplication]
GO
