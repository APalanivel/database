
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
      
NAME: [DBO].[Cost_Usage_History_Del_For_Account]        
           
DESCRIPTION:       
      
 To Delete Cost/Usage history associated with the given Account Id.      
            
INPUT PARAMETERS:                
NAME   DATATYPE DEFAULT  DESCRIPTION                
------------------------------------------------------------                
@Account_Id  INT            
                      
OUTPUT PARAMETERS:      
NAME   DATATYPE DEFAULT  DESCRIPTION      
      
------------------------------------------------------------      
USAGE EXAMPLES:      
------------------------------------------------------------      
      
 BEGIN TRAN  
  EXEC Cost_Usage_History_Del_For_Account  263445  --- 346646      
 ROLLBACK TRAN  
      
AUTHOR INITIALS:  
INITIALS NAME  
------------------------------------------------------------  
HG   Harihara Suthan G  
PNR   PANDARINATH  
RR   Raghu Reddy  
AP   Athmaram Pabbathi  
RKV   Ravi Kumar Vegesna  
  
MODIFICATIONS  
INITIALS   DATE   MODIFICATION  
------------------------------------------------------------  
HG   06/08/2010  Created      
PNR   06/29/2010  Added logic to delete Comment Id's from Cost_Usage_Comment_Map and Comment tables      
       as backed out(Wrong account) invoices will have entries in Cost_usage_comment_map and these accounts cost/usage should be deleted along with account delete.      
RR   2011-12-30  Deleted the condition SYSTEM_ROW = 1 as data can be entered from ude screens which is not invoice data      
RR   2012-01-02  Script added to delete data from dbo.Cost_Usage_Account_Dtl table also      
AP   2012-03-19  Following Changes are made for Addl Data      
      -- Removed the references of Cost_Usage, Cost_Usage_Comment_Map tables      
      -- Replaced dbo.Cost_Usage_Account_Dtl_Del_By_Cost_Usage_Account_Dtl_Id SP call with dbo.Cost_Usage_Account_Dtl_Del_By_Client_Hier_Account_Bucket_Service_Month      
      -- Used Temp tables with clustered indexes      
RR   2012-06-13  Replaced while exists(with select and deleting processed row) for looping through each record in #Cost_Usage_Account_Dtl_List       
       with row number column, generated and saved into local variable, iterating the loop till count reaches variable value      
       
RKV   2013-04-22  Added Logic to Delete Bucket_Account_Interval_Dtl history associated with the given Account Id  
  
*/  
CREATE PROCEDURE dbo.Cost_Usage_History_Del_For_Account @Account_Id INT
AS 
BEGIN  
      SET NOCOUNT ON ;  
  
      CREATE TABLE #Cost_Usage_Account_Dtl_List
            ( 
             Client_Hier_Id INT
            ,Account_Id INT
            ,Bucket_Master_Id INT
            ,Service_Month DATE
            ,Row_Num INT IDENTITY(1, 1) )                
  
      CREATE TABLE #Bucket_Account_Interval_Dtl_List
            ( 
             Client_Hier_Id INT
            ,Account_Id INT
            ,Bucket_Master_Id INT
            ,Service_Start_Dt DATE
            ,Service_End_Dt DATE
            ,Data_Source_Cd INT
            ,Row_Num INT IDENTITY(1, 1) )       
  
      DECLARE
            @Client_Hier_Id INT
           ,@Service_Month DATE
           ,@Service_Start_Dt DATE
           ,@Service_End_Dt DATE
           ,@Data_Source_Cd INT
           ,@Bucket_Master_Id INT
           ,@Total_Row_Count INT
           ,@Row_Counter INT 
           
      DECLARE @Ch_Acc TABLE
            ( 
             Client_Hier_Id INT
            ,Account_Id INT )
            
      INSERT      INTO @Ch_Acc
                  ( 
                   Client_Hier_Id
                  ,Account_Id )
                  SELECT
                        cha.Client_Hier_Id
                       ,cha.Account_Id
                  FROM
                        core.Client_Hier_Account cha
                  WHERE
                        cha.Account_Id = @Account_Id
                  GROUP BY
                        cha.Client_Hier_Id
                       ,cha.Account_Id
  
      INSERT      INTO #Cost_Usage_Account_Dtl_List
                  ( 
                   Client_Hier_Id
                  ,Account_Id
                  ,Bucket_Master_Id
                  ,Service_Month )
                  SELECT
                        cuad.Client_Hier_Id
                       ,cuad.Account_ID
                       ,cuad.Bucket_Master_Id
                       ,cuad.Service_Month
                  FROM
                        dbo.Cost_Usage_Account_Dtl cuad
                  WHERE
                        cuad.Account_Id = @Account_Id      
                           
      INSERT      INTO #Bucket_Account_Interval_Dtl_List
                  ( 
                   Client_Hier_Id
                  ,Account_Id
                  ,Bucket_Master_Id
                  ,Service_Start_Dt
                  ,Service_End_Dt
                  ,Data_Source_Cd )
                  SELECT
                        baid.Client_Hier_Id
                       ,baid.Account_Id
                       ,baid.Bucket_Master_Id
                       ,baid.Service_Start_Dt
                       ,baid.Service_End_Dt
                       ,baid.Data_Source_Cd
                  FROM
                        dbo.Bucket_Account_Interval_Dtl baid
                        JOIN @Ch_Acc cha
                              ON baid.Account_Id = cha.Account_Id
                                 AND baid.Client_Hier_Id = cha.Client_Hier_Id
                    
      BEGIN TRY  
   --Loop for deleting records from Cost_Usage_Account_Dtl table                         
            SELECT
                  @Total_Row_Count = max(Row_Num)
            FROM
                  #Cost_Usage_Account_Dtl_List      
       
            SET @Row_Counter = 1       
            WHILE ( @Row_Counter <= @Total_Row_Count ) 
                  BEGIN      
      
                        SELECT
                              @Client_Hier_Id = cuad.Client_Hier_Id
                             ,@Bucket_Master_Id = cuad.Bucket_Master_Id
                             ,@Service_Month = cuad.Service_Month
                        FROM
                              #Cost_Usage_Account_Dtl_List cuad
                        WHERE
                              cuad.Row_Num = @Row_Counter      
      
                        EXECUTE dbo.Cost_Usage_Account_Dtl_DEL_By_Client_Hier_Account_Bucket_Service_Month 
                              @Client_Hier_Id = @Client_Hier_Id
                             ,@Account_Id = @Account_Id
                             ,@Bucket_Master_Id = @Bucket_Master_Id
                             ,@Service_Month = @Service_Month      
      
                        SET @Row_Counter = @Row_Counter + 1      
                  END    
                      
           --Loop for deleting records from Bucket_Account_Interval_Dtl table      
            SELECT
                  @Total_Row_Count = max(Row_Num)
            FROM
                  #Bucket_Account_Interval_Dtl_List      
            SET @Row_Counter = 1       
                  
            WHILE ( @Row_Counter <= @Total_Row_Count ) 
                  BEGIN      
      
                        SELECT
                              @Client_Hier_Id = baid.Client_Hier_Id
                             ,@Account_Id = baid.Account_Id
                             ,@Bucket_Master_Id = baid.Bucket_Master_Id
                             ,@Service_Start_Dt = baid.Service_Start_Dt
                             ,@Service_End_Dt = baid.Service_End_Dt
                             ,@Data_Source_Cd = baid.Data_Source_Cd
                        FROM
                              #Bucket_Account_Interval_Dtl_List baid
                        WHERE
                              Row_Num = @Row_Counter       
                                  
                        EXECUTE dbo.Bucket_Account_Interval_Dtl_Del 
                              @Client_Hier_Id = @Client_Hier_Id
                             ,@Account_Id = @Account_Id
                             ,@Bucket_Master_Id = @Bucket_Master_Id
                             ,@Service_Start_Dt = @Service_Start_Dt
                             ,@Service_End_Dt = @Service_End_Dt
                             ,@Data_Source_Cd = @Data_Source_Cd    
                                   
                        SET @Row_Counter = @Row_Counter + 1      
                  END  
   
      END TRY      
  
      BEGIN CATCH      
            EXEC dbo.usp_RethrowError      
      END CATCH      
  
      DROP TABLE #Cost_Usage_Account_Dtl_List    
      DROP TABLE #Bucket_Account_Interval_Dtl_List      
  
END ;  
  
;
GO



GRANT EXECUTE ON  [dbo].[Cost_Usage_History_Del_For_Account] TO [CBMSApplication]
GO
