SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
   
 dbo.Report_DE_Sales_Utility_ZipCode
   
DESCRIPTION:  
	This report is to get Utility data for different commodities for the Rebate Team in Phoenix
  
INPUT PARAMETERS:  
Name			DataType  Default Description  
-------------------------------------------------------------------------------------------  
@comid	INT
   
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
-------------------------------------------------------------  
  
EXEC REPORT_DE_SALES_UTILITY_ZIPCODE '67'
exec Report_DE_Sales_Utility_ZipCode '291'

   
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
LEC Lynn Cox

MODIFICATIONS	
Initials	Date		Modification  
------------------------------------------------------------  
LEC			5/9/2016	Created for Ian
*/
CREATE PROCEDURE [dbo].[Report_DE_Sales_Utility_ZipCode] @comid INT
AS
BEGIN

      SET NOCOUNT ON;

      WITH  details_cte
              AS ( SELECT
                        LEFT(cha.Meter_ZipCode, 5) AS Meter_Zipcode
                       ,cha.Meter_Country_Name
                       ,cha.Meter_State_Name
                       ,cha.Account_Vendor_Name
                       ,COUNT(cha.Account_Vendor_Name) AS VendorRecords
                   FROM
                        CBMS.Core.Client_Hier_Account cha
                   WHERE
                        cha.Commodity_Id = @comid
                        AND cha.Account_Type = 'Utility'
                        AND ( cha.Meter_Country_Id = 4
                              OR cha.Meter_Country_Id = 1 )
                        AND cha.Account_Vendor_Name NOT LIKE 'Landlord%'
                   GROUP BY
                        cha.Meter_ZipCode
                       ,Account_Vendor_Name
                       ,cha.Meter_State_Name
                       ,Meter_Country_Name),
            maxdata_cte
              AS ( SELECT
                        MAX(dc.VendorRecords) maxvr
                       ,dc.Meter_Zipcode
                       ,dc.Meter_State_Name
                   FROM
                        details_cte dc
                   GROUP BY
                        dc.Meter_Zipcode
                       ,dc.Meter_State_Name)
            SELECT
                  dc.Meter_Zipcode AS ZipCode
                 ,dc.Meter_State_Name AS State_Province
                 ,dc.Meter_Country_Name AS Country
                 ,COUNT(dc2.Account_Vendor_Name) AS [Utilities]
                 ,dc.Account_Vendor_Name AS [Primary Utility]
                 ,'(' + CAST(dc.VendorRecords AS VARCHAR(5)) + ' of ' + CAST(SUM(dc2.VendorRecords) AS VARCHAR(5)) + ')' AS [Primary Match Count]
                 ,ROUND(CAST(dc.VendorRecords AS DECIMAL(6, 2)) / CAST(SUM(dc2.VendorRecords) AS DECIMAL(6, 2)), 2) AS [Primary Match %]
            FROM
                  maxdata_cte mx
                  INNER JOIN details_cte dc
                        ON dc.Meter_Zipcode = mx.Meter_Zipcode
                           AND dc.Meter_State_Name = mx.Meter_State_Name
                           AND dc.VendorRecords = mx.maxvr
                  INNER JOIN details_cte dc2
                        ON dc2.Meter_Zipcode = mx.Meter_Zipcode
                           AND mx.Meter_State_Name = dc2.Meter_State_Name
            GROUP BY
                  dc.Meter_Zipcode
                 ,dc.Meter_State_Name
                 ,dc.Account_Vendor_Name
                 ,dc.VendorRecords
                 ,dc.Meter_Country_Name
            ORDER BY
                  dc.Meter_State_Name
                 ,dc.Meter_Zipcode;
END;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Sales_Utility_ZipCode] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_Sales_Utility_ZipCode] TO [CBMSApplication]
GO
