SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	dbo.Sr_Rfp_Set_Supplier_Bid_Requirements_Price_Comments

DESCRIPTION: 


INPUT PARAMETERS:    
    Name		DataType          Default     Description    
---------------------------------------------------------------------------------    
	@Sr_Rfp_Id	INT
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRANSACTION
		SELECT
            count(tpm.SR_RFP_BID_ID)
        FROM
            dbo.SR_RFP_TERM_PRODUCT_MAP tpm
            INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                  ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
            INNER JOIN dbo.SR_RFP_ACCOUNT acc
                  ON term.SR_ACCOUNT_GROUP_ID = acc.SR_RFP_ACCOUNT_ID
        WHERE
            acc.SR_RFP_ID = 12236
            AND term.IS_BID_GROUP = 0
            AND tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID IS NOT NULL			  
		EXEC dbo.Sr_Rfp_Set_Supplier_Bid_Requirements_Price_Comments 12236,49
		SELECT
            count(tpm.SR_RFP_BID_ID)
        FROM
            dbo.SR_RFP_TERM_PRODUCT_MAP tpm
            INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                  ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
            INNER JOIN dbo.SR_RFP_ACCOUNT acc
                  ON term.SR_ACCOUNT_GROUP_ID = acc.SR_RFP_ACCOUNT_ID
        WHERE
            acc.SR_RFP_ID = 12236
            AND term.IS_BID_GROUP = 0
            AND tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID IS NOT NULL
	ROLLBACK TRANSACTION

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-04-14	Global Sourcing - Phase3 - Manage bid details - Created

******/
CREATE PROCEDURE [dbo].[Sr_Rfp_Set_Supplier_Bid_Requirements_Price_Comments] ( @Sr_Rfp_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;

      BEGIN 
            DECLARE
                  @accountTermsId INT
                 ,@supplierContactId INT
                 ,@supplierBOCId INT
                 ,@selectedProductsId INT
                 ,@STATUS INT
                 ,@isDataFound INT = 0
                 ,@Bid_Requirements_Id INT
                 ,@Supplier_Price_Comments_Id INT
                 ,@Sr_Rfp_Bid_Id INT
                 ,@newProductTermMapId INT
                 
            SELECT
                  @STATUS = ENTITY_ID
            FROM
                  dbo.ENTITY
            WHERE
                  ENTITY_TYPE = 1013
                  AND ENTITY_NAME = 'Post'

            DECLARE SET_BIDS CURSOR FAST_FORWARD
            FOR
            SELECT
                  map.sr_rfp_account_term_id
                 ,supplier.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                 ,map.SR_RFP_BID_ID
                 ,map.SR_RFP_SELECTED_PRODUCTS_ID
            FROM
                  dbo.SR_RFP_SEND_SUPPLIER supplier
                  INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                        ON supplier.SR_ACCOUNT_GROUP_ID = term.SR_ACCOUNT_GROUP_ID
                           AND supplier.IS_BID_GROUP = term.IS_BID_GROUP
                  INNER JOIN dbo.SR_RFP_TERM_PRODUCT_MAP map
                        ON term.SR_RFP_ACCOUNT_TERM_ID = map.SR_RFP_ACCOUNT_TERM_ID
                  INNER JOIN dbo.SR_RFP_ACCOUNT account
                        ON account.SR_RFP_ACCOUNT_ID = term.SR_ACCOUNT_GROUP_ID
            WHERE
                  term.IS_BID_GROUP = 0
                  AND term.IS_SOP IS NULL
                  AND map.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID IS NULL
                  AND account.SR_RFP_BID_GROUP_ID IS NULL
                  AND account.SR_RFP_ID = @Sr_Rfp_Id
                  AND account.IS_DELETED = 0
                  AND supplier.STATUS_TYPE_ID = @STATUS
            UNION
            SELECT
                  map.sr_rfp_account_term_id
                 ,supplier.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                 ,map.SR_RFP_BID_ID
                 ,map.SR_RFP_SELECTED_PRODUCTS_ID
            FROM
                  dbo.SR_RFP_SEND_SUPPLIER supplier
                  INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                        ON supplier.SR_ACCOUNT_GROUP_ID = term.SR_ACCOUNT_GROUP_ID
                           AND supplier.IS_BID_GROUP = term.IS_BID_GROUP
                  INNER JOIN dbo.SR_RFP_TERM_PRODUCT_MAP map
                        ON term.SR_RFP_ACCOUNT_TERM_ID = map.SR_RFP_ACCOUNT_TERM_ID
                  INNER JOIN dbo.SR_RFP_ACCOUNT account
                        ON account.SR_RFP_BID_GROUP_ID = term.SR_ACCOUNT_GROUP_ID
            WHERE
                  term.IS_BID_GROUP = 1
                  AND term.IS_SOP IS NULL
                  AND map.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID IS NULL
                  AND account.SR_RFP_ID = @Sr_Rfp_Id
                  AND account.IS_DELETED = 0
                  AND supplier.STATUS_TYPE_ID = @STATUS


            OPEN SET_BIDS
            FETCH NEXT FROM SET_BIDS INTO @accountTermsId, @supplierContactId, @supplierBOCId, @selectedProductsId
            WHILE ( @@fetch_status <> -1 ) 
                  BEGIN
                        SELECT
                              @isDataFound = ( SELECT
                                                count(1)
                                               FROM
                                                SR_RFP_TERM_PRODUCT_MAP map
                                               WHERE
                                                map.sr_rfp_account_term_id = @accountTermsId
                                                AND map.sr_rfp_supplier_contact_vendor_map_id = @supplierContactId
                                                AND map.sr_rfp_selected_products_id = @selectedProductsId )

                        IF ( @@fetch_status <> -2
                             AND @isDataFound = 0 ) 
                              BEGIN
			

                                    INSERT      INTO dbo.SR_RFP_BID_REQUIREMENTS
                                                ( 
                                                 DELIVERY_POINT
                                                ,TRANSPORTATION_LEVEL_TYPE_ID
                                                ,NOMINATION_TYPE_ID
                                                ,BALANCING_TYPE_ID
                                                ,COMMENTS
                                                ,DELIVERY_POINT_POWER_TYPE_ID
                                                ,SERVICE_LEVEL_POWER_TYPE_ID )
                                                SELECT
                                                      DELIVERY_POINT
                                                     ,TRANSPORTATION_LEVEL_TYPE_ID
                                                     ,NOMINATION_TYPE_ID
                                                     ,BALANCING_TYPE_ID
                                                     ,COMMENTS
                                                     ,DELIVERY_POINT_POWER_TYPE_ID
                                                     ,SERVICE_LEVEL_POWER_TYPE_ID
                                                FROM
                                                      dbo.SR_RFP_BID_REQUIREMENTS srbr
                                                      INNER JOIN dbo.SR_RFP_BID srb
                                                            ON srbr.SR_RFP_BID_REQUIREMENTS_ID = srb.SR_RFP_BID_REQUIREMENTS_ID
                                                WHERE
                                                      srb.SR_RFP_BID_ID = @supplierBOCId
				
                                    SELECT
                                          @Bid_Requirements_Id = scope_identity()


                                    INSERT      INTO dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS
                                                ( 
                                                 IS_CREDIT_APPROVAL
                                                ,NO_CREDIT_COMMENTS
                                                ,PRICE_RESPONSE_COMMENTS )
                                    VALUES
                                                ( 
                                                 NULL
                                                ,NULL
                                                ,NULL )
			
                                    SELECT
                                          @Supplier_Price_Comments_Id = scope_identity()

                                    INSERT      INTO dbo.SR_RFP_BID
                                                ( 
                                                 PRODUCT_NAME
                                                ,SR_RFP_BID_REQUIREMENTS_ID
                                                ,SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
                                                ,SR_RFP_SUPPLIER_SERVICE_ID
                                                ,IS_SUB_PRODUCT )
                                    VALUES
                                                ( 
                                                 NULL
                                                ,@Bid_Requirements_Id
                                                ,@Supplier_Price_Comments_Id
                                                ,NULL
                                                ,0 )

                                    SELECT
                                          @Sr_Rfp_Bid_Id = scope_identity()
		
                                    INSERT      INTO dbo.SR_RFP_TERM_PRODUCT_MAP
                                                ( 
                                                 SR_RFP_ACCOUNT_TERM_ID
                                                ,SR_RFP_SELECTED_PRODUCTS_ID
                                                ,SR_RFP_BID_ID
                                                ,SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                                ,BID_DATE )
                                    VALUES
                                                ( 
                                                 @accountTermsId
                                                ,@selectedProductsId
                                                ,@Sr_Rfp_Bid_Id
                                                ,@supplierContactId
                                                ,NULL )
                                    SELECT
                                          @newProductTermMapId = scope_identity()
                                          
                              END
                        FETCH NEXT FROM SET_BIDS INTO @accountTermsId, @supplierContactId, @supplierBOCId, @selectedProductsId
                  END
            CLOSE SET_BIDS
            DEALLOCATE SET_BIDS
      END
      
END 
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Set_Supplier_Bid_Requirements_Price_Comments] TO [CBMSApplication]
GO
