
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME: dbo.GET_UPDATE_CLIENT_P  
  
  
DESCRIPTION:  
  
  
INPUT PARAMETERS:      
Name                             DataType          Default     Description      
---------------------------------------------------------------------------------      
@clientTypeId					int  
@UBMServiceId					int  
@reportFrequencyTypeId			int  
@clientName						varchar(200)  
@fiscalYearStartMonthTypeId		int  
@UBMStartDate					datetime  
@DSMStrategy					bit  
@isSEPIssued					bit  
@SEPIssuedDate					datetime  
@clientId						int  
@not_managed					bit  
@not_managed_id					int  
@not_managed_date				datetime  
@riskProfileTypeId				int  
@Analyst_Mapping_Cd				INT   
@Is_Analyst_Mapping_Cd_Changed	BIT  
                            
                             
OUTPUT PARAMETERS:           
      Name              DataType          Default     Description      
------------------------------------------------------------      
  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
BEGIN TRANSACTION 
DECLARE @UBMStartDate1 DATETIME=convert( VARCHAR,getdate(),109)
	SELECT CLIENT_NAME,UBM_START_DATE,Variance_Exception_Notice_Contact_Email FROM dbo.CLIENT WHERE CLIENT_ID=102 
	exec dbo.GET_UPDATE_CLIENT_P  
		@clientTypeId =142,  
		@UBMServiceId =23,  
		@reportFrequencyTypeId =144,  
		@clientName ='Brooksville, KY (City of )',  -- Original value = 'Brooksville, KY (City of )'  
		@fiscalYearStartMonthTypeId =296,  
		@UBMStartDate = @UBMStartDate1,  
		@DSMStrategy =0,  
		@isSEPIssued =NULL,  
		@SEPIssuedDate =NULL,  
		@clientId = 102,  
		@not_managed =0,  
		@not_managed_id =1111,  
		@not_managed_date = NULL,  
		@riskProfileTypeId = 1491,  
		@Analyst_Mapping_Cd = 100503,   
		@Is_Analyst_Mapping_Cd_Changed = 1,
		@Variance_Exception_Notice_Contact_Email='Jessica.Kipper@ems.schneider-electric.com' 
	SELECT CLIENT_NAME,UBM_START_DATE,Variance_Exception_Notice_Contact_Email FROM dbo.CLIENT WHERE CLIENT_ID=102	
ROLLBACK TRANSACTION
  
  
AUTHOR INITIALS:  
Initials	Name  
------------------------------------------------------------  
DR			Deana Ritter   
CH			Chad Hattabaugh  
AKR         Ashok Kumar Raju
RR			Raghu Reddy  

MODIFICATIONS  
Initials	Date		Modification  
------------------------------------------------------------  
DR			06/02/2009	Modified for SV replciation implementation  
CH			06/22/2009	Modified for Sitegroup divsion table split  
DMR			09/10/2010  Modified for Quoted_Identifier  
AKR			2012-09-21  Added the code to Update Analust Mapping code at Site and Account level  
AKR			2012-10-09  Removed the If Else Condition as we have smae code in both the IF and else
RR			2014-06-25	MAINT-2824 Added new column Variance_Exception_Notice_Contact_Email to client table
******/  
CREATE PROCEDURE dbo.GET_UPDATE_CLIENT_P
      ( 
       @clientTypeId INT
      ,@UBMServiceId INT
      ,@reportFrequencyTypeId INT
      ,@clientName VARCHAR(200)
      ,@fiscalYearStartMonthTypeId INT
      ,@UBMStartDate DATETIME
      ,@DSMStrategy BIT
      ,@isSEPIssued BIT
      ,@SEPIssuedDate DATETIME
      ,@clientId INT
      ,@not_managed BIT
      ,@not_managed_id INT
      ,@not_managed_date DATETIME
      ,@riskProfileTypeId INT
      ,@Analyst_Mapping_Cd INT
      ,@Is_Analyst_Mapping_Cd_Changed BIT
      ,@Variance_Exception_Notice_Contact_Email VARCHAR(MAX) )
AS 
BEGIN   
      SET nocount ON  
  
      EXEC cbmsClient_UpdateParticipation 
            93
           ,@clientid
           ,@not_managed   
  
      SET nocount OFF  
  
      SET @not_managed_date = NULL  
  
      IF @not_managed = 1 
            BEGIN  
                  SET @not_managed_date = getdate()  
            END  
  
      UPDATE
            CLIENT
      SET   
            CLIENT_TYPE_ID = @clientTypeId
           ,UBM_SERVICE_ID = @UBMServiceId
           ,REPORT_FREQUENCY_TYPE_ID = @reportFrequencyTypeId
           ,CLIENT_NAME = @clientName
           ,FISCALYEAR_STARTMONTH_TYPE_ID = @fiscalYearStartMonthTypeId
           ,UBM_START_DATE = @UBMStartDate
           ,DSM_STRATEGY = @DSMStrategy
           ,IS_SEP_ISSUED = @isSEPIssued
           ,SEP_ISSUE_DATE = @SEPIssuedDate
           ,NOT_MANAGED = @not_managed
           ,NOT_MANAGED_BY_ID = @not_managed_id
           ,RISK_PROFILE_TYPE_ID = @riskProfileTypeId
           ,Analyst_Mapping_Cd = @Analyst_Mapping_Cd
           ,Variance_Exception_Notice_Contact_Email = @Variance_Exception_Notice_Contact_Email
      WHERE
            client_ID = @clientId  
    
              
  
  
      UPDATE
            s
      SET   
            s.Analyst_Mapping_Cd = NULL
      FROM
            dbo.SITE s
      WHERE
            s.CLIENT_ID = @clientId
            AND @Is_Analyst_Mapping_Cd_Changed = 1
            AND s.Analyst_Mapping_Cd IS NOT NULL  
     
      UPDATE
            acc
      SET   
            acc.Analyst_Mapping_Cd = NULL
      FROM
            dbo.ACCOUNT acc
            INNER JOIN core.Client_Hier ch
                  ON acc.SITE_ID = ch.Site_Id
      WHERE
            ch.Client_Id = @clientId
            AND @Is_Analyst_Mapping_Cd_Changed = 1
            AND acc.Analyst_Mapping_Cd IS NOT NULL  
  
      UPDATE
            dbo.SiteGroup
      SET   
            Sitegroup_Name = @clientName
      WHERE
            client_ID = @clientId
            AND sitegroup_id IN ( SELECT
                                    SiteGroup_Id
                                  FROM
                                    dbo.division_dtl
                                  WHERE
                                    client_id = @clientId
                                    AND is_corporate_division = 1 )  
END;

;
GO


GRANT EXECUTE ON  [dbo].[GET_UPDATE_CLIENT_P] TO [CBMSApplication]
GO
