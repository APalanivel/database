
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.cbmsCuExceptionDenorm_ReloadAll

DESCRIPTION:
	to refresh the table with new data.

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------

SELECT * FROM CU_EXCEPTION_DENORM WHERE QUEUE_ID = 19997

AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------
PNR				Pandarinath
HG				Harihara Suthan G

MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
SKA			10/21/2010	Created		
			11/19/2010	Added the execute statement so that it will refresh the table with new logic.
SKA			01/13/2011	Added the where condition for ed.IS_CLOSED AND ed.CLOSED_REASON_TYPE_ID (Bug#21854)
PNR			01/20/2011	Added the where condition for ed.IS_CLOSED AND ed.CLOSED_REASON_TYPE_ID (Bug#21854) at Sub query of join
HG			02/18/2011	Removed the filter condition Is_Closed = 0 on Cu_Exception as per the requirement of #21854
						(Requirement is to show all the closed exceptions if closed reason type is not available)
HG			02/22/2011	Removed the Execute statement which was added at the end as it is not required to run in Production.
HG			03/14/2011	As per the request reverted the changes made to show the invoice exception which are in closed status but the closed reason type id is null
							(MAINT-534)

******/

CREATE PROCEDURE dbo.cbmsCuExceptionDenorm_ReloadAll
AS 
BEGIN

      TRUNCATE TABLE dbo.cu_exception_denorm
  
      INSERT INTO
            dbo.CU_EXCEPTION_DENORM
            ( 
             CBMS_IMAGE_ID
            ,CBMS_DOC_ID
            ,CU_INVOICE_ID
            ,QUEUE_ID
            ,EXCEPTION_TYPE
            ,EXCEPTION_STATUS_TYPE
            ,UBM_Account_Number
            ,Service_month
            ,UBM_Client_Name
            ,UBM_Site_Name
            ,UBM_State_Name
            ,UBM_City
            ,IS_MANUAL
            ,DATE_IN_QUEUE
            ,SORT_ORDER
            ,Client_Hier_ID
            ,Account_ID )
            SELECT DISTINCT
                  ci.cbms_image_id
                 ,ci.cbms_doc_id
                 ,i.cu_invoice_id
                 ,ex.queue_id
                 ,'exception_type' = CASE WHEN x.exception_type_count = 1 THEN et.exception_type
                                          ELSE 'Multiple'
                                     END
                 ,'exception_status_type' = CASE WHEN ex.routed_reason_type_id IS NOT NULL THEN rr.entity_name
                                                 ELSE CASE WHEN y.exception_status_type_count = 1 THEN es.entity_name
                                                           ELSE 'Multiple'
                                                      END
                                            END
                 ,'UBM_Account_Number' = ISNULL(ISNULL(a.account_number, l.account_number), i.ubm_account_number)
                 ,'Service_Month' = CASE WHEN z.service_month_count = 1 THEN CONVERT(VARCHAR, cism.service_month, 101)
                                         WHEN z.service_month_count IS NULL
                                              OR z.service_month_count = 0 THEN NULL
                                         ELSE 'Multiple'
                                    END
                 ,'UBM_Client_Name' = CASE WHEN cism.account_id IS NULL THEN ISNULL(l.client_name, chcl.client_name)
                                           ELSE ISNULL(chcl.client_name, l.client_name)
                                      END
                 ,'UBM_Site_Name' = CASE WHEN cism.account_id IS NOT NULL THEN RTRIM(ad.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')'
                                         WHEN l.city IS NOT NULL THEN l.city + ', ' + l.state_name
                                         WHEN i.ubm_city IS NOT NULL THEN i.ubm_city + ', ' + i.ubm_state_code
                                         ELSE NULL
                                    END
                 ,'UBM_State_Name' = ISNULL(ISNULL(ch.state_name, l.state_name), i.ubm_state_code)
                 ,'UBM_City' = ISNULL(ISNULL(ad.city, l.city), i.ubm_city)
                 ,i.is_manual
                 ,ex.opened_date date_in_queue
                 ,et.sort_order
                 ,ch.Client_Hier_Id
                 ,vam.ACCOUNT_ID
            FROM
                  dbo.CU_EXCEPTION ex
                  JOIN dbo.CU_EXCEPTION_DETAIL ed
                        ON ed.cu_exception_id = ex.cu_exception_id
                  JOIN dbo.CU_EXCEPTION_TYPE et
                        ON et.cu_exception_type_id = ed.exception_type_id
                  JOIN dbo.ENTITY es
                        ON es.entity_id = ed.exception_status_type_id
                  JOIN dbo.cu_invoice i
                        ON i.cu_invoice_id = ex.cu_invoice_id
                  JOIN dbo.cbms_image ci
                        ON ci.cbms_image_id = i.cbms_image_id
                  LEFT OUTER JOIN dbo.ENTITY rr
                        ON rr.entity_id = ex.routed_reason_type_id
                  LEFT OUTER JOIN dbo.cu_invoice_service_month cism
                        ON cism.cu_invoice_id = i.cu_invoice_id
                           AND ISNULL(ed.Account_ID, 0) = ( CASE WHEN et.EXCEPTION_TYPE = 'Failed Recalc' THEN cism.Account_ID
                                                                 ELSE 0
                                                            END )
                  LEFT OUTER JOIN dbo.CU_INVOICE_LABEL l
                        ON l.cu_invoice_id = ex.cu_invoice_id
                  LEFT OUTER JOIN dbo.ACCOUNT a
                        ON a.account_id = cism.account_id
                  LEFT OUTER JOIN dbo.VENDOR v
                        ON v.vendor_id = i.vendor_id
                  LEFT OUTER JOIN Core.Client_Hier chcl
                        ON i.CLIENT_ID = chcl.Client_Id
                           AND chcl.Site_Id = 0
                           AND chcl.Sitegroup_Id = 0
                  LEFT OUTER JOIN dbo.vwAccountMeter vam
                        ON vam.account_id = cism.account_id
                  LEFT OUTER JOIN dbo.METER me
                        ON me.meter_id = vam.meter_id
                  LEFT OUTER JOIN dbo.ADDRESS ad
                        ON ad.address_id = me.address_id
                  LEFT OUTER JOIN Core.Client_Hier ch
                        ON vam.site_id = ch.Site_Id
                           AND ch.Site_Id > 0
                  JOIN ( SELECT
                              ex.cu_invoice_id
                             ,COUNT(DISTINCT ed.exception_type_id) exception_type_count
                         FROM
                              dbo.CU_EXCEPTION ex
                              JOIN dbo.CU_EXCEPTION_DETAIL ed
                                    ON ed.cu_exception_id = ex.cu_exception_id
                         WHERE
									ed.Is_Closed = 0
									AND ex.Is_Closed = 0
                         GROUP BY
                              ex.cu_invoice_id ) x
                        ON x.cu_invoice_id = ex.cu_invoice_id
                  JOIN ( SELECT
                              ex.cu_invoice_id
                             ,COUNT(DISTINCT ed.exception_status_type_id) exception_status_type_count
                         FROM
                              dbo.CU_EXCEPTION ex
                              JOIN dbo.CU_EXCEPTION_DETAIL ed
                                    ON ed.cu_exception_id = ex.cu_exception_id
                         WHERE
								ed.Is_Closed = 0
								AND ex.Is_Closed = 0

                         GROUP BY
                              ex.cu_invoice_id ) y
                        ON y.cu_invoice_id = ex.cu_invoice_id
                  LEFT OUTER JOIN ( SELECT
                                          cu_invoice_id
                                         ,COUNT(DISTINCT service_month) service_month_count
                                    FROM
                                          dbo.CU_INVOICE_SERVICE_MONTH
                                    GROUP BY
                                          cu_invoice_id ) z
                        ON z.cu_invoice_id = i.cu_invoice_id
            WHERE
				ed.Is_Closed = 0
				AND ex.Is_Closed = 0

END
GO

GRANT EXECUTE ON  [dbo].[cbmsCuExceptionDenorm_ReloadAll] TO [CBMSApplication]
GO
