SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.UBM_BATCH_MASTER_LOG_Sel_For_Cass_Invoice

DESCRIPTION:

INPUT PARAMETERS:
	Name					DataType		Default	Description
---------------------------------------------------------------
	@Invoice_Dt				Date

OUTPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	
USAGE EXAMPLES:
------------------------------------------------------------

	Exec UBM_BATCH_MASTER_LOG_Sel_For_Cass_Invoice '05/30/2015','05/31/2015',16, 0


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	AL			Ajeesh L
	Meera		R
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	AL       	2018-05-21	Created for Cass Image loading
	Meera R		2018-06-25	CASS Image migration changes
	KVK			10/01/2018	Added filter to pick only completed Batches
	KVK			10/29/2018	Modified to pick distinct CBMS_Doc_Ids and its min CBMS_Image_Id
******/
CREATE PROCEDURE [dbo].[UBM_BATCH_MASTER_LOG_Sel_For_Cass_Invoice]
(
    @Start_Dt DATE,
    @End_Dt DATE,
    @User_Info_Id INT,
    @Image_Location_Id INT,
    @Failed_Only BIT = 0
)
AS
BEGIN

    SET NOCOUNT ON;

    DECLARE @In_Progress_Cd INT,
            @Error_Cd INT,
            @Completed_Cd INT,
            @UBM_Batch_Master_Log_Id INT,
            @UBM_Batch_Status_ID INT;

    SELECT @In_Progress_Cd = cd.Code_Id
    FROM dbo.Code cd
        INNER JOIN dbo.Codeset cs
            ON cs.Codeset_Id = cd.Codeset_Id
    WHERE cs.Codeset_Name = 'ICQ Batch Status'
          AND cd.Code_Value = 'In Progress';

    SELECT @Error_Cd = cd.Code_Id
    FROM dbo.Code cd
        INNER JOIN dbo.Codeset cs
            ON cs.Codeset_Id = cd.Codeset_Id
    WHERE cs.Codeset_Name = 'ICQ Batch Status'
          AND cd.Code_Value = 'Error';

    SELECT @Completed_Cd = cd.Code_Id
    FROM dbo.Code cd
        INNER JOIN dbo.Codeset cs
            ON cs.Codeset_Id = cd.Codeset_Id
    WHERE cs.Codeset_Name = 'ICQ Batch Status'
          AND cd.Code_Value = 'Completed';

    SELECT @UBM_Batch_Status_ID = e.ENTITY_ID
    FROM dbo.ENTITY e
    WHERE e.ENTITY_DESCRIPTION = 'UBM_BATCH_PROCESS_STATUS_TYPE'
          AND e.ENTITY_NAME = 'Success';

    --Completed
    DECLARE @Invoice_Image_Migration AS TABLE
    (
        UBM_BATCH_MASTER_LOG_ID INT NOT NULL,
        CBMS_DOC_ID VARCHAR(200) NOT NULL,
        CBMS_Image_FileName VARCHAR(255) NULL,
        CBMS_Image_Directory VARCHAR(255) NULL,
        CBMS_IMAGE_ID INT NOT NULL
    );

    SELECT TOP (1)
        @UBM_Batch_Master_Log_Id = ubml.UBM_BATCH_MASTER_LOG_ID
    FROM dbo.UBM_BATCH_MASTER_LOG ubml
        INNER JOIN dbo.UBM u
            ON u.UBM_ID = ubml.UBM_ID
        LEFT JOIN dbo.Invoice_Image_Migration_Batch iimb
            ON iimb.UBM_Batch_Master_Log_Id = ubml.UBM_BATCH_MASTER_LOG_ID
        LEFT JOIN dbo.Invoice_Image_Migration_Log iiml
            ON iiml.UBM_Batch_Master_Log_Id = ubml.UBM_BATCH_MASTER_LOG_ID
    WHERE u.UBM_NAME = 'Cass'
          AND ubml.STATUS_TYPE_ID = @UBM_Batch_Status_ID
          AND
          (
              (
                  @Failed_Only = 0
                  AND iimb.UBM_Batch_Master_Log_Id IS NULL
                  AND iiml.UBM_Batch_Master_Log_Id IS NULL
              )
              OR
              (
                  @Failed_Only = 1
                  AND iiml.Status_Cd = @Error_Cd
              )
          )
          AND CAST(ubml.START_DATE AS DATE)
          BETWEEN @Start_Dt AND @End_Dt
    GROUP BY ubml.UBM_BATCH_MASTER_LOG_ID;

    ;WITH distinctDocIds
    AS (SELECT @UBM_Batch_Master_Log_Id UBM_Batch_Master_Log_Id,
               ci.CBMS_DOC_ID,
               MIN(ci.CBMS_IMAGE_ID) min_CBMS_IMAGE_ID
        FROM dbo.UBM_INVOICE ui
            INNER JOIN dbo.cbms_image ci
                ON ci.CBMS_IMAGE_ID = ui.CBMS_IMAGE_ID
        WHERE @Failed_Only = 0
              AND ci.CBMS_Image_Location_Id <> @Image_Location_Id
              AND ui.UBM_BATCH_MASTER_LOG_ID = @UBM_Batch_Master_Log_Id
        GROUP BY ci.CBMS_DOC_ID)
    INSERT INTO @Invoice_Image_Migration
    SELECT dd.UBM_Batch_Master_Log_Id UBM_BATCH_MASTER_LOG_ID,
           ci.CBMS_DOC_ID,
           ci.CBMS_Image_FileName,
           ci.CBMS_Image_Directory,
           ci.CBMS_IMAGE_ID
    FROM distinctDocIds dd
        INNER JOIN dbo.cbms_image ci
            ON ci.CBMS_IMAGE_ID = dd.min_CBMS_IMAGE_ID
        LEFT JOIN dbo.Invoice_Image_Migration_Log iiml
            ON ci.CBMS_IMAGE_ID = iiml.CBMS_Image_Id
    WHERE iiml.UBM_Batch_Master_Log_Id IS NULL;


    INSERT INTO @Invoice_Image_Migration
    SELECT ubml.UBM_BATCH_MASTER_LOG_ID,
           ci.CBMS_DOC_ID,
           ci.CBMS_Image_FileName,
           ci.CBMS_Image_Directory,
           ci.CBMS_IMAGE_ID
    FROM dbo.UBM_BATCH_MASTER_LOG ubml
        INNER JOIN dbo.Invoice_Image_Migration_Batch iimb
            ON iimb.UBM_Batch_Master_Log_Id = ubml.UBM_BATCH_MASTER_LOG_ID
        INNER JOIN dbo.Invoice_Image_Migration_Log iiml
            ON iiml.UBM_Batch_Master_Log_Id = ubml.UBM_BATCH_MASTER_LOG_ID
        INNER JOIN dbo.cbms_image ci
            ON ci.CBMS_IMAGE_ID = iiml.CBMS_Image_Id
    WHERE @Failed_Only = 1
          AND ci.CBMS_Image_Location_Id <> @Image_Location_Id
          AND ubml.UBM_BATCH_MASTER_LOG_ID = @UBM_Batch_Master_Log_Id
          AND iiml.Status_Cd = @Error_Cd
          AND iimb.Status_Cd = @Completed_Cd
    GROUP BY ubml.UBM_BATCH_MASTER_LOG_ID,
             ci.CBMS_DOC_ID,
             ci.CBMS_Image_FileName,
             ci.CBMS_Image_Directory,
             ci.CBMS_IMAGE_ID;

    BEGIN TRY
        BEGIN TRANSACTION;
        INSERT INTO dbo.Invoice_Image_Migration_Batch
        (
            UBM_Batch_Master_Log_Id,
            Status_Cd,
            Created_User_Id,
            Created_Ts,
            Updated_User_Id,
            Last_Change_Ts
        )
        SELECT @UBM_Batch_Master_Log_Id,
               @In_Progress_Cd,
               @User_Info_Id,
               GETDATE(),
               @User_Info_Id,
               GETDATE()
        WHERE @Failed_Only = 0
              AND @UBM_Batch_Master_Log_Id IS NOT NULL;

        INSERT INTO dbo.Invoice_Image_Migration_Log
        (
            CBMS_Image_Id,
            UBM_Batch_Master_Log_Id,
            Status_Cd,
            Created_User_Id,
            Created_Ts,
            Updated_User_Id,
            Last_Change_Ts
        )
        SELECT CBMS_IMAGE_ID,
               UBM_BATCH_MASTER_LOG_ID,
               @In_Progress_Cd,
               @User_Info_Id,
               GETDATE(),
               @User_Info_Id,
               GETDATE()
        FROM @Invoice_Image_Migration
        WHERE @Failed_Only = 0;
        COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;

        EXECUTE dbo.usp_RethrowError;
    END CATCH;
    SELECT UBM_BATCH_MASTER_LOG_ID,
           CBMS_DOC_ID,
           CBMS_Image_FileName,
           CBMS_Image_Directory,
           CBMS_IMAGE_ID
    FROM @Invoice_Image_Migration;

END;





GO
GRANT EXECUTE ON  [dbo].[UBM_BATCH_MASTER_LOG_Sel_For_Cass_Invoice] TO [CBMSApplication]
GO
