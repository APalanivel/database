SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE dbo.BUDGET_SAVE_XML_P
	@budget_account_id int,
	@budget_xml text
	AS

	if (select count(budget_detail_snap_shot_id) from budget_detail_snap_shot where budget_account_id = @budget_account_id) > 0
	begin
		update budget_detail_snap_shot set budget_xml = @budget_xml where budget_account_id = @budget_account_id
	end else
	begin
		insert into budget_detail_snap_shot(budget_account_id, budget_xml) values(@budget_account_id, @budget_xml)
	end








GO
GRANT EXECUTE ON  [dbo].[BUDGET_SAVE_XML_P] TO [CBMSApplication]
GO
