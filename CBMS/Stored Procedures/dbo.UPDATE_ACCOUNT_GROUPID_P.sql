SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE dbo.UPDATE_ACCOUNT_GROUPID_P
	@account_group_id int,
	@account_id int
	AS
	begin
		set nocount on

		update	account 
		set	ACCOUNT_GROUP_ID = @account_group_id  
		where	account_id = @account_id

	end
GO
GRANT EXECUTE ON  [dbo].[UPDATE_ACCOUNT_GROUPID_P] TO [CBMSApplication]
GO
