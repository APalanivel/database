SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******        
  

NAME:        
dbo.Consumption_Unit_Conversion_Upd       

DESCRIPTION:        
	Used to insert/update UOM conversion into COMMODITY_UOM_CONVERSION table      

INPUT PARAMETERS:        
Name				DataType Default Description        
------------------------------------------------------------        
  @BaseUnitId AS INT
, @ConvertedUnitId AS INT
, @Conversion_Factor AS DECIMAL(   32 , 16 )     
      
OUTPUT PARAMETERS:        
Name				DataType Default Description        
------------------------------------------------------------        
USAGE EXAMPLES:        
------------------------------------------------------------      

EXEC dbo.Consumption_Unit_Conversion_Upd 87,102,22


AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
GB   Geetansu Behera        
CMH  Chad Hattabaugh         
HG   Hari       
SKA Shobhit Kr Agrawal    

MODIFICATIONS         
Initials Date Modification        
------------------------------------------------------------        
GB    Created      
SKA  08/07/09 Modified    

GB  14/07/09 In Entity Table the Unit for Description is different for Different Commodity,   
			It does not follow standard pattern for electriv power, Natural gas and Alternative Natural Gas  
			Hence those are treated separately in the case  

    
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[Consumption_Unit_Conversion_Upd]
	@BaseUnitId AS INT
	, @ConvertedUnitId AS INT
	, @Conversion_Factor AS DECIMAL(   32 , 16 )
	, @Is_Updated BIT =0 OUT
AS 

BEGIN

    SET  NOCOUNT ON;
    
    DECLARE @TConversion_Factor AS DECIMAL(32, 16)

    BEGIN TRY
		BEGIN TRANSACTION    

			UPDATE 
				dbo.CONSUMPTION_UNIT_CONVERSION SET  BASE_UNIT_ID = @BaseUnitId
				, CONVERTED_UNIT_ID = @ConvertedUnitId
				, CONVERSION_FACTOR = @Conversion_Factor
			WHERE 
				BASE_UNIT_ID = @BaseUnitId AND
				CONVERTED_UNIT_ID = @ConvertedUnitId

			SELECT @Is_Updated = CASE WHEN @@ROWCOUNT > 0 THEN 1 ELSE 0 END 

			SET @TConversion_Factor = 1 / @Conversion_Factor

			UPDATE 
				dbo.CONSUMPTION_UNIT_CONVERSION SET BASE_UNIT_ID = @ConvertedUnitId
				, CONVERTED_UNIT_ID = @BaseUnitId
				, CONVERSION_FACTOR = @TConversion_Factor
			WHERE 
				BASE_UNIT_ID = @ConvertedUnitId 
				AND	CONVERTED_UNIT_ID = @BaseUnitId

			SELECT @Is_Updated = CASE WHEN @@ROWCOUNT > 0 OR @Is_Updated > 0 THEN 1 ELSE 0 END 
			SELECT @Is_Updated AS IS_UPDATED
    
		COMMIT TRAN
	END TRY
		BEGIN CATCH
		ROLLBACK TRAN
		EXEC dbo.usp_RethrowError
	END CATCH

END
GO
GRANT EXECUTE ON  [dbo].[Consumption_Unit_Conversion_Upd] TO [CBMSApplication]
GO
