
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.Utility_Account_Budget_Vendor_Sel_By_State_Id_Vendor_Name
           
DESCRIPTION:             
			To get vendors by service
			
INPUT PARAMETERS:            
	Name				DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Commodity_Id		INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  

    EXEC dbo.Utility_Account_Budget_Vendor_Sel_By_State_Id_Vendor_Name 
      @Vendor_Name = 'Di'
	
    EXEC dbo.Utility_Account_Budget_Vendor_Sel_By_State_Id_Vendor_Name 
      @Vendor_Name = 'geor'

 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
    RKV         Ravi Kumar Vegesna
    NR		    Narayana Reddy


 MODIFICATIONS:
	Initials	Date			 Modification
------------------------------------------------------------
    RKV         2017-05-23       Created as part of SE2017-147  
    NR			2017-08-03		 MAINT-5514 - Chnaged Account_Vendor_Name order by clause DESC to ASC.		              
******/
CREATE  PROCEDURE [dbo].[Utility_Account_Budget_Vendor_Sel_By_State_Id_Vendor_Name]
      ( 
       @Vendor_Name VARCHAR(200) = NULL
      ,@State_Id VARCHAR(200) = NULL )
AS 
BEGIN

      SET NOCOUNT ON;
      
      SELECT
            cha.Account_Vendor_Id
           ,cha.Account_Vendor_Name
      FROM
            Core.Client_Hier_Account cha
            INNER JOIN core.Client_Hier ch
                  ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN dbo.BUDGET_ACCOUNT ba
                  ON cha.Account_Id = ba.ACCOUNT_ID
      WHERE
            cha.Account_Type = 'Utility'
            AND ( @Vendor_Name IS NULL
                  OR cha.Account_Vendor_Name LIKE +'%' + @Vendor_Name + '%' )
            AND ( @State_Id IS NULL
                  OR ch.State_Id = @State_Id )
      GROUP BY
            cha.Account_Vendor_Id
           ,cha.Account_Vendor_Name
      ORDER BY
            cha.Account_Vendor_Name 
END;

;
GO

GRANT EXECUTE ON  [dbo].[Utility_Account_Budget_Vendor_Sel_By_State_Id_Vendor_Name] TO [CBMSApplication]
GO
