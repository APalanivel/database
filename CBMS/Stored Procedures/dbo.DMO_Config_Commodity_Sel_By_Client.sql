SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.DMO_Config_Commodity_Sel_By_Client
           
DESCRIPTION:             
			To select DMO configurations
			
INPUT PARAMETERS:            
	Name				DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Client_Id			INT
    @Commodity_Id		INT			NULL 
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT DISTINCT a.Commodity_Id,b.Client_Id FROM dbo.Client_Hier_DMO_Config a 
	INNER JOIN Core.Client_Hier b ON a.Client_Hier_Id = b.Client_Hier_Id
	ORDER BY b.Client_Id

            
	EXEC dbo.DMO_Config_Commodity_Sel_By_Client 218
	EXEC dbo.DMO_Config_Commodity_Sel_By_Client 1009
	EXEC dbo.DMO_Config_Commodity_Sel_By_Client 11548
	EXEC dbo.DMO_Config_Commodity_Sel_By_Client 11548,291
		
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-01-20	Contract placeholder - CP-4 Created
******/

CREATE PROCEDURE [dbo].[DMO_Config_Commodity_Sel_By_Client]
      ( 
       @Client_Id INT
      ,@Commodity_Id INT = NULL )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Tbl_Config AS TABLE
            ( 
             Commodity_Id INT
            ,Commodity_Name VARCHAR(50) )
      
      
            
      INSERT      INTO @Tbl_Config
                  ( 
                   Commodity_Id
                  ,Commodity_Name )
                  SELECT
                        chdc.Commodity_Id
                       ,com.Commodity_Name
                  FROM
                        dbo.Client_Hier_DMO_Config chdc
                        INNER JOIN Core.Client_Hier ch
                              ON chdc.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.Commodity com
                              ON chdc.Commodity_Id = com.Commodity_Id
                  WHERE
                        @Client_Id IS NOT NULL
                        AND ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id = 0
                        AND ch.Site_Id = 0
                        
      INSERT      INTO @Tbl_Config
                  ( 
                   Commodity_Id
                  ,Commodity_Name )
                  SELECT
                        chdc.Commodity_Id
                       ,com.Commodity_Name
                  FROM
                        dbo.Client_Hier_DMO_Config chdc
                        INNER JOIN Core.Client_Hier ch
                              ON chdc.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.Commodity com
                              ON chdc.Commodity_Id = com.Commodity_Id
                  WHERE
                        @Client_Id IS NOT NULL
                        AND ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id > 0
                        AND ch.Site_Id = 0
                        
      INSERT      INTO @Tbl_Config
                  ( 
                   Commodity_Id
                  ,Commodity_Name )
                  SELECT
                        chdc.Commodity_Id
                       ,com.Commodity_Name
                  FROM
                        dbo.Client_Hier_DMO_Config chdc
                        INNER JOIN Core.Client_Hier ch
                              ON chdc.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.Commodity com
                              ON chdc.Commodity_Id = com.Commodity_Id
           WHERE
                        @Client_Id IS NOT NULL
                        AND ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id > 0
                        AND ch.Site_Id > 0
                        
      INSERT      INTO @Tbl_Config
                  ( 
                   Commodity_Id
                  ,Commodity_Name )
                  SELECT
                        adc.Commodity_Id
                       ,com.Commodity_Name
                  FROM
                        dbo.Account_DMO_Config adc
                        INNER JOIN Core.Client_Hier_Account cha
                              ON adc.Account_Id = cha.Account_Id
                        INNER JOIN Core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.Commodity com
                              ON adc.Commodity_Id = com.Commodity_Id
                  WHERE
                        @Client_Id IS NOT NULL
                        AND ch.Client_Id = @Client_Id
                        
      
      SELECT
            tc.Commodity_Id
           ,tc.Commodity_Name
      FROM
            @Tbl_Config tc
      WHERE
            @Commodity_Id IS NULL
            OR tc.Commodity_Id = @Commodity_Id
      GROUP BY
            tc.Commodity_Id
           ,tc.Commodity_Name
      ORDER BY
            CASE WHEN tc.Commodity_Name = 'Electric Power' THEN 1
                 WHEN tc.Commodity_Name = 'Natural Gas' THEN 2
                 ELSE 3
            END 
      
END;
;


;
GO
GRANT EXECUTE ON  [dbo].[DMO_Config_Commodity_Sel_By_Client] TO [CBMSApplication]
GO
