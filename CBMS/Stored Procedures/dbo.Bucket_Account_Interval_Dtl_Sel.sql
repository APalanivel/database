SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:        
        
 dbo.Bucket_Account_Interval_Dtl_Sel        
        
DESCRIPTION:        
        
 This procedure is used to Select the data for given client hier id,bucket master id,account id,service start date and service end date        
         
        
INPUT PARAMETERS:        
   Name      DataType  Default     Description         
-------------------------------------------------------------------------------------        
 @Client_Hier_Id      INT        
 @Account_Id          INT        
 @Bucket_Master_Id    INT        
 @Service_Start_Dt    DATE        
 @Service_End_Dt      DATE        
        
        
OUTPUT PARAMETERS:        
 Name       DataType     Default     Description                    
-------------------------------------------------------------------------------------        
        
USAGE EXAMPLES:        
-------------------------------------------------------------------------------------        
DECLARE @Client_Hier_Id INT,@Account_Id INT,@Bucket_Master_Id INT,@Service_Start_Dt DATE,@Service_End_Dt DATE,@Data_Source_Cd INT
      
SELECT TOP 1 @Client_Hier_Id=Client_Hier_Id,@Account_Id=Account_Id,@Bucket_Master_Id=Bucket_Master_Id,@Service_Start_Dt=Service_Start_Dt,
@Service_End_Dt=Service_End_Dt,@Data_Source_Cd=Data_Source_Cd FROM dbo.Bucket_Account_Interval_Dtl

EXEC dbo.Bucket_Account_Interval_Dtl_Sel  @Client_Hier_Id=@Client_Hier_Id,@Account_Id=@Account_Id,@Bucket_Master_Id=@Bucket_Master_Id,
@Service_Start_Dt=@Service_Start_Dt,@Service_End_Dt=@Service_End_Dt,@Data_Source_Cd=@Data_Source_Cd
      
        
AUTHOR INITIALS:        
 Initials  Name        
-------------------------------------------------------------------------------------        
   BCH         Balaraju        
                                      
MODIFICATIONS                    
 Initials  Date         Modification                    
-------------------------------------------------------------------------------------        
    BCH   2013-03-07   Created                            
***********/              
CREATE PROCEDURE dbo.Bucket_Account_Interval_Dtl_Sel
      ( 
       @Client_Hier_Id INT
      ,@Account_Id INT
      ,@Bucket_Master_Id INT
      ,@Service_Start_Dt DATE
      ,@Service_End_Dt DATE
      ,@Data_Source_Cd INT )
AS 
BEGIN        
      SET NOCOUNT ON ;        
        
      SELECT
            Client_Hier_Id
           ,Account_Id
           ,Bucket_Master_Id
           ,Service_Start_Dt
           ,Service_End_Dt
           ,Bucket_Daily_Avg_Value
           ,Data_Source_Cd
           ,Uom_Type_Id
           ,Currency_Unit_Id
           ,Created_User_Id
           ,Updated_User_Id
           ,Created_Ts
           ,Last_Changed_Ts
      FROM
            dbo.Bucket_Account_Interval_Dtl
      WHERE
            Client_Hier_Id = @Client_Hier_Id
            AND Account_Id = @Account_Id
            AND Bucket_Master_Id = @Bucket_Master_Id
            AND Service_Start_Dt = @Service_Start_Dt
            AND Service_End_Dt = @Service_End_Dt
            AND Data_Source_Cd = @Data_Source_Cd       
                    
        
END 
;
GO
GRANT EXECUTE ON  [dbo].[Bucket_Account_Interval_Dtl_Sel] TO [CBMSApplication]
GO
