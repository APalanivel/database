SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE   PROCEDURE dbo.ADD_UBM_IDS_P
	@ubmID int,
	@ubmClientCode varchar(1000),
	@clientID int
	AS
	begin
		set nocount on

		insert into ubm_client_map(
				UBM_ID, 
				UBM_CLIENT_CODE, 
				CLIENT_ID) 
		values 	       (@ubmID,
				@ubmClientCode,
				@clientID)

	end




GO
GRANT EXECUTE ON  [dbo].[ADD_UBM_IDS_P] TO [CBMSApplication]
GO
