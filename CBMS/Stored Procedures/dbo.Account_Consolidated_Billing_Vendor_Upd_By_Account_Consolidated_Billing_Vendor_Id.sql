SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.Account_Consolidated_Billing_Vendor_Upd_By_Account_Consolidated_Billing_Vendor_Id
           
DESCRIPTION:             
			To update consolidated billing configurations
			
INPUT PARAMETERS:            
	Name									DataType	Default		Description  
--------------------------------------------------------------------------------------------------  
	@Account_Consolidated_Billing_Vendor_Id INT
    @Billing_Start_Dt						DATE
    @Billing_End_Dt							DATE
    @User_Info_Id							INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
--------------------------------------------------------------------------------------------------  

 USAGE EXAMPLES:
--------------------------------------------------------------------------------------------------  

	EXEC dbo.Account_Consolidated_Billing_Vendor_Upd_By_Account_Consolidated_Billing_Vendor_Id
    @Account_Consolidated_Billing_Vendor_Id = 1
    , @Billing_Start_Dt = '2019-01-01'
    , @Billing_End_Dt = '2019-02-01'
    , @User_Info_Id = 49
	
	
		
 AUTHOR INITIALS:            
	Initials			Name            
--------------------------------------------------------------------------------------------------  
	NR					Narayana Reddy			

 MODIFICATIONS:
	Initials		Date				Modification
--------------------------------------------------------------------------------------------------  
	NR				2019-09-30			Add Contract - Created.
******/

CREATE PROCEDURE [dbo].[Account_Consolidated_Billing_Vendor_Upd_By_Account_Consolidated_Billing_Vendor_Id]
    (
        @Account_Consolidated_Billing_Vendor_Id INT
        , @Billing_Start_Dt DATE
        , @Billing_End_Dt DATE
        , @User_Info_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        UPDATE
            dbo.Account_Consolidated_Billing_Vendor
        SET
            Billing_Start_Dt = @Billing_Start_Dt
            , Billing_End_Dt = @Billing_End_Dt
            , Updated_User_Id = @User_Info_Id
            , Last_Change_Ts = GETDATE()
        WHERE
            Account_Consolidated_Billing_Vendor_Id = @Account_Consolidated_Billing_Vendor_Id;


    END;


GO
GRANT EXECUTE ON  [dbo].[Account_Consolidated_Billing_Vendor_Upd_By_Account_Consolidated_Billing_Vendor_Id] TO [CBMSApplication]
GO
