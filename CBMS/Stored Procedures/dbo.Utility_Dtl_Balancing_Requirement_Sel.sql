SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Utility_Dtl_Balancing_Requirement_Sel

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Vendor_Id		INT
	@Commodity_Id	INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC Utility_Dtl_Balancing_Requirement_Sel 370,291
	
	SELECT * FROM vendor_Commodity_Map WHERE Vendor_Id = 370
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal
	HG			Harihara Suthan G

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	SKA			01/12/2011	Created
	HG			03/11/2011	Script modified to implement the column changes 
							(Utility_Summary_Question_Id and Utility_Detail_Id column replaced by Question_Commodity_Map_Id and Vendor_Commodity_Map_Id column as this utility balancing requirement section becomes specific to commodity(Natural Gas))

******/
CREATE PROC dbo.Utility_Dtl_Balancing_Requirement_Sel
(
 @Vendor_Id INT
,@Commodity_Id INT )
AS
BEGIN

      SET NOCOUNT ON

      SELECT
            utl_br.Utility_Dtl_Balancing_Requirement_Id
           ,qcm.Question_Commodity_Map_Id
           ,usq.Question_Label
           ,utl_br.Industrial_Flag_Cd
           ,cd1.Code_Value AS Industrial_Flag_Value
           ,utl_br.Commercial_Flag_Cd
           ,cd2.Code_Value AS Commercial_Flag_Value
           ,utl_br.Comment_ID
           ,cmt.Comment_Text
      FROM
            dbo.Question_Commodity_Map qcm
            INNER JOIN dbo.Utility_Summary_Question usq
                  ON usq.Utility_Summary_Question_Id = qcm.Utility_Summary_Question_Id
            INNER JOIN dbo.Utility_Summary_Section uss
                  ON uss.Utility_Summary_Section_Id = usq.Utility_Summary_Section_Id
            INNER JOIN dbo.VENDOR_COMMODITY_MAP vcm
                  ON vcm.COMMODITY_TYPE_ID = qcm.Commodity_Id
            LEFT JOIN dbo.Utility_Dtl_Balancing_Requirement utl_br
                  ON vcm.VENDOR_COMMODITY_MAP_ID = utl_br.VENDOR_COMMODITY_MAP_ID
                     AND qcm.Question_Commodity_Map_Id = utl_br.Question_Commodity_Map_Id
            LEFT JOIN dbo.Comment cmt
                  ON cmt.Comment_ID = utl_br.Comment_ID
            LEFT JOIN dbo.Code cd1
                  ON cd1.Code_Id = utl_br.Industrial_Flag_Cd
            LEFT JOIN dbo.Code cd2
                  ON cd2.Code_Id = utl_br.Commercial_Flag_Cd
      WHERE
            vcm.VENDOR_ID = @Vendor_ID
            AND vcm.COMMODITY_TYPE_ID = @Commodity_Id
            AND usq.Is_Active = 1
            AND uss.Section_Label = 'Utility Balancing Requirements'
      ORDER BY
            usq.Display_Order

END
GO
GRANT EXECUTE ON  [dbo].[Utility_Dtl_Balancing_Requirement_Sel] TO [CBMSApplication]
GO
