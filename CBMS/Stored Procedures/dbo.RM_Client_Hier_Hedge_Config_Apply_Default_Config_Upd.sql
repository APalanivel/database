SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        dbo.RM_Client_Hier_Hedge_Config_Apply_Default_Config_Upd                    
                      
Description:                      
        To delete a hedge configuration
                      
Input Parameters:                      
    Name				DataType        Default     Description                        
--------------------------------------------------------------------------------
	@RM_Hedge_Config_Id	INT
    
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	@RM_Group_Id	INT  
                    
Usage Examples:                          
--------------------------------------------------------------------------------
	
	BEGIN TRANSACTION
		EXEC dbo.RM_Client_Hier_Hedge_Config_Apply_Default_Config_Upd 235,291,'4',16
		 
	ROLLBACK TRANSACTION     
                     
 Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy        
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			2018-08-23     Global Risk Management - Created                    
                     
******/


CREATE PROCEDURE [dbo].[RM_Client_Hier_Hedge_Config_Apply_Default_Config_Upd]
    (
        @Client_Hier_Id INT
        , @Commodity_Id INT
        , @Apply_Default_Config BIT
        , @User_Info_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DELETE
        chhc
        FROM
            Trade.RM_Client_Hier_Hedge_Config chhc
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON chhc.RM_Client_Hier_Onboard_Id = chob.RM_Client_Hier_Onboard_Id
        WHERE
            chob.Client_Hier_Id = @Client_Hier_Id
            AND chob.Commodity_Id = @Commodity_Id
            AND @Apply_Default_Config IS NOT NULL
            AND @Apply_Default_Config = 1;

        DELETE
        chob
        FROM
            Trade.RM_Client_Hier_Onboard chob
        WHERE
            chob.Client_Hier_Id = @Client_Hier_Id
            AND chob.Commodity_Id = @Commodity_Id
            AND @Apply_Default_Config IS NOT NULL
            AND @Apply_Default_Config = 1;


    END;

GO
GRANT EXECUTE ON  [dbo].[RM_Client_Hier_Hedge_Config_Apply_Default_Config_Upd] TO [CBMSApplication]
GO
