SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UPDATE_BYPASS_RATE_COMPARISON_DOCUMENT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(20)	          	
	@sessionId     	varchar(20)	          	
	@imageId       	int       	          	
	@rateComparisonId	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE   PROCEDURE DBO.UPDATE_BYPASS_RATE_COMPARISON_DOCUMENT_P 

@userId varchar(20),
@sessionId varchar(20),
@imageId int,
@rateComparisonId int


AS
set nocount on
	UPDATE RC_RATE_COMPARISON SET BYPASS_IMAGE_ID=@imageId
	WHERE RC_RATE_COMPARISON_id=@rateComparisonId
GO
GRANT EXECUTE ON  [dbo].[UPDATE_BYPASS_RATE_COMPARISON_DOCUMENT_P] TO [CBMSApplication]
GO
