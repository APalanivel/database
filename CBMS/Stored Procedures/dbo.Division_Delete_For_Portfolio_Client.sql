SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******************************************************************************************************
 NAME : Division_Delete_For_Portfolio_Client   
     
 DESCRIPTION: This procedure used to aggregate  
     
 INPUT PARAMETERS:      
 Name			DataType		Default			Description      
 --------------------------------------------------------------------        
 @Message		XML
      
 OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
 --------------------------------------------------------------------      
  
  
  USAGE EXAMPLES:      
 --------------------------------------------------------------------      
	
	SP Should be executed from Service Broker 
	
	DECLARE @Message XML =  '<UnMapping_Client_From_Portfolio>
								<Client>
									<Client_Id>108</Client_Id>
									<Portfolio_Client_Id>11738</ Portfolio_Client_Id>
								</Client>
							 </UnMapping_Client_From_Portfolio>'

	EXEC Division_Delete_For_Portfolio_Client @Message
    
 AUTHOR INITIALS:      
 Initials		Name      
 -------------------------------------------------------------------       
 DSC			Kaushik
    
 MODIFICATIONS  
 Initials		Date			Modification  
 --------------------------------------------------------------------  
 DSC			06/26/2014		Created 	

******************************************************************************************************/
 CREATE PROCEDURE [dbo].[Division_Delete_For_Portfolio_Client]
 ( 
 @Message XML
 ,@Conversation_Handle UNIQUEIDENTIFIER 
 )
AS 
BEGIN

      SET NOCOUNT ON 

      DECLARE
            @Client_Id INT
           ,@Portfolio_Client_Id INT
           ,@Sitegroup_Id INT
           ,@id INT = 1
           ,@Site_Message XML


      DECLARE @Portfolio_Client_Ids TABLE
            ( 
             Portfolio_Client_Id INT )

      INSERT      INTO @Portfolio_Client_Ids
                  ( 
                   Portfolio_Client_Id )
                  SELECT
                        us.Segments
                  FROM
                        dbo.App_Config AS ac
                        CROSS APPLY dbo.ufn_split(ac.App_Config_Value, ',') AS us
                  WHERE
                        ac.App_Config_Cd = 'Portfolio_ClientHier_Management'

      SELECT
            @Client_Id = cng.ch.value('Client_Id[1]', 'INT')
           ,@Portfolio_Client_Id = cng.ch.value('Portfolio_Client_Id[1]', 'INT')
      FROM
            @Message.nodes('/UnMapping_Client_From_Portfolio/Client') cng ( ch ) 


      SELECT
            @Sitegroup_Id = s.Sitegroup_Id
      FROM
            dbo.Sitegroup AS s
      WHERE
            s.Client_Id = @Portfolio_Client_Id
            AND s.Portfolio_Client_Hier_Reference_Number = ( SELECT
                                                                  ch.Client_Hier_Id
                                                             FROM
                                                                  core.client_hier ch
                                                             WHERE
                                                                  ch.client_id = @Client_Id
                                                                  AND ch.Sitegroup_Id = 0 )

      IF EXISTS ( SELECT
                        1
                  FROM
                        @Portfolio_Client_Ids
                  WHERE
                        Portfolio_Client_Id = @Portfolio_Client_Id ) 
            BEGIN

                  IF ( @Sitegroup_Id > 0 ) 
                        BEGIN

                              DECLARE @Site_Ch TABLE
                                    ( 
                                     Id INT IDENTITY(1, 1)
                                    ,Site_CH_Id INT )

                              INSERT      INTO @Site_Ch
                                          ( 
                                           Site_Ch_Id )
									   SELECT
												ch.Portfolio_Client_Hier_Reference_Number
                                          FROM
                                                dbo.SITE s
												JOIN core.Client_Hier ch 
													ON ch.Site_Id = s.SITE_ID
                                          WHERE
                                                s.DIVISION_ID = @Sitegroup_Id



                              DECLARE @ch UNIQUEIDENTIFIER

                              WHILE EXISTS ( SELECT
                                                1
                                             FROM
                                                @Site_Ch ) 
                                    BEGIN

                                          SET @Site_Message = ( SELECT
                                                                  Site_Ch_Id AS Client_Hier_Id
                                                                 ,'D' AS Op_Code
																 ,@Portfolio_Client_Id AS Portfolio_Client_Id
                                                                FROM
                                                                  @Site_Ch
                                                                WHERE
                                                                  id = @id
																FOR
																	XML PATH('Site')
																		,ELEMENTS
																		,ROOT('Site_Info'))






                                          BEGIN DIALOG CONVERSATION @ch
												FROM SERVICE [//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping]    
												TO SERVICE '//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping'    
												ON CONTRACT [//Change_Control/Contract/Mapping_UnMapping_Site_To_Portfolio];    

                                          SEND ON CONVERSATION @ch    
												MESSAGE TYPE [//Change_Control/Message/Delete_Portfolio_Site] (@Site_Message)    

                                          DELETE
                                                @Site_Ch
                                          WHERE
                                                id = @id

                                          SET @Id = @Id + 1

                                               
                                    END 


                              DECLARE @Division_Del_Msg XML 

                              SET @Division_Del_Msg = ( SELECT
                                                            @Sitegroup_Id AS Division_Id
                                    FOR
                                                        XML PATH('Delete_Division') )



					  
							  BEGIN DIALOG CONVERSATION @ch    
								FROM SERVICE [//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping]    
								TO SERVICE '//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping'   
								ON CONTRACT [//Change_Control/Contract/Mapping_UnMapping_Client_To_Portfolio];    

                              SEND ON CONVERSATION @ch 
                              MESSAGE TYPE [//Change_Control/Message/Division_Del] (@Division_Del_Msg)
                       
				   
					   
					    END 


            END;
      END CONVERSATION @Conversation_Handle

END



;
GO
GRANT EXECUTE ON  [dbo].[Division_Delete_For_Portfolio_Client] TO [CBMSApplication]
GO
