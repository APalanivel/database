SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 
 dbo.SmartGroup_Site_SEL

 DESCRIPTION:   
 
       This procedure process the rules of a smart group and fetch the valid sites.

 INPUT PARAMETERS:  
	Name            DataType    Default     Description  
 ------------------------------------------------------------    
	@SiteGroup_id   INT                  

 OUTPUT PARAMETERS:  
	Name            DataType      Default   Description  
 ------------------------------------------------------------

 USAGE EXAMPLES:  
 ------------------------------------------------------------  


    SELECT COUNT(1) FROM Core.Client_Hier WHERE Client_Id = 235 AND Site_Id > 0 AND Country_Id = 4
	DECLARE @tvp_Cbms_Smartgroup_Rule tvp_Cbms_Smartgroup_Rule
	INSERT      INTO @tvp_Cbms_Smartgroup_Rule
	VALUES
				( 1, 100010, '4' )
	EXEC dbo.Cbms_SmartGroup_Rule_Site_Sel 
		  @Client_Id = 235
		 ,@tvp_Cbms_Smartgroup_Rule = @tvp_Cbms_Smartgroup_Rule
		 ,@Is_Inclusive = 1

	
	SELECT COUNT(1) FROM Core.Client_Hier WHERE Client_Id = 235 AND Site_Id > 0 AND Country_Id <> 4     
	DECLARE @tvp_Cbms_Smartgroup_Rule tvp_Cbms_Smartgroup_Rule
	INSERT      INTO @tvp_Cbms_Smartgroup_Rule
	VALUES
				( 1, 100011, '4' )
	EXEC dbo.Cbms_SmartGroup_Rule_Site_Sel 
		  @Client_Id = 235
		 ,@tvp_Cbms_Smartgroup_Rule = @tvp_Cbms_Smartgroup_Rule
		 ,@Is_Inclusive = 1  
	     
	
	SELECT COUNT(1) FROM Core.Client_Hier WHERE Client_Id = 235 AND Site_Id > 0 AND Country_Id = 1
	DECLARE @tvp_Cbms_Smartgroup_Rule tvp_Cbms_Smartgroup_Rule
	INSERT      INTO @tvp_Cbms_Smartgroup_Rule
	VALUES
				( 1, 100010, '1' )
	EXEC dbo.Cbms_SmartGroup_Rule_Site_Sel 
		  @Client_Id = 235
		 ,@tvp_Cbms_Smartgroup_Rule = @tvp_Cbms_Smartgroup_Rule
		 ,@Is_Inclusive = 1

	
	SELECT COUNT(1) FROM Core.Client_Hier WHERE Client_Id = 235 AND Site_Id > 0 AND Country_Id <> 1     
	DECLARE @tvp_Cbms_Smartgroup_Rule tvp_Cbms_Smartgroup_Rule
	INSERT      INTO @tvp_Cbms_Smartgroup_Rule
	VALUES
				( 1, 100011, '1' )
	EXEC dbo.Cbms_SmartGroup_Rule_Site_Sel 
		  @Client_Id = 235
		 ,@tvp_Cbms_Smartgroup_Rule = @tvp_Cbms_Smartgroup_Rule
		 ,@Is_Inclusive = 1 

	SELECT COUNT(1) FROM Core.Client_Hier WHERE Client_Id = 235 AND Site_Id > 0 AND Country_Id <> 1  AND Country_Id <> 4     
	DECLARE @tvp_Cbms_Smartgroup_Rule tvp_Cbms_Smartgroup_Rule
	INSERT      INTO @tvp_Cbms_Smartgroup_Rule
	VALUES
				( 1, 100011, '1' ),( 1, 100011, '4' )
	EXEC dbo.Cbms_SmartGroup_Rule_Site_Sel 
		  @Client_Id = 235
		 ,@tvp_Cbms_Smartgroup_Rule = @tvp_Cbms_Smartgroup_Rule
		 ,@Is_Inclusive = 1 
	     
	SELECT COUNT(1) FROM Core.Client_Hier WHERE Client_Id = 235 AND Site_Id > 0 AND Country_Id = 1  AND Country_Id = 4     
	DECLARE @tvp_Cbms_Smartgroup_Rule tvp_Cbms_Smartgroup_Rule
	INSERT      INTO @tvp_Cbms_Smartgroup_Rule
	VALUES
				( 1, 100010, '1' ),( 1, 100010, '4' )
	EXEC dbo.Cbms_SmartGroup_Rule_Site_Sel 
		  @Client_Id = 235
		 ,@tvp_Cbms_Smartgroup_Rule = @tvp_Cbms_Smartgroup_Rule
		 ,@Is_Inclusive = 1 

       
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR          Raghu Reddy

MODIFICATIONS
	Initials	Date          Modification
------------------------------------------------------------
	RR          2018-09-28    Create for Global Risk Management
******/

CREATE PROCEDURE [dbo].[Cbms_SmartGroup_Rule_Site_Sel]
    (
        @Client_Id INT
        , @tvp_Cbms_Smartgroup_Rule dbo.tvp_Cbms_Smartgroup_Rule READONLY
        , @Is_Inclusive BIT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Rule_Count SMALLINT;

        DECLARE @Tbl_Rules TABLE
              (
                  Rule_Id INT IDENTITY(1, 1)
                  , Cbms_Rule_Filter_Id INT NOT NULL
                  , Filter_Condition_Id INT NOT NULL
                  , Filter_Value VARCHAR(50)
              );

        DECLARE
            @CurRuleId INT
            , @Condition VARCHAR(50)
            , @SourceValue VARCHAR(50)
            , @TargetTest VARCHAR(50)
            , @Stmt VARCHAR(8000)
            , @WClause VARCHAR(4000)
            , @Group_Id INT;


        CREATE TABLE #SiteList
             (
                 Site_id INT
             );


        INSERT  @Tbl_Rules
             (
                 Cbms_Rule_Filter_Id
                 , Filter_Condition_Id
                 , Filter_Value
             )
        SELECT
            tvp.Cbms_Rule_Filter_Id
            , tvp.Filter_Condition_Id
            , tvp.Filter_Value
        FROM
            @tvp_Cbms_Smartgroup_Rule tvp;



        SELECT  @Rule_Count = MAX(Rule_Id)FROM  @Tbl_Rules;

        SELECT  @CurRuleId = MIN(Rule_Id)FROM   @Tbl_Rules;

        WHILE (@CurRuleId <= @Rule_Count)
            BEGIN

                SET @Stmt = '';
                SET @WClause = '';
                SET @Condition = '';
                SET @SourceValue = '';
                SET @TargetTest = '';


                SELECT
                    @SourceValue = MAX(CASE WHEN fp.Param_Name = 'SourceValue' THEN fp.Default_Value
                                       END)
                    , @Stmt = MAX(cfc.Base_Stmt)
                FROM
                    dbo.Cbms_Filter_Class cfc
                    INNER JOIN dbo.Cbms_Rule_Filter rf
                        ON cfc.Cbms_Filter_Class_Id = rf.Cbms_Filter_Class_Id
                    INNER JOIN dbo.Cbms_Filter_Param fp
                        ON rf.Cbms_Rule_Filter_Id = fp.Cbms_Rule_Filter_Id
                    INNER JOIN @Tbl_Rules tvp
                        ON tvp.Cbms_Rule_Filter_Id = fp.Cbms_Rule_Filter_Id
                    INNER JOIN dbo.Code cd
                        ON cd.Code_Id = tvp.Filter_Condition_Id
                WHERE
                    tvp.Rule_Id = @CurRuleId;

                SELECT
                    @Condition = cd.Code_Value
                FROM
                    @Tbl_Rules tvp
                    INNER JOIN dbo.Code cd
                        ON cd.Code_Id = tvp.Filter_Condition_Id
                WHERE
                    tvp.Rule_Id = @CurRuleId;

                SELECT
                    @TargetTest = tvp.Filter_Value
                FROM
                    @Tbl_Rules tvp
                    INNER JOIN dbo.Code cd
                        ON cd.Code_Id = tvp.Filter_Condition_Id
                WHERE
                    tvp.Rule_Id = @CurRuleId;



                SELECT
                    @WClause = ' CH.Client_Id = ' + CAST(@Client_Id AS VARCHAR(10)) + ' AND ' + @SourceValue
                               + @Condition + @TargetTest;


                SET @WClause = @WClause + ' GROUP BY CH.Site_Id';

                SET @Stmt = 'INSERT INTO #SiteList ' + @Stmt + @WClause;



                EXEC (@Stmt);

                SELECT  @CurRuleId = @CurRuleId + 1;

            END; -- End for Rule WHILE Loop

        IF @Is_Inclusive = 1
            BEGIN

                SELECT
                    sl.Site_id
                    , ch.Site_name
                    , ch.Sitegroup_Id
                    , ch.Client_Id
                FROM
                    #SiteList sl
                    INNER JOIN Core.Client_Hier ch
                        ON sl.Site_id = ch.Site_Id
                WHERE
                    sl.Site_id > 0
                GROUP BY
                    sl.Site_id
                    , ch.Site_name
                    , ch.Sitegroup_Id
                    , ch.Client_Id
                HAVING
                    COUNT(1) >= @Rule_Count;    -- All: Must be Equal | Not Needed for ANY

            END;
        ELSE
            BEGIN

                SELECT
                    sl.Site_id
                    , ch.Site_name
                    , ch.Sitegroup_Id
                    , ch.Client_Id
                FROM
                    #SiteList sl
                    INNER JOIN Core.Client_Hier ch
                        ON sl.Site_id = ch.Site_Id
                WHERE
                    sl.Site_id > 0
                GROUP BY
                    sl.Site_id
                    , ch.Site_name
                    , ch.Sitegroup_Id
                    , ch.Client_Id;

            END;

        DROP TABLE #SiteList;


    END;



GO
GRANT EXECUTE ON  [dbo].[Cbms_SmartGroup_Rule_Site_Sel] TO [CBMSApplication]
GO
