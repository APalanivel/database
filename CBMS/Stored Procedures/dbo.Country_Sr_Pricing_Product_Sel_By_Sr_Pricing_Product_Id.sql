SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Country_Sr_Pricing_Product_Sel_By_Sr_Pricing_Product_Id]
           
DESCRIPTION:             
			To get mapped countries to SR pricing product
			
INPUT PARAMETERS:            
	Name			DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Country_Id		INT			NULL
    @Commodity_Id	INT			NULL


OUTPUT PARAMETERS:
	Name			DataType		Default		Description  
---------------------------------------------------------------------------------  


 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT * FROM dbo.SR_PRICING_PRODUCT
	
		EXEC dbo.Country_Sr_Pricing_Product_Sel_By_Sr_Pricing_Product_Id 29
		
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2015-09-22	Global Sourcing - Phase2 -Created
******/
CREATE PROCEDURE [dbo].[Country_Sr_Pricing_Product_Sel_By_Sr_Pricing_Product_Id]
      (
       @SR_PRICING_PRODUCT_ID INT )
AS
BEGIN

      SET NOCOUNT ON;

      SELECT
            cspp.SR_Pricing_Product_Id
           ,con.COUNTRY_ID
           ,con.COUNTRY_NAME
      FROM
            dbo.SR_Pricing_Product_Country_Map cspp
            INNER JOIN dbo.COUNTRY con
                  ON cspp.Country_Id = con.COUNTRY_ID
      WHERE
            cspp.SR_Pricing_Product_Id = @SR_PRICING_PRODUCT_ID;
            
END;
GO
GRANT EXECUTE ON  [dbo].[Country_Sr_Pricing_Product_Sel_By_Sr_Pricing_Product_Id] TO [CBMSApplication]
GO
