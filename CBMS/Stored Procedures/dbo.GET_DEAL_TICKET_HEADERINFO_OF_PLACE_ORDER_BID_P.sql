SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_DEAL_TICKET_HEADERINFO_OF_PLACE_ORDER_BID_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@dealTicketId  	int       	          	
	@isManageBid   	bit       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE dbo.GET_DEAL_TICKET_HEADERINFO_OF_PLACE_ORDER_BID_P

@userId varchar(10),
@sessionId varchar(20),
@dealTicketId int,
@isManageBid bit 

AS
set nocount on
	SELECT 
	rdtcp.RM_COUNTERPARTY_ID as RM_COUNTERPARTY_ID,
	rcp.COUNTERPARTY_NAME as COUNTERPARTY_NAME,
	rdtcp.COUNTERPARTY_CONTACT_NAME as COUNTERPARTY_CONTACT_NAME,
	rdtcp.COUNTERPARTY_CONTACT_PHONE as COUNTERPARTY_CONTACT_PHONE,
	rdtcp.COUNTERPARTY_CONTACT_CELL as COUNTERPARTY_CONTACT_CELL,
	rdtcp.COUNTERPARTY_CONTACT_FAX as COUNTERPARTY_CONTACT_FAX,
	rdtcp.COUNTERPARTY_CONTACT_EMAIL as COUNTERPARTY_CONTACT_EMAIL


FROM 
	RM_DEAL_TICKET_COUNTER_PARTY rdtcp,
	RM_COUNTERPARTY rcp 

WHERE 
	rdtcp.RM_DEAL_TICKET_ID = @dealTicketId and 
	rdtcp.IS_MANAGE_BID = @isManageBid and
        rdtcp.RM_COUNTERPARTY_ID = rcp.RM_COUNTERPARTY_ID
GO
GRANT EXECUTE ON  [dbo].[GET_DEAL_TICKET_HEADERINFO_OF_PLACE_ORDER_BID_P] TO [CBMSApplication]
GO
