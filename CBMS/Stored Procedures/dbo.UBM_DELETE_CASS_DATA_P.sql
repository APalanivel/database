SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.UBM_DELETE_CASS_DATA_P
	AS
	set nocount on	
	delete UBM_CASS_BILL_CHARGES where ubm_batch_master_log_id is null
	delete UBM_CASS_METER_ACTUALS where ubm_batch_master_log_id is null
	DELETE UBM_CASS_SERVICE where ubm_batch_master_log_id is null
	delete UBM_CASS_CHECK where ubm_batch_master_log_id is null
	delete UBM_CASS_BILL where ubm_batch_master_log_id is null
	DELETE UBM_CASS_ACCOUNT where ubm_batch_master_log_id is null
	delete UBM_CASS_BUILDING where ubm_batch_master_log_id is null
	delete UBM_CASS_FACILITY where ubm_batch_master_log_id is null
	delete UBM_CASS_CUSTOMER where ubm_batch_master_log_id is null
	delete UBM_CASS_FACILITY_GROUP where ubm_batch_master_log_id is null
	delete UBM_CASS_IMAGES_CONTROL where ubm_batch_master_log_id is null
	delete UBM_CASS_METER where ubm_batch_master_log_id is null
	delete UBM_CASS_TABLE_COUNT where ubm_batch_master_log_id is null
	delete UBM_CASS_VENDOR where ubm_batch_master_log_id is null
	delete UBM_CASS_WEATHER where ubm_batch_master_log_id is null
GO
GRANT EXECUTE ON  [dbo].[UBM_DELETE_CASS_DATA_P] TO [CBMSApplication]
GO
