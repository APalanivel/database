SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



--select * from entity
--EXEC dbo.UBM_GET_BATCHLOG_FILES_P '1','1'



CREATE      PROCEDURE dbo.UBM_GET_BATCHLOG_FILES_P2
@userId varchar,
@sessionId varchar
as
	set nocount on
	select	top 100 UBM_BATCH_MASTER_LOG_ID BATCH_ID, ubm.UBM_NAME UBM_NAME, START_DATE, END_DATE ,ENTITY_NAME STATUS
	FROM 
		UBM_BATCH_MASTER_LOG master_log,UBM ubm, ENTITY entity
	where 	
		master_log.UBM_ID=ubm.UBM_ID AND 
		entity.entity_id=master_log.STATUS_TYPE_ID
order by UBM_BATCH_MASTER_LOG_ID desc

GO
GRANT EXECUTE ON  [dbo].[UBM_GET_BATCHLOG_FILES_P2] TO [CBMSApplication]
GO
