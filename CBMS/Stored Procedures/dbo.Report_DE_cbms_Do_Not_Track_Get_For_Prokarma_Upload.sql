SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
      

/******
NAME:
	 dbo.Report_DE_cbms_Do_Not_Track_Get_For_Prokarma_Upload
		
DESCRIPTION:
	Query to Get the Active Supplier Contractsfor Non North Region
	query built to accomodate request from jeff sullivan,to look for active supplier contracts and have the query run every monday   

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
EXEC dbo.Report_DE_cbms_Do_Not_Track_Get_For_Prokarma_Upload



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	AKR        Ashok Kumar Raju    

MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	 3/10/2009    DMR    
    Eliminated Union All and entire second query.  This was producing duplicates and no data from the     
    Account table was actually used.  This modification produced the same result set as removing just    
    the "all" from the union query and performs significantly faster.    
    11/2/2010 PD  
    Added WHERE clause to eliminate AT&T data  
	AKR        2012-05-23  Modified to Fit to Standards
	
******/

CREATE PROCEDURE dbo.Report_DE_cbms_Do_Not_Track_Get_For_Prokarma_Upload
AS 
BEGIN

      SET NOCOUNT ON 

      SELECT
            isnull(x.client_name, '') AS Client
           ,isnull(x.client_city, '') AS City
           ,isnull(st.state_name, '') AS State
           ,isnull(x.account_number, '') AS [Account Number]
           ,isnull(x.vendor_name, '') AS Vendor
      FROM
            dbo.do_not_track x
            JOIN dbo.state st
                  ON st.state_id = x.state_id
      WHERE
            x.CLIENT_NAME != 'AT&T Services, Inc.'
      GROUP BY
            x.client_name
           ,x.client_city
           ,st.state_name
           ,x.account_number
           ,x.vendor_name
            
END


;

GO
GRANT EXECUTE ON  [dbo].[Report_DE_cbms_Do_Not_Track_Get_For_Prokarma_Upload] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_cbms_Do_Not_Track_Get_For_Prokarma_Upload] TO [CBMSApplication]
GO
