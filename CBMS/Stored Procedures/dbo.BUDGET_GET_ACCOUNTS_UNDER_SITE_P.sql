SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.BUDGET_GET_ACCOUNTS_UNDER_SITE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@user_id       	varchar(10)	          	
	@session_id    	varchar(20)	          	
	@site_id       	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE  PROCEDURE dbo.BUDGET_GET_ACCOUNTS_UNDER_SITE_P
	@user_id varchar(10),
	@session_id varchar(20),
	@site_id int
	AS
begin
	set nocount on
		select account_id , account_number
		from account 
		where site_id = @site_id
	end
GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_ACCOUNTS_UNDER_SITE_P] TO [CBMSApplication]
GO
