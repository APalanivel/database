SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--select * from entity where entity_name like 'kW'
--exec dbo.SR_RFP_LP_GET_DETERMINANTS_P '1','1',1400,101

CREATE  PROCEDURE dbo.SR_RFP_LP_GET_DETERMINANTS_P
	@user_id varchar(10),
	@session_id varchar(20),
	@rfp_account_id int,
	@unit_entity_type int
	AS
	set nocount on
	select	determinant.sr_rfp_load_profile_determinant_id,
		determinant.determinant_name,
		en_unit.entity_name as unit 
	from 	sr_rfp_load_profile_determinant determinant(nolock),
		sr_rfp_load_profile_setup setup(nolock),
		entity en_unit(nolock),
		entity en(nolock),
		sr_rfp_account rfp_account(nolock)
	where	setup.sr_rfp_account_id = @rfp_account_id
		and rfp_account.sr_rfp_account_id = setup.sr_rfp_account_id
		and rfp_account.is_deleted = 0
		and determinant.sr_rfp_load_profile_setup_id = setup.sr_rfp_load_profile_setup_id
		and determinant.is_checked = 1
		and determinant.determinant_type_id = en.entity_id
		and en.entity_type = 1030 and (en.entity_name = 'RFP Level' or en.entity_name = 'Account Level')
		and determinant.determinant_unit_type_id = en_unit.entity_id
		and en_unit.entity_type = @unit_entity_type

	order by determinant.sr_rfp_load_profile_determinant_id
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_LP_GET_DETERMINANTS_P] TO [CBMSApplication]
GO
