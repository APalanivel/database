SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********          
NAME:          
  dbo.Report_JoinedFile_ATT_All    
    
DESCRIPTION:     
 Client Info, Accounts, Meters, Contract, Utility , Rate data  
    
    
INPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
 region_name  
  
  
OUTPUT PARAMETERS:  
      Name              DataType          Default     Description  
------------------------------------------------------------   
  
USAGE EXAMPLES:  
------------------------------------------------------------  
Stress /Prod  
exec Report_JoinedFile_ATT_All   -- All Regions (almost 2 minutes to run 149,304 records)  
   
  
AUTHOR INITIALS:  
   
Initials Name  
------------------------------------------------------------  
LC    Lynn Cox  
PS   Patchava Srinivasarao   
  
Initials Date  Modification          
------------------------------------------------------------     
lec 9/14/11 - Created because had to modify the original to only show contracts prior to specified contract exprirations  
PS  2018-08-22  Added new Meter_Id column  
*/   
CREATE PROC [dbo].[Report_JoinedFile_ATT_All]
      ( 
       @region_name VARCHAR(10) = NULL )
AS 
BEGIN  
      SET NoCount ON;   
--Utility Accounts  
      WITH  UtilityAccts_CTE
              AS ( SELECT
                        Account_Number
                       ,svl.entity_Name UtilityAccountServiceLevel
                       ,ea.MODIFIED_DATE AccountCreated
                       ,AccountInvoiceNotExpected = ISNULL(CASE a.not_expected
                                                             WHEN '1' THEN 'Yes'
                                                             ELSE 'No'
                                                           END, 'Null')
                       ,AccountNotManaged = ISNULL(CASE a.not_managed
                                                     WHEN '1' THEN 'Yes'
                                                     ELSE 'No'
                                                   END, 'Null')
                       ,istyp.ENTITY_NAME UtilityAccountInvoiceSource
                       ,v.VENDOR_NAME Utility
                       ,comm.Commodity_Name CommodityType
                       ,r.RATE_NAME Rate
                       ,'NotApplicable' PurchaseMethod
                       ,a.SITE_ID
                       ,m.ADDRESS_ID
                       ,m.METER_NUMBER MeterNumber
                       ,m.METER_ID
                       ,ui.username AccountCreatedBy
                       ,vcl.Consumption_Level_Desc
                       ,IsDataEntryOnly = ISNULL(CASE a.Is_Data_Entry_Only
                                                   WHEN '1' THEN 'Yes'
                                                   ELSE 'No'
                                                 END, 'Null')
                   FROM
                        dbo.Account a
                        INNER JOIN dbo.VENDOR v
                              ON v.VENDOR_ID = a.VENDOR_ID
                        INNER JOIN dbo.Entity svl
                              ON svl.entity_id = a.service_level_Type_Id
                        INNER JOIN dbo.ENTITY_AUDIT ea
                              ON ea.ENTITY_IDENTIFIER = a.account_ID
                                 AND ea.ENTITY_ID = 491
                                 AND ea.AUDIT_TYPE = 1
                        INNER JOIN dbo.user_info ui
                              ON ui.USER_INFO_ID = ea.USER_INFO_ID
                        LEFT JOIN dbo.ENTITY istyp
                              ON istyp.ENTITY_ID = a.INVOICE_SOURCE_TYPE_ID
                        INNER JOIN dbo.METER m
                              ON m.ACCOUNT_ID = a.ACCOUNT_ID
                        INNER JOIN dbo.RATE r
                              ON r.RATE_ID = m.RATE_ID
                        INNER JOIN dbo.Commodity comm
                              ON comm.Commodity_Id = r.COMMODITY_TYPE_ID
                        INNER JOIN Account_Variance_Consumption_Level av
                              ON av.ACCOUNT_ID = a.ACCOUNT_ID
                        INNER JOIN Variance_Consumption_Level vcl
                              ON vcl.Variance_Consumption_Level_Id = av.Variance_Consumption_Level_Id
                                 AND vcl.Commodity_Id = r.COMMODITY_TYPE_ID),  
  
--Contracts    
            Contracts_CTE
              AS ( SELECT
                        c.Contract_Id ContractId
                       ,c.BASE_CONTRACT_ID BaseContractId
                       ,c.ED_CONTRACT_NUMBER ContractNumber
                       ,ContractPricingStatus = CASE WHEN lps.CONTRACT_ID IS NULL THEN 'NotBuilt'
                                                     ELSE 'Built'
                                                END
                       ,e.ENTITY_NAME ContractType
                       ,v.VENDOR_NAME ContractedVendor
                       ,c.CONTRACT_START_DATE ContractStartDate
                       ,c.CONTRACT_END_DATE ContractEndDate
                       ,c.NOTIFICATION_DAYS NotificationDays
                       ,NotificationDate = contract_end_date - ( Notification_Days + 15 )
                       ,ContractTriggerRights = ISNULL(CASE is_contract_trigger_rights
                                                         WHEN '1' THEN 'Yes'
                                                         ELSE 'No'
                                                       END, 'null')
                       ,FullRequirements = ISNULL(CASE is_contract_full_requirement
                                                    WHEN '1' THEN 'Yes'
                                                    ELSE 'No'
                                                  END, 'null')
                       ,CONTRACT_PRICING_SUMMARY
                       ,CONTRACT_COMMENTS ContractComments
                       ,cu.CURRENCY_UNIT_NAME Currency
                       ,e1.ENTITY_NAME RenewalType
                       ,a.ACCOUNT_NUMBER SupplierAccountNumber
                       ,e2.ENTITY_NAME SupplierAccountServiceLevel
                       ,e3.ENTITY_NAME SupplierAccountInvoiceSource
                       ,ui.USERNAME ContractCreatedBy
                       ,ea.MODIFIED_DATE ContractCreatedDate
                       ,m.METER_ID  
    --*/  
                   FROM
                        dbo.Contract c
                        INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                              ON samm.Contract_ID = c.CONTRACT_ID
                        INNER JOIN dbo.Account a
                              ON a.ACCOUNT_ID = samm.ACCOUNT_ID
                        INNER JOIN dbo.METER m
                              ON m.meter_ID = samm.meter_ID
                        INNER JOIN dbo.ENTITY e
                              ON e.ENTITY_ID = c.CONTRACT_TYPE_ID
                        INNER JOIN dbo.VENDOR v
                              ON v.VENDOR_ID = a.VENDOR_ID
                        INNER JOIN dbo.CURRENCY_UNIT cu
                              ON cu.CURRENCY_UNIT_ID = c.CURRENCY_UNIT_ID
                        INNER JOIN dbo.ENTITY e1
                              ON e1.ENTITY_ID = c.RENEWAL_TYPE_ID
                        INNER JOIN dbo.ENTITY e2
                              ON e2.ENTITY_ID = a.SERVICE_LEVEL_TYPE_ID
                        LEFT JOIN dbo.ENTITY e3
                              ON e3.ENTITY_ID = a.INVOICE_SOURCE_TYPE_ID
                        INNER JOIN dbo.ENTITY_AUDIT ea
                              ON ea.ENTITY_IDENTIFIER = c.CONTRACT_ID
                                 AND ea.ENTITY_ID = 494
                                 AND ea.AUDIT_TYPE = '1'
                        INNER JOIN dbo.USER_INFO ui
                              ON ui.USER_INFO_ID = ea.USER_INFO_ID
                        LEFT JOIN dbo.LOAD_PROFILE_SPECIFICATION lps
                              ON lps.CONTRACT_ID = c.CONTRACT_ID
                   WHERE
                        samm.IS_HISTORY = 0  
  
--  group by c.CONTRACT_ID  
                        )
            --SiteList  
SELECT
      Client_Name [ClientName]
     ,c.CLIENT_ID [ClientID]
     ,ClientNotManaged = ISNULL(CASE c.not_managed
                                  WHEN '1' THEN 'Yes'
                                  ELSE 'No'
                                END, 'Null')
     ,Division_Name
     ,s.Division_Id [DivisionID]
     ,DivisionNotManaged = ISNULL(CASE d.not_managed
                                    WHEN '1' THEN 'Yes'
                                    ELSE 'No'
                                  END, 'Null')
     ,Site_Name [SiteName]
     ,s.Site_Id [SiteID]
     ,ea.MODIFIED_DATE SiteCreated
     ,s.SITE_REFERENCE_NUMBER [SiteRef.]
     ,rg.GROUP_NAME SiteRMGroup
     ,SiteNotManaged = ISNULL(CASE s.not_managed
                                WHEN '1' THEN 'Yes'
                                ELSE 'No'
                              END, 'Null')
     ,SiteClosed = ISNULL(CASE s.closed
                            WHEN '1' THEN 'Yes'
                            ELSE 'No'
                          END, 'Null')
     ,sty.ENTITY_NAME SiteType
     ,addr.ADDRESS_LINE1 [AddressLine1]
     ,addr.ADDRESS_LINE2 [AddressLine2]
     ,addr.CITY City
     ,st.STATE_NAME [State_Province]
     ,addr.ZIPCODE [ZipCode]
     ,Ctry.COUNTRY_NAME [Country]
     ,reg.REGION_NAme [Region]
     ,uacte.ACCOUNT_NUMBER [AccountNumber]
     ,uacte.UtilityAccountServiceLevel
     ,uacte.AccountCreated
     ,uacte.AccountInvoiceNotExpected
     ,uacte.AccountNotManaged
     ,uacte.UtilityAccountInvoiceSource
     ,uacte.Utility
     ,uacte.CommodityType
     ,uacte.Rate
     ,uacte.MeterNumber
     ,uacte.PurchaseMethod
     ,cc.ContractID
     ,cc.ContractNumber
     ,cc.ContractPricingStatus
     ,cc.ContractType
     ,cc.ContractedVendor
     ,cc.ContractStartDate
     ,cc.ContractEndDate
     ,NotificationDays
     ,NotificationDate
     ,ContractTriggerRights
     ,FullRequirements
     ,Contract_Pricing_Summary
     ,ContractComments
     ,Currency
     ,RenewalType
     ,SupplierAccountNumber
     ,SupplierAccountServiceLevel
     ,SupplierAccountInvoiceSource
     ,ContractCreatedBy
     ,ContractCreatedDate
     ,uacte.AccountCreatedBy
     ,Consumption_Level_Desc
     ,IsDataEntryOnly  
    --,addr.ADDRESS_ID  
     ,uacte.Meter_Id AS Meter_Id
FROM
      dbo.Site s
      JOIN dbo.Division d
            ON d.Division_Id = s.Division_Id
      JOIN dbo.Client c
            ON c.Client_Id = s.Client_Id
      JOIN dbo.Address addr
            ON addr.Address_Parent_Id = s.Site_Id
      JOIN dbo.State st
            ON st.State_Id = addr.State_Id
      JOIN dbo.Country Ctry
            ON Ctry.Country_Id = st.Country_Id
      JOIN dbo.Region reg
            ON reg.Region_Id = st.Region_Id
      JOIN dbo.ENTITY_AUDIT ea
            ON ea.ENTITY_IDENTIFIER = s.SITE_ID
               AND ea.ENTITY_ID = 506
               AND ea.AUDIT_TYPE = 1
      LEFT JOIN dbo.RM_GROUP_SITE_MAP rgm
            ON rgm.SITE_ID = s.SITE_ID
      LEFT JOIN dbo.RM_GROUP rg
            ON rg.RM_GROUP_ID = rgm.rm_group_id
      JOIN dbo.ENTITY sty
            ON sty.ENTITY_ID = s.SITE_TYPE_ID
      LEFT JOIN UtilityAccts_CTE uacte
            ON uacte.SITE_ID = s.SITE_ID
               AND uacte.ADDRESS_ID = addr.ADDRESS_ID
      LEFT JOIN Contracts_CTE cc
            ON cc.meter_id = uacte.meter_id
WHERE
      ( REG.REGION_NAME = @region_name
        OR @region_name IS NULL )
      AND c.CLIENT_ID = 11231
GROUP BY
      Client_Name
     ,c.CLIENT_ID
     ,c.not_managed
     ,Division_Name
     ,s.Division_Id
     ,d.not_managed
     ,Site_Name
     ,s.Site_Id
     ,ea.MODIFIED_DATE
     ,s.SITE_REFERENCE_NUMBER
     ,rg.GROUP_NAME
     ,s.not_managed
     ,s.closed
     ,sty.ENTITY_NAME
     ,addr.ADDRESS_LINE1
     ,addr.ADDRESS_LINE2
     ,addr.CITY
     ,st.STATE_NAME
     ,addr.ZIPCODE
     ,Ctry.COUNTRY_NAME
     ,reg.REGION_NAME
     ,uacte.ACCOUNT_NUMBER
     ,uacte.UtilityAccountServiceLevel
     ,uacte.AccountCreated
     ,uacte.AccountInvoiceNotExpected
     ,uacte.AccountNotManaged
     ,uacte.UtilityAccountInvoiceSource
     ,uacte.Utility
     ,uacte.CommodityType
     ,uacte.Rate
     ,uacte.MeterNumber
     ,uacte.PurchaseMethod
     ,cc.ContractID
     ,BaseContractId
     ,cc.ContractNumber
     ,cc.ContractPricingStatus
     ,cc.ContractType
     ,cc.ContractedVendor
     ,cc.ContractStartDate
     ,cc.ContractEndDate
     ,NotificationDays
     ,NotificationDate
     ,ContractTriggerRights
     ,FullRequirements
     ,Contract_Pricing_Summary
     ,ContractComments
     ,Currency
     ,RenewalType
     ,SupplierAccountNumber
     ,SupplierAccountServiceLevel
     ,SupplierAccountInvoiceSource
     ,ContractCreatedBy
     ,ContractCreatedDate
     ,addr.ADDRESS_ID
     ,uacte.AccountCreatedBy
     ,Consumption_Level_Desc
     ,IsDataEntryOnly
     ,uacte.Meter_Id  
END   
   
GO
GRANT EXECUTE ON  [dbo].[Report_JoinedFile_ATT_All] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_JoinedFile_ATT_All] TO [CBMSApplication]
GO
