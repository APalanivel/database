SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
NAME:  
 [cbmsCuInvoiceDeterminant_GetForInvoiceUnmappedSubBuckets]
  
DESCRIPTION:  
  
  
INPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 @cu_invoice_id  int                     
  
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
  
 EXEC dbo.[cbmsCuInvoiceDeterminant_GetForInvoiceUnmappedSubBuckets] 1187403  
  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
HKT    Harish Kumar Reddy Tirumandyam 
 
MODIFICATIONS  
  
 Initials Date  Modification  
------------------------------------------------------------  
HKT     2020-05-27   From old cbmsCuInvoiceDeterminant_GetForInvoiceUnmappedBuckets same logic ( including Sub Buckets ) added UOM Columns 

******/
CREATE PROCEDURE [dbo].[cbmsCuInvoiceDeterminant_GetForInvoiceUnmappedSubBuckets]
(@cu_invoice_id INT)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @State_Id INT;

	SELECT TOP 1
		   @State_Id = cha.Meter_State_Id
	FROM
		   dbo.CU_INVOICE_SERVICE_MONTH cism
		   INNER JOIN Core.Client_Hier_Account cha
			   ON cism.Account_ID = cha.Account_Id
	WHERE
		   cism.CU_INVOICE_ID = @cu_invoice_id;

	SELECT
		cd.COMMODITY_TYPE_ID
		, com.Commodity_Name commodity_type
		, cd.Bucket_Master_Id bucket_type_id
		, cd.UBM_BUCKET_CODE
		, cd.EC_Invoice_Sub_Bucket_Master_Id
		, cd.Ubm_Sub_Bucket_Code
		, cd.Bucket_Master_Id mapped_Bucket_Master_Id
		, ubdeisbm.EC_Invoice_Sub_Bucket_Master_Id mapped_EC_Invoice_Sub_Bucket_Master_Id
		, cd.UBM_UNIT_OF_MEASURE_CODE AS UBM_UOM
		, E.ENTITY_NAME AS CBMS_UOM
	FROM
		dbo.CU_INVOICE_DETERMINANT cd
		INNER JOIN dbo.CU_INVOICE ci
			ON ci.CU_INVOICE_ID = cd.CU_INVOICE_ID
		JOIN dbo.Commodity com
			ON com.Commodity_Id = cd.COMMODITY_TYPE_ID
		JOIN dbo.ENTITY AS E
			ON E.ENTITY_ID = cd.UNIT_OF_MEASURE_TYPE_ID
		LEFT OUTER JOIN dbo.UBM_BUCKET_DETERMINANT_MAP ubdm
			ON ubdm.COMMODITY_TYPE_ID = cd.COMMODITY_TYPE_ID AND ubdm.UBM_BUCKET_CODE = cd.UBM_BUCKET_CODE
		LEFT OUTER JOIN dbo.Ubm_Bucket_Determinant_Ec_Invoice_Sub_Bucket_Map ubdeisbm
			ON ubdeisbm.Ubm_Bucket_Determinant_Map_Id = ubdm.UBM_BUCKET_DETERMINANT_MAP_ID
			   AND ubdeisbm.Ubm_Sub_Bucket_Code = cd.Ubm_Sub_Bucket_Code
			   AND ubdeisbm.State_Id = @State_Id
	WHERE
		cd.CU_INVOICE_ID = @cu_invoice_id
		AND cd.UBM_BUCKET_CODE IS NOT NULL
		AND (
				cd.Bucket_Master_Id IS NOT NULL
				AND cd.Ubm_Sub_Bucket_Code IS NOT NULL
				AND cd.EC_Invoice_Sub_Bucket_Master_Id IS NULL
				AND EXISTS (
							   SELECT
								   1
							   FROM
								   dbo.EC_Invoice_Sub_Bucket_Master eisbm
								   INNER JOIN dbo.Bucket_Master bm
									   ON bm.Bucket_Master_Id = eisbm.Bucket_Master_Id
							   WHERE
								   bm.Commodity_Id = cd.COMMODITY_TYPE_ID AND eisbm.State_Id = @State_Id
						   )
			)
	GROUP BY
		cd.COMMODITY_TYPE_ID
		, com.Commodity_Name
		, cd.Bucket_Master_Id
		, cd.UBM_BUCKET_CODE
		, cd.EC_Invoice_Sub_Bucket_Master_Id
		, ubdeisbm.EC_Invoice_Sub_Bucket_Master_Id
		, cd.Ubm_Sub_Bucket_Code
		, cd.Bucket_Master_Id
		, cd.UBM_UNIT_OF_MEASURE_CODE
		, E.ENTITY_NAME
	ORDER BY
		com.Commodity_Name
		, cd.UBM_BUCKET_CODE;

END;
GO
GRANT EXECUTE ON  [dbo].[cbmsCuInvoiceDeterminant_GetForInvoiceUnmappedSubBuckets] TO [CBMSApplication]
GO
