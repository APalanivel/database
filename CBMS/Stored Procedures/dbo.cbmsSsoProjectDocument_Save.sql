SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  procedure [dbo].[cbmsSsoProjectDocument_Save]
	( @MyAccountId int
	, @project_id int
	, @document_id int
	)
AS
BEGIN

	set nocount on

	declare @RecordCount int

	  select @RecordCount = count(*)
	    from sso_project_document_map
	   where sso_project_id = @project_id
	     and sso_document_id = @document_id

	if @RecordCount = 0
	begin

		insert into sso_project_document_map
			(sso_project_id
			, sso_document_id			
			)
		values
			(@project_id
			,@document_id
			)
	end
END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoProjectDocument_Save] TO [CBMSApplication]
GO
