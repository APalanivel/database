SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.EC_Meter_Attribute_Value_Del           
              
Description:              
        To delete Data to EC_Meter_Attribute_Value table.              
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
    @EC_Meter_Attribute_Value_Id		INT
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                
	BEGIN TRAN  
	SELECT * FROM dbo.EC_Meter_Attribute_Value emav WHERE EC_Meter_Attribute_Value_Id=7
	EXEC dbo.EC_Meter_Attribute_Value_Del 
			@EC_Meter_Attribute_Value_Id =7 
	SELECT * FROM dbo.EC_Meter_Attribute_Value emav WHERE EC_Meter_Attribute_Value_Id=7
	ROLLBACK TRAN                      
             
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy               
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2015-04-22		Created For AS400.         
             
******/ 
CREATE PROCEDURE [dbo].[EC_Meter_Attribute_Value_Del]
      ( 
       @EC_Meter_Attribute_Value_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 
      
      DELETE
            emav
      FROM
            dbo.EC_Meter_Attribute_Value emav
      WHERE
            emav.EC_Meter_Attribute_Value_Id = @EC_Meter_Attribute_Value_Id
     
            
END
        



;
GO
GRANT EXECUTE ON  [dbo].[EC_Meter_Attribute_Value_Del] TO [CBMSApplication]
GO
