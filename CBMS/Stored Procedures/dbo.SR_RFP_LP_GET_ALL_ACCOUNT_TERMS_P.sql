
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                 
                             
 NAME:  dbo.SR_RFP_LP_GET_ALL_ACCOUNT_TERMS_P                          
                                
 DESCRIPTION:                                
                                 
                                
 INPUT PARAMETERS:                
                               
 Name		DataType   Default     Description                
-----------------------------------------------------------------------------------    
@rfp_id		INT
                                      
 OUTPUT PARAMETERS:                     
                                
 Name     DataType   Default     Description                
-----------------------------------------------------------------------------------    
                                
 USAGE EXAMPLES:                
-----------------------------------------------------------------------------------    
    
  EXEC  SR_RFP_LP_GET_ALL_ACCOUNT_TERMS_P 358
    
    
 AUTHOR INITIALS:                
               
 Initials		Name                
-----------------------------------------------------------------------------------    
 NR				Narayana Reddy                                      
                                 
 MODIFICATIONS:              
               
 Initials               Date             Modification              
-----------------------------------------------------------------------------------    
 NR                     2016-03-31      Added Header and max of no_of_months added for GCS-701.                              
                               
******/   
 

CREATE  PROCEDURE [dbo].[SR_RFP_LP_GET_ALL_ACCOUNT_TERMS_P] ( @rfp_id INT )
AS 
SET nocount ON

BEGIN

      SELECT
            account_term.sr_account_group_id AS sr_rfp_account_id
           ,MIN(account_term.from_month) AS from_month
           ,MAX(account_term.to_month) AS to_month
           ,MAX(account_term.NO_OF_MONTHS) AS no_of_months
      FROM
            sr_rfp_account_term account_term
           ,sr_rfp_term term
           ,sr_rfp_account rfp_account
      WHERE
            term.sr_rfp_id = @rfp_id
            AND account_term.sr_rfp_term_id = term.sr_rfp_term_id
            AND rfp_account.sr_rfp_account_id = account_term.sr_account_group_id
            AND account_term.is_bid_group = 0
            AND rfp_account.is_deleted = 0
            AND account_term.is_sdp = 0
      GROUP BY
            account_term.sr_account_group_id
      UNION
      SELECT
            rfp_account.sr_rfp_account_id AS sr_rfp_account_id
           ,MIN(account_term.from_month) AS from_month
           ,MAX(account_term.to_month) AS to_month
           ,MAX(account_term.NO_OF_MONTHS) AS no_of_months
      FROM
            sr_rfp_account_term account_term
           ,sr_rfp_term term
           ,sr_rfp_account rfp_account
      WHERE
            term.sr_rfp_id = @rfp_id
            AND account_term.sr_rfp_term_id = term.sr_rfp_term_id
            AND rfp_account.sr_rfp_bid_group_id = account_term.sr_account_group_id
            AND account_term.is_bid_group = 1
            AND rfp_account.is_deleted = 0
            AND account_term.is_sdp = 0
      GROUP BY
            rfp_account.sr_rfp_account_id
      
END

;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_LP_GET_ALL_ACCOUNT_TERMS_P] TO [CBMSApplication]
GO
