
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
		dbo.CU_Invoice_Charge_Type_Merge

DESCRIPTION:


INPUT PARAMETERS:
	Name				DataType				Default				Description
---------------------------------------------------------------------------------------
	@Invoice_Charge		CU_Invoice_Charge_Type (its a type as table to accept the input from user as table type)

OUTPUT PARAMETERS:
	Name				DataType				Default				Description
---------------------------------------------------------------------------------------

USAGE EXAMPLES:
---------------------------------------------------------------------------------------
	
	
	
	
AUTHOR INITIALS:
	Initials	Name
---------------------------------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal
	NR			Narayana Reddy
	
MODIFICATIONS

	Initials	Date		Modification
---------------------------------------------------------------------------------------
	        	06/15/2010	Created
	        	06/21/2010	Added the out put clause so that for child proc we can eliminate join with base tables
	        	06/24/2010	Removed two coulumns from update statement (UBM_SERVICE_TYPE_ID,UBM_SERVICE_CODE)
	DMR			09/04/2012	Modified Output to Table Variable then output Table Variable This is to allow Triggers to base tables.	
	NR			2015-06-04	For AS400 Added new input parameter EC_Invoice_Sub_Bucket_Master_Id to TVP CU_Invoice_Charge_Type.											
******/
CREATE PROCEDURE [dbo].[CU_Invoice_Charge_Type_Merge]
      ( 
       @Invoice_Charge AS CU_Invoice_Charge_Type READONLY )
AS 
BEGIN
      SET NOCOUNT ON;
      DECLARE @OutTable TABLE
            ( 
             CU_INVOICE_CHARGE_ID INT NULL
            ,CU_DETERMINANT_CODE VARCHAR(55) NULL )
      

      --DELETEING THE RECORD FROM CHILD TABLE AS WE HAVE TO DELETE THE RECORD FROM PARENT TABLE
      DELETE
            cica
      FROM
            @Invoice_Charge cic
            INNER JOIN dbo.CU_Invoice_Charge_Account cica
                  ON cica.CU_INVOICE_CHARGE_ID = cic.CU_INVOICE_CHARGE_ID
      WHERE
            cic.Is_Delete = 1
                  
 
      MERGE INTO CU_Invoice_Charge AS tgt
            USING 
                  ( SELECT
                        CU_INVOICE_CHARGE_ID
                       ,CU_INVOICE_ID
                       ,COMMODITY_TYPE_ID
                       ,UBM_SERVICE_TYPE_ID
                       ,UBM_SERVICE_CODE
                       ,UBM_BUCKET_CODE
                       ,CHARGE_NAME
                       ,CHARGE_VALUE
                       ,UBM_INVOICE_DETAILS_ID
                       ,CU_DETERMINANT_CODE
                       ,Bucket_Master_Id
                       ,Is_Delete
                       ,EC_Invoice_Sub_Bucket_Master_Id
                    FROM
                        @Invoice_Charge ) AS src
            ON tgt.CU_INVOICE_CHARGE_ID = src.CU_INVOICE_CHARGE_ID
            WHEN MATCHED AND Is_Delete = 0
                  THEN UPDATE
                    SET 
                        tgt.CU_INVOICE_ID = src.CU_INVOICE_ID
                       ,tgt.COMMODITY_TYPE_ID = src.COMMODITY_TYPE_ID
                       ,tgt.UBM_BUCKET_CODE = src.UBM_BUCKET_CODE
                       ,tgt.CHARGE_NAME = src.CHARGE_NAME
                       ,tgt.CHARGE_VALUE = src.CHARGE_VALUE
                       ,tgt.UBM_INVOICE_DETAILS_ID = src.UBM_INVOICE_DETAILS_ID
                       ,tgt.CU_DETERMINANT_CODE = src.CU_DETERMINANT_CODE
                       ,tgt.Bucket_Master_Id = src.Bucket_Master_Id
                       ,tgt.EC_Invoice_Sub_Bucket_Master_Id = src.EC_Invoice_Sub_Bucket_Master_Id
            WHEN MATCHED AND Is_Delete = 1
                  THEN DELETE
            WHEN NOT MATCHED 
                  THEN INSERT
                        ( 
                         CU_INVOICE_ID
                        ,COMMODITY_TYPE_ID
                        ,UBM_SERVICE_TYPE_ID
                        ,UBM_SERVICE_CODE
                        ,UBM_BUCKET_CODE
                        ,CHARGE_NAME
                        ,CHARGE_VALUE
                        ,UBM_INVOICE_DETAILS_ID
                        ,CU_DETERMINANT_CODE
                        ,Bucket_Master_Id
                        ,EC_Invoice_Sub_Bucket_Master_Id )
                    VALUES
                        ( 
                         src.CU_INVOICE_ID
                        ,src.COMMODITY_TYPE_ID
                        ,src.UBM_SERVICE_TYPE_ID
                        ,src.UBM_SERVICE_CODE
                        ,src.UBM_BUCKET_CODE
                        ,src.CHARGE_NAME
                        ,src.CHARGE_VALUE
                        ,src.UBM_INVOICE_DETAILS_ID
                        ,src.CU_DETERMINANT_CODE
                        ,src.Bucket_Master_Id
                        ,src.EC_Invoice_Sub_Bucket_Master_Id )
            OUTPUT
                  INSERTED.CU_INVOICE_CHARGE_ID
                 ,INSERTED.CU_DETERMINANT_CODE
                  INTO @OutTable
                        ( CU_INVOICE_CHARGE_ID, CU_DETERMINANT_CODE );
      SELECT
            CU_INVOICE_CHARGE_ID
           ,CU_DETERMINANT_CODE
      FROM
            @OutTable
END;

;
GO


GRANT EXECUTE ON  [dbo].[CU_Invoice_Charge_Type_Merge] TO [CBMSApplication]
GO
