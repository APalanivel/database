SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********     
NAME:    
	dbo.CBMS_ADD_SUPPLIER_DETAIL_P    

DESCRIPTION:  This procedure used to insert the records in SUPPLIER_DETAIL table 

IINPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
@vendor_id              int
@is_base_agreement      bit
@is_supplier_credit     bit
@comments               varchar(4000)
@is_minority            bit  

    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    

    
USAGE EXAMPLES:    
------------------------------------------------------------    
exec [dbo].[CBMS_ADD_SUPPLIER_DETAIL_P]   
   @vendor_id = 3039,  
   @is_base_agreement = 0,  
   @is_supplier_credit = 0,   
   @comments = null,  
   @is_minority = null  


Initials Name    
------------------------------------------------------------    
DR       Deana Ritter

Initials Date  Modification    
------------------------------------------------------------    

DR	8/4/009  Removed linked server updates
 DMR		  09/10/2010 Modified for Quoted_Identifier

          
*********/
CREATE PROCEDURE [dbo].[CBMS_ADD_SUPPLIER_DETAIL_P]   
@vendor_id int,  
@is_base_agreement bit,  
@is_supplier_credit bit,   
@comments varchar(4000),  
@is_minority bit  
  
AS  

  

INSERT INTO SUPPLIER_DETAIL 
(
	VENDOR_ID,
	IS_BASE_AGREEMENT,
	IS_SUPPLIER_CREDIT,
	COMMENTS,
	Is_Minority
)   
VALUES 
(
	@vendor_id, 
	@is_base_agreement, 
	@is_supplier_credit, 
	@comments, 
	@is_minority
)  

RETURN
GO
GRANT EXECUTE ON  [dbo].[CBMS_ADD_SUPPLIER_DETAIL_P] TO [CBMSApplication]
GO
