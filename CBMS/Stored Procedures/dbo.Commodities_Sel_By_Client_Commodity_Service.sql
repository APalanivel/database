SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
      
/******      
NAME:      
 dbo.Commodities_Sel_By_Client_Commodity_Service    
    
DESCRIPTION:      
 Used to get all commodity as per Client_ID & @Commodity_Service_Type    
    
INPUT PARAMETERS:      
Name      DataType  Default  Description      
------------------------------------------------------------      
  @Client_Id INT    
  @Commodity_Service_Type VARCHAR(25)           
           
OUTPUT PARAMETERS:      
Name      DataType  Default  Description      
------------------------------------------------------------      
    
USAGE EXAMPLES:      
------------------------------------------------------------    
EXEC dbo.Commodities_Sel_By_Client_Commodity_Service 108,'Invoice'    
EXEC dbo.Commodities_Sel_By_Client_Commodity_Service 218,'Invoice'    
     
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
AKR     Ashok Kumar Raju  
    
MODIFICATIONS       
Initials Date  Modification      
------------------------------------------------------------      
AKR      created, to show the commodities in specific Order.  
    
******/    
CREATE PROCEDURE dbo.Commodities_Sel_By_Client_Commodity_Service
      ( 
       @Client_Id INT
      ,@Commodity_Service_Type VARCHAR(25) )
AS 
BEGIN            
             
      SET NOCOUNT ON ;            
    
      WITH  cte_Commodity_list
              AS ( SELECT
                        cc.commodity_id
                       ,com.Commodity_Name
                       ,case WHEN com.Commodity_Name = 'Electric Power' THEN 1
                             WHEN com.Commodity_Name = 'Natural Gas' THEN 2
                             ELSE 3
                        END Sort_Order
                   FROM
                        Core.Client_Commodity cc
                        INNER JOIN dbo.Commodity com
                              ON cc.Commodity_Id = com.Commodity_Id
                        INNER JOIN Code cd
                              ON cd.Code_Id = cc.Commodity_Service_Cd
                   WHERE
                        cc.CLIENT_ID = @Client_Id
                        AND cd.Code_Value = @Commodity_Service_Type)
            SELECT
                  ccl.commodity_id
                 ,ccl.Commodity_Name
            FROM
                  cte_Commodity_list ccl
            ORDER BY
                  ccl.Sort_Order
                 ,ccl.Commodity_Name                  
END    
  
;
GO
GRANT EXECUTE ON  [dbo].[Commodities_Sel_By_Client_Commodity_Service] TO [CBMSApplication]
GO
