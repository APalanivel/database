SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:    [dbo].[CuInvoiceWorkflowActionBatchDtl]  
DESCRIPTION:  it'll Return the CBMS Home Page Exceptions Counts details for each  Queue Modules  
------------------------------------------------------------     
 INPUT PARAMETERS:      
 Name   DataType  Default Description      
 @userid int,   
 @moduleid int     
------------------------------------------------------------      
 OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
 USAGE EXAMPLES:      
   
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
AKP   ARUN KUMAR  Summit Energy   
   
 MODIFICATIONS       
 Initials Date   Modification      
------------------------------------------------------------      
 AKP    22-OCT-2019  Created  
  
******/ 
CREATE PROCEDURE [dbo].[CuInvoiceWorkflowActionBatchDtl]   
 (   
 @userid INT,   
 @moduleid INT   
 )   
 AS 
 BEGIN   
 
 SELECT   
	wabd.[Cu_Invoice_Workflow_Action_Batch_Id]  
   ,wabd.[Cu_Invoice_Id]  
   ,wabd.[Status_Cd]  
   FROM [Workflow].[Cu_Invoice_Workflow_Action_Batch_Dtl] wabd   
   INNER JOIN  
   [Workflow].[Cu_Invoice_Workflow_Action_Batch] wab ON wabd.Cu_Invoice_Workflow_Action_Batch_Id=wab.Cu_Invoice_Workflow_Action_Batch_Id  
   INNER JOIN Workflow.Workflow_Queue_Action AS WQA ON wab.Workflow_Queue_Action_Id=WQA.Workflow_Queue_Action_Id  
 WHERE wab.Requested_User_Id=@userid AND WQA.Workflow_Queue_Id =@moduleid   
 END 

GO
GRANT EXECUTE ON  [dbo].[CuInvoiceWorkflowActionBatchDtl] TO [CBMSApplication]
GO
