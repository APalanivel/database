SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                    
Name:   dbo.Invoice_Collection_Issue_upd         
                    
Description:                    
   This sproc to get the attribute details for a Given id's.            
                                 
 Input Parameters:                    
    Name            DataType   Default   Description                      
----------------------------------------------------------------------------------------                      
@Invoice_Collection_Issue_Id       INT    
@Issue_Status_Cd          INT    
@Invoice_Collection_Issue_Type_Cd      INT    
@Invoice_Collection_Issue_Entity_Owner_Cd    INT    
@Issue_Owner_User_Info_Id        INT    
@Invoice_Internal_Comment        NVARCHAR(MAX) = NULL    
@Invoice_External_Comment        NVARCHAR(MAX) = NULL    
@User_Info_Id           INT    
@CBMS_Image_Ids           VARCHAR(MAX) = NULL                         
       
 Output Parameters:                          
    Name        DataType   Default   Description                      
----------------------------------------------------------------------------------------                      
                    
 Usage Examples:                        
----------------------------------------------------------------------------------------         
  BEGIN TRAN    
  EXEC Invoice_Collection_Issue_Upd '141,142',102362,102257,49,null,null,49    
  ROLLBACK    
      
      
           
         
Author Initials:                    
    Initials  Name                    
----------------------------------------------------------------------------------------                      
 RKV    Ravi Kumar Vegesna      
 SLP    Sri Lakshmi Pallikonda
 Modifications:                    
    Initials        Date   Modification                    
----------------------------------------------------------------------------------------                      
    RKV    2016-12-29  Created For Invoice_Collection.    
    RKV    2017-09-08  Added ICQ filter while updating the Issue Events.      
 RKV    2018-05-31  Added group by clause while inserting Invoice_Collection_Queue_Id's  into table variable     
 RKV    2019-06-28  Added two new parameters as part of IC Project.    
 RKV    2019-08-14  Added Next Action Date update    
 RKV	2019-10-03  Added logic for issue autolinking 
 SLP    2019-10-16  Added the condition of @Issue_Status_Cd when @Next_Action_Date is not null
      
                   
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Issue_Upd]
(
    @Invoice_Collection_Issue_Id INT,
    @Issue_Status_Cd INT,
    @Invoice_Collection_Issue_Type_Cd INT,
    @Invoice_Collection_Issue_Entity_Owner_Cd INT,
    @Issue_Owner_User_Info_Id INT = NULL,
    @Invoice_Internal_Comment NVARCHAR(MAX) = NULL,
    @Invoice_External_Comment NVARCHAR(MAX) = NULL,
    @User_Info_Id INT,
    @CBMS_Image_Ids VARCHAR(MAX) = NULL,
    @Contact_Level_Cd INT = NULL,
    @Invoice_Collection_Queue_Id VARCHAR(MAX),
    @Is_Blocker BIT = NULL,
    @Blocker_Action_Date DATE = NULL,
    @Next_Action_Date DATE = NULL
)
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @Internal_Issue_event_Type_Cd INT,
            @External_Issue_event_Type_Cd INT,
            @Issue_Status_Change_Event_Type_Cd INT,
            @Issue_Type_Change_Event_Type_Cd INT,
            @Invoice_Collection_Activity_Id INT,
            @Invoice_Collection_Activity_Log_Id INT,
            @Ts DATETIME = GETDATE(),
            @Issue_Status_Cd_Close INT;

    DECLARE @tvp_Invoice_Collection_Queue_Account_Config_Id_Sel tvp_Invoice_Collection_Queue_Account_Config_Id_Sel;


    SELECT @Issue_Status_Cd_Close = c.Code_Id
    FROM dbo.Code c
        INNER JOIN dbo.Codeset cs
            ON cs.Codeset_Id = c.Codeset_Id
    WHERE cs.Codeset_Name = 'IC Chase Status'
          AND c.Code_Value = 'Close';

    SELECT @Internal_Issue_event_Type_Cd = MAX(   CASE
                                                      WHEN c.Code_Value = 'IC Internal Comments' THEN
                                                          c.Code_Id
                                                  END
                                              ),
           @External_Issue_event_Type_Cd = MAX(   CASE
                                                      WHEN c.Code_Value = 'IC External Comments' THEN
                                                          c.Code_Id
                                                  END
                                              ),
           @Issue_Status_Change_Event_Type_Cd = MAX(   CASE
                                                           WHEN c.Code_Value = 'Issue Status Change' THEN
                                                               c.Code_Id
                                                       END
                                                   ),
           @Issue_Type_Change_Event_Type_Cd = MAX(   CASE
                                                         WHEN c.Code_Value = 'Issue Type change' THEN
                                                             c.Code_Id
                                                     END
                                                 )
    FROM dbo.Code c
        INNER JOIN dbo.Codeset cs
            ON cs.Codeset_Id = c.Codeset_Id
    WHERE cs.Std_Column_Name = 'Invoice_Collection_Issue_Event_Type_Cd'
          AND c.Code_Value IN ( 'IC Internal Comments', 'IC External Comments', 'Issue Status Change',
                                'Issue Type change'
                              );


    SELECT @Invoice_Collection_Activity_Id = ici.Invoice_Collection_Activity_Id
    FROM dbo.Invoice_Collection_Issue_Log ici
    WHERE ici.Invoice_Collection_Issue_Log_Id = @Invoice_Collection_Issue_Id;

    DECLARE @Tb_Invoice_Collection_Queue_Id TABLE
    (
        Invoice_Collection_Queue_Id INT
    );


    INSERT INTO @Tb_Invoice_Collection_Queue_Id
    SELECT us.Segments
    FROM dbo.ufn_split(@Invoice_Collection_Queue_Id, ',') us
    GROUP BY us.Segments;

    BEGIN TRY
        BEGIN TRAN;




        INSERT INTO dbo.Invoice_Collection_Issue_Event
        (
            Invoice_Collection_Issue_Log_Id,
            Issue_Event_Type_Cd,
            Event_Desc,
            Created_User_Id,
            Created_Ts,
            Updated_User_Id
        )
        SELECT ici.Invoice_Collection_Issue_Log_Id,
               @Internal_Issue_event_Type_Cd,
               @Invoice_Internal_Comment,
               @User_Info_Id,
               @Ts,
               @User_Info_Id
        FROM dbo.Invoice_Collection_Issue_Log ici
            INNER JOIN @Tb_Invoice_Collection_Queue_Id us
                ON us.Invoice_Collection_Queue_Id = ici.Invoice_Collection_Queue_Id
        WHERE ici.Invoice_Collection_Activity_Id = @Invoice_Collection_Activity_Id
              AND @Invoice_Internal_Comment IS NOT NULL
              AND LTRIM(RTRIM(@Invoice_Internal_Comment)) <> '';


        INSERT INTO dbo.Invoice_Collection_Issue_Event
        (
            Invoice_Collection_Issue_Log_Id,
            Issue_Event_Type_Cd,
            Event_Desc,
            Created_User_Id,
            Created_Ts,
            Updated_User_Id
        )
        SELECT ici.Invoice_Collection_Issue_Log_Id,
               @External_Issue_event_Type_Cd,
               @Invoice_External_Comment,
               @User_Info_Id,
               @Ts,
               @User_Info_Id
        FROM dbo.Invoice_Collection_Issue_Log ici
            INNER JOIN @Tb_Invoice_Collection_Queue_Id us
                ON us.Invoice_Collection_Queue_Id = ici.Invoice_Collection_Queue_Id
        WHERE ici.Invoice_Collection_Activity_Id = @Invoice_Collection_Activity_Id
              AND @Invoice_External_Comment IS NOT NULL
              AND LTRIM(RTRIM(@Invoice_External_Comment)) <> '';

        INSERT INTO dbo.Invoice_Collection_Issue_Event
        (
            Invoice_Collection_Issue_Log_Id,
            Issue_Event_Type_Cd,
            Event_Desc,
            Created_User_Id,
            Created_Ts
        )
        SELECT ici.Invoice_Collection_Issue_Log_Id,
               @Issue_Type_Change_Event_Type_Cd,
               'Issue type change from ' + aic.Code_Value + ' to ' + gic.Code_Value,
               @User_Info_Id,
               @Ts
        FROM dbo.Invoice_Collection_Issue_Log ici
            INNER JOIN @Tb_Invoice_Collection_Queue_Id us
                ON us.Invoice_Collection_Queue_Id = ici.Invoice_Collection_Queue_Id
            LEFT JOIN dbo.Code aic
                ON ici.Invoice_Collection_Issue_Type_Cd = aic.Code_Id
            LEFT JOIN dbo.Code gic
                ON gic.Code_Id = @Invoice_Collection_Issue_Type_Cd
        WHERE ISNULL(ici.Invoice_Collection_Issue_Type_Cd, 0) <> @Invoice_Collection_Issue_Type_Cd
              AND ici.Invoice_Collection_Activity_Id = @Invoice_Collection_Activity_Id;


        INSERT INTO dbo.Invoice_Collection_Issue_Event
        (
            Invoice_Collection_Issue_Log_Id,
            Issue_Event_Type_Cd,
            Event_Desc,
            Created_User_Id,
            Created_Ts
        )
        SELECT ici.Invoice_Collection_Issue_Log_Id,
               @Issue_Status_Change_Event_Type_Cd,
               'Issue Status change from ' + aic.Code_Value + ' to ' + gic.Code_Value,
               @User_Info_Id,
               @Ts
        FROM dbo.Invoice_Collection_Issue_Log ici
            INNER JOIN @Tb_Invoice_Collection_Queue_Id us
                ON us.Invoice_Collection_Queue_Id = ici.Invoice_Collection_Queue_Id
            LEFT JOIN dbo.Code aic
                ON ici.Issue_Status_Cd = aic.Code_Id
            LEFT JOIN dbo.Code gic
                ON gic.Code_Id = @Issue_Status_Cd
        WHERE ici.Issue_Status_Cd <> @Issue_Status_Cd
              AND ici.Invoice_Collection_Activity_Id = @Invoice_Collection_Activity_Id
              AND aic.Code_Value <> 'Close';

        UPDATE icil
        SET icil.Invoice_Collection_Issue_Type_Cd = @Invoice_Collection_Issue_Type_Cd,
            icil.Issue_Status_Cd = @Issue_Status_Cd,
            icil.Issue_Entity_Owner_Cd = @Invoice_Collection_Issue_Entity_Owner_Cd,
            icil.Issue_Owner_User_Id = @Issue_Owner_User_Info_Id,
            icil.Updated_User_Id = @User_Info_Id,
            icil.Last_Change_Ts = @Ts,
            icil.Type_Of_Issue_Owner_Cd = @Contact_Level_Cd,
            icil.Blocker_Action_Date = CASE
                                           WHEN @Blocker_Action_Date IS NULL
                                                AND @Is_Blocker = 0 THEN
                                               @Blocker_Action_Date
                                           WHEN @Blocker_Action_Date IS NULL
                                                AND @Is_Blocker = 1 THEN
                                               icil.Blocker_Action_Date
                                           ELSE
                                               @Blocker_Action_Date
                                       END,
            icil.Is_Blocker = ISNULL(@Is_Blocker, icil.Is_Blocker)
        FROM dbo.Invoice_Collection_Issue_Log icil
            INNER JOIN @Tb_Invoice_Collection_Queue_Id us
                ON us.Invoice_Collection_Queue_Id = icil.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Code sc
                ON sc.Code_Id = icil.Issue_Status_Cd
        WHERE icil.Invoice_Collection_Activity_Id = @Invoice_Collection_Activity_Id
              AND sc.Code_Value <> 'Close';




        DELETE a
        FROM dbo.Invoice_Collection_Issue_Attachment a
        WHERE NOT EXISTS
        (
            SELECT 1
            FROM dbo.ufn_split(@CBMS_Image_Ids, ',') f
            WHERE a.Cbms_Image_Id = f.Segments
        );

        INSERT INTO dbo.Invoice_Collection_Issue_Attachment
        (
            Invoice_Collection_Issue_Log_Id,
            Cbms_Image_Id,
            Created_User_Id,
            Created_Ts
        )
        SELECT ici.Invoice_Collection_Issue_Log_Id,
               f.Segments,
               @User_Info_Id,
               @Ts
        FROM dbo.ufn_split(@CBMS_Image_Ids, ',') f
            CROSS APPLY dbo.Invoice_Collection_Issue_Log ici
            INNER JOIN @Tb_Invoice_Collection_Queue_Id us
                ON us.Invoice_Collection_Queue_Id = ici.Invoice_Collection_Queue_Id
        WHERE ici.Invoice_Collection_Activity_Id = @Invoice_Collection_Activity_Id
              AND NOT EXISTS
        (
            SELECT 1
            FROM dbo.Invoice_Collection_Issue_Attachment a
            WHERE a.Cbms_Image_Id = f.Segments
        );



        INSERT INTO dbo.Invoice_Collection_Activity_Log
        (
            Invoice_Collection_Activity_Id,
            Next_Action_Dt,
            Created_User_Id
        )
        SELECT @Invoice_Collection_Activity_Id,
               @Next_Action_Date,
               @User_Info_Id
        WHERE @Next_Action_Date IS NOT NULL
              AND @Issue_Status_Cd_Close <> @Issue_Status_Cd;

        SELECT @Invoice_Collection_Activity_Log_Id = MAX(Invoice_Collection_Activity_Log_Id)
        FROM dbo.Invoice_Collection_Activity_Log ical
        WHERE ical.Invoice_Collection_Activity_Id = @Invoice_Collection_Activity_Id
              AND @Next_Action_Date IS NOT NULL
              AND @Issue_Status_Cd_Close <> @Issue_Status_Cd;

        INSERT INTO dbo.Invoice_Collection_Activity_Log_Queue_Map
        (
            Invoice_Collection_Activity_Log_Id,
            Invoice_Collection_Queue_Id,
            Collection_Start_Dt,
            Collection_End_Dt
        )
        SELECT @Invoice_Collection_Activity_Log_Id,
               ticq.Invoice_Collection_Queue_Id,
               icq.Collection_Start_Dt,
               icq.Collection_End_Dt
        FROM @Tb_Invoice_Collection_Queue_Id ticq
            INNER JOIN dbo.Invoice_Collection_Queue icq
                ON icq.Invoice_Collection_Queue_Id = ticq.Invoice_Collection_Queue_Id
        WHERE @Next_Action_Date IS NOT NULL
              AND @Issue_Status_Cd_Close <> @Issue_Status_Cd;


        UPDATE icq
        SET icq.Next_Action_Dt = CASE
                                     WHEN @Next_Action_Date IS NOT NULL THEN
                                         @Next_Action_Date
                                     ELSE
                                         icq.Next_Action_Dt
                                 END,
            icq.Last_Change_Ts = GETDATE()
        FROM dbo.Invoice_Collection_Queue icq
            INNER JOIN @Tb_Invoice_Collection_Queue_Id ticq
                ON ticq.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id;

        EXEC dbo.Invoice_Collection_Issue_Link_Auto_By_ActivityId @Invoice_Collection_Activity_Id,
                                                                  @User_Info_Id;


        COMMIT TRAN;


    END TRY
    BEGIN CATCH

        IF @@TRANCOUNT > 0
        BEGIN
            ROLLBACK TRAN;
        END;

        EXEC dbo.usp_RethrowError;

    END CATCH;


END;




;














GO

GRANT EXECUTE ON  [dbo].[Invoice_Collection_Issue_Upd] TO [CBMSApplication]
GO
