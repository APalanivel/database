
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
Name:  
 [dbo].[SR_RFP_GET_USER_EMAIL_ADDRESS_P]
 
 Description:  
 			
 
 Input Parameters:  
  Name					DataType			Default					Description  
-------------------------------------------------------------------------------------   
 @userId				VARCHAR
 @sessionId				VARCHAR

 
 Output Parameters:  
  Name					DataType			Default					Description  
-------------------------------------------------------------------------------------   
 
 Usage Examples:
------------------------------------------------------------------------------------- 

EXEC dbo.SR_RFP_GET_USER_EMAIL_ADDRESS_P 
      '1'
     ,'1'

EXEC SR_RFP_GET_USER_EMAIL_ADDRESS_P 
      61212
     ,'5687D09C4AF43D0BD6203713478944A6'

Author Initials:  
 Initials	  Name
-------------------------------------------------------------------------------------   
  RKV		  Ravi Kumar Vegesna
 
 Modifications :  
 Initials		Date			Modification  
-------------------------------------------------------------------------------------   
  RKV			2017-11-14		SE2017-322 Added new column cc_Email_Address 
  
      
******/
CREATE   PROCEDURE [dbo].[SR_RFP_GET_USER_EMAIL_ADDRESS_P]
      @user_id VARCHAR(10)
     ,@session_id VARCHAR(20)
AS 
BEGIN 

      SET NOCOUNT ON
	
      DECLARE @CC_Email_Address VARCHAR(150)
      
      SELECT
            @CC_Email_Address = App_Config_Value
      FROM
            dbo.App_Config ac
      WHERE
            App_Config_Cd = 'Email_CC_Sourcing_Recommendation_Email'
      
      SELECT
            EMAIL_ADDRESS
           ,FIRST_NAME
           ,LAST_NAME
           ,PHONE_NUMBER
           ,@CC_Email_Address CC_Email_Address
      FROM
            USER_INFO(NOLOCK)
      WHERE
            USER_INFO_ID = @user_id
END
;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_GET_USER_EMAIL_ADDRESS_P] TO [CBMSApplication]
GO
