SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                    
NAME:   [dbo].Incorrectly_Mapped_Client_Contact       
                   
DESCRIPTION:                     
 Incorrectly_Mapped_Client_Contact    
     
INPUT PARAMETERS:                    
Name     DataType  Default   Description          
------------------------------------------------------------------------          
       
          
 OUTPUT PARAMETERS:                    
Name     DataType  Default   Description          
------------------------------------------------------------------------        
                 
 USAGE EXAMPLES:                    
------------------------------------------------------------------------                         
Execute [dbo].Incorrectly_Mapped_Client_Contact    
         
    
 AUTHOR INITIALS:                 
 Initials  Name    
------------------------------------------------------------------------    
 SP    Srinivasarao Patchava.    
    
 moDIFICATIONS:                     
 Initials  Date   Modification                    
------------------------------------------------------------------------        
 SP            29/10/2018       created.    
******/    
CREATE PROCEDURE [dbo].[Incorrectly_Mapped_Client_Contact]
AS 
BEGIN    
           
      SET NOCOUNT ON;    
            
      WITH  CTE_SQL
              AS ( SELECT
                        icac.Invoice_Collection_Account_Config_Id
                       ,icac2.Account_Id
                       ,ch.Client_Name Actual_Client_Name
                       ,c.CLIENT_NAME Wrongly_Mapped_Client_Name
                       ,ccm.Contact_Info_Id
                       ,ui.USERNAME
                       ,icac.Last_Change_Ts
                       ,ch.Site_name
                       ,cha.Account_Number AS Account_Number
                   FROM
                        dbo.Invoice_Collection_Account_Contact icac
                        INNER JOIN dbo.Client_Contact_Map ccm
                              ON ccm.Contact_Info_Id = icac.Contact_Info_Id
                        INNER JOIN dbo.CLIENT c
                              ON c.CLIENT_ID = ccm.CLIENT_ID
                        INNER JOIN dbo.Invoice_Collection_Account_Config icac2
                              ON icac2.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                        INNER JOIN Core.Client_Hier_Account cha
                              ON cha.Account_Id = icac2.Account_Id
                        INNER JOIN Core.Client_Hier ch
                              ON ch.Client_Hier_Id = cha.Client_Hier_Id
                        INNER JOIN dbo.USER_INFO ui
                              ON ui.USER_INFO_ID = icac.Updated_User_Id
                   WHERE
                        ccm.CLIENT_ID <> ch.Client_Id
                   GROUP BY
                        icac.Invoice_Collection_Account_Config_Id
                       ,icac2.Account_Id
                       ,c.CLIENT_NAME
                       ,ccm.Contact_Info_Id
                       ,ch.Client_Name
                       ,icac.Last_Change_Ts
                       ,ch.Site_name
                       ,ui.USERNAME
                       ,cha.Account_Number)
            SELECT
                  cs.Account_Number
                 ,cs.Site_name
                 ,cs.Actual_Client_Name
                 ,cs.Wrongly_Mapped_Client_Name
                 ,cs.USERNAME
                 ,cs.Last_Change_Ts AS Last_Change_Ts
            FROM
                  CTE_SQL cs
                  INNER JOIN dbo.Invoice_Collection_Account_Config icac
                        ON icac.Invoice_Collection_Account_Config_Id = cs.Invoice_Collection_Account_Config_Id
                  INNER JOIN dbo.Invoice_Collection_Queue icq
                        ON icac.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
                  LEFT OUTER JOIN ( dbo.Invoice_Collection_Chase_Log_Queue_Map icclqm
                                    INNER JOIN dbo.Invoice_Collection_Chase_Log iccl
                                          ON iccl.Invoice_Collection_Chase_Log_Id = icclqm.Invoice_Collection_Chase_Log_Id
                                    INNER JOIN dbo.Code c
                                          ON c.Code_Id = iccl.Invoice_Collection_Method_Of_Contact_Cd )
                                    ON cs.Contact_Info_Id = iccl.Contact_Info_Id
            GROUP BY
                  cs.Account_Number
                 ,cs.Site_name
                 ,cs.Actual_Client_Name
                 ,cs.Wrongly_Mapped_Client_Name
                 ,cs.USERNAME
                 ,cs.Last_Change_Ts; 
                 
              
        
        
        
END  
  

GO
GRANT EXECUTE ON  [dbo].[Incorrectly_Mapped_Client_Contact] TO [CBMS_SSRS_Reports]
GO
