SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE     PROCEDURE dbo.GET_CEM_CLIENT_DATA_P 
@userId varchar,
@sessionId varchar,
@displayStatusId int,
@cemTeamId int,
@cemUserId int

as

IF @displayStatusId=1

	select 	cl.client_name CLIENT_NAME,cl.client_id CLIENT_ID
	from 
		rm_cem_team team, 
		client_cem_map map, 
		user_info ui, 
		client cl
	where 
		team.cem_team_type_id = @cemTeamId and
		team.cem_user_id = ui.user_info_id
		and map.user_info_id = ui.user_info_id
		and map.client_id = cl.client_id

	order by cl.client_name

	/*select	 distinct 
		client.CLIENT_NAME CLIENT_NAME,client.CLIENT_ID CLIENT_ID
 	
	from	ENTITY entity,
		RM_CEM_TEAM rcem,
		USER_INFO userinfo,
		CLIENT_CEM_MAP cmap,
		CLIENT 	client	
	
	
	WHERE	entity.ENTITY_ID=rcem.CEM_TEAM_TYPE_ID AND
		rcem.CEM_USER_ID=userinfo.USER_INFO_ID  AND
		rcem.CEM_USER_ID=cmap.USER_INFO_ID AND 
		cmap.CLIENT_ID=client.CLIENT_ID AND
		entity.ENTITY_ID=@cemTeamId
	
	order by CLIENT_NAME*/

ELSE IF @displayStatusId=2

	select	 distinct 
		client.CLIENT_NAME CLIENT_NAME,client.CLIENT_ID CLIENT_ID
	
	from	CLIENT_CEM_MAP cmap,
		CLIENT 	client	
	
	
	WHERE	cmap.USER_INFO_ID=@cemUserId and
		cmap.CLIENT_ID=client.CLIENT_ID 
		
	
	order by CLIENT_NAME
GO
GRANT EXECUTE ON  [dbo].[GET_CEM_CLIENT_DATA_P] TO [CBMSApplication]
GO
