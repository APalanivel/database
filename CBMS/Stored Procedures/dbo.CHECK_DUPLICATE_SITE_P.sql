SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.CHECK_DUPLICATE_SITE_P
	@siteName varchar(200),
	@divisionID int
	AS
	begin
		set nocount on
		SELECT COUNT( 1 )  
		FROM   SITE 
		WHERE  SITE_NAME = @siteName 
		       AND DIVISION_ID = @divisionID
	end
GO
GRANT EXECUTE ON  [dbo].[CHECK_DUPLICATE_SITE_P] TO [CBMSApplication]
GO
