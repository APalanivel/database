SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  procedure [dbo].[cbmsGroupInfoPermissionInfo_Get]
	( @MyAccountId int
	, @group_info_id int
	, @permission_info_id int
	)
AS
BEGIN

	   select group_info_id
		, permission_info_id
	     from group_info_permission_info_map
	    where group_info_id = @group_info_id
	      and permission_info_id = @permission_info_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsGroupInfoPermissionInfo_Get] TO [CBMSApplication]
GO
