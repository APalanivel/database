
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                    
                          
NAME: dbo.Merge_DE_Cost_Usage_Account_Dtl_Changes                            
                               
DESCRIPTION:      
	used to Merge the data received from dvdehub cost_usage_Site_Dtl table(change_tracking data)  to Cost_Usage_Account_Dtl table                                        
	Strictly used by ETL          

INPUT PARAMETERS:                      
	NAME				DATATYPE       DEFAULT     DESCRIPTION                      
--------------------------------------------------------------                                  
	@Batch_Size        INT                 
                      
OUTPUT PARAMETERS:                      
NAME							DATATYPE	DEFAULT		DESCRIPTION                      
----------------------------------------------------------------------------                      

          
USAGE EXAMPLES:          
----------------------------------------------------------
BEGIN TRAN         
	EXEC dbo.Merge_DE_Cost_Usage_Account_Dtl_Changes   5000          
ROLLBACK TRAN          

AUTHOR INITIALS:          
INITIALS	NAME                   
----------------------------------------------------------          
AKR			Ashok Kumar raju
RR			Raghu Reddy  
NR			Narayana Reddy      
          
MODIFICATIONS          
INITIALS	DATE		MODIFICATION          
----------------------------------------------------------          
AKR			2011-11-04  Created          
AKR			2012-04-03  Modified the script for Additional Data changes     
AKR			2012-04-16  Removed the Commodity_Id for Cost_Usage_Account_Billing_Dtl    
AKR			2012-04-17  modified the code Insert data to Cost_Usage_Account_Billing_Dtl only if the @Sys_Change_Operation is I or U  
RR			2012-06-14	Corrected usage example
RR			2013-05-03	Modified the code to use batch processing instead of row by row and from stage table Stage.Cost_Usage_Account_Dtl_Transfer
						as the data source 
HG			2016-12-06	VTE enhancement , replaced the Variance_Effective_Dt with the new table created for this enhancement.
NR			2017-03-16	MAINT-5063 - Removed Is_Variance_Tested logic and replace with 1.
RKV         2017-06-22  Added new column Data_Type_Cd as part of Data enhancement.						
*/          
CREATE PROCEDURE [dbo].[Merge_DE_Cost_Usage_Account_Dtl_Changes] ( @Batch_Size INT )
AS 
BEGIN

      SET NOCOUNT ON;

      CREATE TABLE #Affected_Data
            ( 
             Client_Hier_Id INT
            ,Account_Id INT
            ,Service_Month DATETIME
            ,Bucket_Master_Id INT
            ,Action_Type VARCHAR(10) );

      DECLARE
            @Min_Cost_Usage_Site_Dtl_Transfer_Id INT = 1
           ,@Max_Cost_Usage_Site_Dtl_Transfer_Id INT
           ,@Data_Source_Cd_CBMS INT
           ,@Data_Source_Cd_DE INT;

      SELECT
            @Max_Cost_Usage_Site_Dtl_Transfer_Id = MAX(cuad.Cost_Usage_Account_Dtl_Transfer_Id)
      FROM
            Stage.Cost_Usage_Account_Dtl_Transfer cuad;      
                                   
      SELECT
            @Data_Source_Cd_CBMS = cd.Code_Id
      FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code cd
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Std_Column_Name = 'Data_Source_Cd'
            AND cd.Code_Value = 'CBMS';                       

      SELECT
            @Data_Source_Cd_DE = cd.Code_Id
      FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code cd
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Std_Column_Name = 'Data_Source_Cd'
            AND cd.Code_Value = 'DE';

      WHILE @Min_Cost_Usage_Site_Dtl_Transfer_Id <= @Max_Cost_Usage_Site_Dtl_Transfer_Id 
            BEGIN

                  MERGE INTO dbo.Cost_Usage_Account_Dtl tgt_cuad
                        USING 
                              ( SELECT
                                    cuad.Bucket_Master_Id
                                   ,cuad.Bucket_Value
                                   ,cuad.UOM_Type_Id
                                   ,cuad.CURRENCY_UNIT_ID
                                   ,cuad.Created_By_Id
                                   ,cuad.Created_Ts
                                   ,cuad.Updated_By_Id
                                   ,cuad.Updated_Ts
                                   ,cuad.Account_Id
                                   ,cuad.Client_Hier_Id
                                   ,cuad.Service_Month
                                   ,cuad.Data_Source_Cd
                                   ,cuad.Sys_Change_Operation
                                   ,cuad.Data_Type_Cd
                                FROM
                                    Stage.Cost_Usage_Account_Dtl_Transfer cuad
                                WHERE
                                    cuad.Cost_Usage_Account_Dtl_Transfer_Id BETWEEN @Min_Cost_Usage_Site_Dtl_Transfer_Id
                                                                            AND     ( @Min_Cost_Usage_Site_Dtl_Transfer_Id + @Batch_Size - 1 ) ) AS src_cuad
                        ON ( tgt_cuad.Client_Hier_Id = src_cuad.Client_Hier_Id
                             AND tgt_cuad.ACCOUNT_ID = src_cuad.Account_Id
                             AND tgt_cuad.Bucket_Master_Id = src_cuad.Bucket_Master_Id
                             AND tgt_cuad.Service_Month = src_cuad.Service_Month )
                        WHEN MATCHED AND ( src_cuad.Sys_Change_Operation = 'D'
                                           AND src_cuad.Bucket_Value IS NULL
                                           AND ( tgt_cuad.Data_Source_Cd = @Data_Source_Cd_CBMS
                                                 OR tgt_cuad.Data_Source_Cd = @Data_Source_Cd_DE ) )
                              THEN
                        DELETE
                        WHEN MATCHED AND ( tgt_cuad.Data_Source_Cd = @Data_Source_Cd_CBMS
                                           OR tgt_cuad.Data_Source_Cd = @Data_Source_Cd_DE )
                              THEN
                        UPDATE     SET
                                    tgt_cuad.Bucket_Value = src_cuad.Bucket_Value
                                   ,tgt_cuad.UOM_Type_Id = src_cuad.UOM_Type_Id
                                   ,tgt_cuad.CURRENCY_UNIT_ID = src_cuad.CURRENCY_UNIT_ID
                                   ,tgt_cuad.Updated_By_Id = ISNULL(src_cuad.Updated_By_Id, src_cuad.Created_By_Id)
                                   ,tgt_cuad.Updated_Ts = src_cuad.Updated_Ts
                                   ,tgt_cuad.Data_Source_Cd = src_cuad.Data_Source_Cd
                                   ,tgt_cuad.Data_Type_Cd = src_cuad.Data_Type_Cd
                        WHEN NOT MATCHED AND ( src_cuad.Sys_Change_Operation IN ( 'I', 'U' ) )
                              THEN
                        INSERT
                                    ( 
                                     Bucket_Master_Id
                                    ,Bucket_Value
                                    ,UOM_Type_Id
                                    ,CURRENCY_UNIT_ID
                                    ,Created_By_Id
                                    ,Created_Ts
                                    ,Updated_Ts
                                    ,Updated_By_Id
                                    ,Account_Id
                                    ,Service_Month
                                    ,Data_Source_Cd
                                    ,Client_Hier_Id
                                    ,Data_Type_Cd )
                                   VALUES
                                    ( 
                                     src_cuad.Bucket_Master_Id
                                    ,src_cuad.Bucket_Value
                                    ,src_cuad.UOM_Type_Id
                                    ,src_cuad.CURRENCY_UNIT_ID
                                    ,src_cuad.Created_By_Id
                                    ,src_cuad.Created_Ts
                                    ,src_cuad.Updated_Ts
                                    ,src_cuad.Updated_By_Id
                                    ,src_cuad.Account_Id
                                    ,src_cuad.Service_Month
                                    ,src_cuad.Data_Source_Cd
                                    ,src_cuad.Client_Hier_Id
                                    ,src_cuad.Data_Type_Cd )
                        OUTPUT
                              ISNULL(inserted.Client_Hier_Id, deleted.Client_Hier_Id)
                             ,ISNULL(inserted.ACCOUNT_ID, deleted.ACCOUNT_ID)
                             ,ISNULL(inserted.Service_Month, deleted.Service_Month)
                             ,ISNULL(inserted.Bucket_Master_Id, deleted.Bucket_Master_Id)
                             ,SUBSTRING($ACTION, 1, 1)
                              INTO #Affected_Data
                                    ( Client_Hier_Id, Account_Id, Service_Month, Bucket_Master_Id, Action_Type );         

				-- Load the data in to Queue table for Site aggregation and variance test
                  MERGE INTO Stage.Cost_Usage_Transfer_Queue tgt
                        USING 
                              ( SELECT
                                    ad.Client_Hier_Id
                                   ,ad.Account_Id
                                   ,ad.Service_Month
                                   ,0 Is_Aggregated_To_Site
							 -- WHEN the current month falls with in the DEO test or Full variane test or no configuration available then set the variance tested as false, and the variance test app picks it up and do the variance test
                                   ,1 AS Is_Variance_Tested
                                FROM
                                    #Affected_Data ad
                                    INNER JOIN dbo.Bucket_Master bm
                                          ON bm.Bucket_Master_Id = ad.Bucket_Master_Id
                                GROUP BY
                                    ad.Client_Hier_Id
                                   ,ad.Account_Id
                                   ,ad.Service_Month ) src
                        ON src.Client_Hier_Id = tgt.Client_Hier_Id
                              AND src.Account_Id = tgt.Account_Id
                              AND src.Service_Month = tgt.Service_Month
                        WHEN MATCHED 
                              THEN
                        UPDATE     SET
                                    tgt.Is_Aggregated_To_Site = src.Is_Aggregated_To_Site
                                   ,tgt.Is_Variance_Tested = src.Is_Variance_Tested
                        WHEN NOT MATCHED 
                              THEN
                        INSERT
                                    ( 
                                     Client_Hier_Id
                                    ,Account_Id
                                    ,Service_Month
                                    ,Is_Aggregated_To_Site
                                    ,Is_Variance_Tested )
                                   VALUES
                                    ( 
                                     src.Client_Hier_Id
                                    ,src.Account_Id
                                    ,src.Service_Month
                                    ,src.Is_Aggregated_To_Site
                                    ,src.Is_Variance_Tested );

                  --insert the data to Cost_Usage_Account_Billing_Dtl table
                  INSERT      INTO dbo.Cost_Usage_Account_Billing_Dtl
                              ( 
                               Account_Id
                              ,Service_Month
                              ,Billing_Start_Dt
                              ,Billing_End_Dt
                              ,Billing_Days )
                              SELECT
                                    ad.Account_Id
                                   ,ad.Service_Month
                                   ,ad.Service_Month Billing_Start_Dt
                                   ,DATEADD(dd, -1, DATEADD(mm, 1, ad.Service_Month)) Billing_End_Dt
                                   ,DATEPART(dd, DATEADD(dd, -1, DATEADD(mm, 1, ad.Service_Month))) Billing_Days
                              FROM
                                    #Affected_Data ad
                              WHERE
                                    NOT EXISTS ( SELECT
                                                      1
                                                 FROM
                                                      dbo.Cost_Usage_Account_Billing_Dtl cu
                                                 WHERE
                                                      cu.Service_Month = ad.Service_Month
                                                      AND cu.Account_Id = ad.Account_Id )
                                    AND ad.Action_Type IN ( 'I', 'U' )
                              GROUP BY
                                    ad.Account_Id
                                   ,ad.Service_Month;
                 
                  SET @Min_Cost_Usage_Site_Dtl_Transfer_Id = @Min_Cost_Usage_Site_Dtl_Transfer_Id + @Batch_Size;
                  TRUNCATE TABLE #Affected_Data;

            END;    

      DROP TABLE #Affected_Data;

END;


;

;
GO




GRANT EXECUTE ON  [dbo].[Merge_DE_Cost_Usage_Account_Dtl_Changes] TO [CBMSApplication]
GRANT EXECUTE ON  [dbo].[Merge_DE_Cost_Usage_Account_Dtl_Changes] TO [ETL_Execute]
GO
