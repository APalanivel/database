SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.Client_Comment_Ins

DESCRIPTION:

INPUT PARAMETERS:
	Name					DataType		Default	Description
---------------------------------------------------------------
	@Client_Id				INT
	@Comment_Type			VARCHAR(25)
	@Comment_User_Info_Id	INT
	@Comment_Dt				DATE
	@Comment_Text			VARCHAR(MAX)

OUTPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	
USAGE EXAMPLES:
------------------------------------------------------------

BEGIN TRAN
select * from Client_Comment_Map where client_id=11803
EXEC dbo.Client_Comment_Ins  11803,'CEAComment',49,NULL,'CEA CEM and CSA Comments'
select * from Client_Comment_Map where client_id=11803
ROLLBACK TRAN

select top 3 cd.code_id,cd.code_value from code cd join codeset cs on cd.codeset_id=cs.codeset_id where codeset_name='CommentType'
order by cd.code_id desc


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	BCH			Balaraju
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	 BCH       	2012-08-29	Created

******/
CREATE PROCEDURE dbo.Client_Comment_Ins
      ( 
       @Client_Id INT
      ,@Comment_Type VARCHAR(25)
      ,@Comment_User_Info_Id INT
      ,@Comment_Dt DATE = NULL
      ,@Comment_Text VARCHAR(MAX) )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE
            @Comment_Id INT
           ,@Comment_Type_CD INT
      
      SET @Comment_Dt = ISNULL(@Comment_Dt, GETDATE())
      SELECT
            @Comment_Type_CD = cd.Code_Id
      FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'CommentType'
            AND cd.Code_Value = @Comment_Type  
        
      
      EXEC dbo.Comment_Ins 
            @Comment_Type_CD
           ,@Comment_User_Info_Id
           ,@Comment_Dt
           ,@Comment_Text
           ,@Comment_Id OUT 
      
      INSERT      INTO dbo.Client_Comment_Map
                  ( Client_Id, Comment_Id )
      VALUES
                  ( @Client_Id, @Comment_Id )
                  
END
;
GO
GRANT EXECUTE ON  [dbo].[Client_Comment_Ins] TO [CBMSApplication]
GO
