SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.Account_Consolidated_Billing_Most_Recent_Invoice_Vendor_Sel
           
DESCRIPTION:             
			To get most recent consolidated billing supplier vendor
			
INPUT PARAMETERS:            
	Name				DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Account_Id			INT


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT TOP 10 * FROM dbo.Account_Consolidated_Billing_Vendor
            
	EXEC dbo.Account_Consolidated_Billing_Vendor_Sel 1269921
	EXEC dbo.Account_Consolidated_Billing_Most_Recent_Invoice_Vendor_Sel 1269921
	
	EXEC dbo.Account_Consolidated_Billing_Vendor_Sel 1269922
	EXEC dbo.Account_Consolidated_Billing_Most_Recent_Invoice_Vendor_Sel 1269922
	
	EXEC dbo.Account_Consolidated_Billing_Vendor_Sel 90155
	EXEC dbo.Account_Consolidated_Billing_Most_Recent_Invoice_Vendor_Sel 90155
	
AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-02-23	Contract placeholder - CP-50 Created
******/

CREATE PROCEDURE [dbo].[Account_Consolidated_Billing_Most_Recent_Invoice_Vendor_Sel] ( @Account_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Current_Date DATE = GETDATE()
      
      DECLARE @Tbl_Most_Recent_Invoice_Vendor TABLE
            ( 
             Account_Consolidated_Billing_Vendor_Id INT
            ,Vendor_Name VARCHAR(200)
            ,Billing_Start_Dt DATE
            ,Billing_End_Dt DATE
            ,Recent_By INT )
            
      DECLARE
            @DMO_Config_Exists BIT
           ,@Recent_By INT
      DECLARE @DMO_Config_Exists_For_Account TABLE ( Config_Exists BIT )
      
      INSERT      INTO @DMO_Config_Exists_For_Account
                  EXEC dbo.DMO_Config_Exists_For_Account 
                        @Account_Id = @Account_Id

      SELECT
            @DMO_Config_Exists = Config_Exists
      FROM
            @DMO_Config_Exists_For_Account
            
      INSERT      INTO @Tbl_Most_Recent_Invoice_Vendor
                  ( 
                   Account_Consolidated_Billing_Vendor_Id
                  ,Vendor_Name
                  ,Billing_Start_Dt
                  ,Billing_End_Dt
                  ,Recent_By )
                  SELECT
                        cbv.Account_Consolidated_Billing_Vendor_Id
                       ,CASE WHEN iv.ENTITY_NAME = 'Utility' THEN cha.Account_Vendor_Name
                             WHEN iv.ENTITY_NAME = 'Supplier' THEN ISNULL(ven.VENDOR_NAME, 'Contracted Vendor')
                        END AS Vendor_Name
                       ,cbv.Billing_Start_Dt
                       ,cbv.Billing_End_Dt
                       ,0 AS Recent_By
                  FROM
                        dbo.Account_Consolidated_Billing_Vendor cbv
                        INNER JOIN Core.Client_Hier_Account cha
                              ON cbv.Account_Id = cha.Account_Id
                        INNER JOIN dbo.ENTITY iv
                              ON cbv.Invoice_Vendor_Type_Id = iv.ENTITY_ID
                        LEFT JOIN dbo.VENDOR ven
                              ON cbv.Supplier_Vendor_Id = ven.VENDOR_ID
                        INNER JOIN dbo.USER_INFO ui
                              ON cbv.Updated_User_Id = ui.USER_INFO_ID
                  WHERE
                        cbv.Account_Id = @Account_Id
                        AND cha.Account_Type = 'Utility'
                        AND ( ( cbv.Billing_End_Dt IS NOT NULL
                                AND @Current_Date BETWEEN cbv.Billing_Start_Dt
                                                  AND     cbv.Billing_End_Dt )
                              OR ( cbv.Billing_End_Dt IS NULL
                                   AND @Current_Date >= cbv.Billing_Start_Dt ) )
                  GROUP BY
                        cbv.Account_Consolidated_Billing_Vendor_Id
                       ,CASE WHEN iv.ENTITY_NAME = 'Utility' THEN cha.Account_Vendor_Name
                             WHEN iv.ENTITY_NAME = 'Supplier' THEN ISNULL(ven.VENDOR_NAME, 'Contracted Vendor')
                        END
                       ,cbv.Billing_Start_Dt
                       ,cbv.Billing_End_Dt
                       
                       
      INSERT      INTO @Tbl_Most_Recent_Invoice_Vendor
                  ( 
                   Account_Consolidated_Billing_Vendor_Id
                  ,Vendor_Name
                  ,Billing_Start_Dt
                  ,Billing_End_Dt
                  ,Recent_By )
                  SELECT
                        cbv.Account_Consolidated_Billing_Vendor_Id
                       ,CASE WHEN iv.ENTITY_NAME = 'Utility' THEN cha.Account_Vendor_Name
                             WHEN iv.ENTITY_NAME = 'Supplier' THEN ISNULL(ven.VENDOR_NAME, 'Contracted Vendor')
                        END AS Vendor_Name
                       ,cbv.Billing_Start_Dt
                       ,cbv.Billing_End_Dt
                       ,DATEDIFF(dd, cbv.Billing_End_Dt, @Current_Date) AS Recent_By
                  FROM
                        dbo.Account_Consolidated_Billing_Vendor cbv
                        INNER JOIN Core.Client_Hier_Account cha
                              ON cbv.Account_Id = cha.Account_Id
                        INNER JOIN dbo.ENTITY iv
                              ON cbv.Invoice_Vendor_Type_Id = iv.ENTITY_ID
                        LEFT JOIN dbo.VENDOR ven
                              ON cbv.Supplier_Vendor_Id = ven.VENDOR_ID
                        INNER JOIN dbo.USER_INFO ui
                              ON cbv.Updated_User_Id = ui.USER_INFO_ID
                  WHERE
                        cbv.Account_Id = @Account_Id
                        AND cha.Account_Type = 'Utility'
                        AND cbv.Billing_End_Dt <= @Current_Date
                  GROUP BY
                        cbv.Account_Consolidated_Billing_Vendor_Id
                       ,CASE WHEN iv.ENTITY_NAME = 'Utility' THEN cha.Account_Vendor_Name
                             WHEN iv.ENTITY_NAME = 'Supplier' THEN ISNULL(ven.VENDOR_NAME, 'Contracted Vendor')
                        END
                       ,cbv.Billing_Start_Dt
                       ,cbv.Billing_End_Dt
                       
      SELECT
            @Recent_By = MIN(Recent_By)
      FROM
            @Tbl_Most_Recent_Invoice_Vendor
            
      SELECT
            Account_Consolidated_Billing_Vendor_Id
           ,Vendor_Name
           ,Billing_Start_Dt
           ,Billing_End_Dt
      FROM
            @Tbl_Most_Recent_Invoice_Vendor
      WHERE
            Recent_By = @Recent_By
      
END;
;
GO
GRANT EXECUTE ON  [dbo].[Account_Consolidated_Billing_Most_Recent_Invoice_Vendor_Sel] TO [CBMSApplication]
GO
