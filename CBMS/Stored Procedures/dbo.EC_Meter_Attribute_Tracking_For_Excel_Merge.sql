SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
NAME: dbo.EC_Meter_Attribute_Tracking_For_Excel_Merge                                                  
                                                   
                                                  
DESCRIPTION:                                                  
       This sproc is to get the Meter and attribute tracking details for the given search criteria                                            
                     
                                                  
INPUT PARAMETERS:                                                  
 Name               DataType   Default   Description                                                  
------------------------------------------------------------------------------------                                                  
 @EC_Meter_Attribute_Tracking_Id         INT          
 @EC_Meter_Attribute_Value               NVARCHAR(400)       
 @Meter_Attribute_Type_Value    NVARCHAR(400)       
 @User_Info_Id                           INT          
           
                                                  
OUTPUT PARAMETERS:                                                  
 Name       DataType  Default   Description                                                  
------------------------------------------------------------------------------------                                                  
                                                
                                                  
USAGE EXAMPLES:                                                  
------------------------------------------------------------------------------------                                                  
select * from EC_Meter_Attribute_Tracking    where EC_Meter_Attribute_Tracking_Id = 2478119                                     
BEGIN TRANSACTION          
          
exec dbo.EC_Meter_Attribute_Tracking_For_Excel_Merge           
 @User_Info_Id=49,          
 @EC_Meter_Attribute_Tracking_Id=2478119,          
 @EC_Meter_Attribute_Value=N'10',          
 @Meter_Attribute_Type_Value=N'Actual'          
                
               
select * from EC_Meter_Attribute_Tracking    where EC_Meter_Attribute_Tracking_Id = 2478119          
ROLLBACK TRANSACTION                            
                                                  
AUTHOR INITIALS:                                                  
 Initials  Name                                                  
------------------------------------------------------------------------------------                                                  
 SLP             Sri Lakshmi Pallikonda          
                                                   
MODIFICATIONS                                                  
                                                  
 Initials Date   Modification                                                  
------------------------------------------------------------------------------------                                                  
 SLP   2020-01-17  Created For Excel Bulk Upload- Meter attributes through Excel         
          
******/  
CREATE PROCEDURE [dbo].[EC_Meter_Attribute_Tracking_For_Excel_Merge]  
     (  
         @EC_Meter_Attribute_Tracking_Id INT  
         , @EC_Meter_Attribute_Value NVARCHAR(400)  
         , @Meter_Attribute_Type_Value NVARCHAR(400)  
         , @User_Info_Id INT  
     )  
AS  
    BEGIN  
        SET NOCOUNT ON;  
  
        DECLARE @Meter_Attribute_Type_Cd INT;  
        DECLARE  
            @Account_Id INT  
            , @Meter_ID INT  
            , @New_Exception_Status_Cd INT  
            , @Closed_Exception_Status_Cd INT  
            , @Missing_Meter_Attribute_Exception_Type_Cd INT  
            , @Actual_Attribute_Type_Cd INT;  
  
        SELECT  
            @Actual_Attribute_Type_Cd = cd.Code_Id  
        FROM  
            dbo.Code cd  
            JOIN dbo.Codeset cs  
                ON cd.Codeset_Id = cs.Codeset_Id  
        WHERE  
            cd.Code_Value = 'Actual'  
            AND cs.Codeset_Name = 'MeterAttributeType';  
  
        SELECT  
            @New_Exception_Status_Cd = cd.Code_Id  
        FROM  
            dbo.Code cd  
            JOIN dbo.Codeset cs  
                ON cd.Codeset_Id = cs.Codeset_Id  
        WHERE  
            cs.Codeset_Name = 'Exception Status'  
            AND cd.Code_Value = 'New';  
  
        SELECT  
            @Closed_Exception_Status_Cd = cd.Code_Id  
        FROM  
            dbo.Code cd  
            JOIN dbo.Codeset cs  
                ON cd.Codeset_Id = cs.Codeset_Id  
        WHERE  
            cs.Codeset_Name = 'Exception Status'  
            AND cd.Code_Value = 'Closed';  
        SELECT  
            @Missing_Meter_Attribute_Exception_Type_Cd = C.Code_Id  
        FROM  
            dbo.Code C  
            INNER JOIN dbo.Codeset CS  
                ON C.Codeset_Id = CS.Codeset_Id  
        WHERE  
            CS.Codeset_Name = 'Exception Type'  
            AND C.Code_Value = 'Missing Meter Attribute';  
        SELECT  
            @Account_Id = cha.Account_Id  
        FROM  
            Core.Client_Hier_Account cha  
            INNER JOIN dbo.EC_Meter_Attribute_Tracking emat  
                ON cha.Meter_Id = emat.Meter_Id  
        WHERE  
            cha.Account_Type = 'Utility'  
            AND EC_Meter_Attribute_Tracking_Id = @EC_Meter_Attribute_Tracking_Id;  
  
        SELECT  
            @Meter_ID = emat.Meter_Id  
        FROM  
            dbo.EC_Meter_Attribute_Tracking emat  
        WHERE  
            emat.EC_Meter_Attribute_Tracking_Id = @EC_Meter_Attribute_Tracking_Id;  
  
  
  
        SELECT  
            @Meter_Attribute_Type_Cd = c.Code_Id  
        FROM  
            Code c  
            JOIN dbo.Codeset AS c2  
                ON c2.Codeset_Id = c.Codeset_Id  
        WHERE  
            c2.Codeset_Name = 'MeterAttributeType'  
            AND c.Code_Value = @Meter_Attribute_Type_Value;  
  
        MERGE INTO dbo.EC_Meter_Attribute_Tracking tgt  
        USING (   SELECT  
                        @EC_Meter_Attribute_Tracking_Id AS EC_Meter_Attribute_Tracking_Id  
                        , @EC_Meter_Attribute_Value AS EC_Meter_Attribute_Value  
                        , @Meter_Attribute_Type_Cd AS Meter_Attribute_Type_Cd) src  
        ON (tgt.EC_Meter_Attribute_Tracking_Id = src.EC_Meter_Attribute_Tracking_Id)  
        WHEN MATCHED AND src.EC_Meter_Attribute_Value IS NOT NULL  
                         AND src.EC_Meter_Attribute_Value <> '' THEN  
            UPDATE SET  
                tgt.EC_Meter_Attribute_Value = src.EC_Meter_Attribute_Value  
                , tgt.Meter_Attribute_Type_Cd = @Meter_Attribute_Type_Cd  
                , tgt.Updated_User_Id = @User_Info_Id  
                , tgt.Last_Change_Ts = GETDATE()  
        WHEN MATCHED AND (   src.EC_Meter_Attribute_Value IS NULL  
                             OR src.EC_Meter_Attribute_Value = '') THEN DELETE;  
  
  
        IF EXISTS (   SELECT  
                            1  
                      FROM  
                            dbo.Account_Exception ae  
                      WHERE  
                            ae.Meter_Id = @Meter_ID  
                            AND ae.Exception_Status_Cd = @New_Exception_Status_Cd  
                            AND ae.Exception_Type_Cd = @Missing_Meter_Attribute_Exception_Type_Cd)  
            BEGIN  
  
                UPDATE  
                    ae  
                SET  
                    ae.Exception_Status_Cd = @Closed_Exception_Status_Cd  
                    , ae.Closed_By_User_Id = @User_Info_Id  
                    , ae.Closed_Ts = GETDATE()  
                    , ae.Last_Change_Ts = GETDATE()  
                    , ae.Updated_User_Id = @User_Info_Id  
                FROM  
                    dbo.Account_Exception ae  
                WHERE  
                    ae.Meter_Id = @Meter_ID  
                    AND ae.Exception_Status_Cd = @New_Exception_Status_Cd  
                    AND ae.Exception_Type_Cd = @Missing_Meter_Attribute_Exception_Type_Cd  
                    AND ae.Closed_By_User_Id IS NULL  
               AND EXISTS (   SELECT  
                                        1  
                                   FROM  
                                        dbo.EC_Meter_Attribute_Tracking emat  
                                   WHERE  
                                        emat.Meter_Attribute_Type_Cd = @Actual_Attribute_Type_Cd  
                                        AND emat.Meter_Id = @Meter_ID);  
            END;  
  
  
    END;
GO
GRANT EXECUTE ON  [dbo].[EC_Meter_Attribute_Tracking_For_Excel_Merge] TO [CBMSApplication]
GO
