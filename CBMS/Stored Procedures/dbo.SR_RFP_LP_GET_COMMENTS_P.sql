SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.SR_RFP_LP_GET_COMMENTS_P
	@user_id varchar(10),
	@session_id varchar(20),
	@rfp_account_id int
	AS
	set nocount on
	select 	comment.sr_rfp_lp_comment_id,
		comment.comment,
		info.username,
		comment.comment_date
	from	sr_rfp_lp_comment comment(nolock),
		user_info info(nolock),
		sr_rfp_account rfp_account(nolock)
	where	comment.sr_rfp_account_id = @rfp_account_id
		and rfp_account.sr_rfp_account_id = comment.sr_rfp_account_id
		and rfp_account.is_deleted = 0
		and info.user_info_id = comment.user_info_id
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_LP_GET_COMMENTS_P] TO [CBMSApplication]
GO
