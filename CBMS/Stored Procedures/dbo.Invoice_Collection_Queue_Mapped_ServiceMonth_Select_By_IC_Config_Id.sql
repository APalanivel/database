SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                                  
                        
NAME: dbo.Invoice_Collection_Queue_ServiceMonth_Delete_By_Invoice_Collection_Queue_Id  
  
DESCRIPTION:                         
 To get the ICQ details for the given account config Id
                              
INPUT PARAMETERS:                                  
NAME									DATATYPE	DEFAULT		DESCRIPTION                                  
------------------------------------------------------------------------------                                  
@Invoice_Collection_Account_Config_Id   INT  
  
OUTPUT PARAMETERS:                                  
NAME   DATATYPE DEFAULT  DESCRIPTION                                                        
------------------------------------------------------------------------------  
  
USAGE EXAMPLES:                                  
------------------------------------------------------------------------------  
  
  
AUTHOR INITIALS:                                  
INITIALS NAME                                  
------------------------------------------------------------                                  
PR   Pradip Rajput  
                      
MODIFICATIONS                        
INITIALS	DATE(YYYY-MM-DD)	MODIFICATION                        
------------------------------------------------------------                        
PR			2018-10-02			Created  
*/  
CREATE PROCEDURE [dbo].[Invoice_Collection_Queue_Mapped_ServiceMonth_Select_By_IC_Config_Id]
      (
       @Invoice_Collection_Account_Config_Id INT )
AS
BEGIN  
  
      SET NOCOUNT ON;  
  
      SELECT
            q.Invoice_Collection_Queue_Id
           ,q.Collection_Start_Dt
           ,q.Collection_End_Dt
           ,cm.Service_Month
           ,c.Code_Value AS Record_Status
           ,cc.Code_Value AS Record_type
      FROM
            dbo.Invoice_Collection_Queue q
            JOIN dbo.Invoice_Collection_Queue_Month_Map qmap
                  ON qmap.Invoice_Collection_Queue_Id = q.Invoice_Collection_Queue_Id
            JOIN dbo.Account_Invoice_Collection_Month cm
                  ON cm.Account_Invoice_Collection_Month_Id = qmap.Account_Invoice_Collection_Month_Id
            JOIN dbo.Code cc
                  ON cc.Code_Id = q.Invoice_Collection_Queue_Type_Cd
            JOIN dbo.Code c
                  ON c.Code_Id = q.Status_Cd
      WHERE
            q.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
      ORDER BY
            q.Collection_Start_Dt
  
END; 
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Queue_Mapped_ServiceMonth_Select_By_IC_Config_Id] TO [CBMSApplication]
GO
