SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: dbo.Supplier_Account_Config_History_Del_By_Supplier_Account_Config_Id

DESCRIPTION:

	To Delete Time band History Associated with given Account.

INPUT PARAMETERS:
	NAME							DATATYPE	DEFAULT		DESCRIPTION     
---------------------------------------------------------------------------------------------
	@Supplier_Account_Config		INT
    @Account_Id						INT
    @User_Info_Id					INT
                
OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
---------------------------------------------------------------------------------------------

USAGE EXAMPLES:          
---------------------------------------------------------------------------------------------


	BEGIN TRAN

	SELECT * FROM dbo.Supplier_Account_Config sac WHERE sac.Account_Id =1658700
	SELECT * FROM dbo.SUPPLIER_ACCOUNT_METER_MAP samm WHERE samm.ACCOUNT_ID = 1658700
	SELECT * FROM dbo.INVOICE_PARTICIPATION ip WHERE ip.ACCOUNT_ID = 1658700

		EXEC dbo.Supplier_Account_Config_History_Del_By_Supplier_Account_Config_Id
    @Account_Id = 1658700
    , @Supplier_Account_Config_Id = 507443
    , @User_Info_Id = 49


	SELECT * FROM dbo.Supplier_Account_Config sac WHERE sac.Account_Id =1658700
	SELECT * FROM dbo.SUPPLIER_ACCOUNT_METER_MAP samm WHERE samm.ACCOUNT_ID = 1658700
	SELECT * FROM dbo.INVOICE_PARTICIPATION ip WHERE ip.ACCOUNT_ID = 1658700
		
	ROLLBACK TRAN
	


AUTHOR INITIALS:
	INITIALS	NAME
---------------------------------------------------------------------------------------------
	NR			Narayana Reddy
	

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
---------------------------------------------------------------------------------------------
	NR			2020-09-01	Created For MAINT-9734 -Delete individual account time bands

*/
CREATE PROCEDURE [dbo].[Supplier_Account_Config_History_Del_By_Supplier_Account_Config_Id]
    (
        @Account_Id INT
        , @Supplier_Account_Config_Id INT
        , @User_Info_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Current_Ts DATETIME
            , @Audit_Function SMALLINT = -1
            , @User_Name VARCHAR(81)
            , @Application_Name VARCHAR(60) = 'Delete Supplier Account Time Band'
            , @Client_Name VARCHAR(200)
            , @Meter_Id INT
            , @Site_Id INT
            , @Service_Month DATETIME
            , @Lookup_Value XML
            , @Total_Row_Count INT
            , @Row_Counter INT;

        SELECT
            @Client_Name = ch.Client_Name
        FROM
            Core.Client_Hier ch
            JOIN Core.Client_Hier_Account cha
                ON cha.Client_Hier_Id = ch.Client_Hier_Id
        WHERE
            cha.Account_Id = @Account_Id
        GROUP BY
            ch.Client_Name;


        SELECT
            @User_Name = FIRST_NAME + SPACE(1) + LAST_NAME
        FROM
            dbo.USER_INFO
        WHERE
            USER_INFO_ID = @User_Info_Id;


        SET @Lookup_Value = (   SELECT
                                    sac.Account_Id
                                    , a.ACCOUNT_NUMBER
                                    , sac.Contract_Id
                                    , sac.Supplier_Account_Begin_Dt
                                    , sac.Supplier_Account_End_Dt
                                FROM
                                    dbo.Supplier_Account_Config sac
                                    INNER JOIN dbo.ACCOUNT a
                                        ON a.ACCOUNT_ID = sac.Account_Id
                                WHERE
                                    sac.Supplier_Account_Config_Id = @Supplier_Account_Config_Id
                                    AND sac.Account_Id = @Account_Id
                                    AND sac.Contract_Id = -1
                                GROUP BY
                                    sac.Account_Id
                                    , a.ACCOUNT_NUMBER
                                    , sac.Contract_Id
                                    , sac.Supplier_Account_Begin_Dt
                                    , sac.Supplier_Account_End_Dt
                                FOR XML RAW);

        DECLARE @Supplier_Account_Meter_Map_List TABLE
              (
                  Meter_Id INT
                  , PRIMARY KEY CLUSTERED
                    (
                        Meter_Id
                    )
                  , Row_Num INT IDENTITY(1, 1)
              );

        DECLARE @Invoice_Participation_List TABLE
              (
                  Site_Id INT
                  , Service_Month DATETIME
                  , PRIMARY KEY CLUSTERED
                    (
                        Site_Id
                        , Service_Month
                    )
                  , Row_Num INT IDENTITY(1, 1)
              );
        DECLARE @IP_Site_Reaggregation_List TABLE
              (
                  Site_Id INT
                  , Service_Month DATETIME
                  , PRIMARY KEY CLUSTERED
                    (
                        Site_Id
                        , Service_Month
                    )
                  , Row_Num INT IDENTITY(1, 1)
              );

        INSERT INTO @Supplier_Account_Meter_Map_List
             (
                 Meter_Id
             )
        SELECT
            METER_ID
        FROM
            dbo.SUPPLIER_ACCOUNT_METER_MAP
        WHERE
            Supplier_Account_Config_Id = @Supplier_Account_Config_Id
            AND ACCOUNT_ID = @Account_Id
            AND Contract_ID = -1
        GROUP BY
            METER_ID;

        INSERT INTO @Invoice_Participation_List
             (
                 Site_Id
                 , Service_Month
             )
        SELECT
            ip.SITE_ID
            , ip.SERVICE_MONTH
        FROM
            dbo.INVOICE_PARTICIPATION ip
            JOIN dbo.Supplier_Account_Config sac
                ON sac.Account_Id = ip.ACCOUNT_ID
        WHERE
            sac.Supplier_Account_Config_Id = @Supplier_Account_Config_Id
            AND sac.Account_Id = @Account_Id
            AND sac.Contract_Id = -1
            AND ip.SERVICE_MONTH BETWEEN sac.Supplier_Account_Begin_Dt
                                 AND     ISNULL(sac.Supplier_Account_End_Dt, '9999-12-31')
        GROUP BY
            ip.SITE_ID
            , ip.SERVICE_MONTH;


        INSERT INTO @IP_Site_Reaggregation_List
             (
                 Site_Id
                 , Service_Month
             )
        SELECT
            ipl.Site_Id
            , ipl.Service_Month
        FROM
            @Invoice_Participation_List ipl
        GROUP BY
            ipl.Site_Id
            , ipl.Service_Month;

        BEGIN TRY

            ---- Removed  Meter map data for associated deleted supplier account time band . 


            SELECT  @Total_Row_Count = MAX(Row_Num)FROM @Supplier_Account_Meter_Map_List;
            SET @Row_Counter = 1;

            WHILE (@Row_Counter <= @Total_Row_Count)
                BEGIN

                    SELECT
                        @Meter_Id = Meter_Id
                    FROM
                        @Supplier_Account_Meter_Map_List
                    WHERE
                        Row_Num = @Row_Counter;


                    DELETE
                    samm
                    FROM
                        dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                    WHERE
                        samm.Supplier_Account_Config_Id = @Supplier_Account_Config_Id
                        AND samm.ACCOUNT_ID = @Account_Id
                        AND samm.Contract_ID = -1
                        AND samm.METER_ID = @Meter_Id;


                    SET @Row_Counter = @Row_Counter + 1;


                END;


            ---- Removed  IP data for deleted supplier time band . 

            SELECT  @Total_Row_Count = MAX(Row_Num)FROM @Invoice_Participation_List;
            SET @Row_Counter = 1;

            WHILE (@Row_Counter <= @Total_Row_Count)
                BEGIN

                    SELECT
                        @Site_Id = Site_Id
                        , @Service_Month = Service_Month
                    FROM
                        @Invoice_Participation_List
                    WHERE
                        Row_Num = @Row_Counter;

                    EXEC dbo.Invoice_Participation_Del @Account_Id, @Site_Id, @Service_Month;

                    SET @Row_Counter = @Row_Counter + 1;

                END;

            ---- Re Aggrigate the  Ip Account level to site level.


            SELECT  @Total_Row_Count = MAX(Row_Num)FROM @IP_Site_Reaggregation_List;
            SET @Row_Counter = 1;



            WHILE (@Row_Counter <= @Total_Row_Count)
                BEGIN

                    SELECT
                        @Site_Id = Site_Id
                        , @Service_Month = Service_Month
                    FROM
                        @IP_Site_Reaggregation_List
                    WHERE
                        Row_Num = @Row_Counter;

                    EXEC dbo.cbmsInvoiceParticipationSite_Save3
                        @User_Info_Id
                        , @Site_Id
                        , @Service_Month;

                    SET @Row_Counter = @Row_Counter + 1;

                END;


            ----To delete the  Passed Supplier account Time band

            DELETE
            sac
            FROM
                dbo.Supplier_Account_Config sac
            WHERE
                sac.Supplier_Account_Config_Id = @Supplier_Account_Config_Id
                AND sac.Account_Id = @Account_Id
                AND sac.Contract_Id = -1;





            SET @Current_Ts = GETDATE();
            EXEC dbo.Application_Audit_Log_Ins
                @Client_Name
                , @Application_Name
                , @Audit_Function
                , 'Supplier_Account_Config'
                , @Lookup_Value
                , @User_Name
                , @Current_Ts;

        END TRY
        BEGIN CATCH


            EXEC dbo.usp_RethrowError;

        END CATCH;
    END;



GO
GRANT EXECUTE ON  [dbo].[Supplier_Account_Config_History_Del_By_Supplier_Account_Config_Id] TO [CBMSApplication]
GO
