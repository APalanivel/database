SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
NAME:  
  
DESCRIPTION: [Clinet_Invoice_Process_Tracking_Account]      
  
 This procedure is used to check if the invoice tracking is allowed based on the Account and commodity of the account  
 Intended to used by Stage-3 after the invoice resolved to account  
  
 INPUT PARAMETERS:          
 Name    DataType  Default  Description          
------------------------------------------------------------------------------------------------------------------------          
  
 OUTPUT PARAMETERS:          
 Name    DataType  Default  Description          
------------------------------------------------------------------------------------------------------------------------          
  
       
 USAGE EXAMPLES:          
------------------------------------------------------------------------------------------------------------------------          
 exec    [Client_Invoice_Process_Tracking] 89865720  
  
AUTHOR INITIALS:          
 Initials	Name          
------------------------------------------------------------------------------------------------------------------------          
 TRK		Ramakrishna Thummala      
  
 MODIFICATIONS           
       
 Initials	Date		Modification          
------------------------------------------------------------------------------------------------------------------------          
 TRK		2020-01-13  Created  
******/
CREATE PROCEDURE [dbo].[Clinet_Invoice_Process_Tracking_Account]
    (@CU_Invoice_ID INT)
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @Is_Invoice_DNT BIT = 1;

    DECLARE @Invoice_Account TABLE
        (
            [CU_INVOICE_ID] INT
           ,[Account_ID]    INT
           ,[Client_Id]     INT
           ,[Commodity_Id]  INT
           ,Region_ID       INT
           ,Region_Name     NVARCHAR (200)
           ,State_Id        INT
           ,State_Name      NVARCHAR (200)
           ,Country_Id      INT
           ,Country_Name    NVARCHAR (200)
           ,Begin_Date      DATETIME
           ,End_Date        DATETIME
        );
    DECLARE @Invoice_Commodity TABLE
        (
            Commodity_Id INT
        );

    INSERT INTO @Invoice_Commodity
    (
        Commodity_Id
    )
    SELECT d.COMMODITY_TYPE_ID
    FROM
        dbo.CU_INVOICE_DETERMINANT d
    WHERE
        CU_INVOICE_ID = @CU_Invoice_ID
        AND d.COMMODITY_TYPE_ID <> -1
    GROUP BY d.COMMODITY_TYPE_ID;

    INSERT INTO @Invoice_Commodity
    (
        Commodity_Id
    )
    SELECT d.COMMODITY_TYPE_ID
    FROM
        dbo.CU_INVOICE_CHARGE d
    WHERE
        CU_INVOICE_ID = @CU_Invoice_ID
        AND d.COMMODITY_TYPE_ID <> -1
        AND NOT EXISTS (
                           SELECT 1
                           FROM
                               @Invoice_Commodity ic
                           WHERE
                               ic.Commodity_Id = d.COMMODITY_TYPE_ID
                       )
    GROUP BY d.COMMODITY_TYPE_ID;

    INSERT INTO @Invoice_Account
    (
        CU_INVOICE_ID
       ,Account_ID
       ,Client_Id
       ,Commodity_Id
       ,Region_ID
       ,Region_Name
       ,State_Id
       ,State_Name
       ,Country_Id
       ,Country_Name
       ,Begin_Date
       ,End_Date
    )
    SELECT
        CI.CU_INVOICE_ID
       ,CHA.Account_Id
       ,CH.Client_Id
       ,CHA.Commodity_Id
       ,CH.Region_ID
       ,CH.Region_Name
       ,CH.State_Id
       ,CH.State_Name
       ,CH.Country_Id
       ,CH.Country_Name
       ,CI.BEGIN_DATE
       ,CI.END_DATE
    FROM
        dbo.CU_INVOICE CI
        INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH AS CISM
            ON CISM.CU_INVOICE_ID = CI.CU_INVOICE_ID
        INNER JOIN Core.Client_Hier_Account AS CHA
            ON CHA.Account_Id = CISM.Account_ID
        INNER JOIN Core.Client_Hier AS CH
            ON CH.Client_Hier_Id = CHA.Client_Hier_Id
    WHERE
        CI.CU_INVOICE_ID = @CU_Invoice_ID
        AND EXISTS (
                       SELECT 1
                       FROM
                           @Invoice_Commodity ic
                       WHERE
                           ic.Commodity_Id = CHA.Commodity_Id
                   )
    GROUP BY CI.CU_INVOICE_ID
            ,CHA.Account_Id
            ,CH.Client_Id
            ,CHA.Commodity_Id
            ,CH.Region_ID
            ,CH.Region_Name
            ,CH.State_Id
            ,CH.State_Name
            ,CH.Country_Id
            ,CH.Country_Name
            ,CI.BEGIN_DATE
            ,CI.END_DATE;

    SELECT
        IA.CU_INVOICE_ID
       ,IA.Account_ID
       ,IA.Client_Id
       ,IA.Commodity_Id
       ,CIDC.Client_Invoice_DNT_Config_Id
       ,CIDC.CLIENT_ID AS DNT_Config_CLIENT_ID
       ,CIDC.Commodity_Id AS DNT_Config_Commodity_Id
       ,CIDC.Effective_Start_Dt
       ,CIDC.Effective_End_Dt
       ,IA.Region_ID
       ,IA.Region_Name
       ,IA.State_Id
       ,IA.State_Name
       ,IA.Country_Id
       ,IA.Country_Name
       ,IA.Begin_Date
       ,IA.End_Date
    INTO #Account_DNT_Config
    FROM
        @Invoice_Account AS IA
        INNER JOIN dbo.Client_Invoice_DNT_Config AS CIDC
            ON IA.Client_Id = CIDC.CLIENT_ID
               AND IA.Commodity_Id = CIDC.Commodity_Id
    WHERE
        IA.Begin_Date >= CIDC.Effective_Start_Dt
        AND IA.End_Date <= CIDC.Effective_End_Dt;

    SELECT *
    INTO #DNT_State
    FROM (

             -- Configuration at Region level
             SELECT
                 st.STATE_ID
                ,st.STATE_NAME
                ,dc.Commodity_Id
             FROM
                 dbo.STATE st
                 CROSS JOIN #Account_DNT_Config dc
             WHERE
                 EXISTS (
                            SELECT 1
                            FROM
                                dbo.Client_Invoice_DNT_Config_Region_Map rm
                            WHERE
                                st.REGION_ID = rm.REGION_ID
                                AND rm.Client_Invoice_DNT_Config_Id = dc.Client_Invoice_DNT_Config_Id
                        )
                 AND NOT EXISTS (
                                    SELECT 1
                                    FROM
                                        dbo.Client_Invoice_DNT_Config_Country_Map cm
                                    WHERE
                                        cm.Client_Invoice_DNT_Config_Id = dc.Client_Invoice_DNT_Config_Id
                                )
                 AND NOT EXISTS (
                                    SELECT 1
                                    FROM
                                        dbo.Client_Invoice_DNT_Config_State_Map sm
                                    WHERE
                                        sm.Client_Invoice_DNT_Config_Id = dc.Client_Invoice_DNT_Config_Id
                                )
             UNION
             -- Configuration at Country level
             SELECT
                 st.STATE_ID
                ,st.STATE_NAME
                ,dc.Commodity_Id
             FROM
                 dbo.STATE st
                 CROSS JOIN #Account_DNT_Config dc
             WHERE
                 EXISTS (
                            SELECT 1
                            FROM
                                dbo.Client_Invoice_DNT_Config_Country_Map cm
                            WHERE
                                cm.COUNTRY_ID = st.COUNTRY_ID
                                AND cm.Client_Invoice_DNT_Config_Id = dc.Client_Invoice_DNT_Config_Id
                        )
                 AND NOT EXISTS (
                                    SELECT 1
                                    FROM
                                        dbo.Client_Invoice_DNT_Config_State_Map sm
                                    WHERE
                                        sm.Client_Invoice_DNT_Config_Id = dc.Client_Invoice_DNT_Config_Id
                                )
             UNION
             -- Configuration at State level
             SELECT
                 st.STATE_ID
                ,st.STATE_NAME
                ,dc.Commodity_Id
             FROM
                 dbo.STATE st
                 CROSS JOIN #Account_DNT_Config dc
             WHERE
                 EXISTS (
                            SELECT 1
                            FROM
                                dbo.Client_Invoice_DNT_Config_State_Map sm
                            WHERE
                                sm.STATE_ID = st.STATE_ID
                                AND sm.Client_Invoice_DNT_Config_Id = dc.Client_Invoice_DNT_Config_Id
                        )
             UNION
             SELECT
                 st.STATE_ID
                ,st.STATE_NAME
                ,dc.Commodity_Id
             FROM
                 dbo.STATE st
                 CROSS JOIN #Account_DNT_Config dc
             WHERE
                 NOT EXISTS (
                                SELECT 1
                                FROM
                                    dbo.Client_Invoice_DNT_Config_State_Map sm
                                WHERE
                                    sm.Client_Invoice_DNT_Config_Id = dc.Client_Invoice_DNT_Config_Id
                            )
                 AND NOT EXISTS (
                                    SELECT 1
                                    FROM
                                        dbo.Client_Invoice_DNT_Config_Country_Map cm
                                    WHERE
                                        cm.Client_Invoice_DNT_Config_Id = dc.Client_Invoice_DNT_Config_Id
                                )
                 AND NOT EXISTS (
                                    SELECT 1
                                    FROM
                                        dbo.Client_Invoice_DNT_Config_Region_Map rm
                                    WHERE
                                        rm.Client_Invoice_DNT_Config_Id = dc.Client_Invoice_DNT_Config_Id
                                )
         ) x;

    -- No DNT Configuration exist for that client
    SELECT @Is_Invoice_DNT = 0
    WHERE
        NOT EXISTS (
                       SELECT 1
                       FROM
                           #Account_DNT_Config
                   );

    -- Atleast one commodity of the account is tracked   
    SELECT @Is_Invoice_DNT = 0
    FROM
        @Invoice_Account AS cu
    WHERE
        NOT EXISTS (
                       SELECT 1
                       FROM
                           #Account_DNT_Config AS ci
                       WHERE
                           ci.Commodity_Id = cu.Commodity_Id
                   );

    SELECT @Is_Invoice_DNT = 0
    FROM
        #Account_DNT_Config ci
    WHERE
        NOT EXISTS (
                       SELECT 1
                       FROM
                           #DNT_State dnt
                       WHERE
                           dnt.STATE_ID = ci.State_Id
                           AND ci.Commodity_Id = dnt.Commodity_Id
                   )
        AND @Is_Invoice_DNT = 1;

    SELECT @Is_Invoice_DNT Invoice_Rule;

    DROP TABLE #Account_DNT_Config;
    DROP TABLE #DNT_State;

END;
GO
GRANT EXECUTE ON  [dbo].[Clinet_Invoice_Process_Tracking_Account] TO [CBMSApplication]
GO
