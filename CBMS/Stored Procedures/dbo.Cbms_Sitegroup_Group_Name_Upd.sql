SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 
 dbo.Cbms_Sitegroup_Group_Name_Upd
 
 DESCRIPTION:   
 
	This procedure used to insert the Non metric(Division/State/Country/Supplier/Vendor..) rules of a smart group
 
 INPUT PARAMETERS:  
 Name			DataType	Default		Description  
------------------------------------------------------------    
	@SiteGroup_Id INT
	, @Rule_Filter_Id INT	
	, @Filter_Condition_Id INT
	, @Filter_Value VARCHAR(50)
	, @Is_Inclusive BIT
	
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  USAGE EXAMPLES:  
------------------------------------------------------------  
	

	EXEC dbo.Cbms_Sitegroup_Group_Name_Upd
    @Cbms_Sitegroup_Id = 1
    , @Group_Name = 'XXX'
   

AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 NR			Narayana Reddy    
 
 MODIFICATIONS   
 Initials	Date		Modification  
------------------------------------------------------------  
 NR			2018-09-27	Created for GRM.
  
******/

CREATE PROC [dbo].[Cbms_Sitegroup_Group_Name_Upd]
      ( 
       @Cbms_Sitegroup_Id INT
      ,@Group_Name NVARCHAR(255)
      ,@User_Info_Id INT = NULL )
AS 
BEGIN
      SET NOCOUNT ON;


      UPDATE
            cs
      SET   
            cs.Group_Name = @Group_Name
           ,cs.Updated_User_Id = @User_Info_Id
           ,cs.Last_Change_Ts = GETDATE()
      FROM
            dbo.Cbms_Sitegroup cs
      WHERE
            cs.Cbms_Sitegroup_Id = @Cbms_Sitegroup_Id;


END;

GO
GRANT EXECUTE ON  [dbo].[Cbms_Sitegroup_Group_Name_Upd] TO [CBMSApplication]
GO
