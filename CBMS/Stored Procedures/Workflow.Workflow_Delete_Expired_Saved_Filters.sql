SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******    
NAME:    [Workflow].[Workflow_Delete_Expired_Saved_Filters]
DESCRIPTION: Deleting the saved filters after expire 
------------------------------------------------------------   
 INPUT PARAMETERS:    
 Name   DataType  Default Description    
-----------------------------------------------------------    
 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
 @str_Return  AS VARCHAR(100) OUTPUT
 ------------------------------------------------------------    
 USAGE EXAMPLES:    
[Workflow].[Workflow_Delete_Expired_Saved_Filters]                                    
------------------------------------------------------------    
AUTHOR INITIALS:    
Initials	Name    
------------------------------------------------------------    
TRK			Ramakrishna Thummala	Summit Energy 
AP			Arunkumar Palanivel			Summit Energy 
 
 MODIFICATIONS     
 Initials Date   Modification    
------------------------------------------------------------    
 TRK		  Sep-13-2019  Created
 AP			Oct-10-2019		Rewrite the logic completely

******/ 
  
CREATE PROCEDURE [Workflow].[Workflow_Delete_Expired_Saved_Filters]  
(  
 @Number_Records INT  
 --@str_Return  AS VARCHAR(100) OUTPUT  
)  
AS  
BEGIN  
  
  declare @ERROR_MESSAGE nvarchar(max)
 BEGIN TRY      
  
 DECLARE @Workflow_Queue_Saved_Filter_Query TABLE ( Workflow_Queue_Saved_Filter_Query_Id INT)  

  
  DECLARE @Deleted_Rows INT;  
  SET @Deleted_Rows = 1;  
  
  
  WHILE (@Deleted_Rows > 0)  
    BEGIN  
       -- Delete some number of rows at a time 

	   DELETE FROM @Workflow_Queue_Saved_Filter_Query

	   --INSERT BATCH RECORDS INTO TABLE VARIABLE FIRST AND DELETE IN BATCH

	    INSERT INTO @Workflow_Queue_Saved_Filter_Query (Workflow_Queue_Saved_Filter_Query_Id)  
		SELECT top (@Number_Records) Workflow_Queue_Saved_Filter_Query_Id FROM workflow.Workflow_Queue_Saved_Filter_Query AS WQSFQ   
			 WHERE WQSFQ.Expiration_Dt < GETDATE()  
	   
	   --IN ORDER TO MAINTAIN REFERENTIAL INTEGRITY NEED TO DELETE DEPENDENT TABLES FIRST (ORDER IS MENTIONED BELOW)

	   --STEP 1 DELETE WORKFLOW GROUP INVOICE MAP TABLE 
	   DELETE WQSFIM  from workflow.Workflow_Queue_Search_Filter_Group_Invoice_Map wqsfim
	   join workflow.Workflow_Queue_Search_Filter_Group wqsfg 
	   on wqsfim.Workflow_Queue_Search_Filter_Group_Id = wqsfg.Workflow_Queue_Search_Filter_Group_Id 
	   join @Workflow_Queue_Saved_Filter_Query wqs 
	   on wqsfg.Workflow_Queue_Saved_Filter_Query_Id = wqs.Workflow_Queue_Saved_Filter_Query_Id
	    

		--STEP 2  DELETE WORKFLOW QUEUE SEARCH FILTER GROUP TABLE
	   delete WQSFG from workflow.Workflow_Queue_Search_Filter_Group WQSFG 
	    join @Workflow_Queue_Saved_Filter_Query wqs 
	   on wqsfg.Workflow_Queue_Saved_Filter_Query_Id = wqs.Workflow_Queue_Saved_Filter_Query_Id
	  

	   --STEP 3 DELETE WORKFLOW QUEUE SAVED FILTER QUERY VALUE 
	     DELETE wqsfv from  workflow.Workflow_Queue_Saved_Filter_Query_Value wqsfv
		 join @Workflow_Queue_Saved_Filter_Query Temp
		on wqsfv.Workflow_Queue_Saved_Filter_Query_Id = temp.Workflow_Queue_Saved_Filter_Query_Id
        

		--STEP 4 DELETE WORKFLOW QUEUE SAVED FILTER QUERY TABLE 
 	     DELETE wqsf  from  workflow.Workflow_Queue_Saved_Filter_Query  wqsf 
		 join @Workflow_Queue_Saved_Filter_Query Temp
		on wqsf .Workflow_Queue_Saved_Filter_Query_Id = temp.Workflow_Queue_Saved_Filter_Query_Id

    SET @Deleted_Rows = @@ROWCOUNT;  
     
  END  
   
 END TRY        
 BEGIN CATCH  
 
 set @ERROR_MESSAGE = ERROR_MESSAGE ()    
 
  EXEC [dbo].[usp_RethrowError] @ERROR_MESSAGE;    
     
 END CATCH     
  
END  
GO
GRANT EXECUTE ON  [Workflow].[Workflow_Delete_Expired_Saved_Filters] TO [CBMSApplication]
GO
