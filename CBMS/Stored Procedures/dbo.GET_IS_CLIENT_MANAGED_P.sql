SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE dbo.GET_IS_CLIENT_MANAGED_P
	@client_id int
	AS
	begin
		set nocount on

		select	NOT_MANAGED 
		from	CLIENT 
		where	CLIENT_ID =  @client_id

	end
GO
GRANT EXECUTE ON  [dbo].[GET_IS_CLIENT_MANAGED_P] TO [CBMSApplication]
GO
