
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 dbo.Contract_Termination_Dtls_Sel_BY_Contract_Msg_Queue_Id_For_Resend    
 
DESCRIPTION:  

INPUT PARAMETERS:    
Name						DataType	Default			Description    
----------------------------------------------------------------    
@Contract_Id				INT
@Notification_Msg_Queue_Id	INT				

OUTPUT PARAMETERS:    
Name					DataType	Default			Description    
----------------------------------------------------------------    
 
USAGE EXAMPLES:    
----------------------------------------------------------------    
    
SELECT TOP 10 fvl.* FROM dbo.Contract_Notification_Log fvl 
		JOIN dbo.Notification_Msg_Queue nmq ON fvl.Notification_Msg_Queue_Id = nmq.Notification_Msg_Queue_Id
		JOIN dbo.Notification_Template nt ON nmq.Notification_Template_Id = nt.Notification_Template_Id
		JOIN Core.Client_Hier_Account cha ON fvl.Contract_Id = cha.Supplier_Contract_ID 
											AND fvl.Meter_Start_End_Date = cha.Supplier_Meter_Disassociation_Date
		WHERE nt.Notification_Type_Cd IN (102196,102197)

	SELECT * FROM  dbo.Notification_Template a JOIN dbo.Code c 
		ON a.Notification_Type_Cd = c.Code_Id

EXEC dbo.Contract_Termination_Dtls_Sel_BY_Contract_Msg_Queue_Id_For_Resend 119322,8140,26958
EXEC dbo.Contract_Termination_Dtls_Sel_BY_Contract_Msg_Queue_Id_For_Resend 126237,8142,26958
EXEC dbo.Contract_Termination_Dtls_Sel_BY_Contract_Msg_Queue_Id_For_Resend 119322,8325,26958

AUTHOR INITIALS:    
Initials	Name    
----------------------------------------------------------------    
RR			Raghu Reddy

MODIFICATIONS:
Initials	Date		Modification    
----------------------------------------------------------------    
RR			2016-06-06	Global Sourcing - Phase5 - GCS-985 Created
RR			2016-07-13	GCS - 5b - Added locale code
RR			2016-07-22	GCS-1179 - Modified to get supplier contact mapped to commodity, state and country  of the expiring contract 					
						
    
******/    
    
CREATE PROCEDURE [dbo].[Contract_Termination_Dtls_Sel_BY_Contract_Msg_Queue_Id_For_Resend]
      ( 
       @Contract_Id INT
      ,@Notification_Msg_Queue_Id INT
      ,@Analyst_Id INT = NULL )
AS 
BEGIN    
    
      SET NOCOUNT ON;    
      
      DECLARE
            @Analyst_Name VARCHAR(200)
           ,@Analyst_Email_Address VARCHAR(150)
           ,@Phone_Number VARCHAR(50)
            
        
      --SELECT
      --      @Execution_Date = ISNULL(@Execution_Date, CONVERT(DATETIME, CONVERT(VARCHAR(10), GETDATE(), 101))); 
      SELECT
            @Analyst_Id = ISNULL(@Analyst_Id, 49)

      DECLARE @Client_Con_Info TABLE
            ( 
             Client_Name VARCHAR(200)
            ,Client_Id INT
            ,Site_Name VARCHAR(800)
            ,Site_Id INT
            ,Account_Number VARCHAR(50)
            ,Account_Id INT
            ,Meter_Id INT
            ,Meter_Number VARCHAR(50)
            ,Contract_Id INT
            ,Ed_Contract_Number VARCHAR(150)
            ,Account_Vendor_Id INT
            ,Vendor_Name VARCHAR(200)
            ,Expiry_Date DATETIME
            ,Commodity_Type_id INT
            ,Commodity VARCHAR(50)
            ,Country VARCHAR(200) );     
            
      SELECT
            @Analyst_Name = ui.FIRST_NAME + ' ' + ui.LAST_NAME
           ,@Analyst_Email_Address = ui.EMAIL_ADDRESS
           ,@Phone_Number = ui.PHONE_NUMBER
      FROM
            dbo.USER_INFO ui
      WHERE
            ui.USER_INFO_ID = @Analyst_Id  
                  
---> Eligible for todays notification
      INSERT      INTO @Client_Con_Info
                  ( 
                   Client_Name
                  ,Client_Id
                  ,Site_Name
                  ,Site_Id
                  ,Account_Number
                  ,Account_Id
                  ,Meter_Id
                  ,Meter_Number
                  ,Contract_Id
                  ,Ed_Contract_Number
                  ,Account_Vendor_Id
                  ,Vendor_Name
                  ,Expiry_Date
                  ,Commodity_Type_id
                  ,Commodity
                  ,Country )
                  SELECT
                        ch.Client_Name
                       ,ch.Client_Id
                       ,RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' Site_Name
                       ,ch.Site_Id
                       ,chautil.Account_Number
                       ,chautil.Account_Id
                       ,chasupp.Meter_Id
                       ,chasupp.Meter_Number
                       ,con.CONTRACT_ID
                       ,con.ED_CONTRACT_NUMBER
                       ,chasupp.Account_Vendor_Id
                       ,chasupp.Account_Vendor_Name
                       ,chasupp.Supplier_Meter_Disassociation_Date AS Expiry_Date
                       ,chautil.Commodity_Id AS COMMODITY_TYPE_ID
                       ,com.Commodity_Name
                       ,chautil.Meter_Country_Name
                  FROM
                        Core.Client_Hier ch
                        INNER JOIN Core.Client_Hier_Account chautil
                              ON ch.Client_Hier_Id = chautil.Client_Hier_Id
                        INNER JOIN Core.Client_Hier_Account chasupp
                              ON chautil.Meter_Id = chasupp.Meter_Id
                        INNER JOIN dbo.CONTRACT con
                              ON chasupp.Supplier_Contract_ID = con.CONTRACT_ID
                        INNER JOIN dbo.Commodity com
                              ON con.COMMODITY_TYPE_ID = com.Commodity_Id
                  WHERE
                        chautil.Account_Type = 'Utility'
                        AND chasupp.Account_Type = 'Supplier'
                        --AND chasupp.Supplier_Meter_Disassociation_Date >= @Execution_Date
                        --AND con.CONTRACT_END_DATE >= @Execution_Date
                        AND con.Is_Notification_Required_On_Termination = 1
                        AND chasupp.Supplier_Contract_ID = @Contract_Id
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.Contract_Notification_Log fvl
                                     WHERE
                                          fvl.Contract_Id = @Contract_Id
                                          AND fvl.Notification_Msg_Queue_Id = @Notification_Msg_Queue_Id
                                          AND fvl.Contract_Id = chasupp.Supplier_Contract_ID
                                          AND fvl.Meter_Term_Dt = chasupp.Supplier_Meter_Disassociation_Date )
                  GROUP BY
                        ch.Client_Name
                       ,ch.Client_Id
                       ,ch.City
                       ,ch.State_Name
                       ,ch.Site_name
                       ,ch.Site_Id
                       ,chautil.Account_Number
                       ,chautil.Account_Id
                       ,chasupp.Meter_Id
                       ,chasupp.Meter_Number
                       ,con.CONTRACT_ID
                       ,con.ED_CONTRACT_NUMBER
                       ,chasupp.Account_Vendor_Id
                       ,chasupp.Account_Vendor_Name
                       ,chasupp.Supplier_Meter_Disassociation_Date
                       ,chautil.Commodity_Id
                       ,com.Commodity_Name
                       ,chautil.Meter_Country_Name;
                       
      WITH  Cte_Terminations
              AS ( SELECT
                        cn.Client_Name
                       ,cn.Site_Name
                       ,cn.Account_Number
                       ,cn.Account_Id
                       ,cn.Meter_Id
                       ,cn.Meter_Number
                       ,cn.Contract_Id
                       ,cn.Ed_Contract_Number
                       ,cn.Expiry_Date
                       ,cn.Account_Vendor_Id
                       ,cn.Vendor_Name
                       ,cn.Commodity
                       ,cn.Country
                   FROM
                        @Client_Con_Info cn
                   WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          Core.Client_Hier_Account samesupp
                                     WHERE
                                          samesupp.Meter_Id = cn.Meter_Id
                                          AND samesupp.Account_Vendor_Id = cn.Account_Vendor_Id
                                          AND samesupp.Supplier_Contract_ID <> cn.Contract_Id
                                          AND samesupp.Supplier_Meter_Association_Date = DATEADD(DAY, 1, cn.Expiry_Date) )
                   GROUP BY
                        cn.Client_Name
                       ,cn.Site_Name
                       ,cn.Account_Number
                       ,cn.Account_Id
                       ,cn.Meter_Id
                       ,cn.Meter_Number
                       ,cn.Contract_Id
                       ,cn.Ed_Contract_Number
                       ,cn.Expiry_Date
                       ,cn.Account_Vendor_Id
                       ,cn.Vendor_Name
                       ,cn.Commodity
                       ,cn.Country)
            SELECT
                  ctet.Contract_Id
                 ,ctet.Ed_Contract_Number
                 ,ctet.Expiry_Date
                 ,ctet.Client_Name
                 ,LEFT(mtr.mtrs, LEN(mtr.mtrs) - 1) AS Meter_Id
                 ,LEFT(mtrnum.mtrnums, LEN(mtrnum.mtrnums) - 1) AS Meter_Number
                 ,ctet.Vendor_Name
                 ,LEFT(vcui.vcuis, LEN(vcui.vcuis) - 1) AS Vendor_Contact_User_Info_Id
                 ,LEFT(tcmail.tcmails, LEN(tcmail.tcmails) - 1) AS Termination_Contact_Email_Address
                 ,@Analyst_Id AS Analyst_Id
                 ,@Analyst_Name AS Analyst_Name
                 ,@Analyst_Email_Address AS Analyst_Email_Address
                 ,@Phone_Number AS Phone_Number
                 ,ctet.Commodity
                 ,LEFT(con.cons, LEN(con.cons) - 1) AS Country
                 ,LEFT(locale.code, LEN(locale.code) - 1) AS Locale_Cd
            FROM
                  Cte_Terminations ctet
                  CROSS APPLY ( SELECT
                                    CAST(ct.Meter_Id AS VARCHAR(20)) + ','
                                FROM
                                    Cte_Terminations ct
                                WHERE
                                    ctet.Contract_Id = ct.Contract_Id
                                    AND ct.Meter_Id IS NOT NULL
                                GROUP BY
                                    ct.Meter_Id
                  FOR
                                XML PATH('') ) mtr ( mtrs )
                  CROSS APPLY ( SELECT
                                    ct.Meter_Number + ', '
                                FROM
                                    Cte_Terminations ct
                                WHERE
                                    ctet.Contract_Id = ct.Contract_Id
                                    AND ct.Meter_Number IS NOT NULL
                                GROUP BY
                                    ct.Meter_Number
                                   ,ct.Meter_Id
                  FOR
                                XML PATH('') ) mtrnum ( mtrnums )
                  CROSS APPLY ( SELECT
                                    CAST(ssci.USER_INFO_ID AS VARCHAR(20)) + ','
                                FROM
                                    dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP sscvm
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                                          ON sscvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO AS vci
                                          ON vci.USER_INFO_ID = ssci.USER_INFO_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP metermap
                                          ON metermap.SR_SUPPLIER_CONTACT_INFO_ID = sscvm.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN Core.Client_Hier_Account cha
                                          ON metermap.COUNTRY_ID = cha.Meter_Country_Id
                                             AND metermap.STATE_ID = cha.Meter_State_Id
                                             AND metermap.COMMODITY_TYPE_ID = cha.Commodity_Id
                                    INNER JOIN Cte_Terminations mtr
                                          ON mtr.Meter_Id = cha.Meter_Id
                                WHERE
                                    sscvm.IS_PRIMARY = 1
                                    AND ctet.Account_Vendor_Id = sscvm.VENDOR_ID
                                    AND ssci.Termination_Contact_Email_Address IS NOT NULL
                                    AND ssci.Termination_Contact_Email_Address <> ''
                                    AND vci.IS_HISTORY = 0
                                    AND metermap.IS_PRIMARY = 0
                                    AND cha.Supplier_Contract_ID = ctet.Contract_Id
                                GROUP BY
                                    ssci.USER_INFO_ID
                  FOR
                                XML PATH('') ) vcui ( vcuis )
                  CROSS APPLY ( SELECT
                                    ssci.Termination_Contact_Email_Address + ','
                                FROM
                                    dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP sscvm
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                                          ON sscvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO AS vci
                                          ON vci.USER_INFO_ID = ssci.USER_INFO_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP metermap
                                          ON metermap.SR_SUPPLIER_CONTACT_INFO_ID = sscvm.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN Core.Client_Hier_Account cha
                                          ON metermap.COUNTRY_ID = cha.Meter_Country_Id
                                             AND metermap.STATE_ID = cha.Meter_State_Id
                                             AND metermap.COMMODITY_TYPE_ID = cha.Commodity_Id
                                    INNER JOIN Cte_Terminations mtr
                                          ON mtr.Meter_Id = cha.Meter_Id
                                WHERE
                                    sscvm.IS_PRIMARY = 1
                                    AND ctet.Account_Vendor_Id = sscvm.VENDOR_ID
                                    AND ssci.Termination_Contact_Email_Address IS NOT NULL
                                    AND ssci.Termination_Contact_Email_Address <> ''
                                    AND vci.IS_HISTORY = 0
                                    AND metermap.IS_PRIMARY = 0
                                    AND cha.Supplier_Contract_ID = ctet.Contract_Id
                                GROUP BY
                                    ssci.Termination_Contact_Email_Address
                  FOR
                                XML PATH('') ) tcmail ( tcmails )
                  CROSS APPLY ( SELECT
                                    ct.Country + ','
                                FROM
                                    Cte_Terminations ct
                                WHERE
                                    ctet.Contract_Id = ct.Contract_Id
                                    AND ct.Country IS NOT NULL
                                GROUP BY
                                    ct.Country
                  FOR
                                XML PATH('') ) con ( cons )
                  CROSS APPLY ( SELECT TOP 1
                                    CAST(Code_Id AS VARCHAR(10)) + '~' + cd.Code_Value + ','
                                FROM
                                    dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP sscvm
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                                          ON sscvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO AS vci
                                          ON vci.USER_INFO_ID = ssci.USER_INFO_ID
                                    INNER JOIN dbo.Code cd
                                          ON cd.Code_Value = vci.locale_code
                                    INNER JOIN dbo.Codeset cs
                                          ON cd.Codeset_Id = cs.Codeset_Id
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP metermap
                                          ON metermap.SR_SUPPLIER_CONTACT_INFO_ID = sscvm.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN Core.Client_Hier_Account cha
                                          ON metermap.COUNTRY_ID = cha.Meter_Country_Id
                                             AND metermap.STATE_ID = cha.Meter_State_Id
                                             AND metermap.COMMODITY_TYPE_ID = cha.Commodity_Id
                                    INNER JOIN Cte_Terminations mtr
                                          ON mtr.Meter_Id = cha.Meter_Id
                                WHERE
                                    sscvm.IS_PRIMARY = 1
                                    AND ctet.Account_Vendor_Id = sscvm.VENDOR_ID
                                    AND ssci.Termination_Contact_Email_Address IS NOT NULL
                                    AND ssci.Termination_Contact_Email_Address <> ''
                                    AND vci.IS_HISTORY = 0
                                    AND cs.Codeset_Name = 'LocalizationLanguage'
                                    AND metermap.IS_PRIMARY = 0
                                    AND cha.Supplier_Contract_ID = ctet.Contract_Id
                  FOR
                                XML PATH('') ) locale ( code )
            GROUP BY
                  ctet.Contract_Id
                 ,ctet.Ed_Contract_Number
                 ,ctet.Expiry_Date
                 ,ctet.Client_Name
                 ,LEFT(mtr.mtrs, LEN(mtr.mtrs) - 1)
                 ,LEFT(mtrnum.mtrnums, LEN(mtrnum.mtrnums) - 1)
                 ,ctet.Vendor_Name
                 ,LEFT(vcui.vcuis, LEN(vcui.vcuis) - 1)
                 ,LEFT(tcmail.tcmails, LEN(tcmail.tcmails) - 1)
                 ,ctet.Commodity
                 ,LEFT(con.cons, LEN(con.cons) - 1)
                 ,LEFT(locale.code, LEN(locale.code) - 1); 
        

END;





;
GO

GRANT EXECUTE ON  [dbo].[Contract_Termination_Dtls_Sel_BY_Contract_Msg_Queue_Id_For_Resend] TO [CBMSApplication]
GO
