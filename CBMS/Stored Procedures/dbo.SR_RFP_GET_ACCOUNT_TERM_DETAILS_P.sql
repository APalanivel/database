
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_ACCOUNT_TERM_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@rfp_id        	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

 exec dbo.SR_RFP_GET_ACCOUNT_TERM_DETAILS_P 138
 
 exec SR_RFP_GET_ACCOUNT_TERM_DETAILS_P 13287

AUTHOR INITIALS:
Initials		Name
------------------------------------------------------------
NR				Narayana Reddy


MODIFICATIONS

Initials		Date			Modification
------------------------------------------------------------
	        	9/21/2010		Modify Quoted Identifier
 DMR			09/10/2010		Modified for Quoted_Identifier
 NR				2016-03-31		Gloabl Sourcing Phase-3 GCS-614 Added Account_Id in Output List.


******/

CREATE  PROCEDURE [dbo].[SR_RFP_GET_ACCOUNT_TERM_DETAILS_P] ( @rfp_id INT )
AS 
BEGIN
      SET NOCOUNT ON
      ( SELECT
            term.sr_rfp_term_id
           ,account_term.SR_ACCOUNT_GROUP_ID AS SR_ACCOUNT_GROUP_ID
           ,account_term.is_bid_group
           ,account_term.from_month
           ,account_term.to_month
           ,account_term.no_of_months
           ,rfp_acc.ACCOUNT_ID
        FROM
            sr_rfp_account_term account_term
            INNER JOIN sr_rfp_term term
                  ON account_term.sr_rfp_term_id = term.sr_rfp_term_id
            INNER JOIN sr_rfp_account rfp_acc
                  ON rfp_acc.sr_rfp_bid_group_id = account_term.SR_ACCOUNT_GROUP_ID
        WHERE
            term.sr_rfp_id = @rfp_id
            AND account_term.is_bid_group = 1
            AND rfp_acc.is_deleted = 0)
      UNION
      ( SELECT
            term.sr_rfp_term_id
           ,rfp_acc.account_id AS SR_ACCOUNT_GROUP_ID
           ,account_term.is_bid_group
           ,account_term.from_month
           ,account_term.to_month
           ,account_term.no_of_months
           ,rfp_acc.ACCOUNT_ID
        FROM
            sr_rfp_account_term account_term
            INNER JOIN sr_rfp_term term
                  ON account_term.sr_rfp_term_id = term.sr_rfp_term_id
            INNER JOIN sr_rfp_account rfp_acc
                  ON rfp_acc.sr_rfp_account_id = account_term.SR_ACCOUNT_GROUP_ID
        WHERE
            term.sr_rfp_id = @rfp_id
            AND account_term.is_bid_group = 0
            AND rfp_acc.is_deleted = 0)
      ORDER BY
            term.sr_rfp_term_id
END
;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_GET_ACCOUNT_TERM_DETAILS_P] TO [CBMSApplication]
GO
