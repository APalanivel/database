SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********   
NAME:  dbo..Account_Consumption_Level_SEL  
 
DESCRIPTION:  Used to select consumption level  set to account

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
@AccountId				Int
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:  
	
	dbo.Account_Consumption_Level_SEL 141351

------------------------------------------------------------  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
NK   Nageswara Rao Kosuri         


Initials Date  Modification  
------------------------------------------------------------  
NK	10/23/2009  Created


******/  


CREATE PROCEDURE dbo.Account_Consumption_Level_SEL
@AccountId int
		
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT vcl.Variance_Consumption_Level_Id
		, vcl.Consumption_Level_Desc
		,vcl.Commodity_Id
		,com.Commodity_Name
	FROM Account a
		Join Account_Variance_Consumption_Level avcl ON avcl.Account_Id = a.Account_Id
		Join Variance_Consumption_Level vcl ON vcl.Variance_Consumption_Level_Id = avcl.Variance_Consumption_Level_Id		
		Join Commodity com on com.Commodity_Id = vcl.Commodity_Id
	
	WHERE a.Account_Id = @AccountId

	
END
GO
GRANT EXECUTE ON  [dbo].[Account_Consumption_Level_SEL] TO [CBMSApplication]
GO
