SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[cbmsUbmUnitOfMeasureMap_Get]
	( @MyAccountId int
	, @ubm_unit_of_measure_map_id int
	)
AS
BEGIN

	   select ubm_unit_of_measure_map_id
		, ubm_id
		, ubm_unit_of_measure_code
		, commodity_type_id
		, unit_of_measure_type_id
	     from ubm_unit_of_measure_map
	    where ubm_unit_of_measure_map_id = @ubm_unit_of_measure_map_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsUbmUnitOfMeasureMap_Get] TO [CBMSApplication]
GO
