
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.Account_DMO_Config_Affected_Entities
           
DESCRIPTION:             
			To insert entity DMO configurations
			
INPUT PARAMETERS:            
	Name				DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Client_Hier_Id		INT
    @Commodity_Id		INT
    @DMO_Start_Dt		DATE
    @DMO_End_Dt			DATE
    @User_Info_Id		INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT TOP 10 * FROM dbo.Account_DMO_Config
            
	EXEC dbo.Account_DMO_Config_Affected_Entities 63
	EXEC dbo.Account_DMO_Config_Affected_Entities 49
		
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-01-27	Contract placeholder - CP-7 Created
	RR			2017-05-02	MAINT-5292 Config dates to defaulted first day of the month, validation fails if account end date is other than first day, 
							so end date reset to last day of the month
******/

CREATE PROCEDURE [dbo].[Account_DMO_Config_Affected_Entities]
      ( 
       @Account_DMO_Config_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE
            @Account_Id INT
           ,@Commodity_Id INT
           ,@Commodity_Name VARCHAR(50)
      
      SELECT
            @Account_Id = adc.Account_Id
           ,@Commodity_Id = adc.Commodity_Id
      FROM
            dbo.Account_DMO_Config adc
      WHERE
            adc.Account_DMO_Config_Id = @Account_DMO_Config_Id
            
      SELECT
            @Commodity_Name = Commodity_Name
      FROM
            dbo.Commodity
      WHERE
            Commodity_Id = @Commodity_Id
            
     
      SELECT
            ch.Client_Name
           ,ch.Sitegroup_Name
           ,ch.Site_name
           ,@Commodity_Name AS Commodity
           ,utlt.Account_Number AS Utility_Account_Number
           ,supp.Account_Number AS Supplier_Account_Number
           ,supp.Supplier_Account_begin_Dt
           ,supp.Supplier_Account_End_Dt
           ,CONVERT(VARCHAR(10), cism.SERVICE_MONTH, 101) AS Invoice_Month
           ,cism.CU_INVOICE_ID AS Invoice_Id
      FROM
            Core.Client_Hier_Account utlt
            INNER JOIN Core.Client_Hier ch
                  ON utlt.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN Core.Client_Hier_Account supp
                  ON utlt.Meter_Id = supp.Meter_Id
            LEFT JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                  ON supp.Account_Id = cism.Account_ID
      WHERE
            utlt.Account_Id = @Account_Id
            AND utlt.Account_Type = 'Utility'
            AND utlt.Commodity_Id = @Commodity_Id
            AND supp.Account_Type = 'Supplier'
            AND supp.Supplier_Contract_ID = -1
            AND EXISTS ( SELECT
                              1
                         FROM
                              dbo.Account_DMO_Config cdc
                         WHERE
                              cdc.Account_DMO_Config_Id = @Account_DMO_Config_Id
                              AND ( ( cdc.DMO_End_Dt IS NULL
                                      AND supp.Supplier_Account_End_Dt IS NULL
                                      AND cdc.DMO_Start_Dt <= supp.Supplier_Account_begin_Dt )
                                    OR ( cdc.DMO_End_Dt IS NULL
                                         AND supp.Supplier_Account_End_Dt IS NOT NULL
                                         AND cdc.DMO_Start_Dt <= supp.Supplier_Account_begin_Dt
                                         AND cdc.DMO_Start_Dt <= supp.Supplier_Account_End_Dt )
                                    OR ( cdc.DMO_End_Dt IS NOT NULL
                                         AND supp.Supplier_Account_End_Dt IS NOT NULL
                                         AND supp.Supplier_Account_begin_Dt BETWEEN cdc.DMO_Start_Dt
                                                                            AND     DATEADD(DD, -DATEPART(DD, cdc.DMO_End_Dt), DATEADD(mm, 1, cdc.DMO_End_Dt))
                                         AND supp.Supplier_Account_End_Dt BETWEEN cdc.DMO_Start_Dt
                                                                          AND     DATEADD(DD, -DATEPART(DD, cdc.DMO_End_Dt), DATEADD(mm, 1, cdc.DMO_End_Dt)) ) ) )
      GROUP BY
            ch.Client_Name
           ,ch.Sitegroup_Name
           ,ch.Site_name
           ,utlt.Account_Number
           ,supp.Account_Number
           ,supp.Supplier_Account_begin_Dt
           ,supp.Supplier_Account_End_Dt
           ,CONVERT(VARCHAR(10), cism.SERVICE_MONTH, 101)
           ,cism.CU_INVOICE_ID
           
END;
;
GO

GRANT EXECUTE ON  [dbo].[Account_DMO_Config_Affected_Entities] TO [CBMSApplication]
GO
