SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.[cbmsGroupInfoPermissionInfo_Save]


DESCRIPTION: 


INPUT PARAMETERS:    
      Name                  DataType          Default     Description    
------------------------------------------------------------------    
	 @MyAccountId              int
	 @group_info_id            int
	 @permission_info_id       int
        
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- insert new entry using the procedure
--exec  cbmsGroupInfoPermissionInfo_Save
--	 @MyAccountId =1
--	, @group_info_id =117
--	, @permission_info_id =348


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates
	   DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[cbmsGroupInfoPermissionInfo_Save]
	( @MyAccountId int
	, @group_info_id int
	, @permission_info_id int
	)
AS

BEGIN

	set nocount on

	declare @RecordCount int

	  select @RecordCount = count(*)
	    from group_info_permission_info_map
	   where group_info_id = @group_info_id
	     and permission_info_id = @permission_info_id

	if @RecordCount = 0
	begin


		insert into group_info_permission_info_map
			( group_info_id, permission_info_id )
		values
			( @group_info_id, @permission_info_id )

	end

	exec cbmsGroupInfoPermissionInfo_Get @MyAccountId, @group_info_id, @permission_info_id
	
END
GO
GRANT EXECUTE ON  [dbo].[cbmsGroupInfoPermissionInfo_Save] TO [CBMSApplication]
GO
