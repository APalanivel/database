SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME:	dbo.Budget_Currency_Changes_Sel_By_Audit_Ts

DESCRIPTION:
	Gets all changes made between the Audit_Start_Ts and the Audit_End_Ts. 
	
INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@Audit_Start_Ts		DATETIME		Minimum Audit Start Time stamp
	@Audit_End_Ts		DATETIME		Maximum Audit End Time stamp   	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	DECLARE @Cur_Ts DateTime = GETDATE()

	EXEC dbo.Budget_Currency_Changes_Sel_By_Audit_Ts '4/29/2010', @Cur_Ts

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	PNR			06/11/2010	Created
	
******/
         
CREATE PROCEDURE dbo.Budget_Currency_Changes_Sel_By_Audit_Ts
(    
	 @Audit_Start_Ts DATETIME    
	,@Audit_End_Ts	 DATETIME    
)    
AS     
BEGIN     
	SET NOCOUNT ON;     
	
	DECLARE @Base_Unit_Id	INT

	DECLARE @Budget_Audit TABLE(Audit_Function SMALLINT,Budget_Id INT)
	DECLARE @Budget_Currency_Map_Audit TABLE(Budget_Currency_Map_Id INT,Budget_Id INT,Currency_Unit_Id INT, Audit_Function SMALLINT)

	DECLARE @T1 TABLE
	(	 Budget_ID			INT
		,Currency_Unit_Id	INT
		,Conversion_Factor	DECIMAL(32,16)
		,Source_Column		VARCHAR(25)
		,Audit_Function		INT
	)

	INSERT INTO @Budget_Audit
	(
		 Budget_Id
		,Audit_Function
	)
	SELECT
		badt.Budget_Id
	   ,badt.Audit_Function
	FROM
		dbo.Budget_Audit badt
		JOIN
		(
		SELECT
			 Budget_Id
			,MAX(Audit_Ts) Max_Audit_Ts
		FROM
			dbo.Budget_Audit
		WHERE
			Audit_Ts BETWEEN @Audit_Start_Ts
							AND @Audit_End_Ts
		GROUP BY
			Budget_Id

		) maxadt
		ON maxadt.Budget_id = badt.Budget_id
			  AND maxadt.Max_Audit_Ts = badt.Audit_Ts

	INSERT INTO @Budget_Currency_Map_Audit
	(
		Budget_Currency_Map_Id
		,Budget_Id
		,Currency_Unit_Id
		,Audit_Function
	)
	SELECT
		bcma.Budget_Currency_Map_Id
		,bcma.Budget_Id
		,bcma.Currency_Unit_Id
		,bcma.Audit_Function
	FROM
		dbo.Budget_Currency_Map_Audit bcma
		JOIN 
		(
			SELECT
				Budget_Currency_Map_Id
				,MAX(Audit_Ts) Max_Audit_Ts
			FROM
				dbo.Budget_Currency_Map_Audit
			WHERE
				Audit_Ts BETWEEN @Audit_Start_Ts
								AND @Audit_End_Ts
			GROUP BY
				Budget_Currency_Map_Id
		) maxadt
		ON maxadt.Budget_Currency_Map_Id = bcma.Budget_Currency_Map_Id
			AND maxadt.Max_Audit_Ts = bcma.Audit_Ts

	INSERT @T1 (Budget_Id, Currency_Unit_Id, Conversion_Factor, Source_Column,Audit_Function)
	SELECT
		 bcma.BUDGET_ID
		,bcma.CURRENCY_UNIT_ID
		,MAX(BCM.CONVERSION_FACTOR)
		,N'BUDGET_CURRENCY_MAP'
		,bcma.Audit_Function
	FROM
		@Budget_Currency_Map_Audit bcma
		LEFT JOIN dbo.BUDGET_CURRENCY_MAP BCM
			ON BCM.BUDGET_CURRENCY_MAP_ID = bcma.Budget_Currency_Map_Id
		JOIN dbo.BUDGET B
			ON B.BUDGET_ID = BCM.BUDGET_ID
				AND B.IS_POSTED_TO_DV = 1
	GROUP BY
		 bcma.BUDGET_ID
		,bcma.CURRENCY_UNIT_ID
		,bcma.Audit_Function

	SELECT
		 @Base_Unit_Id = CURRENCY_UNIT_ID
	FROM
		Currency_Unit
	WHERE
		Symbol = 'USD'

	INSERT @T1 (Budget_Id, Currency_Unit_Id, Conversion_Factor, Source_Column,Audit_Function)
	SELECT
		 bd.Budget_Id
		,CUC.Converted_Unit_Id
		,CUC.Conversion_Factor
		,N'CURRENCY_UNIT_CONVERSION'
		,bd.Audit_Function
	FROM
		@Budget_Audit bd
		JOIN dbo.Budget B
			ON bd.Budget_Id = B.BUDGET_ID
		JOIN dbo.CLIENT C
			ON	C.Client_Id = B.Client_Id
		JOIN dbo.Currency_Unit_Conversion CUC
			ON	CUC.Currency_Group_Id = C.Currency_Group_Id
	WHERE
		CUC.Conversion_Date = CONVERT(DATETIME, CONVERT(VARCHAR,b.budget_start_year))
		AND B.is_Posted_to_DV = 1
		AND CUC.Base_Unit_ID = @Base_Unit_Id
		AND NOT EXISTS (SELECT 1
							FROM @t1
							WHERE Budget_Id = b.Budget_Id
								AND Currency_Unit_Id = cuc.Converted_Unit_id)

	INSERT @T1 (Budget_Id, Currency_Unit_Id, Conversion_Factor, Source_Column,Audit_Function)
	SELECT
		 bd.BUDGET_ID
		,@Base_Unit_Id   AS CURRENCY_UNIT_ID
		,1				 AS CONVERSION_FACTOR
		,N'Default'
		,bd.Audit_Function
	FROM
		@Budget_Audit bd
		LEFT JOIN (
					dbo.Budget B
			 		CROSS JOIN Currency_Unit cu)
					ON bd.Budget_Id=B.BUDGET_ID
						AND B.Is_Posted_To_DV = 1
						AND cu.Symbol = 'USD'
	WHERE
		NOT EXISTS (SELECT 1
							FROM @T1
							WHERE Budget_ID = B.Budget_ID
								AND Currency_Unit_Id = @Base_Unit_Id )
	SELECT
		 Budget_ID
		,Currency_Unit_Id
		,Source_Column
		,Conversion_Factor
		,Audit_Function
	FROM
		@T1
	ORDER BY
		Budget_ID
		,Currency_Unit_Id
		,Source_Column
		,Conversion_Factor
		,Audit_Function

END
GO
GRANT EXECUTE ON  [dbo].[Budget_Currency_Changes_Sel_By_Audit_Ts] TO [CBMSApplication]
GO
