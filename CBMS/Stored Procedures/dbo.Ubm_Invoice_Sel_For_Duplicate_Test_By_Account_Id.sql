SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	cbms_prod.dbo.Ubm_Invoice_Sel_For_Duplicate_Test_By_Account_Id

DESCRIPTION:

	This procedure will fetch the duplicate invoices matches with the given account_id , ubm_invoice_id and the date range.
	
INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@Account_id			INT
	@ubm_invoice_id		int       	          	
	@Service_Begin_Dt   date
	@Service_End_Dt     date

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Ubm_Invoice_Sel_For_Duplicate_Test_By_Account_Id 24222, 350446,'2006-06-01','2006-06-30'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan G

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG			06/15/2010	Created based on cbmsUbmInvoice_GetProcessedForDuplicateTest
	HG			06/21/2010	As Account_id column moved to Cu_Invoice_Service_Month from Cu_Invoice added it in query to filter the account_id.
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.Ubm_Invoice_Sel_For_Duplicate_Test_By_Account_Id
	(
	@Account_Id			INT	
	,@Ubm_Invoice_Id	INT 
	,@Service_Begin_Dt	DATE
	,@Service_End_Dt	DATE
	)
AS

BEGIN

	SET NOCOUNT ON

	DECLARE @Start_Dt_Begin_Range	DATE
		,@Start_Dt_End_Range		DATE
		,@End_Dt_Begin_Range		DATE
		,@End_Dt_End_Range			DATE

	SELECT
		@Start_Dt_Begin_Range = DATEADD(D,-1, @Service_Begin_Dt)
		,@Start_Dt_End_Range = DATEADD(D,1, @Service_Begin_Dt)
		,@End_Dt_Begin_Range = DATEADD(D,-1, @Service_End_Dt)
		,@End_Dt_End_Range = DATEADD(D,1, @Service_End_Dt)

	SELECT
		i.ubm_invoice_id
		,i.ubm_account_id
		,i.currency ubm_currency_code
		,cu.cu_invoice_id
		,det.service_start_date
		,det.SERVICE_END_DATE
	FROM
		dbo.ubm_invoice i
		JOIN dbo.cu_invoice cu
			ON cu.ubm_invoice_id = i.ubm_invoice_id
		JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
			ON cism.CU_INVOICE_ID = cu.CU_INVOICE_ID
		JOIN dbo.UBM_INVOICE_DETAILS det
			ON det.ubm_invoice_id = i.ubm_invoice_id
	WHERE
		cism.ACCOUNT_ID = @Account_Id
		AND i.UBM_INVOICE_ID != @ubm_invoice_id
		AND i.UBM_ACCOUNT_ID IS NOT NULL
		AND i.IS_PROCESSED = 1
		AND cu.IS_DUPLICATE = 0
		AND cu.IS_DNT = 0
		AND (
				
				(
					det.SERVICE_START_DATE = @Service_Begin_Dt 
					AND (det.SERVICE_END_DATE BETWEEN @End_Dt_Begin_Range
														AND @End_Dt_End_Range
						)
				)
				OR
				
				(
					det.SERVICE_END_DATE = @Service_End_Dt 
					AND (det.SERVICE_START_DATE BETWEEN @Start_Dt_Begin_Range
														AND @Start_Dt_End_Range
						)
				)
		
			)
	GROUP BY
		i.ubm_invoice_id
		,i.ubm_account_id
		,i.currency
		,cu.cu_invoice_id
		,det.service_start_date
		,det.SERVICE_END_DATE

END
GO
GRANT EXECUTE ON  [dbo].[Ubm_Invoice_Sel_For_Duplicate_Test_By_Account_Id] TO [CBMSApplication]
GO
