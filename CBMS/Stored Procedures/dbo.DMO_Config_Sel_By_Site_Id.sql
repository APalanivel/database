SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.DMO_Supplier_Account_Dates_Validation
           
DESCRIPTION:             
			
			
INPUT PARAMETERS:            
	Name				DataType			Default				Description  
---------------------------------------------------------------------------------
	@Site_Id			INT
   
    
OUTPUT PARAMETERS:
	Name				DataType			Default				Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  

select * from core.client_hier where client_name ='bs client'

	EXEC dbo.DMO_Config_Sel_By_Site_Id 39717
	EXEC dbo.DMO_Config_Sel_By_Site_Id 39993


		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	NR			Narayana Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	NR			2019-06-11	Add Contract - Created
******/

CREATE PROCEDURE [dbo].[DMO_Config_Sel_By_Site_Id]
    (
        @Site_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;


        DECLARE
            @Sitegroup_Id INT
            , @Client_Id INT;

        DECLARE
            @Sitegroup_Hier_Level_Cd INT
            , @Division_Hier_Level_Cd INT
            , @Site_Hier_Level_Cd INT
            , @Corporate_Hier_Level_Cd INT;

        SELECT
            @Sitegroup_Hier_Level_Cd = MAX(CASE WHEN c.Code_Value = 'Site Group' THEN c.Code_Id
                                           END)
            , @Division_Hier_Level_Cd = MAX(CASE WHEN c.Code_Value = 'Division' THEN c.Code_Id
                                            END)
            , @Site_Hier_Level_Cd = MAX(CASE WHEN c.Code_Value = 'Site' THEN c.Code_Id
                                        END)
            , @Corporate_Hier_Level_Cd = MAX(CASE WHEN c.Code_Value = 'Corporate' THEN c.Code_Id
                                             END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Std_Column_Name = 'Hier_Level_Cd'
            AND cs.Codeset_Name = 'HierLevel'
            AND c.Code_Value IN ( 'Site Group', 'Division', 'Site', 'Corporate' );


        SELECT
            @Client_Id = NULLIF(ch.Client_Id, 0)
            , @Sitegroup_Id = NULLIF(ch.Sitegroup_Id, 0)
            , @Site_Id = NULLIF(ch.Site_Id, 0)
        FROM
            Core.Client_Hier ch
        WHERE
            ch.Site_Id = @Site_Id;

        DECLARE @Tbl_Config AS TABLE
              (
                  Client_Hier_DMO_Config_Id INT
                  , Client_Hier_Id INT
                  , Hier_level_Cd INT
                  , Account_Id INT
                  , Account_DMO_Config_Id INT
                  , Commodity_Id INT
                  , DMO_Start_Dt VARCHAR(10)
                  , DMO_End_Dt VARCHAR(10)
              );



        CREATE TABLE #Account_List
             (
                 Account_Id INT
                 , Commodity_Id INT
                 , DMO_Start_Dt DATE
                 , DMO_End_Dt DATE
             );



        INSERT INTO @Tbl_Config
             (
                 Client_Hier_DMO_Config_Id
                 , Client_Hier_Id
                 , Hier_level_Cd
                 , Account_Id
                 , Account_DMO_Config_Id
                 , Commodity_Id
                 , DMO_Start_Dt
                 , DMO_End_Dt
             )
        SELECT
            chdc.Client_Hier_DMO_Config_Id
            , chdc.Client_Hier_Id
            , ch.Hier_level_Cd
            , NULL AS Account_Id
            , NULL AS Account_DMO_Config_Id
            , chdc.Commodity_Id
            , chdc.DMO_Start_Dt AS DMO_Start_Dt
            , chdc.DMO_End_Dt AS DMO_End_Dt
        FROM
            dbo.Client_Hier_DMO_Config chdc
            INNER JOIN Core.Client_Hier ch
                ON chdc.Client_Hier_Id = ch.Client_Hier_Id
        WHERE
            ch.Client_Id = @Client_Id
            AND ch.Sitegroup_Id = 0
            AND ch.Site_Id = 0
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Client_Hier_Not_Applicable_DMO_Config na
                                    INNER JOIN Core.Client_Hier ch
                                        ON na.Client_Hier_Id = ch.Client_Hier_Id
                               WHERE
                                    na.Client_Hier_DMO_Config_Id = chdc.Client_Hier_DMO_Config_Id
                                    AND ch.Client_Id = @Client_Id
                                    AND ch.Sitegroup_Id = 0
                                    AND ch.Site_Id = 0);



        INSERT INTO @Tbl_Config
             (
                 Client_Hier_DMO_Config_Id
                 , Client_Hier_Id
                 , Hier_level_Cd
                 , Account_Id
                 , Account_DMO_Config_Id
                 , Commodity_Id
                 , DMO_Start_Dt
                 , DMO_End_Dt
             )
        SELECT
            chdc.Client_Hier_DMO_Config_Id
            , chdc.Client_Hier_Id
            , ch.Hier_level_Cd
            , NULL AS Account_Id
            , NULL AS Account_DMO_Config_Id
            , chdc.Commodity_Id
            , chdc.DMO_Start_Dt AS DMO_Start_Dt
            , chdc.DMO_End_Dt AS DMO_End_Dt
        FROM
            dbo.Client_Hier_DMO_Config chdc
            INNER JOIN Core.Client_Hier ch
                ON chdc.Client_Hier_Id = ch.Client_Hier_Id
        WHERE
            ch.Client_Id = @Client_Id
            AND ch.Sitegroup_Id = @Sitegroup_Id
            AND ch.Site_Id = 0
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Client_Hier_Not_Applicable_DMO_Config na
                                    INNER JOIN Core.Client_Hier ch
                                        ON na.Client_Hier_Id = ch.Client_Hier_Id
                               WHERE
                                    na.Client_Hier_DMO_Config_Id = chdc.Client_Hier_DMO_Config_Id
                                    AND ch.Client_Id = @Client_Id
                                    AND ch.Sitegroup_Id = @Sitegroup_Id
                                    AND ch.Site_Id = 0);


        INSERT INTO @Tbl_Config
             (
                 Client_Hier_DMO_Config_Id
                 , Client_Hier_Id
                 , Hier_level_Cd
                 , Account_Id
                 , Account_DMO_Config_Id
                 , Commodity_Id
                 , DMO_Start_Dt
                 , DMO_End_Dt
             )
        SELECT
            chdc.Client_Hier_DMO_Config_Id
            , chdc.Client_Hier_Id
            , ch.Hier_level_Cd
            , NULL AS Account_Id
            , NULL AS Account_DMO_Config_Id
            , chdc.Commodity_Id
            , chdc.DMO_Start_Dt AS DMO_Start_Dt
            , chdc.DMO_End_Dt AS DMO_End_Dt
        FROM
            dbo.Client_Hier_DMO_Config chdc
            INNER JOIN Core.Client_Hier ch
                ON chdc.Client_Hier_Id = ch.Client_Hier_Id
        WHERE
            ch.Client_Id = @Client_Id
            AND ch.Sitegroup_Id = @Sitegroup_Id
            AND ch.Site_Id = @Site_Id
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Client_Hier_Not_Applicable_DMO_Config na
                                    INNER JOIN Core.Client_Hier ch
                                        ON na.Client_Hier_Id = ch.Client_Hier_Id
                               WHERE
                                    na.Client_Hier_DMO_Config_Id = chdc.Client_Hier_DMO_Config_Id
                                    AND @Client_Id IS NOT NULL
                                    AND @Sitegroup_Id IS NOT NULL
                                    AND @Site_Id IS NOT NULL
                                    AND ch.Client_Id = @Client_Id
                                    AND ch.Sitegroup_Id = @Sitegroup_Id
                                    AND ch.Site_Id = @Site_Id);



        ------------------------------------ To Load the Account Level set up------------------------------------------------------------  


        INSERT INTO #Account_List
             (
                 Account_Id
                 , Commodity_Id
                 , DMO_Start_Dt
                 , DMO_End_Dt
             )
        SELECT
            adc.Account_Id
            , adc.Commodity_Id
            , adc.DMO_Start_Dt
            , ISNULL(
                  CAST(DATEADD(DD, -DATEPART(DD, adc.DMO_End_Dt), DATEADD(mm, 1, adc.DMO_End_Dt)) AS DATE)
                  , '9999-12-31')
        FROM
            dbo.Account_DMO_Config adc
            INNER JOIN Core.Client_Hier_Account cha
                ON adc.Account_Id = cha.Account_Id
            INNER JOIN Core.Client_Hier ch
                ON cha.Client_Hier_Id = ch.Client_Hier_Id
        WHERE
            ch.Client_Id = @Client_Id
            AND ch.Site_Id = @Site_Id
        GROUP BY
            adc.Account_Id
            , adc.Commodity_Id
            , adc.DMO_Start_Dt
            , ISNULL(
                  CAST(DATEADD(DD, -DATEPART(DD, adc.DMO_End_Dt), DATEADD(mm, 1, adc.DMO_End_Dt)) AS DATE)
                  , '9999-12-31');
        ------------------------------------ To Load the Site level setup accounts------------------------------------------------------------  


        INSERT INTO #Account_List
             (
                 Account_Id
                 , Commodity_Id
                 , DMO_Start_Dt
                 , DMO_End_Dt
             )
        SELECT
            cha.Account_Id
            , cha.Commodity_Id
            , tc.DMO_Start_Dt
            , ISNULL(
                  CAST(DATEADD(DD, -DATEPART(DD, tc.DMO_End_Dt), DATEADD(mm, 1, tc.DMO_End_Dt)) AS DATE), '9999-12-31')
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN @Tbl_Config tc
                ON tc.Client_Hier_Id = cha.Client_Hier_Id
                   AND  tc.Commodity_Id = cha.Commodity_Id
        WHERE
            tc.Hier_level_Cd = @Site_Hier_Level_Cd
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #Account_List al
                               WHERE
                                    al.Account_Id = cha.Account_Id
                                    AND al.Commodity_Id = cha.Commodity_Id)
        GROUP BY
            cha.Account_Id
            , cha.Commodity_Id
            , tc.DMO_Start_Dt
            , ISNULL(
                  CAST(DATEADD(DD, -DATEPART(DD, tc.DMO_End_Dt), DATEADD(mm, 1, tc.DMO_End_Dt)) AS DATE), '9999-12-31');

        ------------------------------------ To Load the Sitegroup level setup accounts------------------------------------------------------------  


        INSERT INTO #Account_List
             (
                 Account_Id
                 , Commodity_Id
                 , DMO_Start_Dt
                 , DMO_End_Dt
             )
        SELECT
            cha2.Account_Id
            , cha2.Commodity_Id
            , tc.DMO_Start_Dt
            , ISNULL(
                  CAST(DATEADD(DD, -DATEPART(DD, tc.DMO_End_Dt), DATEADD(mm, 1, tc.DMO_End_Dt)) AS DATE), '9999-12-31')
        FROM
            Core.Client_Hier ch
            INNER JOIN @Tbl_Config tc
                ON tc.Client_Hier_Id = ch.Client_Hier_Id
                   AND  tc.Hier_level_Cd = ch.Hier_level_Cd
            INNER JOIN Core.Client_Hier ch_div
                ON ch_div.Sitegroup_Id = ch.Sitegroup_Id
            INNER JOIN Core.Client_Hier_Account cha2
                ON ch_div.Client_Hier_Id = cha2.Client_Hier_Id
                   AND  tc.Commodity_Id = cha2.Commodity_Id
        WHERE
            ch.Hier_level_Cd IN ( @Sitegroup_Hier_Level_Cd, @Division_Hier_Level_Cd )
            AND ch_div.Site_Id > 0
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #Account_List al
                               WHERE
                                    al.Account_Id = cha2.Account_Id
                                    AND al.Commodity_Id = cha2.Commodity_Id)
        GROUP BY
            cha2.Account_Id
            , cha2.Commodity_Id
            , tc.DMO_Start_Dt
            , ISNULL(
                  CAST(DATEADD(DD, -DATEPART(DD, tc.DMO_End_Dt), DATEADD(mm, 1, tc.DMO_End_Dt)) AS DATE), '9999-12-31');
        ------------------------------------ To Load the Client level setup accounts------------------------------------------------------------  


        INSERT INTO #Account_List
             (
                 Account_Id
                 , Commodity_Id
                 , DMO_Start_Dt
                 , DMO_End_Dt
             )
        SELECT
            cha3.Account_Id
            , cha3.Commodity_Id
            , tc.DMO_Start_Dt
            , ISNULL(
                  CAST(DATEADD(DD, -DATEPART(DD, tc.DMO_End_Dt), DATEADD(mm, 1, tc.DMO_End_Dt)) AS DATE), '9999-12-31')
        FROM
            Core.Client_Hier ch
            INNER JOIN @Tbl_Config tc
                ON tc.Client_Hier_Id = ch.Client_Hier_Id
                   AND  tc.Hier_level_Cd = ch.Hier_level_Cd
            INNER JOIN Core.Client_Hier ch_Cli
                ON ch_Cli.Client_Id = ch.Client_Id
            INNER JOIN Core.Client_Hier_Account cha3
                ON ch_Cli.Client_Hier_Id = cha3.Client_Hier_Id
                   AND  tc.Commodity_Id = cha3.Commodity_Id
        WHERE
            ch.Hier_level_Cd = @Corporate_Hier_Level_Cd
            AND ch_Cli.Site_Id > 0
            AND ch_Cli.Sitegroup_Id > 0
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #Account_List al
                               WHERE
                                    al.Account_Id = cha3.Account_Id
                                    AND al.Commodity_Id = cha3.Commodity_Id)
        GROUP BY
            cha3.Account_Id
            , cha3.Commodity_Id
            , tc.DMO_Start_Dt
            , ISNULL(
                  CAST(DATEADD(DD, -DATEPART(DD, tc.DMO_End_Dt), DATEADD(mm, 1, tc.DMO_End_Dt)) AS DATE), '9999-12-31');






        SELECT
            al.Account_Id
            , al.Commodity_Id
            , al.DMO_Start_Dt
            , al.DMO_End_Dt
        FROM
            #Account_List al;



    END;



GO
GRANT EXECUTE ON  [dbo].[DMO_Config_Sel_By_Site_Id] TO [CBMSApplication]
GO
