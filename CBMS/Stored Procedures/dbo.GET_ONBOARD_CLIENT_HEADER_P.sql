SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_ONBOARD_CLIENT_HEADER_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@clientId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC GET_ONBOARD_CLIENT_HEADER_P 1,1,12068

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
				9/21/2010	Modify Quoted Identifier
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2018-09-14	Global Risk Management  - Modified, removed old on-board references

******/

CREATE  PROCEDURE [dbo].[GET_ONBOARD_CLIENT_HEADER_P]
      ( 
       @userId VARCHAR(10)
      ,@sessionId VARCHAR(20)
      ,@clientId INT )
AS 
BEGIN
      SET NOCOUNT ON;
      
      DECLARE @Client_Hier_Id INT
      
      
      
      SELECT
            clientType.entity_name type
           ,fiscal.entity_name
           ,NULL AS max_hedge_percent
           ,div.trigger_rights
      FROM
            CLIENT cli
            INNER JOIN ENTITY clientType
                  ON cli.client_type_id = clientType.entity_id
            INNER JOIN ENTITY fiscal
                  ON cli.fiscalyear_startmonth_type_id = fiscal.entity_id
            INNER JOIN division div
                  ON div.client_id = cli.client_id
                     AND div.is_corporate_division = 1
      WHERE
            cli.client_id = @clientId
END

GO
GRANT EXECUTE ON  [dbo].[GET_ONBOARD_CLIENT_HEADER_P] TO [CBMSApplication]
GO
