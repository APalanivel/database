SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Sr_Rfp_History_Del_For_Account]  
     
DESCRIPTION: 

	To delete Sr Rfp History associated with the given Account Id.
	
	Here is the order of deletion , this order should not be changed
	
	1 --- SR_RFP_SOP_DETAILS --- Child of SR_RFP_ACCOUNT_TERM
	2 --- SR_RFP_SOP_SHORTLIST_DETAILS --- Child of SR_RFP_ACCOUNT_TERM
	3 --- SR_RFP_TERM_PRODUCT_MAP --- Child of SR_RFP_ACCOUNT_TERM
	4 --- SR_RFP_ACCOUNT_TERM --- 
	5 --- SR_RFP_BID --- Parent of SR_RFP_ACCOUNT_TERM
	6 --- SR_RFP_SUPPLIER_PRICE_COMMENTS_ARCHIVE --- Child of SR_RFP_SUPPLIER_PRICE_COMMENTS
	7 --- SR_RFP_SUPPLIER_PRICE_COMMENTS --- Parent of Sr_Rfp_Bid
	8 --- SR_RFP_SUPPLIER_SERVICE --- Parent Of Sr_Rfp_Bid
	9 --- SR_RFP_DELIVERY_FUEL --- Parent of Sr_Rfp_Supplier_Service
	10 --- SR_RFP_BALANCING_TOLERANCE --- Parent of Sr_Rfp_Supplier_Service
	11 --- SR_RFP_RISK_MANAGEMENT --- Parent of Sr_Rfp_Supplier_Service
	12 --- SR_RFP_ALTERNATE_FUEL --- Parent of Sr_Rfp_Supplier_Service
	13 --- SR_RFP_PRICING_SCOPE --- Parent of Sr_Rfp_Supplier_Service
	14 --- SR_RFP_MISCELLANEOUS_POWER --- Parent of Sr_Rfp_Supplier_Service
	15 --- SR_RFP_SEND_SUPPLIER_LOG  --- 
	16 --- SR_RFP_SEND_SUPPLIER --- 
	17 --- SR_RFP_ARCHIVE_ACCOUNT --- Child of Sr_Rfp_Account and no child for this.
	18 --- SR_RFP_ARCHIVE_CHECKLIST --- Child of Sr_Rfp_Account and no child for this.
	19 --- SR_RFP_CHECKLIST --- Child of Sr_Rfp_Account and no child for this.
	20 --- SR_RFP_LOAD_PROFILE_SETUP --- Child of Sr_Rfp_Account and no child for this.
	21 --- SR_RFP_LP_ACCOUNT_ADDITIONAL_ROW --- Child of Sr_Rfp_Account and no child for this.
	22 --- SR_RFP_LP_COMMENT --- Child of Sr_Rfp_Account and no child for this.
	23 --- SR_RFP_LP_INTERVAL_DATA --- Child of Sr_Rfp_Account and no child for this.
	24 --- SR_BATCH_LOG_DETAILS --- 
	25 --- SR_CANCEL_RENEGOTIATION_DOCUMENT --- 
	26 --- SR_COMMENTS_ACCOUNTS_MAP --- 
	27 --- SR_DEAL_TICKET_ACCOUNT_MAP --- 
	28 --- SR_MASTER_CHECKLIST_REVIEW_STATUS --- 
	29 --- SR_RC_CONTRACT_DOCUMENT_ACCOUNTS_MAP --- 
	30 --- SR_RFP_ACCOUNT --- 
	31 --- SR_RFP_BID_GROUP --- 
 
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Account_Id		INT						

OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        

	BEGIN TRAN

		EXEC Sr_Rfp_History_Del_For_Account  19094

	Rollback Tran

	BEGIN TRAN

		EXEC Sr_Rfp_History_Del_For_Account  20556

	Rollback Tran

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			06/08/2010	Created

*/

CREATE PROCEDURE dbo.Sr_Rfp_History_Del_For_Account
	 @Account_Id	INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE	@Invoice_Participation_Queue_Id				INT
		   ,@Invoice_Participation_Site_Id				INT
		   ,@Site_Id									INT
		   ,@Service_Month								DATETIME
		   ,@Sr_Rfp_Account_Id							INT
		   ,@Sr_Rfp_Archive_Account_Id					INT
		   ,@Sr_Rfp_Archive_Checklist_Id				INT
		   ,@Sr_Rfp_Checklist_Id						INT
		   ,@Sr_Rfp_Load_Profile_Setup_Id				INT
		   ,@Sr_Rfp_Lp_Account_Additional_Row_Id		INT
		   ,@Sr_Rfp_Lp_Comment_Id						INT
		   ,@Sr_Rfp_Lp_Interval_Data_Id					INT
		   ,@Sr_Rfp_Send_Supplier_Id					INT
		   ,@Sr_Rfp_Send_Supplier_Log_Id				INT
		   ,@Sr_Rfp_Bid_Group_Id						INT
		   ,@Sr_Rfp_Bid_Group_Name						VARCHAR(200)
		   ,@Sr_Rfp_Account_Term_Id						INT
		   ,@Sr_Rfp_Sop_Details_Id						INT
		   ,@Sr_Rfp_Sop_Shortlist_Details_Id			INT
		   ,@Sr_Rfp_Term_Product_Map_Id					INT
		   ,@Sr_Rfp_Bid_Id								INT
		   ,@Sr_Rfp_Supplier_Price_Comments_Id			INT
		   ,@Sr_Rfp_Supplier_Price_Comments_Archive_Id	INT
		   ,@Sr_Rfp_Supplier_Service_Id					INT
		   ,@Sr_Rfp_Delivery_Fuel_Id					INT
		   ,@Sr_Rfp_Balancing_Tolerance_Id				INT
		   ,@Sr_Rfp_Risk_Management_Id					INT
		   ,@Sr_Rfp_Alternate_Fuel_Id					INT
		   ,@Sr_Rfp_Pricing_Scope_Id					INT
		   ,@Sr_Rfp_Miscellaneous_Power_Id				INT
		   ,@Sr_Batch_Log_Details_Id					INT
		   ,@Sr_Cancel_Renegotiation_Document_Id		INT
		   ,@Sr_Comments_Accounts_Map_Id				INT
		   ,@Sr_Deal_Ticket_Account_Map_Id				INT
		   ,@Sr_Master_Checklist_Review_Status_Id		INT
		   ,@Sr_Rc_Contract_Document_Accounts_Map_Id	INT
	
	DECLARE @Sr_Rfp_Account_List TABLE(Sr_Rfp_Account_Id INT PRIMARY KEY CLUSTERED, Sr_Rfp_Id INT,Sr_Rfp_Bid_Group_Id INT)
	DECLARE @Sr_Rfp_Archive_Account_List TABLE(Sr_Rfp_Archive_Account_Id INT PRIMARY KEY CLUSTERED)
	DECLARE @Sr_Rfp_Archive_Checklist_List TABLE(Sr_Rfp_Archive_Checklist_Id INT PRIMARY KEY CLUSTERED)
	DECLARE @Sr_Rfp_Checklist_List TABLE(Sr_Rfp_Checklist_Id INT PRIMARY KEY CLUSTERED)
	DECLARE @Sr_Rfp_Load_Profile_Setup_List TABLE(Sr_Rfp_Load_Profile_Setup_Id INT PRIMARY KEY CLUSTERED)
	DECLARE @Sr_Rfp_Lp_Account_Additional_Row_List TABLE(Sr_Rfp_Lp_Account_Additional_Row_Id INT PRIMARY KEY CLUSTERED)
	DECLARE @Sr_Rfp_Lp_Comment_List TABLE(Sr_Rfp_Lp_Comment_Id INT PRIMARY KEY CLUSTERED)
	DECLARE @Sr_Rfp_Lp_Interval_Data_List TABLE(Sr_Rfp_Lp_Interval_Data_Id INT PRIMARY KEY CLUSTERED)

	DECLARE @Other_Account_Bidgroup_List TABLE (Sr_Rfp_Bid_Group_Id INT PRIMARY KEY CLUSTERED)
	DECLARE @Orphan_Bidgroup_List TABLE (Sr_Rfp_Bid_Group_Id INT PRIMARY KEY CLUSTERED)
	DECLARE @Sr_Rfp_Send_Supplier_List TABLE(Sr_Rfp_Send_Supplier_Id INT PRIMARY KEY CLUSTERED)
	DECLARE @Sr_Rfp_Send_Supplier_Log_List TABLE(Sr_Rfp_Send_Supplier_Log_Id INT PRIMARY KEY CLUSTERED)
	DECLARE @Sr_Rfp_Bid_Group_List TABLE(Sr_Rfp_Bid_Group_Id INT PRIMARY KEY CLUSTERED, Group_Name VARCHAR(200));
	DECLARE @Sr_Rfp_Account_Term_List TABLE(Sr_Rfp_Account_Term_Id INT PRIMARY KEY CLUSTERED);
	DECLARE @Sr_Rfp_Sop_Details_List TABLE(Sr_Rfp_Sop_Details_Id INT PRIMARY KEY CLUSTERED);
	DECLARE @Sr_Rfp_Sop_Shortlist_Details_List TABLE(Sr_Rfp_Sop_Shortlist_Details_Id INT PRIMARY KEY CLUSTERED);
	DECLARE @Sr_Rfp_Term_Product_Map_List TABLE(Sr_Rfp_Term_Product_Map_Id INT PRIMARY KEY CLUSTERED, Sr_Rfp_Bid_Id INT);

	DECLARE @Sr_Rfp_Bid_List TABLE(Sr_Rfp_Bid_Id INT PRIMARY KEY CLUSTERED, Sr_Rfp_Supplier_Price_Comments_Id INT, Sr_Rfp_Supplier_Service_Id INT);
	DECLARE @Sr_Rfp_Supplier_Price_Comments_List TABLE (Sr_Rfp_Supplier_Price_Comments_Id INT PRIMARY KEY CLUSTERED);
	DECLARE @Sr_Rfp_Supplier_Price_Comments_Archive_List TABLE(Sr_Rfp_Supplier_Price_Comments_Archive_Id INT PRIMARY KEY CLUSTERED);

	DECLARE @Sr_Rfp_Supplier_Service_List TABLE
	(
		Sr_Rfp_Supplier_Service_Id		INT PRIMARY KEY CLUSTERED
		,Sr_Rfp_Delivery_Fuel_Id		INT
		,Sr_Rfp_Balancing_Tolerance_Id	INT
		,Sr_Rfp_Risk_Management_Id		INT
		,Sr_Rfp_Alternate_Fuel_Id		INT
		,Sr_Rfp_Pricing_Scope_Id		INT
		,Sr_Rfp_Miscellaneous_Power_Id	INT
	);
	DECLARE @Sr_Rfp_Delivery_Fuel_List TABLE(Sr_Rfp_Delivery_Fuel_Id INT PRIMARY KEY CLUSTERED);
	DECLARE @Sr_Rfp_Balancing_Tolerance_List TABLE(Sr_Rfp_Balancing_Tolerance_Id INT PRIMARY KEY CLUSTERED);
	DECLARE @Sr_Rfp_Risk_Management_List TABLE(Sr_Rfp_Risk_Management_Id INT PRIMARY KEY CLUSTERED);
	DECLARE @Sr_Rfp_Alternate_Fuel_List TABLE( Sr_Rfp_Alternate_Fuel_Id INT PRIMARY KEY CLUSTERED);
	DECLARE @Sr_Rfp_Pricing_Scope_List TABLE(Sr_Rfp_Pricing_Scope_Id INT PRIMARY KEY CLUSTERED);
	DECLARE @Sr_Rfp_Miscellaneous_Power_List TABLE(Sr_Rfp_Miscellaneous_Power_Id INT PRIMARY KEY CLUSTERED);

	DECLARE @Sr_Batch_Log_Details_List TABLE(Sr_Batch_Log_Details_Id INT PRIMARY KEY CLUSTERED);
	DECLARE @Sr_Cancel_Renegotiation_Document_List TABLE(Sr_Cancel_Renegotiation_Document_Id INT PRIMARY KEY CLUSTERED);
	DECLARE @Sr_Comments_Accounts_Map_List TABLE(Sr_Comments_Accounts_Map_Id INT PRIMARY KEY CLUSTERED);
	DECLARE @Sr_Deal_Ticket_Account_Map_List TABLE(Sr_Deal_Ticket_Account_Map_Id INT PRIMARY KEY CLUSTERED);
	DECLARE @Sr_Master_Checklist_Review_Status_List TABLE(Sr_Master_Checklist_Review_Status_Id INT PRIMARY KEY CLUSTERED);
	DECLARE @Sr_Rc_Contract_Document_Accounts_Map_List TABLE(Sr_Rc_Contract_Document_Accounts_Map_Id INT PRIMARY KEY CLUSTERED);


	INSERT INTO @Sr_Rfp_Account_List
	(
		Sr_Rfp_Account_Id
		,Sr_Rfp_Id
		,Sr_Rfp_Bid_Group_Id
	)
	SELECT
		Sr_Rfp_Account_Id
		,Sr_Rfp_Id
		,Sr_Rfp_Bid_Group_Id
	FROM
		dbo.SR_RFP_ACCOUNT
	WHERE
		ACCOUNT_ID = @Account_id
		AND Is_Deleted = 1
		AND NOT EXISTS
					(
						SELECT
							1
						FROM
							dbo.SR_RFP_ACCOUNT
						WHERE
							ACCOUNT_ID = @Account_id
							AND IS_DELETED = 0
					)

	IF EXISTS(SELECT 1 FROM @Sr_Rfp_Account_List)
	BEGIN
	
		INSERT INTO @Other_Account_Bidgroup_List(Sr_Rfp_Bid_Group_Id)
		SELECT
			RfpAcc.SR_RFP_BID_GROUP_ID
		FROM
			@Sr_Rfp_Account_List AccLst
			JOIN dbo.SR_RFP_ACCOUNT RfpAcc
				ON AccLst.SR_RFP_BID_GROUP_ID = RfpAcc.SR_RFP_BID_GROUP_ID
					AND AccLst.Sr_Rfp_Id = RfpAcc.Sr_Rfp_Id
		WHERE
			RfpAcc.Account_Id != @Account_Id
		GROUP BY
			RfpAcc.SR_RFP_BID_GROUP_ID

		INSERT INTO @Orphan_Bidgroup_List
		SELECT
			al.Sr_Rfp_Bid_Group_Id
		FROM
			@Sr_Rfp_Account_List al
			LEFT JOIN @Other_Account_Bidgroup_List Othbgl
				ON Othbgl.Sr_Rfp_Bid_Group_Id = al.Sr_Rfp_Bid_Group_Id
		WHERE
			al.Sr_Rfp_Bid_Group_Id IS NOT NULL
			AND Othbgl.Sr_Rfp_Bid_Group_Id IS NULL

		IF EXISTS(SELECT 1 FROM @Orphan_Bidgroup_List)-- Orphan bid group details should be removed from the child tables and SR_RFP_BIDGROUP
		BEGIN

			INSERT INTO @Sr_Rfp_Send_Supplier_List(Sr_Rfp_Send_Supplier_Id)
			SELECT
				RfpSupp.SR_RFP_SEND_SUPPLIER_ID
			FROM
				dbo.SR_RFP_SEND_SUPPLIER RfpSupp
				JOIN @Orphan_Bidgroup_List Obgl
					ON Obgl.Sr_Rfp_Bid_Group_Id = RfpSupp.SR_ACCOUNT_GROUP_ID
			WHERE
				RfpSupp.IS_BID_GROUP = 1

			INSERT INTO @Sr_Rfp_Bid_Group_List
			(
				Sr_Rfp_Bid_Group_Id
				,Group_Name
			)
			SELECT
				bg.SR_RFP_BID_GROUP_ID
				,bg.GROUP_NAME
			FROM
				dbo.SR_RFP_BID_GROUP bg
				JOIN @Orphan_Bidgroup_List Obgl
					ON Obgl.Sr_Rfp_Bid_Group_Id = bg.SR_RFP_BID_GROUP_ID

			INSERT INTO @Sr_Rfp_Account_Term_List(Sr_Rfp_Account_Term_Id)
			SELECT
				AccTrm.SR_RFP_ACCOUNT_TERM_ID
			FROM
				dbo.SR_RFP_ACCOUNT_TERM AccTrm
				JOIN @Orphan_Bidgroup_List Obgl
					ON Obgl.Sr_Rfp_Bid_Group_Id = AccTrm.SR_ACCOUNT_GROUP_ID
			WHERE
				AccTrm.IS_BID_GROUP = 1

		END


		INSERT INTO @Sr_Rfp_Send_Supplier_List(Sr_Rfp_Send_Supplier_Id)
		SELECT
			RfpSupp.SR_RFP_SEND_SUPPLIER_ID
		FROM
			dbo.SR_RFP_SEND_SUPPLIER RfpSupp
			JOIN @Sr_Rfp_Account_List AccLst
				ON AccLst.Sr_Rfp_Account_Id = RfpSupp.SR_ACCOUNT_GROUP_ID
		WHERE
			RfpSupp.IS_BID_GROUP = 0
			AND AccLst.Sr_Rfp_Bid_Group_Id IS NULL

		INSERT INTO @Sr_Rfp_Send_Supplier_Log_List(Sr_Rfp_Send_Supplier_Log_Id)
		SELECT
			SuppLog.SR_RFP_SEND_SUPPLIER_LOG_ID
		FROM
			dbo.SR_RFP_SEND_SUPPLIER_LOG SuppLog
			JOIN @Sr_Rfp_Send_Supplier_List SuppLst
				ON SuppLst.Sr_Rfp_Send_Supplier_Id = SuppLog.SR_RFP_SEND_SUPPLIER_ID	
	
		INSERT INTO @Sr_Rfp_Account_Term_List(Sr_Rfp_Account_Term_Id)
		SELECT
			AccTrm.SR_RFP_ACCOUNT_TERM_ID
		FROM
			dbo.SR_RFP_ACCOUNT_TERM AccTrm
			JOIN @Sr_Rfp_Account_List AccLst
				ON AccLst.Sr_Rfp_Account_Id = AccTrm.SR_ACCOUNT_GROUP_ID
		WHERE
			AccTrm.IS_BID_GROUP = 0
			AND AccLst.Sr_Rfp_Bid_Group_Id IS NULL

		INSERT INTO @Sr_Rfp_Sop_Details_List(Sr_Rfp_Sop_Details_Id)
		SELECT
			SopDtl.SR_RFP_SOP_DETAILS_ID
		FROM
			dbo.SR_RFP_SOP_DETAILS SopDtl
			JOIN @Sr_Rfp_Account_Term_List TermLst
				ON TermLst.Sr_Rfp_Account_Term_Id = SopDtl.SR_RFP_ACCOUNT_TERM_ID

		INSERT INTO @Sr_Rfp_Sop_Shortlist_Details_List(Sr_Rfp_Sop_Shortlist_Details_Id)
		SELECT
			Dtl.SR_RFP_SOP_SHORTLIST_DETAILS_ID
		FROM
			dbo.SR_RFP_SOP_SHORTLIST_DETAILS Dtl
			JOIN @Sr_Rfp_Account_Term_List TermLst
				ON TermLst.Sr_Rfp_Account_Term_Id = Dtl.SR_RFP_ACCOUNT_TERM_ID

		INSERT INTO @Sr_Rfp_Term_Product_Map_List
		(
			Sr_Rfp_Term_Product_Map_Id
			,Sr_Rfp_Bid_Id
		)
		SELECT
			ProdMap.SR_RFP_TERM_PRODUCT_MAP_ID
			,ProdMap.Sr_Rfp_Bid_Id
		FROM
			dbo.SR_RFP_TERM_PRODUCT_MAP ProdMap
			JOIN @Sr_Rfp_Account_Term_List TermLst
				ON TermLst.Sr_Rfp_Account_Term_Id = ProdMap.SR_RFP_ACCOUNT_TERM_ID

		INSERT INTO @Sr_Rfp_Bid_List
		(
			Sr_Rfp_Bid_Id
			,Sr_Rfp_Supplier_Price_Comments_Id
			,Sr_Rfp_Supplier_Service_Id
		)
		SELECT
			bid.Sr_Rfp_Bid_Id
			,bid.Sr_Rfp_Supplier_Price_Comments_Id
			,bid.Sr_Rfp_Supplier_Service_Id
		FROM
			dbo.SR_RFP_BID bid
			JOIN @Sr_Rfp_Term_Product_Map_List ProdMapLst
				ON ProdMapLst.Sr_Rfp_Bid_Id = bid.SR_RFP_BID_ID

		INSERT INTO @Sr_Rfp_Supplier_Price_Comments_List(Sr_Rfp_Supplier_Price_Comments_Id)
		SELECT
			PrcCmts.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
		FROM
			dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS PrcCmts
			JOIN @Sr_Rfp_Bid_List BidLst
				ON BidLst.Sr_Rfp_Supplier_Price_Comments_Id = PrcCmts.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID

		INSERT INTO @Sr_Rfp_Supplier_Price_Comments_Archive_List(Sr_Rfp_Supplier_Price_Comments_Archive_Id)
		SELECT
			PrcCmntArc.Sr_Rfp_Supplier_Price_Comments_Archive_Id
		FROM
			dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS_ARCHIVE PrcCmntArc
			JOIN @Sr_Rfp_Supplier_Price_Comments_List PrcCmntsLst
				ON PrcCmntsLst.Sr_Rfp_Supplier_Price_Comments_Id = PrcCmntArc.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID

		INSERT INTO @Sr_Rfp_Supplier_Service_List
		(
			Sr_Rfp_Supplier_Service_Id
			,Sr_Rfp_Delivery_Fuel_Id
			,Sr_Rfp_Balancing_Tolerance_Id
			,Sr_Rfp_Risk_Management_Id
			,Sr_Rfp_Alternate_Fuel_Id
			,Sr_Rfp_Pricing_Scope_Id
			,Sr_Rfp_Miscellaneous_Power_Id
		)
		SELECT
			SuppSrvc.SR_RFP_SUPPLIER_SERVICE_ID
			,SuppSrvc.Sr_Rfp_Delivery_Fuel_Id
			,SuppSrvc.Sr_Rfp_Balancing_Tolerance_Id
			,SuppSrvc.Sr_Rfp_Risk_Management_Id
			,SuppSrvc.Sr_Rfp_Alternate_Fuel_Id
			,SuppSrvc.Sr_Rfp_Pricing_Scope_Id
			,SuppSrvc.Sr_Rfp_Miscellaneous_Power_Id
		FROM
			dbo.SR_RFP_SUPPLIER_SERVICE SuppSrvc
			JOIN @Sr_Rfp_Bid_List BidLst
				ON BidLst.Sr_Rfp_Supplier_Service_Id = SuppSrvc.SR_RFP_SUPPLIER_SERVICE_ID

		INSERT INTO @Sr_Rfp_Delivery_Fuel_List(Sr_Rfp_Delivery_Fuel_Id)
		SELECT
			DlvryFuel.SR_RFP_DELIVERY_FUEL_ID
		FROM
			dbo.SR_RFP_DELIVERY_FUEL DlvryFuel
			JOIN @Sr_Rfp_Supplier_Service_List SrvcLst
				ON SrvcLst.Sr_Rfp_Delivery_Fuel_Id = DlvryFuel.SR_RFP_DELIVERY_FUEL_ID

		INSERT INTO @Sr_Rfp_Balancing_Tolerance_List(Sr_Rfp_Balancing_Tolerance_Id)
		SELECT
			Tol.SR_RFP_BALANCING_TOLERANCE_ID
		FROM
			dbo.SR_RFP_BALANCING_TOLERANCE Tol
			JOIN @Sr_Rfp_Supplier_Service_List SrvcLst
				ON SrvcLst.Sr_Rfp_Balancing_Tolerance_Id = Tol.SR_RFP_BALANCING_TOLERANCE_ID

		INSERT INTO @Sr_Rfp_Risk_Management_List(Sr_Rfp_Risk_Management_Id)
		SELECT
			RfpRskMgmt.SR_RFP_RISK_MANAGEMENT_ID
		FROM
			dbo.SR_RFP_RISK_MANAGEMENT RfpRskMgmt
			JOIN @Sr_Rfp_Supplier_Service_List SrvcLst
				ON SrvcLst.Sr_Rfp_Risk_Management_Id = RfpRskMgmt.SR_RFP_RISK_MANAGEMENT_ID

		INSERT INTO @Sr_Rfp_Alternate_Fuel_List(Sr_Rfp_Alternate_Fuel_Id)
		SELECT
			RfpAltFuel.SR_RFP_ALTERNATE_FUEL_ID
		FROM
			dbo.SR_RFP_ALTERNATE_FUEL RfpAltFuel
			JOIN @Sr_Rfp_Supplier_Service_List SrvcLst
				ON SrvcLst.Sr_Rfp_Supplier_Service_Id = RfpAltFuel.SR_RFP_ALTERNATE_FUEL_ID

		INSERT INTO @Sr_Rfp_Pricing_Scope_List(Sr_Rfp_Pricing_Scope_Id)
		SELECT
			RfpPrcScope.SR_RFP_PRICING_SCOPE_ID
		FROM
			dbo.SR_RFP_PRICING_SCOPE RfpPrcScope
			JOIN @Sr_Rfp_Supplier_Service_List SrvcLst
				ON SrvcLst.Sr_Rfp_Pricing_Scope_Id = RfpPrcScope.SR_RFP_PRICING_SCOPE_ID

		INSERT INTO @Sr_Rfp_Miscellaneous_Power_List(Sr_Rfp_Miscellaneous_Power_Id)
		SELECT
			MiscPower.SR_RFP_MISCELLANEOUS_POWER_ID
		FROM
			dbo.SR_RFP_MISCELLANEOUS_POWER MiscPower
			JOIN @Sr_Rfp_Supplier_Service_List SrvcLst
				ON SrvcLst.Sr_Rfp_Miscellaneous_Power_Id = MiscPower.SR_RFP_MISCELLANEOUS_POWER_ID


		INSERT INTO @Sr_Rfp_Archive_Account_List(Sr_Rfp_Archive_Account_Id)
		SELECT
			ArcAcc.SR_RFP_ARCHIVE_ACCOUNT_ID
		FROM
			dbo.SR_RFP_ARCHIVE_ACCOUNT ArcAcc
			JOIN @Sr_Rfp_Account_List al
				ON al.Sr_Rfp_Account_Id = ArcAcc.SR_RFP_ACCOUNT_ID

		INSERT INTO @Sr_Rfp_Archive_Checklist_List(Sr_Rfp_Archive_Checklist_Id)
		SELECT
			ArcChkLst.SR_RFP_ARCHIVE_CHECKLIST_ID
		FROM
			dbo.SR_RFP_ARCHIVE_CHECKLIST ArcChkLst
			JOIN @Sr_Rfp_Account_List al
				ON al.Sr_Rfp_Account_Id = ArcChkLst.SR_RFP_ACCOUNT_ID

		INSERT INTO @Sr_Rfp_Checklist_List(Sr_Rfp_Checklist_Id)
		SELECT
			ChkLst.SR_RFP_CHECKLIST_ID
		FROM
			dbo.SR_RFP_CHECKLIST ChkLst
			JOIN @Sr_Rfp_Account_List al
				ON al.Sr_Rfp_Account_Id = ChkLst.SR_RFP_ACCOUNT_ID

		INSERT INTO @Sr_Rfp_Load_Profile_Setup_List(Sr_Rfp_Load_Profile_Setup_Id)
		SELECT
			RfpLps.SR_RFP_LOAD_PROFILE_SETUP_ID
		FROM
			dbo.SR_RFP_LOAD_PROFILE_SETUP RfpLps
			JOIN @Sr_Rfp_Account_List al
				ON al.Sr_Rfp_Account_Id = RfpLps.SR_RFP_ACCOUNT_ID

		INSERT INTO @Sr_Rfp_Lp_Account_Additional_Row_List(Sr_Rfp_Lp_Account_Additional_Row_Id)
		SELECT
			AccAdtnlRow.SR_RFP_LP_ACCOUNT_ADDITIONAL_ROW_ID
		FROM
			dbo.SR_RFP_LP_ACCOUNT_ADDITIONAL_ROW AccAdtnlRow
			JOIN @Sr_Rfp_Account_List al
				ON al.Sr_Rfp_Account_Id = AccAdtnlRow.SR_RFP_ACCOUNT_ID

		INSERT INTO @Sr_Rfp_Lp_Comment_List(Sr_Rfp_Lp_Comment_Id)
		SELECT
			LpCmnt.SR_RFP_LP_COMMENT_ID
		FROM
			dbo.SR_RFP_LP_COMMENT LpCmnt
			JOIN @Sr_Rfp_Account_List al
				ON al.Sr_Rfp_Account_Id = LpCmnt.SR_RFP_ACCOUNT_ID

		INSERT INTO @Sr_Rfp_Lp_Interval_Data_List(Sr_Rfp_Lp_Interval_Data_Id)
		SELECT
			LpIntData.SR_RFP_LP_INTERVAL_DATA_ID
		FROM
			dbo.SR_RFP_LP_INTERVAL_DATA LpIntData
			JOIN @Sr_Rfp_Account_List al
				ON al.Sr_Rfp_Account_Id = LpIntData.SR_RFP_ACCOUNT_ID



		INSERT INTO @Sr_Batch_Log_Details_List(Sr_Batch_Log_Details_Id)
		SELECT
			SR_BATCH_LOG_DETAILS_ID
		FROM
			dbo.SR_BATCH_LOG_DETAILS
		WHERE
			ACCOUNT_ID = @Account_id

		INSERT INTO @Sr_Cancel_Renegotiation_Document_List(Sr_Cancel_Renegotiation_Document_Id)
		SELECT
			SR_CANCEL_RENEGOTIATION_DOCUMENT_ID
		FROM
			dbo.SR_CANCEL_RENEGOTIATION_DOCUMENT
		WHERE
			ACCOUNT_ID = @Account_id
			
		INSERT INTO @Sr_Comments_Accounts_Map_List(Sr_Comments_Accounts_Map_Id)
		SELECT
			Sr_Comments_Accounts_Map_Id
		FROM
			dbo.Sr_Comments_Accounts_Map
		WHERE
			ACCOUNT_ID = @Account_id
			
		INSERT INTO @Sr_Deal_Ticket_Account_Map_List(Sr_Deal_Ticket_Account_Map_Id)
		SELECT
			Sr_Deal_Ticket_Account_Map_Id
		FROM
			dbo.SR_DEAL_TICKET_ACCOUNT_MAP
		WHERE
			ACCOUNT_ID = @Account_id

		INSERT INTO @Sr_Master_Checklist_Review_Status_List(Sr_Master_Checklist_Review_Status_Id)
		SELECT
			Sr_Master_Checklist_Review_Status_Id
		FROM
			dbo.SR_MASTER_CHECKLIST_REVIEW_STATUS
		WHERE
			ACCOUNT_ID = @Account_id

		INSERT INTO @Sr_Rc_Contract_Document_Accounts_Map_List(Sr_Rc_Contract_Document_Accounts_Map_Id)
		SELECT
			SR_RC_CONTRACT_DOCUMENT_ACCOUNTS_MAP_ID
		FROM
			SR_RC_CONTRACT_DOCUMENT_ACCOUNTS_MAP
		WHERE
			ACCOUNT_ID = @Account_id

	 	 
		BEGIN TRY
			BEGIN TRAN
			
				
				WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Sop_Details_List)
				BEGIN

					SET @Sr_Rfp_Sop_Details_Id = (SELECT TOP 1 Sr_Rfp_Sop_Details_Id FROM @Sr_Rfp_Sop_Details_List)

					EXEC dbo.SR_RFP_Sop_Details_Del @Sr_Rfp_Sop_Details_Id

					DELETE
						@Sr_Rfp_Sop_Details_List
					WHERE
						Sr_Rfp_Sop_Details_Id = @Sr_Rfp_Sop_Details_Id

				END



				WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Sop_Shortlist_Details_List)
				BEGIN

					SET @Sr_Rfp_Sop_Shortlist_Details_Id = (SELECT TOP 1 Sr_Rfp_Sop_Shortlist_Details_Id FROM @Sr_Rfp_Sop_Shortlist_Details_List)

					EXEC dbo.SR_RFP_Sop_Shortlist_Details_Del @Sr_Rfp_Sop_Shortlist_Details_Id

					DELETE
						@Sr_Rfp_Sop_Shortlist_Details_List
					WHERE
						Sr_Rfp_Sop_Shortlist_Details_Id = @Sr_Rfp_Sop_Shortlist_Details_Id

				END



				WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Term_Product_Map_List)
				BEGIN

					SET @Sr_Rfp_Term_Product_Map_Id=(SELECT TOP 1 Sr_Rfp_Term_Product_Map_Id FROM @Sr_Rfp_Term_Product_Map_List)

					EXEC dbo.SR_RFP_Term_Product_Map_Del @Sr_Rfp_Term_Product_Map_Id

					DELETE
						@Sr_Rfp_Term_Product_Map_List
					WHERE
						Sr_Rfp_Term_Product_Map_Id = @Sr_Rfp_Term_Product_Map_Id

				END



				WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Account_Term_List)
				BEGIN

					SET @Sr_Rfp_Account_Term_Id = (SELECT TOP 1 Sr_Rfp_Account_Term_Id FROM @Sr_Rfp_Account_Term_List)

					EXEC dbo.SR_RFP_Account_Term_Del @Sr_Rfp_Account_Term_Id

					DELETE
						@Sr_Rfp_Account_Term_List
					WHERE
						Sr_Rfp_Account_Term_Id = @Sr_Rfp_Account_Term_Id

				END


				WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Bid_List)
				BEGIN

					SET @Sr_Rfp_Bid_Id =(SELECT TOP 1 Sr_Rfp_Bid_Id FROM @Sr_Rfp_Bid_List)

					EXEC dbo.SR_RFP_Bid_Del @Sr_Rfp_Bid_Id

					DELETE
						@Sr_Rfp_Bid_List
					WHERE
						Sr_Rfp_Bid_Id = @Sr_Rfp_Bid_Id

				END


				WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Supplier_Price_Comments_Archive_List)
				BEGIN

					SET @Sr_Rfp_Supplier_Price_Comments_Archive_Id =(SELECT TOP 1 Sr_Rfp_Supplier_Price_Comments_Archive_Id FROM @Sr_Rfp_Supplier_Price_Comments_Archive_List)

					EXEC dbo.SR_RFP_Supplier_Price_Comments_Archive_Del @Sr_Rfp_Supplier_Price_Comments_Archive_Id

					DELETE
						@Sr_Rfp_Supplier_Price_Comments_Archive_List
					WHERE
						Sr_Rfp_Supplier_Price_Comments_Archive_Id = @Sr_Rfp_Supplier_Price_Comments_Archive_Id

				END


				WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Supplier_Price_Comments_List)
				BEGIN

					SET @Sr_Rfp_Supplier_Price_Comments_Id =(SELECT TOP 1 Sr_Rfp_Supplier_Price_Comments_Id FROM @Sr_Rfp_Supplier_Price_Comments_List)

					EXEC dbo.SR_RFP_Supplier_Price_Comments_Del @Sr_Rfp_Supplier_Price_Comments_Id

					DELETE
						@Sr_Rfp_Supplier_Price_Comments_List
					WHERE
						Sr_Rfp_Supplier_Price_Comments_Id = @Sr_Rfp_Supplier_Price_Comments_Id

				END


				WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Supplier_Service_List)
				BEGIN

					SET @Sr_Rfp_Supplier_Service_Id =(SELECT TOP 1 Sr_Rfp_Supplier_Service_Id FROM @Sr_Rfp_Supplier_Service_List)

					EXEC dbo.SR_RFP_Supplier_Service_Del @Sr_Rfp_Supplier_Service_Id

					DELETE
						@Sr_Rfp_Supplier_Service_List
					WHERE
						Sr_Rfp_Supplier_Service_Id = @Sr_Rfp_Supplier_Service_Id

				END


				WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Delivery_Fuel_List)
				BEGIN

					SET @Sr_Rfp_Delivery_Fuel_Id =(SELECT TOP 1 Sr_Rfp_Delivery_Fuel_Id FROM @Sr_Rfp_Delivery_Fuel_List)

					EXEC dbo.SR_RFP_Delivery_Fuel_Del @Sr_Rfp_Delivery_Fuel_Id

					DELETE
						@Sr_Rfp_Delivery_Fuel_List
					WHERE
						Sr_Rfp_Delivery_Fuel_Id = @Sr_Rfp_Delivery_Fuel_Id

				END


				WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Balancing_Tolerance_List)
				BEGIN

					SET @Sr_Rfp_Balancing_Tolerance_Id =(SELECT TOP 1 Sr_Rfp_Balancing_Tolerance_Id FROM @Sr_Rfp_Balancing_Tolerance_List)

					EXEC dbo.SR_RFP_Balancing_Tolerance_Del @Sr_Rfp_Balancing_Tolerance_Id

					DELETE
						@Sr_Rfp_Balancing_Tolerance_List
					WHERE
						 Sr_Rfp_Balancing_Tolerance_Id = @Sr_Rfp_Balancing_Tolerance_Id

				END


				WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Risk_Management_List)
				BEGIN

					SET @Sr_Rfp_Risk_Management_Id =(SELECT TOP 1 Sr_Rfp_Risk_Management_Id FROM @Sr_Rfp_Risk_Management_List)

					EXEC dbo.SR_RFP_Risk_Management_Del @Sr_Rfp_Risk_Management_Id

					DELETE
						@Sr_Rfp_Risk_Management_List
					WHERE
						 Sr_Rfp_Risk_Management_Id = @Sr_Rfp_Risk_Management_Id

				END


				WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Alternate_Fuel_List)
				BEGIN

					SET @Sr_Rfp_Alternate_Fuel_Id =(SELECT TOP 1 Sr_Rfp_Alternate_Fuel_Id FROM @Sr_Rfp_Alternate_Fuel_List)

					EXEC dbo.SR_RFP_Alternate_Fuel_Del @Sr_Rfp_Alternate_Fuel_Id

					DELETE
						@Sr_Rfp_Alternate_Fuel_List
					WHERE
						 Sr_Rfp_Alternate_Fuel_Id = @Sr_Rfp_Alternate_Fuel_Id

				END


				WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Pricing_Scope_List)
				BEGIN

					SET @Sr_Rfp_Pricing_Scope_Id =(SELECT TOP 1 Sr_Rfp_Pricing_Scope_Id FROM @Sr_Rfp_Pricing_Scope_List)

					EXEC dbo.SR_RFP_Pricing_Scope_Del @Sr_Rfp_Pricing_Scope_Id

					DELETE
						@Sr_Rfp_Pricing_Scope_List
					WHERE
						 Sr_Rfp_Pricing_Scope_Id = @Sr_Rfp_Pricing_Scope_Id

				END


				WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Miscellaneous_Power_List)
				BEGIN

					SET @Sr_Rfp_Miscellaneous_Power_Id =(SELECT TOP 1 Sr_Rfp_Miscellaneous_Power_Id FROM @Sr_Rfp_Miscellaneous_Power_List)

					EXEC dbo.SR_RFP_Miscellaneous_Power_Del @Sr_Rfp_Miscellaneous_Power_Id

					DELETE
						@Sr_Rfp_Miscellaneous_Power_List
					WHERE
						 Sr_Rfp_Miscellaneous_Power_Id = @Sr_Rfp_Miscellaneous_Power_Id

				END


				WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Send_Supplier_Log_List)
				BEGIN

					SET @Sr_Rfp_Send_Supplier_Log_Id = (SELECT TOP 1 Sr_Rfp_Send_Supplier_Log_Id FROM @Sr_Rfp_Send_Supplier_Log_List)

					EXEC dbo.SR_RFP_Send_Supplier_Log_Del @Sr_Rfp_Send_Supplier_Log_Id

					DELETE
						@Sr_Rfp_Send_Supplier_Log_List
					WHERE
						Sr_Rfp_Send_Supplier_Log_Id = @Sr_Rfp_Send_Supplier_Log_Id

				END


				WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Send_Supplier_List)
				BEGIN

					SET @Sr_Rfp_Send_Supplier_Id=(SELECT TOP 1 Sr_Rfp_Send_Supplier_Id FROM @Sr_Rfp_Send_Supplier_List)

					EXEC dbo.SR_RFP_Send_Supplier_Del @Sr_Rfp_Send_Supplier_Id

					DELETE
						@Sr_Rfp_Send_Supplier_List
					WHERE
						Sr_Rfp_Send_Supplier_Id = @Sr_Rfp_Send_Supplier_Id

				END


				WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Archive_Account_List)
				BEGIN

					SET @Sr_Rfp_Archive_Account_Id = (SELECT TOP 1 Sr_Rfp_Archive_Account_Id FROM @Sr_Rfp_Archive_Account_List)

					EXEC dbo.SR_RFP_Archive_Account_Del @Sr_Rfp_Archive_Account_Id

					DELETE
						@Sr_Rfp_Archive_Account_List
					WHERE
						Sr_Rfp_Archive_Account_Id = @Sr_Rfp_Archive_Account_Id

				END


				WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Archive_Checklist_List)
				BEGIN

					SET @Sr_Rfp_Archive_Checklist_Id = (SELECT TOP 1 Sr_Rfp_Archive_Checklist_Id FROM @Sr_Rfp_Archive_Checklist_List)

					EXEC dbo.SR_RFP_Archive_Checklist_Del @Sr_Rfp_Archive_Checklist_Id

					DELETE
						@Sr_Rfp_Archive_Checklist_List
					WHERE
						 Sr_Rfp_Archive_Checklist_Id = @Sr_Rfp_Archive_Checklist_Id

				END


				WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Checklist_List)
				BEGIN

					SET @Sr_Rfp_Checklist_Id = (SELECT TOP 1 Sr_Rfp_Checklist_Id FROM @Sr_Rfp_Checklist_List)

					EXEC dbo.SR_RFP_Checklist_Del @Sr_Rfp_Checklist_Id

					DELETE
						@Sr_Rfp_Checklist_List
					WHERE
						 Sr_Rfp_Checklist_Id = @Sr_Rfp_Checklist_Id

				END


				WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Load_Profile_Setup_List)
				BEGIN

					SET @Sr_Rfp_Load_Profile_Setup_Id = (SELECT TOP 1 Sr_Rfp_Load_Profile_Setup_Id FROM @Sr_Rfp_Load_Profile_Setup_List)

					EXEC dbo.SR_RFP_Load_Profile_Setup_Del @Sr_Rfp_Load_Profile_Setup_Id

					DELETE
						@Sr_Rfp_Load_Profile_Setup_List
					WHERE
						 Sr_Rfp_Load_Profile_Setup_Id = @Sr_Rfp_Load_Profile_Setup_Id

				END



				WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Lp_Account_Additional_Row_List)
				BEGIN

					SET @Sr_Rfp_Lp_Account_Additional_Row_Id = (SELECT TOP 1 Sr_Rfp_Lp_Account_Additional_Row_Id FROM @Sr_Rfp_Lp_Account_Additional_Row_List)

					EXEC dbo.SR_RFP_LP_Account_Additional_Row_Del @Sr_Rfp_Lp_Account_Additional_Row_Id

					DELETE
						@Sr_Rfp_Lp_Account_Additional_Row_List
					WHERE
						 Sr_Rfp_Lp_Account_Additional_Row_Id = @Sr_Rfp_Lp_Account_Additional_Row_Id

				END



				WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Lp_Comment_List)
				BEGIN

					SET @Sr_Rfp_Lp_Comment_Id = (SELECT TOP 1 Sr_Rfp_Lp_Comment_Id FROM @Sr_Rfp_Lp_Comment_List)

					EXEC dbo.SR_RFP_LP_Comment_Del @Sr_Rfp_Lp_Comment_Id

					DELETE
						@Sr_Rfp_Lp_Comment_List
					WHERE
						 Sr_Rfp_Lp_Comment_Id = @Sr_Rfp_Lp_Comment_Id

				END


				WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Lp_Interval_Data_List)
				BEGIN

					SET @Sr_Rfp_Lp_Interval_Data_Id = (SELECT TOP 1 Sr_Rfp_Lp_Interval_Data_Id FROM @Sr_Rfp_Lp_Interval_Data_List)

					EXEC dbo.SR_RFP_LP_Interval_Data_Del @Sr_Rfp_Lp_Interval_Data_Id

					DELETE
						@Sr_Rfp_Lp_Interval_Data_List
					WHERE
						 Sr_Rfp_Lp_Interval_Data_Id = @Sr_Rfp_Lp_Interval_Data_Id

				END


				WHILE EXISTS(SELECT 1 FROM @Sr_Batch_Log_Details_List)
				BEGIN

					SET @Sr_Batch_Log_Details_Id = (SELECT TOP 1 Sr_Batch_Log_Details_Id FROM @Sr_Batch_Log_Details_List)

					EXEC dbo.SR_Batch_Log_Details_Del @Sr_Batch_Log_Details_Id

					DELETE
						@Sr_Batch_Log_Details_List
					WHERE
						 Sr_Batch_Log_Details_Id = @Sr_Batch_Log_Details_Id

				END



				WHILE EXISTS(SELECT 1 FROM @Sr_Cancel_Renegotiation_Document_List)
				BEGIN

					SET @Sr_Cancel_Renegotiation_Document_Id = (SELECT TOP 1 Sr_Cancel_Renegotiation_Document_Id FROM @Sr_Cancel_Renegotiation_Document_List)

					EXEC dbo.SR_Cancel_Renegotiation_Document_Del @Sr_Cancel_Renegotiation_Document_Id

					DELETE
						@Sr_Cancel_Renegotiation_Document_List
					WHERE
						 Sr_Cancel_Renegotiation_Document_Id = @Sr_Cancel_Renegotiation_Document_Id

				END


				WHILE EXISTS(SELECT 1 FROM @Sr_Comments_Accounts_Map_List)
				BEGIN

					SET @Sr_Comments_Accounts_Map_Id = (SELECT TOP 1 Sr_Comments_Accounts_Map_Id FROM @Sr_Comments_Accounts_Map_List)

					EXEC dbo.SR_Comments_Accounts_Map_Del @Sr_Comments_Accounts_Map_Id

					DELETE
						@Sr_Comments_Accounts_Map_List
					WHERE
						 Sr_Comments_Accounts_Map_Id = @Sr_Comments_Accounts_Map_Id

				END

				WHILE EXISTS(SELECT 1 FROM @Sr_Deal_Ticket_Account_Map_List)
				BEGIN

					SET @Sr_Deal_Ticket_Account_Map_Id = (SELECT TOP 1 Sr_Deal_Ticket_Account_Map_Id FROM @Sr_Deal_Ticket_Account_Map_List)

					EXEC dbo.SR_Deal_Ticket_Account_Map_Del @Sr_Deal_Ticket_Account_Map_Id

					DELETE
						@Sr_Deal_Ticket_Account_Map_List
					WHERE
						 Sr_Deal_Ticket_Account_Map_Id = @Sr_Deal_Ticket_Account_Map_Id

				END

				WHILE EXISTS(SELECT 1 FROM @Sr_Master_Checklist_Review_Status_List)
				BEGIN

					SET @Sr_Master_Checklist_Review_Status_Id = (SELECT TOP 1 Sr_Master_Checklist_Review_Status_Id FROM @Sr_Master_Checklist_Review_Status_List)

					EXEC dbo.Sr_Master_Checklist_Review_Status_Del @Sr_Master_Checklist_Review_Status_Id

					DELETE
						@Sr_Master_Checklist_Review_Status_List
					WHERE
						 Sr_Master_Checklist_Review_Status_Id = @Sr_Master_Checklist_Review_Status_Id

				END

				WHILE EXISTS(SELECT 1 FROM @Sr_Rc_Contract_Document_Accounts_Map_List)
				BEGIN

					SET @Sr_Rc_Contract_Document_Accounts_Map_Id = (SELECT TOP 1 Sr_Rc_Contract_Document_Accounts_Map_Id FROM @Sr_Rc_Contract_Document_Accounts_Map_List)
					

					EXEC dbo.Sr_Rc_Contract_Document_Accounts_Map_Del @Sr_Rc_Contract_Document_Accounts_Map_Id

					DELETE
						@Sr_Rc_Contract_Document_Accounts_Map_List
					WHERE
						 Sr_Rc_Contract_Document_Accounts_Map_Id = @Sr_Rc_Contract_Document_Accounts_Map_Id

				END

				WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Account_List)
				BEGIN

					SET @Sr_Rfp_Account_Id=(SELECT TOP 1 Sr_Rfp_Account_Id FROM @Sr_Rfp_Account_List)

					EXEC dbo.SR_RFP_Account_Del @Sr_Rfp_Account_Id

					DELETE
						@Sr_Rfp_Account_List
					WHERE
						Sr_Rfp_Account_Id = @Sr_Rfp_Account_Id

				END

				WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Bid_Group_List)
				BEGIN

					SELECT TOP 1 @Sr_Rfp_Bid_Group_Id = Sr_Rfp_Bid_Group_Id,@Sr_Rfp_Bid_Group_Name = Group_Name FROM @Sr_Rfp_Bid_Group_List

					EXEC dbo.SR_RFP_BID_Group_Del @Sr_Rfp_Bid_Group_Id

					DELETE
						@Sr_Rfp_Bid_Group_List
					WHERE
						Sr_Rfp_Bid_Group_Id = @Sr_Rfp_Bid_Group_Id

				END
			
			COMMIT TRAN
		END TRY
		
		BEGIN CATCH
		
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRAN
			END
				
			EXEC dbo.usp_RethrowError

		END CATCH
	
	END

END
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_History_Del_For_Account] TO [CBMSApplication]
GO
