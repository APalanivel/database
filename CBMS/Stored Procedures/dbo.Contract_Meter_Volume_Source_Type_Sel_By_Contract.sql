SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:    dbo.Contract_Meter_Volume_Source_Type_Sel_By_Contract 
     
      
DESCRIPTION:     
     
      
INPUT PARAMETERS:      
      Name          DataType       Default        Description      
-----------------------------------------------------------------------------      
   
    
      
OUTPUT PARAMETERS:    
      
 Name     DataType   Default  Description      
-----------------------------------------------------------------------------      
      
      
      
USAGE EXAMPLES:      
-----------------------------------------------------------------------------      
   
	Exec dbo.Contract_Meter_Volume_Source_Type_Sel_By_Contract @Contract_Id = 185074, @ED_CONTRACT_NUMBER = NULL
	Exec dbo.Contract_Meter_Volume_Source_Type_Sel_By_Contract @Contract_Id = NULL, @ED_CONTRACT_NUMBER = '185074-0001'

  
   
AUTHOR INITIALS:       
	Initials    Name  
-----------------------------------------------------------------------------         
	RR			Raghu Reddy
      
MODIFICATIONS       
	Initials    Date		Modification        
-----------------------------------------------------------------------------         
	RR			2020-04-18	GRM - Contract volumes enhancement - Excel plugin - Created
******/
CREATE PROCEDURE [dbo].[Contract_Meter_Volume_Source_Type_Sel_By_Contract]
    (
        @Contract_Id INT = NULL
        , @ED_CONTRACT_NUMBER VARCHAR(150) = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            @Contract_Id = ISNULL(@Contract_Id, c.CONTRACT_ID)
        FROM
            dbo.CONTRACT c
        WHERE
            c.ED_CONTRACT_NUMBER = @ED_CONTRACT_NUMBER
            AND @ED_CONTRACT_NUMBER IS NOT NULL;

        SELECT
            ch.Client_Name
            , ch.Client_Id
            , c.ED_CONTRACT_NUMBER
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE
            , c2.Commodity_Name
            , c.COMMODITY_TYPE_ID
            , c.CONTRACT_ID
            , c.Contract_Meter_Volume_Source_Cd
            , srccd.Code_Value AS Contract_Meter_Volume_Source
            , c.Contract_Meter_Volume_Type_Cd
            , typcd.Code_Value AS Contract_Meter_Volume_Type
        FROM
            dbo.CONTRACT c
            LEFT JOIN dbo.Code srccd
                ON c.Contract_Meter_Volume_Source_Cd = srccd.Code_Id
            LEFT JOIN dbo.Code typcd
                ON c.Contract_Meter_Volume_Type_Cd = typcd.Code_Id
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Supplier_Contract_ID = c.CONTRACT_ID
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN dbo.Commodity c2
                ON c2.Commodity_Id = c.COMMODITY_TYPE_ID
        WHERE
            c.CONTRACT_ID = @Contract_Id
        GROUP BY
            ch.Client_Name
            , ch.Client_Id
            , c.ED_CONTRACT_NUMBER
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE
            , c2.Commodity_Name
            , c.COMMODITY_TYPE_ID
            , c.CONTRACT_ID
            , c.Contract_Meter_Volume_Source_Cd
            , srccd.Code_Value
            , c.Contract_Meter_Volume_Type_Cd
            , typcd.Code_Value;


    END;
GO
GRANT EXECUTE ON  [dbo].[Contract_Meter_Volume_Source_Type_Sel_By_Contract] TO [CBMSApplication]
GO
