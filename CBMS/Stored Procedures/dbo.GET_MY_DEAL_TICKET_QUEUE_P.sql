
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_MY_DEAL_TICKET_QUEUE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@dealTicketNumber	varchar(200)	          	
	@clientId      	int       	          	
	@siteId        	int       	          	
	@startDate     	datetime  	          	
	@endDate       	datetime  	          	
	@dealStatusTypeId	int       	          	
	@currentlyWithId	int       	          	
	@startDateFlag 	bit       	          	
	@endDateFlag   	bit       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.GET_MY_DEAL_TICKET_QUEUE_P '-1','-1','105070','','','','','','',0,0
	EXEC dbo.GET_MY_DEAL_TICKET_QUEUE_P 49,-1,'',0,0,'2010-10-18','2010-10-18',0,15028,'false','false'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/27/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on
	HG			10/18/2010	Script modified to fix the QI post production release.
							DEAL_TRANSACTION_TYPE column set to empty string in the inner select query as in the WHERE clause of derived table we have filter on this column.
							(If the column value equals to NULL means it will not be selected as ANSI NULLS is set ON now, earlier it worked as ANSI NULLS was set off)
	BCH			2013-03-28	Added Hedge_Type column to select list (MAIN - 1795).
******/
CREATE PROCEDURE dbo.GET_MY_DEAL_TICKET_QUEUE_P
      @userId VARCHAR(10)
     ,@sessionId VARCHAR(20)
     ,@dealTicketNumber VARCHAR(200)
     ,@clientId INT
     ,@siteId INT
     ,@startDate DATETIME
     ,@endDate DATETIME
     ,@dealStatusTypeId INT
     ,@currentlyWithId INT
     ,@startDateFlag BIT
     ,@endDateFlag BIT
AS 
BEGIN

      SET NOCOUNT ON;

      DECLARE @selectClause VARCHAR(8000)
      DECLARE @fromClause VARCHAR(8000)
      DECLARE @whereClause VARCHAR(8000)
      DECLARE @SQLStatement VARCHAR(8000)

      SELECT
            @selectClause = ' hedge.ENTITY_NAME AS HEDGE_LEVEL_TYPE, ISNULL(trans.ENTITY_NAME,'''') AS DEAL_TRANSACTION_TYPE,' + ' rmdt.RM_DEAL_TICKET_ID, rmdt.DEAL_TICKET_NUMBER, c.CLIENT_NAME, ' + ' viewSite.SITE_NAME, ' + ' d.DIVISION_NAME, ' + ' h_type.ENTITY_NAME AS Hedge_Type, ' + ' e1.ENTITY_NAME AS DEAL_TYPE,  rmdt.DEAL_INITIATED_DATE, ' + ' rmdt.HEDGE_START_MONTH, rmdt.HEDGE_END_MONTH, ' + ' (ui.FIRST_NAME + '' '' + ui.LAST_NAME) AS CURRENTLY_WITH_NAME,' + ' case e2.ENTITY_NAME ' + ' when ''New'' then ''New'' ' + ' when ''Client Approved'' then ''Client Approved'' ' + ' when ''Place Bid'' then ''Bidding'' ' + ' when ''Manage Bid'' then ''Bidding'' ' + ' when ''Order Placed'' then ''Order Placed'' ' + ' when ''Order Executed'' then ''Order Executed'' ' + ' when ''Potentially Fixed'' then ''Potentially Fixed'' ' + ' when ''Closed'' then ''Closed'' ' + ' when ''Marked To Cancel'' then ''Marked To Cancel''  ' + ' when ''Cancelled'' then ''Cancelled'' ' + ' when ''Expired'' then ''Expired'' ' + ' else  ' + ' (select 	 ' + ' case count(1) ' + ' when 1 then ''Order Executed'' ' + ' when 2 then ''Closed'' ' + ' end  ' + ' from 	rm_deal_ticket_transaction_status, entity ' + ' where	rm_deal_ticket_id = rmdt.rm_deal_ticket_id and  ' + ' deal_transaction_status_type_id = entity_id and  ' + ' entity_name in (''Client Confirmation'', ''Counterparty Confirmation'')) ' + ' end AS DEAL_STATUS  ' 

      SELECT
            @fromClause = 'RM_DEAL_TICKET rmdt ' + ' LEFT JOIN SITE s ON (rmdt.SITE_ID = s.SITE_ID) ' + ' LEFT JOIN vwSiteName viewSite ON (s.SITE_ID = viewSite.SITE_ID) ' + 'LEFT JOIN DIVISION d ON (rmdt.DIVISION_ID = d.DIVISION_ID) ' + 'LEFT JOIN ENTITY trans ON (rmdt.DEAL_TRANSACTION_TYPE_ID = trans.ENTITY_ID) ' + 'LEFT JOIN ENTITY h_type ON (h_type.ENTITY_ID = rmdt.HEDGE_TYPE_ID) ' + 'JOIN  ENTITY hedge ON (rmdt.HEDGE_LEVEL_TYPE_ID = hedge.ENTITY_ID) ' + 'JOIN USER_INFO ui ON (rmdt.QUEUE_ID = ui.QUEUE_ID) ' + 'JOIN  CLIENT c ON (rmdt.CLIENT_ID = c.CLIENT_ID) ' + 'JOIN  ENTITY e1 ON (rmdt.DEAL_TYPE_ID = e1.ENTITY_ID) ' + 'JOIN  RM_DEAL_TICKET_TRANSACTION_STATUS rmdtts ON (rmdt.RM_DEAL_TICKET_ID = rmdtts.RM_DEAL_TICKET_ID), ' + ' ENTITY e2 '   

      SELECT
            @whereClause = 'ui.USER_INFO_ID =' + STR(@currentlyWithId) + ' AND rmdtts.DEAL_TRANSACTION_STATUS_TYPE_ID = e2.ENTITY_ID ' + ' AND rmdtts.DEAL_TRANSACTION_DATE IN ' + '(SELECT MAX(DEAL_TRANSACTION_DATE) ' + 'FROM RM_DEAL_TICKET_TRANSACTION_STATUS  ' + 'WHERE RM_DEAL_TICKET_ID = rmdt.RM_DEAL_TICKET_ID) ' 
	
      IF @dealTicketNumber <> '' 
            BEGIN
                  SELECT
                        @whereClause = @whereClause + ' AND rmdt.DEAL_TICKET_NUMBER = ' + '''' + @dealTicketNumber + ''''
            END 
	
      IF @clientId > 0 
            BEGIN
                  SELECT
                        @whereClause = @whereClause + ' AND rmdt.CLIENT_ID = ' + STR(@clientId) 
            END 

      IF @siteId > 0 
            BEGIN
                  SELECT
                        @whereClause = @whereClause + ' AND rmdt.SITE_ID = ' + STR(@siteId) 
            END 

      IF @startDateFlag <> 0 
            BEGIN
                  SELECT
                        @whereClause = @whereClause + ' AND month(rmdt.HEDGE_START_MONTH) = month(' + '''' + CONVERT (VARCHAR(12), @startDate, 1) + '''' + ')' + ' AND year(rmdt.HEDGE_START_MONTH) = year(' + '''' + CONVERT (VARCHAR(12), @startDate, 1) + '''' + ')'
            END 

      IF @endDateFlag <> 0 
            BEGIN

                  SELECT
                        @whereClause = @whereClause + ' AND month(rmdt.HEDGE_END_MONTH) = month(' + '''' + CONVERT (VARCHAR(12), @endDate, 1) + '''' + ')' + ' AND year(rmdt.HEDGE_END_MONTH) = year(' + '''' + CONVERT(VARCHAR(12), @endDate, 1) + '''' + ')'
            END

      IF @dealStatusTypeId > 0 
            BEGIN

                  SELECT
                        @whereClause = @whereClause + ' AND rmdtts.DEAL_TRANSACTION_STATUS_TYPE_ID = ' + STR(@dealStatusTypeId)

            END

      SELECT
            @SQLStatement = 'SELECT * FROM (SELECT ' + @selectClause + 'FROM ' + @fromClause + 'WHERE ' + @whereClause + ' ) A WHERE A.DEAL_STATUS NOT IN (''Closed'', ''Cancelled'', ''Expired'') ' + ' AND A.DEAL_TRANSACTION_TYPE NOT IN (''Triggers placed and hit'') ' + ' ORDER BY ' + 'A.RM_DEAL_TICKET_ID DESC '

      EXEC(@SQLStatement)

END

;
GO

GRANT EXECUTE ON  [dbo].[GET_MY_DEAL_TICKET_QUEUE_P] TO [CBMSApplication]
GO
