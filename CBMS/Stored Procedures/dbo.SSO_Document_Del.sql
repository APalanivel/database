SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******************************************************************************************************         
NAME: dbo.SSO_Document_Del
    
DESCRIPTION:    
    
      To delete a document
    
INPUT PARAMETERS:    
      NAME					DATATYPE		DEFAULT           DESCRIPTION    
------------------------------------------------------------------------    
  @Document_Id		         INT			
                    
OUTPUT PARAMETERS:              
      NAME              DATATYPE    DEFAULT           DESCRIPTION       
           
------------------------------------------------------------              
USAGE EXAMPLES:              
------------------------------------------------------------            
BEGIN TRAN
EXEC dbo.SSO_Document_Del 16528
ROLLBACK TRAN
	 
AUTHOR INITIALS:              
      INITIALS    NAME              
------------------------------------------------------------              
      RMG         RANI MARY GEORGE   
	      
MODIFICATIONS:    
      INITIALS    DATE				MODIFICATION              
------------------------------------------------------------              
      RMG 		  23-AUG-14			CREATED
******************************************************************************************************/ 

CREATE PROCEDURE [dbo].[SSO_Document_Del] 
	( 
		@Document_Id INT 
	)
AS 
BEGIN
	
      SET NOCOUNT ON;

      BEGIN TRY
            BEGIN TRAN
            DELETE
                  spdm
            FROM
                  dbo.SSO_PROJECT_DOCUMENT_MAP spdm
            WHERE
                  spdm.SSO_DOCUMENT_ID = @Document_Id

            DELETE
                  cdcm
            FROM
                  dbo.Client_document_Category_map cdcm
            WHERE
                  cdcm.SSO_DOCUMENT_ID = @Document_Id

            DELETE
                  sdom
            FROM
                  dbo.SSO_Document_OWNER_MAP sdom
            WHERE
                  sdom.SSO_DOCUMENT_ID = @Document_Id
			

            DELETE
                  sdvl
            FROM
                  dbo.SSO_DOCUMENT_VIEW_LOG sdvl
            WHERE
                  sdvl.SSO_DOCUMENT_ID = @Document_Id

            DELETE
                  sd
            FROM
                  dbo.SSO_DOCUMENT sd
            WHERE
                  sd.SSO_DOCUMENT_ID = @Document_Id

            COMMIT TRAN
      END TRY
      BEGIN CATCH
            ROLLBACK TRAN      
      END CATCH

END







;
GO
GRANT EXECUTE ON  [dbo].[SSO_Document_Del] TO [CBMSApplication]
GO
