SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        dbo.RM_Client_Workflow_Upd                    
                      
Description:                      
        To get global site groups
                      
Input Parameters:                      
    Name									DataType        Default     Description                        
------------------------------------------------------------------------------------
	@RM_Client_Deal_Ticket_Work_Flow_Map_Id INT
    @RM_Deal_Ticket_Work_Flow_Id			INT
    @User_Info_Id							INT
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	@RM_Group_Id	INT  
                    
Usage Examples:                          
--------------------------------------------------------------------------------
	
	BEGIN TRANSACTION
		EXEC dbo.RM_Deal_Ticket_Work_Flow_Sel
		--EXEC dbo.RM_Client_Deal_Ticket_Work_Flow_Map_Sel 235
		EXEC dbo.RM_Client_Deal_Ticket_Work_Flow_Map_Sel 235,1
		EXEC dbo.RM_Client_Workflow_Upd 235,1,4,49
		EXEC dbo.RM_Client_Deal_Ticket_Work_Flow_Map_Sel 235
	ROLLBACK TRANSACTION    
                     
 Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy        
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			2018-09-12     Global Risk Management - Created                    
                     
******/       
CREATE PROCEDURE [dbo].[RM_Client_Workflow_Upd]
      ( 
       @RM_Client_Workflow_Id INT
      ,@Workflow_Id INT
      ,@User_Info_Id INT )
AS 
BEGIN      
      SET NOCOUNT ON;
                   
      UPDATE
            dbo.RM_Client_Workflow
      SET   
            Workflow_Id = @Workflow_Id
           ,Updated_User_Id = @User_Info_Id
           ,Last_Change_Ts = GETDATE()
      WHERE
            RM_Client_Workflow_Id = @RM_Client_Workflow_Id
                                   
            
END;
GO
GRANT EXECUTE ON  [dbo].[RM_Client_Workflow_Upd] TO [CBMSApplication]
GO
