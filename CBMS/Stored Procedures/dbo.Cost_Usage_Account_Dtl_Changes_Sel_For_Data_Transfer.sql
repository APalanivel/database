
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                                        
                              
NAME: [DBO].[Cost_Usage_Account_Dtl_Changes_Sel_For_Data_Transfer]                              
                              
DESCRIPTION:                               
      Select the changed Account Level data from cost_usage and cost_usage_Account_Dtl tables(using change tracking) to transfer                  
      it to Hub                              
                                    
INPUT PARAMETERS:                                        
NAME			DATATYPE DEFAULT  DESCRIPTION                                        
------------------------------------------------------------                                        
 @Min_CT_Ver	BIGINT                      
 @Max_CT_Ver	BIGINT                           
                                              
OUTPUT PARAMETERS:                                        
NAME   DATATYPE DEFAULT  DESCRIPTION                                 
                                     
------------------------------------------------------------                                        
USAGE EXAMPLES:                                        
------------------------------------------------------------                              
                  
DECLARE @Current_Version BIGINT                  
SELECT @Current_Version = CHANGE_TRACKING_CURRENT_VERSION()                  
EXEC dbo.Cost_Usage_Account_Dtl_Changes_Sel_For_Data_Transfer 0,@Current_Version                  
                  
AUTHOR INITIALS:                                        
INITIALS	NAME                  
------------------------------------------------------------                                        
HG			Harihara Suthan G
AKR			Ashok Kumar Raju

MODIFICATIONS                              
INITIALS	DATE		MODIFICATION                              
------------------------------------------------------------                              
AKR			2011-11-20  Created                         
HG			2012-02-18  Script modified to fetch the cost usage data only if the account is mapped with the commodity of the bucket.                  
AKR			2012-03-30  Modified the script for Additional Data   
AKR			2012-10-08  Modified the code the Get the Demand data as a part of POCO.        
AKR			2013-01-04  Modified the code to get all the buckets as a part of Detailed Report. 
HG		    2013-06-13	Removed the INNER JOIN on Core.Client_Hier_Account as it is not allowing the script to select the deleted records from Changes table if an account is moved to a different site.  
						Removed the unused table variables
RKV         2017-06-22  Added new column Data_Type_Cd as part of Data enhancement.						
*/
CREATE PROCEDURE [dbo].[Cost_Usage_Account_Dtl_Changes_Sel_For_Data_Transfer]
      ( 
       @Min_CT_Ver BIGINT
      ,@Max_CT_Ver BIGINT )
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE
            @CBMS_Data_Source_Cd INT
           ,@Invoice_Data_Source_Cd INT

      SELECT
            @Invoice_Data_Source_Cd = MAX(CASE WHEN cd.Code_Value = 'Invoice' THEN cd.CODE_ID
                                          END)
           ,@CBMS_Data_Source_Cd = MAX(CASE WHEN cd.Code_Value = 'CBMS' THEN cd.CODE_ID
                                       END)
      FROM
            dbo.codeset cs
            INNER JOIN dbo.code cd
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Std_Column_Name = 'Data_Source_Cd'
            AND cd.Code_Value IN ( 'Invoice', 'CBMS' );                                      
        
      SELECT
            ct.Service_Month
           ,ct.Bucket_Master_Id
           ,cuad.Bucket_Value
           ,cuad.UOM_Type_Id
           ,cuad.CURRENCY_UNIT_ID
           ,cuad.Created_By_Id
           ,cuad.Created_Ts
           ,ISNULL(cuad.Updated_Ts, cuad.Created_Ts) Updated_Ts
           ,cuad.Updated_By_Id
           ,ct.Account_Id
           ,cuad.Data_Source_Cd
           ,ct.Client_Hier_Id
           ,CAST(ct.SYS_CHANGE_OPERATION AS CHAR(1)) AS Sys_Change_Operation
           ,cuad.Data_Type_Cd
      FROM
            CHANGETABLE(CHANGES dbo.Cost_Usage_Account_Dtl, @Min_CT_Ver) ct
            LEFT OUTER JOIN dbo.Cost_Usage_Account_Dtl cuad
                  ON cuad.Account_Id = ct.Account_Id
                     AND cuad.client_Hier_Id = ct.client_Hier_Id
                     AND cuad.Bucket_Master_Id = ct.Bucket_Master_Id
                     AND cuad.Service_Month = ct.Service_Month
                     AND cuad.Data_Source_Cd IN ( @CBMS_Data_Source_Cd, @Invoice_Data_Source_Cd )
      WHERE
            ct.Sys_Change_Version <= @Max_CT_Ver
            AND ( ct.Sys_Change_Operation = 'D'
                  OR ( ct.SYS_CHANGE_OPERATION IN ( 'I', 'U' )
                       AND cuad.Data_Source_Cd IN ( @CBMS_Data_Source_Cd, @Invoice_Data_Source_Cd ) ) )
      GROUP BY
            ct.Service_Month
           ,ct.Bucket_Master_Id
           ,cuad.Bucket_Value
           ,cuad.UOM_Type_Id
           ,cuad.CURRENCY_UNIT_ID
           ,cuad.Created_By_Id
           ,cuad.Created_Ts
           ,cuad.Updated_Ts
           ,cuad.Updated_By_Id
           ,ct.Account_Id
           ,cuad.Data_Source_Cd
           ,ct.Client_Hier_Id
           ,cuad.Data_Type_Cd
           ,ct.SYS_CHANGE_OPERATION

END;

;
GO






GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Dtl_Changes_Sel_For_Data_Transfer] TO [CBMSApplication]
GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Dtl_Changes_Sel_For_Data_Transfer] TO [ETL_Execute]
GO
