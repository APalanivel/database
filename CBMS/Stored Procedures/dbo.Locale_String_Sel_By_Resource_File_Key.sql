SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 dbo.Locale_String_Sel_By_Resource_File_Key    
 
DESCRIPTION:  

INPUT PARAMETERS:    
Name			DataType		Default						Description    
------------------------------------------------------------------------    
@Resource_File	NVARCHAR(100)	CbmsSourcingNotification
@Key			NVARCHAR(255)	NULL

OUTPUT PARAMETERS:    
Name					DataType	Default			Description    
-------------------------------------------------------------------------
 
USAGE EXAMPLES:    
-------------------------------------------------------------------------    
    
EXEC dbo.Locale_String_Sel_By_Resource_File_Key 
EXEC dbo.Locale_String_Sel_By_Resource_File_Key 'CbmsSourcingNotification','ElectricPower'

AUTHOR INITIALS:    
Initials	Name    
-------------------------------------------------------------------------    
RR			Raghu Reddy

MODIFICATIONS:
Initials	Date		Modification    
-------------------------------------------------------------------------    
RR			2016-07-13	GCS - 5b - Created					
						
    
******/    
    
CREATE PROCEDURE [dbo].[Locale_String_Sel_By_Resource_File_Key]
      ( 
       @Resource_File NVARCHAR(100) = 'CbmsSourcingNotification'
      ,@Key NVARCHAR(255) = NULL )
AS 
BEGIN    
    
      SET NOCOUNT ON; 
      
      SELECT
            *
      FROM
            ( SELECT
                  rf.Resource_File_Name
                 ,rk.Resource_Key
                 ,rklv.Locale_Value
                 ,lng.Code_Value AS Translation_Language
              FROM
                  Resource_CS.dbo.Resource_File_Key rfk
                  INNER JOIN Resource_CS.dbo.Resource_File rf
                        ON rfk.Resource_File_Id = rf.Resource_File_Id
                  INNER JOIN Resource_CS.dbo.Resource_File_Location rfl
                        ON rf.Resource_File_Location_Id = rfl.Resource_File_Location_Id
                  INNER JOIN dbo.Code rff
                        ON rff.Code_Id = rf.File_Format_Cd
                  INNER JOIN Resource_CS.dbo.Resource_Key rk
                        ON rfk.Resource_Key_Id = rk.Resource_Key_Id
                  INNER JOIN Resource_CS.dbo.Resource_Key_Locale_Value rklv
                        ON rklv.Resource_File_Key_Id = rfk.Resource_File_Key_Id
                  INNER JOIN dbo.Code lng
                        ON lng.Code_Id = rklv.Language_Cd
              WHERE
                  rf.Resource_File_Name = @Resource_File
                  AND ( @Key IS NULL
                        OR rk.Resource_Key = @Key ) ) t PIVOT ( MIN(Locale_Value) FOR Translation_Language IN ( [en-US], [fr-FR], [nl-BE], [de-DE], [It-IT], [es-ES], [pl-PL], [hu-HU], [en-GB], [en-AU] ) ) pvt
			



      
      
        

END;
;
GO
GRANT EXECUTE ON  [dbo].[Locale_String_Sel_By_Resource_File_Key] TO [CBMSApplication]
GO
