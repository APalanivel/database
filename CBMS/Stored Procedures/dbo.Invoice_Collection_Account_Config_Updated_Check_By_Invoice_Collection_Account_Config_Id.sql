SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Invoice_Collection_Account_Config_Updated_Check_By_Invoice_Collection_Account_Config_Id       
              
Description:              
			This sproc is to check whether the account config or invoice changes are updated or not    
                           
 Input Parameters:              
    Name									DataType								Default			Description                
-----------------------------------------------------------------------------------------------------------------                
	@tvp_Invoice_Collection_Queue_Sel		tvp_Invoice_Collection_Queue_Sel                      
    @Tvp_Invoice_Collection_Sources         Tvp_Invoice_Collection_Sources
    @Start_Index							INT
    @End_Index								INT
    @Total_Count							INT
    
 Output Parameters:                    
    Name								DataType			Default			Description                
-----------------------------------------------------------------------------------------------------------------                
              
 Usage Examples:                  
-----------------------------------------------------------------------------------------------------------------                


 EXEC dbo.Invoice_Collection_Account_Config_Updated_Check_By_Invoice_Collection_Account_Config_Id 
      @Invoice_Collection_Account_Config_Id = 303
     
              
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2017-01-31		Created For Invoice_Collection.         
             
******/ 
CREATE PROCEDURE [dbo].[Invoice_Collection_Account_Config_Updated_Check_By_Invoice_Collection_Account_Config_Id]
      ( 
       @Invoice_Collection_Account_Config_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;

      DECLARE
            @Is_Account_Config_Updated BIT = 0
           ,@Is_Invoice_Created BIT = 0
           ,@Is_Invoice_Deleted BIT = 0
           ,@Is_First_Time BIT = 0


      
      SELECT
            @Is_First_Time = 1
      FROM
            dbo.Invoice_Collection_Account_Config icac
      WHERE
            icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              dbo.Account_Invoice_Collection_Month aicm
                             WHERE
                              aicm.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id )
      
      SELECT
            @Is_Account_Config_Updated = 1
      FROM
            dbo.Invoice_Collection_Account_Config icac
            INNER JOIN dbo.Invoice_Collection_Queue icq
                  ON icac.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
      WHERE
            icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
      GROUP BY
            icac.Last_Change_Ts
           ,icq.Last_Change_Ts
           ,icac.Invoice_Collection_Account_Config_Id
      HAVING
            icac.Last_Change_Ts > MAX(ICQ.Last_Change_Ts)


      SELECT
            @Is_Invoice_Created = 1
      FROM
            dbo.Invoice_Collection_Account_Config icac
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                  ON icac.Account_Id = cism.Account_ID
      WHERE
            icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
            AND cism.SERVICE_MONTH BETWEEN icac.Invoice_Collection_Service_Start_Dt
                                   AND     icac.Invoice_Collection_Service_End_Dt
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map aicmcim
                              INNER JOIN dbo.Account_Invoice_Collection_Month aicm
                                    ON aicmcim.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id
                             WHERE
                              aicmcim.Cu_Invoice_Id = cism.Cu_Invoice_Id
                              AND aicm.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id )
                        
                        
      SELECT
            @Is_Invoice_Deleted = 1
      FROM
            dbo.Invoice_Collection_Account_Config icac
            INNER JOIN dbo.Account_Invoice_Collection_Month aicm
                  ON icac.Invoice_Collection_Account_Config_Id = aicm.Invoice_Collection_Account_Config_Id
            INNER JOIN Account_Invoice_Collection_Month_Cu_Invoice_Map aicmcim
                  ON aicmcim.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id
      WHERE
            icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
            AND aicm.SERVICE_MONTH BETWEEN icac.Invoice_Collection_Service_Start_Dt
                                   AND     icac.Invoice_Collection_Service_End_Dt
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              dbo.CU_INVOICE_SERVICE_MONTH cism
                             WHERE
                              aicmcim.Cu_Invoice_Id = cism.Cu_Invoice_Id )


      SELECT
            @Is_Account_Config_Updated Is_Account_Config_Updated
           ,@Is_Invoice_Created Is_Invoice_Created
           ,@Is_Invoice_Deleted Is_Invoice_Deleted
           ,@Is_First_Time Is_First_Time

END;
;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Account_Config_Updated_Check_By_Invoice_Collection_Account_Config_Id] TO [CBMSApplication]
GO
