SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE         PROCEDURE [dbo].[cbmsClient_GetAll]
	( @MyAccountId int 
	, @region_id int = null
	, @state_id int = null
	)
AS
BEGIN

	   select distinct c.client_id
		, c.client_name
		, sm.start_month
	     from client c
	     join vwClientFiscalYearStartMonth sm on sm.client_id = c.client_id
	 order by c.client_name

END
GO
GRANT EXECUTE ON  [dbo].[cbmsClient_GetAll] TO [CBMSApplication]
GO
