SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- select * from SR_RFP_SMU

CREATE PROCEDURE [dbo].[SR_RFP_UPLOAD_SMU_IMAGE_P]
	@userId INT,
	@sessionId VARCHAR(200),
	@cbmsImageId INT,
	@rfpId INT,
	@comments VARCHAR(200)
AS
BEGIN

	SET NOCOUNT ON
	
	DECLARE @entityId INT
	
	SELECT @entityId = ENTITY_ID FROM dbo.ENTITY (NOLOCK) WHERE Entity_Name = 'SMU' AND Entity_Type = 100
	
	 /*INSERT INTO CBMS_IMAGE (CBMS_IMAGE_TYPE_ID, CBMS_DOC_ID, CBMS_IMAGE, CONTENT_TYPE, DATE_IMAGED)   
	 VALUES (@entityId, @cbmsImageDocId, @cbmsImage, @contentType, getDate())  
	*/
	
	UPDATE dbo.CBMS_IMAGE SET CBMS_IMAGE_TYPE_ID = @entityId WHERE cbms_image_id = @cbmsImageId

	--select @cbmsImageId = (select @@Identity)
	
	INSERT INTO dbo.SR_RFP_SMU(SR_RFP_ID
		, CBMS_IMAGE_ID
		, UPLOADED_BY
		, UPLOADED_DATE
		, COMMENTS)
	VALUES (@rfpId
		, @cbmsImageId
		, @userId
		, GETDATE()
		, @comments)

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_UPLOAD_SMU_IMAGE_P] TO [CBMSApplication]
GO
