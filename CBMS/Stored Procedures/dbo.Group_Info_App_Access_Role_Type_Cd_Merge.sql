SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME: dbo.Group_Info_App_Access_Role_Type_Cd_Merge

	To save the App_Access_Role_Type_Cd for a group

  
DESCRIPTION:   
  
INPUT PARAMETERS:      
 Name						DataType			Default     Description      
------------------------------------------------------------------      
 @Group_Info_Id				INT					
 @App_Access_Role_Type_Cd	INT					NULL		will be null if the user doesn't assign any role for the group

OUTPUT PARAMETERS:      
Name              DataType          Default     Description      
------------------------------------------------------------      
  
USAGE EXAMPLES:  
------------------------------------------------------------  

	BEGIN TRAN
		EXEC dbo.Group_Info_App_Access_Role_Type_Cd_Merge  
			@group_info_id =209
			,@App_Access_Role_Type_Cd = 100260
	ROLLBACK TRAN

	BEGIN TRAN
		EXEC dbo.Group_Info_App_Access_Role_Type_Cd_Merge  
			@group_info_id =1
			,@App_Access_Role_Type_Cd = NULL
	ROLLBACK TRAN
	
AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------
HG			Harihara Suthan G

MODIFICATIONS
Initials	Date			Modification
------------------------------------------------------------
HG			2014-02-04		Created for RA Admin UM enhancement
******/

CREATE PROCEDURE dbo.Group_Info_App_Access_Role_Type_Cd_Merge
      (
       @Group_Info_Id INT
      ,@App_Access_Role_Type_Cd INT = NULL )
AS
BEGIN

      SET NOCOUNT ON;

      MERGE INTO dbo.Group_Info_App_Access_Role_Type_Cd tgt
            USING
                  ( SELECT
                        @Group_Info_Id AS Group_Info_Id
                       ,@App_Access_Role_Type_Cd AS App_Access_Role_Type_Cd ) src
            ON src.Group_Info_Id = tgt.Group_Info_Id
            WHEN MATCHED AND @App_Access_Role_Type_Cd IS NULL
                  THEN
				DELETE
            WHEN MATCHED AND @App_Access_Role_Type_Cd IS NOT NULL
                  THEN
                  UPDATE
                    SET 
                        App_Access_Role_Type_Cd = @App_Access_Role_Type_Cd
            WHEN NOT MATCHED AND @App_Access_Role_Type_Cd IS NOT NULL
                  THEN
                  INSERT
                        ( 
                         Group_Info_Id
                        ,App_Access_Role_Type_Cd )
                    VALUES
                        ( 
                         src.Group_Info_Id
                        ,src.App_Access_Role_Type_Cd );

END;
;
GO
GRANT EXECUTE ON  [dbo].[Group_Info_App_Access_Role_Type_Cd_Merge] TO [CBMSApplication]
GO
