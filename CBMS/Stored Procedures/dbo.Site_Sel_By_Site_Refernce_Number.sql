SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
    
NAME: 
               
	[dbo].[Site_Sel_By_Site_Refernce_Number]           
    
DESCRIPTION:     
     
	This procedure for checking the existance of Site Id      
    

INPUT PARAMETERS:          
Name                     DataType          Default     Description            
---------------------------------------------------------------          
    
@SITE_REFERENCE_NUMBER   Nvarchar(30)         
    
    
OUTPUT PARAMETERS:          
    
 Name              DataType          Default     Description          
----------------------------------------------------------------          
 
    
USAGE EXAMPLES:          
------------------------------------------------------------          
  EXEC Site_Sel_By_Site_Refernce_Number '',1043

AUTHOR INITIALS:          
    
Initials    Name                
------------------------------------------------------------          
Kamma       Srinivasa Rao               
     

MODIFICATIONS:          
    
 Initials    Date           Modification            
------------------------------------------------------------           
 Kamma       04/06/2014     Created        
    
******/         
CREATE PROCEDURE [dbo].[Site_Sel_By_Site_Refernce_Number]
      (
       @SITE_REFERENCE_NUMBER VARCHAR(30)
      ,@Client_Id INT )
AS
BEGIN          
    
      SET NOCOUNT ON; 
	          
      SELECT
            SITE_ID
      FROM
            [dbo].[SITE]
      WHERE
            [SITE_REFERENCE_NUMBER] = @SITE_REFERENCE_NUMBER
            AND Client_ID = @Client_Id
END        
;
GO
GRANT EXECUTE ON  [dbo].[Site_Sel_By_Site_Refernce_Number] TO [CBMSApplication]
GO
