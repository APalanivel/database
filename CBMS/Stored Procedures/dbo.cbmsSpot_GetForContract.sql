SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[cbmsSpot_GetForContract]
	( @MyAccountId int
	, @contract_id int
	)
AS
BEGIN

	   select spot_id
		, contract_id
		, spot_name
		, spot_from_date
		, spot_to_date
		, spot_volume
		, spot_price
	     from spot
	    where contract_id = @contract_id

END



GO
GRANT EXECUTE ON  [dbo].[cbmsSpot_GetForContract] TO [CBMSApplication]
GO
