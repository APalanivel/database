SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    Trade.Deal_Ticket_Current_Status_Sel_By_Deal_Ticket_Id
   
    
DESCRIPTION:   
   
  This procedure is to get deal ticket current status
    
INPUT PARAMETERS:    
      Name          DataType       Default        Description    
-----------------------------------------------------------------------------    
	@Deal_Ticket_Id   INT
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec Trade.Deal_Ticket_Current_Status_Sel_By_Deal_Ticket_Id  195
	Exec Trade.Deal_Ticket_Current_Status_Sel_By_Deal_Ticket_Id  198
	Exec Trade.Deal_Ticket_Current_Status_Sel_By_Deal_Ticket_Id  265
	Exec Trade.Deal_Ticket_Current_Status_Sel_By_Deal_Ticket_Id  291
	Exec Trade.Deal_Ticket_Current_Status_Sel_By_Deal_Ticket_Id  131861
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	RR			Raghu Reddy
    
MODIFICATIONS     
	Initials    Date        Modification      
-------------------------------------------------------------------------------------------------       
	RR          2019-01-04  Global Risk Management - Created to get deal ticket current status
     
    
******/

CREATE PROCEDURE [Trade].[Deal_Ticket_Current_Status_Sel_By_Deal_Ticket_Id] ( @Deal_Ticket_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;

      DECLARE
            @Status VARCHAR(200) = ''
           ,@Site_Count VARCHAR(200) = '';

                  
      SELECT
            @Site_Count = CAST(COUNT(DISTINCT chws.Deal_Ticket_Client_Hier_Id) AS VARCHAR(20))
      FROM
            Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
      WHERE
            chws.Is_Active = 1
            AND EXISTS ( SELECT
                              1
                         FROM
                              Trade.Deal_Ticket_Client_Hier dtch
                         WHERE
                              dtch.Deal_Ticket_Id = @Deal_Ticket_Id
                              AND dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id );



      SELECT
            @Deal_Ticket_Id AS Deal_Ticket_Id
           ,ws.Workflow_Status_Name AS Deal_Ticket_Status
           ,CAST(COUNT(DISTINCT dtch.Deal_Ticket_Client_Hier_Id) AS VARCHAR(20)) AS Status_Site_Count
           ,@Site_Count AS Site_Count
           ,ISNULL(cwsm.Workflow_Status_Message, wsm.Workflow_Status_Default_Message) AS Workflow_Status_Default_Message
           ,chws.Workflow_Status_Map_Id
      FROM
            Trade.Deal_Ticket_Client_Hier dtch
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                  ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
            INNER JOIN Trade.Workflow_Status_Map wsm
                  ON chws.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
            INNER JOIN Trade.Workflow_Status ws
                  ON wsm.Workflow_Status_Id = ws.Workflow_Status_Id
            INNER JOIN Trade.Deal_Ticket dt
                  ON dtch.Deal_Ticket_Id = dt.Deal_Ticket_Id
            LEFT JOIN dbo.RM_Client_Workflow_Status_Message_Map cwsm
                  ON dt.Client_Id = cwsm.Client_Id
                     AND chws.Workflow_Status_Map_Id = cwsm.Workflow_Status_Map_Id
      WHERE
            dtch.Deal_Ticket_Id = @Deal_Ticket_Id
            AND chws.Is_Active = 1
      GROUP BY
            ws.Workflow_Status_Name
           ,ISNULL(cwsm.Workflow_Status_Message, wsm.Workflow_Status_Default_Message)
           ,chws.Workflow_Status_Map_Id;


    

END;






GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Current_Status_Sel_By_Deal_Ticket_Id] TO [CBMSApplication]
GO
