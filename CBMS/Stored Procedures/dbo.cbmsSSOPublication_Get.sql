SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsSSOPublication_Get

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@sso_publication_id	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE       PROCEDURE [dbo].[cbmsSSOPublication_Get]
	( @MyAccountId int
	, @sso_publication_id int
	)
AS
BEGIN

	   select p.sso_publication_id
		, p.publication_title
		, p.publication_description
		, pc.entity_name publication_category_type
		, p.publication_category_type_id
		, p.publication_date
		, p.full_text
		, p.cbms_image_id
		, img.cbms_doc_id
	     from sso_publications p
	     join entity pc on pc.entity_id = p.publication_category_type_id
left outer   join cbms_image img on p.cbms_image_id = img.cbms_image_id
	where sso_publication_id = @sso_publication_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSSOPublication_Get] TO [CBMSApplication]
GO
