SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsRMInsight_Delete

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@RMInsightId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE      procedure [dbo].[cbmsRMInsight_Delete]
	( @MyAccountId int
	, @RMInsightId int
	)
AS
BEGIN

	set nocount on

	delete rm_risk_insight where rm_risk_insight_id = @RMInsightId

END
GO
GRANT EXECUTE ON  [dbo].[cbmsRMInsight_Delete] TO [CBMSApplication]
GO
