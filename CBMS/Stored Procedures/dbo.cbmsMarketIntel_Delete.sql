SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsMarketIntel_Delete

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@MarketIntelId 	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE       procedure [dbo].[cbmsMarketIntel_Delete]
	( @MyAccountId int
	, @MarketIntelId int
	)
AS
BEGIN

set nocount on

	declare @ThisId int

	select @ThisId = market_intel_id
	from market_intel
	where market_intel_id = @MarketIntelId

if @ThisId is not null
begin
	
	delete market_intel_state_map
	where market_intel_id = @MarketIntelId
	
	delete market_intel_commodity_map
	where market_intel_id = @MarketIntelId

	delete market_intel 
	where market_intel_id = @MarketIntelId

end

--set nocount off	

END
GO
GRANT EXECUTE ON  [dbo].[cbmsMarketIntel_Delete] TO [CBMSApplication]
GO
