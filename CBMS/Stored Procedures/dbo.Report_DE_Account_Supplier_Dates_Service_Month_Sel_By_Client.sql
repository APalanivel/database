SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********           
    
NAME:    
     dbo.Report_DE_Account_Supplier_Dates_Service_Month_Sel_By_Client    
           
DESCRIPTION:              
       
          
 INPUT PARAMETERS:              
                         
 Name                        DataType         Default       Description            
---------------------------------------------------------------------------------------------------------------     
 @Client_Name                NVARCHAR(200)           
 @Start_Dt                   DATE     
 @End_Dt                     DATE       
              
 OUTPUT PARAMETERS:              
                               
 Name                        DataType         Default       Description            
---------------------------------------------------------------------------------------------------------------          
              
 USAGE EXAMPLES:                                
---------------------------------------------------------------------------------------------------------------                                
     
 EXEC dbo.Report_DE_Account_Supplier_Dates_Service_Month_Sel_By_Client 'city of boston','2012-05-01','2013-06-01'    
     
      
 AUTHOR INITIALS:            
           
 Initials              Name            
---------------------------------------------------------------------------------------------------------------                          
 AKR                   Ashok Kumar Raju            
    
      
 MODIFICATIONS:          
              
 Initials              Date             Modification          
---------------------------------------------------------------------------------------------------------------          
 AKR                   2014-06-01      Created.    
*********/    
CREATE PROCEDURE dbo.Report_DE_Account_Supplier_Dates_Service_Month_Sel_By_Client  
      (   
       @Client_Name NVARCHAR(200)  
      ,@Start_Dt DATE  
      ,@End_Dt DATE )  
AS   
BEGIN    
      SET NOCOUNT ON    
        
      DECLARE @Client_Id INT  
        
      SELECT @Client_Id = c.CLIENT_ID FROM dbo.CLIENT c WHERE c.CLIENT_NAME = @Client_Name  
          
      SELECT  
            Client_Name [Client]  
           ,ca.Account_Type [Account Type]  
           ,dd.FIRST_DAY_OF_MONTH_D [Service Month]  
           ,site_name [Site]  
           ,ca.display_account_number [Display Account Number]  
           ,ca.Account_Number [Account Number]  
           ,ca.Supplier_Account_begin_Dt [SAStDate]  
           ,ca.Supplier_Account_End_Dt [SAEndDate]  
           ,com.Commodity_Name [Commodity]  
      FROM  
            Core.Client_Hier_Account ca  
            JOIN Core.Client_Hier ch  
                  ON ch.Client_Hier_Id = ca.Client_Hier_Id  
            JOIN Commodity com  
                  ON com.Commodity_Id = ca.Commodity_Id  
            CROSS JOIN META.DATE_DIM dd  
      WHERE  
            ch.Client_Id = @Client_Id  
            AND dd.FIRST_DAY_OF_MONTH_D BETWEEN @Start_Dt  
                                        AND     @End_Dt  
      GROUP BY  
            Client_Name  
           ,ca.Account_Type  
           ,dd.FIRST_DAY_OF_MONTH_D  
           ,site_name  
           ,ca.display_account_number  
           ,ca.Account_Number  
           ,ca.Supplier_Account_begin_Dt  
           ,ca.Supplier_Account_End_Dt  
           ,com.Commodity_Name  
      ORDER BY  
            Site_name  
           ,Account_Type  
           ,Account_Number    
END    
    
    

;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Account_Supplier_Dates_Service_Month_Sel_By_Client] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_Account_Supplier_Dates_Service_Month_Sel_By_Client] TO [CBMSReports]
GO
