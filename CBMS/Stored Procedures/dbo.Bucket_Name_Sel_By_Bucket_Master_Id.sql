SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
	dbo.Bucket_Name_Sel_By_Bucket_Master_Id

DESCRIPTION:


INPUT PARAMETERS:
	Name				DataType		Default	Description
-------------------------------------------------------------------------------------------
	@Bucket_Master_Id		int  					
	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------
	EXEC dbo.Bucket_Name_Sel_By_Bucket_Master_Id 101697
	

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	AC			Ajay Chejarla

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	AC        	6/20/2013	Created

*/

CREATE PROCEDURE [dbo].[Bucket_Name_Sel_By_Bucket_Master_Id]
      @Bucket_Master_Id INT
AS 
BEGIN

      SET NOCOUNT ON

      SELECT
            bm.Bucket_Name AS BucketName
      FROM
            dbo.Bucket_Master bm
      WHERE
            bm.Bucket_Master_Id = @Bucket_Master_Id
 
END

;
GO
GRANT EXECUTE ON  [dbo].[Bucket_Name_Sel_By_Bucket_Master_Id] TO [CBMSApplication]
GO
