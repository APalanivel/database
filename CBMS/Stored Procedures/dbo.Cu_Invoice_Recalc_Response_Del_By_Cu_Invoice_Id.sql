SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
  dbo.Cu_Invoice_Recalc_Response_Del_By_Cu_Invoice_Id
  
DESCRIPTION:  
  
  
INPUT PARAMETERS:  
 Name								DataType				Default		Description  
-------------------------------------------------------------------------------------  

 @Cu_Invoice_Id		INT
   
  

OUTPUT PARAMETERS:  
 Name								DataType				Default		Description  
-------------------------------------------------------------------------------------  
  
USAGE EXAMPLES:  
-------------------------------------------------------------------------------------  
 BEGIN Transaction
 
 EXEC Cu_Invoice_Recalc_Response_Del_By_Cu_Invoice_Id 60863784
 
 
 ROLLBACK Transaction 
  
  
AUTHOR INITIALS:  
 Initials	Name  
-------------------------------------------------------------------------------------  
 RKV		Ravi Kumar Vegesna  
 NR			Narayaan Reddy
   
MODIFICATIONS  
  
 Initials		Date			Modification  
-------------------------------------------------------------------------------------  
 RKV			2016-02-05		Created for AS400-PII
 NR				2018-03-26		Data Interval - Removed Records from  Cu_Invoice_Recalc_Determinant when Invoice Back out.
            
              
******/  
CREATE PROCEDURE [dbo].[Cu_Invoice_Recalc_Response_Del_By_Cu_Invoice_Id] ( @Cu_Invoice_Id INT )
AS 
BEGIN  
      SET NOCOUNT ON;  
     
      BEGIN TRY
            BEGIN TRANSACTION   
            
   
     
            DELETE
                  cirr
            FROM
                  dbo.Cu_Invoice_Recalc_Response cirr
                  INNER JOIN dbo.RECALC_HEADER rh
                        ON rh.RECALC_HEADER_ID = cirr.Recalc_Header_Id
            WHERE
                  rh.CU_INVOICE_ID = @Cu_Invoice_Id
                  
            DELETE
                  cird
            FROM
                  dbo.Cu_Invoice_Recalc_Determinant cird
                  INNER JOIN dbo.RECALC_HEADER rh
                        ON cird.Recalc_Header_Id = rh.RECALC_HEADER_ID
            WHERE
                  rh.CU_INVOICE_ID = @Cu_Invoice_Id
      
                
            DELETE
                  rh
            FROM
                  dbo.RECALC_HEADER rh
            WHERE
                  rh.CU_INVOICE_ID = @Cu_Invoice_Id
                  AND NOT EXISTS ( SELECT
                                    1
                                   FROM
                                    dbo.Cu_Invoice_Recalc_Response cirr
                                   WHERE
                                    rh.RECALC_HEADER_ID = cirr.Recalc_Header_Id )
                  AND NOT EXISTS ( SELECT
                                    1
                                   FROM
                                    dbo.Cu_Invoice_Recalc_Determinant cid
                                   WHERE
                                    rh.RECALC_HEADER_ID = cid.Recalc_Header_Id )
        
      
            COMMIT TRANSACTION
      END TRY

      BEGIN CATCH
            IF @@TRANCOUNT > 0 
                  ROLLBACK TRANSACTION
            EXEC dbo.usp_RethrowError

      END CATCH			
END;

GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Recalc_Response_Del_By_Cu_Invoice_Id] TO [CBMSApplication]
GO
