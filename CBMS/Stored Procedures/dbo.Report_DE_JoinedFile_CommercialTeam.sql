SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********                
NAME:                
  dbo.Report_DE_JoinedFile_CommercialTeam        
          
DESCRIPTION:           
 Client Info, Accounts, Meters, Contract, Utility , Rate data        
          
          
INPUT PARAMETERS:              
      Name              DataType          Default     Description              
------------------------------------------------------------              
 region_name        
        
        
OUTPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------         
        
        
USAGE EXAMPLES:        
------------------------------------------------------------        
Stress /Prod        
exec Report_DE_JoinedFile_CommercialTeam NULL       
exec Report_DE_JoinedFile_CommercialTeam 'cp'        
        
        
AUTHOR INITIALS:        
         
Initials	Name        
------------------------------------------------------------        
LC			Lynn Cox        
RR			Raghu Reddy      
AJ			Ajay Chejarla 
PS			Patchava Srinivasarao      
        
Initials	Date		Modification         
------------------------------------------------------------           
lec			Adding		BJs Restaurants and Eddie Bauer        
						11671    
						11674    
RR			2012-06-07	Cloned from LEC Objects. Replaced base tables with client hier tables    
AJ			12/17/2012	Added two columns Utility Account ID, Supplier Account ID to the select list    
AKR			2013-01-28	Added the Analyst Name and Broker Fee columns    
AKR			2013-02-01	Added indexes to Temp tables.    
HG			2013-05-02	Added INNER JOIN with code table to filter only the Cost & usages services mapped to the client in Core.Client_Commodity    
AKR			2013-09-04	Added the Sourcing Analyst Mappings for Client, Account and Site
HG			2017-03-10	Added new columns
						SupplierAccountStartDate
						SupplierAccountEndDate when null will be shown as Unspecified and date is casted to varchar as to show Unspecified when null.
						SupplierAccountCreatedBy - show username similar to the existing column "ContractCreatedBy" in the Joined File today.
						SupplierAccountCreatedDate  - show date and timestamp similar to the existing column "AccountCreated" in the Joined File today.
LEC			2018-02-14  Added new column Contract Product Type
PS		    2018-08-22  Added new Meter_Id column
lec			2020-01-27	Modified to left outer join for supplier acct service level. No longer required field as of 11/19 enhancement.  Supplier accts were not showing up in JF.
*/
CREATE PROCEDURE [dbo].[Report_DE_JoinedFile_CommercialTeam]
      ( 
       @region_name VARCHAR(10) = NULL )
AS 
BEGIN

      SET NOCOUNT ON;

      CREATE TABLE #UtilityAccts_CTE
            ( 
             Account_Number VARCHAR(50)
            ,Account_Id INT
            ,UtilityAccountServiceLevel VARCHAR(200)
            ,AccountCreated DATETIME
            ,AccountInvoiceNotExpected VARCHAR(10)
            ,AccountNotManaged VARCHAR(10)
            ,UtilityAccountInvoiceSource VARCHAR(200)
            ,Utility VARCHAR(200)
            ,CommodityType VARCHAR(50)
            ,Rate VARCHAR(200)
            ,PurchaseMethod VARCHAR(50)
            ,Site_Id INT
            ,Client_Hier_Id INT
            ,ADDRESS_ID INT
            ,MeterNumber VARCHAR(50)
            ,Meter_Id INT
            ,AccountCreatedBy VARCHAR(30)
            ,Consumption_Level_Desc VARCHAR(100)
            ,IsDataEntryOnly VARCHAR(10)
            ,Is_Broker_Account VARCHAR(5)
            ,Broker_fee DECIMAL(28, 12)
            ,BrokerFee_Currency_Unit VARCHAR(10)
            ,BrokerFee_Uom_Unit VARCHAR(25)
            ,Analyst_Id INT
            ,Account_Analyst_Mapping_Cd INT
            ,Site_Analyst_Mapping_Cd INT
            ,Client_Analyst_Mapping_Cd INT )                
       
       
                 
      CREATE TABLE #Contracts_CTE
            ( 
             ContractId INT
            ,Account_Id INT
            ,BaseContractId INT
            ,ContractNumber VARCHAR(150)
            ,ContractPricingStatus VARCHAR(10)
            ,ContractType VARCHAR(200)
            ,ContractedVendor VARCHAR(200)
            ,ContractStartDate DATETIME
            ,ContractEndDate DATETIME
            ,NotificationDays DECIMAL(12, 2)
            ,NotificationDate DATETIME
            ,ContractTriggerRights VARCHAR(10)
            ,FullRequirements VARCHAR(10)
            ,CONTRACT_PRICING_SUMMARY VARCHAR(1000)
            ,ContractComments VARCHAR(4000)
            ,Currency VARCHAR(200)
            ,RenewalType VARCHAR(200)
            ,SupplierAccountNumber VARCHAR(50)
            ,SupplierAccountServiceLevel VARCHAR(200)
            ,SupplierAccountInvoiceSource VARCHAR(200)
            ,ContractCreatedBy VARCHAR(30)
            ,ContractCreatedDate DATETIME
            ,ContractRecalcType VARCHAR(30)
            ,Meter_Id INT
            ,SupplierAccountStartDate DATETIME
            ,SupplierAccountEndDate DATETIME
            ,SupplierAccountCreatedBy VARCHAR(30)
            ,SupplierAccountCreatedDate DATETIME
            ,ContractProductType VARCHAR(200) )            
    
      DECLARE
            @Supplier_Contract_Type_Id INT
           ,@Utility_Contract_Type_Id INT
           ,@Contract_End_Date DATE = '2008-12-31'
           ,@Account_Table INT
           ,@Contract_Table INT
           ,@Site_Table INT
           ,@Client_Id INT
           ,@Default_Analyst INT
           ,@Custom_Analyst INT    
    
           
      SELECT
            @Default_Analyst = MAX(CASE WHEN c.Code_Value = 'Default' THEN c.Code_Id
                                   END)
           ,@Custom_Analyst = MAX(CASE WHEN c.Code_Value = 'Custom' THEN c.Code_Id
                                  END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Analyst Type'    
    
                       
                       
      SELECT
            @Supplier_Contract_Type_Id = MAX(CASE WHEN ENTITY_NAME = 'Supplier' THEN ENTITY_ID
                                             END)
           ,@Utility_Contract_Type_Id = MAX(CASE WHEN ENTITY_NAME = 'Utility' THEN ENTITY_ID
                                            END)
           ,@Account_Table = MAX(CASE WHEN ENTITY_NAME = 'Account_Table' THEN ENTITY_ID
                                 END)
           ,@Contract_Table = MAX(CASE WHEN ENTITY_NAME = 'CONTRACT_TABLE' THEN ENTITY_ID
                                  END)
           ,@Site_Table = MAX(CASE WHEN ENTITY_NAME = 'Site_Table' THEN ENTITY_ID
                              END)
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_DESCRIPTION IN ( 'Table_Type', 'Table Type', 'Contract Type', 'Client Type' );            
            
--Utility Accounts            
      INSERT      INTO #UtilityAccts_CTE
                  ( 
                   Account_Number
                  ,Account_Id
                  ,UtilityAccountServiceLevel
                  ,AccountCreated
                  ,AccountInvoiceNotExpected
                  ,AccountNotManaged
                  ,UtilityAccountInvoiceSource
                  ,Utility
                  ,CommodityType
                  ,Rate
                  ,PurchaseMethod
                  ,Site_Id
                  ,Client_Hier_Id
                  ,ADDRESS_ID
                  ,MeterNumber
                  ,Meter_Id
                  ,AccountCreatedBy
                  ,Consumption_Level_Desc
                  ,IsDataEntryOnly
                  ,Is_Broker_Account
                  ,Broker_fee
                  ,BrokerFee_Currency_Unit
                  ,BrokerFee_Uom_Unit
                  ,Analyst_Id
                  ,Account_Analyst_Mapping_Cd
                  ,Site_Analyst_Mapping_Cd
                  ,Client_Analyst_Mapping_Cd )
                  SELECT
                        cha.Account_Number Account_Number
                       ,cha.Account_Id Account_Id
                       ,svl.ENTITY_NAME UtilityAccountServiceLevel
                       ,ea.MODIFIED_DATE AccountCreated
                       ,AccountInvoiceNotExpected = CASE cha.Account_Not_Expected
                                                      WHEN 1 THEN 'Yes'
                                                      ELSE 'No'
                                                    END
                       ,AccountNotManaged = CASE cha.Account_Not_Managed
                                              WHEN 1 THEN 'Yes'
                                              ELSE 'No'
                                            END
                       ,istyp.ENTITY_NAME UtilityAccountInvoiceSource
                       ,cha.Account_Vendor_Name Utility
                       ,comm.Commodity_Name CommodityType
                       ,cha.Rate_Name Rate
                       ,'NotApplicable' PurchaseMethod
                       ,ch.Site_Id
                       ,cha.Client_Hier_Id
                       ,cha.Meter_Address_ID ADDRESS_ID
                       ,cha.Meter_Number MeterNumber
                       ,cha.Meter_Id
                       ,ui.USERNAME AccountCreatedBy
                       ,vcl.Consumption_Level_Desc
                       ,IsDataEntryOnly = CASE cha.Account_Is_Data_Entry_Only
                                            WHEN 1 THEN 'Yes'
                                            ELSE 'No'
                                          END
                       ,Is_Broker_Account = CASE cha.Account_Is_Broker
                                              WHEN 1 THEN 'Yes'
                                              ELSE 'No'
                                            END
                       ,acbf.Broker_Fee
                       ,ecur.CURRENCY_UNIT_NAME
                       ,euom.ENTITY_NAME
                       ,CASE WHEN COALESCE(cha.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) = @Default_Analyst THEN vcam.Analyst_Id
                             WHEN COALESCE(cha.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) = @Custom_Analyst THEN COALESCE(aca.Analyst_User_Info_Id, sca.Analyst_User_Info_Id, cca.Analyst_User_Info_Id)
                        END AS Analyst_Id
                       ,COALESCE(cha.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) Analyst_Mapping_Cd
                       ,COALESCE(ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) Site_Analyst_Mapping_Cd
                       ,ch.Client_Analyst_Mapping_Cd Client_Analyst_Mapping_Cd
                  FROM
                        Core.Client_Hier_Account cha
                        INNER JOIN dbo.ENTITY svl
                              ON svl.ENTITY_ID = cha.Account_Service_level_Cd
                        INNER JOIN Core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.ENTITY_AUDIT ea
                              ON ea.ENTITY_IDENTIFIER = cha.Account_Id
                                 AND ea.ENTITY_ID = @Account_Table
                                 AND ea.AUDIT_TYPE = 1
                        LEFT OUTER JOIN dbo.ENTITY istyp
                              ON istyp.ENTITY_ID = cha.Account_Invoice_Source_Cd
                        INNER JOIN dbo.Commodity comm
                              ON comm.Commodity_Id = cha.Commodity_Id
                        INNER JOIN dbo.USER_INFO ui
                              ON ui.USER_INFO_ID = ea.USER_INFO_ID
                        INNER JOIN Account_Variance_Consumption_Level av
                              ON av.ACCOUNT_ID = cha.Account_Id
                        INNER JOIN Variance_Consumption_Level vcl
                              ON vcl.Variance_Consumption_Level_Id = av.Variance_Consumption_Level_Id
                                 AND vcl.Commodity_Id = cha.Commodity_Id
                        LEFT OUTER JOIN Account_Commodity_Broker_Fee acbf
                              ON cha.Account_Id = acbf.Account_Id
                                 AND cha.Commodity_Id = acbf.Commodity_Id
                        LEFT OUTER JOIN dbo.CURRENCY_UNIT ecur
                              ON ecur.CURRENCY_UNIT_ID = acbf.Currency_Unit_Id
                        LEFT OUTER JOIN dbo.ENTITY euom
                              ON euom.ENTITY_ID = acbf.UOM_Entity_Id
                        LEFT OUTER JOIN dbo.VENDOR_COMMODITY_MAP vcm
                              ON cha.Account_Vendor_Id = vcm.VENDOR_ID
                                 AND cha.Commodity_Id = vcm.COMMODITY_TYPE_ID
                        LEFT OUTER JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                              ON vcm.VENDOR_COMMODITY_MAP_ID = vcam.Vendor_Commodity_Map_Id
                        LEFT OUTER JOIN ( Core.Client_Commodity ccc
                                          INNER JOIN dbo.Code cd
                                                ON cd.Code_Id = ccc.Commodity_Service_Cd
                                                   AND cd.Code_Value = 'Invoice' )
                                          ON ccc.Client_Id = ch.Client_Id
                                             AND ccc.Commodity_Id = cha.Commodity_Id
                        LEFT OUTER JOIN dbo.Account_Commodity_Analyst aca
                              ON aca.Account_Id = cha.Account_Id
                                 AND aca.Commodity_Id = cha.Commodity_Id
                        LEFT OUTER JOIN dbo.Site_Commodity_Analyst sca
                              ON sca.Site_Id = ch.Site_Id
                                 AND sca.Commodity_Id = cha.Commodity_Id
                        LEFT OUTER JOIN dbo.Client_Commodity_Analyst cca
                              ON ccc.Client_Commodity_Id = cca.Client_Commodity_Id
                  WHERE
                        cha.Account_Type = 'Utility'
                        AND ch.Client_Id IN ( 11174, 10065, 11429, 226, 11415, 11419, 218, 11431, 10046, 11073, 10092, 11265, 11292, 10066, 10032, 147, 10081, 10055, 11404, 11434, 11459, 11466, 11468, 11448, 11485, 11487, 11467, 11519, 11544, 11547, 11550, 11586, 11557, 11565, 11587, 11598, 11597, 11612, 1034, 11622, 11671, 11674, 11691, 11694, 11697, 11688, 11586, 11702, 11668, 11687, 11711, 11611, 11732, 11706 )    
    
      CREATE CLUSTERED INDEX [ix_#UtilityAccts_CTE__Site_Id__Address_ID] ON [#UtilityAccts_CTE] ([Site_Id],[ADDRESS_ID])      
    
--Contracts              
      INSERT      INTO #Contracts_CTE
                  ( 
                   ContractId
                  ,Account_Id
                  ,BaseContractId
                  ,ContractNumber
                  ,ContractPricingStatus
                  ,ContractType
                  ,ContractedVendor
                  ,ContractStartDate
                  ,ContractEndDate
                  ,NotificationDays
                  ,NotificationDate
                  ,ContractTriggerRights
                  ,FullRequirements
                  ,CONTRACT_PRICING_SUMMARY
                  ,ContractComments
                  ,Currency
                  ,RenewalType
                  ,SupplierAccountNumber
                  ,SupplierAccountServiceLevel
                  ,SupplierAccountInvoiceSource
                  ,ContractCreatedBy
                  ,ContractCreatedDate
                  ,ContractRecalcType
                  ,Meter_Id
                  ,SupplierAccountStartDate
                  ,SupplierAccountEndDate
                  ,SupplierAccountCreatedBy
                  ,SupplierAccountCreatedDate
                  ,ContractProductType )
                  SELECT
                        c.CONTRACT_ID ContractId
                       ,cha.Account_Id Account_Id
                       ,c.BASE_CONTRACT_ID BaseContractId
                       ,c.ED_CONTRACT_NUMBER ContractNumber
                       ,ContractPricingStatus = CASE WHEN lps.CONTRACT_ID IS NULL THEN 'NotBuilt'
                                                     ELSE 'Built'
                                                END
                       ,e.ENTITY_NAME ContractType
                       ,cha.Account_Vendor_Name ContractedVendor
                       ,c.CONTRACT_START_DATE ContractStartDate
                       ,c.CONTRACT_END_DATE ContractEndDate
                       ,c.NOTIFICATION_DAYS NotificationDays
                       ,NotificationDate = CONTRACT_END_DATE - ( NOTIFICATION_DAYS + 15 )
                       ,ContractTriggerRights = CASE c.IS_CONTRACT_TRIGGER_RIGHTS
                                                  WHEN 1 THEN 'Yes'
                                                  ELSE 'No'
                                                END
                       ,FullRequirements = CASE c.IS_CONTRACT_FULL_REQUIREMENT
                                             WHEN 1 THEN 'Yes'
                                             ELSE 'No'
                                           END
                       ,c.CONTRACT_PRICING_SUMMARY
                       ,c.CONTRACT_COMMENTS ContractComments
                       ,cu.CURRENCY_UNIT_NAME Currency
                       ,e1.ENTITY_NAME RenewalType
                       ,cha.Account_Number SupplierAccountNumber
                       ,e2.ENTITY_NAME SupplierAccountServiceLevel
                       ,e3.ENTITY_NAME SupplierAccountInvoiceSource
                       ,ui.USERNAME ContractCreatedBy
                       ,ea.MODIFIED_DATE ContractCreatedDate
                       ,e4.ENTITY_NAME ContractRecalcType
                       ,cha.Meter_Id
                       ,cha.Supplier_Account_begin_Dt AS SupplierAccountStartDate
                       ,cha.Supplier_Account_End_Dt AS SupplierAccountEndDate
                       ,ui1.USERNAME SupplierAccountCreatedBy
                       ,ea1.MODIFIED_DATE SupplierAccountCreatedDate
                       ,prodtype.code_value ContractProductType
                  FROM
                        dbo.CONTRACT c
                        INNER JOIN Core.Client_Hier_Account cha
                              ON c.CONTRACT_ID = cha.Supplier_Contract_ID
                        INNER JOIN Core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.ENTITY e
                              ON e.ENTITY_ID = c.CONTRACT_TYPE_ID
                        INNER JOIN dbo.CURRENCY_UNIT cu
                              ON cu.CURRENCY_UNIT_ID = c.CURRENCY_UNIT_ID
                        INNER JOIN dbo.ENTITY e1
                              ON e1.ENTITY_ID = c.RENEWAL_TYPE_ID
                        left outer JOIN dbo.ENTITY e2
                              ON e2.ENTITY_ID = cha.Account_Service_level_Cd
						--INNER JOIN dbo.ENTITY e2
      --                        ON e2.ENTITY_ID = cha.Account_Service_level_Cd
                        LEFT OUTER JOIN dbo.ENTITY e3
                              ON e3.ENTITY_ID = cha.Account_Invoice_Source_Cd
                        LEFT OUTER JOIN ENTITY e4
                              ON e4.ENTITY_ID = c.CONTRACT_RECALC_TYPE_ID
                        INNER JOIN dbo.ENTITY_AUDIT ea
                              ON ea.ENTITY_IDENTIFIER = c.CONTRACT_ID
                                 AND ea.ENTITY_ID = @Contract_Table --494            
                                 AND ea.AUDIT_TYPE = '1'
                        INNER JOIN dbo.USER_INFO ui
                              ON ui.USER_INFO_ID = ea.USER_INFO_ID
                        LEFT OUTER JOIN dbo.LOAD_PROFILE_SPECIFICATION lps
                              ON lps.CONTRACT_ID = c.CONTRACT_ID
                        LEFT JOIN dbo.ENTITY_AUDIT ea1
                              ON ea1.ENTITY_IDENTIFIER = cha.Account_Id
                                 AND ea1.ENTITY_ID = @Account_Table --494                      
                                 AND ea1.AUDIT_TYPE = 1
                        LEFT JOIN dbo.USER_INFO ui1
                              ON ui1.USER_INFO_ID = ea1.USER_INFO_ID
                        LEFT JOIN code prodtype
                              ON prodtype.code_id = c.Contract_Product_Type_Cd
                  WHERE
                        ( c.CONTRACT_TYPE_ID = @Utility_Contract_Type_Id
                          OR ( c.CONTRACT_TYPE_ID = @Supplier_Contract_Type_Id
                               AND c.CONTRACT_END_DATE > @Contract_End_Date ) )
                        AND cha.Account_Type = 'Supplier'
                        AND ch.Client_Id IN ( 11174, 10065, 11429, 226, 11415, 11419, 218, 11431, 10046, 11073, 10092, 11265, 11292, 10066, 10032, 147, 10081, 10055, 11404, 11434, 11459, 11466, 11468, 11448, 11485, 11487, 11467, 11519, 11544, 11547, 11550, 11586, 11557, 11565, 11587, 11598, 11597, 11612, 1034, 11622, 11671, 11674, 11691, 11694, 11697, 11688, 11586, 11702, 11668, 11687, 11711, 11611, 11732, 11706 )            
                            

      INSERT      INTO #Contracts_CTE
                  ( 
                   ContractId
                  ,Account_Id
                  ,BaseContractId
                  ,ContractNumber
                  ,ContractPricingStatus
                  ,ContractType
                  ,ContractedVendor
                  ,ContractStartDate
                  ,ContractEndDate
                  ,NotificationDays
                  ,NotificationDate
                  ,ContractTriggerRights
                  ,FullRequirements
                  ,CONTRACT_PRICING_SUMMARY
                  ,ContractComments
                  ,Currency
                  ,RenewalType
                  ,SupplierAccountNumber
                  ,SupplierAccountServiceLevel
                  ,SupplierAccountInvoiceSource
                  ,ContractCreatedBy
                  ,ContractCreatedDate
                  ,Meter_Id
                  ,SupplierAccountStartDate
                  ,SupplierAccountEndDate
                  ,SupplierAccountCreatedBy
                  ,SupplierAccountCreatedDate
                  ,ContractProductType )
                  SELECT
                        -1 ContractId
                       ,cha.Account_Id Account_Id
                       ,NULL BaseContractId
                       ,NULL ContractNumber
                       ,NULL ContractPricingStatus
                       ,NULL ContractType
                       ,NULL ContractedVendor
                       ,NULL ContractStartDate
                       ,NULL ContractEndDate
                       ,NULL NotificationDays
                       ,NULL NotificationDate
                       ,NULL ContractTriggerRights
                       ,NULL FullRequirements
                       ,NULL CONTRACT_PRICING_SUMMARY
                       ,NULL ContractComments
                       ,NULL Currency
                       ,NULL RenewalType
                       ,suppacc.ACCOUNT_NUMBER SupplierAccountNumber
                       ,e2.ENTITY_NAME SupplierAccountServiceLevel
                       ,e3.ENTITY_NAME SupplierAccountInvoiceSource
                       ,NULL ContractCreatedBy
                       ,NULL ContractCreatedDate
                       ,cha.Meter_Id
                       ,suppacc.Supplier_Account_Begin_Dt AS SupplierAccountStartDate
                       ,suppacc.Supplier_Account_End_Dt AS SupplierAccountEndDate
                       ,ui.USERNAME SupplierAccountCreatedBy
                       ,ea.MODIFIED_DATE SupplierAccountCreatedDate
                       ,NULL ContractProductType
                  FROM
                        dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                        INNER JOIN dbo.ACCOUNT suppacc
                              ON samm.ACCOUNT_ID = suppacc.ACCOUNT_ID
                        LEFT JOIN dbo.ENTITY_AUDIT ea
                              ON ea.ENTITY_IDENTIFIER = suppacc.ACCOUNT_ID
                                 AND ea.ENTITY_ID = @Account_Table --494                      
                                 AND ea.AUDIT_TYPE = 1
                        LEFT JOIN dbo.USER_INFO ui
                              ON ui.USER_INFO_ID = ea.USER_INFO_ID
						Left outer JOIN dbo.ENTITY e2
                              ON e2.ENTITY_ID = suppacc.SERVICE_LEVEL_TYPE_ID
                        --INNER JOIN dbo.ENTITY e2
                        --      ON e2.ENTITY_ID = suppacc.SERVICE_LEVEL_TYPE_ID
                        LEFT OUTER JOIN dbo.ENTITY e3
                              ON e3.ENTITY_ID = suppacc.INVOICE_SOURCE_TYPE_ID
                        INNER JOIN Core.Client_Hier_Account cha
                              ON samm.ACCOUNT_ID = cha.Account_Id
                        INNER JOIN Core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        samm.Contract_ID = -1
                        AND ch.Client_Id IN ( 11174, 10065, 11429, 226, 11415, 11419, 218, 11431, 10046, 11073, 10092, 11265, 11292, 10066, 10032, 147, 10081, 10055, 11404, 11434, 11459, 11466, 11468, 11448, 11485, 11487, 11467, 11519, 11544, 11547, 11550, 11586, 11557, 11565, 11587, 11598, 11597, 11612, 1034, 11622, 11671, 11674, 11691, 11694, 11697, 11688, 11586, 11702, 11668, 11687, 11711, 11611, 11732, 11706 )

      INSERT      INTO #Contracts_CTE
                  ( 
                   ContractId
                  ,Account_Id
                  ,BaseContractId
                  ,ContractNumber
                  ,ContractPricingStatus
                  ,ContractType
                  ,ContractedVendor
                  ,ContractStartDate
                  ,ContractEndDate
                  ,NotificationDays
                  ,NotificationDate
                  ,ContractTriggerRights
                  ,FullRequirements
                  ,CONTRACT_PRICING_SUMMARY
                  ,ContractComments
                  ,Currency
                  ,RenewalType
                  ,SupplierAccountNumber
                  ,SupplierAccountServiceLevel
                  ,SupplierAccountInvoiceSource
                  ,ContractCreatedBy
                  ,ContractCreatedDate
                  ,Meter_Id
                  ,SupplierAccountStartDate
                  ,SupplierAccountEndDate
                  ,SupplierAccountCreatedBy
                  ,SupplierAccountCreatedDate
                  ,ContractProductType )
                  SELECT
                        -1 ContractId
                       ,cha.Account_Id Account_Id
                       ,NULL BaseContractId
                       ,NULL ContractNumber
                       ,NULL ContractPricingStatus
                       ,NULL ContractType
                       ,NULL ContractedVendor
                       ,NULL ContractStartDate
                       ,NULL ContractEndDate
                       ,NULL NotificationDays
                       ,NULL NotificationDate
                       ,NULL ContractTriggerRights
                       ,NULL FullRequirements
                       ,NULL CONTRACT_PRICING_SUMMARY
                       ,NULL ContractComments
                       ,NULL Currency
                       ,NULL RenewalType
                       ,suppacc.ACCOUNT_NUMBER SupplierAccountNumber
                       ,e2.ENTITY_NAME SupplierAccountServiceLevel
                       ,e3.ENTITY_NAME SupplierAccountInvoiceSource
                       ,NULL ContractCreatedBy
                       ,NULL ContractCreatedDate
                       ,cha.Meter_Id
                       ,suppacc.Supplier_Account_Begin_Dt AS SupplierAccountStartDate
                       ,suppacc.Supplier_Account_End_Dt AS SupplierAccountEndDate
                       ,ui.USERNAME SupplierAccountCreatedBy
                       ,ea.MODIFIED_DATE SupplierAccountCreatedDate
                       ,NULL ContractProductType
                  FROM
                        dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                        INNER JOIN dbo.ACCOUNT suppacc
                              ON samm.ACCOUNT_ID = suppacc.ACCOUNT_ID
                        LEFT JOIN dbo.ENTITY_AUDIT ea
                              ON ea.ENTITY_IDENTIFIER = suppacc.ACCOUNT_ID
                                 AND ea.ENTITY_ID = @Account_Table --494                      
                                 AND ea.AUDIT_TYPE = 1
                        LEFT JOIN dbo.USER_INFO ui
                              ON ui.USER_INFO_ID = ea.USER_INFO_ID
                        left outer JOIN dbo.ENTITY e2
                              ON e2.ENTITY_ID = suppacc.SERVICE_LEVEL_TYPE_ID
						--INNER JOIN dbo.ENTITY e2
      --                        ON e2.ENTITY_ID = suppacc.SERVICE_LEVEL_TYPE_ID
                        LEFT OUTER JOIN dbo.ENTITY e3
                              ON e3.ENTITY_ID = suppacc.INVOICE_SOURCE_TYPE_ID
                        INNER JOIN Core.Client_Hier_Account cha
                              ON samm.ACCOUNT_ID = cha.Account_Id
                        INNER JOIN Core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        samm.Contract_ID = -1
                        AND ch.Client_Id IN ( 11174, 10065, 11429, 226, 11415, 11419, 218, 11431, 10046, 11073, 10092, 11265, 11292, 10066, 10032, 147, 10081, 10055, 11404, 11434, 11459, 11466, 11468, 11448, 11485, 11487, 11467, 11519, 11544, 11547, 11550, 11586, 11557, 11565, 11587, 11598, 11597, 11612, 1034, 11622, 11671, 11674, 11691, 11694, 11697, 11688, 11586, 11702, 11668, 11687, 11711, 11611, 11732, 11706 )

      CREATE CLUSTERED INDEX [ix_#Contracts_CTE__Meter_Id] ON [#Contracts_CTE] ([Meter_Id])

       --SiteList      
      SELECT
            ch.Client_Name [ClientName]
           ,ch.Client_Id [ClientID]
           ,ClientNotManaged = CASE ch.Client_Not_Managed
                                 WHEN 1 THEN 'Yes'
                                 ELSE 'No'
                               END
           ,ch.Sitegroup_Name [DivisionName]
           ,ch.Sitegroup_Id [DivisionID]
           ,DivisionNotManaged = CASE ch.Site_Not_Managed
                                   WHEN 1 THEN 'Yes'
                                   ELSE 'No'
                                 END
           ,ch.Site_name [SiteName]
           ,ch.Site_Id [SiteID]
           ,ea.MODIFIED_DATE SiteCreated
           ,s.SITE_REFERENCE_NUMBER [SiteRef.]
           ,rg.GROUP_NAME SiteRMGroup
           ,SiteNotManaged = CASE ch.Site_Not_Managed
                               WHEN 1 THEN 'Yes'
                               ELSE 'No'
                             END
           ,SiteClosed = CASE ch.Site_Closed
                           WHEN 1 THEN 'Yes'
                           ELSE 'No'
                         END
           ,ch.Site_Type_Name SiteType
           ,addr.ADDRESS_LINE1 [AddressLine1]
           ,addr.ADDRESS_LINE2 [AddressLine2]
           ,addr.CITY City
           ,st.STATE_NAME [State_Province]
           ,addr.ZIPCODE [ZipCode]
           ,Ctry.COUNTRY_NAME [Country]
           ,reg.REGION_NAME [Region]
           ,uacte.Account_Number [AccountNumber]
           ,uacte.UtilityAccountServiceLevel
           ,uacte.AccountCreated
           ,uacte.AccountInvoiceNotExpected
           ,uacte.AccountNotManaged
           ,uacte.UtilityAccountInvoiceSource
           ,uacte.Utility
           ,uacte.CommodityType
           ,uacte.Rate
           ,uacte.MeterNumber
           ,uacte.PurchaseMethod
           ,cc.ContractId
           ,BaseContractId
           ,cc.ContractNumber
           ,cc.ContractPricingStatus
           ,cc.ContractType
           ,cc.ContractedVendor
           ,cc.ContractStartDate
           ,cc.ContractEndDate
           ,cc.NotificationDays
           ,cc.NotificationDate
           ,cc.ContractTriggerRights
           ,cc.FullRequirements
           ,cc.CONTRACT_PRICING_SUMMARY
           ,cc.ContractComments
           ,cc.Currency
           ,cc.RenewalType
           ,cc.SupplierAccountNumber
           ,cc.SupplierAccountServiceLevel
           ,cc.SupplierAccountInvoiceSource
           ,cc.ContractCreatedBy
           ,cc.ContractCreatedDate
           ,cc.ContractRecalcType
           ,uacte.AccountCreatedBy
           ,uacte.Consumption_Level_Desc
           ,uacte.IsDataEntryOnly
           ,uacte.Account_Id [Utility Account ID]
           ,cc.Account_Id [Supplier Account ID]
           ,uacte.Is_Broker_Account [Broker Account]
           ,CAST(uacte.Broker_fee AS VARCHAR) + ' ' + uacte.BrokerFee_Currency_Unit + '/' + uacte.BrokerFee_Uom_Unit AS [Broker Fee]
           ,uia.FIRST_NAME + SPACE(1) + uia.LAST_NAME [Sourcing Analyst]
           ,cacc.Code_Value [Client Sourcing Mapping]
           ,sacc.Code_Value [Site Sourcing Mapping]
           ,aacc.Code_Value [Account Sourcing Mapping]
           ,cc.SupplierAccountStartDate
           ,CASE WHEN cc.ContractId = -1
                      AND cc.SupplierAccountEndDate IS NULL THEN 'Unspecified'
                 ELSE CONVERT(VARCHAR(10), cc.SupplierAccountEndDate, 101)
            END AS SupplierAccountEndDate
           ,cc.SupplierAccountCreatedBy
           ,cc.SupplierAccountCreatedDate
           ,cc.ContractProductType
           ,uacte.Meter_Id AS Meter_Id
      FROM
            Core.Client_Hier ch
            JOIN dbo.SITE s
                  ON ch.Site_Id = s.SITE_ID
            JOIN dbo.ADDRESS addr
                  ON addr.ADDRESS_PARENT_ID = ch.Site_Id
            JOIN dbo.STATE st
                  ON st.STATE_ID = addr.STATE_ID
            JOIN dbo.COUNTRY Ctry
                  ON Ctry.COUNTRY_ID = st.COUNTRY_ID
            JOIN dbo.REGION reg
                  ON reg.REGION_ID = st.REGION_ID
            JOIN dbo.ENTITY_AUDIT ea
                  ON ea.ENTITY_IDENTIFIER = ch.Site_Id
                     AND ea.ENTITY_ID = @Site_Table-- 506            
                     AND ea.AUDIT_TYPE = 1
            LEFT OUTER JOIN dbo.RM_GROUP_SITE_MAP rgm
                  ON rgm.SITE_ID = ch.Site_Id
            LEFT OUTER JOIN dbo.RM_GROUP rg
                  ON rg.RM_GROUP_ID = rgm.RM_GROUP_ID
            LEFT OUTER JOIN #UtilityAccts_CTE uacte
                  ON uacte.Site_Id = ch.Site_Id
                     AND uacte.ADDRESS_ID = addr.ADDRESS_ID
            LEFT JOIN Code cacc
                  ON cacc.Code_Id = uacte.Client_Analyst_Mapping_Cd
            LEFT JOIN Code sacc
                  ON sacc.Code_Id = uacte.Site_Analyst_Mapping_Cd
            LEFT JOIN Code aacc
                  ON aacc.Code_Id = uacte.Account_Analyst_Mapping_Cd
            LEFT OUTER JOIN #Contracts_CTE cc
                  ON cc.Meter_Id = uacte.Meter_Id
            LEFT OUTER JOIN dbo.USER_INFO uia
                  ON uacte.Analyst_Id = uia.USER_INFO_ID
      WHERE
            ( reg.REGION_NAME = @region_name
              OR @region_name IS NULL )
            AND ch.Client_Id IN ( 11174, 10065, 11429, 226, 11415, 11419, 218, 11431, 10046, 11073, 10092, 11265, 11292, 10066, 10032, 147, 10081, 10055, 11404, 11434, 11459, 11466, 11468, 11448, 11485, 11487, 11467, 11519, 11544, 11547, 11550, 11586, 11557, 11565, 11587, 11598, 11597, 11612, 1034, 11622, 11671, 11674, 11691, 11694, 11697, 11688, 11586, 11702, 11668, 11687, 11711, 11611, 11732, 11706 )
            AND ch.Site_Not_Managed != 1
      GROUP BY
            ch.Client_Name
           ,ch.Client_Id
           ,ch.Client_Not_Managed
           ,ch.Sitegroup_Name
           ,ch.Sitegroup_Id
           ,ch.Division_Not_Managed
           ,ch.Site_name
           ,ch.Site_Id
           ,ea.MODIFIED_DATE
           ,s.SITE_REFERENCE_NUMBER
           ,rg.GROUP_NAME
           ,ch.Site_Not_Managed
           ,ch.Site_Closed
           ,ch.Site_Type_Name
           ,addr.ADDRESS_LINE1
           ,addr.ADDRESS_LINE2
           ,addr.CITY
           ,st.STATE_NAME
           ,addr.ZIPCODE
           ,Ctry.COUNTRY_NAME
           ,reg.REGION_NAME
           ,uacte.Account_Number
           ,uacte.UtilityAccountServiceLevel
           ,uacte.AccountCreated
           ,uacte.AccountInvoiceNotExpected
           ,uacte.AccountNotManaged
           ,uacte.UtilityAccountInvoiceSource
           ,uacte.Utility
           ,uacte.CommodityType
           ,uacte.Rate
           ,uacte.MeterNumber
           ,uacte.PurchaseMethod
           ,cc.ContractId
           ,BaseContractId
           ,cc.ContractNumber
           ,cc.ContractPricingStatus
           ,cc.ContractType
           ,cc.ContractedVendor
           ,cc.ContractStartDate
           ,cc.ContractEndDate
           ,cc.NotificationDays
           ,cc.NotificationDate
           ,cc.ContractTriggerRights
           ,cc.FullRequirements
           ,cc.CONTRACT_PRICING_SUMMARY
           ,cc.ContractComments
           ,cc.Currency
           ,cc.RenewalType
           ,cc.SupplierAccountNumber
           ,cc.SupplierAccountServiceLevel
           ,cc.SupplierAccountInvoiceSource
           ,cc.ContractCreatedBy
           ,cc.ContractCreatedDate
           ,addr.ADDRESS_ID
           ,uacte.AccountCreatedBy
           ,cc.ContractRecalcType
           ,uacte.Consumption_Level_Desc
           ,uacte.IsDataEntryOnly
           ,uacte.Account_Id
           ,cc.Account_Id
           ,uacte.Is_Broker_Account
           ,uacte.Broker_fee
           ,uacte.BrokerFee_Currency_Unit
           ,uacte.BrokerFee_Uom_Unit
           ,uia.FIRST_NAME
           ,uia.LAST_NAME
           ,cacc.Code_Value
           ,sacc.Code_Value
           ,aacc.Code_Value
           ,cc.SupplierAccountStartDate
           ,CASE WHEN cc.ContractId = -1
                      AND cc.SupplierAccountEndDate IS NULL THEN 'Unspecified'
                 ELSE CONVERT(VARCHAR(10), cc.SupplierAccountEndDate, 101)
            END
           ,cc.SupplierAccountCreatedBy
           ,cc.SupplierAccountCreatedDate
           ,cc.ContractProductType
           ,uacte.Meter_Id
           
      DROP TABLE #UtilityAccts_CTE
      DROP TABLE #Contracts_CTE

END;

;
GO





GRANT EXECUTE ON  [dbo].[Report_DE_JoinedFile_CommercialTeam] TO [CBMS_SSRS_Reports]
GO
