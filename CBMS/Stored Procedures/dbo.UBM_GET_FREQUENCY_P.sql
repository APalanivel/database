SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE dbo.UBM_GET_FREQUENCY_P
@userId varchar,
@sessionId varchar

as
	set nocount on
	SELECT ENTITY_ID, ENTITY_NAME 
	FROM ENTITY 
	WHERE ENTITY_TYPE=657
GO
GRANT EXECUTE ON  [dbo].[UBM_GET_FREQUENCY_P] TO [CBMSApplication]
GO
