SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:    dbo.Account_Load_Profile_Cost_Usage_Vol_Sel 
     
      
DESCRIPTION:     
     
      
INPUT PARAMETERS:      
      Name          DataType       Default        Description      
-----------------------------------------------------------------------------      
   
    
      
OUTPUT PARAMETERS:    
      
 Name     DataType   Default  Description      
-----------------------------------------------------------------------------      
      
      
      
USAGE EXAMPLES:      
-----------------------------------------------------------------------------      
   
Exec dbo.Account_Load_Profile_Cost_Usage_Vol_Sel @Sr_Rfp_Id = NULL,@Client_Id = 1005,@Commodity_Id = 290,
	@Contract_Start_Dt = '2020-01-01',@Contract_End_Dt = '2020-02-01',@Meter_Ids = '2818,2815',@Contract_Id = NULL, @ED_CONTRACT_NUMBER = NULL
Exec dbo.Account_Load_Profile_Cost_Usage_Vol_Sel @Sr_Rfp_Id = 12345,@Client_Id = 1005,@Commodity_Id = 290,
	@Contract_Start_Dt = '2020-01-01',@Contract_End_Dt = '2020-02-01',@Meter_Ids = '2818,2815',@Contract_Id = NULL, @ED_CONTRACT_NUMBER = NULL,@Is_Contract_Term = 1

Exec dbo.Account_Load_Profile_Cost_Usage_Vol_Sel @Sr_Rfp_Id = NULL,@Client_Id = NULL,@Commodity_Id = NULL,
	@Contract_Start_Dt = NULL,@Contract_End_Dt = NULL,@Meter_Ids = NULL,@Contract_Id = NULL, @ED_CONTRACT_NUMBER = '185050-0001'
Exec dbo.Account_Load_Profile_Cost_Usage_Vol_Sel @Sr_Rfp_Id = 12345,@Client_Id = NULL,@Commodity_Id = NULL,
	@Contract_Start_Dt = NULL,@Contract_End_Dt = NULL,@Meter_Ids = NULL,@Contract_Id = NULL, @ED_CONTRACT_NUMBER = '185050-0001'

Exec dbo.Account_Load_Profile_Cost_Usage_Vol_Sel @Sr_Rfp_Id = 24630,@Client_Id = 1005,@Commodity_Id = 290,
	@Contract_Start_Dt = '2019-01-01',@Contract_End_Dt = '2019-12-01',@Meter_Ids = '54340,54341'
Exec dbo.Account_Load_Profile_Cost_Usage_Vol_Sel @Sr_Rfp_Id = NULL,@Client_Id = 1005,@Commodity_Id = 290,
	@Contract_Start_Dt = '2019-01-01',@Contract_End_Dt = '2019-02-01',@Meter_Ids = '54340,54341'
Exec dbo.Account_Load_Profile_Cost_Usage_Vol_Sel null,1005,290,'2019-02-01','2019-04-01','3082',null,null,false
Exec dbo.Account_Load_Profile_Cost_Usage_Vol_Sel null,1005,290,'2019-02-01','2019-04-01','3082',null,null,false,102185
Exec dbo.Account_Load_Profile_Cost_Usage_Vol_Sel null,1005,290,'2019-02-01','2019-04-01','3082',null,null,false,102184
Exec dbo.Account_Load_Profile_Cost_Usage_Vol_Sel null,1005,290,'2019-02-01','2019-02-01','3082',null,null,false,102185
Exec dbo.Account_Load_Profile_Cost_Usage_Vol_Sel null,1005,290,'2019-02-01','2019-02-01','3082',null,null,false,103121
Exec dbo.Account_Load_Profile_Cost_Usage_Vol_Sel null,1005,290,'2019-02-01','2019-02-01','3082',null,null,false,103122

 
AUTHOR INITIALS:       
	Initials    Name  
-----------------------------------------------------------------------------         
	RR			Raghu Reddy
      
MODIFICATIONS       
	Initials    Date		Modification        
-----------------------------------------------------------------------------         
	RR			2020-04-01	GRM - Contract volumes enhancement - Created
	RR			2020-04-01	GRMUER-146 Added input parameter @Contract_Volume_Frequency_Cd
******/
CREATE PROC [dbo].[Account_Load_Profile_Cost_Usage_Vol_Sel]
    (
        @Sr_Rfp_Id INT = NULL
        , @Client_Id INT = NULL
        , @Commodity_Id INT = NULL
        , @Contract_Start_Dt DATE = NULL
        , @Contract_End_Dt DATE = NULL
        , @Meter_Ids VARCHAR(MAX) = NULL
        , @Contract_Id INT = NULL
        , @ED_CONTRACT_NUMBER VARCHAR(150) = NULL
        , @Is_Contract_Term BIT = 0
        , @Contract_Volume_Frequency_Cd INT = NULL
    )
WITH RECOMPILE
AS
    BEGIN

        SET NOCOUNT ON;


        CREATE TABLE #Account_Vols
             (
                 Account_Id INT
                 , Meter_Id INT
                 , Service_Month DATE
                 , Determinant_Type VARCHAR(50)
                 , Volume DECIMAL(28, 12)
                 , Bucket_Master_Id INT
                 , Uom_Type_Id INT
                 , Account_Number VARCHAR(50)
                 , Meter_Number VARCHAR(50)
             );

        CREATE TABLE #Meter_Tbl
             (
                 Meter_Id INT
             );

        DECLARE
            @Uom_Type_Id INT
            , @Uom_Name VARCHAR(200)
            , @Frequency_Type_Id INT
            , @Frequency_Name VARCHAR(200)
            , @On_Peak_Usage_Bucket_Master_Id INT
            , @Off_Peak_Usage_Bucket_Master_Id INT
            , @Contract_Volume_Frequency VARCHAR(25);


        SELECT
            @Client_Id = ch.Client_Id
            , @Commodity_Id = c.COMMODITY_TYPE_ID
            , @Contract_Start_Dt = c.CONTRACT_START_DATE
            , @Contract_End_Dt = c.CONTRACT_END_DATE
            , @Meter_Ids = NULL
            , @Contract_Id = c.CONTRACT_ID
        FROM
            dbo.CONTRACT c
            INNER JOIN Core.Client_Hier_Account cha
                ON c.CONTRACT_ID = cha.Supplier_Contract_ID
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
        WHERE
            c.ED_CONTRACT_NUMBER = @ED_CONTRACT_NUMBER
            AND @ED_CONTRACT_NUMBER IS NOT NULL;

        SELECT
            @Contract_Start_Dt = DATEADD(dd, -DATEPART(dd, @Contract_Start_Dt) + 1, @Contract_Start_Dt)
            , @Contract_End_Dt = DATEADD(dd, -DATEPART(dd, @Contract_End_Dt) + 1, @Contract_End_Dt);

        SELECT
            @Contract_Volume_Frequency_Cd = c.Contract_Volume_Frequency_Cd
        FROM
            dbo.CONTRACT c
        WHERE
            c.CONTRACT_ID = @Contract_Id
            AND @Contract_Id IS NOT NULL;

        SELECT
            @Contract_Volume_Frequency = c.Code_Value
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset c2
                ON c2.Codeset_Id = c.Codeset_Id
        WHERE
            c2.Codeset_Name = 'Contract Volume Frequency'
            AND c.Code_Id = @Contract_Volume_Frequency_Cd;

        SELECT
            @Is_Contract_Term = 1
        WHERE
            @Contract_Volume_Frequency = 'Contract Term';

        SELECT
            @Frequency_Type_Id = ent.ENTITY_ID
            , @Frequency_Name = ent.ENTITY_NAME
        FROM
            dbo.ENTITY ent
        WHERE
            ent.ENTITY_TYPE = 144
            AND ent.ENTITY_NAME = @Contract_Volume_Frequency;

        SELECT
            @On_Peak_Usage_Bucket_Master_Id = bm.Bucket_Master_Id
        FROM
            dbo.Bucket_Master bm
        WHERE
            bm.Commodity_Id = @Commodity_Id
            AND bm.Bucket_Name IN ( 'On-Peak Usage' )
            AND bm.Is_Category = 1;

        SELECT
            @Off_Peak_Usage_Bucket_Master_Id = bm.Bucket_Master_Id
        FROM
            dbo.Bucket_Master bm
        WHERE
            bm.Commodity_Id = @Commodity_Id
            AND bm.Bucket_Name IN ( 'Off-Peak Usage' )
            AND bm.Is_Category = 1;

        INSERT INTO #Meter_Tbl
             (
                 Meter_Id
             )
        SELECT
            CAST(us.Segments AS INT)
        FROM
            dbo.ufn_split(@Meter_Ids, ',') us
        WHERE
            @Meter_Ids IS NOT NULL
            AND @ED_CONTRACT_NUMBER IS NULL;

        INSERT INTO #Meter_Tbl
             (
                 Meter_Id
             )
        SELECT
            cha.Meter_Id
        FROM
            dbo.CONTRACT c
            INNER JOIN Core.Client_Hier_Account cha
                ON c.CONTRACT_ID = cha.Supplier_Contract_ID
        WHERE
            c.ED_CONTRACT_NUMBER = @ED_CONTRACT_NUMBER
            AND @ED_CONTRACT_NUMBER IS NOT NULL
            AND @Meter_Ids IS NULL;


        ---RFP
        INSERT INTO #Account_Vols
             (
                 Account_Id
                 , Meter_Id
                 , Service_Month
                 , Determinant_Type
                 , Volume
                 , Uom_Type_Id
                 , Account_Number
                 , Meter_Number
             )
        SELECT
            cha.Account_Id
            , cha.Meter_Id
            , dd.DATE_D
            , 'Total'
            , srldv.LP_VALUE
            , srlpd.DETERMINANT_UNIT_TYPE_ID
            , cha.Account_Number
            , cha.Meter_Number
        FROM
            #Meter_Tbl mt
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Meter_Id = mt.Meter_Id
            CROSS JOIN meta.DATE_DIM dd
            LEFT JOIN dbo.SR_RFP_ACCOUNT sra
                ON sra.ACCOUNT_ID = cha.Account_Id
                   AND  sra.SR_RFP_ID = @Sr_Rfp_Id
            LEFT JOIN dbo.SR_RFP_LOAD_PROFILE_SETUP srlps
                ON srlps.SR_RFP_ACCOUNT_ID = sra.SR_RFP_ACCOUNT_ID
                   AND  srlps.SR_RFP_ID = sra.SR_RFP_ID
            LEFT JOIN dbo.SR_RFP_LOAD_PROFILE_DETERMINANT srlpd
                ON srlpd.SR_RFP_LOAD_PROFILE_SETUP_ID = srlps.SR_RFP_LOAD_PROFILE_SETUP_ID
                   AND  srlpd.DETERMINANT_NAME IN ( 'Total Usage', 'Total Volume' )
            LEFT JOIN dbo.SR_RFP_LP_DETERMINANT_VALUES srldv
                ON srldv.SR_RFP_LOAD_PROFILE_DETERMINANT_ID = srlpd.SR_RFP_LOAD_PROFILE_DETERMINANT_ID
                   AND  dd.DATE_D = srldv.MONTH_IDENTIFIER
        WHERE
            dd.DATE_D BETWEEN @Contract_Start_Dt
                      AND     @Contract_End_Dt
            AND cha.Commodity_Id = @Commodity_Id
            AND cha.Account_Type = 'Utility'
            AND @Sr_Rfp_Id IS NOT NULL
        GROUP BY
            cha.Account_Id
            , cha.Meter_Id
            , dd.DATE_D
            , srldv.LP_VALUE
            , srlpd.DETERMINANT_UNIT_TYPE_ID
            , cha.Account_Number
            , cha.Meter_Number;

        INSERT INTO #Account_Vols
             (
                 Account_Id
                 , Meter_Id
                 , Service_Month
                 , Determinant_Type
                 , Volume
                 , Bucket_Master_Id
                 , Uom_Type_Id
                 , Account_Number
                 , Meter_Number
             )
        SELECT
            cha.Account_Id
            , cha.Meter_Id
            , dd.DATE_D
            , 'On Peak'
            , srldv.LP_VALUE
            , @On_Peak_Usage_Bucket_Master_Id
            , srlpd.DETERMINANT_UNIT_TYPE_ID
            , cha.Account_Number
            , cha.Meter_Number
        FROM
            #Meter_Tbl mt
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Meter_Id = mt.Meter_Id
            CROSS JOIN meta.DATE_DIM dd
            LEFT JOIN dbo.SR_RFP_ACCOUNT sra
                ON sra.ACCOUNT_ID = cha.Account_Id
                   AND  sra.SR_RFP_ID = @Sr_Rfp_Id
            LEFT JOIN dbo.SR_RFP_LOAD_PROFILE_SETUP srlps
                ON srlps.SR_RFP_ACCOUNT_ID = sra.SR_RFP_ACCOUNT_ID
                   AND  srlps.SR_RFP_ID = sra.SR_RFP_ID
            LEFT JOIN dbo.SR_RFP_LOAD_PROFILE_DETERMINANT srlpd
                ON srlpd.SR_RFP_LOAD_PROFILE_SETUP_ID = srlps.SR_RFP_LOAD_PROFILE_SETUP_ID
                   AND  srlpd.DETERMINANT_NAME IN ( 'On Peak kWh' )
            LEFT JOIN dbo.SR_RFP_LP_DETERMINANT_VALUES srldv
                ON srldv.SR_RFP_LOAD_PROFILE_DETERMINANT_ID = srlpd.SR_RFP_LOAD_PROFILE_DETERMINANT_ID
                   AND  dd.DATE_D = srldv.MONTH_IDENTIFIER
        WHERE
            dd.DATE_D BETWEEN @Contract_Start_Dt
                      AND     @Contract_End_Dt
            AND cha.Commodity_Id = @Commodity_Id
            AND cha.Account_Type = 'Utility'
            AND @Sr_Rfp_Id IS NOT NULL
        GROUP BY
            cha.Account_Id
            , cha.Meter_Id
            , dd.DATE_D
            , srldv.LP_VALUE
            , srlpd.DETERMINANT_UNIT_TYPE_ID
            , cha.Account_Number
            , cha.Meter_Number;

        INSERT INTO #Account_Vols
             (
                 Account_Id
                 , Meter_Id
                 , Service_Month
                 , Determinant_Type
                 , Volume
                 , Bucket_Master_Id
                 , Uom_Type_Id
                 , Account_Number
                 , Meter_Number
             )
        SELECT
            cha.Account_Id
            , cha.Meter_Id
            , dd.DATE_D
            , 'Off Peak'
            , srldv.LP_VALUE
            , @Off_Peak_Usage_Bucket_Master_Id
            , srlpd.DETERMINANT_UNIT_TYPE_ID
            , cha.Account_Number
            , cha.Meter_Number
        FROM
            #Meter_Tbl mt
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Meter_Id = mt.Meter_Id
            CROSS JOIN meta.DATE_DIM dd
            LEFT JOIN dbo.SR_RFP_ACCOUNT sra
                ON sra.ACCOUNT_ID = cha.Account_Id
                   AND  sra.SR_RFP_ID = @Sr_Rfp_Id
            LEFT JOIN dbo.SR_RFP_LOAD_PROFILE_SETUP srlps
                ON srlps.SR_RFP_ACCOUNT_ID = sra.SR_RFP_ACCOUNT_ID
                   AND  srlps.SR_RFP_ID = sra.SR_RFP_ID
            LEFT JOIN dbo.SR_RFP_LOAD_PROFILE_DETERMINANT srlpd
                ON srlpd.SR_RFP_LOAD_PROFILE_SETUP_ID = srlps.SR_RFP_LOAD_PROFILE_SETUP_ID
                   AND  srlpd.DETERMINANT_NAME IN ( 'Off Peak kWh' )
            LEFT JOIN dbo.SR_RFP_LP_DETERMINANT_VALUES srldv
                ON srldv.SR_RFP_LOAD_PROFILE_DETERMINANT_ID = srlpd.SR_RFP_LOAD_PROFILE_DETERMINANT_ID
                   AND  dd.DATE_D = srldv.MONTH_IDENTIFIER
        WHERE
            dd.DATE_D BETWEEN @Contract_Start_Dt
                      AND     @Contract_End_Dt
            AND cha.Commodity_Id = @Commodity_Id
            AND cha.Account_Type = 'Utility'
            AND @Sr_Rfp_Id IS NOT NULL
        GROUP BY
            cha.Account_Id
            , cha.Meter_Id
            , dd.DATE_D
            , srldv.LP_VALUE
            , srlpd.DETERMINANT_UNIT_TYPE_ID
            , cha.Account_Number
            , cha.Meter_Number;

        ----Cost and Usage
        INSERT INTO #Account_Vols
             (
                 Account_Id
                 , Meter_Id
                 , Service_Month
                 , Determinant_Type
                 , Volume
                 , Uom_Type_Id
                 , Account_Number
                 , Meter_Number
             )
        SELECT
            cha.Account_Id
            , cha.Meter_Id
            , dd.DATE_D
            , 'Total'
            , MAX(COALESCE(cuad.Bucket_Value, cuad0.Bucket_Value, cuad1.Bucket_Value, cuad2.Bucket_Value))
            , MAX(COALESCE(cuad.UOM_Type_Id, cuad0.UOM_Type_Id, cuad1.UOM_Type_Id, cuad2.UOM_Type_Id))
            , cha.Account_Number
            , cha.Meter_Number
        FROM
            #Meter_Tbl mt
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Meter_Id = mt.Meter_Id
            CROSS JOIN meta.DATE_DIM dd
            INNER JOIN dbo.Bucket_Master bm
                ON bm.Commodity_Id = cha.Commodity_Id
            LEFT JOIN dbo.Cost_Usage_Account_Dtl cuad
                ON cuad.Bucket_Master_Id = bm.Bucket_Master_Id
                   AND  cuad.ACCOUNT_ID = cha.Account_Id
                   AND  cuad.Client_Hier_ID = cha.Client_Hier_Id
                   AND  cuad.Service_Month = dd.DATE_D
            LEFT JOIN dbo.Cost_Usage_Account_Dtl cuad0
                ON cuad0.Bucket_Master_Id = bm.Bucket_Master_Id
                   AND  cuad0.ACCOUNT_ID = cha.Account_Id
                   AND  cuad0.Client_Hier_ID = cha.Client_Hier_Id
                   AND  cuad0.Service_Month = DATEADD(yy, -DATEDIFF(yy, GETDATE(), dd.DATE_D), dd.DATE_D)
            LEFT JOIN dbo.Cost_Usage_Account_Dtl cuad1
                ON cuad1.Bucket_Master_Id = bm.Bucket_Master_Id
                   AND  cuad1.ACCOUNT_ID = cha.Account_Id
                   AND  cuad1.Client_Hier_ID = cha.Client_Hier_Id
                   AND  cuad1.Service_Month = DATEADD(yy, -DATEDIFF(yy, GETDATE(), dd.DATE_D) - 1, dd.DATE_D)
            LEFT JOIN dbo.Cost_Usage_Account_Dtl cuad2
                ON cuad2.Bucket_Master_Id = bm.Bucket_Master_Id
                   AND  cuad2.ACCOUNT_ID = cha.Account_Id
                   AND  cuad2.Client_Hier_ID = cha.Client_Hier_Id
                   AND  cuad2.Service_Month = DATEADD(yy, -DATEDIFF(yy, GETDATE(), dd.DATE_D) - 2, dd.DATE_D)
        WHERE
            dd.DATE_D BETWEEN @Contract_Start_Dt
                      AND     @Contract_End_Dt
            AND cha.Commodity_Id = @Commodity_Id
            AND cha.Account_Type = 'Utility'
            AND @Sr_Rfp_Id IS NULL
            AND bm.Bucket_Name IN ( 'Total Usage', 'Total Volume' )
        GROUP BY
            cha.Account_Id
            , cha.Meter_Id
            , dd.DATE_D
            , cha.Account_Number
            , cha.Meter_Number;

        INSERT INTO #Account_Vols
             (
                 Account_Id
                 , Meter_Id
                 , Service_Month
                 , Determinant_Type
                 , Volume
                 , Bucket_Master_Id
                 , Uom_Type_Id
                 , Account_Number
                 , Meter_Number
             )
        SELECT
            cha.Account_Id
            , cha.Meter_Id
            , dd.DATE_D
            , 'On Peak'
            , MAX(COALESCE(cuad.Bucket_Value, cuad0.Bucket_Value, cuad1.Bucket_Value, cuad2.Bucket_Value))
            , @On_Peak_Usage_Bucket_Master_Id
            , MAX(COALESCE(cuad.UOM_Type_Id, cuad0.UOM_Type_Id, cuad1.UOM_Type_Id, cuad2.UOM_Type_Id))
            , cha.Account_Number
            , cha.Meter_Number
        FROM
            #Meter_Tbl mt
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Meter_Id = mt.Meter_Id
            CROSS JOIN meta.DATE_DIM dd
            INNER JOIN dbo.Bucket_Master bm
                ON bm.Commodity_Id = cha.Commodity_Id
            LEFT JOIN dbo.Cost_Usage_Account_Dtl cuad
                ON cuad.Bucket_Master_Id = bm.Bucket_Master_Id
                   AND  cuad.ACCOUNT_ID = cha.Account_Id
                   AND  cuad.Client_Hier_ID = cha.Client_Hier_Id
                   AND  cuad.Service_Month = dd.DATE_D
            LEFT JOIN dbo.Cost_Usage_Account_Dtl cuad0
                ON cuad0.Bucket_Master_Id = bm.Bucket_Master_Id
                   AND  cuad0.ACCOUNT_ID = cha.Account_Id
                   AND  cuad0.Client_Hier_ID = cha.Client_Hier_Id
                   AND  cuad0.Service_Month = DATEADD(yy, -DATEDIFF(yy, GETDATE(), dd.DATE_D), dd.DATE_D)
            LEFT JOIN dbo.Cost_Usage_Account_Dtl cuad1
                ON cuad1.Bucket_Master_Id = bm.Bucket_Master_Id
                   AND  cuad1.ACCOUNT_ID = cha.Account_Id
                   AND  cuad1.Client_Hier_ID = cha.Client_Hier_Id
                   AND  cuad1.Service_Month = DATEADD(yy, -DATEDIFF(yy, GETDATE(), dd.DATE_D) - 1, dd.DATE_D)
            LEFT JOIN dbo.Cost_Usage_Account_Dtl cuad2
                ON cuad2.Bucket_Master_Id = bm.Bucket_Master_Id
                   AND  cuad2.ACCOUNT_ID = cha.Account_Id
                   AND  cuad2.Client_Hier_ID = cha.Client_Hier_Id
                   AND  cuad2.Service_Month = DATEADD(yy, -DATEDIFF(yy, GETDATE(), dd.DATE_D) - 2, dd.DATE_D)
        WHERE
            dd.DATE_D BETWEEN @Contract_Start_Dt
                      AND     @Contract_End_Dt
            AND cha.Commodity_Id = @Commodity_Id
            AND cha.Account_Type = 'Utility'
            AND @Sr_Rfp_Id IS NULL
            AND bm.Bucket_Name = 'On-Peak Usage'
        GROUP BY
            cha.Account_Id
            , cha.Meter_Id
            , dd.DATE_D
            , cha.Account_Number
            , cha.Meter_Number;


        INSERT INTO #Account_Vols
             (
                 Account_Id
                 , Meter_Id
                 , Service_Month
                 , Determinant_Type
                 , Volume
                 , Bucket_Master_Id
                 , Uom_Type_Id
                 , Account_Number
                 , Meter_Number
             )
        SELECT
            cha.Account_Id
            , cha.Meter_Id
            , dd.DATE_D
            , 'Off Peak'
            , MAX(COALESCE(cuad.Bucket_Value, cuad0.Bucket_Value, cuad1.Bucket_Value, cuad2.Bucket_Value))
            , @Off_Peak_Usage_Bucket_Master_Id
            , MAX(COALESCE(cuad.UOM_Type_Id, cuad0.UOM_Type_Id, cuad1.UOM_Type_Id, cuad2.UOM_Type_Id))
            , cha.Account_Number
            , cha.Meter_Number
        FROM
            #Meter_Tbl mt
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Meter_Id = mt.Meter_Id
            CROSS JOIN meta.DATE_DIM dd
            INNER JOIN dbo.Bucket_Master bm
                ON bm.Commodity_Id = cha.Commodity_Id
            LEFT JOIN dbo.Cost_Usage_Account_Dtl cuad
                ON cuad.Bucket_Master_Id = bm.Bucket_Master_Id
                   AND  cuad.ACCOUNT_ID = cha.Account_Id
                   AND  cuad.Client_Hier_ID = cha.Client_Hier_Id
                   AND  cuad.Service_Month = dd.DATE_D
            LEFT JOIN dbo.Cost_Usage_Account_Dtl cuad0
                ON cuad0.Bucket_Master_Id = bm.Bucket_Master_Id
                   AND  cuad0.ACCOUNT_ID = cha.Account_Id
                   AND  cuad0.Client_Hier_ID = cha.Client_Hier_Id
                   AND  cuad0.Service_Month = DATEADD(yy, -DATEDIFF(yy, GETDATE(), dd.DATE_D), dd.DATE_D)
            LEFT JOIN dbo.Cost_Usage_Account_Dtl cuad1
                ON cuad1.Bucket_Master_Id = bm.Bucket_Master_Id
                   AND  cuad1.ACCOUNT_ID = cha.Account_Id
                   AND  cuad1.Client_Hier_ID = cha.Client_Hier_Id
                   AND  cuad1.Service_Month = DATEADD(yy, -DATEDIFF(yy, GETDATE(), dd.DATE_D) - 1, dd.DATE_D)
            LEFT JOIN dbo.Cost_Usage_Account_Dtl cuad2
                ON cuad2.Bucket_Master_Id = bm.Bucket_Master_Id
                   AND  cuad2.ACCOUNT_ID = cha.Account_Id
                   AND  cuad2.Client_Hier_ID = cha.Client_Hier_Id
                   AND  cuad2.Service_Month = DATEADD(yy, -DATEDIFF(yy, GETDATE(), dd.DATE_D) - 2, dd.DATE_D)
        WHERE
            dd.DATE_D BETWEEN @Contract_Start_Dt
                      AND     @Contract_End_Dt
            AND cha.Commodity_Id = @Commodity_Id
            AND cha.Account_Type = 'Utility'
            AND @Sr_Rfp_Id IS NULL
            AND bm.Bucket_Name = 'Off-Peak Usage'
        GROUP BY
            cha.Account_Id
            , cha.Meter_Id
            , dd.DATE_D
            , cha.Account_Number
            , cha.Meter_Number;

        UPDATE
            avop
        SET
            avop.Volume = avttl.Volume
            , avop.Uom_Type_Id = avttl.Uom_Type_Id
        FROM
            #Account_Vols avop
            INNER JOIN #Account_Vols avttl
                ON avttl.Account_Id = avop.Account_Id
                   AND  avttl.Meter_Id = avop.Meter_Id
                   AND  avttl.Service_Month = avop.Service_Month
        WHERE
            avop.Determinant_Type = 'On Peak'
            AND avttl.Determinant_Type = 'Total'
            AND avop.Volume IS NULL;

        UPDATE
            av
        SET
            av.Volume = av.Volume / dd.DAYS_IN_MONTH_NUM
        FROM
            #Account_Vols av
            INNER JOIN meta.DATE_DIM dd
                ON dd.DATE_D = av.Service_Month
        WHERE
            @Contract_Volume_Frequency = 'Daily';




        UPDATE
            #Account_Vols
        SET
            Service_Month = @Contract_Start_Dt
        WHERE
            @Is_Contract_Term = 1;


        SELECT
            @Uom_Type_Id = en.ENTITY_ID
            , @Uom_Name = en.ENTITY_NAME
        FROM
            dbo.Country_Commodity_Uom ccu
            INNER JOIN dbo.ENTITY en
                ON en.ENTITY_ID = ccu.Default_Uom_Type_Id
        WHERE
            ccu.Commodity_Id = @Commodity_Id
            AND EXISTS (   SELECT
                                1
                           FROM
                                #Meter_Tbl mt
                                INNER JOIN Core.Client_Hier_Account cha
                                    ON cha.Meter_Id = mt.Meter_Id
                           WHERE
                                ccu.COUNTRY_ID = cha.Meter_Country_Id);

        SELECT
            @Uom_Type_Id = ISNULL(@Uom_Type_Id, en.ENTITY_ID)
            , @Uom_Name = ISNULL(@Uom_Name, en.ENTITY_NAME)
        FROM
            dbo.Commodity c
            JOIN dbo.ENTITY en
                ON en.ENTITY_TYPE = c.UOM_Entity_Type
        WHERE
            c.Commodity_Id = @Commodity_Id
            AND c.Default_UOM_Entity_Type_Id = en.ENTITY_ID;

        SELECT
            av.Account_Id
            , av.Service_Month
            , COUNT(DISTINCT av.Meter_Id) AS Meters_Cnt
        INTO
            #Account_Meters_Cnt
        FROM
            #Account_Vols av
        GROUP BY
            av.Account_Id
            , av.Service_Month;

        SELECT
            av.Account_Id
            , av.Meter_Id
            , av.Service_Month
            , av.Determinant_Type
            , CAST(SUM(ISNULL((av.Volume * cuc.CONVERSION_FACTOR), 0)) / amc.Meters_Cnt AS DECIMAL(32, 0)) AS Volume
            , @Uom_Type_Id AS Uom_Type_Id
            , @Uom_Name AS Uom_Name
            , @Frequency_Type_Id AS Frequency_Type_Id
            , @Frequency_Name AS Frequency_Name
            , av.Bucket_Master_Id
            , av.Account_Number
            , av.Meter_Number
            , CAST(0 AS BIT) AS Is_Edited
            , amc.Meters_Cnt
        FROM
            #Account_Vols av
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = av.Uom_Type_Id
                   AND  cuc.CONVERTED_UNIT_ID = @Uom_Type_Id
            INNER JOIN #Account_Meters_Cnt amc
                ON amc.Account_Id = av.Account_Id
                   AND  amc.Service_Month = av.Service_Month
        GROUP BY
            av.Account_Id
            , av.Meter_Id
            , av.Service_Month
            , av.Determinant_Type
            , av.Bucket_Master_Id
            , av.Account_Number
            , av.Meter_Number
            , amc.Meters_Cnt
        ORDER BY
            av.Account_Id
            , av.Meter_Id
            , av.Service_Month
            , av.Determinant_Type;

        DROP TABLE
            #Account_Vols
            , #Account_Meters_Cnt
            , #Meter_Tbl;

    END;
GO
GRANT EXECUTE ON  [dbo].[Account_Load_Profile_Cost_Usage_Vol_Sel] TO [CBMSApplication]
GO
