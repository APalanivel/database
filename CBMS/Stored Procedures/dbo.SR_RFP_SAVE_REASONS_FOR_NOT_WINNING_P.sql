SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_SAVE_REASONS_FOR_NOT_WINNING_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@reasonNotWinningId	int       	          	
	@reasonTypeId  	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE     PROCEDURE DBO.SR_RFP_SAVE_REASONS_FOR_NOT_WINNING_P

@reasonNotWinningId int,
@reasonTypeId int

AS
set nocount on

INSERT INTO SR_RFP_REASON_NOT_WINNING_MAP(SR_RFP_REASON_NOT_WINNING_ID,NOT_WINNING_REASON_TYPE_ID) 
VALUES ( @reasonNotWinningId , @reasonTypeId )
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_SAVE_REASONS_FOR_NOT_WINNING_P] TO [CBMSApplication]
GO
