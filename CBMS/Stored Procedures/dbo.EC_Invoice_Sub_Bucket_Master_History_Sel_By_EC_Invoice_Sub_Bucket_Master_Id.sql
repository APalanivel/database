
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:                
      [dbo].[EC_Invoice_Sub_Bucket_Master_History_Sel_By_EC_Invoice_Sub_Bucket_Master_Id]              
                
Description:                
            To get the details of active tracking data  data for a given sub bucket id.               
                
 Input Parameters:                
    Name        DataType   Default    Description                  
----------------------------------------------------------------------------------------------              
 @EC_Invoice_Sub_Bucket_Master_Id    INT              
                 
 Output Parameters:                      
    Name        DataType   Default    Description                  
----------------------------------------------------------------------------------------------              
                
 Usage Examples:                    
----------------------------------------------------------------------------------------------         
       
 EXEC dbo.EC_Invoice_Sub_Bucket_Master_History_Sel_By_EC_Invoice_Sub_Bucket_Master_Id  1   
   
 EXEC dbo.EC_Invoice_Sub_Bucket_Master_History_Sel_By_EC_Invoice_Sub_Bucket_Master_Id  2  
   
Author Initials:                
      Initials         Name                
----------------------------------------------------------------------------------------------              
      NR              Narayana Reddy 
      RKV             Ravi Kumar Vegesna        
 Modifications:                
      Initials  Date   Modification                
----------------------------------------------------------------------------------------------              
       NR   2015-05-08       Created for AS400.           
       RKV  2015-11-10       changed the table EC_Calc_Val to Ec_Calc_Val_Bucket_Sub_Bucket_Map 
									to get the info of sub_buckets as Part of AS400-PII
******/               
            
CREATE PROCEDURE [dbo].[EC_Invoice_Sub_Bucket_Master_History_Sel_By_EC_Invoice_Sub_Bucket_Master_Id]
      ( 
       @EC_Invoice_Sub_Bucket_Master_Id INT )
AS 
BEGIN            
      SET NOCOUNT ON;      
        
      DECLARE @EC_Invoice_Sub_Bucket_Master_Id_Is_History_Exists BIT   
         
      SET @EC_Invoice_Sub_Bucket_Master_Id_Is_History_Exists = 0       
                
      SELECT
            @EC_Invoice_Sub_Bucket_Master_Id_Is_History_Exists = 1
      FROM
            dbo.Ec_Calc_Val_Bucket_Sub_Bucket_Map ecvbsbm
            
      WHERE
            ecvbsbm.EC_Invoice_Sub_Bucket_Master_Id = @EC_Invoice_Sub_Bucket_Master_Id   
            
      SELECT
            @EC_Invoice_Sub_Bucket_Master_Id_Is_History_Exists = 1
      FROM
            dbo.CU_INVOICE_CHARGE cic
      WHERE
            cic.EC_Invoice_Sub_Bucket_Master_Id = @EC_Invoice_Sub_Bucket_Master_Id   
            
      SELECT
            @EC_Invoice_Sub_Bucket_Master_Id_Is_History_Exists = 1
      FROM
            dbo.CU_INVOICE_DETERMINANT cid
      WHERE
            cid.EC_Invoice_Sub_Bucket_Master_Id = @EC_Invoice_Sub_Bucket_Master_Id  
            
            
              
      SELECT
            @EC_Invoice_Sub_Bucket_Master_Id_Is_History_Exists AS EC_Invoice_Sub_Bucket_Master_Exists  
                       
END    
                  
;
;
GO

GRANT EXECUTE ON  [dbo].[EC_Invoice_Sub_Bucket_Master_History_Sel_By_EC_Invoice_Sub_Bucket_Master_Id] TO [CBMSApplication]
GO
