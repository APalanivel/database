SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[cbmsInvoiceParticipation_EstablishForDarden]
	( @MyAccountId int )
AS
BEGIN

	set nocount on

	declare @report_year int
	set @report_year = 2007

	  declare @start_month int
		, @begin_date datetime
		, @end_date datetime
		, @reference_date datetime
		, @working_date datetime
		, @account_id int
		, @site_id int
		, @is_expected bit



      set @begin_date 	= convert(datetime, '6/1/' + convert(varchar, @report_year))
      set @end_date 	= convert(datetime, '5/1/' + convert(varchar, @report_year + 1))

	set @reference_date = dateadd(m, -1, @begin_date)

/*
	print @begin_date
	print @end_date
	print @reference_date
*/
	
	declare curUtility cursor
	read_only
	for
		   select distinct a.account_id
			, vws.site_id 
			, 1
		     from client cl with (nolock)
		     join vwSiteName vws with (nolock) on vws.client_id = cl.client_id
		     join vwAccountMeter vam with (nolock) on vam.site_id = vws.site_id
		     join account a with (nolock) on a.account_id = vam.account_id
	  left outer join invoice_participation ip with (nolock) on ip.site_id = vws.site_id and ip.account_id = vam.account_id
		    where cl.client_id = 218
		      and a.not_managed = 0
	              and a.account_type_id = 38
		      and ip.invoice_participation_id is null

	
	OPEN curUtility
	
	FETCH NEXT FROM curUtility INTO @account_id, @site_id, @is_expected
	WHILE (@@fetch_status <> -1)
	BEGIN
	
		set @working_date = @begin_date
	
		while @working_date <= @end_date
		begin

			EXEC cbmsInvoiceParticipationQueue_Save
				  @MyAccountId
				, 9 -- Make Expected
				, null
				, null
				, @site_id 
				, @account_id 
				, @working_date
				, 1

			set @working_date = dateadd("m", 1, @working_date)

		end
	
		FETCH NEXT FROM curUtility INTO @account_id, @site_id, @is_expected
	END
	
	CLOSE curUtility 
	DEALLOCATE curUtility 


END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvoiceParticipation_EstablishForDarden] TO [CBMSApplication]
GO
