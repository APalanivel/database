SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[cbmsSite_UpdateParticipation]
   (
     @user_info_id int
   , @site_id int
   , @closed bit = null
   , @closed_date datetime = null
   , @not_managed bit = null
	
   )
AS 
   BEGIN

      set nocount on

      if @closed = 1
         or @not_managed = 1 
         begin

            declare @current_managed bit
             , @current_closed bit

            select   @current_managed = isNull(not_managed, 0)
                   , @current_closed = isNull(closed, 0)
            from     site  
            where    site_id = @site_id

            if @not_managed = 1
               and @current_managed = 0 
               begin
	
                  insert   into do_not_track
                           (
                             user_info_id
                           , account_id
                           , reason_type_id
                           , account_number
                           , client_name
                           , client_city
                           , state_id
                           , vendor_name
                           , date_added
				   )
                           select distinct
                                    @user_info_id user_info_id
                                  , a.account_id
                                  , 314
                                  , isNull(a.account_number,
                                           'Blank for '
                                           + v.vendor_name)
                                  , cl.client_name
                                  , ad.city
                                  , ad.state_id
                                  , v.vendor_name
                                  , getdate()
                           from     site s  
                                    join vwAccountMeter vam
                                     
                                         on vam.site_id = s.site_id
                                    join account a  
                                                        on a.account_id = vam.account_id
                                    join division d  
                                                         on d.division_id = s.division_id
                                    join client cl  
                                                        on cl.client_id = d.client_id
                                    join address ad  
                                                         on ad.address_id = s.primary_address_id
                                    join vendor v  
                                                       on v.vendor_id = a.vendor_id
                                    left outer join do_not_track dnt
                                     
                                         on dnt.account_id = a.account_id
                           where    s.site_id = @site_id
                                    and dnt.do_not_track_id is null


                  update   invoice_participation  
                  set      is_expected = 0
                  where    site_id = @site_id
                           and is_received = 0
	
               end

            if @closed = 1
               and @current_closed = 0 
               begin
	
                  update   invoice_participation  
                  set      is_expected = 0
                  where    site_id = @site_id
                           and is_received = 0
                           and service_month >= @closed_date

               end
         end
   END
GO
GRANT EXECUTE ON  [dbo].[cbmsSite_UpdateParticipation] TO [CBMSApplication]
GO
