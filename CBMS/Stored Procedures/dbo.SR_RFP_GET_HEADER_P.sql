
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	cbms_prod.dbo.SR_RFP_GET_HEADER_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------      	
	@userId			VARCHAR
    @sessionId		VARCHAR
    @rfpId			INT 

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC SR_RFP_GET_HEADER_P 0,0,584
	EXEC SR_RFP_GET_HEADER_P 0,0,199
	EXEC SR_RFP_GET_HEADER_P 0,0,3395
	EXEC SR_RFP_GET_HEADER_P 0,0,13244
	EXEC SR_RFP_GET_HEADER_P 0,0,6
	EXEC SR_RFP_GET_HEADER_P 0,0,13256


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RR			2015-10-14	Added header
							Global Sourcing - Phase2 - Added RFP_INITIATED_DATE to select list
	RR			2016-02-11	Global Sourcing - Phase3 - Added Is_Term_Date_Specific to select list
							GCS-524 - Added email address of the user in whose queue given RFP is in
	RR			2016-04-05	Global Sourcing - Phase3 - GCS-521 Added Rfp_Round column
******/
CREATE  PROCEDURE [dbo].[SR_RFP_GET_HEADER_P]
      ( 
       @userId VARCHAR
      ,@sessionId VARCHAR
      ,@rfpId INT )
AS 
BEGIN
      SET NOCOUNT ON;
      
      DECLARE
            @isHybridRFP BIT
           ,@Rfp_Round INT

      SELECT
            @isHybridRFP = 0

      SELECT
            @isHybridRFP = 1
      FROM
            dbo.SR_RFP_SMU
      WHERE
            SR_RFP_ID = @rfpId;
      WITH  Cte_Rfp_Order ( SR_RFP_ID, Base_Sr_Rfp_Id, Ranking, Last_Child )
              AS ( SELECT
                        SR_RFP_ID
                       ,Base_Sr_Rfp_Id
                       ,1 AS Ranking
                       ,SR_RFP_ID AS Last_Child
                   FROM
                        dbo.SR_RFP
                   WHERE
                        SR_RFP_ID = @rfpId
                   UNION ALL
                   SELECT
                        t1.SR_RFP_ID
                       ,t1.Base_Sr_Rfp_Id
                       ,Ranking + 1 AS Ranking
                       ,Last_Child
                   FROM
                        dbo.SR_RFP t1
                        INNER JOIN Cte_Rfp_Order sg
                              ON sg.Base_Sr_Rfp_Id = t1.SR_RFP_ID )
            SELECT
                  @Rfp_Round = max(c1.Ranking)
            FROM
                  Cte_Rfp_Order c1
           
                 

      SELECT
            rfp.SR_RFP_ID
           ,ENT.ENTITY_NAME AS RFP_STATUS
           ,ENT1.ENTITY_NAME AS COMMODITY
           ,vndr.VENDOR_NAME
           ,rfpacc.ACCOUNT_ID
           ,@isHybridRFP AS IS_HYBRID_RFP
           ,initidt.RFP_INITIATED_DATE
           ,rfp.Is_Term_Date_Specific
           ,ui.EMAIL_ADDRESS AS Queue_User_Email_Address
           ,@Rfp_Round AS Rfp_Round
      FROM
            dbo.SR_RFP rfp
            LEFT JOIN dbo.SR_RFP_ACCOUNT rfpacc
                  ON rfp.SR_RFP_ID = rfpacc.SR_RFP_ID
            LEFT JOIN dbo.ENTITY ENT
                  ON ENT.ENTITY_ID = rfp.RFP_STATUS_TYPE_ID
            LEFT JOIN dbo.ENTITY ENT1
                  ON ENT1.ENTITY_ID = rfp.COMMODITY_TYPE_ID
            LEFT JOIN dbo.ACCOUNT acc
                  ON rfpacc.ACCOUNT_ID = acc.ACCOUNT_ID
            LEFT JOIN dbo.VENDOR vndr
                  ON acc.VENDOR_ID = vndr.VENDOR_ID
            LEFT JOIN ( SELECT
                              dtrfpacc.SR_RFP_ID
                             ,min(dtrfpchk.RFP_INITIATED_DATE) AS RFP_INITIATED_DATE
                        FROM
                              dbo.SR_RFP_ACCOUNT dtrfpacc
                              LEFT JOIN dbo.SR_RFP_CHECKLIST dtrfpchk
                                    ON dtrfpchk.SR_RFP_ACCOUNT_ID = dtrfpacc.SR_RFP_ACCOUNT_ID
                        GROUP BY
                              dtrfpacc.SR_RFP_ID ) initidt
                  ON rfp.SR_RFP_ID = initidt.SR_RFP_ID
            INNER JOIN dbo.USER_INFO ui
                  ON rfp.Queue_Id = ui.QUEUE_ID
      WHERE
            rfp.SR_RFP_ID = @rfpId
     
           
END


;
GO



GRANT EXECUTE ON  [dbo].[SR_RFP_GET_HEADER_P] TO [CBMSApplication]
GO
