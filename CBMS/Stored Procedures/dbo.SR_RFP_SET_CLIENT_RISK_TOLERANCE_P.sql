SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_SET_CLIENT_RISK_TOLERANCE_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@accountBidGroupId int,
@isBidGroup int,
@riskToleranceTypeId int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- Test an update
--exec [dbo].[SR_RFP_SET_CLIENT_RISK_TOLERANCE_P]
--@accountBidGroupId = 164 ,
--@isBidGroup = 0, -- orginal value = null
--@riskToleranceTypeId = 1  -- original value = null

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[SR_RFP_SET_CLIENT_RISK_TOLERANCE_P] 

@accountBidGroupId int,
@isBidGroup int,
@riskToleranceTypeId int

as
	
set nocount on

IF @isBidGroup=0

BEGIN

	UPDATE SR_RFP_ACCOUNT SET 
	RISK_TOLERANCE_TYPE_ID=@riskToleranceTypeId 
	WHERE SR_RFP_ACCOUNT_ID=@accountBidGroupId 
	
END

ELSE IF @isBidGroup=1
BEGIN

	UPDATE SR_RFP_ACCOUNT SET 
	RISK_TOLERANCE_TYPE_ID=@riskToleranceTypeId 
	WHERE SR_RFP_BID_GROUP_ID=@accountBidGroupId
	
END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_SET_CLIENT_RISK_TOLERANCE_P] TO [CBMSApplication]
GO
