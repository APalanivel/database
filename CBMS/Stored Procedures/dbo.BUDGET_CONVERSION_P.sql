SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







--exec BUDGET_CONVERSION_P 821


CREATE                      PROCEDURE DBO.BUDGET_CONVERSION_P 

@budget_id int

AS

DECLARE @month_identifier datetime
DECLARE @account_id int
DECLARE @error as int
DECLARE @budget_Account_id int
DECLARE @budget_usage decimal (32,16)
DECLARE @transportation decimal (32,16)
DECLARE @variable decimal (32,16)
DECLARE	@transmission decimal (32,16)
DECLARE	@distribution decimal (32,16)
DECLARE	@other_unit decimal (32,16)
DECLARE	@other_fixed decimal (32,16)

set @error = 0

set nocount on

begin tran


update budget set DATE_POSTED = getDate() where budget_id = @budget_id

DECLARE C_ACCOUNT CURSOR FAST_FORWARD
	FOR
	select distinct account_id from budget_conversion 

OPEN C_ACCOUNT
	
	FETCH NEXT FROM C_ACCOUNT INTO @account_id
	WHILE (@@fetch_status <> -1)
	BEGIN
		IF (@@fetch_status <> -2)
		BEGIN
			
			select distinct @budget_Account_id =  budget_account_id from budget_account where account_id = @account_id
			and budget_id = @budget_id
			
			UPDATE budget_account set SOURCING_COMPLETED_DATE = getDate(),
			--SOURCING_COMPLETED_BY = 'System Conversion',
			RATES_COMPLETED_DATE = getDate(),
			--RATES_COMPLETED_BY = 'System Conversion',
			RATES_REVIEWED_DATE = getDate(),
			--RATES_REVIEWED_BY = 'System Conversion'
			HAS_DETAILS_SAVED = 1
			where budget_account_id = @budget_Account_id		

			DECLARE C_MONTH_IDENTIFIER CURSOR FAST_FORWARD
			FOR
			select distinct MONTH_IDENTIFIER from budget_conversion where account_id = @account_id
	
			OPEN C_MONTH_IDENTIFIER
			
			FETCH NEXT FROM C_MONTH_IDENTIFIER INTO @month_identifier
			WHILE (@@fetch_status <> -1)
			BEGIN
				IF (@@fetch_status <> -2)
				BEGIN
					
				      select @budget_usage = budget_usage, @variable = gen_or_comm,
				       @transportation = Transportation, @transmission = transmission,
				       @distribution = distribution,@other_unit = other_unit,@other_fixed = other_fixed
				       from budget_conversion
				       where account_id = @account_id and month_identifier = @month_identifier	
					
					
					Update budget_details set BUDGET_USAGE = @budget_usage, VARIABLE = @variable,
					VARIABLE_VALUE=@variable,TRANSPORTATION=@transportation,
					TRANSPORTATION_VALUE=@transportation,TRANSMISSION=@transmission, 
					TRANSMISSION_VALUE=@transmission,DISTRIBUTION=@distribution,
					DISTRIBUTION_VALUE=@distribution,OTHER_BUNDLED=@other_unit,
					OTHER_BUNDLED_VALUE=@other_unit,OTHER_FIXED_COSTS=@other_fixed ,
					OTHER_FIXED_COSTS_VALUE=@other_fixed
					where month_identifier = @month_identifier and budget_account_id = @budget_Account_id
					
					
					/*select @budget_usage = budget_usage from budget_conversion
				       where account_id = @account_id and month_identifier = @month_identifier
					Update budget_details set BUDGET_USAGE = @budget_usage
					where month_identifier = @month_identifier and budget_account_id = @budget_Account_id*/
					

					set @error = @error + @@error
					
				  END
			 FETCH NEXT FROM C_MONTH_IDENTIFIER INTO @month_identifier
			 END
			 CLOSE C_MONTH_IDENTIFIER
			 DEALLOCATE C_MONTH_IDENTIFIER
	


		   END
		FETCH NEXT FROM C_ACCOUNT INTO @account_id
		END
		CLOSE C_ACCOUNT
		DEALLOCATE C_ACCOUNT

if @error = 0
	commit tran
else
	rollback tran

set nocount off









GO
GRANT EXECUTE ON  [dbo].[BUDGET_CONVERSION_P] TO [CBMSApplication]
GO
