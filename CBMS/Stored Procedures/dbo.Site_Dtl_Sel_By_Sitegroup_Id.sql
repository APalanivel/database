SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                 
/******                  
Name:                  
        dbo.Site_Dtl_Sel_By_Sitegroup_Id                
                  
Description:                  
        To get the sites of sitegroup                  
                  
Input Parameters:                  
 Name                           DataType        Default        Description                    
-----------------------------------------------------------------------------------------------                    
 @SiteGroup_Id					INT
                  
 Output Parameters:                        
 Name                           DataType        Default        Description                    
-----------------------------------------------------------------------------------------------                    
                  
Usage Examples:                      
-----------------------------------------------------------------------------------------------                    

select Sitegroup_Id from core.client_hier where hier_level_cd=100017
EXEC dbo.Site_Dtl_Sel_By_Sitegroup_Id 12201010
                 
Author Initials:                  
   Initials        Name                  
-----------------------------------------------------------------------------------------------                    
   SP              Sandeep Pigilam    
                   
Modifications:                  
   Initials       Date             Modification                  
-----------------------------------------------------------------------------------------------                    
   SP             2014-01-24       Created                
                 
******/ 
CREATE PROCEDURE [dbo].[Site_Dtl_Sel_By_Sitegroup_Id]
      ( 
       @SiteGroup_Id INT = NULL )
AS 
BEGIN

      SET NOCOUNT ON

      SELECT
            ch.site_id
           ,ch.site_name
           ,sgs.Sitegroup_id
      FROM
            core.Client_Hier ch
            JOIN dbo.SiteGroup_Site sgs
                  ON ch.Site_Id = sgs.Site_id
      WHERE
            sgs.Sitegroup_id = @SiteGroup_Id
      ORDER BY
            ch.Site_Name

END



;
GO
GRANT EXECUTE ON  [dbo].[Site_Dtl_Sel_By_Sitegroup_Id] TO [CBMSApplication]
GO
