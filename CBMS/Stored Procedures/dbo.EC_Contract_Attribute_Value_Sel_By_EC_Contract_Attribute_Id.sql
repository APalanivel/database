SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.EC_Contract_Attribute_Value_Sel_By_EC_Contract_Attribute_Id       
              
Description:              
			This sproc get the attribute value details for the given attribute id.       
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
    @EC_Contract_Attribute_Id			INT
    
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

   Exec dbo.EC_Contract_Attribute_Value_Sel_By_EC_Contract_Attribute_Id 23
   
   Exec dbo.EC_Contract_Attribute_Value_Sel_By_EC_Contract_Attribute_Id 28
   
	
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy               
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2015-04-22		Created For AS400.         
             
******/ 

CREATE PROCEDURE [dbo].[EC_Contract_Attribute_Value_Sel_By_EC_Contract_Attribute_Id]
      ( 
       @EC_Contract_Attribute_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 
            
      SELECT
            eca.EC_Contract_Attribute_Id
           ,eca.EC_Contract_Attribute_Name
           ,ecav.EC_Contract_Attribute_Value_Id
           ,ecav.EC_Contract_Attribute_Value
           ,CASE WHEN ecat.EC_Contract_Attribute_Value IS NULL THEN 0
                 ELSE 1
            END AS Is_Used
      FROM
            dbo.EC_Contract_Attribute_Value ecav
            INNER JOIN dbo.EC_Contract_Attribute eca
                  ON ecav.EC_Contract_Attribute_Id = eca.EC_Contract_Attribute_Id
            LEFT OUTER JOIN dbo.EC_Contract_Attribute_Tracking ecat
                  ON ecav.EC_Contract_Attribute_Id = ecat.EC_Contract_Attribute_Id
                     AND ecav.EC_Contract_Attribute_Value = ecat.EC_Contract_Attribute_Value
      WHERE
            ecav.EC_Contract_Attribute_Id = @EC_Contract_Attribute_Id
      GROUP BY
            eca.EC_Contract_Attribute_Id
           ,eca.EC_Contract_Attribute_Name
           ,ecav.EC_Contract_Attribute_Value_Id
           ,ecav.EC_Contract_Attribute_Value
           ,CASE WHEN ecat.EC_Contract_Attribute_Value IS NULL THEN 0
                 ELSE 1
            END 
                 
END

;
GO
GRANT EXECUTE ON  [dbo].[EC_Contract_Attribute_Value_Sel_By_EC_Contract_Attribute_Id] TO [CBMSApplication]
GO
