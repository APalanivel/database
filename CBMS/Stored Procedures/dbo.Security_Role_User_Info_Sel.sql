
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
NAME:    dbo.Security_Role_User_Info_Sel          
          
DESCRIPTION:          
   
 Procedure will return Security Role all users  
   
          
INPUT PARAMETERS:          
 Name     DataType  Default    Description          
----------------------------------------------------------------------  
 @Security_Role_Id  INT  
       
OUTPUT PARAMETERS:          
 Name     DataType  Default    Description          
-----------------------------------------------------------------------  
  User_Info_Id   BIT        
  UserName    varchar  
  EmailAddress   varchar  
          
USAGE EXAMPLES:          
------------------------------------------------------------------------  
 exec dbo.Security_Role_User_Info_Sel 2  
   
 exec dbo.Security_Role_User_Info_Sel  749  
  
          
AUTHOR INITIALS:          
   Initials  Name          
-------------------------------------------------------------------------  
 DSC   Kaushik  
 NR   Narayana Reddy  
  
  
MODIFICATIONS          
          
 Initials	Date		Modification          
--------------------------------------------------------------------------  
DSC			08/21/2012  Created  
NR			2017-03-21  MAINT-4824 Added IS_SSO_User flag.  
HG			2017-04-12	Modified to return the Is_SSO_User only for the clients where Is_Single_Signon_Login is 1 OR Client_Host_Url is not null and only for Starwoods or any other clients configured in App_Config table.
KVK			10/3/2017	modified to not to have any conditions and returning flag from select
******/
CREATE PROCEDURE [dbo].[Security_Role_User_Info_Sel] ( @Security_Role_Id INT )
AS
BEGIN

      SET NOCOUNT ON;

      DECLARE @SSO_Email_Notification_Client TABLE 
													( Client_Id INT );

      DECLARE @SSO_Email_Notification_Client_Id VARCHAR(8000);

      SELECT
            @SSO_Email_Notification_Client_Id = App_Config_Value
      FROM
            dbo.App_Config
      WHERE
            App_Config_Cd = 'SSO_Email_Notification_Client';

      INSERT INTO @SSO_Email_Notification_Client ( Client_Id )
                  SELECT
                        cast(s.Segments AS INT)
                  FROM
                        dbo.ufn_split(@SSO_Email_Notification_Client_Id, ',') s;

      SELECT
            ui.USER_INFO_ID AS User_Info_Id
          , ui.USERNAME AS UserName
          , ui.EMAIL_ADDRESS AS Email_Address
          , CASE WHEN  (     cl.Is_Single_Signon_Login = 1
                             OR Client_Host_Url IS NOT NULL ) THEN 1
                 ELSE  0
            END AS Is_SSo_User
          , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS FullName
          , CASE WHEN ssenc.Client_Id IS NOT NULL THEN 1
                 ELSE  0
            END SSO_Email_Notification
      FROM
            dbo.User_Security_Role AS usr
            INNER JOIN dbo.USER_INFO AS ui
                  ON usr.User_Info_Id = ui.USER_INFO_ID
            INNER JOIN dbo.CLIENT cl
                  ON cl.CLIENT_ID = ui.CLIENT_ID
            LEFT JOIN @SSO_Email_Notification_Client ssenc
                  ON ssenc.Client_Id = cl.CLIENT_ID
      WHERE
            usr.Security_Role_Id = @Security_Role_Id
            AND ui.New_User_EMail_Ts IS NULL;



--AND ( ( cl.Is_Single_Signon_Login = 0
--        AND Client_Host_Url IS NULL )
--      OR ( ( cl.Is_Single_Signon_Login = 1
--             OR Client_Host_Url IS NOT NULL )
--           AND EXISTS ( SELECT
--                              1
--                        FROM
--                              @SSO_Email_Notification_Client ssenc
--                        WHERE
--                              ssenc.Client_Id = cl.CLIENT_ID ) ) )

END;

;
GO


GRANT EXECUTE ON  [dbo].[Security_Role_User_Info_Sel] TO [CBMSApplication]
GO
