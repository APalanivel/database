SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsConsumptionConversion_LoadUnits

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@UnitTypeId    	int       	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE PROCEDURE [dbo].[cbmsConsumptionConversion_LoadUnits]
(
	@MyAccountId int
	,@UnitTypeId int = null
)
AS
select entity_id
	,entity_name
	,case entity_type when 101 then 'Electric Power'
			when 102 then 'Natural Gas' end as 'commodity_name'
	,entity_type
from entity
where entity_type = coalesce(@UnitTypeId, entity_type)
and entity_type in (101,102)
order by entity_type, entity_name
GO
GRANT EXECUTE ON  [dbo].[cbmsConsumptionConversion_LoadUnits] TO [CBMSApplication]
GO
