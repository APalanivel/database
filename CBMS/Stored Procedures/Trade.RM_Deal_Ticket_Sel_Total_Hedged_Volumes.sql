SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        Trade.RM_Deal_Ticket_Sel_Total_Hedged_Volumes                      
                        
Description:                        
        To get site's hedge configurations  
                        
Input Parameters:                        
    Name    DataType        Default     Description                          
--------------------------------------------------------------------------------  
 @Client_Id   INT  
    @Commodity_Id  INT  
    @Hedge_Type  INT  
    @Start_Dt DATE  
    @End_Dt  DATE  
    @Contract_Id  VARCHAR(MAX)  
    @Site_Id   INT    NULL  
                        
 Output Parameters:                              
 Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
     
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
 EXEC Trade.RM_Client_Hier_Hedge_Config_Sel 11236  
  
 EXEC Trade.Contract_Sel_By_Commodity_Hedge_Type_Period @Client_Id = 11236,@Commodity_Id = 291  
   ,@Start_Dt='2015-01-01',@End_Dt='2017-12-30',@Hedge_Type=586  
    
 EXEC Trade.RM_Deal_Ticket_Sel_Total_Hedged_Volumes  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2019-01-01',@End_Dt='2019-01-01',@Hedge_Type=586,@Site_Str = '24401'  
 EXEC Trade.RM_Deal_Ticket_Sel_Total_Hedged_Volumes  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2019-01-01',@End_Dt='2019-01-01',@Hedge_Type=586,@Contract_Id = '148361'  
 EXEC Trade.RM_Deal_Ticket_Sel_Total_Hedged_Volumes  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2019-01-01',@End_Dt='2019-01-01',@Hedge_Type=587,@Site_Str='24401' ,@RM_Group_Id='730',@Division_Id='1928'   
    
 EXEC Trade.RM_Deal_Ticket_Sel_Total_Hedged_Volumes  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2019-08-01',@End_Dt='2019-12-01',@Hedge_Type=586,@Site_Str = '24401,24415'  
 EXEC Trade.RM_Deal_Ticket_Sel_Total_Hedged_Volumes  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2019-08-01',@End_Dt='2019-12-01',@Hedge_Type=586,@Contract_Id = '148361'  
 EXEC Trade.RM_Deal_Ticket_Sel_Total_Hedged_Volumes  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2019-08-01',@End_Dt='2019-12-01',@Hedge_Type=587,@Site_Str='24401' ,@RM_Group_Id='730',@Division_Id='1928'   
  
Author Initials:                        
    Initials    Name                        
--------------------------------------------------------------------------------  
    RR          Raghu Reddy     
                         
 Modifications:                        
    Initials Date           Modification                        
--------------------------------------------------------------------------------  
 PR          02-11-2018    Created GRM  
                       
******/
CREATE PROCEDURE [Trade].[RM_Deal_Ticket_Sel_Total_Hedged_Volumes]
(
    @Client_Id INT,
    @Commodity_Id INT,
    @Hedge_Type INT,
    @Start_Dt DATE,
    @End_Dt DATE,
    @Contract_Id VARCHAR(MAX) = NULL,
    @Uom_Id INT = NULL,
    @Site_Str VARCHAR(MAX) = NULL,
    @Division_Id VARCHAR(MAX) = NULL,
    @RM_Group_Id VARCHAR(MAX) = NULL
)
AS
BEGIN
    SET NOCOUNT ON;

    CREATE TABLE #Volumes
    (
        Client_Hier_Id INT,
        Account_Id INT,
        Service_Month DATE,
        Forecast_Volume DECIMAL(28, 0),
        Uom_Id INT,
        Deal_Ticket_Id INT
    );

    DECLARE @Common_Uom_Id INT;

    DECLARE @Hedge_Type_Input VARCHAR(200);

    SELECT @Hedge_Type_Input = ENTITY_NAME
    FROM dbo.ENTITY
    WHERE ENTITY_ID = @Hedge_Type;


    CREATE TABLE #RM_Group_Sites
    (
        [Site_Id] INT
    );

    INSERT INTO #RM_Group_Sites
    (
        Site_Id
    )
    SELECT ch.Site_Id
    FROM dbo.Cbms_Sitegroup_Participant rgd
        INNER JOIN dbo.Code grp
          ON grp.Code_Id = rgd.Group_Participant_Type_Cd
        INNER JOIN dbo.CONTRACT c
            ON c.CONTRACT_ID = rgd.Group_Participant_Id
        INNER JOIN Core.Client_Hier_Account chasupp
            ON chasupp.Supplier_Contract_ID = c.CONTRACT_ID
        INNER JOIN Core.Client_Hier_Account chautil
            ON chasupp.Meter_Id = chautil.Meter_Id
        INNER JOIN Core.Client_Hier ch
            ON ch.Client_Hier_Id = chasupp.Client_Hier_Id
    WHERE (
              EXISTS
    (
        SELECT 1
        FROM dbo.ufn_split(@RM_Group_Id, ',') con
        WHERE rgd.Cbms_Sitegroup_Id = CAST(Segments AS INT)
    )
              AND grp.Code_Value = 'Contract'
              AND chasupp.Account_Type = 'Supplier'
              AND chautil.Account_Type = 'Utility'
          );

    INSERT INTO #RM_Group_Sites
    (
        Site_Id
    )
    SELECT ch.Site_Id
    FROM dbo.Cbms_Sitegroup_Participant rgd
        INNER JOIN dbo.Code grp
            ON grp.Code_Id = rgd.Group_Participant_Type_Cd
        INNER JOIN Core.Client_Hier ch
            ON ch.Site_Id = rgd.Group_Participant_Id
    WHERE (
              EXISTS
    (
        SELECT 1
        FROM dbo.ufn_split(@RM_Group_Id, ',') con
        WHERE rgd.Cbms_Sitegroup_Id = CAST(Segments AS INT)
    )
              AND grp.Code_Value = 'Site'
          );

    INSERT INTO #RM_Group_Sites
    (
        Site_Id
    )
    SELECT CAST(Segments AS INT)
    FROM dbo.ufn_split(@Site_Str, ',') con;

    INSERT INTO #RM_Group_Sites
    (
        Site_Id
    )
    SELECT ch.Site_Id
    FROM Core.Client_Hier ch
    WHERE ch.Site_Id > 0
          AND (EXISTS
    (
        SELECT 1
        FROM dbo.ufn_split(@Division_Id, ',') con
        WHERE ch.Sitegroup_Id = CAST(Segments AS INT)
    )
              );


    ----------------------To get site own config  
    INSERT INTO #Volumes
    (
        Client_Hier_Id,
        Account_Id,
        Service_Month,
        Forecast_Volume,
        Uom_Id,
        Deal_Ticket_Id
    )
    SELECT dtv.Client_Hier_Id,
           dtv.Account_Id,
           dtv.Deal_Month,
           dtv.Total_Volume * cuc.CONVERSION_FACTOR AS Forecast_Volume,
           ISNULL(chob.RM_Forecast_UOM_Type_Id, dtv.Uom_Type_Id) AS Uom_Id,
           dtv.Deal_Ticket_Id
    FROM Core.Client_Hier ch
        INNER JOIN Trade.RM_Client_Hier_Onboard chob
            ON ch.Client_Hier_Id = chob.Client_Hier_Id
        INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
            ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
        INNER JOIN dbo.ENTITY et
            ON et.ENTITY_ID = chhc.Hedge_Type_Id
        CROSS JOIN meta.DATE_DIM dd
        LEFT JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtv
            ON ch.Client_Hier_Id = dtv.Client_Hier_Id
        LEFT JOIN Trade.Deal_Ticket dt
            ON dtv.Deal_Ticket_Id = dt.Deal_Ticket_Id
               AND dt.Commodity_Id = chob.Commodity_Id
        INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
            ON cuc.BASE_UNIT_ID = dtv.Uom_Type_Id
               AND cuc.CONVERTED_UNIT_ID = ISNULL(chob.RM_Forecast_UOM_Type_Id, dtv.Uom_Type_Id)
    WHERE ch.Site_Id > 0
          AND ch.Client_Id = @Client_Id
          AND chob.Commodity_Id = @Commodity_Id
          AND
          (
              @Start_Dt
          BETWEEN chhc.Config_Start_Dt AND chhc.Config_End_Dt
              OR @End_Dt
          BETWEEN chhc.Config_Start_Dt AND chhc.Config_End_Dt
              OR chhc.Config_Start_Dt
          BETWEEN @Start_Dt AND @End_Dt
              OR chhc.Config_End_Dt
          BETWEEN @Start_Dt AND @End_Dt
          )
          AND
          (
              (
                  @Hedge_Type_Input = 'Physical'
                  AND et.ENTITY_NAME IN ( 'Physical', 'Physical & Financial' )
              )
              OR
              (
                  @Hedge_Type_Input = 'Financial'
    AND et.ENTITY_NAME IN ( 'Financial', 'Physical & Financial' )
              )
          )
          AND
          (
              EXISTS
    (
        SELECT 1 FROM #RM_Group_Sites rgs WHERE rgs.Site_Id = ch.Site_Id
    )
              OR EXISTS
    (
        SELECT 1
        FROM Core.Client_Hier_Account cha
            INNER JOIN dbo.ufn_split(@Contract_Id, ',') con
                ON cha.Supplier_Contract_ID = CAST(Segments AS INT)
        WHERE cha.Client_Hier_Id = ch.Client_Hier_Id
    )
          )
          --AND ( @Site_Str IS NULL  
          --      OR EXISTS ( SELECT  
          --                        1  
          --                  FROM  
          --                        dbo.ufn_split(@Site_Str, ',')  
          --                  WHERE  
          --                        CAST(Segments AS INT) = ch.Site_Id ) )  
          --AND ( @Division_Id IS NULL  
          --      OR EXISTS ( SELECT  
          --                        1  
          --                  FROM  
          --                        dbo.ufn_split(@Division_Id, ',')  
          --                  WHERE  
          --                        CAST(Segments AS INT) = ch.Sitegroup_Id ) )  
          --AND ( @RM_Group_Id IS NULL  
          --      OR EXISTS ( SELECT  
          --                        1  
          --                  FROM  
          --                        #RM_Group_Sites rgs  
          --                  WHERE  
          --                        rgs.Site_Id = ch.Site_Id ) )  
          AND dtv.Deal_Month
          BETWEEN @Start_Dt AND @End_Dt
    GROUP BY dtv.Client_Hier_Id,
             dtv.Account_Id,
             dtv.Deal_Month,
             dtv.Total_Volume * cuc.CONVERSION_FACTOR,
             ISNULL(chob.RM_Forecast_UOM_Type_Id, dtv.Uom_Type_Id),
             dtv.Deal_Ticket_Id;



    ----------------------To get default config                         
    INSERT INTO #Volumes
    (
        Client_Hier_Id,
        Account_Id,
        Service_Month,
        Forecast_Volume,
        Uom_Id,
        Deal_Ticket_Id
    )
    SELECT dtv.Client_Hier_Id,
           dtv.Account_Id,
           dtv.Deal_Month,
           dtv.Total_Volume * cuc.CONVERSION_FACTOR AS Forecast_Volume,
           ISNULL(chob.RM_Forecast_UOM_Type_Id, dtv.Uom_Type_Id) AS Uom_Id,
           dtv.Deal_Ticket_Id
    FROM Core.Client_Hier ch
        INNER JOIN Core.Client_Hier chclient
            ON ch.Client_Id = chclient.Client_Id
        INNER JOIN Trade.RM_Client_Hier_Onboard chob
            ON chclient.Client_Hier_Id = chob.Client_Hier_Id
               AND ch.Country_Id = chob.Country_Id
        INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
            ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
        INNER JOIN dbo.ENTITY et
            ON et.ENTITY_ID = chhc.Hedge_Type_Id
        LEFT JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtv
            ON ch.Client_Hier_Id = dtv.Client_Hier_Id
        LEFT JOIN Trade.Deal_Ticket dt
            ON dtv.Deal_Ticket_Id = dt.Deal_Ticket_Id
               AND dt.Commodity_Id = chob.Commodity_Id
        INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
            ON cuc.BASE_UNIT_ID = dtv.Uom_Type_Id
               AND cuc.CONVERTED_UNIT_ID = ISNULL(chob.RM_Forecast_UOM_Type_Id, dtv.Uom_Type_Id)
    WHERE ch.Site_Id > 0
          AND ch.Client_Id = @Client_Id
          AND chclient.Sitegroup_Id = 0
          AND chob.Commodity_Id = @Commodity_Id
          AND
          (
              @Start_Dt
          BETWEEN chhc.Config_Start_Dt AND chhc.Config_End_Dt
              OR @End_Dt
          BETWEEN chhc.Config_Start_Dt AND chhc.Config_End_Dt
              OR chhc.Config_Start_Dt
          BETWEEN @Start_Dt AND @End_Dt
              OR chhc.Config_End_Dt
          BETWEEN @Start_Dt AND @End_Dt
          )
          AND
          (
              (
                  @Hedge_Type_Input = 'Physical'
                  AND et.ENTITY_NAME IN ( 'Physical', 'Physical & Financial' )
              )
              OR
              (
                  @Hedge_Type_Input = 'Financial'
                  AND et.ENTITY_NAME IN ( 'Financial', 'Physical & Financial' )
              )
          )
          AND NOT EXISTS
    (
        SELECT 1
        FROM Trade.RM_Client_Hier_Onboard siteob
        WHERE siteob.Client_Hier_Id = ch.Client_Hier_Id
              AND siteob.Commodity_Id = chob.Commodity_Id
    )
          AND
          (
              EXISTS
    (
        SELECT 1 FROM #RM_Group_Sites rgs WHERE rgs.Site_Id = ch.Site_Id
    )
              OR EXISTS
    (
        SELECT 1
        FROM Core.Client_Hier_Account cha
            INNER JOIN dbo.ufn_split(@Contract_Id, ',') con
                ON cha.Supplier_Contract_ID = CAST(Segments AS INT)
        WHERE cha.Client_Hier_Id = ch.Client_Hier_Id
    )
          )
          --AND ( @Site_Str IS NULL  
          --      OR EXISTS ( SELECT  
          --                        1  
          --                  FROM  
          --                        dbo.ufn_split(@Site_Str, ',')  
          --                  WHERE  
          --                        CAST(Segments AS INT) = ch.Site_Id ) )  
          --AND ( @Division_Id IS NULL  
          --      OR EXISTS ( SELECT  
          --                        1  
          --                  FROM  
          --                        dbo.ufn_split(@Division_Id, ',')  
          --                  WHERE  
          --                        CAST(Segments AS INT) = ch.Sitegroup_Id ) )  
          --AND ( @RM_Group_Id IS NULL  
          --      OR EXISTS ( SELECT  
          --                        1  
          --                  FROM  
          --                        #RM_Group_Sites rgs  
          --                  WHERE  
          --                        rgs.Site_Id = ch.Site_Id ) )  
          AND dtv.Deal_Month
          BETWEEN @Start_Dt AND @End_Dt
    GROUP BY dtv.Client_Hier_Id,
             dtv.Account_Id,
             dtv.Deal_Month,
             dtv.Total_Volume * cuc.CONVERSION_FACTOR,
             ISNULL(chob.RM_Forecast_UOM_Type_Id, dtv.Uom_Type_Id),
             dtv.Deal_Ticket_Id;

    SELECT @Common_Uom_Id = MAX(Uom_Id)
    FROM #Volumes;

    SELECT dd.DATE_D AS Service_Month,
           CAST(ISNULL(SUM(vol.Forecast_Volume) * cuc.CONVERSION_FACTOR, 0) AS DECIMAL(28, 0)) AS Volume_Hedged
    FROM meta.DATE_DIM dd
        LEFT JOIN #Volumes vol
            ON dd.DATE_D = vol.Service_Month
        LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
            ON cuc.BASE_UNIT_ID = vol.Uom_Id
               AND cuc.CONVERTED_UNIT_ID = ISNULL(@Uom_Id, @Common_Uom_Id)
    WHERE dd.DATE_D
    BETWEEN @Start_Dt AND @End_Dt
    GROUP BY dd.DATE_D,
             cuc.CONVERSION_FACTOR;

END;
GO
GRANT EXECUTE ON  [Trade].[RM_Deal_Ticket_Sel_Total_Hedged_Volumes] TO [CBMSApplication]
GO
