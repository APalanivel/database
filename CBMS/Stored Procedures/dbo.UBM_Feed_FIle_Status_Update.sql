SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 dbo.UBM_Feed_FIle_Status_Update
  
DESCRIPTION:  
  
To update the file status
  
INPUT PARAMETERS:  
 Name						DataType		Default      Description  
---------------------------------------------------------------------------                
@UBM_BATCH_MASTER_LOG_ID	INT
@fileStatus					VARCHAR(200) 
   
OUTPUT PARAMETERS:  
 Name						DataType			Default			Description  
-----------------------------------------------------------------------------  


 
AUTHOR INITIALS:  
 Initials	 Name  
------------------------------------------------------------  
 RKV		Ravi Kumar vegesna
 
MODIFICATIONS  
  
 Initials	Date		Modification  
------------------------------------------------------------  
 RKV		2015-09-15	Created
******/  
CREATE  PROCEDURE [dbo].[UBM_Feed_FIle_Status_Update]
      @UBM_BATCH_MASTER_LOG_ID INT
     ,@fileStatus VARCHAR(200)
AS 
BEGIN  
      SET nocount ON
      DECLARE @fileStatusTypeId INT

      SELECT
            @fileStatusTypeId = entity_id
      FROM
            entity
      WHERE
            entity_name = @fileStatus
            AND entity_type = 656

      UPDATE
            UBM_FEED_FILE_MAP
      SET   
            STATUS_TYPE_ID = @fileStatusTypeId
      WHERE
            UBM_BATCH_MASTER_LOG_ID = @UBM_BATCH_MASTER_LOG_ID
                  
END

;
GO
GRANT EXECUTE ON  [dbo].[UBM_Feed_FIle_Status_Update] TO [CBMSApplication]
GO
