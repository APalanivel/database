SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SAVE_NYMEX_XML_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@batch_execution_date	datetime  	          	
	@nymex_xml     	text      	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE dbo.SAVE_NYMEX_XML_P	
	
	@batch_execution_date datetime,
	@nymex_xml text
	
	AS
set nocount on
	insert into rm_nymex_xml values(@batch_execution_date, @nymex_xml)
GO
GRANT EXECUTE ON  [dbo].[SAVE_NYMEX_XML_P] TO [CBMSApplication]
GO
