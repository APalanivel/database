
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
  dbo.Report_DE_Get_Client_Site_Account_Details_For_Prokarma_Upload  
    
DESCRIPTION:  
 Query to Get the Client,Site,Account Details (leaving out AT&T US And Canada accounts)  
 Used by batch process  
  
INPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
  
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
  
EXEC dbo.Report_DE_Get_Client_Site_Account_Details_For_Prokarma_Upload  
  
  
AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 AKR		Ashok Kumar Raju  
 HG			Harihara Suthan Ganesan  
  
MODIFICATIONS:  
 Initials	Date		Modification  
------------------------------------------------------------  
 AKR		2012-05-22  Cloned from the select statement of dts job "ProKarmaUpload - Accounts.csv"  
 HG			2013-07-03	MAINT-2022, modified the logic to exclude the ATT accounts from USA and Canada and all other accounts   
 HG			2014-02-10	MAINT-2358, returning Display_Account_Number to show the supplier account dates along with the account number  
							GROUP BY Caluse added to eliminate the duplicate records (multiple meters for account)  
							LEFT OUTER JOIN on DO_NOT_TRACK and IS NULL changed to NOT EXISTS as per standards
 HG			2014-02-11	MAINT-2502 , removed Canada as an exclusion for AT&T accounts
 HG			2014-07-28	MAINT-2951, modified to return the supplier account dates after account number instead of the Display Account_Number
 HG			2014-07-31	Modified to return the actual account number for Utility Account and date appended value for Supplier Account
******/
CREATE PROCEDURE [dbo].[Report_DE_Get_Client_Site_Account_Details_For_Prokarma_Upload]
AS 
BEGIN  
  
      SET NOCOUNT ON  
  
      SELECT
            ch.Client_Name AS Client
           ,ISNULL(s.Site_Reference_Number, '') AS [Site Ref No]
           ,RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_Name + ')' AS [Site]
           ,ch.Site_Address_Line1 AS [Address 1]
           ,ISNULL(ch.Site_Address_Line2, '') AS [Address 2]
           ,'' AS [Address 3]
           ,ch.City
           ,ch.State_Name AS [State]
           ,ch.ZipCode AS [Zip Code]
           ,ch.Country_Name AS [Country]
           ,CASE WHEN cha.Account_Type = 'Utility' THEN cha.Account_Number
                 ELSE ISNULL(cha.Account_Number, 'Blank for ' + cha.Account_Vendor_Name) + ' (' + CONVERT(VARCHAR, cha.Supplier_Account_Begin_Dt, 101) + '-' + CONVERT(VARCHAR, cha.Supplier_Account_End_Dt, 101) + ')'
            END AS [Account]
           ,cha.Account_Id AS [Account Id]
           ,cha.Account_Vendor_Name AS [Vendor]
           ,cha.Account_Type AS [Account Type]
           ,( CASE WHEN cha.Account_Group_Id IS NULL THEN 'No'
                   ELSE 'Yes'
              END ) AS [Group Bill]
      FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account cha
                  ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN dbo.[Site] s
                  ON s.Site_Id = ch.Site_Id
      WHERE
            ( ch.Client_Name <> 'AT&T Services, Inc.'
              OR ( ch.Client_Name = 'AT&T Services, Inc.'
                   AND ch.Country_Name <> 'USA' ) )
            AND cha.Account_Not_Managed = 0
            AND ch.Site_Not_Managed = 0
            AND ch.Division_Not_Managed = 0
            AND ch.Client_Not_Managed = 0
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              dbo.DO_NOT_TRACK dnt
                             WHERE
                              dnt.ACCOUNT_ID = cha.Account_Id )
      GROUP BY
            ch.Client_Name
           ,ISNULL(s.Site_Reference_Number, '')
           ,RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_Name + ')'
           ,ch.Site_Address_Line1
           ,ISNULL(ch.Site_Address_Line2, '')
           ,ch.City
           ,ch.State_Name
           ,ch.ZipCode
           ,ch.Country_Name
           ,CASE WHEN cha.Account_Type = 'Utility' THEN cha.Account_Number
                 ELSE ISNULL(cha.Account_Number, 'Blank for ' + cha.Account_Vendor_Name) + ' (' + CONVERT(VARCHAR, cha.Supplier_Account_Begin_Dt, 101) + '-' + CONVERT(VARCHAR, cha.Supplier_Account_End_Dt, 101) + ')'
            END
           ,cha.Account_Id
           ,cha.Account_Vendor_Name
           ,cha.Account_Type
           ,( CASE WHEN cha.Account_Group_Id IS NULL THEN 'No'
                   ELSE 'Yes'
              END )  
  
END;  
;
GO



GRANT EXECUTE ON  [dbo].[Report_DE_Get_Client_Site_Account_Details_For_Prokarma_Upload] TO [CBMS_SSRS_Reports]
GO
