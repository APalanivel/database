SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_FISCALYEAR_START_MONTH_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@clientId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE dbo.GET_FISCALYEAR_START_MONTH_P
@userId varchar(10),
@sessionId varchar(20),
@clientId int
AS
set nocount on
select entity_name from entity 
where entity_id =(select fiscalyear_startmonth_type_id from client where client_id=@clientId)
GO
GRANT EXECUTE ON  [dbo].[GET_FISCALYEAR_START_MONTH_P] TO [CBMSApplication]
GO
