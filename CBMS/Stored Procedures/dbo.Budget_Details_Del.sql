SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Budget_Details_Del]  
     
DESCRIPTION: 
		It Deletes Budget Detail for Selected Budget Detail Id.
      
INPUT PARAMETERS:          
NAME				DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Budget_Detail_Id	INT				
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------

	BEGIN TRAN

		EXEC Budget_Details_Del 13331

	ROLLBACK TRAN

	SELECT * FROM Budget_Details bd WHERE NOT EXISTS(SELECT 1 FROM Budget_Detail_Comments_Owner_Map m WHERE m.Budget_Detail_id = bd.Budget_Detail_id)

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			26-MAY-10	CREATED

*/

CREATE PROCEDURE dbo.Budget_Details_Del
    (
      @Budget_Detail_Id INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE
		dbo.BUDGET_DETAILS
	WHERE
		BUDGET_DETAIL_ID = @Budget_Detail_Id

END
GO
GRANT EXECUTE ON  [dbo].[Budget_Details_Del] TO [CBMSApplication]
GO
