SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

























/******                              
 
                              
 INPUT PARAMETERS:                
                           
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
@Account_Id			INT
@Commodity_ID      INT               NULL   
@service_month	   Datetime
      
                                  
 OUTPUT PARAMETERS:                
                                 
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
                              
 USAGE EXAMPLES:                                  
---------------------------------------------------------------------------------------------------------------                                  
      
--Commodity Level      
   
DECLARE	@return_value INT



EXEC	@return_value = [dbo].[Get_Variance_Analog_Params_Consumption_ID_Based]
		@Account_Id =1407581 , --1407581 1405819
		@Commodity_Id = 290,
		@Service_Month = N'2017-12-01'

SELECT	'Return Value' = @return_value

GO

  
     
                             
 AUTHOR INITIALS:    
 Arunkumar Palanivel AP          
             
 Initials              Name              
---------------------------------------------------------------------------------------------------------------  
  
                               
 MODIFICATIONS:            
                
 Initials              Date             Modification            
---------------------------------------------------------------------------------------------------------------   
Ap					march 5,2020		THe procedure is created to check if the supporting parameters are same for analagous
******/
CREATE PROCEDURE [dbo].[Get_Variance_Analog_Params_Consumption_ID_Based] 
      (
      @Variance_Consumption_Level_id INT,
	  @Unique_Value INT output ) AS
 
      BEGIN

            SET NOCOUNT ON;

			IF (     (     SELECT
                        count(DISTINCT VESP.Param_Value)
               FROM      dbo.Variance_Consumption_Extraction_Service_Param_Value VESP 
                        JOIN
                        dbo.Variance_Extraction_Service_Param VES
                              ON VES.Variance_Extraction_Service_Param_Id = VESP.Variance_Extraction_Service_Param_Id 
               WHERE    VES.Param_Name = 'Linear_Regression_Sigma_value'
                        AND vesp.Variance_Consumption_Level_Id = @Variance_Consumption_Level_id) = 1 )
   AND (     (     SELECT
                              count(DISTINCT VESP.Param_Value)
                   FROM         dbo.Variance_Consumption_Extraction_Service_Param_Value VESP
                              JOIN
                              dbo.Variance_Extraction_Service_Param VES
                                    ON VES.Variance_Extraction_Service_Param_Id = VESP.Variance_Extraction_Service_Param_Id
                   WHERE      VES.Param_Name = 'Analogous_Account_Distance_value' 
                                AND vesp.Variance_Consumption_Level_Id = @Variance_Consumption_Level_id) = 1 )
   AND (     (     SELECT
                              count(DISTINCT VESP.Param_Value)
                   FROM        
                              dbo.Variance_Consumption_Extraction_Service_Param_Value VESP 
                              JOIN
                              dbo.Variance_Extraction_Service_Param VES
                                    ON VES.Variance_Extraction_Service_Param_Id = VESP.Variance_Extraction_Service_Param_Id 
                   WHERE      VES.Param_Name = 'Analogous_Account_Max_Account_Required'
                                AND vesp.Variance_Consumption_Level_Id = @Variance_Consumption_Level_id) = 1  )
   AND (     (     SELECT
                              count(DISTINCT VESP.Param_Value)
                   FROM        dbo.Variance_Consumption_Extraction_Service_Param_Value VESP 
                              JOIN
                              dbo.Variance_Extraction_Service_Param VES
                                    ON VES.Variance_Extraction_Service_Param_Id = VESP.Variance_Extraction_Service_Param_Id 
                   WHERE      VES.Param_Name = 'Analogous_Account_Min_Account_Required'
                              AND vesp.Variance_Consumption_Level_Id = @Variance_Consumption_Level_id) = 1  )
   AND (     (     SELECT
                              count(DISTINCT VESP.Param_Value)
                   FROM        
                              dbo.Variance_Consumption_Extraction_Service_Param_Value VESP 
                              JOIN
                              dbo.Variance_Extraction_Service_Param VES
                                    ON VES.Variance_Extraction_Service_Param_Id = VESP.Variance_Extraction_Service_Param_Id 
                   WHERE      VES.Param_Name = 'Error_Metric'
                              AND vesp.Variance_Consumption_Level_Id = @Variance_Consumption_Level_id) = 1 ) 
      BEGIN
             
                  SET @Unique_Value =1
      END;
	  RETURN @Unique_Value
      END;

GO
GRANT EXECUTE ON  [dbo].[Get_Variance_Analog_Params_Consumption_ID_Based] TO [CBMSApplication]
GO
