SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO











-- select * from budget_details
CREATE      PROCEDURE dbo.BUDGET_UPDATE_BUDGET_USAGE_DETAILS_P
	@user_id varchar(10),
	@session_id varchar(20),
	@budgetAccountId int,	
	@serviceMonth datetime,
	@budgetUsage decimal(10,2)
	

	AS
	begin
	set nocount on

	declare @cnt int	
	select @cnt = count(budget_detail_id) from  budget_details where budget_account_id = @budgetAccountId 
					and month_identifier = @serviceMonth
	if(@cnt > 0)
	begin
		update 	budget_details 
	        set 	budget_usage = @budgetUsage
		where 	budget_details.budget_account_id = @budgetAccountId
			and budget_details.month_identifier = @serviceMonth
	end
	else
	begin
		
		insert into budget_details(budget_account_id,month_identifier,budget_usage) 
			values(@budgetAccountId,@serviceMonth,@budgetUsage)
	end
	
	
		
	
	
		
	end



-- select * from budget_details










GO
GRANT EXECUTE ON  [dbo].[BUDGET_UPDATE_BUDGET_USAGE_DETAILS_P] TO [CBMSApplication]
GO
