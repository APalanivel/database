SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.EC_Meter_Attribute_Sel_By_State_Account_Meter_Id       
              
Description:              
			This sproc get the attribute details for the given State,Account and Meter.               
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
    @Commodity_Id						INT
    @State_Id							INT        
    
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

   Exec Budget.EC_Meter_Attribute_Sel_By_State_Account_Meter_Id 136,1354499,995488
   Exec Budget.EC_Meter_Attribute_Sel_By_State_Account_Meter_Id 136,1447414,995488
   Exec Budget.EC_Meter_Attribute_Sel_By_State_Account_Meter_Id 236,1354501,995489
   Exec Budget.EC_Meter_Attribute_Sel_By_State_Account_Meter_Id 73,1365759,995489   

Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna               
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2019-05-06		Created For Calculation_Tester.         
             
******/

CREATE PROCEDURE [Budget].[EC_Meter_Attribute_Sel_By_State_Account_Meter_Id]
    (
        @Account_Id INT
        , @Meter_Id INT
        , @State_Id INT = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Commodity_Id INT;

        SELECT
            @Commodity_Id = cha.Commodity_Id
        FROM
            Core.Client_Hier_Account AS cha
        WHERE
            cha.Account_Id = @Account_Id
            AND cha.Meter_Id = @Meter_Id;

        SELECT
            @State_Id = ch.State_Id
        FROM
            Core.Client_Hier_Account AS cha
            INNER JOIN Core.Client_Hier AS ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
        WHERE
            cha.Account_Id = @Account_Id
            AND cha.Meter_Id = @Meter_Id
            AND @State_Id IS NULL
        GROUP BY
            ch.State_Id;

        SELECT
            ema.EC_Meter_Attribute_Id
            , ema.State_Id
            , ema.Commodity_Id
            , ema.EC_Meter_Attribute_Name
            , ema.Attribute_Type_Cd
            , c.Code_Value AS Attribute_Type
            , ema.Is_Used_In_Calc_Vals
        FROM
            dbo.EC_Meter_Attribute ema
            INNER JOIN dbo.Code c
                ON c.Code_Id = ema.Attribute_Type_Cd
        WHERE
            ema.State_Id = @State_Id
            AND ema.Commodity_Id = @Commodity_Id;


    END;
GO
GRANT EXECUTE ON  [Budget].[EC_Meter_Attribute_Sel_By_State_Account_Meter_Id] TO [CBMSApplication]
GO
