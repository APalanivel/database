SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsEuroPricing_GetByIndex

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@price_point   	varchar(200)	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE    PROCEDURE [dbo].[cbmsEuroPricing_GetByIndex]
	( @price_point varchar(200) = null
	)
AS
BEGIN

	   select euro_pricing_id
		, detail_date
		, price_type
		, year
		, interval
		, value
		, price_point
	     from price_point with (nolock)
	    where price_point = @price_point

END
GO
GRANT EXECUTE ON  [dbo].[cbmsEuroPricing_GetByIndex] TO [CBMSApplication]
GO
