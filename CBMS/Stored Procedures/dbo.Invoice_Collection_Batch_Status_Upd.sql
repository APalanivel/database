SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Invoice_Collection_Batch_Status_Upd         
                
Description:                
   This sproc is used to fill the ICQ Batch Table.        
                             
 Input Parameters:                
    Name										DataType   Default   Description                  
--------------------------------------------------------------------------------------                  
                         
   
 Output Parameters:                      
    Name        DataType   Default   Description                  
--------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
--------------------------------------------------------------------------------------     
  exec dbo.Invoice_Collection_Batch_Status_Upd
    
Author Initials:                
    Initials  Name                
--------------------------------------------------------------------------------------                  
 RKV    Ravi Kumar Vegesna  
 Modifications:                
    Initials        Date   Modification                
--------------------------------------------------------------------------------------                  
    RKV    2017-02-03  Created For Invoice_Collection.           
               
******/   
CREATE PROCEDURE [dbo].[Invoice_Collection_Batch_Status_Upd]
      (
       @Status_Cd INT
      ,@Invoice_Collection_Batch_Id INT )
AS
BEGIN  
      SET NOCOUNT ON;  
        
      
      
      UPDATE
            Invoice_Collection_Batch
      SET
            Status_Cd = @Status_Cd
           ,Last_Change_Ts = GETDATE()
      WHERE
            Invoice_Collection_Batch_Id = @Invoice_Collection_Batch_Id;
            
         
      

END;
;
;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Batch_Status_Upd] TO [CBMSApplication]
GO
