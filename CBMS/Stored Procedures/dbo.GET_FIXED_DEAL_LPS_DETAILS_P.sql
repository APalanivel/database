SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.GET_FIXED_DEAL_LPS_DETAILS_P
	@contractId INT,
	@dealEntityName VARCHAR(200),
	@dealEntityType INT,
	@TypeEntityType INT,
	@TypeEntityName VARCHAR(200)
AS
BEGIN
	
	SET NOCOUNT ON

	DECLARE @dealEntityID INT
	
	SELECT @dealEntityID=entity_id FROM Entity 
	WHERE entity_name = @dealEntityName
		AND entity_type = @dealEntityType
	
	SELECT lps.load_profile_specification_id,
		@dealEntityID entity_id,
		@dealEntityName deal_type,
		lps.contract_id,  
		lps.lps_type_id,
		typ.entity_name lps_type,
		lps.month_identifier,
		lps.lps_fixed_price, 
		lps.lps_unit_type_id,
		unit.ENTITY_NAME unit,
		lps.lps_frequency_type_id, 
		FREQ.ENTITY_NAME FREQUENCY,
		lps.lps_expression
	FROM dbo.load_profile_specification lps (NOLOCK) INNER JOIN dbo.ENTITY FREQ (NOLOCK) ON FREQ.ENTITY_ID = lps.LPS_FREQUENCY_TYPE_ID
		INNER JOIN dbo.ENTITY unit (NOLOCK) ON unit.ENTITY_ID = lps.LPS_UNIT_TYPE_ID
		INNER JOIN dbo.entity typ (NOLOCK) ON typ.entity_id = lps.lps_type_id
	WHERE lps.contract_id = @contractId 
		AND lps.price_index_id IS NULL	
		AND typ.entity_type = @TypeEntityType 
		AND typ.entity_name = @TypeEntityName

END
GO
GRANT EXECUTE ON  [dbo].[GET_FIXED_DEAL_LPS_DETAILS_P] TO [CBMSApplication]
GO
