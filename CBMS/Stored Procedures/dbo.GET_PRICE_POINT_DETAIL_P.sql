SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




--exec GET_PRICE_POINT_DETAIL_P 342
--select * from market_price_point where market_price_point_id = 342
CREATE    PROCEDURE dbo.GET_PRICE_POINT_DETAIL_P
@pricePointID int
AS
begin
select 	price_point.market_price_point_id,
	price_point.market_price_point,
	price_point.market_price_point_short_name,
	price_point.is_show_on_dv,
	state.state_id,
	state.state_name
from 	market_price_point price_point
left join    market_price_point_state_map map on map.market_price_point_id = price_point.market_price_point_id 
left join    state state on state.state_id = map.state_id
where	price_point.market_price_point_id = @pricePointID
order by state.state_name
end





GO
GRANT EXECUTE ON  [dbo].[GET_PRICE_POINT_DETAIL_P] TO [CBMSApplication]
GO
