SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[SR_RFP_UPLOAD_MANAGER_APPROVAL_P]
	@userId INT,    
	@sessionId VARCHAR(100),
	@cbmsImageId int,  --added by Jaya
	--@cbmsImageDocId varchar(200),
	--@cbmsImage image,
	--@contentType varchar(200),
	@rfpId INT,
	@accountGroupId INT,
	@isBidGroup INT    
AS
BEGIN

	SET NOCOUNT ON
	
	DECLARE @entityId INT

	SELECT @entityId = Entity_ID FROM dbo.Entity (NOLOCK) WHERE Entity_Name = 'Manager Approval Upload ' AND Entity_Type = 100
    
	/* commneted by Jaya     
	INSERT INTO CBMS_IMAGE (CBMS_IMAGE_TYPE_ID, CBMS_DOC_ID, CBMS_IMAGE, CONTENT_TYPE, DATE_IMAGED)     
	VALUES (@entityId, @cbmsImageDocId, @cbmsImage, @contentType, getDate())    
    
	declare @cbmsImageId int    
	select @cbmsImageId = (select @@Identity)  */    
    
	--added by Jaya for updating entityid
     
	UPDATE dbo.CBMS_IMAGE SET CBMS_IMAGE_TYPE_ID = @entityId WHERE CBMS_IMAGE_ID = @cbmsImageId
    
	INSERT INTO dbo.SR_RFP_MANAGER_APPROVAL(CBMS_IMAGE_ID,SR_ACCOUNT_GROUP_ID,IS_BID_GROUP,UPLOADED_BY,UPLOADED_DATE)
	VALUES ( @cbmsImageId, @accountGroupId, @isBidGroup, @userId, GETDATE())
    
	UPDATE rfpCheckList
		SET IS_MANAGER_APPROVED = 1
	FROM dbo.SR_RFP_CHECKLIST rfpCheckList
		INNER JOIN dbo.SR_RFP_ACCOUNT rfpAcct ON rfpAcct.SR_RFP_ACCOUNT_ID = rfpCheckList.SR_RFP_ACCOUNT_ID
	WHERE rfpAcct.SR_RFP_ACCOUNT_ID = @accountGroupId
		AND rfpAcct.SR_RFP_ID = @rfpId
        
	IF (@isBidGroup = 0)    
	 BEGIN
	 
		UPDATE dbo.SR_RFP_CHECKLIST
			SET IS_MANAGER_APPROVED = 1   
		WHERE SR_RFP_ACCOUNT_ID = @accountGroupId

	 END
	ELSE IF (@isBidGroup > 0 )    
	 BEGIN
	 
		UPDATE rfpCheckList
			SET IS_MANAGER_APPROVED = 1
		FROM dbo.SR_RFP_CHECKLIST rfpCheckList
			INNER JOIN dbo.SR_RFP_ACCOUNT rfpAcct ON rfpAcct.SR_RFP_ACCOUNT_ID = rfpCheckList.SR_RFP_ACCOUNT_ID
		WHERE rfpAcct.SR_RFP_BID_GROUP_ID = @accountGroupId
 
	 END
END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_UPLOAD_MANAGER_APPROVAL_P] TO [CBMSApplication]
GO
