SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.Vendor_Sel_By_Account_Vendor_Type
           
DESCRIPTION:             
			To get vendors by service
			
INPUT PARAMETERS:            
	Name				DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Commodity_Id		INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT DISTINCT Commodity_Id FROM Core.Client_Hier_Account WHERE Account_Id = 688949

	EXEC dbo.Vendor_Sel_By_Account_Vendor_Type 1
		     
	EXEC dbo.Vendor_Sel_By_Account_Vendor_Type 688949
	EXEC dbo.Vendor_Sel_By_Account_Vendor_Type 688949,'Blue'
	EXEC dbo.Vendor_Sel_By_Account_Vendor_Type 688949,NULL,'Supplier'
	EXEC dbo.Vendor_Sel_By_Account_Vendor_Type 688949,'Blue','Supplier'

	
		
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-02-14	Contract placeholder - CP-8 Created
******/

CREATE PROCEDURE [dbo].[Vendor_Sel_By_Account_Vendor_Type]
      ( 
       @Account_Id INT
      ,@Keyword VARCHAR(200) = NULL
      ,@Vendor_Type VARCHAR(200) = NULL )
AS 
BEGIN

      SET NOCOUNT ON;
      
      SELECT
            vndr.VENDOR_ID
           ,vndr.VENDOR_NAME
      FROM
            Core.Client_Hier_Account cha
            INNER JOIN dbo.VENDOR_COMMODITY_MAP vcm
                  ON cha.Commodity_Id = vcm.COMMODITY_TYPE_ID
            INNER JOIN dbo.VENDOR vndr
                  ON vcm.VENDOR_ID = vndr.VENDOR_ID
            INNER JOIN dbo.ENTITY ventyp
                  ON vndr.VENDOR_TYPE_ID = ventyp.ENTITY_ID
      WHERE
            vndr.IS_HISTORY = 0
            AND cha.Account_Id = @Account_Id
            AND ( @Keyword IS NULL
                  OR vndr.VENDOR_NAME LIKE +'%' + @Keyword + '%' )
            AND ( @Vendor_Type IS NULL
                  OR ventyp.ENTITY_NAME = @Vendor_Type )
      GROUP BY
            vndr.VENDOR_ID
           ,vndr.VENDOR_NAME
      
END;
;
GO
GRANT EXECUTE ON  [dbo].[Vendor_Sel_By_Account_Vendor_Type] TO [CBMSApplication]
GO
