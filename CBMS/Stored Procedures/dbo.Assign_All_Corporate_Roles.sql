SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/******************************************************************************************************    
NAME :	dbo.Assign_All_Corporate_Roles
   
DESCRIPTION: This procedure used to Insert record into USER_SECURITY_ROLE_QUEUE  table 
   
 INPUT PARAMETERS:    
 Name             DataType               Default        Description    
--------------------------------------------------------------------      
 @UserInfoId        INT                                User ID 
    
 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
--------------------------------------------------------------------    
  USAGE EXAMPLES:    
--------------------------------------------------------------------    
   
  
AUTHOR INITIALS:    
 Initials Name    
-------------------------------------------------------------------     
 KVK K Vinay Kumar
 
 
 MODIFICATIONS     
 Initials Date  Modification    
--------------------------------------------------------------------
KVK 12/15/2010 As we are removing USER_SECURITY_ROLE_QUEUE table and when the user is not external user
			   insert the user with role into  User_Security_Role table  
******************************************************************************************************/
CREATE PROCEDURE [dbo].[Assign_All_Corporate_Roles]
(
	@UserInfoId INT
)
AS
BEGIN

	SET NOCOUNT ON
	
	INSERT INTO dbo.User_Security_Role(
										User_Info_Id
									  ,	Security_Role_Id	
									  )
								 SELECT @UserInfoId
								 ,	    security_role_id 
								 FROM dbo.Security_Role
								 WHERE Is_corporate = 1

END
GO
GRANT EXECUTE ON  [dbo].[Assign_All_Corporate_Roles] TO [CBMSApplication]
GO
