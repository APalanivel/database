SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE     PROCEDURE [dbo].[cbmsInvProKarmaNotReported_Get]
	( @MyAccountId int 
	, @inv_prokarma_not_reported_id int
	)
AS
BEGIN

	   select inv_prokarma_not_reported_id
		, inv_prokarma_not_reported_batch_id
		, source_file
		, inv_sourced_image_id
		, client_name
		, city_state
		, vendor_name
		, account_number
		, bill_month
		, barcode
		, bill_status
		, remarks
	     from inv_prokarma_not_reported
	    where inv_prokarma_not_reported_id = @inv_prokarma_not_reported_id


END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvProKarmaNotReported_Get] TO [CBMSApplication]
GO
