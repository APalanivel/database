SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.CREATE_DEAL_TICKET_VOLUME_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@siteId        	int       	          	
	@dealTicketDetailsId	int       	          	
	@hedgeVolume   	decimal(32,16)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE     PROCEDURE dbo.CREATE_DEAL_TICKET_VOLUME_DETAILS_P  

@userId varchar,
@sessionId varchar,
@siteId int, 
@dealTicketDetailsId int, 
@hedgeVolume decimal(32,16)

AS
begin
	set nocount on
	INSERT INTO RM_DEAL_TICKET_VOLUME_DETAILS 
	(SITE_ID, RM_DEAL_TICKET_DETAILS_ID, HEDGE_VOLUME)
	VALUES
	(@siteId, @dealTicketDetailsId, @hedgeVolume)
end
GO
GRANT EXECUTE ON  [dbo].[CREATE_DEAL_TICKET_VOLUME_DETAILS_P] TO [CBMSApplication]
GO
