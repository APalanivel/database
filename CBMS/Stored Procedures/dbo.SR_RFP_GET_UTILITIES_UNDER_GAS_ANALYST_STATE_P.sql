SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_UTILITIES_UNDER_GAS_ANALYST_STATE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default			Description
--------------------------------------------------------------------------------
	@stateId       	int       	          	
	@analystId     	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default			Description
--------------------------------------------------------------------------------

USAGE EXAMPLES:
--------------------------------------------------------------------------------

Exec SR_RFP_GET_UTILITIES_UNDER_GAS_ANALYST_STATE_P 49,30947

Exec SR_RFP_GET_UTILITIES_UNDER_GAS_ANALYST_STATE_P 23,33786

Exec SR_RFP_GET_UTILITIES_UNDER_GAS_ANALYST_STATE_P 13,18900

Exec SR_RFP_GET_UTILITIES_UNDER_GAS_ANALYST_STATE_P 131,35697



AUTHOR INITIALS:
	Initials	Name
--------------------------------------------------------------------------------
	NR			Narayana Reddy

MODIFICATIONS

	Initials	Date			Modification
--------------------------------------------------------------------------------
	        	9/21/2010		Modify Quoted Identifier
 DMR			09/10/2010		Modified for Quoted_Identifier
 NR				2015-09-14		MAINT-3067 Removed legacy view Utility-Analyst-Map(read "-" as "_").
 NR				2020-05-06		SE2017-952 - Added Vendor_Name in Order by clause.


******/

CREATE PROCEDURE [dbo].[SR_RFP_GET_UTILITIES_UNDER_GAS_ANALYST_STATE_P]
    (
        @stateId INT
        , @analystId INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Vendor_Type_Id INT
            , @Commodity_Id INT;

        SELECT
            @Vendor_Type_Id = e.ENTITY_ID
        FROM
            dbo.ENTITY e
        WHERE
            e.ENTITY_TYPE = 155
            AND e.ENTITY_NAME = 'Utility';

        SELECT
            @Commodity_Id = c.Commodity_Id
        FROM
            dbo.Commodity c
        WHERE
            c.Commodity_Name = 'Natural Gas';

        SELECT
            v.VENDOR_ID
            , v.VENDOR_NAME
        FROM
            dbo.VENDOR v
            INNER JOIN dbo.UTILITY_DETAIL ud
                ON v.VENDOR_ID = ud.VENDOR_ID
            INNER JOIN dbo.VENDOR_STATE_MAP vsm
                ON ud.VENDOR_ID = vsm.VENDOR_ID
            INNER JOIN dbo.VENDOR_COMMODITY_MAP vcmp
                ON vcmp.VENDOR_ID = v.VENDOR_ID
            INNER JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                ON vcam.Vendor_Commodity_Map_Id = vcmp.VENDOR_COMMODITY_MAP_ID
        WHERE
            v.VENDOR_TYPE_ID = @Vendor_Type_Id
            AND vcmp.COMMODITY_TYPE_ID = @Commodity_Id
            AND vsm.STATE_ID = @stateId
            AND vcam.Analyst_Id = @analystId
        ORDER BY
            v.VENDOR_NAME;
    END;

GO

GRANT EXECUTE ON  [dbo].[SR_RFP_GET_UTILITIES_UNDER_GAS_ANALYST_STATE_P] TO [CBMSApplication]
GO
