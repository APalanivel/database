SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.Rm_Budget_Site_Default_Budget_Config_Del                        
                          
Description:                          
        To get market price and forecast pirce if a selected index   
                          
Input Parameters:                          
    Name    DataType        Default     Description                            
--------------------------------------------------------------------------------    
	@Index_Id   INT    
    @Start_Dt	Date    
	@End_Dt		Date
                          
 Output Parameters:                                
	Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
Usage Examples:                              
--------------------------------------------------------------------------------    
	SELECT * FROM dbo.ENTITY e WHERE e.ENTITY_TYPE=272

	EXEC Trade.Rm_Budget_Site_Default_Budget_Config_Del 584,1005,291,NULL,'2020-04-01','2020-05-01','All Sites'

    
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR			2019-11-29	RM-Budgets Enahancement - Created
	                
******/
CREATE PROCEDURE [Trade].[Rm_Budget_Site_Default_Budget_Config_Del]
    (
        @Rm_Budget_Site_Default_Budget_Config_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DELETE  FROM
        Trade.Rm_Budget_Site_Default_Budget_Config
        WHERE
            Rm_Budget_Site_Default_Budget_Config_Id = @Rm_Budget_Site_Default_Budget_Config_Id;



    END;
GO
GRANT EXECUTE ON  [Trade].[Rm_Budget_Site_Default_Budget_Config_Del] TO [CBMSApplication]
GO
