SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:    dbo.Contract_Meter_Volume_Dtl_Upd 
     
      
DESCRIPTION:     
     
      
INPUT PARAMETERS:      
      Name          DataType       Default        Description      
-----------------------------------------------------------------------------      
   
    
      
OUTPUT PARAMETERS:    
      
 Name     DataType   Default  Description      
-----------------------------------------------------------------------------      
      
      
      
USAGE EXAMPLES:      
-----------------------------------------------------------------------------      
   
	Exec dbo.Contract_Meter_Volume_Dtl_Upd NULL,1005,290,'2020-01-01','2020-02-01','2818,2815',NULL
  
   
AUTHOR INITIALS:       
	Initials    Name  
-----------------------------------------------------------------------------         
	RR			Raghu Reddy
      
MODIFICATIONS       
	Initials    Date		Modification        
-----------------------------------------------------------------------------         
	RR			2020-04-01	GRM - Contract volumes enhancement - Created
******/
CREATE PROCEDURE [dbo].[Contract_Meter_Volume_Dtl_Upd]
    (
        @Meter_Id INT
        , @Contract_Id INT
        , @Month_Identifier DATETIME
        , @Volume DECIMAL(32, 16)
        , @Bucket_Master_Id INT
        , @Is_Edited BIT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @CONTRACT_METER_VOLUME_ID INT;

        SELECT
            @CONTRACT_METER_VOLUME_ID = cmv.CONTRACT_METER_VOLUME_ID
        FROM
            dbo.CONTRACT_METER_VOLUME cmv
        WHERE
            cmv.METER_ID = @Meter_Id
            AND cmv.CONTRACT_ID = @Contract_Id
            AND cmv.MONTH_IDENTIFIER = @Month_Identifier;

        --UPDATE
        --    dbo.Contract_Meter_Volume_Dtl
        --SET
        --    Volume = @Volume
        --    , Is_Edited = ISNULL(@Is_Edited, 0)
        --WHERE
        --    CONTRACT_METER_VOLUME_ID = @CONTRACT_METER_VOLUME_ID
        --    AND Bucket_Master_Id = @Bucket_Master_Id;

        MERGE INTO dbo.Contract_Meter_Volume_Dtl tgt
        USING
        (   SELECT
                @CONTRACT_METER_VOLUME_ID AS CONTRACT_METER_VOLUME_ID
                , @Bucket_Master_Id AS Bucket_Master_Id
                , @Volume AS Volume
                , ISNULL(@Is_Edited, 0) AS Is_Edited) src
        ON tgt.CONTRACT_METER_VOLUME_ID = src.CONTRACT_METER_VOLUME_ID
           AND  tgt.Bucket_Master_Id = src.Bucket_Master_Id
        WHEN MATCHED THEN UPDATE SET
                              tgt.Volume = src.Volume
                              , tgt.Is_Edited = src.Is_Edited
                              , tgt.Last_Change_Ts = GETDATE()
        WHEN NOT MATCHED THEN INSERT (CONTRACT_METER_VOLUME_ID
                                      , Bucket_Master_Id
                                      , Volume
                                      , Is_Edited
                                      , Created_User_Id
                                      , Created_Ts
                                      , Updated_User_Id
                                      , Last_Change_Ts)
                              VALUES
                                  (src.CONTRACT_METER_VOLUME_ID
                                   , src.Bucket_Master_Id
                                   , src.Volume
                                   , src.Is_Edited
                                   , NULL
                                   , GETDATE()
                                   , NULL
                                   , GETDATE());



    END;
GO
GRANT EXECUTE ON  [dbo].[Contract_Meter_Volume_Dtl_Upd] TO [CBMSApplication]
GO
