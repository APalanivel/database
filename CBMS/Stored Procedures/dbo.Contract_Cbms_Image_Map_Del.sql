SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Contract_Cbms_Image_Map_Del]  

DESCRIPTION: It Deletes Contract cbms Image Map for Selected Contract Id.     
      
INPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Contract_Cbms_Image_Map_Id	INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
	EXEC Contract_Cbms_Image_Map_Del 20140
  Rollback Tran

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			16-JUN-10	CREATED

*/

CREATE PROCEDURE dbo.Contract_Cbms_Image_Map_Del
    (
      @Contract_Cbms_Image_Map_Id INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.CONTRACT_CBMS_IMAGE_MAP
	WHERE
		Contract_Cbms_Image_Map_Id = @Contract_Cbms_Image_Map_Id

END
GO
GRANT EXECUTE ON  [dbo].[Contract_Cbms_Image_Map_Del] TO [CBMSApplication]
GO
