SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Contract_Recalc_Config_Sel

DESCRIPTION:

INPUT PARAMETERS:
	Name						DataType		Default			Description
-----------------------------------------------------------------------------------------
    @Contract_Id					INT					
 
 
OUTPUT PARAMETERS:
	Name						DataType		Default			Description
-----------------------------------------------------------------------------------------
	
USAGE EXAMPLES:
-----------------------------------------------------------------------------------------
    
  EXEC dbo.Contract_Recalc_Config_Sel
      @Contract_Id = 166647	

	  select * from Contract_Recalc_Config
AUTHOR INITIALS:
	Initials	Name
-----------------------------------------------------------------------------------------
	NR			Narayana Reddy	
	
	
MODIFICATIONS
	Initials	Date			Modification
-----------------------------------------------------------------------------------------
    NR				2019-06-26		Created For Add contract.   

******/

CREATE PROCEDURE [dbo].[Contract_Recalc_Config_Sel]
    (
        @Contract_Id INT
        , @Is_Consolidated_Contract_Config BIT = 0
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            acirt.Contract_Recalc_Config_Id
            , acirt.Contract_Id
            , acirt.Determinant_Source_Cd
            , cd.Code_Value AS Determinant_Source
            , c.Code_Id AS Supplier_Recalc_Type_Cd
            , c.Code_Value AS Supplier_Recalc_Type
            , acirt.Start_Dt
            , acirt.End_Dt
            , ct.ED_CONTRACT_NUMBER
            , com.Commodity_Id
            , com.Commodity_Name
            , c.Code_Value + '(' + CONVERT(VARCHAR, acirt.Start_Dt, 106) + ' - '
              + ISNULL(CONVERT(VARCHAR, acirt.End_Dt, 106), 'Open') + ')' AS Display_Recalc_Type
        FROM
            dbo.Contract_Recalc_Config acirt
            INNER JOIN dbo.Code c
                ON acirt.Supplier_Recalc_Type_Cd = c.Code_Id
            INNER JOIN dbo.CONTRACT ct
                ON ct.CONTRACT_ID = acirt.Contract_Id
            INNER JOIN dbo.Commodity com
                ON com.Commodity_Id = acirt.Commodity_Id
            LEFT JOIN dbo.Code cd
                ON acirt.Determinant_Source_Cd = cd.Code_Id
        WHERE
            acirt.Contract_Id = @Contract_Id
            AND acirt.Is_Consolidated_Contract_Config = @Is_Consolidated_Contract_Config
        ORDER BY
            acirt.Start_Dt;

    END;






GO
GRANT EXECUTE ON  [dbo].[Contract_Recalc_Config_Sel] TO [CBMSApplication]
GO
