SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********

--------------------------------------------------------------    
NAME:    
 dbo.ENTITY_INS  
   
DESCRIPTION:    
 Used to insert/update into Enity table and Commodity_Uom_conversion table to associate UOM with Commodity  
    
INPUT PARAMETERS:    
Name					DataType		Default Description    
------------------------------------------------------------    
@Base_Commodity_Id		INT,
@Base_UOM_Cd			INT,
@Converted_Commodity_Id	INT,
@Converted_UOM_Cd		INT,
@Conversion_Factor		DECIMAL(32,16),
@Is_Active				BIT				= 1	
  
OUTPUT PARAMETERS:    
Name   DataType  Default Description    
------------------------------------------------------------ 
   
USAGE EXAMPLES:    
------------------------------------------------------------  
	DECLARE @Entity_id_Out INT
	
	--SELECT * FROM Entity WHere 	Entity_TYpe = 100035
	

	BEGIN TRAN

		EXEC dbo.ENTITY_INS 'Test Unit for Insert Procedure Test', 100035, 'Units for Trash',1000, @Entity_id_Out OUT
		
		SELECT @Entity_Id_Out
		
	ROLLBACK TRAN
  
AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
GB  Geetansu Behera    
CMH Chad Hattabaugh     
HG  Hari  
SKA Shobhit Kr Agrawal  
SS	Subhash Subrmanyam
   
MODIFICATIONS     
Initials	Date		Modification    
------------------------------------------------------------    
SS			09-17-09	Created  
HG			07/02/2010	Out put parameter @Entity_Id added to return the inserted identity value.
						Removed the not exists check as this procedure used only to insert the new uom entries.

******/

CREATE PROC dbo.ENTITY_INS
(
	@ENTITY_NAME			VARCHAR(200)
	,@ENTITY_TYPE			INT
	,@ENTITY_DESCRIPTION	VARCHAR(1000)
	,@DISPLAY_ORDER			INT
	,@Entity_Id				INT OUT
)
AS
BEGIN

	SET NOCOUNT ON

	BEGIN TRY
		BEGIN TRAN

			INSERT INTO dbo.ENTITY (ENTITY_NAME, ENTITY_TYPE, ENTITY_DESCRIPTION, DISPLAY_ORDER )
			VALUES (@ENTITY_NAME, @ENTITY_TYPE, @ENTITY_DESCRIPTION, @DISPLAY_ORDER)

			SET @Entity_Id = SCOPE_IDENTITY()

		COMMIT TRAN

	END TRY
	BEGIN CATCH

		IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRAN
			END

		EXEC dbo.usp_RethrowError

	END CATCH

END

GO
GRANT EXECUTE ON  [dbo].[ENTITY_INS] TO [CBMSApplication]
GO
