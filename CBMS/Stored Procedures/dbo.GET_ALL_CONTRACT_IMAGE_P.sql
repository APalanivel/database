SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE dbo.GET_ALL_CONTRACT_IMAGE_P
	@contract_id INT
AS
BEGIN

	SET NOCOUNT ON
	
	SELECT map.cbms_image_id,
		map.description,
		map.document_type_id,
		map.contract_cbms_image_map_id,
		map.SIGNATURE_TYPE_ID,
		cimage.cbms_doc_id
	FROM dbo.CONTRACT_CBMS_IMAGE_MAP map INNER JOIN dbo.CBMS_IMAGE cimage ON cimage.cbms_image_id = map.cbms_image_id
	WHERE map.contract_id = @contract_id

END
GO
GRANT EXECUTE ON  [dbo].[GET_ALL_CONTRACT_IMAGE_P] TO [CBMSApplication]
GO
