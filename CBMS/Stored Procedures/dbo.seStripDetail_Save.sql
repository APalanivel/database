SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure dbo.seStripDetail_Save
	(
		@StripDetailId int = null
	, @StripHeaderId int
	, @Symbol varchar(10)
	, @Volume int = null
	, @SortDate datetime
	)
AS
BEGIN

	set nocount on
	
	declare @ThisId int
	set @ThisId = @StripDetailId
	
	if @ThisId is null
	begin
	
		insert into seStripDetail
			( StripHeaderId
			, Symbol
			, Volume
			, SortDate
			)
			values
			( @StripHeaderId
			, @Symbol
			, @Volume
			, @SortDate
			)
	
		set @ThisId = @@IDENTITY
	
	end
	else
	begin
	
		 update seStripDetail
		    set StripHeaderId = @StripHeaderId
					, Symbol = @Symbol
					, Volume = @Volume
					, SortDate = @SortDate
			where StripDetailId = @ThisId
	
	end
	exec seStripDetail_Get @ThisId

END
GO
GRANT EXECUTE ON  [dbo].[seStripDetail_Save] TO [CBMSApplication]
GO
