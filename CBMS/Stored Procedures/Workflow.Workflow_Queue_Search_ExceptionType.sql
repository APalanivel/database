SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/******    
NAME:    
[Workflow].[Workflow_Queue_Search_ExceptionType]   
    
DESCRIPTION:    
    
INPUT PARAMETERS:    
 Name   DataType  Default    Description    
--------------------------------------------------------------------------------    
 @MyAccountId    INT                       
    
OUTPUT PARAMETERS:    
 Name   DataType  Default    Description    
--------------------------------------------------------------------------------    
    
USAGE EXAMPLES:    
--------------------------------------------------------------------------------    
    
EXEC [Workflow].[Workflow_Queue_Search_ExceptionType]    
EXEC [Workflow].[Workflow_Queue_Search_ExceptionType] NULL,NULL,1,25    
    
    
AUTHOR INITIALS:    
 Initials   Name    
--------------------------------------------------------------------------------    
 NR     Narayana Reddy    
 TRK    Ramakrishna Thummala 
 AP		ARUNKUMAR PALANIVEL  
MODIFICATIONS    
    
 Initials Date  Modification    
--------------------------------------------------------------------------------    
          9/21/2010 Modify Quoted Identifier    
 NR   14-08-2018 DATA2.0 - Added IS_Active Filter condition.    
 NR   2018-10-08 DATA2.0 - Phase-2 - Added Individual_User_Info_Id,Username columns. 
 TRK  SEP-2019 Migrated to Workflow Schema.   
 AP OCT 25 2019	MODIFIED FOR TICKET D20 - 1405
******/    
 
      
CREATE PROCEDURE [Workflow].[Workflow_Queue_Search_ExceptionType]    
    (      
        @MyAccountId INT = NULL,      
  @Key_Word varchar(200) = null,      
  @Start_Index INT = 1,      
  @End_Index INT = 2147483647      
    )      
AS      
    BEGIN      
      
      
      
   ;WITH CTE_CuExce      
   AS      
   (      
   SELECT      
            cet.CU_EXCEPTION_TYPE_ID,       
   cet.EXCEPTION_TYPE,      
   ROW_NUMBER() OVER (ORDER BY  cet.EXCEPTION_TYPE) AS Row_Num      
        FROM      
            dbo.CU_EXCEPTION_TYPE cet WITH (NOLOCK)      
            LEFT JOIN dbo.USER_INFO ui      
                ON ui.USER_INFO_ID = cet.Managed_By_User_Info_Id      
        WHERE      
            cet.Is_Active = 1  AND cet.EXCEPTION_TYPE  Like '%' + isNull(@Key_Word, cet.EXCEPTION_TYPE ) + '%'  
			and cet.EXCEPTION_TYPE not like '%FAILED RECALC%'    
   )      
   SELECT       
   CCE.CU_EXCEPTION_TYPE_ID,       
   CCE.EXCEPTION_TYPE FROM CTE_CuExce CCE WHERE Row_Num BETWEEN @Start_Index  AND     @End_Index      
          
  ORDER BY EXCEPTION_TYPE      
      
      
    END     
GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_Search_ExceptionType] TO [CBMSApplication]
GO
