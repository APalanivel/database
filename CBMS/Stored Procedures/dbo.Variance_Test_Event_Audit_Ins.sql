
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
Name:    
	dbo.Variance_Test_Event_Audit_Ins  
   
 Description:    
	To save the data into Variance_Test_Event_Audit table

 Input Parameters:    
    Name							DataType      Default		Description    
------------------------------------------------------------------------    
    @Account_Id						INT   
    @Service_Month					DATE
    @Event_Type_Name				VARCHAR(25)
    @Variance_Routed_To_Queue_Id	INT			
    @Event_By_User_Id				INT

 Output Parameters:
	Name						DataType      Default		Description
------------------------------------------------------------------------

 Usage Examples:  
------------------------------------------------------------------------

 BEGIN TRAN

	EXEC Variance_Test_Event_Audit_Ins
       @Account_Id = 6959
      ,@Service_Month = '2014-10-01'
      ,@Event_Type_Name = 'C/U Variance Tested'
      ,@Variance_Routed_To_Queue_Id = 16
      ,@Event_By_User_Id = 16
	
	SELECT * FROM Variance_Test_Event_Audit WHERE Account_Id = 6959 AND Service_Month = '2014-11-01'

 ROLLBACK TRAN
 
 
 SELECT TOP 10 * FROM Variance_Log
    

Author Initials:
	Initials	Name  
------------------------------------------------------------  
	HG			Harihara Suthan G
	RR			Raghu Reddy
   
Modifications :    
	Initials	Date	    Modification    
------------------------------------------------------------    
	HG			2014-07-24	Created
	RR			2014-09-17	Created MAINT-3040 Moving non-process dependent auditing to queued update to help performance

 ******/
CREATE PROCEDURE [dbo].[Variance_Test_Event_Audit_Ins]
      ( 
       @Account_Id INT
      ,@Service_Month DATE
      ,@Event_Type_Name VARCHAR(25)
      ,@Variance_Routed_To_Queue_Id INT
      ,@Event_By_User_Id INT )
AS 
BEGIN

      SET NOCOUNT ON
      DECLARE
            @Variance_Test_Event_Type_Cd INT
           ,@Variance_Routed_To_User_Id INT
		
      SELECT
            @Variance_Test_Event_Type_Cd = cd.Code_Id
      FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code cd
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Std_Column_Name = 'Variance_Test_Event_Type_Cd'
            AND cd.Code_Value = @Event_Type_Name

      SELECT
            @Variance_Routed_To_User_Id = USER_INFO_ID
      FROM
            dbo.USER_INFO ui
      WHERE
            ui.QUEUE_ID = @Variance_Routed_To_Queue_Id            

      DECLARE
            @Conversation_Handle UNIQUEIDENTIFIER
           ,@Message XML
           ,@From_Service NVARCHAR(255)
           ,@Target_Service NVARCHAR(255)
           ,@Target_Contract NVARCHAR(255) 
      
      SET @Message = ( SELECT
                        @Account_Id AS Account_Id
                       ,@Service_Month AS Service_Month
                       ,@Event_Type_Name AS Event_Type_Name
                       ,@Variance_Routed_To_Queue_Id AS Variance_Routed_To_Queue_Id
                       ,@Event_By_User_Id AS Event_By_User_Id
                       ,@Variance_Test_Event_Type_Cd AS Variance_Test_Event_Type_Cd
                       ,@Variance_Routed_To_User_Id AS Variance_Routed_To_User_Id
                       ,getdate() AS Event_Ts
            FOR
                       XML PATH('Variance_Test_Event')
                          ,ELEMENTS
                          ,ROOT('Variance_Test_Event_Audit') )
                                      
      IF @Message IS NOT NULL 
            BEGIN
	
                  BEGIN DIALOG CONVERSATION @Conversation_Handle
								FROM SERVICE [//Change_Control/Service/CBMS/Variance_Test_Event_Audit]
								TO SERVICE '//Change_Control/Service/CBMS/Variance_Test_Event_Audit'
								ON CONTRACT [//Change_Control/Contract/Variance_Test_Event_Audit];
                  SEND ON CONVERSATION @Conversation_Handle
								MESSAGE TYPE [//Change_Control/Message/Variance_Test_Event_Audit](@Message);
                  END CONVERSATION @Conversation_Handle 
								--SELECT @Message
            END

END
;
GO

GRANT EXECUTE ON  [dbo].[Variance_Test_Event_Audit_Ins] TO [CBMSApplication]
GO
