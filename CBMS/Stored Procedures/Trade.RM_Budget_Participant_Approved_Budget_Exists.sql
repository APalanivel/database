SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.RM_Budget_Participant_Approved_Budget_Exists                        
                          
Description:                          
        To get market price and forecast pirce if a selected index   
                          
Input Parameters:                          
    Name    DataType        Default     Description                            
--------------------------------------------------------------------------------    
	@Index_Id   INT    
    @Start_Dt	Date    
	@End_Dt		Date
                          
 Output Parameters:                                
	Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
Usage Examples:                              
--------------------------------------------------------------------------------    
	SELECT * FROM dbo.ENTITY e WHERE e.ENTITY_TYPE=272

	EXEC Trade.RM_Budget_Participant_Approved_Budget_Exists  '2019-01-01','2019-12-01'
	EXEC Trade.RM_Budget_Participant_Approved_Budget_Exists  @Rm_Budget_Id = 113  
  
    
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR			2019-11-29	RM-Budgets Enahancement - Created
	RR			2020-02-13	GRM-1730 Modified @Site_Id input to varchar                
******/
CREATE PROCEDURE [Trade].[RM_Budget_Participant_Approved_Budget_Exists]
    (
        @Index_Id INT = NULL
        , @Client_Id INT = NULL
        , @Commodity_Id INT = NULL
        , @Country_Id NVARCHAR(MAX) = NULL
        , @Start_Dt DATE = NULL
        , @End_Dt DATE = NULL
        , @Participant_Sites VARCHAR(20) = NULL
        , @Division_Id INT = NULL
        , @Site_Id VARCHAR(MAX) = NULL
        , @Rm_Budget_Id INT = NULL
        , @RM_Group_Id INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @RM_Budget_Participant_Approved_Budget_Exists BIT = 0;

        CREATE TABLE #Participant_Sites
             (
                 Client_Hier_Id INT
             );



        INSERT INTO #Participant_Sites
             (
                 Client_Hier_Id
             )
        EXEC Trade.RM_Budget_Participant_Sites_Sel
            @Client_Id = @Client_Id
            , @Commodity_Id = @Commodity_Id
            , @Country_Id = @Country_Id
            , @Start_Dt = @Start_Dt
            , @End_Dt = @End_Dt
            , @Participant_Sites = @Participant_Sites
            , @Division_Id = @Division_Id
            , @Site_Id = @Site_Id
            , @Index_Id = @Index_Id
            , @RM_Group_Id = @RM_Group_Id;

        DELETE  FROM #Participant_Sites WHERE   @Rm_Budget_Id IS NOT NULL;

        INSERT INTO #Participant_Sites
             (
                 Client_Hier_Id
             )
        SELECT
            rbp.Client_Hier_Id
        FROM
            Trade.Rm_Budget_Participant rbp
        WHERE
            rbp.Rm_Budget_Id = @Rm_Budget_Id
            AND @Rm_Budget_Id IS NOT NULL;

        SELECT
            @Start_Dt = rbp.Budget_Start_Dt
            , @End_Dt = rbp.Budget_End_Dt
        FROM
            Trade.Rm_Budget rbp
        WHERE
            rbp.Rm_Budget_Id = @Rm_Budget_Id
            AND @Rm_Budget_Id IS NOT NULL;

        SELECT
            @RM_Budget_Participant_Approved_Budget_Exists = 1
        FROM
            Trade.Rm_Budget rb
            INNER JOIN Trade.Rm_Budget_Participant rbp
                ON rbp.Rm_Budget_Id = rb.Rm_Budget_Id
            INNER JOIN #Participant_Sites ps
                ON ps.Client_Hier_Id = rbp.Client_Hier_Id
        WHERE
            (   rb.Budget_Start_Dt BETWEEN @Start_Dt
                                   AND     @End_Dt
                OR  rb.Budget_End_Dt BETWEEN @Start_Dt
                                     AND     @End_Dt
                OR  @Start_Dt BETWEEN rb.Budget_Start_Dt
                              AND     rb.Budget_End_Dt
                OR  @End_Dt BETWEEN rb.Budget_Start_Dt
                            AND     rb.Budget_End_Dt)
            AND (   @Commodity_Id IS NULL
                    OR  rb.Commodity_Id = @Commodity_Id)
            AND ISNULL(rb.Is_Budget_Approved, 0) = 1
            AND (   @Rm_Budget_Id IS NULL
                    OR  NOT EXISTS (   SELECT
                                            1
                                       FROM
                                            Trade.Rm_Budget rb2
                                       WHERE
                                            rb2.Rm_Budget_Id = @Rm_Budget_Id
                                            AND rb2.Is_Budget_Approved = 1));

        SELECT
            @RM_Budget_Participant_Approved_Budget_Exists AS Participant_Approved_Budget_Exists;
    END;

GO
GRANT EXECUTE ON  [Trade].[RM_Budget_Participant_Approved_Budget_Exists] TO [CBMSApplication]
GO
