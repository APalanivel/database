SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
 NAME: [dbo].[Related_Invoices_Sel_By_Account_Invoice]         
                          
 DESCRIPTION:                          
   To get the details of related invoices based on account and invoice.                          
                          
 INPUT PARAMETERS:            
                       
 Name                        DataType         Default       Description          
-------------------------------------------------------------------------------------------    
@Cu_Invoice_Id     INT  
  
                      
                          
 OUTPUT PARAMETERS:            
                             
 Name                        DataType         Default       Description          
-------------------------------------------------------------------------------------------    
                          
 USAGE EXAMPLES:                              
-------------------------------------------------------------------------------------------    
        
EXEC [dbo].[Related_Invoices_Sel_By_Account_Invoice]  31942760  
  
EXEC [dbo].[Related_Invoices_Sel_By_Account_Invoice]  32888308  
                              
 AUTHOR INITIALS:          
         
 Initials              Name          
-------------------------------------------------------------------------------------------    
 RKV                   Ravi Kumar Raju    
                           
 MODIFICATIONS:        
            
 Initials              Date             Modification        
-------------------------------------------------------------------------------------------    
 RKV                   2016-08-12       Created For MAINT-4177.                
                         
******/      
   
CREATE PROCEDURE [dbo].[Related_Invoices_Sel_By_Account_Invoice] ( @Cu_Invoice_Id INT )  
AS   
BEGIN                  
      SET NOCOUNT ON;     
        
        
      DECLARE @Variance_Status_Cd INT   
  
  
        
      DECLARE @Selected_Account_Meter TABLE  
            (   
             Meter_Id INT  
            ,Account_Type CHAR(8)  
            ,Service_Month DATE )   
  
       
      CREATE TABLE #Account_Invoice_Information  
            (   
             Account_Number VARCHAR(500)  
            ,Service_Month DATETIME  
            ,CU_INVOICE_ID INT  
            ,CBMS_IMAGE_ID INT  
            ,IS_REPORTED BIT  
            ,Commodity_Id INT  
            ,Client_Id INT  
            ,Division_Id INT  
            ,Account_Id INT  
            ,Site_Client_Hier_Id INT )       
       
      SELECT  
            @Variance_Status_Cd = c.Code_Id  
      FROM  
            dbo.Code c  
            INNER JOIN dbo.Codeset cs  
                  ON c.Codeset_Id = cs.Codeset_Id  
      WHERE  
            cs.Codeset_Name = 'VarianceStatus'  
            AND cs.Std_Column_Name = 'Variance_Status_Cd'  
            AND Code_Value = 'Closed'  
        
      INSERT      INTO @Selected_Account_Meter  
                  SELECT  
                        cha.Meter_Id  
                       ,cha.Account_Type  
                       ,cism.SERVICE_MONTH  
                  FROM  
                        dbo.CU_INVOICE_SERVICE_MONTH cism  
                        INNER JOIN core.Client_Hier_Account cha  
                              ON cism.Account_ID = cha.Account_Id  
                  WHERE  
                        cism.CU_INVOICE_ID = @Cu_Invoice_Id  
              
  
              
              
      INSERT      INTO #Account_Invoice_Information  
                  (   
                   Account_Number  
                  ,Service_Month  
                  ,CU_INVOICE_ID  
                  ,CBMS_IMAGE_ID  
                  ,IS_REPORTED  
                  ,Commodity_Id  
                  ,Client_Id  
                  ,Division_Id  
                  ,Account_Id  
                  ,Site_Client_Hier_Id )  
                  SELECT  
                        cha.Display_Account_Number  
                       ,cism.SERVICE_MONTH  
                       ,ci.CU_INVOICE_ID  
                       ,ci.CBMS_IMAGE_ID  
                       ,ci.IS_REPORTED  
                       ,cha.Commodity_Id  
                       ,ch.Client_Id  
                       ,ch.Sitegroup_Id  
                       ,cha.Account_Id  
                       ,cha.Client_Hier_Id  
                  FROM  
                        Core.Client_Hier_Account cha  
                        INNER JOIN core.Client_Hier ch  
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id  
                        INNER JOIN @Selected_Account_Meter sam  
                              ON cha.Meter_Id = sam.Meter_Id  
                        INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism  
                              ON sam.Service_Month = cism.SERVICE_MONTH  
                                 AND cism.Account_ID = cha.Account_Id  
                        INNER JOIN dbo.CU_INVOICE ci  
                              ON cism.CU_INVOICE_ID = ci.CU_INVOICE_ID  
                  WHERE  
                        cha.Account_Type = 'Supplier'  
                        AND sam.Account_Type = 'Utility'  
                        AND ch.Sitegroup_Type_Name = 'Division'  
                  GROUP BY  
                        cha.Display_Account_Number  
                       ,cism.SERVICE_MONTH  
                       ,ci.CU_INVOICE_ID  
                       ,ci.CBMS_IMAGE_ID  
                       ,ci.IS_REPORTED  
                       ,cha.Commodity_Id  
                       ,ch.Client_Id  
                       ,ch.Sitegroup_Id  
                       ,cha.Account_Id  
                       ,cha.Client_Hier_Id      
        
      INSERT      INTO #Account_Invoice_Information  
                  (   
                   Account_Number  
                  ,Service_Month  
                  ,CU_INVOICE_ID  
                  ,CBMS_IMAGE_ID  
                  ,IS_REPORTED  
                  ,Commodity_Id  
                  ,Client_Id  
                  ,Division_Id  
                  ,Account_Id  
                  ,Site_Client_Hier_Id )  
                  SELECT  
                        cha.Display_Account_Number  
                       ,cism.SERVICE_MONTH  
                       ,ci.CU_INVOICE_ID  
                       ,ci.CBMS_IMAGE_ID  
                       ,ci.IS_REPORTED  
                       ,cha.Commodity_Id  
                       ,ch.Client_Id  
                       ,ch.Sitegroup_Id  
                       ,cha.Account_Id  
                       ,cha.Client_Hier_Id  
                  FROM  
                        Core.Client_Hier_Account cha  
                        INNER JOIN core.Client_Hier ch  
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id  
                        INNER JOIN @Selected_Account_Meter sam  
                              ON cha.Meter_Id = sam.Meter_Id  
                        INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism  
                              ON sam.Service_Month = cism.SERVICE_MONTH  
                                 AND cism.Account_ID = cha.Account_Id  
                        INNER JOIN dbo.CU_INVOICE ci  
                              ON cism.CU_INVOICE_ID = ci.CU_INVOICE_ID  
                  WHERE  
                        cha.Account_Type = 'Utility'  
                        AND sam.Account_Type = 'Supplier'  
                        AND ch.Sitegroup_Type_Name = 'Division'  
                  GROUP BY  
                        cha.Display_Account_Number  
                       ,cism.SERVICE_MONTH  
                       ,ci.CU_INVOICE_ID  
                       ,ci.CBMS_IMAGE_ID  
                       ,ci.IS_REPORTED  
                       ,cha.Commodity_Id  
                       ,ch.Client_Id  
                       ,ch.Sitegroup_Id  
                       ,cha.Account_Id  
                       ,cha.Client_Hier_Id  
  
  
      SELECT  
            ai.Account_Number  
           ,ai.Service_Month  
           ,ai.CU_INVOICE_ID  
           ,ai.CBMS_IMAGE_ID  
           ,ai.IS_REPORTED  
           ,ai.Commodity_Id  
           ,ai.Client_Id  
           ,ai.Division_Id  
           ,ai.Account_Id  
           ,ai.Site_Client_Hier_Id  
           ,MAX(CASE WHEN vl.Variance_Status_Cd = @Variance_Status_Cd or vl.Variance_Status_Cd IS NULL THEN 0  
                     ELSE 1  
                END) AS Is_Open_Variance  
      FROM  
            #Account_Invoice_Information ai  
            LEFT JOIN dbo.Variance_Log vl  
                  ON ai.Client_Id = vl.Client_ID  
                     AND ai.Service_Month = vl.Service_Month  
                     AND ai.Account_Id = vl.Account_ID  
                     AND ai.Commodity_Id = vl.Commodity_ID 
                     AND vl.Partition_Key IS NULL   
      WHERE  
            ai.CU_INVOICE_ID <> @Cu_Invoice_Id  
      GROUP BY  
            ai.Account_Number  
           ,ai.Service_Month  
           ,ai.CU_INVOICE_ID  
           ,ai.CBMS_IMAGE_ID  
           ,ai.IS_REPORTED  
           ,ai.Commodity_Id  
           ,ai.Client_Id  
           ,ai.Division_Id  
           ,ai.Account_Id  
           ,ai.Site_Client_Hier_Id  
             
      DROP TABLE #Account_Invoice_Information  
        
                         
END;  
  
  
  
  
  
;
GO
GRANT EXECUTE ON  [dbo].[Related_Invoices_Sel_By_Account_Invoice] TO [CBMSApplication]
GO
