
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	cbms_prod.dbo.BUDGET_GET_RA_BUDGET_COMPLETED_COUNT_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId	   		INT				
   	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

exec BUDGET_GET_RA_BUDGET_COMPLETED_COUNT_DETAILS_P 35

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CPE			Chaitanya Panduga Eshwar

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	CPE			2013-03-29	Modified for commercial budget
	RR			2017-05-30	SE2017-26 Modified script to get budget accounts in a user queue from table dbo.Budget_Account_Queue	
******/

CREATE PROCEDURE [dbo].[BUDGET_GET_RA_BUDGET_COMPLETED_COUNT_DETAILS_P] @userId INT
AS 
BEGIN

      SET NOCOUNT ON  

      DECLARE
            @Rates_Queue_Cd INT
           ,@Both_Queue_Cd INT
 
      SELECT
            @Rates_Queue_Cd = c.Code_Id
      FROM
            dbo.Code AS c
            JOIN dbo.Codeset AS cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'IncludeInAnalystQueue'
            AND c.Code_Value = 'Yes - Rates only'
  
      SELECT
            @Both_Queue_Cd = c.Code_Id
      FROM
            dbo.Code AS c
            JOIN dbo.Codeset AS cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'IncludeInAnalystQueue'
            AND c.Code_Value = 'Yes - Rates and Sourcing'
             
      SELECT
            COUNT(DISTINCT ba.BUDGET_ACCOUNT_ID) AS totalCompletedAccounts
      FROM
            Core.Client_Hier_Account AS cha
            INNER JOIN dbo.ENTITY AS Service_Level
                  ON cha.Account_Service_level_Cd = Service_Level.ENTITY_ID
            INNER JOIN dbo.BUDGET_ACCOUNT AS ba
                  ON ba.ACCOUNT_ID = cha.Account_Id
            INNER JOIN dbo.Budget_Account_Queue baq
                  ON ba.BUDGET_ACCOUNT_ID = baq.Budget_Account_Id
            INNER JOIN dbo.USER_INFO ui
                  ON baq.Queue_Id = ui.QUEUE_ID
            INNER JOIN dbo.BUDGET AS b
                  ON ba.BUDGET_ID = b.BUDGET_ID
                     AND b.COMMODITY_TYPE_ID = cha.Commodity_Id
            LEFT JOIN ( Core.Client_Hier_Account AS cha1
                        INNER JOIN dbo.CONTRACT AS c
                              ON cha1.Supplier_Contract_ID = c.CONTRACT_ID
                                 AND c.CONTRACT_END_DATE >= GETDATE()
                                 AND cha1.Account_Type = 'Supplier'
                        INNER JOIN dbo.ENTITY AS e
                              ON c.CONTRACT_TYPE_ID = e.ENTITY_ID
                                 AND e.ENTITY_NAME = 'Supplier' )
                        ON cha.Meter_Id = cha1.Meter_Id
      WHERE
            cha.Account_Type = 'Utility'
            AND ( cha1.Account_Id IS NOT NULL		-- Transport
                  OR Service_Level.ENTITY_NAME NOT IN ( 'C', 'D' ) )
            AND ui.USER_INFO_ID = @userId
            AND ba.RATES_COMPLETED_BY IS NOT NULL
            AND ba.CANNOT_PERFORMED_BY IS NULL
            AND ba.RATES_REVIEWED_BY IS NULL
            AND ba.IS_DELETED = 0
            AND b.Analyst_Queue_Display_Cd IN ( @Both_Queue_Cd, @Rates_Queue_Cd )

END;

;
GO


GRANT EXECUTE ON  [dbo].[BUDGET_GET_RA_BUDGET_COMPLETED_COUNT_DETAILS_P] TO [CBMSApplication]
GO
