SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	cbms_prod.dbo.SR_RFP_SET_SUPPLIER_BOC_PRICING_SCOPE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@priceScopeId  	                  int       	          	
	@priceIncludeLosesTypeId	         varchar(20)	          	
	@lossPercent   	                  varchar(20)	          	
	@priceIncludeAnciliaryServiceTypeId	varchar(20)	          	
	@anciliaryServiceEstimatedCost	   varchar(400)	          	
	@priceIncludeCapacityChargesTypeId	varchar(400)	          	
	@capacityChargesEstimatedCost	      varchar(400)	          	
	@priceIncludeISOChargesTypeId	      varchar(400)	          	
	@isoChargesEstimatedCost	         varchar(400)	          	
	@priceincludeTransmissionTypeId	   varchar(400)	          	
	@transmissionEstimatedCost	         varchar(400)	          	
	@priceIncludeTaxesTypeId	         varchar(400)	          	
	@taxEstimatedCost	                  varchar(400)	          	
	@otherCharges  	                  varchar(400)	          	
	@demandCapApplicableTypeId	         varchar(400)	          	
	@capFloorPrice 	                  varchar(400)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@priceScopeIdentityId	int       	          	


USAGE EXAMPLES:
------------------------------------------------------------
--EXEC  SR_RFP_SET_SUPPLIER_BOC_PRICING_SCOPE_P 1

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[SR_RFP_SET_SUPPLIER_BOC_PRICING_SCOPE_P] 
   @priceScopeId int,
   @priceIncludeLosesTypeId varchar(20),
   @lossPercent varchar(20),
   @priceIncludeAnciliaryServiceTypeId varchar(20),
   @anciliaryServiceEstimatedCost varchar(400),
   @priceIncludeCapacityChargesTypeId varchar(400),
   @capacityChargesEstimatedCost varchar(400),
   @priceIncludeISOChargesTypeId varchar(400),
   @isoChargesEstimatedCost varchar(400),
   @priceincludeTransmissionTypeId varchar(400),
   @transmissionEstimatedCost varchar(400),
   @priceIncludeTaxesTypeId varchar(400),
   @taxEstimatedCost varchar(400),
   @otherCharges varchar(400),
   @demandCapApplicableTypeId varchar(400),
   @capFloorPrice varchar(400),
   @priceScopeIdentityId   int   OUT


as

set nocount on
--declare @id int

if @priceScopeId=0

begin

INSERT INTO SR_RFP_PRICING_SCOPE
(
	PRICE_INCLUDE_LOSES_TYPE_ID,
	LOSS_PERCENT,
	PRICE_INCLUDE_ANCILIARY_SERVICE_TYPE_ID,
	ANCILIARY_SERVICE_ESTIMATED_COST,
	PRICE_INCLUDE_CAPACITY_CHARGES_TYPE_ID,
	CAPACITY_CHARGES_ESTIMATED_COST,
	PRICE_INCLUDE_ISO_CHARGES_TYPE_ID,
	ISO_CHARGES_ESTIMATED_COST,
	PRICE_INCLUDE_TRANSMISSION_TYPE_ID,
	TRANSMISSION_ESTIMATED_COST,
	PRICE_INCLUDE_TAXES_TYPE_ID,
	TAXES_ESTIMATED_COST,
	OTHER_CHARGES,
	DEMAND_CAP_APPLICABLE_TYPE_ID,
	CAP_FLOOR_PRICE
)
values
(
	@priceIncludeLosesTypeId,
	@lossPercent,
	@priceIncludeAnciliaryServiceTypeId,			
	@anciliaryServiceEstimatedCost,
	@priceIncludeCapacityChargesTypeId,
	@capacityChargesEstimatedCost,
	@priceIncludeISOChargesTypeId,
	@isoChargesEstimatedCost,
	@priceincludeTransmissionTypeId,
	@transmissionEstimatedCost,
	@priceIncludeTaxesTypeId,
	@taxEstimatedCost,
	@otherCharges,
	@demandCapApplicableTypeId,
	@capFloorPrice
)
		

SELECT @priceScopeIdentityId=@@identity
end

else if @priceScopeId>0
begin
	
	UPDATE SR_RFP_PRICING_SCOPE 
	SET
   	 PRICE_INCLUDE_LOSES_TYPE_ID=@priceIncludeLosesTypeId
	   ,LOSS_PERCENT=@lossPercent
	   ,PRICE_INCLUDE_ANCILIARY_SERVICE_TYPE_ID=@priceIncludeAnciliaryServiceTypeId
	   ,ANCILIARY_SERVICE_ESTIMATED_COST=@anciliaryServiceEstimatedCost
	   ,PRICE_INCLUDE_CAPACITY_CHARGES_TYPE_ID=@priceIncludeCapacityChargesTypeId
	   ,CAPACITY_CHARGES_ESTIMATED_COST=@capacityChargesEstimatedCost
	   ,PRICE_INCLUDE_ISO_CHARGES_TYPE_ID=@priceIncludeISOChargesTypeId
	   ,ISO_CHARGES_ESTIMATED_COST=@isoChargesEstimatedCost
	   ,PRICE_INCLUDE_TRANSMISSION_TYPE_ID=@priceincludeTransmissionTypeId
	   ,TRANSMISSION_ESTIMATED_COST=@transmissionEstimatedCost
	   ,PRICE_INCLUDE_TAXES_TYPE_ID=@priceIncludeTaxesTypeId
	   ,TAXES_ESTIMATED_COST=@taxEstimatedCost
	   ,OTHER_CHARGES=@otherCharges
	   ,DEMAND_CAP_APPLICABLE_TYPE_ID=@demandCapApplicableTypeId
	   ,CAP_FLOOR_PRICE=@capFloorPrice
	WHERE SR_RFP_PRICING_SCOPE_ID=@priceScopeId

end

return
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_SET_SUPPLIER_BOC_PRICING_SCOPE_P] TO [CBMSApplication]
GO
