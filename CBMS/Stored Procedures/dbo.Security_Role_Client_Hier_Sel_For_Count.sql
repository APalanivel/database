SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******     
NAME: 
	dbo.Security_Role_Client_Hier_Sel_For_Count
     
DESCRIPTION:   
	This Stored Procedure is used to get site count for a given security role.
     
INPUT PARAMETERS:      
	Name					DataType				   Default        Description      
----------------------------------------------------------------------------------   
	@Security_Role_Id        INT
      
OUTPUT PARAMETERS:      
	Name		DataType		Default			Description      
--------------------------------------------------------------------      
  
USAGE EXAMPLES:      
----------------------------------------------------------------------------------------------       
    EXEC dbo.Security_Role_Client_Hier_Sel_For_Count 99
    
AUTHOR INITIALS:      
	 Initials   Name       
-------------------------------------------------------------------      
     MJ        Meera Joy   
     
MODIFICATIONS       
	Initials     Date       Modification      
--------------------------------------------------------------------
     MJ			05-MAY-2017	Created                    
******/  

CREATE PROCEDURE [dbo].[Security_Role_Client_Hier_Sel_For_Count]
      ( 
       @Security_Role_Id INT )
AS 
BEGIN
  
      SET NOCOUNT ON;

      SELECT
            count(1) SR_Site_Count
      FROM
            dbo.Security_Role_Client_Hier srch
            INNER JOIN Security_Role sr
                  ON sr.Security_Role_Id = srch.Security_Role_Id
      WHERE
            srch.Security_Role_Id = @Security_Role_Id
END;
;

;
GO
GRANT EXECUTE ON  [dbo].[Security_Role_Client_Hier_Sel_For_Count] TO [CBMSApplication]
GO
