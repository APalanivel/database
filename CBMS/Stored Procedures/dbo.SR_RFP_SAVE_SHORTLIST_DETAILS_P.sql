SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_SAVE_SHORTLIST_DETAILS_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@sopShortListId int,
@accountTermId int,
@selectedProductsId int,
@supplierContactVendorMapId int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- Test a new insert
--exec DBO.SR_RFP_SAVE_SHORTLIST_DETAILS_P
--@sopShortListId =1 ,
--@accountTermId = 1228,
--@selectedProductsId =226,
--@supplierContactVendorMapId =1352

---- Check for the new entry at SV
--select * from SV..SR_RFP_SOP_SHORTLIST_DETAILS where SR_RFP_SOP_SHORTLIST_DETAILS_ID = (select max(SR_RFP_SOP_SHORTLIST_DETAILS_ID) from SR_RFP_SOP_SHORTLIST_DETAILS )

---- Delete the entry
--delete from SR_RFP_SOP_SHORTLIST_DETAILS where SR_RFP_SOP_SHORTLIST_DETAILS_ID = (select max(SR_RFP_SOP_SHORTLIST_DETAILS_ID) from SR_RFP_SOP_SHORTLIST_DETAILS )


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE DBO.SR_RFP_SAVE_SHORTLIST_DETAILS_P

@sopShortListId int,
@accountTermId int,
@selectedProductsId int,
@supplierContactVendorMapId int


AS
	
SET NOCOUNT ON


INSERT INTO SR_RFP_SOP_SHORTLIST_DETAILS(
											SR_RFP_SOP_SHORTLIST_ID,
											SR_RFP_ACCOUNT_TERM_ID , 
											SR_RFP_SELECTED_PRODUCTS_ID , 
											SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID 
										 ) 
								  VALUES ( 
											@sopShortListId , 
											@accountTermId , 
											@selectedProductsId , 
											@supplierContactVendorMapId 
										 )
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_SAVE_SHORTLIST_DETAILS_P] TO [CBMSApplication]
GO
