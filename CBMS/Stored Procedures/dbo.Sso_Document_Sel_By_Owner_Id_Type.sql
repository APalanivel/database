SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******          

NAME: dbo.Sso_Document_Sel_By_Owner_Id_Type
     
DESCRIPTION: 
	To Get Sso Document name for given Owner Id and Owner Type.
      
INPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
    @Owner_Id		INT
    @Owner_Type		VARCHAR(30)				Client/Division/Site
    @StartIndex		INT			1
	@EndIndex		INT			2147483647

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Sso_Document_Sel_By_Owner_Id_Type 281,'Site',1,25
	EXEC dbo.Sso_Document_Sel_By_Owner_Id_Type 10003,'Client',1,25
	EXEC dbo.Sso_Document_Sel_By_Owner_Id_Type 81,'Division',1,25

AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH
	CPE			Chaitanya Panduga Eshwar

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			25-JUN-10	Created   
	PNR			24-AUG-10	Added Pagination logic and also added two parameters @StartIndex,@EndIndex
	CPE			04/06/2011	Added code to get Client_Hier_Id from the Owner_Id and Owner_Type
  
*/  

CREATE PROCEDURE dbo.Sso_Document_Sel_By_Owner_Id_Type
( 
 @Owner_Id INT
,@Owner_Type VARCHAR(30)
,@StartIndex INT = 1
,@EndIndex INT = 2147483647 )
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE @Client_Hier_Id int

      SELECT
            @Client_Hier_Id = Client_Hier_Id
      FROM
            Core.Client_Hier CH
      WHERE
            ( @OWNER_ID = ch.Client_Id
              AND @Owner_Type = 'Client'
              AND ch.Site_Id = 0
              AND ch.Sitegroup_Id = 0 )
            OR ( @OWNER_ID = ch.Sitegroup_Id
                 AND @Owner_Type = 'Division'
                 AND ch.Site_Id = 0 )
            OR ( @OWNER_ID = ch.Site_Id
                 AND @Owner_Type = 'Site'
                 AND ch.Site_Id > 0 )

      ;WITH  Cte_Sso_Document
              AS ( SELECT
                        sd.DOCUMENT_TITLE
                       ,Row_Num = ROW_NUMBER() OVER ( ORDER BY sd.DOCUMENT_TITLE )
                       ,Total_Rows = COUNT(1) OVER ( )
                   FROM
                                                     dbo.SSO_DOCUMENT SD
                                                     JOIN dbo.SSO_DOCUMENT_OWNER_MAP SDOM
                                                      ON SD.SSO_DOCUMENT_ID = SDOM.SSO_DOCUMENT_ID
                   WHERE
                                                     SDOM.Client_Hier_Id = @Client_Hier_Id)
            SELECT
                  DOCUMENT_TITLE
                 ,Total_Rows
            FROM
                  Cte_Sso_Document
            WHERE
                  Row_Num BETWEEN @StartIndex AND @EndIndex
END


GO
GRANT EXECUTE ON  [dbo].[Sso_Document_Sel_By_Owner_Id_Type] TO [CBMSApplication]
GO
