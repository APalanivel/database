SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






































/******                              
 NAME: dbo.[[Get_Variance_Process_Engine_By_Category]]                  
                              
 DESCRIPTION:    
 THis procedure will be called in console app
          
                              
 INPUT PARAMETERS:                
                           
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
 
                                  
 OUTPUT PARAMETERS:                
                                 
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
                              
 USAGE EXAMPLES:                                  
---------------------------------------------------------------------------------------------------------------                                  
      
--Commodity Level      
   
DECLARE	@return_value INT



EXEC	 [dbo].[Update_Batch_Rerun_Variance_Records] 

 
GO

  
     
                             
 AUTHOR INITIALS:    
 Arunkumar Palanivel AP          
             
 Initials              Name              
---------------------------------------------------------------------------------------------------------------  
     Steps(added by Arun):

	
                               
 MODIFICATIONS:            
                
 Initials              Date             Modification            
---------------------------------------------------------------------------------------------------------------   
AP					May 13,2020	Procedure is created
******/
CREATE PROCEDURE [dbo].[Update_Batch_Rerun_Variance_Records]
      (
      @Variance_Batch_Rerun_Log_id INT )
AS
      BEGIN

            SET NOCOUNT ON;

            DECLARE
                  @completed_status_Cd INT;

            SELECT
                  @completed_status_Cd = C.Code_Id
            FROM  dbo.Code C
                  JOIN
                  dbo.Codeset cs
                        ON cs.Codeset_Id = C.Codeset_Id
            WHERE cs.Codeset_Name = 'Data Upload Status'
                  AND   C.Code_Dsc = 'Completed';

            UPDATE
                  VB
            SET
                  VB.Status_Cd = @completed_status_Cd
                , VB.Changes_Ts = getdate()
            FROM  dbo.Variance_Batch_Rerun_Logs VB
            WHERE VB.Variance_Batch_Rerun_Log_id = @Variance_Batch_Rerun_Log_id;




      END;
GO
GRANT EXECUTE ON  [dbo].[Update_Batch_Rerun_Variance_Records] TO [CBMSApplication]
GO
