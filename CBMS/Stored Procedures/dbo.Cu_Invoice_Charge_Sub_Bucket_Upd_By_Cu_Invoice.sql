SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
                         
 NAME: dbo.Cu_Invoice_Charge_Sub_Bucket_Upd_By_Cu_Invoice                     
                          
 DESCRIPTION:      
		  To Update the sub bucket info in Cu_invoice_Charge table.                        
                          
 INPUT PARAMETERS:      
                         
 Name                               DataType          Default       Description      
---------------------------------------------------------------------------------------------------------------    
 @Cu_Invoice_Id				         INT        
                          
 OUTPUT PARAMETERS:      
                               
 Name                               DataType          Default       Description      
---------------------------------------------------------------------------------------------------------------    
                          
 USAGE EXAMPLES:                              
---------------------------------------------------------------------------------------------------------------      
               
	 BEGIN TRAN        
	 SELECT * FROM dbo.CU_INVOICE_Charge cid WHERE CU_INVOICE_ID =22907303
	 EXEC dbo.Cu_Invoice_Charge_Sub_Bucket_Upd_By_Cu_Invoice @Cu_Invoice_Id=22907303    
	 SELECT * FROM dbo.CU_INVOICE_Charge cid WHERE CU_INVOICE_ID =22907303
	 ROLLBACK   
			                
                         
 AUTHOR INITIALS:    
       
 Initials                   Name      
---------------------------------------------------------------------------------------------------------------    
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
---------------------------------------------------------------------------------------------------------------    
 NR                     2019-01-31      Created for Telamon data feed               
                         
******/

CREATE PROCEDURE [dbo].[Cu_Invoice_Charge_Sub_Bucket_Upd_By_Cu_Invoice]
     (
         @Cu_Invoice_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;

        CREATE TABLE #Bucket_State_Commodity_Dtl
             (
                 State_Id INT
                 , Bucket_Master_Id INT
                 , Commodity_Id INT
             );


        CREATE TABLE #Ec_Sub_Invoice_Bucket_Dtl
             (
                 EC_Invoice_Sub_Bucket_Master_Id INT
                 , Bucket_Master_Id INT
                 , Commodity_Id INT
             );

        INSERT INTO #Bucket_State_Commodity_Dtl
             (
                 State_Id
                 , Bucket_Master_Id
                 , Commodity_Id
             )
        SELECT
            cha.Meter_State_Id
            , cid.Bucket_Master_Id
            , cha.Commodity_Id
        FROM
            dbo.CU_INVOICE_CHARGE cid
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cid.CU_INVOICE_ID = cism.CU_INVOICE_ID
            INNER JOIN Core.Client_Hier_Account cha
                ON cism.Account_ID = cha.Account_Id
                   AND  cid.COMMODITY_TYPE_ID = cha.Commodity_Id
        WHERE
            cid.CU_INVOICE_ID = @Cu_Invoice_Id
        GROUP BY
            cha.Meter_State_Id
            , cid.Bucket_Master_Id
            , cha.Commodity_Id;


        INSERT INTO #Ec_Sub_Invoice_Bucket_Dtl
             (
                 EC_Invoice_Sub_Bucket_Master_Id
                 , Bucket_Master_Id
                 , Commodity_Id
             )
        SELECT
            MIN(EC_Invoice_Sub_Bucket_Master_Id) AS EC_Invoice_Sub_Bucket_Master_Id
            , eisbm.Bucket_Master_Id
            , bm.Commodity_Id
        FROM
            dbo.EC_Invoice_Sub_Bucket_Master eisbm
            INNER JOIN dbo.Bucket_Master bm
                ON eisbm.Bucket_Master_Id = bm.Bucket_Master_Id
            INNER JOIN #Bucket_State_Commodity_Dtl bst
                ON eisbm.Bucket_Master_Id = bst.Bucket_Master_Id
                   AND  eisbm.State_Id = bst.State_Id
                   AND  bst.Commodity_Id = bm.Commodity_Id
        GROUP BY
            eisbm.Bucket_Master_Id
            , bm.Commodity_Id;

        BEGIN TRY
            BEGIN TRANSACTION;


            UPDATE
                cid
            SET
                cid.EC_Invoice_Sub_Bucket_Master_Id = esibd.EC_Invoice_Sub_Bucket_Master_Id
            FROM
                dbo.CU_INVOICE_CHARGE cid
                INNER JOIN #Ec_Sub_Invoice_Bucket_Dtl esibd
                    ON cid.Bucket_Master_Id = esibd.Bucket_Master_Id
                       AND  esibd.Commodity_Id = cid.COMMODITY_TYPE_ID
            WHERE
                cid.CU_INVOICE_ID = @Cu_Invoice_Id
                AND cid.EC_Invoice_Sub_Bucket_Master_Id IS NULL;


            COMMIT TRANSACTION;

        END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0
                ROLLBACK TRAN;
            EXEC dbo.usp_RethrowError;

        END CATCH;


    END;

GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Charge_Sub_Bucket_Upd_By_Cu_Invoice] TO [CBMSApplication]
GO
