SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[dmlLookup_GetByType]
	( @LookupTypeId int )
AS
BEGIN
   select t.LookupTypeId
	, t.LookupTypeName
	, l.LookupId
	, l.LookupName
	, l.SortOrder
     from dmlLookupType t
     join dmlLookup l on l.LookupTypeId = t.LookupTypeId
    where t.LookupTypeId = @LookupTypeId
 order by t.LookupTypeName
	, l.SortOrder
	, l.LookupName
END




GO
GRANT EXECUTE ON  [dbo].[dmlLookup_GetByType] TO [CBMSApplication]
GO
