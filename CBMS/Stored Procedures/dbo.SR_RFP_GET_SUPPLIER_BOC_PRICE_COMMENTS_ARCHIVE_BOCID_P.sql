
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	dbo.SR_RFP_GET_SUPPLIER_BOC_PRICE_COMMENTS_ARCHIVE_BOCID_P

DESCRIPTION: 


INPUT PARAMETERS:    
    Name            DataType          Default     Description    
---------------------------------------------------------------------------------    
	@userId			VARCHAR
    @sessionId		VARCHAR
    @bidId			INT
    @isSOP			BIT
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
  

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DR			Deana Ritter

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-05-10	GCS-887 selecting Pricing_Comments for SOP bid 

******/
CREATE      PROCEDURE [dbo].[SR_RFP_GET_SUPPLIER_BOC_PRICE_COMMENTS_ARCHIVE_BOCID_P]
      ( 
       @userId VARCHAR
      ,@sessionId VARCHAR
      ,@bidId INT
      ,@isSOP BIT )
AS 
BEGIN
      SET NOCOUNT ON;
		
      SELECT
            comm.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
           ,arch.PRICE_RESPONSE_COMMENTS
           ,arch.ARCHIVED_ON_DATE
           ,arch.SR_RFP_SUPPLIER_PRICE_COMMENTS_ARCHIVE_ID
           ,arch.Pricing_Comments
      FROM
            dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS comm
            INNER JOIN dbo.SR_RFP_BID bid
                  ON comm.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID = bid.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
            INNER JOIN dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS_ARCHIVE arch
                  ON comm.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID = arch.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
      WHERE
            ( bid.SR_RFP_BID_ID = @bidId )
END
;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_GET_SUPPLIER_BOC_PRICE_COMMENTS_ARCHIVE_BOCID_P] TO [CBMSApplication]
GO
