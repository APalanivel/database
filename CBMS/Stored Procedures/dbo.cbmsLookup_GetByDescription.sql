SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   procedure [dbo].[cbmsLookup_GetByDescription]
	( @MyAccountId int 
	, @entity_description varchar(200)
	)
AS
BEGIN

	   select entity_id
		, entity_name
		, entity_type
		, entity_description
	     from entity
	    where entity_description = @entity_description
	 order by entity_name asc
END
GO
GRANT EXECUTE ON  [dbo].[cbmsLookup_GetByDescription] TO [CBMSApplication]
GO
