SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.BUDGET_GET_SYSTEM_ACCOUNT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

--exec BUDGET_GET_SYSTEM_ACCOUNT_P



CREATE  PROCEDURE dbo.BUDGET_GET_SYSTEM_ACCOUNT_P
	AS
declare @from varchar(50), @to varchar(50)
	select @from = entity_name from entity(nolock) where entity_type = 1059
	select @to = entity_name from entity(nolock) where entity_type = 1060
	select @from as budget_from_address, @to as budget_to_address
GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_SYSTEM_ACCOUNT_P] TO [CBMSApplication]
GO
