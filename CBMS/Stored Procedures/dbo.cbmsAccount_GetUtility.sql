SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsAccount_GetUtility

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@account_id    	int       	null      	
	@client_id     	int       	null      	
	@site_id       	int       	null      	
	@city          	varchar(200)	null      	
	@state_id      	int       	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE procedure [dbo].[cbmsAccount_GetUtility]
( 
	@MyAccountId int
	,@account_id int = null
	,@client_id int = null
	,@site_id int = null
	,@city varchar(200) = null
	,@state_id int = null
)
AS
BEGIN

	select s.site_name
		,addr.address_line1
		,addr.city
		,st.state_name
		, acct.account_id
		,acct.account_number
		,v.vendor_name
		,m.meter_number
		,c.client_name
		,addr.city
		,st.state_id
		,v.vendor_name
	from account acct
	join vendor v with(nolock) on v.vendor_id = acct.vendor_id
	join meter m with(nolock) on m.account_id = acct.account_id
	join address addr with(nolock) on (addr.address_id = m.address_id and addr.city like ('%' + coalesce(@city, addr.city) + '%'))
	join state st with(nolock) on (st.state_id = addr.state_id and st.state_id = coalesce(@state_id, st.state_id))
	join site s with(nolock) on (s.site_id = addr.address_parent_id and s.site_id = coalesce(@site_id, s.site_id)) -- and s.not_managed = 0 and s.closed = 0)
	join division d with(nolock) on (d.division_id = s.division_id)-- and d.not_managed = 0)
	join client c with(nolock) on (c.client_id = d.client_id and c.not_managed = 0 and c.client_id = coalesce(@client_id, c.client_id))
	where acct.account_id = coalesce(@account_id, acct.account_id)
	and acct.account_type_id = 38
	--and acct.not_managed = 0	
	order by acct.account_number
		,v.vendor_name
		,m.meter_number

END
GO
GRANT EXECUTE ON  [dbo].[cbmsAccount_GetUtility] TO [CBMSApplication]
GO
