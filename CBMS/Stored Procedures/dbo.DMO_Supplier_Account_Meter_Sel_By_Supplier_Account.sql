SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.DMO_Supplier_Account_Meter_Sel_By_Supplier_Account
           
DESCRIPTION:             
			
			
INPUT PARAMETERS:            
	Name					DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Supplier_Account_Id	INT
    @StartIndex				INT			1
    @EndIndex				INT			2147483647
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
--------------------------------------------------------------------------------- 
 
	SELECT TOP 10 * FROM Core.Client_Hier_Account WHERE Account_Type = 'Supplier' AND Supplier_Contract_ID = -1

            
	EXEC dbo.DMO_Supplier_Account_Meter_Sel_By_Supplier_Account 1149271
	
	EXEC dbo.DMO_Supplier_Account_Meter_Sel_By_Supplier_Account 1505290,1,25,31959
		

		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy
	NR			Narayana Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-02-16	Contract placeholder - CP-8 Created
	NR			2020-01-16	MAINT-9734 - Added @Supplier_Account_Config_Id new parameter.

******/

CREATE PROCEDURE [dbo].[DMO_Supplier_Account_Meter_Sel_By_Supplier_Account]
    (
        @Supplier_Account_Id INT
        , @StartIndex INT = 1
        , @EndIndex INT = 2147483647
        , @Supplier_Account_Config_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;
        WITH Cte_Accs_Mtrs
        AS (
               SELECT
                    chsite.Client_Hier_Id
                    , chsite.Sitegroup_Id
                    , chsite.Site_Id
                    , chsite.Site_name
                    , chsite.City
                    , chsite.State_Id
                    , chsite.State_Name
                    , cha.Account_Id
                    , cha.Account_Number
                    , cha.Meter_Id
                    , cha.Meter_Number
                    , com.Commodity_Name
                    , cha.Meter_Address_Line_1 + ISNULL(',' + NULLIF(cha.Meter_Address_Line_2, ''), '') AS Meter_Address
                    , cha.Account_Vendor_Name
                    , cha.Rate_Name
                    , chasupp.Account_Id AS Supplier_Account_Id
                    , chasupp.Account_Number AS Supplier_Account_Number
                    , chasupp.Supplier_Account_begin_Dt
                    , chasupp.Supplier_Account_End_Dt
                    , chasupp.Supplier_Contract_ID
                    , ROW_NUMBER() OVER (ORDER BY
                                             chasupp.Account_Id) Row_Num
                    , COUNT(1) OVER () Total_Rows
               FROM
                    Core.Client_Hier chsite
                    INNER JOIN Core.Client_Hier_Account cha
                        ON chsite.Client_Hier_Id = cha.Client_Hier_Id
                    INNER JOIN Core.Client_Hier_Account chasupp
                        ON cha.Meter_Id = chasupp.Meter_Id
                    INNER JOIN dbo.Commodity com
                        ON chasupp.Commodity_Id = com.Commodity_Id
               WHERE
                    chasupp.Account_Type = 'Supplier'
                    AND cha.Account_Type = 'Utility'
                    AND chasupp.Account_Id = @Supplier_Account_Id
                    AND chasupp.Supplier_Account_Config_Id = @Supplier_Account_Config_Id
           )
        SELECT
            cam.Client_Hier_Id
            , cam.Sitegroup_Id
            , cam.Site_Id
            , cam.Site_name
            , cam.City
            , cam.State_Id
            , cam.State_Name
            , cam.Account_Id
            , cam.Account_Number
            , cam.Meter_Id
            , cam.Meter_Number
            , cam.Commodity_Name
            , cam.Meter_Address
            , cam.Account_Vendor_Name
            , cam.Rate_Name
            , cam.Supplier_Account_Id
            , cam.Supplier_Account_Number
            , cam.Supplier_Account_begin_Dt
            , cam.Supplier_Account_End_Dt
            , cam.Supplier_Contract_ID
            , cam.Total_Rows
        FROM
            Cte_Accs_Mtrs cam
        WHERE
            cam.Row_Num BETWEEN @StartIndex
                        AND     @EndIndex;


    END;



GO
GRANT EXECUTE ON  [dbo].[DMO_Supplier_Account_Meter_Sel_By_Supplier_Account] TO [CBMSApplication]
GO
