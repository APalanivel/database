SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
     
	 --cbmsAppMenu_GetMyCbms_ExcelPlugin    49,'CBMS-ExcelBaseApp'
CREATE  PROCEDURE [dbo].[cbmsAppMenu_GetMyCbms_ExcelPlugin]   
 (  
  @MyAccountId int      
 , @menu_profile_name varchar(100)       
 )      
AS      
BEGIN      
      
    select distinct m.app_menu_id      
  , m.app_menu_profile_id      
  , m.permission_info_id      
  , m.display_text      
  , m.menu_description      
  , m.target_server      
  , m.target_action      
  , m.menu_level      
  , m.display_order      
  , m.parent_menu_id      
  , m.app_module_id
  , isnull(p_menu.DISPLAY_TEXT,m.display_text) parent_menu_name
      from app_menu m      
      join group_info_permission_info_map gpmap on gpmap.permission_info_id = m.permission_info_id      
             join user_info_group_info_map ugmap on ugmap.group_info_id = gpmap.group_info_id     
			join app_menu_profile  amp on amp.APP_MENU_PROFILE_ID = m.APP_MENU_PROFILE_ID  
			left join app_menu p_menu on p_menu.APP_MENU_ID = m.PARENT_MENU_ID  
     where ugmap.user_info_id = @MyAccountId      
       and amp.APP_MENU_PROFILE_NAME = @menu_profile_name      
       --and m.menu_level = @menu_level      
       --and (m.parent_menu_id = @parent_menu_id or (m.parent_menu_id is null and @parent_menu_id is null))      
  order by m.display_order      
      
END  
  
  

GO
GRANT EXECUTE ON  [dbo].[cbmsAppMenu_GetMyCbms_ExcelPlugin] TO [CBMSApplication]
GO
