SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*********        
NAME:  dbo.Get_Variance_Python_Request_Response        
         
DESCRIPTION:  Used to select all the consumption level data        
        
INPUT PARAMETERS:        
      Name              DataType         Default     Description        
------------------------------------------------------------         
@Commodity_Id   INT        
@Variance_Category_Id INT        
@Consumption_Level_Id INT     NULL        
@is_DEO     BIT     NULL        
        
OUTPUT PARAMETERS:        
      Name              DataType         Default     Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:        
        
exec dbo.Get_Variance_Python_Request_Response 290,100174,1,1 -- demand category        
exec dbo.Get_Variance_Python_Request_Response 290,100175,1,1 -- usage category        
exec dbo.Get_Variance_Python_Request_Response 290,100176,1,1 -- cost category        
exec dbo.Get_Variance_Python_Request_Response 290,100177,1,1 -- Taxes category        
        
exec CODE_SEL_BY_CodeSet_Name 'VarianceCategory'        
        
        
------------------------------------------------------------          
AUTHOR INITIALS:        
        
Initials Name        
------------------------------------------------------------        
AP		Arunkumar Palanivel 
        
        
Initials Date  Modification        
------------------------------------------------------------        
AP	April 22,2020			new rocedure to get request and response
******/        
CREATE PROCEDURE [dbo].[Get_Variance_Python_Request_Response]
      ( 
       @Account_id INT,
	   @Commodity_id INT,
	   @Service_Month DATE)
AS 
BEGIN        
        
      SET NOCOUNT ON ;        
       
          SELECT TOp (1) vesld.Request_Data , vesld.Response_Data FROM logdb.dbo.Variance_Extraction_Service_Log VESL
		  JOIN logdb.dbo.Variance_Extraction_Service_Log_Dtl vesld 
		  ON vesld.Variance_Extraction_Service_Log_Id = VESL.Variance_Extraction_Service_Log_Id
		  WHERE vesl.Account_Id = @Account_id 
		  AND vesl.Commodity_Id = @Commodity_id 
		  AND vesl.Service_Month = @Service_Month
		  ORDER BY  vesld.Variance_Extraction_Service_Log_Dtl_Id desc
END 

;
GO
GRANT EXECUTE ON  [dbo].[Get_Variance_Python_Request_Response] TO [CBMSApplication]
GO
