SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.[cbmsGroupInfoPermissionInfo_RemoveForGroup]


DESCRIPTION: 


INPUT PARAMETERS:    
      Name                  DataType          Default     Description    
------------------------------------------------------------------    
	@MyAccountId            int
	@group_info_id          int
        
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
--exec cbmsGroupInfoPermissionInfo_RemoveForGroup
--	 @MyAccountId = 1
--	, @group_info_id = 117



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates
 DMR		  09/10/2010 Modified for Quoted_Identifier
	  

******/
CREATE PROCEDURE [dbo].[cbmsGroupInfoPermissionInfo_RemoveForGroup]
	( @MyAccountId int
	, @group_info_id int
	)
AS

BEGIN

	SET NOCOUNT ON


	  DELETE 
			group_info_permission_info_map
	   WHERE 
			group_info_id = @group_info_id


	exec cbmsPermissionInfo_GetMy @MyAccountId, @group_info_id
	

END
GO
GRANT EXECUTE ON  [dbo].[cbmsGroupInfoPermissionInfo_RemoveForGroup] TO [CBMSApplication]
GO
