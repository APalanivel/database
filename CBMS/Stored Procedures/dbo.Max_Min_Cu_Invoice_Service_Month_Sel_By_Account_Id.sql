SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
                           
 NAME:  [dbo].[Max_Min_Cu_Invoice_Service_Month_Sel_By_Account_Id]                        
                            
 DESCRIPTION:        
				TO get the MAX , MIN service month based on account.                            
                            
 INPUT PARAMETERS:        
                           
 Name                                   DataType        Default       Description        
---------------------------------------------------------------------------------------------------------------      
 @Account_Id                              INT
                         
 OUTPUT PARAMETERS:        
                                 
 Name                                   DataType        Default       Description        
---------------------------------------------------------------------------------------------------------------      
                            
 USAGE EXAMPLES:                                
---------------------------------------------------------------------------------------------------------------     
       
 Exec  dbo.Max_Min_Cu_Invoice_Service_Month_Sel_By_Account_Id      1487356  

  Exec  dbo.Max_Min_Cu_Invoice_Service_Month_Sel_By_Account_Id      1487357  

                           
 AUTHOR INITIALS:      
         
 Initials               Name        
---------------------------------------------------------------------------------------------------------------      
 NR                     Narayana Reddy                              
                             
 MODIFICATIONS:      
                             
 Initials               Date            Modification      
---------------------------------------------------------------------------------------------------------------      
 NR                     2020-06-09      SE2017-997 - Created                           
                           
******/

CREATE PROCEDURE [dbo].[Max_Min_Cu_Invoice_Service_Month_Sel_By_Account_Id]
    (
        @Account_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            cism.CU_INVOICE_ID
            , cism.SERVICE_MONTH
            , cism.Account_ID
            , cism.Begin_Dt
            , cism.End_Dt
        FROM
            dbo.CU_INVOICE_SERVICE_MONTH cism
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism2
                ON cism2.Account_ID = cism.Account_ID
        WHERE
            cism.Account_ID = @Account_Id
            AND cism.SERVICE_MONTH IS NOT NULL
        GROUP BY
            cism.CU_INVOICE_ID
            , cism.SERVICE_MONTH
            , cism.Account_ID
            , cism.Begin_Dt
            , cism.End_Dt
        HAVING
            MAX(cism2.SERVICE_MONTH) = cism.SERVICE_MONTH;


    END;


GO
GRANT EXECUTE ON  [dbo].[Max_Min_Cu_Invoice_Service_Month_Sel_By_Account_Id] TO [CBMSApplication]
GO
