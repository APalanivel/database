
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
    
NAME: dbo.Update_Roles_On_Site_Move    
    
DESCRIPTION: It will updates roles when a site moves from one division to another.  
    
INPUT PARAMETERS:        
 Name     DataType          Default     Description        
-------------------------------------------------------------------------  
 @Site_Id    INT  
 @Old_Division_Id  INT  
 @New_Division_Id  INT        
  
OUTPUT PARAMETERS:  
 Name          DataType          Default     Description    
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
  
BEGIN TRAN  
    EXEC Update_Roles_On_Site_Move 42807,384,12208707  
ROLLBACK TRAN  
  
AUTHOR INITIALS:    
 Initials Name    
------------------------------------------------------------    
 PNR   Pandarinath   
 KVK   Vinay K   
    
MODIFICATIONS    
    
 Initials Date  Modification    
------------------------------------------------------------    
 PNR  11/24/2010 created as part proj : Security Roles Administration.  
 HG   05/12/2011 MAINT-634 fixed to insert the records which are not present in Security_Role_Client_Hier  
       - Added not exists clause in select query which inserts the data in to Security_Role_Client_hier so that it will not select the security role id which are already associated with the Site level client hier id.  
 KVK  06/12/2012 Sp is deleting and insertin into SR table even if there is no cange in Site division.  
 KVK		11/14/2017	added condition to not to update/delete corporate SR when site move happens
******/

CREATE PROCEDURE [dbo].[Update_Roles_On_Site_Move]
      ( @Site_Id INT
      , @Old_Division_Id INT
      , @New_Division_Id INT )
AS
BEGIN

      SET NOCOUNT ON;

      DECLARE
            @Client_Id INT
          , @Site_Client_Hier_Id INT
          , @Old_Div_Client_Hier_Id INT
          , @New_Div_Client_Hier_Id INT;

      SELECT
            @Client_Id = Client_ID
      FROM
            dbo.SITE
      WHERE
            SITE_ID = @Site_Id;

      SELECT
            @Site_Client_Hier_Id = ch.Client_Hier_Id
      FROM
            Core.Client_Hier ch
            JOIN dbo.Code c
                  ON ch.Hier_level_Cd = c.Code_Id
            JOIN dbo.Codeset cs
                  ON cs.Codeset_Id = c.Codeset_Id
      WHERE
            ch.Client_Id = @Client_Id
            AND ch.Site_Id = @Site_Id
            AND cs.Codeset_Name = 'HierLevel'
            AND c.Code_Value = 'Site';

      SELECT
            @Old_Div_Client_Hier_Id = ch.Client_Hier_Id
      FROM
            Core.Client_Hier ch
            JOIN dbo.Code c
                  ON ch.Hier_level_Cd = c.Code_Id
            JOIN dbo.Codeset cs
                  ON cs.Codeset_Id = c.Codeset_Id
      WHERE
            ch.Client_Id = @Client_Id
            AND ch.Sitegroup_Id = @Old_Division_Id
            AND cs.Codeset_Name = 'HierLevel'
            AND c.Code_Value = 'Division';

      SELECT
            @New_Div_Client_Hier_Id = ch.Client_Hier_Id
      FROM
            Core.Client_Hier ch
            JOIN dbo.Code c
                  ON ch.Hier_level_Cd = c.Code_Id
            JOIN dbo.Codeset cs
                  ON cs.Codeset_Id = c.Codeset_Id
      WHERE
            ch.Client_Id = @Client_Id
            AND ch.Sitegroup_Id = @New_Division_Id
            AND cs.Codeset_Name = 'HierLevel'
            AND c.Code_Value = 'Division';

      -- Delete the site's client hier id added to OLD division  

      DELETE sch1
      FROM
            dbo.Security_Role_Client_Hier sch1
            JOIN dbo.Security_Role_Client_Hier sch2
                  ON sch1.Security_Role_Id = sch2.Security_Role_Id
            INNER JOIN dbo.Security_Role sr
                  ON sr.Security_Role_Id = sch1.Security_Role_Id
      WHERE
            sch1.Client_Hier_Id = @Site_Client_Hier_Id
            AND sch2.Client_Hier_Id = @Old_Div_Client_Hier_Id
            AND @Old_Div_Client_Hier_Id != @New_Div_Client_Hier_Id --do only if there is a change in DIV  
            AND sr.Is_Corporate != 1;

      -- Insert th@New_Div_Client_Hier_Ide Site client hier id to all the security role which has access to the new NEW division  
      INSERT INTO dbo.Security_Role_Client_Hier ( Security_Role_Id
                                                , Client_Hier_Id )
                  SELECT
                        srch.Security_Role_Id
                      , @Site_Client_Hier_Id
                  FROM
                        dbo.Security_Role_Client_Hier srch
                        JOIN dbo.Security_Role sr
                              ON sr.Security_Role_Id = srch.Security_Role_Id
                  WHERE
                        sr.Client_Id = @Client_Id
                        AND srch.Client_Hier_Id = @New_Div_Client_Hier_Id
                        AND @Old_Div_Client_Hier_Id != @New_Div_Client_Hier_Id --do only if there is a change in DIV  
                        AND NOT EXISTS (     SELECT
                                                   1
                                             FROM
                                                   dbo.Security_Role_Client_Hier srch1
                                             WHERE
                                                   srch1.Security_Role_Id = srch.Security_Role_Id
                                                   AND srch1.Client_Hier_Id = @Site_Client_Hier_Id )
                        AND sr.Is_Corporate != 1;

END;
;
GO


GRANT EXECUTE ON  [dbo].[Update_Roles_On_Site_Move] TO [CBMSApplication]
GO