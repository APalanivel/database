SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Group_Type_Client_Dtl_Sel_By_State_Commodity_Group_Id        
                
Description:                
		This sproc to get the client details based state commodity.        
                             
 Input Parameters:                
    Name						DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
	@State_Id					INT
	@Commodity_Id				INT
	@Ec_Account_Group_Type_Id   INT
	@Start_Dt					DATE
	@End_Dt						DATE
      
 Output Parameters:                      
    Name					  DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
----------------------------------------------------------------------------------------     

  BEGIN TRANSACTION

SELECT * FROM dbo.Ec_Client_Account_Group ecag
WHERE Account_Group_Name='Test17_11_2016'


SELECT * FROM dbo.Ec_Client_Account_Group_Account ecaga
WHERE Account_Id =688875 

EXEC dbo.Ec_Client_Account_Group_Ins 
      @Client_Id = 11236
     ,@Ec_Account_Group_Type_Id = 3
     ,@Account_Group_Name = 'Test17_11_2016'
     ,@Start_Dt = '2013-11-01'
     ,@End_Dt = '2013-12-01'
     ,@Account_Id = 688875
     ,@User_Info_Id = 49

SELECT * FROM dbo.Ec_Client_Account_Group ecag
WHERE Account_Group_Name='Test17_11_2016'

SELECT * FROM dbo.Ec_Client_Account_Group_Account ecaga
WHERE Account_Id =688875   

ROLLBACK TRANSACTION
     
   
Author Initials:                
    Initials		Name                
----------------------------------------------------------------------------------------                  
	NR				Narayana Reddy                 
 Modifications:                
    Initials        Date			Modification                
----------------------------------------------------------------------------------------                  
    NR				2016-11-15		Created For MAINT-4563.           
               
******/   
CREATE PROCEDURE [dbo].[Ec_Client_Account_Group_Ins]
      ( 
       @Client_Id INT
      ,@Ec_Account_Group_Type_Id INT
      ,@Account_Group_Name NVARCHAR(120)
      ,@Start_Dt DATE = NULL
      ,@End_Dt DATE = NULL
      ,@Account_Id VARCHAR(MAX)
      ,@User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON
      
      DECLARE @Ec_Client_Account_Group_Id INT
      
      INSERT      INTO dbo.Ec_Client_Account_Group
                  ( 
                   Client_Id
                  ,Ec_Account_Group_Type_Id
                  ,Account_Group_Name
                  ,Start_Dt
                  ,End_Dt
                  ,Created_User_Id
                  ,Created_Ts
                  ,Updated_User_Id
                  ,Last_Change_Ts )
                  SELECT
                        @Client_Id
                       ,@Ec_Account_Group_Type_Id
                       ,@Account_Group_Name
                       ,@Start_Dt
                       ,@End_Dt
                       ,@User_Info_Id
                       ,GETDATE()
                       ,@User_Info_Id
                       ,GETDATE()
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          Ec_Client_Account_Group ecag1
                                     WHERE
                                          ecag1.Client_Id = @Client_Id
                                          AND ecag1.Ec_Account_Group_Type_Id = @Ec_Account_Group_Type_Id
                                          AND ecag1.Account_Group_Name = @Account_Group_Name )     
      SELECT
            @Ec_Client_Account_Group_Id = ecag.Ec_Client_Account_Group_Id
      FROM
            Ec_Client_Account_Group ecag
      WHERE
            ecag.Client_Id = @Client_Id
            AND ecag.Ec_Account_Group_Type_Id = @Ec_Account_Group_Type_Id
            AND ecag.Account_Group_Name = @Account_Group_Name
      
      
      INSERT      INTO dbo.Ec_Client_Account_Group_Account
                  ( 
                   Ec_Client_Account_Group_Id
                  ,Account_Id
                  ,Created_User_Id
                  ,Created_Ts )
                  SELECT
                        @Ec_Client_Account_Group_Id
                       ,us.Segments
                       ,@User_Info_Id
                       ,GETDATE()
                  FROM
                        dbo.ufn_split(@Account_Id, ',') us
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          Ec_Client_Account_Group_Account ecaga
                                     WHERE
                                          ecaga.Ec_Client_Account_Group_Id = @Ec_Client_Account_Group_Id
                                          AND ecaga.Account_Id = us.Segments )
                  
               
END


;
GO
GRANT EXECUTE ON  [dbo].[Ec_Client_Account_Group_Ins] TO [CBMSApplication]
GO
