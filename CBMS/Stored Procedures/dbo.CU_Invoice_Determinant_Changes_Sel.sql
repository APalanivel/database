
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	 dbo.CU_Invoice_Determinant_Changes_Sel

DESCRIPTION:
			This procedure is used to select changed records between two versions from CU_INVOICE_DETERMINANT.

INPUT PARAMETERS:
	Name						DataType		Default	Description
------------------------------------------------------------
@Last_Version_Id				BIGINT			Beginnng Change tracking Version Id (From Persistent Variables) 			
@Current_Version_Id				BIGINT			Current Change tracking Version Id

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
It will execute only through the replication replacement SSIS package.

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RK			Raghu Kalvapudi
	RKV         Ravi kumar vegesna
	
MODIFICATIONS
	Initials	Date			Modification
------------------------------------------------------------
	RK			08/01/2013		Created
	KVk			09/17/2014		Modified to have select correct result
	RKV			2015-06-30      Added column EC_Invoice_Sub_Bucket_Master_Id as part of AS400
******/

CREATE PROCEDURE [dbo].[CU_Invoice_Determinant_Changes_Sel]
      ( 
       @Last_Version_Id BIGINT
      ,@Current_Version_Id BIGINT
      ,@CU_I_Last_Version_Id BIGINT )
AS 
BEGIN
      SET NOCOUNT ON;

      SELECT
            CONVERT(CHAR(1), cuid_ct.SYS_CHANGE_OPERATION) Op_Code
           ,cuid_ct.CU_INVOICE_DETERMINANT_ID
           ,NULL CU_INVOICE_ID
           ,NULL COMMODITY_TYPE_ID
           ,NULL UBM_METER_NUMBER
           ,NULL UBM_SERVICE_TYPE_ID
           ,NULL UBM_SERVICE_CODE
           ,NULL UBM_BUCKET_CODE
           ,NULL UNIT_OF_MEASURE_TYPE_ID
           ,NULL UBM_UNIT_OF_MEASURE_CODE
           ,NULL DETERMINANT_NAME
           ,NULL DETERMINANT_VALUE
           ,NULL UBM_INVOICE_DETAILS_ID
           ,NULL CU_DETERMINANT_NO
           ,NULL CU_DETERMINANT_CODE
           ,NULL Bucket_Master_Id
           ,NULL EC_Invoice_Sub_Bucket_Master_Id
      FROM
            changetable(CHANGES dbo.CU_INVOICE_DETERMINANT, @Last_Version_Id) cuid_ct
      WHERE
            cuid_ct.SYS_Change_Version <= @Current_Version_Id
            AND CONVERT(CHAR(1), cuid_ct.SYS_CHANGE_OPERATION) = 'D'
      UNION ALL
      SELECT
            'U' Op_Code
           ,cid.CU_INVOICE_DETERMINANT_ID
           ,cid.CU_INVOICE_ID
           ,cid.COMMODITY_TYPE_ID
           ,cid.UBM_METER_NUMBER
           ,cid.UBM_SERVICE_TYPE_ID
           ,cid.UBM_SERVICE_CODE
           ,cid.UBM_BUCKET_CODE
           ,cid.UNIT_OF_MEASURE_TYPE_ID
           ,cid.UBM_UNIT_OF_MEASURE_CODE
           ,cid.DETERMINANT_NAME
           ,cid.DETERMINANT_VALUE
           ,cid.UBM_INVOICE_DETAILS_ID
           ,cid.CU_DETERMINANT_NO
           ,cid.CU_DETERMINANT_CODE
           ,cid.Bucket_Master_Id
           ,cid.EC_Invoice_Sub_Bucket_Master_Id
      FROM
            dbo.CU_INVOICE_DETERMINANT AS cid
            JOIN ( SELECT
                        cuid.CU_INVOICE_DETERMINANT_ID
                   FROM
                        changetable(CHANGES dbo.CU_INVOICE_DETERMINANT, @Last_Version_Id) cuid_ct
                        INNER JOIN dbo.CU_INVOICE_DETERMINANT cuid
                              ON cuid.CU_INVOICE_DETERMINANT_ID = cuid_ct.CU_INVOICE_DETERMINANT_ID
                        INNER JOIN dbo.CU_INVOICE ci
                              ON ci.CU_INVOICE_ID = cuid.CU_INVOICE_ID
                   WHERE
                        cuid_ct.SYS_Change_Version <= @Current_Version_Id
                        AND ci.IS_PROCESSED = 1
                        AND ci.IS_REPORTED = 1
                   UNION
					--changes from CU_Invoice
                   SELECT
                        cuid.CU_INVOICE_DETERMINANT_ID
                   FROM
                        changetable(CHANGES dbo.CU_INVOICE, @CU_I_Last_Version_Id) cui_ct
                        INNER JOIN dbo.CU_INVOICE cui
                              ON cui.CU_INVOICE_ID = cui_ct.CU_INVOICE_ID
                        INNER JOIN dbo.CU_INVOICE_DETERMINANT cuid
                              ON cui.CU_INVOICE_ID = cuid.CU_INVOICE_ID
                   WHERE
                        cui_ct.SYS_Change_Version <= @Current_Version_Id
                        AND cui.IS_PROCESSED = 1
                        AND cui.IS_REPORTED = 1 ) AS change
                  ON change.CU_INVOICE_DETERMINANT_ID = cid.CU_INVOICE_DETERMINANT_ID

END;

;
GO


GRANT EXECUTE ON  [dbo].[CU_Invoice_Determinant_Changes_Sel] TO [ETL_Execute]
GO
