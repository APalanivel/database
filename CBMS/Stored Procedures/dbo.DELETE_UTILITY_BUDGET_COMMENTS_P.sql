SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE dbo.DELETE_UTILITY_BUDGET_COMMENTS_P

@userId varchar(10),
@sessionId varchar(20),
@commentsId int,
@utilityId int,
@entityId int

as


delete	UTILITY_BUDGET_COMMENTS 
where	UTILITY_BUDGET_COMMENTS_ID = @commentsId
	and	VENDOR_ID = @utilityId
	and	COMMODITY_TYPE_ID  = @entityId
GO
GRANT EXECUTE ON  [dbo].[DELETE_UTILITY_BUDGET_COMMENTS_P] TO [CBMSApplication]
GO
