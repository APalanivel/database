SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********    
NAME: dbo.Holiday_Master_Merge  
  
DESCRIPTION:       
  
 Merges the holiday details into Holiday_Master table.  
 If dates are updated for a holiday, the corresponding dates in TOU_Schedule_Term are also udpated if they are not  
 manually overwritten.  
   
INPUT PARAMETERS:      
Name      DataType     Default Description      
------------------------------------------------------------------------------      
@Holiday_Master_Details  dbo.Holiday_Master_Details  
  
  
OUTPUT PARAMETERS:      
Name   DataType  Default Description      
------------------------------------------------------------------------------  
  
USAGE EXAMPLES:      
------------------------------------------------------------------------------  
  
DECLARE @Holiday_Master_Details Holiday_Master_Details  
INSERT @Holiday_Master_Details (  
 Old_Holiday_Name  
,New_Holiday_Name  
,Year_Identifier  
,Holiday_Dt  
)  
SELECT 'Christmas', 'Christmas', 2012, '12/25/2012'UNION ALL  
SELECT 'Christmas', 'Christmas', 2013, '12/25/2013'UNION ALL  
SELECT 'Christmas', 'Christmas', 2014, '12/25/2014'UNION ALL  
SELECT 'Christmas', 'Christmas', 2015, '12/25/2015'UNION ALL  
SELECT 'Christmas', 'Christmas', 2016, '12/25/2016'  
  
EXEC Holiday_Master_Merge @Holiday_Master_Details, 4  
  
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
CPE   Chaitanya Panduga Eshwar  
   
MODIFICATIONS       
Initials Date  Modification      
------------------------------------------------------------      
CPE   2012-07-10 Created       
      
******/  

CREATE PROC dbo.Holiday_Master_Merge  
(  
      @Holiday_Master_Details Holiday_Master_Details READONLY  
     ,@Country_Id INT  
)  
AS   
BEGIN  
  
      SET NOCOUNT ON  
  
      BEGIN TRY  
            BEGIN TRAN  
            MERGE INTO dbo.Holiday_Master tgt   
            USING ( SELECT  
                        Old_Holiday_Name
                       ,New_Holiday_Name  
                       ,Year_Identifier  
                       ,Holiday_Dt  
                   FROM  
                        @Holiday_Master_Details ) src  
				ON tgt.Country_Id = @Country_Id  
                   AND src.Year_Identifier = tgt.Year_Identifier  
				   AND src.Old_Holiday_Name = tgt.Holiday_Name  
			WHEN NOT MATCHED AND src.Holiday_Dt IS NOT NULL THEN   
			INSERT ( Holiday_Name, Year_Identifier, Holiday_Dt, Country_Id )  
			VALUES  
			(  
				src.New_Holiday_Name  
			   ,src.Year_Identifier  
			   ,src.Holiday_Dt  
			   ,@Country_Id )   
			WHEN MATCHED AND src.Holiday_Dt IS NOT NULL THEN  
			UPDATE  
			 SET Holiday_Dt = src.Holiday_Dt  
			WHEN MATCHED AND src.Holiday_Dt IS NULL THEN  -- Deletes the dates that are blanked out
			DELETE  
			;  
  
		   -- Update the holiday names if they are edited to all the years
		   UPDATE
			  HM
		   SET
			  HM.Holiday_Name = HMD.New_Holiday_Name
		   FROM
			  dbo.Holiday_Master HM
			  JOIN ( SELECT
						  Old_Holiday_Name
						 ,New_Holiday_Name
					 FROM
						  @Holiday_Master_Details
					 GROUP BY
						  Old_Holiday_Name
						 ,New_Holiday_Name ) HMD
					ON HM.Holiday_Name = HMD.Old_Holiday_Name
					   AND HM.COUNTRY_ID = @Country_Id
  
			-- Delete all the holidays that are deleted using the Delete link
            DELETE  
                  HM  
            FROM  
                  dbo.Holiday_Master HM  
            WHERE  
                  HM.COUNTRY_ID = @Country_Id  
                  AND HM.Holiday_Name NOT IN ( SELECT  
                                                New_Holiday_Name  
                                               FROM  
                                                @Holiday_Master_Details  
                                               GROUP BY  
												New_Holiday_Name )  
     
		    -- Add the newly added holiday to the terms which already have the Holiday name added and whose end date  
		    -- is either NULL or >= the new holiday date  
            INSERT  
                  dbo.Time_Of_Use_Schedule_Term_Holiday  
                  (  
                   Time_Of_Use_Schedule_Term_Id  
                  ,Holiday_Master_Id  
                  ,Holiday_Dt  
                  ,Is_Manual_Override  
                  ,Is_Rule_One_Active  
                  ,Is_Rule_Two_Active )  
                  SELECT  
                        TOUST.Time_Of_Use_Schedule_Term_Id  
                       ,HM_All.Holiday_Master_Id  
                       ,CASE WHEN TOUSTH.Is_Rule_One_Active = 1  
                                  AND DATENAME(dw, HM_All.Holiday_Dt) = 'Saturday' THEN DATEADD(dd, -1, HM_All.Holiday_Dt)  
                             WHEN TOUSTH.Is_Rule_Two_Active = 1  
                                  AND DATENAME(dw, HM_All.Holiday_Dt) = 'Sunday' THEN DATEADD(dd, 1, HM_All.Holiday_Dt)  
                             ELSE HM_All.Holiday_Dt  
                        END  
                       ,TOUSTH.Is_Manual_Override  
                       ,TOUSTH.Is_Rule_One_Active  
                       ,TOUSTH.Is_Rule_Two_Active  
                  FROM  
                        dbo.Time_Of_Use_Schedule_Term TOUST  
                        JOIN dbo.Time_Of_Use_Schedule_Term_Holiday TOUSTH  
                              ON TOUST.Time_Of_Use_Schedule_Term_Id = TOUSTH.Time_Of_Use_Schedule_Term_Id  
                        JOIN dbo.Holiday_Master HM  
                              ON TOUSTH.Holiday_Master_Id = HM.Holiday_Master_Id  
                        JOIN dbo.Holiday_Master HM_All  
                              ON HM.Holiday_Name = HM_All.Holiday_Name  
                                 AND HM.COUNTRY_ID = HM_All.COUNTRY_ID  
                                 AND HM.Year_Identifier = HM_All.Year_Identifier  
                        JOIN @Holiday_Master_Details HMD  
                              ON HM.Holiday_Name = HMD.New_Holiday_Name  
                                 AND HM.COUNTRY_ID = @Country_Id  
                                 AND HM.Year_Identifier = HMD.Year_Identifier  
                  WHERE  
                        ( ( TOUST.End_Dt IS NULL  
                            OR Year(TOUST.End_Dt) >= YEAR(HMD.Holiday_Dt) )  
                          OR YEAR(TOUST.Start_Dt) <= YEAR(HMD.Holiday_Dt) )  
                        AND NOT EXISTS ( SELECT  
                                          1  
                                         FROM  
                                          dbo.Time_Of_Use_Schedule_Term_Holiday  
                                         WHERE  
                                          Holiday_Master_Id = HM_All.Holiday_Master_Id  
                                          AND Time_Of_Use_Schedule_Term_Id = TOUSTH.Time_Of_Use_Schedule_Term_Id )  
                  GROUP BY  
                        TOUST.Time_Of_Use_Schedule_Term_Id  
                       ,HM_All.Holiday_Master_Id  
                       ,HM_All.Holiday_Dt  
                       ,TOUSTH.Is_Manual_Override  
                       ,TOUSTH.Is_Rule_One_Active  
                       ,TOUSTH.Is_Rule_Two_Active  
         
			-- Update the dates for the terms if it is not set as Manually overridden  
            UPDATE  
                  TOUSTH  
            SET     
                  Holiday_Dt = CASE WHEN TOUSTH.Is_Rule_One_Active = 1  
                                         AND DATENAME(dw, HM.Holiday_Dt) = 'Saturday' THEN DATEADD(dd, -1, HM.Holiday_Dt)  
                                    WHEN TOUSTH.Is_Rule_Two_Active = 1  
                                         AND DATENAME(dw, HM.Holiday_Dt) = 'Sunday' THEN DATEADD(dd, 1, HM.Holiday_Dt)  
                                    ELSE HM.Holiday_Dt  
                               END  
            FROM  
                  dbo.Time_Of_Use_Schedule_Term_Holiday TOUSTH  
                  JOIN dbo.Holiday_Master HM  
           ON TOUSTH.Holiday_Master_Id = HM.Holiday_Master_Id  
                  JOIN @Holiday_Master_Details HMD  
             ON HM.Holiday_Name = HMD.New_Holiday_Name  
                           AND HM.COUNTRY_ID = @Country_Id  
                           AND HM.Year_Identifier = HMD.Year_Identifier  
            WHERE  
                  TOUSTH.Is_Manual_Override = 0  
                  AND TOUSTH.Holiday_Dt <> CASE WHEN TOUSTH.Is_Rule_One_Active = 1  
                                                     AND DATENAME(dw, HM.Holiday_Dt) = 'Saturday' THEN DATEADD(dd, -1, HM.Holiday_Dt)  
                                                WHEN TOUSTH.Is_Rule_Two_Active = 1  
                                                     AND DATENAME(dw, HM.Holiday_Dt) = 'Sunday' THEN DATEADD(dd, 1, HM.Holiday_Dt)  
                                                ELSE HM.Holiday_Dt  
                                           END  
   
            COMMIT TRAN   
      END TRY  
  
      BEGIN CATCH  
            IF @@TranCount > 0   
                  ROLLBACK TRAN  
            EXEC usp_RethrowError  
      END CATCH  
END  
  
  
  
  


;
GO
GRANT EXECUTE ON  [dbo].[Holiday_Master_Merge] TO [CBMSApplication]
GO
