SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_LP_GET_DETERMINANT_VALUE_CNT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@user_id       	varchar(10)	          	
	@session_id    	varchar(20)	          	
	@determinant_id	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE dbo.SR_RFP_LP_GET_DETERMINANT_VALUE_CNT_P
	@user_id varchar(10),
	@session_id varchar(20),
	@determinant_id int
	AS
set nocount on
	select COUNT(sr_rfp_lp_determinant_values_id)as determinant_values_count
	from sr_rfp_lp_determinant_values(nolock)
	where sr_rfp_load_profile_determinant_id = @determinant_id
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_LP_GET_DETERMINANT_VALUE_CNT_P] TO [CBMSApplication]
GO
