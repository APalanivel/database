SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/******
NAME:
	CBMS.dbo.Cost_Usage_Account_Dtl_Comment_Map_Ins

DESCRIPTION:
	Used to save the variance comments added for the given account, commodity and service month.

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Account_Id			Int
	@Commodity_Id		Int
	@Service_Month		Date
	@Comment_ID			Int

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	
	EXEC dbo.Cost_Usage_Account_Dtl_Comment_Map_Ins 158038,67,'2010-03-01',371792

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hari
	AP			Arunkumar Palanivel
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG        	03/18/2010	Created
	AP			feb 21, 2020 Modified

******/

CREATE PROCEDURE [dbo].[Cost_Usage_Account_Dtl_Comment_Map_Ins]
	@Account_Id			INT
	,@Commodity_Id		INT
	,@Service_Month		DATE
	,@Comment_ID		INT
	,@inserted_value INT OUTPUT
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO dbo.Cost_Usage_Account_Dtl_Comment_Map(Account_Id
		,Commodity_Id
		,Service_Month
		,Comment_ID)
	VALUES(@Account_Id
		,@Commodity_Id
		,@Service_Month
		,@Comment_ID)

		SET    @inserted_value = SCOPE_IDENTITY()

END
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Dtl_Comment_Map_Ins] TO [CBMSApplication]
GO
