SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: [dbo].[Cu_Invoice_Account_Commodity_Recalc_Contract_Merge]           
                        
 DESCRIPTION:                        
			To Update and insert data for Multiple contractes recalculations.                  
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@tvp_Cu_Invoice_Account_Commodity_Recalc_Contract tvp_Cu_Invoice_Account_Commodity_Recalc_Contract READONLY
@Cu_Invoice_Id				INT
@Account_Id					INT
@Commodity_Id				INT
@User_Info_Id				INT 

                      
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 
 BEGIN TRAN

 DECLARE @tvp_Cu_Invoice_Account_Commodity_Recalc_Contract tvp_Cu_Invoice_Account_Commodity_Recalc_Contract;  
  
 INSERT     INTO @tvp_Cu_Invoice_Account_Commodity_Recalc_Contract
            ( [Contract_Id], [Contract_Recalc_Begin_Dt], [Contract_Recalc_End_Dt] )
 VALUES
            ( 36551, '2008-01-01', '2009-12-31' )
            ,
            ( 81982, '2009-01-01', '2010-12-31' )
           

SELECT * FROM dbo.Cu_Invoice_Account_Commodity_Recalc_Contract WHERE Cu_Invoice_Account_Commodity_Id = 24 
           
     
      
 EXEC [dbo].[Cu_Invoice_Account_Commodity_Recalc_Contract_Merge] 
      @tvp_Cu_Invoice_Account_Commodity_Recalc_Contract =@tvp_Cu_Invoice_Account_Commodity_Recalc_Contract  
      ,@Cu_Invoice_Id = 32227387
      ,@Account_Id =55262
      ,@Commodity_Id =290
      ,@User_Info_Id  = 49
      
SELECT * FROM dbo.Cu_Invoice_Account_Commodity_Recalc_Contract WHERE Cu_Invoice_Account_Commodity_Id = 24 

ROLLBACK TRAN
      

                        
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 NR		               Narayana Reddy
 
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 NR                    2017-10-17       Created for SE2017-263.               
                       
******/                 
CREATE   PROCEDURE [dbo].[Cu_Invoice_Account_Commodity_Recalc_Contract_Merge]
      ( 
       @tvp_Cu_Invoice_Account_Commodity_Recalc_Contract tvp_Cu_Invoice_Account_Commodity_Recalc_Contract READONLY
      ,@Cu_Invoice_Id INT
      ,@Account_Id INT
      ,@Commodity_Id INT
      ,@User_Info_Id INT )
AS 
BEGIN                
      SET NOCOUNT ON;    
    
      DECLARE @Cu_Invoice_Account_Commodity_Id INT
      
   
      SELECT
            @Cu_Invoice_Account_Commodity_Id = Cu_Invoice_Account_Commodity_Id
      FROM
            dbo.Cu_Invoice_Account_Commodity ciac
      WHERE
            Cu_Invoice_Id = @Cu_Invoice_Id
            AND Account_Id = @Account_Id
            AND Commodity_Id = @Commodity_Id
   
                   
      IF @Cu_Invoice_Account_Commodity_Id IS NULL 
            BEGIN 
     
                  DECLARE @Cu_Invoice_Account_Commodity_Id_Out INT 
                  
                  EXEC Cu_Invoice_Account_Commodity_Ins 
                        @Cu_Invoice_Id = @Cu_Invoice_Id
                       ,@Account_Id = @Account_Id
                       ,@Commodity_Id = @Commodity_Id
                       ,@Cu_Invoice_Account_Commodity_Id = @Cu_Invoice_Account_Commodity_Id OUTPUT     
                        
                     
            END
            
                        
      MERGE INTO dbo.Cu_Invoice_Account_Commodity_Recalc_Contract AS tgt
            USING 
                  ( SELECT
                        @Cu_Invoice_Account_Commodity_Id AS Cu_Invoice_Account_Commodity_Id
                       ,tvp.Contract_Id
                       ,tvp.Contract_Recalc_Begin_Dt
                       ,tvp.Contract_Recalc_End_Dt
                    FROM
                        @tvp_Cu_Invoice_Account_Commodity_Recalc_Contract tvp ) AS src
            ON  tgt.Cu_Invoice_Account_Commodity_Id = src.Cu_Invoice_Account_Commodity_Id
                  AND tgt.Contract_Id = src.Contract_Id
            WHEN MATCHED AND src.Contract_Recalc_Begin_Dt IS NOT NULL
                  AND src.Contract_Recalc_End_Dt IS NOT NULL
                  THEN UPDATE
                    SET 
                        Contract_Recalc_Begin_Dt = src.Contract_Recalc_Begin_Dt
                       ,Contract_Recalc_End_Dt = src.Contract_Recalc_End_Dt
                       ,Updated_User_Id = @User_Info_Id
                       ,Last_Change_Ts = GETDATE()
            WHEN NOT MATCHED AND src.Contract_Recalc_Begin_Dt IS NOT NULL
                  AND src.Contract_Recalc_End_Dt IS NOT NULL
                  THEN
            INSERT
                        ( 
                         Cu_Invoice_Account_Commodity_Id
                        ,Contract_Id
                        ,Contract_Recalc_Begin_Dt
                        ,Contract_Recalc_End_Dt
                        ,Created_User_Id
                        ,Created_Ts
                        ,Updated_User_Id
                        ,Last_Change_Ts )
                    VALUES
                        ( 
                         src.Cu_Invoice_Account_Commodity_Id
                        ,src.Contract_Id
                        ,src.Contract_Recalc_Begin_Dt
                        ,src.Contract_Recalc_End_Dt
                        ,@User_Info_Id
                        ,GETDATE()
                        ,@User_Info_Id
                        ,GETDATE() )
            WHEN  MATCHED AND src.Contract_Recalc_Begin_Dt IS  NULL
                  AND src.Contract_Recalc_End_Dt IS  NULL
                  THEN
            DELETE;  

                        
     
END

;
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Account_Commodity_Recalc_Contract_Merge] TO [CBMSApplication]
GO
