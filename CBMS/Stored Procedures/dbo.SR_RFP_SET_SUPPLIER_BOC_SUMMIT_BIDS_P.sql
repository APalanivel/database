SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.GET_UPDATE_SITE_INFO_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@bidId                     int,
@delivery                  varchar(200),
@transServiceLevelId       int,
@supplierNominationId      int,
@supplierBalancingId       int,
@summitComments            varchar(4000),

@deliveryPowerId           int,
@transServiceLevelPowerId  int,

@selectedProductId         int,
@accountTermId             int,
@productName               varchar(200),
@supplierContactId         INT,
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    
@bidIdentityId             int


USAGE EXAMPLES:
------------------------------------------------------------
--exec DBO.GET_UPDATE_SITE_INFO_P_M 
--@site_id = 29

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[SR_RFP_SET_SUPPLIER_BOC_SUMMIT_BIDS_P] 

@bidId int,
@delivery varchar(200),
@transServiceLevelId int,
@supplierNominationId int,
@supplierBalancingId int,
@summitComments varchar(4000),

@deliveryPowerId int,
@transServiceLevelPowerId int,

@selectedProductId int,
@accountTermId int,
@productName varchar(200),
@supplierContactId INT,
@bidIdentityId int output

AS
	
SET NOCOUNT ON

if @bidId=0

BEGIN
 
	
		INSERT INTO SR_RFP_BID_REQUIREMENTS 
		(
			DELIVERY_POINT,
			TRANSPORTATION_LEVEL_TYPE_ID,
			NOMINATION_TYPE_ID,
			BALANCING_TYPE_ID,
			COMMENTS,
			DELIVERY_POINT_POWER_TYPE_ID,
			SERVICE_LEVEL_POWER_TYPE_ID
		)  
		
		VALUES
		
		(
			@delivery,
			@transServiceLevelId,
			@supplierNominationId,
			@supplierBalancingId,
			@summitComments,
			@deliveryPowerId ,
			@transServiceLevelPowerId 
			
		)
		
		SELECT @bidIdentityId = SCOPE_IDENTITY()
	
END

--UPDATE operation

else if @bidId>0

begin
		
		UPDATE 
			SR_RFP_BID_REQUIREMENTS SET 
			DELIVERY_POINT=@delivery,
			TRANSPORTATION_LEVEL_TYPE_ID=@transServiceLevelId,
			NOMINATION_TYPE_ID=@supplierNominationId,
			BALANCING_TYPE_ID=@supplierBalancingId,
			COMMENTS=@summitComments,
			DELIVERY_POINT_POWER_TYPE_ID=@deliveryPowerId,
			SERVICE_LEVEL_POWER_TYPE_ID=@transServiceLevelPowerId
		WHERE
			SR_RFP_BID_REQUIREMENTS_ID=@bidId


		UPDATE  
			SR_RFP_BID 
		SET 
			PRODUCT_NAME=@productName 
		WHERE 
			SR_RFP_BID_REQUIREMENTS_ID=@bidId

			
end

return
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_SET_SUPPLIER_BOC_SUMMIT_BIDS_P] TO [CBMSApplication]
GO
