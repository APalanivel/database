
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_CURRENT_MONTH_CONTRACT_NOTIFICATIONS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@user_id       		int       	          	
	@month_identifier	datetime  	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

    EXEC dbo.SR_SAD_GET_CURRENT_MONTH_CONTRACT_NOTIFICATIONS_P 20713, '2013-11-01' 
	EXEC dbo.SR_SAD_GET_CURRENT_MONTH_CONTRACT_NOTIFICATIONS_P 24991, '2013-11-01'
	EXEC dbo.SR_SAD_GET_CURRENT_MONTH_CONTRACT_NOTIFICATIONS_P 36847,'2013-11-28'
	EXEC dbo.SR_SAD_GET_CURRENT_MONTH_CONTRACT_NOTIFICATIONS_P 35684,'2014-04-01'
	EXEC dbo.SR_SAD_GET_CURRENT_MONTH_CONTRACT_NOTIFICATIONS_P 26958,'2016-04-01'



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
    AKR         Ashok Kumar Raju
    RR			Raghu Reddy
    
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	DMR			09/10/2010	Modified for Quoted_Identifier
	HG			01/14/2011	Re-write the whole script to fix performance bug #21922.
							Eliminated Temp table that contained all sites with no filter, scaler function in select clause,
							and use of views joining back to base tables. 
	AKR         2012-10-29  Modified the code to get the data based on Analyst Mapping Cd
	RR			2013-11-22	MAINT-2155 Selecting contract type to validate expiring contracts based on type also.
							Script is not differentiating type and if supplier contract is expiring but have future utility contract
							and this expiring supplier contract is not displaying in dashboard and vice versa.
	RR			2014-03-19	MAINT-2619 Script not returning/counting the contract expiring account(meter's account) if the account is having 
							active RFP for different commodity. Added commodity check condition.
	RR			2016-04-02	Global Sourcing - Phase3 - GCS-505 Script modified to send/display notifications for all commodities, currently
							notifications works for EP&NG only						
******/

CREATE PROCEDURE [dbo].[SR_SAD_GET_CURRENT_MONTH_CONTRACT_NOTIFICATIONS_P]
      ( 
       @user_id INT
      ,@month_identifier DATETIME )
AS 
BEGIN

      SET NOCOUNT ON;
    
      DECLARE @month_end_identifier DATETIME        
        
      DECLARE
            @Rfp_Closed_Action_Type_id INT
           ,@Rfp_Closed_Bid_Status_Type_id INT
           ,@Default_Analyst INT
           ,@Custom_Analyst INT
           ,@EP_commodity_type_id INT
           ,@NG_commodity_type_id INT    
        
      DECLARE @Rfp_Account TABLE
            ( 
             Account_id INT
            ,COMMODITY_TYPE_ID INT )        
        
      DECLARE @t_contract_notification TABLE
            ( 
             vendor_name VARCHAR(200)
            ,client_name VARCHAR(200)
            ,site_name VARCHAR(500)
            ,account_number VARCHAR(50)
            ,Account_Id INT
            ,meter_id INT
            ,contract_id INT
            ,ED_Contract_Number VARCHAR(150)
            ,Contract_Start_date DATETIME
            ,Contract_End_Date DATETIME
            ,CONTRACT_TYPE_ID INT
            ,Commodity_Id INT )        
   
      SET @month_end_identifier = dateadd(mm, 1, @month_identifier) - 1        
        
      SELECT
            @Rfp_Closed_Action_Type_id = entity_id
      FROM
            dbo.Entity
      WHERE
            entity_type = 1014
            AND entity_name = 'Closed - Return to Utility'       
                  
                  
      SELECT
            @Default_Analyst = max(case WHEN c.Code_Value = 'Default' THEN c.Code_Id
                                   END)
           ,@Custom_Analyst = max(case WHEN c.Code_Value = 'Custom' THEN c.Code_Id
                                  END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Analyst Type'     
                
      SELECT
            @EP_commodity_type_id = max(case WHEN com.Commodity_Name = 'Electric Power' THEN com.Commodity_Id
                                        END)
           ,@NG_commodity_type_id = max(case WHEN com.Commodity_Name = 'Natural Gas' THEN com.Commodity_Id
                                        END)
      FROM
            dbo.Commodity com
      WHERE
            com.Commodity_Name IN ( 'Electric Power', 'Natural Gas' )       
                 
                    
        
      SELECT
            @Rfp_Closed_Bid_Status_Type_id = entity_id
      FROM
            dbo.Entity
      WHERE
            Entity_Type = 1029
            AND entity_name = 'Closed';
      WITH  cte_Accouts
              AS ( SELECT
                        ucha.Account_Vendor_Name
                       ,ch.Client_Name
                       ,rtrim(CH.City) + ', ' + CH.State_Name + ' (' + CH.Site_name + ')' Site_Name
                       ,ucha.Account_Number
                       ,ucha.Account_Id
                       ,ucha.Meter_Id
                       ,con.Contract_Id
                       ,con.ED_CONTRACT_NUMBER
                       ,con.CONTRACT_START_DATE
                       ,con.CONTRACT_END_DATE
                       ,coalesce(UCHA.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) Analyst_Mapping_Cd
                       ,UCHA.Account_Vendor_Id
                       ,UCHA.Commodity_Id
                       ,ch.Client_Id
                       ,ch.Site_Id
                       ,con.CONTRACT_TYPE_ID
                   FROM
                        Core.Client_Hier CH
                        INNER JOIN Core.Client_Hier_Account UCHA
                              ON CH.Client_Hier_Id = UCHA.Client_Hier_Id
                        INNER JOIN dbo.supplier_account_meter_map SAMM
                              ON UCHA.Meter_Id = SAMM.METER_ID
                        INNER JOIN dbo.Contract CON
                              ON SAMM.Contract_ID = CON.CONTRACT_ID
                   WHERE
                        ucha.Account_Type = 'Utility'
                        AND UCHA.Account_Service_level_Cd != 1024
                        AND ch.site_Id > 0
                        AND ucha.Account_Not_Managed = 0
                        AND samm.Is_History = 0
                        --AND con.COMMODITY_TYPE_ID IN ( @EP_commodity_type_id, @NG_commodity_type_id )
                        AND con.Contract_End_Date >= @month_identifier),
            cte_Account_User
              AS ( SELECT
                        acc.Account_Vendor_Name
                       ,acc.Client_Name
                       ,acc.Site_Name
                       ,acc.Account_Number
                       ,acc.Account_Id
                       ,acc.Meter_Id
                       ,acc.Contract_Id
                       ,acc.ED_CONTRACT_NUMBER
                       ,acc.CONTRACT_START_DATE
                       ,acc.CONTRACT_TYPE_ID
                       ,acc.CONTRACT_END_DATE
                       ,case WHEN acc.Analyst_Mapping_Cd = @Default_Analyst THEN vcam.Analyst_Id
                             WHEN acc.Analyst_Mapping_Cd = @Custom_Analyst THEN coalesce(aca.Analyst_User_Info_Id, sca.Analyst_User_Info_Id, cca.Analyst_User_Info_Id)
                        END AS Analyst_Id
                       ,acc.Commodity_Id
                   FROM
                        cte_Accouts acc
                        LEFT JOIN dbo.VENDOR_COMMODITY_MAP vcm
                              ON acc.Account_Vendor_Id = vcm.VENDOR_ID
                                 AND acc.Commodity_Id = vcm.COMMODITY_TYPE_ID
                        LEFT JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                              ON vcm.VENDOR_COMMODITY_MAP_ID = vcam.Vendor_Commodity_Map_Id
                        JOIN core.Client_Commodity ccc
                              ON ccc.Client_Id = acc.Client_Id
                                 AND ccc.Commodity_Id = acc.COMMODITY_ID
                        LEFT JOIN dbo.Account_Commodity_Analyst aca
                              ON aca.Account_Id = acc.Account_Id
                                 AND aca.Commodity_Id = acc.COMMODITY_ID
                        LEFT JOIN dbo.SITE_Commodity_Analyst sca
                              ON sca.Site_Id = acc.Site_Id
                                 AND sca.Commodity_Id = acc.COMMODITY_ID
                        LEFT JOIN dbo.Client_Commodity_Analyst cca
                              ON ccc.Client_Commodity_Id = cca.Client_Commodity_Id
                   GROUP BY
                        acc.Account_Vendor_Name
                       ,acc.Client_Name
                       ,acc.Site_Name
                       ,acc.Account_Number
                       ,acc.Account_Id
                       ,acc.Meter_Id
                       ,acc.Contract_Id
                       ,acc.ED_CONTRACT_NUMBER
                       ,acc.CONTRACT_START_DATE
                       ,acc.CONTRACT_END_DATE
                       ,vcam.Analyst_Id
                       ,acc.Analyst_Mapping_Cd
                       ,aca.Analyst_User_Info_Id
                       ,sca.Analyst_User_Info_Id
                       ,cca.Analyst_User_Info_Id
                       ,acc.CONTRACT_TYPE_ID
                       ,acc.Commodity_Id)
            INSERT      INTO @t_contract_notification
                        ( 
                         Vendor_Name
                        ,Client_Name
                        ,Site_Name
                        ,Account_Number
                        ,Account_Id
                        ,Meter_Id
                        ,Contract_Id
                        ,Ed_Contract_Number
                        ,Contract_Start_Date
                        ,Contract_End_Date
                        ,CONTRACT_TYPE_ID
                        ,Commodity_Id )
                        SELECT
                              Account_Vendor_Name
                             ,Client_Name
                             ,Site_Name
                             ,Account_Number
                             ,Account_Id
                             ,Meter_Id
                             ,Contract_Id
                             ,ED_CONTRACT_NUMBER
                             ,CONTRACT_START_DATE
                             ,CONTRACT_END_DATE
                             ,CONTRACT_TYPE_ID
                             ,Commodity_Id
                        FROM
                              cte_Account_User cau
                        WHERE
                              cau.Analyst_Id = @User_Id    
     
        
      INSERT      INTO @Rfp_Account
                  ( 
                   Account_id
                  ,COMMODITY_TYPE_ID )
                  SELECT
                        rfp_account.account_id
                       ,rfp.COMMODITY_TYPE_ID
                  FROM
                        dbo.sr_rfp_closure rfp_closure
                        INNER JOIN dbo.SR_Rfp_Account rfp_account
                              ON rfp_closure.sr_account_group_id = rfp_account.sr_rfp_account_id
                        INNER JOIN @t_contract_notification cn
                              ON cn.Account_Id = rfp_account.Account_id
                        INNER JOIN dbo.SR_RFP rfp
                              ON rfp_account.SR_RFP_ID = rfp.SR_RFP_ID
                  WHERE
                        rfp_closure.close_action_type_id = @Rfp_Closed_Action_Type_id
                  GROUP BY
                        rfp_account.account_id
                       ,rfp.COMMODITY_TYPE_ID      
        
      INSERT      INTO @Rfp_Account
                  ( 
                   Account_id
                  ,COMMODITY_TYPE_ID )
                  SELECT
                        rfp_account.account_id
                       ,rfp.COMMODITY_TYPE_ID
                  FROM
                        dbo.sr_rfp_account rfp_account
                        INNER JOIN @t_contract_notification cn
                              ON cn.Account_Id = rfp_account.Account_id
                        INNER JOIN dbo.SR_RFP rfp
                              ON rfp_account.SR_RFP_ID = rfp.SR_RFP_ID
                  WHERE
                        rfp_account.bid_status_type_id != @Rfp_Closed_Bid_Status_Type_id
                        AND rfp_account.is_deleted = 0
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          @Rfp_Account ra
                                         WHERE
                                          ra.Account_Id = rfp_account.Account_Id )        
                                          
                                         
      
      SELECT
            cn.vendor_name
           ,cn.client_name
           ,cn.site_name
           ,cn.account_number
           ,cn.ed_contract_number
           ,cn.contract_id
           ,cn.contract_end_date
      FROM
            @t_contract_notification cn
            INNER JOIN ( SELECT
                              Meter_ID
                             ,max(CONTRACT_END_DATE) MaxEndDate
                             ,CONTRACT_TYPE_ID
                         FROM
                              @t_contract_notification
                         GROUP BY
                              Meter_ID
                             ,CONTRACT_TYPE_ID ) T2
                  ON t2.Meter_ID = cn.meter_ID
                     AND t2.MaxEndDate = cn.CONTRACT_END_DATE
                     AND cn.CONTRACT_TYPE_ID = t2.CONTRACT_TYPE_ID
            LEFT JOIN @Rfp_Account ra
                  ON ra.Account_Id = cn.Account_Id
                     AND cn.Commodity_Id = ra.COMMODITY_TYPE_ID
      WHERE
            ra.Account_Id IS NULL
            AND cn.CONTRACT_END_DATE BETWEEN @month_identifier
                                     AND     @month_end_identifier
      GROUP BY
            cn.vendor_name
           ,cn.client_name
           ,cn.site_name
           ,cn.account_number
           ,cn.ed_contract_number
           ,cn.contract_id
           ,cn.contract_end_date
           ,cn.CONTRACT_TYPE_ID      

END;
;

;

;
GO




GRANT EXECUTE ON  [dbo].[SR_SAD_GET_CURRENT_MONTH_CONTRACT_NOTIFICATIONS_P] TO [CBMSApplication]
GO
