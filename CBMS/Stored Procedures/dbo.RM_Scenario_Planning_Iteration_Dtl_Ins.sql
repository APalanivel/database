SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                 
/******                        
 NAME: dbo.RM_Scenario_Planning_Iteration_Dtl_Ins            
                        
 DESCRIPTION:                        
			To Insert data into RM_Scenario_Planning_Iteration_Dtl table.                        
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@RM_Scenario_Planning_Iteration_Id   INT
,@Service_Month						DATE
,@Scenario_Input_Name_Cd			 INT
,@Scenario_Input_Value				DECIMAL(32,16)
,@Created_User_Id					 INT						              
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
BEGIN TRAN 
EXEC dbo.RM_Scenario_Planning_Iteration_Dtl_Ins 
      @RM_Scenario_Planning_Iteration_ID = 1
      ,@Service_Month='2014-02-01'
     ,@Scenario_Input_Name_Cd = 100721
     ,@Scenario_Input_Value = 20
     ,@Created_User_Id = 49   
SELECT * FROM RM_Scenario_Planning_Iteration_Dtl WHERE RM_Scenario_Planning_Iteration_ID=1

ROLLBACK
        
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2014-10-01       Created                
                       
******/                
          
CREATE PROCEDURE [dbo].[RM_Scenario_Planning_Iteration_Dtl_Ins]
      ( 
       @RM_Scenario_Planning_Iteration_Id INT
      ,@Service_Month DATE
      ,@Scenario_Input_Name_Cd INT
      ,@Scenario_Input_Value DECIMAL(32, 16)
      ,@Created_User_Id INT )
AS 
BEGIN                
      SET NOCOUNT ON;     
      
      INSERT      INTO [dbo].[RM_Scenario_Planning_Iteration_Dtl]
                  ( 
                   [RM_Scenario_Planning_Iteration_Id]
                  ,[Service_Month]
                  ,[Scenario_Input_Name_Cd]
                  ,[Scenario_Input_Value]
                  ,[Created_User_Id]
                  ,[Created_Ts]
                  ,[Updated_User_Id]
                  ,[Last_Change_Ts] )
      VALUES
                  ( 
                   @RM_Scenario_Planning_Iteration_Id
                  ,@Service_Month
                  ,@Scenario_Input_Name_Cd
                  ,@Scenario_Input_Value
                  ,@Created_User_Id
                  ,GETDATE()
                  ,@Created_User_Id
                  ,GETDATE() )
                  
                                                                   
                                 
                       
END 
;
GO
GRANT EXECUTE ON  [dbo].[RM_Scenario_Planning_Iteration_Dtl_Ins] TO [CBMSApplication]
GO
