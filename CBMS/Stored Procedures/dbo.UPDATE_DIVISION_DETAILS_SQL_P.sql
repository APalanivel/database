SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.UPDATE_DIVISION_DETAILS_SQL_P


DESCRIPTION:

INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
	@SBA_TYPE_ID 	 		               integer 	 		,
	@PRICE_INDEX_ID 	 		            integer 	 		,
	@CLIENT_ID 	 		                  integer 	 		,
	@TERM_PREFERRED_TYPE_ID 	 		   integer 	 		,
	@CONTRACT_REVIEWER_TYPE_ID 	 		integer,
	@DECISION_MAKER_TYPE_ID 	 		   integer,
	@SIGNATORY_TYPE_ID 	 		         integer,
	@DIVISION_NAME 	 		            varchar(200),
	@IS_INTEREST_MINORITY_SUPPLIERS 	 	BIT,
	@IS_CORPORATE_HEDGE 	 		         BIT,
	@NAICS_CODE 	 		               varchar(30) 	 		,
	@TAX_NUMBER 	 		               varchar(30),
	@DUNS_NUMBER 	 		               varchar(30),
	@Not_Managed 	 		               BIT,
	@MISC_COMMENTS 	 		            varchar(4000),
	@TRIGGER_RIGHTS 	 		            BIT,
	@CONTRACTING_ENTITY 	 		         varchar(200),
	@CLIENT_LEGAL_STRUCTURE 	 		   varchar(4000),
	@division_id 	 		               integer,
	@Not_Managed_By_Id 	 		         integer,
	@Not_Managed_Date 	 		         datetime
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    

USAGE EXAMPLES:
------------------------------------------------------------
exec dbo.UPDATE_DIVISION_DETAILS_SQL_P
	@SBA_TYPE_ID = 459,
	@PRICE_INDEX_ID = 464 ,
	@CLIENT_ID = 1043 ,
	@TERM_PREFERRED_TYPE_ID = 200,
	@CONTRACT_REVIEWER_TYPE_ID =203,
	@DECISION_MAKER_TYPE_ID =204,
	@SIGNATORY_TYPE_ID =206,
	@DIVISION_NAME = 'Test Data',  		-- 'Firestone Fibers & Textiles Company, LLC'
	@IS_INTEREST_MINORITY_SUPPLIERS =1,
	@IS_CORPORATE_HEDGE =0,
	@NAICS_CODE ='' ,
	@TAX_NUMBER = '62-1867018',
	@DUNS_NUMBER = '11-266-2812',
	@Not_Managed =0,
	@MISC_COMMENTS ='',
	@TRIGGER_RIGHTS =1,
	@CONTRACTING_ENTITY ='Firestone Fibers & Textiles Company, LLC',
	@CLIENT_LEGAL_STRUCTURE = 'Headquartered in Kings Mountain, N.C., FSFT has a total of three locations in the U.S. and Canada. FSFT?s 560 teammates manufacture nylon, polyester, rayon and fiberglass tire cord for Bridgestone Americas tire plants and other non-tire product manufacturers, along with other industrial fabrics, nylon 6 yarns and resins. A number of these products are for military applications and include such unique products as aircraft fuel cells, large capacity pillow tanks and the heavy industrial fabric used to reinforce the shielding skirts on the Landing Craft Air Cushion (LCAC). Other non-military products include reinforcement fabrics for conveyor belts, pump and air brake diaphragms, air suspension systems, drive belts, gaskets and various coated fabrics. FSFT also produces all the weft insertion reinforcement materials used in Firestone EPDM and TPO roofing systems.',
	@division_id =1,
	@Not_Managed_By_Id =65,
	@Not_Managed_Date = '2009-03-13 17:34:44.860'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
DR       Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.UPDATE_DIVISION_DETAILS_SQL_P
(
	@SBA_TYPE_ID integer ,
	@PRICE_INDEX_ID integer ,
	@CLIENT_ID integer ,
	@TERM_PREFERRED_TYPE_ID integer ,
	@CONTRACT_REVIEWER_TYPE_ID integer,
	@DECISION_MAKER_TYPE_ID integer,
	@SIGNATORY_TYPE_ID integer,
	@DIVISION_NAME varchar(200),
	@IS_INTEREST_MINORITY_SUPPLIERS BIT,
	@IS_CORPORATE_HEDGE BIT,
	@NAICS_CODE varchar(30) ,
	@TAX_NUMBER varchar(30),
	@DUNS_NUMBER varchar(30),
	@Not_Managed BIT,
	@MISC_COMMENTS varchar(4000),
	@TRIGGER_RIGHTS BIT,
	@CONTRACTING_ENTITY varchar(200),
	@CLIENT_LEGAL_STRUCTURE varchar(4000),
	@division_id integer,
	@Not_Managed_By_Id integer,
	@Not_Managed_Date datetime
)
AS

BEGIN 
	SET NOCOUNT ON

	exec cbmsDivision_UpdateParticipation
		  93
		, @division_id 
		, @not_managed 

	SET NOCOUNT OFF
	
	DECLARE @Err INT
	BEGIN TRANSACTION
	
		UPDATE dbo.SiteGroup
		SET Sitegroup_Name = @DIVISION_NAME
		WHERE Sitegroup_Id = @division_id
		
		SET @Err = @@Error
		if @Err != 0  GOTO Err_handler

		UPDATE dbo.division_dtl
		SET 
			SBA_TYPE_ID=@SBA_TYPE_ID
			,PRICE_INDEX_ID=@PRICE_INDEX_ID
			,CLIENT_ID=@CLIENT_ID
			,TERM_PREFERRED_TYPE_ID=@TERM_PREFERRED_TYPE_ID
			,CONTRACT_REVIEWER_TYPE_ID=@CONTRACT_REVIEWER_TYPE_ID
			,DECISION_MAKER_TYPE_ID=@DECISION_MAKER_TYPE_ID
			,SIGNATORY_TYPE_ID=@SIGNATORY_TYPE_ID
			,IS_INTEREST_MINORITY_SUPPLIERS=@IS_INTEREST_MINORITY_SUPPLIERS
			,IS_CORPORATE_HEDGE=@IS_CORPORATE_HEDGE
			,NAICS_CODE=@NAICS_CODE
			,TAX_NUMBER=@TAX_NUMBER
			,DUNS_NUMBER=@DUNS_NUMBER
			,Not_Managed=@Not_Managed
			,MISC_COMMENTS=@MISC_COMMENTS
			,TRIGGER_RIGHTS=@TRIGGER_RIGHTS
			,CONTRACTING_ENTITY=@CONTRACTING_ENTITY
			,CLIENT_LEGAL_STRUCTURE=@CLIENT_LEGAL_STRUCTURE
			,NOT_MANAGED_BY_ID = @Not_Managed_By_Id
			,NOT_MANAGED_DATE = getdate() 
		WHERE
			Sitegroup_id = @division_id

		SET @Err = @@Error
		if @Err != 0  GOTO Err_handler

Err_Handler:

	IF @@TRANCOUNT != 0 
		IF @Err != 0 
			ROLLBACK TRANSACTION
		ELSE 
			COMMIT TRANSACTION
END
GO
GRANT EXECUTE ON  [dbo].[UPDATE_DIVISION_DETAILS_SQL_P] TO [CBMSApplication]
GO
