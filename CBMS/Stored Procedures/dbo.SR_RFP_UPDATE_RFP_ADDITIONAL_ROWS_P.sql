SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_UPDATE_RFP_ADDITIONAL_ROWS_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
	@user_id       varchar(10),
	@session_id    varchar(20),
	@row_id        int,
	@row_name      varchar(100),
	@row_value     varchar(200),
	@vendor_id     int,
	@rfp_id        int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
-- select * from sr_rfp_load_profile_determinant(nolock) where sr_rfp_load_profile_determinant_id = 59
-- update sr_rfp_load_profile_determinant set UPDATED_DETERMINANT_UNIT_TYPE_ID = DETERMINANT_UNIT_TYPE_ID, UPDATED_IS_CHECKED = IS_CHECKED

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_UPDATE_RFP_ADDITIONAL_ROWS_P
	@user_id varchar(10),
	@session_id varchar(20),
	@row_id int,
	@row_name varchar(100),
	@row_value varchar(200),
	@vendor_id int,
	@rfp_id int
	AS
	
set nocount on
	
	Declare @account_determinant_type_id int
	
	select @account_determinant_type_id = entity_id from entity(nolock) where entity_type = 1030 and entity_name = 'Account Level'

	if(@row_id > 0)
	begin
		declare @old_row_name varchar(100), @old_row_value varchar(200) 

		select  @old_row_name = updated_row_name,
			@old_row_value = updated_row_value
		from sr_rfp_lp_account_additional_row(nolock) 
		where sr_rfp_lp_account_additional_row_id = @row_id
		
		if(@row_name != @old_row_name) OR (@row_value != @old_row_value) 
		begin
			update	sr_rfp_lp_account_additional_row 
			set 	updated_row_name = @row_name,
				updated_row_value = @row_value
			
			from	
				sr_rfp_lp_account_additional_row additional_row(nolock),
				sr_rfp_account rfp_acc(nolock),
				account acc(nolock),
				vendor ven(nolock)
			
			where 	rfp_acc.sr_rfp_id = @rfp_id
				and additional_row.sr_rfp_account_id = rfp_acc.sr_rfp_account_id
				and additional_row.row_type_id != @account_determinant_type_id
				and additional_row.updated_row_name = @old_row_name
				and acc.account_id = rfp_acc.account_id
				and ven.vendor_id = acc.vendor_id
				and acc.vendor_id = @vendor_id

		end

	end
	else --//insert new determinants  for all accounts belong to the utility for the RFP
	begin
		declare @determinant_type_id int
		select @determinant_type_id = entity_id from entity(nolock) where entity_type = 1030 and entity_name = 'New'
		DECLARE C_ADD_ROWS CURSOR FAST_FORWARD
		FOR
	
		select 	rfp_acc.sr_rfp_account_id
		from	sr_rfp_account rfp_acc(nolock),
			account acc(nolock),
			vendor ven(nolock)
		
		where 	rfp_acc.sr_rfp_id = @rfp_id
			and acc.account_id = rfp_acc.account_id
			and rfp_acc.is_deleted = 0
			and ven.vendor_id = acc.vendor_id
			and acc.vendor_id = @vendor_id

		declare @sr_rfp_account_id int, @sr_rfp_lp_account_additional_row_id int
		OPEN C_ADD_ROWS
		
		FETCH NEXT FROM C_ADD_ROWS INTO @sr_rfp_account_id
		WHILE (@@fetch_status <> -1)
		BEGIN
			IF (@@fetch_status <> -2)
			BEGIN
				insert into sr_rfp_lp_account_additional_row
				(sr_rfp_account_id, row_name, row_value, updated_row_name, updated_row_value, row_type_id) 
			 	values(@sr_rfp_account_id, @row_name,@row_value,@row_name,@row_value, @determinant_type_id)
			END
			FETCH NEXT FROM C_ADD_ROWS INTO @sr_rfp_account_id
		END
		CLOSE C_ADD_ROWS
		DEALLOCATE C_ADD_ROWS

	end
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_UPDATE_RFP_ADDITIONAL_ROWS_P] TO [CBMSApplication]
GO
