SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********  
NAME: dbo.Convert_Calc_Values

DESCRIPTION:     

	To convert the calc values received from Interval data ( PAM ) to the CBMS calc value.

INPUT PARAMETERS:
Name						DataType					Default			Description
------------------------------------------------------------------------------------------
@tvp_Calc_Val_Conversion	@tvp_Calc_Val_Conversion
@Commodity_Id				INT

OUTPUT PARAMETERS:
Name						DataType					Default			Description
------------------------------------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------------------------------------    
DECLARE @t tvp_Calc_Val_Conversion

INSERT INTO @t VALUES ( 1, 500, 'Mwh','Kwh'), ( 2, 1000, 'kw','kw')

EXEC Convert_Calc_Values @t, 290

AUTHOR INITIALS:    
Initials	Name    
------------------------------------------------------------------------------------------    
HG			Hariharasuthan Ganesan
 
MODIFICATIONS
Initials	Date		Modification
------------------------------------------------------------------------------------------
HG			2018-03-28	Created for IDR changes
******/
CREATE PROCEDURE [dbo].[Convert_Calc_Values]
      (
       @tvp_Calc_Val_Conversion AS tvp_Calc_Val_Conversion READONLY
      ,@Commodity_Id INT )
AS
BEGIN

      SET NOCOUNT ON;
      DECLARE @Uom_Entity_Type INT
	
      SELECT
            @Uom_Entity_Type = UOM_Entity_Type
      FROM
            dbo.Commodity
      WHERE
            Commodity_Id = @Commodity_Id

      SELECT
            cv.EC_Calc_Val_Id
           ,cv.Calc_Value * cuc.CONVERSION_FACTOR AS Calc_Value
           ,cv.Converted_Uom
           ,conv_uom.ENTITY_ID AS Converted_Uom_Id
      FROM
            @tvp_Calc_Val_Conversion cv
            LEFT OUTER JOIN dbo.ENTITY base_uom
                  ON base_uom.ENTITY_NAME = cv.Base_Uom
                     AND base_uom.ENTITY_TYPE = @Uom_Entity_Type
            LEFT OUTER JOIN dbo.ENTITY conv_uom
                  ON conv_Uom.Entity_Name = cv.Converted_Uom
                     AND conv_uom.ENTITY_TYPE = @Uom_Entity_Type
            LEFT OUTER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                  ON cuc.BASE_UNIT_ID = base_uom.ENTITY_ID
                     AND cuc.CONVERTED_UNIT_ID = conv_uom.ENTITY_ID
END
GO
GRANT EXECUTE ON  [dbo].[Convert_Calc_Values] TO [CBMSApplication]
GO
