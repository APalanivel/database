SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE   PROCEDURE [dbo].[cbmsInvSourcedImageBatchTargetLog_Save]
	( @MyAccountId int
	, @inv_sourced_image_batch_id int
	, @target_folder_path varchar(200)
	, @file_count int = 0
	)
AS
BEGIN

	declare @this_id int
	
	set nocount on

	insert into inv_sourced_image_batch_target_log
		( inv_sourced_image_batch_id 
		, target_folder_path
		, file_count
		)
	values
		( @inv_sourced_image_batch_id 
		, @target_folder_path
		, @file_count
		)

	set @this_id = @@IDENTITY

--	set nocount off

	exec cbmsInvSourcedImageBatchTargetLog_Get @MyAccountId, @this_id


END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvSourcedImageBatchTargetLog_Save] TO [CBMSApplication]
GO
