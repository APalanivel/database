SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.INSERT_LPS_P
	@priceIndexID INT,
	@lpsTypeID INT,
	@monthIdentifier DATETIME,
	@lpsFixedPrice decimal(32,16),
	@lpsUnitTypeID INT,
	@lpsFrequencyTypeID INT,
	@lpsExpression VARCHAR(1000),
	@contractID INT
AS
BEGIN

	SET NOCOUNT ON

	INSERT INTO dbo.LOAD_PROFILE_SPECIFICATION 
		(PRICE_INDEX_ID, 
		LPS_TYPE_ID, 
		MONTH_IDENTIFIER, 
		LPS_FIXED_PRICE, 
		LPS_UNIT_TYPE_ID, 
		LPS_FREQUENCY_TYPE_ID, 
		LPS_EXPRESSION, 
		CONTRACT_ID) 
	VALUES(@priceIndexID,
		@lpsTypeID,
		@monthIdentifier,
		@lpsFixedPrice,
		@lpsUnitTypeID,
		@lpsFrequencyTypeID,
		@lpsExpression,
		@contractID)

END
GO
GRANT EXECUTE ON  [dbo].[INSERT_LPS_P] TO [CBMSApplication]
GO
