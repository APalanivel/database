
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 dbo.SR_RFP_GET_ACCOUNT_METER_DETAILS_P    
  
DESCRIPTION:

	Used to get meters for initiating rfps

INPUT PARAMETERS:

Name			DataType	Default		Description
------------------------------------------------------------ 
@userId			VARCHAR
@sessionId		VARCHAR
@accountid		INT
@bidGroupId		INT
@SrrfpID		INT

OUTPUT PARAMETERS:
 Name   DataType  Default Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	exec dbo.SR_RFP_GET_ACCOUNT_METER_DETAILS_P 0,0,33062,0,997
	exec dbo.SR_RFP_GET_ACCOUNT_METER_DETAILS_P 0,0,438940,0,4115
	exec dbo.SR_RFP_GET_ACCOUNT_METER_DETAILS_P 0,0,439164,0,4162
	exec dbo.SR_RFP_GET_ACCOUNT_METER_DETAILS_P 0,0,0,10002220,4261

	

AUTHOR INITIALS:
Initials		Name
------------------------------------------------------------
RR				Raghu Reddy

MODIFICATIONS
Initials	Date		Modification
------------------------------------------------------------
RR			11/17/2011	MAINT-864 When initiating RFP's, specifically on accounts that have both an EP and NG meters on the same account, showing 
						both commodity the meters, but it should display only the commodity meters for which the rfp is initiating. Added @SrrfpID
						which is commodity specific as it is not possible to filter when @accountid is passed 
						(Only one parameter @accountid or @bidGroupId will have value, other one is zero as system defined)
						
						All the tables are replaced with client hier tables and the bid status filter is removed as anyway it is collecting data 
						for all stages of bid status(bid_status_type_id) with union. Except for bid status closed rfp accounts collecting from base 
						tables and for closed accounts collectong from archive tables(populated with site,account,meter related data once the rfp 
						accounts bid status is closed).
						Note: The meter number is not updating in archive tables, have different meter numbers for same meter ids.
						Removed the unused variable @bid_status_type_id
RR			2015-10-20	Global Sourcing - Phase2 - Modified to return TAX_EXEMPT_STATUS 

******/
CREATE PROCEDURE [dbo].[SR_RFP_GET_ACCOUNT_METER_DETAILS_P]
      @userId VARCHAR
     ,@sessionId VARCHAR
     ,@accountid INT
     ,@bidGroupId INT
     ,@SrrfpID INT
AS 
SET nocount ON

SELECT
      rtrim(ch.city) + ', ' + ch.State_Name + ' (' + ch.site_name + ')' SITE_NAME
     ,cha.Account_Number
     ,cha.Meter_Number
     ,cha.Meter_Address_Line_1 + ' ' + cha.Meter_Address_Line_2 METER_ADDRESS
     ,tes.ENTITY_NAME AS TAX_EXEMPT_STATUS
     ,cha.Rate_Name
FROM
      dbo.SR_RFP_ACCOUNT rfp_acc
      INNER JOIN dbo.SR_RFP srfp
            ON rfp_acc.SR_RFP_ID = srfp.SR_RFP_ID
      INNER JOIN Core.Client_Hier_Account cha
            ON cha.Account_Id = rfp_acc.ACCOUNT_ID
               AND cha.Commodity_Id = srfp.COMMODITY_TYPE_ID
      INNER JOIN Core.Client_Hier ch
            ON ch.Client_Hier_Id = cha.Client_Hier_Id
      INNER JOIN dbo.METER mt
            -- TAX_EXEMPT_STATUS column not present in Core.Client_Hier_Account
            ON cha.Meter_Id = mt.METER_ID
      LEFT JOIN dbo.ENTITY tes
            ON mt.TAX_EXEMPTION_ID = tes.ENTITY_ID
WHERE
      -- Application make sure to pass the value for @Accountid or @Bidgroupid to the SP
      ( ( @accountid > 0
          AND cha.Account_Id = @accountid )
        OR ( @bidGroupId > 0
             AND rfp_acc.SR_RFP_BID_GROUP_ID = @bidGroupId ) )
      AND rfp_acc.is_deleted = 0
      AND rfp_acc.SR_RFP_ID = @SrrfpID



GO


GRANT EXECUTE ON  [dbo].[SR_RFP_GET_ACCOUNT_METER_DETAILS_P] TO [CBMSApplication]
GO
