SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: DBO.Rm_Deal_Ticket_Option_details_Del  

DESCRIPTION: It Deletes Rm Deal Ticket Option details for Selected Rm_Deal_Ticket_Option_Details_Id.
      
INPUT PARAMETERS:          
	NAME								DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Rm_Deal_Ticket_Option_Details_Id	INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  	BEGIN TRAN
		
		EXEC dbo.rm_deal_ticket_OPTION_details_del  106
		
	ROLLBACK TRAN
	
	SELECT
		Rm_Deal_Ticket_Option_Details_Id
	FROM
		dbo.rm_deal_ticket_OPTION_details rmdl
		JOIN rm_deal_ticket_details rdtd
			 ON rmdl.Rm_Deal_Ticket_Details_Id = rdtd.Rm_Deal_Ticket_Details_Id
	WHERE
		rdtd.rm_deal_ticket_id = 106703


AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			31-AUG-10	CREATED

*/

CREATE PROCEDURE dbo.Rm_Deal_Ticket_Option_details_Del
    (
      @Rm_Deal_Ticket_Option_Details_Id		INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.Rm_Deal_Ticket_Option_details
	WHERE
		Rm_Deal_Ticket_Option_Details_Id = @Rm_Deal_Ticket_Option_Details_Id
		
END
GO
GRANT EXECUTE ON  [dbo].[Rm_Deal_Ticket_Option_details_Del] TO [CBMSApplication]
GO
