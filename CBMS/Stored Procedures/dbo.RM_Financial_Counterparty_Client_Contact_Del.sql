SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/**

NAME: dbo.RM_Financial_Counterparty_Client_Contact_Del

    
DESCRIPTION:    

      To check duplicate client contact


INPUT PARAMETERS:    
     NAME					DATATYPE		DEFAULT           DESCRIPTION    
------------------------------------------------------------------------    
	@Client_Id				INT		

                    

OUTPUT PARAMETERS:              
      NAME              DATATYPE    DEFAULT           DESCRIPTION       
-------------------------------------------------------------------------              


USAGE EXAMPLES:              
-------------------------------------------------------------------------              
	SELECT TOP 5 * FROM dbo.Client_Contact_Map a INNER JOIN dbo.Contact_Info b ON a.Contact_Info_Id = b.Contact_Info_Id
			WHERE a.CLIENT_ID  = 10003 ORDER BY b.Last_Change_Ts DESC
	BEGIN TRANSACTION
		SELECT * FROM dbo.Client_Contact_Map where  Client_Id=10003 and Contact_Info_Id = 13200 
		SELECT * FROM  dbo.Contact_Info WHERE Contact_Info_Id = 13200
		EXEC dbo.RM_Financial_Counterparty_Client_Contact_Del @Client_Id=10003,@Contact_Info_Id = 13200
		SELECT * FROM dbo.Client_Contact_Map where  Client_Id=10003 and Contact_Info_Id = 13200 
		SELECT * FROM  dbo.Contact_Info WHERE Contact_Info_Id = 13200 
	ROLLBACK TRANSACTION

	 

AUTHOR INITIALS:              
	INITIALS	NAME              
------------------------------------------------------------              
    RR			Raghu Reddy


MODIFICATIONS:    
	INITIALS	DATE		MODIFICATION              
------------------------------------------------------------              
	RR 			31-07-2018	Created - Global Risk Management

**/ 
CREATE PROCEDURE [dbo].[RM_Financial_Counterparty_Client_Contact_Del]
      ( 
       @Client_Id INT
      ,@Counterparty_Id INT
      ,@Contact_Info_Id INT )
AS 
BEGIN
	
      SET NOCOUNT ON;
      
      DELETE
            ccm
      FROM
            Trade.Financial_Counterparty_Client_Contact_Map ccm
            INNER JOIN dbo.Contact_Info ci
                  ON ccm.Contact_Info_Id = ci.Contact_Info_Id
      WHERE
            ccm.CLIENT_ID = @Client_Id
            AND ccm.RM_COUNTERPARTY_ID = @Counterparty_Id
            AND ccm.Contact_Info_Id = @Contact_Info_Id
            
      DELETE FROM
            dbo.Contact_Info
      WHERE
            Contact_Info_Id = @Contact_Info_Id
     
				

END;
GO
GRANT EXECUTE ON  [dbo].[RM_Financial_Counterparty_Client_Contact_Del] TO [CBMSApplication]
GO
