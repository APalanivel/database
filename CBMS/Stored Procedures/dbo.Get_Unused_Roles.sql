SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/******************************************************************************************************    
NAME :	dbo.Get_Unused_Roles
   
DESCRIPTION: 
This Stored Procedure is used get all security roles which are not assigned to any users.
   
 INPUT PARAMETERS:    
 Name             DataType               Default        Description    
--------------------------------------------------------------------      
   
 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
-------------------------------------------------------------------- 
Client_Id			INT
Security_Role_Id	INT
Role_Name			VARCHAR
   
  USAGE EXAMPLES:    
--------------------------------------------------------------------    
   
   EXEC dbo.Get_Unused_Roles
  
AUTHOR INITIALS:    
 Initials	Name    
-------------------------------------------------------------------    
 KVK		K Vinay Kumar
 HG			Harihara Suthan G

 MODIFICATIONS     
 Initials	Date		Modification    
--------------------------------------------------------------------    
 HG			4/1/2011	Script modified to remove Role_Name column from Security_Role table as it is not used by application anywhere.
*********/

CREATE PROCEDURE [dbo].[Get_Unused_Roles]
AS 
BEGIN

      SET NOCOUNT ON ;
	
	
        SELECT
              Client_Id
             ,Security_Role_Id
        FROM
              dbo.SECURITY_ROLE sr
        WHERE
              IS_CORPORATE = 0
              AND NOT EXISTS ( SELECT
                                      1
                                FROM
                                      USER_SECURITY_ROLE usr
                                WHERE
									usr.Security_Role_Id = sr.Security_Role_Id
                              )

END
GO
GRANT EXECUTE ON  [dbo].[Get_Unused_Roles] TO [CBMSApplication]
GO
