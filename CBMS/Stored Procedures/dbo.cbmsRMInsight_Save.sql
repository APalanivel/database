SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










CREATE         PROCEDURE [dbo].[cbmsRMInsight_Save]
	( @MyAccountId int
	, @RMInsightId int
	, @author_id int
	, @insight_date datetime
	, @insight_title varchar(200)
	, @insight_text text
	)
AS
BEGIN 

set nocount on

declare @this_id int
set @this_id = @RMInsightId

if @this_id is null
begin

INSERT INTO RM_RISK_INSIGHT
	( 
	author_id
	, insight_date
	, insight_title
	, insight_text
	)
VALUES
( @author_id 
, @insight_date 
, @insight_title 
, @insight_text 
)

set @this_id = scope_identity()

end
else
begin

UPDATE rm_risk_insight

set author_id = @author_id
, insight_date = @insight_date
, insight_title = @insight_title 
, insight_text = @insight_text
where rm_risk_insight_id = @this_id

end

exec cbmsRMInsight_Get @MyAccountId, @this_id

END









GO
GRANT EXECUTE ON  [dbo].[cbmsRMInsight_Save] TO [CBMSApplication]
GO
