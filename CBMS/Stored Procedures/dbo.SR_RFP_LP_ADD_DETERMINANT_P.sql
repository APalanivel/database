SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_LP_ADD_DETERMINANT_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
	@user_id varchar(10),
	@session_id varchar(20),
	@rfp_account_id int,
	@determinant_name varchar(50),
	@unit varchar(10),
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    
	@determinant_id int OUTPUT


USAGE EXAMPLES:
------------------------------------------------------------
-- TEST 2
---- Test a new entry
--EXEC dbo.SR_RFP_LP_ADD_DETERMINANT_P
--	@user_id = 1,
--	@session_id = -1,
--	@rfp_account_id = 414 ,
--	@determinant_name = 'Test Data',
--	@unit = 'Days',
--	@determinant_id = NULL
---- check if the new entry exist at SV
--SELECT * FROM SR_RFP_LOAD_PROFILE_DETERMINANT WHERE SR_RFP_LOAD_PROFILE_DETERMINANT_ID = (SELECT MAX(SR_RFP_LOAD_PROFILE_DETERMINANT_ID) FROM SR_RFP_LOAD_PROFILE_DETERMINANT) AND DETERMINANT_NAME = 'Test Data'

----Delete the entry
--DELETE FROM SR_RFP_LOAD_PROFILE_DETERMINANT WHERE SR_RFP_LOAD_PROFILE_DETERMINANT_ID = (SELECT MAX(SR_RFP_LOAD_PROFILE_DETERMINANT_ID) FROM SR_RFP_LOAD_PROFILE_DETERMINANT) AND DETERMINANT_NAME = 'Test Data'


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_LP_ADD_DETERMINANT_P
	@user_id varchar(10),
	@session_id varchar(20),
	@rfp_account_id int,
	@determinant_name varchar(50),
	@unit varchar(10),
	@determinant_id int OUTPUT
	AS
	
SET NOCOUNT ON

	
	declare @unit_type_id int, @commodity varchar(100)

	select 	@commodity = entity_name 
	from 	sr_rfp(nolock),
	     	entity(nolock)
	where 	sr_rfp_id = (select sr_rfp_id from sr_rfp_account(nolock) where sr_rfp_account_id = @rfp_account_id)
		and entity_id = commodity_type_id 
	
	
	if(@commodity = 'Natural Gas')
	begin
		select @unit_type_id = entity_id from entity(nolock) where entity_type = 102 and entity_name = @unit
	end
	else
	begin
		select @unit_type_id = entity_id from entity(nolock) where entity_type = 101 and entity_name = @unit
	end

	insert into sr_rfp_load_profile_determinant
	(sr_rfp_load_profile_setup_id, determinant_name, determinant_unit_type_id, is_checked, determinant_type_id)
	select 
	(select sr_rfp_load_profile_setup_id from sr_rfp_load_profile_setup(nolock) where sr_rfp_account_id = @rfp_account_id),
	@determinant_name,
	@unit_type_id,
	1,
	1157--(select entity_id from entity(nolock) where entity_type = 1030 and entity_name = 'Account Level')

	
	select @determinant_id = SCOPE_IDENTITY()

	RETURN
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_LP_ADD_DETERMINANT_P] TO [CBMSApplication]
GO
