SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

 
/******        
NAME:    [Workflow].[Get_Modules_List_Permissions_Base]    
DESCRIPTION: STORED PROCEDURE IS CREATED TO PULL THE MODULES BASED ON THE USER PERMISSION    
------------------------------------------------------------       
 INPUT PARAMETERS:        
 Name   DataType  Default Description        
 @userID INT  -- Based on the UserID or QueueID data will be return  
------------------------------------------------------------        
 OUTPUT PARAMETERS:        
 Name   DataType  Default Description        
------------------------------------------------------------        
 USAGE EXAMPLES:    
 EXEC [Workflow].[Get_Modules_List_Permissions_Base] @userID=49 , @Workflow_Queue_Category_name='Invoice'  
 EXEC [Workflow].[Get_Modules_List_Permissions_Base] @userID=49 , @Workflow_Queue_Category_name='Budget'  
------------------------------------------------------------        
AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
AKP   ARUNKUMAR PALANIVEL   Summit Energy     
TRK   RAMAKRISHNA THUMMALA  Summit Energy    
 MODIFICATIONS         
 Initials Date   Modification        
------------------------------------------------------------        
 AKP    SEP 3,2019 Created    
 TRK	NOV 11,2019 Modify the C.Code_Value to  WQ.Workflow_Queue_Name 
******/  
CREATE PROCEDURE [Workflow].[Get_Modules_List_Permissions_Base]  
      -- Add the parameters for the stored procedure here       
      @userID                       INT  
    , @Workflow_Queue_Category_name VARCHAR(50)  
AS  
      BEGIN -- SET NOCOUNT ON added to prevent extra result sets from       
            -- interfering with SELECT statements.       
            DECLARE  
                  @Proc_name     VARCHAR(100) = 'Get_Modules_List_Permissions_Base'  
                , @Input_Params  VARCHAR(1000)  
                , @Error_Line    INT  
                , @Error_Message VARCHAR(3000);  
  
            SELECT  
                  @Input_Params = cast(@userID AS VARCHAR);  
            SET NOCOUNT ON;  
  
            DECLARE @ISUSERSAVEDFILTERACTIVE BIT;  
              
  
            -- INSERT STATEMENTS FOR PROCEDURE HERE       
  
  
            BEGIN TRY  
  
                  IF @userID IS NULL  
                        RAISERROR('User ID is Null.'  -- Message text.        
                                , 16                  -- Severity.        
                                , 1                   -- State.        
                        );  
  
                        ;WITH LEFT_NAVIGATION_PERMISSIONS  
                        AS ( SELECT DISTINCT  
                                    WQ.Workflow_Queue_Id AS ID  
                                  , WQ.Workflow_Queue_Name  
                             FROM   GROUP_INFO GI  
                                    JOIN  
                                    USER_INFO_GROUP_INFO_MAP UIG  
                                          ON GI.GROUP_INFO_ID = UIG.GROUP_INFO_ID  
                                    JOIN  
                                    USER_INFO U  
                                          ON U.USER_INFO_ID = UIG.USER_INFO_ID  
                                    JOIN  
                                    GROUP_INFO_PERMISSION_INFO_MAP GIPM  
                                          ON GIPM.GROUP_INFO_ID = GI.GROUP_INFO_ID  
                                    JOIN  
                                    PERMISSION_INFO P WITH ( NOLOCK )  
                                          ON P.PERMISSION_INFO_ID = GIPM.PERMISSION_INFO_ID  
                                    JOIN  
                                    Workflow.Workflow_Queue WQ  
                                          ON WQ.Permission_Info_Id = P.PERMISSION_INFO_ID  
                                    JOIN  
                                    Code C  
                                          ON WQ.Workflow_Queue_Category_Cd = C.Code_Id  
                             WHERE  C.Code_Value = @Workflow_Queue_Category_name  
                                    AND   U.USER_INFO_ID = @userID  
                                    AND   WQ.Is_Active = 1 )  
                  SELECT  
                              *  
FROM        LEFT_NAVIGATION_PERMISSIONS  
                  ORDER BY    1;  
  
  
            END TRY  
            BEGIN CATCH  
				
  
                  -- Entry made to the logging SP to capture the errors.      
                  SELECT  
                        @Error_Line = error_line()  
                      , @Error_Message = error_message();  
  
                  INSERT INTO StoredProc_Error_Log (  
                                                         StoredProc_Name  
                                                       , Error_Line  
                                                       , Error_message  
                                                       , Input_Params  
                                                   )  
                  VALUES  
                       ( @Proc_name, @Error_Line, @Error_Message, @Input_Params );  
            END CATCH;  
      END;      
GO
GRANT EXECUTE ON  [Workflow].[Get_Modules_List_Permissions_Base] TO [CBMSApplication]
GO
