SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.Account_DMO_Config_Sel_By_Account_Commodity
           
DESCRIPTION:             
			To select DMO configurations
			
INPUT PARAMETERS:            
	Name				DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Account_Id			INT
	@Commodity_Id		INT    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
            
	EXEC dbo.Account_DMO_Config_Sel_By_Account_Commodity 22452,290
	
	EXEC dbo.Account_DMO_Config_Sel_By_Account_Commodity 22452,291
		
	EXEC dbo.Account_DMO_Config_Sel_By_Account_Commodity 48519,290
		
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	NR			Narayana Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	NR			2017-01-20	Contract placeholder 
******/

CREATE PROCEDURE [dbo].[Account_DMO_Config_Sel_By_Account_Commodity]
      ( 
       @Account_Id INT
      ,@Commodity_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Tbl_Config AS TABLE
            ( 
             Client_Hier_DMO_Config_Id INT
            ,Client_Hier_Id INT
            ,Hier_level_Cd INT
            ,Account_Id INT
            ,Account_DMO_Config_Id INT
            ,Code_Dsc VARCHAR(255)
            ,Commodity_Id INT
            ,Commodity_Name VARCHAR(50)
            ,DMO_Start_Dt VARCHAR(10)
            ,DMO_End_Dt VARCHAR(10)
            ,Updated_User VARCHAR(100)
            ,Last_Change_Ts DATETIME )
            
            
      INSERT      INTO @Tbl_Config
                  ( 
                   Client_Hier_DMO_Config_Id
                  ,Client_Hier_Id
                  ,Hier_level_Cd
                  ,Account_Id
                  ,Account_DMO_Config_Id
                  ,Code_Dsc
                  ,Commodity_Id
                  ,Commodity_Name
                  ,DMO_Start_Dt
                  ,DMO_End_Dt
                  ,Updated_User
                  ,Last_Change_Ts )
                  EXEC dbo.Account_DMO_Config_Sel 
                        @Account_Id = @Account_Id
            
      SELECT
            Client_Hier_DMO_Config_Id
           ,Client_Hier_Id
           ,Hier_level_Cd
           ,Account_Id
           ,Account_DMO_Config_Id
           ,Code_Dsc
           ,Commodity_Id
           ,Commodity_Name
           ,DMO_Start_Dt
           ,DMO_End_Dt
           ,Updated_User
           ,Last_Change_Ts
      FROM
            @Tbl_Config tc
      WHERE
            tc.Commodity_Id = @Commodity_Id
     
            
      
      
END

;
GO
GRANT EXECUTE ON  [dbo].[Account_DMO_Config_Sel_By_Account_Commodity] TO [CBMSApplication]
GO
