
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
                  
NAME: [dbo].[User_Info_Group_Info_Map_Merge]
                    
DESCRIPTION:                    
			 To update the User Info Group info Map table.                    
                    
INPUT PARAMETERS:    
                   
Name                              DataType            Default        Description    
---------------------------------------------------------------------------------------------------------------  
@User_Info_Id                     INT          
@Group_Info_Id					VARCHAR(MAX)   
                    
OUTPUT PARAMETERS:      
                      
Name                              DataType            Default        Description    
---------------------------------------------------------------------------------------------------------------  
                    
USAGE EXAMPLES:                        
---------------------------------------------------------------------------------------------------------------                          
		BEGIN TRAN      
		    
		SELECT      
			  *
		FROM      
			  dbo.User_Info_Group_Info_Map UIG 
		WHERE      
			  UIG.User_Info_Id = 39151      
		            
		EXEC dbo.User_Info_Group_Info_Map_Merge 
			  @User_Info_Id = 39151
			 ,@Group_Info_Id = '209,210,211,212,213,214,218,219,220,221,222,223,224,225,226,227,228,232,233,235,260,263,296,307,308,319,323,338,339,340,350,354,356,358' 
		                    
		SELECT      
			  *
		FROM      
			  dbo.User_Info_Group_Info_Map UIG 
		WHERE      
			  UIG.User_Info_Id = 39151          
		ROLLBACK TRAN   
                   
AUTHOR INITIALS:    
                  
Initials                Name    
---------------------------------------------------------------------------------------------------------------  
SP                      Sandeep Pigilam   
NR						Narayana Reddy   
                     
MODIFICATIONS:   
                    
Initials                Date            Modification  
---------------------------------------------------------------------------------------------------------------  
 SP                     2013-11-27      Created for RA Admin user management 
 NR						2016-12-21		MAINT-4785 - Added WITH RECOMPILE option.
                 
                   
******/         
                  
CREATE  PROCEDURE [dbo].[User_Info_Group_Info_Map_Merge]
      ( 
       @User_Info_Id INT
      ,@Group_Info_Id VARCHAR(MAX) = NULL
      ,@Assigned_User_Id INT )
      WITH RECOMPILE
AS 
BEGIN                  
      SET NOCOUNT ON;       
                  
      DECLARE @User_Info_Group_Info_Map_List TABLE
            ( 
             Group_Info_Id INT PRIMARY KEY CLUSTERED )                
                
                
      INSERT      INTO @User_Info_Group_Info_Map_List
                  ( 
                   Group_Info_Id )
                  SELECT
                        ufn.Segments
                  FROM
                        dbo.ufn_split(@Group_Info_Id, ',') ufn             

      BEGIN TRY                      
            BEGIN TRANSACTION   
            
            INSERT      INTO dbo.User_Info_Group_Info_Map
                        ( 
                         Group_Info_Id
                        ,User_Info_Id
                        ,Assigned_User_Id
                        ,Assigned_Ts )
                        SELECT
                              urr.Group_Info_Id
                             ,@User_Info_Id
                             ,@Assigned_User_Id
                             ,GETDATE()
                        FROM
                              @User_Info_Group_Info_Map_List urr
                        WHERE
                              NOT EXISTS ( SELECT
                                                1
                                           FROM
                                                dbo.User_Info_Group_Info_Map ugm
                                           WHERE
                                                ugm.Group_Info_Id = urr.Group_Info_Id
                                                AND ugm.User_Info_Id = @User_Info_Id )
               
            DELETE
                  ugm
            FROM
                  dbo.User_Info_Group_Info_Map ugm
            WHERE
                  ugm.User_Info_Id = @User_Info_Id
                  AND NOT EXISTS ( SELECT
                                    1
                                   FROM
                                    @User_Info_Group_Info_Map_List ugml
                                   WHERE
                                    ugml.Group_Info_Id = ugm.Group_Info_Id )
               
  
                                   
                               
            COMMIT TRANSACTION                               
                                 
                                 
      END TRY                  
      BEGIN CATCH                  
            IF @@TRANCOUNT > 0 
                  BEGIN  
                        ROLLBACK TRANSACTION  
                  END                 
            EXEC dbo.usp_RethrowError                  
      END CATCH                                
                         
END;

;
GO

GRANT EXECUTE ON  [dbo].[User_Info_Group_Info_Map_Merge] TO [CBMSApplication]
GO
