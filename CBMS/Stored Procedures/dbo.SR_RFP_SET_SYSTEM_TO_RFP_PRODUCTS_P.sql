
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	dbo.SR_RFP_SET_SYSTEM_TO_RFP_PRODUCTS_P

DESCRIPTION: 


INPUT PARAMETERS:    
    Name			DataType          Default     Description    
---------------------------------------------------------------------------------    
	@userId			varchar
	@sessionId		varchar
	@rfpId			int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DR			Deana Ritter
	RR			Raghu Reddy

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	DR			08/04/2009	Removed Linked Server Updates
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2015-11-09	Global Sourcing - Phase2 - Products made geography specific, so added logic to select only products matching 
							accounts country and along with existing commodity match filter
******/
CREATE PROCEDURE [dbo].[SR_RFP_SET_SYSTEM_TO_RFP_PRODUCTS_P]
      @userId VARCHAR
     ,@sessionId VARCHAR
     ,@rfpId INT
AS
BEGIN 
      SET NOCOUNT ON;

      DECLARE @commodityId INT;
     
      SELECT
            @commodityId = COMMODITY_TYPE_ID
      FROM
            dbo.SR_RFP
      WHERE
            SR_RFP_ID = @rfpId;
      
      WITH  Cte_Prdcts
              AS ( SELECT
                        @rfpId AS SR_RFP_ID
                       ,spp.PRODUCT_NAME
                       ,spp.PRODUCT_DESCRIPTION
                   FROM
                        dbo.SR_PRICING_PRODUCT spp
                        INNER JOIN dbo.SR_Pricing_Product_Country_Map cspp
                              ON spp.SR_PRICING_PRODUCT_ID = cspp.SR_PRICING_PRODUCT_ID
                   WHERE
                        spp.COMMODITY_TYPE_ID = @commodityId
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.SR_RFP_ACCOUNT rfpacc
                                          INNER JOIN Core.Client_Hier_Account cha
                                                ON rfpacc.ACCOUNT_ID = cha.Account_Id
                                     WHERE
                                          rfpacc.SR_RFP_ID = @rfpId
                                          AND cha.Commodity_Id = spp.COMMODITY_TYPE_ID
                                          AND cha.Meter_Country_Id = cspp.Country_ID )
                   GROUP BY
                        spp.PRODUCT_NAME
                       ,spp.PRODUCT_DESCRIPTION)
            INSERT      INTO dbo.SR_RFP_PRICING_PRODUCT
                        ( SR_RFP_ID
                        ,PRODUCT_NAME
                        ,PRODUCT_DESCRIPTION )
                        SELECT
                              spp.SR_RFP_ID
                             ,spp.PRODUCT_NAME
                             ,spp.PRODUCT_DESCRIPTION
                        FROM
                              Cte_Prdcts spp
                        WHERE
                              NOT EXISTS ( SELECT
                                                1
                                           FROM
                                                dbo.SR_RFP_PRICING_PRODUCT srpp
                                           WHERE
                                                srpp.SR_RFP_ID = spp.SR_RFP_ID
                                                AND srpp.PRODUCT_NAME = spp.PRODUCT_NAME );
				
END;


GO

GRANT EXECUTE ON  [dbo].[SR_RFP_SET_SYSTEM_TO_RFP_PRODUCTS_P] TO [CBMSApplication]
GO
