
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
    CBMS.dbo.Report_Regional_Manager_Budget_Queue_Dtl  
    
 DESCRIPTION:   
		To get the budget queue details for all the regional managers.
 
INPUT PARAMETERS:
Name			DataType		Default			Description 
------------------------------------------------------------

OUTPUT PARAMETERS:
Name			DataType		Default			Description
------------------------------------------------------------  
 USAGE EXAMPLES:  
 exec Report_Regional_Manager_Budget_Queue_Dtl  
------------------------------------------------------------  
         

AUTHOR INITIALS:  
Initials	Name  
-----------------------------------------------------------  
HG			Harihara Suthan Ganesan
LEC			Lynn

MODIFICATIONS
Initials	Date		Modification  
------------------------------------------------------------  
HG			2015-06-18	Created based on BUDGET_GET_RM_BUDGET_RESULTS_P
LEC			2016-03-09  Modified to include budget name and budget year
******/
CREATE PROCEDURE [dbo].[Report_Regional_Manager_Budget_Queue_Dtl]
AS
BEGIN            
      SET NOCOUNT ON;          
	
      CREATE TABLE #Budget_Queue_Results
            (
             budgetId INT
            ,budgetstartyear INT
            ,budgetname VARCHAR(200)
            ,clientName VARCHAR(200)
            ,SiteName VARCHAR(1000)
            ,accountId INT
            ,budgetAccountId INT
            ,accountNumber VARCHAR(1000)
            ,dueDate DATE
            ,utility VARCHAR(200)
            ,rates VARCHAR(MAX)
            ,commodity VARCHAR(200)
            ,service_level_char VARCHAR(200)
            ,tariff_transport VARCHAR(200)
            --,startYear INT
            ,startMonth INT
            ,isPosted BIT
            ,Record_Number INT
            ,Total_Rowcount INT
            ,User_Info_Id INT );

      CREATE NONCLUSTERED INDEX ix_#Budget_Queue_Results_User_Info_Id ON #Budget_Queue_Results( User_Info_Id);
	
      CREATE TABLE #Accounts
            (
             Account_Id INT
            ,Commodity_Id INT
            ,service_level_char VARCHAR(1)
            ,Account_Vendor_Id INT
            ,Account_Number VARCHAR(50)
            ,Account_Vendor_Name VARCHAR(200)
            ,Client_Hier_Id INT
            ,tariff_Transport VARCHAR(50)
            ,RA_RM TINYINT PRIMARY KEY ( Account_Id, Client_Hier_Id, Commodity_Id ) );

      DECLARE @Users TABLE
            (
             User_Info_Id INT
            ,Row_Num INT IDENTITY(1, 1) );

      DECLARE @Group_Legacy_Name TABLE
            (
             GROUP_INFO_ID INT PRIMARY KEY CLUSTERED
            ,GROUP_NAME VARCHAR(200)
            ,Legacy_Group_Name VARCHAR(200) );                   

      DECLARE
            @Current_Row_Num INT = 1
           ,@Total_Rows INT = 0
           ,@User_Info_id INT
           ,@userId INT
           ,@Is_Before_Due_Date BIT = 0
           ,@Is_History BIT = 0
           ,@SortColumn VARCHAR(500) = 'dueDate,clientName,utility'
           ,@SortIndex VARCHAR(10) = 'Asc'
           ,@Total_RowCount INT = 0
           ,@Start_Index INT = 1
           ,@End_Index INT = 2147483647
           ,@Rates_Queue_Cd INT
           ,@Both_Queue_Cd INT
           ,@SQLStatement NVARCHAR(MAX)
           ,@ParamList NVARCHAR(500)
           ,@Due_Date DATE;
         
      INSERT      INTO @Group_Legacy_Name
                  EXEC dbo.Group_Info_Sel_By_Group_Legacy
                        @Group_Legacy_Name_Cd_Value = 'Regulated_Markets_Budgets';  

      INSERT      INTO @Users
                  (User_Info_Id )
                  SELECT
                        userInfo.USER_INFO_ID
                  FROM
                        dbo.USER_INFO userInfo
                        INNER JOIN dbo.USER_INFO_GROUP_INFO_MAP map
                              ON map.USER_INFO_ID = userInfo.USER_INFO_ID
                        INNER JOIN dbo.GROUP_INFO groupInfo
                              ON map.GROUP_INFO_ID = groupInfo.GROUP_INFO_ID
                  WHERE
                        groupInfo.GROUP_NAME = 'operations'
                        AND userInfo.IS_HISTORY = 0;

      SET @Total_Rows = @@ROWCOUNT;

      SELECT
            @SortColumn = CASE @SortColumn
                            WHEN 'budgetId' THEN 'ba.budget_id'
                            WHEN 'budgetstartyear' THEN 'b.budget_start_year'
                            WHEN 'budgetname' THEN 'b.budget_name'
                            WHEN 'clientName' THEN 'ch.Client_Name'
                            WHEN 'SiteName' THEN 'RTRIM(ch.City) + '', '' + ch.State_Name + '' ('' + ch.Site_name + '')'''
                            WHEN 'accountId' THEN 'cha.account_id'
                            WHEN 'budgetAccountId' THEN 'ba.BUDGET_ACCOUNT_ID'
                            WHEN 'accountNumber' THEN 'cha.account_number'
                            WHEN 'dueDate' THEN 'b.DUE_DATE'
                            WHEN 'utility' THEN 'cha.Account_Vendor_Name'
                            WHEN 'rates' THEN 'LEFT(Rates.Rate_Names, LEN(Rates.Rate_Names) - 1)'
                            WHEN 'commodity' THEN 'c.Commodity_Name'
                            --WHEN 'startYear' THEN 'b.budget_start_year'
                            WHEN 'startMonth' THEN 'b.budget_start_month'
                            WHEN 'isPosted' THEN 'b.posted_by'
                            WHEN 'dueDate,clientName,utility' THEN 'b.DUE_DATE, ch.Client_Name,cha.Account_Vendor_Name'
                            ELSE @SortColumn
                          END;

      SELECT
            @Due_Date = DATEADD(dd, 30, GETDATE());

      SELECT
            @Rates_Queue_Cd = c.Code_Id
      FROM
            dbo.Code AS c
            JOIN dbo.Codeset AS cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'IncludeInAnalystQueue'
            AND c.Code_Value = 'Yes - Rates only';
  
      SELECT
            @Both_Queue_Cd = c.Code_Id
      FROM
            dbo.Code AS c
            JOIN dbo.Codeset AS cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'IncludeInAnalystQueue'
            AND c.Code_Value = 'Yes - Rates and Sourcing';

      WHILE @Total_Rows >= @Current_Row_Num
            BEGIN
                  TRUNCATE TABLE #Accounts;
      
                  SELECT
                        @User_Info_id = User_Info_Id
                  FROM
                        @Users
                  WHERE
                        Row_Num = @Current_Row_Num;

                  SET @userId = @User_Info_id;
   
                  INSERT      #Accounts
                              (Account_Id
                              ,Commodity_Id
                              ,service_level_char
                              ,Account_Vendor_Id
                              ,Account_Number
                              ,Account_Vendor_Name
                              ,Client_Hier_Id
                              ,tariff_Transport
                              ,RA_RM )
                              SELECT
                                    cha.Account_Id
                                   ,cha.Commodity_Id
                                   ,Service_Level.ENTITY_NAME AS service_level_char
                                   ,cha.Account_Vendor_Id
                                   ,cha.Account_Number
                                   ,cha.Account_Vendor_Name
                                   ,cha.Client_Hier_Id
                                   ,CASE WHEN COUNT(c.CONTRACT_ID) > 0 THEN 'Transport'
                                         ELSE 'Tariff'
                                    END AS tariff_Transport
                                   ,MAX(CASE WHEN udam.Analyst_ID = @userId THEN 1
                                             WHEN rmm.USER_INFO_ID = @userId THEN 2
                                        END) AS RA_RM
                              FROM
                                    Core.Client_Hier_Account AS cha
                                    INNER JOIN dbo.ENTITY AS Service_Level
                                          ON cha.Account_Service_level_Cd = Service_Level.ENTITY_ID
                                    INNER JOIN dbo.UTILITY_DETAIL AS ud
                                          ON ud.VENDOR_ID = cha.Account_Vendor_Id
                                    INNER JOIN dbo.Utility_Detail_Analyst_Map udam
                                          ON ud.UTILITY_DETAIL_ID = udam.Utility_Detail_ID
                                    INNER JOIN dbo.GROUP_INFO gi
                                          ON udam.Group_Info_ID = gi.GROUP_INFO_ID
                                    INNER JOIN @Group_Legacy_Name gil
                                          ON gi.GROUP_INFO_ID = gil.GROUP_INFO_ID
                                    LEFT JOIN ( Core.Client_Hier_Account AS cha1
                                                INNER JOIN dbo.CONTRACT AS c
                                                      ON cha1.Supplier_Contract_ID = c.CONTRACT_ID
                                                         AND c.CONTRACT_END_DATE >= GETDATE()
                                                         AND cha1.Account_Type = 'Supplier'
                                                INNER JOIN dbo.ENTITY AS e
                                                      ON c.CONTRACT_TYPE_ID = e.ENTITY_ID
                                                         AND e.ENTITY_NAME = 'Supplier' )
                                          ON cha.Meter_Id = cha1.Meter_Id
                                    LEFT JOIN ( dbo.REGION_MANAGER_MAP AS rmm
                                                INNER JOIN dbo.STATE AS s
                                                      ON rmm.REGION_ID = s.REGION_ID
                                                INNER JOIN dbo.VENDOR_STATE_MAP AS vsm
                                                      ON s.STATE_ID = vsm.STATE_ID )
                                          ON vsm.VENDOR_ID = cha.Account_Vendor_Id
                              WHERE
                                    ( udam.Analyst_ID = @userId
                                      OR rmm.USER_INFO_ID = @userId )
                                    AND cha.Account_Type = 'Utility'
                                    AND ( cha1.Account_Id IS NOT NULL		-- Transport
                                          OR Service_Level.ENTITY_NAME NOT IN ( 'C', 'D' ) )
                                    AND EXISTS ( SELECT
                                                      1
                                                 FROM
                                                      dbo.BUDGET_ACCOUNT AS ba
                                                 WHERE
                                                      ACCOUNT_ID = cha.Account_Id
                                                      AND ba.IS_DELETED = 0 )
                              GROUP BY
                                    cha.Account_Id
                                   ,cha.Commodity_Id
                                   ,Service_Level.ENTITY_NAME
                                   ,cha.Account_Vendor_Id
                                   ,cha.Account_Number
                                   ,cha.Account_Vendor_Name
                                   ,cha.Client_Hier_Id;
                       


                  SELECT
                        @SQLStatement = N'
      IF @Total_RowCount = 0 
            BEGIN

                  SELECT
                        @Total_RowCount = COUNT(1)
				  FROM
                        #Accounts cha
                        INNER JOIN Core.Client_Hier AS ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.BUDGET_ACCOUNT AS ba
                              ON cha.Account_Id = ba.ACCOUNT_ID
                        INNER JOIN dbo.BUDGET AS b
                              ON ba.BUDGET_ID = b.BUDGET_ID
                                 AND cha.Commodity_Id = b.COMMODITY_TYPE_ID
                         WHERE
            ba.IS_DELETED = 0' + CASE WHEN @Is_History = 0 THEN N'
                              AND b.POSTED_BY IS NULL
                              AND b.Analyst_Queue_Display_Cd IN ( @Both_Queue_Cd, @Rates_Queue_Cd )
                              AND ( ( cha.RA_RM = 1
                                      AND ba.RATES_COMPLETED_BY IS NULL )
                                    OR ( cha.RA_RM = 2
                                         AND ba.RATES_COMPLETED_BY IS NOT NULL
                                         AND ba.RATES_REVIEWED_BY IS NULL ) )' + CASE WHEN @Is_Before_Due_Date = 1 THEN N'
                               AND b.DUE_DATE <= @Due_Date'                           WHEN @Is_Before_Due_Date = 0 THEN N''
                                                                                 END
                                      WHEN @Is_History = 1 THEN ' 
                               AND ( ba.RATES_COMPLETED_BY IS NOT NULL
                                     AND ba.RATES_REVIEWED_BY IS NOT NULL
                                     OR ba.CANNOT_PERFORMED_BY IS NOT NULL )
'
                                 END + N'

            END
            ;
            WITH  CTE_ResultSet
                    AS ( SELECT TOP ( @End_Index )
                              ba.budget_id AS budgetId
                              ,b.budget_start_year as budgetstartyear
                              ,b.budget_name as budgetname
                             ,ch.Client_Name AS clientName
                             ,RTRIM(ch.City) + '', '' + ch.State_Name + '' ('' + ch.Site_name + '')'' SiteName
                             ,cha.account_id AS accountId
                             ,ba.BUDGET_ACCOUNT_ID AS budgetAccountId
                             ,cha.account_number AS accountNumber
                             ,b.DUE_DATE AS dueDate
                             ,cha.Account_Vendor_Name AS utility
                             ,LEFT(Rates.Rate_Names, LEN(Rates.Rate_Names) - 1) AS rates
                             ,c.Commodity_Name AS commodity
                             ,cha.service_level_char
                             ,cha.tariff_transport
                             --,b.budget_start_year AS startYear
                             ,b.budget_start_month AS startMonth
                             ,b.posted_by AS isPosted
                             ,Record_Number = ROW_NUMBER() OVER ( ORDER BY ' + CONVERT(NVARCHAR(MAX), @SortColumn) + ' ' + CONVERT(NVARCHAR(MAX), @SortIndex) + ')
                         FROM
                              #Accounts cha
                              INNER JOIN Core.Client_Hier AS ch
                                    ON cha.Client_Hier_Id = ch.Client_Hier_Id
                              INNER JOIN dbo.BUDGET_ACCOUNT AS ba
                                    ON cha.Account_Id = ba.ACCOUNT_ID
                              INNER JOIN dbo.BUDGET AS b
                                    ON ba.BUDGET_ID = b.BUDGET_ID
                                       AND cha.Commodity_Id = b.COMMODITY_TYPE_ID
                              INNER JOIN dbo.Commodity AS c
                                    ON cha.Commodity_Id = c.Commodity_Id
                              CROSS APPLY ( SELECT
                                                cha2.Rate_Name + '', ''
                                            FROM
												Core.Client_Hier_Account AS cha2
											WHERE
												cha2.Account_Id = cha.Account_Id
												AND cha2.Commodity_Id = cha.Commodity_Id
   GROUP BY
                                                cha2.Rate_Name
											FOR
                                            XML PATH('''') ) Rates ( Rate_Names )
                         WHERE
                              ba.IS_DELETED = 0' + CASE WHEN @Is_History = 0 THEN N'
                              AND b.POSTED_BY IS NULL
                              AND b.Analyst_Queue_Display_Cd IN ( @Both_Queue_Cd, @Rates_Queue_Cd )
                            AND ( ( cha.RA_RM = 1
                                      AND ba.RATES_COMPLETED_BY IS NULL )
                                    OR ( cha.RA_RM = 2
                                         AND ba.RATES_COMPLETED_BY IS NOT NULL
                                         AND ba.RATES_REVIEWED_BY IS NULL ) )' + CASE WHEN @Is_Before_Due_Date = 1 THEN N'
                               AND b.DUE_DATE <= @Due_Date )'                         WHEN @Is_Before_Due_Date = 0 THEN N')'
                                                                                 END
                                                        WHEN @Is_History = 1 THEN ' 
                               AND ( ba.RATES_COMPLETED_BY IS NOT NULL
                                     AND ba.RATES_REVIEWED_BY IS NOT NULL
                                     OR ba.CANNOT_PERFORMED_BY IS NOT NULL ))
'
                                                   END + N'
                  SELECT
                        budgetId
                        ,budgetstartyear
                        ,budgetname
                       ,clientName
                       ,SiteName
                       ,accountId
                       ,budgetAccountId
                       ,accountNumber
                       ,dueDate
                       ,utility
                       ,rates
                       ,commodity
                       ,service_level_char
                       ,tariff_transport
                       --,startYear
                       ,startMonth
                       ,isPosted
                       ,Record_Number
                       ,@Total_RowCount AS Total_RowCount
                  FROM
                        CTE_ResultSet
                  WHERE
                        Record_Number BETWEEN @Start_Index AND @End_Index';

                  SELECT
                        @ParamList = N'
            @userId INT
           ,@Due_Date DATE
           ,@Is_Before_Due_Date BIT
           ,@Is_History BIT
           ,@Start_Index INT
           ,@End_Index INT
           ,@Rates_Queue_Cd INT
           ,@Both_Queue_Cd INT
           ,@Total_RowCount INT';

                  INSERT      INTO #Budget_Queue_Results
                              (budgetId
                              ,budgetstartyear
                              ,budgetname
                              ,clientName
                              ,SiteName
                              ,accountId
                              ,budgetAccountId
                              ,accountNumber
                              ,dueDate
                              ,utility
                              ,rates
                              ,commodity
                              ,service_level_char
                              ,tariff_transport
                              --,startYear
                              ,startMonth
                              ,isPosted
                              ,Record_Number
                              ,Total_Rowcount )
                              EXEC sys.sp_executesql
                                    @SQLStatement
                                   ,@ParamList
                                   ,@userId
                                   ,@Due_Date
                                   ,@Is_Before_Due_Date
                                   ,@Is_History
                                   ,@Start_Index
                                   ,@End_Index
                                   ,@Rates_Queue_Cd
                                   ,@Both_Queue_Cd
                                   ,@Total_RowCount;

                  UPDATE
                        #Budget_Queue_Results
                  SET
                        User_Info_Id = @User_Info_id
                  WHERE
                        User_Info_Id IS NULL;
	
                  SET @Current_Row_Num = @Current_Row_Num + 1;

            END;

--

      SELECT
            ui.FIRST_NAME + ' ' + ui.LAST_NAME AS [User Full Name]
           ,ui.USERNAME AS [User Name]
           ,qr.budgetId AS [Budget Id]
           ,qr.budgetstartyear [Budget Year]
           ,qr.budgetname [Budget Name]
           ,qr.clientName AS [Client]
           ,qr.SiteName AS [Site]
           ,qr.accountNumber AS [Account]
           ,qr.dueDate AS [Due Date]
           ,qr.utility
           ,qr.rates
           ,qr.commodity
           ,qr.service_level_char AS [Service Level]
           ,qr.tariff_transport AS [Tariff/Transport]
           ,CASE WHEN qr.dueDate <= DATEADD(dd, 30, GETDATE()) THEN '<<= 30 days'
                 ELSE '>>30 days'
            END AS Due_In
      FROM
            #Budget_Queue_Results qr
            INNER JOIN dbo.USER_INFO ui
                  ON ui.USER_INFO_ID = qr.User_Info_Id
      ORDER BY
            ui.USERNAME
           ,qr.budgetId
           ,qr.accountNumber;

END;
;
GO

GRANT EXECUTE ON  [dbo].[Report_Regional_Manager_Budget_Queue_Dtl] TO [CBMS_SSRS_Reports]
GO
