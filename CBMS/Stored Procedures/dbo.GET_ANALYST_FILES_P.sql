SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.GET_ANALYST_FILES_P
	@USER_INFO_ID INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT vwaf.client_name
		, vwaf.site_name
		, vwaf.site_id
		, vwaf.address_line1
		, vwaf.address_line2
		, vwaf.city
		, vwaf.state_name
		, vwaf.zipcode
		, vwaf.Country
		, vwaf.UtilityAccountNumber	
		, vwaf.UtilityAccountServiceLevel
		, vwaf.UtilityAccountOnWatchList
		, vwaf.UtilityAccountPartofGroupBill
		, vwaf.Utility
		, vwaf.Commodity
		, vwaf.Rate
		, vwaf.meter_number	
		, vwaf.Supplier
		, vwaf.[Contract]
		, vwaf.contract_start_date
		, vwaf.contract_end_date
		, vwaf.SupplierAccountNumber
		, vwaf.SupplierAccountServiceLevel
		, vwaf.SupplierAccountOnWatchList
		, vwaf.GroupBill
		, vwaf.LastRateComparison	
		, vwaf.RateAnalyst
		, vwaf.RateManager
	FROM dbo.vw_ANALYST_FILES vwaf INNER JOIN dbo.USER_INFO ui ON ui.UserName=vwaf.RateAnalyst
	WHERE (ui.USER_INFO_ID = @USER_INFO_ID)
option (maxdop 1)
END
GO
GRANT EXECUTE ON  [dbo].[GET_ANALYST_FILES_P] TO [CBMSApplication]
GO
