SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Account_Inovice_Collection_History_Del_By_Account_Id         
                
Description:                
   This sproc is used to Delete the Invoice Collection Account Configuration Details.        
                             
 Input Parameters:                
    Name										DataType   Default   Description                  
--------------------------------------------------------------------------------------                  
                         
   
 Output Parameters:                      
    Name        DataType   Default   Description                  
--------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
--------------------------------------------------------------------------------------     
BEGIN Transaction
	EXEC dbo.Account_Inovice_Collection_History_Del_By_Account_Id 488808
Rollback Transaction

Author Initials:                
Initials  Name                
--------------------------------------------------------------------------------------                  
 RKV    Ravi Kumar Vegesna  

 Modifications:                
 Initials        Date			Modification                
-------------------------------------------------------------------------------------                  
  RKV			2017-03-08		Created For DeleteTool(Contract PlaceHolder).           
  HG			2017-10-01		PRSUPPORT-1069, SELF Join on the Account_Invoice_Collection_Online_Source_Dtl & Invoice_Collection_Account_Contact table wiped out the table , corrected the alias name to delete only the IC history associated with the account being deleted.             
  RKV           2017-10-07      MAINT-5967,Add functionality to delete the Archived issues,chased,Exception Comment Records.
  RKV           2018-01-09      SE2017-389  Modified the code to delete all the records irrespective of status.
  RKV           2019-06-19      IC - Added Two new tables ETL Image and ETL File
  RKV           2019-07-16      IC - Added Exclude Comments new table 
  RKV           2019-09-03      IC - Added Two new tables Activity and Final Review Tables  
******/
CREATE PROCEDURE [dbo].[Account_Inovice_Collection_History_Del_By_Account_Id]
     (
         @Account_Id INT
     )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Loop INT = 1
            , @Delete_Loop INT = 1;

        DECLARE @Invoice_Collection_Account_Config_Ids TABLE
              (
                  Invoice_Collection_Account_Config_Id INT
                  , Sno INT IDENTITY(1, 1)
              );

        DECLARE @Account_Invoice_Collection_Month_Cu_Invoice_Map TABLE
              (
                  Sno INT IDENTITY(1, 1)
                  , Account_Invoice_Collection_Month_Id INT
                  , Cu_Invoice_Id INT
              );

        DECLARE @Invoice_Collection_Queue_Month_Map TABLE
              (
                  Sno INT IDENTITY(1, 1)
                  , Invoice_Collection_Queue_Id INT
                  , Account_Invoice_Collection_Month_Id INT
              );

        DECLARE @Invoice_Collection_Chase_Log TABLE
              (
                  Sno INT IDENTITY(1, 1)
                  , Invoice_Collection_Chase_Log_Id INT
                  , Invoice_Collection_Queue_Id INT
              );
        DECLARE @Invoice_Collection_Final_Review_Log TABLE
              (
                  Sno INT IDENTITY(1, 1)
                  , Invoice_Collection_Final_Review_Log_Id INT
                  , Invoice_Collection_Queue_Id INT
              );
        DECLARE @Invoice_Collection_Activity_Log TABLE
              (
                  Sno INT IDENTITY(1, 1)
                  , Invoice_Collection_Activity_Log_Id INT
                  , Invoice_Collection_Queue_Id INT
              );

        DECLARE @Invoice_Collection_Issue_Log TABLE
              (
                  Sno INT IDENTITY(1, 1)
                  , Invoice_Collection_Issue_Log_Id INT
                  , Invoice_Collection_Queue_Id INT
              );

        DECLARE @Invoice_Collection_Exception_Comment TABLE
              (
                  Sno INT IDENTITY(1, 1)
                  , Invoice_Collection_Exception_Comment_Id INT
                  , Invoice_Collection_Queue_Id INT
              );
        DECLARE @Invoice_Collection_Queue_Exclude_Comment TABLE
              (
                  Sno INT IDENTITY(1, 1)
                  , Invoice_Collection_Queue_Exclude_Comment_Id INT
                  , Invoice_Collection_Queue_Id INT
              );

        DECLARE @Invoice_Collection_Queue TABLE
              (
                  Sno INT IDENTITY(1, 1)
                  , Invoice_Collection_Queue_Id INT
              );

        DECLARE @Account_Invoice_Collection_Month TABLE
              (
                  Sno INT IDENTITY(1, 1)
                  , Account_Invoice_Collection_Month_Id INT
              );

        DECLARE @Invoice_Collection_Account_Contact TABLE
              (
                  Sno INT IDENTITY(1, 1)
                  , Invoice_Collection_Account_Config_Id INT
                  , Contact_Info_Id INT
              );

        DECLARE @Account_Invoice_Collection_UBM_Source_Dtl TABLE
              (
                  Sno INT IDENTITY(1, 1)
                  , Account_Invoice_Collection_UBM_Source_Dtl_Id INT
              );

        DECLARE @Account_Invoice_Collection_Online_Source_Dtl TABLE
              (
                  Sno INT IDENTITY(1, 1)
                  , Account_Invoice_Collection_Online_Source_Dtl_Id INT
              );

        DECLARE @Account_Invoice_Collection_Source TABLE
              (
                  Sno INT IDENTITY(1, 1)
                  , Account_Invoice_Collection_Source_Id INT
              );

        DECLARE @Account_Invoice_Collection_Frequency TABLE
              (
                  Sno INT IDENTITY(1, 1)
                  , Account_Invoice_Collection_Frequency_Id INT
              );

        DECLARE @Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl TABLE
              (
                  Sno INT IDENTITY(1, 1)
                  , Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl_Id INT
              );
        DECLARE @Account_Invoice_Collection_Data_Feed_Etl_Image_Source_Dtl TABLE
              (
                  Sno INT IDENTITY(1, 1)
                  , Account_Invoice_Collection_Data_Feed_Etl_Image_Source_Dtl_Id INT
              );
        INSERT INTO @Invoice_Collection_Account_Config_Ids
             (
                 Invoice_Collection_Account_Config_Id
             )
        SELECT
            Invoice_Collection_Account_Config_Id
        FROM
            dbo.Invoice_Collection_Account_Config icac
        WHERE
            Account_Id = @Account_Id;
        BEGIN TRY
            BEGIN TRAN;


            --Account_Invoice_Collection_Month_Cu_Invoice_Map Delete
            SET @Delete_Loop = 1;

            INSERT INTO @Account_Invoice_Collection_Month_Cu_Invoice_Map
                 (
                     Account_Invoice_Collection_Month_Id
                     , Cu_Invoice_Id
                 )
            SELECT
                aicmcim.Account_Invoice_Collection_Month_Id
                , aicmcim.Cu_Invoice_Id
            FROM
                dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map aicmcim
                INNER JOIN dbo.Account_Invoice_Collection_Month aicm
                    ON aicm.Account_Invoice_Collection_Month_Id = aicmcim.Account_Invoice_Collection_Month_Id
                INNER JOIN @Invoice_Collection_Account_Config_Ids icaci
                    ON icaci.Invoice_Collection_Account_Config_Id = aicm.Invoice_Collection_Account_Config_Id;

            WHILE @Delete_Loop <= (SELECT   MAX(Sno)FROM    @Account_Invoice_Collection_Month_Cu_Invoice_Map)
                BEGIN
                    DELETE
                    aicmcim
                    FROM
                        dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map aicmcim
                        INNER JOIN @Account_Invoice_Collection_Month_Cu_Invoice_Map aicmcim2
                            ON aicmcim.Account_Invoice_Collection_Month_Id = aicmcim2.Account_Invoice_Collection_Month_Id
                               AND  aicmcim.Cu_Invoice_Id = aicmcim2.Cu_Invoice_Id
                    WHERE
                        aicmcim2.Sno = @Delete_Loop;

                    SET @Delete_Loop = @Delete_Loop + 1;
                END;




            -- Deleting Month Id Form ICQ Mapping


            SET @Delete_Loop = 1;

            INSERT INTO @Invoice_Collection_Queue_Month_Map
                 (
                     Invoice_Collection_Queue_Id
                     , Account_Invoice_Collection_Month_Id
                 )
            SELECT
                icqmm.Invoice_Collection_Queue_Id
                , icqmm.Account_Invoice_Collection_Month_Id
            FROM
                dbo.Invoice_Collection_Queue_Month_Map icqmm
                INNER JOIN dbo.Account_Invoice_Collection_Month aicm
                    ON aicm.Account_Invoice_Collection_Month_Id = icqmm.Account_Invoice_Collection_Month_Id
                INNER JOIN @Invoice_Collection_Account_Config_Ids icaci
                    ON icaci.Invoice_Collection_Account_Config_Id = aicm.Invoice_Collection_Account_Config_Id;


            WHILE @Delete_Loop <= (SELECT   MAX(Sno)FROM    @Invoice_Collection_Queue_Month_Map)
                BEGIN

                    DELETE
                    icqmm
                    FROM
                        dbo.Invoice_Collection_Queue_Month_Map icqmm
                        INNER JOIN @Invoice_Collection_Queue_Month_Map icqmm2
                            ON icqmm.Invoice_Collection_Queue_Id = icqmm2.Invoice_Collection_Queue_Id
                               AND  icqmm.Account_Invoice_Collection_Month_Id = icqmm2.Account_Invoice_Collection_Month_Id
                    WHERE
                        icqmm2.Sno = @Delete_Loop;


                    SET @Delete_Loop = @Delete_Loop + 1;
                END;


            -- Invoice_Collection_Chase_Log Delete

            SET @Delete_Loop = 1;

            INSERT INTO @Invoice_Collection_Chase_Log
                 (
                     Invoice_Collection_Chase_Log_Id
                     , Invoice_Collection_Queue_Id
                 )
            SELECT
                icclqm.Invoice_Collection_Chase_Log_Id
                , icclqm.Invoice_Collection_Queue_Id
            FROM
                dbo.Invoice_Collection_Chase_Log iccl
                INNER JOIN dbo.Invoice_Collection_Chase_Log_Queue_Map icclqm
                    ON iccl.Invoice_Collection_Chase_Log_Id = icclqm.Invoice_Collection_Chase_Log_Id
                INNER JOIN dbo.Invoice_Collection_Queue icq
                    ON icclqm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                INNER JOIN @Invoice_Collection_Account_Config_Ids icaci
                    ON icaci.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
            GROUP BY
                icclqm.Invoice_Collection_Chase_Log_Id
                , icclqm.Invoice_Collection_Queue_Id;




            WHILE @Delete_Loop <= (SELECT   MAX(Sno)FROM    @Invoice_Collection_Chase_Log)
                BEGIN

                    DELETE
                    icclqm
                    FROM
                        dbo.Invoice_Collection_Chase_Log_Queue_Map icclqm
                        INNER JOIN @Invoice_Collection_Chase_Log iccl
                            ON icclqm.Invoice_Collection_Chase_Log_Id = iccl.Invoice_Collection_Chase_Log_Id
                               AND  icclqm.Invoice_Collection_Queue_Id = iccl.Invoice_Collection_Queue_Id
                    WHERE
                        iccl.Sno = @Delete_Loop;


                    SET @Delete_Loop = @Delete_Loop + 1;
                END;

            -- Invoice_Collection_Activity_Log Delete

            SET @Delete_Loop = 1;

            INSERT INTO @Invoice_Collection_Activity_Log
                 (
                     Invoice_Collection_Activity_Log_Id
                     , Invoice_Collection_Queue_Id
                 )
            SELECT
                icalqm.Invoice_Collection_Activity_Log_Id
                , icalqm.Invoice_Collection_Queue_Id
            FROM
                dbo.Invoice_Collection_Activity_Log ical
                INNER JOIN dbo.Invoice_Collection_Activity_Log_Queue_Map icalqm
                    ON ical.Invoice_Collection_Activity_Log_Id = icalqm.Invoice_Collection_Activity_Log_Id
                INNER JOIN dbo.Invoice_Collection_Queue icq
                    ON icalqm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                INNER JOIN @Invoice_Collection_Account_Config_Ids icaci
                    ON icaci.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
            GROUP BY
                icalqm.Invoice_Collection_Activity_Log_Id
                , icalqm.Invoice_Collection_Queue_Id;




            WHILE @Delete_Loop <= (SELECT   MAX(Sno)FROM    @Invoice_Collection_Activity_Log)
                BEGIN

                    DELETE
                    icalqm
                    FROM
                        dbo.Invoice_Collection_Activity_Log_Queue_Map icalqm
                        INNER JOIN @Invoice_Collection_Activity_Log ical
                            ON icalqm.Invoice_Collection_Activity_Log_Id = ical.Invoice_Collection_Activity_Log_Id
                               AND  icalqm.Invoice_Collection_Queue_Id = ical.Invoice_Collection_Queue_Id
                    WHERE
                        ical.Sno = @Delete_Loop;


                    SET @Delete_Loop = @Delete_Loop + 1;
                END;



            -- Invoice_Collection_Final_Review_Log Delete

            SET @Delete_Loop = 1;

            INSERT INTO @Invoice_Collection_Final_Review_Log
                 (
                     Invoice_Collection_Final_Review_Log_Id
                     , Invoice_Collection_Queue_Id
                 )
            SELECT
                icfrlqm.Invoice_Collection_Final_Review_Log_Id
                , icfrlqm.Invoice_Collection_Queue_Id
            FROM
                dbo.Invoice_Collection_Final_Review_Log icfrl
                INNER JOIN dbo.Invoice_Collection_Final_Review_Log_Queue_Map icfrlqm
                    ON icfrl.Invoice_Collection_Final_Review_Log_Id = icfrlqm.Invoice_Collection_Final_Review_Log_Id
                INNER JOIN dbo.Invoice_Collection_Queue icq
                    ON icfrlqm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                INNER JOIN @Invoice_Collection_Account_Config_Ids icaci
                    ON icaci.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
            GROUP BY
                icfrlqm.Invoice_Collection_Final_Review_Log_Id
                , icfrlqm.Invoice_Collection_Queue_Id;




            WHILE @Delete_Loop <= (SELECT   MAX(Sno)FROM    @Invoice_Collection_Final_Review_Log)
                BEGIN

                    DELETE
                    icfrlqm
                    FROM
                        dbo.Invoice_Collection_Final_Review_Log_Queue_Map icfrlqm
                        INNER JOIN @Invoice_Collection_Final_Review_Log icfrl
                            ON icfrlqm.Invoice_Collection_Final_Review_Log_Id = icfrl.Invoice_Collection_Final_Review_Log_Id
                               AND  icfrlqm.Invoice_Collection_Queue_Id = icfrl.Invoice_Collection_Queue_Id
                    WHERE
                        icfrl.Sno = @Delete_Loop;


                    SET @Delete_Loop = @Delete_Loop + 1;
                END;

            -- Invoice_Collection_Issue_Log Delete

            SET @Delete_Loop = 1;



            INSERT INTO @Invoice_Collection_Issue_Log
                 (
                     Invoice_Collection_Issue_Log_Id
                     , Invoice_Collection_Queue_Id
                 )
            SELECT
                icil.Invoice_Collection_Issue_Log_Id
                , icil.Invoice_Collection_Queue_Id
            FROM
                dbo.Invoice_Collection_Issue_Log icil
                INNER JOIN dbo.Invoice_Collection_Queue icq
                    ON icil.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                INNER JOIN @Invoice_Collection_Account_Config_Ids icaci
                    ON icaci.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
            GROUP BY
                icil.Invoice_Collection_Issue_Log_Id
                , icil.Invoice_Collection_Queue_Id;


            WHILE @Delete_Loop <= (SELECT   MAX(Sno)FROM    @Invoice_Collection_Issue_Log)
                BEGIN

                    DELETE
                    icia
                    FROM
                        dbo.Invoice_Collection_Issue_Attachment icia
                        INNER JOIN @Invoice_Collection_Issue_Log icil
                            ON icia.Invoice_Collection_Issue_Log_Id = icil.Invoice_Collection_Issue_Log_Id
                    WHERE
                        icil.Sno = @Delete_Loop;


                    DELETE
                    icie
                    FROM
                        dbo.Invoice_Collection_Issue_Event icie
                        INNER JOIN @Invoice_Collection_Issue_Log icil
                            ON icie.Invoice_Collection_Issue_Log_Id = icil.Invoice_Collection_Issue_Log_Id
                    WHERE
                        icil.Sno = @Delete_Loop;



                    DELETE
                    icil
                    FROM
                        Invoice_Collection_Issue_Log icil
                        INNER JOIN @Invoice_Collection_Issue_Log icil2
                            ON icil.Invoice_Collection_Issue_Log_Id = icil2.Invoice_Collection_Issue_Log_Id
                    WHERE
                        icil2.Sno = @Delete_Loop;


                    SET @Delete_Loop = @Delete_Loop + 1;
                END;


            -- Invoice_Collection_Queue_Exclude_Comment Delete

            SET @Delete_Loop = 1;



            INSERT INTO @Invoice_Collection_Queue_Exclude_Comment
                 (
                     Invoice_Collection_Queue_Exclude_Comment_Id
                     , Invoice_Collection_Queue_Id
                 )
            SELECT
                icec.Invoice_Collection_Queue_Exclude_Comment_Id
                , icec.Invoice_Collection_Queue_Id
            FROM
                dbo.Invoice_Collection_Queue_Exclude_Comment icec
                INNER JOIN dbo.Invoice_Collection_Queue icq
                    ON icec.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                INNER JOIN @Invoice_Collection_Account_Config_Ids icaci
                    ON icaci.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
            GROUP BY
                icec.Invoice_Collection_Queue_Exclude_Comment_Id
                , icec.Invoice_Collection_Queue_Id;


            WHILE @Delete_Loop <= (SELECT   MAX(Sno)FROM    @Invoice_Collection_Queue_Exclude_Comment)
                BEGIN
                    DELETE
                    icec
                    FROM
                        dbo.Invoice_Collection_Queue_Exclude_Comment icec
                        INNER JOIN @Invoice_Collection_Queue_Exclude_Comment icec2
                            ON icec.Invoice_Collection_Queue_Exclude_Comment_Id = icec2.Invoice_Collection_Queue_Exclude_Comment_Id
                    WHERE
                        icec2.Sno = @Delete_Loop;

                    SET @Delete_Loop = @Delete_Loop + 1;
                END;




            -- Invoice_Collection_Exception_Comment Delete

            SET @Delete_Loop = 1;



            INSERT INTO @Invoice_Collection_Exception_Comment
                 (
                     Invoice_Collection_Exception_Comment_Id
                     , Invoice_Collection_Queue_Id
                 )
            SELECT
                icec.Invoice_Collection_Exception_Comment_Id
                , icec.Invoice_Collection_Queue_Id
            FROM
                dbo.Invoice_Collection_Exception_Comment icec
                INNER JOIN dbo.Invoice_Collection_Queue icq
                    ON icec.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                INNER JOIN @Invoice_Collection_Account_Config_Ids icaci
                    ON icaci.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
            GROUP BY
                icec.Invoice_Collection_Exception_Comment_Id
                , icec.Invoice_Collection_Queue_Id;


            WHILE @Delete_Loop <= (SELECT   MAX(Sno)FROM    @Invoice_Collection_Exception_Comment)
                BEGIN

                    DELETE
                    icec
                    FROM
                        dbo.Invoice_Collection_Exception_Comment icec
                        INNER JOIN @Invoice_Collection_Exception_Comment icec2
                            ON icec.Invoice_Collection_Exception_Comment_Id = icec2.Invoice_Collection_Exception_Comment_Id
                    WHERE
                        icec2.Sno = @Delete_Loop;


                    DELETE
                    icie
                    FROM
                        dbo.Invoice_Collection_Issue_Event icie
                        INNER JOIN @Invoice_Collection_Issue_Log icil
                            ON icie.Invoice_Collection_Issue_Log_Id = icil.Invoice_Collection_Issue_Log_Id
                    WHERE
                        icil.Sno = @Delete_Loop;



                    DELETE
                    icil
                    FROM
                        Invoice_Collection_Issue_Log icil
                        INNER JOIN @Invoice_Collection_Issue_Log icil2
                            ON icil.Invoice_Collection_Issue_Log_Id = icil2.Invoice_Collection_Issue_Log_Id
                    WHERE
                        icil2.Sno = @Delete_Loop;


                    SET @Delete_Loop = @Delete_Loop + 1;
                END;

            --Deleting the ICQ Table
            SET @Delete_Loop = 1;

            INSERT INTO @Invoice_Collection_Queue
                 (
                     Invoice_Collection_Queue_Id
                 )
            SELECT
                Invoice_Collection_Queue_Id
            FROM
                dbo.Invoice_Collection_Queue icq
                INNER JOIN @Invoice_Collection_Account_Config_Ids icaci
                    ON icaci.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id;


            WHILE @Delete_Loop <= (SELECT   MAX(Sno)FROM    @Invoice_Collection_Queue)
                BEGIN


                    DELETE
                    icq
                    FROM
                        dbo.Invoice_Collection_Queue icq
                        INNER JOIN @Invoice_Collection_Queue icq1
                            ON icq.Invoice_Collection_Queue_Id = icq1.Invoice_Collection_Queue_Id
                    WHERE
                        icq1.Sno = @Delete_Loop;


                    SET @Delete_Loop = @Delete_Loop + 1;
                END;

            --Deleting the month IDs

            SET @Delete_Loop = 1;

            INSERT INTO @Account_Invoice_Collection_Month
                 (
                     Account_Invoice_Collection_Month_Id
                 )
            SELECT
                Account_Invoice_Collection_Month_Id
            FROM
                dbo.Account_Invoice_Collection_Month aicm
                INNER JOIN @Invoice_Collection_Account_Config_Ids icaci
                    ON icaci.Invoice_Collection_Account_Config_Id = aicm.Invoice_Collection_Account_Config_Id;

            WHILE @Delete_Loop <= (SELECT   MAX(Sno)FROM    @Account_Invoice_Collection_Month)
                BEGIN


                    DELETE
                    aicm
                    FROM
                        dbo.Account_Invoice_Collection_Month aicm
                        INNER JOIN @Account_Invoice_Collection_Month aicm1
                            ON aicm.Account_Invoice_Collection_Month_Id = aicm1.Account_Invoice_Collection_Month_Id
                    WHERE
                        aicm1.Sno = @Delete_Loop;

                    SET @Delete_Loop = @Delete_Loop + 1;
                END;



            --Deleting the Account Contact

            SET @Delete_Loop = 1;


            INSERT INTO @Invoice_Collection_Account_Contact
                 (
                     Invoice_Collection_Account_Config_Id
                     , Contact_Info_Id
                 )
            SELECT
                icac.Invoice_Collection_Account_Config_Id
                , icac.Contact_Info_Id
            FROM
                dbo.Invoice_Collection_Account_Contact icac
                INNER JOIN @Invoice_Collection_Account_Config_Ids icaci
                    ON icaci.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id;

            WHILE @Delete_Loop <= (SELECT   MAX(Sno)FROM    @Invoice_Collection_Account_Contact)
                BEGIN


                    DELETE
                    icac
                    FROM
                        dbo.Invoice_Collection_Account_Contact icac
                        INNER JOIN @Invoice_Collection_Account_Contact icac1
                            ON icac1.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                               AND  icac.Contact_Info_Id = icac1.Contact_Info_Id
                    WHERE
                        icac1.Sno = @Delete_Loop;

                    SET @Delete_Loop = @Delete_Loop + 1;
                END;



            --Deleting the UBM_Source_Dtl

            SET @Delete_Loop = 1;


            INSERT INTO @Account_Invoice_Collection_UBM_Source_Dtl
                 (
                     Account_Invoice_Collection_UBM_Source_Dtl_Id
                 )
            SELECT
                sd.Account_Invoice_Collection_UBM_Source_Dtl_Id
            FROM
                dbo.Account_Invoice_Collection_UBM_Source_Dtl sd
                INNER JOIN dbo.Account_Invoice_Collection_Source aic
                    ON sd.Account_Invoice_Collection_Source_Id = aic.Account_Invoice_Collection_Source_Id
                INNER JOIN @Invoice_Collection_Account_Config_Ids icaci
                    ON icaci.Invoice_Collection_Account_Config_Id = aic.Invoice_Collection_Account_Config_Id;

            WHILE @Delete_Loop <= (SELECT   MAX(Sno)FROM    @Account_Invoice_Collection_UBM_Source_Dtl)
                BEGIN

                    DELETE
                    sd
                    FROM
                        dbo.Account_Invoice_Collection_UBM_Source_Dtl sd
                        INNER JOIN @Account_Invoice_Collection_UBM_Source_Dtl sd1
                            ON sd1.Account_Invoice_Collection_UBM_Source_Dtl_Id = sd.Account_Invoice_Collection_UBM_Source_Dtl_Id
                    WHERE
                        sd1.Sno = @Delete_Loop;

                    SET @Delete_Loop = @Delete_Loop + 1;
                END;

            --Deleting the Online_Source_Dtl

            SET @Delete_Loop = 1;

            INSERT INTO @Account_Invoice_Collection_Online_Source_Dtl
                 (
                     Account_Invoice_Collection_Online_Source_Dtl_Id
                 )
            SELECT
                os.Account_Invoice_Collection_Online_Source_Dtl_Id
            FROM
                dbo.Account_Invoice_Collection_Online_Source_Dtl os
                INNER JOIN dbo.Account_Invoice_Collection_Source aic
                    ON os.Account_Invoice_Collection_Source_Id = aic.Account_Invoice_Collection_Source_Id
                INNER JOIN @Invoice_Collection_Account_Config_Ids icaci
                    ON icaci.Invoice_Collection_Account_Config_Id = aic.Invoice_Collection_Account_Config_Id;

            WHILE @Delete_Loop <= (SELECT   MAX(Sno)FROM    @Account_Invoice_Collection_Online_Source_Dtl)
                BEGIN

                    DELETE
                    os
                    FROM
                        dbo.Account_Invoice_Collection_Online_Source_Dtl os
                        INNER JOIN @Account_Invoice_Collection_Online_Source_Dtl os1
                            ON os1.Account_Invoice_Collection_Online_Source_Dtl_Id = os.Account_Invoice_Collection_Online_Source_Dtl_Id
                    WHERE
                        os1.Sno = @Delete_Loop;

                    SET @Delete_Loop = @Delete_Loop + 1;
                END;

            --Deleting the Data Feed ETL File DTL

            SET @Delete_Loop = 1;

            INSERT INTO @Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl
                 (
                     Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl_Id
                 )
            SELECT
                aicdfefsd.Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl_Id
            FROM
                dbo.Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl AS aicdfefsd
                INNER JOIN dbo.Account_Invoice_Collection_Source aic
                    ON aicdfefsd.Account_Invoice_Collection_Source_Id = aic.Account_Invoice_Collection_Source_Id
                INNER JOIN @Invoice_Collection_Account_Config_Ids icaci
                    ON icaci.Invoice_Collection_Account_Config_Id = aic.Invoice_Collection_Account_Config_Id;

            WHILE @Delete_Loop <= (   SELECT
                                            MAX(Sno)
                                      FROM
                                            @Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl)
                BEGIN

                    DELETE
                    os
                    FROM
                        dbo.Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl os
                        INNER JOIN @Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl os1
                            ON os1.Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl_Id = os.Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl_Id
                    WHERE
                        os1.Sno = @Delete_Loop;

                    SET @Delete_Loop = @Delete_Loop + 1;
                END;

            --Deleting the Data Feed ETL Image DTL

            SET @Delete_Loop = 1;

            INSERT INTO @Account_Invoice_Collection_Data_Feed_Etl_Image_Source_Dtl
                 (
                     Account_Invoice_Collection_Data_Feed_Etl_Image_Source_Dtl_Id
                 )
            SELECT
                aicdfeisd.Account_Invoice_Collection_Data_Feed_Etl_Image_Source_Dtl_Id
            FROM
                dbo.Account_Invoice_Collection_Data_Feed_Etl_Image_Source_Dtl AS aicdfeisd
                INNER JOIN dbo.Account_Invoice_Collection_Source aic
                    ON aicdfeisd.Account_Invoice_Collection_Source_Id = aic.Account_Invoice_Collection_Source_Id
                INNER JOIN @Invoice_Collection_Account_Config_Ids icaci
                    ON icaci.Invoice_Collection_Account_Config_Id = aic.Invoice_Collection_Account_Config_Id;

            WHILE @Delete_Loop <= (   SELECT
                                            MAX(Sno)
                                      FROM
                                            @Account_Invoice_Collection_Data_Feed_Etl_Image_Source_Dtl)
                BEGIN

                    DELETE
                    os
                    FROM
                        dbo.Account_Invoice_Collection_Data_Feed_Etl_Image_Source_Dtl os
                        INNER JOIN @Account_Invoice_Collection_Data_Feed_Etl_Image_Source_Dtl os1
                            ON os1.Account_Invoice_Collection_Data_Feed_Etl_Image_Source_Dtl_Id = os.Account_Invoice_Collection_Data_Feed_Etl_Image_Source_Dtl_Id
                    WHERE
                        os1.Sno = @Delete_Loop;

                    SET @Delete_Loop = @Delete_Loop + 1;
                END;

            --Deleting the Account_Invoice_Collection_Source details

            SET @Delete_Loop = 1;

            INSERT INTO @Account_Invoice_Collection_Source
                 (
                     Account_Invoice_Collection_Source_Id
                 )
            SELECT
                aic.Account_Invoice_Collection_Source_Id
            FROM
                dbo.Account_Invoice_Collection_Source aic
                INNER JOIN @Invoice_Collection_Account_Config_Ids icaci
                    ON icaci.Invoice_Collection_Account_Config_Id = aic.Invoice_Collection_Account_Config_Id;

            WHILE @Delete_Loop <= (SELECT   MAX(Sno)FROM    @Account_Invoice_Collection_Source)
                BEGIN

                    DELETE
                    aic
                    FROM
                        dbo.Account_Invoice_Collection_Source aic
                        INNER JOIN @Account_Invoice_Collection_Source aic1
                            ON aic1.Account_Invoice_Collection_Source_Id = aic.Account_Invoice_Collection_Source_Id
                    WHERE
                        aic1.Sno = @Delete_Loop;

                    SET @Delete_Loop = @Delete_Loop + 1;
                END;



            --Deleting the Frequency details

            SET @Delete_Loop = 1;

            INSERT INTO @Account_Invoice_Collection_Frequency
                 (
                     Account_Invoice_Collection_Frequency_Id
                 )
            SELECT
                aicf.Account_Invoice_Collection_Frequency_Id
            FROM
                dbo.Account_Invoice_Collection_Frequency aicf
                INNER JOIN @Invoice_Collection_Account_Config_Ids icaci
                    ON icaci.Invoice_Collection_Account_Config_Id = aicf.Invoice_Collection_Account_Config_Id;

            WHILE @Delete_Loop <= (SELECT   MAX(Sno)FROM    @Account_Invoice_Collection_Frequency)
                BEGIN

                    DELETE
                    aicf
                    FROM
                        dbo.Account_Invoice_Collection_Frequency aicf
                        INNER JOIN @Account_Invoice_Collection_Frequency aicf1
                            ON aicf1.Account_Invoice_Collection_Frequency_Id = aicf.Account_Invoice_Collection_Frequency_Id
                    WHERE
                        aicf1.Sno = @Delete_Loop;

                    SET @Delete_Loop = @Delete_Loop + 1;
                END;



            WHILE @Loop <= (SELECT  MAX(Sno)FROM    @Invoice_Collection_Account_Config_Ids)
                BEGIN

                    -- Delete Account Config
                    DELETE
                    aic
                    FROM
                        dbo.Invoice_Collection_Account_Config aic
                        INNER JOIN @Invoice_Collection_Account_Config_Ids icaci
                            ON icaci.Invoice_Collection_Account_Config_Id = aic.Invoice_Collection_Account_Config_Id
                    WHERE
                        icaci.Sno = @Loop;


                    SET @Loop = @Loop + 1;
                END;

            COMMIT TRAN;
        END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0
                ROLLBACK TRAN;

            EXEC dbo.usp_RethrowError;

        END CATCH;

    END;
    ;



GO
GRANT EXECUTE ON  [dbo].[Account_Inovice_Collection_History_Del_By_Account_Id] TO [CBMSApplication]
GO
