SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
    
--[cbmsDataFeedPage_Sel] 'DataFeedMapping',71920    
CREATE PROCEDURE [dbo].[cbmsDataFeedPage_Sel]              
 (                   
 @app_name varchar(100),    
 @User_Info_Id int                
 )                    
AS                    
BEGIN                    
 --'DataFeedMapping'                
    
 DECLARE @app_config TABLE (    
    Config_Name varchar(255),    
 Display_Seq int,    
 Nav_Link varchar(255),    
 Is_Default bit    
 );    
    
     
 insert into @app_config            
 select distinct agc.Config_Name,agc.Display_Seq,agc.Nav_Link,agc.Is_Default                 
 from group_info GI             
 JOIN user_info_group_info_map UIG ON GI.group_info_id = UIG.group_info_id             
 JOIN user_info U ON U.user_info_id = UIG.user_info_id             
 JOIN group_info_permission_info_map GIPM ON GIPM.group_info_id = GI.group_info_id             
 JOIN permission_info P WITH (NOLOCK) ON P.permission_info_id = GIPM.permission_info_id     
 inner join App_Global_Config agc    ON agc.Permission_Info_Id = P.Permission_Info_Id           
 inner join code c_app on c_app.code_id = agc.App_Module_Cd                  
 where c_app.code_value = @app_name      
 and U.USER_INFO_ID=@User_Info_Id               
 and agc.Is_Active = 1      
     
 select * from @app_config              
                 
 select Filter_Display_Name,c_filter.code_value,asf.Is_Default,asf.Is_Hidden,asf.Is_Smart_Search_Filter,asf.Display_Seq,asf. Data_Source_Query                 
 from App_Global_Config_Search_Filter asf                  
   inner join App_Global_Config agc on asf.App_Global_Config_Id = agc.App_Global_Config_Id                  
   left join code c_app on c_app.code_id = agc.App_Module_Cd                  
   left join code c_filter on c_filter.code_id = asf.Filter_Type_Cd                  
 where c_app.code_value = @app_name                  
 and agc.Is_Default = 1                 
 and asf.Is_Active = 1      
 and agc.Config_Name in ( select Config_Name from @app_config)         
                  
 select Column_Display_Name,Data_Source_Column_Name,asf.Display_Seq,Date_Format,Column_Header_CSS,asf.Is_Active,        
   Data_Source_Query, Is_Visible_To_User, c_sel.code_value AS [Selection_Filter_Type], Is_Editable, c_link.code_value AS [Link_Type]    
 from App_Global_Config_Output_Column asf                  
   inner join App_Global_Config agc on asf.App_Global_Config_Id = agc.App_Global_Config_Id                  
   left join code c_app on c_app.code_id = agc.App_Module_Cd           
   left join code c_sel on c_sel.code_id = asf.Selection_Filter_Type_Cd                     
   left join code c_link on c_link.code_id = asf.Link_Type_CD   
 where c_app.code_value = @app_name                  
 and agc.Is_Default = 1                 
 and asf.Is_Active = 1     
 and agc.Config_Name in ( select Config_Name from @app_config)                  
 order by asf.Display_Seq      
                  
END 

GO
GRANT EXECUTE ON  [dbo].[cbmsDataFeedPage_Sel] TO [CBMSApplication]
GO
