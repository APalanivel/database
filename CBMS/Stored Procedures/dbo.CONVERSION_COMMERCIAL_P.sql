SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[CONVERSION_COMMERCIAL_P] 

AS
BEGIN

	DECLARE @site_name varchar(200)
	DECLARE @client_id int
	declare @division_id int
	DECLARE @site_id varchar(50)
	DECLARE @address_id int
	DECLARE @util_account_id int
	DECLARE @meter_id int
	DECLARE @created_site_id int
	DECLARE @util_account_number varchar(50)
	DECLARE @meter_number varchar(50)
	declare @site_count int
	declare @account_count int
	declare @meter_count int
	declare @contracting_entity varchar (100)  
	declare @fein_number varchar (50)  
	declare @duns_number varchar (50)
	DECLARE @client_legal_structure varchar (2000)  

	declare @lp_contact_first_name varchar (50)  
	declare @lp_contact_last_name varchar (50)
	declare @lp_contact_email_address varchar (50)
	declare @lp_contact_cc varchar (50)

	DECLARE @supp_account_id int
	DECLARE @month_identifier datetime
	DECLARE @contract_id int
	DECLARE @billing_determinant_master_id int

	DECLARE @supplier_id int
	DECLARE @commodity_type varchar(50)
	DECLARE @start_date datetime
	DECLARE @end_date datetime
	DECLARE @pricing varchar(1000)
	DECLARE @trigger_rights bit
	DECLARE @full_requirements bit
	DECLARE @renewal_type varchar(100) 
	DECLARE @notification_days decimal (12,2)
	DECLARE @site_comments varchar(1000) 
	DECLARE @contract_comments varchar(4000) 
	declare @contract_count int
	declare @util_account_number_contract varchar(50)
	declare @state_name varchar(10)
	declare @is_primary_address bit
	DECLARE @contract_type varchar(10)  
	DECLARE @supplier_account_number varchar(50)
	DECLARE @group_account_number varchar(50)  
	DECLARE @account_number varchar(50)  
	DECLARE @group_account_id INT  
    
	DECLARE @error AS INT
	SET @error = 0
	SET @site_count = 0
	SET @account_count = 0
	SET @meter_count = 0
	SET @contract_count = 0

	SET NOCOUNT ON

	BEGIN TRY
	
		BEGIN TRAN

			DECLARE C_CLIENT CURSOR FAST_FORWARD
				FOR
				SELECT DISTINCT client_id FROM conversion_commercial

			OPEN C_CLIENT
				
				FETCH NEXT FROM C_CLIENT INTO @client_id
				WHILE (@@fetch_status <> -1)
				BEGIN
					IF (@@fetch_status <> -2)
					BEGIN
						DECLARE C_SITE CURSOR FAST_FORWARD
							FOR
							SELECT DISTINCT site_id FROM conversion_commercial WHERE client_id = @client_id

						OPEN C_SITE
						FETCH NEXT FROM C_SITE INTO @site_id
						WHILE (@@fetch_status <> -1)
						BEGIN
							IF (@@fetch_status <> -2)
							BEGIN
								--if (@client_id = 226 and @site_id = '561')
								--begin
									--print @site_id
								SELECT 
									DISTINCT TOP 1 @division_id = division_id,
									@site_name = site_name,
									@contracting_entity = contracting_entity,
									@client_legal_structure = client_legal_structure, 
									@fein_number = fein_number,  
									@duns_number = duns_number,
									@lp_contact_first_name = lp_contact_first_name, 
									@lp_contact_last_name = lp_contact_last_name, 
									@lp_contact_email_address = lp_contact_email_address 
								FROM 
									conversion_commercial conv
								WHERE 
									conv.client_id = @client_id
									AND conv.SITE_ID = @site_id

								print 'client id ->' + str(@client_id)
								print 'site name ->' + @site_name

								-- Insert into site	
								INSERT INTO SITE (site_type_id, site_name, division_id, site_reference_number, client_legal_structure, contracting_entity,tax_number,duns_number,   
									is_history, is_preference_by_dv,is_preference_by_email,SITE_PRODUCT_SERVICE,PRODUCTION_SCHEDULE,SHUTDOWN_SCHEDULE,NAICS_CODE, 
									lp_contact_first_name, lp_contact_last_name, lp_contact_email_address, lp_contact_cc, Client_id)
								SELECT
									 entity_id, @site_name, @division_id, @site_id, @client_legal_structure,@contracting_entity,@fein_number,@duns_number,
									 0,0,1,'','','','',@lp_contact_first_name,@lp_contact_last_name,@lp_contact_email_address,null,@Client_id 
								FROM 
									entity siteType  
								WHERE 
									siteType.entity_name = 'Commercial' and siteType.entity_type = 150

								--SET @error = @error + @@error
								
								SELECT @created_site_id = SCOPE_IDENTITY() --@@IDENTITY
								
								PRINT 'created site id->' + str(@created_site_id)

								SET @site_count = @site_count + 1	
								
								-- Enter audit information for site
								INSERT INTO entity_audit (entity_id, entity_identifier, user_info_id, audit_type, modified_date) 
								SELECT 
									entity_id, @created_site_id, user_info_id, 1, getdate()
								FROM 
									entity entity
									CROSS JOIN user_info ui
								WHERE 
									entity.entity_name = 'SITE_TABLE' AND 
									entity.entity_type = 500 AND
									ui.username = 'conversion'					

								--SET @error = @error + @@error

								--insert into sitegroup_site
								--insert into sitegroup_site (sitegroup_id, site_id)
								--values (@division_id, @created_site_id)
							
								--SET @error = @error + @@error	
								
								SELECT 
									DISTINCT @is_primary_address = is_primary_Address
								FROM 
									--entity addrType, state st, conversion_commercial conv
										entity addrType 
										CROSS JOIN state st
										CROSS JOIN conversion_commercial conv
								WHERE 
									addrType.entity_name = 'Site' and addrType.entity_type = 115
									and conv.client_id = @client_id
									and conv.site_id = @site_id
									and conv.site_name = @site_name
									and conv.state = st.state_name
								
								-- Insert primary address of site in Address	
					
								INSERT INTO ADDRESS (address_type_id, state_id, address_line1, address_line2, city, zipcode, address_parent_id, address_parent_type_id, GEO_LAT, GEO_LONG, is_primary_address)
									SELECT 
										DISTINCT entity_id, st.state_id, conv.site_address, conv.site_address_line2, conv.city, conv.zip_code, @created_site_id, entity_id, GEO_LAT, GEO_LONG, is_primary_address
									FROM 
										--entity addrType, state st, conversion_commercial conv
										entity addrType 
										CROSS JOIN state st
										CROSS JOIN conversion_commercial conv
									WHERE 
										addrType.entity_name = 'Site' and addrType.entity_type = 115
										and conv.client_id = @client_id
										and conv.site_id = @site_id
										and conv.site_name = @site_name
										and conv.state = st.state_name
								
								--SET @error = @error + @@error

								SELECT @address_id = SCOPE_IDENTITY()-- @@IDENTITY
								PRINT 'created address id->' + STR(@address_id)
								
								-- update site with this generated addres_id	
								IF @is_primary_address = 1
								 BEGIN
								 
									UPDATE SITE SET primary_address_id = @address_id where site_id = @created_site_id

								 END

								--SET @error = @error + @@error
								
								-- Insert rm_onboard_hedge_setup for this site
								/*INSERT INTO rm_onboard_hedge_setup (rm_onboard_client_id, division_id, site_id, hedge_type_id, include_in_reports)
									SELECT 
										rm_onboard_client_id, site.division_id, @created_site_id, hedgeType.entity_id, 0
									FROM 
										--rm_onboard_client oc, entity hedgeType, site site
										rm_onboard_client oc
										CROSS JOIN entity hedgeType
										CROSS JOIN site site
									WHERE oc.client_id = @client_id
										AND site.site_id = @created_site_id
										AND hedgeType.entity_name = 'Does not hedge' and hedgeType.entity_type = 273
								
								--SET @error = @error + @@error
								
								-- Enter audit information for rm_onboard_hedge_setup, identifier is the client_id
								INSERT INTO entity_audit (entity_id, entity_identifier, user_info_id, audit_type, modified_date) 
									SELECT 
										entity_id, @client_id, user_info_id, 2, GETDATE()
									FROM
										--entity entity, user_info ui
										entity entity
										CROSS JOIN user_info ui
									WHERE entity.entity_name = 'RM_ONBOARD_HEDGE_SETUP' and 
										entity.entity_type = 500 AND
										ui.username = 'conversion'

								--set @error = @error + @@error

								DECLARE C_UTIL_ACC CURSOR FAST_FORWARD
								FOR
									SELECT 
										DISTINCT account_number 
									FROM 
										conversion_commercial 
									WHERE client_id = @client_id and site_id = @site_id
								OPEN C_UTIL_ACC
								FETCH NEXT FROM C_UTIL_ACC INTO @util_account_number
								WHILE (@@fetch_status <> -1)
								BEGIN
									IF (@@fetch_status <> -2 and @util_account_number IS NOT NULL)
									 BEGIN

										-- Insert Utility Account	
										INSERT INTO account (vendor_id, site_id, invoice_source_type_id, account_number, account_type_id, service_level_type_id)
											SELECT 
												DISTINCT conv.vendor_id, @created_site_id, invSourceType.entity_id, conv.account_number, accType.entity_id, serviceLevelType.entity_id
											FROM
												conversion_commercial conv, entity invSourceType, entity accType, entity serviceLevelType
											WHERE 
												conv.client_id = @client_id
												AND conv.site_id = @site_id
												AND conv.account_number = @util_account_number
												AND invSourceType.entity_name = 'UBM' and invSourceType.entity_type = 110
												AND accType.entity_name = 'Utility' and accType.entity_type = 111
												AND serviceLevelType.entity_name = 'C' and serviceLevelType.entity_type = 708

										--set @error = @error + @@error

										SELECT @util_account_id = SCOPE_IDENTITY() --@@IDENTITY
										PRINT 'created util acc id->' + str(@util_account_id)

										SET @account_count = @account_count + 1	

									-- Enter audit information for account
									INSERT INTO entity_audit (entity_id, entity_identifier, user_info_id, audit_type, modified_date) 
									SELECT 
										entity_id, @util_account_id, user_info_id, 1, GETDATE()
									FROM 
										--entity entity, user_info ui
										entity entity
										CROSS JOIN user_info ui
									WHERE
										entity.entity_name = 'ACCOUNT_TABLE' and entity.entity_type = 500
										and ui.username = 'conversion'

									--set @error = @error + @@error

									DECLARE C_METER CURSOR FAST_FORWARD
										FOR								
										SELECT 
											DISTINCT meter 
										FROM 
											conversion_commercial 
										WHERE 
											client_id = @client_id and 
											site_id = @site_id and 
											account_number = @util_account_number

									OPEN C_METER
									FETCH NEXT FROM C_METER INTO @meter_number
									WHILE (@@fetch_status <> -1)
									BEGIN
										IF (@@fetch_status <> -2 AND @meter_number!= '')
										BEGIN

											SELECT 
												@address_id = address_id
											FROM 
												--address addr, conversion_commercial conv, state st
												address addr
												JOIN conversion_commercial conv ON conv.city = addr.city
													AND conv.zip_code = addr.zipcode
													AND conv.site_address = addr.address_line1
												JOIN state st ON addr.state_id = st.state_id
													AND conv.state = st.state_name										
											WHERE 
												conv.client_id = @client_id
												and conv.site_id = @site_id
												and conv.account_number = @util_account_number
												and conv.meter = @meter_number


										-- Insert Meter	
										INSERT INTO meter (rate_id, purchase_method_type_id, account_id, address_id, meter_number)
											SELECT 
												conv.rate_id, purMethod.entity_id, @util_account_id, @address_id, conv.meter
											FROM 
												--conversion_commercial conv, entity purMethod
												conversion_commercial conv
												JOIN entity purMethod ON purMethod.entity_name = conv.purchase_method
											WHERE 
												conv.client_id = @client_id
												and conv.site_id = @site_id
												and conv.account_number = @util_account_number
												and conv.meter = @meter_number
												and purMethod.entity_type = 145
				
										--set @error = @error + @@error

										SELECT @meter_id = SCOPE_IDENTITY() --@@IDENTITY
										print 'created meter id->' + str(@meter_id)
										
										SET @meter_count = @meter_count + 1	

										END
										FETCH NEXT FROM C_METER INTO @meter_number
										END
										CLOSE C_METER
										DEALLOCATE C_METER

									END
									
									FETCH NEXT FROM C_UTIL_ACC INTO @util_account_number
									
								END
								CLOSE C_UTIL_ACC
								DEALLOCATE C_UTIL_ACC

								--end*/
							END
						FETCH NEXT FROM C_SITE INTO @site_id
						END
						CLOSE C_SITE
						DEALLOCATE C_SITE

					END
				FETCH NEXT FROM C_CLIENT INTO @client_id
				END
			CLOSE C_CLIENT
			DEALLOCATE C_CLIENT

			print 'site count -->' + str(@site_count)
			print 'account count -->' + str(@account_count)
			print 'meter count -->' + str(@meter_count)

/*
			DECLARE C_CONTRACT CURSOR READ_ONLY  
			FOR  
			SELECT 
				DISTINCT client_id, state, supplier_id, commodity_type, start_date, end_date, pricing,    
				trigger_rights, full_requirements, renewal_type, notification_days,site_comments, contract_type, supplier_Account_number,  
				group_account_number, account_number  
			FROM 
				conversion_commercial   
			WHERE 
				supplier_id IS NOT NULL

			OPEN C_CONTRACT	  
			FETCH NEXT FROM C_CONTRACT INTO
				@client_id, @state_name, @supplier_id, @commodity_type, @start_date, @end_date, @pricing,   
				@trigger_rights, @full_requirements, @renewal_type, @notification_days, @contract_comments, @contract_type,   
				@supplier_Account_number, @group_account_number, @account_number  
			  
			WHILE (@@fetch_status <> -1)  
			BEGIN  
				 IF (@@fetch_status <> -2)  
				 BEGIN  
				  
					 -- Insert supplier account    
					 INSERT INTO account (vendor_id, invoice_source_type_id, account_type_id, service_level_type_id, account_number)  
					 SELECT 
						@supplier_id, invSourceType.entity_id, accType.entity_id, serviceLevelType.entity_id, @supplier_Account_number  
					 FROM 
						--entity invSourceType, entity accType, entity serviceLevelType  
						entity invSourceType
						CROSS JOIN entity accType
						CROSS JOIN entity serviceLevelType
					 WHERE
						invSourceType.entity_name = 'UBM' and invSourceType.entity_type = 110  
						and accType.entity_name = 'Supplier' and accType.entity_type = 111 			  
						and serviceLevelType.entity_name = 'D' and serviceLevelType.entity_type = 708  
					  
					 --set @error = @error + @@error  
					  
					 SELECT @supp_account_id = SCOPE_IDENTITY() --@@IDENTITY  
					 print 'Created supp account id ->' + str(@supp_account_id)  

						 --set @contract_comments = ''  
					  
					 print 'contract comments ->' + @contract_comments  
					   
					 INSERT INTO account_group (group_billing_number, vendor_id)  
					 VALUES (@group_account_number, @supplier_id)  
					  
					 --set @error = @error + @@error  
					  
					 SELECT @group_account_id = SCOPE_IDENTITY() --@@IDENTITY  
					 PRINT 'Created Group account id ->' + STR(@group_account_id)  

					 UPDATE ACCOUNT 
						SET account_group_id = @group_account_id 
					WHERE account_number = @account_number
						and account_type_id = 38

					--set @error = @error + @@error
			  
					UPDATE account
						SET account_group_id = @group_account_id 
					WHERE account_id = @supp_account_id  
			   
					--set @error = @error + @@error  
			   
					 -- Insert into Contract   
					 INSERT INTO CONTRACT (account_id, renewal_type_id, contract_type_id, commodity_type_id, currency_unit_id, contract_start_date, contract_end_date, is_contract_trigger_rights, is_contract_full_requirement, notification_days, contract_pricing_summary, 
						contract_comments, contract_recalc_type_id, is_only_one_baseload, is_volumes_not_required)  
					 SELECT 
						@supp_account_id, renewalType.entity_id, conType.entity_id, commo.entity_id, currency_unit_id, @start_date, @end_date, @trigger_rights, @full_requirements, @notification_days, @pricing, @contract_comments, recalcType.entity_id, 1, 0  
					 FROM 
						--entity renewalType, entity conType, entity commo, currency_unit, entity recalcType   
						entity renewalType
						CROSS JOIN entity conType
						CROSS JOIN entity commo
						CROSS JOIN currency_unit
						CROSS JOIN entity recalcType
					 WHERE 
						renewalType.entity_name = @renewal_type and renewalType.entity_type = 128  
						 and conType.entity_name = @contract_type and conType.entity_type = 129  
						 and commo.entity_name = @commodity_type and commo.entity_type = 157  
						 and recalcType.entity_name = 'No Recalculation' and recalcType.entity_type = 301  
						 and currency_unit.currency_unit_name = 'USD'  
			   
					--set @error = @error + @@error  
			  
					SELECT @contract_id = SCOPE_IDENTITY() --@@IDENTITY  
			  
					 -- Update contract number  
					 UPDATE contract 
						SET ed_contract_number = LTRIM(STR(@contract_id)) + '-0001' 
					WHERE contract_id = @contract_id

					--SET @error = @error + @@error
			  
					 -- Enter audit information for contract  
					 INSERT INTO entity_audit (entity_id, entity_identifier, user_info_id, audit_type, modified_date)   
					 SELECT 
						entity_id, @contract_id, user_info_id, 1, getdate()  
					 FROM 
						--entity entity, user_info ui  
						entity entity
						CROSS JOIN user_info ui
					 WHERE entity.entity_name = 'CONTRACT_TABLE' and entity.entity_type = 500  
						AND ui.username = 'conversion'
						
					--SET @error = @error + @@error  
			 
					PRINT 'Created Contract -->' + str(@contract_id)
					SET @contract_count = @contract_count + 1 	  
			  
					 DECLARE C_CONTRACT_METERS CURSOR READ_ONLY  
					 FOR  
					 SELECT 
						DISTINCT meter, account_number 
					 FROM
						conversion_commercial 
					 WHERE 
						client_id = @client_id and supplier_id = @supplier_id   
						and start_date = @start_date and end_date = @end_date and pricing = @pricing and state = @state_name  
						and site_comments = @contract_comments and supplier_account_number = @supplier_account_number and commodity_type = @commodity_type  
						and renewal_type = @renewal_type and trigger_rights = @trigger_rights  

					 OPEN C_CONTRACT_METERS  
			   
					 FETCH NEXT FROM C_CONTRACT_METERS INTO @meter_number, @util_account_number_contract  
					 WHILE (@@fetch_status <> -1)  
					 BEGIN  
					  IF (@@fetch_status <> -2)  
					  BEGIN  
			  
						  SELECT 
								@meter_id = meter_id 
						  FROM 
								--meter met, account acc, conversion_commercial conv  
								meter met						
								JOIN conversion_commercial conv ON met.meter_number = conv.meter
								JOIN account acc ON met.account_id = acc.account_id
									AND acc.account_number = conv.account_number
						  WHERE met.meter_number = @meter_number
							  and acc.account_number = @util_account_number_contract
							  and conv.client_id = @client_id 
							  and conv.supplier_id = @supplier_id
							  and conv.start_date = @start_date  
							  and conv.end_date = @end_date  
							  and conv.pricing = @pricing  
							  and conv.state = @state_name  
							  and conv.supplier_account_number=@supplier_account_number  
							  and conv.site_comments=@contract_comments  
							  and conv.commodity_type=@commodity_type  
							  and conv.renewal_type= @renewal_type  
							  and conv.trigger_rights = @trigger_rights
						  
						  -- Insert into supplier_account_meter_map    
						  INSERT INTO supplier_account_meter_map (account_id, meter_id)  
						  VALUES (@supp_account_id, @meter_id)  
						  
						  --SET @error = @error + @@error  
						  
						  --Insert into contract_meter_volume    
						  SET @month_identifier = @start_date  
						  WHILE (@month_identifier <= @end_date)  
						  BEGIN
						  
						   INSERT INTO contract_meter_volume (meter_id, contract_id, volume, month_identifier)  
						   VALUES (@meter_id, @contract_id, 0, @month_identifier)  
						   
						   --SET @error = @error + @@error
						   set @month_identifier = dateadd(mm, 1, @month_identifier)  
						  END  
			  
					  END  
					 FETCH NEXT FROM C_CONTRACT_METERS INTO @meter_number, @util_account_number_contract  
					 END  
					 CLOSE C_CONTRACT_METERS  
					 DEALLOCATE C_CONTRACT_METERS  
			  
				 -- Insert into billing_determinant_master (A1)    
				 INSERT INTO billing_determinant_master (bd_type_id, bd_display_id, bd_parent_id, bd_parent_type_id)  
				 SELECT 
					bdType.entity_id, 'A1', @contract_id, parentType.entity_id  
				 FROM 
					--entity bdType, entity parentType
					entity bdType
					CROSS JOIN entity parentType
				 WHERE bdType.entity_name = 'New' and bdType.entity_type = 117  
					and parentType.entity_name = 'Contract' and parentType.entity_type = 120  

				 --set @error = @error + @@error  
			  
				 SELECT @billing_determinant_master_id = SCOPE_IDENTITY() --@@IDENTITY  

				 -- Insert into billing_determinant    
				 INSERT INTO billing_determinant (billing_determinant_master_id, billing_determinant_name, bd_parent_id, bd_parent_type_id)  
				 SELECT 
					@billing_determinant_master_id, 'From Date', @contract_id, parentType.entity_id  
				 FROM 
					entity parentType  
				 WHERE
					parentType.entity_name = 'Contract' and parentType.entity_type = 120  
				   
				 --set @error = @error + @@error  
			  
				 -- Insert into billing_determinant_master A2    
				 INSERT INTO billing_determinant_master (bd_type_id, bd_display_id, bd_parent_id, bd_parent_type_id)  
				 SELECT 
					bdType.entity_id, 'A2', @contract_id, parentType.entity_id  
				 FROM 
					--entity bdType, entity parentType  
					entity bdType
					CROSS JOIN entity parentType
				 WHERE bdType.entity_name = 'New' and bdType.entity_type = 117  
					AND parentType.entity_name = 'Contract' and parentType.entity_type = 120  

				--SET @error = @error + @@error
				
				SELECT @billing_determinant_master_id = SCOPE_IDENTITY() --@@IDENTITY

				 -- Insert into billing_determinant    
				 INSERT INTO billing_determinant (billing_determinant_master_id, billing_determinant_name, bd_parent_id, bd_parent_type_id)  
				 SELECT @billing_determinant_master_id, 'To Date', @contract_id, parentType.entity_id  
				 FROM 
					entity parentType  
				 WHERE 
					parentType.entity_name = 'Contract' and parentType.entity_type = 120  
			   
				--set @error = @error + @@error  
			  
				 -- Insert into billing_determinant_master A3    
				 INSERT INTO billing_determinant_master (bd_type_id, bd_display_id, bd_parent_id, bd_parent_type_id)  
				 SELECT 
					bdType.entity_id, 'A3', @contract_id, parentType.entity_id  
				 FROM 
					--entity bdType, entity parentType  
					entity bdType
					CROSS JOIN entity parentType
				 WHERE bdType.entity_name = 'New' and bdType.entity_type = 117  
					and parentType.entity_name = 'Contract' and parentType.entity_type = 120  

				--set @error = @error + @@error  

				SELECT @billing_determinant_master_id = SCOPE_IDENTITY() --@@IDENTITY

				--Insert into billing_determinant    
				INSERT INTO billing_determinant (billing_determinant_master_id, billing_determinant_name, bd_parent_id, bd_parent_type_id)  
				SELECT 
					@billing_determinant_master_id, 'Billing Days', @contract_id, parentType.entity_id  
				FROM 
					entity parentType  
				WHERE 
					parentType.entity_name = 'Contract' and parentType.entity_type = 120  
			   
				--set @error = @error + @@error  
			  
			 END  

			FETCH NEXT FROM C_CONTRACT INTO   
				@client_id, @state_name, @supplier_id, @commodity_type, @start_date, @end_date, @pricing,   
				@trigger_rights, @full_requirements, @renewal_type, @notification_days,@contract_comments, @contract_type,  
				@supplier_Account_number, @group_account_number, @account_number  

			END   
			CLOSE C_CONTRACT  
			DEALLOCATE C_CONTRACT  

			print 'contract count -->' + str(@contract_count)
			*/
		COMMIT TRAN	

	END TRY
	BEGIN CATCH 
		ROLLBACK TRAN
		
		--EXEC usp_RethrowError;
		
	END CATCH

	--if @error = 0
	--	COMMIT TRAN
	--ELSE
	--	ROLLBACK TRAN

	SET NOCOUNT OFF	
END
GO
GRANT EXECUTE ON  [dbo].[CONVERSION_COMMERCIAL_P] TO [CBMSApplication]
GO
