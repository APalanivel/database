SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









--exec dbo.BUDGET_GET_GENERATION_DEFAULTS_P '','',29175

CREATE    PROCEDURE dbo.BUDGET_GET_GENERATION_DEFAULTS_P
	@user_id varchar(10),
	@session_id varchar(10),
	@contract_id int
	AS
	begin
		set nocount on

		select	isnull(contract_defaults.volume,100) as volume,
			avg(isnull(index_detail.index_detail_value, 0))as index_detail_value,
			case when conv.conversion_factor > 0
			     then ((isnull(contract_defaults.adder,0) * curr_conv.conversion_factor)/conv.conversion_factor)
			     else 0
			end adder,
			-1 as fuel,
			contract_defaults.multiplier,
			contract_defaults.tax,
			contract_defaults.is_nymex_forecast as is_nymex,
			contract_defaults.budget_contract_defaults_id
	
		from	budget_contract_budget contract_budget
			join budget_contract_defaults contract_defaults
			on contract_defaults.budget_contract_budget_id = contract_budget.budget_contract_budget_id
			and contract_budget.contract_id = @contract_id

			join contract con on con.contract_id = contract_budget.contract_id
			left join clearport_index_months index_months on index_months.clearport_index_id = contract_defaults.market_id
			and index_months.clearport_index_month between dateadd(month, 1, convert(datetime,str(datepart(mm, con.contract_end_date))+'/01/'+str(datepart(yyyy, con.contract_end_date))))and dateadd(month, 12, convert(datetime, str(datepart(mm, con.contract_end_date))+'/01/'+str(datepart(yyyy, con.contract_end_date)))) 
			left join clearport_index_detail index_detail on index_detail.clearport_index_month_id = index_months.clearport_index_month_id
			and index_detail.index_detail_date = (select max(index_detail_date) from clearport_index_detail where clearport_index_month_id = index_detail.clearport_index_month_id),

			consumption_unit_conversion conv,
			currency_unit_conversion curr_conv

		where 	conv.base_unit_id = contract_defaults.volume_unit_type_id
			and conv.converted_unit_id = 12 --// kWh
			and curr_conv.base_unit_id = contract_defaults.currency_unit_id
			and curr_conv.converted_unit_id = 3 --// USD 
			and curr_conv.conversion_date = (select max(conversion_date) from currency_unit_conversion where base_unit_id = contract_defaults.currency_unit_id and converted_unit_id = 3 and currency_group_id = 3)
			and curr_conv.currency_group_id = 3

		group by contract_defaults.volume,
			conv.conversion_factor,
			contract_defaults.adder,
			curr_conv.conversion_factor,			     			
			contract_defaults.multiplier,
			contract_defaults.tax,
			contract_defaults.is_nymex_forecast,
			contract_defaults.budget_contract_defaults_id
		
	end












GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_GENERATION_DEFAULTS_P] TO [CBMSApplication]
GO
