SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	cbms_prod.dbo.cbmsCuInvoice_GetByAccountPreviousSibling

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------ 	          	
	@account_id    	int       	          	
	@working_date  	datetime  	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC cbmsCuInvoice_GetByAccountPreviousSibling 95434,'2008-04-24'
	EXEC cbmsCuInvoice_GetByAccountPreviousSibling 35784,'2007-04-30'

	    
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan G

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script
     SKA        08/01/2010  References Account_ID from CU_Invoice to Cu_invoice_Account_Map
     SKA		03/12/2010	Added Schema owner for tables
							Removed nolock hints
							Removed unused input parameter @MyAccountId
	SSR			03/18/2010	Removed cu_invoice_account_map(Account_id) with cu_invoice_service_month(Account_id)
	HG			10/08/2010	Below mentioned changes made to fix #20385
							Begin_Date , End_Date and Billing days column fetched from CU_INVOICE_SERVICE_MONTH instead of Cu_Invoice as these are properties of account and account_id column moved during GB.

******/

CREATE PROCEDURE dbo.cbmsCuInvoice_GetByAccountPreviousSibling
(
	@account_id		INT
	, @working_date DATETIME
)
AS
BEGIN

	SET NOCOUNT ON

	DECLARE @Previous_WORKING_DATE DATETIME
	SET @Previous_WORKING_DATE = DATEADD(D, -1, @WORKING_DATE)

	SELECT
		im.account_id
		, MAX(im.service_month) service_month
		, im.Begin_Dt begin_date
		, im.End_Dt end_date
		, im.billing_days
	FROM
		dbo.cu_invoice i
		JOIN dbo.cu_invoice_service_month im
			ON im.cu_invoice_id = i.cu_invoice_id
	WHERE
		im.account_id = @account_id
		AND i.is_default = 1
		AND im.end_dt BETWEEN @Previous_WORKING_DATE AND @working_date
		AND im.service_month IS NOT NULL
	GROUP BY
		  im.account_id
		, im.Begin_Dt
		, im.End_Dt
		, im.billing_days

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCuInvoice_GetByAccountPreviousSibling] TO [CBMSApplication]
GO
