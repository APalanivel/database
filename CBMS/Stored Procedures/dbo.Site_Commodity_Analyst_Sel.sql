
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
      dbo.Site_Commodity_Analyst_Sel  
     
 DESCRIPTION:     
      Gets the Analyst for a given Client,Division,Site.  
        
 INPUT PARAMETERS:    
 Name   DataType Default Description    
------------------------------------------------------------    
 @Client_Id      INT       
 @Sitegroup_Id   INT   NULL  
 @Site_Id   INT   NULL                  
 @Commodity_Id   INT  
   
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 USAGE EXAMPLES:    
------------------------------------------------------------  
  
	EXEC dbo.Site_Commodity_Analyst_Sel 235,NULL,NULL,290  
	EXEC dbo.Site_Commodity_Analyst_Sel 235,NULL,NULL,290,1,1000,1
	EXEC dbo.Site_Commodity_Analyst_Sel 235,NULL,NULL,290,1,1000,4
	EXEC dbo.Site_Commodity_Analyst_Sel 235,NULL,NULL,290,1,1000,4,44
	EXEC dbo.Site_Commodity_Analyst_Sel NULL,NULL,NULL,290,1,50,1
	
	EXEC dbo.Site_Commodity_Analyst_Sel 11231,NULL,NULL,291,1,100
	EXEC dbo.Site_Commodity_Analyst_Sel 11231,NULL,NULL,291,1,100,2
	EXEC dbo.Site_Commodity_Analyst_Sel 11231,NULL,NULL,291,1,100,4
	EXEC dbo.Site_Commodity_Analyst_Sel 11231,NULL,NULL,291,1,100,4,44
	EXEC dbo.Site_Commodity_Analyst_Sel NULL,NULL,NULL,291,1,100,1  
  
AUTHOR INITIALS:  
	Initials	Name  
------------------------------------------------------------   
	AKR			Ashok Kumar Raju 
	RR			Raghu Reddy 
   
  
 MODIFICATIONS     
	Initials	Date		Modification  
------------------------------------------------------------  
	AKR			2012-09-26  Created for POCO
	RR			2015-07-10	Global Sourcing - Added new input optional parameters @Country_Id, @State_Id and existing
							parameter @Client_Id made as optional, application will pass any one of these three parameters
  
******/  
CREATE  PROCEDURE [dbo].[Site_Commodity_Analyst_Sel]
      ( 
       @Client_Id INT = NULL
      ,@Sitegroup_Id INT = NULL
      ,@Site_Id INT = NULL
      ,@Commodity_Id INT
      ,@Start_Index INT = 1
      ,@End_Index INT = 2147483647
      ,@Country_Id INT = NULL
      ,@State_Id INT = NULL )
AS 
BEGIN  
      SET nocount ON  
        
      DECLARE @Commodity_Service_Cd INT  
        
      SELECT
            @Commodity_Service_Cd = Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'CommodityServiceSource'
            AND c.Code_Value = 'Invoice';  
      WITH  cte_Site
              AS ( SELECT
                        ch.Sitegroup_Name
                       ,ss.Site_name
                       ,ss.Site_Id
                       ,isnull(sca.Analyst_User_Info_Id, ca.Analyst_User_Info_Id) Analyst_User_Info_Id
                       ,row_number() OVER ( ORDER BY ch.Sitegroup_Name, ss.Site_name ) Row_Num
                       ,count(1) OVER ( ) AS Total_Row_Count
                       ,ss.State_Name
                       ,ss.Country_Name
                   FROM
                        Core.Client_Hier ch
                        INNER JOIN Core.Client_Commodity cc
                              ON ch.Client_Id = cc.Client_Id
                                 AND cc.Commodity_Id = @Commodity_Id
                                 AND cc.Commodity_Service_Cd = @Commodity_Service_Cd
                        INNER JOIN Core.Client_Hier ss
                              ON ch.Sitegroup_Id = ss.Sitegroup_id
                                 AND ss.site_id > 0
                        LEFT JOIN dbo.Site_Commodity_Analyst sca
                              ON ss.Site_id = sca.Site_Id
                                 AND sca.Commodity_Id = @Commodity_Id
                        LEFT JOIN dbo.Client_Commodity_Analyst ca
                              ON ca.Client_Commodity_Id = cc.Client_Commodity_Id
                   WHERE
                        ( @Client_Id IS NULL
                          OR ch.CLIENT_ID = @Client_Id )
                        AND ( @Sitegroup_Id IS NULL
                              OR ch.Sitegroup_Id = @Sitegroup_Id )
                        AND ( @Site_Id IS NULL
                              OR ss.Site_Id = @Site_Id )
                        AND ch.Site_Id = 0
                        AND ch.Sitegroup_Id > 0
                        AND ( @Country_Id IS NULL
                              OR ss.Country_Id = @Country_Id )
                        AND ( @State_Id IS NULL
                              OR ss.State_Id = @State_Id ))
            SELECT
                  cs.Sitegroup_Name
                 ,cs.Site_name
                 ,cs.Site_Id
                 ,cs.Analyst_User_Info_Id
                 ,cs.Row_Num
                 ,cs.Total_Row_Count
                 ,cs.State_Name
                 ,cs.Country_Name
            FROM
                  cte_Site cs
            WHERE
                  cs.Row_Num BETWEEN @Start_Index AND @End_Index  
     
END;


;
GO

GRANT EXECUTE ON  [dbo].[Site_Commodity_Analyst_Sel] TO [CBMSApplication]
GO
