SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[cbmsCuExceptionClosedReason_GetByExceptionType]
	( @MyAccountId int
	, @exception_type_id int
	)
AS
BEGIN

	   select e.entity_id
		, e.entity_name
	     from cu_exception_closed_reason_map map with (nolock)
	     join entity e with (nolock) on e.entity_id = map.closed_reason_type_id
	    where map.cu_exception_type_id = @exception_type_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCuExceptionClosedReason_GetByExceptionType] TO [CBMSApplication]
GO
