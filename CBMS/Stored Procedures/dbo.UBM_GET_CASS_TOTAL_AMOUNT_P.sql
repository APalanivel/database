SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UBM_GET_CASS_TOTAL_AMOUNT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@masterLogId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE   PROCEDURE dbo.UBM_GET_CASS_TOTAL_AMOUNT_P
	@masterLogId int	
	AS
set nocount on
	/*SELECT 
		CAST(SUM(BILL_CHARGE_AMOUNT) AS DECIMAL(20,3)) AS TOTAL_AMOUNT 
	FROM 
		UBM_CASS_BILL_CHARGES 
	WHERE 
		UBM_BATCH_MASTER_LOG_ID =78@masterLogId*/

	SELECT 
		CAST(SUM(AMOUNT_DUE) AS DECIMAL(20,3)) AS TOTAL_AMOUNT 
	FROM 
		UBM_CASS_BILL
	WHERE 
		UBM_BATCH_MASTER_LOG_ID =@masterLogId
GO
GRANT EXECUTE ON  [dbo].[UBM_GET_CASS_TOTAL_AMOUNT_P] TO [CBMSApplication]
GO
