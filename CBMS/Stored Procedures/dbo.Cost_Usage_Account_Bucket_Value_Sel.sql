SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
Name:  
 CBMS.dbo.Cost_Usage_Account_Bucket_Value_Sel
 
 Description:
 
 
 Input Parameters:  
    Name					DataType		    Default Description  
------------------------------------------------------------------------  
    @Bucket_List			Cost_Usage_Bucket	READONLY - Table Value Parameter
    @Report_Year			INT					NULL
    @Client_Id				INT					NULL
    @Division_Id			INT					NULL
    @Site_Client_Hier_Id	INT					NULL
    @RegionId				INT					NULL
    @Country_Id				INT					NULL
    @Site_Not_Managed		INT					NULL
    @Start_Index			INT					1
    @End_Index				INT					2147483647 
 
 Output Parameters:  
 Name  Datatype  Default Description  
------------------------------------------------------------  
 
 Usage Examples:
------------------------------------------------------------
  
DECLARE @Bucket_List1 AS Cost_Usage_Bucket
INSERT INTO @Bucket_List1
VALUES (290, 'Total Usage', 'Determinant', 12)
    ,(290, 'Total Cost', 'Charge', 3)
EXECUTE dbo.Cost_Usage_Account_Bucket_Value_Sel @Bucket_List1, 2005, 10069, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2
EXECUTE dbo.Cost_Usage_Account_Bucket_Value_Sel @Bucket_List1, 2011, 1043, NULL, 7191, NULL, NULL, NULL, NULL
EXECUTE dbo.Cost_Usage_Account_Bucket_Value_Sel @Bucket_List1, 2011



DECLARE @Bucket_List1 AS Cost_Usage_Bucket
INSERT INTO @Bucket_List1
VALUES (290, 'Total Usage', 'Determinant', 12)
--,(290, 'Total Cost', 'Charge', 3)
EXECUTE dbo.Cost_Usage_Account_Bucket_Value_Sel @Bucket_List1, 2010, 235, 258, NULL, NULL, NULL, NULL, 250, 1, 25
EXECUTE dbo.Cost_Usage_Account_Bucket_Value_Sel @Bucket_List1, 2011, 11554, 12208691, NULL, NULL, NULL, NULL, 250, 1,2147483647
 
Author Initials:  
 Initials	Name
------------------------------------------------------------
 AP			Athmaram Pabbathi
 RR			Raghu Reddy
 NR			Narayana Reddy
 
 Modifications :  
 Initials	  Date	    Modification  
------------------------------------------------------------  
 AP		  10/10/2011	Created and replaced following SPs as a part of Addtl Data changes
						- dbo.cbmsCostUsage_LoadForAccount
 AP		  2012-03-15	Added Client_Hier_Id & Data_Source_Cd in the result set
 AP		  2012-04-18	Replaced @Site_Id parameter with @Site_Client_Hier_Id
 AP		  2012-04-19	Replaced Account_Number case statement with cha.Display_Account_Number filed.
						Moved calculation of Bucket_Value to sub query and removed hardcoded uom_id's and used entity_table
 AP		  2012-04-25	Added UOM_Type_Id in the result set
 RR		  2012-06-04	Replaced Account_id with site's city in the CTE for generating row number. The final select order of records varying
						with different index range as row number have account_id in order but the final select have city in order.
 RR		  2012-06-07	Reverted the previous change and used the account_id for both row number generation and final SELECT, using city
						for row number generation is breaking the pagination(Displays fixed number of accounts for page).
 NR		  2020-07-01	SE2017-512 - Added @Rate_Id input parameter.
******/

CREATE PROCEDURE [dbo].[Cost_Usage_Account_Bucket_Value_Sel]
    (
        @Bucket_List AS dbo.Cost_Usage_Bucket READONLY
        , @Report_Year INT
        , @Client_Id INT = NULL
        , @Division_Id INT = NULL
        , @Site_Client_Hier_Id INT = NULL
        , @Region_Id INT = NULL
        , @State_Id INT = NULL
        , @Vendor_Id INT = NULL
        , @Site_Type_Id INT = NULL
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
        , @Rate_Id INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Start_Month DATE
            , @End_Month DATE
            , @Usd_Currency_Unit_Id INT
            , @Site_Type_Name VARCHAR(200);

        CREATE TABLE #Account_Dtl
             (
                 Client_Name VARCHAR(200)
                 , Sitegroup_Name VARCHAR(200)
                 , Site_Id INT
                 , Client_Hier_Id INT
                 , City VARCHAR(200)
                 , State_Name VARCHAR(20)
                 , Region_Name VARCHAR(200)
                 , Address_Line1 VARCHAR(200)
                 , Account_Id INT
                 , Account_Number VARCHAR(200)
                 , Vendor_Name VARCHAR(200)
                 , Client_Currency_Group_Id INT
                 , Total_Rows INT
                 , Rate_Name VARCHAR(200)
             );

        CREATE CLUSTERED INDEX IDX_Account_Dtl_Account_Id
            ON #Account_Dtl
        (
            Client_Hier_Id
            , Account_Id
        )   ;

        SELECT
            @Usd_Currency_Unit_Id = CU.CURRENCY_UNIT_ID
        FROM
            dbo.CURRENCY_UNIT CU
        WHERE
            CU.SYMBOL = 'USD';

        SELECT
            @Site_Type_Name = st.ENTITY_NAME
        FROM
            dbo.ENTITY st
        WHERE
            st.ENTITY_ID = @Site_Type_Id;

        SELECT
            @Start_Month = MIN(dd.DATE_D)
            , @End_Month = MAX(dd.DATE_D)
        FROM    (   SELECT
                        DATEADD(m, ch.Client_Fiscal_Offset, CAST('1/1/' + CAST(@Report_Year AS VARCHAR(4)) AS DATE)) Start_month
                    FROM
                        Core.Client_Hier ch
                    WHERE
                        ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id = 0
                        AND ch.Site_Id = 0
                    UNION ALL
                    SELECT
                        CAST('1/1/' + CAST(@Report_Year AS VARCHAR(4)) AS VARCHAR(10))
                    WHERE
                        @Client_Id IS NULL) X
                CROSS JOIN meta.DATE_DIM dd
        WHERE
            dd.DATE_D BETWEEN X.Start_month
                      AND     DATEADD(MONTH, -1, (DATEADD(YEAR, 1, X.Start_month)));


        WITH CTE_Account_Dtl
        AS (
               SELECT
                    CH.Client_Name
                    , CH.Sitegroup_Name
                    , CH.Site_Id
                    , CH.Client_Hier_Id
                    , CH.City
                    , CH.State_Name
                    , CH.Region_Name
                    , CH.Site_Address_Line1
                    , CHA.Account_Id
                    , CHA.Display_Account_Number AS Account_Number
                    , CHA.Account_Vendor_Name
                    , CH.Client_Currency_Group_Id
                    , ROW_NUMBER() OVER (ORDER BY
                                             CH.Client_Name
                                             , CH.Sitegroup_Name
                                             , CHA.Account_Id ASC) AS Row_Num
                    , COUNT(1) OVER () AS Total_Rows
                    , CHA.Rate_Name
               FROM
                    Core.Client_Hier CH
                    INNER JOIN Core.Client_Hier_Account CHA
                        ON CH.Client_Hier_Id = CHA.Client_Hier_Id
               WHERE
                    (   @Client_Id IS NULL
                        OR  CH.Client_Id = @Client_Id)
                    AND (   @Division_Id IS NULL
                            OR  CH.Sitegroup_Id = @Division_Id)
                    AND (   @Site_Client_Hier_Id IS NULL
                            OR  CH.Client_Hier_Id = @Site_Client_Hier_Id)
                    AND (   @Site_Type_Id IS NULL
                            OR  CH.Site_Type_Name = @Site_Type_Name)
                    AND (   @State_Id IS NULL
                            OR  CH.State_Id = @State_Id)
                    AND (   @Region_Id IS NULL
                            OR  CH.Region_ID = @Region_Id)
                    AND (   @Vendor_Id IS NULL
                            OR  CHA.Account_Vendor_Id = @Vendor_Id)
                    AND CH.Client_Not_Managed = 0
                    AND CH.Site_Not_Managed = 0
                    AND CH.Site_Closed = 0
                    AND CH.Division_Not_Managed = 0
                    AND CHA.Account_Not_Managed = 0
                    AND (   CHA.Supplier_Account_begin_Dt IS NULL
                            OR  CHA.Supplier_Account_begin_Dt <= @End_Month)
                    AND (   CHA.Supplier_Account_End_Dt IS NULL
                            OR  CHA.Supplier_Account_End_Dt >= @Start_Month)
                    AND (   @Rate_Id IS NULL
                            OR  CHA.Rate_Id = @Rate_Id)
               GROUP BY
                   CH.Client_Name
                   , CH.Sitegroup_Name
                   , CH.Site_Id
                   , CH.Client_Hier_Id
                   , CH.City
                   , CH.State_Name
                   , CH.Region_Name
                   , CH.Site_Address_Line1
                   , CHA.Account_Id
                   , CHA.Display_Account_Number
                   , CHA.Account_Vendor_Name
                   , CH.Client_Currency_Group_Id
                   , CHA.Rate_Name
           )
        INSERT INTO #Account_Dtl
             (
                 Client_Name
                 , Sitegroup_Name
                 , Site_Id
                 , Client_Hier_Id
                 , City
                 , State_Name
                 , Region_Name
                 , Address_Line1
                 , Account_Id
                 , Account_Number
                 , Vendor_Name
                 , Client_Currency_Group_Id
                 , Total_Rows
                 , Rate_Name
             )
        SELECT
            ad.Client_Name
            , ad.Sitegroup_Name
            , ad.Site_Id
            , ad.Client_Hier_Id
            , ad.City
            , ad.State_Name
            , ad.Region_Name
            , ad.Site_Address_Line1
            , ad.Account_Id
            , ad.Account_Number
            , ad.Account_Vendor_Name
            , ad.Client_Currency_Group_Id
            , ad.Total_Rows
            , ad.Rate_Name
        FROM
            CTE_Account_Dtl ad
        WHERE
            ad.Row_Num BETWEEN @Start_Index
                       AND     @End_Index;

        SELECT
            AD.Client_Name
            , AD.Sitegroup_Name AS Division_Name
            , AD.City
            , AD.State_Name
            , AD.Region_Name
            , AD.City AS Site_Name
            , AD.Address_Line1
            , AD.Account_Id
            , AD.Account_Number
            , AD.Vendor_Name
            , AD.Site_Id
            , AD.Client_Hier_Id
            , @Start_Month AS Fiscal_Start
            , @End_Month AS Fiscal_End
            , ISNULL(CUAD.Service_Month, '01/01/1900') AS Service_Month
            , CUAD.Commodity_Name
            , CUAD.Bucket_Name
            , CUAD.Bucket_Type
            , CUAD.Bucket_Value
            , AD.Total_Rows
            , CUAD.Data_Source_Code
            , CUAD.UOM_Type_Id
            , AD.Rate_Name
        FROM
            #Account_Dtl AD
            LEFT OUTER JOIN (   SELECT
                                    CUAD.ACCOUNT_ID
                                    , CUAD.Client_Hier_ID
                                    , CUAD.Service_Month
                                    , BM.Bucket_Name
                                    , CD.Code_Value AS Bucket_Type
                                    , CUAD.UOM_Type_Id
                                    , CUAD.CURRENCY_UNIT_ID
                                    , SUM(CASE WHEN BL.Bucket_Type = 'Determinant'
                                                    AND BM.Bucket_Name IN ( 'Demand', 'Billed Demand'
                                                                            , 'Contract Demand' )
                                                    AND uom.ENTITY_NAME IN ( 'kVa', 'kvar', 'kW' ) THEN
                                                   CUAD.Bucket_Value
                                              WHEN BL.Bucket_Type = 'Determinant'
                                                   AND  BM.Bucket_Name NOT IN ( 'Demand', 'Billed Demand'
                                                                                , 'Contract Demand' ) THEN
                                                  CUAD.Bucket_Value * UOMConv.CONVERSION_FACTOR
                                              WHEN BL.Bucket_Type = 'Charge' THEN
                                                  CUAD.Bucket_Value * CurConv.CONVERSION_FACTOR
                                          END) AS Bucket_Value
                                    , COM.Commodity_Name
                                    , dsc.Code_Value AS Data_Source_Code
                                FROM
                                    #Account_Dtl ad
                                    INNER JOIN dbo.Cost_Usage_Account_Dtl CUAD
                                        ON CUAD.ACCOUNT_ID = ad.Account_Id
                                           AND  CUAD.Client_Hier_ID = ad.Client_Hier_Id
                                    INNER JOIN dbo.Bucket_Master BM
                                        ON BM.Bucket_Master_Id = CUAD.Bucket_Master_Id
                                    INNER JOIN @Bucket_List BL
                                        ON BL.Bucket_Name = BM.Bucket_Name
                                           AND  BL.Commodity_Id = BM.Commodity_Id
                                    INNER JOIN dbo.Code CD
                                        ON CD.Code_Id = BM.Bucket_Type_Cd
                                           AND  CD.Code_Value = BL.Bucket_Type
                                    INNER JOIN dbo.Commodity COM
                                        ON COM.Commodity_Id = BM.Commodity_Id
                                    INNER JOIN dbo.Code dsc
                                        ON dsc.Code_Id = CUAD.Data_Source_Cd
                                    LEFT OUTER JOIN dbo.CONSUMPTION_UNIT_CONVERSION UOMConv
                                        ON UOMConv.BASE_UNIT_ID = CUAD.UOM_Type_Id
                                           AND  UOMConv.CONVERTED_UNIT_ID = COALESCE(
                                                                                BL.Bucket_Uom_Id
                                                                                , BM.Default_Uom_Type_Id)
                                           AND  BL.Bucket_Type = 'Determinant'
                                    LEFT OUTER JOIN dbo.CURRENCY_UNIT_CONVERSION CurConv
                                        ON CurConv.BASE_UNIT_ID = CUAD.CURRENCY_UNIT_ID
                                           AND  CurConv.CURRENCY_GROUP_ID = ad.Client_Currency_Group_Id
                                           AND  CurConv.CONVERSION_DATE = CUAD.Service_Month
                                           AND  CurConv.CONVERTED_UNIT_ID = COALESCE(
                                                                                BL.Bucket_Uom_Id, @Usd_Currency_Unit_Id)
                                           AND  BL.Bucket_Type = 'Charge'
                                    LEFT OUTER JOIN dbo.ENTITY uom
                                        ON uom.ENTITY_ID = CUAD.UOM_Type_Id
                                           AND  BL.Bucket_Type = 'Determinant'
                                WHERE
                                    CUAD.Service_Month BETWEEN @Start_Month
                                                       AND     @End_Month
                                GROUP BY
                                    CUAD.ACCOUNT_ID
                                    , CUAD.Client_Hier_ID
                                    , CUAD.Service_Month
                                    , BM.Bucket_Name
                                    , CD.Code_Value
                                    , CUAD.UOM_Type_Id
                                    , CUAD.CURRENCY_UNIT_ID
                                    , COM.Commodity_Name
                                    , dsc.Code_Value) AS CUAD
                ON CUAD.Account_Id = AD.Account_Id
                   AND  CUAD.Client_Hier_Id = AD.Client_Hier_Id
        ORDER BY
            AD.Client_Name
            , AD.Sitegroup_Name
            , AD.Account_Id;

        DROP TABLE #Account_Dtl;

    END;

GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Bucket_Value_Sel] TO [CBMSApplication]
GO
