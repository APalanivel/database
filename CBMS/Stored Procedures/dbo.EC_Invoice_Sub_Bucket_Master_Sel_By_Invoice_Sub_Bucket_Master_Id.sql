SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.EC_Invoice_Sub_Bucket_Master_Sel_By_Invoice_Sub_Bucket_Master_Id       
              
Description:              
			This sproc to get the bucket details details for a Given id.      
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
	@EC_Invoice_Sub_Bucket_Master_Id	INT
    
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

   Exec dbo.EC_Invoice_Sub_Bucket_Master_Sel_By_Invoice_Sub_Bucket_Master_Id 16
   
   Exec dbo.EC_Invoice_Sub_Bucket_Master_Sel_By_Invoice_Sub_Bucket_Master_Id  7       
	
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy               
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2015-04-22		Created For AS400.         
             
******/ 

CREATE PROCEDURE [dbo].[EC_Invoice_Sub_Bucket_Master_Sel_By_Invoice_Sub_Bucket_Master_Id]
      ( 
       @EC_Invoice_Sub_Bucket_Master_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 
            
      SELECT
            eisbm.EC_Invoice_Sub_Bucket_Master_Id
           ,eisbm.Bucket_Master_Id
           ,eisbm.Sub_Bucket_Name
           ,s.COUNTRY_ID
           ,s.STATE_ID
           ,s.STATE_NAME
      FROM
            dbo.EC_Invoice_Sub_Bucket_Master eisbm
            INNER JOIN dbo.STATE s
                  ON s.STATE_ID = eisbm.State_Id
      WHERE
            eisbm.EC_Invoice_Sub_Bucket_Master_Id = @EC_Invoice_Sub_Bucket_Master_Id
                
END
         
;
GO
GRANT EXECUTE ON  [dbo].[EC_Invoice_Sub_Bucket_Master_Sel_By_Invoice_Sub_Bucket_Master_Id] TO [CBMSApplication]
GO
