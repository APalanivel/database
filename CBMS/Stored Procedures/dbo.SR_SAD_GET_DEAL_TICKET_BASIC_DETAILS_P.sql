SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_DEAL_TICKET_BASIC_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@srDealTicketId	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--exec dbo.SR_SAD_GET_DEAL_TICKET_BASIC_DETAILS_P '-1','-1',925
--select * from SR_DEAL_TICKET_DETAILS
-- select * from sr_deal_ticket
--select * from SR_DEAL_TICKET_TRACE_LOG where SR_DEAL_TICKET_ID = 925
--select * from SR_DEAL_TICKET_DETAILS where SR_DEAL_TICKET_ID = 727
--select * from entity where entity_id >1000
--select * from SR_DEAL_TICKET_DETAILS where SR_DEAL_TICKET_ID = 724
--select * from SR_DT_ORDER_COMPLETION_INFO where SR_DEAL_TICKET_ID = 602
--select * from SR_DEAL_TICKET where SR_DEAL_TICKET_ID = 602
--select * from entity where entity_type>1000 
--select * from address
--select * from site where site_name like '%chicago%'
--select dbo.SR_SAD_FN_GET_DEALTICKET_SITE(904)



CREATE            PROCEDURE dbo.SR_SAD_GET_DEAL_TICKET_BASIC_DETAILS_P
@userId varchar(10),
@sessionId varchar(20),
@srDealTicketId int
AS
set nocount on


select 
	distinct sdt.SR_DEAL_TICKET_ID,
	e1.ENTITY_NAME as COMMODITY_NAME, 
	cry.COUNTRY_NAME COUNTRY_NAME, ui.USERNAME as USERNAME,
	cl.CLIENT_ID, cl.CLIENT_NAME, sdt.INITIATION_DATE INITIATION_DATE, sdt.FROM_DATE,
	sdt.TO_DATE, e2.ENTITY_NAME VOLUME_TERM_TYPE_NAME, e3.ENTITY_NAME as VOLUME_SOURCE_NAME, 
	sdt.PERCENTAGE_HEDGE as PERCENTAGE_HEDGE,
        e1.ENTITY_ID as COMMODITY_ID, sdci.IS_CEM_PARTICIPATION, sdci.PARTICIPATION_COMMENTS,
	sdci.VENDOR_ID, sdci.IS_AUTHORITY_GRANTED, sdci.AUTHORITY_LIMIT,
	sddp.DELIVERY_POINT_NUMBER, sddp.DELIVERY_POINT_NAME,
	sddp.PRICING_METHODOLOGY, sdoci.ORDER_PLACED_DATE, sdoci.SA_PLACING_ORDER,
	sdoci.VERBAL_CONFIRM_DATE, sdoci.SA_COMPLETING_ORDER, sdoci.BUYER_COMMENTS,
	sdoci.SELLER_COMMENTS, sdrfb.UTILITY_TYPE_ID as UTILITY_ID_FOR_BALANCING,
	sdrfb.PIPELINE_TYPE_ID as PIPELINE_ID_FOR_BALANCING,
	sdrfb.DELIVERY_POINT_TYPE_ID as DELIVERY_POINT_ID_FOR_BALANCING,
	sdrfb.CITYGATE_VALUE as CITYGATE_VALUE_FOR_BALANCING,
	sdrfb.OTHER_VALUE as OTHER_VALUE_FOR_BALANCING,
	sdrfn.UTILITY_TYPE_ID as UTILITY_ID_FOR_NORMS,
	sdrfn.PIPELINE_TYPE_ID as PIPELINE_ID_FOR_NORMS,
	sdtd.DEAL_TYPE_ID, sdtd.DEAL_TYPE_OTHER_VALUE, sdtd.NEW_CONTRACT_NUMBER,
	sdtd.DEAL_STATUS_TYPE_ID, sdtd.COUNTER_PARTY, sdtd.CONTACT,
	sdtd.OFFICE_PHONE, sdtd.CELL_PHONE, sdtd.ACTION_TYPE_ID,
	sdtd.ACTION_OTHER_VALUE, sdtd.VOLUME_UNIT_TYPE_ID,
	sdtd.COMMODITY_PRICE_POINT_TYPE_ID, sdtd.PRICING_REQUEST_TYPE_ID,
	sdtd.CURRENCY_UNIT_ID, sdtd.WITH_WHOM,
	sdttc.TRANSPORTATION_TYPE_ID, sdttc.SUPPLY_TYPE_ID,
	sdttc.INCLUSIVE_OF_FUEL_TYPE_ID, sdttc.SR_DT_DELIVERY_POINT_ID,
	sdv.IS_DATABASE, sdv.CONTRACT_END_DATE, sdv.NOTIFICATION_REQUIREMENTS, 
	sdv.CONTRACT_NUMBER, sdv.SA_NAME,
	sdt.INITIATED_BY,
	sdtai.DELIVERY_POINT as HR_DELIVERY_POINT,
	sdtai.ADDITIONAL_PROVISIONS as ADDITIONAL_PROVISIONS,
	sdtai.CLIENT_ACCOUNT_INFORMATION as CLIENT_ACCOUNT_INFORMATION,
	dbo.SR_SAD_FN_GET_DEALTICKET_SITE(sdt.SR_DEAL_TICKET_ID) as SITE_NAME,
	dbo.SR_SAD_FN_GET_DEALTICKET_ADDRESS(sdt.SR_DEAL_TICKET_ID) as ADDRESS,
	e4.ENTITY_NAME as PRICED_STATUS_NAME,
	sdttl.UPDATE_DATE as PRICED_UPDATE_DATE
 

from 
	SR_DEAL_TICKET sdt


join ENTITY e1 on e1.ENTITY_ID = sdt.COMMODITY_TYPE_ID 
join COUNTRY cry on cry.COUNTRY_ID = sdt.COUNTRY_ID 
join USER_INFO ui on ui.USER_INFO_ID = sdt.INITIATED_BY 
join CLIENT cl on cl.CLIENT_ID = sdt.CLIENT_ID 
join ENTITY e2 on e2.ENTITY_ID = sdt.VOLUME_TERM_TYPE_ID 
join ENTITY e3 on e3.ENTITY_ID = sdt.VOLUME_SOURCE_TYPE_ID

 

left join SR_DT_CEM_INFO sdci on sdci.SR_DEAL_TICKET_ID = sdt.SR_DEAL_TICKET_ID 
left join SR_DT_DELIVERY_POINT sddp on sddp.SR_DEAL_TICKET_ID = sdt.SR_DEAL_TICKET_ID 
left join SR_DT_ORDER_COMPLETION_INFO sdoci on sdoci.SR_DEAL_TICKET_ID = sdt.SR_DEAL_TICKET_ID 
left join SR_DEAL_TICKET_DETAILS sdtd on sdtd.SR_DEAL_TICKET_ID = sdt.SR_DEAL_TICKET_ID 
left join SR_DT_RESPONSIBLE_FOR_NORMS sdrfn on sdrfn.SR_DEAL_TICKET_ID = sdt.SR_DEAL_TICKET_ID 
left join SR_DT_RESPONSIBLE_FOR_BALANCING sdrfb on sdrfb.SR_DEAL_TICKET_ID = sdt.SR_DEAL_TICKET_ID
left join SR_DT_TRANSPORT_TERMS_CONDITIONS sdttc on sdttc.SR_DEAL_TICKET_ID = sdt.SR_DEAL_TICKET_ID
left join SR_DT_VERIFICATION sdv on sdv.SR_DEAL_TICKET_ID = sdt.SR_DEAL_TICKET_ID
left join SR_DT_TRANSACTION_ADDITIONAL_INFO sdtai on sdtai.SR_DEAL_TICKET_ID = sdt.SR_DEAL_TICKET_ID
left join SR_DEAL_TICKET_TRACE_LOG sdttl on sdttl.SR_DEAL_TICKET_ID = sdt.SR_DEAL_TICKET_ID
left join ENTITY e4 on e4.ENTITY_ID = sdttl.DEAL_STATUS_TYPE_ID

where
sdt.SR_DEAL_TICKET_ID = @srDealTicketId
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_DEAL_TICKET_BASIC_DETAILS_P] TO [CBMSApplication]
GO
