SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********  
NAME:  dbo.Client_SEL_By_Portfolio_Client_Id

DESCRIPTION:
	This procedure retruns the clients under the Portfolio client.

INPUT PARAMETERS:
    Name					DataType          Default     Description
------------------------------------------------------------
	@Portfolio_client_id	int

OUTPUT PARAMETERS:
      Name              DataType          Default     Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
exec dbo.Client_SEL_By_Portfolio_Client_Id 11727

Initials	Name
------------------------------------------------------------
CPE			Chaitanya Panduga Eshwar

Initials	Date		    Modification
------------------------------------------------------------
CPE			2012-03-12		Created SP

******/

CREATE PROCEDURE dbo.Client_SEL_By_Portfolio_Client_Id
      (
       @Portfolio_Client_Id INT )
AS 
BEGIN
      SELECT
            CH.CLIENT_ID
           ,CH.CLIENT_NAME
           ,@Portfolio_Client_Id AS Portfolio_Client_Id
      FROM
            core.Client_Hier CH
      WHERE
            Portfolio_Client_Id = @Portfolio_Client_Id
            AND CH.Sitegroup_Id = 0
      GROUP BY
            CH.CLIENT_ID
           ,CH.CLIENT_NAME
           ,Portfolio_Client_Id
      ORDER BY
            CH.Client_Name
END

GO
GRANT EXECUTE ON  [dbo].[Client_SEL_By_Portfolio_Client_Id] TO [CBMSApplication]
GO
