SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******************************************************************************** 
 NAME:  dbo.Cbms_Image_Ids_Sel_By_File_Name 
 
 DESCRIPTION: Select CBMS Image Ids based on file name
 
 INPUT PARAMETERS:  
 Name			Type	Default		Description  
 ------------------------------------------------------------    
@FileNames Image_Filenames READONLY 
,@Cbms_Image_Type_Id INT	

 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
 ------------------------------------------------------------  

 USAGE EXAMPLES:  
 ------------------------------------------------------------  
 BEGIN TRAN
 DECLARE @FileNames ImageFileNames
 insert into @FileNames values('AA','FGHTJ','tt')
 EXEC Cbms_Image_Ids_Sel_By_File_Name @FileNames,1

 ROLLBACK TRAN
 
 AUTHOR INITIALS:  
 Initials	Name  
 ------------------------------------------------------------  
 AL			Ajeesh L
 MODIFICATIONS   
 Initials	Date			Modification  
 ------------------------------------------------------------  	
 AL			05-27-2013		Created
    
********************************************************************************/

CREATE PROCEDURE [dbo].[Cbms_Image_Ids_Sel_By_File_Name]
      ( 
       @FileNames ImageFileNames READONLY
      ,@Cbms_Image_Type_Id INT )
AS 
BEGIN
      
      SELECT
            tvp.BillId
           ,ci.CBMS_IMAGE_ID
           ,tvp.Image_Filename
           ,tvp.Barcode
      FROM
            @FileNames tvp
            LEFT JOIN dbo.cbms_image ci
                  ON ( tvp.Image_Filename = ci.CBMS_DOC_ID
                       OR tvp.Barcode = ci.CBMS_Image_FileName )
                     AND ci.CBMS_IMAGE_TYPE_ID = @Cbms_Image_Type_Id				
END






GO
GRANT EXECUTE ON  [dbo].[Cbms_Image_Ids_Sel_By_File_Name] TO [CBMSApplication]
GO
