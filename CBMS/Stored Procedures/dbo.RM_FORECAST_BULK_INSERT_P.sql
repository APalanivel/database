SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.RM_FORECAST_BULK_INSERT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@fileName      	varchar(100)	          	
	@server        	varchar(100)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	
	EXEC dbo.RM_FORECAST_BULK_INSERT_P 'FC_29750_eff413dd6ddc41fe8369b1bd15f8ce5e.txt','chengalsettyn'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/28/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on	        
	
******/

CREATE PROCEDURE dbo.RM_FORECAST_BULK_INSERT_P
@fileName VARCHAR(100),
@server	  VARCHAR(100)
AS
BEGIN
	
	SET NOCOUNT ON ;
	
	DECLARE @bulkInsert VARCHAR(8000)
	SET @bulkInsert = 
		' BULK INSERT  RM_FORECAST_VOLUME_DETAILS '+
		' FROM '+'''\\'+ @server +'\temp\'+ @fileName +''' '+ 
		' WITH (MAXERRORS=0, FIRE_TRIGGERS, CHECK_CONSTRAINTS, FORMATFILE ='+''''+'C:\rm_forecast_fmt\rm_forecast_volume_details.fmt'+''''+')'

	EXEC(@bulkInsert)
END	
GO
GRANT EXECUTE ON  [dbo].[RM_FORECAST_BULK_INSERT_P] TO [CBMSApplication]
GO
