SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        dbo.RM_Forecast_Volume_Batch_Dtl_Sel                    
                      
Description:                      
        To load forecast volumes for on-boarded sites
                      
Input Parameters:                      
    Name							DataType        Default     Description                        
--------------------------------------------------------------------------------
	@RM_Client_Hier_Onboard_Id		INT
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	  
                    
Usage Examples:                          
--------------------------------------------------------------------------------
	
		EXEC [dbo].[RM_Forecast_Volume_Batch_Dtl_Sel] 1
		                     
 Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy   
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			2018-09-27     Global Risk Management - Created 
                     
******/
CREATE PROCEDURE [dbo].[RM_Forecast_Volume_Batch_Dtl_Sel]
    (
        @RM_Forecast_Volume_Batch_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT  TOP 5000
                dtl.RM_Forecast_Volume_Batch_Id
                , dtl.RM_Forecast_Volume_Batch_Dtl_Id
                , dtl.Client_Hier_Id
                , dtl.Forecast_Service_Month
        FROM
            Trade.RM_Forecast_Volume_Batch_Dtl dtl
            INNER JOIN Trade.RM_Forecast_Volume_Batch batch
                ON dtl.RM_Forecast_Volume_Batch_Id = batch.RM_Forecast_Volume_Batch_Id
            INNER JOIN dbo.Code c
                ON c.Code_Id = dtl.Status_Cd
        WHERE
            batch.RM_Forecast_Volume_Batch_Id = @RM_Forecast_Volume_Batch_Id
            AND c.Code_Value = 'Pending';


    END;


GO
GRANT EXECUTE ON  [dbo].[RM_Forecast_Volume_Batch_Dtl_Sel] TO [CBMSApplication]
GO
