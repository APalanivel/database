
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	 dbo.CU_Invoice_Service_Month_Changes_Sel

DESCRIPTION:
			This procedure is used to select changed records between two versions from CU_INVOICE_SERVICE_MONTH.

INPUT PARAMETERS:
	Name						DataType		Default	Description
------------------------------------------------------------
@Last_Version_Id				BIGINT			Beginnng Change tracking Version Id (From Change_Control_Threshold) 			
@Current_Version_Id				BIGINT			Current Change tracking Version Id

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
It will execute only through the replication replacement SSIS package.

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RK			Raghu Kalvapudi
	
MODIFICATIONS
	Initials	Date			Modification
------------------------------------------------------------
	RK			08/01/2013		Created
	KVk			09/17/2014		Modified to have select correct result
******/

CREATE PROCEDURE [dbo].[CU_Invoice_Service_Month_Changes_Sel]
      (
       @Last_Version_Id BIGINT
      ,@Current_Version_Id BIGINT
      ,@CU_I_Last_Version_Id BIGINT )
AS
BEGIN
      SET NOCOUNT ON;

      SELECT
            convert(CHAR(1), cuism_ct.SYS_CHANGE_OPERATION) Op_Code
           ,cuism_ct.CU_INVOICE_SERVICE_MONTH_ID
           ,NULL CU_INVOICE_ID
           ,NULL SERVICE_MONTH
           ,NULL Account_ID
           ,NULL Begin_Dt
           ,NULL End_Dt
           ,NULL Billing_Days
           ,NULL rowguid
      FROM
            changetable(CHANGES dbo.CU_INVOICE_SERVICE_MONTH, @Last_Version_Id) cuism_ct
      WHERE
            cuism_ct.SYS_Change_Version <= @Current_Version_Id
            AND convert(CHAR(1), cuism_ct.SYS_CHANGE_OPERATION) = 'D'
      UNION ALL
      SELECT
            'U' Op_Code
           ,cism.CU_INVOICE_SERVICE_MONTH_ID
           ,cism.CU_INVOICE_ID
           ,cism.SERVICE_MONTH
           ,cism.Account_ID
           ,cism.Begin_Dt
           ,cism.End_Dt
           ,cism.Billing_Days
           ,cism.rowguid
      FROM
            dbo.CU_INVOICE_SERVICE_MONTH AS cism
            JOIN ( SELECT
                        cuism.CU_INVOICE_SERVICE_MONTH_ID
                   FROM
                        changetable(CHANGES dbo.CU_INVOICE_SERVICE_MONTH, @Last_Version_Id) cuism_ct
                        INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cuism
                              ON cuism.CU_INVOICE_SERVICE_MONTH_ID = cuism_ct.CU_INVOICE_SERVICE_MONTH_ID
                        INNER JOIN dbo.CU_INVOICE ci
                              ON ci.CU_INVOICE_ID = cuism.CU_INVOICE_ID
                   WHERE
                        cuism_ct.SYS_Change_Version <= @Current_Version_Id
                        AND ci.IS_PROCESSED = 1
                        AND ci.IS_REPORTED = 1
                   UNION
					--changes from CU_Invoice
                   SELECT
                        cism2.CU_INVOICE_SERVICE_MONTH_ID
                   FROM
                        changetable(CHANGES dbo.CU_INVOICE, @CU_I_Last_Version_Id) cui_ct
                        INNER JOIN dbo.CU_INVOICE cui
                              ON cui.CU_INVOICE_ID = cui_ct.CU_INVOICE_ID
                        INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH AS cism2
                              ON cism2.CU_INVOICE_ID = cui.CU_INVOICE_ID
                   WHERE
                        cui_ct.SYS_Change_Version <= @Current_Version_Id
                        AND cui.IS_PROCESSED = 1
                        AND cui.IS_REPORTED = 1 ) AS change
                  ON change.CU_INVOICE_SERVICE_MONTH_ID = cism.CU_INVOICE_SERVICE_MONTH_ID


END
GO

GRANT EXECUTE ON  [dbo].[CU_Invoice_Service_Month_Changes_Sel] TO [ETL_Execute]
GO
