SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
              
/******                        
 NAME: dbo.RM_Scenario_Report_Ins            
                        
 DESCRIPTION:                        
			To Insert data into RM_Scenario_Report table.                        
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@Client_Id						INT
,@Scenario_Name				NVARCHAR(200)
,@Client_Hier_Id				INT
,@Is_Include_Trigger			BIT
,@Start_Service_Month			DATE
,@End_Service_Month				DATE
,@Uom_Type_Id					INT
,@Currency_Unit_Id				INT
,@Created_User_Id				INT
,@RM_Scenario_Report_Id			INT								OUTPUT              
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
--client level just pass client_id no need to pass division and site ids--
BEGIN TRAN 
declare @RM_Scenario_Report_Id int =0

EXEC dbo.RM_Scenario_Report_Ins 
      @Client_Id = 11278
     ,@Scenario_Name = 'scenario test 1'
     ,@Is_Include_Trigger = 0
     ,@Start_Service_Month = '2013-01-01'
     ,@End_Service_Month = '2014-12-31'
     ,@Uom_Type_Id = 25
     ,@Currency_Unit_Id = 3
     ,@Created_User_Id = 49
     ,@RM_Scenario_Report_Id=@RM_Scenario_Report_Id OUTPUT

SELECT @RM_Scenario_Report_Id     
     
SELECT * FROM RM_Scenario_Report WHERE Client_Id=11278 AND Scenario_Name='scenario test 1'

ROLLBACK

     


--Division level, need to pass client and division no need to pass site id--
BEGIN TRAN 
declare @RM_Scenario_Report_Id int =0

EXEC dbo.RM_Scenario_Report_Ins 
      @Client_Id = 11278
     ,@Scenario_Name = 'scenario test 1'
     ,@Division_Id  = 2012
     ,@Is_Include_Trigger = 0
     ,@Start_Service_Month = '2013-01-01'
     ,@End_Service_Month = '2014-12-31'
     ,@Uom_Type_Id = 25
     ,@Currency_Unit_Id = 3
     ,@Created_User_Id = 49
     ,@RM_Scenario_Report_Id=@RM_Scenario_Report_Id OUTPUT

SELECT @RM_Scenario_Report_Id     
     
SELECT * FROM RM_Scenario_Report WHERE Client_Id=11278 AND Scenario_Name='scenario test 1'

SELECT Client_Hier_Id FROM CORE.Client_Hier WHERE Client_Id=11278 AND Sitegroup_Id=2012 AND Site_Id=0
SELECT Client_Hier_Id FROM [dbo].[RM_Scenario_Report] WHERE RM_Scenario_Report_Id=@RM_Scenario_Report_Id

ROLLBACK   



--site level, need to pass client and site id or Client Division and site ids
BEGIN TRAN 
declare @RM_Scenario_Report_Id int =0

EXEC dbo.RM_Scenario_Report_Ins 
      @Client_Id = 11278
     ,@Scenario_Name = 'scenario test 1'
     ,@Site_Id  = 26514
     ,@Is_Include_Trigger = 0
     ,@Start_Service_Month = '2013-01-01'
     ,@End_Service_Month = '2014-12-31'
     ,@Uom_Type_Id = 25
     ,@Currency_Unit_Id = 3
     ,@Created_User_Id = 49
     ,@RM_Scenario_Report_Id=@RM_Scenario_Report_Id OUTPUT

SELECT @RM_Scenario_Report_Id     
     
SELECT * FROM RM_Scenario_Report WHERE Client_Id=11278 AND Scenario_Name='scenario test 1'

SELECT Client_Hier_Id FROM CORE.Client_Hier WHERE Client_Id=11278 AND Sitegroup_Id=2012 AND Site_Id=26514
SELECT Client_Hier_Id FROM [dbo].[RM_Scenario_Report] WHERE RM_Scenario_Report_Id=@RM_Scenario_Report_Id

ROLLBACK    
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2014-10-01       Created                
                       
******/


          
CREATE PROCEDURE [dbo].[RM_Scenario_Report_Ins]
      ( 
       @Client_Id INT
      ,@Scenario_Name NVARCHAR(200)
      ,@Site_Id INT = 0
      ,@Division_Id INT = 0
      ,@Is_Include_Trigger BIT
      ,@Start_Service_Month DATE
      ,@End_Service_Month DATE
      ,@Uom_Type_Id INT
      ,@Currency_Unit_Id INT
      ,@Created_User_Id INT
      ,@RM_Scenario_Report_Id INT OUTPUT )
AS 
BEGIN                
      SET NOCOUNT ON;     
      
      DECLARE @Client_Hier_Id INT

      SELECT
            @Client_Hier_Id = ch.Client_Hier_Id
      FROM
            Core.Client_Hier ch
      WHERE
            ch.Client_Id = @Client_Id
            AND ( ( Sitegroup_Id = @Division_Id
                    AND Site_Id = 0
                    AND @Site_Id = 0 )
                  OR ( @Site_Id > 0
                       AND @Site_Id = Site_Id ) )  
                             
      
      INSERT      INTO [dbo].[RM_Scenario_Report]
                  ( 
                   [Client_Id]
                  ,[Scenario_Name]
                  ,[Client_Hier_Id]
                  ,[Is_Include_Trigger]
                  ,[Start_Service_Month]
                  ,[End_Service_Month]
                  ,[Uom_Type_Id]
                  ,[Currency_Unit_Id]
                  ,[Created_User_Id]
                  ,[Created_Ts]
                  ,[Updated_User_Id]
                  ,[Last_Change_Ts] )
      VALUES
                  ( 
                   @Client_Id
                  ,@Scenario_Name
                  ,@Client_Hier_Id
                  ,@Is_Include_Trigger
                  ,@Start_Service_Month
                  ,@End_Service_Month
                  ,@Uom_Type_Id
                  ,@Currency_Unit_Id
                  ,@Created_User_Id
                  ,GETDATE()
                  ,@Created_User_Id
                  ,GETDATE() )        
                
      SET @RM_Scenario_Report_Id = SCOPE_IDENTITY()
                                                                   
                                 
                       
END 

;
GO
GRANT EXECUTE ON  [dbo].[RM_Scenario_Report_Ins] TO [CBMSApplication]
GO
