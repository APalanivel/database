SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********  
NAME: dbo.TOU_Schedule_Term_Peak_Dtl_Merge

DESCRIPTION:     

	Update/Delete Peak details of a term
	
INPUT PARAMETERS:    
Name									DataType		Default Description    
------------------------------------------------------------------------------    
@Time_Of_Use_Schedule_Term_Peak_Id		INT
@Peak_Name								NVARCHAR(200)
@Season_Id								INT				NULL
@Start_Time								TIME			NULL
@End_Time								TIME			NULL
@Days_Of_Week							VARCHAR(200)
@Is_Holiday_Active						BIT



OUTPUT PARAMETERS:    
Name   DataType  Default Description    
------------------------------------------------------------------------------

USAGE EXAMPLES:    
------------------------------------------------------------------------------

exec dbo.TOU_Schedule_Term_Peak_Dtl_Merge 

AUTHOR INITIALS:    
Initials	Name    
------------------------------------------------------------    
CPE			Chaitanya Panduga Eshwar
 
MODIFICATIONS     
Initials	Date		Modification    
------------------------------------------------------------    
CPE			2012-07-10	Created     
    
******/
CREATE PROC dbo.TOU_Schedule_Term_Peak_Dtl_Merge
( 
 @Time_Of_Use_Schedule_Term_Id INT
,@Time_Of_Use_Schedule_Term_Peak_Id INT = NULL
,@Peak_Name NVARCHAR(200)
,@Season_Name VARCHAR(200) = NULL
,@Start_Time TIME
,@End_Time TIME
,@Old_Start_Time TIME = NULL
,@Old_End_Time TIME = NULL
,@Days_Of_Week VARCHAR(200)
,@Is_Holiday_Active BIT )
AS 
BEGIN

      SET NOCOUNT ON
      DECLARE @Days_Of_Week_Split TABLE ( Day_Cd INT )
      DECLARE @Season_Id INT

	  -- Get the Season_Id for the passed in Season_Name under the term
      SELECT
            @Season_Id = SEA.SEASON_ID
      FROM
            dbo.SEASON SEA
            JOIN dbo.ENTITY Ent
                  ON SEA.SEASON_PARENT_TYPE_ID = Ent.ENTITY_ID
      WHERE
            SEA.SEASON_NAME = @Season_Name
            AND SEA.SEASON_PARENT_ID = @Time_Of_Use_Schedule_Term_Id
            AND Ent.ENTITY_NAME = 'Time Of Use Schedule Term'
            AND Ent.ENTITY_TYPE = 120

      BEGIN TRY
            BEGIN TRAN

            INSERT
                  @Days_Of_Week_Split
                  SELECT
                        Segments
                  FROM
                        ufn_Split(@Days_Of_Week, ',')                   
            
			-- If @Time_Of_Use_Schedule_Term_Peak_Id IS NOT NULL, this is an update
            UPDATE
                  dbo.Time_Of_Use_Schedule_Term_Peak
            SET   
                  Peak_Name = @Peak_Name
                 ,SEASON_ID = @Season_Id
            WHERE
                  Time_Of_Use_Schedule_Term_Peak_Id = @Time_Of_Use_Schedule_Term_Peak_Id
                  
			-- If @Time_Of_Use_Schedule_Term_Peak_Id IS NULL, this is an insert
            INSERT
                  dbo.Time_Of_Use_Schedule_Term_Peak
                  ( 
                   Time_Of_Use_Schedule_Term_Id
                  ,Peak_Name
                  ,SEASON_ID )
                  SELECT
                        @Time_Of_Use_Schedule_Term_Id
                       ,@Peak_Name
                       ,@Season_Id
                  WHERE
                        @Time_Of_Use_Schedule_Term_Peak_Id IS NULL
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Time_Of_Use_Schedule_Term_Peak
                                         WHERE
                                          Peak_Name = @Peak_Name
                                          AND Time_Of_Use_Schedule_Term_Id = @Time_Of_Use_Schedule_Term_Id
                                          AND ( ISNULL(@Season_Id, 0) = ISNULL(SEASON_ID, 0) ) )

            SELECT
                  @Time_Of_Use_Schedule_Term_Peak_Id = Time_Of_Use_Schedule_Term_Peak_Id
            FROM
                  dbo.Time_Of_Use_Schedule_Term_Peak
            WHERE
                  Peak_Name = @Peak_Name
                  AND Time_Of_Use_Schedule_Term_Id = @Time_Of_Use_Schedule_Term_Id
                  AND ( ISNULL(@Season_Id, 0) = ISNULL(SEASON_ID, 0) )
                  AND @Time_Of_Use_Schedule_Term_Peak_Id IS NULL
			
            MERGE INTO dbo.Time_of_Use_Schedule_Term_Peak_Dtl tgt 
            USING
				( SELECT
					  Day_Cd
				  FROM
					  @Days_Of_Week_Split) src
			   ON tgt.Time_Of_Use_Schedule_Term_Peak_Id = @Time_Of_Use_Schedule_Term_Peak_Id
                  AND tgt.Start_Time = @Start_Time
                  AND tgt.End_Time = @End_Time
                  AND tgt.Day_Of_Week_Cd = src.Day_Cd 
            WHEN NOT MATCHED THEN
            INSERT ( Time_Of_Use_Schedule_Term_Peak_Id, Start_Time, End_Time, Day_Of_Week_Cd )
            VALUES
                  (
                   @Time_Of_Use_Schedule_Term_Peak_Id
                  ,@Start_Time
                  ,@End_Time
                  ,src.Day_Cd ) 
            WHEN NOT MATCHED BY SOURCE 
				 AND Time_Of_Use_Schedule_Term_Peak_Id = @Time_Of_Use_Schedule_Term_Peak_Id
				 AND Start_Time = @Old_Start_Time 
				 AND End_Time = @Old_End_Time
				 AND Day_Of_Week_Cd IS NOT NULL 
			THEN DELETE ;

            INSERT
                  dbo.Time_of_Use_Schedule_Term_Peak_Dtl
                  ( 
                   Time_Of_Use_Schedule_Term_Peak_Id
                  ,Start_Time
                  ,End_Time
                  ,Is_Holiday_Active )
                  SELECT
                        @Time_Of_Use_Schedule_Term_Peak_Id
                       ,@Start_Time
                       ,@End_Time
                       ,@Is_Holiday_Active
                  WHERE
                        @Is_Holiday_Active = 1
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Time_of_Use_Schedule_Term_Peak_Dtl
                                         WHERE
                                          Is_Holiday_Active = 1
                                          AND Time_Of_Use_Schedule_Term_Peak_Id = @Time_Of_Use_Schedule_Term_Peak_Id
                                          AND Start_Time = @Start_Time
                                          AND End_Time = @End_Time )
			
            DELETE
                  TOUSTPD
            FROM
                  dbo.Time_of_Use_Schedule_Term_Peak_Dtl TOUSTPD
            WHERE
                  Is_Holiday_Active = 1
                  AND ( Start_Time = @Old_Start_Time
                        AND End_Time = @Old_End_Time
                        AND ( @Old_Start_Time <> @Start_Time
                              OR @Old_End_Time <> @End_Time
                              OR @Is_Holiday_Active = 0 ) )
                  AND Time_Of_Use_Schedule_Term_Peak_Id = @Time_Of_Use_Schedule_Term_Peak_Id
                  
			
            COMMIT TRAN	
      END TRY

      BEGIN CATCH
            IF @@TRANCOUNT > 0 
                  ROLLBACK TRAN
            EXEC usp_RethrowError
      END CATCH
END
;
GO
GRANT EXECUTE ON  [dbo].[TOU_Schedule_Term_Peak_Dtl_Merge] TO [CBMSApplication]
GO
