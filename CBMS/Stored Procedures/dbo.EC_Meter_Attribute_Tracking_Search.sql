SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                                                                          
NAME: dbo.EC_Meter_Attribute_Tracking_Search                                                                                          
                      
DESCRIPTION:                                                                                          
       This sproc is to get the Meter and attribute tracking details for the given search criteria                                   
                                  
    --Details on the working                               
    Case 1                              
   Attribute name is not selected in Step 1 & Attribute name is selected in Step 2                              
   SP returns the Meter having meter attributes and meter which do not have meter attributes                              
                                  
    Case 2                              
   Attribute name is  selected in Step 1 & Attribute name is selected in Step 2                              
   SP returns meter which do not have meter attributes                              
                                  
    Case 3                              
   Attribute name is selected along with its value in Step 1 & Attribute name is selected in Step 2                              
   SP returns the Meter having meter attributes        
                            
                                                                                          
INPUT PARAMETERS:                                                                                          
 Name      DataType  Default   Description                                                                                          
------------------------------------------------------------------------------------                                                                                          
 @Country_Id    INT    NULL                                                  
 @State_Id     INT    NULL                                                  
 @Commodity_Id    INT    NULL                                                  
 @zipcode     VARCHAR(20)  NULL                                                  
 @EC_Meter_Attribute_Id  INT    NULL                                                  
 @EC_Meter_Attribute_Value  NVARCHAR(255)   NULL                                                  
                                                   
OUTPUT PARAMETERS:                                                                                          
 Name       DataType  Default   Description                                                                                          
------------------------------------------------------------------------------------                                                                                          
                                                                                          
USAGE EXAMPLES:                                                                                          
------------------------------------------------------------------------------------                                                                                          
                                                                                   
EXEC dbo.EC_Meter_Attribute_Tracking_Search                                               
      @Country_Id = 27                                              
     ,@State_Id = 124                                              
     ,@Commodity_Id = 290                                              
     ,@EC_Meter_Attribute_Id_2 = 46                                              
     ,@EC_Meter_Attribute_Id_1 = 46                                              
                                                       
EXEC dbo.EC_Meter_Attribute_Tracking_Search                                               
      @Country_Id = 27                                              
  ,@State_Id = 124                                              
     ,@Commodity_Id = 290                                       
     ,@EC_Meter_Attribute_Id_2 = 7                           
                                                       
EXEC dbo.EC_Meter_Attribute_Tracking_Search                                               
      @Country_Id = 27                            
     ,@State_Id = 124                                              
     ,@Commodity_Id = 290                                              
     ,@EC_Meter_Attribute_Id_2 = 38                                              
   ,@Start_Index = 1                                              
     ,@End_Index = 100                                              
   ,@Total_Count = 0                                                
                                                   
EXEC dbo.EC_Meter_Attribute_Tracking_Search                                            
      @Country_Id = 27                                              
     ,@State_Id = 124                                              
     ,@Commodity_Id = 290                                              
     ,@EC_Meter_Attribute_Id_2 = 46                                      
     ,@EC_Meter_Attribute_Id_1 = 46                                              
    ,@Vendor_Id = 1988                                              
                                                                         
         EXEC dbo.EC_Meter_Attribute_Tracking_Search                                              
    @Country_Id = 4                                              
    , @State_Id = 5                                              
    , @Commodity_Id = 290                                              
    , @EC_Meter_Attribute_Id_2 = 120                                              
    , @EC_Meter_Attribute_Id_1 = 155                                              
    , @Total_Count = 0                                              
    , @Start_Index = 1                                              
    , @End_Index = 100;                                              
                                              
                                              
 EXEC dbo.EC_Meter_Attribute_Tracking_Search                                              
    @Country_Id = 4                                              
    , @State_Id = 5                                              
    , @Commodity_Id = 290                                              
    , @EC_Meter_Attribute_Id_1 = 155                                              
    , @EC_Meter_Attribute_Value = 'PGE'                                              
    , @EC_Meter_Attribute_Id_2 = 120                                              
    , @Start_Index = 1                                              
    , @End_Index = 25;                                              
                      
AUTHOR INITIALS:                                                                   
 Initials  Name                                                                                          
------------------------------------------------------------------------------------                                                                                          
 RKV            Ravi Kumar Vegesna                                                  
 NR    Narayana Reddy                                              
 SP             Srinivas Patchava                    
 SLP   Sri Lakshmi Pallikonda                                          
                                                                                           
MODIFICATIONS                                                                                          
                                                                                          
Initials Date  Modification                                                                                          
------------------------------------------------------------------------------------                                                                
 RKV  2015-05-19  Created For As400.                                              
 RKV  2016-09-13  MAINT-4274,Added two Columns Attribute_Searched,Current_Value to the result set.                                                   
 NR   2017-04-27  Added  @Site_Status ,@Vendor_Id as optional parameter.                                              
 SP   2019-09-03  MAINT-9209 Modified the script Timeout Error on Bulk Meter Attribute                                               
 SLP  2020-01-15  Added new parameters Vendor_Type_Cd,Meter_Attribute_Type_Cd ,Tarrif_Rate,Is_Primary                                            
******/
CREATE PROCEDURE [dbo].[EC_Meter_Attribute_Tracking_Search]
     (
         @Country_Id INT
         , @State_Id INT
         , @Commodity_Id INT
         , @Client_Id INT = NULL
         , @Sitegroup_Id INT = NULL
         , @Site_Client_Hier_Id INT = NULL
         , @zipcode VARCHAR(20) = NULL
         , @EC_Meter_Attribute_Id_1 INT = NULL
         , @EC_Meter_Attribute_Value NVARCHAR(255) = NULL
         , @EC_Meter_Attribute_Id_2 INT
         , @Start_Index INT = 1
         , @End_Index INT = 2147483647
         , @Total_Count INT = 0
         , @Site_Status BIT = NULL
         , @Vendor_Id INT = NULL
         , @Contract_Number VARCHAR(150) = NULL
         , @Vendor_Type_Cd INT = NULL
         , @Meter_Attribute_Type_Cd INT = NULL
         , @Tarrif_Rate INT = NULL
         , @Is_Primary BIT = NULL
     )
AS
    BEGIN
        SET NOCOUNT ON;


        SET @Contract_Number = '%' + @Contract_Number + '%';

        DECLARE
            @EC_Meter_Attribute_Name NVARCHAR(400)
            , @Attribute_Type VARCHAR(25)
            , @sitegroup_Name VARCHAR(200)
            , @Attribute_Searched NVARCHAR(200);


        DECLARE @Vendor_Type_Value VARCHAR(25);
        DECLARE @Meter_Attribute_Type_Value VARCHAR(25);
        DECLARE @Vendor_Type_Update VARCHAR(25);

        SELECT
            @Vendor_Type_Value = Code_Value
        FROM
            dbo.Code
        WHERE
            Code_Id = @Vendor_Type_Cd;

        SELECT
            @Meter_Attribute_Type_Value = Code_Value
        FROM
            dbo.Code
        WHERE
            Code_Id = @Meter_Attribute_Type_Cd;

        SELECT
            @Vendor_Type_Value = c.Code_Value
        FROM
            dbo.EC_Meter_Attribute AS ema
            JOIN dbo.Code c
                ON c.Code_Id = ema.Vendor_Type_Cd
        WHERE
            (   @EC_Meter_Attribute_Id_1 IS NULL
                OR  ema.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_1)
            AND @Vendor_Type_Value IS NULL;
        SELECT
            cha.Meter_State_Id
            , cha.Meter_State_Name
            , ch.Client_Id
            , ch.Client_Name
            , ISNULL(@Sitegroup_Id, ch.Sitegroup_Id) Sitegroup_id
            , ISNULL(@sitegroup_Name, ch.Sitegroup_Name) Sitegroup_Name
            , ch.Client_Hier_Id
            , ch.Site_Id
            , ch.Site_name
            , cha.Meter_Id
            , cha.Meter_Number
            , cha.Display_Account_Number Account_Number
            , cha.Account_Vendor_Id
            , cha.Account_Vendor_Name
            , cha.Rate_Name
            , CASE WHEN acpm.Meter_Id IS NOT NULL THEN 1
                  ELSE 0
              END AS Is_Primary
        INTO
            #Attribute_Tracking
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN Core.Client_Hier ch
                ON cha.Client_Hier_Id = ch.Client_Hier_Id
            LEFT JOIN Budget.Account_Commodity_Primary_Meter acpm
                ON acpm.Account_Id = cha.Account_Id
                   AND  acpm.Commodity_Id = cha.Commodity_Id
                   AND  cha.Meter_Id = acpm.Meter_Id
        WHERE
            cha.Meter_Country_Id = @Country_Id
            AND cha.Meter_State_Id = @State_Id
            AND cha.Commodity_Id = @Commodity_Id
            AND (   @zipcode IS NULL
                    OR  cha.Meter_ZipCode = @zipcode)
            AND (   @Client_Id IS NULL
                    OR  ch.Client_Id = @Client_Id)
            AND (   @Site_Client_Hier_Id IS NULL
                    OR  ch.Client_Hier_Id = @Site_Client_Hier_Id)
            AND (   @Tarrif_Rate IS NULL
                    OR  cha.Rate_Id = @Tarrif_Rate)
            AND cha.Account_Type = 'Utility'
            AND (   @Is_Primary IS NULL
                    OR  (   @Is_Primary = 1
                            AND acpm.Meter_Id IS NOT NULL)
                    OR  (   @Is_Primary = 0
                            AND acpm.Meter_Id IS NULL))
            AND EXISTS (   SELECT
                                1
                           FROM
                                dbo.Sitegroup_Site sgs
                                INNER JOIN dbo.Sitegroup sg
                                    ON sgs.Sitegroup_id = sg.Sitegroup_Id
                           WHERE
                                ch.Site_Id = sgs.Site_id
                                AND (   @Sitegroup_Id IS NULL
                                        OR  sgs.Sitegroup_id = @Sitegroup_Id))
            AND (   @Site_Status IS NULL
                    OR  ch.Site_Not_Managed = @Site_Status)
            AND (   @Vendor_Id IS NULL
                    OR  cha.Account_Vendor_Id = @Vendor_Id)
            AND (   @Contract_Number IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        Core.Client_Hier_Account sup_cha
                                        INNER JOIN dbo.CONTRACT c
                                            ON c.CONTRACT_ID = sup_cha.Supplier_Contract_ID
                                   WHERE
                                        sup_cha.Client_Hier_Id = cha.Client_Hier_Id
                                        AND sup_cha.Meter_Id = cha.Meter_Id
                                        AND sup_cha.Account_Type = 'Supplier'
                                        AND c.ED_CONTRACT_NUMBER LIKE @Contract_Number))
        GROUP BY
            cha.Meter_State_Id
            , cha.Meter_State_Name
            , ch.Client_Id
            , ch.Client_Name
            , ISNULL(@Sitegroup_Id, ch.Sitegroup_Id)
            , ISNULL(@sitegroup_Name, ch.Sitegroup_Name)
            , ch.Client_Hier_Id
            , ch.Site_Id
            , ch.Site_name
            , cha.Meter_Id
            , cha.Meter_Number
            , cha.Display_Account_Number
            , cha.Account_Vendor_Id
            , cha.Account_Vendor_Name
            , cha.Rate_Name
            , CASE WHEN acpm.Meter_Id IS NOT NULL THEN 1
                  ELSE 0
              END;

        CREATE TABLE #EC_Meter_Attribute_Tracking_Search
             (
                 EC_Meter_Attribute_Id INT
                 , Meter_Id INT
                 , EC_Meter_Attribute_Value NVARCHAR(400)
                 , EC_Meter_Attribute_Name NVARCHAR(400)
                 , Attribute_Type VARCHAR(25)
                 , Start_Dt DATE
                 , End_Dt DATE
                 , Vendor_Type_value VARCHAR(25)
                 , Meter_Attribute_Type_Cd INT
                 , Meter_Attribute_Type_Value VARCHAR(25)
             );

        CREATE TABLE #EC_Meter_Attribute_Tracking_Value
             (
                 EC_Meter_Attribute_Id INT
                 , Meter_Id INT
                 , EC_Meter_Attribute_Value NVARCHAR(400)
                 , EC_Meter_Attribute_Name NVARCHAR(400)
                 , Attribute_Type VARCHAR(25)
                 , Start_Dt DATE
                 , End_Dt DATE
                 , Vendor_Type_Value VARCHAR(25)
                 , Meter_Attribute_Type_Value VARCHAR(25)
             );



        INSERT INTO #EC_Meter_Attribute_Tracking_Search
             (
                 EC_Meter_Attribute_Id
                 , Meter_Id
                 , EC_Meter_Attribute_Value
                 , EC_Meter_Attribute_Name
                 , Attribute_Type
                 , Start_Dt
                 , End_Dt
                 , Vendor_Type_value
                 , Meter_Attribute_Type_Cd
                 , Meter_Attribute_Type_Value
             )
        SELECT
            x.EC_Meter_Attribute_Id
            , x.Meter_Id
            , x.EC_Meter_Attribute_Value
            , x.EC_Meter_Attribute_Name
            , x.Code_Value
            , x.Start_Dt
            , x.End_Dt
            , x.Vendor_Type_Value
            , x.Meter_Attribute_Type_Cd
            , x.Meter_Attribute_Type_Value
        FROM    (   SELECT
                        emae.EC_Meter_Attribute_Id
                        , emat.Meter_Id
                        , emat.EC_Meter_Attribute_Value
                        , emae.EC_Meter_Attribute_Name
                        , cd.Code_Value
                        , emat.Start_Dt
                        , emat.End_Dt
                        , cdd.Code_Value AS Vendor_Type_Value
                        , emat.Meter_Attribute_Type_Cd AS Meter_Attribute_Type_Cd
                        , cs.Code_Value AS Meter_Attribute_Type_Value
                        , ROW_NUMBER() OVER (PARTITION BY
                                                 emat.Meter_Id
                                             -- , emat.Meter_Attribute_Type_Cd -- added 13 may              
                                             ORDER BY
                                                 ISNULL(emat.End_Dt, '2099-12-31') DESC) AS Row_Num
                    FROM
                        #Attribute_Tracking at
                        INNER JOIN dbo.EC_Meter_Attribute_Tracking emat
                            ON at.Meter_Id = emat.Meter_Id
                        INNER JOIN dbo.EC_Meter_Attribute emae
                            ON emae.EC_Meter_Attribute_Id = emat.EC_Meter_Attribute_Id
                        INNER JOIN dbo.Code cd
                            ON cd.Code_Id = emae.Attribute_Type_Cd
                        LEFT JOIN dbo.Code cdd
                            ON cdd.Code_Id = emae.Vendor_Type_Cd
                        LEFT JOIN dbo.Code cs
                            ON cs.Code_Id = emat.Meter_Attribute_Type_Cd
                    WHERE
                        (   @EC_Meter_Attribute_Id_1 IS NULL
                            OR emae.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_1)
                        AND (   @EC_Meter_Attribute_Value IS NULL
                                OR emat.EC_Meter_Attribute_Value = @EC_Meter_Attribute_Value)
                        AND (   @Vendor_Type_Cd IS NULL
                                OR emae.Vendor_Type_Cd = @Vendor_Type_Cd)
                        AND (   @Meter_Attribute_Type_Cd IS NULL
                                OR emat.Meter_Attribute_Type_Cd = @Meter_Attribute_Type_Cd)
                        AND (   @State_Id IS NULL
                                OR emae.State_Id = @State_Id)
                        AND (   @Commodity_Id IS NULL
                                OR emae.Commodity_Id = @Commodity_Id)) x
        WHERE
            x.Row_Num = 1;




        --  SELECT * FROM #EC_Meter_Attribute_Tracking_Search AS emats                               

        INSERT INTO #EC_Meter_Attribute_Tracking_Value
             (
                 EC_Meter_Attribute_Id
                 , Meter_Id
                 , EC_Meter_Attribute_Value
                 , EC_Meter_Attribute_Name
                 , Attribute_Type
                 , Start_Dt
                 , End_Dt
                 , Vendor_Type_Value
                 , Meter_Attribute_Type_Value
             )
        SELECT
            x.EC_Meter_Attribute_Id
            , x.Meter_Id
            , x.EC_Meter_Attribute_Value
            , x.EC_Meter_Attribute_Name
            , x.Code_Value
            , x.Start_Dt
            , x.End_Dt
            , x.Vendor_Type_Value
            , x.Meter_Attribute_Type_Value
        FROM    (   SELECT
                        emae.EC_Meter_Attribute_Id
                        , emat.Meter_Id
                        , emat.EC_Meter_Attribute_Value
                        , emae.EC_Meter_Attribute_Name
                        , cd.Code_Value
                        , emat.Start_Dt
                        , emat.End_Dt
                        , css.Code_Value Vendor_Type_Value
                        , cs.Code_Value Meter_Attribute_Type_Value
                        , ROW_NUMBER() OVER (PARTITION BY
                                                 emat.Meter_Id
                                                 , emat.Meter_Attribute_Type_Cd
                                             ORDER BY
                                                 emat.Start_Dt DESC
                                                 , ISNULL(emat.End_Dt, '2099-12-31') DESC) AS Row_Num
                    FROM
                        #Attribute_Tracking at
                        LEFT JOIN dbo.EC_Meter_Attribute_Tracking emat
                            ON at.Meter_Id = emat.Meter_Id
                        LEFT JOIN dbo.EC_Meter_Attribute emae
                            ON emae.EC_Meter_Attribute_Id = emat.EC_Meter_Attribute_Id
                        LEFT OUTER JOIN dbo.Code cd
                            ON cd.Code_Id = emae.Attribute_Type_Cd
                        LEFT OUTER JOIN dbo.Code cs
                            ON cs.Code_Id = emat.Meter_Attribute_Type_Cd
                        LEFT OUTER JOIN dbo.Code css
                            ON css.Code_Id = emae.Vendor_Type_Cd
                    WHERE
                        emae.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_2
                        AND (   @State_Id IS NULL
                                OR emae.State_Id = @State_Id)
                        AND (   @Commodity_Id IS NULL
                                OR emae.Commodity_Id = @Commodity_Id)
                        AND (   @Meter_Attribute_Type_Cd IS NULL
                                OR emat.Meter_Attribute_Type_Cd = @Meter_Attribute_Type_Cd)) x
        WHERE
            x.Row_Num = 1;
        --SELECT * FROM #EC_Meter_Attribute_Tracking_Value AS ematv                                  


        SELECT
            @EC_Meter_Attribute_Name = ema.EC_Meter_Attribute_Name
            , @Vendor_Type_Update = c.Code_Value
        FROM
            dbo.EC_Meter_Attribute AS ema
            LEFT JOIN dbo.Code c
                ON ema.Vendor_Type_Cd = c.Code_Id
        WHERE
            ema.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_2;


        SELECT
            @sitegroup_Name = s.Sitegroup_Name
        FROM
            dbo.Sitegroup s
        WHERE
            Sitegroup_Id = @Sitegroup_Id;

        SELECT
            @Attribute_Searched = ema.EC_Meter_Attribute_Name
        FROM
            dbo.EC_Meter_Attribute ema
        WHERE
            ema.State_Id = @State_Id
            AND ema.Commodity_Id = @Commodity_Id
            AND ema.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_1;

        SELECT
            cha.Meter_State_Id
            , cha.Meter_State_Name
            , cha.Client_Id
            , cha.Client_Name
            , cha.Sitegroup_id
            , cha.Sitegroup_Name
            , cha.Client_Hier_Id
            , cha.Site_Id
            , cha.Site_name
            , cha.Meter_Id
            , cha.Meter_Number
            , CASE WHEN @EC_Meter_Attribute_Id_1 IS NOT NULL THEN @Attribute_Searched
                  WHEN @EC_Meter_Attribute_Id_1 IS NULL THEN emat.EC_Meter_Attribute_Name
                  ELSE NULL
              END AS Attribute_Searched
            , CASE WHEN @EC_Meter_Attribute_Id_1 IS NOT NULL THEN emat.EC_Meter_Attribute_Value
                  WHEN @EC_Meter_Attribute_Id_1 IS NULL THEN emat.EC_Meter_Attribute_Value
                  ELSE NULL
              END AS Current_Value
            , cha.Account_Number
            , @EC_Meter_Attribute_Id_2 EC_Meter_Attribute_Id
            , @EC_Meter_Attribute_Name EC_Meter_Attribute_Name
            , emat2.Attribute_Type AS Meter_Attribute_Type
            , emat2.EC_Meter_Attribute_Value
            , emat.Meter_Attribute_Type_Cd
            , cd.Code_Value Meter_Attribute_Type_Value
            , emat2.Start_Dt
            , emat2.End_Dt
            , cha.Account_Vendor_Id
            , cha.Account_Vendor_Name
            , cha.Rate_Name
            , cha.Is_Primary
            , emat.Vendor_Type_value
            , emat2.Meter_Attribute_Type_Value Meter_Attribute_Type_Value_Update
            , ISNULL(emat2.Vendor_Type_Value, @Vendor_Type_Update) Vendor_Type_Value_Update
            , ROW_NUMBER() OVER (ORDER BY
                                     cha.Meter_Number
                                     , emat2.Start_Dt
                                     , emat2.End_Dt) AS Row_Num
        INTO
            #Attribute_Tracking_1
        FROM
            #Attribute_Tracking cha
            LEFT OUTER JOIN #EC_Meter_Attribute_Tracking_Search emat
                ON cha.Meter_Id = emat.Meter_Id
            LEFT OUTER JOIN #EC_Meter_Attribute_Tracking_Value emat2
                ON cha.Meter_Id = emat2.Meter_Id
            LEFT JOIN dbo.Code cd
                ON cd.Code_Id = emat.Meter_Attribute_Type_Cd
        WHERE
            (   @EC_Meter_Attribute_Id_1 IS NULL
                OR  (   @EC_Meter_Attribute_Id_1 IS NOT NULL
                        AND @EC_Meter_Attribute_Value IS NULL
                        AND NOT EXISTS (   SELECT
                                                1
                                           FROM
                                                #EC_Meter_Attribute_Tracking_Search ea
                                           WHERE
                                                ea.Meter_Id = cha.Meter_Id
                                                AND ea.Meter_Attribute_Type_Value = emat.Meter_Attribute_Type_Value
                                                AND ea.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_1))
                OR  (   @EC_Meter_Attribute_Id_1 IS NOT NULL
                        AND @EC_Meter_Attribute_Value IS NOT NULL
                        AND EXISTS (   SELECT
                                            1
                                       FROM
                                            #EC_Meter_Attribute_Tracking_Search eam
                                       WHERE
                                            eam.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_1
                                            AND eam.Meter_Id = cha.Meter_Id
                                            AND eam.EC_Meter_Attribute_Value = @EC_Meter_Attribute_Value
                                            AND emat.EC_Meter_Attribute_Id = eam.EC_Meter_Attribute_Id
                                            AND emat.Meter_Attribute_Type_Value = eam.Meter_Attribute_Type_Value)))
        GROUP BY
            cha.Meter_State_Id
            , cha.Meter_State_Name
            , cha.Client_Id
            , cha.Client_Name
            , cha.Sitegroup_id
            , cha.Sitegroup_Name
            , cha.Client_Hier_Id
            , cha.Site_Id
            , cha.Site_name
            , cha.Meter_Id
            , cha.Meter_Number
            , CASE WHEN @EC_Meter_Attribute_Id_1 IS NOT NULL THEN @Attribute_Searched
                  WHEN @EC_Meter_Attribute_Id_1 IS NULL THEN emat.EC_Meter_Attribute_Name
                  ELSE NULL
              END
            , CASE WHEN @EC_Meter_Attribute_Id_1 IS NOT NULL THEN emat.EC_Meter_Attribute_Value
                  WHEN @EC_Meter_Attribute_Id_1 IS NULL THEN emat.EC_Meter_Attribute_Value
                  ELSE NULL
              END
            , cha.Account_Number
            , emat2.Attribute_Type
            , emat2.EC_Meter_Attribute_Value
            , emat.Meter_Attribute_Type_Cd
            , emat2.Start_Dt
            , emat2.End_Dt
            , emat.EC_Meter_Attribute_Name
            , emat.EC_Meter_Attribute_Value
            , cha.Account_Vendor_Id
            , cha.Account_Vendor_Name
            , cd.Code_Value
            , cha.Rate_Name
            , cha.Is_Primary
            , emat.Vendor_Type_value
            , emat2.Meter_Attribute_Type_Value
            , emat2.Vendor_Type_Value;


        SELECT  @Total_Count = MAX(Row_Num)FROM #Attribute_Tracking_1 at;
        SELECT
            at.Meter_State_Id
            , at.Meter_State_Name
            , at.Client_Id
            , at.Client_Name
            , at.Sitegroup_id
            , at.Sitegroup_Name
            , at.Client_Hier_Id
            , at.Site_Id
            , at.Site_name
            , at.Meter_Id
            , at.Meter_Number
            , at.Account_Number
            , at.Attribute_Searched
            , at.Current_Value
            , ISNULL(@Vendor_Type_Value, at.Vendor_Type_value) Vendor_Type_Value
            , at.EC_Meter_Attribute_Id
            , at.EC_Meter_Attribute_Name
            , at.EC_Meter_Attribute_Value
            , at.Start_Dt
            , at.End_Dt
            , at.Vendor_Type_Value_Update
            , at.Meter_Attribute_Type
            , at.Meter_Attribute_Type_Value_Update
            , at.Account_Vendor_Id
            , at.Account_Vendor_Name
            , at.Rate_Name
            , at.Is_Primary
            , at.Row_Num
            , @Total_Count AS Total_Rows
        FROM
            #Attribute_Tracking_1 at
        WHERE
            at.Row_Num BETWEEN @Start_Index
                       AND     @End_Index
        GROUP BY
            at.Meter_State_Id
            , at.Meter_State_Name
            , at.Client_Id
            , at.Client_Name
            , at.Sitegroup_id
            , at.Sitegroup_Name
            , at.Client_Hier_Id
            , at.Site_Id
            , at.Site_name
            , at.Meter_Id
            , at.Meter_Number
            , at.Account_Number
            , at.Attribute_Searched
            , at.Current_Value
            , ISNULL(@Vendor_Type_Value, at.Vendor_Type_value)
            , at.EC_Meter_Attribute_Id
            , at.EC_Meter_Attribute_Name
            , at.EC_Meter_Attribute_Value
            , at.Start_Dt
            , at.End_Dt
            , at.Vendor_Type_Value_Update
            , at.Meter_Attribute_Type
            , at.Meter_Attribute_Type_Value_Update
            , at.Account_Vendor_Id
            , at.Account_Vendor_Name
            , at.Rate_Name
            , at.Is_Primary
            , at.Row_Num
        ORDER BY
            at.Row_Num;

        DROP TABLE
            #Attribute_Tracking
            , #Attribute_Tracking_1
            , #EC_Meter_Attribute_Tracking_Search
            , #EC_Meter_Attribute_Tracking_Value;
    END;
GO


GRANT EXECUTE ON  [dbo].[EC_Meter_Attribute_Tracking_Search] TO [CBMSApplication]
GO
