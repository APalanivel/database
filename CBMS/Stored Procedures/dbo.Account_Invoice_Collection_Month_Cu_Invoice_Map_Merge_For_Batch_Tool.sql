SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map_Merge_For_Batch_Tool         
                
Description:                
   This sproc is to map the Frequency With the Cu_Invoice .        
                             
 Input Parameters:                
    Name          DataType   Default   Description                  
-------------------------------------------------------------------------------------------------                  
@Cu_Invoice_Id     INT  
@Account_Invoice_Collection_Month_Id      INT      NULL  
   
 Output Parameters:                      
    Name        DataType   Default   Description                  
--------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
--------------------------------------------------------------------------------------     
  
   BEGIN Transaction  
   Exec dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map_Merge_For_Batch_Tool 114,1  
   SELECT * FROM Account_Invoice_Collection_Month_Cu_Invoice_Map where cu_invoice_Id = 114  
  
   Rollback Transaction  
    
     
Author Initials:                
    Initials  Name                
--------------------------------------------------------------------------------------                  
 RKV    Ravi Kumar Vegesna  
 Modifications:                
    Initials        Date   Modification                
--------------------------------------------------------------------------------------                  
    RKV    2017-01-25  Created For Invoice_Collection.           
               
******/   
  
CREATE PROCEDURE [dbo].[Account_Invoice_Collection_Month_Cu_Invoice_Map_Merge_For_Batch_Tool]
      ( 
       @Account_Invoice_Collection_Month_Id INT
      ,@Cu_Invoice_Id INT
      ,@Invoice_Begin_Dt DATE
      ,@Invoice_End_Dt DATE )
AS 
BEGIN  
  
      SET NOCOUNT ON;  
  
        
      INSERT      INTO dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map
                  ( 
                   Account_Invoice_Collection_Month_Id
                  ,Cu_Invoice_Id
                  ,Invoice_Begin_Dt
                  ,Invoice_End_Dt
                  ,Created_Ts )
                  SELECT
                        @Account_Invoice_Collection_Month_Id
                       ,@Cu_Invoice_Id
                       ,@Invoice_Begin_Dt
                       ,@Invoice_End_Dt
                       ,GETDATE()
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map aicm
                                     WHERE
                                          @Cu_Invoice_Id = aicm.Cu_Invoice_Id
                                          AND @Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id );  
        
        
      DELETE
            aicm
      FROM
            dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map aicm
      WHERE
            aicm.Cu_Invoice_Id = @Cu_Invoice_Id
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              Account_Invoice_Collection_Month_Cu_Invoice_Map aicm
                             WHERE
                              aicm.Account_Invoice_Collection_Month_Id = @Account_Invoice_Collection_Month_Id );  
        
     
  
END;  

;
;
GO
GRANT EXECUTE ON  [dbo].[Account_Invoice_Collection_Month_Cu_Invoice_Map_Merge_For_Batch_Tool] TO [CBMSApplication]
GO
