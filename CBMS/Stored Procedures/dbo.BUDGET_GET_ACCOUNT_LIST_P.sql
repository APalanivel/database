SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





--exec BUDGET_GET_ACCOUNT_LIST_P -1,-1,264

CREATE    PROCEDURE dbo.BUDGET_GET_ACCOUNT_LIST_P
	@userId varchar(10),
	@sessionId varchar(20),	
	@budgetId int
	AS
	begin
	set nocount on

	select 	account_id 
	from 	budget_account
	where 	budget_id = @budgetId


	end











GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_ACCOUNT_LIST_P] TO [CBMSApplication]
GO
