SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_UPDATE_PLACE_BID_SUMMIT_BOC_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@delivery                  varchar(200),
@transServiceLevelId       int,
@supplierNominationId      int,
@supplierBalancingId       int,
@summitComments            varchar(4000),
@deliveryPowerId           int,
@transServiceLevelPowerId  int,
@rfpId                     int,
@priceProductId            int,
@isSystemProduct           int,
@bidId                     int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
--select * from sr_rfp_bid where sr_rfp_bid_id=1119
--select * from sr_rfp_supplier_service
--select * from sr_rfp_supplier_price_comments
--select * from sr_rfp_account_term
--select * from sr_rfp_sop_summary
--select * from sr_rfp_sop
--select * from sr_rfp_sop_details


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_UPDATE_PLACE_BID_SUMMIT_BOC_P


@delivery varchar(200),
@transServiceLevelId int,
@supplierNominationId int,
@supplierBalancingId int,
@summitComments varchar(4000),
@deliveryPowerId int,
@transServiceLevelPowerId int,
@rfpId int,
@priceProductId int,
@isSystemProduct int,
@bidId int


AS 
	
SET NOCOUNT ON
	declare @sr_rfp_bid_req_id int
	declare @rfpBidId int
	declare @rfpSummitBidId int
	
	
BEGIN

	DECLARE UPDATE_SUMMIT_PLACE_BID_BOC CURSOR FAST_FORWARD
	FOR 
		SELECT 
			SR_RFP_BID.SR_RFP_BID_REQUIREMENTS_ID,
			SR_RFP_TERM_PRODUCT_MAP.SR_RFP_BID_ID
		FROM
			SR_RFP_SELECTED_PRODUCTS,
			SR_RFP_TERM_PRODUCT_MAP,
			SR_RFP_BID
		WHERE
			SR_RFP_SELECTED_PRODUCTS.SR_RFP_BID_REQUIREMENTS_ID=@bidId AND
			SR_RFP_TERM_PRODUCT_MAP.SR_RFP_SELECTED_PRODUCTS_ID=
			SR_RFP_SELECTED_PRODUCTS.SR_RFP_SELECTED_PRODUCTS_ID AND
			SR_RFP_TERM_PRODUCT_MAP.SR_RFP_BID_ID=SR_RFP_BID.SR_RFP_BID_ID
			



	OPEN UPDATE_SUMMIT_PLACE_BID_BOC
	FETCH NEXT FROM UPDATE_SUMMIT_PLACE_BID_BOC INTO @rfpSummitBidId,@rfpBidId

	

	WHILE (@@fetch_status <> -1)
	BEGIN
		IF (@@fetch_status <> -2)
		BEGIN
			
			
				
			UPDATE  SR_RFP_BID_REQUIREMENTS 
			
			SET 
				DELIVERY_POINT=@delivery,
				TRANSPORTATION_LEVEL_TYPE_ID=@transServiceLevelId,
				NOMINATION_TYPE_ID=@supplierNominationId,
				BALANCING_TYPE_ID=@supplierBalancingId,
				COMMENTS=@summitComments,
				DELIVERY_POINT_POWER_TYPE_ID=@deliveryPowerId ,
				SERVICE_LEVEL_POWER_TYPE_ID=@transServiceLevelPowerId
			WHERE 
				SR_RFP_BID_REQUIREMENTS_ID=@rfpSummitBidId

		END

	FETCH NEXT FROM UPDATE_SUMMIT_PLACE_BID_BOC INTO @rfpSummitBidId,@rfpBidId
	END

	CLOSE UPDATE_SUMMIT_PLACE_BID_BOC
	DEALLOCATE UPDATE_SUMMIT_PLACE_BID_BOC

end
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_UPDATE_PLACE_BID_SUMMIT_BOC_P] TO [CBMSApplication]
GO
