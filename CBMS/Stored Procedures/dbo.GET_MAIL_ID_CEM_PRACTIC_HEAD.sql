SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE dbo.GET_MAIL_ID_CEM_PRACTIC_HEAD 
@userId varchar(10),
@sessionId varchar(20)

as
	set nocount on
select email_address from user_info where user_info_id=(
	select cem_practice_head_id from rm_cem_team where rm_cem_team_id=@userId)
GO
GRANT EXECUTE ON  [dbo].[GET_MAIL_ID_CEM_PRACTIC_HEAD] TO [CBMSApplication]
GO
