
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	cbms_prod.dbo.InvoiceImage_Search

DESCRIPTION:


 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@Begin_Date    				DATETIME  			NULL      	
@End_Date      				DATETIME  			NULL      	
@account_id    				INT      	

 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      

 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            

        EXEC dbo.InvoiceImage_Search
            @Begin_Date = NULL
           ,@End_Date = NULL
           ,@Account_Id = 914502

        EXEC dbo.InvoiceImage_Search
            @Begin_Date = NULL
           ,@End_Date = NULL
           ,@Account_Id = 1	

        EXEC dbo.InvoiceImage_Search
            @Begin_Date = '2013-01-01'
           ,@End_Date = '2013-12-01'
           ,@Account_Id = 1
		 
        EXEC dbo.InvoiceImage_Search
            @Begin_Date = NULL
           ,@End_Date = NULL
           ,@Account_Id = 910988			 

        EXEC dbo.InvoiceImage_Search
            @Begin_Date = NULL
           ,@End_Date = NULL
           ,@Account_Id = 913724		
		
		SELECT  DISTINCT TOP 100 Account_Id FROM CU_INVOICE_SERVICE_MONTH ORDER BY Account_Id DESC
		
		
		    SELECT
            im.Service_Month
           ,ci.Cbms_Image_Id
           ,ci.Is_Processed
           ,ci.Is_Reported
           ,ci.Cu_Invoice_Id
           ,im.Begin_Dt
           ,im.End_Dt
           ,im.Billing_Days
           ,ci.Is_Duplicate
      FROM
            dbo.Cu_Invoice ci
            INNER JOIN dbo.Cu_Invoice_Service_Month im
                  ON im.Cu_Invoice_id = ci.Cu_Invoice_id
      WHERE
            im.Account_id =914502

 AUTHOR INITIALS:        
       
    Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
	SP					  Sandeep Pigilam
	NR					  Narayana Reddy	

 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
  SP				2014-05-23			Created,Data Operations Enhancements.
  SP				2014-07-22			Data Operations Enhancement Phase III,Added Invoice_Type,Invoice_Type_Cd in select list
  NR				2016-12-23			MAINT-4737 replaced Invoice_Type_Cd with Meter_Read_Type_Cd.

******/
CREATE PROCEDURE [dbo].[InvoiceImage_Search]
      ( 
       @Begin_Date DATETIME = NULL
      ,@End_Date DATETIME = NULL
      ,@Account_Id INT )
AS 
BEGIN

      SET NOCOUNT ON

      SELECT
            im.Service_Month
           ,ci.Cbms_Image_Id
           ,ci.Is_Processed
           ,ci.Is_Reported
           ,ci.Cu_Invoice_Id
           ,im.Begin_Dt
           ,im.End_Dt
           ,im.Billing_Days
           ,ci.Is_Duplicate
           --,it.Code_Value AS Invoice_Type
           --,ci.Invoice_Type_Cd
           ,it.Code_Value AS Meter_Read_Type
           ,ci.Meter_Read_Type_Cd
      FROM
            dbo.Cu_Invoice ci
            INNER JOIN dbo.Cu_Invoice_Service_Month im
                  ON im.Cu_Invoice_id = ci.Cu_Invoice_id
            LEFT JOIN dbo.Code it
                  ON it.Code_Id = ci.Meter_Read_Type_Cd
      WHERE
            im.Account_id = @Account_id
            AND ( @Begin_Date IS NULL
                  OR im.Service_Month >= @Begin_Date )
            AND ( @End_Date IS NULL
                  OR im.Service_Month <= @End_Date )
      GROUP BY
            im.Service_Month
           ,ci.Cbms_Image_Id
           ,ci.Is_Processed
           ,ci.Is_Reported
           ,ci.Cu_Invoice_Id
           ,im.Begin_Dt
           ,im.End_Dt
           ,im.Billing_Days
           ,ci.Is_Duplicate
           ,it.Code_Value
           ,ci.Meter_Read_Type_Cd
      ORDER BY
            CASE WHEN im.Service_Month IS NULL THEN 2
                 ELSE 1
            END       

END;



;

;
GO


GRANT EXECUTE ON  [dbo].[InvoiceImage_Search] TO [CBMSApplication]
GO
