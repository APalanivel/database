SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[cbmsCountry_GetCountry]
	( @MyAccountId int 
	, @CountryId int = null
)
AS
BEGIN

	   select country_id
		, country_name
	     from country
		where country_id = @CountryId
	 order by country_name

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCountry_GetCountry] TO [CBMSApplication]
GO
