SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
  
NAME: dbo.Delete_Site_From_Roles  
  
DESCRIPTION: It will delete site client_hier_id from all the roles in Security_Role_Client_Hier table for given Site.
  
INPUT PARAMETERS:      
 Name			DataType          Default     Description      
------------------------------------------------------------------      
 @Site_Id		INT
      
OUTPUT PARAMETERS:
 Name           DataType          Default     Description      
------------------------------------------------------------      
   
  
USAGE EXAMPLES:  
------------------------------------------------------------  

	EXEC dbo.Delete_Site_From_Roles 601
  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 PNR	  Pandarinath  
  
MODIFICATIONS  
  
 Initials	Date		Modification  
------------------------------------------------------------  
 PNR		11/23/2010	created as part proj : Security Roles Administration.
 
******/  

CREATE PROCEDURE dbo.Delete_Site_From_Roles
    (
		@Site_Id	INT
    )
AS 
BEGIN  
	
	SET NOCOUNT ON ;
	
	DECLARE
		 @Client_Id	INT
	
	SELECT 
		@Client_Id = CLIENT_ID
	FROM 
		dbo.Site
	WHERE
		Site_Id = @Site_Id
	
	DELETE
		 srch
	FROM
		dbo.Security_Role_Client_Hier srch
		JOIN Core.Client_Hier ch
			 ON ch.Client_Hier_Id = srch.Client_Hier_Id
		JOIN dbo.Code c 
			 ON ch.Hier_level_Cd = c.Code_Id
		JOIN dbo.Codeset cs 
			 ON cs.Codeset_Id = c.Codeset_Id
	WHERE
		ch.Client_Id = @Client_Id
		AND ch.Site_Id = @Site_Id
		AND cs.Codeset_Name = 'HierLevel'
		AND c.Code_Value = 'Site'
		
END
GO
GRANT EXECUTE ON  [dbo].[Delete_Site_From_Roles] TO [CBMSApplication]
GO
