SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    Trade.My_Deal_Ticket_Queue_Sel
   
    
DESCRIPTION:   
   
  
    
INPUT PARAMETERS:    
	Name          DataType       Default        Description    
-----------------------------------------------------------------------------    
	@User_Info_Id		INT
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec Trade.My_Deal_Ticket_Queue_Sel 49
	Exec Trade.My_Deal_Ticket_Queue_Sel @User_Info_Id = 112, @Sort_Key = 'Deal_Ticket_Id'
	Exec Trade.My_Deal_Ticket_Queue_Sel @User_Info_Id = 112, @Sort_Key = 'Deal_Ticket_Number', @Sort_Order = 'Asc'
	Exec Trade.My_Deal_Ticket_Queue_Sel @User_Info_Id = 112, @Sort_Key = 'client_Name', @Sort_Order = 'Asc'
	
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	RR          Raghu Reddy
    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	RR          2019-01-10	GRM Proejct.
     
    
******/

CREATE PROC [Trade].[My_Deal_Ticket_Queue_Sel]
     (
         @User_Info_Id INT
         , @Deal_Ticket_Number NVARCHAR(255) = NULL
         , @Client_Id INT = NULL
         , @Site_Client_Hier_Id VARCHAR(MAX) = NULL
         , @Hedge_Start_Dt DATE = NULL
         , @Hedge_End_Dt DATE = NULL
         , @Workflow_Status_Id VARCHAR(MAX) = NULL
         , @Currently_With_User_Info_Id VARCHAR(MAX) = NULL
         , @Start_Index INT = 1
         , @End_Index INT = 2147483647
         , @Sort_Key VARCHAR(50) = NULL
         , @Sort_Order VARCHAR(50) = NULL
     )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT  @Sort_Key = ISNULL(@Sort_Key, 'Deal_Ticket_Number');
        SELECT  @Sort_Order = ISNULL(@Sort_Order, 'Desc');

        DECLARE @Tbl_Dt_Dtls AS TABLE
              (
                  Deal_Ticket_Id INT
                  , Deal_Ticket_Number NVARCHAR(255)
                  , Client_Name VARCHAR(200)
                  , Sites VARCHAR(600)
                  , Date_Initiated VARCHAR(50)
                  , Deal_Type VARCHAR(50)
                  , Hedge_Type VARCHAR(200)
                  , Commodity_Name VARCHAR(50)
                  , Deal_Status NVARCHAR(255)
                  , Initiated_By VARCHAR(50)
                  , Currently_With VARCHAR(50)
                  , Workflow_Name NVARCHAR(255)
                  , Workflow_Description NVARCHAR(MAX)
                  , USER_INFO_ID INT
                  , Row_Num INT
                  , Hedge_Start_Dt VARCHAR(20)
                  , Hedge_End_Dt VARCHAR(20)
                  , Client_Id INT
                  , Sitegroup_Id INT
                  , Site_Id INT
              );

        DECLARE
            @Total INT
            , @All_Are_Null BIT = 0;

        SELECT
            @All_Are_Null = 1
        WHERE
            @Deal_Ticket_Number IS NULL
            AND @Client_Id IS NULL
            AND @Site_Client_Hier_Id IS NULL
            AND @Hedge_Start_Dt IS NULL
            AND @Hedge_End_Dt IS NULL
            AND @Workflow_Status_Id IS NULL
            AND @Currently_With_User_Info_Id IS NULL;


        INSERT INTO @Tbl_Dt_Dtls
             (
                 Deal_Ticket_Id
                 , Deal_Ticket_Number
                 , Client_Name
                 , Sites
                 , Date_Initiated
                 , Deal_Type
                 , Hedge_Type
                 , Commodity_Name
                 , Deal_Status
                 , Initiated_By
                 , Currently_With
                 , Workflow_Name
                 , Workflow_Description
                 , USER_INFO_ID
                 , Row_Num
                 , Hedge_Start_Dt
                 , Hedge_End_Dt
                 , Client_Id
                 , Sitegroup_Id
                 , Site_Id
             )
        SELECT
            dt.Deal_Ticket_Id
            , dt.Deal_Ticket_Number
            , ch.Client_Name
            , CASE WHEN COUNT(DISTINCT ch.Site_Id) > 1 THEN 'Multiple'
                  ELSE MAX(RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')')
              END AS Sites
            , REPLACE(CONVERT(VARCHAR(15), dt.Created_Ts, 106), ' ', '-') AS Date_Initiated
            , atype.Code_Value + ' ' + ttype.Code_Value AS Deal_Type
            , ht.ENTITY_NAME AS Hedge_Type
            , c.Commodity_Name
            , CASE WHEN COUNT(DISTINCT ws.Workflow_Status_Id) > 1 THEN 'Multiple'
                  ELSE MAX(ws.Workflow_Status_Name)
              END AS Deal_Status
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Initiated_By
            , cui.FIRST_NAME + ' ' + cui.LAST_NAME AS Currently_With
            , w.Workflow_Name
            , w.Workflow_Description
            , cui.USER_INFO_ID
            , CASE WHEN @Sort_Order = 'Asc' THEN
                       ROW_NUMBER() OVER (ORDER BY
                                              CASE WHEN @Sort_Key = 'Deal_Ticket_Id' THEN dt.Deal_Ticket_Id
                                                  WHEN @Sort_Key = 'Deal_Ticket_Number' THEN dt.Deal_Ticket_Id
                                              END ASC
                                              , CASE WHEN @Sort_Key = 'Deal_Type' THEN
                                                         atype.Code_Value + ' ' + ttype.Code_Value
                                                    WHEN @Sort_Key = 'Hedge_Type' THEN ht.ENTITY_NAME
                                                    WHEN @Sort_Key = 'Commodity_Name' THEN c.Commodity_Name
                                                    WHEN @Sort_Key = 'Deal_Status' THEN
                                                        CASE WHEN COUNT(DISTINCT ws.Workflow_Status_Id) > 1 THEN
                                                                 'Multiple'
                                                            ELSE MAX(ws.Workflow_Status_Name)
                                                        END
                                                    WHEN @Sort_Key = 'Initiated_By' THEN
                                                        ui.FIRST_NAME + ' ' + ui.LAST_NAME
                                                    WHEN @Sort_Key = 'Currently_With' THEN
                                                        cui.FIRST_NAME + ' ' + cui.LAST_NAME
                                                    WHEN @Sort_Key = 'Client_Name' THEN ch.Client_Name
                                                    WHEN @Sort_Key = 'Sites' THEN
                                                        CASE WHEN COUNT(DISTINCT ch.Site_Id) > 1 THEN 'Multiple'
                                                            ELSE
                                                                MAX(RTRIM(ch.City) + ', ' + ch.State_Name + ' ('
                                                                    + ch.Site_name + ')')
                                                        END
                                                END ASC
                                              , CASE WHEN @Sort_Key = 'Date_Initiated' THEN dt.Created_Ts
                                                END ASC)
                  WHEN @Sort_Order = 'Desc' THEN
                      ROW_NUMBER() OVER (ORDER BY
                                             CASE WHEN @Sort_Key = 'Deal_Ticket_Id' THEN dt.Deal_Ticket_Id
                                                 WHEN @Sort_Key = 'Deal_Ticket_Number' THEN dt.Deal_Ticket_Id
                                             END DESC
                                             , CASE WHEN @Sort_Key = 'Client_Name' THEN ch.Client_Name
                                                   WHEN @Sort_Key = 'Sites' THEN
                                                       CASE WHEN COUNT(DISTINCT ch.Site_Id) > 1 THEN 'Multiple'
                                                           ELSE
                                                               MAX(RTRIM(ch.City) + ', ' + ch.State_Name + ' ('
                                                                   + ch.Site_name + ')')
                                                       END
                                                   WHEN @Sort_Key = 'Deal_Type' THEN
                                                       atype.Code_Value + ' ' + ttype.Code_Value
                                                   WHEN @Sort_Key = 'Hedge_Type' THEN ht.ENTITY_NAME
                                                   WHEN @Sort_Key = 'Commodity_Name' THEN c.Commodity_Name
                                                   WHEN @Sort_Key = 'Deal_Status' THEN
                                                       CASE WHEN COUNT(DISTINCT ws.Workflow_Status_Id) > 1 THEN
                                                                'Multiple'
                                                           ELSE MAX(ws.Workflow_Status_Name)
                                                       END
                                                   WHEN @Sort_Key = 'Initiated_By' THEN
                                                       ui.FIRST_NAME + ' ' + ui.LAST_NAME
                                                   WHEN @Sort_Key = 'Currently_With' THEN
                                                       cui.FIRST_NAME + ' ' + cui.LAST_NAME
                                               END DESC
                                             , CASE WHEN @Sort_Key = 'Date_Initiated' THEN dt.Created_Ts
                                               END DESC)
              END
            , SUBSTRING(DATENAME(MONTH, dt.Hedge_Start_Dt), 1, 3) + '-'
              + CAST(DATEPART(YEAR, dt.Hedge_Start_Dt) AS VARCHAR(5)) AS Start_Date
            , SUBSTRING(DATENAME(MONTH, dt.Hedge_End_Dt), 1, 3) + '-'
              + CAST(DATEPART(YEAR, dt.Hedge_End_Dt) AS VARCHAR(5)) AS End_Date
            , ch.Client_Id
            , CASE WHEN COUNT(DISTINCT ch.Sitegroup_Id) > 1 THEN -1
                  ELSE MAX(ch.Sitegroup_Id)
              END AS Sitegroup_Id
            , CASE WHEN COUNT(DISTINCT ch.Site_Id) > 1 THEN -1
                  ELSE MAX(ch.Site_Id)
              END AS Site_Id
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dtch.Deal_Ticket_Id = dt.Deal_Ticket_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Id = dt.Client_Id
                   AND  ch.Client_Hier_Id = dtch.Client_Hier_Id
            INNER JOIN dbo.Commodity c
                ON c.Commodity_Id = dt.Commodity_Id
            INNER JOIN dbo.Workflow w
                ON w.Workflow_Id = dt.Workflow_Id
            LEFT JOIN dbo.USER_INFO ui
                ON ui.USER_INFO_ID = dt.Created_User_Id
            INNER JOIN dbo.Code ttype
                ON dt.Deal_Ticket_Type_Cd = ttype.Code_Id
            INNER JOIN dbo.Code atype
                ON dt.Trade_Action_Type_Cd = atype.Code_Id
            INNER JOIN dbo.ENTITY ht
                ON dt.Hedge_Type_Cd = ht.ENTITY_ID
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
            INNER JOIN Trade.Workflow_Status_Map wsm
                ON chws.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
            INNER JOIN Trade.Workflow_Status ws
                ON wsm.Workflow_Status_Id = ws.Workflow_Status_Id
            LEFT JOIN dbo.USER_INFO cui
                ON cui.QUEUE_ID = dt.Queue_Id
        WHERE
            chws.Is_Active = 1
            AND cui.USER_INFO_ID = @User_Info_Id
            AND (   (   @All_Are_Null = 1
                        AND ws.Workflow_Status_Name NOT IN ( 'Closed', 'Order Executed', 'Expired', 'Canceled'
                                                             , 'Completed' ))
                    OR  (   @All_Are_Null = 0
                            AND (   @Deal_Ticket_Number IS NULL
                                    OR  dt.Deal_Ticket_Number = @Deal_Ticket_Number)
                            AND (   @Client_Id IS NULL
                                    OR  ch.Client_Id = @Client_Id)
                            AND (   (@Site_Client_Hier_Id IS NULL)
                                    OR  EXISTS (   SELECT
                                                        1
                                                   FROM
                                                        dbo.ufn_split(@Site_Client_Hier_Id, ',')
                                                   WHERE
                                                        CAST(Segments AS INT) = dtch.Client_Hier_Id))
                            AND (   @Hedge_Start_Dt IS NULL
                                    OR  dt.Hedge_Start_Dt = @Hedge_Start_Dt)
                            AND (   @Hedge_End_Dt IS NULL
                                    OR  dt.Hedge_End_Dt = @Hedge_End_Dt)
                            AND ((   @Workflow_Status_Id IS NULL
                                     OR EXISTS (   SELECT
                                                        1
                                                   FROM
                                                        dbo.ufn_split(@Workflow_Status_Id, ',') wsid
                                                   WHERE
                                                        CAST(wsid.Segments AS INT) = ws.Workflow_Status_Id)))
                            AND (   @Currently_With_User_Info_Id IS NULL
                                    OR  EXISTS (   SELECT
                                                        1
                                                   FROM
                                                        dbo.ufn_split(@Currently_With_User_Info_Id, ',') usid
                                                        INNER JOIN dbo.USER_INFO cui
                                                            ON CAST(usid.Segments AS INT) = cui.USER_INFO_ID
                                                   WHERE
                                                        cui.QUEUE_ID = dt.Queue_Id))))
        GROUP BY
            dt.Deal_Ticket_Id
            , dt.Deal_Ticket_Number
            , ch.Client_Name
            , dt.Created_Ts
            , atype.Code_Value + ' ' + ttype.Code_Value
            , ht.ENTITY_NAME
            , c.Commodity_Name
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME
            , w.Workflow_Name
            , w.Workflow_Description
            , cui.FIRST_NAME + ' ' + cui.LAST_NAME
            , cui.USER_INFO_ID
            , dt.Hedge_Start_Dt
            , dt.Hedge_End_Dt
            , ch.Client_Id
        ORDER BY
            dt.Deal_Ticket_Id DESC;


        SELECT  @Total = MAX(Row_Num)FROM   @Tbl_Dt_Dtls;

        SELECT
            Deal_Ticket_Id
            , Deal_Ticket_Number
            , Client_Name
            , Sites
            , Date_Initiated
            , Deal_Type
            , Hedge_Type
            , Commodity_Name
            , Deal_Status
            , Initiated_By
            , Currently_With
            , Workflow_Name
            , Workflow_Description
            , USER_INFO_ID
            , Row_Num
            , @Total AS Total
            , Hedge_Start_Dt
            , Hedge_End_Dt
            , Client_Id
            , Sitegroup_Id
            , Site_Id
        FROM
            @Tbl_Dt_Dtls
        WHERE
            Row_Num BETWEEN @Start_Index
                    AND     @End_Index
        ORDER BY
            Row_Num;
    END;









GO
GRANT EXECUTE ON  [Trade].[My_Deal_Ticket_Queue_Sel] TO [CBMSApplication]
GO
