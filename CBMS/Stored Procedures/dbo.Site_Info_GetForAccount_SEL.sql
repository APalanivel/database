SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME: dbo.Site_Info_GetForAccount_SEL
	

DESCRIPTION:
select * from account where account_type_id = 37

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@account_id    	int       	          	
	@site_id       	int       	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
		
	dbo.Site_Info_GetForAccount_SEL 4641
	Site_Info_GetForAccount_SEL 1741124 

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	NK			Nageswara Rao Kosuri
	TRK			Ramakrishna Thummala
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	12/23/2009	Removed @MyAccountId parameter and added contract releated information
	 SKA       	01/12/2010  Removed the view VwSiteName as no column selected from this
	 SKA		03/30/2010	Removed the reference of view 'vwAccountMeter' and used CTE concept							
							Removed UNION ALL Query of Supplier_Meter_map	
	 SKA		06/23/2010	Add a join with Contract table to get Contract_Id and Number							
	 SKA		06/24/2010	Added acc.account_type_id column in select clause
	 SSR		07/09/2010  Removed the logic of fetching data from SAMM table
							(( SAMM.meter_disassociation_date > Account.Supplier_Account_Begin_Dt  
								OR SAMM.meter_disassociation_date IS NULL ))
							(After contract enhancement application is populating both Meter_association_date and meter_disassociation_Date column supplier_Account_meter_map and removing the entries when ever is_history = 1)
	TRK			2020-02-17			Added the Is_Consolidated_Billing column in select List.
******/
CREATE PROCEDURE [dbo].[Site_Info_GetForAccount_SEL]
    (
        @account_id INT
        , @site_id INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        WITH CTE_AccountMeter
        AS (
               SELECT
                    ACCOUNT_ID
                    , METER_ID
                    , NULL contract_id
               FROM
                    dbo.METER met
               WHERE
                    met.ACCOUNT_ID = @account_id
               UNION
               SELECT
                    map.ACCOUNT_ID
                    , map.METER_ID
                    , map.Contract_ID
               FROM
                    dbo.SUPPLIER_ACCOUNT_METER_MAP AS map
                    JOIN dbo.ACCOUNT a
                        ON a.ACCOUNT_ID = map.ACCOUNT_ID
               WHERE
                    a.ACCOUNT_ID = @account_id
           )
        SELECT
            m.METER_ID
            , m.RATE_ID
            , r.RATE_NAME
            , r.COMMODITY_TYPE_ID
            , com.Commodity_Name commodity_type
            , u.VENDOR_ID utility_id
            , u.VENDOR_NAME utility_name
            , act.ENTITY_NAME AS account_type
            , acc.ACCOUNT_TYPE_ID
            , am.ACCOUNT_ID
            , acc.ACCOUNT_NUMBER account_number
            , m.ADDRESS_ID
            , ad.ADDRESS_LINE1
            , ad.ADDRESS_LINE2
            , ad.CITY
            , st.STATE_ID
            , st.STATE_NAME
            , m.METER_NUMBER
            , m.TAX_EXEMPT_STATUS
            , m.IS_HISTORY
            , s.SITE_ID
            , RTRIM(ad.CITY) + ', ' + st.STATE_NAME + ' (' + s.SITE_NAME + ')' site_name
            , s.NOT_MANAGED
            , sac.Supplier_Account_Begin_Dt AS Contract_Start_Date
            , sac.Supplier_Account_End_Dt AS Contract_End_Date
            , con.CONTRACT_ID
            , con.ED_CONTRACT_NUMBER
			, CASE
               WHEN acb.Account_Id IS NOT NULL THEN
                   1
               ELSE
                   0
           END AS Is_Consolidated_Billing
        FROM
            dbo.SITE AS s
            JOIN dbo.ADDRESS AS ad
                ON ad.ADDRESS_PARENT_ID = s.SITE_ID
            JOIN dbo.METER AS m
                ON m.ADDRESS_ID = ad.ADDRESS_ID
            JOIN CTE_AccountMeter am
                ON am.METER_ID = m.METER_ID
            JOIN dbo.RATE r
                ON r.RATE_ID = m.RATE_ID
            JOIN dbo.Commodity com
                ON com.Commodity_Id = r.COMMODITY_TYPE_ID
            JOIN dbo.ACCOUNT acc
                ON acc.ACCOUNT_ID = am.ACCOUNT_ID
            JOIN dbo.VENDOR u
                ON u.VENDOR_ID = acc.VENDOR_ID
            JOIN dbo.STATE st
                ON st.STATE_ID = ad.STATE_ID
            JOIN dbo.ENTITY act
                ON act.ENTITY_ID = acc.ACCOUNT_TYPE_ID
            LEFT JOIN dbo.Supplier_Account_Config sac
                ON sac.Account_Id = am.ACCOUNT_ID
                   AND  sac.Contract_Id = am.contract_id
            LEFT JOIN dbo.CONTRACT con
                ON con.CONTRACT_ID = am.contract_id
			LEFT OUTER JOIN dbo.Account_Consolidated_Billing_Vendor acb
            ON acb.Account_Id = am.Account_Id
        WHERE
            (   @site_id IS NULL
                OR  s.SITE_ID = @site_id)
            AND (   s.NOT_MANAGED = 0
                    OR  s.NOT_MANAGED IS NULL)
        GROUP BY
            m.METER_ID
            , m.RATE_ID
            , r.RATE_NAME
            , r.COMMODITY_TYPE_ID
            , com.Commodity_Name
            , u.VENDOR_ID
            , u.VENDOR_NAME
            , act.ENTITY_NAME
            , acc.ACCOUNT_TYPE_ID
            , am.ACCOUNT_ID
            , acc.ACCOUNT_NUMBER
            , m.ADDRESS_ID
            , ad.ADDRESS_LINE1
            , ad.ADDRESS_LINE2
            , ad.CITY
            , st.STATE_ID
            , st.STATE_NAME
            , m.METER_NUMBER
            , m.TAX_EXEMPT_STATUS
            , m.IS_HISTORY
            , s.SITE_ID
            , RTRIM(ad.CITY) + ', ' + st.STATE_NAME + ' (' + s.SITE_NAME + ')'
            , s.NOT_MANAGED
            , sac.Supplier_Account_Begin_Dt
            , sac.Supplier_Account_End_Dt
            , con.CONTRACT_ID
            , con.ED_CONTRACT_NUMBER
			, acb.Account_Id;
    END;


GO
GRANT EXECUTE ON  [dbo].[Site_Info_GetForAccount_SEL] TO [CBMSApplication]
GO
