SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Service_Condition_Category_Sel_By_Language_CD]
           
DESCRIPTION:             
			To get service condition category list
			
INPUT PARAMETERS:            
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  
	@Language_CD INT = NULL
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
	EXEC dbo.Sr_Service_Condition_Category_Sel_By_Language_CD 
	EXEC dbo.Sr_Service_Condition_Category_Sel_By_Language_CD 100157
	EXEC dbo.Sr_Service_Condition_Category_Sel_By_Language_CD 100158
			
		
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-02-29	Global Sourcing - Phase3 - GCS-448 Created
******/
CREATE PROCEDURE [dbo].[Sr_Service_Condition_Category_Sel_By_Language_CD]
      ( 
       @Language_CD INT = NULL )
AS 
BEGIN

      SET NOCOUNT ON;
      
      SELECT
            @Language_CD = isnull(@Language_CD, cd.Code_Id)
      FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cd.Code_Value = 'en-US'
            AND cs.Codeset_Name = 'LocalizationLanguage'
      
      SELECT
            sscc.Sr_Service_Condition_Category_Id
           ,isnull(sscclv.Category_Name_Locale_Value, sscc.Category_Name) AS Category_Name_Locale_Value
           ,sscc.Display_Seq
      FROM
            dbo.Sr_Service_Condition_Category sscc
            LEFT JOIN dbo.Sr_Service_Condition_Category_Locale_Value sscclv
                  ON sscc.Sr_Service_Condition_Category_Id = sscclv.Sr_Service_Condition_Category_Id
                     AND sscclv.Language_Cd = @Language_CD
            
                  
            
END;
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Service_Condition_Category_Sel_By_Language_CD] TO [CBMSApplication]
GO
