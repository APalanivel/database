SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO

CREATE    PROCEDURE dbo.SR_SAD_SAVE_DT_TRANS_ADDITIONAL_DETAILS_P
@srDealTicketId int,
@heat_Delivery_Point varchar(200),
@additional_Provisions varchar(200),
@client_Account_Information varchar(200)


AS
	set nocount on
IF (select count(1) from SR_DT_TRANSACTION_ADDITIONAL_INFO where SR_DEAL_TICKET_ID = @srDealTicketId) = 0

BEGIN

	insert into SR_DT_TRANSACTION_ADDITIONAL_INFO
		(SR_DEAL_TICKET_ID, DELIVERY_POINT, ADDITIONAL_PROVISIONS, CLIENT_ACCOUNT_INFORMATION)
	
	values	(@srDealTicketId, @heat_Delivery_Point, @additional_Provisions, @client_Account_Information)

END

ELSE

BEGIN

	update SR_DT_TRANSACTION_ADDITIONAL_INFO set DELIVERY_POINT=@heat_Delivery_Point,
	        ADDITIONAL_PROVISIONS=@additional_Provisions, CLIENT_ACCOUNT_INFORMATION=@client_Account_Information

	where
		SR_DEAL_TICKET_ID=@srDealTicketId

END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_SAVE_DT_TRANS_ADDITIONAL_DETAILS_P] TO [CBMSApplication]
GO
