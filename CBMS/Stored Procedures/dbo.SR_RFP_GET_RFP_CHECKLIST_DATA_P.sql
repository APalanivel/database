
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
NAME:              
     dbo.SR_RFP_GET_RFP_CHECKLIST_DATA_P              
          
 DESCRIPTION:               
             
 INPUT PARAMETERS:              
 Name                DataType  Default Description              
------------------------------------------------------------              
 @rfpId    INT,          
 @fieldName   VARCHAR(80),          
 @sortState   INT                          
            
 OUTPUT PARAMETERS:              
 Name   DataType  Default Description              
------------------------------------------------------------              
            
 USAGE EXAMPLES:              
------------------------------------------------------------              
 EXEC dbo.SR_RFP_GET_RFP_CHECKLIST_DATA_P 1474 , 'ACCOUNT',1        
 EXEC dbo.SR_RFP_GET_RFP_CHECKLIST_DATA_P 1474 , 'site',1         
 EXEC dbo.SR_RFP_GET_RFP_CHECKLIST_DATA_P 1474 , 'BrokerFee',-1        
   
 EXEC dbo.SR_RFP_GET_RFP_CHECKLIST_DATA_P 1474 , 'address',0  
 EXEC dbo.SR_RFP_GET_RFP_CHECKLIST_DATA_P 1474 , 'meter_numbers',0  
 EXEC dbo.SR_RFP_GET_RFP_CHECKLIST_DATA_P 1474 , 'Addres_List',0  
   
 EXEC dbo.SR_RFP_GET_RFP_CHECKLIST_DATA_P 1475 , 'address',-1  
 EXEC dbo.SR_RFP_GET_RFP_CHECKLIST_DATA_P 1475 , 'meter_numbers',-1  
 EXEC dbo.SR_RFP_GET_RFP_CHECKLIST_DATA_P 1475 , 'Addres_List',-1  
   
 EXEC dbo.SR_RFP_GET_RFP_CHECKLIST_DATA_P 5, 'address',-1  
 EXEC dbo.SR_RFP_GET_RFP_CHECKLIST_DATA_P 13132 , 'address',-1  
 EXEC dbo.SR_RFP_GET_RFP_CHECKLIST_DATA_P 13137 , 'address',-1  
   
 EXEC dbo.SR_RFP_GET_RFP_CHECKLIST_DATA_P 13132 , 'Meter_States',-1  
 EXEC dbo.SR_RFP_GET_RFP_CHECKLIST_DATA_P 13132 , 'Meter_Country',-1  
 EXEC dbo.SR_RFP_GET_RFP_CHECKLIST_DATA_P 13137 , 'Contract_Signed_DATE',-1  
 EXEC dbo.SR_RFP_GET_RFP_CHECKLIST_DATA_P 13132 , 'DUE_DATE',-1  
 EXEC dbo.SR_RFP_GET_RFP_CHECKLIST_DATA_P 13137 , 'MANAGER_APPROVAL_DATE',-1  
   
   
 EXEC dbo.SR_RFP_GET_RFP_CHECKLIST_DATA_P 12873,'site',1,1,25  
 EXEC dbo.SR_RFP_GET_RFP_CHECKLIST_DATA_P 12873,'site',-1,1,25  
            
 AUTHOR INITIALS:          
 Initials Name              
------------------------------------------------------------              
 SKA  Shobhit Kumar Agrawal          
 AKR        Ashok Kumar Raju   
 RR   Raghu Reddy       
            
 MODIFICATIONS               
 Initials Date  Modification              
------------------------------------------------------------              
 SKA  12/08/2009  Removed the hard coded int values and used varriables for same.          
   08/09/2101 Added Pagination        
 DMR  09/10/2010  Modified for Quoted_Identifier        
 RR  2012-10-08 MAINT-1457 Office 2007 docs not able to be uploaded in sourcing module.        
      Table variable @temp_rc_contract_for_rfp is getting filled from DBO.CBMS_IMAGE table, but table variable columns         
      rcContentType,contractContentType data size is less than the respective column sizes of DBO.CBMS_IMAGE table.         
      Modified the column sizes as per DBO.CBMS_IMAGE table.        
 AKR  2012-10-19  Modified the sproc to show Broker Fee as a part of POCO     
 RR  2014-11-10 As a part of Site Reference Number enahancement, divided the 'Address' column to 'meters' and 'address'  
      (The zip code for the site will be added to the end of the address following two spaces )  
 RR  2015-10-19 Global Sourcing - Phase2 - Below function returns all future ative contracts, modified to return supplier contracts only  
 RR  2016-02-10 Global Sourcing - Phase3 - GCS-442 - Added Meters state, country, due date, manager approval date, contract signed date  
      to select list and sorting also  
 RR  2016-07-26 GCS-1202 Added bid group name to slect list  
   
      TO DO:- Cursor should be replaced in the function dbo.SR_RFP_FN_GET_CONTRACT_DETAILS_FOR_CHECKLIST  
   
 RR  2016-07-26 MAINT-4236 Perfomance tuning  
      Cursor replaced in the function dbo.SR_RFP_FN_GET_CONTRACT_DETAILS_FOR_CHECKLIST on 2015-11-19 Global Sourcing - Phase2   
      Removed unused parameters  
 NR  2017-08-31 SE2017-264(SE2017-265) - Added Alternate_Account_Number  column in output List.  
******/        
CREATE PROCEDURE [dbo].[SR_RFP_GET_RFP_CHECKLIST_DATA_P]  
      (   
       @rfpId INT  
      ,@fieldName VARCHAR(80)  
      ,@sortState INT  
      ,@StartIndex INT = 1  
      ,@EndIndex INT = 2147483647 )  
AS   
BEGIN        
        
      SET NOCOUNT ON        
          
      DECLARE  
            @closedStatus INT  
           ,@fieldNameSort VARCHAR(80)        
          
      DECLARE  
            @dateFrom DATETIME  
           ,@dateTo DATETIME  
           ,@CurrentDt DATETIME          
          
      DECLARE  
            @ServiceLvl_ID1 INT  
           ,@ServiceLvl_ID2 INT          
        
      DECLARE @temp_rc_contract_for_rfp TABLE  
            (   
             rcDocId INT  
            ,rcContentType VARCHAR(200)  
            ,contractDocId INT  
            ,contractContentType VARCHAR(200)  
            ,sr_rfp_id INT  
            ,sr_rfp_account_id INT )         
          
      DECLARE @temp_rc TABLE  
            (   
             account_id INT  
            ,cbms_image_id INT  
            ,rateContentType VARCHAR(50) )   
              
              
              
      CREATE TABLE #Accounts_Bid_Status_Not_Closed  
            (   
             account_Id INT  
            ,Account_Number VARCHAR(50)  
            ,Site_Id INT  
            ,Site_Name VARCHAR(200)  
            ,Client_Name VARCHAR(200)  
            ,Account_Vendor_Name VARCHAR(200)  
            ,Account_Service_level_Cd INT  
            ,Account_Is_Broker BIT  
            ,Meter_Country_Name VARCHAR(200)  
            ,Alternate_Account_Number NVARCHAR(200) )  
              
      CREATE TABLE #Accounts_Bid_Status_Closed  
            (   
             account_Id INT  
            ,Account_Number VARCHAR(50)  
            ,Site_Id INT  
            ,Site_Name VARCHAR(200)  
            ,Client_Name VARCHAR(200)  
            ,Account_Vendor_Name VARCHAR(200)  
            ,Account_Service_level_Cd INT  
            ,Account_Is_Broker BIT  
            ,Meter_Country_Name VARCHAR(200)  
            ,Alternate_Account_Number NVARCHAR(200) )  
        
      CREATE TABLE #Sr_Rfp_Check_List  
            (   
             Client_Name VARCHAR(200)  
            ,Site_Id INT  
            ,Site_name VARCHAR(200)  
            ,Account_Number VARCHAR(50)  
            ,SR_RFP_ID INT  
            ,Account_Id INT  
            ,SR_RFP_ACCOUNT_ID INT  
            ,COMMODITY_TYPE_ID INT  
            ,VENDOR_NAME VARCHAR(200)  
            ,contracts VARCHAR(200)  
            ,IS_NOTICE_GIVEN BIT  
            ,IS_CONTRACT_REVIEWED BIT  
            ,RFP_INITIATED_DATE DATETIME  
            ,LP_SENT_TO_CLIENT_DATE DATETIME  
            ,LP_APPROVED_DATE DATETIME  
            ,cbmsImageId INT  
            ,RFP_SENT_DATE DATETIME  
            ,IS_RC_COMPLETED BIT  
            ,IS_CLIENT_CREDIT BIT  
            ,IS_SUPPLIER_CREDIT BIT  
            ,IS_SUPPLIER_DOCS BIT  
            ,IS_MANAGER_APPROVED BIT  
            ,SENT_RECO_DATE DATETIME  
            ,IS_CLIENT_APPROVED BIT  
            ,IS_CONTRACT_ADMIN_INITIATED BIT  
            ,newSupplier VARCHAR(200)  
            ,CONTRACT_START_DATE DATETIME  
            ,CONTRACT_END_DATE DATETIME  
            ,UTILITY_SWITCH_DEADLINE_DATE DATETIME  
            ,UTILITY_SWITCH_SUPPLIER_DATE DATETIME  
            ,IS_SWITCH_NOTICE_GIVEN BIT  
            ,IS_VERIFY_FLOW BIT  
            ,newContract VARCHAR(150)  
            ,SA_SAVINGS VARCHAR(100)  
            ,isNA INT  
            ,rcDocId INT  
            ,rcContentType VARCHAR(200)  
            ,iscommercialAcc INT  
            ,contractDocId INT  
            ,contractContentType VARCHAR(200)  
            ,rcRateCompImageId INT  
            ,rateContentType VARCHAR(50)  
            ,tariff_transport VARCHAR(50)  
            ,Is_Broker_Account VARCHAR(10)  
            ,SR_RFP_BID_GROUP_ID INT  
            ,Is_BID_GROUP INT  
            ,Meter_Country_Name VARCHAR(200)  
            ,GROUP_NAME VARCHAR(200)  
    ,Is_Archived_Account BIT  
            ,Alternate_Account_Number NVARCHAR(200) )  
              
               
   
      CREATE TABLE #Sr_Rfp_Check_List_Indexed  
            (   
             Client_Name VARCHAR(200)  
            ,Site_Id INT  
            ,Site_name VARCHAR(200)  
            ,Account_Number VARCHAR(50)  
            ,ADDRESS VARCHAR(MAX)  
            ,SR_RFP_ID INT  
            ,Account_Id INT  
            ,SR_RFP_ACCOUNT_ID INT  
            ,COMMODITY_TYPE_ID INT  
            ,VENDOR_NAME VARCHAR(200)  
            ,rate VARCHAR(MAX)  
            ,contracts VARCHAR(200)  
            ,IS_NOTICE_GIVEN BIT  
            ,IS_CONTRACT_REVIEWED BIT  
            ,RFP_INITIATED_DATE DATETIME  
            ,LP_SENT_TO_CLIENT_DATE DATETIME  
            ,LP_APPROVED_DATE DATETIME  
            ,cbmsImageId INT  
            ,RFP_SENT_DATE DATETIME  
            ,IS_RC_COMPLETED BIT  
            ,IS_CLIENT_CREDIT BIT  
            ,IS_SUPPLIER_CREDIT BIT  
            ,IS_SUPPLIER_DOCS BIT  
            ,IS_MANAGER_APPROVED BIT  
            ,SENT_RECO_DATE DATETIME  
            ,IS_CLIENT_APPROVED BIT  
            ,IS_CONTRACT_ADMIN_INITIATED BIT  
            ,newSupplier VARCHAR(200)  
            ,CONTRACT_START_DATE DATETIME  
            ,CONTRACT_END_DATE DATETIME  
            ,UTILITY_SWITCH_DEADLINE_DATE DATETIME  
            ,UTILITY_SWITCH_SUPPLIER_DATE DATETIME  
            ,IS_SWITCH_NOTICE_GIVEN BIT  
            ,IS_VERIFY_FLOW BIT  
            ,newContract VARCHAR(150)  
            ,SA_SAVINGS VARCHAR(100)  
            ,isNA INT  
            ,rcDocId INT  
            ,rcContentType VARCHAR(200)  
            ,iscommercialAcc INT  
            ,contractDocId INT  
            ,contractContentType VARCHAR(200)  
            ,rcRateCompImageId INT  
            ,rateContentType VARCHAR(50)  
            ,tariff_transport VARCHAR(50)  
            ,Is_Broker_Account VARCHAR(10)  
            ,Row_Num INT  
            ,Total INT  
            ,Meter_Numbers VARCHAR(MAX)  
            ,Address_List VARCHAR(MAX)  
            ,Meter_States_List VARCHAR(MAX)  
            ,Meter_Country VARCHAR(200)  
            ,DUE_DATE DATETIME  
            ,MANAGER_APPROVAL_DATE DATETIME  
            ,Contract_Signed_DATE DATETIME  
            ,GROUP_NAME VARCHAR(200)  
            ,SR_RFP_BID_GROUP_ID INT  
            ,Is_BID_GROUP INT  
            ,Is_Archived_Account BIT  
            ,Alternate_Account_Number NVARCHAR(200) )   
              
             
      SELECT  
            @ServiceLvl_ID1 = ENTITY_ID  
      FROM  
            dbo.ENTITY  
      WHERE  
            ENTITY_NAME = 'N/A'  
            AND ENTITY_DESCRIPTION = 'Utility Switch Supplier'          
         
      SELECT  
            @ServiceLvl_ID2 = ENTITY_ID  
      FROM  
            dbo.ENTITY  
      WHERE  
            ENTITY_NAME = 'C'  
            AND ENTITY_DESCRIPTION = 'Commercial'          
          
      SET @dateTo = GETDATE()          
      SET @dateFrom = DATEADD(MM, -11, @dateTo)          
      SET @CurrentDt = CONVERT(DATETIME, CONVERT(VARCHAR(10), GETDATE(), 101))          
          
      SELECT  
            @closedStatus = ENTITY_ID  
      FROM  
            dbo.ENTITY  
      WHERE  
            ENTITY_DESCRIPTION = 'BID_STATUS'  
            AND ENTITY_NAME = 'Closed'          
          
      INSERT      INTO @temp_rc  
                  (   
                   account_id  
                  ,cbms_image_id  
                  ,rateContentType )  
                  SELECT  
                        a.ACCOUNT_ID  
                       ,MAX(rcRate.CBMS_IMAGE_ID)  
                       ,rcRateImage.CONTENT_TYPE rateContentType  
                  FROM  
                        dbo.SR_RFP_ACCOUNT rfp_account  
                        LEFT JOIN dbo.ACCOUNT a  
                              ON a.ACCOUNT_ID = rfp_account.ACCOUNT_ID  
                                 AND a.SERVICE_LEVEL_TYPE_ID != 861  
                                 AND a.ACCOUNT_ID IS NOT NULL  
                        LEFT JOIN dbo.RC_RATE_COMPARISON rcRate  
                              ON rcRate.ACCOUNT_ID = a.ACCOUNT_ID  
                        LEFT JOIN dbo.cbms_image rcRateImage  
                              ON rcRateImage.CBMS_IMAGE_ID = rcRate.CBMS_IMAGE_ID  
                        JOIN dbo.SR_RFP rfp  
                              ON rfp.SR_RFP_ID = rfp_account.SR_RFP_ID  
                        JOIN dbo.RATE r  
                              ON r.RATE_ID = rcRate.RATE_ID  
                                 AND r.COMMODITY_TYPE_ID = rfp.COMMODITY_TYPE_ID  
                  WHERE  
                        rfp_account.SR_RFP_ID = @rfpId  
                        AND rfp_account.IS_DELETED = 0  
                        AND rcRateImage.DATE_IMAGED BETWEEN @dateFrom  
                                                    AND     @dateTo  
                        AND rcRateImage.DATE_IMAGED IS NOT NULL  
                  GROUP BY  
                        a.ACCOUNT_ID  
                       ,rcRateImage.CONTENT_TYPE   
            
      INSERT      INTO @temp_rc_contract_for_rfp  
                  SELECT  
                        rcContract.RC_IMAGE_ID AS rcDocId  
                       ,cbmsimage.CONTENT_TYPE AS rcContentType  
                       ,rcContract1.CONTRACT_IMAGE_ID AS contractDocId  
                       ,cbmsimage1.CONTENT_TYPE AS contractContentType  
                       ,rfpAcc.SR_RFP_ID  
                       ,rfpAcc.SR_RFP_ACCOUNT_ID  
                  FROM  
                        dbo.SR_RFP_ACCOUNT rfpAcc  
                        JOIN dbo.SR_RC_CONTRACT_DOCUMENT_ACCOUNTS_MAP accMap  
                              ON accMap.ACCOUNT_ID = rfpAcc.ACCOUNT_ID  
                                 AND accMap.SR_RFP_ID = rfpAcc.SR_RFP_ID  
                        JOIN dbo.SR_RC_CONTRACT_DOCUMENT rcContract  
                              ON rcContract.SR_RC_CONTRACT_DOCUMENT_ID = accMap.SR_RC_CONTRACT_DOCUMENT_ID  
                                 AND rcContract.UPLOADED_DATE BETWEEN @dateFrom  
                                                              AND     @dateTo  
                        LEFT JOIN dbo.cbms_image cbmsimage  
                              ON cbmsimage.CBMS_IMAGE_ID = rcContract.RC_IMAGE_ID  
                        JOIN dbo.SR_RC_CONTRACT_DOCUMENT rcContract1  
                              ON rcContract1.SR_RC_CONTRACT_DOCUMENT_ID = accMap.SR_RC_CONTRACT_DOCUMENT_ID  
                                 AND rcContract1.UPLOADED_DATE BETWEEN @dateFrom  
                                                               AND     @dateTo  
                        LEFT JOIN dbo.cbms_image cbmsimage1  
                              ON cbmsimage1.CBMS_IMAGE_ID = rcContract1.CONTRACT_IMAGE_ID  
                  WHERE  
                        accMap.SR_RFP_ID = @rfpId      
                          
      INSERT      INTO #Accounts_Bid_Status_Not_Closed  
                  (   
                   account_Id  
                  ,Account_Number  
                  ,Site_Id  
                  ,Site_Name  
                  ,Client_Name  
                  ,Account_Vendor_Name  
                  ,Account_Service_level_Cd  
                  ,Account_Is_Broker  
                  ,Meter_Country_Name  
                  ,Alternate_Account_Number )  
                  SELECT  
                        cha.Account_Id  
                       ,cha.Account_Number  
                       ,ch.Site_Id  
                       ,ch.Site_name  
                       ,ch.Client_Name  
                       ,cha.Account_Vendor_Name  
                       ,cha.Account_Service_level_Cd  
                       ,cha.Account_Is_Broker  
                       ,cha.Meter_Country_Name  
                       ,cha.Alternate_Account_Number  
                  FROM  
                        Core.Client_Hier_Account cha  
                        INNER JOIN Core.Client_Hier ch  
                              ON ch.Client_Hier_Id = cha.Client_Hier_Id  
                        INNER JOIN dbo.SR_RFP_ACCOUNT sra  
                              ON cha.Account_Id = sra.ACCOUNT_ID  
                  WHERE  
                        sra.SR_RFP_ID = @rfpId  
                        AND sra.IS_DELETED = 0  
                        AND sra.BID_STATUS_TYPE_ID != @closedStatus  
                  GROUP BY  
                        cha.Account_Id  
                       ,cha.Account_Number  
                       ,ch.Site_Id  
                       ,ch.Site_name  
                       ,ch.Client_Name  
                       ,cha.Account_Vendor_Name  
                       ,cha.Account_Service_level_Cd  
                       ,cha.Account_Is_Broker  
                       ,cha.Meter_Country_Name  
                       ,cha.Alternate_Account_Number      
                         
      INSERT      INTO #Accounts_Bid_Status_Closed  
                  (   
                   account_Id  
                  ,Account_Number  
                  ,Site_Id  
                  ,Site_Name  
                  ,Client_Name  
                  ,Account_Vendor_Name  
                  ,Account_Service_level_Cd  
                  ,Account_Is_Broker  
                  ,Meter_Country_Name  
                  ,Alternate_Account_Number )  
                  SELECT  
                        cha.Account_Id  
                       ,cha.Account_Number  
                       ,ch.Site_Id  
                       ,ch.Site_name  
                       ,ch.Client_Name  
                       ,cha.Account_Vendor_Name  
                       ,cha.Account_Service_level_Cd  
                       ,cha.Account_Is_Broker  
                       ,cha.Meter_Country_Name  
                       ,cha.Alternate_Account_Number  
                  FROM  
                        Core.Client_Hier_Account cha  
                        INNER JOIN Core.Client_Hier ch  
                              ON ch.Client_Hier_Id = cha.Client_Hier_Id  
                        INNER JOIN dbo.SR_RFP_ACCOUNT sra  
                              ON cha.Account_Id = sra.ACCOUNT_ID  
                  WHERE  
                        sra.SR_RFP_ID = @rfpId  
                        AND sra.IS_DELETED = 0  
                        AND sra.BID_STATUS_TYPE_ID = @closedStatus  
                  GROUP BY  
                        cha.Account_Id  
                       ,cha.Account_Number  
                       ,ch.Site_Id  
                       ,ch.Site_name  
                       ,ch.Client_Name  
                       ,cha.Account_Vendor_Name  
                       ,cha.Account_Service_level_Cd  
                       ,cha.Account_Is_Broker  
                       ,cha.Meter_Country_Name  
                       ,cha.Alternate_Account_Number    
      
      IF ISNULL(@sortState, 1) = 1  
            OR ( @sortState = 0 ) -- ASC          
            BEGIN          
  -- Query 1          
          
                  INSERT      INTO #Sr_Rfp_Check_List  
                              (   
                               Client_Name  
                              ,Site_Id  
                              ,Site_name  
                              ,Account_Number  
                              ,SR_RFP_ID  
                              ,Account_Id  
                              ,SR_RFP_ACCOUNT_ID  
                              ,COMMODITY_TYPE_ID  
                              ,VENDOR_NAME  
                              ,contracts  
                              ,IS_NOTICE_GIVEN  
                              ,IS_CONTRACT_REVIEWED  
                              ,RFP_INITIATED_DATE  
                              ,LP_SENT_TO_CLIENT_DATE  
                              ,LP_APPROVED_DATE  
                              ,cbmsImageId  
                              ,RFP_SENT_DATE  
                              ,IS_RC_COMPLETED  
                              ,IS_CLIENT_CREDIT  
                              ,IS_SUPPLIER_CREDIT  
                              ,IS_SUPPLIER_DOCS  
                              ,IS_MANAGER_APPROVED  
      ,SENT_RECO_DATE  
                              ,IS_CLIENT_APPROVED  
                              ,IS_CONTRACT_ADMIN_INITIATED  
                              ,newSupplier  
                              ,CONTRACT_START_DATE  
                              ,CONTRACT_END_DATE  
                              ,UTILITY_SWITCH_DEADLINE_DATE  
                              ,UTILITY_SWITCH_SUPPLIER_DATE  
                              ,IS_SWITCH_NOTICE_GIVEN  
                              ,IS_VERIFY_FLOW  
                              ,newContract  
                              ,SA_SAVINGS  
                              ,isNA  
                              ,rcDocId  
                              ,rcContentType  
                              ,iscommercialAcc  
                              ,contractDocId  
                              ,contractContentType  
                              ,rcRateCompImageId  
                              ,rateContentType  
                              ,tariff_transport  
                              ,Is_Broker_Account  
                              ,SR_RFP_BID_GROUP_ID  
                              ,Is_BID_GROUP  
                              ,Meter_Country_Name  
                              ,GROUP_NAME  
                              ,Is_Archived_Account  
                              ,Alternate_Account_Number )  
                              SELECT  
                                    chier.Client_Name  
                                   ,chier.Site_Id  
                                   ,chier.Site_Name  
                                   ,chier.Account_Number  
                                   ,rfp.SR_RFP_ID  
                                   ,chier.account_Id  
                                   ,rfp_account.SR_RFP_ACCOUNT_ID  
                                   ,rfp.COMMODITY_TYPE_ID  
                                   ,chier.Account_Vendor_Name VENDOR_NAME  
                                   ,NULL AS contracts  
                                   ,checklist.IS_NOTICE_GIVEN  
                                   ,checklist.IS_CONTRACT_REVIEWED  
                                   ,checklist.RFP_INITIATED_DATE  
                                   ,checklist.LP_SENT_TO_CLIENT_DATE  
                                   ,checklist.LP_APPROVED_DATE  
                                   ,approval.CBMS_IMAGE_ID AS cbmsImageId  
                                   ,checklist.RFP_SENT_DATE  
                                   ,checklist.IS_RC_COMPLETED  
                                   ,checklist.IS_CLIENT_CREDIT  
                                   ,checklist.IS_SUPPLIER_CREDIT  
                                   ,checklist.IS_SUPPLIER_DOCS  
                                   ,checklist.IS_MANAGER_APPROVED  
                                   ,checklist.SENT_RECO_DATE  
                                   ,checklist.IS_CLIENT_APPROVED  
                                   ,checklist.IS_CONTRACT_ADMIN_INITIATED  
                                   ,supp.VENDOR_NAME AS newSupplier  
                                   ,checklist.CONTRACT_START_DATE  
                                   ,checklist.CONTRACT_END_DATE  
                                   ,checklist.UTILITY_SWITCH_DEADLINE_DATE  
                                   ,checklist.UTILITY_SWITCH_SUPPLIER_DATE  
                                   ,checklist.IS_SWITCH_NOTICE_GIVEN  
                                   ,checklist.IS_VERIFY_FLOW  
                                   ,cont.ED_CONTRACT_NUMBER AS newContract  
                                   ,checklist.SA_SAVINGS  
                                   ,CASE WHEN utility_switch.RETURN_TO_TARIFF_TYPE_ID = @ServiceLvl_ID1 THEN 1  
                                         ELSE 0  
                                    END isNA  
                                   ,rcContract.rcDocId AS rcDocId  
                                   ,rcContract.rcContentType AS rcContentType  
                                   ,CASE WHEN chier.Account_Service_level_Cd = @ServiceLvl_ID2 THEN 1  
                                         ELSE 0  
                                    END iscommercialAcc  
                                   ,rcContract.contractDocId AS contractDocId  
                                   ,rcContract.contractContentType AS contractContentType  
                                   ,rc.cbms_image_id AS rcRateCompImageId  
                                   ,rc.rateContentType AS rateContentType  
                                   ,CASE WHEN trvw.Tariff_Transport IS NULL THEN 'Tariff'  
                                         ELSE 'Transport'  
                                    END tariff_transport  
                                   ,CASE WHEN chier.Account_Is_Broker = 1 THEN 'Y'  
                                         ELSE 'N'  
                                    END AS Is_Broker_Account  
                                   ,rfp_account.SR_RFP_BID_GROUP_ID  
                                   ,CASE WHEN rfp_account.SR_RFP_BID_GROUP_ID IS NOT NULL THEN 1  
                                         ELSE 0  
                                    END AS Is_BID_GROUP  
                                   ,chier.Meter_Country_Name  
                                   ,bidgroup.GROUP_NAME  
                                   ,0 AS Is_Archived_Account  
                                   ,chier.Alternate_Account_Number  
                              FROM  
                                    dbo.SR_RFP rfp  
                                    INNER JOIN dbo.SR_RFP_ACCOUNT rfp_account  
                                          ON rfp_account.SR_RFP_ID = rfp.SR_RFP_ID  
                                    INNER JOIN dbo.SR_RFP_CHECKLIST checklist  
                                          ON checklist.SR_RFP_ACCOUNT_ID = rfp_account.SR_RFP_ACCOUNT_ID  
                                    INNER JOIN #Accounts_Bid_Status_Not_Closed chier  
                                          ON chier.account_Id = rfp_account.ACCOUNT_ID  
                                    LEFT JOIN @temp_rc rc  
                                          ON rc.account_id = rfp_account.ACCOUNT_ID  
                                    LEFT JOIN dbo.SR_RFP_LP_CLIENT_APPROVAL approval  
                                          ON approval.SR_ACCOUNT_GROUP_ID = rfp_account.SR_RFP_ACCOUNT_ID  
                                             AND approval.IS_GROUP_LP = 0  
                                    LEFT JOIN @temp_rc_contract_for_rfp rcContract  
                                          ON rcContract.sr_rfp_id = rfp_account.SR_RFP_ID  
                                             AND rcContract.sr_rfp_account_id = rfp_account.SR_RFP_ACCOUNT_ID  
                                    LEFT JOIN dbo.SR_RFP_UTILITY_SWITCH utility_switch  
                                          ON utility_switch.SR_ACCOUNT_GROUP_ID = rfp_account.SR_RFP_ACCOUNT_ID  
                                    LEFT JOIN dbo.BUDGET_TRANSPORT_ACCOUNT_VW trvw  
                                          ON trvw.account_id = chier.account_Id  
                                             AND trvw.commodity_type_id = rfp.COMMODITY_TYPE_ID  
                                    LEFT JOIN dbo.VENDOR supp  
                                          ON checklist.NEW_SUPPLIER_ID = supp.VENDOR_ID  
                                    LEFT JOIN dbo.CONTRACT cont  
                                          ON cont.CONTRACT_ID = checklist.NEW_CONTRACT_ID  
                                    LEFT JOIN dbo.SR_RFP_BID_GROUP bidgroup  
                                          ON rfp_account.SR_RFP_BID_GROUP_ID = bidgroup.SR_RFP_BID_GROUP_ID  
                              WHERE  
                                    rfp_account.SR_RFP_ID = @rfpId  
                                    AND rfp_account.IS_DELETED = 0  
                                    AND rfp_account.BID_STATUS_TYPE_ID != @closedStatus  
                              UNION           
   -- Query 2          
                       SELECT  
                                    chier.Client_Name  
                                   ,chier.Site_Id  
                                   ,chier.Site_Name  
                                   ,chier.Account_Number  
                                   ,rfp.SR_RFP_ID  
                                   ,chier.account_Id  
                                   ,rfp_account.SR_RFP_ACCOUNT_ID  
                                   ,rfp.COMMODITY_TYPE_ID  
                                   ,chier.Account_Vendor_Name VENDOR_NAME  
                                   ,archiveChecklist.ED_CONTRACT_NUMBER + '~' + archiveChecklist.CURRENT_SUPPLIER + '|' + CONVERT(VARCHAR(10), archiveChecklist.CONTRACT_EXPIRY_DATE, 101) AS CONTRACTS  
                                   ,archiveChecklist.IS_NOTICE_GIVEN  
                                   ,checklist.IS_CONTRACT_REVIEWED  
                                   ,checklist.RFP_INITIATED_DATE  
                                   ,checklist.LP_SENT_TO_CLIENT_DATE  
                                   ,checklist.LP_APPROVED_DATE  
                                   ,approval.CBMS_IMAGE_ID AS cbmsImageId  
                                   ,checklist.RFP_SENT_DATE  
                                   ,archiveChecklist.IS_RC_COMPLETE  
                                   ,checklist.IS_CLIENT_CREDIT  
                                   ,checklist.IS_SUPPLIER_CREDIT  
                                   ,checklist.IS_SUPPLIER_DOCS  
                                   ,checklist.IS_MANAGER_APPROVED  
                                   ,checklist.SENT_RECO_DATE  
                                   ,checklist.IS_CLIENT_APPROVED  
                                   ,checklist.IS_CONTRACT_ADMIN_INITIATED  
                                   ,archiveChecklist.NEW_SUPPLIER_NAME  
                                   ,checklist.CONTRACT_START_DATE  
                                   ,checklist.CONTRACT_END_DATE  
                                   ,checklist.UTILITY_SWITCH_DEADLINE_DATE  
                                   ,checklist.UTILITY_SWITCH_SUPPLIER_DATE  
                                   ,checklist.IS_SWITCH_NOTICE_GIVEN  
                                   ,checklist.IS_VERIFY_FLOW  
                                   ,archiveChecklist.NEW_CONTRACT_NUMBER  
                                   ,checklist.SA_SAVINGS  
                                   ,CASE WHEN utility_switch.RETURN_TO_TARIFF_TYPE_ID = @ServiceLvl_ID1 THEN 1  
                                         ELSE 0  
                                    END isNA  
                                   ,rcContract.rcDocId AS rcDocId  
                                   ,rcContract.rcContentType AS rcContentType  
                                   ,CASE WHEN chier.Account_Service_level_Cd = @ServiceLvl_ID2 THEN 1  
                                         ELSE 0  
                                    END iscommercialAcc  
                                   ,rcContract.contractDocId AS contractDocId  
                                   ,rcContract.contractContentType AS contractContentType  
                                   ,rc.cbms_image_id AS rcRateCompImageId  
                                   ,rc.rateContentType AS rateContentType  
                                   ,CASE WHEN trvw.Tariff_Transport IS NULL THEN 'Tariff'  
                                         ELSE 'Transport'  
                                    END tariff_transport  
                                   ,CASE WHEN chier.Account_Is_Broker = 1 THEN 'Y'  
                                         ELSE 'N'  
                                    END AS Is_Broker_Account  
                                   ,rfp_account.SR_RFP_BID_GROUP_ID  
                                   ,CASE WHEN rfp_account.SR_RFP_BID_GROUP_ID IS NOT NULL THEN 1  
                                         ELSE 0  
                                    END AS Is_BID_GROUP  
                                   ,chier.Meter_Country_Name  
                                   ,bidgroup.GROUP_NAME  
                                   ,1 AS Is_Archived_Account  
                                   ,chier.Alternate_Account_Number  
                              FROM  
                                    dbo.SR_RFP rfp  
                                    INNER JOIN dbo.SR_RFP_ACCOUNT rfp_account  
                                          ON rfp_account.SR_RFP_ID = rfp.SR_RFP_ID  
                                    INNER JOIN dbo.SR_RFP_CHECKLIST checklist  
                                          ON checklist.SR_RFP_ACCOUNT_ID = rfp_account.SR_RFP_ACCOUNT_ID  
                                    INNER JOIN #Accounts_Bid_Status_Closed chier  
                                          ON chier.account_Id = rfp_account.ACCOUNT_ID  
                                    INNER JOIN dbo.SR_RFP_ARCHIVE_ACCOUNT rfp_archive_account  
                                          ON rfp_archive_account.SR_RFP_ACCOUNT_ID = checklist.SR_RFP_ACCOUNT_ID  
                                    INNER JOIN dbo.SR_RFP_ARCHIVE_CHECKLIST archiveChecklist  
                                          ON archiveChecklist.SR_RFP_ACCOUNT_ID = rfp_archive_account.SR_RFP_ACCOUNT_ID  
                                    LEFT JOIN @temp_rc rc  
                                          ON rc.account_id = rfp_account.ACCOUNT_ID  
                                    LEFT JOIN dbo.SR_RFP_LP_CLIENT_APPROVAL approval  
                                          ON approval.SR_ACCOUNT_GROUP_ID = rfp_account.SR_RFP_ACCOUNT_ID  
                                             AND approval.IS_GROUP_LP = 0  
                                    LEFT JOIN @temp_rc_contract_for_rfp rcContract  
                                          ON rcContract.sr_rfp_id = rfp_account.SR_RFP_ID  
                                             AND rcContract.sr_rfp_account_id = rfp_account.SR_RFP_ACCOUNT_ID  
                                    LEFT JOIN dbo.SR_RFP_UTILITY_SWITCH utility_switch  
                                          ON utility_switch.SR_ACCOUNT_GROUP_ID = rfp_account.SR_RFP_ACCOUNT_ID  
                                    LEFT JOIN dbo.BUDGET_TRANSPORT_ACCOUNT_VW trvw  
                                          ON trvw.account_id = chier.account_Id  
                                             AND trvw.commodity_type_id = rfp.COMMODITY_TYPE_ID  
                                    LEFT JOIN dbo.SR_RFP_BID_GROUP bidgroup  
                                          ON rfp_account.SR_RFP_BID_GROUP_ID = bidgroup.SR_RFP_BID_GROUP_ID  
                              WHERE  
                                    rfp_account.SR_RFP_ID = @rfpId  
                                    AND rfp_account.IS_DELETED = 0  
                                    AND rfp_account.BID_STATUS_TYPE_ID = @closedStatus   
                                      
                  INSERT      INTO #Sr_Rfp_Check_List_Indexed  
                              (   
                               Client_Name  
                              ,Site_Id  
                              ,Site_name  
                              ,Account_Number  
                              ,ADDRESS  
                              ,SR_RFP_ID  
                              ,Account_Id  
                              ,SR_RFP_ACCOUNT_ID  
                              ,COMMODITY_TYPE_ID  
                              ,VENDOR_NAME  
                              ,rate  
                              ,contracts  
                              ,IS_NOTICE_GIVEN  
                              ,IS_CONTRACT_REVIEWED  
                              ,RFP_INITIATED_DATE  
                              ,LP_SENT_TO_CLIENT_DATE  
                              ,LP_APPROVED_DATE  
                              ,cbmsImageId  
                              ,RFP_SENT_DATE  
                              ,IS_RC_COMPLETED  
                              ,IS_CLIENT_CREDIT  
                              ,IS_SUPPLIER_CREDIT  
          ,IS_SUPPLIER_DOCS  
                              ,IS_MANAGER_APPROVED  
                              ,SENT_RECO_DATE  
                              ,IS_CLIENT_APPROVED  
                              ,IS_CONTRACT_ADMIN_INITIATED  
                              ,newSupplier  
                              ,CONTRACT_START_DATE  
                              ,CONTRACT_END_DATE  
                              ,UTILITY_SWITCH_DEADLINE_DATE  
                              ,UTILITY_SWITCH_SUPPLIER_DATE  
                              ,IS_SWITCH_NOTICE_GIVEN  
                              ,IS_VERIFY_FLOW  
                              ,newContract  
                              ,SA_SAVINGS  
                              ,isNA  
                              ,rcDocId  
                              ,rcContentType  
                              ,iscommercialAcc  
                              ,contractDocId  
                              ,contractContentType  
                              ,rcRateCompImageId  
                              ,rateContentType  
                              ,tariff_transport  
                              ,Is_Broker_Account  
                              ,Row_Num  
                              ,Total  
                              ,Meter_Numbers  
                              ,Address_List  
                              ,Meter_States_List  
                              ,Meter_Country  
                              ,DUE_DATE  
                              ,MANAGER_APPROVAL_DATE  
                              ,Contract_Signed_DATE  
                              ,GROUP_NAME  
                              ,SR_RFP_BID_GROUP_ID  
                              ,Is_BID_GROUP  
                              ,Is_Archived_Account  
                              ,Alternate_Account_Number )  
                              SELECT  
                                    Client_Name  
                                   ,Site_Id  
                                   ,Site_name  
                                   ,Account_Number  
                                   ,LEFT(Address_List, LEN(Address_List) - 1) AS address  
                                   ,SR_RFP_ID  
                                   ,Account_Id  
                                   ,SR_RFP_ACCOUNT_ID  
                                   ,COMMODITY_TYPE_ID  
                                   ,VENDOR_NAME  
                                   ,LEFT(Rate_List, LEN(Rate_List) - 1) AS rate  
                                   ,contracts  
                                   ,IS_NOTICE_GIVEN  
                                   ,IS_CONTRACT_REVIEWED  
                                   ,RFP_INITIATED_DATE  
                                   ,LP_SENT_TO_CLIENT_DATE  
                                   ,LP_APPROVED_DATE  
                                   ,cbmsImageId  
                                   ,RFP_SENT_DATE  
                                   ,IS_RC_COMPLETED  
                                   ,IS_CLIENT_CREDIT  
                                   ,IS_SUPPLIER_CREDIT  
                                   ,IS_SUPPLIER_DOCS  
                                   ,IS_MANAGER_APPROVED  
                                   ,SENT_RECO_DATE  
                                   ,IS_CLIENT_APPROVED  
                                   ,IS_CONTRACT_ADMIN_INITIATED  
                                   ,newSupplier  
                                   ,CONTRACT_START_DATE  
                                   ,CONTRACT_END_DATE  
                                   ,UTILITY_SWITCH_DEADLINE_DATE  
                                   ,UTILITY_SWITCH_SUPPLIER_DATE  
                                   ,IS_SWITCH_NOTICE_GIVEN  
                                   ,IS_VERIFY_FLOW  
                                   ,newContract  
                                   ,SA_SAVINGS  
                                   ,isNA  
                                   ,rcDocId  
                                   ,rcContentType  
                                ,iscommercialAcc  
                                   ,contractDocId  
                                   ,contractContentType  
                                   ,rcRateCompImageId  
                                   ,rateContentType  
                                   ,tariff_transport  
                                   ,Is_Broker_Account  
                                   ,Row_Num = ROW_NUMBER() OVER ( ORDER BY ( CASE @fieldName -- String Data type          
                                                                               WHEN 'site' THEN Site_name  
                                                                               WHEN 'client' THEN Client_Name  
                                                                               WHEN 'account' THEN Account_Number  
                                                                               WHEN 'address' THEN LEFT(Address_List, LEN(Address_List) - 1)  
                                                                               WHEN 'vendor' THEN VENDOR_NAME  
                                                                               WHEN 'rate' THEN LEFT(Rate_List, LEN(Rate_List) - 1)  
                                                                               WHEN 'contract' THEN contracts  
                                                                               WHEN 'newSupp' THEN newSupplier  
                                                                               WHEN 'saSavings' THEN SA_SAVINGS  
                                                                               WHEN 'newContract' THEN newContract  
                                                                               WHEN 'meter_numbers' THEN LEFT(Mtr_Num.meter_numbers, LEN(Mtr_Num.meter_numbers) - 1)  
                                                                               WHEN 'Addres_List' THEN LEFT(Addres.Addres_List, LEN(Addres.Addres_List) - 1)  
                                                                               WHEN 'Meter_Country' THEN Meter_Country_Name  
                                                                               WHEN 'Meter_States' THEN LEFT(Meter_States.Meter_States_List, LEN(Meter_States.Meter_States_List) - 1)  
                                                                             END ) ASC, ( CASE @fieldName -- NUMERIC Data type          
                                                                                            WHEN 'isNoticeGiven' THEN IS_NOTICE_GIVEN  
                                                                                            WHEN 'isConReviewed' THEN IS_CONTRACT_REVIEWED  
                                                                                            WHEN 'lpCAImageId' THEN cbmsImageId  
                                                                                            WHEN 'isRcCompleted' THEN IS_RC_COMPLETED  
                                                                                            WHEN 'isClientCredit' THEN IS_CLIENT_CREDIT  
                                                                                            WHEN 'isSupplierCredit' THEN IS_SUPPLIER_CREDIT  
                                                                                            WHEN 'isSuppDocs' THEN IS_SUPPLIER_DOCS  
                                                                                            WHEN 'isManagerApproved' THEN IS_MANAGER_APPROVED  
                                                                                            WHEN 'isClientApproved' THEN IS_CLIENT_APPROVED  
                                                                                            WHEN 'isConAdmin' THEN IS_CONTRACT_ADMIN_INITIATED  
                                                                                            WHEN 'isSnoticeGiven' THEN IS_SWITCH_NOTICE_GIVEN  
                                                 WHEN 'isVerifyFlow' THEN IS_VERIFY_FLOW  
                                                                                          END ) ASC, ( CASE @fieldName  -- DATE Data type          
                                                                                                         WHEN 'conStartDate' THEN CONTRACT_START_DATE  
                                                                                                         WHEN 'conEndDate' THEN CONTRACT_END_DATE  
                                                                                                         WHEN 'UtilitySwitchDdate' THEN UTILITY_SWITCH_DEADLINE_DATE  
                                                                                                         WHEN 'UtilitySwitchSdate' THEN UTILITY_SWITCH_SUPPLIER_DATE  
                                                                                                         WHEN 'rfpInitiateDate' THEN RFP_INITIATED_DATE  
                                                                                                         WHEN 'lpSentToClient' THEN LP_SENT_TO_CLIENT_DATE  
                                                                                                         WHEN 'lpApproveDate' THEN LP_APPROVED_DATE  
                                                                                                         WHEN 'sentRecoDate' THEN SENT_RECO_DATE  
                                                                                                         WHEN 'rfpSentDate' THEN RFP_SENT_DATE  
                                                                                                         WHEN 'DUE_DATE' THEN DUE_DATE  
                                                                                                         WHEN 'MANAGER_APPROVAL_DATE' THEN approval.UPLOADED_DATE  
                                                                                                         WHEN 'Contract_Signed_DATE' THEN srcai.UPLOADED_DATE  
                                                                                                       END ) ASC )  
                                   ,Total = COUNT(1) OVER ( )  
                                   ,LEFT(Mtr_Num.meter_numbers, LEN(Mtr_Num.meter_numbers) - 1) AS Meter_Numbers  
                                   ,LEFT(Addres.Addres_List, LEN(Addres.Addres_List) - 1) AS Address_List  
                                   ,LEFT(Meter_States.Meter_States_List, LEN(Meter_States.Meter_States_List) - 1) AS Meter_States_List  
                                   ,Meter_Country_Name AS Meter_Country  
                                   ,send_supp.DUE_DATE  
                                   ,approval.UPLOADED_DATE AS MANAGER_APPROVAL_DATE  
                                   ,srcai.UPLOADED_DATE AS Contract_Signed_DATE  
                                   ,cte.GROUP_NAME  
                                   ,cte.SR_RFP_BID_GROUP_ID  
                                   ,cte.Is_BID_GROUP  
                                   ,cte.Is_Archived_Account  
                                   ,cte.Alternate_Account_Number  
                              FROM  
                                    #Sr_Rfp_Check_List cte  
                                    CROSS APPLY ( SELECT  
                                                      cha.Meter_Number + ' ' + cha.Meter_Address_Line_1 + ','  
                                                  FROM  
                                                      Core.Client_Hier_Account cha  
                                                      JOIN dbo.SR_RFP_ACCOUNT_METER_MAP map  
                                                            ON map.METER_ID = cha.Meter_Id  
                                                      JOIN dbo.SR_RFP_ACCOUNT rfpAccount  
                                                            ON rfpAccount.ACCOUNT_ID = cha.Account_Id  
                                            AND rfpAccount.SR_RFP_ACCOUNT_ID = map.SR_RFP_ACCOUNT_ID  
                                                      JOIN dbo.SR_RFP rfp  
                                                            ON rfp.SR_RFP_ID = rfpAccount.SR_RFP_ID  
                                                  WHERE  
                                                      cha.Account_Id = cte.Account_Id  
                                                      AND rfp.SR_RFP_ID = cte.SR_RFP_ID  
                                                      AND rfpAccount.IS_DELETED = 0  
                                                  GROUP BY  
                                                      cha.Meter_Number  
                                                     ,cha.Meter_Address_Line_1  
                                    FOR  
                                                  XML PATH('') ) meter ( Address_List )  
                                    CROSS APPLY ( SELECT  
                                                      cha.Rate_Name + ','  
                                                  FROM  
                                                      Core.Client_Hier_Account cha  
                                                      JOIN dbo.SR_RFP_ACCOUNT_METER_MAP map  
                                                            ON map.METER_ID = cha.Meter_Id  
                                                      JOIN dbo.SR_RFP_ACCOUNT rfp_account  
                                                            ON rfp_account.SR_RFP_ACCOUNT_ID = map.SR_RFP_ACCOUNT_ID  
                                                  WHERE  
                                                      map.SR_RFP_ACCOUNT_ID = cte.SR_RFP_ACCOUNT_ID  
                                                      AND rfp_account.IS_DELETED = 0  
                                                      AND cha.Commodity_Id = cte.COMMODITY_TYPE_ID  
                                                  GROUP BY  
                                                      cha.Rate_Name  
                                    FOR  
                                                  XML PATH('') ) Rate ( Rate_List )  
                                    CROSS APPLY ( SELECT  
                                                      cha.Meter_Number + ','  
                                                  FROM  
                                                      Core.Client_Hier_Account cha  
                                                      JOIN dbo.SR_RFP_ACCOUNT_METER_MAP map  
                                                            ON map.METER_ID = cha.Meter_Id  
                                                      JOIN dbo.SR_RFP_ACCOUNT rfpAccount  
                                                            ON rfpAccount.ACCOUNT_ID = cha.Account_Id  
                                                               AND rfpAccount.SR_RFP_ACCOUNT_ID = map.SR_RFP_ACCOUNT_ID  
                                                      JOIN dbo.SR_RFP rfp  
                                                            ON rfp.SR_RFP_ID = rfpAccount.SR_RFP_ID  
                                                  WHERE  
                                                      cha.Account_Id = cte.Account_Id  
                                                      AND rfp.SR_RFP_ID = cte.SR_RFP_ID  
                                                      AND rfpAccount.IS_DELETED = 0  
                                                  GROUP BY  
                                                      cha.Meter_Number  
                                    FOR  
                                                  XML PATH('') ) Mtr_Num ( meter_numbers )  
                                    CROSS APPLY ( SELECT  
                                                      cha.Meter_Address_Line_1 + '  ' + cha.Meter_ZipCode + ','  
                                                  FROM  
                   Core.Client_Hier_Account cha  
                                                      JOIN dbo.SR_RFP_ACCOUNT_METER_MAP map  
                                                            ON map.METER_ID = cha.Meter_Id  
                                                      JOIN dbo.SR_RFP_ACCOUNT rfpAccount  
                                                            ON rfpAccount.ACCOUNT_ID = cha.Account_Id  
                                                               AND rfpAccount.SR_RFP_ACCOUNT_ID = map.SR_RFP_ACCOUNT_ID  
                                                      JOIN dbo.SR_RFP rfp  
                                                            ON rfp.SR_RFP_ID = rfpAccount.SR_RFP_ID  
                                                  WHERE  
                                                      cha.Account_Id = cte.Account_Id  
                                                      AND rfp.SR_RFP_ID = cte.SR_RFP_ID  
                                                      AND rfpAccount.IS_DELETED = 0  
                                                  GROUP BY  
                                                      cha.Meter_ZipCode  
                                                     ,cha.Meter_Address_Line_1  
                                    FOR  
                                                  XML PATH('') ) Addres ( Addres_List )  
                                    CROSS APPLY ( SELECT  
                                                      cha.Meter_State_Name + ','  
                                                  FROM  
                                                      dbo.SR_RFP_ACCOUNT rfpAccount  
                                                      JOIN dbo.SR_RFP rfp  
                                                            ON rfp.SR_RFP_ID = rfpAccount.SR_RFP_ID  
                                                      JOIN Core.Client_Hier_Account cha  
                                                            ON rfpAccount.ACCOUNT_ID = cha.Account_Id  
                                                               AND cha.Commodity_Id = rfp.COMMODITY_TYPE_ID  
                                                  WHERE  
                                                      cha.Account_Id = cte.Account_Id  
                                                      AND rfp.SR_RFP_ID = cte.SR_RFP_ID  
                                                      AND rfpAccount.IS_DELETED = 0  
                                                  GROUP BY  
                                                      cha.Meter_State_Name  
                                    FOR  
                                                  XML PATH('') ) Meter_States ( Meter_States_List )  
                                    LEFT JOIN ( SELECT  
                                                      MAX(DUE_DATE) AS DUE_DATE  
                                                     ,IS_BID_GROUP  
                                                     ,SR_ACCOUNT_GROUP_ID  
                                                FROM  
                                                      dbo.SR_RFP_SEND_SUPPLIER  
                                                GROUP BY  
                                                      IS_BID_GROUP  
                                                     ,SR_ACCOUNT_GROUP_ID ) send_supp  
                                          ON send_supp.SR_ACCOUNT_GROUP_ID = ISNULL(cte.SR_RFP_BID_GROUP_ID, cte.SR_RFP_ACCOUNT_ID)  
                                             AND send_supp.IS_BID_GROUP = cte.Is_BID_GROUP  
                                    LEFT JOIN dbo.SR_RFP_MANAGER_APPROVAL approval  
                                          ON approval.SR_ACCOUNT_GROUP_ID = ISNULL(cte.SR_RFP_BID_GROUP_ID, cte.SR_RFP_ACCOUNT_ID)  
                                             AND approval.IS_BID_GROUP = cte.Is_BID_GROUP  
                                    LEFT JOIN dbo.SR_RFP_CONTRACT_ADMIN_INITIATIVES srcai  
                               ON srcai.SR_ACCOUNT_GROUP_ID = ISNULL(cte.SR_RFP_BID_GROUP_ID, cte.SR_RFP_ACCOUNT_ID)  
                                             AND srcai.IS_BID_GROUP = cte.Is_BID_GROUP  
                  SELECT  
                        Client_Name  
                       ,Site_Id  
                       ,Site_name  
                       ,Account_Number  
                       ,ADDRESS  
                       ,VENDOR_NAME  
                       ,rate  
                       ,CASE WHEN Is_Archived_Account = 1 THEN contracts  
                             ELSE dbo.SR_RFP_FN_GET_CONTRACT_DETAILS_FOR_CHECKLIST(Account_Id, @CurrentDt, COMMODITY_TYPE_ID)  
                        END AS contracts  
                       ,IS_NOTICE_GIVEN  
                       ,dbo.SR_SAD_FN_GET_IS_NOTICE_IMAGE_ID(Account_Id, @CurrentDt, COMMODITY_TYPE_ID) AS isNoticeImageId  
                       ,IS_CONTRACT_REVIEWED  
                       ,RFP_INITIATED_DATE  
                       ,LP_SENT_TO_CLIENT_DATE  
                       ,LP_APPROVED_DATE  
                       ,cbmsImageId  
                       ,RFP_SENT_DATE  
                       ,IS_RC_COMPLETED  
                       ,IS_CLIENT_CREDIT  
                       ,IS_SUPPLIER_CREDIT  
                       ,IS_SUPPLIER_DOCS  
                       ,IS_MANAGER_APPROVED  
                       ,SENT_RECO_DATE  
                       ,IS_CLIENT_APPROVED  
                       ,IS_CONTRACT_ADMIN_INITIATED  
                       ,newSupplier  
                       ,CONTRACT_START_DATE  
                       ,CONTRACT_END_DATE  
                       ,UTILITY_SWITCH_DEADLINE_DATE  
                       ,UTILITY_SWITCH_SUPPLIER_DATE  
                       ,IS_SWITCH_NOTICE_GIVEN  
                       ,IS_VERIFY_FLOW  
                       ,newContract  
                       ,SA_SAVINGS  
                       ,isNA  
                       ,rcDocId  
                       ,rcContentType  
                       ,iscommercialAcc  
                       ,contractDocId  
                       ,contractContentType  
                       ,rcRateCompImageId  
                       ,rateContentType  
                       ,tariff_transport  
                       ,Is_Broker_Account  
                       ,Total  
                       ,Meter_Numbers  
                       ,Address_List  
                       ,Meter_States_List  
                       ,Meter_Country  
                       ,DUE_DATE  
                       ,MANAGER_APPROVAL_DATE  
                       ,Contract_Signed_DATE  
                       ,GROUP_NAME AS Bid_Group  
                       ,cte.Alternate_Account_Number  
                  FROM  
                        #Sr_Rfp_Check_List_Indexed cte  
                  WHERE  
                        Row_Num BETWEEN @StartIndex AND @EndIndex         
         
            END          
      ELSE -- DESC          
            BEGIN          
            
            
                  INSERT      INTO #Sr_Rfp_Check_List  
                              (   
                               Client_Name  
                              ,Site_Id  
                              ,Site_name  
                              ,Account_Number  
                              ,SR_RFP_ID  
                              ,Account_Id  
                              ,SR_RFP_ACCOUNT_ID  
                              ,COMMODITY_TYPE_ID  
                              ,VENDOR_NAME  
                              ,contracts  
                              ,IS_NOTICE_GIVEN  
                              ,IS_CONTRACT_REVIEWED  
                              ,RFP_INITIATED_DATE  
                              ,LP_SENT_TO_CLIENT_DATE  
                              ,LP_APPROVED_DATE  
                              ,cbmsImageId  
                              ,RFP_SENT_DATE  
                              ,IS_RC_COMPLETED  
                              ,IS_CLIENT_CREDIT  
,IS_SUPPLIER_CREDIT  
                              ,IS_SUPPLIER_DOCS  
                              ,IS_MANAGER_APPROVED  
                              ,SENT_RECO_DATE  
                              ,IS_CLIENT_APPROVED  
                              ,IS_CONTRACT_ADMIN_INITIATED  
                              ,newSupplier  
                              ,CONTRACT_START_DATE  
                              ,CONTRACT_END_DATE  
                              ,UTILITY_SWITCH_DEADLINE_DATE  
                              ,UTILITY_SWITCH_SUPPLIER_DATE  
                              ,IS_SWITCH_NOTICE_GIVEN  
                              ,IS_VERIFY_FLOW  
                              ,newContract  
                              ,SA_SAVINGS  
                              ,isNA  
                              ,rcDocId  
                              ,rcContentType  
                              ,iscommercialAcc  
                              ,contractDocId  
                              ,contractContentType  
                              ,rcRateCompImageId  
                              ,rateContentType  
                              ,tariff_transport  
                              ,Is_Broker_Account  
                              ,SR_RFP_BID_GROUP_ID  
                              ,Is_BID_GROUP  
                              ,Meter_Country_Name  
                              ,GROUP_NAME  
                              ,Is_Archived_Account  
                              ,Alternate_Account_Number )  
                              SELECT  
                                    chier.Client_Name  
                                   ,chier.Site_Id  
                                   ,chier.Site_Name  
                                   ,chier.Account_Number  
                                   ,rfp.SR_RFP_ID  
                                   ,chier.account_Id  
                                   ,rfp_account.SR_RFP_ACCOUNT_ID  
                                   ,rfp.COMMODITY_TYPE_ID  
                                   ,chier.Account_Vendor_Name VENDOR_NAME  
                                   ,NULL AS contracts  
                                   ,checklist.IS_NOTICE_GIVEN  
                                   ,checklist.IS_CONTRACT_REVIEWED  
                                   ,checklist.RFP_INITIATED_DATE  
                                   ,checklist.LP_SENT_TO_CLIENT_DATE  
                                   ,checklist.LP_APPROVED_DATE  
                                   ,approval.CBMS_IMAGE_ID AS cbmsImageId  
                                   ,checklist.RFP_SENT_DATE  
                                   ,checklist.IS_RC_COMPLETED  
                                   ,checklist.IS_CLIENT_CREDIT  
                                   ,checklist.IS_SUPPLIER_CREDIT  
                                   ,checklist.IS_SUPPLIER_DOCS  
                                   ,checklist.IS_MANAGER_APPROVED  
                                   ,checklist.SENT_RECO_DATE  
                                   ,checklist.IS_CLIENT_APPROVED  
                                   ,checklist.IS_CONTRACT_ADMIN_INITIATED  
                                   ,supp.VENDOR_NAME AS newSupplier  
                                   ,checklist.CONTRACT_START_DATE  
                                   ,checklist.CONTRACT_END_DATE  
                                   ,checklist.UTILITY_SWITCH_DEADLINE_DATE  
                                   ,checklist.UTILITY_SWITCH_SUPPLIER_DATE  
                                   ,checklist.IS_SWITCH_NOTICE_GIVEN  
                                   ,checklist.IS_VERIFY_FLOW  
                                   ,cont.ED_CONTRACT_NUMBER AS newContract  
                                   ,checklist.SA_SAVINGS  
                                   ,CASE utility_switch.RETURN_TO_TARIFF_TYPE_ID  
                                      WHEN 1280 THEN 1  
                                      ELSE 0  
                                    END isNA  
                                   ,rcContract.rcDocId AS rcDocId  
                                   ,rcContract.rcContentType AS rcContentType  
                                   ,CASE chier.Account_Service_level_Cd  
                                      WHEN 861 THEN 1  
                                      ELSE 0  
                                    END iscommercialAcc  
                                   ,rcContract.contractDocId AS contractDocId  
                                   ,rcContract.contractContentType AS contractContentType  
                                   ,rc.cbms_image_id AS rcRateCompImageId  
                                   ,rc.rateContentType AS rateContentType  
                                   ,CASE WHEN trvw.Tariff_Transport IS NULL THEN 'Tariff'  
                                         ELSE 'Transport'  
                                    END tariff_transport  
                                   ,CASE WHEN chier.Account_Is_Broker = 1 THEN 'Y'  
                                         ELSE 'N'  
                                    END AS Is_Broker_Account  
                                   ,rfp_account.SR_RFP_BID_GROUP_ID  
                                   ,CASE WHEN rfp_account.SR_RFP_BID_GROUP_ID IS NOT NULL THEN 1  
                                         ELSE 0  
                                    END AS Is_BID_GROUP  
                                   ,chier.Meter_Country_Name  
                                   ,bidgroup.GROUP_NAME  
                                   ,0 AS Is_Archived_Account  
                                   ,chier.Alternate_Account_Number  
                              FROM  
                                    dbo.SR_RFP rfp  
                                    INNER JOIN dbo.SR_RFP_ACCOUNT rfp_account  
                                          ON rfp_account.SR_RFP_ID = rfp.SR_RFP_ID  
                                    INNER JOIN dbo.SR_RFP_CHECKLIST checklist  
                                          ON checklist.SR_RFP_ACCOUNT_ID = rfp_account.SR_RFP_ACCOUNT_ID  
                                    INNER JOIN #Accounts_Bid_Status_Not_Closed chier  
                                          ON chier.account_Id = rfp_account.ACCOUNT_ID  
                                    LEFT JOIN @temp_rc rc  
                                          ON rc.account_id = rfp_account.ACCOUNT_ID  
                                    LEFT JOIN SR_RFP_LP_CLIENT_APPROVAL approval  
                                          ON approval.SR_ACCOUNT_GROUP_ID = rfp_account.SR_RFP_ACCOUNT_ID  
                                             AND approval.IS_GROUP_LP = 0  
                                    LEFT JOIN @temp_rc_contract_for_rfp rcContract  
                                          ON rcContract.sr_rfp_id = rfp_account.SR_RFP_ID  
                                             AND rcContract.sr_rfp_account_id = rfp_account.SR_RFP_ACCOUNT_ID  
                                    LEFT JOIN dbo.SR_RFP_UTILITY_SWITCH utility_switch  
                                          ON utility_switch.SR_ACCOUNT_GROUP_ID = rfp_account.SR_RFP_ACCOUNT_ID  
                                    LEFT JOIN dbo.BUDGET_TRANSPORT_ACCOUNT_VW trvw  
                                          ON trvw.account_id = chier.account_Id  
                                             AND trvw.commodity_type_id = rfp.COMMODITY_TYPE_ID  
                                    LEFT JOIN dbo.VENDOR supp  
                                          ON checklist.NEW_SUPPLIER_ID = supp.VENDOR_ID  
                                    LEFT JOIN dbo.CONTRACT cont  
                                          ON cont.CONTRACT_ID = checklist.NEW_CONTRACT_ID  
                                    LEFT JOIN dbo.SR_RFP_BID_GROUP bidgroup  
                                          ON rfp_account.SR_RFP_BID_GROUP_ID = bidgroup.SR_RFP_BID_GROUP_ID  
                              WHERE  
                                    rfp_account.SR_RFP_ID = @rfpId  
                                    AND rfp_account.IS_DELETED = 0  
                                    AND rfp_account.BID_STATUS_TYPE_ID != @closedStatus  
                              UNION           
  -- Query 2          
                              SELECT  
                                    chier.Client_Name  
                                   ,chier.Site_Id  
                                   ,chier.Site_Name  
                                   ,chier.Account_Number  
                                   ,rfp.SR_RFP_ID  
                                   ,chier.account_Id  
                                   ,rfp_account.SR_RFP_ACCOUNT_ID  
                                   ,rfp.COMMODITY_TYPE_ID  
                                   ,chier.Account_Vendor_Name VENDOR_NAME  
                                   ,archiveChecklist.ED_CONTRACT_NUMBER + '~' + archiveChecklist.CURRENT_SUPPLIER + '|' + CONVERT(VARCHAR(10), archiveChecklist.CONTRACT_EXPIRY_DATE, 101) AS CONTRACTS  
                                   ,archiveChecklist.IS_NOTICE_GIVEN  
                                   ,checklist.IS_CONTRACT_REVIEWED  
                                   ,checklist.RFP_INITIATED_DATE  
                                   ,checklist.LP_SENT_TO_CLIENT_DATE  
                                   ,checklist.LP_APPROVED_DATE  
                                   ,approval.CBMS_IMAGE_ID AS cbmsImageId  
                                   ,checklist.RFP_SENT_DATE  
                                   ,archiveChecklist.IS_RC_COMPLETE  
                                   ,checklist.IS_CLIENT_CREDIT  
                                   ,checklist.IS_SUPPLIER_CREDIT  
                                   ,checklist.IS_SUPPLIER_DOCS  
                                   ,checklist.IS_MANAGER_APPROVED  
                                   ,checklist.SENT_RECO_DATE  
                                   ,checklist.IS_CLIENT_APPROVED  
                                   ,checklist.IS_CONTRACT_ADMIN_INITIATED  
                                   ,archiveChecklist.NEW_SUPPLIER_NAME  
                                   ,checklist.CONTRACT_START_DATE  
                                   ,checklist.CONTRACT_END_DATE  
                                   ,checklist.UTILITY_SWITCH_DEADLINE_DATE  
                                   ,checklist.UTILITY_SWITCH_SUPPLIER_DATE  
                                   ,checklist.IS_SWITCH_NOTICE_GIVEN  
                                   ,checklist.IS_VERIFY_FLOW  
                                   ,archiveChecklist.NEW_CONTRACT_NUMBER  
                                   ,checklist.SA_SAVINGS  
                                   ,CASE WHEN utility_switch.RETURN_TO_TARIFF_TYPE_ID = @ServiceLvl_ID1 THEN 1  
                                         ELSE 0  
                                    END isNA  
                                   ,rcContract.rcDocId AS rcDocId  
                                   ,rcContract.rcContentType AS rcContentType  
                                   ,CASE WHEN chier.Account_Service_level_Cd = @ServiceLvl_ID2 THEN 1  
                                         ELSE 0  
                                    END iscommercialAcc  
                                   ,rcContract.contractDocId AS contractDocId  
                                   ,rcContract.contractContentType AS contractContentType  
                                   ,rc.cbms_image_id AS rcRateCompImageId  
                                   ,rc.rateContentType AS rateContentType  
                                   ,CASE WHEN trvw.Tariff_Transport IS NULL THEN 'Tariff'  
                                         ELSE 'Transport'  
                                    END tariff_transport  
                                   ,CASE WHEN chier.Account_Is_Broker = 1 THEN 'Y'  
                                         ELSE 'N'  
                                    END AS Is_Broker_Account  
                                   ,rfp_account.SR_RFP_BID_GROUP_ID  
                                   ,CASE WHEN rfp_account.SR_RFP_BID_GROUP_ID IS NOT NULL THEN 1  
                                         ELSE 0  
                                    END AS Is_BID_GROUP  
                                   ,chier.Meter_Country_Name  
                                   ,bidgroup.GROUP_NAME  
                                   ,1 AS Is_Archived_Account  
                                   ,chier.Alternate_Account_Number  
                              FROM  
                                    dbo.SR_RFP rfp  
                                    INNER JOIN dbo.SR_RFP_ACCOUNT rfp_account  
                                          ON rfp_account.SR_RFP_ID = rfp.SR_RFP_ID  
                                    INNER JOIN dbo.SR_RFP_CHECKLIST checklist  
                                          ON checklist.SR_RFP_ACCOUNT_ID = rfp_account.SR_RFP_ACCOUNT_ID  
                                    INNER JOIN #Accounts_Bid_Status_Closed chier  
                                          ON chier.account_Id = rfp_account.ACCOUNT_ID  
                                    JOIN dbo.SR_RFP_ARCHIVE_ACCOUNT rfp_archive_account  
                                          ON rfp_archive_account.SR_RFP_ACCOUNT_ID = checklist.SR_RFP_ACCOUNT_ID  
                                    JOIN dbo.SR_RFP_ARCHIVE_CHECKLIST archiveChecklist  
                                          ON archiveChecklist.SR_RFP_ACCOUNT_ID = rfp_archive_account.SR_RFP_ACCOUNT_ID  
                                    LEFT JOIN @temp_rc rc  
                                          ON rc.account_id = rfp_account.ACCOUNT_ID  
                                    LEFT JOIN dbo.SR_RFP_LP_CLIENT_APPROVAL approval  
                                          ON approval.SR_ACCOUNT_GROUP_ID = rfp_account.SR_RFP_ACCOUNT_ID  
                                             AND approval.IS_GROUP_LP = 0  
                                    LEFT JOIN @temp_rc_contract_for_rfp rcContract  
                                          ON rcContract.sr_rfp_id = rfp_account.SR_RFP_ID  
                                             AND rcContract.sr_rfp_account_id = rfp_account.SR_RFP_ACCOUNT_ID  
                                    LEFT JOIN dbo.SR_RFP_UTILITY_SWITCH utility_switch  
                                          ON utility_switch.SR_ACCOUNT_GROUP_ID = rfp_account.SR_RFP_ACCOUNT_ID  
                                    LEFT JOIN dbo.BUDGET_TRANSPORT_ACCOUNT_VW trvw  
                                          ON trvw.account_id = chier.account_Id  
                                             AND trvw.commodity_type_id = rfp.COMMODITY_TYPE_ID  
                                    LEFT JOIN dbo.SR_RFP_BID_GROUP bidgroup  
                                          ON rfp_account.SR_RFP_BID_GROUP_ID = bidgroup.SR_RFP_BID_GROUP_ID  
                              WHERE  
                                    rfp_account.SR_RFP_ID = @rfpId  
                                    AND rfp_account.IS_DELETED = 0  
                                    AND rfp_account.BID_STATUS_TYPE_ID = @closedStatus   
                                  
                  INSERT      INTO #Sr_Rfp_Check_List_Indexed  
                              (   
                               Client_Name  
                              ,Site_Id  
                              ,Site_name  
                              ,Account_Number  
                              ,ADDRESS  
                              ,SR_RFP_ID  
                              ,Account_Id  
                              ,SR_RFP_ACCOUNT_ID  
                              ,COMMODITY_TYPE_ID  
                              ,VENDOR_NAME  
                              ,rate  
                              ,contracts  
                              ,IS_NOTICE_GIVEN  
                              ,IS_CONTRACT_REVIEWED  
                              ,RFP_INITIATED_DATE  
                              ,LP_SENT_TO_CLIENT_DATE  
                              ,LP_APPROVED_DATE  
                              ,cbmsImageId  
                              ,RFP_SENT_DATE  
                              ,IS_RC_COMPLETED  
                              ,IS_CLIENT_CREDIT  
                              ,IS_SUPPLIER_CREDIT  
                              ,IS_SUPPLIER_DOCS  
                              ,IS_MANAGER_APPROVED  
                              ,SENT_RECO_DATE  
                              ,IS_CLIENT_APPROVED  
                              ,IS_CONTRACT_ADMIN_INITIATED  
                              ,newSupplier  
                              ,CONTRACT_START_DATE  
                              ,CONTRACT_END_DATE  
                              ,UTILITY_SWITCH_DEADLINE_DATE  
                              ,UTILITY_SWITCH_SUPPLIER_DATE  
                              ,IS_SWITCH_NOTICE_GIVEN  
                              ,IS_VERIFY_FLOW  
                              ,newContract  
                              ,SA_SAVINGS  
                              ,isNA  
                              ,rcDocId  
                              ,rcContentType  
                              ,iscommercialAcc  
                              ,contractDocId  
                              ,contractContentType  
                              ,rcRateCompImageId  
                              ,rateContentType  
                              ,tariff_transport  
                              ,Is_Broker_Account  
                              ,Row_Num  
                              ,Total  
                              ,Meter_Numbers  
                              ,Address_List  
                              ,Meter_States_List  
                              ,Meter_Country  
                              ,DUE_DATE  
                              ,MANAGER_APPROVAL_DATE  
                              ,Contract_Signed_DATE  
                              ,GROUP_NAME  
                              ,SR_RFP_BID_GROUP_ID  
                              ,Is_BID_GROUP  
                              ,Is_Archived_Account  
                              ,Alternate_Account_Number )  
                              SELECT  
                                    Client_Name  
                                   ,Site_Id  
                                   ,Site_name  
                                   ,Account_Number  
                                   ,LEFT(Address_List, LEN(Address_List) - 1) AS address  
                                   ,SR_RFP_ID  
                                   ,Account_Id  
                                   ,SR_RFP_ACCOUNT_ID  
                                   ,COMMODITY_TYPE_ID  
                                   ,VENDOR_NAME  
                                   ,LEFT(Rate_List, LEN(Rate_List) - 1) AS rate  
                                   ,contracts  
                                   ,IS_NOTICE_GIVEN  
                                   ,IS_CONTRACT_REVIEWED  
                                   ,RFP_INITIATED_DATE  
                                   ,LP_SENT_TO_CLIENT_DATE  
                                   ,LP_APPROVED_DATE  
                                   ,cbmsImageId  
                                   ,RFP_SENT_DATE  
                                   ,IS_RC_COMPLETED  
                                   ,IS_CLIENT_CREDIT  
                                   ,IS_SUPPLIER_CREDIT  
                                   ,IS_SUPPLIER_DOCS  
                                   ,IS_MANAGER_APPROVED  
                                   ,SENT_RECO_DATE  
                                   ,IS_CLIENT_APPROVED  
                                   ,IS_CONTRACT_ADMIN_INITIATED  
                                   ,newSupplier  
                                   ,CONTRACT_START_DATE  
                                   ,CONTRACT_END_DATE  
                                   ,UTILITY_SWITCH_DEADLINE_DATE  
                                   ,UTILITY_SWITCH_SUPPLIER_DATE  
                                   ,IS_SWITCH_NOTICE_GIVEN  
                                   ,IS_VERIFY_FLOW  
                                   ,newContract  
                ,SA_SAVINGS  
                                   ,isNA  
                                   ,rcDocId  
                                   ,rcContentType  
                                   ,iscommercialAcc  
                                   ,contractDocId  
                                   ,contractContentType  
                                   ,rcRateCompImageId  
                                   ,rateContentType  
                                   ,tariff_transport  
                                   ,Is_Broker_Account  
                                   ,Row_num = ROW_NUMBER() OVER ( ORDER BY ( CASE @fieldName -- String Data type          
                                                                               WHEN 'site' THEN Site_name  
                                                                               WHEN 'client' THEN Client_Name  
                                                                               WHEN 'account' THEN Account_Number  
                                                                               WHEN 'address' THEN LEFT(Address_List, LEN(Address_List) - 1)  
                                                                               WHEN 'vendor' THEN VENDOR_NAME  
                                                                               WHEN 'rate' THEN LEFT(Rate_List, LEN(Rate_List) - 1)  
                                                                               WHEN 'contract' THEN contracts  
                                                                               WHEN 'newSupp' THEN newSupplier  
                                                                               WHEN 'saSavings' THEN SA_SAVINGS  
                                                                               WHEN 'newContract' THEN newContract  
                                                                               WHEN 'meter_numbers' THEN LEFT(Mtr_Num.meter_numbers, LEN(Mtr_Num.meter_numbers) - 1)  
                                                                               WHEN 'Addres_List' THEN LEFT(Addres.Addres_List, LEN(Addres.Addres_List) - 1)  
                                                                               WHEN 'Meter_Country' THEN Meter_Country_Name  
                                                                               WHEN 'Meter_States' THEN LEFT(Meter_States.Meter_States_List, LEN(Meter_States.Meter_States_List) - 1)  
                                                                             END ) DESC, ( CASE @fieldName -- NUMERIC Data type              
                                                                                             WHEN 'isNoticeGiven' THEN IS_NOTICE_GIVEN  
                                                                                             WHEN 'isConReviewed' THEN IS_CONTRACT_REVIEWED  
                                                                                             WHEN 'lpCAImageId' THEN cbmsImageId  
                                                                                             WHEN 'isRcCompleted' THEN IS_RC_COMPLETED  
                                                                                             WHEN 'isClientCredit' THEN IS_CLIENT_CREDIT  
                                                                                             WHEN 'isSupplierCredit' THEN IS_SUPPLIER_CREDIT  
                                                                                             WHEN 'isSuppDocs' THEN IS_SUPPLIER_DOCS  
                                                                                             WHEN 'isManagerApproved' THEN IS_MANAGER_APPROVED  
                                                                                             WHEN 'isClientApproved' THEN IS_CLIENT_APPROVED  
                                                                                             WHEN 'isConAdmin' THEN IS_CONTRACT_ADMIN_INITIATED  
                                                                                            WHEN 'isSnoticeGiven' THEN IS_SWITCH_NOTICE_GIVEN  
                                                                                             WHEN 'isVerifyFlow' THEN IS_VERIFY_FLOW  
                                                                                           END ) DESC, ( CASE @fieldName  -- DATE Data type          
                                                                                                           WHEN 'conStartDate' THEN CONTRACT_START_DATE  
                                                                                                           WHEN 'conEndDate' THEN CONTRACT_END_DATE  
                                                                                                           WHEN 'UtilitySwitchDdate' THEN UTILITY_SWITCH_DEADLINE_DATE  
                                                                                                           WHEN 'UtilitySwitchSdate' THEN UTILITY_SWITCH_SUPPLIER_DATE  
                                                                                                           WHEN 'rfpInitiateDate' THEN RFP_INITIATED_DATE  
                                                                                                           WHEN 'lpSentToClient' THEN LP_SENT_TO_CLIENT_DATE  
                                                                                                           WHEN 'lpApproveDate' THEN LP_APPROVED_DATE  
                                                                                                           WHEN 'sentRecoDate' THEN SENT_RECO_DATE  
                                                                                                           WHEN 'rfpSentDate' THEN RFP_SENT_DATE  
                                                                                                           WHEN 'DUE_DATE' THEN DUE_DATE  
                                                                                                           WHEN 'MANAGER_APPROVAL_DATE' THEN approval.UPLOADED_DATE  
                                                                                                           WHEN 'Contract_Signed_DATE' THEN srcai.UPLOADED_DATE  
                                                                                                         END ) DESC )  
                                   ,Total = COUNT(1) OVER ( )  
                                   ,LEFT(Mtr_Num.meter_numbers, LEN(Mtr_Num.meter_numbers) - 1) AS Meter_Numbers  
                                   ,LEFT(Addres.Addres_List, LEN(Addres.Addres_List) - 1) AS Address_List  
                                   ,LEFT(Meter_States.Meter_States_List, LEN(Meter_States.Meter_States_List) - 1) AS Meter_States_List  
                                   ,Meter_Country_Name AS Meter_Country  
                                   ,send_supp.DUE_DATE  
                                   ,approval.UPLOADED_DATE AS MANAGER_APPROVAL_DATE  
                                   ,srcai.UPLOADED_DATE AS Contract_Signed_DATE  
                                   ,cte.GROUP_NAME  
                                   ,cte.SR_RFP_BID_GROUP_ID  
                                   ,cte.Is_BID_GROUP  
                                   ,cte.Is_Archived_Account  
                                   ,cte.Alternate_Account_Number  
                              FROM  
                                    #Sr_Rfp_Check_List cte  
                                    CROSS APPLY ( SELECT  
                                                      cha.Meter_Number + ' ' + cha.Meter_Address_Line_1 + ','  
                                                  FROM  
                                                      Core.Client_Hier_Account cha  
                                                      JOIN dbo.SR_RFP_ACCOUNT_METER_MAP map  
                                                            ON map.METER_ID = cha.Meter_Id  
                                                      JOIN dbo.SR_RFP_ACCOUNT rfpAccount  
                                                            ON rfpAccount.ACCOUNT_ID = cha.Account_Id  
                                                               AND rfpAccount.SR_RFP_ACCOUNT_ID = map.SR_RFP_ACCOUNT_ID  
                                                      JOIN dbo.SR_RFP rfp  
                                                            ON rfp.SR_RFP_ID = rfpAccount.SR_RFP_ID  
                                                  WHERE  
                                                      cha.Account_Id = cte.Account_Id  
                                                      AND rfp.SR_RFP_ID = cte.SR_RFP_ID  
                                                      AND rfpAccount.IS_DELETED = 0  
                                                  GROUP BY  
                                                      cha.Meter_Number  
                                                     ,cha.Meter_Address_Line_1  
                                    FOR  
                                                  XML PATH('') ) meter ( Address_List )  
                                    CROSS APPLY ( SELECT  
                                                      cha.Rate_Name + ','  
                                                  FROM  
                                                      Core.Client_Hier_Account cha  
                                                      JOIN dbo.SR_RFP_ACCOUNT_METER_MAP map  
                                                            ON map.METER_ID = cha.Meter_Id  
                                                      JOIN dbo.SR_RFP_ACCOUNT rfp_account  
                                                            ON rfp_account.SR_RFP_ACCOUNT_ID = map.SR_RFP_ACCOUNT_ID  
                                                  WHERE  
                                                      map.SR_RFP_ACCOUNT_ID = cte.SR_RFP_ACCOUNT_ID  
                                                      AND rfp_account.IS_DELETED = 0  
                                                      AND cha.Commodity_Id = cte.COMMODITY_TYPE_ID  
                                    FOR  
                                                  XML PATH('') ) Rate ( Rate_List )  
                                    CROSS APPLY ( SELECT  
                                                      cha.Meter_Number + ','  
                                                  FROM  
                                                      Core.Client_Hier_Account cha  
                                                      JOIN dbo.SR_RFP_ACCOUNT_METER_MAP map  
                                                            ON map.METER_ID = cha.Meter_Id  
                                                      JOIN dbo.SR_RFP_ACCOUNT rfpAccount  
                                                            ON rfpAccount.ACCOUNT_ID = cha.Account_Id  
                                                               AND rfpAccount.SR_RFP_ACCOUNT_ID = map.SR_RFP_ACCOUNT_ID  
                                                      JOIN dbo.SR_RFP rfp  
                                                            ON rfp.SR_RFP_ID = rfpAccount.SR_RFP_ID  
                                                  WHERE  
                                                      cha.Account_Id = cte.Account_Id  
                                                      AND rfp.SR_RFP_ID = cte.SR_RFP_ID  
                                                      AND rfpAccount.IS_DELETED = 0  
                                                  GROUP BY  
                                                      cha.Meter_Number  
                                    FOR  
                                                  XML PATH('') ) Mtr_Num ( meter_numbers )  
                                    CROSS APPLY ( SELECT  
                                                      cha.Meter_Address_Line_1 + '  ' + cha.Meter_ZipCode + ','  
                                              FROM  
                                                      Core.Client_Hier_Account cha  
                                                      JOIN dbo.SR_RFP_ACCOUNT_METER_MAP map  
                                                            ON map.METER_ID = cha.Meter_Id  
                                                      JOIN dbo.SR_RFP_ACCOUNT rfpAccount  
                                                            ON rfpAccount.ACCOUNT_ID = cha.Account_Id  
                                                               AND rfpAccount.SR_RFP_ACCOUNT_ID = map.SR_RFP_ACCOUNT_ID  
                                                      JOIN dbo.SR_RFP rfp  
                                                            ON rfp.SR_RFP_ID = rfpAccount.SR_RFP_ID  
                                                  WHERE  
                                                      cha.Account_Id = cte.Account_Id  
                                                      AND rfp.SR_RFP_ID = cte.SR_RFP_ID  
                                                      AND rfpAccount.IS_DELETED = 0  
                                                  GROUP BY  
                                                      cha.Meter_ZipCode  
                                                     ,cha.Meter_Address_Line_1  
                                    FOR  
                                                  XML PATH('') ) Addres ( Addres_List )  
                                    CROSS APPLY ( SELECT  
                                                      cha.Meter_State_Name + ','  
                                                  FROM  
                                                      dbo.SR_RFP_ACCOUNT rfpAccount  
                                                      JOIN dbo.SR_RFP rfp  
                                                            ON rfp.SR_RFP_ID = rfpAccount.SR_RFP_ID  
                                                      JOIN Core.Client_Hier_Account cha  
                                                            ON rfpAccount.ACCOUNT_ID = cha.Account_Id  
                                                               AND cha.Commodity_Id = rfp.COMMODITY_TYPE_ID  
                                                  WHERE  
                                                      cha.Account_Id = cte.Account_Id  
                                                      AND rfp.SR_RFP_ID = cte.SR_RFP_ID  
                                                      AND rfpAccount.IS_DELETED = 0  
                                                  GROUP BY  
                                                      cha.Meter_State_Name  
                                    FOR  
                                                  XML PATH('') ) Meter_States ( Meter_States_List )  
                                    LEFT JOIN ( SELECT  
                                                      MAX(DUE_DATE) AS DUE_DATE  
                                                     ,IS_BID_GROUP  
                                                     ,SR_ACCOUNT_GROUP_ID  
                                                FROM  
                                                      dbo.SR_RFP_SEND_SUPPLIER  
                                                GROUP BY  
                                                      IS_BID_GROUP  
                                                     ,SR_ACCOUNT_GROUP_ID ) send_supp  
                                          ON send_supp.SR_ACCOUNT_GROUP_ID = ISNULL(cte.SR_RFP_BID_GROUP_ID, cte.SR_RFP_ACCOUNT_ID)  
                                             AND send_supp.IS_BID_GROUP = cte.Is_BID_GROUP  
                                    LEFT JOIN dbo.SR_RFP_MANAGER_APPROVAL approval  
                                          ON approval.SR_ACCOUNT_GROUP_ID = ISNULL(cte.SR_RFP_BID_GROUP_ID, cte.SR_RFP_ACCOUNT_ID)  
                                             AND approval.IS_BID_GROUP = cte.Is_BID_GROUP  
                     LEFT JOIN dbo.SR_RFP_CONTRACT_ADMIN_INITIATIVES srcai  
                                          ON srcai.SR_ACCOUNT_GROUP_ID = ISNULL(cte.SR_RFP_BID_GROUP_ID, cte.SR_RFP_ACCOUNT_ID)  
                                             AND srcai.IS_BID_GROUP = cte.Is_BID_GROUP  
                  SELECT  
                        Client_Name  
                       ,Site_Id  
                       ,Site_name  
                       ,Account_Number  
                       ,ADDRESS  
                       ,VENDOR_NAME  
                       ,rate  
                       ,CASE WHEN Is_Archived_Account = 1 THEN contracts  
                             ELSE dbo.SR_RFP_FN_GET_CONTRACT_DETAILS_FOR_CHECKLIST(Account_Id, @CurrentDt, COMMODITY_TYPE_ID)  
                        END AS contracts  
                       ,IS_NOTICE_GIVEN  
                       ,dbo.SR_SAD_FN_GET_IS_NOTICE_IMAGE_ID(Account_Id, @CurrentDt, COMMODITY_TYPE_ID) AS isNoticeImageId  
                       ,IS_CONTRACT_REVIEWED  
                       ,RFP_INITIATED_DATE  
                       ,LP_SENT_TO_CLIENT_DATE  
                       ,LP_APPROVED_DATE  
                       ,cbmsImageId  
                       ,RFP_SENT_DATE  
                       ,IS_RC_COMPLETED  
                       ,IS_CLIENT_CREDIT  
                       ,IS_SUPPLIER_CREDIT  
                       ,IS_SUPPLIER_DOCS  
                       ,IS_MANAGER_APPROVED  
                       ,SENT_RECO_DATE  
                       ,IS_CLIENT_APPROVED  
                       ,IS_CONTRACT_ADMIN_INITIATED  
                       ,newSupplier  
                       ,CONTRACT_START_DATE  
                       ,CONTRACT_END_DATE  
                       ,UTILITY_SWITCH_DEADLINE_DATE  
                       ,UTILITY_SWITCH_SUPPLIER_DATE  
                       ,IS_SWITCH_NOTICE_GIVEN  
                       ,IS_VERIFY_FLOW  
                       ,newContract  
                       ,SA_SAVINGS  
                       ,isNA  
                       ,rcDocId  
                       ,rcContentType  
                       ,iscommercialAcc  
                       ,contractDocId  
                       ,contractContentType  
                       ,rcRateCompImageId  
                       ,rateContentType  
                       ,tariff_transport  
                       ,Is_Broker_Account  
                       ,Total  
                       ,Meter_Numbers  
                       ,Address_List  
                       ,Meter_States_List  
                       ,Meter_Country  
                       ,DUE_DATE  
                       ,MANAGER_APPROVAL_DATE  
                       ,Contract_Signed_DATE  
                       ,GROUP_NAME AS Bid_Group  
                       ,Alternate_Account_Number
                  FROM  
                        #Sr_Rfp_Check_List_Indexed cte  
                  WHERE  
                        Row_Num BETWEEN @StartIndex AND @EndIndex         
            
            END          
          
END;  
  
;  
  
  
;  
  
  
  
  
;
GO








GRANT EXECUTE ON  [dbo].[SR_RFP_GET_RFP_CHECKLIST_DATA_P] TO [CBMSApplication]
GO
