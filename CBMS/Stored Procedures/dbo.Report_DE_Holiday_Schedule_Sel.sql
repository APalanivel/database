SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
NAME:              
              
  dbo.[Report_DE_Holiday_Schedule_Sel]          
               
 DESCRIPTION:  To fetch holiday and schedule data
 INPUT PARAMETERS:              
Name    DataType  Default  Description              
------------------------------------------------------------              
  
  
OUTPUT PARAMETERS:              
Name    DataType  Default  Description              
------------------------------------------------------------    
            
 USAGE EXAMPLES:              
------------------------------------------------------------             
            
 EXEC dbo.[Report_DE_Holiday_Schedule_Sel]
             
 AUTHOR INITIALS:              
Initials Name              
-----------------------------------------------------------              
AKR      Ashok Kumar Raju
          
MODIFICATIONS               
Initials Date  Modification              
------------------------------------------------------------              
AKR      2014-09-11   Created           

******/              
              
CREATE PROCEDURE [dbo].[Report_DE_Holiday_Schedule_Sel]
AS 
BEGIN              
              
      SET NOCOUNT ON;            
      SELECT
            v.VENDOR_NAME [Vendor]
           ,ts.Schedule_Name [Schedule]
           ,com.Commodity_Name [Commodity]
           ,CONVERT(VARCHAR(2000), comm.Comment_Text) [Comment]
           ,CONVERT(NVARCHAR(2000), tst.Start_Dt, 1) [Start Date]
           ,[End Date] = CASE WHEN tst.end_Dt = '2099-12-31' THEN 'Unspecified'
                              ELSE CONVERT(NVARCHAR(2000), tst.End_Dt, 1)
                         END
           ,hm.Holiday_Name [Holiday]
           ,CONVERT(NVARCHAR(2000), tsth.Holiday_Dt, 1) [Holiday Date]
      FROM
            dbo.VENDOR v
            INNER JOIN dbo.Time_Of_Use_Schedule ts
                  ON ts.VENDOR_ID = v.VENDOR_ID
            LEFT JOIN dbo.Comment comm
                  ON comm.Comment_ID = ts.Comment_ID
            LEFT JOIN dbo.Commodity com
                  ON com.Commodity_Id = ts.Commodity_Id
            LEFT JOIN dbo.Time_Of_Use_Schedule_Term tst
                  ON tst.Time_Of_Use_Schedule_Id = ts.Time_Of_Use_Schedule_Id
            LEFT JOIN dbo.Time_Of_Use_Schedule_Term_Holiday tsth
                  ON tsth.Time_Of_Use_Schedule_Term_Id = tst.Time_Of_Use_Schedule_Term_Id
            LEFT JOIN dbo.Holiday_Master hm
                  ON hm.Holiday_Master_Id = tsth.Holiday_Master_Id
      GROUP BY
            v.VENDOR_NAME
           ,ts.Schedule_Name
           ,com.Commodity_Name
           ,comm.Comment_Text
           ,tst.Start_Dt
           ,tst.End_Dt
           ,hm.Holiday_Name
           ,tsth.Holiday_Dt
      ORDER BY
            v.VENDOR_NAME
           ,ts.Schedule_Name
           ,Holiday_Name
             
END;  

;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Holiday_Schedule_Sel] TO [CBMS_SSRS_Reports]
GO
