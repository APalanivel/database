SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.DMO_Config_Make_Not_Applicable
           
DESCRIPTION:             
			To mark a DMO configuration not applicable to lower hier level entities
			
INPUT PARAMETERS:            
	Name						DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Client_Hier_DMO_Config_Id	INT
    @User_Info_Id				INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT TOP 10 * FROM dbo.Client_Hier_DMO_Config
            
	BEGIN TRANSACTION
		SELECT count(1) FROM dbo.Account_Not_Applicable_DMO_Config
		SELECT count(1) FROM dbo.Client_Hier_Not_Applicable_DMO_Config
		EXEC dbo.DMO_Config_Make_Not_Applicable 24,16
		SELECT count(1) FROM dbo.Account_Not_Applicable_DMO_Config
		SELECT count(1) FROM dbo.Client_Hier_Not_Applicable_DMO_Config
	ROLLBACK TRANSACTION
	
	
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-01-31	Contract placeholder - CP-4 Created
******/

CREATE PROCEDURE [dbo].[DMO_Config_Make_Not_Applicable]
      ( 
       @Client_Hier_DMO_Config_Id INT
      ,@User_Info_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE
            @Client_Id INT
           ,@Sitegroup_Id INT
           ,@Site_Id INT
           ,@Commodity_Id INT
           ,@DMO_Start_Dt DATE
           ,@DMO_End_Dt DATE
           
      SELECT
            @Client_Id = NULLIF(ch.Client_Id, 0)
           ,@Sitegroup_Id = NULLIF(ch.Sitegroup_Id, 0)
           ,@Site_Id = NULLIF(ch.Site_Id, 0)
           ,@Commodity_Id = chdc.Commodity_Id
           ,@DMO_Start_Dt = chdc.DMO_Start_Dt
           ,@DMO_End_Dt = chdc.DMO_End_Dt
      FROM
            Core.Client_Hier ch
            INNER JOIN dbo.Client_Hier_DMO_Config chdc
                  ON ch.Client_Hier_Id = chdc.Client_Hier_Id
      WHERE
            chdc.Client_Hier_DMO_Config_Id = @Client_Hier_DMO_Config_Id
      
      --DELETE FROM
      --      dbo.Client_Hier_Not_Applicable_DMO_Config
      --WHERE
      --      Client_Hier_DMO_Config_Id = @Client_Hier_DMO_Config_Id
      --DELETE FROM
      --      dbo.Account_Not_Applicable_DMO_Config
      --WHERE
      --      Client_Hier_DMO_Config_Id = @Client_Hier_DMO_Config_Id
            
--> Client level overlaps with existing division level config
      INSERT      INTO dbo.Client_Hier_Not_Applicable_DMO_Config
                  ( 
                   Client_Hier_Id
                  ,Client_Hier_DMO_Config_Id
                  ,Created_User_Id
                  ,Created_Ts )
                  SELECT
                        ch.Client_Hier_Id
                       ,@Client_Hier_DMO_Config_Id
                       ,@User_Info_Id
                       ,GETDATE()
                  FROM
                        dbo.Client_Hier_DMO_Config chdc
                        INNER JOIN Core.Client_Hier ch
                              ON chdc.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        chdc.Commodity_Id = @Commodity_Id
                        AND @Client_Id IS NOT NULL
                        AND ch.Client_Id = @Client_Id
                        AND @Sitegroup_Id IS NULL
                        AND ch.Sitegroup_Id > 0
                        AND ch.Site_Id = 0
                        AND ( ( @DMO_End_Dt IS NULL
                                AND chdc.DMO_End_Dt IS NULL )
                              OR ( @DMO_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NULL
                                   AND ( @DMO_Start_Dt >= chdc.DMO_Start_Dt
                                         OR @DMO_End_Dt >= chdc.DMO_Start_Dt ) )
                              OR ( @DMO_End_Dt IS NULL
                                   AND chdc.DMO_End_Dt IS NOT NULL
                                   AND ( chdc.DMO_Start_Dt >= @DMO_Start_Dt
                                         OR chdc.DMO_End_Dt >= @DMO_Start_Dt ) )
                              OR ( @DMO_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NOT NULL
                                   AND ( @DMO_Start_Dt BETWEEN chdc.DMO_Start_Dt
                                                       AND     chdc.DMO_End_Dt
                                         OR @DMO_End_Dt BETWEEN chdc.DMO_Start_Dt
                                                        AND     chdc.DMO_End_Dt
                                         OR chdc.DMO_Start_Dt BETWEEN @DMO_Start_Dt
                                                              AND     @DMO_End_Dt
                                         OR chdc.DMO_End_Dt BETWEEN @DMO_Start_Dt
                                                            AND     @DMO_End_Dt ) ) )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Client_Hier_Not_Applicable_DMO_Config na
                                         WHERE
                                          na.Client_Hier_Id = ch.Client_Hier_Id
                                          AND na.Client_Hier_DMO_Config_Id = @Client_Hier_DMO_Config_Id )
            
--> Client level overlaps with existing site level config
      INSERT      INTO dbo.Client_Hier_Not_Applicable_DMO_Config
                  ( 
                   Client_Hier_Id
                  ,Client_Hier_DMO_Config_Id
                  ,Created_User_Id
                  ,Created_Ts )
                  SELECT
                        ch.Client_Hier_Id
                       ,@Client_Hier_DMO_Config_Id
                       ,@User_Info_Id
                       ,GETDATE()
                  FROM
                        dbo.Client_Hier_DMO_Config chdc
                        INNER JOIN Core.Client_Hier ch
                              ON chdc.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        chdc.Commodity_Id = @Commodity_Id
                        AND @Client_Id IS NOT NULL
                        AND ch.Client_Id = @Client_Id
                        AND @Sitegroup_Id IS NULL
                        AND ch.Sitegroup_Id > 0
                        AND @Site_Id IS NULL
                        AND ch.Site_Id > 0
                        AND ( ( @DMO_End_Dt IS NULL
                                AND chdc.DMO_End_Dt IS NULL )
                              OR ( @DMO_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NULL
                                   AND ( @DMO_Start_Dt >= chdc.DMO_Start_Dt
                                         OR @DMO_End_Dt >= chdc.DMO_Start_Dt ) )
                              OR ( @DMO_End_Dt IS NULL
                                   AND chdc.DMO_End_Dt IS NOT NULL
                                   AND ( chdc.DMO_Start_Dt >= @DMO_Start_Dt
                                         OR chdc.DMO_End_Dt >= @DMO_Start_Dt ) )
                              OR ( @DMO_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NOT NULL
                                   AND ( @DMO_Start_Dt BETWEEN chdc.DMO_Start_Dt
                                                       AND     chdc.DMO_End_Dt
                                         OR @DMO_End_Dt BETWEEN chdc.DMO_Start_Dt
                                                        AND     chdc.DMO_End_Dt
                                         OR chdc.DMO_Start_Dt BETWEEN @DMO_Start_Dt
                                                              AND     @DMO_End_Dt
                                         OR chdc.DMO_End_Dt BETWEEN @DMO_Start_Dt
                                                            AND     @DMO_End_Dt ) ) )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Client_Hier_Not_Applicable_DMO_Config na
                                         WHERE
                                          na.Client_Hier_Id = ch.Client_Hier_Id
                                          AND na.Client_Hier_DMO_Config_Id = @Client_Hier_DMO_Config_Id )
                                                            

--> Client level overlaps with existing account level config
      INSERT      INTO dbo.Account_Not_Applicable_DMO_Config
                  ( 
                   Client_Hier_Id
                  ,Account_Id
                  ,Client_Hier_DMO_Config_Id
                  ,Created_User_Id
                  ,Created_Ts )
                  SELECT
                        ch.Client_Hier_Id
                       ,cha.Account_Id
                       ,@Client_Hier_DMO_Config_Id
                       ,@User_Info_Id
                       ,GETDATE()
                  FROM
                        dbo.Account_DMO_Config adc
                        INNER JOIN Core.Client_Hier_Account cha
                              ON adc.Account_Id = cha.Account_Id
                        INNER JOIN Core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        adc.Commodity_Id = @Commodity_Id
                        AND @Client_Id IS NOT NULL
                        AND ch.Client_Id = @Client_Id
                        AND @Sitegroup_Id IS NULL
                        AND ch.Sitegroup_Id > 0
                        AND @Site_Id IS NULL
                        AND ch.Site_Id > 0
                        AND ( ( @DMO_End_Dt IS NULL
                                AND adc.DMO_End_Dt IS NULL )
                              OR ( @DMO_End_Dt IS NOT NULL
                                   AND adc.DMO_End_Dt IS NULL
                                   AND ( @DMO_Start_Dt >= adc.DMO_Start_Dt
                                         OR @DMO_End_Dt >= adc.DMO_Start_Dt ) )
                              OR ( @DMO_End_Dt IS NULL
                                   AND adc.DMO_End_Dt IS NOT NULL
                                   AND ( adc.DMO_Start_Dt >= @DMO_Start_Dt
                                         OR adc.DMO_End_Dt >= @DMO_Start_Dt ) )
                              OR ( @DMO_End_Dt IS NOT NULL
                                   AND adc.DMO_End_Dt IS NOT NULL
                                   AND ( @DMO_Start_Dt BETWEEN adc.DMO_Start_Dt
                                                       AND     adc.DMO_End_Dt
                                         OR @DMO_End_Dt BETWEEN adc.DMO_Start_Dt
                                                        AND     adc.DMO_End_Dt
                                         OR adc.DMO_Start_Dt BETWEEN @DMO_Start_Dt
                                                             AND     @DMO_End_Dt
                                         OR adc.DMO_End_Dt BETWEEN @DMO_Start_Dt
                                                           AND     @DMO_End_Dt ) ) )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Account_Not_Applicable_DMO_Config na
                                         WHERE
                                          na.Client_Hier_Id = ch.Client_Hier_Id
                                          AND na.Account_Id = cha.Account_Id
                                          AND na.Client_Hier_DMO_Config_Id = @Client_Hier_DMO_Config_Id )
                  GROUP BY
                        ch.Client_Hier_Id
                       ,cha.Account_Id
            
--> Division level overlaps with existing site level config
      INSERT      INTO dbo.Client_Hier_Not_Applicable_DMO_Config
                  ( 
                   Client_Hier_Id
                  ,Client_Hier_DMO_Config_Id
                  ,Created_User_Id
                  ,Created_Ts )
                  SELECT
                        ch.Client_Hier_Id
                       ,@Client_Hier_DMO_Config_Id
                       ,@User_Info_Id
                       ,GETDATE()
                  FROM
                        dbo.Client_Hier_DMO_Config chdc
                        INNER JOIN Core.Client_Hier ch
                              ON chdc.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        chdc.Commodity_Id = @Commodity_Id
                        AND @Client_Id IS NOT NULL
                        AND ch.Client_Id = @Client_Id
                        AND @Sitegroup_Id IS NOT NULL
                        AND ch.Sitegroup_Id = @Sitegroup_Id
                        AND @Site_Id IS NULL
                        AND ch.Site_Id > 0
                        AND ( ( @DMO_End_Dt IS NULL
                                AND chdc.DMO_End_Dt IS NULL )
                              OR ( @DMO_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NULL
                                   AND ( @DMO_Start_Dt >= chdc.DMO_Start_Dt
                                         OR @DMO_End_Dt >= chdc.DMO_Start_Dt ) )
                              OR ( @DMO_End_Dt IS NULL
                                   AND chdc.DMO_End_Dt IS NOT NULL
                                   AND ( chdc.DMO_Start_Dt >= @DMO_Start_Dt
                                         OR chdc.DMO_End_Dt >= @DMO_Start_Dt ) )
                              OR ( @DMO_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NOT NULL
                                   AND ( @DMO_Start_Dt BETWEEN chdc.DMO_Start_Dt
                                                       AND     chdc.DMO_End_Dt
                                         OR @DMO_End_Dt BETWEEN chdc.DMO_Start_Dt
                                                        AND     chdc.DMO_End_Dt
                                         OR chdc.DMO_Start_Dt BETWEEN @DMO_Start_Dt
                                                              AND     @DMO_End_Dt
                                         OR chdc.DMO_End_Dt BETWEEN @DMO_Start_Dt
                                                            AND     @DMO_End_Dt ) ) )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Client_Hier_Not_Applicable_DMO_Config na
                                         WHERE
                                          na.Client_Hier_Id = ch.Client_Hier_Id
                                          AND na.Client_Hier_DMO_Config_Id = @Client_Hier_DMO_Config_Id )
                                                            
--> Division level overlaps with existing account level config
      INSERT      INTO dbo.Account_Not_Applicable_DMO_Config
                  ( 
                   Client_Hier_Id
                  ,Account_Id
                  ,Client_Hier_DMO_Config_Id
                  ,Created_User_Id
                  ,Created_Ts )
                  SELECT
                        ch.Client_Hier_Id
                       ,cha.Account_Id
                       ,@Client_Hier_DMO_Config_Id
                       ,@User_Info_Id
                       ,GETDATE()
                  FROM
                        dbo.Account_DMO_Config adc
                        INNER JOIN Core.Client_Hier_Account cha
                              ON adc.Account_Id = cha.Account_Id
                        INNER JOIN Core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        adc.Commodity_Id = @Commodity_Id
                        AND @Client_Id IS NOT NULL
                        AND ch.Client_Id = @Client_Id
                        AND @Sitegroup_Id IS NOT NULL
                        AND ch.Sitegroup_Id = @Sitegroup_Id
                        AND @Site_Id IS NULL
                        AND ch.Site_Id > 0
                        AND ( ( @DMO_End_Dt IS NULL
                                AND adc.DMO_End_Dt IS NULL )
                              OR ( @DMO_End_Dt IS NOT NULL
                                   AND adc.DMO_End_Dt IS NULL
                                   AND ( @DMO_Start_Dt >= adc.DMO_Start_Dt
                                         OR @DMO_End_Dt >= adc.DMO_Start_Dt ) )
                              OR ( @DMO_End_Dt IS NULL
                                   AND adc.DMO_End_Dt IS NOT NULL
                                   AND ( adc.DMO_Start_Dt >= @DMO_Start_Dt
                                         OR adc.DMO_End_Dt >= @DMO_Start_Dt ) )
                              OR ( @DMO_End_Dt IS NOT NULL
                                   AND adc.DMO_End_Dt IS NOT NULL
                                   AND ( @DMO_Start_Dt BETWEEN adc.DMO_Start_Dt
                                                       AND     adc.DMO_End_Dt
                                         OR @DMO_End_Dt BETWEEN adc.DMO_Start_Dt
                                                        AND     adc.DMO_End_Dt
                                         OR adc.DMO_Start_Dt BETWEEN @DMO_Start_Dt
                                                             AND     @DMO_End_Dt
                                         OR adc.DMO_End_Dt BETWEEN @DMO_Start_Dt
                                                           AND     @DMO_End_Dt ) ) )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Account_Not_Applicable_DMO_Config na
                                         WHERE
                                          na.Client_Hier_Id = ch.Client_Hier_Id
                                          AND na.Account_Id = cha.Account_Id
                                          AND na.Client_Hier_DMO_Config_Id = @Client_Hier_DMO_Config_Id )
                  GROUP BY
                        ch.Client_Hier_Id
                       ,cha.Account_Id
                                          
 --> Site level overlaps with existing account level config
      INSERT      INTO dbo.Account_Not_Applicable_DMO_Config
                  ( 
                   Client_Hier_Id
                  ,Account_Id
                  ,Client_Hier_DMO_Config_Id
                  ,Created_User_Id
                  ,Created_Ts )
                  SELECT
                        ch.Client_Hier_Id
                       ,cha.Account_Id
                       ,@Client_Hier_DMO_Config_Id
                       ,@User_Info_Id
                       ,GETDATE()
                  FROM
                        dbo.Account_DMO_Config adc
                        INNER JOIN Core.Client_Hier_Account cha
                              ON adc.Account_Id = cha.Account_Id
                        INNER JOIN Core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        adc.Commodity_Id = @Commodity_Id
                        AND @Client_Id IS NOT NULL
                        AND ch.Client_Id = @Client_Id
                        AND @Sitegroup_Id IS NOT NULL
                        AND ch.Sitegroup_Id = @Sitegroup_Id
                        AND @Site_Id IS NOT NULL
                        AND ch.Site_Id = @Site_Id
                        AND ( ( @DMO_End_Dt IS NULL
                                AND adc.DMO_End_Dt IS NULL )
                              OR ( @DMO_End_Dt IS NOT NULL
                                   AND adc.DMO_End_Dt IS NULL
                                   AND ( @DMO_Start_Dt >= adc.DMO_Start_Dt
                                         OR @DMO_End_Dt >= adc.DMO_Start_Dt ) )
                              OR ( @DMO_End_Dt IS NULL
                                   AND adc.DMO_End_Dt IS NOT NULL
                                   AND ( adc.DMO_Start_Dt >= @DMO_Start_Dt
                                         OR adc.DMO_End_Dt >= @DMO_Start_Dt ) )
                              OR ( @DMO_End_Dt IS NOT NULL
                                   AND adc.DMO_End_Dt IS NOT NULL
                                   AND ( @DMO_Start_Dt BETWEEN adc.DMO_Start_Dt
                                                       AND     adc.DMO_End_Dt
                                         OR @DMO_End_Dt BETWEEN adc.DMO_Start_Dt
                                                        AND     adc.DMO_End_Dt
                                         OR adc.DMO_Start_Dt BETWEEN @DMO_Start_Dt
                                                             AND     @DMO_End_Dt
                                         OR adc.DMO_End_Dt BETWEEN @DMO_Start_Dt
                                                           AND     @DMO_End_Dt ) ) )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Account_Not_Applicable_DMO_Config na
                                         WHERE
                                          na.Client_Hier_Id = ch.Client_Hier_Id
                                          AND na.Account_Id = cha.Account_Id
                                          AND na.Client_Hier_DMO_Config_Id = @Client_Hier_DMO_Config_Id )
                  GROUP BY
                        ch.Client_Hier_Id
                       ,cha.Account_Id     
      
      
END;
;


;
GO
GRANT EXECUTE ON  [dbo].[DMO_Config_Make_Not_Applicable] TO [CBMSApplication]
GO
