SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.RM_FORECAST_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@forecastFromDate	datetime  	          	
	@forecastAsOfDate	datetime  	          	
	@forecastType  	int       	          	
	@forecastTypeName	varchar(200)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2019-11-29	RM-Budgets Enahancement - Modified to return @Forecast_Id in result set


******/

CREATE PROCEDURE [dbo].[RM_FORECAST_P]
    (
        @userId VARCHAR(10)
        , @sessionId VARCHAR(20)
        , @forecastFromDate DATETIME
        , @forecastAsOfDate DATETIME
        , @forecastType INT
        , @forecastTypeName VARCHAR(200)
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @entityId INT
            , @toDate DATETIME
            , @Forecast_Id INT;

        SELECT
            @entityId = ENTITY_ID
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_TYPE = @forecastType
            AND ENTITY_NAME = @forecastTypeName;

        INSERT INTO dbo.RM_FORECAST
             (
                 FORECAST_TYPE_ID
                 , FORECAST_FROM_DATE
                 , FORECAST_AS_OF_DATE
             )
        VALUES
            (@entityId
             , @forecastFromDate
             , @forecastAsOfDate);

        SELECT  @Forecast_Id = @@IDENTITY;

        SELECT
            @toDate = MAX(FORECAST_FROM_DATE)
        FROM
            dbo.RM_FORECAST
        WHERE
            FORECAST_FROM_DATE < @forecastFromDate
            AND FORECAST_TYPE_ID = @entityId;

        UPDATE
            dbo.RM_FORECAST
        SET
            FORECAST_TO_DATE = @forecastFromDate - 1
        WHERE
            FORECAST_FROM_DATE = @toDate;

        SELECT  @Forecast_Id AS Forecast_Id;

    END;
GO
GRANT EXECUTE ON  [dbo].[RM_FORECAST_P] TO [CBMSApplication]
GO
