SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


  
CREATE PROCEDURE [dbo].[UPDATE_CBMS_IMAGE_DETAILS_P](
	@Cbms_Image_ID INT
	, @Cbms_Image_Type_ID INT
	, @Cbms_Doc_ID VARCHAR(200)
	, @Date_Imaged DATETIME
	, @Cbms_Image_Size DECIMAL(18,0)
	, @Content_Type VARCHAR(200)
	, @CBMS_Image_Directory VARCHAR(255)
	, @CBMS_Image_FileName VARCHAR(255)
	, @CBMS_Image_Location_Id INT )
	
AS
BEGIN
  
	SET NOCOUNT ON ;

	UPDATE dbo.CBMS_IMAGE
	SET CBMS_IMAGE_TYPE_ID = @Cbms_Image_Type_ID,
		CBMS_DOC_ID = @Cbms_Doc_ID,
		DATE_IMAGED = @Date_Imaged,
		CBMS_IMAGE_SIZE = @Cbms_Image_Size,
		CONTENT_TYPE = @Content_Type,
		CBMS_Image_Directory = @CBMS_Image_Directory,
		CBMS_Image_FileName = @CBMS_Image_FileName ,
		CBMS_Image_Location_Id = @CBMS_Image_Location_Id		
	WHERE CBMS_IMAGE_ID = @Cbms_Image_ID
  
	RETURN @@ROWCOUNT

END

GO
GRANT EXECUTE ON  [dbo].[UPDATE_CBMS_IMAGE_DETAILS_P] TO [CBMSApplication]
GO
