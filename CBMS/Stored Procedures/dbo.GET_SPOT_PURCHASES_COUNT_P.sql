SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


--exec dbo.GET_SPOT_PURCHASES_COUNT_P '20267-0002'

CREATE PROCEDURE dbo.GET_SPOT_PURCHASES_COUNT_P
	@edContractNumber VARCHAR(150)
AS
BEGIN

	SET NOCOUNT ON

	SELECT COUNT(spot_id) AS CntSpotID
	FROM dbo.[contract] c INNER JOIN dbo.spot s ON c.contract_id = s.contract_id
		AND c.ed_contract_number = @edContractNumber

END
GO
GRANT EXECUTE ON  [dbo].[GET_SPOT_PURCHASES_COUNT_P] TO [CBMSApplication]
GO
