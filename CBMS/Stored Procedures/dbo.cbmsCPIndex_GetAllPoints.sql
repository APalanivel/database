SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsCPIndex_GetAllPoints

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

create  PROCEDURE [dbo].[cbmsCPIndex_GetAllPoints]

AS
BEGIN

	   select ci.clearport_index
		, ci.clearport_index_id
		, ci.clearport_index_symbol
             from clearport_index ci with (nolock)
	 order by ci.clearport_index_id
	

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCPIndex_GetAllPoints] TO [CBMSApplication]
GO
