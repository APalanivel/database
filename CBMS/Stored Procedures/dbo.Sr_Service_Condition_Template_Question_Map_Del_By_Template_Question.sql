SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Service_Condition_Template_Question_Map_Del_By_Template_Question]
           
DESCRIPTION:             
			To get mapped countries to SR pricing product
			
INPUT PARAMETERS:            
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  
	@Template_Name	INT				NULL
    @Commodity_Id						INT				NULL
    @Country_Id							VARCHAR(MAX)	NULL
    @SR_PRICING_PRODUCT_ID				INT				NULL


OUTPUT PARAMETERS:
	Name			DataType		Default		Description  
---------------------------------------------------------------------------------  


 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
	SELECT TOP 10 * FROM dbo.Sr_Service_Condition_Template_Category_Map ssctcm 
	INNER JOIN dbo.Sr_Service_Condition_Template_Question_Map sctqm 
	ON ssctcm.Sr_Service_Condition_Template_Category_Map_Id = sctqm.Sr_Service_Condition_Template_Category_Map_Id

BEGIN TRANSACTION
	SELECT * FROM dbo.Sr_Service_Condition_Template_Category_Map ssctcm 
		LEFT JOIN dbo.Sr_Service_Condition_Template_Question_Map sctqm 
		ON ssctcm.Sr_Service_Condition_Template_Category_Map_Id = sctqm.Sr_Service_Condition_Template_Category_Map_Id
		WHERE ssctcm.Sr_Service_Condition_Template_Id = 9 
	EXEC dbo.Sr_Service_Condition_Template_Question_Map_Del_By_Template_Question 9,8
	SELECT * FROM dbo.Sr_Service_Condition_Template_Category_Map ssctcm 
		LEFT JOIN dbo.Sr_Service_Condition_Template_Question_Map sctqm 
		ON ssctcm.Sr_Service_Condition_Template_Category_Map_Id = sctqm.Sr_Service_Condition_Template_Category_Map_Id
		WHERE ssctcm.Sr_Service_Condition_Template_Id = 9 
ROLLBACK TRANSACTION

BEGIN TRANSACTION
	SELECT * FROM dbo.Sr_Service_Condition_Template_Category_Map ssctcm 
		LEFT JOIN dbo.Sr_Service_Condition_Template_Question_Map sctqm 
		ON ssctcm.Sr_Service_Condition_Template_Category_Map_Id = sctqm.Sr_Service_Condition_Template_Category_Map_Id
		WHERE ssctcm.Sr_Service_Condition_Template_Id = 6 
	EXEC dbo.Sr_Service_Condition_Template_Question_Map_Del_By_Template_Question 6,7
	SELECT * FROM dbo.Sr_Service_Condition_Template_Category_Map ssctcm 
		LEFT JOIN dbo.Sr_Service_Condition_Template_Question_Map sctqm 
		ON ssctcm.Sr_Service_Condition_Template_Category_Map_Id = sctqm.Sr_Service_Condition_Template_Category_Map_Id
		WHERE ssctcm.Sr_Service_Condition_Template_Id = 6 
	EXEC dbo.Sr_Service_Condition_Template_Question_Map_Del_By_Template_Question 6,8
	SELECT * FROM dbo.Sr_Service_Condition_Template_Category_Map ssctcm 
		LEFT JOIN dbo.Sr_Service_Condition_Template_Question_Map sctqm 
		ON ssctcm.Sr_Service_Condition_Template_Category_Map_Id = sctqm.Sr_Service_Condition_Template_Category_Map_Id
		WHERE ssctcm.Sr_Service_Condition_Template_Id = 6 
ROLLBACK TRANSACTION

		
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-03-31	Global Sourcing - Phase3 - GCS-602 Created
******/

CREATE PROCEDURE [dbo].[Sr_Service_Condition_Template_Question_Map_Del_By_Template_Question]
      ( 
       @Sr_Service_Condition_Template_Id INT
      ,@Sr_Service_Condition_Question_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DELETE
            ssctqm
      FROM
            dbo.Sr_Service_Condition_Template_Question_Map ssctqm
            INNER JOIN dbo.Sr_Service_Condition_Template_Category_Map ssctcm
                  ON ssctqm.Sr_Service_Condition_Template_Category_Map_Id = ssctcm.Sr_Service_Condition_Template_Category_Map_Id
      WHERE
            ssctcm.Sr_Service_Condition_Template_Id = @Sr_Service_Condition_Template_Id
            AND ssctqm.Sr_Service_Condition_Question_Id = @Sr_Service_Condition_Question_Id
            
      DELETE
            ssctcm
      FROM
            dbo.Sr_Service_Condition_Template_Category_Map ssctcm
      WHERE
            ssctcm.Sr_Service_Condition_Template_Id = @Sr_Service_Condition_Template_Id
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              dbo.Sr_Service_Condition_Template_Question_Map ssctqm
                             WHERE
                              ssctqm.Sr_Service_Condition_Template_Category_Map_Id = ssctcm.Sr_Service_Condition_Template_Category_Map_Id )
            
            
END;
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Service_Condition_Template_Question_Map_Del_By_Template_Question] TO [CBMSApplication]
GO
