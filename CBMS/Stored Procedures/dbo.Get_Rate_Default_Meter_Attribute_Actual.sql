SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                      
Name: [dbo].[Get_Rate_Default_Meter_Attribute_Actual]                         
                        
Description:                                      
        To get the rate level default meter attributes for Actual.                                
                        
 Input Parameters:                                      
     Name        DataType   Default   Description                                        
----------------------------------------------------------------------------------------                                        
@Rate_Id          INT                     
                        
 Output Parameters:                                            
    Name        DataType   Default   Description                                        
----------------------------------------------------------------------------------------                                        
                        
 Usage Examples:                                          
----------------------------------------------------------------------------------------                                        
 BEGIN TRAN                          
                       
 EXEC [dbo].[Get_Rate_Default_Meter_Attribute_Actual]                   
     @Rate_Id =9367                         
                        
 ROLLBACK TRAN                             
                     
Author Initials:                                      
    Initials  Name                                      
----------------------------------------------------------------------------------------                                        
SC  Sreenivasulu Cheerala                    
                          
 Modifications:                                      
    Initials        Date  Modification                                      
----------------------------------------------------------------------------------------                                        
 SC    2020-01-24   Initial Creation                    
******/  
CREATE PROCEDURE [dbo].[Get_Rate_Default_Meter_Attribute_Actual]  
    (  
        @Rate_Id INT  
    )  
AS  
    BEGIN  
        SET NOCOUNT ON;  
  
        DECLARE @Actual_Type_Cd INT;  
  
        SELECT  
            @Actual_Type_Cd = cd.Code_Id  
        FROM  
            dbo.Code cd  
            INNER JOIN dbo.Codeset cs  
                ON cs.Codeset_Id = cd.Codeset_Id  
        WHERE  
            cs.Codeset_Name = 'MeterAttributeType'  
            AND cd.Code_Value = 'Actual';  
  
        SELECT  
            rdma.Rate_Default_Meter_Attribute_Id  
            , ema.EC_Meter_Attribute_Id  
            , ema.EC_Meter_Attribute_Name  
            , rdma.EC_Meter_Attribute_Value  
            , cd.Code_Value Attribute_Type  
            , rdma.Start_Dt  
            , rdma.End_Dt  
            , ema.Vendor_Type_Cd  
            , rdma.Attribute_Value_Type_Cd Meter_Attribute_Type_Cd  
            , metatri.Code_Value Meter_Attribute_Name  
            , VenCd.Code_Value Vendor_Type_Value  
            , rdma.Attribute_Value_Requirement_Option_Cd  
            , Cdd.Code_Value Attribute_Value_Requirement_Option_Name  
            , rdma.RATE_ID  
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME Updated_User_Name  
            , rdma.Last_Change_Ts Last_Cahnge_Ts  
        FROM  
            Budget.Rate_Default_Meter_Attribute rdma  
            INNER JOIN dbo.RATE r  
                ON r.RATE_ID = rdma.RATE_ID  
            INNER JOIN dbo.Code Cdd  
                ON Cdd.Code_Id = rdma.Attribute_Value_Requirement_Option_Cd  
            INNER JOIN dbo.EC_Meter_Attribute ema  
                ON ema.EC_Meter_Attribute_Id = rdma.EC_Meter_Attribute_Id  
            INNER JOIN dbo.Code cd  
                ON cd.Code_Id = ema.Attribute_Type_Cd  
            INNER JOIN dbo.USER_INFO ui  
                ON ui.USER_INFO_ID = rdma.Updated_User_Id  
            --LEFT JOIN dbo.EC_Meter_Attribute_Tracking emat      
            --    ON rdma.Attribute_Value_Type_Cd = emat.Meter_Attribute_Type_Cd      
            --       AND  ema.EC_Meter_Attribute_Id = emat.EC_Meter_Attribute_Id      
            LEFT JOIN dbo.Code VenCd  
                ON VenCd.Code_Id = ema.Vendor_Type_Cd  
            LEFT JOIN dbo.Code metatri  
                ON metatri.Code_Id = rdma.Attribute_Value_Type_Cd  
        WHERE  
            rdma.Attribute_Value_Type_Cd = @Actual_Type_Cd  
            AND rdma.RATE_ID = @Rate_Id  
        GROUP BY  
            rdma.Rate_Default_Meter_Attribute_Id  
            , ema.EC_Meter_Attribute_Id  
            , ema.EC_Meter_Attribute_Name  
            , rdma.EC_Meter_Attribute_Value  
            , cd.Code_Value  
            , rdma.Start_Dt  
            , rdma.End_Dt  
            , ema.Vendor_Type_Cd  
            , rdma.Attribute_Value_Type_Cd  
            , metatri.Code_Value  
            , VenCd.Code_Value  
            , rdma.Attribute_Value_Requirement_Option_Cd  
            , Cdd.Code_Value  
            , rdma.RATE_ID  
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME  
            , rdma.Last_Change_Ts
			ORDER BY ema.EC_Meter_Attribute_Id 
    END;
GO
GRANT EXECUTE ON  [dbo].[Get_Rate_Default_Meter_Attribute_Actual] TO [CBMSApplication]
GO
