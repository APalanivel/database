SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  Procedure [dbo].[seContentType_GetAll]
As
BEGIN
	set nocount on
	 select ContentTypeId
				, TypeLabel
				, TypeCode
				, SortOrder
		 from seContentType
		order by TypeLabel

END
GO
GRANT EXECUTE ON  [dbo].[seContentType_GetAll] TO [CBMSApplication]
GO
