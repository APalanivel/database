SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
          
 NAME: dbo.App_Access_Role_Sel_By_Client          
          
 DESCRIPTION:           
		 To get the all roles from client_app_access_role table.          
          
 INPUT PARAMETERS:            
 Name                DataType         Default         Description            
---------------------------------------------------------------------------------------------------------------          
 @Client_Id          INT       
           
 OUTPUT PARAMETERS:            
          
 Name                DataType         Default         Description          
---------------------------------------------------------------------------------------------------------------          
           
 USAGE EXAMPLES:            
---------------------------------------------------------------------------------------------------------------          
           
		 EXEC dbo.App_Access_Role_Sel_By_Client 110         
           
           
 AUTHOR INITIALS:            
 Initials           Name            
---------------------------------------------------------------------------------------------------------------          
 NR                 NARAYANA REDDY          
           
           
 MODIFICATIONS             
 Initials           Date            Modification          
---------------------------------------------------------------------------------------------------------------          
 NR                 2013-12-13      Created for RA Admin user management          
           
******/  
        
CREATE PROCEDURE [dbo].[App_Access_Role_Sel_By_Client] ( @Client_Id INT )
AS 
BEGIN          
      SET NOCOUNT ON;          
           
      SELECT
            Client_App_Access_Role_Id
           ,App_Access_Role_Name
      FROM
            dbo.Client_App_Access_Role
      WHERE
            Client_Id = @Client_Id       
            
END 


;
GO
GRANT EXECUTE ON  [dbo].[App_Access_Role_Sel_By_Client] TO [CBMSApplication]
GO
