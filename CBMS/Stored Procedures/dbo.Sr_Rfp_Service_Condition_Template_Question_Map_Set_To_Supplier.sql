SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Rfp_Service_Condition_Template_Question_Map_Set_To_Supplier]
           
DESCRIPTION:             
			To set service condition questions post to supplier
			
INPUT PARAMETERS:            
	Name								DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Selected_Pricing_Product_Id INT
    @SR_RFP_ACCOUNT_TERM_ID INT
    @Sr_Service_Condition_Question_Ids	VARCHAR(MAX) NULL
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
	EXEC dbo.Sr_Service_Condition_Template_Question_Sel_By_Product_Account_Term  1211471 ,109834 
	
	BEGIN TRANSACTION	
		SELECT * FROM dbo.Sr_Rfp_Service_Condition_Template_Question_Map 
			WHERE SR_RFP_SELECTED_PRODUCTS_ID = 1211471 AND SR_RFP_ACCOUNT_TERM_ID = 109834
		EXEC dbo.Sr_Rfp_Service_Condition_Template_Question_Map_Set_To_Supplier 1211471 ,109834 ,'126,127,128,129',49
		SELECT * FROM dbo.Sr_Rfp_Service_Condition_Template_Question_Map 
			WHERE SR_RFP_SELECTED_PRODUCTS_ID = 1211471 AND SR_RFP_ACCOUNT_TERM_ID = 109834 AND Is_Post_To_Supplier = 1
	ROLLBACK TRANSACTION

		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-04-01	Global Sourcing - Phase3 - GCS-478 Created
******/
CREATE PROCEDURE [dbo].[Sr_Rfp_Service_Condition_Template_Question_Map_Set_To_Supplier]
      ( 
       @Selected_Pricing_Product_Id INT
      ,@SR_RFP_ACCOUNT_TERM_ID INT
      ,@Sr_Service_Condition_Question_Ids VARCHAR(MAX) = NULL
      ,@User_Info_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      UPDATE
            tqm
      SET   
            tqm.Is_Post_To_Supplier = 1
      FROM
            dbo.Sr_Rfp_Service_Condition_Template_Question_Map tqm
      WHERE
            tqm.SR_RFP_SELECTED_PRODUCTS_ID = @Selected_Pricing_Product_Id
            AND tqm.SR_RFP_ACCOUNT_TERM_ID = @SR_RFP_ACCOUNT_TERM_ID
            AND EXISTS ( SELECT
                              1
                         FROM
                              dbo.ufn_split(@Sr_Service_Condition_Question_Ids, ',')
                         WHERE
                              cast(Segments AS INT) = tqm.Sr_Service_Condition_Question_Id )
                        
      UPDATE
            tqm
      SET   
            tqm.Is_Post_To_Supplier = 0
      FROM
            dbo.Sr_Rfp_Service_Condition_Template_Question_Map tqm
      WHERE
            tqm.SR_RFP_SELECTED_PRODUCTS_ID = @Selected_Pricing_Product_Id
            AND tqm.SR_RFP_ACCOUNT_TERM_ID = @SR_RFP_ACCOUNT_TERM_ID
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              dbo.ufn_split(@Sr_Service_Condition_Question_Ids, ',')
                             WHERE
                              cast(Segments AS INT) = tqm.Sr_Service_Condition_Question_Id )
END;
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Service_Condition_Template_Question_Map_Set_To_Supplier] TO [CBMSApplication]
GO
