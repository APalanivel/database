SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Cu_Invoice_Sel_For_Contract]  
     
DESCRIPTION: 
	To Get Invoice Details for Selected Contract Id.
      
INPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
	@Contract_Id	INT
	@StartIndex		INT			1			
	@EndIndex		INT			2147483647
                
OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------

	EXEC Cu_Invoice_Sel_For_Contract  75767,1,10
	
	EXEC Cu_Invoice_Sel_For_Contract  60460,1,10

	EXEC Cu_Invoice_Sel_For_Contract  118269


	
AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH
	NR			Narayana Reddy

MODIFICATIONS:
	INITIALS	DATE			MODIFICATION
------------------------------------------------------------
	PNR			04-June-10		CREATED
	NR			2019-12-16		MAINT-9670 - Get suplier & consolidated  invoice list assoicated with the contract. 
	NR			2020-01-29		MAINT-9782 - Added IS_PROCESSED flag in Temp table.                       

*/

CREATE PROCEDURE [dbo].[Cu_Invoice_Sel_For_Contract]
    (
        @Contract_Id INT
        , @StartIndex INT = 1
        , @EndIndex INT = 2147483647
    )
AS
    BEGIN

        SET NOCOUNT ON;


        CREATE TABLE #Invoice_List
             (
                 Cu_Invoice_Id INT
                 , Account_Id INT
                 , CBMS_IMAGE_ID INT
                 , Begin_Dt DATE
                 , End_Dt DATE
                 , IS_PROCESSED BIT
             );

        -- > To save the contract associated list for Supplier & consoldidated.

        INSERT INTO #Invoice_List
             (
                 Cu_Invoice_Id
                 , Account_Id
                 , CBMS_IMAGE_ID
                 , Begin_Dt
                 , End_Dt
                 , IS_PROCESSED
             )
        EXEC dbo.Associate_Dis_Associate_Invoice_Sel_By_Contract_Id_Account_Id
            @Contract_Id = @Contract_Id
            , @Account_Id = NULL
            , @Is_Associated_Invoice = 1
            , @Is_Dis_Associated_Invoice = 0;

        INSERT INTO #Invoice_List
             (
                 Cu_Invoice_Id
                 , Account_Id
                 , CBMS_IMAGE_ID
                 , Begin_Dt
                 , End_Dt
                 , IS_PROCESSED
             )
        EXEC dbo.Associate_Dis_Associate_Invoice_Sel_By_Contract_Id_Account_Id_For_Consolidated
            @Contract_Id = @Contract_Id
            , @Account_Id = NULL
            , @Is_Associated_Invoice = 1
            , @Is_Dis_Associated_Invoice = 0;

        -- > To save the contract  un associated list for Supplier & consoldidated.

        INSERT INTO #Invoice_List
             (
                 Cu_Invoice_Id
                 , Account_Id
                 , CBMS_IMAGE_ID
                 , Begin_Dt
                 , End_Dt
                 , IS_PROCESSED
             )
        EXEC dbo.Associate_Dis_Associate_Invoice_Sel_By_Contract_Id_Account_Id
            @Contract_Id = @Contract_Id
            , @Account_Id = NULL
            , @Is_Associated_Invoice = 0
            , @Is_Dis_Associated_Invoice = 1;

        INSERT INTO #Invoice_List
             (
                 Cu_Invoice_Id
                 , Account_Id
                 , CBMS_IMAGE_ID
                 , Begin_Dt
                 , End_Dt
                 , IS_PROCESSED
             )
        EXEC dbo.Associate_Dis_Associate_Invoice_Sel_By_Contract_Id_Account_Id_For_Consolidated
            @Contract_Id = @Contract_Id
            , @Account_Id = NULL
            , @Is_Associated_Invoice = 0
            , @Is_Dis_Associated_Invoice = 1;



        WITH Cte_Invoice_List
        AS (
               SELECT
                    cuism.CU_INVOICE_ID
                    , cuism.SERVICE_MONTH
                    , Row_Num = ROW_NUMBER() OVER (ORDER BY
                                                       cuism.CU_INVOICE_ID)
                    , Total_Rows = COUNT(1) OVER ()
               FROM
                    #Invoice_List sam
                    JOIN dbo.CU_INVOICE_SERVICE_MONTH cuism
                        ON cuism.CU_INVOICE_ID = sam.Cu_Invoice_Id
               GROUP BY
                   cuism.CU_INVOICE_ID
                   , cuism.SERVICE_MONTH
           )
        SELECT
            ci.CU_INVOICE_ID
            , ci.BEGIN_DATE
            , ci.END_DATE
            , cte_ci.SERVICE_MONTH
            , ci.IS_PROCESSED
            , ci.IS_DUPLICATE
            , ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME AS Queue_User_Name
            , cte_ci.Total_Rows
        FROM
            Cte_Invoice_List cte_ci
            JOIN dbo.CU_INVOICE ci
                ON cte_ci.CU_INVOICE_ID = ci.CU_INVOICE_ID
            LEFT JOIN(dbo.CU_EXCEPTION_DENORM cud
                      JOIN dbo.USER_INFO ui
                          ON ui.QUEUE_ID = cud.QUEUE_ID)
                ON cud.CU_INVOICE_ID = ci.CU_INVOICE_ID
        WHERE
            cte_ci.Row_Num BETWEEN @StartIndex
                           AND     @EndIndex;

    END;


GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Sel_For_Contract] TO [CBMSApplication]
GO
