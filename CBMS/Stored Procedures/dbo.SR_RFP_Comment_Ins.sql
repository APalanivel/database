SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[SR_RFP_Comment_Ins]
           
DESCRIPTION:             
			
			
INPUT PARAMETERS:            
	Name					DataType		Default		Description  
---------------------------------------------------------------------------------  
	@SR_RFP_Id				INT
    @User_Info_Id			INT
    @Comment_Text			VARCHAR(MAX)


OUTPUT PARAMETERS:
	Name			DataType		Default		Description  
---------------------------------------------------------------------------------  


 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
	EXEC dbo.CODE_SEL_BY_CodeSet_Name @CodeSet_Name = 'CommentType', @Code_Value = 'RFPComment'
	SELECT TOP 10 * FROM dbo.SR_RFP

	BEGIN TRANSACTION
		SELECT * FROM dbo.SR_RFP_Comment a INNER JOIN dbo.Comment b ON a.Comment_Id = b.Comment_ID
			WHERE a.SR_RFP_Id = 5
		EXEC dbo.SR_RFP_Comment_Ins 5, 16, 'Test_SR_RFP_Comment_Ins_Test'
		SELECT * FROM dbo.SR_RFP_Comment a INNER JOIN dbo.Comment b ON a.Comment_Id = b.Comment_ID
			WHERE a.SR_RFP_Id = 5
	ROLLBACK TRANSACTION


 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2015-10-15	Global Sourcing - Phase2 -Created
								
******/
CREATE PROCEDURE [dbo].[SR_RFP_Comment_Ins]
      ( 
       @SR_RFP_Id INT
      ,@User_Info_Id INT
      ,@Comment_Text VARCHAR(MAX) )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE
            @Comment_Id INT
           ,@Comment_Type_CD INT
          
      SELECT
            @Comment_Type_CD = cd.Code_Id
      FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'CommentType'
            AND cd.Code_Value = 'RFPComment'
        
      EXEC dbo.Comment_Ins 
            @Comment_Type_CD = @Comment_Type_CD
           ,@Comment_User_Info_Id = @User_Info_Id
           ,@Comment_Dt = NULL
           ,@Comment_Text = @Comment_Text
           ,@Comment_Id = @Comment_Id OUT 
      
      INSERT      INTO dbo.SR_RFP_Comment
                  ( SR_RFP_Id, Comment_Id )
      VALUES
                  ( @SR_RFP_Id, @Comment_Id )
                  
END;
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_Comment_Ins] TO [CBMSApplication]
GO
