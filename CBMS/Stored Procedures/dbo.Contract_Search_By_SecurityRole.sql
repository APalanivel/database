SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******

 NAME:
	dbo.Contract_Search_By_SecurityRole

 DESCRIPTION:
 INPUT PARAMETERS:
 
 Name					DataType	Default Description    
------------------------------------------------------------    
 @MyAccountId			int                       
 @MyClientId			int                       
 @Sitegroup_Id			int        null           
 @site_id				int        null           
 @site_not_managed		int        -1       0 - Managed Sites, 1- Not managed Sites and -1 for all sites
 @vendor_id				int        null           
 @contract_start_date	datetime   null  
 @contract_end_date		datetime   null  
 @ed_contract_number	varchar(25)null
 @contract_status_id	int		   -1		1- Acitve Contracts , 0 - inactive contracts and -1 for all Contracts
 @Tvp_Source			tvp_Source  READONLY 

 OUTPUT PARAMETERS:
 Name					DataType	Default Description    
------------------------------------------------------------
  
 USAGE EXAMPLES:  
------------------------------------------------------------  
 sp_recompile Contract_Search_By_Client_Hier_Role  

 
 EXEC [dbo].[Contract_Search_By_SecurityRole] 
      @Security_Role_Id = 99
     ,@sitegroup_id = NULL
     ,@site_id = NULL
     ,@site_not_managed = 0
     ,@vendor_id = NULL
     ,@contract_start_date = NULL
     ,@contract_end_date = NULL
     ,@ed_contract_number = NULL
     ,@contract_status_id = -1

 EXEC [dbo].[Contract_Search_By_SecurityRole] 
      @Security_Role_Id = 99
     ,@sitegroup_id = NULL
     ,@site_id = NULL
     ,@site_not_managed = -1
     ,@vendor_id = NULL
     ,@contract_start_date = NULL
     ,@contract_end_date = NULL
     ,@ed_contract_number = NULL
     ,@contract_status_id = 1


 EXEC [dbo].[Contract_Search_By_SecurityRole] 
      @Security_Role_Id = 99
     ,@sitegroup_id = NULL
     ,@site_id = NULL
     ,@site_not_managed = -1
     ,@vendor_id = NULL
     ,@contract_start_date = NULL
     ,@contract_end_date = NULL
     ,@ed_contract_number = NULL
     ,@contract_status_id = 0


 EXEC [dbo].[Contract_Search_By_SecurityRole] 
      @Security_Role_Id = 99
     ,@sitegroup_id = NULL
     ,@site_id = NULL
     ,@site_not_managed = -1
     ,@vendor_id = NULL
     ,@contract_start_date = NULL
     ,@contract_end_date = NULL
     ,@ed_contract_number = NULL
     ,@contract_status_id = -1
									
 DECLARE @Tvp_Source tvp_Source 
 INSERT     INTO @Tvp_Source
 VALUES
            ( 290, 'Data Streams' ),
            ( 291, 'Data Streams' ),
            ( 67, 'Data Streams' ) ,
            ( 1585, 'Data Streams' ),
            ( 100028, 'Data Streams' )   
            
 EXEC [dbo].[Contract_Search_By_SecurityRole] 
      @Security_Role_Id = 99
     ,@sitegroup_id = NULL
     ,@site_id = NULL
     ,@site_not_managed = 0
     ,@vendor_id = NULL
     ,@contract_start_date = NULL
     ,@contract_end_date = NULL
     ,@ed_contract_number = NULL
     ,@contract_status_id = -1
     ,@Tvp_Source = @Tvp_Source

  
AUTHOR INITIALS:    
 Initials	Name    
------------------------------------------------------------    
 NR			Narayana Reddy
 
 MODIFICATIONS:  
 Initials	Date			Modification    
------------------------------------------------------------    
 NR			2020-02-11		MAINT-9861 - Moved Sproc From DV to CBMS.	

******/

CREATE PROCEDURE [dbo].[Contract_Search_By_SecurityRole]
     (
         @Security_Role_Id INT
         , @sitegroup_id INT = NULL
         , @site_id INT = NULL
         , @site_not_managed INT = -1
         , @vendor_id INT = NULL
         , @contract_start_date DATETIME = NULL
         , @contract_end_date DATETIME = NULL
         , @ed_contract_number VARCHAR(25) = NULL
         , @contract_status_id INT = -1
         , @Tvp_Source tvp_Source READONLY
     )
AS
    BEGIN

        SET NOCOUNT ON;

        CREATE TABLE #Client_Commodity_Ids
             (
                 Commodity_Id INT
             );

        DECLARE
            @today DATETIME
            , @Client_Id INT
            , @Tvp_Count INT
            , @Commodity_Service_Cd INT;

        SET @today = CONVERT(DATETIME, CONVERT(VARCHAR, GETDATE(), 101));

        SELECT
            @Client_Id = Client_Id
        FROM
            dbo.Security_Role
        WHERE
            Security_Role_Id = @Security_Role_Id;

        SELECT  @Tvp_Count = COUNT(1)FROM   @Tvp_Source;

        SELECT
            @Commodity_Service_Cd = c.Code_Id
        FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code c
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            Codeset_Name = 'CommodityServiceSource'
            AND Code_Value = 'Invoice';


        INSERT INTO #Client_Commodity_Ids
             (
                 Commodity_Id
             )
        SELECT  Source_Id FROM  @Tvp_Source tvp WHERE   tvp.Source_Type = 'Data Streams'
        UNION
        SELECT
            cc.Commodity_Id
        FROM
            @Tvp_Source tvp
            INNER JOIN dbo.Client_Commodity_Tag cct
                ON tvp.Source_Id = cct.Tag_Id
            INNER JOIN Core.Client_Commodity cc
                ON cct.Client_Commodity_Id = cc.Client_Commodity_Id
                   AND  cc.Commodity_Service_Cd = @Commodity_Service_Cd
                   AND  cc.Client_Id = @Client_Id
        WHERE
            tvp.Source_Type = 'Tags';

        WITH cte_SiteList
        AS (
               SELECT
                    ch.Client_Hier_Id
                    , ch.Client_Id
                    , ch.Site_Id
                    , ch.Site_name
                    , ch.Site_Not_Managed
               FROM
                    Core.Client_Hier ch
                    INNER JOIN dbo.Security_Role_Client_Hier srch
                        ON srch.Client_Hier_Id = ch.Client_Hier_Id
               WHERE
                    srch.Security_Role_Id = @Security_Role_Id
                    AND ch.Site_Id > 0
                    AND (   @sitegroup_id IS NULL
                            OR  (   ch.Sitegroup_Id = @sitegroup_id
                                    AND ch.Sitegroup_Type_Name = 'Division')
                            OR  EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.Sitegroup_Site sgs
                                           WHERE
                                                sgs.Site_id = ch.Site_Id
                                                AND sgs.Sitegroup_id = @sitegroup_id))
                    AND (   @site_id IS NULL
                            OR  ch.Site_Id = @site_id)
           )
             , Contract_Dtl_Cte
        AS (
               SELECT
                    c.CONTRACT_ID
                    , c.ED_CONTRACT_NUMBER
                    , CTE_Site.Site_name
                    , CHA.Account_Vendor_Name Vendor_Name
                    , com.Commodity_Name commodity_type
                    , c.CONTRACT_START_DATE
                    , c.CONTRACT_END_DATE
                    , c.CONTRACT_PRICING_SUMMARY
                    , CASE WHEN c.Is_Contract_Heat_Rate = 1 THEN 'Yes'
                          ELSE 'No'
                      END AS Is_Contract_Heat_Rate
               FROM
                    cte_SiteList CTE_Site
                    INNER JOIN Core.Client_Hier_Account CHA
                        ON CTE_Site.Client_Hier_Id = CHA.Client_Hier_Id
                    INNER JOIN dbo.CONTRACT c
                        ON c.CONTRACT_ID = CHA.Supplier_Contract_ID
                    INNER JOIN dbo.Commodity com
                        ON com.Commodity_Id = c.COMMODITY_TYPE_ID
               WHERE
                    (   @contract_status_id = -1
                        OR  (   @contract_status_id = 0
                                AND c.CONTRACT_END_DATE >= @today)
                        OR  (   @contract_status_id = 1
                                AND c.CONTRACT_END_DATE < @today))
                    AND (   @ed_contract_number IS NULL
                            OR  c.ED_CONTRACT_NUMBER = @ed_contract_number)
                    AND (   EXISTS (   SELECT
                                            1
                                       FROM
                                            #Client_Commodity_Ids cmi
                                       WHERE
                                            cmi.Commodity_Id = c.COMMODITY_TYPE_ID)
                            OR  @Tvp_Count = 0)
                    AND (   @contract_start_date IS NULL
                            OR  (   c.CONTRACT_START_DATE >= @contract_start_date
                                    AND c.CONTRACT_END_DATE >= @contract_start_date))
                    AND (   @contract_end_date IS NULL
                            OR  (   c.CONTRACT_START_DATE <= @contract_end_date
                                    AND c.CONTRACT_END_DATE <= @contract_end_date))
                    AND (   @site_not_managed = -1
                            OR  CTE_Site.Site_Not_Managed = @site_not_managed)
                    AND (   @vendor_id IS NULL
                            OR  CHA.Account_Vendor_Id = @vendor_id)
               GROUP BY
                   c.CONTRACT_ID
                   , c.ED_CONTRACT_NUMBER
                   , CTE_Site.Site_name
                   , CHA.Account_Vendor_Name
                   , com.Commodity_Name
                   , c.CONTRACT_START_DATE
                   , c.CONTRACT_END_DATE
                   , c.CONTRACT_PRICING_SUMMARY
                   , c.Is_Contract_Heat_Rate
           )
        SELECT
            c.CONTRACT_ID
            , c.ED_CONTRACT_NUMBER
            , c.Site_name
            , c.Vendor_Name
            , c.commodity_type
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE
            , c.CONTRACT_PRICING_SUMMARY
            , MIN(imap.CBMS_IMAGE_ID) cbms_image_id
            , c.Is_Contract_Heat_Rate
        FROM
            Contract_Dtl_Cte c
            LEFT OUTER JOIN dbo.CONTRACT_CBMS_IMAGE_MAP imap
                ON imap.CONTRACT_ID = c.CONTRACT_ID
        GROUP BY
            c.CONTRACT_ID
            , c.ED_CONTRACT_NUMBER
            , c.Site_name
            , c.Vendor_Name
            , c.commodity_type
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE
            , c.CONTRACT_PRICING_SUMMARY
            , c.Is_Contract_Heat_Rate
        ORDER BY
            c.Site_name
            , c.commodity_type
            , c.CONTRACT_START_DATE;
    END;


GO
GRANT EXECUTE ON  [dbo].[Contract_Search_By_SecurityRole] TO [CBMSApplication]
GO
