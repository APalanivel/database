SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.RM_Budget_Is_Budget_Name_Exists                        
                          
Description:                          
        To get market price and forecast pirce if a selected index   
                          
Input Parameters:                          
    Name    DataType        Default     Description                            
--------------------------------------------------------------------------------    
	@Index_Id   INT    
    @Start_Dt	Date    
	@End_Dt		Date
                          
 Output Parameters:                                
	Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
Usage Examples:                              
--------------------------------------------------------------------------------    
	SELECT * FROM dbo.ENTITY e WHERE e.ENTITY_TYPE=272

	EXEC Trade.RM_Budget_Is_Budget_Name_Exists  1005,'Budget with Comments New',49
    
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR			2019-11-29	RM-Budgets Enahancement - Created
	                
******/
CREATE PROCEDURE [Trade].[RM_Budget_Is_Budget_Name_Exists]
    (
        @Client_Id INT
        , @Budget_Name NVARCHAR(1000)
        , @Rm_Budget_Id INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @RM_Budget_Is_Budget_Name_Exists BIT = 0;

        SELECT
            @RM_Budget_Is_Budget_Name_Exists = 1
        FROM
            Trade.Rm_Budget rb
        WHERE
            rb.Client_Id = @Client_Id
            AND rb.Budget_NAME = @Budget_Name
            AND (   @Rm_Budget_Id IS NULL
                    OR  rb.Rm_Budget_Id <> @Rm_Budget_Id);

        SELECT  @RM_Budget_Is_Budget_Name_Exists AS RM_Budget_Is_Budget_Name_Exists;

    END;
GO
GRANT EXECUTE ON  [Trade].[RM_Budget_Is_Budget_Name_Exists] TO [CBMSApplication]
GO
