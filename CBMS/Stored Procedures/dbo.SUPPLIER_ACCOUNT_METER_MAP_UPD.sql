SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******        
NAME:        
 dbo.SUPPLIER_ACCOUNT_METER_MAP_UPD         
        
DESCRIPTION:        
        To update the SUPPLIER_ACCOUNT_METER_MAP based on provided Meter_Id & Contrcat_ID
        
INPUT PARAMETERS:        
	Name				DataType  Default Description        
------------------------------------------------------------        
@METER_ID					INT
@CONTRACT_ID				INT
@METER_ASSOCIATE_DATE		DATE
@METER_DISASSOCIATE_DATE	DATE
        
OUTPUT PARAMETERS:        
 Name   DataType  Default Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:        
------------------------------------------------------------        
	BEGIN TRAN
	EXEC SUPPLIER_ACCOUNT_METER_MAP_UPD 801,12048, '1 JAN 2010','31 DEC 2010'
	EXEC SUPPLIER_ACCOUNT_METER_MAP_UPD NULL,12048, '1 JAN 2010','31 DEC 2010'
	select * from SUPPLIER_ACCOUNT_METER_MAP WHERE CONTRACT_ID = 12048 AND METER_ID = 801
	ROLLBACK TRAN
        
AUTHOR INITIALS:        
 Initials Name        
------------------------------------------------------------        
SKA		Shobhit Kumar Agrawal

MODIFICATIONS                
 Initials  Date   Modification        
------------------------------------------------------------        
SKA		06/11/2009  Created    
SKA		06/12/2010	@METER_ID MARKED AS NULLABLE
NR		2019-08-08	Add Contract - Added @Is_Rolling_Meter parameter.

  
 
******/
CREATE PROCEDURE [dbo].[SUPPLIER_ACCOUNT_METER_MAP_UPD]
    (
        @METER_ID INT = NULL
        , @CONTRACT_ID INT
        , @METER_ASSOCIATE_DATE DATE
        , @METER_DISASSOCIATE_DATE DATE
        , @Is_Rolling_Meter BIT = 0
    )
AS
    BEGIN
        UPDATE
            SUPPLIER_ACCOUNT_METER_MAP
        SET
            METER_ASSOCIATION_DATE = @METER_ASSOCIATE_DATE
            , METER_DISASSOCIATION_DATE = @METER_DISASSOCIATE_DATE
            , Is_Rolling_Meter = @Is_Rolling_Meter
        WHERE
            (   @METER_ID IS NULL
                OR  METER_ID = @METER_ID)
            AND Contract_ID = @CONTRACT_ID;

    END;


GO
GRANT EXECUTE ON  [dbo].[SUPPLIER_ACCOUNT_METER_MAP_UPD] TO [CBMSApplication]
GO
