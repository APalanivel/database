
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
NAME:              
 dbo.Utility_Invoice_Control_Analyst_Dtl_Sel_By_Contract    
            
DESCRIPTION:            
 Procedure to get the details of the invoice control analysts for the given contract
             
INPUT PARAMETERS:            
 Name          DataType Default  Description              
------------------------------------------------------------              
 @Contract_Id  INT  
             
OUTPUT PARAMETERS:              
 Name		   DataType Default  Description              
------------------------------------------------------------           
             
USAGE EXAMPLES:            
------------------------------------------------------------              
  
EXEC dbo.Utility_Invoice_Control_Analyst_Dtl_Sel_By_Contract 95248
EXEC dbo.Utility_Invoice_Control_Analyst_Dtl_Sel_By_Contract 95247
EXEC dbo.Utility_Invoice_Control_Analyst_Dtl_Sel_By_Contract 95339
           
              
AUTHOR INITIALS:              
 Initials Name              
------------------------------------------------------------              
 RR		  Raghu Reddy  
 SP		  Sandeep Pigilam           
             
 MODIFICATIONS               
 Initials Date		 Modification              
------------------------------------------------------------              
 RR		  2012-10-08 Created Maint-1354 To get email address of Invoice Control Analysts to add the analysts in CC of Hedge Confirm E-mails   
 SP		  2014-09-09 For Data Transition used a table variable @Group_Legacy_Name to handle the hardcoded group names  
******/     
            
CREATE PROCEDURE [dbo].[Utility_Invoice_Control_Analyst_Dtl_Sel_By_Contract] ( @Contract_Id INT )
AS 
BEGIN            
            
      SET NOCOUNT ON;  
      
      DECLARE @Group_Legacy_Name TABLE
            ( 
             GROUP_INFO_ID INT
            ,GROUP_NAME VARCHAR(200)
            ,Legacy_Group_Name VARCHAR(200) )
      INSERT      INTO @Group_Legacy_Name
                  ( 
                   GROUP_INFO_ID
                  ,GROUP_NAME
                  ,Legacy_Group_Name )
                  EXEC dbo.Group_Info_Sel_By_Group_Legacy 
                        @Group_Legacy_Name_Cd_Value = 'Invoice_Control' 
      
      SELECT
            utl.Account_Vendor_Id Vendor_Id
           ,utl.Account_Vendor_Name Vendor_Name
           ,ana.Analyst_ID
           ,ui.FIRST_NAME + ' ' + ui.LAST_NAME Analyst_Name
           ,ui.EMAIL_ADDRESS
           ,gi.GROUP_NAME
      FROM
            Core.Client_Hier_Account supp
            JOIN Core.Client_Hier_Account utl
                  ON supp.Meter_Id = utl.Meter_Id
            JOIN dbo.UTILITY_DETAIL ud
                  ON utl.Account_Vendor_Id = ud.VENDOR_ID
            JOIN dbo.Utility_Detail_Analyst_Map ana
                  ON ud.UTILITY_DETAIL_ID = ana.Utility_Detail_ID
            INNER JOIN dbo.GROUP_INFO gi
                  ON ana.Group_Info_ID = gi.GROUP_INFO_ID
            INNER JOIN @Group_Legacy_Name gil
                  ON gil.Group_Info_ID = gi.GROUP_INFO_ID
            JOIN dbo.USER_INFO ui
                  ON ui.USER_INFO_ID = ana.Analyst_ID
      WHERE
            supp.Account_Type = 'Supplier'
            AND utl.Account_Type = 'Utility'
            AND supp.Supplier_Contract_ID = @Contract_Id
      GROUP BY
            utl.Account_Vendor_Id
           ,utl.Account_Vendor_Name
           ,ana.Analyst_ID
           ,ui.FIRST_NAME
           ,ui.LAST_NAME
           ,ui.EMAIL_ADDRESS
           ,gi.GROUP_NAME  
     
END;


;
GO

GRANT EXECUTE ON  [dbo].[Utility_Invoice_Control_Analyst_Dtl_Sel_By_Contract] TO [CBMSApplication]
GO
