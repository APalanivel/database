SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--Mark McCallon
--11-19-07
--delta delete for dv2_account_site
--
create procedure dbo.DV2GetAccountSiteDeltaDeletes
as
begin
set nocount on

delete dbo.dv2_account_site from dbo.dv2_account_site a
left outer join
tempdb.dbo.dv2_account_site_delta b
on a.site_id=b.site_id and a.account_id=b.account_id and a.account_number=b.account_number
and a.commodity_type_id=b.commodity_type_id
where b.site_id is null

end
GO
GRANT EXECUTE ON  [dbo].[DV2GetAccountSiteDeltaDeletes] TO [CBMSApplication]
GO
