SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Utility_Dtl_Volume_Requirement_Upd

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	 @Utility_Dtl_Volume_Requirement_Id		INT
     @Utility_Volume_Dsc					VARCHAR(30) = NULL
     @Utility_UOM_Cd						INT = NULL
     @Time_Period							VARCHAR(200) = NULL
     @Time_Req								VARCHAR(500) = NULL
     @Other_Req								VARCHAR(500) = NULL

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	
	EXEC Utility_Dtl_Volume_Requirement_Upd 1,'Utility_Volume_Dsc TEST',0,'Monthly','Time_Req TEST','Other_Req TEST'	
	
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	SKA			01/12/2011	Created
******/

CREATE PROC dbo.Utility_Dtl_Volume_Requirement_Upd
      (
       @Utility_Dtl_Volume_Requirement_Id INT
      ,@Utility_Volume_Dsc VARCHAR(30) = NULL
      ,@Utility_UOM_Cd INT = NULL
      ,@Time_Period VARCHAR(200) = NULL
      ,@Time_Req VARCHAR(500) = NULL
      ,@Other_Req VARCHAR(500) = NULL )
AS 
BEGIN
 
      SET nocount ON ;

      UPDATE
            Utility_Dtl_Volume_Requirement
      SET   
            Utility_Volume_Dsc = @Utility_Volume_Dsc
           ,Utility_UOM_Cd = @Utility_UOM_Cd
           ,Time_Period = @Time_Period
           ,Time_Req = @Time_Req
           ,Other_Req = @Other_Req
      WHERE
            Utility_Dtl_Volume_Requirement_Id = @Utility_Dtl_Volume_Requirement_Id
END                        
GO
GRANT EXECUTE ON  [dbo].[Utility_Dtl_Volume_Requirement_Upd] TO [CBMSApplication]
GO
