SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
     
 dbo.Report_Contract_Expiration_Summary    
     
DESCRIPTION:    
 This Procedure is to get all Contract Expiration summary details.    
     
INPUT PARAMETERS:    
Name     DataType  Default Description    
-------------------------------------------------------------------------------------------    
@Client_Id_List   VARCHAR(MAX)    
@Commodity_Id_List  VARCHAR(MAX)    
@Region_Id_List   VARCHAR(MAX)    
@State_id_List   VARCHAR(MAX)    
@Begin_date    DATETIME    
@End_date    DATETIME    
@Contract_Name   VARCHAR(200)    
@Sourcing_Buyer_id_List VARCHAR(MAX)    
@Sourcing_Manager_id_List VARCHAR(MAX)  
@Account_Service_lvl VARCHAR(24)
  
OUTPUT PARAMETERS:  
Name     DataType  Default Description  
------------------------------------------------------------    
  
USAGE EXAMPLES:  
-------------------------------------------------------------  
  
EXEC dbo.Report_Contract_Expiration_Summary   
      '1005'  
     ,'290,291'  
     ,'2,5,4,3'  
     ,'23,24,25,36'  
     ,'2009-01-01'  
     ,'2012-12-01'  
     ,'Supplier,Utility'  
     ,'20713,865,7537'  
     ,'120,350,8483'  
     ,'859'          

AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
 SSR  Sharad srivastava    
 AKR  Ashok Kumat Raju  
 hg   Harihara Suthan Ganesan  
  
 MODIFICATIONS Initials Date  Modification  
------------------------------------------------------------  
 SSR     01/18/2011 Created  
 SSR     02/07/2011 Replaced Parameter @Contract_id with @Contract_Name  
 LEC     02/14/2011 Removed AND ch.Country_Name IN ( ''usa'', ''mexico'', ''canada'' )   
 AKR     2010-10-12 Added Total Metered Volume Column  
 AKR     2013-01-28 Modified the script to include Custom Analysts  
 AKR     2012-02-07 Coeected the Dynamic Code.  
 HG		 2013-05-02 Added INNER JOIN with code table to filter only the Cost & usages services mapped to the client in Core.Client_Commodity  
 AKR	2014-06-05  Modified to remove CHA.Supplier_Meter_Disassociation_Date >= CHA.Supplier_Account_End_Dt  condition and
                    moved Account service level filter to Report level.
*/  
CREATE PROC [dbo].[Report_Contract_Expiration_Summary]
      (
       @Client_Id_List VARCHAR(MAX)
      ,@Commodity_Id_List VARCHAR(MAX)
      ,@Region_Id_List VARCHAR(MAX)
      ,@State_id_List VARCHAR(MAX)
      ,@Begin_date DATETIME
      ,@End_date DATETIME
      ,@Contract_Name VARCHAR(200)
      ,@Sourcing_Buyer_id_List VARCHAR(MAX)
      ,@Sourcing_Manager_id_List VARCHAR(MAX)
      ,@Account_Service_lvl VARCHAR(24) )
AS
BEGIN                       
                      
      SET NOCOUNT ON                      
                      
      DECLARE
            @Contract_Classification_id INT
           ,@Contract_Supplier_id INT
           ,@Contract_Utility_id INT
           ,@EP_KWh_Unit INT
           ,@NG_MMBtu_Unit INT             
                        
      DECLARE @SQL VARCHAR(MAX)                      
      DECLARE @SQL_ext VARCHAR(MAX)       
      DECLARE @SQL_ext1 VARCHAR(MAX)                      
      DECLARE
            @Default_Analyst INT
           ,@Custom_Analyst INT  
  
      SELECT
            @Contract_Supplier_id = a.ENTITY_ID
      FROM
            dbo.ENTITY a
      WHERE
            a.ENTITY_NAME = 'Supplier'
            AND a.ENTITY_DESCRIPTION = 'Contract Type'                      
                         
      SELECT
            @Contract_Utility_id = a.ENTITY_ID
      FROM
            dbo.ENTITY a
      WHERE
            a.ENTITY_NAME = 'Utility'
            AND a.ENTITY_DESCRIPTION = 'Contract Type'                         
                          
      SELECT
            @Contract_Classification_id = a.ENTITY_ID
      FROM
            dbo.ENTITY a
      WHERE
            a.ENTITY_NAME = 'Pipeline - Transportation'
            AND a.ENTITY_DESCRIPTION = 'Contract Classification'                       
                          
             
      SELECT
            @Default_Analyst = MAX(CASE WHEN c.Code_Value = 'Default' THEN c.Code_Id
                                   END)
           ,@Custom_Analyst = MAX(CASE WHEN c.Code_Value = 'Custom' THEN c.Code_Id
                                  END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Analyst Type'           
                           
                      
      SELECT
            @NG_MMBtu_Unit = ent.ENTITY_ID
      FROM
            dbo.ENTITY ent
      WHERE
            ent.ENTITY_NAME = 'MMBtu'
            AND ent.ENTITY_DESCRIPTION = 'Unit for Gas'                      
                            
      SELECT
            @EP_KWh_Unit = ent.ENTITY_ID
      FROM
            dbo.ENTITY ent
      WHERE
            ent.ENTITY_NAME = 'KWh'
            AND ent.ENTITY_DESCRIPTION = 'Unit for electricity'                      
                            
                                  
      SET @SQL = ';                        
    WITH    maxcontract_cte                      
                  AS ( SELECT    
                  cha.meter_id [MeterID]    
                 ,max(contract_end_date) [MaxContractExp]    
                 ,c.CONTRACT_TYPE_ID [ContractType]    
                 ,c.Contract_Classification_Cd [Class]    
             FROM    
                  core.Client_Hier_Account cha    
                  INNER JOIN dbo.CONTRACT c    
                        ON c.CONTRACT_ID = cha.Supplier_Contract_ID '                 
                                            
      IF @Contract_Name <> 'Supplier,Utility'
            BEGIN                                      
                  SET @SQL = @SQL + ' WHERE                      
        (                    
            c.CONTRACT_TYPE_ID = CASE WHEN ''' + @Contract_Name + '''= ''Utility'' THEN ' + CAST(@Contract_Utility_id AS VARCHAR) + ' ELSE ' + CAST(@Contract_Supplier_id AS VARCHAR) + ' END ' + ' AND cha.COMMODITY_ID in (' + @Commodity_Id_List + ')   ' + '  '                      
                  IF @Contract_Name = 'Utility'
                        SET @SQL = @SQL + ' AND c.Contract_Classification_Cd = ' + CAST(@Contract_Classification_id AS VARCHAR)                 
            END                
      ELSE
            BEGIN                
                   
                  SET @SQL = @SQL + ' WHERE                      
        (  c.CONTRACT_TYPE_ID = ' + CAST(@Contract_Supplier_id AS VARCHAR) + ' AND cha.COMMODITY_ID in (' + @Commodity_Id_List + ')' + '  )                
            GROUP BY                      
                        cha.METER_ID    
                  ,c.CONTRACT_TYPE_ID    
                 ,c.Contract_Classification_Cd    
               
				 UNION     
			         
				 SELECT              
                        samm.meter_id [MeterID]                      
                      , Max(contract_end_date) [MaxContractExp]                      
                      , c.CONTRACT_TYPE_ID [ContractType]                      
                      , c.Contract_Classification_Cd [Class]                      
                       FROM                      
                        dbo.CONTRACT c                      
                        JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm                      
                            ON samm.Contract_ID = c.CONTRACT_ID                      
                        JOIN dbo.ACCOUNT a                      
               ON a.ACCOUNT_ID = samm.ACCOUNT_ID    
                where     
     (  c.CONTRACT_TYPE_ID =' + CAST(@Contract_Utility_id AS VARCHAR) + '                
             AND c.COMMODITY_TYPE_ID in (' + @Commodity_Id_List + ')' + ' AND samm.IS_HISTORY != 1                 
     AND c.Contract_Classification_Cd = ' + CAST(@Contract_Classification_id AS VARCHAR)                 
            END      
      SET @SQL = @SQL + ')  GROUP BY                      
                        METER_ID                  
                      , c.CONTRACT_TYPE_ID                      
                      , c.Contract_Classification_Cd       
                          
                      ) ,                      
                sourcing_analyst_cte                      
                 AS ( SELECT                      
                        com.Commodity_Name                      
                      , vcam.Analyst_Id                      
                      , vcm.vendor_id                      
                      , SA.first_name + Space(1) + SA.last_name [SA]                      
                      , sm.first_name + Space(1) + sm.last_name [SM]                      
                       FROM                      
                        dbo.VENDOR_COMMODITY_MAP vcm                      
                        JOIN dbo.Vendor_Commodity_Analyst_Map vcam                      
                            ON vcam.Vendor_Commodity_Map_Id = vcm.VENDOR_COMMODITY_MAP_ID                      
                        JOIN dbo.UTILITY_DETAIL ud                      
                            ON ud.VENDOR_ID = vcm.VENDOR_ID                      
            JOIN dbo.Commodity com                      
                            ON com.Commodity_Id = vcm.COMMODITY_TYPE_ID                      
                        JOIN dbo.user_info sa                      
                            ON sa.USER_INFO_ID = vcam.Analyst_Id                      
                        LEFT JOIN dbo.SR_SA_SM_MAP smap                      
             ON smap.SOURCING_ANALYST_ID = vcam.Analyst_ID                      
                        LEFT JOIN user_info sm                      
                            ON sm.USER_INFO_ID = smap.SOURCING_MANAGER_ID                      
                            AND sm.USER_INFO_ID in ( ' + @Sourcing_Manager_id_List + ')                      
                        WHERE                      
                        com.Commodity_id IN (' + @Commodity_Id_List + ')                      
                        And sa.USER_INFO_ID IN (' + @Sourcing_Buyer_id_List + ')                      
                     )                    
                select * into #Temp_Account_Details        
                from        
                 ( SELECT                       
                        c.Ed_contract_number                      
                      , ch.Client_Name                      
                      , ch.Sitegroup_Name                      
                      , ch.Site_name              
                      ,ch.Site_Id        
                      ,Ch.Client_Id        
                      ,ca.Account_Id                
                      , ch.City                      
                      , ca.Account_Number                      
                      , ch.state_Name                      
                      , ch.Region_Name                      
                      , com.Commodity_Name                      
                      , ctyp.ENTITY_NAME                      
                      , cd.Code_Value                      
                      ,chs.Account_Vendor_Name VENDOR_NAME              
                       ,ca.Account_Vendor_Id      
                 ,CA.Commodity_Id      
                 ,coalesce(ca.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) Analyst_Mapping_Cd     
                      , ca.Account_Vendor_Name                      
                      , c.CONTRACT_START_DATE                      
                      , c.CONTRACT_end_DATE                      
  , c.CONTRACT_END_DATE - (NOTIFICATION_DAYS) NotificationDate                      
                   ,c.CONTRACT_ID      
                   ,mc.MeterID      
                               ,ch.Sitegroup_Id      
   ,ch.Region_ID      
                 ,ch.State_Id   
                FROM                      
                        dbo.CONTRACT c                      
                        JOIN maxcontract_cte mc                      
                            ON mc.Class = c.Contract_Classification_Cd                      
                               AND mc.ContractType = c.CONTRACT_TYPE_ID                      
                               AND mc.MaxContractExp = c.CONTRACT_END_DATE     
                                 JOIN core.Client_Hier_Account chs    
                        ON chs.Meter_Id = mc.MeterID    
                           AND c.CONTRACT_ID = chs.Supplier_Contract_ID    
                           AND chs.Account_type = ''Supplier''                             
                         JOIN Core.Client_Hier_Account ca                      
                            ON ca.Meter_Id = mc.MeterID                      
                               AND ca.Account_type = ''Utility''                      
                        JOIN Core.Client_Hier ch                      
                            ON ch.Client_Hier_Id = ca.Client_Hier_Id                      
                        JOIN dbo.ENTITY ctyp                      
                            ON ctyp.ENTITY_ID = c.contract_type_id                      
                        JOIN dbo.Code cd                      
                            ON cd.Code_Id = c.Contract_Classification_Cd                      
                        JOIN dbo.Commodity com                      
                            ON com.Commodity_Id = ca.Commodity_Id                      
                               AND com.Commodity_Id = c.COMMODITY_TYPE_ID                      
                        WHERE                      
                        ch.Site_Id != 0                      
                        AND ch.Site_Not_Managed != 1                      
                        AND ca.Account_Service_level_Cd IN  (' + @Account_Service_lvl + ')                  
                        AND ch.State_id in (' + @State_id_List + ')                      
                        AND ch.Region_id in (' + @Region_Id_List + ')                      
                        AND ch.Client_Id in (' + @Client_Id_List + ')                    
            AND mc.MaxContractExp BETWEEN ' + '''' + CONVERT(VARCHAR(10), @Begin_date, 101) + ''' AND ' + '''' + CONVERT(VARCHAR(10), @End_date, 101) + '''                      
                     ) k        
                             
                             
                             
                      ;          
      WITH  cte_Account_User          
              AS ( SELECT          
                acc.Ed_contract_number                      
                      , acc.Client_Name                      
                      , acc.Sitegroup_Name                      
                      , acc.Site_name              
                      ,acc.Site_Id        
                      ,acc.Client_Id        
                      ,acc.Account_Id                
                      , acc.City                      
                      , acc.Account_Number                      
                      , acc.state_Name                      
                      , acc.Region_Name                      
                      , acc.Commodity_Name                      
                      , acc.ENTITY_NAME                      
                      , acc.Code_Value                      
                      , acc.VENDOR_NAME                      
     , acc.Account_Vendor_Name                      
                    , acc.CONTRACT_START_DATE                  
                      , acc.CONTRACT_end_DATE           
                      , acc.NotificationDate              
                      ,acc.Sitegroup_Id      
                 ,acc.Region_ID      
                 ,acc.State_Id      
                 ,acc.Account_Vendor_Id      
     ,acc.CONTRACT_ID      
                   ,acc.MeterID      
                     ,case WHEN acc.Analyst_Mapping_Cd = ' + CAST(@Default_Analyst AS VARCHAR) + ' THEN vcam.Analyst_Id          
                             WHEN acc.Analyst_Mapping_Cd = ' + CAST(@Custom_Analyst AS VARCHAR) + '  THEN coalesce(aca.Analyst_User_Info_Id, sca.Analyst_User_Info_Id, cca.Analyst_User_Info_Id)          
                        END AS Analyst_Id          
                              
                   FROM          
                        #Temp_Account_Details acc          
                        JOIN dbo.VENDOR_COMMODITY_MAP vcm          
                              ON acc.Account_Vendor_Id = vcm.VENDOR_ID          
                                 AND acc.Commodity_Id = vcm.COMMODITY_TYPE_ID          
                        INNER JOIN dbo.Vendor_Commodity_Analyst_Map vcam          
                              ON vcm.VENDOR_COMMODITY_MAP_ID = vcam.Vendor_Commodity_Map_Id          
                        INNER JOIN core.Client_Commodity ccc  
                              ON ccc.Client_Id = acc.Client_Id  
                                 AND ccc.Commodity_Id = acc.COMMODITY_ID  
                        INNER JOIN dbo.Code cd  
       ON cd.Code_Id = ccc.Commodity_Service_Cd  
        AND cd.Code_Value = ''Invoice''  
                        LEFT JOIN dbo.Account_Commodity_Analyst aca  
                              ON aca.Account_Id = acc.Account_Id       
                                 AND aca.Commodity_Id = acc.COMMODITY_ID  
                        LEFT JOIN dbo.SITE_Commodity_Analyst sca  
                              ON sca.Site_Id = acc.Site_Id    
                                 AND sca.Commodity_Id = acc.COMMODITY_ID  
                        LEFT JOIN dbo.Client_Commodity_Analyst cca    
                              ON ccc.Client_Commodity_Id = cca.Client_Commodity_Id  
                              )  
  
SELECT * INTO #Temp_Final        
FROM                                      
    (        SELECT  DISTINCT        
                 cau.Ed_contract_number                      
                      , cau.Client_Name                      
                      , cau.Sitegroup_Name                      
                      , cau.Site_name              
                      ,cau.Site_Id        
                      ,cau.Client_Id        
                      ,cau.Account_Id                
                      , cau.City                      
                      , cau.Account_Number                      
                      , cau.state_Name                      
                      , cau.Region_Name                      
                      , cau.Commodity_Name                      
                      , cau.ENTITY_NAME                      
                      , cau.Code_Value                      
                      , cau.VENDOR_NAME                      
                      , cau.Account_Vendor_Name                      
                      , cau.CONTRACT_START_DATE                      
                      , cau.CONTRACT_end_DATE                      
                      , cau.NotificationDate              
                        ,cau.Sitegroup_Id      
                 ,cau.Region_ID      
                 ,cau.State_Id      
                 ,cau.Account_Vendor_Id       
                  ,cau.CONTRACT_ID      
                   ,cau.MeterID      
           ,uia.first_name + space(1) + uia.last_name [Sourcing Buyer]        
           ,uim.first_name + space(1) + uim.last_name [Sourcing Manager]        
      FROM        
     cte_Account_User cau        
            INNER JOIN dbo.USER_INFO uia        
                  ON cau.Analyst_Id = uia.USER_INFO_ID        
            LEFT JOIN dbo.SR_SA_SM_MAP sm        
                  ON cau.Analyst_Id = sm.SOURCING_ANALYST_ID        
          LEFT JOIN dbo.USER_INFO uim        
                  ON uim.USER_INFO_ID = sm.SOURCING_MANAGER_ID        
                  AND sm.SOURCING_MANAGER_ID in ( ' + @Sourcing_Manager_id_List + ')         
            WHERE  cau.Analyst_Id IN ( ' + @Sourcing_Buyer_id_List + ')  ) k        
                             
                     '             
                             
                             
                                      
      SET @SQL_ext = '        
      ;WITH CTE_Final AS        
      ( SELECT        
       tf.Ed_contract_number                      
                      , tf.Client_Name                      
                      , tf.Sitegroup_Name                      
                      , tf.Site_name              
                      ,tf.Site_Id        
                      ,tf.Client_Id        
                      ,tf.Account_Id                
                      , tf.City                      
                      , tf.Account_Number                      
                      , tf.state_Name                      
                      , tf.Region_Name                      
                      , tf.Commodity_Name                      
                  , tf.ENTITY_NAME                      
                      , tf.Code_Value                      
                      , tf.VENDOR_NAME                      
                      , tf.Account_Vendor_Name   
                      , pt.Code_Value AS Product_Type                   
                      , tf.CONTRACT_START_DATE                      
                      , tf.CONTRACT_end_DATE                      
                      , tf.NotificationDate              
           ,tf.[Sourcing Buyer] SA        
           ,tf.[Sourcing Manager] SM        
            , DENSE_RANK() OVER ( PARTITION BY tf.Ed_contract_number ORDER BY tf.Account_id ) Account_count                      
                      , DENSE_RANK() OVER ( PARTITION BY tf.Ed_contract_number ORDER BY tf.Site_id ) Site_count                      
                      , DENSE_RANK() OVER ( PARTITION BY tf.Ed_contract_number ORDER BY tf.sitegroup_id ) Division_Count                      
                      , DENSE_RANK() OVER ( PARTITION BY tf.Ed_contract_number ORDER BY tf.city ) City_count                      
                      , DENSE_RANK() OVER ( PARTITION BY tf.Ed_contract_number ORDER BY tf.region_id ) Region_count                      
                      , DENSE_RANK() OVER ( PARTITION BY tf.Ed_contract_number ORDER BY tf.Account_vendor_id ) Vendor_count                      
                      , DENSE_RANK() OVER ( PARTITION BY tf.Ed_contract_number ORDER BY [Sourcing Buyer] ) Analyst_count                      
                      , DENSE_RANK() OVER ( PARTITION BY tf.Ed_contract_number ORDER BY [Sourcing Manager] ) Manager_count                      
                      , DENSE_RANK() OVER ( PARTITION BY tf.Ed_contract_number ORDER BY tf.State_id ) State_count                 
                        ,case when tf.Commodity_name = ''Electric Power''  THEN          
                                  CASE WHEN e.entity_name = ''Daily'' THEN dd.Days_In_Month_Num * cmv.Volume * cucep.Conversion_Factor          
                                       ELSE cmv.Volume * cucep.Conversion_Factor end          
                             when tf.Commodity_name = ''NATURAL Gas''   THEN                   
                                  CASE WHEN e.entity_name = ''Daily'' THEN dd.Days_In_Month_Num * cmv.Volume * cucng.Conversion_Factor          
                                       ELSE cmv.Volume * cucng.Conversion_Factor END          
            END Metered_Volume       
           FROM #Temp_Final tf  
             JOIN dbo.Contract con
				ON con.Contract_Id = tf.Contract_Id
             LEFT JOIN dbo.Code pt
				ON pt.Code_Id = con.Contract_Product_Type_Cd 
             left JOIN dbo.CONTRACT_METER_VOLUME cmv          
                        ON cmv.CONTRACT_Id = tf.CONTRACT_Id          
                        AND   tf.MeterID = cmv.Meter_Id              
                        left JOIN dbo.ENtity e ON e.Entity_Id = cmv.Frequency_Type_Id    
                        left JOIN Meta.Date_Dim dd ON cmv.Month_Identifier = dd.Date_D          
   left JOIN dbo.CONSUMPTION_UNIT_CONVERSION cucep          
                 ON cmv.Unit_Type_Id = cucep.Base_Unit_Id          
                               and cucep.converted_Unit_Id = ' + CAST(@EP_KWh_Unit AS VARCHAR) + '          
                        left JOIN dbo.CONSUMPTION_UNIT_CONVERSION cucng          
                               ON cmv.Unit_Type_Id = cucng.Base_Unit_Id          
                               and cucng.converted_Unit_Id = ' + CAST(@NG_MMBtu_Unit AS VARCHAR) + '         
      )  '      
              
      SET @SQL_ext1 = '      
      SELECT                      
                cte_f.Ed_contract_number [Contract Number]                      
              , cte_f.Client_Name [Client]                      
              , CASE WHEN Acct_cnt.Division_Cnt > 1 THEN ''Multiple''                      
                     ELSE cte_f.Sitegroup_Name                      
                END [Division]                      
              , CASE WHEN Acct_cnt.Site_cnt > 1                      
                     THEN ''Multiple''                      
                     ELSE cte_f.Site_name                      
                END [Site]                      
               , Acct_cnt.Site_cnt [Site Count]                       
              , CASE WHEN Acct_cnt.City_cnt > 1 THEN ''Multiple''                      
                     ELSE cte_f.City                      
                END [City]                      
              , CASE WHEN Acct_cnt.cnt > 1 THEN ''Multiple''                      
                     ELSE cte_f.Account_Number                      
                END [Account Number]                      
              , Acct_cnt.cnt [Account Count]                        
              , CASE WHEN Acct_cnt.State_cnt > 1 THEN ''Multiple''                      
                     ELSE cte_f.State_Name                      
                END [State]                      
              , CASE WHEN Acct_cnt.Region_cnt > 1 THEN ''Multiple''                      
                     ELSE cte_f.Region_Name                      
                END [Region]                      
              , cte_f.Commodity_Name [Commodity]                      
              , cte_f.ENTITY_NAME [Contract Type]                      
              , cte_f.Code_Value [Contract Classification Type]                      
              , cte_f.VENDOR_NAME [Vendor]                      
              , CASE WHEN Acct_cnt.Vendor_cnt > 1 THEN ''Multiple''                      
                     ELSE cte_f.Account_vendor_Name                      
                END [Utility]
              , cte_f.Product_Type                      
              , cte_f.CONTRACT_START_DATE [Contract Start Date]                      
              , cte_f.CONTRACT_END_DATE  [Contract End Date]                      
              , cte_f.NotificationDate [Contract Notification Date]                      
              , CASE WHEN Acct_cnt.Analyst_cnt > 1 THEN ''Multiple''                      
        ELSE cte_f.SA                      
                END [Sourcing Buyer]                      
              , CASE WHEN Acct_cnt.Manager_cnt > 1 THEN ''Multiple''                      
                     ELSE cte_f.SM                      
                END [Sourcing Manager]              
               ,sum(cte_f.Metered_Volume) Metered_Volume                   
            FROM                      
                CTE_Final cte_f                      
                JOIN ( SELECT                      
                        MAX(cte_Acc_cnt.Account_count) cnt                      
                      , MAX(cte_Acc_cnt.Site_count) Site_cnt                      
                      , MAX(cte_Acc_cnt.Division_Count) Division_cnt                      
                      , MAX(cte_Acc_cnt.City_count) City_cnt                      
                      , MAX(cte_Acc_cnt.Region_count) Region_cnt                      
                      , MAX(cte_Acc_cnt.Vendor_count) Vendor_cnt                      
                      , MAX(cte_Acc_cnt.Analyst_count) Analyst_cnt                      
             , MAX(cte_Acc_cnt.Manager_count) Manager_cnt      
                      , MAX(cte_Acc_cnt.State_count) State_cnt                      
                      , cte_Acc_cnt.Ed_contract_number                      
                       FROM                      
                        CTE_Final cte_Acc_cnt                      
                       GROUP BY                      
                        cte_Acc_cnt.Ed_contract_number                     
                     ) Acct_cnt                      
                    ON Acct_cnt.ED_CONTRACT_NUMBER = cte_f.ED_CONTRACT_NUMBER                      
            GROUP BY                      
                cte_f.Ed_contract_number                      
              , cte_f.Client_Name                      
              , cte_f.Commodity_Name                      
              , cte_f.ENTITY_NAME                      
              , cte_f.Code_Value                      
              , cte_f.CONTRACT_START_DATE                      
              , cte_f.CONTRACT_END_DATE                      
              , cte_f.NotificationDate                      
              , cte_f.VENDOR_NAME  
              , cte_f.Product_Type                    
              , Acct_cnt.Site_cnt                       
              , Acct_cnt.cnt                      
              , CASE WHEN Acct_cnt.Division_Cnt > 1 THEN ''Multiple''                   
                     ELSE cte_f.Sitegroup_Name                      
                END                      
              , CASE WHEN Acct_cnt.Site_cnt > 1                      
                     THEN ''Multiple''                      
                     ELSE cte_f.Site_name                      
                END                      
              , CASE WHEN Acct_cnt.City_cnt > 1 THEN ''Multiple''                      
                     ELSE cte_f.City                      
                END                      
              , CASE WHEN Acct_cnt.cnt > 1 THEN ''Multiple''                      
                     ELSE cte_f.Account_Number                      
                END                      
              , CASE WHEN Acct_cnt.State_cnt > 1 THEN ''Multiple''                      
    ELSE cte_f.State_Name                      
                END                      
              , CASE WHEN Acct_cnt.Region_cnt > 1 THEN ''Multiple''                      
                     ELSE cte_f.Region_Name                      
                END                      
              , CASE WHEN Acct_cnt.Vendor_cnt > 1 THEN ''Multiple''                      
                     ELSE cte_f.Account_vendor_Name                      
                END                      
              , CASE WHEN Acct_cnt.Analyst_cnt > 1 THEN ''Multiple''                      
                     ELSE cte_f.SA                      
                END                      
              , CASE WHEN Acct_cnt.Manager_cnt > 1 THEN ''Multiple''                      
                     ELSE cte_f.SM                      
                END                       
                '                      
      EXEC    
      ( @SQL + @SQL_Ext + @SQL_ext1 )                      
              
                   
END;  
;  
  
  
  
;

GO




GRANT EXECUTE ON  [dbo].[Report_Contract_Expiration_Summary] TO [CBMS_SSRS_Reports]
GO
