SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
NAME:  
      dbo.Account_Commodity_Broker_Fee_Merge
   
 DESCRIPTION:   
      Inserts/Updates the Broker Fee for a given Account and Commodity.
      
 INPUT PARAMETERS:  
 Name			DataType	Default Description  
------------------------------------------------------------  
 @Account_Id      INT  
 @Commodity_Id    INT
 @Broker_Fee     decimal(28, 10)
 @Currency_Unit_Id INT    
 @UOM_Entity_Id    INT
 
 OUTPUT PARAMETERS:
 Name   DataType  Default Description
------------------------------------------------------------
 USAGE EXAMPLES:  
------------------------------------------------------------
    Begin TRAN
	EXEC dbo.Account_Commodity_Broker_Fee_Merge 10010
	End tran
	
AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------ 
 AKR		Ashok Kumar Raju
 

 MODIFICATIONS   
 Initials	Date		Modification
------------------------------------------------------------
 AKR        2012-09-28  Created for POCO

******/
CREATE  PROCEDURE dbo.Account_Commodity_Broker_Fee_Merge
      ( 
       @Account_Id INT
      ,@Commodity_Id INT
      ,@Broker_Fee DECIMAL(28, 10)
      ,@Currency_Unit_Id INT
      ,@UOM_Entity_Id INT )
AS 
BEGIN
      SET NOCOUNT ON
      
      MERGE dbo.Account_Commodity_Broker_Fee AS Tgt
            USING 
                  ( SELECT
                        @Account_Id AS Account_Id
                       ,@Commodity_Id AS Commodity_Id
                       ,@Broker_Fee AS Broker_Fee
                       ,@Currency_Unit_Id AS Currency_Unit_Id
                       ,@UOM_Entity_Id AS UOM_Entity_Id ) AS Src
            ON Tgt.Account_Id = Src.Account_Id
                  AND Tgt.Commodity_Id = Src.Commodity_Id
            WHEN MATCHED 
                  THEN 
                  UPDATE
                    SET 
                        Tgt.Broker_Fee = Src.Broker_Fee
                       ,Tgt.Currency_Unit_Id = Src.Currency_Unit_Id
                       ,Tgt.UOM_Entity_Id = Src.UOM_Entity_Id
            WHEN NOT MATCHED 
                  THEN 
                       INSERT
                        ( 
                         Account_Id
                        ,Commodity_Id
                        ,Broker_Fee
                        ,Currency_Unit_Id
                        ,UOM_Entity_Id )
                    VALUES
                        ( 
                         Src.Account_Id
                        ,Src.Commodity_Id
                        ,Src.Broker_Fee
                        ,Src.Currency_Unit_Id
                        ,Src.UOM_Entity_Id ) ;
      
    
	  
END
;
GO
GRANT EXECUTE ON  [dbo].[Account_Commodity_Broker_Fee_Merge] TO [CBMSApplication]
GO
