SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.Report_DE_IC_KPI            
                        
 DESCRIPTION:                        
			This will allow the managers to gain a better understanding of the scope of officers and their allocations of  chase.
			                  
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                       
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            


   EXEC [dbo].[Report_DE_IC_KPI] 
      @Start_Dt = '2017-01-01'
     ,@End_Dt = '2017-02-01'
       
  
   EXEC [dbo].[Report_DE_IC_KPI] 
      @Start_Dt = '2017-01-01'
     ,@End_Dt = '2017-02-01'
       
   EXEC [dbo].[Report_DE_IC_KPI] 
      @Start_Dt = '2016-01-01'
     ,@End_Dt = '2016-12-01'


  
     
       SELECT top 1 ch.Client_Id FROM core.Client_Hier ch WHERE ch.Client_Name LIKE 'A J Rorato'                    
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam 
 NR					   Narayana Reddy         
         
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2017-01-25       Created for Invoice Tracking.   
 NR					   2018-01-02       SE2017-388 - IC Reports - To excluded the  Demo Clients.                 
            
                       
******/                 
   
               
CREATE  PROCEDURE [dbo].[Report_DE_IC_KPI]
      ( 
       @Invoice_Collection_Officer_User_Id INT = -1
      ,@Start_Dt DATE
      ,@End_Dt DATE
      ,@Is_Invoice_Collection_Source_UBM INT = 0
      ,@UBM_Id INT = -1 )
AS 
BEGIN                
      SET NOCOUNT ON;    
    
      DECLARE
            @UBM_Source INT
           ,@Vendor_Source INT
           ,@Client_Source INT
           ,@Online INT
           ,@Vendor_Primary_Contact INT
           ,@Client_Primary_Contact INT
           ,@Account_Primary_Contact INT
           ,@Mail_Redirect INT
           ,@Issue_Open_Status INT
           ,@IC_Type_ICR_Code_Id INT
           ,@IC_Type_ICE_Code_Id INT
           ,@IC_Exception_Type_UBM_Missing_Code_Id INT
           ,@ICR_Status_Resolved INT
           ,@ICE_Status_Open INT
           ,@ICR_Status_Open INT
           ,@Exclude_Demo_Client_Type_Id INT

      SELECT
            @UBM_Source = MAX(CASE WHEN c.Code_Value = 'UBM' THEN C.Code_Id
                              END)
           ,@Vendor_Source = MAX(CASE WHEN c.Code_Value = 'Vendor' THEN C.Code_Id
                                 END)
           ,@Client_Source = MAX(CASE WHEN c.Code_Value = 'Client' THEN C.Code_Id
                                 END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'InvoiceCollectionSource'
            AND c.Code_Value IN ( 'UBM', 'Vendor', 'Client' )

      SELECT
            @Issue_Open_Status = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            Code_value = 'Open'
            AND cs.Codeset_Name = 'IC Chase Status'

      SELECT
            @IC_Type_ICR_Code_Id = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Invoice Collection Type'
            AND C.Code_Value = 'ICR'
	 
	             
      SELECT
            @IC_Type_ICE_Code_Id = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Invoice Collection Type'
            AND C.Code_Value = 'ICE'
   
      SELECT
            @IC_Exception_Type_UBM_Missing_Code_Id = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'IC Exception Type'
            AND C.Code_Value = 'UBM Missing'
            
      SELECT
            @ICR_Status_Resolved = MAX(CASE WHEN c.Code_Value = 'Resolved' THEN c.Code_Id
                                       END)
           ,@ICR_Status_Open = MAX(CASE WHEN c.Code_Value = 'Open' THEN c.Code_Id
                                   END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            Code_value IN ( 'Resolved', 'Open' )
            AND cs.Codeset_Name = 'ICR Status'
                           
      SELECT
            @ICE_Status_Open = MAX(CASE WHEN c.Code_Value = 'Open' THEN c.Code_Id
                                   END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            Code_value = 'Open'
            AND cs.Codeset_Name = 'ICE Status'
                               
      SELECT
            @Online = MAX(CASE WHEN c.Code_Value = 'Online' THEN C.Code_Id
                          END)
           ,@Vendor_Primary_Contact = MAX(CASE WHEN c.Code_Value = 'Vendor Primary Contact' THEN C.Code_Id
                                          END)
           ,@Client_Primary_Contact = MAX(CASE WHEN c.Code_Value = 'Client Primary Contact' THEN C.Code_Id
                                          END)
           ,@Account_Primary_Contact = MAX(CASE WHEN c.Code_Value = 'Account Primary Contact' THEN C.Code_Id
                                           END)
           ,@Mail_Redirect = MAX(CASE WHEN c.Code_Value = 'Mail Redirect' THEN C.Code_Id
                                 END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'InvoiceSourceType'
            AND c.Code_Value IN ( 'Online', 'Vendor Primary Contact', 'Client Primary Contact', 'Account Primary Contact', 'Mail Redirect' )
   
   
   
   
      SELECT
            @Exclude_Demo_Client_Type_Id = e.ENTITY_ID
      FROM
            dbo.ENTITY e
      WHERE
            ENTITY_NAME = 'Demo'
            AND ENTITY_TYPE = 125
            AND ENTITY_DESCRIPTION = 'Client Type'
            
            
      CREATE TABLE #KPI_Final_Report
            ( 
             ICO_User_Info_Id INT
            ,Col_Type VARCHAR(50)
            ,Col_Val INT )
      
      CREATE TABLE #Icq_Month_Map_Count
            ( 
             Invoice_Collection_Account_Config_Id INT
            ,Seq_No INT
            ,Col_Val INT )  
      
      CREATE TABLE #Inv_Month_Map_Count
            ( 
             Invoice_Collection_Account_Config_Id INT
            ,Seq_No INT
            ,Col_Val INT ) 
                  
      CREATE TABLE #Never_Created_ICR
            ( 
             Invoice_Collection_Account_Config_Id INT
            ,Col_Val INT )

      INSERT      INTO #KPI_Final_Report
                  ( 
                   ICO_User_Info_Id
                  ,Col_Type
                  ,Col_Val )
                  SELECT
                        icac.Invoice_Collection_Officer_User_Id
                       ,'ICR_All'
                       ,COUNT(DISTINCT icq.Invoice_Collection_Queue_Id)
                  FROM
                        dbo.Invoice_Collection_Queue icq
                        INNER JOIN dbo.Invoice_Collection_Account_Config icac
                              ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                        INNER JOIN core.Client_Hier_Account cha
                              ON icac.Account_Id = cha.Account_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        icq.Invoice_Collection_Queue_Type_Cd = @IC_Type_ICR_Code_Id
                        AND ( @Invoice_Collection_Officer_User_Id = -1
                              OR icac.Invoice_Collection_Officer_User_Id = @Invoice_Collection_Officer_User_Id )
                        AND icq.Created_Ts BETWEEN @Start_Dt AND @End_Dt
                        AND icq.Is_Manual = 0
                        AND ( @Is_Invoice_Collection_Source_UBM = -1
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                                INNER JOIN dbo.Account_Invoice_Collection_UBM_Source_Dtl ubm
                                                      ON aic.Account_Invoice_Collection_Source_Id = ubm.Account_Invoice_Collection_Source_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value = 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 1
                                                AND ( @UBM_Id = -1
                                                      OR ubm.UBM_Id = @UBM_Id ) )
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value <> 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 0 ) )
                        AND ch.Client_Type_Id <> @Exclude_Demo_Client_Type_Id
                  GROUP BY
                        icac.Invoice_Collection_Officer_User_Id
                              
      INSERT      INTO #KPI_Final_Report
                  ( 
                   ICO_User_Info_Id
                  ,Col_Type
                  ,Col_Val )
                  SELECT
                        icac.Invoice_Collection_Officer_User_Id
                       ,'ICRs_Brought_Forward'
                       ,COUNT(DISTINCT icq.Invoice_Collection_Queue_Id)
                  FROM
                        dbo.Invoice_Collection_Queue icq
                        INNER JOIN dbo.Invoice_Collection_Account_Config icac
                              ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                        INNER JOIN dbo.Code icqsc
                              ON icqsc.Code_Id = icq.Status_Cd
                        INNER JOIN core.Client_Hier_Account cha
                              ON icac.Account_Id = cha.Account_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        icq.Invoice_Collection_Queue_Type_Cd = @IC_Type_ICR_Code_Id
                        AND ( @Invoice_Collection_Officer_User_Id = -1
                              OR icac.Invoice_Collection_Officer_User_Id = @Invoice_Collection_Officer_User_Id )
                        AND icq.Created_Ts < @Start_Dt
                        AND icq.Is_Manual = 0
                        AND icqsc.Code_Value = 'Open'
                        AND ( @Is_Invoice_Collection_Source_UBM = -1
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                                INNER JOIN dbo.Account_Invoice_Collection_UBM_Source_Dtl ubm
                                                      ON aic.Account_Invoice_Collection_Source_Id = ubm.Account_Invoice_Collection_Source_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value = 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 1
                                                AND ( @UBM_Id = -1
                                                      OR ubm.UBM_Id = @UBM_Id ) )
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value <> 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 0 ) )
                        AND ch.Client_Type_Id <> @Exclude_Demo_Client_Type_Id
                  GROUP BY
                        icac.Invoice_Collection_Officer_User_Id                             
                              

      INSERT      INTO #KPI_Final_Report
                  ( 
                   ICO_User_Info_Id
                  ,Col_Type
                  ,Col_Val )
                  SELECT
                        icac.Invoice_Collection_Officer_User_Id
                       ,'ICRs_Received_Not_Processed'
                       ,COUNT(DISTINCT icq.Invoice_Collection_Queue_Id)
                  FROM
                        dbo.Invoice_Collection_Queue icq
                        INNER JOIN dbo.Invoice_Collection_Account_Config icac
                              ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                        INNER JOIN dbo.Code icqsc
                              ON icqsc.Code_Id = icq.Status_Cd
                        INNER JOIN core.Client_Hier_Account cha
                              ON icac.Account_Id = cha.Account_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        ( @Invoice_Collection_Officer_User_Id = -1
                          OR icac.Invoice_Collection_Officer_User_Id = @Invoice_Collection_Officer_User_Id )
                        AND icqsc.Code_Value IN ( 'Received', 'Resolved', 'Processed' )
                        AND icq.Created_Ts BETWEEN @Start_Dt AND @End_Dt
                        AND icq.Is_Manual = 0
                        AND ( @Is_Invoice_Collection_Source_UBM = -1
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                                INNER JOIN dbo.Account_Invoice_Collection_UBM_Source_Dtl ubm
                                                      ON aic.Account_Invoice_Collection_Source_Id = ubm.Account_Invoice_Collection_Source_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value = 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 1
                                                AND ( @UBM_Id = -1
                                                      OR ubm.UBM_Id = @UBM_Id ) )
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value <> 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 0 ) )
                        AND ch.Client_Type_Id <> @Exclude_Demo_Client_Type_Id
                  GROUP BY
                        icac.Invoice_Collection_Officer_User_Id 


 
--Never Created ICR

              	

      
      INSERT      INTO #Never_Created_ICR
                  ( 
                   Invoice_Collection_Account_Config_Id
                  ,Col_Val )
                  SELECT
                        icac.Invoice_Collection_Account_Config_Id
                       ,COUNT(DISTINCT ic.Account_Invoice_Collection_Month_Id)
                  FROM
                        dbo.Account_Invoice_Collection_Month ic
                        INNER JOIN dbo.Invoice_Collection_Account_Config icac
                              ON ic.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                        INNER JOIN core.Client_Hier_Account cha
                              ON icac.Account_Id = cha.Account_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.Invoice_Collection_Queue_Month_Map icqmm
                                     WHERE
                                          icqmm.Account_Invoice_Collection_Month_Id = ic.Account_Invoice_Collection_Month_Id
                                          AND icqmm.Created_Ts BETWEEN @Start_Dt AND @End_Dt )
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map AB
                                     WHERE
                                          ic.Account_Invoice_Collection_Month_Id = AB.Account_Invoice_Collection_Month_Id
                                          AND AB.Created_Ts BETWEEN @Start_Dt AND @End_Dt )
                        AND icac.Invoice_Collection_Officer_User_Id = @Invoice_Collection_Officer_User_Id
                        AND ch.Client_Type_Id <> @Exclude_Demo_Client_Type_Id
                  GROUP BY
                        icac.Invoice_Collection_Account_Config_Id   
      
      
      
      INSERT      INTO #KPI_Final_Report
                  ( 
                   ICO_User_Info_Id
                  ,Col_Type
                  ,Col_Val )
                  SELECT
                        icac.Invoice_Collection_Officer_User_Id
                       ,'Never_Created_ICR'
                       ,SUM(CASE WHEN Col_Val < 0 THEN 0
                                 ELSE Col_Val
                            END)
                  FROM
                        #Never_Created_ICR nci
                        INNER JOIN dbo.Invoice_Collection_Account_Config icac
                              ON icac.Invoice_Collection_Account_Config_Id = nci.Invoice_Collection_Account_Config_Id
                  GROUP BY
                        icac.Invoice_Collection_Officer_User_Id 



      INSERT      INTO #KPI_Final_Report
                  ( 
                   ICO_User_Info_Id
                  ,Col_Type
                  ,Col_Val )
                  SELECT
                        icac.Invoice_Collection_Officer_User_Id
                       ,'Other_than_UBM_Resolved'
                       ,COUNT(DISTINCT icq.Invoice_Collection_Queue_Id)
                  FROM
                        dbo.Invoice_Collection_Queue icq
                        INNER JOIN dbo.Invoice_Collection_Account_Config icac
                              ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                        INNER JOIN dbo.Code c
                              ON c.Code_Id = icq.Status_Cd
                        INNER JOIN core.Client_Hier_Account cha
                              ON icac.Account_Id = cha.Account_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        icq.Invoice_Collection_Queue_Type_Cd = @IC_Type_ICE_Code_Id
                        AND ( @Invoice_Collection_Officer_User_Id = -1
                              OR icac.Invoice_Collection_Officer_User_Id = @Invoice_Collection_Officer_User_Id )
                        AND c.Code_Value = 'Resolved'
                        AND icq.Invoice_Collection_Exception_Type_Cd <> @IC_Exception_Type_UBM_Missing_Code_Id
                        AND ( icq.Created_Ts BETWEEN @Start_Dt AND @End_Dt )
                        AND icq.Is_Manual = 0
                        AND ( @Is_Invoice_Collection_Source_UBM = -1
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                                INNER JOIN dbo.Account_Invoice_Collection_UBM_Source_Dtl ubm
                                                      ON aic.Account_Invoice_Collection_Source_Id = ubm.Account_Invoice_Collection_Source_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value = 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 1
                                                AND ( @UBM_Id = -1
                                                      OR ubm.UBM_Id = @UBM_Id ) )
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND aic.Is_Primary = 1
                                                AND c.Code_Value <> 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 0 ) )
                        AND ch.Client_Type_Id <> @Exclude_Demo_Client_Type_Id
                  GROUP BY
                        icac.Invoice_Collection_Officer_User_Id 
                  
                  

      INSERT      INTO #KPI_Final_Report
                  ( 
                   ICO_User_Info_Id
                  ,Col_Type
                  ,Col_Val )
                  SELECT
                        icac.Invoice_Collection_Officer_User_Id
                       ,'Other_than_UBM'
                       ,COUNT(DISTINCT icq.Invoice_Collection_Queue_Id)
                  FROM
                        dbo.Invoice_Collection_Queue icq
                        INNER JOIN dbo.Invoice_Collection_Account_Config icac
                              ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                        INNER JOIN core.Client_Hier_Account cha
                              ON icac.Account_Id = cha.Account_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        icq.Invoice_Collection_Queue_Type_Cd = @IC_Type_ICE_Code_Id
                        AND ( @Invoice_Collection_Officer_User_Id = -1
                              OR icac.Invoice_Collection_Officer_User_Id = @Invoice_Collection_Officer_User_Id )
                        AND icq.Status_Cd = @ICE_Status_Open
                        AND icq.Invoice_Collection_Exception_Type_Cd <> @IC_Exception_Type_UBM_Missing_Code_Id
                        AND ( icq.Created_Ts BETWEEN @Start_Dt AND @End_Dt )
                        AND icq.Is_Manual = 0
                        AND ( @Is_Invoice_Collection_Source_UBM = -1
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                                INNER JOIN dbo.Account_Invoice_Collection_UBM_Source_Dtl ubm
                                                      ON aic.Account_Invoice_Collection_Source_Id = ubm.Account_Invoice_Collection_Source_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value = 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 1
                                                AND ( @UBM_Id = -1
                                                      OR ubm.UBM_Id = @UBM_Id ) )
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND aic.Is_Primary = 1
                                                AND c.Code_Value <> 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 0 ) )
                        AND ch.Client_Type_Id <> @Exclude_Demo_Client_Type_Id
                  GROUP BY
                        icac.Invoice_Collection_Officer_User_Id
                                         


      INSERT      INTO #KPI_Final_Report
                  ( 
                   ICO_User_Info_Id
                  ,Col_Type
                  ,Col_Val )
                  SELECT
                        icac.Invoice_Collection_Officer_User_Id
                       ,'ICRs_Received_Not_Processed_1_Week'
                       ,COUNT(DISTINCT icq.Invoice_Collection_Queue_Id)
                  FROM
                        dbo.Invoice_Collection_Queue icq
                        INNER JOIN dbo.Invoice_Collection_Account_Config icac
                              ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                        INNER JOIN dbo.Code icqsc
                              ON icqsc.Code_Id = icq.Status_Cd
                        INNER JOIN core.Client_Hier_Account cha
                              ON icac.Account_Id = cha.Account_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        icq.Received_Status_Updated_Dt IS NOT NULL
                        AND ( @Invoice_Collection_Officer_User_Id = -1
                              OR icac.Invoice_Collection_Officer_User_Id = @Invoice_Collection_Officer_User_Id )
                        AND icqsc.Code_Value IN ( 'Received', 'Resolved', 'Processed' )
                        AND DATEDIFF(DAY, icq.Created_Ts, icq.Received_Status_Updated_Dt) BETWEEN 1
                                                                                          AND     7
                        AND icq.Is_Manual = 0
                        AND ( @Is_Invoice_Collection_Source_UBM = -1
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                                INNER JOIN dbo.Account_Invoice_Collection_UBM_Source_Dtl ubm
                                                      ON aic.Account_Invoice_Collection_Source_Id = ubm.Account_Invoice_Collection_Source_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value = 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 1
                                                AND ( @UBM_Id = -1
                                                      OR ubm.UBM_Id = @UBM_Id ) )
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value <> 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 0 ) )
                        AND ch.Client_Type_Id <> @Exclude_Demo_Client_Type_Id
                  GROUP BY
                        icac.Invoice_Collection_Officer_User_Id 


      INSERT      INTO #KPI_Final_Report
                  ( 
                   ICO_User_Info_Id
                  ,Col_Type
                  ,Col_Val )
                  SELECT
                        icac.Invoice_Collection_Officer_User_Id
                       ,'ICRs_Received_Not_Processed_2_Week'
                       ,COUNT(DISTINCT icq.Invoice_Collection_Queue_Id)
                  FROM
                        dbo.Invoice_Collection_Queue icq
                        INNER JOIN dbo.Invoice_Collection_Account_Config icac
                              ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                        INNER JOIN dbo.Code icqsc
                              ON icqsc.Code_Id = icq.Status_Cd
                        INNER JOIN core.Client_Hier_Account cha
                              ON icac.Account_Id = cha.Account_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        icq.Received_Status_Updated_Dt IS NOT NULL
                        AND ( @Invoice_Collection_Officer_User_Id = -1
                              OR icac.Invoice_Collection_Officer_User_Id = @Invoice_Collection_Officer_User_Id )
                        AND icqsc.Code_Value IN ( 'Received', 'Resolved', 'Processed' )
                        AND DATEDIFF(DAY, icq.Created_Ts, icq.Received_Status_Updated_Dt) BETWEEN 8
                                                                                          AND     14
                        AND icq.Is_Manual = 0
                        AND ( @Is_Invoice_Collection_Source_UBM = -1
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                                INNER JOIN dbo.Account_Invoice_Collection_UBM_Source_Dtl ubm
                                                      ON aic.Account_Invoice_Collection_Source_Id = ubm.Account_Invoice_Collection_Source_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value = 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 1
                                                AND ( @UBM_Id = -1
                                                      OR ubm.UBM_Id = @UBM_Id ) )
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value <> 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 0 ) )
                        AND ch.Client_Type_Id <> @Exclude_Demo_Client_Type_Id
                  GROUP BY
                        icac.Invoice_Collection_Officer_User_Id 
                  
                  
      INSERT      INTO #KPI_Final_Report
                  ( 
                   ICO_User_Info_Id
                  ,Col_Type
                  ,Col_Val )
                  SELECT
                        icac.Invoice_Collection_Officer_User_Id
                       ,'ICRs_Received_Not_Processed_3_Week'
                       ,COUNT(DISTINCT icq.Invoice_Collection_Queue_Id)
                  FROM
                        dbo.Invoice_Collection_Queue icq
                        INNER JOIN dbo.Invoice_Collection_Account_Config icac
                              ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                        INNER JOIN dbo.Code icqsc
                              ON icqsc.Code_Id = icq.Status_Cd
                        INNER JOIN core.Client_Hier_Account cha
                              ON icac.Account_Id = cha.Account_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        icq.Received_Status_Updated_Dt IS NOT NULL
                        AND ( @Invoice_Collection_Officer_User_Id = -1
                              OR icac.Invoice_Collection_Officer_User_Id = @Invoice_Collection_Officer_User_Id )
                        AND icqsc.Code_Value IN ( 'Received', 'Resolved', 'Processed' )
                        AND DATEDIFF(DAY, icq.Created_Ts, icq.Received_Status_Updated_Dt) BETWEEN 15
                                                                                          AND     21
                        AND icq.Is_Manual = 0
                        AND ( @Is_Invoice_Collection_Source_UBM = -1
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                                INNER JOIN dbo.Account_Invoice_Collection_UBM_Source_Dtl ubm
                                                      ON aic.Account_Invoice_Collection_Source_Id = ubm.Account_Invoice_Collection_Source_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value = 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 1
                                                AND ( @UBM_Id = -1
                                                      OR ubm.UBM_Id = @UBM_Id ) )
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value <> 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 0 ) )
                        AND ch.Client_Type_Id <> @Exclude_Demo_Client_Type_Id
                  GROUP BY
                        icac.Invoice_Collection_Officer_User_Id 
                  
                  
                  
      INSERT      INTO #KPI_Final_Report
                  ( 
                   ICO_User_Info_Id
                  ,Col_Type
                  ,Col_Val )
                  SELECT
                        icac.Invoice_Collection_Officer_User_Id
                       ,'ICRs_Received_Not_Processed_4_Week'
                       ,COUNT(DISTINCT icq.Invoice_Collection_Queue_Id)
                  FROM
                        dbo.Invoice_Collection_Queue icq
                        INNER JOIN dbo.Invoice_Collection_Account_Config icac
                              ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                        INNER JOIN dbo.Code icqsc
                              ON icqsc.Code_Id = icq.Status_Cd
                        INNER JOIN core.Client_Hier_Account cha
                              ON icac.Account_Id = cha.Account_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        icq.Received_Status_Updated_Dt IS NOT NULL
                        AND ( @Invoice_Collection_Officer_User_Id = -1
                              OR icac.Invoice_Collection_Officer_User_Id = @Invoice_Collection_Officer_User_Id )
                        AND icqsc.Code_Value IN ( 'Received', 'Resolved', 'Processed' )
                        AND DATEDIFF(DAY, icq.Created_Ts, icq.Received_Status_Updated_Dt) BETWEEN 21
                                                                                          AND     28
                        AND icq.Is_Manual = 0
                        AND ( @Is_Invoice_Collection_Source_UBM = -1
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                                INNER JOIN dbo.Account_Invoice_Collection_UBM_Source_Dtl ubm
                                                      ON aic.Account_Invoice_Collection_Source_Id = ubm.Account_Invoice_Collection_Source_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value = 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 1
                                                AND ( @UBM_Id = -1
                                                      OR ubm.UBM_Id = @UBM_Id ) )
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value <> 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 0 ) )
                        AND ch.Client_Type_Id <> @Exclude_Demo_Client_Type_Id
                  GROUP BY
                        icac.Invoice_Collection_Officer_User_Id 
                  
      INSERT      INTO #KPI_Final_Report
                  ( 
                   ICO_User_Info_Id
                  ,Col_Type
                  ,Col_Val )
                  SELECT
                        icac.Invoice_Collection_Officer_User_Id
                       ,'ICRs_Received_Not_Processed_5_Week'
                       ,COUNT(DISTINCT icq.Invoice_Collection_Queue_Id)
                  FROM
                        dbo.Invoice_Collection_Queue icq
                        INNER JOIN dbo.Invoice_Collection_Account_Config icac
                              ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                        INNER JOIN dbo.Code icqsc
                              ON icqsc.Code_Id = icq.Status_Cd
                        INNER JOIN core.Client_Hier_Account cha
                              ON icac.Account_Id = cha.Account_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        icq.Received_Status_Updated_Dt IS NOT NULL
                        AND ( @Invoice_Collection_Officer_User_Id = -1
                              OR icac.Invoice_Collection_Officer_User_Id = @Invoice_Collection_Officer_User_Id )
                        AND icqsc.Code_Value IN ( 'Received', 'Resolved', 'Processed' )
                        AND DATEDIFF(DAY, icq.Created_Ts, icq.Received_Status_Updated_Dt) BETWEEN 29
                                                                                          AND     35
                        AND icq.Is_Manual = 0
                        AND ( @Is_Invoice_Collection_Source_UBM = -1
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                                INNER JOIN dbo.Account_Invoice_Collection_UBM_Source_Dtl ubm
                                                      ON aic.Account_Invoice_Collection_Source_Id = ubm.Account_Invoice_Collection_Source_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value = 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 1
                                                AND ( @UBM_Id = -1
                                                      OR ubm.UBM_Id = @UBM_Id ) )
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value <> 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 0 ) )
                        AND ch.Client_Type_Id <> @Exclude_Demo_Client_Type_Id
                  GROUP BY
                        icac.Invoice_Collection_Officer_User_Id
                  
      INSERT      INTO #KPI_Final_Report
                  ( 
                   ICO_User_Info_Id
                  ,Col_Type
                  ,Col_Val )
                  SELECT
                        icac.Invoice_Collection_Officer_User_Id
                       ,'ICRs_Received_Not_Processed_6_Week'
                       ,COUNT(DISTINCT icq.Invoice_Collection_Queue_Id)
                  FROM
                        dbo.Invoice_Collection_Queue icq
                        INNER JOIN dbo.Invoice_Collection_Account_Config icac
                              ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                        INNER JOIN dbo.Code icqsc
                              ON icqsc.Code_Id = icq.Status_Cd
                        INNER JOIN core.Client_Hier_Account cha
                              ON icac.Account_Id = cha.Account_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        icq.Received_Status_Updated_Dt IS NOT NULL
                        AND ( @Invoice_Collection_Officer_User_Id = -1
                              OR icac.Invoice_Collection_Officer_User_Id = @Invoice_Collection_Officer_User_Id )
                        AND icqsc.Code_Value IN ( 'Received', 'Resolved', 'Processed' )
                        AND DATEDIFF(DAY, icq.Created_Ts, icq.Received_Status_Updated_Dt) BETWEEN 36
                                                                                          AND     42
                        AND icq.Is_Manual = 0
                        AND ( @Is_Invoice_Collection_Source_UBM = -1
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                                INNER JOIN dbo.Account_Invoice_Collection_UBM_Source_Dtl ubm
                                                      ON aic.Account_Invoice_Collection_Source_Id = ubm.Account_Invoice_Collection_Source_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value = 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 1
                                                AND ( @UBM_Id = -1
                                                      OR ubm.UBM_Id = @UBM_Id ) )
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value <> 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 0 ) )
                        AND ch.Client_Type_Id <> @Exclude_Demo_Client_Type_Id
                  GROUP BY
                        icac.Invoice_Collection_Officer_User_Id
                  
      INSERT      INTO #KPI_Final_Report
                  ( 
                   ICO_User_Info_Id
                  ,Col_Type
                  ,Col_Val )
                  SELECT
                        icac.Invoice_Collection_Officer_User_Id
                       ,'ICRs_Received_Not_Processed_7_Week'
                       ,COUNT(DISTINCT icq.Invoice_Collection_Queue_Id)
                  FROM
                        dbo.Invoice_Collection_Queue icq
                        INNER JOIN dbo.Invoice_Collection_Account_Config icac
                              ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                        INNER JOIN dbo.Code icqsc
                              ON icqsc.Code_Id = icq.Status_Cd
                        INNER JOIN core.Client_Hier_Account cha
                              ON icac.Account_Id = cha.Account_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        icq.Received_Status_Updated_Dt IS NOT NULL
                        AND ( @Invoice_Collection_Officer_User_Id = -1
                              OR icac.Invoice_Collection_Officer_User_Id = @Invoice_Collection_Officer_User_Id )
                        AND icqsc.Code_Value IN ( 'Received', 'Resolved', 'Processed' )
                        AND DATEDIFF(DAY, icq.Created_Ts, icq.Received_Status_Updated_Dt) BETWEEN 43
                                                                                          AND     49
                        AND icq.Is_Manual = 0
                        AND ( @Is_Invoice_Collection_Source_UBM = -1
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                                INNER JOIN dbo.Account_Invoice_Collection_UBM_Source_Dtl ubm
                                                      ON aic.Account_Invoice_Collection_Source_Id = ubm.Account_Invoice_Collection_Source_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value = 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 1
                                                AND ( @UBM_Id = -1
                                                      OR ubm.UBM_Id = @UBM_Id ) )
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value <> 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 0 ) )
                        AND ch.Client_Type_Id <> @Exclude_Demo_Client_Type_Id
                  GROUP BY
                        icac.Invoice_Collection_Officer_User_Id
                  

      INSERT      INTO #KPI_Final_Report
                  ( 
                   ICO_User_Info_Id
                  ,Col_Type
                  ,Col_Val )
                  SELECT
                        icac.Invoice_Collection_Officer_User_Id
                       ,'ICRs_Received_Not_Processed_8_Week'
                       ,COUNT(DISTINCT icq.Invoice_Collection_Queue_Id)
                  FROM
                        dbo.Invoice_Collection_Queue icq
                        INNER JOIN dbo.Invoice_Collection_Account_Config icac
                              ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                        INNER JOIN dbo.Code icqsc
                              ON icqsc.Code_Id = icq.Status_Cd
                        INNER JOIN core.Client_Hier_Account cha
                              ON icac.Account_Id = cha.Account_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        icq.Received_Status_Updated_Dt IS NOT NULL
                        AND ( @Invoice_Collection_Officer_User_Id = -1
                              OR icac.Invoice_Collection_Officer_User_Id = @Invoice_Collection_Officer_User_Id )
                        AND icqsc.Code_Value IN ( 'Received', 'Resolved', 'Processed' )
                        AND DATEDIFF(DAY, icq.Created_Ts, icq.Received_Status_Updated_Dt) BETWEEN 50
                                                                                          AND     56
                        AND icq.Is_Manual = 0
                        AND ( @Is_Invoice_Collection_Source_UBM = -1
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                                INNER JOIN dbo.Account_Invoice_Collection_UBM_Source_Dtl ubm
                                                      ON aic.Account_Invoice_Collection_Source_Id = ubm.Account_Invoice_Collection_Source_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value = 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 1
                                                AND ( @UBM_Id = -1
                                                      OR ubm.UBM_Id = @UBM_Id ) )
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value <> 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 0 ) )
                        AND ch.Client_Type_Id <> @Exclude_Demo_Client_Type_Id
                  GROUP BY
                        icac.Invoice_Collection_Officer_User_Id
                  
                  
      INSERT      INTO #KPI_Final_Report
                  ( 
                   ICO_User_Info_Id
                  ,Col_Type
                  ,Col_Val )
                  SELECT
                        icac.Invoice_Collection_Officer_User_Id
                       ,'ICRs_Received_Not_Processed_8to_Week'
                       ,COUNT(DISTINCT icq.Invoice_Collection_Queue_Id)
                  FROM
                        dbo.Invoice_Collection_Queue icq
                        INNER JOIN dbo.Invoice_Collection_Account_Config icac
                              ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                        INNER JOIN dbo.Code icqsc
                              ON icqsc.Code_Id = icq.Status_Cd
                        INNER JOIN core.Client_Hier_Account cha
                              ON icac.Account_Id = cha.Account_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        icq.Received_Status_Updated_Dt IS NOT NULL
                        AND ( @Invoice_Collection_Officer_User_Id = -1
                              OR icac.Invoice_Collection_Officer_User_Id = @Invoice_Collection_Officer_User_Id )
                        AND icqsc.Code_Value IN ( 'Received', 'Resolved', 'Processed' )
                        AND DATEDIFF(DAY, icq.Created_Ts, icq.Received_Status_Updated_Dt) > 56
                        AND icq.Is_Manual = 0
                        AND ( @Is_Invoice_Collection_Source_UBM = -1
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                                INNER JOIN dbo.Account_Invoice_Collection_UBM_Source_Dtl ubm
                                                      ON aic.Account_Invoice_Collection_Source_Id = ubm.Account_Invoice_Collection_Source_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value = 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 1
                                                AND ( @UBM_Id = -1
                                                      OR ubm.UBM_Id = @UBM_Id ) )
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value <> 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 0 ) )
                        AND ch.Client_Type_Id <> @Exclude_Demo_Client_Type_Id
                  GROUP BY
                        icac.Invoice_Collection_Officer_User_Id


      SELECT
            Invoice_Collection_Officer AS [Invoice Collection Officer]
           ,ISNULL(ICR_All, 0) AS ICR_All
           ,ISNULL(ICRs_Brought_Forward, 0) AS ICRs_Brought_Forward
           ,ISNULL(ICRs_Received_Not_Processed, 0) AS ICRs_Received_Not_Processed
           ,ISNULL(never_created_ICR, 0) AS never_created_ICR
           ,ISNULL(Other_than_UBM_Resolved, 0) AS Other_than_UBM_Resolved
           ,ISNULL(Other_than_UBM, 0) AS Other_than_UBM
           ,ISNULL(ICRs_Received_Not_Processed_1_Week, 0) AS ICRs_Received_Not_Processed_1_Week
           ,ISNULL(ICRs_Received_Not_Processed_2_Week, 0) AS ICRs_Received_Not_Processed_2_Week
           ,ISNULL(ICRs_Received_Not_Processed_3_Week, 0) AS ICRs_Received_Not_Processed_3_Week
           ,ISNULL(ICRs_Received_Not_Processed_4_Week, 0) AS ICRs_Received_Not_Processed_4_Week
           ,ISNULL(ICRs_Received_Not_Processed_5_Week, 0) AS ICRs_Received_Not_Processed_5_Week
           ,ISNULL(ICRs_Received_Not_Processed_6_Week, 0) AS ICRs_Received_Not_Processed_6_Week
           ,ISNULL(ICRs_Received_Not_Processed_7_Week, 0) AS ICRs_Received_Not_Processed_7_Week
           ,ISNULL(ICRs_Received_Not_Processed_8_Week, 0) AS ICRs_Received_Not_Processed_8_Week
           ,ISNULL(ICRs_Received_Not_Processed_8to_Week, 0) AS ICRs_Received_Not_Processed_8to_Week
           ,ICO_User_Info_Id AS USER_INFO_ID
           ,@Start_Dt AS Start_Dt
           ,@End_Dt AS End_Dt
           ,@Is_Invoice_Collection_Source_UBM AS Is_Invoice_Collection_Source_UBM
           ,@UBM_Id AS UBM_Id
      FROM
            ( SELECT
                  ICO_User_Info_Id
                 ,ui.FIRST_NAME + ' ' + ui.LAST_NAME Invoice_Collection_Officer
                 ,Col_Val
                 ,Col_Type
              FROM
                  #KPI_Final_Report kfp
                  INNER JOIN dbo.USER_INFO ui
                        ON kfp.ICO_User_Info_Id = ui.USER_INFO_ID ) src PIVOT( MAX(Col_Val) FOR Col_Type IN ( [ICR_All], [ICRs_Brought_Forward], [ICRs_Received_Not_Processed], [Never_Created_ICR], [Other_than_UBM_Resolved], [Other_than_UBM], [ICRs_Received_Not_Processed_1_Week], [ICRs_Received_Not_Processed_2_Week], [ICRs_Received_Not_Processed_3_Week], [ICRs_Received_Not_Processed_4_Week], [ICRs_Received_Not_Processed_5_Week], [ICRs_Received_Not_Processed_6_Week], [ICRs_Received_Not_Processed_7_Week], [ICRs_Received_Not_Processed_8_Week], [ICRs_Received_Not_Processed_8to_Week] ) ) piv
      DROP TABLE #KPI_Final_Report
      DROP TABLE #Icq_Month_Map_Count
      DROP TABLE #Inv_Month_Map_Count
      DROP TABLE  #Never_Created_ICR



END





;
GO

GRANT EXECUTE ON  [dbo].[Report_DE_IC_KPI] TO [CBMS_SSRS_Reports]
GO
