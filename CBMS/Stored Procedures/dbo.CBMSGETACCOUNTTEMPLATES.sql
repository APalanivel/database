SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*=============================================================      
      
NAME: CBMSGETACCOUNTTEMPLATES    
  
DESCRIPTION:      
USED BY DTS PACKAGE ACCOUNTTEMPLATES TO GENERATE COMMAND DELIMITED FILE THAT IS SEND TO PROKARM      
  
INPUT PARAMETERS:      
NONE      
  
OUTPUT PARAMETERS:      
NONE         
  
EXAMPLE:      
EXEC CBMSGETACCOUNTTEMPLATES    
  
AUTHOR LEGEND:      
DMR   DEANA RITTER      
PD	  PADMANAVA DEBNATH
	  
MAINTENANCE LOG:      
WHO  WHEN  WHAT      
--- ---------- ----------------------------------------------      
DMR  4/14/2009   ADDED ACCOUNT_ID TO THE SELECT STATEMENT      
SSR  03/18/2010 Removed Cu_invoice_Account_map(Account_id) with Cu_invoice_service_month(account_ID)   
SSR  03/23/2010 Removed View vwsitename  
     Removed Nolock and added table Owner Name (Partaily Modified)  
SSR  04/22/2010 Removed Distinct and added group by Clause  
SKA  05/28/2010 Added the TODO statement  
PD	 11/02/2010 Added a WHERE clause to filter out AT&T data  
TODO:  
 This needs to be fine tuned, duplication of tables removed, views removed.  
//=============================================================*/     
CREATE PROCEDURE dbo.CBMSGETACCOUNTTEMPLATES      
AS      
BEGIN      
  
SET NOCOUNT ON;  
    
DECLARE @INVOICE TABLE   
 (ACCOUNT_ID INT NOT NULL  
 , CU_INVOICE_ID  INT NOT NULL)      
  
 INSERT INTO @INVOICE ( ACCOUNT_ID,CU_INVOICE_ID)       
 SELECT   
    CISM.ACCOUNT_ID      
  , MAX(CU.CU_INVOICE_ID) CU_INVOICE_ID      
 FROM   
  dbo.CU_INVOICE CU   
  JOIN dbo.CU_INVOICE_SERVICE_MONTH CISM   
   ON CISM.CU_INVOICE_ID = CU.CU_INVOICE_ID      
  JOIN (SELECT   
      SM.ACCOUNT_ID      
    , MAX(CU.UPDATED_DATE) UPDATED_DATE      
   FROM   
    dbo.CU_INVOICE CU   
    JOIN dbo.CU_INVOICE_SERVICE_MONTH SM   
     ON SM.CU_INVOICE_ID = CU.CU_INVOICE_ID      
    JOIN dbo.CU_INVOICE_CHARGE D   
     ON D.CU_INVOICE_ID = CU.CU_INVOICE_ID  
   WHERE   
    CU.IS_PROCESSED = 1      
    AND (CU.UBM_ID IS NULL OR CU.UBM_ID IN (1, 2))      
    AND D.CHARGE_VALUE IS NOT NULL      
   GROUP BY   
    SM.ACCOUNT_ID       
   ) X   
   ON X.ACCOUNT_ID = CISM.ACCOUNT_ID   
    AND X.UPDATED_DATE = CU.UPDATED_DATE      
  JOIN dbo.CU_INVOICE_CHARGE D   
   ON D.CU_INVOICE_ID = CU.CU_INVOICE_ID      
 WHERE   
  CU.IS_PROCESSED = 1      
  AND (CU.UBM_ID IS NULL OR CU.UBM_ID IN (1, 2))      
  AND D.CHARGE_VALUE IS NOT NULL      
 GROUP BY   
  CISM.ACCOUNT_ID    
  
 SELECT   
   CL.CLIENT_NAME      
  , RTRIM(ad.city) + ', ' + st.state_name + ' (' + s.site_name + ')' Site_Name  
  , VAS.ACCOUNT_NUMBER      
  , V.VENDOR_NAME      
  , X.ACCOUNT_ID      
  , X.CU_INVOICE_ID      
  , SM.SERVICE_MONTH      
 FROM   
  dbo.SITE s     
  JOIN dbo.ADDRESS ad   
   ON ad.ADDRESS_ID =s.PRIMARY_ADDRESS_ID  
  JOIN dbo.STATE st   
   ON st.STATE_ID =ad.STATE_ID  
  JOIN dbo.VWCBMSACCOUNTSITE VAS   
   ON VAS.SITE_ID = s.SITE_ID      
  JOIN @INVOICE X   
   ON X.ACCOUNT_ID = VAS.ACCOUNT_ID      
  JOIN dbo.ACCOUNT A   
   ON A.ACCOUNT_ID = VAS.ACCOUNT_ID      
  JOIN dbo.VENDOR V   
   ON V.VENDOR_ID = A.VENDOR_ID      
  JOIN dbo.CLIENT CL   
   ON CL.CLIENT_ID = s.CLIENT_ID      
  JOIN dbo.CU_INVOICE_SERVICE_MONTH SM   
   ON SM.CU_INVOICE_ID = X.CU_INVOICE_ID      
  WHERE CL.CLIENT_ID != 11231 
  GROUP BY   
    CL.CLIENT_NAME      
  , ad.city  
  , st.state_name   
  , s.site_name   
  , VAS.ACCOUNT_NUMBER      
  , V.VENDOR_NAME      
  , X.ACCOUNT_ID      
  , X.CU_INVOICE_ID      
  , SM.SERVICE_MONTH        
  
END 
GO
GRANT EXECUTE ON  [dbo].[CBMSGETACCOUNTTEMPLATES] TO [CBMSApplication]
GO
