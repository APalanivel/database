
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.CU_Invoice_Upd_Invoice_Type

DESCRIPTION:
	 To Update CU_Invoice table Invoice

 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
Cu_Invoice_Id					INT
Invoice_Type_Cd					INT				NULL
Updated_By_Id					INT  	

 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      

 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
BEGIN TRAN
Select Cu_Invoice_Id,Meter_Read_Type_Cd,Updated_By_Id,Updated_Date from dbo.cu_invoice where cu_invoice_id=11685552
EXEC dbo.CU_Invoice_Upd_Invoice_Type 
      @Cu_Invoice_Id = 11685552
     ,@Meter_Read_Type_Cd = 100690
     ,@Updated_By_Id = 21665
Select Cu_Invoice_Id,Meter_Read_Type_Cd,Updated_By_Id,Updated_Date from dbo.cu_invoice where cu_invoice_id=11685552  
ROLLBACK

 AUTHOR INITIALS:        
       
	Initials            Name        
---------------------------------------------------------------------------------------------------------------                      
	SP					Sandeep Pigilam
	NR					Narayana Reddy

 MODIFICATIONS:      
          
 Initials              Date            Modification      
---------------------------------------------------------------------------------------------------------------      
  SP				   2014-08-07	   Created for Data Operations Enhancements Phase III.
  NR				   2016-12-23	   MAINT-4737 replaced Invoice_Type_Cd with Meter_Read_Type_Cd and Added @Invoice_Type_Cd
										As Input.

******/

CREATE PROCEDURE [dbo].[CU_Invoice_Upd_Invoice_Type]
      ( 
       @Cu_Invoice_Id INT
      ,@Meter_Read_Type_Cd INT = NULL
      ,@Updated_By_Id INT
      ,@Invoice_Type_Cd INT = NULL )
AS 
BEGIN
      SET NOCOUNT ON  
      UPDATE
            dbo.CU_Invoice
      SET   
            Meter_Read_Type_Cd = @Meter_Read_Type_Cd
           ,Updated_By_Id = @Updated_By_Id
           ,Updated_Date = GETDATE()
           ,Invoice_Type_Cd = @Invoice_Type_Cd
      WHERE
            Cu_Invoice_Id = @Cu_Invoice_Id
END;


;
GO

GRANT EXECUTE ON  [dbo].[CU_Invoice_Upd_Invoice_Type] TO [CBMSApplication]
GO
