SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.dbo.Sr_Batch_Log_Details_Del_Orphan_Utility_Account_By_Contract_Id

DESCRIPTION:

	To delete the orphan utility account from SR_BATCH_LOG_DETAILS for the given contract_id

INPUT PARAMETERS:
	Name					DataType		Default	Description
----------------------------------------------------------------
	@contract_id   			INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	BEGIN TRAN

		--SELECT * FROM SR_BATCH_LOG_DETAILS WHERE Contract_id = 28521 AND ACcount_Id = 20420

		EXEC dbo.Sr_Batch_Log_Details_Del_Orphan_Utility_Account_By_Contract_Id  28521

		--SELECT * FROM SR_BATCH_LOG_DETAILS WHERE Contract_id = 28521  AND ACcount_Id = 20420

	ROLLBACK TRAN

	BEGIN TRAN

		SELECT * FROM SR_BATCH_LOG_DETAILS WHERE Contract_id = 29388 And Account_id = 25163

		EXEC dbo.Sr_Batch_Log_Details_Del_Orphan_Utility_Account_By_Contract_Id  29388

		SELECT * FROM SR_BATCH_LOG_DETAILS WHERE Contract_id = 29388 And Account_id = 25163
	ROLLBACK TRAN

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan G

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG        	04/25/2011	MAINT-487 Created to delete the orphan utility account associated with

******/

CREATE PROCEDURE dbo.Sr_Batch_Log_Details_Del_Orphan_Utility_Account_By_Contract_Id ( @contract_id INT )
AS
BEGIN

      SET NOCOUNT ON

      DECLARE @Contract_Utility_Account_List TABLE
		( Account_Id INT PRIMARY KEY CLUSTERED)

	  DECLARE @Sr_Batch_Log_Details_List TABLE
	  (Sr_Batch_Log_Details_id INT PRIMARY KEY CLUSTERED)

	  DECLARE @Sr_Batch_Log_Details_Id INT

	  BEGIN TRY	
			BEGIN TRAN

			  INSERT INTO @Contract_Utility_Account_List
				(Account_id )
			  SELECT
					m.Account_Id
			  FROM
					dbo.SUPPLIER_ACCOUNT_METER_MAP samm
					INNER JOIN dbo.Meter m
						  ON samm.Meter_Id = m.Meter_Id
			  WHERE
					samm.Contract_Id = @Contract_Id
			  GROUP BY
					m.Account_id

			  -- Save the orphan utilty account of Contract
			  INSERT INTO @Sr_Batch_Log_Details_List
					  ( Sr_Batch_Log_Details_id )
			  SELECT
					bld.SR_BATCH_LOG_DETAILS_ID
			  FROM
					dbo.SR_BATCH_LOG_DETAILS bld
			  WHERE
					bld.Contract_Id = @Contract_Id
					AND NOT EXISTS ( SELECT
									  1
									 FROM
									  @Contract_Utility_Account_List ual
									 WHERE
									  ual.Account_Id = bld.ACCOUNT_ID )

			  -- Delete the records based on the PK of Sr_Batch_Log_Details
			  WHILE EXISTS(SELECT 1 FROM @Sr_Batch_Log_Details_List)
			  BEGIN

					SET @Sr_Batch_Log_Details_Id = (SELECT TOP 1 Sr_Batch_Log_Details_Id FROM @Sr_Batch_Log_Details_List)

					EXEC dbo.SR_Batch_Log_Details_Del @Sr_Batch_Log_Details_Id

					DELETE bld
					FROM
						@Sr_Batch_Log_Details_List bld
					WHERE
						Sr_Batch_Log_Details_id = @Sr_Batch_Log_Details_Id

			  END

			COMMIT TRAN
	  END TRY
	  BEGIN CATCH
			IF @@TRANCOUNT>0 ROLLBACK TRAN

	  END CATCH

END
GO
GRANT EXECUTE ON  [dbo].[Sr_Batch_Log_Details_Del_Orphan_Utility_Account_By_Contract_Id] TO [CBMSApplication]
GO
