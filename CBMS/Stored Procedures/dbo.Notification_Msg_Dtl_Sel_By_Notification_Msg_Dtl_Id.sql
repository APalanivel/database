SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** 

NAME:
		dbo.Notification_Msg_Dtl_Sel_By_Notification_Msg_Dtl_Id 

DESCRIPTION: 
				To get full details of notification deliveried

INPUT PARAMETERS: 
Name						DataType		Default		Description 
----------------------------------------------------------------------
@Notification_Msg_Queue_Id	INT

OUTPUT PARAMETERS:   
Name						DataType	Default		Description 
---------------------------------------------------------------------- 

USAGE EXAMPLES:   
----------------------------------------------------------------------   

	EXEC dbo.Notification_Msg_Dtl_Sel_By_Notification_Msg_Dtl_Id 10558


AUTHOR INITIALS:   
	Initials	Name   
----------------------------------------------------------------------   
	RR			Raghu Reddy


MODIFICATIONS:  
	Initials	Date		Modification 
-------------------------------------------------------------------------------   
	RR			2019-02-22  Created  GRM
	 	 		 
******/
CREATE PROCEDURE [dbo].[Notification_Msg_Dtl_Sel_By_Notification_Msg_Dtl_Id]
      ( 
       @Notification_Msg_Dtl_Id INT )
AS 
BEGIN 

      SET NOCOUNT ON;
      
      SELECT
            nmd.Recipient_Email_Address
           ,nmd.CC_Email_Address
           ,nmq.Email_Subject
           ,nmq.Email_Body
           ,nma.Cbms_Image_Id
      FROM
            dbo.Notification_Msg_Dtl nmd
            INNER JOIN dbo.Notification_Msg_Queue nmq
                  ON nmd.Notification_Msg_Queue_Id = nmq.Notification_Msg_Queue_Id
            LEFT JOIN dbo.Notification_Msg_Attachment nma
                  ON nmq.Notification_Msg_Queue_Id = nma.Notification_Msg_Queue_Id
      WHERE
            nmd.Notification_Msg_Dtl_Id = @Notification_Msg_Dtl_Id;
      
END
GO
GRANT EXECUTE ON  [dbo].[Notification_Msg_Dtl_Sel_By_Notification_Msg_Dtl_Id] TO [CBMSApplication]
GO
