SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME: dbo.Utility_Budget_Comments_Del

DESCRIPTION:
	
	Used to delete all the Utility_Budget_Comments for the given utility_Budget_COmments_Id

INPUT PARAMETERS:
	NAME						DATATYPE	DEFAULT		DESCRIPTION     
----------------------------------------------------------------
	@Utility_Budget_Comments_Id	INT
                
OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------         
	USAGE EXAMPLES:
------------------------------------------------------------ 

	BEGIN TRAN

		EXEC dbo.Utility_Budget_Comments_Del 2431

	ROLLBACK TRAN

	SELECT
		* FROM VENDOR v 
	WHERE Vendor_Type_id = 289
		and exists(select 1 from Utility_Budget_Comments a where a.vendor_id = v.vendor_id)

	SELECT * FROM utility_Budget_Comments WHERE vendor_id = 468

AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	HG			Harihara Suthan G

MODIFICATIONS
	INITIALS	DATE			MODIFICATION
------------------------------------------------------------
	HG			9/24/2010		Created

*/
CREATE PROCEDURE dbo.Utility_Budget_Comments_Del
    (
       @Utility_Budget_Comments_Id	INT
    )
AS
BEGIN

    SET NOCOUNT ON ;

	DELETE
		dbo.Utility_Budget_Comments
	WHERE
		Utility_Budget_Comments_Id = @Utility_Budget_Comments_Id

END
GO
GRANT EXECUTE ON  [dbo].[Utility_Budget_Comments_Del] TO [CBMSApplication]
GO
