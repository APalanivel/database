SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.Invoice_Collection_Client_Config_Merge            
                        
 DESCRIPTION:                        
			To Update and insert data for Invoice_Collection_Client_Config table                  
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
 @Client_Id								 INT
,@Is_Active								 INT
,@Invoice_Collection_Service_Start_Dt    DATE
,@Invoice_Collection_Service_End_Dt		 DATE
,@Invoice_Collection_Officer_User_Id	 INT
,@Invoice_Collection_Coordinator_User_Id INT
,@Invoice_Collection_Service_Level_Cd	 INT
,@User_Info_Id							 INT 
                       
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 --INSERT
 BEGIN TRAN
 SELECT * FROM dbo.Invoice_Collection_Client_Config WHERE Client_Id=235
 EXEC [dbo].[Invoice_Collection_Client_Config_Merge] 
      @Client_Id = 235
      ,@Is_Active = 1
      ,@Invoice_Collection_Service_Start_Dt ='2016-01-01'
      ,@Invoice_Collection_Service_End_Dt ='2016-12-01'
      ,@Invoice_Collection_Officer_User_Id =49
      ,@Invoice_Collection_Coordinator_User_Id =49
      ,@Invoice_Collection_Service_Level_Cd =102252
      ,@User_Info_Id =49
      ,@Invoice_Collection_Client_Config_Id=NULL
      SELECT * FROM dbo.Invoice_Collection_Client_Config WHERE Client_Id=235
 ROLLBACK         

--UPDATE
 BEGIN TRAN
 SELECT * FROM dbo.Invoice_Collection_Client_Config WHERE Client_Id=235
 EXEC [dbo].[Invoice_Collection_Client_Config_Merge] 
      @Client_Id = 235
      ,@Is_Active = 1
      ,@Invoice_Collection_Service_Start_Dt ='2016-01-01'
      ,@Invoice_Collection_Service_End_Dt ='2016-12-01'
      ,@Invoice_Collection_Officer_User_Id =49
      ,@Invoice_Collection_Coordinator_User_Id =49
      ,@Invoice_Collection_Service_Level_Cd =102252
      ,@User_Info_Id =49
      ,@Invoice_Collection_Client_Config_Id=NULL

 EXEC [dbo].[Invoice_Collection_Client_Config_Merge] 
      @Client_Id = 235
      ,@Is_Active = 1
      ,@Invoice_Collection_Service_Start_Dt ='2016-02-01'
      ,@Invoice_Collection_Service_End_Dt ='2016-12-01'
      ,@Invoice_Collection_Officer_User_Id =49
      ,@Invoice_Collection_Coordinator_User_Id =49
      ,@Invoice_Collection_Service_Level_Cd =102252
      ,@User_Info_Id =49   
      ,@Invoice_Collection_Client_Config_Id=NULL   
      SELECT * FROM dbo.Invoice_Collection_Client_Config WHERE Client_Id=235
 ROLLBACK  
 
                        
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2016-11-17       Created for Invoice Tracking Phase I.               
                       
******/                 
                
CREATE PROCEDURE [dbo].[Invoice_Collection_Client_Config_Merge]
      ( 
       @Client_Id INT
      ,@Is_Active INT
      ,@Invoice_Collection_Service_Start_Dt DATE
      ,@Invoice_Collection_Service_End_Dt DATE
      ,@Invoice_Collection_Officer_User_Id INT
      ,@Invoice_Collection_Coordinator_User_Id INT
      ,@Invoice_Collection_Service_Level_Cd INT
      ,@User_Info_Id INT
      ,@Invoice_Collection_Setup_Instruction_Comment NVARCHAR(MAX)
      ,@Invoice_Collection_Client_Config_Id INT OUTPUT )
AS 
BEGIN                
      SET NOCOUNT ON;    
    
      MERGE INTO dbo.Invoice_Collection_Client_Config AS tgt
            USING 
                  ( SELECT
                        @Client_Id AS Client_Id
                       ,@Is_Active AS Is_Active
                       ,@Invoice_Collection_Service_Start_Dt AS Invoice_Collection_Service_Start_Dt
                       ,@Invoice_Collection_Service_End_Dt AS Invoice_Collection_Service_End_Dt
                       ,@Invoice_Collection_Officer_User_Id AS Invoice_Collection_Officer_User_Id
                       ,@Invoice_Collection_Coordinator_User_Id AS Invoice_Collection_Coordinator_User_Id
                       ,@Invoice_Collection_Service_Level_Cd AS Invoice_Collection_Service_Level_Cd
                       ,@Invoice_Collection_Setup_Instruction_Comment AS Invoice_Collection_Setup_Instruction_Comment
                       ,@User_Info_Id AS User_Info_Id
                       ,GETDATE() AS Ts ) AS src
            ON ( tgt.[Is_Active] = src.[Is_Active] )
                  AND ( tgt.[Client_Id] = src.[Client_Id] )
            WHEN MATCHED 
                  THEN UPDATE
                    SET 
                        Invoice_Collection_Service_Start_Dt = src.Invoice_Collection_Service_Start_Dt
                       ,Invoice_Collection_Service_End_Dt = src.Invoice_Collection_Service_End_Dt
                       ,Invoice_Collection_Officer_User_Id = src.Invoice_Collection_Officer_User_Id
                       ,Invoice_Collection_Coordinator_User_Id = src.Invoice_Collection_Coordinator_User_Id
                       ,Invoice_Collection_Service_Level_Cd = src.Invoice_Collection_Service_Level_Cd
                       ,Invoice_Collection_Setup_Instruction_Comment = src.Invoice_Collection_Setup_Instruction_Comment
                       ,Updated_User_Id = src.User_Info_Id
                       ,Last_Change_Ts = src.Ts
            WHEN NOT MATCHED 
                  THEN INSERT
                        ( 
                         Client_Id
                        ,Is_Active
                        ,Invoice_Collection_Service_Start_Dt
                        ,Invoice_Collection_Service_End_Dt
                        ,Invoice_Collection_Officer_User_Id
                        ,Invoice_Collection_Coordinator_User_Id
                        ,Invoice_Collection_Service_Level_Cd
                        ,Invoice_Collection_Setup_Instruction_Comment
                        ,Created_User_Id
                        ,Created_Ts
                        ,Updated_User_Id
                        ,Last_Change_Ts )
                    VALUES
                        ( 
                         src.Client_Id
                        ,src.Is_Active
                        ,src.Invoice_Collection_Service_Start_Dt
                        ,src.Invoice_Collection_Service_End_Dt
                        ,src.Invoice_Collection_Officer_User_Id
                        ,src.Invoice_Collection_Coordinator_User_Id
                        ,src.Invoice_Collection_Service_Level_Cd
                        ,src.Invoice_Collection_Setup_Instruction_Comment
                        ,src.User_Info_Id
                        ,src.Ts
                        ,src.User_Info_Id
                        ,src.Ts );
      SELECT
            @Invoice_Collection_Client_Config_Id = SCOPE_IDENTITY()
				  
END;

;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Client_Config_Merge] TO [CBMSApplication]
GO
