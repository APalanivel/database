SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
  
/******    
NAME:    
 dbo.Standing_Data_Exception_Group_Map_Sel  
    
DESCRIPTION:    
    
INPUT PARAMETERS:    
 Name          DataType    Default    Description    
----------------------      ---------------   -------------------------------------------    
@Standing_Data_Exception_Group_Map_Id       INT   
    
OUTPUT PARAMETERS:    
 Name   DataType  Default    Description    
--------------------------------------------------------------------------------    
    
USAGE EXAMPLES:    
--------------------------------------------------------------------------------    
    
EXEC dbo.Standing_Data_Exception_Group_Map_Sel     
    
AUTHOR INITIALS:      
 Initials     Name      
-----------    -------------------------------------------------      
 HKT       Harish Kumar Tirumandyam    
    
MODIFICATIONS    
    
MODIFICATIONS      
      
 Initials      Date       Modification      
-------------     ------------    -----------------------------------      
HKT         2020-04-23       created for getting Standing_Data_Exceptions for Sub Bucket  
  
  
******/  
  
  
CREATE PROCEDURE [dbo].[Standing_Data_Exception_Group_Map_Sel]  
    (  
        @Standing_Data_Exception_Group_Map_Id INT = NULL  
    )  
AS  
    BEGIN  
  
        SELECT  
            cet.Standing_Data_Exception_Group_Map_Id  
            , cet.Exception_Type_Cd  
            , cet.Exception_Desc  
            , cd.Code_Dsc Exception_Name  
            , cet.Managed_By_Group_Info_Id  
   , gi.GROUP_NAME   
            , cet.Managed_By_User_Info_Id  
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Username  
            , cet.Created_User_Id  
            , cet.Created_Ts  
            , cet.Updated_User_Id  
            , cet.Last_Change_Ts  
        FROM  
            dbo.Standing_Data_Exception_Group_Map cet WITH (NOLOCK)  
            LEFT JOIN dbo.USER_INFO ui WITH (NOLOCK)  
                ON ui.USER_INFO_ID = cet.Managed_By_User_Info_Id  
            JOIN Code cd WITH (NOLOCK)  
                ON cd.Code_Id = cet.Exception_Type_Cd  
            LEFT JOIN dbo.group_info gi  WITH (NOLOCK)  
      ON gi.GROUP_INFO_ID = cet.Managed_By_Group_Info_Id  
        WHERE  
            (   @Standing_Data_Exception_Group_Map_Id IS NULL  
                OR  Standing_Data_Exception_Group_Map_Id = @Standing_Data_Exception_Group_Map_Id)  
        ORDER BY  
            cet.Exception_Type_Cd;  
  
  
  
    END;  
  
  
GO
GRANT EXECUTE ON  [dbo].[Standing_Data_Exception_Group_Map_Sel] TO [CBMSApplication]
GO
