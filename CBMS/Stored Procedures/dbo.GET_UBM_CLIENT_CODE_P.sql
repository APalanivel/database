SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE  PROCEDURE dbo.GET_UBM_CLIENT_CODE_P
	@clientID int
	AS
	begin
		set nocount on

		select 	distinct UBM_CLIENT_CODE 
		from 	ubm_client_map 
		where	client_id = @clientID 

	end
GO
GRANT EXECUTE ON  [dbo].[GET_UBM_CLIENT_CODE_P] TO [CBMSApplication]
GO
