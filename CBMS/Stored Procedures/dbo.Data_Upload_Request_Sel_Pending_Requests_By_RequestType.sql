SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/******************************************************************************************************      
NAME : dbo.Data_Upload_Request_Sel_Pending_Requests_By_RequestType 
     
DESCRIPTION:   
Stored Procedure is used to get the pending requests for a given request type
 
 INPUT PARAMETERS:      
 Name             DataType               Default        Description      
--------------------------------------------------------------------   


 OUTPUT PARAMETERS:      
 Name			DataType				Default			Description      
--------------------------------------------------------------------      
DATA_UPLOAD_QUEUE_ID	INT
REQUEST_HANDLER_URL		VARCHAR
  
  USAGE EXAMPLES:      
--------------------------------------------------------------------      
EXEC [dbo].[Data_Upload_Request_Sel_Pending_Requests_By_RequestType] 'BudgetUsage',1
    
AUTHOR INITIALS:      
Initials           Name      
-------------------------------------------------------------------      
RC                Rao Chejarla
RT				  Romy Thomas
RMG				  Rani Mary George
     
 MODIFICATIONS       
Initials Date         Modification      
--------------------------------------------------------------------
RC		 1/2/2012     Copied Data_Upload_Request_Pending_SEL from CBMS and modified
RT		 09/24/2012	  New filed EMAIL_ADDRESS included in select list
RMG      03/08/2013   Copied from DVDehub
******************************************************************************************************/  
CREATE PROCEDURE [dbo].[Data_Upload_Request_Sel_Pending_Requests_By_RequestType]
      ( 
       @RequestType VARCHAR(200)
      ,@PendingMinutes INT )
AS 
BEGIN

      SET NOCOUNT ON ;
      DECLARE @PENDING_STATUS_CODE INT
     
      SELECT
            @PENDING_STATUS_CODE = C.CODE_ID
      FROM
            dbo.CODE c
            INNER JOIN CODESET cs
                  ON c.CODESET_ID = cs.CODESET_ID
                     AND cs.CODESET_NAME = 'Data Upload Status'
                     AND c.CODE_DSC = 'Pending'

      SELECT
            duq.Data_Upload_Request_Type_Id
           ,duq.DATA_UPLOAD_QUEUE_ID
           ,duq.REQUESTED_USER_INFO_ID
           ,duq.CBMS_IMAGE_ID
           ,duq.STATUS_CD
           ,duq.SOURCE_DSC
           ,ui.EMAIL_ADDRESS
      FROM
            dbo.DATA_UPLOAD_QUEUE duq
            INNER JOIN dbo.DATA_UPLOAD_REQUEST_TYPE durt
                  ON duq.DATA_UPLOAD_REQUEST_TYPE_ID = durt.DATA_UPLOAD_REQUEST_TYPE_ID
                     AND duq.STATUS_CD = @PENDING_STATUS_CODE
                     AND durt.Request_Name = @RequestType
                     AND datediff(MINUTE, duq.CREATE_TS, getdate()) > @PendingMinutes
            LEFT JOIN dbo.USER_INFO ui
                  ON duq.Requested_User_Info_Id = ui.USER_INFO_ID 
END ;



;
GO
GRANT EXECUTE ON  [dbo].[Data_Upload_Request_Sel_Pending_Requests_By_RequestType] TO [CBMSApplication]
GO
