SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[P2P_u_dboSR_RFP_ACCOUNT_TERM1218899476]
		@c1 int = NULL,
		@c2 int = NULL,
		@c3 int = NULL,
		@c4 bit = NULL,
		@c5 bit = NULL,
		@c6 datetime = NULL,
		@c7 datetime = NULL,
		@c8 int = NULL,
		@c9 bit = NULL,
		@c10 uniqueidentifier = NULL,
		@pkc1 int = NULL,
		@bitmap binary(2),
		@MSp2pPreVersion varbinary(32) , @MSp2pPostVersion varbinary(32) 

as
begin  
update [dbo].[SR_RFP_ACCOUNT_TERM] set
		[SR_RFP_TERM_ID] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [SR_RFP_TERM_ID] end,
		[SR_ACCOUNT_GROUP_ID] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [SR_ACCOUNT_GROUP_ID] end,
		[IS_BID_GROUP] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [IS_BID_GROUP] end,
		[IS_SOP] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [IS_SOP] end,
		[FROM_MONTH] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [FROM_MONTH] end,
		[TO_MONTH] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [TO_MONTH] end,
		[NO_OF_MONTHS] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [NO_OF_MONTHS] end,
		[IS_SDP] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [IS_SDP] end,
		[msrepl_tran_version] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [msrepl_tran_version] end		,$sys_p2p_cd_id = @MSp2pPostVersion

where [SR_RFP_ACCOUNT_TERM_ID] = @pkc1
		and ($sys_p2p_cd_id = @MSp2pPreVersion or $sys_p2p_cd_id is null)
if @@rowcount = 0
begin  
	declare @cur_version varbinary(32) 
		,@conflict_type int = 1
		,@conflict_type_txt nvarchar(20) = N'Update-Update'
		,@reason_code int = 1
		,@reason_text nvarchar(720) = NULL
		,@is_on_disk_winner bit = 0
		,@is_incoming_winner bit = 0
		,@peer_id_current_node int = NULL
		,@peer_id_incoming int
		,@tranid_incoming nvarchar(40)
		,@peer_id_on_disk int
		,@tranid_on_disk nvarchar(40)
	select @peer_id_incoming = sys.fn_replvarbintoint(@MSp2pPostVersion)
		,@tranid_incoming = sys.fn_replp2pversiontotranid(@MSp2pPostVersion)
	select @cur_version = $sys_p2p_cd_id from [dbo].[SR_RFP_ACCOUNT_TERM] 
where [SR_RFP_ACCOUNT_TERM_ID] = @pkc1
	if @@rowcount = 0  
			select @conflict_type = 3, @conflict_type_txt = N'Update-Delete', @is_on_disk_winner = 1, @reason_text = formatmessage(22823), @reason_code = 0
	else 
	begin  
		select @peer_id_on_disk = sys.fn_replvarbintoint(@cur_version)
			,@tranid_on_disk = sys.fn_replp2pversiontotranid(@cur_version)
		if(@peer_id_incoming > @peer_id_on_disk)
			set @is_incoming_winner = 1
		else
			set @is_on_disk_winner = 1
	end  
		select @peer_id_current_node = p.originator_id from syspublications p join sysarticles a on a.pubid = p.pubid where a.objid = object_id(N'[dbo].[SR_RFP_ACCOUNT_TERM]') and p.options & 0x1 = 0x1
	if (@peer_id_current_node is not null) 
	begin 
		if (@reason_text is NULL)
			if (@peer_id_incoming > @peer_id_on_disk)
			begin  
				select @reason_text = formatmessage(22822,@peer_id_incoming,@peer_id_on_disk,@peer_id_current_node)
			end  
			else  
			begin  
				select @reason_text = formatmessage(22821,@peer_id_incoming,@peer_id_on_disk,@peer_id_current_node)
			end  
		create table #change_id (change_id varbinary(8))
		insert [dbo].[conflict_dbo_SR_RFP_ACCOUNT_TERM] (
		[SR_RFP_ACCOUNT_TERM_ID],
		[SR_RFP_TERM_ID],
		[SR_ACCOUNT_GROUP_ID],
		[IS_BID_GROUP],
		[IS_SOP],
		[FROM_MONTH],
		[TO_MONTH],
		[NO_OF_MONTHS],
		[IS_SDP],
		[msrepl_tran_version]
			,__$originator_id
			,__$origin_datasource
			,__$tranid
			,__$conflict_type
			,__$is_winner
			,__$reason_code
			,__$reason_text
			,__$update_bitmap
			,__$pre_version)
		output inserted.__$row_id into #change_id
		select 
    @pkc1,
    @c2,
    @c3,
    @c4,
    @c5,
    @c6,
    @c7,
    @c8,
    @c9,
    @c10			,@peer_id_current_node
			,@peer_id_incoming
			,@tranid_incoming
			,@conflict_type
			,@is_incoming_winner
			,@reason_code
			,@reason_text
			,@bitmap
			,@MSp2pPreVersion
		insert [dbo].[conflict_dbo_SR_RFP_ACCOUNT_TERM] (

		[SR_RFP_ACCOUNT_TERM_ID],
		[SR_RFP_TERM_ID],
		[SR_ACCOUNT_GROUP_ID],
		[IS_BID_GROUP],
		[IS_SOP],
		[FROM_MONTH],
		[TO_MONTH],
		[NO_OF_MONTHS],
		[IS_SDP],
		[msrepl_tran_version]			,__$originator_id
			,__$origin_datasource
			,__$tranid
			,__$conflict_type
			,__$is_winner
			,__$reason_code
			,__$reason_text
			,__$pre_version
			,__$change_id)
		select 

		[SR_RFP_ACCOUNT_TERM_ID],
		[SR_RFP_TERM_ID],
		[SR_ACCOUNT_GROUP_ID],
		[IS_BID_GROUP],
		[IS_SOP],
		[FROM_MONTH],
		[TO_MONTH],
		[NO_OF_MONTHS],
		[IS_SDP],
		[msrepl_tran_version]			,@peer_id_current_node
			,@peer_id_on_disk
			,@tranid_on_disk
			,@conflict_type
			,@is_on_disk_winner
			,@reason_code
			,@reason_text
			,NULL
			,(select change_id from #change_id)
		from [dbo].[SR_RFP_ACCOUNT_TERM] 

where [SR_RFP_ACCOUNT_TERM_ID] = @pkc1
	end 
	if(@peer_id_incoming > @peer_id_on_disk)
	begin  
update [dbo].[SR_RFP_ACCOUNT_TERM] set
		[SR_RFP_TERM_ID] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [SR_RFP_TERM_ID] end,
		[SR_ACCOUNT_GROUP_ID] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [SR_ACCOUNT_GROUP_ID] end,
		[IS_BID_GROUP] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [IS_BID_GROUP] end,
		[IS_SOP] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [IS_SOP] end,
		[FROM_MONTH] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [FROM_MONTH] end,
		[TO_MONTH] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [TO_MONTH] end,
		[NO_OF_MONTHS] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [NO_OF_MONTHS] end,
		[IS_SDP] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [IS_SDP] end,
		[msrepl_tran_version] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [msrepl_tran_version] end		,$sys_p2p_cd_id = @MSp2pPostVersion

where [SR_RFP_ACCOUNT_TERM_ID] = @pkc1
	end  
		if exists(select * from syspublications p join sysarticles a on a.pubid = p.pubid where a.objid = object_id(N'[dbo].[SR_RFP_ACCOUNT_TERM]') and p.options & 0x10 = 0x10)
			raiserror(22815, 10, -1, @conflict_type_txt, @peer_id_current_node, @peer_id_incoming, @tranid_incoming, @peer_id_on_disk, @tranid_on_disk) with log
		else
			raiserror(22815, 16, -1, @conflict_type_txt, @peer_id_current_node, @peer_id_incoming, @tranid_incoming, @peer_id_on_disk, @tranid_on_disk) with log
end 
end 
GO
