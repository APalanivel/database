SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_LP_DELETE_COMMENTS_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
	@user_id varchar(10),
	@session_id varchar(20),
	@sr_rfp_lp_comment_id int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_LP_DELETE_COMMENTS_P
	@user_id varchar(10),
	@session_id varchar(20),
	@sr_rfp_lp_comment_id int
	AS
	
SET NOCOUNT ON

	
	DELETE 
		sr_rfp_lp_comment
	WHERE 
		sr_rfp_lp_comment_id = @sr_rfp_lp_comment_id
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_LP_DELETE_COMMENTS_P] TO [CBMSApplication]
GO
