SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	cmbs.dbo.etl_Get_Budget_Currency


DESCRIPTION:
	Gets all changes made between the MinDBTS and the MaxDBTS. 
	This procedure is stricly used for ETL applications 


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MinDBTS		bigint					minimum row version 
	@MaxDBTS		bigint        	        max row version   	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
EXEC dbo.etl_Get_Budget_Currency 100000000, 1000000000

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CMH			Chad Hattabaugh
	HG			Harihara Suthan G

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	CMH			06/08/2009	Created        	
	HG			10/21/2011	MAINT-861, In the query which pull the data for Source_Column = 'Currency_Conversion_Factor' 
								fixed the code to pull the Base_Unit_Id in place of Currency_Unit_Id to match with the logic of source_Column = 'BUDGET_CURRENCY_MAP'
	HG			11/01/2011	Renamed the table @T1 to @Budget_Currency_Conversion, Aliased the tables and qualified the columns irrespective of number of tables in the query.
							Prepopulated the USD currency id in a scalar variable and used it wherever hardcoded.
	HG			11/01/2011	Changed the date format to MM/DD/YYYY in the comments header
	HG			11/15/2011	MAINT-887, Changed the not exists clause to check the record based on Base_Unit_Id instead of Converted_Unit_Id in the query pulls the data for source column 'CURRENCY_UNIT_CONVERSION'
******/
CREATE PROCEDURE dbo.etl_Get_Budget_Currency
      ( 
       @MinDBTS BIGINT
      ,@MaxDBTS BIGINT )
AS 
BEGIN     
      SET NOCOUNT ON     

      DECLARE @Budget_Currency_Conversion TABLE
            ( 
             Budget_ID INT
            ,Currency_Unit_Id INT
            ,Conversion_Factor DECIMAL(32, 16)
            ,Source_Column VARCHAR(25) )     

      DECLARE @USD_Currency_Id INT
	  
      SELECT
            @USD_Currency_Id = cu.Currency_Unit_Id
      FROM
            dbo.CURRENCY_UNIT cu
      WHERE
            cu.SYMBOL = 'USD'

	  -- Default currency conversion value entered from CBMS
      INSERT      @Budget_Currency_Conversion
                  ( 
                   Budget_Id
                  ,Currency_Unit_Id
                  ,Conversion_Factor
                  ,Source_Column )
                  SELECT
                        BCM.BUDGET_ID
                       ,BCM.CURRENCY_UNIT_ID
                       ,max(BCM.CONVERSION_FACTOR)
                       ,N'BUDGET_CURRENCY_MAP'
                  FROM
                        dbo.BUDGET_CURRENCY_MAP BCM
                        INNER JOIN dbo.BUDGET B
                              ON BCM.BUDGET_ID = B.BUDGET_ID
                  WHERE
                        B.ROW_VERSION BETWEEN @MinDBTS AND @MaxDBTS
                        AND B.IS_POSTED_TO_DV = 1
                  GROUP BY
                        BCM.BUDGET_ID
                       ,BCM.CURRENCY_UNIT_ID    



      INSERT      @Budget_Currency_Conversion
                  ( 
                   Budget_Id
                  ,Currency_Unit_Id
                  ,Conversion_Factor
                  ,Source_Column )
                  SELECT
                        B.Budget_Id
                       ,CUC.BASE_UNIT_ID
                       ,CUC.Conversion_Factor
                       ,N'CURRENCY_UNIT_CONVERSION'
                  FROM
                        dbo.CLIENT C
                        INNER JOIN dbo.Budget B
                              ON C.Client_Id = B.Client_Id
                        INNER JOIN dbo.Currency_Unit_Conversion CUC
                              ON CUC.Currency_Group_Id = C.Currency_Group_Id
                                 AND CUC.Conversion_Date = convert(DATETIME, convert(VARCHAR, b.budget_start_year))
                  WHERE
                        B.ROW_VERSION BETWEEN @MinDBTS AND @MaxDBTS
                        AND B.is_Posted_to_DV = 1
                        AND CUC.Converted_Unit_Id = @USD_Currency_Id
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          @Budget_Currency_Conversion bcc
                                         WHERE
                                          bcc.Budget_Id = b.Budget_Id
                                          AND bcc.Currency_Unit_Id = cuc.BASE_UNIT_ID )    


      INSERT      @Budget_Currency_Conversion
                  ( 
                   Budget_Id
                  ,Currency_Unit_Id
                  ,Conversion_Factor
                  ,Source_Column )
                  SELECT
                        b.Budget_Id
                       ,@USD_Currency_Id AS CURRENCY_UNIT_ID
                       ,1 AS CONVERSION_FACTOR
                       ,N'Default'
                  FROM
                        dbo.Budget B
                        CROSS JOIN dbo.Currency_Unit cu
                  WHERE
                        cu.Symbol = 'USD'
                        AND B.Is_Posted_To_DV = 1
                        AND B.ROW_VERSION BETWEEN @MinDBTS AND @MaxDBTS
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          @Budget_Currency_Conversion bcc
                                         WHERE
                                          bcc.Budget_ID = B.Budget_ID
                                          AND bcc.Currency_Unit_Id = @USD_Currency_Id )


      SELECT
            bcc.Budget_ID
           ,bcc.Currency_Unit_Id
           ,bcc.Source_Column
           ,bcc.Conversion_Factor
      FROM
            @Budget_Currency_Conversion bcc
      ORDER BY
            bcc.Budget_ID
           ,bcc.Currency_Unit_Id
           ,bcc.Source_Column
           ,bcc.Conversion_Factor
END
GO

GRANT EXECUTE ON  [dbo].[etl_Get_Budget_Currency] TO [CBMSApplication]
GO