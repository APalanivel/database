SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO





CREATE PROCEDURE [dbo].[cbmsNYISO_Detail_GetAvgDailyForZoneAndDate]  
	( @zone_id int
	, @start_date datetime
	, @end_date datetime
	, @pricing_type varchar(50)
	)
AS
BEGIN

	   select 
		 convert(varchar(10),date,101) as date
		, avg(zd.lbmp) as lbmp
	     from ny_iso_detail zd
	    where zone_id = @zone_id
	      and convert(varchar(10),date,101) >= @start_date
	      and convert(varchar(10),date,101) <= @end_date
	      and pricing_type = @pricing_type
	  group by convert(varchar(10),date,101) 
    	  order by convert(varchar(10),date,101) 
		

END


GO
GRANT EXECUTE ON  [dbo].[cbmsNYISO_Detail_GetAvgDailyForZoneAndDate] TO [CBMSApplication]
GO
