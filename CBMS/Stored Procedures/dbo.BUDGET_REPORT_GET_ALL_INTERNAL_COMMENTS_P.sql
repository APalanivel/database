SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.BUDGET_REPORT_GET_ALL_INTERNAL_COMMENTS_P
	@budget_id int,
	@clientId int,
	@divisionId int,
	@siteId int ,
	@commodityId int
	AS
	begin
		set nocount on

		select	case when owner_type_id = 701 --//Division level comments
			then d.division_id
			when owner_type_id = 702 --//Site level comments
			then s.site_id
			else --// Account level comments
			budget_account.budget_account_id
			end comments_key,
			budget_account.budget_account_id,
			comments.budget_detail_comments
			

	 
		from 	budget 
			join budget_account on budget_account.budget_id = budget.budget_id
			and budget.commodity_type_id = @commodityId
			and budget.budget_id = @budget_id
			join budget_detail_comments_owner_map comments on comments.budget_account_id = budget_account.budget_account_id
			join account utilacc on utilacc.account_id = budget_account.account_id
			JOIN SITE S WITH(NOLOCK) ON (utilacc.site_id = s.site_id and S.SITE_ID = COALESCE(@siteId, s.site_id))
			JOIN DIVISION D WITH(NOLOCK) ON (D.DIVISION_ID = S.DIVISION_ID AND  D.DIVISION_ID = COALESCE(@divisionId, D.DIVISION_ID))
			JOIN CLIENT C WITH(NOLOCK) ON (C.CLIENT_ID = D.CLIENT_ID AND C.CLIENT_ID = COALESCE(@clientId, c.client_id))

	end





GO
GRANT EXECUTE ON  [dbo].[BUDGET_REPORT_GET_ALL_INTERNAL_COMMENTS_P] TO [CBMSApplication]
GO
