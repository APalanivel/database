SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_GROUP_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@groupName     	varchar(200)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE           PROCEDURE dbo.GET_GROUP_INFO_P

@userId varchar(10),
@sessionId varchar(20), 
@groupName varchar(200)

AS
set nocount on
	SELECT 	GROUP_INFO_ID, QUEUE_ID, GROUP_EMAIL_ADDRESS
	FROM 	GROUP_INFO
	WHERE	GROUP_NAME = @groupName
GO
GRANT EXECUTE ON  [dbo].[GET_GROUP_INFO_P] TO [CBMSApplication]
GO
