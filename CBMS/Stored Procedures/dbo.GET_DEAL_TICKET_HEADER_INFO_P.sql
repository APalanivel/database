SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.GET_DEAL_TICKET_HEADER_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@dealTicketId  	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC dbo.GET_DEAL_TICKET_HEADER_INFO_P 1,1,105070

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/24/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on.
							Commented code remvoed.
	
******/

CREATE PROCEDURE dbo.GET_DEAL_TICKET_HEADER_INFO_P
    @userId			VARCHAR(10),
    @sessionId		VARCHAR(20),
    @dealTicketId	INT
AS 
BEGIN
    SET NOCOUNT ON;
    
    DECLARE @selectClause VARCHAR(1000)
    DECLARE @fromClause	  VARCHAR(1000)
    DECLARE @whereClause  VARCHAR(1000)
    DECLARE @SQLStatement VARCHAR(8000)
    DECLARE @statusCheck  INT
    DECLARE @cancelCheck  INT
    DECLARE @isBid		  BIT
    DECLARE @isManageBid  BIT
    DECLARE @potentialCheck INT
    DECLARE @openCheck      INT
    DECLARE @fixedCheck		INT 
    DECLARE @countImageId INT
    DECLARE @PFCheck	  INT
	

	
    SELECT  @isManageBid = COUNT(1)
    FROM    RM_DEAL_TICKET_COUNTER_PARTY
    WHERE   IS_MANAGE_BID = 1
            AND RM_DEAL_TICKET_ID = @dealTicketId
	
    SELECT  @potentialCheck = ( SELECT  COUNT(1)
                                FROM    RM_DEAL_TICKET_TRANSACTION_STATUS rmdtts,
                                        ENTITY e
                                WHERE   rmdtts.RM_DEAL_TICKET_ID = @dealTicketId
                                        AND rmdtts.DEAL_TRANSACTION_STATUS_TYPE_ID = e.ENTITY_ID
                                        AND e.ENTITY_NAME LIKE ( 'Potentially Fixed' )
                                        AND rmdtts.DEAL_TRANSACTION_DATE IN (
                                        SELECT  MAX(DEAL_TRANSACTION_DATE)
                                        FROM    RM_DEAL_TICKET_TRANSACTION_STATUS
                                        WHERE   RM_DEAL_TICKET_ID = @dealTicketId )
                              ) 

    SELECT  @statusCheck = ( SELECT COUNT(1)
                             FROM   RM_DEAL_TICKET_TRANSACTION_STATUS rmdtts,
                                    ENTITY e
                             WHERE  rmdtts.RM_DEAL_TICKET_ID = @dealTicketId
                                    AND rmdtts.DEAL_TRANSACTION_STATUS_TYPE_ID = e.ENTITY_ID
                                    AND e.ENTITY_NAME IN (
                                    'CLIENT CONFIRMATION',
                                    'COUNTERPARTY CONFIRMATION' )
                           )
		

    SELECT  @cancelCheck = ( SELECT COUNT(1)
                             FROM   RM_DEAL_TICKET_TRANSACTION_STATUS rmdtts,
                                    ENTITY e
                             WHERE  rmdtts.RM_DEAL_TICKET_ID = @dealTicketId
                                    AND rmdtts.DEAL_TRANSACTION_STATUS_TYPE_ID = e.ENTITY_ID
                                    AND e.ENTITY_NAME IN ( 'CANCELLED' )
                           )


    SELECT  @openCheck = ( SELECT   COUNT(1)
                           FROM     RM_DEAL_TICKET_DETAILS rmdtd,
                                    ENTITY e
                           WHERE    rmdtd.RM_DEAL_TICKET_ID = @dealTicketId
                                    AND rmdtd.TRIGGER_STATUS_TYPE_ID = e.ENTITY_ID
                                    AND e.ENTITY_NAME LIKE ( 'Open' )
                         ) 


    SELECT  @fixedCheck = ( SELECT  COUNT(1)
                            FROM    RM_DEAL_TICKET_DETAILS rmdtd,
                                    ENTITY e
                            WHERE   rmdtd.RM_DEAL_TICKET_ID = @dealTicketId
                                    AND rmdtd.TRIGGER_STATUS_TYPE_ID = e.ENTITY_ID
                                    AND e.ENTITY_NAME LIKE ( 'Fixed' )
                          )

    SELECT  @PFCheck = ( SELECT COUNT(1)
                         FROM   RM_DEAL_TICKET_DETAILS rmdtd,
                                ENTITY e
                         WHERE  rmdtd.RM_DEAL_TICKET_ID = @dealTicketId
                                AND rmdtd.TRIGGER_STATUS_TYPE_ID = e.ENTITY_ID
                                AND e.ENTITY_NAME LIKE ( 'Potentially Fixed' )
                       ) 


    SELECT  @countImageId = ( SELECT    COUNT(client_confirm_cbms_image_id)
                                        + COUNT(cp_confirm_cbms_image_id)
                              FROM      rm_deal_ticket_details
                              WHERE     rm_deal_ticket_id = @dealTicketId
                                        AND ( client_confirm_cbms_image_id IS NOT NULL
                                              OR cp_confirm_cbms_image_id IS NOT NULL
                                            )
                            )

	
    SELECT  @isBid = IS_BID
    FROM    RM_DEAL_TICKET
    WHERE   RM_DEAL_TICKET_ID = @dealTicketId

    SELECT  @selectClause = 'rmdt.DEAL_TICKET_NUMBER,  ' + 'c.CLIENT_ID,  '
            + 'c.CLIENT_NAME,  ' + 'rmdt.CONTACT_NAME,  '
            + 'd.DIVISION_NAME,  ' + 's.SITE_NAME,  ' + 'rmdt.CONTACT_NAME,  '
            + 'rmcp.COUNTERPARTY_NAME,  ' + 'v.VENDOR_NAME,  '
            + 'rmdt.COUNTERPARTY_CONTACT_NAME,  '
            + 'e1.ENTITY_NAME AS HEDGE_NAME,  '
            + 'e2.ENTITY_NAME AS HEDGE_LEVEL_NAME,  '
            + 'e3.ENTITY_NAME AS ACTION_REQUIRED_NAME,  '
            + 'e4.ENTITY_NAME AS FREQUENCY_NAME,  '
            + 'e5.ENTITY_NAME AS UNIT_NAME, '
            + 'e6.ENTITY_NAME AS PRICING_REQUEST_NAME,  '
            + 'e7.ENTITY_NAME AS INDEX_NAME,  ' + 'pin.PRICING_POINT,  '
            + 'e8.ENTITY_NAME AS DEAL_TYPE,  '
            + 'rmdt.HEDGE_MODE_TYPE_ID AS HEDGE_MODE_ID,  '
            + 'e9.ENTITY_NAME AS HEDGE_MODE_NAME,  '
            + 'e10.CURRENCY_UNIT_NAME AS CURRENCY_NAME,  '
            + 'e11.ENTITY_NAME AS ORDER_CONFIRMATION,  '
            + 'e13.ENTITY_NAME AS DEAL_TRANSACTION, '
            + 'rmdt.IS_INPUT_BY_CLIENT, ' + 'con.ED_CONTRACT_NUMBER, '
            + 'rmgroup.GROUP_NAME, ' + 'rmdt.IS_BID, '
            + 'rmdt.HEDGE_START_MONTH, ' + 'rmdt.HEDGE_END_MONTH, '
            + 'e14.ENTITY_NAME AS HEDGE  ' 
			
    SELECT  @fromClause = ' CLIENT c,   ' + 'PRICE_INDEX pin,   '
            + 'ENTITY e1 , ENTITY e2 , ENTITY e3, ENTITY e4, ENTITY e5,   '
            + 'ENTITY e6, ENTITY e7, ENTITY e8,  ENTITY e9, CURRENCY_UNIT e10,  '
            + 'RM_DEAL_TICKET rmdt   '
            + 'LEFT JOIN DIVISION d ON (rmdt.DIVISION_ID = d.DIVISION_ID)  '
            + 'LEFT JOIN vwSiteName s ON (rmdt.SITE_ID = s.SITE_ID)  '
            + 'LEFT JOIN RM_COUNTERPARTY rmcp ON (rmdt.RM_COUNTERPARTY_ID = rmcp.RM_COUNTERPARTY_ID)	'
            + 'LEFT JOIN VENDOR v ON (rmdt.VENDOR_ID = v.VENDOR_ID)  '
            + 'LEFT JOIN ENTITY e11 ON (rmdt.CONFIRMATION_TYPE_ID = e11.ENTITY_ID)  '
            + 'LEFT JOIN ENTITY e13 ON (rmdt.DEAL_TRANSACTION_TYPE_ID = e13.ENTITY_ID)  '
            + 'LEFT JOIN CONTRACT con ON (rmdt.CONTRACT_ID = con.CONTRACT_ID)  '
            + 'LEFT JOIN RM_GROUP rmgroup ON (rmdt.RM_GROUP_ID = rmgroup.RM_GROUP_ID)  '
            + 'LEFT JOIN ENTITY e14 ON (rmdt.HEDGE_LEVEL_TYPE_ID = e14.ENTITY_ID)  '  
				

    SELECT  @whereClause = ' rmdt.RM_DEAL_TICKET_ID = ' + STR(@dealTicketId)
            + ' AND   ' + 'rmdt.CLIENT_ID = c.CLIENT_ID AND   '
            + 'rmdt.HEDGE_TYPE_ID = e1.ENTITY_ID AND  '
            + 'rmdt.HEDGE_LEVEL_TYPE_ID = e2.ENTITY_ID AND  '
            + 'rmdt.ACTION_REQUIRED_TYPE_ID = e3.ENTITY_ID AND  '
            + 'rmdt.FREQUENCY_TYPE_ID = e4.ENTITY_ID AND  '
            + 'rmdt.UNIT_TYPE_ID = e5.ENTITY_ID AND  '
            + 'rmdt.PRICING_REQUEST_TYPE_ID = e6.ENTITY_ID AND  '
            + 'rmdt.PRICE_INDEX_ID = pin.PRICE_INDEX_ID AND  '
            + 'pin.INDEX_ID = e7.ENTITY_ID AND  '
            + 'rmdt.DEAL_TYPE_ID = e8.ENTITY_ID AND  '
            + 'rmdt.HEDGE_MODE_TYPE_ID = e9.ENTITY_ID AND   '
            + 'rmdt.CURRENCY_TYPE_ID = e10.CURRENCY_UNIT_ID ' 
			
			
    IF @isBid = 1
        AND @isManageBid > 0 
        BEGIN
            SELECT  @selectClause = @selectClause
                    + ' , rmcp1.COUNTERPARTY_NAME as BID_COUNTER_PARTY,  rdtcp.COUNTERPARTY_CONTACT_NAME as BID_CONTACT'
			
            SELECT  @fromClause = @fromClause
                    + ' , RM_DEAL_TICKET_COUNTER_PARTY rdtcp LEFT JOIN RM_COUNTERPARTY rmcp1 ON (rdtcp.RM_COUNTERPARTY_ID = rmcp1.RM_COUNTERPARTY_ID) '

            SELECT  @whereClause = @whereClause
                    + 'AND rmdt.RM_DEAL_TICKET_ID = rdtcp.RM_DEAL_TICKET_ID AND '
                    + 'rdtcp.IS_MANAGE_BID = 1 '
        END 

    ELSE 
        IF @isBid = 1
            AND @isManageBid = 0 
            BEGIN		
                SELECT  @selectClause = @selectClause
                        + ', ''Bid''  as BID_COUNTER_PARTY, ''Bid'' as BID_CONTACT '	
            END
		
    IF @potentialCheck = 1
        AND @PFCheck > 0 
        BEGIN	
            IF @cancelCheck = 0 
                BEGIN
                    SELECT  @selectClause = @selectClause
                            + ', ''Potentially Fixed'' AS DEAL_TICKET_STATUS '
                END
            ELSE 
                BEGIN
                    SELECT  @selectClause = @selectClause
                            + ', ''Cancelled'' AS DEAL_TICKET_STATUS '
                END
        END
    ELSE 
        IF @potentialCheck = 1
            AND @PFCheck = 0 
            BEGIN
		
                SELECT  @selectClause = @selectClause
                        + ', ''Order Executed'' AS DEAL_TICKET_STATUS '
		
		
            END
	
        ELSE 
            IF @potentialCheck = 0 
                BEGIN	
                    IF @statusCheck = 1 
                        BEGIN
                            IF @cancelCheck = 0 
                                BEGIN
                                    SELECT  @selectClause = @selectClause
                                            + ', ''Order Executed'' AS DEAL_TICKET_STATUS '
					
                                END
                            ELSE 
                                BEGIN
                                    SELECT  @selectClause = @selectClause
                                            + ', ''Cancelled'' AS DEAL_TICKET_STATUS '
                                END
                        END
                    ELSE 
                        IF @statusCheck = 2 
                            BEGIN
                                IF @openCheck > 0 
                                    BEGIN
                                        SELECT  @selectClause = @selectClause
                                                + ', ''Order Executed'' AS DEAL_TICKET_STATUS '
                                    END
                                ELSE 
                                    IF @openCheck = 0 
                                        BEGIN
                                            IF @countImageId > 0
                                                AND @fixedCheck > 0
                                                AND @countImageId = 2
                                                * @fixedCheck 
                                                BEGIN
                                                    IF @cancelCheck = 0 
                                                        BEGIN
                                                            SELECT  @selectClause = @selectClause + ', ''Closed''  AS DEAL_TICKET_STATUS '
                                                        END
                                                    ELSE 
                                                        BEGIN
                                                            SELECT  @selectClause = @selectClause + ', ''Cancelled'' AS DEAL_TICKET_STATUS '
                                                        END
					
                                                END

                                            ELSE 
                                                BEGIN
                                                    IF @cancelCheck = 0 
                                                        BEGIN
                                                            SELECT  @selectClause = @selectClause + ', ''Order Executed''  AS DEAL_TICKET_STATUS '
                                                        END
                                                    ELSE 
                                                        BEGIN
                                                            SELECT  @selectClause = @selectClause + ', ''Cancelled'' AS DEAL_TICKET_STATUS '
                                                        END
                                                END
                                        END
                            END

                        ELSE 
                            IF @statusCheck = 0 
                                BEGIN
                                    SELECT  @selectClause = @selectClause
                                            + ', e12.ENTITY_NAME AS DEAL_TICKET_STATUS '


                                    SELECT  @fromClause = @fromClause
                                            + ' , RM_DEAL_TICKET_TRANSACTION_STATUS rmdtts, '
                                            + ' ENTITY e12 '

                                    SELECT  @whereClause = @whereClause
                                            + ' AND rmdt.RM_DEAL_TICKET_ID = rmdtts.RM_DEAL_TICKET_ID AND '
                                            + ' rmdtts.DEAL_TRANSACTION_STATUS_TYPE_ID = e12.ENTITY_ID '
                                            + ' ORDER BY rmdtts.DEAL_TRANSACTION_DATE desc '

                                END

	
                END
	
    SELECT  @SQLStatement = 'SELECT TOP 1 ' + @selectClause + ' FROM '
            + @fromClause + ' WHERE ' + @whereClause 
            
    EXEC ( @SQLStatement )
    
END
GO
GRANT EXECUTE ON  [dbo].[GET_DEAL_TICKET_HEADER_INFO_P] TO [CBMSApplication]
GO
