SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [dbo].[cbmsPermissionInfo_Get]
	( @MyAccountId int 
	, @permission_info_id int
	)
AS
BEGIN

	   select permission_info_id 
		, permission_name
		, permission_description
	     from permission_info
	    where permission_info_id = @permission_info_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsPermissionInfo_Get] TO [CBMSApplication]
GO
