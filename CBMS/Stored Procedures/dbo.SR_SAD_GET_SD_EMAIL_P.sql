SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE dbo.SR_SAD_GET_SD_EMAIL_P
	AS
	set nocount on
select 	info.email_address
from 	user_info_group_info_map map(nolock),
	user_info info(nolock)
where 	map.group_info_id = (select group_info_id from group_info where group_name = 'sourcing.director')
	and info.user_info_id = map.user_info_id
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_SD_EMAIL_P] TO [CBMSApplication]
GO
