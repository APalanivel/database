SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.GET_RS_TIME_P
	@rate_id INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT rs.rate_id,
		rs.rate_schedule_id, rs.rs_start_date, rs.rs_end_date, 
		sea.season_id, sea.season_name, 
		sea.season_FROM_date, sea.season_to_date 
	FROM dbo.rate_schedule rs LEFT OUTER JOIN dbo.season sea ON (sea.season_parent_id=rs.rate_schedule_id)
	WHERE rs.rate_id = @rate_id
	ORDER BY rs.rate_schedule_id
		, rs_start_date
		, sea.season_FROM_date

END

GO
GRANT EXECUTE ON  [dbo].[GET_RS_TIME_P] TO [CBMSApplication]
GO
