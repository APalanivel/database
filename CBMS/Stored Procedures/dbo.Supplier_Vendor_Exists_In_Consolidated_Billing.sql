SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Supplier_Vendor_Exists_In_Consolidated_Billing       
              
Description:              
			This sproc is to return the flag if atleast one supplier vendor exists in Consolidated_Billing for the given account.      
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
  @Account_Id							INT
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

   Exec dbo.Supplier_Vendor_Exists_In_Consolidated_Billing 13958
   
  
   
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2016-12-29		Created For Invoice_Collection.         
             
******/
CREATE  PROCEDURE [dbo].[Supplier_Vendor_Exists_In_Consolidated_Billing] ( @Account_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 
      
      DECLARE @Is_Exist_Consolidated_Billing BIT = 0
      
      SELECT
            @Is_Exist_Consolidated_Billing = 1
      FROM
            dbo.Account_Consolidated_Billing_Vendor acbv
            INNER JOIN dbo.ENTITY e
                  ON e.ENTITY_ID = acbv.Invoice_Vendor_Type_Id
      WHERE
            Account_Id = @Account_Id
            AND e.ENTITY_NAME = 'Supplier'
     
     
     
     SELECT  @Is_Exist_Consolidated_Billing AS Is_Exist_Consolidated_Billing
      
END
;
GO
GRANT EXECUTE ON  [dbo].[Supplier_Vendor_Exists_In_Consolidated_Billing] TO [CBMSApplication]
GO
