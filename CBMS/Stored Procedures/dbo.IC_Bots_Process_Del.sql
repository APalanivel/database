SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          
            
 NAME: dbo.IC_Bots_Process_Del			
            
 DESCRIPTION:            
    To delete the Bots Process names    
            
 INPUT PARAMETERS:            
           
 Name                               DataType            Default      Description            
---------------------------------------------------------------------------------------------------------------          
@IC_Bots_Process_Id					INT
     
             
 OUTPUT PARAMETERS:           
                  
 Name                               DataType            Default      Description            
---------------------------------------------------------------------------------------------------------------          
            
 USAGE EXAMPLES:                
---------------------------------------------------------------------------------------------------------------          
BEGIN TRAN;

select  * from  dbo.IC_Bots_Process where   IC_Bots_Process_Id = 1;

EXEC IC_Bots_Process_Del
    @IC_Bots_Process_Id = 1

select  * from  dbo.IC_Bots_Process where   IC_Bots_Process_Id = 1;

ROLLBACK TRAN;

 AUTHOR INITIALS:          
            
 Initials               Name            
---------------------------------------------------------------------------------------------------------------          
 NR                     Narayana Reddy              
             
 MODIFICATIONS:           
           
 Initials               Date            Modification          
---------------------------------------------------------------------------------------------------------------          
 NR                     2020-05-27      Created for SE2017-963          
           
******/


CREATE PROC [dbo].[IC_Bots_Process_Del]
     (
         @IC_Bots_Process_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;


        DELETE
        ibp
        FROM
            dbo.IC_Bots_Process ibp
        WHERE
            ibp.IC_Bots_Process_Id = @IC_Bots_Process_Id;



    END;

GO
GRANT EXECUTE ON  [dbo].[IC_Bots_Process_Del] TO [CBMSApplication]
GO
