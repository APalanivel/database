SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.UPDATE_DIVISION_DETAILS_P


DESCRIPTION: Updates information for a divsion

USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		CH		Chad Hattabaugh

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  CH		06/22/2009	modified for the sitegroup division table split      	
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE       PROCEDURE dbo.UPDATE_DIVISION_DETAILS_P
(
		@sba_type_id int,
		@price_index_id int,
		@client_id int,
		@term_preferred_type_id int,
		@contract_reviewer_type_id int,
		@decision_maker_type_id int,
		@signature_type_id int,
		@division_name varchar(200),
		@is_interest_minority_suppliers bit,
		@is_corporate_hedge bit,
		@naics_code varchar(30),
		@tax_number varchar(30),
		@duns_number varchar(30),
		@not_managed bit,
		@misc_comments varchar(4000),
		@trigger_rights bit,
		@contracting_entity varchar(200),
		@client_legal_structure varchar(4000),
		@division_id int
)
AS

begin
	
	set nocount on
	DECLARE @Err INT

	BEGIN TRANSACTION 
		UPDATE dbo.SiteGroup 
		SET Sitegroup_Name = @division_name
		WHERE Sitegroup_Id = @division_id
		
		SET @Err = @@Error
		if @Err != 0  GOTO Err_handler
	
		update dbo.division_dtl 
		set 	SBA_TYPE_ID = @sba_type_id,
				PRICE_INDEX_ID = @price_index_id,
				CLIENT_ID = @client_id,
				TERM_PREFERRED_TYPE_ID = @term_preferred_type_id,
				CONTRACT_REVIEWER_TYPE_ID = @contract_reviewer_type_id,
				DECISION_MAKER_TYPE_ID = @decision_maker_type_id,
				SIGNATORY_TYPE_ID = @signature_type_id,
				IS_INTEREST_MINORITY_SUPPLIERS = @is_interest_minority_suppliers,
				IS_CORPORATE_HEDGE = @is_corporate_hedge,
				NAICS_CODE = @naics_code,
				TAX_NUMBER = @tax_number,
				DUNS_NUMBER = @duns_number,
				Not_Managed = @not_managed,
				MISC_COMMENTS = @misc_comments,
				TRIGGER_RIGHTS = @trigger_rights,
				CONTRACTING_ENTITY = @contracting_entity,
				CLIENT_LEGAL_STRUCTURE = @client_legal_structure
		where 	sitegroup_id = @division_id
	
		SET @Err = @@Error
		if @Err != 0  GOTO Err_handler

Err_Handler:

	IF @@TRANCOUNT != 0 
		IF @Err != 0 
			ROLLBACK TRANSACTION
		ELSE 
			COMMIT TRANSACTION
END
GO
GRANT EXECUTE ON  [dbo].[UPDATE_DIVISION_DETAILS_P] TO [CBMSApplication]
GO
