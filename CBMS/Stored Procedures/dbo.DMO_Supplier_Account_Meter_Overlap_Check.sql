SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.DMO_Supplier_Account_Meter_Overlap_Check
           
DESCRIPTION:             
			To check whether DMO suppleir account meters overlapping with new dates
			
INPUT PARAMETERS:            
	Name					DataType		Default		Description  
---------------------------------------------------------------------------------  
	@Supplier_Account_Id	INT
    @DMO_Supp_Start_Dt	DATE
    @DMO_Supp_End_Dt	DATE			NULL
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
	EXEC dbo.DMO_Supplier_Account_Meter_Sel 235,290,'05/01/2017','05/31/2017',193656
	EXEC dbo.DMO_Supplier_Account_Meter_Overlap_Check 1149300,'04/01/2017','04/30/2017'
	EXEC dbo.DMO_Supplier_Account_Meter_Overlap_Check 1149300,'05/01/2017','05/31/2017'
	EXEC dbo.DMO_Supplier_Account_Meter_Overlap_Check 1149300,'06/01/2017','06/30/2017'
	EXEC dbo.DMO_Supplier_Account_Meter_Overlap_Check 1149300,'08/01/2017','09/30/2017'
	
	EXEC DMO_Supplier_Account_Meter_Overlap_Check 1740987,'2014-01-01','2014-06-30'

	EXEC DMO_Supplier_Account_Meter_Overlap_Check 1656164,'2018-03-01',null
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-03-16	Contract placeholder - CP-8 Created
******/

CREATE PROCEDURE [dbo].[DMO_Supplier_Account_Meter_Overlap_Check]
    (
        @Supplier_Account_Id INT
        , @DMO_Supp_Start_Dt DATE
        , @DMO_Supp_End_Dt DATE = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Meter_Overlappig BIT = 0;

        DECLARE @Meters TABLE
              (
                  Meter_Id INT
              );

        SELECT
            @Meter_Overlappig = 1
        WHERE
            @DMO_Supp_End_Dt IS NOT NULL
            AND @DMO_Supp_Start_Dt > @DMO_Supp_End_Dt;

        INSERT INTO @Meters
             (
                 Meter_Id
             )
        SELECT
            samm.METER_ID
        FROM
            dbo.SUPPLIER_ACCOUNT_METER_MAP samm
        WHERE
            samm.ACCOUNT_ID = @Supplier_Account_Id
        GROUP BY
            samm.METER_ID;



        SELECT
            @Meter_Overlappig = 1
        FROM
            dbo.SUPPLIER_ACCOUNT_METER_MAP samm
            INNER JOIN @Meters mtrs
                ON samm.METER_ID = mtrs.Meter_Id
        WHERE
            samm.ACCOUNT_ID <> @Supplier_Account_Id
            AND (   (   @DMO_Supp_End_Dt IS NULL
                        AND samm.METER_DISASSOCIATION_DATE IS NULL
                        AND @DMO_Supp_Start_Dt < samm.METER_ASSOCIATION_DATE)
                    OR  (   @DMO_Supp_End_Dt IS NOT NULL
                            AND samm.METER_DISASSOCIATION_DATE IS NULL
                            AND samm.METER_ASSOCIATION_DATE > @DMO_Supp_Start_Dt
                            AND samm.METER_ASSOCIATION_DATE < @DMO_Supp_End_Dt)
                    OR  (   @DMO_Supp_End_Dt IS NULL
                            AND samm.METER_DISASSOCIATION_DATE IS NOT NULL
                            AND samm.METER_DISASSOCIATION_DATE > @DMO_Supp_Start_Dt)
                    OR  (   @DMO_Supp_End_Dt IS NOT NULL
                            AND samm.METER_DISASSOCIATION_DATE IS NOT NULL
                            AND (   (   (   @DMO_Supp_Start_Dt > samm.METER_ASSOCIATION_DATE
                                            AND @DMO_Supp_Start_Dt < samm.METER_DISASSOCIATION_DATE)
                                        OR  (   @DMO_Supp_End_Dt > samm.METER_ASSOCIATION_DATE
                                                AND @DMO_Supp_End_Dt < samm.METER_DISASSOCIATION_DATE))
                                    OR  (   (   samm.METER_ASSOCIATION_DATE > @DMO_Supp_Start_Dt
                                                AND samm.METER_ASSOCIATION_DATE < @DMO_Supp_End_Dt)
                                            OR  (   samm.METER_DISASSOCIATION_DATE > @DMO_Supp_Start_Dt
                                                    AND samm.METER_DISASSOCIATION_DATE < @DMO_Supp_End_Dt))
                                    OR  (   samm.METER_ASSOCIATION_DATE = @DMO_Supp_Start_Dt
                                            AND samm.METER_DISASSOCIATION_DATE = @DMO_Supp_End_Dt))));



        SELECT  @Meter_Overlappig AS Meter_Overlappig;


    END;

    ;


GO
GRANT EXECUTE ON  [dbo].[DMO_Supplier_Account_Meter_Overlap_Check] TO [CBMSApplication]
GO
