SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******    
NAME:    
[dbo].[Commodity_Upd_By_Commodity_Name]  

DESCRIPTION:  
 
Used to update COMMODITY table  

INPUT PARAMETERS:    
Name		     DataType	Default Description    
------------------------------------------------------------    
@Commodity_Name		VARCHAR(250),
@IS_ALTERNATE_FUEL  BIT		= 0,  
@IS_SUSTAINABLE		BIT		= 1,  
@IS_ACTIVE			BIT		= 1,
          
OUTPUT PARAMETERS:    
Name		     DataType	Default Description    
------------------------------------------------------------    
@IS_UPDATED			BIT = 0

USAGE EXAMPLES:    
------------------------------------------------------------  



AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
GB   Geetansu Behera    
CMH  Chad Hattabaugh     
HG   Hari   

MODIFICATIONS     
Initials Date		Modification    
------------------------------------------------------------    
SKA		31-SEP-09	Created new seperate _UPD sp against Commodity_Ins_Upd
SS		06-SEP-09	SP Renamed from Commodity_Upd to Commodity_Upd_By_Commodity_Name

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.Commodity_Upd_By_Commodity_Name
(	
	@Commodity_Name		VARCHAR(250),
	@IS_ALTERNATE_FUEL  BIT = 0,  
	@IS_SUSTAINABLE		BIT = 1,  
	@IS_ACTIVE			BIT = 1
 ) 
AS  

BEGIN  
 
      
   BEGIN TRY  
	BEGIN TRANSACTION                              
		UPDATE	dbo.COMMODITY 
			SET IS_SUSTAINABLE= @IS_SUSTAINABLE, 
				IS_ACTIVE= @IS_ACTIVE  ,
				IS_ALTERNATE_FUEL = @IS_ALTERNATE_FUEL
			WHERE 
				COMMODITY_NAME = @Commodity_Name  
	
		SELECT @@ROWCOUNT AS 'Updated_Rows'
	
	COMMIT TRANSACTION  
 END TRY   
 BEGIN CATCH  
  ROLLBACK TRANSACTION  
	EXEC dbo.usp_RethrowError                  
 END CATCH  
      
END
GO
GRANT EXECUTE ON  [dbo].[Commodity_Upd_By_Commodity_Name] TO [CBMSApplication]
GO
