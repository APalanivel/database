SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.User_Dtls_Sel_By_Permission_Name            
                        
 DESCRIPTION:                        
			This will get the users based on groups assiged to them.
			                  
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
--------------------------------------------------------------------------------
@MyAccountId					INT
@Permission_Info_names			VARCHAR(200)	 NULL
@User_Name_Str					VARCHAR(MAX)	 NULL
                            
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
--------------------------------------------------------------------------------
                        
 USAGE EXAMPLES:                            
--------------------------------------------------------------------------------
 
Exec User_Dtls_Sel_By_Permission_Name_User_Search
    @Permission_Info_names = 'standingdata.exceptionqueue'

Exec User_Dtls_Sel_By_Permission_Name_User_Search
    @Permission_Info_names = 'standingdata.exceptionqueue'
    , @User_Name_Str = 'Medina'
  
                
 AUTHOR INITIALS:        
       
 Initials              Name        
--------------------------------------------------------------------------------
 NR                    Narayana Reddy         
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------
NR						2019-06-11		Add Contract - Created.										
										
										       
                  
******/
CREATE PROCEDURE [dbo].[User_Dtls_Sel_By_Permission_Name_User_Search]
    (
        @Permission_Info_names VARCHAR(MAX)
        , @User_Name_Str VARCHAR(200) = NULL
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
    )
AS
    BEGIN
        SET NOCOUNT ON;
        WITH Cte_Users
        AS (
               SELECT
                    ui.USER_INFO_ID
                    , ui.USERNAME
                    , ui.QUEUE_ID
                    , ui.FIRST_NAME
                    , ui.MIDDLE_NAME
                    , ui.LAST_NAME
                    , ui.EMAIL_ADDRESS
                    , ui.IS_HISTORY
                    , ui.FIRST_NAME + ' ' + ui.LAST_NAME full_name
                    , ui.ACCESS_LEVEL
                    , ROW_NUMBER() OVER (ORDER BY
                                             ui.FIRST_NAME + ' ' + ui.LAST_NAME) Row_Num
                    , COUNT(1) OVER () AS Total_Row_Count
               FROM
                    dbo.GROUP_INFO gi
                    INNER JOIN dbo.GROUP_INFO_PERMISSION_INFO_MAP gipim
                        ON gipim.GROUP_INFO_ID = gi.GROUP_INFO_ID
                    INNER JOIN dbo.PERMISSION_INFO pi
                        ON pi.PERMISSION_INFO_ID = gipim.PERMISSION_INFO_ID
                    INNER JOIN dbo.USER_INFO_GROUP_INFO_MAP map
                        ON map.GROUP_INFO_ID = gipim.GROUP_INFO_ID
                    INNER JOIN dbo.USER_INFO ui
                        ON ui.USER_INFO_ID = map.USER_INFO_ID
               WHERE
                    ui.IS_HISTORY != 1
                    AND (   @User_Name_Str IS NULL
                            OR  ui.FIRST_NAME LIKE '%' + @User_Name_Str + '%'
                            OR  ui.LAST_NAME LIKE '%' + @User_Name_Str + '%')
                    AND EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Permission_Info_names, ',') uf
                                   WHERE
                                        uf.Segments = pi.PERMISSION_NAME)
               GROUP BY
                   ui.USER_INFO_ID
                   , ui.USERNAME
                   , ui.QUEUE_ID
                   , ui.FIRST_NAME
                   , ui.MIDDLE_NAME
                   , ui.LAST_NAME
                   , ui.EMAIL_ADDRESS
                   , ui.IS_HISTORY
                   , ui.FIRST_NAME + ' ' + ui.LAST_NAME
                   , ui.ACCESS_LEVEL
           )
        SELECT
            cu.USER_INFO_ID
            , cu.USERNAME
            , cu.QUEUE_ID
            , cu.FIRST_NAME
            , cu.MIDDLE_NAME
            , cu.LAST_NAME
            , cu.EMAIL_ADDRESS
            , cu.IS_HISTORY
            , cu.full_name
            , cu.ACCESS_LEVEL
            , cu.Row_Num
            , cu.Total_Row_Count
        FROM
            Cte_Users cu
        WHERE
            cu.Row_Num BETWEEN @Start_Index
                       AND     @End_Index
        ORDER BY
            cu.Row_Num;

    END;
GO
GRANT EXECUTE ON  [dbo].[User_Dtls_Sel_By_Permission_Name_User_Search] TO [CBMSApplication]
GO
