SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.etl_dv2_Client_Account_Sum   
(      
    @MinDBTS BIGINT          
    ,@MaxDBTS BIGINT  
)           
AS           
BEGIN           
    SET NOCOUNT ON   

    DECLARE @stage TABLE   
    (  
        Client_Id                    int             NOT NULL  
        ,Division_Id                 int             NOT NULL  
        ,Site_Id                     int             NOT NULL  
        ,Account_Id                  int             NOT NULL  
        ,Client_Name                 varchar(200)    NULL  
        ,Client_Currency_Group_Id    int             NULL  
        ,Division_Name               varchar(200)    NULL  
        ,Site_Name                   varchar(200)    NULL  
        ,Site_Not_Managed            bit             NULL  
        ,Account_Number              varchar(50)     NULL  
        ,Account_Not_Managed         bit             NULL 
        ,Account_Type_Name			 varchar(50)	 NULL 
        ,Account_Type_Id			 INT			 NULL
        ,Vendor_Id                   int             NULL  
        ,Vendor_Name                 varchar(200)    NULL  
        ,ADDRESS_LINE1               varchar(200)    NULL  
        ,City                        varchar(200)    NULL  
        ,State_Name                  varchar(50)     NULL  
        ,ZIPCODE                     varchar(30)     NULL  
        ,COUNTRY_NAME                varchar(50)     NULL  
    )  
    
    -- Get Util Records for Account   
    INSERT @Stage   
    (   CLIENT_ID   
        ,CLIENT_NAME  
        ,Client_Currency_Group_Id        
        ,DIVISION_ID  
        ,DIVISION_NAME  
        ,SITE_ID  
        ,SITE_NAME  
        ,Site_Not_Managed          
        ,ACCOUNT_ID   
        ,ACCOUNT_NUMBER   
        ,Account_Not_Managed 
        ,Account_Type_Name
        ,Account_Type_Id         
        ,VENDOR_ID    
        ,VENDOR_NAME   
        ,ADDRESS_LINE1  
        ,City  
        ,State_Name  
        ,ZIPCODE  
        ,COUNTRY_NAME  
    )   
    SELECT          
        c.CLIENT_ID   
        ,c.CLIENT_NAME  
        ,c.CURRENCY_GROUP_ID        
        ,d.DIVISION_ID  
        ,d.DIVISION_NAME  
        ,s.SITE_ID  
        ,s.SITE_NAME  
        ,s.NOT_MANAGED as Site_Not_Managed          
        ,a.ACCOUNT_ID   
        ,a.ACCOUNT_NUMBER
        ,a.NOT_MANAGED     as Account_Not_Managed  
        ,left(acct.ENTITY_NAME, 50)
        ,a.ACCOUNT_TYPE_ID        
        ,V.VENDOR_ID    
        ,VENDOR_NAME   
        ,isnull(addr.ADDRESS_LINE1, '')  
        ,isnull(addr.CITY, '')  
        ,isnull(st.State_Name, '')  
        ,isnull(addr.ZIPCODE, '')   
        ,isnull(cnty.COUNTRY_NAME, '')    
    FROM CLIENT c  (NOLOCK)          
        INNER JOIN DIVISION d (NOLOCK) 
			ON c.CLIENT_ID = d.CLIENT_ID          
        INNER JOIN SITE s  (NOLOCK)
			ON d.DIVISION_ID = s.DIVISION_ID          
        INNER JOIN ACCOUNT a  (NOLOCK)
			ON a.SITE_ID = s.SITE_ID          
        LEFT JOIN VENDOR v  (NOLOCK)
			ON a.VENDOR_ID = v.VENDOR_ID          
        LEFT JOIN ADDRESS addr  (NOLOCK)
			ON s.PRIMARY_ADDRESS_ID = addr.ADDRESS_ID          
        LEFT JOIN STATE st  (NOLOCK)
			ON addr.STATE_ID = st.STATE_ID          
        LEFT JOIN COUNTRY cnty  (NOLOCK)
			ON st.COUNTRY_ID = cnty.COUNTRY_ID  
        INNER JOIN ENTITY acct  (NOLOCK)
			ON acct.ENTITY_ID = a.ACCOUNT_TYPE_ID         
    WHERE       
        c.ROW_VERSION BETWEEN @MinDBTS and @MaxDBTS          
        OR d.ROW_VERSION BETWEEN @MinDBTS and @MaxDBTS          
        OR s.ROW_VERSION BETWEEN @MinDBTS AND @MaxDBTS          
        OR a.ROW_VERSION BETWEEN @MinDBTS AND @MaxDBTS          
        OR addr.ROW_VERSION BETWEEN @MinDBTS and @MaxDBTS  
        
        
     -- GET Supplier Records
	INSERT @stage
    (   CLIENT_ID   
        ,CLIENT_NAME  
        ,Client_Currency_Group_Id        
        ,DIVISION_ID  
        ,DIVISION_NAME  
        ,SITE_ID  
        ,SITE_NAME  
        ,Site_Not_Managed          
        ,ACCOUNT_ID   
        ,ACCOUNT_NUMBER   
        ,Account_Not_Managed
        ,Account_Type_Id 
        ,Account_Type_Name          
        ,VENDOR_ID    
        ,VENDOR_NAME   
        ,ADDRESS_LINE1  
        ,City  
        ,State_Name  
        ,ZIPCODE  
        ,COUNTRY_NAME  
    )    
	SELECT       
		c.CLIENT_ID   
        ,c.CLIENT_NAME  
        ,c.CURRENCY_GROUP_ID        
        ,d.DIVISION_ID  
        ,d.DIVISION_NAME  
        ,s.SITE_ID  
        ,s.SITE_NAME  
        ,s.NOT_MANAGED as Site_Not_Managed          
        ,sup.ACCOUNT_ID   
        ,sup.ACCOUNT_NUMBER   
        ,sup.NOT_MANAGED     as Account_Not_Managed
        ,Sup.ACCOUNT_TYPE_ID
        ,acct.Entity_Name          
        ,V.VENDOR_ID    
        ,VENDOR_NAME   
        ,isnull(addr.ADDRESS_LINE1, '')  
        ,isnull(addr.CITY, '')  
        ,isnull(st.State_Name, '')  
        ,isnull(addr.ZIPCODE, '')   
        ,isnull(cnty.COUNTRY_NAME, '')
    FROM CLIENT c (NOLOCK)           
        INNER JOIN DIVISION d (NOLOCK) 
			ON c.CLIENT_ID = d.CLIENT_ID          
        INNER JOIN SITE s  (NOLOCK)
			ON d.DIVISION_ID = s.DIVISION_ID          
        INNER JOIN ACCOUNT util  (NOLOCK)
			ON util.SITE_ID = s.SITE_ID 
		INNER JOIN METER m  (NOLOCK)
			on util.ACCOUNT_ID = m.Account_id
		INNER JOIN SUPPLIER_ACCOUNT_METER_MAP sam  (NOLOCK)
			ON sam.METER_ID = m.METER_ID
		INNER JOIN ACCOUNT Sup  (NOLOCK)
			on sam.ACCOUNT_ID = Sup.ACCOUNT_ID
        LEFT JOIN VENDOR v  (NOLOCK)
			ON sup.VENDOR_ID = v.VENDOR_ID          
        LEFT JOIN ADDRESS addr  (NOLOCK)
			ON s.PRIMARY_ADDRESS_ID = addr.ADDRESS_ID          
        LEFT JOIN STATE st  (NOLOCK)
			ON addr.STATE_ID = st.STATE_ID          
        LEFT JOIN COUNTRY cnty  (NOLOCK)
			ON st.COUNTRY_ID = cnty.COUNTRY_ID 
        INNER JOIN Entity acct  (NOLOCK)
			ON acct.ENTITY_ID = Sup.ACCOUNT_TYPE_ID 
   WHERE ( c.ROW_VERSION BETWEEN @MinDBTS and @MaxDBTS          
			OR d.ROW_VERSION BETWEEN @MinDBTS and @MaxDBTS          
			OR s.ROW_VERSION BETWEEN @MinDBTS AND @MaxDBTS          
			OR sup.ROW_VERSION BETWEEN @MinDBTS AND @MaxDBTS )
		AND NOT EXISTS (	SELECT 1 
						FROM @Stage 
						WHERE 
								Client_Id = c.CLIENT_ID 
								AND Division_Id = d.DIVISION_ID
								AND SITE_ID = s.SITE_ID 
								AND Account_Id = Sup.ACCOUNT_ID )
   Group BY 
		c.CLIENT_ID   
        ,c.CLIENT_NAME  
        ,c.CURRENCY_GROUP_ID        
        ,d.DIVISION_ID  
        ,d.DIVISION_NAME  
        ,s.SITE_ID  
        ,s.SITE_NAME  
        ,s.NOT_MANAGED         
        ,sup.ACCOUNT_ID   
        ,sup.ACCOUNT_NUMBER   
        ,sup.NOT_MANAGED
        ,Sup.ACCOUNT_TYPE_ID
        ,acct.Entity_Name          
        ,V.VENDOR_ID    
        ,VENDOR_NAME   
        ,isnull(addr.ADDRESS_LINE1, '')  
        ,isnull(addr.CITY, '')  
        ,isnull(st.State_Name, '')  
        ,isnull(addr.ZIPCODE, '')   
        ,isnull(cnty.COUNTRY_NAME, '')       



    -- Get Site level records  

    INSERT @Stage   
    (    CLIENT_ID   
        ,CLIENT_NAME  
        ,Client_Currency_Group_Id        
        ,DIVISION_ID  
        ,DIVISION_NAME  
        ,SITE_ID  
        ,SITE_NAME  
        ,Site_Not_Managed          
        ,ACCOUNT_ID   
        ,ACCOUNT_NUMBER   
        ,Account_Not_Managed          
        ,VENDOR_ID    
        ,VENDOR_NAME   
        ,ADDRESS_LINE1  
        ,City  
        ,State_Name  
        ,ZIPCODE  
        ,COUNTRY_NAME  
    )   
    SELECT          
        c.CLIENT_ID   
        ,c.CLIENT_NAME  
        ,c.CURRENCY_GROUP_ID        
        ,d.DIVISION_ID  
        ,d.DIVISION_NAME  
        ,s.SITE_ID  
        ,s.SITE_NAME  
        ,s.NOT_MANAGED as Site_Not_Managed          
        ,0   
        ,''  
        ,0   
        ,0  
        ,''  
        ,isnull(addr.ADDRESS_LINE1, '')  
        ,isnull(addr.CITY, '')  
        ,isnull(st.State_Name, '')  
        ,isnull(addr.ZIPCODE, '')   
        ,isnull(cnty.COUNTRY_NAME, '')    
        FROM CLIENT c  (NOLOCK)          
        INNER JOIN DIVISION d  (NOLOCK)
			ON c.CLIENT_ID = d.CLIENT_ID          
        INNER JOIN SITE s  (NOLOCK)
			ON d.DIVISION_ID = s.DIVISION_ID                
        LEFT JOIN ADDRESS addr  (NOLOCK)
			ON s.PRIMARY_ADDRESS_ID = addr.ADDRESS_ID          
        LEFT JOIN STATE st  (NOLOCK)
			ON addr.STATE_ID = st.STATE_ID          
        LEFT JOIN COUNTRY cnty  (NOLOCK)
			ON st.COUNTRY_ID = cnty.COUNTRY_ID          
    WHERE       
        c.ROW_VERSION BETWEEN @MinDBTS and @MaxDBTS          
        OR d.ROW_VERSION BETWEEN @MinDBTS and @MaxDBTS          
        OR s.ROW_VERSION BETWEEN @MinDBTS AND @MaxDBTS                
        OR addr.ROW_VERSION BETWEEN @MinDBTS and @MaxDBTS         

    -- Get Divsion level records  
    INSERT @Stage   
    (    CLIENT_ID   
        ,CLIENT_NAME  
        ,Client_Currency_Group_Id        
        ,DIVISION_ID  
        ,DIVISION_NAME  
        ,SITE_ID  
        ,SITE_NAME  
        ,Site_Not_Managed          
        ,ACCOUNT_ID   
        ,ACCOUNT_NUMBER   
        ,Account_Not_Managed          
        ,VENDOR_ID    
        ,VENDOR_NAME   
        ,ADDRESS_LINE1  
        ,City  
        ,State_Name  
        ,ZIPCODE  
        ,COUNTRY_NAME  
    )   
    SELECT          
        c.CLIENT_ID   
        ,c.CLIENT_NAME  
        ,c.CURRENCY_GROUP_ID        
        ,d.DIVISION_ID  
        ,d.DIVISION_NAME  
        ,0  
        ,''  
        ,0    
        ,0   
        ,''  
        ,0   
        ,0  
        ,''  
        ,''   
        ,''  
        ,''   
        ,''   
        ,''    
    FROM CLIENT c   (NOLOCK)     
        INNER JOIN DIVISION d  (NOLOCK)
			ON c.CLIENT_ID = d.CLIENT_ID               
    WHERE       
        c.ROW_VERSION BETWEEN @MinDBTS and @MaxDBTS          
        OR d.ROW_VERSION BETWEEN @MinDBTS and @MaxDBTS     


    -- Get Client level records  
    INSERT @Stage   
    (   CLIENT_ID   
        ,CLIENT_NAME  
        ,Client_Currency_Group_Id        
        ,DIVISION_ID  
        ,DIVISION_NAME  
        ,SITE_ID  
        ,SITE_NAME  
        ,Site_Not_Managed          
        ,ACCOUNT_ID   
        ,ACCOUNT_NUMBER   
        ,Account_Not_Managed          
        ,VENDOR_ID    
        ,VENDOR_NAME   
        ,ADDRESS_LINE1  
        ,City  
        ,State_Name  
        ,ZIPCODE  
        ,COUNTRY_NAME  
    )   
    SELECT          
        c.CLIENT_ID   
        ,c.CLIENT_NAME  
        ,c.CURRENCY_GROUP_ID        
        ,0  
        ,''  
        ,0  
        ,''  
        ,0    
        ,0   
        ,''  
        ,0   
        ,0  
        ,''  
        ,''   
        ,''  
        ,''   
        ,''   
        ,''    
    FROM CLIENT c   (NOLOCK)                   
    WHERE       
        c.ROW_VERSION BETWEEN @MinDBTS and @MaxDBTS 
 
    -- Return All Records   
    SELECT  
        CLIENT_ID   
        ,CLIENT_NAME  
        ,Client_Currency_Group_Id        
        ,DIVISION_ID  
        ,DIVISION_NAME  
        ,SITE_ID  
        ,SITE_NAME  
        ,Site_Not_Managed          
        ,ACCOUNT_ID   
        ,ACCOUNT_NUMBER 
        ,Account_Not_Managed          
        ,Account_Type_Id 
        ,Account_Type_Name
        ,VENDOR_ID    
        ,VENDOR_NAME   
        ,ADDRESS_LINE1  
        ,City  
        ,State_Name  
        ,ZIPCODE  
        ,COUNTRY_NAME   
    FROM @stage  
    ORDER BY   
        Client_Id  
        ,Division_Id  
        ,Site_Id  
        ,Account_Id  
 END
GO
GRANT EXECUTE ON  [dbo].[etl_dv2_Client_Account_Sum] TO [CBMSApplication]
GO
