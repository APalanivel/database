SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Service_Condition_Template_Question_Sel_By_Template_Language_CD]
           
DESCRIPTION:             
			To get service condition category list mapped to template
			
INPUT PARAMETERS:            
	Name								DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Sr_Service_Condition_Template_Id	INT
    @Language_CD						INT			NULL
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
	EXEC dbo.Sr_Service_Condition_Template_Question_Sel_By_Template_Language_CD 3 
	EXEC dbo.Sr_Service_Condition_Template_Question_Sel_By_Template_Language_CD 3,100157
	EXEC dbo.Sr_Service_Condition_Template_Question_Sel_By_Template_Language_CD 3,100158
			
		
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-02-29	Global Sourcing - Phase3 - GCS-448 Created
******/
CREATE PROCEDURE [dbo].[Sr_Service_Condition_Template_Question_Sel_By_Template_Language_CD]
      ( 
       @Sr_Service_Condition_Template_Id INT
      ,@Language_CD INT = NULL )
AS 
BEGIN

      SET NOCOUNT ON;
      
      SELECT
            @Language_CD = isnull(@Language_CD, cd.Code_Id)
      FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cd.Code_Value = 'en-US'
            AND cs.Codeset_Name = 'LocalizationLanguage'
      
      SELECT
            ssctcm.Sr_Service_Condition_Template_Id
           ,sscc.Sr_Service_Condition_Category_Id
           ,isnull(sscclv.Category_Name_Locale_Value, sscc.Category_Name) AS Category_Name_Locale_Value
           ,ssctcm.Display_Seq AS Category_Display_Seq
           ,ssctqm.Sr_Service_Condition_Question_Id
           ,isnull(sscqlv.Question_Label_Locale_Value, sscq.Question_Label) AS Question_Label_Locale_Value
           ,ssctqm.Display_Seq AS Question_Display_Seq
      FROM
            dbo.Sr_Service_Condition_Template_Category_Map ssctcm
            INNER JOIN dbo.Sr_Service_Condition_Category sscc
                  ON ssctcm.Sr_Service_Condition_Category_Id = sscc.Sr_Service_Condition_Category_Id
            LEFT JOIN dbo.Sr_Service_Condition_Category_Locale_Value sscclv
                  ON sscc.Sr_Service_Condition_Category_Id = sscclv.Sr_Service_Condition_Category_Id
                     AND sscclv.Language_Cd = @Language_CD
            INNER JOIN dbo.Sr_Service_Condition_Template_Question_Map ssctqm
                  ON ssctqm.Sr_Service_Condition_Template_Category_Map_Id = ssctcm.Sr_Service_Condition_Template_Category_Map_Id
            INNER JOIN dbo.Sr_Service_Condition_Question sscq
                  ON ssctqm.Sr_Service_Condition_Question_Id = sscq.Sr_Service_Condition_Question_Id
            LEFT JOIN dbo.Sr_Service_Condition_Question_Locale_Value sscqlv
                  ON sscq.Sr_Service_Condition_Question_Id = sscqlv.Sr_Service_Condition_Question_Id
                     AND sscqlv.Language_Cd = @Language_CD
      WHERE
            ssctcm.Sr_Service_Condition_Template_Id = @Sr_Service_Condition_Template_Id
            
                  
            
END;
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Service_Condition_Template_Question_Sel_By_Template_Language_CD] TO [CBMSApplication]
GO
