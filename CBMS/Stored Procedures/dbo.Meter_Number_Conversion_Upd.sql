SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/*********  
  
NAME:                           
     dbo.Meter_Number_Conversion_Upd  
  
DESCRIPTION:                                          
  
 INPUT PARAMETERS:                                          
  
 Name                        DataType         Default       Description                                        
---------------------------------------------------------------------------------------------------------------                                      
                             
  
 OUTPUT PARAMETERS:                                          
  
 Name                        DataType         Default       Description                                        
---------------------------------------------------------------------------------------------------------------                                      
  
 USAGE EXAMPLES:                                                            
---------------------------------------------------------------------------------------------------------------                                                            
  
 BEGIN TRAN                                
 EXEC [dbo].Meter_Number_Conversion_Upd        
 ROLLBACK TRAN                           
                                    
  
  
 AUTHOR INITIALS:                                        
  
 Initials   Name                                        
---------------------------------------------------------------------------------------------------------------                                                      
 ABK   Aditya Bharadwaj K                                                              
  
 MODIFICATIONS:                                      
  
 Initials Date        Modification                                      
---------------------------------------------------------------------------------------------------------------                                      
 AB   2018-08-21 Created For - DDCR-35 - Added New if clause for meter number update package.  
*********/  
  
  
CREATE PROCEDURE [dbo].[Meter_Number_Conversion_Upd]  
AS  
    BEGIN  
        UPDATE  
            m  
        SET  
            m.METER_NUMBER = s.METER  
        FROM  
            #Update_Meter_Numbers_Stage s  
            JOIN dbo.METER m  
                ON m.METER_ID = s.METER_ID  
        WHERE  
            s.Is_Validation_Passed = 1;  
    END;  
GO
GRANT EXECUTE ON  [dbo].[Meter_Number_Conversion_Upd] TO [CBMSApplication]
GO
