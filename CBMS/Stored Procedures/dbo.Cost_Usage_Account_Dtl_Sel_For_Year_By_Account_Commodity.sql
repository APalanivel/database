
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 CBMS.dbo.Cost_Usage_Account_Dtl_Sel_For_Year_By_Account_Commodity  
 DESCRIPTION:   
 Used to Select the data from Cost_Usage_Account_Dtl table for the given account, commodity and report year   INPUT PARAMETERS:  
 Name				DataType  Default Description  
------------------------------------------------------------  
 @Client_id			INT  
 @Account_Id		INT  
 @Commodity_id		INT  
 @Report_year		INT  
 @Uom_Id			INT  
 @Currency_Unit_Id  INT  
 @Client_Hier_Id	INT
 @Country_Id		INT		   NULL
 
 OUTPUT PARAMETERS:  
 Name				DataType  Default Description  
------------------------------------------------------------  

 USAGE EXAMPLES:  
------------------------------------------------------------   

EXEC dbo.Cost_Usage_Account_Dtl_Sel_For_Year_By_Account_Commodity  108,438818,291,2012,25,3, 1154
EXEC dbo.Cost_Usage_Account_Dtl_Sel_For_Year_By_Account_Commodity  108,438818,291,2012,25,3, 1154,4
EXEC dbo.Cost_Usage_Account_Dtl_Sel_For_Year_By_Account_Commodity  108,439139,290,2012,12,1131
EXEC dbo.Cost_Usage_Account_Dtl_Sel_For_Year_By_Account_Commodity 10036,455714,67,2009,1409,3,10718,null


AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 HG			Hari  
 BCH		Balaraju  
 AP			Athmaram Pabbathi
    
 MODIFICATIONS   
 Initials	Date	Modification  
------------------------------------------------------------  
 HG		3/24/2010   Created  
 HG		04/01/2010  Script modified to fix the issue with the fiscal year calculation.  
					   If fiscal start month is January then fiscal start date is 01-01-<Report Year> and End Date is 12-31-<Report Year>  
					   otherwise previous will be considered for the fiscal start and end date calculation.  

					    Changed the condition in Cte_fy   
							  dd.Year_Num = @Report_Year to  
									(CASE  
										  WHEN dd.Month_Num = 1 THEN @report_year  
										  ELSE @Report_Year - 1  
									   END  
										  )  
 HG		05/07/2010  logic modified to get Is_published flag  
						As per the EP/NG procedures if record is present in Invoice_participation then Is_Published returned as true.  
 BCH	2011-11-10  Added Data_Sourct_Code column to select list and Added code table with left outer join  
 AP		2011-12-22  Removed & modified logic for CTE_fy and replaced with @Service_Month
 AP		2012-03-15  Following are the changes made as a part of Additional data enhancement
				    -- Fiscal year service month calculation simplified using Client_Fiscal_offset column in Client_Hier table & replaced @Service_Month table varible and used META.Date_Dim.
				    -- Removed the Marketer_Cost, Utility_Cost , Account_Type_id and Converted_Uom_Name columns as it is not used by application
				    -- Hardcoded Bucket name used to find the unit cost logic replaced by Calling a new procedure Cost_usage_Bucket_Sel_By_Commodity to get the respective bucket id for total cost/ voume and total cost
 RR		2012-07-20	MAINT-1383, Missing client hier id join on Cost_Usage_Account_Dtl multiplying the value for the supplier account spans across multiple sites.
					value is already filled by month and bucket wise
					Added new input parameter @Client_Hier_Id which can be NULL 
					Site_Id filter applied on Invoice_Participation
					Usage example sections updated
BCH		2012-08-22	(MAINT 1496 ). Added @Country_Id parameter as optional.
RR		2014-04-15	MAINT-1820 Data row repeating for each account VS service month if cost and usage received from different manual(CBSM/DE) sources,
					modified to return CBMS only if the account have data from both CBSM & DE data source codes

******/
CREATE PROCEDURE dbo.Cost_Usage_Account_Dtl_Sel_For_Year_By_Account_Commodity
      ( 
       @Client_Id INT
      ,@Account_Id INT
      ,@Commodity_Id INT
      ,@Report_Year INT
      ,@Uom_Type_Id INT
      ,@Currency_Unit_Id INT
      ,@Client_Hier_Id INT = NULL
      ,@Country_Id INT = NULL )
AS 
BEGIN  
      SET NOCOUNT ON  
  
      DECLARE
            @Currency_Group_Id INT
           ,@Start_Dt DATE
           ,@End_Dt DATE


      DECLARE @Cost_Usage_Bucket_Id TABLE
            ( 
             Bucket_Master_Id INT PRIMARY KEY CLUSTERED
            ,Bucket_Name VARCHAR(200)
            ,Bucket_Type VARCHAR(25) )


      SELECT
            @Currency_Group_Id = ch.Client_Currency_Group_Id
           ,@Start_Dt = dateadd(m, ch.Client_Fiscal_Offset, cast('1/1/' + cast(@Report_Year AS VARCHAR(4)) AS DATE))
           ,@End_Dt = dateadd(MONTH, -1, ( dateadd(YEAR, 1, dateadd(m, ch.Client_Fiscal_Offset, cast('1/1/' + cast(@Report_Year AS VARCHAR(4)) AS DATE))) ))
      FROM
            Core.Client_Hier ch
      WHERE
            ch.Client_ID = @Client_Id
            AND ch.Sitegroup_Id = 0
            AND ch.Site_Id = 0

      INSERT      INTO @Cost_Usage_Bucket_Id
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Bucket_Type )
                  EXEC dbo.Cost_usage_Bucket_Sel_By_Commodity 
                        @Commodity_id = @Commodity_Id;
   

      WITH  Cte_Cost_Usage_Account_Dtl
              AS ( SELECT
                        cha.Account_Id
                       ,cha.Display_Account_Number AS Account_Number
                       ,cha.Account_Type
                       ,cha.Account_Vendor_Id AS Vendor_Id
                       ,cha.Account_Vendor_Name AS Vendor_Name
                       ,cua.Service_Month
                       ,bkt.Bucket_Type
                       ,case WHEN dts_code.code_value = 'CBMS'
                                  OR dts_code.code_value = 'DE' THEN 'CBMS'
                             ELSE dts_code.code_value
                        END AS Data_Source_code
                       ,cha.Site_Id
                       ,sum(case WHEN bkt.Bucket_Type = 'Determinant' THEN cua.Bucket_Value * uc.Conversion_factor
                            END) Volume
                       ,sum(case WHEN bkt.Bucket_Type = 'Charge' THEN cua.Bucket_Value * cc.CONVERSION_FACTOR
                            END) Total_cost
                   FROM
                        ( SELECT
                              cha.Client_Hier_Id
                             ,cha.Account_Id
                             ,cha.Account_Type
                             ,cha.Display_Account_Number
                             ,cha.Account_Vendor_Id
                             ,cha.Account_Vendor_Name
                             ,ch.Site_Id
                          FROM
                              core.Client_Hier_Account cha
                              JOIN core.Client_Hier ch
                                    ON ch.Client_Hier_Id = cha.Client_Hier_id
                          WHERE
                              cha.Account_id = @Account_Id
                              AND cha.Commodity_Id = @Commodity_Id
                              AND ( @Client_Hier_Id IS NULL
                                    OR cha.Client_Hier_Id = @Client_Hier_Id )
                              AND ( @Country_Id IS NULL
                                    OR ch.Country_Id = @Country_Id )
                          GROUP BY
                              cha.Client_Hier_Id
                             ,cha.Account_Id
                             ,cha.Account_Type
                             ,cha.Display_Account_Number
                             ,cha.Account_Vendor_Id
                             ,cha.Account_Vendor_Name
                             ,ch.Site_Id ) cha
                        JOIN dbo.Cost_Usage_Account_Dtl cua
                              ON cua.ACCOUNT_ID = cha.Account_Id
                                 AND cha.Client_Hier_Id = cua.Client_Hier_Id
                        JOIN @Cost_Usage_Bucket_Id bkt
                              ON bkt.Bucket_Master_Id = cua.Bucket_Master_Id
                        LEFT OUTER JOIN dbo.Code dts_code
                              ON dts_code.code_id = cua.data_source_cd
                        LEFT OUTER JOIN dbo.consumption_unit_conversion uc
                              ON uc.BASE_UNIT_ID = cua.UOM_Type_Id
                                 AND uc.CONVERTED_UNIT_ID = @Uom_Type_Id
                        LEFT OUTER JOIN dbo.CURRENCY_UNIT_CONVERSION cc
                              ON cc.currency_group_id = @Currency_Group_Id
                                 AND cc.base_unit_id = cua.CURRENCY_UNIT_ID
                                 AND cc.converted_unit_id = @Currency_unit_Id
                                 AND cc.conversion_date = cua.Service_month
                   WHERE
                        cha.Account_Id = @Account_Id
                        AND cua.ACCOUNT_ID = @Account_ID
                        AND cua.Service_Month BETWEEN @Start_Dt AND @End_Dt
                   GROUP BY
                        cha.Account_Id
                       ,cha.Display_Account_Number
                       ,cha.Account_Type
                       ,cha.Account_Vendor_Id
                       ,cha.Account_Vendor_Name
                       ,cua.Service_Month
                       ,bkt.Bucket_Type
                       ,dts_code.Code_Value
                       ,cha.Site_Id)
            SELECT
                  cua.Account_Id
                 ,cua.Account_Number
                 ,cua.Account_Type
                 ,cua.Vendor_ID
                 ,cua.Vendor_Name
                 ,fy.Date_D AS Service_Month
                 ,cua.Data_Source_Code
                 ,isnull(sum(cua.Volume), 0) AS Volume
                 ,isnull(sum(cua.Total_cost), 0) AS Total_Cost
                 ,isnull(sum(cua.Total_cost) / nullif(sum(cua.Volume), 0), 0) Unit_Cost
                 ,ip.Cbms_Image_Id
                 ,isnull(max(case WHEN ip.Is_Expected = 1
                                       AND ip.Is_Received = 1 THEN 1
                                  ELSE 0
                             END), 0) AS Is_Complete
                 ,max(case WHEN ip.Invoice_Participation_Id IS NOT NULL THEN 1
                           ELSE 0
                      END) AS Is_Published
                 ,isnull(max(case WHEN ip.Recalc_Under_Review = 1
                                       OR ip.Variance_Under_Review = 1 THEN 1
                                  ELSE 0
                             END), 0) AS Is_Under_Review
            FROM
                  meta.Date_Dim fy
                  LEFT OUTER JOIN Cte_Cost_Usage_Account_Dtl cua
                        ON cua.Service_Month = fy.Date_D
                  LEFT OUTER JOIN dbo.Invoice_Participation ip
                        ON ip.ACCOUNT_ID = cua.ACCOUNT_ID
                           AND ip.Site_Id = cua.Site_id
                           AND ip.SERVICE_MONTH = cua.Service_Month
            WHERE
                  fy.Date_D BETWEEN @Start_Dt AND @End_Dt
            GROUP BY
                  cua.Account_Id
                 ,cua.Account_Number
                 ,cua.Account_Type
                 ,cua.Vendor_ID
                 ,cua.Vendor_Name
                 ,fy.Date_D
                 ,cua.Data_Source_Code
                 ,ip.cbms_image_id
            ORDER BY
                  fy.Date_D  

END;

;
GO






GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Dtl_Sel_For_Year_By_Account_Commodity] TO [CBMSApplication]
GO
