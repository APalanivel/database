SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[SR_RFP_Account_Term_Del]  
     
DESCRIPTION: 
	It Deletes SR RFP Account Term for Selected 
						SR RFP Account Term Id.
      
INPUT PARAMETERS:          
NAME							DATATYPE	DEFAULT		DESCRIPTION          
-------------------------------------------------------------------          
@SR_RFP_Account_Term_Id			INT	

   
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------

	Begin Tran
		EXEC SR_RFP_Account_Term_Del  16453
	Rollback Tran

	SELECT * FROM SR_RFP_Account_Term a
		WHERE NOT EXISTS (SELECT 1 FROM SR_RFP_SOP_DETAILS d WHERE d.SR_RFP_ACCOUNT_TERM_ID = a.SR_RFP_ACCOUNT_TERM_ID )
			AND NOT EXISTS (SELECT 1 FROM SR_RFP_SOP_SHORTLIST_DETAILS d WHERE d.SR_RFP_ACCOUNT_TERM_ID = a.SR_RFP_ACCOUNT_TERM_ID )
			AND NOT EXISTS (SELECT 1 FROM SR_RFP_TERM_PRODUCT_MAP d WHERE d.SR_RFP_ACCOUNT_TERM_ID = a.SR_RFP_ACCOUNT_TERM_ID )
			
			
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR			PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR		    28-MAY-10	CREATED     

*/  

CREATE PROCEDURE dbo.SR_RFP_Account_Term_Del
   (
    @SR_RFP_Account_Term_Id INT
   )
AS 
BEGIN

    SET NOCOUNT ON;
   
    DELETE 
   	FROM	
		dbo.SR_RFP_ACCOUNT_TERM
	WHERE 
		SR_RFP_ACCOUNT_TERM_ID = @SR_RFP_Account_Term_Id
END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_Account_Term_Del] TO [CBMSApplication]
GO
