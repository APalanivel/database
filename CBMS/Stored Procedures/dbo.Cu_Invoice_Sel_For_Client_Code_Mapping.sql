SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Cu_Invoice_Sel_For_Client_Code_Mapping]  
     
DESCRIPTION: 
	To Get Cu_Invoice_Id,Cu_Exception_Id for the given Ubm_Id, Ubm_Service_Code and Exception_Type_Id.
      
INPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
	@Ubm_Id				INT
	@Ubm_Service_Code	VARCHAR(200)
	@Exception_Type_Id	INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Cu_Invoice_Sel_For_Client_Code_Mapping 2,420900,1
	EXEC dbo.Cu_Invoice_Sel_For_Client_Code_Mapping 1,434,1

AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	PNR			Pandarinath

MODIFICATIONS
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			08/20/2010	Created

*/

CREATE PROCEDURE dbo.Cu_Invoice_Sel_For_Client_Code_Mapping
	  @Ubm_Id				INT
	 ,@Ubm_Service_Code		VARCHAR(200)
	 ,@Exception_Type_Id	INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		 i.Cu_Invoice_Id
		,e.Cu_Exception_Id
    FROM
		dbo.Cu_Invoice i
		JOIN dbo.Cu_Exception e
			 ON e.Cu_Invoice_Id = i.Cu_Invoice_Id
		JOIN dbo.Cu_Exception_Detail ed
			 ON ed.Cu_Exception_Id = e.Cu_Exception_Id
     WHERE
		i.Ubm_id = @Ubm_Id
        AND i.UBM_CLIENT_CODE = @Ubm_Service_Code
        AND ed.exception_type_id = @Exception_Type_Id
        AND ed.is_closed = 0

END
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Sel_For_Client_Code_Mapping] TO [CBMSApplication]
GO
