
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Pricing_Product_Sel_By_Country_Commodity]
           
DESCRIPTION:             
			To get SR pricing products
			
INPUT PARAMETERS:            
	Name			DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Country_Id		INT			NULL
    @Commodity_Id	INT			NULL


OUTPUT PARAMETERS:
	Name			DataType		Default		Description  
---------------------------------------------------------------------------------  


 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT * FROM dbo.SR_PRICING_PRODUCT
	
	EXEC dbo.SR_SAD_GET_SYSTEM_PRICE_PRODUCTS_P 1,1,0
	EXEC dbo.SR_SAD_GET_SYSTEM_PRICE_PRODUCTS_P 1,1,0,null,290
	EXEC dbo.SR_SAD_GET_SYSTEM_PRICE_PRODUCTS_P 1,1,0,null,291
	EXEC dbo.SR_SAD_GET_SYSTEM_PRICE_PRODUCTS_P 1,1,0,null,null,29
	EXEC dbo.SR_SAD_GET_SYSTEM_PRICE_PRODUCTS_P 1,1,0,1


 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2015-09-16	Added header
							Global Sourcing - Phase2 -Created							
******/
CREATE PROCEDURE [dbo].[SR_SAD_GET_SYSTEM_PRICE_PRODUCTS_P]
      (
       @userId VARCHAR
      ,@sessionId VARCHAR
      ,@rfpId INT -- for showing system level product for particular commodity with in RFP
      ,@Country_Id INT = NULL
      ,@Commodity_Id INT = NULL
      ,@SR_PRICING_PRODUCT_ID INT = NULL )
AS
BEGIN
      SET NOCOUNT ON;
      
      IF @rfpId = 0
            BEGIN

                  SELECT
                        srpr.SR_PRICING_PRODUCT_ID
                       ,srpr.COMMODITY_TYPE_ID
                       ,comm.Commodity_Name AS COMMODITY
                       ,srpr.PRODUCT_NAME
                       ,srpr.PRODUCT_DESCRIPTION
                       ,COUNTRY_NAME = REPLACE(LEFT(contrys.contrys_list, LEN(contrys.contrys_list) - 1), '&amp;', '&')
                  FROM
                        dbo.SR_PRICING_PRODUCT srpr
                        INNER JOIN dbo.Commodity comm
                              ON srpr.COMMODITY_TYPE_ID = comm.Commodity_Id
                        CROSS APPLY ( SELECT --TOP 10
                                          ISNULL(con.COUNTRY_NAME, '') + ', '
                                      FROM
                                          dbo.SR_Pricing_Product_Country_Map csrpr
                                          INNER JOIN dbo.COUNTRY con
                                                ON csrpr.Country_Id = con.COUNTRY_ID
                                      WHERE
                                          csrpr.SR_Pricing_Product_Id = srpr.SR_PRICING_PRODUCT_ID
                                      GROUP BY
                                          con.COUNTRY_NAME
                        FOR
                                      XML PATH('') ) contrys ( contrys_list )
                  WHERE
                        ( @Commodity_Id IS NULL
                          OR srpr.COMMODITY_TYPE_ID = @Commodity_Id )
                        AND ( @Country_Id IS NULL
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.SR_Pricing_Product_Country_Map csrpr
                                          WHERE
                                                csrpr.Country_Id = @Country_Id
                                                AND srpr.SR_PRICING_PRODUCT_ID = csrpr.SR_Pricing_Product_Id ) )
                        AND ( @SR_PRICING_PRODUCT_ID IS NULL
                              OR srpr.SR_PRICING_PRODUCT_ID = @SR_PRICING_PRODUCT_ID );
            END;
      ELSE
            IF @rfpId > 0
                  BEGIN

                        SELECT
                              SR_PRICING_PRODUCT.SR_PRICING_PRODUCT_ID
                             ,PRODUCT_NAME
                             ,PRODUCT_DESCRIPTION
                        FROM
                              SR_PRICING_PRODUCT
                             ,SR_RFP
                        WHERE
                              SR_RFP.COMMODITY_TYPE_ID = SR_PRICING_PRODUCT.COMMODITY_TYPE_ID
                              AND SR_RFP.SR_RFP_ID = @rfpId;
                  END;
END;
GO

GRANT EXECUTE ON  [dbo].[SR_SAD_GET_SYSTEM_PRICE_PRODUCTS_P] TO [CBMSApplication]
GO
