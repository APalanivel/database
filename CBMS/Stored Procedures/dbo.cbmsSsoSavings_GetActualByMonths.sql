SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE  [dbo].[cbmsSsoSavings_GetActualByMonths]

	( @sso_savings_id int
	, @start_date datetime = null
	, @end_date datetime = null
	, @currency_unit_id int
	, @client_id int = null
	)

AS
BEGIN

	set nocount on

	declare @convert_date datetime
	set @convert_date = convert(datetime, convert(varchar, month(getdate())) + '/1/' + convert(varchar, year(getdate())))


	   select sum(s.actual_savings_value * cuc.conversion_factor) as actual_term_savings
	     from sso_Savings_detail s
	     join sso_savings sa on sa.sso_savings_id = @sso_savings_id
             join client cl on cl.client_id = @client_id
  left outer join currency_unit_conversion cuc on cuc.currency_group_id = cl.currency_group_id and cuc.base_unit_id = sa.currency_unit_id and cuc.converted_unit_id = @currency_unit_id and cuc.conversion_date = @convert_date
	    where s.sso_savings_id = @sso_savings_id
	      and s.service_month >= @start_date
	      and s.service_month <= @end_date
	

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoSavings_GetActualByMonths] TO [CBMSApplication]
GO
