SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Country_Commodity_Uom_Merge]
           
DESCRIPTION:             
			To insert/update country and commodity specific default UOM data
			This sproc gets called when a new commodity / country is created . Commodity_Ins & cbmsCountry_Save
			
INPUT PARAMETERS:            
	Name			DataType		Default		Description  
---------------------------------------------------------------------------------  
	@Country_Id				INT			
    @Commodity_Id			INT			
    @Default_Uom_Type_Id	INT			
    @User_Info_Id			INT			


OUTPUT PARAMETERS:
	Name			DataType		Default		Description  
---------------------------------------------------------------------------------  


 USAGE EXAMPLES:
---------------------------------------------------------------------------------  

	SELECT * FROM dbo.ENTITY WHERE ENTITY_TYPE = 101
	SELECT * FROM dbo.ENTITY WHERE ENTITY_TYPE = 102

	BEGIN TRANSACTION
		SELECT * FROM dbo.Country_Commodity_Uom WHERE COUNTRY_ID = 4 AND Commodity_Id = 290
		EXEC dbo.Country_Commodity_Uom_Merge 4, 290, 12, 49
		SELECT * FROM dbo.Country_Commodity_Uom WHERE COUNTRY_ID = 4 AND Commodity_Id = 290
		EXEC dbo.Country_Commodity_Uom_Merge 4, 290, 1566, 16
		SELECT * FROM dbo.Country_Commodity_Uom WHERE COUNTRY_ID = 4 AND Commodity_Id = 290
	ROLLBACK TRANSACTION

	BEGIN TRANSACTION
		SELECT * FROM dbo.Country_Commodity_Uom WHERE COUNTRY_ID = 4 AND Commodity_Id = 291
		EXEC dbo.Country_Commodity_Uom_Merge 4, 291, 25, 49
		SELECT * FROM dbo.Country_Commodity_Uom WHERE COUNTRY_ID = 4 AND Commodity_Id = 291
		EXEC dbo.Country_Commodity_Uom_Merge 4, 291, 1545, 16
		SELECT * FROM dbo.Country_Commodity_Uom WHERE COUNTRY_ID = 4 AND Commodity_Id = 291
	ROLLBACK TRANSACTION


 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date			Modification
------------------------------------------------------------
	RR			2015-07-09		Global Sourcing - Created
								
******/
CREATE PROCEDURE [dbo].[Country_Commodity_Uom_Merge]
      ( 
       @Country_Id INT
      ,@Commodity_Id INT
      ,@Default_Uom_Type_Id INT
      ,@User_Info_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;

      MERGE INTO dbo.Country_Commodity_Uom AS tgt
            USING 
                  ( SELECT
                        cy.Country_Id AS Country_Id
                       ,com.Commodity_Id AS Commodity_Id
                       ,ISNULL(@Default_Uom_Type_Id, com.Default_UOM_Entity_Type_Id) AS Default_Uom_Type_Id
                       ,@User_Info_Id AS User_Info_Id
                    FROM
                        dbo.Country cy
                        CROSS JOIN dbo.Commodity com
                    WHERE
                        ( @Country_Id IS NULL
                          OR cy.Country_Id = @Country_Id )
                        AND ( ( @Commodity_Id IS NULL
                                AND ( com.Is_Active = 1
                                      AND com.Commodity_Id != -1 ) )
                              OR com.Commodity_Id = @Commodity_Id ) ) AS src
            ON tgt.Country_Id = src.Country_Id
                  AND tgt.Commodity_Id = src.Commodity_Id
            WHEN NOT MATCHED 
                  THEN
            INSERT
                        ( 
                         Commodity_Id
                        ,Country_Id
                        ,Default_Uom_Type_Id
                        ,Created_User_Id
                        ,Created_Ts
                        ,Updated_User_Id
                        ,Last_Change_Ts )
                    VALUES
                        ( 
                         src.Commodity_Id
                        ,src.Country_Id
                        ,src.Default_Uom_Type_Id
                        ,src.User_Info_Id
                        ,GETDATE()
                        ,src.User_Info_Id
                        ,GETDATE() )
            WHEN MATCHED 
                  THEN
            UPDATE  SET 
                        tgt.Default_Uom_Type_Id = src.Default_Uom_Type_Id
                       ,tgt.Updated_User_Id = src.User_Info_Id
                       ,tgt.Last_Change_Ts = GETDATE();  
            
                         
END;

;
GO
GRANT EXECUTE ON  [dbo].[Country_Commodity_Uom_Merge] TO [CBMSApplication]
GO
