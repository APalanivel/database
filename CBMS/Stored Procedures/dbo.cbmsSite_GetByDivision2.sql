SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsSite_GetByDivision2

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@division_id   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE     PROCEDURE [dbo].[cbmsSite_GetByDivision2]
	( @MyAccountId int
	, @division_id int
	)
AS
BEGIN

	   select s.site_id
		, vw.site_name
		, d.division_name
		, d.division_id
		, ad.city
		, ad.address_line1
		, ad.address_line2
		, st.state_name
	     from site s with (nolock)
	     join vwSiteName vw with (nolock) on vw.site_id = s.site_id
	     join division d with (nolock) on d.division_id = @division_id
    	     join address ad with (nolock) on ad.address_id = s.primary_address_id
	     join state st with (nolock) on st.state_id = ad.state_id
	    where s.division_id = @division_id
	order by vw.site_name asc

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSite_GetByDivision2] TO [CBMSApplication]
GO
