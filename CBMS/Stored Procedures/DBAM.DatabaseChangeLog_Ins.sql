SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME: dbam.DatabaseChangeLog_Ins

DESCRIPTION:
	Inserts a record into dbam.Database_Change_Log

INPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	@ObjectName				Varchar(255)
	@Revision				Varchar(255)
	@ChangeType				Varchar(25)
	@ObjectType				Varchar(25)
	@ReviewName				Varchar(255)
	@ReleaseName			Varchar(255)	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------




AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CMH		Chad Hattabaugh
	DMR		Deana Ritter
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	CMH			03/19/2012	Created
	CMH			03/23/2012	Added SARG to Avoid adding 
	DMR			10/08/2013  Modified Object revision to Varchar for GIT Conversion
******/
CREATE PROCEDURE [DBAM].[DatabaseChangeLog_Ins]
(	@ObjectName				VARCHAR(255)
	,@ObjectRevision		VARCHAR(255)
	,@ChangeType			VARCHAR(25)
	,@ObjectType			VARCHAR(25)
	,@ReviewName			VARCHAR(255)
	,@ReleaseName			VARCHAR(255)
)
AS
BEGIN 
	SET NOCOUNT ON; 
	
	INSERT INTO dbam.Database_Change_Log 
	(	Object_Name
		,Object_Revision
		,Change_Type
		,Object_type
		,Review_Name
		,Release_Name
	)
	SELECT 
		@ObjectName
		,@ObjectRevision
		,@ChangeType
		,@ObjectType
		,@ReviewName
		,@ReleaseName
	WHERE NOT EXISTS (SELECT 1
						FROM 
							dbam.Database_Change_Log 
						WHERE 
							Object_Name = @ObjectName
							AND Object_Revision = @ObjectRevision
							AND Object_Type = @ObjectType
							AND Release_Name = @ReleaseName
							AND Review_Name = @ReviewName)
		
END

GO
GRANT EXECUTE ON  [DBAM].[DatabaseChangeLog_Ins] TO [CBMSApplication]
GO
