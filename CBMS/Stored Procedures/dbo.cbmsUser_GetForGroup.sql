SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsUser_GetForGroup

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@group_info_id 	int       	null      	
	@is_history    	bit       	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE        Procedure dbo.cbmsUser_GetForGroup
	( @MyAccountId int
	, @group_info_id int = null
	, @is_history bit = null
	)
As
BEGIN
	set nocount on	
	   select distinct ui.user_info_id
		, ui.username
		, ui.queue_id
		, null passcode
		, ui.first_name
		, ui.middle_name
		, ui.last_name
		, ui.first_name + ' ' + ui.last_name full_name
		, ui.email_address
		, ui.is_history
		, ui.access_level
		, ui.client_id
		, ui.division_id
		, ui.site_id
	     from user_info ui
	     join user_info_group_info_map map on map.user_info_id = ui.user_info_id
	    where map.group_info_id = isNull(@group_info_id, map.group_info_id)
	      and ui.is_history = isNull(@is_history, ui.is_history)
	 order by ui.first_name
		, ui.last_name

END
GO
GRANT EXECUTE ON  [dbo].[cbmsUser_GetForGroup] TO [CBMSApplication]
GO
