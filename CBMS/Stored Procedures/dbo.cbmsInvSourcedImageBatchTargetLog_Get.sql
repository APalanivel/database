SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[cbmsInvSourcedImageBatchTargetLog_Get]
	( @MyAccountId int
	, @inv_sourced_image_batch_target_log_id int
	)
AS
BEGIN

   select inv_sourced_image_batch_target_log_id
	, target_folder_path
	, file_count
     from inv_sourced_image_batch_target_log
    where inv_sourced_image_batch_target_log_id = @inv_sourced_image_batch_target_log_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvSourcedImageBatchTargetLog_Get] TO [CBMSApplication]
GO
