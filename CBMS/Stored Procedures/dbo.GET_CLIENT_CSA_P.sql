SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_CLIENT_CSA_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@clientID      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

exec dbo.GET_CLIENT_CSA_P 10003

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	NR			Narayana Reddy

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	NR			2019-10-30	Add Contract - Added Order by clause.
******/

CREATE PROCEDURE [dbo].[GET_CLIENT_CSA_P]
    (
        @clientID INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            userInfo.LAST_NAME
            , userInfo.FIRST_NAME
            , userInfo.USER_INFO_ID
        FROM
            dbo.CLIENT_CSA_MAP csa
            INNER JOIN dbo.USER_INFO userInfo
                ON userInfo.USER_INFO_ID = csa.USER_INFO_ID
        WHERE
            userInfo.IS_HISTORY = 0
            AND csa.CLIENT_ID = @clientID
        ORDER BY
            userInfo.FIRST_NAME + ' ' + userInfo.LAST_NAME;


    END;

GO
GRANT EXECUTE ON  [dbo].[GET_CLIENT_CSA_P] TO [CBMSApplication]
GO
