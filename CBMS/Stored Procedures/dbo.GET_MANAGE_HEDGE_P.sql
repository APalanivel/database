SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.GET_MANAGE_HEDGE_P
	@contract_id INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT h.hedge_id,
		h.hedge_name,
		h.hedge_transaction_date,
		e.entity_name,
		e1.entity_name ,
		e2.entity_name,
		hedge_comments,
		DATENAME(MONTH,hd.month_identifier),
		DATENAME(YEAR,hd.month_identifier),
		hd.hedge_volume,
		hd.hedge_price,
		cu.currency_unit_name 
	FROM dbo.currency_unit cu INNER JOIN dbo.hedge h ON h.currency_unit_id=cu.currency_unit_id
		INNER JOIN dbo.hedge_detail hd ON hd.hedge_id = h.hedge_id
		INNER JOIN dbo.entity e ON e.entity_id=h.transaction_by_type_id
		INNER JOIN dbo.entity e1 ON e1.entity_id=h.deal_type_id
		INNER JOIN dbo.entity e2 ON e2.entity_id=h.hedge_unit_type_id
	WHERE h.contract_id=@contract_id
	ORDER BY h.hedge_id

END
GO
GRANT EXECUTE ON  [dbo].[GET_MANAGE_HEDGE_P] TO [CBMSApplication]
GO
