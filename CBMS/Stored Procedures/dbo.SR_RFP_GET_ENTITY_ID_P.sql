SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_ENTITY_ID_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@commodityName 	varchar(15)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE dbo.SR_RFP_GET_ENTITY_ID_P 
@userId varchar,
@sessionId varchar,
@commodityName varchar(15)

as
set nocount on
DECLARE @entityId int
SELECT @entityId = (select entity_id from entity where entity_type=157 and entity_name=@commodityName)


RETURN  @entityId
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_ENTITY_ID_P] TO [CBMSApplication]
GO
