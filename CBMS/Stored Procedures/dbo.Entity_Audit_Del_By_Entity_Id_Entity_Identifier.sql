SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Entity_Audit_Del_By_Entity_Id_Entity_Identifier]  
     
DESCRIPTION:

	It Deletes Entity Audit for given Entity Identifier and entity_id

INPUT PARAMETERS:
NAME				DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------          
@Entity_id			INT						This will tell if this procedure has to delete Site/Account/Contract(SELECT * FROM ENTITY WHERE ENTITY_TYPE = 500)
@Entity_Identifier	INT						Value of the Entity(Site_id/Account_id/Contract_Id)

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	SELECT * FROM ENTITY WHERE ENTITY_TYPE = 500

	BEGIN TRAN
	
		EXEC dbo.Entity_Audit_Del_By_Entity_Id_Entity_Identifier 491, 7310
	ROLLBACK TRAN

    SELECT * FROM Entity_Audit WHERE entity_id = 491 AND ENTITY_IDENTIFIER = 7310

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			26-MAY-10	CREATED

*/

CREATE PROCEDURE dbo.Entity_Audit_Del_By_Entity_Id_Entity_Identifier
(
	@Entity_Id			INT
	,@Entity_Identifier	INT
)
AS
BEGIN

    SET NOCOUNT ON;

	DELETE
		dbo.ENTITY_AUDIT
	WHERE
		ENTITY_ID = @Entity_Id
		AND ENTITY_IDENTIFIER = @Entity_Identifier

END
GO
GRANT EXECUTE ON  [dbo].[Entity_Audit_Del_By_Entity_Id_Entity_Identifier] TO [CBMSApplication]
GO
