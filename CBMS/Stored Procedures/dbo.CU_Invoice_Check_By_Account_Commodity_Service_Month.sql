
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME: 

	dbo.CU_Invoice_Check_By_Account_Commodity_Service_Month 

DESCRIPTION:

Used to Check the invoice and manual data presence by given account id and service month
This procedure will be called to know the what data source code should be updated in Site level or for the given account.

INPUT PARAMETERS:

      Name				DataType          Default     Description
------------------------------------------------------------------
      @Account_Id		INT
      @Service_Month	DATETIME
      @Commodity_Id	     INT

OUTPUT PARAMETERS:
      Name                DataType          Default     Description        
------------------------------------------------------------

    
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.CU_Invoice_Check_By_Account_Commodity_Service_Month  1, '2005-09-01', 290
	EXEC dbo.CU_Invoice_Check_By_Account_Commodity_Service_Month  1, '2005-01-01', 290

	SELECT TOP 10 * FROM Cost_usage WHERE EL_DATA_SOURCE_CD = 100348
	
	SELECT* FROM Code WHERE CodeSet_id = 119

AUTHOR INITIALS:
 Initials	Name
------------------------------------------------------------
 HG			Harihara Suthan G

MODIFICATIONS

 Initials	Date		Modification
------------------------------------------------------------
 HG			11/08/2011	Created
 HG			2016-11-21	With Recompile option added as this sproc is trying to use the wrong plan and runs more than 2 minutes
******/

CREATE PROCEDURE [dbo].[CU_Invoice_Check_By_Account_Commodity_Service_Month]
      (
       @Account_Id INT
      ,@Service_Month DATETIME
      ,@Commodity_Id INT )
      WITH RECOMPILE
AS
BEGIN  

      SET NOCOUNT ON;  

      DECLARE @Is_Invoice_Exist BIT = 0;

      IF EXISTS ( SELECT
                        1
                  FROM
                        dbo.CU_INVOICE cui
                        JOIN dbo.CU_INVOICE_SERVICE_MONTH cusm
                              ON cui.CU_INVOICE_ID = cusm.CU_INVOICE_ID
                        JOIN dbo.CU_INVOICE_DETERMINANT cuid
                              ON cuid.CU_INVOICE_ID = cui.CU_INVOICE_ID
                  WHERE
                        cusm.Account_ID = @Account_Id
                        AND cusm.SERVICE_MONTH = @Service_Month
                        AND cuid.COMMODITY_TYPE_ID = @Commodity_Id
                        AND cui.IS_PROCESSED = 1
                        AND cui.IS_REPORTED = 1
                        AND cui.IS_DNT = 0
                        AND cui.IS_DUPLICATE = 0 )
            OR EXISTS ( SELECT
                              1
                        FROM
                              dbo.CU_INVOICE cui
                              JOIN dbo.CU_INVOICE_SERVICE_MONTH cusm
                                    ON cui.CU_INVOICE_ID = cusm.CU_INVOICE_ID
                              JOIN dbo.CU_INVOICE_CHARGE cuic
                                    ON cuic.CU_INVOICE_ID = cui.CU_INVOICE_ID
                        WHERE
                              cusm.Account_ID = @Account_Id
                              AND cusm.SERVICE_MONTH = @Service_Month
                              AND cuic.COMMODITY_TYPE_ID = @Commodity_Id
                              AND cui.IS_PROCESSED = 1
                              AND cui.IS_REPORTED = 1
                              AND cui.IS_DNT = 0
                              AND cui.IS_DUPLICATE = 0 )
            BEGIN
                  SET @Is_Invoice_Exist = 1;
            END;

      
      SELECT
            @Is_Invoice_Exist Is_Invoice_Exist;

END;

;
GO

GRANT EXECUTE ON  [dbo].[CU_Invoice_Check_By_Account_Commodity_Service_Month] TO [CBMSApplication]
GO
