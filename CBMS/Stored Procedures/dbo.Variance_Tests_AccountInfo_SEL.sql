
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********   
NAME:  dbo.Variance_Tests_AccountInfo_SEL  
 
DESCRIPTION:  This will retrieve all the all the account related information

INPUT PARAMETERS:    
    Name				   DataType          Default     Description    
------------------------------------------------------------    
    @Account_Id		   INT
    @Site_Client_Hier_Id	   INT
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:  
------------------------------------------------------------  
	Variance_Tests_AccountInfo_SEL 87154, 2498
	Variance_Tests_AccountInfo_SEL 141640, 6274
		
AUTHOR INITIALS:  
Initials	  Name  
------------------------------------------------------------  
NK		  Nageswara Rao Kosuri         
AP		  Athmaram Pabbathi
SP		  Sandeep Pigilam

Initials	  Date	    Modification  
------------------------------------------------------------  
NK		  10/29/2009  Created
AP		  04/18/2012  Addnl Data Changes
					   --Added @Site_Client_Hier_Id parameter
SP		  2014-09-09 For Data Transition used a table variable @Group_Legacy_Name to handle the hardcoded group names  
					   
******/  
CREATE        PROCEDURE [dbo].[Variance_Tests_AccountInfo_SEL]
      ( 
       @Account_Id INT
      ,@Site_Client_Hier_Id INT )
AS 
BEGIN  
      SET NOCOUNT ON 
 
      DECLARE
            @Supplier_Account_Type_Id INT
           ,@Utility_Account_Type_Id INT

      DECLARE @Group_Legacy_Name TABLE
            ( 
             GROUP_INFO_ID INT
            ,GROUP_NAME VARCHAR(200)
            ,Legacy_Group_Name VARCHAR(200) )           
 
 
      SELECT
            @Supplier_Account_Type_Id = MAX(CASE WHEN at.Entity_Name = 'Supplier' THEN at.Entity_Id
                                            END)
           ,@Utility_Account_Type_Id = MAX(CASE WHEN at.Entity_Name = 'Utility' THEN at.Entity_Id
                                           END)
      FROM
            dbo.Entity at
      WHERE
            at.Entity_Description = 'Account Type'
            AND at.Entity_Name IN ( 'Supplier', 'Utility' )

      INSERT      INTO @Group_Legacy_Name
                  ( 
                   GROUP_INFO_ID
                  ,GROUP_NAME
                  ,Legacy_Group_Name )
                  EXEC dbo.Group_Info_Sel_By_Group_Legacy 
                        @Group_Legacy_Name_Cd_Value = 'Quality_Assurance'             
 
      SELECT
            cha.Account_Id
           ,ch.Client_Id
           ,ch.Client_Name
           ,cha.Account_Vendor_Id AS Vendor_Id
           ,cha.Account_Vendor_Name AS Vendor_Name
           ,ch.Site_Id
           ,RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_Name + ')' AS Site_Name
           ,ch.State_Name
           ,ISNULL(cha.Account_Number, 'Not Yet Assigned') AS Account_Number
           ,( CASE WHEN cha.Account_Type = 'Supplier' THEN @Supplier_Account_Type_Id
                   WHEN cha.Account_Type = 'Utility' THEN @Utility_Account_Type_Id
              END ) AS Account_Type_Id
           ,cha.Account_Type
           ,cha.Meter_City AS City
           ,COALESCE(ui.Queue_Id, uir.Queue_Id) AS Analyst_Id
           ,COALESCE(ui.First_Name + SPACE(1) + ui.Last_Name, uir.First_Name + SPACE(1) + uir.Last_Name) AS Queue_Name
      FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account cha
                  ON cha.Client_Hier_Id = ch.Client_Hier_Id
            LEFT OUTER JOIN Core.Client_Hier_Account ucha
                  ON ucha.Meter_Id = cha.Meter_Id
                     AND cha.Account_Type = 'Supplier'
                     AND ucha.Account_Type = 'Utility'
            LEFT OUTER JOIN dbo.Region_Manager_Map rmm
                  ON rmm.Region_Id = ch.Region_Id
            LEFT OUTER JOIN dbo.User_Info uir
                  ON uir.User_Info_Id = rmm.User_Info_Id
            LEFT OUTER JOIN ( dbo.Utility_Detail ud
                              INNER JOIN dbo.Utility_Detail_Analyst_Map map
                                    ON map.Utility_Detail_Id = ud.Utility_Detail_Id
                              INNER JOIN dbo.Group_info gi
                                    ON gi.Group_Info_Id = map.Group_Info_Id
                              INNER JOIN @Group_Legacy_Name gil
                                    ON gi.Group_Info_ID = gil.GROUP_INFO_ID
                              INNER JOIN dbo.[Queue] que
                                    ON que.Queue_Id = gi.Queue_Id
                              INNER JOIN dbo.User_Info ui
                                    ON map.Analyst_Id = ui.User_Info_Id
                              INNER JOIN dbo.Codeset cdset
                                    ON cdset.Codeset_Id = que.queue_type_id )
                              ON ( ( cha.Account_Type = 'Utility'
                                     AND ud.Vendor_id = cha.Account_Vendor_Id )
                                   OR ( cha.Account_Type = 'Supplier'
                                        AND ud.Vendor_Id = ucha.Account_Vendor_Id ) )
                                 AND cdset.Codeset_Name = 'InvoiceProcessingTeam'
      WHERE
            ch.Client_Hier_Id = @Site_Client_Hier_Id
            AND cha.Account_Id = @Account_Id
            AND ( cha.Account_Type = 'Utility'
                  OR ( cha.Account_Type = 'Supplier'
                       AND ( cha.Supplier_Meter_Disassociation_Date > cha.Supplier_Account_Begin_Dt
                             OR cha.Supplier_Meter_Disassociation_Date IS NULL )
                       AND cha.Supplier_Contract_ID IS NOT NULL ) )
      GROUP BY
            cha.Account_Id
           ,ch.Client_Id
           ,ch.Client_Name
           ,cha.Account_Vendor_Id
           ,cha.Account_Vendor_Name
           ,ch.Site_Id
           ,ch.City
           ,ch.State_Name
           ,ch.Site_Name
           ,ch.State_Name
           ,cha.Account_Number
           ,cha.Account_Type
           ,cha.Meter_City
           ,ui.Queue_Id
           ,uir.Queue_Id
           ,ui.First_Name + SPACE(1) + ui.Last_Name
           ,uir.First_Name + SPACE(1) + uir.Last_Name
 
END;
;
GO


GRANT EXECUTE ON  [dbo].[Variance_Tests_AccountInfo_SEL] TO [CBMSApplication]
GO
