SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** 

NAME: dbo.Notification_Msg_Dtl_Status_Upd_By_Msg_Queue_Id 

DESCRIPTION: 
				To update the status of the message

INPUT PARAMETERS: 
Name						DataType	Default		Description 
------------------------------------------------------------------------------- 
@Notification_Msg_Queue_Id	INT 
@Msg_Status					VARCHAR(50) Sent		Status of the message 


OUTPUT PARAMETERS:   
Name       DataType  Default  Description  
------------------------------------------------------------------------------- 

USAGE EXAMPLES:   
-------------------------------------------------------------------------------   

BEGIN TRANSACTION
SELECT
      Notification_Msg_Queue_Id,c.Code_Value
FROM
      dbo.Notification_Msg_Dtl nmd
      INNER JOIN dbo.Code c
            ON c.Code_Id = nmd.Msg_Delivery_Status_Cd
WHERE
      Notification_Msg_Queue_Id = 8543
      
EXEC dbo.Notification_Msg_Dtl_Status_Upd_By_Msg_Queue_Id 
      @Notification_Msg_Queue_Id = 8543
     ,@Msg_Status = 'Sent'
     
     SELECT
      Notification_Msg_Queue_Id,c.Code_Value
FROM
      dbo.Notification_Msg_Dtl nmd
      INNER JOIN dbo.Code c
            ON c.Code_Id = nmd.Msg_Delivery_Status_Cd
WHERE
      Notification_Msg_Queue_Id = 8543
      
ROLLBACK TRANSACTION  

AUTHOR INITIALS:   
Initials	Name   
-------------------------------------------------------------------------------   
RR			Raghu Reddy


MODIFICATIONS:  
Initials	Date		Modification 
-------------------------------------------------------------------------------   
RR			2016-05-30  Created  For GCS Phase- 5.
	 	 		 
******/

CREATE PROCEDURE [dbo].[Notification_Msg_Dtl_Status_Upd_By_Msg_Queue_Id]
      ( 
       @Notification_Msg_Queue_Id INT
      ,@Msg_Status VARCHAR(50) = 'Sent' )
AS 
BEGIN 
      SET NOCOUNT ON 
 	 	 	 		  
      DECLARE @Msg_Delivery_Status_Cd INT 
 	 	 	 		  
      SELECT
            @Msg_Delivery_Status_Cd = cd.Code_Id
      FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code cd
                  ON cd.Codeset_id = cs.Codeset_Id
      WHERE
            cd.Code_Value = @Msg_Status
            AND cs.Codeset_Name = 'Message Delivery Status' 
 	 	 	 		 
      UPDATE
            dbo.Notification_Msg_Dtl
      SET   
            Msg_Delivery_Status_Cd = @Msg_Delivery_Status_Cd
      WHERE
            Notification_Msg_Queue_Id = @Notification_Msg_Queue_Id 
 	 	 	 		 
END; 


;
GO
GRANT EXECUTE ON  [dbo].[Notification_Msg_Dtl_Status_Upd_By_Msg_Queue_Id] TO [CBMSApplication]
GO
