SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_LP_UPDATE_ADDITIONAL_ROW_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
	@user_id varchar(10),
	@session_id varchar(20),
	@row_id int,
	@row_name varchar(100),
	@row_value varchar(200)
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- Test an update of entry
--exec dbo.SR_RFP_LP_UPDATE_ADDITIONAL_ROW_P
--	@user_id = 1,
--	@session_id = -1,
--	@row_id = 7958,
--	@row_name ='Transmission PLS',  -- Original Value = Transmission PLS
--	@row_value = ''


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_LP_UPDATE_ADDITIONAL_ROW_P
	@user_id varchar(10),
	@session_id varchar(20),
	@row_id int,
	@row_name varchar(100),
	@row_value varchar(200)
	AS
	
set nocount on	
	

	UPDATE sr_rfp_lp_account_additional_row 
	SET row_name = @row_name, row_value = @row_value
	WHERE sr_rfp_lp_account_additional_row_id = @row_id
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_LP_UPDATE_ADDITIONAL_ROW_P] TO [CBMSApplication]
GO
