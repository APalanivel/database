SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



--exec dbo.GET_CONTRACT_NUMBER_QUERY_P '31837%'

create  PROCEDURE dbo.GET_CONTRACT_NUMBER_QUERY_P
	@ed_contract_number VARCHAR(150)
AS
BEGIN

	SET NOCOUNT ON

	SELECT ed_contract_number
	FROM dbo.[contract]
	WHERE ed_contract_number like @ed_contract_number
	ORDER BY ed_contract_number DESC

END

GO
GRANT EXECUTE ON  [dbo].[GET_CONTRACT_NUMBER_QUERY_P] TO [CBMSApplication]
GO
