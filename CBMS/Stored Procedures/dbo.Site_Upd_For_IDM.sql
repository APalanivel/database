SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME: [dbo].[Site_Upd_For_IDM]
     
DESCRIPTION: 
	procedure to update IDM Node XID
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
 @SiteIDMInfo AS dbo.IDM_Site_Info
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        

     declare @SiteIDMInfo AS dbo.IDM_Site_Info
	 insert into @SiteIDMInfo([Client_Hier_Id],[IDM_Node_Id])
	 values(3703,679335)
	 exec [dbo].[Site_Upd_For_IDM] 

AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------
	KVK		Vinay K

MODIFICATIONS

INITIALS	DATE		MODIFICATION
------------------------------------------------------------          
	KVK		12/14/2014	created
*/
CREATE PROCEDURE [dbo].[Site_Upd_For_IDM]
      (
       @SiteIDMInfo AS dbo.IDM_Site_Info READONLY )
AS
BEGIN

      SET NOCOUNT ON;
     
	  UPDATE
            s
      SET
            s.IDM_Node_XId = tvp_s.IDM_Node_Id
      FROM
            dbo.SITE s
            INNER JOIN core.Client_Hier ch
                  ON ch.Site_Id = s.SITE_ID
            INNER JOIN @SiteIDMInfo tvp_s
                  ON ch.Client_Hier_Id = tvp_s.Client_Hier_Id
      WHERE
            isnull(s.IDM_Node_XId, 0) <> isnull(tvp_s.IDM_Node_Id, 0)
   
END
;
GO
GRANT EXECUTE ON  [dbo].[Site_Upd_For_IDM] TO [CBMSApplication]
GO
