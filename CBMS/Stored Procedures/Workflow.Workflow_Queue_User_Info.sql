SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

  
/******      
NAME:    [Workflow].[Workflow_Queue_User_Info]  
DESCRIPTION:   
------------------------------------------------------------     
 INPUT PARAMETERS:      
 Name   DataType  Default Description        
  @session_id INT       
------------------------------------------------------------      
 OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
 USAGE EXAMPLES:   --exec [Workflow].[Workflow_Queue_User_Info] 49    
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
TRK   Ramakrishna Thummala Summit Energy   
   
 MODIFICATIONS       
 Initials Date   Modification      
------------------------------------------------------------      
 TRK    Sep-2019  Created  
  
******/  
CREATE PROCEDURE [Workflow].[Workflow_Queue_User_Info]    
      
    (        
        @session_id INT        
    )        
AS        
    BEGIN        
          SELECT        
            SI.SESSION_INFO_ID        
            , SI.USER_INFO_ID ,      
   UI.QUEUE_ID,      
   UI.FIRST_NAME+' '+UI.LAST_NAME as USERNAME       
        FROM        
            dbo.SESSION_INFO  SI left join USER_INFO UI on SI.USER_INFO_ID=UI.USER_INFO_ID      
        WHERE        
            SI.SESSION_INFO_ID = @session_id;        
    END;     
    

GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_User_Info] TO [CBMSApplication]
GO
