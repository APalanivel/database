
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	cbms_prod.dbo.BUDGET_GET_BUDGET_RESULTS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@userId				VARCHAR(10)	        
  
	@Is_History			BIT				0		If 1, get the history
												If 0, get the present
	@SortColumn			VARCHAR(200)	'duedate,clientName,utility'
    @Sortindex			VARCHAR(20)		'Asc'
	@Total_RowCount		INT				0
	@Start_Index		INT				1
	@End_Index			INT				2147483647
        	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC dbo.BUDGET_GET_BUDGET_RESULTS_P 27032
	EXEC dbo.BUDGET_GET_BUDGET_RESULTS_P 36217
	EXEC dbo.BUDGET_GET_BUDGET_RESULTS_P  9231,0,'duedate,clientName,utility','asc',0,1,200
	EXEC dbo.BUDGET_GET_BUDGET_RESULTS_P  9231,0,'status','asc',0,1,200
	EXEC dbo.BUDGET_GET_BUDGET_RESULTS_P  9231,0,'status','desc',0,1,200
	EXEC dbo.BUDGET_GET_BUDGET_RESULTS_P  9231,0,'completedby','asc',0,1,200
	EXEC dbo.BUDGET_GET_BUDGET_RESULTS_P  9231,0,'completedby','desc',0,1,200
	EXEC dbo.BUDGET_GET_BUDGET_RESULTS_P  9231,0,'lastroutedby','asc',0,1,200


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
	CPE			Chaitanya Panduga Eshwar
	SP			Sandeep Pigilam           


MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	SKA        	22-Jul-2009 Removed unused variables
	SKA        	06/24/2010	Added pagination Logic
							Refer Commodity in place of Entity
	PNR			08/04/2010	To make it uniform across modified the order of the input paramters.						
	SKA			08/19/2010	Added the usage example for sorting input parameters
	RR			07/20/2011	Maint ? 733: Modified as the default sort order "duedate,clientName,utility"
	CPE			2013-04-08	Performance tuned for Commercial Budgets
	CPE			2013-06-03	When >30 days is selected, all the records should be returned	
    SP			2014-09-09  For Data Transition used a table variable @Group_Legacy_Name to handle the hardcoded group names  
	HG			2015-03-16	MAINT-3433, changed the legacy group name to "Regulated Markets Budgets"
	RKV         2017-05-23  Removed Parameter Is_Before_Due_Date as part of SE2017-26			
	RR			2017-05-30	SE2017-26 Modified script to get budget accounts in a user queue from table dbo.Budget_Account_Queue  
******/

CREATE PROCEDURE [dbo].[BUDGET_GET_BUDGET_RESULTS_P]
      ( 
       @userId INT
      ,@Is_History BIT = 0
      ,@SortColumn VARCHAR(500) = 'dueDate,clientName,utility'
      ,@SortIndex VARCHAR(10) = 'Asc'
      ,@Total_RowCount INT = 0
      ,@Start_Index INT = 1
      ,@End_Index INT = 2147483647
      ,@Client_Id INT = NULL
      ,@Status VARCHAR(25) = NULL
      ,@State_Id INT = NULL
      ,@Vendor_Id INT = NULL
      ,@Rate_Id INT = NULL
      ,@T_Tr VARCHAR(15) = NULL )
AS 
BEGIN

      SET NOCOUNT ON  
  
      DECLARE
            @SQLStatement NVARCHAR(MAX)
           ,@ParamList NVARCHAR(500)
           ,@Rates_Queue_Cd INT
           ,@Both_Queue_Cd INT
          

                

      CREATE TABLE #Accounts
            ( 
             Account_Id INT
            ,Commodity_Id INT
            ,service_level_char VARCHAR(1)
            ,Account_Vendor_Id INT
            ,Account_Number VARCHAR(50)
            ,Account_Vendor_Name VARCHAR(200)
            ,Client_Hier_Id INT
            ,tariff_Transport VARCHAR(50) PRIMARY KEY ( tariff_transport, service_level_char, Account_Id, Client_Hier_Id, Commodity_Id, Budget_Account_Id )
            ,Budget_Account_Id INT
            ,Analyst VARCHAR(100)
            ,Alternate_Account_Number NVARCHAR(200) )
		   
      
	  
      SELECT
            @Rates_Queue_Cd = c.Code_Id
      FROM
            dbo.Code AS c
            JOIN dbo.Codeset AS cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'IncludeInAnalystQueue'
            AND c.Code_Value = 'Yes - Rates only'
  
      SELECT
            @Both_Queue_Cd = c.Code_Id
      FROM
            dbo.Code AS c
            JOIN dbo.Codeset AS cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'IncludeInAnalystQueue'
            AND c.Code_Value = 'Yes - Rates and Sourcing'

      SELECT
            @SortColumn = CASE @SortColumn
                            WHEN 'budgetId' THEN 'ba.budget_id'
                            WHEN 'clientName' THEN 'ch.Client_Name'
                            WHEN 'SiteName' THEN 'RTRIM(ch.City) + '', '' + ch.State_Name + '' ('' + ch.Site_name + '')'''
                            WHEN 'accountId' THEN 'cha.account_id'
                            WHEN 'budgetAccountId' THEN 'ba.BUDGET_ACCOUNT_ID'
                            WHEN 'accountNumber' THEN 'cha.account_number'
                            WHEN 'dueDate' THEN 'b.DUE_DATE'
                            WHEN 'utility' THEN 'cha.Account_Vendor_Name'
                            WHEN 'rates' THEN 'LEFT(Rates.Rate_Names, LEN(Rates.Rate_Names) - 1)'
                            WHEN 'commodity' THEN 'c.Commodity_Name'
                            WHEN 'startYear' THEN 'b.budget_start_year'
                            WHEN 'startMonth' THEN 'b.budget_start_month'
                            WHEN 'isPosted' THEN 'b.posted_by'
                            WHEN 'dueDate,clientName,utility' THEN 'b.DUE_DATE, ch.Client_Name,cha.Account_Vendor_Name'
                            WHEN 'status' THEN 'ISNULL(ba.Rates_Completed_Date,''2079-06-06'')'
                            WHEN 'completedby' THEN 'rcby.FIRST_NAME + '' '' + rcby.LAST_NAME'
                            WHEN 'lastroutedby' THEN 'routby.FIRST_NAME + '' '' + routby.LAST_NAME'
                            ELSE @SortColumn
                          END

      
      INSERT      #Accounts
                  ( 
                   Account_Id
                  ,Commodity_Id
                  ,service_level_char
                  ,Account_Vendor_Id
                  ,Account_Number
                  ,Account_Vendor_Name
                  ,Client_Hier_Id
                  ,tariff_Transport
                  ,Budget_Account_Id
                  ,Analyst
                  ,Alternate_Account_Number )
                  SELECT
                        cha.Account_Id
                       ,cha.Commodity_Id
                       ,Service_Level.ENTITY_NAME AS service_level_char
                       ,cha.Account_Vendor_Id
                       ,cha.Account_Number
                       ,cha.Account_Vendor_Name
                       ,cha.Client_Hier_Id
                       ,CASE WHEN COUNT(c.CONTRACT_ID) > 0 THEN 'Transport'
                             ELSE 'Tariff'
                        END AS tariff_Transport
                       ,ba.BUDGET_ACCOUNT_ID
                       ,ui.FIRST_NAME + '' + ui.LAST_NAME AS Analyst
                       ,cha.Alternate_Account_Number
                  FROM
                        dbo.BUDGET_ACCOUNT ba
                        INNER JOIN dbo.Budget_Account_Queue baq
                              ON ba.BUDGET_ACCOUNT_ID = baq.Budget_Account_Id
                        INNER JOIN dbo.USER_INFO ui
                              ON baq.Queue_Id = ui.QUEUE_ID
                        INNER JOIN Core.Client_Hier_Account AS cha
                              ON ba.ACCOUNT_ID = cha.Account_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.ENTITY AS Service_Level
                              ON cha.Account_Service_level_Cd = Service_Level.ENTITY_ID
                        LEFT JOIN ( Core.Client_Hier_Account AS cha1
                                    INNER JOIN dbo.CONTRACT AS c
                                          ON cha1.Supplier_Contract_ID = c.CONTRACT_ID
                                             AND c.CONTRACT_END_DATE >= GETDATE()
                                             AND cha1.Account_Type = 'Supplier'
                                    INNER JOIN dbo.ENTITY AS e
                                          ON c.CONTRACT_TYPE_ID = e.ENTITY_ID
                                             AND e.ENTITY_NAME = 'Supplier' )
                                    ON cha.Meter_Id = cha1.Meter_Id
                  WHERE
                        ( @userId = 0
                          OR ui.USER_INFO_ID = @userId )
                        AND ba.IS_DELETED = 0
                        AND cha.Account_Type = 'Utility'
                        AND ( @Client_Id IS NULL
                              OR ch.Client_Id = @Client_Id )
                        AND ( @State_Id IS NULL
                              OR ch.State_Id = @State_Id )
                        AND ( @Vendor_Id IS NULL
                              OR cha.Account_Vendor_Id = @Vendor_Id )
                        AND ( @Rate_Id IS NULL
                              OR cha.Rate_Id = @Rate_Id )
                        AND baq.Analyst_Type_Cd IN ( @Rates_Queue_Cd )
                  GROUP BY
                        cha.Account_Id
                       ,cha.Commodity_Id
                       ,Service_Level.ENTITY_NAME
                       ,cha.Account_Vendor_Id
                       ,cha.Account_Number
                       ,cha.Account_Vendor_Name
                       ,cha.Client_Hier_Id
                       ,ba.BUDGET_ACCOUNT_ID
                       ,ui.FIRST_NAME + '' + ui.LAST_NAME
                       ,cha.Alternate_Account_Number
      
      SET @SQLStatement = N'
      IF @Total_RowCount = 0 
            BEGIN

                  SELECT
                        @Total_RowCount = COUNT(1)
                  FROM
                        #Accounts cha
                        INNER JOIN Core.Client_Hier AS ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.BUDGET_ACCOUNT AS ba
                              ON cha.Budget_Account_Id = ba.Budget_Account_Id
                        INNER JOIN dbo.BUDGET AS b
                              ON ba.BUDGET_ID = b.BUDGET_ID
                                 AND cha.Commodity_Id = b.COMMODITY_TYPE_ID
                        INNER JOIN dbo.Budget_Account_Queue baq
							   ON ba.BUDGET_ACCOUNT_ID = baq.Budget_Account_Id
                         WHERE
                              (@T_Tr is null  OR cha.tariff_Transport = @T_Tr)
                              AND (@Status is null  or (@Status = ''NEW'' AND ba.Rates_Completed_Date is null) or (@Status = ''Analyst Complete'' AND ba.Rates_Completed_Date is NOT NULL))
                              AND ba.IS_DELETED = 0
                              AND b.POSTED_BY IS NULL
                              AND baq.Analyst_Type_Cd IN ( @Rates_Queue_Cd )
							  AND ( cha.tariff_transport = ''Transport''
								    OR cha.service_level_char IN ( ''A'', ''B'' ) )' + N'

            END
            ;
            WITH  CTE_ResultSet
                    AS ( SELECT TOP ( @End_Index )
                              ba.budget_id AS budgetId
                             ,ch.Client_Name AS clientName
                             ,RTRIM(ch.City) + '', '' + ch.State_Name + '' ('' + ch.Site_name + '')'' SiteName
                             ,cha.account_id AS accountId
                             ,ba.BUDGET_ACCOUNT_ID AS budgetAccountId
                             ,cha.account_number AS accountNumber
                             ,b.DUE_DATE AS dueDate
                             ,cha.Account_Vendor_Name AS utility
                             ,LEFT(Rates.Rate_Names, LEN(Rates.Rate_Names) - 1) AS rates
                             ,c.Commodity_Name AS commodity
                             ,cha.service_level_char
                             ,cha.tariff_transport
                             ,b.budget_start_year AS startYear
                             ,b.budget_start_month AS startMonth
                             ,b.posted_by AS isPosted
							 ,CASE WHEN ba.Rates_Completed_Date is null Then ''New'' ELSE ''Analyst Complete'' END Status
                             ,Record_Number = ROW_NUMBER() OVER ( ORDER BY ' + CONVERT(NVARCHAR(MAX), @SortColumn) + ' ' + CONVERT(NVARCHAR(MAX), @SortIndex) + ')
                             ,rcby.FIRST_NAME + '' '' + rcby.LAST_NAME AS Completed_By
                             ,routby.FIRST_NAME + '' '' + routby.LAST_NAME + '' ('' + CONVERT(varchar(15), baq.Routed_Ts,106) + '')'' AS Last_Routed_By
                             ,baq.Budget_Account_Queue_Id
                             ,cha.Alternate_Account_Number
                             ,cha.Analyst
                         FROM
                              #Accounts cha
                              INNER JOIN Core.Client_Hier AS ch
                                    ON cha.Client_Hier_Id = ch.Client_Hier_Id
                              INNER JOIN dbo.BUDGET_ACCOUNT AS ba
                                    ON cha.Budget_Account_Id = ba.Budget_Account_Id
                              INNER JOIN dbo.BUDGET AS b
                                    ON ba.BUDGET_ID = b.BUDGET_ID
                                       AND cha.Commodity_Id = b.COMMODITY_TYPE_ID
                              INNER JOIN dbo.Commodity AS c
                                    ON cha.Commodity_Id = c.Commodity_Id
                              CROSS APPLY ( SELECT
                                                cha2.Rate_Name + '', ''
                                            FROM
                                                Core.Client_Hier_Account AS cha2
                                            WHERE
                                                cha2.Account_Id = cha.Account_Id
                                                AND cha2.Commodity_Id = cha.Commodity_Id
                                            GROUP BY
                                                cha2.Rate_Name
                              FOR
                                            XML PATH('''') ) Rates ( Rate_Names )
                              LEFT JOIN dbo.USER_INFO rcby
									ON ba.RATES_COMPLETED_BY = rcby.USER_INFO_ID 
							  INNER JOIN dbo.Budget_Account_Queue baq
									ON ba.BUDGET_ACCOUNT_ID = baq.Budget_Account_Id
							  LEFT JOIN dbo.USER_INFO routby
									ON baq.Routed_By_User_Id = routby.USER_INFO_ID
                         WHERE
                              (@T_Tr is null  OR cha.tariff_Transport = @T_Tr)
                              AND (@Status is null  or (@Status = ''NEW'' AND ba.Rates_Completed_Date is null) or (@Status = ''Analyst Complete'' AND ba.Rates_Completed_Date is NOT NULL))
                              AND ba.IS_DELETED = 0
                              AND b.POSTED_BY IS NULL
                              AND baq.Analyst_Type_Cd IN ( @Rates_Queue_Cd )
							  AND ( cha.tariff_transport = ''Transport''
								    OR cha.service_level_char IN ( ''A'', ''B'' ) ))
                  SELECT
                        budgetId
                       ,clientName
                       ,SiteName
                       ,accountId
                       ,budgetAccountId
                       ,accountNumber
                       ,dueDate
                       ,utility
                       ,rates
                       ,commodity
                       ,service_level_char
                       ,tariff_transport
                       ,startYear
                       ,startMonth
                       ,isPosted
                       ,Record_Number
                       ,Status
                       ,@Total_RowCount AS Total_RowCount
                       ,Completed_By
                       ,Last_Routed_By 
                       ,Budget_Account_Queue_Id
                       ,Analyst
                       ,Alternate_Account_Number
                  FROM
                        CTE_ResultSet
                  WHERE
                        Record_Number BETWEEN @Start_Index AND @End_Index'
            
      SELECT
            @ParamList = N'
            @userId INT
           ,@Is_History BIT
           ,@Start_Index INT
           ,@End_Index INT
			,@Rates_Queue_Cd INT
           ,@Both_Queue_Cd INT
           ,@Total_RowCount INT
            ,@T_Tr VARCHAR(15)
           ,@Status VARCHAR(25)'
           
      EXEC sys.sp_executesql 
            @SQLStatement
           ,@ParamList
           ,@userId
           ,@Is_History
           ,@Start_Index
           ,@End_Index
           ,@Rates_Queue_Cd
           ,@Both_Queue_Cd
           ,@Total_RowCount
           ,@T_Tr
           ,@Status

      IF OBJECT_ID('tempdb..#Accounts') IS NOT NULL 
            DROP TABLE #Accounts 

END;
;







;
GO





GRANT EXECUTE ON  [dbo].[BUDGET_GET_BUDGET_RESULTS_P] TO [CBMSApplication]
GO
