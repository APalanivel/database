SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******************************************************************************************************                        
NAME: dbo.Invoice_Submission_Log_Ins
    
DESCRIPTION:    
    
      Insert records into Invoice_Submission_Log
    
INPUT PARAMETERS:    
      NAME                                      DATATYPE    DEFAULT           DESCRIPTION    
------------------------------------------------------------------------    

      
OUTPUT PARAMETERS:              
      NAME              DATATYPE    DEFAULT           DESCRIPTION 
-----------------------------------------------------------      



------------------------------------------------------------              
USAGE EXAMPLES:              
------------------------------------------------------------            

	Exec Invoice_Submission_Log_Ins
         
AUTHOR INITIALS:              
      INITIALS    NAME              
------------------------------------------------------------              
		HK			Harish Kurma
		      
MODIFICATIONS:    
      INITIALS    DATE			MODIFICATION              
------------------------------------------------------------              
      HK		  10 29 2018	Created
******************************************************************************************************/

CREATE PROCEDURE [dbo].[Invoice_Submission_Log_Ins]
(
    @User_Info_Id INT,
    @Client_Id INT,
    @External_File_name NVARCHAR(255),
    @Invoice_Submission_Log_Id INT OUT
)
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @Status_Cd INT,
            @Invoice_Submission_Client_Location_Id INT;

    SELECT @Status_Cd = cd.Code_Id
    FROM dbo.Code cd
        JOIN dbo.Codeset cs
            ON cd.Codeset_Id = cs.Codeset_Id
    WHERE cs.Codeset_Name = 'ProjectStatus'
          AND cd.Code_Dsc = 'In Progress';

    SELECT @Invoice_Submission_Client_Location_Id = isc.Invoice_Submission_Client_Location_Id
    FROM dbo.Invoice_Submission_Client_Location isc
    WHERE isc.Client_Id = @Client_Id;

    INSERT INTO dbo.Invoice_Submission_Log
    (
        User_Info_Id,
        External_File_Name,
        Status_Cd,
        Invoice_Submission_Client_Location_Id,
        Created_User_Id,
        Created_Ts,
        Updated_User_Id,
        Updated_Ts
    )
    VALUES
    (@User_Info_Id, @External_File_name, @Status_Cd, @Invoice_Submission_Client_Location_Id, @User_Info_Id, GETDATE(),
     @User_Info_Id, GETDATE());

    SELECT @Invoice_Submission_Log_Id = SCOPE_IDENTITY();

END;




GO
GRANT EXECUTE ON  [dbo].[Invoice_Submission_Log_Ins] TO [CBMSApplication]
GO
