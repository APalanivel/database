
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [DBO].[Division_History_Del_By_Division_Id]

DESCRIPTION:

	To Delete Division History associated with the given Division Id.

INPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
	@Division_Id	INT		
	@User_Info_Id	INT				

OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
	SELECT * FROM dbo.Division WHERE Division_Id = 2796
	BEGIN TRAN
		EXEC Division_History_Del_By_Division_Id  2796,49
		SELECT * FROM dbo.Division WHERE Division_Id = 2796
		--SELECT count(1) FROM Core.Client_Commodity_Detail WHERE	Sitegroup_Id = 2796
	ROLLBACK TRAN
	

AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH
	HG			Harihara Suthan G
	RR			Raghu Reddy
	CPE			Chaitanya Panduga Eshwar
	NR			Narayana reddy

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			07/21/2010	Created
	HG			9/27/2010	Script added to remove the entries from User_DIvision_Map table.
	PNR			24/11/2010	Removed Transaction part as part of Project : Security Roles Administration.
	RR			2011-12-13	Removed the part of script used to delete the division client attribute entries from tracking table as the client attribute
							tables moved to DVDEHub
	RR			2012-02-08	Added script to delete the GHG history, removed the old script used to delete data only from Core.Client_Commodity_Detail
							table, but there are two more tables referring this table su.Client_Commodity_Detail_Emission_Factor,su.GHG_Tracking. New
							script will delete from all the three tables in order
	CPE			2012-04-06	Removed the call to Site_Group_GHG_History_Del_By_Site_Group_Id SP as core.Client_Commodity_Detail table is moved to Hub
	NR			2015-11-30	MAINT-3681 To removed the USER_DIVISION_MAP table reference.
*/
CREATE PROCEDURE [dbo].[Division_History_Del_By_Division_Id]
      ( 
       @Division_Id INT
      ,@User_Info_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;

      DECLARE
            @Client_Name VARCHAR(200)
           ,@User_Name VARCHAR(81)
           ,@Application_Name VARCHAR(30) = 'Delete Division'
           ,@Audit_Function SMALLINT = -1
           ,@Current_Ts DATETIME
           ,@Lookup_Value XML
           ,@Sitegroup_Id INT
           ,@Vendor_Id INT
           ,@Rm_Batch_Configuration_Details_Id INT
           ,@User_Id INT
           ,@Savings_Category_Type_Id INT
           ,@Inactive_User_Info_Id INT

      DECLARE @Prefer_Not_ToDo_Business_List TABLE
            ( 
             Vendor_Id INT PRIMARY KEY CLUSTERED )
      DECLARE @Rm_Batch_Configuration_Details_List TABLE
            ( 
             Rm_Batch_Configuration_Details_Id INT PRIMARY KEY CLUSTERED )
      DECLARE @User_Info_List TABLE
            ( 
             User_Info_Id INT PRIMARY KEY CLUSTERED )
      DECLARE @Savings_Category_Division_Map_List TABLE
            ( 
             Savings_Category_Type_id INT PRIMARY KEY CLUSTERED )

      SET @Lookup_Value = ( SELECT
                              Sitegroup_Name
                             ,Sitegroup_Id
                            FROM
                              Sitegroup
                            WHERE
                              Sitegroup_Id = @Division_Id
            FOR
                            XML AUTO )


      SELECT
            @Client_Name = cl.Client_Name
      FROM
            dbo.Division_Dtl dd
            JOIN dbo.Client cl
                  ON cl.CLIENT_ID = dd.CLIENT_ID
      WHERE
            dd.SiteGroup_Id = @Division_Id

      SELECT
            @User_Name = FIRST_NAME + SPACE(1) + LAST_NAME
      FROM
            dbo.USER_INFO
      WHERE
            USER_INFO_ID = @User_Info_Id
		


      INSERT      INTO @User_Info_List
                  ( 
                   User_Info_Id )
                  SELECT
                        User_Info_Id
                  FROM
                        dbo.User_Info
                  WHERE
                        DIVISION_Id = @Division_Id

	
      INSERT      INTO @Prefer_Not_ToDo_Business_List
                  ( 
                   Vendor_Id )
                  SELECT
                        Vendor_Id
                  FROM
                        dbo.PREFER_NOT_TODO_BUSINESS
                  WHERE
                        Division_Id = @Division_Id

      INSERT      INTO @Rm_Batch_Configuration_Details_List
                  ( 
                   Rm_Batch_Configuration_Details_Id )
                  SELECT
                        Rm_Batch_Configuration_Details_Id
                  FROM
                        dbo.Rm_Batch_Configuration_Details
                  WHERE
                        Division_Id = @Division_Id

	
      INSERT      INTO @Savings_Category_Division_Map_List
                  ( 
                   Savings_Category_Type_Id )
                  SELECT
                        SAVINGS_CATEGORY_TYPE_ID
                  FROM
                        dbo.SAVINGS_CATEGORY_DIVISION_MAP
                  WHERE
                        DIVISION_ID = @Division_Id

      BEGIN TRY

            WHILE EXISTS ( SELECT
                              1
                           FROM
                              @Prefer_Not_ToDo_Business_List ) 
                  BEGIN

                        SET @Vendor_Id = ( SELECT TOP 1
                                                Vendor_Id
                                           FROM
                                                @Prefer_Not_ToDo_Business_List )

                        EXEC dbo.Prefer_Not_ToDo_Business_Del 
                              @Vendor_Id
                             ,@Division_Id

                        DELETE
                              @Prefer_Not_ToDo_Business_List
                        WHERE
                              Vendor_Id = @Vendor_Id

                  END

            WHILE EXISTS ( SELECT
                              1
                           FROM
                              @Savings_Category_Division_Map_List ) 
                  BEGIN

                        SET @Savings_Category_Type_Id = ( SELECT TOP 1
                                                            Savings_Category_Type_id
                                                          FROM
                                                            @Savings_Category_Division_Map_List )

                        EXEC dbo.Savings_Category_Division_Map_Del 
                              @Savings_Category_Type_Id
                             ,@Division_Id

                        DELETE
                              @Savings_Category_Division_Map_List
                        WHERE
                              Savings_Category_Type_id = @Savings_Category_Type_Id

                  END

            WHILE EXISTS ( SELECT
                              1
                           FROM
                              @Rm_Batch_Configuration_Details_List ) 
                  BEGIN

                        SET @Rm_Batch_Configuration_Details_Id = ( SELECT TOP 1
                                                                        Rm_Batch_Configuration_Details_Id
                                                                   FROM
                                                                        @Rm_Batch_Configuration_Details_List )

                        EXEC dbo.Rm_Batch_Configuration_Details_Del 
                              @Rm_Batch_Configuration_Details_Id

                        DELETE
                              @Rm_Batch_Configuration_Details_List
                        WHERE
                              Rm_Batch_Configuration_Details_Id = @Rm_Batch_Configuration_Details_Id
                  END

            WHILE EXISTS ( SELECT
                              1
                           FROM
                              @User_Info_List ) 
                  BEGIN

                        SET @User_Id = ( SELECT TOP 1
                                          User_Info_Id
                                         FROM
                                          @User_Info_List )

                        EXEC dbo.User_Info_Upd_Division_Id_Null_By_User_Info_Id 
                              @User_Id
					
                        DELETE
                              @User_Info_List
                        WHERE
                              User_Info_Id = @User_Id
	
                  END

			
            EXEC dbo.Division_Dtl_Del 
                  @Division_Id

            SET @Current_Ts = GETDATE()
            EXEC dbo.Application_Audit_Log_Ins 
                  @Client_Name
                 ,@Application_Name
                 ,@Audit_Function
                 ,'DIVISION_DTL'
                 ,@Lookup_Value
                 ,@User_Name
                 ,@Current_Ts

      END TRY
      BEGIN CATCH

            EXEC usp_RethrowError

      END CATCH 				
END;

;
GO



GRANT EXECUTE ON  [dbo].[Division_History_Del_By_Division_Id] TO [CBMSApplication]
GO
