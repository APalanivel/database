SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.BUDGET_UPDATE_BUDGET_DETAILS_P2
	@user_id varchar(10),
	@session_id varchar(20),
	@budgetAccountId int,	
	@serviceMonth datetime,
	@forecastPrice decimal(32,16),
	@budgetUsage decimal(32,16)
	
	

	AS
	begin
		set nocount on
	
		if exists (select budget_detail_id from  budget_details 
			where budget_account_id = @budgetAccountId and month_identifier = @serviceMonth)
		begin
			update 	budget_details 
		        set 	budget_usage = @budgetUsage,
				nymex_forecast = @forecastPrice
			where 	budget_details.budget_account_id = @budgetAccountId
				and budget_details.month_identifier = @serviceMonth
		end
		else
		begin
			
			insert into budget_details(budget_account_id,month_identifier, budget_usage, nymex_forecast) 
			values(@budgetAccountId,@serviceMonth, @budgetUsage, @forecastPrice)
		end
	
	end


GO
GRANT EXECUTE ON  [dbo].[BUDGET_UPDATE_BUDGET_DETAILS_P2] TO [CBMSApplication]
GO
