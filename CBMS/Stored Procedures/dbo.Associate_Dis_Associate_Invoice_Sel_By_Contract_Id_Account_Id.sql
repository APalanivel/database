SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.Associate_Dis_Associate_Invoice_Sel_By_Contract_Id

DESCRIPTION:

INPUT PARAMETERS:
	Name					DataType		Default	Description
---------------------------------------------------------------
	@Contract_Id			INT

OUTPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	
USAGE EXAMPLES:
------------------------------------------------------------

	

EXEC Associate_Dis_Associate_Invoice_Sel_By_Contract_Id_Account_Id
    @Contract_Id = 167450
    , @Account_Id = 1685738
    , @Is_Associated_Invoice = 0
    , @Is_Dis_Associated_Invoice = 1

EXEC Associate_Dis_Associate_Invoice_Sel_By_Contract_Id_Account_Id
    @Account_Id = null
    , @Contract_id = 159815
    , @Is_Associated_Invoice = 1
    , @Is_Dis_Associated_Invoice = 0

	EXEC Associate_Dis_Associate_Invoice_Sel_By_Contract_Id_Account_Id
    @Account_Id = null
    , @Contract_id = 159815
    , @Is_Associated_Invoice = 0
    , @Is_Dis_Associated_Invoice = 1

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	NR			Narayaana Reddy
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	NR       	2019-07-18	Created for Add - contract
	NR			2020-01-29	MAINT-9782- Added IS_PROCESSED flag.
	NR		   2020-04-29		MAINT-10157 - Removed contract filter on map table for un associated list to if invoice is mapped any other contract.


******/
CREATE PROCEDURE [dbo].[Associate_Dis_Associate_Invoice_Sel_By_Contract_Id_Account_Id]
    (
        @Contract_Id INT
        , @Account_Id INT = NULL
        , @Is_Associated_Invoice BIT = 0
        , @Is_Dis_Associated_Invoice BIT = 0
    )
AS
    BEGIN

        SET NOCOUNT ON;



        SELECT
            cism.CU_INVOICE_ID
            , cism.Account_ID
            , ci.CBMS_IMAGE_ID
            , cism.Begin_Dt
            , cism.End_Dt
            , ci.IS_PROCESSED
        FROM
            Core.Client_Hier_Account Sup_Cha
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.Account_ID = Sup_Cha.Account_Id
            INNER JOIN dbo.CU_INVOICE ci
                ON ci.CU_INVOICE_ID = cism.CU_INVOICE_ID
        WHERE
            Sup_Cha.Supplier_Contract_ID = @Contract_Id
            AND (   @Account_Id IS NULL
                    OR  Sup_Cha.Account_Id = @Account_Id)
            AND (   cism.Begin_Dt BETWEEN Sup_Cha.Supplier_Account_begin_Dt
                                  AND     Sup_Cha.Supplier_Account_End_Dt
                    OR  cism.End_Dt BETWEEN Sup_Cha.Supplier_Account_begin_Dt
                                    AND     Sup_Cha.Supplier_Account_End_Dt
                    OR  Sup_Cha.Supplier_Account_begin_Dt BETWEEN cism.Begin_Dt
                                                          AND     cism.End_Dt
                    OR  Sup_Cha.Supplier_Account_End_Dt BETWEEN cism.Begin_Dt
                                                        AND     cism.End_Dt)
            AND (   (   @Is_Associated_Invoice = 1
                        AND @Is_Dis_Associated_Invoice = 0
                        AND EXISTS (   SELECT
                                            1
                                       FROM
                                            dbo.Contract_Cu_Invoice_Map ccim
                                       WHERE
                                            ccim.Cu_Invoice_Id = cism.CU_INVOICE_ID
                                            AND ccim.Contract_Id = @Contract_Id
                                            AND (   @Account_Id IS NULL
                                                    OR  ccim.Account_Id = @Account_Id)))
                    OR  (   @Is_Associated_Invoice = 0
                            AND @Is_Dis_Associated_Invoice = 1
                            AND NOT EXISTS (   SELECT
                                                    1
                                               FROM
                                                    dbo.Contract_Cu_Invoice_Map ccim
                                               WHERE
                                                    ccim.Cu_Invoice_Id = cism.CU_INVOICE_ID
                                                    AND ccim.Account_Id = cism.Account_ID
                                                    AND (   @Account_Id IS NULL
                                                            OR  ccim.Account_Id = @Account_Id))))
        GROUP BY
            cism.CU_INVOICE_ID
            , cism.Account_ID
            , ci.CBMS_IMAGE_ID
            , cism.Begin_Dt
            , cism.End_Dt
            , ci.IS_PROCESSED;
    END;












GO
GRANT EXECUTE ON  [dbo].[Associate_Dis_Associate_Invoice_Sel_By_Contract_Id_Account_Id] TO [CBMSApplication]
GO
