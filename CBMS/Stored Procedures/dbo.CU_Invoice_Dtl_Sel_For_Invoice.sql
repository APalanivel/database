
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:

cbms_prod.dbo.CU_Invoice_Dtl_Sel_For_Invoice

DESCRIPTION:

This objects is used only for Alt Services until EP/NG are migrated to Cost_Usage_Account_Dtl

INPUT PARAMETERS:
Name			DataType		Default	Description
------------------------------------------------------------
@cu_invoice_id 	int       	          	

OUTPUT PARAMETERS:
Name			DataType		Default	Description
------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
	EXEC CU_Invoice_Dtl_Sel_For_Invoice 3722763
	EXEC CU_Invoice_Dtl_Sel_For_Invoice 3721723
	EXEC CU_Invoice_Dtl_Sel_For_Invoice 3721796

AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------
SSR			Sharad Srivastava

MODIFICATIONS

Initials	Date		Modification
------------------------------------------------------------
SSR	       	04/09/2010	Created
HG			04/14/2010	Commodity_id column removed from SELECT clause as it not required to show in application.
HG			04/16/2010	UNION query replaced by subselect with union on Account/Supplier_Account_Meter_Map to eliminate the base table join twice.
SKA			04/28/2010	Removed Cu_invoice(Account_id) with cu_invoice_service_month(Account_id)
SSR			05/03/2010	Added Commodity_type_id in select clause 
SSR			05/05/2010  Added Account_number in select Clause
SKA			05/21/2010	Account Number to be Concatenate with supplier begin and End date for supplier Account
SKA			05/27/2010	Added the description.
DMR		    09/10/2010  Modified for Quoted_Identifier
RK          04/02/2012  Modified with ADDLT changes.
                        Removed Base table references Account,Meter,RATE,Entity and added tables Client_Hier and Client_Hier_Account tables. 
                        Added Client_Hier_Id column in the return result set.
******/
CREATE PROCEDURE dbo.CU_Invoice_Dtl_Sel_For_Invoice ( @cu_invoice_id INT )
AS 
BEGIN

      SET NOCOUNT ON ;

      SELECT
            i.cu_invoice_id
           ,sm.service_month
           ,CH.SITE_ID
           ,CH.Client_Hier_Id
           ,rtrim(CH.city) + ', ' + CH.state_name + ' (' + CH.site_name + ')' Site_Name
           ,cuad.ACCOUNT_ID
           ,Cha.Commodity_Id AS COMMODITY_TYPE_ID
           ,cha.Display_Account_Number ACCOUNT_NUMBER
      FROM
            dbo.cu_invoice i
            JOIN dbo.cu_invoice_service_month sm
                  ON sm.cu_invoice_id = i.cu_invoice_id
            JOIN dbo.Cost_Usage_Account_Dtl cuad
                  ON cuad.ACCOUNT_ID = sm.ACCOUNT_ID
                     AND cuad.Service_Month = sm.SERVICE_MONTH
            JOIN Core.Client_Hier_Account Cha
                  ON cuad.Account_ID = Cha.Account_Id
                     AND cuad.Client_Hier_Id = cha.Client_Hier_Id
            JOIN Core.Client_Hier CH
                  ON Cha.Client_Hier_Id = CH.Client_Hier_Id
            JOIN dbo.Bucket_Master bm
                  ON bm.Bucket_Master_Id = cuad.Bucket_Master_Id
      WHERE
            i.cu_invoice_id = @cu_invoice_id
      GROUP BY
            i.cu_invoice_id
           ,sm.service_month
           ,CH.SITE_ID
           ,CH.Client_Hier_Id
           ,CH.city
           ,CH.state_name
           ,CH.site_name
           ,cuad.ACCOUNT_ID
           ,Cha.COMMODITY_ID
           ,cha.Display_Account_Number


END
;
GO

GRANT EXECUTE ON  [dbo].[CU_Invoice_Dtl_Sel_For_Invoice] TO [CBMSApplication]
GO
