SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[SR_RFP_BID_Group_Del]  
     
DESCRIPTION: 
	It Deletes SR RFP BID GROUP for Selected 
						SR RFP BID Group Id.
      
INPUT PARAMETERS:          
NAME							DATATYPE	DEFAULT		DESCRIPTION          
-------------------------------------------------------------------
@SR_RFP_BID_Group_Id			INT

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	Begin Tran

		EXEC SR_RFP_BID_Group_Del  10001133

	Rollback Tran
    
    SELECT * FROM SR_RFP_BID_GROUP a WHERE NOT EXISTS (SELECT 1 FROM SR_RFP_ACCOUNT b WHERE a.SR_RFP_BID_GROUP_ID = b.SR_RFP_BID_GROUP_ID)

    
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR			PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR		    28-MAY-10	CREATED     

*/  

CREATE PROCEDURE dbo.SR_RFP_BID_Group_Del
   (
    @SR_RFP_BID_Group_Id INT
   )
AS 
BEGIN

    SET NOCOUNT ON;
   
    DELETE 
   	FROM	
		dbo.SR_RFP_BID_GROUP
	WHERE 
		SR_RFP_BID_GROUP_ID = @SR_RFP_BID_Group_Id

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_BID_Group_Del] TO [CBMSApplication]
GO
