SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
     [dbo].[Cost_Usage_Account_Dtl_Del_By_Meter_Id]   
  
DESCRIPTION:  
	
	WHen the last meter of the account removed relevant cost usage data also should be deleted, this SP does that.
        
INPUT PARAMETERS:  
Name            DataType          Default     Description  
-----------------------------------------------------------  
@Meter_Id		INT
@Commodity_id	INT		
     
OUTPUT PARAMETERS:  
Name			DataType          Default     Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------      

BEGIN TRAN

	EXEC dbo.Cost_Usage_Account_Dtl_Del_By_Meter_Id 25704,291 --13649

	SELECT cha.account_id,cha.meter_id,cu.SERVICE_MONTH,Bucket_Value
	FROM dbo.Cost_Usage_Account_Dtl cu JOIN Core.Client_Hier_Account cha ON cha.Account_Id = cu.ACCOUNT_ID AND cha.Account_Type = 'Utility'
		WHERE  cha.Meter_Id = 25704 --13649

ROLLBACK TRAN

        
AUTHOR INITIALS:  
 Initials		Name   
------------------------------------------------------------  
 HG			    Harihara Suthan G
                
MODIFICATIONS:  
 Initials   Date	    Modification  
------------------------------------------------------------  
 HG			2012-20-06	Created for ADLDT requirement
******/
CREATE PROCEDURE dbo.Cost_Usage_Account_Dtl_Del_By_Meter_Id
      @Meter_Id INT
     ,@Commodity_id INT
AS 
BEGIN    
      SET NOCOUNT ON ;
	  
      DECLARE @Account_Id INT

      SELECT
            @Account_Id = cuad.Account_Id
      FROM
            dbo.Cost_Usage_Account_Dtl cuad
            JOIN Core.Client_Hier_Account cha
                  ON cha.Account_Id = cuad.ACCOUNT_ID
                     AND cha.Account_Type = 'Utility'
            JOIN dbo.Bucket_Master bm
                  ON bm.Bucket_Master_Id = cuad.Bucket_Master_Id
      WHERE
            bm.Commodity_Id = @Commodity_id
            AND cha.Meter_Id = @Meter_Id
      GROUP BY
            cuad.Account_Id
			
      EXEC dbo.Cost_Usage_History_Del_For_Account 
            @Account_Id			

END
;
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Dtl_Del_By_Meter_Id] TO [CBMSApplication]
GO
