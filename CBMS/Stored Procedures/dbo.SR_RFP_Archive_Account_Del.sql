SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [DBO].[SR_RFP_Archive_Account_Del]  
     
DESCRIPTION: 
	It Deletes SR RFP Archive Account for Selected Sr RFP Archive Account Id.
      
INPUT PARAMETERS:          
NAME						DATATYPE	DEFAULT		DESCRIPTION          
-------------------------------------------------------------------          
@SR_RFP_Archive_Account_Id	INT	

   
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:
------------------------------------------------------------
  
	Begin Tran
		EXEC SR_RFP_Archive_Account_Del  42
	Rollback Tran

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			27-MAY-10	CREATED

*/

CREATE PROCEDURE dbo.SR_RFP_Archive_Account_Del
   (
    @SR_RFP_Archive_Account_Id INT
   )
AS
BEGIN

    SET NOCOUNT ON;

    DELETE
   	FROM
		dbo.SR_RFP_ARCHIVE_ACCOUNT
	WHERE
		SR_RFP_ARCHIVE_ACCOUNT_ID = @SR_RFP_Archive_Account_Id

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_Archive_Account_Del] TO [CBMSApplication]
GO
