SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map_Merge         
                
Description:                
   This sproc is to map the Frequency With the Cu_Invoice .        
                             
 Input Parameters:                
    Name          DataType   Default   Description                  
-------------------------------------------------------------------------------------------------                  
@Cu_Invoice_Id     INT  
@Account_Invoice_Collection_Month_Id      INT      NULL  
   
 Output Parameters:                      
    Name        DataType   Default   Description                  
--------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
--------------------------------------------------------------------------------------     
  
   BEGIN Transaction  
   Exec dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map_Merge 114,1  
   SELECT * FROM Account_Invoice_Collection_Month_Cu_Invoice_Map where cu_invoice_Id = 114  
  
   Rollback Transaction  
    
     
Author Initials:                
    Initials  Name                
--------------------------------------------------------------------------------------                  
 RKV    Ravi Kumar Vegesna  
 Modifications:                
    Initials        Date   Modification                
--------------------------------------------------------------------------------------                  
    RKV    2017-01-25  Created For Invoice_Collection.           
               
******/   
  
CREATE PROCEDURE [dbo].[Account_Invoice_Collection_Month_Cu_Invoice_Map_Merge]
      ( 
       @tvp_Cu_Invoice_Id_Invoice_Collection_Month tvp_Cu_Invoice_Id_Invoice_Collection_Month READONLY )
AS 
BEGIN  
  
      SET NOCOUNT ON;  
  
        
      INSERT      INTO dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map
                  ( 
                   Account_Invoice_Collection_Month_Id
                  ,Cu_Invoice_Id
                  ,Invoice_Begin_Dt
                  ,Invoice_End_Dt
                  ,Created_Ts )
                  SELECT
                        cicm.Account_Invoice_Collection_Month_Id
                       ,cicm.Cu_Invoice_Id
                       ,cicm.Invoice_Begin_Dt
                       ,cicm.Invoice_End_Dt
                       ,GETDATE()
                  FROM
                        @tvp_Cu_Invoice_Id_Invoice_Collection_Month cicm
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map aicm
                                     WHERE
                                          cicm.Cu_Invoice_Id = aicm.Cu_Invoice_Id
                                          AND cicm.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id );  
        
        
      DELETE
            aicm
      FROM
            dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map aicm
            INNER JOIN @tvp_Cu_Invoice_Id_Invoice_Collection_Month cicm
                  ON cicm.Cu_Invoice_Id = aicm.Cu_Invoice_Id
      WHERE
            NOT EXISTS ( SELECT
                              1
                         FROM
                              @tvp_Cu_Invoice_Id_Invoice_Collection_Month cicm1
                         WHERE
                              cicm1.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id );  
        
     
  
END;  

;
GO
GRANT EXECUTE ON  [dbo].[Account_Invoice_Collection_Month_Cu_Invoice_Map_Merge] TO [CBMSApplication]
GO
