SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

















/******
NAME: [dbo].[Get_Analagous_Accounts_Account_Id_Commodity_Base]

DESCRIPTION:

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	AP	Arunkumar Palanivel

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	AP			Jan 23,2020
	THis stored Procedure is created to get the list of all analogous account

******/
CREATE PROCEDURE [dbo].[Get_Analagous_Accounts_Account_Id_Commodity_Base_Category]
      (
      @Account_id                    INT
    , @commodity_id                  INT
    , @Previous_Month                DATE
    , @Current_Service_Month         DATE
    , @Second_Previous_Month         DATE
    , @Bucket_master_id              INT
    , @Min_Account                   INT
    , @Max_Account                   INT
    , @Analogous_Distance            INT
    , @Variance_Consumption_Level_id INT
    , @Linear_Regression_Sigma_value VARCHAR(10)
    , @error_metric                  VARCHAR(30))
AS
      BEGIN

            SET NOCOUNT ON;
            CREATE TABLE #Bucket_Values -- This table will use to store the Final Value
                  ( Source_Account_Id             INT
                  , Service_Month                 DATE
                  , Billing_Start_Date            DATE
                  , Billing_End_Date              DATE
                  , BUcket_Master_id              INT
                  , Bucket_Name                   VARCHAR(255)
                  , Bucket_Type                   VARCHAR(25)
                  , Bucket_Value                  DECIMAL(28, 10)
                  , uom_cd                        INT
                  , uom_Name                      VARCHAR(25)
                  , Target_Account_ID             INT
                  , Target_Account_Client_Hier_Id INT
                  , Service_Type                  VARCHAR(25)
                  , Rate_Id                       INT
                  , ThresholdSigma                VARCHAR(10)
                  , [Test Calculation Type]       VARCHAR(30)
                  , category_Name                 VARCHAR(20)
                  , row_num                       INT
                  , BillingDays                   INT );


            CREATE TABLE #Account_Details
                  ( Account_id INT );

            CREATE CLUSTERED INDEX IX_Account_id
            ON #Account_Details ( Account_id );
            CREATE TABLE #Account
                  ( Account_id     INT
                  , Client_Hier_id INT
                  , PRIMARY KEY CLUSTERED (
                          Account_id
                        , Client_Hier_id ));
            DECLARE
                  @Rate_id           INT
                , @Lat               DECIMAL(32, 16)
                , @long              DECIMAL(32, 16)
                , @Site_Type_Name    VARCHAR(100)
                , @count             INT
                , @CBMS_Code_id      INT
                , @Invoice_Code_Id   INT
                , @Estimated_code_Id INT
                , @country_id        INT
                , @Region_id         INT
                , @vendor_id         INT
                , @currency_unit_id  INT,
				@Category_name varchar(20);

			SELECT  @Category_name = c.Code_Value FROM dbo.Variance_category_Bucket_Map vcbm
			JOIN dbo.code c
			ON vcbm.Variance_Category_Cd =  c.Code_Id
			WHERE vcbm.Bucket_master_Id = @Bucket_master_id



            SELECT
                  @currency_unit_id = cucm.CURRENCY_UNIT_ID
            FROM  dbo.CURRENCY_UNIT_COUNTRY_MAP cucm
                  JOIN
                  dbo.CURRENCY_UNIT cu
                        ON cu.CURRENCY_UNIT_ID = cucm.CURRENCY_UNIT_ID
                  INNER JOIN
                  Core.Client_Hier ch
                        ON ch.Country_Id = cucm.COUNTRY_ID
                  JOIN
                  Core.Client_Hier_Account cha
                        ON cha.Client_Hier_Id = ch.Client_Hier_Id
            WHERE cha.Account_Id = @Account_id;

            SET @currency_unit_id = isnull(@currency_unit_id, 3);





            SELECT
                  @CBMS_Code_id = c.Code_Id
            FROM  dbo.Code c
                  JOIN
                  dbo.Codeset cs
                        ON cs.Codeset_Id = c.Codeset_Id
            WHERE c.Code_Dsc = 'Data manually entered from CBMS'
                  AND   cs.Codeset_Name = 'DataSource';

            SELECT
                  @Invoice_Code_Id = c.Code_Id
            FROM  dbo.Code c
                  JOIN
                  dbo.Codeset cs
                        ON cs.Codeset_Id = c.Codeset_Id
            WHERE cs.Codeset_Name = 'DataSource'
                  AND   c.Code_Value = 'Invoice';

            SELECT
                  @Estimated_code_Id = c.Code_Id
            FROM  dbo.Code c
                  JOIN
                  dbo.Codeset cs
                        ON cs.Codeset_Id = c.Codeset_Id
            WHERE c.Code_Value = 'estimated'
                  AND   cs.Codeset_Name = 'Data_Type';

            SELECT
                  @Rate_id = cha.Rate_Id
                , @Lat = ch.Geo_Lat
                , @long = ch.Geo_Long
                , @Site_Type_Name = ch.Site_Type_Name_FTSearch
                , @country_id = ch.Country_Id
                , @Region_id = ch.Region_ID
                , @vendor_id = cha.Account_Vendor_Id
            FROM  Core.Client_Hier_Account cha
                  JOIN
                  Core.Client_Hier ch
                        ON ch.Client_Hier_Id = cha.Client_Hier_Id
            WHERE cha.Account_Id = @Account_id
                  AND   cha.Commodity_Id = @commodity_id;




            SET @Min_Account = isnull(@Min_Account, 0);
            SET @Max_Account = isnull(@Max_Account, 0);

            --PRINT @Rate_id;
            --PRINT @Site_Type_Name;
            --PRINT @Lat;
            --PRINT @long;
            --PRINT @Variance_Consumption_Level_id;
            --PRINT @Previous_Month;
            --PRINT @Second_Previous_Month;
            --PRINT @Current_Service_Month;
            --PRINT @Bucket_master_id;

            --   PRINT 'STEP 1: CHECKING FOR AN ACTUAL MATCH, DO WE HAVE CURRENT AND PREVIOUS MONTH DATA FOR THE ACCOUNTS'

            INSERT INTO #Account_Details
                        SELECT      TOP ( @Min_Account )
                                    cha.Account_Id
                        FROM        CBMS.Core.Client_Hier_Account cha
                                    JOIN
                                    Core.Client_Hier ch
                                          ON ch.Client_Hier_Id = cha.Client_Hier_Id
                        WHERE       power(ch.Geo_Lat - @Lat, 2) + power(( ch.Geo_Long - ( @long )) * cos(radians(@Lat)), 2) < power(@Analogous_Distance / 110.25, 2)
                                    AND   cha.Rate_Id = @Rate_id
                                    AND   ch.Site_Type_Name = @Site_Type_Name
                                    AND   cha.Account_Id <> @Account_id
                                    AND   cha.Account_Type = 'utility'
                                    AND   EXISTS
                              (     SELECT
                                          1
                                    FROM  CBMS.dbo.Account_Variance_Consumption_Level avcl
                                    WHERE avcl.ACCOUNT_ID = cha.Account_Id
                                          AND   avcl.Variance_Consumption_Level_Id = @Variance_Consumption_Level_id )
                                    AND   EXISTS
                              (     SELECT
                                          1
                                    FROM  dbo.Cost_Usage_Account_Dtl cuad
                                    WHERE cuad.ACCOUNT_ID = cha.Account_Id
                                          AND   cuad.Client_Hier_ID = cha.Client_Hier_Id
                                          AND   cuad.Bucket_Master_Id = @Bucket_master_id
                                          AND   cuad.Service_Month = @Current_Service_Month
                                          AND   cuad.Data_Source_Cd = @Invoice_Code_Id
                                          AND   cuad.Data_Type_Cd <> @Estimated_code_Id
                                          AND
                                                (     (     @Category_name IN (
                                                                  'Demand'
                                                                , 'Billed Demand')
                                                            AND   cuad.Bucket_Value > 0 )
                                                      OR
                                                            (     @Category_name =   'Usage' 
                                                                  AND   cuad.Bucket_Value >= 0 )
																  OR
                                                            (     @Category_name ='Cost'
                                                                      )
																	  )
                                          AND   NOT EXISTS
                                          (     SELECT
                                                      1
                                                FROM  dbo.Variance_Account_Month_Anomalous_Status vra
                                                WHERE vra.Account_Id = cuad.ACCOUNT_ID
                                                      AND   vra.Service_Month = @Current_Service_Month
                                                      AND   vra.Is_Anomalous = 1 ))
                                    AND   EXISTS
                              (     SELECT
                                          1
                                    FROM  dbo.Cost_Usage_Account_Dtl cuad
                                    WHERE cuad.ACCOUNT_ID = cha.Account_Id
                                          AND   cuad.Client_Hier_ID = cha.Client_Hier_Id
                                          AND   cuad.Bucket_Master_Id = @Bucket_master_id
                                          AND   cuad.Service_Month = @Previous_Month
                                          AND   cuad.Data_Source_Cd = @Invoice_Code_Id
                                          AND   cuad.Data_Type_Cd <> @Estimated_code_Id
                                         AND
                                                (     (     @Category_name IN (
                                                                  'Demand'
                                                                , 'Billed Demand')
                                                            AND   cuad.Bucket_Value > 0 )
                                                      OR
                                                            (     @Category_name =   'Usage' 
                                                                  AND   cuad.Bucket_Value >= 0 )
																  OR
                                                            (     @Category_name ='Cost'
                                                                      )
																	  )
                                          AND   NOT EXISTS
                                          (     SELECT
                                                      1
                                                FROM  dbo.Variance_Account_Month_Anomalous_Status vra
                                                WHERE vra.Account_Id = cuad.ACCOUNT_ID
                                                      AND   vra.Service_Month = @Previous_Month
                                                      AND   vra.Is_Anomalous = 1 ))
                        GROUP BY    cha.Account_Id;




            SET @count =
                  (     SELECT
                              count(1)
                        FROM  #Account_Details );

            --SELECT @count
            --	PRINT '2'

            --STEP 2: CHECKING FOR AN LOGICAL MATCH, DO WE HAVE CURRENT AND PREVIOUS MONTH DATA FOR THE ACCOUNTS
            --/*
            IF ( @count < @Min_Account )
                  BEGIN

                        INSERT INTO #Account
                                    SELECT
                                          cha.Account_Id
                                        , cha.Client_Hier_Id
                                    FROM
                                          (     SELECT
                                                            cha.Account_Id
                                                          , cha.Client_Hier_Id
                                                FROM        CBMS.Core.Client_Hier_Account cha
                                                WHERE       cha.Account_Id <> @Account_id
                                                            AND   cha.Account_Type = 'utility'
                                                            AND   EXISTS
                                                      (     SELECT
                                                                  1
                                                            FROM  CBMS.dbo.Account_Variance_Consumption_Level avcl
                                                            WHERE avcl.ACCOUNT_ID = cha.Account_Id
                                                                  AND   avcl.Variance_Consumption_Level_Id = @Variance_Consumption_Level_id )
                                                GROUP BY    cha.Account_Id
                                                          , cha.Client_Hier_Id ) cha
                                          JOIN
                                          Core.Client_Hier ch
                                                ON ch.Client_Hier_Id = cha.Client_Hier_Id
                                    WHERE power(ch.Geo_Lat - @Lat, 2) + power(( ch.Geo_Long - ( @long )) * cos(radians(@Lat)), 2) < power(@Analogous_Distance / 110.25, 2)
                                          AND   ch.Country_Id = @country_id
                                          AND   ch.Site_Type_Name = @Site_Type_Name
                                          AND   ch.Region_ID = @Region_id;
                        --AND cha.Commodity_Id = @commodity_id


                        --AND cha.Account_Vendor_Id = @vendor_id



                        --SELECT COUNT(1) FROM #Account
                        INSERT INTO #Account_Details (
                                                           Account_id
                                                     )
                                    SELECT      TOP ( @Min_Account - @count )
                                                cha.Account_id
                                    FROM        #Account cha
                                    WHERE       NOT EXISTS
                                          (     SELECT
                                                      1
                                                FROM  #Account_Details
                                                WHERE Account_id = cha.Account_id )
                                                AND   EXISTS
                                          (     SELECT
                                                      1
                                                FROM  dbo.Cost_Usage_Account_Dtl cuad 
                                                WHERE cuad.ACCOUNT_ID = cha.Account_id
                                                      AND   cuad.Client_Hier_ID = cha.Client_Hier_id
                                                      AND   cuad.Bucket_Master_Id = @Bucket_master_id
                                                      AND   cuad.Service_Month = @Current_Service_Month
                                                      AND   cuad.Data_Source_Cd = @Invoice_Code_Id
                                                      AND   cuad.Data_Type_Cd <> @Estimated_code_Id
                                                     AND
                                                (     (     @Category_name IN (
                                                                  'Demand'
                                                                , 'Billed Demand')
                                                            AND   cuad.Bucket_Value > 0 )
                                                      OR
                                                            (     @Category_name =   'Usage' 
                                                                  AND   cuad.Bucket_Value >= 0 )
																  OR
                                                            (     @Category_name ='Cost'
                                                                      )
																	  )
                                                      AND   NOT EXISTS
                                                      (     SELECT
                                                                  1
                                                            FROM  dbo.Variance_Account_Month_Anomalous_Status vra
                                                            WHERE vra.Account_Id = cuad.ACCOUNT_ID
                                                                  AND   vra.Service_Month = @Current_Service_Month
                                                                  AND   vra.Is_Anomalous = 1 ))
                                                AND   EXISTS
                                          (   
										  SELECT
                                          1
                                    FROM  dbo.Cost_Usage_Account_Dtl cuad 
                                    WHERE cuad.ACCOUNT_ID = cha.Account_Id
                                          AND   cuad.Client_Hier_ID = cha.Client_Hier_Id
                                          AND   cuad.Bucket_Master_Id = @Bucket_master_id
                                          AND   cuad.Service_Month = @Previous_Month
                              AND   cuad.Data_Source_Cd = @Invoice_Code_Id
                              AND   cuad.Data_Type_Cd <> @Estimated_code_Id
										  AND
                                                (     (     @Category_name IN (
                                                                  'Demand'
                                                                , 'Billed Demand')
                                                            AND   cuad.Bucket_Value > 0 )
                                                      OR
                                                            (     @Category_name =   'Usage' 
                                                                  AND   cuad.Bucket_Value >= 0 )
																  OR
                                                            (     @Category_name ='Cost'
                                                                      )
																	  )
                                                      AND   NOT EXISTS
                                                      (     SELECT
                                                                  1
                                                            FROM  dbo.Variance_Account_Month_Anomalous_Status vra
                                                            WHERE vra.Account_Id = cuad.ACCOUNT_ID
                                                                  AND   vra.Service_Month = @Previous_Month
                                                                  AND   vra.Is_Anomalous = 1 ))
                                    GROUP BY    cha.Account_id;
                  END;
            --*/

            SET @count =
                  (     SELECT
                              count(1)
                        FROM  #Account_Details );


            --PRINT '3'
            --STEP 3: CHECKING FOR AN LOGICAL MATCH, DO WE HAVE CURRENT AND SECOND PREVIOUS MONTH DATA FOR THE ACCOUNTS AT LEAST

            IF ( @Min_Account > @count )
                  BEGIN
                        INSERT INTO #Account_Details
                                    SELECT      TOP ( @Min_Account - @count )
                                                Account_id
                                    FROM        #Account CHA
                                    -- WHERE    cha.Meter_Country_Id = @country_id
                                    WHERE       EXISTS
                                          (    
										  SELECT
                                          1
                                    FROM  dbo.Cost_Usage_Account_Dtl cuad
									       WHERE cuad.ACCOUNT_ID = cha.Account_Id
                                          AND   cuad.Client_Hier_ID = cha.Client_Hier_Id
                                          AND   cuad.Bucket_Master_Id = @Bucket_master_id
                                          AND   cuad.Service_Month = @Current_Service_Month
                              AND   cuad.Data_Source_Cd = @Invoice_Code_Id
                              AND   cuad.Data_Type_Cd <> @Estimated_code_Id
										 AND
                                                (     (     @Category_name IN (
                                                                  'Demand'
                                                                , 'Billed Demand')
                                                            AND   cuad.Bucket_Value > 0 )
                                                      OR
                                                            (     @Category_name =   'Usage' 
                                                                  AND   cuad.Bucket_Value >= 0 )
																  OR
                                                            (     @Category_name ='Cost'
                                                                      )
																	  )
                                                      AND   NOT EXISTS
                                                      (     SELECT
                                                                  1
                                                            FROM  dbo.Variance_Account_Month_Anomalous_Status vra
                                                            WHERE vra.Account_Id = cuad.ACCOUNT_ID
                                                                  AND   vra.Service_Month = @Current_Service_Month
                                                                  AND   vra.Is_Anomalous = 1 ))
                                                AND   EXISTS
                                          (   
										  SELECT
                                          1
                                    FROM  dbo.Cost_Usage_Account_Dtl cuad
									
                                    WHERE cuad.ACCOUNT_ID = cha.Account_Id
                                          AND   cuad.Client_Hier_ID = cha.Client_Hier_Id
                                          AND   cuad.Bucket_Master_Id = @Bucket_master_id
                                          AND   cuad.Service_Month = @Second_Previous_Month
                              AND   cuad.Data_Source_Cd = @Invoice_Code_Id
                              AND   cuad.Data_Type_Cd <> @Estimated_code_Id
										 AND
                                                (     (     @Category_name IN (
                                                                  'Demand'
                                                                , 'Billed Demand')
                                                            AND   cuad.Bucket_Value > 0 )
                                                      OR
                                                            (     @Category_name =   'Usage' 
                                                                  AND   cuad.Bucket_Value >= 0 )
																  OR
                                                            (     @Category_name ='Cost'
                                                                      )
																	  )
                                                      AND   NOT EXISTS
                                                      (     SELECT
                                                                  1
                                                            FROM  dbo.Variance_Account_Month_Anomalous_Status vra
                                                            WHERE vra.Account_Id = cuad.ACCOUNT_ID
                                                                  AND   vra.Service_Month = @Second_Previous_Month
                                                                  AND   vra.Is_Anomalous = 1 ))
                                    GROUP BY    CHA.Account_id;

                  END;


            --  WE NEED TO SEND ACTUAL ACCOUNT DETAILS AS WELL FOR ANALOGOUS
            INSERT INTO #Account_Details (
                                               Account_id
                                         )
            VALUES
                 ( @Account_id );



            INSERT INTO #Bucket_Values (
                                             Source_Account_Id
                                           , Service_Month
                                           , Billing_Start_Date
                                           , Billing_End_Date
                                           , BUcket_Master_id
                                           , Bucket_Name
                                           , Bucket_Type
                                           , Bucket_Value
                                           , uom_cd
                                           , uom_Name
                                           , Target_Account_ID
                                           , Target_Account_Client_Hier_Id
                                           , Service_Type
                                           , Rate_Id
                                           , ThresholdSigma
                                           , [Test Calculation Type]
                                           , category_Name
                                           , row_num
                                           , BillingDays
                                       )
                        SELECT
                                    @Account_id
                                  , CUAD.Service_Month AS Service_Month
                                  , cuabd.Billing_Start_Dt AS Billing_Period_StartDate
                                  , cuabd.Billing_End_Dt AS Billing_Period_EndDate
                                  , @Bucket_master_id
                                  , bm.Bucket_Name
                                  , cst.Code_Value Bucket_Type
                                  , ( CASE WHEN cst.Code_Value = 'Charge'
                                                 THEN CUAD.Bucket_Value * isnull(curconv.CONVERSION_FACTOR, 0)
                                           WHEN cst.Code_Value = 'Determinant'
                                                 THEN CUAD.Bucket_Value * isnull(uomconv.CONVERSION_FACTOR, 0)
                                           ELSE  0
                                      END ) Bucket_Value
                                  , ( CASE WHEN cst.Code_Value = 'Charge'
                                                 THEN cu.CURRENCY_UNIT_ID
                                           WHEN cst.Code_Value = 'Determinant'
                                                 THEN uom.ENTITY_ID
                                      END )
                                  , ( CASE WHEN cst.Code_Value = 'Charge'
                                                 THEN cu.CURRENCY_UNIT_NAME
                                           WHEN cst.Code_Value = 'Determinant'
                                                 THEN uom.ENTITY_NAME
                                      END ) AS uom_Name
                                  , cha.Account_Id
                                  , cha.Client_Hier_Id
                                  , 'Analagous'
                                  , cha.Rate_Id
                                  , @Linear_Regression_Sigma_value
                                  , @error_metric
                                  , vcode.Code_Value
                                  , NULL
                                  , cuabd.Billing_Days
                        -- , row_number() OVER(PARTITION BY cha.Account_Id ORDER BY cha.Rate_Id asc)
                        FROM        dbo.Cost_Usage_Account_Dtl CUAD
                                    JOIN
                                    #Account_Details AD
                                          ON AD.Account_id = CUAD.ACCOUNT_ID
                                    JOIN
                                    dbo.Cost_Usage_Account_Billing_Dtl cuabd
                                          ON cuabd.Account_Id = CUAD.ACCOUNT_ID
                                             AND cuabd.Service_Month = CUAD.Service_Month
                                    JOIN
                                    dbo.Bucket_Master bm
                                          ON bm.Bucket_Master_Id = CUAD.Bucket_Master_Id
                                    INNER JOIN
                                    dbo.Code cst
                                          ON cst.Code_Id = bm.Bucket_Type_Cd
                                    CROSS APPLY
                              (     SELECT      TOP ( 1 )
                                                cha.Rate_Id
                                              , cha.Account_Id
                                              , cha.Client_Hier_Id
                                              , ch.Client_Currency_Group_Id
                                              , cha.Commodity_Id
                                    FROM        Core.Client_Hier_Account cha
                                                JOIN
                                                Core.Client_Hier ch
                                                      ON ch.Client_Hier_Id = cha.Client_Hier_Id
                                    WHERE       cha.Client_Hier_Id = CUAD.Client_Hier_ID
                                                AND   cha.Account_Id = CUAD.ACCOUNT_ID
                                                AND   cha.Commodity_Id = @commodity_id ) cha
                                    JOIN
                                    dbo.Variance_category_Bucket_Map vcbm
                                          ON vcbm.Bucket_master_Id = bm.Bucket_Master_Id
                                    JOIN
                                    Code vcode
                                          ON vcode.Code_Id = vcbm.Variance_Category_Cd
                                    LEFT OUTER JOIN
                                    dbo.CURRENCY_UNIT_CONVERSION curconv
                                          ON curconv.BASE_UNIT_ID = CUAD.CURRENCY_UNIT_ID
                                             AND curconv.CONVERSION_DATE = CUAD.Service_Month
                                             AND curconv.CURRENCY_GROUP_ID = cha.Client_Currency_Group_Id
                                             AND curconv.CONVERTED_UNIT_ID = @currency_unit_id --@currency_Unit_Id
                                             AND cst.Code_Value = 'Charge'
                                    LEFT OUTER JOIN
                                    dbo.CONSUMPTION_UNIT_CONVERSION uomconv
                                          ON uomconv.BASE_UNIT_ID = CUAD.UOM_Type_Id
                                             AND uomconv.CONVERTED_UNIT_ID = bm.Default_Uom_Type_Id
                                             AND cst.Code_Value = 'Determinant'
                                    LEFT OUTER JOIN
                                    dbo.CURRENCY_UNIT cu
                                          ON cu.CURRENCY_UNIT_ID = @currency_unit_id --@currency_Unit_Id
                                    LEFT OUTER JOIN
                                    dbo.ENTITY uom
                                          ON uom.ENTITY_ID = bm.Default_Uom_Type_Id
                        WHERE       CUAD.Service_Month
                                    BETWEEN @Second_Previous_Month AND @Current_Service_Month
                                    AND   cha.Commodity_Id = @commodity_id
                                    AND   CUAD.Data_Source_Cd = @Invoice_Code_Id --NOT IN ( @DE_Code_id, @CBMS_Code_id )
                                    AND   CUAD.Data_Type_Cd <> @Estimated_code_Id
                                    AND   bm.Bucket_Master_Id = @Bucket_master_id
                                    AND   NOT EXISTS
                              (     SELECT
                                          1
                                    FROM  dbo.Variance_Account_Month_Anomalous_Status vra
                                    WHERE vra.Account_Id = CUAD.ACCOUNT_ID
                                          AND   vra.Service_Month = CUAD.Service_Month
                                          AND   vra.Is_Anomalous = 1 )
                        GROUP BY    bm.Bucket_Name
                                  , cst.Code_Value
                                  , CUAD.Service_Month
                                  , ( CASE WHEN cst.Code_Value = 'Charge'
                                                 THEN cu.CURRENCY_UNIT_ID
                                           WHEN cst.Code_Value = 'Determinant'
                                                 THEN uom.ENTITY_ID
                                      END )
                                  , ( CASE WHEN cst.Code_Value = 'Charge'
                                                 THEN cu.CURRENCY_UNIT_NAME
                                           WHEN cst.Code_Value = 'Determinant'
                                                 THEN uom.ENTITY_NAME
                                      END )
                                  , ( CASE WHEN cst.Code_Value = 'Charge'
                                                 THEN CUAD.Bucket_Value * isnull(curconv.CONVERSION_FACTOR, 0)
                                           WHEN cst.Code_Value = 'Determinant'
                                                 THEN CUAD.Bucket_Value * isnull(uomconv.CONVERSION_FACTOR, 0)
                                           ELSE  0
                                      END )
                                  , cha.Client_Hier_Id
                                  , cha.Rate_Id
                                  , cha.Account_Id
                                  , cuabd.Billing_Start_Dt
                                  , cuabd.Billing_End_Dt
                                  , vcode.Code_Value
                                  , cuabd.Billing_Days
                        ORDER BY    bm.Bucket_Name
                                  , cst.Code_Value
                                  , Service_Month;


            SELECT
                  Source_Account_Id
                , Service_Month
                , Billing_Start_Date
                , Billing_End_Date
                , Bucket_Name
                , Bucket_Type
                , Bucket_Value
                , uom_cd
                , uom_Name
                , Target_Account_ID
                , Target_Account_Client_Hier_Id
                , Service_Type
                , Rate_Id
                , BUcket_Master_id
                , ThresholdSigma
                , [Test Calculation Type]
                , category_Name
                , BillingDays
            FROM  #Bucket_Values;
            --WHERE row_num = 1;

            DROP TABLE #Account_Details;
            DROP TABLE
                  #Bucket_Values
                , #Account;

      END;

GO
