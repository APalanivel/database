
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	cbmsPricePoint_Get 

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType			Default		Description
---------------------------------------------------------------
@MyAccountId			int  
@price_point_id			int  
  	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
---------------------------------------------------------------

USAGE EXAMPLES:
---------------------------------------------------------------
exec cbmsPricePoint_Get @price_point_id = 1418,@MyAccountId=1

AUTHOR INITIALS:
	Initials	Name
---------------------------------------------------------------
	RKV			Ravi Kumar Vegesna
	
MODIFICATIONS

	Initials	Date		Modification
---------------------------------------------------------------
	RKV        	02/10/2016	added comments and added new column Price_Point_Short_Name,Commodity_Id as part of AS400-PII
******/   
CREATE   PROCEDURE [dbo].[cbmsPricePoint_Get]
      ( 
       @MyAccountId INT
      ,@price_point_id INT )
AS 
BEGIN  
  
      SELECT
            pr.price_index_id price_point_id
           ,pr.index_id market_index_id
           ,mi.entity_name market_index
           ,pr.pricing_point price_point
           ,pr.index_description price_point_desc
           ,pr.currency_unit_id
           ,cur.currency_unit_name
           ,pr.Price_Point_Short_Name
           ,pr.Commodity_Id
           ,c.Commodity_Name
           ,pr.VOLUME_UNIT_ID
           ,vni.ENTITY_NAME Volume_Unit
      FROM
            price_index pr
            JOIN entity mi
                  ON mi.entity_id = pr.index_id
            LEFT OUTER JOIN currency_unit cur
                  ON cur.currency_unit_id = pr.currency_unit_id
            LEFT OUTER JOIN dbo.Commodity c
                  ON pr.Commodity_Id = c.Commodity_Id
            LEFT OUTER JOIN dbo.ENTITY vni
				ON pr.VOLUME_UNIT_ID = vni.ENTITY_ID      
      WHERE
            pr.price_index_id = @price_point_id  
  
END

;
GO

GRANT EXECUTE ON  [dbo].[cbmsPricePoint_Get] TO [CBMSApplication]
GO
