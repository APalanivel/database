SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
CREATE      PROCEDURE DBO.UPDATE_DEAL_TICKET_VOLUME_P

@userId varchar(10),
@sessionId varchar(16),
@dealTicketDetailsId int, 
@totalVolume decimal(32,16), 
@hedgePrice decimal(32,16), 
@dealTransactionDate datetime 
AS
	set nocount on
UPDATE 
	RM_DEAL_TICKET_DETAILS
SET 
	TOTAL_VOLUME = @totalVolume, 
	HEDGE_PRICE = @hedgePrice, 
	DEAL_TRANSACTION_DATE = @dealTransactionDate
WHERE 
	RM_DEAL_TICKET_DETAILS_ID = @dealTicketDetailsId
GO
GRANT EXECUTE ON  [dbo].[UPDATE_DEAL_TICKET_VOLUME_P] TO [CBMSApplication]
GO
