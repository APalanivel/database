SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  dbo.Cbms_ContractDateChange_IpReset  

 DESCRIPTION:   
			This procedure called to reset/recalculate INVOICE_PARTICIPATION/INVOICE_PARTICIPATION_Site
			Delete contract_meter_volume records for the respective service months deleted
		
 
 INPUT PARAMETERS:
 Name					DataType	Default Description
------------------------------------------------------------
 @MyAccountId			INT
 @Contract_Number		VARCHAR(50)
 @Change_Start_Date		DATETIME
 @Change_End_Date		DATETIME
 
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 USAGE EXAMPLES:  
------------------------------------------------------------  

BEGIN TRAN
	EXEC dbo.Cbms_ContractDateChange_IpReset 49,'86437-0001',NULL,'2017-06-23'
	
	SELECT ACCOUNT_ID,SITE_ID,SERVICE_MONTH FROM dbo.INVOICE_PARTICIPATION WHERE ACCOUNT_ID = 447088
	--SELECT * FROM dbo.INVOICE_PARTICIPATION_SITE WHERE SITE_ID IN (37097,37300)
ROLLBACK TRAN

 AUTHOR INITIALS:  
	Initials	Name  
------------------------------------------------------------  
	RR			Raghu Reddy
 
 MODIFICATIONS:  
	Initials	Date		Modification  
------------------------------------------------------------  
	RR			11/21/2011  Created. This procedure is part of dbo.cbmsContract_ChangeDate which is used in .net contract date change tool, the procedure 
							is splitted to reuse the same in .net contract date change tool and edit contract page in java to avoid the duplication 
							of functionality. The procedure accepts only one date start/end date and other date should be NULL
	RR			11/25/2011  In case of start date moved in inward direction @Date1,@Date2 set to first day of the month as it is missing 
							the first/old start date service month to delete if the date is not first day of the month.
							CASE: old start date :4/15/2011 new start date: 6/15/2011, as per the old condition the procedure deleting service 
								which falls in between 4/15/2011 - 6/14/2011(-1 day to escape current month) are 5/1/2011 and 6/1/2011, but the 
								procedure should delete service months 4/1/2011 and 5/1/2011 falls between 4/1/2011 - 5/31/2011
							In case end date moved in outward direction adding 1 day as the condition (lessthan @date2) will miss to collect the last month
							from meta.Date_Dim to add/reset ip_site table
	RR			2020-04-30	GRM - Contract volumes enhancement - Modified to delete data from dbo.Contract_Meter_Volume_Dtl
	
******/

CREATE PROCEDURE [dbo].[Cbms_ContractDateChange_IpReset]
    (
        @MyAccountId INT
        , @Contract_Number VARCHAR(50)
        , @Change_Start_Date DATETIME = NULL
        , @Change_End_Date DATETIME = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;



        DECLARE @Contract_Id INT;
        DECLARE @Current_Start_Date DATETIME;
        DECLARE @Current_End_Date DATETIME;
        DECLARE @Date1 DATETIME;
        DECLARE @Date2 DATETIME;
        DECLARE @Will_Delete_Ip BIT;

        DECLARE @Counter INT;
        DECLARE @Site_id INT;
        DECLARE @Account_id INT;
        DECLARE @Insert_Month DATETIME;
        DECLARE @Service_Month DATETIME;

        DECLARE @tbl_Contract_Supplier_Account TABLE
              (
                  Account_Id INT PRIMARY KEY CLUSTERED
              );

        DECLARE @contract_date_change TABLE
              (
                  Id INT IDENTITY(1, 1)
                  , site_id INT
                  , service_month DATETIME
              );

        DECLARE @tbl_Siteid_SupplierAccount TABLE
              (
                  Id INT IDENTITY(1, 1)
                  , Site_Id INT
                  , Account_id INT
              );

        SELECT
            @Contract_Id = CONTRACT_ID
            , @Current_Start_Date = CONTRACT_START_DATE
            , @Current_End_Date = CONTRACT_END_DATE
        FROM
            dbo.CONTRACT
        WHERE
            ED_CONTRACT_NUMBER = @Contract_Number;

        IF @Change_Start_Date IS NOT NULL
            BEGIN
                IF @Change_Start_Date > @Current_Start_Date
                    BEGIN

                        SET @Will_Delete_Ip = 1; --will need to DELETE invoice_participation.
                        -- Dates set to first day of month service months may miss if the date is not first day of month
                        SET @Date1 = CASE WHEN DATEPART(D, @Current_Start_Date) = 1 THEN @Current_Start_Date
                                         ELSE
                                             CAST(DATEPART(MONTH, @Current_Start_Date) AS VARCHAR(2)) + '/1/'
                                             + CAST(DATEPART(YEAR, @Current_Start_Date) AS VARCHAR(4))
                                     END;

                        SET @Date2 = CASE WHEN DATEPART(D, @Change_Start_Date) = 1 THEN @Change_Start_Date
                                         ELSE
                                             CAST(DATEPART(MONTH, @Change_Start_Date) AS VARCHAR(2)) + '/1/'
                                             + CAST(DATEPART(YEAR, @Change_Start_Date) AS VARCHAR(4))
                                     END;
                        SET @Date2 = @Date2 - 1; --subtract 1 day so that this month also doesn't get deleted.

                    END;
                ELSE
                    BEGIN
                        SET @Date1 = @Change_Start_Date; -- Setting it to first day of the month
                        SET @Date2 = @Current_Start_Date - 1;
                        SET @Will_Delete_Ip = 0; --will need to insert invoice_participation
                    END;
            END;

        IF @Change_End_Date IS NOT NULL
            BEGIN
                IF @Change_End_Date < @Current_End_Date
                    BEGIN
                        SET @Date1 = @Change_End_Date + 1; --add 1 day so that this month doesn't also get DELETEd
                        SET @Date2 = @Current_End_Date;
                        SET @Will_Delete_Ip = 1; --will need to DELETE invoice_participation
                    END;
                ELSE
                    BEGIN
                        SET @Date1 = @Current_End_Date; -- Setting it to first day of the month
                        SET @Date2 = @Change_End_Date + 1; --add 1 day so that this month also added to ip_site table
                        SET @Will_Delete_Ip = 0; --will need to insert invoice_participation
                    END;
            END;

        BEGIN TRY

            BEGIN TRAN;

            INSERT INTO @tbl_Contract_Supplier_Account
                 (
                     Account_Id
                 )
            SELECT
                ACCOUNT_ID
            FROM
                dbo.SUPPLIER_ACCOUNT_METER_MAP
            WHERE
                Contract_ID = @Contract_Id
            GROUP BY
                ACCOUNT_ID;
            INSERT INTO @tbl_Siteid_SupplierAccount
                 (
                     Site_Id
                     , Account_id
                 )
            SELECT  DISTINCT
                    ch.Site_Id
                    , cha.Account_Id
            FROM
                Core.Client_Hier ch
                JOIN Core.Client_Hier_Account cha
                    ON ch.Client_Hier_Id = cha.Client_Hier_Id
                JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP sam
                    ON cha.Account_Id = sam.ACCOUNT_ID
            WHERE
                sam.Contract_ID = @Contract_Id;


            -- DELETE invoice_participation
            IF @Will_Delete_Ip = 1
                BEGIN

                    DELETE
                    cmvd
                    FROM
                        dbo.Contract_Meter_Volume_Dtl cmvd
                        INNER JOIN dbo.CONTRACT_METER_VOLUME cmv
                            ON cmv.CONTRACT_METER_VOLUME_ID = cmvd.CONTRACT_METER_VOLUME_ID
                    WHERE
                        cmv.CONTRACT_ID = @Contract_Id
                        AND cmv.MONTH_IDENTIFIER >= @Date1
                        AND cmv.MONTH_IDENTIFIER <= @Date2;

                    DELETE  FROM
                    dbo.CONTRACT_METER_VOLUME
                    WHERE
                        CONTRACT_ID = @Contract_Id
                        AND MONTH_IDENTIFIER >= @Date1
                        AND MONTH_IDENTIFIER <= @Date2;

                    PRINT '' + CAST(@@ROWCOUNT AS VARCHAR(10)) + ' contract_meter_volume(s) Deleted';

                    INSERT INTO @contract_date_change
                         (
                             site_id
                             , service_month
                         )
                    SELECT
                        SITE_ID
                        , SERVICE_MONTH
                    FROM
                        dbo.INVOICE_PARTICIPATION ip
                        JOIN @tbl_Contract_Supplier_Account sa
                            ON sa.Account_Id = ip.ACCOUNT_ID
                    WHERE
                        ip.SERVICE_MONTH >= @Date1
                        AND ip.SERVICE_MONTH <= @Date2;

                    DELETE
                    ip
                    FROM
                        dbo.INVOICE_PARTICIPATION ip
                        JOIN @tbl_Contract_Supplier_Account sa
                            ON sa.Account_Id = ip.ACCOUNT_ID
                    WHERE
                        SERVICE_MONTH >= @Date1
                        AND SERVICE_MONTH <= @Date2;

                    -- Deletes from INVOICE_PARTICIPATION_Site only if there is no record(s) for the same deleted service month(s) of site(s) present in INVOICE_PARTICIPATION
                    DELETE
                    ips
                    FROM
                        dbo.INVOICE_PARTICIPATION_SITE ips
                        JOIN @contract_date_change cd
                            --(Deleted service month(s), site(s))
                            ON ips.SITE_ID = cd.site_id
                               AND  cd.service_month = ips.SERVICE_MONTH
                    WHERE
                        NOT EXISTS (   SELECT
                                            1
                                       FROM
                                            dbo.INVOICE_PARTICIPATION ip
                                            JOIN @contract_date_change cdc
                                                ON cdc.site_id = ip.SITE_ID
                                                   AND  cdc.service_month = ip.SERVICE_MONTH
                                                   AND  ips.SITE_ID = ip.SITE_ID
                                                   AND  ips.SERVICE_MONTH = ip.SERVICE_MONTH);


                    PRINT '' + CAST(@@ROWCOUNT AS VARCHAR(10)) + ' invoice_participation(s) Deleted';

                END;
            ELSE --@Will_Delete_Ip = 0, insert invoice_participation
                BEGIN
                    SET @Insert_Month = @Date1;
                    --IF the @Insert_Month is not the first day of a month, change @Insert_Month to the first day of the month
                    SET @Insert_Month = CASE WHEN DATEPART(D, @Insert_Month) = 1 THEN @Date1
                                            ELSE
                                                CAST(DATEPART(MONTH, @Insert_Month) AS VARCHAR(2)) + '/1/'
                                                + CAST(DATEPART(YEAR, @Insert_Month) AS VARCHAR(4))
                                        END;

                    --for every month inserted, load the site_id AND service_month into the temp table
                    INSERT INTO @contract_date_change
                         (
                             site_id
                             , service_month
                         )
                    SELECT
                        s.SITE_ID
                        , dd.DATE_D
                    FROM
                        dbo.SITE s
                        JOIN dbo.ADDRESS a
                            ON a.ADDRESS_ID = s.PRIMARY_ADDRESS_ID
                        JOIN dbo.METER m
                            ON m.ADDRESS_ID = a.ADDRESS_ID
                        JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                            ON samm.METER_ID = m.METER_ID
                        CROSS JOIN meta.DATE_DIM dd
                    WHERE
                        samm.Contract_ID = @Contract_Id
                        AND dd.DATE_D >= @Insert_Month
                        AND dd.DATE_D < @Date2
                    GROUP BY
                        s.SITE_ID
                        , dd.DATE_D;

                    SET @Counter = 1;

                    WHILE (@Counter <= (SELECT  COUNT(1)FROM    @tbl_Siteid_SupplierAccount))
                        BEGIN

                            SELECT
                                @Site_id = Site_Id
                                , @Account_id = Account_id
                            FROM
                                @tbl_Siteid_SupplierAccount
                            WHERE
                                Id = @Counter;

                            --this sproc will add new invoice_participation to accomodate the expanded contract period

                            EXEC cbmsInvoiceParticipation_InsertSupplierAccount
                                @MyAccountId
                                , @Account_id
                                , @Site_id;

                            PRINT 'invoice_participation(s) in queue to be added';

                            SET @Counter = @Counter + 1;

                        END;
                END;

            -- RECALCULATE TOTALS IN  INVOICE_PARTICIPATION_SITE

            SET @Counter = 1;

            WHILE (@Counter <= (SELECT  COUNT(1)FROM    @contract_date_change))
                BEGIN

                    SELECT
                        @Site_id = site_id
                        , @Service_Month = service_month
                    FROM
                        @contract_date_change
                    WHERE
                        Id = @Counter;

                    EXEC cbmsInvoiceParticipationSite_Save
                        @MyAccountId
                        , @Site_id
                        , @Service_Month;
                    PRINT 'invoice_participation_site saved';

                    SET @Counter = @Counter + 1;

                END;

            /*-----------------------------------------------------------------*/

            COMMIT TRAN;

        END TRY
        BEGIN CATCH

            ROLLBACK TRAN;
            EXEC usp_RethrowError
                'An error occurred AND the transaction was rolled back';
            PRINT ('-----------------------------------------------------');
            PRINT ('An error occurred AND the transaction was rolled back');

        END CATCH;

    END;

GO
GRANT EXECUTE ON  [dbo].[Cbms_ContractDateChange_IpReset] TO [CBMSApplication]
GO
