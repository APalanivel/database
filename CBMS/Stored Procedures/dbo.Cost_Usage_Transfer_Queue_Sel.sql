SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME:
		dbo.Cost_Usage_Transfer_Queue_Sel
		
DESCRIPTION:
			To select the records for site level aggregation and variance test on DE cost and usage data 
			added to the queue table during data transfer
      
INPUT PARAMETERS:          
	NAME					DATATYPE	DEFAULT		DESCRIPTION          
---------------------------------------------------------------------          
	@Is_Aggregated_To_Site	BIT			NULL
    @Is_Variance_Tested		BIT			NULL
	@Top_X_Records			INT			500

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION
---------------------------------------------------------------------          

USAGE EXAMPLES:          
---------------------------------------------------------------------  
	Exec dbo.Cost_Usage_Transfer_Queue_Sel 1,null
	Exec dbo.Cost_Usage_Transfer_Queue_Sel 0,NULL
     
AUTHOR INITIALS:          
	INITIALS	NAME          
------------------------------------------------------------          
	RR			Raghu Reddy

MODIFICATIONS
	INITIALS	DATE		MODIFICATION
-----------------------------------------------------------
	RR			2013-05-03	Created
*/

CREATE PROCEDURE dbo.Cost_Usage_Transfer_Queue_Sel
      ( 
       @Is_Aggregated_To_Site BIT = NULL
      ,@Is_Variance_Tested BIT = NULL
      ,@Top_X_Records INT = 500 )
AS 
BEGIN
      SET NOCOUNT ON;

      SELECT TOP ( @Top_X_Records )
            que.Cost_Usage_Transfer_Queue_Id
           ,que.Client_Hier_Id
           ,que.Account_Id
           ,que.Service_Month
           ,ch.Client_Id
      FROM
            Stage.Cost_Usage_Transfer_Queue que
            INNER JOIN core.Client_Hier ch
                  ON que.Client_Hier_Id = ch.Client_Hier_Id
      WHERE
            ( @Is_Aggregated_To_Site IS NULL
              OR que.Is_Aggregated_To_Site = @Is_Aggregated_To_Site )
            AND ( @Is_Variance_Tested IS NULL
                  OR que.Is_Variance_Tested = @Is_Variance_Tested )
      ORDER BY
            que.Cost_Usage_Transfer_Queue_Id
      
END
;
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Transfer_Queue_Sel] TO [CBMSApplication]
GO
