
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME: dbo.Cost_Usage_Check_Manual_Data_Presence_By_Account_Service_Month  
  
DESCRIPTION:   
  
Used to Check the manual data entry for 'DE' and 'CBMS'  by given account id and service month
  
INPUT PARAMETERS:  
      
  
      Name					DataType          Default     Description      
------------------------------------------------------------------    
       @account_id			INT
      ,@service_month		DATE      
      ,@Client_Hier_Id		INT

OUTPUT PARAMETERS:      
      Name                DataType          Default     Description      
------------------------------------------------------------      
  
  
USAGE EXAMPLES:  
------------------------------------------------------------  

EXEC Cost_Usage_Check_Manual_Data_Presence_By_Account_Service_Month 442265,'2011-06-01',1155
EXEC Cost_Usage_Check_Manual_Data_Presence_By_Account_Service_Month 429712,'2010-08-01',11064
EXEC Cost_Usage_Check_Manual_Data_Presence_By_Account_Service_Month 29408,'2010-01-01',1262

                    
AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 BCH		Balaraju  
 RR			Raghu Reddy
 SP			Sandeep Pigilam
 NR			Narayana Reddy
  
MODIFICATIONS  
  
 Initials	Date			Modification  
------------------------------------------------------------  
 BCH		11/03/2011		Created   
 RR			2012-04-27		Added input paramater @Client_Hier_Id
 SP			2014-07-23		Data Operations Enhancement Phase III,Added Has_Estimated_Invoice columns in the Select list.
 NR			2016-12-23		MAINT-4737 replaced Invoice_Type_Cd with Meter_Read_Type_Cd.

     
******/  
CREATE PROCEDURE [dbo].[Cost_Usage_Check_Manual_Data_Presence_By_Account_Service_Month]
      ( 
       @Account_id INT
      ,@Service_Month DATE
      ,@Client_Hier_Id INT )
AS 
BEGIN  
  
  
      SET NOCOUNT ON
	  
      DECLARE @EL_NG_Data_Source_Code TABLE
            ( 
             Data_Source_Code INT PRIMARY KEY )  
      DECLARE
            @Has_Manual_Data BIT
           ,@Has_Estimated_Invoice BIT
        
      INSERT      INTO @EL_NG_Data_Source_Code
                  ( 
                   Data_Source_Code )
                  SELECT
                        cu.Data_Source_Cd
                  FROM
                        dbo.Cost_Usage_Account_Dtl cu
                  WHERE
                        cu.ACCOUNT_ID = @Account_id
                        AND cu.Service_Month = @Service_Month
                        AND cu.Client_Hier_Id = @Client_Hier_Id
                  GROUP BY
                        cu.Data_Source_Cd
  
  
      SELECT
            @Has_Manual_Data = CASE WHEN D_Entry.Code_Value IN ( 'DE', 'CBMS' ) THEN 1
                                    ELSE 0
                               END
      FROM
            @EL_NG_Data_Source_Code elngd
            JOIN dbo.Code D_Entry
                  ON D_Entry.Code_id = elngd.Data_Source_Code  


      SELECT
            @Has_Estimated_Invoice = ISNULL(MAX(CASE WHEN c.Code_Value = 'Estimated' THEN 1
                                                     ELSE 0
                                                END), 0)
      FROM
            dbo.cu_invoice inv
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                  ON cism.CU_INVOICE_ID = inv.CU_INVOICE_ID
            LEFT OUTER JOIN dbo.Code c
                  --ON inv.Invoice_Type_Cd = c.Code_Id
                  ON inv.Meter_Read_Type_Cd = c.Code_Id
      WHERE
            cism.Account_ID = @Account_id
            AND cism.Service_Month = @Service_Month
            AND inv.IS_REPORTED = 1
            AND inv.IS_PROCESSED = 1

      SELECT
            @Has_Manual_Data AS Has_Manual_Data
           ,@Has_Estimated_Invoice AS Has_Estimated_Invoice
      
END;




;

;
GO



GRANT EXECUTE ON  [dbo].[Cost_Usage_Check_Manual_Data_Presence_By_Account_Service_Month] TO [CBMSApplication]
GO
