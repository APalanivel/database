
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/*********       
NAME:      
 dbo.CBMS_ADD_CLIENT_P      
  
DESCRIPTION:  This procedure used to insert the records in Client table  
  
INPUT PARAMETERS:      
      Name              DataType          Default     Description      
------------------------------------------------------------------      
@clientTypeId              int,  
@ubmServiceId              int,  
@reportFreqId              int,  
@clientName                varchar(200),  
@fiscalYearTypeId          int,  
@ubmStartDate              datetime,  
@dsmStrategy               int,  
@isSepIssued               int,  
@sepDate                   datetime,  
@currencyGroupId           int,  
@notManaged                int,  
@riskProfileTypeId         int,  
@Analyst_Mapping_Cd        INT    
      
OUTPUT PARAMETERS:      
      Name              DataType          Default     Description      
------------------------------------------------------------      
@clientId               int   
      
      
USAGE EXAMPLES:      
------------------------------------------------------------      
  
EXEC CBMS_ADD_CLIENT_P   
    @clientTypeId = 142,  
    @ubmServiceId = 23,  
    @reportFreqId = 144,  
    @clientName = 'Testing Data',  
    @fiscalYearTypeId = 296,  
    @ubmStartDate ='2007-01-01 00:00:00.000',  
    @dsmStrategy =0,  
    @isSepIssued =0,  
    @sepDate = NULL,  
    @currencyGroupId = 0,  
    @notManaged =3,  
    @riskProfileTypeId = 1491,  
    @clientId = NULL,  
    @Analyst_Mapping_Cd = 100503  
  
  
Initials Name      
------------------------------------------------------------      
DR       Deana Ritter  
AKR      Ashok Kumar Raju    
  
Initials  Date  Modification      
------------------------------------------------------------      
DR        8/4/2009    Removed Linked Server Updates  
DMR    09/10/2010   Modified for Quoted_Identifier  
AKR    2012-09-18   Added Analyst_Mapping_Cd Column    
  
******/  
CREATE PROCEDURE dbo.CBMS_ADD_CLIENT_P  
      ( @clientTypeId INT  
      ,@ubmServiceId INT  
      ,@reportFreqId INT  
      ,@clientName VARCHAR(200)  
      ,@fiscalYearTypeId INT  
      ,@ubmStartDate DATETIME  
      ,@dsmStrategy INT  
      ,@isSepIssued INT  
      ,@sepDate DATETIME  
      ,@currencyGroupId INT  
      ,@notManaged INT  
      ,@riskProfileTypeId INT  
      ,@Analyst_Mapping_Cd INT  
      ,@clientId INT OUT )  
AS   
BEGIN  
  
      SET NOCOUNT ON  
  
      INSERT      INTO CLIENT  
                  (   
                   CLIENT_TYPE_ID  
                  ,UBM_SERVICE_ID  
                  ,REPORT_FREQUENCY_TYPE_ID  
                  ,CLIENT_NAME  
                  ,FISCALYEAR_STARTMONTH_TYPE_ID  
                  ,UBM_START_DATE  
                  ,DSM_STRATEGY  
                  ,IS_SEP_ISSUED  
                  ,SEP_ISSUE_DATE  
                  ,CURRENCY_GROUP_ID  
                  ,RISK_PROFILE_TYPE_ID  
                  ,NOT_MANAGED  
                  ,Analyst_Mapping_Cd )  
      VALUES  
                  (   
                   @clientTypeId  
                  ,@ubmServiceId  
                  ,@reportFreqId  
                  ,@clientName  
                  ,@fiscalYearTypeId  
                  ,@ubmStartDate  
                  ,@dsmStrategy  
                  ,@isSepIssued  
                  ,@sepDate  
                  ,@currencyGroupId  
                  ,@riskProfileTypeId  
                  ,@notManaged  
                  ,@Analyst_Mapping_Cd )  
  
      SELECT  
            @clientId = scope_identity()  
  
      INSERT      INTO dbo.CLIENT_CURRENCY_GROUP_MAP
                  ( currency_group_id, client_id )
      VALUES  
                  ( 3, @clientId )  
  
      RETURN  
  
END  
;
GO

GRANT EXECUTE ON  [dbo].[CBMS_ADD_CLIENT_P] TO [CBMSApplication]
GO
