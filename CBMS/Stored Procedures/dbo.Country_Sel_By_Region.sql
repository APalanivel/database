SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
		dbo.Country_Sel_By_Region

DESCRIPTION:
			Country list by given region

INPUT PARAMETERS:
	Name				DataType		Default				Description
---------------------------------------------------------------------------
	@Region_Id      	INT          	

OUTPUT PARAMETERS:
	Name				DataType		Default				Description
---------------------------------------------------------------------------

USAGE EXAMPLES:
---------------------------------------------------------------------------

	EXEC dbo.Country_Sel_By_Region 4
	EXEC dbo.Country_Sel_By_Region 5
	EXEC dbo.Country_Sel_By_Region 6
	EXEC dbo.Country_Sel_By_Region 7



AUTHOR INITIALS:

	Initials			Name
---------------------------------------------------------------------------
	NR					Narayana Reddy
MODIFICATIONS

	Initials			Date			Modification
---------------------------------------------------------------------------
	NR					2015-07-03		Created for Global CBMS Sourcing.


******/
CREATE PROCEDURE [dbo].[Country_Sel_By_Region] ( @Region_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 

      SELECT
            c.COUNTRY_ID
           ,c.COUNTRY_NAME
      FROM
            dbo.STATE s
            INNER JOIN dbo.COUNTRY c
                  ON s.COUNTRY_ID = c.COUNTRY_ID
      WHERE
            s.REGION_ID = @Region_Id
      GROUP BY
            c.COUNTRY_ID
           ,c.COUNTRY_NAME
      ORDER BY
            c.COUNTRY_NAME
           
     
END;
;
GO
GRANT EXECUTE ON  [dbo].[Country_Sel_By_Region] TO [CBMSApplication]
GO
