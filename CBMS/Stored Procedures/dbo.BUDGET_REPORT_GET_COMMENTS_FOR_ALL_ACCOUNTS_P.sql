SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




--select * from budget_account  where budget_account_id = 28
--select * from budget_detail_comments
--exec BUDGET_REPORT_GET_COMMENTS_FOR_ALL_ACCOUNTS_P 11 , null,null,null,291


CREATE  PROCEDURE dbo.BUDGET_REPORT_GET_COMMENTS_FOR_ALL_ACCOUNTS_P
	@budget_id int,
	@clientId int,
	@divisionId int,
	@siteId int ,
	@commodityId int
	AS
	begin
		set nocount on

		select	budget_account.budget_account_id,
			comments.variable_comments,
			comments.transportation_comments,
			comments.distribution_comments,
			comments.transmission_comments,
			comments.other_bundled_comments,
			comments.other_fixed_costs_comments,
			comments.sourcing_tax_comments,
			comments.rates_tax_comments

	 
		from 	budget 
			join budget_account on budget_account.budget_id = budget.budget_id
			and budget.commodity_type_id = @commodityId
			and budget.budget_id = @budget_id
			join budget_detail_comments comments on comments.budget_account_id = budget_account.budget_account_id
			join account utilacc on utilacc.account_id = budget_account.account_id
			JOIN SITE S WITH(NOLOCK) ON (utilacc.site_id = s.site_id and S.SITE_ID = COALESCE(@siteId, s.site_id))
			JOIN DIVISION D WITH(NOLOCK) ON (D.DIVISION_ID = S.DIVISION_ID AND  D.DIVISION_ID = COALESCE(@divisionId, D.DIVISION_ID))
			JOIN CLIENT C WITH(NOLOCK) ON (C.CLIENT_ID = D.CLIENT_ID AND C.CLIENT_ID = COALESCE(@clientId, c.client_id))

	end







GO
GRANT EXECUTE ON  [dbo].[BUDGET_REPORT_GET_COMMENTS_FOR_ALL_ACCOUNTS_P] TO [CBMSApplication]
GO
