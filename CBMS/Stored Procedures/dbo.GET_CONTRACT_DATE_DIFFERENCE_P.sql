SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE dbo.GET_CONTRACT_DATE_DIFFERENCE_P
	@contract_id INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT MONTH(contract_start_date),
		DATENAME(YEAR,contract_start_date),
		DATEDIFF(MONTH,contract_start_date,contract_end_date)
	FROM dbo.contract
	WHERE contract_id=@contract_id

END
GO
GRANT EXECUTE ON  [dbo].[GET_CONTRACT_DATE_DIFFERENCE_P] TO [CBMSApplication]
GO
