SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
       
/******         
         
 NAME: dbo.Client_App_Access_Role_Group_Info_Map_Sel_By_Group_Info_Id      
                
 DESCRIPTION:          
      To get the roles  for a given group info id.  
             
 INPUT PARAMETERS:          
         
 Name                               DataType       Default          Description          
---------------------------------------------------------------------------------------------------------------        
 @Group_Info_Id                      INT  
 @Client_Id							 INT      
          
 OUTPUT PARAMETERS:        
                 
 Name                               DataType       Default          Description          
---------------------------------------------------------------------------------------------------------------        
          
 USAGE EXAMPLES:              
---------------------------------------------------------------------------------------------------------------   
        
	 EXEC dbo.Client_App_Access_Role_Group_Info_Map_Sel_By_Group_Info_Id   @Group_Info_Id=213,@Client_Id=235           
         
 AUTHOR INITIALS:          
        
 Initials               Name          
---------------------------------------------------------------------------------------------------------------        
 NR                     Narayana Reddy            
           
 MODIFICATIONS:          
         
 Initials               Date            Modification        
---------------------------------------------------------------------------------------------------------------        
 NR                     2013-12-26      Created for RA Admin user management        
         
******/  

CREATE  PROCEDURE dbo.Client_App_Access_Role_Group_Info_Map_Sel_By_Group_Info_Id
      ( 
       @Group_Info_Id INT
      ,@Client_Id INT )
AS 
BEGIN  
      SET NOCOUNT ON   
  
      SELECT
            car.Client_App_Access_Role_Id
           ,car.App_Access_Role_Name
           ,car.App_Access_Role_Dsc
           ,caargim.Group_Info_Id
           ,CASE WHEN c.Code_Value = 'Admin' THEN 1
                 WHEN c.Code_Value = 'Full Access (Non Admin)' THEN 1
                 ELSE 0
            END AS Is_Default_Role
      FROM
            dbo.Client_App_Access_Role car
            INNER JOIN dbo.Client_App_Access_Role_Group_Info_Map caargim
                  ON car.Client_App_Access_Role_Id = caargim.Client_App_Access_Role_Id
            INNER JOIN dbo.Code c
                  ON c.Code_Id = car.App_Access_Role_Type_Cd
      WHERE
            caargim.Group_Info_Id = @Group_Info_Id
            AND car.Client_Id = @Client_Id
            
END;


;
GO
GRANT EXECUTE ON  [dbo].[Client_App_Access_Role_Group_Info_Map_Sel_By_Group_Info_Id] TO [CBMSApplication]
GO
