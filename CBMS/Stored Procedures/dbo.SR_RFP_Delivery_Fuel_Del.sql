SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[SR_RFP_Delivery_Fuel_Del]  
     
DESCRIPTION: 
	It Deletes SR RFP Delivery Fuel for Selected 
						SR RFP Delivery Fuel Id.
      
INPUT PARAMETERS:          
NAME							DATATYPE	DEFAULT		DESCRIPTION          
--------------------------------------------------------------------
@SR_RFP_Delivery_Fuel_Id		INT	

   
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  
	Begin Tran
		EXEC SR_RFP_Delivery_Fuel_Del  3995
	Rollback Tran
    
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR			PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR		    31-MAY-10	CREATED     

*/  

CREATE PROCEDURE dbo.SR_RFP_Delivery_Fuel_Del
   (
    @SR_RFP_Delivery_Fuel_Id INT
   )
AS 
BEGIN

    SET NOCOUNT ON;
   
    DELETE 
   	FROM	
		dbo.SR_RFP_DELIVERY_FUEL
	WHERE 
		SR_RFP_DELIVERY_FUEL_ID = @SR_RFP_Delivery_Fuel_Id
END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_Delivery_Fuel_Del] TO [CBMSApplication]
GO
