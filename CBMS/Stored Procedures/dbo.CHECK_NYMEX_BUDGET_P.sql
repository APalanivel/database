SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE    PROCEDURE dbo.CHECK_NYMEX_BUDGET_P

@userId varchar(10),
@sessionId varchar(20),
@clientId int,
@siteId int,
@startDate dateTime,
@enddate datetime

as
begin
	set nocount on
 	select 	budget.division_id DIVISION,budget.SITE_ID SITE  
	from 	RM_BUDGET budget,
		RM_budget_site_map map
		where IS_BUDGET_APPROVED=1
	and (
		(BUDGET_START_MONTH>=@startDate and BUDGET_START_MONTH <=@endDate)
		or 			
		(BUDGET_END_MONTH >= @startDate and BUDGET_END_MONTH <=@endDate)
	    )

	and client_id=@clientId
	and map.rm_budget_id=budget.rm_budget_id
	and map.site_id=@siteId
end
GO
GRANT EXECUTE ON  [dbo].[CHECK_NYMEX_BUDGET_P] TO [CBMSApplication]
GO
