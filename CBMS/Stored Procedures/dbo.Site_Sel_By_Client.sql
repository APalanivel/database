SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:      
 dbo.Site_Sel_By_Client    
    
DESCRIPTION:  
  
   Selects the site name(combination city, state , address and actual sitename)  for the given client.  
   
INPUT PARAMETERS:  
 Name				DataType  Default   Description      
------------------------------------------------------------------------------      
 @Client_id			 INT  
 @division_id		 INT		  NULL  
 @Keyword			VARCHAR(100)  NULL  
 @StartRecordNumber	 INT			1  
 @EndRecordNumber	 INT          2147483647  
 @Account_Id		 INT		  NULL  
 @Site_State_Id		 INT		  NULL  
 @Site_Managed		 INT		  NULL  
  
OUTPUT PARAMETERS:      
 Name    DataType  Default   Description      
------------------------------------------------------------------------------      
  
USAGE EXAMPLES:  
------------------------------------------------------------------------------      
  
EXEC dbo.Site_Sel_By_Client 11552
EXEC dbo.Site_Sel_By_Client 11554, null, 'O2 World'
EXEC dbo.Site_Sel_By_Client 11554, null, 'Berlin'

EXEC dbo.Site_Ref_Number_Search_By_Client 235
EXEC dbo.Site_Sel_By_Client 235, null, null, 1, 100, null, '0001', 0
EXEC dbo.Site_Sel_By_Client 235, null, null, 1, 100, null, null, 0

EXEC Site_Sel_By_Client
    @Client_Id = 235
    , @division_id = null
    , @Keyword = N''
    , @StartRecordNumber = 1
    , @EndRecordNumber = 100
    , @Account_Id = NULL
    , @Site_State_Id = NULL
    , @Site_Managed = 0

AUTHOR INITIALS:  
  
Initials  Name   
------------------------------------------------------------  
HG		 Hari     
BCH		Balaraju  
CPE    Chaitanya Panduga Eswara  
RKV    Ravi Kumar Vegesna  
NR     Narayana Reddy  
PRG    Prasanna R Gachinmani 
SP     Srinivas Patchava 
    
MODIFICATIONS    
     
Initials Date		Modification      
------------------------------------------------------------      
HG		8/25/2009	created  
SKA		23-sep-09	added nolock for site  
KH		2/17/2010	added param division_id  
DR		2/23/2010	Remove Site table as all data is available from the view.  
DMR		09/10/2010  Modified for Quoted_Identifier  
BCH		2012-03-16  Used core.client_hier table instead of vwSiteName and added client_hier_id to select list.  
CPE		2014-02-19  MAINT-2423 Added new parameters to ensure that only following sites are listed:  
					--Site should not be the same as the one from which the account is being moved from  
				    -- Sites should not have the account being moved  
					--  Sites that belong to the same state and client as that of the old site  
HG		2014-03-17	MAINT-2669, Modified to apply the keyword filter on site_name, state_name and city as site name returned by this sproc is combination of all 3 columns  
RKV     2014-10-31  Modified to apply the keyword filter on Site_Reference_Number.  
NR		2016-04-11	MAINT-3782(3940) Added @Site_State_Id is optional parameter.  
PRG		2019-04-08	Added Optional Parameter @Site_Managed, for filtering Managed Sites Which is optional Parameter  
RKV		2019-05-27  Added optional parameter @Rate_Id  
SP		2019-07-18  MAINT-9004 Added Left join replace of inner join for the show the sites

******/
CREATE PROCEDURE [dbo].[Site_Sel_By_Client]
    (
        @Client_Id INT
        , @division_id INT = NULL
        , @Keyword VARCHAR(100) = NULL
        , @StartRecordNumber INT = 1
        , @EndRecordNumber INT = 2147483647
        , @Account_Id INT = NULL
        , @Site_Reference_Number VARCHAR(30) = NULL
        , @Site_State_Id INT = NULL
        , @Site_Managed INT = NULL
        , @Rate_Id INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @State_Id INT
            , @Old_Client_Hier_Id INT
            , @Account_Number VARCHAR(50)
            , @Total_Cnt INT;

        IF @Account_Id IS NOT NULL
            BEGIN
                SELECT
                    @Old_Client_Hier_Id = cha.Client_Hier_Id
                    , @Account_Number = cha.Account_Number
                FROM
                    Core.Client_Hier_Account cha
            WHERE
                    cha.Account_Id = @Account_Id;

                SELECT
                    @State_Id = State_Id
                FROM
                    Core.Client_Hier
                WHERE
                    Client_Hier_Id = @Old_Client_Hier_Id;
            END;
        WITH CTE_Site
        AS (
               SELECT   TOP (@EndRecordNumber)
                        ch.Site_Id
                        , ch.Client_Hier_Id
                        , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
                        , ch.Sitegroup_Id AS Division_Id
                        , ROW_NUMBER() OVER (ORDER BY
                                                 Site_name ASC) AS Record_Number
                        , ch.Site_Reference_Number
               FROM
                    Core.Client_Hier ch
                    LEFT JOIN Core.Client_Hier_Account AS cha
                        ON cha.Client_Hier_Id = ch.Client_Hier_Id
               WHERE
                    ch.Client_Id = @Client_Id
                    AND ch.Site_Id > 0
                    AND (   @Site_Managed IS NULL
                            OR  ch.Site_Not_Managed = @Site_Managed)
                    AND (   @division_id IS NULL
                            OR  ch.Sitegroup_Id = @division_id)
                    AND (   @Old_Client_Hier_Id IS NULL
                            OR  (   ch.Client_Hier_Id <> @Old_Client_Hier_Id
                                    AND ch.State_Id = @State_Id))
                    AND (   @Keyword IS NULL
                            OR  ch.Site_name LIKE '%' + @Keyword + '%'
                            OR  ch.State_Name LIKE '%' + @Keyword + '%'
                            OR  ch.City LIKE '%' + @Keyword + '%')
                    AND (   @Site_Reference_Number IS NULL
                            OR  ch.Site_Reference_Number = @Site_Reference_Number)
                    AND (   @Rate_Id IS NULL
                            OR  cha.Rate_Id = @Rate_Id)
                    AND (   @Site_State_Id IS NULL
                            OR  ch.State_Id = @Site_State_Id)
               GROUP BY
                   ch.Site_Id
                   , ch.Client_Hier_Id
                   , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
                   , ch.Sitegroup_Id
                   , ch.Site_name
                   , ch.Site_Reference_Number
           )
        SELECT
            cs.Client_Hier_Id
            , cs.Site_Id
            , cs.Site_name
            , cs.Division_Id
            , cs.Site_Reference_Number
        FROM
            CTE_Site AS cs
        WHERE
            cs.Record_Number BETWEEN @StartRecordNumber
                             AND     @EndRecordNumber
        ORDER BY
            cs.Site_name;

    END;

    ;

GO





GRANT EXECUTE ON  [dbo].[Site_Sel_By_Client] TO [CBMSApplication]
GO
