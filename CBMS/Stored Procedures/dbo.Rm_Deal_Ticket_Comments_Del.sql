SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: DBO.Rm_Deal_Ticket_Comments_Del  

DESCRIPTION: It Deletes Rm Deal Ticket Comments for Selected Rm Deal Ticket Comments Id.
      
INPUT PARAMETERS:          
	NAME							DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Rm_Deal_Ticket_Comments_Id		INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN
		EXEC dbo.Rm_Deal_Ticket_Comments_del 15		
	ROLLBACK TRAN
	
	SELECT 
		*
	FROM
		dbo.Rm_Deal_Ticket_Comments 
	WHERE
		Rm_Deal_Ticket_Comments_Id = 15

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			31-AUG-10	CREATED

*/

CREATE PROCEDURE dbo.Rm_Deal_Ticket_Comments_Del
    (
      @Rm_Deal_Ticket_Comments_Id	 INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.Rm_Deal_Ticket_Comments
	WHERE
		Rm_Deal_Ticket_Comments_Id = @Rm_Deal_Ticket_Comments_Id
END
GO
GRANT EXECUTE ON  [dbo].[Rm_Deal_Ticket_Comments_Del] TO [CBMSApplication]
GO
