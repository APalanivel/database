SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                                      
 NAME: dbo.CU_Invoice_Determinant_Charges_Commodity_Buckets_Override                                          
                                                      
 DESCRIPTION:                                                      
   To get the details of invoice collection account level Frequency_And_Expected_Dtl_Sel config                                           
                                                      
 INPUT PARAMETERS:                                        
                                                   
 Name              DataType         Default       Description                                      
---------------------------------------------------------------------------------------------------------------                                    
@Invoice_Id   INT                                 
                                                            
 OUTPUT PARAMETERS:                                        
                                                         
 Name                        DataType         Default       Description                                      
---------------------------------------------------------------------------------------------------------------                                    
                                                      
 USAGE EXAMPLES:                                                          
---------------------------------------------------------------------------------------------------------------                           

SELECT * FROM dbo.CU_INVOICE_DETERMINANT WHERE CU_INVOICE_ID = 93899261
SELECT * FROM dbo.CU_INVOICE_CHARGE WHERE CU_INVOICE_ID = 93899261


BEGIN TRAN

	EXEC [CU_Invoice_Determinant_Charges_Commodity_Buckets_Override] 93899261,16  

	SELECT * FROM dbo.CU_INVOICE_DETERMINANT WHERE CU_INVOICE_ID = 93899261
	SELECT * FROM dbo.CU_INVOICE_CHARGE WHERE CU_INVOICE_ID = 93899261

ROLLBACK
   
 AUTHOR INITIALS:                                      
                                     
 Initials              Name                                      
---------------------------------------------------------------------------------------------------------------                                                    
 HKT                  Harish Kumar Tirumandyam                              
                                                       
 MODIFICATIONS:                                    
                                        
 Initials   Date             Modification                                    
---------------------------------------------------------------------------------------------------------------                                    
 HKT		2020-03-13		Created For Data purple.                                       
 HG			2020-04-27		D20-1967, Modified the procedure to consider the scenario when the client doesn't have any configuration to use the default service mapping defiined at the UBM level.
******/
CREATE PROCEDURE [dbo].[CU_Invoice_Determinant_Charges_Commodity_Buckets_Override]
    (
        @Invoice_Id   INT
       ,@user_Info_Id INT = NULL
    )
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @Updated TABLE
        (
            charge_or_determinant_id INT           NULL
           ,old_commodity_name       VARCHAR (50)  NULL
           ,new_commodity_name       VARCHAR (50)  NULL
           ,CU_DETERMINANT_CODE      VARCHAR (200) NULL
           ,old_Bucket_Master_Name   VARCHAR (255) NULL
           ,new_Bucket_Master_Name   VARCHAR (255) NULL
           ,old_UNIT_OF_MEASURE_TYPE VARCHAR (200) NULL
           ,new_UNIT_OF_MEASURE_TYPE VARCHAR (200) NULL
        );

    DECLARE
        @user_id   INT = @user_Info_Id
       ,@Client_Id INT
       ,@Ubm_Id    INT;

    CREATE TABLE #Override_Commodity
        (
            Cu_Invoice_Determinant_Charge_Id INT
           ,Data_Type                        VARCHAR (20)
           ,Old_Commodity_Id                 INT
           ,Override_Commodity_Id            INT
        );

    SELECT
        @Client_Id = CLIENT_ID
       ,@Ubm_Id = UBM_ID
    FROM
        dbo.CU_INVOICE
    WHERE
        CU_INVOICE_ID = @Invoice_Id;

    SELECT @user_id = USER_INFO.USER_INFO_ID
    FROM
        dbo.USER_INFO
    WHERE
        USER_INFO.USERNAME = 'Conversion'
        AND @user_id IS NULL;

    INSERT INTO #Override_Commodity
    (
        Cu_Invoice_Determinant_Charge_Id
       ,Data_Type
       ,Old_Commodity_Id
       ,Override_Commodity_Id
    )
    SELECT
        cid.CU_INVOICE_DETERMINANT_ID
       ,'Determinant'
       ,sm.COMMODITY_TYPE_ID
       ,cisoc.Override_Commodity_Id
    FROM
        dbo.Client_Invoice_Service_Override_Config AS cisoc
        INNER JOIN dbo.UBM_SERVICE_MAP AS sm
            ON sm.UBM_ID = cisoc.UBM_Id
               AND sm.UBM_SERVICE_CODE = cisoc.Ubm_Service_Code
        INNER JOIN dbo.CU_INVOICE_DETERMINANT AS cid
            ON cid.UBM_SERVICE_CODE = sm.UBM_SERVICE_CODE
    WHERE
        cid.CU_INVOICE_ID = @Invoice_Id
        AND cisoc.UBM_Id = @Ubm_Id
        AND cisoc.Client_Id = @Client_Id
        AND ISNULL(cid.COMMODITY_TYPE_ID, -1) <> cisoc.Override_Commodity_Id;

    INSERT INTO #Override_Commodity
    (
        Cu_Invoice_Determinant_Charge_Id
       ,Data_Type
       ,Old_Commodity_Id
       ,Override_Commodity_Id
    )
    SELECT
        cic.CU_INVOICE_CHARGE_ID
       ,'Charge'
       ,sm.COMMODITY_TYPE_ID
       ,cisoc.Override_Commodity_Id
    FROM
        dbo.Client_Invoice_Service_Override_Config AS cisoc
        INNER JOIN dbo.UBM_SERVICE_MAP AS sm
            ON sm.UBM_ID = cisoc.UBM_Id
               AND sm.UBM_SERVICE_CODE = cisoc.Ubm_Service_Code
        INNER JOIN dbo.CU_INVOICE_CHARGE AS cic
            ON cic.UBM_SERVICE_CODE = sm.UBM_SERVICE_CODE
    WHERE
        cic.CU_INVOICE_ID = @Invoice_Id
        AND cisoc.UBM_Id = @Ubm_Id
        AND cisoc.Client_Id = @Client_Id
        AND ISNULL(cic.COMMODITY_TYPE_ID, -1) <> cisoc.Override_Commodity_Id;

    -- For the clients who doesn't have override configuration for the current ubm_service_code 
    INSERT INTO #Override_Commodity
    (
        Cu_Invoice_Determinant_Charge_Id
       ,Data_Type
       ,Old_Commodity_Id
       ,Override_Commodity_Id
    )
    SELECT
        cid.CU_INVOICE_DETERMINANT_ID
       ,'Determinant'
       ,cid.COMMODITY_TYPE_ID
       ,sm.COMMODITY_TYPE_ID
    FROM
        dbo.CU_INVOICE_DETERMINANT cid
        INNER JOIN dbo.UBM_SERVICE_MAP sm
            ON sm.UBM_SERVICE_CODE = cid.UBM_SERVICE_CODE
    WHERE
        cid.CU_INVOICE_ID = @Invoice_Id
        AND sm.UBM_ID = @Ubm_Id
        AND NOT EXISTS (
                           SELECT 1
                           FROM
                               dbo.Client_Invoice_Service_Override_Config cisoc
                           WHERE
                               cisoc.Client_Id = @Client_Id
                               AND cisoc.UBM_Id = sm.UBM_ID
                               AND cisoc.Ubm_Service_Code = sm.UBM_SERVICE_CODE
                       )
        AND sm.COMMODITY_TYPE_ID <> ISNULL(cid.COMMODITY_TYPE_ID, -1);

    INSERT INTO #Override_Commodity
    (
        Cu_Invoice_Determinant_Charge_Id
       ,Data_Type
       ,Old_Commodity_Id
       ,Override_Commodity_Id
    )
    SELECT
        cic.CU_INVOICE_CHARGE_ID
       ,'Charge'
       ,cic.COMMODITY_TYPE_ID
       ,sm.COMMODITY_TYPE_ID
    FROM
        dbo.CU_INVOICE_CHARGE cic
        INNER JOIN dbo.UBM_SERVICE_MAP sm
            ON sm.UBM_SERVICE_CODE = cic.UBM_SERVICE_CODE
    WHERE
        cic.CU_INVOICE_ID = @Invoice_Id
        AND sm.UBM_ID = @Ubm_Id
        AND NOT EXISTS (
                           SELECT 1
                           FROM
                               dbo.Client_Invoice_Service_Override_Config cisoc
                           WHERE
                               cisoc.Client_Id = @Client_Id
                               AND cisoc.UBM_Id = sm.UBM_ID
                               AND cisoc.Ubm_Service_Code = sm.UBM_SERVICE_CODE
                       )
        AND sm.COMMODITY_TYPE_ID <> ISNULL(cic.COMMODITY_TYPE_ID, 0); -- Using 0 as -1 Commodity Id is possible in charge

    BEGIN TRY
        BEGIN TRAN;

        BEGIN

            UPDATE cid
            SET
                cid.COMMODITY_TYPE_ID = oc.Override_Commodity_Id
               ,cid.Bucket_Master_Id = ubdm.Bucket_Master_Id
               ,cid.UNIT_OF_MEASURE_TYPE_ID = uumm.UNIT_OF_MEASURE_TYPE_ID
               ,cid.EC_Invoice_Sub_Bucket_Master_Id = NULL
            OUTPUT
                934
               ,old_commodity.Commodity_Name
               ,new_commodity.Commodity_Name
               ,Inserted.CU_DETERMINANT_CODE
               ,old_bucket_master.Bucket_Name
               ,new_bucket_master.Bucket_Name
               ,old_unit_of_measure_type.ENTITY_NAME
               ,new_unit_of_measure_type.ENTITY_NAME
            INTO @Updated (
                              charge_or_determinant_id
                             ,old_commodity_name
                             ,new_commodity_name
                             ,CU_DETERMINANT_CODE
                             ,old_Bucket_Master_Name
                             ,new_Bucket_Master_Name
                             ,old_UNIT_OF_MEASURE_TYPE
                             ,new_UNIT_OF_MEASURE_TYPE
                          )
            FROM
                dbo.CU_INVOICE_DETERMINANT AS cid
                INNER JOIN dbo.CU_INVOICE AS ci
                    ON ci.CU_INVOICE_ID = cid.CU_INVOICE_ID
                INNER JOIN #Override_Commodity oc
                    ON oc.Cu_Invoice_Determinant_Charge_Id = cid.CU_INVOICE_DETERMINANT_ID
                       AND oc.Data_Type = 'Determinant'
                INNER JOIN dbo.Commodity old_commodity
                    ON old_commodity.Commodity_Id = oc.Old_Commodity_Id
                INNER JOIN dbo.Commodity new_commodity
                    ON new_commodity.Commodity_Id = oc.Override_Commodity_Id
                LEFT JOIN dbo.UBM_BUCKET_DETERMINANT_MAP AS ubdm
                    ON ubdm.COMMODITY_TYPE_ID = oc.Override_Commodity_Id
                       AND ubdm.UBM_ID = @Ubm_Id
                       AND ubdm.UBM_BUCKET_CODE = cid.UBM_BUCKET_CODE
                LEFT JOIN dbo.UBM_UNIT_OF_MEASURE_MAP AS uumm
                    ON uumm.UBM_ID = @Ubm_Id
                       AND uumm.COMMODITY_TYPE_ID = oc.Override_Commodity_Id
                       AND uumm.UBM_UNIT_OF_MEASURE_CODE = cid.UBM_UNIT_OF_MEASURE_CODE
                LEFT JOIN dbo.Bucket_Master old_bucket_master
                    ON old_bucket_master.Bucket_Master_Id = cid.Bucket_Master_Id
                LEFT JOIN dbo.Bucket_Master new_bucket_master
                    ON new_bucket_master.Bucket_Master_Id = ubdm.Bucket_Master_Id
                LEFT JOIN dbo.ENTITY old_unit_of_measure_type
                    ON old_unit_of_measure_type.ENTITY_ID = cid.UNIT_OF_MEASURE_TYPE_ID
                LEFT JOIN dbo.ENTITY new_unit_of_measure_type
                    ON new_unit_of_measure_type.ENTITY_ID = uumm.UNIT_OF_MEASURE_TYPE_ID
            WHERE
                ci.CU_INVOICE_ID = @Invoice_Id
                AND ISNULL(cid.COMMODITY_TYPE_ID, -1) <> oc.Override_Commodity_Id;

            UPDATE cic
            SET
                cic.COMMODITY_TYPE_ID = oc.Override_Commodity_Id
               ,cic.Bucket_Master_Id = ubcm.Bucket_Master_Id
               ,cic.EC_Invoice_Sub_Bucket_Master_Id = NULL
            OUTPUT
                933
               ,old_commodity.Commodity_Name
               ,new_commodity.Commodity_Name
               ,Inserted.CU_DETERMINANT_CODE
               ,old_bucket_master.Bucket_Name
               ,new_bucket_master.Bucket_Name
            INTO @Updated (
                              charge_or_determinant_id
                             ,old_commodity_name
                             ,new_commodity_name
                             ,CU_DETERMINANT_CODE
                             ,old_Bucket_Master_Name
                             ,new_Bucket_Master_Name
                          )
            FROM
                dbo.CU_INVOICE_CHARGE AS cic
                INNER JOIN dbo.CU_INVOICE AS ci
                    ON ci.CU_INVOICE_ID = cic.CU_INVOICE_ID
                INNER JOIN #Override_Commodity oc
                    ON oc.Cu_Invoice_Determinant_Charge_Id = cic.CU_INVOICE_CHARGE_ID
                       AND oc.Data_Type = 'Charge'
                INNER JOIN dbo.Commodity old_commodity
                    ON old_commodity.Commodity_Id = oc.Old_Commodity_Id
                INNER JOIN dbo.Commodity new_commodity
                    ON new_commodity.Commodity_Id = oc.Override_Commodity_Id
                LEFT JOIN dbo.UBM_BUCKET_CHARGE_MAP AS ubcm
                    ON ubcm.COMMODITY_TYPE_ID = oc.Override_Commodity_Id
                       AND ubcm.UBM_ID = @Ubm_Id
                       AND ubcm.UBM_BUCKET_CODE = cic.UBM_BUCKET_CODE
                LEFT JOIN dbo.Bucket_Master old_bucket_master
                    ON old_bucket_master.Bucket_Master_Id = cic.Bucket_Master_Id
                LEFT JOIN dbo.Bucket_Master new_bucket_master
                    ON new_bucket_master.Bucket_Master_Id = ubcm.Bucket_Master_Id
            WHERE
                ci.CU_INVOICE_ID = @Invoice_Id
                AND ISNULL(cic.COMMODITY_TYPE_ID, -1) <> oc.Override_Commodity_Id;

            -- insert into Invoice Log ... @Updated Table Values

            -- Commodity                    
            INSERT INTO dbo.CU_INVOICE_CHANGE_LOG
            (
                CU_INVOICE_ID
               ,CHANGE_TYPE_ID
               ,FIELD_TYPE_ID
               ,FIELD_NAME
               ,PREVIOUS_VALUE
               ,CURRENT_VALUE
               ,CHANGE_DATE
               ,Bucket_Number
               ,Updated_User_Id
            )
            SELECT
                @Invoice_Id
               ,932
               ,U.charge_or_determinant_id
               ,'Service override'
               ,U.old_commodity_name
               ,U.new_commodity_name
               ,GETDATE()
               ,U.CU_DETERMINANT_CODE
               ,@user_id
            FROM
                @Updated AS U;


            -- Bucket Master 	
            INSERT INTO dbo.CU_INVOICE_CHANGE_LOG
            (
                CU_INVOICE_ID
               ,CHANGE_TYPE_ID
               ,FIELD_TYPE_ID
               ,FIELD_NAME
               ,PREVIOUS_VALUE
               ,CURRENT_VALUE
               ,CHANGE_DATE
               ,Bucket_Number
               ,Updated_User_Id
            )
            SELECT
                @Invoice_Id
               ,932
               ,U.charge_or_determinant_id
               ,'Service override'
               ,U.old_Bucket_Master_Name
               ,U.new_Bucket_Master_Name
               ,GETDATE()
               ,U.CU_DETERMINANT_CODE
               ,@user_id
            FROM
                @Updated AS U
            WHERE
                U.old_Bucket_Master_Name IS NOT NULL
                AND U.new_Bucket_Master_Name IS NULL;

            -- Unit of measure UOM 
            INSERT INTO dbo.CU_INVOICE_CHANGE_LOG
            (
                CU_INVOICE_ID
               ,CHANGE_TYPE_ID
               ,FIELD_TYPE_ID
               ,FIELD_NAME
               ,PREVIOUS_VALUE
               ,CURRENT_VALUE
               ,CHANGE_DATE
               ,Bucket_Number
               ,Updated_User_Id
            )
            SELECT
                @Invoice_Id
               ,932
               ,U.charge_or_determinant_id
               ,'Service override'
               ,U.old_UNIT_OF_MEASURE_TYPE
               ,U.new_UNIT_OF_MEASURE_TYPE
               ,GETDATE()
               ,U.CU_DETERMINANT_CODE
               ,@user_id
            FROM
                @Updated AS U
            WHERE
                U.old_UNIT_OF_MEASURE_TYPE IS NOT NULL
                AND U.new_UNIT_OF_MEASURE_TYPE IS NULL;

            INSERT INTO dbo.CU_INVOICE_EVENT
            (
                CU_INVOICE_ID
               ,EVENT_DATE
               ,EVENT_BY_ID
               ,EVENT_DESCRIPTION
            )
            SELECT
                @Invoice_Id
               ,GETDATE()
               ,@user_id
               ,'Service Override Configuration Found - ' + old_commodity_name + ' to ' + new_commodity_name
            FROM
                @Updated
            GROUP BY 'Service Override Configuration Found - ' + old_commodity_name + ' to ' + new_commodity_name;

        END;

        COMMIT TRAN;
    END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0
        BEGIN
            ROLLBACK TRAN;
        END;

        EXEC dbo.usp_RethrowError;
    END CATCH;

    DROP TABLE #Override_Commodity;
END;



GO
GRANT EXECUTE ON  [dbo].[CU_Invoice_Determinant_Charges_Commodity_Buckets_Override] TO [CBMSApplication]
GO
