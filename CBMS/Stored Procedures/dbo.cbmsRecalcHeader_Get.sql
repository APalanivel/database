SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE     PROCEDURE [dbo].[cbmsRecalcHeader_Get]
	( @MyAccountId int
	, @recalc_header_id int
	)
AS
BEGIN

   select recalc_header_id
	, cu_invoice_id
	, account_id
	, contract_id
	, date_performed
	, grand_total
     from recalc_header with (nolock)
    where recalc_header_id = @recalc_header_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsRecalcHeader_Get] TO [CBMSApplication]
GO
