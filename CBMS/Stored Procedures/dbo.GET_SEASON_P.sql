
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******    
NAME: dbo.GET_SEASON_P
    
DESCRIPTION:     
    
	Get the Season details of non-term parent types
  
INPUT PARAMETERS:        
	Name			DataType       Default   Description        
--------------------------------------------------------------------------------        
	@rateScheduleId INT

OUTPUT PARAMETERS:        
      Name                DataType          Default     Description        
------------------------------------------------------------        
    
USAGE EXAMPLES:    
------------------------------------------------------------    
BEGIN TRAN    
    
 Exec dbo.GET_SEASON_P 1
  
ROLLBACK TRAN    
    
AUTHOR INITIALS:    
 Initials Name  
------------------------------------------------------------  
 CPE  Chaitanya Panduga Eshwar  
 JK   Jignesh Kumar
  
MODIFICATIONS  
  
 Initials	Date		Modification  
------------------------------------------------------------    
 CPE		2012-07-30	Modified to return only seasons having parents other than terms.
 JK			2012-08-17  Modified Objects Names in Capitalized as per Summit standards.  
  
******/  

CREATE PROCEDURE [dbo].[GET_SEASON_P] @rateScheduleId INT
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE @Term_Type_Id INT
	
      SELECT
            @Term_Type_Id = ent.ENTITY_ID
      FROM
            dbo.ENTITY ent
      WHERE
            ent.ENTITY_NAME = 'Time Of Use Schedule Term'
            AND ent.ENTITY_TYPE = 120
	
      SELECT
            SEASON_ID
           ,SEASON_NAME
           ,SEASON_FROM_DATE
           ,SEASON_TO_DATE
           ,SEASON_PARENT_ID
           ,SEASON_PARENT_TYPE_ID
      FROM
            dbo.SEASON
      WHERE
            SEASON_PARENT_ID = @rateScheduleId
            AND SEASON_PARENT_TYPE_ID <> @Term_Type_Id
      ORDER BY
            SEASON_FROM_DATE

END


;
GO

GRANT EXECUTE ON  [dbo].[GET_SEASON_P] TO [CBMSApplication]
GO
