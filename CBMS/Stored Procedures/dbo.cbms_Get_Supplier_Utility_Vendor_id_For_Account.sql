SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
NAME:          
	[DBO].[cbms_Get_Supplier_Utility_Vendor_id_For_Account]  
     
DESCRIPTION:       
   
Procedure to get Supplier's Utility vendor ID 
      
INPUT PARAMETERS:          
NAME			DATATYPE		DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@account_id		INT			
                
OUTPUT PARAMETERS:          
NAME			DATATYPE		DEFAULT		DESCRIPTION          
------------------------------------------------------------          

USAGE EXAMPLES:          
------------------------------------------------------------ 

EXEC cbms_Get_Supplier_Utility_Vendor_id_For_Account  115965
     
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
SSR			Sharad Srivastava
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
SSR			05/21/2010	Created
SSR			06/03/2010	Added Column Alias for Util_acc.vendor_id as utility_id
SSR			07/09/2010  Removed the logic of data reterival from SAMM table
							(( map.meter_disassociation_date > a.Supplier_Account_Begin_Dt  
								OR map.meter_disassociation_date IS NULL ))
							(After contract enhancement application is populating both Meter_association_date and meter_disassociation_Date column supplier_Account_meter_map and removing the entries when ever is_history = 1)
*/  
CREATE PROCEDURE dbo.cbms_Get_Supplier_Utility_Vendor_id_For_Account ( @account_id INT )
AS 
BEGIN  
  
      SET NOCOUNT ON ;  
  
      SELECT
            map.account_id
           ,a.ACCOUNT_TYPE_ID
           ,a.ACCOUNT_NUMBER
           ,a.SITE_ID
           ,Util_acc.vendor_id AS utility_id
           ,map.Contract_ID
           ,a.Supplier_Account_Begin_Dt
           ,a.Supplier_Account_End_Dt
      FROM
            dbo.supplier_account_meter_map AS map
            JOIN dbo.ACCOUNT a
				ON a.ACCOUNT_ID = map.ACCOUNT_ID
            JOIN dbo.METER m
				ON m.METER_ID = map.METER_ID
            JOIN dbo.account Util_acc
				ON Util_acc.account_id = m.ACCOUNT_ID
            JOIN dbo.VENDOR v
				ON v.VENDOR_ID = Util_acc.VENDOR_ID
      WHERE
            map.ACCOUNT_ID = @Account_Id
      GROUP BY
            map.account_id
           ,a.ACCOUNT_TYPE_ID
           ,a.ACCOUNT_NUMBER
           ,a.SITE_ID
           ,Util_acc.vendor_id
           ,map.Contract_ID
           ,a.Supplier_Account_Begin_Dt
           ,a.Supplier_Account_End_Dt  	
	
END  

GO
GRANT EXECUTE ON  [dbo].[cbms_Get_Supplier_Utility_Vendor_id_For_Account] TO [CBMSApplication]
GO
