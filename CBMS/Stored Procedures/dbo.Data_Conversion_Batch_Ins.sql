SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********               
        
NAME:        
     dbo.Data_Conversion_Batch_Ins          
               
DESCRIPTION:                  
           
              
 INPUT PARAMETERS:                  
                             
 Name                        DataType         Default       Description                
---------------------------------------------------------------------------------------------------------------              
 @File_Name                  VARCHAR(200)        
 @Conversion_Type            VARCHAR(25)        
                  
 OUTPUT PARAMETERS:                  
                                   
 Name                        DataType         Default       Description                
---------------------------------------------------------------------------------------------------------------              
                  
 USAGE EXAMPLES:                                    
---------------------------------------------------------------------------------------------------------------                                    
         
 BEGIN TRAN        
 EXEC dbo.Data_Conversion_Batch_Ins 'D:\Test.xls'         
 ROLLBACK TRAN           
          
              
 AUTHOR INITIALS:                
               
 Initials              Name                
---------------------------------------------------------------------------------------------------------------                              
 AKR                    Ashok Kumar Raju     
 ABK					Aditya Bharadwaj K                  
          
 MODIFICATIONS:              
                  
 Initials	Date    Modification              
---------------------------------------------------------------------------------------------------------------              
 AKR		2014-06-01	Created.        
 HG			2017-03-10	Modified to use the temp table created with in SSIS for account conversion.
 RR			2017-05-08	SE2017-32 - New template for DMO supplier accounts conversion
 ABK		2018-08-21	DDCR-35 - Added IF condition for new Meter number update package.
 ABK		2018-09-05  DDCR-36 - Added IF condition for Site Name & Site reference number Update package
 ABK		2018-10-08	DDCR-34 - Added IF condition for updating Sites to New division package
*********/

CREATE PROCEDURE [dbo].[Data_Conversion_Batch_Ins]
    (
        @File_Name       VARCHAR(200),
        @Conversion_Type VARCHAR(100) = 'Account'
    )
AS
    BEGIN

        DECLARE
            @Email_Address VARCHAR(1000) = '',
            @Identity      INT;

        IF (@Conversion_Type = 'Account')
            BEGIN
                SELECT
                    @Email_Address = @Email_Address + cca.Email_Adddress + ';'
                FROM
                    #Data_Conversion_Accounts_Stage cca
                WHERE
                    cca.Email_Adddress IS NOT NULL
                GROUP BY
                    cca.Email_Adddress;

            END;

        IF (@Conversion_Type = 'DMOSupplierAccount')
            BEGIN
                SELECT
                    @Email_Address = @Email_Address + cca.Email_Adddress + ';'
                FROM
                    #DMO_Supplier_Account_Conversion_Stage cca
                WHERE
                    cca.Email_Adddress IS NOT NULL
                GROUP BY
                    cca.Email_Adddress;

            END;


        IF @Conversion_Type = 'Sitegroup_Site'
            BEGIN
                SELECT
                    @Email_Address = @Email_Address + cca.Email_Address + ';'
                FROM
                    ETL.Data_Conversion_Sitegroup_Site_Stage cca
                WHERE
                    cca.Email_Address IS NOT NULL
                GROUP BY
                    cca.Email_Address;

            END;



        IF (@Conversion_Type = 'UpdateMeterNumbers')
            BEGIN
                SELECT
                    @Email_Address = @Email_Address + cca.Email_Address + ';'
                FROM
                    #Update_Meter_Numbers_Stage cca
                WHERE
                    cca.Email_Address IS NOT NULL
                GROUP BY
                    cca.Email_Address;

            END;


        IF (@Conversion_Type = 'UpdateSiteNameAndSiteRef')
            BEGIN
                SELECT
                    @Email_Address = @Email_Address + cca.Email_Address + ';'
                FROM
                    #Update_SiteName_SiteRefNumbers_Stage cca
                WHERE
                    cca.Email_Address IS NOT NULL
                GROUP BY
                    cca.Email_Address;

            END;

		IF (@Conversion_Type = 'MoveSitesToNewDivision')
            BEGIN
                SELECT
                    @Email_Address = @Email_Address + cca.Email_Address + ';'
                FROM
                    #Update_Sites_To_New_Division_Stage cca
                WHERE
                    cca.Email_Address IS NOT NULL
                GROUP BY
                    cca.Email_Address;

            END;


        SELECT
            @Email_Address = LEFT(@Email_Address, LEN(@Email_Address) - 1);

        INSERT INTO ETL.Data_Conversion_Batch
            (
                File_Name,
                Conversion_Type,
                Batch_Start_Ts,
                Batch_End_Ts,
                Email_Address,
                Status
            )
        VALUES
            (
                @File_Name, @Conversion_Type, GETDATE(), NULL, @Email_Address, NULL
            );

        SELECT
            @Identity = SCOPE_IDENTITY();
        SELECT
            @Identity      Data_Conversion_Batch_Id,
            @Email_Address Email_Address;

    END;




GO



GRANT EXECUTE ON  [dbo].[Data_Conversion_Batch_Ins] TO [ETL_Execute]
GO
