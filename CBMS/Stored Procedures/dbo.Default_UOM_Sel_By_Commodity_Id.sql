SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
	CBMS.dbo.Default_UOM_Sel_By_Commodity_Id  

DESCRIPTION:  
	Used to get the default uom of the given commodity

INPUT PARAMETERS:  
	Name			DataType	Default		Description  
------------------------------------------------------------  
	@Commodity_Id	INT

OUTPUT PARAMETERS:  
	Name			DataType	Default		Description  
------------------------------------------------------------  


USAGE EXAMPLES:  
------------------------------------------------------------
	EXEC dbo.Default_UOM_Sel_By_Commodity_Id 290
	EXEC dbo.Default_UOM_Sel_By_Commodity_Id 291
	EXEC dbo.Default_UOM_Sel_By_Commodity_Id 67
	
AUTHOR INITIALS:  
	Initials	Name  
------------------------------------------------------------  
	RR			Raghu Reddy
	  
MODIFICATIONS   
	Initials	Date		Modification  
------------------------------------------------------------  
	RR			2012-02-06	Created
******/  
  
CREATE PROCEDURE dbo.Default_UOM_Sel_By_Commodity_Id @Commodity_Id INT
AS 
BEGIN  
  
      SET NOCOUNT ON  
  
      SELECT
            com.Commodity_Id
           ,com.Commodity_Name
           ,com.Default_UOM_Entity_Type_Id
           ,du.ENTITY_NAME Default_Uom_Name
      FROM
            dbo.Commodity com
            LEFT JOIN dbo.ENTITY du
                  ON du.ENTITY_ID = com.Default_UOM_Entity_Type_Id
      WHERE
            com.Commodity_Id = @Commodity_Id
END  

GO
GRANT EXECUTE ON  [dbo].[Default_UOM_Sel_By_Commodity_Id] TO [CBMSApplication]
GO
