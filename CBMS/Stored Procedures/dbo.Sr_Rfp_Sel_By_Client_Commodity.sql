SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
   dbo.Sr_Rfp_Sel_By_Client_Commodity  
  
DESCRIPTION:    
		
  
INPUT PARAMETERS:    
	Name					DataType			Default			Description    
-------------------------------------------------------------------------------- 
	@User_Info_Id			INT

OUTPUT PARAMETERS:    
	Name				DataType		Default			Description    
--------------------------------------------------------------------------------  
	@Client_Id			INT
    @Commodity_Id		INT
    @Contract_Start_Dt	DATE
    @Contract_End_Dt	DATE
    @Rfp_Id				VARCHAR(10)		NULL

USAGE EXAMPLES:    
--------------------------------------------------------------------------------  

	EXEC dbo.Sr_Rfp_Sel_By_Client_Commodity 1005,290,'2020-01-01','2020-12-01'
	EXEC dbo.Sr_Rfp_Sel_By_Client_Commodity 1005,290,'2019-06-01','2019-08-31',null

AUTHOR INITIALS:    
	Initials	Name    
--------------------------------------------------------------------------------  
	RR			Raghu Reddy  
  
MODIFICATIONS     
	Initials	Date			Modification    
--------------------------------------------------------------------------------  
	RR			2020-03-31		GRM-US EP & Renewables - Created

******/

CREATE PROCEDURE [dbo].[Sr_Rfp_Sel_By_Client_Commodity]
    (
        @Client_Id INT
        , @Commodity_Id INT
        , @Contract_Start_Dt DATE
        , @Contract_End_Dt DATE
        , @Meter_Ids VARCHAR(MAX) = NULL
        , @Contract_Id INT = NULL
    )
WITH RECOMPILE
AS
    BEGIN

        SET NOCOUNT ON;
        --SELECT @Meter_Ids  = NULL
        DECLARE @RFP AS TABLE
              (
                  SR_RFP_ID INT
                  , FROM_MONTH DATE
              );

        SELECT
            @Contract_Start_Dt = DATEADD(dd, -DATEPART(dd, @Contract_Start_Dt) + 1, @Contract_Start_Dt)
            , @Contract_End_Dt = DATEADD(dd, -DATEPART(dd, @Contract_End_Dt) + 1, @Contract_End_Dt);

        CREATE TABLE #Meter_Tbl
             (
                 Meter_Id INT
             );

        INSERT INTO #Meter_Tbl
             (
                 Meter_Id
             )
        SELECT
            CAST(us.Segments AS INT)
        FROM
            dbo.ufn_split(@Meter_Ids, ',') us
        WHERE
            @Meter_Ids IS NOT NULL
            AND @Contract_Id IS NULL;

        INSERT INTO #Meter_Tbl
             (
                 Meter_Id
             )
        SELECT
            cha.Meter_Id
        FROM
            dbo.CONTRACT c
            INNER JOIN Core.Client_Hier_Account cha
                ON c.CONTRACT_ID = cha.Supplier_Contract_ID
        WHERE
            c.CONTRACT_ID = @Contract_Id
            AND @Contract_Id IS NOT NULL
            AND @Meter_Ids IS NULL;

        INSERT INTO @RFP
             (
                 SR_RFP_ID
                 , FROM_MONTH
             )
        SELECT
            sr.SR_RFP_ID
            , MAX(srat.FROM_MONTH)
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.SR_RFP_ACCOUNT sra
                ON sra.ACCOUNT_ID = cha.Account_Id
            INNER JOIN dbo.SR_RFP sr
                ON sra.SR_RFP_ID = sr.SR_RFP_ID
            INNER JOIN dbo.SR_RFP_ACCOUNT_TERM srat
                ON sra.SR_RFP_ACCOUNT_ID = srat.SR_ACCOUNT_GROUP_ID
        WHERE
            ch.Client_Id = @Client_Id
            AND sr.COMMODITY_TYPE_ID = @Commodity_Id
            AND sra.SR_RFP_BID_GROUP_ID IS NULL
            AND srat.IS_BID_GROUP = 0
            AND @Contract_Start_Dt BETWEEN srat.FROM_MONTH
                                   AND     srat.TO_MONTH
            AND EXISTS (SELECT  1 FROM  #Meter_Tbl mt WHERE mt.Meter_Id = cha.Meter_Id)
        GROUP BY
            sr.SR_RFP_ID;

        INSERT INTO @RFP
             (
                 SR_RFP_ID
                 , FROM_MONTH
             )
        SELECT
            sr.SR_RFP_ID
            , MAX(srat.FROM_MONTH)
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.SR_RFP_ACCOUNT sra
                ON sra.ACCOUNT_ID = cha.Account_Id
            INNER JOIN dbo.SR_RFP sr
                ON sra.SR_RFP_ID = sr.SR_RFP_ID
            INNER JOIN dbo.SR_RFP_ACCOUNT_TERM srat
                ON sra.SR_RFP_BID_GROUP_ID = srat.SR_ACCOUNT_GROUP_ID
        WHERE
            ch.Client_Id = @Client_Id
            AND sr.COMMODITY_TYPE_ID = @Commodity_Id
            AND sra.SR_RFP_BID_GROUP_ID IS NOT NULL
            AND srat.IS_BID_GROUP = 1
            AND @Contract_Start_Dt BETWEEN srat.FROM_MONTH
                                   AND     srat.TO_MONTH
            AND EXISTS (SELECT  1 FROM  #Meter_Tbl mt WHERE mt.Meter_Id = cha.Meter_Id)
        GROUP BY
            sr.SR_RFP_ID;

        SELECT
            r.SR_RFP_ID
            , r.FROM_MONTH
        FROM
            @RFP r
        GROUP BY
            r.SR_RFP_ID
            , r.FROM_MONTH
        ORDER BY
            r.SR_RFP_ID DESC;

    END;

GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Sel_By_Client_Commodity] TO [CBMSApplication]
GO
