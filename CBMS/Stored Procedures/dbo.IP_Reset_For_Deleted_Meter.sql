SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******

NAME: dbo.IP_Reset_For_Deleted_Meter

DESCRIPTION: 

	This procedure used to reset the Invoice_participation and Invoice_participation_Site table if the disassociated meter 
	from account is the last one in the supplier account site
	

INPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
	@Account_Id		INT
	@Meter_Id		VARCHAR(max)			Comma seperated meter_id which are removed from the contract.

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	
	EXEC dbo.IP_Reset_For_Deleted_Meter
	@Account_Id = 1102


AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	RR			Raghu Reddy

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	RR			2017-03-06	Contract palaceholder - Created CP-8 
							Logic will same as dbo.IP_Reset_For_Deleted_Contract_Meter
*/

CREATE PROCEDURE [dbo].[IP_Reset_For_Deleted_Meter] ( @Account_Id INT )
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE
            @Site_Id INT
           ,@Service_Month DATE

      DECLARE @Sites_With_No_Meter_To_Account TABLE --> Last meter of the site removed from account
            ( 
             Supplier_Account_Id INT
            ,Site_Id INT
            ,PRIMARY KEY CLUSTERED ( Supplier_Account_Id, Site_Id ) )
            
      DECLARE @Sites_With_Meter_To_Account TABLE --> Meter(s) of the site associated to account
            ( 
             Supplier_Account_Id INT
            ,Site_Id INT
            ,PRIMARY KEY CLUSTERED ( Supplier_Account_Id, Site_Id ) )

      DECLARE @Invoice_Participation_List TABLE
            ( 
             Account_Id INT
            ,Site_Id INT
            ,Service_Month DATETIME
            ,PRIMARY KEY CLUSTERED ( Site_id, Service_Month, Account_Id ) )
	
      INSERT      INTO @Sites_With_Meter_To_Account
                  ( 
                   Supplier_Account_Id
                  ,Site_Id )
                  SELECT
                        samm.Account_Id
                       ,acc.SITE_ID
                  FROM
                        dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                        INNER JOIN dbo.METER mtr
                              ON samm.METER_ID = mtr.METER_ID
                        INNER JOIN dbo.ACCOUNT acc
                              ON mtr.ACCOUNT_ID = acc.ACCOUNT_ID
                  WHERE
                        samm.Account_Id = @Account_Id
                  GROUP BY
                        samm.Account_Id
                       ,acc.Site_Id	

      INSERT      INTO @Sites_With_No_Meter_To_Account
                  ( 
                   Supplier_Account_Id
                  ,Site_Id )
                  SELECT
                        ip.ACCOUNT_ID
                       ,ip.Site_Id
                  FROM
                        dbo.INVOICE_PARTICIPATION ip
                  WHERE
                        ip.ACCOUNT_ID = @Account_Id
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          @Sites_With_Meter_To_Account sma
                                         WHERE
                                          sma.Site_Id = ip.Site_Id
                                          AND sma.Supplier_Account_Id = ip.ACCOUNT_ID )
			
      INSERT      INTO @Invoice_Participation_List
                  ( 
                   Account_Id
                  ,Site_Id
                  ,Service_Month )
                  SELECT
                        ip.ACCOUNT_ID
                       ,d.Site_Id
                       ,ip.SERVICE_MONTH
                  FROM
                        dbo.Invoice_Participation ip
                        INNER JOIN @Sites_With_No_Meter_To_Account d
                              ON ip.Account_Id = d.Supplier_Account_Id
                                 AND ip.SITE_ID = d.Site_Id
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          @Sites_With_Meter_To_Account rml
                                     WHERE
                                          d.Site_id = rml.Site_id
                                          AND d.Supplier_Account_Id = rml.Supplier_Account_Id )

      BEGIN TRY
            BEGIN TRAN

            DELETE
                  ipq
            FROM
                  dbo.INVOICE_PARTICIPATION_QUEUE ipq
                  INNER JOIN @Invoice_Participation_List ip
                        ON ip.Account_Id = ipq.ACCOUNT_ID
                           AND ip.Site_Id = ipq.SITE_ID

            WHILE EXISTS ( SELECT
                              1
                           FROM
                              @Invoice_Participation_List ) 
                  BEGIN
			
                        SELECT TOP 1
                              @Account_id = Account_Id
                             ,@Site_Id = Site_Id
                             ,@Service_Month = Service_Month
                        FROM
                              @Invoice_Participation_List

                        EXEC dbo.Invoice_Participation_Del 
                              @Account_Id
                             ,@Site_Id
                             ,@Service_Month

                        EXEC dbo.cbmsInvoiceParticipationSite_Save 
                              -1
                             ,@Site_Id
                             ,@Service_Month -- To reset Site level invoice participation.

                        DELETE
                              @Invoice_Participation_List
                        WHERE
                              Account_Id = @Account_Id
                              AND Site_Id = @Site_Id
                              AND Service_Month = @Service_Month

                  END
            COMMIT TRAN
      END TRY
      BEGIN CATCH
            IF @@TRANCOUNT > 0 
                  BEGIN
                        ROLLBACK TRAN
                  END

            EXEC dbo.usp_RethrowError

      END CATCH

END

;
GO
GRANT EXECUTE ON  [dbo].[IP_Reset_For_Deleted_Meter] TO [CBMSApplication]
GO
