SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	cbms_prod.dbo.cbmsInvSourcedImageReport_Reload2

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script
	GDP			8/24/2009	Changed .b.ubm_batch_master_log_id to b.ubm_batch_master_log (SQL 2008 bug)
 DMR		  09/10/2010 Modified for Quoted_Identifier
	
******/
create           PROCEDURE dbo.cbmsInvSourcedImageReport_Reload2
	( @MyAccountId int )
AS

BEGIN


	set nocount on
create table #crap (inv_sourced_image_id int)
create table #crap2 (inv_sourced_image_id int, cbms_image_id int, ismatch bit default 0)

create clustered index CX_INV on #crap (inv_sourced_image_id)
create clustered index CX_INV2 on #crap2 (cbms_image_id,inv_sourced_image_id,ismatch)


insert into #crap2 select dbo.getinvsourceid(left(cbms_doc_id,7)),a.cbms_image_id,0 from cbms_image a
inner join ubm_invoice b on a.cbms_image_id=b.cbms_image_id
inner join ubm_batch_master_log c on c.ubm_batch_master_log_id=b.ubm_batch_master_log_id
where (cbms_image_type_id=2 or cbms_image_type_id=1038) and inv_sourced_image_id is null
and ubm_id=7
--need to change to 6-1-06 for whenever this process started presumably, 
--though this seems excessive
and date_imaged>='6-1-06'--41728 rows, 111092

--select * from #crap2
--drop table #crap2
insert into #crap select inv_sourced_image_id from inv_sourced_image_track where is_received=0--124696

update #crap2 set ismatch=1 where inv_sourced_image_id in (select inv_sourced_image_id from #crap)

update cbms_image
set cbms_image.inv_sourced_image_id=a.inv_sourced_image_id
from #crap2 a inner join  cbms_image b with(index(cbms_image_pk))
on a.cbms_image_id=b.cbms_image_id and a.ismatch=1 and b.inv_sourced_image_id is null 

/*	declare @inv_count int

	   select @inv_count = count(*)
	     from inv_sourced_image with (nolock)
	    where charindex(space(1), original_filename) > 0
	       or charindex(space(1), cbms_filename) > 0

	print convert(varchar, @inv_count)

	while @inv_count > 0
	begin

	   update inv_sourced_image  with (rowlock)
	      set original_filename = replace(original_filename, space(1), space(0))
		, cbms_filename = replace(cbms_filename, space(1), space(0))

	   select @inv_count = count(*)
	     from inv_sourced_image with (nolock)
	    where charindex(space(1), original_filename) > 0
	       or charindex(space(1), cbms_filename) > 0

	print convert(varchar, @inv_count)

	end
*/
	-- STRIP SPACES FROM CBMS_IMAGE TABLE
/*
	   select @inv_count = count(distinct cbms_image_id)
	     from cbms_image with (nolock)
	    where cbms_image_type_id in (1038, 2)
	      and date_imaged >= '6/1/2006'
	      and inv_sourced_image_id is null
	      and charindex(space(1), cbms_doc_id) > 0



	while @inv_count > 0
	begin

	   update cbms_image with (rowlock)
	      set cbms_doc_id = replace(cbms_doc_id, space(1), space(0))
	    where cbms_image_type_id in (1038, 2)
	      and date_imaged >= '6/1/2006'
	      and inv_sourced_image_id is null
	      and charindex(space(1), cbms_doc_id) > 0

	   select @inv_count = count(distinct cbms_image_id)
	     from cbms_image with (nolock)
	    where cbms_image_type_id in (1038, 2)
	      and date_imaged >= '6/1/2006'
	      and inv_sourced_image_id is null
	      and charindex(space(1), cbms_doc_id) > 0



	end
*/

--added to remove unnecessary hits to the table
	update inv_sourced_image
	set original_filename=replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(
	replace(original_filename,'=',''),'&','')
	,'#',''),';',''),',',''),'(',''),')',''),'[',''),']',''),'''',''),'~','')

/*
	   update inv_sourced_image  with (rowlock)
	      set original_filename = replace(original_filename, '=', space(0))
		, cbms_filename = replace(cbms_filename, '=', space(0))

	   update inv_sourced_image  with (rowlock)
	      set original_filename = replace(original_filename, '&', space(0))
		, cbms_filename = replace(cbms_filename, '&', space(0))

	   update inv_sourced_image  with (rowlock)
	      set original_filename = replace(original_filename, '#', space(0))
		, cbms_filename = replace(cbms_filename, '#', space(0))

	   update inv_sourced_image  with (rowlock)
	      set original_filename = replace(original_filename, ';', space(0))
		, cbms_filename = replace(cbms_filename, ';', space(0))

	   update inv_sourced_image  with (rowlock)
	      set original_filename = replace(original_filename, ',', space(0))
		, cbms_filename = replace(cbms_filename, ',', space(0))

	   update inv_sourced_image  with (rowlock)
	      set original_filename = replace(original_filename, '(', space(0))
		, cbms_filename = replace(cbms_filename, '(', space(0))

	   update inv_sourced_image  with (rowlock)
	      set original_filename = replace(original_filename, ')', space(0))
		, cbms_filename = replace(cbms_filename, ')', space(0))

	   update inv_sourced_image  with (rowlock)
	      set original_filename = replace(original_filename, '[', space(0))
		, cbms_filename = replace(cbms_filename, '[', space(0))

	   update inv_sourced_image  with (rowlock)
	      set original_filename = replace(original_filename, ']', space(0))
		, cbms_filename = replace(cbms_filename, ']', space(0))

	   update inv_sourced_image  with (rowlock)
	      set original_filename = replace(original_filename, '''', space(0))
		, cbms_filename = replace(cbms_filename, '''', space(0))

	   update inv_sourced_image  with (rowlock)
	      set original_filename = replace(original_filename, '~', space(0))
		, cbms_filename = replace(cbms_filename, '~', space(0))

*/
--next statement was cost prohibitive and would never finish with current load
--taken care of in the above statements
/*
	   update cbms_image with (rowlock)
	      set cbms_image.inv_sourced_image_id = s.inv_sourced_image_id
	     from cbms_image i with (nolock)
	     join inv_sourced_image s with (nolock) on i.cbms_doc_id like left(s.cbms_filename, case when (charindex('.',s.cbms_filename) - 1) < 0 then len(s.cbms_filename) else charindex('.',s.cbms_filename) - 1 end) + '%'
	     join ubm_invoice ui with (nolock) on ui.cbms_image_id = i.cbms_image_id
	     join ubm_batch_master_log l with (nolock) on l.ubm_batch_master_log_id = ui.ubm_batch_master_log_id
	    where i.cbms_image_type_id in (1038, 2)
	      and i.date_imaged >= '6/1/2006'
	      and l.ubm_id = 7
	      and i.inv_sourced_image_id is null
*/


	  declare @received_type_id int
		, @not_received_type_id int
		, @exception_type_id int

	   select @received_type_id = entity_id
	     from entity with (nolock)
	    where entity_type = 553
	      and entity_name = 'Received'

	   select @not_received_type_id = entity_id
	     from entity with (nolock)
	    where entity_type = 553
	      and entity_name = 'Not Received'

	   select @exception_type_id = entity_id
	     from entity with (nolock)
	    where entity_type = 553
	      and entity_name = 'Exception'


	/* clean up keywords */


	   update inv_sourced_image
	      set keyword = archive_path
	    where inv_source_id = 6
	      and keyword is null

--added to remove unnecessary hits to the table
	   update inv_source_email_attachment  
	set final_filename=replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(
	replace(replace(final_filename,'=',''),'&','')
	,'#',''),';',''),',',''),'(',''),')',''),'[',''),']',''),'''',''),'~',''),' ','')

/*
	   update inv_source_email_attachment  with (rowlock)
	      set final_filename = replace(final_filename, space(1), space(0))

	   update inv_source_email_attachment  with (rowlock)
	      set final_filename = replace(final_filename, '=', space(0))

	   update inv_source_email_attachment  with (rowlock)
	      set final_filename = replace(final_filename, '&', space(0))

	   update inv_source_email_attachment  with (rowlock)
	      set final_filename = replace(final_filename, '#', space(0))

	   update inv_source_email_attachment  with (rowlock)
	      set final_filename = replace(final_filename, ';', space(0))

	   update inv_source_email_attachment  with (rowlock)
	      set final_filename = replace(final_filename, ',', space(0))

	   update inv_source_email_attachment  with (rowlock)
	      set final_filename = replace(final_filename, '(', space(0))

	   update inv_source_email_attachment  with (rowlock)
	      set final_filename = replace(final_filename, ')', space(0))

	   update inv_source_email_attachment  with (rowlock)
	      set final_filename = replace(final_filename, '[', space(0))

	   update inv_source_email_attachment  with (rowlock)
	      set final_filename = replace(final_filename, ']', space(0))

	   update inv_source_email_attachment  with (rowlock)
	      set final_filename = replace(final_filename, '''', space(0))

	   update inv_source_email_attachment  with (rowlock)
	      set final_filename = replace(final_filename, '~', space(0))
*/
	   update inv_sourced_image
	      set keyword = l.from_address
	     from inv_sourced_image s
	     join inv_source_email_attachment ea on ea.final_filename = s.original_filename
	     join inv_source_email_log l on l.inv_source_email_log_id = ea.inv_source_email_log_id and l.inv_sourced_image_batch_id = s.inv_sourced_image_batch_id
	    where s.inv_source_id = 8
	      and s.keyword is null


	/* update just-received in tracking table */
	   update inv_sourced_image_track
	      set is_received = 1
		, cu_invoice_id = cu.cu_invoice_id
--	   select distinct t.*
	     from inv_sourced_image_track t with (nolock)
             join cbms_image ci with (nolock) on ci.inv_sourced_image_id = t.inv_sourced_image_id
  	     join cu_invoice cu with (nolock) on cu.cbms_image_id = ci.cbms_image_id
	    where t.is_received = 0


	TRUNCATE TABLE inv_sourced_image_report
--select * from inv_sourced_image_report where inv_sourced_image_id=201667


	   insert into inv_sourced_image_report		( inv_sourced_image_track_id
		, inv_sourced_image_id
		, inv_source_id
		, inv_source_label
		, inv_source_keyword
		, original_filename
		, prokarma_filename
		, archive_path
		, sent_date
		, received_date
		, status_type_id
		, status_type
		, account_number
		, cbms_image_id
		, ubm_batch_master_log_id
		, exception_comments
		)
	   select distinct t.inv_sourced_image_track_id
		, s.inv_sourced_image_id
		, s.inv_source_id
		, sl.inv_source_label
		, s.keyword inv_source_keyword
		, s.original_filename
		, ci.cbms_doc_id prokarma_filename  
		, s.archive_path
		, bs.batch_date sent_date
		, ci.date_imaged received_date
		, @received_type_id status_type_id
		, 'Received' status_type
		, ui.ubm_account_number account_number
		, ci.cbms_image_id
		, ui.ubm_batch_master_log_id
		, null exception_comments

	     from inv_sourced_image_track t
	     join inv_sourced_image s on s.inv_sourced_image_id = t.inv_sourced_image_id
	     join inv_sourced_image_batch bs with (nolock) on bs.inv_sourced_image_batch_id = s.inv_sourced_image_batch_id
	     join inv_source sl with (nolock) on sl.inv_source_id = s.inv_source_id
	     join cu_invoice cu on cu.cu_invoice_id = t.cu_invoice_id
             join cbms_image ci with (nolock) on ci.cbms_image_id = cu.cbms_image_id

  left outer join ubm_invoice ui with (nolock) on ui.ubm_invoice_id = cu.ubm_invoice_id
  left outer join ubm_batch_master_log bl with (nolock) on bl.ubm_batch_master_log_id = ui.ubm_batch_master_log_id

	    where t.is_received = 1

	union all

	   select distinct t.inv_sourced_image_track_id
		, s.inv_sourced_image_id
		, s.inv_source_id
		, sl.inv_source_label
		, s.keyword inv_source_keyword
		, s.original_filename
		, pr.barcode prokarma_filename
		, s.archive_path
		, bs.batch_date sent_date
		, pb.batch_date received_date
		, @exception_type_id status_type_id
		, 'Exception' status_type
		, pr.account_number
		, null cbms_image_id
		, null ubm_batch_master_log_id
		, pr.remarks exception_comments

	     from inv_sourced_image_track t with (nolock)
             join inv_prokarma_not_reported pr with (nolock) on pr.inv_sourced_image_id = t.inv_sourced_image_id and pr.barcode = t.prokarma_filename
	     join inv_sourced_image s with (nolock) on s.inv_sourced_image_id = t.inv_sourced_image_id
	     join inv_sourced_image_batch bs with (nolock) on bs.inv_sourced_image_batch_id = s.inv_sourced_image_batch_id
	     join inv_source sl with (nolock) on sl.inv_source_id = s.inv_source_id
	     join inv_prokarma_not_reported_batch pb with (nolock) on pb.inv_prokarma_not_reported_batch_id = pr.inv_prokarma_not_reported_batch_id

	    where t.is_received = 0

	union all

	   select distinct t.inv_sourced_image_track_id
		, s.inv_sourced_image_id
		, s.inv_source_id
		, sl.inv_source_label
		, s.keyword inv_source_keyword
		, s.original_filename
		, null prokarma_filename
		, s.archive_path
		, bs.batch_date sent_date
		, null received_date
		, @not_received_type_id status_type_id
		, 'Not Received' status_type
		, null account_number
		, null cbms_image_id
		, null ubm_batch_master_log_id
		, null exception_comments

	     from inv_sourced_image_track t with (nolock) 
  left outer join inv_prokarma_not_reported pr with (nolock) on pr.inv_sourced_image_id = t.inv_sourced_image_id 
	     join inv_sourced_image s with (nolock) on s.inv_sourced_image_id = t.inv_sourced_image_id
	     join inv_sourced_image_batch bs with (nolock) on bs.inv_sourced_image_batch_id = s.inv_sourced_image_batch_id
	     join inv_source sl with (nolock) on sl.inv_source_id = s.inv_source_id

	    where t.is_received = 0
	      and pr.inv_sourced_image_id is null

drop table #crap2
drop table #crap

END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvSourcedImageReport_Reload2] TO [CBMSApplication]
GO
