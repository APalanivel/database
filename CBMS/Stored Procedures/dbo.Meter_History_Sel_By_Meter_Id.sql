SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Meter_History_Sel_By_Meter_Id]  
     
DESCRIPTION:

	To Get meter history for Selected Meter Id.
      
INPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION 
------------------------------------------------------------
@Meter_Id		INT
@StartIndex		INT			1	
@EndIndex		INT			2147483647

OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------

	EXEC Meter_History_Sel_By_Meter_Id  62461,1,25
	EXEC Meter_History_Sel_By_Meter_Id  21086,1,25
	EXEC dbo.GET_METER_P 62461
	
AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR		PANDARINATH

MODIFICATIONS
INITIALS	DATE			MODIFICATION
------------------------------------------------------------
PNR			03-June-10		CREATED

*/

CREATE PROCEDURE dbo.Meter_History_Sel_By_Meter_Id
(
	@Meter_Id		INT
	,@StartIndex	INT		= 1
	,@EndIndex		INT		= 2147483647
)
AS
BEGIN

	SET NOCOUNT ON;

	EXEC dbo.GET_METER_P @Meter_Id
	EXEC dbo.Rate_Comparison_Sel_By_Meter_Id @Meter_Id, @StartIndex, @EndIndex
	EXEC dbo.Contract_Sel_By_Meter_Id @Meter_Id, @StartIndex, @EndIndex
	EXEC dbo.RFP_Sel_By_Meter_Id @Meter_Id, @StartIndex, @EndIndex
	EXEC dbo.Tax_Exemption_Sel_By_Meter_Id @Meter_Id, @StartIndex, @EndIndex
	EXEC dbo.Other_Meter_Sel_For_Meter_Account @Meter_Id

END
GO
GRANT EXECUTE ON  [dbo].[Meter_History_Sel_By_Meter_Id] TO [CBMSApplication]
GO
