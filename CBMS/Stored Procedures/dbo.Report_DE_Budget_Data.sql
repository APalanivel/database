
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********            
NAME:            
  dbo.Report_DE_Budget_Data         
      
DESCRIPTION:       
      
      
INPUT PARAMETERS:          
      Name              DataType          Default     Description          
------------------------------------------------------------          
    
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------     
    
USAGE EXAMPLES:    
------------------------------------------------------------    
Stress /Prod    
exec Report_DE_Budget_Data
    
AUTHOR INITIALS:    
     
Initials Name    
------------------------------------------------------------    
AKR Ashok Kumar Raju   
    
Initials Date  Modification            
------------------------------------------------------------       
AKR		2013-05-16 Created the Sproc
AKR		2014-09-10  Modified the code remove Utility_Analyst_Map View
DMR		2014-10-02  Modified to add in the "Set NoCount on"  When this option is off, it results in SSIS
                  failing with the error "A rowset based on the SQL command was not returned by the OLE DB provider"
HG		2015-03-16	MAINT-3433, changed the legacy group name to "Regulated Markets Budgets"
*/    
    
CREATE PROCEDURE [dbo].[Report_DE_Budget_Data]
AS 
BEGIN

SET NOCOUNT ON
      DECLARE
            @Commodity_Id INT
           ,@Contract_Type_Id INT
           ,@Entity_Id_C INT
           ,@Entity_Id_D INT
           
      DECLARE @Group_Legacy_Name TABLE
            ( 
             GROUP_INFO_ID INT
            ,GROUP_NAME VARCHAR(200)
            ,Legacy_Group_Name VARCHAR(200) )             

      SELECT
            @Contract_Type_Id = en.ENTITY_ID
      FROM
            dbo.ENTITY en
      WHERE
            en.ENTITY_NAME = 'Supplier'
            AND en.ENTITY_DESCRIPTION = 'Contract Type'

      SELECT
            @Entity_Id_C = en.ENTITY_ID
      FROM
            dbo.ENTITY en
      WHERE
            en.ENTITY_NAME = 'C'
            AND en.ENTITY_DESCRIPTION = 'Commercial'

      SELECT
            @Entity_Id_D = en.ENTITY_ID
      FROM
            dbo.ENTITY en
      WHERE
            en.ENTITY_NAME = 'D'
            AND en.ENTITY_DESCRIPTION = 'Other'

      SELECT
            @Commodity_Id = com.Commodity_Id
      FROM
            dbo.Commodity com
      WHERE
            com.Commodity_Name = 'Electric Power'

      INSERT      INTO @Group_Legacy_Name
                  (
                   GROUP_INFO_ID
                  ,GROUP_NAME
                  ,Legacy_Group_Name )
                  EXEC dbo.Group_Info_Sel_By_Group_Legacy
                        @Group_Legacy_Name_Cd_Value = 'Regulated_Markets_Budgets'

      SELECT
            b.budget_id [Budget ID]
           ,original_budget_id [Revised from Budget ID]
           ,date_initiated [Budget Creation Date]
           ,due_date [Budget Internal Due Date]
           ,[Budget Posted to DV] = CASE WHEN is_posted_to_dv = 1 THEN 'Yes'
                                         ELSE 'No'
                                    END
           ,date_posted [Budget Posted to DV Date]
           ,ui.first_name + ' ' + ui.last_name [CEM Name]
           ,budget_start_year [Budget Year]
           ,e.entity_name AS [Original/Revised]
           ,commodity = CASE WHEN b.commodity_type_id = @Commodity_Id THEN 'EP'
                             ELSE 'NG'
                        END
           ,ch.client_name [Client Name]
           ,ch.sitegroup_name [Division]
           ,Region = CASE WHEN ch.state_name IN ( 'mn', 'ia' ) THEN 'MW'
                          WHEN ch.state_name IN ( 'la', 'ar' ) THEN 'SE'
                          WHEN ch.country_Id = '1' THEN 'NE'
                          ELSE region_name
                     END
           ,ch.state_name [State]
           ,ch.city
           ,ch.site_name [Site Name]
           ,cha.Account_Vendor_Name [Utility]
           ,cha.account_number [Account Number]
           ,rates_completed_date [Regulated Markets Completion Date]
           ,rates_reviewed_date [Regulated Markets Review Date]
           ,cannot_performed_date [Cannot Perform Date]
           ,[Analyst Name] = CASE WHEN ui1.first_name IS NULL THEN ui4.first_name + ' ' + ui4.last_name
                                  ELSE ui1.first_name + ' ' + ui1.last_name
                             END
           ,ui2.first_name + ' ' + ui2.last_name reganalyst
           ,ui3.first_name + ' ' + ui3.last_name managername
           ,ba.budget_comments
           ,cd_create_multiplier
           ,c_d_tariff = CASE WHEN cha.Account_Service_level_Cd IN ( @Entity_Id_C, @Entity_Id_D )
                                   AND k.account_id IS NULL THEN 'Yes'
                              ELSE 'NotApplicable'
                         END
      FROM
            dbo.budget b
            INNER JOIN dbo.client cl
                  ON cl.client_id = b.client_id
            INNER JOIN dbo.client_cem_map ccm
                  ON ccm.client_id = cl.client_id
            INNER JOIN dbo.user_info ui
                  ON ui.user_info_id = ccm.user_info_id
            INNER JOIN dbo.budget_account ba
                  ON ba.budget_id = b.budget_id
            INNER JOIN core.Client_Hier_Account cha
                  ON cha.Account_Id = ba.ACCOUNT_ID
            INNER JOIN core.Client_Hier ch
                  ON ch.Client_Hier_Id = cha.Client_Hier_Id
                     AND b.CLIENT_ID = ch.Client_Id
            LEFT JOIN dbo.entity e
                  ON e.entity_id = b.status_type_id
            LEFT JOIN dbo.user_info ui1
                  ON ui1.user_info_id = ba.cannot_performed_by
            LEFT JOIN dbo.utility_detail ud
                  ON ud.vendor_id = cha.Account_vendor_id
            LEFT JOIN ( dbo.Utility_Detail_Analyst_Map UDAM
                        INNER JOIN @Group_Legacy_Name GI
                              ON UDAM.Group_Info_ID = GI.GROUP_INFO_ID )
                        ON ud.UTILITY_DETAIL_ID = udam.UTILITY_DETAIL_ID
            LEFT JOIN dbo.user_info ui2
                  ON ui2.user_info_id = udam.Analyst_id
            LEFT JOIN dbo.user_info ui3
                  ON ui3.user_info_id = ba.rates_reviewed_by
            LEFT JOIN dbo.user_info ui4
                  ON ui4.user_info_id = ba.rates_completed_by
            LEFT JOIN ( SELECT
                              ba.account_id
                        FROM
                              budget_account ba
                              JOIN account acct
                                    ON acct.account_id = ba.account_id
                              JOIN meter m
                                    ON m.account_id = acct.account_id
                              JOIN supplier_account_meter_map samm
                                    ON samm.meter_id = m.meter_id
                              JOIN contract c
                                    ON c.contract_id = samm.contract_id
                        WHERE
                              acct.service_level_type_id IN ( @Entity_Id_C, @Entity_Id_D )
                              AND GETDATE() BETWEEN contract_start_date
                                            AND     contract_end_date
                              AND c.contract_type_id = @Contract_Type_Id
                              AND samm.meter_disassociation_date IS NULL ) k
                  ON k.account_id = ba.Account_Id
      GROUP BY
            b.budget_id
           ,original_budget_id
           ,date_initiated
           ,due_date
           ,is_posted_to_dv
           ,date_posted
           ,ui.first_name
           ,ui.last_name
           ,budget_start_year
           ,e.entity_name
           ,b.commodity_type_id
           ,ch.client_name
           ,ch.sitegroup_name
           ,ch.state_name
           ,ch.country_Id
           ,region_name
          ,ch.state_name
           ,ch.city
           ,ch.site_name
           ,cha.Account_Vendor_Name
           ,cha.account_number
           ,rates_completed_date
           ,rates_reviewed_date
           ,cannot_performed_date
           ,ui1.first_name
           ,ui4.first_name
           ,ui4.last_name
           ,ui1.last_name
           ,ui2.first_name
           ,ui2.last_name
           ,ui3.first_name
           ,ui3.last_name
           ,ba.budget_comments
           ,cd_create_multiplier
           ,cha.Account_Service_level_Cd
           ,k.account_id

END;  
;
GO



GRANT EXECUTE ON  [dbo].[Report_DE_Budget_Data] TO [CBMS_SSRS_Reports]
GO
