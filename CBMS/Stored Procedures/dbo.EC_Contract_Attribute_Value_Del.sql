SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name: dbo.EC_Contract_Attribute_Value_Del           
              
Description:              
        To delete Data to EC_Contract_Attribute_Value table.              
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------        
    @EC_Contract_Attribute_Value_Id		INT
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                
 BEGIN TRAN  
 DECLARE  @tvp_EC_Contract_Attribute_Value tvp_EC_Contract_Attribute_Value ,@EC_Contract_Attribute_Value_Id int  
 INSERT @tvp_EC_Contract_Attribute_Value      
  SELECT NULL,'Delete_1'     
     
 SELECT * FROM EC_Contract_Attribute_Value WHERE EC_Contract_Attribute_Value in ('Delete_1')    
    EXEC dbo.EC_Contract_Attribute_Value_Ins     
      @tvp_EC_Contract_Attribute_Value = @tvp_EC_Contract_Attribute_Value  
      ,@EC_Contract_Attribute_Id = 7  
     ,@User_Info_Id = 100    
 SELECT @EC_Contract_Attribute_Value_Id=EC_Contract_Attribute_Value_Id FROM EC_Contract_Attribute_Value WHERE EC_Contract_Attribute_Value in ('Delete_1')  
 SELECT @EC_Contract_Attribute_Value_Id  
  SELECT * FROM EC_Contract_Attribute_Value WHERE EC_Contract_Attribute_Value='Delete_1'	
		
    EXEC dbo.EC_Contract_Attribute_Value_Del 
      @EC_Contract_Attribute_Value_Id = @EC_Contract_Attribute_Value_Id
      
      SELECT * FROM EC_Contract_Attribute_Value WHERE EC_Contract_Attribute_Value='Delete_1'	
	ROLLBACK TRAN             
             
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy               
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2015-04-22		Created For AS400.         
             
******/
CREATE PROCEDURE [dbo].[EC_Contract_Attribute_Value_Del]
      ( 
       @EC_Contract_Attribute_Value_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 
      
      DELETE
            ecav
      FROM
            dbo.EC_Contract_Attribute_Value ecav
      WHERE
            ecav.EC_Contract_Attribute_Value_Id = @EC_Contract_Attribute_Value_Id

END
        

;
GO
GRANT EXECUTE ON  [dbo].[EC_Contract_Attribute_Value_Del] TO [CBMSApplication]
GO
