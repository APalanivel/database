SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
dbo.Unit_Of_Measurement_Sel_By_Commodity

DESCRIPTION:

	Selects UOM for the given commodity

INPUT PARAMETERS:
Name 			DataType 	Default 	Description
------------------------------------------------------------
@Commodity_Id 	INT						indiactes commodity type(kerosene,water etc..)

OUTPUT PARAMETERS:
Name DataType Default Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Unit_Of_Measurement_Sel_By_Commodity 67

AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------
GM			GANGADHAR MAMILLAPALLI


MODIFICATIONS
Initials	Date		Modification
------------------------------------------------------------
GM        	1/28/2010	Created


 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.Unit_Of_Measurement_Sel_By_Commodity
(
	@Commodity_Id INT
)
AS

BEGIN

	SET NOCOUNT ON

	SELECT
		en.Entity_id Uom_Type_Id
		, en.Entity_name Uom_Name
		, c.Default_UOM_Entity_Type_Id
	FROM
		dbo.Commodity c
		JOIN dbo.Entity en
			ON en.Entity_type = c.UOM_Entity_Type
	WHERE 
		c.Commodity_Id = @Commodity_id

END
GO
GRANT EXECUTE ON  [dbo].[Unit_Of_Measurement_Sel_By_Commodity] TO [CBMSApplication]
GO
