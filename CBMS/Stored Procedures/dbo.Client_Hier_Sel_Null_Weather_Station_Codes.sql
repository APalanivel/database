SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
NAME:   dbo.Client_Hier_Sel_Null_Weather_Station_Codes    

DESCRIPTION:  
	Gets the sites with NULL weather station codes

INPUT PARAMETERS:          
Name						DataType          Default Description          
------------------------------------------------------------          

OUTPUT PARAMETERS:          
Name                    DataType        Default         Description          
------------------------------------------------------------ 


USAGE EXAMPLES:          
------------------------------------------------------------ 
EXEC dbo.Client_Hier_Sel_Null_Weather_Station_Codes

AUTHOR INITIALS:          
Initials        Name          
------------------------------------------------------------          
CPE				Chaitanya Panduga Eshwar

MODIFICATIONS           
Initials         Date			Modification          
------------------------------------------------------------          
CPE              2013-02-15		Created

*****/

CREATE PROCEDURE [dbo].[Client_Hier_Sel_Null_Weather_Station_Codes]
AS 
BEGIN 
      SET NOCOUNT ON ;
      
      SELECT
            Site_Id
           ,Geo_Lat
           ,Geo_Long
      FROM
            Core.Client_Hier
      WHERE
            Weather_Station_Code IS NULL
            AND Site_Id > 0
            AND Geo_Lat IS NOT NULL
            AND Geo_Long IS NOT NULL
            AND Geo_Lat <> 0 
            AND Geo_Long <> 0
                
END ;
;
GO
GRANT EXECUTE ON  [dbo].[Client_Hier_Sel_Null_Weather_Station_Codes] TO [CBMSApplication]
GO
