SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.Variance_Log_Sel_Top_For_Max_Queue_Dt_By_Account_Commodity

DESCRIPTION:
	procedure used to select top N variance log entries for the maximum queue date available for the given account, commodity and service month.

INPUT PARAMETERS:
	Name			DataType	Default	Description
------------------------------------------------------------
	@Account_Id	    INT
	@Commodity_Id	INT		    NULL
	@Service_Month	DATE
	@Top_Count	    INT		    1

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC dbo.Variance_Log_Sel_Top_For_Max_Queue_Dt_By_Account_Commodity 158109,291,'2011-01-01', 2

	SELECT TOP 10 * FROM Variance_Log ORDER BY Variance_Log_id DESC

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RK		    Rajesh Kasoju

MODIFICATIONS
	Initials	Date		   Modification
------------------------------------------------------------
    RK          03/30/2012     Created to remove Cost_Usage_ID column as part of ADLDT changes and also added Top_Count parameter  
     
******/

CREATE PROCEDURE dbo.Variance_Log_Sel_Top_For_Max_Queue_Dt_By_Account_Commodity
      ( 
       @Account_Id INT
      ,@Commodity_Id INT = NULL
      ,@Service_Month DATE
      ,@Top_Count INT = 1 )
AS 
BEGIN

      SET NOCOUNT ON ;

      SELECT TOP ( @Top_Count )
            vl.variance_log_id
           ,vl.Execution_Dt
           ,vl.Is_Failure
           ,vl.Closed_Dt
           ,vl.queue_id
           ,vl.queue_name
           ,vl.Queue_Dt
      FROM
            dbo.variance_log vl
      WHERE
            ( vl.Account_ID = @Account_Id )
            AND ( @Commodity_Id IS NULL
                  OR vl.Commodity_ID = @Commodity_Id )
            AND ( vl.Service_Month = @Service_Month )
      ORDER BY
            vl.Queue_Dt DESC

END
;
GO
GRANT EXECUTE ON  [dbo].[Variance_Log_Sel_Top_For_Max_Queue_Dt_By_Account_Commodity] TO [CBMSApplication]
GO
