SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: DBO.Prefer_Not_ToDo_Business_Del
     
DESCRIPTION: 
	It Deletes Prefer Not to do Business Details for Selected Division Id and Vendor Id.
      
INPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION         
-----------------------------------------------------------------------
	@Vendor_Id		INT
    @Division_Id	INT
    
OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN
		EXEC Prefer_Not_ToDo_Business_Del 625,49
	ROLLBACK TRAN
	
	SELECT * FROM dbo.PREFER_NOT_TODO_BUSINESS 
	WHERE Vendor_Id = 625 AND Division_Id = 49

AUTHOR INITIALS:          
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			21-JULY-10	CREATED

*/

CREATE PROCEDURE dbo.Prefer_Not_ToDo_Business_Del
    (
       @Vendor_Id	INT
      ,@Division_Id	INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.Prefer_Not_ToDo_Business
	WHERE
		VENDOR_ID = @Vendor_Id
		AND Division_Id = @Division_Id

END
GO
GRANT EXECUTE ON  [dbo].[Prefer_Not_ToDo_Business_Del] TO [CBMSApplication]
GO
