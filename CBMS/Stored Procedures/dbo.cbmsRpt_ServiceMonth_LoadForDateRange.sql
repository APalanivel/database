SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE      procedure [dbo].[cbmsRpt_ServiceMonth_LoadForDateRange]
	( @MyAccountId int
	, @session_uid uniqueidentifier
	, @begin_date datetime
	, @end_date datetime
	)
AS
BEGIN

set nocount on

	  declare @working_date datetime
		, @month_number int

	      set @working_date = @begin_date
	      set @month_number = 1

	     exec cbmsRpt_ServiceMonth_RemoveForSession @MyAccountId, @session_uid

    while @working_date <= @end_date
    begin

	insert into rpt_service_month ( session_uid, service_month, report_year, month_number ) 
	     values ( @session_uid, @working_date, null, @month_number )

		set @working_date = dateadd("m", 1, @working_date)
		set @month_number = @month_number + 1

    end


END
GO
GRANT EXECUTE ON  [dbo].[cbmsRpt_ServiceMonth_LoadForDateRange] TO [CBMSApplication]
GO
