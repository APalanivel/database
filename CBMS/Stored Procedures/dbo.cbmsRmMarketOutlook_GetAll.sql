SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE      procedure dbo.cbmsRmMarketOutlook_GetAll
	--  59, 5, 1477, '4/8/2008'

	(@AccountId int
	, @MarketGroupId int = null
	, @IntervalTypeId int = null
	, @PubDate datetime = null
	)

as
begin
	/*select o.rm_market_outlook_id
		, o.rm_market_group_id
		, g.rm_market_group
		, o.interval_type_id
		, i.entity_name interval_type
		, o.cbms_image_id
		, o.overview
		, o.pub_date
		, o.rm_market_outlook_title
	from rm_market_outlook o
	join entity i with (nolock) on i.entity_id = o.interval_type_id
 	join rm_market_group g with (nolock) on g.rm_market_group_id = o.rm_market_group_id
	where o.rm_market_group_id = isNull(@MarketGroupId, o.rm_market_group_id)
	and o.interval_type_id = isNull(@IntervalTypeId, o.interval_type_id)
	order by o.pub_date desc

select o.rm_market_outlook_id
		, o.rm_market_group_id
		, g.rm_market_group
		, o.interval_type_id
		, i.entity_name interval_type
		, od.language_type
		, od.cbms_image_id
		, od.overview
		, o.pub_date
		, od.rm_market_outlook_title
	from rm_market_outlook o
	join rm_market_outlook_detail od with (nolock) on od.rm_market_outlook_id = o.rm_market_outlook_id
	join rm_market_group g with (nolock) on g.rm_market_group_id = o.rm_market_group_id
	join entity i with (nolock) on i.entity_id = o.interval_type_id
	where o.rm_market_group_id = isNull(@MarketGroupId, o.rm_market_group_id)
	and o.interval_type_id = isNull(@IntervalTypeId, o.interval_type_id)
	and o.pub_date = isNull(@PubDate, o.pub_date)
	order by o.pub_date desc
*/


	select o.rm_market_outlook_id
		, o.rm_market_group_id
		, g.rm_market_group
		, o.interval_type_id
		, i.entity_name interval_type
		--, od.language_type
		--, od.cbms_image_id
		--, od.overview
		, o.pub_date
		--, od.rm_market_outlook_title
	from rm_market_outlook o
	--join rm_market_outlook_detail od with (nolock) on od.rm_market_outlook_id = o.rm_market_outlook_id
	join rm_market_group g with (nolock) on g.rm_market_group_id = o.rm_market_group_id
	join entity i with (nolock) on i.entity_id = o.interval_type_id
	where o.rm_market_group_id = isNull(@MarketGroupId, o.rm_market_group_id)
	and o.interval_type_id = isNull(@IntervalTypeId, o.interval_type_id)
	and o.pub_date = isNull(@PubDate, o.pub_date)
	order by o.pub_date desc
end






GO
GRANT EXECUTE ON  [dbo].[cbmsRmMarketOutlook_GetAll] TO [CBMSApplication]
GO
