SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 CBMS.dbo.UPDATE_RATE_COMPARISON_DOCUMENT_IMAGE  
  
DESCRIPTION:  
  
  
INPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 @userId         varchar(20)              
 @sessionId      varchar(20)              
 @imageId        int                     
 @rateComparisonId int                     
  
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
  VRV-VENKATA REDDY VANGA

MODIFICATIONS  
  
 Initials Date  Modification  
------------------------------------------------------------   
10-12-2019   - Created SP for inserting  RATE COMPARISON DOCUMENT IMAGE
  
  
******/  
  
CREATE PROCEDURE [dbo].[UPDATE_RATE_COMPARISON_DOCUMENT_IMAGE]
  
@userId varchar(20),  
@sessionId varchar(20),  
@imageId int,  
@rateComparisonId int  
  
  
AS  
set nocount on  
 UPDATE RC_RATE_COMPARISON SET RATE_COMPARISON_DOCUMENT_IMAGE_ID=@imageId  
 WHERE RC_RATE_COMPARISON_id=@rateComparisonId  


 
GO
GRANT EXECUTE ON  [dbo].[UPDATE_RATE_COMPARISON_DOCUMENT_IMAGE] TO [CBMSApplication]
GO
