SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[SR_RFP_Send_Supplier_Del]  
     
DESCRIPTION: 
	It Deletes SR RFP Send Supplier for Selected 
						SR RFP Send Supplier Id.
      
INPUT PARAMETERS:          
NAME						DATATYPE	DEFAULT		DESCRIPTION          
-------------------------------------------------------------------          
@SR_RFP_Send_Supplier_Id	INT	

   
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------
  
	Begin Tran
		EXEC SR_RFP_Send_Supplier_Del  40620
	Rollback Tran
    
    SELECT * FROM Sr_Rfp_Send_Supplier s WHERE NOT EXISTS(SELECT 1 FROM SR_RFP_SEND_SUPPLIER_LOG l WHERE s.Sr_Rfp_Send_Supplier_id = l.Sr_Rfp_Send_Supplier_id)
    
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR			PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR		    28-MAY-10	CREATED     

*/  

CREATE PROCEDURE dbo.SR_RFP_Send_Supplier_Del
   (
    @SR_RFP_Send_Supplier_Id INT
   )
AS 
BEGIN

    SET NOCOUNT ON;
   
    DELETE 
   	FROM	
		dbo.SR_RFP_SEND_SUPPLIER
	WHERE 
		SR_RFP_SEND_SUPPLIER_ID = @SR_RFP_Send_Supplier_Id

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_Send_Supplier_Del] TO [CBMSApplication]
GO
