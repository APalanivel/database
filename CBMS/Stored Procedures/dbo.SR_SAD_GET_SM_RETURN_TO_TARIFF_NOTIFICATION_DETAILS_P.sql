
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.SR_SAD_GET_SM_RETURN_TO_TARIFF_NOTIFICATION_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@user_id		VARCHAR(10),  
	@from_week_identifier datetime,  
	@to_week_identifier datetime 

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
exec SR_SAD_GET_SM_RETURN_TO_TARIFF_NOTIFICATION_DETAILS_P 105, '2012-10-25','2013-10-25'
exec SR_SAD_GET_SM_RETURN_TO_TARIFF_NOTIFICATION_DETAILS_P 105, '2012-10-25','2013-10-25'
                   
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	AKR			Ashok Kumar Raju


MODIFICATIONS

	Initials	Date		Modification
-----------------------------------------------------------
	AKR			2012-10-29  Modified the script to use the Analyst Mapping Code
	                        Modified to Standards
	
******/ 
CREATE PROCEDURE dbo.SR_SAD_GET_SM_RETURN_TO_TARIFF_NOTIFICATION_DETAILS_P
      ( 
       @user_id VARCHAR(10)
      ,@from_week_identifier DATETIME
      ,@to_week_identifier DATETIME )
AS 
BEGIN

      SET NOCOUNT ON ;

	
      DECLARE
            @EP_commodity_type_id INT
           ,@NG_commodity_type_id INT
           ,@Account_Service_Level_Cd INT
           ,@Default_Analyst INT
           ,@Custom_Analyst INT
           ,@Utility INT
           ,@transport_type_id INT
           ,@na_type_id INT
           
      DECLARE @Account_List TABLE
            ( 
             Return_To_Tariff_Date DATETIME
            ,Client_Name VARCHAR(200)
            ,Site_Name VARCHAR(500)
            ,Account_Id INT
            ,Account_Number VARCHAR(50)
            ,vendor_name VARCHAR(200)
            ,contract_id INT
            ,Analyst_Mapping_Cd INT
            ,Commodity_Id INT
            ,Account_Vendor_Id INT
            ,Client_Id INT
            ,Site_Id INT )

      SELECT
            @EP_commodity_type_id = max(case WHEN com.Commodity_Name = 'Electric Power' THEN com.Commodity_Id
                                        END)
           ,@NG_commodity_type_id = max(case WHEN com.Commodity_Name = 'Natural Gas' THEN com.Commodity_Id
                                        END)
      FROM
            dbo.Commodity com
      WHERE
            com.Commodity_Name IN ( 'Electric Power', 'Natural Gas' )  
      
      
      SELECT
            @Utility = e.entity_id
      FROM
            dbo.ENTITY e
      WHERE
            e.entity_type = 111
            AND e.entity_name = 'Utility'     
                 
      SELECT
            @Account_Service_Level_Cd = e.ENTITY_ID
      FROM
            dbo.ENTITY e
      WHERE
            e.ENTITY_NAME = 'D'
            AND e.ENTITY_DESCRIPTION = 'Other'
            AND e.ENTITY_TYPE = 708

      SELECT
            @transport_type_id = e.entity_id
      FROM
            dbo.ENTITY e
      WHERE
            e.entity_name = 'Remaining ON transport'
            AND e.entity_type = 1034 
      SELECT
            @na_type_id = en.entity_id
      FROM
            dbo.ENTITY en
      WHERE
            en.entity_name = 'N/A'
            AND en.entity_type = 1034



      SELECT
            @Default_Analyst = max(case WHEN c.Code_Value = 'Default' THEN c.Code_Id
                                   END)
           ,@Custom_Analyst = max(case WHEN c.Code_Value = 'Custom' THEN c.Code_Id
                                  END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Analyst Type' 
      
    
      
      INSERT      INTO @Account_List
                  ( 
                   Return_To_Tariff_Date
                  ,Client_Name
                  ,Site_Name
                  ,Account_Id
                  ,Account_Number
                  ,vendor_name
                  ,contract_id
                  ,Analyst_Mapping_Cd
                  ,Commodity_Id
                  ,Account_Vendor_Id
                  ,Client_Id
                  ,Site_Id )
                  SELECT
                        switch.return_to_tariff_date
                       ,ch.client_name
                       ,rtrim(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' Site_Name
                       ,utilacc.account_id
                       ,utilacc.account_number
                       ,ven.vendor_name
                       ,DBO.SR_SAD_FN_GET_CONTRACT_ID_OF_ACCOUNT(utilacc.account_id, convert(DATETIME, convert(VARCHAR(10), getdate(), 101))) AS contract_id
                       ,coalesce(utilacc.Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) Analyst_Mapping_Cd
                       ,rfp.COMMODITY_TYPE_ID Commodity_Id
                       ,utilacc.VENDOR_ID Account_Vendor_Id
                       ,ch.Client_Id
                       ,ch.Site_Id
                  FROM
                        dbo.sr_rfp_utility_switch switch
                        JOIN dbo.sr_rfp_account rfp_account
                              ON rfp_account.sr_rfp_account_id = switch.sr_account_group_id
                        JOIN dbo.sr_rfp rfp
                              ON rfp.sr_rfp_id = rfp_account.sr_rfp_id
                        INNER JOIN dbo.account utilacc
                              ON utilacc.Account_Id = rfp_account.account_id
                        JOIN dbo.VENDOR ven
                              ON utilacc.VENDOR_ID = ven.VENDOR_ID
                        JOIN core.client_Hier ch
                              ON ch.site_Id = utilacc.Site_Id
                  WHERE
                        utilacc.account_type_id = @Utility
                        AND switch.return_to_tariff_date IS NOT NULL
                        AND switch.return_to_tariff_date BETWEEN @from_week_identifier
                                                         AND     @to_week_identifier
                        AND ( ( switch.return_to_tariff_type_id != @transport_type_id
                                AND switch.return_to_tariff_type_id != @na_type_id )
                              OR switch.return_to_tariff_type_id IS NULL )
                        AND switch.utility_switch_supplier_type_id IS NULL
                        AND switch.change_notice_image_id IS NULL
                        AND switch.SUPPLIER_NOTICE_IMAGE_ID IS NULL
                        AND switch.FLOW_VERIFICATION_IMAGE_ID IS NULL
                        AND switch.is_bid_group = 0
                        AND rfp_account.is_deleted = 0
                        AND rfp_account.bid_status_type_id != 1224
                        AND rfp.COMMODITY_TYPE_ID IN ( @EP_commodity_type_id, @NG_commodity_type_id )
                  GROUP BY
                        switch.return_to_tariff_date
                       ,ch.client_name
                       ,ch.city
                       ,ch.State_Name
                       ,ch.Site_Name
                       ,utilacc.account_id
                       ,utilacc.account_number
                       ,ven.vendor_name
                       ,utilacc.Analyst_Mapping_Cd
                       ,ch.Site_Analyst_Mapping_Cd
                       ,ch.Client_Analyst_Mapping_Cd
                       ,rfp.COMMODITY_TYPE_ID
                       ,utilacc.VENDOR_ID
                       ,ch.Client_Id
                       ,ch.Site_Id ;
      WITH  cte_Account_User
              AS ( SELECT
                        acc.Return_To_Tariff_Date CONTRACT_END_DATE
                       ,acc.vendor_name
                       ,acc.client_name
                       ,acc.site_name
                       ,acc.account_id
                       ,acc.account_number
                       ,acc.contract_id
                       ,case WHEN acc.Analyst_Mapping_Cd = @Default_Analyst THEN vcam.Analyst_Id
                             WHEN acc.Analyst_Mapping_Cd = @Custom_Analyst THEN coalesce(aca.Analyst_User_Info_Id, sca.Analyst_User_Info_Id, cca.Analyst_User_Info_Id)
                        END AS Analyst_Id
                   FROM
                        @Account_List acc
                        JOIN dbo.VENDOR_COMMODITY_MAP vcm
                              ON acc.Account_Vendor_Id = vcm.VENDOR_ID
                                 AND acc.Commodity_Id = vcm.COMMODITY_TYPE_ID
                        INNER JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                              ON vcm.VENDOR_COMMODITY_MAP_ID = vcam.Vendor_Commodity_Map_Id
                        INNER JOIN core.Client_Commodity ccc
                              ON ccc.Client_Id = acc.Client_Id
                                 AND ccc.Commodity_Id = acc.COMMODITY_ID
                        LEFT JOIN dbo.Account_Commodity_Analyst aca
                              ON aca.Account_Id = acc.Account_Id
                                 AND aca.Commodity_Id = acc.COMMODITY_ID
                        LEFT JOIN dbo.SITE_Commodity_Analyst sca
                              ON sca.Site_Id = acc.Site_Id
                                 AND sca.Commodity_Id = acc.COMMODITY_ID
                        LEFT JOIN dbo.Client_Commodity_Analyst cca
                              ON ccc.Client_Commodity_Id = cca.Client_Commodity_Id)
            SELECT
                  cau.CONTRACT_END_DATE CONTRACT_END_DATE
                 ,cau.vendor_name
                 ,cau.client_name
                 ,cau.site_name
                 ,cau.account_id
                 ,cau.account_number
                 ,cau.contract_id
                 ,con.ed_contract_number
            FROM
                  cte_Account_User cau
                  INNER JOIN dbo.SR_SA_SM_MAP sm
                        ON cau.Analyst_Id = sm.SOURCING_ANALYST_ID
                  LEFT JOIN dbo.CONTRACT con
                        ON cau.contract_id = con.CONTRACT_ID
            WHERE
                  sm.SOURCING_MANAGER_ID = @user_id
                  OR cau.Analyst_Id = @user_id
            GROUP BY
                  cau.CONTRACT_END_DATE
                 ,cau.vendor_name
                 ,cau.client_name
                 ,cau.site_name
                 ,cau.account_id
                 ,cau.account_number
                 ,cau.contract_id
                 ,con.ed_contract_number
                            
   
END

;
GO

GRANT EXECUTE ON  [dbo].[SR_SAD_GET_SM_RETURN_TO_TARIFF_NOTIFICATION_DETAILS_P] TO [CBMSApplication]
GO
