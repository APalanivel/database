
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********  
NAME:  dbo.cbmsClient_Get

DESCRIPTION:
	This procedure retruns the details of the client.

INPUT PARAMETERS:
    Name				DataType          Default     Description
------------------------------------------------------------
	@client_id			int

OUTPUT PARAMETERS:
    Name              DataType          Default     Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
exec dbo.cbmsClient_Get 11727 

Initials	Name
------------------------------------------------------------
CPE			Chaitanya Panduga Eshwar
RR			Raghu Reddy

Initials	Date		    Modification
------------------------------------------------------------
CPE			2012-03-12		Added Portfolio_Id to the output
							Removed unused @MyAccountId paramter
							Used core.Client_Hier instead of dbo.Client to remove the reference to the view vwClientFiscalYearStartMonth
HG			2014-02-28		Is_Singlesign_on column added as per RA Admin enh reqmt
RR			2014-06-25		MAINT-2824 Added new column Variance_Exception_Notice_Contact_Email to select list
******/

CREATE PROCEDURE dbo.cbmsClient_Get ( @client_id INT )
AS 
BEGIN
      SET NOCOUNT ON
      
      DECLARE @Is_Portfolio_Client BIT = 0
      
      SELECT
            @Is_Portfolio_Client = 1
      WHERE
            EXISTS ( SELECT
                        1
                     FROM
                        core.Client_Hier CH
                     WHERE
                        CH.Portfolio_Client_Id = @client_id
                        AND CH.Sitegroup_Id = 0 )
      
      SELECT
            c.client_id
           ,c.client_type_id
           ,ct.entity_name client_type
           ,c.report_frequency_type_id
           ,rft.entity_name report_frequency_type
           ,c.client_name
           ,c.fiscalyear_startmonth_type_id
           ,fys.entity_name fiscalyear_startmonth_type
           ,c.ubm_service_id
           ,ubm.entity_name ubm_service
           ,c.dsm_strategy
           ,c.is_sep_issued
           ,c.sep_issue_date
           ,DATEPART(month, ( DATEADD(mm, c.Client_Fiscal_Offset, '01/01/1900') )) AS start_month
           ,cem.user_info_id cem_id
           ,cem.first_name + ' ' + cem.last_name full_name
           ,cem.email_address email_address
           ,c.Portfolio_Client_Id
           ,@Is_Portfolio_Client AS Is_Portfolio_Client
           ,cl.Is_Single_Signon_Login
           ,cl.Variance_Exception_Notice_Contact_Email
      FROM
            core.Client_Hier c
            INNER JOIN dbo.entity ct
                  ON ct.entity_id = c.client_type_id
            INNER JOIN dbo.entity rft
                  ON rft.entity_id = c.report_frequency_type_id
            INNER JOIN dbo.entity fys
                  ON fys.entity_id = c.fiscalyear_startmonth_type_id
            INNER JOIN dbo.Client cl
                  ON cl.Client_Id = @Client_Id
            LEFT OUTER JOIN dbo.entity ubm
                  ON ubm.entity_id = c.ubm_service_id
            LEFT OUTER JOIN dbo.client_cem_map x
                  ON x.client_id = c.client_id
            LEFT OUTER JOIN dbo.user_info cem
                  ON cem.user_info_id = x.user_info_id
      WHERE
            c.client_id = @client_id
            AND c.Sitegroup_Id = 0
END


;

;
GO



GRANT EXECUTE ON  [dbo].[cbmsClient_Get] TO [CBMSApplication]
GO
