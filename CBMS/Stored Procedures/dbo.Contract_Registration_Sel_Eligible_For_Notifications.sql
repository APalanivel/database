
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
		dbo.Contract_Registration_Sel_Eligible_For_Notifications    
 
DESCRIPTION:  

INPUT PARAMETERS:    
Name					DataType	Default Description    
------------------------------------------------------------    
@batch_execution_date	datetime                  
@Commodity_Id			INT					

OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
 
USAGE EXAMPLES:    
------------------------------------------------------------    

SELECT DISTINCT TOP 10 cha.Supplier_Contract_ID,cha.Supplier_Meter_Association_Date
	,DATEADD(DAY, -30, cha.Supplier_Meter_Association_Date) AS Notice_Date
FROM
      core.Client_Hier_Account cha
      JOIN dbo.CONTRACT c
            ON cha.Supplier_Contract_ID = c.CONTRACT_ID
WHERE
      cha.Supplier_Meter_Association_Date > CONVERT(DATETIME, CONVERT(VARCHAR(10), GETDATE(), 101))
      AND c.CONTRACT_START_DATE > CONVERT(DATETIME, CONVERT(VARCHAR(10), GETDATE(), 101))
      AND c.Is_Notification_Required_On_Registration_Validation = 1
      AND DATEADD(DAY, -30, cha.Supplier_Meter_Association_Date) > CONVERT(DATETIME, CONVERT(VARCHAR(10), GETDATE(), 101))
ORDER BY DATEADD(DAY, -30, cha.Supplier_Meter_Association_Date)

EXEC dbo.Contract_Registration_Sel_Eligible_For_Notifications  '2016-06-11'
EXEC dbo.Contract_Registration_Sel_Eligible_For_Notifications  '2044-10-25'
EXEC dbo.Contract_Registration_Sel_Eligible_For_Notifications  '2068-10-31'

AUTHOR INITIALS:    
Initials	Name    
------------------------------------------------------------    
RR			Raghu Reddy
NR			Narayana Reddy

MODIFICATIONS:
Initials	Date		Modification    
------------------------------------------------------------    
RR			2016-05-26	Global Sourcing - Phase5 - GCS-988 Created		
NR			2016-07-13	GCS - 5b - Added locale code					
RR			2016-07-22	GCS-1179 - Modified to get supplier contact mapped to commodity, state and country  of the expiring contract 
				
    
******/    
    
CREATE PROCEDURE [dbo].[Contract_Registration_Sel_Eligible_For_Notifications]
      ( 
       @Execution_Date DATETIME = NULL )
AS 
BEGIN    
    
      SET NOCOUNT ON;    
        
      DECLARE
            @Default_Analyst INT
           ,@Custom_Analyst INT      
        
      SELECT
            @Default_Analyst = MAX(CASE WHEN c.Code_Value = 'Default' THEN c.Code_Id
                                   END)
           ,@Custom_Analyst = MAX(CASE WHEN c.Code_Value = 'Custom' THEN c.Code_Id
                                  END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Analyst Type'      
      
      
      SELECT
            @Execution_Date = ISNULL(@Execution_Date, CONVERT(DATETIME, CONVERT(VARCHAR(10), GETDATE(), 101))) 

      DECLARE @Client_Con_Info TABLE
            ( 
             Client_Name VARCHAR(200)
            ,Client_Id INT
            ,Site_Name VARCHAR(800)
            ,Site_Id INT
            ,Account_Number VARCHAR(50)
            ,Alternate_Account_Number NVARCHAR(200)
            ,Account_Id INT
            ,Meter_Id INT
            ,Meter_Number VARCHAR(50)
            ,Contract_Id INT
            ,Ed_Contract_Number VARCHAR(150)
            ,Vendor_Id INT
            ,Vendor_Name VARCHAR(200)
            ,Start_Date DATETIME
            ,Analyst_Id INT
            ,Analyst_Name VARCHAR(200)
            ,Analyst_Email_Address VARCHAR(150)
            ,Commodity_Type_id INT
            ,Analyst_Mapping_Cd INT
            ,Phone_Number VARCHAR(50)
            ,Product_Type VARCHAR(25)
            ,Commodity_Name VARCHAR(50) )  
            
      DECLARE @Tbl_Regisrations TABLE
            ( 
             Client_Name VARCHAR(200)
            ,Site_Name VARCHAR(800)
            ,Site_Id INT
            ,Account_Number VARCHAR(50)
            ,Alternate_Account_Number NVARCHAR(200)
            ,Account_Id INT
            ,Meter_Id INT
            ,Meter_Number VARCHAR(50)
            ,Contract_Id INT
            ,Ed_Contract_Number VARCHAR(150)
            ,START_DATE DATETIME
            ,Vendor_Id INT
            ,Vendor_Name VARCHAR(200)
            ,Analyst_Id INT
            ,Analyst_Name VARCHAR(200)
            ,Analyst_Email_Address VARCHAR(150)
            ,COMMODITY_TYPE_ID INT
            ,Commodity_Name VARCHAR(50)
            ,Phone_Number VARCHAR(50)
            ,Product_Type VARCHAR(25) ) 
            
      DECLARE @Tbl_Site_Meter_Number TABLE
            ( 
             Site_Name VARCHAR(800)
            ,Site_Id INT
            ,Site_Meter_Number VARCHAR(MAX)
            ,Contract_Id INT )       
                  
      INSERT      INTO @Client_Con_Info
                  ( 
                   Client_Name
                  ,Client_Id
                  ,Site_Name
                  ,Site_Id
                  ,Account_Number
                  ,Alternate_Account_Number
                  ,Account_Id
                  ,Meter_Id
                  ,Meter_Number
                  ,Contract_Id
                  ,Ed_Contract_Number
                  ,Vendor_Id
                  ,Vendor_Name
                  ,Start_Date
                  ,Analyst_Id
                  ,Analyst_Name
                  ,Analyst_Email_Address
                  ,Commodity_Type_id
                  ,Analyst_Mapping_Cd
                  ,Phone_Number
                  ,Product_Type
                  ,Commodity_Name )
                  SELECT
                        ch.client_name
                       ,ch.Client_Id
                       ,RTRIM(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' Site_Name
                       ,ch.Site_Id
                       ,chautil.account_number
                       ,chautil.Alternate_Account_Number
                       ,chautil.account_id
                       ,chasupp.Meter_Id
                       ,chasupp.Meter_Number
                       ,con.Contract_Id
                       ,con.Ed_Contract_Number
                       ,chasupp.Account_Vendor_Id vendor_id
                       ,chasupp.Account_Vendor_Name
                       ,chasupp.Supplier_Meter_Association_Date AS Start_Date
                       ,vcam.Analyst_Id Analyst_ID
                       ,ui.first_name + SPACE(1) + ui.last_name AS Analyst_Name
                       ,ui.EMAIL_ADDRESS Analyst_Email_Address
                       ,chautil.Commodity_Id AS COMMODITY_TYPE_ID
                       ,COALESCE(chautil.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) Analyst_Mapping_Cd
                       ,ui.PHONE_NUMBER
                       ,Product_Type.Code_Value AS Product_Type
                       ,c.Commodity_Name
                  FROM
                        core.Client_Hier ch
                        INNER JOIN core.Client_Hier_Account chautil
                              ON ch.Client_Hier_Id = chautil.Client_Hier_Id
                        INNER JOIN core.Client_Hier_Account chasupp
                              ON chautil.Meter_Id = chasupp.Meter_Id
                        INNER JOIN dbo.CONTRACT con
                              ON chasupp.Supplier_Contract_ID = con.CONTRACT_ID
                        LEFT JOIN dbo.VENDOR_COMMODITY_MAP vcm
                              ON vcm.VENDOR_ID = chautil.Account_Vendor_Id
                                 AND vcm.COMMODITY_TYPE_ID = chautil.Commodity_Id
                        LEFT JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                              ON vcm.Vendor_Commodity_Map_Id = vcam.Vendor_Commodity_Map_Id
                        LEFT JOIN dbo.USER_INFO AS ui
                              ON ui.user_info_id = vcam.analyst_id
                        LEFT JOIN dbo.Code Product_Type
                              ON Product_Type.Code_Id = con.Contract_Product_Type_Cd
                        INNER JOIN dbo.Commodity c
                              ON c.Commodity_Id = chautil.Commodity_Id
                  WHERE
                        chautil.Account_Type = 'Utility'
                        AND chasupp.Account_Type = 'Supplier'
          --AND chasupp.Supplier_Meter_Association_Date > @Execution_Date
                        --AND con.CONTRACT_END_DATE > @Execution_Date
                        AND DATEADD(DAY, -30, chasupp.Supplier_Meter_Association_Date) = @Execution_Date
                        AND con.Is_Notification_Required_On_Registration_Validation = 1
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Contract_Notification_Log fvl
                                          INNER JOIN dbo.Contract_Notification_Log_Meter_Dtl fvmd
                                                ON fvl.Contract_Notification_Log_Id = fvmd.Contract_Notification_Log_Id
                                          INNER JOIN dbo.Notification_Msg_Queue nmq
                                                ON fvl.Notification_Msg_Queue_Id = nmq.Notification_Msg_Queue_Id
                                          INNER JOIN dbo.Notification_Template nt
                                                ON nmq.Notification_Template_Id = nt.Notification_Template_Id
                                          INNER JOIN dbo.Code cd
                                                ON nt.Notification_Type_Cd = cd.Code_Id
                                          INNER JOIN dbo.Codeset cs
                                                ON cd.Codeset_Id = cs.Codeset_Id
                                         WHERE
                                          cs.Codeset_Name = 'Notification Type'
                                          AND cd.Code_Value IN ( 'Registration Notification' )
                                          AND chasupp.Supplier_Contract_ID = fvl.Contract_Id
                                          AND chasupp.Meter_Id = fvmd.Meter_Id
                                          AND chasupp.Supplier_Meter_Association_Date = fvl.Meter_Term_Dt )
                  GROUP BY
                        ch.client_name
                       ,ch.Client_Id
                       ,ch.city
                       ,ch.state_name
                       ,ch.site_name
                       ,ch.Site_Id
                       ,chautil.account_number
                       ,chautil.Alternate_Account_Number
                       ,chautil.account_id
                       ,chasupp.Meter_Id
                       ,chasupp.Meter_Number
                       ,con.Contract_Id
                       ,con.Ed_Contract_Number
                       ,chasupp.Account_Vendor_Id
                       ,chasupp.Account_Vendor_Name
                       ,chasupp.Supplier_Meter_Association_Date
                       ,vcam.Analyst_Id
                       ,ui.first_name
                       ,ui.last_name
                       ,ui.EMAIL_ADDRESS
                       ,chautil.Commodity_Id
                       ,chautil.Account_Analyst_Mapping_Cd
                       ,ch.Site_Analyst_Mapping_Cd
                       ,ch.Client_Analyst_Mapping_Cd
                       ,ui.PHONE_NUMBER
                       ,Product_Type.Code_Value
                       ,c.Commodity_Name
                       
      INSERT      INTO @Tbl_Regisrations
                  ( 
                   Client_Name
                  ,Site_Name
                  ,Site_Id
                  ,Account_Number
                  ,Alternate_Account_Number
                  ,Account_Id
                  ,Meter_Id
                  ,Meter_Number
                  ,Contract_Id
                  ,Ed_Contract_Number
                  ,START_DATE
                  ,Vendor_Id
                  ,Vendor_Name
                  ,Analyst_Id
                  ,Analyst_Name
                  ,Analyst_Email_Address
                  ,COMMODITY_TYPE_ID
                  ,Commodity_Name
                  ,Phone_Number
                  ,Product_Type )
                  SELECT
                        cn.Client_Name
                       ,cn.Site_Name
                       ,cn.Site_Id
                       ,cn.Account_Number
                       ,cn.Alternate_Account_Number
                       ,cn.Account_Id
                       ,cn.Meter_Id
                       ,cn.Meter_Number
                       ,cn.Contract_Id
                       ,cn.Ed_Contract_Number
                       ,cn.Start_Date
                       ,cn.Vendor_Id
                       ,cn.Vendor_Name
                       ,CASE WHEN cn.Analyst_Mapping_Cd = @Default_Analyst THEN cn.Analyst_Id
                             WHEN cn.Analyst_Mapping_Cd = @Custom_Analyst THEN COALESCE(aca.Analyst_User_Info_Id, sca.Analyst_User_Info_Id, cca.Analyst_User_Info_Id)
                        END AS Analyst_Id
                       ,CASE WHEN cn.Analyst_Mapping_Cd = @Default_Analyst THEN cn.Analyst_Name
                             WHEN cn.Analyst_Mapping_Cd = @Custom_Analyst THEN COALESCE(uia.first_name + SPACE(1) + uia.last_name, uis.first_name + SPACE(1) + uis.last_name, uic.first_name + SPACE(1) + uic.last_name)
                        END AS Analyst_Name
                       ,CASE WHEN cn.Analyst_Mapping_Cd = @Default_Analyst THEN cn.Analyst_Email_Address
                             WHEN cn.Analyst_Mapping_Cd = @Custom_Analyst THEN COALESCE(uia.EMAIL_ADDRESS, uis.EMAIL_ADDRESS, uic.EMAIL_ADDRESS)
                        END AS Analyst_Email_Address
                       ,cn.COMMODITY_TYPE_ID
                       ,cn.Commodity_Name
                       ,CASE WHEN cn.Analyst_Mapping_Cd = @Default_Analyst THEN cn.Phone_Number
                             WHEN cn.Analyst_Mapping_Cd = @Custom_Analyst THEN COALESCE(uia.PHONE_NUMBER, uis.PHONE_NUMBER, uic.PHONE_NUMBER)
                        END AS Phone_Number
                       ,cn.Product_Type
                  FROM
                        @Client_Con_Info cn
                        JOIN core.Client_Commodity ccc
                              ON ccc.Client_Id = cn.Client_Id
                                 AND ccc.Commodity_Id = cn.COMMODITY_TYPE_ID
                        LEFT JOIN dbo.Account_Commodity_Analyst aca
                              ON aca.Account_Id = cn.Account_Id
                                 AND aca.Commodity_Id = cn.COMMODITY_TYPE_ID
                        LEFT JOIN dbo.USER_INFO uia
                              ON aca.Analyst_User_Info_Id = uia.USER_INFO_ID
                        LEFT JOIN dbo.SITE_Commodity_Analyst sca
                              ON sca.Site_Id = cn.Site_Id
                                 AND sca.Commodity_Id = cn.COMMODITY_TYPE_ID
                        LEFT JOIN dbo.USER_INFO uis
                              ON sca.Analyst_User_Info_Id = uis.USER_INFO_ID
                        LEFT JOIN dbo.Client_Commodity_Analyst cca
                              ON ccc.Client_Commodity_Id = cca.Client_Commodity_Id
                        LEFT JOIN dbo.USER_INFO uic
                              ON cca.Analyst_User_Info_Id = uic.USER_INFO_ID
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          Core.Client_Hier_Account samesupp
                                     WHERE
                                          samesupp.Meter_Id = cn.Meter_Id
                                          AND samesupp.Account_Vendor_Id = cn.Vendor_Id
                                          AND samesupp.Supplier_Contract_ID <> cn.Contract_Id
                                          AND DATEADD(day, 1, samesupp.Supplier_Meter_Disassociation_Date) = cn.Start_Date )
                  GROUP BY
                        cn.Client_Name
                       ,cn.Site_Name
                       ,cn.Site_Id
                       ,cn.Account_Number
                       ,cn.Alternate_Account_Number
                       ,cn.Account_Id
                       ,cn.Meter_Id
                       ,cn.Meter_Number
                       ,cn.Contract_Id
                       ,cn.Ed_Contract_Number
                       ,cn.Vendor_Id
                       ,cn.Vendor_Name
                       ,cn.Analyst_Mapping_Cd
                       ,CASE WHEN cn.Analyst_Mapping_Cd = @Default_Analyst THEN cn.Analyst_Id
                             WHEN cn.Analyst_Mapping_Cd = @Custom_Analyst THEN COALESCE(aca.Analyst_User_Info_Id, sca.Analyst_User_Info_Id, cca.Analyst_User_Info_Id)
                        END
                       ,CASE WHEN cn.Analyst_Mapping_Cd = @Default_Analyst THEN cn.Analyst_Name
                             WHEN cn.Analyst_Mapping_Cd = @Custom_Analyst THEN COALESCE(uia.first_name + SPACE(1) + uia.last_name, uis.first_name + SPACE(1) + uis.last_name, uic.first_name + SPACE(1) + uic.last_name)
                        END
                       ,CASE WHEN cn.Analyst_Mapping_Cd = @Default_Analyst THEN cn.Analyst_Email_Address
                             WHEN cn.Analyst_Mapping_Cd = @Custom_Analyst THEN COALESCE(uia.EMAIL_ADDRESS, uis.EMAIL_ADDRESS, uic.EMAIL_ADDRESS)
                        END
                       ,cn.COMMODITY_TYPE_ID
                       ,cn.Start_Date
                       ,CASE WHEN cn.Analyst_Mapping_Cd = @Default_Analyst THEN cn.Phone_Number
                             WHEN cn.Analyst_Mapping_Cd = @Custom_Analyst THEN COALESCE(uia.PHONE_NUMBER, uis.PHONE_NUMBER, uic.PHONE_NUMBER)
                        END
                       ,cn.Product_Type
                       ,cn.Commodity_Name
                       
                       
      INSERT      INTO @Tbl_Site_Meter_Number
                  ( 
                   Site_Name
                  ,Site_Id
                  ,Site_Meter_Number
                  ,Contract_Id )
                  SELECT
                        cter.Site_Name
                       ,cter.Site_Id
                       ,cter.Site_Name + '~' + ISNULL(LEFT(mtrnum.mtrnums, LEN(mtrnum.mtrnums) - 1), '') + '~' + ISNULL(LEFT(accnum.accnums, LEN(accnum.accnums) - 1), '') + '~' + ISNULL(LEFT(altacc.altaccs, LEN(altacc.altaccs) - 1), '')
                       ,cter.Contract_Id
                  FROM
                        @Tbl_Regisrations cter
                        CROSS APPLY ( SELECT
                                          ct.Meter_Number + ', '
                                      FROM
                                          @Tbl_Regisrations ct
                                      WHERE
                                          cter.Contract_Id = ct.Contract_Id
                                          AND cter.Site_Id = ct.Site_Id
                                      GROUP BY
                                          ct.Meter_Number
                                         ,ct.Meter_Id
                        FOR
                                      XML PATH('') ) mtrnum ( mtrnums )
                        CROSS APPLY ( SELECT
                                          ct.Account_Number + ', '
                                      FROM
                                          @Tbl_Regisrations ct
                                      WHERE
                                          cter.Contract_Id = ct.Contract_Id
                                          AND cter.Site_Id = ct.Site_Id
                                      GROUP BY
                                          ct.Account_Number
                        FOR
                                      XML PATH('') ) accnum ( accnums )
                        CROSS APPLY ( SELECT
                                          ct.Alternate_Account_Number + ', '
                                      FROM
                                          @Tbl_Regisrations ct
                                      WHERE
                                          cter.Contract_Id = ct.Contract_Id
                                          AND cter.Site_Id = ct.Site_Id
                                          AND ct.Alternate_Account_Number IS NOT NULL
                                      GROUP BY
                                          ct.Alternate_Account_Number
                        FOR
                                      XML PATH('') ) altacc ( altaccs )
                  GROUP BY
                        cter.Site_Name
                       ,cter.Site_Id
                       ,cter.Site_Name + '~' + ISNULL(LEFT(mtrnum.mtrnums, LEN(mtrnum.mtrnums) - 1), '') + '~' + ISNULL(LEFT(accnum.accnums, LEN(accnum.accnums) - 1), '') + '~' + ISNULL(LEFT(altacc.altaccs, LEN(altacc.altaccs) - 1), '')
                       ,cter.Contract_Id
            
            
      SELECT
            cter.Contract_Id
           ,cter.Ed_Contract_Number
           ,cter.Start_Date
           ,cter.Client_Name
           ,REPLACE(LEFT(sit.sites, LEN(sit.sites) - 1), '&amp;', '&') AS Site_Name
           ,LEFT(mtr.mtrs, LEN(mtr.mtrs) - 1) AS Meter_Id
           ,LEFT(mtrnum.mtrnums, LEN(mtrnum.mtrnums) - 1) AS Meter_Number
           ,cter.Vendor_Name
           ,LEFT(vcui.vcuis, LEN(vcui.vcuis) - 1) AS Vendor_Contact_User_Info_Id
           ,LEFT(tcmail.tcmails, LEN(tcmail.tcmails) - 1) AS Registration_Contact_Email_Address
           ,LEFT(auid.auids, LEN(auid.auids) - 1) AS Analyst_Id
           ,LEFT(aname.anames, LEN(aname.anames) - 1) AS Analyst_Name
           ,LEFT(aea.aeas, LEN(aea.aeas) - 1) AS Analyst_Email_Address
           ,LEFT(pn.pns, LEN(pn.pns) - 1) AS Phone_Number
           ,cter.COMMODITY_TYPE_ID
           ,cter.Commodity_Name
           ,cter.Product_Type
           ,LEFT(sitemtr.sitemtrs, LEN(sitemtr.sitemtrs) - 1) AS Site_Meter_Number
           ,LEFT(reqmail.reqmails, LEN(reqmail.reqmails) - 1) AS Requested_User_Info_Id_Email_Address
           ,LEFT(locale.code, LEN(locale.code) - 1) AS Locale_Cd
      FROM
            @Tbl_Regisrations cter
            CROSS APPLY ( SELECT
                              ct.Site_Name + '^'
                          FROM
                              @Tbl_Regisrations ct
                          WHERE
                              cter.Contract_Id = ct.Contract_Id
                          GROUP BY
                              ct.Site_Name
            FOR
                          XML PATH('') ) sit ( sites )
            CROSS APPLY ( SELECT
                              CAST(ct.Meter_Id AS VARCHAR(20)) + ','
                          FROM
                              @Tbl_Regisrations ct
                          WHERE
                              cter.Contract_Id = ct.Contract_Id
                              AND ct.Meter_Id IS NOT NULL
                          GROUP BY
                              ct.Meter_Id
            FOR
                          XML PATH('') ) mtr ( mtrs )
            CROSS APPLY ( SELECT
                              ct.Meter_Number + ', '
                          FROM
                              @Tbl_Regisrations ct
                          WHERE
                              cter.Contract_Id = ct.Contract_Id
                              AND ct.Meter_Number IS NOT NULL
                          GROUP BY
                              ct.Meter_Number
                             ,ct.Meter_Id
            FOR
                          XML PATH('') ) mtrnum ( mtrnums )
            CROSS APPLY ( SELECT
                              CAST(ssci.USER_INFO_ID AS VARCHAR(20)) + ','
                          FROM
                              dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP sscvm
                              INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                                    ON sscvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
                              INNER JOIN dbo.USER_INFO AS vci
                                    ON vci.USER_INFO_ID = ssci.USER_INFO_ID
                              INNER JOIN dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP metermap
                                    ON metermap.SR_SUPPLIER_CONTACT_INFO_ID = sscvm.SR_SUPPLIER_CONTACT_INFO_ID
                              INNER JOIN Core.Client_Hier_Account cha
                                    ON metermap.COUNTRY_ID = cha.Meter_Country_Id
                                       AND metermap.STATE_ID = cha.Meter_State_Id
                                       AND metermap.COMMODITY_TYPE_ID = cha.Commodity_Id
                              INNER JOIN @Tbl_Regisrations mtr
                                    ON mtr.Meter_Id = cha.Meter_Id
                          WHERE
                              sscvm.IS_PRIMARY = 1
                              AND cter.Vendor_Id = sscvm.VENDOR_ID
                              AND ssci.Registration_Contact_Email_Address IS NOT NULL
                              AND ssci.Registration_Contact_Email_Address <> ''
                              AND vci.IS_HISTORY = 0
                              AND metermap.IS_PRIMARY = 0
                              AND cha.Supplier_Contract_ID = cter.Contract_Id
                          GROUP BY
                              ssci.USER_INFO_ID
            FOR
                          XML PATH('') ) vcui ( vcuis )
            CROSS APPLY ( SELECT
                              ssci.Registration_Contact_Email_Address + ','
                          FROM
                              dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP sscvm
                              INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                                    ON sscvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
                              INNER JOIN dbo.USER_INFO AS vci
                                    ON vci.USER_INFO_ID = ssci.USER_INFO_ID
                              INNER JOIN dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP metermap
                                    ON metermap.SR_SUPPLIER_CONTACT_INFO_ID = sscvm.SR_SUPPLIER_CONTACT_INFO_ID
                              INNER JOIN Core.Client_Hier_Account cha
                                    ON metermap.COUNTRY_ID = cha.Meter_Country_Id
                                       AND metermap.STATE_ID = cha.Meter_State_Id
                                       AND metermap.COMMODITY_TYPE_ID = cha.Commodity_Id
                              INNER JOIN @Tbl_Regisrations mtr
                                    ON mtr.Meter_Id = cha.Meter_Id
                          WHERE
                              sscvm.IS_PRIMARY = 1
                              AND cter.Vendor_Id = sscvm.VENDOR_ID
                              AND ssci.Registration_Contact_Email_Address IS NOT NULL
                              AND ssci.Registration_Contact_Email_Address <> ''
                              AND vci.IS_HISTORY = 0
                              AND metermap.IS_PRIMARY = 0
                              AND cha.Supplier_Contract_ID = cter.Contract_Id
                          GROUP BY
                              ssci.Registration_Contact_Email_Address
            FOR
                          XML PATH('') ) tcmail ( tcmails )
            CROSS APPLY ( SELECT
                              CAST(ct.Analyst_Id AS VARCHAR(20)) + ','
                          FROM
                              @Tbl_Regisrations ct
                          WHERE
                              cter.Contract_Id = ct.Contract_Id
                              AND ct.Analyst_Id IS NOT NULL
                          GROUP BY
                              ct.Analyst_Id
            FOR
                          XML PATH('') ) auid ( auids )
            CROSS APPLY ( SELECT
                              ct.Analyst_Name + ','
                          FROM
                              @Tbl_Regisrations ct
                          WHERE
                              cter.Contract_Id = ct.Contract_Id
                              AND ct.Analyst_Name IS NOT NULL
                          GROUP BY
                              ct.Analyst_Name
            FOR
                          XML PATH('') ) aname ( anames )
            CROSS APPLY ( SELECT
                              ct.Analyst_Email_Address + ','
                          FROM
                              @Tbl_Regisrations ct
                          WHERE
                              cter.Contract_Id = ct.Contract_Id
                              AND ct.Analyst_Email_Address IS NOT NULL
                          GROUP BY
                              ct.Analyst_Email_Address
            FOR
                          XML PATH('') ) aea ( aeas )
            CROSS APPLY ( SELECT
                              ct.Phone_Number + ','
                          FROM
                              @Tbl_Regisrations ct
                          WHERE
                              cter.Contract_Id = ct.Contract_Id
                              AND ct.Phone_Number IS NOT NULL
                          GROUP BY
                              ct.Phone_Number
            FOR
                          XML PATH('') ) pn ( pns )
            CROSS APPLY ( SELECT
                              ct.Site_Meter_Number + '^'
                          FROM
                              @Tbl_Site_Meter_Number ct
                          WHERE
                              cter.Contract_Id = ct.Contract_Id
                              AND ct.Site_Meter_Number IS NOT NULL
                          GROUP BY
                              ct.Site_Meter_Number
            FOR
                          XML PATH('') ) sitemtr ( sitemtrs )
            CROSS APPLY ( SELECT
                              CAST(ct.Analyst_Id AS VARCHAR(20)) + '~' + ct.Analyst_Email_Address + '^'
                          FROM
                              @Tbl_Regisrations ct
                          WHERE
                              cter.Contract_Id = ct.Contract_Id
                              AND ct.Analyst_Email_Address IS NOT NULL
                          GROUP BY
                              ct.Analyst_Id
                             ,ct.Analyst_Email_Address
            FOR
                          XML PATH('') ) reqmail ( reqmails )
            CROSS APPLY ( SELECT TOP 1
                              CAST(cd.Code_Id AS VARCHAR(10)) + '~' + cd.Code_Value + ','
                          FROM
                              dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP sscvm
                              INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                                    ON sscvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
                              INNER JOIN dbo.USER_INFO AS vci
                                    ON vci.USER_INFO_ID = ssci.USER_INFO_ID
                              INNER JOIN dbo.Code cd
                                    ON cd.Code_Value = vci.locale_code
                              INNER JOIN dbo.Codeset cs
                                    ON cd.Codeset_Id = cs.Codeset_Id
                              INNER JOIN dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP metermap
                                    ON metermap.SR_SUPPLIER_CONTACT_INFO_ID = sscvm.SR_SUPPLIER_CONTACT_INFO_ID
                              INNER JOIN Core.Client_Hier_Account cha
                                    ON metermap.COUNTRY_ID = cha.Meter_Country_Id
                                       AND metermap.STATE_ID = cha.Meter_State_Id
                                       AND metermap.COMMODITY_TYPE_ID = cha.Commodity_Id
                              INNER JOIN @Tbl_Regisrations mtr
                                    ON mtr.Meter_Id = cha.Meter_Id
                          WHERE
                              sscvm.IS_PRIMARY = 1
                              AND cter.Vendor_Id = sscvm.VENDOR_ID
                              AND ssci.Registration_Contact_Email_Address IS NOT NULL
                              AND ssci.Registration_Contact_Email_Address <> ''
                              AND vci.IS_HISTORY = 0
                              AND cs.Codeset_Name = 'LocalizationLanguage'
                              AND metermap.IS_PRIMARY = 0
                              AND cha.Supplier_Contract_ID = cter.Contract_Id
            FOR
                          XML PATH('') ) locale ( code )
      GROUP BY
            cter.Contract_Id
           ,cter.Ed_Contract_Number
           ,cter.Start_Date
           ,cter.Client_Name
           ,REPLACE(LEFT(sit.sites, LEN(sit.sites) - 1), '&amp;', '&')
           ,LEFT(mtr.mtrs, LEN(mtr.mtrs) - 1)
           ,LEFT(mtrnum.mtrnums, LEN(mtrnum.mtrnums) - 1)
           ,cter.Vendor_Name
           ,LEFT(vcui.vcuis, LEN(vcui.vcuis) - 1)
           ,LEFT(tcmail.tcmails, LEN(tcmail.tcmails) - 1)
           ,LEFT(auid.auids, LEN(auid.auids) - 1)
           ,LEFT(aname.anames, LEN(aname.anames) - 1)
           ,LEFT(aea.aeas, LEN(aea.aeas) - 1)
           ,LEFT(pn.pns, LEN(pn.pns) - 1)
           ,cter.COMMODITY_TYPE_ID
           ,cter.Commodity_Name
           ,cter.Product_Type
           ,LEFT(sitemtr.sitemtrs, LEN(sitemtr.sitemtrs) - 1)
           ,LEFT(reqmail.reqmails, LEN(reqmail.reqmails) - 1)
           ,LEFT(locale.code, LEN(locale.code) - 1)
    
END








;
GO

GRANT EXECUTE ON  [dbo].[Contract_Registration_Sel_Eligible_For_Notifications] TO [CBMSApplication]
GO
