SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.DV2GetIPDeltaDeletes

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

--Mark McCallon
--11-19-07
--delta delete for dv2_ip
--
create procedure dbo.DV2GetIPDeltaDeletes
as
begin
set nocount on

delete dbo.dv2_ip from dbo.dv2_ip a
left outer join
tempdb.dbo.dv2_ip_delta b
on a.site_id=b.site_id and a.account_id=b.account_id and a.commodity_type_id=b.commodity_type_id and
a.service_month_id=b.service_month_id
where b.site_id is null

end
GO
GRANT EXECUTE ON  [dbo].[DV2GetIPDeltaDeletes] TO [CBMSApplication]
GO
