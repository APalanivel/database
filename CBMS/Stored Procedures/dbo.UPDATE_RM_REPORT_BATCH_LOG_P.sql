SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.UPDATE_RM_REPORT_BATCH_LOG_P
@userId varchar(10),
@sessionId varchar(10),
@reportBatchId int,
@reportId int,
@clientId int,
@siteId int,
@divisionId int,
@batchEvent varchar(100),
@reportFor int,
@groupId int

as
	set nocount on
	if @reportFor=1
	
		insert into RM_REPORT_BATCH_LOG(RM_REPORT_BATCH_ID,RM_REPORT_ID,CLIENT_ID,LOG_TIME,BATCH_EVENT) values (@reportBatchId,@reportId,@clientId,GETDATE(),@batchEvent)
	
	else if @reportFor=2
	
		insert into RM_REPORT_BATCH_LOG(RM_REPORT_BATCH_ID,RM_REPORT_ID,CLIENT_ID,DIVISION_ID,LOG_TIME,BATCH_EVENT) values (@reportBatchId,@reportId,@clientId,@divisionId,GETDATE(),@batchEvent)
	
	else if @reportFor=3
	
		insert into RM_REPORT_BATCH_LOG(RM_REPORT_BATCH_ID,RM_REPORT_ID,CLIENT_ID,SITE_ID,LOG_TIME,BATCH_EVENT) values (@reportBatchId,@reportId,@clientId,@siteId,GETDATE(),@batchEvent)

	else if @reportFor=4
	
		insert into RM_REPORT_BATCH_LOG(RM_REPORT_BATCH_ID,RM_REPORT_ID,CLIENT_ID,RM_GROUP_ID,LOG_TIME,BATCH_EVENT) values (@reportBatchId,@reportId,@clientId,@groupId,GETDATE(),@batchEvent)
GO
GRANT EXECUTE ON  [dbo].[UPDATE_RM_REPORT_BATCH_LOG_P] TO [CBMSApplication]
GO
