SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--Mark McCallon
--8-14-08
--get total invoices ready for 3rd stage
CREATE procedure dbo.dbagetprocessqueue
as
begin
set nocount on

select count(*) from  ubm_invoice  with (nolock) where cu_invoice_batch_id is null and
	       is_processed = 0
	      and is_quarterly = 0
	      and ubm_client_id is not null
	      and cbms_image_id is not null

end

GO
GRANT EXECUTE ON  [dbo].[dbagetprocessqueue] TO [CBMSApplication]
GO
