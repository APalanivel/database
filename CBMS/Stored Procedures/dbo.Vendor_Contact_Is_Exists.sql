SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
	dbo.Vendor_Contact_Is_Exists

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default		Description
------------------------------------------------------------
	@First_Name		VARCHAR(40)
    @Last_Name		VARCHAR(40)
    @Email_Address	VARCHAR(150)
    @Vendor_Id		INT
    @SR_SUPPLIER_CONTACT_INFO_ID	INT				NULL	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	SELECT TOP 10
		ui.FIRST_NAME,ui.LAST_NAME,ui.EMAIL_ADDRESS,sscvm.VENDOR_ID
	FROM
		dbo.SR_Supplier_Contact_Vendor_Map sscvm
		INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
			  ON sscvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
		INNER JOIN dbo.User_Info ui
			  ON ssci.USER_INFO_ID = ui.USER_INFO_ID
	WHERE
		ui.is_History = 0
		AND sscvm.Is_Primary = 1
		
		
	EXEC dbo.Vendor_Contact_Is_Exists 'Joe','Rose','DoNotReply@ems.schneider-electric.com',757
	EXEC dbo.Vendor_Contact_Is_Exists 'Joe','Rose1','DoNotReply@ems.schneider-electric.com',757
	
	EXEC dbo.Vendor_Contact_Is_Exists 'Douglas','Tom','DoNotReply@ems.schneider-electric.com',699
	EXEC dbo.Vendor_Contact_Is_Exists 'Douglas1','Tom','DoNotReply@ems.schneider-electric.com',699



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	RR			2018-06-26	Global Risk Management - Created
******/
CREATE PROCEDURE [dbo].[Vendor_Contact_Is_Exists]
      ( 
       @First_Name VARCHAR(40)
      ,@Last_Name VARCHAR(40)
      ,@Email_Address VARCHAR(150)
      ,@Vendor_Id INT
      ,@SR_SUPPLIER_CONTACT_INFO_ID INT = NULL )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Vendor_Contact_Is_Exists BIT = 0
      
      SELECT
            @Vendor_Contact_Is_Exists = 1
      FROM
            dbo.SR_Supplier_Contact_Vendor_Map sscvm
            INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                  ON sscvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
            INNER JOIN dbo.User_Info ui
                  ON ssci.USER_INFO_ID = ui.USER_INFO_ID
      WHERE
            ui.is_History = 0
            AND sscvm.Is_Primary = 1
            AND ui.FIRST_NAME = @First_Name
            AND ui.LAST_NAME = @Last_Name
            AND ui.EMAIL_ADDRESS = @Email_Address
            AND sscvm.VENDOR_ID = @Vendor_Id
            AND ( @SR_SUPPLIER_CONTACT_INFO_ID IS NULL
                  OR sscvm.SR_SUPPLIER_CONTACT_INFO_ID <> @SR_SUPPLIER_CONTACT_INFO_ID )
            
      SELECT
            @Vendor_Contact_Is_Exists AS Vendor_Contact_Is_Exists
     
        
END;


GO
GRANT EXECUTE ON  [dbo].[Vendor_Contact_Is_Exists] TO [CBMSApplication]
GO
