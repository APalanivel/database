SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	cbms.dbo.Determinant_Bucket_Master_Id_Sel_By_Ubm_Commodity_Bucket_Cd

	DESCRIPTION: It Displays Bucket Master Id for the given UBM Id , Commodity Id ,UBM Bucket Code.

INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@Ubm_Id				INT
    @commodity_type_id		INT
    @ubm_bucket_code		VARCHAR(200) 
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC dbo.Determinant_Bucket_Master_Id_Sel_By_Ubm_Commodity_Bucket_Cd 1,291,'Natural Gas'
	EXEC dbo.Determinant_Bucket_Master_Id_Sel_By_Ubm_Commodity_Bucket_Cd 1,290,'Electric'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	PNR			08/19/2010	Created
	SKA			08/31/2010	Renamed the input parameters
	SKA			09/01/2010	renamed the column name
	
******/

CREATE PROCEDURE dbo.Determinant_Bucket_Master_Id_Sel_By_Ubm_Commodity_Bucket_Cd
      @Ubm_Id				INT
     ,@commodity_type_id			INT
     ,@ubm_bucket_code		VARCHAR(200) 
AS 
BEGIN
    SET NOCOUNT ON ;

	SELECT 
		Bucket_Master_Id AS Bucket_Type_ID
	FROM	
		dbo.Ubm_Bucket_Determinant_Map
	WHERE
		UBM_ID = @Ubm_Id
		AND Commodity_Type_Id = @commodity_type_id
		AND UBM_Bucket_Code   = @ubm_bucket_code
END
GO
GRANT EXECUTE ON  [dbo].[Determinant_Bucket_Master_Id_Sel_By_Ubm_Commodity_Bucket_Cd] TO [CBMSApplication]
GO
