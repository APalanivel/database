SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE           procedure [dbo].[cbmsRpt_ServiceMonth_LoadForReportYear]
	( @MyAccountId int
	, @session_uid uniqueidentifier
	, @report_year int
	, @client_id int
	)
AS
BEGIN

set nocount on

	  declare @begin_date datetime
		, @end_date datetime
		, @service_year int

	  declare @month_number int

	   select @month_number = start_month
	     from vwClientFiscalYearStartMonth WITH (NOLOCK)
	    where client_id = @client_id

	if @month_number is null
	begin
		set @month_number = 1
	end

	if @month_number = 1
	begin
	      set @begin_date 	= convert(datetime, '1/1/' + convert(varchar, @report_year))
	      set @end_date 	= convert(datetime, '12/31/' + convert(varchar, @report_year))
	end
	else
	begin
	      set @begin_date 	= convert(datetime, convert(varchar,@month_number) + '/1/' + convert(varchar, @report_year - 1))
	      set @end_date 	= dateadd(d, -1, dateadd(m,12,@begin_date))
	end

     --exec cbmsRpt_ServiceMonth_RemoveForSession @MyAccountId, @session_uid

	set @month_number = 1

    while @begin_date <= @end_date
    begin

	insert into rpt_service_month ( session_uid, service_month, report_year, month_number ) 
	     values ( @session_uid, @begin_date, @report_year, @month_number )

		set @begin_date = dateadd("m", 1, @begin_date)

		set @month_number = @month_number + 1

    end


END
GO
GRANT EXECUTE ON  [dbo].[cbmsRpt_ServiceMonth_LoadForReportYear] TO [CBMSApplication]
GO
