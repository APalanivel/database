SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
CREATE    PROCEDURE DBO.UPDATE_DEAL_TICKET_OPTIONS_DETAILS_P

@userId varchar(10),
@sessionId varchar(16),
@dealTicketOptionDetailsId int, 
@optionsVolume decimal(32,16), 
@strikePrice decimal(32,16), 
@premiumPrice decimal(32,16)

AS
	set nocount on
UPDATE 
	RM_DEAL_TICKET_OPTION_DETAILS 
SET 
	VOLUME = @optionsVolume, 
	STRIKE_PRICE = @strikePrice, 
	PREMIUM_PRICE = @premiumPrice 
WHERE 
	RM_DEAL_TICKET_OPTION_DETAILS_ID = @dealTicketOptionDetailsId

GO
GRANT EXECUTE ON  [dbo].[UPDATE_DEAL_TICKET_OPTIONS_DETAILS_P] TO [CBMSApplication]
GO
