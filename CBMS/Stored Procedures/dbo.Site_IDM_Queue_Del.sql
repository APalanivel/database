
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME: [dbo].[Site_IDM_Queue_Del]
     
DESCRIPTION: 
	procedure to update IDM Node flag
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
 
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        

	 exec Site_IDM_Queue_Del

AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------
	KVK		Vinay K

MODIFICATIONS

INITIALS	DATE		MODIFICATION
------------------------------------------------------------          
	KVK		12/14/2014	created
	KVK		05/01/2017	Modified to join with input parameter
*/
CREATE PROCEDURE [dbo].[Site_IDM_Queue_Del]
AS
BEGIN
      SET NOCOUNT ON;
      DELETE
            siq
      FROM
            dbo.Site_IDM_Queue siq
      WHERE
            siq.Last_Change_Ts = '1900-01-01';

END;
;
GO

GRANT EXECUTE ON  [dbo].[Site_IDM_Queue_Del] TO [CBMSApplication]
GO
