SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.RM_Budget_Current_Hedge_Position_Sel_By_Rm_Budget_Id                        
                          
Description:                          
        To get market price and forecast pirce if a selected index   
                          
Input Parameters:                          
    Name    DataType        Default     Description                            
--------------------------------------------------------------------------------    
	@Index_Id   INT    
    @Start_Dt	Date    
	@End_Dt		Date
                          
 Output Parameters:                                
	Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
Usage Examples:                              
--------------------------------------------------------------------------------    
	SELECT * FROM dbo.ENTITY e WHERE e.ENTITY_TYPE=272

	EXEC Trade.RM_Budget_Current_Hedge_Position_Sel_By_Rm_Budget_Id  96
    
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR			2019-12-23	RM-Budgets Enahancement - Created
	                
******/
CREATE PROCEDURE [Trade].[RM_Budget_Current_Hedge_Position_Sel_By_Rm_Budget_Id]
    (
        @Rm_Budget_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            rb.Rm_Budget_Id
            , dd.DATE_D AS Budget_Month
            , rbchpd.Total_Volume_Hedged AS Total_Volume_Hedged
            , CAST(rbchpd.Hedged_Unit_Cost AS DECIMAL(28, 3)) AS Hedged_Unit_Cost
            , CAST(((rbchpd.Total_Volume_Hedged / rbchpd.Total_Forecast_Volume) * 100) AS DECIMAL(22, 0)) AS Hedged_Percent_Of_Total_Usage
            , CAST((((rbchpd.Total_Forecast_Volume - rbchpd.Total_Volume_Hedged) / rbchpd.Total_Forecast_Volume) * 100) AS DECIMAL(22, 0)) AS Unhedged_Percent_Of_Total_Usage
            , (rbchpd.Total_Forecast_Volume - rbchpd.Total_Volume_Hedged) AS Total_Volume_Unhedged
            , rbchpd.Total_Forecast_Volume AS Forecasted_Volume
        FROM
            Trade.Rm_Budget rb
            CROSS JOIN meta.DATE_DIM dd
            INNER JOIN Trade.Rm_Budget_Current_Hedge_Position_Dtl rbchpd
                ON rbchpd.Rm_Budget_Id = rb.Rm_Budget_Id
                   AND  dd.DATE_D = rbchpd.Service_Month
        WHERE
            rb.Rm_Budget_Id = @Rm_Budget_Id
            AND dd.DATE_D BETWEEN rb.Budget_Start_Dt
                          AND     rb.Budget_End_Dt;
    END;

GO
GRANT EXECUTE ON  [Trade].[RM_Budget_Current_Hedge_Position_Sel_By_Rm_Budget_Id] TO [CBMSApplication]
GO
