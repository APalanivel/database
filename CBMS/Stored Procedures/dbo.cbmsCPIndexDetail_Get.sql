SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsCPIndexDetail_Get

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@clearport_index_detail_id	int       	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE    PROCEDURE [dbo].[cbmsCPIndexDetail_Get]
	( @clearport_index_detail_id int = null
	)
AS
BEGIN

	   select clearport_index_detail_id
		, index_detail_value
		, index_detail_date
		, clearport_index_month_id	
	     from clearport_index_detail with (nolock)
	    where clearport_index_detail_id = @clearport_index_detail_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCPIndexDetail_Get] TO [CBMSApplication]
GO
