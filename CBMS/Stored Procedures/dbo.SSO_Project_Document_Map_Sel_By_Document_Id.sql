SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******************************************************************************************************         
NAME: dbo.SSO_Project_document_Map_Sel_By_Document_Id
  
DESCRIPTION:    
    
      To check whether document project mapping exists.
    
INPUT PARAMETERS:    
      NAME					DATATYPE		DEFAULT           DESCRIPTION    
------------------------------------------------------------------------    
@Document_Id				INT			
                    
OUTPUT PARAMETERS:              
      NAME              DATATYPE    DEFAULT           DESCRIPTION       
           
------------------------------------------------------------              
USAGE EXAMPLES:              
------------------------------------------------------------            

EXEC dbo.SSO_Project_document_Map_Sel_By_Document_Id  5331
	 
AUTHOR INITIALS:              
      INITIALS    NAME              
------------------------------------------------------------              
      RMG         RANI MARY GEORGE   
	      
MODIFICATIONS:    
      INITIALS    DATE				MODIFICATION              
------------------------------------------------------------              
      RMG 		  12-SEP-14			CREATED
******************************************************************************************************/ 
CREATE PROCEDURE [dbo].[SSO_Project_Document_Map_Sel_By_Document_Id] 
	( 
		@Document_Id INT 
	)
AS 
BEGIN

      SET NOCOUNT ON ;

      SELECT
            1
      WHERE
            EXISTS ( SELECT
                        1
                     FROM
                        [dbo].[SSO_Project_document_Map] spdm
                     WHERE
                        spdm.SSO_Document_Id = @Document_Id )

END


;
GO
GRANT EXECUTE ON  [dbo].[SSO_Project_Document_Map_Sel_By_Document_Id] TO [CBMSApplication]
GO
