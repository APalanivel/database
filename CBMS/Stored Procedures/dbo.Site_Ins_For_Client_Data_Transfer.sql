SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******************************************************************************************************
NAME : dbo.[Site_Ins_For_Client_Data_Transfer]

DESCRIPTION: This procedure used to transfer sites from a source client to destination client.  

 INPUT PARAMETERS:      

 Name			DataType		Default			Description      
--------------------------------------------------------------------        

 @Message		XML   

 OUTPUT PARAMETERS:      

 Name   DataType  Default Description      
--------------------------------------------------------------------      
  	
  USAGE EXAMPLES:
--------------------------------------------------------------------  
BEGIN TRAN
	DECLARE  @Client_Hier_Id INT
	   
	EXEC [Site_Ins_For_Client_Data_Transfer] 
		@From_Site_Client_Hier_Id = 1142 
		,@To_Client_Id = 109
		,@To_Division_Client_Hier_Id = 32399 
		,@Client_Hier_Id = @Client_Hier_Id OUT
	
	SELECT @Client_Hier_Id
ROLLBACK TRAN
		
AUTHOR INITIALS:      

 Initials		Name      
-------------------------------------------------------------------       
 MSV			Muhamed Shahid V

 MODIFICATIONS  

 Initials		Date			Modification  
--------------------------------------------------------------------  
 MSV			18 Jul 2019		Created 	
 
*****************************************************************************************************/
CREATE PROCEDURE [dbo].[Site_Ins_For_Client_Data_Transfer]
	(
		@From_Site_Client_Hier_Id INT,
		@To_Client_Id INT,
		@To_Division_Client_Hier_Id INT,
		@Client_Hier_Id INT OUT
	)
AS
BEGIN
	
    SET NOCOUNT ON

    DECLARE
        @From_Site_Id INT
		,@To_Division_Id INT
        ,@Site_name VARCHAR(200)
        ,@Site_Id INT
        ,@User_Info_Id INT
		,@SITE_TYPE_ID INT
        ,@UBMSITE_ID VARCHAR(30)
        ,@SITE_PRODUCT_SERVICE VARCHAR(2000)
        ,@PRODUCTION_SCHEDULE VARCHAR(2000)
        ,@SHUTDOWN_SCHEDULE VARCHAR(2000)
        ,@IS_ALTERNATE_POWER BIT
        ,@IS_ALTERNATE_GAS BIT
        ,@DOING_BUSINESS_AS VARCHAR(200)
        ,@NAICS_CODE VARCHAR(30)
        ,@TAX_NUMBER VARCHAR(30)
        ,@DUNS_NUMBER VARCHAR(30)
        ,@CLOSED BIT
        ,@NOT_MANAGED BIT
        ,@CONTRACTING_ENTITY VARCHAR(200)
        ,@CLIENT_LEGAL_STRUCTURE VARCHAR(4000)
        ,@firstName VARCHAR(50)
        ,@lastName VARCHAR(50)
        ,@contactEmailId VARCHAR(50)
        ,@emailCC VARCHAR(400)
        ,@emailApproval BIT
        ,@dbViewApproval BIT
        ,@Analyst_Mapping_Cd INT
        ,@Weather_Station_Cd VARCHAR(10)
        ,@Site_Reference_Number VARCHAR(30)
		,@addressTypeId INT
        ,@stateId INT
        ,@addressLine1 VARCHAR(200)
        ,@addressLine2 VARCHAR(200)
        ,@city VARCHAR(80)
        ,@zipcode VARCHAR(20)
        ,@addressParentTypeId INT
        ,@latitude DECIMAL(32, 16)
        ,@longitude DECIMAL(32, 16)
        ,@addressId INT
        ,@Is_System_Generated_Geocode BIT 
		,@Time_Zone_Id INT
		
    SELECT
        @User_Info_Id = ui.User_Info_Id
    FROM
        dbo.USER_INFO AS ui
    WHERE
        ui.username = 'conversion'
	
	BEGIN TRY
		BEGIN TRAN;
		
		SELECT @From_Site_Id = ch.Site_Id
		FROM Core.Client_Hier ch 
		WHERE ch.Client_Hier_Id = @From_Site_Client_Hier_Id   
		
		SELECT @To_Division_Id = ch.Sitegroup_Id
		FROM Core.Client_Hier ch 
		WHERE ch.Client_Hier_Id = @To_Division_Client_Hier_Id   
		        
        SELECT
            @Site_Name = SITE_NAME
            ,@SITE_TYPE_ID = SITE_TYPE_ID
            ,@UBMSITE_ID = UBMSITE_ID
            ,@SITE_PRODUCT_SERVICE = SITE_PRODUCT_SERVICE
            ,@PRODUCTION_SCHEDULE = PRODUCTION_SCHEDULE
            ,@SHUTDOWN_SCHEDULE = SHUTDOWN_SCHEDULE
            ,@IS_ALTERNATE_POWER = IS_ALTERNATE_POWER
            ,@IS_ALTERNATE_GAS = IS_ALTERNATE_GAS
            ,@DOING_BUSINESS_AS = DOING_BUSINESS_AS
            ,@NAICS_CODE = NAICS_CODE
            ,@TAX_NUMBER = TAX_NUMBER
            ,@DUNS_NUMBER = DUNS_NUMBER
			,@CLOSED = CLOSED
            ,@NOT_MANAGED = NOT_MANAGED
			,@CONTRACTING_ENTITY = CONTRACTING_ENTITY
            ,@CLIENT_LEGAL_STRUCTURE = CLIENT_LEGAL_STRUCTURE
            ,@firstName = LP_CONTACT_FIRST_NAME
            ,@lastName = LP_CONTACT_LAST_NAME
            ,@contactEmailId = LP_CONTACT_EMAIL_ADDRESS
            ,@emailCC = LP_CONTACT_CC
            ,@emailApproval = IS_PREFERENCE_BY_EMAIL
            ,@dbViewApproval = IS_PREFERENCE_BY_DV
            ,@Analyst_Mapping_Cd = Analyst_Mapping_Cd
            ,@Weather_Station_Cd = Weather_Station_Code
            ,@Site_Reference_Number = SITE_REFERENCE_NUMBER
			,@Time_Zone_Id = Time_Zone_Id
        FROM
            dbo.SITE
        WHERE
            SITE_ID = @From_Site_Id				
		
        SELECT
            @addressTypeId = a.ADDRESS_TYPE_ID
            ,@stateId = a.STATE_ID
            ,@addressLine1 = a.ADDRESS_LINE1
            ,@addressLine2 = a.ADDRESS_LINE2
            ,@city = a.city
            ,@zipcode = a.zipcode
            ,@addressParentTypeId = a.ADDRESS_PARENT_TYPE_ID
            ,@latitude = a.GEO_LAT
            ,@longitude = a.GEO_LONG
            ,@addressId = a.ADDRESS_ID
            ,@Is_System_Generated_Geocode = a.Is_System_Generated_Geocode
        FROM
            dbo.ADDRESS a
            INNER JOIN site s
                    ON s.PRIMARY_ADDRESS_ID = a.ADDRESS_ID
        WHERE
            s.SITE_ID = @From_Site_Id
		
		--Sitegroup insert
		EXEC dbo.CBMS_ADD_SITE_P
            @SITE_TYPE_ID = @SITE_TYPE_ID
            ,@SITE_NAME = @Site_Name
            ,@DIVISION_ID = @To_Division_Id
            ,@UBMSITE_ID = @UBMSITE_ID
            ,@SITE_REFERENCE_NUMBER = @site_reference_number
            ,@SITE_PRODUCT_SERVICE = @SITE_PRODUCT_SERVICE
            ,@PRODUCTION_SCHEDULE = @PRODUCTION_SCHEDULE
            ,@SHUTDOWN_SCHEDULE = @SHUTDOWN_SCHEDULE
            ,@IS_ALTERNATE_POWER = @IS_ALTERNATE_POWER
            ,@IS_ALTERNATE_GAS = @IS_ALTERNATE_GAS
            ,@DOING_BUSINESS_AS = @DOING_BUSINESS_AS
            ,@NAICS_CODE = @NAICS_CODE
            ,@TAX_NUMBER = @TAX_NUMBER
            ,@DUNS_NUMBER = @DUNS_NUMBER
            ,@CLOSED = @CLOSED
            ,@NOT_MANAGED = @NOT_MANAGED
            ,@CONTRACTING_ENTITY = @CONTRACTING_ENTITY
            ,@CLIENT_LEGAL_STRUCTURE = @CLIENT_LEGAL_STRUCTURE
            ,@firstName = @firstName
            ,@lastName = @lastName
            ,@contactEmailId = @contactEmailId
            ,@emailCC = @emailCC
            ,@emailApproval = @emailApproval
            ,@dbViewApproval = @dbViewApproval
            ,@Client_Id = @To_Client_Id
            ,@Analyst_Mapping_Cd = @Analyst_Mapping_Cd
			,@Weather_Station_Code = @Weather_Station_Cd
			,@Time_Zone_Id = @Time_Zone_Id
            ,@siteId = @Site_Id OUTPUT
		
		--Address insert
        EXEC CBMS_ADD_SITE_ADDRESS_P
            @addressTypeId
            ,@stateId
			,@addressLine1
            ,@addressLine2
            ,@city
            ,@zipcode
            ,@Site_Id
            ,@addressParentTypeId
            ,@latitude
            ,@longitude
            ,@addressId OUT
            ,@Is_System_Generated_Geocode
		
		--Address id update
        EXECUTE CBMS_UPDATE_SITE_ADDRESSID_P
            @addressId
            ,@Site_Id		
		
        EXEC dbo.ADD_ENTITY_AUDIT_ITEM_P
            @entity_identifier = @Site_Id
            ,@user_info_id = @User_Info_Id
            ,@audit_type = 1
            ,@entity_name = 'SITE_TABLE'
            ,@entity_type = 500

        EXEC dbo.Add_Site_To_Roles
            @Site_Id = @Site_Id

		SELECT @Client_Hier_Id = ch.Client_Hier_Id
		FROM Core.Client_Hier ch
		WHERE ch.Site_Id = @Site_Id

		COMMIT TRAN;
	END TRY
	BEGIN CATCH

		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRAN;
		END;

		EXEC dbo.usp_RethrowError;
	END CATCH;
END;

GO
GRANT EXECUTE ON  [dbo].[Site_Ins_For_Client_Data_Transfer] TO [CBMSApplication]
GO
