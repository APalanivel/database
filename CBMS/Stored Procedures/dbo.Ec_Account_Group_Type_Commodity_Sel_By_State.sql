SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Ec_Account_Group_Type_Commodity_Sel_By_State        
                
Description:                
		This sproc to get the Group types based country and state and commodity.        
                             
 Input Parameters:                
    Name						DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
	@State_Id					INT
      
 Output Parameters:                      
    Name					  DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
----------------------------------------------------------------------------------------  
   
   Exec dbo.Ec_Account_Group_Type_Commodity_Sel_By_State   134  
   
   Exec dbo.Ec_Account_Group_Type_Commodity_Sel_By_State   236 
 
       
   
Author Initials:                
    Initials		Name                
----------------------------------------------------------------------------------------                  
	NR				Narayana Reddy                 
 Modifications:                
    Initials        Date			Modification                
----------------------------------------------------------------------------------------                  
    NR				2016-11-15		Created For MAINT-4563.           
               
******/   
CREATE PROCEDURE [dbo].[Ec_Account_Group_Type_Commodity_Sel_By_State] ( @State_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 

      SELECT
            com.Commodity_Id
           ,com.Commodity_Name
      FROM
            dbo.Ec_Account_Group_Type eagt
            INNER JOIN dbo.Commodity com
                  ON com.Commodity_Id = eagt.Commodity_Id
      WHERE
            eagt.State_Id = @State_Id
      GROUP BY
            com.Commodity_Id
           ,com.Commodity_Name
      ORDER BY
            com.Commodity_Name
            
      
END
      


;
GO
GRANT EXECUTE ON  [dbo].[Ec_Account_Group_Type_Commodity_Sel_By_State] TO [CBMSApplication]
GO
