SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********
NAME:  dbo.TOU_Schedule_Upd

DESCRIPTION:  
	Insert a new schedule

INPUT PARAMETERS:
Name						DataType        Default     Description
-------------------------------------------------------------------
@Time_Of_Use_Schedule_Id	INT
@Schedule_Name				NVARCHAR(100)
@Comment_Id					INT
@Comment_Text				NVARCHAR(max)	NULL
@Comment_User_Id			INT				NULL
				

OUTPUT PARAMETERS:
      Name								DataType          Default     Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------

Exec dbo.TOU_Schedule_Upd 3, 'Test_Update_Schedule', NULL, 'Applicable to single- and three-phase general service including lighting and power customers whose monthly Maximum Demand registers, or in the opinion of SCE is expected to register, above 20 kW', 49


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CPE			Chaitanya Panduga Eshwar


	Initials Date		Modification
------------------------------------------------------------
	CPE		2012-07-11  Created

******/
CREATE PROCEDURE dbo.TOU_Schedule_Upd
( 
 @Time_Of_Use_Schedule_Id INT
,@Schedule_Name NVARCHAR(100)
,@Comment_Id INT = NULL
,@Comment_Text NVARCHAR(max) = NULL
,@Comment_User_Id INT = NULL )
AS 
BEGIN
      SET NOCOUNT ON

      DECLARE
            @Comment_Type_Cd INT
           ,@Comment_Dt DATETIME
                 
      BEGIN TRY
            BEGIN TRAN

            IF @Comment_Id IS NOT NULL 
                  BEGIN
                        EXECUTE dbo.Comment_Upd @Comment_Id = @Comment_Id, @Comment_Text = @Comment_Text
                  END
            ELSE 
                  IF @Comment_Id IS NULL
                        AND @Comment_Text IS NOT NULL 
                        BEGIN
                              SELECT
                                    @Comment_Type_Cd = CD.Code_Id
                              FROM
                                    dbo.Code CD
                                    INNER JOIN dbo.Codeset CS
                                          ON CD.Codeset_Id = CS.Codeset_Id
                              WHERE
                                    CD.Code_Value = 'TOUSComment'
                                    AND CS.Codeset_Name = 'CommentType'

                              SELECT
                                    @Comment_Dt = GETDATE()
                              
                              EXECUTE dbo.Comment_Ins @Comment_Text = @Comment_Text, 
									  @Comment_User_Info_Id = @Comment_User_Id, 
									  @Comment_Type_Cd = @Comment_Type_Cd, 
									  @Comment_Dt = @Comment_Dt, 
									  @Comment_Id = @Comment_Id OUTPUT
                        END
            UPDATE
                  dbo.Time_Of_Use_Schedule
            SET   
                  Schedule_Name = @Schedule_Name
                 ,Comment_ID = @Comment_Id
            WHERE
                  Time_Of_Use_Schedule_Id = @Time_Of_Use_Schedule_Id
			
            COMMIT TRAN
      END TRY
      BEGIN CATCH
            IF @@TRANCOUNT > 0 
                  ROLLBACK
            EXEC usp_RethrowError
      END CATCH

END 


;
GO
GRANT EXECUTE ON  [dbo].[TOU_Schedule_Upd] TO [CBMSApplication]
GO
