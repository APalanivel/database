SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE dbo.GET_SUPPLIER_HEADER_INFO_P
	@vendorId INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT v.vendor_name,
		detail.is_base_agreement,
		detail.is_supplier_credit,
		detail.comments,
		detail.is_minority
	FROM dbo.supplier_detail detail INNER JOIN dbo.vendor v ON v.vendor_id = detail.vendor_id
	WHERE v.vendor_id = @vendorId

END
GO
GRANT EXECUTE ON  [dbo].[GET_SUPPLIER_HEADER_INFO_P] TO [CBMSApplication]
GO
