SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_STATES_FOR_REGION_ID_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@regionId      	varchar(10)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE  dbo.SR_SAD_GET_STATES_FOR_REGION_ID_P  (
	@regionId varchar(10)
)

AS
set nocount on
Select 	STATE_ID,
	STATE_NAME  
	from dbo.STATE s 
	where s.REGION_ID=@regionId
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_STATES_FOR_REGION_ID_P] TO [CBMSApplication]
GO
