SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--exec GET_SITES_UNDER_DIVISION_P -1,-1,357
--select * from site where division_id=357
--select * from division where division_id=357
--select * from RM_ONBOARD_HEDGE_SETUP where division_id=357



CREATE       PROCEDURE dbo.BUDGET_GET_SITES_UNDER_DIVISION_FOR_INITIATE_P 
@userId varchar,
@sessionId varchar,
@divisionId int
as
select	viewSite.SITE_ID,
	viewSite.SITE_NAME

from	
	VWSITENAME viewSite,
	site site

where 	
	viewSite.DIVISION_ID=@divisionId
	and viewSite.site_id = site.site_id
	and site.not_managed= 0
	and site.closed= 0		

order by viewSite.SITE_NAME
GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_SITES_UNDER_DIVISION_FOR_INITIATE_P] TO [CBMSApplication]
GO
