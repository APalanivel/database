SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- exec dbo.SR_SW_GET_RFP_FEEDBACK_REPORT_FOR_SUPPLIER_CONTACT_DATA_P 8380,'','','',0,0,0,0,0,0,0

CREATE PROCEDURE dbo.SR_SW_GET_RFP_FEEDBACK_REPORT_FOR_SUPPLIER_CONTACT_DATA_P
	@userId VARCHAR(20),
	@sessionId VARCHAR(20),
	@fromDate VARCHAR(20),
	@todate VARCHAR(20),
	@commodityId INT,
	@regionId INT,
	@stateId INT,
	@utilityId INT,
	@supplierId INT,
	@analystId INT,
	@rfpId INT 

AS
BEGIN

	SET NOCOUNT ON	

	SET @fromDate = ISNULL(@fromDate,'')
	SET @todate = ISNULL(@todate,'')

	/* Query 1 - Start */


	IF @stateId = 0
	 BEGIN

		SELECT SR_RFP.SR_RFP_ID as RFP_ID
			, SR_RFP_REASON_NOT_WINNING.COMMENTS as COMMENTS
			, supplier.VENDOR_NAME AS SUPPLIER_NAME
			, utility.VENDOR_NAME AS UTILITY_NAME
			, reason.ENTITY_NAME AS REASON 
			, site.site_name AS CITY_STATE
			, client.CLIENT_NAME as CLIENT_NAME
			, commodity.ENTITY_NAME AS COMMODITY
			, ACCOUNT.ACCOUNT_NUMBER AS BID_GROUP  
			, initiated.FIRST_NAME + SPACE(1) + initiated.LAST_NAME AS ANALYST
		FROM dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP  map
			JOIN dbo.sr_rfp_send_supplier send ON send.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_id = map.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_id
			JOIN dbo.sr_supplier_contact_info contactinfo ON  contactinfo.sr_supplier_contact_info_id = map.sr_supplier_contact_info_id
			JOIN dbo.SR_RFP_ACCOUNT sracc ON send.SR_ACCOUNT_GROUP_ID = sracc.sr_rfp_account_id
				AND sracc.is_deleted = 0
			JOIN dbo.sr_rfp_checklist rfpChecklist ON 	sracc.sr_rfp_account_id = rfpChecklist.sr_rfp_account_id
			JOIN dbo.SR_RFP ON sracc.SR_RFP_ID = SR_RFP.SR_RFP_ID 
			JOIN dbo.SR_RFP_REASON_NOT_WINNING ON sracc.SR_RFP_ACCOUNT_ID = SR_RFP_REASON_NOT_WINNING.SR_ACCOUNT_GROUP_ID
				AND SR_RFP_REASON_NOT_WINNING.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = send.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
			JOIN dbo.SR_RFP_REASON_NOT_WINNING_MAP ON SR_RFP_REASON_NOT_WINNING_MAP.SR_RFP_REASON_NOT_WINNING_ID = SR_RFP_REASON_NOT_WINNING.SR_RFP_REASON_NOT_WINNING_ID
			JOIN dbo.VENDOR supplier ON map.VENDOR_ID = supplier.VENDOR_ID
			JOIN dbo.ACCOUNT ON sracc.ACCOUNT_ID = ACCOUNT.ACCOUNT_ID
			JOIN dbo.VENDOR utility ON ACCOUNT.VENDOR_ID = utility.VENDOR_ID
			JOIN dbo.ENTITY reason ON SR_RFP_REASON_NOT_WINNING_MAP.NOT_WINNING_REASON_TYPE_ID = reason.ENTITY_ID
			JOIN dbo.ENTITY commodity ON SR_RFP.COMMODITY_TYPE_ID = commodity.ENTITY_ID
			JOIN dbo.USER_INFO initiated ON SR_RFP.INITIATED_BY = initiated.USER_INFO_ID
			JOIN dbo.vwSiteName site ON ACCOUNT.SITE_ID = site.SITE_ID
			JOIN dbo.CLIENT client ON site.CLIENT_ID = client.CLIENT_ID
		WHERE (SR_RFP_REASON_NOT_WINNING.IS_BID_GROUP = 0
			AND contactinfo.user_info_id = @userId
			AND send.is_bid_group = 0
			AND initiated.is_history = 0)
			AND (@rfpId = 0 OR SR_RFP.SR_RFP_ID = @rfpId)
			AND (@commodityId = 0 OR SR_RFP.COMMODITY_TYPE_ID = @commodityId)
			--AND (@regionId  = 0 OR REGION.REGION_ID = @regionId)
			AND (@utilityId = 0 OR utility.VENDOR_ID = @utilityId)
			AND (@analystId = 0 OR SR_RFP.INITIATED_BY = @analystId)
			AND ( (@fromDate = '' AND @toDate = '')
				OR ((@fromDate <> '' AND @toDate <> '')
					AND rfpChecklist.rfp_initiated_date BETWEEN CONVERT(VARCHAR(12), @fromDate,101)  AND CONVERT(VARCHAR(12), @toDate,101)
				   )
				OR ((@fromDate ='' and @toDate <>'')
					AND rfpChecklist.rfp_initiated_date <= CONVERT(Varchar(12), @toDate,101)
				   )
				OR ((@fromDate <> '' and @toDate ='')
					AND rfpChecklist.rfp_initiated_date >= CONVERT(VARCHAR(12), @fromDate,101)
				   )
				)
		
		UNION

		SELECT SR_RFP.SR_RFP_ID as RFP_ID
			, SR_RFP_REASON_NOT_WINNING.COMMENTS AS COMMENTS
			, supplier.VENDOR_NAME AS SUPPLIER_NAME
			, utility.VENDOR_NAME AS UTILITY_NAME
			, reason.ENTITY_NAME AS REASON 
			, site.site_name AS CITY_STATE
			, client.CLIENT_NAME AS CLIENT_NAME
			, commodity.ENTITY_NAME AS COMMODITY
			, SR_RFP_BID_GROUP.GROUP_NAME AS BID_GROUP
			, initiated.FIRST_NAME + SPACE(1) + initiated.LAST_NAME AS ANALYST
		FROM dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP  map
			JOIN dbo.sr_rfp_send_supplier send  ON send.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_id = map.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_id
			JOIN dbo.sr_supplier_contact_info contactinfo ON contactinfo.sr_supplier_contact_info_id = map.sr_supplier_contact_info_id
			JOIN dbo.SR_RFP_ACCOUNT sracc ON send.SR_ACCOUNT_GROUP_ID = sracc.sr_rfp_bid_group_id
				AND sracc.is_deleted = 0
			JOIN dbo.sr_rfp_checklist rfpChecklist ON 	sracc.sr_rfp_account_id = rfpChecklist.sr_rfp_account_id
			JOIN dbo.SR_RFP_BID_GROUP ON send.SR_ACCOUNT_GROUP_ID = SR_RFP_BID_GROUP.SR_RFP_BID_GROUP_ID
			JOIN dbo.SR_RFP ON sracc.SR_RFP_ID = SR_RFP.SR_RFP_ID 
			JOIN dbo.SR_RFP_REASON_NOT_WINNING ON sracc.sr_rfp_bid_group_id = SR_RFP_REASON_NOT_WINNING.SR_ACCOUNT_GROUP_ID
				AND SR_RFP_REASON_NOT_WINNING.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = send.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
			JOIN dbo.SR_RFP_REASON_NOT_WINNING_MAP ON SR_RFP_REASON_NOT_WINNING_MAP.SR_RFP_REASON_NOT_WINNING_ID = SR_RFP_REASON_NOT_WINNING.SR_RFP_REASON_NOT_WINNING_ID
			JOIN dbo.VENDOR supplier ON map.VENDOR_ID = supplier.VENDOR_ID 
			JOIN dbo.ACCOUNT ON sracc.ACCOUNT_ID = ACCOUNT.ACCOUNT_ID 
			JOIN dbo.VENDOR utility ON ACCOUNT.VENDOR_ID = utility.VENDOR_ID 
			JOIN dbo.ENTITY reason ON SR_RFP_REASON_NOT_WINNING_MAP.NOT_WINNING_REASON_TYPE_ID = reason.ENTITY_ID 
			JOIN dbo.ENTITY commodity ON SR_RFP.COMMODITY_TYPE_ID = commodity.ENTITY_ID 
			JOIN dbo.USER_INFO initiated ON SR_RFP.INITIATED_BY = initiated.USER_INFO_ID 
			JOIN dbo.vwSiteName site ON ACCOUNT.SITE_ID = site.SITE_ID 
			JOIN dbo.CLIENT client ON site.CLIENT_ID = client.CLIENT_ID
		WHERE (SR_RFP_REASON_NOT_WINNING.IS_BID_GROUP = 1
			AND contactinfo.user_info_id = @userId
			AND send.is_bid_group = 1 and initiated.is_history = 0 )
			AND (@rfpId = 0 OR SR_RFP.SR_RFP_ID = @rfpId)
			AND (@commodityId = 0 OR SR_RFP.COMMODITY_TYPE_ID = @commodityId)
			--AND (@regionId  = 0 OR REGION.REGION_ID = @regionId)
			AND (@utilityId = 0 OR utility.VENDOR_ID = @utilityId)
			AND (@analystId = 0 OR SR_RFP.INITIATED_BY = @analystId)
			AND ( (@fromDate = '' AND @toDate = '')
				OR ((@fromDate <> '' AND @toDate <> '')
					AND rfpChecklist.rfp_initiated_date BETWEEN CONVERT(VARCHAR(12), @fromDate,101)  AND CONVERT(VARCHAR(12), @toDate,101)
				   )
				OR ((@fromDate ='' and @toDate <>'')
					AND rfpChecklist.rfp_initiated_date <= CONVERT(Varchar(12), @toDate,101)
				   )
				OR ((@fromDate <> '' and @toDate ='')
					AND rfpChecklist.rfp_initiated_date >= CONVERT(VARCHAR(12), @fromDate,101)
				   )
				)		

	 END
	ELSE
	 BEGIN

		SELECT SR_RFP.SR_RFP_ID as RFP_ID
			, SR_RFP_REASON_NOT_WINNING.COMMENTS as COMMENTS
			, supplier.VENDOR_NAME AS SUPPLIER_NAME
			, utility.VENDOR_NAME AS UTILITY_NAME
			, reason.ENTITY_NAME AS REASON 
			, site.site_name AS CITY_STATE
			, client.CLIENT_NAME as CLIENT_NAME
			, commodity.ENTITY_NAME AS COMMODITY
			, ACCOUNT.ACCOUNT_NUMBER AS BID_GROUP
			, initiated.FIRST_NAME + SPACE(1) + initiated.LAST_NAME AS ANALYST
		FROM dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP  map
			JOIN dbo.sr_rfp_send_supplier send ON send.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_id = map.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_id
			JOIN dbo.sr_supplier_contact_info contactinfo ON  contactinfo.sr_supplier_contact_info_id = map.sr_supplier_contact_info_id
			JOIN dbo.SR_RFP_ACCOUNT sracc ON send.SR_ACCOUNT_GROUP_ID = sracc.sr_rfp_account_id
				AND sracc.is_deleted = 0
			JOIN dbo.sr_rfp_checklist rfpChecklist ON 	sracc.sr_rfp_account_id = rfpChecklist.sr_rfp_account_id
			JOIN dbo.SR_RFP ON sracc.SR_RFP_ID = SR_RFP.SR_RFP_ID 
			JOIN dbo.SR_RFP_REASON_NOT_WINNING ON sracc.SR_RFP_ACCOUNT_ID = SR_RFP_REASON_NOT_WINNING.SR_ACCOUNT_GROUP_ID
				AND SR_RFP_REASON_NOT_WINNING.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = send.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
			JOIN dbo.SR_RFP_REASON_NOT_WINNING_MAP ON SR_RFP_REASON_NOT_WINNING_MAP.SR_RFP_REASON_NOT_WINNING_ID = SR_RFP_REASON_NOT_WINNING.SR_RFP_REASON_NOT_WINNING_ID
			JOIN dbo.VENDOR supplier ON map.VENDOR_ID = supplier.VENDOR_ID
			JOIN dbo.ACCOUNT ON sracc.ACCOUNT_ID = ACCOUNT.ACCOUNT_ID
			JOIN dbo.VENDOR utility ON ACCOUNT.VENDOR_ID = utility.VENDOR_ID
			JOIN dbo.ENTITY reason ON SR_RFP_REASON_NOT_WINNING_MAP.NOT_WINNING_REASON_TYPE_ID = reason.ENTITY_ID
			JOIN dbo.ENTITY commodity ON SR_RFP.COMMODITY_TYPE_ID = commodity.ENTITY_ID
			JOIN dbo.USER_INFO initiated ON SR_RFP.INITIATED_BY = initiated.USER_INFO_ID
			JOIN dbo.vwSiteName site ON ACCOUNT.SITE_ID = site.SITE_ID
			JOIN dbo.CLIENT client ON site.CLIENT_ID = client.CLIENT_ID
			JOIN dbo.VENDOR_STATE_MAP ON utility.VENDOR_ID = VENDOR_STATE_MAP.VENDOR_ID 
			JOIN dbo.STATE ON VENDOR_STATE_MAP.STATE_ID = STATE.STATE_ID
		WHERE (SR_RFP_REASON_NOT_WINNING.IS_BID_GROUP = 0
			AND contactinfo.user_info_id = @userId
			AND send.is_bid_group = 0
			AND initiated.is_history = 0)
			AND (@rfpId = 0 OR SR_RFP.SR_RFP_ID = @rfpId)
			AND (@commodityId = 0 OR SR_RFP.COMMODITY_TYPE_ID = @commodityId)
			--AND (@regionId  = 0 OR REGION.REGION_ID = @regionId)
			AND (@utilityId = 0 OR utility.VENDOR_ID = @utilityId)
			AND (@analystId = 0 OR SR_RFP.INITIATED_BY = @analystId)
			AND ( (@fromDate = '' AND @toDate = '')
				OR ((@fromDate <> '' AND @toDate <> '')
					AND rfpChecklist.rfp_initiated_date BETWEEN CONVERT(VARCHAR(12), @fromDate,101)  AND CONVERT(VARCHAR(12), @toDate,101)
				   )
				OR ((@fromDate ='' and @toDate <>'')
					AND rfpChecklist.rfp_initiated_date <= CONVERT(Varchar(12), @toDate,101)
				   )
				OR ((@fromDate <> '' and @toDate ='')
					AND rfpChecklist.rfp_initiated_date >= CONVERT(VARCHAR(12), @fromDate,101)
				   )
				)
			AND VENDOR_STATE_MAP.STATE_ID = @stateId
		
		UNION

		SELECT SR_RFP.SR_RFP_ID as RFP_ID
			, SR_RFP_REASON_NOT_WINNING.COMMENTS AS COMMENTS
			, supplier.VENDOR_NAME AS SUPPLIER_NAME
			, utility.VENDOR_NAME AS UTILITY_NAME
			, reason.ENTITY_NAME AS REASON 
			, site.site_name AS CITY_STATE
			, client.CLIENT_NAME AS CLIENT_NAME
			, commodity.ENTITY_NAME AS COMMODITY
			, SR_RFP_BID_GROUP.GROUP_NAME AS BID_GROUP
			, initiated.FIRST_NAME + SPACE(1) + initiated.LAST_NAME AS ANALYST
		FROM dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP  map
			JOIN dbo.sr_rfp_send_supplier send  ON send.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_id = map.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_id
			JOIN dbo.sr_supplier_contact_info contactinfo ON contactinfo.sr_supplier_contact_info_id = map.sr_supplier_contact_info_id
			JOIN dbo.SR_RFP_ACCOUNT sracc ON send.SR_ACCOUNT_GROUP_ID = sracc.sr_rfp_bid_group_id
				AND sracc.is_deleted = 0
			JOIN dbo.sr_rfp_checklist rfpChecklist ON 	sracc.sr_rfp_account_id = rfpChecklist.sr_rfp_account_id
			JOIN dbo.SR_RFP_BID_GROUP ON send.SR_ACCOUNT_GROUP_ID = SR_RFP_BID_GROUP.SR_RFP_BID_GROUP_ID
			JOIN dbo.SR_RFP ON sracc.SR_RFP_ID = SR_RFP.SR_RFP_ID 
			JOIN dbo.SR_RFP_REASON_NOT_WINNING ON sracc.sr_rfp_bid_group_id = SR_RFP_REASON_NOT_WINNING.SR_ACCOUNT_GROUP_ID
				AND SR_RFP_REASON_NOT_WINNING.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = send.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
			JOIN dbo.SR_RFP_REASON_NOT_WINNING_MAP ON SR_RFP_REASON_NOT_WINNING_MAP.SR_RFP_REASON_NOT_WINNING_ID = SR_RFP_REASON_NOT_WINNING.SR_RFP_REASON_NOT_WINNING_ID
			JOIN dbo.VENDOR supplier ON map.VENDOR_ID = supplier.VENDOR_ID 
			JOIN dbo.ACCOUNT ON sracc.ACCOUNT_ID = ACCOUNT.ACCOUNT_ID 
			JOIN dbo.VENDOR utility ON ACCOUNT.VENDOR_ID = utility.VENDOR_ID 
			JOIN dbo.ENTITY reason ON SR_RFP_REASON_NOT_WINNING_MAP.NOT_WINNING_REASON_TYPE_ID = reason.ENTITY_ID 
			JOIN dbo.ENTITY commodity ON SR_RFP.COMMODITY_TYPE_ID = commodity.ENTITY_ID 
			JOIN dbo.USER_INFO initiated ON SR_RFP.INITIATED_BY = initiated.USER_INFO_ID 
			JOIN dbo.vwSiteName site ON ACCOUNT.SITE_ID = site.SITE_ID 
			JOIN dbo.CLIENT client ON site.CLIENT_ID = client.CLIENT_ID 
			JOIN dbo.VENDOR_STATE_MAP ON utility.VENDOR_ID = VENDOR_STATE_MAP.VENDOR_ID 
			JOIN dbo.STATE ON VENDOR_STATE_MAP.STATE_ID = STATE.STATE_ID
		WHERE (SR_RFP_REASON_NOT_WINNING.IS_BID_GROUP = 1
			AND contactinfo.user_info_id = @userId
			AND send.is_bid_group = 1 and initiated.is_history = 0 )
			AND (@rfpId = 0 OR SR_RFP.SR_RFP_ID = @rfpId)
			AND (@commodityId = 0 OR SR_RFP.COMMODITY_TYPE_ID = @commodityId)
			--AND (@regionId  = 0 OR REGION.REGION_ID = @regionId)
			AND (@utilityId = 0 OR utility.VENDOR_ID = @utilityId)
			AND (@analystId = 0 OR SR_RFP.INITIATED_BY = @analystId)
			AND ( (@fromDate = '' AND @toDate = '')
				OR ((@fromDate <> '' AND @toDate <> '')
					AND rfpChecklist.rfp_initiated_date BETWEEN CONVERT(VARCHAR(12), @fromDate,101)  AND CONVERT(VARCHAR(12), @toDate,101)
				   )
				OR ((@fromDate ='' and @toDate <>'')
					AND rfpChecklist.rfp_initiated_date <= CONVERT(VARCHAR(12), @toDate,101)
				   )
				OR ((@fromDate <> '' and @toDate ='')
					AND rfpChecklist.rfp_initiated_date >= CONVERT(VARCHAR(12), @fromDate,101)
				   )
				)
			AND VENDOR_STATE_MAP.STATE_ID = @stateId
			
	 END

END
GO
GRANT EXECUTE ON  [dbo].[SR_SW_GET_RFP_FEEDBACK_REPORT_FOR_SUPPLIER_CONTACT_DATA_P] TO [CBMSApplication]
GO
