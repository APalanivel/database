SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******    
NAME:    
[dbo].[Commodity_Count_Sel_By_COMMODITY]  

DESCRIPTION:  
 
Used to insert and update from COMMODITY table  

INPUT PARAMETERS:    
Name		     DataType	Default Description    
------------------------------------------------------------    
@Commodity_Name		VARCHAR(250)
          
OUTPUT PARAMETERS:    
Name		     DataType	Default Description    
------------------------------------------------------------    
USAGE EXAMPLES:    
------------------------------------------------------------  

[dbo].[Commodity_Count_Sel_By_COMMODITY] 'test'  
[dbo].[Commodity_Count_Sel_By_COMMODITY] 'Water'  


AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
GB   Geetansu Behera    
CMH  Chad Hattabaugh     
HG   Hari   

MODIFICATIONS     
Initials Date		Modification    
------------------------------------------------------------    
GB	06-sep-09		created  
GB  16-sep-2009		combine both select into one

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.Commodity_Count_Sel_By_COMMODITY
(
 @Commodity_Name VARCHAR(250)
)
AS 

BEGIN  
 
      SET NOCOUNT ON ;  
	
      SELECT
            CASE WHEN COUNT(1) > 0 THEN @Commodity_Name
                 ELSE NULL
            END AS 'Duplicate_Commodity'
      FROM
            dbo.COMMODITY
      WHERE
            IS_SUSTAINABLE = 1
            AND IS_ACTIVE = 1
            AND COMMODITY_NAME = @Commodity_Name
END
GO
GRANT EXECUTE ON  [dbo].[Commodity_Count_Sel_By_COMMODITY] TO [CBMSApplication]
GO
