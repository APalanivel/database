
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
    
NAME: [DBO].[GET_ACTIVE_IMAGE_REPOSITORY_P]      
         
DESCRIPTION:     
     
          
INPUT PARAMETERS:              
NAME       DATATYPE DEFAULT  DESCRIPTION              
------------------------------------------------------------              
                    
OUTPUT PARAMETERS:              
NAME   DATATYPE DEFAULT  DESCRIPTION       
           
------------------------------------------------------------              
USAGE EXAMPLES:              
------------------------------------------------------------      
    
 EXEC GET_ACTIVE_IMAGE_REPOSITORY_P 27004    
 EXEC GET_ACTIVE_IMAGE_REPOSITORY_P   
     
     
AUTHOR INITIALS:              
INITIALS NAME              
------------------------------------------------------------              
RR   RAGHUREDDY    
AC   Ajay Chejarla  
JR   Jishnu
    
MODIFICATIONS    
INITIALS DATE  MODIFICATION    
------------------------------------------------------------              
RR  13-JUNE-11 MAINT-703 Removed the GRANT permission TO "dv2javauser"    
      Comments header added    
      Removed the NOLOCK optimizer hint   
        
AC  08-18-2014  Added optional cbms_image_location parameter   
JR	08-19-2014	Added optional Image_Type_ID parameter and Client_ID Parameter
				Added logic to return Image Location ID based on the client set up	
    
*/    
    
CREATE PROCEDURE [dbo].[GET_ACTIVE_IMAGE_REPOSITORY_P]
      (
       @Image_Type_ID INT = NULL
      ,@Client_ID INT = NULL
      ,@CBMS_Image_Location_Id INT = NULL )
AS
BEGIN    
    
      SET NOCOUNT ON; 

      DECLARE @Xid VARCHAR(15)    
	
      SELECT
            @Xid = App_Config_Value
      FROM
            dbo.App_Config
      WHERE
            App_Config_Cd = 'Xid'
            AND Is_Active = 1    
 

      SELECT
            cil.CBMS_Image_Location_Id AS ActiveDriveID
           ,Image_Drive AS ActiveDrive
           ,Active_Directory AS ActiveFolder
           ,Max_File_Cnt AS MaxFileCount
           ,Active_File_Cnt AS ActualFileCount
           ,@Xid Xid
           ,c.Code_Value AS StorageType
           ,UserName
           ,[Password]
      FROM
            CBMS_Image_Location cil
            JOIN Code c
                  ON c.Code_Id = cil.Storage_Type_Cd
            LEFT OUTER JOIN Client_Image_Location_Map cilm
                  ON ( cilm.CBMS_Image_Location_Id = cil.CBMS_Image_Location_Id )
      WHERE
            ( @CBMS_Image_Location_Id IS NULL
              AND ( ( @Image_Type_ID IS NULL
                      AND @Client_ID IS NULL
                      AND cilm.Image_Type_Id = -1
                      AND cilm.Client_Id = -1 )
                    OR ( @Image_Type_ID IS NULL
                         AND cilm.Image_Type_Id = -1
                         AND cilm.Client_Id = @Client_ID )
                    OR ( @Client_ID IS NULL
                         AND cilm.Image_Type_Id = @Image_Type_ID
                         AND cilm.Client_Id = -1 )
                    OR ( cilm.Image_Type_Id = @Image_Type_ID
                         AND cilm.Client_Id = @Client_ID ) ) )
            OR cil.CBMS_Image_Location_Id = @CBMS_Image_Location_Id
      UNION
      SELECT
            cil.CBMS_Image_Location_Id AS ActiveDriveID
           ,Image_Drive AS ActiveDrive
           ,Active_Directory AS ActiveFolder
           ,Max_File_Cnt AS MaxFileCount
           ,Active_File_Cnt AS ActualFileCount
           ,@Xid Xid
           ,c.Code_Value AS StorageType
           ,UserName
           ,[Password]
      FROM
            CBMS_Image_Location cil
            JOIN Code c
                  ON c.Code_Id = cil.Storage_Type_Cd
            JOIN Client_Image_Location_Map cilm
                  ON ( cilm.CBMS_Image_Location_Id = cil.CBMS_Image_Location_Id )
      WHERE
            ( ( cilm.Image_Type_Id = @Image_Type_ID
                AND cilm.Client_Id = -1
                AND NOT EXISTS ( SELECT
                                    1
                                 FROM
                                    Client_Image_Location_Map
                                 WHERE
                                    Image_Type_Id = -1
                                    AND Client_Id = @Client_ID ) )
              OR ( cilm.Image_Type_Id = -1
                   AND cilm.Client_Id = @Client_ID )
              OR ( cilm.Image_Type_Id = -1
                   AND cilm.Client_Id = -1
                   AND NOT EXISTS ( SELECT
                                          1
                                    FROM
                                          Client_Image_Location_Map
                                    WHERE
                                          Image_Type_Id = -1
                                          AND Client_Id = @Client_ID )
                   AND NOT EXISTS ( SELECT
                                          1
                                    FROM
                                          Client_Image_Location_Map
                                    WHERE
                                          Image_Type_Id = @Image_Type_ID
                                          AND Client_Id = -1 ) ) )
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              CBMS_Image_Location cil
                              LEFT OUTER JOIN Client_Image_Location_Map cilm
                                    ON ( cilm.CBMS_Image_Location_Id = cil.CBMS_Image_Location_Id )
                             WHERE
                              ( @CBMS_Image_Location_Id IS NULL
                                AND ( ( @Image_Type_ID IS NULL
                                        AND @Client_ID IS NULL
                                        AND cilm.Image_Type_Id = -1
                                        AND cilm.Client_Id = -1 )
                                      OR ( @Image_Type_ID IS NULL
                                           AND cilm.Image_Type_Id = -1
                                           AND cilm.Client_Id = @Client_ID )
                                      OR ( @Client_ID IS NULL
                                           AND cilm.Image_Type_Id = @Image_Type_ID
                                           AND cilm.Client_Id = -1 )
                                      OR ( cilm.Image_Type_Id = @Image_Type_ID
                                           AND cilm.Client_Id = @Client_ID ) ) )
                              OR cil.CBMS_Image_Location_Id = @CBMS_Image_Location_Id ) 
    
END    
;
GO

GRANT EXECUTE ON  [dbo].[GET_ACTIVE_IMAGE_REPOSITORY_P] TO [CBMSApplication]
GO
