SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Get_MostRecent_Contract_By_SecurityRole

DESCRIPTION:


INPUT PARAMETERS:
	Name				DataType		Default				Description
----------------------------------------------------------------------------
	@Security_Role_Id	INT
	@Top_Count			INT
	@Tvp_Source			tvp_Source READONLY

OUTPUT PARAMETERS:
	Name				DataType		Default				Description
----------------------------------------------------------------------------

USAGE EXAMPLES:
----------------------------------------------------------------------------

	EXEC dbo.Get_MostRecent_Contract_By_SecurityRole 1136
	
	EXEC dbo.Get_MostRecent_Contract_By_SecurityRole 36
	
	EXEC dbo.Get_MostRecent_Contract_By_SecurityRole 36,10


 DECLARE @Tvp_Source tvp_Source 
 INSERT     INTO @Tvp_Source
 VALUES
            ( 290, 'Data Streams' ),
            ( 291, 'Data Streams' ),
            ( 67, 'Data Streams' ) ,
            ( 1585, 'Data Streams' ),
            ( 100028, 'Data Streams' )   
            
 EXEC dbo.Get_MostRecent_Contract_By_SecurityRole 
      @Security_Role_Id = 36
     ,@Top_Count = 10
     ,@Tvp_Source = @Tvp_Source

AUTHOR INITIALS:
	Initials	Name
----------------------------------------------------------------------------
    NR			Narayana Reddy

MODIFICATIONS:
	Initials	Date			Modification
----------------------------------------------------------------------------
	 NR			2020-02-11		MAINT-9861 - Moved Sproc From DV to CBMS.	

******/
CREATE PROCEDURE [dbo].[Get_MostRecent_Contract_By_SecurityRole]
     (
         @Security_Role_Id INT
         , @Top_Count INT = 5
         , @Tvp_Source tvp_Source READONLY
     )
AS
    BEGIN

        SET NOCOUNT ON;


        CREATE TABLE #Client_Commodity_Ids
             (
                 Commodity_Id INT
             );

        DECLARE
            @Client_Id INT
            , @Tvp_Count INT
            , @Commodity_Service_Cd INT;

        SELECT
            @Client_Id = Client_Id
        FROM
            dbo.Security_Role
        WHERE
            Security_Role_Id = @Security_Role_Id;


        SELECT  @Tvp_Count = COUNT(1)FROM   @Tvp_Source;


        SELECT
            @Commodity_Service_Cd = c.Code_Id
        FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code c
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            Codeset_Name = 'CommodityServiceSource'
            AND Code_Value = 'Invoice';


        INSERT INTO #Client_Commodity_Ids
             (
                 Commodity_Id
             )
        SELECT  Source_Id FROM  @Tvp_Source tvp WHERE   tvp.Source_Type = 'Data Streams'
        UNION
        SELECT
            cc.Commodity_Id
        FROM
            @Tvp_Source tvp
            INNER JOIN dbo.Client_Commodity_Tag cct
                ON tvp.Source_Id = cct.Tag_Id
            INNER JOIN Core.Client_Commodity cc
                ON cct.Client_Commodity_Id = cc.Client_Commodity_Id
                   AND  cc.Commodity_Service_Cd = @Commodity_Service_Cd
                   AND  cc.Client_Id = @Client_Id
        WHERE
            tvp.Source_Type = 'Tags';


        SELECT  TOP (@Top_Count)
                C.CONTRACT_ID
                , COM.Commodity_Name commodity_type
                , CHA.Account_Vendor_Name AS vendor_name
                , C.CONTRACT_START_DATE
                , C.CONTRACT_END_DATE
                , CHA.Account_Id
                , SUBSTRING(Stn.Site_name, 1, LEN(Stn.Site_name) - 1) Site_name
        FROM
            dbo.Security_Role_Client_Hier SRCH
            JOIN Core.Client_Hier ch
                ON SRCH.Client_Hier_Id = ch.Client_Hier_Id
            JOIN Core.Client_Hier_Account CHA
                ON ch.Client_Hier_Id = CHA.Client_Hier_Id
            JOIN dbo.CONTRACT C
                ON C.CONTRACT_ID = CHA.Supplier_Contract_ID
            JOIN dbo.Commodity COM
                ON COM.Commodity_Id = C.COMMODITY_TYPE_ID
            CROSS APPLY
        (   SELECT
                CAST(Site_name AS VARCHAR(MAX)) + ' | '
            FROM
                Core.Client_Hier ch
            WHERE
                ch.Client_Hier_Id = CHA.Client_Hier_Id
                AND ch.Site_Not_Managed = 0
                AND ch.Site_Id > 0
            FOR XML PATH('')) AS Stn(Site_name)
        WHERE
            SRCH.Security_Role_Id = @Security_Role_Id
            AND ch.Site_Not_Managed = 0
            AND (   EXISTS (   SELECT
                                    1
                               FROM
                                    #Client_Commodity_Ids cmi
                               WHERE
                                    cmi.Commodity_Id = COM.Commodity_Id)
                    OR  @Tvp_Count = 0)
        GROUP BY
            C.CONTRACT_ID
            , COM.Commodity_Name
            , CHA.Account_Vendor_Name
            , C.CONTRACT_START_DATE
            , C.CONTRACT_END_DATE
            , CHA.Account_Id
            , Stn.Site_name
        ORDER BY
            C.CONTRACT_ID DESC;

    END;


GO
GRANT EXECUTE ON  [dbo].[Get_MostRecent_Contract_By_SecurityRole] TO [CBMSApplication]
GO
