SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  procedure [dbo].[cbmsPermissionInfo_GetAll]
	( @MyAccountId int )
AS
BEGIN

	   select permission_info_id 
		, permission_name
		, permission_description
	     from permission_info
	 order by permission_name asc

END
GO
GRANT EXECUTE ON  [dbo].[cbmsPermissionInfo_GetAll] TO [CBMSApplication]
GO
