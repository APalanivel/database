SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO    
    
/******              
    
NAME: [DBO].[Utility_Dtl_Balancing_Requirement_Del]      
         
DESCRIPTION:     
 To Delete Utility_Dtl_Balancing_Requirement records based on  Utility_Dtl_Balancing_Requirement_Id.    
          
INPUT PARAMETERS:  
            
NAME									DATATYPE DEFAULT  DESCRIPTION              
------------------------------------------------------------              
@Utility_Dtl_Balancing_Requirement_Id	   INT          
                    
OUTPUT PARAMETERS:              
NAME   DATATYPE DEFAULT  DESCRIPTION       
           
------------------------------------------------------------              
USAGE EXAMPLES:              
------------------------------------------------------------      
 BEGIN TRAN
 EXEC Utility_Dtl_Balancing_Requirement_Del  1    
 ROLLBACK
     
 BEGIN TRAN
 EXEC Utility_Dtl_Balancing_Requirement_Del  2    
 ROLLBACK 
     
         
AUTHOR INITIALS: 
             
INITIALS	NAME              
------------------------------------------------------------              
GK			GOPI    
              
MODIFICATIONS               
INITIALS	DATE		MODIFICATION              
------------------------------------------------------------              
GK			06-MAY-11	Created to fix MAINT-596      
*/    
    
CREATE PROCEDURE dbo.Utility_Dtl_Balancing_Requirement_Del
      @Utility_Dtl_Balancing_Requirement_Id INT
AS 
BEGIN   
 
      SET NOCOUNT ON

      DELETE FROM
            dbo.Utility_Dtl_Balancing_Requirement
      WHERE
            Utility_Dtl_Balancing_Requirement_Id = @Utility_Dtl_Balancing_Requirement_Id      

END

GO
GRANT EXECUTE ON  [dbo].[Utility_Dtl_Balancing_Requirement_Del] TO [CBMSApplication]
GO
GO