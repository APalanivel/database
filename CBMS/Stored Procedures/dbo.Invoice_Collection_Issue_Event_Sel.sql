SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.Invoice_Collection_Issue_Event_Sel            
                        
 DESCRIPTION:                        
			To get Invoice_Collection_Issue_Event      
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@Invoice_Collection_Issue_Id INT

                             
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            

          exec [dbo].[Invoice_Collection_Issue_Event_Sel] 59                    
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
 RKV					Ravi Kumar Raju
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2016-11-21       Created for Invoice Tracking     
 RKV                   2018-02-03       SE2017-273,Added three columns Last_Change_Ts,Updated_User_id,Invoice_Collection_Issue_Event_Id         
                      
******/    

CREATE PROCEDURE [dbo].[Invoice_Collection_Issue_Event_Sel]
      ( 
       @Invoice_Collection_Issue_Id INT )
AS 
BEGIN                
      SET NOCOUNT ON;  

      

      SELECT
            et.Code_Value AS Event_Type
           ,ie.Event_Desc
           ,ie.Created_User_Id
           ,ie.Created_Ts
           ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS UserName
           ,uui.FIRST_NAME + ' ' + uui.LAST_NAME AS Updated_UserName
           ,ie.Last_Change_Ts
           ,ie.Updated_User_Id
           ,ie.Invoice_Collection_Issue_Event_Id
           ,ie.Issue_Event_Type_Cd
           ,icil.Issue_Status_Cd
      FROM
            Invoice_Collection_Issue_Event ie
            INNER JOIN dbo.Invoice_Collection_Issue_Log icil
            ON ie.Invoice_Collection_Issue_Log_Id = icil.Invoice_Collection_Issue_Log_Id
            INNER JOIN Code et
                  ON ie.Issue_Event_Type_Cd = et.Code_Id
            INNER JOIN USER_INFO ui
                  ON ie.Created_User_Id = ui.USER_INFO_ID
            LEFT OUTER JOIN USER_INFO uui
                  ON ie.Updated_User_Id = uui.USER_INFO_ID
      WHERE
            ie.Invoice_Collection_Issue_Log_Id = @Invoice_Collection_Issue_Id
      ORDER BY
            ie.Created_Ts DESC     

END;



GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Issue_Event_Sel] TO [CBMSApplication]
GO
