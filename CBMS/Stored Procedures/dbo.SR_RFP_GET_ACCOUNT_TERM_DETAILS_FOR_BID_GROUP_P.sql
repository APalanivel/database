SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_ACCOUNT_TERM_DETAILS_FOR_BID_GROUP_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@rfp_id        	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

-- exec dbo.SR_RFP_GET_ACCOUNT_TERM_DETAILS_P 138
CREATE    PROCEDURE dbo.SR_RFP_GET_ACCOUNT_TERM_DETAILS_FOR_BID_GROUP_P
	@rfp_id int
	AS
set nocount on
	(select	term.sr_rfp_term_id,
		account_term.SR_ACCOUNT_GROUP_ID as SR_ACCOUNT_GROUP_ID,
		account_term.is_bid_group,
		account_term.from_month,
		account_term.to_month,
		account_term.no_of_months
		 	 
	from 	sr_rfp_account_term account_term(nolock),
		sr_rfp_term term(nolock),
		sr_rfp_account rfp_acc(nolock)
	
	where	term.sr_rfp_id = @rfp_id
		and account_term.sr_rfp_term_id = term.sr_rfp_term_id
		and account_term.is_bid_group = 1
		and rfp_acc.sr_rfp_bid_group_id = account_term.SR_ACCOUNT_GROUP_ID
		and rfp_acc.is_deleted = 0

	)
	
	union

	(select	term.sr_rfp_term_id,
		rfp_acc.sr_rfp_account_id as SR_ACCOUNT_GROUP_ID,
		account_term.is_bid_group,
		account_term.from_month,
		account_term.to_month,
		account_term.no_of_months
		 	 
	from 	sr_rfp_account_term account_term(nolock),
		sr_rfp_term term(nolock),
		sr_rfp_account rfp_acc(nolock)
	
	where	term.sr_rfp_id = @rfp_id
		and account_term.is_bid_group = 0
		and rfp_acc.is_deleted = 0
		and account_term.sr_rfp_term_id = term.sr_rfp_term_id
		and rfp_acc.sr_rfp_account_id = account_term.SR_ACCOUNT_GROUP_ID
	)
	
	order by term.sr_rfp_term_id
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_ACCOUNT_TERM_DETAILS_FOR_BID_GROUP_P] TO [CBMSApplication]
GO
