SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******               
                           
 NAME:  dbo.Sr_Rfp_Supply_Point_Summary_Sel_By_Rfp_Account_Group_Id                  
                              
 DESCRIPTION:                              
                               
                              
 INPUT PARAMETERS:              
                             
 Name					DataType			Default			Description              
---------------------------------------------------------------------
 @SR_RFP_ID				INT
 @SR_RFP_ACCOUNT_ID		INT
 @SR_RFP_BID_GROUP_ID	INT
                                    
 OUTPUT PARAMETERS:                   
                              
 Name				DataType			Default			Description              
---------------------------------------------------------------------
                              
 USAGE EXAMPLES:              
---------------------------------------------------------------------
  
   EXEC dbo.Sr_Rfp_Supply_Point_Summary_Sel_By_Rfp_Account_Group_Id  1429
   EXEC dbo.Sr_Rfp_Supply_Point_Summary_Sel_By_Rfp_Account_Group_Id 350
   EXEC dbo.Sr_Rfp_Supply_Point_Summary_Sel_By_Rfp_Account_Group_Id 3246
   EXEC dbo.Sr_Rfp_Supply_Point_Summary_Sel_By_Rfp_Account_Group_Id 13523,NULL,NULL

   EXEC dbo.Sr_Rfp_Supply_Point_Summary_Sel_By_Rfp_Account_Group_Id 13137,null,null
   EXEC dbo.Sr_Rfp_Supply_Point_Summary_Sel_By_Rfp_Account_Group_Id 13569
   EXEC dbo.Sr_Rfp_Supply_Point_Summary_Sel_By_Rfp_Account_Group_Id 13569,null,null   
   EXEC dbo.Sr_Rfp_Supply_Point_Summary_Sel_By_Rfp_Account_Group_Id 13570
   
   EXEC dbo.Sr_Rfp_Supply_Point_Summary_Sel_By_Rfp_Account_Group_Id 13556
   EXEC dbo.Sr_Rfp_Supply_Point_Summary_Sel_By_Rfp_Account_Group_Id 13580

   EXEC dbo.Sr_Rfp_Supply_Point_Summary_Sel_By_Rfp_Account_Group_Id 13733
   
   EXEC dbo.Sr_Rfp_Supply_Point_Summary_Sel_By_Rfp_Account_Group_Id 13104,null,null


 AUTHOR INITIALS:              
 Initials			Name              
---------------------------------------------------------------------
 RKV				Ravi Kumar Raju     
 NR					Narayana Reddy                    
                               
 MODIFICATIONS:            
             
 Initials   Date        Modification            
---------------------------------------------------------------------
 RKV        2016-06-09  Created for GCS Phase - 5.
 RR			2016-07-21	GCS-1135 Modified to return load profile size in country default UOM for natural gas only     
 NR			2016-11-03	MAINT-4544 Addded State and City as separate column.  
 NR			2017-02-27	MAINT-4841 Showing Max values for deman dth uom.  
 NR			2017-03-09	Hotfix -  Showing Max values for demand Determinant. 
						PRSUPPORT-455 - Chnaged the datatype int decimal.   
 NR			2017-12-22	SE2017-443 And SE2017-434 - Added Rate_Name,Contracting_Entity	filed in output list.	
 RR			2017-12-28	MAINT-6428 Longest term dates displaying as numbers due to extra spaces in custom date format .		                
                  
******/ 
CREATE PROCEDURE [dbo].[Sr_Rfp_Supply_Point_Summary_Sel_By_Rfp_Account_Group_Id]
      ( 
       @SR_RFP_ID INT
      ,@SR_RFP_ACCOUNT_ID INT = NULL
      ,@SR_RFP_BID_GROUP_ID INT = NULL )
AS 
BEGIN 
 
      SET NOCOUNT ON;

      DECLARE
            @pivot_String VARCHAR(MAX)
           ,@SQL_STR NVARCHAR(MAX)
           ,@pivot_String1 VARCHAR(MAX)
           ,@pivot_String2 VARCHAR(MAX)
     


      DECLARE
            @Convertion_Id INT
           ,@unit_name VARCHAR(200)
           ,@Commodity_Name VARCHAR(200)
           ,@Is_Term_Date_Specific BIT
           ,@Commodity_Id INT
           
      
      CREATE TABLE #LP_Volume
            ( 
             SR_RFP_ACCOUNT_ID INT
            ,Load_Profile_Volume DECIMAL(32, 16)
            ,Load_Profile_Volume_uom VARCHAR(200) )

      CREATE TABLE #Max_Capacity
            ( 
             SR_RFP_ACCOUNT_ID INT
            ,Max_Capacity DECIMAL(32, 16)
            ,Max_Capacity_uom VARCHAR(200) )

      CREATE TABLE #Contract_Longest_Term_Dates
            ( 
             SR_RFP_ACCOUNT_ID INT
            ,Start_dt DATE
            ,End_Dt DATE )
 
      CREATE TABLE #SR_RFP_Account_Id
            ( 
             SR_RFP_Account_Id INT )
      
      
      DECLARE @Determinant_Names TABLE
            ( 
             Determinant_Name VARCHAR(200) )
      
      CREATE TABLE #Determinant_Values_Exclude_Total_Usage
            ( 
             SR_RFP_ACCOUNT_ID INT
            ,Determinant_Name VARCHAR(100)
            ,LP_Value DECIMAL(32, 16)
            ,DETERMINANT_UNIT_TYPE_ID INT )
      
      
      
      CREATE TABLE #Client_Hier_Rfp_Data
            ( 
             Client_Id INT
            ,Client_Name VARCHAR(200)
            ,Site_Id INT
            ,Site_Name VARCHAR(800)
            ,Group_Name VARCHAR(200)
            ,DUNS_NUMBER VARCHAR(30)
            ,Account_Id INT
            ,Account_Number VARCHAR(50)
            ,Alternate_Account_Number NVARCHAR(200)
            ,Meter_Number VARCHAR(50)
            ,Sr_Rfp_Account_Id INT
            ,Sr_Rfp_Bid_Group_Id INT
            ,ADDRESS VARCHAR(450)
            ,City VARCHAR(200)
            ,State_Id INT
            ,State_Name VARCHAR(200)
            ,Country_Id INT
            ,Country_Name VARCHAR(200)
            ,ZipCode VARCHAR(30)
            ,Meter_Type VARCHAR(25)
            ,Vendor_Name VARCHAR(200)
            ,Is_Bid_Group BIT
            ,Rate_Name VARCHAR(200)
            ,Contracting_Entity VARCHAR(200) )



      SELECT
            @Is_Term_Date_Specific = sr.Is_Term_Date_Specific
      FROM
            dbo.SR_RFP sr
      WHERE
            sr.SR_RFP_ID = @SR_RFP_ID
            
      INSERT      INTO #SR_RFP_Account_Id
                  ( 
                   SR_RFP_Account_Id )
                  SELECT
                        sra.SR_RFP_ACCOUNT_ID
                  FROM
                        dbo.SR_RFP_ACCOUNT sra
                  WHERE
                        sra.SR_RFP_ID = @SR_RFP_ID
                        AND sra.IS_DELETED = 0
                       

      
      INSERT      INTO #Client_Hier_Rfp_Data
                  ( 
                   Client_Id
                  ,Client_Name
                  ,Site_Id
                  ,Site_Name
                  ,Group_Name
                  ,DUNS_NUMBER
                  ,Account_Id
                  ,Account_Number
                  ,Alternate_Account_Number
                  ,Meter_Number
                  ,Sr_Rfp_Account_Id
                  ,Sr_Rfp_Bid_Group_Id
                  ,ADDRESS
                  ,City
                  ,State_Id
                  ,State_Name
                  ,Country_Id
                  ,Country_Name
                  ,ZipCode
                  ,Meter_Type
                  ,Vendor_Name
                  ,Is_Bid_Group
                  ,Rate_Name
                  ,Contracting_Entity )
                  SELECT
                        ch.Client_Id
                       ,ch.Client_Name
                       ,ch.SITE_ID
                       ,ch.site_name
                       ,srbg.GROUP_NAME
                       ,st.Duns_Number
                       ,cha.ACCOUNT_ID
                       ,cha.ACCOUNT_NUMBER
                       ,cha.Alternate_Account_Number
                       ,cha.Meter_Number
                       ,rfpacc.SR_RFP_ACCOUNT_ID
                       ,rfpacc.SR_RFP_BID_GROUP_ID
                       ,LEFT(cha.Meter_Address_Line_1 + ', ' + cha.Meter_Address_Line_2, LEN(cha.Meter_Address_Line_1 + ', ' + cha.Meter_Address_Line_2) - 1) AS ADDRESS
                       ,cha.Meter_City
                       ,cha.Meter_State_Id
                       ,cha.Meter_State_Name
                       ,cha.Meter_Country_Id
                       ,cha.Meter_Country_Name
                       ,cha.Meter_ZipCode
                       ,mc.Code_Value
                       ,cha.Account_Vendor_Name
                       ,CASE WHEN rfpacc.SR_RFP_BID_GROUP_ID IS NOT NULL THEN 1
                             ELSE 0
                        END
                       ,cha.Rate_Name
                       ,st.CONTRACTING_ENTITY
                  FROM
                        dbo.SR_RFP_ACCOUNT rfpacc
                        INNER JOIN #SR_RFP_Account_Id srai
                              ON rfpacc.SR_RFP_ACCOUNT_ID = srai.SR_RFP_Account_Id
                        INNER JOIN dbo.SR_RFP_ACCOUNT_METER_MAP sramm
                              ON srai.SR_RFP_Account_Id = sramm.SR_RFP_ACCOUNT_ID
                        INNER JOIN Core.Client_Hier_Account cha
                              ON rfpacc.ACCOUNT_ID = cha.Account_Id
                                 AND sramm.METER_ID = cha.Meter_Id
                        INNER JOIN Core.Client_Hier ch
                              ON ch.Client_Hier_Id = cha.Client_Hier_Id
                        INNER JOIN dbo.METER mtr
                              ON cha.Meter_Id = mtr.METER_ID
                        LEFT JOIN dbo.Code mtrtyp
                              ON cha.Meter_Type_Cd = mtrtyp.Code_Id
                        INNER JOIN dbo.SITE st
                              ON ch.Site_Id = st.SITE_ID
                        INNER JOIN dbo.Division_Dtl dd
                              ON dd.Sitegroup_Id = st.Division_Id
                        LEFT JOIN dbo.ENTITY tes
                              ON mtr.TAX_EXEMPTION_ID = tes.ENTITY_ID
                        LEFT OUTER JOIN dbo.SR_RFP_BID_GROUP srbg
                              ON rfpacc.SR_RFP_BID_GROUP_ID = srbg.SR_RFP_BID_GROUP_ID
                        LEFT OUTER JOIN code mc
                              ON mc.Code_Id = cha.Meter_Type_Cd
                  GROUP BY
                        ch.Client_Id
                       ,ch.Client_Name
                       ,ch.SITE_ID
                       ,ch.site_name
                       ,srbg.GROUP_NAME
                       ,st.Duns_Number
                       ,cha.ACCOUNT_ID
                       ,cha.ACCOUNT_NUMBER
                       ,cha.Alternate_Account_Number
                       ,cha.Meter_Number
                       ,rfpacc.SR_RFP_ACCOUNT_ID
                       ,rfpacc.SR_RFP_BID_GROUP_ID
                       ,cha.Meter_Address_Line_1
                       ,cha.Meter_Address_Line_2
                       ,cha.Meter_City
                       ,cha.Meter_State_Id
                       ,cha.Meter_State_Name
                       ,cha.Meter_Country_Id
                       ,cha.Meter_Country_Name
                       ,cha.Meter_ZipCode
                       ,mc.Code_Value
                       ,cha.Account_Vendor_Name
                       ,cha.Rate_Name
                       ,st.CONTRACTING_ENTITY
                 
                
 

                
    
    
      INSERT      INTO #Contract_Longest_Term_Dates
                  ( 
                   SR_RFP_ACCOUNT_ID
                  ,Start_dt
                  ,End_Dt )
                  SELECT
                        chrd.SR_RFP_ACCOUNT_ID
                       ,x.start_month
                       ,y.To_month
                  FROM
                        #Client_Hier_Rfp_Data chrd
                        INNER JOIN ( SELECT
                                          cd.SR_RFP_ACCOUNT_ID
                                         ,MIN(srat.FROM_MONTH) start_month
                                     FROM
                                          dbo.SR_RFP_TERM srt
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM srat
                                                ON srt.SR_RFP_TERM_ID = srat.SR_RFP_TERM_ID
                                          INNER JOIN dbo.#Client_Hier_Rfp_Data cd
                                                ON cd.SR_RFP_ACCOUNT_ID = srat.SR_ACCOUNT_GROUP_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON srt.SR_RFP_ID = rfp.SR_RFP_ID
                                     GROUP BY
                                          cd.SR_RFP_ACCOUNT_ID ) x
                              ON chrd.SR_RFP_ACCOUNT_ID = x.SR_RFP_ACCOUNT_ID
                        INNER JOIN ( SELECT
                                          cd.SR_RFP_ACCOUNT_ID
                                         ,MAX(srat.To_MONTH) To_month
                                     FROM
                                          dbo.SR_RFP_TERM srt
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM srat
                                                ON srt.SR_RFP_TERM_ID = srat.SR_RFP_TERM_ID
                                          INNER JOIN dbo.#Client_Hier_Rfp_Data cd
                                                ON cd.SR_RFP_ACCOUNT_ID = srat.SR_ACCOUNT_GROUP_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON srt.SR_RFP_ID = rfp.SR_RFP_ID
                                     GROUP BY
                                          cd.SR_RFP_ACCOUNT_ID ) y
                              ON x.SR_RFP_ACCOUNT_ID = y.SR_RFP_ACCOUNT_ID
                  WHERE
                        chrd.Is_Bid_Group = 0
                  GROUP BY
                        chrd.SR_RFP_ACCOUNT_ID
                       ,x.start_month
                       ,y.To_month




      INSERT      INTO #Contract_Longest_Term_Dates
                  ( 
                   SR_RFP_ACCOUNT_ID
                  ,Start_dt
                  ,End_Dt )
                  SELECT
                        chrd.SR_RFP_ACCOUNT_ID
                       ,x.start_month
                       ,y.To_month
                  FROM
                        #Client_Hier_Rfp_Data chrd
                        INNER JOIN ( SELECT
                                          cd.SR_RFP_ACCOUNT_ID
                                         ,MIN(srat.FROM_MONTH) start_month
                                     FROM
                                          dbo.SR_RFP_TERM srt
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM srat
                                                ON srt.SR_RFP_TERM_ID = srat.SR_RFP_TERM_ID
                                          INNER JOIN dbo.#Client_Hier_Rfp_Data cd
                                                ON cd.SR_RFP_BID_GROUP_ID = srat.SR_ACCOUNT_GROUP_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON srt.SR_RFP_ID = rfp.SR_RFP_ID
                                     GROUP BY
                                          cd.SR_RFP_ACCOUNT_ID ) x
                              ON chrd.SR_RFP_ACCOUNT_ID = x.SR_RFP_ACCOUNT_ID
                        INNER JOIN ( SELECT
                                          cd.SR_RFP_ACCOUNT_ID
                                         ,MAX(srat.To_MONTH) To_month
                                     FROM
                                          dbo.SR_RFP_TERM srt
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM srat
                                                ON srt.SR_RFP_TERM_ID = srat.SR_RFP_TERM_ID
                                          INNER JOIN dbo.#Client_Hier_Rfp_Data cd
                                                ON cd.SR_RFP_BID_GROUP_ID = srat.SR_ACCOUNT_GROUP_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON srt.SR_RFP_ID = rfp.SR_RFP_ID
                                     GROUP BY
                                          cd.SR_RFP_ACCOUNT_ID ) y
                              ON x.SR_RFP_ACCOUNT_ID = y.SR_RFP_ACCOUNT_ID
                  WHERE
                        chrd.Is_Bid_Group = 1
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          #Contract_Longest_Term_Dates cltd
                                         WHERE
                                          cltd.SR_RFP_ACCOUNT_ID = chrd.SR_RFP_ACCOUNT_ID )
                  GROUP BY
                        chrd.SR_RFP_ACCOUNT_ID
                       ,x.start_month
                       ,y.To_month
      
      
      SELECT
            @Commodity_Name = c.Commodity_Name
           ,@Commodity_Id = c.Commodity_Id
      FROM
            dbo.SR_RFP sr
            INNER JOIN dbo.Commodity c
                  ON c.Commodity_Id = sr.COMMODITY_TYPE_ID
      WHERE
            SR_RFP_ID = @SR_RFP_ID


--Getting Contract Volume
      SELECT
            @Convertion_Id = ENTITY_ID
      FROM
            Entity
      WHERE
            ENTITY_NAME = 'KW'
            AND ENTITY_TYPE = 101
            AND @Commodity_Name = 'Electric Power'
 
      SELECT
            @Convertion_Id = MIN(srlpd.DETERMINANT_UNIT_TYPE_ID)
      FROM
            dbo.SR_RFP_LOAD_PROFILE_SETUP srlps
            INNER JOIN dbo.SR_RFP_LOAD_PROFILE_DETERMINANT srlpd
                  ON srlps.SR_RFP_LOAD_PROFILE_SETUP_ID = srlpd.SR_RFP_LOAD_PROFILE_SETUP_ID
            INNER JOIN dbo.SR_RFP_LP_DETERMINANT_VALUES srldv
                  ON srlpd.SR_RFP_LOAD_PROFILE_DETERMINANT_ID = srldv.SR_RFP_LOAD_PROFILE_DETERMINANT_ID
            INNER JOIN #SR_RFP_Account_Id srai
                  ON srlps.SR_RFP_ACCOUNT_ID = srai.SR_RFP_ACCOUNT_ID
      WHERE
            @Convertion_Id IS NULL
      GROUP BY
            srai.SR_RFP_ACCOUNT_ID
 
 
 
                

      INSERT      INTO #LP_Volume
                  ( 
                   SR_RFP_ACCOUNT_ID
                  ,Load_Profile_Volume
                  ,Load_Profile_Volume_uom )
                  SELECT
                        srlps.SR_RFP_ACCOUNT_ID
                       ,CASE WHEN @Commodity_Name = 'Natural Gas' THEN SUM(srldv.LP_VALUE * cuc.CONVERSION_FACTOR)
                             ELSE SUM(srldv.LP_VALUE)
                        END AS Load_Profile_Volume
                       ,MIN(e.ENTITY_NAME) Load_Profile_Volume_uom
                  FROM
                        dbo.SR_RFP_LOAD_PROFILE_SETUP srlps
                        INNER JOIN dbo.SR_RFP_LOAD_PROFILE_DETERMINANT srlpd
                              ON srlps.SR_RFP_LOAD_PROFILE_SETUP_ID = srlpd.SR_RFP_LOAD_PROFILE_SETUP_ID
                        INNER JOIN dbo.SR_RFP_LP_DETERMINANT_VALUES srldv
                              ON srlpd.SR_RFP_LOAD_PROFILE_DETERMINANT_ID = srldv.SR_RFP_LOAD_PROFILE_DETERMINANT_ID
                        INNER JOIN #SR_RFP_Account_Id cltd
                              ON cltd.SR_RFP_Account_Id = srlps.SR_RFP_ACCOUNT_ID
                        INNER JOIN dbo.ENTITY en
                              ON srldv.READING_TYPE_ID = en.ENTITY_ID
                        LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                              ON cuc.BASE_UNIT_ID = srlpd.DETERMINANT_UNIT_TYPE_ID
                                 AND cuc.CONVERTED_UNIT_ID = srlpd.DETERMINANT_UNIT_TYPE_ID
                        LEFT OUTER JOIN dbo.ENTITY e
                              ON e.ENTITY_ID = srlpd.DETERMINANT_UNIT_TYPE_ID
                  WHERE
                        srlpd.DETERMINANT_NAME IN ( 'Total Usage' )
                        AND en.ENTITY_TYPE = 1031
                        AND ( en.ENTITY_NAME = 'Actual LP Value'
                              OR en.ENTITY_NAME = 'Actual Avg LP Value' )
                        AND srldv.LP_VALUE IS NOT NULL
                  GROUP BY
                        srlps.SR_RFP_ACCOUNT_ID
                       
                 

      INSERT      INTO #Max_Capacity
                  ( 
                   SR_RFP_ACCOUNT_ID
                  ,Max_Capacity
                  ,Max_Capacity_uom )
                  SELECT
                        srlps.SR_RFP_ACCOUNT_ID
                       ,MAX(srldv.LP_VALUE * Ep_ccon.CONVERSION_FACTOR) Max_Capacity
                       ,MIN(ep.ENTITY_NAME) Max_Capacity_uom
                  FROM
                        dbo.SR_RFP_LOAD_PROFILE_SETUP srlps
                        INNER JOIN dbo.SR_RFP_LOAD_PROFILE_DETERMINANT srlpd
                              ON srlps.SR_RFP_LOAD_PROFILE_SETUP_ID = srlpd.SR_RFP_LOAD_PROFILE_SETUP_ID
                        INNER JOIN dbo.SR_RFP_LP_DETERMINANT_VALUES srldv
                              ON srlpd.SR_RFP_LOAD_PROFILE_DETERMINANT_ID = srldv.SR_RFP_LOAD_PROFILE_DETERMINANT_ID
                        INNER JOIN #Contract_Longest_Term_Dates cltd
                              ON cltd.SR_RFP_ACCOUNT_ID = srlps.SR_RFP_ACCOUNT_ID
                        INNER JOIN dbo.ENTITY ep
                              ON ep.ENTITY_ID = @Convertion_Id
                                 AND @Commodity_Name = 'Electric Power'
                        INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION Ep_ccon
                              ON Ep_ccon.BASE_UNIT_ID = srlpd.DETERMINANT_UNIT_TYPE_ID
                                 AND Ep_ccon.CONVERTED_UNIT_ID = @Convertion_Id
                                 AND @Commodity_Name = 'Electric Power'
                  WHERE
                        srldv.MONTH_IDENTIFIER BETWEEN DATEADD(dd, -DATEPART(dd, cltd.Start_dt) + 1, cltd.Start_dt)
                                               AND     DATEADD(dd, -DATEPART(dd, cltd.End_dt) + 1, cltd.End_dt)
                        AND srlpd.DETERMINANT_NAME NOT IN ( 'Total Usage' )
                  GROUP BY
                        srlps.SR_RFP_ACCOUNT_ID


      
      
      INSERT      INTO #Determinant_Values_Exclude_Total_Usage
                  ( 
                   SR_RFP_ACCOUNT_ID
                  ,Determinant_Name
                  ,LP_Value
                  ,DETERMINANT_UNIT_TYPE_ID )
                  SELECT
                        srai.SR_RFP_ACCOUNT_ID
                       ,srlpd.DETERMINANT_NAME
                       ,CASE WHEN e.ENTITY_NAME = 'Kw'
                                  OR srlpd.DETERMINANT_NAME = 'Demand' THEN MAX(srldv.LP_VALUE)
                             ELSE SUM(srldv.LP_VALUE)
                        END
                       ,srlpd.DETERMINANT_UNIT_TYPE_ID
                  FROM
                        dbo.SR_RFP_LOAD_PROFILE_SETUP srlps
                        INNER JOIN dbo.SR_RFP_LOAD_PROFILE_DETERMINANT srlpd
                              ON srlps.SR_RFP_LOAD_PROFILE_SETUP_ID = srlpd.SR_RFP_LOAD_PROFILE_SETUP_ID
                        INNER JOIN dbo.SR_RFP_LP_DETERMINANT_VALUES srldv
                              ON srlpd.SR_RFP_LOAD_PROFILE_DETERMINANT_ID = srldv.SR_RFP_LOAD_PROFILE_DETERMINANT_ID
                        INNER JOIN dbo.ENTITY e
                              ON e.ENTITY_ID = srlpd.DETERMINANT_UNIT_TYPE_ID
                        INNER JOIN #SR_RFP_Account_Id srai
                              ON srlps.SR_RFP_ACCOUNT_ID = srai.SR_RFP_ACCOUNT_ID
                        INNER JOIN #Contract_Longest_Term_Dates cltd
                              ON cltd.SR_RFP_ACCOUNT_ID = srai.SR_RFP_ACCOUNT_ID
                  WHERE
                        srldv.MONTH_IDENTIFIER BETWEEN DATEADD(dd, -DATEPART(dd, cltd.Start_dt) + 1, cltd.Start_dt)
                                               AND     DATEADD(dd, -DATEPART(dd, cltd.End_dt) + 1, cltd.End_dt)
                        AND Determinant_Name NOT IN ( 'Total Usage', 'Total Demand' )
                  GROUP BY
                        srai.SR_RFP_ACCOUNT_ID
                       ,srlpd.DETERMINANT_NAME
                       ,srlpd.DETERMINANT_UNIT_TYPE_ID
                       ,e.ENTITY_NAME
      
    
      
      
      INSERT      INTO @Determinant_Names
                  SELECT
                        DETERMINANT_NAME
                  FROM
                        dbo.SR_RFP_LOAD_PROFILE_SETUP srlps
                        INNER JOIN dbo.SR_RFP_LOAD_PROFILE_DETERMINANT srlpd
                              ON srlps.SR_RFP_LOAD_PROFILE_SETUP_ID = srlpd.SR_RFP_LOAD_PROFILE_SETUP_ID
                        INNER JOIN dbo.SR_RFP_LP_DETERMINANT_VALUES srldv
                              ON srlpd.SR_RFP_LOAD_PROFILE_DETERMINANT_ID = srldv.SR_RFP_LOAD_PROFILE_DETERMINANT_ID
                        INNER JOIN #Client_Hier_Rfp_Data chrd
                              ON srlps.SR_RFP_ACCOUNT_ID = chrd.SR_RFP_ACCOUNT_ID
                        INNER JOIN dbo.ENTITY e
                              ON e.ENTITY_ID = srlpd.DETERMINANT_UNIT_TYPE_ID
                        INNER JOIN #Contract_Longest_Term_Dates cltd
                              ON cltd.SR_RFP_ACCOUNT_ID = chrd.SR_RFP_ACCOUNT_ID
                  WHERE
                        srldv.MONTH_IDENTIFIER BETWEEN DATEADD(dd, -DATEPART(dd, cltd.Start_dt) + 1, cltd.Start_dt)
                                               AND     DATEADD(dd, -DATEPART(dd, cltd.End_dt) + 1, cltd.End_dt)
                        AND srlpd.DETERMINANT_NAME NOT IN ( 'Total Usage', 'Total Demand' )
                  GROUP BY
                        DETERMINANT_NAME     
                                          
      INSERT      INTO @Determinant_Names
                  SELECT
                        DETERMINANT_NAME + ' UOM'
                  FROM
                        dbo.SR_RFP_LOAD_PROFILE_SETUP srlps
                        INNER JOIN dbo.SR_RFP_LOAD_PROFILE_DETERMINANT srlpd
                              ON srlps.SR_RFP_LOAD_PROFILE_SETUP_ID = srlpd.SR_RFP_LOAD_PROFILE_SETUP_ID
                        INNER JOIN dbo.SR_RFP_LP_DETERMINANT_VALUES srldv
                              ON srlpd.SR_RFP_LOAD_PROFILE_DETERMINANT_ID = srldv.SR_RFP_LOAD_PROFILE_DETERMINANT_ID
                        INNER JOIN #Client_Hier_Rfp_Data chrd
                              ON srlps.SR_RFP_ACCOUNT_ID = chrd.SR_RFP_ACCOUNT_ID
                        INNER JOIN dbo.ENTITY e
                              ON e.ENTITY_ID = srlpd.DETERMINANT_UNIT_TYPE_ID
                        INNER JOIN #Contract_Longest_Term_Dates cltd
                              ON cltd.SR_RFP_ACCOUNT_ID = chrd.SR_RFP_ACCOUNT_ID
                  WHERE
                        srldv.MONTH_IDENTIFIER BETWEEN DATEADD(dd, -DATEPART(dd, cltd.Start_dt) + 1, cltd.Start_dt)
                                               AND     DATEADD(dd, -DATEPART(dd, cltd.End_dt) + 1, cltd.End_dt)
                        AND srlpd.DETERMINANT_NAME NOT IN ( 'Total Usage', 'Total Demand' )
                  GROUP BY
                        DETERMINANT_NAME   




 
      SELECT
  DISTINCT
            @pivot_String = STUFF(( SELECT
                                          ',MIN([' + dn.DETERMINANT_NAME + ']) AS [' + dn.DETERMINANT_NAME + ']'--',[' + dn.DETERMINANT_NAME + ']'
                                    FROM
                                          ( SELECT
                                                DETERMINANT_NAME
                                            FROM
                                                @Determinant_Names ) dn
                                    ORDER BY
                                          dn.DETERMINANT_NAME
                                  FOR
                                    XML PATH('') ), 1, 1, '') 
                             


      SELECT
            @pivot_String1 = STUFF(( SELECT
                                          ',[' + dn.DETERMINANT_NAME + ']'
                                     FROM
                                          ( SELECT
                                                DETERMINANT_NAME
                                            FROM
                                                @Determinant_Names ) dn
                                     WHERE
                                          DETERMINANT_NAME NOT LIKE '%UOM%'
                                     ORDER BY
                                          dn.DETERMINANT_NAME
                                   FOR
                                     XML PATH('') ), 1, 1, '') 

      SELECT
            @pivot_String2 = STUFF(( SELECT
                                          ',[' + dn.DETERMINANT_NAME + ']'
                                     FROM
                                          ( SELECT
                                                DETERMINANT_NAME
                                            FROM
                                                @Determinant_Names ) dn
                                     WHERE
                                          DETERMINANT_NAME LIKE '%UOM%'
                                     ORDER BY
                                          dn.DETERMINANT_NAME
                                   FOR
                                     XML PATH('') ), 1, 1, '') 

     
      SELECT
            @SQL_STR = 'SELECT              
			      chrd1.Client_Name as Client
			      ,chrd1.Contracting_Entity
                 ,isnull(chrd1.site_name,chrd1.GROUP_NAME) [Site/Group]
                 ,chrd1.Duns_Number [Company/Credit Number]
                 ,chrd1.ACCOUNT_NUMBER [Account#]
                 ,chrd1.Alternate_Account_Number [Alternate Account#]
                 ,STUFF(mn.Meter_Number, LEN(mn.Meter_Number), 1, '''') [Meter Point ID]
                 ,chrd1.ADDRESS [Street]
                 ,chrd1.City [City]
                 ,chrd1.State_Name [State]
                 ,chrd1.Country_Name [Country]
                 ,chrd1.ZipCode [Postal Code]
                 ,CAST(ROUND(mc.Max_Capacity,0) AS DECIMAL(32, 0)) [Max Capacity]
                 ,mc.Max_Capacity_uom [Max Capacity UOM]
				 ,CAST(ROUND(lv.Load_Profile_Volume,0) AS DECIMAL(32, 0)) [Load Profile Volume]
				 ,lv.Load_Profile_Volume_uom [Load Profile Volume UOM],' + @pivot_String + '
			    ,STUFF(rn.Rate_Name, LEN(rn.Rate_Name), 1, '''') as [Supply Level] 
			    ,STUFF(mt.Meter_Type, LEN(mt.Meter_Type), 1, '''') [Metering]
			    ,chrd1.Vendor_Name as [Distribution Company]
			    ,CASE WHEN ' + CAST(@Is_Term_Date_Specific AS VARCHAR) + ' = 1 THEN ( CONVERT(VARCHAR(12), DATEPART(DD, MIN(cltd.Start_dt))) + ''-'' + SUBSTRING(CONVERT(VARCHAR(12), MIN(cltd.Start_dt), 106), 4, 3) + ''-'' + SUBSTRING(CONVERT(VARCHAR(12), MIN(cltd.Start_dt), 106), 10, 2) )
                             WHEN ' + CAST(@Is_Term_Date_Specific AS VARCHAR) + ' = 0 THEN ( SUBSTRING(CONVERT(VARCHAR(12), MIN(cltd.Start_dt), 106), 4, 3) + ''-'' + SUBSTRING(CONVERT(VARCHAR(12), MIN(cltd.Start_dt), 106), 10, 2) )
                        END as [Start]
                 ,CASE WHEN ' + CAST(@Is_Term_Date_Specific AS VARCHAR) + ' = 1 THEN ( CONVERT(VARCHAR(12), DATEPART(DD, MIN(cltd.End_Dt))) + ''-'' + SUBSTRING(CONVERT(VARCHAR(12), MIN(cltd.End_Dt), 106), 4, 3) + ''-'' + SUBSTRING(CONVERT(VARCHAR(12),MIN(cltd.End_Dt), 106), 10, 2) )
                             WHEN ' + CAST(@Is_Term_Date_Specific AS VARCHAR) + ' = 0 THEN ( SUBSTRING(CONVERT(VARCHAR(12), MIN(cltd.End_Dt), 106), 4, 3) + ''-'' + SUBSTRING(CONVERT(VARCHAR(12), MIN(cltd.End_Dt), 106), 10, 2) )
                        END as [End]
			    ,Case when chrd1.Is_Bid_Group = 1 then chrd1.SR_RFP_BID_GROUP_ID ELSE chrd1.SR_RFP_ACCOUNT_ID END SR_Account_Group_Id,chrd1.Is_Bid_Group
   
					 FROM
						  ( SELECT
								src.Client_Id
							   ,src.SR_RFP_ACCOUNT_ID
							   ,src.DETERMINANT_NAME
							   ,src.Determinant_Name_Uom  
							  ,src.LP_VALUE
							   ,src.Entity_Name
							FROM
					(SELECT
                                        chrd.Client_Id
										   ,chrd.SR_RFP_ACCOUNT_ID
										   ,dv.DETERMINANT_NAME
										   ,dv.DETERMINANT_NAME + '' Uom'' AS Determinant_Name_Uom  
										  ,CAST(ROUND(dv.LP_VALUE,0) AS DECIMAL(32, 0)) LP_VALUE
										   ,e.Entity_Name
							 FROM
                                         #Client_Hier_Rfp_Data chrd
                                          left Outer Join #Determinant_Values_Exclude_Total_Usage dv
                                          on dv.SR_RFP_ACCOUNT_ID = chrd.SR_RFP_ACCOUNT_ID
                                          Left Outer JOIN dbo.ENTITY e
                                                ON e.ENTITY_ID = dv.DETERMINANT_UNIT_TYPE_ID
                     
												                                      
                                          
                                      )src) as t 
            PIVOT( MIN(LP_VALUE) FOR DETERMINANT_NAME IN ( ' + @pivot_String1 + '  ) ) AS pvt 
            PIVOT( MIN(Entity_Name) FOR Determinant_Name_Uom IN ( ' + @pivot_String2 + '  ) ) AS pvt1 
            INNER JOIN #Client_Hier_Rfp_Data chrd1
            on pvt1.Client_Id = chrd1.Client_Id and pvt1.SR_RFP_ACCOUNT_ID = chrd1.SR_RFP_ACCOUNT_ID
            LEFT OUTER JOIN #Contract_Longest_Term_Dates cltd
            on   chrd1.SR_RFP_ACCOUNT_ID = cltd.SR_RFP_ACCOUNT_ID
             CROSS APPLY ( SELECT
                              rch.Meter_Number + '',''
                          FROM
                              #Client_Hier_Rfp_Data rch
                          WHERE
                              rch.SR_RFP_ACCOUNT_ID = chrd1.Sr_Rfp_Account_Id
                          GROUP BY
                              rch.Meter_Number
            FOR
                          XML PATH('''') ) mn ( Meter_Number )
            CROSS APPLY ( SELECT
                              rch.Meter_Type + '',''
                          FROM
                              #Client_Hier_Rfp_Data rch
                          WHERE
                              rch.SR_RFP_ACCOUNT_ID = chrd1.Sr_Rfp_Account_Id
                          GROUP BY
                              rch.Meter_Type
            FOR
                          XML PATH('''') ) mt ( Meter_Type )
              
               CROSS APPLY ( SELECT ( SELECT
                                          CAST(rch.Rate_Name AS VARCHAR(MAX)) + '',''
                                      FROM
                                          #Client_Hier_Rfp_Data rch
                                      WHERE
                                          rch.SR_RFP_ACCOUNT_ID = chrd1.Sr_Rfp_Account_Id
                                      GROUP BY
                                          rch.Rate_Name
                        FOR
                                       XML PATH('''')
                                             ,TYPE).value(''.'', ''VARCHAR(MAX)'') Rate_Name ) rn
                                      
            LEFT OUTER JOIN #Max_Capacity mc
            on  mc.SR_RFP_ACCOUNT_ID = chrd1.SR_RFP_ACCOUNT_ID
            LEFT OUTER JOIN #LP_Volume lv
            on lv.SR_RFP_ACCOUNT_ID = chrd1.SR_RFP_ACCOUNT_ID
            group by chrd1.Client_Name
                 ,chrd1.site_name,chrd1.GROUP_NAME
                 ,chrd1.Duns_Number 
                 ,chrd1.ACCOUNT_NUMBER
                 ,chrd1.Alternate_Account_Number
                 ,STUFF(mn.Meter_Number, LEN(mn.Meter_Number), 1, '''') 
                 ,STUFF(mt.Meter_Type, LEN(mt.Meter_Type), 1, '''') 
                 ,chrd1.ADDRESS
                 ,chrd1.City
                 ,chrd1.State_Name
                 ,chrd1.Country_Name
                 ,chrd1.ZipCode
                 ,mc.Max_Capacity
                 ,mc.Max_Capacity_uom
				,lv.Load_Profile_Volume
				,lv.Load_Profile_Volume_uom
				,cltd.Start_dt
				,cltd.End_dt				
				,chrd1.Vendor_Name
				,chrd1.SR_RFP_ACCOUNT_ID
				,chrd1.Is_Bid_Group
				,chrd1.SR_RFP_BID_GROUP_ID
				,STUFF(rn.Rate_Name, LEN(rn.Rate_Name), 1, '''')
				,chrd1.Contracting_Entity
            order by chrd1.Client_Name
            '
            
      
                        
 
      IF @pivot_String IS NOT NULL 
            BEGIN
                  EXECUTE (@SQL_STR)
            END     
      ELSE 
            BEGIN
                  SELECT
                        chrd1.Client_Name AS Client
                       ,chrd1.Contracting_Entity
                       ,ISNULL(chrd1.site_name, chrd1.GROUP_NAME) [Site/Group]
                       ,chrd1.Duns_Number [Company/Credit Number]
                       ,chrd1.ACCOUNT_NUMBER [Account#]
                       ,chrd1.Alternate_Account_Number [Alternate Account#]
                       ,STUFF(mn.Meter_Number, LEN(mn.Meter_Number), 1, '') [Meter Point ID]
                       ,chrd1.ADDRESS [Street]
                       ,chrd1.City [City]
                       ,chrd1.State_Name [State]
                       ,chrd1.Country_Name [Country]
                       ,chrd1.ZipCode [Postal Code]
                       ,CAST(ROUND(mc.Max_Capacity, 0) AS DECIMAL(32, 0)) [Max Capacity]
                       ,mc.Max_Capacity_uom [Max Capacity UOM]
                       ,CAST(ROUND(lv.Load_Profile_Volume, 0) AS DECIMAL(32, 0)) [Load Profile Volume]
                       ,lv.Load_Profile_Volume_uom [Load Profile Volume UOM]
                       ,STUFF(rn.Rate_Name, LEN(rn.Rate_Name), 1, '') AS [Supply Level]
                       ,STUFF(mt.Meter_Type, LEN(mt.Meter_Type), 1, '') [Metering]
                       ,chrd1.Vendor_Name AS [Distribution Company]
                       ,CASE WHEN @Is_Term_Date_Specific = 1 THEN ( CONVERT(VARCHAR(12), DATEPART(DD, MIN(cltd.Start_dt))) + '-' + SUBSTRING(CONVERT(VARCHAR(12), MIN(cltd.Start_dt), 106), 4, 3) + '-' + SUBSTRING(CONVERT(VARCHAR(12), MIN(cltd.Start_dt), 106), 10, 2) )
                             WHEN @Is_Term_Date_Specific = 0 THEN ( SUBSTRING(CONVERT(VARCHAR(12), MIN(cltd.Start_dt), 106), 4, 3) + '-' + SUBSTRING(CONVERT(VARCHAR(12), MIN(cltd.Start_dt), 106), 10, 2) )
                        END AS [Start]
                       ,CASE WHEN @Is_Term_Date_Specific = 1 THEN ( CONVERT(VARCHAR(12), DATEPART(DD, MIN(cltd.End_Dt))) + '-' + SUBSTRING(CONVERT(VARCHAR(12), MIN(cltd.End_Dt), 106), 4, 3) + '-' + SUBSTRING(CONVERT(VARCHAR(12), MIN(cltd.End_Dt), 106), 10, 2) )
                             WHEN @Is_Term_Date_Specific = 0 THEN ( SUBSTRING(CONVERT(VARCHAR(12), MIN(cltd.End_Dt), 106), 4, 3) + '-' + SUBSTRING(CONVERT(VARCHAR(12), MIN(cltd.End_Dt), 106), 10, 2) )
                        END AS [End]
                       ,CASE WHEN chrd1.Is_Bid_Group = 1 THEN chrd1.SR_RFP_BID_GROUP_ID
                             ELSE chrd1.SR_RFP_ACCOUNT_ID
                        END SR_Account_Group_Id
                       ,chrd1.Is_Bid_Group
                  FROM
                        #Client_Hier_Rfp_Data chrd1
                        LEFT OUTER JOIN #Contract_Longest_Term_Dates cltd
                              ON chrd1.SR_RFP_ACCOUNT_ID = cltd.SR_RFP_ACCOUNT_ID
                        CROSS APPLY ( SELECT
                                          rch.Meter_Number + ','
                                      FROM
                                          #Client_Hier_Rfp_Data rch
                                      WHERE
                                          rch.SR_RFP_ACCOUNT_ID = chrd1.Sr_Rfp_Account_Id
                                      GROUP BY
                                          rch.Meter_Number
                        FOR
                                      XML PATH('') ) mn ( Meter_Number )
                        CROSS APPLY ( SELECT
                                          rch.Meter_Type + ','
                                      FROM
                                          #Client_Hier_Rfp_Data rch
                                      WHERE
                                          rch.SR_RFP_ACCOUNT_ID = chrd1.Sr_Rfp_Account_Id
                                      GROUP BY
                                          rch.Meter_Type
                        FOR
                                      XML PATH('') ) mt ( Meter_Type )
                        CROSS APPLY ( SELECT (
                                          SELECT
                                                CAST(rch.Rate_Name AS VARCHAR(MAX)) + ','
                                          FROM
                                                #Client_Hier_Rfp_Data rch
                                          WHERE
                                                rch.SR_RFP_ACCOUNT_ID = chrd1.Sr_Rfp_Account_Id
                                          GROUP BY
                                                rch.Rate_Name
                                      FOR
                                          XML PATH('')
                                             ,TYPE).value('.', 'VARCHAR(MAX)') Rate_Name ) rn
                        LEFT OUTER JOIN #Max_Capacity mc
                              ON mc.SR_RFP_ACCOUNT_ID = chrd1.SR_RFP_ACCOUNT_ID
                        LEFT OUTER JOIN #LP_Volume lv
                              ON lv.SR_RFP_ACCOUNT_ID = chrd1.SR_RFP_ACCOUNT_ID
                  GROUP BY
                        chrd1.Client_Name
                       ,chrd1.site_name
                       ,chrd1.GROUP_NAME
                       ,chrd1.Duns_Number
                       ,chrd1.ACCOUNT_NUMBER
                       ,chrd1.Alternate_Account_Number
                       ,STUFF(mn.Meter_Number, LEN(mn.Meter_Number), 1, '')
                       ,STUFF(mt.Meter_Type, LEN(mt.Meter_Type), 1, '')
                       ,chrd1.ADDRESS
                       ,chrd1.City
                       ,chrd1.State_Name
                       ,chrd1.Country_Name
                       ,chrd1.ZipCode
                       ,mc.Max_Capacity
                       ,mc.Max_Capacity_uom
                       ,lv.Load_Profile_Volume
                       ,lv.Load_Profile_Volume_uom
                       ,cltd.Start_dt
                       ,cltd.End_dt
                       ,chrd1.Vendor_Name
                       ,chrd1.SR_RFP_ACCOUNT_ID
                       ,chrd1.SR_RFP_BID_GROUP_ID
                       ,chrd1.Is_Bid_Group
                       ,STUFF(rn.Rate_Name, LEN(rn.Rate_Name), 1, '')
                       ,chrd1.Contracting_Entity
                  ORDER BY
                        chrd1.Client_Name
	  
            END
	  	

   
                              
      DROP TABLE #Client_Hier_Rfp_Data
      DROP TABLE #Contract_Longest_Term_Dates
      DROP TABLE #SR_RFP_Account_Id
      DROP TABLE #LP_Volume
      DROP TABLE #Max_Capacity
      DROP TABLE #Determinant_Values_Exclude_Total_Usage
 


END








;
GO




GRANT EXECUTE ON  [dbo].[Sr_Rfp_Supply_Point_Summary_Sel_By_Rfp_Account_Group_Id] TO [CBMSApplication]
GO
