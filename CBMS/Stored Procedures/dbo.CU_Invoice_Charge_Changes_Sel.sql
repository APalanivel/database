
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	 dbo.CU_Invoice_Charge_Changes_Sel

DESCRIPTION:
			This procedure is used to select changed records between two versions from CU_INVOICE_CHARGE.

INPUT PARAMETERS:
	Name						DataType		Default	Description
------------------------------------------------------------
@Last_Version_Id				BIGINT			Beginnng Change tracking Version Id (From Persistent vaiables) 			
@Current_Version_Id				BIGINT			Current Change tracking Version Id

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
It will execute only through the replication replacement SSIS package.

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RK			Raghu Kalvapudi
	KVK			VInay K
	RKV         Ravi Kumar Vegesna
MODIFICATIONS
	Initials	Date			Modification
------------------------------------------------------------
	RK			07/31/2013		Created
	KVk			09/17/2014		Modified to have select correct result
	RKV			2015-06-30      Added column EC_Invoice_Sub_Bucket_Master_Id as part of AS400
******/

CREATE PROCEDURE [dbo].[CU_Invoice_Charge_Changes_Sel]
      ( 
       @Last_Version_Id BIGINT
      ,@Current_Version_Id BIGINT
      ,@CU_I_Last_Version_Id BIGINT )
AS 
BEGIN
      SET NOCOUNT ON;

      SELECT
            CONVERT(CHAR(1), cuic_ct.SYS_CHANGE_OPERATION) Op_Code
           ,cuic_ct.CU_INVOICE_CHARGE_ID
           ,NULL CU_INVOICE_ID
           ,NULL COMMODITY_TYPE_ID
           ,NULL UBM_SERVICE_TYPE_ID
           ,NULL UBM_SERVICE_CODE
           ,NULL UBM_BUCKET_CODE
           ,NULL CHARGE_NAME
           ,NULL CHARGE_VALUE
           ,NULL UBM_INVOICE_DETAILS_ID
           ,NULL CU_DETERMINANT_CODE
           ,NULL Bucket_Master_Id
           ,NULL EC_Invoice_Sub_Bucket_Master_Id
      FROM
            changetable(CHANGES dbo.CU_INVOICE_CHARGE, @Last_Version_Id) cuic_ct
      WHERE
            cuic_ct.SYS_Change_Version <= @Current_Version_Id
            AND CONVERT(CHAR(1), cuic_ct.SYS_CHANGE_OPERATION) = 'D'
      UNION ALL
      SELECT
            'U' Op_Code --Insert or update
           ,cic.CU_INVOICE_CHARGE_ID
           ,cic.CU_INVOICE_ID
           ,cic.COMMODITY_TYPE_ID
           ,cic.UBM_SERVICE_TYPE_ID
           ,cic.UBM_SERVICE_CODE
           ,cic.UBM_BUCKET_CODE
           ,cic.CHARGE_NAME
           ,cic.CHARGE_VALUE
           ,cic.UBM_INVOICE_DETAILS_ID
           ,cic.CU_DETERMINANT_CODE
           ,cic.Bucket_Master_Id
           ,cic.EC_Invoice_Sub_Bucket_Master_Id
      FROM
            dbo.CU_INVOICE_CHARGE AS cic
            JOIN ( SELECT
                        cuic.CU_INVOICE_CHARGE_ID
                   FROM
                        changetable(CHANGES dbo.CU_INVOICE_CHARGE, @Last_Version_Id) cuic_ct
                        INNER JOIN dbo.CU_INVOICE_CHARGE cuic
                              ON cuic.CU_INVOICE_CHARGE_ID = cuic_ct.CU_INVOICE_CHARGE_ID
                        INNER JOIN dbo.CU_INVOICE ci
                              ON ci.CU_INVOICE_ID = cuic.CU_INVOICE_ID
                   WHERE
                        cuic_ct.SYS_Change_Version <= @Current_Version_Id
                        AND ci.IS_PROCESSED = 1
                        AND ci.IS_REPORTED = 1
                   UNION
					--changes from CU_Invoice
                   SELECT
                        cuic.CU_INVOICE_CHARGE_ID
                   FROM
                        changetable(CHANGES dbo.CU_INVOICE, @CU_I_Last_Version_Id) cui_ct
                        INNER JOIN dbo.CU_INVOICE cui
                              ON cui.CU_INVOICE_ID = cui_ct.CU_INVOICE_ID
                        INNER JOIN dbo.CU_INVOICE_CHARGE cuic
                              ON cuic.CU_INVOICE_ID = cui.CU_INVOICE_ID
                   WHERE
                        cui_ct.SYS_Change_Version <= @Current_Version_Id
                        AND cui.IS_PROCESSED = 1
                        AND cui.IS_REPORTED = 1 ) AS change
                  ON change.CU_INVOICE_CHARGE_ID = cic.CU_INVOICE_CHARGE_ID



END;

;
GO


GRANT EXECUTE ON  [dbo].[CU_Invoice_Charge_Changes_Sel] TO [ETL_Execute]
GO
