SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_LP_ADD_COMMENTS_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
	@user_id varchar(10),
	@session_id varchar(20),
	@rfp_account_id int,
	@comment varchar(4000),
	@username varchar(30),
	@commentDate datetime
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- Test Insert
--exec dbo.SR_RFP_LP_ADD_COMMENTS_P
--	@user_id = 8870,
--	@session_id = -1,
--	@rfp_account_id = 22297,
--	@comment = 'Test Data',
--	@username = 'lgriffith1',
--	@commentDate = '2009-04-17 12:00:00.000' 
--go

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_LP_ADD_COMMENTS_P
	@user_id varchar(10),
	@session_id varchar(20),
	@rfp_account_id int,
	@comment varchar(4000),
	@username varchar(30),
	@commentDate datetime
	AS
	
set nocount on

	declare @user_info_id int
	select	@user_info_id = user_info_id	
	from  user_info(nolock) where username =@username

	INSERT INTO sr_rfp_lp_comment(sr_rfp_account_id, comment, user_info_id, comment_date)
	VALUES (@rfp_account_id, @comment, @user_info_id, @commentDate)
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_LP_ADD_COMMENTS_P] TO [CBMSApplication]
GO
