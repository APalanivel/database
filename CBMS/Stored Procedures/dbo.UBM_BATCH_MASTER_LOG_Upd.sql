SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 dbo.UBM_UPDATE_LOG_STATUS_FAILURE_P    

 DESCRIPTION:     
 
	Used to update the UBM_BATCH_MASTER_LOG and Feed File map status

 INPUT PARAMETERS:    
 Name						DataType  Default Description    
------------------------------------------------------------    
@Ubm_Batch_Master_Log_Id	INTEGER
 @BATCH_FAILURE_REASON		VARCHAR(500)
 @Status					VARCHAR(200)
                            
 OUTPUT PARAMETERS:    
 Name			DataType  Default Description    
------------------------------------------------------------    
 USAGE EXAMPLES:    
------------------------------------------------------------    
  
    
 AUTHOR INITIALS:
 Initials	Name
 PR			Pradip Rajput
 
------------------------------------------------------------    
 MODIFICATIONS  
 Initials		Date			Modification    
------------------------------------------------------------              
 PR				2017-09-13		HG
******/
CREATE PROCEDURE [dbo].[UBM_BATCH_MASTER_LOG_Upd]
      (
       @Ubm_Batch_Master_Log_Id INTEGER
      ,@BATCH_FAILURE_REASON VARCHAR(500)
      ,@Status VARCHAR(200) )
AS
BEGIN
   
      SET NOCOUNT ON 
      DECLARE @StatusId INT  

      SELECT
            @StatusId = ENTITY_ID
      FROM
            ENTITY
      WHERE
            ENTITY_NAME = @Status
            AND ENTITY_TYPE = 656  
            
      UPDATE
            dbo.UBM_BATCH_MASTER_LOG
      SET
            BATCH_FAILURE_REASON = @BATCH_FAILURE_REASON
           ,STATUS_TYPE_ID = @StatusId
      WHERE
            UBM_BATCH_MASTER_LOG_ID = @Ubm_Batch_Master_Log_Id   
   
      EXEC dbo.UBM_Feed_FIle_Status_Update
            @Ubm_Batch_Master_Log_Id
           ,@Status

END
;
GO
GRANT EXECUTE ON  [dbo].[UBM_BATCH_MASTER_LOG_Upd] TO [CBMSApplication]
GO
