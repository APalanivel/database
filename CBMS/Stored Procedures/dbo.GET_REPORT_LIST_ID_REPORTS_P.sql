SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.GET_REPORT_LIST_ID_REPORTS_P
@userId varchar(10),
@sessionId varchar(10),
@reportName varchar(60)

as
	set nocount on	
	select	REPORT_LIST_ID
	
	from	REPORT_LIST
	
	where  REPORT_NAME like @reportName
GO
GRANT EXECUTE ON  [dbo].[GET_REPORT_LIST_ID_REPORTS_P] TO [CBMSApplication]
GO
