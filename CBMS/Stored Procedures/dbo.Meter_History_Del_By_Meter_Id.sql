SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
  
NAME: [DBO].[Meter_History_Del_By_Meter_Id]  
  
DESCRIPTION:  
  
 To delete all the history associated with a Meter  
  
INPUT PARAMETERS:  
NAME   DATATYPE  DEFAULT  DESCRIPTION       
------------------------------------------------------------  
@Meter_Id  INT  
@User_Info_Id INT    NULL  
@Is_Called_By_Delete_Meter BIT 0   Utility_Account_History_Del_By_Account_Id procedure will call this procedure to delete the meters associated with the account that time this value will be 0.  
  
OUTPUT PARAMETERS:  
NAME   DATATYPE DEFAULT  DESCRIPTION  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
  
 BEGIN TRAN  
  
  EXEC dbo.Meter_History_Del_By_Meter_Id 190,49  
  
 ROLLBACK TRAN  
   
 SELECT TOP 10 mat.EC_Meter_Attribute_Tracking_Id,mat.Meter_Id   
 FROM dbo.EC_Meter_Attribute_Tracking mat INNER JOIN dbo.METER mtr ON mat.Meter_Id = mtr.METER_ID  
  
 BEGIN TRAN  
  SELECT * FROM dbo.EC_Meter_Attribute_Tracking mat   
   INNER JOIN dbo.METER mtr ON mat.Meter_Id = mtr.METER_ID WHERE mtr.Meter_Id = 1  
  EXEC dbo.Meter_History_Del_By_Meter_Id 1,49  
  SELECT * FROM dbo.EC_Meter_Attribute_Tracking mat   
   INNER JOIN dbo.METER mtr ON mat.Meter_Id = mtr.METER_ID WHERE mtr.Meter_Id = 1  
 ROLLBACK TRAN  
   
 BEGIN TRAN  
  SELECT mat.EC_Meter_Attribute_Tracking_Id,mat.EC_Meter_Attribute_Id,ae.Account_Exception_Id FROM dbo.EC_Meter_Attribute_Tracking mat   
   INNER JOIN dbo.METER mtr ON mat.Meter_Id = mtr.METER_ID  
   INNER JOIN dbo.Account_Exception ae ON ae.Meter_Id = mtr.METER_ID WHERE mtr.Meter_Id = 749583  
  EXEC dbo.Meter_History_Del_By_Meter_Id 749583,49  
  SELECT mat.EC_Meter_Attribute_Tracking_Id,mat.EC_Meter_Attribute_Id,ae.Account_Exception_Id FROM dbo.EC_Meter_Attribute_Tracking mat   
   INNER JOIN dbo.METER mtr ON mat.Meter_Id = mtr.METER_ID  
   INNER JOIN dbo.Account_Exception ae ON ae.Meter_Id = mtr.METER_ID WHERE mtr.Meter_Id = 749583  
 ROLLBACK TRAN  
  
         
AUTHOR INITIALS:  
INITIALS NAME  
------------------------------------------------------------  
HG   Harihara Suthan G  
BCH   Balaraju  
RR   Raghu Reddy  
SC	 Screenivasulu Cheerala  
MODIFICATIONS  
INITIALS DATE  MODIFICATION  
------------------------------------------------------------  
HG   06/07/2010 Created  
      @User_Info_Id parameter currently not in use will be used when we add meter delete functionality.  
HG   08/20/2010 Added @Is_Called_By_Delete_Meter bit parameter to determine if we need to add the entry in to Application_Audit_Log.  
      If this procedure is called from Utility_Account_History_Del_By_Account_Id means we dont need to log anything ,if it is called from delete meter utility then we need to add log.  
BCH   2011-12-30  Deleted transactions (delete tool enhancement - Transactions are maintained in Application)  
       Using Client hier tables to get the Client name of a meter   
RR   2015-05-20 AS400 Modified to delete history from EC_Meter_Attribute_Tracking, Account_Exception  
RKV         2019-06-25 As part of Calculation Tester added functionality to delete the account details in table budget.Calculation_Tester_Audit    
SC		2020-05-21 Added delete code from Primary Meter Table
*/

CREATE PROCEDURE [dbo].[Meter_History_Del_By_Meter_Id]
    (
        @Meter_Id INT
        , @User_Info_Id INT = NULL
        , @Is_Called_By_Delete_Meter BIT = 0
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Sr_Rfp_Archive_Meter_Id INT
            , @Sr_Rfp_Archive_Meter_Contracts_Id INT
            , @Sr_Rfp_Account_Meter_Map_Id INT
            , @Meter_Cbms_Image_Map_Id INT
            , @History_Load_Profile_List_Id INT
            , @Meter_Number VARCHAR(50)
            , @Client_Name VARCHAR(50)
            , @Table_Name VARCHAR(10) = 'METER'
            , @Lookup_Value XML
            , @Application_Name VARCHAR(30) = 'Delete Meter'
            , @Audit_Function SMALLINT = -1
            , @User_Name VARCHAR(81)
            , @Current_Ts DATETIME
            , @Row_Counter INT
            , @Total_Row_Count INT
            , @EC_Meter_Attribute_Tracking_Id INT
            , @Account_Exception_Id INT
            , @Calculation_Tester_Audit_Id INT;
        DECLARE @SR_RFP_ARCHIVE_METER_LIST TABLE
              (
                  Sr_Rfp_Archive_Meter_Id INT PRIMARY KEY CLUSTERED
              );
        DECLARE @SR_RFP_ARCHIVE_METER_CONTRACTS_LIST TABLE
              (
                  Sr_Rfp_Archive_Meter_Contracts_Id INT PRIMARY KEY CLUSTERED
              );
        DECLARE @SR_RFP_ACCOUNT_METER_MAP_LIST TABLE
              (
                  Sr_Rfp_Account_Meter_Map_Id INT PRIMARY KEY CLUSTERED
              );
        DECLARE @Meter_Cbms_Image_Map_List TABLE
              (
                  Meter_Cbms_Image_Map_Id INT PRIMARY KEY CLUSTERED
              );
        DECLARE @History_Load_Profile_List TABLE
              (
                  History_Load_Profile_List_Id INT PRIMARY KEY CLUSTERED
              );
        DECLARE @EC_Meter_Attribute_Tracking TABLE
              (
                  EC_Meter_Attribute_Tracking_Id INT PRIMARY KEY CLUSTERED
                  , Row_Num INT IDENTITY(1, 1)
              );
        DECLARE @Account_Exception TABLE
              (
                  Account_Exception_Id INT PRIMARY KEY CLUSTERED
                  , Row_Num INT IDENTITY(1, 1)
              );
        DECLARE @Calculation_Tester_Audit TABLE
              (
                  Calculation_Tester_Audit_Id INT PRIMARY KEY CLUSTERED
                  , Row_Num INT IDENTITY(1, 1)
              );

        SELECT
            @Client_Name = ch.Client_Name
            , @Meter_Number = cha.Meter_Number
        FROM
            Core.Client_Hier ch
            JOIN Core.Client_Hier_Account cha
                ON cha.Client_Hier_Id = ch.Client_Hier_Id
        WHERE
            cha.Meter_Id = @Meter_Id;

        SELECT
            @User_Name = FIRST_NAME + SPACE(1) + LAST_NAME
        FROM
            dbo.USER_INFO
        WHERE
            USER_INFO_ID = @User_Info_Id;

        SET @Lookup_Value = (   SELECT
                                    SITE.SITE_NAME
                                    , ACCOUNT.ACCOUNT_NUMBER
                                    , ACCOUNT.ACCOUNT_ID
                                    , METER.METER_NUMBER
                                    , METER.METER_ID
                                    , RATE.RATE_NAME
                                    , Commodity.Commodity_Name
                                FROM
                                    SITE
                                    INNER JOIN ACCOUNT
                                        ON SITE.SITE_ID = ACCOUNT.SITE_ID
                                    INNER JOIN METER
                                        ON METER.ACCOUNT_ID = ACCOUNT.ACCOUNT_ID
                                    INNER JOIN RATE
                                        ON RATE.RATE_ID = METER.RATE_ID
                                    INNER JOIN Commodity
                                        ON Commodity.Commodity_Id = RATE.COMMODITY_TYPE_ID
                                WHERE
                                    METER.METER_ID = @Meter_Id
                                FOR XML AUTO);

        INSERT INTO @SR_RFP_ACCOUNT_METER_MAP_LIST
             (
                 Sr_Rfp_Account_Meter_Map_Id
             )
        SELECT
            mm.SR_RFP_ACCOUNT_METER_MAP_ID
        FROM
            dbo.SR_RFP_ACCOUNT rfpacc
            JOIN dbo.SR_RFP_ACCOUNT_METER_MAP mm
                ON rfpacc.SR_RFP_ACCOUNT_ID = mm.SR_RFP_ACCOUNT_ID
        WHERE
            mm.METER_ID = @Meter_Id
            AND rfpacc.IS_DELETED = 1;


        INSERT INTO @SR_RFP_ARCHIVE_METER_LIST
             (
                 Sr_Rfp_Archive_Meter_Id
             )
        SELECT
            arcmtr.SR_RFP_ARCHIVE_METER_ID
        FROM
            @SR_RFP_ACCOUNT_METER_MAP_LIST mm
            JOIN dbo.SR_RFP_ARCHIVE_METER arcmtr
                ON arcmtr.SR_RFP_ACCOUNT_METER_MAP_ID = mm.Sr_Rfp_Account_Meter_Map_Id;

        INSERT INTO @SR_RFP_ARCHIVE_METER_CONTRACTS_LIST
             (
                 Sr_Rfp_Archive_Meter_Contracts_Id
             )
        SELECT
            arcmtr.SR_RFP_ARCHIVE_METER_CONTRACTS_ID
        FROM
            @SR_RFP_ACCOUNT_METER_MAP_LIST mm
            JOIN dbo.SR_RFP_ARCHIVE_METER_CONTRACTS arcmtr
                ON arcmtr.SR_RFP_ACCOUNT_METER_MAP_ID = mm.Sr_Rfp_Account_Meter_Map_Id;


        INSERT INTO @Meter_Cbms_Image_Map_List
             (
                 Meter_Cbms_Image_Map_Id
             )
        SELECT
            imgmap.METER_CBMS_IMAGE_MAP_ID
        FROM
            dbo.METER_CBMS_IMAGE_MAP imgmap
        WHERE
            imgmap.METER_ID = @Meter_Id;

        INSERT INTO @History_Load_Profile_List
             (
                 History_Load_Profile_List_Id
             )
        SELECT
            hlp.HISTORIC_LOAD_PROFILE_ID
        FROM
            dbo.HISTORIC_LOAD_PROFILE hlp
        WHERE
            hlp.METER_ID = @Meter_Id;

        INSERT INTO @EC_Meter_Attribute_Tracking
             (
                 EC_Meter_Attribute_Tracking_Id
             )
        SELECT
            mat.EC_Meter_Attribute_Tracking_Id
        FROM
            dbo.EC_Meter_Attribute_Tracking mat
        WHERE
            mat.Meter_Id = @Meter_Id;

        INSERT INTO @Account_Exception
             (
                 Account_Exception_Id
             )
        SELECT
            ae.Account_Exception_Id
        FROM
            dbo.Account_Exception ae
        WHERE
            ae.Meter_Id = @Meter_Id;

        INSERT INTO @Calculation_Tester_Audit
             (
                 Calculation_Tester_Audit_Id
             )
        SELECT
            Calculation_Tester_Audit_Id
        FROM
            Budget.Calculation_Tester_Audit
        WHERE
            Meter_Id = @Meter_Id;
        BEGIN TRY



            WHILE EXISTS (SELECT    1 FROM  @SR_RFP_ARCHIVE_METER_LIST)
                BEGIN

                    SET @Sr_Rfp_Archive_Meter_Id = (SELECT  TOP 1   Sr_Rfp_Archive_Meter_Id FROM    @SR_RFP_ARCHIVE_METER_LIST);

                    EXEC dbo.Sr_Rfp_Archive_Meter_Del @Sr_Rfp_Archive_Meter_Id;

                    DELETE
                    @SR_RFP_ARCHIVE_METER_LIST
                    WHERE
                        Sr_Rfp_Archive_Meter_Id = @Sr_Rfp_Archive_Meter_Id;

                END;

            WHILE EXISTS (SELECT    1 FROM  @SR_RFP_ARCHIVE_METER_CONTRACTS_LIST)
                BEGIN

                    SET @Sr_Rfp_Archive_Meter_Contracts_Id = (   SELECT TOP 1
                                                                        Sr_Rfp_Archive_Meter_Contracts_Id
                                                                 FROM
                                                                        @SR_RFP_ARCHIVE_METER_CONTRACTS_LIST);

                    EXEC dbo.Sr_Rfp_Archive_Meter_Contracts_Del
                        @Sr_Rfp_Archive_Meter_Contracts_Id;

                    DELETE
                    @SR_RFP_ARCHIVE_METER_CONTRACTS_LIST
                    WHERE
                        Sr_Rfp_Archive_Meter_Contracts_Id = @Sr_Rfp_Archive_Meter_Contracts_Id;

                END;



            WHILE EXISTS (SELECT    1 FROM  @SR_RFP_ACCOUNT_METER_MAP_LIST)
                BEGIN

                    SET @Sr_Rfp_Account_Meter_Map_Id = (SELECT  TOP 1   Sr_Rfp_Account_Meter_Map_Id FROM    @SR_RFP_ACCOUNT_METER_MAP_LIST);

                    EXEC dbo.Sr_Rfp_Account_Meter_Map_Del @Sr_Rfp_Account_Meter_Map_Id;

                    DELETE
                    @SR_RFP_ACCOUNT_METER_MAP_LIST
                    WHERE
                        Sr_Rfp_Account_Meter_Map_Id = @Sr_Rfp_Account_Meter_Map_Id;

                END;

            WHILE EXISTS (SELECT    1 FROM  @Meter_Cbms_Image_Map_List)
                BEGIN

                    SET @Meter_Cbms_Image_Map_Id = (SELECT  TOP 1   Meter_Cbms_Image_Map_Id FROM    @Meter_Cbms_Image_Map_List);

                    EXEC dbo.Meter_Cbms_Image_Map_Del @Meter_Cbms_Image_Map_Id;

                    DELETE
                    @Meter_Cbms_Image_Map_List
                    WHERE
                        Meter_Cbms_Image_Map_Id = @Meter_Cbms_Image_Map_Id;

                END;

            WHILE EXISTS (SELECT    1 FROM  @History_Load_Profile_List)
                BEGIN

                    SET @History_Load_Profile_List_Id = (SELECT TOP 1   History_Load_Profile_List_Id FROM   @History_Load_Profile_List);

                    EXEC dbo.Historic_Load_Profile_Del @History_Load_Profile_List_Id;

                    DELETE
                    @History_Load_Profile_List
                    WHERE
                        History_Load_Profile_List_Id = @History_Load_Profile_List_Id;

                END;

            SELECT  @Total_Row_Count = MAX(Row_Num)FROM @EC_Meter_Attribute_Tracking;

            SET @Row_Counter = 1;
            WHILE (@Row_Counter <= @Total_Row_Count)
                BEGIN
                    SELECT
                        @EC_Meter_Attribute_Tracking_Id = EC_Meter_Attribute_Tracking_Id
                    FROM
                        @EC_Meter_Attribute_Tracking
                    WHERE
                        Row_Num = @Row_Counter;

                    DELETE
                    dbo.EC_Meter_Attribute_Tracking
                    WHERE
                        EC_Meter_Attribute_Tracking_Id = @EC_Meter_Attribute_Tracking_Id;

                    SET @Row_Counter = @Row_Counter + 1;
                END;


            SELECT  @Total_Row_Count = MAX(Row_Num)FROM @Account_Exception;

            SET @Row_Counter = 1;
            WHILE (@Row_Counter <= @Total_Row_Count)
                BEGIN
                    SELECT
                        @Account_Exception_Id = Account_Exception_Id
                    FROM
                        @Account_Exception
                    WHERE
                        Row_Num = @Row_Counter;

                    DELETE
                    dbo.Account_Exception
                    WHERE
                        Account_Exception_Id = @Account_Exception_Id;

                    SET @Row_Counter = @Row_Counter + 1;
                END;

            SELECT  @Total_Row_Count = MAX(Row_Num)FROM @Calculation_Tester_Audit;

            SET @Row_Counter = 1;
            WHILE (@Row_Counter <= @Total_Row_Count)
                BEGIN
                    SELECT
                        @Calculation_Tester_Audit_Id = Calculation_Tester_Audit_Id
                    FROM
                        @Calculation_Tester_Audit
                    WHERE
                        Row_Num = @Row_Counter;

                    DELETE
                    Budget.Calculation_Tester_Audit
                    WHERE
                        Calculation_Tester_Audit_Id = @Calculation_Tester_Audit_Id;

                    SET @Row_Counter = @Row_Counter + 1;
                END;

            EXEC dbo.Meter_Del @Meter_Id;

            IF @Is_Called_By_Delete_Meter = 1
                BEGIN

                    SET @Current_Ts = GETDATE();
                    EXEC dbo.Application_Audit_Log_Ins
                        @Client_Name
                        , @Application_Name
                        , @Audit_Function
                        , @Table_Name
                        , @Lookup_Value
                        , @User_Name
                        , @Current_Ts;
                END;
            --Delete from Primary meter table 
            IF EXISTS (   SELECT
                                1
                          FROM
                                Budget.Account_Commodity_Primary_Meter acpm
                          WHERE
                                acpm.Meter_Id = @Meter_Id)
                BEGIN
                    DELETE
                    acpm
                    FROM
                        Budget.Account_Commodity_Primary_Meter acpm
                    WHERE
                        acpm.Meter_Id = @Meter_Id;
                END;

        END TRY
        BEGIN CATCH

            EXEC usp_RethrowError;
        END CATCH;

    END;
GO


GRANT EXECUTE ON  [dbo].[Meter_History_Del_By_Meter_Id] TO [CBMSApplication]
GO
