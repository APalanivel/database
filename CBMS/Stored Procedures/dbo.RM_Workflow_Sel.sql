SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        dbo.RM_Workflow_Sel                    
                      
Description:                      
        To get deal ticket workflows
                      
Input Parameters:                      
    Name							DataType        Default     Description                        
--------------------------------------------------------------------------------
	
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	  
                    
Usage Examples:                          
--------------------------------------------------------------------------------
	
		EXEC dbo.RM_Workflow_Sel
		                     
 Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy   
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			2018-09-12     Global Risk Management - Created 
                     
******/
CREATE PROCEDURE [dbo].[RM_Workflow_Sel]
AS 
BEGIN
      SET NOCOUNT ON;
      
      SELECT
            Workflow_Id
           ,Workflow_Name
           ,Workflow_Description
      FROM
            dbo.Workflow
END;
GO
GRANT EXECUTE ON  [dbo].[RM_Workflow_Sel] TO [CBMSApplication]
GO
