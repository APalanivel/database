
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******               
                           
 NAME:  dbo.cbmsMarketIndex_GetAll
                              
 DESCRIPTION:                              
                               
                              
 INPUT PARAMETERS:              
                             
 Name					DataType			Default			Description              
---------------------------------------------------------------------
 @MyAccountId            int                                    
 
 
 OUTPUT PARAMETERS:                   
                              
 Name				DataType			Default			Description              
---------------------------------------------------------------------
                              
 USAGE EXAMPLES:              
---------------------------------------------------------------------
  
   EXEC dbo.cbmsMarketIndex_GetAll 11231

  
  
 AUTHOR INITIALS:              
             
 Initials			Name              
---------------------------------------------------------------------
 RKV				Ravi Kumar Raju                         
                               
 MODIFICATIONS:            
             
 Initials               Date             Modification            
---------------------------------------------------------------------
 RKV                    2016-06-09      Comments Added and updated to return the Benelux price based on the user Permission.
                             
******/ 
CREATE PROCEDURE [dbo].[cbmsMarketIndex_GetAll] ( @MyAccountId INT )
AS 
BEGIN
/*
Stored in ENTITY TABLE AS

ENTITY_TYPE = 165	
ENTITY_DESCRIPTION = 'Deal Type'
*/
      SET NOCOUNT ON;   
 
      DECLARE @Market_Index TABLE
            ( 
             Market_Index_Id INT
            ,Market_Index VARCHAR(200) )
 
      
      INSERT      INTO @Market_Index
                  ( 
                   Market_Index_Id
                  ,Market_Index )
                  SELECT
                        entity_id market_index_id
                       ,entity_name market_index
                  FROM
                        entity
                  WHERE
                        entity_type = 165
                        AND ENTITY_NAME NOT IN ( 'Benelux' )
                        
     
      INSERT      INTO @Market_Index
                  ( 
                   Market_Index_Id
                  ,Market_Index )
                  SELECT
                        entity_id market_index_id
                       ,entity_name market_index
                  FROM
                        entity
                  WHERE
                        entity_type = 165
                        AND ENTITY_NAME IN ( 'Benelux' )
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.USER_INFO_GROUP_INFO_MAP uigim
                                          INNER JOIN dbo.GROUP_INFO_PERMISSION_INFO_MAP gipim
                                                ON uigim.GROUP_INFO_ID = gipim.GROUP_INFO_ID
                                          INNER JOIN dbo.PERMISSION_INFO p
                                                ON gipim.PERMISSION_INFO_ID = p.PERMISSION_INFO_ID
                                     WHERE
                                          uigim.USER_INFO_ID = @MyAccountId
                                          AND P.PERMISSION_NAME = 'system.managemarketindexbenelux' )     
                                          
                                          
                                          
      SELECT
            Market_Index_Id
           ,Market_Index
      FROM
            @Market_Index
      ORDER BY
            Market_Index 

END
;
GO

GRANT EXECUTE ON  [dbo].[cbmsMarketIndex_GetAll] TO [CBMSApplication]
GO
