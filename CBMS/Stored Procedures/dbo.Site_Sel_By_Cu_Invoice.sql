SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.dbo.Site_Sel_By_Cu_Invoice

DESCRIPTION:

	Used to select Site and client details for the given Cu_invoice_id

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Cu_Invoice_Id	Int

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC dbo.Site_Sel_By_Cu_Invoice 2928687
	EXEC dbo.Site_Sel_By_Cu_Invoice 3452682
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hari
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG        	03/12/2010	Created
	SSR			03/18/2010	Removed Cu_invoice(Account_id) with cu_invoice_service_month(Account_id)
	SSR			07/09/2010  Removed the logic of fetching data from SAMM table
							(( SAMM.meter_disassociation_date > Account.Supplier_Account_Begin_Dt  
								OR SAMM.meter_disassociation_date IS NULL ))
							(After contract enhancement application is populating both Meter_association_date and meter_disassociation_Date column supplier_Account_meter_map and removing the entries when ever is_history = 1)

******/

CREATE PROCEDURE dbo.Site_Sel_By_Cu_Invoice
	@Cu_Invoice_Id	INT
AS
BEGIN

	SET NOCOUNT ON;

	WITH CTE_Site_sel
		AS
		  (
			SELECT
				m.account_id
				,meter_id
			FROM
				dbo.meter m
				JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
					ON cism.ACCOUNT_ID = m.ACCOUNT_ID
			WHERE
				cism.CU_INVOICE_ID = @Cu_Invoice_Id


			UNION

			SELECT
				 map.account_id
				,map.meter_id
			FROM
				dbo.supplier_account_meter_map AS map
				JOIN dbo.account a
					ON a.account_id = map.ACCOUNT_ID
				JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
					ON cism.ACCOUNT_ID = a.ACCOUNT_ID	
			WHERE
			cism.CU_INVOICE_ID = @Cu_Invoice_Id		
		)	  
				
	SELECT
		 s.SITE_ID
		,s.SITE_NAME
		,c.CLIENT_ID
		,c.CLIENT_NAME
	FROM
		CTE_Site_sel css
		JOIN dbo.METER m
			ON m.METER_ID =css.METER_ID	
		JOIN dbo.ACCOUNT uacc
			ON uacc.ACCOUNT_ID =m.ACCOUNT_ID	
		JOIN dbo.SITE s 
			ON s.SITE_ID =uacc.SITE_ID	
		JOIN dbo.Client c
			ON c.CLIENT_ID = s.Client_ID
		JOIN dbo.ENTITY actype
			ON actype.ENTITY_ID = uacc.ACCOUNT_TYPE_ID
	GROUP BY 
		s.SITE_ID
		,s.SITE_NAME
		,c.CLIENT_ID
		,c.CLIENT_NAME		
	ORDER BY
		s.SITE_NAME
		
END
GO
GRANT EXECUTE ON  [dbo].[Site_Sel_By_Cu_Invoice] TO [CBMSApplication]
GO
