SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.Account_DMO_Config_Ins_Upd
           
DESCRIPTION:             
			To insert/update DMO configurations
			
INPUT PARAMETERS:            
	Name				DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Account_Id			INT
    @Commodity_Id		INT
    @DMO_Start_Dt		DATE
    @DMO_End_Dt			DATE
    @User_Info_Id		INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT TOP 10 * FROM dbo.Account_DMO_Config
            
	BEGIN TRANSACTION
		SELECT * FROM  dbo.Account_DMO_Config 
			WHERE Account_Id = 1 AND Commodity_Id = 290 AND DMO_Start_Dt='2018-1-1' AND DMO_End_Dt='2018-12-31'
		EXEC dbo.Account_DMO_Config_Ins_Upd 1,290,'2018-1-1','2018-12-31',NULL,NULL,16
		SELECT * FROM  dbo.Account_DMO_Config 
			WHERE Account_Id = 1 AND Commodity_Id = 290 AND DMO_Start_Dt='2018-1-1' AND DMO_End_Dt='2018-12-31'
	ROLLBACK TRANSACTION
	
	BEGIN TRANSACTION
		DECLARE @Account_DMO_Config_Id INT = NULL
		SELECT * FROM  dbo.Account_DMO_Config 
			WHERE Account_Id = 1 AND Commodity_Id = 291 AND DMO_Start_Dt='2018-1-1' AND DMO_End_Dt='2018-12-31'
		EXEC dbo.Account_DMO_Config_Ins_Upd 1,291,'2018-1-1','2018-12-31',NULL,NULL,16
		SELECT @Account_DMO_Config_Id = Account_DMO_Config_Id FROM  dbo.Account_DMO_Config 
			WHERE Account_Id = 1 AND Commodity_Id = 291 AND DMO_Start_Dt='2018-1-1' AND DMO_End_Dt='2018-12-31'
		SELECT * FROM  dbo.Account_DMO_Config 
			WHERE Account_DMO_Config_Id = @Account_DMO_Config_Id
			EXEC dbo.Account_DMO_Config_Ins_Upd 1,291,'2018-1-1','2019-12-31',@Account_DMO_Config_Id,NULL,16
		SELECT * FROM  dbo.Account_DMO_Config 
			WHERE Account_DMO_Config_Id = @Account_DMO_Config_Id
	ROLLBACK TRANSACTION
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-01-23	Contract placeholder - CP-7 Created
******/

CREATE PROCEDURE [dbo].[Account_DMO_Config_Ins_Upd]
      ( 
       @Account_Id INT
      ,@Commodity_Id INT
      ,@DMO_Start_Dt DATE
      ,@DMO_End_Dt DATE
      ,@Account_DMO_Config_Id INT = NULL
      ,@Client_Hier_DMO_Config_Id INT = NULL
      ,@User_Info_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Client_Hier_Id INT
      
      SELECT
            @Client_Hier_Id = Client_Hier_Id
      FROM
            Core.Client_Hier_Account
      WHERE
            Account_Id = @Account_Id
      
      INSERT      INTO dbo.Account_DMO_Config
                  ( 
                   Account_Id
                  ,Commodity_Id
                  ,DMO_Start_Dt
                  ,DMO_End_Dt
                  ,Created_User_Id
                  ,Created_Ts
                  ,Updated_User_Id
                  ,Last_Change_Ts )
                  SELECT
                        @Account_Id
                       ,@Commodity_Id
                       ,@DMO_Start_Dt
                       ,@DMO_End_Dt
                       ,@User_Info_Id
                       ,GETDATE()
                       ,@User_Info_Id
                       ,GETDATE()
                  WHERE
                        @Account_DMO_Config_Id IS NULL
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Account_DMO_Config
                                         WHERE
                                          Account_Id = @Account_Id
                                          AND Commodity_Id = @Commodity_Id
                                          AND DMO_Start_Dt = @DMO_Start_Dt
                                          AND DMO_End_Dt = @DMO_End_Dt )
                                          
      UPDATE
            dbo.Account_DMO_Config
      SET   
            DMO_Start_Dt = @DMO_Start_Dt
           ,DMO_End_Dt = @DMO_End_Dt
           ,Updated_User_Id = @User_Info_Id
           ,Last_Change_Ts = GETDATE()
      WHERE
            @Account_DMO_Config_Id IS NOT NULL
            AND Account_DMO_Config_Id = @Account_DMO_Config_Id
            
      INSERT      INTO dbo.Account_Not_Applicable_DMO_Config
                  ( 
                   Client_Hier_Id
                  ,Account_Id
                  ,Client_Hier_DMO_Config_Id
                  ,Created_User_Id
                  ,Created_Ts )
                  SELECT
                        @Client_Hier_Id
                       ,@Account_Id
                       ,@Client_Hier_DMO_Config_Id
                       ,@User_Info_Id
                       ,GETDATE()
                  WHERE
                        @Client_Hier_DMO_Config_Id IS NOT NULL
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Account_Not_Applicable_DMO_Config
                                         WHERE
                                          Account_Id = @Account_Id
                                          AND Client_Hier_Id = @Client_Hier_Id
                                          AND Client_Hier_DMO_Config_Id = @Client_Hier_DMO_Config_Id )
      
      
END;
;



;
GO
GRANT EXECUTE ON  [dbo].[Account_DMO_Config_Ins_Upd] TO [CBMSApplication]
GO
