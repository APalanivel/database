SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********

NAME:                         
     [dbo].[Site_Name_Site_Reference_Number_Conversion_Upd]

DESCRIPTION:                                        

 INPUT PARAMETERS:                                        

 Name                        DataType         Default       Description                                      
---------------------------------------------------------------------------------------------------------------                                    


 OUTPUT PARAMETERS:                                        

 Name                        DataType         Default       Description                                      
---------------------------------------------------------------------------------------------------------------                                    

 USAGE EXAMPLES:                                                          
---------------------------------------------------------------------------------------------------------------                                                          

 BEGIN TRAN                              
 EXEC [dbo].[Site_Name_Site_Reference_Number_Conversion_Upd]                               
      
 ROLLBACK TRAN                                 


 AUTHOR INITIALS:                                      

 Initials   Name                                      
---------------------------------------------------------------------------------------------------------------                                                    
 ABK        Aditya Bharadwaj Kolipaka
                                                                  

 MODIFICATIONS:                                    

	Initials	Date        Modification                                    
---------------------------------------------------------------------------------------------------------------                                    
	ABK			05-09-2018  Created for DDCR - 36.                              
	
*********/

CREATE PROCEDURE [dbo].[Site_Name_Site_Reference_Number_Conversion_Upd]
AS
    BEGIN

        --	Creating a temp1 table to store	Site Id which has both New Site Name and Site Reference number

        CREATE TABLE #temp1
            (
                ID                        INT          NOT NULL IDENTITY(1, 1),
                Site_ID                   INT          NULL,
                New_Site_Name             VARCHAR(200) NULL,
                New_Site_Reference_Number VARCHAR(300) NULL,
                Email_Address             VARCHAR(500) NULL,
                Error_Msg                 VARCHAR(MAX),
                Is_Validation_Passed      BIT
                    DEFAULT (1)
            );

        -- Creating temp2 table to store -	Site Id which has only the Site Name and Site reference number is blank/Null

        CREATE TABLE #temp2
            (
                ID                        INT          NOT NULL IDENTITY(1, 1),
                Site_ID                   INT          NULL,
                New_Site_Name             VARCHAR(200) NULL,
                New_Site_Reference_Number VARCHAR(300) NULL,
                Email_Address             VARCHAR(500) NULL,
                Error_Msg                 VARCHAR(MAX),
                Is_Validation_Passed      BIT
                    DEFAULT (1)
            );

        -- Creating temp3 table to store -	Site Id which has valid Site reference number and Site Name is blank/null

        CREATE TABLE #temp3
            (
                ID                        INT          NOT NULL IDENTITY(1, 1),
                Site_ID                   INT          NULL,
                New_Site_Name             VARCHAR(200) NULL,
                New_Site_Reference_Number VARCHAR(300) NULL,
                Email_Address             VARCHAR(500) NULL,
                Error_Msg                 VARCHAR(MAX),
                Is_Validation_Passed      BIT
                    DEFAULT (1)
            );

        DECLARE
            @BatchSize    INT,
            @Bottom       INT,
            @Top          INT,
            @TotalRecords INT,
            @entity_id    INT,
            @user_info_id INT,
            @audit_type   INT;


        SELECT
            @entity_id = e.ENTITY_ID
        FROM
            dbo.ENTITY e
        WHERE
            e.ENTITY_NAME = 'SITE_TABLE'
            AND e.ENTITY_TYPE = 500
            AND e.ENTITY_DESCRIPTION = 'Table_Type';

        SELECT
            @user_info_id = ui.USER_INFO_ID
        FROM
            dbo.USER_INFO ui
        WHERE
            ui.USERNAME = 'conversion'
            AND ui.FIRST_NAME = 'System';


        SET @audit_type = 2;


        BEGIN

            SET @BatchSize = 100;
            SET @Bottom = 1;
            SET @Top = @BatchSize;

            INSERT INTO #temp1
                (
                    Site_ID,
                    New_Site_Name,
                    New_Site_Reference_Number,
                    Email_Address,
                    Error_Msg,
                    Is_Validation_Passed
                )
                        (SELECT
                                 s1.SITE_ID,
                                 s1.New_Site_Name,
                                 s1.New_Site_Reference_Number,
                                 s1.Email_Address,
                                 s1.Error_Msg,
                                 s1.Is_Validation_Passed
                         FROM
                                 #Update_SiteName_SiteRefNumbers_Stage s1
                             JOIN
                                 dbo.SITE                              s2
                                     ON s1.SITE_ID = s2.SITE_ID
                         WHERE
                                 s1.Is_Validation_Passed = 1
                                 AND s1.SITE_ID IS NOT NULL
                                 AND s1.New_Site_Name IS NOT NULL
                                 AND s1.New_Site_Reference_Number IS NOT NULL);

            -- Updating Site table in batches from the data in temp1 table

            SET @TotalRecords =
                (
                    SELECT
                        COUNT(*)
                    FROM
                        #temp1
                );

            WHILE (@Bottom <= @TotalRecords)
                BEGIN
                    UPDATE
                            s
                    SET
                            s.SITE_NAME = t.New_Site_Name,
                            s.SITE_REFERENCE_NUMBER = t.New_Site_Reference_Number
                    FROM
                            #temp1   t
                        JOIN
                            dbo.SITE s
                                ON t.Site_ID = s.SITE_ID
                    WHERE
                            t.ID
                    BETWEEN @Bottom AND @Top;


                    INSERT INTO dbo.ENTITY_AUDIT
                        (
                            ENTITY_ID,
                            ENTITY_IDENTIFIER,
                            USER_INFO_ID,
                            AUDIT_TYPE,
                            MODIFIED_DATE
                        )
                                SELECT
                                        @entity_id,
                                        Site_ID,
                                        @user_info_id,
                                        @audit_type,
                                        GETDATE()
                                FROM
                                        #temp1
                                WHERE
                                        ID
                                BETWEEN @Bottom AND @Top;

                    SET @Bottom = @Bottom + @BatchSize;
                    SET @Top = @Top + @BatchSize;

                END;
        END;

        -- temp2

        BEGIN

            SET @BatchSize = 100;
            SET @Bottom = 1;
            SET @Top = @BatchSize;

            INSERT INTO #temp2
                (
                    Site_ID,
                    New_Site_Name,
                    New_Site_Reference_Number,
                    Email_Address,
                    Error_Msg,
                    Is_Validation_Passed
                )
                        (SELECT
                                 s1.SITE_ID,
                                 s1.New_Site_Name,
                                 s1.New_Site_Reference_Number,
                                 s1.Email_Address,
                                 s1.Error_Msg,
                                 s1.Is_Validation_Passed
                         FROM
                                 #Update_SiteName_SiteRefNumbers_Stage s1
                             JOIN
                                 dbo.SITE                              s2
                                     ON s1.SITE_ID = s2.SITE_ID
                         WHERE
                                 s1.Is_Validation_Passed = 1
                                 AND s1.SITE_ID IS NOT NULL
                                 AND s1.New_Site_Name IS NOT NULL
                                 AND s1.New_Site_Reference_Number IS NULL);

            -- Updating Site table in batches from the data in temp2 table

            SET @TotalRecords =
                (
                    SELECT
                        COUNT(*)
                    FROM
                        #temp2
                );

            WHILE (@Bottom <= @TotalRecords)
                BEGIN
                    UPDATE
                            s
                    SET
                            s.SITE_NAME = t.New_Site_Name
                    FROM
                            #temp2   t
                        JOIN
                            dbo.SITE s
                                ON t.Site_ID = s.SITE_ID
                    WHERE
                            t.ID
                    BETWEEN @Bottom AND @Top;

                    SET @Bottom = @Bottom + @BatchSize;
                    SET @Top = @Top + @BatchSize;

                    INSERT INTO dbo.ENTITY_AUDIT
                        (
                            ENTITY_ID,
                            ENTITY_IDENTIFIER,
                            USER_INFO_ID,
                            AUDIT_TYPE,
                            MODIFIED_DATE
                        )
                                SELECT
                                        @entity_id,
                                        Site_ID,
                                        @user_info_id,
                                        @audit_type,
                                        GETDATE()
                                FROM
                                        #temp2
                                WHERE
                                        ID
                                BETWEEN @Bottom AND @Top;

                END;
        END;

        -- temp3

        BEGIN

            SET @BatchSize = 100;
            SET @Bottom = 1;
            SET @Top = @BatchSize;

            INSERT INTO #temp3
                (
                    Site_ID,
                    New_Site_Name,
                    New_Site_Reference_Number,
                    Email_Address,
                    Error_Msg,
                    Is_Validation_Passed
                )
                        (SELECT
                                 s1.SITE_ID,
                                 s1.New_Site_Name,
                                 s1.New_Site_Reference_Number,
                                 s1.Email_Address,
                                 s1.Error_Msg,
                                 s1.Is_Validation_Passed
                         FROM
                                 #Update_SiteName_SiteRefNumbers_Stage s1
                             JOIN
                                 dbo.SITE                              s2
                                     ON s1.SITE_ID = s2.SITE_ID
                         WHERE
                                 s1.Is_Validation_Passed = 1
                                 AND s1.SITE_ID IS NOT NULL
                                 AND s1.New_Site_Name IS NULL
                                 AND s1.New_Site_Reference_Number IS NOT NULL);

            -- Updating Site table in batches from the data in temp3 table

            SET @TotalRecords =
                (
                    SELECT
                        COUNT(*)
                    FROM
                        #temp3
                );

            WHILE (@Bottom <= @TotalRecords)
                BEGIN
                    UPDATE
                            s
                    SET
                            s.SITE_REFERENCE_NUMBER = t.New_Site_Reference_Number
                    FROM
                            #temp3   t
                        JOIN
                            dbo.SITE s
                                ON t.Site_ID = s.SITE_ID
                    WHERE
                            t.ID
                    BETWEEN @Bottom AND @Top;

                    INSERT INTO dbo.ENTITY_AUDIT
                        (
                            ENTITY_ID,
                            ENTITY_IDENTIFIER,
                            USER_INFO_ID,
                            AUDIT_TYPE,
                            MODIFIED_DATE
                        )
                                SELECT
                                        @entity_id,
                                        Site_ID,
                                        @user_info_id,
                                        @audit_type,
                                        GETDATE()
                                FROM
                                        #temp3
                                WHERE
                                        ID
                                BETWEEN @Bottom AND @Top;

                    SET @Bottom = @Bottom + @BatchSize;
                    SET @Top = @Top + @BatchSize;

                END;
        END;

        DROP TABLE #temp1;
        DROP TABLE #temp2;
        DROP TABLE #temp3;

    END;




GO
GRANT EXECUTE ON  [dbo].[Site_Name_Site_Reference_Number_Conversion_Upd] TO [CBMSApplication]
GO
