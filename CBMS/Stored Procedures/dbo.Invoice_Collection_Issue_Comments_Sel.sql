SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                
Name:   dbo.Invoice_Collection_Issue_Comments_Sel         
                
Description:                
          
                             
 Input Parameters:                
    Name        DataType   Default   Description                  
----------------------------------------------------------------------------------------                  
                         
   
 Output Parameters:                      
    Name        DataType   Default   Description                  
----------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
----------------------------------------------------------------------------------------     
  
   
     
declare @p18 dbo.tvp_Invoice_Collection_Source
exec dbo.[Invoice_Collection_Issue_Comments_Sel] 
@Invoice_Collection_Officer_Id=NULL,@Start_Date='2018-03-01',@End_Date=NULL,
@Client_Id  = 11624,@Site_Id=263210 ,@Account_ID=NULL,@Country_Id=NULL,@State_Id=NULL,
@Commodity_Id=NULL,@Vendor_Type=NULL,@Vendor_Id=NULL,@Issue_Status_Cd=102304,@Is_Blocker=NULL,
@Issue_Activity_Id=NULL,@AlternateAccount=NULL,@TypeOfIssueCodeId=NULL,@Issue_Entity_Owner_Cd=NULL,
@Tvp_Invoice_Collection_Sources=@p18  
       
   102335--72,1037  
    
     
Author Initials:                
    Initials  Name                
----------------------------------------------------------------------------------------                  
 RKV    Ravi Kumar Vegesna  
 Modifications:                
    Initials        Date   Modification                
----------------------------------------------------------------------------------------                  
     
 RKV             2018-03-13      SE2017-273 Create to get the Invoice_Collection_Issue_Comments.  
 RKV             2018-05-24      Maint-7414, mdified the code to get the invoice collection office, when client id is passed.          
 SLP            2019-09-28		 Included additional parameter    
 RKV            D20-295          Fixed the start and end date issue.      
 SLP			2019-10-25		 Performance issue  
                  
                        
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Issue_Comments_Sel]
(
    @Invoice_Collection_Officer_Id INT = NULL,
    @Start_Date DATE = NULL,
    @End_Date DATE = NULL,
    @Client_Id VARCHAR(MAX) = NULL, --'14278,14643',      
    @Site_Id INT = NULL,
    @Account_ID INT = NULL,
    @Country_Id INT = NULL,
    @State_Id INT = NULL,
    @Commodity_Id INT = NULL,
    @Vendor_Type CHAR(8) = NULL,
    @Vendor_Id VARCHAR(MAX) = NULL,
    @Issue_Status_Cd INT = NULL,
    @Issue_Activity_Id INT = NULL,
    @AlternateAccount VARCHAR(255) = NULL,
    @TypeOfIssueCodeId INT = NULL,
    @Is_Blocker BIT = NULL,
    @Issue_Entity_Owner_Cd VARCHAR(50) = NULL,
    @Tvp_Invoice_Collection_Sources tvp_Invoice_Collection_Source READONLY
)
WITH RECOMPILE
AS
BEGIN

    SET NOCOUNT ON;

    DECLARE @Source_Type_Client INT,
            @Source_Type_Account INT,
            @Source_Type_Vendor INT,
            @Contact_Level_Client INT,
            @Contact_Level_Account INT,
            @Contact_Level_Vendor INT;
    DECLARE @Cnt_Of_Invoice_Collection_Source BIT = 0,
            @Invoice_Collection_Type_Cd INT,
            @Invoice_Collection_Issue_Status_Cd INT;

    CREATE TABLE #Invoice_Collection_Account_Config_Ids
    (
        Invoice_Collection_Account_Config_Id INT,
        Account_Id INT,
        Invoice_Collection_Service_Start_Dt DATE,
        Invoice_Collection_Service_End_Dt DATE,
        Client_Id INT,
        Client_Name VARCHAR(255),
        Site_Id INT,
        Site_Name VARCHAR(255),
        Country_Id INT,
        Country_Name VARCHAR(255),
        State_Id INT,
        State_Name VARCHAR(255),
        Account_Number VARCHAR(255),
        Commodity_Id INT,
        Commodity_Name VARCHAR(255),
        Account_Type VARCHAR(255),
        Account_Vendor_Name VARCHAR(255),
        Account_Vendor_Id VARCHAR(255),
        Invoice_Collection_Alternative_Account_Number VARCHAR(255)
            PRIMARY KEY CLUSTERED (Invoice_Collection_Account_Config_Id)
    );

    SELECT @Cnt_Of_Invoice_Collection_Source = 1
    FROM @Tvp_Invoice_Collection_Sources tvp;

	DECLARE @Internal_Comment_Cd int, @External_Comment_Cd int
	SELECT @Internal_Comment_Cd =code_id FROM code C JOIN codeset cs 
	ON C.Codeset_Id=cs.Codeset_Id
	WHERE c.code_value ='IC Internal Comments'
	SELECT @External_Comment_Cd =code_id FROM code C JOIN codeset cs 
	ON C.Codeset_Id=cs.Codeset_Id
	WHERE c.code_value = 'IC External Comments'
	


    CREATE TABLE #Client_hier_Account_Details
    (
        Account_Vendor_Name VARCHAR(200),
        Account_Type CHAR(8),
        Account_Vendor_Id INT,
        Account_Id INT,
        Invoice_Collection_Account_Config_Id INT
    );

    CREATE TABLE #Consolidated_Billing_Account_Id
    (
        Account_Id INT
    );


    CREATE TABLE #Vendor_Account_Details
    (
        Account_Vendor_Name VARCHAR(200),
        Account_Type CHAR(8),
        Account_Vendor_Id INT,
        Account_Id INT,
        Invoice_Collection_Account_Config_Id INT
    );


    CREATE TABLE #Ic_Account_Details
    (
        Account_Vendor_Name VARCHAR(200),
        Account_Type CHAR(8),
        Account_Vendor_Id INT,
        Account_Id INT,
        Invoice_Collection_Account_Config_Id INT
    );

    CREATE TABLE #Invoice_Collection_Accounts
    (
        Invoice_Collection_Account_Config_Id INT,
        Account_Id INT,
        Invoice_Collection_Service_End_Dt DATE,
        Account_Vendor_Name VARCHAR(200),
        Account_Type CHAR(8),
        Account_Vendor_Id INT
    );
    CREATE TABLE #Vendor_Dtls
    (
        Account_Vendor_Name VARCHAR(200),
        Account_Vendor_Type CHAR(8),
        Account_Vendor_Id INT,
        Invoice_Collection_Account_Config_Id INT
    );

    CREATE CLUSTERED INDEX cx_#Vendor_Dtls
    ON #Vendor_Dtls (Invoice_Collection_Account_Config_Id);


    DECLARE @Filtered_Vendor_Id TABLE
    (
        Vendor_Id INT
    );

    INSERT INTO @Filtered_Vendor_Id
    (
        Vendor_Id
    )
    SELECT us.Segments
    FROM dbo.ufn_split(@Vendor_Id, ',') us;

    CREATE TABLE [#ConsolidatedResults]
    (
        Id INT IDENTITY PRIMARY KEY,
        [Client_Id] INT,
        [Client_Name] VARCHAR(255),
        [Site_Id] INT,
        [Site_Name] VARCHAR(255),
        [Country_Id] INT,
        [Country_Name] VARCHAR(255),
        [State_Id] INT,
        [State_Name] VARCHAR(255),
        [Commodity_Name] VARCHAR(255),
        [Account_Type] VARCHAR(255),
        [Account_Vendor_Name] VARCHAR(255),
        [Account_Vendor_Id] VARCHAR(255),
        [Account_Number] VARCHAR(255),
        [Account_Id] INT,
        [Collection_Start_Dt] VARCHAR(12),
        [Collection_End_Dt] VARCHAR(12),
        [Invoice_Collection_Issue_Log_Id] INT NOT NULL,
        [Event_Desc] NVARCHAR(MAX) NOT NULL,
        [Invoice_Collection_Activity_Id] INT,
        [Event_Type] VARCHAR(255),
        [Invoice_Collection_Issue_Event_Id] INT NOT NULL,
        [Code_Value] VARCHAR(255),
        [Invoice_Collection_Alternative_Account_Number] VARCHAR(255),
        [Created_Ts] DATETIME NOT NULL,
        [Is_Blocker] BIT NOT NULL,
        [Invoice_Collection_Account_Config_Id] INT NOT NULL,
        [Issue_Status_Cd] INT
    );

    CREATE TABLE [#ChasePeriods]
    (
        Id INT IDENTITY PRIMARY KEY,
        [Code_Value] VARCHAR(200),
        [Issue_Status_Cd] VARCHAR(200),
        [Invoice_Collection_Activity_Id] INT,
        [Invoice_Collection_Account_Config_Id] INT NOT NULL,
        [Period_to_chase] NVARCHAR(MAX)
    );

    CREATE TABLE [#EventIds]
    (
        Id INT IDENTITY PRIMARY KEY,
        [Code_Value] VARCHAR(200),
        [Issue_Status_Cd] VARCHAR(200),
        [Invoice_Collection_Activity_Id] INT,
        [Invoice_Collection_Account_Config_Id] INT NOT NULL,
        [Event_Desc] NVARCHAR(MAX) NOT NULL,
        [Invoice_Collection_Issue_Event_Id] NVARCHAR(MAX),
    );

    INSERT INTO #Invoice_Collection_Account_Config_Ids
    (
        Invoice_Collection_Account_Config_Id,
        Account_Id,
        Invoice_Collection_Service_Start_Dt,
        Invoice_Collection_Service_End_Dt,
        Client_Id,
        Client_Name,
        Site_Id,
        Site_Name,
        Country_Id,
        Country_Name,
        State_Id,
        State_Name,
        Account_Number,
        Commodity_Id,
        Commodity_Name,
        Account_Type,
        Account_Vendor_Name,
        Account_Vendor_Id,
        Invoice_Collection_Alternative_Account_Number
    )
    SELECT icac.Invoice_Collection_Account_Config_Id,
           MAX(icac.Account_Id),
           MAX(icac.Invoice_Collection_Service_Start_Dt),
           MAX(icac.Invoice_Collection_Service_End_Dt),
           MAX(ch.Client_Id),
           MAX(ch.Client_Name),
           MAX(ch.Site_Id),
           MAX(ch.Site_name),
           MAX(ch.Country_Id),
           MAX(ch.Country_Name),
           MAX(ch.State_Id),
           MAX(ch.State_Name),
           MAX(cha.Account_Number),
           MAX(cha.Commodity_Id),
           MAX(c.Commodity_Name),
           MAX(cha.Account_Type),
           MAX(cha.Account_Vendor_Name),
           MAX(cha.Account_Vendor_Id),
           MAX(icac.Invoice_Collection_Alternative_Account_Number)
    FROM dbo.Invoice_Collection_Account_Config icac
        INNER JOIN Core.Client_Hier_Account cha
            ON cha.Account_Id = icac.Account_Id
        INNER JOIN Core.Client_Hier ch
            ON ch.Client_Hier_Id = cha.Client_Hier_Id
        INNER JOIN dbo.Commodity c
            ON c.Commodity_Id = cha.Commodity_Id
        LEFT OUTER JOIN dbo.Account_Invoice_Collection_Source aics
            ON aics.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
               AND aics.Is_Primary = 1
        LEFT OUTER JOIN dbo.Code cics
            ON cics.Code_Id = aics.Invoice_Collection_Source_Cd
        LEFT OUTER JOIN dbo.Code cicst
            ON cicst.Code_Id = aics.Invoice_Source_Type_Cd
        LEFT OUTER JOIN dbo.Code cicsm
            ON cicsm.Code_Id = aics.Invoice_Source_Method_of_Contact_Cd
    WHERE (
              @Invoice_Collection_Officer_Id IS NULL
              OR icac.Invoice_Collection_Officer_User_Id = @Invoice_Collection_Officer_Id
          )
          AND
          (
              @Site_Id IS NULL
              OR ch.Site_Id = @Site_Id
          )
          AND
          (
              @State_Id IS NULL
              OR ch.State_Id = @State_Id
          )
          AND
          (
              @Country_Id IS NULL
              OR ch.Country_Id = @Country_Id
          )
          AND
          (
              @Account_ID IS NULL
              OR icac.Account_Id = @Account_ID
          )
          AND
          (
              @Commodity_Id IS NULL
              OR cha.Commodity_Id = @Commodity_Id
          )
          AND
          (
              @Client_Id IS NULL
              OR EXISTS
    (
        SELECT 1
        FROM dbo.ufn_split(@Client_Id, ',') us
        WHERE us.Segments = ch.Client_Id
    )
          )
          AND
          (
              @AlternateAccount IS NULL
              OR icac.Invoice_Collection_Alternative_Account_Number = @AlternateAccount
          )
          AND
          (
              @Cnt_Of_Invoice_Collection_Source = 0
              OR
              (
                  EXISTS
    (
        SELECT 1
        FROM @Tvp_Invoice_Collection_Sources tvp
        WHERE tvp.Column_Name = 'Invoice_Source_Type_Cd'
              AND aics.Invoice_Source_Type_Cd = CAST(tvp.Column_Value AS INT)
              AND tvp.Column_Type = cics.Code_Value
              AND cicst.Code_Value NOT IN ( 'Online', 'Mail Redirect' )
    )
                  AND EXISTS
    (
        SELECT 1
        FROM @Tvp_Invoice_Collection_Sources tvp
        WHERE tvp.Column_Name = 'Invoice_Source_Method_of_Contact_Cd'
              AND aics.Invoice_Source_Method_of_Contact_Cd = CAST(tvp.Column_Value AS INT)
              AND tvp.Column_Type = cics.Code_Value
              AND cicst.Code_Value NOT IN ( 'Online', 'Mail Redirect' )
    )
              )
              OR (EXISTS
    (
        SELECT 1
        FROM @Tvp_Invoice_Collection_Sources tvp
        WHERE tvp.Column_Name = 'Invoice_Source_Type_Cd'
              AND aics.Invoice_Source_Type_Cd = CAST(tvp.Column_Value AS INT)
              AND tvp.Column_Type = cics.Code_Value
              AND cicst.Code_Value IN ( 'Online', 'Mail Redirect' )
    )
                 )
              OR (EXISTS
    (
        SELECT 1
        FROM @Tvp_Invoice_Collection_Sources tvp
        WHERE tvp.Column_Name = 'Invoice_Source_Type_Cd'
              AND aics.Invoice_Source_Type_Cd = CAST(tvp.Column_Value AS INT)
              AND tvp.Column_Type = cics.Code_Value
              AND cicst.Code_Value IN ( 'Data Feed', 'ETL' )
    )
                 )
          )
    GROUP BY icac.Invoice_Collection_Account_Config_Id;

    SELECT @Source_Type_Client = MAX(   CASE
                                            WHEN c.Code_Value = 'Client Primary Contact' THEN
                                                c.Code_Id
                                        END
                                    ),
           @Source_Type_Account = MAX(   CASE
                                             WHEN c.Code_Value = 'Account Primary Contact' THEN
                                                 c.Code_Id
                                         END
                                     ),
           @Source_Type_Vendor = MAX(   CASE
                                            WHEN c.Code_Value = 'Vendor Primary Contact' THEN
                                                c.Code_Id
                                        END
                                    )
    FROM dbo.Code c
        INNER JOIN dbo.Codeset cs
            ON c.Codeset_Id = cs.Codeset_Id
    WHERE cs.Codeset_Name = 'InvoiceSourceType'
          AND c.Code_Value IN ( 'Account Primary Contact', 'Client Primary Contact', 'Vendor Primary Contact' );


    SELECT @Contact_Level_Client = MAX(   CASE
                                              WHEN c.Code_Value = 'Client' THEN
                                                  c.Code_Id
                                          END
                                      ),
           @Contact_Level_Account = MAX(   CASE
                                               WHEN c.Code_Value = 'Account' THEN
                                                   c.Code_Id
                                           END
                                       ),
           @Contact_Level_Vendor = MAX(   CASE
                                              WHEN c.Code_Value = 'Vendor' THEN
                                                  c.Code_Id
                                          END
                                      )
    FROM dbo.Code c
        INNER JOIN dbo.Codeset cs
            ON c.Codeset_Id = cs.Codeset_Id
    WHERE cs.Codeset_Name = 'ContactLevel';




    INSERT INTO #Invoice_Collection_Accounts
    (
        Invoice_Collection_Account_Config_Id,
        Account_Id,
        Invoice_Collection_Service_End_Dt,
        Account_Vendor_Name,
        Account_Type,
        Account_Vendor_Id
    )
    SELECT icac.Invoice_Collection_Account_Config_Id,
           icac.Account_Id,
           icac.Invoice_Collection_Service_End_Dt,
           ucha.Account_Vendor_Name,
           ucha.Account_Type,
           ucha.Account_Vendor_Id
    FROM dbo.Invoice_Collection_Account_Config icac
        INNER JOIN Core.Client_Hier_Account ucha
            ON icac.Account_Id = ucha.Account_Id
    WHERE ucha.Account_Type = 'Utility'
          AND EXISTS
    (
        SELECT 1
        FROM #Invoice_Collection_Account_Config_Ids icac1
        WHERE icac.Invoice_Collection_Account_Config_Id = icac1.Invoice_Collection_Account_Config_Id
    )
    GROUP BY icac.Invoice_Collection_Account_Config_Id,
             icac.Account_Id,
             icac.Invoice_Collection_Service_End_Dt,
             ucha.Account_Vendor_Name,
             ucha.Account_Type,
             ucha.Account_Vendor_Id;

    INSERT INTO #Client_hier_Account_Details
    (
        Account_Vendor_Name,
        Account_Type,
        Account_Vendor_Id,
        Account_Id,
        Invoice_Collection_Account_Config_Id
    )
    SELECT scha.Account_Vendor_Name,
           scha.Account_Type,
           scha.Account_Vendor_Id,
           ucha1.Account_Id,
           icac.Invoice_Collection_Account_Config_Id
    FROM Core.Client_Hier_Account scha
        INNER JOIN Core.Client_Hier_Account ucha1
            ON ucha1.Meter_Id = scha.Meter_Id
               AND ucha1.Account_Type = 'Utility'
        INNER JOIN dbo.CONTRACT c
            ON c.CONTRACT_ID = scha.Supplier_Contract_ID
        INNER JOIN dbo.ENTITY ce
            ON c.CONTRACT_TYPE_ID = ce.ENTITY_ID
               AND ce.ENTITY_NAME = 'Supplier'
               AND scha.Account_Type = 'Supplier'
        INNER JOIN #Invoice_Collection_Accounts icac
            ON icac.Account_Id = ucha1.Account_Id
               AND (CASE
                        WHEN icac.Invoice_Collection_Service_End_Dt IS NULL
                             OR icac.Invoice_Collection_Service_End_Dt > GETDATE() THEN
                            GETDATE()
                        ELSE
                            icac.Invoice_Collection_Service_End_Dt
                    END
                   )
               BETWEEN scha.Supplier_Meter_Association_Date AND scha.Supplier_Meter_Disassociation_Date
    GROUP BY scha.Account_Vendor_Name,
             scha.Account_Type,
             scha.Account_Vendor_Id,
             ucha1.Account_Id,
             icac.Invoice_Collection_Account_Config_Id;


    INSERT INTO #Consolidated_Billing_Account_Id
    (
        Account_Id
    )
    SELECT asbv1.Account_Id
    FROM dbo.Account_Consolidated_Billing_Vendor asbv1
        INNER JOIN dbo.ENTITY e1
            ON asbv1.Invoice_Vendor_Type_Id = e1.ENTITY_ID
               AND e1.ENTITY_NAME = 'Supplier'
        LEFT OUTER JOIN dbo.VENDOR v1
            ON v1.VENDOR_ID = asbv1.Supplier_Vendor_Id
    WHERE EXISTS
    (
        SELECT 1
        FROM #Invoice_Collection_Accounts icac
        WHERE asbv1.Account_Id = icac.Account_Id
    )
    GROUP BY asbv1.Account_Id;


    INSERT INTO #Vendor_Account_Details
    (
        Account_Vendor_Name,
        Account_Type,
        Account_Vendor_Id,
        Account_Id,
        Invoice_Collection_Account_Config_Id
    )
    SELECT v.VENDOR_NAME,
           e.ENTITY_NAME,
           v.VENDOR_ID,
           asbv.Account_Id,
           icac.Invoice_Collection_Account_Config_Id
    FROM dbo.Account_Consolidated_Billing_Vendor asbv
        INNER JOIN dbo.ENTITY e
            ON asbv.Invoice_Vendor_Type_Id = e.ENTITY_ID
               AND e.ENTITY_NAME = 'Supplier'
        LEFT OUTER JOIN dbo.VENDOR v
            ON v.VENDOR_ID = asbv.Supplier_Vendor_Id
        INNER JOIN #Invoice_Collection_Accounts icac
            ON icac.Account_Id = asbv.Account_Id
               AND (CASE
                        WHEN icac.Invoice_Collection_Service_End_Dt IS NULL
                             OR asbv.Billing_End_Dt > GETDATE() THEN
                            GETDATE()
                        ELSE
                            icac.Invoice_Collection_Service_End_Dt
                    END
                   )
               BETWEEN asbv.Billing_Start_Dt AND ISNULL(asbv.Billing_End_Dt, '9999-12-31')
    GROUP BY v.VENDOR_NAME,
             e.ENTITY_NAME,
             v.VENDOR_ID,
             asbv.Account_Id,
             icac.Invoice_Collection_Account_Config_Id;


    INSERT INTO #Ic_Account_Details
    (
        Account_Vendor_Name,
        Account_Type,
        Account_Vendor_Id,
        Account_Id,
        Invoice_Collection_Account_Config_Id
    )
    SELECT icav.VENDOR_NAME,
           'Supplier',
           icav.VENDOR_ID,
           icac.Account_Id,
           aics.Invoice_Collection_Account_Config_Id
    FROM dbo.Invoice_Collection_Account_Contact icc
        INNER JOIN dbo.Account_Invoice_Collection_Source aics
            ON icc.Invoice_Collection_Account_Config_Id = aics.Invoice_Collection_Account_Config_Id
        INNER JOIN dbo.Contact_Info ci
            ON ci.Contact_Info_Id = icc.Contact_Info_Id
               AND
               (
                   (
                       @Source_Type_Client = aics.Invoice_Source_Type_Cd
                       AND @Contact_Level_Client = ci.Contact_Level_Cd
                   )
                   OR
                   (
                       @Source_Type_Account = aics.Invoice_Source_Type_Cd
                       AND @Contact_Level_Account = ci.Contact_Level_Cd
                   )
                   OR
                   (
                       @Source_Type_Vendor = aics.Invoice_Source_Type_Cd
                       AND @Contact_Level_Vendor = ci.Contact_Level_Cd
                   )
               )
        INNER JOIN dbo.Vendor_Contact_Map vcm
            ON ci.Contact_Info_Id = vcm.Contact_Info_Id
        INNER JOIN dbo.VENDOR icav
            ON icav.VENDOR_ID = vcm.VENDOR_ID
        INNER JOIN dbo.ENTITY ve
            ON icav.VENDOR_TYPE_ID = ve.ENTITY_ID
        INNER JOIN #Invoice_Collection_Accounts icac
            ON icc.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
               AND icc.Is_Primary = 1
    GROUP BY icav.VENDOR_NAME,
             icav.VENDOR_ID,
             icac.Account_Id,
             aics.Invoice_Collection_Account_Config_Id;


    INSERT INTO #Vendor_Dtls
    (
        Account_Vendor_Name,
        Account_Vendor_Type,
        Account_Vendor_Id,
        Invoice_Collection_Account_Config_Id
    )
    SELECT avd.Account_Vendor_Name,
           avd.Account_Type,
           avd.Account_Vendor_Id,
           avd.Invoice_Collection_Account_Config_Id
    FROM #Client_hier_Account_Details avd
    WHERE EXISTS
    (
        SELECT 1
        FROM #Consolidated_Billing_Account_Id cbai
        WHERE avd.Account_Id = cbai.Account_Id
    );



    INSERT INTO #Vendor_Dtls
    (
        Account_Vendor_Name,
        Account_Vendor_Type,
        Account_Vendor_Id,
        Invoice_Collection_Account_Config_Id
    )
    SELECT vad.Account_Vendor_Name,
           vad.Account_Type,
           vad.Account_Vendor_Id,
           vad.Invoice_Collection_Account_Config_Id
    FROM #Vendor_Account_Details vad
    WHERE NOT EXISTS
    (
        SELECT 1
        FROM #Vendor_Dtls vd
        WHERE vad.Invoice_Collection_Account_Config_Id = vd.Invoice_Collection_Account_Config_Id
    );



    INSERT INTO #Vendor_Dtls
    (
        Account_Vendor_Name,
        Account_Vendor_Type,
        Account_Vendor_Id,
        Invoice_Collection_Account_Config_Id
    )
    SELECT icd.Account_Vendor_Name,
           icd.Account_Type,
           icd.Account_Vendor_Id,
           icd.Invoice_Collection_Account_Config_Id
    FROM #Ic_Account_Details icd
    WHERE EXISTS
    (
        SELECT 1
        FROM #Consolidated_Billing_Account_Id cbai
        WHERE icd.Account_Id = cbai.Account_Id
    )
          AND NOT EXISTS
    (
        SELECT 1
        FROM #Vendor_Dtls vd
        WHERE icd.Invoice_Collection_Account_Config_Id = vd.Invoice_Collection_Account_Config_Id
    );



    INSERT INTO #Vendor_Dtls
    (
        Account_Vendor_Name,
        Account_Vendor_Type,
        Account_Vendor_Id,
        Invoice_Collection_Account_Config_Id
    )
    SELECT ica.Account_Vendor_Name,
           ica.Account_Type,
           ica.Account_Vendor_Id,
           ica.Invoice_Collection_Account_Config_Id
    FROM #Invoice_Collection_Accounts ica
    WHERE NOT EXISTS
    (
        SELECT 1
        FROM #Vendor_Dtls vd
        WHERE ica.Invoice_Collection_Account_Config_Id = vd.Invoice_Collection_Account_Config_Id
    );



    CREATE TABLE [#TempIssueComment]
    (
        [Invoice_Collection_Account_Config_Id] INT,
        [Invoice_Collection_Activity_Id] INT,
        Invoice_Collection_Issue_Event_Id INT,
        [Code_Value] VARCHAR(100),
        [Invoice_Collection_Issue_Log_Id] INT,
        [Issue_Event_Type_Cd] INT,
        [Event_Desc] [NVARCHAR](MAX)
    );

    CREATE NONCLUSTERED INDEX [idx_#TempIssueComment]
    ON [#TempIssueComment] (
                               Invoice_Collection_Issue_Event_Id,
                               Invoice_Collection_Issue_Log_Id
                           )
    INCLUDE (
                Event_Desc,
                Issue_Event_Type_Cd
            );

    INSERT INTO #TempIssueComment
    (
        Invoice_Collection_Account_Config_Id,
        Invoice_Collection_Activity_Id,
        Invoice_Collection_Issue_Event_Id,
        Code_Value,
        Invoice_Collection_Issue_Log_Id,
        --Issue_Event_Type_Cd,
        Event_Desc
    )
    SELECT MAX(icaci.Invoice_Collection_Account_Config_Id) Invoice_Collection_Account_Config_Id,
           MAX(icil.Invoice_Collection_Activity_Id) Invoice_Collection_Activity_Id,
           icie.Invoice_Collection_Issue_Event_Id,
           MAX(ec.Code_Value) Code_Value,
           MAX(icil.Invoice_Collection_Issue_Log_Id) Invoice_Collection_Issue_Log_Id,
         --  MAX(icie.Issue_Event_Type_Cd) Issue_Event_Type_Cd,
           MAX(icie.Event_Desc) AS Event_Desc
    FROM dbo.Invoice_Collection_Issue_Log icil
        INNER JOIN dbo.Invoice_Collection_Queue icq
            ON icq.Invoice_Collection_Queue_Id = icil.Invoice_Collection_Queue_Id
        INNER JOIN #Invoice_Collection_Account_Config_Ids icaci
            ON icaci.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
        INNER JOIN dbo.Invoice_Collection_Issue_Event icie
            ON icie.Invoice_Collection_Issue_Log_Id = icil.Invoice_Collection_Issue_Log_Id
        INNER JOIN Code ec
            ON icie.Issue_Event_Type_Cd = ec.Code_Id
    WHERE (
              icil.Invoice_Collection_Activity_Id = @Issue_Activity_Id
              OR @Issue_Activity_Id IS NULL
          )
          AND
          (
              (
                  @Start_Date IS NULL
                  AND @End_Date IS NULL
              )
              OR
              (
                  @Start_Date IS NOT NULL
                  AND @End_Date IS NULL
                  AND @Start_Date <= icq.Collection_Start_Dt
              )
              OR
              (
                  @Start_Date IS NULL
                  AND @End_Date IS NOT NULL
                  AND @End_Date >= icq.Collection_End_Dt
              )
              OR
              (
                  @Start_Date IS NOT NULL
                  AND @End_Date IS NOT NULL
                  AND
                  (
                      @Start_Date <= icq.Collection_Start_Dt
                      AND @End_Date >= icq.Collection_End_Dt
                  )
              )
          )
          --AND ec.Code_Value IN ( 'IC Internal Comments', 'IC External Comments' )
		  AND  icie.Issue_Event_Type_Cd IN (@Internal_Comment_Cd,@External_Comment_Cd)
    GROUP BY icie.Invoice_Collection_Issue_Event_Id;


    INSERT INTO [#ConsolidatedResults]
    (
        Client_Id,
        [Client_Name],
        [Site_Id],
        [Site_Name],
        [Country_Id],
        [Country_Name],
        [State_Id],
        [State_Name],
        [Commodity_Name],
        [Account_Type],
        [Account_Vendor_Name],
        [Account_Vendor_Id],
        [Account_Number],
        [Account_Id],
        [Collection_Start_Dt],
        [Collection_End_Dt],
        [Invoice_Collection_Issue_Log_Id],
        [Event_Desc],
        [Invoice_Collection_Activity_Id],
        [Event_Type],
        [Invoice_Collection_Issue_Event_Id],
        [Code_Value],
        [Invoice_Collection_Alternative_Account_Number],
        [Created_Ts],
        [Is_Blocker],
        [Invoice_Collection_Account_Config_Id],
        [Issue_Status_Cd]
    )
    SELECT icac.Client_Id,
           icac.Client_Name,
           icac.Site_Id,
           icac.Site_Name,
           icac.Country_Id,
           icac.Country_Name,
           icac.State_Id,
           icac.State_Name,
           icac.Commodity_Name,
           ISNULL(vd.Account_Vendor_Type, icac.Account_Type) Account_Type,
           ISNULL(vd.Account_Vendor_Name, icac.Account_Vendor_Name) Account_Vendor_Name,
           ISNULL(vd.Account_Vendor_Id, icac.Account_Vendor_Id) AS Account_Vendor_Id,
           icac.Account_Number,
           icac.Account_Id,
           MAX(CONVERT(VARCHAR(12), icil2.Collection_Start_Dt, 105)) AS Collection_Start_Dt,
           MAX(CONVERT(VARCHAR(12), icil2.Collection_End_Dt, 105)) AS Collection_End_Dt,
           icil2.Invoice_Collection_Issue_Log_Id,
           tlec.Event_Desc,
           tlec.Invoice_Collection_Activity_Id,
           tlec.Code_Value AS Event_Type,
           tlec.Invoice_Collection_Issue_Event_Id,
           tlec.Code_Value,
           icac.Invoice_Collection_Alternative_Account_Number,
           ical.Created_Ts Created_Ts,
           icil2.Is_Blocker,
           icac.Invoice_Collection_Account_Config_Id,
           MAX(Issue_Status_Cd) AS Issue_Status_Cd
    FROM #Invoice_Collection_Account_Config_Ids icac
        LEFT OUTER JOIN #Vendor_Dtls vd
            ON vd.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
        JOIN #TempIssueComment tlec
            ON icac.Invoice_Collection_Account_Config_Id = tlec.Invoice_Collection_Account_Config_Id
        JOIN dbo.Invoice_Collection_Activity ical
            ON tlec.[Invoice_Collection_Activity_Id] = ical.[Invoice_Collection_Activity_Id]
        JOIN dbo.Invoice_Collection_Issue_Log icil2
            ON icil2.Invoice_Collection_Issue_Log_Id = tlec.Invoice_Collection_Issue_Log_Id
        JOIN Code ieoc
            ON ieoc.Code_Id = icil2.Issue_Entity_Owner_Cd
        JOIN Code isc
            ON isc.Code_Id = icil2.Issue_Status_Cd
        JOIN Code itc
            ON itc.Code_Id = icil2.Invoice_Collection_Issue_Type_Cd
    WHERE (
              @Issue_Activity_Id IS NULL
              OR ical.Invoice_Collection_Activity_Id = @Issue_Activity_Id
          )
          AND
          (
              @Vendor_Type IS NULL
              OR ISNULL(vd.Account_Vendor_Type, icac.Account_Type) = @Vendor_Type
          )
          AND
          (
              @Vendor_Id IS NULL
              OR EXISTS
    (
        SELECT 1
        FROM @Filtered_Vendor_Id fvi
        WHERE fvi.Vendor_Id = ISNULL(vd.Account_Vendor_Id, icac.Account_Vendor_Id)
    )
          )
          AND
          (
              @Is_Blocker IS NULL
              OR (icil2.Is_Blocker = @Is_Blocker)
          )
          AND
          (
              @Issue_Status_Cd IS NULL
              OR (icil2.Issue_Status_Cd = @Issue_Status_Cd)
          )
          AND
          (
              @Issue_Entity_Owner_Cd IS NULL
              OR (icil2.Issue_Entity_Owner_Cd = @Issue_Entity_Owner_Cd)
          )
          AND
          (
              @TypeOfIssueCodeId IS NULL
              OR (icil2.Invoice_Collection_Issue_Type_Cd = @TypeOfIssueCodeId)
          )
          AND
          (
              @Vendor_Id IS NULL
              OR EXISTS
    (
        SELECT 1
        FROM @Filtered_Vendor_Id fvi
        WHERE fvi.Vendor_Id = ISNULL(vd.Account_Vendor_Id, icac.Account_Vendor_Id)
    )
          )
    GROUP BY icac.Client_Id,
             icac.Client_Name,
             icac.Site_Id,
             icac.Site_Name,
             icac.Country_Id,
             icac.Country_Name,
             icac.State_Id,
             icac.State_Name,
             icac.Commodity_Name,
             ISNULL(vd.Account_Vendor_Type, icac.Account_Type),
             ISNULL(vd.Account_Vendor_Name, icac.Account_Vendor_Name),
             ISNULL(vd.Account_Vendor_Id, icac.Account_Vendor_Id),
             icac.Account_Number,
             icac.Account_Id,
             icac.Invoice_Collection_Alternative_Account_Number,
             icil2.Invoice_Collection_Issue_Log_Id,
             tlec.Invoice_Collection_Issue_Event_Id,
             tlec.Event_Desc,
             tlec.Invoice_Collection_Activity_Id,
             tlec.Code_Value,
             ical.Created_Ts,
             icil2.Is_Blocker,
             icac.Invoice_Collection_Account_Config_Id;


    INSERT INTO [#ChasePeriods]
    (
        [Code_Value],
        [Issue_Status_Cd],
        [Invoice_Collection_Activity_Id],
        [Invoice_Collection_Account_Config_Id],
        [Period_to_chase]
    )
    SELECT mr.Code_Value,
           mr.Issue_Status_Cd,
           mr.Invoice_Collection_Activity_Id,
           mr.Invoice_Collection_Account_Config_Id,
           CASE
               WHEN LEN(chase.Period_to_chase) > 0 THEN
                   LEFT(chase.Period_to_chase, LEN(chase.Period_to_chase) - 1)
               ELSE
                   chase.Period_to_chase
           END AS Period_to_chase
    FROM #ConsolidatedResults mr
        CROSS APPLY
    (
        SELECT CONVERT(VARCHAR(12), mr2.Collection_Start_Dt, 105) + ' , '
               + CONVERT(VARCHAR(12), mr2.Collection_End_Dt, 105) + ' ~ '
               + CAST(mr2.Invoice_Collection_Issue_Log_Id AS VARCHAR(12)) + CAST(CASE
                                                                                     WHEN (mr2.Is_Blocker = 1) THEN
                                                                                         'Y'
                                                                                     WHEN (mr2.Is_Blocker = 0) THEN
                                                                                         'N'
                                                                                     ELSE
                                                                                         'N'
                                                                                 END AS VARCHAR(1)) + '#'
        FROM #ConsolidatedResults mr2
        WHERE mr.Invoice_Collection_Account_Config_Id = mr2.Invoice_Collection_Account_Config_Id
              AND mr.Invoice_Collection_Activity_Id = mr2.Invoice_Collection_Activity_Id
              AND mr.Code_Value = mr2.Code_Value
              AND mr.Issue_Status_Cd = mr2.Issue_Status_Cd
        GROUP BY mr2.Collection_Start_Dt,
                 mr2.Collection_End_Dt,
                 mr2.Invoice_Collection_Issue_Log_Id,
                 mr2.Is_Blocker
        FOR XML PATH('')
    ) chase(Period_to_chase)
    GROUP BY mr.Invoice_Collection_Activity_Id,
             mr.Invoice_Collection_Account_Config_Id,
             mr.Code_Value,
             mr.Issue_Status_Cd,
             CASE
                 WHEN LEN(chase.Period_to_chase) > 0 THEN
                     LEFT(chase.Period_to_chase, LEN(chase.Period_to_chase) - 1)
                 ELSE
                     chase.Period_to_chase
             END;

    INSERT INTO [#EventIds]
    (
        [Code_Value],
        [Issue_Status_Cd],
        [Invoice_Collection_Activity_Id],
        [Invoice_Collection_Account_Config_Id],
        [Event_Desc],
        [Invoice_Collection_Issue_Event_Id]
    )
    SELECT mr.Code_Value,
           mr.Issue_Status_Cd,
           mr.Invoice_Collection_Activity_Id,
           mr.Invoice_Collection_Account_Config_Id,
           mr.Event_Desc,
           CASE
               WHEN LEN(Event_Ids.Invoice_Collection_Issue_Event_Ids) > 0 THEN
                   LEFT(Event_Ids.Invoice_Collection_Issue_Event_Ids, LEN(Event_Ids.Invoice_Collection_Issue_Event_Ids)
                                                                      - 1)
               ELSE
                   Event_Ids.Invoice_Collection_Issue_Event_Ids
           END AS Invoice_Collection_Issue_Event_Id
    FROM #ConsolidatedResults mr
        CROSS APPLY
    (
        SELECT CAST(mr2.Invoice_Collection_Issue_Event_Id AS VARCHAR(MAX)) + ','
        FROM #ConsolidatedResults mr2
        WHERE mr.Invoice_Collection_Account_Config_Id = mr2.Invoice_Collection_Account_Config_Id
              AND mr.Invoice_Collection_Activity_Id = mr2.Invoice_Collection_Activity_Id
              AND mr.Code_Value = mr2.Code_Value
              AND mr.Event_Desc = mr2.Event_Desc
              AND mr.Issue_Status_Cd = mr2.Issue_Status_Cd
        GROUP BY mr2.Invoice_Collection_Issue_Event_Id
        FOR XML PATH('')
    ) Event_Ids(Invoice_Collection_Issue_Event_Ids)
    GROUP BY mr.Invoice_Collection_Activity_Id,
             mr.Invoice_Collection_Account_Config_Id,
             mr.Code_Value,
             mr.Issue_Status_Cd,
             CASE
                 WHEN LEN(Event_Ids.Invoice_Collection_Issue_Event_Ids) > 0 THEN
                     LEFT(Event_Ids.Invoice_Collection_Issue_Event_Ids, LEN(Event_Ids.Invoice_Collection_Issue_Event_Ids)
                                                                        - 1)
                 ELSE
                     Event_Ids.Invoice_Collection_Issue_Event_Ids
             END,
             mr.Event_Desc;


    SELECT mr.Client_Id,
           mr.Client_Name,
           mr.Site_Id,
           mr.Site_Name,
           mr.Country_Id,
           mr.Country_Name,
           mr.State_Id,
           mr.State_Name,
           CASE
               WHEN LEN(Commodities.Commodity_Name) > 0 THEN
                   LEFT(Commodities.Commodity_Name, LEN(Commodities.Commodity_Name) - 1)
               ELSE
                   Commodities.Commodity_Name
           END AS Commodity_Name,
           mr.Account_Type,
           mr.Account_Vendor_Name,
           mr.Account_Vendor_Id,
           mr.Account_Number,
           mr.Account_Id,
           cp.Period_to_chase,
           mr.Event_Desc,
           ROW_NUMBER() OVER (ORDER BY mr.Account_Number) AS Row_Num,
           COUNT(1) OVER () Total_Count,
           mr.Invoice_Collection_Activity_Id,
           mr.Event_Type,
           e.Invoice_Collection_Issue_Event_Id,
           mr.Invoice_Collection_Alternative_Account_Number,
           mr.Created_Ts Created_Ts,
           '' Issue_Entity_Owner,
           '' Issue_Status,
           '' Issue_Type,
           mr.Invoice_Collection_Account_Config_Id,
           mr.Issue_Status_Cd,
           mr.Is_Blocker
    FROM #ConsolidatedResults mr
        JOIN #ChasePeriods cp
            ON mr.Invoice_Collection_Activity_Id = cp.Invoice_Collection_Activity_Id
               AND mr.Invoice_Collection_Account_Config_Id = cp.Invoice_Collection_Account_Config_Id
               AND mr.Code_Value = cp.Code_Value
               AND mr.Issue_Status_Cd = cp.Issue_Status_Cd
        JOIN #EventIds e
            ON mr.Invoice_Collection_Activity_Id = e.Invoice_Collection_Activity_Id
               AND mr.Invoice_Collection_Account_Config_Id = e.Invoice_Collection_Account_Config_Id
               AND mr.Code_Value = e.Code_Value
               AND mr.Issue_Status_Cd = e.Issue_Status_Cd
               AND mr.Event_Desc = e.Event_Desc
        CROSS APPLY
    (
        SELECT mr1.Commodity_Name + ','
        FROM #ConsolidatedResults mr1
        WHERE mr1.Invoice_Collection_Account_Config_Id = mr.Invoice_Collection_Account_Config_Id
              AND mr.Account_Id = mr1.Account_Id
        GROUP BY mr1.Commodity_Name
        FOR XML PATH('')
    ) Commodities(Commodity_Name)
    GROUP BY mr.Client_Id,
             mr.Client_Name,
             mr.Site_Id,
             mr.Site_Name,
             mr.Country_Id,
             mr.Country_Name,
             mr.State_Id,
             mr.State_Name,
             CASE
                 WHEN LEN(Commodities.Commodity_Name) > 0 THEN
                     LEFT(Commodities.Commodity_Name, LEN(Commodities.Commodity_Name) - 1)
                 ELSE
                     Commodities.Commodity_Name
             END,
             mr.Account_Type,
             mr.Account_Vendor_Name,
             mr.Account_Vendor_Id,
             mr.Account_Number,
             mr.Account_Id,
             mr.Invoice_Collection_Alternative_Account_Number,
             cp.Period_to_chase,
             e.Invoice_Collection_Issue_Event_Id,
             mr.Event_Desc,
             mr.Invoice_Collection_Activity_Id,
             mr.Event_Type,
             mr.Created_Ts,
             mr.Invoice_Collection_Account_Config_Id,
             mr.Issue_Status_Cd,
             mr.Is_Blocker;


    DROP TABLE #Client_hier_Account_Details,
               #Consolidated_Billing_Account_Id,
               #Ic_Account_Details,
               #Invoice_Collection_Accounts,
               #Invoice_Collection_Account_Config_Ids,
               #Vendor_Account_Details,
               #Vendor_Dtls,
               #TempIssueComment,
               #ConsolidatedResults,
               #ChasePeriods,
               #EventIds;


END;












GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Issue_Comments_Sel] TO [CBMSApplication]
GO
