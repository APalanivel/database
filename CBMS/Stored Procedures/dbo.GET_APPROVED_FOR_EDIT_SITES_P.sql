SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[GET_APPROVED_FOR_EDIT_SITES_P]
@userId varchar(10),
@sessionId varchar(20),
@clientId int
AS
	 set nocount on
select 
	approval.site_id
from 
	rm_manage_forecast_approval approval,
     	site sit,
	
	client cli
where 	
	approval.site_id = sit.site_id
	and sit.client_id = cli.client_id
	and cli.client_id = @clientId
GO
GRANT EXECUTE ON  [dbo].[GET_APPROVED_FOR_EDIT_SITES_P] TO [CBMSApplication]
GO
