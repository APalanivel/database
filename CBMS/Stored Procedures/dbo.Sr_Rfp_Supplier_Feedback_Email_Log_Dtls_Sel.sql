SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Rfp_Supplier_Feedback_Email_Log_Dtls_Sel]
           
DESCRIPTION:             
			To get Shortlist Supplier by suppliers 
			
INPUT PARAMETERS:            
	Name			DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Supplier_Id	INT
    @Contact_Id		INT
    @Sr_Rfp_Id		INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
            	
	EXEC dbo.Sr_Rfp_Supplier_Feedback_Email_Log_Dtls_Sel 13155,824,118

	EXEC dbo.Sr_Rfp_Supplier_Feedback_Email_Log_Dtls_Sel 13320,669,245
	
	EXEC dbo.Sr_Rfp_Supplier_Feedback_Email_Log_Dtls_Sel 13469,11038,1131
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	NR			Narayana Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	NR			2016-07-19	Created For MAINT-4186.
	
******/

CREATE PROCEDURE [dbo].[Sr_Rfp_Supplier_Feedback_Email_Log_Dtls_Sel]
      ( 
       @Sr_Rfp_Id INT
      ,@Supplier_Id INT
      ,@Contact_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      SELECT
            ui.FIRST_NAME
           ,ui.EMAIL_ADDRESS
           ,srel.GENERATION_DATE
           ,srel.SR_RFP_EMAIL_LOG_ID
           ,ui.USER_INFO_ID
      FROM
            dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP scvm
            INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                  ON scvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
            INNER JOIN dbo.USER_INFO ui
                  ON ssci.USER_INFO_ID = ui.USER_INFO_ID
            INNER JOIN dbo.SR_RFP_EMAIL_LOG srel
                  ON scvm.SR_RFP_ID = srel.SR_RFP_ID
                     AND srel.TO_EMAIL_ADDRESS = ui.EMAIL_ADDRESS
            INNER JOIN dbo.ENTITY typ
                  ON srel.EMAIL_TYPE_ID = typ.ENTITY_ID
      WHERE
            scvm.SR_RFP_ID = @Sr_Rfp_Id
            AND scvm.VENDOR_ID = @Supplier_Id
            AND scvm.SR_SUPPLIER_CONTACT_INFO_ID = @Contact_Id
            AND typ.ENTITY_TYPE = '1033'
            AND typ.ENTITY_NAME IN ( 'Email-036-RFPSupplierFeedback_SA_SupplierContact' )
      GROUP BY
            ui.FIRST_NAME
           ,ui.EMAIL_ADDRESS
           ,srel.GENERATION_DATE
           ,srel.SR_RFP_EMAIL_LOG_ID
           ,ui.USER_INFO_ID
      ORDER BY
            srel.GENERATION_DATE
      
END;

;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Supplier_Feedback_Email_Log_Dtls_Sel] TO [CBMSApplication]
GO
