
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: dbo.[Site_History_Sel_By_Site_Id]  
     
DESCRIPTION:

	To Select all the Site history information for the given Site id.
	Instead of calling individual procedures application will call this procedure 
	to get all the informations required for the page at once.

INPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
@Site_Id		INT						
@StartIndex		INT			1
@EndIndex		INT			2147483647

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Site_History_Sel_By_Site_Id  281,1,10

	EXEC dbo.Site_History_Sel_By_Site_Id  411, 1,25
	
	EXEC dbo.Site_History_Sel_By_Site_Id  33597
	
	EXEC dbo.Site_History_Sel_By_Site_Id  39907
	
AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------          
PNR			PANDARINATH
HG			Harihara Suthan G
RR			Raghu Reddy

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			29-JUN-10	CREATED
HG			08/03/2010	Users_Sel_By_Site_Id procedure renamed to User_Sel_By_Site_Id and the input parameter added for @Is_History column added as 0.
RR			2011-12-09	Part of delete tool enhancement, procedure used to select client attributes "dbo.Client_Attributes_Sel_By_Site_Id" removed
						as client attribute tables moved to DVDEHub
CPE			2012-04-06	Removed the call to Sustainability_Sources_Sel_By_Site_Id as core.Client_Commodity_Detail is moved to Hub
HG			2012-05-24	ADLDT- Unused parameter @MyAccountid was removed from CbmsSite_Get applied that change here.
*/

CREATE PROCEDURE dbo.Site_History_Sel_By_Site_Id
      ( 
       @Site_Id INT
      ,@StartIndex INT = 1
      ,@EndIndex INT = 2147483647 )
AS 
BEGIN

      SET NOCOUNT ON ;
	
      EXEC dbo.cbmsSite_Get 
           @Site_Id = @Site_Id

      EXEC dbo.GET_SITE_UTILITY_ACCOUNTS_P 
            @Site_Id
      EXEC dbo.cbmsAddress_Get 
            -1
           ,@Site_Id
      EXEC dbo.Cost_Usage_Sel_By_Site_Id 
            @Site_Id
           ,@StartIndex
           ,@EndIndex
      EXEC dbo.Sso_Savings_Detail_Sel_By_Owner_Id_Type 
            @Site_Id
           ,'Site'
      EXEC dbo.Sso_Document_Sel_By_Owner_Id_Type 
            @Site_Id
           ,'Site'
      EXEC dbo.Sso_Project_Sel_By_Owner_Id_Type 
            @Site_Id
           ,'Site'
      EXEC dbo.RM_Onboard_Hedge_Setup_Existence_Status_Sel_For_Site 
            @Site_Id
      EXEC dbo.RM_Deal_Ticket_Volume_Sel_By_Site_Id 
            @Site_Id
      EXEC dbo.Manual_Site_Group_Sel_By_Site_Id 
            @Site_Id
      EXEC dbo.User_Sel_By_Site_Id 
            @Site_Id
           ,0
	
END


;

;
GO



GRANT EXECUTE ON  [dbo].[Site_History_Sel_By_Site_Id] TO [CBMSApplication]
GO
