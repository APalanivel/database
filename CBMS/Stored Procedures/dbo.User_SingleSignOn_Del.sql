
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:			dbo.User_SingleSignOn_Del
DESCRIPTION:	Delete mappings from the Single Signon User Map table

INPUT PARAMETERS:
	Name				DataType			Default	Description
-----------------------------------------------------------------
	@user_info_id		INT
	@client_id			INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
EXEC dbo.User_SingleSignOn_Del


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	KH			Kevin Horton
	
MODIFICATIONS

	Initials	Date			Modification
------------------------------------------------------------
	KH			05-Nov-2012		Created
	KH			18-Aug-2014		Added client_id param so only client specific mappings are deleted
	KVK			Vinay K			For internal User CLient_Id will not be passed so defaulting to zero
								Note: Basically this procedure shouldnt call incase of Internal user but some reason it is calling in App
			
******/
CREATE PROCEDURE [dbo].[User_SingleSignOn_Del]
	 (
		@user_info_id		INT
		,@client_id			INT = 0
	)
	
AS
BEGIN

	SET NOCOUNT ON;
	
	DELETE 
	FROM 
		dbo.Single_Signon_User_Map
	WHERE 
		Internal_User_ID = @user_info_id
		AND Client_ID = @client_id 
	
END
;
GO


GRANT EXECUTE ON  [dbo].[User_SingleSignOn_Del] TO [CBMSApplication]
GO
