SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:        
        
 dbo.Bucket_Account_Interval_Dtl_Del_By_Account_Date_Range_And_Invoice_Service        
        
DESCRIPTION:        
        
 This procedure is used to Delete the records from dbo.Bucket_Account_Interval_Dtl table for given input.      
  -- Deletes all the invoice data for the given account and date range      
  -- Deletes all the manual data for the given invoive service month and commodity      
         
        
INPUT PARAMETERS:        
   Name     DataType  Default     Description         
-------------------------------------------------------------------------------------        
   @Account_Id   INT       
   @Service_Start_Dt    DATE        
   @Service_End_Dt      DATE      
   @CU_Invoice_Id  INT      
      
OUTPUT PARAMETERS:        
 Name       DataType     Default     Description                    
-------------------------------------------------------------------------------------        
        
USAGE EXAMPLES:        
-------------------------------------------------------------------------------------        
        
 SELECT * FROM Bucket_Account_Interval_Dtl        
BEGIN TRAN       
 EXEC dbo.Bucket_Account_Interval_Dtl_Del 100001, 50001, 100991, '1/1/2013', '1/5/2013',100350        
ROLLBACK TRAN               
      
AUTHOR INITIALS:      
 Initials  Name      
-------------------------------------------------------------------------------------      
 HG    Harihara Suthan G      
      
MODIFICATIONS      
 Initials	Date		Modification      
-------------------------------------------------------------------------------------      
 HG			2013-03-14	Created
 HG			2013-07-01	Added Client_Hier_Id join to the BAID to improve the performance of delete
******/
CREATE PROCEDURE dbo.Bucket_Account_Interval_Dtl_Del_By_Account_Date_Range_And_Invoice_Service
      (
       @Account_Id INT
      ,@Service_Start_Dt DATE
      ,@Service_End_Dt DATE
      ,@CU_Invoice_Id INT )
AS
BEGIN

      SET NOCOUNT ON;

      DECLARE @Invoice_Service_Month_Commodity TABLE
            ( 
             Service_Month DATE
            ,Commodity_Id INT
            ,PRIMARY KEY CLUSTERED ( Service_Month, Commodity_Id ) )      
      DECLARE @Client_Hier_Account TABLE
            ( 
             Client_Hier_Id INT
            ,Account_Id INT
            ,PRIMARY KEY CLUSTERED ( Client_Hier_Id, Account_Id ) )	
      
      DECLARE
            @Invoice_Data_Source_Cd INT
           ,@Cbms_Data_Source_Cd INT
           ,@DE_Data_Source_Cd INT      
      
      INSERT      INTO @Client_Hier_Account
                  ( 
                   Client_Hier_Id
                  ,Account_Id )
                  SELECT
                        Client_Hier_Id
                       ,Account_Id
                  FROM
                        core.Client_Hier_Account
                  WHERE
                        Account_Id = @Account_Id
                  GROUP BY
                        Client_Hier_Id
                       ,Account_Id

      INSERT      INTO @Invoice_Service_Month_Commodity
                  ( 
                   Service_Month
                  ,Commodity_Id )
                  SELECT
                        sm.Service_Month
                       ,cid.Commodity_Type_Id
                  FROM
                        dbo.CU_INVOICE_SERVICE_MONTH sm
                        INNER JOIN dbo.CU_INVOICE_DETERMINANT cid
                              ON cid.CU_INVOICE_ID = sm.CU_INVOICE_ID
                        INNER JOIN dbo.CU_INVOICE_DETERMINANT_ACCOUNT cida
                              ON cida.CU_INVOICE_DETERMINANT_ID = cid.CU_INVOICE_DETERMINANT_ID
                                 AND cida.ACCOUNT_ID = sm.Account_ID
                  WHERE
                        sm.CU_INVOICE_ID = @CU_Invoice_Id
                        AND sm.Account_ID = @Account_Id
                        AND sm.SERVICE_MONTH IS NOT NULL
                  GROUP BY
                        sm.Service_Month
                       ,cid.Commodity_Type_Id      
      
      INSERT      INTO @Invoice_Service_Month_Commodity
                  ( 
                   Service_Month
                  ,Commodity_Id )
                  SELECT
                        sm.Service_Month
                       ,cic.Commodity_Type_Id
                  FROM
                        dbo.CU_INVOICE_SERVICE_MONTH sm
                        INNER JOIN dbo.CU_INVOICE_CHARGE cic
                              ON cic.CU_INVOICE_ID = sm.CU_INVOICE_ID
                        INNER JOIN dbo.CU_INVOICE_CHARGE_ACCOUNT cica
                              ON cica.CU_INVOICE_CHARGE_ID = cic.CU_INVOICE_CHARGE_ID
                                 AND cica.ACCOUNT_ID = sm.Account_ID
                  WHERE
                        sm.CU_INVOICE_ID = @CU_Invoice_Id
                        AND sm.Account_ID = @Account_Id
                        AND sm.SERVICE_MONTH IS NOT NULL
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          @Invoice_Service_Month_Commodity mc
                                         WHERE
                                          mc.Service_Month = sm.SERVICE_MONTH
                                          AND mc.Commodity_Id = cic.COMMODITY_TYPE_ID )
                  GROUP BY
                        sm.Service_Month
                       ,cic.Commodity_Type_Id      
      
      SELECT
            @Cbms_Data_Source_Cd = max(case WHEN cd.Code_Value = 'Cbms' THEN cd.Code_Id
                                       END)
           ,@DE_Data_Source_Cd = max(case WHEN cd.Code_Value = 'DE' THEN cd.Code_Id
                                     END)
           ,@Invoice_Data_Source_Cd = max(case WHEN cd.Code_Value = 'Invoice' THEN cd.Code_Id
                                          END)
      FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code cd
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Std_Column_Name = 'Data_Source_Cd'      
        
 --- Delete the invoice data for the given date range      
      DELETE
            baid
      FROM
            dbo.Bucket_Account_Interval_Dtl baid
            INNER JOIN @Client_Hier_Account cha
                  ON cha.Client_Hier_Id = baid.Client_Hier_Id
                     AND cha.Account_Id = baid.Account_Id
      WHERE
            baid.Service_Start_Dt >= @Service_Start_Dt
            AND baid.Service_End_Dt <= @Service_End_Dt
            AND baid.Data_Source_Cd = @Invoice_Data_Source_Cd

    --- Delete the manual data based on the given invoice service month and commodity
      DELETE
            ba
      FROM
            dbo.Bucket_Account_Interval_Dtl ba
            INNER JOIN @Client_Hier_Account cha
                  ON cha.Client_Hier_Id = ba.Client_Hier_Id
                     AND cha.Account_Id = ba.Account_Id
            INNER JOIN dbo.Bucket_Master bm
                  ON bm.Bucket_Master_Id = ba.Bucket_Master_Id
            INNER JOIN @Invoice_Service_Month_Commodity smc
                  ON smc.Commodity_Id = bm.Commodity_Id
            INNER JOIN meta.Date_Dim dd
                  ON dd.Date_D = smc.Service_Month
                     AND dd.First_Day_Of_Month_D = ba.Service_Start_Dt
                     AND dd.Last_Day_Of_Month_D = ba.Service_End_Dt
      WHERE
            ba.Data_Source_Cd IN ( @Cbms_Data_Source_Cd, @DE_Data_Source_Cd )
            AND ba.Account_Id = @Account_id

END
;
GO
GRANT EXECUTE ON  [dbo].[Bucket_Account_Interval_Dtl_Del_By_Account_Date_Range_And_Invoice_Service] TO [CBMSApplication]
GO
