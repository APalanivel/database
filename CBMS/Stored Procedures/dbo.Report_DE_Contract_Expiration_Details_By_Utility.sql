
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
NAME:                
                 
 dbo.[Report_DE_Contract_Expiration_Details_By_Utility]                
                 
DESCRIPTION:                
 This Procedure is to get all Contract Expiration Details.                
                 
INPUT PARAMETERS:                
Name     DataType  Default Description                
-------------------------------------------------------------------------------------------                
 @Client_Id_List  VARCHAR(MAX)                
 @Commodity_Id_List  VARCHAR(MAX)                
 @Region_Id_List  VARCHAR(MAX)                
 @State_id_List   VARCHAR(MAX)                
 @Begin_date   DATETIME                
 @End_date    DATETIME                
 @Contract_Name VARCHAR(200)        
 @Utility_Id_List VARCHAR(MAX)       
 @Account_Service_lvl VARCHAR(25)          
                 
OUTPUT PARAMETERS:                
Name   DataType  Default Description                
------------------------------------------------------------                
                
USAGE EXAMPLES:                
-------------------------------------------------------------                
                
Exec dbo.[Report_DE_Contract_Expiration_Details_By_Utility] 235,'290,291','2,5,4,3','6,7,8,9','01/01/2008','12/01/2010','Supplier','1,2,3,4,5'            
                   
                
AUTHOR INITIALS:                
Initials Name                
------------------------------------------------------------                
AKR   Ashok Kumar                
                
MODIFICATIONS                
Initials Date  Modification                
------------------------------------------------------------                
 AKR        2013-10-18 cloned from Report_Contract_Expiration_Details        
 AKR        2014-06-05  Modified to remove CHA.Supplier_Meter_Disassociation_Date >= CHA.Supplier_Account_End_Dt  condition and    
                    moved Account service level filter to Report level, and modified to get all contracnt end dates.                
 AKR        2014-07-23 Modified the code to show the  data even if is no Sourcing Analyst associated to a Utility            
 */                
CREATE PROCEDURE [dbo].[Report_DE_Contract_Expiration_Details_By_Utility]  
      (   
       @Client_Id_List VARCHAR(MAX)  
      ,@Commodity_Id_List VARCHAR(MAX)  
      ,@Region_Id_List VARCHAR(MAX)  
      ,@State_id_List VARCHAR(MAX)  
      ,@Begin_date DATETIME  
      ,@End_date DATETIME  
      ,@Contract_Name VARCHAR(200)  
      ,@Utility_Id_List VARCHAR(MAX)  
      ,@Account_Service_lvl VARCHAR(25) )  
AS   
BEGIN                
                         
      SET NOCOUNT ON                               
                
      DECLARE  
            @Contract_Classification_id INT  
           ,@Contract_Supplier_id INT  
           ,@Contract_Utility_id INT                
                
      DECLARE @SQL VARCHAR(MAX)                
      DECLARE @SQL_ext VARCHAR(MAX)                
                
      SELECT  
            @Contract_Supplier_id = a.ENTITY_ID  
      FROM  
            dbo.Entity a  
      WHERE  
            a.ENTITY_Name = 'Supplier'  
            AND a.Entity_Description = 'Contract Type'                            
                               
      SELECT  
            @Contract_Utility_id = a.ENTITY_ID  
      FROM  
            dbo.Entity a  
      WHERE  
            a.ENTITY_Name = 'Utility'  
            AND a.Entity_Description = 'Contract Type'                        
                
      SELECT  
            @Contract_Classification_id = a.Entity_id  
      FROM  
            dbo.Entity a  
      WHERE  
            a.Entity_Name = 'Pipeline - Transportation'  
            AND a.Entity_Description = 'Contract Classification'                
                
                                          
      SET @SQL = ';                              
    WITH    maxcontract_cte                            
    AS ( SELECT                             
                        samm.meter_id [MeterID]                            
                      , contract_end_date [MaxContractExp]                            
   , c.CONTRACT_TYPE_ID [ContractType]                            
                      , c.Contract_Classification_Cd [Class]                         
                       FROM                         
                        dbo.CONTRACT c                            
                        JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm                            
                            ON samm.Contract_ID = c.CONTRACT_ID                       
                        JOIN dbo.ACCOUNT a                            
                            ON a.ACCOUNT_ID = samm.ACCOUNT_ID '                       
                                                  
      IF @Contract_Name <> 'Supplier,Utility'   
            BEGIN                                            
                  SET @SQL = @SQL + ' WHERE                            
        (     contract_end_date BETWEEN ' + '''' + CONVERT(VARCHAR(10), @Begin_date, 101) + ''' AND ' + '''' + CONVERT(VARCHAR(10), @End_date, 101) + '''                            
           AND c.CONTRACT_TYPE_ID = CASE WHEN ''' + @Contract_Name + '''= ''Utility'' THEN ' + CAST(@Contract_Utility_id AS VARCHAR) + ' ELSE ' + CAST(@Contract_Supplier_id AS VARCHAR) + ' END ' + ' AND c.COMMODITY_TYPE_ID in (' + CAST(@Commodity_Id_List 
AS VARCHAR) + ')' + ' AND samm.IS_HISTORY != 1 '                            
                  IF @Contract_Name = 'Utility'   
                        SET @SQL = @SQL + ' AND c.Contract_Classification_Cd = ' + CAST(@Contract_Classification_id AS VARCHAR)                       
            END                      
      ELSE   
            BEGIN                      
                         
                  SET @SQL = @SQL + ' WHERE                            
        (    contract_end_date BETWEEN ' + '''' + CONVERT(VARCHAR(10), @Begin_date, 101) + ''' AND ' + '''' + CONVERT(VARCHAR(10), @End_date, 101) + '''                          
           AND c.CONTRACT_TYPE_ID = ' + CAST(@Contract_Supplier_id AS VARCHAR) + ' AND c.COMMODITY_TYPE_ID in (' + CAST(@Commodity_Id_List AS VARCHAR) + ')' + ' AND samm.IS_HISTORY != 1 )                      
     OR                      
     ( ( samm.METER_DISASSOCIATION_DATE >= a.Supplier_Account_End_Dt                      
                       OR samm.METER_DISASSOCIATION_DATE IS NULL )                      
                     AND c.CONTRACT_TYPE_ID =' + CAST(@Contract_Utility_id AS VARCHAR) + '                      
                     AND c.COMMODITY_TYPE_ID in (' + CAST(@Commodity_Id_List AS VARCHAR) + ')' + ' AND samm.IS_HISTORY != 1                       
     AND c.Contract_Classification_Cd = ' + CAST(@Contract_Classification_id AS VARCHAR)                       
                         
                         
            END                      
      SET @SQL = @SQL + ' )                             
    GROUP BY                              
   samm.METER_ID                              
    , c.CONTRACT_TYPE_ID                              
    , c.Contract_Classification_Cd         
    ,contract_end_date                           
  )                   
                    
  SELECT * INTO #Temp_Account_Details                  
  FROM                  
  (                  
   SELECT                               
    c.Ed_contract_number [Contract Number]                              
     , ch.Client_Name [Client]                        
     ,ch.Client_Id                  
     ,ch.Site_Id                  
     ,s.Site_Reference_Number              
     ,ca.Account_Id                        
     , ch.Sitegroup_Name [Division]                              
     , ch.Site_name [Site]                              
     , ch.City                              
     , ca.Account_Number [Account Number]                              
     , ch.state_Name [State]                              
     , ch.Region_Name [Region]                              
     , com.Commodity_Name [Commodity]                              
     , ctyp.ENTITY_NAME [ContractType]                        
     , cd.Code_Value [Contract Classification Type]                              
     , v.VENDOR_NAME [Vendor]                      
     , ca.Account_Vendor_Name [Utility]                         
     ,ca.Account_Vendor_Id                  
     ,Com.Commodity_Id                       
     , c.CONTRACT_START_DATE [Contract Start Date]                              
     , c.CONTRACT_end_DATE [Contract End Date]                              
     , c.CONTRACT_END_DATE - ( NOTIFICATION_DAYS ) [Contract Notification Date]                      
     ,c.CONTRACT_PRICING_SUMMARY          
  FROM                              
    dbo.CONTRACT c                     
    JOIN maxcontract_cte mc                              
     ON mc.Class = c.Contract_Classification_Cd                              
        AND mc.ContractType = c.CONTRACT_TYPE_ID                              
        AND mc.MaxContractExp = c.CONTRACT_END_DATE                              
    JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm                              
     ON samm.METER_ID = mc.MeterID                              
        AND samm.Contract_ID = c.CONTRACT_ID                              
    JOIN Core.Client_Hier_Account ca                              
     ON ca.Meter_Id = mc.MeterID                              
        AND ca.Account_type = ''Utility''              
    JOIN Core.Client_Hier ch                              
     ON ch.Client_Hier_Id = ca.Client_Hier_Id                
     inner join dbo.site s on s.site_id = ch.site_id                            
    JOIN dbo.ENTITY ctyp                              
     ON ctyp.ENTITY_ID = c.contract_type_id                              
    JOIN dbo.Code cd                              
     ON cd.Code_Id = c.Contract_Classification_Cd                              
    JOIN dbo.Commodity com                              
     ON com.Commodity_Id = ca.Commodity_Id                              
        AND com.Commodity_Id = c.COMMODITY_TYPE_ID                              
    JOIN dbo.ACCOUNT a                              
     ON a.ACCOUNT_ID = samm.ACCOUNT_ID                              
    JOIN dbo.VENDOR v                              
     ON v.VENDOR_ID = a.VENDOR_ID                                 
   WHERE                              
     ch.Client_Id in (' + @Client_Id_List + ') AND ch.State_id in (' + @State_id_List + ')                              
      AND ch.Region_id in (' + @Region_Id_List + ')               
      AND ca.Account_Vendor_Id IN (' + @Utility_Id_List + ')                             
      AND ca.Account_Service_level_Cd IN (' + @Account_Service_lvl + ')                              
      AND ch.Site_Id != 0                              
    AND ch.Site_Not_Managed != 1                   
    GROUP BY                  
    c.Ed_contract_number                               
     , ch.Client_Name                               
     , ch.Sitegroup_Name                               
     , ch.Site_name                    
     , ch.state_Name             
     ,s.Site_Reference_Number                        
     , ch.City                              
     , ca.Account_Number                               
     , ch.Region_Name                               
     , com.Commodity_Name                               
     , ctyp.ENTITY_NAME                               
     , cd.Code_Value                               
     , v.VENDOR_NAME                       
     , ca.Account_Vendor_Name                          
     ,ca.Account_Vendor_Id                  
     ,Com.Commodity_Id                       
     , c.CONTRACT_START_DATE                               
     , c.CONTRACT_end_DATE                               
     ,  NOTIFICATION_DAYS                   
      ,ch.Client_Id                  
     ,ch.Site_Id                  
     ,ca.Account_Id                  
     ,c.CONTRACT_PRICING_SUMMARY                 
     )    k                         
                      
      SELECT        
            acc.[Contract Number]              
           ,acc.[Client]              
           ,acc.[Division]              
           ,acc.Site_Id              
           ,acc.Site_Reference_Number              
           ,acc.[Site]              
           ,acc.City              
           ,acc.[Account Number]              
           ,acc.[State]              
           ,acc.[Region]              
           ,acc.[Commodity]              
           ,acc.[ContractType]              
           ,acc.[Contract Classification Type]              
           ,acc.[Vendor]              
           ,acc.[Utility]              
           ,acc.Account_Vendor_Id              
           ,acc.Commodity_Id              
           ,acc.[Contract Start Date]              
           ,acc.[Contract End Date]              
           ,acc.[Contract Notification Date]           
           ,acc.Contract_Pricing_Summary             
      FROM              
            #Temp_Account_Details acc              
            INNER JOIN core.Client_Commodity ccc              
                  ON ccc.Client_Id = acc.Client_Id              
                     AND ccc.Commodity_Id = acc.COMMODITY_ID              
            INNER JOIN dbo.Code cd              
                  ON cd.Code_Id = ccc.Commodity_Service_Cd              
                     AND cd.Code_Value = ''Invoice''              
      GROUP BY              
            acc.[Contract Number]              
           ,acc.[Client]              
           ,acc.[Division]              
           ,acc.Site_Id              
           ,acc.Site_Reference_Number              
           ,acc.[Site]              
           ,acc.City              
           ,acc.[Account Number]              
           ,acc.[State]              
           ,acc.[Region]              
           ,acc.[Commodity]              
          ,acc.[ContractType]              
           ,acc.[Contract Classification Type]              
           ,acc.[Vendor]              
           ,acc.[Utility]              
           ,acc.Account_Vendor_Id              
           ,acc.Commodity_Id              
           ,acc.[Contract Start Date]              
           ,acc.[Contract End Date]              
           ,acc.[Contract Notification Date]                          
             ,acc.CONTRACT_PRICING_SUMMARY          
                       
  '                              
                              
      EXEC        
      ( @SQL )                       
                             
END;
;
GO


GRANT EXECUTE ON  [dbo].[Report_DE_Contract_Expiration_Details_By_Utility] TO [CBMS_SSRS_Reports]
GO
