SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.GET_DEAL_TICKET_TRIGGER_STATUS_DATA_P
@userId varchar,
@sessionId varchar,
@dealTicketId integer,
@startDate datetime,
@endDate datetime
AS
	set nocount on
select 
	ent.entity_name Trigger_status,
	rm_deal_ticket_details_id 

from 
	rm_deal_ticket_details rmdtd,
	entity ent
where 
	rmdtd.rm_deal_ticket_id = @dealTicketId AND 
	rmdtd.month_identifier between @startDate and @endDate and
	rmdtd.trigger_status_type_id = ent.entity_id
GO
GRANT EXECUTE ON  [dbo].[GET_DEAL_TICKET_TRIGGER_STATUS_DATA_P] TO [CBMSApplication]
GO
