SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:                  
        dbo.Get_Invoice_Collection_Queue_last_record_detail_by_account                    
                  
Description:                  
			
 

                  
 Output Parameters:                        
 Name                                        DataType        Default        Description                    
--------------------------------------------------------------------------------------------------                   
                  
 Usage Examples:                      
--------------------------------------------------------------------------------------------------
exec Get_Invoice_Collection_Queue_last_record_detail_by_account 55792, '6/1/2018',0

exec Get_Invoice_Collection_Queue_last_record_detail_by_account 64272, '1/1/2019',0
       
 Author Initials:                  
  Initials        Name                  
--------------------------------------------------------------------------------------------------                   
 
                   
 Modifications:                  
  Initials        Date              Modification                  
--------------------------------------------------------------------------------------------------                   
           
                 
******/


CREATE PROCEDURE [dbo].[Get_Invoice_Collection_Queue_last_record_detail_by_account]
     (
         @invoice_Collection_Account_Config_Id INT
         , @invoice_collection_service_begin_dt DATE
         , @seq_no INT = 0
     )
AS
    BEGIN
        DECLARE @account_ID INT;
        SELECT
            @account_ID = Account_Id
        FROM
            Invoice_Collection_Account_Config
        WHERE
            Invoice_Collection_Account_Config_Id = @invoice_Collection_Account_Config_Id;

        SET NOCOUNT ON;

        SELECT  TOP 1
                MAX(icq.Collection_End_Dt) AS max_ICR_ICE_endDate
                , MAX(icac.Invoice_Collection_Account_Config_Id) AS account_config_id
                , MAX(aicm.Service_Month) AS service_month
        FROM
            Invoice_Collection_Account_Config icac
            JOIN Invoice_Collection_Queue icq
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            JOIN Invoice_Collection_Queue_Month_Map icqmm
                ON icqmm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            JOIN Account_Invoice_Collection_Month aicm
                ON aicm.Account_Invoice_Collection_Month_Id = icqmm.Account_Invoice_Collection_Month_Id
            JOIN Code c
                ON c.Code_Id = icq.Status_Cd
        WHERE
            icac.Account_Id = @account_ID
            AND icac.Invoice_Collection_Service_End_Dt < @invoice_collection_service_begin_dt
            AND aicm.Seq_No = @seq_no
            AND c.Code_Value <> 'Archived'
        GROUP BY (icq.Collection_End_Dt)
                 , (icac.Invoice_Collection_Account_Config_Id)
                 , (aicm.Service_Month)
        ORDER BY
            (icq.Collection_End_Dt) DESC
            , service_month DESC;


    END;







GO
GRANT EXECUTE ON  [dbo].[Get_Invoice_Collection_Queue_last_record_detail_by_account] TO [CBMSApplication]
GO
