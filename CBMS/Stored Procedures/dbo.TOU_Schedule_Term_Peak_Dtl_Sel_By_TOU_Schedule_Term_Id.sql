SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********
NAME:  dbo.TOU_Schedule_Term_Peak_Dtl_Sel_By_TOU_Schedule_Term_Id

DESCRIPTION:  

	Used to select Season and TOU Peak name, time and day by given TOU Term Id.

INPUT PARAMETERS:
      Name								DataType          Default     Description
------------------------------------------------------------
	@Time_Of_Use_Schedule_Term_Id		INT
				

OUTPUT PARAMETERS:
      Name								DataType          Default     Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------

	Exec dbo.TOU_Schedule_Term_Peak_Dtl_Sel_By_TOU_Schedule_Term_Id 14


AUTHOR INITIALS:
	Initials Name
------------------------------------------------------------
	BCH		Balaraju


	Initials Date		Modification
------------------------------------------------------------
	BCH		2012-07-12  Created

******/ 
CREATE PROCEDURE dbo.TOU_Schedule_Term_Peak_Dtl_Sel_By_TOU_Schedule_Term_Id
( 
 @Time_Of_Use_Schedule_Term_Id INT )
AS 
BEGIN
      SET NOCOUNT ON ;
 
      SELECT
            ssn.SEASON_ID
           ,ssn.SEASON_NAME
           ,tpk.Time_Of_Use_Schedule_Term_Peak_Id
           ,tpk.Peak_Name
           ,tpkd.Start_Time
           ,tpkd.End_Time
           ,CASE WHEN EXISTS ( SELECT
                                    1
                               FROM
                                    dbo.Time_Of_Use_Schedule_Term_Peak_Billing_Determinant
                               WHERE
                                    Time_of_Use_Schedule_Term_Peak_Id = tpk.Time_of_Use_Schedule_Term_Peak_Id ) THEN 1
                 ELSE 0
            END AS BD_Exists
           ,CASE WHEN MAX(CONVERT(INT,tpkd.Is_Holiday_Active)) = 1 THEN ISNULL(DOWC.Day_Name,'') + 'Holidays'
                                                               ELSE LEFT(DOWC.Day_Name, LEN(DOWC.Day_Name) - 1)
                                                          END AS Days_Of_Week
		   ,LEFT(TOUSDI.Dtl_Id, LEN(TOUSDI.Dtl_Id) - 1) AS Dtl_Ids
		   ,LEFT(DOWCD.Day_Cd, LEN(DOWCD.Day_Cd) - 1) AS Day_Of_Week_Cds
		   ,MAX(CONVERT(INT,tpkd.Is_Holiday_Active)) AS Is_Holiday_Active
      FROM
            dbo.Time_Of_Use_Schedule_Term_Peak tpk
            JOIN dbo.Time_of_Use_Schedule_Term_Peak_Dtl tpkd
                  ON tpkd.Time_Of_Use_Schedule_Term_Peak_Id = tpk.Time_Of_Use_Schedule_Term_Peak_Id
            LEFT JOIN dbo.SEASON ssn
                  ON tpk.SEASON_ID = ssn.SEASON_ID
            CROSS APPLY ( SELECT
                              Cd.Code_Dsc + ', '
                          FROM
                              dbo.Code Cd
                              JOIN dbo.Time_of_Use_Schedule_Term_Peak_Dtl TOUSTPDI
                                ON Cd.Code_Id = TOUSTPDI.Day_Of_Week_Cd
                          WHERE
                              TOUSTPDI.Time_Of_Use_Schedule_Term_Peak_Id = tpk.Time_Of_Use_Schedule_Term_Peak_Id
                              AND TOUSTPDI.Start_Time = tpkd.Start_Time
                              AND TOUSTPDI.End_Time = tpkd.End_Time
            FOR
                          XML PATH('') ) DOWC ( Day_Name )
            CROSS APPLY ( SELECT
                              CONVERT(VARCHAR,TOUSTPDI.Day_Of_Week_Cd) + ', '
                          FROM
                              dbo.Time_of_Use_Schedule_Term_Peak_Dtl TOUSTPDI
                          WHERE
                              TOUSTPDI.Time_Of_Use_Schedule_Term_Peak_Id = tpk.Time_Of_Use_Schedule_Term_Peak_Id
                              AND TOUSTPDI.Start_Time = tpkd.Start_Time
                              AND TOUSTPDI.End_Time = tpkd.End_Time
                              AND TOUSTPDI.Day_Of_Week_Cd IS NOT NULL
            FOR
                          XML PATH('') ) DOWCD ( Day_Cd )
            CROSS APPLY ( SELECT
                              CONVERT(VARCHAR,TOUSTPDI.Time_Of_Use_Schedule_Term_Peak_Dtl_Id) + ', '
                          FROM
                              dbo.Time_of_Use_Schedule_Term_Peak_Dtl TOUSTPDI
                          WHERE
                              TOUSTPDI.Time_Of_Use_Schedule_Term_Peak_Id = tpk.Time_Of_Use_Schedule_Term_Peak_Id
                              AND TOUSTPDI.Start_Time = tpkd.Start_Time
                              AND TOUSTPDI.End_Time = tpkd.End_Time
            FOR
                          XML PATH('') ) TOUSDI ( Dtl_Id )
      WHERE
            tpk.Time_Of_Use_Schedule_Term_Id = @Time_Of_Use_Schedule_Term_Id
      GROUP BY
            ssn.SEASON_ID
           ,ssn.SEASON_NAME
           ,tpk.Time_Of_Use_Schedule_Term_Peak_Id
           ,tpk.Peak_Name
           ,tpkd.Start_Time
           ,tpkd.End_Time
           ,DOWC.Day_Name
           ,TOUSDI.Dtl_Id
           ,DOWCD.Day_Cd
END



;
GO
GRANT EXECUTE ON  [dbo].[TOU_Schedule_Term_Peak_Dtl_Sel_By_TOU_Schedule_Term_Id] TO [CBMSApplication]
GO
