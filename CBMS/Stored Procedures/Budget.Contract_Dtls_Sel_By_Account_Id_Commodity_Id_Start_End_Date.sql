SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
NAME:                      
 [Budget].[Contract_Dtls_Sel_By_Account_Id_Commodity_Id_Start_End_Date]                     
                      
DESCRIPTION:                      
  to get the contract_dtls for the given utility and supplier account                    
                      
INPUT PARAMETERS:                      
 Name     DataType  Default    Description                      
-------------------------------------------------------------------------------------                    
 @Account_Id   INT                    
 @Start_Date Date                
 @End_Date Date                   
 @commodity_Id   INT            
 @Is_Recalc     bit       1 for Recalc , 0 for Budget          
            
                    
OUTPUT PARAMETERS:                      
 Name     DataType  Default    Description                      
-------------------------------------------------------------------------------------                    
                      
USAGE EXAMPLES:                      
-------------------------------------------------------------------------------------                      
exec [Budget].[Contract_Dtls_Sel_By_Account_Id_Commodity_Id_Start_End_Date] 206098,'2020-01-01','2020-12-01',290                    
exec [Budget].[Contract_Dtls_Sel_By_Account_Id_Commodity_Id_Start_End_Date] 206098,'2020-01-01','2020-12-01',290 ,0          
        
-- Test case for Budget         
EXEC [Budget].[Contract_Dtls_Sel_By_Account_Id_Commodity_Id_Start_End_Date] 773607,'2021-01-01','2021-12-01',null ,0         
        
EXEC [Budget].[Contract_Dtls_Sel_By_Account_Id_Commodity_Id_Start_End_Date] 1627544,'2020-01-01','2020-12-01',null ,0         
        
EXEC [Budget].[Contract_Dtls_Sel_By_Account_Id_Commodity_Id_Start_End_Date] 31128,'2020-08-01','2021-07-01',null ,0         
        
        
EXEC [Budget].[Contract_Dtls_Sel_By_Account_Id_Commodity_Id_Start_End_Date] 773593,'2021-01-01','2021-12-01',null ,0         
        
EXEC [Budget].[Contract_Dtls_Sel_By_Account_Id_Commodity_Id_Start_End_Date] 1741439,'2020-06-01','2021-05-31',null ,0         
                    
                      
AUTHOR INITIALS:                      
 Initials Name                      
-------------------------------------------------------------------------------------                      
 RKV  Ravi Kumar Vegesna           
 SLP  Sri Lakshmi Pallikonda               
                       
MODIFICATIONS                      
                      
 Initials Date  Modification                      
-------------------------------------------------------------------------------------                      
 RKV  2016-01-27  Created for Calculator_tester              
 SLP  2020-04-30  Included @Is_Recalc parameter            
******/
CREATE PROCEDURE [Budget].[Contract_Dtls_Sel_By_Account_Id_Commodity_Id_Start_End_Date]
    (
        @Account_Id INT
        , @Start_Date DATE
        , @End_Date DATE
        , @commodity_Id INT = NULL
        , @Is_Recalc BIT = 1
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @tbl_timeperiod AS TABLE
              (
                  service_month DATE
              );
        INSERT INTO @tbl_timeperiod
        SELECT
            dm.FIRST_DAY_OF_MONTH_D
        FROM
            meta.DATE_DIM dm
        WHERE
            dm.FIRST_DAY_OF_MONTH_D BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, @Start_Date), 0)
                                    AND     @End_Date
        GROUP BY
            dm.FIRST_DAY_OF_MONTH_D;


        DECLARE @Contract_Id TABLE
              (
                  Contract_Id INT
                  , Account_Id INT
                  , ED_Contract_Nr VARCHAR(100)
                  , Contract_Start_Date DATE
                  , Contract_End_Date DATE
                  , ENTITY_NAME VARCHAR(100)
                  , Meter_State_Id INT
              );


        INSERT INTO @Contract_Id
             (
                 Contract_Id
                 , Account_Id
                 , ED_Contract_Nr
                 , Contract_Start_Date
                 , Contract_End_Date
                 , ENTITY_NAME
                 , Meter_State_Id
             )
        SELECT
            CASE WHEN e.ENTITY_NAME = 'Supplier' THEN scha.Supplier_Contract_ID
                ELSE 0
            END
            , scha.Account_Id
            , c.ED_CONTRACT_NUMBER
            , CASE WHEN e.ENTITY_NAME = 'Supplier' THEN c.CONTRACT_START_DATE
                  ELSE NULL
              END
            , CASE WHEN e.ENTITY_NAME = 'Supplier' THEN c.CONTRACT_END_DATE
                  ELSE NULL
              END
            , e.ENTITY_NAME
            , ucha.Meter_State_Id
        FROM
            Core.Client_Hier_Account ucha
            INNER JOIN Core.Client_Hier_Account scha
                ON ucha.Meter_Id = scha.Meter_Id
            INNER JOIN dbo.CONTRACT AS c
                ON c.CONTRACT_ID = scha.Supplier_Contract_ID
            LEFT JOIN dbo.ENTITY AS e
                ON e.ENTITY_ID = c.CONTRACT_TYPE_ID
        WHERE
            @Is_Recalc = 0
            AND ucha.Account_Type = 'Utility'
            AND scha.Account_Type = 'Supplier'
            AND scha.Supplier_Contract_ID <> -1
            AND ucha.Account_Id = @Account_Id
            AND (   @Start_Date BETWEEN c.CONTRACT_START_DATE
                                AND     c.CONTRACT_END_DATE
                    OR  @End_Date BETWEEN c.CONTRACT_START_DATE
                                  AND     c.CONTRACT_END_DATE
                    OR  c.CONTRACT_START_DATE BETWEEN @Start_Date
                                              AND     @End_Date
                    OR  c.CONTRACT_END_DATE BETWEEN @Start_Date
                                            AND     @End_Date
                    OR  c.CONTRACT_END_DATE >= GETDATE())
            AND (   @commodity_Id IS NULL
                    OR  ucha.Commodity_Id = @commodity_Id)
        GROUP BY
            CASE WHEN e.ENTITY_NAME = 'Supplier' THEN scha.Supplier_Contract_ID
                ELSE 0
            END
            , scha.Account_Id
            , c.ED_CONTRACT_NUMBER
            , CASE WHEN e.ENTITY_NAME = 'Supplier' THEN c.CONTRACT_START_DATE
                  ELSE NULL
              END
            , CASE WHEN e.ENTITY_NAME = 'Supplier' THEN c.CONTRACT_END_DATE
                  ELSE NULL
              END
            , e.ENTITY_NAME
            , ucha.Meter_State_Id;


        INSERT INTO @Contract_Id
             (
                 Contract_Id
                 , Account_Id
             )
        SELECT
            scha.Supplier_Contract_ID
            , scha.Account_Id
        FROM
            Core.Client_Hier_Account ucha
            INNER JOIN Core.Client_Hier_Account scha
                ON ucha.Meter_Id = scha.Meter_Id
        WHERE
            ucha.Account_Type = 'Utility'
            AND scha.Account_Type = 'Supplier'
            AND scha.Supplier_Contract_ID <> -1
            AND ucha.Account_Id = @Account_Id
            AND (   (@Start_Date BETWEEN scha.Supplier_Meter_Association_Date
                                 AND     scha.Supplier_Meter_Disassociation_Date)
                    OR  (@End_Date BETWEEN scha.Supplier_Meter_Association_Date
                                   AND     scha.Supplier_Meter_Disassociation_Date))
            AND (   @commodity_Id IS NULL
                    OR  ucha.Commodity_Id = @commodity_Id)
            AND @Is_Recalc = 1
        GROUP BY
            scha.Supplier_Contract_ID
            , scha.Account_Id;

        INSERT INTO @Contract_Id
             (
                 Contract_Id
                 , Account_Id
             )
        SELECT
            scha.Supplier_Contract_ID
            , scha.Account_Id
        FROM
            Core.Client_Hier_Account scha
        WHERE
            scha.Account_Type = 'Supplier'
            AND scha.Account_Id = @Account_Id
            AND (   (@Start_Date BETWEEN scha.Supplier_Meter_Association_Date
                                 AND     scha.Supplier_Meter_Disassociation_Date)
                    OR  (@End_Date BETWEEN scha.Supplier_Meter_Association_Date
                                   AND     scha.Supplier_Meter_Disassociation_Date))
            AND (   @commodity_Id IS NULL
                    OR  scha.Commodity_Id = @commodity_Id)
            AND scha.Supplier_Contract_ID <> -1
            AND NOT EXISTS (SELECT  1 FROM  @Contract_Id c WHERE c.Contract_Id = scha.Supplier_Contract_ID)
            AND @Is_Recalc = 1
        GROUP BY
            scha.Supplier_Contract_ID
            , scha.Account_Id;


        -- SELECT  * FROM  @Contract_Id AS ci;          

        IF @Is_Recalc = 0
            BEGIN

                SELECT
                    x.service_month
                    , x.Contract_Id
                    , x.ED_Contract_Nr
                    , x.Contract_Start_Date
                    , x.Contract_End_Date
                    , x.ENTITY_NAME
                    , x.Meter_State_Id
                INTO
                    #final_output
                FROM    (   SELECT
                                tt.service_month
                                , Contract_Id
                                , ci.ED_Contract_Nr
                                , ci.Contract_Start_Date
                                , ci.Contract_End_Date
                                , ci.ENTITY_NAME
                                , ci.Meter_State_Id
                                , ROW_NUMBER() OVER (PARTITION BY
                                                         service_month
                                                     ORDER BY
                                                         Contract_Start_Date ASC) Row_nr
                            FROM
                                @tbl_timeperiod AS tt
                                CROSS APPLY @Contract_Id AS ci) x
                WHERE
                    Row_nr = 1
                    AND (   Contract_Start_Date BETWEEN @Start_Date
                                                AND     @End_Date
                            OR  Contract_End_Date BETWEEN @Start_Date
                                                  AND     @End_Date
                            OR  Contract_End_Date >= GETDATE())
                    AND x.service_month <= Contract_End_Date;


                INSERT INTO #final_output
                SELECT
                    x.service_month
                    , x.Contract_Id
                    , x.ED_Contract_Nr
                    , x.Contract_Start_Date
                    , x.Contract_End_Date
                    , x.ENTITY_NAME
                    , x.Meter_State_Id
                FROM    (   SELECT
                                tt.service_month
                                , Contract_Id
                                , ci.ED_Contract_Nr
                                , ci.Contract_Start_Date
                                , ci.Contract_End_Date
                                , ci.ENTITY_NAME
                                , ci.Meter_State_Id
                                , ROW_NUMBER() OVER (PARTITION BY
                                                         service_month
                                                     ORDER BY
                                                         Contract_End_Date DESC) Row_nr
                            FROM
                                @tbl_timeperiod AS tt
                                CROSS APPLY @Contract_Id AS ci) x
                WHERE
                    Row_nr = 1
                    AND (Contract_End_Date >= GETDATE())
                    AND NOT EXISTS (SELECT  1 FROM  #final_output fo WHERE  fo.service_month = x.service_month);

                SELECT
                    service_month
                    , Contract_Id
                    , ED_Contract_Nr
                    , Contract_Start_Date
                    , Contract_End_Date
                    , ENTITY_NAME
                    , Meter_State_Id
                    , @Start_Date Invoice_Begin_Dt
                    , @End_Date Invoice_End_Dt
                FROM
                    #final_output ORDER BY service_month

                DROP TABLE #final_output;


            END;
        ELSE
            BEGIN
                SELECT
                    @Start_Date service_month
                    , c.CONTRACT_ID
                    , c.ED_CONTRACT_NUMBER AS ED_Contract_Nr
                    , c.CONTRACT_START_DATE
                    , c.CONTRACT_END_DATE
                    , cha.Meter_State_Id
                    , @Start_Date AS Invoice_Begin_Dt
                    , @End_Date AS Invoice_End_Dt
                FROM
                    Core.Client_Hier_Account cha
                    INNER JOIN dbo.CONTRACT c
                        ON cha.Supplier_Contract_ID = c.CONTRACT_ID
                WHERE
                    EXISTS (   SELECT
                                    1
                               FROM
                                    @Contract_Id e
                               WHERE
                                    e.Contract_Id = c.CONTRACT_ID
                                    AND e.Account_Id = cha.Account_Id)
                GROUP BY
                    c.CONTRACT_ID
                    , c.ED_CONTRACT_NUMBER
                    , c.CONTRACT_START_DATE
                    , c.CONTRACT_END_DATE
                    , cha.Meter_State_Id;
            END;

    END;

GO
GRANT EXECUTE ON  [Budget].[Contract_Dtls_Sel_By_Account_Id_Commodity_Id_Start_End_Date] TO [CBMSApplication]
GO
