SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 [DBO].[SUPPLIER_ACCOUNT_DETAILS_INS]    
    
DESCRIPTION:  
  
 INSERTS SUPPLIER_ACCOUNT DETAILS INTO ACCOUNT TABLE    
 IF "APPLY THIS SETTINGS AS THE DEFAULT FOR NEW ACCOUNTS" IS ENABLED IN PAGE IT HAS TO 
 RESET ALL THE SUPPLIER_ACCOUNT_IS_DEFAULT_SETTING TO 0 AND UPDATE SUPPLIER_ACCOUNT_IS_DEFAULT_SETTING 
 TO 1 FOR NEWLY SELECTED SUPPLIER ACCOUNT.    
    
    
INPUT PARAMETERS:    
NAME						DATATYPE				DEFAULT						DESCRIPTION    
---------------------------------------------------------------------------------------------    
@VENDOR_ID					INT    
@ACCOUNT_NUMBER				VARCHAR(50)    
@ACCOUNT_TYPE_ID			INT    
@NOT_EXPECTED				BIT    
@NOT_MANAGED				BIT    
@NOT_EXPECTED_DATE			DATETIME    
@UBM_ID						INT    
@SUPPLIER_ACCOUNT_BEGIN_DT	DATETIME    
@SUPPLIER_ACCOUNT_END_DT	DATETIME    
@SERVICE_LEVEL_TYPE_ID		INT    
@DEFAULT_SETTING			INT    
@CONTRACT_ID				INT    
@INVOICE_SOURCE_TYPE_ID		INT    
@RECALC_TYPE_ID				INT 
@Is_Invoice_Posting_Blocked BIT		0	
@Alternate_Account_Number	 NVARCHAR(200)  
    
    
OUTPUT PARAMETERS:    
NAME						DATATYPE				DEFAULT						DESCRIPTION    
---------------------------------------------------------------------------------------------    
@ACCOUNTID					INT    
    
USAGE EXAMPLES:    
---------------------------------------------------------------------------------------------    
    
BEGIN TRANSACTION
	DECLARE @Acc INT
	EXEC dbo.SUPPLIER_ACCOUNT_DETAILS_INS 
		  @VENDOR_ID = 251
		 ,@ACCOUNT_NUMBER = 'Test Suplier Acc'
		 ,@ACCOUNT_TYPE_ID = 37
		 ,@NOT_EXPECTED = 0
		 ,@NOT_MANAGED = 1
		 ,@NOT_EXPECTED_DATE = ''
		 ,@UBM_ACCOUNT_CODE = ''
		 ,@SUPPLIER_ACCOUNT_BEGIN_DT = '2013-06-12 10:53:23'
		 ,@SUPPLIER_ACCOUNT_END_DT = '2013-06-12 10:53:23'
		 ,@SERVICE_LEVEL_TYPE_ID = 859
		 ,@DEFAULT_SETTING = 0
		 ,@CONTRACT_ID = 0
		 ,@INVOICE_SOURCE_TYPE_ID = 29
		 ,@RECALC_TYPE_CD = 824
		 ,@DATA_ENTRY_ONLY = NULL
		
		 ,@ACCOUNTID = @Acc OUTPUT
		 ,@Not_Expected_By_Id=49
		 ,@Is_Invoice_Posting_Blocked=0
		 ,@Alternate_Account_Number='123456-123456'
		 ,@Supplier_Account_Determinant_Source_Cd=102536
	SELECT * FROM dbo.ACCOUNT WHERE ACCOUNT_ID = @Acc   
ROLLBACK TRANSACTION
    
AUTHOR INITIALS:    
INITIALS	NAME    
---------------------------------------------------------------------------------------------    
MGB		BHASKARAN GOPALAKRISHNAN    
HG		Hari  
NK		Nageswara Rao Kosuri
RR		Raghu Reddy 
SP		Sandeep Pigilam
NR		Narayana Reddy
TRK		Ramakrishna Thummala 

MODIFICATIONS    
INITIALS DATE		MODIFICATION    
---------------------------------------------------------------------------------------------    
MGB		21-AUG-09	CREATED  
HG		23-OCT-09	IF statement used to update acct table eliminiated.  
						Subselect substituted with explicit joins.  
						Contract table join removed from the update statement.  
						Unused variable @Supplier_Account_Default_Setting removed.  
MGB		03-Nov-09	included @RA_WATCH_LIST as a parameter  
HG		11/05/2009	Contract.Contract_Recalc_Type_id and Account.Supplier_Account_Recalc_Type_id column relation moved to Code/Codeset from Entity  
						Account.Supplier_Account_Recalc_Type_id renamed to Supplier_Account_Recalc_Type_Cd   
						@Recalc_Type_id parameter renamed to @Recalc_Type_Cd  
  
NK		11/06/2009	commented RA watch list
NK		11/11/2009	Incorporated the watchlist and Is Data Entry Only  
MGB		11/17/2009	Modified @UBM_ID to @UBM_ACCOUNT_CODE
RR		2013-06-10	ENHANCE-38 Its an enhancement the user have given ability to make account not expected or not managed while creating
							the account, so added new parameter @Not_Expected_By_Id for record
SP		2014-08-05  Data Operations Enhancement Phase III,Added Is_Invoice_Posting_Blocked as input parameter.		
RR		2015-07-20  For Global CBMS Sourcing - Added @Alternate_Account_Number as input parameter.	
NR		2016-10-19	MAINT-4358	Added is null Function for Is_Data_Entry_Only column.	
SP		2016-12-14	Invoice tracking,Added  Invoice_Collection_Is_Chased,Invoice_Collection_Is_Chased_Changed AS INPUT PARAM.  
NR		2018-02-22	Recalc Data Interval - Add  @Supplier_Account_Determinant_Source_Cd To this Parameter.
NR		2019-04-03	Data2.0 - Removed WATCHLIST-GROUP-INFO-ID  column from Account Table.	
NR		2019-08-27	Add Contract - Saved Service Level type as Blank entity for Suppler accounts.		
RKV		2019-12-30	D20-1762  Removed UBM Details
TRK		2020-02-05  Added dbo.ACCOUNT_POSTING_BLOCKED_AUDIT_TRACKING insert for Audit purpose.
******/

CREATE PROCEDURE [dbo].[SUPPLIER_ACCOUNT_DETAILS_INS]
     (
         @VENDOR_ID INT
         , @ACCOUNT_NUMBER VARCHAR(50)
         , @ACCOUNT_TYPE_ID INT
         , @NOT_EXPECTED BIT
         , @NOT_MANAGED BIT
         , @NOT_EXPECTED_DATE DATETIME
         , @SUPPLIER_ACCOUNT_BEGIN_DT DATETIME
         , @SUPPLIER_ACCOUNT_END_DT DATETIME
         , @SERVICE_LEVEL_TYPE_ID INT
         , @DEFAULT_SETTING INT
         , @CONTRACT_ID INT
         , @INVOICE_SOURCE_TYPE_ID INT
         , @RECALC_TYPE_CD INT
         , @DATA_ENTRY_ONLY INT
         , @Not_Expected_By_Id INT = NULL
         , @ACCOUNTID INT OUT
         , @Is_Invoice_Posting_Blocked BIT = 0
         , @Alternate_Account_Number NVARCHAR(200) = NULL
         , @Invoice_Collection_Is_Chased VARCHAR = NULL
         , @Invoice_Collection_Is_Chased_Changed BIT = 0
         , @User_Info_Id INT = NULL
         , @Supplier_Account_Determinant_Source_Cd INT = NULL
         
     )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Blank_Entity_Id INT
            , @Supplier_Account_Type_Id INT;

        SELECT
            @Blank_Entity_Id = e.ENTITY_ID
        FROM
            dbo.ENTITY e
        WHERE
            e.ENTITY_NAME = ' '
            AND e.ENTITY_TYPE = 708;

        SELECT
            @Supplier_Account_Type_Id = e.ENTITY_ID
        FROM
            dbo.ENTITY e
        WHERE
            e.ENTITY_NAME = 'Supplier'
            AND e.ENTITY_DESCRIPTION = 'Account Type';

        BEGIN TRY

            BEGIN TRAN;

            DECLARE @Invoice_Collection_Is_Chased_Bit BIT;

            UPDATE
                acc
            SET
                acc.Supplier_Account_Is_Default_Setting = 0
            FROM
                dbo.ACCOUNT acc
                JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                    ON samm.ACCOUNT_ID = acc.ACCOUNT_ID
            WHERE
                samm.Contract_ID = @CONTRACT_ID
                AND @DEFAULT_SETTING = 1;

            INSERT INTO dbo.ACCOUNT
                 (
                     VENDOR_ID
                     , ACCOUNT_NUMBER
                     , ACCOUNT_TYPE_ID
                     , NOT_EXPECTED
                     , NOT_MANAGED
                     , NOT_EXPECTED_DATE
                     , Supplier_Account_Begin_Dt
                     , Supplier_Account_End_Dt
                     , SERVICE_LEVEL_TYPE_ID
                     , Supplier_Account_Is_Default_Setting
                     , INVOICE_SOURCE_TYPE_ID
                     , Supplier_Account_Recalc_Type_Cd
                     , Is_Data_Entry_Only
                     , NOT_EXPECTED_BY_ID
                     , Is_Invoice_Posting_Blocked
                     , Alternate_Account_Number
                     , Supplier_Account_Determinant_Source_Cd
                 )
            VALUES
                (@VENDOR_ID
                 , @ACCOUNT_NUMBER
                 , @ACCOUNT_TYPE_ID
                 , @NOT_EXPECTED
                 , @NOT_MANAGED
                 , @NOT_EXPECTED_DATE
                 , @SUPPLIER_ACCOUNT_BEGIN_DT
                 , @SUPPLIER_ACCOUNT_END_DT
                 , CASE WHEN @Supplier_Account_Type_Id = @ACCOUNT_TYPE_ID THEN @Blank_Entity_Id
                       ELSE ISNULL(@SERVICE_LEVEL_TYPE_ID, @Blank_Entity_Id)
                   END
                 , @DEFAULT_SETTING
                 , @INVOICE_SOURCE_TYPE_ID
                 , @RECALC_TYPE_CD
                 , ISNULL(@DATA_ENTRY_ONLY, 0)
                 , @Not_Expected_By_Id
                 , @Is_Invoice_Posting_Blocked
                 , @Alternate_Account_Number
                 , @Supplier_Account_Determinant_Source_Cd);
				

            SELECT  @ACCOUNTID = SCOPE_IDENTITY();
		  
			IF @Is_Invoice_Posting_Blocked = 1
			BEGIN
				INSERT INTO dbo.ACCOUNT_POSTING_BLOCKED_AUDIT_TRACKING
				(
					ACCOUNT_ID,
					Created_User_Id,
					Created_Ts,
					Updated_User_Id,
					Last_Change_Ts
				)
				VALUES
				(   @ACCOUNTID,    -- ACCOUNT_ID - int
					@User_Info_Id, -- Created_User_Id - int
					GETDATE(),     -- Created_Ts - datetime
					@User_Info_Id, -- Updated_User_Id - int
					GETDATE()      -- Last_Change_Ts - datetime
					);
			END;

            SELECT
                @Invoice_Collection_Is_Chased_Bit = CAST(ISNULL(@Invoice_Collection_Is_Chased, 0) AS BIT);



            COMMIT TRAN;
        END TRY
        BEGIN CATCH

            ROLLBACK TRAN;
            EXEC usp_RethrowError;
        END CATCH;

    END;






    ;






GO



GRANT EXECUTE ON  [dbo].[SUPPLIER_ACCOUNT_DETAILS_INS] TO [CBMSApplication]
GO
