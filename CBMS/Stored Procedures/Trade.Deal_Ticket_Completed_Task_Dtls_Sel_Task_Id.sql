SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    Trade.Deal_Ticket_Completed_Task_Dtls_Sel_Task_Id
   
    
DESCRIPTION:   
   
  This procedure to get summary page details for deal ticket summery.
    
INPUT PARAMETERS:    
      Name          DataType       Default        Description    
-----------------------------------------------------------------------------    
	@Deal_Ticket_Id   INT
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
		Exec Trade.Deal_Ticket_Completed_Task_Dtls_Sel_Task_Id  288 ,21
		Exec Trade.Deal_Ticket_Completed_Task_Dtls_Sel_Task_Id  291 ,21

       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	RR          Raghu Reddy
    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	RR          2019-02-21  Global Risk Management - Create sp to get summary page details for deal ticket summery
     
    
******/  

CREATE PROCEDURE [Trade].[Deal_Ticket_Completed_Task_Dtls_Sel_Task_Id]
      ( 
       @Deal_Ticket_Id INT
      ,@Workflow_Task_Id INT )
AS 
BEGIN  
      SET NOCOUNT ON;	
            
      SELECT
            dtch.Deal_Ticket_Id
           ,wftsn.Transition_Name
           ,REPLACE(CONVERT(VARCHAR(20), MAX(chws.Created_Ts), 106), ' ', '-') AS Task_Date
           ,REPLACE(CONVERT(VARCHAR(20), MAX(chws.Created_Ts), 106), ' ', '-') + ' at ' + LTRIM(RIGHT(CONVERT(VARCHAR(20), MAX(chws.Created_Ts), 100), 7)) + ' EST' AS Task_Date_Time
           ,chws.Workflow_Status_Comment
           ,CASE WHEN wtl.Level_Name = 'Deal Ticket' THEN -1
                 WHEN wtl.Level_Name = 'Participant' THEN dtch.Client_Hier_Id
            END AS Client_Hier_Id
           ,CASE WHEN wtl.Level_Name = 'Deal Ticket' THEN NULL
                 WHEN wtl.Level_Name = 'Participant' THEN ch.Site_name
            END AS Site_Name
           ,ui.USERNAME
      FROM
            Trade.Deal_Ticket_Client_Hier dtch
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                  ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
            INNER JOIN Trade.Workflow_Task_Status_Transition_Map wftsts
                  ON wftsts.Descendant_Workflow_Status_Map_Id = chws.Workflow_Status_Map_Id
            INNER JOIN Trade.Workflow_Transition wftsn
                  ON wftsts.Workflow_Transition_Id = wftsn.Workflow_Transition_Id
            INNER JOIN Trade.Workflow_Task_Status_Map wtsm
                  ON wtsm.Workflow_Task_Status_Map_Id = wftsts.Workflow_Task_Status_Map_Id
            INNER JOIN Trade.Workflow_Status_Map wsm
                  ON wtsm.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
            INNER JOIN Trade.Workflow_Status ws
                  ON wsm.Workflow_Status_Id = ws.Workflow_Status_Id
            INNER JOIN dbo.USER_INFO ui
                  ON ui.USER_INFO_ID = chws.Updated_User_Id
            INNER JOIN Trade.Workflow_Transition_Level wtl
                  ON wftsts.Workflow_Transition_Level_Id = wtl.Workflow_Transition_Level_Id
            INNER JOIN Core.Client_Hier ch
                  ON dtch.Client_Hier_Id = ch.Client_Hier_Id
      WHERE
            dtch.Deal_Ticket_Id = @Deal_Ticket_Id
            --AND chws.Is_Active = 0
            AND wtsm.Workflow_Task_Id = @Workflow_Task_Id
      GROUP BY
            dtch.Deal_Ticket_Id
           ,wftsn.Transition_Name
           ,chws.Created_Ts
           ,chws.Workflow_Status_Comment
           ,CASE WHEN wtl.Level_Name = 'Deal Ticket' THEN -1
                 WHEN wtl.Level_Name = 'Participant' THEN dtch.Client_Hier_Id
            END
           ,CASE WHEN wtl.Level_Name = 'Deal Ticket' THEN NULL
                 WHEN wtl.Level_Name = 'Participant' THEN ch.Site_name
            END
           ,ui.USERNAME
      ORDER BY
            chws.Created_Ts DESC
END;

GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Completed_Task_Dtls_Sel_Task_Id] TO [CBMSApplication]
GO
