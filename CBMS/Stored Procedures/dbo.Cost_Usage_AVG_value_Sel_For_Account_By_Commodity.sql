
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	[dbo].[Cost_Usage_AVG_value_Sel_For_Account_By_Commodity]

DESCRIPTION:
		
INPUT PARAMETERS:
	Name				   DataType		Default	Description
------------------------------------------------------------
	@Account_Id		   INT
	@Site_Client_Hier_Id   INT
	@Commodity_id		   INT
	@Begin_Dt			   DATE
	@End_Dt			   DATE
	@Currency_Unit_Id	   INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	 EXEC [dbo].[Cost_Usage_Avg_value_Sel_For_Account_By_Commodity] 
						  @Account_Id = 209976
						 ,@Site_Client_Hier_Id = 53868
						 ,@Begin_Dt = '1/1/2011'
						 ,@End_Dt = '12/1/2011'
						 ,@Currency_Unit_Id = 3
						 ,@Commodity_id = 290
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG		Hari
	SSR		Sharad Srivastava
	AP		Athmaram Pabbathi
	KVK		Vinay k
	SP		Sandeep Pigilam
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG			02/05/2010	Created
	SSR			02/17/2010	Removed Input parameter @Client_ID
	SSR			03/02/2010	Refered Account & SUPPLIER_ACCOUNT_METER_MAP table to get the Site_ID
	HG			03/29/2010	ISNULL function used to determine the conversion factor removed and using bucket type to determine the conversion factors.
	SSR			04/14/2010	Replaced LEFT JOIN of Cost_usage_Account_dtl to INNER JOIN with Bucket_Master
	HG			04/29/2010	Unit Cost added as one of the row with other bucket values as per the variance page requirement
	AP			08/09/2011	Removed @UOM_Type_ID parameter and modified the conv logic to use default_uom_type_id from bucket_master. 
						    Replaced script for @currency_group_id with client_heir & client_hier_account tables; included "Total Usage" bucket for volume
						    and added additional filters for Is_Required_For_Variance_Test = 1 and Is_Active = 1 on bucket_master
    AP			08/30/2011	Removed CTE and UNION statment used for Unit_Cost calculation, since it is handled in application
    AP			09/09/2011	Added UNION statement back to the script
	AP			09/12/2011	Replaced CTE_Cu_Account_Dtl with @Cost_Usage_Account_Dtl table variable
	AP			03/27/2012	Added @Site_Client_Hier_Id as a parameter and modified SELECT for currency_group_id and removed CHA table
	HG			2012-07-09	MAINT-1355, modified to fix the Unit cost calculation to consider only Total Usage/ Volume determinant
								--UNION changed to UNION ALL as it is always going to return unique set of values
    AKR			2013-07-16  Modified for Detailed Variance, Added UOM_Id and UOM_Name
	KVK			10/01/2013  Added recompile statement in the procedure definition	
	SP			2014-05-13  Added Service_Month in select list	.  
	SP			13-10-2016	Variance Test Enhancements,Used CURRENCY_UNIT_COUNTRY_MAP for default currency_id and Country_Commodity_Uom for default uom_id. 	
	RR			06-12-2016	Added Bucket_Daily_Average_Value to select list		
******/
CREATE PROCEDURE [dbo].[Cost_Usage_AVG_value_Sel_For_Account_By_Commodity]
      ( 
       @Account_Id INT
      ,@Site_Client_Hier_Id INT
      ,@Commodity_id INT
      ,@Begin_Dt DATE
      ,@End_Dt DATE
      ,@Currency_Unit_Id INT )
      WITH RECOMPILE
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE
            @Currency_Group_Id INT
           ,@Currency_Name VARCHAR(200)
           ,@Usage_Bucket_Stream_Cd INT
           ,@Country_Id INT
           ,@Country_Default_Uom_Type_Id INT               

      DECLARE @Cost_Usage_Account_Dtl TABLE
            ( 
             Service_Month DATE
            ,Account_Id INT
            ,Bucket_Type VARCHAR(25)
            ,Bucket_Name VARCHAR(255)
            ,Bucket_Value DECIMAL(28, 10)
            ,Total_Cost DECIMAL(28, 10)
            ,Volume DECIMAL(28, 10)
            ,UOM_Id INT
            ,UOM_Name VARCHAR(200)
            ,Billing_Days INT )

      SELECT
            @Usage_Bucket_Stream_Cd = cd.Code_Id
      FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code cd
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'BucketStream'
            AND cd.Code_Value = 'Usage';
            
      SELECT
            @Currency_Group_Id = ch.Client_Currency_Group_Id
           ,@Country_Id = ch.Country_Id
      FROM
            core.Client_Hier ch
      WHERE
            ch.Client_Hier_Id = @Site_Client_Hier_Id

      SELECT
            @Currency_Unit_Id = MAX(cucm.CURRENCY_UNIT_ID)
      FROM
            dbo.CURRENCY_UNIT_COUNTRY_MAP cucm
      WHERE
            cucm.Country_Id = @Country_Id
	  
	  
      SELECT
            @Country_Default_Uom_Type_Id = ccu.Default_Uom_Type_Id
      FROM
            dbo.Country_Commodity_Uom ccu
      WHERE
            ccu.Commodity_Id = @Commodity_Id
            AND ccu.COUNTRY_ID = @Country_Id
                  
      SELECT
            @Currency_Name = cu.CURRENCY_UNIT_NAME
      FROM
            dbo.CURRENCY_UNIT cu
      WHERE
            cu.CURRENCY_UNIT_ID = @Currency_Unit_Id   

      INSERT      INTO @Cost_Usage_Account_Dtl
                  ( 
                   Service_Month
                  ,Account_Id
                  ,Bucket_Type
                  ,Bucket_Name
                  ,Bucket_Value
                  ,Total_Cost
                  ,Volume
                  ,UOM_Id
                  ,UOM_Name
                  ,Billing_Days )
                  SELECT
                        cua.Service_Month
                       ,@Account_Id Account_ID
                       ,bkt_cd.Code_Value Bucket_Type
                       ,bm.Bucket_Name
                       ,( cua.Bucket_Value * ( CASE WHEN bkt_cd.Code_Value = 'Charge' THEN cc.Conversion_factor
                                                    WHEN bkt_cd.Code_Value = 'Determinant' THEN uc.Conversion_Factor
                                               END ) ) Bucket_Value
                       ,( CASE WHEN bkt_cd.Code_Value = 'Charge' THEN cua.Bucket_Value * cc.Conversion_Factor
                          END ) AS Total_Cost
                       ,( CASE WHEN bkt_cd.Code_Value = 'Determinant' THEN cua.Bucket_Value * uc.Conversion_Factor
                          END ) AS Volume
                       ,CASE WHEN bkt_cd.Code_Value = 'Charge' THEN @Currency_Unit_Id
                             ELSE ( CASE WHEN bm.Bucket_Stream_Cd = @Usage_Bucket_Stream_Cd THEN @Country_Default_Uom_Type_Id
                                         ELSE bm.Default_Uom_Type_Id
                                    END )
                        END UOM_Id
                       ,CASE WHEN bkt_cd.Code_Value = 'Charge' THEN @Currency_Name
                             ELSE en.ENTITY_NAME
                        END UOM_Name
                       ,cubd.Billing_Days
                  FROM
                        dbo.Cost_Usage_Account_Dtl cua
                        INNER JOIN dbo.Bucket_Master bm
                              ON bm.Bucket_Master_Id = cua.Bucket_Master_Id
                        INNER JOIN dbo.Code bkt_cd
                              ON bkt_cd.Code_Id = bm.Bucket_Type_Cd
                        LEFT JOIN dbo.Consumption_Unit_Conversion uc
                              ON uc.Base_Unit_ID = cua.UOM_Type_Id
                                 AND uc.Converted_Unit_ID = ( CASE WHEN bm.Bucket_Stream_Cd = @Usage_Bucket_Stream_Cd THEN @Country_Default_Uom_Type_Id
                                                                   ELSE bm.Default_Uom_Type_Id
                                                              END )
                        LEFT OUTER JOIN dbo.ENTITY en
                              ON en.ENTITY_ID = ( CASE WHEN bm.Bucket_Stream_Cd = @Usage_Bucket_Stream_Cd THEN @Country_Default_Uom_Type_Id
                                                       ELSE bm.Default_Uom_Type_Id
                                                  END )
                        LEFT JOIN dbo.Currency_Unit_Conversion cc
                              ON ( cc.currency_group_id = @Currency_Group_Id
                                   AND cc.base_unit_id = cua.CURRENCY_UNIT_ID
                                   AND cc.converted_unit_id = @Currency_unit_id
                                   AND cc.conversion_date = cua.Service_month )
                        LEFT JOIN dbo.Cost_Usage_Account_Billing_Dtl cubd
                              ON cua.ACCOUNT_ID = cubd.Account_Id
                                 AND cua.Service_Month = cubd.Service_Month
                  WHERE
                        cua.Account_Id = @Account_Id
                        AND cua.Client_Hier_Id = @Site_Client_Hier_Id
                        AND cua.Service_Month BETWEEN @Begin_Dt AND @End_Dt
                        AND bm.Commodity_Id = @Commodity_id
                        AND BM.Is_Required_For_Variance_Test = 1
                        AND BM.Is_Active = 1
                        

      SELECT
            cu.Account_Id
           ,cu.Bucket_Name
           ,cu.Bucket_Type
           ,AVG(cu.Bucket_value) Bucket_value
           ,cu.UOM_Id
           ,cu.UOM_Name
           ,NULL AS Service_Month
           ,AVG(cu.Bucket_value / NULLIF(cu.Billing_Days, 0)) AS Bucket_Daily_Average_Value
      FROM
            @Cost_Usage_Account_Dtl cu
      GROUP BY
            cu.Account_Id
           ,cu.Bucket_Name
           ,cu.Bucket_Type
           ,cu.UOM_Id
           ,cu.UOM_Name
      UNION ALL
      SELECT
            UnitCost.Account_Id
           ,UnitCost.Bucket_Name
           ,UnitCost.Bucket_Type
           ,AVG(UnitCost.Unit_Cost)
           ,@Currency_Unit_Id UOM_Id
           ,@Currency_Name UOM_Name
           ,NULL AS Service_Month
           ,AVG(UnitCost.Daily_Unit_Cost) AS Bucket_Daily_Average_Value
      FROM
            ( SELECT
                  cu.Account_id
                 ,cu.Service_Month
                 ,'Unit Cost' Bucket_Name
                 ,'Charge' Bucket_Type
                 ,SUM(cu.Total_Cost) / NULLIF(SUM(cu.Volume), 0) Unit_Cost
                 ,SUM(cu.Total_Cost / NULLIF(cu.Billing_Days, 0)) / NULLIF(SUM(cu.Volume / NULLIF(cu.Billing_Days, 0)), 0) Daily_Unit_Cost
              FROM
                  @Cost_Usage_Account_Dtl cu
              WHERE
                  cu.Bucket_Name IN ( 'Total Cost', 'Total Usage', 'Volume' )
              GROUP BY
                  cu.Account_id
                 ,cu.Service_Month ) AS UnitCost
      GROUP BY
            UnitCost.Account_Id
           ,UnitCost.Bucket_Name
           ,UnitCost.Bucket_Type
      ORDER BY
            Bucket_Type
           ,Bucket_Name
END;
;



;
GO







GRANT EXECUTE ON  [dbo].[Cost_Usage_AVG_value_Sel_For_Account_By_Commodity] TO [CBMSApplication]
GO
