SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO











--exec BUDGET_GET_CONTRACT_HEADER_INFO_P -1,-1,'29719' 

CREATE    PROCEDURE dbo.BUDGET_GET_CONTRACT_HEADER_INFO_P 
	@userId varchar,
	@sessionId varchar,
	@contractId int 
       	AS
	begin
	set nocount on

	select	contract_vw.ed_contract_number as contract_number, 
		cli.client_name, 
		utility.vendor_name utility, 
		supplier.vendor_name supplier, 
		isnull(suppacc.ACCOUNT_NUMBER,'Not yet Assigned') as supplier_account_number, 
		commodity_type.entity_name commodity, 
		contract_vw.contract_start_date, 
		contract_vw.contract_end_date,  
		stat.state_name,
		budget_contract_budget.budget_contract_budget_id,
		budget_contract_budget.comments

	from 	client cli(nolock), 
		account utilacc (nolock), 
		account suppacc (nolock), 
		budget_contract_vw contract_vw(nolock)
		left join budget_contract_budget on budget_contract_budget.contract_id = contract_vw.contract_id,
		entity commodity_type (nolock), 
		vwSiteName veSite (nolock), 
		state stat (nolock), 
		vendor utility (nolock), 
		vendor supplier (nolock)

	where	contract_vw.contract_id = @contractId
		and commodity_type.entity_id = contract_vw.commodity_type_id
		and utilacc.account_id = contract_vw.account_id
		and supplier.vendor_id = contract_vw.vendor_id
		and suppacc.account_id = contract_vw.supp_account_id
		and utility.vendor_id = utilacc.vendor_id
		and veSite.site_id = utilacc.site_id
		and cli.client_id = veSite.client_id
		and stat.state_id = veSite.state_id 	 
              
	end









GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_CONTRACT_HEADER_INFO_P] TO [CBMSApplication]
GO
