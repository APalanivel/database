SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_ONBOARD_CLIENT_ID_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(50)	          	
	@clientId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE dbo.GET_ONBOARD_CLIENT_ID_P
@userId varchar(10),
@sessionId varchar(50),
@clientId int
as
set nocount on
	select  RM_onboard_client_id  
	from RM_ONBOARD_CLIENT
	where client_id = @clientId
GO
GRANT EXECUTE ON  [dbo].[GET_ONBOARD_CLIENT_ID_P] TO [CBMSApplication]
GO
