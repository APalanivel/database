SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO


CREATE PROCEDURE dbo.ADD_APPROVAL_FOR_EDIT_FORECAST_P
@userId varchar(10),
@sessionId varchar(20),
@siteId int,
@cbmsImageId int,
@description varchar(1000)
 AS

insert into RM_MANAGE_FORECAST_APPROVAL(site_id, cbms_image_id, description)
values(@siteId, @cbmsImageId, @description)
GO
GRANT EXECUTE ON  [dbo].[ADD_APPROVAL_FOR_EDIT_FORECAST_P] TO [CBMSApplication]
GO
