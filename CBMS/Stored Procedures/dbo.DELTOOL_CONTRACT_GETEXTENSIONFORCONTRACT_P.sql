SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE  PROCEDURE dbo.DELTOOL_CONTRACT_GETEXTENSIONFORCONTRACT_P
	( @MyAccountId int
	, @contract_id int
	)
AS
BEGIN
	set nocount on
	   select c.contract_id
		, c.ed_contract_number
	     from contract c
	    where c.base_contract_id = @contract_id 

END
GO
GRANT EXECUTE ON  [dbo].[DELTOOL_CONTRACT_GETEXTENSIONFORCONTRACT_P] TO [CBMSApplication]
GO
