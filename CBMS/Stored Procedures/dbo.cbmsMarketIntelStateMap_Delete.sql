SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE    PROCEDURE [dbo].[cbmsMarketIntelStateMap_Delete]
	( @MyAccountId int
	, @MarketIntelId int
	)
AS
BEGIN

	set nocount on

	  delete market_intel_state_map
	   where market_intel_id = @MarketIntelId

--	set nocount off

END
GO
GRANT EXECUTE ON  [dbo].[cbmsMarketIntelStateMap_Delete] TO [CBMSApplication]
GO
