SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.GET_NOT_MANAGED_ACCOUNTS_FOR_DO_NOT_TRACK_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
@userId				VARCHAR(10)
,@sessionId			VARCHAR(20)
,@clientId			INT
,@division_id		INT
,@site_id			INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

--exec dbo.GET_NOT_MANAGED_ACCOUNTS_FOR_DO_NOT_TRACK_P -1,-1, 10056, null , null
--exec dbo.GET_NOT_MANAGED_ACCOUNTS_FOR_DO_NOT_TRACK_P -1,-1, null, 12

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SP			Sandeep Pigilam

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	SP			2016-12-29	Comments header added removed no lock for the tables.And removed base tables and used Client_Hier
							Invoice tracking,@Is_Client_Config_Exists is added as select list.
	RKV         2020-01-16  Removed UBM_Columns
							
******/


CREATE PROCEDURE [dbo].[GET_NOT_MANAGED_ACCOUNTS_FOR_DO_NOT_TRACK_P]
      @userId VARCHAR(10)
     ,@sessionId VARCHAR(20)
     ,@clientId INT
     ,@division_id INT
     ,@site_id INT
AS 
BEGIN

      DECLARE @Is_Client_Config_Exists BIT= 0


      SELECT
            @Is_Client_Config_Exists = 1
      FROM
            dbo.Invoice_Collection_Client_Config icc
      WHERE
            icc.Client_Id = @clientId
                        

      SELECT
            cha.Account_Id
           ,cha.Account_Number
           ,ch.Client_Name
           ,ch.City
           ,ch.State_Id
           ,cha.Account_Vendor_Name AS vendor_name
           ,@Is_Client_Config_Exists AS Is_Client_Config_Exists
           ,cha.Client_Hier_Id
      FROM
            core.Client_Hier ch
            INNER JOIN core.Client_Hier_Account cha
                  ON ch.Client_Hier_Id = cha.Client_Hier_Id
      WHERE
            cha.Account_Type = 'Utility'
            AND ch.Site_Id > 0
            AND ( @division_id IS NULL
                  OR ch.Sitegroup_Id = @division_id )
            AND ( @clientId IS NULL
                  OR ch.Client_Id = @clientId )
            AND ( @site_id IS NULL
                  OR ch.Site_Id = @site_id )
      GROUP BY
            cha.Account_Id
           ,cha.Account_Number
           ,ch.Client_Name
           ,ch.City
           ,ch.State_Id
           ,cha.Account_Vendor_Name
           ,cha.Client_Hier_Id       


END


;
GO

GRANT EXECUTE ON  [dbo].[GET_NOT_MANAGED_ACCOUNTS_FOR_DO_NOT_TRACK_P] TO [CBMSApplication]
GO
