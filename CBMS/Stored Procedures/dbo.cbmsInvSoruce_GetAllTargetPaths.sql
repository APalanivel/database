SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[cbmsInvSoruce_GetAllTargetPaths]
	( @MyAccountId int )
AS
BEGIN

   select distinct target_folder_path
     from inv_source
    where inv_source_type_id = 1247
      and is_active = 1

END


GO
GRANT EXECUTE ON  [dbo].[cbmsInvSoruce_GetAllTargetPaths] TO [CBMSApplication]
GO
