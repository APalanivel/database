SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
CREATE              PROCEDURE DBO.UPDATE_DEAL_TICKET_VOLUME_TRIGGER_PRICE_P

@userId varchar(10),
@sessionId varchar(16),
@dealTicketDetailsId int, 
@triggerPrice decimal(32,16), 
@totalVolume decimal(32,16), 
@dealTicketStatus varchar(200), 
@triggerStatusId int, 
@triggerStatusName varchar(200), 
@cbmsImageId int, 
@isTriggerConfirmed bit, 
@dealTransactionDate datetime, 
@confirmationTypeId int 

AS
	set nocount on
--incase of individual month trigger
IF @triggerStatusId IS NOT NULL 
	BEGIN
		--incase of individual month  - Fixed Month
		IF @triggerStatusName = 'Fixed' OR @dealTicketStatus is NOT NULL
			BEGIN 
				IF @dealTransactionDate is NULL
					BEGIN 
						UPDATE RM_DEAL_TICKET_DETAILS 
						SET 	TOTAL_VOLUME = @totalVolume, 
							HEDGE_PRICE = @triggerPrice, 
							TRIGGER_STATUS_TYPE_ID = @triggerStatusId, 
							CBMS_IMAGE_ID = @cbmsImageId, 
							IS_TRIGGER_CONFIRMED = @isTriggerConfirmed, 
							CONFIRMATION_TYPE_ID = @confirmationTypeId
						WHERE RM_DEAL_TICKET_DETAILS_ID = @dealTicketDetailsId
					END
				ELSE 
					BEGIN
						UPDATE RM_DEAL_TICKET_DETAILS 
						SET 	TOTAL_VOLUME = @totalVolume, 
							HEDGE_PRICE = @triggerPrice, 
							TRIGGER_STATUS_TYPE_ID = @triggerStatusId, 
							TRIGGER_TRANSACTION_DATE = @dealTransactionDate,  
							CBMS_IMAGE_ID = @cbmsImageId, 
							IS_TRIGGER_CONFIRMED = @isTriggerConfirmed, 
							CONFIRMATION_TYPE_ID = @confirmationTypeId
						WHERE RM_DEAL_TICKET_DETAILS_ID = @dealTicketDetailsId				
					END
			END
		--incase of individual month - Trigger Deal 
		ELSE
			BEGIN
				IF @dealTransactionDate is NULL
					BEGIN 
						UPDATE RM_DEAL_TICKET_DETAILS 
						SET 	TOTAL_VOLUME = @totalVolume, 
							TRIGGER_PRICE = @triggerPrice, 
							TRIGGER_STATUS_TYPE_ID = @triggerStatusId, 
							CBMS_IMAGE_ID = @cbmsImageId, 
							IS_TRIGGER_CONFIRMED = @isTriggerConfirmed, 
							CONFIRMATION_TYPE_ID = @confirmationTypeId
						WHERE RM_DEAL_TICKET_DETAILS_ID = @dealTicketDetailsId
					END
				ELSE 
					BEGIN
						UPDATE RM_DEAL_TICKET_DETAILS 
						SET 	TOTAL_VOLUME = @totalVolume, 
							TRIGGER_PRICE = @triggerPrice, 
							TRIGGER_STATUS_TYPE_ID = @triggerStatusId, 
							TRIGGER_TRANSACTION_DATE = @dealTransactionDate, 
							CBMS_IMAGE_ID = @cbmsImageId, 
							IS_TRIGGER_CONFIRMED = @isTriggerConfirmed, 
							CONFIRMATION_TYPE_ID = @confirmationTypeId
						WHERE RM_DEAL_TICKET_DETAILS_ID = @dealTicketDetailsId
					END
			END
	END
--incase of weighted strip trigger and all other deal types
ELSE 
	BEGIN
		--incase of process flow other than order executed 	
		IF @dealTicketStatus IS NULL 
			BEGIN
				IF @dealTransactionDate is NULL
					BEGIN
						UPDATE RM_DEAL_TICKET_DETAILS 
						SET 	TOTAL_VOLUME = @totalVolume, 
							TRIGGER_PRICE = @triggerPrice 
						WHERE RM_DEAL_TICKET_DETAILS_ID = @dealTicketDetailsId
					END
				ELSE 
					BEGIN
						UPDATE RM_DEAL_TICKET_DETAILS 
						SET 	TOTAL_VOLUME = @totalVolume, 
							TRIGGER_PRICE = @triggerPrice,
							TRIGGER_TRANSACTION_DATE = @dealTransactionDate
						WHERE RM_DEAL_TICKET_DETAILS_ID = @dealTicketDetailsId
					END
			END
		--incase of order executed process flow
		ELSE
			BEGIN 
				IF @dealTransactionDate IS NULL
					BEGIN
						UPDATE RM_DEAL_TICKET_DETAILS 
						SET 	TOTAL_VOLUME = @totalVolume, 
							HEDGE_PRICE = @triggerPrice  
						WHERE RM_DEAL_TICKET_DETAILS_ID = @dealTicketDetailsId
					END
				ELSE
					BEGIN
						UPDATE RM_DEAL_TICKET_DETAILS 
						SET 	TOTAL_VOLUME = @totalVolume, 
							HEDGE_PRICE = @triggerPrice, 
							DEAL_TRANSACTION_DATE = @dealTransactionDate, 
							TRIGGER_TRANSACTION_DATE = @dealTransactionDate
						WHERE RM_DEAL_TICKET_DETAILS_ID = @dealTicketDetailsId
					END
			END
	END
GO
GRANT EXECUTE ON  [dbo].[UPDATE_DEAL_TICKET_VOLUME_TRIGGER_PRICE_P] TO [CBMSApplication]
GO
