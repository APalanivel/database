SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










/*********     
NAME:  dbo.Consumption_Level_Variance_Test_UPD    
   
DESCRIPTION:  Used to update all the consumption level data    
  
INPUT PARAMETERS:      
      Name              DataType          Default     Description      
------------------------------------------------------------      
@Variance_Rule_Id   Int   
@Variance_Rule_Dtl_Id  Int    
@Consumption_Level_Id  int  
@is_Data_Entry_Only   bit  
@is_Active     bit  
@Default_Tolerance   decimal(16,4)  
@Test_Description   varchar(255)      
      
OUTPUT PARAMETERS:      
      Name              DataType          Default     Description      
------------------------------------------------------------      
      
USAGE EXAMPLES:     
  
   
------------------------------------------------------------    
AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
NK   Nageswara Rao Kosuri
PD	 Padmanava Debnath	           
HG	 Harihara Suthan G 
AP	Arunkumar Palanivel 

Initials	Date		Modification
------------------------------------------------------------
NK			10/13/2009  Created
NK			12/07/2009  Modified to Change Name as per standards
NK			12/08/2009	Removed Commented code and removed update duplication
PD			02/02/2010	Put in a condition to prevent inserts into Account_Override table to fix the issue of a test switched
							off at the account level and then consumption level changes should not again insert this data and turn it on
HG			06/15/2011	MAINT-660 fixed the code to insert the records in to Variance_Rule_Dtl_Account_Override when test is switched on irrespective of any account manually overridden
							- Tolerence NOT NULL condition added in the existence which used in the else part of IF Loop.
AP			Feb 24,2020		Added exclusion condition in procedure to avoid adding avt rules at account level
AP		June 11,2020  https://summit.jira.com/browse/SE2017-996 Tax exclusion
******/

CREATE PROCEDURE [dbo].[Consumption_Level_Variance_Test_UPD]
      @Variance_Rule_Dtl_Id INT
    , @Consumption_Level_Id INT
    , @is_Data_Entry_Only   BIT
    , @is_Active            BIT
    , @Default_Tolerance    DECIMAL(16, 4) = NULL
    , @Test_Description     VARCHAR(255)   = NULL
AS
      BEGIN

            SET NOCOUNT ON;

            IF ( @is_Active = 0 )
                  BEGIN

                        DELETE      vrdac
                        FROM        dbo.Variance_Rule_Dtl_Account_Override vrdac
                                    JOIN
                                    dbo.Account_Variance_Consumption_Level avcl
                                          ON avcl.ACCOUNT_ID = vrdac.ACCOUNT_ID
                        WHERE       avcl.Variance_Consumption_Level_Id = @Consumption_Level_Id
                                    AND   vrdac.Variance_Rule_Dtl_Id = @Variance_Rule_Dtl_Id
                                    AND   vrdac.Tolerance IS NULL;

                  END;
            ELSE IF ( @is_Active = 1 )
                       BEGIN
                             IF NOT EXISTS
                                   (     SELECT
                                                1
                                         FROM   dbo.Variance_Rule_Dtl_Account_Override
                                         WHERE  Variance_Rule_Dtl_Id = @Variance_Rule_Dtl_Id
                                                AND   Tolerance IS NULL )
                                   BEGIN

                                         INSERT INTO  dbo.Variance_Rule_Dtl_Account_Override (
                                                                                                   Variance_Rule_Dtl_Id
                                                                                                 , ACCOUNT_ID
                                                                                             )
                                                      SELECT
                                                            @Variance_Rule_Dtl_Id
                                                          , avcl.ACCOUNT_ID
                                                      FROM  dbo.Account_Variance_Consumption_Level avcl
                                                            LEFT JOIN
                                                            dbo.Variance_Rule_Dtl_Account_Override vrdac
                                                                  ON avcl.ACCOUNT_ID = vrdac.ACCOUNT_ID
                                                                     AND vrdac.Variance_Rule_Dtl_Id = @Variance_Rule_Dtl_Id
                                                      WHERE avcl.Variance_Consumption_Level_Id = @Consumption_Level_Id
                                                            AND   vrdac.ACCOUNT_ID IS NULL
                                                            AND   NOT EXISTS
                                                            (     SELECT
                                                                        1
                                                                  FROM  Core.Client_Hier_Account cha
                                                                        JOIN
                                                                        dbo.Account_Variance_Consumption_Level avcl1
                                                                              ON avcl1.ACCOUNT_ID = cha.Account_Id
                                                                        JOIN
                                                                        dbo.Variance_Consumption_Level vcl
                                                                              ON vcl.Variance_Consumption_Level_Id = avcl1.Variance_Consumption_Level_Id
                                                                                 AND vcl.Commodity_Id = cha.Commodity_Id
                                                                        JOIN
                                                                        dbo.Bucket_Master bm
                                                                              ON bm.Commodity_Id = vcl.Commodity_Id
                                                                        JOIN
                                                                        dbo.Variance_category_Bucket_Map vcbm
                                                                              ON vcbm.Bucket_master_Id = bm.Bucket_Master_Id
                                                                        JOIN
                                                                        dbo.Variance_Parameter vp
                                                                              ON vp.Variance_Category_Cd = vcbm.Variance_Category_Cd
                                                                        JOIN
                                                                        dbo.Variance_Parameter_Baseline_Map vpbm
                                                                              ON vpbm.Variance_Parameter_Id = vp.Variance_Parameter_Id
                                                                        JOIN
                                                                        dbo.Variance_Rule vr
                                                                              ON vr.Variance_Baseline_Id = vpbm.Variance_Baseline_Id
                                                                        JOIN
                                                                        dbo.Variance_Rule_Dtl vrd
                                                                              ON vrd.Variance_Rule_Id = vr.Variance_Rule_Id
                                                                        JOIN
                                                                        dbo.Variance_Parameter_Commodity_Map vpcm
                                                                              ON vpcm.Variance_Parameter_Id = vp.Variance_Parameter_Id
                                                                        JOIN
                                                                        dbo.Variance_Processing_Engine vpe
                                                                              ON vpe.Variance_Processing_Engine_Id = vpcm.variance_process_Engine_id
                                                                        JOIN
                                                                        dbo.Code vpcat
                                                                              ON vpcat.Code_Id = vpe.Variance_Engine_Cd
                                                                        JOIN
                                                                        dbo.Variance_Consumption_Extraction_Service_Param_Value vesp
                                                                              ON vesp.Variance_Consumption_Level_Id = vcl.Variance_Consumption_Level_Id
                                                                  WHERE cha.Account_Id = avcl.ACCOUNT_ID
                                                                        AND   cha.Account_Type = 'Utility'
                                                                        AND   vpcat.Code_Value = 'Advanced Variance Test'
                                                                        AND   vrd.Variance_Rule_Dtl_Id = @Variance_Rule_Dtl_Id
                                                                        AND   vcl.Variance_Consumption_Level_Id = @Consumption_Level_Id )
                                                            AND   NOT EXISTS
                                                            (     SELECT
                                                                        1
                                                                  FROM  dbo.Variance_Rule vr
                                                                        JOIN
                                                                        dbo.Variance_Rule_Dtl vrd
                                                                              ON vrd.Variance_Rule_Id = vr.Variance_Rule_Id
                                                                        INNER JOIN
                                                                        dbo.Variance_Parameter_Baseline_Map vpbmap
                                                                              ON vr.Variance_Baseline_Id = vpbmap.Variance_Baseline_Id
                                                                        INNER JOIN
                                                                        dbo.Variance_Parameter vp
                                                                              ON vp.Variance_Parameter_Id = vpbmap.Variance_Parameter_Id
                                                                        INNER JOIN
                                                                        dbo.Code cd
                                                                              ON cd.Code_Id = vp.Variance_Category_Cd
                                                                        INNER JOIN
                                                                        dbo.Code cdb
                                                                              ON cdb.Code_Id = vpbmap.Baseline_Cd
                                                                        JOIN
                                                                        Core.Client_Hier_Account cha
                                                                              ON cha.Account_Id = avcl.ACCOUNT_ID
                                                                        JOIN
                                                                        dbo.Variance_Rules_Exclusion vre
                                                                              ON vre.Country_id = cha.Meter_Country_Id
                                                                                 AND vre.Baseline_Cd = cdb.Code_Id
                                                                                 AND vre.Category_id = cd.Code_Id
                                                                  WHERE vrd.Variance_Rule_Dtl_Id = @Variance_Rule_Dtl_Id );;
                                   END;
                       END;

            UPDATE
                  dbo.Variance_Rule_Dtl
            SET
                  IS_Active = @is_Active
                , Default_Tolerance = CASE WHEN @is_Active = 0
                                                 THEN Default_Tolerance
                                           ELSE  @Default_Tolerance
                                      END
                , Test_Description = CASE WHEN @is_Active = 0
                                                THEN Test_Description
                                          ELSE  @Test_Description
                                     END
                , Is_Data_Entry_Only = @is_Data_Entry_Only
            WHERE Variance_Rule_Dtl_Id = @Variance_Rule_Dtl_Id;

      END;
GO
GRANT EXECUTE ON  [dbo].[Consumption_Level_Variance_Test_UPD] TO [CBMSApplication]
GO