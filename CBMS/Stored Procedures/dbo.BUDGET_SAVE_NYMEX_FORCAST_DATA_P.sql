
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.BUDGET_SAVE_NYMEX_FORCAST_DATA_P

DESCRIPTION:


INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@Account_Price_Map	XML
	@commodityTypeId	INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
SELECT @Account_Price_Map = '
<root>
	<Acct val = "100" Nymex="Hedge">
		<Dtl Mnth="2012-01-01" Prc="23"/>
		<Dtl Mnth="2012-02-01" Prc="24"/>
		<Dtl Mnth="2012-03-01" Prc="25"/>
	</Acct>
	<Acct val = "200" Nymex="Manual">
		<Dtl Mnth="2012-01-01" Prc="26"/>
		<Dtl Mnth="2012-02-01" Prc="27"/>
		<Dtl Mnth="2012-03-01" Prc="28"/>
	</Acct>
</root>
'



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CPE			Chaitanya Panduga Eshwar

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	 CPE		2013-03-19	Modified the SP for commercial Budget.
	 
******/

CREATE PROC dbo.BUDGET_SAVE_NYMEX_FORCAST_DATA_P
      ( 
       @Account_Price_Map XML
      ,@commodityTypeId INT )
AS 
BEGIN
	
      SET NOCOUNT ON
	
      MERGE INTO dbo.BUDGET_NYMEX_FORECAST AS tgt
            USING 
                  ( SELECT
                        T.c.value('@val', 'INT') AS Account_Id
                       ,en.ENTITY_ID AS Nymex_Type_Id
                       ,c.c.value('@Mnth', 'DATE') AS Month_Identifier
                       ,c.c.value('@Prc', 'DECIMAL(32,16)') AS Price
                    FROM
                        @Account_Price_Map.nodes('/root/Acct') T ( c )
                        JOIN dbo.ENTITY en
                              ON T.c.value('@Nymex', 'VARCHAR(25)') = en.ENTITY_NAME
                                 AND en.ENTITY_DESCRIPTION = 'NYMEX_TYPE'
                        CROSS APPLY T.c.nodes('./Dtl') AS c ( c ) ) AS src
            ON tgt.ACCOUNT_ID = src.Account_Id
                  AND tgt.MONTH_IDENTIFIER = src.Month_Identifier
                  AND tgt.COMMODITY_TYPE_ID = @commodityTypeId
            WHEN MATCHED 
                  THEN UPDATE
                    SET 
                        tgt.NYMEX_TYPE_ID = src.Nymex_Type_Id
                       ,tgt.PRICE = src.Price
            WHEN NOT MATCHED 
                  THEN INSERT
                        ( 
                         ACCOUNT_ID
                        ,NYMEX_TYPE_ID
                        ,COMMODITY_TYPE_ID
                        ,MONTH_IDENTIFIER
                        ,PRICE )
                    VALUES
                        ( 
                         src.ACCOUNT_ID
                        ,src.Nymex_Type_Id
                        ,@commodityTypeId
                        ,MONTH_IDENTIFIER
                        ,PRICE );


END
;
GO

GRANT EXECUTE ON  [dbo].[BUDGET_SAVE_NYMEX_FORCAST_DATA_P] TO [CBMSApplication]
GO
