SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
                         
 NAME: dbo.Account_Valcal_Dtl_Sel_By_Contract                     
                          
 DESCRIPTION: To raise the Valcal Exception
       
		              
                          
 INPUT PARAMETERS:      
                         
 Name                  DataType          Default       Description      
--------------------------------------------------------------------
 @Contract_Id			INT        
                          
 OUTPUT PARAMETERS:      
                               
 Name                  DataType          Default       Description      
-------------------------------------------------------------------------------------                            
 USAGE EXAMPLES:                              
-------------------------------------------------------------------------------------                            


   EXEC dbo.Account_Valcal_Dtl_Sel_By_Contract  @Contract_Id  = 163963      
    
	                   
 AUTHOR INITIALS:    
       
 Initials                   Name      
-------------------------------------------------------------------------------------  
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
-------------------------------------------------------------------------------------  
 NR                     2019-06-13      Created for ADD Contract.                        
                         
******/

CREATE PROCEDURE [dbo].[Account_Consolidated_Recalc_Dtl_Sel_By_Contract_Account_Id]
    (
        @Account_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;


        SELECT
            acirt.Account_Id
            , acirt.Commodity_ID
            , acirt.Start_Dt
            , acirt.End_Dt
            , Invoice_Recalc_Type_Cd
            , vac.Is_Config_Complete
        FROM
            dbo.Account_Commodity_Invoice_Recalc_Type acirt
            INNER JOIN dbo.Valcon_Account_Config vac
                ON vac.Account_Id = acirt.Account_Id
        WHERE
            vac.Account_Id = @Account_Id
        GROUP BY
            acirt.Account_Id
            , acirt.Commodity_ID
            , acirt.Start_Dt
            , acirt.End_Dt
            , Invoice_Recalc_Type_Cd
            , vac.Is_Config_Complete;





    END;




GO
GRANT EXECUTE ON  [dbo].[Account_Consolidated_Recalc_Dtl_Sel_By_Contract_Account_Id] TO [CBMSApplication]
GO
