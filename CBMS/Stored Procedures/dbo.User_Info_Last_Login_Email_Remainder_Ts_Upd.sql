SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/******          
       
 NAME:  User_Info_Last_Login_Email_Remainder_Ts_Upd

 DESCRIPTION:         
   
   Updates Last_Login_Email_Remainder_Ts of a User
           
   
 INPUT PARAMETERS:          
   
  Name				DataType		Default						Description  
 -------------------------------------------------------------------------------------------------------
  @User_Info_Id		INT
  @Last_Login_Email_Remainder_Ts DATETIME	0		
           
   
 OUTPUT PARAMETERS:          
   
  Name				DataType		Default						Description          
   
 -------------------------------------------------------------------------------------------------------
             
   
 USAGE EXAMPLES:          
 -------------------------------------------------------------------------------------------------------
   
 
 
   
 AUTHOR INITIALS:          
   
  Initials	Name          
 -------------------------------------------------------------------------------------------------------
  HK		Harish Kurma
   
 MODIFICATIONS          
   
  Initials	Date		Modification          
   
-------------------------------------------------------------------------------------------------------

***************************************************************************************************************************/

CREATE PROCEDURE [dbo].[User_Info_Last_Login_Email_Remainder_Ts_Upd]
      (
       @User_Info_Id INT
      ,@Last_Login_Email_Remainder_Ts DATETIME )
AS
BEGIN
             
      SET NOCOUNT ON
      
      UPDATE
            dbo.USER_INFO
      SET
            last_Login_Email_Reminder_Ts = @Last_Login_Email_Remainder_Ts
      WHERE
            USER_INFO_ID = @User_Info_Id

END
GO
GRANT EXECUTE ON  [dbo].[User_Info_Last_Login_Email_Remainder_Ts_Upd] TO [CBMSApplication]
GO
