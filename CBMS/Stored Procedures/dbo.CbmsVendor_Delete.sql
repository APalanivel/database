SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/******

NAME:	dbo.[CbmsVendor_Delete]

DESCRIPTION: 

INPUT PARAMETERS:    
    Name				DataType          Default     Description    
---------------------------------------------------------------------------------    
	@MyAccountId		INT
	@VendorId			INT
        
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    

USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN
		EXEC dbo.cbmsVendor_Delete 49, 3835
	ROLLBACK TRAN

	SELECT * FROM APPLICATION_AUDIT_LOG WHERE TABLE_NAME = 'Vendor'
	SELECT * FROM dbo.vendor where vendor_id = 3835

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DR			Deana Ritter
	PNR			Pandarinath
	HG			Harihara Suthan G

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	DR			08/04/2009	Removed Linked Server Updates
	PNR			08/17/2010	Removed Subqueries and rewritten code as per db standards and script added to log the deleted record in to Application_Audit_Log.
	HG			09/24/2010	Script added to remove the entries Utility_Budget_Comments , Utility_Profile table and SSo_Savings History.
	HG			05/05/2011	MAINT-596 Fixes the code to delete the utility successfully.
								-- Removed the scripts added to remove the records from Utility_Addition_Detail table as this table was dropped as a part of Summary and highlights enhancement
								-- Created a new SP Utility_Dtl_History_Del_By_Vendor_id and called it from this SP to remove the records from Utlity_Dtl_* tables which is used to stored the utility Summary and highlights details.
								-- Removed the unused variables @Client_Name, @Utility_Additional_Detail_Id and @Vendor_Commodity_Map_id
	RKV         2018-06-22  MAINT-7400  Added the code to delete data from the Account_Group

******/

CREATE PROCEDURE [dbo].[CbmsVendor_Delete]
     (
         @MyAccountId INT
         , @VendorId INT
     )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Application_Name VARCHAR(30) = 'Delete Vendor'
            , @Audit_Function SMALLINT = -1
            , @User_Name VARCHAR(81)
            , @Current_Ts DATETIME
            , @Table_Name VARCHAR(10) = 'VENDOR'
            , @Lookup_Value XML;

        DECLARE
            @Commodity_Type_Id INT
            , @State_Id INT
            , @utility_detail_id INT
            , @Sr_Load_Profile_Default_Setup_Id INT
            , @Entity_Id INT
            , @Sr_Load_Profile_Determinant_Id INT
            , @Sr_Load_Profile_Additional_Row_Id INT
            , @Utility_Detail_Analyst_Map_ID INT
            , @Vendor_Commodity_Analyst_Map_Id INT
            , @Utility_Budget_Comments_Id INT
            , @Account_Group_Id INT;

        DECLARE @vendor_commodity_map_list TABLE
              (
                  Commodity_Type_Id INT
                  , Vendor_Commodity_Map_Id INT
              );
        DECLARE @Account_Group_List TABLE
              (
                  Account_Group_Id INT
              );
        DECLARE @Vendor_Commodity_Analyst_Map_list TABLE
              (
                  Vendor_Commodity_Analyst_Map_Id INT
              );
        DECLARE @vendor_state_map_list TABLE
              (
                  State_Id INT
              );
        DECLARE @Utility_Detail_Analyst_Map_list TABLE
              (
                  Utility_Detail_Analyst_Map_ID INT
              );
        DECLARE @Utility_detail_list TABLE
              (
                  Utility_Detail_Id INT
              );
        DECLARE @Sr_load_profile_determinant_list TABLE
              (
                  Sr_Load_Profile_Determinant_Id INT
              );
        DECLARE @Sr_load_profile_additional_row_list TABLE
              (
                  Sr_Load_Profile_Additional_Row_Id INT
              );
        DECLARE @Sr_load_profile_default_setup_list TABLE
              (
                  Sr_Load_Profile_Default_Setup_Id INT
              );
        DECLARE @Utility_Budget_Comments_list TABLE
              (
                  Utility_Budget_Comments_Id INT
              );
        DECLARE @Utility_Profile_list TABLE
              (
                  Commodity_Type_Id INT
              );

        SET @Current_Ts = GETDATE();

        SET @Lookup_Value = (   SELECT
                                    VENDOR.VENDOR_ID
                                    , VENDOR_NAME
                                    , ENTITY_NAME Vendory_Type
                                    , STATE_NAME
                                FROM
                                    dbo.VENDOR
                                    JOIN dbo.ENTITY
                                        ON ENTITY_ID = VENDOR_TYPE_ID
                                    JOIN dbo.VENDOR_STATE_MAP
                                        ON VENDOR_STATE_MAP.VENDOR_ID = VENDOR.VENDOR_ID
                                    JOIN dbo.STATE
                                        ON STATE.STATE_ID = VENDOR_STATE_MAP.STATE_ID
                                WHERE
                                    VENDOR.VENDOR_ID = @VendorId
                                FOR XML RAW);

        SELECT
            @Entity_Id = ENTITY_ID
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_NAME = 'UTILITY_TABLE'
            AND ENTITY_DESCRIPTION = 'Table_Type';

        SELECT
            @User_Name = FIRST_NAME + SPACE(1) + LAST_NAME
        FROM
            dbo.USER_INFO
        WHERE
            USER_INFO_ID = @MyAccountId;

        INSERT INTO @Account_Group_List
             (
                 Account_Group_Id
             )
        SELECT
            ag.ACCOUNT_GROUP_ID
        FROM
            dbo.ACCOUNT_GROUP ag
        WHERE
            ag.VENDOR_ID = @VendorId;



        INSERT INTO @vendor_commodity_map_list
             (
                 Commodity_Type_Id
                 , Vendor_Commodity_Map_Id
             )
        SELECT
            COMMODITY_TYPE_ID
            , VENDOR_COMMODITY_MAP_ID
        FROM
            dbo.VENDOR_COMMODITY_MAP
        WHERE
            VENDOR_ID = @VendorId;

        INSERT INTO @Vendor_Commodity_Analyst_Map_list
             (
                 Vendor_Commodity_Analyst_Map_Id
             )
        SELECT
            vcam.Vendor_Commodity_Analyst_Map_Id
        FROM
            dbo.Vendor_Commodity_Analyst_Map vcam
            JOIN @vendor_commodity_map_list vcm
                ON vcm.Vendor_Commodity_Map_Id = vcam.Vendor_Commodity_Map_Id;

        INSERT INTO @vendor_state_map_list
             (
                 State_Id
             )
        SELECT  STATE_ID FROM   dbo.VENDOR_STATE_MAP WHERE  VENDOR_ID = @VendorId;

        INSERT INTO @Utility_detail_list
             (
                 Utility_Detail_Id
             )
        SELECT  UTILITY_DETAIL_ID FROM  dbo.UTILITY_DETAIL WHERE VENDOR_ID = @VendorId;

        INSERT INTO @Utility_Detail_Analyst_Map_list
             (
                 Utility_Detail_Analyst_Map_ID
             )
        SELECT
            map.Utility_Detail_Analyst_Map_ID
        FROM
            dbo.Utility_Detail_Analyst_Map map
            JOIN @Utility_detail_list ud
                ON map.Utility_Detail_ID = ud.Utility_Detail_Id;


        INSERT INTO @Sr_load_profile_determinant_list
             (
                 Sr_Load_Profile_Determinant_Id
             )
        SELECT
            sr_load_prof_det.SR_LOAD_PROFILE_DETERMINANT_ID
        FROM
            dbo.SR_LOAD_PROFILE_DETERMINANT sr_load_prof_det
            JOIN dbo.SR_LOAD_PROFILE_DEFAULT_SETUP sr_load_prof
                ON sr_load_prof.SR_LOAD_PROFILE_DEFAULT_SETUP_ID = sr_load_prof_det.SR_LOAD_PROFILE_DEFAULT_SETUP_ID
        WHERE
            sr_load_prof.VENDOR_ID = @VendorId;

        INSERT INTO @Sr_load_profile_additional_row_list
             (
                 Sr_Load_Profile_Additional_Row_Id
             )
        SELECT
            sr_load_prof_ad.SR_LOAD_PROFILE_DEFAULT_SETUP_ID
        FROM
            dbo.SR_LOAD_PROFILE_ADDITIONAL_ROW sr_load_prof_ad
            JOIN dbo.SR_LOAD_PROFILE_DEFAULT_SETUP sr_load_prof_def
                ON sr_load_prof_ad.SR_LOAD_PROFILE_DEFAULT_SETUP_ID = sr_load_prof_def.SR_LOAD_PROFILE_DEFAULT_SETUP_ID
        WHERE
            sr_load_prof_def.VENDOR_ID = @VendorId;

        INSERT INTO @Sr_load_profile_default_setup_list
             (
                 Sr_Load_Profile_Default_Setup_Id
             )
        SELECT
            SR_LOAD_PROFILE_DEFAULT_SETUP_ID
        FROM
            dbo.SR_LOAD_PROFILE_DEFAULT_SETUP
        WHERE
            VENDOR_ID = @VendorId;

        INSERT INTO @Utility_Budget_Comments_list
             (
                 Utility_Budget_Comments_Id
             )
        SELECT
            UTILITY_BUDGET_COMMENTS_ID
        FROM
            dbo.UTILITY_BUDGET_COMMENTS
        WHERE
            VENDOR_ID = @VendorId;

        INSERT INTO @Utility_Profile_list
             (
                 Commodity_Type_Id
             )
        SELECT  COMMODITY_TYPE_ID FROM  dbo.UTILITY_PROFILE WHERE   VENDOR_ID = @VendorId;

        BEGIN TRY
            BEGIN TRAN;

            EXEC dbo.Utility_Dtl_History_Del_By_Vendor_Id @Vendor_Id = @VendorId;

            WHILE EXISTS (SELECT    1 FROM  @Vendor_Commodity_Analyst_Map_list)
                BEGIN

                    SET @Vendor_Commodity_Analyst_Map_Id = (   SELECT   TOP 1
                                                                        Vendor_Commodity_Analyst_Map_Id
                                                               FROM
                                                                    @Vendor_Commodity_Analyst_Map_list);

                    EXEC dbo.Vendor_Commodity_Analyst_Map_Del @Vendor_Commodity_Analyst_Map_Id;

                    DELETE  FROM
                    @Vendor_Commodity_Analyst_Map_list
                    WHERE
                        Vendor_Commodity_Analyst_Map_Id = @Vendor_Commodity_Analyst_Map_Id;

                END;

            WHILE EXISTS (SELECT    1 FROM  @vendor_commodity_map_list)
                BEGIN

                    SELECT  TOP 1
                            @Commodity_Type_Id = Commodity_Type_Id
                    FROM
                        @vendor_commodity_map_list;

                    EXEC dbo.Vendor_Commodity_Map_Del @VendorId, @Commodity_Type_Id;

                    DELETE  FROM
                    @vendor_commodity_map_list
                    WHERE
                        Commodity_Type_Id = @Commodity_Type_Id;

                END;

            WHILE EXISTS (SELECT    1 FROM  @vendor_state_map_list)
                BEGIN
                    SET @State_Id = (SELECT TOP 1   State_Id FROM   @vendor_state_map_list);

                    EXEC dbo.Vendor_State_Map_Del @VendorId, @State_Id;

                    DELETE  FROM @vendor_state_map_list WHERE   State_Id = @State_Id;

                END;

            WHILE EXISTS (SELECT    1 FROM  @Utility_Detail_Analyst_Map_list)
                BEGIN
                    SET @Utility_Detail_Analyst_Map_ID = (   SELECT TOP 1
                                                                    Utility_Detail_Analyst_Map_ID
                                                             FROM
                                                                    @Utility_Detail_Analyst_Map_list);

                    EXEC dbo.Utility_Detail_Analyst_Map_Del @Utility_Detail_Analyst_Map_ID;

                    DELETE  FROM
                    @Utility_Detail_Analyst_Map_list
                    WHERE
                        Utility_Detail_Analyst_Map_ID = @Utility_Detail_Analyst_Map_ID;

                END;

            WHILE EXISTS (SELECT    1 FROM  @Utility_detail_list)
                BEGIN
                    SET @utility_detail_id = (SELECT    TOP 1  Utility_Detail_Id FROM   @Utility_detail_list);

                    EXEC dbo.Utility_Detail_Del @utility_detail_id;

                    DELETE  FROM @Utility_detail_list WHERE Utility_Detail_Id = @utility_detail_id;

                END;

            WHILE EXISTS (SELECT    1 FROM  @Utility_Budget_Comments_list)
                BEGIN

                    SET @Utility_Budget_Comments_Id = (SELECT   TOP 1   Utility_Budget_Comments_Id FROM @Utility_Budget_Comments_list);

                    EXEC dbo.Utility_Budget_Comments_Del @Utility_Budget_Comments_Id;

                    DELETE  FROM
                    @Utility_Budget_Comments_list
                    WHERE
                        Utility_Budget_Comments_Id = @Utility_Budget_Comments_Id;

                END;

            SET @Commodity_Type_Id = NULL;
            WHILE EXISTS (SELECT    1 FROM  @Utility_Profile_list)
                BEGIN

                    SET @Commodity_Type_Id = (SELECT    TOP 1  Commodity_Type_Id FROM   @Utility_Profile_list);

                    EXEC dbo.Utility_Profile_Del @VendorId, @Commodity_Type_Id;

                    DELETE  FROM
                    @Utility_Profile_list
                    WHERE
                        Commodity_Type_Id = @Commodity_Type_Id;

                END;

            WHILE EXISTS (SELECT    1 FROM  @Sr_load_profile_determinant_list)
                BEGIN

                    SET @Sr_Load_Profile_Determinant_Id = (   SELECT    TOP 1
                                                                        Sr_Load_Profile_Determinant_Id
                                                              FROM
                                                                    @Sr_load_profile_determinant_list);

                    EXEC dbo.Sr_Load_Profile_Determinant_Del @Sr_Load_Profile_Determinant_Id;

                    DELETE  FROM
                    @Sr_load_profile_determinant_list
                    WHERE
                        Sr_Load_Profile_Determinant_Id = @Sr_Load_Profile_Determinant_Id;

                END;

            WHILE EXISTS (SELECT    1 FROM  @Sr_load_profile_additional_row_list)
                BEGIN
                    SET @Sr_Load_Profile_Additional_Row_Id = (   SELECT TOP 1
                                                                        Sr_Load_Profile_Additional_Row_Id
                                                                 FROM
                                                                        @Sr_load_profile_additional_row_list);

                    EXEC dbo.Sr_Load_Profile_Additional_Row_Del
                        @Sr_Load_Profile_Additional_Row_Id;

                    DELETE  FROM
                    @Sr_load_profile_additional_row_list
                    WHERE
                        Sr_Load_Profile_Additional_Row_Id = @Sr_Load_Profile_Additional_Row_Id;

                END;

            WHILE EXISTS (SELECT    1 FROM  @Sr_load_profile_default_setup_list)
                BEGIN
                    SET @Sr_Load_Profile_Default_Setup_Id = (   SELECT  TOP 1
                                                                        Sr_Load_Profile_Default_Setup_Id
                                                                FROM
                                                                    @Sr_load_profile_default_setup_list);

                    EXEC dbo.Sr_Load_Profile_Default_Setup_Del @Sr_Load_Profile_Default_Setup_Id;

                    DELETE  FROM
                    @Sr_load_profile_default_setup_list
                    WHERE
                        Sr_Load_Profile_Default_Setup_Id = @Sr_Load_Profile_Default_Setup_Id;

                END;

            WHILE EXISTS (SELECT    1 FROM  @Account_Group_List)
                BEGIN
                    SET @Account_Group_Id = (SELECT TOP 1   Account_Group_Id FROM   @Account_Group_List);


                    DELETE  FROM dbo.ACCOUNT_GROUP WHERE ACCOUNT_GROUP_ID = @Account_Group_Id;


                    DELETE  FROM @Account_Group_List WHERE  Account_Group_Id = @Account_Group_Id;

                END;

            EXEC dbo.Sso_Savings_History_Del_By_Vendor_Id @VendorId;

            EXEC dbo.Entity_Audit_Del_By_Entity_Id_Entity_Identifier
                @Entity_Id
                , @VendorId;

            EXEC dbo.Vendor_Del @VendorId;

            EXEC dbo.Application_Audit_Log_Ins
                @Client_Name = NULL
                , @Application_Name = @Application_Name
                , @Audit_Function = @Audit_Function
                , @Table_Name = @Table_Name
                , @Lookup_Value = @Lookup_Value
                , @Event_By_User = @User_Name
                , @Execution_Ts = @Current_Ts;

            COMMIT TRAN;
        END TRY
        BEGIN CATCH

            IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRAN;
                END;

            EXEC dbo.usp_RethrowError;

        END CATCH;
    END;



GO
GRANT EXECUTE ON  [dbo].[CbmsVendor_Delete] TO [CBMSApplication]
GO