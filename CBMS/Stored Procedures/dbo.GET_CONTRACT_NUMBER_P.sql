SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
		dbo.GET_CONTRACT_NUMBER_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
    @userId			VARCHAR
	@sessionId		VARCHAR
	@siteId			INTEGER
	@DCstartDate	DATETIME
	@DCendDate		DATETIME     	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

exec GET_CONTRACT_NUMBER_P 1,1,2818,'1-1-2002','1-1-2005'  

exec GET_CONTRACT_NUMBER_P 1,1,25001,'1-1-2007','1-1-2008'  

exec GET_CONTRACT_NUMBER_P 1,1, 25861,'1-1-2008','1-1-2009'  

exec GET_CONTRACT_NUMBER_P 1,1, 25846,'10-1-2008','1-1-2009'  

exec GET_CONTRACT_NUMBER_P 1,1,25035,'1-1-2008','1-1-2009'  
  
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SS			Subhash Subramanyam

MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	        	04/28/2009	Autogenerated script
	 SS       	08/31/2009	Included contract_id in place of account_id to join contract with supplier_account_meter_map

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.GET_CONTRACT_NUMBER_P
  @userId AS VARCHAR
, @sessionId AS VARCHAR
, @siteId AS INTEGER
, @DCstartDate AS DATETIME
, @DCendDate AS DATETIME
AS 

BEGIN
SET  NOCOUNT ON
    
SELECT DISTINCT
            con.CONTRACT_ID
          , con.ED_CONTRACT_NUMBER
          , con.CONTRACT_START_DATE
          , con.CONTRACT_END_DATE
FROM        dbo.SITE AS sit
INNER JOIN  dbo.ACCOUNT AS utilacc
ON          sit.SITE_ID = utilacc.SITE_ID
INNER JOIN  dbo.METER AS met
ON          utilacc.ACCOUNT_ID = met.ACCOUNT_ID
INNER JOIN  dbo.SUPPLIER_ACCOUNT_METER_MAP AS map
ON          met.METER_ID = map.METER_ID
INNER JOIN  dbo.ACCOUNT AS suppacc
ON          map.ACCOUNT_ID = suppacc.ACCOUNT_ID
INNER JOIN  dbo.CONTRACT AS con
ON          con.contract_id = map.contract_id
WHERE       ( sit.SITE_ID = @siteId ) AND
            ( con.COMMODITY_TYPE_ID <> ( SELECT      ENTITY_ID
                                         FROM        ENTITY
                                         WHERE       ( ENTITY_TYPE = 157 ) AND
                                                     ( ENTITY_NAME = 'Electric Power' ) ) )
ORDER BY    con.CONTRACT_ID DESC
                
END
GO
GRANT EXECUTE ON  [dbo].[GET_CONTRACT_NUMBER_P] TO [CBMSApplication]
GO
