SET NUMERIC_ROUNDABORT OFF 
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******
NAME:
	dbo.cbmsSsoProject_GetNewProjects

DESCRIPTION:


INPUT PARAMETERS:
	Name							DataType		Default	Description
------------------------------------------------------------
	@commodity_type_id				int       	null      	
	@project_status_type_id			int       	null      	
	@savings_probability_type_id	int       	null      	
	@client_id     					int       	null      	
	@division_id   					int       	null      	
	@site_id       					int       	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
EXEC dbo.cbmsSsoProject_GetNewProjects
EXEC dbo.cbmsSsoProject_GetNewProjects @Client_Id = 207


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CPE			Chaitanya Panduga Eshwar

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	CPE			03/28/2011	Replaced vwCbmsSSOProjectOwnerFlat with SSO_PROJECT_OWNER_MAP table. 
							Replaced vwSsoProject_LastActivity with the base tables.
******/

CREATE PROCEDURE dbo.cbmsSsoProject_GetNewProjects
( 
 @commodity_type_id int = null
,@project_status_type_id int = null
,@savings_probability_type_id int = null
,@client_id int = null
,@division_id int = null
,@site_id int = null )
AS 
BEGIN
	/*
		Need owners for two reasons
			1) Find distinct projects this user can see
			2) Return all owners of this project as part of RS
	*/

      SET NOCOUNT ON

      SELECT DISTINCT
            SP.SSO_PROJECT_ID
           ,CASE CD.Code_Value
              WHEN 'Corporate' THEN CH.Client_Id
              WHEN 'Division' THEN CH.Sitegroup_Id
              WHEN 'Site' THEN CH.Site_Id
            END AS Owner_Id
           ,CASE CD.Code_Value
              WHEN 'Corporate' THEN CH.Client_Name
              WHEN 'Division' THEN CH.Sitegroup_Name
              WHEN 'Site' THEN RTRIM(CH.City) + ', ' + CH.State_Name + ' (' + CH.Site_name + ')'
            END AS Owner_Name
           ,Cd.Display_Seq AS Owner_Sort
           ,SP.PROJECT_TITLE
           ,SP.PROJECT_DESCRIPTION
           ,SP.PROJECT_STATUS_TYPE_ID
           ,PST.ENTITY_NAME Project_Status_Type
           ,SP.COMMODITY_TYPE_ID
           ,COM.ENTITY_NAME commodity_type
           ,SP.START_DATE
           ,SP.PROJECTED_END_DATE
           ,SP.PROJECT_OWNERSHIP_TYPE_ID
           ,POT.ENTITY_NAME project_ownership_type
           ,SP.IS_URGENT
           ,PA.SSO_PROJECT_ACTIVITY_ID
           ,PA.ACTIVITY_DATE
           ,PA.ACTIVITY_DESCRIPTION
      FROM
            dbo.SSO_PROJECT SP
            JOIN dbo.ENTITY PST
                  on PST.ENTITY_ID = SP.PROJECT_STATUS_TYPE_ID
            JOIN dbo.ENTITY COM
                  on COM.ENTITY_ID = SP.COMMODITY_TYPE_ID
            JOIN dbo.ENTITY POT
                  on POT.ENTITY_ID = SP.PROJECT_OWNERSHIP_TYPE_ID
            JOIN ( SELECT
                        SSO_PROJECT_ID
                   FROM
                        dbo.SSO_PROJECT_OWNER_MAP SPOM
                        JOIN Core.Client_Hier CHI
                              ON SPOM.Client_Hier_Id = CHI.Client_Hier_Id
                   WHERE
                        ( @client_id IS NULL
                          OR @client_id = CHI.Client_Id )
                        AND ( @division_id IS NULL
                              OR @division_id = CHI.Sitegroup_Id )
                        AND ( @site_id IS NULL
                              OR @Site_Id = CHI.Site_Id ) 
                    GROUP BY
						spom.SSO_PROJECT_ID
                    ) acc
                  ON acc.SSO_PROJECT_ID = SP.SSO_PROJECT_ID
            JOIN dbo.SSO_PROJECT_OWNER_MAP OWN
                  ON OWN.SSO_PROJECT_ID = SP.SSO_PROJECT_ID
            JOIN Core.Client_Hier CH
                  ON OWN.Client_Hier_Id = CH.Client_Hier_Id
            JOIN dbo.Code CD
                  ON CH.Hier_level_Cd = CD.Code_Id
            LEFT JOIN ( SELECT
                              SSO_PROJECT_ID
                             ,MAX(SSO_PROJECT_ACTIVITY_ID) SSO_PROJECT_ACTIVITY_ID
                        FROM
                              dbo.SSO_PROJECT_ACTIVITY
                        GROUP BY
                              SSO_PROJECT_ID ) SPA
                  ON SP.SSO_PROJECT_ID = SPA.SSO_PROJECT_ID
            LEFT JOIN dbo.SSO_PROJECT_ACTIVITY PA
                  ON PA.SSO_PROJECT_ACTIVITY_ID = SPA.SSO_PROJECT_ACTIVITY_ID
      WHERE
            DATEDIFF(day, SP.START_DATE, getdate()) <= 60
      ORDER BY
            SP.PROJECT_TITLE
           ,SP.SSO_PROJECT_ID
           ,owner_sort
           ,owner_name
END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoProject_GetNewProjects] TO [CBMSApplication]
GO
