SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                          
 NAME: dbo.Account_Invoice_Collection_Frequency_Global_Dtl_Sel              
                          
 DESCRIPTION:                          
   To get the details of invoice collection account level Frequency_And_Expected_Dtl_Sel config               
                          
 INPUT PARAMETERS:            
                       
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
@Invoice_Collection_Account_Config_Id INT  NULL   
                                
 OUTPUT PARAMETERS:            
                             
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
                          
 USAGE EXAMPLES:                              
---------------------------------------------------------------------------------------------------------------                              
      
  
    
    
   EXEC dbo.Account_Invoice_Collection_Frequency_Global_Dtl_Sel  
      @Invoice_Collection_Account_Config_Id = 109205      
  
                         
 AUTHOR INITIALS:          
         
 Initials              Name          
---------------------------------------------------------------------------------------------------------------                        
 RKV                  Ravi Kumar Vegesna  
                           
 MODIFICATIONS:        
            
 Initials              Date             Modification        
---------------------------------------------------------------------------------------------------------------        
    RKV    2017-01-25  Created For Invoice_Collection. 
	RKV    2020-04-16  Removed the logic to get the global configs if the frequency is not available.          
                       
******/
CREATE PROCEDURE [dbo].[Account_Invoice_Collection_Frequency_Global_Dtl_Sel]
     (
         @Invoice_Collection_Account_Config_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;


        DECLARE
            @Client_Id INT
            , @Additional_Invoice_Cnt INT
            , @Loop_Cnt INT = 0
            , @Invoice_Frequency_Monthly_Cd INT
            , @bimonthly INT
            , @quarterly INT
            , @biannual INT
            , @annual INT
            , @monthly INT
            , @bi_weekly INT
            , @Custom INT
            , @Custom_frq_1_9_Cd INT
            , @Custom_frq_Custom_Days_Cd INT;

        SELECT
            @Custom_frq_1_9_Cd = MAX(CASE WHEN c.Code_Value = '1-9 Days' THEN c.Code_Id
                                     END)
            , @Custom_frq_Custom_Days_Cd = MAX(CASE WHEN c.Code_Value = 'Custom Days' THEN c.Code_Id
                                               END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'FrequencyPattern'
            AND c.Code_Value IN ( '1-9 Days', 'Custom Days' );

        SELECT
            @bimonthly = MAX(CASE WHEN c.Code_Value = 'bi-monthly' THEN c.Code_Id
                             END)
            , @quarterly = MAX(CASE WHEN c.Code_Value = 'quarterly' THEN c.Code_Id
                               END)
            , @biannual = MAX(CASE WHEN c.Code_Value = 'bi-annual' THEN c.Code_Id
                              END)
            , @annual = MAX(CASE WHEN c.Code_Value = 'annual' THEN c.Code_Id
                            END)
            , @monthly = MAX(CASE WHEN c.Code_Value = 'monthly' THEN c.Code_Id
                             END)
            , @bi_weekly = MAX(CASE WHEN c.Code_Value = 'bi-weekly' THEN c.Code_Id
                               END)
            , @Custom = MAX(CASE WHEN c.Code_Value = 'Custom' THEN c.Code_Id
                            END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'InvoiceFrequency'
            AND c.Code_Value IN ( 'bi-monthly', 'quarterly', 'bi-annual', 'annual', 'monthly', 'Custom', 'bi-weekly' );


        SELECT
            @Client_Id = ch.Client_Id
        FROM
            dbo.Invoice_Collection_Account_Config icac
            INNER JOIN Core.Client_Hier_Account cha
                ON icac.Account_Id = cha.Account_Id
            INNER JOIN Core.Client_Hier ch
                ON cha.Client_Hier_Id = ch.Client_Hier_Id
        WHERE
            icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id;


        SELECT
            @Invoice_Frequency_Monthly_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            c.Code_Value = 'Monthly'
            AND cs.Std_Column_Name = 'Invoice_Frequency_Cd';


        SELECT
            @Additional_Invoice_Cnt = ISNULL(icac.Additional_Invoices_Per_Period, 0)
        FROM
            dbo.Invoice_Collection_Account_Config icac
        WHERE
            icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id;

        CREATE TABLE #Account_Invoice_Collection_Frequency
             (
                 Seq_No SMALLINT
                 , Is_Primary BIT
                 , Invoice_Frequency_Cd INT
                 , Invoice_Frequency_Pattern_Cd INT
                 , Expected_Invoice_Raised_Day TINYINT
                 , Expected_Invoice_Received_Day TINYINT
                 , Expected_Invoice_Raised_Month TINYINT
                 , Expected_Invoice_Received_Month TINYINT
                 , Expected_Invoice_Raised_Day_Lag TINYINT
                 , Expected_Invoice_Received_Day_Lag TINYINT
                 , Account_Invoice_Collection_Frequency_Id INT
                 , Invoice_Collection_Global_Config_Value_Id INT
                 , Custom_days INT
                 , Custom_Invoice_Received_day INT
                 , Next_Config_Run_Date DATE
             );

        WHILE @Loop_Cnt <= @Additional_Invoice_Cnt
            BEGIN

                INSERT INTO #Account_Invoice_Collection_Frequency
                     (
                         Seq_No
                         , Is_Primary
                         , Invoice_Frequency_Cd
                         , Invoice_Frequency_Pattern_Cd
                         , Expected_Invoice_Raised_Day
                         , Expected_Invoice_Received_Day
                         , Expected_Invoice_Raised_Month
                         , Expected_Invoice_Received_Month
                         , Expected_Invoice_Raised_Day_Lag
                         , Expected_Invoice_Received_Day_Lag
                         , Account_Invoice_Collection_Frequency_Id
                         , Invoice_Collection_Global_Config_Value_Id
                         , Custom_days
                         , Custom_Invoice_Received_day
                         , Next_Config_Run_Date
                     )
                SELECT
                    aicf.Seq_No
                    , aicf.Is_Primary
                    , aicf.Invoice_Frequency_Cd
                    , aicf.Invoice_Frequency_Pattern_Cd
                    , aicf.Expected_Invoice_Raised_Day
                    , aicf.Expected_Invoice_Received_Day
                    , aicf.Expected_Invoice_Raised_Month
                    , aicf.Expected_Invoice_Received_Month
                    , aicf.Expected_Invoice_Raised_Day_Lag
                    , aicf.Expected_Invoice_Received_Day_Lag
                    , aicf.Account_Invoice_Collection_Frequency_Id
                    , NULL
                    , aicf.Custom_Days
                    , aicf.Custom_Invoice_Received_Day
                    , aicf.Next_Config_Run_Date
                FROM
                    dbo.Account_Invoice_Collection_Frequency aicf
                    INNER JOIN dbo.Invoice_Collection_Account_Config icac
                        ON aicf.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                    INNER JOIN Code cp
                        ON cp.Code_Id = icac.Chase_Priority_Cd
                WHERE
                    aicf.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
                    AND aicf.Seq_No = @Loop_Cnt
                    AND aicf.Invoice_Frequency_Cd <> 0
                    AND 1 = CASE WHEN cp.Code_Value = 'Day Expected'
                                      AND   aicf.Invoice_Frequency_Cd IN ( @bimonthly, @quarterly, @biannual, @annual )
                                      AND   (   aicf.Expected_Invoice_Received_Day <> 0
                                                OR  aicf.Expected_Invoice_Received_Day IS NOT NULL)
                                      AND   aicf.Expected_Invoice_Received_Month IS NOT NULL THEN 1
                                WHEN cp.Code_Value = 'Day Expected'
                                     AND aicf.Invoice_Frequency_Cd IN ( @monthly, @bi_weekly )
                                     AND (   aicf.Expected_Invoice_Received_Day <> 0
                                             OR aicf.Expected_Invoice_Received_Day IS NOT NULL) THEN 1
                                WHEN cp.Code_Value = 'Day Issued'
                                     AND aicf.Invoice_Frequency_Cd IN ( @bimonthly, @quarterly, @biannual, @annual )
                                     AND (   aicf.Expected_Invoice_Raised_Day <> 0
                                             OR aicf.Expected_Invoice_Raised_Day IS NOT NULL)
                                     AND aicf.Expected_Invoice_Raised_Month IS NOT NULL THEN 1
                                WHEN cp.Code_Value = 'Day Issued'
                                     AND aicf.Invoice_Frequency_Cd IN ( @monthly, @bi_weekly )
                                     AND (   aicf.Expected_Invoice_Raised_Day <> 0
                                             OR aicf.Expected_Invoice_Raised_Day IS NOT NULL) THEN 1
                                WHEN cp.Code_Value = 'Day Issued'
                                     AND aicf.Invoice_Frequency_Cd IN ( @Custom )
                                     AND aicf.Invoice_Frequency_Pattern_Cd = @Custom_frq_1_9_Cd
                                     AND (   aicf.Expected_Invoice_Raised_Day <> 0
                                             OR aicf.Expected_Invoice_Raised_Day IS NOT NULL) THEN 1
                                WHEN cp.Code_Value = 'Day Expected'
                                     AND aicf.Invoice_Frequency_Cd IN ( @Custom )
                                     AND aicf.Invoice_Frequency_Pattern_Cd = @Custom_frq_1_9_Cd
                                     AND (   aicf.Expected_Invoice_Received_Day <> 0
                                             OR aicf.Expected_Invoice_Received_Day IS NOT NULL) THEN 1
                                WHEN aicf.Invoice_Frequency_Cd IN ( @Custom )
                                     AND aicf.Invoice_Frequency_Pattern_Cd = @Custom_frq_Custom_Days_Cd
                                     AND (   aicf.Custom_Days IS NOT NULL
                                             OR aicf.Custom_Days <> 0)
                                     AND (   aicf.Custom_Invoice_Received_Day IS NOT NULL
                                             OR aicf.Custom_Invoice_Received_Day <> 0) THEN 1
                                ELSE 0
                            END;




                SELECT  @Loop_Cnt = @Loop_Cnt + 1;

            END;

        SELECT
            Seq_No
            , Is_Primary
            , ifc.Code_Value Invoice_Frequency
            , gpc.Code_Value Invoice_Frequency_Pattern
            , aicf.Expected_Invoice_Raised_Day
            , aicf.Expected_Invoice_Received_Day
            , aicf.Expected_Invoice_Raised_Month
            , aicf.Expected_Invoice_Received_Month
            , aicf.Expected_Invoice_Raised_Day_Lag
            , aicf.Expected_Invoice_Received_Day_Lag
            , aicf.Account_Invoice_Collection_Frequency_Id
            , aicf.Invoice_Collection_Global_Config_Value_Id
            , aicf.Invoice_Frequency_Cd
            , aicf.Custom_days
            , aicf.Custom_Invoice_Received_day
            , aicf.Next_Config_Run_Date
            , aicf.Invoice_Frequency_Pattern_Cd
        FROM
            #Account_Invoice_Collection_Frequency aicf
            INNER JOIN dbo.Code ifc
                ON ifc.Code_Id = aicf.Invoice_Frequency_Cd
            INNER JOIN dbo.Code gpc
                ON gpc.Code_Id = aicf.Invoice_Frequency_Pattern_Cd;

        DROP TABLE #Account_Invoice_Collection_Frequency;

    END;
GO
GRANT EXECUTE ON  [dbo].[Account_Invoice_Collection_Frequency_Global_Dtl_Sel] TO [CBMSApplication]
GO
