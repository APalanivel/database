SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:    dbo.DELETE_EDITED_CONTRACT_MONTHS_P 
     
      
DESCRIPTION:     
     
      
INPUT PARAMETERS:      
      Name          DataType       Default        Description      
-----------------------------------------------------------------------------      
   
    
      
OUTPUT PARAMETERS:    
      
	Name     DataType   Default  Description      
-----------------------------------------------------------------------------      
      
      
      
USAGE EXAMPLES:      
-----------------------------------------------------------------------------      
   

 
AUTHOR INITIALS:       
	Initials    Name  
-----------------------------------------------------------------------------         
	RR			Raghu Reddy
      
MODIFICATIONS       
	Initials    Date		Modification        
-----------------------------------------------------------------------------         
	RR			2020-04-30	Added header
							GRM - Contract volumes enhancement - Modified to delete data from dbo.Contract_Meter_Volume_Dtl
******/

CREATE PROCEDURE [dbo].[DELETE_EDITED_CONTRACT_MONTHS_P]
    @contractID INT
    , @monthIdentifier1 DATETIME
    , @monthIdentifier2 DATETIME
AS
    BEGIN

        SET NOCOUNT ON;

        DELETE
        cmvd
        FROM
            dbo.Contract_Meter_Volume_Dtl cmvd
            INNER JOIN dbo.CONTRACT_METER_VOLUME cmv
                ON cmv.CONTRACT_METER_VOLUME_ID = cmvd.CONTRACT_METER_VOLUME_ID
        WHERE
            cmv.CONTRACT_ID = @contractID
            AND cmv.MONTH_IDENTIFIER NOT BETWEEN @monthIdentifier1
                                         AND     @monthIdentifier2;

        DELETE
        dbo.CONTRACT_METER_VOLUME
        WHERE
            CONTRACT_ID = @contractID
            AND MONTH_IDENTIFIER NOT BETWEEN @monthIdentifier1
                                     AND     @monthIdentifier2;

    END;

GO
GRANT EXECUTE ON  [dbo].[DELETE_EDITED_CONTRACT_MONTHS_P] TO [CBMSApplication]
GO
