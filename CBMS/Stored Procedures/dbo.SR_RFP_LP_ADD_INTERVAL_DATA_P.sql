SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_LP_ADD_INTERVAL_DATA_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@User_Id       	varchar(10)	          	
	@Session_Id    	varchar(20)	          	
	@Rfp_Account_Id	int       	          	
	@Interval_name 	varchar(100)	          	
	@Image_Type    	varchar(150)	          	
	@Cbms_Doc_Id   	varchar(200)	          	
	@Cbms_Image_Id 	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

create PROCEDURE [dbo].[SR_RFP_LP_ADD_INTERVAL_DATA_P]
	@User_Id VARCHAR(10),
	@Session_Id VARCHAR(20),  
	@Rfp_Account_Id INT,
	@Interval_name VARCHAR(100),
	@Image_Type VARCHAR(150),
	@Cbms_Doc_Id VARCHAR(200),  
	@Cbms_Image_Id INT
 AS
BEGIN
 
	SET NOCOUNT ON


	DECLARE @Sr_Rfp_Lp_Interval_Data_Id INT
   		, @Image_Type_id INT --, @Cbms_Image_Id int
  
	
	SELECT @Image_Type_Id = Entity_Id FROM dbo.Entity (NOLOCK) WHERE Entity_Name = @Image_Type AND Entity_Type = 100


	UPDATE dbo.CBMS_IMAGE SET CBMS_IMAGE_TYPE_ID = @Image_Type_id WHERE cbms_image_id = @Cbms_Image_Id  
  
	INSERT INTO dbo.Sr_Rfp_Lp_Interval_Data(Sr_Rfp_Account_Id, Cbms_Image_Id, Interval_Name)
	VALUES (@Rfp_Account_Id, @Cbms_Image_Id, @Interval_Name)
  
END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_LP_ADD_INTERVAL_DATA_P] TO [CBMSApplication]
GO
