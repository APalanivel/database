SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.Site_Sel_For_Supplier_Account

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Account_Id		Int				Supplier Account

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Site_Sel_For_Supplier_Account 139332
	EXEC dbo.Site_Sel_For_Supplier_Account 5333
	EXEC dbo.Site_Sel_For_Supplier_Account 7907

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hari
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG        	03/10/2010	Created
******/

CREATE PROCEDURE dbo.Site_Sel_For_Supplier_Account
	@Account_Id INT
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT
		s.SITE_ID
		,s.SITE_NAME
		,samm.ACCOUNT_ID
	FROM
		dbo.Site s
		JOIN dbo.Account util_acc
			ON util_acc.SITE_ID = s.SITE_ID
		JOIN dbo.METER m
			ON m.ACCOUNT_ID = util_acc.ACCOUNT_ID
		JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
			ON samm.METER_ID = m.METER_ID
	WHERE
		samm.ACCOUNT_ID = @Account_Id
	GROUP BY
		s.SITE_ID
		,s.SITE_NAME
		,samm.ACCOUNT_ID

END
GO
GRANT EXECUTE ON  [dbo].[Site_Sel_For_Supplier_Account] TO [CBMSApplication]
GO
