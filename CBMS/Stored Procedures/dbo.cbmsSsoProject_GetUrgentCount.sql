SET NUMERIC_ROUNDABORT OFF 
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******
NAME:
	dbo.cbmsSsoProject_GetUrgentCount

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@client_id     	int       		null      	
	@division_id   	int       		null      	
	@site_id       	int       		null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC dbo.cbmsSsoProject_GetUrgentCount 49

	EXEC dbo.cbmsSsoProject_GetUrgentCount 8523

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CPE			Chaitanya Panduga Eshwar
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	CPE			03/28/2011	Replaced vwCbmsSSOProjectOwnerFlat with SSO_PROJECT_OWNER_MAP table. 
******/

CREATE PROCEDURE dbo.cbmsSsoProject_GetUrgentCount
( 
 @MyAccountId int
,@client_id int = null
,@division_id int = null
,@site_id int = null )
AS 
BEGIN

	/*
		Need owners for two reasons
			1) Find distinct projects this user can see
			2) Return all owners of this project as part of RS
	*/

      SET NOCOUNT ON
      DECLARE @Project_Status_Type_id INT

	SELECT
		@Project_Status_Type_id = ENTITY_ID
	FROM
	ENTITY
	WHERE
	ENTITY_DESCRIPTION = 'Project Status Type'
	AND ENTITY_NAME = 'Open'

      EXEC cbmsSecurity_GetClientAccess @MyAccountId, @client_id OUTPUT, @division_id OUTPUT, @site_id OUTPUT

  
      SELECT COUNT(DISTINCT SP.IS_URGENT) urgent_count
      FROM
            dbo.SSO_PROJECT SP
            JOIN ( SELECT
                        SSO_PROJECT_ID
                   FROM
                        dbo.SSO_PROJECT_OWNER_MAP SPOM
                        JOIN Core.Client_Hier CHI
                              ON SPOM.Client_Hier_Id = CHI.Client_Hier_Id
                   WHERE
                        ( @client_id IS NULL
                          OR @client_id = CHI.Client_Id )
                        AND ( @division_id IS NULL
                              OR @division_id = CHI.Sitegroup_Id )
                        AND ( @site_id IS NULL
                              OR @Site_Id = CHI.Site_Id ) GROUP BY SSO_PROJECT_ID) acc
                  ON acc.SSO_PROJECT_ID = SP.SSO_PROJECT_ID
            JOIN dbo.SSO_PROJECT_OWNER_MAP OWN
                  ON OWN.SSO_PROJECT_ID = SP.SSO_PROJECT_ID
      WHERE
            SP.IS_URGENT = 1
            AND SP.PROJECT_STATUS_TYPE_ID = @Project_Status_Type_id
           
END

GO
GRANT EXECUTE ON  [dbo].[cbmsSsoProject_GetUrgentCount] TO [CBMSApplication]
GO
GO