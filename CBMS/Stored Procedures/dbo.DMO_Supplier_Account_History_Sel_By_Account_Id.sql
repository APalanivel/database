SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: dbo.[DMO_Supplier_Account_History_Sel_By_Account_Id]  
     
DESCRIPTION:

	To select all the account history information for the given account id.
	Instead of calling individual procedures application will call this procedure to get all the informations required for the page at once.

INPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
	@Supplier_Account_Id		INT						
	@StartIndex		INT			1
	@EndIndex		INT			2147483647

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------  

EXEC dbo.DMO_Supplier_Account_Sel_By_Client 235  

	EXEC dbo.DMO_Supplier_Account_History_Sel_By_Account_Id  1,1,25
	
	EXEC dbo.DMO_Supplier_Account_History_Sel_By_Account_Id 1197550, 1,25,507644

AUTHOR INITIALS:
	INITIALS	NAME          
------------------------------------------------------------          
	RR			Raghu Reddy
	NR			Narayana Reddy

MODIFICATIONS
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------          
	RR			2017-02-17	Contract placeholder - CP-54 Created
	NR			2020-01-16	MAINT-9734 - Added @Supplier_Account_Config_Id new parameter.

*/

CREATE PROCEDURE [dbo].[DMO_Supplier_Account_History_Sel_By_Account_Id]
    (
        @Supplier_Account_Id INT
        , @StartIndex INT = 1
        , @EndIndex INT = 2147483647
        , @Supplier_Account_Config_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        EXEC dbo.DMO_Supplier_Account_Dtls_Sel_By_Supplier_Account_Id
            @Supplier_Account_Id
            , @Supplier_Account_Config_Id;

        EXEC dbo.DMO_Supplier_Account_Meter_Sel_By_Supplier_Account
            @Supplier_Account_Id
            , @StartIndex
            , @EndIndex
            , @Supplier_Account_Config_Id;

        EXEC dbo.CU_Invoice_Dtl_Sel_By_Supplier_Account_Id_Config_Id
            @Supplier_Account_Id
            , @Supplier_Account_Config_Id
            , @StartIndex
            , @EndIndex;

        EXEC dbo.Cost_Usage_Service_Month_Sel_By_Supplier_Account_Id_Config_Id
            @Supplier_Account_Id
            , @Supplier_Account_Config_Id
            , @StartIndex
            , @EndIndex;

        EXEC dbo.Audit_Log_Sel_By_Account_Id
            @Supplier_Account_Id
            , @StartIndex
            , @EndIndex;
        EXEC dbo.Invoice_Collection_Period_Sel_By_Supplier_Account_Id_Config_Id
            @Supplier_Account_Id
            , @Supplier_Account_Config_Id
            , @StartIndex
            , @EndIndex;
        EXEC dbo.Account_Group_Sel_By_Account_Id @Supplier_Account_Id;
    END;
    ;

    ;

GO
GRANT EXECUTE ON  [dbo].[DMO_Supplier_Account_History_Sel_By_Account_Id] TO [CBMSApplication]
GO
