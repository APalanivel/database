SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********     

NAME:   DBO.Utility_Account_Search      
      
DESCRIPTION:

	TO GET THE UTILITY ACCOUNT DETAILS BASED ON GIVEN INPUTS WITH PAGINATION.
	
	
	Created as a part of Delete Tool.
      
INPUT PARAMETERS:      
NAME			DATATYPE	  DEFAULT  DESCRIPTION      
--------------------------------------------------------------------      
@CLIENT_ID		INT      
@SITE_ID		INT			  NULL      
@ACCOUNT_NUMBER VARCHAR(50)   NULL		LIKE search (account number started with the input values)
@CITY			VARCHAR(200)  NULL      LIKE search (City name started with the input values)
@STATE_ID		INT			  NULL      
@STARTINDEX		INT				1  
@ENDINDEX		INT				2147483647
    
OUTPUT PARAMETERS:      
NAME   DATATYPE DEFAULT  DESCRIPTION      
------------------------------------------------------------      
USAGE EXAMPLES:      
------------------------------------------------------------      

	EXEC Utility_Account_Search 235,null,'000 222 490 023 5259 5'
    --Searching with Client
    EXEC Utility_Account_Search 235,NULL,NULL,NULL,NULL,1,25
    --Search With Site
    EXEC Utility_Account_Search NULL,31271,NULL,NULL,NULL,1,25
    --Search With Partial Account Number
    EXEC Utility_Account_Search NULL,NULL,'1008',NULL,NULL,1,25
    ---Search With Partial City Name
    EXEC Utility_Account_Search NULL,NULL,NULL,'Hou',NULL,1,25
    ---Search With State 
    EXEC Utility_Account_Search NULL,NULL,NULL,NULL,44,1,25
    ---For All records...
    EXEC Utility_Account_Search 235,NULL,NULL,NULL,NULL
    
AUTHOR INITIALS:      
INITIALS	NAME      
------------------------------------------------------------      
PNR			Pandarinath
      
MODIFICATIONS      
INITIALS	DATE		MODIFICATION      
------------------------------------------------------------      
PNR			25-May-2010 Created to fetch account details based on given inputs with pagination.
						And the user can fetch account details by giving any input such as Client,
						Site,Account,City etc.
*/        
         
         
CREATE PROCEDURE dbo.Utility_Account_Search
(
	@CLIENT_ID			INT      
	,@SITE_ID			INT			= NULL
	,@ACCOUNT_NUMBER	VARCHAR(50) = NULL        
	,@CITY				VARCHAR(200)= NULL      
	,@STATE_ID			INT			= NULL      
	,@STARTINDEX		INT			= 1  
	,@ENDINDEX			INT			= 2147483647       
)
AS
BEGIN

	SET NOCOUNT ON;

	SET @ACCOUNT_NUMBER = @ACCOUNT_NUMBER+'%'
	SET @CITY = @CITY + '%';

    WITH Cte_Acct_Detail
    AS
    (
		SELECT
			acc.Account_Id
			,cl.CLIENT_NAME
			,s.SITE_NAME
			,adr.CITY
			,st.STATE_NAME
			,acc.ACCOUNT_NUMBER
			,vdr.VENDOR_NAME
			,Row_Num = ROW_NUMBER() OVER (ORDER BY  acc.ACCOUNT_NUMBER)
			,Total_Rows=COUNT(1) OVER ()
		FROM
			dbo.SITE s
			JOIN dbo.ACCOUNT acc
				 ON acc.SITE_ID = s.SITE_ID
			JOIN dbo.CLIENT cl
				 ON s.Client_ID = cl.CLIENT_ID
			JOIN dbo.VENDOR	vdr
				 ON acc.VENDOR_ID = vdr.VENDOR_ID
			JOIN dbo.ADDRESS adr
				 ON s.PRIMARY_ADDRESS_ID = adr.ADDRESS_ID
			JOIN dbo.STATE st
				 ON adr.STATE_ID = st.STATE_ID
			JOIN dbo.Entity en
				ON en.ENTITY_ID = acc.ACCOUNT_TYPE_ID
		WHERE
			 (@CLIENT_ID IS NULL OR cl.CLIENT_ID=@CLIENT_ID)
			 AND(@SITE_ID IS NULL OR s.SITE_ID = @SITE_ID)
			 AND(@ACCOUNT_NUMBER IS NULL OR acc.ACCOUNT_NUMBER LIKE @ACCOUNT_NUMBER)
			 AND(@CITY IS NULL OR adr.CITY LIKE @CITY)
			 AND(@STATE_ID IS NULL OR st.STATE_ID = @STATE_ID)
			 AND (en.ENTITY_NAME = 'Utility')
	)
	SELECT
		CLIENT_NAME
		,SITE_NAME
		,CITY
		,STATE_NAME
		,Account_Id
		,ISNULL(ACCOUNT_NUMBER,'Not Yet Assigned') Account_Number
		,VENDOR_NAME
		,Commodity_Name = LEFT(Cmdty_List.Cmdty_Name, LEN(Cmdty_List.Cmdty_Name)-1)
		,Total_Rows
	FROM
		Cte_Acct_Detail acc
		CROSS APPLY
		(
			SELECT
				 CONVERT(VARCHAR,com.Commodity_Name) + ' , '
			FROM
				dbo.METER mtr
				JOIN dbo.RATE R
					 ON mtr.RATE_ID=r.RATE_ID
				JOIN dbo.Commodity com
					 ON r.COMMODITY_TYPE_ID= com.Commodity_Id
			WHERE
				mtr.account_id = acc.account_id
			GROUP BY
				com.Commodity_Name
			FOR XML PATH('')
		) Cmdty_List(Cmdty_Name)
	WHERE
		Row_Num BETWEEN @STARTINDEX AND @ENDINDEX

END
GO
GRANT EXECUTE ON  [dbo].[Utility_Account_Search] TO [CBMSApplication]
GO
