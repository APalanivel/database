SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	
	dbo.Commodity_History_Check_For_Account_Level_By_Client_Commmodity

DESCRIPTION:


INPUT PARAMETERS:
	Name						DataType	Default	    Description
------------------------------------------------------------------------------
	@Client_id 					INT       			
	@Commodity_Id				INT

OUTPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@Is_History_Exists	BIT							
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Commodity_History_Check_For_Account_Level_By_Client_Commmodity 10069,67

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan Ganesan

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	HG        	1/28/2010	Created

*/
CREATE PROCEDURE dbo.Commodity_History_Check_For_Account_Level_By_Client_Commmodity(
	@Client_id	INT
	,@Commodity_Id INT
	,@Is_History_Exists BIT = 0 OUT)
AS

BEGIN
	SET NOCOUNT ON;

	IF EXISTS(
		SELECT
			1
		FROM
			dbo.Site s
			JOIN dbo.Account util_Acc
				ON util_Acc.Site_id = s.SITE_ID
			JOIN dbo.METER m
				ON m.ACCOUNT_ID = util_Acc.ACCOUNT_ID
			JOIN dbo.Rate r
				ON r.RATE_ID = m.RATE_ID
		WHERE
			s.Client_ID = @Client_id
			AND r.COMMODITY_TYPE_ID = @Commodity_Id
			)
	BEGIN
		SET @Is_History_Exists = 1
	END
	
	

END
GO
GRANT EXECUTE ON  [dbo].[Commodity_History_Check_For_Account_Level_By_Client_Commmodity] TO [CBMSApplication]
GO
