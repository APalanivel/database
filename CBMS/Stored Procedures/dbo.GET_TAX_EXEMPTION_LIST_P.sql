SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE dbo.GET_TAX_EXEMPTION_LIST_P
	AS
	begin
		set nocount on

		select  entity_name,
			entity_id 
		from	entity 
		where   entity_type = 1058 
		order by entity_id asc

	end
GO
GRANT EXECUTE ON  [dbo].[GET_TAX_EXEMPTION_LIST_P] TO [CBMSApplication]
GO
