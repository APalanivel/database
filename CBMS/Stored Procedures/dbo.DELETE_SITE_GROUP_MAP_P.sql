SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.DELETE_SITE_GROUP_MAP_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@clientId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE  PROCEDURE [dbo].[DELETE_SITE_GROUP_MAP_P] 

@clientId integer
AS
delete from rm_group_site_map where rm_group_id in
(select rm_group_id from rm_group where client_id=@clientId)
GO
GRANT EXECUTE ON  [dbo].[DELETE_SITE_GROUP_MAP_P] TO [CBMSApplication]
GO
