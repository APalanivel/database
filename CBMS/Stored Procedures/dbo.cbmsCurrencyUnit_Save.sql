SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.[cbmsCurrencyUnitConversion_Save]


DESCRIPTION: 


INPUT PARAMETERS:    
      Name                  DataType          Default     Description    
------------------------------------------------------------------    
	@myAccountId               int
	@currencyGroupId           int
	@baseUnitId                int
	@convertedUnitId           int
	@conversionFactor          decimal(32,16)
	@startDate                 datetime
	@yearsForward              int               0
        
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
--exec cbmsCurrencyUnit_Save
--	  @MyAccountId = 1
--	, @CurrencyUnitId = null
--	, @CurrencyUnitName ='Test Data'
--	, @Symbol = 'TST'
--	, @CountryId = null



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates
 DMR		  09/10/2010 Modified for Quoted_Identifier
	  

******/
CREATE PROCEDURE [dbo].[cbmsCurrencyUnit_Save]
	( @MyAccountId int
	, @CurrencyUnitId int = null
	, @CurrencyUnitName varchar(200)
	, @Symbol varchar(10) = null
	, @CountryId int = null
	)
AS

BEGIN

	declare @this_id int
	set @this_id = @CurrencyUnitId

	if @this_id is null
	begin

		INSERT INTO currency_unit 
			( currency_unit_name
			, symbol
			, sort_order
			)
		   VALUES 
			( @CurrencyUnitName
			, @Symbol
			, 10
			)
	
	      SET @this_id = SCOPE_IDENTITY()
	end
	else
	begin


	   UPDATE 
		currency_unit
	   SET 
		currency_unit_name = @CurrencyUnitName
		, symbol = @Symbol
	   WHERE 
		currency_unit_id = @this_id
	end


	if @CountryId is not null
	begin

		INSERT INTO currency_unit_country_map
			( currency_unit_id
			, country_id
			)
		   VALUES
			( @this_id
			, @CountryId
			)

	end

	exec cbmsCurrencyUnit_GetAll @MyAccountId, @this_id, NULL, NULL, NULL
	
END
GO
GRANT EXECUTE ON  [dbo].[cbmsCurrencyUnit_Save] TO [CBMSApplication]
GO
