SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Rfp_Service_Condition_Template_Question_Response_Ins]
           
DESCRIPTION:             
			To get service condition template details 
			
INPUT PARAMETERS:            
	Name								DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Sr_Service_Condition_Template_Id	INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
	EXEC dbo.Sr_Rfp_Service_Condition_Template_Question_Sel_By_Product_Account_Term  1211423 ,109758 
	EXEC dbo.Sr_Rfp_Service_Condition_Template_Question_Sel_By_Product_Account_Term  1211423 ,109757 

	BEGIN TRANSACTION
		SELECT * FROM dbo.Sr_Rfp_Service_Condition_Template_Question_Response WHERE Sr_Rfp_Service_Condition_Template_Question_Map_Id = 41
		EXEC dbo.Sr_Rfp_Service_Condition_Template_Question_Response_Ins  41,88369,'Dynamic','Dynamic',16
		SELECT * FROM dbo.Sr_Rfp_Service_Condition_Template_Question_Response WHERE Sr_Rfp_Service_Condition_Template_Question_Map_Id = 41
	ROLLBACK TRANSACTION
			
		
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-03-02	Global Sourcing - Phase3 - GCS-532 Created
******/
CREATE PROCEDURE [dbo].[Sr_Rfp_Service_Condition_Template_Question_Response_Ins]
      ( 
       @Sr_Rfp_Service_Condition_Template_Question_Map_Id INT
      ,@SR_RFP_BID_ID INT
      ,@Response_Text_Value NVARCHAR(500)
      ,@Comment NVARCHAR(MAX) = NULL
      ,@User_Info_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DELETE FROM
            dbo.Sr_Rfp_Service_Condition_Template_Question_Response
      WHERE
            Sr_Rfp_Service_Condition_Template_Question_Map_Id = @Sr_Rfp_Service_Condition_Template_Question_Map_Id
            AND SR_RFP_BID_ID = @SR_RFP_BID_ID
            
      INSERT      INTO dbo.Sr_Rfp_Service_Condition_Template_Question_Response
                  ( 
                   Sr_Rfp_Service_Condition_Template_Question_Map_Id
                  ,SR_RFP_BID_ID
                  ,Response_Text_Value
                  ,Created_User_Id
                  ,Created_Ts
                  ,Updated_User_Id
                  ,Last_Change_Ts
                  ,Comment )
                  SELECT
                        @Sr_Rfp_Service_Condition_Template_Question_Map_Id
                       ,@SR_RFP_BID_ID
                       ,@Response_Text_Value
                       ,@User_Info_Id
                       ,getdate()
                       ,@User_Info_Id
                       ,getdate()
                       ,@Comment
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.Sr_Rfp_Service_Condition_Template_Question_Response qtnresp
                                     WHERE
                                          qtnresp.Sr_Rfp_Service_Condition_Template_Question_Map_Id = @Sr_Rfp_Service_Condition_Template_Question_Map_Id
                                          AND qtnresp.SR_RFP_BID_ID = @SR_RFP_BID_ID
                                          AND qtnresp.Response_Text_Value = @Response_Text_Value )
      
            
END;
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Service_Condition_Template_Question_Response_Ins] TO [CBMSApplication]
GO
