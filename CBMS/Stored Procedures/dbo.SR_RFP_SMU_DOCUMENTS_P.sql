SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[SR_RFP_SMU_DOCUMENTS_P]
	@user_id varchar(10),
	@session_id varchar(20),
	@rfp_id int
	AS

select	c.cbms_doc_id,
	    c.cbms_image_id
from 	cbms_image c(nolock),
	sr_rfp_smu smu (nolock)
	 
where 	smu.sr_rfp_id = @rfp_id
	and c.cbms_image_id = smu.cbms_image_id


















GO
GRANT EXECUTE ON  [dbo].[SR_RFP_SMU_DOCUMENTS_P] TO [CBMSApplication]
GO
