SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********  
NAME:	[dbo].[IC_Supllier_Account_Changes_Queue_Upd]   

DESCRIPTION:     

	
INPUT PARAMETERS:    
Name								DataType		Default		Description    
----------------------------------------------------------------------------    
@User_Info_Id					 	INT      

OUTPUT PARAMETERS:    
Name					DataType		Default		Description    
----------------------------------------------------------------------------    

USAGE EXAMPLES:    
----------------------------------------------------------------------------   
 


EXEC dbo.CODE_SEL_BY_CodeSet_Name
    @CodeSet_Name = 'Request Status'
    , @Code_Value = 'Completed'

BEGIN TRAN

EXEC [dbo].[IC_Supllier_Account_Changes_Queue_Upd] 1, 100431

ROLLBACK TRAN

AUTHOR INITIALS:    
Initials	Name    
----------------------------------------------------------------------------    
NR			Narayana Reddy
 
MODIFICATIONS     
Initials	Date			Modification    
-----------------------------------------------------------------------------    
NR			2020-04-22		Created for Small enhancement
******/

CREATE PROCEDURE [dbo].[IC_Supllier_Account_Changes_Queue_Upd]
    (
        @IC_Supllier_Account_Changes_Queue_Id INT
        , @Status_Cd INT
    )
AS
    BEGIN

        SET NOCOUNT ON;


        UPDATE
            isacq
        SET
            isacq.Status_Cd = @Status_Cd
        FROM
            dbo.IC_Supllier_Account_Changes_Queue isacq
        WHERE
            isacq.IC_Supllier_Account_Changes_Queue_Id = @IC_Supllier_Account_Changes_Queue_Id;
    END;

GO
GRANT EXECUTE ON  [dbo].[IC_Supllier_Account_Changes_Queue_Upd] TO [CBMSApplication]
GO
