SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME: dbo.SSO_SAVINGS_DETAIL_Del

DESCRIPTION:

	Used to delete SSO_SAVINGS_DETAIL.

INPUT PARAMETERS:
	NAME				DATATYPE	DEFAULT		DESCRIPTION     
----------------------------------------------------------------
	@SSO_Savings_Id		INT
	@Service_Month		DATETIME

OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------         
	USAGE EXAMPLES:
------------------------------------------------------------ 

	BEGIN TRAN

		EXEC dbo.SSO_SAVINGS_DETAIL_Del 4, '1/1/2005'

	ROLLBACK TRAN

	SELECT TOP 100 * FROM SSO_SAVINGS_DETAIL
	
AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	HG			Harihara Suthan G

MODIFICATIONS
	INITIALS	DATE			MODIFICATION
------------------------------------------------------------
	HG			9/24/2010		Created

*/
CREATE PROCEDURE dbo.SSO_SAVINGS_DETAIL_Del
    (
       @SSO_Savings_Id	INT
       ,@Service_Month	DateTime
       
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE
		dbo.SSO_SAVINGS_DETAIL
	WHERE
		SSO_SAVINGS_ID = @Sso_Savings_Id
		AND Service_Month = @Service_Month

END
GO
GRANT EXECUTE ON  [dbo].[SSO_SAVINGS_DETAIL_Del] TO [CBMSApplication]
GO
