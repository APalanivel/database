SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:   [Workflow].[Workflow_Queue_D_Save_FilterQuery]     
DESCRIPTION:   The Stored Procedure will Delete  the Saved Filter information   
------------------------------------------------------------       
 INPUT PARAMETERS:        
 Name   DataType  Default Description       
 @vcSubQueueName AS NVARCHAR(MAX),        
 @int_Queue_id  AS INT,      
      
------------------------------------------------------------        
 OUTPUT PARAMETERS:        
 Name   DataType  Default Description        
 @str_Return  AS VARCHAR(100) OUTPUT    
------------------------------------------------------------        
 USAGE EXAMPLES:        
------------------------------------------------------------        
AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
TRK   Ramakrishna  Summit Energy     
     
 MODIFICATIONS         
 Initials Date   Modification        
------------------------------------------------------------        
 TRK    SEP-2019 Created    
    
******/    
CREATE PROCEDURE [Workflow].[Workflow_Queue_D_Save_FilterQuery]              
(              
 @Saved_Filter_Id AS INT,              
 @int_Module_Id  AS INT,            
 @str_Return  AS VARCHAR(100) OUTPUT              
              
)              
AS              
BEGIN              
 BEGIN TRY              
              
 DECLARE @Saved_Filter_Query_Id INT,  @Workflow_Queue_Search_Filter_Group_Id INT          
           
 SELECT @Saved_Filter_Query_Id = Workflow_Queue_Saved_Filter_Query_Id          
 FROM Workflow.workflow_queue_saved_filter_query fq     
 INNER JOIN  Workflow.Workflow_Sub_Queue sq  ON fq.Workflow_Sub_Queue_Id = sq.Workflow_Sub_Queue_Id          
 AND fq.Workflow_Queue_Saved_Filter_Query_Id= @Saved_Filter_Id           
 AND sq.Workflow_Queue_Id = @int_Module_Id       
     
 SELECT  @Workflow_Queue_Search_Filter_Group_Id =  Workflow_Queue_Search_Filter_Group_Id FROM Workflow.Workflow_Queue_Search_Filter_Group     
 WHERE Workflow_Queue_Saved_Filter_Query_Id=@Saved_Filter_Query_Id    
     
 DELETE FROM Workflow.Workflow_Queue_Search_Filter_Group_Invoice_Map WHERE Workflow_Queue_Search_Filter_Group_Id=@Workflow_Queue_Search_Filter_Group_Id    
     
 DELETE FROM  Workflow.Workflow_Queue_Search_Filter_Group WHERE Workflow_Queue_Saved_Filter_Query_Id  =@Saved_Filter_Query_Id     
     
 DELETE FROM  Workflow.Workflow_queue_saved_filter_query_value WHERE Workflow_Queue_Saved_Filter_Query_Id =  @Saved_Filter_Query_Id             
            
 DELETE FROM  Workflow.Workflow_queue_saved_filter_query WHERE Workflow_Queue_Saved_Filter_Query_Id =  @Saved_Filter_Query_Id             
          
 SELECT  @str_Return =  'Deleted successfully'               
 END TRY              
 BEGIN CATCH               
 SELECT @str_Return =  'Error Occured , please try again'              
 END CATCH              
              
END                                              
GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_D_Save_FilterQuery] TO [CBMSApplication]
GO
