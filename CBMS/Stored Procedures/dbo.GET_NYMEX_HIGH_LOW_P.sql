SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.GET_NYMEX_HIGH_LOW_P

DESCRIPTION:


INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@lastTradeDate 		varchar(12)	          	
	@symbol        		varchar(200)	          	
	@isIndividualMonth	bit       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	
	EXEC dbo.GET_NYMEX_HIGH_LOW_P '9/24/2010','NGV10',0
	EXEC dbo.GET_NYMEX_HIGH_LOW_P '9/24/2010','NGV10',1

	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/27/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on
	
******/

CREATE PROCEDURE dbo.GET_NYMEX_HIGH_LOW_P
@lastTradeDate		VARCHAR(12),
@symbol				VARCHAR(200), 
@isIndividualMonth	BIT
AS
BEGIN

	SET NOCOUNT ON ;
	
	--fetch the NYMEX High and Low only for a particular month
	IF @isIndividualMonth > 0
		BEGIN
			SELECT 	HIGH_PRICE AS NYMEX_HIGH, 
				LOW_PRICE AS NYMEX_LOW
			FROM 	RM_NYMEX_DATA 
			WHERE 	LAST_TRADE_DATE = CONVERT(VARCHAR(12),@lastTradeDate, 101) AND 
				SYMBOL = @symbol
		END
	--fetch the average of NYMEX High and Low for all months in DT
	ELSE 
		BEGIN
			DECLARE @SQLStatement VARCHAR(1000)
			DECLARE @whereClause VARCHAR(1000)
			DECLARE @baseQuery VARCHAR(1000)

			SELECT @baseQuery = 	' SELECT AVG(HIGH_PRICE) AS NYMEX_HIGH, ' + 
						' AVG(LOW_PRICE) AS NYMEX_LOW ' + 
						' FROM 	dbo.RM_NYMEX_DATA WHERE '
			
			SELECT @whereClause = 	' LAST_TRADE_DATE = ' +''''+ CONVERT(VARCHAR(12),@lastTradeDate, 101) +''''+ ' AND ' + 
						' SYMBOL IN (' +''''+ @symbol +''''+ ')'

			SELECT @SQLStatement = @baseQuery + @whereClause
			EXEC (@SQLStatement)
			
			PRINT @SqlStatement

		END
END
GO
GRANT EXECUTE ON  [dbo].[GET_NYMEX_HIGH_LOW_P] TO [CBMSApplication]
GO
