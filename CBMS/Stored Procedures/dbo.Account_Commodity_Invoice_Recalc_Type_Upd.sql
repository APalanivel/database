SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Account_Commodity_Invoice_Recalc_Type_Upd

DESCRIPTION:

INPUT PARAMETERS:
	Name										DataType		Default		Description
-------------------------------------------------------------------------------------------
	@Account_Commodity_Invoice_Recalc_Type_Id	INT
    @Start_Dt									DATE
    @End_Dt										DATE
    @Invoice_Recalc_Type_Cd						INT
    @User_Info_Id								INT

OUTPUT PARAMETERS:
	Name										DataType		Default		Description
-------------------------------------------------------------------------------------------
	
USAGE EXAMPLES:
-------------------------------------------------------------------------------------------
   DECLARE @Account_Commodity_Invoice_Recalc_Type_Id INT
	SELECT * FROM Account_Commodity_Invoice_Recalc_Type WHERE Account_Id=1148520
	BEGIN TRANSACTION
	  EXEC dbo.Account_Commodity_Invoice_Recalc_Type_Ins 
      @Account_Id =1148520
     ,@Commodity_Id = 290
     ,@Start_Dt = '2015-01-01'
     ,@End_Dt= NULL
     ,@Invoice_Recalc_Type_Cd = 102030
     ,@User_Info_Id = 49
     ,@Determinant_Source_Cd = 102536
     
select * FROM Account_Commodity_Invoice_Recalc_Type WHERE Account_Id=1148520
		
	SET @Account_Commodity_Invoice_Recalc_Type_Id = @@identity
	
      EXEC Account_Commodity_Invoice_Recalc_Type_Upd 
            @Account_Commodity_Invoice_Recalc_Type_Id
            ,290
           ,'2015-01-01'
           ,'2015-01-01'
           ,102029
           ,49
           ,102536
           
	SELECT * FROM Account_Commodity_Invoice_Recalc_Type WHERE Account_Id=1148520  
			
	ROLLBACK TRANSACTION


AUTHOR INITIALS:
	Initials	Name
-------------------------------------------------------------------------------------------
	RKV			Ravi Kumar Vegesna
	NR			Narayana Reddy
	
MODIFICATIONS
	Initials	Date			Modification
-------------------------------------------------------------------------------------------
	RKV       	2015-08-10		Created for AS400-II
	NR			2018-02-08		Interval Data Recalcs - IDR-5 - Added @Determinant_Source_Cd as parameter.      


******/

CREATE PROCEDURE [dbo].[Account_Commodity_Invoice_Recalc_Type_Upd]
    (
        @Account_Commodity_Invoice_Recalc_Type_Id INT
        , @Commodity_Id INT
        , @Start_Dt DATE
        , @End_Dt DATE
        , @Invoice_Recalc_Type_Cd INT
        , @User_Info_Id INT
        , @Determinant_Source_Cd INT
        , @Source_Cd INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @New_Exception_Status_Cd INT
            , @Closed_Exception_Status_Cd INT
            , @Exception_Type_Cd INT
            , @Account_Id INT;


        SELECT
            @Account_Id = Account_Id
        FROM
            Account_Commodity_Invoice_Recalc_Type
        WHERE
            Account_Commodity_Invoice_Recalc_Type_Id = @Account_Commodity_Invoice_Recalc_Type_Id;



        SELECT
            @New_Exception_Status_Cd = cd.Code_Id
        FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Status'
            AND cd.Code_Value = 'New';

        SELECT
            @Closed_Exception_Status_Cd = cd.Code_Id
        FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Status'
            AND cd.Code_Value = 'Closed';


        SELECT
            @Exception_Type_Cd = cd.Code_Id
        FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Type'
            AND cd.Code_Value = 'Missing Recalc Type';


        UPDATE
            acirt
        SET
            Commodity_ID = @Commodity_Id
            , Start_Dt = @Start_Dt
            , End_Dt = @End_Dt
            , Invoice_Recalc_Type_Cd = @Invoice_Recalc_Type_Cd
            , Updated_User_Id = @User_Info_Id
            , Last_Change_Ts = GETDATE()
            , Determinant_Source_Cd = @Determinant_Source_Cd
            , acirt.Source_Cd = @Source_Cd
        FROM
            dbo.Account_Commodity_Invoice_Recalc_Type acirt
        WHERE
            acirt.Account_Commodity_Invoice_Recalc_Type_Id = @Account_Commodity_Invoice_Recalc_Type_Id;


        EXEC dbo.ADD_ENTITY_AUDIT_ITEM_P
            @entity_identifier = @Account_Id
            , @user_info_id = @User_Info_Id
            , @audit_type = 2
            , @entity_name = 'ACCOUNT_TABLE'
            , @entity_type = 500;


        UPDATE
            ae
        SET
            ae.Exception_Status_Cd = @Closed_Exception_Status_Cd
            , ae.Closed_By_User_Id = @User_Info_Id
            , ae.Closed_Ts = GETDATE()
            , ae.Last_Change_Ts = GETDATE()
            , ae.Updated_User_Id = @User_Info_Id
        FROM
            dbo.Account_Exception ae
            INNER JOIN dbo.Account_Commodity_Invoice_Recalc_Type acirt
                ON ae.Account_Id = acirt.Account_Id
        WHERE
            acirt.Account_Commodity_Invoice_Recalc_Type_Id = @Account_Commodity_Invoice_Recalc_Type_Id
            AND ae.Commodity_Id = @Commodity_Id
            AND ae.Exception_Type_Cd = @Exception_Type_Cd
            AND ae.Exception_Status_Cd = @New_Exception_Status_Cd;



    END;
    ;



GO
GRANT EXECUTE ON  [dbo].[Account_Commodity_Invoice_Recalc_Type_Upd] TO [CBMSApplication]
GO
