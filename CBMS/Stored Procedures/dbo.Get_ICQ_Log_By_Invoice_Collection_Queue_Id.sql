SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
    
    
/******                    
Name:       
   dbo.Get_ICQ_Log_By_Invoice_Collection_Queue_Id    
    
           
                    
Description:                    
   This sproc to get the data from  cbms.dbo.user_info table  and Analysis.dbo.ICQ_LOG     
   Based on  Given Invoice_Collection_Queue_Id             
                                 
 Input Parameters:                    
    Name      DataType   Default   Description                      
----------------------------------------------------------------------------------------                      
 @Invoice_Collection_Queue_Id     INT      
          
 Output Parameters:                          
    Name        DataType   Default   Description                      
----------------------------------------------------------------------------------------                      
                    
 Usage Examples:                        
----------------------------------------------------------------------------------------         
  Exec dbo.Get_ICQ_Log_By_Invoice_Collection_Queue_Id   @Invoice_Collection_Queue_Id  =3241871    
  Exec dbo.Get_ICQ_Log_By_Invoice_Collection_Queue_Id   @Invoice_Collection_Queue_Id  =2250913    
  Exec dbo.Get_ICQ_Log_By_Invoice_Collection_Queue_Id @Invoice_Collection_Queue_Id  =3241783    
    
         
             
       
Author Initials:                    
    Initials  Name                    
----------------------------------------------------------------------------------------                      
 vc       veera sekhar chamanthula    
       
 Modifications:                    
    Initials        Date   Modification                    
----------------------------------------------------------------------------------------                      
              
              
              
                   
******/       
CREATE PROCEDURE [dbo].[Get_ICQ_Log_By_Invoice_Collection_Queue_Id]    
      (       
        @Invoice_Collection_Queue_Id     INT      
   )    
AS       
    
BEGIN    
  SET NOCOUNT ON       
                  
    SELECT      
       AI.Invoice_Collection_Account_Config_Id    
      ,AI.ICQ_Log_Message      
      ,UI.FIRST_NAME + ' ' + UI.LAST_NAME as Username     
      ,AI.Created_Ts      
    
    FROM   LOGDB.dbo.ICQ_LOG AI    
     join       
      cbms.dbo.user_info UI    
      ON AI.user_info_id=UI.user_info_id    
    
    WHERE AI.Invoice_Collection_Queue_Id =@Invoice_Collection_Queue_Id      
     
    
END    
    
    
    
GO
GRANT EXECUTE ON  [dbo].[Get_ICQ_Log_By_Invoice_Collection_Queue_Id] TO [CBMSApplication]
GO
