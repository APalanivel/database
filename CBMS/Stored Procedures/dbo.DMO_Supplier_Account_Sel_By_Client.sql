SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.DMO_Supplier_Account_Sel_By_Client
           
DESCRIPTION:             
			To get DMO supplier accounts associated to a client
			
INPUT PARAMETERS:            
	Name			DataType		Default		Description  
---------------------------------------------------------------------------------  
	@Client_Id		INT
    @Site_Id		INT				NULL
    @Account_Number VARCHAR(50)		NULL
    @City			VARCHAR(200)	NULL
    @State_Id		VARCHAR(200)	NULL
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	          
	EXEC dbo.DMO_Supplier_Account_Sel_By_Client 11278
	
	EXEC dbo.DMO_Supplier_Account_Sel_By_Client 235	
		
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy
	NR			Narayana Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-02-17	Contract placeholder - CP-54 Created
	NR			2019-09-18	Add Contract -  Restricted the accounts when we have any contract is mapped.
	NR			2020-01-16	MAINT-9734 - Removed Restricted to delete the time band.
******/

CREATE PROCEDURE [dbo].[DMO_Supplier_Account_Sel_By_Client]
    (
        @Client_Id INT
        , @Site_Id INT = NULL
        , @Account_Number VARCHAR(50) = NULL
        , @City VARCHAR(200) = NULL
        , @State_Id INT = NULL
        , @Supplier_Account_Id INT = NULL
        , @StartIndex INT = 1
        , @EndIndex INT = 2147483647
    )
AS
    BEGIN

        SET NOCOUNT ON;

        CREATE TABLE #DMO_Supplier_Account
             (
                 Client_Name VARCHAR(200)
                 , Site_Id INT
                 , Site_name VARCHAR(200)
                 , City VARCHAR(200)
                 , State_Id INT
                 , State_Name VARCHAR(200)
                 , Supplier_Account_Id INT
                 , Supplier_Account_Number VARCHAR(50)
                 , Supplier_Account_Vendor_Name VARCHAR(200)
                 , Commodity_Name VARCHAR(50)
                 , Supplier_Account_Config_Id INT
                 , Supplier_Account_begin_Dt DATE
                 , Supplier_Account_End_Dt DATE
             );

        INSERT INTO #DMO_Supplier_Account
             (
                 Client_Name
                 , Site_Id
                 , Site_name
                 , City
                 , State_Id
                 , State_Name
                 , Supplier_Account_Id
                 , Supplier_Account_Number
                 , Supplier_Account_Vendor_Name
                 , Commodity_Name
                 , Supplier_Account_Config_Id
                 , Supplier_Account_begin_Dt
                 , Supplier_Account_End_Dt
             )
        SELECT
            chsite.Client_Name
            , chsite.Site_Id
            , chsite.Site_name
            , chsite.City
            , chsite.State_Id
            , chsite.State_Name
            , chasupp.Account_Id AS Supplier_Account_Id
            , chasupp.Account_Number AS Supplier_Account_Number
            , chasupp.Account_Vendor_Name AS Supplier_Account_Vendor_Name
            , comm.Commodity_Name
            , chasupp.Supplier_Account_Config_Id
            , chasupp.Supplier_Account_begin_Dt
            , chasupp.Supplier_Account_End_Dt
        FROM
            Core.Client_Hier chsite
            INNER JOIN Core.Client_Hier_Account chautil
                ON chsite.Client_Hier_Id = chautil.Client_Hier_Id
            INNER JOIN Core.Client_Hier_Account chasupp
                ON chautil.Meter_Id = chasupp.Meter_Id
            INNER JOIN dbo.Commodity comm
                ON chasupp.Commodity_Id = comm.Commodity_Id
        WHERE
            chsite.Client_Id = @Client_Id
            AND chasupp.Account_Type = 'Supplier'
            AND chasupp.Supplier_Contract_ID = -1
            AND chautil.Account_Type = 'Utility'
            AND (   @Site_Id IS NULL
                    OR  chsite.Site_Id = @Site_Id)
            AND (   @Account_Number IS NULL
                    OR  chasupp.Account_Number LIKE '%' + @Account_Number + '%')
            AND (   @City IS NULL
                    OR  chsite.City LIKE '%' + @City + '%')
            AND (   @State_Id IS NULL
                    OR  chsite.State_Id = @State_Id)
            AND (   @Supplier_Account_Id IS NULL
                    OR  chasupp.Account_Id = @Supplier_Account_Id)
        GROUP BY
            chsite.Client_Name
            , chsite.Site_Id
            , chsite.Site_name
            , chsite.City
            , chsite.State_Id
            , chsite.State_Name
            , chasupp.Account_Id
            , chasupp.Account_Number
            , chasupp.Account_Vendor_Name
            , comm.Commodity_Name
            , chasupp.Supplier_Account_Config_Id
            , chasupp.Supplier_Account_begin_Dt
            , chasupp.Supplier_Account_End_Dt;



        WITH Cte_Acc_Site_Cnt
        AS (
               SELECT
                    Supplier_Account_Id
                    , COUNT(dsa.Site_Id) AS Site_Cnt
               FROM
                    #DMO_Supplier_Account dsa
               GROUP BY
                   Supplier_Account_Id
           )
             , Cte_Accs
        AS (
               SELECT
                    dsa.Client_Name
                    , CASE WHEN ca.Site_Cnt > 1 THEN '-1'
                          ELSE dsa.Site_Id
                      END AS Site_Id
                    , CASE WHEN ca.Site_Cnt > 1 THEN 'Multiple'
                          ELSE dsa.Site_name
                      END AS Site_name
                    , CASE WHEN ca.Site_Cnt > 1 THEN 'Multiple'
                          ELSE dsa.City
                      END AS City
                    , CASE WHEN ca.Site_Cnt > 1 THEN '-1'
                          ELSE dsa.State_Id
                      END AS State_Id
                    , CASE WHEN ca.Site_Cnt > 1 THEN 'Multiple'
                          ELSE dsa.State_Name
                      END AS State_Name
                    , dsa.Supplier_Account_Id
                    , dsa.Supplier_Account_Number
                    , dsa.Supplier_Account_Vendor_Name
                    , dsa.Commodity_Name
                    , ROW_NUMBER() OVER (ORDER BY
                                             dsa.Supplier_Account_Id) Row_Num
                    , COUNT(1) OVER () Total_Rows
                    , dsa.Supplier_Account_Config_Id
                    , dsa.Supplier_Account_begin_Dt
                    , dsa.Supplier_Account_End_Dt
               FROM
                    #DMO_Supplier_Account dsa
                    INNER JOIN Cte_Acc_Site_Cnt ca
                        ON dsa.Supplier_Account_Id = ca.Supplier_Account_Id
               GROUP BY
                   dsa.Client_Name
                   , CASE WHEN ca.Site_Cnt > 1 THEN '-1'
                         ELSE dsa.Site_Id
                     END
                   , CASE WHEN ca.Site_Cnt > 1 THEN 'Multiple'
                         ELSE dsa.Site_name
                     END
                   , CASE WHEN ca.Site_Cnt > 1 THEN 'Multiple'
                         ELSE dsa.City
                     END
                   , CASE WHEN ca.Site_Cnt > 1 THEN '-1'
                         ELSE dsa.State_Id
                     END
                   , CASE WHEN ca.Site_Cnt > 1 THEN 'Multiple'
                         ELSE dsa.State_Name
                     END
                   , dsa.Supplier_Account_Id
                   , dsa.Supplier_Account_Number
                   , dsa.Supplier_Account_Vendor_Name
                   , dsa.Commodity_Name
                   , dsa.Supplier_Account_Config_Id
                   , dsa.Supplier_Account_begin_Dt
                   , dsa.Supplier_Account_End_Dt
           )
        SELECT
            acc.Client_Name
            , acc.Site_Id
            , acc.Site_name
            , acc.City
            , acc.State_Id
            , acc.State_Name
            , acc.Supplier_Account_Id
            , acc.Supplier_Account_Number
            , acc.Supplier_Account_Vendor_Name
            , acc.Commodity_Name
            , acc.Row_Num
            , acc.Total_Rows
            , acc.Supplier_Account_Config_Id
            , acc.Supplier_Account_begin_Dt
            , acc.Supplier_Account_End_Dt
        FROM
            Cte_Accs acc
        WHERE
            acc.Row_Num BETWEEN @StartIndex
                        AND     @EndIndex;


    END;


    ;



GO
GRANT EXECUTE ON  [dbo].[DMO_Supplier_Account_Sel_By_Client] TO [CBMSApplication]
GO
