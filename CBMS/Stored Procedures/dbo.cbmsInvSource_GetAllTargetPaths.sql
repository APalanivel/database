
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
             
NAME: cbmsInvSource_GetAllTargetPaths

DESCRIPTION:

To get the invoice source target paths

INPUT PARAMETERS:
NAME							DATATYPE		DEFAULT		DESCRIPTION
-----------------------------------------------------------------------
@MyAccountId					INT

OUTPUT PARAMETERS:
NAME							DATATYPE		DEFAULT		DESCRIPTION
-----------------------------------------------------------------------
USAGE EXAMPLES:
-----------------------------------------------------------------------

	EXEC dbo.cbmsInvSource_GetAllTargetPaths -1

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------------
HG			Harihara Suthan Ganesan

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------------
HG			2015-03-26	Comments header added
						Modified for Invoice scraping recipient enh to sort it by Priority Folder name
HG			2015-04-08	Modified to return only the priority and Regular folders

*/
CREATE PROCEDURE [dbo].[cbmsInvSource_GetAllTargetPaths] ( @MyAccountId INT )
AS 
BEGIN

      SELECT
            Target_Folder_Path
      FROM
            dbo.Inv_Source
      WHERE
            inv_source_type_id = 1247
            AND is_active = 1
            AND ( Target_Folder_Path LIKE '%\Priority%'
                  OR Target_Folder_Path LIKE '%\Regular%' )
      GROUP BY
            Target_Folder_Path
      ORDER BY
            CASE WHEN CHARINDEX('priority', Target_Folder_Path, 1) > 0 THEN 1
                 ELSE 2
            END

END;


;
GO

GRANT EXECUTE ON  [dbo].[cbmsInvSource_GetAllTargetPaths] TO [CBMSApplication]
GO
