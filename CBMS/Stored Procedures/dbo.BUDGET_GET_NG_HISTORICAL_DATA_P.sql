
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*=============================================================
Name:
    CBMS.dbo.BUDGET_GET_NG_HISTORICAL_DATA_P
    
Description:
    What is the business description/function of the procedure.

Input Parameters:
	Name					DataType		Default	Description
---------------------------------------------------------------
	@BudgetID				INT
	@From_Month			DATETIME
	@To_Month				DATETIME

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	exec BUDGET_GET_NG_HISTORICAL_DATA_P 1, '01/01/2005', '12/01/2005'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
    AP		Athmaram Pabbathi

MODIFICATIONS:
Initials	Date		    Modification
--------	----------	----------------------------------------------
AP		07/26/2011	Removed @User_ID & @Session_ID parameters and 
					Modified the SP logic and Remove Cost_Usage table & use Cost_Usage_Account_Dtl, Bucket_Master, Commodity tables
AP		08/23/2011    Added qualifiers to all objects with the owner name.
=============================================================*/

CREATE PROCEDURE [dbo].[BUDGET_GET_NG_HISTORICAL_DATA_P]
      ( 
       @Budget_Account_ID INT
      ,@From_Month DATETIME
      ,@To_Month DATETIME )
AS 
BEGIN
      SET NOCOUNT ON

      DECLARE
            @Consumption_Conv_Unit_ID INT
           ,@Currency_Conv_Unit_ID INT
           ,@Currency_Group_ID INT
           ,@Account_ID INT
           ,@Commodity_Id INT
           
                 
      SELECT
            @Consumption_Conv_Unit_ID = uom.Entity_ID
           ,@Commodity_Id = com.Commodity_Id
      FROM
            dbo.Commodity com
            INNER JOIN dbo.Entity uom
                  ON uom.Entity_Type = com.UOM_Entity_Type
      WHERE
            com.Commodity_Name = 'Natural Gas'
            AND uom.Entity_Name = 'MMbtu'
            
      SELECT
            @Currency_Conv_Unit_ID = cu.Currency_Unit_Id
      FROM
            dbo.Currency_Unit cu
      WHERE
            cu.Currency_Unit_Name = 'USD'
 
      SELECT
            @Currency_Group_ID = cg.currency_group_id
      FROM
            dbo.Currency_Group cg
      WHERE
            cg.Currency_Group_Name = 'Standard'

--Fetching the Account ID based on Budget Account ID            
      SELECT TOP 1
            @Account_ID = ba.Account_ID
      FROM
            dbo.Budget_Account ba
      WHERE
            ba.Budget_Account_ID = @Budget_Account_ID ;


      WITH  CTE_Max_Currency_Conv_Date
              AS ( SELECT
                        max(cuc.Conversion_Date) AS Max_Conversion_Date
                       ,cuc.Base_Unit_Id
                       ,cuc.Converted_Unit_Id
                       ,cuc.Currency_Group_Id
                   FROM
                        dbo.Currency_Unit_Conversion cuc
                   WHERE
                        cuc.Converted_Unit_ID = @Currency_Conv_Unit_ID
                        AND cuc.Currency_Group_ID = @Currency_Group_ID
                   GROUP BY
                        cuc.Base_Unit_Id
                       ,cuc.Converted_Unit_Id
                       ,cuc.Currency_Group_Id),
            CTE_Cost_Usage_Account_Info
              AS ( SELECT
                        CUAD.Service_Month AS ServiceMonth
                       ,isnull(( case WHEN BM.Bucket_Name = 'Total Usage' THEN CUAD.Bucket_Value * UC.Conversion_Factor
                                 END ), 0) AS Usage_Value
                       ,isnull(( case WHEN BM.Bucket_Name = 'Taxes' THEN CUAD.Bucket_Value * CurConv.Conversion_Factor
                                 END ), 0) AS Tax
                       ,isnull(( case WHEN BM.Bucket_Name = 'Total Cost' THEN CUAD.Bucket_Value * CurConv.Conversion_Factor
                                 END ), 0) AS Total_Cost
                   FROM
                        dbo.Cost_Usage_Account_Dtl CUAD
                        INNER JOIN dbo.Bucket_Master BM
                              ON BM.Bucket_Master_Id = CUAD.Bucket_Master_Id
		--Consumption Unit Converstion		
                        LEFT OUTER JOIN dbo.Consumption_Unit_Conversion UC
                              ON UC.Base_Unit_ID = CUAD.UOM_Type_ID
                                 AND UC.Converted_Unit_ID = @Consumption_Conv_Unit_ID
		--Currency Unit Converstion		
                        LEFT OUTER JOIN ( dbo.Currency_Unit_Conversion CurConv
                                          INNER JOIN CTE_Max_Currency_Conv_Date max_date
                                                ON CurConv.Base_Unit_Id = max_date.Base_Unit_Id
                                                   AND CurConv.Converted_Unit_ID = max_date.Converted_Unit_ID
                                                   AND CurConv.Currency_Group_ID = max_date.Currency_Group_ID
                                                   AND CurConv.Conversion_Date = max_date.Max_Conversion_Date )
                                          ON CurConv.Base_Unit_ID = CUAD.Currency_Unit_ID
                                             AND CurConv.Converted_Unit_ID = @Currency_Conv_Unit_ID
                                             AND CurConv.Currency_Group_ID = @Currency_Group_ID
                   WHERE
                        CUAD.Account_ID = @Account_ID
                        AND BM.Bucket_Name IN ( 'Total Cost', 'Total Usage', 'Taxes' ) --To eliminate other un-used records
                        AND CUAD.Service_Month BETWEEN @From_Month
                                               AND     @To_Month
                        AND bm.Commodity_Id = @Commodity_Id)
            SELECT
                  CUA.ServiceMonth
                 ,sum(CUA.Usage_Value) Usage_Value
                 ,sum(CUA.Tax) Tax
                 ,sum(CUA.Total_Cost) Total_Cost
            FROM
                  CTE_Cost_Usage_Account_Info CUA
            GROUP BY
                  CUA.ServiceMonth
	
            
END
;
GO

GRANT EXECUTE ON  [dbo].[BUDGET_GET_NG_HISTORICAL_DATA_P] TO [CBMSApplication]
GO
