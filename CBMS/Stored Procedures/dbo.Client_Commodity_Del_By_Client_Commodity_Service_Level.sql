SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	
	dbo.Client_Commodity_Del_By_Client_Commodity_Service_Level

DESCRIPTION:


INPUT PARAMETERS:
	Name						DataType	Default	    Description
------------------------------------------------------------------------------
	@Client_id 					INT       			
	@Commodity_Id				INT
	@Commodity_Service_Cd_Value VARCHAR(25)				Invoice/GHG
	@Hier_Level_Cd_Value		VARCHAR(25)				Site/Account

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Client_Commodity_Del_By_Client_Commodity_Service_Level 10069,67,'Invoice','Site'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan G
	SP			Sandeep Pigilam


MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG        	1/28/2010	Created
 DMR		  09/10/2010 Modified for Quoted_Identifier
 SP				2016-11-09	Variance Enhancement Phase II VTE-15,Added DELETE FOR Client_Commodity_Variance_Test_Config table.
 


******/
CREATE PROCEDURE [dbo].[Client_Commodity_Del_By_Client_Commodity_Service_Level]
      ( 
       @Client_id INT
      ,@Commodity_Id INT
      ,@Commodity_Service_Cd_Value VARCHAR(25)
      ,@Hier_Level_Cd_Value VARCHAR(25) )
AS 
BEGIN

      SET QUOTED_IDENTIFIER ON
      SET NOCOUNT ON;

      DELETE
            ccv
      FROM
            dbo.Client_Commodity_Variance_Test_Config ccv
            INNER JOIN Core.Client_Commodity cc
                  ON ccv.Client_Commodity_Id = cc.Client_Commodity_Id
            JOIN dbo.Code Service_type
                  ON Service_type.Code_Id = cc.Commodity_Service_Cd
            JOIN dbo.Code lvl
                  ON lvl.Code_Id = cc.Hier_Level_Cd
      WHERE
            cc.Client_Id = @Client_id
            AND cc.Commodity_Id = @Commodity_Id
            AND Service_type.Code_Value = @Commodity_Service_Cd_Value
            AND lvl.Code_Value = @Hier_Level_Cd_Value
            
     
            
         

END

;
GO

GRANT EXECUTE ON  [dbo].[Client_Commodity_Del_By_Client_Commodity_Service_Level] TO [CBMSApplication]
GO
