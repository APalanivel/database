SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Rfp_Legacy_Bids_Sel_By_Sr_Account_Group_Id]
           
DESCRIPTION:             
			To get bid responses placed by suppliers 
			
INPUT PARAMETERS:            
	Name					DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Sr_Account_Group_Id	INT
    @Is_Bid_Group			INT
    @Sr_Rfp_Id				INT
    @Language_CD			INT			NULL
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
	SELECT TOP 10 term.SR_ACCOUNT_GROUP_ID,rfpacc.SR_RFP_ID FROM dbo.SR_RFP_TERM_PRODUCT_MAP tpm 
	INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
	INNER JOIN dbo.SR_RFP_ACCOUNT rfpacc ON term.SR_ACCOUNT_GROUP_ID = rfpacc.SR_RFP_ACCOUNT_ID
	INNER JOIN dbo.SR_RFP rfp ON rfpacc.SR_RFP_ID = rfp.SR_RFP_ID
	WHERE term.IS_BID_GROUP = 0 AND rfpacc.SR_RFP_ID<10000 AND rfpacc.SR_RFP_ID>9000
			AND tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID IS NOT NULL
			AND rfp.COMMODITY_TYPE_ID = 291
	
	--EP
	EXEC dbo.Sr_Rfp_Legacy_Bids_Sel_By_Sr_Account_Group_Id  12058280,0,9389
	--NG
	EXEC dbo.Sr_Rfp_Legacy_Bids_Sel_By_Sr_Account_Group_Id  12062007,0,9706
	
	EXEC dbo.Sr_Rfp_Legacy_Bids_Sel_By_Sr_Account_Group_Id  10010238,1,11470 
	
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-04-16	Global Sourcing - Phase3 - GCS-530 Created
******/
CREATE PROCEDURE [dbo].[Sr_Rfp_Legacy_Bids_Sel_By_Sr_Account_Group_Id]
      ( 
       @Sr_Account_Group_Id INT
      ,@Is_Bid_Group INT
      ,@Sr_Rfp_Id INT
      ,@Language_CD INT = NULL )
AS 
BEGIN

/*Service conditions questions are static and are not available in database only answers saving in
column format and now this service conditions are made dynamic and this script will be called only
for old RFPs
*/

      SET NOCOUNT ON;
      
      DECLARE
            @Vendor_Header VARCHAR(MAX)
           ,@Sql_Str NVARCHAR(MAX)
           ,@Commodity_Name VARCHAR(50)
      
      SELECT
            @Language_CD = isnull(@Language_CD, cd.Code_Id)
      FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cd.Code_Value = 'en-US'
            AND cs.Codeset_Name = 'LocalizationLanguage';
      
      SELECT
            @Commodity_Name = comm.Commodity_Name
      FROM
            dbo.SR_RFP rfp
            INNER JOIN dbo.Commodity comm
                  ON rfp.COMMODITY_TYPE_ID = comm.Commodity_Id
      WHERE
            rfp.SR_RFP_ID = @Sr_Rfp_Id
            
      CREATE TABLE #Bids
            ( 
             Category_Name_Locale_Value NVARCHAR(255)
            ,Category_Display_Seq INT
            ,Question_Label_Locale_Value NVARCHAR(MAX)
            ,Question_Display_Seq INT
            ,Response NVARCHAR(MAX)
            ,VENDOR_NAME VARCHAR(200)
            ,Vendor_Contact VARCHAR(100)
            ,Term VARCHAR(50)
            ,PRODUCT_NAME VARCHAR(200)
            ,FROM_MONTH DATETIME
            ,TO_MONTH DATETIME )
           
      
                  

      IF @Commodity_Name = 'Electric Power' 
            BEGIN
                     ---------------Bid Requirements-----------------Start----------------   
                  INSERT      INTO #Bids
                              ( 
                               Category_Name_Locale_Value
                              ,Category_Display_Seq
                              ,Question_Label_Locale_Value
                              ,Question_Display_Seq
                              ,Response
                              ,VENDOR_NAME
                              ,Vendor_Contact
                              ,Term
                              ,PRODUCT_NAME
                              ,FROM_MONTH
                              ,TO_MONTH )
                              SELECT
                                    'Bid Requirements' AS Category_Name_Locale_Value
                                   ,-1 AS Category_Display_Seq
                                   ,'Delivery Point' AS Question_Label_Locale_Value
                                   ,1 AS Question_Display_Seq
                                   ,ent.ENTITY_NAME AS Response
                                   ,vndr.VENDOR_NAME
                                   ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                   ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                         ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                    END AS Term
                                   ,srpp.PRODUCT_NAME
                                   ,term.FROM_MONTH
                                   ,term.TO_MONTH
                              FROM
                                    dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                    INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                          ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                          ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO ui
                                          ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                    INNER JOIN dbo.VENDOR vndr
                                          ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                    INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                          ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                    INNER JOIN dbo.SR_RFP rfp
                                          ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                    INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                          ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                    INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                          ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                    INNER JOIN dbo.SR_RFP_BID bid
                                          ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                    LEFT JOIN dbo.SR_RFP_BID_REQUIREMENTS srbr
                                          ON bid.SR_RFP_BID_REQUIREMENTS_ID = srbr.SR_RFP_BID_REQUIREMENTS_ID
                                    LEFT JOIN dbo.ENTITY ent
                                          ON srbr.DELIVERY_POINT_POWER_TYPE_ID = ent.ENTITY_ID
                              WHERE
                                    term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                    AND term.IS_BID_GROUP = @Is_Bid_Group
                              
                  INSERT      INTO #Bids
                              ( 
                               Category_Name_Locale_Value
                              ,Category_Display_Seq
                              ,Question_Label_Locale_Value
                              ,Question_Display_Seq
                              ,Response
                              ,VENDOR_NAME
                              ,Vendor_Contact
                              ,Term
                              ,PRODUCT_NAME
                              ,FROM_MONTH
                              ,TO_MONTH )
                              SELECT
                                    'Bid Requirements' AS Category_Name_Locale_Value
                                   ,-1 AS Category_Display_Seq
                                   ,'Transportation Service Level' AS Question_Label_Locale_Value
                                   ,2 AS Question_Display_Seq
                                   ,ent.ENTITY_NAME AS Response
                                   ,vndr.VENDOR_NAME
                                   ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                   ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                         ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                    END AS Term
                                   ,srpp.PRODUCT_NAME
                                   ,term.FROM_MONTH
                                   ,term.TO_MONTH
                              FROM
                                    dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                    INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                          ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                          ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO ui
                                          ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                    INNER JOIN dbo.VENDOR vndr
                                          ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                    INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                          ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                    INNER JOIN dbo.SR_RFP rfp
                                          ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                    INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                          ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                    INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                          ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                    INNER JOIN dbo.SR_RFP_BID bid
                                          ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                    LEFT JOIN dbo.SR_RFP_BID_REQUIREMENTS srbr
                                          ON bid.SR_RFP_BID_REQUIREMENTS_ID = srbr.SR_RFP_BID_REQUIREMENTS_ID
                                    LEFT JOIN dbo.ENTITY ent
                                          ON srbr.SERVICE_LEVEL_POWER_TYPE_ID = ent.ENTITY_ID
                              WHERE
                                    term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                    AND term.IS_BID_GROUP = @Is_Bid_Group
                              
                  INSERT      INTO #Bids
                              ( 
                               Category_Name_Locale_Value
                              ,Category_Display_Seq
                              ,Question_Label_Locale_Value
                              ,Question_Display_Seq
                              ,Response
                              ,VENDOR_NAME
                              ,Vendor_Contact
                              ,Term
                              ,PRODUCT_NAME
                              ,FROM_MONTH
                              ,TO_MONTH )
                              SELECT
                                    'Bid Requirements' AS Category_Name_Locale_Value
                                   ,-1 AS Category_Display_Seq
                                   ,'Comments' AS Question_Label_Locale_Value
                                   ,3 AS Question_Display_Seq
                                   ,srbr.COMMENTS AS Response
                                   ,vndr.VENDOR_NAME
                                   ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                   ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                         ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                    END AS Term
                                   ,srpp.PRODUCT_NAME
                                   ,term.FROM_MONTH
                                   ,term.TO_MONTH
                              FROM
                                    dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                    INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                          ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                          ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO ui
                                          ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                    INNER JOIN dbo.VENDOR vndr
                                          ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                    INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                          ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                    INNER JOIN dbo.SR_RFP rfp
                                          ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                    INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                          ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                    INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                          ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                    INNER JOIN dbo.SR_RFP_BID bid
                                          ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                    LEFT JOIN dbo.SR_RFP_BID_REQUIREMENTS srbr
                                          ON bid.SR_RFP_BID_REQUIREMENTS_ID = srbr.SR_RFP_BID_REQUIREMENTS_ID
                              WHERE
                                    term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                    AND term.IS_BID_GROUP = @Is_Bid_Group
                                    
                  INSERT      INTO #Bids
                              ( 
                               Category_Name_Locale_Value
                              ,Category_Display_Seq
                              ,Question_Label_Locale_Value
                              ,Question_Display_Seq
                              ,Response
                              ,VENDOR_NAME
                              ,Vendor_Contact
                              ,Term
                              ,PRODUCT_NAME
                              ,FROM_MONTH
                              ,TO_MONTH )
                              SELECT
                                    'Pricing Scope' AS Category_Name_Locale_Value
                                   ,1 AS Category_Display_Seq
                                   ,'Prices includes losses' AS Question_Label_Locale_Value
                                   ,1 AS Question_Display_Seq
                                   ,ent.ENTITY_NAME + isnull(( '    Loss%: ' + srps.LOSS_PERCENT ), '') AS Response
                                   ,vndr.VENDOR_NAME
                                   ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                   ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                         ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                    END AS Term
                                   ,srpp.PRODUCT_NAME
                                   ,term.FROM_MONTH
                                   ,term.TO_MONTH
                              FROM
                                    dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                    INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                          ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                          ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO ui
                                          ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                    INNER JOIN dbo.VENDOR vndr
                                          ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                    INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                          ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                    INNER JOIN dbo.SR_RFP rfp
                                          ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                    INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                          ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                    INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                          ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                    INNER JOIN dbo.SR_RFP_BID bid
                                          ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                    LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                          ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                    LEFT JOIN dbo.SR_RFP_PRICING_SCOPE srps
                                          ON srss.SR_RFP_PRICING_SCOPE_ID = srps.SR_RFP_PRICING_SCOPE_ID
                                    LEFT JOIN dbo.ENTITY ent
                                          ON srps.PRICE_INCLUDE_LOSES_TYPE_ID = ent.ENTITY_ID
                              WHERE
                                    term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                    AND term.IS_BID_GROUP = @Is_Bid_Group
                  
                  INSERT      INTO #Bids
                              ( 
                               Category_Name_Locale_Value
                              ,Category_Display_Seq
                              ,Question_Label_Locale_Value
                              ,Question_Display_Seq
                              ,Response
                              ,VENDOR_NAME
                              ,Vendor_Contact
                              ,Term
                              ,PRODUCT_NAME
                              ,FROM_MONTH
                              ,TO_MONTH )
                              SELECT
                                    'Pricing Scope' AS Category_Name_Locale_Value
                                   ,1 AS Category_Display_Seq
                                   ,'Prices includes ancillary services' AS Question_Label_Locale_Value
                                   ,2 AS Question_Display_Seq
                                   ,ent.ENTITY_NAME + isnull(( '    Estimated Cost: ' + srps.ANCILIARY_SERVICE_ESTIMATED_COST ), '') AS Response
                                   ,vndr.VENDOR_NAME
                                   ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                   ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                         ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                    END AS Term
                                   ,srpp.PRODUCT_NAME
                                   ,term.FROM_MONTH
                                   ,term.TO_MONTH
                              FROM
                                    dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                    INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                          ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                          ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO ui
                                          ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                    INNER JOIN dbo.VENDOR vndr
                                          ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                    INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                          ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                    INNER JOIN dbo.SR_RFP rfp
                                          ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                    INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                          ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                    INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                          ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                    INNER JOIN dbo.SR_RFP_BID bid
                                          ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                    LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                          ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                    LEFT JOIN dbo.SR_RFP_PRICING_SCOPE srps
                                          ON srss.SR_RFP_PRICING_SCOPE_ID = srps.SR_RFP_PRICING_SCOPE_ID
                                    LEFT JOIN dbo.ENTITY ent
                                          ON srps.PRICE_INCLUDE_ANCILIARY_SERVICE_TYPE_ID = ent.ENTITY_ID
                              WHERE
                                    term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                    AND term.IS_BID_GROUP = @Is_Bid_Group
                  
                  INSERT      INTO #Bids
                              ( 
                               Category_Name_Locale_Value
                              ,Category_Display_Seq
                              ,Question_Label_Locale_Value
                              ,Question_Display_Seq
                              ,Response
                              ,VENDOR_NAME
                              ,Vendor_Contact
                              ,Term
                              ,PRODUCT_NAME
                              ,FROM_MONTH
                              ,TO_MONTH )
                              SELECT
                                    'Pricing Scope' AS Category_Name_Locale_Value
                                   ,1 AS Category_Display_Seq
                                   ,'Prices includes capacity charges' AS Question_Label_Locale_Value
                                   ,3 AS Question_Display_Seq
                                   ,ent.ENTITY_NAME + isnull(( '    Estimated Cost: ' + srps.CAPACITY_CHARGES_ESTIMATED_COST ), '') AS Response
                                   ,vndr.VENDOR_NAME
                                   ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                   ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                         ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                    END AS Term
                                   ,srpp.PRODUCT_NAME
                                   ,term.FROM_MONTH
                                   ,term.TO_MONTH
                              FROM
                                    dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                    INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                          ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                          ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO ui
                                          ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                    INNER JOIN dbo.VENDOR vndr
                                          ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                    INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                          ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                    INNER JOIN dbo.SR_RFP rfp
                                          ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                    INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                          ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                    INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                          ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                    INNER JOIN dbo.SR_RFP_BID bid
                                          ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                    LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                          ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                    LEFT JOIN dbo.SR_RFP_PRICING_SCOPE srps
                                          ON srss.SR_RFP_PRICING_SCOPE_ID = srps.SR_RFP_PRICING_SCOPE_ID
                                    LEFT JOIN dbo.ENTITY ent
                                          ON srps.PRICE_INCLUDE_CAPACITY_CHARGES_TYPE_ID = ent.ENTITY_ID
                              WHERE
                                    term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                    AND term.IS_BID_GROUP = @Is_Bid_Group
                                    
                  INSERT      INTO #Bids
                              ( 
                               Category_Name_Locale_Value
                              ,Category_Display_Seq
                              ,Question_Label_Locale_Value
                              ,Question_Display_Seq
                              ,Response
                              ,VENDOR_NAME
                              ,Vendor_Contact
                              ,Term
                              ,PRODUCT_NAME
                              ,FROM_MONTH
                              ,TO_MONTH )
                              SELECT
                                    'Pricing Scope' AS Category_Name_Locale_Value
                                   ,1 AS Category_Display_Seq
                                   ,'Prices includes ISO charges' AS Question_Label_Locale_Value
                                   ,4 AS Question_Display_Seq
                                   ,ent.ENTITY_NAME + isnull(( '    Estimated Cost: ' + srps.ISO_CHARGES_ESTIMATED_COST ), '') AS Response
                                   ,vndr.VENDOR_NAME
                                   ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                   ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                         ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                    END AS Term
                                   ,srpp.PRODUCT_NAME
                                   ,term.FROM_MONTH
                                   ,term.TO_MONTH
                              FROM
                                    dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                    INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                          ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                          ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO ui
                                          ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                    INNER JOIN dbo.VENDOR vndr
                                          ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                    INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                          ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                    INNER JOIN dbo.SR_RFP rfp
                                          ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                    INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                          ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                    INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                          ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                    INNER JOIN dbo.SR_RFP_BID bid
                                          ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                    LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                          ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                    LEFT JOIN dbo.SR_RFP_PRICING_SCOPE srps
                                          ON srss.SR_RFP_PRICING_SCOPE_ID = srps.SR_RFP_PRICING_SCOPE_ID
                                    LEFT JOIN dbo.ENTITY ent
                                          ON srps.PRICE_INCLUDE_ISO_CHARGES_TYPE_ID = ent.ENTITY_ID
                              WHERE
                                    term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                    AND term.IS_BID_GROUP = @Is_Bid_Group
                  
                  INSERT      INTO #Bids
                              ( 
                               Category_Name_Locale_Value
                              ,Category_Display_Seq
                              ,Question_Label_Locale_Value
                              ,Question_Display_Seq
                              ,Response
                              ,VENDOR_NAME
                              ,Vendor_Contact
                              ,Term
                              ,PRODUCT_NAME
                              ,FROM_MONTH
                              ,TO_MONTH )
                              SELECT
                                    'Pricing Scope' AS Category_Name_Locale_Value
                                   ,1 AS Category_Display_Seq
                                   ,'Prices includes transmission' AS Question_Label_Locale_Value
                                   ,5 AS Question_Display_Seq
                                   ,ent.ENTITY_NAME + isnull(( '    Estimated Cost: ' + srps.TRANSMISSION_ESTIMATED_COST ), '') AS Response
                                   ,vndr.VENDOR_NAME
                                   ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                   ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                         ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                    END AS Term
                                   ,srpp.PRODUCT_NAME
                                   ,term.FROM_MONTH
                                   ,term.TO_MONTH
                              FROM
                                    dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                    INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                          ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                          ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO ui
                                          ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                    INNER JOIN dbo.VENDOR vndr
                                          ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                    INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                          ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                    INNER JOIN dbo.SR_RFP rfp
                                          ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                    INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                          ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                    INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                          ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                    INNER JOIN dbo.SR_RFP_BID bid
                                          ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                    LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                          ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                    LEFT JOIN dbo.SR_RFP_PRICING_SCOPE srps
                                          ON srss.SR_RFP_PRICING_SCOPE_ID = srps.SR_RFP_PRICING_SCOPE_ID
                                    LEFT JOIN dbo.ENTITY ent
                                          ON srps.PRICE_INCLUDE_TRANSMISSION_TYPE_ID = ent.ENTITY_ID
                              WHERE
                                    term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                    AND term.IS_BID_GROUP = @Is_Bid_Group
                  
                  INSERT      INTO #Bids
                              ( 
                               Category_Name_Locale_Value
                              ,Category_Display_Seq
                              ,Question_Label_Locale_Value
                              ,Question_Display_Seq
                              ,Response
                              ,VENDOR_NAME
                              ,Vendor_Contact
                              ,Term
                              ,PRODUCT_NAME
                              ,FROM_MONTH
                              ,TO_MONTH )
                              SELECT
                                    'Pricing Scope' AS Category_Name_Locale_Value
                                   ,1 AS Category_Display_Seq
                                   ,'Prices includes taxes' AS Question_Label_Locale_Value
                                   ,6 AS Question_Display_Seq
                                   ,ent.ENTITY_NAME + isnull(( '    Taxes Included: ' + srps.TAXES_ESTIMATED_COST ), '') AS Response
                                   ,vndr.VENDOR_NAME
                                   ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                   ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                         ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                    END AS Term
                                   ,srpp.PRODUCT_NAME
                                   ,term.FROM_MONTH
                                   ,term.TO_MONTH
                              FROM
                                    dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                    INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                          ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                          ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO ui
                                          ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                    INNER JOIN dbo.VENDOR vndr
                                          ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                    INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                          ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                    INNER JOIN dbo.SR_RFP rfp
                                          ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                    INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                          ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                    INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                          ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                    INNER JOIN dbo.SR_RFP_BID bid
                                          ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                    LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                          ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                    LEFT JOIN dbo.SR_RFP_PRICING_SCOPE srps
                                          ON srss.SR_RFP_PRICING_SCOPE_ID = srps.SR_RFP_PRICING_SCOPE_ID
                                    LEFT JOIN dbo.ENTITY ent
                                          ON srps.PRICE_INCLUDE_TAXES_TYPE_ID = ent.ENTITY_ID
                              WHERE
                                    term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                    AND term.IS_BID_GROUP = @Is_Bid_Group
                  
                  INSERT      INTO #Bids
                              ( 
                               Category_Name_Locale_Value
                              ,Category_Display_Seq
                              ,Question_Label_Locale_Value
                              ,Question_Display_Seq
                              ,Response
                              ,VENDOR_NAME
                              ,Vendor_Contact
                              ,Term
                              ,PRODUCT_NAME
                              ,FROM_MONTH
                              ,TO_MONTH )
                              SELECT
                                    'Pricing Scope' AS Category_Name_Locale_Value
                                   ,1 AS Category_Display_Seq
                                   ,'List any other charges invoices by supplier not included above' AS Question_Label_Locale_Value
                                   ,7 AS Question_Display_Seq
                                   ,isnull(( '    Taxes Included: ' + srps.OTHER_CHARGES ), '') AS Response
                                   ,vndr.VENDOR_NAME
                                   ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                   ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                         ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                    END AS Term
                                   ,srpp.PRODUCT_NAME
                                   ,term.FROM_MONTH
                                   ,term.TO_MONTH
                              FROM
                                    dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                    INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                          ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                          ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO ui
                                          ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                    INNER JOIN dbo.VENDOR vndr
                                          ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                    INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                          ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                    INNER JOIN dbo.SR_RFP rfp
                                          ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                    INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                          ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                    INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                          ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                    INNER JOIN dbo.SR_RFP_BID bid
                                          ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                    LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                          ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                    LEFT JOIN dbo.SR_RFP_PRICING_SCOPE srps
                                          ON srss.SR_RFP_PRICING_SCOPE_ID = srps.SR_RFP_PRICING_SCOPE_ID
                              WHERE
                                    term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                    AND term.IS_BID_GROUP = @Is_Bid_Group
                  
                  INSERT      INTO #Bids
                              ( 
                               Category_Name_Locale_Value
                              ,Category_Display_Seq
                              ,Question_Label_Locale_Value
                              ,Question_Display_Seq
                              ,Response
                              ,VENDOR_NAME
                              ,Vendor_Contact
                              ,Term
                              ,PRODUCT_NAME
                              ,FROM_MONTH
                              ,TO_MONTH )
                              SELECT
                                    'Pricing Scope' AS Category_Name_Locale_Value
                                   ,1 AS Category_Display_Seq
                                   ,'Demand cap/ floor applicable' AS Question_Label_Locale_Value
                                   ,8 AS Question_Display_Seq
                                   ,ent.ENTITY_NAME + isnull(( '    Cap/ floor price: ' + srps.CAP_FLOOR_PRICE ), '') AS Response
                                   ,vndr.VENDOR_NAME
                                   ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                   ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                         ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                    END AS Term
                                   ,srpp.PRODUCT_NAME
                                   ,term.FROM_MONTH
                                   ,term.TO_MONTH
                              FROM
                                    dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                    INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                          ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                          ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO ui
                                          ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                    INNER JOIN dbo.VENDOR vndr
                                          ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                    INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                          ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                    INNER JOIN dbo.SR_RFP rfp
                                          ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                    INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                          ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                    INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                          ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                    INNER JOIN dbo.SR_RFP_BID bid
                                          ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                    LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                          ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                    LEFT JOIN dbo.SR_RFP_PRICING_SCOPE srps
                                          ON srss.SR_RFP_PRICING_SCOPE_ID = srps.SR_RFP_PRICING_SCOPE_ID
                                    LEFT JOIN dbo.ENTITY ent
                                          ON srps.DEMAND_CAP_APPLICABLE_TYPE_ID = ent.ENTITY_ID
                              WHERE
                                    term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                    AND term.IS_BID_GROUP = @Is_Bid_Group
                                    
                  INSERT      INTO #Bids
                              ( 
                               Category_Name_Locale_Value
                              ,Category_Display_Seq
                              ,Question_Label_Locale_Value
                              ,Question_Display_Seq
                              ,Response
                              ,VENDOR_NAME
                              ,Vendor_Contact
                              ,Term
                              ,PRODUCT_NAME
                              ,FROM_MONTH
                              ,TO_MONTH )
                              SELECT
                                    'Tolarances' AS Category_Name_Locale_Value
                                   ,2 AS Category_Display_Seq
                                   ,'Is all volume priced at single price' AS Question_Label_Locale_Value
                                   ,1 AS Question_Display_Seq
                                   ,case srbt.IS_PRICE_VARYING
                                      WHEN 1 THEN 'Yes'
                                      WHEN 0 THEN 'No'
                                    END AS Response
                                   ,vndr.VENDOR_NAME
                                   ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                   ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                         ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                    END AS Term
                                   ,srpp.PRODUCT_NAME
                                   ,term.FROM_MONTH
                                   ,term.TO_MONTH
                              FROM
                                    dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                    INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                          ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                          ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO ui
                                          ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                    INNER JOIN dbo.VENDOR vndr
                                          ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                    INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                          ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                    INNER JOIN dbo.SR_RFP rfp
                                          ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                    INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                          ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                    INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                          ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                    INNER JOIN dbo.SR_RFP_BID bid
                                          ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                    LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                          ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                    LEFT JOIN dbo.SR_RFP_BALANCING_TOLERANCE srbt
                                          ON srss.SR_RFP_BALANCING_TOLERANCE_ID = srbt.SR_RFP_BALANCING_TOLERANCE_ID
                              WHERE
                                    term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                    AND term.IS_BID_GROUP = @Is_Bid_Group
                  
                  INSERT      INTO #Bids
                              ( 
                               Category_Name_Locale_Value
                              ,Category_Display_Seq
                              ,Question_Label_Locale_Value
                              ,Question_Display_Seq
                              ,Response
                              ,VENDOR_NAME
                              ,Vendor_Contact
                              ,Term
                              ,PRODUCT_NAME
                              ,FROM_MONTH
                              ,TO_MONTH )
                              SELECT
                                    'Tolarances' AS Category_Name_Locale_Value
                                   ,2 AS Category_Display_Seq
                                   ,'Balancing tolerances' AS Question_Label_Locale_Value
                                   ,2 AS Question_Display_Seq
                                   ,ent.ENTITY_NAME AS Response
                                   ,vndr.VENDOR_NAME
                                   ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                   ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                         ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                    END AS Term
                                   ,srpp.PRODUCT_NAME
                                   ,term.FROM_MONTH
                                   ,term.TO_MONTH
                              FROM
                                    dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                    INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                          ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                          ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO ui
                                          ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                    INNER JOIN dbo.VENDOR vndr
                                          ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                    INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                          ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                    INNER JOIN dbo.SR_RFP rfp
                                          ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                    INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                          ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                    INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                          ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                    INNER JOIN dbo.SR_RFP_BID bid
                                          ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                    LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                          ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                    LEFT JOIN dbo.SR_RFP_BALANCING_TOLERANCE srbt
                                          ON srss.SR_RFP_BALANCING_TOLERANCE_ID = srbt.SR_RFP_BALANCING_TOLERANCE_ID
                                    LEFT JOIN dbo.ENTITY ent
                                          ON srbt.BALANCING_FREQUENCY_TYPE_ID = ent.ENTITY_ID
                              WHERE
                                    term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                    AND term.IS_BID_GROUP = @Is_Bid_Group
                  
                  INSERT      INTO #Bids
                              ( 
                               Category_Name_Locale_Value
                              ,Category_Display_Seq
                              ,Question_Label_Locale_Value
                              ,Question_Display_Seq
                              ,Response
                              ,VENDOR_NAME
                              ,Vendor_Contact
                              ,Term
                              ,PRODUCT_NAME
                              ,FROM_MONTH
                              ,TO_MONTH )
                              SELECT
                                    'Tolarances' AS Category_Name_Locale_Value
                                   ,2 AS Category_Display_Seq
                                   ,'State tolerances percentage' AS Question_Label_Locale_Value
                                   ,3 AS Question_Display_Seq
                                   ,srbt.TOLERANCE_COMMENTS AS Response
                                   ,vndr.VENDOR_NAME
                                   ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                   ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                         ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                    END AS Term
                                   ,srpp.PRODUCT_NAME
                                   ,term.FROM_MONTH
                                   ,term.TO_MONTH
                              FROM
                                    dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                    INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                          ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                          ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO ui
                                          ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                    INNER JOIN dbo.VENDOR vndr
                                          ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                    INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                          ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                    INNER JOIN dbo.SR_RFP rfp
                                          ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                    INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                          ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                    INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                          ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                    INNER JOIN dbo.SR_RFP_BID bid
                                          ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                    LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                          ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                    LEFT JOIN dbo.SR_RFP_BALANCING_TOLERANCE srbt
                                          ON srss.SR_RFP_BALANCING_TOLERANCE_ID = srbt.SR_RFP_BALANCING_TOLERANCE_ID
                              WHERE
                                    term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                    AND term.IS_BID_GROUP = @Is_Bid_Group
                  
                  INSERT      INTO #Bids
                              ( 
                               Category_Name_Locale_Value
                              ,Category_Display_Seq
                              ,Question_Label_Locale_Value
                              ,Question_Display_Seq
                              ,Response
                              ,VENDOR_NAME
                              ,Vendor_Contact
                              ,Term
                              ,PRODUCT_NAME
                              ,FROM_MONTH
                              ,TO_MONTH )
                              SELECT
                                    'Tolarances' AS Category_Name_Locale_Value
                                   ,2 AS Category_Display_Seq
                                   ,'State price mechanism for volumes over tolerance' AS Question_Label_Locale_Value
                                   ,4 AS Question_Display_Seq
                                   ,srbt.ABOVE_TOLERANCE_COMMENTS AS Response
                                   ,vndr.VENDOR_NAME
                                   ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                   ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                         ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                    END AS Term
                                   ,srpp.PRODUCT_NAME
                                   ,term.FROM_MONTH
                                   ,term.TO_MONTH
                              FROM
                                    dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                    INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                          ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                          ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO ui
                                          ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                    INNER JOIN dbo.VENDOR vndr
                                          ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                    INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                          ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                    INNER JOIN dbo.SR_RFP rfp
                                          ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                    INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                          ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                    INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                          ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                    INNER JOIN dbo.SR_RFP_BID bid
                                          ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                    LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                          ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                    LEFT JOIN dbo.SR_RFP_BALANCING_TOLERANCE srbt
                                          ON srss.SR_RFP_BALANCING_TOLERANCE_ID = srbt.SR_RFP_BALANCING_TOLERANCE_ID
                              WHERE
                                    term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                    AND term.IS_BID_GROUP = @Is_Bid_Group
                  
                  INSERT      INTO #Bids
                              ( 
                               Category_Name_Locale_Value
                              ,Category_Display_Seq
                              ,Question_Label_Locale_Value
                              ,Question_Display_Seq
                              ,Response
                              ,VENDOR_NAME
                              ,Vendor_Contact
                              ,Term
                              ,PRODUCT_NAME
                              ,FROM_MONTH
                              ,TO_MONTH )
                              SELECT
                                    'Tolarances' AS Category_Name_Locale_Value
                                   ,2 AS Category_Display_Seq
                                   ,'State price mechanism for volumes under tolerance' AS Question_Label_Locale_Value
                                   ,5 AS Question_Display_Seq
                                   ,srbt.BELOW_TOLERANCE_COMMENTS AS Response
                                   ,vndr.VENDOR_NAME
                                   ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                   ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                         ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                    END AS Term
                                   ,srpp.PRODUCT_NAME
                                   ,term.FROM_MONTH
                                   ,term.TO_MONTH
                              FROM
                                    dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                    INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                          ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                          ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO ui
                                          ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                    INNER JOIN dbo.VENDOR vndr
                                          ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                    INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                          ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                    INNER JOIN dbo.SR_RFP rfp
                                          ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                    INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                          ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                    INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                          ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                    INNER JOIN dbo.SR_RFP_BID bid
                                          ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                    LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                          ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                    LEFT JOIN dbo.SR_RFP_BALANCING_TOLERANCE srbt
                                          ON srss.SR_RFP_BALANCING_TOLERANCE_ID = srbt.SR_RFP_BALANCING_TOLERANCE_ID
                              WHERE
                                    term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                    AND term.IS_BID_GROUP = @Is_Bid_Group
                                    
                  INSERT      INTO #Bids
                              ( 
                               Category_Name_Locale_Value
                              ,Category_Display_Seq
                              ,Question_Label_Locale_Value
                              ,Question_Display_Seq
                              ,Response
                              ,VENDOR_NAME
                              ,Vendor_Contact
                              ,Term
                              ,PRODUCT_NAME
                              ,FROM_MONTH
                              ,TO_MONTH )
                              SELECT
                                    'Miscellaneous' AS Category_Name_Locale_Value
                                   ,3 AS Category_Display_Seq
                                   ,'Define block sizes, if not applicable state N/A' AS Question_Label_Locale_Value
                                   ,1 AS Question_Display_Seq
                                   ,srmp.BLOCK_SIZES AS Response
                                   ,vndr.VENDOR_NAME
                                   ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                   ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                         ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                    END AS Term
                                   ,srpp.PRODUCT_NAME
                                   ,term.FROM_MONTH
                                   ,term.TO_MONTH
                              FROM
                                    dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                    INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                          ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                          ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO ui
                                          ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                    INNER JOIN dbo.VENDOR vndr
                                          ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                    INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                          ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                    INNER JOIN dbo.SR_RFP rfp
                                          ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                    INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                          ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                    INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                          ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                    INNER JOIN dbo.SR_RFP_BID bid
                                          ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                    LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                          ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                    LEFT JOIN dbo.SR_RFP_MISCELLANEOUS_POWER srmp
                                          ON srss.SR_RFP_MISCELLANEOUS_POWER_ID = srmp.SR_RFP_MISCELLANEOUS_POWER_ID
                              WHERE
                                    term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                    AND term.IS_BID_GROUP = @Is_Bid_Group
                  
                  INSERT      INTO #Bids
                              ( 
                               Category_Name_Locale_Value
                              ,Category_Display_Seq
                              ,Question_Label_Locale_Value
                              ,Question_Display_Seq
                              ,Response
                              ,VENDOR_NAME
                              ,Vendor_Contact
                              ,Term
                              ,PRODUCT_NAME
                              ,FROM_MONTH
                              ,TO_MONTH )
                              SELECT
                                    'Miscellaneous' AS Category_Name_Locale_Value
                                   ,3 AS Category_Display_Seq
                                   ,'Prices includes green energy' AS Question_Label_Locale_Value
                                   ,2 AS Question_Display_Seq
                                   ,ent.ENTITY_NAME + isnull(( '    Percentage: ' + srmp.PERCENT_GREEN_ENERGY ), '') AS Response
                                   ,vndr.VENDOR_NAME
                                   ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                   ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                         ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                    END AS Term
                                   ,srpp.PRODUCT_NAME
                                   ,term.FROM_MONTH
                                   ,term.TO_MONTH
                              FROM
                                    dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                    INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                          ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                          ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO ui
                                          ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                    INNER JOIN dbo.VENDOR vndr
                                          ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                    INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                          ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                    INNER JOIN dbo.SR_RFP rfp
                                          ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                    INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                          ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                    INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                          ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                    INNER JOIN dbo.SR_RFP_BID bid
                                          ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                    LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                          ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                    LEFT JOIN dbo.SR_RFP_MISCELLANEOUS_POWER srmp
                                          ON srss.SR_RFP_MISCELLANEOUS_POWER_ID = srmp.SR_RFP_MISCELLANEOUS_POWER_ID
                                    LEFT JOIN dbo.ENTITY ent
                                          ON srmp.PRICE_INCLUDES_GREEN_ENERGY_TYPE_ID = ent.ENTITY_ID
                              WHERE
                                    term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                    AND term.IS_BID_GROUP = @Is_Bid_Group
                  
                  INSERT      INTO #Bids
                              ( 
                               Category_Name_Locale_Value
                              ,Category_Display_Seq
                              ,Question_Label_Locale_Value
                              ,Question_Display_Seq
                              ,Response
                              ,VENDOR_NAME
                              ,Vendor_Contact
                              ,Term
                              ,PRODUCT_NAME
                              ,FROM_MONTH
                              ,TO_MONTH )
                              SELECT
                                    'Miscellaneous' AS Category_Name_Locale_Value
                                   ,3 AS Category_Display_Seq
                                   ,'Supplier allows buyer to convert product to fixed price' AS Question_Label_Locale_Value
                                   ,3 AS Question_Display_Seq
                                   ,ent.ENTITY_NAME + isnull(( '    Restrictions: ' + srmp.CONVERT_PRODUCT_FIXED_PRICE_COMMENTS ), '') AS Response
                                   ,vndr.VENDOR_NAME
                                   ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                   ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                         ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                    END AS Term
                                   ,srpp.PRODUCT_NAME
                                   ,term.FROM_MONTH
                                   ,term.TO_MONTH
                              FROM
                                    dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                    INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                          ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                          ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO ui
                                          ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                    INNER JOIN dbo.VENDOR vndr
                                          ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                    INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                          ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                    INNER JOIN dbo.SR_RFP rfp
                                          ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                    INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                          ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                    INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                          ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                    INNER JOIN dbo.SR_RFP_BID bid
                                          ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                    LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                          ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                    LEFT JOIN dbo.SR_RFP_MISCELLANEOUS_POWER srmp
                                          ON srss.SR_RFP_MISCELLANEOUS_POWER_ID = srmp.SR_RFP_MISCELLANEOUS_POWER_ID
                                    LEFT JOIN dbo.ENTITY ent
                                          ON srmp.CONVERT_PRODUCT_FIXED_PRICE_TYPE_ID = ent.ENTITY_ID
                              WHERE
                                    term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                    AND term.IS_BID_GROUP = @Is_Bid_Group
                  
                  INSERT      INTO #Bids
                              ( 
                               Category_Name_Locale_Value
                              ,Category_Display_Seq
                              ,Question_Label_Locale_Value
                              ,Question_Display_Seq
                              ,Response
                              ,VENDOR_NAME
                              ,Vendor_Contact
                              ,Term
                              ,PRODUCT_NAME
                              ,FROM_MONTH
                              ,TO_MONTH )
                              SELECT
                                    'Miscellaneous' AS Category_Name_Locale_Value
                                   ,3 AS Category_Display_Seq
                                   ,'Supplier allows buyer to buy block hedges' AS Question_Label_Locale_Value
                                   ,4 AS Question_Display_Seq
                                   ,ent.ENTITY_NAME + isnull(( '    Restrictions: ' + srmp.BUYER_BLOCK_HEDGES_COMMENTS ), '') AS Response
                                   ,vndr.VENDOR_NAME
                                   ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                   ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                         ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                    END AS Term
                                   ,srpp.PRODUCT_NAME
                                   ,term.FROM_MONTH
                                   ,term.TO_MONTH
                              FROM
                                    dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                    INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                          ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                          ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO ui
                                          ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                    INNER JOIN dbo.VENDOR vndr
                                          ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                    INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                          ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                    INNER JOIN dbo.SR_RFP rfp
                                          ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                    INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                          ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                    INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                          ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                    INNER JOIN dbo.SR_RFP_BID bid
                                          ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                    LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                          ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                    LEFT JOIN dbo.SR_RFP_MISCELLANEOUS_POWER srmp
                                          ON srss.SR_RFP_MISCELLANEOUS_POWER_ID = srmp.SR_RFP_MISCELLANEOUS_POWER_ID
                                    LEFT JOIN dbo.ENTITY ent
                                          ON srmp.BUYER_BLOCK_HEDGES_TYPE_ID = ent.ENTITY_ID
                              WHERE
                                    term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                    AND term.IS_BID_GROUP = @Is_Bid_Group
                  
                  INSERT      INTO #Bids
                              ( 
                               Category_Name_Locale_Value
                              ,Category_Display_Seq
                              ,Question_Label_Locale_Value
                              ,Question_Display_Seq
                              ,Response
                              ,VENDOR_NAME
                              ,Vendor_Contact
                              ,Term
                              ,PRODUCT_NAME
                              ,FROM_MONTH
                              ,TO_MONTH )
                              SELECT
                                    'Miscellaneous' AS Category_Name_Locale_Value
                                   ,3 AS Category_Display_Seq
                                   ,'Supplier allows heat rate/pricing triggers' AS Question_Label_Locale_Value
                                   ,5 AS Question_Display_Seq
                                   ,ent.ENTITY_NAME + isnull(( '    Restrictions: ' + srmp.HEAT_PRICING_TRIGGERS_COMMENTS ), '') AS Response
                                   ,vndr.VENDOR_NAME
                                   ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                   ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                         ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                    END AS Term
                                   ,srpp.PRODUCT_NAME
                                   ,term.FROM_MONTH
                                   ,term.TO_MONTH
                              FROM
                                    dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                    INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                          ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                          ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO ui
                                          ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                    INNER JOIN dbo.VENDOR vndr
                                          ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                    INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                          ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                    INNER JOIN dbo.SR_RFP rfp
                                          ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                    INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                          ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                    INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                          ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                    INNER JOIN dbo.SR_RFP_BID bid
                                          ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                    LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                          ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                    LEFT JOIN dbo.SR_RFP_MISCELLANEOUS_POWER srmp
                                          ON srss.SR_RFP_MISCELLANEOUS_POWER_ID = srmp.SR_RFP_MISCELLANEOUS_POWER_ID
                                    LEFT JOIN dbo.ENTITY ent
                                          ON srmp.HEAT_PRICING_TRIGGERS_TYPE_ID = ent.ENTITY_ID
                              WHERE
                                    term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                    AND term.IS_BID_GROUP = @Is_Bid_Group
                  
                  
                                    
            END
      ELSE 
            IF @Commodity_Name = 'Natural Gas' 
                  BEGIN
                        INSERT      INTO #Bids
                                    ( 
                                     Category_Name_Locale_Value
                                    ,Category_Display_Seq
                                    ,Question_Label_Locale_Value
                                    ,Question_Display_Seq
                                    ,Response
                                    ,VENDOR_NAME
                                    ,Vendor_Contact
                                    ,Term
                                    ,PRODUCT_NAME
                                    ,FROM_MONTH
                                    ,TO_MONTH )
                                    SELECT
                                          'Bid Requirements' AS Category_Name_Locale_Value
                                         ,-1 AS Category_Display_Seq
                                         ,'Delivery Point' AS Question_Label_Locale_Value
                                         ,1 AS Question_Display_Seq
                                         ,srbr.DELIVERY_POINT AS Response
                                         ,vndr.VENDOR_NAME
                                         ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                         ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                               ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                          END AS Term
                                         ,srpp.PRODUCT_NAME
                                         ,term.FROM_MONTH
                                         ,term.TO_MONTH
                                    FROM
                                          dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                          INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                                ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                          INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                                ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                          INNER JOIN dbo.USER_INFO ui
                                                ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                          INNER JOIN dbo.VENDOR vndr
                                                ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                                ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                          INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                                ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                          INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                                ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                          INNER JOIN dbo.SR_RFP_BID bid
                                                ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                          LEFT JOIN dbo.SR_RFP_BID_REQUIREMENTS srbr
                                                ON bid.SR_RFP_BID_REQUIREMENTS_ID = srbr.SR_RFP_BID_REQUIREMENTS_ID
                                    WHERE
                                          term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                          AND term.IS_BID_GROUP = @Is_Bid_Group
                              
                        INSERT      INTO #Bids
                                    ( 
                                     Category_Name_Locale_Value
                                    ,Category_Display_Seq
                                    ,Question_Label_Locale_Value
                                    ,Question_Display_Seq
                                    ,Response
                                    ,VENDOR_NAME
                                    ,Vendor_Contact
                                    ,Term
                                    ,PRODUCT_NAME
                                    ,FROM_MONTH
                                    ,TO_MONTH )
                                    SELECT
                                          'Bid Requirements' AS Category_Name_Locale_Value
                                         ,-1 AS Category_Display_Seq
                                         ,'Transportation Service Level' AS Question_Label_Locale_Value
                                         ,2 AS Question_Display_Seq
                                         ,ent.ENTITY_NAME AS Response
                                         ,vndr.VENDOR_NAME
                                         ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                         ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                               ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                          END AS Term
                                         ,srpp.PRODUCT_NAME
                                         ,term.FROM_MONTH
                                         ,term.TO_MONTH
                                    FROM
                                          dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                          INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                                ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                          INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                                ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                          INNER JOIN dbo.USER_INFO ui
                                                ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                          INNER JOIN dbo.VENDOR vndr
                                                ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                                ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                          INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                                ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                          INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                                ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                          INNER JOIN dbo.SR_RFP_BID bid
                                                ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                          LEFT JOIN dbo.SR_RFP_BID_REQUIREMENTS srbr
                                                ON bid.SR_RFP_BID_REQUIREMENTS_ID = srbr.SR_RFP_BID_REQUIREMENTS_ID
                                          LEFT JOIN dbo.ENTITY ent
                                                ON srbr.TRANSPORTATION_LEVEL_TYPE_ID = ent.ENTITY_ID
                                    WHERE
                                          term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                          AND term.IS_BID_GROUP = @Is_Bid_Group
                              
                        INSERT      INTO #Bids
                                    ( 
                                     Category_Name_Locale_Value
                                    ,Category_Display_Seq
                                    ,Question_Label_Locale_Value
                                    ,Question_Display_Seq
                                    ,Response
                                    ,VENDOR_NAME
                                    ,Vendor_Contact
                                    ,Term
                                    ,PRODUCT_NAME
                                    ,FROM_MONTH
                                    ,TO_MONTH )
                                    SELECT
                                          'Bid Requirements' AS Category_Name_Locale_Value
                                         ,-1 AS Category_Display_Seq
                                         ,'Supplier responsible for nominations' AS Question_Label_Locale_Value
                                         ,3 AS Question_Display_Seq
                                         ,ent.ENTITY_NAME AS Response
                                         ,vndr.VENDOR_NAME
                                         ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                         ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                               ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                          END AS Term
                                         ,srpp.PRODUCT_NAME
                                         ,term.FROM_MONTH
                                         ,term.TO_MONTH
                                    FROM
                                          dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                          INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                                ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                          INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                                ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                          INNER JOIN dbo.USER_INFO ui
                                                ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                          INNER JOIN dbo.VENDOR vndr
                                                ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                                ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                          INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                                ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                          INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                                ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                          INNER JOIN dbo.SR_RFP_BID bid
                                                ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                          LEFT JOIN dbo.SR_RFP_BID_REQUIREMENTS srbr
                                                ON bid.SR_RFP_BID_REQUIREMENTS_ID = srbr.SR_RFP_BID_REQUIREMENTS_ID
                                          LEFT JOIN dbo.ENTITY ent
                                                ON srbr.NOMINATION_TYPE_ID = ent.ENTITY_ID
                                    WHERE
                                          term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                          AND term.IS_BID_GROUP = @Is_Bid_Group
                              
                        INSERT      INTO #Bids
                                    ( 
                                     Category_Name_Locale_Value
                                    ,Category_Display_Seq
                                    ,Question_Label_Locale_Value
                                    ,Question_Display_Seq
                                    ,Response
                                    ,VENDOR_NAME
                                    ,Vendor_Contact
                                    ,Term
                                    ,PRODUCT_NAME
                                    ,FROM_MONTH
                                    ,TO_MONTH )
                                    SELECT
                                          'Bid Requirements' AS Category_Name_Locale_Value
                                         ,-1 AS Category_Display_Seq
                                         ,'Supplier responsible for balancing' AS Question_Label_Locale_Value
                                         ,4 AS Question_Display_Seq
                                         ,ent.ENTITY_NAME AS Response
                                         ,vndr.VENDOR_NAME
                                         ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                         ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                               ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                          END AS Term
                                         ,srpp.PRODUCT_NAME
                                         ,term.FROM_MONTH
                                         ,term.TO_MONTH
                                    FROM
                                          dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                          INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                                ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                          INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                                ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                          INNER JOIN dbo.USER_INFO ui
                                                ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                          INNER JOIN dbo.VENDOR vndr
                                                ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                                ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                          INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                                ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                          INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                                ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                          INNER JOIN dbo.SR_RFP_BID bid
                                                ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                          LEFT JOIN dbo.SR_RFP_BID_REQUIREMENTS srbr
                                                ON bid.SR_RFP_BID_REQUIREMENTS_ID = srbr.SR_RFP_BID_REQUIREMENTS_ID
                                          LEFT JOIN dbo.ENTITY ent
                                                ON srbr.BALANCING_TYPE_ID = ent.ENTITY_ID
                                    WHERE
                                          term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                          AND term.IS_BID_GROUP = @Is_Bid_Group
                              
                        INSERT      INTO #Bids
                                    ( 
                                     Category_Name_Locale_Value
                                    ,Category_Display_Seq
                                    ,Question_Label_Locale_Value
                                    ,Question_Display_Seq
                                    ,Response
                                    ,VENDOR_NAME
                                    ,Vendor_Contact
                                    ,Term
                                    ,PRODUCT_NAME
                                    ,FROM_MONTH
                                    ,TO_MONTH )
                                    SELECT
                                          'Bid Requirements' AS Category_Name_Locale_Value
                                         ,-1 AS Category_Display_Seq
                                         ,'Comments' AS Question_Label_Locale_Value
                                         ,5 AS Question_Display_Seq
                                         ,srbr.COMMENTS AS Response
                                         ,vndr.VENDOR_NAME
                                         ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                         ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                               ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                          END AS Term
                                         ,srpp.PRODUCT_NAME
                                         ,term.FROM_MONTH
                                         ,term.TO_MONTH
                                    FROM
                                          dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                          INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                                ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                          INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                                ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                          INNER JOIN dbo.USER_INFO ui
                                                ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                          INNER JOIN dbo.VENDOR vndr
                                                ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                                ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                          INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                                ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                          INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                                ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                          INNER JOIN dbo.SR_RFP_BID bid
                                                ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                          LEFT JOIN dbo.SR_RFP_BID_REQUIREMENTS srbr
                                                ON bid.SR_RFP_BID_REQUIREMENTS_ID = srbr.SR_RFP_BID_REQUIREMENTS_ID
                                    WHERE
                                          term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                          AND term.IS_BID_GROUP = @Is_Bid_Group
                                          
                                          
                        INSERT      INTO #Bids
                                    ( 
                                     Category_Name_Locale_Value
                                    ,Category_Display_Seq
                                    ,Question_Label_Locale_Value
                                    ,Question_Display_Seq
                                    ,Response
                                    ,VENDOR_NAME
                                    ,Vendor_Contact
                                    ,Term
                                    ,PRODUCT_NAME
                                    ,FROM_MONTH
                                    ,TO_MONTH )
                                    SELECT
                                          'Delivery and Fuel' AS Category_Name_Locale_Value
                                         ,1 AS Category_Display_Seq
                                         ,'What is the delivery pipeline?' AS Question_Label_Locale_Value
                                         ,1 AS Question_Display_Seq
                                         ,srdf.DELIVERY_PIPELINE_COMMENTS AS Response
                                         ,vndr.VENDOR_NAME
                                         ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                         ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                               ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                          END AS Term
                                         ,srpp.PRODUCT_NAME
                                         ,term.FROM_MONTH
                                         ,term.TO_MONTH
                                    FROM
                                          dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                          INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                                ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                          INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                                ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                          INNER JOIN dbo.USER_INFO ui
                                                ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                          INNER JOIN dbo.VENDOR vndr
                                                ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                                ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                          INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                                ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                          INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                                ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                          INNER JOIN dbo.SR_RFP_BID bid
                                                ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                          LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                                ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                          LEFT JOIN dbo.SR_RFP_DELIVERY_FUEL srdf
                                                ON srss.SR_RFP_DELIVERY_FUEL_ID = srdf.SR_RFP_DELIVERY_FUEL_ID
                                    WHERE
                                          term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                          AND term.IS_BID_GROUP = @Is_Bid_Group
                  
                        INSERT      INTO #Bids
                                    ( 
                                     Category_Name_Locale_Value
                                    ,Category_Display_Seq
                                    ,Question_Label_Locale_Value
                                    ,Question_Display_Seq
                                    ,Response
                                    ,VENDOR_NAME
                                    ,Vendor_Contact
                                    ,Term
                                    ,PRODUCT_NAME
                                    ,FROM_MONTH
                                    ,TO_MONTH )
                                    SELECT
                                          'Delivery and Fuel' AS Category_Name_Locale_Value
                                         ,1 AS Category_Display_Seq
                                         ,'Is fuel included in bid?' AS Question_Label_Locale_Value
                                         ,2 AS Question_Display_Seq
                                         ,ent.ENTITY_NAME + isnull(( '    %: ' + srdf.FUEL_IN_BID_VALUE ), '') AS Response
                                         ,vndr.VENDOR_NAME
                                         ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                         ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                               ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                          END AS Term
                                         ,srpp.PRODUCT_NAME
                                         ,term.FROM_MONTH
                                         ,term.TO_MONTH
                                    FROM
                                          dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                          INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                                ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                          INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                                ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                          INNER JOIN dbo.USER_INFO ui
                                                ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                          INNER JOIN dbo.VENDOR vndr
                                                ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                                ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                          INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                                ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                          INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                                ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                          INNER JOIN dbo.SR_RFP_BID bid
                                                ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                          LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                                ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                          LEFT JOIN dbo.SR_RFP_DELIVERY_FUEL srdf
                                                ON srss.SR_RFP_DELIVERY_FUEL_ID = srdf.SR_RFP_DELIVERY_FUEL_ID
                                          LEFT JOIN dbo.ENTITY ent
                                                ON srdf.FUEL_IN_BID_TYPE_ID = ent.ENTITY_ID
                                    WHERE
                                          term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                          AND term.IS_BID_GROUP = @Is_Bid_Group
                  
                        INSERT      INTO #Bids
                                    ( 
                                     Category_Name_Locale_Value
                                    ,Category_Display_Seq
                                    ,Question_Label_Locale_Value
                                    ,Question_Display_Seq
                                    ,Response
                                    ,VENDOR_NAME
                                    ,Vendor_Contact
                                    ,Term
                                    ,PRODUCT_NAME
                                    ,FROM_MONTH
                                    ,TO_MONTH )
                                    SELECT
                                          'Delivery and Fuel' AS Category_Name_Locale_Value
                                         ,1 AS Category_Display_Seq
                                         ,'Is transport included in bid?' AS Question_Label_Locale_Value
                                         ,3 AS Question_Display_Seq
                                         ,ent.ENTITY_NAME + isnull(( '    $: ' + srdf.TRANSPORT_IN_BID_VALUE ), '') AS Response
                                         ,vndr.VENDOR_NAME
                                         ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                         ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                               ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                          END AS Term
                                         ,srpp.PRODUCT_NAME
                                         ,term.FROM_MONTH
                                         ,term.TO_MONTH
                                    FROM
                                          dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                          INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                                ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                          INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                                ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                          INNER JOIN dbo.USER_INFO ui
                                                ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                          INNER JOIN dbo.VENDOR vndr
                                                ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                                ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                          INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                                ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                          INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                                ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                          INNER JOIN dbo.SR_RFP_BID bid
                                                ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                          LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                                ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                          LEFT JOIN dbo.SR_RFP_DELIVERY_FUEL srdf
                                                ON srss.SR_RFP_DELIVERY_FUEL_ID = srdf.SR_RFP_DELIVERY_FUEL_ID
                                          LEFT JOIN dbo.ENTITY ent
                                                ON srdf.TRANSPORT_IN_BID_TYPE_ID = ent.ENTITY_ID
                                    WHERE
                                          term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                          AND term.IS_BID_GROUP = @Is_Bid_Group
                                          
                                          
                        INSERT      INTO #Bids
                                    ( 
                                     Category_Name_Locale_Value
                                    ,Category_Display_Seq
                                    ,Question_Label_Locale_Value
                                    ,Question_Display_Seq
                                    ,Response
                                    ,VENDOR_NAME
                                    ,Vendor_Contact
                                    ,Term
                                    ,PRODUCT_NAME
                                    ,FROM_MONTH
                                    ,TO_MONTH )
                                    SELECT
                                          'Balancing' AS Category_Name_Locale_Value
                                         ,2 AS Category_Display_Seq
                                         ,'Will pricing vary outside of a stated volume tolerance?' AS Question_Label_Locale_Value
                                         ,1 AS Question_Display_Seq
                                         ,case srbt.IS_PRICE_VARYING
                                            WHEN 1 THEN 'Yes'
                                            WHEN 0 THEN 'No' + isnull(( '    %: ' + srbt.VOLUME_TOLERANCE ), '')
                                          END AS Response
                                         ,vndr.VENDOR_NAME
                                         ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                         ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                               ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                          END AS Term
                                         ,srpp.PRODUCT_NAME
                                         ,term.FROM_MONTH
                                         ,term.TO_MONTH
                                    FROM
                                          dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                          INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                                ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                          INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                                ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                          INNER JOIN dbo.USER_INFO ui
                                                ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                          INNER JOIN dbo.VENDOR vndr
                                                ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                                ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                          INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                                ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                          INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                                ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                          INNER JOIN dbo.SR_RFP_BID bid
                                                ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                          LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                                ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                          LEFT JOIN SR_RFP_BALANCING_TOLERANCE srbt
                                                ON srss.SR_RFP_BALANCING_TOLERANCE_ID = srbt.SR_RFP_BALANCING_TOLERANCE_ID
                                    WHERE
                                          term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                          AND term.IS_BID_GROUP = @Is_Bid_Group
                  
                  
                  
                        INSERT      INTO #Bids
                                    ( 
                                     Category_Name_Locale_Value
                                    ,Category_Display_Seq
                                    ,Question_Label_Locale_Value
                                    ,Question_Display_Seq
                                    ,Response
                                    ,VENDOR_NAME
                                    ,Vendor_Contact
                                    ,Term
                                    ,PRODUCT_NAME
                                    ,FROM_MONTH
                                    ,TO_MONTH )
                                    SELECT
                                          'Balancing' AS Category_Name_Locale_Value
                                         ,2 AS Category_Display_Seq
                                         ,'Imbalance pricing above tolerance' AS Question_Label_Locale_Value
                                         ,2 AS Question_Display_Seq
                                         ,srbt.ABOVE_TOLERANCE_COMMENTS AS Response
                                         ,vndr.VENDOR_NAME
                                         ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                         ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                               ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                          END AS Term
                                         ,srpp.PRODUCT_NAME
                                         ,term.FROM_MONTH
                                         ,term.TO_MONTH
                                    FROM
                                          dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                          INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                                ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                          INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                                ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                          INNER JOIN dbo.USER_INFO ui
                                                ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                          INNER JOIN dbo.VENDOR vndr
                                                ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                                ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                          INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                                ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                          INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                                ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                          INNER JOIN dbo.SR_RFP_BID bid
                                                ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                          LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                                ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                          LEFT JOIN SR_RFP_BALANCING_TOLERANCE srbt
                                                ON srss.SR_RFP_BALANCING_TOLERANCE_ID = srbt.SR_RFP_BALANCING_TOLERANCE_ID
                                    WHERE
                                          term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                          AND term.IS_BID_GROUP = @Is_Bid_Group
                  
                        INSERT      INTO #Bids
                                    ( 
                                     Category_Name_Locale_Value
                                    ,Category_Display_Seq
                                    ,Question_Label_Locale_Value
                                    ,Question_Display_Seq
                                    ,Response
                                    ,VENDOR_NAME
                                    ,Vendor_Contact
                                    ,Term
                                    ,PRODUCT_NAME
                                    ,FROM_MONTH
                                    ,TO_MONTH )
                                    SELECT
                                          'Balancing' AS Category_Name_Locale_Value
                                         ,2 AS Category_Display_Seq
                                         ,'Imbalance pricing below tolerance' AS Question_Label_Locale_Value
                                         ,3 AS Question_Display_Seq
                                         ,srbt.BELOW_TOLERANCE_COMMENTS AS Response
                                         ,vndr.VENDOR_NAME
                                         ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                         ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                               ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                          END AS Term
                                         ,srpp.PRODUCT_NAME
                                         ,term.FROM_MONTH
                                         ,term.TO_MONTH
                                    FROM
                                          dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                          INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                                ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                          INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                                ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                          INNER JOIN dbo.USER_INFO ui
                                                ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                          INNER JOIN dbo.VENDOR vndr
                                                ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                                ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                          INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                                ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                          INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                                ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                          INNER JOIN dbo.SR_RFP_BID bid
                                                ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                          LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                                ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                          LEFT JOIN SR_RFP_BALANCING_TOLERANCE srbt
                                                ON srss.SR_RFP_BALANCING_TOLERANCE_ID = srbt.SR_RFP_BALANCING_TOLERANCE_ID
                                    WHERE
                                          term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                          AND term.IS_BID_GROUP = @Is_Bid_Group
                  
                        INSERT      INTO #Bids
                                    ( 
                                     Category_Name_Locale_Value
                                    ,Category_Display_Seq
                                    ,Question_Label_Locale_Value
                                    ,Question_Display_Seq
                                    ,Response
                                    ,VENDOR_NAME
                                    ,Vendor_Contact
                                    ,Term
                                    ,PRODUCT_NAME
                                    ,FROM_MONTH
                                    ,TO_MONTH )
                                    SELECT
                                          'Balancing' AS Category_Name_Locale_Value
                                         ,2 AS Category_Display_Seq
                                         ,'Balancing frequency' AS Question_Label_Locale_Value
                                         ,4 AS Question_Display_Seq
                                         ,ent.ENTITY_NAME + isnull(( '    Comment: ' + cast(srbt.TOLERANCE_COMMENTS AS VARCHAR(50)) ), '') AS Response
                                         ,vndr.VENDOR_NAME
                                         ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                         ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                               ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                          END AS Term
                                         ,srpp.PRODUCT_NAME
                                         ,term.FROM_MONTH
                                         ,term.TO_MONTH
                                    FROM
                                          dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                          INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                                ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                          INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                                ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                          INNER JOIN dbo.USER_INFO ui
                                                ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                          INNER JOIN dbo.VENDOR vndr
                                                ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                                ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                          INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                                ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                          INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                                ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                          INNER JOIN dbo.SR_RFP_BID bid
                                                ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                          LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                                ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                          LEFT JOIN dbo.SR_RFP_BALANCING_TOLERANCE srbt
                                                ON srss.SR_RFP_BALANCING_TOLERANCE_ID = srbt.SR_RFP_BALANCING_TOLERANCE_ID
                                          LEFT JOIN dbo.ENTITY ent
                                                ON srbt.BALANCING_FREQUENCY_TYPE_ID = ent.ENTITY_ID
                                    WHERE
                                          term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                          AND term.IS_BID_GROUP = @Is_Bid_Group
                  
                        INSERT      INTO #Bids
                                    ( 
                                     Category_Name_Locale_Value
                                    ,Category_Display_Seq
                                    ,Question_Label_Locale_Value
                                    ,Question_Display_Seq
                                    ,Response
                                    ,VENDOR_NAME
                                    ,Vendor_Contact
                                    ,Term
                                    ,PRODUCT_NAME
                                    ,FROM_MONTH
                                    ,TO_MONTH )
                                    SELECT
                                          'Risk Management' AS Category_Name_Locale_Value
                                         ,3 AS Category_Display_Seq
                                         ,'Swap index to NYMEX?' AS Question_Label_Locale_Value
                                         ,1 AS Question_Display_Seq
                                         ,ent.ENTITY_NAME + isnull(( '    Comment: ' + srrm.SWAP_INDEX_NYMEX_COMMENTS ), '') AS Response
                                         ,vndr.VENDOR_NAME
                                         ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                         ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                               ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                          END AS Term
                                         ,srpp.PRODUCT_NAME
                                         ,term.FROM_MONTH
                                         ,term.TO_MONTH
                                    FROM
                                          dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                          INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                                ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                          INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                                ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                          INNER JOIN dbo.USER_INFO ui
                                                ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                          INNER JOIN dbo.VENDOR vndr
                                                ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                                ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                          INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                                ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                          INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                                ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                          INNER JOIN dbo.SR_RFP_BID bid
                                                ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                          LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                                ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                          LEFT JOIN dbo.SR_RFP_RISK_MANAGEMENT srrm
                                                ON srss.SR_RFP_RISK_MANAGEMENT_ID = srrm.SR_RFP_RISK_MANAGEMENT_ID
                                          LEFT JOIN dbo.ENTITY ent
                                                ON srrm.SWAP_INDEX_NYMEX_TYPE_ID = ent.ENTITY_ID
                                    WHERE
                                          term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                          AND term.IS_BID_GROUP = @Is_Bid_Group
                  
                        INSERT      INTO #Bids
                                    ( 
                                     Category_Name_Locale_Value
                                    ,Category_Display_Seq
                                    ,Question_Label_Locale_Value
                                    ,Question_Display_Seq
                                    ,Response
                                    ,VENDOR_NAME
                                    ,Vendor_Contact
                                    ,Term
                                    ,PRODUCT_NAME
                                    ,FROM_MONTH
                                    ,TO_MONTH )
                                    SELECT
                                          'Risk Management' AS Category_Name_Locale_Value
                                         ,3 AS Category_Display_Seq
                                         ,'Swap contract price to fixed price?' AS Question_Label_Locale_Value
                                         ,2 AS Question_Display_Seq
                                         ,ent.ENTITY_NAME + isnull(( '    Comment: ' + srrm.SWAP_CONTRACT_FIXED_COMMENTS ), '') AS Response
                                         ,vndr.VENDOR_NAME
                                         ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                         ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                               ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                          END AS Term
                                         ,srpp.PRODUCT_NAME
                                         ,term.FROM_MONTH
                                         ,term.TO_MONTH
                                    FROM
                                          dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                          INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                                ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                          INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                                ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                          INNER JOIN dbo.USER_INFO ui
                                                ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                          INNER JOIN dbo.VENDOR vndr
                                                ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                                ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                          INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                                ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                          INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                                ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                          INNER JOIN dbo.SR_RFP_BID bid
                                                ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                          LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                                ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                          LEFT JOIN dbo.SR_RFP_RISK_MANAGEMENT srrm
                                                ON srss.SR_RFP_RISK_MANAGEMENT_ID = srrm.SR_RFP_RISK_MANAGEMENT_ID
                                          LEFT JOIN dbo.ENTITY ent
                                                ON srrm.SWAP_CONTRACT_FIXED_TYPE_ID = ent.ENTITY_ID
                                    WHERE
                                          term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                          AND term.IS_BID_GROUP = @Is_Bid_Group
                  
                        INSERT      INTO #Bids
                                    ( 
                                     Category_Name_Locale_Value
                                    ,Category_Display_Seq
                                    ,Question_Label_Locale_Value
                                    ,Question_Display_Seq
                                    ,Response
                                    ,VENDOR_NAME
                                    ,Vendor_Contact
                                    ,Term
                                    ,PRODUCT_NAME
                                    ,FROM_MONTH
                                    ,TO_MONTH )
                                    SELECT
                                          'Risk Management' AS Category_Name_Locale_Value
                                         ,3 AS Category_Display_Seq
                                         ,'place fixed price triggers?' AS Question_Label_Locale_Value
                                         ,3 AS Question_Display_Seq
                                         ,ent.ENTITY_NAME + isnull(( '    Comment: ' + srrm.FIXED_PRICE_TRIGGER_COMMENTS ), '') AS Response
                                         ,vndr.VENDOR_NAME
                                         ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                         ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                               ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                          END AS Term
                                         ,srpp.PRODUCT_NAME
                                         ,term.FROM_MONTH
                                         ,term.TO_MONTH
                                    FROM
                                          dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                          INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                                ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                          INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                                ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                          INNER JOIN dbo.USER_INFO ui
                                                ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                          INNER JOIN dbo.VENDOR vndr
                                                ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                                ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                          INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                                ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                          INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                                ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                          INNER JOIN dbo.SR_RFP_BID bid
                                                ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                          LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                                ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                          LEFT JOIN dbo.SR_RFP_RISK_MANAGEMENT srrm
                                                ON srss.SR_RFP_RISK_MANAGEMENT_ID = srrm.SR_RFP_RISK_MANAGEMENT_ID
                                          LEFT JOIN dbo.ENTITY ent
                                                ON srrm.FIXED_PRICE_TRIGGER_TYPE_ID = ent.ENTITY_ID
                                    WHERE
                                          term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                          AND term.IS_BID_GROUP = @Is_Bid_Group
                  
                        INSERT      INTO #Bids
                                    ( 
                                     Category_Name_Locale_Value
                                    ,Category_Display_Seq
                                    ,Question_Label_Locale_Value
                                    ,Question_Display_Seq
                                    ,Response
                                    ,VENDOR_NAME
                                    ,Vendor_Contact
                                    ,Term
                                    ,PRODUCT_NAME
                                    ,FROM_MONTH
                                    ,TO_MONTH )
                                    SELECT
                                          'Alternate Fuel' AS Category_Name_Locale_Value
                                         ,4 AS Category_Display_Seq
                                         ,'Will buyer be able to liquidate contract volumes to utilize alternate fuels?' AS Question_Label_Locale_Value
                                         ,1 AS Question_Display_Seq
                                         ,ent.ENTITY_NAME + isnull(( '    Comment: ' + sraf.COMMENTS ), '') AS Response
                                         ,vndr.VENDOR_NAME
                                         ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                         ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                               ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                          END AS Term
                                         ,srpp.PRODUCT_NAME
                                         ,term.FROM_MONTH
                                         ,term.TO_MONTH
                                    FROM
                                          dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                          INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                                ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                          INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                                ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                          INNER JOIN dbo.USER_INFO ui
                                                ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                          INNER JOIN dbo.VENDOR vndr
                                                ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                                ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                          INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                                ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                          INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                                ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                          INNER JOIN dbo.SR_RFP_BID bid
                                                ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                          LEFT JOIN dbo.SR_RFP_SUPPLIER_SERVICE srss
                                                ON bid.SR_RFP_SUPPLIER_SERVICE_ID = srss.SR_RFP_SUPPLIER_SERVICE_ID
                                          LEFT JOIN dbo.SR_RFP_ALTERNATE_FUEL sraf
                                                ON srss.SR_RFP_ALTERNATE_FUEL_ID = sraf.SR_RFP_ALTERNATE_FUEL_ID
                                          LEFT JOIN dbo.ENTITY ent
                                                ON sraf.BUYER_LIQUIDATE_TYPE_ID = ent.ENTITY_ID
                                    WHERE
                                          term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                          AND term.IS_BID_GROUP = @Is_Bid_Group   
                                          
                                          
                  
                  END

            
      INSERT      INTO #Bids
                  ( 
                   Category_Name_Locale_Value
                  ,Category_Display_Seq
                  ,Question_Label_Locale_Value
                  ,Question_Display_Seq
                  ,Response
                  ,VENDOR_NAME
                  ,Vendor_Contact
                  ,Term
                  ,PRODUCT_NAME
                  ,FROM_MONTH
                  ,TO_MONTH )
                  SELECT
                        'Supplier Price and Comments' AS Category_Name_Locale_Value
                       ,0 AS Category_Display_Seq
                       ,'Credit Approval' AS Question_Label_Locale_Value
                       ,1 AS Question_Display_Seq
                       ,case srspc.IS_CREDIT_APPROVAL
                          WHEN 1 THEN 'Yes'
                          WHEN 0 THEN 'No' + isnull(( '    Comment: ' + srspc.NO_CREDIT_COMMENTS ), '')
                        END AS Response
                       ,vndr.VENDOR_NAME
                       ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                       ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                             ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                        END AS Term
                       ,srpp.PRODUCT_NAME
                       ,term.FROM_MONTH
                       ,term.TO_MONTH
                  FROM
                        dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                        INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                              ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                        INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                              ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                        INNER JOIN dbo.USER_INFO ui
                              ON ci.USER_INFO_ID = ui.USER_INFO_ID
                        INNER JOIN dbo.VENDOR vndr
                              ON cvm.VENDOR_ID = vndr.VENDOR_ID
                        INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                              ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                        INNER JOIN dbo.SR_RFP rfp
                              ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                        INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                              ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                        INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                              ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                        INNER JOIN dbo.SR_RFP_BID bid
                              ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                        LEFT JOIN dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS srspc
                              ON bid.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID = srspc.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
                  WHERE
                        term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                        AND term.IS_BID_GROUP = @Is_Bid_Group
                              
      INSERT      INTO #Bids
                  ( 
                   Category_Name_Locale_Value
                  ,Category_Display_Seq
                  ,Question_Label_Locale_Value
                  ,Question_Display_Seq
                  ,Response
                  ,VENDOR_NAME
                  ,Vendor_Contact
                  ,Term
                  ,PRODUCT_NAME
                  ,FROM_MONTH
                  ,TO_MONTH )
                  SELECT
                        'Supplier Price and Comments' AS Category_Name_Locale_Value
                       ,0 AS Category_Display_Seq
                       ,'Is the broker fee included?' AS Question_Label_Locale_Value
                       ,2 AS Question_Display_Seq
                       ,ent.ENTITY_NAME AS Response
                       ,vndr.VENDOR_NAME
                       ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                       ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                             ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                        END AS Term
                       ,srpp.PRODUCT_NAME
                       ,term.FROM_MONTH
                       ,term.TO_MONTH
                  FROM
                        dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                        INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                              ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                        INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                              ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                        INNER JOIN dbo.USER_INFO ui
                              ON ci.USER_INFO_ID = ui.USER_INFO_ID
                        INNER JOIN dbo.VENDOR vndr
                              ON cvm.VENDOR_ID = vndr.VENDOR_ID
                        INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                              ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                        INNER JOIN dbo.SR_RFP rfp
                              ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                        INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                              ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                        INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                              ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                        INNER JOIN dbo.SR_RFP_BID bid
                              ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                        LEFT JOIN dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS srspc
                              ON bid.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID = srspc.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
                        LEFT JOIN dbo.ENTITY ent
                              ON srspc.Broker_Included_Type_Id = ent.ENTITY_ID
                  WHERE
                        term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                        AND term.IS_BID_GROUP = @Is_Bid_Group
                              
      INSERT      INTO #Bids
                  ( 
                   Category_Name_Locale_Value
                  ,Category_Display_Seq
                  ,Question_Label_Locale_Value
                  ,Question_Display_Seq
                  ,Response
                  ,VENDOR_NAME
                  ,Vendor_Contact
                  ,Term
                  ,PRODUCT_NAME
                  ,FROM_MONTH
                  ,TO_MONTH )
                  SELECT
                        'Supplier Price and Comments' AS Category_Name_Locale_Value
                       ,0 AS Category_Display_Seq
                       ,'Price' AS Question_Label_Locale_Value
                       ,3 AS Question_Display_Seq
                       ,srspc.PRICE_RESPONSE_COMMENTS AS Response
                       ,vndr.VENDOR_NAME
                       ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                       ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                             ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                        END AS Term
                       ,srpp.PRODUCT_NAME
                       ,term.FROM_MONTH
                       ,term.TO_MONTH
                  FROM
                        dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                        INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                              ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                        INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                              ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                        INNER JOIN dbo.USER_INFO ui
                              ON ci.USER_INFO_ID = ui.USER_INFO_ID
                        INNER JOIN dbo.VENDOR vndr
                              ON cvm.VENDOR_ID = vndr.VENDOR_ID
                        INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                              ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                        INNER JOIN dbo.SR_RFP rfp
                              ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                        INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                              ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                        INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                              ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                        INNER JOIN dbo.SR_RFP_BID bid
                              ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                        LEFT JOIN dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS srspc
                              ON bid.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID = srspc.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
                  WHERE
                        term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                        AND term.IS_BID_GROUP = @Is_Bid_Group
                        
      INSERT      INTO #Bids
                  ( 
                   Category_Name_Locale_Value
                  ,Category_Display_Seq
                  ,Question_Label_Locale_Value
                  ,Question_Display_Seq
                  ,Response
                  ,VENDOR_NAME
                  ,Vendor_Contact
                  ,Term
                  ,PRODUCT_NAME
                  ,FROM_MONTH
                  ,TO_MONTH )
                  SELECT
                        'Supplier Price and Comments' AS Category_Name_Locale_Value
                       ,0 AS Category_Display_Seq
                       ,'Price' AS Question_Label_Locale_Value
                       ,4 AS Question_Display_Seq
                       ,srspc.PRICE_RESPONSE_COMMENTS AS Response
                       ,vndr.VENDOR_NAME
                       ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                       ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                             ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                        END AS Term
                       ,srpp.PRODUCT_NAME
                       ,term.FROM_MONTH
                       ,term.TO_MONTH
                  FROM
                        dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                        INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                              ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                        INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                              ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                        INNER JOIN dbo.USER_INFO ui
                              ON ci.USER_INFO_ID = ui.USER_INFO_ID
                        INNER JOIN dbo.VENDOR vndr
                              ON cvm.VENDOR_ID = vndr.VENDOR_ID
                        INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                              ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                        INNER JOIN dbo.SR_RFP rfp
                              ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                        INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                              ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                        INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                              ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                        INNER JOIN dbo.SR_RFP_BID bid
                              ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                        INNER JOIN dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS_ARCHIVE srspc
                              ON bid.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID = srspc.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
                  WHERE
                        term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                        AND term.IS_BID_GROUP = @Is_Bid_Group
                        AND srspc.PRICE_RESPONSE_COMMENTS IS NOT NULL
                              
      INSERT      INTO #Bids
                  ( 
                   Category_Name_Locale_Value
                  ,Category_Display_Seq
                  ,Question_Label_Locale_Value
                  ,Question_Display_Seq
                  ,Response
                  ,VENDOR_NAME
                  ,Vendor_Contact
                  ,Term
                  ,PRODUCT_NAME
                  ,FROM_MONTH
                  ,TO_MONTH )
                  SELECT
                        'Supplier Price and Comments' AS Category_Name_Locale_Value
                       ,0 AS Category_Display_Seq
                       ,'Pricing Comments' AS Question_Label_Locale_Value
                       ,5 AS Question_Display_Seq
                       ,srspc.Pricing_Comments AS Response
                       ,vndr.VENDOR_NAME
                       ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                       ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                             ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                        END AS Term
                       ,srpp.PRODUCT_NAME
                       ,term.FROM_MONTH
                       ,term.TO_MONTH
                  FROM
                        dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                        INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                              ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                        INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                              ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                        INNER JOIN dbo.USER_INFO ui
                              ON ci.USER_INFO_ID = ui.USER_INFO_ID
                        INNER JOIN dbo.VENDOR vndr
                              ON cvm.VENDOR_ID = vndr.VENDOR_ID
                        INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                              ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                        INNER JOIN dbo.SR_RFP rfp
                              ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                        INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                              ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                        INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                              ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                        INNER JOIN dbo.SR_RFP_BID bid
                              ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                        LEFT JOIN dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS srspc
                              ON bid.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID = srspc.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
                  WHERE
                        term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                        AND term.IS_BID_GROUP = @Is_Bid_Group
      
 
  
      SELECT
            @Vendor_Header = replace(left(Vendor.Vendors, len(Vendor.Vendors) - 1), '&amp;', '&')
      FROM
            #Bids
            CROSS APPLY ( SELECT
                              '[' + vndr.VENDOR_NAME + '] ,'
                          FROM
                              #Bids vndr
                          GROUP BY
                              vndr.VENDOR_NAME
            FOR
                          XML PATH('') ) Vendor ( Vendors )

      
      SELECT
            @Sql_Str = 'SELECT PRODUCT_NAME,Category_Name_Locale_Value,Category_Display_Seq+2 AS Category_Display_Seq
						,Question_Label_Locale_Value,Question_Display_Seq,Term,' + @Vendor_Header + 'FROM (
						SELECT PRODUCT_NAME,Category_Name_Locale_Value,Category_Display_Seq
						,Question_Label_Locale_Value,Question_Display_Seq
						,Response ,VENDOR_NAME,Term,FROM_MONTH,TO_MONTH
						FROM #Bids) up
						PIVOT (max(Response) FOR VENDOR_NAME IN (' + @Vendor_Header + ')) AS pvt
						WHERE Question_Label_Locale_Value is not null
						ORDER BY PRODUCT_NAME,FROM_MONTH,TO_MONTH,Category_Display_Seq,Question_Display_Seq'
						
      EXECUTE (@Sql_Str)
      
      DROP TABLE #Bids

END;
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Legacy_Bids_Sel_By_Sr_Account_Group_Id] TO [CBMSApplication]
GO
