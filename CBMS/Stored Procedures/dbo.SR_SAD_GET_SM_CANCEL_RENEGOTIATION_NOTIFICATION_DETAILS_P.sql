
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.SR_SAD_GET_SM_CANCEL_RENEGOTIATION_NOTIFICATION_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@user_id		VARCHAR(10),  
	@from_week_identifier datetime,  
	@to_week_identifier datetime 

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
exec dbo.SR_SAD_GET_SM_CANCEL_RENEGOTIATION_NOTIFICATION_DETAILS_P 5837, '2005-10-31','2006-10-31'
                   
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

	AKR			Ashok Kumar Raju


MODIFICATIONS

	Initials	Date		Modification
-----------------------------------------------------------
	AKR			2012-10-29  Modified the script to use the Analyst Mapping Code
	                        Modified to Standards
	
******/ 

CREATE PROCEDURE dbo.SR_SAD_GET_SM_CANCEL_RENEGOTIATION_NOTIFICATION_DETAILS_P
      ( @user_id VARCHAR(10)
      ,@from_week_identifier DATETIME
      ,@to_week_identifier DATETIME )
AS 
BEGIN

      SET NOCOUNT ON ;
  
      DECLARE
            @EP_commodity_type_id INT
           ,@NG_commodity_type_id INT
           ,@Account_Service_Level_Cd INT
           ,@Default_Analyst INT
           ,@Custom_Analyst INT
           ,@Utility INT

      SELECT
            @EP_commodity_type_id = max(case WHEN com.Commodity_Name = 'Electric Power' THEN com.Commodity_Id
                                        END)
           ,@NG_commodity_type_id = max(case WHEN com.Commodity_Name = 'Natural Gas' THEN com.Commodity_Id
                                        END)
      FROM
            dbo.Commodity com
      WHERE
            com.Commodity_Name IN ( 'Electric Power', 'Natural Gas' )  
      
      
      SELECT
            @Utility = e.entity_id
      FROM
            dbo.ENTITY e
      WHERE
            e.entity_type = 111
            AND e.entity_name = 'Utility'     
                 
      SELECT
            @Account_Service_Level_Cd = e.ENTITY_ID
      FROM
            dbo.ENTITY e
      WHERE
            e.ENTITY_NAME = 'D'
            AND e.ENTITY_DESCRIPTION = 'Other'
            AND e.ENTITY_TYPE = 708


      SELECT
            @Default_Analyst = max(case WHEN c.Code_Value = 'Default' THEN c.Code_Id
                                   END)
           ,@Custom_Analyst = max(case WHEN c.Code_Value = 'Custom' THEN c.Code_Id
                                  END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Analyst Type' ;
      WITH  cte_Accounts_List
              AS ( SELECT
                        ven.VENDOR_NAME
                       ,ch.Client_Name
                       ,rtrim(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' Site_Name
                       ,utilacc.ACCOUNT_ID
                       ,utilacc.ACCOUNT_NUMBER
                       ,con.CONTRACT_ID
                       ,dateadd(D, -1 * con.notification_days, con.contract_end_date) conDt
                       ,con.ed_contract_number
                       ,con.contract_end_date
                       ,con.notification_days
                       ,coalesce(utilacc.Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) Analyst_Mapping_Cd
                       ,con.COMMODITY_TYPE_ID Commodity_Id
                       ,utilacc.VENDOR_ID Account_Vendor_Id
                       ,ch.Client_Id
                       ,ch.Site_Id
                   FROM
                        dbo.account utilacc
                        JOIN dbo.meter met
                              ON met.account_id = utilacc.account_id
                        JOIN dbo.supplier_account_meter_map meterMap
                              ON meterMap.meter_id = met.meter_id
                                 AND meterMap.is_history = 0
                        JOIN dbo.account suppacc
                              ON meterMap.account_id = suppacc.account_id
                        JOIN dbo.contract con
                              ON meterMap.contract_id = con.contract_id
                        JOIN dbo.VENDOR ven
                              ON utilacc.VENDOR_ID = ven.VENDOR_ID
                        JOIN core.client_Hier ch
                              ON ch.site_Id = utilacc.Site_Id
                        LEFT JOIN dbo.sr_rfp_account rfp_account
                              ON rfp_account.account_id = utilacc.account_id
                                 AND rfp_account.is_deleted = 0
                        LEFT JOIN dbo.sr_rfp_utility_switch switch
                              ON rfp_account.sr_rfp_account_id = switch.sr_account_group_id
                        LEFT JOIN dbo.sr_cancel_renegotiation_document cancel_renegotiation
                              ON cancel_renegotiation.contract_id = con.contract_id
                   WHERE
                        utilacc.account_type_id = @Utility
                        AND utilacc.SERVICE_LEVEL_TYPE_ID != @Account_Service_Level_Cd
                        AND cancel_renegotiation.sr_cancel_renegotiation_document_id IS NULL
                        AND switch.supplier_notice_image_id IS NULL
                        AND con.COMMODITY_TYPE_ID IN ( @EP_commodity_type_id, @NG_commodity_type_id )
                        AND utilacc.NOT_MANAGED = 0
                   GROUP BY
                        ven.VENDOR_NAME
                       ,ch.Client_Name
                       ,ch.city
                       ,ch.state_name
                       ,ch.site_name
                       ,utilacc.ACCOUNT_ID
                       ,utilacc.ACCOUNT_NUMBER
                       ,con.CONTRACT_ID
                       ,con.notification_days
                       ,con.contract_end_date
                       ,con.ed_contract_number
                       ,utilacc.Analyst_Mapping_Cd
                       ,ch.Site_Analyst_Mapping_Cd
                       ,ch.Client_Analyst_Mapping_Cd
                       ,con.COMMODITY_TYPE_ID
                       ,utilacc.VENDOR_ID
                       ,ch.Client_Id
                       ,ch.Site_Id),
            cte_Account_User
              AS ( SELECT
                        acc.vendor_name
                       ,acc.client_name
                       ,acc.site_name
                       ,acc.account_id
                       ,acc.account_number
                       ,acc.contract_id
                       ,acc.ConDt
                       ,acc.ed_contract_number
                       ,acc.contract_end_date
                       ,acc.notification_days
                       ,case WHEN acc.Analyst_Mapping_Cd = @Default_Analyst THEN vcam.Analyst_Id
                             WHEN acc.Analyst_Mapping_Cd = @Custom_Analyst THEN coalesce(aca.Analyst_User_Info_Id, sca.Analyst_User_Info_Id, cca.Analyst_User_Info_Id)
                        END AS Analyst_Id
                   FROM
                        cte_Accounts_List acc
                        JOIN dbo.VENDOR_COMMODITY_MAP vcm
                              ON acc.Account_Vendor_Id = vcm.VENDOR_ID
                                 AND acc.Commodity_Id = vcm.COMMODITY_TYPE_ID
                        INNER JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                              ON vcm.VENDOR_COMMODITY_MAP_ID = vcam.Vendor_Commodity_Map_Id
                        JOIN core.Client_Commodity ccc
                              ON ccc.Client_Id = acc.Client_Id
                                 AND ccc.Commodity_Id = acc.COMMODITY_ID
                        LEFT JOIN dbo.Account_Commodity_Analyst aca
                              ON aca.Account_Id = acc.Account_Id
                                 AND aca.Commodity_Id = acc.COMMODITY_ID
                        LEFT JOIN dbo.SITE_Commodity_Analyst sca
                              ON sca.Site_Id = acc.Site_Id
                                 AND sca.Commodity_Id = acc.COMMODITY_ID
                        LEFT JOIN dbo.Client_Commodity_Analyst cca
                              ON ccc.Client_Commodity_Id = cca.Client_Commodity_Id
                   WHERE
                        acc.conDt BETWEEN @from_week_identifier
                                  AND     @to_week_identifier)
            SELECT
                  ( cau.contract_end_date - cau.notification_days ) AS contract_end_date
                 ,cau.client_name
                 ,cau.site_name
                 ,cau.account_id
                 ,cau.account_number
                 ,cau.vendor_name
                 ,cau.ed_contract_number
                 ,cau.contract_id
            FROM
                  cte_Account_User cau
                  INNER JOIN dbo.SR_SA_SM_MAP sm
                        ON cau.Analyst_Id = sm.SOURCING_ANALYST_ID
            WHERE
                  sm.SOURCING_MANAGER_ID = @user_id
                  OR cau.Analyst_Id = @user_id
            GROUP BY
                  cau.contract_end_date
                 ,cau.notification_days
                 ,cau.client_name
                 ,cau.site_name
                 ,cau.account_id
                 ,cau.account_number
                 ,cau.vendor_name
                 ,cau.ed_contract_number
                 ,cau.contract_id
                   
  
END
;
GO

GRANT EXECUTE ON  [dbo].[SR_SAD_GET_SM_CANCEL_RENEGOTIATION_NOTIFICATION_DETAILS_P] TO [CBMSApplication]
GO
