SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_DEAL_TICKET_TRANSACTION_STATUS_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@dealTicketId  	int       	          	
	@operation     	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--select * from entity where entity_name like 'new'
--sp_help cbms_image


CREATE      PROCEDURE dbo.GET_DEAL_TICKET_TRANSACTION_STATUS_DETAILS_P

@userId varchar(10),
@sessionId varchar(20),
@dealTicketId int, 
@operation int 

AS
set nocount on
	IF @operation = 0
		BEGIN
			SELECT 
				e.ENTITY_NAME, 
				rmdtts.DEAL_TRANSACTION_DATE, 
				rmdtts.CBMS_IMAGE_ID 
			FROM
				RM_DEAL_TICKET_TRANSACTION_STATUS rmdtts,
				ENTITY e
			WHERE
				rmdtts.RM_DEAL_TICKET_ID = @dealTicketId AND 
				e.ENTITY_ID = rmdtts.DEAL_TRANSACTION_STATUS_TYPE_ID
			
			union 
			
			SELECT 
				'Send Confirm Request' , 
				img.DATE_IMAGED, 
				rmdtts.CBMS_IMAGE_ID 
			FROM
				RM_DEAL_TICKET_TRANSACTION_STATUS rmdtts,
				ENTITY e,
				cbms_image img
			WHERE
				rmdtts.RM_DEAL_TICKET_ID = @dealTicketId AND 
				img.CBMS_IMAGE_ID = rmdtts.CBMS_IMAGE_ID and
				e.ENTITY_ID = 607
			ORDER BY  
				DEAL_TRANSACTION_DATE DESC

		END
	ELSE IF @operation = 1
		BEGIN
			SELECT 
				e.ENTITY_NAME, 
				rmdtd.TRIGGER_TRANSACTION_DATE AS DEAL_TRANSACTION_DATE, 
				rmdtd.CBMS_IMAGE_ID 
			FROM
				RM_DEAL_TICKET_DETAILS rmdtd,
				ENTITY e
			WHERE
				rmdtd.RM_DEAL_TICKET_ID = @dealTicketId AND 
				e.ENTITY_ID = rmdtd.TRIGGER_STATUS_TYPE_ID

			union 
			
			SELECT 
				'Send Confirm Request' , 
				img.DATE_IMAGED, 
				rmdtts.CBMS_IMAGE_ID 
			FROM
				RM_DEAL_TICKET_TRANSACTION_STATUS rmdtts,
				ENTITY e,
				cbms_image img
			WHERE
				rmdtts.RM_DEAL_TICKET_ID = @dealTicketId AND 
				img.CBMS_IMAGE_ID = rmdtts.CBMS_IMAGE_ID and
				e.ENTITY_ID = 607
			ORDER BY  
				DEAL_TRANSACTION_DATE DESC


		END
GO
GRANT EXECUTE ON  [dbo].[GET_DEAL_TICKET_TRANSACTION_STATUS_DETAILS_P] TO [CBMSApplication]
GO
