SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    Trade.My_Deal_Ticket_Queue_Cnt
   
    
DESCRIPTION:   
   
  
    
INPUT PARAMETERS:    
	Name          DataType       Default        Description    
-----------------------------------------------------------------------------    
	@User_Info_Id		INT
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec Trade.My_Deal_Ticket_Queue_Cnt 49
	Exec Trade.My_Deal_Ticket_Queue_Cnt 112
	
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	RR          Raghu Reddy
    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	RR          2019-01-10	GRM Proejct.
     
    
******/

CREATE PROC [Trade].[My_Deal_Ticket_Queue_Cnt] ( @User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;
      
      SELECT
            COUNT(DISTINCT dt.Deal_Ticket_Id) AS My_Deal_Ticket_Queue_Cnt
      FROM
            Trade.Deal_Ticket dt
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                  ON dtch.Deal_Ticket_Id = dt.Deal_Ticket_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                  ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
            INNER JOIN Trade.Workflow_Status_Map wsm
                  ON chws.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
            INNER JOIN Trade.Workflow_Status ws
                  ON wsm.Workflow_Status_Id = ws.Workflow_Status_Id
            INNER JOIN dbo.USER_INFO cui
                  ON cui.QUEUE_ID = dt.QUEUE_ID
      WHERE
            chws.Is_Active = 1
            AND cui.USER_INFO_ID = @User_Info_Id
            AND ws.Workflow_Status_Name NOT	IN ( 'Closed', 'Order Executed', 'Expired', 'Canceled', 'Completed' )
            
      
END;


GO
GRANT EXECUTE ON  [Trade].[My_Deal_Ticket_Queue_Cnt] TO [CBMSApplication]
GO
