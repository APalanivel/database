
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsCuInvoiceChangeLog_GetForCuInvoice

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@cu_invoice_id 	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

    EXEC cbmsCuInvoiceChangeLog_GetForCuInvoice 
      @MyAccountId = 49
     ,@cu_invoice_id = 16566240	

    EXEC cbmsCuInvoiceChangeLog_GetForCuInvoice 
      @MyAccountId = 49
     ,@cu_invoice_id = 24115565
	
	SELECT TOP 10 * FROM CU_INVOICE_CHANGE_LOG ORDER BY CU_INVOICE_CHANGE_LOG_ID DESC
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SP			Sandeep Pigilam

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	  SP		2014-07-22	Removed NOLOCKS
							Data Operations Enhancement Phase III,Added new columns Bucket_Number, CHANGE_DATE,USERNAME in the Select list. 	        	
******/

CREATE  PROCEDURE [dbo].[cbmsCuInvoiceChangeLog_GetForCuInvoice]
      ( 
       @MyAccountId INT
      ,@cu_invoice_id INT )
AS 
BEGIN
      SET NOCOUNT ON

      SELECT
            l.cu_invoice_change_log_id
           ,l.cu_invoice_id
           ,ct.entity_name change_type
           ,ft.entity_name field_type
           ,l.field_name
           ,l.previous_value
           ,l.current_value
           ,l.Bucket_Number
           ,l.CHANGE_DATE
           ,ui.FIRST_NAME + space(1) + ui.LAST_NAME AS UserName
      FROM
            dbo.CU_Invoice_Change_Log l
            INNER JOIN dbo.Entity ct
                  ON ct.entity_id = l.change_type_id
            INNER JOIN dbo.Entity ft
                  ON ft.entity_id = l.field_type_id
            LEFT OUTER JOIN dbo.USER_INFO ui
                  ON l.Updated_User_Id = ui.USER_INFO_ID
      WHERE
            l.cu_invoice_id = @cu_invoice_id
      ORDER BY
            l.Change_Date ASC
           ,l.Bucket_Number ASC

END



;
GO

GRANT EXECUTE ON  [dbo].[cbmsCuInvoiceChangeLog_GetForCuInvoice] TO [CBMSApplication]
GO
