SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          
NAME:   etl.Verify_CT_Concurrency  

    
DESCRIPTION:  
	Checks the last processed version of a table to see if it is contained int eh change tracking table
	If the value with within the table the procedure returns the value 'Change' toehrwise it returns the value 'full'
	
	The change and full values are used to drive the ETL process       
	
      
INPUT PARAMETERS:          
Name			DataType	Default		Description          
------------------------------------------------------------          
@Last_Processed	BIGINT					-- Last processed Version. 
@CT_Table_FQN	SYSNAME					-- Fully qualified name of the Change tracking table
                
OUTPUT PARAMETERS:          
Name			DataType	Default		Description          
------------------------------------------------------------ 

         
USAGE EXAMPLES:          
------------------------------------------------------------ 

EXEC etl.Verify_CTConcurrency 0, 'dbo.CU_Invoice'
       
     
AUTHOR INITIALS:          
Initials	Name          
------------------------------------------------------------          
CMH			Chad Hattabaugh  
     
     
MODIFICATIONS           
Initials	Date	Modification          
------------------------------------------------------------          
CMH			12/29/2010	Created
*****/ 
CREATE PROCEDURE etl.Verify_CT_Concurrency
(	@Last_Processed			BIGINT
	,@CT_Table_FQN			SYSNAME) 
AS 
BEGIN
	SET NOCOUNT ON;
		
	DECLARE @CT_Min_Valid_Version BIGINT =  CHANGE_TRACKING_MIN_VALID_VERSION(object_id(@CT_Table_FQN))

	IF @CT_Min_Valid_Version IS NULL
		RAISERROR('Change_tracking is not enabled for table ''%s''', 16, 1, @CT_Table_FQN) 
	
	IF @Last_Processed >= @CT_Min_Valid_Version
		SELECT 
			'Change'							AS Execution_Type
			,CHANGE_TRACKING_CURRENT_VERSION()	AS Current_Version
	ELSE 
		SELECT 
			'Full'								AS Execution_Type
			,CHANGE_TRACKING_CURRENT_VERSION()	AS Current_Version
END	

GO
GRANT EXECUTE ON  [ETL].[Verify_CT_Concurrency] TO [CBMSApplication]
GRANT EXECUTE ON  [ETL].[Verify_CT_Concurrency] TO [ETL_Execute]
GO
