SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.Invoice_Participation_DMO_Supplier_Account_Ins

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@user_info_id  	int       	          	
	@account_id    	int       	          	
	@site_id       	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RR	        2017-02-03	Created Contract placeholder CP-8
							Copied from dbo.cbmsInvoiceParticipation_InsertSupplierAccount, only event type input changed
******/

CREATE PROCEDURE [dbo].[Invoice_Participation_DMO_Supplier_Account_Ins]
      ( 
       @user_info_id INT
      ,@account_id INT
      ,@site_id INT )
AS 
BEGIN

      SET nocount ON

      INSERT      INTO invoice_participation_queue
                  ( 
                   event_type
                  ,client_id
                  ,division_id
                  ,site_id
                  ,account_id
                  ,service_month
                  ,event_by_id
                  ,event_date )
                  SELECT
                        10
                       ,NULL client_id
                       ,NULL division_id
                       ,ip.site_id
                       ,ip.account_id
                       ,ip.service_month
                       ,93
                       ,GETDATE()
                  FROM
                        vwCbmsAccountSite vas
                        JOIN invoice_participation ip
                              ON ip.account_id = vas.account_id
                                 AND ip.site_id = vas.site_id
                  WHERE
                        vas.account_type_id = 37
                        AND vas.account_id = @account_id
                        AND vas.site_id = @site_id
                        AND ip.is_received = 0
                        AND ip.is_expected = 1


      EXEC dbo.cbmsInvoiceParticipationQueue_Save 
            @user_info_id
           ,21 -- see called sproc for definition
           ,NULL
           ,NULL
           ,@site_id
           ,@account_id
           ,NULL
           ,1

END;
;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Participation_DMO_Supplier_Account_Ins] TO [CBMSApplication]
GO
