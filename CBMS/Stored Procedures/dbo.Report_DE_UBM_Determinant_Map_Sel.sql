SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********       

NAME:
     dbo.Report_DE_UBM_Determinant_Map_Sel
       
DESCRIPTION:          
			
      
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
--------------------------------------------------------------------------  
          
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
--------------------------------------------------------------------------  
          
 USAGE EXAMPLES:                            
--------------------------------------------------------------------------  

 EXEC dbo.Report_DE_UBM_Determinant_Map_Sel
      
 AUTHOR INITIALS:        
       
 Initials              Name        
--------------------------------------------------------------------------  
 AKR                   Ashok Kumar Raju        

  
 MODIFICATIONS:      
          
 Initials              Date             Modification      
--------------------------------------------------------------------------  
 AKR                   2014-10-21      Created.
*********/
CREATE PROCEDURE [dbo].[Report_DE_UBM_Determinant_Map_Sel]
AS 
BEGIN
      SET NOCOUNT ON 
      
---Determinant Mappings

      SELECT
            u.UBM_NAME
           ,co.Commodity_Name AS COMMODITY_NAME
           ,ubdm.UBM_BUCKET_CODE
           ,bm.Bucket_Name AS CBMS_BUCKET_NAME
      FROM
            UBM_BUCKET_DETERMINANT_MAP ubdm
            JOIN Bucket_Master bm
                  ON bm.Bucket_Master_Id = ubdm.Bucket_Master_Id
                     AND bm.Commodity_Id = ubdm.COMMODITY_TYPE_ID
            JOIN UBM u
                  ON u.UBM_ID = ubdm.UBM_ID
            JOIN Commodity co
                  ON co.Commodity_Id = ubdm.COMMODITY_TYPE_ID
      ORDER BY
            UBM_NAME
           ,Commodity_Name
     
END

;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_UBM_Determinant_Map_Sel] TO [CBMS_SSRS_Reports]
GO
