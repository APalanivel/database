SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_DELETE_TERM_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
	@rfp_id int,
	@term_id int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
-- exec dbo.SR_RFP_DELETE_TERM_P 1 1

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_DELETE_TERM_P
	@rfp_id int,
	@term_id int
	AS 
	
set nocount on
	
	
	delete sr_rfp_account_term where sr_rfp_term_id = @term_id
 	delete sr_rfp_term where sr_rfp_term_id = @term_id and sr_rfp_id = @rfp_id
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_DELETE_TERM_P] TO [CBMSApplication]
GO
