SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********           
NAME:  dbo.Client_Commodity_Analyst_Merge          
         
DESCRIPTION: Used to Insert/update the data into dbo.Client_Commodity_Analyst table.    
        
INPUT PARAMETERS:            
      Name     DataType          Default     Description            
------------------------------------------------------------            
   @Client_Commodity_Id INT    
      @Analyst_User_Info_Id INT    
            
            
OUTPUT PARAMETERS:            
      Name              DataType          Default     Description            
------------------------------------------------------------            
            
USAGE EXAMPLES:       
    
BEGIN TRAN    
select * from Client_Commodity_Analyst WHERE Client_Commodity_Id=20000078    
EXEC DBO.Client_Commodity_Analyst_Merge 20000078,49    
select * from Client_Commodity_Analyst WHERE Client_Commodity_Id=20000078    
ROLLBACK TRAN    
    
BEGIN TRAN    
select * from Client_Commodity_Analyst WHERE Client_Commodity_Id=20000066    
EXEC DBO.Client_Commodity_Analyst_Merge 20000066,49    
select * from Client_Commodity_Analyst WHERE Client_Commodity_Id=20000066    
ROLLBACK TRAN    
    
select top 10 * from core.client_commodity order by 1 desc    
    
    
------------------------------------------------------------      
        
AUTHOR INITIALS:          
Initials Name          
------------------------------------------------------------          
BCH   Balaraju  Chalumuri    
        
        
Initials Date  Modification          
------------------------------------------------------------          
BCH  2012-09-17  Created new sp (for POCO Project)    
******/    
CREATE PROCEDURE dbo.Client_Commodity_Analyst_Merge
      ( 
       @Client_Commodity_Id INT
      ,@Analyst_User_Info_Id INT )
AS 
BEGIN    
      SET NOCOUNT ON ;    
          
      BEGIN TRY                
            BEGIN TRAN  
     
            MERGE dbo.Client_Commodity_analyst AS Tgt
                  USING 
                        ( SELECT
                              @Client_Commodity_Id AS Client_Commodity_Id
                             ,@Analyst_User_Info_Id AS Analyst_User_Info_Id ) AS Src
                  ON Tgt.Client_Commodity_Id = Src.Client_Commodity_Id
                  WHEN MATCHED AND src.Analyst_User_Info_Id IS NOT NULL
                        THEN     
                  UPDATE SET  
                              Tgt.Analyst_User_Info_Id = Src.Analyst_User_Info_Id
                  WHEN MATCHED AND src.Analyst_User_Info_Id IS NULL
                        THEN     
                  DELETE
                  WHEN NOT MATCHED AND src.Analyst_User_Info_Id IS NOT NULL
                        THEN     
                       INSERT
                              ( 
                               Client_Commodity_Id
                              ,Analyst_User_Info_Id )
                         VALUES
                              ( 
                               Src.Client_Commodity_Id
                              ,Src.Analyst_User_Info_Id ) ;    
        
            DELETE
                  sca
            FROM
                  core.Client_Commodity cc
                  INNER JOIN core.Client_Hier ch
                        ON ch.Client_Id = cc.Client_Id
                  INNER JOIN dbo.Site_Commodity_Analyst sca
                        ON ch.Site_Id = sca.Site_Id
                           AND sca.Commodity_Id = cc.Commodity_Id
            WHERE
                  cc.Client_Commodity_Id = @Client_Commodity_Id  
          
          
            DELETE
                  aca
            FROM
                  core.Client_Commodity cc
                  INNER JOIN core.Client_Hier ch
                        ON ch.Client_Id = cc.Client_Id
                  INNER JOIN dbo.ACCOUNT acc
                        ON ch.Site_Id = acc.SITE_ID
                  INNER JOIN dbo.Account_Commodity_Analyst aca
                        ON acc.ACCOUNT_ID = aca.Account_Id
                           AND aca.Commodity_Id = cc.Commodity_Id
            WHERE
                  cc.Client_Commodity_Id = @Client_Commodity_Id   
            
            COMMIT TRAN                
      END TRY                
      BEGIN CATCH                
            IF @@TRANCOUNT > 0 
                  ROLLBACK TRAN                
                
            EXEC dbo.usp_RethrowError                
                
      END CATCH                
      
        
                      
END    
;
GO
GRANT EXECUTE ON  [dbo].[Client_Commodity_Analyst_Merge] TO [CBMSApplication]
GO
