SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Budget_Account_Rate_Queue_Users_For_Routing

DESCRIPTION:


INPUT PARAMETERS:
	Name						DataType		Default	Description
----------------------------------------------------------------
	
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
-----------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Budget_Account_Rate_Queue_Users_For_Routing

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-06-01	SE2017-26 - Created
	
******/

CREATE PROCEDURE [dbo].[Budget_Account_Rate_Queue_Users_For_Routing]
AS 
BEGIN
      SET NOCOUNT ON;
      
      SELECT
            ui.USER_INFO_ID
           ,ui.FIRST_NAME + ' ' + ui.LAST_NAME USER_INFO_NAME
      FROM
            dbo.USER_INFO ui
            INNER JOIN dbo.USER_INFO_GROUP_INFO_MAP map
                  ON ui.USER_INFO_ID = map.USER_INFO_ID
            INNER JOIN dbo.GROUP_INFO gi
                  ON map.GROUP_INFO_ID = gi.GROUP_INFO_ID
      WHERE
            gi.GROUP_NAME IN ( 'operations', 'Distribution Budgets', 'Budget Reviewer' )
            AND ui.IS_HISTORY = 0
      GROUP BY
            ui.USER_INFO_ID
           ,ui.FIRST_NAME
           ,ui.LAST_NAME
      ORDER BY
            ui.FIRST_NAME
         

END;

;
GO
GRANT EXECUTE ON  [dbo].[Budget_Account_Rate_Queue_Users_For_Routing] TO [CBMSApplication]
GO
