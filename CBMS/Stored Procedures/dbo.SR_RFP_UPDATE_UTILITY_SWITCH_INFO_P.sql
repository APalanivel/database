SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_UPDATE_UTILITY_SWITCH_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@accountGroupId	int       	          	
	@returnToTarrifDate	datetime  	          	
	@returnToTarrifTypeId	int       	          	
	@isSwitchRate  	bit       	          	
	@switchSupplierDate	datetime  	          	
	@switchSupplierTypeId	int       	          	
	@isSwitchSupplier	bit       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--exec SR_RFP_UPDATE_UTILITY_SWITCH_INFO_P 916,'',null,0,'',null,0
--select * from SR_RFP_UTILITY_SWITCH 

CREATE       PROCEDURE DBO.SR_RFP_UPDATE_UTILITY_SWITCH_INFO_P

@accountGroupId int,
@returnToTarrifDate datetime ,
@returnToTarrifTypeId int ,
@isSwitchRate bit ,
@switchSupplierDate datetime,
@switchSupplierTypeId int ,
@isSwitchSupplier bit


AS
set nocount on
UPDATE	SR_RFP_UTILITY_SWITCH 


SET		RETURN_TO_TARIFF_DATE = @returnToTarrifDate, 
		RETURN_TO_TARIFF_TYPE_ID = @returnToTarrifTypeId,
		IS_SWITCH_RATE_ESTIMATED = @isSwitchRate,
		UTILITY_SWITCH_SUPPLIER_DATE = @switchSupplierDate,
		UTILITY_SWITCH_SUPPLIER_TYPE_ID = @switchSupplierTypeId,
		IS_SWITCH_SUPPLIER_ESTIMATED = @isSwitchSupplier

 
WHERE	SR_ACCOUNT_GROUP_ID = @accountGroupId


UPDATE SR_RFP_CHECKLIST set
	UTILITY_SWITCH_DEADLINE_DATE = @returnToTarrifDate,
	UTILITY_SWITCH_SUPPLIER_DATE = @switchSupplierDate
WHERE SR_RFP_ACCOUNT_ID =  @accountGroupId
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_UPDATE_UTILITY_SWITCH_INFO_P] TO [CBMSApplication]
GO
