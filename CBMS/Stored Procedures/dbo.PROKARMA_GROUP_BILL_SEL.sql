SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

  
  
/******    
NAME:    
   
 dbo.PROKARMA_GROUP_BILL_SEL  
   
 DESCRIPTION:     
   
 Stored procedure that returns data for Prokarma/Group Bills.  
 This stored procedure is called from the ProkarmaGroupBills  
 SSIS pakcage (within the DataExtract SSIS project).  
   
 INPUT PARAMETERS:    
 Name   DataType Default  Description    
------------------------------------------------------------      
  
   
 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
  USAGE EXAMPLES:    
------------------------------------------------------------    
  
 EXEC dbo.PROKARMA_GROUP_BILL_SEL  
  
AUTHOR INITIALS:    
 Initials Name    
------------------------------------------------------------    
 GP   Garrett Page    11/05/2009  
 PD	  Padmanava Debnath	11/02/2010	  
 MODIFICATIONS     
 Initials Date  Modification    
------------------------------------------------------------           
PD		11/02/2010	Adding a WHERE clause to filter out AT&T data
******/  
  
  
CREATE PROCEDURE [dbo].[PROKARMA_GROUP_BILL_SEL]  
  
AS  
BEGIN  
  
 SET NOCOUNT ON   
    
 select Client_Name [Client]  
   ,Site_Reference_Number [Site Ref No]  
   ,Site_Name [Site]  
   ,Address_line1 [Address 1]  
   ,Address_line2 [Address 2]  
   ,ADDR.CITY [City]  
   ,ST.STATE_NAME [State]  
   ,ADDR.ZIPCODE [Zip Code]  
   ,country_name [Country]  
   ,a.ACCOUNT_NUMBER [Account]  
   ,a.ACCOUNT_ID [Account id]  
   ,v1.vendor_name [Vendor]  
   ,[Account Type] = case when a.ACCOUNT_TYPE_ID = 38 then 'Utility' else 'Supplier' end  
   ,ag.GROUP_BILLING_NUMBER [Group Bill Number]  
   ,NotManaged = case when a.NOT_MANAGED = '1' then 'NotManaged' else 'Managed' end    
 from ACCOUNT a  
 join ACCOUNT_GROUP ag on ag.ACCOUNT_GROUP_ID = a.ACCOUNT_GROUP_ID  
 join vwCbmsAccountSite v on v.account_id = a.ACCOUNT_ID  
 join SITE s on s.SITE_ID = v.site_id  
 join ADDRESS addr on addr.ADDRESS_ID = s.PRIMARY_ADDRESS_ID  
 join STATE st on st.STATE_ID = addr.STATE_ID  
 join COUNTRY ctry on ctry.COUNTRY_ID = st.COUNTRY_ID  
 join VENDOR v1 on v1.vendor_id = a.VENDOR_ID  
 join CLIENT c on c.CLIENT_ID = s.Client_ID
 WHERE c.CLIENT_ID != 11231  
 ORDER BY Client_Name  
   
END  
GO
GRANT EXECUTE ON  [dbo].[PROKARMA_GROUP_BILL_SEL] TO [CBMSApplication]
GO
