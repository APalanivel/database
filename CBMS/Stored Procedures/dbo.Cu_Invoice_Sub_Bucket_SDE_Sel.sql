SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
                         
 NAME: dbo.Cu_Invoice_Sub_Bucket_SDE_Sel                 
                          
 DESCRIPTION:      
		  To select/deicde the Sub bucket SDE on a invoice

                          
 INPUT PARAMETERS:      
                         
 Name                               DataType          Default       Description      
---------------------------------------------------------------------------------------------------------------    
 @Cu_Invoice_Id				         INT        
                          
 OUTPUT PARAMETERS:      
                               
 Name                               DataType          Default       Description      
---------------------------------------------------------------------------------------------------------------    
                          
 USAGE EXAMPLES:                              
---------------------------------------------------------------------------------------------------------------      

-- Template Exeption ( Auto mapping logic not defined, multiple Sub buckets Definition exist)
EXEC dbo.Cu_Invoice_Sub_Bucket_SDE_Sel @Cu_Invoice_Id = 93412929
EXEC dbo.Cu_Invoice_Sub_Bucket_SDE_Sel @Cu_Invoice_Id = 93946233
                         
 AUTHOR INITIALS:    
       
 Initials                   Name      
---------------------------------------------------------------------------------------------------------------    
 HG							Harihara Suthan Ganesan

 MODIFICATIONS:    
                           
 Initials               Date            Modification    
---------------------------------------------------------------------------------------------------------------    
 HG						2020-05-31		D20-1972, Created to select the Sub bucket SDE.
******/

CREATE PROCEDURE [dbo].[Cu_Invoice_Sub_Bucket_SDE_Sel]
(@Cu_Invoice_Id INT)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE
		@Template_Sb_Cd INT
		, @Charge_Sb_Cd INT
		, @Determinant_Sb_Cd INT;

	CREATE TABLE #Missing_Sub_Bucket
	(
		Account_Id					 INT
		, State_Id					 INT
		, Bucket_Master_Id			 INT
		, Commodity_Id				 INT
		, Bucket_Type				 VARCHAR(25)
		, Ubm_Sub_Bucket_Code		 NVARCHAR(255)
		, Sub_Bucket_Definition_Cnt	 INT
			  DEFAULT(0)
		, Sub_Bucket_Exist_In_Market BIT
			  DEFAULT(0)
	);

	DECLARE @Exception TABLE
	(
		Account_Id			INT
		, Commodity			INT
		, Bucket_Type		VARCHAR(25)
		, Exception_Type_Cd INT
	);




	SELECT
		@Template_Sb_Cd = MAX(CASE WHEN c.Code_Value = 'Sub-Bkt Template Mapping' THEN c.Code_Id END)
		, @Charge_Sb_Cd = MAX(CASE WHEN c.Code_Value = 'Sub-Bkt Charge Mapping' THEN c.Code_Id END)
		, @Determinant_Sb_Cd = MAX(CASE WHEN c.Code_Value = 'Sub-Bkt Determnt Mapping' THEN c.Code_Id END)
	FROM
		dbo.Code c
		INNER JOIN dbo.Codeset cs
			ON cs.Codeset_Id = c.Codeset_Id
	WHERE
		cs.Codeset_Name = 'Exception Type'
		AND c.Code_Value IN ( 'Sub-Bkt Template Mapping', 'Sub-Bkt Charge Mapping', 'Sub-Bkt Determnt Mapping' );



	INSERT INTO #Missing_Sub_Bucket
		(
			Account_Id
			, State_Id
			, Bucket_Master_Id
			, Commodity_Id
			, Bucket_Type
			, Ubm_Sub_Bucket_Code
		)
	SELECT
		cha.Account_Id
		, cha.Meter_State_Id
		, cid.Bucket_Master_Id
		, cha.Commodity_Id
		, 'Charge'
		, cid.Ubm_Sub_Bucket_Code
	FROM
		dbo.CU_INVOICE_CHARGE cid
		INNER JOIN dbo.CU_INVOICE_CHARGE_ACCOUNT ca
			ON ca.CU_INVOICE_CHARGE_ID = cid.CU_INVOICE_CHARGE_ID
		INNER JOIN Core.Client_Hier_Account cha
			ON cha.Account_Id = ca.ACCOUNT_ID AND cha.Commodity_Id = cid.COMMODITY_TYPE_ID
	WHERE
		cid.CU_INVOICE_ID = @Cu_Invoice_Id
		AND cid.Bucket_Master_Id IS NOT NULL
		AND cid.EC_Invoice_Sub_Bucket_Master_Id IS NULL
	GROUP BY
		cha.Account_Id
		, cha.Meter_State_Id
		, cid.Bucket_Master_Id
		, cha.Commodity_Id
		, cid.Ubm_Sub_Bucket_Code;

	INSERT INTO #Missing_Sub_Bucket
		(
			Account_Id
			, State_Id
			, Bucket_Master_Id
			, Commodity_Id
			, Bucket_Type
			, Ubm_Sub_Bucket_Code
		)
	SELECT
		cha.Account_Id
		, cha.Meter_State_Id
		, cid.Bucket_Master_Id
		, cha.Commodity_Id
		, 'Determinant'
		, cid.Ubm_Sub_Bucket_Code
	FROM
		dbo.CU_INVOICE_DETERMINANT cid
		INNER JOIN dbo.CU_INVOICE_DETERMINANT_ACCOUNT da
			ON da.CU_INVOICE_DETERMINANT_ID = cid.CU_INVOICE_DETERMINANT_ID
		INNER JOIN Core.Client_Hier_Account cha
			ON cha.Account_Id = da.ACCOUNT_ID AND cha.Commodity_Id = cid.COMMODITY_TYPE_ID
	WHERE
		cid.CU_INVOICE_ID = @Cu_Invoice_Id
		AND cid.EC_Invoice_Sub_Bucket_Master_Id IS NULL
		AND cid.Bucket_Master_Id IS NOT NULL
	GROUP BY
		cha.Account_Id
		, cha.Meter_State_Id
		, cid.Bucket_Master_Id
		, cha.Commodity_Id
		, cid.Ubm_Sub_Bucket_Code;


	UPDATE
		msb
	SET
		msb.Sub_Bucket_Definition_Cnt = ISNULL(x.Sub_Bucket_Cnt, 0)
		, msb.Sub_Bucket_Exist_In_Market = CASE WHEN scim.Sub_Bucket_Cnt_In_Market = 0 THEN 0 ELSE 1 END
	FROM
		#Missing_Sub_Bucket msb
		LEFT OUTER JOIN(
						   SELECT
							   sb.Bucket_Master_Id
							   , sb.State_Id
							   , COUNT(1) Sub_Bucket_Cnt
						   FROM
							   dbo.EC_Invoice_Sub_Bucket_Master sb
						   GROUP BY
							   sb.Bucket_Master_Id
							   , sb.State_Id
					   ) x
			ON x.Bucket_Master_Id = msb.Bucket_Master_Id AND x.State_Id = msb.State_Id
		OUTER APPLY(
					   SELECT
						   COUNT(1)
					   FROM
						   dbo.EC_Invoice_Sub_Bucket_Master mksb
						   INNER JOIN dbo.Bucket_Master bm
							   ON bm.Bucket_Master_Id = mksb.Bucket_Master_Id
					   WHERE
						   mksb.State_Id = msb.State_Id AND msb.Commodity_Id = bm.Commodity_Id
				   ) scim(Sub_Bucket_Cnt_In_Market);


	INSERT INTO @Exception
		(
			Account_Id
			, Commodity
			, Bucket_Type
			, Exception_Type_Cd
		)
	SELECT
		msb.Account_Id
		, msb.Commodity_Id
		, msb.Bucket_Type
		, CASE WHEN msb.Bucket_Type = 'Charge' THEN
				   @Charge_Sb_Cd
			  WHEN msb.Bucket_Type = 'Determinant' THEN
				  @Determinant_Sb_Cd
			  ELSE
				  ''
		  END
	FROM
		#Missing_Sub_Bucket msb
	WHERE
		/*
			Missing Sub bucket SDE
				Sub bucket config doesn't exist for Usage-Total bucket but configuration exist for the State and Commodity.
				Multiple Sub bucket config defined, auto mapping logic is not defined and Ubm sub bucket exist 
		
		*/

		(
			msb.Sub_Bucket_Definition_Cnt = 0
			AND msb.Ubm_Sub_Bucket_Code IS NOT NULL
			AND msb.Sub_Bucket_Exist_In_Market = 1
		)
		OR ((
				msb.Sub_Bucket_Definition_Cnt > 1 AND msb.Ubm_Sub_Bucket_Code IS NOT NULL
			)
		   )
	GROUP BY
		msb.Account_Id
		, msb.Commodity_Id
		, msb.Bucket_Type;


	INSERT INTO @Exception
		(
			Account_Id
			, Commodity
			, Bucket_Type
			, Exception_Type_Cd
		)
	SELECT
		msb.Account_Id
		, msb.Commodity_Id
		, ''
		, @Template_Sb_Cd
	FROM
		#Missing_Sub_Bucket msb
	WHERE
		/*

			Template Exception
			  Multiple Sub bucket config defined, auto mapping logic is not defined and Ubm sub bucket doesn't exist	
		
		*/
		msb.Sub_Bucket_Definition_Cnt > 1 AND msb.Ubm_Sub_Bucket_Code IS NULL
	GROUP BY
		msb.Account_Id
		, msb.Commodity_Id;

	SELECT
		e.Account_Id
		, e.Commodity
		, e.Exception_Type_Cd
		, c.Code_Dsc Exception_Type
	FROM
		@Exception e
		INNER JOIN dbo.Code c
			ON c.Code_Id = e.Exception_Type_Cd
	ORDER BY
		e.Bucket_Type
		, Exception_Type;

	DROP TABLE #Missing_Sub_Bucket;

END;

GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Sub_Bucket_SDE_Sel] TO [CBMSApplication]
GO
