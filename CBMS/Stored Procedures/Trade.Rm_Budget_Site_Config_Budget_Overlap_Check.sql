SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.Rm_Budget_Site_Config_Budget_Overlap_Check                        
                          
Description:                          
        To get market price and forecast pirce if a selected index   
                          
Input Parameters:                          
    Name    DataType        Default     Description                            
--------------------------------------------------------------------------------    
	@Index_Id   INT    
    @Start_Dt	Date    
	@End_Dt		Date
                          
 Output Parameters:                                
	Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
Usage Examples:                              
--------------------------------------------------------------------------------    
	SELECT * FROM Trade.Rm_Budget_Participant rbp 
	WHERE rbp.Client_Hier_Id IN (5690,268359,673435,764782,764783) ORDER BY rbp.Client_Hier_Id
	EXEC Trade.Rm_Budget_Site_Config_Budget_Overlap_Check @Client_Hier_Id ='5690,268359,673435,764782,764783'
	EXEC Trade.Rm_Budget_Site_Config_Budget_Overlap_Check @Client_Hier_Id ='5690,268359,673435'
	EXEC Trade.Rm_Budget_Site_Config_Budget_Overlap_Check @Client_Hier_Id ='764782,764783'

	EXEC Trade.Rm_Budget_Site_Config_Budget_Overlap_Check  @Client_Id = 1005,@Start_Index=1,@End_Index = 25
    
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR			2019-12-23	RM-Budgets Enahancement - Created
	                
******/
CREATE PROCEDURE [Trade].[Rm_Budget_Site_Config_Budget_Overlap_Check]
    (
        @Client_Hier_Id VARCHAR(MAX)
        , @Start_Dt DATE
        , @End_Dt DATE
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Is_Site_Config_Budget_Overlap BIT = 0;

        SELECT
            @Is_Site_Config_Budget_Overlap = 1
        FROM
            Trade.Rm_Budget_Site_Default_Budget_Config rbsdbc
        WHERE
            EXISTS (   SELECT
                            1
                       FROM
                            dbo.ufn_split(@Client_Hier_Id, ',') us
                       WHERE
                            CAST(us.Segments AS INT) = rbsdbc.Client_Hier_Id)
            AND (   rbsdbc.Start_Dt BETWEEN @Start_Dt
                                    AND     @End_Dt
                    OR  rbsdbc.End_Dt BETWEEN @Start_Dt
                                      AND     @End_Dt
                    OR  @Start_Dt BETWEEN rbsdbc.Start_Dt
                                  AND     rbsdbc.End_Dt
                    OR  @End_Dt BETWEEN rbsdbc.Start_Dt
                                AND     rbsdbc.End_Dt);

        SELECT  @Is_Site_Config_Budget_Overlap AS Is_Site_Config_Budget_Overlap;

    END;

GO
GRANT EXECUTE ON  [Trade].[Rm_Budget_Site_Config_Budget_Overlap_Check] TO [CBMSApplication]
GO
