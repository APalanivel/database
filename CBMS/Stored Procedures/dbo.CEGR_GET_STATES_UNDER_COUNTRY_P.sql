SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE dbo.CEGR_GET_STATES_UNDER_COUNTRY_P 
@userId varchar,
@sessionId varchar,
@countryId int

as
set nocount on
select	STATE_ID,
	STATE_NAME

from	STATE

where	COUNTRY_ID=@countryId 

order by STATE_NAME
GO
GRANT EXECUTE ON  [dbo].[CEGR_GET_STATES_UNDER_COUNTRY_P] TO [CBMSApplication]
GO
