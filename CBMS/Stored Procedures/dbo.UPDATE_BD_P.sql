SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.UPDATE_BD_P
	@bdMasterId INT,
	@bdId INT
AS
BEGIN

	SET NOCOUNT ON

	UPDATE dbo.billing_determinant
	SET billing_determinant_master_id = @bdMasterId
	WHERE billing_determinant_id = @bdId

END
GO
GRANT EXECUTE ON  [dbo].[UPDATE_BD_P] TO [CBMSApplication]
GO
