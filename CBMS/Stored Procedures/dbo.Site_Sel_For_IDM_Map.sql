
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/***** 
 NAME: [dbo].[Site_Sel_For_IDM_Map]      
        
DESCRIPTION:        
        
      procedure is to return IDM enabled site details    
        
INPUT PARAMETERS:        
      NAME                                      DATATYPE    DEFAULT           DESCRIPTION        
------------------------------------------------------------------------        
  @Security_Role_Id								
                        
OUTPUT PARAMETERS:                  
      NAME              DATATYPE    DEFAULT           DESCRIPTION           
               
------------------------------------------------------------   
	                
USAGE EXAMPLES:                  
------------------------------------------------------------                
      exec [dbo].[Site_Sel_For_IDM_Map] 12068  
        
             
AUTHOR INITIALS:                  
      INITIALS    NAME                  
------------------------------------------------------------                  
        KVK			Vinay K 
		SS			Sani
MODIFICATIONS:        
      INITIALS  DATE   MODIFICATION                  
------------------------------------------------------------                  
		KVK		12/30/2014	created
		SS		2017/03/17	modified to have Time Zone in the select list
*******/  

CREATE PROCEDURE [dbo].[Site_Sel_For_IDM_Map]
      (
       @Security_Role_Id AS INT )
AS
BEGIN

      SELECT
            s.SITE_ID
           ,s.SITE_NAME
           ,isnull(cast(s.IDM_Node_XId AS VARCHAR(10)), '') AS IDM_Reference_Number
           ,ch.Sitegroup_Name AS Division_Name
           ,isnull(ch.Country_Name, '') AS Country_Name
           ,CASE WHEN ch.Site_Closed = 0 THEN 'Active'
                 ELSE 'Inactive'
            END AS Status
           ,ch.Client_Hier_Id
           ,isnull(ch.City, '') AS City
           ,ch.Site_Not_Managed AS Site_Status
           ,ch.Client_Name
           ,'Site' AS Entity_Type
		   ,ISNULL(ch.Weather_Station_Code,'') AS Weather_Station_Code
		   ,ISNULL(ch.Time_Zone,'') AS Time_Zone
      FROM
            dbo.Security_Role sr
            INNER JOIN Security_Role_Client_Hier srch
                  ON srch.Security_Role_Id = sr.Security_Role_Id
            INNER JOIN core.Client_Hier ch
                  ON ch.Client_Hier_Id = srch.Client_Hier_Id
            INNER JOIN dbo.SITE s
                  ON s.SITE_ID = ch.Site_Id
      WHERE
            sr.Security_Role_Id = @Security_Role_Id
            AND s.Is_IDM_Enabled = 1
      --GROUP BY
      --      s.SITE_ID
      --     ,s.SITE_NAME
      --     ,s.IDM_Node_XId
      --     ,ch.Sitegroup_Name
      --     ,ch.Country_Name
      --     ,CASE WHEN ch.Site_Closed = 0 THEN 'Active'
      --           ELSE 'Inactive'
      --      END
      --     ,ch.Client_Hier_Id
      --     ,ch.City
      --     ,ch.Site_Not_Managed
      --     ,ch.Client_Name
END

;


;
GO


GRANT EXECUTE ON  [dbo].[Site_Sel_For_IDM_Map] TO [CBMSApplication]
GO
