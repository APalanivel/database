
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_UPLOAD_GROUP_LP_APPROVAL_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
	@rfpId INT,
	@siteId INT,    
	@uploadedById INT,
	@cbmsImageId INT,   
	@approvedDate DATETIME
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[SR_RFP_UPLOAD_GROUP_LP_APPROVAL_P]
      @rfpId INT
     ,@siteId INT
     ,@uploadedById INT
     ,@cbmsImageId INT
     ,@approvedDate DATETIME
AS 
BEGIN

      SET NOCOUNT ON
	
      DECLARE
            @entityId INT
           ,@clientApprovalId INT
           ,@DateNow DATETIME
		
      SELECT
            @entityId = ( SELECT
                              ENTITY_ID
                          FROM
                              dbo.Entity (NOLOCK)
                          WHERE
                              Entity_Name = 'Group Load Profiles'
                              AND Entity_Type = 100 )
	
      UPDATE
            dbo.CBMS_IMAGE
      SET   
            CBMS_IMAGE_TYPE_ID = @entityId
      WHERE
            CBMS_IMAGE_ID = @cbmsImageId

      IF @siteId <> 0 
            BEGIN

                  SET @DateNow = getdate()

                  --IF EXISTS ( SELECT
                  --                  1
                  --            FROM
                  --                  dbo.SR_RFP_LP_CLIENT_APPROVAL
                  --            WHERE
                  --                  site_id = @siteId
                  --                  AND sr_rfp_id = @rfpId
                  --                  AND IS_GROUP_LP = 1 ) 
                  --      BEGIN
                  --            UPDATE
                  --                  dbo.SR_RFP_LP_CLIENT_APPROVAL
                  --            SET   
                  --                  cbms_image_id = @cbmsImageId
                  --            WHERE
                  --                  site_id = @siteId
                  --      END
                  --ELSE 
                        --BEGIN
                  INSERT      INTO dbo.SR_RFP_LP_CLIENT_APPROVAL
                              ( 
                               SR_ACCOUNT_GROUP_ID
                              ,IS_BID_GROUP
                              ,CBMS_IMAGE_ID
                              ,APPROVE_DATE
                              ,SR_RFP_ID
                              ,SITE_ID
                              ,IS_GROUP_LP
                              ,UPLOADED_BY
                              ,UPLOADED_DATE )
                  VALUES
                              ( 
                               0
                              ,0
                              ,@cbmsImageId
                              ,@approvedDate
                              ,@rfpId
                              ,@siteId
                              ,1
                              ,@uploadedById
                              ,@DateNow )

                  SELECT
                        @clientApprovalId = scope_identity() -- @@identity
                        --END
            END
END
RETURN

;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_UPLOAD_GROUP_LP_APPROVAL_P] TO [CBMSApplication]
GO
