SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  procedure [dbo].[cbmsCbmsImage_Delete]
	( @MyAccountId int
	, @cbms_image_id int
	)
AS
BEGIN
	set nocount on

	   delete cbms_image
	    where cbms_image_id = @cbms_image_id

	set nocount off

	exec cbmsCbmsImage_Get @MyAccountId, @cbms_image_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCbmsImage_Delete] TO [CBMSApplication]
GO
