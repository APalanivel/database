SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










CREATE     PROCEDURE dbo.BUDGET_UPDATE_REFORECAST_P
	@user_id varchar(10),
	@session_id varchar(20),
	@budgetAccountId int	
	

	AS
	begin
	set nocount on

	
	update 	budget_account
        set 	is_reforecast = 0
	where 	budget_account_id = @budgetAccountId
		
	end














GO
GRANT EXECUTE ON  [dbo].[BUDGET_UPDATE_REFORECAST_P] TO [CBMSApplication]
GO
