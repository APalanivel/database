SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE dbo.GET_UTILITY_BASIC_INFO_P
	@utilityId INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT DISTINCT vendOR.vendor_name,
		utilitydetail.utility_comments,
		state.state_id,
		state.country_id
	FROM dbo.vendOR vendOR INNER JOIN dbo.utility_detail utilitydetail ON utilitydetail.vendor_id=vendor.vendor_id
		INNER JOIN dbo.vendor_state_map vendorstatemap ON vendorstatemap.vendor_id=vendOR.vendor_id
		INNER JOIN dbo.state state ON state.state_id=vendorstatemap.state_id
	WHERE vendOR.vendor_id=@utilityId

END
GO
GRANT EXECUTE ON  [dbo].[GET_UTILITY_BASIC_INFO_P] TO [CBMSApplication]
GO
