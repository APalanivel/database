SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********   
NAME:  dbo.Master_Variance_Test_Rule_SAVE  
 
DESCRIPTION:  Used to save all the Master level rules data  

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
	@operator_Id			int	
	@Positive_Negative		varchar(3)
	@Variance_Test_Id		int
	@Baseline_Id			int    
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:   

	
------------------------------------------------------------  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
NK   Nageswara Rao Kosuri         


Initials Date  Modification  
------------------------------------------------------------  
NK	10/13/2009  Created


 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.Master_Variance_Test_Rule_SAVE
	@operator_Id int,	
	@Positive_Negative varchar(3),
	@Variance_Test_Id int,
	@Baseline_Id int
	AS

BEGIN
	
	SET NOCOUNT ON;
	
	INSERT INTO Variance_Rule(
		Operator_Cd,
		Positive_Negative,
		Variance_Test_Id,
		Variance_Baseline_Id
		)    
	values(@operator_Id
		,@Positive_Negative
		,@Variance_Test_Id
		,@Baseline_Id
		)	
END
GO
GRANT EXECUTE ON  [dbo].[Master_Variance_Test_Rule_SAVE] TO [CBMSApplication]
GO
