SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
             
NAME: Inv_Sourced_Duplicate_Image_Ins

DESCRIPTION:

 To insert the data in to Inv_Sourced_Duplicate_Image table

INPUT PARAMETERS:
NAME							DATATYPE		DEFAULT  DESCRIPTION
------------------------------------------------------------
@Image_File_Name				VARCHAR(200)	
@Image_Hash_Value				NVARCHAR(128)
@Duplicate_Image_Inv_Source_Id	INT
@Original_Inv_Sourced_Image_Id	INT
@Keyword						VARCHAR(1000)	NULL
@Inv_Sourced_Image_Batch_Id		INT

OUTPUT PARAMETERS:
NAME							DATATYPE	DEFAULT		DESCRIPTION
--------------------------------------------------------------------

USAGE EXAMPLES:
--------------------------------------------------------------------

BEGIN TRAN


	EXEC dbo.Inv_Sourced_Duplicate_Image_Ins
		  @Image_File_Name = '3013845.tif'
		 ,@Image_Hash_Value = 'N341UKL09OM12E7NGJ'
		 ,@Duplicate_Image_Inv_Source_Id = 9
		 ,@Original_Inv_Sourced_Image_Id = 2253189
		 ,@Keyword = NULL
		 ,@Inv_Sourced_Image_Batch_Id = 3000
		 
	SELECT TOP 1
		  *
	FROM
		  Inv_Sourced_Duplicate_Image
	ORDER BY
		  Inv_Sourced_Duplicate_Image_Id DESC

ROLLBACK TRAN

SELECT TOP 10 * FROM Inv_Sourced_Image ORDER BY Inv_Sourced_Image_Id DESC

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
HG			Harihara Suthan Ganesan

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
HG			2014-06-20	Created for Invoice scraping image hash enh
	KVK			10/30/2018		modified since Inv_Sourced_Duplicate_Image table Image_File_Name column datatype is changing from VARCHHAR to NVARCHAR
*/

CREATE PROCEDURE [dbo].[Inv_Sourced_Duplicate_Image_Ins]
(
    @Image_File_Name NVARCHAR(200),
    @Image_Hash_Value NVARCHAR(128),
    @Duplicate_Image_Inv_Source_Id INT,
    @Original_Inv_Sourced_Image_Id INT,
    @Keyword VARCHAR(1000) = NULL,
    @Inv_Sourced_Image_Batch_Id INT
)
AS
BEGIN

    SET NOCOUNT ON;

    INSERT INTO dbo.Inv_Sourced_Duplicate_Image
    (
        Image_File_Name,
        Image_Hash_Value,
        Duplicate_Image_Inv_Source_Id,
        Original_Inv_Sourced_Image_Id,
        Keyword,
        Processed_Ts,
        Inv_Sourced_Image_Batch_Id
    )
    VALUES
    (@Image_File_Name, @Image_Hash_Value, @Duplicate_Image_Inv_Source_Id, @Original_Inv_Sourced_Image_Id, @Keyword,
     GETDATE(), @Inv_Sourced_Image_Batch_Id);

    SELECT SCOPE_IDENTITY() AS Inv_Sourced_Duplicate_Image_Id;

END;
GO
GRANT EXECUTE ON  [dbo].[Inv_Sourced_Duplicate_Image_Ins] TO [CBMSApplication]
GO
