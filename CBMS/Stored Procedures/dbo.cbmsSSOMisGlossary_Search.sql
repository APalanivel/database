SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsSSOMisGlossary_Search

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@term          	varchar(200)	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE     PROCEDURE 
	[dbo].[cbmsSSOMisGlossary_Search]

	( @MyAccountId int
	, @term varchar(200) = null
	)
AS
BEGIN

	set @term = '%' + @term + '%'

	   select term
		, definition
	     from sso_mis_glossary
            where term like isNull(@term, '%%') 


END
GO
GRANT EXECUTE ON  [dbo].[cbmsSSOMisGlossary_Search] TO [CBMSApplication]
GO
