
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	dbo.SR_RFP_DELETE_SELECTED_PRODUCT_P

DESCRIPTION: 


INPUT PARAMETERS:    
    Name                DataType          Default     Description    
---------------------------------------------------------------------------------    
	@rfpId				int
	@priceProductId		int
	@isSystemProduct	int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
	
		BEGIN TRANSACTION  
			SELECT * FROM dbo.SR_RFP_TERM_PRODUCT_MAP a JOIN dbo.SR_RFP_SELECTED_PRODUCTS b 
				ON a.SR_RFP_SELECTED_PRODUCTS_ID = b.SR_RFP_SELECTED_PRODUCTS_ID
				WHERE b.SR_RFP_ID = 11579 AND b.PRICING_PRODUCT_ID = 1235278
			EXEC dbo.SR_RFP_DELETE_SELECTED_PRODUCT_P 11579,1235278,0
			SELECT * FROM dbo.SR_RFP_TERM_PRODUCT_MAP a JOIN dbo.SR_RFP_SELECTED_PRODUCTS b 
				ON a.SR_RFP_SELECTED_PRODUCTS_ID = b.SR_RFP_SELECTED_PRODUCTS_ID
				WHERE b.SR_RFP_ID = 11579 AND b.PRICING_PRODUCT_ID = 1235278
		ROLLBACK TRANSACTION

		BEGIN TRANSACTION  
			SELECT * FROM dbo.SR_PRICING_PRODUCT a left JOIN dbo.SR_Pricing_Product_Country_Map b 
				ON a.SR_PRICING_PRODUCT_ID = b.SR_PRICING_PRODUCT_ID WHERE a.SR_PRICING_PRODUCT_ID = 29
			EXEC dbo.Country_SR_Pricing_Product_Ins_Upd 29,'1,2,3,4,5',49
			SELECT * FROM dbo.SR_PRICING_PRODUCT a left JOIN dbo.SR_Pricing_Product_Country_Map b 
				ON a.SR_PRICING_PRODUCT_ID = b.SR_PRICING_PRODUCT_ID WHERE a.SR_PRICING_PRODUCT_ID = 29
			EXEC dbo.SR_RFP_DELETE_SELECTED_PRODUCT_P 0,29,1
			SELECT * FROM dbo.SR_PRICING_PRODUCT a left JOIN dbo.SR_Pricing_Product_Country_Map b 
				ON a.SR_PRICING_PRODUCT_ID = b.SR_PRICING_PRODUCT_ID WHERE a.SR_PRICING_PRODUCT_ID = 29
		ROLLBACK TRANSACTION

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DR			Deana Ritter
	RR			Raghu Reddy

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	DR			08/04/2009	Removed Linked Server Updates
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2015-09-22	Global Sourcing - Phase2 - Modified to delete records from new tabledbo.SR_Pricing_Product_Country_Map whenver deleting 
							SR_PRICING_PRODUCT from dbo.SR_PRICING_PRODUCT

******/
CREATE PROCEDURE [dbo].[SR_RFP_DELETE_SELECTED_PRODUCT_P]
      ( 
       @rfpId INT
      ,@priceProductId INT
      ,@isSystemProduct INT )
AS 
BEGIN

      SET NOCOUNT ON;

      IF @isSystemProduct = 0
            AND @rfpId > 0 
            BEGIN
                  DELETE FROM
                        SR_RFP_TERM_PRODUCT_MAP
                  WHERE
                        SR_RFP_SELECTED_PRODUCTS_ID IN ( SELECT
                                                            SR_RFP_SELECTED_PRODUCTS_ID
                                                         FROM
                                                            SR_RFP_SELECTED_PRODUCTS
                                                         WHERE
                                                            PRICING_PRODUCT_ID = @priceProductId
                                                            AND IS_SYSTEM_PRODUCT IS NULL
                                                            AND SR_RFP_ID = @rfpId );	


                  DELETE FROM
                        SR_RFP_SOP_SHORTLIST_DETAILS
                  WHERE
                        SR_RFP_SELECTED_PRODUCTS_ID IN ( SELECT
                                                            SR_RFP_SELECTED_PRODUCTS_ID
                                                         FROM
                                                            SR_RFP_SELECTED_PRODUCTS
                                                         WHERE
                                                            PRICING_PRODUCT_ID = @priceProductId
                                                            AND IS_SYSTEM_PRODUCT IS NULL
                                                            AND SR_RFP_ID = @rfpId );	
	
                  DELETE FROM
                        SR_RFP_SELECTED_PRODUCTS
                  WHERE
                        PRICING_PRODUCT_ID = @priceProductId
                        AND IS_SYSTEM_PRODUCT IS NULL
                        AND SR_RFP_ID = @rfpId;

            END;

      ELSE 
            IF @isSystemProduct = 1
                  AND @rfpId > 0 
                  BEGIN


                        DELETE FROM
                              SR_RFP_TERM_PRODUCT_MAP
                        WHERE
                              SR_RFP_SELECTED_PRODUCTS_ID IN ( SELECT
                                                                  SR_RFP_SELECTED_PRODUCTS_ID
                                                               FROM
                                                                  SR_RFP_SELECTED_PRODUCTS
                                                               WHERE
                                                                  PRICING_PRODUCT_ID = @priceProductId
                                                                  AND IS_SYSTEM_PRODUCT = @isSystemProduct
                                                                  AND SR_RFP_ID = @rfpId );

                        DELETE FROM
                              SR_RFP_SELECTED_PRODUCTS
                        WHERE
                              PRICING_PRODUCT_ID = @priceProductId
                              AND IS_SYSTEM_PRODUCT = @isSystemProduct
                              AND SR_RFP_ID = @rfpId;

                  END;

            ELSE 
                  IF @isSystemProduct = 1
                        AND @rfpId = 0 -- when delete for global products
                        BEGIN

                              PRINT 'DELETING GLOBAL PRODUCT LIST';
                              
                              DELETE FROM
                                    dbo.SR_Pricing_Product_Country_Map
                              WHERE
                                    SR_PRICING_PRODUCT_ID = @priceProductId;
		
                              DELETE FROM
                                    SR_PRICING_PRODUCT
                              WHERE
                                    SR_PRICING_PRODUCT_ID = @priceProductId;

                        END;

END;

GO

GRANT EXECUTE ON  [dbo].[SR_RFP_DELETE_SELECTED_PRODUCT_P] TO [CBMSApplication]
GO
