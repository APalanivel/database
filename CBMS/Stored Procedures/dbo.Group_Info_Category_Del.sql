SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME: dbo.Group_Info_Category_Del
  
DESCRIPTION: It will remove group_info_category for selected group_info_category_id 
			 when no groups are not depended on it.
  
INPUT PARAMETERS:
 Name						DataType			Default     Description
------------------------------------------------------------------
 @Group_Info_Category_Id	INT

OUTPUT PARAMETERS:
Name              DataType          Default     Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

SELECT * FROM GROUP_INFO_CATEGORY ORDER BY SOrt_Order

BEGIN TRAN

	DECLARE @Group_info_Category_Id INT
	
	SET  @Group_info_Category_Id  = 
				(SELECT TOP 1 
					 Group_Info_Category_Id
				FROM
					dbo.Group_Info_Category gic
				WHERE
					NOT EXISTS (SELECT 1 FROM dbo.Group_Info gi WHERE gi.Group_Info_Category_id = gic.Group_Info_Category_id) )

	SELECT @Group_info_Category_id 
	
	EXEC dbo.Group_Info_Category_Del @Group_Info_Category_id
	
	SELECT * FROM GROUP_INFO_CATEGORY ORDER BY SOrt_Order
		
ROLLBACK TRAN
  
AUTHOR INITIALS:
Initials Name
------------------------------------------------------------  
PNR		 Pandarinath
HG		 Harihara Suthan G

MODIFICATIONS

Initials	Date		  Modification
------------------------------------------------------------
 PNR		03/31/2011	  Created as part of Project : DV Groups Revamp
 HG			04/25/2011	  Added script to reset the sort order of category. 
******/  

CREATE PROCEDURE dbo.Group_Info_Category_Del
(
	@Group_Info_Category_Id	INT
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Sort_Order_Tbl TABLE(Sort_Order INT)
	DECLARE @Sort_Order INT

    DELETE
    FROM
		dbo.Group_Info_Category
	OUTPUT DELETED.Sort_Order INTO @Sort_Order_Tbl
    WHERE
		Group_Info_Category_Id = @Group_Info_Category_Id

	SELECT
		@Sort_Order = Sort_Order
	FROM
		@Sort_Order_Tbl

	UPDATE
		dbo.Group_Info_Category
	SET
		Sort_Order = Sort_Order -1
	WHERE
		Sort_Order >@Sort_Order

END
GO
GRANT EXECUTE ON  [dbo].[Group_Info_Category_Del] TO [CBMSApplication]
GO
