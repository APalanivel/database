SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME:
	[dbo].[User_Info_Sel_By_Id] 

DESCRIPTION:
	This procedure for selecting User Info Details
	   
INPUT PARAMETERS:
      Name              DataType          Default     Description
------------------------------------------------------------
	@User_Info_Id		INT 
	   
OUTPUT PARAMETERS:
      Name              DataType          Default     Description
--------------------------------------------------------------------

USAGE EXAMPLES:
----------------------------------------------------------------  
	EXEC User_Info_Sel_By_Id 10
      
AUTHOR INITIALS:
      Initials			Name	
------------------------------------------------------------
	   L			    Ajeesh		
	   RMG              Rani Mary George
	                  
MODIFICATIONS:
	 Initials    Date			 Modification
------------------------------------------------------------
	 L		     12/29/2011		 Created
	 RMG         3/15/2013       Copied from DVDeHub
	 	 NMK		28-09-2018    Modified to select access level
	 
******/

CREATE PROCEDURE [dbo].[User_Info_Sel_By_Id] (@User_Info_Id INT)
AS
BEGIN
    SET NOCOUNT ON;

    SELECT u.FIRST_NAME + ' ' + u.LAST_NAME AS UserName,
           u.EMAIL_ADDRESS,
           u.ACCESS_LEVEL,
           u.USERNAME AS name,
           u.IS_HISTORY
    FROM dbo.USER_INFO u
    WHERE u.USER_INFO_ID = @User_Info_Id;
END;

GO
GRANT EXECUTE ON  [dbo].[User_Info_Sel_By_Id] TO [CBMSApplication]
GO
