SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: DBO.Rm_Deal_Ticket_Transaction_Status_Del  

DESCRIPTION: 

	It Deletes Rm Deal Ticket Transaction Status for Selected Rm Deal Ticket Transaction Status Id.

INPUT PARAMETERS:
	NAME								DATATYPE	DEFAULT		DESCRIPTION
--------------------------------------------------------------------
	@Rm_Deal_Ticket_Transaction_Status_Id		INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  	BEGIN TRAN
		EXEC dbo.rm_deal_ticket_transaction_status_del  21191
	ROLLBACK TRAN
	
	SELECT
		Rm_Deal_Ticket_Transaction_Status_Id
	FROM
		dbo.rm_deal_ticket_transaction_status
	WHERE
		rm_deal_ticket_id = 106703

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			31-AUG-10	CREATED

*/

CREATE PROCEDURE dbo.Rm_Deal_Ticket_Transaction_Status_Del
    (
      @Rm_Deal_Ticket_Transaction_Status_Id		INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.Rm_Deal_Ticket_Transaction_Status
	WHERE
		Rm_Deal_Ticket_Transaction_Status_Id = @Rm_Deal_Ticket_Transaction_Status_Id
		
END
GO
GRANT EXECUTE ON  [dbo].[Rm_Deal_Ticket_Transaction_Status_Del] TO [CBMSApplication]
GO
