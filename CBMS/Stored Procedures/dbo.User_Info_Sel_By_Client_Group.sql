SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                 
/******                        
 NAME: dbo.User_Info_Sel_By_Client_Group            
                        
 DESCRIPTION:                        
			To get the users for invoice collection officers and coordinators dropdown                     
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@Client_Id						 INT			NULL
@Group_Names					 VARCHAR(MAX)  'Invoice Collection Group,Invoice Collection Admin'                     
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 
 EXEC [dbo].[User_Info_Sel_By_Client_Group] 
      @Client_Id = 74776
          
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2013-11-25       Created                
                       
******/                     
CREATE PROCEDURE [dbo].[User_Info_Sel_By_Client_Group]
      ( 
       @Client_Id INT = NULL
      ,@Group_Names VARCHAR(MAX) = 'Invoice Collection Group,Invoice Collection Admin' )
AS 
BEGIN                
      SET NOCOUNT ON;    
     
      SELECT
            u.USER_INFO_ID
           ,u.FIRST_NAME + ' ' + U.LAST_NAME AS UserName
           ,u.EMAIL_ADDRESS
      FROM
            dbo.USER_INFO u
      WHERE
            u.IS_HISTORY = 0
            AND u.ACCESS_LEVEL = 0
            AND EXISTS ( SELECT
                              1
                         FROM
                              dbo.USER_INFO_GROUP_INFO_MAP uig
                              INNER JOIN dbo.GROUP_INFO gi
                                    ON uig.GROUP_INFO_ID = gi.GROUP_INFO_ID
                              INNER JOIN dbo.ufn_split(@Group_Names, ',') s
                                    ON gi.GROUP_NAME = s.Segments
                         WHERE
                              uig.USER_INFO_ID = u.USER_INFO_ID )
      ORDER BY
            UserName  

END 



;
GO
GRANT EXECUTE ON  [dbo].[User_Info_Sel_By_Client_Group] TO [CBMSApplication]
GO
