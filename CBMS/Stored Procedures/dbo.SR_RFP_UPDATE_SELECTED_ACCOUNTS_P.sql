SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_UPDATE_SELECTED_ACCOUNTS_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@rfpAccountId           int,
@contractExpDate        datetime,
@currentContractId      int,
@contractNoticeDate     datetime,
@incumbantVendorId      int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_UPDATE_SELECTED_ACCOUNTS_P
@rfpAccountId int,
@contractExpDate datetime,
@currentContractId int,
@contractNoticeDate datetime,
@incumbantVendorId int

AS
	
SET NOCOUNT ON


UPDATE SR_RFP_CHECKLIST 
   SET INCUMBANT_ID = @incumbantVendorId
 WHERE  SR_RFP_ACCOUNT_ID = @rfpAccountId
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_UPDATE_SELECTED_ACCOUNTS_P] TO [CBMSApplication]
GO
