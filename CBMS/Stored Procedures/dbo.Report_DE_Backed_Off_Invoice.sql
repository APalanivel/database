
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
                         
 NAME: dbo.Report_DE_Backed_Off_Invoice

 DESCRIPTION:      
		  To get the backed off invocies
                          
 INPUT PARAMETERS:      
                         
 Name                               DataType          Default       Description      
---------------------------------------------------------------------------------------------------------------    
 @Client_Name						VARCHAR(200)
                 
 OUTPUT PARAMETERS:      
                               
 Name                               DataType          Default       Description      
---------------------------------------------------------------------------------------------------------------    
                          
 USAGE EXAMPLES:                              
--------------------------------------------------------------------------------------------------------------- 
 
	BEGIN TRAN
	SELECT * FROM REPTMGR.dbo.Client_Report_Config
    EXEC Report_DE_Backed_Off_Invoice
      @Client_Name = 'Philips'
	SELECT * FROM REPTMGR.dbo.Client_Report_Config 
	ROLLBACK TRAN      
    
	BEGIN TRAN
	SELECT * FROM REPTMGR.dbo.Client_Report_Config
	EXEC Report_DE_Backed_Off_Invoice  @Client_Name = 'Philips','2014-03-01','2014-09-01'
	SELECT * FROM REPTMGR.dbo.Client_Report_Config 
	ROLLBACK TRAN      
	  
	  
	  SELECT * FROM Client WHERE Client_id = 12344
                         
 AUTHOR INITIALS:    
       
 Initials                   Name      
---------------------------------------------------------------------------------------------------------------
 HG							Harihara Suthan

 MODIFICATIONS:

 Initials               Date            Modification
---------------------------------------------------------------------------------------------------------------
 HG                     2016-01-11      Created for JLL data feed requirement
 HG						2016-05-18		Since the Philips client is splitted to 3 different client Royal Philips|Philips Lighting|New Lumileds changed the input to considered the Pipe separated client name and getting all the clients data together
******/
CREATE PROCEDURE [dbo].[Report_DE_Backed_Off_Invoice]
      (
       @Client_Name VARCHAR(MAX) )
AS
BEGIN

      SET NOCOUNT ON;

      DECLARE
            @Last_Run_Ts DATETIME
           ,@Current_Ts DATETIME; -- Uses the maximum Cu_Invoice Updated time stamp available in Copy database

      DECLARE @Client_List TABLE ( Client_Id INT );
      SELECT
            @Current_Ts = MAX(UPDATED_DATE)
      FROM
            dbo.CU_INVOICE;

      INSERT      INTO @Client_List
                  ( Client_Id )
                  SELECT
                        cl.CLIENT_ID
                  FROM
                        dbo.ufn_split(@Client_Name, '|') cn
                        INNER JOIN dbo.CLIENT cl
                              ON cl.CLIENT_NAME = cn.Segments
                  GROUP BY
                        cl.CLIENT_ID;

      SELECT
            @Last_Run_Ts = crc.Last_Run_Ts
      FROM
            REPTMGR.dbo.Client_Report_Config crc
      WHERE
            crc.Report_Type = 'Backed Off Invoice'
            AND EXISTS ( SELECT
                              1
                         FROM
                              @Client_List cl
                         WHERE
                              cl.Client_Id = crc.Client_Id );

      SELECT
            CU_INVOICE_ID AS [Invoice ID]
      FROM
            dbo.CU_INVOICE cu
      WHERE
            EXISTS ( SELECT
                        1
                     FROM
                        @Client_List cl
                     WHERE
                        cl.Client_Id = cu.CLIENT_ID )
            AND cu.UPDATED_DATE BETWEEN @Last_Run_Ts
                                AND     @Current_Ts
            AND cu.IS_REPORTED = 0
            AND cu.IS_PROCESSED = 0;

      UPDATE
            cnf
      SET
            Last_Run_Ts = @Current_Ts
      FROM
            REPTMGR.dbo.Client_Report_Config cnf
      WHERE
            cnf.Report_Type = 'Backed Off Invoice'
            AND EXISTS ( SELECT
                              1
                         FROM
                              @Client_List cl
                         WHERE
                              cl.Client_Id = cnf.Client_Id );

END;
;

GO
GRANT EXECUTE ON  [dbo].[Report_DE_Backed_Off_Invoice] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_Backed_Off_Invoice] TO [CBMSApplication]
GO
