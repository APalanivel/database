SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/*********     
NAME:  dbo.Variance_Processing_SEL_BY_Engine_Id    
   
DESCRIPTION:  Used to select Variance Processing Engine ID from code table using the code_id    
  
INPUT PARAMETERS:      
      Name              DataType          Default     Description      
------------------------------------------------------------      
@Code_id   INT    
          
      
OUTPUT PARAMETERS:      
      Name              DataType          Default     Description      
------------------------------------------------------------      
      
USAGE EXAMPLES:     
  
 EXEC Variance_Processing_SEL_BY_Engine_Id '103224'  
 EXEC Variance_Processing_SEL_BY_Engine_Id '103225'  
 
------------------------------------------------------------    
AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
SCH   Satish Chadarajupalli          
 
  
Initials Date  Modification    
------------------------------------------------------------    
SCH 01/03/2020  Creating The SP  

  
  
******/

CREATE PROCEDURE [dbo].[Variance_Processing_SEL_BY_Engine_Id]  
 @Code_id INT  
AS    
BEGIN  
   
 SET NOCOUNT ON;  
   
 SELECT  
  vpe.Variance_Processing_Engine_Id    
 FROM  
  dbo.CODE cd  
  INNER JOIN dbo.Codeset cs   
   ON cs.Codeset_Id = cd.CodeSet_Id 
 INNER JOIN dbo.Variance_Processing_Engine vpe
   ON cd.Code_Id = vpe.Variance_Engine_Cd 
   AND cd.Code_Id = @Code_id    
  
END  

GO
GRANT EXECUTE ON  [dbo].[Variance_Processing_SEL_BY_Engine_Id] TO [CBMSApplication]
GO
