SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:                  
    [dbo].[Client_Hier_Account_Search]               
                 
 Description:                  
 To get the account information for the given search criteria.  Uses Full Text indexing and computed columns.            
 Dynamic SQL required for performance as addition OR condition in the WHERE clause results in performance issues.          
           
          
 Input Parameters:                  
    Name    DataType  Default  Description                  
-------------------------------------------------------------------------------------                  
 @Client_Name  VARCHAR(200) null           
 @Site_Name   VARCHAR(200) NULL                    
 @Account_Number  VARCHAR(200) null                     
 @City    VARCHAR(200) null                     
 @State_Name   VARCHAR(50)  null                     
 @Vendor_Name  VARCHAR(200) null          
 @Meter_Number  VARCHAR(200) null          
 @Start_Index  INT    1          
 @End_Index   INT    2147483647          
 @Account_ID   VARCHAR(200) NULL              
          
 Output Parameters:          
    Name      DataType   Default  Description          
--------------------------------------------------------------------------------------          
                 
 Usage Examples:                
--------------------------------------------------------------------------------------                
                  
     EXEC dbo.Client_Hier_Account_Search          
      @Client_Name = 'Kraft'          
     ,@Vendor_Name = 'Hess'          
     ,@Start_Index = 1          
     ,@End_Index = 25          
            
  EXEC dbo.Client_Hier_Account_Search           
   @Client_Name = 'AT&T'          
  ,@State_name = 'NY'          
          
  EXEC dbo.Client_Hier_Account_Search           
   @Site_Name = 'Brillion'          
          
  EXEC dbo.Client_Hier_Account_Search           
   @Client_Name = 'Macy''s'          
          
  EXEC dbo.Client_Hier_Account_Search           
   @Client_Name = 'Kraft Foods Group, Inc. (fka Kraft Foods Global, Inc.)', @Site_Name='Woodstock, IL'          
          
  EXEC dbo.Client_Hier_Account_Search           
   @Account_ID = '123'          
          
    EXEC DBO.Client_Hier_Account_Search           
      @Account_Number = 'OSIR504'  
          
Author Initials:              
 Initials Name              
------------------------------------------------------------              
 HG   Harihara Suthan G          
 RR   Raghu Reddy           
 DMR   Deana Ritter             
 VPIDAPARTHI Venkata Sriram Pavan Kumar Pidaparthi     
 VRV Venkata Reddy Vanga
 MN Nagaraju Muppa         
          
 Modifications :              
 Initials		Date		Modification              
------------------------------------------------------------              
 HG				2014-06-23  Created Data operations Phase-2          
 HG				2014-08-14	MAINT-3007, Modified to handle the account_number search on DO_NOT_TRACK with out removing special characters          
							Also modified to remove the replace statements added to replace the single quote with double single quotes as it is already handled in the application.          
 RR				2014-12-23	MAINT-3245 Removed script part to get number of records based @Total_Rows value           
 DMR			2014-02-07	MAINT-3386 Modified to use Full text indexing. Switched to dynamic SQL as OR condition in the WHERE conflicted with           
							Full text indexing not meeting performance standards (subseconds to minutes).          
 RKV			2018-10-22	D20-287 Added new column Is_DNT               
 VPIDAPARTHI	2019-09-16  Added AccountID input parameter for SE2017-511             
 VPIDAPARTHI	2019-10-04  Changed code for Req change for SE2017-511 - Account_ID Search should be exact match and not partial match like original requirement     
 VRV			2019-10-23  Alternate Account Number is added to search filter criteria for SE2017-618  
 VRV			2019-10-29  Account Group id &  Is_Consolidated_Billing fileds are added to the grip result for SE2017-164.
 HG				2019-12-11	MAINT-9660 decided to return DNT Account Number as Display Account Number if the DNT account is not mapped to any CBMS account. Since research account other pages are using Display Account number column decided to make this change. 
								User should not resolve the invoice to  DNT account if it was displayed on the page.
NM				2020-05-07	Added Group by clause to eliminate duplicate records for consolidated billing accounts in research page.
NR				2020-06-05	SE2017-986 - Added @Rate_Name is added optional parameter.
******/
CREATE PROCEDURE [dbo].[Client_Hier_Account_Search]
    (
        @Client_Name VARCHAR(200) = NULL
        , @Site_Name VARCHAR(200) = NULL
        , @Account_Number VARCHAR(200) = NULL
        , @Alternate_Account_Number VARCHAR(200) = NULL
        , @City VARCHAR(200) = NULL
        , @State_Name VARCHAR(50) = NULL
        , @Vendor_Name VARCHAR(200) = NULL
        , @Meter_Number VARCHAR(200) = NULL
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
        , @User_Info_ID INT = NULL
        , @Account_ID VARCHAR(200) = NULL
        , @Rate_Name VARCHAR(200) = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Account_Number_Search VARCHAR(200) = '"*' + dbo.udf_StripNonAlphaNumerics(@Account_Number) + '*"'
            , @Alternate_Account_Number_Search VARCHAR(200) = '"*'
                                                              + dbo.udf_StripNonAlphaNumerics(@Alternate_Account_Number)
                                                              + '*"'
            , @Account_ID_Search NVARCHAR(200) = @Account_ID    -- dbo.udf_StripNonAlphaNumerics(@Account_ID)           
            , @Meter_Number_Search VARCHAR(200) = '"*' + dbo.udf_StripNonAlphaNumerics(@Meter_Number) + '*"'
            , @Client_Name_Search VARCHAR(200) = '"*' + dbo.udf_StripNonAlphaNumerics(@Client_Name) + '*"'
            , @City_Search VARCHAR(200) = '"*' + dbo.udf_StripNonAlphaNumerics(@City) + '*"'
            , @State_Name_Search VARCHAR(200) = '"*' + dbo.udf_StripNonAlphaNumerics(@State_Name) + '*"'
            , @Vendor_Name_Search VARCHAR(200) = '"*' + dbo.udf_StripNonAlphaNumerics(@Vendor_Name) + '*"'
            , @Site_Name_Search VARCHAR(200) = '"*' + dbo.udf_StripNonAlphaNumerics(@Site_Name) + '*"'
            , @Total_Rows INT = 0
            , @sSQL VARCHAR(MAX)
            , @sSelect VARCHAR(MAX) = ''
            , @sWhere VARCHAR(MAX) = ''
            , @sGroupBy VARCHAR(MAX) = '';

        CREATE TABLE #Tbl_Accounts
             (
                 Account_Id INT
                 , Account_Type VARCHAR(20)
                 , Account_Number VARCHAR(200)
                 , Alternate_Account_Number VARCHAR(200)
                 , Account_Group_ID INT
                 , Meter_Number VARCHAR(50)
                 , Client_Id INT
                 , Client_Name VARCHAR(200)
                 , City VARCHAR(200)
                 , State_Name VARCHAR(200)
                 , Vendor_Id INT
                 , Vendor_Name VARCHAR(200)
                 , Begin_Date DATETIME
                 , End_Date DATETIME
                 , Site_Name VARCHAR(200)
                 , Client_Hier_Id INT
                 , Commodity_Id INT
                 , Sitegroup_Id INT
                 , Site_Id INT
                 , State_Id INT
                 , Contract_ID INT
                 , Is_DNT BIT
                 , Display_Account_Number VARCHAR(500)
                 , Rate_Name VARCHAR(200)
                 , Row_Num INT IDENTITY(1, 1)
             );

        SELECT
            @sSelect = '     INSERT      INTO #Tbl_Accounts          
                  (           
                   Account_Id          
                  ,Account_Type          
                  ,Account_Number        
                  ,Alternate_Account_Number  
				 ,Account_Group_ID          
				,Meter_Number          
                  ,Client_Id          
                  ,Client_Name          
                  ,City          
                  ,State_Name          
                  ,Vendor_Id          
                  ,Vendor_Name          
                  ,Begin_Date          
                  ,End_Date          
                  ,Site_Name          
               ,Client_Hier_Id          
                  ,Commodity_Id          
          ,Sitegroup_Id          
                  ,Site_Id          
                  ,State_Id          
                  ,Contract_ID          
                  ,Is_DNT
				  ,Display_Account_Number
				  ,Rate_Name)  
                
                  SELECT          
                              cha.Account_Id          
                             ,cha.Account_Type          
                             ,cha.Account_Number        
                             ,cha.Alternate_Account_Number  
        ,cha.Account_Group_ID           
                             ,cha.Meter_Number          
                             ,ch.Client_Id          
                             ,ch.Client_Name          
                             ,ch.City          
                             ,ch.State_Name          
                             ,cha.Account_Vendor_Id AS Vendor_Id          
                             ,cha.Account_Vendor_Name AS Vendor_Name          
                             ,cha.Supplier_Account_begin_Dt AS Begin_Date          
                             ,cha.Supplier_Account_End_Dt AS End_Date          
                             ,ch.Site_Name          
                             ,ch.Client_Hier_Id          
                             ,cha.Commodity_Id          
                             ,ch.Sitegroup_Id          
                             ,ch.Site_Id          
                             ,ch.State_Id          
                             ,cha.Supplier_Contract_ID AS Contract_ID          
                             ,cha.Account_Not_Managed Is_DNT   
							 ,Cha.Display_Account_Number       
							 ,Cha.Rate_Name
                          FROM          
                              Core.Client_Hier ch          
                              INNER JOIN Core.Client_Hier_Account cha          
                                    ON cha.Client_Hier_Id = ch.Client_Hier_Id';
        SET @sWhere = CHAR(10) + ' WHERE 1=1';

        IF @Account_Number IS NOT NULL
            BEGIN
                SET @sWhere = @sWhere + CHAR(10) + ' And CONTAINS((cha.Account_Number_FTSearch,cha.Account_Number),'''
                              + @Account_Number_Search + ''')';
            END;

        IF @Alternate_Account_Number IS NOT NULL
            BEGIN
                SET @sWhere = @sWhere + CHAR(10) + ' And CHA.Alternate_Account_Number LIKE ''%''+CAST(' + ''''
                              + RTRIM(LTRIM(@Alternate_Account_Number)) + ''' AS VARCHAR)+''%''';

            END;

        IF @Account_ID_Search IS NOT NULL
            BEGIN
                ----SET @sWhere = @sWhere + CHAR(10) + ' And CHA.Account_ID LIKE ''%''+CAST('+''''+@Account_ID_Search+''' AS VARCHAR)+''%''';          
                SET @sWhere = @sWhere + CHAR(10) + ' And CHA.Account_ID = ' + @Account_ID_Search;
            END;

        IF @Site_Name IS NOT NULL
            BEGIN
                SET @sWhere = @sWhere + CHAR(10) + ' And CONTAINS((ch.Site_Name_FTSearch, ch.Site_Name),'''
                              + @Site_Name_Search + ''')';
            END;
        IF @Client_Name IS NOT NULL
            BEGIN
                SET @sWhere = @sWhere + CHAR(10) + ' And CONTAINS((ch.Client_Name_FTSearch, ch.Client_Name),'''
                              + @Client_Name_Search + ''')';
            END;
        IF @City IS NOT NULL
            BEGIN
                SET @sWhere = @sWhere + CHAR(10) + ' And CONTAINS((ch.CITY_FTSearch, ch.City),''' + @City_Search
                              + ''')';
            END;
        IF @State_Name IS NOT NULL
            BEGIN
                SET @sWhere = @sWhere + CHAR(10) + ' And CONTAINS((ch.State_Name_FTSearch, ch.State_Name),'''
                              + @State_Name_Search + ''')';
            END;
        IF @Vendor_Name IS NOT NULL
            BEGIN
                SET @sWhere = @sWhere + CHAR(10)
                              + ' And CONTAINS((cha.Account_Vendor_Name_FTSearch,cha.Account_Vendor_Name),'''
                              + @Vendor_Name_Search + ''')';
            END;
        IF @Meter_Number IS NOT NULL
            BEGIN
                SET @sWhere = @sWhere + CHAR(10)
                              + ' And CONTAINS((Meter_Number_FTSearch,Meter_Number,Meter_Number_Search),'''
                              + @Meter_Number_Search + ''')';
            END;

        IF @Rate_Name IS NOT NULL
            BEGIN
                SET @sWhere = @sWhere + CHAR(10) + ' And  Cha.Rate_Name LIKE ''%' + @Rate_Name + '%''';
            END;

        SET @sGroupBy = '          
                             GROUP BY          
                              cha.Account_Id          
                             ,cha.Account_Type          
                             ,cha.Account_Number        
                             ,cha.Alternate_Account_Number  
							 ,cha.Account_Group_ID           
                             ,cha.Meter_Number          
                             ,ch.Client_Id          
                             ,ch.Client_Name          
                             ,ch.City          
                             ,ch.State_Name          
                             ,cha.Account_Vendor_Id          
                             ,cha.Account_Vendor_Name          
                             ,cha.Supplier_Account_begin_Dt          
                             ,cha.Supplier_Account_End_Dt          
                             ,ch.Site_Name          
                             ,ch.Client_Hier_Id          
                             ,cha.Commodity_Id          
                             ,ch.Sitegroup_Id          
                             ,ch.Site_Id          
                             ,ch.State_Id          
                             ,cha.Supplier_Contract_ID          
							,cha.Account_Not_Managed
							 ,Cha.Display_Account_Number
							 ,Cha.Rate_Name  ';
        SET @sSQL = @sSelect + @sWhere + @sGroupBy;

        EXEC (@sSQL);

        IF @Site_Name IS NULL
           AND  @Meter_Number IS NULL
            BEGIN
                SELECT
                    @sSelect = '     INSERT      INTO #Tbl_Accounts          
                  (           
                   Account_Id          
                  ,Account_Type          
                  ,Account_Number         
                  ,Alternate_Account_Number  
				  ,Account_Group_ID          
                  ,Meter_Number          
                  ,Client_Id          
                  ,Client_Name          
                  ,City          
                  ,State_Name          
                  ,Vendor_Id          
                  ,Vendor_Name          
                  ,Begin_Date          
                  ,End_Date          
                  ,Site_Name          
                  ,Client_Hier_Id          
                  ,Commodity_Id          
                  ,Sitegroup_Id          
                  ,Site_Id          
                  ,State_Id          
                  ,Contract_ID          
				  ,Is_DNT   
				  ,Display_Account_Number     
				  ,Rate_Name  
                   )          
         SELECT          
                              dnt.Do_Not_Track_Id AS Account_Id          
                              ,''Unknown DNT'' AS Account_Type          
                             ,dnt.Account_Number        
                             ,cha.Alternate_Account_Number  
							 ,cha.Account_Group_ID           
                             ,NULL AS Meter_Number          
                             ,NULL AS Client_Id          
                             ,dnt.Client_Name          
                             ,dnt.Client_City AS City          
                             ,st.State_Name          
							 ,NULL AS Vendor_Id          
                             ,dnt.Vendor_Name          
                             ,cast(NULL AS DATETIME) AS Begin_Date          
                             ,cast(NULL AS DATETIME) AS End_Date          
                             ,NULL AS Site_Name  
                             ,NULL AS Client_Hier_Id          
                             ,NULL AS Commodity_Id          
                             ,NULL AS Sitegroup_Id          
                             ,NULL AS Site_Id          
                             ,NULL AS State_Id          
                             ,NULL AS Contract_ID          
                             ,1 AS Is_DNT   
							 , CASE 
									WHEN cha.Account_Id IS NOT NULL THEN Cha.Display_Account_Number       
									ELSE dnt.Account_Number 
								END AS Display_Account_Number
								,cha.Rate_Name
                          FROM
                              dbo.Do_Not_Track dnt
                              INNER JOIN dbo.State AS st
                                    ON st.state_id = dnt.state_id
                LEFT JOIN Core.Client_Hier_Account cha
                         ON dnt.ACCOUNT_ID=cha.Account_Id';
                SET @sWhere = CHAR(10)
                              + ' WHERE 1=1 AND NOT EXISTS(SELECT 1 FROM #Tbl_Accounts ta WHERE ta.Account_Id = dnt.Account_Id )';

                IF @Account_Number IS NOT NULL
                    BEGIN
                        SET @sWhere = @sWhere + CHAR(10)
                                      + ' And CONTAINS((dnt.Account_Number_FTSearch,dnt.Account_Number),'''
                                      + @Account_Number_Search + ''')';
                    END;

                IF @Account_ID_Search IS NOT NULL
                    BEGIN
                        ----SET @sWhere = @sWhere + CHAR(10) + ' And dnt.Do_Not_Track_Id LIKE ''%''+CAST('+''''+@Account_ID_Search+''' AS VARCHAR)+''%''';          
                        SET @sWhere = @sWhere + CHAR(10) + ' And dnt.Do_Not_Track_Id = ' + @Account_ID_Search;
                    END;

                IF @Alternate_Account_Number IS NOT NULL
                    BEGIN
                        SET @sWhere = @sWhere + CHAR(10) + ' And cha.Alternate_Account_Number LIKE ''%''+CAST(' + ''''
                                      + @Alternate_Account_Number + ''' AS VARCHAR)+''%''';

                    END;


                IF @Client_Name IS NOT NULL
                    BEGIN
                        SET @sWhere = @sWhere + CHAR(10)
                                      + ' And CONTAINS((dnt.Client_Name_FTSearch,dnt.Client_Name),'''
                                      + @Client_Name_Search + ''')';
                    END;
                IF @City IS NOT NULL
                    BEGIN
                        SET @sWhere = @sWhere + CHAR(10)
                                      + ' And CONTAINS((dnt.Client_CITY_FTSearch,dnt.Client_CITY),''' + @City_Search
                                      + ''')';
                    END;
                IF @State_Name IS NOT NULL
                    BEGIN
                        SET @sWhere = @sWhere + CHAR(10) + ' And  st.State_Name LIKE ''%' + @State_Name + '%''';
                    END;
                IF @Vendor_Name IS NOT NULL
                    BEGIN
                        SET @sWhere = @sWhere + CHAR(10)
                                      + ' And CONTAINS((dnt.Vendor_Name_FTSearch,dnt.Vendor_Name),'''
                                      + @Vendor_Name_Search + ''')';
                    END;



                IF @Rate_Name IS NOT NULL
                    BEGIN
                        SET @sWhere = @sWhere + CHAR(10) + ' And  Cha.Rate_Name LIKE ''%' + @Rate_Name + '%''';
                    END;




                SET @sSQL = @sSelect + @sWhere;

                EXEC (@sSQL);

            END;

        SELECT  @Total_Rows = MAX(Row_Num)FROM  #Tbl_Accounts;

        SELECT
            cha.Account_Id
            , cha.Account_Type
            , cha.Account_Number
            , cha.Account_Group_ID
            , cha.Meter_Number
            , cha.Client_Id
            , cha.Client_Name
            , cha.City
            , cha.State_Name
            , cha.Vendor_Id
            , cha.Vendor_Name
            , cha.Begin_Date
            , cha.End_Date
            , cha.Site_Name
            , cha.Client_Hier_Id
            , cha.Commodity_Id
            , com.Commodity_Name
            , cha.Sitegroup_Id
            , cha.Site_Id
            , cha.State_Id
            , cha.Contract_ID
            , cha.Is_DNT
            , MAX(CASE WHEN acb.Account_Id IS NOT NULL THEN 1
                      ELSE 0
                  END) AS Is_Consolidated_Billing
            , @Total_Rows AS Total_Rows
            , cha.Display_Account_Number
            , cha.Rate_Name
        FROM
            #Tbl_Accounts cha
            LEFT OUTER JOIN dbo.Commodity com
                ON com.Commodity_Id = cha.Commodity_Id
            LEFT OUTER JOIN Account_Consolidated_Billing_Vendor acb
                ON acb.Account_Id = cha.Account_Id
        WHERE
            cha.Row_Num BETWEEN @Start_Index
                        AND     @End_Index
        GROUP BY
            cha.Account_Id
            , cha.Account_Type
            , cha.Account_Number
            , cha.Account_Group_ID
            , cha.Meter_Number
            , cha.Client_Id
            , cha.Client_Name
            , cha.City
            , cha.State_Name
            , cha.Vendor_Id
            , cha.Vendor_Name
            , cha.Begin_Date
            , cha.End_Date
            , cha.Site_Name
            , cha.Client_Hier_Id
            , cha.Commodity_Id
            , com.Commodity_Name
            , cha.Sitegroup_Id
            , cha.Site_Id
            , cha.State_Id
            , cha.Contract_ID
            , cha.Is_DNT
            , cha.Display_Account_Number
            , cha.Rate_Name
        ORDER BY
            cha.Account_Number;

        DROP TABLE #Tbl_Accounts;

    END;

GO



GRANT EXECUTE ON  [dbo].[Client_Hier_Account_Search] TO [CBMSApplication]
GO
