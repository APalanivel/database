
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME: 
		dbo.GET_ENTITY_AUDIT_LIST_P            

DESCRIPTION:            
This procedure is to get the details of the last user modified the entity/table             
            
INPUT PARAMETERS:            
	Name				DataType		Default		Description            
-----------------------------------------------------------------            
	@ENTITY_IDENTIFIER	INT
    @ENTITY_NAME		VARCHAR(200)
    @ENTITY_TYPE		INT         
            
OUTPUT PARAMETERS:            
	Name				DataType		Default		Description            
-----------------------------------------------------------------            
                 
           
                
USAGE EXAMPLES:            
------------------------------------------------------------            
	SELECT * FROM dbo.ENTITY WHERE ENTITY_TYPE=500
	SELECT TOP 10 * FROM dbo.ENTITY_AUDIT a JOIN dbo.ENTITY b ON a.ENTITY_ID = b.ENTITY_ID WHERE b.ENTITY_NAME='CONTRACT_TABLE' AND b.ENTITY_TYPE=500
	ORDER BY a.MODIFIED_DATE DESC
	
	EXEC dbo.GET_ENTITY_AUDIT_LIST_P @ENTITY_IDENTIFIER=462615,@ENTITY_NAME='ACCOUNT_TABLE',@ENTITY_TYPE=500
	EXEC dbo.GET_ENTITY_AUDIT_LIST_P @ENTITY_IDENTIFIER=109112,@ENTITY_NAME='CONTRACT_TABLE',@ENTITY_TYPE=500
	           
AUTHOR INITIALS:            
	Initials	Name            
------------------------------------------------------------            
	RR			Raghu Reddy            
            
MODIFICATIONS:            
	Initials	Date		Modification            
------------------------------------------------------------            
	RR			2014-05-23	Data Operations Enhancements - Added first name and last name in select list
      
******/

CREATE  PROCEDURE dbo.GET_ENTITY_AUDIT_LIST_P
      @ENTITY_IDENTIFIER INT
     ,@ENTITY_NAME VARCHAR(200)
     ,@ENTITY_TYPE INT
AS 
BEGIN

      SET NOCOUNT ON;

      SELECT TOP 1
            entityAudit.MODIFIED_DATE
           ,userInfo.USERNAME
           ,userInfo.FIRST_NAME + ' ' + userInfo.LAST_NAME AS Name
      FROM
            dbo.ENTITY_AUDIT entityAudit
            INNER JOIN dbo.USER_INFO userInfo
                  ON entityAudit.USER_INFO_ID = userInfo.USER_INFO_ID
            INNER JOIN dbo.Entity e
                  ON e.Entity_ID = entityAudit.Entity_ID
      WHERE
            entityAudit.ENTITY_IDENTIFIER = @ENTITY_IDENTIFIER
            AND e.ENTITY_NAME = @ENTITY_NAME
            AND e.ENTITY_TYPE = @ENTITY_TYPE
      ORDER BY
            entityAudit.MODIFIED_DATE DESC

END
;
GO

GRANT EXECUTE ON  [dbo].[GET_ENTITY_AUDIT_LIST_P] TO [CBMSApplication]
GO
