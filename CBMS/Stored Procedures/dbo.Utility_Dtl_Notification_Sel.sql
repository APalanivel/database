SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Utility_Dtl_Notification_Sel

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Vendor_ID				INT
	@Commodity_Id			INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC Utility_Dtl_Notification_Sel 30,290
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	SKA			01/12/2011	Created
******/

CREATE PROC [dbo].[Utility_Dtl_Notification_Sel]
      ( 
       @Vendor_ID INT
      ,@Commodity_Id INT )
AS 
BEGIN
      SET NOCOUNT ON ;
	
      SELECT
            utl_nt.Utility_Dtl_Notification_Id
           ,qcm.Question_Commodity_Map_Id
           ,usq.Question_Label
           ,utl_nt.Industrial_Flag_Cd
           ,cd1.Code_Value AS Industrial_Flag_Value
           ,utl_nt.Commercial_Flag_Cd
           ,cd2.Code_Value AS Commercial_Flag_Value
           ,utl_nt.Comment_ID
           ,cmt.Comment_Text
      FROM
            dbo.Question_Commodity_Map qcm
            INNER JOIN dbo.Utility_Summary_Question usq
                  ON usq.Utility_Summary_Question_Id = qcm.Utility_Summary_Question_Id
            INNER JOIN dbo.Utility_Summary_Section uss
                  ON uss.Utility_Summary_Section_Id = usq.Utility_Summary_Section_Id
            INNER JOIN VENDOR_COMMODITY_MAP vcm
                  ON vcm.COMMODITY_TYPE_ID = qcm.Commodity_Id
            LEFT JOIN dbo.Utility_Dtl_Notification utl_nt
                  ON vcm.VENDOR_COMMODITY_MAP_ID = utl_nt.VENDOR_COMMODITY_MAP_ID
                     AND qcm.Question_Commodity_Map_Id = utl_nt.Question_Commodity_Map_Id
            LEFT JOIN dbo.Code cd1
                  ON cd1.Code_Id = utl_nt.Industrial_Flag_Cd
            LEFT JOIN dbo.Code cd2
                  ON cd2.Code_Id = utl_nt.Commercial_Flag_Cd
            LEFT JOIN dbo.Comment cmt
                  ON cmt.Comment_ID = utl_nt.Comment_ID
      WHERE
            vcm.VENDOR_ID = @Vendor_ID
            AND vcm.COMMODITY_TYPE_ID = @Commodity_Id
            AND usq.Is_Active = 1
            AND uss.Section_Label = 'Notifications/Deadlines'
      ORDER BY
            usq.Display_Order
END
GO
GRANT EXECUTE ON  [dbo].[Utility_Dtl_Notification_Sel] TO [CBMSApplication]
GO
