SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE     PROCEDURE [dbo].[cbmsUbmInvoice_GetProcessedForBatch]
	( @ubm_batch_master_log_id int )
AS
BEGIN

	   select ui.ubm_invoice_id
		, cu.cu_invoice_id
		, vw.client_name
		, vw.site_name
		, vw.account_number
		, vw.service_month
		, case when cu.is_processed = 1 then 'Yes' else 'No' end is_processed
		, case when cu.is_reported = 1 then 'Yes' else 'No' end is_reported
		, case when cu.is_dnt = 1 then 'Yes' else 'No' end is_dnt
		, case when cu.is_duplicate = 1 then 'Yes' else 'No' end is_duplicate
		, vw.queue_name
		, t.exception_type
		, case when d.is_closed = 1 then 'Yes' else 'No' end exception_is_closed
		, cr.entity_name closed_reason
		, u.first_name + ' ' + u.last_name
	     from ubm_invoice ui with (nolock)
  left outer join cu_invoice cu with (nolock) on cu.ubm_invoice_id = ui.ubm_invoice_id
  left outer join vwCbmsCuInvoiceDetails vw with (nolock) on vw.cu_invoice_id = cu.cu_invoice_id
  left outer join cu_exception ex with (nolock) on ex.cu_invoice_id = cu.cu_invoice_id  
  left outer join cu_exception_detail d with (nolock) on d.cu_exception_id = ex.cu_exception_id
  left outer join cu_exception_type t with (nolock) on t.cu_exception_type_id = d.exception_type_id
  left outer join entity cr with (nolock) on cr.entity_id = d.closed_reason_type_id
  left outer join user_info u with (nolock) on u.user_info_id = d.closed_by_id
	    where ui.ubm_batch_master_log_id = @ubm_batch_master_log_id

	 order by vw.client_name
		, vw.site_name
		, vw.account_number
		, vw.service_month

END
GO
GRANT EXECUTE ON  [dbo].[cbmsUbmInvoice_GetProcessedForBatch] TO [CBMSApplication]
GO
