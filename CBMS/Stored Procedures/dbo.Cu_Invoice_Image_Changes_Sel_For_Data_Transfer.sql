
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******            
NAME:   dbo.Cu_Invoice_Image_Changes_Sel_For_Data_Transfer
  
      
DESCRIPTION:    
 Gets CU_Invoice Image data for CU_Invoice records that have changed  
   
 assumes CBMS_Image_Id and Service_Month are populated      
   
        
INPUT PARAMETERS:            
Name   DataType Default  Description            
------------------------------------------------------------            
  
                  
OUTPUT PARAMETERS:            
Name   DataType Default  Description            
------------------------------------------------------------   
  
           
USAGE EXAMPLES:            
------------------------------------------------------------   
 EXEC dbo.Cu_Invoice_Image_Changes_Sel_For_Data_Transfer 0  
         
       
AUTHOR INITIALS:            
Initials Name            
------------------------------------------------------------            
AKR   Ashok Kumar Raju
DMR	  Deana Ritter	
       
       
MODIFICATIONS             
Initials Date Modification            
------------------------------------------------------------            
AKR   2011-12-30 Created  
DMR		12/8/2014	Removed Transaction Isolation Level statements.  Disabled Database level snap shot isolation.
*****/   
CREATE PROCEDURE [dbo].[Cu_Invoice_Image_Changes_Sel_For_Data_Transfer]
      ( 
       @Min_CT_Ver BIGINT
       ,@Max_CT_Ver BIGINT )
AS 
BEGIN  
      
  
    
 -- Data is continious: get changes  
      SELECT
            cha.Client_Hier_Id
           ,cha.Commodity_Id
           ,ism.Account_Id
           ,cng.Cu_Invoice_Id
           ,convert(DATE, ism.Service_Month) AS Service_Month
           ,ci.CBMS_IMAGE_ID
           ,convert(CHAR(1), cng.Sys_CHANGE_OPERATION) AS Sys_Change_Operation
      FROM
            CHANGETABLE(CHANGES dbo.CU_Invoice, @Min_CT_Ver) cng
            LEFT JOIN dbo.cu_Invoice_Service_Month ism
                  ON cng.CU_invoice_Id = ism.CU_invoice_Id
                     AND ism.Service_Month IS NOT NULL
            LEFT JOIN Core.Client_Hier_Account cha
                  ON ism.Account_id = cha.Account_id
            LEFT JOIN dbo.CU_INVOICE ci
                  ON ci.Cu_Invoice_Id = cng.Cu_Invoice_Id
                     AND ci.IS_REPORTED = 1
      WHERE
            ( cng.Sys_Change_Operation = 'd'
              OR ci.CBMS_Image_Id IS NOT NULL )
              AND cng.SYS_CHANGE_VERSION <= @Max_CT_Ver
      GROUP BY
            cha.Client_Hier_Id
           ,cha.Commodity_Id
           ,ism.ACCOUNT_ID
           ,cng.Cu_Invoice_id
           ,convert(DATE, ism.Service_Month)
           ,ci.CBMS_IMAGE_ID
           ,cng.Sys_CHANGE_OPERATION  
END  



;
GO


GRANT EXECUTE ON  [dbo].[Cu_Invoice_Image_Changes_Sel_For_Data_Transfer] TO [CBMSApplication]
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Image_Changes_Sel_For_Data_Transfer] TO [ETL_Execute]
GO
