
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
Name:  
 CBMS.dbo.Invoice_Participation_Completed_Status_Sel_By_Site
 
 Description:  
 
 Input Parameters:  
    Name			    DataType		Default Description  
------------------------------------------------------------------------  
    @Report_Year	    INT			    
    @Client_Id		    INT			    NULL
    @Division_Id	    INT			    NULL
    @Client_Hier_Id	    INT			    NULL
    @RegionId		    INT			    NULL
    @Country_Id			INT			    NULL
    @Site_Not_Managed   INT			    NULL
    @StartIndex			INT				1
    @EndIndex			INT				2147483647  
 
 Output Parameters:  
 Name  Datatype  Default Description  
------------------------------------------------------------  
 
 Usage Examples:
------------------------------------------------------------  
EXECUTE dbo.Invoice_Participation_Completed_Status_Sel_By_Site 2010, 235, NULL, NULL, NULL, NULL, NULL,1,100
EXECUTE dbo.Invoice_Participation_Completed_Status_Sel_By_Site 2011, 170,NULL, NULL, NULL, NULL, NULL,1,50
EXECUTE dbo.Invoice_Participation_Completed_Status_Sel_By_Site 2008, 170, NULL, 2195, NULL, NULL, NULL,1,4
 
Author Initials:  
 Initials	  Name
------------------------------------------------------------
 AP		  Athmaram Pabbathi
 BCH	  Balaraju Chalumuri

 
 Modifications :  
 Initials	  Date	     Modification  
------------------------------------------------------------  
 AP			 10/10/2011  Created and replaced flowlling SPs as a part of Addtl Data changes
						 - dbo.cbmsCostUsageSite_LoadForSite
 AP			 04/02/2012  Replaced @Site_Id param with @Client_Hier_Id; added Client_Hier_Id in result set
 BCH		 2012-08-06  (For MAIN -1295) Added StartIndex and EndIndex input parameters for Pagination and Pivoted rows for Performance issue.
******/
CREATE PROCEDURE [dbo].Invoice_Participation_Completed_Status_Sel_By_Site
      ( 
       @Report_Year INT
      ,@Client_Id INT = NULL
      ,@Division_Id INT = NULL
      ,@Client_Hier_Id INT = NULL
      ,@Region_Id INT = NULL
      ,@State_Id INT = NULL
      ,@Site_Type_Id INT = NULL
      ,@StartIndex INT = 1
      ,@EndIndex INT = 2147483647 )
AS 
BEGIN

      SET NOCOUNT ON
      
      DECLARE
            @Start_Month DATE
           ,@End_Month DATE
           ,@Usd_Currency_Unit_Id INT
           ,@Site_Type_Name VARCHAR(200)

      DECLARE @Service_Months TABLE
            ( 
             Service_Month DATE PRIMARY KEY CLUSTERED
            ,Month_Num SMALLINT ) 

      CREATE TABLE #Client_Dtl
            ( 
             Client_Name VARCHAR(200)
            ,Division_Name VARCHAR(200)
            ,City VARCHAR(200)
            ,Address_Line1 VARCHAR(200)
            ,State_Name VARCHAR(20)
            ,Region_Name VARCHAR(200)
            ,Site_Id INT
            ,Client_Hier_Id INT
            ,Client_Currency_Group_Id INT
            ,Total_Rows INT PRIMARY KEY CLUSTERED ( Site_Id, client_hier_id ) )
       
      SELECT
            @Usd_Currency_Unit_Id = CU.Currency_Unit_Id
      FROM
            dbo.Currency_Unit CU
      WHERE
            CU.Symbol = 'USD'
            
      SELECT
            @Site_Type_Name = E.Entity_Name
      FROM
            dbo.ENTITY E
      WHERE
            E.Entity_Id = @Site_Type_Id        

      INSERT      INTO @Service_Months
                  ( 
                   Service_Month
                  ,Month_Num )
                  SELECT
                        dd.Date_D
                       ,row_number() OVER ( ORDER BY dd.Date_D )
                  FROM
                        ( SELECT
                              DATEADD(m, ch.Client_Fiscal_Offset, CAST('1/1/' + CAST(@Report_Year AS VARCHAR(4)) AS DATE)) Start_month
                          FROM
                              Core.Client_Hier ch
                          WHERE
                              ch.Client_ID = @Client_Id
                              AND ch.Sitegroup_Id = 0
                              AND ch.Site_Id = 0
                          UNION ALL
                          SELECT
                              CAST('1/1/' + CAST(@report_year AS VARCHAR(4)) AS VARCHAR(10))
                          WHERE
                              @client_id IS NULL ) X
                        CROSS JOIN meta.Date_Dim dd
                  WHERE
                        dd.Date_D BETWEEN x.Start_month
                                  AND     DATEADD(MONTH, -1, ( DATEADD(YEAR, 1, X.Start_Month) ))

      SELECT
            @Start_Month = MIN(sm.Service_Month)
           ,@End_Month = MAX(sm.Service_Month)
      FROM
            @Service_Months sm ;
      WITH  cte_Client_Detail
              AS ( SELECT
                        CH.Client_Name
                       ,CH.Sitegroup_Name
                       ,CH.City
                       ,CH.Site_Address_Line1
                       ,CH.State_Name
                       ,CH.Region_Name
                       ,CH.Site_Id
                       ,ch.Client_Hier_Id
                       ,CH.Client_Currency_Group_Id
                       ,ROW_NUMBER() OVER ( ORDER BY ch.Client_Name , ch.Sitegroup_Name , ch.City , ch.Site_Id ) Row_Num
                       ,COUNT(1) OVER ( ) Total_Rows
                   FROM
                        Core.Client_Hier CH
                   WHERE
                        ( @Client_Id IS NULL
                          OR CH.Client_Id = @Client_Id )
                        AND ( @Division_Id IS NULL
                              OR CH.Sitegroup_Id = @Division_Id )
                        AND ( @Client_Hier_Id IS NULL
                              OR CH.Client_Hier_Id = @Client_Hier_Id )
                        AND ( @Site_Type_Id IS NULL
                              OR ch.Site_Type_Name = @Site_Type_Name )
                        AND ( @State_Id IS NULL
                              OR CH.State_Id = @State_Id )
                        AND ( @Region_Id IS NULL
                              OR CH.Region_ID = @Region_Id )
                        AND CH.Client_Not_Managed = 0
                        AND CH.Site_Not_Managed = 0
                        AND CH.Site_Closed = 0
                        AND CH.Division_Not_Managed = 0
                   GROUP BY
                        CH.Client_Name
                       ,CH.Sitegroup_Name
                       ,CH.City
                       ,CH.Site_Address_Line1
                       ,CH.State_Name
                       ,CH.Region_Name
                       ,CH.Site_Id
                       ,ch.Client_Hier_Id
                       ,CH.Client_Currency_Group_Id)
            INSERT      INTO #Client_Dtl
                        ( 
                         Client_Name
                        ,Division_Name
                        ,City
                        ,Address_Line1
                        ,State_Name
                        ,Region_Name
                        ,Site_Id
                        ,Client_Hier_Id
                        ,Client_Currency_Group_Id
                        ,Total_Rows )
                        SELECT
                              Client_Name
                             ,Sitegroup_Name
                             ,City
                             ,Site_Address_Line1
                             ,State_Name
                             ,Region_Name
                             ,Site_Id
                             ,Client_Hier_Id
                             ,Client_Currency_Group_Id
                             ,Total_Rows
                        FROM
                              cte_Client_Detail
                        WHERE
                              Row_Num BETWEEN @StartIndex AND @EndIndex ;                    
                    
      WITH  CTE_Invoice_Participation_Dtl
              AS ( SELECT
                        CDtl.Client_Name
                       ,CDtl.Division_Name
                       ,CDtl.City
                       ,CDtl.Address_Line1
                       ,CDtl.State_Name
                       ,CDtl.Region_Name
                       ,CDtl.City AS Site_Name
                       ,CDtl.Site_Id
                       ,CDtl.Client_Hier_Id
                       ,@Start_Month AS Fiscal_Start
                       ,@End_Month AS Fiscal_End
                       ,sm.Service_Month
                       ,sm.Month_Num
                       ,( CASE WHEN SUM(CAST(IP.Is_Expected AS INT)) = SUM(CAST(IP.Is_Received AS INT)) THEN 1
                               WHEN SUM(CAST(IP.Is_Expected AS INT)) != SUM(CAST(IP.Is_Received AS INT)) THEN 0
                          END ) AS Complete
                       ,CDtl.Total_Rows
                   FROM
                        #Client_Dtl CDtl
                        CROSS APPLY @Service_Months sm
                        LEFT OUTER JOIN dbo.Invoice_Participation IP
                              ON IP.Site_Id = CDtl.Site_Id
                                 AND IP.Service_Month = sm.Service_Month
                   GROUP BY
                        CDtl.Client_Name
                       ,CDtl.Division_Name
                       ,CDtl.City
                       ,CDtl.Address_Line1
                       ,CDtl.State_Name
                       ,CDtl.Region_Name
                       ,CDtl.Site_Id
                       ,CDtl.Client_Hier_Id
                       ,sm.Service_Month
                       ,sm.Month_Num
                       ,CDtl.Total_Rows)
            SELECT
                  Client_Name
                 ,Division_Name
                 ,City
                 ,Address_Line1
                 ,State_Name
                 ,Region_Name
                 ,Site_Name
                 ,Site_Id
                 ,Client_Hier_Id
                 ,Fiscal_Start
                 ,Fiscal_End
                 ,MAX(CASE WHEN Month_Num = 1
                                AND Complete = 1 THEN 'yes'
                           ELSE 'No'
                      END) Month1
                 ,MAX(CASE WHEN Month_Num = 2
                                AND Complete = 1 THEN 'yes'
                           ELSE 'No'
                      END) Month2
                 ,MAX(CASE WHEN Month_Num = 3
                                AND Complete = 1 THEN 'yes'
                           ELSE 'No'
                      END) Month3
                 ,MAX(CASE WHEN Month_Num = 4
                                AND Complete = 1 THEN 'yes'
                           ELSE 'No'
                      END) Month4
                 ,MAX(CASE WHEN Month_Num = 5
                                AND Complete = 1 THEN 'yes'
                           ELSE 'No'
                      END) Month5
                 ,MAX(CASE WHEN Month_Num = 6
                                AND Complete = 1 THEN 'yes'
                           ELSE 'No'
                      END) Month6
                 ,MAX(CASE WHEN Month_Num = 7
                                AND Complete = 1 THEN 'yes'
                           ELSE 'No'
                      END) Month7
                 ,MAX(CASE WHEN Month_Num = 8
                                AND Complete = 1 THEN 'yes'
                           ELSE 'No'
                      END) Month8
                 ,MAX(CASE WHEN Month_Num = 9
                                AND Complete = 1 THEN 'yes'
                           ELSE 'No'
                      END) Month9
                 ,MAX(CASE WHEN Month_Num = 10
                                AND Complete = 1 THEN 'yes'
                           ELSE 'No'
                      END) Month10
                 ,MAX(CASE WHEN Month_Num = 11
                                AND Complete = 1 THEN 'yes'
                           ELSE 'No'
                      END) Month11
                 ,MAX(CASE WHEN Month_Num = 12
                                AND Complete = 1 THEN 'yes'
                           ELSE 'No'
                      END) Month12
                 ,Total_Rows
            FROM
                  CTE_Invoice_Participation_Dtl
            GROUP BY
                  Client_Name
                 ,Division_Name
                 ,City
                 ,Address_Line1
                 ,State_Name
                 ,Region_Name
                 ,Site_Name
                 ,Site_Id
                 ,Client_Hier_Id
                 ,Fiscal_Start
                 ,Fiscal_End
                 ,Total_Rows

      DROP TABLE #Client_Dtl
END ;

;
GO

GRANT EXECUTE ON  [dbo].[Invoice_Participation_Completed_Status_Sel_By_Site] TO [CBMSApplication]
GO
