
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.UBM_GET_UBM_CASS_EXPECTED_RECORDS_P

DESCRIPTION:

INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@userId				VARCHAR(10)
	@sessionId			VARCHAR(20)
	@masterLogId		INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	Exec dbo.UBM_GET_UBM_CASS_EXPECTED_RECORDS_P -1,-1,16768

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan G

MODIFICATIONS
	Initials	Date			Modification
------------------------------------------------------------
	HG			2014-10-16		Modified for Prokarma rename , added condition to exclude the Cass_tablename for Prokarma
******/

CREATE PROCEDURE [dbo].[UBM_GET_UBM_CASS_EXPECTED_RECORDS_P]
      @userId VARCHAR(10)
     ,@sessionId VARCHAR(20)
     ,@masterLogId INT
AS 
BEGIN

      SET NOCOUNT ON
	
      SELECT
            'ubm_' + CASS_TABLENAME AS CASS_TABLE_NAME
           ,EXPECTED_DATA
      FROM
            dbo.Ubm_Cass_Table_Count
      WHERE
            Ubm_Batch_Master_Log_Id = @masterLogId
            AND CASS_TABLENAME NOT IN ( 'summit_building', 'cass_building', 'summit_weather', 'cass_weather', 'summit_facility_group', 'cass_facility_group', 'cass_meter', 'summit_meter', 'summit_check', 'cass_check', 'gfe_building', 'gfe_weather', 'gfe_facility_group', 'gfe_meter', 'gfe_check', 'prokarma_building', 'prokarma_weather', 'prokarma_facility_group', 'prokarma_meter', 'prokarma_check' )
				
END

;
GO

GRANT EXECUTE ON  [dbo].[UBM_GET_UBM_CASS_EXPECTED_RECORDS_P] TO [CBMSApplication]
GO
