SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********
NAME:  dbo.TOU_Schedule_Term_Ins

DESCRIPTION:  
	Insert a new schedule term

INPUT PARAMETERS:
Name								DataType        Default     Description
-------------------------------------------------------------------
@Time_Of_Use_Schedule_Id			INT
@Start_Dt							DATETIME
@End_Dt								DATETIME		NULL
@CBMS_Image_Id						INT				NULL
@Base_Time_Of_Use_Schedule_Term_Id	INT				NULL				
@oTime_Of_Use_Schedule_Term_Id		INT							OUTPUT

OUTPUT PARAMETERS:
      Name								DataType          Default     Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
DECLARE @oTime_Of_Use_Schedule_Term_Id INT

EXEC dbo.TOU_Schedule_Term_Ins
 @Time_Of_Use_Schedule_Id  =100
,@Start_Dt = '2012-01-01'
,@End_Dt = NULL
,@CBMS_Image_Id = NULL
,@Base_Time_Of_Use_Schedule_Term_Id = NULL
,@oTime_Of_Use_Schedule_Term_Id = @oTime_Of_Use_Schedule_Term_Id OUTPUT

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CPE			Chaitanya Panduga Eshwar


	Initials Date		Modification
------------------------------------------------------------
	CPE		2012-07-11  Created

******/
CREATE PROCEDURE dbo.TOU_Schedule_Term_Ins
( 
 @Time_Of_Use_Schedule_Id INT
,@Start_Dt DATETIME
,@End_Dt DATETIME = NULL
,@CBMS_Image_Id INT = NULL
,@Base_Time_Of_Use_Schedule_Term_Id INT = NULL
,@oTime_Of_Use_Schedule_Term_Id INT OUTPUT )
AS 
BEGIN
      SET NOCOUNT ON

      SET @End_Dt = ISNULL(@End_Dt, '2099-12-31')

      BEGIN TRY
            BEGIN TRAN

			-- End a previous open term if the passed in start date is greater than the corresponding start date
            UPDATE
                  dbo.Time_Of_Use_Schedule_Term
            SET   
                  End_Dt = DATEADD(DD, -1, @Start_Dt)
            WHERE
                  Time_Of_Use_Schedule_Id = @Time_Of_Use_Schedule_Id
                  AND End_Dt = '2099-12-31'
                  AND Start_Dt < @Start_Dt
		
            INSERT
                  dbo.Time_Of_Use_Schedule_Term
                  ( 
                   Start_Dt
                  ,End_Dt
                  ,CBMS_IMAGE_ID
                  ,Base_Time_Of_Use_Schedule_Term_Id
                  ,Time_Of_Use_Schedule_Id )
                  SELECT
                        @Start_Dt
                       ,@End_Dt
                       ,@CBMS_Image_Id
                       ,@Base_Time_Of_Use_Schedule_Term_Id
                       ,@Time_Of_Use_Schedule_Id
                 
            SELECT
                  @oTime_Of_Use_Schedule_Term_Id = SCOPE_IDENTITY()
            COMMIT TRAN
      END TRY
      BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK
            EXEC dbo.usp_RethrowError
      END CATCH
END 





;
GO
GRANT EXECUTE ON  [dbo].[TOU_Schedule_Term_Ins] TO [CBMSApplication]
GO
