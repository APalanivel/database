SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******         
                     
 NAME: dbo.CU_Invoice_CU_Data_Correction_Batch_Dtl_Ins                      
                        
 DESCRIPTION:                        
        To save the batch details.                        
                        
 INPUT PARAMETERS:        
                       
 Name										DataType           Default       Description        
--------------------------------------------------------------------------------------- 
 @CU_Invoice_CU_Data_Correction_Batch_Id	INT
 @From_Account_Id							INT
 @To_Account_Id								INT
 @Cu_Invoice_Id								INT
 @Service_Month								DATE
 @Action_Cd									INT
 @Status_Cd									INT  
 @Data_Source								VARCHAR(25)			NULL            
                        
 OUTPUT PARAMETERS:             
                        
 Name										DataType           Default       Description        
--------------------------------------------------------------------------------------- 
                        
 USAGE EXAMPLES:        
--------------------------------------------------------------------------------------- 
   BEGIN TRAN     
   
   Select * from CU_Invoice_CU_Data_Correction_Batch_Dtl  where  CU_Invoice_CU_Data_Correction_Batch_Id = 6   
   EXEC dbo.CU_Invoice_CU_Data_Correction_Batch_Dtl_Ins 
      @CU_Invoice_CU_Data_Correction_Batch_Id = 6
     ,@From_Account_Id = 100
     ,@To_Account_Id = 101
     ,@Cu_Invoice_Id = '10015,10016,10015,10015'
     ,@Service_Month = '2017-03-09'
     ,@Action_Cd = 1
     ,@Status_Cd = 1   
   Select * from CU_Invoice_CU_Data_Correction_Batch_Dtl  where  CU_Invoice_CU_Data_Correction_Batch_Id = 6   
   ROLLBACK TRAN  
                     
 AUTHOR INITIALS:        
       
 Initials               Name        
--------------------------------------------------------------------------------------- 
 NR                     Narayana Reddy
                         
 MODIFICATIONS:      
       
 Initials               Date             Modification      
--------------------------------------------------------------------------------------- 
 NR                     2017-03-09      Created for Contract Placeholder.                     
                       
******/    
CREATE	 PROCEDURE [dbo].[CU_Invoice_CU_Data_Correction_Batch_Dtl_Ins]
      ( 
       @CU_Invoice_CU_Data_Correction_Batch_Id INT
      ,@From_Account_Id INT
      ,@To_Account_Id INT = NULL
      ,@Cu_Invoice_Id VARCHAR(MAX) = NULL
      ,@Service_Month DATE = NULL
      ,@Action_Cd INT
      ,@Status_Cd INT )
AS 
BEGIN 
                       
      SET NOCOUNT ON;     
      
      DECLARE @Tbl_Inv_Service_Months AS TABLE
            ( 
             Cu_Invoice_Id INT
            ,Service_Month DATETIME )
            
      INSERT      INTO @Tbl_Inv_Service_Months
                  ( 
                   Cu_Invoice_Id
                  ,Service_Month )
                  SELECT
                        cism.CU_INVOICE_ID
                       ,cism.SERVICE_MONTH
                  FROM
                        dbo.CU_INVOICE_SERVICE_MONTH cism
                        INNER JOIN dbo.ufn_split(@Cu_Invoice_Id, ',') ufn
                              ON ufn.Segments = cism.CU_INVOICE_ID
      
      INSERT      INTO dbo.CU_Invoice_CU_Data_Correction_Batch_Dtl
                  ( 
                   CU_Invoice_CU_Data_Correction_Batch_Id
                  ,From_Account_Id
                  ,To_Account_Id
                  ,Cu_Invoice_Id
                  ,Service_Month
                  ,Action_Cd
                  ,Status_Cd
                  ,Created_Ts
                  ,Last_Change_Ts )
                  SELECT
                        @CU_Invoice_CU_Data_Correction_Batch_Id
                       ,@From_Account_Id
                       ,@To_Account_Id
                       ,ufn.Segments
                       ,ISNULL(ism.Service_Month, @Service_Month)
                       ,@Action_Cd
                       ,@Status_Cd
                       ,GETDATE()
                       ,GETDATE()
                  FROM
                        dbo.ufn_split(@Cu_Invoice_Id, ',') ufn
                        LEFT JOIN @Tbl_Inv_Service_Months ism
                              ON ufn.Segments = ism.Cu_Invoice_Id
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.CU_Invoice_CU_Data_Correction_Batch_Dtl cicdcbd
                                     WHERE
                                          cicdcbd.CU_Invoice_CU_Data_Correction_Batch_Id = @CU_Invoice_CU_Data_Correction_Batch_Id
                                          AND ufn.Segments = cicdcbd.Cu_Invoice_Id
                                          AND cicdcbd.From_Account_Id = @From_Account_Id
                                          AND cicdcbd.To_Account_Id = @To_Account_Id
                                          AND cicdcbd.Service_Month = @Service_Month )
                  GROUP BY
                        ufn.Segments
                       ,ISNULL(ism.Service_Month, @Service_Month)
                        


END






;
GO
GRANT EXECUTE ON  [dbo].[CU_Invoice_CU_Data_Correction_Batch_Dtl_Ins] TO [CBMSApplication]
GO
