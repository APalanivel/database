
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 CBMS.dbo.SR_SAD_GET_RETURN_TO_TARIFF_NOTIFICATION_COUNT_P  
  
DESCRIPTION:  
  
  
INPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 @user_id        varchar(10)              
 @from_week_identifier datetime                
 @to_week_identifier datetime                
  
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
EXEC SR_SAD_GET_RETURN_TO_TARIFF_NOTIFICATION_COUNT_P 51, '06/25/2007','07/01/2009'  
EXEC SR_SAD_GET_RETURN_TO_TARIFF_NOTIFICATION_COUNT_P 18807, '2010-01-01','2012-01-01'    
EXEC SR_SAD_GET_RETURN_TO_TARIFF_NOTIFICATION_COUNT_P 33833, '2007-01-01','2012-01-01'    
  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
    AKR        Ashok Kumar Raju  
      
MODIFICATIONS  
  
 Initials Date  Modification  
------------------------------------------------------------  
          9/21/2010 Modify Quoted Identifier  
 DMR    09/10/2010 Modified for Quoted_Identifier  
 AKR    2012-10-30 Modified the code to get data based on Analyst Mapping code  
 AKR    2012-12-13  Modified the code to Use Core.Client_Hier_Account  
******/  
CREATE PROCEDURE dbo.SR_SAD_GET_RETURN_TO_TARIFF_NOTIFICATION_COUNT_P
      ( 
       @user_id VARCHAR(10)
      ,@from_week_identifier DATETIME
      ,@to_week_identifier DATETIME )
AS 
BEGIN  
  
      SET NOCOUNT ON ;  
  
      DECLARE
            @transport_type_id INT
           ,@na_type_id INT
           ,@EP_commodity_type_id INT
           ,@NG_commodity_type_id INT
           ,@Default_Analyst INT
           ,@Custom_Analyst INT
        
      SELECT
            @EP_commodity_type_id = max(case WHEN com.Commodity_Name = 'Electric Power' THEN com.Commodity_Id
                                        END)
           ,@NG_commodity_type_id = max(case WHEN com.Commodity_Name = 'Natural Gas' THEN com.Commodity_Id
                                        END)
      FROM
            dbo.Commodity com
      WHERE
            com.Commodity_Name IN ( 'Electric Power', 'Natural Gas' )      
                
     
      SELECT
            @Default_Analyst = max(case WHEN c.Code_Value = 'Default' THEN c.Code_Id
                                   END)
           ,@Custom_Analyst = max(case WHEN c.Code_Value = 'Custom' THEN c.Code_Id
                                  END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Analyst Type'     
       
  
      SELECT
            @transport_type_id = entity_id
      FROM
            dbo.entity
      WHERE
            entity_name = 'Remaining ON transport'
            AND entity_type = 1034  
      SELECT
            @na_type_id = entity_id
      FROM
            dbo.entity
      WHERE
            entity_name = 'N/A'
            AND entity_type = 1034 ;
      WITH  cte_Account_List
              AS ( SELECT
                        switch.UTILITY_SWITCH_SUPPLIER_DATE Return_To_Tariff_Date
                       ,utilacc.Account_Id
                       ,DBO.SR_SAD_FN_GET_CONTRACT_ID_OF_ACCOUNT(utilacc.Account_Id, convert(DATETIME, convert(VARCHAR(10), getdate(), 101))) AS contract_id
                       ,coalesce(utilacc.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) Analyst_Mapping_Cd
                       ,rfp.COMMODITY_TYPE_ID Commodity_Id
                       ,utilacc.Account_Vendor_Id Account_Vendor_Id
                       ,ch.Client_Id
                       ,ch.Site_Id
                   FROM
                        dbo.SR_RFP_UTILITY_SWITCH switch
                        JOIN dbo.SR_RFP_ACCOUNT rfp_account
                              ON rfp_account.sr_rfp_account_id = switch.sr_account_group_id
                        JOIN dbo.SR_RFP rfp
                              ON rfp.sr_rfp_id = rfp_account.sr_rfp_id
                        INNER JOIN Core.Client_Hier_Account utilacc
                              ON utilacc.Account_Id = rfp_account.account_id
                        JOIN dbo.VENDOR ven
                              ON utilacc.Account_Vendor_Id = ven.VENDOR_ID
                        JOIN core.client_Hier ch
                              ON ch.Client_Hier_Id = utilacc.Client_Hier_Id
                   WHERE
                        utilacc.Account_Type = 'Utility'
                        AND switch.return_to_tariff_date IS NOT NULL
                        AND switch.return_to_tariff_date BETWEEN @from_week_identifier
                                                         AND     @to_week_identifier
                        AND ( ( switch.return_to_tariff_type_id != @transport_type_id
                                AND switch.return_to_tariff_type_id != @na_type_id )
                              OR switch.return_to_tariff_type_id IS NULL )
                        AND switch.utility_switch_supplier_type_id IS NULL
                        AND switch.change_notice_image_id IS NULL
                        AND switch.SUPPLIER_NOTICE_IMAGE_ID IS NULL
                        AND switch.FLOW_VERIFICATION_IMAGE_ID IS NULL
                        AND switch.is_bid_group = 0
                        AND rfp_account.is_deleted = 0
                        AND rfp_account.bid_status_type_id != 1224
                        AND rfp.COMMODITY_TYPE_ID IN ( @EP_commodity_type_id, @NG_commodity_type_id )
                   GROUP BY
                        switch.UTILITY_SWITCH_SUPPLIER_DATE
                       ,utilacc.Account_id
                       ,utilacc.Account_Analyst_Mapping_Cd
                       ,ch.Site_Analyst_Mapping_Cd
                       ,ch.Client_Analyst_Mapping_Cd
                       ,rfp.COMMODITY_TYPE_ID
                       ,utilacc.Account_Vendor_Id
                       ,ch.Client_Id
                       ,ch.Site_Id),
            cte_Account_User
              AS ( SELECT
                        acc.Return_To_Tariff_Date CONTRACT_END_DATE
                       ,acc.account_id
                       ,acc.contract_id
                       ,acc.Commodity_Id
                       ,case WHEN acc.Analyst_Mapping_Cd = @Default_Analyst THEN vcam.Analyst_Id
                             WHEN acc.Analyst_Mapping_Cd = @Custom_Analyst THEN coalesce(aca.Analyst_User_Info_Id, sca.Analyst_User_Info_Id, cca.Analyst_User_Info_Id)
                        END AS Analyst_Id
                   FROM
                        cte_Account_List acc
                        JOIN dbo.VENDOR_COMMODITY_MAP vcm
                              ON acc.Account_Vendor_Id = vcm.VENDOR_ID
                                 AND acc.Commodity_Id = vcm.COMMODITY_TYPE_ID
                        INNER JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                              ON vcm.VENDOR_COMMODITY_MAP_ID = vcam.Vendor_Commodity_Map_Id
                        INNER JOIN core.Client_Commodity ccc
                              ON ccc.Client_Id = acc.Client_Id
                                 AND ccc.Commodity_Id = acc.COMMODITY_ID
                        LEFT JOIN dbo.Account_Commodity_Analyst aca
                              ON aca.Account_Id = acc.Account_Id
                                 AND aca.Commodity_Id = acc.COMMODITY_ID
                        LEFT JOIN dbo.SITE_Commodity_Analyst sca
                              ON sca.Site_Id = acc.Site_Id
                                 AND sca.Commodity_Id = acc.COMMODITY_ID
                        LEFT JOIN dbo.Client_Commodity_Analyst cca
                              ON ccc.Client_Commodity_Id = cca.Client_Commodity_Id
                        LEFT JOIN dbo.CONTRACT con
                              ON acc.contract_id = con.CONTRACT_ID)
            SELECT
                  isnull(count(DISTINCT cau.account_id), 0) no_of_alerts
                 ,min(cau.CONTRACT_END_DATE) due_date
            FROM
                  cte_Account_User cau
            WHERE
                  cau.Analyst_Id = @user_id  
   
  
END ;
;
GO


GRANT EXECUTE ON  [dbo].[SR_SAD_GET_RETURN_TO_TARIFF_NOTIFICATION_COUNT_P] TO [CBMSApplication]
GO
