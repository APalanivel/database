SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[CU_Invoice_Dtl_Sel_By_Account_Id]  

DESCRIPTION:
	To Get Invoices Details for Selected Account Id.

INPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION 
------------------------------------------------------------
@Account_Id		INT						Utility Account
@StartIndex		INT			1
@EndIndex		INT			2147483647

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------


EXEC CU_Invoice_Dtl_Sel_By_Supplier_Account_Id_Config_Id  1197550,507644, 1,25


AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
NR			Narayana Reddy

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
NR			2020-01-16	MAINT-9734 - Created - Added @Supplier_Account_Config_Id new parameter.
*/

CREATE PROCEDURE [dbo].[CU_Invoice_Dtl_Sel_By_Supplier_Account_Id_Config_Id]
    (
        @Account_Id INT
        , @Supplier_Account_Config_Id INT
        , @StartIndex INT = 1
        , @EndIndex INT = 2147483647
    )
AS
    BEGIN

        SET NOCOUNT ON;

        WITH Cte_Invoice_List
        AS (
               SELECT
                    ci.CU_INVOICE_ID
                    , cuim.Begin_Dt AS BEGIN_DATE
                    , cuim.End_Dt AS END_DATE
                    , cuim.SERVICE_MONTH
                    , ci.IS_PROCESSED
                    , ci.IS_DUPLICATE
                    , ROW_NUMBER() OVER (ORDER BY (CASE WHEN cuim.SERVICE_MONTH IS NULL THEN 1
                                                       ELSE 0
                                                   END)
                                                  , cuim.SERVICE_MONTH) Row_Num
                    , COUNT(1) OVER () Total_Rows
               FROM
                    dbo.CU_INVOICE_SERVICE_MONTH cuim
                    INNER JOIN dbo.CU_INVOICE ci
                        ON cuim.CU_INVOICE_ID = ci.CU_INVOICE_ID
                    INNER JOIN Core.Client_Hier_Account chaa
                        ON chaa.Account_Id = cuim.Account_ID
               WHERE
                    chaa.Account_Id = @Account_Id
                    AND chaa.Supplier_Account_Config_Id = @Supplier_Account_Config_Id
                    AND (   cuim.Begin_Dt BETWEEN chaa.Supplier_Account_begin_Dt
                                          AND     chaa.Supplier_Account_End_Dt
                            OR  cuim.End_Dt BETWEEN chaa.Supplier_Account_begin_Dt
                                            AND     chaa.Supplier_Account_End_Dt
                            OR  chaa.Supplier_Account_begin_Dt BETWEEN cuim.Begin_Dt
                                                               AND     cuim.End_Dt
                            OR  chaa.Supplier_Account_End_Dt BETWEEN cuim.Begin_Dt
                                                             AND     cuim.End_Dt)
               GROUP BY
                   ci.CU_INVOICE_ID
                   , cuim.Begin_Dt
                   , cuim.End_Dt
                   , cuim.SERVICE_MONTH
                   , ci.IS_PROCESSED
                   , ci.IS_DUPLICATE
           )
        SELECT
            CU_INVOICE_ID
            , BEGIN_DATE
            , END_DATE
            , SERVICE_MONTH
            , IS_PROCESSED
            , IS_DUPLICATE
            , Total_Rows
        FROM
            Cte_Invoice_List
        WHERE
            Row_Num BETWEEN @StartIndex
                    AND     @EndIndex;

    END;

GO
GRANT EXECUTE ON  [dbo].[CU_Invoice_Dtl_Sel_By_Supplier_Account_Id_Config_Id] TO [CBMSApplication]
GO
