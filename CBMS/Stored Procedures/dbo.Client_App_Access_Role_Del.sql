SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 /******      
                         
 NAME: dbo.Client_App_Access_Role_Del                     
                          
 DESCRIPTION:      
		  To delete colums from  Client App Access Role   table.                          
                          
 INPUT PARAMETERS:      
                         
 Name                               DataType          Default       Description      
---------------------------------------------------------------------------------------------------------------    
 @Client_App_Access_Role_Id         INT        
                          
 OUTPUT PARAMETERS:      
                               
 Name                               DataType          Default       Description      
---------------------------------------------------------------------------------------------------------------    
                          
 USAGE EXAMPLES:                              
---------------------------------------------------------------------------------------------------------------      
               
	 BEGIN TRAN        
	 SELECT * FROM dbo.Client_App_Access_Role where Client_App_Access_Role_Id=4                 
	 EXEC dbo.Client_App_Access_Role_Del @Client_App_Access_Role_Id=4    
	 SELECT * FROM dbo.Client_App_Access_Role where Client_App_Access_Role_Id=4       
	 ROLLBACK   
			                
                         
 AUTHOR INITIALS:    
       
 Initials                   Name      
---------------------------------------------------------------------------------------------------------------    
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
---------------------------------------------------------------------------------------------------------------    
 NR                     2013-12-30      Created for RA Admin user management.                        
                         
******/   
     
     
 CREATE PROCEDURE dbo.Client_App_Access_Role_Del
      ( 
       @Client_App_Access_Role_Id INT )
 AS 
 BEGIN  

      SET NOCOUNT ON
         
      DECLARE
            @GROUP_INFO_ID INT
           ,@User_Info_Id INT
           ,@Row_Counter INT
           ,@Total_Row_Count INT   
        
      DECLARE @Client_App_Access_Role_Group_Info_Map_List TABLE
            ( 
             Client_App_Access_Role_Id INT
            ,GROUP_INFO_ID INT
            ,PRIMARY KEY CLUSTERED ( Client_App_Access_Role_Id, GROUP_INFO_ID )
            ,Row_Num INT IDENTITY(1, 1) )    
              
                  
      DECLARE @User_Info_Client_App_Access_Role_Map_List TABLE
            ( 
             Client_App_Access_Role_Id INT
            ,User_Info_Id INT
            ,PRIMARY KEY CLUSTERED ( Client_App_Access_Role_Id, User_Info_Id )
            ,Row_Num INT IDENTITY(1, 1) )    
              
        
              
      INSERT      INTO @Client_App_Access_Role_Group_Info_Map_List
                  ( 
                   Client_App_Access_Role_Id
                  ,GROUP_INFO_ID )
                  SELECT
                        Client_App_Access_Role_Id
                       ,GROUP_INFO_ID
                  FROM
                        [dbo].[Client_App_Access_Role_Group_Info_Map]
                  WHERE
                        Client_App_Access_Role_Id = @Client_App_Access_Role_Id   
                          
                          
      INSERT      INTO @User_Info_Client_App_Access_Role_Map_List
                  ( 
                   Client_App_Access_Role_Id
                  ,User_Info_Id )
                  SELECT
                        Client_App_Access_Role_Id
                       ,User_Info_Id
                  FROM
                        [dbo].[User_Info_Client_App_Access_Role_Map]
                  WHERE
                        Client_App_Access_Role_Id = @Client_App_Access_Role_Id  
                          
      BEGIN TRY                  
            BEGIN TRANSACTION                   
                 
            SELECT
                  @Total_Row_Count = MAX(Row_Num)
            FROM
                  @Client_App_Access_Role_Group_Info_Map_List  
                      
            SET @Row_Counter = 1   
        
            WHILE ( @Row_Counter <= @Total_Row_Count ) 
                  BEGIN          
                        SELECT
                              @Client_App_Access_Role_Id = Client_App_Access_Role_Id
                             ,@GROUP_INFO_ID = GROUP_INFO_ID
                        FROM
                              @Client_App_Access_Role_Group_Info_Map_List
                        WHERE
                              Row_Num = @Row_Counter          
          
                        EXEC dbo.Client_App_Access_Role_Group_Info_Map_Del 
                              @Client_App_Access_Role_Id
                             ,@GROUP_INFO_ID  
          
                        SET @Row_Counter = @Row_Counter + 1          
                  END  
                 
            SELECT
                  @Total_Row_Count = MAX(Row_Num)
            FROM
                  @User_Info_Client_App_Access_Role_Map_List  
                      
            SET @Row_Counter = 1   
        
            WHILE ( @Row_Counter <= @Total_Row_Count ) 
                  BEGIN          
                        SELECT
                              @Client_App_Access_Role_Id = Client_App_Access_Role_Id
                             ,@User_Info_Id = User_Info_Id
                        FROM
                              @User_Info_Client_App_Access_Role_Map_List
                        WHERE
                              Row_Num = @Row_Counter          
          
                        EXEC dbo.App_Access_Role_Del_For_User_By_Access_Role 
                              @Client_App_Access_Role_Id
                             ,@User_Info_Id  
          
                        SET @Row_Counter = @Row_Counter + 1          
                  END     
                    
            DELETE FROM
                  dbo.Client_App_Access_Role
            WHERE
                  Client_App_Access_Role_Id = @Client_App_Access_Role_Id      
   
            COMMIT TRANSACTION       
                  
      END TRY              
      BEGIN CATCH          
            IF @@TRANCOUNT > 0 
                  ROLLBACK TRAN    
            EXEC dbo.usp_RethrowError      
                          
      END CATCH  
 END  




 
;
GO
GRANT EXECUTE ON  [dbo].[Client_App_Access_Role_Del] TO [CBMSApplication]
GO
