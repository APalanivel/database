
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_UPDATE_RFP_CLIENT_APPROVAL_SSO_PROJECT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	int       	          	
	@sessionId     	varchar(100)	          	
	@rfp_id        	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

exec SR_RFP_UPDATE_RFP_CLIENT_APPROVAL_SSO_PROJECT_P -1,-1,389

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	      9/21/2010	Modify Quoted Identifier
 DMR	  09/10/2010 Modified for Quoted_Identifier
 AKR      2012-01-21 Changes Summit to Schnider Electric as a part of Name Change Project

******/
CREATE PROCEDURE DBO.SR_RFP_UPDATE_RFP_CLIENT_APPROVAL_SSO_PROJECT_P
      ( 
       @userId INT
      ,@sessionId VARCHAR(100)
      ,@rfp_id INT )
AS 
BEGIN
      SET nocount ON
      DECLARE @temp TABLE ( [site_id] [int] )

      BEGIN
            INSERT      INTO @temp
                        ( 
                         site_id )
                        SELECT DISTINCT
                              account.site_id
                        FROM
                              sr_rfp_sop_client_approval clientapp
                              JOIN sr_rfp_account rfpacc
                                    ON clientapp.sr_account_group_id = rfpacc.sr_rfp_account_id
                                       AND is_bid_group = 0
                                       AND rfpacc.sr_rfp_id = @rfp_id
                                       AND rfpacc.is_deleted = 0
                              JOIN account
                                    ON rfpacc.account_id = account.account_id
                        UNION
                        SELECT DISTINCT
                              account.site_id
                        FROM
                              sr_rfp_sop_client_approval clientapp
                              JOIN sr_rfp_account rfpacc
                                    ON clientapp.sr_account_group_id = rfpacc.sr_rfp_bid_group_id
                                       AND is_bid_group = 1
                                       AND rfpacc.sr_rfp_id = @rfp_id
                                       AND rfpacc.is_deleted = 0
                              JOIN account
                                    ON rfpacc.account_id = account.account_id 

            DECLARE
                  @siteId INT
                 ,@projectId INT
                 ,@user_id INT
            DECLARE
                  @totalaccountcount INT
                 ,@approvedacccount INT
            WHILE ( ( SELECT
                        count(site_id)
                      FROM
                        @temp ) > 0 ) 
                  BEGIN --//while
                        SET @siteId = ( SELECT TOP 1 * FROM @temp )
                        IF ( SELECT
                              count(*)
                             FROM
                              RFP_SITE_PROJECT
                             WHERE
                              RFP_ID = @rfp_id
                              AND SITE_ID = @siteId ) > 0 
                              BEGIN --//if1
                                    DECLARE @totalaccount AS INT
                                    SET @totalaccount = 0

                                    SELECT
                                          @totalaccount = count(sr_rfp_account_id)
                                    FROM
                                          sr_rfp_account
                                         ,account
                                    WHERE
                                          sr_rfp_account.sr_rfp_id = @rfp_id
                                          AND sr_rfp_bid_group_id IS  NULL
                                          AND sr_rfp_account.is_deleted = 0
                                          AND sr_rfp_account.account_id = account.account_id
                                          AND account.site_id = @siteId

                                    SELECT
                                          @totalaccount = @totalaccount + count(DISTINCT sr_rfp_bid_group_id)
                                    FROM
                                          sr_rfp_account
                                         ,account
                                    WHERE
                                          sr_rfp_account.sr_rfp_id = @rfp_id
                                          AND sr_rfp_bid_group_id IS NOT NULL
                                          AND sr_rfp_account.is_deleted = 0
                                          AND sr_rfp_account.account_id = account.account_id
                                          AND account.site_id = @siteId
                                    SET @totalaccountcount = ( SELECT @totalaccount )

		--query to select all CLIENT APPROVED accounts for that rfp id and siteid

			--select @approvedacccount= 
							
                                    DECLARE @account AS INT
                                    SET @account = 0

                                    SELECT
                                          @account = count(DISTINCT sr_rfp_account_id)
                                    FROM
                                          sr_rfp_sop_client_approval
                                         ,sr_rfp_account
                                         ,account
                                    WHERE
                                          sr_rfp_sop_client_approval.sr_account_group_id = sr_rfp_account.sr_rfp_account_id
                                          AND sr_rfp_account.sr_rfp_id = @rfp_id
                                          AND is_bid_group = 0
                                          AND sr_rfp_account.is_deleted = 0
                                          AND sr_rfp_account.account_id = account.account_id
                                          AND account.site_id = @siteId

                                    SELECT
                                          @account = @account + count(DISTINCT sr_rfp_bid_group_id)
                                    FROM
                                          sr_rfp_sop_client_approval
                                         ,sr_rfp_account
                                         ,account
                                    WHERE
                                          sr_rfp_sop_client_approval.sr_account_group_id = sr_rfp_account.sr_rfp_bid_group_id
                                          AND sr_rfp_account.sr_rfp_id = @rfp_id
                                          AND is_bid_group = 1
                                          AND sr_rfp_account.is_deleted = 0
                                          AND sr_rfp_account.account_id = account.account_id
                                          AND account.site_id = @siteId

                                    SELECT
                                          @approvedacccount = ( SELECT @account )
							
                                    SET @projectId = ( SELECT
                                                            SSO_PROJECT_ID
                                                       FROM
                                                            RFP_SITE_PROJECT
                                                       WHERE
                                                            RFP_ID = @rfp_id
                                                            AND SITE_ID = @siteid )
                                    SET @user_id = ( SELECT TOP 1
                                                      user_info_id
                                                     FROM
                                                      client_cem_map
                                                     ,vwsitename
                                                     WHERE
                                                      vwsitename.site_id = @siteid
                                                      AND vwsitename.client_id = client_cem_map.client_id )
                                    IF ( SELECT
                                          count(SSO_PROJECT_ACTIVITY_ID)
                                         FROM
                                          SSO_PROJECT_ACTIVITY
                                         WHERE
                                          SSO_PROJECT_ID = @projectId
                                          AND ACTIVITY_DESCRIPTION = 'Schneider Electric is taking action to implement the approved path forward. Resource Advisor will reflect the actions taken and will categorize any new supporting information, contracts or other documents.' ) = 0 
                                          BEGIN --//if2
                                                INSERT      INTO SSO_PROJECT_ACTIVITY
                                                            ( 
                                                             SSO_PROJECT_ID
                                                            ,CREATED_BY_ID
                                                            ,ACTIVITY_DATE
                                                            ,ACTIVITY_DESCRIPTION )
                                                VALUES
                                                            ( 
                                                             @projectId
                                                            ,@user_id
                                                            ,getdate()
                                                            ,'Schneider Electric is taking action to implement the approved path forward. Resource Advisor will reflect the actions taken and will categorize any new supporting information, contracts or other documents.' )

                                          END --//if2
                                    IF ( SELECT
                                          count(SSO_PROJECT_STEP_ID)
                                         FROM
                                          SSO_PROJECT_STEP
                                         WHERE
                                          SSO_PROJECT_ID = @projectId
                                          AND STEP_NO = 4 ) > 0 
                                          BEGIN --if3
                                                IF ( @totalaccountcount = @approvedacccount ) 
                                                      BEGIN
                                                            UPDATE
                                                                  sso_project_step
                                                            SET   
                                                                  is_active = 0
                                                                 ,is_complete = 1
                                                            WHERE
                                                                  sso_project_id = @projectid
                                                                  AND step_no = 4
                                                      END
                                                IF ( SELECT
                                                      count(SSO_PROJECT_STEP_ID)
                                                     FROM
                                                      SSO_PROJECT_STEP
                                                     WHERE
                                                      SSO_PROJECT_ID = @projectId
                                                      AND STEP_NO = 5 ) = 0 
                                                      BEGIN
                                                            INSERT      INTO SSO_PROJECT_STEP
                                                                        ( 
                                                                         SSO_PROJECT_ID
                                                                        ,STEP_NO
                                                                        ,IS_ACTIVE
                                                                        ,IS_COMPLETE )
                                                            VALUES
                                                                        ( 
                                                                         @projectId
                                                                        ,5
                                                                        ,1
                                                                        ,0 )
				--added for 18499
                                                            UPDATE
                                                                  sso_project
                                                            SET   
                                                                  project_status_type_id = ( SELECT
                                                                                                entity_id
                                                                                             FROM
                                                                                                entity
                                                                                             WHERE
                                                                                                entity_type = 601
                                                                                                AND entity_name = 'Open' )
                                                            WHERE
                                                                  sso_project_id = @projectid 

                                                      END
                                          END
                                    ELSE 
                                          IF ( SELECT
                                                count(SSO_PROJECT_STEP_ID)
                                               FROM
                                                SSO_PROJECT_STEP
                                               WHERE
                                                SSO_PROJECT_ID = @projectId
                                                AND STEP_NO = 4 ) = 0 
                                                BEGIN --//else if
                                                      IF ( SELECT
                                                            count(SSO_PROJECT_ACTIVITY_ID)
                                                           FROM
                                                            SSO_PROJECT_ACTIVITY
                                                           WHERE
                                                            SSO_PROJECT_ID = @projectId
                                                            AND ACTIVITY_DESCRIPTION = 'Schneider Electric has completed the analysis and prepared a recommendation.  Timely review of the recommendation is essential to ensure implementation.' ) = 0 
                                                            BEGIN --//if2
                                                                  INSERT      INTO SSO_PROJECT_ACTIVITY
                                                                              ( 
                                                                               SSO_PROJECT_ID
                                                                              ,CREATED_BY_ID
                                                                              ,ACTIVITY_DATE
                                                                              ,ACTIVITY_DESCRIPTION )
                                                                  VALUES
                                                                              ( 
                                                                               @projectId
                                                                              ,@user_id
                                                                              ,getdate()
                                                                              ,'Schneider Electric has completed the analysis and prepared a recommendation.  Timely review of the recommendation is essential to ensure implementation.' )

                                                            END --//if2

                                                      IF ( @totalaccountcount = @approvedacccount ) 
                                                            BEGIN --//if5
                                                                  INSERT      INTO SSO_PROJECT_STEP
                                                                              ( 
                                                                               SSO_PROJECT_ID
                                                                              ,STEP_NO
                                                                              ,IS_ACTIVE
                                                                              ,IS_COMPLETE )
                                                                  VALUES
                                                                              ( 
                                                                               @projectId
                                                                              ,4
                                                                              ,0
                                                                              ,1 ) 
                                                            END ----//if5
                                                      ELSE 
                                                            BEGIN --else
                                                                  INSERT      INTO SSO_PROJECT_STEP
                                                                              ( 
                                                                               SSO_PROJECT_ID
                                                                              ,STEP_NO
                                                                              ,IS_ACTIVE
                                                                              ,IS_COMPLETE )
                                                                  VALUES
                                                                              ( 
                                                                               @projectId
                                                                              ,4
                                                                              ,1
                                                                              ,0 ) 
				
                                                            END --else
                                                      IF ( SELECT
                                                            count(SSO_PROJECT_STEP_ID)
                                                           FROM
                                                            SSO_PROJECT_STEP
                                                           WHERE
                                                            SSO_PROJECT_ID = @projectId
                                                            AND STEP_NO = 5 ) = 0 
                                                            BEGIN --//if6
                                                                  INSERT      INTO SSO_PROJECT_STEP
                                                                              ( 
                                                                               SSO_PROJECT_ID
                                                                              ,STEP_NO
                                                                              ,IS_ACTIVE
                                                                              ,IS_COMPLETE )
                                                                  VALUES
                                                                              ( 
                                                                               @projectId
                                                                              ,5
                                                                              ,1
                                                                              ,0 )
				--added for 18499
                                                                  UPDATE
                                                                        sso_project
                                                                  SET   
                                                                        project_status_type_id = ( SELECT
                                                                                                      entity_id
                                                                                                   FROM
                                                                                                      entity
                                                                                                   WHERE
                                                                                                      entity_type = 601
                                                                                                      AND entity_name = 'Open' )
                                                                  WHERE
                                                                        sso_project_id = @projectid 

                                                            END --//if6
                                                END --//else if
                        
                              END --//if1

                        DELETE FROM
                              @temp
                        WHERE
                              site_id = @siteId
                  END --//while

      END --//procedure
END

;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_UPDATE_RFP_CLIENT_APPROVAL_SSO_PROJECT_P] TO [CBMSApplication]
GO
