
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:  User_PassCode_Merge_Transfer        
        
DESCRIPTION:        
 Merges changes from [//Change_Control/Service/CBMS/User_Info_Transfer] Service to the User_PassCode table        
        
INPUT PARAMETERS:        
 Name					DataType			Default		Description        
-------------------------------------------------------------------------------------------------------  
 @@Message				XML								XML string of changes to the client heir table        
 ,@Conversation_Handle  UNIQUEIDENTIFER					Conversation Handle that sent the message         
        
OUTPUT PARAMETERS:        
 Name      DataType  Default Description        
------------------------------------------------------------------------------------------------------        
@Message_Batch_Count  INT    Returns Count of source Records       
        
        
USAGE EXAMPLES:        
------------------------------------------------------------------------------------------------------  
  
  This sp should be executed from SQL Server Service Broker 
        
AUTHOR INITIALS:        
 Initials	Name        
------------------------------------------------------------------------------------------------------  
 DSC		Kaushik        


MODIFICATIONS        
        
 Initials	Date			Modification        
------------------------------------------------------------        
 DSC		02/03/2014		Created   
 DMR  10/09/2015 Modified to not execute.  This is in response to production issue.     
******/        
CREATE PROCEDURE [dbo].[User_PassCode_Merge_Transfer]
      ( 
       @Message XML
      ,@Conversation_Handle UNIQUEIDENTIFIER )
AS 
BEGIN        
        
      SET NOCOUNT ON;        
 IF 1=2
 BEGIN              
      DECLARE
            @Idoc INT
           ,@Op_Code CHAR(1)

      EXEC sp_xml_preparedocument 
            @idoc OUTPUT
           ,@Message        
                         

      DECLARE @User_PassCode_Changes TABLE
            ( 
             [User_PassCode_Id] [int] NOT NULL
            ,[User_Info_Id] [int] NULL
            ,[PassCode] [nvarchar](128) NULL
            ,[Active_From_Dt] [date] NULL
            ,[Created_By_Id] [int] NULL
            ,[Last_Change_Ts] [datetime] NULL
            ,[Op_Code] [char](1) )

      INSERT      INTO @User_PassCode_Changes
                  ( 
                   User_PassCode_Id
                  ,User_Info_Id
                  ,PassCode
                  ,Active_From_Dt
                  ,Created_By_Id
                  ,Last_Change_Ts
                  ,Op_Code )
                  SELECT
                        User_PassCode_Id
                       ,User_Info_Id
                       ,PassCode
                       ,Active_From_Dt
                       ,Created_By_Id
                       ,Last_Change_Ts
                       ,Op_Code
                  FROM
                        OPENXML (@idoc, '/User_PassCode_Changes/User_PassCode_Change',2)        
        WITH ( 
  				[User_PassCode_Id] [int],
				[User_Info_Id] [int],
				[PassCode] [nvarchar](128),
				[Active_From_Dt] [date],
				[Created_By_Id] [int],
				[Last_Change_Ts] [datetime],
				[Op_Code] [char](1)
          )   

       
      EXEC sp_xml_removedocument 
            @idoc 

      DECLARE @HoldIdent INT  
  
      SELECT
            @HoldIdent = ident_current('User_PassCode')  
  

      SET IDENTITY_INSERT User_PassCode ON

      INSERT      INTO dbo.User_PassCode
                  ( 
                   User_PassCode_Id
                  ,User_Info_Id
                  ,PassCode
                  ,Active_From_Dt
                  ,Created_By_Id
                  ,Last_Change_Ts )
                  SELECT
                        User_PassCode_Id
                       ,User_Info_Id
                       ,PassCode
                       ,Active_From_Dt
                       ,Created_By_Id
                       ,Last_Change_Ts
                  FROM
                        @User_PassCode_Changes upc
                  WHERE
                        Op_Code = 'I'


      SET IDENTITY_INSERT User_PassCode OFF

      DBCC CHECKIDENT ('User_PassCode', RESEED, @holdident)  WITH NO_INFOMSGS;

      DELETE
            up
      FROM
            dbo.User_PassCode up
            INNER JOIN @User_PassCode_Changes upc
                  ON up.User_PassCode_Id = upc.User_PassCode_Id
      WHERE
            upc.Op_Code = 'D' 					
				
				
END
END
;
GO

GRANT EXECUTE ON  [dbo].[User_PassCode_Merge_Transfer] TO [sb_Execute]
GO
