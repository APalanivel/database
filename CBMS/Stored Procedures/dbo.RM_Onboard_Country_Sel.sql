SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/**
NAME:    
   
	dbo.RM_Onboard_Country_Sel
   
 DESCRIPTION:     
   
	This procedure is to get client onboarded details
   
 INPUT PARAMETERS:    
	Name				DataType		 Default		 Description    
----------------------------------------------------------------------      
	@Client_Id	INT
   
 OUTPUT PARAMETERS:    
 Name				DataType		 Default		 Description    
----------------------------------------------------------------------

  
  USAGE EXAMPLES:    
------------------------------------------------------------  

  
	EXEC RM_Onboard_Country_Sel 
	   
	
  
AUTHOR INITIALS:    
	Initials	Name    
------------------------------------------------------------    
	RR			Raghu Reddy
    
 MODIFICATIONS     
	Initials	Date		Modification    
------------------------------------------------------------    
	RR			01-08-2018	Created For Global Risk Managemnet		


******/

CREATE PROCEDURE [dbo].[RM_Onboard_Country_Sel]
AS 
BEGIN

      SET NOCOUNT ON;

      SELECT
            cho.COUNTRY_ID
           ,con.COUNTRY_NAME
      FROM
            Trade.RM_Client_Hier_Onboard cho
            INNER JOIN dbo.COUNTRY con
                  ON cho.COUNTRY_ID = con.COUNTRY_ID
      GROUP BY
            cho.COUNTRY_ID
           ,con.COUNTRY_NAME
      ORDER BY
            con.COUNTRY_NAME;

END;
GO
GRANT EXECUTE ON  [dbo].[RM_Onboard_Country_Sel] TO [CBMSApplication]
GO
