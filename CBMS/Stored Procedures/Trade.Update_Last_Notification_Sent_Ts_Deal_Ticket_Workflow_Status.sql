SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    Trade.Update_Last_Notification_Sent_Ts_Deal_Ticket_Workflow_Status
   
    
DESCRIPTION:   
   
    
INPUT PARAMETERS:    
      Name          DataType       Default        Description    
-----------------------------------------------------------------------------    
	
     
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
       
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
      
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	PRV          Pramod Reddy V

    
MODIFICATIONS     
	Initials    Date			Modification      
-----------------------------------------------------------------------------       
	PRV          2019-06-26		GRM Proejct.

******/
CREATE PROCEDURE [Trade].[Update_Last_Notification_Sent_Ts_Deal_Ticket_Workflow_Status]
      (
      @Trade_tvp_Trade_Alert_Upd AS Trade.tvp_Trade_Alert_Upd READONLY )
AS
      BEGIN

            SET NOCOUNT ON;

            UPDATE
                  dtchws
            SET
                  dtchws.Last_Notification_Sent_Ts = getdate()
            FROM  Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtchvd
                  INNER JOIN
                  Trade.Deal_Ticket_Client_Hier dtch
                        ON dtch.Client_Hier_Id = dtchvd.Client_Hier_Id
                           AND dtch.Deal_Ticket_Id = dtchvd.Deal_Ticket_Id
                  INNER JOIN
                  Trade.Deal_Ticket_Client_Hier_Workflow_Status dtchws
                        ON dtchws.Deal_Ticket_Client_Hier_Id = dtch.Deal_Ticket_Client_Hier_Id
                  INNER JOIN
                  @Trade_tvp_Trade_Alert_Upd tvp
                        ON tvp.Trade_Id = dtchvd.Trade_Number
                           AND tvp.Status_Id = dtchws.Workflow_Status_Map_Id
            WHERE dtchws.Is_Active = 1
                  AND
                        (     dtchws.Trade_Month IS NULL
                              OR    dtchvd.Deal_Month = dtchws.Trade_Month )
                  AND
                        (     dtchws.Contract_Id IS NULL
                              OR    dtchws.Contract_Id = dtchvd.Contract_Id );

      END;


GO
GRANT EXECUTE ON  [Trade].[Update_Last_Notification_Sent_Ts_Deal_Ticket_Workflow_Status] TO [CBMSApplication]
GO
