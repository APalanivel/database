
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:        
	dbo.cbmsInvoiceParticipationQueue_Save      
       
DESCRIPTION:         
		This SP populates the Detail configuration for Service C and U.      
       
INPUT PARAMETERS:        
	Name				DataType		Default  Description        
------------------------------------------------------------          
	@MyAccountId		int
	@event_type			varchar(200)
	@client_id			int				null
	@division_id		int				null
	@site_id			int				null
	@account_id			int				null
	@service_month		datetime		null
	@suppress_output	bit				1     
      
       
 OUTPUT PARAMETERS:        
 Name   DataType  Default Description        
------------------------------------------------------------        
  
 USAGE EXAMPLES:        
------------------------------------------------------------        

	SELECT TOP 2 * FROM invoice_participation_queue WHERE EVENT_TYPE != 7
	BEGIN TRANSACTION
		EXECUTE dbo.cbmsInvoiceParticipationQueue_Save 49,7,NULL,NULL,NULL,23163,NULL,1
		SELECT * FROM dbo.invoice_participation_queue WHERE ACCOUNT_ID = 23163
	ROLLBACK TRANSACTION

	BEGIN TRANSACTION
		EXECUTE dbo.cbmsInvoiceParticipationQueue_Save 49,7,NULL,NULL,NULL,10954,NULL,1
		SELECT * FROM dbo.invoice_participation_queue WHERE ACCOUNT_ID = 10954
	ROLLBACK TRANSACTION     
      

AUTHOR INITIALS:        
 Initials	Name        
------------------------------------------------------------
 RR		    Raghu Reddy 
       
 MODIFICATIONS         
 Initials	Date		Modification        
------------------------------------------------------------      
 RR			2013-08-13	Added description header
						MAINT-2139 Script block callig the procedure cbmsInvoiceParticipationQueue_Get (if @suppress_output=0) is removed,
						it will never called from this script as value of @suppress_output is always hardcoded as 1 inside the scripts 
						and application wherever dbo.cbmsInvoiceParticipationQueue_Save has reference.
RR			2017-03-02	Updated events list(17-21), 21-New(UpdateSupplierAccount)
******/
CREATE PROCEDURE [dbo].[cbmsInvoiceParticipationQueue_Save]
      ( 
       @MyAccountId INT
      ,@event_type VARCHAR(200)
      ,@client_id INT = NULL
      ,@division_id INT = NULL
      ,@site_id INT = NULL
      ,@account_id INT = NULL
      ,@service_month DATETIME = NULL
      ,@suppress_output BIT = 1 )
AS 
BEGIN

      SET nocount ON

/*
        Enum IpQueueEvent
            InsertUtilityAccount = 1
            InsertSupplierAccount = 2
            MakeClientNotManaged = 3
            MakeDivisionNotManaged = 4
            MakeSiteNotManaged = 5
            MakeSiteClosed = 6
            MakeAccountNotManaged = 7
            MakeAccountNotExpected = 8
            MakeIpExpected = 9
            MakeIpNotExpected = 10
            MakeIpReceived = 11
            MakeIpNotReceived = 12
            MakeIpVarianceUnderReview = 13
            MakeIpVarianceNotUnderReview = 14
            MakeIpRecalcUnderReview = 15
            MakeIpRecalcNotUnderReview = 16
            MakeUtilityAccountManaged = 17
			MakeUtilityAccountExpected = 18
			MakeSupplierAccountManaged = 19
			MakeSupplierAccountExpected = 20
			UpdateSupplierAccount = 21
        End Enum
*/

      DECLARE @this_id INT

      INSERT      INTO dbo.invoice_participation_queue
                  ( 
                   event_type
                  ,client_id
                  ,division_id
                  ,site_id
                  ,account_id
                  ,service_month
                  ,event_by_id
                  ,event_date )
      VALUES
                  ( 
                   @event_type
                  ,@client_id
                  ,@division_id
                  ,@site_id
                  ,@account_id
                  ,@service_month
                  ,@MyAccountId
                  ,GETDATE() )

      SET @this_id = @@IDENTITY
	
END;


;
GO


GRANT EXECUTE ON  [dbo].[cbmsInvoiceParticipationQueue_Save] TO [CBMSApplication]
GO
