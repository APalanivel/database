SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.DELETE_NYMEX_BUDGET_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@budgetId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE       PROCEDURE dbo.DELETE_NYMEX_BUDGET_P
@userId varchar(10),
@sessionId varchar(20),
@budgetId int


as
delete from rm_target_budget where rm_budget_id=@budgetId
	delete from rm_budget_site_map where rm_budget_id=@budgetId
	delete from rm_budget where rm_budget_id=@budgetId
GO
GRANT EXECUTE ON  [dbo].[DELETE_NYMEX_BUDGET_P] TO [CBMSApplication]
GO
