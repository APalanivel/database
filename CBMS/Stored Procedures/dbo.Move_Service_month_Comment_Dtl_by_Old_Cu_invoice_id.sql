SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Move_Service_month_Comment_Dtl_by_Old_Cu_invoice_id

DESCRIPTION:
	This SP is to Copy All Records from below tables of a given cu_invoice_id with New Cu_invoice_id
Tables
	Cu_invoice_service_month
	,cu_invoice_comment
	
INPUT PARAMETERS:
	Name			   DataType		Default	Description
------------------------------------------------------------	     	          	
 @Old_cu_invoice_id 	INT       	          	
 @New_cu_invoice_id 	INT 
 	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Move_Service_month_Comment_Dtl_by_Old_Cu_invoice_id 67,68
	
grantexecute	

AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------
SSR			Sharad Srivastava
	
MODIFICATIONS

Initials	Date		Modification
------------------------------------------------------------
SSR        	04/19/2010	Created
SKA			05/31/2010	Added three columns for insert into CU_INVOICE_SERVICE_MONTH
******/
CREATE PROCEDURE [dbo].[Move_Service_month_Comment_Dtl_by_Old_Cu_invoice_id]
      @Old_cu_invoice_id INT
     ,@New_cu_invoice_id INT
AS 
BEGIN

      SET NOCOUNT ON ;


      DELETE
            CU_INVOICE_SERVICE_MONTH
      WHERE
            CU_INVOICE_ID = @New_cu_invoice_id
		
      INSERT INTO
            CU_INVOICE_SERVICE_MONTH
            (
             CU_INVOICE_ID
            ,Account_ID
            ,Begin_Dt
            ,End_Dt
            ,Billing_Days )
            SELECT
                  @New_cu_invoice_id
                 ,Account_id
                 ,Begin_Dt
                 ,End_Dt
                 ,Billing_Days
            FROM
                  dbo.CU_INVOICE_SERVICE_MONTH
            WHERE
                  CU_INVOICE_ID = @Old_cu_invoice_id
            GROUP BY
                  Account_id
                 ,Begin_Dt
                 ,End_Dt
                 ,Billing_Days

      INSERT INTO
            CU_INVOICE_COMMENT
            (
             CU_INVOICE_ID
            ,COMMENT_BY_ID
            ,COMMENT_DATE
            ,IMAGE_COMMENT )
            SELECT
                  @New_cu_invoice_id
                 ,COMMENT_BY_ID
                 ,COMMENT_DATE
                 ,IMAGE_COMMENT
            FROM
                  dbo.CU_INVOICE_COMMENT
            WHERE
                  CU_INVOICE_ID = @Old_cu_invoice_id		
END
GO
GRANT EXECUTE ON  [dbo].[Move_Service_month_Comment_Dtl_by_Old_Cu_invoice_id] TO [CBMSApplication]
GO
