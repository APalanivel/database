SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:      
 dbo.CU_Invoice_Sel_By_Account_Commodity_Service_Month      
      
DESCRIPTION:      
      
 Used to select the CU Invoice data for the given account, commodity and service month      
      
INPUT PARAMETERS:      
 Name    DataType Default Description      
------------------------------------------------------------      
 @Account_id  Int      
 @Commodity_id  Int   NULL      
 @Service_Month  Date      
      
OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
      
USAGE EXAMPLES:      
------------------------------------------------------------      
      
 EXEC dbo.CU_Invoice_Sel_By_Account_Commodity_Service_Month 36064,NULL,'03/01/2010'      
 EXEC dbo.CU_Invoice_Sel_By_Account_Commodity_Service_Month 10883,291,'2004-07-01'      
       
 EXEC dbo.CU_Invoice_Sel_By_Account_Commodity_Service_Month 994489,290,'2015-11-01'      
 EXEC dbo.CU_Invoice_Sel_By_Account_Commodity_Service_Month 1003016,290,'2015-11-01'      
    
 EXEC dbo.CU_Invoice_Sel_By_Account_Commodity_Service_Month 129300,290,'2012-04-01'      
 EXEC dbo.CU_Invoice_Sel_By_Account_Commodity_Service_Month 116799,291,'2010-11-01'        
      
      
 SELECT *       
 FROM       
  cu_Invoice_Charge c WHERE commodity_type_id = 67      
  AND Exists (SELECT 1 FROM cu_Invoice_Service_Month sm WHERE sm.cu_Invoice_id = c.Cu_invoice_id)      
        
      
 SELECT * FROM Cu_Invoice_Service_Month WHERE CU_Invoice_Id in (69399636 )      
      
835099)      
 SELECT * FROM Cu_Invoice WHERE CU_Invoice_Id in (3788388, 3819023,  3835099)      
     SELECT TOP 1 * FROM  dbo.CU_INVOICE_PROCESS cip  


Exec CU_Invoice_Sel_By_Account_Commodity_Service_Month 1498217,290,'2018-02-01 00:00:00.000' 
 
 select * from account_commodity_invoice_recalc_Type where account_id=129300

 EXEC dbo.CU_Invoice_Sel_By_Account_Commodity_Service_Month 686444,290,'2015-04-01'

 Grantexecute      
AUTHOR INITIALS:      
      
 Initials  Name       
------------------------------------------------------------      
 HG    Harihara Suthan G      
 PNR   Pandarinath       
 SP    Sandeep Pigilam      
 RKV   Ravi kumar vegesna    
 NR    Narayana Reddy    
 SP    Srinivasarao Patchava  
    
MODIFICATIONS      
      
 Initials Date   Modification      
------------------------------------------------------------      
 HG   03/09/2010  Created      
 HG   05/10/2010  Deftaul value(NULL) set for @Commodity_id parameter.      
       (In Invoice checklist page application has to show all the invoices dtls belongs to the given acount and service month irrespective of the commodity)      
       Usage Example updated      
 SKA  05/20/2010  Removed cu_invoice_account_map(Account_id) with cu_invoice_service_month(Account_id)      
 SKA  06/24/2010  Marked @Commodity_id as nullable parameter      
 HG   08/10/2010  As all the commodities may not have determinants modified the script to filter the given commodity invoices based on either Cu_Invoice_Determinant/Cu_Invoice_Charges      
       (Cable, Document Service,Fire Protection are the few commodities which will not have determinants)      
 SP   2014-07-22  Data Operations Enhancement Phase III,Invoice_Type_Cd,Invoice_Type,Is_Level2_Recalc_Validation_Required      
       columns and Case statement for contract_recalc_type added to select list.      
 RKV  2015-09-07  Added Reclac_Type and removed Is_Level2_Recalc_Validation_Required as part of AS400-PII    
 NR   2016-12-23  MAINT-4737 replaced Invoice_Type_Cd with Meter_Read_Type_Cd and Added New Invoice Type.    
 SP   2018-10-17  D20-235 increase the size of the column Contract_Recalc_Type,Meter_Read_Type,Recalc_Type,Invoice_Type of temp tables from 25 to 255  
 SP   2019-09-24  SE2017-457 Added column name Is_Exception_Closed  
 NR	  2019-09-21	Add Contract - Make left join service leve type cd & pull the recalc from account_commdity_recalc table.
 NR	  2019-12-17	MAINT-9675 - Added Contract realc for consolidated accounts.
******/

CREATE PROCEDURE [dbo].[CU_Invoice_Sel_By_Account_Commodity_Service_Month]
    (
        @Account_id INT
        , @Commodity_id INT = NULL
        , @Service_Month DATE
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Supplier_Contract_Id INT
            , @Consolidated_Contract_Id INT;


        SELECT
            @Supplier_Contract_Id = cha.Supplier_Contract_ID
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Account_Type = 'Supplier'
            AND cha.Account_Id = @Account_id
            AND @Service_Month BETWEEN cha.Supplier_Account_begin_Dt
                               AND     cha.Supplier_Account_End_Dt
            AND (   @Commodity_id IS NULL
                    OR  cha.Commodity_Id = @Commodity_id);


        SELECT
            @Consolidated_Contract_Id = c.CONTRACT_ID
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account utility_cha
                ON utility_cha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN Core.Client_Hier_Account Supplier_Cha
                ON Supplier_Cha.Meter_Id = utility_cha.Meter_Id
                   AND  Supplier_Cha.Client_Hier_Id = utility_cha.Client_Hier_Id
            INNER JOIN dbo.CONTRACT c
                ON Supplier_Cha.Supplier_Contract_ID = c.CONTRACT_ID
            INNER JOIN dbo.Account_Consolidated_Billing_Vendor acbv
                ON utility_cha.Account_Id = acbv.Account_Id
                   AND  (   c.CONTRACT_START_DATE BETWEEN acbv.Billing_Start_Dt
                                                  AND     ISNULL(acbv.Billing_End_Dt, '9999-12-31')
                            OR  c.CONTRACT_END_DATE BETWEEN acbv.Billing_Start_Dt
                                                    AND     ISNULL(acbv.Billing_End_Dt, '9999-12-31')
                            OR  acbv.Billing_Start_Dt BETWEEN c.CONTRACT_START_DATE
                                                      AND     c.CONTRACT_END_DATE
                            OR  ISNULL(acbv.Billing_End_Dt, '9999-12-31') BETWEEN c.CONTRACT_START_DATE
                                                                          AND     c.CONTRACT_END_DATE)
        WHERE
            acbv.Account_Id = @Account_id
            AND (   @Commodity_id IS NULL
                    OR  Supplier_Cha.Commodity_Id = @Commodity_id)
            AND utility_cha.Account_Type = 'Utility'
            AND Supplier_Cha.Account_Type = 'Supplier'
            AND @Service_Month BETWEEN c.CONTRACT_START_DATE
                               AND     c.CONTRACT_END_DATE;





        DECLARE @Invoice TABLE
              (
                  Cu_Invoice_id INT
                  , Ubm_Id INT
                  , Cbms_Image_Id INT
                  , Account_Id INT
                  , Service_Month DATETIME
                  , Begin_Date DATETIME
                  , End_Date DATETIME
                  , Billing_Days INT
                  , Is_Reported BIT
                  , Is_Posted BIT
                  , Contract_Recalc_Type_Id INT
                  , Contract_Recalc_Type VARCHAR(255)
                  , Service_Level_Type_Id INT
                  , Service_Level_Type VARCHAR(200)
                  , Meter_Read_Type VARCHAR(255)
                  , Meter_Read_Type_Cd INT
                  , Recalc_Type VARCHAR(255)
                  , Commodity_id INT
                  , Invoice_Type VARCHAR(255)
                  , Invoice_Type_Cd INT
                  , Is_Exception_Closed INT
              );

        INSERT INTO @Invoice
             (
                 Cu_Invoice_id
                 , Ubm_Id
                 , Cbms_Image_Id
                 , Account_Id
                 , Service_Month
                 , Begin_Date
                 , End_Date
                 , Billing_Days
                 , Is_Reported
                 , Is_Posted
                 , Contract_Recalc_Type_Id
                 , Contract_Recalc_Type
                 , Service_Level_Type_Id
                 , Service_Level_Type
                 , Meter_Read_Type
                 , Meter_Read_Type_Cd
                 , Recalc_Type
                 , Commodity_id
                 , Invoice_Type
                 , Invoice_Type_Cd
                 , Is_Exception_Closed
             )
        SELECT
            i.CU_INVOICE_ID
            , i.UBM_ID
            , i.CBMS_IMAGE_ID
            , im.Account_ID
            , im.SERVICE_MONTH
            , i.BEGIN_DATE
            , i.END_DATE
            , COALESCE(i.BILLING_DAYS, im.Billing_Days, DATEDIFF(DAY, i.BEGIN_DATE, i.END_DATE)) AS Billing_Days
            , i.IS_REPORTED
            , i.IS_PROCESSED is_posted
            , a.Supplier_Account_Recalc_Type_Cd contract_recalc_type_id
            , CASE WHEN a.Account_Type = 'Utility' THEN 'Utility Recalc'
                  WHEN a.Account_Type = 'Supplier' THEN ISNULL(cd.Code_Value, rt.Code_Value)
              END AS contract_recalc_type
            , a.Account_Service_level_Cd SERVICE_LEVEL_TYPE_ID
            , ser.ENTITY_NAME service_level_type
            , it.Code_Value AS Meter_Read_Type
            , i.Meter_Read_Type_Cd
            , ISNULL(cd.Code_Value, u_rt.Code_Value) AS Recalc_Type
            , acirt.Commodity_ID
            , ite.Code_Value AS Invoice_Type
            , i.Invoice_Type_Cd
            , CASE WHEN cip.CONTRACT_RECALC = 1
                        AND MIN(CAST(ISNULL(ced.IS_CLOSED, 1) AS INT)) = 1 THEN 1
                  ELSE 0
              END AS Is_Exception_Closed
        FROM
            dbo.CU_INVOICE i
            JOIN dbo.CU_INVOICE_SERVICE_MONTH im
                ON im.CU_INVOICE_ID = i.CU_INVOICE_ID
            JOIN Core.Client_Hier_Account a
                ON a.Account_Id = im.Account_ID
            LEFT JOIN dbo.ENTITY ser
                ON ser.ENTITY_ID = a.Account_Service_level_Cd
            LEFT OUTER JOIN dbo.Account_Commodity_Invoice_Recalc_Type acirt
                ON acirt.Account_Id = a.Account_Id
                   AND  acirt.Commodity_ID = a.Commodity_Id
                   AND  im.SERVICE_MONTH BETWEEN acirt.Start_Dt
                                         AND     ISNULL(acirt.End_Dt, '2099-01-01')
            LEFT OUTER JOIN dbo.Code cd
                ON acirt.Invoice_Recalc_Type_Cd = cd.Code_Id
            LEFT OUTER JOIN dbo.Contract_Recalc_Config crc
                ON crc.Contract_Id = @Supplier_Contract_Id
                   AND  crc.Commodity_Id = a.Commodity_Id
                   AND  crc.Is_Consolidated_Contract_Config = 0
                   AND  im.SERVICE_MONTH BETWEEN crc.Start_Dt
                                         AND     ISNULL(crc.End_Dt, '9999-12-31')
            LEFT OUTER JOIN dbo.Code rt
                ON rt.Code_Id = crc.Supplier_Recalc_Type_Cd
            LEFT OUTER JOIN dbo.Contract_Recalc_Config u_crc
                ON u_crc.Contract_Id = @Consolidated_Contract_Id
                   AND  u_crc.Commodity_Id = a.Commodity_Id
                   AND  u_crc.Is_Consolidated_Contract_Config = 1
                   AND  im.SERVICE_MONTH BETWEEN u_crc.Start_Dt
                                         AND     ISNULL(u_crc.End_Dt, '9999-12-31')
            LEFT OUTER JOIN dbo.Code u_rt
                ON u_rt.Code_Id = u_crc.Supplier_Recalc_Type_Cd
            LEFT OUTER JOIN dbo.Code it
                ON it.Code_Id = i.Meter_Read_Type_Cd
            LEFT OUTER JOIN dbo.Code ite
                ON ite.Code_Id = i.Invoice_Type_Cd
            LEFT JOIN(dbo.CU_EXCEPTION ce
                      INNER JOIN dbo.CU_EXCEPTION_DETAIL ced
                          ON ce.CU_EXCEPTION_ID = ced.CU_EXCEPTION_ID)
                ON ce.CU_INVOICE_ID = i.CU_INVOICE_ID
            LEFT JOIN dbo.CU_INVOICE_PROCESS cip
                ON cip.CU_INVOICE_ID = i.CU_INVOICE_ID
        WHERE
            im.Account_ID = @Account_id
            AND im.SERVICE_MONTH = @Service_Month
            AND i.IS_DEFAULT = 1
            AND (   @Commodity_id IS NULL
                    OR  a.Commodity_Id = @Commodity_id)
        GROUP BY
            i.CU_INVOICE_ID
            , i.UBM_ID
            , i.CBMS_IMAGE_ID
            , im.Account_ID
            , im.SERVICE_MONTH
            , i.BEGIN_DATE
            , i.END_DATE
            , COALESCE(i.BILLING_DAYS, im.Billing_Days, DATEDIFF(DAY, i.BEGIN_DATE, i.END_DATE))
            , i.IS_REPORTED
            , i.IS_PROCESSED
            , a.Supplier_Account_Recalc_Type_Cd
            , rt.Code_Value
            , a.Account_Service_level_Cd
            , ser.ENTITY_NAME
            , it.Code_Value
            , i.Meter_Read_Type_Cd
            , ISNULL(cd.Code_Value, rt.Code_Value)
            , ISNULL(cd.Code_Value, u_rt.Code_Value)
            , a.Account_Type
            , acirt.Commodity_ID
            , ite.Code_Value
            , i.Invoice_Type_Cd
            , cip.CONTRACT_RECALC;

        WITH Cte_Determinant_Charge_Invoice
        AS (
               SELECT
                    inv.Cu_Invoice_id
               FROM
                    dbo.CU_INVOICE_DETERMINANT det
                    JOIN @Invoice inv
                        ON det.CU_INVOICE_ID = inv.Cu_Invoice_id
               WHERE
                    @Commodity_id IS NULL
                    OR  det.COMMODITY_TYPE_ID = @Commodity_id
               UNION
               SELECT
                    inv.Cu_Invoice_id
               FROM
                    dbo.CU_INVOICE_CHARGE chg
                    JOIN @Invoice inv
                        ON chg.CU_INVOICE_ID = inv.Cu_Invoice_id
               WHERE
                    @Commodity_id IS NULL
                    OR  chg.COMMODITY_TYPE_ID = @Commodity_id
           )
        SELECT
            inv.Cu_Invoice_id
            , inv.Ubm_Id
            , inv.Cbms_Image_Id
            , inv.Account_Id
            , inv.Service_Month
            , inv.Begin_Date
            , inv.End_Date
            , inv.Billing_Days
            , inv.Is_Reported
            , NULL Is_Closed
            , inv.Is_Posted
            , inv.Contract_Recalc_Type_Id
            , inv.Contract_Recalc_Type
            , inv.Service_Level_Type_Id
            , inv.Service_Level_Type
            , Meter_Read_Type
            , inv.Meter_Read_Type_Cd
            , inv.Recalc_Type
            , inv.Commodity_id
            , inv.Invoice_Type
            , inv.Invoice_Type_Cd
            , inv.Is_Exception_Closed
        FROM
            @Invoice inv
            JOIN Cte_Determinant_Charge_Invoice Chg_Det_Inv
                ON Chg_Det_Inv.Cu_Invoice_id = inv.Cu_Invoice_id
        ORDER BY
            inv.Service_Month;

    END;

    ;


    ;






GO



GRANT EXECUTE ON  [dbo].[CU_Invoice_Sel_By_Account_Commodity_Service_Month] TO [CBMSApplication]
GO
