SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_UPDATE_STATUS_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@rfpId int 
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_UPDATE_STATUS_P
@rfpId int 
as 
	
set nocount on
declare @entityId int

select @entityId = (select ENTITY_ID from entity where entity_type = 1004 and  ENTITY_NAME = 'Closed')

UPDATE 
	sr_rfp 
SET
	RFP_STATUS_TYPE_ID = @entityId 
WHERE 
	sr_rfp_id =@rfpId
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_UPDATE_STATUS_P] TO [CBMSApplication]
GO
