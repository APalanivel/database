SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                 
/******                        
 NAME: dbo.RM_Onboard_Sites_Sel_By_RM_Group_Country            
                        
 DESCRIPTION:                        
			To Get RM On Board Sites based on RM Group
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@groupId					INT
@Country_Name			VARCHAR(200)
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 
   	exec RM_Onboard_Sites_Sel_By_RM_Group_Country @groupId=191,@Country_Name='USA'


                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2014-11-07       Created                
                       
******/
CREATE PROCEDURE [dbo].[RM_Onboard_Sites_Sel_By_RM_Group_Country]
      ( 
       @groupId INT
      ,@Country_Name VARCHAR(200) = NULL )
AS 
BEGIN
      SET NOCOUNT ON
      SELECT
            ch.SITE_ID
           ,RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' SITE_NAME
           ,ch.SITE_REFERENCE_NUMBER
      FROM
            core.Client_Hier ch
            INNER JOIN dbo.RM_ONBOARD_HEDGE_SETUP hedge
                  ON hedge.SITE_ID = ch.SITE_ID
            INNER JOIN dbo.RM_GROUP_SITE_MAP groupMap
                  ON groupMap.SITE_ID = ch.SITE_ID
      WHERE
            groupMap.RM_GROUP_ID = @groupId
            AND ( @Country_Name IS NULL
                  OR ch.Country_Name = @Country_Name )
      ORDER BY
            RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' 
END

;
GO
GRANT EXECUTE ON  [dbo].[RM_Onboard_Sites_Sel_By_RM_Group_Country] TO [CBMSApplication]
GO
