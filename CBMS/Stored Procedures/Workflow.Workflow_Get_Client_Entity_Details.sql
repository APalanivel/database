SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
       
/******                  
NAME:    [Workflow].[Workflow_Get_Client_Entity_Details]               
DESCRIPTION: It'll Return the Client Entity details based on the Clinet IDs.           
------------------------------------------------------------                 
 INPUT PARAMETERS:                  
 Name   DataType  Default Description                  
  @CLIENT_ID INT            
------------------------------------------------------------                  
 OUTPUT PARAMETERS:                  
 Name   DataType  Default Description                  
------------------------------------------------------------                  
 USAGE EXAMPLES:      
             
 EXEC [Workflow].[Workflow_Get_Client_Entity_Details] @CLIENT_ID = 10069             
 EXEC [Workflow].[Workflow_Get_Client_Entity_Details] @CLIENT_ID = 235            
 EXEC [Workflow].[Workflow_Get_Client_Entity_Details] @CLIENT_ID = 10003            
 EXEC [Workflow].[Workflow_Get_Client_Entity_Details] @CLIENT_ID = 11278          
------------------------------------------------------------                  
AUTHOR INITIALS:                  
Initials Name                  
------------------------------------------------------------                  
TRK RAMAKRISHNA THUMMALA Summit Energy               
               
 MODIFICATIONS                   
 Initials Date   Modification                  
------------------------------------------------------------                 
 TRK    10-OCT-2019 Created         
 CPK  01-Nov - 2019 add Updated time      
 CPK  14-Nov - 2019 changed Client_Attribute and user_info join condition by updated_user_id   
 TRK  28-01-2019 Updated the script, remove the multiple Spaces.  
******/            
      
CREATE PROCEDURE [Workflow].[Workflow_Get_Client_Entity_Details]            
(            
@CLIENT_ID INT            
)            
            
AS            
BEGIN            
            
DECLARE @CLIENT_TABLE TABLE            
(            
 CLIENT_ID INT            
   ,CLIENT_TYPE_ID INT             
   ,CLIENT_TYPE_Name NVARCHAR(255)             
   ,UBM_SERVICE_ID  INT            
   ,UBM_SERVICE_Name NVARCHAR(255)            
   ,REPORT_FREQUENCY_TYPE_ID  INT            
   ,REPORT_FREQUENCY_TYPE_Name NVARCHAR(255)            
   ,CLIENT_NAME  NVARCHAR(255)            
   ,FISCALYEAR_STARTMONTH_TYPE_ID  INT            
   ,FISCALYEAR_STARTMONTH_TYPE_Name NVARCHAR(255)            
   ,UBM_START_DATE  DATETIME            
   ,DSM_STRATEGY BIT             
   ,IS_SEP_ISSUED  BIT            
   ,SEP_ISSUE_DATE  DATETIME            
   ,Not_Managed  BIT            
   ,TRIGGER_RIGHTS  BIT            
   ,RISK_PROFILE_TYPE_ID  INT            
   ,RISK_PROFILE_TYPE_Name NVARCHAR(255)            
   ,Analyst_Mapping_Cd INT             
   ,Analyst_Mapping_Name NVARCHAR(255)            
   ,Variance_Exception_Notice_Contact_Email  NVARCHAR(MAX)            
   ,Client_Hier_Id  INT            
   ,Client_Hier_Name NVARCHAR(255)            
            
)            
            
SELECT DISTINCT            
   C.CLIENT_ID            
           ,C.CLIENT_TYPE_ID              
           ,ISNULL(C.UBM_SERVICE_ID, 0) AS UBM_SERVICE_ID             
           ,C.REPORT_FREQUENCY_TYPE_ID              
           ,C.CLIENT_NAME              
           ,C.FISCALYEAR_STARTMONTH_TYPE_ID              
           ,C.UBM_START_DATE              
           ,C.DSM_STRATEGY              
           ,C.IS_SEP_ISSUED              
           ,C.SEP_ISSUE_DATE              
           ,C.Not_Managed              
           ,division.TRIGGER_RIGHTS              
           ,C.RISK_PROFILE_TYPE_ID              
           ,C.Analyst_Mapping_Cd              
           ,C.Variance_Exception_Notice_Contact_Email              
           ,ch.Client_Hier_Id              
      INTO #CLIENT_TABLE FROM              
            dbo.CLIENT C              
            INNER JOIN dbo.DIVISION division ON division.client_id = C.CLIENT_ID              
            INNER JOIN Core.Client_Hier ch   ON C.CLIENT_ID = ch.CLIENT_ID              
      WHERE              
            C.CLIENT_ID = @CLIENT_ID              
            AND ch.Sitegroup_Id = 0              
            AND ch.Site_Id = 0             
               
   INSERT INTO @CLIENT_TABLE               
   SELECT          
     CT.CLIENT_ID            
    ,CT.CLIENT_TYPE_ID             
    ,E.ENTITY_NAME  AS CLIENT_TYPE_NAME            
    ,CT.UBM_SERVICE_ID            
    ,E2.UBM_SERVICE_NAME AS UBM_SERVICE_NAME            
    ,CT.REPORT_FREQUENCY_TYPE_ID            
    ,E3.ENTITY_NAME AS REPORT_FREQUENCY_TYPE_NAME          
    ,CT.CLIENT_NAME            
    ,CT.FISCALYEAR_STARTMONTH_TYPE_ID            
    ,E4.ENTITY_NAME AS FISCALYEAR_STARTMONTH_TYPE_NAME            
    ,CT.UBM_START_DATE              
    ,CT.DSM_STRATEGY              
    ,CT.IS_SEP_ISSUED              
    ,CT.SEP_ISSUE_DATE              
    ,CT.Not_Managed              
    ,CT.TRIGGER_RIGHTS            
    ,CT.RISK_PROFILE_TYPE_ID            
    ,E5.ENTITY_NAME AS RISK_PROFILE_TYPE_NAME            
    ,CT.Analyst_Mapping_Cd            
    ,C.Code_Value AS Analyst_Mapping_Name            
    ,CT.Variance_Exception_Notice_Contact_Email            
    ,CT.Client_Hier_Id            
    ,CT.CLIENT_NAME AS Client_Hier_Name            
            
   FROM #CLIENT_TABLE CT            
   INNER JOIN DBO.ENTITY E  ON E.ENTITY_ID =CT.CLIENT_TYPE_ID            
   INNER JOIN dbo.UBM_SERVICE E2 ON E2.UBM_SERVICE_ID=CT.UBM_SERVICE_ID            
   INNER JOIN DBO.ENTITY E3 ON E3.ENTITY_ID=CT.REPORT_FREQUENCY_TYPE_ID            
   INNER JOIN DBO.ENTITY E4 ON E4.ENTITY_ID=CT.FISCALYEAR_STARTMONTH_TYPE_ID            
   INNER JOIN DBO.ENTITY E5 ON E5.ENTITY_ID=CT.RISK_PROFILE_TYPE_ID            
   INNER JOIN DBO.Code C ON C.Code_Id=CT.Analyst_Mapping_Cd            
              
 /*****************  CEM Client details   *******************/             
  DECLARE @Client_User_CEM TABLE             
  (            
  CLIENT_ID INT,            
  USER_NAME NVARCHAR(4000),            
  USER_INFO_ID INT            
  )            
  INSERT INTO @Client_User_CEM            
  SELECT             
  CCM.CLIENT_ID,            
  U.FIRST_NAME +' '+ U.LAST_NAME AS USER_NAME,            
  U.USER_INFO_ID            
  FROM dbo.CLIENT_CEM_MAP CCM           
  INNER JOIN dbo.USER_INFO U ON U.USER_INFO_ID = CCM.USER_INFO_ID            
  WHERE CCM.CLIENT_ID=@CLIENT_ID AND   U.IS_HISTORY = 0              
            
  SELECT CLIENT_ID,STUFF((            
   SELECT ', '+ CAST(USER_NAME AS NVARCHAR(255))             
   FROM @Client_User_CEM b            
   WHERE a.CLIENT_ID= b.CLIENT_ID            
   FOR XML PATH('')            
   ),1,1,'') AS [USER_NAME]            
  INTO #Client_User_CEM FROM @Client_User_CEM a            
  GROUP BY a.CLIENT_ID            
              
/*****************  CSA Client details   *******************/             
 DECLARE @Client_User_CSA TABLE             
 (            
 CLIENT_ID INT,            
 USER_NAME NVARCHAR(4000),            
 USER_INFO_ID INT            
 )            
 INSERT INTO @Client_User_CSA            
 SELECT             
  CCM.CLIENT_ID,            
  U.FIRST_NAME +' '+ U.LAST_NAME AS USER_NAME,            
  U.USER_INFO_ID              
  FROM dbo.CLIENT_CSA_MAP CCM              
   INNER JOIN dbo.USER_INFO U ON U.USER_INFO_ID = CCM.USER_INFO_ID              
  WHERE U.Is_History = 0              
   AND CCM.CLIENT_ID = @CLIENT_ID              
            
   SELECT CLIENT_ID,STUFF((            
  SELECT ', '+ CAST(USER_NAME AS NVARCHAR(255))             
  FROM @Client_User_CSA b            
  WHERE a.CLIENT_ID= b.CLIENT_ID            
  FOR XML PATH('')            
  ),1,1,'') AS USER_NAME            
 INTO #Client_User_CSA FROM @Client_User_CSA a            
  GROUP BY a.CLIENT_ID            
/*****************  CEA Client details   *******************/             
            
 DECLARE @Client_User_CEA TABLE             
 (            
 CLIENT_ID INT,            
 USER_NAME NVARCHAR(4000),            
 USER_INFO_ID INT            
 )              INSERT INTO @Client_User_CEA            
  SELECT             
  CEM.CLIENT_ID,            
  U.FIRST_NAME +' '+ U.LAST_NAME AS USER_NAME,            
  U.USER_INFO_ID            
  FROM dbo.CLIENT_CEA_MAP CEM              
   INNER JOIN dbo.USER_INFO U ON U.USER_INFO_ID = CEM.USER_INFO_ID              
  WHERE U.is_history = 0              
   AND CEM.CLIENT_ID = @CLIENT_ID             
            
   SELECT CLIENT_ID,STUFF((            
  SELECT ', '+ CAST(USER_NAME AS NVARCHAR(255))             
  FROM @Client_User_CEA b            
  WHERE a.CLIENT_ID= b.CLIENT_ID            
  FOR XML PATH('')            
  ),1,1,'') AS USER_NAME            
 INTO #Client_User_CEA FROM @Client_User_CEA a            
 GROUP BY a.CLIENT_ID            
            
            
            
/*****************  Final Output  *******************/               
   SELECT TOP 1            
     C.CLIENT_TYPE_NAME            
    ,C.UBM_SERVICE_NAME            
    ,C.REPORT_FREQUENCY_TYPE_NAME            
    ,C.CLIENT_NAME            
    ,C.FISCALYEAR_STARTMONTH_TYPE_NAME            
    ,C.UBM_START_DATE            
    ,C.DSM_STRATEGY            
    ,C.IS_SEP_ISSUED            
    ,C.SEP_ISSUE_DATE            
    ,(CASE WHEN Not_Managed =1 THEN cast(0 as bit)
		 WHEN Not_Managed =0 THEN cast(1 as bit) 
	 END ) AS Not_Managed         
    ,C.TRIGGER_RIGHTS            
    ,C.RISK_PROFILE_TYPE_NAME            
    ,C.Analyst_Mapping_Name            
    ,C.Variance_Exception_Notice_Contact_Email            
    ,C.Client_Hier_Name            
    ,CUE.USER_NAME AS [CEM(s)]            
    ,CUC.USER_NAME AS [CSA(s)]           
    ,CEA.USER_NAME AS [CEA(s)]            
    ,U.UBM_NAME AS [Utility Bill Manager]           
    ,cast(isnull(Mark_Invoice_Exception_As_Priority,0) AS BIT) AS Is_Dynamic_Priority         
    ,UI.FIRST_NAME + ' '+LAST_NAME AS [USER_NAME]           
    ,CA.Created_Ts        
 ,CA.Updated_Ts        
   FROM @CLIENT_TABLE C            
   INNER JOIN dbo.UBM_SERVICE US ON US.UBM_SERVICE_ID=C.UBM_SERVICE_ID            
   INNER JOIN dbo.UBM U ON U.UBM_ID=US.UBM_ID            
   LEFT JOIN #Client_User_CEM CUE ON CUE.CLIENT_ID=C.CLIENT_ID            
   LEFT JOIN #Client_User_CSA CUC ON CUC.CLIENT_ID=C.CLIENT_ID            
   LEFT JOIN #Client_User_CEA CEA ON CEA.CLIENT_ID=C.CLIENT_ID            
   LEFT JOIN dbo.Client_Attribute CA ON CA.CLIENT_ID=C.CLIENT_ID           
   LEFT JOIN dbo.USER_INFO UI ON UI.USER_INFO_ID=CA.Updated_User_Id        
   ORDER BY C.TRIGGER_RIGHTS DESC          
          
          
   DROP TABLE #CLIENT_TABLE            
   DROP TABLE #Client_User_CEM          
   DROP TABLE #Client_User_CSA          
   DROP TABLE #Client_User_CEA            
            
END 
GO
GRANT EXECUTE ON  [Workflow].[Workflow_Get_Client_Entity_Details] TO [CBMSApplication]
GO
