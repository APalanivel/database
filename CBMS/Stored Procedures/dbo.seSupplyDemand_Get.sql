SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure dbo.seSupplyDemand_Get
	(
		@SupplyDemandId int
	)
As
BEGIN
	set nocount on
	 select SupplyDemandId
				, DataTypeId
				, ReportYear
				, ReportMonth
				, DryProduction
				, NetImports
				, SupplementalProduction
				, StorageWithdrawal
				, PlantFuel
				, Vehicles
				, Industrial
				, Commercial
				, Residential
				, Transportation
				, PowerGeneration
				, UnaccountedFor
		 from seSupplyDemand
		where SupplyDemandId = @SupplyDemandId
END
GO
GRANT EXECUTE ON  [dbo].[seSupplyDemand_Get] TO [CBMSApplication]
GO
