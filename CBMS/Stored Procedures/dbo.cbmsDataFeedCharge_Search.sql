SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  /******                                                                  
          
 NAME:  dbo.[cbmsDataFeedCharge_Search]    
                                                                  
 DESCRIPTION:                                                                  
   search result to show data in grid    
       
  Unit test:    
  --exec [cbmsDataFeedCharge_Search] null,null,1,1,25, 'UBM_BUCKET_CODE', 1,'Commodity_Name','Electric Power' ,@Total_Count = @o                  
                                                                 
 AUTHOR INITIALS:                                                  
                                                 
 Initials              Name                                                  
---------------------------------------------------------------------------------------------------------------                                                                
 PK                     Prasan kumar               
                                                                   
 MODIFICATIONS:                                                
                                                    
 Initials   Date     Modification                                                
------------  ----------    -----------------------------------------------------------------------------------------                                                
     [cbmsDataFeedCharge_Search]      @missing_mapping =1  
                                           
******/                
CREATE PROCEDURE [dbo].[cbmsDataFeedCharge_Search]                                 
 (                                   
 @umb_ids varchar(500) =null,                                  
 @commodity_ids varchar(500) =null,                                  
 @missing_mapping bit  = 0    ,              
 @Start_Index INT = 1,                    
 @End_Index INT = 500,               
 @sort_column  varchar(500) = null,            
 @sort_order BIT = 1,            
 @filter_column  varchar(500) = null,            
 @filter_value  varchar(500) = null,             
 @Total_Count INT = 0    output          
 )                                    
AS                                    
BEGIN                
                                 
                
  BEGIN TRY             
             
   ;WITH CTE_RowNum                    
   AS                    
   (                                
     SELECT u.UBM_NAME,                          
     com.Commodity_Name,                            
     buckets.UBM_ID                          
       ,buckets.COMMODITY_TYPE_ID                          
       ,buckets.UBM_BUCKET_CODE                          
       ,buckets.Bucket_Name                          
       ,buckets.Bucket_Master_Id                          
       ,buckets.Missing_Count                         
  ,buckets.CU_INVOICE_ID  
  ,buckets.Nav_Path  
  ,ui.FIRST_NAME + ' '+ ui.LAST_NAME AS UserName  
  ,buckets.UpdatedOn FROM (                          
       SELECT                      
       ci.UBM_ID                          
       ,cic.COMMODITY_TYPE_ID                          
       ,UBM_BUCKET_CODE                          
       ,null AS Bucket_Name                          
       ,null AS Bucket_Master_Id                          
       ,count(*) AS Missing_Count                         
  ,min(cic.CU_INVOICE_ID) AS CU_INVOICE_ID                        
  ,'/ip2/cuexception/detail.aspx?CuInvoiceId={invoiceId}&xid={xid}&uid={uid}&from=datafeed' AS Nav_Path   
  ,'' AS UpdatedBy  
  ,null AS UpdatedOn                       
      FROM                          
          dbo.CU_INVOICE_CHARGE cic                          
          INNER JOIN dbo.CU_INVOICE ci                          
              ON ci.CU_INVOICE_ID = cic.CU_INVOICE_ID                             
      WHERE                                
          EXISTS ( SELECT 1 FROM CU_EXCEPTION_DENORM ced                           
             WHERE EXCEPTION_TYPE = 'Mapping Required - Charge Bucket Not Mapped'                          
AND ced.CU_INVOICE_ID = ci.CU_INVOICE_ID                          
           )                          
       AND cic.Bucket_Master_Id IS NULL                            
       AND (@umb_ids is null or ci.ubm_id in (select Segments from  [ufn_split] ( @umb_ids,','))   )                              
       AND (@commodity_ids is null or cic.COMMODITY_TYPE_ID in (select Segments from  [ufn_split] ( @commodity_ids,','))  )                           
                               
      GROUP BY                          
       ci.ubm_id,                          
       cic.COMMODITY_TYPE_ID,            
       UBM_BUCKET_CODE                             
                             
      UNION ALL                          
                 
      SELECT                            
        ubc.UBM_ID                          
        ,ubc.COMMODITY_TYPE_ID                            
        ,ubc.UBM_BUCKET_CODE                          
        ,bm.Bucket_Name                              
        ,bm.Bucket_Master_Id                          
        ,null as Missing_Count                        
  ,null AS CU_INVOICE_ID                        
  ,null AS Nav_Path  
  ,Updated_User_Id AS UpdatedBy  
  ,Last_Change_Ts  AS UpdatedOn                             
       FROM UBM_BUCKET_CHARGE_MAP ubc                           
        INNER JOIN bucket_master bm                           
       ON ubc.Bucket_Master_Id = bm.Bucket_Master_Id                                  
       WHERE (@umb_ids is null or ubc.ubm_id in (select Segments from  [ufn_split] ( @umb_ids,','))   )                              
       AND (@commodity_ids is null or ubc.COMMODITY_TYPE_ID in (select Segments from  [ufn_split] ( @commodity_ids,','))  )                        
    AND  @missing_mapping = 0                    
                                  
     ) buckets                          
    INNER JOIN dbo.UBM u                          
     ON u.UBM_ID = buckets.UBM_ID                          
    INNER JOIN dbo.COMMODITY com                          
     ON com.COMMODITY_ID = buckets.COMMODITY_TYPE_ID   
	LEFT JOIN dbo.USER_INFO ui                          
     ON buckets.UpdatedBy = ui.USER_INFO_ID                  
   )             
            
   SELECT * FROM CTE_RowNum              
         ORDER BY  UBM_Name,COMMODITY_NAME,UBM_BUCKET_CODE      
             
  END TRY                              
        BEGIN CATCH                              
                              
            DECLARE                              
                @Error_Line INT                              
                , @Error_Message VARCHAR(3000);                              
                                                           
            SELECT  @Error_Line = ERROR_LINE(), @Error_Message = ERROR_MESSAGE();                              
                              
            INSERT INTO [dbo].[StoredProc_Error_Log]                              
                 (                              
                     [StoredProc_Name]                              
                     , [Error_Line]                              
                     , [Error_message]                              
                     , [Input_Params]                              
                 )                              
            VALUES                             
                ('[cbmsDataFeedCharge_Search]'                              
                 , @Error_Line                              
                 , @Error_Message                              
                 , '');             
                 
   IF OBJECT_ID('tempdb.dbo.#Result', 'U') IS NOT NULL              
    DROP TABLE #Result;             
   IF OBJECT_ID('tempdb.dbo.#ResultFilter', 'U') IS NOT NULL              
    DROP TABLE #ResultFilter;             
   IF OBJECT_ID('tempdb.dbo.#ResultSorting', 'U') IS NOT NULL              
    DROP TABLE #ResultSorting;                                
                              
           EXEC [dbo].[usp_RethrowError] @Error_Message;                              
        END CATCH;               
                    
                     
                                  
END 
GO
GRANT EXECUTE ON  [dbo].[cbmsDataFeedCharge_Search] TO [CBMSApplication]
GO
