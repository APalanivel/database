SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.Get_WeeklyClosedExceptions_Report_P
AS
BEGIN

	SET NOCOUNT ON

	SELECT exception_type
		, 'Amanda Meredith' = ISNULL(SUM(CASE WHEN queue_name = 'Amanda Meredith' THEN InvoiceCount ELSE 0 END), 0)
		, 'Amber Hensley' = ISNULL(SUM(CASE WHEN queue_name = 'Amber Hensley' THEN InvoiceCount ELSE 0 END), 0)
		, 'Catherine James' = ISNULL(SUM(CASE WHEN queue_name = 'Catherine James' THEN InvoiceCount ELSE 0 END), 0)
		, 'Hillary Stevenson' = ISNULL(SUM(CASE WHEN queue_name = 'Hillary Stevenson' THEN InvoiceCount ELSE 0 END), 0)
		, 'James Ede' = ISNULL(SUM(CASE WHEN queue_name = 'James Ede' THEN InvoiceCount ELSE 0 END), 0)
		, 'Jordan Martin' = ISNULL(SUM(CASE WHEN queue_name = 'Jordan Martin' THEN InvoiceCount ELSE 0 END), 0)
		, 'Micah Nave' = ISNULL(SUM(CASE WHEN queue_name = 'Micah Nave' THEN InvoiceCount ELSE 0 END), 0)
		, 'Sergio Victor' = ISNULL(SUM(CASE WHEN queue_name = 'Sergio Victor' THEN InvoiceCount ELSE 0 END), 0)
		, 'Rajitha Sakani' = ISNULL(SUM(CASE WHEN queue_name = 'Rajitha Sakani' THEN InvoiceCount ELSE 0 END), 0)
		, 'Ram Kumar Gopisetti' = ISNULL(SUM(CASE WHEN queue_name = 'Ram Kumar Gopisetti' THEN InvoiceCount ELSE 0 END), 0)
		, 'Nageshwara Ankolu' = ISNULL(SUM(CASE WHEN queue_name = 'Nageshwara Ankolu' THEN InvoiceCount ELSE 0 END), 0)
		, 'Rashmi Khandelwal' = ISNULL(SUM(CASE WHEN queue_name = 'Rashmi Khandelwal' THEN InvoiceCount ELSE 0 END), 0)
		, 'AnAND Asthana' = ISNULL(SUM(CASE WHEN queue_name = 'AnAND Asthana' THEN InvoiceCount ELSE 0 END), 0)
		, 'Ritesh Agarwal' = ISNULL(SUM(CASE WHEN queue_name = 'Ritesh Agarwal' THEN InvoiceCount ELSE 0 END), 0)
		, 'Sravan Bachu' = ISNULL(SUM(CASE WHEN queue_name = 'Sravan Bachu' THEN InvoiceCount ELSE 0 END), 0)
		, 'Hema Mandhapati' = ISNULL(SUM(CASE WHEN queue_name = 'Hema Mandhapati' THEN InvoiceCount ELSE 0 END), 0)
		, 'Nagaswathi Pisupati' = ISNULL(SUM(CASE WHEN queue_name = 'Nagaswathi Pisupati' THEN InvoiceCount ELSE 0 END), 0)
		, 'Abhilash Raj Dyda' = ISNULL(SUM(CASE WHEN queue_name = 'Abhilash Raj Dyda' THEN InvoiceCount ELSE 0 END), 0)
		, 'Mayukh Ghatak' = ISNULL(SUM(CASE WHEN queue_name = 'Mayukh Ghatak' THEN InvoiceCount ELSE 0 END), 0)
		, 'Rajini Ganga Reddy' = ISNULL(SUM(CASE WHEN queue_name = 'Rajini Ganga Reddy' THEN InvoiceCount ELSE 0 END), 0)		
		, SUM(invoicecount) AS InvoiceTotal
		 
	FROM(
		SELECT CONVERT(VARCHAR, d.closed_date, 101) AS closeddate
			, t.exception_type
			, ui.first_name + ' ' + ui.last_name as queue_name
			, COUNT(*) AS invoicecount
		FROM cu_exception_detail d
			JOIN user_info ui on ui.user_info_id = d.closed_by_id
			JOIN cu_exception_type t on t.cu_exception_type_id = d.exception_type_id
			JOIN cu_exception cue on cue.cu_exception_id = d.cu_exception_id
			JOIN cu_invoice cui on cui.cu_invoice_id = cue.cu_invoice_id
		WHERE d.closed_date >= GETDATE() -7
			AND d.is_closed = 1
		  --AND cui.is_processed = '1'
			  -- AND is_duplicate <> '1'
			AND ui.first_name + ' ' + ui.last_name IN 
		   ('Amanda Meredith'
		   , 'Amber Hensley'
		   , 'Catherine James'
		   , 'Hillary Stevenson'
		   , 'James Ede'
		   , 'Jordan Martin'
		   , 'Micah Nave'
		   , 'Sergio Victor'
		   , 'Rajitha Sakani '
		   , 'Ram Kumar Gopisetti'
		   , 'Nageshwara Ankolu'
		   , 'Rashmi Khandelwal'
		   , 'AnAND Asthana'
		   , 'Ritesh Agarwal'
		   , 'Sravan Bachu'
		   , 'Hema Mandhapati'
		   , 'Nagaswathi Pisupati'
		   , 'Abhilash Raj Dyda'
		   , 'Mayukh Ghatak'
		   , 'Rajini Ganga Reddy')  
		GROUP BY CONVERT(VARCHAR, d.closed_date, 101)
			, t.exception_type
			, ui.first_name + ' ' + ui.last_name
		) Y
	GROUP BY exception_type
	OPTION (MAXDOP 1)

END
GO
GRANT EXECUTE ON  [dbo].[Get_WeeklyClosedExceptions_Report_P] TO [CBMSApplication]
GO
