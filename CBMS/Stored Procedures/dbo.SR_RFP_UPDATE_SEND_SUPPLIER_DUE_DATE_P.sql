
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	dbo.SR_RFP_UPDATE_SEND_SUPPLIER_DUE_DATE_P

DESCRIPTION: 


INPUT PARAMETERS:    
    Name                DataType          Default     Description    
---------------------------------------------------------------------------------    
	@user_id			varchar(10)
	@session_id			varchar(20)
	@account_group_id	int
	@is_bid_group		bit
	@due_date			datetime
	@Time_Zone_Id		INT				NULL
    @Due_Dt_By_Timezone DATETIME                      
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRANSACTION
		SELECT * FROM dbo.SR_RFP_SEND_SUPPLIER a INNER JOIN dbo.Time_Zone b ON a.Time_Zone_Id = b.Time_Zone_Id
			WHERE SR_ACCOUNT_GROUP_ID = 10000695 AND IS_BID_GROUP = 1
		EXEC dbo.SR_RFP_UPDATE_SEND_SUPPLIER_DUE_DATE_P 1,1,10000695,1,'2016-03-21 12:00:00.000',566,'2016-03-21 02:30:00.000'
		SELECT * FROM dbo.SR_RFP_SEND_SUPPLIER a INNER JOIN dbo.Time_Zone b ON a.Time_Zone_Id = b.Time_Zone_Id
			WHERE SR_ACCOUNT_GROUP_ID = 10000695 AND IS_BID_GROUP = 1
	ROLLBACK TRANSACTION

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DR			Deana Ritter
	RR			Raghu Reddy

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	DR			08/04/2009	Removed Linked Server Updates
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2016-03-16	Global Sourcing - Phase3 - GCS-544 Added new input parameter @Time_Zone_Id

******/
CREATE PROCEDURE [dbo].[SR_RFP_UPDATE_SEND_SUPPLIER_DUE_DATE_P]
      ( 
       @user_id VARCHAR(10)
      ,@session_id VARCHAR(20)
      ,@account_group_id INT
      ,@is_bid_group BIT
      ,@due_date DATETIME
      ,@Time_Zone_Id INT = NULL
      ,@Due_Dt_By_Timezone DATETIME )
AS 
BEGIN
      SET NOCOUNT ON;
	
      DECLARE @sr_rfp_send_supplier_log_id INT
      DECLARE
            @expiredTypeId INT
           ,@processingTypeId INT
           ,@bidPlacedTypeId INT
           ,@openTypeId INT

      SELECT
            @expiredTypeId = entity_id
      FROM
            dbo.ENTITY
      WHERE
            entity_type = 1029
            AND entity_name = 'Expired'
      SELECT
            @processingTypeId = entity_id
      FROM
            dbo.ENTITY
      WHERE
            entity_type = 1029
            AND entity_name = 'Processing'	
      SELECT
            @bidPlacedTypeId = entity_id
      FROM
            dbo.ENTITY
      WHERE
            entity_type = 1029
            AND entity_name = 'Bid Placed'
      SELECT
            @openTypeId = entity_id
      FROM
            dbo.ENTITY
      WHERE
            entity_type = 1029
            AND entity_name = 'Open'

      IF @is_bid_group = 1 
            BEGIN
                  IF @due_date IS NULL 
                        BEGIN
                              SELECT
                                    @due_date = max(due_date)
                                    ,@Due_Dt_By_Timezone = max(Due_Dt_By_Timezone)
                              FROM
                                    dbo.SR_RFP_SEND_SUPPLIER
                              WHERE
                                    sr_account_group_id = @account_group_id
                                    AND is_bid_group = 1
                        END
		
                  UPDATE
                        dbo.SR_RFP_SEND_SUPPLIER
                  SET   
                        due_date = @due_date
                       ,Time_Zone_Id = @Time_Zone_Id
                       ,Due_Dt_By_Timezone = @Due_Dt_By_Timezone
                  WHERE
                        sr_account_group_id = @account_group_id
                        AND is_bid_group = 1

                  UPDATE
                        dbo.SR_RFP_SEND_SUPPLIER
                  SET   
                        bid_status_type_id = @openTypeId
                  WHERE
                        sr_account_group_id = @account_group_id
                        AND is_bid_group = 1
                        AND bid_status_type_id = @expiredTypeId

                  UPDATE
                        dbo.SR_RFP_SEND_SUPPLIER
                  SET   
                        bid_status_type_id = @bidPlacedTypeId
                  WHERE
                        sr_account_group_id = @account_group_id
                        AND is_bid_group = 1
                        AND bid_status_type_id = @processingTypeId


                  UPDATE
                        supplier_log
                  SET   
                        GENERATION_DATE = getdate()
                  FROM
                        dbo.SR_RFP_SEND_SUPPLIER_LOG supplier_log
                        INNER JOIN dbo.SR_RFP_SEND_SUPPLIER supplier
                              ON supplier_log.SR_RFP_SEND_SUPPLIER_ID = supplier.SR_RFP_SEND_SUPPLIER_ID
                  WHERE
                        supplier.sr_account_group_id = @account_group_id
                        AND supplier.is_bid_group = 1
                        
            END 
      ELSE 
            BEGIN
                  IF @due_date IS NULL 
                        BEGIN
                              SELECT
                                    @due_date = max(due_date)
                                    ,@Due_Dt_By_Timezone = max(Due_Dt_By_Timezone)
                              FROM
                                    sr_rfp_send_supplier
                              WHERE
                                    sr_account_group_id = @account_group_id
                                    AND is_bid_group = 0
                        END

                  UPDATE
                        dbo.SR_RFP_SEND_SUPPLIER
                  SET   
                        due_date = @due_date
                       ,Time_Zone_Id = @Time_Zone_Id
                       ,Due_Dt_By_Timezone = @Due_Dt_By_Timezone
                  WHERE
                        sr_account_group_id = @account_group_id
                        AND is_bid_group = 0

                  UPDATE
                        dbo.SR_RFP_SEND_SUPPLIER
                  SET   
                        bid_status_type_id = @openTypeId
                  WHERE
                        sr_account_group_id = @account_group_id
                        AND is_bid_group = 0
                        AND bid_status_type_id = @expiredTypeId

                  UPDATE
                        dbo.SR_RFP_SEND_SUPPLIER
                  SET   
                        bid_status_type_id = @bidPlacedTypeId
                  WHERE
                        sr_account_group_id = @account_group_id
                        AND is_bid_group = 0
                        AND bid_status_type_id = @processingTypeId



                  UPDATE
                        supplier_log
                  SET   
                        generation_date = getdate()
                  FROM
                        dbo.SR_RFP_SEND_SUPPLIER_LOG supplier_log
                        INNER JOIN dbo.SR_RFP_SEND_SUPPLIER supplier
                              ON supplier_log.SR_RFP_SEND_SUPPLIER_ID = supplier.SR_RFP_SEND_SUPPLIER_ID
                  WHERE
                        supplier.sr_account_group_id = @account_group_id
                        AND supplier.is_bid_group = 0

            END
END
;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_UPDATE_SEND_SUPPLIER_DUE_DATE_P] TO [CBMSApplication]
GO
