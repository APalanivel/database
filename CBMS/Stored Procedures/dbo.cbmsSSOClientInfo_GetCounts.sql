SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[cbmsSSOClientInfo_GetCounts]
	( @MyAccountId int
	, @client_id int = null
	)
AS
BEGIN

	   select count(distinct d.division_id) division_count
		, count(distinct s.site_id) site_count
		, count(distinct a.account_id) account_count
		, count(distinct m.meter_id) meter_count
	     from division d
	     join site s on s.division_id = d.division_id
  left outer join account a on a.site_id = s.site_id
  left outer join meter m on m.account_id = a.account_id
            where d.client_id = @client_id
	      and s.closed = 0
	      and s.not_managed = 0


END
GO
GRANT EXECUTE ON  [dbo].[cbmsSSOClientInfo_GetCounts] TO [CBMSApplication]
GO
