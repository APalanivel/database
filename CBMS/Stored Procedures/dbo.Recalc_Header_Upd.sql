SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                        
 NAME: [dbo].[Recalc_Header_Upd]
                        
 DESCRIPTION:                        
			To Update Recalc_Header table.                        
                        
 INPUT PARAMETERS:          
                     
 Name							DataType         Default       Description        
--------------------------------------------------------------------------------------               
 @Recalc_Header_Id				INT
 @Updated_User_Id				INT
 @Grand_Total					DECIMAL(32, 16)  
 Recalc_Failure_Desc			NVARCHAR(MAX)		                  
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
--------------------------------------------------------------------------------------               
                        
 USAGE EXAMPLES:                            
--------------------------------------------------------------------------------------               
 
 BEGIN TRAN
 Select * from Recalc_Header where Recalc_Header_Id = 2
 EXEC [dbo].[Recalc_Header_Upd] 
      @Recalc_Header_Id = 2
     ,@Updated_User_Id = 49
     ,@Grand_Total = '178879.6800000000000000'
	 ,@Recalc_Failure_Desc = 'Invoice total Vs Recalc total > 5%'

	 Select * from Recalc_Header where Recalc_Header_Id = 2
 ROLLBACK                      
 
 AUTHOR INITIALS:        
       
 Initials              Name        
--------------------------------------------------------------------------------------               
 SP                    Sandeep Pigilam    
 NR						Narayana Reddy      
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
--------------------------------------------------------------------------------------               
 SP                    2013-07-25       Created for Data Operations Enhancement Phase III ,Updates recalc_header with  grand_total and logging.           
 NR						2018-11-21		Data2.0 -  Added @Recalc_Failure_Desc to input.                      
******/
CREATE PROCEDURE [dbo].[Recalc_Header_Upd]
    (
        @Recalc_Header_Id INT
        , @Updated_User_Id INT
        , @Grand_Total DECIMAL(32, 16)
        , @Recalc_Failure_Desc NVARCHAR(MAX) = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;
        UPDATE
            dbo.RECALC_HEADER
        SET
            GRAND_TOTAL = @Grand_Total
            , Updated_User_Id = @Updated_User_Id
            , Last_Change_Ts = GETDATE()
            , Recalc_Failure_Desc = @Recalc_Failure_Desc
        WHERE
            RECALC_HEADER_ID = @Recalc_Header_Id;



    END;



GO
GRANT EXECUTE ON  [dbo].[Recalc_Header_Upd] TO [CBMSApplication]
GO
