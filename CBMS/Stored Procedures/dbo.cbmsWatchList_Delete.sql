SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[cbmsWatchList_Delete]
	( @MyAccountId int
	, @watch_list_id int
	)
AS
BEGIN

set nocount on

   delete watch_list
    where watch_list_id = @watch_list_id
exec cbmsWatchList_Get @MyAccountId, @watch_list_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsWatchList_Delete] TO [CBMSApplication]
GO
