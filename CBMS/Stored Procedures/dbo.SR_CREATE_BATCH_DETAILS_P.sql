SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.SR_CREATE_BATCH_DETAILS_P
	@batch_log_id int,
	@account_id int,
	@contract_id int
	AS
	set nocount on	
	insert into sr_batch_log_details(sr_batch_log_id, account_id, contract_id)
	values(@batch_log_id, @account_id, @contract_id)
GO
GRANT EXECUTE ON  [dbo].[SR_CREATE_BATCH_DETAILS_P] TO [CBMSApplication]
GO
