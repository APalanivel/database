SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                
 NAME: [dbo].[Utility_Detail_Analyst_Map_Save_All]                    
                                 
 DESCRIPTION:                                
  Insert or Update data of table Utility_Detail_Analyst_Map based on Search for given @Analyst_Id_Update and @Queue.                  
                                
 INPUT PARAMETERS:                  
                             
 Name                        DataType         Default       Description                
---------------------------------------------------------------------------------------------------------------              
  
@State_Id      INT  
@Commodity_Type_Id    INT     NULL  
@Analyst_Id      INT     NULL  
@Is_Analyst_Mapped    INT     0  
@Vendor_Id      INT     NULL  
@Queue       INT     NULL  
@Analyst_Id_Update    INT     NULL 
@QueueType VARCHAR(25) 'InvoiceProcessingTeam' 
  
                                
OUTPUT PARAMETERS:                  
                                   
 Name                        DataType         Default       Description                
---------------------------------------------------------------------------------------------------------------              
                                
 USAGE EXAMPLES:                                    
---------------------------------------------------------------------------------------------------------------                                    
  
BEGIN TRAN  
SELECT
      *
FROM
      Utility_Detail_Analyst_Map
WHERE
      Group_Info_Id = 379  
EXEC utility_detail_analyst_map_Save_All 
      @State_Id = NULL
     ,@Country_id = 1
     ,@Commodity_Type_Id = NULL
     ,@Analyst_Id = NULL
     ,@Is_Analyst_Mapped = -1
     ,@Vendor_Id = NULL
     ,@Queue = 379
     ,@Analyst_Id_Update = 20590  
    
SELECT
      *
FROM
      Utility_Detail_Analyst_Map
WHERE
      Group_Info_Id = 379  
ROLLBACK TRAN   
  
BEGIN TRAN  
    
EXEC Utility_Detail_Analyst_Map_Sel 
      @State_Id = 5
     ,@Country_id = 4
     ,@Commodity_Type_Id = NULL
     ,@Analyst_Id = NULL
     ,@Is_Analyst_Mapped = 0
     ,@Vendor_Id = NULL
     ,@Queue = 379  
    
EXEC utility_detail_analyst_map_Save_All 
      @State_Id = 5
     ,@Country_id = 4
     ,@Commodity_Type_Id = NULL
     ,@Analyst_Id = NULL
     ,@Is_Analyst_Mapped = 0
     ,@Vendor_Id = NULL
     ,@Queue = 379
     ,@Analyst_Id_Update = 38291  
    
EXEC Utility_Detail_Analyst_Map_Sel 
      @State_Id = 5
     ,@Country_id = 4
     ,@Commodity_Type_Id = NULL
     ,@Analyst_Id = NULL
     ,@Is_Analyst_Mapped = 0
     ,@Vendor_Id = NULL
     ,@Queue = 379  
  
ROLLBACK TRAN  
  
  
BEGIN TRAN   
EXEC [dbo].[Utility_Detail_Analyst_Map_Sel] 
      @State_Id = 5
     ,@Country_Id = 4
     ,@Commodity_Type_Id = 100025
     ,@Vendor_Id = NULL
     ,@Queue = 135
     ,@Is_Analyst_Mapped = 1
     ,@Analyst_Id = 21373   
     
      
EXEC [dbo].[Utility_Detail_Analyst_Map_Save_All] 
      @State_Id = 5
     ,@Country_Id = 4
     ,@Commodity_Type_Id = 100025
     ,@Vendor_Id = NULL
     ,@Queue = 135
     ,@Is_Analyst_Mapped = 1
     ,@Analyst_Id = 21373
     ,@Analyst_Id_Update = 29574
  --"Michael Buchberger"  
              
  
       
EXEC [dbo].[Utility_Detail_Analyst_Map_Sel] 
      @State_Id = 5
     ,@Country_Id = 4
     ,@Commodity_Type_Id = 100025
     ,@Vendor_Id = NULL
     ,@Queue = 135
     ,@Is_Analyst_Mapped = 1
     ,@Analyst_Id = 29574      
ROLLBACK                                        
                               
 AUTHOR INITIALS:                
               
 Initials              Name                
---------------------------------------------------------------------------------------------------------------                              
 SP  Sandeep Pigilam                  
                                 
 MODIFICATIONS:              
                  
 Initials              Date             Modification              
---------------------------------------------------------------------------------------------------------------              
 SP                    2014-02-24       Created  
******/      
CREATE PROCEDURE dbo.Utility_Detail_Analyst_Map_Save_All  
      (   
       @Country_Id INT  
      ,@State_Id INT = NULL  
      ,@Commodity_Type_Id INT = NULL  
      ,@Analyst_Id INT = NULL  
      ,@Is_Analyst_Mapped INT = -1  
      ,@Vendor_Id INT = NULL  
      ,@Queue INT = NULL  
      ,@Analyst_Id_Update INT = NULL  
      ,@QueueType VARCHAR(25) = 'InvoiceProcessingTeam' )  
AS   
BEGIN    
      SET NOCOUNT ON  
        
      DECLARE @Group_List TABLE ( Group_Info_Id INT )  
  
      INSERT      INTO @Group_List  
                  (   
                   Group_Info_Id )  
                  SELECT  
                        gi.Group_Info_Id  
                  FROM  
                        dbo.GROUP_INFO gi  
                        INNER JOIN dbo.QUEUE q  
                              ON q.queue_id = gi.QUEUE_ID  
                        INNER JOIN dbo.ENTITY qt  
                              ON qt.Entity_Id = q.QUEUE_TYPE_ID  
                         INNER JOIN dbo.Codeset cs  
         ON cs.Codeset_Id = q.QUEUE_TYPE_ID            
                  WHERE  
                        ( @Queue IS NULL  
                          OR gi.GROUP_INFO_ID = @Queue )  
                        AND qt.Entity_Description = 'Invoice Source'  
                        AND cs.Codeset_Name = @QueueType  
  
      MERGE INTO dbo.Utility_Detail_Analyst_Map AS tgt  
            USING   
                  ( SELECT  
                        ud.Utility_Detail_ID  
                       ,@Queue AS Group_Info_Id  
                    FROM  
                        dbo.Vendor v  
                        INNER JOIN dbo.Vendor_State_Map stmap  
                              ON stmap.Vendor_id = v.Vendor_id  
                        INNER JOIN dbo.state st  
                              ON st.State_Id = stmap.State_Id  
                        INNER JOIN dbo.Utility_Detail ud  
                              ON ud.Vendor_id = v.Vendor_id  
                        CROSS JOIN @Group_List gl  
                        LEFT OUTER JOIN dbo.Utility_Detail_Analyst_Map map  
                              ON map.Utility_Detail_id = ud.Utility_Detail_id  
                                 AND map.Group_Info_ID = gl.Group_Info_Id  
                    WHERE  
                        st.Country_Id = @Country_Id  
                        AND ( @State_Id IS NULL  
                              OR st.State_Id = @State_Id )  
                        AND ( ( @Is_Analyst_Mapped = -1  
                                AND ( @Analyst_Id IS NULL  
                                      OR map.Analyst_ID = @Analyst_Id ) )  
                              OR ( @Is_Analyst_Mapped = 1  
                                   AND ( map.Analyst_ID = @Analyst_Id  
                                         OR ( map.Analyst_ID IS NOT NULL  
                                              AND @Analyst_Id IS NULL ) ) )  
                              OR ( @Is_Analyst_Mapped = 0  
                                   AND map.Analyst_ID IS NULL  
                                   AND @Analyst_ID IS NULL ) )  
                        AND ( @Commodity_Type_Id IS NULL  
                              OR EXISTS ( SELECT  
                                                1  
                                          FROM  
                                                dbo.Vendor_Commodity_Map cmap  
                                          WHERE  
                                                cmap.Vendor_ID = v.Vendor_ID  
                                                AND cmap.Commodity_Type_Id = @Commodity_Type_Id ) )  
                        AND ( @Vendor_Id IS NULL  
                              OR v.Vendor_ID = @Vendor_Id )  
                    GROUP BY  
                        ud.Utility_Detail_Id ) AS src  
            ON tgt.Utility_Detail_Id = src.Utility_Detail_Id  
                  AND tgt.Group_Info_Id = src.Group_Info_Id  
            WHEN MATCHED   
                  THEN      
     UPDATE         SET   
                        tgt.Analyst_ID = @Analyst_Id_Update  
            WHEN NOT MATCHED   
                  THEN INSERT  
                        (   
                         Utility_Detail_Id  
                        ,Group_Info_Id  
                        ,Analyst_Id )  
                    VALUES  
                        (   
                         src.Utility_Detail_Id  
                        ,@Queue  
                        ,@Analyst_Id_Update );  
  
END  
  
;
GO
GRANT EXECUTE ON  [dbo].[Utility_Detail_Analyst_Map_Save_All] TO [CBMSApplication]
GO
