SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE      PROCEDURE dbo.BUDGET_GET_BUDGET_ACCOUNT_LIST_P
	@userId varchar(10),
	@sessionId varchar(20),
	@originalBudgetId int,
	@revisedBudgetId int
	AS
	begin
	set nocount on

	select 	a.budget_account_id as originalBudgetAccountId ,b.budget_account_id revisedBudgetAccountId
	from 	budget_account a, budget_account b,
		BUDGET_ACCOUNT_TYPE_VW type_vw
	where 	a.budget_id = @originalBudgetId
		and b.budget_id = @revisedBudgetId
		and a.account_id = b.account_id
		and type_vw.account_id = a.account_id
		and type_vw.budget_account_id = a.budget_account_id
		and type_vw.budget_account_type in('A&B', 'C&D Created')
		
	end













GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_BUDGET_ACCOUNT_LIST_P] TO [CBMSApplication]
GO
