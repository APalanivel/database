SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
NAME:  
 
 dbo.SiteGroup_UPD
 
 DESCRIPTION:   
 
	This procedure udpates the Sitegroup table data.
 
 INPUT PARAMETERS:  
 Name			DataType	Default		Description  
------------------------------------------------------------    
	@SiteGroup_Id INT
	, @Sitegroup_Name VARCHAR(200)
	, @Sitegroup_Type_Cd INT
	, @Mod_User_Id INT
	, @Is_Locked BIT = 0
	, @Is_Complete BIT = 1
	
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  USAGE EXAMPLES:  
------------------------------------------------------------  

	EXEC dbo.SiteGroup_UPD 
	

AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 HG			Hari    
 
 MODIFICATIONS   
 Initials	Date		Modification  
------------------------------------------------------------  

						    
  
******/

CREATE PROCEDURE [dbo].[SiteGroup_UPD](
	@SiteGroup_Id INT
	, @Sitegroup_Name VARCHAR(200)
	, @Sitegroup_Type_Cd INT
	, @Mod_User_Id INT
	, @Is_Locked BIT = 0
	, @Is_Complete BIT = 1)          
AS
BEGIN

	SET NOCOUNT ON
	
	UPDATE sg
		SET sg.Sitegroup_Name = @SiteGroup_Name
			   , sg.SiteGroup_Type_Cd = @Sitegroup_Type_Cd
			   , sg.Mod_User_Id = @Mod_User_Id
			   , sg.Mod_Dt = GetDate()
			   , sg.Is_Locked = @Is_Locked
			   , sg.Is_Complete = @Is_Complete
	FROM dbo.Sitegroup sg
	WHERE sg.SiteGroup_Id = @SiteGroup_Id

END

GO
GRANT EXECUTE ON  [dbo].[SiteGroup_UPD] TO [CBMSApplication]
GO
