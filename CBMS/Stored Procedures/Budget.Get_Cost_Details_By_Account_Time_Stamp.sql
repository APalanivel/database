SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
NAME:  
 CBMS.Budget.[Get_Cost_Details_By_Account_Time_Stamp]
  
DESCRIPTION:  
  Script to get Cost details for that account's for a given time period 
  
INPUT PARAMETERS:  
 Name     DataType  Default Description  
---------------------------------------------------------------  
 @Cu_Invoice_Id     INT  
 @dtStart_Date 	    Date
 @dtEnd_Date		Date
  
OUTPUT PARAMETERS:  
 Name     DataType  Default Description  
------------------------------------------------------------  
   
USAGE EXAMPLES:  
------------------------------------------------------------  
  
 EXEC Budget.[Get_Cost_Details_By_Account_Time_Stamp] 26654,'2001-02-01','2019-03-03'  
 
 
AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 SP		Srinivas Patchava

   
MODIFICATIONS  
 Initials Date			Modification  
------------------------------------------------------------  
 SP      2019-05-28	Created
 RKV	 2019-06-05 Chaged the source table to CUAD
******/
CREATE PROC [Budget].[Get_Cost_Details_By_Account_Time_Stamp]
     (
         @intAccount_ID INT
         , @dtStart_Date DATETIME
         , @dtEnd_Date DATETIME
         , @Meter_Id INT
         , @currency_Unit_Id INT = NULL

     )
AS
    BEGIN

        DECLARE
            @Client_Currency_Group_Id INT
            , @Commodity_Id INT;

        SELECT
            @Client_Currency_Group_Id = ch.Client_Currency_Group_Id
            , @Commodity_Id = cha.Commodity_Id
        FROM
            Core.Client_Hier_Account AS cha
            INNER JOIN Core.Client_Hier AS ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
        WHERE
            cha.Account_Id = @intAccount_ID
            AND cha.Meter_Id = @Meter_Id
        GROUP BY
            ch.Client_Currency_Group_Id
            , cha.Commodity_Id;



        SELECT
            cuad.Service_Month
            , SUM(CASE WHEN bm.Bucket_Name = 'Total Cost' THEN cuad.Bucket_Value * cuc.CONVERSION_FACTOR
                  END) Charge_Value
            , cu.CURRENCY_UNIT_NAME
        FROM
            dbo.Cost_Usage_Account_Dtl AS cuad
            INNER JOIN dbo.Bucket_Master bm
                ON bm.Bucket_Master_Id = cuad.Bucket_Master_Id
            LEFT OUTER JOIN dbo.Code c
                ON c.Code_Id = bm.Bucket_Type_Cd
            LEFT OUTER JOIN dbo.CURRENCY_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = cuad.CURRENCY_UNIT_ID
                   AND  cuc.CURRENCY_GROUP_ID = @Client_Currency_Group_Id
                   AND  cuc.CONVERSION_DATE = cuad.Service_Month
                   AND  cuc.CONVERTED_UNIT_ID = COALESCE(@currency_Unit_Id, cuad.CURRENCY_UNIT_ID)
                   AND  c.Code_Value = 'Charge'
            LEFT OUTER JOIN dbo.CURRENCY_UNIT AS cu
                ON cu.CURRENCY_UNIT_ID = COALESCE(@currency_Unit_Id, cuad.CURRENCY_UNIT_ID)
        WHERE
            cuad.ACCOUNT_ID = @intAccount_ID
            AND cuad.Service_Month BETWEEN @dtStart_Date
                                   AND     @dtEnd_Date
            AND bm.Bucket_Name = 'Total Cost'
			AND bm.Commodity_Id = @Commodity_Id
        GROUP BY
            cuad.Service_Month
            , cu.CURRENCY_UNIT_NAME;



    END;
GO
GRANT EXECUTE ON  [Budget].[Get_Cost_Details_By_Account_Time_Stamp] TO [CBMSApplication]
GO
