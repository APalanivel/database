
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
          
/******            
NAME:            
 dbo.cbmsAlternateFuel_GetTrackedForSite          
          
DESCRIPTION:          
  Used to distinct Alternate Fuel types tracked based on the given input.          
           
 INPUT PARAMETERS:            
 Name			DataType	   Default Description            
------------------------------------------------------------            
 @Client_Id		INT		   NULL
 @Division_Id		INT		   NULL
 @Client_Hier_Id	INT		   NULL        
            
 OUTPUT PARAMETERS:          
 Name   DataType  Default Description          
------------------------------------------------------------          
 
 USAGE EXAMPLES:          
------------------------------------------------------------          
    EXECUTE dbo.cbmsAlternateFuel_GetTrackedForSite 
           
AUTHOR INITIALS:            
 Initials	Name            
------------------------------------------------------------            
 AP		Athmaram Pabbathi

 MODIFICATIONS             
 Initials	Date		Modification            
------------------------------------------------------------            
 AP	     2012-03-14 Following changes has been made as apart of Addl Data Prj
					   - Removed dbo.Site, dbo.Client, dbo.Division table and used Core.Client_Hier to get list of sites
					   - Replaced @Site_Id parameter with @Client_Hier_Id
					   - Removed unused parameter @MyAccountId 
******/

CREATE  PROCEDURE [dbo].[cbmsAlternateFuel_GetTrackedForSite]
      ( 
       @Client_Id INT = NULL
      ,@Division_Id INT = NULL
      ,@Client_Hier_Id INT = NULL )
AS 
BEGIN
      SET NOCOUNT ON

      SELECT
            af.Alternate_Fuel_Type_Id
           ,e.Entity_Name AS Alternate_Fuel_Type
      FROM
            Core.Client_Hier ch
            INNER JOIN dbo.Alternate_Fuel af
                  ON af.Site_Id = ch.Site_Id
            INNER JOIN dbo.Entity e
                  ON e.Entity_Id = af.Alternate_Fuel_Type_Id
      WHERE
            ( @Client_Hier_Id IS NULL
              OR ch.Client_Hier_Id = @Client_Hier_Id )
            AND ( @Client_Id IS NULL
                  OR ch.Client_Id = @Client_Id )
            AND ( @Division_Id IS NULL
                  OR ch.Sitegroup_Id = @Division_Id )
            AND af.Is_Reported = 1
      GROUP BY
            af.Alternate_Fuel_Type_Id
           ,e.Entity_Name
      ORDER BY
            Alternate_Fuel_Type


END
;
GO

GRANT EXECUTE ON  [dbo].[cbmsAlternateFuel_GetTrackedForSite] TO [CBMSApplication]
GO
