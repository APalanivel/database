SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure dbo.seContentPage_GetAll
As
BEGIN
	set nocount on
	 select ContentPageId
				, PageLabel
		 from seContentPage
 order by PageLabel asc

END
GO
GRANT EXECUTE ON  [dbo].[seContentPage_GetAll] TO [CBMSApplication]
GO
