
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	dbo.[cbmsCountry_Save]


DESCRIPTION: 


INPUT PARAMETERS:    
      Name                  DataType          Default     Description    
------------------------------------------------------------------    
	@MyAccountId               int
	@CountryId                 int               null
	@CountryName               varchar(200)
        
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------

BEGIN TRAN

exec cbmsCountry_Save
      @MyAccountId = 1
     ,@CountryId = null
     ,@CountryName = 'Test Value'

SELECT
      *
FROM
      dbo.Country
WHERE
      Country_Name = 'Test Value'

SELECT
      *
FROM
      dbo.Country_Commodity_Uom
WHERE
      country_id = 153

ROLLBACK TRAN			


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	DR			08/04/2009	Removed Linked Server Updates
	DMR			09/10/2010	Modified for Quoted_Identifier
	HG			2015-07-10	Added code to save the default uom for each service of the new country
******/
CREATE PROCEDURE [dbo].[cbmsCountry_Save]
      ( 
       @MyAccountId INT
      ,@CountryId INT = NULL
      ,@CountryName VARCHAR(200) )
AS 
BEGIN

      SET NOCOUNT ON;
      DECLARE
            @this_id INT
           ,@System_User_Id INT;

      SELECT
            @System_User_Id = User_Info_Id
      FROM
            dbo.User_Info
      WHERE
            Username = 'conversion';

      IF @CountryId IS NOT NULL	--Update the record
            BEGIN
                  UPDATE
                        country
                  SET   
                        country_name = @CountryName
                  WHERE
                        country_id = @CountryId;
            END;
      ELSE				--Insert the record
            BEGIN
            
                  BEGIN TRY
                        BEGIN TRAN;
                        
                        INSERT      INTO country
                                    ( country_name )
                        VALUES
                                    ( @CountryName );

                        SELECT
                              @this_id = SCOPE_IDENTITY();

			-- Assign the default UOM for all the country
			
                        EXEC dbo.Country_Commodity_Uom_Merge 
                              @Country_Id = @this_id
                             ,@Commodity_Id = NULL
                             ,@Default_Uom_Type_Id = NULL
                             ,@User_Info_Id = @System_User_Id;

	--get the currency_unit_id of the newly inserted record
                        SELECT
                              @this_id;

                        COMMIT TRAN;
                  END TRY
                  BEGIN CATCH
                        IF @@TRANCOUNT > 0 
                              BEGIN
                                    ROLLBACK;
                              END;

                        EXEC dbo.usp_RethrowError;

                  END CATCH;
            END;
END;

;
GO

GRANT EXECUTE ON  [dbo].[cbmsCountry_Save] TO [CBMSApplication]
GO
