
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********  
NAME: 
		dbo.BUDGET_GET_CONTRACTS_FOR_OVERLAP_P   

DESCRIPTION:     

	
INPUT PARAMETERS:    
Name				DataType	Default		Description    
------------------------------------------------------------    
@budgetaccountId	INT
@from_date			DATETIME
@to_date			DATETIME  


OUTPUT PARAMETERS:    
Name   DataType  Default Description    
------------------------------------------------------------    

USAGE EXAMPLES:    
------------------------------------------------------------    

	EXEC dbo.BUDGET_GET_CONTRACTS_FOR_OVERLAP_P 539894,'2013-12-01','2014-11-01'
	EXEC dbo.BUDGET_GET_CONTRACTS_FOR_OVERLAP_P 509568,'2013-12-01','2014-12-01'
	EXEC dbo.BUDGET_GET_CONTRACTS_FOR_OVERLAP_P 443799,'2013-12-01','2014-12-01'
	
	SELECT * FROM dbo.budget_contract_vw WHERE account_id=35877 AND contract_id = 93097
	SELECT * FROM dbo.budget_contract_vw WHERE account_id=75491 AND contract_id = 97491
	SELECT * FROM dbo.budget_contract_vw WHERE account_id=16594 AND contract_id = 83217


AUTHOR INITIALS:    
Initials	Name    
------------------------------------------------------------    
RR			Raghu Reddy
 
MODIFICATIONS     
Initials	Date		Modification    
------------------------------------------------------------    
RR			2013-07-05	Added description header
						Maint-2041 Added group by to avoid duplicates, contract will have entries for each supplier account, returning
						duplicate records if the contract have multiple supplier accounts associated with an utility account
						dbo.budget_contract_vw view is replaced with Client_Hier_Account and contract tables
						dbo.budget_contract_budget table removed as it is not using anywhere
						
    
******/
CREATE PROCEDURE dbo.BUDGET_GET_CONTRACTS_FOR_OVERLAP_P
      @budgetaccountId INT
     ,@from_date DATETIME
     ,@to_date DATETIME
AS 
BEGIN
      SET nocount ON

      SELECT
            con.contract_id
           ,con.contract_start_date
           ,con.contract_end_date
      FROM
            dbo.budget bdg
            INNER JOIN dbo.budget_account bdgacc
                  ON bdg.budget_id = bdgacc.budget_id
            INNER JOIN Core.Client_Hier_Account utility
                  ON utility.Account_Id = bdgacc.ACCOUNT_ID
                     AND bdg.COMMODITY_TYPE_ID = utility.Commodity_Id
            INNER JOIN Core.Client_Hier_Account supp
                  ON utility.Meter_Id = supp.Meter_Id
            INNER JOIN dbo.CONTRACT con
                  ON con.CONTRACT_ID = supp.Supplier_Contract_ID
                     AND con.CONTRACT_TYPE_ID = 153
      WHERE
            bdgacc.budget_account_id = @budgetaccountId
            AND con.contract_start_date <= ( dateadd(month, 1, @to_date) - 1 )
            AND con.contract_end_date > getdate()
            AND con.contract_end_date >= @from_date
            AND utility.Account_Type = 'Utility '
            AND supp.Account_Type = 'Supplier'
      GROUP BY
            con.contract_id
           ,con.contract_start_date
           ,con.contract_end_date
      ORDER BY
            con.contract_start_date 

END
;
GO

GRANT EXECUTE ON  [dbo].[BUDGET_GET_CONTRACTS_FOR_OVERLAP_P] TO [CBMSApplication]
GO
