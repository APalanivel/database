SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [dbo].[cbmsSsoProject_GetForRateComparison] 
	( @rc_rate_comparison_id int 
	)
AS
BEGIN

	   select rc.rc_rate_comparison_id
		, rc.client_id
		, rc.site_id
		, s.site_name
		, rc.creation_date
		, r.commodity_type_id
		, c.entity_name commodity_type
		, a.account_number
	     from rc_rate_comparison rc
	     join rate r on r.rate_id = rc.rate_id
	     join entity c on c.entity_id = r.commodity_type_id
	     join vwsitename s on s.site_id = rc.site_id
	     join account a on a.account_id = rc.account_id
	    where rc.rc_rate_comparison_id = @rc_rate_comparison_id 
	
END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoProject_GetForRateComparison] TO [CBMSApplication]
GO
