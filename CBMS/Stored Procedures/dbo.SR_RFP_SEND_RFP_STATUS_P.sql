
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/******

NAME:  
  dbo.SR_RFP_SEND_RFP_STATUS_P
  
DESCRIPTION:
  
INPUT PARAMETERS:  
Name				DataType	Default Description  
------------------------------------------------------------
@rfp_Id				INT,
  
OUTPUT PARAMETERS:  
Name   DataType  Default Description  
------------------------------------------------------------  

USAGE EXAMPLES:  
------------------------------------------------------------  
	
	exec SR_RFP_SEND_RFP_STATUS_P 661

AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------
 CPE		Chaitanya Panduga Eshwar
 AKR        Ashok Kumar Raju
  
MODIFICATIONS  
 Initials Date		 Modification  
------------------------------------------------------------  
 AKR      2012-01-21 Changes Summit to Schnider Electric as a part of Name Change Project
******/
CREATE   PROCEDURE dbo.SR_RFP_SEND_RFP_STATUS_P ( @rfp_id INT )
AS 
BEGIN
      SET nocount ON
      DECLARE @temp TABLE ( [site_id] [int] )

      BEGIN --procedure
            INSERT      INTO @temp
                        ( 
                         site_id )
                        SELECT DISTINCT
                              account.site_id
                        FROM
                              sr_rfp_send_supplier supp
                              JOIN sr_rfp_account rfpacc
                                    ON supp.sr_account_group_id = rfpacc.sr_rfp_account_id
                                       AND is_bid_group = 0 
	--and status_type_id = 1090 --for posted
                                       AND rfpacc.sr_rfp_id = @rfp_id
                                       AND rfpacc.is_deleted = 0
                              JOIN account
                                    ON rfpacc.account_id = account.account_id
                        UNION
                        SELECT DISTINCT
                              account.site_id
                        FROM
                              sr_rfp_send_supplier supp
                              JOIN sr_rfp_account rfpacc
                                    ON supp.sr_account_group_id = rfpacc.sr_rfp_bid_group_id
                                       AND is_bid_group = 1 
	--and status_type_id = 1090 --for posted
                                       AND rfpacc.sr_rfp_id = @rfp_id
                                       AND rfpacc.is_deleted = 0
                              JOIN account
                                    ON rfpacc.account_id = account.account_id 



            DECLARE
                  @siteId INT
                 ,@projectId INT
                 ,@user_id INT
                 ,@smruploadedaccountcount INT
                 ,@account AS INT
                 ,@postedcount AS INT
            WHILE ( ( SELECT
                        count(site_id)
                      FROM
                        @temp ) > 0 ) 
                  BEGIN --//while
                        SET @siteId = ( SELECT TOP 1 * FROM @temp )
                        IF ( SELECT
                              count(*)
                             FROM
                              RFP_SITE_PROJECT
                             WHERE
                              RFP_ID = @rfp_id
                              AND SITE_ID = @siteId ) > 0 
                              BEGIN --//if1
                                    SET @projectId = ( SELECT
                                                            SSO_PROJECT_ID
                                                       FROM
                                                            RFP_SITE_PROJECT
                                                       WHERE
                                                            RFP_ID = @rfp_id
                                                            AND SITE_ID = @siteid )
                                    SET @user_id = ( SELECT TOP 1
                                                      user_info_id
                                                     FROM
                                                      client_cem_map
                                                     ,vwsitename
                                                     WHERE
                                                      vwsitename.site_id = @siteid
                                                      AND vwsitename.client_id = client_cem_map.client_id )
                                    IF ( SELECT
                                          count(SSO_PROJECT_ACTIVITY_ID)
                                         FROM
                                          SSO_PROJECT_ACTIVITY
                                         WHERE
                                          SSO_PROJECT_ID = @projectId
                                          AND ACTIVITY_DESCRIPTION = 'Schneider Electric is actively engaged with the supplier community, soliciting proposals and shaping the offers to align with your strategic sourcing profile.  Schneider Electric is building the analysis package around the various offers and formulating its recommendation for your review' ) = 0 
                                          BEGIN --//if2
                                                SET @postedcount = 0
                                                SELECT
                                                      @postedcount = count(DISTINCT sr_rfp_account_id)
                                                FROM
                                                      sr_rfp_send_supplier supp
                                                      JOIN sr_rfp_account rfpacc
                                                            ON supp.sr_account_group_id = rfpacc.sr_rfp_account_id
                                                               AND is_bid_group = 0
                                                               AND status_type_id = 1090 --for posted
                                                               AND rfpacc.sr_rfp_id = @rfp_id
                                                               AND rfpacc.is_deleted = 0
                                                      JOIN account
                                                            ON rfpacc.account_id = account.account_id
                                                               AND account.site_id = @siteId
                                                SELECT
                                                      @postedcount = @postedcount + count(DISTINCT sr_rfp_bid_group_id)
                                                FROM
                                                      sr_rfp_send_supplier supp
                                                      JOIN sr_rfp_account rfpacc
                                                            ON supp.sr_account_group_id = rfpacc.sr_rfp_bid_group_id
                                                               AND is_bid_group = 1
                                                               AND status_type_id = 1090 --for posted
                                                               AND rfpacc.sr_rfp_id = @rfp_id
                                                               AND rfpacc.is_deleted = 0
                                                      JOIN account
                                                            ON rfpacc.account_id = account.account_id
                                                               AND account.site_id = @siteId
                                                IF ( @postedcount ) > 0 
                                                      BEGIN --if3
                                                            INSERT      INTO SSO_PROJECT_ACTIVITY
                                                                        ( 
                                                                         SSO_PROJECT_ID
                                                                        ,CREATED_BY_ID
                                                                        ,ACTIVITY_DATE
                                                                        ,ACTIVITY_DESCRIPTION )
                                                            VALUES
                                                                        ( 
                                                                         @projectId
                                                                        ,@user_id
                                                                        ,getdate()
                                                                        ,'Schneider Electric is actively engaged with the supplier community, soliciting proposals and shaping the offers to align with your strategic sourcing profile.  Schneider Electric is building the analysis package around the various offers and formulating its recommendation for your review' )
                                                            INSERT      INTO SSO_PROJECT_STEP
                                                                        ( 
                                                                         SSO_PROJECT_ID
                                                                        ,STEP_NO
                                                                        ,IS_ACTIVE
                                                                        ,IS_COMPLETE )
                                                            VALUES
                                                                        ( 
                                                                         @projectId
                                                                        ,3
                                                                        ,1
                                                                        ,0 )

                                                      END  --if3

                                          END --//if2
                                    ELSE 
                                          BEGIN  --else of if2
                                                SET @postedcount = 0
                                                SELECT
                                                      @postedcount = count(DISTINCT sr_rfp_account_id)
                                                FROM
                                                      sr_rfp_send_supplier supp
                                                      JOIN sr_rfp_account rfpacc
                                                            ON supp.sr_account_group_id = rfpacc.sr_rfp_account_id
                                                               AND is_bid_group = 0
                                                               AND status_type_id = 1090 --for posted
                                                               AND rfpacc.sr_rfp_id = @rfp_id
                                                               AND rfpacc.is_deleted = 0
                                                      JOIN account
                                                            ON rfpacc.account_id = account.account_id
                                                               AND account.site_id = @siteId
                                                SELECT
                                                      @postedcount = @postedcount + count(DISTINCT sr_rfp_bid_group_id)
                                                FROM
                                                      sr_rfp_send_supplier supp
                                                      JOIN sr_rfp_account rfpacc
                                                            ON supp.sr_account_group_id = rfpacc.sr_rfp_account_id
                                                               AND is_bid_group = 1
                                                               AND status_type_id = 1090 --for posted
                                                               AND rfpacc.sr_rfp_id = @rfp_id
                                                               AND rfpacc.is_deleted = 0
                                                      JOIN account
                                                            ON rfpacc.account_id = account.account_id
                                                               AND account.site_id = @siteId
                                                IF ( @postedcount ) = 0 
                                                      BEGIN --if4

                                                            SET @smruploadedaccountcount = 0
                                                            SET @account = 0
                                                            SELECT
                                                                  @account = count(DISTINCT sr_rfp_account_id)
                                                            FROM
                                                                  sr_rfp_sop_smr
                                                                 ,sr_rfp_account
                                                                 ,account
                                                                 ,cbms_image img
                                                            WHERE
                                                                  sr_rfp_sop_smr.sr_account_group_id = sr_rfp_account.sr_rfp_account_id
                                                                  AND sr_rfp_account.sr_rfp_id = @rfp_id
                                                                  AND is_bid_group = 0
                                                                  AND sr_rfp_account.is_deleted = 0
                                                                  AND sr_rfp_account.account_id = account.account_id
                                                                  AND sr_rfp_sop_smr.cbms_image_id = img.cbms_image_id
                                                                  AND img.cbms_image_type_id = 1200 --//for smr upload.
                                                                  AND account.site_id = @siteId
	
                                                            SELECT
                                                                  @account = @account + count(DISTINCT sr_rfp_bid_group_id)
                                                            FROM
                                                                  sr_rfp_sop_smr
                                                                 ,sr_rfp_account
                                                                 ,account
                                                                 ,cbms_image img
                                                            WHERE
                                                                  sr_rfp_sop_smr.sr_account_group_id = sr_rfp_account.sr_rfp_bid_group_id
                                                                  AND sr_rfp_account.sr_rfp_id = @rfp_id
                                                                  AND is_bid_group = 1
                                                                  AND sr_rfp_account.is_deleted = 0
                                                                  AND sr_rfp_account.account_id = account.account_id
                                                                  AND sr_rfp_sop_smr.cbms_image_id = img.cbms_image_id
                                                                  AND img.cbms_image_type_id = 1200 --//for smr upload.
                                                                  AND account.site_id = @siteId
	
                                                            SET @smruploadedaccountcount = ( SELECT @account )
                                                            IF ( @smruploadedaccountcount ) = 0 
                                                                  BEGIN  --if5
                                                                        DELETE FROM
                                                                              SSO_PROJECT_ACTIVITY
                                                                        WHERE
                                                                              sso_project_id = @projectId
                                                                              AND ACTIVITY_DESCRIPTION = 'Schneider Electric is actively engaged with the supplier community, soliciting proposals and shaping the offers to align with your strategic sourcing profile.  Schneider Electric is building the analysis package around the various offers and formulating its recommendation for your review'
                                                                  END    --if5
                                                      END   --if4
                                          END	--else of if2
                                    IF ( SELECT
                                          count(SSO_PROJECT_STEP_ID)
                                         FROM
                                          SSO_PROJECT_STEP
                                         WHERE
                                          SSO_PROJECT_ID = @projectId
                                          AND STEP_NO = 3 ) = 0 
                                          BEGIN --//if6
                                                SET @postedcount = 0
                                                SELECT
                                                      @postedcount = count(DISTINCT sr_rfp_account_id)
                                                FROM
                                                      sr_rfp_send_supplier supp
                                                      JOIN sr_rfp_account rfpacc
                                                            ON supp.sr_account_group_id = rfpacc.sr_rfp_account_id
                                                               AND is_bid_group = 0
                                                               AND status_type_id = 1090 --for posted
                                                               AND rfpacc.sr_rfp_id = @rfp_id
                                                               AND rfpacc.is_deleted = 0
                                                      JOIN account
                                                            ON rfpacc.account_id = account.account_id
                                                               AND account.site_id = @siteId
                                                SELECT
                                                      @postedcount = @postedcount + count(DISTINCT sr_rfp_bid_group_id)
                                                FROM
                                                      sr_rfp_send_supplier supp
                                                      JOIN sr_rfp_account rfpacc
                                                            ON supp.sr_account_group_id = rfpacc.sr_rfp_bid_group_id
                                                               AND is_bid_group = 1
                                                               AND status_type_id = 1090 --for posted
                                                               AND rfpacc.sr_rfp_id = @rfp_id
                                                               AND rfpacc.is_deleted = 0
                                                      JOIN account
                                                            ON rfpacc.account_id = account.account_id
                                                               AND account.site_id = @siteId
                                                IF ( @postedcount ) > 0 
                                                      BEGIN --if7
                                                            INSERT      INTO SSO_PROJECT_STEP
                                                                        ( 
                                                                         SSO_PROJECT_ID
                                                                        ,STEP_NO
                                                                        ,IS_ACTIVE
                                                                        ,IS_COMPLETE )
                                                            VALUES
                                                                        ( 
                                                                         @projectId
                                                                        ,3
                                                                        ,1
                                                                        ,0 )
                                                      END  --if7
                                          END --//if6
                                    ELSE 
                                          BEGIN --else of if6
                                                SET @postedcount = 0
                                                SELECT
                                                      @postedcount = count(DISTINCT sr_rfp_account_id)
                                                FROM
                                                      sr_rfp_send_supplier supp
                                                      JOIN sr_rfp_account rfpacc
                                                            ON supp.sr_account_group_id = rfpacc.sr_rfp_account_id
                                                               AND is_bid_group = 0
                                                               AND status_type_id = 1090 --for posted
                                                               AND rfpacc.sr_rfp_id = @rfp_id
                                                               AND rfpacc.is_deleted = 0
                                                      JOIN account
                                                            ON rfpacc.account_id = account.account_id
                                                               AND account.site_id = @siteId
                                                SELECT
                                                      @postedcount = @postedcount + count(DISTINCT sr_rfp_bid_group_id)
                                                FROM
                                                      sr_rfp_send_supplier supp
                                                      JOIN sr_rfp_account rfpacc
                                                            ON supp.sr_account_group_id = rfpacc.sr_rfp_bid_group_id
                                                               AND is_bid_group = 1
                                                               AND status_type_id = 1090 --for posted
                                                               AND rfpacc.sr_rfp_id = @rfp_id
                                                               AND rfpacc.is_deleted = 0
                                                      JOIN account
                                                            ON rfpacc.account_id = account.account_id
                                                               AND account.site_id = @siteId
                                                IF ( @postedcount ) = 0 
                                                      BEGIN --if8

                                                            SET @smruploadedaccountcount = 0

                                                            SET @account = 0
	
                                                            SELECT
                                                                  @account = count(DISTINCT sr_rfp_account_id)
                                                            FROM
                                                                  sr_rfp_sop_smr
                                                                 ,sr_rfp_account
                                                                 ,account
                                                                 ,cbms_image img
                                                            WHERE
                                                                  sr_rfp_sop_smr.sr_account_group_id = sr_rfp_account.sr_rfp_account_id
                                                                  AND sr_rfp_account.sr_rfp_id = @rfp_id
                                                                  AND is_bid_group = 0
                                                                  AND sr_rfp_account.is_deleted = 0
                                                                  AND sr_rfp_account.account_id = account.account_id
                                                                  AND sr_rfp_sop_smr.cbms_image_id = img.cbms_image_id
                                                                  AND img.cbms_image_type_id = 1200 --//for smr upload.
                                                                  AND account.site_id = @siteId
	
                                                            SELECT
                                                                  @account = @account + count(DISTINCT sr_rfp_bid_group_id)
                                                            FROM
                                                                  sr_rfp_sop_smr
                                                                 ,sr_rfp_account
                                                                 ,account
                                                                 ,cbms_image img
                                                            WHERE
                                                                  sr_rfp_sop_smr.sr_account_group_id = sr_rfp_account.sr_rfp_bid_group_id
                                                                  AND sr_rfp_account.sr_rfp_id = @rfp_id
                                                                  AND is_bid_group = 1
                                                                  AND sr_rfp_account.is_deleted = 0
                                                                  AND sr_rfp_account.account_id = account.account_id
                                                                  AND sr_rfp_sop_smr.cbms_image_id = img.cbms_image_id
                                                                  AND img.cbms_image_type_id = 1200 --//for smr upload.
                                                                  AND account.site_id = @siteId
	
                                                            SET @smruploadedaccountcount = ( SELECT @account )
                                                            IF ( @smruploadedaccountcount ) = 0 
                                                                  BEGIN  --if9
                                                                        DELETE FROM
                                                                              SSO_PROJECT_STEP
                                                                        WHERE
                                                                              sso_project_id = @projectId
                                                                              AND step_no = 3
                                                                  END    --if9
                                                      END   --if8

                                          END --else of if 6
                              END --//if1

                        DELETE FROM
                              @temp
                        WHERE
                              site_id = @siteId
                  END --//while

      END --//procedure
END

;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_SEND_RFP_STATUS_P] TO [CBMSApplication]
GO
