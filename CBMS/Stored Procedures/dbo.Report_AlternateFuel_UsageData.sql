
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/********
NAME:
		Report_AlternateFuel_UsageData

DESCRIPTION:
	Created this stored proc for SSRS report 'Account_Usage_Report' under Cost & Usage.
	This report would allow users to get the cost & usage data through Reporting Services for these Other Services (not EP & NG)

INPUT PARAMETERS:
    Name			    DataType		Default	Description
------------------------------------------------------------
    @Client_id_List	    INT
    @Begin_Date	    DATE
    @End_Date		    DATE
    @Services		    VARCHAR(50)
    @Account_Status	    INT
    @Site_Status	    INT    	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC dbo.Report_AlternateFuel_UsageData '11231','09/01/2010','01/27/2011',67,0,0,1631
	
AUTHOR INITIALS:
INITIALS	  NAME
------------------------------------------------------------
SKA		  Shobhit Kumar Agrawal
AP		  Athmaram Pabbathi

MODIFICATION
INITIALS	  DATE	    MODIFICATION
------------------------------------------------------------
SKA		  11/07/2010  Created
SKA		  11/30/2010  Qualified all the columns and tables.
SKA		  12/03/2010  Rewrote the object to move the Pivot from sp to SSRS Report itself
SKA		  01/27/2011  Added one more input parameter for UOM Conversion
SSR		  02/02/2011  Added Meta.Date_dim in final Query to get all Month.	
AP		  2012-05-23  Addnl Data Changes
					   - Added Client_Hier_Id in the joins of CUAD and CTE
					   - Removed TotalAltFuelAccounts_CTE and used temporary table
					   - Removed dbo.Currency_Unit table from AltFuelUsage_CTE 
					   - Used Temp table instead of AltFuelUsage_CTE 
					   - Removed CTE_Final_Month_wise and modified the logic accordingly
*/
CREATE PROCEDURE dbo.Report_AlternateFuel_UsageData
      ( 
       @Client_id_List VARCHAR(MAX)
      ,@begin_date DATETIME
      ,@end_date DATETIME
      ,@Commodity_id INT
      ,@Account_Status INT
      ,@Site_Status INT
      ,@Default_UOM_Entity_Type_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;

      CREATE TABLE #TotalAltFuelAccounts
            ( 
             Client_Name VARCHAR(200)
            ,Sitegroup_Name VARCHAR(200)
            ,Site_Name VARCHAR(200)
            ,State_Name VARCHAR(200)
            ,Country VARCHAR(200)
            ,REGION_NAME VARCHAR(200)
            ,Account_Number VARCHAR(50)
            ,Vendor_Name VARCHAR(200)
            ,Account_Id INT
            ,Service_Level VARCHAR(200)
            ,Consumption_Level VARCHAR(200)
            ,DataEntry_Only VARCHAR(10)
            ,Account_Status VARCHAR(10)
            ,Site_Status VARCHAR(10)
            ,Account_Type VARCHAR(50)
            ,Commodity_Name VARCHAR(200)
            ,Commodity_Id INT
            ,Client_Hier_Id INT
            ,PRIMARY KEY CLUSTERED ( Client_Hier_Id, Account_Id, Commodity_Id ) )
                  
      INSERT      INTO #TotalAltFuelAccounts
                  ( 
                   Client_Name
                  ,Sitegroup_Name
                  ,Site_Name
                  ,State_Name
                  ,Country
                  ,REGION_NAME
                  ,Account_Number
                  ,Vendor_Name
                  ,Account_Id
                  ,Service_Level
                  ,Consumption_Level
                  ,DataEntry_Only
                  ,Account_Status
                  ,Site_Status
                  ,Account_Type
                  ,Commodity_Name
                  ,Commodity_Id
                  ,Client_Hier_Id )
                  SELECT
                        ch.Client_Name
                       ,ch.Sitegroup_Name
                       ,ch.Site_name
                       ,ca.Meter_State_Name AS State_Name
                       ,ca.Meter_Country_Name AS Country
                       ,ch.REGION_NAME
                       ,ca.Account_Number
                       ,ca.Account_Vendor_Name AS Vendor_Name
                       ,ca.Account_Id
                       ,e.ENTITY_NAME AS Service_Level
                       ,vcl.Consumption_Level_Desc AS Consumption_Level
                       ,case WHEN ca.Account_Is_Data_Entry_Only = 1 THEN 'Yes'
                             ELSE 'No'
                        END AS DataEntry_Only
                       ,case WHEN ca.Account_Not_Managed = 1 THEN 'No'
                             ELSE 'Yes'
                        END AS account_status
                       ,case WHEN ch.Site_Not_Managed = 1 THEN 'No'
                             ELSE 'Yes'
                        END AS site_status
                       ,ca.Account_Type
                       ,com.Commodity_Name
                       ,com.Commodity_Id
                       ,ch.Client_Hier_Id
                  FROM
                        Core.Client_Hier ch
                        JOIN dbo.ufn_split(@Client_id_List, ',') ufn_Client_tmp
                              ON cast(ufn_Client_tmp.Segments AS INT) = ch.CLIENT_ID
                        JOIN Core.Client_Hier_Account ca
                              ON ca.Client_Hier_Id = ch.Client_Hier_Id
                        JOIN dbo.Commodity com
                              ON com.Commodity_Id = ca.Commodity_Id
                        JOIN dbo.ENTITY e
                              ON e.ENTITY_ID = ca.Account_Service_level_Cd
                        JOIN dbo.Account_Variance_Consumption_Level avcl
                              ON avcl.ACCOUNT_ID = ca.Account_Id
                        JOIN dbo.Variance_Consumption_Level vcl
                              ON vcl.Commodity_Id = ca.Commodity_Id
                                 AND vcl.Variance_Consumption_Level_Id = avcl.Variance_Consumption_Level_Id
                  WHERE
                        com.Commodity_Name NOT IN ( 'Electric Power', 'Natural Gas' )
                        AND ch.Site_Id > 0
                        AND com.Commodity_Id = @Commodity_Id
                        AND ( @Account_Status = -1
                              OR ca.Account_Not_Managed = @Account_Status )
                        AND ( @Site_Status = -1
                              OR ch.Site_Not_Managed = @Site_Status )
                  GROUP BY
                        ch.Client_Name
                       ,ch.Sitegroup_Name
                       ,ch.Site_name
                       ,ca.Meter_State_Name
                       ,ca.Meter_Country_Name
                       ,ch.REGION_NAME
                       ,ca.Account_Number
                       ,ca.Account_Vendor_Name
                       ,ca.Account_Id
                       ,e.ENTITY_NAME
                       ,vcl.Consumption_Level_Desc
                       ,case WHEN ca.Account_Is_Data_Entry_Only = 1 THEN 'Yes'
                             ELSE 'No'
                        END
                       ,case WHEN ca.Account_Not_Managed = 1 THEN 'No'
                             ELSE 'Yes'
                        END
                       ,case WHEN ch.Site_Not_Managed = 1 THEN 'No'
                             ELSE 'Yes'
                        END
                       ,ca.Account_Type
                       ,com.Commodity_Name
                       ,com.Commodity_Id
                       ,ch.Client_Hier_Id;

      WITH  AltFuelUsage_CTE
              AS ( SELECT
                        accts.Client_Name
                       ,accts.Sitegroup_Name
                       ,accts.Site_name
                       ,accts.Site_Status
                       ,accts.State_Name
                       ,accts.Country
                       ,accts.Region_Name
                       ,accts.Account_Number
                       ,accts.Account_Status
                       ,accts.Vendor_Name
                       ,accts.Service_Level
                       ,accts.Consumption_Level
                       ,accts.DataEntry_Only
                       ,accts.Account_Type
                       ,accts.Commodity_Name
                       ,ad.bucket_value AS Usage
                       ,ad.ACCOUNT_ID
                       ,ad.Service_Month
                       ,e.ENTITY_NAME [UofM]
                       ,cuc.CONVERSION_FACTOR
                       ,cuc.CONVERTED_UNIT_ID
                       ,cuc.BASE_UNIT_ID
                       ,ad.Client_Hier_Id
                       ,( ad.Bucket_Value * cuc.CONVERSION_FACTOR ) AS UsageConvertedValue
                   FROM
                        #TotalAltFuelAccounts accts
                        JOIN dbo.Cost_Usage_Account_Dtl ad
                              ON accts.Account_Id = ad.Account_Id
                                 AND accts.Client_Hier_Id = ad.Client_Hier_Id
                        JOIN dbo.Bucket_Master bm
                              ON bm.Bucket_Master_Id = ad.Bucket_Master_Id
                                 AND bm.Commodity_Id = accts.Commodity_Id
                        JOIN dbo.ENTITY e
                              ON e.entity_id = @Default_UOM_Entity_Type_Id
                        JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                              ON cuc.BASE_UNIT_ID = ad.UOM_Type_Id
                                 AND cuc.CONVERTED_UNIT_ID = @Default_UOM_Entity_Type_Id
                   WHERE
                        bm.Bucket_Name = 'Volume'
                        AND ad.Service_Month BETWEEN @Begin_Date
                                             AND     @end_date)
            SELECT
                  cte_fmw.Client_Name
                 ,cte_fmw.Sitegroup_Name
                 ,cte_fmw.Site_name
                 ,cte_fmw.Site_Status
                 ,cte_fmw.State_Name
                 ,cte_fmw.Country
                 ,cte_fmw.Region_Name
                 ,cte_fmw.Account_Number
                 ,cte_fmw.Account_Status
                 ,cte_fmw.Vendor_Name
                 ,cte_fmw.Service_Level
                 ,cte_fmw.Consumption_Level
                 ,cte_fmw.DataEntry_Only
                 ,cte_fmw.Account_Type
                 ,cte_fmw.Commodity_Name
                 ,cte_fmw.UofM
                 ,cte_fmw.CONVERSION_FACTOR
                 ,cte_fmw.[Usage]
                 ,dd.Date_D [Service_Month]
                 ,cte_fmw.Account_Id
                 ,cte_fmw.UsageConvertedValue
            FROM
                  meta.Date_Dim dd
                  LEFT JOIN AltFuelUsage_CTE cte_fmw
                        ON dd.date_d = cte_fmw.Service_Month
            WHERE
                  dd.Date_D BETWEEN @begin_date AND @end_date
            ORDER BY
                  dd.Date_D
                  
      DROP TABLE #TotalAltFuelAccounts
                
END

;
GO

GRANT EXECUTE ON  [dbo].[Report_AlternateFuel_UsageData] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_AlternateFuel_UsageData] TO [CBMSApplication]
GO
