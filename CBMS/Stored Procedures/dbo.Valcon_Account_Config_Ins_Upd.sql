SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Valcon_Account_Config_Ins_Upd      
              
Description:              
        To insert Data into Contract_Recalc_Config table.              
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
     @Contract_Id						INT
     @Commodity_Id						INT
     @Start_Dt							DATE
     @End_Dt							DATE				NULL
     @Invoice_Recalc_Type_Cd			INT
     @User_Info_Id						INT    
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                
	USE CBMS

	BEGIN TRAN  

	SELECT * FROM Valcon_Account_Config WHERE Contract_Id =1

    EXEC dbo.Valcon_Account_Config_Ins_Upd 
       @Account_Id =1
         , @Contract_Id =1
		 ,@Source_Cd = 1
         , @Is_Config_Complete =1
         , @Comment_Id  = NULL
         , @User_Info_Id =49
    
	SELECT * FROM Valcon_Account_Config WHERE Contract_Id =1
	ROLLBACK TRAN             
             
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2019-06-26		Created For Add contract.   
             
******/

CREATE PROCEDURE [dbo].[Valcon_Account_Config_Ins_Upd]
    (
        @Account_Id INT
        , @Contract_Id INT
        , @Source_Cd INT
        , @Is_Config_Complete BIT
        , @Comment_Id INT = NULL
        , @User_Info_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        UPDATE
            vac
        SET
            vac.Is_Config_Complete = @Is_Config_Complete
            , vac.Source_Cd = @Source_Cd
            , vac.Comment_Id = @Comment_Id
            , vac.Updated_User_Id = @User_Info_Id
            , vac.Last_Change_Ts = GETDATE()
        FROM
            dbo.Valcon_Account_Config vac
        WHERE
            vac.Account_Id = @Account_Id
            AND vac.Contract_Id = @Contract_Id;


        INSERT INTO dbo.Valcon_Account_Config
             (
                 Account_Id
                 , Contract_Id
                 , Source_Cd
                 , Is_Config_Complete
                 , Comment_Id
                 , Created_Ts
                 , Created_User_Id
                 , Updated_User_Id
                 , Last_Change_Ts
             )
        SELECT
            @Account_Id
            , @Contract_Id
            , @Source_Cd
            , @Is_Config_Complete
            , @Comment_Id
            , GETDATE()
            , @User_Info_Id
            , @User_Info_Id
            , GETDATE()
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                dbo.Valcon_Account_Config vac
                           WHERE
                                vac.Account_Id = @Account_Id
                                AND vac.Contract_Id = @Contract_Id);




    END;



GO
GRANT EXECUTE ON  [dbo].[Valcon_Account_Config_Ins_Upd] TO [CBMSApplication]
GO
