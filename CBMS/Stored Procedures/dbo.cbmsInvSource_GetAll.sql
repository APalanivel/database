SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsInvSource_GetAll

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE         PROCEDURE [dbo].[cbmsInvSource_GetAll]
	( @MyAccountId int 
	)
AS
BEGIN

	   select s.inv_source_id
		, s.inv_source_type_id
		, s.inv_source_label
		, s.is_active
		, s.source_folder_path
		, s.archive_folder_path
		, s.include_sub_folders
		, s.preserve_sub_folders
		, s.query_fax_database
		, s.email_server
		, s.email_uid
		, s.email_pwd
		, s.target_folder_path
	     from inv_source s with (nolock)
	     join entity st with (nolock) on st.entity_id = s.inv_source_type_id
	    where s.show_in_report = 1

	 order by s.inv_source_label

END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvSource_GetAll] TO [CBMSApplication]
GO
