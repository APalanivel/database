SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Workflow].[Workflow_Get_Letf_Nav_Search_Filter_Count]
(
@Logged_In_User						  INT,
@MODULE_ID							  INT,
@Workflow_Queue_Saved_Filter_QueryId  INT,
@TOTAL_COUNT						  INT OUTPUT
)
AS
BEGIN

DECLARE
  -- Add the parameters for the stored procedure here	
	--@MODULE_ID								INT,    
   -- @Workflow_Queue_Saved_Filter_Query_Id	INT = NULL,    
   -- @Logged_In_User							INT,    
    @Queue_id								VARCHAR(MAX) = NULL, ---  User selected from drop down Id 49,1,2,3      
    @Exception_Type							VARCHAR(MAX) = NULL,    
    @Account_Number							VARCHAR(500) = NULL,    
    @Client									VARCHAR(500) = NULL,    
    @Site									VARCHAR(500) = NULL,    
    @Country								VARCHAR(500) = NULL,    
    @State									VARCHAR(500) = NULL,    
    @City									VARCHAR(500) = NULL,    
    @Commodity								VARCHAR(500) = NULL,    
    @Invoice_ID								VARCHAR(500) = NULL,    
    @Priority								VARCHAR(500) = NULL, --- Yes      
    @Exception_Status						VARCHAR(500) = NULL,    
    @Comments								VARCHAR(500) = NULL,    
    @Start_Date_in_Queue					DATETIME = NULL,    
    @End_Date_in_Queue						DATETIME = NULL,    
    @Start_Date_in_CBMS						DATETIME = NULL,    
    @End_Date_in_CBMS						DATETIME = NULL,    
    @Data_Source							VARCHAR(100) = NULL,    
    @Vendor									VARCHAR(100) = NULL,    
    @Vendor_Type							VARCHAR(100) = NULL,    
    @Month									DATETIME = NULL,    
    @Filename								VARCHAR(100) = NULL        
    --@ONLY_COUNT_NEEDED						INT = 1
	
	BEGIN TRY 		
    
	SELECT DISTINCT          
              --SF.Search_Filter_Id,    
              WQSF.Workflow_Queue_Search_Filter_Id,    
              SF.Filter_Name,    
              WQSFP.Param_Name,              
              --WQSF.Workflow_Queue_Id,    
              WQSFQV.Selected_Value,    
              WQSFQ.Workflow_Queue_Saved_Filter_Query_Id,    
              WQSFQ.Saved_Filter_Name    
INTO #Search_Filter_Count 
FROM Workflow.Search_Filter AS SF    
INNER JOIN Workflow.Workflow_Queue_Search_Filter AS WQSF ON SF.Search_Filter_Id = WQSF.Search_Filter_Id    
INNER JOIN Workflow.Workflow_Queue_Saved_Filter_Query_Value AS WQSFQV ON WQSF.Workflow_Queue_Search_Filter_Id = WQSFQV.Workflow_Queue_Search_Filter_Id    
INNER JOIN Workflow.Workflow_Queue_Saved_Filter_Query AS WQSFQ ON WQSFQV.Workflow_Queue_Saved_Filter_Query_Id = WQSFQ.Workflow_Queue_Saved_Filter_Query_Id    
INNER JOIN Workflow.Workflow_Sub_Queue AS WSQ ON WSQ.Workflow_Sub_Queue_Id=WQSFQ.Workflow_Sub_Queue_Id AND WSQ.Workflow_Sub_Queue_Name NOT IN ('All Open Exceptions','Assigned','UnAssigned')   
INNER JOIN workflow.Workflow_Queue_Search_Filter_Param AS WQSFP ON WQSF.Workflow_Queue_Search_Filter_Id = WQSFP.Workflow_Queue_Search_Filter_Id  
WHERE   WQSFQ.Workflow_Queue_Saved_Filter_Query_Id=@Workflow_Queue_Saved_Filter_QueryId
ORDER BY WQSFQ.Workflow_Queue_Saved_Filter_Query_Id,WQSFQ.Saved_Filter_Name 

--SET @MODULE_ID	=	1						
--SET @Logged_In_User	=	@User_Id

				
SELECT @Queue_id= CASE WHEN SFC.Selected_Value	='CURRENT_USER' THEN @Logged_In_User ELSE 	SFC.Selected_Value END	FROM #Search_Filter_Count AS SFC WHERE SFC.Param_Name='@Queue_id'	
SELECT @Exception_Type= SFC.Selected_Value			FROM #Search_Filter_Count AS SFC WHERE SFC.Param_Name='@Exception_Type'			
SELECT @Account_Number= SFC.Selected_Value			FROM #Search_Filter_Count AS SFC WHERE SFC.Param_Name='@Account_Number'			
SELECT @Client= SFC.Selected_Value					FROM #Search_Filter_Count AS SFC WHERE SFC.Param_Name='@Client'					
SELECT @Site= SFC.Selected_Value					FROM #Search_Filter_Count AS SFC WHERE SFC.Param_Name='@Site'					
SELECT @Country= SFC.Selected_Value					FROM #Search_Filter_Count AS SFC WHERE SFC.Param_Name='@Country'				
SELECT @State= SFC.Selected_Value					FROM #Search_Filter_Count AS SFC WHERE SFC.Param_Name='@State'					
SELECT @City= SFC.Selected_Value					FROM #Search_Filter_Count AS SFC WHERE SFC.Param_Name='@City'					
SELECT @Commodity= SFC.Selected_Value				FROM #Search_Filter_Count AS SFC WHERE SFC.Param_Name='@Commodity'				
SELECT @Invoice_ID= SFC.Selected_Value				FROM #Search_Filter_Count AS SFC WHERE SFC.Param_Name='@Invoice_ID'				
SELECT @Priority= CASE WHEN SFC.Selected_Value='YES' THEN '1' ELSE SFC.Selected_Value END	FROM #Search_Filter_Count AS SFC WHERE SFC.Param_Name='@Priority'				
SELECT @Comments= SFC.Selected_Value				FROM #Search_Filter_Count AS SFC WHERE SFC.Param_Name='@Comments'				
SELECT @Start_Date_in_Queue= SFC.Selected_Value		FROM #Search_Filter_Count AS SFC WHERE SFC.Param_Name='@Start_Date_in_Queue'	
SELECT @End_Date_in_Queue= SFC.Selected_Value		FROM #Search_Filter_Count AS SFC WHERE SFC.Param_Name='@End_Date_in_Queue'		
SELECT @Start_Date_in_CBMS= SFC.Selected_Value		FROM #Search_Filter_Count AS SFC WHERE SFC.Param_Name='@Start_Date_in_CBMS'		
SELECT @End_Date_in_CBMS= SFC.Selected_Value		FROM #Search_Filter_Count AS SFC WHERE SFC.Param_Name='@End_Date_in_CBMS'		
SELECT @Data_Source= SFC.Selected_Value				FROM #Search_Filter_Count AS SFC WHERE SFC.Param_Name='@Data_Source'							
SELECT @Vendor= SFC.Selected_Value					FROM #Search_Filter_Count AS SFC WHERE SFC.Param_Name='@Vendor'								
SELECT @Month= SFC.Selected_Value					FROM #Search_Filter_Count AS SFC WHERE SFC.Param_Name='@Month'								
SELECT @Filename= SFC.Selected_Value				FROM #Search_Filter_Count AS SFC WHERE SFC.Param_Name='@Filename'								
					
	

    DECLARE @PROC_NAME VARCHAR(100) = 'Workflow_Get_Letf_Nav_Search_Filter_Count',    
            @INPUT_PARAMS VARCHAR(1000),    
            @ERROR_LINE INT,    
            @ERROR_MESSAGE VARCHAR(3000),    
            @SQL NVARCHAR(MAX);    
    
       
    
        /* STEP 1 FORMAT THE INPUTS. IF INPUT IS COMMA SEPARATED, SPILT THE STRING USING BELOW FORMULA*/    
    
    
        IF (@Queue_id IS NOT NULL)    
        BEGIN    
            SELECT Segments AS User_info_id    
            INTO #temp_user_info    
            FROM dbo.ufn_split(@Queue_id, ',');    
        END;    
    
        IF (@Exception_Type IS NOT NULL)    
        BEGIN    
            SELECT Segments AS exception_type    
            INTO #temp_Exception_Type    
            FROM dbo.ufn_split(@Exception_Type, ',');    
        -- select * from #temp_Exception_Type      
        END;    
    
        IF (@State IS NOT NULL)    
        BEGIN    
            SELECT Segments AS state_id    
            INTO #temp_State    
            FROM dbo.ufn_split(@State, ',');    
        -- select * from #temp_State      
        END;    
    
    
        IF (@Country IS NOT NULL)    
        BEGIN    
            SELECT Segments AS country_id    
            INTO #temp_Country    
            FROM dbo.ufn_split(@Country, ',');    
        -- select * from #temp_Country      
        END;    
    
    
        IF (@Commodity IS NOT NULL)    
        BEGIN    
            SELECT Segments AS commodity_id    
            INTO #temp_Commodity    
            FROM dbo.ufn_split(@Commodity, ',');    
        -- select * from #temp_Commodity      
        END;    
    
    
        CREATE TABLE #DEFAULTEXCEPTIONS    
        (    
            UserGroupId INT NULL,    
            UserGroupName VARCHAR(300) NULL,    
            usergroupcount INT NULL,    
            systemgroupid INT NULL,    
            SystemGroupCount INT NULL,    
            ubmid INT NULL,    
            ubmaccountcode VARCHAR(200) NULL,    
            [RN] [BIGINT] NULL,    
            [CU_INVOICE_ID] [INT] NULL,    
            [ACCOUNT] [VARCHAR](MAX) NULL,    
            [EXCEPTION_TYPE] [VARCHAR](MAX) NULL,    
            [EXCEPTION_STATUS_TYPE] [VARCHAR](MAX) NULL,    
            [ASSIGNEE] [VARCHAR](MAX) NULL,    
            [CLIENTID] INT NULL,    
            [CLIENT] [VARCHAR](MAX) NULL,    
			[IsClientDataFromLabel] BIT NULL ,    
            [COMMODITY] [VARCHAR](MAX) NULL,    
            [DATA SOURCE] [VARCHAR](MAX) NULL,    
			[DATE_IN_QUEUE] [DATETIME] NULL,    
            [DATE_IN_CBMS] [DATETIME] NULL,    
            [FILEID] INT NULL,    
            [FILENAME] [VARCHAR](MAX) NULL,    
            [SERVICE_MONTH] [VARCHAR](MAX) NULL,    
            [SITE] [VARCHAR](MAX) NULL,    
            [COUNTRY] [NVARCHAR](MAX) NULL,    
            [STATE] [NVARCHAR](MAX) NULL,    
            [VENDOR] [NVARCHAR](MAX) NULL,    
            [VENDOR_TYPE] [NVARCHAR](MAX) NULL,    
            [COMMENTS] VARCHAR(MAX) NULL    
        );    
    
   
        --@Queue_id = value @Queue_id      
    
        SET @SQL    
            = '   insert #DEFAULTEXCEPTIONS      
     SELECT       
   NULL,      
   null,    
   null,     
   null,    
   null,    
    CI.UBM_ID,    
    CI.UBM_ACCOUNT_CODE,    
     ROW_NUMBER ()OVER(PARTITION BY CUEX.CU_INVOICE_ID ORDER BY CUEX.CU_INVOICE_ID ASC) AS RN,       
   CUEX.CU_INVOICE_ID      
    , ( CASE WHEN CUEX.EXCEPTION_TYPE = ' + '''' + 'WRONG ACCOUNT' + ''''    
              + ' THEN NULL      
             WHEN AG.ACCOUNT_GROUP_ID IS NULL      
                   THEN ISNULL(CHA.ACCOUNT_NUMBER, CUEX.UBM_ACCOUNT_NUMBER)      
             ELSE  COALESCE(AG.GROUP_BILLING_NUMBER, CHA.ACCOUNT_NUMBER, CUEX.UBM_ACCOUNT_NUMBER)      
        END ) AS ACCOUNT      
    , CUEX.EXCEPTION_TYPE      
    , CUEX.EXCEPTION_STATUS_TYPE       
    , UI.FIRST_NAME + ' + '''' + ' ' + ''''    
              + '+ UI.LAST_NAME ASSIGNEE      
   , CASE WHEN CUEX.CLIENT_HIER_ID IS NOT NULL      
                 THEN CH.CLIENT_HIER_ID      
           ELSE  null      
      END CLIENT_ID      
    , CASE WHEN CUEX.CLIENT_HIER_ID IS NOT NULL      
                 THEN CH.CLIENT_NAME      
           ELSE  isnull(CUEX.UBM_CLIENT_NAME , INV_LBL.CLIENT_NAME)    
      END CLIENT       
 , CASE WHEN CUEX.CLIENT_HIER_ID IS NOT NULL then  0    
           when  CUEX.UBM_CLIENT_NAME is not null then 0    
    when  INV_LBL.CLIENT_NAME is not null then 1     
    else 0    
      END IsClientDataFromLabel     
    , COM.COMMODITY_NAME COMMODITY      
    , UBM.UBM_NAME [DATA SOURCE]      
    , CUEX.DATE_IN_QUEUE      
    , CUEX.DATE_IN_CBMS      
 , CUEX.CBMS_IMAGE_ID [FILEID]    
    , CUEX.CBMS_DOC_ID [FILENAME]      
    , ( CASE WHEN CUEX.EXCEPTION_TYPE = ' + '''' + 'WRONG MONTH' + ''''    
              + 'THEN NULL      
             WHEN AG.ACCOUNT_GROUP_ID IS NULL      
                   THEN CONVERT(VARCHAR(10), SM.SERVICE_MONTH, 101)      
             ELSE  CUEX.SERVICE_MONTH      
        END ) AS SERVICE_MONTH      
    , CASE WHEN CUEX.CLIENT_HIER_ID IS NOT NULL      
                 THEN CH.SITE_NAME      
           ELSE  CUEX.UBM_SITE_NAME      
      END [SITE]      
    ,isnull( C.country_name,' + '''' + '''' + ') as country,      
 isnull(S.state_name,' + '''' + ''''    
              + ') as state,      
  CASE WHEN  CI.ACCOUNT_GROUP_ID IS NULL THEN CHA.Account_Vendor_Name      
     WHEN CI.ACCOUNT_GROUP_ID IS NOT NULL THEN V.VENDOR_NAME       
     ELSE INV_LBL.VENDOR_NAME      
     end  AS VENDOR       
 ,  CASE WHEN  CI.ACCOUNT_GROUP_ID IS NULL THEN AVT.ENTITY_NAME      
   WHEN CI.ACCOUNT_GROUP_ID IS NOT NULL THEN VT.ENTITY_NAME       
     ELSE ' + '''' + ''''    
              + '      
     end AS VENDOR_TYPE       
    , NULL       
FROM  DBO.CU_EXCEPTION_DENORM CUEX      
  JOIN dbo.QUEUE q ON q.QUEUE_ID = CUEX.QUEUE_ID      
    INNER JOIN dbo.Entity qt ON qt.ENTITY_ID = q.QUEUE_TYPE_ID      
           JOIN      
      DBO.CU_INVOICE CI      
            ON CI.CU_INVOICE_ID = CUEX.CU_INVOICE_ID      
      LEFT JOIN WORKFLOW.CU_INVOICE_ATTRIBUTE CIA       
 ON  CIA.CU_INVOICE_ID = cuex.CU_INVOICE_ID      
      LEFT JOIN      
      DBO.CU_INVOICE_SERVICE_MONTH SM      
            ON SM.CU_INVOICE_ID = CUEX.CU_INVOICE_ID      
               AND SM.ACCOUNT_ID = CUEX.ACCOUNT_ID      
      LEFT OUTER JOIN      
      DBO.USER_INFO UI      
            ON UI.QUEUE_ID = CUEX.QUEUE_ID      
      LEFT JOIN      
      DBO.CU_EXCEPTION_TYPE CET      
            ON CUEX.EXCEPTION_TYPE = CET.EXCEPTION_TYPE      
      LEFT JOIN      
      DBO.ACCOUNT_GROUP AG      
            ON AG.ACCOUNT_GROUP_ID = CI.ACCOUNT_GROUP_ID      
      LEFT JOIN      
      (DBO.VENDOR V      
       INNER JOIN      
       DBO.ENTITY VT      
             ON VT.ENTITY_ID = V.VENDOR_TYPE_ID)      
            ON V.VENDOR_ID = AG.VENDOR_ID      
      
      LEFT JOIN      
      CORE.CLIENT_HIER_ACCOUNT CHA      
            ON CHA.CLIENT_HIER_ID = CUEX.CLIENT_HIER_ID      
               AND CHA.ACCOUNT_ID = CUEX.ACCOUNT_ID       
       LEFT JOIN      
      (DBO.VENDOR AV      
       INNER JOIN      
       DBO.ENTITY AVT      
             ON AVT.ENTITY_ID = AV.VENDOR_TYPE_ID)      
            ON AV.VENDOR_ID = CHA.Account_Vendor_Id      
      LEFT JOIN      
      CORE.CLIENT_HIER CH      
            ON CH.CLIENT_HIER_ID = CUEX.CLIENT_HIER_ID       
 LEFT JOIN COUNTRY c      
            ON CH.COUNTRY_ID = C.COUNTRY_ID       
   LEFT JOIN state s      
            ON CH.state_id = s.state_id         
  LEFT JOIN      
      DBO.COMMODITY COM      
            ON COM.COMMODITY_ID = CHA.COMMODITY_ID      
      LEFT JOIN      
      DBO.CU_INVOICE_LABEL INV_LBL      
            ON CUEX.CU_INVOICE_ID = INV_LBL.CU_INVOICE_ID      
      LEFT JOIN      
      DBO.UBM UBM      
            ON UBM.UBM_ID = CI.UBM_ID' + CASE    
                                             WHEN @Country IS NOT NULL THEN    
                                                 ' JOIN #temp_country TC      
   ON TC.COUNTRY_ID = C.COUNTRY_ID'    
                                             ELSE    
                                                 ''    
                                         END + CASE    
                                                   WHEN @State IS NOT NULL THEN    
                                                       ' JOIN #temp_state TS      
   ON TS.state_id = S.state_id'    
                                                   ELSE    
                                                       ''    
                                               END    
              + ' OUTER APPLY      
      (     SELECT      
                        UAC.ACCOUNT_VENDOR_NAME + ' + '''' + ', ' + ''''    
              + 'FROM        CORE.CLIENT_HIER_ACCOUNT UAC      
                        INNER JOIN      
                        CORE.CLIENT_HIER_ACCOUNT SAC      
                              ON SAC.METER_ID = UAC.METER_ID      
            WHERE       UAC.ACCOUNT_TYPE = ' + '''' + 'UTILITY' + '''' + 'AND   SAC.ACCOUNT_TYPE = ' + ''''    
              + 'SUPPLIER' + '''' + 'AND   CHA.ACCOUNT_TYPE = ' + '''' + 'SUPPLIER' + ''''    
              + 'AND   SAC.ACCOUNT_ID = CHA.ACCOUNT_ID      
            GROUP BY    UAC.ACCOUNT_VENDOR_NAME      
            FOR XML PATH(' + '''' + ''''    
              + ')) UV(VENDOR_NAME)      
   WHERE CUEX.IS_MANUAL = 1      
      AND   CUEX.EXCEPTION_TYPE <> ' + '''' + 'FAILED RECALC' + ''''    
              + CASE    
                    WHEN @Account_Number IS NOT NULL THEN    
                        'and (CASE WHEN CUEX.EXCEPTION_TYPE = ' + '''' + 'WRONG ACCOUNT' + ''''    
                        + ' THEN NULL      
             WHEN AG.ACCOUNT_GROUP_ID IS NULL      
                   THEN ISNULL(CHA.ACCOUNT_NUMBER, CUEX.UBM_ACCOUNT_NUMBER)      
             ELSE  COALESCE(AG.GROUP_BILLING_NUMBER, CHA.ACCOUNT_NUMBER, CUEX.UBM_ACCOUNT_NUMBER)      
        END )  ='                                                                 + '''' + @Account_Number + ''''    
                    ELSE    
                        ''    
                END + CASE    
                          WHEN @Queue_id IS NOT NULL    
                               AND @Queue_id = '-1' THEN    
                              'AND qt.[Entity_Name] = ' + '''' + 'Private' + ''''    
                          WHEN @Queue_id IS NOT NULL    
                               AND @Queue_id = '0' THEN    
                              'AND qt.[Entity_Name] = ' + '''' + 'Public' + ''''    
                          WHEN @Queue_id IS NOT NULL    
                               AND @Queue_id != '-1'    
                               AND @Queue_id != '0' THEN    
            'and cuex.queue_id in (select user_info_id from #temp_user_info)'    
                          ELSE    
                              ''    
                      END    
              + CASE    
                    WHEN @Client IS NOT NULL THEN    
                        'and CASE WHEN CUEX.CLIENT_HIER_ID IS NOT NULL      
                 THEN CH.CLIENT_NAME      
           ELSE   isnull(CUEX.UBM_CLIENT_NAME , INV_LBL.CLIENT_NAME)      
      END =' + '''' + @Client + ''''    
                    ELSE    
                        ''    
                END + CASE    
                          WHEN @Commodity IS NOT NULL THEN    
                              'and com.Commodity_name in (select commodity_id from #temp_commodity)'    
                          ELSE    
                              ''    
                      END + CASE    
                                WHEN @Data_Source IS NOT NULL THEN    
                                    'and ubm.ubm_Name=' + '''' + @Data_Source + ''''    
                                ELSE    
                                    ''    
                            END    
              + CASE    
                    WHEN @Start_Date_in_CBMS IS NOT NULL THEN    
                        ' and cuex.Date_In_CBMS between ''' + CAST(@Start_Date_in_CBMS AS VARCHAR) + '''' + ' and '''    
                        + CAST(@End_Date_in_CBMS AS VARCHAR) + ''''    
                    ELSE    
                        ''    
                END    
              + CASE    
                    WHEN @Start_Date_in_Queue IS NOT NULL THEN    
                        'and cuex.DATE_IN_QUEUE between ''' + CAST(@Start_Date_in_Queue AS VARCHAR) + '''' + ' and '''    
                        + CAST(@End_Date_in_Queue AS VARCHAR) + ''''    
                    ELSE    
                        ''    
                END + CASE    
                          WHEN @Filename IS NOT NULL THEN    
                              'and CUEX.CBMS_DOC_ID=' + '''' + @Filename + ''''    
                          ELSE    
                              ''    
                      END + CASE    
                                WHEN @Invoice_ID IS NOT NULL THEN    
                                    'and cuex.CU_INVOICE_ID=' + @Invoice_ID    
                                ELSE    
                                    ''    
                            END + CASE    
                                      WHEN @Exception_Status IS NOT NULL THEN    
                                          'and CUEX.EXCEPTION_STATUS_TYPE=' + @Exception_Status    
                                      ELSE    
                                          ''    
                                  END    
              + CASE    
                    WHEN @Month IS NOT NULL THEN    
                        'and CASE WHEN CUEX.EXCEPTION_TYPE = ' + '''' + 'WRONG MONTH' + ''''    
                        + 'THEN NULL      
             WHEN AG.ACCOUNT_GROUP_ID IS NULL      
                   THEN CONVERT(VARCHAR(10), SM.SERVICE_MONTH, 101)      
             ELSE  CUEX.SERVICE_MONTH      
        END'    
                        + '=' + '''' + CONVERT(VARCHAR(10), @Month, 101) + ''''    
        ELSE    
                        ''    
                END    
              + CASE    
                    WHEN @Site IS NOT NULL THEN    
                        'and CASE WHEN CUEX.CLIENT_HIER_ID IS NOT NULL      
                 THEN CH.SITE_NAME      
           ELSE  CUEX.UBM_SITE_NAME      
      END =' + '''' + @Site + ''''    
                    ELSE    
                        ''    
                END + CASE    
                          WHEN @Exception_Type IS NOT NULL THEN    
                              'and cuex.EXCEPTION_TYPE in (select exception_type from #temp_exception_type)'    
                          ELSE    
                              ''    
                      END + CASE    
                                WHEN @City IS NOT NULL THEN    
                                    'and ch.city=' + '''' + @City + ''''    
                                ELSE    
                                    ''    
                            END + CASE    
                                  WHEN @Priority IS NOT NULL    
                                           AND @Priority = 'Yes' THEN    
                                          'and (CIA.CU_INVOICE_ID IS NOT NULL AND CIA.Is_Priority =1)'    
                                      WHEN @Priority IS NOT NULL    
                                           AND @Priority = 'No' THEN    
                                          'AND ( CIA.CU_INVOICE_ID IS NULL OR CIA.Is_Priority =0)'    
                                      ELSE    
                                          ''    
                                  END    
              + CASE    
                    WHEN @Vendor IS NOT NULL THEN    
                        'and  COALESCE(CASE WHEN VT.ENTITY_NAME = ' + '''' + 'UTILITY' + ''''    
                        + 'THEN V.VENDOR_NAME      
               END      
             , CASE WHEN CHA.ACCOUNT_TYPE = '    
                        + '''' + 'UTILITY' + ''''    
                        + 'THEN CHA.ACCOUNT_VENDOR_NAME      
                    WHEN CHA.ACCOUNT_TYPE = '    
                        + '''' + 'SUPPLIER' + ''''    
                        + 'THEN LEFT(UV.VENDOR_NAME, LEN(UV.VENDOR_NAME) - 1)      
               END      
             , INV_LBL.VENDOR_NAME)='    
                        + '''' + @Vendor + ''''    
                    ELSE    
                        ''    
                END    
              + CASE    
                    WHEN @Vendor_Type IS NOT NULL THEN    
                        'and CASE WHEN  CI.ACCOUNT_GROUP_ID IS NULL THEN AVT.ENTITY_NAME      
      WHEN CI.ACCOUNT_GROUP_ID IS NOT NULL THEN VT.ENTITY_NAME       
     ELSE ' + '''' + '''' + '      
     end=' + '''' + @Vendor_Type + ''''    
                    ELSE    
                        ''    
                END;    
    
    --SELECT @sql     
        EXECUTE sp_executesql @SQL;    
    
        
    
        DELETE FROM #DEFAULTEXCEPTIONS    
        WHERE RN <> 1;    
    
       
    
    
    
        SET @TOTAL_COUNT =    
        (    
            SELECT COUNT(1) FROM #DEFAULTEXCEPTIONS    
        );    
        --IF ONLY COUNT IS NEEDED THEN RETURN THE SP, NO NEED TO REST OF THE OPERATION    
    
    
        ---STEP 1 UPDATE THE SYSTEM GROUP, GROUP THE INVOICES      
    
       
        DROP TABLE #DEFAULTEXCEPTIONS;    
		DROP TABLE #Search_Filter_Count

    END TRY    
    BEGIN CATCH    
    
    
        -- Entry made to the logging SP to capture the errors.      
        SELECT @ERROR_LINE = ERROR_LINE(),    
               @ERROR_MESSAGE = ERROR_MESSAGE();    
    
        INSERT INTO StoredProc_Error_Log    
        (    
            StoredProc_Name,    
            Error_Line,    
            Error_message,    
            Input_Params    
        )    
        VALUES    
        (@PROC_NAME, @ERROR_LINE, @ERROR_MESSAGE, @INPUT_PARAMS);    
    
        RETURN -99;    
    END CATCH;    
END;    

GO
GRANT EXECUTE ON  [Workflow].[Workflow_Get_Letf_Nav_Search_Filter_Count] TO [CBMSApplication]
GO
