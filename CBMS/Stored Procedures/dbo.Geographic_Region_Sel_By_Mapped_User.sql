SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    dbo.Geographic_Region_Sel_By_Mapped_User
   
    
DESCRIPTION:   
   
  This procedure to get summary page details Deal Ticket Approval Queue.
    
INPUT PARAMETERS:    
      Name          DataType       Default        Description    
-----------------------------------------------------------------------------    
	 @Client_Id		INT
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec dbo.Geographic_Region_Sel_By_Mapped_User 112
	Exec dbo.Geographic_Region_Sel_By_Mapped_User 49
	Exec dbo.Geographic_Region_Sel_By_Mapped_User 8510
	
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	NR          Narayana Reddy
    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	NR          2019-01-10	GRM Proejct.
     
    
******/

CREATE PROC [dbo].[Geographic_Region_Sel_By_Mapped_User]
      ( 
       @User_Info_Id INT = NULL )
AS 
BEGIN
      SET NOCOUNT ON;
      
      SELECT
            grp.Geographic_Region_Id
           ,gr.Geographic_Region_Name
      FROM
            dbo.Geographic_Region_Permission_Info_Map grp
            INNER JOIN dbo.GROUP_INFO_PERMISSION_INFO_MAP gipi
                  ON grp.Permission_Info_Id = gipi.PERMISSION_INFO_ID
            INNER JOIN dbo.USER_INFO_GROUP_INFO_MAP uigi
                  ON gipi.GROUP_INFO_ID = uigi.GROUP_INFO_ID
            INNER JOIN dbo.Geographic_Region gr
                  ON grp.Geographic_Region_Id = gr.Geographic_Region_Id
      WHERE
            @User_Info_Id IS NULL
            OR uigi.USER_INFO_ID = @User_Info_Id
      GROUP BY
            grp.Geographic_Region_Id
           ,gr.Geographic_Region_Name
                                   
END;

GO
GRANT EXECUTE ON  [dbo].[Geographic_Region_Sel_By_Mapped_User] TO [CBMSApplication]
GO
