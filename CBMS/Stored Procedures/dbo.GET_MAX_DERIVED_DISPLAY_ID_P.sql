SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE dbo.GET_MAX_DERIVED_DISPLAY_ID_P
	@rateId	INT
AS
BEGIN

	SET NOCOUNT ON
	
	SELECT MAX(CONVERT(INT,(SUBSTRING(cm.charge_display_id,2,LEN(cm.charge_display_id)))))
	FROM dbo.charge_master cm INNER JOIN dbo.Entity e ON e.entity_id = cm.charge_type_id
	WHERE cm.charge_parent_id= @rateId
		AND cm.charge_parent_type_id = 96
		AND e.entity_name ='Derived'
		AND e.entity_type =113

END
GO
GRANT EXECUTE ON  [dbo].[GET_MAX_DERIVED_DISPLAY_ID_P] TO [CBMSApplication]
GO
