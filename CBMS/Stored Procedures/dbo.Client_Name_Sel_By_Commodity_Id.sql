SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******    
NAME:    
	[dbo].[Client_Name_Sel_By_Commodity_Id]  

DESCRIPTION:  
	Lists all Client_Names that use the Commodity_Id Input

INPUT PARAMETERS:    
Name		     DataType	Default Description    
------------------------------------------------------------    
@Commodity_ID		INT  
          
OUTPUT PARAMETERS:    
Name		     DataType	Default Description    
------------------------------------------------------------    
USAGE EXAMPLES:    
------------------------------------------------------------  
[Client_Name_Sel_By_Commodity_Id] 291

[Client_Name_Sel_By_Commodity_Id] 290


AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
GB   Geetansu Behera    
CMH  Chad Hattabaugh     
HG   Hari   
SKA  Shobhit Kumar Agrawal

MODIFICATIONS     
Initials Date		Modification    
------------------------------------------------------------    
GB					created  
SKA		14-Aug-09	Modified as per coding standard
SKA		25 Aug 09	Removed the unwanted Select and show the data directly instead of to store in varaible
SS		06-SEP-09	SP renamed from Client_Csv_Sel_by_Commodity_Id to Client_Name_Sel_By_Commodity_Id
SKA		01/19/2010	Added Input parameter @type for Sustainability and Other Fuels.
					Reffered Commodity_Service_Cd from Core.Client_commodity	
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.Client_Name_Sel_By_Commodity_Id
(	
	@Commodity_Id INT,
	@Service_Type Varchar(100) = NULL
 ) 
AS  

BEGIN  
 
	SET NOCOUNT ON;  

	SELECT 
		c.CLIENT_NAME AS 'Client_List'
	FROM 
		Core.Client_Commodity cc 
		INNER JOIN dbo.CLIENT c 
			ON c.CLIENT_ID = cc.Client_Id
		JOIN dbo.Code cd
			ON cd.Code_Id =cc.Commodity_Service_Cd
	WHERE
		cc.Commodity_Id =@Commodity_Id
		AND (@Service_Type IS NULL OR cd.Code_Value = @Service_Type)
	ORDER BY
		c.CLIENT_NAME
     
END
GO
GRANT EXECUTE ON  [dbo].[Client_Name_Sel_By_Commodity_Id] TO [CBMSApplication]
GO
