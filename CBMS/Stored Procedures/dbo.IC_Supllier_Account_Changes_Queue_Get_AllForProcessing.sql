SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********  
NAME:	dbo.IC_Supllier_Account_Changes_Queue_Get_AllForProcessing   

DESCRIPTION:     

	
INPUT PARAMETERS:    
Name								DataType		Default		Description    
----------------------------------------------------------------------------    
@User_Info_Id					 	INT      

OUTPUT PARAMETERS:    
Name					DataType		Default		Description    
----------------------------------------------------------------------------    

USAGE EXAMPLES:    
----------------------------------------------------------------------------   
 
EXEC IC_Supllier_Account_Changes_Queue_Get_AllForProcessing 49



AUTHOR INITIALS:    
Initials	Name    
----------------------------------------------------------------------------    
NR			Narayana Reddy
 
MODIFICATIONS     
Initials	Date			Modification    
-----------------------------------------------------------------------------    
NR			2020-04-22		Created for Small enhancement
******/

CREATE PROCEDURE [dbo].[IC_Supllier_Account_Changes_Queue_Get_AllForProcessing]
     (
         @User_Info_Id INT
     )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @IC_Supllier_Account_Changes_Batch_Id INT
            , @Pending_Status_Cd INT;



        SELECT
            @Pending_Status_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Request Status'
            AND c.Code_Value = 'Pending';




        INSERT INTO dbo.IC_Supllier_Account_Changes_Batch
             (
                 Batch_Run_Date
             )
        VALUES
            (GETDATE());

        SET @IC_Supllier_Account_Changes_Batch_Id = SCOPE_IDENTITY();


        UPDATE
            isc
        SET
            isc.IC_Supllier_Account_Changes_Batch_Id = @IC_Supllier_Account_Changes_Batch_Id
        FROM
            dbo.IC_Supllier_Account_Changes_Queue isc
        WHERE
            isc.Status_Cd = @Pending_Status_Cd
            AND isc.IC_Supllier_Account_Changes_Batch_Id IS NULL;



        SELECT
            isc.IC_Supllier_Account_Changes_Queue_Id
            , isc.Account_Id
        FROM
            dbo.IC_Supllier_Account_Changes_Queue isc
        WHERE
            isc.IC_Supllier_Account_Changes_Batch_Id = @IC_Supllier_Account_Changes_Batch_Id
            AND isc.Status_Cd = @Pending_Status_Cd
        GROUP BY
            isc.IC_Supllier_Account_Changes_Queue_Id
            , isc.Account_Id
        ORDER BY
            isc.IC_Supllier_Account_Changes_Queue_Id ASC;


    END;

GO
GRANT EXECUTE ON  [dbo].[IC_Supllier_Account_Changes_Queue_Get_AllForProcessing] TO [CBMSApplication]
GO
