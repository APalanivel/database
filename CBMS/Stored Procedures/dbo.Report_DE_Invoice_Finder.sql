
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********                 
NAME:  dbo.Report_DE_Invoice_Finder                
               
DESCRIPTION:            
              
INPUT PARAMETERS:                  
      Name              DataType          Default     Description                  
------------------------------------------------------------                  
@Client_Name VARCHAR(MAX)          
@Site_Name  VARCHAR(MAX)          
@Account_Number VARCHAR(MAX)            
                  
                  
OUTPUT PARAMETERS:                  
      Name              DataType          Default     Description                  
------------------------------------------------------------                  
                  
USAGE EXAMPLES:             
          
Exec dbo.Report_DE_Invoice_Finder '117','312','440' ,125362      

Exec dbo.Report_DE_Invoice_Finder '-1','-1','-1' ,125362  
         
          
------------------------------------------------------------            
              
AUTHOR INITIALS:                
Initials Name                
------------------------------------------------------------                
AKR      Ashok Kumar Raju     
NR		 Narayana Reddy                   
     
              
              
Initials   Date			Modification                
------------------------------------------------------------                
AKR      2012-09-10		Created          
AKR      2012-11-16		Modified the code to add the Account_Id Parameter   
AKR      2013-02-05		Modified the code to add Supplier accounts  
AKR		 2014-09-02		Modified the code to include Invoice Type  
NR		 2016-01-20		MAINT-4737 Replaced Invoice_Type_Cd with Meter_Read_Type.
******/
CREATE PROCEDURE [dbo].[Report_DE_Invoice_Finder]
      (
       @Client_Name VARCHAR(MAX) = -1
      ,@Site_Name VARCHAR(MAX) = -1
      ,@Account_Number VARCHAR(MAX) = -1
      ,@Account_Id INT = NULL )
AS
BEGIN

      SET NOCOUNT ON;

      DECLARE @Accounts_Id_List TABLE
            (
             Account_id INT PRIMARY KEY );                    

      INSERT      INTO @Accounts_Id_List
                  ( Account_id )
                  SELECT
                        Segments
                  FROM
                        dbo.ufn_split(@Account_Number, ',');              
              
      IF ( ( @Account_Id IS NOT NULL
             AND @Client_Name = -1 )
           OR ( @Account_Id IS NULL
                AND @Client_Name != -1 ) )
            BEGIN             
            ;      
                  WITH  cte_Invoice_Data
                          AS ( SELECT
                                    ch.Client_Name [Client]
                                   ,ch.Site_name [Site]
                                   ,ch.State_Name [State]
                                   ,ch.Country_Name [Country]
                                   ,cha.Display_Account_Number Account_number
                                   ,cuism.CU_INVOICE_ID
                                   ,cui.BEGIN_DATE
                                   ,cui.END_DATE
                                   ,cuism.SERVICE_MONTH
                                   ,[Processed] = CASE WHEN cui.IS_PROCESSED = 1 THEN 'Yes'
                                                       ELSE 'No'
                                                  END
                                   ,[Reported] = CASE WHEN cui.IS_REPORTED = 1 THEN 'Yes'
                                                      ELSE 'No'
                                                 END
                                   ,[Duplicate] = CASE WHEN cui.IS_DUPLICATE = 1 THEN 'Yes'
                                                       ELSE 'No'
                                                  END
                                   ,ce.Code_Value AS Meter_Read_Type
                               FROM
                                    dbo.CU_INVOICE_SERVICE_MONTH cuism
                                    JOIN dbo.CU_INVOICE cui
                                          ON cui.CU_INVOICE_ID = cuism.CU_INVOICE_ID
                                    JOIN Core.Client_Hier_Account cha
                                          ON cha.Account_Id = cuism.Account_ID
                                    JOIN Core.Client_Hier ch
                                          ON ch.Client_Hier_Id = cha.Client_Hier_Id
                                    LEFT JOIN Code ce
                                          ON ce.Code_Id = cui.Meter_Read_Type_Cd
                               WHERE
                                    ch.Site_Id <> 0
                                    AND ( ( @Account_Id IS NULL
                                            AND ch.Client_Id = @Client_Name
                                            AND ch.Site_Id = @Site_Name
                                            AND EXISTS ( SELECT
                                                            1
                                                         FROM
                                                            @Accounts_Id_List ail
                                                         WHERE
                                                            ail.Account_id = cha.Account_Id ) )
                                          OR cha.Account_Id = @Account_Id )
                               GROUP BY
                                    ch.Client_Name
                                   ,ch.Site_name
                                   ,ch.State_Name
                                   ,ch.Country_Name
                                   ,cha.Display_Account_Number
                                   ,cuism.CU_INVOICE_ID
                                   ,cui.BEGIN_DATE
                                   ,cui.END_DATE
                                   ,cuism.SERVICE_MONTH
                                   ,cui.IS_PROCESSED
                                   ,cui.IS_REPORTED
                                   ,cui.IS_DUPLICATE
                                   ,ce.Code_Value)
                        SELECT
                              cid.Client
                             ,cid.Site
                             ,cid.State
                             ,cid.Country
                             ,cid.Account_number [Account Number]
                             ,cid.CU_INVOICE_ID [Invoice ID]
                             ,cid.BEGIN_DATE [Begin Date]
                             ,cid.END_DATE [End Date]
                             ,cid.SERVICE_MONTH [Service Month]
                             ,cid.Processed
                             ,cid.Reported
                             ,cid.Duplicate
                             ,cid.Meter_Read_Type
                        FROM
                              cte_Invoice_Data cid
                        ORDER BY
                              cid.Account_number
                             ,cid.CU_INVOICE_ID
                             ,cid.SERVICE_MONTH;              
            END;        
                
END;
;
GO




GRANT EXECUTE ON  [dbo].[Report_DE_Invoice_Finder] TO [CBMS_SSRS_Reports]
GO
