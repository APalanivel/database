SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [dbo].[RM_Deal_Ticket_Sel_By_Account_Id]  
     
DESCRIPTION: 
	To Get RFP id for Selected Account Id.
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Account_Id		INT						
@StartIndex		INT			1			
@EndIndex		INT			2147483647
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:          
------------------------------------------------------------
  
	EXEC dbo.RM_Deal_Ticket_Sel_By_Account_Id  101527
	EXEC dbo.RM_Deal_Ticket_Sel_By_Account_Id  36126
	EXEC dbo.Account_Details_Sel_By_Account_Id 36126
	EXEC dbo.RM_Deal_Ticket_Sel_By_Account_Id     
            @Account_Id = 1658654


AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR			PANDARINATH
          
MODIFICATIONS           
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			25-MAY-10	CREATED
	RR			2019-05-28	GRM - Modified to refer new schema
	RKV         2019-12-17  Maint-9678 Removed the core.client_hier_account and meter tables

*/

CREATE PROCEDURE [dbo].[RM_Deal_Ticket_Sel_By_Account_Id]
    (
        @Account_Id INT
        , @StartIndex INT = 1
        , @EndIndex INT = 2147483647
    )
AS
    BEGIN

        SET NOCOUNT ON;

        WITH Cte_RM
        AS (
               SELECT
                    rdt.Deal_Ticket_Id AS RM_DEAL_TICKET_ID
                    , ROW_NUMBER() OVER (ORDER BY
                                             rdt.Deal_Ticket_Id) Row_Num
                    , COUNT(1) OVER () Total_Rows
               FROM
                    Trade.Deal_Ticket_Client_Hier_Volume_Dtl AS rdt
               WHERE
                    rdt.Account_Id = @Account_Id
               GROUP BY
                   rdt.Deal_Ticket_Id
               HAVING
                    SUM(Total_Volume) > 0
           )
        SELECT
            RM_DEAL_TICKET_ID
            , Row_Num
            , Total_Rows
        FROM
            Cte_RM
        WHERE
            Row_Num BETWEEN @StartIndex
                    AND     @EndIndex;

    END;

GO
GRANT EXECUTE ON  [dbo].[RM_Deal_Ticket_Sel_By_Account_Id] TO [CBMSApplication]
GO
