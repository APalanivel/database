SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_ACCOUNTS_UNDER_SITE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@siteId        	int       	          	
	@commodityId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--select * from account
--select * from sr_deal_ticket
--select * from entity where entity_id>=59
--select * from rate
--exec dbo.SR_SAD_GET_ACCOUNTS_UNDER_SITE_P '-1','-1', 343, 290



CREATE      PROCEDURE dbo.SR_SAD_GET_ACCOUNTS_UNDER_SITE_P
@userId varchar(10),
@sessionId varchar(20),
@siteId int,
@commodityId int
AS
set nocount on
declare @selectClause varchar(150)
declare @heatRateCommodityId int
	select @selectClause=(select ENTITY_NAME from ENTITY where ENTITY_ID=@commodityId) 
	select @heatRateCommodityId=(select ENTITY_ID from ENTITY where ENTITY_NAME='Electric Power' and ENTITY_TYPE=157) 
	

/*
select 
	ACCOUNT_ID, ACCOUNT_NUMBER 
from 
	ACCOUNT acc, METER mt, RATE rt

where
	rt.RATE_ID = mt.RATE_ID and
	mt.ACCOUNT_ID = acc.ACCOUNT_ID and
	acc.SITE_ID = @siteId and
	rt.COMMODITY_TYPE_ID = @commodityId*/

IF(@selectClause = 'Heat Rate')
BEGIN
	SELECT
		distinct ACCOUNT.ACCOUNT_ID AS ACCOUNT_ID, ACCOUNT.ACCOUNT_NUMBER AS ACCOUNT_NUMBER
	FROM
		ACCOUNT INNER JOIN
	        METER ON ACCOUNT.ACCOUNT_ID = METER.ACCOUNT_ID INNER JOIN
	        RATE ON METER.RATE_ID = RATE.RATE_ID
	WHERE
		(ACCOUNT.SITE_ID = @siteId) AND (RATE.COMMODITY_TYPE_ID = @heatRateCommodityId)
END

ELSE
BEGIN

	SELECT
		distinct ACCOUNT.ACCOUNT_ID AS ACCOUNT_ID, ACCOUNT.ACCOUNT_NUMBER AS ACCOUNT_NUMBER
	FROM
		ACCOUNT INNER JOIN
	        METER ON ACCOUNT.ACCOUNT_ID = METER.ACCOUNT_ID INNER JOIN
	        RATE ON METER.RATE_ID = RATE.RATE_ID
	WHERE
		(ACCOUNT.SITE_ID = @siteId) AND (RATE.COMMODITY_TYPE_ID = @commodityId)

END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_ACCOUNTS_UNDER_SITE_P] TO [CBMSApplication]
GO
