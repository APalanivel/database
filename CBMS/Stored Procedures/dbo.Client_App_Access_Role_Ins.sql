SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
         
/******          
            
 NAME: dbo.Client_App_Access_Role_Ins			
            
 DESCRIPTION:            
    To insert the values into Client App Access Role  table.            
            
 INPUT PARAMETERS:            
           
 Name                               DataType            Default      Description            
---------------------------------------------------------------------------------------------------------------          
 @App_Access_Role_Name              NVARCHAR(255)       NOT NULL          
 @App_Access_Role_Dsc               NVARCHAR(1000)      NULL          
 @Client_Id                         INT                 NOT NULL          
 @Created_User_Id                   INT                 NOT NULL   
 @Group_Info_Id						VARCHAR(MAX)  
 @App_Access_Role_Type_Cd			INT   
     
             
 OUTPUT PARAMETERS:           
                  
 Name                               DataType            Default      Description            
---------------------------------------------------------------------------------------------------------------          
            
 USAGE EXAMPLES:                
---------------------------------------------------------------------------------------------------------------          
 BEGIN TRAN
 DECLARE @Var INT    
 EXEC dbo.Client_App_Access_Role_Ins           
        @App_Access_Role_Name='Test1234'          
       ,@App_Access_Role_Dsc=null          
       ,@Client_Id=110          
       ,@Created_User_Id=1     
       ,@Group_Info_Id='1,2,3,4,5,6'
       ,@App_Access_Role_Type_Cd=100697
       
  SELECT @Var=MAX(Client_App_Access_Role_Id) FROM dbo.Client_App_Access_Role    
   select * from dbo.Client_App_Access_Role where Client_App_Access_Role_Id=@var
  SELECT * FROM dbo.Client_App_Access_Role_Group_Info_Map WHERE Client_App_Access_Role_Id=@var    
 ROLLBACK TRAN            
           
 AUTHOR INITIALS:          
            
 Initials               Name            
---------------------------------------------------------------------------------------------------------------          
 NR                     Narayana Reddy              
             
 MODIFICATIONS:           
           
 Initials               Date            Modification          
---------------------------------------------------------------------------------------------------------------          
 NR                     2013-11-25      Created for RA Admin user management          
           
******/            
      
CREATE  PROCEDURE dbo.Client_App_Access_Role_Ins
      ( 
       @App_Access_Role_Name NVARCHAR(255)
      ,@App_Access_Role_Dsc NVARCHAR(1000) = NULL
      ,@Client_Id INT
      ,@Created_User_Id INT
      ,@Group_Info_Id VARCHAR(MAX)
      ,@App_Access_Role_Type_Cd INT )
AS 
BEGIN          

      SET NOCOUNT ON; 
      DECLARE @Client_App_Access_Role_Id INT         
           
      INSERT      INTO dbo.Client_App_Access_Role
                  ( 
                   App_Access_Role_Name
                  ,App_Access_Role_Dsc
                  ,Client_Id
                  ,Created_User_Id
                  ,Created_Ts
                  ,Updated_User_Id
                  ,Last_Change_Ts
                  ,App_Access_Role_Type_Cd )
      VALUES
                  ( 
                   @App_Access_Role_Name
                  ,@App_Access_Role_Dsc
                  ,@Client_Id
                  ,@Created_User_Id
                  ,GETDATE()
                  ,@Created_User_Id
                  ,GETDATE()
                  ,@App_Access_Role_Type_Cd )  


                     
                       
    
      SELECT
            @Client_App_Access_Role_Id = SCOPE_IDENTITY()   
            

      EXEC dbo.Client_App_Access_Role_Group_Info_Map_Merge 
            @Client_App_Access_Role_Id = @Client_App_Access_Role_Id
           ,@Group_Info_Id = @Group_Info_Id
           ,@Assigned_User_Id = @Created_User_Id                             
END   


;

;
GO
GRANT EXECUTE ON  [dbo].[Client_App_Access_Role_Ins] TO [CBMSApplication]
GO
