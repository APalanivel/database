SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.Get_Site_Details_by_Client_Hier_Id

DESCRIPTION:


INPUT PARAMETERS:
	Name			    DataType		Default	Description
------------------------------------------------------------
	@Client_Hier_Id    int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
EXECUTE dbo.Get_Site_Details_by_Client_Hier_Id 
      @Client_Hier_Id = 1130
EXECUTE dbo.Get_Site_Details_by_Client_Hier_Id
      @Client_Hier_Id = 1229

AUTHOR INITIALS:
    Initials	 Name
------------------------------------------------------------
    AP		 Athmaram Pabbathi
    
MODIFICATIONS
    Initials	 Date	   Modification
------------------------------------------------------------
    AP		 2012-04-04  Created for Addnl Data
******/

CREATE PROCEDURE dbo.Get_Site_Details_by_Client_Hier_Id 
( @Client_Hier_Id INT )
AS 
BEGIN
      SET NOCOUNT ON

      SELECT
            ch.Client_Hier_Id
           ,ch.Site_Id
           ,s.Site_Type_Id
           ,st.Entity_Name AS Site_Type
           ,ch.Site_Name
           ,ch.Sitegroup_Id AS Division_Id
           ,ch.Sitegroup_Name AS Division_Name
           ,ch.Client_Id
           ,ch.Client_Name
           ,s.UBMSite_Id
           ,s.Primary_Address_Id
           ,s.Site_Reference_Number
           ,s.Site_Product_Service
           ,s.Production_Schedule
           ,s.Shutdown_Schedule
           ,s.Is_Alternate_Gas
           ,s.Doing_Business_As
           ,s.Naics_Code
           ,s.Tax_Number
           ,s.Duns_Number
           ,s.Is_History
           ,s.History_Reason_Type_Id
           ,hr.Entity_Name AS History_Reason_Type
           ,rtrim(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' AS Site_Label
           ,ch.Site_Not_Managed AS Not_Managed
      FROM
            Core.Client_Hier ch
            INNER JOIN dbo.[Site] s
                  ON s.Site_Id = ch.Site_Id
            INNER JOIN dbo.Entity st
                  ON st.Entity_Id = s.Site_Type_Id
            LEFT OUTER JOIN dbo.Entity hr
                  ON hr.Entity_Id = s.History_Reason_Type_Id
      WHERE
            ch.Client_Hier_Id = @Client_Hier_Id

END
;
GO
GRANT EXECUTE ON  [dbo].[Get_Site_Details_by_Client_Hier_Id] TO [CBMSApplication]
GO
