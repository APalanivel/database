SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
  
Name:    
    dbo.RM_Scenario_Report_Del_By_Report_Id  
   
Description:    
   
 Input Parameters:    
    Name							DataType    Default		Description    
-------------------------------------------------------------------------------------    
    @RM_Scenario_Report_Id			INT  

 Output Parameters:    
    Name   Datatype  Default Description    
------------------------------------------------------------    
   
 Usage Examples:  
------------------------------------------------------------    
    SELECT * FROM dbo.RM_Scenario_Report

BEGIN TRANSACTION
		SELECT * FROM dbo.RM_Scenario_Report a 
		JOIN dbo.RM_Scenario_Planning_Iteration b ON a.RM_Scenario_Report_Id = b.RM_Scenario_Report_Id
		JOIN dbo.RM_Scenario_Planning_Iteration_Dtl c ON b.RM_Scenario_Planning_Iteration_Id = c.RM_Scenario_Planning_Iteration_Id
		WHERE a.RM_Scenario_Report_Id = 202
	EXECUTE dbo.RM_Scenario_Report_Del_By_Report_Id 202
		SELECT * FROM dbo.RM_Scenario_Report a 
		JOIN dbo.RM_Scenario_Planning_Iteration b ON a.RM_Scenario_Report_Id = b.RM_Scenario_Report_Id
		JOIN dbo.RM_Scenario_Planning_Iteration_Dtl c ON b.RM_Scenario_Planning_Iteration_Id = c.RM_Scenario_Planning_Iteration_Id
		WHERE a.RM_Scenario_Report_Id = 202
ROLLBACK TRANSACTION
    
     
Author Initials:  
	Initials	Name  
------------------------------------------------------------  
	RR			Raghu Reddy
  
 Modifications :    
	Initials	Date		Modification    
------------------------------------------------------------    
	RR			2014-11-25	Created
******/  
CREATE PROCEDURE [dbo].[RM_Scenario_Report_Del_By_Report_Id]
      ( 
       @RM_Scenario_Report_Id INT )
AS 
BEGIN  
      SET NOCOUNT ON; 
       
      DELETE
            dtl
      FROM
            dbo.RM_Scenario_Planning_Iteration_Dtl dtl
            INNER JOIN dbo.RM_Scenario_Planning_Iteration itr
                  ON dtl.RM_Scenario_Planning_Iteration_Id = itr.RM_Scenario_Planning_Iteration_Id
            INNER JOIN dbo.RM_Scenario_Report rpt
                  ON itr.RM_Scenario_Report_Id = rpt.RM_Scenario_Report_Id
      WHERE
            rpt.RM_Scenario_Report_Id = @RM_Scenario_Report_Id
            
      DELETE
            itr
      FROM
            dbo.RM_Scenario_Planning_Iteration itr
            INNER JOIN dbo.RM_Scenario_Report rpt
                  ON itr.RM_Scenario_Report_Id = rpt.RM_Scenario_Report_Id
      WHERE
            rpt.RM_Scenario_Report_Id = @RM_Scenario_Report_Id
            
      DELETE
            rpt
      FROM
            dbo.RM_Scenario_Report rpt
      WHERE
            rpt.RM_Scenario_Report_Id = @RM_Scenario_Report_Id


END;
;
GO
GRANT EXECUTE ON  [dbo].[RM_Scenario_Report_Del_By_Report_Id] TO [CBMSApplication]
GO
