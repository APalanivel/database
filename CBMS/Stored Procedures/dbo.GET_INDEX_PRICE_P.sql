SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- exec dbo.GET_INDEX_PRICE_P '','',364
CREATE   PROCEDURE dbo.GET_INDEX_PRICE_P
@userId varchar,
@sessionId varchar,
@priceIndexId int

as
	set nocount on
select	CONVERT (Varchar(12), priceindex.INDEX_MONTH, 102)  MONTH_IDENTIFIER,
	 CAST(priceindex.INDEX_VALUE as decimal(8,4)) INDEX_PRICE

from	PRICE_INDEX_VALUE priceindex

where	priceindex.PRICE_INDEX_ID=@priceIndexId 
--select * from PRICE_INDEX_VALUE where MONTH_IDENTIFIER='01-01-2004'
GO
GRANT EXECUTE ON  [dbo].[GET_INDEX_PRICE_P] TO [CBMSApplication]
GO
