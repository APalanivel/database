SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

    
    
      
      
/******            
NAME:    [Workflow].[Workflow_Queue_Action_Label]        
DESCRIPTION: it'll return the Action lables details.        
------------------------------------------------------------           
 INPUT PARAMETERS:            
 Name   DataType  Default Description         
 @MODULE_ID INT,    
    @Workflow_Queue_Saved_Filter_Query_Id INT = NULL,    
    @Logged_In_User INT,    
    @Queue_id VARCHAR(MAX) = NULL,     ---  User selected from drop down Id 49,1,2,3              
    @Exception_Type VARCHAR(MAX) = NULL,    
    @Account_Number VARCHAR(500) = NULL,    
    @Client VARCHAR(500) = NULL,    
    @Site VARCHAR(500) = NULL,    
    @Country VARCHAR(500) = NULL,    
    @State VARCHAR(500) = NULL,    
    @City VARCHAR(500) = NULL,    
    @Commodity VARCHAR(500) = NULL,    
    @Invoice_ID VARCHAR(500) = NULL,    
    @Priority INT = 0,                 --- Yes              
    @Exception_Status INT = NULL,    
    @Comments VARCHAR(500) = NULL,    
    @Start_Date_in_Queue DATETIME = NULL,    
    @End_Date_in_Queue DATETIME = NULL,    
    @Start_Date_in_CBMS DATETIME = NULL,    
    @End_Date_in_CBMS DATETIME = NULL,    
    @Data_Source INT = NULL,    
    @Vendor VARCHAR(100) = NULL,    
    @Vendor_Type VARCHAR(100) = NULL,    
    @Month DATETIME = NULL,    
    @Filename VARCHAR(100) = NULL,    
    @invoice_List VARCHAR(MAX) = NULL, ---- if new invoice needs to be added send those invoices     
    @User_Group_Id_List VARCHAR(MAX),    
    @Invoice_List_TVP Invoice_List READONLY,    
      
 @Label_account_number VARCHAR(200) = NULL  ,                  
 @Label_client VARCHAR(200) = NULL   ,                 
 @Label_city VARCHAR(200) = NULL        ,            
 @Label_state VARCHAR(200) = NULL ,                   
 @Label_vendor VARCHAR(200) = NULL  ,               
 @Label_Country VARCHAR(200) = NULL    ,             
 @Label_Commodity VARCHAR(200) = NULL   ,                   
 @Label_Vendor_type VARCHAR(200)  = NULL  ,                    
 @Label_Site VARCHAR(200)  = NULL                   
------------------------------------------------------------            
 OUTPUT PARAMETERS:            
 Name   DataType  Default Description            
------------------------------------------------------------            
 USAGE EXAMPLES:            
------------------------------------------------------------            
AUTHOR INITIALS:            
Initials Name            
------------------------------------------------------------            
TRK   Ramakrishna Thummala Summit Energy         
         
 MODIFICATIONS             
 Initials Date   Modification            
------------------------------------------------------------            
 TRK 11/09/2019 Created
 CPK	Oct-2019  added event log logic        
        
******/              
CREATE  PROCEDURE [Workflow].[Workflow_Queue_Action_Label]              
 (                     
    @MODULE_ID INT,    
    @Workflow_Queue_Saved_Filter_Query_Id INT = NULL,    
    @Logged_In_User INT,    
    @Queue_id VARCHAR(MAX) = NULL,     ---  User selected from drop down Id 49,1,2,3              
    @Exception_Type VARCHAR(MAX) = NULL,    
    @Account_Number VARCHAR(500) = NULL,    
    @Client VARCHAR(500) = NULL,    
    @Site VARCHAR(500) = NULL,    
    @Country VARCHAR(500) = NULL,    
    @State VARCHAR(500) = NULL,    
    @City VARCHAR(500) = NULL,    
    @Commodity VARCHAR(500) = NULL,    
    @Invoice_ID VARCHAR(500) = NULL,    
    @Priority INT = 0,                 --- Yes              
    @Exception_Status INT = NULL,    
    @Comments VARCHAR(500) = NULL,    
    @Start_Date_in_Queue DATETIME = NULL,    
    @End_Date_in_Queue DATETIME = NULL,    
    @Start_Date_in_CBMS DATETIME = NULL,    
    @End_Date_in_CBMS DATETIME = NULL,    
    @Data_Source INT = NULL,    
    @Vendor VARCHAR(100) = NULL,    
    @Vendor_Type VARCHAR(100) = NULL,    
    @Month DATETIME = NULL,    
    @Filename VARCHAR(100) = NULL,    
    @invoice_List VARCHAR(MAX) = NULL, ---- if new invoice needs to be added send those invoices     
    @User_Group_Id_List VARCHAR(MAX),    
    @Invoice_List_TVP Invoice_List READONLY,    
      
 @Label_account_number VARCHAR(200) = NULL  ,                  
 @Label_client VARCHAR(200) = NULL   ,                      
 @Label_state VARCHAR(200) = NULL ,                   
 @Label_vendor VARCHAR(200) = NULL  ,               
 @Label_Country VARCHAR(200) = NULL    ,             
 @Label_Commodity VARCHAR(200) = NULL   ,                   
 @Label_Vendor_type VARCHAR(200)  = NULL  ,                    
 @Label_Site VARCHAR(200)  = NULL              
      
       
 )                  
AS                  
BEGIN                  
 -- SET NOCOUNT ON added to prevent extra result sets from         
    -- interfering with SELECT statements.         
    DECLARE @Proc_name VARCHAR(100) = 'Workflow_Queue_Action_Label',    
            @Input_Params VARCHAR(1000),    
            @Error_Line INT,    
            @Error_Message VARCHAR(3000),    
            @Workflow_Queue_Search_Filter_Group_Id INT;    
    
    DECLARE @invoice_table TABLE    
    (    
        Invoice_id INT    
    );    
    
    SELECT @Input_Params    
        = '@User ID:' + CAST(@Logged_In_User AS VARCHAR) + '@Workflow_Queue_Saved_Filter_Query_id:'    
          + CAST(@Workflow_Queue_Saved_Filter_Query_Id AS VARCHAR) + '@invoice_List:' + ISNULL(@invoice_List, '');    
    SET NOCOUNT ON;    
    
    
    
    BEGIN TRY    
    
    
    
        --get the list of invoices    
    
        INSERT @invoice_table    
        EXEC [Workflow].[Get_List_Of_Invoices] @MODULE_ID,    
                                               @Workflow_Queue_Saved_Filter_Query_Id,    
                                               @Logged_In_User,    
                                               @Queue_id,    
                                               @Exception_Type,    
                                               @Account_Number,    
                                               @Client,    
                                               @Site,    
                                               @Country,    
                                               @State,       
                                               @City,      
											   @Commodity, 
                                               @Invoice_ID,    
                                               @Priority,    
                                               @Exception_Status,    
                                               NULL,    
                                               @Start_Date_in_Queue,    
                                               @End_Date_in_Queue,    
                                               @Start_Date_in_CBMS,    
                                               @End_Date_in_CBMS,    
                                               @Data_Source,    
                                               @Vendor,    
                                               @Vendor_Type,    
                                               @Month,    
                                               @Filename,    
                                               @invoice_List,    
                                               @User_Group_Id_List,    
                                               @Invoice_List_TVP;   
     
     
    

 --- Inserting the Lable Data -----        
     
              
 MERGE INTO dbo.cu_invoice_label tgt                  
    USING ( SELECT Invoice_id FROM @invoice_table ) src                  
    ON src.Invoice_id = tgt.CU_Invoice_ID                         
    WHEN NOT MATCHED THEN INSERT (                    
    cu_invoice_id                  
   ,account_number                   
   ,client_name                  
   ,state_name                  
   ,vendor_name               
   ,Country               
   ,Commodity                      
   ,Vendor_type            
   ,Site_Name            
   ,Created_User_Id            
   ,Created_Ts             
     )                  
 VALUES                  
 (                    
    Invoice_id                  
   ,@Label_Account_Number                  
   ,@Label_client                         
   ,@Label_State                   
   ,@Label_Vendor              
   ,@Label_Country            
   ,@Label_Commodity                       
   ,@Label_Vendor_Type            
   ,@Label_Site            
   ,@Logged_In_User            
   ,GETDATE()            
  )                  
    WHEN MATCHED THEN UPDATE SET                  
        --tgt.cu_invoice_id = @cu_invoice_id                  
    tgt.Account_Number = @Label_Account_Number                  
  , tgt.client_name=@Label_Client                        
  , tgt.state_name=@Label_State                  
  , tgt.vendor_name=@Label_Vendor                    
  , tgt.COUNTRY =@Label_Country               
  , tgt.COMMODITY =@Label_Commodity                       
  , tgt.VENDOR_TYPE =@Label_Vendor_Type            
  , tgt.Site_Name= @Label_Site            
  , tgt.Updated_User_Id = @Logged_In_User            
  , tgt.Last_Change_Ts = GETDATE();            
                  
	
   INSERT INTO dbo.cu_invoice_event          
   (          
      cu_invoice_id  
		,event_date  
		,event_by_id  
		,event_description         
   )       
   SELECT           
    AC.Invoice_id AS CU_INVOICE_ID          
   ,getdate() AS event_date          
   ,@Logged_In_User AS event_by_id         
   ,'Invoice Labeled' AS event_description          
   FROM @invoice_table AS AC        

                  
 END TRY    
    BEGIN CATCH    
        -- Entry made to the logging SP to capture the errors.        
        SELECT @Error_Line = error_line(),    
               @Error_Message = error_message();    
    
        INSERT INTO StoredProc_Error_Log    
        (    
            StoredProc_Name,    
            Error_Line,    
            Error_message,    
            Input_Params    
        )    
        VALUES    
        (@Proc_name, @Error_Line, @Error_Message, @Input_Params);    
    
        EXEC [dbo].[usp_RethrowError] @Error_Message;    
    END CATCH;    
END;                
GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_Action_Label] TO [CBMSApplication]
GO
