SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_ACCOUNT_MANAGE_NO_BID_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@userId int,
@rfpId int,
@accountTermId int,
@accountgroupId int,
@selectedProductId int, 
@supplierContactId int,
@noBidComments varchar(4000)
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
--exec dbo.SR_RFP_ACCOUNT_MANAGE_NO_BID_P
--@userId = -1,
--@rfpId = NULL,
--@accountTermId =3151,
--@accountgroupId = NULL,
--@selectedProductId = 414 , 
--@supplierContactId =2587,
--@noBidComments = NULL  -- Original value = NULL


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_ACCOUNT_MANAGE_NO_BID_P
@userId int,
@rfpId int,
@accountTermId int,
@accountgroupId int,
@selectedProductId int, 
@supplierContactId int,
@noBidComments varchar(4000)

as 
	
set nocount on

--added by Shailesh for inserting account level manage no bid

UPDATE 
	sr_rfp_term_product_map 
SET 
	  is_no_bid = 1
	, no_bid_comments = @noBidComments
	, bid_status = 'NB' 
WHERE
	sr_rfp_supplier_contact_vendor_map_id= @supplierContactId and
	sr_rfp_account_term_id = @accountTermId and
	sr_rfp_selected_products_id = @selectedProductId
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_ACCOUNT_MANAGE_NO_BID_P] TO [CBMSApplication]
GO
