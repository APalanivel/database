SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                 
/******                        
 NAME: dbo.RM_Scenario_Planning_Iteration_Dtl_Sel_By_RM_Scenario_Report            
                        
 DESCRIPTION:                        
			To get the RM_Scenario_Planning_Iteration_Dtl based on RM Scenario Report Id         
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@RM_Scenario_Report_Id		   INT 
                 
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 
 EXEC [dbo].[RM_Scenario_Planning_Iteration_Dtl_Sel_By_RM_Scenario_Report] 
      @RM_Scenario_Report_Id=1 
          
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2014-10-01       Created                
                       
******/   
       
                
CREATE PROCEDURE [dbo].[RM_Scenario_Planning_Iteration_Dtl_Sel_By_RM_Scenario_Report]
      ( 
       @RM_Scenario_Report_Id INT )
AS 
BEGIN                
      SET NOCOUNT ON;     
      
      SELECT
            rmsi.Iteration_Num
           ,rmsid.Service_Month
           ,rmc.Code_Value AS Scenario_Input_Type
           ,rmsid.Scenario_Input_Value
      FROM
            dbo.RM_Scenario_Report rms
            INNER JOIN dbo.RM_Scenario_Planning_Iteration rmsi
                  ON rms.RM_Scenario_Report_Id = rmsi.RM_Scenario_Report_Id
            INNER JOIN dbo.RM_Scenario_Planning_Iteration_Dtl rmsid
                  ON rmsid.RM_Scenario_Planning_Iteration_Id = rmsi.RM_Scenario_Planning_Iteration_Id
            INNER JOIN dbo.Code rmc
                  ON rmsid.Scenario_Input_Name_Cd = rmc.Code_Id
      WHERE
            rms.RM_Scenario_Report_Id = @RM_Scenario_Report_Id
                                                      
                                 
                       
END 
;
GO
GRANT EXECUTE ON  [dbo].[RM_Scenario_Planning_Iteration_Dtl_Sel_By_RM_Scenario_Report] TO [CBMSApplication]
GO
