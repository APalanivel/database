SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*********              
NAME:  dbo.Master_Variance_Test_Commodity_SEL_By_Variance_Test                
               
DESCRIPTION:  Used to select all the Master level variance data                
1 for @Commodity_Mapped_List gives the non-associated Commodities for the Variance Test Id    
              
INPUT PARAMETERS:                  
      Name              DataType          Default     Description                  
------------------------------------------------------------                  
@Variance_Test_Id  INT          
@Commodity_Mapped_List BIT                 1          
    
                  
OUTPUT PARAMETERS:                  
      Name              DataType          Default     Description                  
------------------------------------------------------------                  
                  
USAGE EXAMPLES:                 
              
 EXEC dbo.Master_Variance_Test_Commodity_SEL_By_Variance_Test    47,0    
              
------------------------------------------------------------                
AUTHOR INITIALS:                
Initials Name                
------------------------------------------------------------                
AKR      Ashok Kumar Raju  
SCH      Satish Chadarajupalli
AP		Arunkumar Palanivel  
              
              
Initials Date Modification                
------------------------------------------------------------                
 AKR     2013-05-24 Created   
 SCH     2020-01-03  added new input paramter @variance_process_Engine_id for filtering the Variance test commidttes 
 AP		Jan 8 2020 added new parameter @AVT_Code_Id, this will be used for new AVT TAB. SInce we dont have variance test ID for AVT, we need @AVT_Code_Id field. THis id is usage/total cost        
******/

CREATE PROCEDURE [dbo].[Master_Variance_Test_Commodity_SEL_By_Variance_Test]
      (
      @Variance_Test_Id           INT     --- pass this value as null for AVT
    , @Commodity_Mapped_List      BIT = 1
    , @AVT_Code_Id                INT     ---> pass this value only FOR AVT
    , @variance_process_Engine_id INT )
AS
      BEGIN

            SET NOCOUNT ON;

            IF EXISTS
                  (     SELECT
                              1
                        FROM  dbo.Variance_Processing_Engine VP
                              JOIN
                              dbo.Code C
                                    ON VP.Variance_Engine_Cd = C.Code_Id
                        WHERE C.Code_Dsc = 'Advanced Variance Test'
                              AND   VP.Variance_Processing_Engine_Id = @variance_process_Engine_id )
                  BEGIN

                        ;WITH cte_com_AVT
                         AS ( SELECT      DISTINCT
                                          VMAP.Commodity_Id
                                        , CM.Commodity_Name
                              FROM        dbo.Variance_Parameter_Commodity_Map VMAP
                                          JOIN
                                          dbo.Commodity CM
                                                ON CM.Commodity_Id = VMAP.Commodity_Id
                                          JOIN
                                          dbo.Variance_Parameter VP
                                                ON VP.Variance_Parameter_Id = VMAP.Variance_Parameter_Id
                                          JOIN
                                          dbo.Code C
                                                ON C.Code_Id = VP.Variance_Category_Cd
                              WHERE       C.Code_Id = @AVT_Code_Id
                                          AND   VMAP.variance_process_Engine_id = @variance_process_Engine_id )
                        SELECT
                                    *
                        FROM        cte_com_AVT cpc
                        ORDER BY    CASE WHEN cpc.Commodity_Name = 'Electric Power'
                                               THEN '1'
                                         WHEN cpc.Commodity_Name = 'Natural Gas'
                                               THEN '2'
                                         ELSE  cpc.Commodity_Name
                                    END;
                  END;
            ELSE
                  BEGIN
                        ;
                        WITH Cte_Parameter_Commodities
                        AS ( SELECT
                                    vpcm.Commodity_Id
                                  , com.Commodity_Name
								  ,cd.Code_Value
                             FROM   dbo.Variance_Rule vr
                                    INNER JOIN
                                    Variance_Parameter_Baseline_Map vpbmap
                                          ON vr.Variance_Baseline_Id = vpbmap.Variance_Baseline_Id
                                    INNER JOIN
                                    Variance_Parameter vp
                                          ON vp.Variance_Parameter_Id = vpbmap.Variance_Parameter_Id
                                    INNER JOIN
                                    dbo.Variance_Parameter_Commodity_Map vpcm
                                          ON vpcm.Variance_Parameter_Id = vp.Variance_Parameter_Id
										  		INNER JOIN Code cd
									ON cd.code_id = vp.Variance_Category_cd
                                    --AND vpcm.variance_process_Engine_id=@variance_process_Engine_id    
                                    INNER JOIN
                                    dbo.Commodity com
                                          ON com.Commodity_Id = vpcm.Commodity_Id
                             WHERE  vr.Variance_Test_Id = @Variance_Test_Id  

							-- AND vpcm.variance_process_Engine_id=@variance_process_Engine_id   
							   )
                        SELECT
                                    @Variance_Test_Id Variance_Test_Id
                                  , cpc.Commodity_Id
                                  , cpc.Commodity_Name
                        FROM        dbo.Variance_Test_Master vt
                                    INNER JOIN
                                    Variance_Test_Commodity_Map cmap
                                          ON cmap.Variance_Test_Id = vt.Variance_Test_Id
                                             AND vt.Variance_Test_Id = @Variance_Test_Id
                                    RIGHT JOIN
                                    Cte_Parameter_Commodities cpc
                                          ON cpc.Commodity_Id = cmap.Commodity_Id
                        WHERE    NOT  ( cpc.Code_Value ='Cost' AND cpc.Commodity_Name IN 
						('Electric Power', 'Water', 'Natural gas', 'Waste Water')) 
						AND  (     (     @Commodity_Mapped_List = 1
                                                AND   cmap.Commodity_Id IS NOT NULL )
                                          OR
                                                (     @Commodity_Mapped_List = 0
                                                      AND   cmap.Commodity_Id IS NULL ))
                        GROUP BY    cpc.Commodity_Id
                                  , cpc.Commodity_Name
                        ORDER BY    CASE WHEN cpc.Commodity_Name = 'Electric Power'
                                               THEN '1'
                                         WHEN cpc.Commodity_Name = 'Natural Gas'
                                               THEN '2'
                                         ELSE  cpc.Commodity_Name
                                    END;

                  END;

      END;
GO
GRANT EXECUTE ON  [dbo].[Master_Variance_Test_Commodity_SEL_By_Variance_Test] TO [CBMSApplication]
GO
