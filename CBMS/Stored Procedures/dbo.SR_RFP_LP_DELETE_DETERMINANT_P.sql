SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_LP_DELETE_DETERMINANT_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
	@user_id varchar(10),
	@session_id varchar(20),
	@determinant_id int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- Test a delete
--exec  dbo.SR_RFP_LP_DELETE_DETERMINANT_P
--	@user_id = 1,
--	@session_id = -1,
--	@determinant_id = 83578

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_LP_DELETE_DETERMINANT_P
	@user_id varchar(10),
	@session_id varchar(20),
	@determinant_id int
	AS
	
set nocount on

	delete sr_rfp_lp_determinant_values where sr_rfp_load_profile_determinant_id = @determinant_id

	if (select determinant_type_id from sr_rfp_load_profile_determinant(nolock) where sr_rfp_load_profile_determinant_id = @determinant_id) = 1157 --//'Account Level' entity
	begin
		delete sr_rfp_load_profile_determinant where sr_rfp_load_profile_determinant_id = @determinant_id

	end
	else
	begin
		update sr_rfp_load_profile_determinant set determinant_type_id = 1159, is_checked = 0
		where sr_rfp_load_profile_determinant_id = @determinant_id
	end
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_LP_DELETE_DETERMINANT_P] TO [CBMSApplication]
GO
