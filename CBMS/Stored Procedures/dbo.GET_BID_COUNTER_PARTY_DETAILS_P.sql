SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_BID_COUNTER_PARTY_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@dealTicketId  	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE          PROCEDURE dbo.GET_BID_COUNTER_PARTY_DETAILS_P

@userId varchar(10),
@sessionId varchar(20),
@dealTicketId int 

AS
SELECT 
	rdtcp.RM_DEAL_TICKET_ID as RM_DEAL_TICKET_ID, 
	rdtcp.RM_COUNTERPARTY_ID as RM_COUNTERPARTY_ID,
	rdtcp.COUNTERPARTY_CONTACT_NAME as COUNTERPARTY_CONTACT_NAME,
	rdtcp.COUNTERPARTY_CONTACT_PHONE as COUNTERPARTY_CONTACT_PHONE,
	rdtcp.COUNTERPARTY_CONTACT_CELL as COUNTERPARTY_CONTACT_CELL,
	rdtcp.COUNTERPARTY_CONTACT_FAX as COUNTERPARTY_CONTACT_FAX,
	rdtcp.COUNTERPARTY_CONTACT_EMAIL as COUNTERPARTY_CONTACT_EMAIL,
	rcp.COUNTERPARTY_NAME as COUNTERPARTY_NAME


FROM 
	RM_DEAL_TICKET_COUNTER_PARTY rdtcp,
	RM_COUNTERPARTY rcp 

WHERE 
	rdtcp.RM_DEAL_TICKET_ID = @dealTicketId and 
        rdtcp.RM_COUNTERPARTY_ID = rcp.RM_COUNTERPARTY_ID
GO
GRANT EXECUTE ON  [dbo].[GET_BID_COUNTER_PARTY_DETAILS_P] TO [CBMSApplication]
GO
