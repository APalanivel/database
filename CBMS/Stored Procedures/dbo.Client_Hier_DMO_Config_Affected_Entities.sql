
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.Client_Hier_DMO_Config_Affected_Entities
           
DESCRIPTION:             
			To get affected entities of a DMO configuration
			
INPUT PARAMETERS:            
	Name							DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Client_Hier_DMO_Config_Id		INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT TOP 10 * FROM dbo.Client_Hier_DMO_Config
            
	EXEC dbo.Client_Hier_DMO_Config_Affected_Entities 39
	EXEC dbo.Client_Hier_DMO_Config_Affected_Entities 45
		
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-01-25	Contract placeholder - CP-4 Created
	RR			2017-05-02	MAINT-5292 Added @Client_Hier_Id input to filter records of respective entity
******/

CREATE PROCEDURE [dbo].[Client_Hier_DMO_Config_Affected_Entities]
      ( 
       @Client_Hier_DMO_Config_Id INT
      ,@Client_Hier_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Is_DMO_Config_Dates_Valid INT = 1
      
      DECLARE
            @Client_Id INT
           ,@Sitegroup_Id INT
           ,@Site_Id INT
           ,@Commodity_Id INT
           ,@Commodity_Name VARCHAR(50)
      
      SELECT
            @Client_Id = NULLIF(ch.Client_Id, 0)
           ,@Sitegroup_Id = NULLIF(ch.Sitegroup_Id, 0)
           ,@Site_Id = NULLIF(ch.Site_Id, 0)
      FROM
            Core.Client_Hier ch
      WHERE
            ch.Client_Hier_Id = @Client_Hier_Id
            
      SELECT
            @Commodity_Id = comm.Commodity_Id
           ,@Commodity_Name = comm.Commodity_Name
      FROM
            dbo.Commodity comm
            INNER JOIN dbo.Client_Hier_DMO_Config cdc
                  ON comm.Commodity_Id = cdc.Commodity_Id
      WHERE
            cdc.Client_Hier_DMO_Config_Id = @Client_Hier_DMO_Config_Id
            
     
      SELECT
            ch.Client_Name
           ,ch.Sitegroup_Name
           ,ch.Site_name
           ,@Commodity_Name AS Commodity
           ,utlt.Account_Number AS Utility_Account_Number
           ,supp.Account_Number AS Supplier_Account_Number
           ,supp.Supplier_Account_begin_Dt
           ,supp.Supplier_Account_End_Dt
      FROM
            Core.Client_Hier_Account utlt
            INNER JOIN Core.Client_Hier ch
                  ON utlt.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN Core.Client_Hier_Account supp
                  ON utlt.Meter_Id = supp.Meter_Id
      WHERE
            ch.Client_Id = @Client_Id
            AND ( @Sitegroup_Id IS NULL
                  OR ch.Sitegroup_Id = @Sitegroup_Id )
            AND ( @Site_Id IS NULL
                  OR ch.Site_Id = @Site_Id )
            AND utlt.Account_Type = 'Utility'
            AND utlt.Commodity_Id = @Commodity_Id
            AND supp.Account_Type = 'Supplier'
            AND supp.Supplier_Contract_ID = -1
            AND EXISTS ( SELECT
                              1
                         FROM
                              dbo.Client_Hier_DMO_Config cdc
                         WHERE
                              cdc.Client_Hier_DMO_Config_Id = @Client_Hier_DMO_Config_Id
                              AND ( ( cdc.DMO_End_Dt IS NULL
                                      AND supp.Supplier_Account_End_Dt IS NULL
                                      AND cdc.DMO_Start_Dt <= supp.Supplier_Account_begin_Dt )
                                    OR ( cdc.DMO_End_Dt IS NULL
                                         AND supp.Supplier_Account_End_Dt IS NOT NULL
                                         AND cdc.DMO_Start_Dt <= supp.Supplier_Account_begin_Dt
                                         AND cdc.DMO_Start_Dt <= supp.Supplier_Account_End_Dt )
                                    OR ( cdc.DMO_End_Dt IS NOT NULL
                                         AND supp.Supplier_Account_End_Dt IS NOT NULL
                                         AND supp.Supplier_Account_begin_Dt BETWEEN cdc.DMO_Start_Dt
                                                                            AND     DATEADD(DD, -DATEPART(DD, cdc.DMO_End_Dt), DATEADD(mm, 1, cdc.DMO_End_Dt))
                                         AND supp.Supplier_Account_End_Dt BETWEEN cdc.DMO_Start_Dt
                                                                          AND     DATEADD(DD, -DATEPART(DD, cdc.DMO_End_Dt), DATEADD(mm, 1, cdc.DMO_End_Dt)) ) ) )
      GROUP BY
            ch.Client_Name
           ,ch.Sitegroup_Name
           ,ch.Site_name
           ,utlt.Account_Number
           ,supp.Account_Number
           ,supp.Supplier_Account_begin_Dt
           ,supp.Supplier_Account_End_Dt
          
           
END;
;




;
GO

GRANT EXECUTE ON  [dbo].[Client_Hier_DMO_Config_Affected_Entities] TO [CBMSApplication]
GO
