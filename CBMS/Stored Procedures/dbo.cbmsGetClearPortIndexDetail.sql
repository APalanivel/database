SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO


--Mark McCallon (Jon)
--1-25-07

CREATE procedure dbo.cbmsGetClearPortIndexDetail
	@clearportindexid int
as
begin
set nocount on
select   	
--distinct 
	
 ci.clearport_index
, cd.index_detail_date  
, cm.clearport_index_month  
, cd.index_detail_value  	

from clearport_index ci inner join 
clearport_index_months cm on cm.clearport_index_id = ci.clearport_index_id and cm.clearport_index_id=@clearportindexid
inner join clearport_index_detail cd on cd.clearport_index_month_id=cm.clearport_index_month_id
where --cm.clearport_index_id =@clearportindexid and
index_detail_date > dateadd(day,-5,getdate())
order by index_detail_date,clearport_index_month

end
--select * from clearport_index_months
--select * from clearport_index_detail
--3.68
GO
GRANT EXECUTE ON  [dbo].[cbmsGetClearPortIndexDetail] TO [CBMSApplication]
GO
