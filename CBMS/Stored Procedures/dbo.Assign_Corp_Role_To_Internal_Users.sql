SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME: dbo.Assign_Corp_Role_To_Internal_Users  
  
DESCRIPTION: To assign all the Summit internal users for the given Corporate Role Id 
			 as well storing all assignments into Queue table.
  
INPUT PARAMETERS:      
	Name			DataType     Default     Description      
------------------------------------------------------------------      
	@CorpRoleId      INT					 Corporate Role Id	
      
 OUTPUT PARAMETERS:
 Name              DataType          Default     Description      
------------------------------------------------------------      
   
USAGE EXAMPLES:  
------------------------------------------------------------  

	EXEC dbo.Assign_Corp_Role_To_Internal_Users 1275
  
AUTHOR INITIALS:
 Initials Name
------------------------------------------------------------
 PNR	  Pandarinath
 HG		  Harihara Suthan G

MODIFICATIONS  
  
 Initials	Date		Modification  
------------------------------------------------------------  
 PNR		11/19/2010	Created as part of project : Security Roles Administration
 PNR		12/20/2010  Replaced dbo.User_Security_Role_Queue with dbo.User_Security_Role table as part of Security Roles Administration.
 HG			01/12/2011	Exisitng code filtered the Internal users by Access_Level = 1 corrected it with Access_Level = 0.
******/

CREATE PROCEDURE dbo.Assign_Corp_Role_To_Internal_Users
    (
       @CorpRoleId	INT
    )
AS 
BEGIN  
	
	SET NOCOUNT ON ;
	
	INSERT INTO dbo.User_Security_Role
	(
		 Security_Role_Id
		,User_Info_Id
	)
	SELECT
		  @CorpRoleId 
		 ,User_Info_Id
	FROM 
		dbo.USER_INFO
	WHERE 
		ACCESS_LEVEL = 0 
		AND IS_HISTORY = 0
END
GO
GRANT EXECUTE ON  [dbo].[Assign_Corp_Role_To_Internal_Users] TO [CBMSApplication]
GO
