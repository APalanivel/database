SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UBM_GET_CASS_EXPECTED_DOLLAR_AMOUNT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@masterLogId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE dbo.UBM_GET_CASS_EXPECTED_DOLLAR_AMOUNT_P
	@masterLogId int
	AS
set nocount on
	SELECT 
		CAST(EXPECTED_DATA AS DECIMAL(20,3)) AS TOTAL_AMOUNT 
		 
	FROM 
		UBM_CASS_TABLE_COUNT 
	WHERE
		UBM_BATCH_MASTER_LOG_ID = @masterLogId
		AND CASS_TABLENAME = 'total_dollars'
GO
GRANT EXECUTE ON  [dbo].[UBM_GET_CASS_EXPECTED_DOLLAR_AMOUNT_P] TO [CBMSApplication]
GO
