SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsMarketPricePoint_LoadPricePoint_MonthlyOnePrice

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Market_Price_Point_ID	int       	          	
	@Market_Price_Point_Future_Date	smalldatetime	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

create        Procedure dbo.cbmsMarketPricePoint_LoadPricePoint_MonthlyOnePrice
	( @Market_Price_Point_ID int
	, @Market_Price_Point_Future_Date smalldatetime
	)
As
BEGIN

	set nocount on

	select Market_Price_Point_ID from Market_Price_Point_Detail where Market_Price_Point_ID=@Market_Price_Point_ID and Market_Price_Point_Future_Date = @Market_Price_Point_Future_Date

	
END
GO
GRANT EXECUTE ON  [dbo].[cbmsMarketPricePoint_LoadPricePoint_MonthlyOnePrice] TO [CBMSApplication]
GO
