SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                            
 NAME: dbo.Contract_Dates_Upd_By_Contract_Id         
                            
 DESCRIPTION:                            
   Update the contract end date based on Meter and Contract.                        
                            
 INPUT PARAMETERS:              
                         
 Name			 DataType         Default       Description            
------------------------------------------------------------------------------         
@Contract_Id     INT    
                            
 OUTPUT PARAMETERS:              
                               
 Name                        DataType         Default       Description            
------------------------------------------------------------------------------         
                            
 USAGE EXAMPLES:                                
------------------------------------------------------------------------------         
  
BEGIN TRAN

SELECT * FROM  dbo.CONTRACT c WHERE c.contract_id =172708


EXEC dbo.Contract_Dates_Upd_By_Contract_Id
    @Contract_Id = 172708
    , @Contract_End_Date = '2020-05-02'
    , @Original_Contract_End_Date = '2020-04-02'

SELECT * FROM  dbo.CONTRACT c WHERE c.contract_id =172708

ROLLBACK TRAN


BEGIN TRAN

SELECT * FROM  dbo.CONTRACT c WHERE c.contract_id =172707


EXEC dbo.Contract_Dates_Upd_By_Contract_Id
    @Contract_Id = 172707
    , @Contract_End_Date = '2018-02-28'
    , @Original_Contract_End_Date = '2018-01-31'

SELECT * FROM  dbo.CONTRACT c WHERE c.contract_id =172707

ROLLBACK TRAN



SELECT * FROM  dbo.CONTRACT c WHERE c.Original_Contract_End_Date IS NOT NULL
   
                           
 AUTHOR INITIALS:            
           
 Initials              Name            
------------------------------------------------------------------------------         
 NR						Narayana Reddy    
                             
 MODIFICATIONS:          
              
 Initials              Date             Modification          
------------------------------------------------------------------------------         
 NR                    2019-05-29       Created for - Add Contract.    
 NR					   2020-03-31		MAINT-10082 - Added condition if multiple times rolled we dont update original contract end date.
                          
******/
CREATE PROCEDURE [dbo].[Contract_Dates_Upd_By_Contract_Id]
    (
        @Contract_Id INT
        , @Contract_End_Date DATE
        , @Original_Contract_End_Date DATE = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;



        UPDATE
            c
        SET
            c.CONTRACT_END_DATE = @Contract_End_Date
            , Original_Contract_End_Date = CASE WHEN c.Original_Contract_End_Date IS NULL
                                                     AND @Original_Contract_End_Date IS NOT NULL THEN
                                                    @Original_Contract_End_Date
                                               ELSE c.Original_Contract_End_Date
                                           END
        FROM
            dbo.CONTRACT c
        WHERE
            c.CONTRACT_ID = @Contract_Id;


    END;


GO
GRANT EXECUTE ON  [dbo].[Contract_Dates_Upd_By_Contract_Id] TO [CBMSApplication]
GO
