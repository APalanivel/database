SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsSSOPublication_Search

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@publication_category_type_id	int       	null      	
	@full_text_keyword	varchar(200)	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE     PROCEDURE 
	[dbo].[cbmsSSOPublication_Search]

	( @MyAccountId int
	, @publication_category_type_id int = null
	, @full_text_keyword varchar(200) = null
	)
AS
BEGIN



	declare @functionname varchar(100)

	   select @functionname = pm.permission_name
	     from user_info_group_info_map ugmap
	     join group_info_permission_info_map gpmap on gpmap.group_info_id = ugmap.group_info_id
	     join permission_info pm on pm.permission_info_id = gpmap.permission_info_id
	    where ugmap.user_info_id = 128 --@MyAccountId
	    and pm.permission_name = 'sso.mis.publications.european.view'
	 order by pm.permission_name

	if @functionname is null
		begin
	
		select p.sso_publication_id
	   	, p.publication_title
		, p.publication_description
		, pc.entity_name publication_category_type
		, p.publication_date
		, p.full_text
		, p.cbms_image_id
	     from sso_publications p
	     join entity pc on pc.entity_id = p.publication_category_type_id
	    where p.publication_category_type_id = isNull(@publication_category_type_id, p.publication_category_type_id)
		and p.full_text like isNull(@full_text_keyword, '%%') 
		and pc.entity_name  <> 'European'
	 order by p.publication_date desc

		end 
		else
		begin
		
		


	select p.sso_publication_id
	   	, p.publication_title
		, p.publication_description
		, pc.entity_name publication_category_type
		, p.publication_date
		, p.full_text
		, p.cbms_image_id
	     from sso_publications p
	     join entity pc on pc.entity_id = p.publication_category_type_id
	    where p.publication_category_type_id = isNull(@publication_category_type_id, p.publication_category_type_id)
		and p.full_text like isNull(@full_text_keyword, '%%') 
	 order by p.publication_date desc
end

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSSOPublication_Search] TO [CBMSApplication]
GO
