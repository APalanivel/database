SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_UTILITY_SWITCH_TARRIF_TYPE_NAME_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@entityId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE     PROCEDURE DBO.SR_RFP_GET_UTILITY_SWITCH_TARRIF_TYPE_NAME_P

@entityId int

AS
set nocount on

select	ENTITY_NAME 

from	ENTITY

where	ENTITY_TYPE=1034
	and ENTITY_ID = @entityId
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_UTILITY_SWITCH_TARRIF_TYPE_NAME_P] TO [CBMSApplication]
GO
