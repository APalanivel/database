SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UPDATE_REALLOCATED_VOLUME_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(16)	          	
	@dealTicketVolumeDetailsId	int       	          	
	@allocatedVolume	decimal(32,16)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE      PROCEDURE DBO.UPDATE_REALLOCATED_VOLUME_DETAILS_P

@userId varchar(10),
@sessionId varchar(16),
@dealTicketVolumeDetailsId int, 
@allocatedVolume decimal(32,16)

AS
set nocount on
UPDATE 
	RM_DEAL_TICKET_VOLUME_DETAILS
SET 
	HEDGE_VOLUME = @allocatedVolume 
WHERE 
	RM_DEAL_TICKET_VOLUME_DETAILS_ID = @dealTicketVolumeDetailsId
GO
GRANT EXECUTE ON  [dbo].[UPDATE_REALLOCATED_VOLUME_DETAILS_P] TO [CBMSApplication]
GO
