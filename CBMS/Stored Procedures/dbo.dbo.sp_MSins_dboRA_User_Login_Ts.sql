SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[dbo.sp_MSins_dboRA_User_Login_Ts]
    @c1 int,
    @c2 int,
    @c3 datetime
as
begin  
	insert into [dbo].[RA_User_Login_Ts](
		[User_Login_Ts_Id],
		[USER_INFO_ID],
		[Login_Ts]
	) values (
    @c1,
    @c2,
    @c3	) 
end  
GO
