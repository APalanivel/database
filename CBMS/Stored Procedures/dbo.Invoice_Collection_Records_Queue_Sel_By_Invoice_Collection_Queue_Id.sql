SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:   dbo.Invoice_Collection_Records_Queue_Sel_By_Invoice_Collection_Queue_Id               
                      
Description:                      
                
                                   
 Input Parameters:                      
    Name        DataType   Default   Description                        
----------------------------------------------------------------------------------------                        
                               
         
 Output Parameters:                            
    Name        DataType   Default   Description                        
----------------------------------------------------------------------------------------                        
                      
 Usage Examples:                          
----------------------------------------------------------------------------------------           
DECLARE        
      @tvp_Invoice_Collection_Queue_Sel tvp_Invoice_Collection_Queue_Sel        
     ,@Tvp_Invoice_Collection_Sources tvp_Invoice_Collection_Source                
         
 INSERT     INTO @tvp_Invoice_Collection_Queue_Sel        
            ( ICO_User_Info_Id )        
 VALUES        
            (49)         
         
         
 --INSERT     INTO @Tvp_Invoice_Collection_Sources        
 --           ( Column_Type, Column_Name, Column_Value )        
 --VALUES        
 --           ( 'UBM', 'Invoice_Source_Type_Cd', '102292' )                    
 --   ,    ( 'Vendor', 'Invoice_Source_Type_Cd', '102293' )          
 --   ,    ( 'Vendor', 'Invoice_Source_Method_of_Contact_Cd', '102302' )            
 --   ,    ( 'Vendor', 'Invoice_Source_Type_Cd', '102297' )              
 --   ,    ( 'Client', 'Invoice_Source_Type_Cd', '102291' )              
 --   ,    ( 'Client', 'Invoice_Source_Method_of_Contact_Cd', '102299' )        
        
           
           
   Exec dbo.Invoice_Collection_Records_Queue_Sel_By_Invoice_Collection_Queue_Id          
      @tvp_Invoice_Collection_Queue_Sel        
     ,@Tvp_Invoice_Collection_Sources,'141,142,143,144'        
           
   EXEC dbo.Invoice_Collection_Records_Queue_Sel_By_Invoice_Collection_Queue_Id         
      @Invoice_Collection_Queue_Id = '100'        
           
        
        
   declare @p1 dbo.tvp_Invoice_Collection_Queue_Sel        
insert into @p1 values(55388,NULL,NULL,NULL,13791,NULL,NULL,NULL,NULL,NULL,N'Supplier',805,NULL,NULL,NULL,NULL,NULL,NULL,102349,NULL,NULL)        
        
exec dbo.Invoice_Collection_Records_Queue_Sel_By_Invoice_Collection_Queue_Id_321 @tvp_Invoice_Collection_Queue_Sel=@p1,@Invoice_Collection_Queue_Id=NULL,@Total_Count=NULL,@Status_Cd=NULL        
        
        
EXEC dbo.Invoice_Collection_Records_Queue_Sel_By_Invoice_Collection_Queue_Id           
      @Invoice_Collection_Queue_Id = '2789923,2789926'        
        
        
Author Initials:                      
    Initials  Name                      
----------------------------------------------------------------------------------------                        
 RKV    Ravi Kumar Vegesna        
 Modifications:                      
    Initials        Date   Modification                      
----------------------------------------------------------------------------------------                        
    RKV    2016-12-29  Created For Invoice_Collection.         
    RKV             2017-06-20  Maint-5484 Modified to hadle when we have consolidated billing end date is open.                 
    NR    2017-10-12  MAINT-5968 - Modifide To show the vendor type as "Supplier" When account is Consolidated and           
         Checked venodr type as "Supplier".          
 RKV    2017-10-20      MAINT-5989 updated the contact search filter        
 RKV             2017-11-22      MAINT - 6340 Modified the filter to consider only the supplier type contract vendors when         
         the meter has two contracts for the same time period(one is utility contract and the other one is supplier contract).        
    RKV    2018-01-05      MAINT - 6364 changed the invoice collection officer as an optional filter.        
         Made Invoice_Collection_Office_Id variable as a temp table because we will get multiple officers.        
            
    RKV             2018-03-16     MAINT-6958 performance tune         
 RKV    2018-04-24     MAINT-7159 implemeted sorting for all Columns         
    RKV             2018-05-25     MAINT-7417 Replaced Supplier_Account_begin_Dt with Supplier_Meter_Association_Date and Supplier_Account_End_Dt with Supplier_Meter_Disassociation_Date         
    RKV    2018-10-26    MAINT-7885 changed the date filter logic and increased the performance.         
    RKV    2018-11-29     MAINT-7982 Collecting the accounts into the temp table based on the selected client_id,vendor_id,vendor_type and used this table to collected the         
         invoice collection account config Ids          
 RKV            2019-02-10      MAINT-8362, Added missing execute statement for the dynamic query which is used to populate vendor details.            
 RKV      2019-06-28    IC-Project Changes added Last_Action_Date,Date_In_Queue,Is_Bloker,Bloker_Action_Date         
    RKV            2019-08-07    MAINT-9135  Add is_Blocker_Filter         
 RKV            2019-08-21    Added Next_Actio_Date,Download_Cnt         
 RKV      2019-09-12    performance tune        
 RKV      2019-09-27    PRSUPPORT-3092,Fixed the issue when the vendor type parameter is passed.       
 SLP      2019-09-27    Added the Is_Manual, Is_Not_Default_Vendor in the select   
 SLP      2019-10-16    Addded @IC_Account_Config_Group_Id filter & @Invoice_Request_Type_Cd @Invoice_Request_Type_Cd     
 RKV      2019-11-06    MAINT-9500,Added Row_number to filter the first_vendor name for the overlapped meter association/disassociation dates     
 SLP      2019-11-21   Fixed the performance issue when searched with Group Id only  
          included IC_Account_Config_Group_Id when Queue_ids are not passed  
 SLP      2019-11-27   MAINT-9561, to fix issue with "Person_to_chase" based upon the Invoice_collection_source marked as primary 
 NR		  2020-07-02   SE2017-1011 - Added @IC_Bots_Process_Id input and Added IC_Bots_Process_Name as output.
 
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Records_Queue_Sel_By_Invoice_Collection_Queue_Id]
    (
        @tvp_Invoice_Collection_Queue_Sel tvp_Invoice_Collection_Queue_Sel READONLY
        , @Tvp_Invoice_Collection_Sources tvp_Invoice_Collection_Source READONLY
        , @Invoice_Collection_Queue_Id VARCHAR(MAX) = NULL
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
        , @Total_Count INT = 0
        , @Status_Cd INT = NULL
        , @Sort_Col VARCHAR(50) = 'Client_Name'
        , @Sort_Order VARCHAR(15) = 'ASC'
        , @Is_Issue_Blocker BIT = NULL
        , @Last_Action_Start_Date DATE = NULL
        , @Last_Action_End_Date DATE = NULL
        , @Chase_Count VARCHAR(MAX) = NULL
        , @Next_Action_Start_Date DATE = NULL
        , @Next_Action_End_Date DATE = NULL
        , @Download_Count VARCHAR(MAX) = NULL
        , @Final_Review BIT = NULL
        , @Chase_Created_Start_Date DATE = NULL
        , @Chase_Created_End_Date DATE = NULL
        , @IC_Account_Config_Group_Id INT = NULL
        , @Invoice_Request_Type_Cd INT = NULL   --send only when Record_Type=Manual ICR   
        , @IC_Bots_Process_Id INT = NULL
    )
WITH EXECUTE AS OWNER, RECOMPILE
AS
    BEGIN
        SET NOCOUNT ON;



        DECLARE
            @Cnt_Of_Invoice_Collection_Source BIT = 0
            , @Invoice_Collection_Type_Cd INT
            , @Issue_Status_Cd INT
            , @Source_Type_Client INT
            , @Source_Type_Account INT
            , @Source_Type_Vendor INT
            , @Contact_Level_Client INT
            , @Contact_Level_Account INT
            , @Contact_Level_Vendor INT
            , @SQL_Statement NVARCHAR(MAX)
            , @SQL_Statement1 NVARCHAR(MAX)
            , @SQL_Statement2 NVARCHAR(MAX)
            , @SQL_Statement3 NVARCHAR(MAX)
            , @Row_Number_SQL_Statement NVARCHAR(MAX)
            , @Invoice_Collection_Issue_Status_Cd INT
            , @ICO_User_Info_Id INT = NULL
            , @Collection_Start_Date DATE = NULL
            , @Collection_End_Date DATE = NULL
            , @Priority_Cd INT = NULL
            , @Client_Id INT = NULL
            , @Site_Id INT = NULL
            , @Account_Id INT = NULL
            , @Country_Id INT = NULL
            , @State_Id INT = NULL
            , @Commodity_Id INT = NULL
            , @Vendor_Type CHAR(8) = NULL
            , @Vendor_Id INT = NULL
            , @Client_Contact_Info_Id INT = NULL
            , @Account_Contact_Info_Id INT = NULL
            , @Vendor_Contact_Info_Id INT = NULL
            , @Issue_Status VARCHAR(25) = NULL
            , @Previously_Chased BIT = NULL
            , @Record_Type_Cd INT = NULL
            , @ICR_Status_Cd INT = NULL
            , @Days_Since_Last_Chased_From DATE = NULL
            , @Days_Since_Last_Chased_To DATE = NULL
            , @Record_Type_Cd_Value VARCHAR(25) = NULL
            , @Supplier_Contract_Type_Id INT
            , @Supplier_Invoice_Vendor_Type_Id INT;


        CREATE TABLE #Invoice_Collection_Account_Config_Ids
             (
                 Invoice_Collection_Account_Config_Id INT
                 , Account_Id INT
                 , Invoice_Collection_Service_Start_Dt DATE
                 , Invoice_Collection_Service_End_Dt DATE
                 , PRIMARY KEY CLUSTERED
                   (
                       Invoice_Collection_Account_Config_Id
                   )
             );

        CREATE TABLE #Client_hier_Account_Details
             (
                 Account_Vendor_Name VARCHAR(200)
                 , Account_Type CHAR(8)
                 , Account_Vendor_Id INT
                 , Account_Id INT
                 , Invoice_Collection_Account_Config_Id INT
                 , Sno TINYINT
             );

        CREATE TABLE #Consolidated_Billing_Account_Id
             (
                 Account_Id INT
             );


        CREATE TABLE #Vendor_Account_Details
             (
                 Account_Vendor_Name VARCHAR(200)
                 , Account_Type CHAR(8)
                 , Account_Vendor_Id INT
                 , Account_Id INT
                 , Invoice_Collection_Account_Config_Id INT
             );


        CREATE TABLE #Ic_Account_Details
             (
                 Account_Vendor_Name VARCHAR(200)
                 , Account_Type CHAR(8)
                 , Account_Vendor_Id INT
                 , Account_Id INT
                 , Invoice_Collection_Account_Config_Id INT
             );

        CREATE TABLE #Invoice_Collection_Accounts
             (
                 Invoice_Collection_Account_Config_Id INT
                 , Account_Id INT
                 , Invoice_Collection_Service_End_Dt DATE
                 , Account_Vendor_Name VARCHAR(200)
                 , Account_Type CHAR(8)
                 , Account_Vendor_Id INT
             );

        CREATE TABLE #Invoice_Collection_Queue_Ids
             (
                 Invoice_Collection_Queue_Id INT NOT NULL PRIMARY KEY CLUSTERED
                 , Client_Id INT
                 , Client_Name VARCHAR(200)
                 , Country_Id INT
                 , Country_Name VARCHAR(200)
                 , Account_Type VARCHAR(8)
                 , Account_Vendor_Name VARCHAR(255)
                 , Account_Vendor_Id INT
                 , Account_Number VARCHAR(255)
                 , Account_Id INT
                 , Alternate_Account_Number VARCHAR(255)
                 , Invoice_collection_Officer VARCHAR(255)
                 , Invoice_collection_Officer_Id INT
                 , Person_to_chase VARCHAR(255)
                 , Invoice_Frequency VARCHAR(255)
                 , Previously_chased INT
                 , Issue VARCHAR(255)
                 , Is_Final_Reviewed VARCHAR(255)
                 , Is_Blocker INT
                 , Date_In_Queue DATE
                 , Last_Action_Date DATE
                 , Next_Action_Date DATE
                 , Download_Attempt_Cnt INT
                 , Has_Issue INT
                 , Invoice_Source VARCHAR(255)
                 , Invoice_Source_Type VARCHAR(25)
                 , Invoice_Source_Method_Of_Contact VARCHAR(25)
                 , Source_Vendor_Client VARCHAR(25)
                 , Invoice_Collection_Account_Config_Id INT
                 , period_to_chase VARCHAR(255)
                 , Period_Start_Date DATE
                 , Is_Manual BIT
                 , Is_Not_Default_Vendor BIT
                 , IC_Account_Config_Group_Name NVARCHAR(255)
                 , IC_Bots_Process_Name VARCHAR(200)
             );


        CREATE TABLE #Vendor_Dtls
             (
                 Account_Vendor_Name VARCHAR(200)
                 , Account_Vendor_Type CHAR(8)
                 , Account_Vendor_Id INT
                 , Invoice_Collection_Account_Config_Id INT
             );

        CREATE CLUSTERED INDEX cx_#Vendor_Dtls
            ON #Vendor_Dtls
        (
            Invoice_Collection_Account_Config_Id
        )   ;

        CREATE TABLE #Contact_Dtls
             (
                 Invoice_Collection_Account_Config_Id INT
                 , Invoice_Collection_Source_Cd INT
                 , Invoice_Source_Type_Cd INT
                 , Invoice_Source_Method_of_Contact_Cd INT
                 , Contact_Level_Cd INT
                 , Contact_Info_Id INT
                 , Is_Primary BIT
             );
        CREATE TABLE #Contact_Dtls_For_search
             (
                 Invoice_Collection_Account_Config_Id INT
                 , Invoice_Collection_Source_Cd INT
                 , Invoice_Source_Type_Cd INT
                 , Invoice_Source_Method_of_Contact_Cd INT
                 , Contact_Level_Cd INT
                 , Contact_Info_Id INT
             );

        CREATE CLUSTERED INDEX cx_#Contact_Dtls
            ON #Contact_Dtls
        (
            Invoice_Collection_Account_Config_Id
        )   ;

        SELECT
            @Invoice_Collection_Issue_Status_Cd = C.Code_Id
        FROM
            dbo.Code C
            INNER JOIN dbo.Codeset cs
                ON C.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Std_Column_Name = 'Invoice_Collection_Chase_Status_Cd'
            AND C.Code_Value = 'Open';

        SELECT
            @Supplier_Contract_Type_Id = ce.ENTITY_ID
        FROM
            dbo.ENTITY ce
        WHERE
            ce.ENTITY_NAME = 'Supplier'
            AND ce.ENTITY_TYPE = 129;

        SELECT
            @Supplier_Invoice_Vendor_Type_Id = ce.ENTITY_ID
        FROM
            dbo.ENTITY ce
        WHERE
            ce.ENTITY_NAME = 'Supplier'
            AND ce.ENTITY_TYPE = 155;

        SELECT
            @Cnt_Of_Invoice_Collection_Source = 1
        FROM
            @Tvp_Invoice_Collection_Sources tvp;

        SELECT
            @Issue_Status_Cd = C.Code_Id
        FROM
            dbo.Code C
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = C.Codeset_Id
        WHERE
            cs.Codeset_Name = 'IC Chase Status'
            AND C.Code_Value = 'Close';

        SELECT
            @Invoice_Collection_Type_Cd = C.Code_Id
        FROM
            dbo.Code C
            INNER JOIN dbo.Codeset cs
                ON C.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Std_Column_Name = 'Invoice_Collection_Type_Cd'
            AND C.Code_Value = 'ICR';


        SELECT
            @ICO_User_Info_Id = ICO_User_Info_Id
            , @Collection_Start_Date = Collection_Start_Date
            , @Collection_End_Date = Collection_End_Date
            , @Priority_Cd = Priority_Cd
            , @Client_Id = Client_Id
            , @Site_Id = Site_Id
            , @Account_Id = Account_Id
            , @Country_Id = Country_Id
            , @State_Id = State_Id
            , @Commodity_Id = Commodity_Id
            , @Vendor_Type = Vendor_Type
            , @Vendor_Id = Vendor_Id
            , @Client_Contact_Info_Id = Client_Contact_Info_Id
            , @Account_Contact_Info_Id = Account_Contact_Info_Id
            , @Vendor_Contact_Info_Id = Vendor_Contact_Info_Id
            , @Issue_Status = Issue_Status
            , @Previously_Chased = Previously_Chased
            , @Record_Type_Cd = Record_Type_Cd
            , @ICR_Status_Cd = ICR_Status_Cd
            , @Days_Since_Last_Chased_From = Days_Since_Last_Chased_From
            , @Days_Since_Last_Chased_To = Days_Since_Last_Chased_To
        FROM
            @tvp_Invoice_Collection_Queue_Sel;


        SELECT
            @Record_Type_Cd_Value = C.Code_Value
        FROM
            dbo.Code C
        WHERE
            C.Code_Id = @Record_Type_Cd;





        SELECT
            @Source_Type_Client = MAX(CASE WHEN C.Code_Value = 'Client Primary Contact' THEN C.Code_Id
                                      END)
            , @Source_Type_Account = MAX(CASE WHEN C.Code_Value = 'Account Primary Contact' THEN C.Code_Id
                                         END)
            , @Source_Type_Vendor = MAX(CASE WHEN C.Code_Value = 'Vendor Primary Contact' THEN C.Code_Id
                                        END)
        FROM
            dbo.Code C
            INNER JOIN dbo.Codeset cs
                ON C.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'InvoiceSourceType'
            AND C.Code_Value IN ( 'Account Primary Contact', 'Client Primary Contact', 'Vendor Primary Contact' );


        SELECT
            @Contact_Level_Client = MAX(CASE WHEN C.Code_Value = 'Client' THEN C.Code_Id
                                        END)
            , @Contact_Level_Account = MAX(CASE WHEN C.Code_Value = 'Account' THEN C.Code_Id
                                           END)
            , @Contact_Level_Vendor = MAX(CASE WHEN C.Code_Value = 'Vendor' THEN C.Code_Id
                                          END)
        FROM
            dbo.Code C
            INNER JOIN dbo.Codeset cs
                ON C.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'ContactLevel';


        INSERT INTO #Invoice_Collection_Account_Config_Ids
             (
                 Invoice_Collection_Account_Config_Id
                 , Account_Id
                 , Invoice_Collection_Service_Start_Dt
                 , Invoice_Collection_Service_End_Dt
             )
        SELECT
            icac.Invoice_Collection_Account_Config_Id
            , icac.Account_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt
        FROM
            dbo.Invoice_Collection_Account_Config icac
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = icac.Account_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            LEFT JOIN dbo.Invoice_Collection_Account_Config_Group_Map icacgm
                ON icac.Invoice_Collection_Account_Config_Id = icacgm.Invoice_Collection_Account_Config_Id
            LEFT JOIN dbo.Invoice_Collection_Account_Config_Group icacg
                ON icacg.Invoice_Collection_Account_Config_Group_ID = icacgm.Invoice_Collection_Account_Config_Group_Id
        WHERE
            (   @Client_Id IS NULL
                OR  (ch.Client_Id = @Client_Id))
            AND (   @ICO_User_Info_Id IS NULL
                    OR  icac.Invoice_Collection_Officer_User_Id = @ICO_User_Info_Id)
            AND (   @IC_Account_Config_Group_Id IS NULL
                    OR  icacg.Invoice_Collection_Account_Config_Group_ID = @IC_Account_Config_Group_Id)
            AND @Invoice_Collection_Queue_Id IS NULL
        GROUP BY
            icac.Invoice_Collection_Account_Config_Id
            , icac.Account_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt;

        INSERT INTO #Invoice_Collection_Account_Config_Ids
             (
                 Invoice_Collection_Account_Config_Id
                 , Account_Id
                 , Invoice_Collection_Service_Start_Dt
                 , Invoice_Collection_Service_End_Dt
             )
        SELECT
            icac.Invoice_Collection_Account_Config_Id
            , icac.Account_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt
        FROM
            dbo.Invoice_Collection_Account_Config icac
            INNER JOIN dbo.Invoice_Collection_Queue icq
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
        WHERE
            EXISTS (   SELECT
                            1
                       FROM
                            dbo.ufn_split(@Invoice_Collection_Queue_Id, ',') us
                       WHERE
                            us.Segments = icq.Invoice_Collection_Queue_Id)
            AND @Invoice_Collection_Queue_Id IS NOT NULL
        GROUP BY
            icac.Invoice_Collection_Account_Config_Id
            , icac.Account_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt;

        INSERT INTO #Invoice_Collection_Accounts
             (
                 Invoice_Collection_Account_Config_Id
                 , Account_Id
                 , Invoice_Collection_Service_End_Dt
                 , Account_Vendor_Name
                 , Account_Type
                 , Account_Vendor_Id
             )
        SELECT
            icac.Invoice_Collection_Account_Config_Id
            , icac.Account_Id
            , icac.Invoice_Collection_Service_End_Dt
            , ucha.Account_Vendor_Name
            , ucha.Account_Type
            , ucha.Account_Vendor_Id
        FROM
            dbo.Invoice_Collection_Account_Config icac
            INNER JOIN Core.Client_Hier_Account ucha
                ON icac.Account_Id = ucha.Account_Id
        WHERE
            ucha.Account_Type = 'Utility'
            AND EXISTS (   SELECT
                                1
                           FROM
                                #Invoice_Collection_Account_Config_Ids icac1
                           WHERE
                                icac.Invoice_Collection_Account_Config_Id = icac1.Invoice_Collection_Account_Config_Id)
            AND (   @ICO_User_Info_Id IS NULL
                    OR  icac.Invoice_Collection_Officer_User_Id = @ICO_User_Info_Id)
        GROUP BY
            icac.Invoice_Collection_Account_Config_Id
            , icac.Account_Id
            , icac.Invoice_Collection_Service_End_Dt
            , ucha.Account_Vendor_Name
            , ucha.Account_Type
            , ucha.Account_Vendor_Id;

        INSERT INTO #Client_hier_Account_Details
             (
                 Account_Vendor_Name
                 , Account_Type
                 , Account_Vendor_Id
                 , Account_Id
                 , Invoice_Collection_Account_Config_Id
                 , Sno
             )
        SELECT
            scha.Account_Vendor_Name
            , scha.Account_Type
            , scha.Account_Vendor_Id
            , ucha1.Account_Id
            , icac.Invoice_Collection_Account_Config_Id
            , ROW_NUMBER() OVER (PARTITION BY
                                     icac.Invoice_Collection_Account_Config_Id
                                 ORDER BY
                                     scha.Supplier_Meter_Association_Date) Sno
        FROM
            Core.Client_Hier_Account scha
            INNER JOIN Core.Client_Hier_Account ucha1
                ON ucha1.Meter_Id = scha.Meter_Id
                   AND  ucha1.Account_Type = 'Utility'
            INNER JOIN dbo.CONTRACT C
                ON C.CONTRACT_ID = scha.Supplier_Contract_ID
            INNER JOIN dbo.ENTITY ce
                ON C.CONTRACT_TYPE_ID = ce.ENTITY_ID
                   AND  ce.ENTITY_NAME = 'Supplier'
                   AND  scha.Account_Type = 'Supplier'
            INNER JOIN #Invoice_Collection_Accounts icac
                ON icac.Account_Id = ucha1.Account_Id
                   AND  (CASE WHEN icac.Invoice_Collection_Service_End_Dt IS NULL
                                   OR   icac.Invoice_Collection_Service_End_Dt > GETDATE() THEN GETDATE()
                             ELSE icac.Invoice_Collection_Service_End_Dt
                         END) BETWEEN scha.Supplier_Meter_Association_Date
                              AND     scha.Supplier_Meter_Disassociation_Date
        GROUP BY
            scha.Account_Vendor_Name
            , scha.Account_Type
            , scha.Account_Vendor_Id
            , ucha1.Account_Id
            , icac.Invoice_Collection_Account_Config_Id
            , scha.Supplier_Meter_Association_Date;



        INSERT INTO #Consolidated_Billing_Account_Id
             (
                 Account_Id
             )
        SELECT
            asbv1.Account_Id
        FROM
            dbo.Account_Consolidated_Billing_Vendor asbv1
            INNER JOIN dbo.ENTITY e1
                ON asbv1.Invoice_Vendor_Type_Id = e1.ENTITY_ID
                   AND  e1.ENTITY_NAME = 'Supplier'
            LEFT OUTER JOIN dbo.VENDOR v1
                ON v1.VENDOR_ID = asbv1.Supplier_Vendor_Id
        WHERE
            EXISTS (   SELECT
                            1
                       FROM
                            #Invoice_Collection_Accounts icac
                       WHERE
                            asbv1.Account_Id = icac.Account_Id)
        GROUP BY
            asbv1.Account_Id;


        INSERT INTO #Vendor_Account_Details
             (
                 Account_Vendor_Name
                 , Account_Type
                 , Account_Vendor_Id
                 , Account_Id
                 , Invoice_Collection_Account_Config_Id
             )
        SELECT
            v.VENDOR_NAME
            , E.ENTITY_NAME
            , v.VENDOR_ID
            , asbv.Account_Id
            , icac.Invoice_Collection_Account_Config_Id
        FROM
            dbo.Account_Consolidated_Billing_Vendor asbv
            INNER JOIN dbo.ENTITY E
                ON asbv.Invoice_Vendor_Type_Id = E.ENTITY_ID
                   AND  E.ENTITY_NAME = 'Supplier'
            LEFT OUTER JOIN dbo.VENDOR v
                ON v.VENDOR_ID = asbv.Supplier_Vendor_Id
            INNER JOIN #Invoice_Collection_Accounts icac
                ON icac.Account_Id = asbv.Account_Id
                   AND  (CASE WHEN icac.Invoice_Collection_Service_End_Dt IS NULL
                                   OR   asbv.Billing_End_Dt > GETDATE() THEN GETDATE()
                             ELSE icac.Invoice_Collection_Service_End_Dt
                         END) BETWEEN asbv.Billing_Start_Dt
                              AND     ISNULL(asbv.Billing_End_Dt, '9999-12-31')
        GROUP BY
            v.VENDOR_NAME
            , E.ENTITY_NAME
            , v.VENDOR_ID
            , asbv.Account_Id
            , icac.Invoice_Collection_Account_Config_Id;


        INSERT INTO #Ic_Account_Details
             (
                 Account_Vendor_Name
                 , Account_Type
                 , Account_Vendor_Id
                 , Account_Id
                 , Invoice_Collection_Account_Config_Id
             )
        SELECT
            icav.VENDOR_NAME
            , 'Supplier'
            , icav.VENDOR_ID
            , icac.Account_Id
            , aics.Invoice_Collection_Account_Config_Id
        FROM
            dbo.Invoice_Collection_Account_Contact icc
            INNER JOIN dbo.Account_Invoice_Collection_Source aics
                ON icc.Invoice_Collection_Account_Config_Id = aics.Invoice_Collection_Account_Config_Id
            INNER JOIN dbo.Contact_Info ci
                ON ci.Contact_Info_Id = icc.Contact_Info_Id
                   AND  (   (   @Source_Type_Client = aics.Invoice_Source_Type_Cd
                                AND @Contact_Level_Client = ci.Contact_Level_Cd)
                            OR  (   @Source_Type_Account = aics.Invoice_Source_Type_Cd
                                    AND @Contact_Level_Account = ci.Contact_Level_Cd)
                            OR  (   @Source_Type_Vendor = aics.Invoice_Source_Type_Cd
                                    AND @Contact_Level_Vendor = ci.Contact_Level_Cd))
            INNER JOIN dbo.Vendor_Contact_Map vcm
                ON ci.Contact_Info_Id = vcm.Contact_Info_Id
            INNER JOIN dbo.VENDOR icav
                ON icav.VENDOR_ID = vcm.VENDOR_ID
            INNER JOIN dbo.ENTITY ve
                ON icav.VENDOR_TYPE_ID = ve.ENTITY_ID
            INNER JOIN #Invoice_Collection_Accounts icac
                ON icc.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                   AND  icc.Is_Primary = 1
        GROUP BY
            icav.VENDOR_NAME
            , icav.VENDOR_ID
            , icac.Account_Id
            , aics.Invoice_Collection_Account_Config_Id;



        INSERT INTO #Vendor_Dtls
             (
                 Account_Vendor_Name
                 , Account_Vendor_Type
                 , Account_Vendor_Id
                 , Invoice_Collection_Account_Config_Id
             )
        SELECT
            avd.Account_Vendor_Name
            , avd.Account_Type
            , avd.Account_Vendor_Id
            , avd.Invoice_Collection_Account_Config_Id
        FROM
            #Client_hier_Account_Details avd
        WHERE
            EXISTS (   SELECT
                            1
                       FROM
                            #Consolidated_Billing_Account_Id cbai
                       WHERE
                            avd.Account_Id = cbai.Account_Id)
            AND avd.Sno = 1;



        INSERT INTO #Vendor_Dtls
             (
                 Account_Vendor_Name
                 , Account_Vendor_Type
                 , Account_Vendor_Id
                 , Invoice_Collection_Account_Config_Id
             )
        SELECT
            vad.Account_Vendor_Name
            , vad.Account_Type
            , vad.Account_Vendor_Id
            , vad.Invoice_Collection_Account_Config_Id
        FROM
            #Vendor_Account_Details vad
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                #Vendor_Dtls vd
                           WHERE
                                vad.Invoice_Collection_Account_Config_Id = vd.Invoice_Collection_Account_Config_Id);



        INSERT INTO #Vendor_Dtls
             (
                 Account_Vendor_Name
                 , Account_Vendor_Type
                 , Account_Vendor_Id
                 , Invoice_Collection_Account_Config_Id
             )
        SELECT
            icd.Account_Vendor_Name
            , icd.Account_Type
            , icd.Account_Vendor_Id
            , icd.Invoice_Collection_Account_Config_Id
        FROM
            #Ic_Account_Details icd
        WHERE
            EXISTS (   SELECT
                            1
                       FROM
                            #Consolidated_Billing_Account_Id cbai
                       WHERE
                            icd.Account_Id = cbai.Account_Id)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #Vendor_Dtls vd
                               WHERE
                                    icd.Invoice_Collection_Account_Config_Id = vd.Invoice_Collection_Account_Config_Id);



        INSERT INTO #Vendor_Dtls
             (
                 Account_Vendor_Name
                 , Account_Vendor_Type
                 , Account_Vendor_Id
                 , Invoice_Collection_Account_Config_Id
             )
        SELECT
            ica.Account_Vendor_Name
            , ica.Account_Type
            , ica.Account_Vendor_Id
            , ica.Invoice_Collection_Account_Config_Id
        FROM
            #Invoice_Collection_Accounts ica
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                #Vendor_Dtls vd
                           WHERE
                                ica.Invoice_Collection_Account_Config_Id = vd.Invoice_Collection_Account_Config_Id);

        --INSERT INTO #Contact_Dtls  
        --(  
        --    Invoice_Collection_Account_Config_Id,  
        --    Contact_Level_Cd,  
        --    Contact_Info_Id,  
        --    Is_Primary  
        --)  
        --SELECT icac.Invoice_Collection_Account_Config_Id,  

        --       ci.Contact_Level_Cd,  
        --       ci.Contact_Info_Id,  
        --       icc.Is_Primary  
        --FROM #Invoice_Collection_Account_Config_Ids icac  
        --    LEFT OUTER JOIN dbo.Invoice_Collection_Account_Contact icc  
        --        ON icac.Invoice_Collection_Account_Config_Id = icc.Invoice_Collection_Account_Config_Id  
        --           AND icc.Is_Primary = 1  
        --    LEFT OUTER JOIN dbo.Contact_Info ci  
        --        ON ci.Contact_Info_Id = icc.Contact_Info_Id;  

        INSERT INTO #Contact_Dtls
             (
                 Invoice_Collection_Account_Config_Id
                 , Invoice_Collection_Source_Cd
                 , Invoice_Source_Type_Cd
                 , Invoice_Source_Method_of_Contact_Cd
                 , Contact_Level_Cd
                 , Contact_Info_Id
                 , Is_Primary
             )
        SELECT
            icac.Invoice_Collection_Account_Config_Id
            , aics.Invoice_Collection_Source_Cd
            , aics.Invoice_Source_Type_Cd
            , aics.Invoice_Source_Method_of_Contact_Cd
            , MAX(ci.Contact_Level_Cd)
            , MAX(ci.Contact_Info_Id)
            , icc.Is_Primary
        FROM
            #Invoice_Collection_Account_Config_Ids icac
            LEFT OUTER JOIN dbo.Invoice_Collection_Account_Contact icc
                ON icac.Invoice_Collection_Account_Config_Id = icc.Invoice_Collection_Account_Config_Id
                   AND  icc.Is_Primary = 1
            LEFT OUTER JOIN dbo.Account_Invoice_Collection_Source aics
                ON icac.Invoice_Collection_Account_Config_Id = aics.Invoice_Collection_Account_Config_Id
                   AND  aics.Is_Primary = 1
            LEFT OUTER JOIN dbo.Contact_Info ci
                ON ci.Contact_Info_Id = icc.Contact_Info_Id
                   AND  (   (   @Source_Type_Client = aics.Invoice_Source_Type_Cd
                                AND @Contact_Level_Client = ci.Contact_Level_Cd)
                            OR  (   @Source_Type_Account = aics.Invoice_Source_Type_Cd
                                    AND @Contact_Level_Account = ci.Contact_Level_Cd)
                            OR  (   @Source_Type_Vendor = aics.Invoice_Source_Type_Cd
                                    AND @Contact_Level_Vendor = ci.Contact_Level_Cd))
        --WHERE EXISTS  
        --(  
        --    SELECT 1  
        --    FROM #Invoice_Collection_Account_Config_ids icac1  
        --    WHERE icac.Invoice_Collection_Account_Config_Id = icac1.Invoice_Collection_Account_Config_Id  
        --)  
        GROUP BY
            icac.Invoice_Collection_Account_Config_Id
            , aics.Invoice_Collection_Source_Cd
            , aics.Invoice_Source_Type_Cd
            , aics.Invoice_Source_Method_of_Contact_Cd
            , icc.Is_Primary;

        INSERT INTO #Contact_Dtls_For_search
             (
                 Invoice_Collection_Account_Config_Id
                 , Invoice_Collection_Source_Cd
                 , Invoice_Source_Type_Cd
                 , Invoice_Source_Method_of_Contact_Cd
                 , Contact_Level_Cd
                 , Contact_Info_Id
             )
        SELECT
            icac.Invoice_Collection_Account_Config_Id
            , aics.Invoice_Collection_Source_Cd
            , aics.Invoice_Source_Type_Cd
            , aics.Invoice_Source_Method_of_Contact_Cd
            , ci.Contact_Level_Cd
            , ci.Contact_Info_Id
        FROM
            dbo.Invoice_Collection_Account_Config icac
            INNER JOIN dbo.Invoice_Collection_Account_Contact icc
                ON icac.Invoice_Collection_Account_Config_Id = icc.Invoice_Collection_Account_Config_Id
            INNER JOIN dbo.Account_Invoice_Collection_Source aics
                ON icac.Invoice_Collection_Account_Config_Id = aics.Invoice_Collection_Account_Config_Id
            INNER JOIN dbo.Contact_Info ci
                ON ci.Contact_Info_Id = icc.Contact_Info_Id
                   AND  (   (   @Source_Type_Client = aics.Invoice_Source_Type_Cd
                                AND @Contact_Level_Client = ci.Contact_Level_Cd)
                            OR  (   @Source_Type_Account = aics.Invoice_Source_Type_Cd
                                    AND @Contact_Level_Account = ci.Contact_Level_Cd)
                            OR  (   @Source_Type_Vendor = aics.Invoice_Source_Type_Cd
                                    AND @Contact_Level_Vendor = ci.Contact_Level_Cd))
        WHERE
            EXISTS (   SELECT
                            1
                       FROM
                            #Invoice_Collection_Account_Config_Ids icac1
                       WHERE
                            icac.Invoice_Collection_Account_Config_Id = icac1.Invoice_Collection_Account_Config_Id);








        INSERT INTO #Invoice_Collection_Queue_Ids
             (
                 Invoice_Collection_Queue_Id
                 , Client_Id
                 , Client_Name
                 , Country_Id
                 , Country_Name
                 , Account_Type
                 , Account_Vendor_Name
                 , Account_Vendor_Id
                 , Account_Number
                 , Account_Id
                 , Alternate_Account_Number
                 , Invoice_collection_Officer
                 , Invoice_collection_Officer_Id
                 , Person_to_chase
                 , Invoice_Frequency
                 , Previously_chased
                 , Issue
                 , Is_Final_Reviewed
                 , Is_Blocker
                 , Date_In_Queue
                 , Last_Action_Date
                 , Next_Action_Date
                 , Download_Attempt_Cnt
                 , Has_Issue
                 , Invoice_Source
                 , Invoice_Source_Type
                 , Invoice_Source_Method_Of_Contact
                 , Source_Vendor_Client
                 , Invoice_Collection_Account_Config_Id
                 , period_to_chase
                 , Period_Start_Date
                 , IC_Account_Config_Group_Name
                 , IC_Bots_Process_Name
             )
        SELECT
            icq.Invoice_Collection_Queue_Id
            , ch.Client_Id
            , ch.Client_Name
            , ch.Country_Id
            , ch.Country_Name
            , CASE WHEN icq.Is_Not_Default_Vendor = 1 THEN ve.ENTITY_NAME
                  ELSE ISNULL(vd.Account_Vendor_Type, cha.Account_Type)
              END Account_Type
            , CASE WHEN icq.Is_Not_Default_Vendor = 1 THEN v.VENDOR_NAME
                  ELSE ISNULL(vd.Account_Vendor_Name, cha.Account_Vendor_Name)
              END Account_Vendor_Name
            , CASE WHEN icq.Is_Not_Default_Vendor = 1 THEN v.VENDOR_ID
                  ELSE ISNULL(vd.Account_Vendor_Id, cha.Account_Vendor_Id)
              END AS Account_Vendor_Id
            --, ISNULL(vd.Account_Vendor_Type, cha.Account_Type)  
            --         , ISNULL(vd.Account_Vendor_Name, cha.Account_Vendor_Name)  
            --         , ISNULL(vd.Account_Vendor_Id, cha.Account_Vendor_Id)  
            , cha.Account_Number
            , cha.Account_Id
            , icac.Invoice_Collection_Alternative_Account_Number
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME Invoice_collection_Officer
            , ui.USER_INFO_ID
            , ci.First_Name + ' ' + ci.Last_Name Person_to_chase
            , ISNULL(ifc.Code_Value, icgc.Code_Value) Invoice_Frequency
            , CASE WHEN lcd.Invoice_Collection_Queue_Id IS NOT NULL THEN CAST(lcd.Chase_Cnt AS VARCHAR(15))
                  ELSE 0
              END Previously_chased
            , CASE WHEN ici.Issue_Status_Cd = 1 THEN 'Y'
                  ELSE 'N'
              END Issue
            , CASE WHEN icfrlqm.Invoice_Collection_Queue_Id IS NOT NULL THEN 'Y'
                  ELSE 'N'
              END AS Is_Final_Reviewed
            , MAX(CASE WHEN ici.Is_Blocker = 1 THEN 1
                      ELSE 0
                  END) AS Is_Blocker
            , icq.Created_Ts Date_In_Queue
            , MAX(icq.Last_Change_Ts) Last_Action_Date
            , MAX(icq.Next_Action_Dt) Next_Action_Date
            , MAX(icq.Download_Attempt_Cnt) Download_Attempt_Cnt
            , CASE WHEN ici.Issue_Status_Cd = 1 THEN 1
                  ELSE 0
              END AS Has_Issue
            , cics.Code_Value AS Invoice_Source
            , cicst.Code_Value AS Invoice_Source_Type
            , cicsm.Code_Value AS Invoice_Source_Method_Of_Contact
            , cci.Code_Value Source_Vendor_Client
            , icac.Invoice_Collection_Account_Config_Id
            , CONVERT(VARCHAR(12), icq.Collection_Start_Dt, 105) + ' , '
              + CONVERT(VARCHAR(12), icq.Collection_End_Dt, 105) + '/' + LEFT(ISNULL(icmrt.Code_Value, ''), 1) period_to_chase
            , icq.Collection_Start_Dt
            , MAX(icacg.Invoice_Collection_Account_Config_Group_name)
            , ibp.IC_Bots_Process_Name
        FROM
            dbo.Invoice_Collection_Queue icq
            LEFT OUTER JOIN dbo.VENDOR v
                ON v.VENDOR_ID = icq.Manual_ICR_Vendor_Id
            LEFT OUTER JOIN dbo.ENTITY ve
                ON ve.ENTITY_ID = v.VENDOR_TYPE_ID
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN #Invoice_Collection_Account_Config_Ids icaci
                ON icac.Invoice_Collection_Account_Config_Id = icaci.Invoice_Collection_Account_Config_Id
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = icac.Account_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN dbo.Commodity com
                ON com.Commodity_Id = cha.Commodity_Id
            INNER JOIN dbo.Code cpc
                ON cpc.Code_Id = icac.Chase_Priority_Cd
            INNER JOIN dbo.USER_INFO ui
                ON ui.USER_INFO_ID = icac.Invoice_Collection_Officer_User_Id
            INNER JOIN dbo.Invoice_Collection_Queue_Month_Map icqmm
                ON icq.Invoice_Collection_Queue_Id = icqmm.Invoice_Collection_Queue_Id
            INNER JOIN(dbo.Account_Invoice_Collection_Month aicm
                       LEFT OUTER JOIN dbo.Account_Invoice_Collection_Frequency aicfe
                           ON aicm.Account_Invoice_Collection_Frequency_Id = aicfe.Account_Invoice_Collection_Frequency_Id
                       LEFT OUTER JOIN dbo.Code ifc
                           ON ifc.Code_Id = aicfe.Invoice_Frequency_Cd
                       LEFT OUTER JOIN dbo.Invoice_Collection_Global_Config_Value icgcv
                           ON aicm.Invoice_Collection_Global_Config_Value_Id = icgcv.Invoice_Collection_Global_Config_Value_Id
                       LEFT OUTER JOIN dbo.Code icgc
                           ON icgc.Code_Id = icgcv.Invoice_Frequency_Cd)
                ON icqmm.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id
            LEFT OUTER JOIN #Vendor_Dtls vd
                ON vd.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            LEFT OUTER JOIN(#Contact_Dtls cd
                            LEFT OUTER JOIN dbo.Contact_Info ci
                                ON ci.Contact_Info_Id = cd.Contact_Info_Id
                                   AND  ci.Contact_Level_Cd = cd.Contact_Level_Cd
                            LEFT OUTER JOIN dbo.Code cci
                                ON cci.Code_Id = ci.Contact_Level_Cd
            --INNER JOIN  
            --(  
            --    SELECT MAX(Contact_Info_Id) contact_Info_id,  
            --           cd2.Invoice_Collection_Account_Config_Id  
            --    FROM #Contact_Dtls cd2  
            --    GROUP BY cd2.Invoice_Collection_Account_Config_Id  
            --) icmc  
            --    ON icmc.contact_Info_id = ci.Contact_Info_Id  
            --       AND icmc.Invoice_Collection_Account_Config_Id = cd.Invoice_Collection_Account_Config_Id  
            )
                ON cd.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            LEFT OUTER JOIN dbo.Account_Invoice_Collection_Source aics
                ON aics.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                   AND  aics.Is_Primary = 1
            LEFT OUTER JOIN dbo.Code cics
                -- ON cics.Code_Id = aics.Invoice_Collection_Source_Cd  
                ON cics.Code_Id = cd.Invoice_Collection_Source_Cd
            LEFT OUTER JOIN dbo.Code cicst
                --ON cicst.Code_Id = aics.Invoice_Source_Type_Cd  
                ON cicst.Code_Id = cd.Invoice_Source_Type_Cd
            LEFT OUTER JOIN dbo.Code cicsm
                -- ON cicsm.Code_Id = aics.Invoice_Source_Method_of_Contact_Cd  
                ON cicsm.Code_Id = cd.Invoice_Source_Method_of_Contact_Cd
            LEFT OUTER JOIN(dbo.Invoice_Collection_Chase_Log_Queue_Map icccm
                            INNER JOIN dbo.Invoice_Collection_Chase_Log iccc
                                ON icccm.Invoice_Collection_Chase_Log_Id = iccc.Invoice_Collection_Chase_Log_Id
                            INNER JOIN dbo.Code sc
                                ON sc.Code_Id = iccc.Status_Cd
                                   AND  sc.Code_Value = 'Close')
                ON icccm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            LEFT OUTER JOIN(dbo.Invoice_Collection_Final_Review_Log_Queue_Map icfrlqm
                            INNER JOIN dbo.Invoice_Collection_Final_Review_Log icfrl
                                ON icfrlqm.Invoice_Collection_Final_Review_Log_Id = icfrl.Invoice_Collection_Final_Review_Log_Id
                            INNER JOIN dbo.Code frsc
                                ON frsc.Code_Id = icfrl.Status_Cd
                                   AND  frsc.Code_Value = 'Close')
                ON icfrlqm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            LEFT OUTER JOIN (   SELECT
                                    MAX(iccc.Last_Change_Ts) Last_Change_Ts
                                    , icccm.Invoice_Collection_Queue_Id
                                    , COUNT(iccc.Invoice_Collection_Chase_Log_Id) Chase_Cnt
                                FROM
                                    dbo.Invoice_Collection_Chase_Log_Queue_Map icccm
                                    INNER JOIN dbo.Invoice_Collection_Chase_Log iccc
                                        ON icccm.Invoice_Collection_Chase_Log_Id = iccc.Invoice_Collection_Chase_Log_Id
                                    INNER JOIN dbo.Code sc
                                        ON sc.Code_Id = iccc.Status_Cd
                                           AND  sc.Code_Value = 'Close'
                                GROUP BY
                                    icccm.Invoice_Collection_Queue_Id) lcd
                ON icccm.Invoice_Collection_Queue_Id = lcd.Invoice_Collection_Queue_Id
            LEFT OUTER JOIN (   SELECT
                                    Invoice_Collection_Queue_Id
                                    , MAX(CASE WHEN Issue_Status_Cd = @Invoice_Collection_Issue_Status_Cd THEN 1
                                              ELSE 0
                                          END) AS Issue_Status_Cd
                                    , MAX(CASE WHEN Is_Blocker = 1 THEN 1
                                              ELSE 0
                                          END) Is_Blocker
                                FROM
                                    dbo.Invoice_Collection_Issue_Log
                                GROUP BY
                                    Invoice_Collection_Queue_Id) ici
                ON ici.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            LEFT OUTER JOIN dbo.Code icisc
                ON icisc.Code_Id = ici.Issue_Status_Cd
            LEFT OUTER JOIN dbo.Code icmrt
                ON icmrt.Code_Id = icq.Invoice_Request_Type_Cd
            LEFT OUTER JOIN dbo.Invoice_Collection_Account_Config_Group_Map icagm
                ON icagm.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            LEFT OUTER JOIN dbo.Invoice_Collection_Account_Config_Group icacg
                ON icagm.Invoice_Collection_Account_Config_Group_Id = icacg.Invoice_Collection_Account_Config_Group_ID
            LEFT OUTER JOIN dbo.IC_Bots_Process_Invoice_Collection_Account_Config_Map ibpicacm
                ON ibpicacm.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            LEFT OUTER JOIN dbo.IC_Bots_Process ibp
                ON ibp.IC_Bots_Process_Id = ibpicacm.IC_Bots_Process_Id
        WHERE
            @Invoice_Collection_Queue_Id IS NOT NULL
            AND EXISTS (   SELECT
                                1
                           FROM
                                dbo.ufn_split(@Invoice_Collection_Queue_Id, ',') ufn
                           WHERE
                                icq.Invoice_Collection_Queue_Id = ufn.Segments)
            AND (   @IC_Bots_Process_Id IS NULL
                    OR  ibpicacm.IC_Bots_Process_Id = @IC_Bots_Process_Id)
        GROUP BY
            icq.Invoice_Collection_Queue_Id
            , ch.Client_Id
            , ch.Client_Name
            , ch.Country_Id
            , ch.Country_Name
            , ici.Issue_Status_Cd
            , CASE WHEN icq.Is_Not_Default_Vendor = 1 THEN ve.ENTITY_NAME
                  ELSE ISNULL(vd.Account_Vendor_Type, cha.Account_Type)
              END
            , CASE WHEN icq.Is_Not_Default_Vendor = 1 THEN v.VENDOR_NAME
                  ELSE ISNULL(vd.Account_Vendor_Name, cha.Account_Vendor_Name)
              END
            , CASE WHEN icq.Is_Not_Default_Vendor = 1 THEN v.VENDOR_ID
                  ELSE ISNULL(vd.Account_Vendor_Id, cha.Account_Vendor_Id)
              END
            --, ISNULL(vd.Account_Vendor_Type, cha.Account_Type)  
            --, ISNULL(vd.Account_Vendor_Name, cha.Account_Vendor_Name)  
            --, ISNULL(vd.Account_Vendor_Id, cha.Account_Vendor_Id)  
            , cha.Account_Number
            , cha.Account_Id
            , icac.Invoice_Collection_Alternative_Account_Number
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME
            , ui.USER_INFO_ID
            , ci.First_Name + ' ' + ci.Last_Name
            , ISNULL(ifc.Code_Value, icgc.Code_Value)
            , CASE WHEN lcd.Invoice_Collection_Queue_Id IS NOT NULL THEN CAST(lcd.Chase_Cnt AS VARCHAR(15))
                  ELSE 0
              END
            , CASE WHEN ici.Issue_Status_Cd = 1 THEN 'Y'
                  ELSE 'N'
              END
            , CASE WHEN icfrlqm.Invoice_Collection_Queue_Id IS NOT NULL THEN 'Y'
                  ELSE 'N'
              END
            --, MAX(CASE WHEN ici.Is_Blocker = 1 THEN 1        
            --          ELSE 0        
            --      END)        
            , icq.Created_Ts
            , CASE WHEN ici.Invoice_Collection_Queue_Id IS NOT NULL THEN 1
                  ELSE 0
              END
            , cics.Code_Value
            , cicst.Code_Value
            , cicsm.Code_Value
            , cci.Code_Value
            , icac.Invoice_Collection_Account_Config_Id
            , icq.Collection_End_Dt
            , icmrt.Code_Value
            , icq.Collection_Start_Dt
            , ibp.IC_Bots_Process_Name;



        INSERT INTO #Invoice_Collection_Queue_Ids
             (
                 Invoice_Collection_Queue_Id
                 , Client_Id
                 , Client_Name
                 , Country_Id
                 , Country_Name
                 , Account_Type
                 , Account_Vendor_Name
                 , Account_Vendor_Id
                 , Account_Number
                 , Account_Id
                 , Alternate_Account_Number
                 , Invoice_collection_Officer
                 , Invoice_collection_Officer_Id
                 , Person_to_chase
                 , Invoice_Frequency
                 , Previously_chased
                 , Issue
                 , Is_Final_Reviewed
                 , Is_Blocker
                 , Date_In_Queue
                 , Last_Action_Date
                 , Next_Action_Date
                 , Download_Attempt_Cnt
                 , Has_Issue
                 , Invoice_Source
                 , Invoice_Source_Type
                 , Invoice_Source_Method_Of_Contact
                 , Source_Vendor_Client
                 , Invoice_Collection_Account_Config_Id
                 , period_to_chase
                 , Period_Start_Date
                 , Is_Manual
                 , Is_Not_Default_Vendor
                 , IC_Account_Config_Group_Name
                 , IC_Bots_Process_Name
             )
        SELECT
            icq.Invoice_Collection_Queue_Id
            , ch.Client_Id
            , ch.Client_Name
            , ch.Country_Id
            , ch.Country_Name
            , CASE WHEN icq.Is_Not_Default_Vendor = 1 THEN ve.ENTITY_NAME
                  ELSE ISNULL(vd.Account_Vendor_Type, cha.Account_Type)
              END Account_Type
            , CASE WHEN icq.Is_Not_Default_Vendor = 1 THEN v.VENDOR_NAME
                  ELSE ISNULL(vd.Account_Vendor_Name, cha.Account_Vendor_Name)
              END Account_Vendor_Name
            , CASE WHEN icq.Is_Not_Default_Vendor = 1 THEN v.VENDOR_ID
                  ELSE ISNULL(vd.Account_Vendor_Id, cha.Account_Vendor_Id)
              END AS Account_Vendor_Id
            --, ISNULL(vd.Account_Vendor_Type, cha.Account_Type)  
            --        , ISNULL(vd.Account_Vendor_Name, cha.Account_Vendor_Name)  
            --         , ISNULL(vd.Account_Vendor_Id, cha.Account_Vendor_Id)  
            , cha.Account_Number
            , cha.Account_Id
            , icac.Invoice_Collection_Alternative_Account_Number
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME Invoice_collection_Officer
            , ui.USER_INFO_ID
            , ci.First_Name + ' ' + ci.Last_Name Person_to_chase
            , ISNULL(ifc.Code_Value, icgc.Code_Value) Invoice_Frequency
            , CASE WHEN lcd.Invoice_Collection_Queue_Id IS NOT NULL THEN CAST(lcd.Chase_Cnt AS VARCHAR(15))
                  ELSE 0
              END Previously_chased
            , CASE WHEN ici.Issue_Status_Cd = 1 THEN 'Y'
                  ELSE 'N'
              END Issue
            , CASE WHEN icfrlqm.Invoice_Collection_Queue_Id IS NOT NULL THEN 'Y'
                  ELSE 'N'
              END AS Is_Final_Reviewed
            , MAX(CASE WHEN ici.Is_Blocker = 1 THEN 1
                      ELSE 0
                  END) AS Is_Blocker
            , icq.Created_Ts Date_In_Queue
            , MAX(icq.Last_Change_Ts) Last_Action_Date
            , MAX(icq.Next_Action_Dt) Next_Action_Date
            , MAX(icq.Download_Attempt_Cnt) Download_Attempt_Cnt
            , CASE WHEN ici.Issue_Status_Cd = 1 THEN 1
                  ELSE 0
              END AS Has_Issue
            , cics.Code_Value AS Invoice_Source
            , cicst.Code_Value AS Invoice_Source_Type
            , cicsm.Code_Value AS Invoice_Source_Method_Of_Contact
            , cci.Code_Value Source_Vendor_Client
            , icac.Invoice_Collection_Account_Config_Id
            , CONVERT(VARCHAR(12), icq.Collection_Start_Dt, 105) + ' , '
              + CONVERT(VARCHAR(12), icq.Collection_End_Dt, 105) + '/' + LEFT(ISNULL(icmrt.Code_Value, ''), 1) period_to_chase
            , icq.Collection_Start_Dt
            , icq.Is_Manual
            , icq.Is_Not_Default_Vendor
            , MAX(icacg.Invoice_Collection_Account_Config_Group_name)
            , ibp.IC_Bots_Process_Name
        FROM
            dbo.Invoice_Collection_Queue icq
            LEFT OUTER JOIN dbo.VENDOR v
                ON v.VENDOR_ID = icq.Manual_ICR_Vendor_Id
            LEFT OUTER JOIN dbo.ENTITY ve
                ON ve.ENTITY_ID = v.VENDOR_TYPE_ID
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN #Invoice_Collection_Account_Config_Ids icaci
                ON icac.Invoice_Collection_Account_Config_Id = icaci.Invoice_Collection_Account_Config_Id
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = icac.Account_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN dbo.Commodity com
                ON com.Commodity_Id = cha.Commodity_Id
            INNER JOIN dbo.Code cpc
                ON cpc.Code_Id = icac.Chase_Priority_Cd
            INNER JOIN dbo.USER_INFO ui
                ON ui.USER_INFO_ID = icac.Invoice_Collection_Officer_User_Id
            INNER JOIN dbo.Invoice_Collection_Queue_Month_Map icqmm
                ON icq.Invoice_Collection_Queue_Id = icqmm.Invoice_Collection_Queue_Id
            INNER JOIN(dbo.Account_Invoice_Collection_Month aicm
                       LEFT OUTER JOIN dbo.Account_Invoice_Collection_Frequency aicfe
                           ON aicm.Account_Invoice_Collection_Frequency_Id = aicfe.Account_Invoice_Collection_Frequency_Id
                       LEFT OUTER JOIN dbo.Code ifc
                           ON ifc.Code_Id = aicfe.Invoice_Frequency_Cd
                       LEFT OUTER JOIN dbo.Invoice_Collection_Global_Config_Value icgcv
                           ON aicm.Invoice_Collection_Global_Config_Value_Id = icgcv.Invoice_Collection_Global_Config_Value_Id
                       LEFT OUTER JOIN dbo.Code icgc
                           ON icgc.Code_Id = icgcv.Invoice_Frequency_Cd)
                ON icqmm.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id
            LEFT OUTER JOIN #Vendor_Dtls vd
                ON vd.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            LEFT OUTER JOIN(#Contact_Dtls cd
                            LEFT OUTER JOIN dbo.Contact_Info ci
                                ON ci.Contact_Info_Id = cd.Contact_Info_Id
                                   AND  ci.Contact_Level_Cd = cd.Contact_Level_Cd
                            LEFT OUTER JOIN dbo.Code cci
                                ON cci.Code_Id = ci.Contact_Level_Cd
            --INNER JOIN  
            --(  
            --    SELECT MAX(Contact_Info_Id) contact_Info_id,  
            --           cd2.Invoice_Collection_Account_Config_Id  
            --    FROM #Contact_Dtls cd2  
            --    GROUP BY cd2.Invoice_Collection_Account_Config_Id  
            --) icmc  
            --    ON icmc.contact_Info_id = ci.Contact_Info_Id  
            --       AND icmc.Invoice_Collection_Account_Config_Id = cd.Invoice_Collection_Account_Config_Id  
            )
                ON cd.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            --LEFT OUTER JOIN dbo.Account_Invoice_Collection_Source aics  
            --    ON aics.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id  
            --       AND aics.Is_Primary = 1  
            LEFT OUTER JOIN dbo.Code cics
                --ON cics.Code_Id = aics.Invoice_Collection_Source_Cd  
                ON cics.Code_Id = cd.Invoice_Collection_Source_Cd
            LEFT OUTER JOIN dbo.Code cicst
                --ON cicst.Code_Id = aics.Invoice_Source_Type_Cd  
                ON cicst.Code_Id = cd.Invoice_Source_Type_Cd
            LEFT OUTER JOIN dbo.Code cicsm
                -- ON cicsm.Code_Id = aics.Invoice_Source_Method_of_Contact_Cd  
                ON cicsm.Code_Id = cd.Invoice_Source_Method_of_Contact_Cd
            LEFT OUTER JOIN(dbo.Invoice_Collection_Chase_Log_Queue_Map icccm
                            INNER JOIN dbo.Invoice_Collection_Chase_Log iccc
                                ON icccm.Invoice_Collection_Chase_Log_Id = iccc.Invoice_Collection_Chase_Log_Id
                            INNER JOIN dbo.Code sc
                                ON sc.Code_Id = iccc.Status_Cd
                                   AND  sc.Code_Value = 'Close')
                ON icccm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            LEFT OUTER JOIN(dbo.Invoice_Collection_Final_Review_Log_Queue_Map icfrlqm
                            INNER JOIN dbo.Invoice_Collection_Final_Review_Log icfrl
                                ON icfrlqm.Invoice_Collection_Final_Review_Log_Id = icfrl.Invoice_Collection_Final_Review_Log_Id
                            INNER JOIN dbo.Code frsc
                                ON frsc.Code_Id = icfrl.Status_Cd
                                   AND  frsc.Code_Value = 'Close')
                ON icfrlqm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            LEFT OUTER JOIN (   SELECT
                                    MAX(iccc.Last_Change_Ts) Last_Change_Ts
                                    , icccm.Invoice_Collection_Queue_Id
                                    , COUNT(iccc.Invoice_Collection_Chase_Log_Id) Chase_Cnt
                                FROM
                                    dbo.Invoice_Collection_Chase_Log_Queue_Map icccm
                                    INNER JOIN dbo.Invoice_Collection_Chase_Log iccc
                                        ON icccm.Invoice_Collection_Chase_Log_Id = iccc.Invoice_Collection_Chase_Log_Id
                                    INNER JOIN dbo.Code sc
                                        ON sc.Code_Id = iccc.Status_Cd
                                           AND  sc.Code_Value = 'Close'
                                GROUP BY
                                    icccm.Invoice_Collection_Queue_Id) lcd
                ON icccm.Invoice_Collection_Queue_Id = lcd.Invoice_Collection_Queue_Id
            LEFT OUTER JOIN (   SELECT
                                    Invoice_Collection_Queue_Id
                                    , MAX(CASE WHEN Issue_Status_Cd = @Invoice_Collection_Issue_Status_Cd THEN 1
                                              ELSE 0
                                          END) AS Issue_Status_Cd
                                    , MAX(CASE WHEN Is_Blocker = 1 THEN 1
                                              ELSE 0
                                          END) Is_Blocker
                                FROM
                                    dbo.Invoice_Collection_Issue_Log
                                GROUP BY
                                    Invoice_Collection_Queue_Id) ici
                ON ici.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            LEFT OUTER JOIN dbo.Code icisc
                ON icisc.Code_Id = ici.Issue_Status_Cd
            LEFT OUTER JOIN dbo.Code icmrt
                ON icmrt.Code_Id = icq.Invoice_Request_Type_Cd
            LEFT OUTER JOIN dbo.Invoice_Collection_Account_Config_Group_Map icagm
                ON icagm.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            LEFT OUTER JOIN dbo.Invoice_Collection_Account_Config_Group icacg
                ON icagm.Invoice_Collection_Account_Config_Group_Id = icacg.Invoice_Collection_Account_Config_Group_ID
            LEFT OUTER JOIN dbo.IC_Bots_Process_Invoice_Collection_Account_Config_Map ibpicacm
                ON ibpicacm.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            LEFT OUTER JOIN dbo.IC_Bots_Process ibp
                ON ibp.IC_Bots_Process_Id = ibpicacm.IC_Bots_Process_Id
        WHERE
            icq.Invoice_Collection_Queue_Type_Cd = @Invoice_Collection_Type_Cd
            AND @Invoice_Collection_Queue_Id IS NULL
            AND (   icac.Is_Chase_Activated = 1
                    OR  (   icac.Is_Chase_Activated = 0
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.Invoice_Collection_Queue icq1
                                                INNER JOIN dbo.Code sc
                                                    ON sc.Code_Id = icq.Status_Cd
                                           WHERE
                                                icq1.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND sc.Code_Value = 'Open')))
            AND (   @ICO_User_Info_Id IS NULL
                    OR  icac.Invoice_Collection_Officer_User_Id = @ICO_User_Info_Id)
            AND (   (   @Collection_Start_Date IS NULL
                        AND @Collection_End_Date IS NULL)
                    OR  (   @Collection_Start_Date IS NOT NULL
                            AND @Collection_End_Date IS NULL
                            AND @Collection_Start_Date <= icq.Collection_Start_Dt)
                    OR  (   @Collection_Start_Date IS NULL
                            AND @Collection_End_Date IS NOT NULL
                            AND @Collection_End_Date >= icq.Collection_End_Dt)
                    OR  (   @Collection_Start_Date IS NOT NULL
                            AND @Collection_End_Date IS NOT NULL
                            AND (   @Collection_Start_Date <= icq.Collection_Start_Dt
                                    AND @Collection_End_Date >= icq.Collection_End_Dt)))
            AND (   @Priority_Cd IS NULL
                    OR  cpc.Code_Id = @Priority_Cd)
            AND (   @Client_Id IS NULL
                    OR  ch.Client_Id = @Client_Id)
            AND (   @Site_Id IS NULL
                    OR  ch.Site_Id = @Site_Id)
            AND (   @Account_Id IS NULL
                    OR  cha.Account_Id = @Account_Id)
            AND (   @Country_Id IS NULL
                    OR  ch.Country_Id = @Country_Id)
            AND (   @State_Id IS NULL
                    OR  ch.State_Id = @State_Id)
            AND (   @Commodity_Id IS NULL
                    OR  cha.Commodity_Id = @Commodity_Id)
            AND (   @Vendor_Type IS NULL
                    OR  ISNULL(vd.Account_Vendor_Type, cha.Account_Type) = @Vendor_Type)
            AND (   @Next_Action_Start_Date IS NULL
                    OR  icq.Next_Action_Dt >= @Next_Action_Start_Date)
            AND (   @Next_Action_End_Date IS NULL
                    OR  icq.Next_Action_Dt <= @Next_Action_End_Date
                        AND icq.Next_Action_Dt > '1900-01-01')
            AND (   @Last_Action_Start_Date IS NULL
                    OR  CAST(icq.Last_Change_Ts AS DATE) >= @Last_Action_Start_Date)
            AND (   @Last_Action_End_Date IS NULL
                    OR  CAST(icq.Last_Change_Ts AS DATE) <= @Last_Action_End_Date
                        AND icq.Last_Change_Ts > '1900-01-01')
            AND (   @Chase_Created_Start_Date IS NULL
                    OR  CAST(iccc.Created_Ts AS DATE) >= @Chase_Created_Start_Date)
            AND (   @Chase_Created_End_Date IS NULL
                    OR  CAST(iccc.Created_Ts AS DATE) <= @Chase_Created_End_Date
                        AND iccc.Created_Ts > '1900-01-01')
            AND (   @Vendor_Id IS NULL
                    OR  ISNULL(vd.Account_Vendor_Id, cha.Account_Vendor_Id) = @Vendor_Id)
            AND (   @Client_Contact_Info_Id IS NULL
                    OR  (EXISTS (   SELECT
                                        1
                                    FROM
                                        #Contact_Dtls_For_search cdfs
                                    WHERE
                                        cdfs.Contact_Info_Id = @Client_Contact_Info_Id
                                        AND cdfs.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id)))
            AND (   @Account_Contact_Info_Id IS NULL
                    OR  (EXISTS (   SELECT
                                        1
                                    FROM
                                        #Contact_Dtls_For_search cdfs
                                    WHERE
                                        cdfs.Contact_Info_Id = @Account_Contact_Info_Id
                                        AND cdfs.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id)))
            AND (   @Vendor_Contact_Info_Id IS NULL
                    OR  (EXISTS (   SELECT
                                        1
                                    FROM
                                        #Contact_Dtls_For_search cdfs
                                    WHERE
                                        cdfs.Contact_Info_Id = @Vendor_Contact_Info_Id
                                        AND cdfs.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id)))
            AND (   @ICR_Status_Cd IS NULL
                    OR  icq.Status_Cd = @ICR_Status_Cd)
            AND (   @Is_Issue_Blocker IS NULL
                    OR  (   @Is_Issue_Blocker = 1
                            AND ici.Is_Blocker = 1)
                    OR  (   @Is_Issue_Blocker = 0
                            AND (   ici.Is_Blocker = 0
                                    OR  ici.Is_Blocker IS NULL)))
            AND (   @Previously_Chased IS NULL
                    OR  (   @Previously_Chased = 1
                            AND icccm.Invoice_Collection_Queue_Id IS NOT NULL)
                    OR  (   @Previously_Chased = 0
                            AND icccm.Invoice_Collection_Queue_Id IS NULL))
            AND (   @Final_Review IS NULL
                    OR  (   @Final_Review = 1
                            AND icfrlqm.Invoice_Collection_Queue_Id IS NOT NULL)
                    OR  (   @Final_Review = 0
                            AND icfrlqm.Invoice_Collection_Queue_Id IS NULL))
            AND (   @Days_Since_Last_Chased_From IS NULL
                    OR  lcd.Last_Change_Ts >= @Days_Since_Last_Chased_From)
            AND (   @Days_Since_Last_Chased_To IS NULL
                    OR  lcd.Last_Change_Ts <= @Days_Since_Last_Chased_To)
            AND (   @Chase_Count IS NULL
                    OR  (EXISTS (   SELECT
                                        1
                                    FROM
                                        dbo.ufn_split(@Chase_Count, ',') us
                                    WHERE
                                        ISNULL(lcd.Chase_Cnt, 0) >= CASE WHEN us.Segments LIKE '%+%' THEN 4
                                                                        ELSE us.Segments
                                                                    END
                                        AND ISNULL(lcd.Chase_Cnt, 0) <= CASE WHEN us.Segments LIKE '%+%' THEN 100
                                                                            ELSE us.Segments
                                                                        END)))
            AND (   @Download_Count IS NULL
                    OR  (EXISTS (   SELECT
                                        1
                                    FROM
                                        dbo.ufn_split(@Download_Count, ',') us
                                    WHERE
                                        ISNULL(icq.Download_Attempt_Cnt, 0) >= CASE WHEN us.Segments LIKE '%+%' THEN 4
                                                                                   ELSE us.Segments
                                                                               END
                                        AND ISNULL(icq.Download_Attempt_Cnt, 0) <= CASE WHEN us.Segments LIKE '%+%' THEN
                                                                                            100
                                                                                       ELSE us.Segments
                                                                                   END)))
            AND (   @Issue_Status IS NULL
                    OR  (   @Issue_Status = 'Include'
                            AND ici.Issue_Status_Cd = 1)
                    OR  (   @Issue_Status = 'Exclude'
                            AND (   ici.Issue_Status_Cd = 0
                                    OR  ici.Invoice_Collection_Queue_Id IS NULL)))
            AND (   @Record_Type_Cd IS NULL
                    OR  (   @Record_Type_Cd_Value = 'Manual ICR'
                            AND icq.Is_Manual = 1
                            AND (   @Invoice_Request_Type_Cd IS NULL
                                    OR  icq.Invoice_Request_Type_Cd = CAST(@Invoice_Request_Type_Cd AS VARCHAR(10))))
                    OR  (   @Record_Type_Cd_Value = 'System ICR'
                            AND icq.Is_Manual = 0)
                    OR  (   @Record_Type_Cd_Value = 'System Exception'
                            AND icq.Invoice_Collection_Exception_Type_Cd IS NOT NULL))
            AND (   @Cnt_Of_Invoice_Collection_Source = 0
                    OR  (   EXISTS (   SELECT
                                            1
                                       FROM
                                            @Tvp_Invoice_Collection_Sources tvp
                                       WHERE
                                            tvp.Column_Name = 'Invoice_Source_Type_Cd'
                                            AND cd.Invoice_Source_Type_Cd = CAST(tvp.Column_Value AS INT)
                                            AND tvp.Column_Type = cics.Code_Value
                                            AND cicst.Code_Value NOT IN ( 'Online', 'Mail Redirect' ))
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                @Tvp_Invoice_Collection_Sources tvp
                                           WHERE
                                                tvp.Column_Name = 'Invoice_Source_Method_of_Contact_Cd'
                                                AND cd.Invoice_Source_Method_of_Contact_Cd = CAST(tvp.Column_Value AS INT)
                                                AND tvp.Column_Type = cics.Code_Value
                                                AND cicst.Code_Value NOT IN ( 'Online', 'Mail Redirect' )))
                    OR  (EXISTS (   SELECT
                                        1
                                    FROM
                                        @Tvp_Invoice_Collection_Sources tvp
                                    WHERE
                                        tvp.Column_Name = 'Invoice_Source_Type_Cd'
                                        AND cd.Invoice_Source_Type_Cd = CAST(tvp.Column_Value AS INT)
                                        AND tvp.Column_Type = cics.Code_Value
                                        AND cicst.Code_Value IN ( 'Online', 'Mail Redirect', 'ETL', 'Partner' ))))
            AND (   @IC_Account_Config_Group_Id IS NULL
                    OR  icacg.Invoice_Collection_Account_Config_Group_ID = CAST(@IC_Account_Config_Group_Id AS VARCHAR(20)))
            AND (   @IC_Bots_Process_Id IS NULL
                    OR  ibpicacm.IC_Bots_Process_Id = @IC_Bots_Process_Id)
        GROUP BY
            icq.Invoice_Collection_Queue_Id
            , ch.Client_Id
            , ch.Client_Name
            , ch.Country_Id
            , ch.Country_Name
            , ici.Issue_Status_Cd
            , CASE WHEN icq.Is_Not_Default_Vendor = 1 THEN ve.ENTITY_NAME
                  ELSE ISNULL(vd.Account_Vendor_Type, cha.Account_Type)
              END
            , CASE WHEN icq.Is_Not_Default_Vendor = 1 THEN v.VENDOR_NAME
                  ELSE ISNULL(vd.Account_Vendor_Name, cha.Account_Vendor_Name)
              END
            , CASE WHEN icq.Is_Not_Default_Vendor = 1 THEN v.VENDOR_ID
                  ELSE ISNULL(vd.Account_Vendor_Id, cha.Account_Vendor_Id)
              END
            --, ISNULL(vd.Account_Vendor_Type, cha.Account_Type)  
            --, ISNULL(vd.Account_Vendor_Name, cha.Account_Vendor_Name)  
            --, ISNULL(vd.Account_Vendor_Id, cha.Account_Vendor_Id)  
            , cha.Account_Number
            , cha.Account_Id
            , icac.Invoice_Collection_Alternative_Account_Number
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME
            , ui.USER_INFO_ID
            , ci.First_Name + ' ' + ci.Last_Name
            , ISNULL(ifc.Code_Value, icgc.Code_Value)
            , CASE WHEN lcd.Invoice_Collection_Queue_Id IS NOT NULL THEN CAST(lcd.Chase_Cnt AS VARCHAR(15))
                  ELSE 0
              END
            , CASE WHEN ici.Issue_Status_Cd = 1 THEN 'Y'
                  ELSE 'N'
              END
            , CASE WHEN icfrlqm.Invoice_Collection_Queue_Id IS NOT NULL THEN 'Y'
                  ELSE 'N'
              END
            --, MAX(CASE WHEN ici.Is_Blocker = 1 THEN 1        
            --          ELSE 0        
            --      END)        
            , icq.Created_Ts
            , CASE WHEN ici.Issue_Status_Cd = 1 THEN 1
                  ELSE 0
              END
            , cics.Code_Value
            , cicst.Code_Value
            , cicsm.Code_Value
            , cci.Code_Value
            , icac.Invoice_Collection_Account_Config_Id
            , icq.Collection_End_Dt
            , icmrt.Code_Value
            , icq.Collection_Start_Dt
            , icq.Is_Manual
            , icq.Is_Not_Default_Vendor
            , ibp.IC_Bots_Process_Name;




        SELECT
            @SQL_Statement = N'; WITH Cte_Invoice_Collection_Queue        
              AS (        
                     SELECT   TOP ( ' + CAST(@End_Index AS VARCHAR(100))
                             + N' )        
                              icq.Client_Id, icq.Client_Name, icq.Country_Id, icq.Country_Name        
                              , CASE WHEN LEN(Commodities.Commodity_Name) > 0 THEN        
                                         LEFT(Commodities.Commodity_Name, LEN(Commodities.Commodity_Name) - 1)        
                                    ELSE Commodities.Commodity_Name        
                                END AS Commodity_Name        
                              , icq.Account_Type        
                              , icq.Account_Vendor_Name        
                              , icq.Account_Vendor_Id        
                 , icq.Account_Number, icq.Account_Id        
                              , icq.Alternate_Account_Number        
                              ,icq.Invoice_collection_Officer        
                              , icq.Invoice_collection_Officer_Id, icq.Person_to_chase        
                              , icq.Invoice_Frequency        
            , icq.Previously_chased        
                              , icq.Issue        
            , icq.Is_Final_Reviewed        
            , icq.Is_Blocker        
            , icq.Date_In_Queue        
            , icq.Last_Action_Date        
            , icq.Next_Action_Date        
            ,icq.Download_Attempt_Cnt        
            , icq.Has_Issue        
                              ,icq.Invoice_Source,icq.Invoice_Source_Type, icq.Invoice_Source_Method_Of_Contact, icq.Source_Vendor_Client        
                              , icq.Invoice_Collection_Account_Config_Id Account_Config_Id, icq.Invoice_Collection_Queue_Id, icq.Invoice_Collection_Account_Config_Id        
                              , icq.period_to_chase ,        
                                  icq.Period_Start_Date      
          , icq.Is_Manual      
          , icq.Is_Not_Default_Vendor  
    , max(icq.IC_Account_Config_Group_Name) as IC_Account_Config_Group_Name  ';

        SELECT
            @Row_Number_SQL_Statement = N', ROW_NUMBER() OVER (  ORDER BY  '
                                        + CASE WHEN @Sort_Col = 'Commodity_Name' THEN
                                                   ' CASE WHEN LEN(Commodities.Commodity_Name) > 0 THEN LEFT(Commodities.Commodity_Name, LEN(Commodities.Commodity_Name) - 1)        
                                ELSE Commodities.Commodity_Name        
                              END '
                                              WHEN @Sort_Col = 'Period_to_chase' THEN
                                                  'CONVERT(DATE,LEFT( CONVERT(VARCHAR(12), icq.Collection_Start_Dt, 105) + '' , ''        
                    + CONVERT(VARCHAR(12), icq.Collection_End_Dt, 105) + ''/'' + LEFT(ISNULL(icmrt.Code_Value, ''''), 1),CHARINDEX('','', CONVERT(VARCHAR(12), icq.Collection_Start_Dt, 105) + '' , ''        
                    + CONVERT(VARCHAR(12), icq.Collection_End_Dt, 105) + ''/'' + LEFT(ISNULL(icmrt.Code_Value, ''''), 1),1)-1),105) '
                                              WHEN @Sort_Col = 'Account_Type' THEN 'icq.Account_Type '
                                              WHEN @Sort_Col = 'Account_Vendor_Name' THEN ' icq.Account_Vendor_Name '
                                              WHEN @Sort_Col = 'Person_to_chase' THEN 'icq.Person_to_chase'
                                              WHEN @Sort_Col = 'Invoice_Frequency' THEN 'icq.Invoice_Frequency'
                                              WHEN @Sort_Col = 'Previously_chased' THEN 'icq.Previously_chased'
                                              WHEN @Sort_Col = 'Issue' THEN 'icq.Issue'
                                              WHEN @Sort_Col = 'Is_Final_Reviewed' THEN 'icq.Is_Final_Reviewed'
                                              WHEN @Sort_Col = 'Invoice_Source' THEN 'icq.Invoice_Source'
                                              WHEN @Sort_Col = 'Alternate_Account_Number' THEN
                                                  'icq.Alternate_Account_Number'
                                              WHEN @Sort_Col = 'Period_Start_Date' THEN ' icq.Period_Start_Date '
                                              WHEN @Sort_Col = 'Last_Action_Date' THEN ' icq.Last_Action_Date '
                                              WHEN @Sort_Col = 'Next_Action_Date' THEN ' icq.Next_Action_Date '
                                              WHEN @Sort_Col = 'Download_Count' THEN 'icq.Download_Attempt_Cnt'
                                              WHEN @Sort_Col = 'Invoice_collection_Officer' THEN
                                                  ' icq.Invoice_collection_Officer '
                                              ELSE @Sort_Col
                                          END + N' ' + @Sort_Order
                                        + N' )         
                                                      AS Row_Num        
                              , COUNT(1) OVER () Total_Count 
							  , icq.IC_Bots_Process_Name        
              FROM        
                           #Invoice_Collection_Queue_Ids icq ';



        SELECT
            @SQL_Statement2 = N' CROSS APPLY        
                     (   SELECT        
                              c.Commodity_Name + '',''        
                         FROM        
                              dbo.Invoice_Collection_Queue icqe        
                              INNER JOIN dbo.Invoice_Collection_Account_Config icac1        
                        ON icqe.Invoice_Collection_Account_Config_Id = icac1.Invoice_Collection_Account_Config_Id        
                              INNER JOIN Core.Client_Hier_Account cha        
                                  ON cha.Account_Id = icac1.Account_Id        
                              INNER JOIN dbo.Commodity c        
                                  ON c.Commodity_Id = cha.Commodity_Id        
        WHERE        
                              icq.Invoice_Collection_Queue_Id = icqe.Invoice_Collection_Queue_Id        
                         GROUP BY        
                             c.Commodity_Name        
                         FOR XML PATH('''')) Commodities(Commodity_Name)        
        
                   
        
           ';
        SELECT
            @SQL_Statement3 = N' GROUP BY        
                        icq.Client_Id, icq.Client_Name, icq.Country_Id, icq.Country_Name        
                              , CASE WHEN LEN(Commodities.Commodity_Name) > 0 THEN        
                                         LEFT(Commodities.Commodity_Name, LEN(Commodities.Commodity_Name) - 1)        
                                    ELSE Commodities.Commodity_Name        
                                END         
                              , icq.Account_Type        
                              , icq.Account_Vendor_Name        
                              , icq.Account_Vendor_Id        
                              , icq.Account_Number, icq.Account_Id        
                              , icq.Alternate_Account_Number        
                              ,icq.Invoice_collection_Officer        
                              , icq.Invoice_collection_Officer_Id, icq.Person_to_chase        
                              , icq.Invoice_Frequency        
            , icq.Previously_chased        
                              , icq.Issue        
            , icq.Is_Final_Reviewed        
            , icq.Is_Blocker        
            , icq.Date_In_Queue        
            , icq.Last_Action_Date        
            , icq.Next_Action_Date        
            ,icq.Download_Attempt_Cnt        
            , icq.Has_Issue        
                              ,icq.Invoice_Source,icq.Invoice_Source_Type, icq.Invoice_Source_Method_Of_Contact, icq.Source_Vendor_Client        
                              , icq.Invoice_Collection_Account_Config_Id, icq.Invoice_Collection_Queue_Id        
                              , icq.period_to_chase        
         , icq.Period_Start_Date      
         , icq.Is_Manual      
         , icq.Is_Not_Default_Vendor    
   ,  IC_Account_Config_Group_Name    ,icq.IC_Bots_Process_Name                      
        
                 )         
               SELECT        
                  cic.Client_Id        
                  , cic.Client_Name        
                  , cic.Country_Name        
                  , cic.Country_Id        
               , cic.Commodity_Name        
                  , cic.Account_Type        
                  , cic.Account_Vendor_Name        
                  , cic.Account_Vendor_Id        
                  , cic.Account_Number        
                  , cic.Account_Id        
                  , cic.Alternate_Account_Number        
                  , cic.Invoice_collection_Officer        
                  , cic.Invoice_collection_Officer_Id   
                  , cic.Invoice_Frequency        
                  , cic.period_to_chase        
                  , cic.Person_to_chase        
                  , cic.Previously_chased        
                  , cic.Issue        
                  , cic.Invoice_Source        
                  , cic.Invoice_Source_Type        
                  , cic.Invoice_Source_Method_Of_Contact        
                  , cic.Source_Vendor_Client        
                  , cic.Invoice_Collection_Queue_Id        
                  , cic.Invoice_Collection_Account_Config_Id        
                  , cic.Row_Num        
                  , cic.Has_Issue                    , cic.Total_Count        
                  ,cic.Period_Start_Date        
,cic.Is_Blocker        
         ,cic.Date_In_Queue        
         ,cic.Last_Action_Date        
         ,CASE WHEN cic.Next_Action_Date = ''1900-01-01'' then NULL ELSE cic.Next_Action_Date END Next_Action_Date        
         ,cic.Download_Attempt_Cnt        
         ,cic.Is_Final_Reviewed      
   , cic.Is_Manual      
   , cic.Is_Not_Default_Vendor    
   ,cic.IC_Account_Config_Group_Name        
   ,cic.IC_Bots_Process_Name
              FROM        
                  Cte_Invoice_Collection_Queue cic        
              WHERE        
                 cic.Row_Num BETWEEN ' + CAST(@Start_Index AS VARCHAR(MAX)) + N' AND '
                              + CAST(@End_Index AS VARCHAR(MAX))
                              + N'         
              ORDER BY        
                  cic.Row_Num;';



        EXEC (@SQL_Statement + @Row_Number_SQL_Statement + @SQL_Statement2 + @SQL_Statement3);





        DROP TABLE
            #Vendor_Dtls
            , #Consolidated_Billing_Account_Id
            , #Contact_Dtls_For_search
            , #Vendor_Account_Details;
        DROP TABLE
            #Contact_Dtls
            , #Client_hier_Account_Details;


        DROP TABLE
            #Invoice_Collection_Queue_Ids
            , #Ic_Account_Details
            , #Invoice_Collection_Account_Config_Ids
            , #Invoice_Collection_Accounts;


    END;



GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Records_Queue_Sel_By_Invoice_Collection_Queue_Id] TO [CBMSApplication]
GO
