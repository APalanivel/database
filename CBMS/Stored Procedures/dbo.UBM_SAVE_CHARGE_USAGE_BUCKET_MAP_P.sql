SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE dbo.UBM_SAVE_CHARGE_USAGE_BUCKET_MAP_P
@userId varchar,
@sessionId varchar,
@ubmId int,
@cbmsBucketTypeId int,
@ubmBucketType varchar(100),
@isCharge int


as
	set nocount on

if @isCharge = 1
begin
	
	insert into UBM_CHARGE_BUCKET_MAP (ubm_id,cbms_charge_bucket_type_id,
						ubm_charge_bucket_type)
	values(@ubmId,@cbmsBucketTypeId,@ubmBucketType)

end
else
begin
	insert into UBM_USAGE_BUCKET_MAP (ubm_id,cbms_USAGE_bucket_type_id,
						ubm_USAGE_bucket_type)
	values(@ubmId,@cbmsBucketTypeId,@ubmBucketType)
end
GO
GRANT EXECUTE ON  [dbo].[UBM_SAVE_CHARGE_USAGE_BUCKET_MAP_P] TO [CBMSApplication]
GO
