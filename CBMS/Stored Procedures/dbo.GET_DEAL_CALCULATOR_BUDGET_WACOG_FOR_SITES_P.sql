SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
    
NAME:      
 dbo.GET_DEAL_CALCULATOR_BUDGET_WACOG_FOR_SITES_P    
       
DESCRIPTION:       
    
INPUT PARAMETERS:      
 Name   DataType Default Description      
------------------------------------------------------------      
 @clientId  INT    
 @startDate  DATETIME    
 @endDate  DATETIME    
 @startYear  INT    
 @endYear  INT      
     
             
OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
  USAGE EXAMPLES:      
------------------------------------------------------------    
      
  EXEC dbo.GET_DEAL_CALCULATOR_BUDGET_WACOG_FOR_SITES_P 10005,'1-1-2011','12-31-2011',2011,2011        
  EXEC GET_DEAL_CALCULATOR_BUDGET_WACOG_FOR_SITES_P 11610,'01/01/2011','12/01/2011',2011,2011    
    
AUTHOR INITIALS:      
 Initials Name      
------------------------------------------------------------      
 SP   sandeep pigilam    
     
 MODIFICATIONS    
 Initials Date  Modification      
------------------------------------------------------------    
  SP        2013-06-11  Created    
******/     
CREATE PROCEDURE [dbo].[GET_DEAL_CALCULATOR_BUDGET_WACOG_FOR_SITES_P]  
      @clientId INT  
     ,@startDate DATETIME  
     ,@endDate DATETIME  
     ,@startYear INT  
     ,@endYear INT  
AS   
BEGIN        
        
 --FOR SITE          
      SET NOCOUNT ON        
      DECLARE  
            @Forecast_As_Of_Date_Startyear DATETIME  
           ,@Forecast_As_Of_Date_Endyear DATETIME  
           ,@InsertedCount INT  
           ,@divisionId INT    
               
               
      DECLARE @ResultSet TABLE  
            (   
             priceVolume DECIMAL(32, 16)  
            ,VOLUME DECIMAL(32, 16)  
            ,FORECAST_ID INT  
            ,SITEID INT  
            ,TARGETPRICE DECIMAL(32, 16)  
            ,TARGETID INT  
            ,DEALMONTHYEAR DATETIME  
            ,MI2 DATETIME  
            ,MONTH_IDENTIFIER VARCHAR(12) )        
        
      DECLARE @tblbase TABLE  
            (   
             RM_TARGET_BUDGET_ID INT  
            ,DIVISION_ID INT  
            ,Site_ID INT  
            ,MONTH_IDENTIFIER DATETIME  
            ,BUDGET_TARGET DECIMAL(32, 16) )    
    
         
      DECLARE @tblIDColln1 TABLE  
            (   
             RM_TARGET_BUDGET_ID INT  
            ,DIVISION_ID INT  
            ,Site_ID INT  
            ,MONTH_IDENTIFIER DATETIME  
            ,BUDGET_TARGET DECIMAL(32, 16) )        
      DECLARE @tblIDColln2 TABLE  
            (   
             RM_TARGET_BUDGET_ID INT  
            ,DIVISION_ID INT  
            ,Site_ID INT  
            ,MONTH_IDENTIFIER DATETIME  
            ,BUDGET_TARGET DECIMAL(32, 16) )        
      DECLARE @tblIDColln3 TABLE  
            (   
             RM_TARGET_BUDGET_ID INT  
            ,DIVISION_ID INT  
            ,Site_ID INT  
            ,MONTH_IDENTIFIER DATETIME  
            ,BUDGET_TARGET DECIMAL(32, 16) )    
    
      DECLARE @divisionids TABLE ( division_id INT )   
         
      INSERT      INTO @divisionids  
                  SELECT  
                        division_id  
                  FROM  
                        dbo.site  
                  WHERE  
                        Client_ID = @clientId                                       
     
      SELECT  
            @Forecast_As_Of_Date_Startyear = MAX(FORECAST_AS_OF_DATE)  
      FROM  
            dbo.RM_FORECAST_VOLUME  
      WHERE  
            FORECAST_YEAR = @startYear  
            AND CLIENT_ID = @clientId        
      SELECT  
            @Forecast_As_Of_Date_Endyear = MAX(FORECAST_AS_OF_DATE)  
      FROM  
            dbo.RM_FORECAST_VOLUME  
      WHERE  
            FORECAST_YEAR = @endYear  
            AND CLIENT_ID = @clientId       
                
         
      INSERT      INTO @tblBase  
                  SELECT DISTINCT  
                        RM_TARGET_BUDGET_ID  
                       ,rm_bdg.DIVISION_ID  
                       ,rm_bdg.Site_ID  
                       ,RM_TARGET_BUDGET.MONTH_IDENTIFIER  
                       ,RM_TARGET_BUDGET.BUDGET_TARGET  
                  FROM  
                        dbo.RM_TARGET_BUDGET  
                        JOIN dbo.RM_BUDGET rm_bdg  
                              ON rm_bdg.RM_BUDGET_ID = RM_TARGET_BUDGET.RM_BUDGET_ID  
                        JOIN dbo.RM_BUDGET rm_bdg_in  
                              ON rm_bdg_in.RM_BUDGET_ID = rm_bdg.RM_BUDGET_ID  
                  WHERE  
                        rm_bdg.CLIENT_ID = @clientId  
                        AND rm_bdg.IS_BUDGET_APPROVED = 1  
                        AND RM_TARGET_BUDGET.MONTH_IDENTIFIER BETWEEN CONVERT(VARCHAR(12), @startDate, 101)  
                                                              AND     CONVERT(VARCHAR(12), @endDate, 101)  
                        AND ( ( CONVERT(VARCHAR(12), @startDate, 101) BETWEEN rm_bdg_in.BUDGET_START_MONTH  
                                                                      AND     rm_bdg_in.BUDGET_END_MONTH )  
                              OR ( CONVERT(VARCHAR(12), @endDate, 101) BETWEEN rm_bdg_in.BUDGET_START_MONTH  
                                                                       AND     rm_bdg_in.BUDGET_END_MONTH )  
                              OR ( rm_bdg_in.BUDGET_START_MONTH BETWEEN CONVERT(VARCHAR(12), @startDate, 101)  
                                                                AND     CONVERT(VARCHAR(12), @endDate, 101) )  
                              OR ( rm_bdg_in.BUDGET_END_MONTH BETWEEN CONVERT(VARCHAR(12), @startDate, 101)  
                                                              AND     CONVERT(VARCHAR(12), @endDate, 101) ) )        
      INSERT      INTO @tblIDColln1  
                  SELECT  
                        base.RM_TARGET_BUDGET_ID  
                       ,base.DIVISION_ID  
                       ,base.Site_ID  
                       ,base.MONTH_IDENTIFIER  
                       ,base.BUDGET_TARGET  
                  FROM  
                        @tblBase base  
                        INNER JOIN @divisionids div  
                              ON base.DIVISION_ID = div.DIVISION_ID  
                  WHERE  
                        base.SITE_ID IS NULL        
        
      INSERT      INTO @tblIDColln2  
                  SELECT  
                        b.RM_TARGET_BUDGET_ID  
                       ,b.DIVISION_ID  
                       ,b.Site_ID  
                       ,b.MONTH_IDENTIFIER  
                       ,b.BUDGET_TARGET  
                  FROM  
                        @tblBase b  
                        INNER JOIN @divisionids div  
                              ON b.DIVISION_ID = div.DIVISION_ID  
                        JOIN Site sit  
                              ON sit.Site_ID = b.Site_ID    
        
      INSERT      INTO @tblIDColln3  
                  SELECT  
                        RM_TARGET_BUDGET_ID  
                       ,DIVISION_ID  
                       ,Site_ID  
                       ,MONTH_IDENTIFIER  
                       ,BUDGET_TARGET  
                  FROM  
                        @tblBase  
                  WHERE  
                        DIVISION_ID IS NULL  
                        AND SITE_ID IS NULL      
      INSERT      INTO @ResultSet  
                  SELECT  
                        volumedetails.VOLUME * tblSite.BUDGET_TARGET priceVolume  
                       ,volumedetails.VOLUME VOLUME  
                       ,volumedetails.RM_FORECAST_VOLUME_DETAILS_ID FORECAST_ID  
                       ,volumedetails.SITE_ID SITEID  
                       ,tblSite.BUDGET_TARGET TARGETPRICE  
                       ,tblSite.RM_TARGET_BUDGET_ID TARGETID  
                       ,volumedetails.MONTH_IDENTIFIER DEALMONTHYEAR  
                       ,tblSite.MONTH_IDENTIFIER MI2  
                       ,CONVERT (VARCHAR(12), tblSite.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER  
                  FROM  
                        RM_FORECAST_VOLUME_DETAILS volumedetails  
                        JOIN RM_FORECAST_VOLUME volume  
                              ON volumedetails.RM_FORECAST_VOLUME_ID = volume.RM_FORECAST_VOLUME_ID  
                        JOIN RM_BUDGET_SITE_MAP  
                              ON RM_BUDGET_SITE_MAP.SITE_ID = volumedetails.site_id  
                        JOIN @tblBase tblSite  
                              ON tblSite.Site_ID = volumedetails.SITE_ID -- Evaluate whether this join is required        
                                 AND tblSite.MONTH_IDENTIFIER = volumedetails.MONTH_IDENTIFIER  
                  WHERE  
                        volume.CLIENT_ID = @clientId  
                        AND volume.FORECAST_AS_OF_DATE IN ( @Forecast_As_Of_Date_Startyear, @Forecast_As_Of_Date_Endyear )  
                        AND volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(VARCHAR(12), @startDate, 101)  
                                                           AND     CONVERT(VARCHAR(12), @endDate, 101)    
  
      INSERT      INTO @ResultSet  
                  SELECT  
                        volumedetails.VOLUME * TblTrgBdg.BUDGET_TARGET priceVolume  
                       ,volumedetails.VOLUME VOLUME  
                       ,volumedetails.RM_FORECAST_VOLUME_DETAILS_ID FORECAST_ID  
                       ,volumedetails.SITE_ID SITEID  
                       ,TblTrgBdg.BUDGET_TARGET TARGETPRICE  
                       ,TblTrgBdg.RM_TARGET_BUDGET_ID TARGETID  
                       ,volumedetails.MONTH_IDENTIFIER DEALMONTHYEAR  
                       ,TblTrgBdg.MONTH_IDENTIFIER MI2  
                       ,CONVERT (VARCHAR(12), TblTrgBdg.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER  
                  FROM  
                        RM_FORECAST_VOLUME_DETAILS volumedetails  
                        JOIN RM_FORECAST_VOLUME volume  
                              ON volume.RM_FORECAST_VOLUME_ID = volumedetails.RM_FORECAST_VOLUME_ID  
                        JOIN RM_BUDGET_SITE_MAP  
                              ON RM_BUDGET_SITE_MAP.SITE_ID = volumedetails.site_id  
                        JOIN @tblIDColln1 TblTrgBdg  
                              ON TblTrgBdg.MONTH_IDENTIFIER = volumedetails.MONTH_IDENTIFIER  
                        JOIN Site sit  
                              ON sit.Site_ID = volumedetails.Site_ID  
                                 AND sit.Division_ID = TblTrgBdg.Division_ID  
                        LEFT OUTER JOIN @tblIDColln2 tblSite  
                              ON tblSite.Site_ID = volumedetails.SITE_ID  
                        LEFT JOIN @ResultSet Result  
                              ON sit.Site_ID = Result.SITEID  
                  WHERE  
                        volume.CLIENT_ID = @clientId  
                        AND volume.FORECAST_AS_OF_DATE IN ( @Forecast_As_Of_Date_Startyear, @Forecast_As_Of_Date_Endyear )  
                        AND volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(VARCHAR(12), @startDate, 101)  
                                                           AND     CONVERT(VARCHAR(12), @endDate, 101)  
                        AND tblSite.Site_ID IS NULL  
                        AND Result.SITEID IS NULL           
           
      SET @InsertedCount = @@ROWCOUNT        
     
        
      INSERT      INTO @ResultSet  
                  SELECT  
                        volumedetails.VOLUME * TblTrgBdg.BUDGET_TARGET priceVolume  
                       ,volumedetails.VOLUME VOLUME  
                       ,volumedetails.RM_FORECAST_VOLUME_DETAILS_ID FORECAST_ID  
                       ,volumedetails.SITE_ID SITEID  
                       ,TblTrgBdg.BUDGET_TARGET TARGETPRICE  
                       ,TblTrgBdg.RM_TARGET_BUDGET_ID TARGETID  
                       ,volumedetails.MONTH_IDENTIFIER DEALMONTHYEAR  
                       ,TblTrgBdg.MONTH_IDENTIFIER MI2  
                       ,CONVERT (VARCHAR(12), TblTrgBdg.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER  
                  FROM  
                        RM_FORECAST_VOLUME_DETAILS volumedetails  
                        JOIN RM_FORECAST_VOLUME volume  
                              ON volume.RM_FORECAST_VOLUME_ID = volumedetails.RM_FORECAST_VOLUME_ID  
                        JOIN RM_BUDGET_SITE_MAP  
                              ON RM_BUDGET_SITE_MAP.SITE_ID = volumedetails.site_id  
                        JOIN @tblIDColln3 TblTrgBdg  
                              ON TblTrgBdg.MONTH_IDENTIFIER = volumedetails.MONTH_IDENTIFIER  
                        JOIN Site sit  
                              ON sit.Site_ID = volumedetails.Site_ID  
                        JOIN Division div  
                              ON div.Division_ID = sit.Division_ID  
                        LEFT JOIN @ResultSet Result  
                              ON sit.Site_ID = Result.SITEID  
                  WHERE  
                        volume.CLIENT_ID = @clientId  
                        AND volume.FORECAST_AS_OF_DATE IN ( @Forecast_As_Of_Date_Startyear, @Forecast_As_Of_Date_Endyear )  
                        AND volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(VARCHAR(12), @startDate, 101)  
                                                           AND     CONVERT(VARCHAR(12), @endDate, 101)  
                        AND div.CLIENT_ID = @clientId  
                        AND Result.SITEID IS NULL        
  
      SELECT  
            A.VOLUME Volume  
           ,A.priceVolume PriceVolume  
           ,A.MONTH_IDENTIFIER MONTH_IDENTIFIER  
           ,a.SITEID  
      FROM  
            @ResultSet a  
             
        
END     
;
GO
GRANT EXECUTE ON  [dbo].[GET_DEAL_CALCULATOR_BUDGET_WACOG_FOR_SITES_P] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[GET_DEAL_CALCULATOR_BUDGET_WACOG_FOR_SITES_P] TO [CBMSApplication]
GO
