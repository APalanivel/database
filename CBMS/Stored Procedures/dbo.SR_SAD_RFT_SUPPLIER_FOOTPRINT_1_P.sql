SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_RFT_SUPPLIER_FOOTPRINT_1_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@commodityId   	int       	          	
	@supplierId    	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
exec dbo.SR_SAD_RFT_SUPPLIER_FOOTPRINT_1_P 290,0
exec dbo.SR_SAD_RFT_SUPPLIER_FOOTPRINT_1_P 290,616

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	SSR			09/27/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on	        	
******/
CREATE PROCEDURE dbo.SR_SAD_RFT_SUPPLIER_FOOTPRINT_1_P
    (
      @commodityId INT
    , @supplierId INT
    )
AS 
    SET nocount ON
    DECLARE @selectClause VARCHAR(500)
    DECLARE @fromClause VARCHAR(1000)
    DECLARE @whereClause VARCHAR(1000)
    DECLARE @orderbyClause VARCHAR(500)
    DECLARE @sql VARCHAR(4000)
	

    BEGIN 


        SELECT
            @selectClause = ' select supplier.vendor_id '
            + '	, supplier.vendor_name '
            + ' ,commodity_map.commodity_type_id as commodity_id '
            + ' , commodity_type.entity_name '	

        SELECT
            @fromClause = ' from 	 vendor supplier , '
            + ' entity vendor_type '
            + ' ,  vendor_commodity_map commodity_map  '
            + ' , entity commodity_type '	

        SELECT
            @whereClause = ' where supplier.vendor_type_id = vendor_type.entity_id '
            + ' and vendor_type.entity_type = 155 '
            + ' and vendor_type.entity_name=''Supplier'' '



        IF ( @commodityId = 0
             AND @supplierId > 0 ) 
            BEGIN


                SELECT
                    @whereClause = ' where supplier.vendor_type_id = vendor_type.entity_id '
                    + ' and vendor_type.entity_type = 155 '
                    + ' and vendor_type.entity_name=''Supplier'' '
                    + ' and supplier.vendor_id = ' + STR(@supplierId)
                    + ' and commodity_map.vendor_id = supplier.vendor_id '
                    + ' and commodity_map.commodity_type_id = commodity_type.entity_id '
                    + ' and commodity_type.entity_type = 157 '

            END


        IF ( @commodityId > 0
             AND @supplierId = 0 ) 
            BEGIN

                SELECT
                    @whereClause = @whereClause
                    + ' and commodity_map.vendor_id = supplier.vendor_id '
                    + ' and commodity_map.commodity_type_id = commodity_type.entity_id '
                    + ' and commodity_type.entity_type = 157 '
                    + ' and commodity_type.entity_id = ' + STR(@commodityId)	
	

            END

        IF ( @commodityId = 0
             AND @supplierId = 0 ) 
            BEGIN

                SELECT
                    @whereClause = @whereClause
                    + ' and commodity_map.vendor_id = supplier.vendor_id '
                    + ' and commodity_map.commodity_type_id = commodity_type.entity_id '
                    + ' and commodity_type.entity_type = 157 '
                    + ' order by supplier.vendor_id '	
            END


        IF ( @commodityId > 0
             AND @supplierId > 0 ) 
            BEGIN

                SELECT
                    @whereClause = @whereClause
                    + ' and commodity_map.vendor_id = supplier.vendor_id '
                    + ' and commodity_map.commodity_type_id = commodity_type.entity_id '
                    + ' and commodity_type.entity_type = 157 '
                    + ' and commodity_type.entity_id = ' + STR(@commodityId)
                    + ' and supplier.vendor_id = ' + STR(@supplierId)

            END

        SELECT
            @sql = @selectClause + @fromClause + @whereClause

        EXEC ( @sql )


    END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_RFT_SUPPLIER_FOOTPRINT_1_P] TO [CBMSApplication]
GO
