SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******

NAME: dbo.IP_Reset_For_Deleted_Account_Meter

DESCRIPTION: 

	This procedure used to reset the Invoice_participation and Invoice_participation_Site table if the disassociated meter 
	from account is the last one in the supplier account site
	

INPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
	@Account_Id		INT
	@Meter_Id		VARCHAR(max)			Comma seperated meter_id which are removed from the contract.

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	
	EXEC dbo.IP_Reset_For_Deleted_Account_Meter


AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	RR			Raghu Reddy

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	RR			2017-03-06	Contract palaceholder - Created CP-8 
							Logic will same as dbo.IP_Reset_For_Deleted_Contract_Meter
*/

CREATE PROCEDURE [dbo].[IP_Reset_For_Deleted_Account_Meter]
      ( 
       @Account_Id INT
      ,@Meter_Id VARCHAR(MAX) )
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE
            @Site_Id INT
           ,@Service_Month DATE

      DECLARE @Deleted_Meter_List TABLE
            ( 
             Meter_Id INT
            ,Supplier_Account_Id INT
            ,Site_Id INT
            ,PRIMARY KEY CLUSTERED ( Meter_Id, Supplier_Account_Id, Site_Id ) )
      DECLARE @Contract_Meter_List TABLE
            ( 
             Meter_Id INT
            ,Supplier_Account_Id INT
            ,Site_Id INT
            ,PRIMARY KEY CLUSTERED ( Meter_Id, Supplier_Account_Id, Site_Id ) )
      DECLARE @Remaining_Meter_List TABLE
            ( 
             Meter_Id INT
            ,Supplier_Account_Id INT
            ,Site_Id INT
            ,PRIMARY KEY CLUSTERED ( Meter_Id, Supplier_Account_Id, Site_Id ) )

      DECLARE @Invoice_Participation_List TABLE
            ( 
             Account_Id INT
            ,Site_Id INT
            ,Service_Month DATETIME
            ,PRIMARY KEY CLUSTERED ( Site_id, Service_Month, Account_Id ) )
	
      INSERT      INTO @Contract_Meter_List
                  ( 
                   Meter_Id
                  ,Supplier_Account_Id
                  ,Site_Id )
                  SELECT
                        cha.Meter_Id
                       ,cha.Account_Id
                       ,ch.Site_Id
                  FROM
                        Core.Client_Hier_Account cha
                        INNER JOIN Core.Client_Hier ch
                              ON ch.Client_Hier_Id = cha.Client_Hier_Id
                  WHERE
                        cha.Account_Id = @Account_Id
                  GROUP BY
                        cha.Meter_Id
                       ,cha.Account_Id
                       ,ch.Site_Id	

      INSERT      INTO @Deleted_Meter_List
                  ( 
                   Meter_Id
                  ,Supplier_Account_Id
                  ,Site_Id )
                  SELECT
                        segm.Segments
                       ,cml.Supplier_Account_Id
                       ,cml.Site_Id
                  FROM
                        dbo.ufn_split(@Meter_Id, ',') segm
                        JOIN @Contract_Meter_List cml
                              ON cml.Meter_Id = CAST(segm.Segments AS INT)
			
      INSERT      INTO @Remaining_Meter_List
                  ( 
                   Meter_Id
                  ,Supplier_Account_Id
                  ,Site_Id )
                  SELECT
                        cm.Meter_Id
                       ,cm.Supplier_Account_Id
                       ,cm.Site_Id
                  FROM
                        @Contract_Meter_List cm
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          @Deleted_Meter_List dm
                                     WHERE
                                          cm.Meter_Id = dm.Meter_Id
                                          AND cm.Supplier_Account_Id = dm.Supplier_Account_Id
                                          AND dm.Site_Id = dm.Site_Id )

      INSERT      INTO @Invoice_Participation_List
                  ( 
                   Account_Id
                  ,Site_Id
                  ,Service_Month )
                  SELECT
                        ip.ACCOUNT_ID
                       ,d.Site_Id
                       ,ip.SERVICE_MONTH
                  FROM
                        dbo.Invoice_Participation ip
                        INNER JOIN @Deleted_Meter_List d
                              ON ip.Account_Id = d.Supplier_Account_Id
                                 AND ip.SITE_ID = d.Site_Id
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          @Remaining_Meter_List rml
                                     WHERE
                                          d.Site_id = rml.Site_id
                                          AND d.Supplier_Account_Id = rml.Supplier_Account_Id )

      BEGIN TRY
            BEGIN TRAN

            DELETE
                  ipq
            FROM
                  dbo.INVOICE_PARTICIPATION_QUEUE ipq
                  INNER JOIN @Invoice_Participation_List ip
                        ON ip.Account_Id = ipq.ACCOUNT_ID
                           AND ip.Site_Id = ipq.SITE_ID

            WHILE EXISTS ( SELECT
                              1
                           FROM
                              @Invoice_Participation_List ) 
                  BEGIN
			
                        SELECT TOP 1
                              @Account_id = Account_Id
                             ,@Site_Id = Site_Id
                             ,@Service_Month = Service_Month
                        FROM
                              @Invoice_Participation_List

                        EXEC dbo.Invoice_Participation_Del 
                              @Account_Id
                             ,@Site_Id
                             ,@Service_Month

                        EXEC dbo.cbmsInvoiceParticipationSite_Save 
                              -1
                             ,@Site_Id
                             ,@Service_Month -- To reset Site level invoice participation.

                        DELETE
                              @Invoice_Participation_List
                        WHERE
                              Account_Id = @Account_Id
                              AND Site_Id = @Site_Id
                              AND Service_Month = @Service_Month

                  END
            COMMIT TRAN
      END TRY
      BEGIN CATCH
            IF @@TRANCOUNT > 0 
                  BEGIN
                        ROLLBACK TRAN
                  END

            EXEC dbo.usp_RethrowError

      END CATCH

END
;
GO
GRANT EXECUTE ON  [dbo].[IP_Reset_For_Deleted_Account_Meter] TO [CBMSApplication]
GO
