SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
 NAME: [dbo].[Get_Client_App_Access_Role_By_Client_Id]        
        
 DESCRIPTION:          
      To Get all the Roles Based on Client along with their Users and Groups Count .    
        
 INPUT PARAMETERS:          
         
 Name                                DataType            Default        Description          
---------------------------------------------------------------------------------------------------------------        
 @Client_Id                         INT                  
 @Start_Index      INT   1            
 @End_Index       INT   2147483647    
 @Total_Row_Count     INT   0  
       
          
 OUTPUT PARAMETERS:                
 Name                                DataType            Default        Description          
---------------------------------------------------------------------------------------------------------------        
          
 USAGE EXAMPLES:              
---------------------------------------------------------------------------------------------------------------         
         
 EXEC dbo.Get_Client_App_Access_Role_By_Client_Id  
      @Client_Id = 235  
     ,@Start_Index = 1  
     ,@End_Index = 10  
     ,@Total_Row_Count = 0          
         
 AUTHOR INITIALS:         
         
 Initials               Name          
---------------------------------------------------------------------------------------------------------------        
 SP                     Sandeep Pigilam    
           
 MODIFICATIONS:         
          
 Initials               Date            Modification        
---------------------------------------------------------------------------------------------------------------        
 SP                     2013-12-11      Created for RA Admin user management        
         
******/     
CREATE PROCEDURE [dbo].[Get_Client_App_Access_Role_By_Client_Id]
      ( 
       @Client_Id INT
      ,@Start_Index INT = 1
      ,@End_Index INT = 2147483647
      ,@Total_Row_Count INT = 0 )
AS 
BEGIN     
  
      SET NOCOUNT ON     
    
      IF ( @Total_Row_Count = 0 ) 
            BEGIN  
              
                  SELECT
                        @Total_Row_Count = COUNT(1)
                  FROM
                        Client_App_Access_Role caar
                  WHERE
                        Client_Id = @Client_Id  
            END;  
      WITH  cte_Client_App_Access_Role
              AS ( SELECT TOP ( @End_Index )
                        caar.App_Access_Role_Name
                       ,caar.App_Access_Role_Dsc
                       ,caar.Client_App_Access_Role_Id
                       ,Users_Count
                       ,Functions_Count
                       ,LEFT(temp.GROUP_INFO_ID_List, LEN(temp.GROUP_INFO_ID_List) - 1) AS GROUP_INFO_ID_List
                       ,CASE WHEN caar.App_Access_Role_Name='Admin' THEN 1
							 WHEN caar.App_Access_Role_Name='Full Access (Non Admin)' THEN 2
						ELSE 3
						END AS Sort_Order
                       
                       ,ROW_NUMBER() OVER ( ORDER BY caar.App_Access_Role_Name ) Row_Num
                   FROM
                        dbo.Client_App_Access_Role caar
                        LEFT JOIN ( SELECT
                                          uicaar.Client_App_Access_Role_Id
                                         ,COUNT(uicaar.User_Info_Id) AS Users_Count
                                    FROM
                                          dbo.User_Info_Client_App_Access_Role_Map uicaar
                                          INNER JOIN dbo.USER_INFO ui
                                                ON uicaar.USER_INFO_ID = ui.USER_INFO_ID
                                                   AND ui.IS_HISTORY = 0
                                                   AND ui.ACCESS_LEVEL = 1
                                                   AND ui.CLIENT_ID = @Client_Id
                                    GROUP BY
                                          uicaar.Client_App_Access_Role_Id ) AS Users_Cnt
                              ON caar.Client_App_Access_Role_Id = Users_Cnt.Client_App_Access_Role_Id
                        LEFT JOIN ( SELECT
                                          caargi.Client_App_Access_Role_Id
                                         ,COUNT(caargi.GROUP_INFO_ID) AS Functions_Count
                                    FROM
                                          dbo.Client_App_Access_Role_Group_Info_Map caargi
                                    GROUP BY
                                          caargi.Client_App_Access_Role_Id ) AS Functions
                              ON caar.Client_App_Access_Role_Id = Functions.Client_App_Access_Role_Id
                        CROSS APPLY ( SELECT
                                          CAST(caargi1.GROUP_INFO_ID AS VARCHAR) + ','
                                      FROM
                                          dbo.Client_App_Access_Role_Group_Info_Map caargi1
                                      WHERE
                                          caar.Client_App_Access_Role_Id = caargi1.Client_App_Access_Role_Id
                        FOR
                                      XML PATH('') ) temp ( GROUP_INFO_ID_List )
                   WHERE
                        caar.Client_Id = @Client_Id)
            SELECT
                  App_Access_Role_Name
                 ,App_Access_Role_Dsc
                 ,Client_App_Access_Role_Id
                 ,Users_Count
                 ,Functions_Count
                 ,GROUP_INFO_ID_List
                 ,@Total_Row_Count AS Total_Row_Count
            FROM
                  cte_Client_App_Access_Role csa
            WHERE
                  csa.Row_Num BETWEEN @Start_Index AND @End_Index
            ORDER BY
				  csa.Sort_Order
                  ,csa.Row_Num    
        
END    
  
;
GO
GRANT EXECUTE ON  [dbo].[Get_Client_App_Access_Role_By_Client_Id] TO [CBMSApplication]
GO
