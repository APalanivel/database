
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	 dbo.CU_Invoice_Determinant_Account_Changes_Sel

DESCRIPTION:
			This procedure is used to select changed records between two versions from CU_INVOICE_DETERMINANT_ACCOUNT.

INPUT PARAMETERS:
	Name						DataType		Default	Description
------------------------------------------------------------
@Last_Version_Id				BIGINT			Beginnng Change tracking Version Id (From Persistent Variables) 			
@Current_Version_Id				BIGINT			Current Change tracking Version Id

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
It will execute only through the replication replacement SSIS package.

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RK			Raghu Kalvapudi
	
MODIFICATIONS
	Initials	Date			Modification
------------------------------------------------------------
	RK			08/01/2013		Created
	KVk			09/17/2014		Modified to have select correct result
******/

CREATE PROCEDURE [dbo].[CU_Invoice_Determinant_Account_Changes_Sel]
      (
       @Last_Version_Id BIGINT
      ,@Current_Version_Id BIGINT
      ,@CU_I_Last_Version_Id BIGINT )
AS
BEGIN
      SET NOCOUNT ON;

      SELECT
            convert(CHAR(1), cuida_ct.SYS_CHANGE_OPERATION) Op_Code
           ,cuida_ct.CU_INVOICE_DETERMINANT_ACCOUNT_ID
           ,NULL CU_INVOICE_DETERMINANT_ID
           ,NULL ACCOUNT_ID
           ,NULL Determinant_Expression
           ,NULL Determinant_Value
      FROM
            changetable(CHANGES dbo.CU_INVOICE_DETERMINANT_ACCOUNT, @Last_Version_Id) cuida_ct
      WHERE
            cuida_ct.SYS_Change_Version <= @Current_Version_Id
            AND convert(CHAR(1), cuida_ct.SYS_CHANGE_OPERATION) = 'D'
      UNION ALL
      SELECT
            'U' Op_Code
           ,cida.CU_INVOICE_DETERMINANT_ACCOUNT_ID
           ,cida.CU_INVOICE_DETERMINANT_ID
           ,cida.ACCOUNT_ID
           ,cida.Determinant_Expression
           ,cida.Determinant_Value
      FROM
            dbo.CU_INVOICE_DETERMINANT_ACCOUNT AS cida
            JOIN ( SELECT
                        cuida.CU_INVOICE_DETERMINANT_ACCOUNT_ID
                   FROM
                        changetable(CHANGES dbo.CU_INVOICE_DETERMINANT_ACCOUNT, @Last_Version_Id) cuida_ct
                        INNER JOIN dbo.CU_INVOICE_DETERMINANT_ACCOUNT cuida
                              ON cuida.CU_INVOICE_DETERMINANT_ACCOUNT_ID = cuida_ct.CU_INVOICE_DETERMINANT_ACCOUNT_ID
                        INNER JOIN dbo.CU_INVOICE_DETERMINANT cid
                              ON cid.CU_INVOICE_DETERMINANT_ID = cuida.CU_INVOICE_DETERMINANT_ID
                        INNER JOIN dbo.CU_INVOICE ci
                              ON ci.CU_INVOICE_ID = cid.CU_INVOICE_ID
                   WHERE
                        cuida_ct.SYS_Change_Version <= @Current_Version_Id
                        AND ci.IS_PROCESSED = 1
                        AND ci.IS_REPORTED = 1
                   UNION
                   SELECT
                        cuida.CU_INVOICE_DETERMINANT_ACCOUNT_ID
                   FROM
                        changetable(CHANGES dbo.CU_INVOICE, @CU_I_Last_Version_Id) cui_ct
                        INNER JOIN dbo.CU_INVOICE cui
                              ON cui.CU_INVOICE_ID = cui_ct.CU_INVOICE_ID
                        INNER JOIN dbo.CU_INVOICE_DETERMINANT cid
                              ON cid.CU_INVOICE_ID = cui.CU_INVOICE_ID
                        INNER JOIN dbo.CU_INVOICE_DETERMINANT_ACCOUNT cuida
                              ON cid.CU_INVOICE_DETERMINANT_ID = cuida.CU_INVOICE_DETERMINANT_ID
                   WHERE
                        cui_ct.SYS_Change_Version <= @Current_Version_Id
                        AND cui.IS_PROCESSED = 1
                        AND cui.IS_REPORTED = 1 ) AS change
                  ON change.CU_INVOICE_DETERMINANT_ACCOUNT_ID = cida.CU_INVOICE_DETERMINANT_ACCOUNT_ID
END
GO

GRANT EXECUTE ON  [dbo].[CU_Invoice_Determinant_Account_Changes_Sel] TO [ETL_Execute]
GO
