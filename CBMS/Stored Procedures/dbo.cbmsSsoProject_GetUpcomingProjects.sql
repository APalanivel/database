SET NUMERIC_ROUNDABORT OFF 
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******
NAME:
	dbo.cbmsSsoProject_GetUpcomingProjects

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@client_id     	int       	null      	
	@division_id   	int       	null      	
	@site_id       	int       	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
EXEC dbo.cbmsSsoProject_GetUpcomingProjects 49
EXEC dbo.cbmsSsoProject_GetUpcomingProjects @MyAccountId = 49, @Client_Id = 11615

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CPE			Chaitanya Panduga Eshwar

MODIFICATIONSR

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
    CPE			03/28/2011	Replaced vwCbmsSSOProjectOwnerFlat with SSO_PROJECT_OWNER_MAP table. 
							Replaced vwSsoProject_LastActivity with the base tables.	        	
******/

CREATE PROCEDURE dbo.cbmsSsoProject_GetUpcomingProjects
( 
 @MyAccountId INT
,@client_id INT = NULL
,@division_id INT = NULL
,@site_id INT = NULL )
AS 
BEGIN
	/*
		Need owners for two reasons
			1) Find distinct projects this user can see
			2) Return all owners of this project as part of RS
	*/
      SET nocount ON
	
      EXEC cbmsSecurity_GetClientAccess @MyAccountId, @client_id OUTPUT, @division_id OUTPUT, @site_id OUTPUT
	  
      SELECT DISTINCT
            SP.SSO_PROJECT_ID
           ,CASE CD.Code_Value
              WHEN 'Corporate' THEN CH.Client_Id
              WHEN 'Division' THEN CH.Sitegroup_Id
              WHEN 'Site' THEN CH.Site_Id
            END AS Owner_Id
           ,CASE CD.Code_Value
              WHEN 'Corporate' THEN CH.Client_Name
              WHEN 'Division' THEN CH.Sitegroup_Name
              WHEN 'Site' THEN RTRIM(CH.City) + ', ' + CH.State_Name + ' (' + CH.Site_name + ')'
            END AS Owner_Name
           ,Cd.Display_Seq AS Owner_Sort
           ,SP.PROJECT_TITLE
           ,SP.PROJECT_DESCRIPTION
           ,SP.PROJECT_STATUS_TYPE_ID
           ,PST.ENTITY_NAME Project_Status_Type
           ,SP.COMMODITY_TYPE_ID
           ,COM.ENTITY_NAME commodity_type
           ,SP.START_DATE
           ,SP.PROJECTED_END_DATE
           ,SP.PROJECT_OWNERSHIP_TYPE_ID
           ,POT.ENTITY_NAME project_ownership_type
           ,SP.IS_URGENT
           ,PA.SSO_PROJECT_ACTIVITY_ID
           ,PA.ACTIVITY_DATE
           ,PA.ACTIVITY_DESCRIPTION
           ,DATEDIFF(day, GETDATE(), '1/1/2003') datediffd
      FROM
            dbo.SSO_PROJECT SP
            JOIN dbo.ENTITY PST
                  ON PST.ENTITY_ID = SP.PROJECT_STATUS_TYPE_ID
            JOIN dbo.ENTITY COM
                  ON COM.ENTITY_ID = SP.COMMODITY_TYPE_ID
            JOIN ENTITY POT
                  ON POT.ENTITY_ID = SP.PROJECT_OWNERSHIP_TYPE_ID
            JOIN ( SELECT
                        SSO_PROJECT_ID
                   FROM
						Core.Client_Hier CHI
                        JOIN dbo.SSO_PROJECT_OWNER_MAP SPOM
                              ON SPOM.Client_Hier_Id = CHI.Client_Hier_Id
                   WHERE
                        ( @client_id IS NULL
                          OR CHI.Client_Id = @client_id )
                        AND ( @division_id IS NULL
                              OR CHI.Sitegroup_Id = @division_id )
                        AND ( @site_id IS NULL
                              OR CHI.Site_Id = @Site_Id )
                   GROUP BY 
						SSO_PROJECT_ID
						) acc
                  ON acc.SSO_PROJECT_ID = SP.SSO_PROJECT_ID
            JOIN dbo.SSO_PROJECT_OWNER_MAP OWN
                  ON OWN.SSO_PROJECT_ID = SP.SSO_PROJECT_ID
            JOIN Core.Client_Hier CH
                  ON OWN.Client_Hier_Id = CH.Client_Hier_Id
            JOIN dbo.Code CD
                  ON CH.Hier_level_Cd = CD.Code_Id
            LEFT JOIN ( SELECT
                              SSO_PROJECT_ID
                             ,MAX(SSO_PROJECT_ACTIVITY_ID) SSO_PROJECT_ACTIVITY_ID
                        FROM
                              dbo.SSO_PROJECT_ACTIVITY
                        GROUP BY
                              SSO_PROJECT_ID ) SPA
                  ON SP.SSO_PROJECT_ID = SPA.SSO_PROJECT_ID
            LEFT JOIN dbo.SSO_PROJECT_ACTIVITY PA
                  ON PA.SSO_PROJECT_ACTIVITY_ID = SPA.SSO_PROJECT_ACTIVITY_ID
      WHERE
            ( ( SP.IS_URGENT = 1 )
              AND ( PST.ENTITY_NAME = 'Open' ) )
            OR ( (( DATEDIFF(day, GETDATE(), SP.PROJECTED_END_DATE) <= 60 )
                 AND ( DATEDIFF(day, GETDATE(), SP.PROJECTED_END_DATE) > 0 )
                 AND ( PST.ENTITY_NAME = 'Open' )) )
      ORDER BY
            SP.PROJECT_TITLE
           ,SP.SSO_PROJECT_ID
           ,owner_sort
           ,owner_name
END


GO
GRANT EXECUTE ON  [dbo].[cbmsSsoProject_GetUpcomingProjects] TO [CBMSApplication]
GO
