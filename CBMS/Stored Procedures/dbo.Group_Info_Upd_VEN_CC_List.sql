SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 dbo.Group_Info_Upd_VEN_CC_List  
  
DESCRIPTION:  
  To Update dbo.Group_Info table by VEN_CC_List  
  
 INPUT PARAMETERS:            
                       
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
Group_Info_Id					INT
Is_Shown_On_VEN_CC_List			BIT 
  
  
 OUTPUT PARAMETERS:            
                             
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
  
 USAGE EXAMPLES:                              
---------------------------------------------------------------------------------------------------------------                              
BEGIN TRAN  
SELECT
      Cu_Invoice_Id
FROM
      dbo.cu_invoice
WHERE
      Group_Info_Id = 133  
EXEC dbo.Group_Info_Upd_VEN_CC_List 
      @Group_Info_Id = 133
     ,@Is_Shown_On_VEN_CC_List = 1  
SELECT
      Cu_Invoice_Id
FROM
      dbo.cu_invoice
WHERE
      Group_Info_Id = 133    
ROLLBACK  
  
 AUTHOR INITIALS:          
         
 Initials              Name          
---------------------------------------------------------------------------------------------------------------                        
 SP					Sandeep Pigilam  
  
 MODIFICATIONS:        
            
 Initials              Date             Modification        
---------------------------------------------------------------------------------------------------------------        
  SP				2014-09-02		Created for Data Transition.  
  
******/  
  
CREATE PROCEDURE [dbo].[Group_Info_Upd_VEN_CC_List]
      ( 
       @Group_Info_Id INT
      ,@Is_Shown_On_VEN_CC_List BIT )
AS 
BEGIN  
      SET NOCOUNT ON    
      UPDATE
            dbo.GROUP_INFO
      SET   
            Is_Shown_On_VEN_CC_List = @Is_Shown_On_VEN_CC_List
      WHERE
            Group_Info_Id = @Group_Info_Id  
END 
;
GO
GRANT EXECUTE ON  [dbo].[Group_Info_Upd_VEN_CC_List] TO [CBMSApplication]
GO
