SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







--exec BUDGET_GET_FORECAST_DETAILS_P '01/01/2007','12/01/2007'
CREATE  PROCEDURE dbo.BUDGET_GET_FORECAST_DETAILS_P
	@fromDate datetime,
	@toDate datetime
	AS
	begin
		set nocount on

		select	forecast_details.month_identifier,
			forecast_details.aggressive_price aggressive, 
			forecast_details.conservative_price conservative,
		 	forecast_details.moderate_price moderate 
			
	
		from 	rm_forecast forecast,
			rm_forecast_details forecast_details
	
		where 	forecast.forecast_as_of_date = (select max(forecast_from_date)
							from rm_forecast where forecast_type_id= 584 --//Nymex type 
					     		)
			and forecast_details.rm_forecast_id = forecast.rm_forecast_id 
			and forecast_details.month_identifier between  @fromDate and @toDate
	end









GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_FORECAST_DETAILS_P] TO [CBMSApplication]
GO
