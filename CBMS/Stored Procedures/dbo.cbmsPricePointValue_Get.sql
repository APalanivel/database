SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[cbmsPricePointValue_Get]
	( @MyAccountId int
	, @price_point_value_id int
	)
AS
BEGIN

	   select pv.price_index_value_id price_point_value_id
		, pr.index_id 		market_index_id
		, mi.entity_name 	market_index
		, pv.price_index_id 	price_point_id
		, pr.pricing_point 	price_point
		, pv.index_month	close_month
		, pv.index_value	close_price
	     from price_index_value pv
	     join price_index pr on pr.price_index_id = pv.price_index_id
	     join entity mi on mi.entity_id = pr.index_id
	    where pv.price_index_value_id = @price_point_value_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsPricePointValue_Get] TO [CBMSApplication]
GO
