SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_UPLOADED_RC_SITES_UNDER_CLIENT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@clientId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE dbo.SR_SAD_GET_UPLOADED_RC_SITES_UNDER_CLIENT_P 
@clientId int

as
set nocount on
select  s.site_id,
	vwSite.site_name
		

from	client c(nolock), 
	vwSiteName vwSite(nolock), 	
	account a(nolock),			
	sr_rc_contract_document_accounts_map accMap (nolock),
	site s(nolock)
	
where  	 vwSite.site_id = a.site_id
	and c.client_id = vwSite.client_id	
	and accMap.sr_rc_contract_document_id is not null
	and accMap.account_id = a.account_id
	and vwSite.site_id = s.site_id
	and c.client_id = @clientId

group by  s.site_id ,vwSite.site_name

order by vwSite.site_name
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_UPLOADED_RC_SITES_UNDER_CLIENT_P] TO [CBMSApplication]
GO
