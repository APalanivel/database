
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                        
 NAME: [dbo].[User_Search]                      
                      
 DESCRIPTION:   Select  Active external  users  by client id and user name ,last name .                     
                            
                        
 INPUT PARAMETERS:                        
                       
 Name                               DataType            Default				Description                        
---------------------------------------------------------------------------------------------------------------                      
 @UserName                          VARCHAR(30)         NULL                        
 @Last_Name                         VARCHAR(40)         NULL                        
 @First_Name                        VARCHAR(40)			NULL
 @Sitegroup_Id						INT					NULL				User will have option to select only Divisions
 @Site_Id							INT					NULL                     
 @Client_Id                         INT                                
 @Start_Index						INT					1          
 @End_Index							INT					2147483647  
 @Total_Row_Count					INT					0
 @Sort_Column_Name					VARCHAR(255)		'First Name ASC'	First_Name, Last_Name, Email_Address, UserName, Last_Login_Ts, Login_Cnt_Past_12 Months
 @Client_App_Access_Role_Id			INT					 NULL
 @Group_Info_Id						INT					 NULL
 @Is_Corporate						BIT					 NULL 
 @User_Access						INT					 NULL 
 @Exclude_Users						VARCHAR(MAX)		 NULL
 @Include_Users						VARCHAR(MAX)		 NULL
 
 OUTPUT PARAMETERS:                              
 Name                                DataType            Default        Description                        
---------------------------------------------------------------------------------------------------------------                      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                       

-- All users of a client
  EXEC dbo.User_Search
      @Client_Id = 11231
     ,@Start_Index = 1
     ,@End_Index = 25


-- Division Level users of a client
  EXEC dbo.User_Search
      @Client_Id = 11231
     ,@Start_Index = 1
     ,@End_Index = 25
      ,@Sitegroup_Id = 1914
    
  EXEC dbo.User_Search
      @Client_Id = 11231
     ,@Start_Index = 1
     ,@End_Index = 25
      ,@Sitegroup_Id = 12208672


-- Site Level users of a client
  EXEC dbo.User_Search 
      @Client_Id = 11231
     ,@Start_Index = 1
     ,@End_Index = 25
     ,@Site_Id = 24867

  EXEC dbo.User_Search 
      @Client_Id = 11231
     ,@Start_Index = 1
     ,@End_Index = 25
     ,@Site_Id = 24872

-- Filter By First_Name
  EXEC dbo.User_Search 
      @Client_Id = 11231
     ,@Start_Index = 1
     ,@End_Index = 25
     ,@Site_Id = 24872
     ,@First_Name = 'Don'

-- Filter By Last Name
  EXEC dbo.User_Search 
      @Client_Id = 11231
     ,@Start_Index = 1
     ,@End_Index = 25
     ,@Site_Id = 24872
     ,@Last_Name = 'Dol'


-- Filter By UserName
  EXEC dbo.User_Search 
      @Client_Id = 11231
     ,@Start_Index = 1
     ,@End_Index = 25
     ,@UserName = 'alance'


  EXEC dbo.User_Search 
      @Client_Id = 11231
     ,@Start_Index = 1
     ,@End_Index = 25
     ,@Sort_Column_Name = 'Last_Login_Ts DESC'


  EXEC dbo.User_Search 
      @Client_Id = 11231
     ,@Start_Index = 1
     ,@End_Index = 25
     ,@Sort_Column_Name = 'Login_Cnt DESC'


EXEC dbo.User_Search   
      @Client_Id = 235  
     ,@Start_Index = 1  
     ,@End_Index = 25  
     ,@Sort_Column_Name = 'Last_Name DESC'

EXEC dbo.User_Search   
      @Client_Id = 235  
     ,@Start_Index = 1  
     ,@End_Index = 25  
     ,@Sort_Column_Name = 'Last_Name ASC'     
          
-----------------------------------------------------------

  EXEC dbo.User_Search 
      @Client_Id = 235
     ,@Start_Index = 1
     ,@End_Index = 25
     ,@Total_Row_Count = 0                   
                     
  EXEC dbo.User_Search 
      @Client_Id = 110
  ,@Start_Index = 1
     ,@End_Index = 25
     ,@Total_Row_Count = 0                   
      
  EXEC dbo.User_Search
      @Client_Id = 11738
     ,@Start_Index = 1
     ,@End_Index = 50
     ,@Total_Row_Count = 0

  EXEC dbo.User_Search
      @Client_Id = 11738
     ,@Start_Index = 1
     ,@End_Index = 50
     ,@Total_Row_Count = 0 
     ,@Client_App_Access_Role_Id=907
     ,@GROUP_INFO_ID=210


-- users who is having ALL sites of the given sitegroup
  EXEC dbo.User_Search 
      @Client_Id = 235
     ,@Start_Index = 1
     ,@sitegroup_id = 258
     ,@User_Access = -1
     ,@End_Index = 25
     ,@Total_Row_Count = 0  

-- users who is having ANY one sites of the given sitegroup    
  EXEC dbo.User_Search 
      @Client_Id = 235
     ,@Start_Index = 1
     ,@sitegroup_id = 258
     ,@User_Access = 1
     ,@End_Index = 25
     ,@Total_Row_Count = 0 

  EXEC dbo.User_Search
      @Client_Id = 11738
     ,@Start_Index = 1
     ,@End_Index = 50
     ,@Total_Row_Count = 0
     ,@Exclude_users='44176,36611'  

  EXEC dbo.User_Search
      @Client_Id = 11738
     ,@Start_Index = 1
     ,@End_Index = 50
     ,@Total_Row_Count = 0
     ,@Include_Users='44176,36611'              

 AUTHOR INITIALS:                       
                       
 Initials               Name                        
---------------------------------------------------------------------------------------------------------------                      
 NR                     Narayana Reddy     
 SP						Sandeep Pigilam                     

 MODIFICATIONS:                       

 Initials               Date            Modification                      
---------------------------------------------------------------------------------------------------------------                      
 NR                     2013-12-11      Created for RA Admin user management     
 NR                     2014-03-12      MAINT-2638  Access_Level_Name column should return 'Client' instead of 'Corporate'   
 SP						2014-04-24		RA User Admin - bulk edit users enhancement ,Added three extra input parameters @Client_App_Access_Role_Id,       
										@Group_Info_Id,@Is_Corporate     
 SP						2014-05-20      RA-Admin BULK UPDATE Added 	@User_Access WITH -1(all) AND 1(any) meaning 
										users HAVING all sites OF given sitegroup OR ANY.now sitegroup_id INPUT parameter IS taking Division/Group .
										added @Exclude_Users AND @Include_Users AS optional parameters.
 									

******/
 
            
CREATE PROCEDURE [dbo].[User_Search]
      ( 
       @UserName VARCHAR(30) = NULL
      ,@Last_Name VARCHAR(40) = NULL
      ,@First_Name VARCHAR(40) = NULL
      ,@Sitegroup_Id INT = NULL
      ,@Site_Id INT = NULL
      ,@Client_Id INT
      ,@Start_Index INT = 1
      ,@End_Index INT = 2147483647
      ,@Total_Row_Count INT = 0
      ,@Sort_Column_Name VARCHAR(255) = 'First_Name ASC'
      ,@Client_App_Access_Role_Id INT = NULL
      ,@Group_Info_Id INT = NULL
      ,@Is_Corporate BIT = NULL
      ,@User_Access INT = NULL
      ,@Exclude_Users VARCHAR(MAX) = NULL
      ,@Include_Users VARCHAR(MAX) = NULL )
      WITH RECOMPILE
AS 
BEGIN      
      
      SET NOCOUNT ON           
      
      CREATE TABLE #User_Attribute
            ( 
             User_Info_Id INT PRIMARY KEY CLUSTERED
            ,UserName VARCHAR(30)
            ,Queue_Id INT
            ,First_Name VARCHAR(40)
            ,Middle_Name VARCHAR(40)
            ,Last_Name VARCHAR(40)
            ,Email_Address VARCHAR(150)
            ,Is_History BIT
            ,Access_Level INT
            ,Client_Id INT
            ,Client_Name VARCHAR(200)
            ,Phone_Number VARCHAR(30)
            ,Job_Title VARCHAR(255)
            ,Address_1 VARCHAR(300)
            ,Address_2 VARCHAR(300)
            ,City VARCHAR(50)
            ,State_Id INT
            ,Country_Id INT
            ,ZipCode VARCHAR(10)
            ,Locale_Code VARCHAR(10)
            ,Phone_Number_Ext VARCHAR(10)
            ,Cell_Number VARCHAR(200)
            ,Fax_Number VARCHAR(200)
            ,Created_Ts DATE
            ,Last_Login_Ts DATE
            ,Login_Cnt INT
            ,Is_Corporate BIT )
           
      DECLARE @User_Access_Level TABLE
            ( 
             User_Info_Id INT PRIMARY KEY CLUSTERED
            ,Security_Role_Id INT
            ,Is_Corporate_User BIT
            ,Is_Division_User BIT
            ,Is_Site_User BIT )


      DECLARE @All_Site_Security_Role_tbl TABLE
            ( 
             Security_Role_Id INT
            ,Site_Count INT )            

      DECLARE
            @UserName_Var VARCHAR(32)= NULL
           ,@Last_Name_Var VARCHAR(42)= NULL
           ,@First_Name_Var VARCHAR(42)= NULL
           ,@Login_Cnt_From_Month DATE = CAST(DATEADD(DD, -DATEPART(DAY, DATEADD(MM, -12, GETDATE())) + 1, DATEADD(MM, -12, GETDATE())) AS DATE)
           ,@Sitegroup_Client_Hier_Id INT
           ,@Site_Client_Hier_Id INT
           ,@All_Site_Cnt INT

      SET @UserName_Var = '%' + @UserName + '%'                        
      SET @Last_Name_Var = '%' + @Last_Name + '%'                        
      SET @First_Name_Var = '%' + @First_Name + '%'                            
      
      SELECT
            @Site_Client_Hier_Id = Client_Hier_Id
      FROM
            Core.Client_Hier
      WHERE
            Site_Id = @Site_Id
            
      SELECT
            @Sitegroup_Client_Hier_Id = Client_Hier_Id
      FROM
            Core.Client_Hier
      WHERE
            Sitegroup_Id = @Sitegroup_Id
            AND Site_Id = 0
            
            

      
      SELECT
            @All_Site_Cnt = COUNT(1)
      FROM
            Sitegroup_Site sgs
            INNER JOIN Core.Client_Hier ch
                  ON ch.Site_Id = sgs.Site_id
      WHERE
            sgs.Sitegroup_Id = @Sitegroup_Id


      INSERT      INTO @All_Site_Security_Role_tbl
                  ( 
                   Security_Role_Id
                  ,Site_Count )
                  SELECT
                        srch.Security_Role_Id
                       ,COUNT(ch.Site_Id)
                  FROM
                        Core.Client_Hier ch
                        INNER JOIN dbo.Security_Role_Client_Hier srch
                              ON srch.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        ch.CLIENT_ID = @Client_Id
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.Sitegroup_Site sgs
                                     WHERE
                                          sgs.Site_id = ch.Site_Id
                                          AND sgs.Sitegroup_id = @Sitegroup_Id )
                  GROUP BY
                        srch.Security_Role_Id
                  --HAVING
                  --      COUNT(1) = @All_Site_Cnt                              

      INSERT      INTO #User_Attribute
                  ( 
                   User_Info_Id
                  ,UserName
                  ,Queue_Id
                  ,First_Name
                  ,Middle_Name
                  ,Last_Name
                  ,Email_Address
                  ,Is_History
                  ,Access_Level
                  ,Client_Id
                  ,Client_Name
                  ,Phone_Number
                  ,Job_Title
                  ,Address_1
                  ,Address_2
                  ,City
                  ,State_Id
                  ,Country_Id
                  ,ZipCode
                  ,Locale_Code
                  ,Phone_Number_Ext
                  ,Cell_Number
                  ,Fax_Number
                  ,Created_Ts
                  ,Is_Corporate )
                  SELECT
                        u.User_Info_Id
                       ,u.UserName
                       ,u.Queue_Id
                       ,u.First_Name
                       ,u.Middle_Name
                       ,u.Last_Name
                       ,u.Email_Address
                       ,u.Is_History
                       ,u.Access_Level
                       ,u.Client_Id
                       ,c.Client_Name
                       ,u.Phone_Number
                       ,u.Job_Title
                       ,u.Address_1
                       ,u.Address_2
                       ,u.City
                       ,u.State_Id
                       ,u.Country_Id
                       ,u.ZipCode
                       ,u.Locale_Code
                       ,u.Phone_Number_Ext
                       ,u.Cell_Number
                       ,u.Fax_Number
                       ,u.Created_Ts
                       ,sr.Is_Corporate
                  FROM
                        dbo.USER_INFO u
                        INNER JOIN dbo.CLIENT c
                              ON c.Client_Id = u.Client_Id
                        INNER JOIN dbo.User_Security_Role usr
                              ON u.USER_INFO_ID = usr.User_Info_Id
                        INNER JOIN dbo.Security_Role sr
                              ON usr.Security_Role_Id = sr.Security_Role_Id
                                 AND c.CLIENT_ID = sr.Client_Id
                  WHERE
                        c.Client_Id = @Client_Id
                        AND ( @UserName_Var IS NULL
                              OR u.UserName LIKE @UserName_Var )
                        AND ( @Last_Name_Var IS NULL
                              OR u.Last_Name LIKE @Last_Name_Var )
                        AND ( @First_Name_Var IS NULL
                              OR u.First_Name LIKE @First_Name_Var )
                        AND u.Is_History = 0
                        AND u.Access_Level = 1
                        AND ( @Site_Id IS NULL
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.User_Security_Role usr
                                                INNER JOIN dbo.Security_Role_Client_Hier srch
                                                      ON srch.Security_Role_Id = usr.Security_Role_Id
                                          WHERE
                                                srch.Client_Hier_Id = @Site_Client_Hier_Id
                                                AND usr.User_Info_Id = u.User_Info_Id ) )
                        AND ( ( @Sitegroup_Id IS NULL
                                AND @User_Access IS NULL )
                              OR ( @User_Access IS NULL
                                   AND EXISTS ( SELECT
                                                      1
                                                FROM
                                                      dbo.User_Security_Role usr
                                                      INNER JOIN dbo.Security_Role_Client_Hier srch
                                                            ON srch.Security_Role_Id = usr.Security_Role_Id
                                                WHERE
                                                      srch.Client_Hier_Id = @Sitegroup_Client_Hier_Id
                                                      AND usr.User_Info_Id = u.User_Info_Id ) )
                              OR ( @User_Access = -1
                                   AND EXISTS ( SELECT
                                                      1
                                                FROM
                                                      @All_Site_Security_Role_tbl asr
                                                WHERE
                                                      asr.Site_Count = @All_Site_Cnt
                                                      AND asr.Security_Role_Id = sr.Security_Role_Id ) )
                              OR ( @User_Access = 1
                                   AND EXISTS ( SELECT
                                                      1
                                                FROM
                                                      @All_Site_Security_Role_tbl asr
                                                WHERE
                                                      asr.Security_Role_Id = sr.Security_Role_Id ) ) )
                        AND ( @Is_Corporate IS NULL
                              OR sr.Is_Corporate = @Is_Corporate )
                        AND ( @Client_App_Access_Role_Id IS NULL
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.User_Info_Client_App_Access_Role_Map uicaarm
                                          WHERE
                                                u.USER_INFO_ID = uicaarm.USER_INFO_ID
                                                AND Client_App_Access_Role_Id = @Client_App_Access_Role_Id ) )
                        AND ( @Group_Info_Id IS NULL
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.USER_INFO_GROUP_INFO_MAP uigim
                                          WHERE
                                                u.USER_INFO_ID = uigim.USER_INFO_ID
                                                AND uigim.GROUP_INFO_ID = @Group_Info_Id
                                          UNION
                                          SELECT
                                                1
                                          FROM
                                                dbo.Client_App_Access_Role_Group_Info_Map gim
                                                INNER JOIN dbo.Client_App_Access_Role ar
                                                      ON gim.Client_App_Access_Role_Id = ar.Client_App_Access_Role_Id
                                                         AND ar.Client_Id = @Client_Id
                                                INNER JOIN dbo.User_Info_Client_App_Access_Role_Map urm
                                                      ON ar.Client_App_Access_Role_Id = urm.Client_App_Access_Role_Id
                                                         AND urm.User_Info_Id = u.USER_INFO_ID
                                          WHERE
                                                gim.GROUP_INFO_ID = @Group_Info_Id ) )
                        AND ( @Exclude_Users IS NULL
                              OR ( @Include_Users IS NULL
                                   AND NOT EXISTS ( SELECT
                                                      1
                                                    FROM
                                                      dbo.ufn_split(@Exclude_Users, ',') x
                                                    WHERE
                                                      x.Segments = u.USER_INFO_ID ) ) )
                        AND ( @Include_Users IS NULL
                              OR ( @Exclude_Users IS NULL
                                   AND EXISTS ( SELECT
                                                      1
                                                FROM
                                                      dbo.ufn_split(@Include_Users, ',') x
                                                WHERE
                                                      x.Segments = u.USER_INFO_ID ) ) )                                                      
                                                                                          
					                                         

      --SET @Total_Row_Count = @@ROWCOUNT

      INSERT      INTO @User_Access_Level
                  ( 
                   User_Info_Id
                  ,Security_Role_Id
                  ,Is_Corporate_User
                  ,Is_Division_User
                  ,Is_Site_User )
                  SELECT
                        usr.User_Info_Id
                       ,usr.Security_Role_Id
                       ,MAX(CASE WHEN hl.Code_Value = 'Corporate' THEN 1
                                 ELSE 0
                            END) AS Is_Corporate_User
                       ,MAX(CASE WHEN hl.Code_Value = 'Division' THEN 1
                                 ELSE 0
                            END) AS Is_Division_User
                       ,MAX(CASE WHEN hl.Code_Value = 'Site' THEN 1
                                 ELSE 0
                            END) AS Is_Site_User
                  FROM
                        dbo.User_Security_Role usr
                        INNER JOIN dbo.Security_Role_Client_Hier csrch
                              ON csrch.Security_Role_Id = usr.Security_Role_Id
                        INNER JOIN core.Client_Hier ch
                              ON ch.Client_Hier_Id = csrch.Client_Hier_Id
                        INNER JOIN dbo.Code hl
                              ON hl.Code_Id = ch.Hier_level_Cd
                        INNER JOIN #User_Attribute ua
                              ON ua.User_Info_Id = usr.User_Info_Id
                  WHERE
                        ch.Client_Id = @Client_Id
                  GROUP BY
                        usr.User_Info_Id
                       ,usr.Security_Role_Id


      UPDATE
            ua
      SET   
            ua.Last_Login_Ts = CAST(lt.Login_Ts AS DATE)
      FROM
            #User_Attribute ua
            INNER JOIN ( SELECT
                              MAX(lt.Login_Ts) AS Login_Ts
                             ,lt.User_Info_Id
                         FROM
                              dbo.RA_User_Login_Ts lt
                              INNER JOIN #User_Attribute ua
                                    ON ua.User_Info_Id = lt.User_Info_Id
                         GROUP BY
                              lt.User_Info_Id ) lt
                  ON lt.User_Info_Id = ua.User_Info_Id

      UPDATE
            ua
      SET   
            ua.Login_Cnt = lc.Login_Cnt
      FROM
            #User_Attribute ua
            INNER JOIN ( SELECT
                              SUM(ulc.Login_Cnt) AS Login_Cnt
                             ,ulc.User_Info_Id
                         FROM
                              dbo.RA_User_Monthly_Login_Count ulc
                         WHERE
                              ulc.Login_Month >= @Login_Cnt_From_Month
                         GROUP BY
                              ulc.User_Info_Id ) lc
                  ON lc.User_Info_Id = ua.User_Info_Id;
      WITH  cte_User_Attributes
              AS ( SELECT
                        u.User_Info_Id
                       ,u.UserName
                       ,u.Queue_Id
                       ,u.First_Name
                       ,u.Middle_Name
                       ,u.Last_Name
                       ,u.Email_Address
                       ,u.Is_History
                       ,u.Access_Level
                       ,u.Client_Id
                       ,u.Client_Name
                       ,u.Phone_Number
                       ,u.Job_Title
                       ,u.Address_1
                       ,u.Address_2
                       ,u.City
                       ,u.State_Id
                       ,u.Country_Id
                       ,u.ZipCode
                       ,u.Locale_Code
                       ,u.Phone_Number_Ext
                       ,u.Cell_Number
                       ,u.Fax_Number
                       ,u.Created_Ts
                       ,u.Last_Login_Ts
                       ,u.Login_Cnt
                       ,u.Is_Corporate
                       ,st.STATE_NAME AS State
                       ,cy.COUNTRY_NAME AS Country
                       ,ual.Security_Role_Id
                       ,LEFT(rn.App_Access_Role_Name, LEN(rn.App_Access_Role_Name) - 1) AS Role_Name
                       ,CASE WHEN ual.Is_Corporate_User = 1 THEN 'Client'
                             WHEN ual.Is_Division_User = 1 THEN 'Division'
                             WHEN ual.Is_Site_User = 1 THEN 'Site'
                        END AS Access_Level_Name
                       ,lc.Code_Dsc AS Locale_Code_Desc
                       ,ROW_NUMBER() OVER ( ORDER BY ( CASE WHEN @Sort_Column_Name = 'First_Name ASC' THEN u.First_Name
                                                            WHEN @Sort_Column_Name = 'Last_Name ASC' THEN u.Last_Name
                                                            WHEN @Sort_Column_Name = 'UserName ASC' THEN u.UserName
                                                            WHEN @Sort_Column_Name = 'Email_Address ASC' THEN u.Email_Address
                                                       END ) ASC
                                                       , ( CASE WHEN @Sort_Column_Name = 'First_Name DESC' THEN u.First_Name
                                                                WHEN @Sort_Column_Name = 'Last_Name DESC' THEN u.Last_Name
                                                                WHEN @Sort_Column_Name = 'UserName DESC' THEN u.UserName
                                                                WHEN @Sort_Column_Name = 'Email_Address DESC' THEN u.Email_Address
                                                           END ) DESC
                                                       , ( CASE WHEN @Sort_Column_Name = 'Login_Cnt ASC' THEN u.Login_Cnt
                                                           END ) ASC
													   , ( CASE WHEN @Sort_Column_Name = 'Login_Cnt DESC' THEN u.Login_Cnt
                                                           END ) DESC 
															   , ( CASE WHEN @Sort_Column_Name = 'Last_Login_Ts ASC' THEN u.Last_Login_Ts
                                                                   END ) ASC
													   , ( CASE WHEN @Sort_Column_Name = 'Last_Login_Ts DESC' THEN u.Last_Login_Ts
                                                           END ) DESC ) AS Row_Num
                       ,COUNT(1) OVER ( ) AS Total_Row_Count
                   FROM
                        #User_Attribute u
                        INNER JOIN dbo.Code lc
                              ON lc.Code_Value = u.Locale_Code
                        INNER JOIN dbo.Codeset cs
                              ON cs.Codeset_Id = lc.Codeset_Id
                        INNER JOIN @User_Access_Level ual
                              ON ual.User_Info_Id = u.User_Info_Id
                        LEFT OUTER JOIN dbo.COUNTRY cy
                              ON cy.Country_Id = u.Country_Id
                        LEFT OUTER JOIN dbo.State st
                              ON st.State_Id = u.State_Id
                        CROSS APPLY ( SELECT
                                          car.App_Access_Role_Name + ','
                                      FROM
                                          dbo.Client_App_Access_Role car
                                          INNER JOIN dbo.User_Info_Client_App_Access_Role_Map uiaarm
                                                ON uiaarm.Client_App_Access_Role_Id = car.Client_App_Access_Role_Id
                                      WHERE
                                          uiaarm.User_Info_Id = u.User_Info_Id
                        FOR
                                      XML PATH('') ) rn ( App_Access_Role_Name )
                   WHERE
                        cs.Codeset_Name = 'LocalizationLanguage')
            SELECT
                  ua.User_Info_Id
                 ,ua.UserName
                 ,ua.Queue_Id
                 ,ua.First_Name
                 ,ua.Middle_Name
                 ,ua.Last_Name
                 ,ua.Email_Address
                 ,ua.Is_History
                 ,ua.Access_Level
                 ,ua.Client_Id
                 ,ua.Client_Name
                 ,ua.Phone_Number
                 ,Security_Role_Id
                 ,Role_Name
                 ,Access_Level_Name
                 ,ua.Last_Login_Ts AS Login_ts
                 ,ua.Login_Cnt
                 ,ua.Job_Title
                 ,ua.Address_1
                 ,ua.Address_2
                 ,ua.City
                 ,State
                 ,Country
                 ,ua.ZipCode
                 ,ua.Locale_Code
                 ,Locale_Code_Desc
                 ,ua.Phone_Number_Ext
                 ,ua.Cell_Number
                 ,ua.Fax_Number
                 ,ua.Created_Ts
                 ,ua.Is_Corporate
                 ,Total_Row_Count
                 ,ua.Row_Num
            FROM
                  cte_User_Attributes ua
            WHERE
                  ua.Row_Num BETWEEN @Start_Index AND @End_Index
                  

      DROP TABLE #User_Attribute
           
END














;
GO


GRANT EXECUTE ON  [dbo].[User_Search] TO [CBMSApplication]
GO
