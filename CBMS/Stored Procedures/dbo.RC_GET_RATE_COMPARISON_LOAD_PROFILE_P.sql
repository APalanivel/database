
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
 CBMS.dbo.RC_GET_RATE_COMPARISON_LOAD_PROFILE_P 
 
DESCRIPTION:
INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@AccountID     	INT       	          	
	@SiteID        	INT       	          	
	@Crr_Service_Month	DATETIME  	          	
	@Prv_Service_Month	DATETIME  	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

exec RC_GET_RATE_COMPARISON_LOAD_PROFILE_P 1187,1483,'2011-06-01','2010-06-01'
exec RC_GET_RATE_COMPARISON_LOAD_PROFILE_P 1187,1483,'2011-07-01','2010-07-01'


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	AP		Athmaram Pabbathi
	
MODIFICATIONS
    Initials	 Date	   Modification
------------------------------------------------------------
    			 9/21/2010   Modify Quoted Identifier
    DMR		 09/10/2010  Modified for Quoted_Identifier
    AP		 07/28/2011  Modified the SP logic and Remove Cost_Usage table & use Cost_Usage_Account_Dtl, Bucket_Master, Code, Commodity tables
    AP		 08/22/2011  Added qualifiers to all objects with the owner name.
    AP		 08/23/2011  Modified the IF logic to return the result set in row format for each bucket; added columns Bucket_Name, Bucket_Value, UOM_Type_ID, UOM_Name, Service_Month
						  and removed following colums EL_Usage ,EL_On_Peak_Usage ,EL_Off_Peak_Usage ,EL_INT_Peak_Usage ,EL_Actual_Demand ,EL_Billing_Demand 
						  ,EL_Contract_Demand ,NG_Usage ,Service_Month ,EL_Entity_Name ,Billing_Name ,Actual_Name ,Contract_Name ,NG_Unit_Of_Measure_Type_ID ,NG_Unit ,System_Row
    AP		 09/21/2011  Added Commodity_Name, Bucket_Type in the return resultset and Removed If-Else section and used table variable and @@rowcount logic
						  to insert/process the data for current year else for previous year
    AP		 10/04/2011  Added a subselect statement in the end result set to return data only for most recent month.
    AP		 10/11/2011  Create @Bucket_List table variable and modified the result set script to return all buckets irrespective of values present.
******/

CREATE PROCEDURE [dbo].[RC_GET_RATE_COMPARISON_LOAD_PROFILE_P]
      ( 
       @AccountID INT
      ,@Site_Id INT
      ,@Crr_Service_Month DATETIME
      ,@Prv_Service_Month DATETIME )
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE @Affected_Rows INT

      DECLARE @Cost_Usage_Account_Dtl TABLE
            ( 
             Bucket_Master_Id INT
            ,Bucket_Name VARCHAR(255)
            ,Bucket_Value DECIMAL(28, 10)
            ,UOM_Type_Id INT
            ,Service_Month DATETIME
            ,Commodity_Name VARCHAR(50)
            ,Bucket_Type VARCHAR(25)
            ,PRIMARY KEY CLUSTERED ( Bucket_Master_Id, Service_Month ) )

      DECLARE @Bucket_List TABLE
            ( 
             Bucket_Master_Id INT PRIMARY KEY CLUSTERED
            ,Bucket_Name VARCHAR(255)
            ,Bucket_Type VARCHAR(25)
            ,Commodity_Name VARCHAR(50)
            ,Default_UOM_Type_Id INT )

      INSERT      INTO @Bucket_List
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Bucket_Type
                  ,Commodity_Name
                  ,Default_UOM_Type_Id )
                  SELECT
                        BM.Bucket_Master_Id
                       ,BM.Bucket_Name
                       ,CD.Code_Value
                       ,COM.Commodity_Name
                       ,BM.Default_Uom_Type_Id
                  FROM
                        dbo.Bucket_Master BM
                        INNER JOIN dbo.Code CD
                              ON CD.Code_Id = BM.Bucket_Type_Cd
                        INNER JOIN dbo.Commodity COM
                              ON COM.Commodity_Id = BM.Commodity_Id
                  WHERE
                        COM.Commodity_Name IN ( 'Natural Gas', 'Electric Power' )
                        AND BM.Bucket_Name IN ( 'Total Usage', 'On-Peak Usage', 'Off-Peak Usage', 'Other-Peak Usage', 'Demand', 'Billed Demand', 'Contract Demand' )
                        AND CD.Code_Value = 'Determinant'


      INSERT      INTO @Cost_Usage_Account_Dtl
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Bucket_Value
                  ,UOM_Type_Id
                  ,Service_Month
                  ,Commodity_Name
                  ,Bucket_Type )
                  SELECT
                        BL.Bucket_Master_Id
                       ,BL.Bucket_Name
                       ,CUAD.Bucket_Value
                       ,CUAD.UOM_Type_Id
                       ,CUAD.Service_Month
                       ,BL.Commodity_Name
                       ,BL.Bucket_Type
                  FROM
                        dbo.Cost_Usage_Account_Dtl CUAD
                        INNER JOIN core.Client_Hier ch
                              ON ch.Client_Hier_id = cuad.Client_Hier_Id
                        INNER JOIN @Bucket_List BL
                              ON BL.Bucket_Master_Id = CUAD.Bucket_Master_Id
                  WHERE
                        cuad.Account_ID = @AccountID
                        AND ch.Site_Id = @Site_id
                        AND CUAD.Service_Month = @Crr_Service_Month

      SET @Affected_Rows = @@ROWCOUNT


      INSERT      INTO @Cost_Usage_Account_Dtl
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Bucket_Value
                  ,UOM_Type_Id
                  ,Service_Month
                  ,Commodity_Name
                  ,Bucket_Type )
                  SELECT
                        BL.Bucket_Master_Id
                       ,BL.Bucket_Name
                       ,CUAD.Bucket_Value
                       ,CUAD.UOM_Type_Id
                       ,CUAD.Service_Month
                       ,BL.Commodity_Name
                       ,BL.Bucket_Type
                  FROM
                        dbo.Cost_Usage_Account_Dtl CUAD
                        INNER JOIN core.Client_Hier ch
                              ON ch.Client_Hier_id = cuad.Client_Hier_Id
                        INNER JOIN @Bucket_List BL
                              ON BL.Bucket_Master_Id = CUAD.Bucket_Master_Id
                        INNER JOIN ( SELECT
                                          max(cuad.Service_Month) Last_Previous_Month
                                     FROM
                                          dbo.Cost_Usage_Account_Dtl CUAD
                                          INNER JOIN core.Client_Hier ch
                                                ON ch.Client_Hier_id = cuad.Client_Hier_Id
                                          INNER JOIN @Bucket_List BL
                                                ON BL.Bucket_Master_Id = CUAD.Bucket_Master_Id
                                     WHERE
                                          CUAD.Account_ID = @AccountID
                                          AND ch.Site_Id = @Site_Id
                                          AND month(CUAD.Service_Month) = month(@Prv_Service_Month)
                                          AND year(CUAD.Service_Month) <= year(@Prv_Service_Month) ) CUAD_Sub
                              ON CUAD_Sub.Last_Previous_Month = CUAD.Service_Month
                  WHERE
                        cuad.Account_ID = @AccountID
                        AND ch.Site_Id = @Site_Id
                        AND month(CUAD.Service_Month) = month(@Prv_Service_Month)
                        AND year(CUAD.Service_Month) <= year(@Prv_Service_Month)
                        AND @Affected_Rows = 0


      SELECT
            BL.Bucket_Name
           ,CUAD.Bucket_Value
           ,isnull(CUAD.UOM_Type_Id, BL.Default_UOM_Type_Id) AS UOM_Type_Id
           ,UOM.Entity_Name AS UOM_Name
           ,CUAD.Service_Month
           ,BL.Commodity_Name
           ,BL.Bucket_Type
      FROM
            @Bucket_List BL
            LEFT OUTER JOIN @Cost_Usage_Account_Dtl CUAD
                  ON BL.Bucket_Master_Id = CUAD.Bucket_Master_Id
            LEFT OUTER JOIN dbo.Entity UOM
                  ON UOM.Entity_ID = isnull(CUAD.UOM_Type_Id, BL.Default_UOM_Type_Id)

END

;
GO

GRANT EXECUTE ON  [dbo].[RC_GET_RATE_COMPARISON_LOAD_PROFILE_P] TO [CBMSApplication]
GO
