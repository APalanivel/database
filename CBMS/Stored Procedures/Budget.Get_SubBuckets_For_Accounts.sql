SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:      
 CBMS.Budget.Get_SubBuckets_For_Accounts      
      
DESCRIPTION:      
  Script to get available sub buckets for that account's state and commodity for which invoice is posted      
      
INPUT PARAMETERS:      
 Name     DataType  Default Description      
---------------------------------------------------------------      
 @Cu_Invoice_Id    INT      
       
      
OUTPUT PARAMETERS:      
 Name     DataType  Default Description      
------------------------------------------------------------      
       
USAGE EXAMPLES:      
------------------------------------------------------------      
      
 EXEC Budget.Get_SubBuckets_For_Accounts 26654,'2001-02-01','2019-03-03'      
     
 --For SubBuckets    
EXEC Budget.Get_SubBuckets_For_Accounts @intAccount_ID = 26654,@dtStart_Date = '2018-02-01',@dtEnd_Date = '2019-03-03';    
EXEC Budget.Get_SubBuckets_For_Accounts @intAccount_ID = 1335321,@dtStart_Date = '2018-02-01',@dtEnd_Date = '2019-03-03';    
EXEC Budget.Get_SubBuckets_For_Accounts  
    @intAccount_ID = 315828  
    , @intMeter_ID = 235722  
    , @dtStart_Date = '2019-01-01'  
    , @dtEnd_Date = '2019-02-03';  
  
  
 EXEC Budget.Get_SubBuckets_For_Accounts  
    @intAccount_ID = 55034  
    , @intMeter_ID = 37988  
    , @dtStart_Date = '2017-01-01'  
    , @dtEnd_Date = '2017-12-01';  
  
EXEC Budget.Get_SubBuckets_For_Accounts
    @intAccount_ID = 1006
    , @intMeter_ID = 55
    , @dtStart_Date = '2018-01-01'
    , @dtEnd_Date = '2019-12-01';
  
  
      
AUTHOR INITIALS:      
 Initials Name      
------------------------------------------------------------      
 PRG  Prasanna R Gachinmani 
 NM   Nagaraju Muppa   
    
       
MODIFICATIONS      
 Initials Date   Modification      
------------------------------------------------------------      
 PRG      2019-04-03 Initial Development    
 PRG   2019-04-26 Exception Handling for Future dates, Where no data is present and dynamically pivoted.    
 PRG   2019-05-02 Handled Case UOM conversion for Sub buckets based on Majority Case / Removed dynamic pivoting which will be handled by Serivce side.    
 PRG   2019-05-06 Added Meter ID Attribute parameter.  
 RKV   2019-05-30 Added Inovice id for the all the rows even for the not related sub buckets.   
 RKV   2019-06-19 Added Account_Id to on clause of CU_INVOICE_DETERMINANT_ACCOUNT
 NM    2020-01-02 Maint-9650,Returning the determinant value with out spliting by Month Count.	  
      
******/
CREATE PROC [Budget].[Get_SubBuckets_For_Accounts]
     (
         @intAccount_ID BIGINT
         , @intMeter_ID BIGINT = NULL
         , @dtStart_Date DATETIME
         , @dtEnd_Date DATETIME
     )
AS
    BEGIN


        DECLARE
            @Str_Query NVARCHAR(MAX) = ''
            , @Str_Service_months NVARCHAR(MAX) = ''
            , @dtFirstDayOfMonth DATE = DATEADD(MONTH, DATEDIFF(MONTH, 0, @dtStart_Date), 0);



        CREATE TABLE #List_Sub_Buckets
             (
                 Sub_Bucket_Name VARCHAR(256)
                 , Service_Months DATE
             );
        DECLARE @Invoice_Ids_Service_Month TABLE
              (
                  Service_Month DATE
                  , CU_INVOICE_ID INT
              );

        DECLARE @Determinant_Value_Spilt_By_Cu_Invoice_Id TABLE
              (
                  service_Month DATE
                  , Cu_Inovice_Id INT
              );

        DECLARE @Cu_Invoice_Id_No_Of_Months TABLE
              (
                  Cu_Inovice_Id INT
                  , No_Of_Months INT
              );

        DECLARE @Service_Month_Inv_Cnt TABLE
              (
                  service_Month DATE
                  , Split_Cnt INT
              );

        SELECT
            CASE WHEN cudet.EC_Invoice_Sub_Bucket_Master_Id IS NULL THEN bm.Bucket_Name
                ELSE eisbm.Sub_Bucket_Name
            END AS Sub_Bucket_Name
            , cism.SERVICE_MONTH
            , CASE WHEN CASE WHEN cudet.EC_Invoice_Sub_Bucket_Master_Id IS NULL THEN bm.Bucket_Name
                            ELSE eisbm.Sub_Bucket_Name
                        END LIKE '%Demand%' THEN
                       MAX(CONVERT(
                               DECIMAL(28, 10)
                               , REPLACE(
                                     REPLACE(
                                         REPLACE(
                                             REPLACE(
                                                 REPLACE(
                                                     REPLACE(
                                                         (CASE WHEN (PATINDEX('%[0-9]-', cudet.DETERMINANT_VALUE)) > 0 THEN
                                                                   STUFF(
                                                                       cudet.DETERMINANT_VALUE
                                                                       , PATINDEX('%[0-9]-', cudet.DETERMINANT_VALUE)
                                                                         + 1, 1, '')
                                                              ELSE cudet.DETERMINANT_VALUE
                                                          END), ',', ''), '0-', 0), '+', ''), '$', ''), ')', ''), '('
                                     , '')))
                  ELSE
                      SUM(CONVERT(
                              DECIMAL(28, 10)
                              , REPLACE(
                                    REPLACE(
                                        REPLACE(
                                            REPLACE(
                                                REPLACE(
                                                    REPLACE(
                                                        (CASE WHEN (PATINDEX('%[0-9]-', cudet.DETERMINANT_VALUE)) > 0 THEN
                                                                  STUFF(
                                                                      cudet.DETERMINANT_VALUE
                                                                      , PATINDEX('%[0-9]-', cudet.DETERMINANT_VALUE)
                                                                        + 1, 1, '')
                                                             ELSE cudet.DETERMINANT_VALUE
                                                         END), ',', ''), '0-', 0), '+', ''), '$', ''), ')', ''), '('
                                    , '')))
              END AS DETERMINANT_VALUE
            , cudet.UNIT_OF_MEASURE_TYPE_ID
            , MAX(cism.CU_INVOICE_ID) CU_INVOICE_ID
        INTO
            #tmp_SubBuckets_Determinants_Sum_Value
        FROM
            dbo.CU_INVOICE cui
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cui.CU_INVOICE_ID = cism.CU_INVOICE_ID
            LEFT OUTER JOIN dbo.CU_INVOICE_DETERMINANT cudet
                ON cudet.CU_INVOICE_ID = cism.CU_INVOICE_ID
            INNER JOIN dbo.CU_INVOICE_DETERMINANT_ACCOUNT cida
                ON cida.CU_INVOICE_DETERMINANT_ID = cudet.CU_INVOICE_DETERMINANT_ID
                   AND  cida.ACCOUNT_ID = cism.Account_ID
            INNER JOIN Core.Client_Hier_Account cha
                ON cism.Account_ID = cha.Account_Id
            LEFT OUTER JOIN dbo.EC_Invoice_Sub_Bucket_Master eisbm
                ON cudet.EC_Invoice_Sub_Bucket_Master_Id = eisbm.EC_Invoice_Sub_Bucket_Master_Id
            LEFT OUTER JOIN dbo.Bucket_Master bm
                ON cudet.Bucket_Master_Id = bm.Bucket_Master_Id
                   AND  cha.Commodity_Id = bm.Commodity_Id
            LEFT OUTER JOIN dbo.Code c
                ON c.Code_Id = bm.Bucket_Type_Cd
        WHERE
            cism.Account_ID = @intAccount_ID
            AND cha.Meter_Id = @intMeter_ID
            AND c.Code_Value = 'Determinant'
            AND cism.SERVICE_MONTH BETWEEN @dtStart_Date
                                   AND     @dtEnd_Date
            AND cui.IS_REPORTED = 1
        GROUP BY
            CASE WHEN cudet.EC_Invoice_Sub_Bucket_Master_Id IS NULL THEN bm.Bucket_Name
                ELSE eisbm.Sub_Bucket_Name
            END
            , cism.SERVICE_MONTH
            , cudet.UNIT_OF_MEASURE_TYPE_ID;



        SELECT
            Sub_Bucket_Name
            , SERVICE_MONTH
            , DETERMINANT_VALUE
            , UNIT_OF_MEASURE_TYPE_ID
            , CU_INVOICE_ID
        INTO
            #tmp_SubBuckets_Determinants
        FROM
            #tmp_SubBuckets_Determinants_Sum_Value;



        INSERT INTO @Invoice_Ids_Service_Month
             (
                 Service_Month
                 , CU_INVOICE_ID
             )
        SELECT
            tsbdmv.SERVICE_MONTH
            , MAX(tsbdmv.CU_INVOICE_ID)
        FROM
            #tmp_SubBuckets_Determinants_Sum_Value AS tsbdmv
        GROUP BY
            tsbdmv.SERVICE_MONTH;

        INSERT INTO @Cu_Invoice_Id_No_Of_Months
        SELECT
            tsbdmv.CU_INVOICE_ID
            , COUNT(DISTINCT cism.SERVICE_MONTH)
        FROM
            #tmp_SubBuckets_Determinants_Sum_Value AS tsbdmv
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.CU_INVOICE_ID = tsbdmv.CU_INVOICE_ID
        GROUP BY
            tsbdmv.CU_INVOICE_ID;




        INSERT INTO @Service_Month_Inv_Cnt
        SELECT
            dvs.SERVICE_MONTH
            , cii.No_Of_Months
        FROM
            dbo.CU_INVOICE_SERVICE_MONTH dvs
            INNER JOIN @Cu_Invoice_Id_No_Of_Months cii
                ON dvs.CU_INVOICE_ID = cii.Cu_Inovice_Id;


        SELECT
            tmp_det.SERVICE_MONTH
            , tmp_det.Sub_Bucket_Name
            , tmp_det.DETERMINANT_VALUE
            , tmp_det.UNIT_OF_MEASURE_TYPE_ID
            , tmp_det.CU_INVOICE_ID
            , Outer_Qry.Unit_To_Be_Converted
            , CASE WHEN tmp_det.UNIT_OF_MEASURE_TYPE_ID = Outer_Qry.Unit_To_Be_Converted THEN 0
                  ELSE 1
              END AS IsConverted
        INTO
            #tmp_SubBuckets_UOM_Id_Conversion
        FROM
            #tmp_SubBuckets_Determinants tmp_det
            INNER JOIN
            --Get Maximum UOM of Sub Bucket and add Column Unit to be converted    
            (   SELECT
                    ROW_NUMBER() OVER (PARTITION BY
                                           Sub_Bucket_Name
                                       ORDER BY
                                           COUNT(UNIT_OF_MEASURE_TYPE_ID) DESC) AS RN
                    , Sub_Bucket_Name
                    , UNIT_OF_MEASURE_TYPE_ID AS Unit_To_Be_Converted
                    , COUNT(UNIT_OF_MEASURE_TYPE_ID) AS Counts
                FROM
                    #tmp_SubBuckets_Determinants
                GROUP BY
                    Sub_Bucket_Name
                    , UNIT_OF_MEASURE_TYPE_ID) Outer_Qry
                ON tmp_det.Sub_Bucket_Name = Outer_Qry.Sub_Bucket_Name
        WHERE
            Outer_Qry.RN = 1;

        UPDATE
            tmp_det
        SET
            tmp_det.DETERMINANT_VALUE = tmp_det.DETERMINANT_VALUE * Outer_Query.CONVERSION_FACTOR
        FROM
            #tmp_SubBuckets_UOM_Id_Conversion tmp_det
            JOIN
            -- Update the converted UOM values    
            (   SELECT
                    tmp.SERVICE_MONTH
                    , tmp.Sub_Bucket_Name
                    , CUC.CONVERSION_FACTOR
                FROM
                    #tmp_SubBuckets_UOM_Id_Conversion tmp
                    JOIN CONSUMPTION_UNIT_CONVERSION CUC
                        ON tmp.UNIT_OF_MEASURE_TYPE_ID = CUC.BASE_UNIT_ID
                           AND  tmp.Unit_To_Be_Converted = CUC.CONVERTED_UNIT_ID
                WHERE
                    IsConverted = 1) Outer_Query
                ON Outer_Query.SERVICE_MONTH = tmp_det.SERVICE_MONTH
                   AND  Outer_Query.Sub_Bucket_Name = tmp_det.Sub_Bucket_Name;

        INSERT INTO #List_Sub_Buckets
             (
                 Sub_Bucket_Name
                 , Service_Months
             )
        SELECT
            Sub_Bucket_Name
            , FIRST_DAY_OF_MONTH_D
        FROM
            #tmp_SubBuckets_UOM_Id_Conversion
            CROSS APPLY meta.DATE_DIM
        WHERE
            FIRST_DAY_OF_MONTH_D BETWEEN @dtFirstDayOfMonth
                                 AND     @dtEnd_Date
        GROUP BY
            Sub_Bucket_Name
            , FIRST_DAY_OF_MONTH_D;


        SELECT
            dm.Service_Months AS Service_Month
            , COALESCE(tmp.Sub_Bucket_Name, dm.Sub_Bucket_Name) AS Sub_Bucket_Name
            , tmp.DETERMINANT_VALUE  AS Determinant_Value
            , e.ENTITY_NAME AS Ubm_Unit_Measure_Code
            , CASE WHEN SUM(tmp.IsConverted) > 0 THEN 1
                  ELSE 0
              END AS IsConverted
            , iism.CU_INVOICE_ID
        FROM
            #List_Sub_Buckets dm
            INNER JOIN @Service_Month_Inv_Cnt smic
                ON dm.Service_Months = smic.service_Month
            INNER JOIN @Invoice_Ids_Service_Month AS iism
                ON iism.Service_Month = dm.Service_Months
            LEFT OUTER JOIN #tmp_SubBuckets_UOM_Id_Conversion tmp
                ON dm.Service_Months = tmp.SERVICE_MONTH
                   AND  tmp.Sub_Bucket_Name = dm.Sub_Bucket_Name
            LEFT OUTER JOIN ENTITY e
                ON tmp.Unit_To_Be_Converted = e.ENTITY_ID
        WHERE
            Service_Months BETWEEN @dtFirstDayOfMonth
                           AND     @dtEnd_Date
        GROUP BY
            dm.Service_Months
            , COALESCE(tmp.Sub_Bucket_Name, dm.Sub_Bucket_Name) --tmp.Sub_Bucket_Name    
            , e.ENTITY_NAME
            , tmp.DETERMINANT_VALUE 
            , iism.CU_INVOICE_ID;


        DROP TABLE #tmp_SubBuckets_Determinants_Sum_Value;
        DROP TABLE #tmp_SubBuckets_Determinants;
        DROP TABLE #tmp_SubBuckets_UOM_Id_Conversion;
        DROP TABLE #List_Sub_Buckets;

    END;






GO
GRANT EXECUTE ON  [Budget].[Get_SubBuckets_For_Accounts] TO [CBMSApplication]
GO
