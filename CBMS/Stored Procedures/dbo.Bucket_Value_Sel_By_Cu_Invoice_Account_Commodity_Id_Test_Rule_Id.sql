SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:      
      dbo.Bucket_Value_Sel_By_Cu_Invoice_Account_Commodity_Id_Test_Rule_Id    
       
 DESCRIPTION:       
      Gets the Test details based on commodity.    
          
 INPUT PARAMETERS:      
 Name					DataType			Default			Description      
----------------------------------------------------------------------------     
 @Commodity_Id			INT       
 @Account_Id			INT    
 @Commodity_Id			INT    
    
 OUTPUT PARAMETERS:    
 Name					DataType			Default			Description      
----------------------------------------------------------------------------     
 USAGE EXAMPLES:      
----------------------------------------------------------------------------     
    
EXEC dbo.Bucket_Value_Sel_By_Cu_Invoice_Account_Commodity_Id_Test_Rule_Id
    @Invoice_Data_Quality_Test_Rule_Id = 11
    , @Cu_Invoice_Id = 14332364
    , @Account_Id = 152169
    , @Commodity_Id = 290
    , @Service_Month = '2019-04-01'
    , @Currency_Unit_Id = NULL
    , @Uom_Id = NULL


EXEC dbo.Bucket_Value_Sel_By_Cu_Invoice_Account_Commodity_Id_Test_Rule_Id
    @Invoice_Data_Quality_Test_Rule_Id = 15
    , @Cu_Invoice_Id = 30412870
    , @Account_Id = 936149
    , @Commodity_Id = 290
    , @Service_Month = '2015-01-01'
    , @Currency_Unit_Id = 3
    , @Uom_Id = NULL

EXEC dbo.Bucket_Value_Sel_By_Cu_Invoice_Account_Commodity_Id_Test_Rule_Id
    @Invoice_Data_Quality_Test_Rule_Id = 20
    , @Cu_Invoice_Id = 75251748
    , @Account_Id = 73349
    , @Commodity_Id = 290
    , @Service_Month = '2019-01-01'
    , @Currency_Unit_Id = NULL
    , @Uom_Id = 10
   
AUTHOR INITIALS:    
Initials		Name    
----------------------------------------------------------------------------     
 NR				Narayana Reddy    
     
    
 MODIFICATIONS       
 Initials	Date			Modification    
----------------------------------------------------------------------------    
 NR        2019-03-28		Created for Data Quality.    
    
******/

CREATE PROCEDURE [dbo].[Bucket_Value_Sel_By_Cu_Invoice_Account_Commodity_Id_Test_Rule_Id]
    (
        @Invoice_Data_Quality_Test_Rule_Id INT
        , @Cu_Invoice_Id INT
        , @Account_Id INT
        , @Commodity_Id INT
        , @Service_Month DATE
        , @Currency_Unit_Id INT = NULL
        , @Uom_Id INT = NULL
        , @Is_Conversion_Required BIT = 1
    )
AS
    BEGIN
        SET NOCOUNT ON;



        DECLARE
            @Start_Dt DATE
            , @End_Dt DATE
            , @Base_Bucket_Master_Id INT
            , @Aggrigation_Type VARCHAR(25)
            , @History_String NVARCHAR(MAX)
            , @Currency_Group_Info_Id INT
            , @Apply_Uom_Conversion INT
            , @Final_Uom_Id INT
            , @Default_Uom_Type_Id INT;



        CREATE TABLE #History_Bucket_Value
             (
                 CU_INVOICE_ID INT NOT NULL
                 , Client_Hier_ID INT
                 , Account_Id INT NOT NULL
                 , Commodity_Id INT
                 , Service_Month DATE
                 , History_Bucket_Value DECIMAL(28, 10)
                 , Bucket_Master_Id INT
                 , Bucket_Name VARCHAR(100)
                 , Bucket_Type VARCHAR(100)
             );



        SELECT
            @Start_Dt = DATEADD(
                            MM
                            , CAST(CAST(Base_Start_cd.Code_Value AS VARCHAR(25))
                                   + CAST(idqtr.Baseline_Starting_Period_Operand AS VARCHAR(25)) AS INT)
                            , @Service_Month)
            , @End_Dt = DATEADD(
                            MM, -1
                            , DATEADD(
                                  MM
                                  , CAST(CAST(Base_End_cd.Code_Value AS VARCHAR(25))
                                         + CAST(idqtr.Baseline_End_Period_Operand AS VARCHAR(25)) AS INT)
                                  , @Service_Month))
            , @Base_Bucket_Master_Id = idqtr.Baseline_Bucket_Master_Id
            , @Apply_Uom_Conversion = dqt.Apply_Uom_Conversion
        FROM
            dbo.Data_Quality_Test dqt
            --INNER JOIN dbo.Data_Quality_Test_Commodity dqtc
            --    ON dqtc.Data_Quality_Test_Id = dqt.Data_Quality_Test_Id
            INNER JOIN dbo.Invoice_Data_Quality_Test_Rule idqtr
                ON idqtr.Data_Quality_Test_Id = dqt.Data_Quality_Test_Id
            LEFT JOIN dbo.Code Current_cd
                ON Current_cd.Code_Id = idqtr.Current_Value_Month_Period_Operator_Cd
            LEFT JOIN dbo.Code Base_Start_cd
                ON Base_Start_cd.Code_Id = idqtr.Baseline_Starting_Period_Operator_Cd
            LEFT JOIN dbo.Code Base_End_cd
                ON Base_End_cd.Code_Id = idqtr.Baseline_End_Period_Operator_Cd
        WHERE
            idqtr.Invoice_Data_Quality_Test_Rule_Id = @Invoice_Data_Quality_Test_Rule_Id;






        SELECT
            @Currency_Group_Info_Id = MAX(ch.Client_Currency_Group_Id)
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
        WHERE
            cha.Account_Id = @Account_Id;


        SELECT
            @Aggrigation_Type = CASE WHEN bm.Bucket_Name = 'Demand' THEN 'MAX'
                                    ELSE 'SUM'
                                END
            , @Default_Uom_Type_Id = bm.Default_Uom_Type_Id
        FROM
            dbo.Bucket_Master bm
        WHERE
            bm.Bucket_Master_Id = @Base_Bucket_Master_Id;


        SET @Apply_Uom_Conversion = CASE WHEN @Is_Conversion_Required = 0
                                              AND   @Apply_Uom_Conversion = 1 THEN 0
                                        ELSE @Apply_Uom_Conversion
                                    END;


        SET @Final_Uom_Id = CASE WHEN @Is_Conversion_Required = 0 THEN @Default_Uom_Type_Id
                                ELSE @Uom_Id
                            END;

        SET @History_String = N'  
  
        INSERT INTO  #History_Bucket_Value  
             (  
                 CU_INVOICE_ID  
				 ,Client_Hier_ID  
                 , Account_Id  
                 , Commodity_Id    
				 , Service_Month                
                 , History_Bucket_Value  
                 , Bucket_Master_Id  
                 , Bucket_Name  
                 , Bucket_Type 
				
				
				
             )  
        SELECT  
  
			   ' + CAST(@Cu_Invoice_Id AS VARCHAR(30))
                              + N'  
			   , cuad.Client_Hier_ID  
			   , cuad.ACCOUNT_ID  
			   ,bm.Commodity_Id  
			   ,cuad.Service_Month  
               ,' + @Aggrigation_Type + N'(  CASE WHEN c.Code_Value = ''Charge'' THEN cuad.Bucket_Value * (CASE WHEN '
                              + CAST(@Apply_Uom_Conversion AS VARCHAR(10))
                              + N'   = 1  THEN cruc.CONVERSION_FACTOR ElSE 1 END )
                   WHEN c.Code_Value = ''Determinant'' THEN cuad.Bucket_Value *  (CASE WHEN '
                              + CAST(@Apply_Uom_Conversion AS VARCHAR(10))
                              + N'   = 1  THEN csuc.CONVERSION_FACTOR ElSE 1 END )
              END ) AS Bucket_Value  
             
			   , bm.Bucket_Master_Id  
               , bm.Bucket_Name  
               , c.Code_Value AS Bucket_Type  
			   
			              
  
	    FROM  
            dbo.Bucket_Master bm  
            INNER JOIN dbo.Cost_Usage_Account_Dtl cuad  
                ON cuad.Bucket_Master_Id = bm.Bucket_Master_Id  
            INNER JOIN dbo.Code c  
                ON c.Code_Id = bm.Bucket_Type_Cd  
		   LEFT OUTER JOIN dbo.CONSUMPTION_UNIT_CONVERSION csuc
                  ON csuc.BASE_UNIT_ID = cuad.UOM_Type_Id
                     AND csuc.CONVERTED_UNIT_ID = ' + CAST(ISNULL(@Uom_Id, -1) AS VARCHAR(30))
                              + N' 
                     AND c.Code_Value = ''Determinant''
            LEFT OUTER JOIN dbo.CURRENCY_UNIT_CONVERSION cruc
                  ON cruc.BASE_UNIT_ID = cuad.CURRENCY_UNIT_ID
                     AND cruc.CONVERSION_DATE = cuad.Service_Month
                     AND cruc.CONVERTED_UNIT_ID =' + CAST(ISNULL(@Currency_Unit_Id, -1) AS VARCHAR(30))
                              + N'  
                     AND cruc.CURRENCY_GROUP_ID = ' + CAST(@Currency_Group_Info_Id AS VARCHAR(30))
                              + N'  
                     AND c.Code_Value = ''Charge''  
        WHERE  
        bm.Commodity_Id = ' + CAST(@Commodity_Id AS VARCHAR(30)) + N'  
            AND cuad.Service_Month BETWEEN ' + N'''' + CAST(@Start_Dt AS VARCHAR(100)) + N''''
                              + N'   
                                   AND    ' + N'''' + CAST(@End_Dt AS VARCHAR(100)) + N''''
                              + N'    
            AND cuad.ACCOUNT_ID = ' + CAST(@Account_Id AS VARCHAR(30))
                              + N'  
            AND cuad.Bucket_Master_Id = ' + CAST(@Base_Bucket_Master_Id AS VARCHAR(30))
                              + N'   
		GROUP BY  
			  cuad.ACCOUNT_ID  
            , bm.Bucket_Master_Id  
            , bm.Bucket_Name  
            , c.Code_Value  
            , cuad.Client_Hier_ID  
            , bm.Bucket_Master_Id  
		    , cuad.Service_Month  
		    , bm.Commodity_Id';


        EXEC (@History_String);



        SELECT
            hbv.Bucket_Type AS Baseline_Bucket_Type
            , hbv.Bucket_Name AS Baseline_Bucket_Name
            , SUM(hbv.History_Bucket_Value) / COUNT(DISTINCT hbv.Service_Month) AS Baseline_Bucket_Value
            , @Start_Dt AS Baseline_Start_Month
            , @End_Dt AS Baseline_End_Month
            , @Final_Uom_Id AS Final_Uom_Id
        FROM
            #History_Bucket_Value hbv
        GROUP BY
            hbv.Bucket_Type
            , hbv.Bucket_Name;




    END;









GO
GRANT EXECUTE ON  [dbo].[Bucket_Value_Sel_By_Cu_Invoice_Account_Commodity_Id_Test_Rule_Id] TO [CBMSApplication]
GO
