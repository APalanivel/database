SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   procedure [dbo].[cbmsUbmInvoice_MarkAsProcessed]
	( @MyAccountId int 
	, @ubm_invoice_id int
	)
AS
BEGIN

	set nocount on

	   update ubm_invoice with (rowlock)
	      set is_processed = 1
		, processed_date = getdate()
	    where ubm_invoice_id = @ubm_invoice_id


	exec cbmsUbmInvoice_GetSimple @MyAccountId, @ubm_invoice_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsUbmInvoice_MarkAsProcessed] TO [CBMSApplication]
GO
