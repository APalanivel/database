SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsContract_GetForDateChange

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@contract_number	varchar(50)	          	
	@change_start_date	datetime  	null      	
	@change_end_date	datetime  	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE procedure [dbo].[cbmsContract_GetForDateChange]
(
	@MyAccountId int
	,@contract_number varchar(50)
	,@change_start_date datetime = null
	,@change_end_date datetime = null
)
as
select 	ed_contract_number
	,contract_start_date
	,contract_end_date

from contract c
where ed_contract_number = @contract_number
GO
GRANT EXECUTE ON  [dbo].[cbmsContract_GetForDateChange] TO [CBMSApplication]
GO
