SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
      
/******             
                         
 NAME: [dbo].[Invoice_Collection_LOA_Merge]                          
                            
 DESCRIPTION:                            
			To insert or update or delete data in Invoice_Collection_LOA_Merge
                            
 INPUT PARAMETERS:            
                           
 Name                            DataType           Default       Description            
---------------------------------------------------------------------------------------------------------------          
@User_Report_Id						INT
@Tvp_Invoice_Collection_LOA							Tvp_Invoice_Collection_LOA		 READONLY                
                            
 OUTPUT PARAMETERS:                 
                            
 Name                            DataType            Default      Description            
---------------------------------------------------------------------------------------------------------------          
                            
 USAGE EXAMPLES:            
---------------------------------------------------------------------------------------------------------------          

--INSERT

BEGIN TRAN

DECLARE @Tvp_Invoice_Collection_LOA Tvp_Invoice_Collection_LOA;  
  
INSERT      INTO @Tvp_Invoice_Collection_LOA([Invoice_Collection_LOA_Id]  ,
[Seq_No]  ,
[LOA_Valid_From_Dt]  ,
[LOA_Valid_End_Dt]  ,
[Cbms_Image_Id] ,
[LOA_Description],
[Operation_Type] )
VALUES
            ( NULL, 1,'2016-01-01','2016-02-01',1,'Desc 1','Insert' )
            ,( NULL, 2,'2016-03-01','2016-04-01',2,'Desc 2','Insert' )
            ,( NULL, 3,'2016-03-01','2016-04-01',3,'Desc 3','Insert' )
             ,( NULL, 3,'2016-03-01','2016-04-01',4,'Desc 4','Insert' );  
            
SELECT * FROM dbo.Invoice_Collection_LOA WHERE  Invoice_Collection_Client_Config_Id = 1
SELECT * FROM dbo.Invoice_Collection_LOA_Image icli INNER JOIN dbo.Invoice_Collection_LOA icl
ON icli.Invoice_Collection_LOA_Id = icl.Invoice_Collection_LOA_Id
WHERE icl.Invoice_Collection_Client_Config_Id=61


EXEC [dbo].[Invoice_Collection_LOA_Merge] 
      @Invoice_Collection_Client_Config_Id = 61
     ,@Tvp_Invoice_Collection_LOA=@Tvp_Invoice_Collection_LOA
     ,@User_Info_Id =49
     
     
     
SELECT * FROM dbo.Invoice_Collection_LOA where  Invoice_Collection_Client_Config_Id = 61  
SELECT * FROM dbo.Invoice_Collection_LOA_Image icli INNER JOIN dbo.Invoice_Collection_LOA icl
ON icli.Invoice_Collection_LOA_Id = icl.Invoice_Collection_LOA_Id
WHERE icl.Invoice_Collection_Client_Config_Id=61
 
ROLLBACK
--

--UPDATE

BEGIN TRAN

DECLARE @Tvp_Invoice_Collection_LOA Tvp_Invoice_Collection_LOA;  
  
INSERT      INTO @Tvp_Invoice_Collection_LOA([Invoice_Collection_LOA_Id]  ,
[Seq_No]  ,
[LOA_Valid_From_Dt]  ,
[LOA_Valid_End_Dt]  ,
[Cbms_Image_Id] ,
[LOA_Description] )
VALUES
            ( 16, 1,'2016-01-01','2016-02-01',1,'Desc 1' )
            ,( 17, 2,'2016-03-01','2016-04-01',2,'Desc 2' );  
            
SELECT * FROM dbo.Invoice_Collection_LOA WHERE  Invoice_Collection_Client_Config_Id = 61
SELECT * FROM dbo.Invoice_Collection_LOA_Image icli INNER JOIN dbo.Invoice_Collection_LOA icl
ON icli.Invoice_Collection_LOA_Id = icl.Invoice_Collection_LOA_Id
WHERE icl.Invoice_Collection_Client_Config_Id=61


EXEC [dbo].[Invoice_Collection_LOA_Merge] 
      @Invoice_Collection_Client_Config_Id = 61
     ,@Tvp_Invoice_Collection_LOA=@Tvp_Invoice_Collection_LOA
     ,@User_Info_Id =49
     
     
     
SELECT * FROM dbo.Invoice_Collection_LOA where  Invoice_Collection_Client_Config_Id = 61  
SELECT * FROM dbo.Invoice_Collection_LOA_Image icli INNER JOIN dbo.Invoice_Collection_LOA icl
ON icli.Invoice_Collection_LOA_Id = icl.Invoice_Collection_LOA_Id
WHERE icl.Invoice_Collection_Client_Config_Id=61
 
ROLLBACK


--Update



DECLARE @Tvp_Invoice_Collection_LOA Tvp_Invoice_Collection_LOA;  

INSERT      INTO @Tvp_Invoice_Collection_LOA([Invoice_Collection_LOA_Id]  ,
[Seq_No]  ,
[LOA_Valid_From_Dt]  ,
[LOA_Valid_End_Dt]  ,
[Cbms_Image_Id] ,
[LOA_Description],
[Operation_Type] )
VALUES
            ( 96, 2,'2016-01-01','2016-02-01',2,'Desc 1','Delete' )
            ,( 96, 2,'2016-01-01','2016-02-01',3,'Desc 1','Delete' )
            ,( NULL, 3,'2016-03-01','2016-04-01',4,'Desc 1','Insert' )
            
             ;  
            
SELECT * FROM dbo.Invoice_Collection_LOA WHERE  Invoice_Collection_Client_Config_Id = 61
SELECT * FROM dbo.Invoice_Collection_LOA_Image icli INNER JOIN dbo.Invoice_Collection_LOA icl
ON icli.Invoice_Collection_LOA_Id = icl.Invoice_Collection_LOA_Id
WHERE icl.Invoice_Collection_Client_Config_Id=61


EXEC   [dbo].[Invoice_Collection_LOA_Merge] 
      @Invoice_Collection_Client_Config_Id = 61
     ,@Tvp_Invoice_Collection_LOA=@Tvp_Invoice_Collection_LOA
     ,@User_Info_Id =49
     
     
     
SELECT * FROM dbo.Invoice_Collection_LOA where  Invoice_Collection_Client_Config_Id = 61  
SELECT * FROM dbo.Invoice_Collection_LOA_Image icli INNER JOIN dbo.Invoice_Collection_LOA icl
ON icli.Invoice_Collection_LOA_Id = icl.Invoice_Collection_LOA_Id
WHERE icl.Invoice_Collection_Client_Config_Id=61

                           
 AUTHOR INITIALS:            
           
 Initials               Name            
---------------------------------------------------------------------------------------------------------------          
 SP                     Sandeep Pigilam              
                             
 MODIFICATIONS:          
           
 Initials               Date             Modification          
---------------------------------------------------------------------------------------------------------------          
 SP                    2016-11-17       Created for Invoice Tracking Phase I.                          
                           
******/      
CREATE PROCEDURE [dbo].[Invoice_Collection_LOA_Merge]
      ( 
       @Invoice_Collection_Client_Config_Id INT
      ,@Tvp_Invoice_Collection_LOA Tvp_Invoice_Collection_LOA READONLY
      ,@User_Info_Id INT )
AS 
BEGIN                            
      SET NOCOUNT ON;                 
                  
      DECLARE @Invoice_Collection_LOA TABLE
            ( 
             [Invoice_Collection_LOA_Id] [int] NULL
            ,[Seq_No] [smallint] NOT NULL
            ,[LOA_Valid_From_Dt] [date] NOT NULL
            ,[LOA_Valid_End_Dt] [date] NULL )  

      DECLARE @Invoice_Collection_LOA_Image TABLE
            ( 
             [Invoice_Collection_LOA_Id] [int] NULL
            ,[Cbms_Image_Id] [int] NOT NULL
            ,[LOA_Description] [nvarchar](MAX) NULL
            ,Operation_Type VARCHAR(10) )
      
      INSERT      INTO @Invoice_Collection_LOA
                  ( 
                   Invoice_Collection_LOA_Id
                  ,Seq_No
                  ,LOA_Valid_From_Dt
                  ,LOA_Valid_End_Dt )
                  SELECT
                        tvp.Invoice_Collection_LOA_Id AS Invoice_Collection_LOA_Id
                       ,tvp.Seq_No AS Seq_No
                       ,tvp.LOA_Valid_From_Dt AS LOA_Valid_From_Dt
                       ,tvp.LOA_Valid_End_Dt AS LOA_Valid_End_Dt
                  FROM
                        @Tvp_Invoice_Collection_LOA tvp
                  GROUP BY
                        tvp.Invoice_Collection_LOA_Id
                       ,tvp.Seq_No
                       ,tvp.LOA_Valid_From_Dt
                       ,tvp.LOA_Valid_End_Dt 

      BEGIN TRY
            BEGIN TRAN

            DELETE
                  ici
            FROM
                  dbo.Invoice_Collection_LOA_Image ici
                  INNER JOIN dbo.Invoice_Collection_LOA icl
                        ON ici.Invoice_Collection_LOA_Id = icl.Invoice_Collection_LOA_Id
            WHERE
                  icl.Invoice_Collection_Client_Config_Id = @Invoice_Collection_Client_Config_Id
                  AND EXISTS ( SELECT
                                    1
                               FROM
                                    @Tvp_Invoice_Collection_LOA tvp
                               WHERE
                                    tvp.Cbms_Image_Id = ici.Cbms_Image_Id
                                    AND tvp.Operation_Type = 'Delete' )
                              
            DELETE
                  cb
            FROM
                  dbo.cbms_image cb
            WHERE
                  EXISTS ( SELECT
                              1
                           FROM
                              @Tvp_Invoice_Collection_LOA tvp
                           WHERE
                              cb.Cbms_Image_Id = tvp.Cbms_Image_Id
                              AND tvp.Operation_Type = 'Delete' )
		   
            MERGE dbo.Invoice_Collection_LOA AS tgt
                  USING 
                        ( SELECT
                              @Invoice_Collection_Client_Config_Id AS Invoice_Collection_Client_Config_Id
                             ,tvp.Invoice_Collection_LOA_Id AS Invoice_Collection_LOA_Id
                             ,tvp.Seq_No AS Seq_No
                             ,tvp.LOA_Valid_From_Dt AS LOA_Valid_From_Dt
                             ,tvp.LOA_Valid_End_Dt AS LOA_Valid_End_Dt
                             ,GETDATE() AS Ts
                             ,@User_Info_Id AS User_Info_Id
                          FROM
                              @Invoice_Collection_LOA tvp ) AS src
                  ON src.Invoice_Collection_Client_Config_Id = tgt.Invoice_Collection_Client_Config_Id
                        AND src.Invoice_Collection_LOA_Id = tgt.Invoice_Collection_LOA_Id
                        AND src.Seq_No = tgt.Seq_No
                  WHEN NOT MATCHED BY TARGET 
                        THEN INSERT
                              ( 
                               Invoice_Collection_Client_Config_Id
                              ,Seq_No
                              ,LOA_Valid_From_Dt
                              ,LOA_Valid_End_Dt
                              ,Created_User_Id
                              ,Created_Ts
                              ,Updated_User_Id
                              ,Last_Change_Ts )
                         VALUES
                              ( 
                               src.Invoice_Collection_Client_Config_Id
                              ,src.Seq_No
                              ,src.LOA_Valid_From_Dt
                              ,src.LOA_Valid_End_Dt
                              ,src.User_Info_Id
                              ,src.TS
                              ,src.User_Info_Id
                              ,src.Ts )
                  WHEN MATCHED AND EXISTS ( SELECT
                                                1
                                            FROM
                                                @Tvp_Invoice_Collection_LOA tvp1
                                            WHERE
                                                tvp1.Invoice_Collection_LOA_Id = tgt.Invoice_Collection_LOA_Id
                                                AND tvp1.Operation_Type = 'Delete' )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          @Tvp_Invoice_Collection_LOA tvp2
                                         WHERE
                                          tvp2.Invoice_Collection_LOA_Id = tgt.Invoice_Collection_LOA_Id
                                          AND ISNULL(tvp2.Operation_Type, '') <> 'Delete' )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Invoice_Collection_LOA_Image tvp3
                WHERE
                                          tvp3.Invoice_Collection_LOA_Id = tgt.Invoice_Collection_LOA_Id )
                        THEN DELETE
                  WHEN MATCHED AND EXISTS ( SELECT
                                                1
                                            FROM
                                                @Tvp_Invoice_Collection_LOA tvp1
                                            WHERE
                                                tvp1.Invoice_Collection_LOA_Id = tgt.Invoice_Collection_LOA_Id
                                                AND tvp1.LOA_Valid_From_Dt <> tgt.LOA_Valid_From_Dt
                                                OR tvp1.LOA_Valid_End_Dt <> tgt.LOA_Valid_End_Dt )
                        THEN UPDATE
                         SET  
                              tgt.LOA_Valid_From_Dt = src.LOA_Valid_From_Dt
                             ,tgt.LOA_Valid_End_Dt = src.LOA_Valid_End_Dt
                             ,tgt.Updated_User_Id = src.User_Info_Id
                             ,tgt.Last_Change_Ts = src.TS;
                         
                        
            INSERT      INTO @Invoice_Collection_LOA_Image
                        ( 
                         Invoice_Collection_LOA_Id
                        ,Cbms_Image_Id
                        ,LOA_Description
                        ,Operation_Type )
                        SELECT
                              icl.Invoice_Collection_LOA_Id AS Invoice_Collection_LOA_Id
                             ,tvp.Cbms_Image_Id AS Cbms_Image_Id
                             ,tvp.LOA_Description AS LOA_Description
                             ,tvp.Operation_Type
                        FROM
                              @Tvp_Invoice_Collection_LOA tvp
                              INNER JOIN Invoice_Collection_LOA icl
                                    ON icl.Invoice_Collection_Client_Config_Id = @Invoice_Collection_Client_Config_Id
                                       AND icl.Seq_No = tvp.Seq_No
                        GROUP BY
                              icl.Invoice_Collection_LOA_Id
                             ,tvp.Cbms_Image_Id
                             ,tvp.LOA_Description
                             ,tvp.Operation_Type
                       
                        
            MERGE dbo.Invoice_Collection_LOA_Image AS tgt
                  USING 
                        ( SELECT
                              tvp.Invoice_Collection_LOA_Id AS Invoice_Collection_LOA_Id
                             ,tvp.Cbms_Image_Id AS Cbms_Image_Id
                             ,tvp.LOA_Description AS LOA_Description
                             ,GETDATE() AS Ts
                             ,@User_Info_Id AS User_Info_Id
                          FROM
                              @Invoice_Collection_LOA_Image tvp
                          WHERE
                              Operation_Type = 'Insert' ) AS src
                  ON src.Invoice_Collection_LOA_Id = tgt.Invoice_Collection_LOA_Id
                        AND src.Cbms_Image_Id = tgt.Cbms_Image_Id
                  WHEN NOT MATCHED BY TARGET 
                        THEN INSERT
                              ( 
                               Invoice_Collection_LOA_Id
                              ,Cbms_Image_Id
                              ,LOA_Description
                              ,Created_User_Id
                              ,Created_Ts
                              ,Updated_User_Id
                              ,Last_Change_Ts )
                         VALUES
                              ( 
                               src.Invoice_Collection_LOA_Id
                              ,src.Cbms_Image_Id
                              ,src.LOA_Description
                              ,src.User_Info_Id
                              ,src.TS
                              ,src.User_Info_Id
                             ,src.Ts );

            COMMIT
      END TRY
  
      BEGIN CATCH
            IF @@TRANCOUNT > 0 
                  ROLLBACK
            EXEC dbo.usp_RethrowError 
                  @CustomMessage = ERROR_MESSAGE
                  
  
      END CATCH
                                                  
END;



;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_LOA_Merge] TO [CBMSApplication]
GO
