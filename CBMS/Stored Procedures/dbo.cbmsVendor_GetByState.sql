SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsVendor_GetByState

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@StateId       	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE PROCEDURE [dbo].[cbmsVendor_GetByState]
(
	@MyAccountId int
	,@StateId int
)
AS
begin
	set nocount on
	select v.vendor_id
		,v.vendor_name
	
	from vendor v
	join vendor_state_map vsm on vsm.vendor_id = v.vendor_id
	where vsm.state_id = @StateId
	order by v.vendor_name
end
GO
GRANT EXECUTE ON  [dbo].[cbmsVendor_GetByState] TO [CBMSApplication]
GO
