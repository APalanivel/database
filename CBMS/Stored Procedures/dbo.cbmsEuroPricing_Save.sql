SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE        PROCEDURE [dbo].[cbmsEuroPricing_Save]
	( @euro_pricing_id int = null
	, @detail_date datetime = null
	, @price_type varchar(40) = null
	, @year int = null
	, @interval int = null
	, @value decimal(32,16) = null
	, @price_point varchar(200)
	)
AS
BEGIN

	set nocount on


	  declare @this_id int

		select @this_id = euro_pricing_id from euro_pricing where detail_date = @detail_date and year = @year and price_type = @price_type and price_point = @price_point and interval = @interval
--	      set @this_id = @euro_pricing_id 


	if @this_id is null
	begin

		insert into euro_pricing
			( 
			detail_date
			, price_type
			, year
			, interval
			, value
			, price_point

			)
		 values
			( @detail_date
			, @price_type
			, @year
			, @interval
			, @value
			, @price_point
			)

		set @this_id = @@IDENTITY
--		select @@IDENTITY as euro_pricing_id

	end
	else
	begin

		   update euro_pricing with (rowlock)
		      set price_point = @price_point
			, detail_date = @detail_date 
			, price_type = @price_type 
			, year = @year 
			, interval = @interval 
			, value = @value 

		    where euro_pricing_id = @this_id

	end

--	set nocount off



END


SET QUOTED_IDENTIFIER ON
GO
GRANT EXECUTE ON  [dbo].[cbmsEuroPricing_Save] TO [CBMSApplication]
GO
