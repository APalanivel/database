SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE [dbo].[cbmsDivision_GetForClient]
	( @MyAccountId int
	, @client_id int
	)
AS
BEGIN


	   select division_id
		, division_name
	     from division with (nolock)
	    where client_id = @client_id
	 order by division_name


END
GO
GRANT EXECUTE ON  [dbo].[cbmsDivision_GetForClient] TO [CBMSApplication]
GO
