SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********           
NAME:  dbo.CU_Invoice_Sel_by_InvoiceID_commodity          
         
DESCRIPTION:      
        
INPUT PARAMETERS:            
Name              DataType          Default     Description            
------------------------------------------------------------            
 @cu_invoice_Id  int    
 @Commodity_Id  INT    NULL       
   
OUTPUT PARAMETERS:            
Name              DataType          Default     Description            
------------------------------------------------------------            
USAGE EXAMPLES:       
    
 EXEC CU_Invoice_Sel_by_InvoiceID_commodity  2928080  
 EXEC CU_Invoice_Sel_by_InvoiceID_commodity 2928214,290  
 EXEC dbo.CU_Invoice_Sel_by_InvoiceID_commodity 35238200,290  
 EXEC CU_Invoice_Sel_by_InvoiceID_commodity  75254431

  EXEC CU_Invoice_Sel_by_InvoiceID_commodity  75254431

  
------------------------------------------------------------          
AUTHOR INITIALS:      
      
Initials Name          
------------------------------------------------------------          
SSR   Sharad Srivastava   
RR   Raghu Reddy   
         
Initials Date  Modification          
------------------------------------------------------------          
SSR   03/23/2010 Created    
SSR   04/20/2010 Added ISNULL condition for Account_number in Select Clause  
SKA   05/27/2010 Modified the CTE Clause  
RR   2017-03-17 Contract Placeholder - Replaced base tables with client hier. For DMO supplier accounts end date is optinal, concatenating  
      end with other fields returning whole account number as NULL, using Display_Account_Number(NULL handled in trigger)  
NR	2019-12-30	MAINT-9684 - Added Account Number , Supplier dates in below stored procedure.
        
******/

CREATE PROCEDURE [dbo].[CU_Invoice_Sel_by_InvoiceID_commodity]
    @cu_invoice_Id INT
    , @Commodity_Id INT = NULL
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            cha.Account_Id
            , cha.Display_Account_Number AS ACCOUNT_NUMBER
            , inv.ACCOUNT_GROUP_ID
            , cha.Account_Type AS ENTITY_NAME
            , com.Commodity_Name
            , cha.Account_Number AS Inv_Account_Number
            , cha.Supplier_Account_begin_Dt
            , cha.Supplier_Account_End_Dt
        FROM
            dbo.CU_INVOICE_SERVICE_MONTH cism
            INNER JOIN Core.Client_Hier_Account cha
                ON cism.Account_ID = cha.Account_Id
            INNER JOIN dbo.CU_INVOICE inv
                ON cism.CU_INVOICE_ID = inv.CU_INVOICE_ID
            INNER JOIN dbo.Commodity com
                ON cha.Commodity_Id = com.Commodity_Id
        WHERE
            cism.CU_INVOICE_ID = @cu_invoice_Id
            AND (   @Commodity_Id IS NULL
                    OR  cha.Commodity_Id = @Commodity_Id)
            AND (   cism.SERVICE_MONTH IS NULL
                    OR  (   cha.Account_Type = 'Utility'
                            OR  (   cha.Account_Type = 'Supplier'
                                    AND (   cism.Begin_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                                          AND     cha.Supplier_Account_End_Dt
                                            OR  cism.End_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                                            AND     cha.Supplier_Account_End_Dt
                                            OR  cha.Supplier_Account_begin_Dt BETWEEN cism.Begin_Dt
                                                                              AND     cism.End_Dt
                                            OR  cha.Supplier_Account_End_Dt BETWEEN cism.Begin_Dt
                                                                            AND     cism.End_Dt))))
        GROUP BY
            cha.Account_Id
            , cha.Display_Account_Number
            , inv.ACCOUNT_GROUP_ID
            , cha.Account_Type
            , com.Commodity_Name
            , cha.Account_Number
            , cha.Supplier_Account_begin_Dt
            , cha.Supplier_Account_End_Dt;


    END;

GO

GRANT EXECUTE ON  [dbo].[CU_Invoice_Sel_by_InvoiceID_commodity] TO [CBMSApplication]
GO
