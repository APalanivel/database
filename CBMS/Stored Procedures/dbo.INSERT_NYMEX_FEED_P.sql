SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.INSERT_NYMEX_FEED_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@closePrice    	decimal(10,3)	          	
	@highPrice     	decimal(10,3)	          	
	@lowPrice      	decimal(10,3)	          	
	@symbol        	varchar(10)	          	
	@description   	varchar(100)	          	
	@batchExecutionDate	datetime  	          	
	@isSuccess     	bit       	          	
	@exchangeShortName	varchar(10)	          	
	@exchangeLongName	varchar(100)	          	
	@symbolType    	varchar(50)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE dbo.INSERT_NYMEX_FEED_P
@userId varchar(10),
@sessionId varchar(20),
@closePrice decimal(10,3),
@highPrice decimal(10,3),
@lowPrice decimal(10,3),
@symbol varchar(10),
@description varchar(100),
@batchExecutionDate datetime,
@isSuccess bit,
@exchangeShortName varchar(10),
@exchangeLongName varchar(100),
@symbolType varchar(50)

as
set nocount on
	insert into RM_NYMEX_DATA 
	(CLOSE_PRICE, HIGH_PRICE, LOW_PRICE, SYMBOL, DESCRIPTION, 
	BATCH_EXECUTION_DATE, IS_SUCCESS, 
	EXCHANGE_SHORTNAME, EXCHANGE_LONGNAME, SYMBOL_TYPE)
	values (
	@closePrice,
	@highPrice,
	@lowPrice,
	@symbol ,
	@description ,
	@batchExecutionDate ,
	@isSuccess ,
	@exchangeShortName,
	@exchangeLongName,
	@symbolType
	)
GO
GRANT EXECUTE ON  [dbo].[INSERT_NYMEX_FEED_P] TO [CBMSApplication]
GO
