SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                              
Name:                              
        dbo.RM_Site_Forecast_Volumes_Sel_For_Deal_Ticket                            
                              
Description:                              
        To get site's hedge configurations        
                              
Input Parameters:                              
    Name    DataType        Default     Description                                
--------------------------------------------------------------------------------        
	@Client_Id   INT        
    @Commodity_Id  INT        
    @Hedge_Type  INT        
    @Start_Dt DATE        
    @End_Dt  DATE        
    @Contract_Id  VARCHAR(MAX)        
    @Site_Id   INT    NULL        
                              
 Output Parameters:                                    
 Name            Datatype        Default  Description                                    
--------------------------------------------------------------------------------        
           
                            
Usage Examples:                                  
--------------------------------------------------------------------------------        
 DECLARE @Trade_tvp_Total_Forecast_Current_Deal_Hedged_ToDate_Volumes AS Trade.tvp_Total_Forecast_Current_Deal_Hedged_ToDate_Volumes        
	INSERT INTO @Trade_tvp_Total_Forecast_Current_Deal_Hedged_ToDate_Volumes      
	VALUES ( '2020-01-01', 56500, 113000,54457 )      
 EXEC dbo.RM_Site_Forecast_Volumes_Sel_For_Deal_Ticket  @Client_Id = 11278,@Commodity_Id = 291        
	 ,@Start_Dt='2020-01-01',@End_Dt='2020-01-01',@Hedge_Type=587,@Site_Str = '285814,364870,648493,421015'      
	 ,@Trade_tvp_Total_Forecast_Current_Deal_Hedged_ToDate_Volumes= @Trade_tvp_Total_Forecast_Current_Deal_Hedged_ToDate_Volumes
	 ,@Deal_Ticket_Frequency_Cd=102744, @Trade_Action_Type_Cd=102746, @Uom_Id=25, @Hedge_Mode_Type_Id= 621, @Index_Id=325  
        
Author Initials:                              
    Initials    Name                              
--------------------------------------------------------------------------------        
    RR          Raghu Reddy           
                               
 Modifications:                              
    Initials	Date        Modification                              
--------------------------------------------------------------------------------        
	RR          02-11-2018  Created GRM  
	RR			2019-10-31	GRM-1550 Modified to exclude already hedged volume in current deal volume calculation
	RR			2020-01-24	GRM-1690 Reverted GRM-1550 fix                       
******/
CREATE PROCEDURE [dbo].[RM_Site_Forecast_Volumes_Sel_For_Deal_Ticket]
    (
        @Client_Id INT
        , @Commodity_Id INT
        , @Hedge_Type INT
        , @Start_Dt DATE
        , @End_Dt DATE
        , @Contract_Id VARCHAR(MAX) = NULL
        , @Uom_Id INT = NULL
        , @Site_Str VARCHAR(MAX) = NULL
        , @Division_Id VARCHAR(MAX) = NULL
        , @RM_Group_Id VARCHAR(MAX) = NULL
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
        , @Trade_tvp_Total_Forecast_Current_Deal_Hedged_ToDate_Volumes AS Trade.tvp_Total_Forecast_Current_Deal_Hedged_ToDate_Volumes READONLY
        , @Hedge_Mode_Type_Id INT = NULL
        , @Index_Id INT = NULL
        , @Deal_Ticket_Frequency_Cd INT = NULL
        , @Trade_Action_Type_Cd INT = NULL
        , @Trade_tvp_Hedge_Contract_Sites AS Trade.tvp_Hedge_Contract_Sites READONLY
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Total_Cnt INT;

        CREATE TABLE #DT_Hedged_Volumes
             (
                 Client_Hier_Id INT
                 , Account_Id INT
                 , Deal_Month DATE
                 , Hedged_Volume DECIMAL(28, 0)
                 , Deal_Ticket_Id INT
                 , Contract_Id INT
             );

        CREATE TABLE #Hedged_Volumes
             (
                 Client_Hier_Id INT
                 , Deal_Month DATE
                 , Hedged_Volume DECIMAL(28, 0)
             );

        CREATE TABLE #Volumes
             (
                 Client_Hier_Id INT
                 , Site_Id INT
                 , Site_name VARCHAR(200)
                 , Service_Month DATE
                 , Forecast_Volume DECIMAL(28, 0)
                 , Uom_Id INT
                 , Hedged_Volume DECIMAL(28, 0)
                 , Max_Hedge_Volume DECIMAL(28, 0)
                 , Max_Hedge_Percentage DECIMAL(5, 2)
                 , Do_Not_Hedge BIT
                 , Site_Not_Managed BIT
                 , Client_Id INT
                 , Sitegroup_Id INT
                 , State_Id INT
             );

        CREATE TABLE #Allotted_Monthly_Volumes
             (
                 Service_Month DATE
                 , Allotted_Hedge_Volume DECIMAL(28, 0)
                 , Total_Hedged_Volume DECIMAL(28, 0)
             );

        CREATE TABLE #Adjust_Lagging_Diff_Volumes
             (
                 Row_Num INT
                 , Service_Month DATE
                 , Forecast_Volume INT
                 , Current_Deal_Volume INT
                 , Diff_Volume INT
                 , Adjust_Volume INT
                 , New_Row_Num INT
             );

        CREATE TABLE #Adjust_Leading_Diff_Volumes
             (
                 Row_Num INT
                 , Service_Month DATE
                 , Forecast_Volume INT
                 , Current_Deal_Volume INT
                 , Diff_Volume INT
                 , Adjust_Volume INT
                 , New_Row_Num INT
             );

        DECLARE @Common_Uom_Id INT;

        DECLARE
            @Hedge_Type_Input VARCHAR(200)
            , @Hedge_Mode_Type VARCHAR(200)
            , @Hedge_Index VARCHAR(200);
        DECLARE
            @Deal_Ticket_Frequency VARCHAR(25)
            , @Trade_Action_Type VARCHAR(25);

        CREATE TABLE #RM_Group_Sites
             (
                 [Site_Id] INT
             );

        SELECT
            @Deal_Ticket_Frequency = Code_Value
        FROM
            dbo.Code
        WHERE
            Code_Id = @Deal_Ticket_Frequency_Cd;

        SELECT
            @Trade_Action_Type = Code_Value
        FROM
            dbo.Code
        WHERE
            Code_Id = @Trade_Action_Type_Cd;

        INSERT INTO #RM_Group_Sites
             (
                 Site_Id
             )
        SELECT
            ch.Site_Id
        FROM
            dbo.Cbms_Sitegroup_Participant rgd
            INNER JOIN dbo.Code grp
                ON grp.Code_Id = rgd.Group_Participant_Type_Cd
            INNER JOIN dbo.CONTRACT c
                ON c.CONTRACT_ID = rgd.Group_Participant_Id
            INNER JOIN Core.Client_Hier_Account chasupp
                ON chasupp.Supplier_Contract_ID = c.CONTRACT_ID
            INNER JOIN Core.Client_Hier_Account chautil
                ON chasupp.Meter_Id = chautil.Meter_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = chasupp.Client_Hier_Id
        WHERE
            (   EXISTS (   SELECT
                                1
                           FROM
                                dbo.ufn_split(@RM_Group_Id, ',') con
                           WHERE
                                rgd.Cbms_Sitegroup_Id = CAST(Segments AS INT))
                AND grp.Code_Value = 'Contract'
                AND chasupp.Account_Type = 'Supplier'
                AND chautil.Account_Type = 'Utility')
        GROUP BY
            ch.Site_Id;

        INSERT INTO #RM_Group_Sites
             (
                 Site_Id
             )
        SELECT
            ch.Site_Id
        FROM
            dbo.Cbms_Sitegroup_Participant rgd
            INNER JOIN dbo.Code grp
                ON grp.Code_Id = rgd.Group_Participant_Type_Cd
            INNER JOIN Core.Client_Hier ch
                ON ch.Site_Id = rgd.Group_Participant_Id
        WHERE
            (   EXISTS (   SELECT
                                1
                           FROM
                                dbo.ufn_split(@RM_Group_Id, ',') con
                           WHERE
                                rgd.Cbms_Sitegroup_Id = CAST(Segments AS INT))
                AND grp.Code_Value = 'Site')
        GROUP BY
            ch.Site_Id;


        INSERT INTO #RM_Group_Sites
             (
                 Site_Id
             )
        SELECT
            CAST(Segments AS INT)
        FROM
            dbo.ufn_split(@Site_Str, ',') con
        GROUP BY
            CAST(Segments AS INT);

        INSERT INTO #RM_Group_Sites
             (
                 Site_Id
             )
        SELECT
            ch.Site_Id
        FROM
            Core.Client_Hier ch
        WHERE
            ch.Site_Id > 0
            AND (EXISTS (   SELECT
                                1
                            FROM
                                dbo.ufn_split(@Division_Id, ',') con
                            WHERE
                                ch.Sitegroup_Id = CAST(Segments AS INT)))
        GROUP BY
            ch.Site_Id;



        SELECT
            @Hedge_Type_Input = ENTITY_NAME
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_ID = @Hedge_Type;

        SELECT
            @Hedge_Mode_Type = ENTITY_NAME
        FROM
            dbo.ENTITY
        WHERE
            (   @Hedge_Mode_Type_Id IS NOT NULL
                AND ENTITY_ID = @Hedge_Mode_Type_Id)
            OR  (   @Hedge_Mode_Type_Id IS NULL
                    AND ENTITY_NAME = 'Index'
                    AND ENTITY_DESCRIPTION = 'HEDGE_MODE_TYPE');


        SELECT
            @Hedge_Index = pridx.ENTITY_NAME
        FROM
            dbo.ENTITY pridx
        WHERE
            pridx.ENTITY_ID = @Index_Id;


        INSERT INTO #DT_Hedged_Volumes
             (
                 Client_Hier_Id
                 , Account_Id
                 , Deal_Month
                 , Hedged_Volume
                 , Deal_Ticket_Id
                 , Contract_Id
             )
        SELECT
            dtv.Client_Hier_Id
            , dtv.Account_Id
            , dtv.Deal_Month
            , CASE WHEN frq.Code_Value = 'Daily' THEN MAX((CASE WHEN trdact.Code_Value = 'Buy' THEN dtv.Total_Volume
                                                               WHEN trdact.Code_Value = 'Sell' THEN -dtv.Total_Volume
                                                           END) * dtcuc.CONVERSION_FACTOR) * dd.DAYS_IN_MONTH_NUM
                  ELSE MAX((CASE WHEN trdact.Code_Value = 'Buy' THEN dtv.Total_Volume
                                WHEN trdact.Code_Value = 'Sell' THEN -dtv.Total_Volume
                            END) * dtcuc.CONVERSION_FACTOR)
              END AS Hedged_Volume
            , dtv.Deal_Ticket_Id
            , dtv.Contract_Id
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtv
                ON dtv.Deal_Ticket_Id = dt.Deal_Ticket_Id
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION dtcuc
                ON dtcuc.BASE_UNIT_ID = dtv.Uom_Type_Id
                   AND  dtcuc.CONVERTED_UNIT_ID = @Uom_Id
            INNER JOIN dbo.PRICE_INDEX pridx
                ON pridx.PRICE_INDEX_ID = dt.Price_Index_Id
            INNER JOIN dbo.ENTITY idx
                ON pridx.INDEX_ID = idx.ENTITY_ID
            INNER JOIN dbo.ENTITY hmt
                ON dt.Hedge_Mode_Type_Id = hmt.ENTITY_ID
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dtv.Client_Hier_Id = dtch.Client_Hier_Id
                   AND  dt.Deal_Ticket_Id = dtch.Deal_Ticket_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                ON chws.Deal_Ticket_Client_Hier_Id = dtch.Deal_Ticket_Client_Hier_Id
                   AND  chws.Trade_Month = dtv.Deal_Month
            INNER JOIN Trade.Deal_Ticket_Client_Hier_TXN_Status trdprc
                ON chws.Deal_Ticket_Client_Hier_Workflow_Status_Id = trdprc.Deal_Ticket_Client_Hier_Workflow_Status_Id
            INNER JOIN Trade.Trade_Price tp
                ON trdprc.Trade_Price_Id = tp.Trade_Price_Id
            INNER JOIN dbo.Code trdact
                ON dt.Trade_Action_Type_Cd = trdact.Code_Id
            INNER JOIN dbo.Code frq
                ON dt.Deal_Ticket_Frequency_Cd = frq.Code_Id
            INNER JOIN meta.DATE_DIM dd
                ON dd.DATE_D = dtv.Deal_Month
        WHERE
            dt.Client_Id = @Client_Id
            AND dt.Commodity_Id = @Commodity_Id
            AND dtv.Deal_Month BETWEEN @Start_Dt
                               AND     @End_Dt
            AND (   (   @Hedge_Mode_Type = 'Index'
                        AND @Hedge_Index = 'Nymex'
                        AND hmt.ENTITY_NAME = 'Index')
                    OR  (   @Hedge_Mode_Type = 'Basis'
                            AND hmt.ENTITY_NAME = 'Basis')
                    OR  (   @Hedge_Mode_Type = 'Index'
                            AND @Hedge_Index <> 'Nymex'
                            AND hmt.ENTITY_NAME = 'Index'))
            AND tp.Trade_Price IS NOT NULL
            AND EXISTS (   SELECT
                                1
                           FROM
                                #RM_Group_Sites rgs
                                INNER JOIN Core.Client_Hier ch
                                    ON ch.Site_Id = rgs.Site_Id
                           WHERE
                                ch.Client_Hier_Id = dtv.Client_Hier_Id)
        GROUP BY
            dtv.Client_Hier_Id
            , dtv.Account_Id
            , dtv.Deal_Month
            , dtv.Deal_Ticket_Id
            , frq.Code_Value
            , dd.DAYS_IN_MONTH_NUM
            , dtv.Contract_Id;

        INSERT INTO #Hedged_Volumes
             (
                 Client_Hier_Id
                 , Deal_Month
                 , Hedged_Volume
             )
        SELECT
            Client_Hier_Id
            , Deal_Month
            , SUM(Hedged_Volume) Hedged_Volume
        FROM
            #DT_Hedged_Volumes
        GROUP BY
            Client_Hier_Id
            , Deal_Month;




        ----------------------To get site own config        
        INSERT INTO #Volumes
             (
                 Client_Hier_Id
                 , Site_Id
                 , Site_name
                 , Service_Month
                 , Forecast_Volume
                 , Uom_Id
                 , Hedged_Volume
                 , Max_Hedge_Volume
                 , Max_Hedge_Percentage
                 , Do_Not_Hedge
                 , Site_Not_Managed
                 , Client_Id
                 , Sitegroup_Id
                 , State_Id
             )
        SELECT
            ch.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
            , dd.DATE_D
            , MAX(chfv.Forecast_Volume * cuc.CONVERSION_FACTOR) AS Forecast_Volume
            , ISNULL(@Uom_Id, chob.RM_Forecast_UOM_Type_Id) AS Uom_Id
            , MAX(ISNULL(dtv.Hedged_Volume, 0)) AS Hedged_Volume
            , MAX((chhc.Max_Hedge_Pct * chfv.Forecast_Volume * cuc.CONVERSION_FACTOR) / 100) AS Max_Hedge_Volume
            , chhc.Max_Hedge_Pct AS Max_Hedge_Percentage
            , CASE WHEN MAX(chfv.Service_Month) IS NULL THEN 1
                  ELSE 0
              END
            , ch.Site_Not_Managed
            , ch.Client_Id
            , ch.Sitegroup_Id
            , ch.State_Id
        FROM
            Core.Client_Hier ch
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON ch.Client_Hier_Id = chob.Client_Hier_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY et
                ON et.ENTITY_ID = chhc.Hedge_Type_Id
            CROSS JOIN meta.DATE_DIM dd
            LEFT JOIN Trade.RM_Client_Hier_Forecast_Volume chfv
                ON chfv.Client_Hier_Id = ch.Client_Hier_Id
                   AND  chob.Commodity_Id = chfv.Commodity_Id
                   AND  dd.DATE_D = chfv.Service_Month
                   AND  chfv.Service_Month BETWEEN chhc.Config_Start_Dt
                                           AND     chhc.Config_End_Dt
                   AND  chfv.Service_Month BETWEEN @Start_Dt
                                           AND     @End_Dt
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = chfv.Uom_Id
                   AND  cuc.CONVERTED_UNIT_ID = ISNULL(@Uom_Id, chob.RM_Forecast_UOM_Type_Id)
            LEFT JOIN #Hedged_Volumes dtv
                ON chfv.Client_Hier_Id = dtv.Client_Hier_Id
                   AND  dd.DATE_D = dtv.Deal_Month
        WHERE
            ch.Site_Id > 0
            AND ch.Client_Id = @Client_Id
            AND chob.Commodity_Id = @Commodity_Id
            AND (   @Start_Dt BETWEEN chhc.Config_Start_Dt
                              AND     chhc.Config_End_Dt
                    OR  @End_Dt BETWEEN chhc.Config_Start_Dt
                                AND     chhc.Config_End_Dt
                    OR  chhc.Config_Start_Dt BETWEEN @Start_Dt
                                             AND     @End_Dt
                    OR  chhc.Config_End_Dt BETWEEN @Start_Dt
                                           AND     @End_Dt)
            AND (   (   @Hedge_Type_Input = 'Physical'
                        AND et.ENTITY_NAME IN ( 'Physical', 'Physical & Financial' ))
                    OR  (   @Hedge_Type_Input = 'Financial'
                            AND et.ENTITY_NAME IN ( 'Financial', 'Physical & Financial' )))
            AND dd.DATE_D BETWEEN @Start_Dt
                          AND     @End_Dt
            AND dd.DATE_D BETWEEN chhc.Config_Start_Dt
                          AND     chhc.Config_End_Dt
            AND EXISTS (SELECT  1 FROM  #RM_Group_Sites rgs WHERE   rgs.Site_Id = ch.Site_Id)
        GROUP BY
            ch.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
            , dd.DATE_D
            , ISNULL(@Uom_Id, chob.RM_Forecast_UOM_Type_Id)
            , chhc.Max_Hedge_Pct
            , ch.Site_Not_Managed
            , ch.Client_Id
            , ch.Sitegroup_Id
            , ch.State_Id;




        ----------------------To get default config                               
        INSERT INTO #Volumes
             (
                 Client_Hier_Id
                 , Site_Id
                 , Site_name
                 , Service_Month
                 , Forecast_Volume
                 , Uom_Id
                 , Hedged_Volume
                 , Max_Hedge_Volume
                 , Max_Hedge_Percentage
                 , Do_Not_Hedge
                 , Site_Not_Managed
                 , Client_Id
                 , Sitegroup_Id
                 , State_Id
             )
        SELECT
            ch.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
            , dd.DATE_D
            , MAX(chfv.Forecast_Volume * cuc.CONVERSION_FACTOR) AS Forecast_Volume
            , ISNULL(@Uom_Id, chob.RM_Forecast_UOM_Type_Id) AS Uom_Id
            , MAX(ISNULL(dtv.Hedged_Volume, 0))
            , MAX((chhc.Max_Hedge_Pct * chfv.Forecast_Volume * cuc.CONVERSION_FACTOR) / 100) AS Max_Hedge_Volume
            , chhc.Max_Hedge_Pct AS Max_Hedge_Percentage
            , CASE WHEN MAX(chfv.Service_Month) IS NULL THEN 1
                  ELSE 0
              END
            , ch.Site_Not_Managed
            , ch.Client_Id
            , ch.Sitegroup_Id
            , ch.State_Id
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier chclient
                ON ch.Client_Id = chclient.Client_Id
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON chclient.Client_Hier_Id = chob.Client_Hier_Id
                   AND  ch.Country_Id = chob.Country_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY et
                ON et.ENTITY_ID = chhc.Hedge_Type_Id
            CROSS JOIN meta.DATE_DIM dd
            LEFT JOIN Trade.RM_Client_Hier_Forecast_Volume chfv
                ON chfv.Client_Hier_Id = ch.Client_Hier_Id
                   AND  chob.Commodity_Id = chfv.Commodity_Id
                   AND  dd.DATE_D = chfv.Service_Month
                   AND  chfv.Service_Month BETWEEN chhc.Config_Start_Dt
                                           AND     chhc.Config_End_Dt
                   AND  chfv.Service_Month BETWEEN @Start_Dt
                                           AND     @End_Dt
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = chfv.Uom_Id
                   AND  cuc.CONVERTED_UNIT_ID = ISNULL(@Uom_Id, chob.RM_Forecast_UOM_Type_Id)
            LEFT JOIN #Hedged_Volumes dtv
                ON chfv.Client_Hier_Id = dtv.Client_Hier_Id
                   AND  dd.DATE_D = dtv.Deal_Month
        WHERE
            ch.Site_Id > 0
            AND ch.Client_Id = @Client_Id
            AND chclient.Sitegroup_Id = 0
            AND chob.Commodity_Id = @Commodity_Id
            AND (   @Start_Dt BETWEEN chhc.Config_Start_Dt
                              AND     chhc.Config_End_Dt
                    OR  @End_Dt BETWEEN chhc.Config_Start_Dt
                                AND     chhc.Config_End_Dt
                    OR  chhc.Config_Start_Dt BETWEEN @Start_Dt
                                             AND     @End_Dt
                    OR  chhc.Config_End_Dt BETWEEN @Start_Dt
                                           AND     @End_Dt)
            AND (   (   @Hedge_Type_Input = 'Physical'
                        AND et.ENTITY_NAME IN ( 'Physical', 'Physical & Financial' ))
                    OR  (   @Hedge_Type_Input = 'Financial'
                            AND et.ENTITY_NAME IN ( 'Financial', 'Physical & Financial' )))
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.RM_Client_Hier_Onboard siteob1
                               WHERE
                                    siteob1.Client_Hier_Id = ch.Client_Hier_Id
                                    AND siteob1.Commodity_Id = chob.Commodity_Id)
            AND dd.DATE_D BETWEEN @Start_Dt
                          AND     @End_Dt
            AND dd.DATE_D BETWEEN chhc.Config_Start_Dt
                          AND     chhc.Config_End_Dt
            AND EXISTS (SELECT  1 FROM  #RM_Group_Sites rgs WHERE   rgs.Site_Id = ch.Site_Id)
        GROUP BY
            ch.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
            , dd.DATE_D
            , ISNULL(@Uom_Id, chob.RM_Forecast_UOM_Type_Id)
            , chhc.Max_Hedge_Pct
            , ch.Site_Not_Managed
            , ch.Client_Id
            , ch.Sitegroup_Id
            , ch.State_Id;


        SELECT
            Client_Hier_Id
            , Site_Id
            , Site_name
            , Client_Id
            , Sitegroup_Id
            , State_Id
            , -1 Account_Id
            , NULL AS Account_Number
            , NULL AS Contract_Vendor
            , Service_Month
            , CASE WHEN @Deal_Ticket_Frequency = 'Monthly' THEN CAST(ISNULL(Forecast_Volume, 0) AS DECIMAL(28, 0))
                  WHEN @Deal_Ticket_Frequency = 'Daily' THEN
                      CAST(ISNULL(Forecast_Volume / dd.DAYS_IN_MONTH_NUM, 0) AS DECIMAL(28, 0))
              END AS Forecast_Volume
            , Uom_Id
            , CASE WHEN @Deal_Ticket_Frequency = 'Monthly' THEN CAST(ISNULL(Hedged_Volume, 0) AS DECIMAL(28, 0))
                  WHEN @Deal_Ticket_Frequency = 'Daily' THEN
                      CAST(ISNULL(Hedged_Volume / dd.DAYS_IN_MONTH_NUM, 0) AS DECIMAL(28, 0))
              END AS Hedged_Volume
            , CAST(ISNULL((Hedged_Volume / NULLIF(Forecast_Volume, 0)) * 100, 0) AS DECIMAL(28, 0)) AS Percent_RM_Forecast_Hedged
            , CASE WHEN @Deal_Ticket_Frequency = 'Monthly' THEN CAST(ISNULL(Max_Hedge_Volume, 0) AS DECIMAL(28, 0))
                  WHEN @Deal_Ticket_Frequency = 'Daily' THEN
                      CAST(ISNULL((Max_Hedge_Volume / dd.DAYS_IN_MONTH_NUM), 0) AS DECIMAL(28, 0))
              END AS Max_Hedge_Volume
            , ISNULL((Max_Hedge_Percentage), 0) AS Max_Hedge_Percentage
            , Do_Not_Hedge
            , Site_Not_Managed
            , DENSE_RANK() OVER (ORDER BY
                                     Site_name) AS Row_num
            , CASE WHEN @Trade_Action_Type = 'Buy' THEN
                       CASE WHEN @Deal_Ticket_Frequency = 'Monthly' THEN
                                CAST(ISNULL(
                                         ((vol.Forecast_Volume) / NULLIF(tvp.Total_Forecast_Volume, 0))
                                         * tvp.Total_Hedge_Volume, 0) AS DECIMAL(28, 0))
                           WHEN @Deal_Ticket_Frequency = 'Daily' THEN
                               CAST(ISNULL(
                                        (((vol.Forecast_Volume) / NULLIF(tvp.Total_Forecast_Volume, 0))
                                         * tvp.Total_Hedge_Volume) / dd.DAYS_IN_MONTH_NUM, 0) AS DECIMAL(28, 0))
                       END
                  WHEN @Trade_Action_Type = 'Sell' THEN
                      CASE WHEN @Deal_Ticket_Frequency = 'Monthly' THEN
                               CAST(ISNULL(
                                        (vol.Hedged_Volume / NULLIF(tvp.Total_Forecast_Volume, 0))
                                        * tvp.Total_Hedge_Volume, 0) AS DECIMAL(28, 0))
                          WHEN @Deal_Ticket_Frequency = 'Daily' THEN
                              CAST(ISNULL(
                                       ((vol.Hedged_Volume / NULLIF(tvp.Total_Forecast_Volume, 0))
                                        * tvp.Total_Hedge_Volume) / dd.DAYS_IN_MONTH_NUM, 0) AS DECIMAL(28, 0))
                      END
              END AS Current_Deal_Volume
            , 0 AS Volume_Adjusted
        INTO
            #Final_Volumes
        FROM
            #Volumes vol
            LEFT JOIN @Trade_tvp_Total_Forecast_Current_Deal_Hedged_ToDate_Volumes tvp
                ON tvp.Deal_Month = vol.Service_Month
            LEFT JOIN meta.DATE_DIM dd
                ON dd.DATE_D = vol.Service_Month;


        SELECT  @Total_Cnt = MAX(fv.Row_num)FROM    #Final_Volumes fv;

        INSERT INTO #Allotted_Monthly_Volumes
             (
                 Service_Month
                 , Allotted_Hedge_Volume
                 , Total_Hedged_Volume
             )
        SELECT
            vol.Service_Month
            , SUM(vol.Current_Deal_Volume) AS Current_Deal_Volume
            , tvp.Total_Hedge_Volume
        FROM
            #Final_Volumes vol
            LEFT JOIN @Trade_tvp_Total_Forecast_Current_Deal_Hedged_ToDate_Volumes tvp
                ON tvp.Deal_Month = vol.Service_Month
        GROUP BY
            vol.Service_Month
            , tvp.Total_Hedge_Volume;


        INSERT INTO #Adjust_Lagging_Diff_Volumes
             (
                 Row_Num
                 , Service_Month
                 , Forecast_Volume
                 , Current_Deal_Volume
                 , Diff_Volume
                 , Adjust_Volume
                 , New_Row_Num
             )
        SELECT
            fv.Row_num
            , fv.Service_Month
            , fv.Forecast_Volume
            , fv.Current_Deal_Volume
            , fv.Forecast_Volume - fv.Hedged_Volume - fv.Current_Deal_Volume
            , mtvol.Total_Hedged_Volume - mtvol.Allotted_Hedge_Volume
            , ROW_NUMBER() OVER (PARTITION BY
                                     fv.Service_Month
                                 ORDER BY
                                     fv.Site_name)
        FROM
            #Final_Volumes fv
            INNER JOIN #Allotted_Monthly_Volumes mtvol
                ON fv.Service_Month = mtvol.Service_Month
        WHERE
            fv.Forecast_Volume - fv.Hedged_Volume - fv.Current_Deal_Volume > 0
            AND mtvol.Total_Hedged_Volume - mtvol.Allotted_Hedge_Volume > 0
            AND fv.Current_Deal_Volume > 0;

        WITH Cte_Lagging
        AS (
               SELECT
                    Forecast_Volume
                    , Current_Deal_Volume
                    , Current_Deal_Volume + CASE WHEN Adjust_Volume >= Diff_Volume THEN Diff_Volume
                                                WHEN Adjust_Volume < Diff_Volume THEN Adjust_Volume
                                                ELSE 0
                                            END AS New_Current_Deal_Volume
                    , Diff_Volume AS New
                    , Row_Num
                    , Adjust_Volume - Diff_Volume AS Rmd
                    , New_Row_Num
                    , Service_Month
               FROM
                    #Adjust_Lagging_Diff_Volumes t
               WHERE
                    New_Row_Num = 1
               UNION ALL
               SELECT
                    t.Forecast_Volume
                    , t.Current_Deal_Volume
                    , t.Current_Deal_Volume + (CASE WHEN t1.Rmd > 0
                                                         AND t1.Rmd >= t.Diff_Volume THEN t.Diff_Volume
                                                   WHEN t1.Rmd > 0
                                                        AND t1.Rmd < t.Diff_Volume THEN t1.Rmd
                                                   ELSE 0
                                               END) AS New_Current_Deal_Volume
                    , (CASE WHEN t1.Rmd > 0
                                 AND t1.Rmd >= t.Diff_Volume THEN t.Diff_Volume
                           WHEN t1.Rmd > 0
                                AND t1.Rmd < t.Diff_Volume THEN t1.Rmd
                           ELSE 0
                       END) AS New
                    , t.Row_Num
                    , t1.Rmd - t.Diff_Volume Rmd
                    , t.New_Row_Num
                    , t.Service_Month
               FROM
                    #Adjust_Lagging_Diff_Volumes t
                    INNER JOIN Cte_Lagging t1
                        ON t.New_Row_Num = t1.New_Row_Num + 1
                           AND  t.Service_Month = t1.Service_Month
               WHERE
                    t1.Rmd > 0
           )
        UPDATE
            fv
        SET
            fv.Current_Deal_Volume = ct.New_Current_Deal_Volume
            , fv.Volume_Adjusted = 1
        FROM
            #Final_Volumes fv
            INNER JOIN Cte_Lagging ct
                ON fv.Row_num = ct.Row_Num
                   AND  fv.Service_Month = ct.Service_Month
        OPTION (MAXRECURSION 32767);

        INSERT INTO #Adjust_Leading_Diff_Volumes
             (
                 Row_Num
                 , Service_Month
                 , Forecast_Volume
                 , Current_Deal_Volume
                 , Diff_Volume
                 , Adjust_Volume
                 , New_Row_Num
             )
        SELECT
            fv.Row_num
            , fv.Service_Month
            , fv.Forecast_Volume
            , fv.Current_Deal_Volume
            , fv.Forecast_Volume - fv.Current_Deal_Volume
            , mtvol.Allotted_Hedge_Volume - mtvol.Total_Hedged_Volume
            , ROW_NUMBER() OVER (PARTITION BY
                                     fv.Service_Month
                                 ORDER BY
                                     fv.Site_name)
        FROM
            #Final_Volumes fv
            INNER JOIN #Allotted_Monthly_Volumes mtvol
                ON fv.Service_Month = mtvol.Service_Month
        WHERE
            mtvol.Allotted_Hedge_Volume - mtvol.Total_Hedged_Volume > 0
            AND fv.Current_Deal_Volume > 0;

        WITH Cte_Leading
        AS (
               SELECT
                    Forecast_Volume
                    , Current_Deal_Volume
                    , Current_Deal_Volume - CAST((CASE WHEN Adjust_Volume >= Diff_Volume
                                                            AND Diff_Volume <= Current_Deal_Volume THEN Diff_Volume
                                                      WHEN Adjust_Volume < Diff_Volume
                                                           AND  Adjust_Volume <= Current_Deal_Volume THEN Adjust_Volume
                                                      ELSE Current_Deal_Volume
                                                  END) AS INT) AS New_Current_Deal_Volume
                    , CASE WHEN Adjust_Volume >= Diff_Volume THEN Diff_Volume
                          WHEN Adjust_Volume < Diff_Volume THEN Adjust_Volume
                          ELSE 0
                      END AS New
                    , Row_Num
                    --,Adjust_Volume - Diff_Volume AS Rmd      
                    , CAST((Adjust_Volume - CASE WHEN Diff_Volume <= Current_Deal_Volume THEN Diff_Volume
                                                ELSE Current_Deal_Volume
                                            END) AS INT) AS Rmd
                    , New_Row_Num
                    , Service_Month
               FROM
                    #Adjust_Leading_Diff_Volumes t
               WHERE
                    New_Row_Num = 1
               UNION ALL
               SELECT
                    t.Forecast_Volume
                    , t.Current_Deal_Volume
                    , t.Current_Deal_Volume - (CASE WHEN t1.Rmd > 0
                                                         AND t1.Rmd <= t.Diff_Volume
                                                         AND t1.Rmd <= t.Current_Deal_Volume THEN t1.Rmd
                                                   WHEN t1.Rmd > 0
                                                        AND t1.Rmd > t.Diff_Volume
                                                        AND t.Diff_Volume <= t.Current_Deal_Volume THEN t.Diff_Volume
                                                   ELSE t.Current_Deal_Volume
                                               END) AS New_Current_Deal_Volume
                    , (CASE WHEN t1.Rmd > 0
                                 AND t1.Rmd <= t.Diff_Volume THEN t1.Rmd
                           WHEN t1.Rmd > 0
                                AND t1.Rmd > t.Diff_Volume THEN t.Diff_Volume
                           ELSE 0
                       END) AS New
                    , t.Row_Num
                    , t1.Rmd - CASE WHEN t.Diff_Volume <= t.Current_Deal_Volume THEN t.Diff_Volume
                                   ELSE t.Current_Deal_Volume
                               END Rmd
                    , t.New_Row_Num
                    , t.Service_Month
               FROM
                    #Adjust_Leading_Diff_Volumes t
                    INNER JOIN Cte_Leading t1
                        ON t.New_Row_Num = t1.New_Row_Num + 1
                           AND  t.Service_Month = t1.Service_Month
               WHERE
                    t1.Rmd > 0
           )
        UPDATE
            fv
        SET
            fv.Current_Deal_Volume = ct.New_Current_Deal_Volume
            , fv.Volume_Adjusted = 1
        FROM
            #Final_Volumes fv
            INNER JOIN Cte_Leading ct
                ON fv.Row_num = ct.Row_Num
                   AND  fv.Service_Month = ct.Service_Month
        OPTION (MAXRECURSION 32767);

        SELECT
            fsd.Client_Hier_Id
            , fsd.Site_Id
            , fsd.Site_name
            , fsd.Account_Id
            , fsd.Account_Number
            , fsd.Contract_Vendor
            , fsd.Service_Month
            , fsd.Current_Deal_Volume
            , fsd.Forecast_Volume
            , fsd.Uom_Id
            , fsd.Hedged_Volume
            , fsd.Percent_RM_Forecast_Hedged
            , fsd.Max_Hedge_Volume
            , fsd.Max_Hedge_Percentage
            , 0 AS Do_Not_Hedge
            , fsd.Site_Not_Managed
            , fsd.Row_num
            , @Total_Cnt AS Total_Cnt
            , fsd.Volume_Adjusted
            , fsd.Client_Id
            , fsd.Sitegroup_Id
            , fsd.State_Id
        FROM
            #Final_Volumes fsd
        WHERE
            fsd.Row_num BETWEEN @Start_Index
                        AND     @End_Index
        ORDER BY
            fsd.Row_num;


        DROP TABLE #RM_Group_Sites;
        DROP TABLE #Allotted_Monthly_Volumes;
        DROP TABLE #Volumes;
        DROP TABLE #Final_Volumes;
        DROP TABLE #Adjust_Leading_Diff_Volumes;
        DROP TABLE #Adjust_Lagging_Diff_Volumes;
        DROP TABLE #Hedged_Volumes;
        DROP TABLE #DT_Hedged_Volumes;

    END;

GO
GRANT EXECUTE ON  [dbo].[RM_Site_Forecast_Volumes_Sel_For_Deal_Ticket] TO [CBMSApplication]
GO
