
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	cbms_prod.dbo.UPDATE_BILLING_DETERMINANT_P

DESCRIPTION:

	This procedure called from below mentioned procedures as well
	UPDATE_DERIVED_BILLING_DETERMINANT_P
	UPDATE_BILLING_DETERMINANT_CMN_P

INPUT PARAMETERS:
Name					DataType		Default	Description
------------------------------------------------------------
@billingDeterminantName VARCHAR(200)
@bdBucketType			INT
@bdUnitType				INT				NULL
@bdFrequencyType		INT				NULL
@bdExpression			VARCHAR(1000)
@billingDeterminantId	INT
@Interval_Manipulation_Logic_Cd	INT	NULL

OUTPUT PARAMETERS:
Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

EXEC UPDATE_BILLING_DETERMINANT_P 'From Date',null,null,null,null,13

AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------
SSR				Sharad Srivastava
CPE				Chaitanya Panduga Eshwar

MODIFICATIONS

Initials	Date		Modification
------------------------------------------------------------
        	7/20/2009	Autogenerated script
SSR			01/28/2010	Replaced bd_bucket_type_id with BD_Bucket_Master_Id
SKA			02/24/2010	Added the usage example
SSR			03/07/2010	Made Default NULL for parameter @bdUnitType,@bdFrequencyType so as this procedure can be called from Other Update Procedures
							UPDATE_DERIVED_BILLING_DETERMINANT_P,UPDATE_BILLING_DETERMINANT_CMN_P
CPE				2012-08-06	Added new optional paramter @Interval_Manipulation_Logic_Cd
******/
CREATE PROCEDURE dbo.UPDATE_BILLING_DETERMINANT_P
( 
 @billingDeterminantName VARCHAR(200)
,@bdBucketType INT
,@bdUnitType INT = NULL
,@bdFrequencyType INT = NULL
,@bdExpression VARCHAR(1000) = NULL
,@billingDeterminantId INT
,@Interval_Manipulation_Logic_Cd INT = NULL )
AS 
BEGIN

      SET NOCOUNT ON

      UPDATE
            dbo.BILLING_DETERMINANT
      SET   
            BILLING_DETERMINANT_NAME = @billingDeterminantName
           ,BD_BUCKET_MASTER_ID = @bdBucketType
           ,BD_UNIT_TYPE_ID = ISNULL(@bdUnitType, BD_UNIT_TYPE_ID)
           ,BD_FREQUENCY_TYPE_ID = ISNULL(@bdFrequencyType, BD_FREQUENCY_TYPE_ID)
           ,BD_EXPRESSION = ISNULL(@bdExpression, BD_EXPRESSION)
           ,Interval_Manipulation_Logic_Cd = ISNULL(@Interval_Manipulation_Logic_Cd, Interval_Manipulation_Logic_Cd)
      WHERE
            BILLING_DETERMINANT_ID = @billingDeterminantId

END

;
GO

GRANT EXECUTE ON  [dbo].[UPDATE_BILLING_DETERMINANT_P] TO [CBMSApplication]
GO
