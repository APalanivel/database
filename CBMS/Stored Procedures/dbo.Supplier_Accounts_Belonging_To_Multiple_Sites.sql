SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: dbo.Supplier_Accounts_Belonging_To_Multiple_Sites
     
DESCRIPTION:

	Returns the supplier accounts of a utility account that belongs to multiple sites.

INPUT PARAMETERS:
NAME				DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
@Account_Id			INT						

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------  

	EXEC dbo.Supplier_Accounts_Belonging_To_Multiple_Sites 119421
	
AUTHOR INITIALS:
INITIALS	NAME          
------------------------------------------------------------          
CPE			Chaitanya Panduga Eswara

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------          
CPE			2014-02-01	Created

*/
CREATE PROC dbo.Supplier_Accounts_Belonging_To_Multiple_Sites
	  (
	   @Account_Id INT
	  )
AS
BEGIN
	  SELECT
			cha2.Account_Id AS Supplier
		   ,cha2.Account_Number
	  FROM
			dbo.METER AS m
			INNER JOIN core.Client_Hier_Account AS cha
				  ON m.METER_ID = cha.Meter_Id
			INNER JOIN core.Client_Hier_Account AS cha2
				  ON cha.Account_Id = cha2.Account_Id
	  WHERE
			cha.Account_Type = 'Supplier'
			AND m.ACCOUNT_ID = @Account_Id
	  GROUP BY
			cha2.Account_Id
		   ,cha2.Account_Number
	  HAVING
			COUNT(DISTINCT cha2.Client_Hier_Id) > 1
END
;
GO
GRANT EXECUTE ON  [dbo].[Supplier_Accounts_Belonging_To_Multiple_Sites] TO [CBMSApplication]
GO
