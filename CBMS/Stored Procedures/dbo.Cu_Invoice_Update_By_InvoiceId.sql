SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
NAME:  
 cbms_prod.dbo.Cu_Invoice_Update_By_InvoiceId 

DESCRIPTION:   

INPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
@MyAccountId		int  
@cu_invoice_id		int = null  
@cbms_image_id		int  
@ubm_id				int = null  
@ubm_invoice_id		int = null  
@ubm_account_code	varchar(200) = null  
@ubm_client_code	varchar(200) = null  
@ubm_city			varchar(200) = null  
@ubm_state_code		varchar(50) = null  
@ubm_vendor_code	varchar(200) = null  
@ubm_account_number varchar(200) = null  
@account_group_id	int = null 
@client_id			int = null  
@vendor_id			int = null  
@begin_date			datetime = null  
@end_date			datetime = null  
@billing_days		int = null  
@ubm_currency_code	varchar(50) = null  
@currency_unit_id	int = null  
@current_charges	decimal(32, 16) = null  
@is_processed		bit = 0  
@is_reported		bit = 0  
@is_dnt				bit = 0  
@do_not_track_id	int = null  
@ubm_invoice_identifier varchar(50) = null  
@ubm_feed_file_name varchar(50) = null  
@is_manual			bit = 0  
@is_duplicate		bit = 0
        
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 USAGE EXAMPLES:  
------------------------------------------------------------  
EXEC Cu_Invoice_Update_By_InvoiceId @MyAccountId = 49,@cbms_image_id = 5678893

 AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 KS   Kailash  MODIFICATIONS   
 Initials Date  Modification  
------------------------------------------------------------  
          
 KS   19/02/2010 Created the stored procedure
 
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.Cu_Invoice_Update_By_InvoiceId
      (
       @MyAccountId INT
      ,@cu_invoice_id INT = NULL
      ,@cbms_image_id INT
      ,@ubm_id INT = NULL
      ,@ubm_invoice_id INT = NULL
      ,@ubm_account_code VARCHAR(200) = NULL
      ,@ubm_client_code VARCHAR(200) = NULL
      ,@ubm_city VARCHAR(200) = NULL
      ,@ubm_state_code VARCHAR(50) = NULL
      ,@ubm_vendor_code VARCHAR(200) = NULL
      ,@ubm_account_number VARCHAR(200) = NULL
      ,@account_group_id INT = NULL
      ,@client_id INT = NULL
      ,@vendor_id INT = NULL
      ,@begin_date DATETIME = NULL
      ,@end_date DATETIME = NULL
      ,@billing_days INT = NULL
      ,@ubm_currency_code VARCHAR(50) = NULL
      ,@currency_unit_id INT = NULL
      ,@current_charges DECIMAL(32, 16) = NULL
      ,@is_processed BIT = 0
      ,@is_reported BIT = 0
      ,@is_dnt BIT = 0
      ,@do_not_track_id INT = NULL
      ,@ubm_invoice_identifier VARCHAR(50) = NULL
      ,@ubm_feed_file_name VARCHAR(50) = NULL
      ,@is_manual BIT = 0
      ,@is_duplicate BIT = 0 )
AS 

BEGIN  
  
      SET NOCOUNT ON  
      BEGIN  
  
            UPDATE
                  cu_invoice
            SET   
                  cbms_image_id = @cbms_image_id
                 ,ubm_id = @ubm_id
                 ,ubm_invoice_id = @ubm_invoice_id
                 ,ubm_account_code = @ubm_account_code
                 ,ubm_client_code = @ubm_client_code
                 ,ubm_city = @ubm_city
                 ,ubm_state_code = @ubm_state_code
                 ,ubm_vendor_code = @ubm_vendor_code
                 ,ubm_account_number = @ubm_account_number
                 ,account_group_id = @account_group_id
                 ,client_id = @client_id
                 ,vendor_id = @vendor_id
                 ,begin_date = @begin_date
                 ,end_date = @end_date
                 ,billing_days = @billing_days
                 ,ubm_currency_code = @ubm_currency_code
                 ,currency_unit_id = @currency_unit_id
                 ,is_processed = @is_processed
                 ,is_reported = @is_reported
                 ,is_dnt = @is_dnt
                 ,do_not_track_id = @do_not_track_id
                 ,updated_by_id = @MyAccountId
                 ,updated_date = GETDATE()
                 ,ubm_invoice_identifier = @ubm_invoice_identifier
                 ,ubm_feed_file_name = @ubm_feed_file_name
                 ,is_manual = @is_manual
                 ,is_duplicate = @is_duplicate
            WHERE
                  cu_invoice_id = @cu_invoice_id

      END
      EXEC cbmsCuInvoice_Get @cu_invoice_id  
END
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Update_By_InvoiceId] TO [CBMSApplication]
GO
