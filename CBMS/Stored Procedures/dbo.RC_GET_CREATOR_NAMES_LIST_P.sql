SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.RC_GET_CREATOR_NAMES_LIST_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(10)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--exec RC_GET_CREATOR_NAMES_LIST_P -1,-1

CREATE  PROCEDURE dbo.RC_GET_CREATOR_NAMES_LIST_P
@userId varchar(10),
@sessionId varchar(10)
as
set nocount on
	select	USER_INFO_ID,
		FIRST_NAME+' '+LAST_NAME USER_INFO_NAME
	
	from	USER_INFO
	
	--where CLIENT_ID IS NULL or access_level <>1 --by Khushwant
	where access_level = 0
	order by FIRST_NAME,LAST_NAME
GO
GRANT EXECUTE ON  [dbo].[RC_GET_CREATOR_NAMES_LIST_P] TO [CBMSApplication]
GO
