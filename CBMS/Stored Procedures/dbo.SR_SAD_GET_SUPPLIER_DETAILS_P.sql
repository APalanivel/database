SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_SUPPLIER_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--select * from vendor


CREATE  PROCEDURE dbo.SR_SAD_GET_SUPPLIER_DETAILS_P
@userId varchar(10),
@sessionId varchar(20)

AS
set nocount on


select VENDOR_ID, VENDOR_NAME from VENDOR
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_SUPPLIER_DETAILS_P] TO [CBMSApplication]
GO
