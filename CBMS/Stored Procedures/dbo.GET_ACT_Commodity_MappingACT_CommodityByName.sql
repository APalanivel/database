SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 [dbo].[GET_ACT_Commodity_MappingACT_CommodityByName]
    
DESCRIPTION:    
    
    
INPUT PARAMETERS:    
 Name				 DataType		Default      Description    
---------------------------------------------------------------------------                  
@ACT_Commodity_Name  VARCHAR(MAX)

OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
 
 



AUTHOR INITIALS:    
 Initials	Name    
------------------------------------------------------------    
   
 NJ		   Naga Jyothi
   
MODIFICATIONS    
    
 Initials	Date		Modification    
--------------------------------------------------------------------------------------    
 NJ		   2019-07-06	 Created for SE2017-733 ACT Commodity Mapping within CBMS
******/
CREATE PROCEDURE [dbo].[GET_ACT_Commodity_MappingACT_CommodityByName]
    (
        @ACT_Commodity_Name VARCHAR(MAX)
    )
AS
    BEGIN

        SET @ACT_Commodity_Name = ISNULL(@ACT_Commodity_Name, '');


        SET NOCOUNT ON;

        SELECT  DISTINCT
                ac.ACT_Commodity_XName
                , ac.ACT_Commodity_Id
        FROM
            dbo.ACT_Commodity ac
        WHERE
            1 = (CASE WHEN @ACT_Commodity_Name = '' THEN 1
                     ELSE CASE WHEN CHARINDEX(@ACT_Commodity_Name, ac.ACT_Commodity_XName) > 0 THEN 1
                              ELSE 0
                          END
                 END)
        ORDER BY
            ac.ACT_Commodity_XName;

    END;

GO
GRANT EXECUTE ON  [dbo].[GET_ACT_Commodity_MappingACT_CommodityByName] TO [CBMSApplication]
GO
