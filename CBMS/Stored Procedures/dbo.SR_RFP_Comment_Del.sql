SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
            
Name:   
	dbo.SR_RFP_Comment_Del       
              
Description:              
			To delete data from dbo.SR_RFP_Comment
              
 Input Parameters:              
    Name					DataType		Default		Description                
---------------------------------------------------------------------------------
	@SR_RFP_Id				INT
    
    
Output Parameters:                    
    Name					DataType		Default		Description                
---------------------------------------------------------------------------------
              
Usage Examples:                  
---------------------------------------------------------------------------------

	BEGIN TRANSACTION
		SELECT * FROM dbo.SR_RFP_Comment a INNER JOIN dbo.Comment b ON a.Comment_Id = b.Comment_ID
			WHERE a.SR_RFP_Id = 5
		EXEC dbo.SR_RFP_Comment_Ins 5, 16, 'Test_SR_RFP_Comment_Del_Test'
		SELECT * FROM dbo.SR_RFP_Comment a INNER JOIN dbo.Comment b ON a.Comment_Id = b.Comment_ID
			WHERE a.SR_RFP_Id = 5
		EXEC dbo.SR_RFP_Comment_Del 5
		SELECT * FROM dbo.SR_RFP_Comment a INNER JOIN dbo.Comment b ON a.Comment_Id = b.Comment_ID
			WHERE a.SR_RFP_Id = 5
	ROLLBACK TRANSACTION
	
Author Initials:              
    Initials	Name              
---------------------------------------------------------------------------------
	RR			Raghu Reddy               
 
Modifications:              
	Initials    Date		Modification              
---------------------------------------------------------------------------------
    RR			2015-10-15	Global Sourcing - Phase2 -Created     
             
******/ 

CREATE PROCEDURE [dbo].[SR_RFP_Comment_Del] ( @SR_RFP_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;
      
      DECLARE @Comment_Id INT
      
      SELECT
            @Comment_Id = Comment_Id
      FROM
            dbo.SR_RFP_Comment
      WHERE
            SR_RFP_Id = @SR_RFP_Id
      
      DELETE FROM
            dbo.SR_RFP_Comment
      WHERE
            Comment_ID = @Comment_Id
            AND SR_RFP_Id = @SR_RFP_Id
                        
      DELETE FROM
            dbo.Comment
      WHERE
            Comment_ID = @Comment_Id
                        
END;
GO
