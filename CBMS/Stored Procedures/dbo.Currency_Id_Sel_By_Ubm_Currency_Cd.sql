SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Currency_Id_Sel_By_Ubm_Currency_Cd]  
     
DESCRIPTION: 

	To Get CURRENCY_UNIT_ID from UBM_CURRENCY_MAP for the given UBM_ID and UBM_CURRENCY_CODE
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Ubm_Id			INT
@Ubm_Currency_Cd		VARCHAR(200)

OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------
  

	EXEC Currency_Id_Sel_By_Ubm_Currency_Cd  1,'US Dollars'
	EXEC Currency_Id_Sel_By_Ubm_Currency_Cd  7,'CAN'
    
    SELECT
		*
	FROM
		dbo.UBM_CURRENCY_MAP
    
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
HG			Harihara Suthan G
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
HG			08/19/2010	Created
*/

CREATE PROCEDURE dbo.Currency_Id_Sel_By_Ubm_Currency_Cd
	@Ubm_Id				INT
	,@Ubm_Currency_Cd	VARCHAR(200)
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT
		CURRENCY_UNIT_ID
	FROM
		dbo.UBM_CURRENCY_MAP
	WHERE
		UBM_ID = @Ubm_Id
		AND UBM_CURRENCY_CODE = @Ubm_Currency_Cd

END
GO
GRANT EXECUTE ON  [dbo].[Currency_Id_Sel_By_Ubm_Currency_Cd] TO [CBMSApplication]
GO
