SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsDivision_Get

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@division_id   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE   PROCEDURE [dbo].[cbmsDivision_Get]
	( @MyAccountId int
	, @division_id int
	)
AS
BEGIN

	   select d.division_id
		, d.division_name
		, d.client_id
	     from division d with (nolock)
	    where d.division_id = @division_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsDivision_Get] TO [CBMSApplication]
GO
