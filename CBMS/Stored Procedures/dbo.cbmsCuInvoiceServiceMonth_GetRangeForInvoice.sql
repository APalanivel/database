SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE [dbo].[cbmsCuInvoiceServiceMonth_GetRangeForInvoice]
	( @MyAccountId int
	, @cu_invoice_id int
	)
AS
BEGIN

	   select cs.cu_invoice_id
		, min(cs.service_month) begin_month
		, max(cs.service_month) end_month
	     from cu_invoice_service_month cs with (nolock)
	    where cs.cu_invoice_id = @cu_invoice_id
	 group by cs.cu_invoice_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCuInvoiceServiceMonth_GetRangeForInvoice] TO [CBMSApplication]
GO
