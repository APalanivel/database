SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.Get_App_Xid

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE dbo.Get_App_Xid    
AS
BEGIN    
    
	SET NOCOUNT ON;     
   
	SELECT App_Config_Value AS XID
	FROM dbo.App_Config (NOLOCK)
	WHERE App_Config_Cd = 'Xid'
		AND Is_Active = 1
   
END
GO
GRANT EXECUTE ON  [dbo].[Get_App_Xid] TO [CBMSApplication]
GO
