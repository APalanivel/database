SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        dbo.GET_USER_EMAIL_ID_P
                      
Description:                      

                      
Input Parameters:                      
    Name			DataType        Default     Description                        
--------------------------------------------------------------------------------
	@userId			VARCHAR(10)
    @sessionId		VARCHAR(20)
    @userInfoId		INT
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	  
                    
Usage Examples:                          
--------------------------------------------------------------------------------
	
		EXEC dbo.GET_USER_EMAIL_ID_P 1,1,49
		exec GET_USER_EMAIL_ID_P null,null,23
                    
 Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy 
	SP          Srinivas patchava  
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			2019-10-02     Global Risk Management - Added name 
    SP          2019-01-30     Added PHONE_NUMBER
                     
******/
CREATE PROCEDURE [dbo].[GET_USER_EMAIL_ID_P]
      ( 
       @userId VARCHAR(10)
      ,@sessionId VARCHAR(20)
      ,@userInfoId INT )
AS 
BEGIN
      SET NOCOUNT ON;
      DECLARE @App_Config_Value VARCHAR(255)
      
      SELECT
            @App_Config_Value = App_Config_Value
      FROM
            dbo.App_Config
      WHERE
            App_Config_Cd = 'RM_Default_Phone_Number'

      SELECT
            ( FIRST_NAME + ' ' + ISNULL(MIDDLE_NAME, '') + ' ' + LAST_NAME )
           ,EMAIL_ADDRESS
           ,FIRST_NAME + ' ' + LAST_NAME AS Name
           ,ISNULL(PHONE_NUMBER, @App_Config_Value) AS PHONE_NUMBER
      FROM
            USER_INFO
      WHERE
            USER_INFO_ID = @userInfoId;

END;



GO
GRANT EXECUTE ON  [dbo].[GET_USER_EMAIL_ID_P] TO [CBMSApplication]
GO
