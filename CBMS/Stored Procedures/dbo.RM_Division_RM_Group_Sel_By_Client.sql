SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
	dbo.RM_Division_RM_Group_Sel_By_Client  

DESCRIPTION:  Procedure used to fetch the division and sitegroups assosciated to the user  

INPUT PARAMETERS:  
	Name				DataType		Default		Description  
------------------------------------------------------------  
	@Security_Role_Id	INT
    @User_Info_Id		INT
    @State_Id			INT				NULL	Not used by the sproc
    @Keyword			VARCHAR(200)	NULL           

OUTPUT PARAMETERS:
	Name				DataType		Default		Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

    EXEC dbo.RM_Division_RM_Group_Sel_By_Client @Client_Id = 235

	EXEC dbo.RM_Division_RM_Group_Sel_By_Client @Client_Id = 11236
	
	EXEC dbo.RM_Division_RM_Group_Sel_By_Client @Client_Id = 1005
	EXEC dbo.RM_Division_RM_Group_Sel_By_Client @Client_Id = 10003



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	NR			Narayana Reddy
	

MODIFICATIONS
	Initials	Date		Modification 
------------------------------------------------------------
	NR			2018-07-25  Created For Risk Managemnet.

******/
CREATE PROCEDURE [dbo].[RM_Division_RM_Group_Sel_By_Client]
    (
        @Client_Id INT
        , @Keyword VARCHAR(200) = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        CREATE TABLE #Client_Groups
             (
                 Group_Id INT
                 , Group_Name VARCHAR(200)
                 , Group_Type VARCHAR(225)
                 , Groups_Order INT
                 , Group_Default_Item INT
             );

        CREATE TABLE #RM_Groups
             (
                 RM_GROUP_ID INT
                 , GROUP_NAME VARCHAR(100)
                 , Country_Name VARCHAR(MAX)
                 , Commodity_Name VARCHAR(100)
                 , RM_Group_Type_Cd INT
                 , RM_Group_Type VARCHAR(100)
                 , Last_Updated_By VARCHAR(100)
                 , Last_Change_Ts DATETIME
                 , Row_Num INT
                 , Total_Count INT
                 , Sites_Cnt INT
             );

        CREATE TABLE #RM_Groups_Filtered
             (
                 RM_GROUP_ID INT
                 , GROUP_NAME VARCHAR(100)
                 , Country_Name VARCHAR(MAX)
                 , Commodity_Name VARCHAR(100)
                 , RM_Group_Type_Cd INT
                 , RM_Group_Type VARCHAR(100)
                 , Last_Updated_By VARCHAR(100)
                 , Last_Change_Ts DATETIME
                 , Row_Num INT
                 , Total_Count INT
                 , Sites_Cnt INT
             );

        INSERT INTO #Client_Groups
             (
                 Group_Id
                 , Group_Name
                 , Group_Type
                 , Groups_Order
                 , Group_Default_Item
             )
        SELECT
            ch.Sitegroup_Id
            , ch.Sitegroup_Name
            , ch.Sitegroup_Type_Name
            , 1 AS Groups_Order
            , 0 AS Group_Default_Item
        FROM
            Core.Client_Hier ch
        WHERE
            ch.Sitegroup_Type_Name IN ( 'Division' )
            AND ch.Client_Id = @Client_Id
            AND ch.Site_Id = 0
            AND ch.Sitegroup_Id > 0
            AND (   @Keyword IS NULL
                    OR  ch.Sitegroup_Name LIKE '%' + @Keyword + '%');

        INSERT INTO #Client_Groups
             (
                 Group_Id
                 , Group_Name
                 , Group_Type
                 , Groups_Order
                 , Group_Default_Item
             )
        SELECT
            -1 AS Group_Id
            , '........All Divisions........' AS Group_Name
            , 'Division' AS Group_Type
            , 1 AS Groups_Order
            , -1 AS Group_Default_Item;



        INSERT INTO #RM_Groups
             (
                 RM_GROUP_ID
                 , GROUP_NAME
                 , Country_Name
                 , Commodity_Name
                 , RM_Group_Type_Cd
                 , RM_Group_Type
                 , Last_Updated_By
                 , Last_Change_Ts
                 , Row_Num
                 , Total_Count
                 , Sites_Cnt
             )
        EXEC dbo.RM_Group_Sel_By_Client_Id @Client_Id = @Client_Id;

        INSERT INTO #RM_Groups_Filtered
             (
                 RM_GROUP_ID
                 , GROUP_NAME
                 , Country_Name
                 , Commodity_Name
                 , RM_Group_Type_Cd
                 , RM_Group_Type
                 , Last_Updated_By
                 , Last_Change_Ts
                 , Row_Num
                 , Total_Count
                 , Sites_Cnt
             )
        SELECT
            RM_GROUP_ID
            , GROUP_NAME
            , Country_Name
            , Commodity_Name
            , RM_Group_Type_Cd
            , RM_Group_Type
            , Last_Updated_By
            , Last_Change_Ts
            , Row_Num
            , Total_Count
            , Sites_Cnt
        FROM
            #RM_Groups rg
        WHERE
            (   @Keyword IS NULL
                OR  rg.GROUP_NAME LIKE '%' + @Keyword + '%');

        INSERT INTO #Client_Groups
             (
                 Group_Id
                 , Group_Name
                 , Group_Type
                 , Groups_Order
                 , Group_Default_Item
             )
        SELECT
            0 AS Group_Id
            , '........RM Groups........' AS Group_Name
            , 'RMGroup' AS Group_Type
            , 2 AS Groups_Order
            , -1 AS Group_Default_Item;

        INSERT INTO #Client_Groups
             (
                 Group_Id
                 , Group_Name
                 , Group_Type
                 , Groups_Order
                 , Group_Default_Item
             )
        SELECT
            RM_GROUP_ID AS Group_Id
            , GROUP_NAME AS Group_Name
            , 'RMGroup' AS Group_Type
            , 2 AS Groups_Order
            , 0 AS Group_Default_Item
        FROM
            #RM_Groups_Filtered rgf;

        SELECT
            Group_Id
            , Group_Name
            , Group_Type
        FROM
            #Client_Groups
        ORDER BY
            Groups_Order
            , Group_Default_Item
            , Group_Name;

        DROP TABLE #Client_Groups;
        DROP TABLE #RM_Groups;
        DROP TABLE #RM_Groups_Filtered;

    END;



GO
GRANT EXECUTE ON  [dbo].[RM_Division_RM_Group_Sel_By_Client] TO [CBMSApplication]
GO
