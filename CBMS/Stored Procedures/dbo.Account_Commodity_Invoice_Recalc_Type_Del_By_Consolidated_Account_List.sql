SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 dbo.Account_Commodity_Invoice_Recalc_Type_Del_By_Consolidated_Account_List   
    
DESCRIPTION:    
 It is used for Consolidated tab , when user copy the contract level recals to account, in that wayprocess  
 If any account have any No recalc then delete and added contract level recalc.   
    
INPUT PARAMETERS:    
 Name					DataType				Default					Description    
----------------------------------------------------------------------------------------    
 @Account_Id			INT     
 @Contract_Id			INT         
 @User_Info_Id			INT  
    
OUTPUT PARAMETERS:    
 Name					DataType				Default					Description    
----------------------------------------------------------------------------------------    
    
USAGE EXAMPLES:    
----------------------------------------------------------------------------------------

SELECT * FROM dbo.Account_Commodity_Invoice_Recalc_Type acirt WHERE acirt.Start_Dt ='2005-01-01'


SELECT
    *
FROM
    dbo.Code c
    INNER JOIN dbo.Codeset cs
        ON cs.Codeset_Id = c.Codeset_Id
WHERE
    cs.Codeset_Name = 'Recalc Type'
    AND cs.Std_Column_Name = 'Recalc_Type_Cd'
  
BEGIN TRAN

EXEC dbo.Account_Commodity_Invoice_Recalc_Type_Del_By_Consolidated_Account_List
    @Contract_Id = 1
    , @Account_Id_List = '1'
    , @Commodity_Id = 290

ROLLBACK TRAN
   
AUTHOR INITIALS:    
 Initials		Name    
----------------------------------------------------------------------------------------    
 NR				Narayana Reddy  
      
MODIFICATIONS    
 Initials   Date			Modification    
----------------------------------------------------------------------------------------    
  NR		2019-10-03		Created For Add Contract.    
  
******/

CREATE PROCEDURE [dbo].[Account_Commodity_Invoice_Recalc_Type_Del_By_Consolidated_Account_List]
    (
        @Contract_Id INT
        , @Account_Id_List NVARCHAR(MAX)
        , @Commodity_Id INT = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;


        DECLARE
            @No_Recalc_Code_Id INT
            , @No_Recalc_Service INT;

        SELECT
            @No_Recalc_Code_Id = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Recalc Type'
            AND cs.Std_Column_Name = 'Recalc_Type_Cd'
            AND c.Code_Value = 'No Recalc';


        SELECT
            @No_Recalc_Service = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Recalc Type'
            AND cs.Std_Column_Name = 'Recalc_Type_Cd'
            AND c.Code_Value = 'No Recalc Service';

        DELETE
        acirt
        FROM
            dbo.Account_Commodity_Invoice_Recalc_Type acirt
            INNER JOIN dbo.ufn_split(@Account_Id_List, ',') us
                ON us.Segments = acirt.Account_Id
        WHERE
            acirt.Invoice_Recalc_Type_Cd IN ( @No_Recalc_Code_Id, @No_Recalc_Service )
            AND (   @Commodity_Id IS NULL
                    OR  acirt.Commodity_ID = @Commodity_Id)
            AND EXISTS (   SELECT
                                1
                           FROM
                                dbo.CONTRACT c
                           WHERE
                                c.CONTRACT_ID = @Contract_Id
                                AND (   acirt.Start_Dt BETWEEN c.CONTRACT_START_DATE
                                                       AND     c.CONTRACT_END_DATE
                                        OR  ISNULL(acirt.End_Dt, '9999-12-31') BETWEEN c.CONTRACT_START_DATE
                                                                               AND     c.CONTRACT_END_DATE
                                        OR  c.CONTRACT_START_DATE BETWEEN acirt.Start_Dt
                                                                  AND     ISNULL(acirt.End_Dt, '9999-12-31')
                                        OR  c.CONTRACT_END_DATE BETWEEN acirt.Start_Dt
                                                                AND     ISNULL(acirt.End_Dt, '9999-12-31')));



    END;
    ;

GO
GRANT EXECUTE ON  [dbo].[Account_Commodity_Invoice_Recalc_Type_Del_By_Consolidated_Account_List] TO [CBMSApplication]
GO
