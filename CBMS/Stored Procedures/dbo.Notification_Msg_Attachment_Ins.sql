SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
NAME:   [dbo].[Notification_Msg_Attachment_Ins]  
             
DESCRIPTION:               
			To insert notification message attachemnts
             
INPUT PARAMETERS:              
Name						DataType		Default		Description    
------------------------------------------------------------------------    
@Notification_Msg_Queue_Id	INT
@Cbms_Image_Id				INT  

OUTPUT PARAMETERS:              
Name							DataType		Default		Description    
------------------------------------------------------------------------    
    
           
USAGE EXAMPLES:              
------------------------------------------------------------------------
	SELECT TOP 5 * FROM dbo.Notification_Msg_Queue

BEGIN TRAN  
	SELECT * FROM dbo.Notification_Msg_Attachment WHERE Notification_Msg_Queue_Id = 1
	EXEC dbo.Notification_Msg_Attachment_Ins 1,1000   
	SELECT * FROM dbo.Notification_Msg_Attachment WHERE Notification_Msg_Queue_Id = 1
ROLLBACK  
  
 AUTHOR INITIALS:              
	Initials	Name              
------------------------------------------------------------------------
	RR			Raghu Reddy  
             
 MODIFICATIONS:               
	Initials	Date		Modification              
------------------------------------------------------------------------  
	RR			2016-06-01  GCS-988 Created

******/

CREATE PROCEDURE [dbo].[Notification_Msg_Attachment_Ins]
      ( 
       @Notification_Msg_Queue_Id INT
      ,@Cbms_Image_Id INT )
AS 
BEGIN  
      SET NOCOUNT ON;
      
      INSERT      INTO dbo.Notification_Msg_Attachment
                  ( 
                   Notification_Msg_Queue_Id
                  ,Cbms_Image_Id
                  ,Created_Ts )
                  SELECT
                        @Notification_Msg_Queue_Id
                       ,@Cbms_Image_Id
                       ,GETDATE()
   
END;
;
GO
GRANT EXECUTE ON  [dbo].[Notification_Msg_Attachment_Ins] TO [CBMSApplication]
GO
