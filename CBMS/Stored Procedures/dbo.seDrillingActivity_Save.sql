SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[seDrillingActivity_Save]
	(
		@DrillingActivityId int = null
	,	@ReportDate datetime
	, @Volume int
	)
As
BEGIN
	set nocount on
	
	declare @ThisId int
	
	set @ThisId = @DrillingActivityId
	
	if @ThisId is null
	begin
		select @ThisId = DrillingActivityId
		  from seDrillingActivity
		 where ReportDate = @ReportDate
	
	end
	if @ThisId is null
	begin
	
		insert into seDrillingActivity
			(ReportDate, Volume)
			values
			(@ReportDate, @Volume)
	
			set @ThisId = @@IDENTITY
	
	end
	else
	begin
	
		update seDrillingActivity
		   set ReportDate = @ReportDate
				 , Volume = @Volume
		 where DrillingActivityId = @ThisId
	
	end
	set nocount off
	
	exec seDrillingActivity_Get @ThisId
END
GO
GRANT EXECUTE ON  [dbo].[seDrillingActivity_Save] TO [CBMSApplication]
GO
