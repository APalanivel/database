SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        Trade.Sites_Mark_To_Cancel_Sel_By_Deal_Ticket                      
                        
Description:                        
        To get site's hedge configurations  
                        
Input Parameters:                        
    Name    DataType        Default     Description                          
--------------------------------------------------------------------------------  
	@Client_Id   INT  
    @Commodity_Id  INT  
    @Hedge_Type  INT  
    @Start_Dt DATE  
    @End_Dt  DATE  
    @Contract_Id  VARCHAR(MAX)  
    @Site_Id   INT    NULL  
                        
 Output Parameters:                              
 Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
     
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
 
	EXEC Trade.Sites_Mark_To_Cancel_Sel_By_Deal_Ticket  291
	EXEC Trade.Sites_Mark_To_Cancel_Sel_By_Deal_Ticket  288
	EXEC Trade.Sites_Mark_To_Cancel_Sel_By_Deal_Ticket  297
	EXEC Trade.Sites_Mark_To_Cancel_Sel_By_Deal_Ticket  131454
  
Author Initials:                        
    Initials    Name                        
--------------------------------------------------------------------------------  
    RR          Raghu Reddy     
                         
 Modifications:                        
    Initials	Date        Modification                        
--------------------------------------------------------------------------------  
	RR          19-02-2019  Created GRM  
                       
******/
CREATE PROCEDURE [Trade].[Sites_Mark_To_Cancel_Sel_By_Deal_Ticket] ( @Deal_Ticket_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;

      SELECT
            ch.Client_Hier_Id
           ,RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
           ,cd.Code_Value AS Trade_Pricing_Option
           ,CASE WHEN cd.Code_Value = 'Weighted Strip Price' THEN NULL
                 WHEN cd.Code_Value = 'Individual Month Pricing' THEN REPLACE(CONVERT(VARCHAR(20), MAX(vol.Deal_Month), 106), ' ', '-')
            END AS Trade_Month
           ,ws.Workflow_Status_Name
           --,dtch.Deal_Ticket_Id AS Trade_Id
           ,chws.Workflow_Status_Comment
           ,CASE WHEN ws.Workflow_Status_Name IN ( 'Marked to Cancel' ) THEN chws.Last_Change_Ts
                 ELSE NULL
            END AS Last_Change_Ts
           ,CASE WHEN ws.Workflow_Status_Name IN ( 'Ready to Trade', 'Order Executed', 'Marked to Cancel' ) THEN 1
                 ELSE 0
            END AS Is_Mark_To_Cancel
           ,chws.CBMS_Image_Id
           ,MAX(trdtxn.Cbms_Trade_Number) AS Trade_Id
      FROM
            Trade.Deal_Ticket dt
            INNER JOIN dbo.Code cd
                  ON cd.Code_Id = dt.Trade_Pricing_Option_Cd
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                  ON dt.Deal_Ticket_Id = dtch.Deal_Ticket_Id
            INNER JOIN Core.Client_Hier ch
                  ON dtch.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                  ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
            INNER JOIN Trade.Workflow_Status_Map wsm
                  ON chws.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
            INNER JOIN Trade.Workflow_Status ws
                  ON wsm.Workflow_Status_Id = ws.Workflow_Status_Id
            INNER JOIN Trade.Workflow_Task_Status_Map tsksts
                  ON tsksts.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
            INNER JOIN Trade.Workflow_Task tsk
                  ON tsksts.Workflow_Task_Id = tsk.Workflow_Task_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl vol
                  ON dt.Deal_Ticket_Id = vol.Deal_Ticket_Id
                     AND dtch.Client_Hier_Id = vol.Client_Hier_Id
            LEFT JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status trdsts
                  ON dtch.Deal_Ticket_Client_Hier_Id = trdsts.Deal_Ticket_Client_Hier_Id
            LEFT JOIN Trade.Deal_Ticket_Client_Hier_TXN_Status trdtxn
                  ON trdsts.Deal_Ticket_Client_Hier_Workflow_Status_Id = trdtxn.Deal_Ticket_Client_Hier_Workflow_Status_Id
      WHERE
            dtch.Deal_Ticket_Id = @Deal_Ticket_Id
            AND chws.Is_Active = 1
            AND ( tsk.Task_Name = 'Mark to Cancel'
                  OR ws.Workflow_Status_Name = 'Marked to Cancel' )
      GROUP BY
            ch.Client_Hier_Id
           ,RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
           ,cd.Code_Value
           ,CASE WHEN cd.Code_Value = 'Weighted Strip Price' THEN NULL
                 WHEN cd.Code_Value = 'Individual Month Pricing' THEN vol.Deal_Month
            END
           ,ws.Workflow_Status_Name
           ,dtch.Deal_Ticket_Id
           ,chws.Workflow_Status_Comment
           ,chws.Last_Change_Ts
           ,CASE WHEN ws.Workflow_Status_Name IN ( 'Ready to Trade', 'Order Executed', 'Marked to Cancel' ) THEN 1
                 ELSE 0
            END
           ,chws.CBMS_Image_Id;

END;

GO
GRANT EXECUTE ON  [Trade].[Sites_Mark_To_Cancel_Sel_By_Deal_Ticket] TO [CBMSApplication]
GO
