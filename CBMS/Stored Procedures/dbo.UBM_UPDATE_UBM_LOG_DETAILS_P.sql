SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UBM_UPDATE_UBM_LOG_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@tableName     	varchar(60)	          	
	@expectedRecords	int       	          	
	@actualRecords 	int       	          	
	@masterLogId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE         PROCEDURE [dbo].[UBM_UPDATE_UBM_LOG_DETAILS_P]
   @userId varchar(10)
 , @sessionId varchar(20)
 , @tableName varchar(60)
 , @expectedRecords int
 , @actualRecords int
 , @masterLogId int
AS
set nocount on
   DECLARE @ubmLogId INT
   SELECT   @ubmLogId = UBM_BATCH_LOG_ID
   FROM     UBM_BATCH_LOG
   WHERE    CASS_TABLE_NAME = @tableName
            AND UBM_BATCH_MASTER_LOG_ID = @masterLogId 
   IF @ubmLogId > 0 
      INSERT   INTO UBM_BATCH_LOG_DETAILS 
               (
                 UBM_BATCH_LOG_ID
               , EXPECTED_DATA_RECORDS
               , ACTUAL_DATA_RECORDS
               )
      VALUES   (
                 @ubmLogId
               , @expectedRecords
               , @actualRecords 
               )
GO
GRANT EXECUTE ON  [dbo].[UBM_UPDATE_UBM_LOG_DETAILS_P] TO [CBMSApplication]
GO
