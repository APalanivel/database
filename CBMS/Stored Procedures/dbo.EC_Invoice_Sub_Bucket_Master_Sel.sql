SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:   dbo.EC_Invoice_Sub_Bucket_Master_Sel           
                  
Description:                  
   This sproc to get the bucket details for a Given id's.          
                               
 Input Parameters:                  
    Name				DataType		Default					Description                    
----------------------------------------------------------------------------------------                    
	@Country_Id			INT				NULL    
    @State_Id			INT				NULL    
    @Commodity_Id		INT				NULL       
    @Start_Index		INT				1                            
    @End_Index			INT				2147483647      
    @Total_Count		INT				0                           
     
 Output Parameters:                        
    Name				DataType		Default					Description                    
----------------------------------------------------------------------------------------                    
                  
 Usage Examples:                      
----------------------------------------------------------------------------------------       
    
   EXEC dbo.EC_Invoice_Sub_Bucket_Master_Sel     
   Exec dbo.EC_Invoice_Sub_Bucket_Master_Sel @Country_Id=98    
   Exec dbo.EC_Invoice_Sub_Bucket_Master_Sel @Country_Id=98,@State_Id=236    
   Exec dbo.EC_Invoice_Sub_Bucket_Master_Sel @Country_Id=98,@State_Id=236,@Commodity_Id=290    
   Exec dbo.EC_Invoice_Sub_Bucket_Master_Sel @Start_Index=1,@End_Index=5    
    
     
Author Initials:                  
    Initials	Name                  
----------------------------------------------------------------------------------------                    
	NR			Narayana Reddy                   
 Modifications:                  
    Initials        Date		Modification                  
----------------------------------------------------------------------------------------                    
    NR				2015-04-22  Created For AS400.             
                 
******/ 
    
CREATE PROCEDURE [dbo].[EC_Invoice_Sub_Bucket_Master_Sel]
      ( 
       @Country_Id INT = NULL
      ,@State_Id INT = NULL
      ,@Commodity_Id INT = NULL
      ,@Start_Index INT = 1
      ,@End_Index INT = 2147483647
      ,@Total_Count INT = 0 )
AS 
BEGIN      
      SET NOCOUNT ON      
            
                                   
      IF @Total_Count = 0 
            BEGIN                           
                  SELECT
                        @Total_Count = COUNT(1) OVER ( )
                  FROM
                        dbo.EC_Invoice_Sub_Bucket_Master eisbm
                        INNER JOIN dbo.Bucket_Master bm
                              ON eisbm.Bucket_Master_Id = bm.Bucket_Master_Id
                        INNER JOIN dbo.Commodity c
                              ON bm.Commodity_Id = c.Commodity_Id
                        INNER JOIN dbo.STATE s
                              ON s.STATE_ID = eisbm.State_Id
                        INNER JOIN dbo.COUNTRY cr
                              ON cr.COUNTRY_ID = s.COUNTRY_ID
                  WHERE
                        ( @Country_Id IS NULL
                          OR cr.COUNTRY_ID = @Country_Id )
                        AND ( @State_Id IS NULL
                              OR s.STATE_ID = @State_Id )
                        AND ( @Commodity_Id IS NULL
                              OR c.Commodity_Id = @Commodity_Id )    
                        
            END;      
                        
      WITH  Cte_Sub_Bucket
              AS ( SELECT TOP ( @End_Index )
                        eisbm.EC_Invoice_Sub_Bucket_Master_Id
                       ,bm.Bucket_Master_Id
                       ,bm.Bucket_Name
                       ,eisbm.Sub_Bucket_Name
                       ,cr.COUNTRY_NAME
                       ,s.STATE_ID
                       ,s.STATE_NAME
                       ,c.Commodity_Name
                       ,ROW_NUMBER() OVER ( ORDER BY cr.COUNTRY_NAME, s.STATE_NAME, c.Commodity_Name, bm.Bucket_Name, eisbm.Sub_Bucket_Name ) AS Row_Num
                   FROM
                        dbo.EC_Invoice_Sub_Bucket_Master eisbm
                        INNER JOIN dbo.Bucket_Master bm
                              ON eisbm.Bucket_Master_Id = bm.Bucket_Master_Id
                        INNER JOIN dbo.Commodity c
                              ON bm.Commodity_Id = c.Commodity_Id
                        INNER JOIN dbo.STATE s
                              ON s.STATE_ID = eisbm.State_Id
                        INNER JOIN dbo.COUNTRY cr
                              ON cr.COUNTRY_ID = s.COUNTRY_ID
                   WHERE
                        ( @Country_Id IS NULL
                          OR cr.COUNTRY_ID = @Country_Id )
                        AND ( @State_Id IS NULL
                              OR s.STATE_ID = @State_Id )
                        AND ( @Commodity_Id IS NULL
                              OR c.Commodity_Id = @Commodity_Id ))
            SELECT
                  csb.EC_Invoice_Sub_Bucket_Master_Id
                 ,csb.COUNTRY_NAME
                 ,csb.STATE_ID
                 ,csb.STATE_NAME
                 ,csb.Commodity_Name
                 ,csb.Bucket_Master_Id
                 ,csb.Bucket_Name
                 ,csb.Sub_Bucket_Name
                 ,@Total_Count AS Total_Rows
                 ,csb.Row_Num
            FROM
                  Cte_Sub_Bucket csb
            WHERE
                  csb.Row_Num BETWEEN @Start_Index AND @End_Index
            ORDER BY
                  Row_Num
                     
END      



;
GO
GRANT EXECUTE ON  [dbo].[EC_Invoice_Sub_Bucket_Master_Sel] TO [CBMSApplication]
GO
