SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:                  
        dbo.Account_Invoice_Processing_Config_Mapping_To_Watch_List                    
                  
Description:                  
			Watch List - Ability to timeband watch list settings.                  
                  
 Input Parameters:                  
 Name                                        DataType        Default        Description                    
--------------------------------------------------------------------------------------------------                   
@Account_Invoice_Processing_Config_Id		VARCHAR(MAX)
@Watch_List_Group_Info_Id					INT
                  
 Output Parameters:                        
 Name                                        DataType        Default        Description                    
--------------------------------------------------------------------------------------------------                   
                  
 Usage Examples:                      
--------------------------------------------------------------------------------------------------


EXEC dbo.CODE_SEL_BY_CodeSet_Name @CodeSet_Name = 'ProcessingInstruction'  ---> Processing_Instruction_Category_Cd


BEGIN TRAN

SELECT  * FROM  [dbo].[Account_Invoice_Processing_Config]
SELECT  * FROM  [dbo].[Account_Invoice_Processing_Config_Log]


EXEC dbo.Account_Invoice_Processing_Config_Mapping_To_Watch_List
    @Account_Invoice_Processing_Config_Ids = '3'   
    , @Watch_List_Group_Info_Id = 7
	, @User_Info_Id = 16

SELECT  * FROM  [dbo].[Account_Invoice_Processing_Config]
SELECT  * FROM  [dbo].[Account_Invoice_Processing_Config_Log]

ROLLBACK TRAN
                 
 Author Initials:                  
  Initials        Name                  
--------------------------------------------------------------------------------------------------                   
  NR              Narayana Reddy    
                   
 Modifications:                  
  Initials        Date              Modification                  
--------------------------------------------------------------------------------------------------                   
  NR              2019-01-03		Created for Data2.0 - Watch List - B.                
                 
******/

CREATE PROCEDURE [dbo].[Account_Invoice_Processing_Config_Mapping_To_Watch_List]
    (
        @Account_Invoice_Processing_Config_Ids VARCHAR(MAX)
        , @Watch_List_Group_Info_Id INT
        , @User_Info_Id INT
        , @Is_Updated_Using_Apply_All BIT = 0
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Group_Name VARCHAR(200);

        SELECT
            @Group_Name = gi.GROUP_NAME
        FROM
            dbo.GROUP_INFO gi
        WHERE
            gi.GROUP_INFO_ID = @Watch_List_Group_Info_Id;

        BEGIN TRY
            BEGIN TRAN;

            UPDATE
                aipc
            SET
                aipc.Watch_List_Group_Info_Id = @Watch_List_Group_Info_Id
                , aipc.Updated_User_Id = @User_Info_Id
                , aipc.Last_Change_Ts = GETDATE()
            FROM
                dbo.Account_Invoice_Processing_Config aipc
                INNER JOIN dbo.ufn_split(@Account_Invoice_Processing_Config_Ids, ',') us
                    ON us.Segments = aipc.Account_Invoice_Processing_Config_Id;


            INSERT INTO dbo.Account_Invoice_Processing_Config_Log
                 (
                     Account_Invoice_Processing_Config_Id
                     , Field_Name
                     , Change_Type
                     , Previous_Value
                     , Current_Value
                     , Is_Updated_Using_Apply_All
                     , Event_By_User_Id
                     , Event_Ts
                 )
            SELECT
                us.Segments
                , 'Watch List Group'
                , 'Created'
                , NULL
                , @Group_Name
                , @Is_Updated_Using_Apply_All
                , @User_Info_Id
                , GETDATE()
            FROM
                dbo.Account_Invoice_Processing_Config aipc
                INNER JOIN dbo.ufn_split(@Account_Invoice_Processing_Config_Ids, ',') us
                    ON us.Segments = aipc.Account_Invoice_Processing_Config_Id
            WHERE
                NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Account_Invoice_Processing_Config_Log aclog
                               WHERE
                                    aclog.Account_Invoice_Processing_Config_Id = us.Segments
                                    AND aclog.Field_Name = 'Watch List Group'
                                    AND aclog.Change_Type = 'Created');

            COMMIT TRAN;
        END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0
                ROLLBACK TRAN;

            EXEC dbo.usp_RethrowError;

        END CATCH;


    END;


GO
GRANT EXECUTE ON  [dbo].[Account_Invoice_Processing_Config_Mapping_To_Watch_List] TO [CBMSApplication]
GO
