SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:    [Workflow].[Get_Quick_Access_Module]  
DESCRIPTION: This is the master stored procedure created to display quick access link for all module based on invoice permissions  
------------------------------------------------------------     
 INPUT PARAMETERS:      
 Name   DataType  Default Description      
 @User_id int,  
 @Module_Id int  
------------------------------------------------------------      
 OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
 USAGE EXAMPLES:    EXEC [Workflow].[Get_Quick_Access_Module] @User_id=49,@Module_Id=1  
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
AKP   Arunkumar Summit Energy   
   
 MODIFICATIONS       
 Initials Date   Modification      
------------------------------------------------------------      
 AKP    Sep 4,2019  Created  
  
******/  
  
-- =============================================   
CREATE PROCEDURE [Workflow].[Get_Quick_Access_Module]       
  -- Add the parameters for the stored procedure here       
   @User_id int,      
   @Module_Id int      
          
      
AS       
  BEGIN -- SET NOCOUNT ON added to prevent extra result sets from       
      -- interfering with SELECT statements.       
      
      
   SET NOCOUNT ON       
      
      DECLARE @Proc_name     VARCHAR(100) = 'Get_Quick_Access_Module',       
              @Input_Params  VARCHAR (1000),       
              @Error_Line    INT,       
              @Error_Message VARCHAR(3000),      
     @sql nvarchar(max)       
      
     BEGIN TRY      
      
    SELECT Navigation_Link      
      ,Navigation_Name  FROM  WORKFLOW.Workflow_Queue_Navigation_Link       
    where Workflow_Queue_Id = @Module_Id      
      
          
        
        
       END try       
      
      BEGIN catch       
      
      
   -- Entry made to the logging SP to capture the errors.      
          SELECT @ERROR_LINE = Error_line(),       
                 @ERROR_MESSAGE = Error_message()       
      
          INSERT INTO storedproc_error_log       
                      (storedproc_name,       
                       error_line,       
                       error_message,       
                       input_params)       
          VALUES      ( @PROC_NAME,       
                        @ERROR_LINE,       
                        @ERROR_MESSAGE,       
                        @INPUT_PARAMS )       
      
      return -99      
      END catch       
  END       
    
GO
GRANT EXECUTE ON  [Workflow].[Get_Quick_Access_Module] TO [CBMSApplication]
GO
