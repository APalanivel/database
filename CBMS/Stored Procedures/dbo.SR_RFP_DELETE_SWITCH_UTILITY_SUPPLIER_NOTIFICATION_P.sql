SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- EXEC SR_RFP_DELETE_SWITCH_UTILITY_SUPPLIER_NOTIFICATION_P 18282, 0, 1658
CREATE PROCEDURE [dbo].[SR_RFP_DELETE_SWITCH_UTILITY_SUPPLIER_NOTIFICATION_P]
	@srRFPAccountId INT,
	@isBidGroup INT,
	@rfpId INT
AS
BEGIN

	SET NOCOUNT ON

	--DECLARE @cbmsImageId INT

	--SELECT @cbmsImageId = change_notice_image_id FROM dbo.Sr_Rfp_Utility_Switch (NOLOCK)
	--WHERE sr_account_group_id = @srRFPAccountId
	--	AND is_bid_group = @isBidGroup

	UPDATE dbo.SR_RFP_UTILITY_SWITCH
		SET SUPPLIER_NOTICE_IMAGE_ID = NULL
	WHERE SR_ACCOUNT_GROUP_ID = @srRFPAccountId
		AND IS_BID_GROUP = @isBidGroup
	  
	--declare @entityId int
	--select @entityId = (
	--SELECT ENTITY_ID FROM ENTITY where entity_name='Supplier Notification' and entity_type=100)

	--DELETE FROM dbo.Cbms_Image
	--WHERE cbms_image_id = @cbmsImageId
  
	IF (@isBidGroup = 0 )
	 BEGIN
	 
		UPDATE dbo.SR_RFP_CHECKLIST
			SET IS_NOTICE_GIVEN = NULL
		WHERE SR_RFP_ACCOUNT_ID =  @srRFPAccountId
		
	 END  
	ELSE IF (@isBidGroup > 0 )  
	 BEGIN
	 
		UPDATE rfpCheckList
			SET rfpCheckList.IS_NOTICE_GIVEN = NULL
		FROM dbo.SR_RFP_CHECKLIST rfpCheckList
			INNER JOIN dbo.SR_RFP_ACCOUNT rfpAcct ON rfpAcct.Sr_Rfp_Account_ID = rfpCheckList.Sr_Rfp_Account_ID
		WHERE rfpAcct.SR_RFP_BID_GROUP_ID = @srRFPAccountId

	 END
END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_DELETE_SWITCH_UTILITY_SUPPLIER_NOTIFICATION_P] TO [CBMSApplication]
GO
