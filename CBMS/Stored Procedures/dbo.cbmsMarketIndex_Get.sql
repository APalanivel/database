SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[cbmsMarketIndex_Get]
	( @MyAccountId int
	, @market_index_id int
	)
AS
BEGIN
/*
Stored in ENTITY TABLE AS

ENTITY_TYPE = 165	
ENTITY_DESCRIPTION = 'Deal Type'
*/
	set nocount on
	   select entity_id market_index_id
		, entity_name market_index
	     from entity
	    where entity_id = @market_index_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsMarketIndex_Get] TO [CBMSApplication]
GO
