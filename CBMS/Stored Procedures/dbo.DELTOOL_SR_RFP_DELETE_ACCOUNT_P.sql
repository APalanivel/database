SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.DELTOOL_SR_RFP_DELETE_ACCOUNT_P


DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
 @account_id                           int,
 
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    
@error_out              int 

USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter
		HG		Harihara Suthan G
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	DR			08/04/2009	Removed Linked Server Updates
	HG			09/27/2010	Commented codes removed and formatted the code.
******/
CREATE PROCEDURE dbo.DELTOOL_SR_RFP_DELETE_ACCOUNT_P
    @account_id INT,
    @error_out INT OUTPUT
AS 
BEGIN

    SET nocount ON
    SET xact_abort ON


    DECLARE @lpAccExist INT
    DECLARE @error INT

    SELECT  @lpAccExist = COUNT(*)
    FROM    dbo.SR_RFP_LOAD_PROFILE_SETUP
    WHERE   SR_RFP_ACCOUNT_ID IN ( SELECT   sr_rfp_account_id
                                   FROM     dbo.sr_rfp_account
                                   WHERE    account_id = @account_id )


    DELETE  FROM dbo.sr_rfp_checklist
    WHERE   sr_rfp_account_id IN ( SELECT   sr_rfp_account_id
                                   FROM     sr_rfp_account
                                   WHERE    account_id = @account_id )
    SET @error = @@error


    IF ( @error = 0 ) 
        BEGIN
            DELETE  FROM SR_RFP_ARCHIVE_METER_CONTRACTS
            WHERE   sr_rfp_account_meter_map_id IN (
                    SELECT  sr_rfp_account_meter_map_id
                    FROM    sr_rfp_account_meter_map
                    WHERE   sr_rfp_account_id IN (
                            SELECT  sr_rfp_account_id
                            FROM    sr_rfp_account
                            WHERE   account_id = @account_id ) )
            SET @error = @@error
        END

    IF ( @error = 0 ) 
        BEGIN
            DELETE  FROM SR_RFP_ARCHIVE_METER
            WHERE   sr_rfp_account_meter_map_id IN (
                    SELECT  sr_rfp_account_meter_map_id
                    FROM    sr_rfp_account_meter_map
                    WHERE   sr_rfp_account_id IN (
                            SELECT  sr_rfp_account_id
                            FROM    sr_rfp_account
                            WHERE   account_id = @account_id ) )
            SET @error = @@error
        END

    IF ( @error = 0 ) 
        BEGIN
            DELETE  FROM sr_rfp_account_meter_map
            WHERE   sr_rfp_account_id IN ( SELECT   sr_rfp_account_id
                                           FROM     sr_rfp_account
                                           WHERE    account_id = @account_id )
            SET @error = @@error
        END

    IF ( @lpAccExist > 0 ) 
        BEGIN

            IF ( @error = 0 ) 
                BEGIN
                    DELETE  FROM SR_RFP_LP_DETERMINANT_VALUES
                    WHERE   SR_RFP_LOAD_PROFILE_DETERMINANT_ID IN (
                            SELECT  SR_RFP_LOAD_PROFILE_DETERMINANT_ID
                            FROM    SR_RFP_LOAD_PROFILE_DETERMINANT
                            WHERE   SR_RFP_LOAD_PROFILE_SETUP_ID IN (
                                    SELECT  SR_RFP_LOAD_PROFILE_SETUP_ID
                                    FROM    SR_RFP_LOAD_PROFILE_SETUP
                                    WHERE   SR_RFP_ACCOUNT_ID IN (
                                            SELECT  sr_rfp_account_id
                                            FROM    sr_rfp_account
                                            WHERE   account_id = @account_id ) ) )
                    SET @error = @@error
                END
	
            IF ( @error = 0 ) 
                BEGIN	
                    DELETE  FROM SR_RFP_LOAD_PROFILE_DETERMINANT
                    WHERE   SR_RFP_LOAD_PROFILE_SETUP_ID IN (
                            SELECT  SR_RFP_LOAD_PROFILE_SETUP_ID
                            FROM    SR_RFP_LOAD_PROFILE_SETUP
                            WHERE   SR_RFP_ACCOUNT_ID IN (
                                    SELECT  sr_rfp_account_id
                                    FROM    sr_rfp_account
                                    WHERE   account_id = @account_id ) )
                    SET @error = @@error
                END

            IF ( @error = 0 ) 
                BEGIN
                    DELETE  FROM SR_RFP_LOAD_PROFILE_SETUP
                    WHERE   SR_RFP_ACCOUNT_ID IN (
                            SELECT  sr_rfp_account_id
                            FROM    sr_rfp_account
                            WHERE   account_id = @account_id )
                    SET @error = @@error
                END

            IF ( @error = 0 ) 
                BEGIN
                    DELETE  FROM SR_RFP_LP_ACCOUNT_ADDITIONAL_ROW
                    WHERE   SR_RFP_ACCOUNT_ID IN (
                            SELECT  sr_rfp_account_id
                            FROM    sr_rfp_account
                            WHERE   account_id = @account_id )
                    SET @error = @@error
                END

            IF ( @error = 0 ) 
                BEGIN	
                    DELETE  FROM SR_RFP_LP_SEND_CLIENT
                    WHERE   SR_ACCOUNT_GROUP_ID IN (
                            SELECT  sr_rfp_account_id
                            FROM    sr_rfp_account
                            WHERE   account_id = @account_id )
                    SET @error = @@error
                END

            IF ( @error = 0 ) 
                BEGIN
                    DELETE  FROM SR_RFP_LP_INTERVAL_DATA
                    WHERE   sr_rfp_account_id IN (
                            SELECT  sr_rfp_account_id
                            FROM    sr_rfp_account
                            WHERE   account_id = @account_id )
                    SET @error = @@error
                END

            IF ( @error = 0 ) 
                BEGIN
                    DELETE  FROM SR_RFP_LP_COMMENT
                    WHERE   sr_rfp_account_id IN (
                            SELECT  sr_rfp_account_id
                            FROM    sr_rfp_account
                            WHERE   account_id = @account_id )
                    SET @error = @@error
                END

            IF ( @error = 0 ) 
                BEGIN
                    DELETE  FROM SR_RFP_LP_CLIENT_APPROVAL
                    WHERE   SR_ACCOUNT_GROUP_ID IN (
                            SELECT  sr_rfp_account_id
                            FROM    sr_rfp_account
                            WHERE   account_id = @account_id
                                    AND sr_rfp_bid_group_id IS NULL )
                            AND is_bid_group = 0
                    SET @error = @@error
                END
	
            IF ( @error = 0 ) 
                BEGIN
                    DELETE  FROM SR_RFP_LP_CLIENT_APPROVAL
                    WHERE   SR_ACCOUNT_GROUP_ID IN (
                            SELECT  sr_rfp_bid_group_id
                            FROM    sr_rfp_account
                            WHERE   account_id = @account_id )
                            AND is_bid_group = 1
                    SET @error = @@error
                END

            IF ( @error = 0 ) 
                BEGIN
                    DELETE  FROM SR_RFP_LP_ACCOUNT_SUMMARY
                    WHERE   SR_ACCOUNT_GROUP_ID IN (
                            SELECT  sr_rfp_account_id
                            FROM    sr_rfp_account
                            WHERE   account_id = @account_id
                                    AND sr_rfp_bid_group_id IS NULL )
                            AND is_bid_group = 0
                    SET @error = @@error
                END

            IF ( @error = 0 ) 
                BEGIN
                    DELETE  FROM SR_RFP_LP_ACCOUNT_SUMMARY
                    WHERE   SR_ACCOUNT_GROUP_ID IN (
                            SELECT  sr_rfp_bid_group_id
                            FROM    sr_rfp_account
                            WHERE   account_id = @account_id )
                            AND is_bid_group = 1
                    SET @error = @@error
                END
	
        END

    IF ( @error = 0 ) 
        BEGIN	
            DELETE  FROM SR_RFP_CLOSURE
            WHERE   SR_ACCOUNT_GROUP_ID IN ( SELECT sr_rfp_account_id
                                             FROM   sr_rfp_account
                                             WHERE  account_id = @account_id )
            SET @error = @@error
        END

    IF ( @error = 0 ) 
        BEGIN
            DELETE  FROM SR_RFP_CONTRACT_ADMIN_INITIATIVES
            WHERE   SR_ACCOUNT_GROUP_ID IN (
                    SELECT  sr_rfp_account_id
                    FROM    sr_rfp_account
                    WHERE   account_id = @account_id
                            AND sr_rfp_bid_group_id IS NULL )
                    AND is_bid_group = 0
            SET @error = @@error
        END

    IF ( @error = 0 ) 
        BEGIN
            DELETE  FROM SR_RFP_CONTRACT_ADMIN_INITIATIVES
            WHERE   SR_ACCOUNT_GROUP_ID IN ( SELECT sr_rfp_bid_group_id
                                             FROM   sr_rfp_account
                                             WHERE  account_id = @account_id )
                    AND is_bid_group = 1
            SET @error = @@error
        END

    IF ( @error = 0 ) 
        BEGIN
            DELETE  FROM SR_RFP_REASON_NOT_WINNING_MAP
            WHERE   SR_RFP_REASON_NOT_WINNING_ID IN (
                    SELECT  SR_RFP_REASON_NOT_WINNING_ID
                    FROM    SR_RFP_REASON_NOT_WINNING
                    WHERE   SR_ACCOUNT_GROUP_ID IN (
                            SELECT  sr_rfp_account_id
                            FROM    sr_rfp_account
                            WHERE   account_id = @account_id
                                    AND sr_rfp_bid_group_id IS NULL )
                            AND is_bid_group = 0 )
            SET @error = @@error
        END

    IF ( @error = 0 ) 
        BEGIN
            DELETE  FROM SR_RFP_REASON_NOT_WINNING_MAP
            WHERE   SR_RFP_REASON_NOT_WINNING_ID IN (
                    SELECT  SR_RFP_REASON_NOT_WINNING_ID
                    FROM    SR_RFP_REASON_NOT_WINNING
                    WHERE   SR_ACCOUNT_GROUP_ID IN (
                            SELECT  sr_rfp_bid_group_id
                            FROM    sr_rfp_account
                            WHERE   account_id = @account_id )
                            AND is_bid_group = 1 )
            SET @error = @@error
        END

    IF ( @error = 0 ) 
        BEGIN
            DELETE  FROM SR_RFP_REASON_NOT_WINNING
            WHERE   SR_ACCOUNT_GROUP_ID IN (
                    SELECT  sr_rfp_account_id
                    FROM    sr_rfp_account
                    WHERE   account_id = @account_id
                            AND sr_rfp_bid_group_id IS NULL )
                    AND is_bid_group = 0
            SET @error = @@error
        END

    IF ( @error = 0 ) 
        BEGIN	
            DELETE  FROM SR_RFP_REASON_NOT_WINNING
            WHERE   SR_ACCOUNT_GROUP_ID IN ( SELECT sr_rfp_bid_group_id
                                             FROM   sr_rfp_account
                                             WHERE  account_id = @account_id )
                    AND is_bid_group = 1
            SET @error = @@error
        END




    DECLARE @sendSuppAccExistNoBidGroup INT
    DECLARE @sendSuppAccExistIsBidGroup INT

    SELECT  @sendSuppAccExistNoBidGroup = COUNT(*)
    FROM    SR_RFP_SEND_SUPPLIER
    WHERE   SR_ACCOUNT_GROUP_ID IN ( SELECT sr_rfp_account_id
                                     FROM   sr_rfp_account
                                     WHERE  account_id = @account_id
                                            AND sr_rfp_bid_group_id IS NULL )
            AND is_bid_group = 0

    SELECT  @sendSuppAccExistIsBidGroup = COUNT(*)
    FROM    SR_RFP_SEND_SUPPLIER
    WHERE   SR_ACCOUNT_GROUP_ID IN ( SELECT sr_rfp_bid_group_id
                                     FROM   sr_rfp_account
                                     WHERE  account_id = @account_id )
            AND is_bid_group = 1


    IF ( @sendSuppAccExistNoBidGroup > 0 ) 
        BEGIN
            IF ( @error = 0 ) 
                BEGIN
                    DELETE  FROM SR_RFP_SEND_SUPPLIER_LOG
                    WHERE   SR_RFP_SEND_SUPPLIER_ID IN (
                            SELECT  SR_RFP_SEND_SUPPLIER_ID
                            FROM    SR_RFP_SEND_SUPPLIER
                            WHERE   SR_ACCOUNT_GROUP_ID IN (
                                    SELECT  sr_rfp_account_id
                                    FROM    sr_rfp_account
                                    WHERE   account_id = @account_id
                                            AND sr_rfp_bid_group_id IS NULL )
                                    AND is_bid_group = 0 )
                    SET @error = @@error
                END
	
            IF ( @error = 0 ) 
                BEGIN
                    DELETE  FROM SR_RFP_SEND_SUPPLIER
                    WHERE   SR_ACCOUNT_GROUP_ID IN (
                            SELECT  sr_rfp_account_id
                            FROM    sr_rfp_account
                            WHERE   account_id = @account_id
                                    AND sr_rfp_bid_group_id IS NULL )
                            AND is_bid_group = 0
                    SET @error = @@error
                END
	
        END

    IF ( @sendSuppAccExistIsBidGroup > 0 ) 
        BEGIN
            IF ( @error = 0 ) 
                BEGIN
                    DELETE  FROM SR_RFP_SEND_SUPPLIER_LOG
                    WHERE   SR_RFP_SEND_SUPPLIER_ID IN (
                            SELECT  SR_RFP_SEND_SUPPLIER_ID
                            FROM    SR_RFP_SEND_SUPPLIER
                            WHERE   SR_ACCOUNT_GROUP_ID IN (
                                    SELECT  sr_rfp_bid_group_id
                                    FROM    sr_rfp_account
                                    WHERE   account_id = @account_id )
                                    AND is_bid_group = 1 )
                    SET @error = @@error
                END
	
            IF ( @error = 0 ) 
                BEGIN


                    DELETE  FROM SR_RFP_SEND_SUPPLIER
                    WHERE   SR_ACCOUNT_GROUP_ID IN (
                            SELECT  sr_rfp_bid_group_id
                            FROM    sr_rfp_account
                            WHERE   account_id = @account_id )
                            AND is_bid_group = 1
                    SET @error = @@error
                END
	
        END


    IF ( @error = 0 ) 
        BEGIN
            DELETE  FROM sr_rfp_sop_shortlist_details
            WHERE   sr_rfp_sop_shortlist_id IN (
                    SELECT  sr_rfp_sop_shortlist_id
                    FROM    sr_rfp_sop_shortlist
                    WHERE   SR_ACCOUNT_GROUP_ID IN (
                            SELECT  sr_rfp_account_id
                            FROM    sr_rfp_account
                            WHERE   account_id = @account_id
                                    AND sr_rfp_bid_group_id IS NULL )
                            AND is_bid_group = 0 )
            SET @error = @@error
        END
	
    IF ( @error = 0 ) 
        BEGIN
            DELETE  FROM SR_RFP_SOP_SHORTLIST
            WHERE   SR_ACCOUNT_GROUP_ID IN (
                    SELECT  sr_rfp_account_id
                    FROM    sr_rfp_account
                    WHERE   account_id = @account_id
                            AND sr_rfp_bid_group_id IS NULL )
                    AND is_bid_group = 0
            SET @error = @@error
        END
	
    IF ( @error = 0 ) 
        BEGIN
            DELETE  FROM SR_RFP_SOP_DETAILS
            WHERE   SR_RFP_SOP_ID IN (
                    SELECT  SR_RFP_SOP_ID
                    FROM    SR_RFP_SOP
                    WHERE   SR_RFP_SOP_SUMMARY_ID IN (
                            SELECT  SR_RFP_SOP_SUMMARY_ID
                            FROM    SR_RFP_SOP_SUMMARY
                            WHERE   SR_ACCOUNT_GROUP_ID IN (
                                    SELECT  sr_rfp_account_id
                                    FROM    sr_rfp_account
                                    WHERE   account_id = @account_id
                                            AND sr_rfp_bid_group_id IS NULL )
                                    AND is_bid_group = 0 ) )
            SET @error = @@error
        END
	
    IF ( @error = 0 ) 
        BEGIN
            DELETE  FROM SR_RFP_SOP
            WHERE   SR_RFP_SOP_SUMMARY_ID IN (
                    SELECT  SR_RFP_SOP_SUMMARY_ID
                    FROM    SR_RFP_SOP_SUMMARY
                    WHERE   SR_ACCOUNT_GROUP_ID IN (
                            SELECT  sr_rfp_account_id
                            FROM    sr_rfp_account
                            WHERE   account_id = @account_id
                                    AND sr_rfp_bid_group_id IS NULL )
                            AND is_bid_group = 0 )
            SET @error = @@error
        END
	
    IF ( @error = 0 ) 
        BEGIN
            DELETE  FROM SR_RFP_SOP_SUMMARY
            WHERE   SR_ACCOUNT_GROUP_ID IN (
                    SELECT  sr_rfp_account_id
                    FROM    sr_rfp_account
                    WHERE   account_id = @account_id
                            AND sr_rfp_bid_group_id IS NULL )
                    AND is_bid_group = 0
            SET @error = @@error
        END
	
    IF ( @error = 0 ) 
        BEGIN
            DELETE  FROM SR_RFP_SOP_CLIENT_APPROVAL
            WHERE   SR_ACCOUNT_GROUP_ID IN (
                    SELECT  sr_rfp_account_id
                    FROM    sr_rfp_account
                    WHERE   account_id = @account_id
                            AND sr_rfp_bid_group_id IS NULL )
                    AND is_bid_group = 0
            SET @error = @@error
        END
	
    IF ( @error = 0 ) 
        BEGIN
            DELETE  FROM SR_RFP_SOP_SMR
            WHERE   SR_ACCOUNT_GROUP_ID IN (
                    SELECT  sr_rfp_account_id
                    FROM    sr_rfp_account
                    WHERE   account_id = @account_id
                            AND sr_rfp_bid_group_id IS NULL )
                    AND is_bid_group = 0
            SET @error = @@error
        END
	

    IF ( @error = 0 ) 
        BEGIN
            DELETE  FROM sr_rfp_sop_shortlist_details
            WHERE   sr_rfp_sop_shortlist_id IN (
                    SELECT  sr_rfp_sop_shortlist_id
                    FROM    sr_rfp_sop_shortlist
                    WHERE   SR_ACCOUNT_GROUP_ID IN (
                            SELECT  sr_rfp_bid_group_id
                            FROM    sr_rfp_account
                            WHERE   account_id = @account_id )
                            AND is_bid_group = 1 )
            SET @error = @@error
        END

    IF ( @error = 0 ) 
        BEGIN
            DELETE  FROM SR_RFP_SOP_SHORTLIST
            WHERE   SR_ACCOUNT_GROUP_ID IN ( SELECT sr_rfp_bid_group_id
                                             FROM   sr_rfp_account
                                             WHERE  account_id = @account_id )
                    AND is_bid_group = 1
            SET @error = @@error
        END

    IF ( @error = 0 ) 
        BEGIN
            DELETE  FROM SR_RFP_SOP_DETAILS
            WHERE   SR_RFP_SOP_ID IN (
                    SELECT  SR_RFP_SOP_ID
                    FROM    SR_RFP_SOP
                    WHERE   SR_RFP_SOP_SUMMARY_ID IN (
                            SELECT  SR_RFP_SOP_SUMMARY_ID
                            FROM    SR_RFP_SOP_SUMMARY
                            WHERE   SR_ACCOUNT_GROUP_ID IN (
                                    SELECT  sr_rfp_bid_group_id
                                    FROM    sr_rfp_account
                                    WHERE   account_id = @account_id )
                                    AND is_bid_group = 1 ) )
            SET @error = @@error
        END

    IF ( @error = 0 ) 
        BEGIN
            DELETE  FROM SR_RFP_SOP
            WHERE   SR_RFP_SOP_SUMMARY_ID IN (
                    SELECT  SR_RFP_SOP_SUMMARY_ID
                    FROM    SR_RFP_SOP_SUMMARY
                    WHERE   SR_ACCOUNT_GROUP_ID IN (
                            SELECT  sr_rfp_bid_group_id
                            FROM    sr_rfp_account
                            WHERE   account_id = @account_id )
                            AND is_bid_group = 1 )
            SET @error = @@error
        END

    IF ( @error = 0 ) 
        BEGIN
            DELETE  FROM SR_RFP_SOP_SUMMARY
            WHERE   SR_ACCOUNT_GROUP_ID IN ( SELECT sr_rfp_bid_group_id
                                             FROM   sr_rfp_account
                                             WHERE  account_id = @account_id )
                    AND is_bid_group = 1
            SET @error = @@error
        END

    IF ( @error = 0 ) 
        BEGIN
            DELETE  FROM SR_RFP_SOP_CLIENT_APPROVAL
            WHERE   SR_ACCOUNT_GROUP_ID IN ( SELECT sr_rfp_bid_group_id
                                             FROM   sr_rfp_account
                                             WHERE  account_id = @account_id )
                    AND is_bid_group = 1
            SET @error = @@error
        END

    IF ( @error = 0 ) 
        BEGIN
            DELETE  FROM SR_RFP_SOP_SMR
            WHERE   SR_ACCOUNT_GROUP_ID IN ( SELECT sr_rfp_bid_group_id
                                             FROM   sr_rfp_account
                                             WHERE  account_id = @account_id )
                    AND is_bid_group = 1
            SET @error = @@error
        END

    DECLARE @termAccExistNoBidGroup INT
    DECLARE @termAccExistIsBidGroup INT


    SELECT  @termAccExistNoBidGroup = COUNT(*)
    FROM    SR_RFP_ACCOUNT_TERM
    WHERE   SR_ACCOUNT_GROUP_ID IN ( SELECT sr_rfp_account_id
                                     FROM   sr_rfp_account
                                     WHERE  account_id = @account_id
                                            AND sr_rfp_bid_group_id IS NULL )
            AND is_bid_group = 0

    SELECT  @termAccExistIsBidGroup = COUNT(*)
    FROM    SR_RFP_ACCOUNT_TERM
    WHERE   SR_ACCOUNT_GROUP_ID IN ( SELECT sr_rfp_bid_group_id
                                     FROM   sr_rfp_account
                                     WHERE  account_id = @account_id )
            AND is_bid_group = 1


    IF ( @termAccExistNoBidGroup > 0 ) 
        BEGIN
            IF ( @error = 0 ) 
                BEGIN
                    DELETE  FROM sr_rfp_term_product_map
                    WHERE   sr_rfp_account_term_id IN (
                            SELECT  sr_rfp_account_term_id
                            FROM    SR_RFP_ACCOUNT_TERM
                            WHERE   SR_ACCOUNT_GROUP_ID IN (
                                    SELECT  sr_rfp_account_id
                                    FROM    sr_rfp_account
                                    WHERE   account_id = @account_id
                                            AND sr_rfp_bid_group_id IS NULL )
                                    AND is_bid_group = 0 )
                    SET @error = @@error
                END

            IF ( @error = 0 ) 
                BEGIN
                    DELETE  FROM SR_RFP_ACCOUNT_TERM
                    WHERE   SR_ACCOUNT_GROUP_ID IN (
                            SELECT  sr_rfp_account_id
                            FROM    sr_rfp_account
                            WHERE   account_id = @account_id
                                    AND sr_rfp_bid_group_id IS NULL )
                            AND is_bid_group = 0
                    SET @error = @@error
                END

        END

    IF ( @termAccExistIsBidGroup > 0 ) 
        BEGIN
            IF ( @error = 0 ) 
                BEGIN
                    DELETE  FROM sr_rfp_term_product_map
                    WHERE   sr_rfp_account_term_id IN (
                            SELECT  sr_rfp_account_term_id
                            FROM    SR_RFP_ACCOUNT_TERM
                            WHERE   SR_ACCOUNT_GROUP_ID IN (
                                    SELECT  sr_rfp_bid_group_id
                                    FROM    sr_rfp_account
                                    WHERE   account_id = @account_id )
                                    AND is_bid_group = 1 )
                    SET @error = @@error
                END

            IF ( @error = 0 ) 
                BEGIN
                    DELETE  FROM SR_RFP_ACCOUNT_TERM
                    WHERE   SR_ACCOUNT_GROUP_ID IN (
                            SELECT  sr_rfp_bid_group_id
                            FROM    sr_rfp_account
                            WHERE   account_id = @account_id )
                            AND is_bid_group = 1
                    SET @error = @@error
                END
        END

    IF ( @error = 0 ) 
        BEGIN
            DELETE  FROM SR_RFP_UTILITY_SWITCH
            WHERE   SR_ACCOUNT_GROUP_ID IN (
                    SELECT  sr_rfp_account_id
                    FROM    sr_rfp_account
                    WHERE   account_id = @account_id
                            AND sr_rfp_bid_group_id IS NULL )
                    AND is_bid_group = 0
            SET @error = @@error
        END

    IF ( @error = 0 ) 
        BEGIN
            DELETE  FROM SR_RFP_UTILITY_SWITCH
            WHERE   SR_ACCOUNT_GROUP_ID IN ( SELECT sr_rfp_bid_group_id
                                             FROM   sr_rfp_account
                                             WHERE  account_id = @account_id )
                    AND is_bid_group = 1
            SET @error = @@error
        END

    IF ( @error = 0 ) 
        BEGIN
            DELETE  FROM SR_RFP_ARCHIVE_CHECKLIST
            WHERE   sr_rfp_account_id IN ( SELECT   sr_rfp_account_id
                                           FROM     sr_rfp_account
                                           WHERE    account_id = @account_id )
            SET @error = @@error
        END

    IF ( @error = 0 ) 
        BEGIN
            DELETE  FROM SR_RFP_ARCHIVE_ACCOUNT
            WHERE   sr_rfp_account_id IN ( SELECT   sr_rfp_account_id
                                           FROM     sr_rfp_account
                                           WHERE    account_id = @account_id )
            SET @error = @@error
        END

    IF ( @error = 0 ) 
        BEGIN
            DELETE  SR_RFP_UPDATE_SUPPLIER
            WHERE   SR_ACCOUNT_GROUP_ID IN (
                    SELECT  sr_rfp_account_id
                    FROM    sr_rfp_account
                    WHERE   account_id = @account_id
                            AND sr_rfp_bid_group_id IS NULL )
                    AND is_bid_group = 0
            SET @error = @@error
        END

    IF ( @error = 0 ) 
        BEGIN
            DELETE  SR_RFP_UPDATE_SUPPLIER
            WHERE   SR_ACCOUNT_GROUP_ID IN ( SELECT sr_rfp_bid_group_id
                                             FROM   sr_rfp_account
                                             WHERE  account_id = @account_id )
                    AND is_bid_group = 1
            SET @error = @@error
        END

    IF ( @error = 0 ) 
        BEGIN
            DELETE  SR_SW_RFP_NO_BID
            WHERE   SR_ACCOUNT_GROUP_ID IN (
                    SELECT  sr_rfp_account_id
                    FROM    sr_rfp_account
                    WHERE   account_id = @account_id
                            AND sr_rfp_bid_group_id IS NULL )
                    AND is_bid_group = 0
            SET @error = @@error
        END

    IF ( @error = 0 ) 
        BEGIN
            DELETE  SR_SW_RFP_NO_BID
            WHERE   SR_ACCOUNT_GROUP_ID IN ( SELECT sr_rfp_bid_group_id
                                             FROM   sr_rfp_account
                                             WHERE  account_id = @account_id )
                    AND is_bid_group = 1
            SET @error = @@error
        END

    IF ( @error = 0 ) 
        BEGIN
            DELETE  sr_comments_accounts_map
            WHERE   account_id = @account_id
            SET @error = @@error
        END

    IF ( @error = 0 ) 
        BEGIN
            DELETE  sr_master_checklist_review_status
            WHERE   account_id = @account_id
            SET @error = @@error
        END

    IF ( @error = 0 ) 
        BEGIN
            DELETE  sr_batch_log_details
            WHERE   account_id = @account_id
            SET @error = @@error
        END

    DECLARE @sr_rc_contract_document_id INT
    SELECT  @sr_rc_contract_document_id = sr_rc_contract_document_id
    FROM    sr_rc_contract_document_accounts_map
    WHERE   account_id = @account_id

    IF ( @error = 0 ) 
        BEGIN
            DELETE  sr_rc_contract_document_accounts_map
            WHERE   account_id = @account_id
            SET @error = @@error
        END

    IF ( @error = 0 ) 
        BEGIN
            DELETE  sr_rc_contract_document
            WHERE   sr_rc_contract_document_id = @sr_rc_contract_document_id
            SET @error = @@error
        END

    IF ( @error = 0 ) 
        BEGIN
            DELETE  SR_DEAL_TICKET_ACCOUNT_MAP
            WHERE   account_id = @account_id
            SET @error = @@error
        END


    IF ( @error = 0 ) 
        BEGIN
            DELETE  FROM sr_rfp_account
            WHERE   account_id = @account_id 
            SET @error = @@error
        END
END
GO
GRANT EXECUTE ON  [dbo].[DELTOOL_SR_RFP_DELETE_ACCOUNT_P] TO [CBMSApplication]
GO
