SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
              
Name:     
 dbo.EC_Calc_Val_Upd         
                
Description:                
   To update the Calc Val configuration  
                
 Input Parameters:                
    Name        DataType  Default  Description                  
---------------------------------------------------------------------------------  
 @EC_Calc_Val_Id      INT  
 @State_Id       INT  
    @Commodity_Id      INT  
 @Calc_Value_Name     NVARCHAR(255)  
 @Bucket_Master_Id     INT  
 @EC_Invoice_Sub_Bucket_Master_Id INT  
 @Starting_Period_Cd     INT  
 @Starting_Period_Operator_Cd  INT  
 @Starting_Period_Operand   SMALLINT  
 @End_Period_Cd      INT  
 @End_Period_Operator_Cd    INT  
 @End_Period_Operand     SMALLINT  
 @Aggregation_Cd      INT  
 @User_Info_Id      INT  
      
Output Parameters:                      
    Name        DataType  Default  Description                  
---------------------------------------------------------------------------------  
                
Usage Examples:                    
---------------------------------------------------------------------------------  
 declare @EC_Calc_Val_Id_Out int  
 SELECT TOP 10 a.EC_Invoice_Sub_Bucket_Master_Id,a.Bucket_Master_Id,a.State_Id,b.Commodity_Id  
 FROM dbo.EC_Invoice_Sub_Bucket_Master a JOIN dbo.Bucket_Master b ON a.Bucket_Master_Id = b.Bucket_Master_Id  
   
 BEGIN TRANSACTION  
  DECLARE  @EC_Calc_Val_Id_Out INT   
  SELECT * FROM dbo.EC_Calc_Val WHERE Calc_Value_Name ='Test_Calc_Val_Ins'  
  EXEC dbo.EC_Calc_Val_Ins  73,290,'Test_Calc_Val_Ins',329,15,101031,101017,5,101030,101018,5,100957,16,@EC_Calc_Val_Id_Out output  
  SELECT * FROM dbo.EC_Calc_Val WHERE Calc_Value_Name ='Test_Calc_Val_Ins'  
  SELECT @EC_Calc_Val_Id_Out=EC_Calc_Val_Id FROM dbo.EC_Calc_Val WHERE Calc_Value_Name ='Test_Calc_Val_Ins'  
  EXEC dbo.EC_Calc_Val_Upd  @EC_Calc_Val_Id_Out,73,290,'Test_Calc_Val_Upd',329,15,101031,101017,5,101030,101018,5,100957,16  
  SELECT * FROM dbo.EC_Calc_Val where EC_Calc_Val_Id=@EC_Calc_Val_Id_Out  
 ROLLBACK TRANSACTION  
     
   
Author Initials:                
    Initials Name                
---------------------------------------------------------------------------------  
 NR   Narayana Reddy        
 RKV         Ravi Kumar Vegesna  
   
Modifications:                
 Initials    Date  Modification                
---------------------------------------------------------------------------------  
    NR   2015-05-13 Created For AS400.  
    RKV         2015-10-15  Removed Bucket_Master_Id as Part of AS400-PII  
    RKV   2016-11-18 Added Ec_Account_Group_Type_Id as part of MAINT-4563       
    NR   2018-02-23 Recalc Data Interval - Saved the IDM_Commodity_Measurement_Group_Id.       
       
  
               
******/   
  
CREATE PROCEDURE [dbo].[EC_Calc_Val_Upd]  
      (   
       @EC_Calc_Val_Id INT  
      ,@State_Id INT  
      ,@Commodity_Id INT  
      ,@Calc_Value_Name NVARCHAR(255)  
      ,@Starting_Period_Cd INT  
      ,@Starting_Period_Operator_Cd INT  
      ,@Starting_Period_Operand SMALLINT  
      ,@End_Period_Cd INT  
      ,@End_Period_Operator_Cd INT  
      ,@End_Period_Operand SMALLINT  
      ,@Aggregation_Cd INT  
      ,@User_Info_Id INT  
      ,@Ec_Account_Group_Type_Id INT = NULL  
      ,@IDM_Commodity_Measurement_Group_Id INT = NULL
	  ,@Multiple_Option_Value_Selection_Cd int    
      ,@Multiple_Option_No_Of_Month int    
      ,@Multiple_Option_Total_Aggregation_Cd int    
      ,@Monthly_settings_Cd int    
      ,@Monthly_Setting_Start_Month_Num int  
      ,@Monthly_Setting_End_Month_Num int    
      ,@Supplier_Account_Source_Type_Cd int   
      ,@Calc_Val_Type_Cd int    
      ,@Uom_Cd  int )  
AS   
BEGIN  
      SET NOCOUNT ON   
        
      UPDATE  
            ecv  
      SET     
            State_Id = @State_Id  
           ,Commodity_Id = @Commodity_Id  
           ,Calc_Value_Name = @Calc_Value_Name  
           ,Starting_Period_Cd = @Starting_Period_Cd  
           ,Starting_Period_Operator_Cd = @Starting_Period_Operator_Cd  
           ,Starting_Period_Operand = @Starting_Period_Operand  
           ,End_Period_Cd = @End_Period_Cd  
           ,End_Period_Operator_Cd = @End_Period_Operator_Cd  
           ,End_Period_Operand = @End_Period_Operand  
           ,Aggregation_Cd = @Aggregation_Cd  
           ,Updated_User_Id = @User_Info_Id  
           ,Last_Change_Ts = GETDATE()  
           ,Ec_Account_Group_Type_Id = @Ec_Account_Group_Type_Id  
           ,IDM_Commodity_Measurement_Group_Id = @IDM_Commodity_Measurement_Group_Id 
	       ,Multiple_Option_Value_Selection_Cd  =@Multiple_Option_Value_Selection_Cd     
           ,Multiple_Option_No_Of_Month     =@Multiple_Option_No_Of_Month     
           ,Multiple_Option_Total_Aggregation_Cd=@Multiple_Option_Total_Aggregation_Cd     
           ,Monthly_settings_Cd     =@Monthly_settings_Cd     
           ,Monthly_Setting_Start_Month_Num   =@Monthly_Setting_Start_Month_Num   
           ,Monthly_Setting_End_Month_Num     =@Monthly_Setting_End_Month_Num     
           ,Supplier_Account_Source_Type_Cd    =@Supplier_Account_Source_Type_Cd    
           ,Calc_Val_Type_Cd     =@Calc_Val_Type_Cd     
           ,Uom_Cd=@Uom_Cd  
      FROM  
            dbo.EC_Calc_Val ecv  
      WHERE  
            ecv.EC_Calc_Val_Id = @EC_Calc_Val_Id  
                     
               
END  
  
  
  
GO
GRANT EXECUTE ON  [dbo].[EC_Calc_Val_Upd] TO [CBMSApplication]
GO
