SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE    PROCEDURE dbo.GET_MAIL_ID_CEM_PRACTIC_HEAD_P 
@userId varchar(10),
@sessionId varchar(20),
@clientId int
as
	set nocount on
select EMAIL_ADDRESS from user_info where user_info_id IN(
	select cem_practice_head_id from rm_cem_team where cem_user_id
IN (select user_info_id from client_cem_map where client_id=@clientId))
GO
GRANT EXECUTE ON  [dbo].[GET_MAIL_ID_CEM_PRACTIC_HEAD_P] TO [CBMSApplication]
GO
