SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	
	dbo.Commodity_History_Check_For_Site_Level_By_Client_Commmodity

DESCRIPTION:

INPUT PARAMETERS:
	Name						DataType	Default	    Description
------------------------------------------------------------------------------
	@Client_id 					INT
	@Commodity_Id				INT

OUTPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@Is_History_Exists	Bit						Returns 0 if no history 1 if history exists.

USAGE EXAMPLES:
------------------------------------------------------------

	DECLARE @Out_param BIT
	
	EXEC dbo.Commodity_History_Check_For_Site_Level_By_Client_Commmodity 10069,67,@Out_param Out
	
	SELECT @Out_param


	DECLARE @Out_param BIT
	
	EXEC dbo.Commodity_History_Check_For_Site_Level_By_Client_Commmodity 11469,66,@Out_param Out
	
	
	SELECT @Out_param


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan Ganesan


MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	HG        	1/28/2010	Created
	HG			03/18/2010	Using Site_Commodity_Image table as the source of image changed from Cost_Usage_Site_Dtl to Site_Commodity_Image.
*/

CREATE PROCEDURE dbo.Commodity_History_Check_For_Site_Level_By_Client_Commmodity(
	@Client_id	INT
	,@Commodity_Id INT
	,@Is_History_Exists BIT = 0 OUT)
AS
BEGIN

	SET NOCOUNT ON;

	IF EXISTS(
			SELECT
				1
			FROM
				dbo.Site s
				JOIN dbo.Site_Commodity_Image sci
					ON s.SITE_ID = sci.SITE_ID
			WHERE
				s.Client_ID = @Client_id
				AND sci.Commodity_Id = @Commodity_Id			
			)
	BEGIN
	
		SET @Is_History_Exists = 1

	END
END
GO
GRANT EXECUTE ON  [dbo].[Commodity_History_Check_For_Site_Level_By_Client_Commmodity] TO [CBMSApplication]
GO
