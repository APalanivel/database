SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        dbo.RM_Client_Onboard_Del                    
                      
Description:                      
        To on board selected commodity and countries
                      
Input Parameters:                      
    Name				DataType        Default     Description                        
--------------------------------------------------------------------------------
	@Country_Id			VARCHAR(MAX)
    @Commodity_Id		INT
    @User_Info_Id		INT 
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	@RM_Group_Id	INT  
                    
Usage Examples:                          
--------------------------------------------------------------------------------
	
	BEGIN TRANSACTION
		EXEC dbo.RM_Client_Onboard_Del 235,291,'4',16
		 SELECT rco.*,map.*,cnfg.*,en.ENTITY_NAME FROM dbo.RM_Client_Onboard rco 
				INNER JOIN dbo.RM_Client_Default_Hedge_Config_Map map ON rco.RM_Client_Onboard_Id = map.RM_Client_Onboard_Id
				INNER JOIN dbo.RM_Hedge_Config cnfg ON map.RM_Hedge_Config_Id = cnfg.RM_Hedge_Config_Id
				INNER JOIN dbo.ENTITY en ON en.ENTITY_ID = cnfg.Hedge_Type_Id
			WHERE rco.Client_Id = 235
	ROLLBACK TRANSACTION     
                     
 Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy        
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			2018-30-07     Global Risk Management - Created                    
                     
******/
CREATE PROCEDURE [dbo].[RM_Client_Onboard_Del]
    (
        @Client_Id INT
        , @Commodity_Id INT
        , @Country_Id INT
        , @User_Info_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @RM_Hedge_Config_Del AS TABLE
              (
                  RM_Hedge_Config_Id INT
              );

        DECLARE
            @Client_Name VARCHAR(200)
            , @Application_Name VARCHAR(30)
            , @Commodity_Name VARCHAR(200)
            , @Country_Name VARCHAR(200)
            , @Audit_Function SMALLINT = -1
            , @Lookup_Value XML
            , @Current_Ts DATETIME = GETDATE()
            , @User_Name VARCHAR(81)
            , @Client_Client_Hier_Id INT
            , @RM_Client_Hier_Onboard VARCHAR(MAX);

        SELECT  @Application_Name = ISNULL(@Application_Name, 'RM Onboard Delete');

        SELECT
            @Client_Client_Hier_Id = Client_Hier_Id
        FROM
            Core.Client_Hier
        WHERE
            Client_Id = @Client_Id
            AND Sitegroup_Id = 0;

        SELECT
            @User_Name = ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME
        FROM
            dbo.USER_INFO ui
        WHERE
            ui.USER_INFO_ID = @User_Info_Id;


        SELECT
            @Commodity_Name = comm.Commodity_Name
            , @Country_Name = con.COUNTRY_NAME
        FROM
            Trade.RM_Client_Hier_Onboard rco
            INNER JOIN dbo.COUNTRY con
                ON rco.COUNTRY_ID = con.COUNTRY_ID
            INNER JOIN dbo.Commodity comm
                ON rco.Commodity_Id = comm.Commodity_Id
        WHERE
            rco.Client_Hier_Id = @Client_Client_Hier_Id
            AND rco.Commodity_Id = @Commodity_Id
            AND rco.COUNTRY_ID = @Country_Id;

        SELECT
            @RM_Client_Hier_Onboard = ISNULL(@RM_Client_Hier_Onboard, '') + ','
                                      + CAST(cho.RM_Client_Hier_Onboard_Id AS VARCHAR(10))
        FROM
            Trade.RM_Client_Hier_Onboard cho
            INNER JOIN Core.Client_Hier ch
                ON cho.Client_Hier_Id = ch.Client_Hier_Id
        WHERE
            ch.Client_Id = @Client_Id
            AND cho.Commodity_Id = @Commodity_Id
            AND cho.Country_Id = @Country_Id;


        SET @Lookup_Value = (   SELECT
                                    @Country_Id AS Country_Id
                                    , @Country_Name AS Country_Name
                                    , @Commodity_Id AS Commodity_Id
                                    , @Commodity_Name AS Commodity_Name
                                    , @RM_Client_Hier_Onboard AS RM_Client_Hier_Onboard
                                FOR XML RAW);

        SELECT
            @Client_Name = cl.CLIENT_NAME
        FROM
            dbo.CLIENT cl
        WHERE
            cl.CLIENT_ID = @Client_Id;

        DELETE
        chhc
        FROM
            dbo.RM_Client_Workflow chhc
            INNER JOIN Trade.RM_Client_Hier_Onboard cho
                ON chhc.RM_Client_Hier_Onboard_Id = cho.RM_Client_Hier_Onboard_Id
            INNER JOIN Core.Client_Hier ch
                ON cho.Client_Hier_Id = ch.Client_Hier_Id
        WHERE
            ch.Client_Id = @Client_Id
            AND cho.Commodity_Id = @Commodity_Id
            AND cho.Country_Id = @Country_Id;

        DELETE
        chhc
        FROM
            Trade.RM_Client_Hier_Hedge_Config chhc
            INNER JOIN Trade.RM_Client_Hier_Onboard cho
                ON chhc.RM_Client_Hier_Onboard_Id = cho.RM_Client_Hier_Onboard_Id
            INNER JOIN Core.Client_Hier ch
                ON cho.Client_Hier_Id = ch.Client_Hier_Id
        WHERE
            ch.Client_Id = @Client_Id
            AND cho.Commodity_Id = @Commodity_Id
            AND cho.Country_Id = @Country_Id;

        DELETE
        cho
        FROM
            Trade.RM_Client_Hier_Onboard cho
            INNER JOIN Core.Client_Hier ch
                ON cho.Client_Hier_Id = ch.Client_Hier_Id
        WHERE
            ch.Client_Id = @Client_Id
            AND cho.Commodity_Id = @Commodity_Id
            AND cho.Country_Id = @Country_Id;



        EXEC dbo.Application_Audit_Log_Ins
            @Client_Name
            , @Application_Name
            , @Audit_Function
            , 'RM_Client_Hier_Onboard'
            , @Lookup_Value
            , @User_Name
            , @Current_Ts;



    END;



GO
GRANT EXECUTE ON  [dbo].[RM_Client_Onboard_Del] TO [CBMSApplication]
GO
