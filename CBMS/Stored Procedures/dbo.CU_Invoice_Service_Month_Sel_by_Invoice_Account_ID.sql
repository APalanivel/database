SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********           
NAME:  dbo.CU_Invoice_Service_Month_Sel_by_Invoice_Account_ID 
         
DESCRIPTION:  This will fetch all the accounts for the cu_invoice_id ,to fill the accounts drop down in invoice details section     
        
INPUT PARAMETERS:            
  Name             DataType         Default     Description            
------------------------------------------------------------            
   @cu_invoice_id	 INT    
   @account_id		 INT
 
OUTPUT PARAMETERS:            
  Name              DataType      Default     Description            
------------------------------------------------------------            
USAGE EXAMPLES:       

	EXEC CU_Invoice_Service_Month_Sel_by_Invoice_Account_ID	27049,10359	
	EXEC CU_Invoice_Service_Month_Sel_by_Invoice_Account_ID	7265,8742	
	CU_Invoice_Service_Month_Sel_by_Invoice_Account_ID 2928574, 141301
------------------------------------------------------------          
AUTHOR INITIALS:          
 Initials	Name          
------------------------------------------------------------          
NK			Nageswara Rao Kosuri                 

 Initials	Date		Modification          
------------------------------------------------------------          
SKA			03/16/2009  Created        
******/   
     
CREATE  PROCEDURE dbo.CU_Invoice_Service_Month_Sel_by_Invoice_Account_ID
  @cu_invoice_id AS INT    
 ,@account_id  AS INT 
  
AS     
BEGIN    
    SET  NOCOUNT ON;
        
    SELECT  
		  account_id    
		,cu_invoice_id  
		,begin_dt begin_date    
		,end_dt end_date    
		,billing_days    
		,SERVICE_MONTH
    FROM        
		dbo.CU_Invoice_Service_Month    
    WHERE       
		CU_INVOICE_ID = @cu_invoice_id    
		AND ACCOUNT_ID = @account_id   
	   
END    


GO
GRANT EXECUTE ON  [dbo].[CU_Invoice_Service_Month_Sel_by_Invoice_Account_ID] TO [CBMSApplication]
GO
