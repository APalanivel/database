SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.Rm_Budget_Ins                        
                          
Description:                          
        To get market price and forecast pirce if a selected index   
                          
Input Parameters:                          
    Name    DataType        Default     Description                            
--------------------------------------------------------------------------------    
	@Index_Id   INT    
    @Start_Dt	Date    
	@End_Dt		Date
                          
 Output Parameters:                                
	Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
Usage Examples:                              
--------------------------------------------------------------------------------    
	SELECT * FROM dbo.ENTITY e WHERE e.ENTITY_TYPE=272

	EXEC Trade.Rm_Budget_Ins  584,'2019-01-01','2019-12-01'  
    
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR			2019-12-23	RM-Budgets Enahancement - Created
	RR			2020-02-13	GRM-1730 Modified @Site_Id input to varchar	                
******/
CREATE PROCEDURE [Trade].[Rm_Budget_Ins]
    (
        @Client_Id INT
        , @Budget_NAME NVARCHAR(1000)
        , @Commodity_Id INT
        , @Index_Id INT
        , @Budget_Type_Cd INT
        , @Budget_Level_Cd INT
        , @Budget_Start_Dt DATE
        , @Budget_End_Dt DATE
        , @Is_Client_Generated BIT = 0
        , @Uom_Type_Id INT
        , @Currency_Unit_Id INT
        , @Rm_Forecast_Id INT = NULL
        , @User_Info_Id INT
        , @Rm_Budget_Id INT OUT
        , @Country_Id VARCHAR(MAX) = NULL
        , @Participant_Sites VARCHAR(20)
        , @Division_Id INT = NULL
        , @Site_Id VARCHAR(MAX) = NULL
        , @Is_Budget_Approved BIT = 0
        , @RM_Group_Id INT = NULL
        , @RM_Budget_Participant_Sites_Type_Cd INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        CREATE TABLE #Participant_Sites
             (
                 Client_Hier_Id INT
             );

        --DECLARE @RM_Budget_Participant_Sites_Type_Cd INT;

        SELECT
            @RM_Budget_Participant_Sites_Type_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset c2
                ON c2.Codeset_Id = c.Codeset_Id
        WHERE
            c.Code_Value = @Participant_Sites
            AND c2.Codeset_Name = 'RMBudgetParticipantSites';

        INSERT INTO Trade.Rm_Budget
             (
                 Client_Id
                 , Budget_NAME
                 , Commodity_Id
                 , Index_Id
                 , Budget_Type_Cd
                 , Budget_Level_Cd
                 , Budget_Start_Dt
                 , Budget_End_Dt
                 , Is_Client_Generated
                 , Uom_Type_Id
                 , Currency_Unit_Id
                 , Rm_Forecast_Id
                 , Created_User_Id
                 , Created_Ts
                 , Last_Change_Ts
                 , RM_Budget_Participant_Sites_Type_Cd
             )
        VALUES
            (@Client_Id
             , @Budget_NAME
             , @Commodity_Id
             , @Index_Id
             , @Budget_Type_Cd
             , @Budget_Level_Cd
             , @Budget_Start_Dt
             , @Budget_End_Dt
             , @Is_Client_Generated
             , @Uom_Type_Id
             , @Currency_Unit_Id
             , @Rm_Forecast_Id
             , @User_Info_Id
             , GETDATE()
             , GETDATE()
             , @RM_Budget_Participant_Sites_Type_Cd);

        SELECT  @Rm_Budget_Id = SCOPE_IDENTITY();

        INSERT INTO #Participant_Sites
             (
                 Client_Hier_Id
             )
        EXEC Trade.RM_Budget_Participant_Sites_Sel
            @Client_Id = @Client_Id
            , @Commodity_Id = @Commodity_Id
            , @Country_Id = @Country_Id
            , @Start_Dt = @Budget_Start_Dt
            , @End_Dt = @Budget_End_Dt
            , @Participant_Sites = @Participant_Sites
            , @Index_Id = @Index_Id
            , @Division_Id = @Division_Id
            , @Site_Id = @Site_Id
            , @RM_Group_Id = @RM_Group_Id;

        INSERT INTO Trade.Rm_Budget_Participant
             (
                 Rm_Budget_Id
                 , Client_Hier_Id
             )
        SELECT  @Rm_Budget_Id, Client_Hier_Id FROM  #Participant_Sites ps;

        UPDATE
            Trade.Rm_Budget
        SET
            Is_Budget_Approved = 1
            , Approved_By_User_Info_Id = @User_Info_Id
            , Approved_Ts = GETDATE()
            , Last_Change_Ts = GETDATE()
        WHERE
            Rm_Budget_Id = @Rm_Budget_Id
            AND (   @Is_Client_Generated = 1
                    OR  @Is_Budget_Approved = 1);

        UPDATE
            rbsdbc
        SET
            rbsdbc.Rm_Budget_Id = rb.Rm_Budget_Id
            , rbsdbc.Last_Updated_By = rb.Created_User_Id
            , rbsdbc.Last_Change_Ts = GETDATE()
        FROM
            Trade.Rm_Budget_Site_Default_Budget_Config rbsdbc
            INNER JOIN Trade.Rm_Budget_Participant rbp
                ON rbp.Client_Hier_Id = rbsdbc.Client_Hier_Id
            INNER JOIN Trade.Rm_Budget rb
                ON rb.Rm_Budget_Id = rbp.Rm_Budget_Id
        WHERE
            rb.Rm_Budget_Id = @Rm_Budget_Id
            AND rb.Is_Client_Generated = 1
            AND @Is_Client_Generated = 1
            AND rbsdbc.Start_Dt >= rb.Budget_Start_Dt
            AND rbsdbc.End_Dt >= rb.Budget_Start_Dt
            AND rbsdbc.Start_Dt <= rb.Budget_End_Dt
            AND rbsdbc.End_Dt <= rb.Budget_End_Dt;

        INSERT INTO Trade.Rm_Budget_Site_Default_Budget_Config
             (
                 Client_Hier_Id
                 , Rm_Budget_Id
                 , Start_Dt
                 , End_Dt
                 , Created_User_Id
                 , Created_Ts
                 , Last_Updated_By
                 , Last_Change_Ts
             )
        SELECT
            rbp.Client_Hier_Id
            , rb.Rm_Budget_Id
            , rb.Budget_Start_Dt
            , rb.Budget_End_Dt
            , rb.Created_User_Id
            , GETDATE()
            , rb.Created_User_Id
            , GETDATE()
        FROM
            Trade.Rm_Budget_Participant rbp
            INNER JOIN Trade.Rm_Budget rb
                ON rb.Rm_Budget_Id = rbp.Rm_Budget_Id
        WHERE
            rb.Rm_Budget_Id = @Rm_Budget_Id
            AND rb.Is_Budget_Approved = 1
            --AND @Is_Client_Generated = 1
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.Rm_Budget_Site_Default_Budget_Config rbsdbc
                               WHERE
                                    rbp.Client_Hier_Id = rbsdbc.Client_Hier_Id
                                    AND (   rbsdbc.Start_Dt BETWEEN rb.Budget_Start_Dt
                                                            AND     rb.Budget_End_Dt
                                            OR  rbsdbc.End_Dt BETWEEN rb.Budget_Start_Dt
                                                              AND     rb.Budget_End_Dt
                                            OR  rb.Budget_Start_Dt BETWEEN rbsdbc.Start_Dt
                                                                   AND     rbsdbc.End_Dt
                                            OR  rb.Budget_End_Dt BETWEEN rbsdbc.Start_Dt
                                                                 AND     rbsdbc.End_Dt));

        INSERT INTO Trade.Rm_Budget_Filter_Participant
             (
                 Rm_Budget_Id
                 , Filter_Participant_Id
                 , RM_Budget_Filter_Participant_Type_Cd
             )
        SELECT
            @Rm_Budget_Id
            , CAST(us.Segments AS INT)
            , c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset c2
                ON c2.Codeset_Id = c.Codeset_Id
            CROSS JOIN dbo.ufn_split(@Site_Id, ',') us
        WHERE
            @Site_Id IS NOT NULL
            AND c2.Codeset_Name = 'RMBudgetFilterParticipant'
            AND c.Code_Value = 'Site';

        INSERT INTO Trade.Rm_Budget_Filter_Participant
             (
                 Rm_Budget_Id
                 , Filter_Participant_Id
                 , RM_Budget_Filter_Participant_Type_Cd
             )
        SELECT
            @Rm_Budget_Id
            , @Division_Id
            , c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset c2
                ON c2.Codeset_Id = c.Codeset_Id
        WHERE
            @Division_Id IS NOT NULL
            AND c2.Codeset_Name = 'RMBudgetFilterParticipant'
            AND c.Code_Value = 'Division';

        INSERT INTO Trade.Rm_Budget_Filter_Participant
             (
                 Rm_Budget_Id
                 , Filter_Participant_Id
                 , RM_Budget_Filter_Participant_Type_Cd
             )
        SELECT
            @Rm_Budget_Id
            , @RM_Group_Id
            , c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset c2
                ON c2.Codeset_Id = c.Codeset_Id
        WHERE
            @RM_Group_Id IS NOT NULL
            AND c2.Codeset_Name = 'RMBudgetFilterParticipant'
            AND c.Code_Value = 'RM Group';

        INSERT INTO Trade.Rm_Budget_Filter_Participant
             (
                 Rm_Budget_Id
                 , Filter_Participant_Id
                 , RM_Budget_Filter_Participant_Type_Cd
             )
        SELECT
            @Rm_Budget_Id
            , CAST(us.Segments AS INT)
            , c.Code_Id
        FROM
            dbo.ufn_split(@Country_Id, ',') us
            CROSS JOIN dbo.Code c
            INNER JOIN dbo.Codeset c2
                ON c2.Codeset_Id = c.Codeset_Id
        WHERE
            @Country_Id IS NOT NULL
            AND c2.Codeset_Name = 'RMBudgetFilterParticipant'
            AND c.Code_Value = 'Country';



    END;

GO
GRANT EXECUTE ON  [Trade].[Rm_Budget_Ins] TO [CBMSApplication]
GO
