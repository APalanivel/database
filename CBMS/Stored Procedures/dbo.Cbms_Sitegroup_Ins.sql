SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 
 dbo.Cbms_Sitegroup_Ins
 
 DESCRIPTION:   
 
	This procedure used to insert the Non metric(Division/State/Country/Supplier/Vendor..) rules of a smart group
 
 INPUT PARAMETERS:  
	Name			DataType	Default		Description  
------------------------------------------------------------    
	@SiteGroup_Id INT
	, @Rule_Filter_Id INT	
	, @Filter_Condition_Id INT
	, @Filter_Value VARCHAR(50)
	, @Is_Inclusive BIT
	
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  USAGE EXAMPLES:  
------------------------------------------------------------  
	
BEGIN TRANSACTION
	EXEC dbo.Cbms_Sitegroup_Ins @Client_Id = 218, @Group_Name = N'Test 07/06', @Commodity_Id = 291
     , @Module_Cd = 102746,@Group_Type_Cd = 102731,@User_Info_Id = 16, @Cbms_Sitegroup_Id = 0 
      SELECT TOP 5 * FROM dbo.Cbms_Sitegroup ORDER BY Cbms_Sitegroup_Id DESC
ROLLBACK TRANSACTION

AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 NR			Narayana Reddy    
 
 MODIFICATIONS   
 Initials	Date		Modification  
------------------------------------------------------------  
 NR			2018-09-27	Created for GRM.
  
******/

CREATE PROC [dbo].[Cbms_Sitegroup_Ins]
      ( 
       @Client_Id INT
      ,@Group_Name NVARCHAR(255)
      ,@Commodity_Id INT = NULL
      ,@Module_Cd INT
      ,@Group_Type_Cd INT
      ,@User_Info_Id INT
      ,@Cbms_Sitegroup_Id INT OUTPUT )
AS 
BEGIN
      SET NOCOUNT ON;


      INSERT      INTO dbo.Cbms_Sitegroup
                  ( 
                   Client_Id
                  ,Module_Cd
                  ,Group_Name
                  ,Commodity_Id
                  ,Group_Type_Cd
                  ,Created_User_Id
                  ,Created_Ts
                  ,Updated_User_Id
                  ,Last_Change_Ts
                  ,Last_Refresh_Ts )
                  SELECT
                        @Client_Id
                       ,@Module_Cd
                       ,@Group_Name
                       ,@Commodity_Id
                       ,@Group_Type_Cd
                       ,@User_Info_Id
                       ,GETDATE()
                       ,@User_Info_Id
                       ,GETDATE()
                       ,GETDATE()
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.Cbms_Sitegroup cs
                                     WHERE
                                          cs.Group_Name = @Group_Name
                                          AND cs.Module_Cd = @Module_Cd
                                          AND cs.Client_Id = @Client_Id );




      SELECT
            @Cbms_Sitegroup_Id = SCOPE_IDENTITY();


END;




GO
GRANT EXECUTE ON  [dbo].[Cbms_Sitegroup_Ins] TO [CBMSApplication]
GO
