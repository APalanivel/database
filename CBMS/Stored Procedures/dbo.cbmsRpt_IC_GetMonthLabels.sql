SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsRpt_IC_GetMonthLabels

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@report_year   	int       	          	
	@client_id     	int       	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE   procedure [dbo].[cbmsRpt_IC_GetMonthLabels]
	( @MyAccountId int
	, @report_year int
	, @client_id int = null
	)
AS
BEGIN

	set nocount on
	set ansi_warnings off

	  declare @session_uid uniqueidentifier
		, @begin_date datetime
		, @end_date datetime

	      set @session_uid = newid()

	     exec cbmsRpt_ServiceMonth_LoadForReportYear @MyAccountId, @session_uid, @report_year, @client_id

	  declare @month1 int
		, @month2 int
		, @month3 int
		, @month4 int
		, @month5 int
		, @month6 int
	  declare @month7 int
		, @month8 int
		, @month9 int
		, @month10 int
		, @month11 int
		, @month12 int
	  declare @row_number int
		, @working_date datetime

	set @row_number = 1

	declare month_list cursor read_only
	for
	   select service_month
	     from rpt_service_month
	    where session_uid = @session_uid
	
	open month_list
	
	fetch next from month_list into @working_date
	while (@@fetch_status <> -1)
	begin

		if @row_number = 1 begin set @month1 = month(@working_date) end
		else if @row_number = 2 begin set @month2 = month(@working_date) end
		else if @row_number = 3 begin set @month3 = month(@working_date) end
		else if @row_number = 4 begin set @month4 = month(@working_date) end
		else if @row_number = 5 begin set @month5 = month(@working_date) end
		else if @row_number = 6 begin set @month6 = month(@working_date) end
		else if @row_number = 7 begin set @month7 = month(@working_date) end
		else if @row_number = 8 begin set @month8 = month(@working_date) end
		else if @row_number = 9 begin set @month9 = month(@working_date) end
		else if @row_number = 10 begin set @month10 = month(@working_date) end
		else if @row_number = 11 begin set @month11 = month(@working_date) end
		else if @row_number = 12 begin set @month12 = month(@working_date) end

		set @row_number = @row_number + 1

		fetch next from month_list into @working_date
	end
	
	close month_list
	DEALLOCATE month_list

	   select 'Month1Label' = max(case when month(service_month) = @month1 then service_month else null end)
		, 'Month2Label' = max(case when month(service_month) = @month2 then service_month else null end)
		, 'Month3Label' = max(case when month(service_month) = @month3 then service_month else null end)
		, 'Month4Label' = max(case when month(service_month) = @month4 then service_month else null end)
		, 'Month5Label' = max(case when month(service_month) = @month5 then service_month else null end)
		, 'Month6Label' = max(case when month(service_month) = @month6 then service_month else null end)
		, 'Month7Label' = max(case when month(service_month) = @month7 then service_month else null end)
		, 'Month8Label' = max(case when month(service_month) = @month8 then service_month else null end)
		, 'Month9Label' = max(case when month(service_month) = @month9 then service_month else null end)
		, 'Month10Label' = max(case when month(service_month) = @month10 then service_month else null end)
		, 'Month11Label' = max(case when month(service_month) = @month11 then service_month else null end)
		, 'Month12Label' = max(case when month(service_month) = @month12 then service_month else null end)
		, @report_year report_year
	     from rpt_service_month
	    where session_uid = @session_uid

        exec cbmsRpt_ServiceMonth_RemoveForSession @MyAccountId, @session_uid
	set ansi_warnings on

END
GO
GRANT EXECUTE ON  [dbo].[cbmsRpt_IC_GetMonthLabels] TO [CBMSApplication]
GO
