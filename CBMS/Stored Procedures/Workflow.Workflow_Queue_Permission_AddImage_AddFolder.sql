SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******        
NAME:    [Workflow].[Workflow_Queue_Permission_AddImage_AddFolder]    
DESCRIPTION:     
------------------------------------------------------------       
 INPUT PARAMETERS:        
 Name   DataType  Default Description      
 @User_Info_Id INT,        
@Module_Id  INT =NULL -- In feature purpose       
------------------------------------------------------------        
 OUTPUT PARAMETERS:        
 Name   DataType  Default Description        
------------------------------------------------------------        
 USAGE EXAMPLES:        
------------------------------------------------------------        
AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
TRK   Ramakrishna Thummala Summit Energy     
     
 MODIFICATIONS         
 Initials Date   Modification        
------------------------------------------------------------        
 TRK  11/09/2019 Created    
    
******/    
      
CREATE PROCEDURE [Workflow].[Workflow_Queue_Permission_AddImage_AddFolder]         
(        
@User_Info_Id INT,        
@Module_Id  INT =NULL -- In feature purpose        
)        
AS        
BEGIN        
        
 SELECT DISTINCT        
 p.PERMISSION_INFO_ID,p.PERMISSION_NAME,U.USER_INFO_ID,'AddImageAddFolder' AS APPLYTO        
 FROM   group_info GI         
 JOIN user_info_group_info_map UIG ON GI.group_info_id = UIG.group_info_id         
 JOIN user_info U ON U.user_info_id = UIG.user_info_id         
 JOIN group_info_permission_info_map GIPM ON GIPM.group_info_id = GI.group_info_id         
 JOIN permission_info P WITH (NOLOCK) ON P.permission_info_id = GIPM.permission_info_id         
 WHERE P.PERMISSION_NAME='Invoice.AddImageAddFromFolder' AND U.USER_INFO_ID=@User_Info_Id    
 
 UNION 
 
 SELECT DISTINCT        
 p.PERMISSION_INFO_ID,p.PERMISSION_NAME,U.USER_INFO_ID,'QueuesClientPriorityDownload' AS APPLYTO      
 FROM   group_info GI         
 JOIN user_info_group_info_map UIG ON GI.group_info_id = UIG.group_info_id         
 JOIN user_info U ON U.user_info_id = UIG.user_info_id         
 JOIN group_info_permission_info_map GIPM ON GIPM.group_info_id = GI.group_info_id         
 JOIN permission_info P WITH (NOLOCK) ON P.permission_info_id = GIPM.permission_info_id         
 WHERE P.PERMISSION_NAME='InvoiceProcessing.InvoicePriority' AND U.USER_INFO_ID=@User_Info_Id        
        
END     
    
  


GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_Permission_AddImage_AddFolder] TO [CBMSApplication]
GO
