SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- sp_help sr_load_profile_additional_row


CREATE PROCEDURE dbo.SR_RFP_UPDATE_ADDITIONAL_ROWS_P
	@user_id varchar(10),
	@session_id varchar(20),
	@commodity_type_id int,
	@vendor_id int,
	@row_id int,
	@row_name varchar(100),
	@row_value varchar(200)
	AS
	set nocount on
declare @setup_id int

select	@setup_id = sr_load_profile_default_setup_id 
from 	sr_load_profile_default_setup(nolock) 
where 	vendor_id = @vendor_id  
	and commodity_type_id = @commodity_type_id

if(@row_id > 0)
begin
	update	sr_load_profile_additional_row 
	set 	row_name = @row_name,
		row_value = @row_value
	where 	sr_load_profile_additional_row_id = @row_id
		and sr_load_profile_default_setup_id = @setup_id
end
else
begin
	insert into sr_load_profile_additional_row 
	(sr_load_profile_default_setup_id, row_name, row_value)
	values(@setup_id, @row_name, @row_value)  
								
end
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_UPDATE_ADDITIONAL_ROWS_P] TO [CBMSApplication]
GO
