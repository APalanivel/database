
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  /*****  
          
NAME: dbo.Report_DE_Utility_Vendor_Analyst_Details        
      
DESCRIPTION:          
 used to get the rates  
         
INPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
                     
  
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
      
USAGE EXAMPLES:  
------------------------------------------------------------  
EXEC Report_DE_Utility_Vendor_Analyst_Details        
      
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 AKR   Ashok Kumar Raju  
       
MAINTENANCE LOG:          
 Initials Date  Modification   
--- ---------- ----------------------------------------------          
 AKR   2012-05-01  Cloned from select statement of job "NightlyUtilityAnlaystII"   
 AKR   2014-09-10  Modified the code remove Utility_Analyst_Map View
******/  
  
CREATE PROCEDURE [dbo].[Report_DE_Utility_Vendor_Analyst_Details]
AS 
BEGIN    
  
      SET NOCOUNT ON;  
      
      DECLARE @Group_Legacy_Name TABLE
            ( 
             GROUP_INFO_ID INT
            ,GROUP_NAME VARCHAR(200)
            ,Legacy_Group_Name VARCHAR(200) )  
   
      DECLARE @Uility_Vendor_Type_Id INT   
          
      SELECT
            @Uility_Vendor_Type_Id = ENTITY_ID
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_NAME = 'Utility'
            AND ENTITY_DESCRIPTION = 'Vendor'  
            
      
      INSERT      INTO @Group_Legacy_Name
                  ( 
                   GROUP_INFO_ID
                  ,GROUP_NAME
                  ,Legacy_Group_Name )
                  EXEC dbo.Group_Info_Sel_By_Group_Legacy 
                        @Group_Legacy_Name_Cd_Value = 'Quality_Assurance'  

  
      SELECT
            v.VENDOR_NAME AS Utility
           ,ui.FIRST_NAME AS AnalystFirstName
           ,ui.LAST_NAME AS AnalystLastName
           ,st.STATE_NAME AS State
           ,r.REGION_NAME AS Region
      FROM
            dbo.VENDOR v
            LEFT JOIN dbo.UTILITY_DETAIL ud
                  ON v.VENDOR_ID = ud.VENDOR_ID
            LEFT JOIN ( dbo.Utility_Detail_Analyst_Map UDAM
                        INNER JOIN @Group_Legacy_Name GI
                              ON UDAM.Group_Info_ID = GI.GROUP_INFO_ID )
                        ON ud.UTILITY_DETAIL_ID = udam.UTILITY_DETAIL_ID
            INNER JOIN dbo.USER_INFO ui
                  ON udam.Analyst_ID = ui.USER_INFO_ID
            INNER JOIN dbo.VENDOR_STATE_MAP vsm
                  ON v.VENDOR_ID = vsm.VENDOR_ID
            INNER JOIN dbo.STATE st
                  ON vsm.STATE_ID = st.STATE_ID
            INNER JOIN dbo.REGION r
                  ON st.REGION_ID = r.REGION_ID
      WHERE
            ( v.VENDOR_TYPE_ID = @Uility_Vendor_Type_Id )   
END;

GO

GRANT EXECUTE ON  [dbo].[Report_DE_Utility_Vendor_Analyst_Details] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_Utility_Vendor_Analyst_Details] TO [CBMSApplication]
GO
