SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
                           
 NAME:  [dbo].[GET_ALL_SERVICE_LEVEL_P]                        
                            
 DESCRIPTION:        
				To get the service level Type  .                            
                            
 INPUT PARAMETERS:        
                           
 Name                         DataType			 Default		Description        
-----------------------------------------------------------------------------------      
                         
 OUTPUT PARAMETERS:        
                                 
 Name                         DataType			 Default		Description        
-----------------------------------------------------------------------------------      
                            
 USAGE EXAMPLES:                                
-----------------------------------------------------------------------------------      
       
Exec dbo.GET_ALL_SERVICE_LEVEL_P
                           
 AUTHOR INITIALS:      
         
 Initials               Name        
-----------------------------------------------------------------------------------      
 NR                     Narayana Reddy                              
                             
 MODIFICATIONS:      
                             
 Initials               Date            Modification      
-----------------------------------------------------------------------------------      
 NR                     2019-08-27      Created for Add contract.                         
                           
******/
CREATE PROCEDURE [dbo].[GET_ALL_SERVICE_LEVEL_P]
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            ENTITY_ID
            , ENTITY_NAME
        FROM
            ENTITY
        WHERE
            ENTITY_TYPE = 708
            AND ENTITY_NAME <> ' '
        ORDER BY
            ENTITY_NAME;

    END;




GO
GRANT EXECUTE ON  [dbo].[GET_ALL_SERVICE_LEVEL_P] TO [CBMSApplication]
GO
