SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.Contract_Admin_User_Sel            
                        
 DESCRIPTION:                        
			To get user list based on permission.                      
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
------------------------------------------------------------------------------     
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
------------------------------------------------------------------------------     
                        
 USAGE EXAMPLES:                            
------------------------------------------------------------------------------     
 
EXEC dbo.Contract_Admin_User_Sel
               
 AUTHOR INITIALS:        
       
 Initials              Name        
------------------------------------------------------------------------------     
 NR						Narayana Reddy
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
------------------------------------------------------------------------------     
 NR                    2019-05-29       Created for - Add Contract.
                       
******/
CREATE PROCEDURE [dbo].[Contract_Admin_User_Sel]
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            ui.USER_INFO_ID
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS full_name
        FROM
            dbo.PERMISSION_INFO pi
            INNER JOIN dbo.GROUP_INFO_PERMISSION_INFO_MAP gipim
                ON gipim.PERMISSION_INFO_ID = pi.PERMISSION_INFO_ID
            INNER JOIN dbo.GROUP_INFO gi
                ON gi.GROUP_INFO_ID = gipim.GROUP_INFO_ID
            INNER JOIN dbo.USER_INFO_GROUP_INFO_MAP uigim
                ON gi.GROUP_INFO_ID = uigim.GROUP_INFO_ID
            INNER JOIN dbo.USER_INFO ui
                ON ui.USER_INFO_ID = uigim.USER_INFO_ID
        WHERE
            pi.PERMISSION_NAME = 'contract.contract-admin'
            AND ui.IS_HISTORY = 0
            AND ui.ACCESS_LEVEL = 0
        GROUP BY
            ui.USER_INFO_ID
            , ui.FIRST_NAME
            , ui.LAST_NAME
        ORDER BY
            CASE WHEN ui.FIRST_NAME = 'Miguel'
                      AND   ui.LAST_NAME = 'Gonzalez' THEN 1
                ELSE 2
            END
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME;




    END;


GO
GRANT EXECUTE ON  [dbo].[Contract_Admin_User_Sel] TO [CBMSApplication]
GO
