SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE dbo.INSERT_TIER_DETAIL_P 
	@chargeSeasonMapId INT,
	@lowerBound DECIMAL(32,16),
	@upperBound DECIMAL(32,16),
	@tierNumber INT
AS
BEGIN

	SET NOCOUNT ON

	INSERT INTO dbo.tier_detail(
		charge_season_map_id, 
		lower_bound, 
		upper_bound, 
		tier_number) 
	VALUES (@chargeSeasonMapId, 
		@lowerBound, 
		@upperBound, 
		@tierNumber)   

END
GO
GRANT EXECUTE ON  [dbo].[INSERT_TIER_DETAIL_P] TO [CBMSApplication]
GO
