SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[cbmsRegion_GetAll]
	( @MyAccountId int )
AS
BEGIN

	   select region_id
		, region_name
	     from region
	 order by region_name

END
GO
GRANT EXECUTE ON  [dbo].[cbmsRegion_GetAll] TO [CBMSApplication]
GO
