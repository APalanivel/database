SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE [dbo].[cbmsAppMenuProfile_GetAll]
	( @MyAccountId int )
AS
BEGIN

	   select app_menu_profile_id
		, app_menu_profile_name
	     from app_menu_profile
	 order by app_menu_profile_name

END







GO
GRANT EXECUTE ON  [dbo].[cbmsAppMenuProfile_GetAll] TO [CBMSApplication]
GO
