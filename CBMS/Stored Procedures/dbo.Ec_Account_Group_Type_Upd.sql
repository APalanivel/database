SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Ec_Account_Group_Type_Upd        
                
Description:                
		This sproc to dto update the group types.        
                             
 Input Parameters:                
    Name						DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
	@Ec_Account_Group_Type_Id	INT
	@Country_Id					INT  
	@State_Id					INT
	@Commodity_Id				INT
      
 Output Parameters:                      
    Name					  DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
----------------------------------------------------------------------------------------     
 
  DECLARE  @tvp_Ec_Account_Group_Type dbo.tvp_Ec_Account_Group_Type
	INSERT @tvp_Ec_Account_Group_Type
		SELECT 36,'Group Three' 
		 
  
  BEGIN TRAN 
  
 SELECT * FROM dbo.Ec_Account_Group_Type eagt WHERE State_Id =1  and Commodity_Id = 290
  EXEC dbo.Ec_Account_Group_Type_Upd 
      @State_Id = 1
     ,@Commodity_Id = 290
     ,@tvp_Ec_Account_Group_Type = @tvp_Ec_Account_Group_Type
     ,@User_Info_Id = 49
 SELECT * FROM dbo.Ec_Account_Group_Type eagt WHERE State_Id =1  and Commodity_Id = 290

  ROLLBACK TRAN
     
       
   
Author Initials:                
    Initials		Name                
----------------------------------------------------------------------------------------                  
	NR				Narayana Reddy                 
 Modifications:                
    Initials        Date			Modification                
----------------------------------------------------------------------------------------                  
    NR				2016-11-15		Created For MAINT-4563.           
               
******/   
CREATE PROCEDURE [dbo].[Ec_Account_Group_Type_Upd]
      ( 
       @State_Id INT
      ,@Commodity_Id INT
      ,@tvp_Ec_Account_Group_Type tvp_Ec_Account_Group_Type READONLY
      ,@User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 
      
      DECLARE @tvp_Ec_Account_Group_Type_insert tvp_Ec_Account_Group_Type
     
      DECLARE @Ec_Account_Group_Type_To_Delete TABLE
            ( 
             Ec_Account_Group_Type_Id INT )
            
      INSERT      INTO @Ec_Account_Group_Type_To_Delete
                  ( 
                   Ec_Account_Group_Type_Id )
                  SELECT
                        Ec_Account_Group_Type_Id
                  FROM
                        dbo.Ec_Account_Group_Type eagt
                  WHERE
                        eagt.State_Id = @State_Id
                        AND eagt.Commodity_Id = @Commodity_Id
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          @tvp_Ec_Account_Group_Type tvp
                                         WHERE
                                          tvp.Ec_Account_Group_Type_Id = eagt.Ec_Account_Group_Type_Id )  
      
      
      --Deleteing the accounts for that group type
      DELETE
            ecaga
      FROM
            dbo.Ec_Client_Account_Group_Account ecaga
            INNER JOIN dbo.Ec_Client_Account_Group ecag
                  ON ecaga.Ec_Client_Account_Group_Id = ecag.Ec_Client_Account_Group_Id
            INNER JOIN @Ec_Account_Group_Type_To_Delete eagttd
                  ON ecag.Ec_Account_Group_Type_Id = eagttd.Ec_Account_Group_Type_Id
      
      DELETE
            ecag
      FROM
            dbo.Ec_Client_Account_Group ecag
            INNER JOIN @Ec_Account_Group_Type_To_Delete eagttd
                  ON ecag.Ec_Account_Group_Type_Id = eagttd.Ec_Account_Group_Type_Id
      
      
      DELETE
            eagt
      FROM
            dbo.Ec_Account_Group_Type eagt
            INNER JOIN @Ec_Account_Group_Type_To_Delete eagttd
                  ON eagt.Ec_Account_Group_Type_Id = eagttd.Ec_Account_Group_Type_Id       
      
      
      
      UPDATE
            eagt
      SET   
            eagt.Group_Type_Name = tvp.Group_Type_Name
           ,Updated_User_Id = @User_Info_Id
           ,Last_Change_Ts = GETDATE()
      FROM
            dbo.Ec_Account_Group_Type eagt
            INNER JOIN @tvp_Ec_Account_Group_Type tvp
                  ON tvp.Ec_Account_Group_Type_Id = eagt.Ec_Account_Group_Type_Id
                  
                  
      INSERT      INTO @tvp_Ec_Account_Group_Type_insert
                  SELECT
                        Ec_Account_Group_Type_Id
                       ,Group_Type_Name
                  FROM
                        @tvp_Ec_Account_Group_Type
                  WHERE
                        Ec_Account_Group_Type_Id IS NULL 
      
      EXEC dbo.Ec_Account_Group_Type_Ins 
            @State_Id = @State_Id
           ,@Commodity_Id = @Commodity_Id
           ,@tvp_Ec_Account_Group_Type = @tvp_Ec_Account_Group_Type_insert
           ,@User_Info_Id = @User_Info_Id 
                  
      
      
            
END
      



;
GO
GRANT EXECUTE ON  [dbo].[Ec_Account_Group_Type_Upd] TO [CBMSApplication]
GO
