SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Utility_Dtl_Election_del

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Utility_Dtl_Election_Id	INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN
		EXEC Utility_Dtl_Election_del 11
	ROLLBACK TRAN
	
	SELECT * FROM Utility_Dtl_Election
		
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	SKA			01/12/2011	Created
******/
    
CREATE PROC dbo.Utility_Dtl_Election_del    
      (    
       @Utility_Dtl_Election_Id INT    
 )    
AS     
BEGIN    
     
      SET nocount ON ;    
    
      DELETE FROM     
            dbo.Utility_Dtl_Election    
      WHERE    
            Utility_Dtl_Election_Id = @Utility_Dtl_Election_Id    
END 
GO
GRANT EXECUTE ON  [dbo].[Utility_Dtl_Election_del] TO [CBMSApplication]
GO
