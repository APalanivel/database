SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_SET_PRODUCTS_TERMS_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@productTermMapId int,
@selectedPriceProductId int,
@accountTermId int

                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- TEST 2
---- Test an insert
--EXEC [dbo].[SR_RFP_SET_PRODUCTS_TERMS_P] 
--@productTermMapId = 0,
--@selectedPriceProductId = 35,
--@accountTermId = 24396


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[SR_RFP_SET_PRODUCTS_TERMS_P] 

@productTermMapId int,
@selectedPriceProductId int,
@accountTermId int


as
	
set nocount on
declare @requirementBidId int
declare @newRequirementBidId int
declare @newIdentity int
declare @newSummitBidId int
declare @newSupplierCommentsId int
declare @newSupplierServiceId int
declare @newProductName varchar(200)
declare @newDeliveryFuelId int
declare @newBalancingToleranceId int
declare @newRiskManagementId int
declare @newAlternateFuelId int
declare @newPricingScopeId int
declare @newMiscPowerId int
declare @newBidId int
declare @newProductTermMapId int


if @productTermMapId=0 -- do insert operation

begin

		SELECT @requirementBidId =  SR_RFP_BID_REQUIREMENTS_ID FROM SR_RFP_SELECTED_PRODUCTS
		WHERE SR_RFP_SELECTED_PRODUCTS_ID=@selectedPriceProductId

		if @requirementBidId>0
		begin
			
			INSERT INTO SR_RFP_BID_REQUIREMENTS 
			(
				DELIVERY_POINT,
			    TRANSPORTATION_LEVEL_TYPE_ID,
				NOMINATION_TYPE_ID,
				BALANCING_TYPE_ID,
				COMMENTS,
				DELIVERY_POINT_POWER_TYPE_ID,
				SERVICE_LEVEL_POWER_TYPE_ID
			)
			    SELECT
				DELIVERY_POINT,
			        TRANSPORTATION_LEVEL_TYPE_ID,
				NOMINATION_TYPE_ID,
				BALANCING_TYPE_ID,
				COMMENTS,
				DELIVERY_POINT_POWER_TYPE_ID,
				SERVICE_LEVEL_POWER_TYPE_ID
			   FROM
				SR_RFP_BID_REQUIREMENTS
			   WHERE
				SR_RFP_BID_REQUIREMENTS_ID=@requirementBidId

  		    select @newRequirementBidId=SCOPE_IDENTITY()

		end
		
		else
		begin
				INSERT INTO SR_RFP_BID_REQUIREMENTS (
				DELIVERY_POINT,
			        TRANSPORTATION_LEVEL_TYPE_ID,
				NOMINATION_TYPE_ID,
				BALANCING_TYPE_ID,
				COMMENTS,
				DELIVERY_POINT_POWER_TYPE_ID,
				SERVICE_LEVEL_POWER_TYPE_ID) 
				values
				(null,null,null,null,null,null,null)
			    select @newRequirementBidId=SCOPE_IDENTITY()

	
			    
		end



		INSERT INTO SR_RFP_BID 
		(
		    SR_RFP_BID_REQUIREMENTS_ID,
		    IS_SUB_PRODUCT
		)  
		VALUES
		(
		   @newRequirementBidId,
		   0
		)	
		SELECT @newIdentity=SCOPE_IDENTITY()


			INSERT INTO SR_RFP_TERM_PRODUCT_MAP 
			(
				SR_RFP_ACCOUNT_TERM_ID,
				SR_RFP_SELECTED_PRODUCTS_ID,
				SR_RFP_BID_ID
			)  
			VALUES
			(
				@accountTermId,
				@selectedPriceProductId,
				@newIdentity
			)

		select @newProductTermMapid=SCOPE_IDENTITY()


end

else if @productTermMapId>0 and @selectedPriceProductId>0 and @accountTermId>0 -- do delete operation

begin
		DELETE FROM 
		SR_RFP_TERM_PRODUCT_MAP 
		WHERE 
		SR_RFP_SELECTED_PRODUCTS_ID=@selectedPriceProductId AND
		SR_RFP_ACCOUNT_TERM_ID=@accountTermId

end
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_SET_PRODUCTS_TERMS_P] TO [CBMSApplication]
GO
