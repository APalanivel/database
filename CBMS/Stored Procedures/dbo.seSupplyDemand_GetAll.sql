SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  Procedure dbo.seSupplyDemand_GetAll
 ( @DataTypeId int = 1 )
As
BEGIN
	set nocount on
	    select ReportYear
					, case When [ReportMonth] < 10 then '0' + [ReportMonth] else [ReportMonth] End As ReportMonth
					, UnaccountedFor
					, Consumption
					, NetImports
					, StorageWithdrawal
					, DryProduction
					, SupplementalProduction
					, DryProduction + NetImports 'Supply'
					, PlantFuel
					, Vehicles
					, Industrial
					, Commercial
					, Residential
					, Transportation
					, PowerGeneration
	     from seSupplyDemand
			where DataTypeId = @DataTypeId
	 order by ReportYear desc
					, ReportMonth desc
END
GO
GRANT EXECUTE ON  [dbo].[seSupplyDemand_GetAll] TO [CBMSApplication]
GO
