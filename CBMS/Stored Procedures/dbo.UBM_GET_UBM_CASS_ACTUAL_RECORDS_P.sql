SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UBM_GET_UBM_CASS_ACTUAL_RECORDS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@tableName     	varchar(60)	          	
	@masterLogId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.UBM_GET_UBM_CASS_ACTUAL_RECORDS_P 1,1,'UBM_CASS_BILL',7877
	EXEC dbo.UBM_GET_UBM_CASS_ACTUAL_RECORDS_P 1,1,'UBM_CASS_BILL',7868

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan G

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	HG			9/23/2010	Double quotes used to annotate text replaced by Single quote to set	quoted identifier on

******/

CREATE PROCEDURE dbo.UBM_GET_UBM_CASS_ACTUAL_RECORDS_P
	@userId varchar(10),
	@sessionId varchar(20),
	@tableName varchar(60),	
	@masterLogId int

AS
BEGIN

	SET NOCOUNT ON

	DECLARE @SQLStatement varchar(1000)
 
	SELECT @SQLStatement = '
								SELECT 
									COUNT(1) 
								FROM ' + 
									@tableName + ' 
								WHERE 
									UBM_BATCH_MASTER_LOG_ID = ' + STR(@masterLogId)

	EXEC(@SQLStatement)

END	
GO
GRANT EXECUTE ON  [dbo].[UBM_GET_UBM_CASS_ACTUAL_RECORDS_P] TO [CBMSApplication]
GO
