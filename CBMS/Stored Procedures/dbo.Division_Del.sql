SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME:        Division_Del

DESCRIPTION:          

          

INPUT PARAMETERS:          

 Name					DataType			Default		Description          
 -------------------------------------------------------------------------------------------------------    
  @Message				XML								XML string of 

 ,@Conversation_Handle  UNIQUEIDENTIFER					Conversation Handle that sent the message           

          

OUTPUT PARAMETERS:          

 Name      DataType  Default Description          
 ------------------------------------------------------------------------------------------------------          

          

 USAGE EXAMPLES:          
 ------------------------------------------------------------------------------------------------------    

 This sp should be executed from SQL Server Service Broker   
          
 <Delete_Division>
 <Division_Id>12270133</Division_Id>
 </Delete_Division>
  
 AUTHOR INITIALS:          

 Initials	Name          
 ------------------------------------------------------------------------------------------------------    
 DSC		Kaushik

 
MODIFICATIONS          

 Initials		Date			Modification          
------------------------------------------------------------------------------------------------------          
 Kaushik		2014-06-23		Created   

******/          
CREATE PROCEDURE [dbo].[Division_Del]
      ( 
       @Message XML
      ,@Conversation_Handle UNIQUEIDENTIFIER )
AS 
BEGIN

      SET NOCOUNT ON;


	  
      DECLARE
            @Division_Id INT
           ,@User_Info_Id INT 

      SELECT
            @Division_Id = cng.ch.value('Division_Id[1]', 'INT')
      FROM
            @Message.nodes('/Delete_Division') cng ( ch ) 

      SELECT
            @User_Info_Id = ui.User_Info_Id
      FROM
            dbo.USER_INFO AS ui
      WHERE
            ui.username = 'conversion'


      EXECUTE Delete_Div_From_Roles 
            @Division_Id

      EXECUTE Division_History_Del_By_Division_Id 
            @Division_Id
           ,@User_Info_Id;
            END CONVERSATION @Conversation_Handle;
END 



;
GO
GRANT EXECUTE ON  [dbo].[Division_Del] TO [CBMSApplication]
GO
