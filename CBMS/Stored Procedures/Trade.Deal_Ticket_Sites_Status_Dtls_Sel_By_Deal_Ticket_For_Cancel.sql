SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.Deal_Ticket_Sites_Status_Dtls_Sel_By_Deal_Ticket_For_Cancel                        
                          
Description:                          
        To get site's hedge configurations    
                          
Input Parameters:                          
    Name    DataType        Default     Description                            
--------------------------------------------------------------------------------    
 @Client_Id   INT    
    @Commodity_Id  INT    
    @Hedge_Type  INT    
    @Start_Dt DATE    
    @End_Dt  DATE    
    @Contract_Id  VARCHAR(MAX)    
    @Site_Id   INT    NULL    
                          
 Output Parameters:                                
 Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
                        
Usage Examples:                              
--------------------------------------------------------------------------------    
   
	EXEC Trade.Deal_Ticket_Sites_Status_Dtls_Sel_By_Deal_Ticket_For_Cancel  291,1  
	EXEC Trade.Deal_Ticket_Sites_Status_Dtls_Sel_By_Deal_Ticket_For_Cancel  291,1,'18508'  
	EXEC Trade.Deal_Ticket_Sites_Status_Dtls_Sel_By_Deal_Ticket_For_Cancel  291,1,'18520'  
	EXEC Trade.Deal_Ticket_Sites_Status_Dtls_Sel_By_Deal_Ticket_For_Cancel  288,16  
	EXEC Trade.Deal_Ticket_Sites_Status_Dtls_Sel_By_Deal_Ticket_For_Cancel  297,16  
    
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR          24-01-2019  Created GRM    
	RR          13-11-2019  GRM-1566 - Modified to use trade number to check cancel and used single status for a site to avoid duplicates                
******          24-01-2019  Created GRM    
                         
******/
CREATE PROCEDURE [Trade].[Deal_Ticket_Sites_Status_Dtls_Sel_By_Deal_Ticket_For_Cancel]
      ( 
       @Deal_Ticket_Id INT
      ,@Workflow_Task_Status_Map_Id INT
      ,@Client_Hier_Id VARCHAR(MAX) = NULL
      ,@Workflow_Status_Id INT = NULL )
AS 
BEGIN

        SET NOCOUNT ON;


        SELECT
            ch.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
            , ws.Workflow_Status_Name
            , trd.Trade_Number AS Trade_Id
            , CASE WHEN ws.Workflow_Status_Name = 'Canceled' THEN chws.Workflow_Status_Comment
                  ELSE NULL
              END AS Cancellation_Comments
            , CASE WHEN ws.Workflow_Status_Name = 'Canceled' THEN chws.Last_Change_Ts
                  ELSE NULL
              END AS Cancellation_Ts
            , CASE WHEN cwsm.Workflow_Status_Map_Id IS NULL
                        AND ws.Workflow_Status_Name = 'Canceled' THEN -1
                  WHEN cwsm.Workflow_Status_Map_Id IS NULL
                       AND  ws.Workflow_Status_Name <> 'Canceled' THEN 0
                  WHEN cwsm.Workflow_Status_Map_Id IS NOT NULL THEN 1
              END AS Is_Cancel
            , CASE WHEN cwsm.Workflow_Status_Map_Id IS NULL
                        AND ws.Workflow_Status_Name = 'Canceled' THEN 'Canceled'
                  WHEN (   (   cwsm.Workflow_Status_Map_Id IS NULL
                               AND  ws.Workflow_Status_Name <> 'Canceled')
                           OR   (trd.Trade_Number IS NOT NULL)) THEN 'Cannot Cancel'
                  WHEN cwsm.Workflow_Status_Map_Id IS NOT NULL THEN 'Cancel'
              END AS Cancel_Task_Label
            , CASE WHEN ws.Workflow_Status_Name = 'Canceled' THEN chws.CBMS_Image_Id
                  ELSE NULL
              END AS CBMS_Image_Id
            --, chws.Workflow_Status_Map_Id
            , cwsm.Workflow_Status_Map_Id
            , CASE WHEN ws.Workflow_Status_Name = 'Order Placed' THEN 1
                  WHEN ws.Workflow_Status_Name = 'Order Executed' THEN 2
                  WHEN ws.Workflow_Status_Name = 'Completed' THEN 3
                  WHEN ws.Workflow_Status_Name = 'Canceled' THEN 4
                  WHEN ws.Workflow_Status_Name = 'Expired' THEN 5
                  WHEN ws.Workflow_Status_Name = 'Bid Emails Generated' THEN 6
                  WHEN ws.Workflow_Status_Name = 'Marked to cancel' THEN 7
                  WHEN ws.Workflow_Status_Name = 'Assigned to trader' THEN 8
                  WHEN ws.Workflow_Status_Name = 'Back to CM' THEN 9
                  WHEN ws.Workflow_Status_Name = 'Not supported' THEN 10
                  WHEN ws.Workflow_Status_Name = 'Ready to trade' THEN 11
                  ELSE 0
              END AS Rnk
        INTO
            #Sites
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dt.Deal_Ticket_Id = dtch.Deal_Ticket_Id
            INNER JOIN Core.Client_Hier ch
                ON dtch.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
            INNER JOIN Trade.Workflow_Status_Map wsm
                ON chws.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
            INNER JOIN Trade.Workflow_Status ws
                ON wsm.Workflow_Status_Id = ws.Workflow_Status_Id
            LEFT JOIN(Trade.Workflow_Status_Map cwsm
                      INNER JOIN Trade.Workflow_Task_Status_Map cwtsm
                          ON cwsm.Workflow_Status_Map_Id = cwtsm.Workflow_Status_Map_Id
                             AND cwtsm.Workflow_Task_Id = @Workflow_Task_Status_Map_Id)
                ON dt.Workflow_Id = cwsm.Workflow_Id
                   AND  chws.Workflow_Status_Map_Id = cwsm.Workflow_Status_Map_Id
            LEFT JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl trd
                ON trd.Deal_Ticket_Id = dt.Deal_Ticket_Id
                   AND  trd.Client_Hier_Id = dtch.Client_Hier_Id
        WHERE
            dt.Deal_Ticket_Id = @Deal_Ticket_Id
            AND chws.Is_Active = 1
            AND (   @Workflow_Status_Id IS NULL
                    OR  ws.Workflow_Status_Id = @Workflow_Status_Id)
        GROUP BY
            ch.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
            , ws.Workflow_Status_Name
            , CASE WHEN ws.Workflow_Status_Name = 'Canceled' THEN chws.Last_Change_Ts
                  ELSE NULL
              END
            , chws.Workflow_Status_Comment
            , chws.CBMS_Image_Id
            , dt.Deal_Ticket_Id
            --, chws.Workflow_Status_Map_Id
            , cwsm.Workflow_Status_Map_Id
            , chws.CBMS_Image_Id
            , trd.Trade_Number;




        SELECT
            ss.Client_Hier_Id
            , ss.Site_Id
            , ss.Site_name
            , ss.Workflow_Status_Name
            , ss.Trade_Id
            , ss.Cancellation_Comments
            , ss.Cancellation_Ts
            , ss.Is_Cancel
            , ss.Cancel_Task_Label
            , ss.CBMS_Image_Id
            , ss.Workflow_Status_Map_Id
            , ss.Rnk
        FROM
            #Sites ss
            INNER JOIN
            (   SELECT
                    s.Client_Hier_Id
                    , MIN(s.Rnk) AS Rnk
                FROM
                    #Sites s
                GROUP BY
                    s.Client_Hier_Id) ss1
                ON ss1.Client_Hier_Id = ss.Client_Hier_Id
                   AND  ss1.Rnk = ss.Rnk;

        DROP TABLE #Sites;

    END;


GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Sites_Status_Dtls_Sel_By_Deal_Ticket_For_Cancel] TO [CBMSApplication]
GO
