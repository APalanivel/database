
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [DBO].[Account_Del]  

DESCRIPTION:

	To Delete given Account_id

INPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
@Account_ID		INT
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        

	BEGIN TRAN  

		EXEC dbo.Account_Del 289653

	ROLLBACK TRAN

     
AUTHOR INITIALS:          
	INITIALS	NAME          
------------------------------------------------------------          
	PNR			PANDARINATH
	RR			Raghu Reddy
          
MODIFICATIONS           
	INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
	PNR			25-MAY-10	CREATED   
	RR			2017-10-06	MAINT-5940 - Throwing FK reference error on delelting GROUP_BILLING_NUMBER(dbo.ACCOUNT_GROUP) as ACCOUNT_GROUP_ID 
							referenced by dbo.ACCOUNT table. Moved the script block to dbo.Account_Del script
							Deletes the GROUP_BILLING_NUMBER only if no other account is referencing it  
*/  

CREATE PROCEDURE [dbo].[Account_Del] ( @Account_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @ACCOUNT_GROUP_ID INT
      
      SELECT
            @ACCOUNT_GROUP_ID = ACCOUNT_GROUP_ID
      FROM
            dbo.ACCOUNT
      WHERE
            ACCOUNT_ID = @Account_Id
      
      DELETE FROM
            dbo.ACCOUNT
      WHERE
            ACCOUNT_ID = @Account_Id
            
      DELETE
            ag
      FROM
            dbo.ACCOUNT_GROUP ag
      WHERE
            ag.ACCOUNT_GROUP_ID = @ACCOUNT_GROUP_ID
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              dbo.ACCOUNT acc
                             WHERE
                              acc.ACCOUNT_GROUP_ID = ag.ACCOUNT_GROUP_ID )

END

;
GO

GRANT EXECUTE ON  [dbo].[Account_Del] TO [CBMSApplication]
GO
