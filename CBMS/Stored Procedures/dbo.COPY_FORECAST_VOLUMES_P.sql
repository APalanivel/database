SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.COPY_FORECAST_VOLUMES_P
@userId varchar(10),
@sessionId varchar(20),
@asOfDate datetime,
@clientId int
AS
declare @cnt int,@rm_forecast_volume_id int, @site_id int,
	@division_id int, @volume int, @month_identifier datetime,
	@volume_type_id int, @maxAsOfDate datetime, @forecast_year int

select @cnt = count(rm_forecast_volume_id) from rm_forecast_volume 
	where forecast_as_of_date= @asOfDate and client_id = @clientId

IF(@cnt = 0)
BEGIN
	select @maxAsOfDate = MAX(forecast_as_of_date) from rm_forecast_volume 
					       where client_id = @clientId	
	DECLARE C_FORECAST CURSOR FAST_FORWARD
	FOR
	SELECT forecast_year FROM rm_forecast_volume WHERE client_id = @clientId AND forecast_as_of_date = @maxAsOfDate	
	OPEN C_FORECAST
	
	FETCH NEXT FROM C_FORECAST INTO @forecast_year
	WHILE (@@fetch_status <> -1)
	BEGIN
	IF (@@fetch_status <> -2)
	BEGIN
		insert into rm_forecast_volume (client_id, forecast_year, forecast_as_of_date)
			values(@clientId, @forecast_year, @asOfDate)

	END
	FETCH NEXT FROM C_FORECAST INTO @forecast_year
	END
	CLOSE C_FORECAST
	DEALLOCATE C_FORECAST

	DECLARE C_FORECAST_DETAILS CURSOR FAST_FORWARD
	FOR 
	SELECT 
		forecast.forecast_year,
		details.site_id,
		details.division_id,
		details.volume,
		details.month_identifier, 
		details.volume_type_id

	FROM  
		rm_forecast_volume_details details, 
		rm_forecast_volume forecast,
		site sit
	WHERE 
		details.site_id = sit.site_id
		and details.rm_forecast_volume_id in (select rm_forecast_volume_id from rm_forecast_volume 
						      where forecast_as_of_date=@maxAsOfDate and client_id = @clientId)
		and forecast.rm_forecast_volume_id = details.rm_forecast_volume_id
	ORDER BY details.site_id, details.month_identifier
	
	OPEN C_FORECAST_DETAILS
	
	FETCH NEXT FROM C_FORECAST_DETAILS INTO @forecast_year, @site_id, @division_id, @volume,
				 @month_identifier, @volume_type_id
	WHILE (@@fetch_status <> -1)
	BEGIN
	IF (@@fetch_status <> -2)
	BEGIN

		SELECT @rm_forecast_volume_id = rm_forecast_volume_id FROM rm_forecast_volume
			WHERE client_id = @clientId AND forecast_as_of_date = @asOfDate AND forecast_year = @forecast_year	
		INSERT INTO rm_forecast_volume_details 
				(rm_forecast_volume_id, site_id, division_id,
				 volume, month_identifier, volume_type_id)
			VALUES  (@rm_forecast_volume_id, @site_id, @division_id,
				 @volume, @month_identifier, @volume_type_id)

	END
	FETCH NEXT FROM C_FORECAST_DETAILS INTO @forecast_year,@site_id, @division_id, @volume, 
				 @month_identifier, @volume_type_id
	END
	
	CLOSE C_FORECAST_DETAILS
	DEALLOCATE C_FORECAST_DETAILS

END
GO
GRANT EXECUTE ON  [dbo].[COPY_FORECAST_VOLUMES_P] TO [CBMSApplication]
GO
