SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[SR_RFP_Account_Del]  
     
DESCRIPTION: 
	It Deletes SR RFP Account for Selected 
						SR RFP Account Id.
      
INPUT PARAMETERS:          
NAME			  DATATYPE	DEFAULT		DESCRIPTION          
-------------------------------------------------------------------          
@Sr_Rfp_Account_Id		  INT	

   
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------
  
	Begin Tran
		EXEC SR_RFP_Account_Del  0
	Rollback Tran

	-- No valid records present in TK1 dev environment which can be deleted through this sp.

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR		    28-MAY-10	CREATED

*/
CREATE PROCEDURE dbo.SR_RFP_Account_Del
   (
    @Sr_Rfp_Account_Id INT
   )
AS
BEGIN

    SET NOCOUNT ON;

    DELETE
   	FROM
		dbo.SR_RFP_ACCOUNT
	WHERE
		SR_RFP_ACCOUNT_ID = @Sr_Rfp_Account_Id

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_Account_Del] TO [CBMSApplication]
GO
