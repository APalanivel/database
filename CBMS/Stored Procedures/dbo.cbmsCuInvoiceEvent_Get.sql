SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[cbmsCuInvoiceEvent_Get]
	( @MyAccountId int
	, @cu_invoice_event_id int
	)
AS
BEGIN

	   select cu_invoice_event_id
		, cu_invoice_id
		, event_date
		, event_by_id
		, event_description
	     from cu_invoice_event with (nolock)
	    where cu_invoice_event_id = @cu_invoice_event_id

END



GO
GRANT EXECUTE ON  [dbo].[cbmsCuInvoiceEvent_Get] TO [CBMSApplication]
GO
