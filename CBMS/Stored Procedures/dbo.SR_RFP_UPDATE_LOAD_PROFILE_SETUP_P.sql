SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_UPDATE_LOAD_PROFILE_SETUP_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
	@user_id                varchar(10),
	@session_id             varchar(20),
	@additional_rows        int,
	@month_selector_type_id int,
	@vendor_id              int,
	@rfp_id                 int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
--exec dbo.SR_RFP_UPDATE_LOAD_PROFILE_SETUP_P '1', '1', 0, 982, 274, 70

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_UPDATE_LOAD_PROFILE_SETUP_P
	@user_id varchar(10),
	@session_id varchar(20),
	@additional_rows int,
	@month_selector_type_id int,
	@vendor_id int,
	@rfp_id int
	AS
	
set nocount on
	
	DECLARE C_RFP_ACCOUNT CURSOR FAST_FORWARD
	FOR
		select 	sr_rfp_account_id 
		from 	sr_rfp_account rfp_acc(nolock),
			account acc(nolock)

		where	acc.vendor_id = @vendor_id
			and rfp_acc.account_id = acc.account_id
			and rfp_acc.is_deleted = 0
			and rfp_acc.sr_rfp_id = @rfp_id
			

	declare @sr_rfp_account_id int
	OPEN C_RFP_ACCOUNT
	
	FETCH NEXT FROM C_RFP_ACCOUNT INTO @sr_rfp_account_id
	WHILE (@@fetch_status <> -1)
	BEGIN
		IF (@@fetch_status <> -2)
		BEGIN

			if (select count(setup.sr_rfp_load_profile_setup_id) 
				from sr_rfp_load_profile_setup setup(nolock), sr_rfp_account rfp_account(nolock) 
				where setup.sr_rfp_id = @rfp_id 
				and setup.sr_rfp_account_id = @sr_rfp_account_id
				and rfp_account.sr_rfp_account_id = setup.sr_rfp_account_id
				and rfp_account.is_deleted = 0
				) > 0
			begin
				update	sr_rfp_load_profile_setup 
				set 	additional_rows = @additional_rows,
					updated_month_selector_type_id = @month_selector_type_id
				where 	sr_rfp_id = @rfp_id
					and sr_rfp_account_id = @sr_rfp_account_id
			end
			else
			begin
				insert into sr_rfp_load_profile_setup (sr_rfp_account_id, sr_rfp_id, additional_rows, month_selector_type_id, updated_month_selector_type_id) 
				values (@sr_rfp_account_id, @rfp_id, @additional_rows, @month_selector_type_id, @month_selector_type_id)
		
			end

		END
		FETCH NEXT FROM C_RFP_ACCOUNT INTO @sr_rfp_account_id
	END
	CLOSE C_RFP_ACCOUNT
	DEALLOCATE C_RFP_ACCOUNT
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_UPDATE_LOAD_PROFILE_SETUP_P] TO [CBMSApplication]
GO
