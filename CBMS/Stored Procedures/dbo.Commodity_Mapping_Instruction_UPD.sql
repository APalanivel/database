SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
/******      
NAME:      
 dbo.Commodity_Mapping_Instruction_UPD
  
DESCRIPTION:      
	Used to update the COMMODITY table for the column 'Service_Mapping_Instructions'
  
INPUT PARAMETERS:      
Name							DataType Default Description      
------------------------------------------------------------      
@Commodity_Id					INT 
@Service_Mapping_Instructions	VARCHAR(MAX)     
            
OUTPUT PARAMETERS:      
Name   DataType  Default Description      
------------------------------------------------------------

USAGE EXAMPLES:      
------------------------------------------------------------    
  
	EXEC dbo.Commodity_Mapping_Instruction_UPD  1,'Test'
  
AUTHOR INITIALS:      
Initials	Name      
------------------------------------------------------------      
PNR			Pandarinath

MODIFICATIONS       
Initials	Date		Modification      
------------------------------------------------------------      
PNR			01/04/2011	Created as part of CBMS Client Service Mapping.

******/  

CREATE PROCEDURE dbo.Commodity_Mapping_Instruction_UPD
	 @Commodity_Id					INT
	,@Service_Mapping_Instructions	VARCHAR(MAX) = NULL
AS    
BEGIN    
  
	SET NOCOUNT ON;    

	UPDATE 
		dbo.COMMODITY
	SET 
		Service_Mapping_Instructions = @Service_Mapping_Instructions
	WHERE 
		COMMODITY_ID = @Commodity_Id      
    
END
GO
GRANT EXECUTE ON  [dbo].[Commodity_Mapping_Instruction_UPD] TO [CBMSApplication]
GO
