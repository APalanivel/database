
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.EC_Calc_Val_Del       
              
Description:              
			This sproc to delete  the calc details for a Given id's.      
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
	@EC_Calc_Val_Id						INT					                
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

   BEGIN TRAN
   SELECT * FROM dbo.EC_Calc_Val ecv WHERE ecv.EC_Calc_Val_Id=13
   Exec dbo.EC_Calc_Val_Del  13
   SELECT * FROM dbo.EC_Calc_Val ecv WHERE ecv.EC_Calc_Val_Id=13
   ROLLBACK TRAN
   
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy 
	RKV             Ravi Kumar Vegesna              
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2015-04-22		Created For AS400.
    RKV             2015-11-04      Added the code to delete the given Ec_Calc_Val_ids from the two child tables 
									Ec_Calc_Val_Bucket_Sub_Bucket_Map,Ec_Calc_Val_Bucket_Map.
									
             
******/ 
CREATE PROCEDURE [dbo].[EC_Calc_Val_Del] ( @EC_Calc_Val_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 

      BEGIN TRY        
            BEGIN TRANSACTION
            
            DELETE
                  ecvsbm
            FROM
                  dbo.Ec_Calc_Val_Bucket_Sub_Bucket_Map ecvsbm
                  INNER JOIN dbo.Ec_Calc_Val_Bucket_Map ecvbm
                        ON ecvsbm.Ec_Calc_Val_Bucket_Map_Id = ecvbm.Ec_Calc_Val_Bucket_Map_Id
            WHERE
                  ecvbm.Ec_Calc_Val_Id = @EC_Calc_Val_Id
               
            DELETE
                  ecvbm
            FROM
                  dbo.Ec_Calc_Val_Bucket_Map ecvbm
            WHERE
                  ecvbm.Ec_Calc_Val_Id = @EC_Calc_Val_Id
            
            
            DELETE
                  ecvma
            FROM
                  dbo.EC_Calc_Val_Meter_Attribute ecvma
            WHERE
                  ecvma.EC_Calc_Val_Id = @EC_Calc_Val_Id

            DELETE
                  ecv
            FROM
                  dbo.EC_Calc_Val ecv
            WHERE
                  ecv.EC_Calc_Val_Id = @EC_Calc_Val_Id
      
      
            COMMIT TRANSACTION         
      END TRY                  
      BEGIN CATCH                  
            IF @@TRANCOUNT > 0 
                  BEGIN      
                        ROLLBACK TRANSACTION      
                  END               
            EXEC usp_RethrowError                  
      END CATCH   
                  
END;
;
GO

GRANT EXECUTE ON  [dbo].[EC_Calc_Val_Del] TO [CBMSApplication]
GO
