SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sourcing_Analyst_Manager_Ins_Upd]
           
DESCRIPTION:             
			To add or remove Sourcing Analyst Manager mapping
			
INPUT PARAMETERS:            
	Name			DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Country_Id		INT			NULL
    @Commodity_Id	INT			NULL
    @StartIndex		INT			1
    @EndIndex		INT			2147483647


OUTPUT PARAMETERS:
	Name			DataType		Default		Description  
---------------------------------------------------------------------------------  


 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
	EXEC dbo.Sourcing_Analyst_Manager_Dtl_Sel NULL,NULL,1,5

	BEGIN TRANSACTION
		SELECT * FROM dbo.SR_SA_SM_MAP WHERE SOURCING_ANALYST_ID = 1023
		EXEC dbo.Sourcing_Analyst_Manager_Ins_Upd 1023,NULL
		SELECT * FROM dbo.SR_SA_SM_MAP WHERE SOURCING_ANALYST_ID = 1023
	ROLLBACK TRANSACTION

	BEGIN TRANSACTION
		SELECT * FROM dbo.SR_SA_SM_MAP WHERE SOURCING_ANALYST_ID = 1023
		EXEC dbo.Sourcing_Analyst_Manager_Ins_Upd 1023,38
		SELECT * FROM dbo.SR_SA_SM_MAP WHERE SOURCING_ANALYST_ID = 1023
	ROLLBACK TRANSACTION

	EXEC dbo.Sourcing_Analyst_Manager_Unassigned_Dtl_Sel

	BEGIN TRANSACTION
		SELECT * FROM dbo.SR_SA_SM_MAP WHERE SOURCING_ANALYST_ID = 39616
		EXEC dbo.Sourcing_Analyst_Manager_Ins_Upd 39616,38
		SELECT * FROM dbo.SR_SA_SM_MAP WHERE SOURCING_ANALYST_ID = 39616
	ROLLBACK TRANSACTION


 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date			Modification
------------------------------------------------------------
	RR			2015-07-20		Global Sourcing - Created
								
******/
CREATE PROCEDURE [dbo].[Sourcing_Analyst_Manager_Ins_Upd]
      ( 
       @Sourcing_Analyst_Id INT
      ,@Sourcing_Manager_Id INT = NULL )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DELETE FROM
            dbo.SR_SA_SM_MAP
      WHERE
            SOURCING_ANALYST_ID = @Sourcing_Analyst_Id
      
      INSERT      INTO dbo.SR_SA_SM_MAP
                  ( 
                   SOURCING_ANALYST_ID
                  ,SOURCING_MANAGER_ID )
                  SELECT
                        @Sourcing_Analyst_Id AS SOURCING_ANALYST_ID
                       ,@Sourcing_Manager_Id AS SOURCING_MANAGER_ID
                  WHERE
                        @Sourcing_Manager_Id IS NOT NULL
            
                         
END;
;
GO
GRANT EXECUTE ON  [dbo].[Sourcing_Analyst_Manager_Ins_Upd] TO [CBMSApplication]
GO
