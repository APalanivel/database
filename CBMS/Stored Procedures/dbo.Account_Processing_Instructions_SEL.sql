SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********   
NAME:  dbo.Account_Processing_Instructions_SEL  
 
DESCRIPTION:  Used to select account processing instruction codes  

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    

    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:  
	
	Account_Processing_Instructions_SEL

------------------------------------------------------------  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
NK   Nageswara Rao Kosuri         


Initials Date  Modification  
------------------------------------------------------------  
NK	10/21/2009  Created


******/  


CREATE PROCEDURE dbo.Account_Processing_Instructions_SEL
		
AS
BEGIN
	
	SET NOCOUNT ON;
	
	
	
	SELECT  cd.Code_Id, cd.Code_Value 
	
	FROM Code cd
	JOIN Codeset cdset on cdset.codeset_id  = cd.codeset_id 
	and cdset.codeset_name = 'ProcessingInstruction'	

	
END
GO
GRANT EXECUTE ON  [dbo].[Account_Processing_Instructions_SEL] TO [CBMSApplication]
GO
