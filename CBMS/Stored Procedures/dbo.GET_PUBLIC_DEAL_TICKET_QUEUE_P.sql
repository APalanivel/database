
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_PUBLIC_DEAL_TICKET_QUEUE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@userId		       	varchar(10)	          	
	@sessionId		   	varchar(20)	          	
	@dealTicketNumber	varchar(200)	          	
	@clientId      		int       	          	
	@siteId        		int       	          	
	@startDate     		datetime  	          	
	@endDate       		datetime  	          	
	@dealStatusTypeId	int       	          	
	@startDateFlag 		bit       	          	
	@endDateFlag   		bit       	          	
	@currentlyWithId	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	
	EXEC dbo.GET_PUBLIC_DEAL_TICKET_QUEUE_P '1','1','',0,0,null,null,0,0,0,0

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath
	BCH			Balaraju Chalumri
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/28/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on	        	
	BCH			2013-03-25	Added Hedge_Type column to select list (MAIN - 1795).
******/
CREATE PROCEDURE dbo.GET_PUBLIC_DEAL_TICKET_QUEUE_P
      @userId VARCHAR(10)
     ,@sessionId VARCHAR(20)
     ,@dealTicketNumber VARCHAR(200)
     ,@clientId INT
     ,@siteId INT
     ,@startDate DATETIME
     ,@endDate DATETIME
     ,@dealStatusTypeId INT
     ,@startDateFlag BIT
     ,@endDateFlag BIT
     ,@currentlyWithId INT
AS 
BEGIN
	
      SET NOCOUNT ON;
	
      DECLARE @selectClause VARCHAR(8000)
      DECLARE @fromClause VARCHAR(8000)
      DECLARE @whereClause VARCHAR(8000)
      DECLARE @SQLStatement VARCHAR(8000)


	--basic selectClause
      SELECT
            @selectClause = ' hedge.ENTITY_NAME AS HEDGE_LEVEL_TYPE, trans.ENTITY_NAME AS DEAL_TRANSACTION_TYPE,  ' + 'rmdt.RM_DEAL_TICKET_ID, rmdt.DEAL_TICKET_NUMBER, c.CLIENT_NAME, ' + ' viewSite.SITE_NAME, ' + 'd.DIVISION_NAME, h_type.ENTITY_NAME AS Hedge_Type, e1.ENTITY_NAME AS DEAL_TYPE,  rmdt.DEAL_INITIATED_DATE, ' + 'rmdt.HEDGE_START_MONTH, rmdt.HEDGE_END_MONTH, ' + ' ''rma'' AS CURRENTLY_WITH_NAME,  ' + ' e2.ENTITY_NAME as DEAL_STATUS '

	--basic fromClause
      SELECT
            @fromClause = 'dbo.RM_DEAL_TICKET rmdt ' + ' LEFT JOIN dbo.SITE s ON (rmdt.SITE_ID = s.SITE_ID) ' + ' LEFT JOIN dbo.vwSiteName viewSite ON (s.SITE_ID = viewSite.SITE_ID) ' + 'LEFT JOIN dbo.DIVISION d ON (rmdt.DIVISION_ID = d.DIVISION_ID) ' + '
LEFT JOIN dbo.ENTITY trans ON (rmdt.DEAL_TRANSACTION_TYPE_ID = trans.ENTITY_ID) ' + 'LEFT JOIN dbo.ENTITY h_type ON (h_type.ENTITY_ID = rmdt.HEDGE_TYPE_ID)' + 'JOIN  dbo.ENTITY hedge ON (rmdt.HEDGE_LEVEL_TYPE_ID = hedge.ENTITY_ID) ' + 'JOIN  dbo.CLIENT c 
ON (rmdt.CLIENT_ID = c.CLIENT_ID) ' + 'JOIN  dbo.ENTITY e1 ON (rmdt.DEAL_TYPE_ID = e1.ENTITY_ID) ' + 'JOIN  dbo.RM_DEAL_TICKET_TRANSACTION_STATUS rmdtts ON (rmdt.RM_DEAL_TICKET_ID = rmdtts.RM_DEAL_TICKET_ID), ' + 'dbo.ENTITY e2, dbo.queue public_queue,  dbo.ENTITY queue_type '

	--basic whereClause
      SELECT
            @whereClause = ' rmdtts.DEAL_TRANSACTION_STATUS_TYPE_ID = e2.ENTITY_ID ' + ' and rmdt.QUEUE_ID= public_queue.QUEUE_ID' + ' and public_queue.queue_type_id= queue_type.entity_id ' + ' and queue_type.entity_type = 147 and queue_type.entity_name = ''Public'' ' + ' AND rmdtts.DEAL_TRANSACTION_DATE IN ' + '(SELECT MAX(DEAL_TRANSACTION_DATE) ' + 'FROM RM_DEAL_TICKET_TRANSACTION_STATUS  ' + 'WHERE RM_DEAL_TICKET_ID = rmdt.RM_DEAL_TICKET_ID) ' 

	-- where clause if only dealticket number is given
      IF @dealTicketNumber <> '' 
            BEGIN
                  SELECT
                        @whereClause = @whereClause + ' AND rmdt.DEAL_TICKET_NUMBER = '' + @dealTicketNumber + ''' 
            END 
	
	-- where clause if only client ID is given
      IF @clientId > 0 
            BEGIN
                  SELECT
                        @whereClause = @whereClause + ' AND rmdt.CLIENT_ID = ' + STR(@clientId) 
            END 

	-- where clause if only site ID is given
      IF @siteId > 0 
            BEGIN
                  SELECT
                        @whereClause = @whereClause + ' AND rmdt.SITE_ID = ' + STR(@siteId) 
            END 

	-- where clause if only start date is given
      IF @startDateFlag <> 0 
            BEGIN
                  SELECT
                        @whereClause = @whereClause + ' AND month(rmdt.HEDGE_START_MONTH) = month(' + '''' + CONVERT (VARCHAR(12), @startDate, 1) + '''' + ')' + ' AND year(rmdt.HEDGE_START_MONTH) = year(' + '''' + CONVERT (VARCHAR(12), @startDate, 1) + '''' + ')'
            END 

	-- where clause if only end date is given
      IF @endDateFlag <> 0 
            BEGIN
                  SELECT
                        @whereClause = @whereClause + ' AND month(rmdt.HEDGE_END_MONTH) = month(' + '''' + CONVERT (VARCHAR(12), @endDate, 1) + '''' + ')' + ' AND year(rmdt.HEDGE_END_MONTH) = year(' + '''' + CONVERT (VARCHAR(12), @endDate, 1) + '''' + ')'

            END 
	
	-- from and where clause if only deal status type ID is given
      IF @dealStatusTypeId > 0 
            BEGIN
                  SELECT
                        @whereClause = @whereClause + ' AND rmdtts.DEAL_TRANSACTION_STATUS_TYPE_ID = ' + STR(@dealStatusTypeId) 
            END 

     SELECT @SQLStatement =	'SELECT * FROM (SELECT ' + @selectClause + 'FROM ' + @fromClause + 'WHERE ' + 	@whereClause +
				' ) A WHERE A.DEAL_STATUS IN (''Client Approved'') ' + 
				' ORDER BY ' +	'A.RM_DEAL_TICKET_ID DESC ' 
				
      PRINT ( @SQLStatement )
	
      EXEC(@SQLStatement)
	
END

;
GO

GRANT EXECUTE ON  [dbo].[GET_PUBLIC_DEAL_TICKET_QUEUE_P] TO [CBMSApplication]
GO
