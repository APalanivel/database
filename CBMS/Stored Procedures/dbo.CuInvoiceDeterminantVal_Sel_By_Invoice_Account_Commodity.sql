SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 dbo.CuInvoiceDeterminantVal_Sel_By_Invoice_Account_Commodity   
    
DESCRIPTION:    
    
    
INPUT PARAMETERS:    
 Name				DataType  Default      Description    
---------------------------------------------------------------------------                  
 @cu_invoice_id		INT      
 @account_id		INT       NULL       
 @Commodity_Id		INT       NULL

OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
     
 
EXEC CuInvoiceDeterminantVal_Sel_By_Invoice_Account_Commodity
    35204801
    , 672993
    , 290



EXEC CuInvoiceDeterminantVal_Sel_By_Invoice_Account_Commodity
    35242086
    , 611927
    , 290

EXEC CuInvoiceDeterminantVal_Sel_By_Invoice_Account_Commodity
    35240999
    , 985427
    , 290

EXEC CuInvoiceDeterminantVal_Sel_By_Invoice_Account_Commodity @cu_invoice_id= 63412245
    ,@account_id= 1439838
    , @Commodity_Id= 290

EXEC CuInvoiceDeterminantVal_Sel_By_Invoice_Account_Commodity  @cu_invoice_id= 53499972
    ,@account_id= 1232320
    , @Commodity_Id= 290

EXEC CuInvoiceDeterminantVal_Sel_By_Invoice_Account_Commodity  @cu_invoice_id= 63692923
    ,@account_id= 1463603
    , @Commodity_Id= 290

EXEC CuInvoiceDeterminantVal_Sel_By_Invoice_Account_Commodity  @cu_invoice_id= 63692844
    ,@account_id= 1328915
    , @Commodity_Id= 290

EXEC CuInvoiceDeterminantVal_Sel_By_Invoice_Account_Commodity  @cu_invoice_id= 63690054
    ,@account_id= 53794
    , @Commodity_Id= 290

EXEC CuInvoiceDeterminantVal_Sel_By_Invoice_Account_Commodity
      @cu_invoice_id = 63484546
    , @account_id = 1406947
    , @Commodity_Id = 290;

EXEC CuInvoiceDeterminantVal_Sel_By_Invoice_Account_Commodity
      @cu_invoice_id = 63662453
    , @account_id = 1444107
    , @Commodity_Id = 291;

EXEC CuInvoiceDeterminantVal_Sel_By_Invoice_Account_Commodity
      @cu_invoice_id = 63662453 -- int
    , @account_id = 1444107     -- int
    , @Commodity_Id = 291;

EXEC CuInvoiceDeterminantVal_Sel_By_Invoice_Account_Commodity 72305061,55188,290
EXEC CuInvoiceDeterminantVal_Sel_By_Invoice_Account_Commodity1 72305061,55188,290

EXEC CuInvoiceDeterminantVal_Sel_By_Invoice_Account_Commodity
   75251919
    , 442925
    , 291

EXEC CuInvoiceDeterminantVal_Sel_By_Invoice_Account_Commodity1
    75251919
    , 442925
    , 291

	SELECT * FROM CU_INVOICE_SERvice_month where cu_invoice_id = 75251919 

AUTHOR INITIALS:    
 Initials	Name    
------------------------------------------------------------       
 RKV		Ravi Kumar Vegesna  
 NR			Narayana Reddy
 SP         Srinivas Patchava
   
MODIFICATIONS    
    
 Initials	Date		Modification    
------------------------------------------------------------    
 RKV		2016-10-05	Created
 HG			2017-10-30	SE2017-270, Modified to return the data by the aggregation type SUM/MAX/Most recent after converting the values to the first line item of that sub bucket /bucket combination 
 RKV        2018-02-12  SE2017-474, Modified to return the Charge Values and Determinant_Name
 HG			2018-02-19	SE2017-491, Modified to return the Priority 1 bucket values if the Priority2 bucket values are 0 or not present.
 NR			2018-10-01	Data2.0-173 - excluded  Usage - Miscellaneous', 'Billed Usage - Miscellaneous'  Both buckets.
						D20-321		- Modified the logic to return the priority buckets with in metered and Billed and return misc buckets all the time.
 HG			2018-11-01	Data 2.0 enh Added logic to send Misc Volume of NG bucket all the time
 RKV        2018-11-19  MAINT-7942 Modified the code to sum the invoice data instead of taking max value.
 SP         2019-07-16  SE2017-733 ACT Commodity Mapping within CBMS 
 HG			2019-08-26	SE2017-759 - Modified the sproc to return both metered and Billed NG volume buckets along with Misc buckets.
******/
CREATE PROCEDURE [dbo].[CuInvoiceDeterminantVal_Sel_By_Invoice_Account_Commodity]
      (
      @cu_invoice_id INT
    , @account_id    INT = NULL
    , @Commodity_Id  INT = NULL )
AS
      BEGIN

            SET NOCOUNT ON;

            CREATE TABLE #Determinant
                  ( Bucket_Type          NVARCHAR(255)
                  , Determinant_Value    DECIMAL(28, 10)
                  , Uom_Id               INT
                  , Unit_Of_Measure_Type VARCHAR(200)
                  , Commodity_Type_Id    INT
                  , Commodity_Type       VARCHAR(200)
                  , Aggregation_Type     VARCHAR(25)
                  , Row_Num              INT );

            CREATE TABLE #Invoice_Charge
                  ( Bucket_Type         NVARCHAR(255)
                  , Charge_Value        DECIMAL(28, 10)
                  , Currency_Unit_Name  VARCHAR(200)
                  , Commodity_Type_Id   INT
                  , Commodity_Type      VARCHAR(200)
                  , ACT_Commodity_XName VARCHAR(200));

            CREATE TABLE #Final_Determinant_Values
                  ( Bucket_Type          NVARCHAR(255)
                  , DetValue             DECIMAL(28, 10)
                  , Unit_Of_Measure_Type VARCHAR(200)
                  , Commodity_Type_Id    INT
                  , Commodity_Type       VARCHAR(200)
                  , ACT_Commodity_XName  VARCHAR(200));

            DECLARE @Aggregation_Type_Tbl TABLE
                  ( Aggregation_Type VARCHAR(25)
                  , Row_Num          INT );

            DECLARE
                  @Ubm_Name         VARCHAR(200)
                , @Priority_Order   INT
                , @Total_Rows       INT
                , @Current_Row_Num  INT         = 1
                , @Sql              VARCHAR(MAX)
                , @Aggregation_Type VARCHAR(25)
                , @EP_Commodity_Id  INT
                , @NG_Commodity_Id  INT;

            DECLARE @Invoice_Bucket_Master_Ids TABLE
                  ( Bucket_Master_Id INT
                  , Max_Value        DECIMAL(28, 10));
            DECLARE @Total_Usage_Child_Buckets TABLE
                  ( Bucket_Master_Id                  INT
                  , Bucket_Name                       VARCHAR(255)
                  , Priority_Order                    INT
                  , Usage_Bucket_Type                 VARCHAR(25)
                  , Recalc_Determinant_Priority_Order INT );

            CREATE TABLE #Bucket_Category_Rule
                  ( Category_Bucket_Master_Id INT
                  , Category_Bucket_Name      VARCHAR(200)
                  , Priority_Order            INT
                  , Child_Bucket_Master_Id    INT
                  , Aggregation_Type          VARCHAR(25));

            DECLARE @Priority_By_Bucket_Type TABLE
                  ( Usage_Bucket_Type VARCHAR(200)
                  , Priority_Order    INT
                  , Misc_Bucket_Name  VARCHAR(200));

            SELECT
                  @Ubm_Name = u.UBM_NAME
            FROM  dbo.CU_INVOICE inv
                  JOIN
                  dbo.UBM u
                        ON u.UBM_ID = inv.UBM_ID
            WHERE inv.CU_INVOICE_ID = @cu_invoice_id;

            SELECT
                  @EP_Commodity_Id = Commodity_Id
            FROM  dbo.Commodity
            WHERE Commodity_Name = 'Electric Power';

            SELECT
                  @NG_Commodity_Id = Commodity_Id
            FROM  dbo.Commodity
            WHERE Commodity_Name = 'Natural Gas';

            INSERT INTO #Bucket_Category_Rule (
                                                    Category_Bucket_Master_Id
                                                  , Category_Bucket_Name
                                                  , Priority_Order
                                                  , Child_Bucket_Master_Id
                                                  , Aggregation_Type
                                              )
                        SELECT
                              bcr.Category_Bucket_Master_Id
                            , bm.Bucket_Name
                            , bcr.Priority_Order
                            , bcr.Child_Bucket_Master_Id
                            , at.Code_Value
                        FROM  dbo.Bucket_Category_Rule bcr
                              INNER JOIN
                              dbo.Bucket_Master bm
                                    ON bm.Bucket_Master_Id = bcr.Category_Bucket_Master_Id
                              INNER JOIN
                              dbo.Bucket_Master cbm
                                    ON cbm.Bucket_Master_Id = bcr.Child_Bucket_Master_Id
                              INNER JOIN
                              dbo.Code al
                                    ON al.Code_Id = bcr.CU_Aggregation_Level_Cd
                              INNER JOIN
                              dbo.Code bt
                                    ON bt.Code_Id = bm.Bucket_Type_Cd
                              INNER JOIN
                              dbo.Code at
                                    ON at.Code_Id = bcr.Aggregation_Type_CD
                        WHERE bm.Commodity_Id = @Commodity_Id
                              AND   al.Code_Value = 'Invoice'
                              AND   bt.Code_Value = 'Determinant';

            --/*
            INSERT INTO @Total_Usage_Child_Buckets (
                                                         Bucket_Master_Id
                                                       , Bucket_Name
                                                       , Priority_Order
                                                       , Usage_Bucket_Type
                                                       , Recalc_Determinant_Priority_Order
                                                   )
                        SELECT
                              x.Child_Bucket_Master_Id
                            , x.Bucket_Name
                            , x.Priority_Order
                            , x.Usage_Bucket_Type
                            , CASE WHEN @Commodity_Id NOT IN (
                                         @NG_Commodity_Id
                                       , @EP_Commodity_Id)
                                         THEN x.Priority_Order
                                   ELSE  CASE WHEN x.Priority_Order = 1
                                                    THEN 2
                                              WHEN x.Priority_Order = 2
                                                    THEN 1
                                              WHEN x.Priority_Order = 3
                                                    THEN 4
                                              WHEN x.Priority_Order = 4
                                                    THEN 3
                                         /*
								In the future release we can add this column in Bucket_Category_Rule itself
								Child buckets of Usage total priority becomes 1
								Usage Total priority becomes 2
								Child buckets of Billed Usage total priority becomes 3
								Billed Usage Total priority becomes 4

							*/

                                         END
                              END
                        FROM
                              (     SELECT
                                          bcr.Child_Bucket_Master_Id
                                        , cbm.Bucket_Name
                                        , CASE WHEN @Commodity_Id = @EP_Commodity_Id
                                                    AND     cbm.Bucket_Name = 'Usage - Miscellaneous'
                                                     THEN 1
                                               WHEN @Commodity_Id = @EP_Commodity_Id
                                                    AND     cbm.Bucket_Name = 'Billed Usage - Miscellaneous'
                                                     THEN 3
                                               WHEN @Commodity_Id = @NG_Commodity_Id
                                                    AND     cbm.Bucket_Name = 'Miscellaneous Volume'
                                                     THEN 1
                                               WHEN @Commodity_Id = @NG_Commodity_Id
                                                    AND     cbm.Bucket_Name = 'Billed Miscellaneous Volume'
                                                     THEN 3
                                               ELSE  bcr.Priority_Order
                                          END AS Priority_Order
                                        , CASE WHEN bcr.Priority_Order <= 2
                                                     THEN 'Metered'
                                               ELSE  'Billed'
                                          END AS Usage_Bucket_Type
                                    FROM  dbo.Bucket_Category_Rule bcr
                                          INNER JOIN
                                          dbo.Bucket_Master bm
                                                ON bm.Bucket_Master_Id = bcr.Category_Bucket_Master_Id
                                                   AND bm.Bucket_Name = 'Total Usage'
                                                   AND bm.Commodity_Id = @Commodity_Id
                                                   AND bm.Bucket_Master_Id <> bcr.Child_Bucket_Master_Id
                                          INNER JOIN
                                          dbo.Code bt
                                                ON bt.Code_Id = bm.Bucket_Type_Cd
                                          INNER JOIN
                                          dbo.Bucket_Master cbm
                                                ON cbm.Bucket_Master_Id = bcr.Child_Bucket_Master_Id
                                    WHERE bt.Code_Value = 'Determinant' ) x;

            INSERT INTO @Invoice_Bucket_Master_Ids (
                                                         Bucket_Master_Id
                                                       , Max_Value
                                                   )
                        SELECT
                                    bm.Bucket_Master_Id
                                  , sum(convert(DECIMAL(28, 10)
                                              , replace(replace(replace(replace(replace(replace(( CASE WHEN ( patindex('%[0-9]-', cid.DETERMINANT_VALUE)) > 0
                                                                                                             THEN stuff(cid.DETERMINANT_VALUE, patindex('%[0-9]-', cid.DETERMINANT_VALUE) + 1, 1, '')
                                                                                                       ELSE  cid.DETERMINANT_VALUE
                                                                                                  END )
                                                                                              , ','
                                                                                              , '')
                                                                                      , '0-'
                                                                                      , 0)
                                                                              , '+'
                                                                              , '')
                                                                      , '$'
                                                                      , '')
                                                              , ')'
                                                              , '')
                                                      , '('
                                                      , '')))
                        FROM        dbo.CU_INVOICE_DETERMINANT cid
                                    INNER JOIN
                                    dbo.CU_INVOICE_DETERMINANT_ACCOUNT cida
                                          ON cida.CU_INVOICE_DETERMINANT_ID = cid.CU_INVOICE_DETERMINANT_ID
                                    INNER JOIN
                                    dbo.Commodity com
                                          ON com.Commodity_Id = cid.COMMODITY_TYPE_ID
                                    INNER JOIN
                                    dbo.Bucket_Master bm
                                          ON bm.Bucket_Master_Id = cid.Bucket_Master_Id
                        WHERE       cida.ACCOUNT_ID = @account_id
                                    AND   bm.Commodity_Id = @Commodity_Id
                                    AND   cid.CU_INVOICE_ID = @cu_invoice_id
                        GROUP BY    bm.Bucket_Master_Id;

            INSERT INTO @Priority_By_Bucket_Type (
                                                       Usage_Bucket_Type
                                                     , Priority_Order
                                                     , Misc_Bucket_Name
                                                 )
                        SELECT
                              y.Usage_Bucket_Type
                            , y.Priority_Order
                            , CASE WHEN @Commodity_Id = @NG_Commodity_Id
                                        AND     y.Priority_Order <= 2
                                         THEN 'Miscellaneous Volume'
                                   WHEN @Commodity_Id = @NG_Commodity_Id
                                        AND     y.Priority_Order > 2
                                         THEN 'Billed Miscellaneous Volume'
                                   WHEN @Commodity_Id = @EP_Commodity_Id
                                        AND     y.Priority_Order <= 2
                                         THEN 'Usage - Miscellaneous'
                                   WHEN @Commodity_Id = @EP_Commodity_Id
                                        AND     y.Priority_Order > 2
                                         THEN 'Billed Usage - Miscellaneous'
                              END
                        FROM
                              (     SELECT
                                          x.Priority_Order
                                        , x.Usage_Bucket_Type
                                        , rank  () OVER  ( PARTITION BY
                                                                 x.Usage_Bucket_Type
                                                           ORDER BY x.Has_Value DESC
                                                                  , x.Recalc_Determinant_Priority_Order ASC ) AS Row_Num
                                    FROM
                                          (     SELECT
                                                            tucb.Priority_Order
                                                          , tucb.Usage_Bucket_Type
                                                          , tucb.Recalc_Determinant_Priority_Order
                                                          , max(  CASE WHEN ibmi.Max_Value <> 0
                                                                             THEN 1
                                                                       ELSE  0
                                                                  END) Has_Value
                                                FROM        @Invoice_Bucket_Master_Ids ibmi
                                                            INNER JOIN
                                                            @Total_Usage_Child_Buckets tucb
                                                                  ON ibmi.Bucket_Master_Id = tucb.Bucket_Master_Id
                                                WHERE       @Commodity_Id IN (
                                                                  @EP_Commodity_Id
                                                                , @NG_Commodity_Id)
                                                GROUP BY    tucb.Priority_Order
                                                          , tucb.Usage_Bucket_Type
                                                          , tucb.Recalc_Determinant_Priority_Order ) x ) y
                        WHERE y.Row_Num = 1;

            INSERT INTO @Priority_By_Bucket_Type (
                                                       Usage_Bucket_Type
                                                     , Priority_Order
                                                     , Misc_Bucket_Name
                                                 )
                        SELECT
                              y.Usage_Bucket_Type
                            , y.Priority_Order
                            , CASE WHEN @Commodity_Id = @NG_Commodity_Id
                                        AND     y.Priority_Order <= 2
                                         THEN 'Miscellaneous Volume'
                                   WHEN @Commodity_Id = @NG_Commodity_Id
                                        AND     y.Priority_Order > 2
                                         THEN 'Billed Miscellaneous Volume'
                                   WHEN @Commodity_Id = @EP_Commodity_Id
                                        AND     y.Priority_Order <= 2
                                         THEN 'Usage - Miscellaneous'
                                   WHEN @Commodity_Id = @EP_Commodity_Id
                                        AND     y.Priority_Order > 2
                                         THEN 'Billed Usage - Miscellaneous'
                              END
                        FROM
                              (     SELECT
                                          x.Priority_Order
                                        , x.Usage_Bucket_Type
                                        , rank  () OVER  ( PARTITION BY
                                                                 x.Usage_Bucket_Type
                                                           ORDER BY x.Has_Value DESC
                                                                  , x.Priority_Order DESC ) AS Row_Num
                                    FROM
                                          (     SELECT
                                                            tucb.Priority_Order
                                                          , tucb.Usage_Bucket_Type
                                                          , tucb.Recalc_Determinant_Priority_Order
                                                          , max(  CASE WHEN ibmi.Max_Value <> 0
                                                                             THEN 1
                                                                       ELSE  0
                                                                  END) Has_Value
                                                FROM        @Invoice_Bucket_Master_Ids ibmi
                                                            INNER JOIN
                                                            @Total_Usage_Child_Buckets tucb
                                                                  ON ibmi.Bucket_Master_Id = tucb.Bucket_Master_Id
                                                WHERE       @Commodity_Id NOT IN (
                                                                  @EP_Commodity_Id
                                                                , @NG_Commodity_Id)
                                                GROUP BY    tucb.Priority_Order
                                                          , tucb.Usage_Bucket_Type
                                                          , tucb.Recalc_Determinant_Priority_Order ) x ) y
                        WHERE y.Row_Num = 1;


            -- Keep the breakup buckets and deletes the priority 1 buckets if atleast one of the breakup bucket values are <> 0
            DELETE      ibmi
            FROM        @Invoice_Bucket_Master_Ids ibmi
                        INNER JOIN
                        @Total_Usage_Child_Buckets tucb
                              ON ibmi.Bucket_Master_Id = tucb.Bucket_Master_Id
                        INNER JOIN
                        @Priority_By_Bucket_Type pb
                              ON pb.Usage_Bucket_Type = tucb.Usage_Bucket_Type
            WHERE       tucb.Priority_Order <> pb.Priority_Order
                        AND   tucb.Bucket_Name <> pb.Misc_Bucket_Name;


            INSERT INTO #Determinant (
                                           Bucket_Type
                                         , Determinant_Value
                                         , Uom_Id
                                         , Unit_Of_Measure_Type
                                         , Commodity_Type_Id
                                         , Commodity_Type
                                         , Aggregation_Type
                                         , Row_Num
                                     )
                        SELECT
                              CASE WHEN CID.EC_Invoice_Sub_Bucket_Master_Id IS NULL
                                         THEN bm.Bucket_Name
                                   ELSE  eisbm.Sub_Bucket_Name
                              END AS bucket_type
                            , ( CASE WHEN nullif(CIDA.Determinant_Expression, '') IS NULL
                                           THEN ( convert(DECIMAL(28, 10)
                                                        , replace(replace(replace(replace(replace(replace(( CASE WHEN ( patindex('%[0-9]-', CID.DETERMINANT_VALUE)) > 0
                                                                                                                       THEN stuff(CID.DETERMINANT_VALUE, patindex('%[0-9]-', CID.DETERMINANT_VALUE) + 1, 1, '')
                                                                                                                 ELSE  CID.DETERMINANT_VALUE
                                                                                                            END )
                                                                                                        , ','
                                                                                                        , '')
                                                                                                , '0-'
                                                                                                , 0)
                                                                                        , '+'
                                                                                        , '')
                                                                                , '$'
                                                                                , '')
                                                                        , ')'
                                                                        , '')
                                                                , '('
                                                                , '')))
                                     ELSE  CIDA.Determinant_Value
                                END )
                            , CID.UNIT_OF_MEASURE_TYPE_ID
                            , u.ENTITY_NAME
                            , CID.COMMODITY_TYPE_ID
                            , com.Commodity_Name
                            , isnull(x.Aggregation_Type, 'Sum')   -- NG other doesn't get aggregated to any other bucket, for such cases defining it as other.
                            , dense_rank() OVER  ( PARTITION BY
                                                         CASE WHEN CID.EC_Invoice_Sub_Bucket_Master_Id IS NULL
                                                                    THEN bm.Bucket_Name
                                                              ELSE  eisbm.Sub_Bucket_Name
                                                         END
                                                   ORDER BY CID.CU_INVOICE_DETERMINANT_ID ) Row_Num
                        FROM  dbo.CU_INVOICE_DETERMINANT CID
                              INNER JOIN
                              @Invoice_Bucket_Master_Ids ibm
                                    ON ibm.Bucket_Master_Id = CID.Bucket_Master_Id
                              LEFT OUTER JOIN
                                    (     SELECT
                                                      bcr.Child_Bucket_Master_Id
                                                    , bcr.Aggregation_Type
                                          FROM        #Bucket_Category_Rule bcr
                                          GROUP BY    bcr.Child_Bucket_Master_Id
                                                    , bcr.Aggregation_Type ) x
                                          ON x.Child_Bucket_Master_Id = CID.Bucket_Master_Id
                              INNER JOIN
                              dbo.CU_INVOICE_DETERMINANT_ACCOUNT CIDA
                                    ON CIDA.CU_INVOICE_DETERMINANT_ID = CID.CU_INVOICE_DETERMINANT_ID
                              INNER JOIN
                              dbo.Commodity com
                                    ON com.Commodity_Id = CID.COMMODITY_TYPE_ID
                              LEFT OUTER JOIN
                              dbo.Bucket_Master bm
                                    ON bm.Bucket_Master_Id = CID.Bucket_Master_Id
                              LEFT OUTER JOIN
                              dbo.ENTITY u
                                    ON u.ENTITY_ID = CID.UNIT_OF_MEASURE_TYPE_ID
                              LEFT OUTER JOIN
                              dbo.EC_Invoice_Sub_Bucket_Master eisbm
                                    ON CID.EC_Invoice_Sub_Bucket_Master_Id = eisbm.EC_Invoice_Sub_Bucket_Master_Id
                        WHERE CID.CU_INVOICE_ID = @cu_invoice_id
                              AND
                                    (     @account_id IS NULL
                                          OR    CIDA.ACCOUNT_ID = @account_id )
                              AND
                                    (     @Commodity_Id IS NULL
                                          OR    com.Commodity_Id = @Commodity_Id );

            INSERT INTO @Aggregation_Type_Tbl (
                                                    Aggregation_Type
                                                  , Row_Num
                                              )
                        SELECT
                                    Aggregation_Type
                                  , row_number() OVER  ( ORDER BY Aggregation_Type )
                        FROM        #Determinant
                        GROUP BY    Aggregation_Type;
            SELECT
                  @Total_Rows = @@ROWCOUNT;

            WHILE @Current_Row_Num <= @Total_Rows
                  BEGIN

                        BEGIN TRY

                              SELECT
                                    @Aggregation_Type = Aggregation_Type
                              FROM  @Aggregation_Type_Tbl
                              WHERE Row_Num = @Current_Row_Num;

                              IF @Aggregation_Type <> 'Recent'
                                    BEGIN

                                          SELECT
                                                @Sql = 'INSERT INTO #Final_Determinant_Values
									( Bucket_Type
                                    ,DetValue
                                    ,Unit_Of_Measure_Type
                                    ,Commodity_Type_Id
                                    ,Commodity_Type
									,ACT_Commodity_XName  )
                                    SELECT
                                          dv.Bucket_Type
                                         ,' +   @Aggregation_Type + '(dv.Determinant_Value*cuc.Conversion_Factor)' + '
                                         ,uom.Unit_Of_Measure_Type
                                         ,dv.Commodity_Type_Id
                                         ,dv.Commodity_Type
										 ,ISNULL(ACT_Commodity_XName,dv.Commodity_Type)
                                    FROM
                                          #Determinant dv
                                          INNER JOIN #Determinant uom
                                                ON uom.Bucket_Type = dv.Bucket_Type
                                                   AND uom.Row_Num = 1
                                                   AND uom.Commodity_Type_Id = dv.Commodity_Type_Id
                                          INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                                                ON cuc.BASE_UNIT_ID = dv.Uom_Id
                                                AND cuc.Converted_Unit_Id = uom.Uom_Id
												LEFT OUTER JOIN(dbo.ACT_Commodity ac
                                                INNER JOIN dbo.Commodity_ACT_Commodity_Map cacm
                                                    ON cacm.ACT_Commodity_Id = ac.ACT_Commodity_Id
                                                       )
                                    ON cacm.Commodity_Id = dv.Commodity_Type_Id
                                    WHERE
                                          dv.Aggregation_Type = ' + '''' + @Aggregation_Type + '''' + '
                                    GROUP BY
										 dv.Bucket_Type
										 ,uom.Unit_Of_Measure_Type
                                         ,dv.Commodity_Type_Id
                                         ,dv.Commodity_Type
										  ,ISNULL(ACT_Commodity_XName,dv.Commodity_Type)';


                                          EXEC  ( @Sql );
                                    END;
                              ELSE
                                    BEGIN
                                          INSERT INTO #Final_Determinant_Values (
                                                                                      Bucket_Type
                                                                                    , DetValue
                                                                                    , Unit_Of_Measure_Type
                                                                                    , Commodity_Type_Id
                                                                                    , Commodity_Type
                                                                                    , ACT_Commodity_XName
                                                                                )
                                                      SELECT
                                                            dv.Bucket_Type
                                                          , dv.Determinant_Value
                                                          , dv.Unit_Of_Measure_Type
                                                          , dv.Commodity_Type_Id
                                                          , dv.Commodity_Type
                                                          , isnull(ACT_Commodity_XName, dv.Commodity_Type)
                                                      FROM  #Determinant dv
                                                            LEFT OUTER JOIN
                                                            (dbo.ACT_Commodity ac
                                                             INNER JOIN
                                                             dbo.Commodity_ACT_Commodity_Map cacm
                                                                   ON cacm.ACT_Commodity_Id = ac.ACT_Commodity_Id)
                                                                  ON cacm.Commodity_Id = dv.Commodity_Type_Id
                                                      WHERE dv.Aggregation_Type = @Aggregation_Type
                                                            AND   dv.Row_Num = 1;   -- Most recent will fetch First entry present in the Invoice for each Sub bucket/bucket
                                    END;

                        END TRY
                        BEGIN CATCH

                              EXEC dbo.usp_RethrowError;
                              BREAK;

                        END CATCH;
                        SET @Current_Row_Num = @Current_Row_Num + 1;
                  END;

            INSERT INTO #Invoice_Charge
            EXEC dbo.CuInvoiceChargeVal_Sel_By_Invoice_Account_Commodity
                  @cu_invoice_id
                , @account_id
                , @Commodity_Id;

            INSERT INTO #Final_Determinant_Values
                        SELECT
                              Bucket_Type
                            , Charge_Value
                            , Currency_Unit_Name
                            , Commodity_Type_Id
                            , Commodity_Type
                            , ACT_Commodity_XName
                        FROM  #Invoice_Charge;

            SELECT
                  '' AS Determinant_Name
                , Bucket_Type
                , DetValue
                , Unit_Of_Measure_Type
                , Commodity_Type_Id
                , Commodity_Type
                , ACT_Commodity_XName
            FROM  #Final_Determinant_Values;

            DROP TABLE
                  #Bucket_Category_Rule
                , #Determinant
                , #Final_Determinant_Values
                , #Invoice_Charge;


      END;



GO
GRANT EXECUTE ON  [dbo].[CuInvoiceDeterminantVal_Sel_By_Invoice_Account_Commodity] TO [CBMSApplication]
GO
