SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 /******      
                         
 NAME: dbo.Client_Cem_Sel_By_User_Info_Id                     
                          
 DESCRIPTION:      
		  To get clinet cem based on user info.                          
                          
 INPUT PARAMETERS:      
                         
 Name                               DataType          Default       Description      
----------------------------------------------------------------------------------
 @User_Info_Id				         INT        
                          
 OUTPUT PARAMETERS:      
                               
 Name                               DataType          Default       Description      
----------------------------------------------------------------------------------
 @User_Info_Id				        INT        
                          
 USAGE EXAMPLES:                              
----------------------------------------------------------------------------------
               
	             
EXEC dbo.Client_Cem_Sel_By_User_Info_Id @User_Info_Id=41424    
	 
EXEC dbo.Client_Cem_Sel_By_User_Info_Id @User_Info_Id=27830 
	  
			                
                         
 AUTHOR INITIALS:    
       
 Initials                   Name      
----------------------------------------------------------------------------------
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
----------------------------------------------------------------------------------
 NR                     2013-12-30      Created for MAINT-4095.                        
                         
******/ 

CREATE PROCEDURE [dbo].[Client_Cem_Sel_By_User_Info_Id] ( @User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 
      SELECT
            ch.Client_Id
           ,ch.Client_Name
           ,ui.USER_INFO_ID
      FROM
            core.Client_Hier ch
            INNER JOIN dbo.CLIENT_CEM_MAP ccm
                  ON ch.Client_Id = ccm.CLIENT_ID
            INNER JOIN dbo.USER_INFO ui
                  ON ccm.USER_INFO_ID = ui.USER_INFO_ID
      WHERE
            ui.USER_INFO_ID = @User_Info_Id
            AND ch.Client_Not_Managed = 0
            AND ch.Site_Id = 0
            AND ch.Sitegroup_Id = 0
      GROUP BY
            ch.Client_Id
           ,ch.Client_Name
           ,ui.USER_INFO_ID
      ORDER BY
            Client_Name
END

;
GO
GRANT EXECUTE ON  [dbo].[Client_Cem_Sel_By_User_Info_Id] TO [CBMSApplication]
GO
