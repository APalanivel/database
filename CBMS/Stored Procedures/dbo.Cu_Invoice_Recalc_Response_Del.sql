SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
  dbo.Cu_Invoice_Recalc_Response_Del
  
DESCRIPTION:  
  
  
INPUT PARAMETERS:  
 Name								DataType				Default		Description  
-------------------------------------------------------------------------------------  

 @Cu_Invoice_Recalc_Response_Id		INT
   
  

OUTPUT PARAMETERS:  
 Name								DataType				Default		Description  
-------------------------------------------------------------------------------------  
  
USAGE EXAMPLES:  
-------------------------------------------------------------------------------------  
 BEGIN Transaction
 
 
 
 ROLLBACK Transaction 
  
  
AUTHOR INITIALS:  
 Initials	Name  
-------------------------------------------------------------------------------------  
 RKV		Ravi Kumar Vegesna  
   
MODIFICATIONS  
  
 Initials		Date			Modification  
-------------------------------------------------------------------------------------  
 RKV			2016-01-13		Created for AS400-PII 
            
              
******/  
CREATE PROCEDURE [dbo].[Cu_Invoice_Recalc_Response_Del]
      ( 
       @Cu_Invoice_Recalc_Response_Id INT )
AS 
BEGIN  
      SET NOCOUNT ON;  
     
     
     
      
      
      DELETE FROM
            dbo.Cu_Invoice_Recalc_Response
      WHERE
            Cu_Invoice_Recalc_Response_Id = @Cu_Invoice_Recalc_Response_Id            
   
        
      
       
END     
   

;
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Recalc_Response_Del] TO [CBMSApplication]
GO
