SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--EXEC dbo.UBM_GET_FEED_FILES_LIST_P '1','1',400


CREATE    PROCEDURE dbo.UBM_GET_FEED_FILES_LIST_P 
@userId varchar(20),
@sessionId varchar(20),
@masterLogId int

as
	set nocount on
declare @fileStatus Varchar(100)

        SELECT feed_file_name,entity_name FROM UBM_FEED_FILE_MAP,entity WHERE ubm_batch_master_log_id=@masterLogId
	and entity_id=status_type_id

GO
GRANT EXECUTE ON  [dbo].[UBM_GET_FEED_FILES_LIST_P] TO [CBMSApplication]
GO
