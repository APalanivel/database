
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_RPT_TOTAL_NO_OF_SOURCING_ANALYSTS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType			Default				Description
--------------------------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType			Default				Description
--------------------------------------------------------------------------------

USAGE EXAMPLES:
--------------------------------------------------------------------------------

EXEC dbo.SR_SAD_RPT_TOTAL_NO_OF_SOURCING_ANALYSTS_P


AUTHOR INITIALS:
	Initials	Name
--------------------------------------------------------------------------------
	NR			Narayana Reddy

MODIFICATIONS

	Initials	Date		    Modification
--------------------------------------------------------------------------------
	        	9/21/2010	    Modify Quoted Identifier
    DMR		    09/10/2010	    Modified for Quoted_Identifier
 	NR			2015-09-14		MAINT-3067 Removed legacy view Utility-Analyst-Map(read "-" as "_")

******/

CREATE PROCEDURE [dbo].[SR_SAD_RPT_TOTAL_NO_OF_SOURCING_ANALYSTS_P]
AS 
BEGIN

      SET NOCOUNT ON;

      DECLARE
            @Ng_Commodity_Id INT
           ,@Ep_Commodity_Id INT;
      
      SELECT
            @Ng_Commodity_Id = c.Commodity_Id
      FROM
            dbo.Commodity c
      WHERE
            c.Commodity_Name = 'Natural Gas';
            
      SELECT
            @Ep_Commodity_Id = c.Commodity_Id
      FROM
            dbo.Commodity c
      WHERE
            c.Commodity_Name = 'Electric Power';

      SELECT
            COUNT(ui.USER_INFO_ID)
      FROM
            dbo.USER_INFO ui
      WHERE
            ui.IS_HISTORY = 0
            AND EXISTS ( SELECT
                              1
                         FROM
                              dbo.UTILITY_DETAIL ud
                              INNER JOIN dbo.VENDOR_COMMODITY_MAP vcm
                                    ON ud.VENDOR_ID = vcm.VENDOR_ID
                              INNER JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                                    ON vcm.VENDOR_COMMODITY_MAP_ID = vcam.Vendor_Commodity_Map_Id
                         WHERE
                              ui.USER_INFO_ID = vcam.Analyst_Id
                              AND vcm.COMMODITY_TYPE_ID IN ( @Ng_Commodity_Id, @Ep_Commodity_Id ) );
  
END;

;
GO

GRANT EXECUTE ON  [dbo].[SR_SAD_RPT_TOTAL_NO_OF_SOURCING_ANALYSTS_P] TO [CBMSApplication]
GO
