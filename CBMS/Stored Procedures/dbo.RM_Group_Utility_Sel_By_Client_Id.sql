SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
   
 dbo.RM_Group_Utility_Sel_By_Client_Id
   
 DESCRIPTION:     
   
 This procedure gets all the Supplier name associated to the user.  
   
 INPUT PARAMETERS:    
 Name				DataType		 Default		 Description    
----------------------------------------------------------------------      
 @Client_Id	INT
   
 OUTPUT PARAMETERS:    
 Name				DataType		 Default		 Description    
----------------------------------------------------------------------

  
  USAGE EXAMPLES:    
------------------------------------------------------------  

  
	EXEC RM_Group_Utility_Sel_By_Client_Id 235
	   
	
  
AUTHOR INITIALS:    
 Initials		Name    
------------------------------------------------------------    
NR				Narayana Reddy
    
 MODIFICATIONS     
 Initials			Date			Modification    
------------------------------------------------------------    
   NR				25-07-2018		Created For Risk managemnet.		


******/


CREATE PROCEDURE [dbo].[RM_Group_Utility_Sel_By_Client_Id]
    (
        @Client_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            'Utility' AS Filter_Name
            , CHA.Account_Vendor_Id AS Filter_Value
            , CHA.Account_Vendor_Name Display_Filter_Name
        FROM
            Core.Client_Hier ch
            JOIN Core.Client_Hier_Account CHA
                ON ch.Client_Hier_Id = CHA.Client_Hier_Id
        WHERE
            ch.Client_Id = @Client_Id
            AND CHA.Account_Type = 'Utility'
        GROUP BY
            CHA.Account_Vendor_Id
            , CHA.Account_Vendor_Name
        ORDER BY
            CHA.Account_Vendor_Name;

    END;

GO
GRANT EXECUTE ON  [dbo].[RM_Group_Utility_Sel_By_Client_Id] TO [CBMSApplication]
GO
