SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Vendor_Commodity_Map_Del]  
     
DESCRIPTION: 
	It Deletes	Vendor Commodity Map detail for Selected Vendor and Commodity Type Ids.
      
INPUT PARAMETERS:          
	NAME				DATATYPE	DEFAULT		DESCRIPTION          
----------------------------------------------------------------         
	@Vendor_Id			INT
    @Commodity_Type_Id	INT	
                
OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------         
	USAGE EXAMPLES:
------------------------------------------------------------ 

	BEGIN TRAN

		EXEC dbo.Vendor_Commodity_Map_Del 3450,291
		--EXEC dbo.Vendor_Commodity_Map_Del 4352,290
		
		--SELECT * FROM dbo.Vendor_Commodity_Map where vendor_id = 3450
		--										and commodity_type_id = 291
		
		--SELECT * FROM dbo.Vendor_Commodity_Map where vendor_id = 4352
		--									AND commodity_type_id = 290											

	ROLLBACK TRAN

AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS
	INITIALS	DATE			MODIFICATION
------------------------------------------------------------
	PNR			23-August-10	CREATED

*/
CREATE PROCEDURE dbo.Vendor_Commodity_Map_Del
    (
       @Vendor_Id			INT
      ,@Commodity_Type_Id	INT
    )
AS
BEGIN

    SET NOCOUNT ON ;

	DELETE
		dbo.Vendor_Commodity_Map
	WHERE
		VENDOR_ID = @Vendor_Id
		AND COMMODITY_TYPE_ID = @Commodity_Type_Id

END
GO
GRANT EXECUTE ON  [dbo].[Vendor_Commodity_Map_Del] TO [CBMSApplication]
GO
