SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










/*********     
NAME:    [dbo].[Get_Variance_Internal_Comments_Required]  
   
DESCRIPTION:  Used to select code value from code table using the code name    
  
INPUT PARAMETERS:      
      Name              DataType          Default     Description      
------------------------------------------------------------      
    
          
      
OUTPUT PARAMETERS:      
      Name              DataType          Default     Description      
------------------------------------------------------------      
      
USAGE EXAMPLES:     
  
 
EXEC [Get_Variance_Internal_Comments_Required] 103310, 103323
  
------------------------------------------------------------    
AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
TRK Ramakrishna Thummala  
AP Arunkumar Palanivel  
  
Initials Date  Modification    
------------------------------------------------------------    

AP April 25,2020 Created stored procedure to get variance closure category  
AP	June 3,2020		AVT Phase 2 https://summit.jira.com/browse/ADVT-358
  
******/
CREATE PROCEDURE [dbo].[Get_Variance_Internal_Comments_Required]
      (
      @Closure_Category_Cd INT
    , @Closure_Reason_Cd   INT )
AS
      BEGIN

            SET NOCOUNT ON;

            DECLARE @Is_Internal_Comments_Required BIT;

            SET @Is_Internal_Comments_Required = 0;



            IF EXISTS
                  (     SELECT
                              1
                        FROM  dbo.Variance_Closed_Reason_Category vrc
                              JOIN
                              dbo.Code c
                                    ON vrc.Closure_Category_Cd = c.Code_Id
                              JOIN
                              dbo.Code c1
                                    ON vrc.Closed_Reason_Cd = c1.Code_Id
                        WHERE vrc.Closed_Reason_Id = @Closure_Reason_Cd
                              AND   c.Code_Dsc = 'Data Source'
                              AND   c1.Code_Dsc = 'UBMD sent (ticket number required)' )
                  BEGIN

                        SET @Is_Internal_Comments_Required = 1;

                  END;


            IF EXISTS
                  (     SELECT
                              1
                        FROM  dbo.Variance_Closed_Reason_Category vrc
                              JOIN
                              dbo.Code c
                                    ON vrc.Closure_Category_Cd = c.Code_Id
                              JOIN
                              dbo.Code c1
                                    ON vrc.Closed_Reason_Cd = c1.Code_Id
                        WHERE vrc.Closed_Reason_Id = @Closure_Reason_Cd
                              AND   c.Code_Dsc = 'DRR Submitted'
                              AND   c1.Code_Dsc = 'DRR created (ticket number required)' )
                  BEGIN

                        SET @Is_Internal_Comments_Required = 1;

                  END;

            IF EXISTS
                  (     SELECT
                              1
                        FROM  dbo.Variance_Closed_Reason_Category vrc
                              JOIN
                              dbo.Code c
                                    ON vrc.Closure_Category_Cd = c.Code_Id
                              JOIN
                              dbo.Code c1
                                    ON vrc.Closed_Reason_Cd = c1.Code_Id
                        WHERE vrc.Closed_Reason_Id = @Closure_Reason_Cd
                              AND   c.Code_Dsc = 'Other'
                              AND   c1.Code_Dsc = 'Other' )
                  BEGIN

                        SET @Is_Internal_Comments_Required = 1;

                  END;


            IF EXISTS
                  (     SELECT
                              1
                        FROM  dbo.Variance_Closed_Reason_Category vrc
                              JOIN
                              dbo.Code c
                                    ON vrc.Closure_Category_Cd = c.Code_Id
                              JOIN
                              dbo.Code c1
                                    ON vrc.Closed_Reason_Cd = c1.Code_Id
                        WHERE vrc.Closed_Reason_Id = @Closure_Reason_Cd
                              AND   c.Code_Dsc = 'Previous Month Escalated'
                              AND   c1.Code_Dsc = 'Previous Month Escalated (elevated month required)' )
                  BEGIN

                        SET @Is_Internal_Comments_Required = 1;

                  END;


            SELECT
                  @Is_Internal_Comments_Required AS Is_Internal_Comments_Required;

      END;
GO
GRANT EXECUTE ON  [dbo].[Get_Variance_Internal_Comments_Required] TO [CBMSApplication]
GO
