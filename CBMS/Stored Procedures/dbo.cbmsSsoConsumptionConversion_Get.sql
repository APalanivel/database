SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsSsoConsumptionConversion_Get

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@base_unit_id  	int       	          	
	@converted_unit_id	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE    PROCEDURE [dbo].[cbmsSsoConsumptionConversion_Get]
	( @MyAccountId int
	, @base_unit_id int
	, @converted_unit_id int
	)
AS
BEGIN
  
	select c.conversion_factor
		, c.base_unit_id
		, c.converted_unit_id
	from consumption_unit_conversion c
	where c.base_unit_id = @base_unit_id
	  and c.converted_unit_id = @converted_unit_id


END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoConsumptionConversion_Get] TO [CBMSApplication]
GO
