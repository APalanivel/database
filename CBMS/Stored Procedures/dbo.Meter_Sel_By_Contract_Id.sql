SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.Meter_Sel_By_Contract_Id

DESCRIPTION:

INPUT PARAMETERS:
	Name					DataType		Default	Description
---------------------------------------------------------------
	@Contract_Id			INT

OUTPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	
USAGE EXAMPLES:
------------------------------------------------------------

	

	EXEC Meter_Sel_By_Contract_Id 1


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	NR			Narayaana Reddy
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	NR       	2019-07-18	Created for Add - contract

******/
CREATE PROCEDURE [dbo].[Meter_Sel_By_Contract_Id]
    (
        @Contract_Id INT = NULL
        , @Account_Id INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            cha.Meter_Id
            , cha.Meter_Number
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Account_Type = 'supplier'
            AND cha.Account_Number <> 'Not Yet Assigned'
            AND (   @Contract_Id IS NULL
                    OR  cha.Supplier_Contract_ID = @Contract_Id)
            AND (   @Account_Id IS NULL
                    OR  cha.Account_Id = @Account_Id)
        GROUP BY
            cha.Meter_Id
            , cha.Meter_Number;

    END;




GO
GRANT EXECUTE ON  [dbo].[Meter_Sel_By_Contract_Id] TO [CBMSApplication]
GO
