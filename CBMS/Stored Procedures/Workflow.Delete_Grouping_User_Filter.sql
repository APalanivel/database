SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    [Workflow].[Delete_Grouping_User_Filter]
DESCRIPTION: THIS STORED PROCEDURE IS CREATED TO DELETE THE GROUPING  
			 If output column id is empty, then code will delete the user column preferences.
------------------------------------------------------------   
 INPUT PARAMETERS:    
 Name   DataType  Default Description 
 @userID								INT    
 @Workflow_Queue_Search_Filter_Group_Id INT   
------------------------------------------------------------    
 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
 USAGE EXAMPLES:    
 EXEC WORKFLOW.Delete_Grouping_User_Filter @userID = 0,                               -- int
                                           @Workflow_Queue_Search_Filter_Group_Id = 0 -- int
------------------------------------------------------------    
AUTHOR INITIALS:    
Initials	Name    
------------------------------------------------------------    
AKP			ARUNKUMAR PALANIVEL	Summit Energy 
 
 MODIFICATIONS     
 Initials Date   Modification    
------------------------------------------------------------    
 AKP		  SEP 5,2019 Created

******/ 
   
CREATE PROCEDURE [Workflow].[Delete_Grouping_User_Filter]     
  -- Add the parameters for the stored procedure here     
  @userID   INT ,     
  @Workflow_Queue_Search_Filter_Group_Id int     
    
AS     
  BEGIN -- SET NOCOUNT ON added to prevent extra result sets from     
      -- interfering with SELECT statements.     
      DECLARE @Proc_name     VARCHAR(100) =     
              'Delete_Grouping_User_Filter',     
              @Input_Params  VARCHAR (1000),     
              @Error_Line    INT,     
              @Error_Message VARCHAR(3000),    
     @display_seq_max int      
    
      SELECT @Input_Params = '@userid: '+ Cast ( @userID AS VARCHAR) +' '    + '@Workflow_Queue_Search_Filter_Group_Id: '+Cast (@Workflow_Queue_Search_Filter_Group_Id AS VARCHAR)    
      SET nocount ON;     
        
         
    
      BEGIN TRY      
    
   DELETE FROM WORKFLOW.WORKFLOW_QUEUE_SEARCH_FILTER_GROUP_INVOICE_MAP     
   WHERE WORKFLOW_QUEUE_SEARCH_FILTER_GROUP_ID = @WORKFLOW_QUEUE_SEARCH_FILTER_GROUP_ID    
    
   DELETE FROM WORKFLOW.WORKFLOW_QUEUE_SEARCH_FILTER_GROUP     
   WHERE WORKFLOW_QUEUE_SEARCH_FILTER_GROUP_ID = @WORKFLOW_QUEUE_SEARCH_FILTER_GROUP_ID     
       
   --DELETE THE ENTRIES WHICH WAS DEFAULT EARLIER AND NOW USER DID NOT WANT TOR    
       
      END try     
    
      BEGIN catch     
    
   Print 'error'    
    
    
   -- Entry made to the logging SP to capture the errors.    
          SELECT @ERROR_LINE = Error_line(),     
                 @ERROR_MESSAGE = Error_message()     
    
          INSERT INTO storedproc_error_log     
                      (storedproc_name,     
                       error_line,     
                       error_message,     
                       input_params)     
          VALUES      ( @PROC_NAME,     
                        @ERROR_LINE,     
                        @ERROR_MESSAGE,     
                        @INPUT_PARAMS )     
      END catch     
  END        
GO
GRANT EXECUTE ON  [Workflow].[Delete_Grouping_User_Filter] TO [CBMSApplication]
GO
