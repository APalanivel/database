
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Country_Sr_Pricing_Product_Ins_Upd]
           
DESCRIPTION:             
			To get map/unmap countries and SR pricing products
			
INPUT PARAMETERS:            
	Name					DataType	Default		Description  
---------------------------------------------------------------------------------  
	@SR_PRICING_PRODUCT_ID	INT
    @Countrty_Id_List		VARCHAR(MAX)
    @User_Info_Id			INT


OUTPUT PARAMETERS:
	Name			DataType		Default		Description  
---------------------------------------------------------------------------------  


 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT * FROM dbo.SR_PRICING_PRODUCT
	
	BEGIN TRANSACTION  
		SELECT * FROM dbo.SR_PRICING_PRODUCT spp left JOIN dbo.SR_Pricing_Product_Country_Map cspp ON spp.SR_PRICING_PRODUCT_ID = cspp.SR_PRICING_PRODUCT_ID
			WHERE spp.SR_PRICING_PRODUCT_ID = 95
		EXEC dbo.Country_Sr_Pricing_Product_Ins_Upd 95,'1,2,3,4,5',49
		SELECT * FROM dbo.SR_PRICING_PRODUCT spp left JOIN dbo.SR_Pricing_Product_Country_Map cspp ON spp.SR_PRICING_PRODUCT_ID = cspp.SR_PRICING_PRODUCT_ID
			WHERE spp.SR_PRICING_PRODUCT_ID = 95
	ROLLBACK TRANSACTION
  
  
  
  	BEGIN TRANSACTION  
		SELECT * FROM dbo.SR_PRICING_PRODUCT spp left JOIN dbo.SR_Pricing_Product_Country_Map cspp ON spp.SR_PRICING_PRODUCT_ID = cspp.SR_PRICING_PRODUCT_ID
			WHERE spp.SR_PRICING_PRODUCT_ID = 86
		EXEC Country_Sr_Pricing_Product_Ins_Upd 86,'1,9,21,4',71920
		SELECT * FROM dbo.SR_PRICING_PRODUCT spp left JOIN dbo.SR_Pricing_Product_Country_Map cspp ON spp.SR_PRICING_PRODUCT_ID = cspp.SR_PRICING_PRODUCT_ID
			WHERE spp.SR_PRICING_PRODUCT_ID = 86
	ROLLBACK TRANSACTION
	
	
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2015-09-22	Global Sourcing - Phase2 -Created
	RR			2016-04-13	Global Sourcing - Phase3 - GCS-618  modified insert logic.
	NR			2017-05-02	MAINT-5261 - Added Sr_Service_Condition_Template_Product_Map in Not Exist clause 
							when deleteing the Country Map.
								
******/

CREATE  PROCEDURE [dbo].[Country_Sr_Pricing_Product_Ins_Upd]
      ( 
       @SR_PRICING_PRODUCT_ID INT
      ,@Countrty_Id_List VARCHAR(MAX) = NULL
      ,@User_Info_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;

      DELETE
            sppcm
      FROM
            dbo.SR_Pricing_Product_Country_Map sppcm
      WHERE
            sppcm.SR_Pricing_Product_Id = @SR_PRICING_PRODUCT_ID
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              dbo.ufn_split(@Countrty_Id_List, ',')
                             WHERE
                              sppcm.Country_Id = CAST(Segments AS INT) )
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              Sr_Service_Condition_Template_Product_Map ssctpm
                             WHERE
                              sppcm.SR_Pricing_Product_Country_Map_Id = ssctpm.SR_Pricing_Product_Country_Map_Id )


      INSERT      INTO dbo.SR_Pricing_Product_Country_Map
                  ( 
                   SR_Pricing_Product_Id
                  ,Country_Id
                  ,Created_User_Id
                  ,Created_Ts )
                  SELECT
                        @SR_PRICING_PRODUCT_ID
                       ,CAST(Segments AS INT)
                       ,@User_Info_Id
                       ,GETDATE()
                  FROM
                        dbo.ufn_split(@Countrty_Id_List, ',')
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.SR_Pricing_Product_Country_Map sppcm
                                     WHERE
                                          sppcm.SR_Pricing_Product_Id = @SR_PRICING_PRODUCT_ID
                                          AND sppcm.Country_Id = CAST(Segments AS INT) );

END;


;
GO

GRANT EXECUTE ON  [dbo].[Country_Sr_Pricing_Product_Ins_Upd] TO [CBMSApplication]
GO
