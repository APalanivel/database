SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
   
/******      
NAME: dbo.Get_Common_Uom_Entity_Id_By_Commodity_Service_Category      
     
DESCRIPTION:       
      
    It is used to get common UOM_Id( ENTITY_Id) between the given ommodity list service category list.  
      
INPUT PARAMETERS:          
      Name    DataType          Default     Description          
------------------------------------------------------------------          
      @Commodity  VARCHAR(MAX)  
      @Service_Category VARCHAR(MAX)   
       
OUTPUT PARAMETERS:          
      Name              DataType          Default     Description          
------------------------------------------------------------          
       
USAGE EXAMPLES:      
------------------------------------------------------------      
      
EXEC dbo.Get_Common_Uom_Entity_Id_By_Commodity_Service_Category   
@Commodity = null  
,@Service_Category = '100,101'  

EXEC dbo.Get_Common_Uom_Entity_Id_By_Commodity_Service_Category   
@Commodity = '290,291'  
,@Service_Category = ''
     

AUTHOR INITIALS:      
 Initials Name      
------------------------------------------------------------      
 BCH   Balaraju      
      
MODIFICATIONS      
      
 Initials Date  Modification      
------------------------------------------------------------      
 BCH     11/09/2011    Created

******/

CREATE PROCEDURE dbo.Get_Common_Uom_Entity_Id_By_Commodity_Service_Category
      ( 
       @Commodity VARCHAR(MAX) = NULL
      ,@Service_Category VARCHAR(MAX) = NULL )
AS 
BEGIN

      SET NOCOUNT ON ;

      DECLARE @Commodity_List TABLE
            ( 
             Commodity_id INT PRIMARY KEY CLUSTERED )

      DECLARE @Commodity_Cnt INT

      INSERT      INTO @Commodity_List
                  ( 
                   Commodity_id )
                  SELECT
                        Segments
                  FROM
                        dbo.ufn_split(@commodity, ',')  
                  
      INSERT      INTO @Commodity_List
                  ( 
                   Commodity_id )
                  SELECT
                        scc.Commodity_Id
                  FROM
                        dbo.ufn_split(@Service_Category, ',') sc
                        JOIN dbo.Client_Service_Category_Commodity scc
                              ON scc.Client_Service_Category_Id = CAST(sc.Segments AS INT)
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          @Commodity_List cl
                                     WHERE
                                          cl.Commodity_id = scc.Commodity_Id )
                  GROUP BY
                        scc.Commodity_Id
      SELECT
            @Commodity_Cnt = COUNT(Commodity_id)
      FROM
            @Commodity_List ;

      SELECT
            MIN(uom.ENTITY_ID) AS Uom_Id
           ,uom.ENTITY_NAME AS Uom_Name
      FROM
            dbo.Commodity com
            INNER JOIN @Commodity_List cl
                  ON cl.Commodity_Id = com.Commodity_Id
            INNER JOIN dbo.ENTITY uom
                  ON uom.Entity_Type = com.Uom_Entity_Type
      GROUP BY
            uom.ENTITY_NAME
      HAVING
            COUNT(1) = @Commodity_Cnt  
                  
END
GO
GRANT EXECUTE ON  [dbo].[Get_Common_Uom_Entity_Id_By_Commodity_Service_Category] TO [CBMSApplication]
GO
