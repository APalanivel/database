SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                          
 NAME: dbo.Invoice_Collection_Account_Config_Sel_By_Chase_Priority_Date            
                          
 DESCRIPTION:                          
   To get the details of invoice collection account level Frequency_And_Expected_Dtl_Sel config               
                          
 INPUT PARAMETERS:            
                       
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
@Invoice_Collection_Account_Config_Id INT  NULL   
                                
 OUTPUT PARAMETERS:            
                             
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
                          
 USAGE EXAMPLES:                              
---------------------------------------------------------------------------------------------------------------                              
      
  
    
    
   EXEC dbo.Invoice_Collection_Account_Config_Sel_By_Chase_Priority_Date  
            
  
                         
 AUTHOR INITIALS:          
         
 Initials              Name          
---------------------------------------------------------------------------------------------------------------                        
 RKV                  Ravi Kumar Vegesna  
                           
 MODIFICATIONS:        
            
 Initials              Date             Modification        
---------------------------------------------------------------------------------------------------------------        
    RKV				2017-06-29		Created For Maint 5509. 
	RKV             2019-03-29      MAINT-8418 modified the code to pick the config ids upto the next month last day of 
									Invoice_Collection_Service_End_Dt  
	RKV             2020-02-18		Added custom days as part of Toolrevemp.      
                         
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Account_Config_Sel_By_Chase_Priority_Date]
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Default_Execution_Day INT;


        CREATE TABLE #Invoice_Collection_Account_Config_ICQ_Last_Executed_Date
             (
                 Invoice_Collection_Account_Config_Id INT
                 , Execution_Date DATE
             );

        CREATE TABLE #Invoice_Collection_Account_Config_Invoice_Last_Executed_Date
             (
                 Invoice_Collection_Account_Config_Id INT
                 , Execution_Date DATE
             );

        CREATE TABLE #Invoice_Collection_Account_Config_Last_Executed_Date
             (
                 Invoice_Collection_Account_Config_Id INT
                 , Execution_Date DATE
             );


        CREATE TABLE #Invoice_Collection_Account_Config_Frequency_Dates
             (
                 Invoice_Collection_Account_Config_Id INT
                 , Execution_Value DECIMAL(8, 2)
                 , Execution_Day INT
             );



        SELECT
            @Default_Execution_Day = (COALESCE(icgcv.Expected_Invoice_Received_Day, icgcv.Expected_Invoice_Raised_Day))
        FROM
            dbo.Invoice_Collection_Global_Config_Value icgcv
            INNER JOIN dbo.Code ifc
                ON ifc.Code_Id = icgcv.Invoice_Frequency_Cd
            INNER JOIN dbo.Codeset cs
                ON ifc.Codeset_Id = cs.Codeset_Id
        WHERE
            ifc.Code_Value = 'monthly'
            AND cs.Codeset_Name = 'InvoiceFrequency';




        INSERT INTO #Invoice_Collection_Account_Config_ICQ_Last_Executed_Date
             (
                 Invoice_Collection_Account_Config_Id
                 , Execution_Date
             )
        SELECT
            aicm.Invoice_Collection_Account_Config_Id
            , MAX(Service_Month)
        FROM
            dbo.Invoice_Collection_Queue_Month_Map icqmm
            INNER JOIN dbo.Account_Invoice_Collection_Month aicm
                ON icqmm.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id
        GROUP BY
            aicm.Invoice_Collection_Account_Config_Id;

        INSERT INTO #Invoice_Collection_Account_Config_Invoice_Last_Executed_Date
             (
                 Invoice_Collection_Account_Config_Id
                 , Execution_Date
             )
        SELECT
            aicm.Invoice_Collection_Account_Config_Id
            , MAX(aicm.Service_Month)
        FROM
            dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map aicmcim
            INNER JOIN dbo.Account_Invoice_Collection_Month aicm
                ON aicmcim.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id
        GROUP BY
            aicm.Invoice_Collection_Account_Config_Id;

        INSERT INTO #Invoice_Collection_Account_Config_Last_Executed_Date
             (
                 Invoice_Collection_Account_Config_Id
                 , Execution_Date
             )
        SELECT
            COALESCE(
                icaciled.Invoice_Collection_Account_Config_Id, icaciled2.Invoice_Collection_Account_Config_Id
                , icac.Invoice_Collection_Account_Config_Id) Invoice_Collection_Account_Config_Id
            , CASE WHEN ISNULL(icaciled.Execution_Date, '1900-01-01') > ISNULL(icaciled2.Execution_Date, '1900-01-01') THEN
                       icaciled.Execution_Date
                  ELSE ISNULL(icaciled2.Execution_Date, '1900-01-01')
              END Execution_Date
        FROM
            dbo.Invoice_Collection_Account_Config icac
            LEFT OUTER JOIN #Invoice_Collection_Account_Config_ICQ_Last_Executed_Date icaciled
                ON icac.Invoice_Collection_Account_Config_Id = icaciled.Invoice_Collection_Account_Config_Id
            LEFT OUTER JOIN #Invoice_Collection_Account_Config_Invoice_Last_Executed_Date icaciled2
                ON icac.Invoice_Collection_Account_Config_Id = icaciled2.Invoice_Collection_Account_Config_Id
        WHERE
            icac.Is_Chase_Activated = 1;


        INSERT INTO #Invoice_Collection_Account_Config_Frequency_Dates
             (
                 Invoice_Collection_Account_Config_Id
                 , Execution_Value
                 , Execution_Day
             )
        SELECT
            icac.Invoice_Collection_Account_Config_Id
            , MIN(CASE WHEN ifc.Code_Value = 'monthly' THEN 1
                      WHEN ifc.Code_Value = 'bi-monthly' THEN 2
                      WHEN ifc.Code_Value = 'quarterly' THEN 3
                      WHEN ifc.Code_Value = 'bi-weekly' THEN 0.5
                      WHEN ifc.Code_Value = 'bi-annual' THEN 6
                      WHEN ifc.Code_Value = 'annual' THEN 12
                      WHEN ifc.Code_Value = 'Custom'
                           AND  ifpc.Code_Value = '1-9 Days' THEN 1
                      WHEN ifc.Code_Value = 'Custom'
                           AND  ifpc.Code_Value = 'Custom Days' THEN aicf.Custom_Days / 30
                      ELSE 5
                  END) code_id
            , MIN(COALESCE(
                      aicf.Expected_Invoice_Received_Day, aicf.Expected_Invoice_Raised_Day
                      , aicf.Custom_Invoice_Received_Day))
        FROM
            dbo.Account_Invoice_Collection_Frequency aicf
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON aicf.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN dbo.Code ifc
                ON ifc.Code_Id = aicf.Invoice_Frequency_Cd
            INNER JOIN dbo.Code ifpc
                ON ifpc.Code_Id = aicf.Invoice_Frequency_Pattern_Cd
        WHERE
            icac.Is_Chase_Activated = 1
            AND GETDATE() < icac.Invoice_Collection_Service_End_Dt
        GROUP BY
            icac.Invoice_Collection_Account_Config_Id;



        INSERT INTO #Invoice_Collection_Account_Config_Frequency_Dates
             (
                 Invoice_Collection_Account_Config_Id
                 , Execution_Value
                 , Execution_Day
             )
        SELECT
            icac.Invoice_Collection_Account_Config_Id
            , 1.0
            , @Default_Execution_Day
        FROM
            dbo.Invoice_Collection_Account_Config icac
        WHERE
            Is_Chase_Activated = 1
            AND GETDATE() < CASE WHEN icac.Invoice_Collection_Service_End_Dt = '9999-12-31' THEN DATEADD(s, -1, DATEADD(mm, DATEDIFF(m, 0, icac.Invoice_Collection_Service_End_Dt) + 0, 0))
							ELSE DATEADD(s, -1, DATEADD(mm, DATEDIFF(m, 0, icac.Invoice_Collection_Service_End_Dt) + 2, 0))
							END
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #Invoice_Collection_Account_Config_Frequency_Dates icacfd
                               WHERE
                                    icacfd.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id);





        SELECT
            led.Invoice_Collection_Account_Config_Id
        FROM
            #Invoice_Collection_Account_Config_Last_Executed_Date led
            INNER JOIN #Invoice_Collection_Account_Config_Frequency_Dates fd
                ON led.Invoice_Collection_Account_Config_Id = fd.Invoice_Collection_Account_Config_Id
        WHERE
            DATEPART(mm, led.Execution_Date) <> DATEPART(mm, DATEADD(mm, -1, GETDATE()))
            AND CAST(DATEADD(dd, 15 - 1, DATEADD(dd, -DATEPART(dd, GETDATE()) + 1, GETDATE())) AS DATE) < CAST(GETDATE() AS DATE)
        ORDER BY
            led.Invoice_Collection_Account_Config_Id;

        DROP TABLE #Invoice_Collection_Account_Config_Last_Executed_Date;
        DROP TABLE #Invoice_Collection_Account_Config_Invoice_Last_Executed_Date;
        DROP TABLE #Invoice_Collection_Account_Config_ICQ_Last_Executed_Date;
        DROP TABLE #Invoice_Collection_Account_Config_Frequency_Dates;
    END;

GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Account_Config_Sel_By_Chase_Priority_Date] TO [CBMSApplication]
GO
