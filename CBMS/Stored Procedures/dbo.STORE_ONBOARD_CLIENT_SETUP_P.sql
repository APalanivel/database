SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.STORE_ONBOARD_CLIENT_SETUP_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@clientId      	int       	          	
	@maxHedgePercent	decimal(18,0)	          	
	@contactName   	varchar(200)	          	
	@contactOfficePhone	varchar(200)	          	
	@contactCellPhone	varchar(200)	          	
	@contactFaxNumber	varchar(200)	          	
	@contactEmail  	varchar(1000)	          	
	@onboardDate   	datetime  	          	
	@comments      	varchar(4000)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE     PROCEDURE dbo.STORE_ONBOARD_CLIENT_SETUP_P
@userId varchar(10),
@sessionId varchar(20),
@clientId integer,
@maxHedgePercent decimal,
@contactName varchar(200),
@contactOfficePhone varchar(200),
@contactCellPhone varchar(200),
@contactFaxNumber varchar(200),
@contactEmail varchar(1000),
@onboardDate datetime,
@comments varchar(4000)
AS
set nocount on
IF (select count(1) from RM_Onboard_Client where  client_id = @clientId) > 0

BEGIN

UPDATE  RM_Onboard_Client set
max_hedge_percent = @maxHedgePercent,
contact_name = @contactName,
contact_office_phone = @contactOfficePhone,
contact_cell_phone = @contactCellPhone,
contact_fax_number = @contactFaxNumber,
contact_email = @contactEmail,
comments = @comments 
where client_id = @clientId

END

ELSE

BEGIN
insert into RM_Onboard_Client(
client_id,
max_hedge_percent,
contact_name,
contact_office_phone,
contact_cell_phone,
contact_fax_number,
contact_email,
onboard_date,
comments)
values(@clientId,
@maxHedgePercent,
@contactName,
@contactOfficePhone,
@contactCellPhone,
@contactFaxNumber,
@contactEmail,
getDate(),
@comments)

END
GO
GRANT EXECUTE ON  [dbo].[STORE_ONBOARD_CLIENT_SETUP_P] TO [CBMSApplication]
GO
