
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
    
NAME: [DBO].Report_SOPS_Supplier_Data_010509      
         
DESCRIPTION:    
    Used in DTS Package SOPS_Supplier_Data_010509 to get Vendor Contract Info    
    
INPUT PARAMETERS:              
NAME   DATATYPE DEFAULT  DESCRIPTION              
------------------------------------------------------------              
    
OUTPUT PARAMETERS:    
NAME   DATATYPE DEFAULT  DESCRIPTION    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
EXECUTE dbo.Report_SOPS_Supplier_Data_010509    
    
AUTHOR INITIALS:    
INITIALS NAME    
------------------------------------------------------------    
AP  Athmaram Pabbathi    
AKR Ashok Kumar Raju
    
MODIFICATIONS    
INITIALS DATE      MODIFICATION    
------------------------------------------------------------    
AP      2012-04-19    Addnl Data Changes    
        -- Created to Replace inline SQL used in DTS Package SOPS_Supplier_Data_010509    
        -- Replaced Account, SAMM, Client, Site, Vendor, Address, State, Region, Division table with Core.Client_Hier & Core.Client_Hier_Account table    
        -- Replaced Cost_Usage table with Cost_Usage_Account_Dtl    
AKR     2015-01-06    Added State_Name         
*/    
CREATE PROCEDURE [dbo].[Report_SOPS_Supplier_Data_010509]  
AS   
BEGIN    
      SET NOCOUNT ON    
    
      SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED    
        
           DECLARE  
            @Ind_Client_Type_Id INT  
           ,@Ind_Comm_Client_Type_Id INT  
           ,@NG_Commodity_Id INT  
           ,@EL_Commodity_Id INT  
           ,@Currency_Unit_Id INT    
             
        
      IF 1 = 2  
      BEGIN  
      SELECT cast(NULL AS VARCHAR(200)) AS Vendor  
           ,cast(NULL AS DATETIME) AS ContractStart  
           ,cast(NULL AS DATETIME) AS ContractEnd  
           ,cast(NULL AS INT) AS TotalInvoicesReceived  
           ,cast(NULL AS INT) AS TotalInvoicesExpected  
           ,cast(NULL AS DECIMAL(28,10)) AS Total_Cost  
           ,cast(NULL AS VARCHAR(200)) AS Region  
           ,cast(NULL AS VARCHAR(2)) AS Commodity  
           ,cast(NULL AS VARCHAR(150)) AS Ed_Contract_Number       
      END   
               
      CREATE TABLE #Client_Hier_Account   
            (   
             Client_Hier_Id INT  
            ,Site_Id INT  
            ,Account_Id INT  
            ,Client_Currency_Group_Id INT  
            ,Vendor VARCHAR(200)  
            ,Ed_Contract_Number VARCHAR(50)  
            ,ContractStart DATETIME  
            ,ContractEnd DATETIME  
            ,Region VARCHAR(200) 
            ,State_Name VARCHAR(200) 
            )    
    
                
      SELECT  
            @Ind_Client_Type_Id = max(case WHEN ct.Entity_Name = 'Industrial' THEN ct.Entity_Id  
                                      END)  
           ,@Ind_Comm_Client_Type_Id = max(case WHEN ct.Entity_Name = 'Industrial with Commercial' THEN ct.Entity_Id  
                                           END)  
      FROM  
            dbo.Entity ct  
      WHERE  
            ct.Entity_Name IN ( 'Industrial', 'Industrial with Commercial' )  
            AND ct.Entity_Description = 'Client Type'    
    
      SELECT  
            @Currency_Unit_Id = cu.Currency_Unit_Id  
      FROM  
            dbo.Currency_Unit cu  
      WHERE  
            cu.Currency_Unit_Name = 'USD'    
    
      SELECT  
            @NG_Commodity_Id = max(case WHEN com.Commodity_Name = 'Natural Gas' THEN com.Commodity_Id  
                                   END)  
           ,@EL_Commodity_Id = max(case WHEN com.Commodity_Name = 'Electric Power' THEN com.Commodity_Id  
                                   END)  
      FROM  
            dbo.Commodity com  
      WHERE  
            com.Commodity_Name IN ( 'Natural Gas', 'Electric Power' )    
    
                            
      INSERT      INTO #Client_Hier_Account  
                  (   
                   Vendor  
                  ,ContractStart  
                  ,ContractEnd  
                  ,Region  
                  ,Site_Id  
                  ,Client_Hier_Id  
                  ,Account_Id  
                  ,Client_Currency_Group_Id  
                  ,Ed_Contract_Number
                  ,State_Name )  
                  SELECT  
                        cha.Account_Vendor_Name AS Vendor  
                       ,con.Contract_Start_Date AS ContractStart  
                       ,con.Contract_End_Date AS ContractEnd  
                       ,ch.Region_Name AS Region  
                       ,ch.Site_Id  
                       ,ch.Client_Hier_Id  
                       ,cha.Account_Id  
                       ,ch.Client_Currency_Group_Id  
                       ,con.Ed_Contract_Number  
                       ,ch.State_Name
                  FROM  
                        Core.Client_Hier ch  
                        INNER JOIN Core.Client_Hier_Account cha  
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id  
                        INNER JOIN dbo.[Contract] con  
                              ON con.Contract_Id = cha.Supplier_Contract_Id  
                  WHERE  
                        cha.Account_Type = 'Supplier'  
                        AND cha.Commodity_Id IN ( @NG_Commodity_Id, @EL_Commodity_Id )  
                        AND ch.Client_Type_Id IN ( @Ind_Client_Type_Id, @Ind_Comm_Client_Type_Id )  
                        AND ( ( cha.Supplier_Account_Recalc_Type_Dsc != 'No Recalc - Commercial'  
                                AND cha.Supplier_Account_Recalc_Type_Cd != '100241'  
                                AND ch.Client_Name = 'AT&T Services, Inc.' )  
                              OR ch.Client_Name != 'AT&T Services, Inc.' )  
                  GROUP BY  
                        cha.Account_Vendor_Name  
                       ,con.Contract_Start_Date  
                       ,con.Contract_End_Date  
                       ,ch.Region_Name  
                       ,ch.Site_Id  
                       ,ch.Client_Hier_Id  
                       ,cha.Account_Id  
                       ,ch.Client_Currency_Group_Id  
                       ,con.Ed_Contract_Number  
                       ,ch.State_Name  
                           
CREATE CLUSTERED INDEX ix_Client_Hier_Account_Client_Hier_Id_Site_Id_Account_Id ON #Client_Hier_Account(Client_Hier_Id, Site_Id, Account_Id)  
    
      SELECT  
            cha.Vendor  
           ,cha.ContractStart  
           ,cha.ContractEnd  
           ,sum(cast(ip.Is_Received AS INT)) AS TotalInvoicesReceived  
           ,sum(cast(ip.Is_Expected AS INT)) AS TotalInvoicesExpected  
           ,sum(cuad.Bucket_Value * cur_conv.Conversion_Factor) AS Total_Cost  
           ,cha.Region  
           ,cha.State_Name State
           ,( case WHEN bm.Commodity_Id = @EL_Commodity_Id THEN 'EP'  
                   WHEN bm.Commodity_Id = @NG_Commodity_Id THEN 'NG'  
              END ) AS Commodity  
           ,cha.Ed_Contract_Number  
      FROM  
            #Client_Hier_Account cha  
            INNER JOIN dbo.Cost_Usage_Account_Dtl cuad  
                  ON cuad.Account_Id = cha.Account_Id  
                     AND cuad.Client_Hier_Id = cha.Client_Hier_Id  
            INNER JOIN dbo.Bucket_Master bm  
                  ON bm.Bucket_Master_Id = cuad.Bucket_Master_Id  
            INNER JOIN dbo.Currency_Unit_Conversion cur_conv  
                  ON cur_conv.Base_Unit_Id = cuad.Currency_Unit_Id  
                     AND cur_conv.Converted_Unit_Id = @Currency_Unit_Id  
                     AND cur_conv.Conversion_Date = cuad.Service_Month  
                     AND cur_conv.Currency_Group_Id = cha.Client_Currency_Group_Id  
            INNER JOIN dbo.Invoice_Participation ip  
                  ON ip.Account_Id = cha.Account_Id  
                     AND ip.Site_Id = cha.Site_Id  
                     AND ip.Service_Month = cuad.Service_Month  
      WHERE  
            cuad.Service_Month BETWEEN dateadd(m, -25, getdate())  
                               AND     dateadd(m, -1, getdate())  
            AND bm.Commodity_Id IN ( @EL_Commodity_Id, @NG_Commodity_Id )  
            AND bm.Bucket_Name = 'Total Cost'  
      GROUP BY  
            cha.Vendor  
           ,cha.ContractStart  
           ,cha.ContractEnd  
           ,cha.Region  
           ,cha.State_Name
			,bm.Commodity_Id  
           ,cha.Ed_Contract_Number    
    
    
END    
;
;
GO

GRANT EXECUTE ON  [dbo].[Report_SOPS_Supplier_Data_010509] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_SOPS_Supplier_Data_010509] TO [CBMSApplication]
GO
