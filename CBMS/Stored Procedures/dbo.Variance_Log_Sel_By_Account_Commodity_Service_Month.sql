
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
    
/******
NAME:
	CBMS.dbo.Variance_Log_Sel_By_Account_Commodity_Service_Month

DESCRIPTION:
	Used to select variance log details for the given account, commodity and service month
	Created as a part of other services.

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Account_Id	    INT
	@Commodity_id	INT
	@Service_Month	DATE

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC dbo.Variance_Log_Sel_By_Account_Commodity_Service_Month 125493,290,'2010-06-01'
	EXEC dbo.Variance_Log_Sel_By_Account_Commodity_Service_Month 198867,290,'2010-04-01'
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hari
	AP		    Athmaram Pabbathi
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG       	03/10/2010	Created
	HG			04/07/2010	queue table Join removed as we have queue_name saved in Variance_Log itself.
	AP			08/18/2011	Replaced COST_USAGE_ID with @Account_Id, @Commodity_Id and service_month Column as part of ADLDT changes.
******/

CREATE PROCEDURE dbo.Variance_Log_Sel_By_Account_Commodity_Service_Month
      ( 
       @Account_Id INT
      ,@Commodity_id INT
      ,@Service_Month DATE )
AS 
BEGIN

      SET NOCOUNT ON ;

      SELECT
            l.Execution_Dt
           ,l.is_failure
           ,l.closed_dt
           ,l.queue_id
           ,l.queue_name
           ,l.queue_dt
      FROM
            dbo.Variance_Log l
      WHERE
            l.Account_id = @Account_Id
            AND l.Commodity_ID = @Commodity_id
            AND l.Service_Month = @Service_Month
      GROUP BY
            l.Execution_Dt
           ,l.is_failure
           ,l.closed_dt
           ,l.queue_id
           ,l.queue_name
           ,l.queue_dt

END
;
GO

GRANT EXECUTE ON  [dbo].[Variance_Log_Sel_By_Account_Commodity_Service_Month] TO [CBMSApplication]
GO
