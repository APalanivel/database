SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_DEAL_TICKET_CONTACT_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@dealTicketId  	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE      PROCEDURE DBO.GET_DEAL_TICKET_CONTACT_INFO_P

@userId varchar,
@sessionId varchar,
@dealTicketId int

AS
set nocount on
	SELECT 
	c.CLIENT_NAME, 
	s.SITE_NAME, 
	d.DIVISION_NAME, 
	rmdt.CONTACT_NAME, 
	rmdt.CONTACT_PHONE, 
	rmdt.CONTACT_CELL, 
	rmdt.CONTACT_FAX, 
	rmdt.CONTACT_EMAIL, 
	rmdt.RM_COUNTERPARTY_ID, 
	rmcp.COUNTERPARTY_NAME, 
	rmdt.COUNTERPARTY_CONTACT_NAME, 
	rmdt.COUNTERPARTY_CONTACT_PHONE, 
	rmdt.COUNTERPARTY_CONTACT_CELL, 
	rmdt.COUNTERPARTY_CONTACT_FAX, 
	rmdt.COUNTERPARTY_CONTACT_EMAIL, 
	v.VENDOR_ID, 
	v.VENDOR_NAME,
	e.ENTITY_NAME AS HEDGE_TYPE_NAME,
	rmgroup.GROUP_NAME AS GROUP_NAME
	  
FROM 
	CLIENT c, 
	RM_DEAL_TICKET rmdt
		LEFT JOIN DIVISION d ON (rmdt.DIVISION_ID = d.DIVISION_ID)
		LEFT JOIN SITE s ON (rmdt.SITE_ID = s.SITE_ID)
		LEFT JOIN RM_COUNTERPARTY rmcp ON (rmdt.RM_COUNTERPARTY_ID = rmcp.RM_COUNTERPARTY_ID)
		LEFT JOIN VENDOR v ON (rmdt.VENDOR_ID = v.VENDOR_ID)
		LEFT JOIN RM_GROUP rmgroup ON (rmdt.RM_GROUP_ID = rmgroup.RM_GROUP_ID)
		LEFT JOIN ENTITY e ON (rmdt.HEDGE_LEVEL_TYPE_ID = e.ENTITY_ID)  

WHERE 
	rmdt.RM_DEAL_TICKET_ID = @dealTicketId AND 
	rmdt.CLIENT_ID = c.CLIENT_ID
GO
GRANT EXECUTE ON  [dbo].[GET_DEAL_TICKET_CONTACT_INFO_P] TO [CBMSApplication]
GO
