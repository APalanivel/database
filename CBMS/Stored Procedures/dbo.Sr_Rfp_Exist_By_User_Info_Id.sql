SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
   dbo.Sr_Rfp_Exist_By_User_Info_Id  
  
DESCRIPTION:    
		Additional validation needed for Move User to History to check whether RFP's exist for a user.
  
INPUT PARAMETERS:    
Name					DataType			Default			Description    
-------------------------------------------------------------------------------- 
@User_Info_Id			INT

OUTPUT PARAMETERS:    
Name					DataType			Default			Description    
--------------------------------------------------------------------------------  
  
USAGE EXAMPLES:    
--------------------------------------------------------------------------------  

EXEC dbo.Sr_Rfp_Exist_By_User_Info_Id 49

EXEC dbo.Sr_Rfp_Exist_By_User_Info_Id 16



EXEC dbo.Sr_Rfp_Exist_By_User_Info_Id 60643
  
   
AUTHOR INITIALS:    
Initials	Name    
--------------------------------------------------------------------------------  
NR			Narayana Reddy  
  
MODIFICATIONS     
Initials	Date			Modification    
--------------------------------------------------------------------------------  
NR			2016-04-01		MAINT-3918 Created new sproc to add one more validition 
							when User is  Move to history.
RKV         2018-09-04      MAINT-7697 Added Exists clause to check for the active accounts 

******/

CREATE PROCEDURE [dbo].[Sr_Rfp_Exist_By_User_Info_Id]
     (
         @User_Info_Id INT
     )
AS
    BEGIN

        SET NOCOUNT ON;



        DECLARE @Is_RFP_Exist BIT = 0;
        SELECT
            @Is_RFP_Exist = 1
        FROM
            dbo.SR_RFP sr
            INNER JOIN dbo.USER_INFO ui
                ON sr.Queue_Id = ui.QUEUE_ID
            INNER JOIN dbo.ENTITY e
                ON sr.RFP_STATUS_TYPE_ID = e.ENTITY_ID
        WHERE
            ui.USER_INFO_ID = @User_Info_Id
            AND e.ENTITY_NAME IN ( 'Open', 'New' )
            AND e.ENTITY_DESCRIPTION = 'RFP Status'
            AND EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.SR_RFP_ACCOUNT sra
                               WHERE
                                    sr.SR_RFP_ID = sra.SR_RFP_ID
                                    AND sra.IS_DELETED = 0);



        SELECT  @Is_RFP_Exist AS Is_RFP_Exist;

    END;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Exist_By_User_Info_Id] TO [CBMSApplication]
GO
