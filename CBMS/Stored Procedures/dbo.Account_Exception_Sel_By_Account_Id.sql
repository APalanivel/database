SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Account_Exception_Sel_By_Account_Id         
                
Description:                
   This sproc to get all exceptions of a given account id.        
                             
 Input Parameters:                
    Name        DataType   Default   Description                  
----------------------------------------------------------------------------------------              
    @Account_Id       INT       
    @Queue_Id       INT     NULL  
                            
   
 Output Parameters:                      
    Name        DataType   Default   Description                  
----------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
----------------------------------------------------------------------------------------     
 SELECT * FROM dbo.Account_Exception  
   
 EXEC dbo.Account_Exception_Sel_By_Account_Id 1080008,116979  
 EXEC dbo.Account_Exception_Sel_By_Account_Id 1342212  
     
Author Initials:                
    Initials  Name                
----------------------------------------------------------------------------------------                  
 NR    Narayana Reddy  
 RKV    Ravi Kumar vegesna      
 SP    Sandeep Pigilam  
              
 Modifications:                
    Initials        Date   Modification                
----------------------------------------------------------------------------------------                  
    NR    2015-04-22  Created For AS400.           
    RKV             2015-09-04      Modified the join filter on Client_Hier_Account as part of AS400-PII    
    SP    2016-12-08  Invoice tracking modified codeset of exception type to desc. Added @Queue_Id as input param.        
         ADDED @Is_Missing_IC_Included INT = NULL  
 RKV    2018-12-27  MAINT-8064 @Invoice_Collection_Officer_User_Id is modified to assign based on the Account_Id from Invoice_Collection_Account_Config Table.  
 NR    2019-07-23  Add Contract -  Added Service_Desk_Id column. 
 RKV   2020-05-11  Add Contract - Added filter Supplier begin and End dates 
******/

CREATE PROCEDURE [dbo].[Account_Exception_Sel_By_Account_Id]
    (
        @Account_Id INT
        , @Queue_Id INT = NULL
        , @Is_Missing_IC_Included INT = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Exception_Type_Cd INT;





        SELECT
            @Exception_Type_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Type'
            AND c.Code_Value = 'Missing IC Data';


        SELECT
            ae.Account_Exception_Id
            , ae.Meter_Id
            , ae.Account_Id
            , CASE WHEN ae.Meter_Id = -1
                        AND TypeCd.Code_Dsc <> 'Missing Invoice Collection Data' THEN ''
                  WHEN ae.Meter_Id = -1
                       AND  TypeCd.Code_Dsc = 'Missing Invoice Collection Data' THEN 'Multiple'
                  ELSE cha.Meter_Number
              END AS Meter_Number
            , CASE WHEN ica.Invoice_Collection_Account_Config_Id IS NULL
                        AND (   ica.Invoice_Collection_Service_Start_Dt IS NULL
                                OR  ica.Invoice_Collection_Service_End_Dt IS NULL) THEN TypeCd.Code_Dsc
                  ELSE
                      TypeCd.Code_Dsc
                      + CASE WHEN ica.Invoice_Collection_Service_Start_Dt IS NOT NULL
                                  AND  ica.Invoice_Collection_Service_End_Dt IS NOT NULL THEN
                                 ' ('
                                 + REPLACE(CONVERT(NVARCHAR, ica.Invoice_Collection_Service_Start_Dt, 106), ' ', '-')
                                 + ' to '
                                 + REPLACE(CONVERT(NVARCHAR, ica.Invoice_Collection_Service_End_Dt, 106), ' ', '-')
                                 + ')'
                        END
              END AS Exception_Type
            , StatusCd.Code_Value AS Exception_Status
            , created.FIRST_NAME + ' ' + created.LAST_NAME AS Opened_By
            , REPLACE(
                  CONVERT(
                      NVARCHAR
                      , (CASE WHEN @Exception_Type_Cd = ae.Exception_Type_Cd THEN
                                  ISNULL(MAX(ica.Created_Ts), ae.Exception_Created_Ts)
                             ELSE ae.Exception_Created_Ts
                         END), 106), ' ', '-') AS Opened_Ts
            , closed.FIRST_NAME + ' ' + closed.LAST_NAME AS Closed_By
            , REPLACE(CONVERT(NVARCHAR, ae.Closed_Ts, 106), ' ', '-') AS Closed_Ts
            , CASE WHEN ae.Meter_Id = -1 THEN ''
                  ELSE cha.Meter_Address_Line_1
              END AS Meter_Address_Line_1
            , CASE WHEN ae.Meter_Id = -1 THEN ''
                  ELSE cha.Meter_State_Id
              END AS Meter_State_Id
            , comm.Commodity_Id
            , CASE WHEN ae.Exception_Type_Cd = @Exception_Type_Cd
                        AND comm.Commodity_Name = 'Not Applicable' THEN 'Multiple'
                  ELSE comm.Commodity_Name
              END AS Commodity_Name
            , CASE WHEN ae.Meter_Id = -1 THEN ''
                  ELSE cha.Rate_Name
              END AS Rate_Name
            , ch.Client_Id
            , CASE WHEN ae.Exception_Type_Cd = @Exception_Type_Cd
                        AND COUNT(ch.Site_Id) > 1 THEN 0
                  ELSE MAX(ch.Site_Id)
              END AS Site_Id
            , CASE WHEN ae.Exception_Type_Cd = @Exception_Type_Cd
                        AND COUNT(ch.Sitegroup_Id) > 1 THEN 0
                  ELSE MAX(ch.Sitegroup_Id)
              END AS Sitegroup_Id
            , cha.Account_Vendor_Id
            , ica.Invoice_Collection_Account_Config_Id
            , MAX(cha.Supplier_Contract_ID) Supplier_Contract_ID
            , ae.Service_Desk_Ticket_XId
        FROM
            dbo.Account_Exception ae
            INNER JOIN Core.Client_Hier_Account cha
                ON ae.Account_Id = cha.Account_Id
                   AND  (   ae.Meter_Id = cha.Meter_Id
                            OR  ae.Meter_Id = -1)
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN dbo.Commodity comm
                ON ae.Commodity_Id = comm.Commodity_Id
            INNER JOIN dbo.Code TypeCd
                ON TypeCd.Code_Id = ae.Exception_Type_Cd
            INNER JOIN dbo.Code StatusCd
                ON StatusCd.Code_Id = ae.Exception_Status_Cd
            LEFT JOIN dbo.USER_INFO closed
                ON ae.Closed_By_User_Id = closed.USER_INFO_ID
            LEFT JOIN(dbo.Invoice_Collection_Account_Config ica
                      INNER JOIN USER_INFO ico_ui
                          ON ico_ui.USER_INFO_ID = ica.Invoice_Collection_Officer_User_Id)
                ON ae.Account_Id = ica.Account_Id
                   AND  TypeCd.Code_Value = 'Missing IC Data'
                   AND  ico_ui.QUEUE_ID = ae.Queue_Id
                   AND  ica.is_Config_Complete = 0
            INNER JOIN dbo.USER_INFO created
                ON (CASE WHEN @Exception_Type_Cd = ae.Exception_Type_Cd THEN
                             ISNULL(ica.Created_User_Id, ae.Exception_By_User_Id)
                        ELSE ae.Exception_By_User_Id
                    END) = created.USER_INFO_ID
        WHERE
            ae.Account_Id = @Account_Id
            AND (   cha.Account_Type = 'Utility'
                    OR  (   cha.Account_Type = 'Supplier'
                            AND (   cha.Supplier_Account_begin_Dt BETWEEN ica.Invoice_Collection_Service_Start_Dt
                                                                  AND     ica.Invoice_Collection_Service_End_Dt
                                    OR  cha.Supplier_Account_End_Dt BETWEEN ica.Invoice_Collection_Service_Start_Dt
                                                                    AND     ica.Invoice_Collection_Service_End_Dt
                                    OR  ica.Invoice_Collection_Service_Start_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                                                                AND     cha.Supplier_Account_End_Dt
                                    OR  ica.Invoice_Collection_Service_End_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                                                              AND     cha.Supplier_Account_End_Dt)))
            AND (   @Queue_Id IS NULL
                    OR  ae.Queue_Id = @Queue_Id)
            AND (   @Is_Missing_IC_Included IS NULL
                    OR  (   @Is_Missing_IC_Included = 1
                            AND ae.Exception_Type_Cd = @Exception_Type_Cd)
                    OR  (   @Is_Missing_IC_Included = 0
                            AND ae.Exception_Type_Cd <> @Exception_Type_Cd))
        GROUP BY
            ae.Account_Exception_Id
            , ae.Meter_Id
            , ae.Account_Id
            , CASE WHEN ae.Meter_Id = -1
                        AND TypeCd.Code_Dsc <> 'Missing Invoice Collection Data' THEN ''
                  WHEN ae.Meter_Id = -1
                       AND  TypeCd.Code_Dsc = 'Missing Invoice Collection Data' THEN 'Multiple'
                  ELSE cha.Meter_Number
              END
            , TypeCd.Code_Dsc
            , StatusCd.Code_Value
            , created.FIRST_NAME + ' ' + created.LAST_NAME
            , ae.Exception_Created_Ts
            , closed.FIRST_NAME + ' ' + closed.LAST_NAME
            , ae.Closed_Ts
            , CASE WHEN ae.Meter_Id = -1 THEN ''
                  ELSE cha.Meter_Address_Line_1
              END
            , CASE WHEN ae.Meter_Id = -1 THEN ''
                  ELSE cha.Meter_State_Id
              END
            , comm.Commodity_Id
            , comm.Commodity_Name
            , CASE WHEN ae.Meter_Id = -1 THEN ''
                  ELSE cha.Rate_Name
              END
            , ch.Client_Id
            , cha.Account_Vendor_Id
            , ica.Invoice_Collection_Account_Config_Id
            , ica.Invoice_Collection_Service_Start_Dt
            , ica.Invoice_Collection_Service_End_Dt
            , ae.Exception_Type_Cd
            , ae.Service_Desk_Ticket_XId;

    END;









--GO
GO


GRANT EXECUTE ON  [dbo].[Account_Exception_Sel_By_Account_Id] TO [CBMSApplication]
GO
