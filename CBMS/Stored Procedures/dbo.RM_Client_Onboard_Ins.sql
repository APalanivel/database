SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        dbo.RM_Client_Onboard_Ins                    
                      
Description:                      
        To on board selected commodity and countries
                      
Input Parameters:                      
    Name				DataType        Default     Description                        
--------------------------------------------------------------------------------
	@Country_Id			VARCHAR(MAX)
    @Commodity_Id		INT
    @User_Info_Id		INT 
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	@RM_Group_Id	INT  
                    
Usage Examples:                          
--------------------------------------------------------------------------------
	
	BEGIN TRANSACTION
		EXEC dbo.RM_Client_Onboard_Ins 235,291,'14',16,'2018-01-01'
		SELECT a.*,c.* FROM trade.RM_Client_Hier_Onboard a INNER JOIN Core.Client_Hier b ON a.Client_Hier_Id = b.Client_Hier_Id
			INNER JOIN trade.RM_Client_Hier_Hedge_Config c ON a.RM_Client_Hier_Onboard_Id = c.RM_Client_Hier_Onboard_Id
			WHERE b.Client_Id = 235 AND b.Sitegroup_Id = 0
	ROLLBACK TRANSACTION     
                     
 Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy        
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			2018-30-07     Global Risk Management - Created                    
                     
******/
CREATE PROCEDURE [dbo].[RM_Client_Onboard_Ins]
    (
        @Client_Id INT
        , @Commodity_Id INT
        , @Country_Id VARCHAR(MAX)
        , @User_Info_Id INT
        , @RM_Default_Config_Start_Dt DATE = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @RM_Hedge_Config_Id INT
            , @Does_not_hedge_Entity INT
            , @RM_Risk_Tolerance_Category_Undefined INT
            , @Client_Client_Hier_Id INT
            --,@RM_Default_Config_Start_Dt DATE
            , @RM_Default_Config_End_Dt DATE
            , @Cnt INT
            , @Row_No INT
            , @RM_Client_Hier_Onboard_Id INT;

        DECLARE @RM_Client_Onboard_Tbl AS TABLE
              (
                  Row_No INT IDENTITY(1, 1)
                  , RM_Client_Hier_Onboard_Id INT
              );

        DECLARE @Country_Tbl AS TABLE
              (
                  Country_Id INT
              );

        --SELECT
        --      @RM_Default_Config_Start_Dt = CAST(App_Config_Value AS DATE)
        --FROM
        --      dbo.App_Config
        --WHERE
        --      App_Config_Cd = 'RM_Default_Config_Start_Dt'
        --      AND App_Id = -1

        SELECT
            @RM_Default_Config_End_Dt = CAST(App_Config_Value AS DATE)
        FROM
            dbo.App_Config
        WHERE
            App_Config_Cd = 'RM_Default_Config_End_Dt'
            AND App_Id = -1;

        SELECT
            @Client_Client_Hier_Id = Client_Hier_Id
        FROM
            Core.Client_Hier
        WHERE
            Client_Id = @Client_Id
            AND Sitegroup_Id = 0;

        INSERT INTO @Country_Tbl
             (
                 Country_Id
             )
        SELECT
            CAST(Segments AS INT) AS Country_Id
        FROM
            dbo.ufn_split(@Country_Id, ',')
        GROUP BY
            Segments;

        INSERT INTO Trade.RM_Client_Hier_Onboard
             (
                 Client_Hier_Id
                 , Country_Id
                 , Commodity_Id
                 , Created_User_Id
                 , Created_Ts
                 , Updated_User_Id
                 , Last_Change_Ts
                 , RM_Forecast_UOM_Type_Id
             )
        OUTPUT
            INSERTED.RM_Client_Hier_Onboard_Id
        INTO @RM_Client_Onboard_Tbl (RM_Client_Hier_Onboard_Id)
        SELECT
            @Client_Client_Hier_Id
            , ct.Country_Id
            , @Commodity_Id
            , @User_Info_Id
            , GETDATE()
            , @User_Info_Id
            , GETDATE()
            , NULL AS RM_Forecast_UOM_Type_Id
        FROM
            @Country_Tbl ct
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                Trade.RM_Client_Hier_Onboard rco
                           WHERE
                                rco.Client_Hier_Id = @Client_Client_Hier_Id
                                AND rco.Country_Id = ct.Country_Id
                                AND rco.Commodity_Id = @Commodity_Id);


        SELECT
            @Does_not_hedge_Entity = ENTITY_ID
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_TYPE = 273
            AND ENTITY_NAME = 'Does not hedge';


        SELECT
            @RM_Risk_Tolerance_Category_Undefined = RM_Risk_Tolerance_Category_Id
        FROM
            Trade.RM_Risk_Tolerance_Category
        WHERE
            RM_Risk_Tolerance_Category = 'Undefined';

        INSERT INTO Trade.RM_Client_Hier_Hedge_Config
             (
                 RM_Client_Hier_Onboard_Id
                 , Config_Start_Dt
                 , Config_End_Dt
                    --,RM_Forecast_UOM_Type_Id
                 , Hedge_Type_Id
                 , Max_Hedge_Pct
                 , RM_Risk_Tolerance_Category_Id
                 , Risk_Manager_User_Info_Id
                 , Contact_Info_Id
                 , Include_In_Reports
                 , Is_Default_Config
                 , Created_User_Id
                 , Created_Ts
                 , Last_Updated_By
                 , Last_Updated_Ts
             )
        SELECT
            new.RM_Client_Hier_Onboard_Id
            , @RM_Default_Config_Start_Dt AS Config_Start_Dt
            , @RM_Default_Config_End_Dt AS Config_End_Dt
            --,NULL AS RM_Forecast_UOM_Type_Id
            , @Does_not_hedge_Entity AS Hedge_Type_Id
            , NULL AS Max_Hedge_Pct
            , @RM_Risk_Tolerance_Category_Undefined AS RM_Risk_Tolerance_Category_Id
            , NULL AS Risk_Manager_User_Info_Id
            , NULL AS Contact_Info_Id
            , 0 AS Include_In_Reports
            , 1 AS Is_Default_Config
            , @User_Info_Id AS Created_User_Id
            , GETDATE() AS Created_Ts
            , @User_Info_Id AS Last_Updated_By
            , GETDATE() AS Last_Updated_Ts
        FROM
            @RM_Client_Onboard_Tbl new;

        INSERT INTO dbo.RM_Client_Workflow
             (
                 RM_Client_Hier_Onboard_Id
                 , Hedge_Type_Id
                 , Workflow_Id
                 , Created_User_Id
                 , Created_Ts
                 , Updated_User_Id
                 , Last_Change_Ts
             )
        SELECT
            new.RM_Client_Hier_Onboard_Id
            , rdw.Hedge_Type_Id
            , rdw.Workflow_Id
            , rdw.Updated_User_Id
            , GETDATE()
            , rdw.Updated_User_Id
            , GETDATE()
        FROM
            @RM_Client_Onboard_Tbl new
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON new.RM_Client_Hier_Onboard_Id = chob.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.RM_Default_Workflow rdw
                ON chob.Country_Id = rdw.Country_Id
                   AND  chob.Commodity_Id = rdw.Commodity_Id;


        SELECT  @Cnt = MAX(Row_No)FROM  @RM_Client_Onboard_Tbl;

        SELECT  @Row_No = MIN(Row_No)FROM   @RM_Client_Onboard_Tbl;



    --WHILE ( @Row_No <= @Cnt ) 
    --      BEGIN
    --            SELECT
    --                  @RM_Client_Hier_Onboard_Id = RM_Client_Hier_Onboard_Id
    --            FROM
    --                  @RM_Client_Onboard_Tbl
    --            WHERE
    --                  Row_No = @Row_No
    --            EXEC dbo.RM_Forecast_Volume_Load 
    --                  @RM_Client_Hier_Onboard_Id = @RM_Client_Hier_Onboard_Id
    --            SELECT
    --                  @Row_No = @Row_No + 1
    --      END

    END;




GO
GRANT EXECUTE ON  [dbo].[RM_Client_Onboard_Ins] TO [CBMSApplication]
GO
