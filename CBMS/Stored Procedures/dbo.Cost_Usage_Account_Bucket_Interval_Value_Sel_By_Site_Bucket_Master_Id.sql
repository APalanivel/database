
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******                    
                     
NAME: [dbo].[Cost_Usage_Account_Bucket_Interval_Value_Sel_By_Site_Bucket_Master_Id]  
  
DESCRIPTION:  
  
          SP is used to get the Utility Account Interval Data Presence Status Per Commodity based on the Client_Hier_Id  
  
INPUT PARAMETERS:  
NAME                        DATATYPE          DEFAULT        DESCRIPTION  
------------------------------------------------------------                       
@Client_Hier_Id    INT  
@Service_Start_Dt   DATE  
@Service_End_Dt    DATE  
@Bucket_Master_Id   INT  
@Bucket_Type    VARCHAR(200)  
@Uom_Type_Id    INT     
@Currency_Unit_Id   INT    
@Account_Type    VARCHAR(200)    
@Data_Source    VARCHAR(10)                        
  
OUTPUT PARAMETERS:                                      
NAME   DATATYPE DEFAULT  DESCRIPTION                               
                                   
------------------------------------------------------------                                      
USAGE EXAMPLES:                                      
------------------------------------------------------------                              
      
    EXEC Cost_Usage_Account_Bucket_Interval_Value_Sel_By_Site_Bucket_Master_Id   1131,'2011-03-01', '2011-03-31',100992,'2011-03-31',null,NULL,NULL,'Invoice'  
 EXEC Cost_Usage_Account_Bucket_Interval_Value_Sel_By_Site_Bucket_Master_Id   1133,'2011-03-01', '2011-03-31',100992,'2011-03-31',null,NULL,NULL,'Invoice'  
                 
    SELECT top 100 * FROM Bucket_Account_Interval_Dtl a join code c on a.Data_Source_cd=c.code_id where Service_Start_Dt >= '2011-01-01'  
            AND Service_End_Dt <= '2011-12-31'   and client_Hier_Id = 1131  -- and  Data_Source_cd = 100350   
            and c.code_value IN ( 'CBMS', 'DE' )  
            select * from bucket_master where bucket_master_id=100349  
                     select * from code where code_id=100992  
 SELECT * FROM Bucket_Master where Bucket_Master_Id = 100992   
                                 
AUTHOR INITIALS:                                      
INITIALS  NAME                                      
------------------------------------------------------------                                      
 PKY      Pavan Kumar Yadalam  
  
MODIFICATIONS  
INITIALS DATE(YYYY-MM-DD)  MODIFICATION  
------------------------------------------------------------   
PKY       2013-05-02        Created
SP		  2017-06-27		Data Estimation,Added Actual  
*/          
CREATE PROCEDURE [dbo].[Cost_Usage_Account_Bucket_Interval_Value_Sel_By_Site_Bucket_Master_Id]
      ( 
       @Client_Hier_Id INT
      ,@Service_Start_Dt DATE
      ,@Service_End_Dt DATE
      ,@Bucket_Master_Id INT
      ,@Bucket_Type VARCHAR(200)
      ,@Uom_Type_Id INT = NULL
      ,@Currency_Unit_Id INT = NULL
      ,@Account_Type VARCHAR(200) = NULL
      ,@Data_Source VARCHAR(10) )
AS 
BEGIN          
          
      SET NOCOUNT ON;          
         
      DECLARE @Data_Type VARCHAR(25)
    
   -- To save the account list of given site, as client_hier_account will have more than one row for account based on the no of meter added to it            
      DECLARE @Account_List TABLE
            ( 
             Account_Id INT PRIMARY KEY CLUSTERED
            ,Account_Type VARCHAR(200)
            ,Currency_Group_Id INT )            
            
      SELECT
            @Data_Type = c.Code_Value
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Std_Column_Name = 'Data_Type_Cd'
            AND cs.Codeset_Name = 'Data_Type'
            AND c.Code_Value = 'Actual'
     
     
      INSERT      INTO @Account_List
                  ( 
                   Account_Id
                  ,Account_Type
                  ,Currency_Group_Id )
                  SELECT
                        cha.Account_Id
                       ,cha.Account_Type
                       ,ch.Client_Currency_Group_Id
                  FROM
                        core.Client_hier ch
                        INNER JOIN core.Client_Hier_Account cha
                              ON cha.Client_hier_Id = ch.Client_Hier_Id
                  WHERE
                        ch.Client_Hier_Id = @Client_hier_Id
                        AND ( @Account_Type IS NULL
                              OR CHA.Account_Type = @Account_Type )
                  GROUP BY
                        cha.Account_Id
                       ,cha.Account_Type
                       ,ch.Client_Currency_Group_Id            
            
      SELECT
            al.Account_Id
           ,al.Account_Type
           ,CASE WHEN @Bucket_Type = 'Determinant' THEN cua.Bucket_Daily_Avg_Value * UomConv.Conversion_Factor
                 WHEN @Bucket_Type = 'Charge' THEN cua.Bucket_Daily_Avg_Value * CurConv.Conversion_Factor
            END AS Bucket_Value
           ,CASE WHEN @Bucket_Type = 'Determinant' THEN cua.Bucket_Daily_Avg_Value * UomConv.Conversion_Factor
                 WHEN @Bucket_Type = 'Charge' THEN cua.Bucket_Daily_Avg_Value * CurConv.Conversion_Factor
            END AS Actual_Bucket_Value
           ,NULL AS Estimated_Bucket_Value
           ,@Data_Type AS Data_Type
      FROM
            dbo.Bucket_Account_Interval_Dtl cua
            INNER JOIN @Account_List al
                  ON al.Account_Id = cua.Account_ID
            INNER JOIN dbo.Code c
                  ON c.Code_Id = cua.Data_Source_Cd
            LEFT OUTER JOIN dbo.Consumption_Unit_Conversion UomConv
                  ON UomConv.Base_Unit_ID = cua.UOM_Type_Id
                     AND UomConv.Converted_Unit_ID = @UOM_Type_id
            LEFT OUTER JOIN dbo.Currency_Unit_Conversion CurConv
                  ON CurConv.Base_Unit_ID = cua.Currency_Unit_ID
                     AND CurConv.Converted_Unit_ID = @Currency_Unit_Id
                     AND curConv.Currency_Group_ID = al.Currency_Group_Id
                     AND curconv.CONVERSION_DATE = CONVERT(VARCHAR, MONTH(cua.Service_Start_Dt)) + '-01-' + CONVERT(VARCHAR, YEAR(cua.Service_Start_Dt))
      WHERE
            cua.Bucket_Master_Id = @Bucket_Master_Id
            AND cua.Service_Start_Dt <= @Service_Start_Dt
            AND cua.Service_End_Dt >= @Service_End_Dt
            AND cua.Client_Hier_Id = @Client_Hier_Id
            AND ( c.Code_Value = @Data_Source
                  OR ( c.Code_Value IN ( 'CBMS', 'DE' )
                       AND @Data_Source = 'Manual' ) )    
  
END;


;
GO

GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Bucket_Interval_Value_Sel_By_Site_Bucket_Master_Id] TO [CBMSApplication]
GO
