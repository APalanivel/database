SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
NAME:   dbo.DMO_Supplier_Account_Dates_Upd  
             
DESCRIPTION:               
   To update DMO supplier account end date  
     
INPUT PARAMETERS:              
 Name      DataType Default  Description    
---------------------------------------------------------------------------------    
 @Account_Id     INT  
    @Supplier_Account_End_Dt DATETIME  
      
  
  
OUTPUT PARAMETERS:  
 Name        DataType  Default  Description    
---------------------------------------------------------------------------------    
  USE CBMS
 USAGE EXAMPLES:  
---------------------------------------------------------------------------------    
  SELECT TOP 10 * FROM dbo.SUPPLIER_ACCOUNT_METER_MAP WHERE Contract_ID = -1  
 BEGIN TRANSACTION  
  SELECT acc.Supplier_Account_Begin_Dt,acc.Supplier_Account_End_Dt,samm.*   
   FROM dbo.ACCOUNT acc INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm ON acc.ACCOUNT_ID = samm.ACCOUNT_ID  
   WHERE acc.ACCOUNT_ID = 1149234  
  EXEC dbo.DMO_Supplier_Account_Dates_Upd 1149234,'2017-03-01 00:00:00.000',NULL  
  SELECT acc.Supplier_Account_Begin_Dt,acc.Supplier_Account_End_Dt,samm.*   
   FROM dbo.ACCOUNT acc INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm ON acc.ACCOUNT_ID = samm.ACCOUNT_ID  
   WHERE acc.ACCOUNT_ID = 1149234  
 ROLLBACK TRANSACTION  
  
 BEGIN TRANSACTION  
  SELECT acc.Supplier_Account_Begin_Dt,acc.Supplier_Account_End_Dt,samm.*   
   FROM dbo.ACCOUNT acc INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm ON acc.ACCOUNT_ID = samm.ACCOUNT_ID  
   WHERE acc.ACCOUNT_ID = 1149235  
  EXEC dbo.DMO_Supplier_Account_Dates_Upd 1149235,NULL,'2017-12-01 00:00:00.000',NULL,1,1,1,1  
  SELECT acc.Supplier_Account_Begin_Dt,acc.Supplier_Account_End_Dt,samm.*   
   FROM dbo.ACCOUNT acc INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm ON acc.ACCOUNT_ID = samm.ACCOUNT_ID  
   WHERE acc.ACCOUNT_ID = 1149235  
 ROLLBACK TRANSACTION  
   
    
 AUTHOR INITIALS:              
 Initials Name              
-------------------------------------------------------------              
 RR   Raghu Reddy  
 NR   Narayana Reddy  
  
 MODIFICATIONS:  
 Initials Date  Modification  
------------------------------------------------------------  
 RR   2017-03-07 Contract placeholder - CP-8 Created  
 RR   2017-04-24 MAINT-5226 Added input parameter @Supplier_Account_Recalc_Type_Cd  
 NR   2018-01-12 Data Interval - Added @Supplier_Account_Determinant_Source_Cd parameter.  
******/

CREATE PROCEDURE [dbo].[DMO_Supplier_Account_Dates_Upd]
    (
        @Account_Id INT
        , @Supplier_Account_Begin_Dt DATETIME = NULL
        , @Supplier_Account_End_Dt DATETIME = NULL
        , @Contract_Id INT = NULL
        , @Vendor_Id INT = NULL
        , @Supplier_Account_Recalc_Type_Cd INT = NULL
        , @Supplier_Account_Determinant_Source_Cd INT = NULL
        , @Supplier_Account_Config_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Min_Supplier_Account_Begin_Dt DATE
            , @Max_Supplier_Account_End_Dt DATE;

        SET @Supplier_Account_End_Dt = NULLIF(@Supplier_Account_End_Dt, '9999-12-31');

        UPDATE
            sac
        SET
            Supplier_Account_Begin_Dt = ISNULL(@Supplier_Account_Begin_Dt, sac.Supplier_Account_Begin_Dt)
            , Supplier_Account_End_Dt = ISNULL(@Supplier_Account_End_Dt, sac.Supplier_Account_End_Dt)
            , Contract_Id = ISNULL(@Contract_Id, sac.Contract_Id)
        FROM
            dbo.Supplier_Account_Config sac
        WHERE
            sac.Account_Id = @Account_Id
            AND sac.Supplier_Account_Config_Id = @Supplier_Account_Config_Id;



        SELECT
            @Min_Supplier_Account_Begin_Dt = MIN(sac.Supplier_Account_Begin_Dt)
            , @Max_Supplier_Account_End_Dt = MAX(sac.Supplier_Account_End_Dt)
        FROM
            dbo.Supplier_Account_Config sac
        WHERE
            sac.Account_Id = @Account_Id;


        UPDATE
            dbo.ACCOUNT
        SET
            Supplier_Account_Begin_Dt = @Min_Supplier_Account_Begin_Dt
            , Supplier_Account_End_Dt = @Max_Supplier_Account_End_Dt
            , VENDOR_ID = ISNULL(@Vendor_Id, VENDOR_ID)
            , Supplier_Account_Recalc_Type_Cd = ISNULL(
                                                    @Supplier_Account_Recalc_Type_Cd, Supplier_Account_Recalc_Type_Cd)
            , Supplier_Account_Determinant_Source_Cd = ISNULL(
                                                           @Supplier_Account_Determinant_Source_Cd
                                                           , Supplier_Account_Determinant_Source_Cd)
        WHERE
            ACCOUNT_ID = @Account_Id;


        UPDATE
            dbo.SUPPLIER_ACCOUNT_METER_MAP
        SET
            METER_ASSOCIATION_DATE = ISNULL(@Supplier_Account_Begin_Dt, METER_ASSOCIATION_DATE)
            , METER_DISASSOCIATION_DATE = ISNULL(@Supplier_Account_End_Dt, METER_DISASSOCIATION_DATE)
            , Contract_ID = ISNULL(@Contract_Id, Contract_ID)
        WHERE
            ACCOUNT_ID = @Account_Id
            AND Supplier_Account_Config_Id = @Supplier_Account_Config_Id;



    END;






GO
GRANT EXECUTE ON  [dbo].[DMO_Supplier_Account_Dates_Upd] TO [CBMSApplication]
GO
