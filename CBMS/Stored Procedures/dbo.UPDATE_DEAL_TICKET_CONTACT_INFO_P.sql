SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UPDATE_DEAL_TICKET_CONTACT_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@dealTicketId  	int       	          	
	@clientContactName	varchar(200)	          	
	@clientContactPhone	varchar(200)	          	
	@clientContactCell	varchar(200)	          	
	@clientContactFax	varchar(200)	          	
	@clientContactEmail	varchar(1000)	          	
	@counterpartyId	int       	          	
	@counterpartyContactName	varchar(200)	          	
	@counterpartyContactPhone	varchar(200)	          	
	@counterpartyContactCell	varchar(200)	          	
	@counterpartyContactFax	varchar(200)	          	
	@counterpartyContactEmail	varchar(1000)	          	
	@vendorId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE     PROCEDURE DBO.UPDATE_DEAL_TICKET_CONTACT_INFO_P 

@userId varchar(10),
@sessionId varchar(20),
@dealTicketId int,
@clientContactName varchar(200),
@clientContactPhone varchar(200),
@clientContactCell varchar(200),
@clientContactFax varchar(200),
@clientContactEmail varchar(1000),
@counterpartyId int,
@counterpartyContactName varchar(200),
@counterpartyContactPhone varchar(200),
@counterpartyContactCell varchar(200),
@counterpartyContactFax varchar(200),
@counterpartyContactEmail varchar(1000), 
@vendorId int

AS
set nocount on
UPDATE 
	RM_DEAL_TICKET 
SET 
	CONTACT_NAME = @clientContactName, 
	CONTACT_PHONE = @clientContactPhone, 
	CONTACT_CELL = @clientContactCell, 
	CONTACT_FAX = @clientContactFax, 
	CONTACT_EMAIL = @clientContactEmail, 
	RM_COUNTERPARTY_ID = @counterpartyId, 
	COUNTERPARTY_CONTACT_NAME = @counterpartyContactName, 
	COUNTERPARTY_CONTACT_PHONE = @counterpartyContactPhone, 
	COUNTERPARTY_CONTACT_CELL = @counterpartyContactCell, 
	COUNTERPARTY_CONTACT_FAX = @counterpartyContactFax, 
	COUNTERPARTY_CONTACT_EMAIL = @counterpartyContactEmail, 
	VENDOR_ID = @vendorId
WHERE
	RM_DEAL_TICKET_ID = @dealTicketId
GO
GRANT EXECUTE ON  [dbo].[UPDATE_DEAL_TICKET_CONTACT_INFO_P] TO [CBMSApplication]
GO
