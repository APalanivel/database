
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------  
/******  
NAME:  
	dbo.Cost_Usage_Summary_Sel_For_Site_By_Commodity_Service_Month
	
 DESCRIPTION:   
 INPUT PARAMETERS:  
	Name				DataType	Default Description  
------------------------------------------------------------  
	@Client_Hier_Id	INT
	@Commodity_Id		INT
	@Currency_Unit_Id	INT
	@UOM_Id			INT
	@Begin_Dt			DATETIME
	@End_Dt			DATETIME
	@Not_Managed		INT		 0

 OUTPUT PARAMETERS:
 Name		DataType	Default Description
------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
	EXEC dbo.Cost_Usage_Summary_Sel_For_Site_By_Commodity_Service_Month 7701 ,290,3,12,'1/1/2011','12/1/2011',NULL
	EXEC dbo.Cost_Usage_Summary_Sel_For_Site_By_Commodity_Service_Month 7448,290,3,12,'1/1/2011','12/1/2011',NULL
	select * from core.client_hier where site_id=17251

AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 SSR		Sharad Srivastava 
 AP			Athmaram Pabbathi
 RR			Raghu Reddy
 
 MODIFICATIONS   
 Initials	Date		Modification  
------------------------------------------------------------  
 SSR		02/08/2010	Created	
 SKA	    03/12/2010	Used Left join for Cost_Usage_Site_Dtl instead of Inner join
 SSR		03/12/2010	Change default value of @Not_managed from 0 to NULL
 HG			03/25/2010	Unit Cost column added in the final select clause.
						Unnecessary Common table expression removed.
 HG			04/05/2010	Procedure modified to return data for supplier accounts as well, added Cte_Account to gather the accounts specific to the given Site_id and used the CTE in the main query.
 						CTE should select accounts specific to the given commodity, added to rate table join to filter the commodity specific accounts.
 HG			04/16/2010	ORDER BY Clause added to sort the records based on Account_Type
 HG			04/17/2010	Unit cost wrongly calculated as Volume/Total Cost corrected it to Total Cost/Volume
 AP			09/14/2011  Following changes has been made as apart of Addl Data Prj
							- Used table variable @Cost_Usage_Bucket_Id to store the Cost & Usage buckets for given @Commodity_Id
							- Replace SAMM, Meter, Rate tables and used Client_Hier & Client_Hier_Account tables
BCH			2012-03-16	Removed @Client_id and @Site_id parameters and instead Used @Client_Hier_Id parameter
RR			2012-07-09  Removed the script replcing null values with zeros for Total_Cost, Volume, Unit_Cost
******/
CREATE PROCEDURE dbo.Cost_Usage_Summary_Sel_For_Site_By_Commodity_Service_Month
      ( 
       @Client_Hier_Id INT
      ,@Commodity_Id INT
      ,@Currency_Unit_Id INT
      ,@UOM_Id INT
      ,@Begin_Dt DATETIME
      ,@End_Dt DATETIME
      ,@Not_Managed INT = NULL )
AS 
BEGIN
      SET NOCOUNT ON

      DECLARE
            @Supplier_Account_Type_Id INT
           ,@Utility_Account_Type_Id INT

      DECLARE @Cost_Usage_Bucket_Id TABLE
            ( 
             Bucket_Master_Id INT PRIMARY KEY CLUSTERED
            ,Bucket_Name VARCHAR(200)
            ,Bucket_Type VARCHAR(25) )
            
      SELECT
            @Supplier_Account_Type_Id = max(case WHEN at.Entity_Name = 'Supplier' THEN at.Entity_Id
                                            END)
           ,@Utility_Account_Type_Id = max(case WHEN at.Entity_Name = 'Utility' THEN at.Entity_Id
                                           END)
      FROM
            dbo.Entity at
      WHERE
            at.Entity_Name IN ( 'Supplier', 'Utility' )
            AND at.Entity_Description = 'Account Type'  

  --Inserting Cost & Usage Buckets to table variable
      INSERT      INTO @Cost_Usage_Bucket_Id
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Bucket_Type )
                  EXEC dbo.Cost_usage_Bucket_Sel_By_Commodity 
                        @Commodity_id = @Commodity_Id ;


      WITH  Cte_Account
              AS ( SELECT
                        CH.Client_Hier_Id
                       ,CH.Site_id
                       ,CHA.Account_Id
                       ,CHA.Account_Number
                       ,CHA.Account_Not_Managed AS Not_Managed
                       ,CHA.Account_Type
                       ,CHA.Account_Vendor_Id AS Vendor_Id
                       ,CHA.Account_Vendor_Name AS Vendor_Name
                       ,ch.Client_Currency_Group_Id
                   FROM
                        Core.Client_Hier CH
                        INNER JOIN Core.Client_Hier_Account CHA
                              ON CH.Client_Hier_Id = CHA.Client_Hier_Id
                   WHERE
                        ch.Client_Hier_Id = @Client_Hier_Id
                        AND CHA.Commodity_Id = @Commodity_Id
                        AND ( @Not_Managed IS NULL
                              OR cha.Account_Not_Managed = @Not_Managed )
                   GROUP BY
                        CHA.Account_Id
                       ,CHA.Account_Number
                       ,CHA.Account_Not_Managed
                       ,CHA.Account_Type
                       ,CHA.Account_Vendor_Id
                       ,CHA.Account_Vendor_Name
                       ,CH.Site_id
                       ,CH.Client_Hier_Id
                       ,ch.Client_Currency_Group_Id)
            SELECT
                  a.Account_ID
                 ,a.Account_Number
                 ,( case WHEN a.Not_Managed = 1 THEN 'Inactive'
                         ELSE 'Active'
                    END ) AS Active
                 ,( case WHEN a.Account_Type = 'Supplier' THEN @Supplier_Account_Type_Id
                         WHEN a.Account_Type = 'Utility' THEN @Supplier_Account_Type_Id
                    END ) AS Account_Type_Id
                 ,a.Account_Type
                 ,a.Vendor_ID
                 ,a.Vendor_Name
                 ,sum(case WHEN CUB.Bucket_Type = 'Determinant' THEN cuad.Bucket_Value * uc.Conversion_Factor
                      END) AS Volume
                 ,sum(case WHEN CUB.Bucket_Type = 'Charge' THEN cuad.Bucket_Value * cc.Conversion_Factor
                      END) AS Total_Cost
                 ,isnull(max(case WHEN ip.Is_Expected = 1
                                       AND ip.Is_Received = 1 THEN 1
                                  ELSE 0
                             END), 0) AS Is_Complete
                 ,isnull(max(convert(INT, ip.Is_Received)), 0) AS Is_published
                 ,isnull(max(case WHEN ip.Recalc_Under_Review = 1
                                       OR ip.Variance_Under_Review = 1 THEN 1
                                  ELSE 0
                             END), 0) Is_Under_Review
                 ,( sum(case WHEN CUB.Bucket_Type = 'Charge' THEN cuad.Bucket_Value * cc.Conversion_Factor
                               END) / nullif(sum(case WHEN CUB.Bucket_Type = 'Determinant' THEN cuad.Bucket_Value * uc.Conversion_Factor
                                                 END), 0) ) AS Unit_Cost
            FROM
                  Cte_Account a
                  LEFT JOIN ( dbo.Cost_Usage_Account_Dtl cuad
                              INNER JOIN @Cost_Usage_Bucket_Id CUB
                                    ON CUB.Bucket_Master_Id = cuad.Bucket_Master_Id )
                              ON a.Account_ID = cuad.Account_ID
                                 AND A.Client_Hier_Id = cuad.Client_Hier_Id
                                 AND cuad.Service_Month BETWEEN @Begin_Dt AND @End_Dt
                  LEFT JOIN dbo.Invoice_Participation ip
                        ON ip.Account_ID = a.Account_ID
                           AND ip.SITE_ID = a.Site_id
                           AND ip.Service_Month = cuad.Service_Month
                  LEFT JOIN dbo.Consumption_Unit_Conversion uc
                        ON uc.Base_Unit_ID = cuad.UOM_Type_Id
                           AND uc.Converted_Unit_ID = @UOM_Id
                  LEFT JOIN dbo.Currency_Unit_Conversion cc
                        ON ( cc.Currency_Group_Id = a.Client_Currency_Group_Id
                             AND cc.Base_Unit_Id = cuad.Currency_Unit_ID
                             AND cc.Converted_Unit_Id = @Currency_Unit_Id
                             AND cc.Conversion_Date = cuad.Service_Month )
            GROUP BY
                  a.Account_ID
                 ,a.ACCOUNT_NUMBER
                 ,a.not_managed
                 ,a.Account_Type
                 ,a.Vendor_ID
                 ,a.Vendor_Name
            ORDER BY
                  a.Account_Type 

END
;
GO


GRANT EXECUTE ON  [dbo].[Cost_Usage_Summary_Sel_For_Site_By_Commodity_Service_Month] TO [CBMSApplication]
GO
