SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.UBM_Generic_Missing_Images_Bill_Dtls_Sel

DESCRIPTION:

INPUT PARAMETERS:
	Name						DataType	Default	Description
----------------------------------------------------------------
	@UBM_Batch_Master_Log_Id	INT	            	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
----------------------------------------------------------------

USAGE EXAMPLES:
---------------------------------------------------------------
	EXEC dbo.UBM_Generic_Missing_Images_Bill_Dtls_Sel 30410
	EXEC dbo.UBM_Generic_Missing_Images_Bill_Dtls_Sel 21649

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------	        	
	RR			2017-09-20	Generic ETL - Payment File Processing - Created

******/
CREATE PROCEDURE [dbo].[UBM_Generic_Missing_Images_Bill_Dtls_Sel]
      ( 
       @UBM_Batch_Master_Log_Id INT )
AS 
BEGIN  
  
      SET NOCOUNT ON;
      
      SELECT
            uc.UBM_ClientName
           ,ua.UBM_AccountNumber
           ,MIN(bd.Service_Begin_Dt) AS Service_Begin_Dt
           ,MAX(bd.Service_End_Dt) AS Service_End_Dt
           ,bm.Bill_Unique_XId
           ,bm.Bill_Image_Reference
           ,bm.Error_Msg
      FROM
            UBM_Generic.dbo.UBM_Bill_Master bm
            INNER JOIN UBM_Generic.dbo.UBM_Bill_Dtl bd
                  ON bm.UBM_Bill_Master_Id = bd.UBM_Bill_Master_Id
            INNER JOIN UBM_Generic.dbo.UBM_Client uc
                  ON bm.UBM_ClientId = uc.UBM_ClientId
            INNER JOIN UBM_Generic.dbo.UBM_Account ua
                  ON bd.UBM_AccountId = ua.UBM_AccountId
      WHERE
            bm.UBM_Batch_Master_Log_Id = @UBM_Batch_Master_Log_Id
            AND ( bm.Cbms_Image_Id IS NULL
                  OR bm.Cbms_Image_Id = 0 )
      GROUP BY
            uc.UBM_ClientName
           ,ua.UBM_AccountNumber
           ,bm.Bill_Unique_XId
           ,bm.Bill_Image_Reference
           ,bm.Error_Msg
      
END
;
GO
GRANT EXECUTE ON  [dbo].[UBM_Generic_Missing_Images_Bill_Dtls_Sel] TO [CBMSApplication]
GO
