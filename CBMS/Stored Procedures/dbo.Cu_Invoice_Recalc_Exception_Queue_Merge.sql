SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
  dbo.Cu_Invoice_Recalc_Exception_Queue_Merge  
  
DESCRIPTION:  
  
  
INPUT PARAMETERS:  
 Name								DataType				Default		Description  
-------------------------------------------------------------------------------------  
 @Cu_Invoice_Id						INT
 @Account_Id						INT
 @Commodity_Id						INT
 @Status_Cd							INT
 @Date_In_Queue						DATETIME
 @User_Info_Id						INT
  

OUTPUT PARAMETERS:  
 Name								DataType				Default		Description  
-------------------------------------------------------------------------------------  
  
USAGE EXAMPLES:  
-------------------------------------------------------------------------------------  
 BEGIN Transaction
 
 select * from Cu_Invoice_Recalc_Exception_Queue
 EXEC Cu_Invoice_Recalc_Exception_Queue_Merge 35238713,55236,290,100981,'2015-11-06',49
 select * from Cu_Invoice_Recalc_Exception_Queue
 
 EXEC Cu_Invoice_Recalc_Exception_Queue_Merge 35238713,55236,290,100982,'2015-11-06',49
 select * from Cu_Invoice_Recalc_Exception_Queue
 
 
 ROLLBACK Transaction 
  
  
AUTHOR INITIALS:  
 Initials	Name  
-------------------------------------------------------------------------------------  
 RKV		Ravi Kumar Vegesna  
   
MODIFICATIONS  
  
 Initials		Date			Modification  
-------------------------------------------------------------------------------------  
 RKV			2015-11-06		Created for AS400-PII 
            
              
******/  
CREATE PROCEDURE [dbo].[Cu_Invoice_Recalc_Exception_Queue_Merge]
      ( 
       @Cu_Invoice_Id INT
      ,@Account_Id INT
      ,@Commodity_Id INT
      ,@Status_Cd INT
      ,@Date_In_Queue DATETIME
      ,@User_Info_Id INT = NULL )
AS 
BEGIN  
      
      
      MERGE INTO dbo.Cu_Invoice_Recalc_Exception_Queue tgt
            USING 
                  ( SELECT
                        @Cu_Invoice_Id AS Cu_Invoice_Id
                       ,@Account_Id AS Account_Id
                       ,@Commodity_Id AS Commodity_Id
                       ,@Status_Cd AS Status_Cd
                       ,@Date_In_Queue AS Date_In_Queue
                       ,@User_Info_Id AS User_Info_Id ) src
            ON ( tgt.Cu_Invoice_Id = src.Cu_Invoice_Id
                 AND tgt.Account_Id = src.Account_Id
                 AND tgt.Commodity_Id = src.Commodity_Id )
            WHEN MATCHED 
                  THEN UPDATE
                    SET 
                        tgt.Status_Cd = src.Status_Cd
                       ,tgt.User_Info_Id = src.User_Info_Id
                       ,tgt.Last_Change_Ts = GETDATE()
            WHEN NOT MATCHED 
                  THEN      
					INSERT
                        ( 
                         Cu_Invoice_Id
                        ,Account_Id
                        ,Commodity_Id
                        ,Status_Cd
                        ,Queue_Date
                        ,User_Info_Id )
                    VALUES
                        ( 
                         src.Cu_Invoice_Id
                        ,src.Account_Id
                        ,src.Commodity_Id
                        ,src.Status_Cd
                        ,src.Date_In_Queue
                        ,src.User_Info_Id );
                        
                  
   
        
      
       
END     
   

;
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Recalc_Exception_Queue_Merge] TO [CBMSApplication]
GO
