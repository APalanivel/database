SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.CBMS_UPDATE_UTILITY_ADDITIONAL_DETAIL_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@nomination_ep 	varchar(500)	          	
	@marketer_changes_ep	varchar(500)	          	
	@contract_changes_ep	varchar(500)	          	
	@is_transport_behind_utility_ep	bit       	          	
	@is_industrial_ep	bit       	          	
	@is_commercial_ep	bit       	          	
	@industrial_volume1	varchar(30)	          	
	@ind_measurement_unit_type_id1	int       	          	
	@ind_measurement_time1	varchar(30)	          	
	@commodity_type_id1	int       	          	
	@commercial_volume1	varchar(30)	          	
	@comm_measurement_unit_type_id1	int       	          	
	@comm_measurement_time1	varchar(30)	          	
	@industrial_volume2	varchar(30)	          	
	@ind_measurement_unit_type_id2	int       	          	
	@ind_measurement_time2	varchar(30)	          	
	@commodity_type_id2	int       	          	
	@commercial_volume2	varchar(30)	          	
	@comm_measurement_unit_type_id2	int       	          	
	@comm_measurement_time2	varchar(30)	          	
	@ind_requirements_other_ep	varchar(500)	          	
	@ind_requirements_other_ng	varchar(500)	          	
	@comm_requirements_other_ep	varchar(500)	          	
	@comm_requirements_other_ng	varchar(500)	          	
	@vendor_id     	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE    PROCEDURE [dbo].[CBMS_UPDATE_UTILITY_ADDITIONAL_DETAIL_P] 

@nomination_ep varchar(500), 
@marketer_changes_ep varchar(500),
@contract_changes_ep varchar(500), 
@is_transport_behind_utility_ep bit, 
@is_industrial_ep bit, 
@is_commercial_ep bit,
@industrial_volume1 varchar(30), 
@ind_measurement_unit_type_id1 int, 
@ind_measurement_time1 varchar(30),
@commodity_type_id1 int,
@commercial_volume1 varchar(30), 
@comm_measurement_unit_type_id1 int, 
@comm_measurement_time1 varchar(30),
@industrial_volume2 varchar(30), 
@ind_measurement_unit_type_id2 int, 
@ind_measurement_time2 varchar(30),
@commodity_type_id2 int,
@commercial_volume2 varchar(30), 
@comm_measurement_unit_type_id2 int, 
@comm_measurement_time2 varchar(30),
@ind_requirements_other_ep varchar(500), 
@ind_requirements_other_ng varchar(500), 
@comm_requirements_other_ep varchar(500), 
@comm_requirements_other_ng varchar(500),
@vendor_id int

AS
declare @utility_detail_id int
select @utility_detail_id =  utility_detail_id from UTILITY_DETAIL where vendor_id = @vendor_id

declare @utility_detail_id_for_add int
select @utility_detail_id_for_add =  count(1) from UTILITY_ADDITIONAL_DETAIL where utility_detail_id = @utility_detail_id


if(@utility_detail_id_for_add > 0)
begin
update UTILITY_ADDITIONAL_DETAIL set	
	
	NOMINATION_EP = @nomination_ep,
	MARKET_CHANGES_EP= @marketer_changes_ep,
	CONTRACT_CHANGE_EP = @contract_changes_ep,
	IS_TRANSPORT_BEHIND_UTILITY_EP = @is_transport_behind_utility_ep,
	IS_INDUSTRIAL_EP = @is_industrial_ep,
	IS_COMMERCIAL_EP = @is_commercial_ep,
	INDUSTRIAL_VOLUME_TWO = @industrial_volume1,
	IND_MEASUREMENT_UNIT_TYPE_ID_TWO = @ind_measurement_unit_type_id1,
	IND_MEASUREMENT_TIME_TWO = @ind_measurement_time1,
	COMMODITY_TYPE_ID_TWO = @commodity_type_id1,
	COMMERCIAL_VOLUME_TWO = @commercial_volume1,
	COMM_MEASUREMENT_UNIT_TYPE_ID_TWO = @comm_measurement_unit_type_id1,
	COMM_MEASUREMENT_TIME_TWO  = @comm_measurement_time1,
	INDUSTRIAL_VOLUME_THREE = @industrial_volume2,
	IND_MEASUREMENT_UNIT_TYPE_ID_THREE = @ind_measurement_unit_type_id2,
	IND_MEASUREMENT_TIME_THREE = @ind_measurement_time2,
	COMMODITY_TYPE_ID_THREE = @commodity_type_id2,
	COMMERCIAL_VOLUME_THREE = @commercial_volume2,
	COMM_MEASUREMENT_UNIT_TYPE_ID_THREE = @comm_measurement_unit_type_id2,
	COMM_MEASUREMENT_TIME_THREE = @comm_measurement_time2,
	IND_REQUIREMENTS_OTHER_EP = @ind_requirements_other_ep,
	IND_REQUIREMENTS_OTHER_NG = @ind_requirements_other_ng,
	COMM_REQUIREMENTS_OTHER_EP = @comm_requirements_other_ep,
	COMM_REQUIREMENTS_OTHER_NG = @comm_requirements_other_ng

where UTILITY_DETAIL_ID = @utility_detail_id
end
else
begin


insert into UTILITY_ADDITIONAL_DETAIL(	
	UTILITY_DETAIL_ID,
	NOMINATION_EP ,
	MARKET_CHANGES_EP,
	CONTRACT_CHANGE_EP,
	IS_TRANSPORT_BEHIND_UTILITY_EP,
	IS_INDUSTRIAL_EP ,
	IS_COMMERCIAL_EP ,
	INDUSTRIAL_VOLUME_TWO,
	IND_MEASUREMENT_UNIT_TYPE_ID_TWO,
	IND_MEASUREMENT_TIME_TWO ,
	COMMODITY_TYPE_ID_TWO ,
	COMMERCIAL_VOLUME_TWO ,
	COMM_MEASUREMENT_UNIT_TYPE_ID_TWO ,
	COMM_MEASUREMENT_TIME_TWO ,
	INDUSTRIAL_VOLUME_THREE ,
	IND_MEASUREMENT_UNIT_TYPE_ID_THREE,
	IND_MEASUREMENT_TIME_THREE ,
	COMMODITY_TYPE_ID_THREE ,
	COMMERCIAL_VOLUME_THREE ,
	COMM_MEASUREMENT_UNIT_TYPE_ID_THREE ,
	COMM_MEASUREMENT_TIME_THREE,
	IND_REQUIREMENTS_OTHER_EP ,
	IND_REQUIREMENTS_OTHER_NG ,
	COMM_REQUIREMENTS_OTHER_EP,
	COMM_REQUIREMENTS_OTHER_NG )
values(
@utility_detail_id,
@nomination_ep, 
@marketer_changes_ep,
@contract_changes_ep, 
@is_transport_behind_utility_ep, 
@is_industrial_ep, 
@is_commercial_ep,
@industrial_volume1, 
@ind_measurement_unit_type_id1, 
@ind_measurement_time1,
@commodity_type_id1,
@commercial_volume1, 
@comm_measurement_unit_type_id1, 
@comm_measurement_time1,
@industrial_volume2, 
@ind_measurement_unit_type_id2, 
@ind_measurement_time2,
@commodity_type_id2,
@commercial_volume2, 
@comm_measurement_unit_type_id2, 
@comm_measurement_time2,
@ind_requirements_other_ep, 
@ind_requirements_other_ng, 
@comm_requirements_other_ep, 
@comm_requirements_other_ng )
end
GO
GRANT EXECUTE ON  [dbo].[CBMS_UPDATE_UTILITY_ADDITIONAL_DETAIL_P] TO [CBMSApplication]
GO
