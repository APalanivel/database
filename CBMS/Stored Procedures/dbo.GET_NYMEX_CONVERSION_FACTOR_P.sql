SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- exec GET_NYMEX_CONVERSION_FACTOR_P '','',24



CREATE PROCEDURE dbo.GET_NYMEX_CONVERSION_FACTOR_P 
@userId varchar,
@sessionId varchar,
@unitId int

as

select	isnull(CAST(consumption.CONVERSION_FACTOR as decimal(16,3)),'1.000' ) VOLUME_CONVERSION_FACTOR	
		
from	CONSUMPTION_UNIT_CONVERSION consumption
	
where	consumption.BASE_UNIT_ID=(select ENTITY_ID 
				  from	ENTITY
				  where	ENTITY_TYPE=102 AND
					ENTITY_NAME like 'mmbtu'
				 ) AND
	consumption.CONVERTED_UNIT_ID=@unitId 






GO
GRANT EXECUTE ON  [dbo].[GET_NYMEX_CONVERSION_FACTOR_P] TO [CBMSApplication]
GO
