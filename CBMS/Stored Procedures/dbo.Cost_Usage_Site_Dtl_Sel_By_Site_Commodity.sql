
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.Cost_Usage_Site_Dtl_Sel_By_Site_Commodity

DESCRIPTION:
	Used to Select the data from Cost_Usage_Site_Dtl table for the given Site_id, period and commodity.
	
INPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	@Client_id				int
	@Site_Id				int
	@Commodity_id			int
	@Begin_Dt				Date
	@End_Dt					Date
	@Uom_Id					INT
	@Currency_Unit_Id		INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC dbo.Cost_Usage_Site_Dtl_Sel_By_Site_Commodity 15205,67,'1/1/2009','12/1/2009',1408,3
	EXEC dbo.Cost_Usage_Site_Dtl_Sel_By_Site_Commodity 1204,1423,'2/1/2010','2/1/2010',1408,3

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG		Hari
	AP		Athmaram Pabbathi

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	HG        1/28/2010	Created
	HG		2/8/2010	Script modified to show all the buckets marked as Is_Shown_On_Site and show all months data if there is no data.
	HG		3/31/2010	Isnull function used to determine conversion factor removed, using bucket type to determine the conversion factors.
							All the columns qualified with the owner.
	HG		4/12/2010	Cbms_Image_id column added in the select clause for the given site, commodity and service month.
     DMR		09/10/2010 Modified for Quoted_Identifier
     AP		03/17/2012 Removed @Client_Id & @Site_Id parameter and added @Client_Hier_Id parameter
******/
CREATE PROCEDURE dbo.Cost_Usage_Site_Dtl_Sel_By_Site_Commodity
      @Client_Hier_Id INT
     ,@Commodity_id INT
     ,@Begin_Dt DATE
     ,@End_Dt DATE
     ,@Uom_Type_Id INT
     ,@Currency_Unit_Id INT
AS 
BEGIN
      SET NOCOUNT ON

      DECLARE
            @Currency_Group_Id INT
           ,@Site_Id INT

      SELECT
            @Currency_Group_Id = ch.Client_Currency_Group_Id
           ,@Site_Id = ch.Site_Id
      FROM
            Core.Client_Hier ch
      WHERE
            ch.Client_Hier_Id = @Client_Hier_Id ;

      SELECT
            cus.Cost_Usage_Site_Dtl_Id
           ,dd.Date_D AS Service_Month
           ,bm.Bucket_Master_Id
           ,bkt_cd.Code_Value Bucket_Type
           ,bm.Bucket_Name
           ,cus.Bucket_Value * ( case WHEN bkt_cd.Code_Value = 'Charge' THEN cc.Conversion_factor
                                      WHEN bkt_cd.Code_Value = 'Determinant' THEN uc.CONVERSION_FACTOR
                                 END ) Bucket_Value
           ,cus.UOM_Type_Id
           ,cus.CURRENCY_UNIT_ID
           ,sci.CBMS_IMAGE_ID
      FROM
            dbo.Bucket_Master bm
            JOIN dbo.Code bkt_cd
                  ON bkt_cd.Code_Id = bm.Bucket_Type_Cd
            CROSS JOIN meta.Date_Dim dd
            LEFT JOIN dbo.Cost_Usage_Site_Dtl cus
                  ON bm.Bucket_Master_Id = cus.Bucket_Master_Id
                     AND cus.Service_Month = dd.Date_D
                     AND cus.Client_Hier_Id = @Client_Hier_Id
            LEFT JOIN dbo.Site_Commodity_Image sci
                  ON sci.Site_id = @Site_Id
                     AND sci.Commodity_Id = bm.Commodity_Id
                     AND sci.Service_Month = cus.Service_Month
            LEFT JOIN dbo.consumption_unit_conversion uc
                  ON uc.BASE_UNIT_ID = cus.UOM_Type_Id
                     AND uc.CONVERTED_UNIT_ID = @Uom_Type_Id
            LEFT JOIN dbo.CURRENCY_UNIT_CONVERSION cc
                  ON cc.currency_group_id = @Currency_Group_id
                     AND cc.base_unit_id = cus.CURRENCY_UNIT_ID
                     AND cc.converted_unit_id = @Currency_unit_id
                     AND cc.conversion_date = cus.Service_month
      WHERE
            bm.Commodity_Id = @Commodity_id
            AND bm.Is_Shown_on_Site = 1
            AND dd.Date_D BETWEEN @Begin_Dt AND @End_Dt
      ORDER BY
            dd.Date_D
           ,bkt_cd.Code_Value
           ,bm.Bucket_Name

END
;
GO

GRANT EXECUTE ON  [dbo].[Cost_Usage_Site_Dtl_Sel_By_Site_Commodity] TO [CBMSApplication]
GO
