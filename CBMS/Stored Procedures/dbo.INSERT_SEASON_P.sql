SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE dbo.INSERT_SEASON_P
	@seasonName VARCHAR(200),
	@seasonFromDate DATETIME,
	@seasonToDate DATETIME,
	@seasonParentId INT,
	@seasonParentType INT
AS
BEGIN

	SET NOCOUNT ON

	INSERT INTO dbo.season (
		season_name, 
		season_from_date, 
		season_to_date, 
		season_parent_id, 
		season_parent_type_id) 
	VALUES (@seasonName, 
		@seasonFromDate, 
		@seasonToDate, 
		@seasonParentId, 
		@seasonParentType)

END
GO
GRANT EXECUTE ON  [dbo].[INSERT_SEASON_P] TO [CBMSApplication]
GO
