SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Load_Profile_Specification_Del]  

DESCRIPTION: 
	It Deletes Load Profile Specification for Selected Load Specification Id.
	This table gets deleted along with Contract
      
INPUT PARAMETERS:          
	NAME								DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Load_Profile_Specification_Id		INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
		EXEC Load_Profile_Specification_Del 277
  Rollback Tran

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			18-JUN-10	CREATED

*/

CREATE PROCEDURE dbo.Load_Profile_Specification_Del
    (
      @Load_Profile_Specification_Id INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.LOAD_PROFILE_SPECIFICATION 
	WHERE
		LOAD_PROFILE_SPECIFICATION_ID = @Load_Profile_Specification_Id

END
GO
GRANT EXECUTE ON  [dbo].[Load_Profile_Specification_Del] TO [CBMSApplication]
GO
