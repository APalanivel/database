SET NUMERIC_ROUNDABORT OFF 
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******
NAME:	dbo.[cbmsGroupInfo_RemoveForUser]

DESCRIPTION:

INPUT PARAMETERS:
      Name                  DataType          Default     Description
---------------------------------------------------------------------
	@user_info_id				INT
	@App_Menu_Profile_id		INT

OUTPUT PARAMETERS:
      Name              DataType          Default     Description
-----------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------

	BEGIN TRAN
		exec cbmsGroupInfo_RemoveForUser @user_info_id = 17756, @App_Menu_Profile_id = 4
	ROLLBACK TRAN


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter
		HG		Harihara Suthan G

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	DR			08/04/2009	Removed Linked Server Updates
	DMR			09/10/2010	Modified for Quoted_Identifier
	HG			04/18/2011	Modified code as a part of DV group revamp to delete the groups assigned to the user based on given App menu prfile id.	  
							 Removed unused paramter @MyAccountId
******/
CREATE PROCEDURE [dbo].[cbmsGroupInfo_RemoveForUser]
(
	@user_info_id			INT
	,@App_Menu_Profile_id	INT
)
AS
BEGIN

      SET NOCOUNT ON

      DELETE
            uigim
      FROM
            dbo.User_info_group_info_map uigim
            INNER JOIN dbo.Group_Info gi
                  ON gi.Group_Info_Id = uigim.Group_Info_id
            INNER JOIN dbo.Group_info_Category gic
                  ON gic.Group_Info_Category_id = gi.Group_Info_Category_Id
      WHERE
            user_info_id = @user_info_id
            AND gic.App_Menu_Profile_Id = @App_Menu_Profile_id

      EXEC dbo.cbmsGroupInfo_GetForUser @user_info_id

END
GO

GRANT EXECUTE ON  [dbo].[cbmsGroupInfo_RemoveForUser] TO [CBMSApplication]
GO