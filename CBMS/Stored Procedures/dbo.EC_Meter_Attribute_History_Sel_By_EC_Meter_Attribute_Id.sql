SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:              
      [dbo].[EC_Meter_Attribute_History_Sel_By_EC_Meter_Attribute_Id]            
              
Description:              
            To get the details of active tracking data  data for a given meter attribute id.             
              
 Input Parameters:              
    Name						DataType			Default				Description                
----------------------------------------------------------------------------------------------            
	@EC_Meter_Attribute_Id		INT            
               
 Output Parameters:                    
    Name						DataType			Default				Description                
----------------------------------------------------------------------------------------------            
              
 Usage Examples:                  
----------------------------------------------------------------------------------------------       
     
 EXEC dbo.EC_Meter_Attribute_History_Sel_By_EC_Meter_Attribute_Id 1 
 
 EXEC dbo.EC_Meter_Attribute_History_Sel_By_EC_Meter_Attribute_Id  2
 
Author Initials:              
      Initials         Name              
----------------------------------------------------------------------------------------------            
      NR              Narayana Reddy       
 Modifications:              
      Initials		Date			Modification              
----------------------------------------------------------------------------------------------            
       NR			2015-05-08       Created for AS400.         
             
******/             
          
CREATE PROCEDURE [dbo].[EC_Meter_Attribute_History_Sel_By_EC_Meter_Attribute_Id]
      ( 
       @EC_Meter_Attribute_Id INT
      ,@Ec_Meter_Attribute_Value NVARCHAR(255) = NULL )
AS 
BEGIN          
      SET NOCOUNT ON;    
      DECLARE @EC_Meter_Attribute_Id_Is_History_Exists BIT  
 
       
      SET @EC_Meter_Attribute_Id_Is_History_Exists = 0     
              
      SELECT
            @EC_Meter_Attribute_Id_Is_History_Exists = 1
      FROM
            dbo.EC_Meter_Attribute_Tracking emat
      WHERE
            emat.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id
            AND ( @Ec_Meter_Attribute_Value IS NULL
                  OR emat.EC_Meter_Attribute_Value = @Ec_Meter_Attribute_Value )
            
      SELECT
            @EC_Meter_Attribute_Id_Is_History_Exists = 1
      FROM
            dbo.EC_Calc_Val_Meter_Attribute ecvma
      WHERE
            ecvma.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id
            AND ( @Ec_Meter_Attribute_Value IS NULL
                  OR ecvma.EC_Meter_Attribute_Value = @Ec_Meter_Attribute_Value )
          
            
      SELECT
            @EC_Meter_Attribute_Id_Is_History_Exists AS EC_Meter_Attribute_Exists
                     
END  
        

;
GO
GRANT EXECUTE ON  [dbo].[EC_Meter_Attribute_History_Sel_By_EC_Meter_Attribute_Id] TO [CBMSApplication]
GO
