SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[cbmsPricePoint_GetForMarketIndex]
	( @MyAccountId int
	, @market_index_id int
	)
AS
BEGIN

	   select pr.price_index_id 	price_point_id
		, pr.index_id 		market_index_id
		, mi.entity_name 	market_index
		, pr.pricing_point 	price_point
		, pr.index_description 	price_point_desc
		, pr.currency_unit_id
		, cur.currency_unit_name	
	     from price_index pr
	     join entity mi on mi.entity_id = pr.index_id
  left outer join currency_unit cur on cur.currency_unit_id = pr.currency_unit_id
	    where pr.index_id = @market_index_id
	 order by pr.pricing_point

END
GO
GRANT EXECUTE ON  [dbo].[cbmsPricePoint_GetForMarketIndex] TO [CBMSApplication]
GO
