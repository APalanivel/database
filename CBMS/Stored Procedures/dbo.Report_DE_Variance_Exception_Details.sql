
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
       
/******      
        
 NAME: dbo.Report_DE_Variance_Exception_Details      
        
 DESCRIPTION:        
     variance exc  details for a given client id.      
        
 INPUT PARAMETERS:        
       
 Name        DataType   Default   Description        
---------------------------------------------------------------------------------------------------------------      
 @Client_Id_List     VARCHAR(MAX)                                          
 @Start_Date                        DATETIME      
 @End_Date                          DATETIME      
         
 OUTPUT PARAMETERS:       
              
 Name        DataType   Default   Description        
---------------------------------------------------------------------------------------------------------------      
        
        
 USAGE EXAMPLES:            
---------------------------------------------------------------------------------------------------------------      
SELECT * FROM CORE.CLIENT_HIER WHERE CLIENT_ID=11682      
 EXEC  dbo.Report_DE_Variance_Exception_Details  11682,'2013-8-28','2013-9-28'       
       
 AUTHOR INITIALS:      
        
 Initials    Name        
---------------------------------------------------------------------------------------------------------------      
  NR                    Narayana Reddy          
         
 MODIFICATIONS:       
       
 Initials    Date   Modification      
---------------------------------------------------------------------------------------------------------------      
  NR                    2013-11-25      Created       
       
******/         
CREATE PROCEDURE dbo.Report_DE_Variance_Exception_Details
      ( 
       @Client_Id_List VARCHAR(MAX)
      ,@Start_Date DATE
      ,@End_Date DATE )
AS 
BEGIN                          
      SET NOCOUNT ON      
            
            
            
      DECLARE @ClientIds TABLE
            ( 
             Client_Id INT PRIMARY KEY )              
      
      INSERT      @ClientIds
                  SELECT
                        Segments
                  FROM
                        dbo.ufn_split(@Client_Id_List, ',')       
                              
            
      CREATE TABLE #Variance_Exc
            ( 
             Category_Name VARCHAR(50)
            ,Cu_Invoice_Id INT
            ,Account_Id INT
            ,Test_Description VARCHAR(500)
            ,Execution_Dt DATETIME
            ,Closed_Dt DATETIME
            ,Client_Id INT
            ,Closed_By VARCHAR(55)
            ,Is_Failure BIT
            ,CU_Invoice_Event_Id INT )       
      
      INSERT      INTO #Variance_Exc
                  ( 
                   Category_Name
                  ,Cu_Invoice_Id
                  ,Account_Id
                  ,Test_Description
                  ,Execution_Dt
                  ,Closed_Dt
                  ,Client_Id
                  ,Closed_By
                  ,Is_Failure
                  ,CU_Invoice_Event_Id )      
  -- failedrecalc_cte      
                  SELECT
                        'FailedRecalc' Category_Name
                       ,ce.CU_INVOICE_ID Cu_Invoice_Id
                       ,cuism.account_id Account_Id
                       ,'Failed Recalc' Test_Description
                       ,cued.OPENED_DATE Execution_Dt
                       ,cued.CLOSED_DATE Closed_Dt
                       ,c.Client_Id
                       ,ui.USERNAME
                       ,1
                       ,cue.CU_Invoice_Event_Id
                  FROM
                        dbo.cu_exception ce
                        JOIN dbo.CU_EXCEPTIOn_detail cued
                              ON cued.CU_EXCEPTION_ID = ce.cu_exception_id
                        JOIN dbo.CU_EXCEPTION_TYPE cuet
                              ON cuet.cu_EXCEPTION_TYPE_ID = cued.EXCEPTION_TYPE_ID
                        JOIN dbo.cu_invoice cui
                              ON cui.CU_INVOICE_ID = ce.CU_INVOICE_ID
                        JOIN dbo.cu_invoice_event cue
                              ON ce.CU_INVOICE_ID = cue.CU_INVOICE_ID
                        JOIN dbo.CU_INVOICE_SERVICE_MONTH cuism
                              ON cuism.cu_invoice_id = cui.CU_INVOICE_ID
                                 AND cuism.Account_ID = cued.Account_ID
                        JOIN @ClientIds C
                              ON cui.CLIENT_ID = C.Client_Id
                        LEFT JOIN dbo.USER_INFO ui
                              ON UI.USER_INFO_ID = cued.CLOSED_BY_ID
                  WHERE
                        cuet.EXCEPTION_TYPE = 'failed recalc'
                        AND cue.event_date BETWEEN @Start_Date AND @End_Date
                        AND cui.IS_REPORTED = 1
                        AND cui.IS_PROCESSED = 1
                        AND cui.IS_DUPLICATE != 1
                        AND cui.is_dnt != 1
                        AND cue.EVENT_DESCRIPTION LIKE 'Invoice Inserted%'
                  GROUP BY
                        ce.CU_INVOICE_ID
                       ,cuism.account_id
                       ,cued.OPENED_DATE
                       ,cued.CLOSED_DATE
                       ,c.Client_Id
                       ,ui.USERNAME
                       ,cue.CU_Invoice_Event_Id                        
                              
                              
      INSERT      INTO #Variance_Exc
                  ( 
                   Category_Name
                  ,Cu_Invoice_Id
                  ,Account_Id
                  ,Test_Description
                  ,Execution_Dt
                  ,Closed_Dt
                  ,Client_Id
                  ,Closed_By
                  ,Is_Failure
                  ,CU_Invoice_Event_Id )      
            --variance_failed_cte      
   --Failed Var Log IDs      
                  SELECT
                        Category_Name = CASE WHEN vl.category_name = 'Usage' THEN 'Volume'
                                             ELSE vl.Category_Name
                                        END
                       ,cui.CU_INVOICE_ID Cu_Invoice_Id
                       ,vl.Account_ID Account_Id
                       ,vl.Test_Description Test_Description
                       ,vl.Execution_Dt Execution_Dt
                       ,vl.Closed_Dt Closed_Dt
                       ,c.Client_Id
                       ,vl.Closed_By_User_Name
                       ,vl.Is_Failure
                       ,cue.CU_INVOICE_EVENT_ID
                  FROM
                        dbo.variance_log vl
                        JOIN dbo.CU_INVOICE_SERVICE_MONTH cuism
                              ON cuism.Account_ID = vl.Account_ID
                                 AND cuism.SERVICE_MONTH = vl.Service_Month
                        JOIN cu_invoice cui
                              ON cui.CU_INVOICE_ID = cuism.CU_INVOICE_ID
                        JOIN dbo.cu_invoice_event cue
                              ON cui.CU_INVOICE_ID = cue.CU_INVOICE_ID
                        JOIN @ClientIds C
                              ON cui.CLIENT_ID = C.Client_Id
                  WHERE
                        cui.IS_REPORTED = 1
                        AND cui.IS_PROCESSED = 1
                        AND cui.IS_DUPLICATE != 1
                        AND cui.is_dnt != 1
                        AND cue.event_date BETWEEN @Start_Date AND @End_Date
                        AND cue.EVENT_DESCRIPTION LIKE 'Invoice Inserted%'
                  GROUP BY
                        vl.category_name
                       ,cui.CU_INVOICE_ID
                       ,vl.Account_ID
                       ,vl.Test_Description
                       ,vl.Execution_Dt
                       ,vl.Closed_Dt
                       ,c.Client_Id
                       ,vl.Closed_By_User_Name
                       ,vl.Is_Failure
                       ,cue.CU_INVOICE_EVENT_ID  
                              
      CREATE CLUSTERED INDEX ix_#Variance ON #Variance_Exc (  CU_INVOICE_EVENT_ID,Account_ID)                            
                              
      SELECT
            x.Category_Name
           ,x.Test_Description
           ,x.Execution_Dt
           ,x.Closed_Dt
           ,x.CU_INVOICE_ID Cu_Invoice_Id
           ,ca.Account_Id [Account ID]
           ,ca.Account_Number [Account Number]
           ,ch.Site_name [Site]
           ,cue.EVENT_DESCRIPTION [Event]
           ,cue.EVENT_DATE [Event Date]
           ,CONVERT(VARCHAR(12), x.Execution_Dt, 101) AS ConvertedExecDate
           ,CONVERT(VARCHAR(12), x.Closed_Dt, 101) AS ConvertedClosedDate
           ,DATEDIFF(DAY, x.Execution_Dt, x.Closed_Dt) AS DayDifference
           ,CASE WHEN DATEDIFF(DAY, x.Execution_Dt, x.Closed_Dt) < 30 THEN '0-30 days'
                 WHEN DATEDIFF(DAY, x.Execution_Dt, x.Closed_Dt) >= 30
                      AND DATEDIFF(DAY, x.Execution_Dt, x.Closed_Dt) < 90 THEN '30-90 days'
                 WHEN DATEDIFF(DAY, x.Execution_Dt, x.Closed_Dt) >= 90 THEN 'over 90 days'
            END AS [Bucket of Days to Close]
           ,x.Closed_By
           ,CASE WHEN x.Is_Failure = 1 THEN 'Failed'
                 ELSE 'Passed'
            END Test_Status
           ,ch.State_Name
           ,ch.Country_Name
      FROM
            #Variance_Exc x
            JOIN core.client_hier_account ca
                  ON ca.account_id = x.Account_ID
            JOIN core.client_hier ch
                  ON ch.client_hier_id = ca.client_hier_id
            JOIN dbo.cu_invoice_event cue
                  ON x.CU_INVOICE_EVENT_ID = cue.CU_INVOICE_EVENT_ID  
      --WHERE  
      --      cue.EVENT_DESCRIPTION LIKE '%insert%'  
      GROUP BY
            x.Category_Name
           ,x.CU_INVOICE_ID
           ,ca.Account_Id
           ,ca.Account_Number
           ,ch.Site_name
           ,cue.EVENT_DESCRIPTION
           ,cue.EVENT_DATE
           ,x.Test_Description
           ,x.Execution_Dt
           ,x.Closed_Dt
           ,x.Closed_By
           ,x.Is_Failure
           ,ch.State_Name
           ,ch.Country_Name 
            
END      
      
;
GO

GRANT EXECUTE ON  [dbo].[Report_DE_Variance_Exception_Details] TO [CBMS_SSRS_Reports]
GO
