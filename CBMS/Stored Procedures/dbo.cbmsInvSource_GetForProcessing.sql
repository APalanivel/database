
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
Name:    
	dbo.cbmsInvSource_GetForProcessing  

 Description:
	To get the folder names for scraping

 Input Parameters:
    Name						DataType      Default		Description   
------------------------------------------------------------------------    
    @MyAccountId				INT

 Output Parameters:
	Name						DataType      Default		Description
------------------------------------------------------------------------

 Usage Examples:
------------------------------------------------------------------------

	EXEC dbo.cbmsInvSource_GetForProcessing
		  @MyAccountId = 16

Author Initials:
 Initials		Name  
------------------------------------------------------------  
 HG				Harihara Suthan G
   
 Modifications :    
 Initials		Date	    Modification    
------------------------------------------------------------    
 HG				2014-07-24	Modified to include the Is_Exclude_For_Duplicate_Check column
 KH				2014-08-26  Modified to move 'Email Account' row to end of result set since this source takes by far the longest to scrape
 HG				2014-08-29	Modified to move the 'Email folder' to move after 'Email Account' so that the files can be copied in the same batch.
******/

CREATE PROCEDURE [dbo].[cbmsInvSource_GetForProcessing] ( @MyAccountId INT )
AS 
BEGIN

      SET NOCOUNT ON

      SELECT
            s.inv_source_id
           ,s.inv_source_type_id
           ,st.entity_name inv_source_type
           ,s.inv_source_label
           ,s.is_active
           ,s.source_folder_path
           ,s.archive_folder_path
           ,s.include_sub_folders
           ,s.preserve_sub_folders
           ,s.query_fax_database
           ,s.email_server
           ,s.email_uid
           ,s.email_pwd
           ,s.target_folder_path
           ,s.Is_Exclude_For_Duplicate_Check
      FROM
            dbo.Inv_Source s
            INNER JOIN dbo.Entity st
                  ON st.entity_id = s.inv_source_type_id
      WHERE
            s.is_active = 1
      ORDER BY
            CASE WHEN s.INV_SOURCE_LABEL = 'E-mail Account' THEN 2
                 WHEN s.INV_SOURCE_LABEL = 'E-mail Folder' THEN 3
                 ELSE 1
            END
           ,s.INV_SOURCE_LABEL
           
END;

;
GO

GRANT EXECUTE ON  [dbo].[cbmsInvSource_GetForProcessing] TO [CBMSApplication]
GO
