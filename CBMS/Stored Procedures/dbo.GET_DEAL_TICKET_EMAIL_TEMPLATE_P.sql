SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_DEAL_TICKET_EMAIL_TEMPLATE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@emailType     	int       	          	
	@emailTypeName 	varchar(200)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE    PROCEDURE dbo.GET_DEAL_TICKET_EMAIL_TEMPLATE_P
@userId varchar,
@sessionId varchar,
@emailType int, 
@emailTypeName varchar(200)

AS
set nocount on
	DECLARE @entityId int
	
	select @entityId = ENTITY_ID FROM ENTITY WHERE ENTITY_TYPE=@emailType AND 
	ENTITY_NAME=@emailTypeName
	
	SELECT 
		SUBJECT, CONTENT 
	FROM 
		RM_EMAIL_TEMPLATE
	WHERE 
		EMAIL_TYPE_ID = @entityId
GO
GRANT EXECUTE ON  [dbo].[GET_DEAL_TICKET_EMAIL_TEMPLATE_P] TO [CBMSApplication]
GO
