SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_DELETE_ACCOUNT_METER_MAP_DETAILS_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                 DataType          Default     Description    
---------------------------------------------------------------------------------    
@rfpId                     int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
--exec dbo.SR_RFP_DELETE_ACCOUNT_METER_MAP_DETAILS_P
--@rfpId = 2047

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_DELETE_ACCOUNT_METER_MAP_DETAILS_P
@rfpId int

AS
	
SET  NOCOUNT ON

DELETE FROM
		SR_RFP_ACCOUNT_METER_MAP 
WHERE
		SR_RFP_ACCOUNT_ID IN (SELECT SR_RFP_ACCOUNT_ID FROM SR_RFP_ACCOUNT WHERE SR_RFP_ID = @rfpId )
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_DELETE_ACCOUNT_METER_MAP_DETAILS_P] TO [CBMSApplication]
GO
