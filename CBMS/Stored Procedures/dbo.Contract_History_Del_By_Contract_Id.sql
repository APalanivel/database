SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [DBO].[Contract_History_Del_By_Contract_Id]

DESCRIPTION:

	To Delete Contract History Associated with given Contract Id.

INPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION     
------------------------------------------------------------
	@Contract_Id	INT				
	@User_Info_Id	INT	
                
OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------
	BEGIN TRAN
		EXEC Contract_History_Del_By_Contract_Id  29179,49
		--EXEC Contract_History_Del_By_Contract_Id  21739,49
		--EXEC Contract_History_Del_By_Contract_Id 71090,49
	ROLLBACK TRAN
	
	BEGIN TRANSACTION
		SELECT * FROM dbo.INVOICE_PARTICIPATION WHERE ACCOUNT_ID = 62557 AND SITE_ID=2435 AND SERVICE_MONTH='2008-08-01 00:00:00.000'
		SELECT * FROM dbo.INVOICE_PARTICIPATION_SITE WHERE SITE_ID=2435 AND SERVICE_MONTH='2008-08-01 00:00:00.000'
		EXEC Contract_History_Del_By_Contract_Id  39858,49
		SELECT * FROM dbo.INVOICE_PARTICIPATION WHERE ACCOUNT_ID = 62557 AND SITE_ID=2435 AND SERVICE_MONTH='2008-08-01 00:00:00.000'
		SELECT * FROM dbo.INVOICE_PARTICIPATION_SITE WHERE SITE_ID=2435 AND SERVICE_MONTH='2008-08-01 00:00:00.000'
	ROLLBACK TRANSACTION
	
	BEGIN TRANSACTION
		SELECT * FROM dbo.INVOICE_PARTICIPATION WHERE ACCOUNT_ID = 11029 AND SITE_ID=313 AND SERVICE_MONTH='2005-05-01 00:00:00.000'
		SELECT * FROM dbo.INVOICE_PARTICIPATION_SITE WHERE SITE_ID=313 AND SERVICE_MONTH='2005-05-01 00:00:00.000'
		EXEC Contract_History_Del_By_Contract_Id  22673,49
		SELECT * FROM dbo.INVOICE_PARTICIPATION WHERE ACCOUNT_ID = 11029 AND SITE_ID=313 AND SERVICE_MONTH='2005-05-01 00:00:00.000'
		SELECT * FROM dbo.INVOICE_PARTICIPATION_SITE WHERE SITE_ID=313 AND SERVICE_MONTH='2005-05-01 00:00:00.000'
	ROLLBACK TRANSACTION
	
---	to delete the Termination/registration notification logs

	BEGIN TRANSACTION
      
      SELECT * FROM dbo.Contract_Notification_Log cnl INNER JOIN dbo.Contract_Notification_Log_Meter_Dtl cnlmd
            ON cnl.Contract_Notification_Log_Id = cnlmd.Contract_Notification_Log_Id  WHERE cnl.Contract_Id = 114767
            
      EXEC Contract_History_Del_By_Contract_Id  114767,49
      		
	  SELECT * FROM dbo.Contract_Notification_Log cnl INNER JOIN dbo.Contract_Notification_Log_Meter_Dtl cnlmd
            ON cnl.Contract_Notification_Log_Id = cnlmd.Contract_Notification_Log_Id  WHERE cnl.Contract_Id = 114767
            
	ROLLBACK TRANSACTION
	
	
	BEGIN TRANSACTION
      
	SELECT * FROM dbo.Supplier_Account_Meter_Map WHERE Contract_ID = 118269 
	SELECT * FROM Cu_Invoice_Account_Commodity_Comment  WHERE Account_Id=1027733 
	        
	EXEC Contract_History_Del_By_Contract_Id 
		  @Contract_Id= 118269
		 ,@User_Info_Id= 49
		 
	SELECT * FROM Cu_Invoice_Account_Commodity_Comment  WHERE Account_Id=1027733         
            
	ROLLBACK TRANSACTION



	BEGIN TRANSACTION
      
	SELECT * FROM dbo.Supplier_Account_Meter_Map WHERE Contract_ID = 118269 
	SELECT * FROM dbo.Supplier_Account_Config WHERE Contract_ID = 118269
	SELECT * FROM dbo.SUPPLIER_ACCOUNT_METER_MAP samm WHERE samm.ACCOUNT_ID =  1027733 
	SELECT * FROM dbo.Supplier_Account_Config acirt WHERE acirt.Account_Id =1027733 

	EXEC Contract_History_Del_By_Contract_Id 
		  @Contract_Id= 118269
		 ,@User_Info_Id= 49
		 
	SELECT * FROM dbo.Supplier_Account_Meter_Map WHERE Contract_ID = 118269 
	SELECT * FROM dbo.Supplier_Account_Config WHERE Contract_ID = 118269
	SELECT * FROM dbo.SUPPLIER_ACCOUNT_METER_MAP samm WHERE samm.ACCOUNT_ID =  1027733 
	SELECT * FROM dbo.Supplier_Account_Config acirt WHERE acirt.Account_Id =1027733 
 
 ROLLBACK TRANSACTION

AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH
	HG			Harihara Suthan G
	BCH			Balaraju Chalumuri
	AKR    Ashok Kumar Raju
	RR			Raghu Reddy
	RKV         Ravi Kumar Vegesna
	NR			Narayana Reddy
	

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			06/24/2010	Created
	HG			07/23/2010	Client_Name column in Application_Audit_Log table saved as NULL as the Samm table joined directly to get the account, site to get the client corrected it to join Samm,Meter, Account, Site and Client.
	BCH			2011-12-30  Deleted transactions (delete tool enhancement - Transactions are maintained in Application)
							Used client hier tables to get the client name
	AKR         2012-10-01  Modified the Code to delete Contract_Lock_Dtl when a contract is deleted as a part of POCO
	BCH			2012-10-05  Modified for using the incrementing the rownum for deletion instead of deleting the data from table varibale.
	RR			2013-10-07	MAINT-2242 Procedure is deleting both the Site level and account level Invoice participation records of the supplier accounts 
							and associated sites of deleting contract, but the site level records should be reaggregated. Modified the script part
							delete site ip records (using dbo.Invoice_Participation_Site_Del) to reaggregation (using dbo.cbmsInvoiceParticipationSite_Save3).
    RKV			2015-10-29	AS400II Modified to delete history from EC_Contract_Attribute_Tracking
    NR			2016-08-18	MAINT-4178 Modified the Code to delete the Termination/registration notification logs when a contract is deleted.
	NR			2016-12-22	MAINT-4741 To delete the Cu_Invoice_Account_Commodity_Comment table account data when contract is deleted.  						
	RKV         2017-04-05  Added Account_Exception and IC data
	RKV         2018-08-09  MAINT-7624 Added Account_Comment_Del,Cu_Invoice_Account_Commodity_Comment_Del removed delete statement on Cu_Invoice_Account_Commodity_Comment
	NR			2019-12-16	MAINT-9670 - Removed account deleted logic & IP  and updated contract_Id as -1 for existing .

*/

CREATE PROCEDURE [dbo].[Contract_History_Del_By_Contract_Id]
    (
        @Contract_Id INT
        , @User_Info_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Current_Ts DATETIME
            , @Audit_Function SMALLINT = -1
            , @User_Name VARCHAR(81)
            , @Application_Name VARCHAR(60) = 'Delete Contract'
            , @Client_Name VARCHAR(200)
            , @Account_Id INT
            , @Contract_Entity_Id INT
            , @Contract_Cbms_Image_Map_Id INT
            , @Invoice_Participation_Site_Id INT
            , @Contract_Meter_Volume_Id INT
            , @Hedge_Id INT
            , @Hedge_Detail_Id INT
            , @Sso_Project_Id INT
            , @Meter_Id INT
            , @Site_Id INT
            , @Service_Month DATETIME
            , @Sr_Batch_Log_Details_Id INT
            , @Sr_Cancel_Renegotiation_Document_Id INT
            , @Lookup_Value XML
            , @Contract_Lock_Dtl_Id INT
            , @Total_Row_Count INT
            , @Row_Counter INT
            , @EC_Contract_Attribute_Tracking_Id INT
            , @Account_Exception_Id INT;

        DECLARE @Supplier_Account_List TABLE
              (
                  Account_Id INT PRIMARY KEY CLUSTERED
                  , Row_Num INT IDENTITY(1, 1)
              );

        DECLARE @Account_Exception TABLE
              (
                  Account_Exception_Id INT PRIMARY KEY CLUSTERED
                  , Row_Num INT IDENTITY(1, 1)
              );

        DECLARE @Invoice_Participation_List TABLE
              (
                  Account_Id INT
                  , Site_Id INT
                  , Service_Month DATETIME
                  , PRIMARY KEY CLUSTERED
                    (
                        Account_Id
                        , Site_Id
                        , Service_Month
                    )
                  , Row_Num INT IDENTITY(1, 1)
              );
        DECLARE @IP_Site_Reaggregation_List TABLE
              (
                  Site_Id INT
                  , Service_Month DATETIME
                  , PRIMARY KEY CLUSTERED
                    (
                        Site_Id
                        , Service_Month
                    )
                  , Row_Num INT IDENTITY(1, 1)
              );
        DECLARE @Contract_Cbms_Image_Map_List TABLE
              (
                  Contract_Cbms_Image_Map_Id INT PRIMARY KEY CLUSTERED
                  , Row_Num INT IDENTITY(1, 1)
              );
        DECLARE @Contract_Meter_Volume_List TABLE
              (
                  Contract_Meter_Volume_Id INT PRIMARY KEY CLUSTERED
                  , Row_Num INT IDENTITY(1, 1)
              );
        DECLARE @Hedge_List TABLE
              (
                  Hedge_Id INT PRIMARY KEY CLUSTERED
                  , Row_Num INT IDENTITY(1, 1)
              );
        DECLARE @Hedge_Detail_List TABLE
              (
                  Hedge_Detail_Id INT PRIMARY KEY CLUSTERED
                  , Row_Num INT IDENTITY(1, 1)
              );
        DECLARE @Sso_Project_Contract_Map_List TABLE
              (
                  Sso_Project_Id INT PRIMARY KEY CLUSTERED
                  , Row_Num INT IDENTITY(1, 1)
              );

        DECLARE @Supplier_Account_Meter_Map_List TABLE
              (
                  Account_Id INT
                  , Meter_Id INT
                  , PRIMARY KEY CLUSTERED
                    (
                        Account_Id
                        , Meter_Id
                    )
                  , Row_Num INT IDENTITY(1, 1)
              );

        DECLARE @Sr_Batch_Log_Details_List TABLE
              (
                  Sr_Batch_Log_Details_Id INT PRIMARY KEY CLUSTERED
                  , Row_Num INT IDENTITY(1, 1)
              );

        DECLARE @Sr_Cancel_Renegotiation_Document_List TABLE
              (
                  Sr_Cancel_Renegotiation_Document_Id INT PRIMARY KEY CLUSTERED
                  , Row_Num INT IDENTITY(1, 1)
              );

        DECLARE @Contract_Lock_Dtl_List TABLE
              (
                  Contract_Lock_Dtl_Id INT PRIMARY KEY CLUSTERED
                  , Row_Num INT IDENTITY(1, 1)
              );

        DECLARE @EC_Contract_Attribute_Tracking TABLE
              (
                  EC_Contract_Attribute_Tracking_Id INT PRIMARY KEY CLUSTERED
                  , Row_Num INT IDENTITY(1, 1)
              );

        INSERT INTO @Supplier_Account_List
             (
                 Account_Id
             )
        SELECT
            ACCOUNT_ID
        FROM
            dbo.SUPPLIER_ACCOUNT_METER_MAP
        WHERE
            Contract_ID = @Contract_Id
        GROUP BY
            ACCOUNT_ID;

        INSERT INTO @Sr_Batch_Log_Details_List
             (
                 Sr_Batch_Log_Details_Id
             )
        SELECT
            SR_BATCH_LOG_DETAILS_ID
        FROM
            dbo.SR_BATCH_LOG_DETAILS
        WHERE
            CONTRACT_ID = @Contract_Id;

        INSERT INTO @Sr_Cancel_Renegotiation_Document_List
             (
                 Sr_Cancel_Renegotiation_Document_Id
             )
        SELECT
            SR_CANCEL_RENEGOTIATION_DOCUMENT_ID
        FROM
            dbo.SR_CANCEL_RENEGOTIATION_DOCUMENT
        WHERE
            CONTRACT_ID = @Contract_Id;

        SET @Lookup_Value = (   SELECT
                                    CONTRACT.CONTRACT_ID
                                    , CONTRACT.ED_CONTRACT_NUMBER
                                    , CONTRACT.CONTRACT_START_DATE
                                    , CONTRACT.CONTRACT_END_DATE
                                    , Base_Contract.ED_CONTRACT_NUMBER AS Base_ED_Contract_Number
                                    , ENTITY_NAME Contract_Type
                                    , VENDOR_NAME
                                    , Commodity_Name
                                FROM
                                    dbo.CONTRACT
                                    INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP
                                        ON SUPPLIER_ACCOUNT_METER_MAP.Contract_ID = CONTRACT.CONTRACT_ID
                                    INNER JOIN dbo.ACCOUNT
                                        ON ACCOUNT.ACCOUNT_ID = SUPPLIER_ACCOUNT_METER_MAP.ACCOUNT_ID
                                    INNER JOIN dbo.VENDOR
                                        ON VENDOR.VENDOR_ID = ACCOUNT.VENDOR_ID
                                    LEFT JOIN dbo.CONTRACT Base_Contract
                                        ON Base_Contract.CONTRACT_ID = CONTRACT.BASE_CONTRACT_ID
                                    INNER JOIN dbo.ENTITY
                                        ON ENTITY_ID = CONTRACT.CONTRACT_TYPE_ID
                                    INNER JOIN dbo.Commodity
                                        ON Commodity_Id = CONTRACT.COMMODITY_TYPE_ID
                                WHERE
                                    CONTRACT.CONTRACT_ID = @Contract_Id
                                GROUP BY
                                    CONTRACT.CONTRACT_ID
                                    , CONTRACT.ED_CONTRACT_NUMBER
                                    , CONTRACT.CONTRACT_START_DATE
                                    , CONTRACT.CONTRACT_END_DATE
                                    , Base_Contract.ED_CONTRACT_NUMBER
                                    , ENTITY_NAME
                                    , VENDOR_NAME
                                    , Commodity_Name
                                FOR XML RAW);

        SELECT
            @Contract_Entity_Id = ENTITY_ID
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_NAME = 'CONTRACT_TABLE'
            AND ENTITY_DESCRIPTION = 'Table Type';

        SELECT
            @Client_Name = ch.Client_Name
        FROM
            Core.Client_Hier ch
            JOIN Core.Client_Hier_Account cha
                ON cha.Client_Hier_Id = ch.Client_Hier_Id
        WHERE
            cha.Supplier_Contract_ID = @Contract_Id
        GROUP BY
            ch.Client_Name;

        SELECT
            @User_Name = FIRST_NAME + SPACE(1) + LAST_NAME
        FROM
            dbo.USER_INFO
        WHERE
            USER_INFO_ID = @User_Info_Id;

        INSERT INTO @Contract_Cbms_Image_Map_List
             (
                 Contract_Cbms_Image_Map_Id
             )
        SELECT
            CONTRACT_CBMS_IMAGE_MAP_ID
        FROM
            dbo.CONTRACT_CBMS_IMAGE_MAP
        WHERE
            CONTRACT_ID = @Contract_Id;

        INSERT INTO @Contract_Meter_Volume_List
             (
                 Contract_Meter_Volume_Id
             )
        SELECT
            CONTRACT_METER_VOLUME_ID
        FROM
            dbo.CONTRACT_METER_VOLUME
        WHERE
            CONTRACT_ID = @Contract_Id;

        INSERT INTO @Hedge_List
             (
                 Hedge_Id
             )
        SELECT  HEDGE_ID FROM   dbo.HEDGE WHERE CONTRACT_ID = @Contract_Id;

        INSERT INTO @Hedge_Detail_List
             (
                 Hedge_Detail_Id
             )
        SELECT
            hd.HEDGE_DETAIL_ID
        FROM
            dbo.HEDGE_DETAIL hd
            JOIN @Hedge_List hl
                ON hd.HEDGE_ID = hl.Hedge_Id;

        INSERT INTO @Sso_Project_Contract_Map_List
             (
                 Sso_Project_Id
             )
        SELECT
            SSO_PROJECT_ID
        FROM
            dbo.SSO_PROJECT_CONTRACT_MAP
        WHERE
            CONTRACT_ID = @Contract_Id;

        INSERT INTO @Supplier_Account_Meter_Map_List
             (
                 Account_Id
                 , Meter_Id
             )
        SELECT
            ACCOUNT_ID
            , METER_ID
        FROM
            dbo.SUPPLIER_ACCOUNT_METER_MAP
        WHERE
            Contract_ID = @Contract_Id;




        INSERT INTO @Contract_Lock_Dtl_List
             (
                 Contract_Lock_Dtl_Id
             )
        SELECT
            cld.Contract_Lock_Dtl_Id
        FROM
            dbo.Contract_Lock_Dtl cld
        WHERE
            cld.Contract_Id = @Contract_Id;


        INSERT INTO @EC_Contract_Attribute_Tracking
             (
                 EC_Contract_Attribute_Tracking_Id
             )
        SELECT
            ecat.EC_Contract_Attribute_Tracking_Id
        FROM
            dbo.EC_Contract_Attribute_Tracking ecat
        WHERE
            ecat.Contract_Id = @Contract_Id;

        BEGIN TRY

            ------- Delete the contract level recalcs once copy the recalcs to account level.

            EXEC dbo.Delete_Contract_Recalc_Config_Copy_To_Account_Level
                @Contract_Id
                , @User_Info_Id;


            EXEC dbo.Charge_History_Del_For_Contract @Contract_Id;

            EXEC dbo.Budget_Contract_Budget_History_Del_For_Contract @Contract_Id;


            SELECT  @Total_Row_Count = MAX(Row_Num)FROM @Contract_Cbms_Image_Map_List;

            SET @Row_Counter = 1;
            WHILE (@Row_Counter <= @Total_Row_Count)
                BEGIN

                    SELECT
                        @Contract_Cbms_Image_Map_Id = Contract_Cbms_Image_Map_Id
                    FROM
                        @Contract_Cbms_Image_Map_List
                    WHERE
                        Row_Num = @Row_Counter;

                    EXEC dbo.Contract_Cbms_Image_Map_Del @Contract_Cbms_Image_Map_Id;

                    SET @Row_Counter = @Row_Counter + 1;

                END;

            SELECT  @Total_Row_Count = MAX(Row_Num)FROM @Contract_Meter_Volume_List;

            SET @Row_Counter = 1;
            WHILE (@Row_Counter <= @Total_Row_Count)
                BEGIN

                    SELECT
                        @Contract_Meter_Volume_Id = Contract_Meter_Volume_Id
                    FROM
                        @Contract_Meter_Volume_List
                    WHERE
                        Row_Num = @Row_Counter;

                    EXEC dbo.Contract_Meter_Volume_Del @Contract_Meter_Volume_Id;

                    SET @Row_Counter = @Row_Counter + 1;

                END;

            SELECT  @Total_Row_Count = MAX(Row_Num)FROM @Hedge_Detail_List;

            SET @Row_Counter = 1;
            WHILE (@Row_Counter <= @Total_Row_Count)
                BEGIN

                    SELECT
                        @Hedge_Detail_Id = Hedge_Detail_Id
                    FROM
                        @Hedge_Detail_List
                    WHERE
                        Row_Num = @Row_Counter;

                    EXEC dbo.Hedge_Detail_Del @Hedge_Detail_Id;

                    SET @Row_Counter = @Row_Counter + 1;

                END;

            SELECT  @Total_Row_Count = MAX(Row_Num)FROM @Hedge_List;

            SET @Row_Counter = 1;
            WHILE (@Row_Counter <= @Total_Row_Count)
                BEGIN

                    SELECT  @Hedge_Id = Hedge_Id FROM   @Hedge_List WHERE   Row_Num = @Row_Counter;

                    EXEC dbo.Hedge_Del @Hedge_Id;

                    SET @Row_Counter = @Row_Counter + 1;

                END;

            SELECT  @Total_Row_Count = MAX(Row_Num)FROM @Sso_Project_Contract_Map_List;

            SET @Row_Counter = 1;
            WHILE (@Row_Counter <= @Total_Row_Count)
                BEGIN

                    SELECT
                        @Sso_Project_Id = Sso_Project_Id
                    FROM
                        @Sso_Project_Contract_Map_List
                    WHERE
                        Row_Num = @Row_Counter;

                    EXEC dbo.Sso_Project_Contract_Map_Del @Sso_Project_Id, @Contract_Id;

                    SET @Row_Counter = @Row_Counter + 1;

                END;

            SELECT  @Total_Row_Count = MAX(Row_Num)FROM @Supplier_Account_Meter_Map_List;

            SET @Row_Counter = 1;
            WHILE (@Row_Counter <= @Total_Row_Count)
                BEGIN

                    SELECT
                        @Account_Id = Account_Id
                        , @Meter_Id = Meter_Id
                    FROM
                        @Supplier_Account_Meter_Map_List
                    WHERE
                        Row_Num = @Row_Counter;



                    UPDATE
                        sac
                    SET
                        sac.Contract_Id = -1
                        , sac.Updated_User_Id = @User_Info_Id
                        , sac.Last_Change_Ts = GETDATE()
                    FROM
                        dbo.Supplier_Account_Config sac
                    WHERE
                        sac.Account_Id = @Account_Id
                        AND sac.Contract_Id = @Contract_Id;


                    UPDATE
                        samm
                    SET
                        samm.Contract_ID = -1
                        , samm.Updated_User_Id = @User_Info_Id
                        , samm.Last_Change_Ts = GETDATE()
                    FROM
                        dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                    WHERE
                        samm.ACCOUNT_ID = @Account_Id
                        AND samm.METER_ID = @Meter_Id
                        AND samm.Contract_ID = @Contract_Id;

                    SET @Row_Counter = @Row_Counter + 1;


                END;




            SELECT  @Total_Row_Count = MAX(Row_Num)FROM @Sr_Batch_Log_Details_List;

            SET @Row_Counter = 1;
            WHILE (@Row_Counter <= @Total_Row_Count)
                BEGIN

                    SELECT
                        @Sr_Batch_Log_Details_Id = Sr_Batch_Log_Details_Id
                    FROM
                        @Sr_Batch_Log_Details_List
                    WHERE
                        Row_Num = @Row_Counter;

                    EXEC dbo.SR_Batch_Log_Details_Del @Sr_Batch_Log_Details_Id;

                    SET @Row_Counter = @Row_Counter + 1;
                END;

            SELECT
                @Total_Row_Count = MAX(Row_Num)
            FROM
                @Sr_Cancel_Renegotiation_Document_List;

            SET @Row_Counter = 1;
            WHILE (@Row_Counter <= @Total_Row_Count)
                BEGIN

                    SELECT
                        @Sr_Cancel_Renegotiation_Document_Id = Sr_Cancel_Renegotiation_Document_Id
                    FROM
                        @Sr_Cancel_Renegotiation_Document_List
                    WHERE
                        Row_Num = @Row_Counter;

                    EXEC dbo.SR_Cancel_Renegotiation_Document_Del
                        @Sr_Cancel_Renegotiation_Document_Id;

                    SET @Row_Counter = @Row_Counter + 1;
                END;




            SELECT  @Total_Row_Count = MAX(Row_Num)FROM @Contract_Lock_Dtl_List;

            SET @Row_Counter = 1;
            WHILE (@Row_Counter <= @Total_Row_Count)
                BEGIN

                    SELECT
                        @Contract_Lock_Dtl_Id = Contract_Lock_Dtl_Id
                    FROM
                        @Contract_Lock_Dtl_List
                    WHERE
                        Row_Num = @Row_Counter;


                    EXEC dbo.Contract_Lock_Dtl_Del @Contract_Lock_Dtl_Id;

                    SET @Row_Counter = @Row_Counter + 1;
                END;


            SELECT  @Total_Row_Count = MAX(Row_Num)FROM @EC_Contract_Attribute_Tracking;

            SET @Row_Counter = 1;
            WHILE (@Row_Counter <= @Total_Row_Count)
                BEGIN
                    SELECT
                        @EC_Contract_Attribute_Tracking_Id = EC_Contract_Attribute_Tracking_Id
                    FROM
                        @EC_Contract_Attribute_Tracking
                    WHERE
                        Row_Num = @Row_Counter;

                    DELETE
                    dbo.EC_Contract_Attribute_Tracking
                    WHERE
                        EC_Contract_Attribute_Tracking_Id = @EC_Contract_Attribute_Tracking_Id;

                    SET @Row_Counter = @Row_Counter + 1;
                END;



            EXEC dbo.Entity_Audit_Del_By_Entity_Id_Entity_Identifier
                @Contract_Entity_Id
                , @Contract_Id;



            DELETE
            cnlmd
            FROM
                dbo.Contract_Notification_Log cnl
                INNER JOIN dbo.Contract_Notification_Log_Meter_Dtl cnlmd
                    ON cnl.Contract_Notification_Log_Id = cnlmd.Contract_Notification_Log_Id
            WHERE
                cnl.Contract_Id = @Contract_Id;



            DELETE
            cnl
            FROM
                dbo.Contract_Notification_Log cnl
            WHERE
                cnl.Contract_Id = @Contract_Id;







            DELETE
            crc
            FROM
                dbo.Contract_Recalc_Config crc
            WHERE
                crc.Contract_Id = @Contract_Id;


            EXEC dbo.Contract_Del @Contract_Id;

            SET @Current_Ts = GETDATE();
            EXEC dbo.Application_Audit_Log_Ins
                @Client_Name
                , @Application_Name
                , @Audit_Function
                , 'CONTRACT'
                , @Lookup_Value
                , @User_Name
                , @Current_Ts;

        END TRY
        BEGIN CATCH


            EXEC dbo.usp_RethrowError;

        END CATCH;
    END;








GO
GRANT EXECUTE ON  [dbo].[Contract_History_Del_By_Contract_Id] TO [CBMSApplication]
GO
