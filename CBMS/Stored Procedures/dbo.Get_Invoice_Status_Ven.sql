SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO












/******        
NAME:      
 dbo.Get_Invoice_Status_Ven

DESCRIPTION:
		Used to update variance closure category and reason for the failed test
		
 INPUT PARAMETERS:      
	Name			DataType  Default Description      
------------------------------------------------------------      
	@Account_Id	INT
	@Old_Closure_Category_reason	varchar		NULL
	@Service_Month	DATE
	@New_reason_Cd		INT
	
 OUTPUT PARAMETERS:     
 Name   DataType  Default Description      
------------------------------------------------------------      
 USAGE EXAMPLES:      
------------------------------------------------------------
DECLARE @tvp_Variance_log_Ven AS tvp_Variance_log_Ven

EXEC dbo.Get_Invoice_Status_Ven
      @Account_Id = 741609              -- int
    , @Commodity_id = 290             -- int
    , @Service_Month = '2018-08-01' -- date
    , @tvp_Variance_log_Ven = @tvp_Variance_log_Ven

	
 AUTHOR INITIALS:
 Initials	Name
------------------------------------------------------------
 AP	Arunkumar Palanivel
 
 MODIFICATIONS
 Initials	     Date		     Modification
------------------------------------------------------------
AP				May 26,2020		New procedure is created to Get Ven STatus


******/

CREATE PROCEDURE [dbo].[Get_Invoice_Status_Ven]
      (
      @Account_Id              INT 
    , @Service_Month           DATE
    , @tvp_Variance_log_Ven AS tvp_Variance_log_Ven READONLY )
AS
      BEGIN

            SET NOCOUNT ON;



            DECLARE @Invoice_Status TABLE
                  ( Status_Value VARCHAR(200) NULL
                  , row_num      INT          IDENTITY(1, 1));


            INSERT INTO @Invoice_Status (
                                              Status_Value
                                        )
            VALUES
                 ( 'Data received/collected' )
               , ( 'Invoice data captured' )
               , ( 'Service type confirmed' )
               , ( 'Account number confirmed' )
               , ( 'Preliminary data quality check' )
               , ( 'Data points categorized' )
               , ( 'Duplicate check' )
               , ( 'Service date/month selection' );



            IF EXISTS
                  (     SELECT
                              1
                        FROM  dbo.CU_INVOICE_SERVICE_MONTH CUI
                              JOIN
                              dbo.CU_INVOICE_PROCESS CIp
                                    ON CIp.CU_INVOICE_ID = CUI.CU_INVOICE_ID
                        WHERE CUI.Account_ID = @Account_Id
                              AND   CUI.SERVICE_MONTH = @Service_Month
                              AND   CIp.Is_Data_Quality_Test = 0 )
                  DELETE FROM @Invoice_Status
                  WHERE Status_Value = 'Preliminary data quality check';


            IF EXISTS
                  (     SELECT
                              1
                        FROM  dbo.CU_INVOICE_SERVICE_MONTH CUI
                              JOIN
                              dbo.CU_INVOICE_PROCESS CIp
                                    ON CIp.CU_INVOICE_ID = CUI.CU_INVOICE_ID
                        WHERE CUI.Account_ID = @Account_Id
                              AND   CUI.SERVICE_MONTH = @Service_Month
                              AND   CIp.INVOICE_AGGREGATE = 0 )
                  DELETE FROM @Invoice_Status
                  WHERE Status_Value = 'Data points categorized';


            IF EXISTS
                  (     SELECT
                              1
                        FROM  dbo.Variance_Log vl
                              JOIN
                              @tvp_Variance_log_Ven tvp
                                    ON vl.Variance_Log_Id = tvp.Variance_log_Id
                        WHERE vl.AVT_R2_Score IS NOT NULL )
                  BEGIN

                        INSERT INTO @Invoice_Status (
                                                          Status_Value
                                                    )
                        VALUES
                             ( 'Algorithmic outliers' );

                  END;


            IF EXISTS
                  (     SELECT
                              1
                        FROM  dbo.Variance_Log vl
                        WHERE vl.Account_ID = @Account_Id 
                              AND   vl.Service_Month = @Service_Month
                              AND   vl.Category_Name = 'U/S Counterpart'
                              AND   vl.Partition_Key IS NOT NULL )
                  BEGIN

                        INSERT INTO @Invoice_Status (
                                                          Status_Value
                                                    )
                        VALUES
                             ( 'Utility/supplier usage match' );

                  END;

            IF EXISTS
                  (     SELECT
                              1
                        FROM  dbo.Variance_Log vl
                        WHERE vl.Account_ID = @Account_Id 
                              AND   vl.Service_Month = @Service_Month
                              AND   vl.AVT_R2_Score IS NOT NULL
                              AND   vl.Category_Name = 'Billed Demand'
                              AND   vl.Partition_Key IS NOT NULL )
                  BEGIN

                        INSERT INTO @Invoice_Status (
                                                          Status_Value
                                                    )
                        VALUES
                             ( 'Billed Demand' );

                  END;



            SELECT
                        Status_Value
            FROM        @Invoice_Status
            ORDER BY    row_num;

      END;
      ;
GO
GRANT EXECUTE ON  [dbo].[Get_Invoice_Status_Ven] TO [CBMSApplication]
GO
