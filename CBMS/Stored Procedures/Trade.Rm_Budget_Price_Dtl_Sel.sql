SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.Rm_Budget_Price_Dtl_Sel                        
                          
Description:                          
        To get market price and forecast pirce if a selected index   
                          
Input Parameters:                          
    Name    DataType        Default     Description                            
--------------------------------------------------------------------------------    
	@Index_Id   INT    
    @Start_Dt	Date    
	@End_Dt		Date
                          
 Output Parameters:                                
	Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
Usage Examples:                              
--------------------------------------------------------------------------------    
	SELECT * FROM dbo.ENTITY e WHERE e.ENTITY_TYPE=272

	EXEC Trade.Rm_Budget_Price_Dtl_Sel  1005
    
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR			2019-12-23	RM-Budgets Enahancement - Created
	RR			2020-03-06	GRM-1743 Added ORDER BY clause	               
******/
CREATE PROCEDURE [Trade].[Rm_Budget_Price_Dtl_Sel]
     (
         @Rm_Budget_Id INT
     )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            rbpd.Rm_Budget_Id
            , rbpd.Service_Month AS Budget_Month
            , rbpd.Rm_Budget_Price
        FROM
            Trade.Rm_Budget_Price_Dtl rbpd
        WHERE
            rbpd.Rm_Budget_Id = @Rm_Budget_Id
        ORDER BY
            rbpd.Service_Month;

    END;

GO
GRANT EXECUTE ON  [Trade].[Rm_Budget_Price_Dtl_Sel] TO [CBMSApplication]
GO
