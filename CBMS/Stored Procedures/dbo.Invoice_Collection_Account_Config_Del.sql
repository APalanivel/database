SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.Invoice_Collection_Account_Config_Del            
                        
 DESCRIPTION:                        
			To delete the details of invoice collection account level             
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@Invoice_Collection_Account_Config_Id	INT		NULL 
                              
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
    
BEGIN TRAN
  EXEC dbo.Invoice_Collection_Account_Config_Del 
      @Invoice_Collection_Account_Config_Id = 1342210
ROLLBACK      

                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2016-12-08       Created   for Invoice tracking
 RKV                   2018-01-04       MAINT8063 Added Invoice_Collection_Issue_Event delete statement
 RKV                   2019-06-12       Added two new source tables to delete functionality as part IC Project  
 RKV				   2019-07-16		Added Exclude_Comments table to delete.	
 RKV                   2019-09-03		Added Final review and Activity tables to delete.             
                       
******/

CREATE PROCEDURE [dbo].[Invoice_Collection_Account_Config_Del]
     (
         @Invoice_Collection_Account_Config_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Is_Chase_History_Exists_tbl TABLE
              (
                  Is_Chase_History_Exists BIT
              );

        DECLARE @Is_ICQ_Exists BIT = 0;

        INSERT INTO @Is_Chase_History_Exists_tbl
             (
                 Is_Chase_History_Exists
             )
        EXEC Invoice_Collection_Chase_History_Exists_For_Invoice_Collection_Account_Config_Id
            @Invoice_Collection_Account_Config_Id;

        SELECT
            @Is_ICQ_Exists = Is_Chase_History_Exists
        FROM
            @Is_Chase_History_Exists_tbl;


        BEGIN TRY
            BEGIN TRAN;


            DELETE  FROM
            dbo.Invoice_Collection_Account_Contact
            WHERE
                Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
                AND @Is_ICQ_Exists = 0;

            DELETE
            sd
            FROM
                dbo.Account_Invoice_Collection_UBM_Source_Dtl sd
                INNER JOIN dbo.Account_Invoice_Collection_Source aic
                    ON sd.Account_Invoice_Collection_Source_Id = aic.Account_Invoice_Collection_Source_Id
            WHERE
                aic.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
                AND @Is_ICQ_Exists = 0;


            DELETE
            os
            FROM
                dbo.Account_Invoice_Collection_Online_Source_Dtl os
                INNER JOIN dbo.Account_Invoice_Collection_Source aic
                    ON os.Account_Invoice_Collection_Source_Id = aic.Account_Invoice_Collection_Source_Id
            WHERE
                aic.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
                AND @Is_ICQ_Exists = 0;

            DELETE
            efsd
            FROM
                dbo.Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl AS efsd
                INNER JOIN dbo.Account_Invoice_Collection_Source aic
                    ON efsd.Account_Invoice_Collection_Source_Id = aic.Account_Invoice_Collection_Source_Id
            WHERE
                aic.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
                AND @Is_ICQ_Exists = 0;

            DELETE
            eisd
            FROM
                dbo.Account_Invoice_Collection_Data_Feed_Etl_Image_Source_Dtl AS eisd
                INNER JOIN dbo.Account_Invoice_Collection_Source aic
                    ON eisd.Account_Invoice_Collection_Source_Id = aic.Account_Invoice_Collection_Source_Id
            WHERE
                aic.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
                AND @Is_ICQ_Exists = 0;

            DELETE
            aic
            FROM
                dbo.Account_Invoice_Collection_Source aic
            WHERE
                aic.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
                AND @Is_ICQ_Exists = 0;

            DELETE
            dt
            FROM
                dbo.Account_Invoice_Collection_Frequency dt
                INNER JOIN dbo.Invoice_Collection_Account_Config aic
                    ON dt.Invoice_Collection_Account_Config_Id = aic.Invoice_Collection_Account_Config_Id
            WHERE
                aic.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
                AND @Is_ICQ_Exists = 0;


            DELETE
            aicmc
            FROM
                dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map aicmc
                INNER JOIN dbo.Account_Invoice_Collection_Month aicf
                    ON aicmc.Account_Invoice_Collection_Month_Id = aicf.Account_Invoice_Collection_Month_Id
                INNER JOIN dbo.Invoice_Collection_Account_Config icac
                    ON aicf.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            WHERE
                icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
                AND @Is_ICQ_Exists = 0;





            DELETE
            icqmm
            FROM
                dbo.Invoice_Collection_Queue_Month_Map icqmm
                INNER JOIN dbo.Invoice_Collection_Queue icq
                    ON icqmm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                INNER JOIN dbo.Invoice_Collection_Account_Config icac
                    ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            WHERE
                icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
                AND @Is_ICQ_Exists = 0;



            DELETE
            aicm
            FROM
                dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map aicm
                INNER JOIN dbo.Account_Invoice_Collection_Month aicf
                    ON aicm.Account_Invoice_Collection_Month_Id = aicf.Account_Invoice_Collection_Month_Id
                INNER JOIN dbo.Invoice_Collection_Account_Config icac
                    ON aicf.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            WHERE
                icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
                AND @Is_ICQ_Exists = 0;



            DELETE
            aicf
            FROM
                dbo.Account_Invoice_Collection_Month aicf
                INNER JOIN dbo.Invoice_Collection_Account_Config icac
                    ON aicf.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            WHERE
                icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
                AND @Is_ICQ_Exists = 0;



            DELETE
            icclqm
            FROM
                dbo.Invoice_Collection_Chase_Log_Queue_Map icclqm
                INNER JOIN dbo.Invoice_Collection_Queue icq
                    ON icclqm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                INNER JOIN dbo.Invoice_Collection_Account_Config icac
                    ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            WHERE
                icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
                AND @Is_ICQ_Exists = 0;


            DELETE
            icfrlqm
            FROM
                dbo.Invoice_Collection_Final_Review_Log_Queue_Map icfrlqm
                INNER JOIN dbo.Invoice_Collection_Queue icq
                    ON icq.Invoice_Collection_Queue_Id = icfrlqm.Invoice_Collection_Queue_Id
                INNER JOIN dbo.Invoice_Collection_Account_Config icac
                    ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            WHERE
                 icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
				 AND @Is_ICQ_Exists = 0;

	--Delete from Activity Log
			DELETE
            icalqm
            FROM
                dbo.Invoice_Collection_Activity_Log_Queue_Map icalqm
                INNER JOIN dbo.Invoice_Collection_Queue icq
                    ON icq.Invoice_Collection_Queue_Id = icalqm.Invoice_Collection_Queue_Id
                INNER JOIN dbo.Invoice_Collection_Account_Config icac
                    ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            WHERE
                 icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
				 AND @Is_ICQ_Exists = 0;
			
			
			
			DELETE
            icie
            FROM
                dbo.Invoice_Collection_Issue_Event icie
                INNER JOIN dbo.Invoice_Collection_Issue_Log icil
                    ON icil.Invoice_Collection_Issue_Log_Id = icie.Invoice_Collection_Issue_Log_Id
                INNER JOIN dbo.Invoice_Collection_Queue icq
                    ON icil.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                INNER JOIN dbo.Invoice_Collection_Account_Config icac
                    ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                INNER JOIN dbo.Code isc
                    ON isc.Code_Id = icil.Issue_Status_Cd
            WHERE
                icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
                AND @Is_ICQ_Exists = 0;

            DELETE
            icil
            FROM
                dbo.Invoice_Collection_Issue_Log icil
                INNER JOIN dbo.Invoice_Collection_Queue icq
                    ON icil.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                INNER JOIN dbo.Invoice_Collection_Account_Config icac
                    ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                INNER JOIN dbo.Code isc
                    ON isc.Code_Id = icil.Issue_Status_Cd
            WHERE
                icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
                AND @Is_ICQ_Exists = 0;


            DELETE
            icec
            FROM
                dbo.Invoice_Collection_Exception_Comment icec
                INNER JOIN dbo.Invoice_Collection_Queue icq
                    ON icec.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                INNER JOIN dbo.Invoice_Collection_Account_Config icac
                    ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            WHERE
                icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
                AND @Is_ICQ_Exists = 0;

            DELETE
            icqec
            FROM
                dbo.Invoice_Collection_Queue_Exclude_Comment icqec
                INNER JOIN dbo.Invoice_Collection_Queue icq
                    ON icqec.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                INNER JOIN dbo.Invoice_Collection_Account_Config icac
                    ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            WHERE
                icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
                AND @Is_ICQ_Exists = 0;


            DELETE
            icq
            FROM
                dbo.Invoice_Collection_Queue icq
                INNER JOIN dbo.Invoice_Collection_Account_Config icac
                    ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            WHERE
                icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
                AND @Is_ICQ_Exists = 0;



            DELETE
            aic
            FROM
                dbo.Invoice_Collection_Account_Config aic
            WHERE
                aic.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
                AND @Is_ICQ_Exists = 0;


            COMMIT TRAN;
        END TRY
        BEGIN CATCH

            IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRAN;
                END;

            EXEC dbo.usp_RethrowError;

        END CATCH;


    END;




    ;



GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Account_Config_Del] TO [CBMSApplication]
GO
