SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--SELECT * FROM SR_RFP_SOP_SUMMARY

--SELECT * FROM SR_RFP_SOP_DETAILS WHERE SR_RFP_SOP_DETAILS_ID BETWEEN 1817 and 1836


--exec SR_RFP_GET_VIEW_SOP_P 1,1,7031,0,NULL

CREATE          PROCEDURE dbo.SR_RFP_GET_VIEW_SOP_P

@userId varchar,
@sessionId varchar,
@accountGroupId int,
@isBidGroupId int,
@productName varchar(200)
as
	set nocount on

declare @minSOPDetailsId int
declare @maxSOPDetailsId int

select @minSOPDetailsId= min(details.sr_rfp_sop_details_id) 
			from sr_rfp_sop_summary summary,sr_rfp_sop sop,sr_rfp_sop_details details 
			where 
			summary.sr_account_group_id=@accountGroupId and
			summary.is_bid_group=@isBidGroupId and
			summary.sr_rfp_sop_summary_id = sop.sr_rfp_sop_summary_id and
			sop.sr_rfp_sop_id=details.sr_rfp_sop_id 

select @maxSOPDetailsId= max(details.sr_rfp_sop_details_id) 
			from sr_rfp_sop_summary summary,sr_rfp_sop sop,sr_rfp_sop_details details
			where
			summary.sr_account_group_id=@accountGroupId and
			summary.is_bid_group=@isBidGroupId and
			summary.sr_rfp_sop_summary_id = sop.sr_rfp_sop_summary_id and
			sop.sr_rfp_sop_id=details.sr_rfp_sop_id 

IF @productName IS NOT NULL

BEGIN

select 

	X.sr_rfp_supplier_contact_vendor_map_id,
	X.supplier_contact_Map,
	X.comments,
	X.sr_rfp_account_term_id,
	X.from_month,
	X.to_month,
	X.product_name,
	X.sr_rfp_bid_id,
	X.price_response_comments,
	X.is_recommended,
	X.is_saved,
	@minSOPDetailsId MIN_SOP_DETAILS_ID,
	@maxSOPDetailsId MAX_SOP_DETAILS_ID

	

from


(select 
	sop.sr_rfp_supplier_contact_vendor_map_id,
	sop.vendor_name supplier_contact_Map,
	sop.comments,
	details.sr_rfp_account_term_id,
	term.from_month,
	term.to_month,
	details.PRODUCT_NAME+'~'+
	CASE 
	WHEN bid.PRODUCT_NAME is null THEN ''
	ELSE bid.PRODUCT_NAME
	END PRODUCT_NAME,

	details.sr_rfp_bid_id,
	details.price_response_comments,
	details.is_recommended,
	sop.is_saved


from

	sr_rfp_sop_summary summary,
	sr_rfp_sop sop,
	sr_rfp_account_term term,
	sr_rfp_sop_details details,
	sr_rfp_bid bid


where
	summary.sr_account_group_id=@accountGroupId and
	summary.is_bid_group=@isBidGroupId and
	summary.sr_rfp_sop_summary_id = sop.sr_rfp_sop_summary_id and
	sop.sr_rfp_sop_id=details.sr_rfp_sop_id and
	term.sr_rfp_account_term_id=details.sr_rfp_account_term_id and
	term.is_sop=1 and
	details.sr_rfp_bid_id=bid.sr_rfp_bid_id) as X

WHERE 
	X.PRODUCT_NAME=@productName

order by X.sr_rfp_supplier_contact_vendor_map_id
--X.from_month,X.to_month



END

ELSE

BEGIN

	select 
	sop.sr_rfp_supplier_contact_vendor_map_id,
	sop.vendor_name supplier_contact_Map,
	sop.comments,
	details.sr_rfp_account_term_id,
	term.from_month,
	term.to_month,
	details.PRODUCT_NAME+'~'+
	CASE 
	WHEN bid.PRODUCT_NAME is null THEN ''
	ELSE bid.PRODUCT_NAME
	END PRODUCT_NAME,

	details.sr_rfp_bid_id,
	details.price_response_comments,
	details.is_recommended,
	sop.is_saved,
	@minSOPDetailsId MIN_SOP_DETAILS_ID,
	@maxSOPDetailsId MAX_SOP_DETAILS_ID


from

	sr_rfp_sop_summary summary,
	sr_rfp_sop sop,
	sr_rfp_account_term term,
	sr_rfp_sop_details details,
	sr_rfp_bid bid


where
	summary.sr_account_group_id=@accountGroupId and
	summary.is_bid_group=@isBidGroupId and
	summary.sr_rfp_sop_summary_id = sop.sr_rfp_sop_summary_id and
	sop.sr_rfp_sop_id=details.sr_rfp_sop_id and
	term.sr_rfp_account_term_id=details.sr_rfp_account_term_id and
	term.is_sop=1 and
	details.sr_rfp_bid_id=bid.sr_rfp_bid_id

order by sop.sr_rfp_supplier_contact_vendor_map_id
--term.from_month,term.to_month

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_VIEW_SOP_P] TO [CBMSApplication]
GO
