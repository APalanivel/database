SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*        
NAME:        
	Su.Client_Links_Upd        
   
DESCRIPTION:       
    
	Used to update the CLIENT_LINKS table      
    
INPUT PARAMETERS:        
Name			DataType  Default Description        
------------------------------------------------------------        
@CLIENT_ID		INT,       
@LINK_TYPE_CD	INT,         
@LINK_URL		VARCHAR(255),      
@LINK_NAME		VARCHAR(100)      
              
OUTPUT PARAMETERS:        
Name			DataType  Default Description  
------------------------------------------------------------        

USAGE EXAMPLES:        
------------------------------------------------------------      

EXEC Su.Client_Links_Ins_Upd 11231,9,'http://www.hotmail.com','hotMAILS'    

  
AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
GB   Geetansu Behera        
CMH  Chad Hattabaugh         
HG   Hari       
SKA  Shobhit Kr Agrawal      
   
 
MODIFICATIONS         
Initials Date  Modification        
------------------------------------------------------------        
GB				Created      
SKA	13-JUL-09	Modified      
SS	27-JUL-09	Split from SP , Su.Client_Links_Ins_Upd
      
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE Su.Client_Links_Upd  
   @CLIENT_ID INT,       
   @LINK_TYPE_CD INT,         
   @LINK_URL VARCHAR(255),      
   @LINK_NAME VARCHAR(100)
         
AS        

BEGIN        
        
        
SET NOCOUNT ON;    
  
BEGIN TRY    
 BEGIN TRANSACTION                                
    
	  UPDATE dbo.CLIENT_LINKS       
	  SET LINK_URL = @LINK_URL,      
		LINK_NAME=@LINK_NAME      
	  WHERE  CLIENT_ID = @CLIENT_ID    
		AND LINK_NAME=@LINK_NAME      
           
 COMMIT TRANSACTION    
       
END TRY     
  
BEGIN CATCH    
 ROLLBACK TRANSACTION    
  EXEC dbo.usp_RethrowError                       
END CATCH    
  
END
GO
GRANT EXECUTE ON  [su].[Client_Links_Upd] TO [CBMSApplication]
GO
