SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   procedure [dbo].[cbmsRMInsight_GetForAuthor]
	( @MyAccountId int
	, @author_id int
	)
as
begin
SELECT rm_risk_insight_id 
	,insight_date 
	,insight_title
	,insight_text
	,author_id
	,ui.first_name + ' ' + ui.last_name [author]

FROM RM_RISK_INSIGHT rri
	join user_info ui on ui.user_info_id = rri.author_id
where author_id = @author_id
order by insight_date desc

end
GO
GRANT EXECUTE ON  [dbo].[cbmsRMInsight_GetForAuthor] TO [CBMSApplication]
GO
