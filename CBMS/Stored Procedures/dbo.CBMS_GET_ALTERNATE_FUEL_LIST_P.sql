SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.CBMS_GET_ALTERNATE_FUEL_LIST_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE   PROCEDURE dbo.CBMS_GET_ALTERNATE_FUEL_LIST_P


AS
select entity_id , entity_name from entity where entity_type = 116 
and entity_description='Alternate Fuel List'
order by entity_name
GO
GRANT EXECUTE ON  [dbo].[CBMS_GET_ALTERNATE_FUEL_LIST_P] TO [CBMSApplication]
GO
