SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE          PROCEDURE [dbo].[cbmsCuInvoiceProcess_Get]
	( @MyAccountId int
	, @cu_invoice_process_id int
	)
AS
BEGIN

	   select cu_invoice_process_id
		, cu_invoice_id
		, cu_invoice_created
		, service_mapping
		, resolve_to_account
		, cu_duplicate_test
		, dm_watch_list
		, required_data
		, data_mapped
		, ra_watch_list
		, resolve_to_month
		, contract_recalc
		, invoice_aggregate
		, variance_test
	     from cu_invoice_process with (nolock)
	    where cu_invoice_process_id = @cu_invoice_process_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCuInvoiceProcess_Get] TO [CBMSApplication]
GO
