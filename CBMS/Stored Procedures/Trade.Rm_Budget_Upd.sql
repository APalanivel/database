SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.Rm_Budget_Upd                        
                          
Description:                          
        To get market price and forecast pirce if a selected index   
                          
Input Parameters:                          
    Name    DataType        Default     Description                            
--------------------------------------------------------------------------------    
	@Index_Id   INT    
    @Start_Dt	Date    
	@End_Dt		Date
                          
 Output Parameters:                                
	Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
Usage Examples:                              
--------------------------------------------------------------------------------    
	SELECT * FROM dbo.ENTITY e WHERE e.ENTITY_TYPE=272

	EXEC Trade.Rm_Budget_Upd  1005
    
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR			2019-12-23	RM-Budgets Enahancement - Created
	                
******/
CREATE PROCEDURE [Trade].[Rm_Budget_Upd]
    (
        @Rm_Budget_Id INT
        , @Is_Budget_Approved BIT = NULL
        , @Comment_Text NVARCHAR(MAX) = NULL
        , @Budget_NAME NVARCHAR(1000) = NULL
        , @User_Info_Id INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @COMMENT_ID INT;

        SELECT
            @COMMENT_ID = rb.Comment_Id
        FROM
            Trade.Rm_Budget_Comment rb
        WHERE
            rb.Rm_Budget_Id = @Rm_Budget_Id;

        UPDATE
            rb
        SET
            rb.Budget_NAME = ISNULL(@Budget_NAME, rb.Budget_NAME)
        FROM
            Trade.Rm_Budget rb
        WHERE
            rb.Rm_Budget_Id = @Rm_Budget_Id;

        UPDATE
            rb
        SET
            rb.Is_Budget_Approved = ISNULL(@Is_Budget_Approved, rb.Is_Budget_Approved)
            , rb.Approved_Ts = CASE WHEN @Is_Budget_Approved = 1 THEN GETDATE()
                                   WHEN @Is_Budget_Approved = 0 THEN NULL
                               END
            , rb.Approved_By_User_Info_Id = @User_Info_Id
        FROM
            Trade.Rm_Budget rb
        WHERE
            rb.Rm_Budget_Id = @Rm_Budget_Id
            AND rb.Is_Budget_Approved <> ISNULL(@Is_Budget_Approved, 0);


        BEGIN
            IF NULLIF(@Comment_Text, '') IS NOT NULL
                EXEC dbo.COMMENT_UPD
                    @COMMENT_ID = @COMMENT_ID
                    , @COMMENT_TEXT = @Comment_Text;
        END;

        INSERT INTO Trade.Rm_Budget_Site_Default_Budget_Config
             (
                 Client_Hier_Id
                 , Rm_Budget_Id
                 , Start_Dt
                 , End_Dt
                 , Created_User_Id
                 , Created_Ts
                 , Last_Updated_By
                 , Last_Change_Ts
             )
        SELECT
            rbp.Client_Hier_Id
            , rb.Rm_Budget_Id
            , rb.Budget_Start_Dt
            , rb.Budget_End_Dt
            , rb.Created_User_Id
            , GETDATE()
            , rb.Created_User_Id
            , GETDATE()
        FROM
            Trade.Rm_Budget_Participant rbp
            INNER JOIN Trade.Rm_Budget rb
                ON rb.Rm_Budget_Id = rbp.Rm_Budget_Id
        WHERE
            rb.Rm_Budget_Id = @Rm_Budget_Id
            AND rb.Is_Budget_Approved = 1
            --AND @Is_Client_Generated = 1
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.Rm_Budget_Site_Default_Budget_Config rbsdbc
                               WHERE
                                    rbp.Client_Hier_Id = rbsdbc.Client_Hier_Id
                                    AND (   rbsdbc.Start_Dt BETWEEN rb.Budget_Start_Dt
                                                            AND     rb.Budget_End_Dt
                                            OR  rbsdbc.End_Dt BETWEEN rb.Budget_Start_Dt
                                                              AND     rb.Budget_End_Dt
                                            OR  rb.Budget_Start_Dt BETWEEN rbsdbc.Start_Dt
                                                                   AND     rbsdbc.End_Dt
                                            OR  rb.Budget_End_Dt BETWEEN rbsdbc.Start_Dt
                                                                 AND     rbsdbc.End_Dt));

        UPDATE
            Trade.Rm_Budget
        SET
            Last_Change_Ts = GETDATE()
        WHERE
            Rm_Budget_Id = @Rm_Budget_Id;

    END;

GO
GRANT EXECUTE ON  [Trade].[Rm_Budget_Upd] TO [CBMSApplication]
GO
