SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Account_Variance_Consumption_Level_Del]  
     
DESCRIPTION: 
	To Delete Account Variance Consumption Level for given Account_Id and Variance_Consumption_Level_Id.
      
INPUT PARAMETERS:          
NAME							DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Variance_Consumption_Level_Id	INT
@Account_Id						INT				

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  
	BEGIN TRAN
	EXEC Account_Variance_Consumption_Level_Del  1, 35
	ROLLBACK TRAN
   
	SELECT * 
   	FROM
		dbo.Account_Variance_Consumption_Level
     
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR			PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR			26-MAY-10	CREATED     
*/  

CREATE PROCEDURE dbo.Account_Variance_Consumption_Level_Del
    (
	@Variance_Consumption_Level_Id	INT
	,@Account_Id					INT
    )
AS 
BEGIN

    SET NOCOUNT ON;

    DELETE
   	FROM
		dbo.Account_Variance_Consumption_Level
	WHERE
		Variance_Consumption_Level_Id = @Variance_Consumption_Level_Id
		AND ACCOUNT_ID = @Account_Id

END
GO
GRANT EXECUTE ON  [dbo].[Account_Variance_Consumption_Level_Del] TO [CBMSApplication]
GO
