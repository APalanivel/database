SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Utility_Dtl_Opportunity_Ins

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	 @Commodity_Id				INT
     @Vendor_Id					INT 
     @Question_Commodity_Map_Id INT
     @Program_Dsc				VARCHAR(500) = NULL
     @Program_Req				VARCHAR(500) = NULL
     @Savings_Potential_Cd		INT = NULL
     @Comment_ID				INT = NULL

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN
	EXEC Utility_Dtl_Opportunity_Ins 291,30,1,'Program_Dsc Test','Program_Req Test',100310
	ROLLBACK TRAN
	
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	SKA			01/12/2011	Created
******/

CREATE PROC dbo.Utility_Dtl_Opportunity_Ins
      (
       @Commodity_Id INT
      ,@Vendor_Id INT
      ,@Question_Commodity_Map_Id INT
      ,@Program_Dsc VARCHAR(500) = NULL
      ,@Program_Req VARCHAR(500) = NULL
      ,@Savings_Potential_Cd INT = NULL
      ,@Comment_ID INT = NULL )
AS 
BEGIN
 
      SET nocount ON ;

      INSERT INTO
            Utility_Dtl_Opportunity
            (
             Question_Commodity_Map_Id
            ,Vendor_Commodity_Map_Id
            ,Program_Dsc
            ,Program_Req
            ,Savings_Potential_Cd
            ,Comment_ID )
            SELECT
                  @Question_Commodity_Map_Id
                 ,vcm.VENDOR_COMMODITY_MAP_ID
                 ,@Program_Dsc
                 ,@Program_Req
                 ,@Savings_Potential_Cd
                 ,@Comment_ID
            FROM
                  dbo.VENDOR_COMMODITY_MAP vcm
            WHERE
                  vcm.vendor_id = @Vendor_Id
                  AND vcm.COMMODITY_TYPE_ID = @Commodity_Id
END                        
GO
GRANT EXECUTE ON  [dbo].[Utility_Dtl_Opportunity_Ins] TO [CBMSApplication]
GO
