SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_CLIENT_FORECAST_VOLUMES_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@clientId      	int       	          	
	@divisionId    	int       	          	
	@siteId        	int       	          	
	@startDate     	datetime  	          	
	@endDate       	datetime  	          	
	@viewId        	int       	          	
	@unitId        	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE      PROCEDURE  dbo.GET_CLIENT_FORECAST_VOLUMES_P
@userId varchar(10),
@sessionId varchar(20),
@clientId int,
@divisionId int,
@siteId int,
@startDate Datetime,
@endDate Datetime,
@viewId int,
@unitId int

AS
if(@divisionId = -1 and @siteId = -1)
begin

	if(@viewId = 3) -- // Site level

		select viewsite.site_name NAMES,   details.month_identifier MONTH_IDENTIFIER,
--		sum(details.volume) VOLUME
		CAST( sum(details.VOLUME * consumption.CONVERSION_FACTOR) as decimal(15,3)) VOLUME
		
		from  
			RM_FORECAST_VOLUME_DETAILS details, 
			SITE sit  ,
			VWSITENAME viewsite,
			CONSUMPTION_UNIT_CONVERSION consumption,
			RM_ONBOARD_HEDGE_SETUP onboard 
		where

		details.site_id = sit.site_id
		and viewsite.site_id=sit.site_id 
		and details.rm_forecast_volume_id
		in (
			select rm_forecast_volume_id from rm_forecast_volume
			where  forecast_as_of_date=(select max(forecast_as_of_date) from rm_forecast_volume
							where client_id=@clientId)
			 and client_id = @clientId)
		and details.month_identifier>=@startDate and details.month_identifier<=@endDate and

		onboard.SITE_ID = sit.SITE_ID and
		onboard.VOLUME_UNITS_TYPE_ID=consumption.BASE_UNIT_ID AND
		consumption.CONVERTED_UNIT_ID=@unitId

		group by details.month_identifier, viewsite.site_name,consumption.CONVERSION_FACTOR
		order by viewsite.site_name, details.month_identifier

	else if(@viewId = 2) -- // Division level

		select div.division_name NAMES, details.month_identifier MONTH_IDENTIFIER, 
			sum(details.volume) VOLUME
		from  rm_forecast_volume_details details, division div where
--		details.division_id = div.division_id and
		 details.rm_forecast_volume_id
		in (select rm_forecast_volume_id from rm_forecast_volume
			where   forecast_as_of_date=(select max(forecast_as_of_date) from rm_forecast_volume
				where client_id = @clientId)
			 and client_id = @clientId)
		and client_id = @clientId
		and details.month_identifier>=@startDate and details.month_identifier<=@endDate
		group by details.month_identifier , div.division_name 


	else if(@viewId = 1) -- // Corporate level

	select    month_identifier MONTH_IDENTIFIER, 
	sum(volume) VOLUME from  rm_forecast_volume_details where
	 rm_forecast_volume_id
	in (select rm_forecast_volume_id from rm_forecast_volume
		where   forecast_as_of_date=(select max(forecast_as_of_date) from rm_forecast_volume
				where client_id = @clientId)
		 and client_id = @clientId)

	and month_identifier>=@startDate and month_identifier<=@endDate
	group by month_identifier

end
else if(@divisionId > 0 and @siteId = -1)
begin


	if(@viewId = 3) -- // Site level

	select sit.site_name NAMES,   details.month_identifier MONTH_IDENTIFIER, 
--	sum(details.volume) VOLUME
	CAST( sum(details.VOLUME * consumption.CONVERSION_FACTOR) as decimal(15,3)) VOLUME

	from  
		rm_forecast_volume_details details, 
		site sit ,
		VWSITENAME viewsite,
		CONSUMPTION_UNIT_CONVERSION consumption,
		RM_ONBOARD_HEDGE_SETUP onboard  
	where
	details.site_id = sit.site_id and 
	 sit.division_id=@divisionId
	and details.rm_forecast_volume_id
	in (select rm_forecast_volume_id from rm_forecast_volume
		where  forecast_as_of_date=(select max(forecast_as_of_date) from rm_forecast_volume
					where client_id = @clientId)
		 and client_id = @clientId)
	
	and details.month_identifier>=@startDate and details.month_identifier<=@endDate and 
	onboard.SITE_ID = sit.SITE_ID and	
	viewsite.SITE_ID=sit.SITE_ID	and
	onboard.VOLUME_UNITS_TYPE_ID=consumption.BASE_UNIT_ID AND
	consumption.CONVERTED_UNIT_ID=@unitId

	group by details.month_identifier, sit.site_name
	order by sit.site_name,details.month_identifier

	else if(@viewId = 2) -- // Division level

	select div.division_name NAMES,  details.month_identifier MONTH_IDENTIFIER,
	sum(details.volume) VOLUME
	from  rm_forecast_volume_details details, division div where
--	details.division_id = div.division_id and  

	div.division_id=@divisionId
	and details.rm_forecast_volume_id
	in (select rm_forecast_volume_id from rm_forecast_volume
		where   forecast_as_of_date=(select max(forecast_as_of_date) from rm_forecast_volume
				where client_id = @clientId)
		 and client_id = @clientId)
	and details.month_identifier>=@startDate and details.month_identifier<=@endDate
	group by details.month_identifier , div.division_name


end
else if(@divisionId = -1 and @siteId > 0)
begin


	select sit.site_name NAMES,   details.month_identifier MONTH_IDENTIFIER,
--	sum(details.volume) VOLUME
	CAST( sum(details.VOLUME * consumption.CONVERSION_FACTOR) as decimal(15,3)) VOLUME

	from  
		rm_forecast_volume_details details, 
--		site sit,
		VWSITENAME sit,
		CONSUMPTION_UNIT_CONVERSION consumption,
		RM_ONBOARD_HEDGE_SETUP onboard  

	where
	details.site_id = sit.site_id and details.site_id =@siteId
	and details.rm_forecast_volume_id
	in (select rm_forecast_volume_id from rm_forecast_volume
		where  forecast_as_of_date=(select max(forecast_as_of_date) from rm_forecast_volume
			where client_id = @clientId)
		 and client_id = @clientId)
	and details.month_identifier>=@startDate and details.month_identifier<=@endDate and
	onboard.SITE_ID = @siteId and
	
	onboard.VOLUME_UNITS_TYPE_ID=consumption.BASE_UNIT_ID AND
	consumption.CONVERTED_UNIT_ID=@unitId

	group by details.month_identifier, sit.site_name

end
else if(@divisionId > 0 and @siteId > 0)
begin

	select sit.site_name NAMES,   details.month_identifier MONTH_IDENTIFIER,
--	sum(details.volume) VOLUME
	CAST( sum(details.VOLUME * consumption.CONVERSION_FACTOR) as decimal(15,3)) VOLUME

	from  
		rm_forecast_volume_details details, 
		VWSITENAME  sit ,
		CONSUMPTION_UNIT_CONVERSION consumption,
		RM_ONBOARD_HEDGE_SETUP onboard  

	where
	details.site_id = sit.site_id and details.site_id =@siteId
	and details.rm_forecast_volume_id
	in (select rm_forecast_volume_id from rm_forecast_volume
		where  forecast_as_of_date=(select max(forecast_as_of_date) from rm_forecast_volume
			where client_id = @clientId)
		 and client_id = @clientId)
	and details.month_identifier>=@startDate and details.month_identifier<=@endDate and
	onboard.SITE_ID = @siteId and
	onboard.VOLUME_UNITS_TYPE_ID=consumption.BASE_UNIT_ID AND
	consumption.CONVERTED_UNIT_ID=@unitId

	group by details.month_identifier, sit.site_name
end
GO
GRANT EXECUTE ON  [dbo].[GET_CLIENT_FORECAST_VOLUMES_P] TO [CBMSApplication]
GO
