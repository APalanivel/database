SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                 
/******                        
 NAME: dbo.Currency_Unit_Sel_By_Site            
                        
 DESCRIPTION:                        
			To Get list of all currencies along with default for given site.                        
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
 @Client_Hier_Id              INT              
         
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 EXEC dbo.Currency_Unit_Sel_By_Site 
      @Client_Hier_Id = 5418
          
  EXEC dbo.Currency_Unit_Sel_By_Site 
      @Client_Hier_Id =379742   
           
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2014-09-01       Created for Data Operations Revamp            
                       
******/ 
CREATE  PROCEDURE [dbo].[Currency_Unit_Sel_By_Site] ( @Client_Hier_Id INT )
AS 
BEGIN

      DECLARE @CURRENCY_UNIT_ID INT

      SELECT
            @CURRENCY_UNIT_ID = cucm.CURRENCY_UNIT_ID
      FROM
            dbo.CURRENCY_UNIT_COUNTRY_MAP cucm
            INNER JOIN core.Client_Hier ch
                  ON ch.Country_Id = cucm.COUNTRY_ID
                     AND ch.Client_Hier_Id = @Client_Hier_Id
                     

      SELECT
            cu.CURRENCY_UNIT_ID
           ,cu.CURRENCY_UNIT_NAME
           ,CASE WHEN @CURRENCY_UNIT_ID = cu.CURRENCY_UNIT_ID THEN 1
                 ELSE 0
            END AS IS_Default
      FROM
            dbo.CURRENCY_UNIT cu
      ORDER BY
            cu.SORT_ORDER
           ,cu.CURRENCY_UNIT_NAME 


END

;
GO
GRANT EXECUTE ON  [dbo].[Currency_Unit_Sel_By_Site] TO [CBMSApplication]
GO
