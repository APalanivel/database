SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Hedge_Contacts_Sel_By_Supplier

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Vendor_Id		INT     	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC dbo.Hedge_Contacts_Sel_By_Supplier

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RR			2018-07-05	Global Risk Management - Created

******/

CREATE PROCEDURE [dbo].[Hedge_Contacts_Sel_By_Supplier] ( @Vendor_Id INT )
AS 
SET NOCOUNT ON;

SELECT
      ssci.SR_SUPPLIER_CONTACT_INFO_ID
     ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS NAME
     ,ui.FIRST_NAME + ' ' + ui.LAST_NAME + ' / ' + ui.EMAIL_ADDRESS AS Name_Email
FROM
      dbo.USER_INFO ui
      INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
            ON ui.USER_INFO_ID = ssci.USER_INFO_ID
      INNER JOIN dbo.Sr_Supplier_Contact_Type_Map ssctm
            ON ssci.SR_SUPPLIER_CONTACT_INFO_ID = ssctm.SR_SUPPLIER_CONTACT_INFO_ID
      INNER JOIN dbo.Code cd
            ON ssctm.Contact_Type_Cd = cd.Code_Id
      INNER JOIN dbo.Codeset cs
            ON cd.Codeset_Id = cs.Codeset_Id
      INNER JOIN dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP sscvm
            ON ssci.SR_SUPPLIER_CONTACT_INFO_ID = sscvm.SR_SUPPLIER_CONTACT_INFO_ID
WHERE
      sscvm.VENDOR_ID = @Vendor_Id
      AND ui.is_History = 0
      AND sscvm.Is_Primary = 1
      AND cs.Codeset_Name = 'ContactType'
      AND cd.Code_Value = 'Hedge'
ORDER BY
      ui.USERNAME


GO
GRANT EXECUTE ON  [dbo].[Hedge_Contacts_Sel_By_Supplier] TO [CBMSApplication]
GO
