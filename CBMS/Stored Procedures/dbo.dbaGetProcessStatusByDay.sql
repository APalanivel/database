SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--Mark McCallon
--8-1-08
--dbo.dbaGetProcessStatus '1-1-08','1-11-08'
--2688+166= 1-3-08
--837+423+3705= 1-8-08
--dbo.dbaGetProcessStatusByDay '1-1-07','12-31-08'
CREATE procedure dbo.dbaGetProcessStatusByDay
	@startdate smalldatetime = null,
	@enddate smalldatetime = null
as
begin
set nocount on
if @startdate is null
begin
	set @startdate=getdate()-3
	set @enddate=getdate()+2
end

select batch_date
--  , x.end_date
  , sum(x.invoice_count) as invoice_count
  , sum(x.processed_count) as processed
--  , x.invoice_count-x.processed_count as todo
      from (
     select --b.cu_invoice_batch_id
    dbo.justdate(batch_date) as batch_date
   , b.invoice_count
   , count(distinct u.ubm_invoice_id) processed_count
  -- , max(e.event_date) end_date
 --  , count(*)  
       from cu_invoice_batch b with (nolock)
   left outer join ubm_invoice u with (nolock) on u.cu_invoice_batch_id = b.cu_invoice_batch_id
   left outer join cu_invoice cu with (nolock) on cu.ubm_invoice_id = u.ubm_invoice_id
   left outer join cu_invoice_event e with (nolock) on e.cu_invoice_id = cu.cu_invoice_id and e.event_description = 'Invoice Inserted (UBM)'
	and event_by_id=16
      where b.batch_date >= @startdate
	and b.batch_date<= @enddate
        and u.is_processed = 1
   group by dbo.justdate(batch_date)
     , b.invoice_count	
    ) x
 
--    where datepart(hh, x.start_date) = 12
group by batch_date
  order by dbo.justdate(batch_date)
--compute avg(sum(invoice_count))
--16.3
 -- compute sum(datediff(ms, x.start_Date, x.end_Date) / x.processed_count)
--compute avg(x.invoice_count),avg(datediff(ss, x.start_date, x.end_date) / x.processed_count),avg(datediff(ms, x.start_Date, x.end_Date) / x.processed_count)
--,sum(x.invoice_count-x.processed_count)

-- select count(*) from ubm_invoice where cu_invoice_batch_id is null
-- 	      and is_processed = 0
-- 	      and is_quarterly = 0
-- 	      and ubm_client_id is not null
-- 	      and cbms_image_id is not null
--2007-> 2056 invoices not processed
--2008-> 4016 invoices not processed, avg processing time- 3405
--ubm password in entity
-- select * from entity where entity_description like '%ubm%'
-- ubmadmin
-- 5umm1t
--2007
--select * from cu_invoice_batch
--823	2	2770	4144
--delete from cu_invoice_batch where cu_invoice_batch_id=1856
--select * from ubm_invoice where cu_invoice_batch_id=1856
--select max(cu_invoice_batch_id) from ubm_invoice
-- select * from cu_invoice_batch where cu_invoice_batch_id=1856
-- dele
--2008
--1245	2	3405	4016

-- select * from app_menu where display_text like '%process%'
-- update app_menu set target_action='/ip2/default.aspx' where app_menu_id=239
-- 
-- /assets/temp/scotttest.aspx
end
GO
GRANT EXECUTE ON  [dbo].[dbaGetProcessStatusByDay] TO [CBMSApplication]
GO
