SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:  dbo.Price_Index_Price_Point_Short_Name_Exist      
              
Description:              
			This sproc checks if the Price_Point_Short_Name is already present for the Given Index Id.      
            If already exists then return 1 else 0.                
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
	@Price_Point_Short_Name				NVARCHAR(255)
	
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

 exec   Price_Index_Price_Point_Short_Name_Exist 'test1234'
    
	
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV        Ravi Kumar Vegesna 
	             
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
   RKV				2016-02-10		created as Part of AS400-PII 
                
******/ 
CREATE PROCEDURE [dbo].[Price_Index_Price_Point_Short_Name_Exist]
      ( 
       @Price_Point_Short_Name NVARCHAR(255)
       )
AS 
BEGIN
      SET NOCOUNT ON 
      DECLARE @Is_Price_Point_Short_Name_Exist INT = 0
      
      SELECT
            @Is_Price_Point_Short_Name_Exist = 1
      FROM
            dbo.PRICE_INDEX pi
      WHERE
            pi.Price_Point_Short_Name = @Price_Point_Short_Name
           
            
            
      SELECT
            @Is_Price_Point_Short_Name_Exist Price_Point_Short_Name_Exist
                 
END;







;
GO
GRANT EXECUTE ON  [dbo].[Price_Index_Price_Point_Short_Name_Exist] TO [CBMSApplication]
GO
