SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/**
NAME:    
   
	dbo.Client_Country_Sel_By_Client
   
 DESCRIPTION:     
   
	This procedure is to get only contries that client sites resides
   
 INPUT PARAMETERS:    
	Name				DataType		 Default		 Description    
----------------------------------------------------------------------      
	@Client_Id	INT
   
 OUTPUT PARAMETERS:    
 Name				DataType		 Default		 Description    
----------------------------------------------------------------------

  
  USAGE EXAMPLES:    
------------------------------------------------------------  

  
	EXEC dbo.Client_Country_Sel_By_Client 235
	   
	
  
AUTHOR INITIALS:    
	Initials	Name    
------------------------------------------------------------    
	RR			Raghu Reddy
    
 MODIFICATIONS     
	Initials	Date		Modification    
------------------------------------------------------------    
	RR			01-08-2018	Created For Global Risk Managemnet		


******/

CREATE PROCEDURE [dbo].[Client_Country_Sel_By_Client] ( @Client_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;

      SELECT
            CH.Country_Id
           ,CH.Country_Name
      FROM
            Core.Client_Hier CH
      WHERE
            CH.Client_Id = @Client_Id
            AND CH.Site_Id > 0
            AND CH.Country_Name IN ( 'USA', 'Canada', 'Mexico' )
      GROUP BY
            CH.Country_Id
           ,CH.Country_Name
      ORDER BY
            CH.Country_Name;

END;

GO
GRANT EXECUTE ON  [dbo].[Client_Country_Sel_By_Client] TO [CBMSApplication]
GO
