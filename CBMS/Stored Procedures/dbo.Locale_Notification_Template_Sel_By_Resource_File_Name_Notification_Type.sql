SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 dbo.Locale_Notification_Template_Sel_By_Resource_File_Name_Notification_Type    
 
DESCRIPTION:  

INPUT PARAMETERS:    
Name						DataType				Default			Description    
----------------------------------------------------------------------------------------------------------    
@Resource_File_Name			NVARCHAR(200)
@Notification_Type			VARCHAR(100)
@Language_Locale_Cd			INT						 NULL 
OUTPUT PARAMETERS:    


Name					DataType	Default			Description    
----------------------------------------------------------------    
 
USAGE EXAMPLES:    
----------------------------------------------------------------    
    
     
EXEC dbo.Locale_Notification_Template_Sel_By_Resource_File_Name_Notification_Type 'CBMSICEmailChaseNotification','IC Email Chase',100157



AUTHOR INITIALS:    
Initials	Name    
----------------------------------------------------------------    
RKV			Ravi Kumar Vegesna

MODIFICATIONS:
Initials	Date		Modification    
----------------------------------------------------------------    
RKV			2017-01-24	Created For Invoice Collection
						
    
******/    
    
CREATE PROCEDURE [dbo].[Locale_Notification_Template_Sel_By_Resource_File_Name_Notification_Type]
      ( 
       @Resource_File_Name NVARCHAR(200)
      ,@Notification_Type VARCHAR(100)
      ,@Language_Locale_Cd INT = NULL )
AS 
BEGIN    
    
      SET NOCOUNT ON; 
      
      DECLARE
            @Locale_Email_Subject NVARCHAR(MAX)
           ,@Locale_Email_Body NVARCHAR(MAX)
           ,@Is_Language_available BIT = 0;
      
      
       
      SELECT
            @Is_Language_available = 1
      FROM
            Resource_CS.dbo.Resource_File rf
            INNER JOIN Resource_CS.dbo.Resource_File_Key rfk
                  ON rf.Resource_File_Id = rfk.Resource_File_Id
            INNER JOIN Resource_CS.dbo.Resource_Key rk
                  ON rfk.Resource_Key_Id = rk.Resource_Key_Id
            INNER JOIN Resource_CS.dbo.Resource_Key_Locale_Value lv
                  ON rfk.Resource_File_Key_Id = lv.Resource_File_Key_Id
            INNER JOIN dbo.Code cd
                  ON lv.Language_Cd = cd.Code_Id
      WHERE
            rf.Resource_File_Name = @Resource_File_Name
            AND rk.Resource_Key = @Resource_File_Name + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@Notification_Type, SPACE(1), SPACE(0)), '-', SPACE(0)), ')', SPACE(0)), '(', SPACE(0)), '_', SPACE(0)) + 'Subject'
            AND lv.Language_Cd = @Language_Locale_Cd
      
      
      
      SELECT
            @Language_Locale_Cd = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            ( @Language_Locale_Cd IS NULL
              OR @Is_Language_available = 0 )
            AND cs.Codeset_Name = 'LocalizationLanguage'
            AND c.Code_Value = 'en-US';
      
      
      
      
      WITH  Cte_Subject
              AS ( SELECT
                        lv.Language_Cd
                       ,lv.Locale_Value
                   FROM
                        Resource_CS.dbo.Resource_File rf
                        INNER JOIN Resource_CS.dbo.Resource_File_Key rfk
                              ON rf.Resource_File_Id = rfk.Resource_File_Id
                        INNER JOIN Resource_CS.dbo.Resource_Key rk
                              ON rfk.Resource_Key_Id = rk.Resource_Key_Id
                        INNER JOIN Resource_CS.dbo.Resource_Key_Locale_Value lv
                              ON rfk.Resource_File_Key_Id = lv.Resource_File_Key_Id
                        INNER JOIN dbo.Code cd
                              ON lv.Language_Cd = cd.Code_Id
                   WHERE
                        rf.Resource_File_Name = @Resource_File_Name
                        AND rk.Resource_Key = @Resource_File_Name + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@Notification_Type, SPACE(1), SPACE(0)), '-', SPACE(0)), ')', SPACE(0)), '(', SPACE(0)), '_', SPACE(0)) + 'Subject'
                        AND lv.Language_Cd = @Language_Locale_Cd),
            Cte_Body
              AS ( SELECT
                        lv.Language_Cd
                       ,lv.Locale_Value
                   FROM
                        Resource_CS.dbo.Resource_File rf
                        INNER JOIN Resource_CS.dbo.Resource_File_Key rfk
                              ON rf.Resource_File_Id = rfk.Resource_File_Id
                        INNER JOIN Resource_CS.dbo.Resource_Key rk
                              ON rfk.Resource_Key_Id = rk.Resource_Key_Id
                        INNER JOIN Resource_CS.dbo.Resource_Key_Locale_Value lv
                              ON rfk.Resource_File_Key_Id = lv.Resource_File_Key_Id
                        INNER JOIN dbo.Code cd
                              ON lv.Language_Cd = cd.Code_Id
                   WHERE
                        rf.Resource_File_Name = @Resource_File_Name
                        AND rk.Resource_Key = @Resource_File_Name + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@Notification_Type, SPACE(1), SPACE(0)), '-', SPACE(0)), ')', SPACE(0)), '(', SPACE(0)), '_', SPACE(0)) + 'Body'
                        AND lv.Language_Cd = @Language_Locale_Cd)
            SELECT
                  cd.Code_Id AS Locale_Cd
                 ,CD.Code_Value AS Locale
                 ,ctes.Locale_Value AS Locale_Email_Subject
                 ,cteb.Locale_Value AS Locale_Email_Body
            FROM
                  dbo.Code cd
                  INNER JOIN dbo.Codeset cs
                        ON cd.Codeset_Id = cs.Codeset_Id
                  LEFT JOIN Cte_Subject ctes
                        ON ctes.Language_Cd = cd.Code_Id
                  LEFT JOIN Cte_Body cteb
                        ON cteb.Language_Cd = cd.Code_Id
            WHERE
                  cs.Codeset_Name = 'LocalizationLanguage'
                  AND ( @Language_Locale_Cd IS NULL
                        OR cd.Code_Id = @Language_Locale_Cd )
      
      
        

END;

;
;

;
GO
GRANT EXECUTE ON  [dbo].[Locale_Notification_Template_Sel_By_Resource_File_Name_Notification_Type] TO [CBMSApplication]
GO
