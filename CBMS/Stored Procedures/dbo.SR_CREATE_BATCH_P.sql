SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.SR_CREATE_BATCH_P
	@master_batch_log_id int,
	@batch_category_type varchar(150),
	@batch_duration_type varchar(150),
	@batch_log_id int out
	AS
	set nocount on	
	declare @batch_category_type_id int , @batch_duration_type_id int

	select @batch_category_type_id = entity_id from entity(nolock) where entity_type = 1001 and entity_name = @batch_category_type
	select @batch_duration_type_id = entity_id from entity(nolock) where entity_type = 1047 and entity_name = @batch_duration_type

	insert into sr_batch_log (sr_batch_master_log_id, batch_category_type_id, batch_duration_type_id)
	values(	@master_batch_log_id, @batch_category_type_id, @batch_duration_type_id)	
	set @batch_log_id = @@identity
	return
GO
GRANT EXECUTE ON  [dbo].[SR_CREATE_BATCH_P] TO [CBMSApplication]
GO
