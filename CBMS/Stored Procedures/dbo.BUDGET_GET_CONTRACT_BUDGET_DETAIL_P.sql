SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO












-- exec BUDGET_GET_CONTRACT_BUDGET_DETAIL_P '','', 29719
CREATE    PROCEDURE dbo.BUDGET_GET_CONTRACT_BUDGET_DETAIL_P
	@user_id varchar(10),
	@session_id varchar(10),
	@contract_id int
	AS
	begin
		set nocount on
		
		declare @volume_unit_id int
		if(select commodity_type_id from contract where contract_id = @contract_id) = 291
		begin
			set @volume_unit_id = 25
		end else
		begin
			set @volume_unit_id = 12
		end
		
		select	months.month_identifier,
			detail.budget_contract_budget_detail_id,
			detail.budget_contract_budget_month_id,
			detail.volume as percetange,
			detail.market_id,
			index_detail.index_detail_value as index_value,
			detail.fuel,
			detail.multiplier,
			detail.adder,
			detail.tax,
			detail.volume_unit_type_id,
			detail.currency_unit_id,
			curr_conv.conversion_factor as curr_conversion_factor,
			conv.conversion_factor,
			detail.is_nymex_forecast
		
		from	budget_contract_budget contract_budget
			join budget_contract_budget_months months on months.budget_contract_budget_id = contract_budget.budget_contract_budget_id
			and contract_budget.contract_id = @contract_id
			join budget_contract_budget_detail detail on detail.budget_contract_budget_month_id = months.budget_contract_budget_month_id
			left join clearport_index_months index_months on index_months.clearport_index_id = detail.market_id
			and index_months.clearport_index_month = months.month_identifier
			left join clearport_index_detail index_detail on index_detail.clearport_index_month_id = index_months.clearport_index_month_id
			and index_detail.index_detail_date = (select max(index_detail_date) from clearport_index_detail where clearport_index_month_id = index_detail.clearport_index_month_id),
			consumption_unit_conversion conv,
			currency_unit_conversion curr_conv

		where 	conv.base_unit_id = detail.volume_unit_type_id
			and conv.converted_unit_id = @volume_unit_id --// MMBtu or kWh
			and curr_conv.base_unit_id = detail.currency_unit_id
			and curr_conv.converted_unit_id = 3 --// USD 
			and curr_conv.conversion_date = (select max(conversion_date) from currency_unit_conversion where base_unit_id = detail.currency_unit_id and converted_unit_id = 3 and currency_group_id = 3)
			and curr_conv.currency_group_id = 3
		group by months.month_identifier,
			detail.budget_contract_budget_detail_id,
			detail.budget_contract_budget_month_id,
			detail.volume,
			detail.market_id,
			index_detail.index_detail_value,
			detail.fuel,
			detail.multiplier,
			detail.adder,
			detail.tax,
			detail.volume_unit_type_id,
			detail.currency_unit_id,
			curr_conv.conversion_factor,
			conv.conversion_factor,
			detail.is_nymex_forecast




		order by months.month_identifier,detail.budget_contract_budget_detail_id
	end














GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_CONTRACT_BUDGET_DETAIL_P] TO [CBMSApplication]
GO
