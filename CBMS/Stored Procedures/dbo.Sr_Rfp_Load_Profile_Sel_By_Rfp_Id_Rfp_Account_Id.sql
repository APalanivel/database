SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******               
                           
 NAME:  dbo.Sr_Rfp_Load_Profile_Sel_By_Rfp_Id_Rfp_Account_Id                  
                              
 DESCRIPTION:                              
                               
                              
 INPUT PARAMETERS:              
                             
 Name					DataType			Default			Description              
---------------------------------------------------------------------
 @SR_RFP_ID				INT
 @SR_RFP_ACCOUNT_ID		INT
 @SR_RFP_BID_GROUP_ID	INT
                                    
 OUTPUT PARAMETERS:                   
                              
 Name				DataType			Default			Description              
---------------------------------------------------------------------
                              
 USAGE EXAMPLES:              
---------------------------------------------------------------------
	EXEC dbo.Sr_Rfp_Load_Profile_Sel_By_Rfp_Id_Rfp_Account_Id 13137,NULL,10011900
	EXEC dbo.Sr_Rfp_Load_Profile_Sel_By_Rfp_Id_Rfp_Account_Id 13137,NULL,NULL
   
	EXEC dbo.Sr_Rfp_Load_Profile_Sel_By_Rfp_Id_Rfp_Account_Id 13569
	EXEC dbo.Sr_Rfp_Load_Profile_Sel_By_Rfp_Id_Rfp_Account_Id 13570
	
	EXEC dbo.Sr_Rfp_Load_Profile_Sel_By_Rfp_Id_Rfp_Account_Id 2331

	EXEC Sr_Rfp_Load_Profile_Sel_By_Rfp_Id_Rfp_Account_Id 13733, null, '10012607' 
	
	EXEC Sr_Rfp_Load_Profile_Sel_By_Rfp_Id_Rfp_Account_Id 13733, null, '10012607' 
   
  
  
 AUTHOR INITIALS:              
             
 Initials			Name              
---------------------------------------------------------------------
 RKV				Ravi Kumar Raju   
 NR					Narayana Reddy                      
                               
 MODIFICATIONS:            
             
 Initials   Date        Modification            
---------------------------------------------------------------------
 RKV        2016-06-09  Created for GCS Phase - 5.
 RR			2016-07-27	GCS-1301 Only actual values should be considered for volume
 NR			2016-11-04	MAINT-4544 [City/State] out put column now we changed to split this as City, State. 
 RKV		2016-11-30  MAINT-4626 Removing the Sr_Rfp_Accounts if it is having cbms image id.
 NR			2017-02-27	MAINT-4841 Showing Max values for demand dth uom. 
 NR			2017-03-09	Hotfix -  Showing Max values for demand Determinant .  
						PRSUPPORT-455 - Chnaged the datatype int decimal. 
 NR			2017-12-22	SE2017-434 - Added Contracting_Entity	filed in output list.	               
                   
  
                             
******/ 
CREATE   PROCEDURE [dbo].[Sr_Rfp_Load_Profile_Sel_By_Rfp_Id_Rfp_Account_Id]
      ( 
       @SR_RFP_ID INT
      ,@SR_RFP_ACCOUNT_ID VARCHAR(MAX) = NULL
      ,@SR_RFP_BID_GROUP_ID VARCHAR(MAX) = NULL )
AS 
BEGIN 
 
      SET NOCOUNT ON;    
 
     
 
      DECLARE
            @pivot_String VARCHAR(MAX)
           ,@SQL_STR NVARCHAR(MAX)
           ,@pivot_String1 VARCHAR(MAX)
           ,@res_pivot_String1 VARCHAR(MAX)
           ,@res_pivot_String VARCHAR(MAX)
           ,@Min_Date DATE
           ,@Max_Date DATE;
    
      CREATE TABLE #Contract_Volume
            ( 
             SR_RFP_ACCOUNT_ID INT
            ,Determinant_Name VARCHAR(200)
            ,Load_Profile_Volume DECIMAL(32, 16)
            ,Load_Profile_Volume_uom VARCHAR(200) );
 
      CREATE TABLE #Contract_Longest_Term_Dates
            ( 
             SR_RFP_ACCOUNT_ID INT
            ,Start_dt DATE
            ,End_Dt DATE );
 
 
      CREATE TABLE #Determinant_Values
            ( 
             SR_RFP_ACCOUNT_ID INT
            ,Determinant_Name VARCHAR(100)
            ,LP_Value DECIMAL(32, 16)
            ,MONTH_IDENTIFIER DATE
            ,DETERMINANT_UNIT_TYPE_ID INT );
  
  
      CREATE TABLE #Client_Hier_Rfp_Data
            ( 
             Client_Id INT
            ,Client_Name VARCHAR(200)
            ,Site_Id INT
            ,Site_Name VARCHAR(800)
            ,Account_Id INT
            ,Account_Number VARCHAR(50)
            ,Alternate_Account_Number NVARCHAR(200)
            ,Meter_Number VARCHAR(50)
            ,SR_RFP_ACCOUNT_ID INT
            ,SR_RFP_BID_GROUP_ID INT
            ,[ADDRESS] VARCHAR(450)
            ,City_Name VARCHAR(200)
            ,State_Id INT
            ,State_Name VARCHAR(200)
            ,Country_Id INT
            ,Country_Name VARCHAR(200)
            ,Is_Bid_Group BIT
            ,Contracting_Entity VARCHAR(200) );

      CREATE TABLE #SR_RFP_Account_Id
            ( 
             SR_RFP_Account_Id INT );




      INSERT      INTO #SR_RFP_Account_Id
                  ( 
                   SR_RFP_Account_Id )
                  SELECT
                        sra.SR_RFP_ACCOUNT_ID
                  FROM
                        dbo.SR_RFP_ACCOUNT sra
                  WHERE
                        sra.SR_RFP_ID = @SR_RFP_ID
                        AND @SR_RFP_ACCOUNT_ID IS NULL
                        AND @SR_RFP_BID_GROUP_ID IS NULL; 


      INSERT      INTO #SR_RFP_Account_Id
                  ( 
                   SR_RFP_Account_Id )
                  SELECT
                        sra.Segments
                  FROM
                        dbo.ufn_split(@SR_RFP_ACCOUNT_ID, ',') sra
                  WHERE
                        @SR_RFP_ACCOUNT_ID IS NOT NULL;     
           

      INSERT      INTO #SR_RFP_Account_Id
                  ( 
                   SR_RFP_Account_Id )
                  SELECT
                        SR_RFP_ACCOUNT_ID
                  FROM
                        dbo.SR_RFP_ACCOUNT sr
                  WHERE
                        EXISTS ( SELECT
                                    sra.Segments
                                 FROM
                                    dbo.ufn_split(@SR_RFP_BID_GROUP_ID, ',') sra
                                 WHERE
                                    sr.SR_RFP_BID_GROUP_ID = sra.Segments )
                        AND @SR_RFP_BID_GROUP_ID IS NOT NULL; 



      DELETE
            srai
      FROM
            #SR_RFP_Account_Id srai
            INNER JOIN dbo.SR_RFP_ACCOUNT rfpacc
                  ON rfpacc.SR_RFP_ACCOUNT_ID = srai.SR_RFP_Account_Id
            INNER JOIN dbo.SR_RFP_LP_CLIENT_APPROVAL srlca
                  ON srlca.SR_RFP_ID = rfpacc.SR_RFP_ID
                     AND srlca.CBMS_IMAGE_ID IS NOT NULL
      WHERE
            EXISTS ( SELECT
                        1
                     FROM
                        core.Client_Hier_Account cha
                        INNER JOIN Core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                     WHERE
                        srlca.SITE_ID = ch.Site_Id
                        AND rfpacc.ACCOUNT_ID = cha.Account_Id )
                  
                  

      INSERT      INTO #Client_Hier_Rfp_Data
                  ( 
                   Client_Id
                  ,Client_Name
                  ,Site_Id
                  ,Site_Name
                  ,Account_Id
                  ,Account_Number
                  ,Alternate_Account_Number
                  ,Meter_Number
                  ,SR_RFP_ACCOUNT_ID
                  ,SR_RFP_BID_GROUP_ID
                  ,[ADDRESS]
                  ,City_Name
                  ,STATE_ID
                  ,State_Name
                  ,Country_Id
                  ,Country_Name
                  ,Is_Bid_Group
                  ,Contracting_Entity )
                  SELECT
                        ch.Client_Id
                       ,ch.Client_Name
                       ,ch.Site_Id
                       ,ch.Site_name
                       ,cha.Account_Id
                       ,cha.Account_Number
                       ,cha.Alternate_Account_Number
                       ,cha.Meter_Number
                       ,rfpacc.SR_RFP_ACCOUNT_ID
                       ,rfpacc.SR_RFP_BID_GROUP_ID
                       ,LEFT(a.ADDRESS_LINE1 + ', ' + a.ADDRESS_LINE2, LEN(a.ADDRESS_LINE1 + ', ' + a.ADDRESS_LINE2) - 1) AS ADDRESS
                       ,a.CITY
                       ,a.STATE_ID
                       ,s.STATE_NAME
                       ,c.COUNTRY_ID
                       ,c.COUNTRY_NAME
                       ,CASE WHEN rfpacc.SR_RFP_BID_GROUP_ID IS NOT NULL THEN 1
                             ELSE 0
                        END
                       ,st.CONTRACTING_ENTITY
                  FROM
                        #SR_RFP_Account_Id srai
                        INNER JOIN dbo.SR_RFP_ACCOUNT rfpacc
                              ON srai.SR_RFP_Account_Id = rfpacc.SR_RFP_ACCOUNT_ID
                        INNER  JOIN dbo.SR_RFP_ACCOUNT_METER_MAP sramm
                              ON rfpacc.SR_RFP_ACCOUNT_ID = sramm.SR_RFP_ACCOUNT_ID
                        INNER JOIN Core.Client_Hier_Account cha
                              ON rfpacc.ACCOUNT_ID = cha.Account_Id
                                 AND sramm.METER_ID = cha.Meter_Id
                        INNER JOIN Core.Client_Hier ch
                              ON ch.Client_Hier_Id = cha.Client_Hier_Id
                        INNER JOIN dbo.METER mtr
                              ON cha.Meter_Id = mtr.METER_ID
                        LEFT JOIN dbo.Code mtrtyp
                              ON cha.Meter_Type_Cd = mtrtyp.Code_Id
                        INNER JOIN dbo.SITE st
                              ON ch.Site_Id = st.SITE_ID
                        INNER JOIN dbo.ADDRESS a
                              ON st.PRIMARY_ADDRESS_ID = a.ADDRESS_ID
                        INNER JOIN dbo.STATE s
                              ON a.STATE_ID = s.STATE_ID
                        INNER JOIN dbo.COUNTRY c
                              ON s.COUNTRY_ID = c.COUNTRY_ID
                        INNER JOIN dbo.Division_Dtl dd
                              ON dd.SiteGroup_Id = st.DIVISION_ID
                        LEFT JOIN dbo.ENTITY tes
                              ON mtr.TAX_EXEMPTION_ID = tes.ENTITY_ID
                        LEFT OUTER JOIN dbo.SR_RFP_BID_GROUP srbg
                              ON rfpacc.SR_RFP_BID_GROUP_ID = srbg.SR_RFP_BID_GROUP_ID
                        LEFT OUTER JOIN Code mc
                              ON mc.Code_Id = cha.Meter_Type_Cd
                  WHERE
                        rfpacc.SR_RFP_ID = @SR_RFP_ID
                  GROUP BY
                        ch.Client_Id
                       ,ch.Client_Name
                       ,ch.Site_Id
                       ,ch.Site_name
                       ,cha.Account_Id
                       ,cha.Account_Number
                       ,cha.Alternate_Account_Number
                       ,cha.Meter_Number
                       ,rfpacc.SR_RFP_ACCOUNT_ID
                       ,rfpacc.SR_RFP_BID_GROUP_ID
                       ,a.ADDRESS_LINE1
                       ,a.ADDRESS_LINE2
                       ,a.CITY
                       ,a.STATE_ID
                       ,s.STATE_NAME
                       ,c.COUNTRY_ID
                       ,c.COUNTRY_NAME
                       ,rfpacc.SR_RFP_BID_GROUP_ID
                       ,st.CONTRACTING_ENTITY;
                 
  
      INSERT      INTO #Contract_Longest_Term_Dates
                  ( 
                   SR_RFP_ACCOUNT_ID
                  ,Start_dt
                  ,End_Dt )
                  SELECT
                        chrd.SR_RFP_ACCOUNT_ID
                       ,x.start_month
                       ,y.To_month
                  FROM
                        #Client_Hier_Rfp_Data chrd
                        INNER JOIN ( SELECT
                                          cd.SR_RFP_ACCOUNT_ID
                                         ,MIN(srat.FROM_MONTH) start_month
                                     FROM
                                          dbo.SR_RFP_TERM srt
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM srat
                                                ON srt.SR_RFP_TERM_ID = srat.SR_RFP_TERM_ID
                                          INNER JOIN dbo.#Client_Hier_Rfp_Data cd
                                                ON cd.SR_RFP_ACCOUNT_ID = srat.SR_ACCOUNT_GROUP_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON srt.SR_RFP_ID = rfp.SR_RFP_ID
                                     GROUP BY
                                          cd.SR_RFP_ACCOUNT_ID ) x
                              ON chrd.SR_RFP_ACCOUNT_ID = x.SR_RFP_ACCOUNT_ID
                        INNER JOIN ( SELECT
                                          cd.SR_RFP_ACCOUNT_ID
                                         ,MAX(srat.TO_MONTH) To_month
                                     FROM
                                          dbo.SR_RFP_TERM srt
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM srat
                                                ON srt.SR_RFP_TERM_ID = srat.SR_RFP_TERM_ID
                                          INNER JOIN dbo.#Client_Hier_Rfp_Data cd
                                                ON cd.SR_RFP_ACCOUNT_ID = srat.SR_ACCOUNT_GROUP_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON srt.SR_RFP_ID = rfp.SR_RFP_ID
                                     GROUP BY
                                          cd.SR_RFP_ACCOUNT_ID ) y
                              ON x.SR_RFP_ACCOUNT_ID = y.SR_RFP_ACCOUNT_ID
                  WHERE
                        chrd.Is_Bid_Group = 0
                  GROUP BY
                        chrd.SR_RFP_ACCOUNT_ID
                       ,x.start_month
                       ,y.To_month;




      INSERT      INTO #Contract_Longest_Term_Dates
                  ( 
                   SR_RFP_ACCOUNT_ID
                  ,Start_dt
                  ,End_Dt )
                  SELECT
                        chrd.SR_RFP_ACCOUNT_ID
                       ,x.start_month
                       ,y.To_month
                  FROM
                        #Client_Hier_Rfp_Data chrd
                        INNER JOIN ( SELECT
                                          cd.SR_RFP_ACCOUNT_ID
                                         ,MIN(srat.FROM_MONTH) start_month
                                     FROM
                                          dbo.SR_RFP_TERM srt
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM srat
                                                ON srt.SR_RFP_TERM_ID = srat.SR_RFP_TERM_ID
                                          INNER JOIN dbo.#Client_Hier_Rfp_Data cd
                                                ON cd.SR_RFP_BID_GROUP_ID = srat.SR_ACCOUNT_GROUP_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON srt.SR_RFP_ID = rfp.SR_RFP_ID
                                     GROUP BY
                                          cd.SR_RFP_ACCOUNT_ID ) x
                              ON chrd.SR_RFP_ACCOUNT_ID = x.SR_RFP_ACCOUNT_ID
                        INNER JOIN ( SELECT
                                          cd.SR_RFP_ACCOUNT_ID
                                         ,MAX(srat.TO_MONTH) To_month
                                     FROM
                                          dbo.SR_RFP_TERM srt
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM srat
                                                ON srt.SR_RFP_TERM_ID = srat.SR_RFP_TERM_ID
                                          INNER JOIN dbo.#Client_Hier_Rfp_Data cd
                                                ON cd.SR_RFP_BID_GROUP_ID = srat.SR_ACCOUNT_GROUP_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON srt.SR_RFP_ID = rfp.SR_RFP_ID
                                     GROUP BY
                                          cd.SR_RFP_ACCOUNT_ID ) y
                              ON x.SR_RFP_ACCOUNT_ID = y.SR_RFP_ACCOUNT_ID
                  WHERE
                        chrd.Is_Bid_Group = 1
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          #Contract_Longest_Term_Dates cltd
                                         WHERE
                                          cltd.SR_RFP_ACCOUNT_ID = chrd.SR_RFP_ACCOUNT_ID )
                  GROUP BY
                        chrd.SR_RFP_ACCOUNT_ID
                       ,x.start_month
                       ,y.To_month;
          




     
      SELECT
            @Min_Date = MIN(Start_dt)
      FROM
            #Contract_Longest_Term_Dates;
     
      SELECT
            @Max_Date = MAX(End_Dt)
      FROM
            #Contract_Longest_Term_Dates;   
            
            
      INSERT      INTO #Determinant_Values
                  ( 
                   SR_RFP_ACCOUNT_ID
                  ,Determinant_Name
                  ,LP_Value
                  ,MONTH_IDENTIFIER
                  ,DETERMINANT_UNIT_TYPE_ID )
                  SELECT
                        cd.SR_RFP_Account_Id
                       ,srlpd.DETERMINANT_NAME
                       ,-1
                       ,dd.DATE_D
                       ,NULL
                  FROM
                        dbo.SR_RFP_LOAD_PROFILE_SETUP srlps
                        INNER JOIN dbo.SR_RFP_LOAD_PROFILE_DETERMINANT srlpd
                              ON srlps.SR_RFP_LOAD_PROFILE_SETUP_ID = srlpd.SR_RFP_LOAD_PROFILE_SETUP_ID
                        INNER JOIN dbo.SR_RFP_LP_DETERMINANT_VALUES srldv
                              ON srlpd.SR_RFP_LOAD_PROFILE_DETERMINANT_ID = srldv.SR_RFP_LOAD_PROFILE_DETERMINANT_ID
                        INNER JOIN #SR_RFP_Account_Id cd
                              ON srlps.SR_RFP_ACCOUNT_ID = cd.SR_RFP_Account_Id
                        INNER JOIN #Contract_Longest_Term_Dates cldt
                              ON cldt.SR_RFP_ACCOUNT_ID = cd.SR_RFP_Account_Id
                        CROSS JOIN meta.DATE_DIM dd
                  WHERE
                        dd.DATE_D BETWEEN DATEADD(dd, -DATEPART(dd, @Min_Date) + 1, @Min_Date)
                                  AND     DATEADD(dd, -DATEPART(dd, @Max_Date) + 1, @Max_Date)
                        AND DATEADD(dd, -DATEPART(dd, cldt.Start_dt) + 1, cldt.Start_dt) > dd.DATE_D
                        AND srlpd.IS_CHECKED = 1
                  GROUP BY
                        srlpd.DETERMINANT_NAME
                       ,cd.SR_RFP_Account_Id
                       ,dd.DATE_D;


      INSERT      INTO #Determinant_Values
                  ( 
                   SR_RFP_ACCOUNT_ID
                  ,Determinant_Name
                  ,LP_Value
                  ,MONTH_IDENTIFIER
                  ,DETERMINANT_UNIT_TYPE_ID )
                  SELECT
                        cd.SR_RFP_Account_Id
                       ,srlpd.DETERMINANT_NAME
                       ,-1
                       ,dd.DATE_D
                       ,NULL
                  FROM
                        dbo.SR_RFP_LOAD_PROFILE_SETUP srlps
                        INNER JOIN dbo.SR_RFP_LOAD_PROFILE_DETERMINANT srlpd
                              ON srlps.SR_RFP_LOAD_PROFILE_SETUP_ID = srlpd.SR_RFP_LOAD_PROFILE_SETUP_ID
                        INNER JOIN dbo.SR_RFP_LP_DETERMINANT_VALUES srldv
                              ON srlpd.SR_RFP_LOAD_PROFILE_DETERMINANT_ID = srldv.SR_RFP_LOAD_PROFILE_DETERMINANT_ID
                        INNER JOIN #SR_RFP_Account_Id cd
                              ON srlps.SR_RFP_ACCOUNT_ID = cd.SR_RFP_Account_Id
                        INNER JOIN #Contract_Longest_Term_Dates cldt
                              ON cldt.SR_RFP_ACCOUNT_ID = cd.SR_RFP_Account_Id
                        CROSS JOIN meta.DATE_DIM dd
                  WHERE
                        dd.DATE_D BETWEEN DATEADD(dd, -DATEPART(dd, @Min_Date) + 1, @Min_Date)
                                  AND     DATEADD(dd, -DATEPART(dd, @Max_Date) + 1, @Max_Date)
                        AND cldt.End_Dt < dd.DATE_D
                        AND srlpd.IS_CHECKED = 1
                  GROUP BY
                        srlpd.DETERMINANT_NAME
                       ,cd.SR_RFP_Account_Id
                       ,dd.DATE_D;


      INSERT      INTO #Determinant_Values
                  ( 
                   SR_RFP_ACCOUNT_ID
                  ,Determinant_Name
                  ,LP_Value
                  ,MONTH_IDENTIFIER
                  ,DETERMINANT_UNIT_TYPE_ID )
                  SELECT
                        srai.SR_RFP_Account_Id
                       ,srlpd.DETERMINANT_NAME
                       ,MIN(srldv.LP_VALUE)
                       ,srldv.MONTH_IDENTIFIER
                       ,srlpd.DETERMINANT_UNIT_TYPE_ID
                  FROM
                        dbo.SR_RFP_LOAD_PROFILE_SETUP srlps
                        INNER JOIN dbo.SR_RFP_LOAD_PROFILE_DETERMINANT srlpd
                              ON srlps.SR_RFP_LOAD_PROFILE_SETUP_ID = srlpd.SR_RFP_LOAD_PROFILE_SETUP_ID
                        INNER JOIN dbo.SR_RFP_LP_DETERMINANT_VALUES srldv
                              ON srlpd.SR_RFP_LOAD_PROFILE_DETERMINANT_ID = srldv.SR_RFP_LOAD_PROFILE_DETERMINANT_ID
                        INNER JOIN #SR_RFP_Account_Id srai
                              ON srlps.SR_RFP_ACCOUNT_ID = srai.SR_RFP_Account_Id
                        INNER JOIN dbo.ENTITY en
                              ON srldv.READING_TYPE_ID = en.ENTITY_ID
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          #Determinant_Values dv
                                     WHERE
                                          srlpd.DETERMINANT_NAME = dv.Determinant_Name
                                          AND srldv.MONTH_IDENTIFIER = dv.MONTH_IDENTIFIER
                                          AND srai.SR_RFP_Account_Id = dv.SR_RFP_ACCOUNT_ID )
                        AND srlpd.IS_CHECKED = 1
                        AND en.ENTITY_TYPE = 1031
                        AND ( en.ENTITY_NAME = 'Actual LP Value'
                              OR en.ENTITY_NAME = 'Actual Avg LP Value' )
                        AND srldv.LP_VALUE IS NOT NULL
                  GROUP BY
                        srai.SR_RFP_Account_Id
                       ,srlpd.DETERMINANT_NAME
                       ,srldv.LP_VALUE
                       ,srldv.MONTH_IDENTIFIER
                       ,srlpd.DETERMINANT_UNIT_TYPE_ID;
                       
      
      INSERT      INTO #Contract_Volume
                  ( 
                   SR_RFP_ACCOUNT_ID
                  ,Determinant_Name
                  ,Load_Profile_Volume
                  ,Load_Profile_Volume_uom )
                  SELECT
                        dv.SR_RFP_ACCOUNT_ID
                       ,dv.Determinant_Name
                       ,CASE WHEN e.ENTITY_NAME = 'Kw'
                                  OR dv.Determinant_Name = 'Demand' THEN MAX(dv.LP_Value)
                             ELSE SUM(dv.LP_Value)
                        END
                       ,e.ENTITY_NAME Load_Profile_Volume_uom
                  FROM
                        #Determinant_Values dv
                        INNER JOIN dbo.ENTITY e
                              ON e.ENTITY_ID = dv.DETERMINANT_UNIT_TYPE_ID
                  GROUP BY
                        dv.SR_RFP_ACCOUNT_ID
                       ,dv.Determinant_Name
                       ,e.ENTITY_NAME;
                      
 
 
      SELECT
 DISTINCT
            @pivot_String = STUFF(( SELECT
                                          ',[' + LEFT(CONVERT(VARCHAR(12), dd.DATE_D, 109), 3) + '-' + RIGHT(CONVERT(VARCHAR(12), dd.DATE_D, 109), 2) + ']'
                                    FROM
                                          meta.DATE_DIM dd
                                    WHERE
                                          dd.DATE_D BETWEEN DATEADD(dd, -DATEPART(dd, @Min_Date) + 1, @Min_Date)
                                                    AND     DATEADD(dd, -DATEPART(dd, @Max_Date) + 1, @Max_Date)
                                  FOR
                                    XML PATH('') ), 1, 1, ''); 
               
                               
  
 
      
      
      SELECT
            @SQL_STR = 'SELECT
            ch.Client_Name Client
            ,ch.Contracting_Entity
           ,ch.Site_Name [Site Name]
           ,ch.[Address] AS [Street Address]
           ,ch.City_Name [City]
           ,ch.State_Name [State]
           ,ch.Country_Name Country
           ,ch.Account_Number [Account#]
           ,ch.Alternate_Account_Number [Alternate Account#]
           ,STUFF(mn.Meter_Number, LEN(mn.Meter_Number), 1, '''') [Meter Point ID]
           ';
            
      IF @pivot_String IS NOT NULL 
            SELECT
                  @SQL_STR = @SQL_STR + ',srlpd.DETERMINANT_NAME [Determinant]
           ,CAST(ROUND(cv.Load_Profile_Volume,0) AS DECIMAL(32, 0)) [Contract Value]
           ,Load_Profile_Volume_uom [Value UOM],' + @pivot_String; 
                  
      SELECT
            @SQL_STR = @SQL_STR + '   
       FROM
            #Client_Hier_Rfp_Data ch
            CROSS APPLY ( SELECT
                              rch.Meter_Number + '',''
                          FROM
                              #Client_Hier_Rfp_Data rch
                          WHERE
                              rch.SR_RFP_ACCOUNT_ID = ch.Sr_Rfp_Account_Id
                          GROUP BY
                          rch.Meter_Number
            FOR
                          XML PATH('''') ) mn ( Meter_Number )';
                          
      IF @pivot_String IS NOT NULL 
            SELECT
                  @SQL_STR = @SQL_STR + '              
            LEFT OUTER JOIN dbo.SR_RFP_LOAD_PROFILE_SETUP srlps
                  ON srlps.SR_RFP_ACCOUNT_ID = ch.SR_RFP_ACCOUNT_ID
           LEFT OUTER JOIN dbo.SR_RFP_LOAD_PROFILE_DETERMINANT srlpd
                  ON srlps.SR_RFP_LOAD_PROFILE_SETUP_ID = srlpd.SR_RFP_LOAD_PROFILE_SETUP_ID
           LEFT OUTER JOIN #Contract_Volume cv
                  ON cv.SR_RFP_ACCOUNT_ID = ch.SR_RFP_ACCOUNT_ID
                    AND srlpd.DETERMINANT_NAME = cv.DETERMINANT_NAME
            LEFT OUTER JOIN ( SELECT
							dv.SR_RFP_ACCOUNT_ID
							,LEFT(CONVERT(VARCHAR(12),dv.MONTH_IDENTIFIER,109),3) + ''-'' +  RIGHT(CONVERT(VARCHAR(12),dv.MONTH_IDENTIFIER,109),2) MONTH_IDENTIFIER
							,CAST(ROUND(dv.LP_VALUE,0) AS DECIMAL(32, 0)) LP_VALUE
							,dv.DETERMINANT_NAME
						FROM
									#Determinant_Values dv ) as t 
									PIVOT( SUM(LP_VALUE) FOR MONTH_IDENTIFIER IN ( ' + @pivot_String + '  ) ) AS pvt 
									ON  pvt.SR_RFP_ACCOUNT_ID = ch.SR_RFP_ACCOUNT_ID and srlpd.DETERMINANT_NAME =  pvt.DETERMINANT_NAME
           where srlpd.IS_CHECKED = 1 ';
            
      SELECT
            @SQL_STR = @SQL_STR + '    
       GROUP BY
            ch.Client_Name
           ,ch.Contracting_Entity
           ,ch.Site_Name
           ,ch.[Address]
           ,ch.City_Name
           ,ch.State_Name
           ,ch.Country_Name
           ,ch.Account_Number
           ,ch.Alternate_Account_Number
           ,STUFF(mn.Meter_Number, LEN(mn.Meter_Number), 1, '''')';
      IF @pivot_String IS NOT NULL 
            SELECT
                  @SQL_STR = @SQL_STR + '
           ,srlpd.DETERMINANT_NAME
           ,cv.Load_Profile_Volume
           ,cv.Load_Profile_Volume_uom,' + @pivot_String;
       
       
      
      EXECUTE (@SQL_STR);
        
      DROP TABLE #Client_Hier_Rfp_Data;
      DROP TABLE #Contract_Longest_Term_Dates;
      DROP TABLE #Contract_Volume;
      DROP TABLE #SR_RFP_Account_Id;
      DROP TABLE #Determinant_Values;
 
END;


;
GO




GRANT EXECUTE ON  [dbo].[Sr_Rfp_Load_Profile_Sel_By_Rfp_Id_Rfp_Account_Id] TO [CBMSApplication]
GO
