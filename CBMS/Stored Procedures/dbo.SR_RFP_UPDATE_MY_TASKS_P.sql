SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- select * from entity_audit
CREATE  PROCEDURE dbo.SR_RFP_UPDATE_MY_TASKS_P
	@user_id varchar(10),
	@session_id varchar(20),
	@rfp_id int,
	@entity_identifier varchar(200)
	AS
	set nocount on	
	declare @entity_id int
	select @entity_id = entity_id from  entity(nolock) where entity_type = 1046 and entity_name = @entity_identifier

	if(select count(entity_id) from entity_audit(nolock) 
	   where entity_id = @entity_id and entity_identifier = @rfp_id) > 0
	begin
		update entity_audit set modified_date=getdate(), user_info_id = @user_id
	  	where entity_id = @entity_id and entity_identifier = @rfp_id
	end
	else
	begin
		insert into entity_audit(entity_id, entity_identifier, user_info_id, audit_type, modified_date)
		values(@entity_id, @rfp_id, @user_id, 2, getdate())
	end
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_UPDATE_MY_TASKS_P] TO [CBMSApplication]
GO
