SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********           
    
NAME:    
 [dbo].[Cu_Exception_Failed_Reason_Sel_By_Cu_Invoice_Id_Account_Id_Commodity_Id ]        
           
DESCRIPTION:              
   Returns only Failed Recalc Reason .    
          
 INPUT PARAMETERS:              
                         
 Name                        DataType         Default       Description            
---------------------------------------------------------------------------------------------------------------          
 @Cu_Invoice_Id					INT
 @Commodity_Id					INT
 @Account_Id					INT
              
 OUTPUT PARAMETERS:              
                               
 Name                        DataType         Default       Description            
---------------------------------------------------------------------------------------------------------------          
              
 USAGE EXAMPLES:                                
---------------------------------------------------------------------------------------------------------------                                
     
    
EXEC  Cu_Exception_Failed_Reason_Sel_By_Cu_Invoice_Id_Account_Id_Commodity_Id 
    
          
 AUTHOR INITIALS:            
           
 Initials              Name            
---------------------------------------------------------------------------------------------------------------                          
 RKV       Ravi Kumar Vegesna              
    
      
 MODIFICATIONS:          
              
 Initials              Date             Modification          
---------------------------------------------------------------------------------------------------------------          
 RKV                   2015-09-10       Created as Part of AS400/CIP UAT UATAS400CI-74
*/    
CREATE PROCEDURE [dbo].[Cu_Exception_Failed_Reason_Sel_By_Cu_Invoice_Id_Account_Id_Commodity_Id]
      ( 
       @Cu_Invoice_Id INT
      ,@Account_Id INT
      ,@Commodity_Id INT )
AS 
BEGIN  
      SELECT
            rfrt.Code_dsc + ' - ' + ISNULL(ced.Recalc_Response_Error_Dsc, '') AS Recalc_Failed_Reason_Type
      FROM
            dbo.cu_exception_denorm ced
            INNER JOIN dbo.Code rfrt
                  ON rfrt.Code_Id = ced.Recalc_Failed_Reason_Type_cd
      WHERE
            ced.Cu_Invoice_Id = @Cu_Invoice_Id
            AND ced.Account_ID = @Account_Id
            AND ced.Commodity_Id = @Commodity_Id
            AND ced.exception_type = 'Failed Recalc'
END


;
GO
GRANT EXECUTE ON  [dbo].[Cu_Exception_Failed_Reason_Sel_By_Cu_Invoice_Id_Account_Id_Commodity_Id] TO [CBMSApplication]
GO
