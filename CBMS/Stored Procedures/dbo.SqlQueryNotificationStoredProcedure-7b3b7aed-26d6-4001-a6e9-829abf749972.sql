SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SqlQueryNotificationStoredProcedure-7b3b7aed-26d6-4001-a6e9-829abf749972] AS BEGIN BEGIN TRANSACTION; RECEIVE TOP(0) conversation_handle FROM [SqlQueryNotificationService-7b3b7aed-26d6-4001-a6e9-829abf749972]; IF (SELECT COUNT(*) FROM [SqlQueryNotificationService-7b3b7aed-26d6-4001-a6e9-829abf749972] WHERE message_type_name = 'http://schemas.microsoft.com/SQL/ServiceBroker/DialogTimer') > 0 BEGIN if ((SELECT COUNT(*) FROM sys.services WHERE name = 'SqlQueryNotificationService-7b3b7aed-26d6-4001-a6e9-829abf749972') > 0)   DROP SERVICE [SqlQueryNotificationService-7b3b7aed-26d6-4001-a6e9-829abf749972]; if (OBJECT_ID('SqlQueryNotificationService-7b3b7aed-26d6-4001-a6e9-829abf749972', 'SQ') IS NOT NULL)   DROP QUEUE [SqlQueryNotificationService-7b3b7aed-26d6-4001-a6e9-829abf749972]; DROP PROCEDURE [SqlQueryNotificationStoredProcedure-7b3b7aed-26d6-4001-a6e9-829abf749972]; END COMMIT TRANSACTION; END
GO
