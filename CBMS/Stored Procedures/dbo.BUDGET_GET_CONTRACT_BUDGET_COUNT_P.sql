SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE dbo.BUDGET_GET_CONTRACT_BUDGET_COUNT_P
	@contract_id int
	AS
	begin
		set nocount on

		select count(budget_contract_budget_id)contract_budget_count from budget_contract_budget where contract_id = @contract_id
	end







GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_CONTRACT_BUDGET_COUNT_P] TO [CBMSApplication]
GO
