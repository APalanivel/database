SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE dbo.SAVE_BUDGET_SITE_MAP_P

@userId varchar(10),
@sessionId varchar(20),

@budgetId int,
@siteId int

as
	set nocount on

	insert into rm_budget_site_map (rm_budget_id,site_id)
	values(@budgetId,@siteId)
GO
GRANT EXECUTE ON  [dbo].[SAVE_BUDGET_SITE_MAP_P] TO [CBMSApplication]
GO
