SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******              
Name:   dbo.Invoice_Collection_Final_Review_Contact_Method_Sel_By_Invoice_Collection_Final_Review_Log_Id       
              
Description:              
			This sproc is to get the Contact Details For the Final_Review log id
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
@Invoice_Collection_Activity_Id 		INT					
                           
                    
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

   Exec dbo.Invoice_Collection_Final_Review_Contact_Method_Sel_By_Invoice_Collection_Final_Review_Log_Id 215
   
   Exec Invoice_Collection_Final_Review_Contact_Method_Sel_By_Invoice_Collection_Final_Review_Log_Id 1040
   
  
   
   
   
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna 
	NR				Narayana Reddy              
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2017-01-09		Created For Invoice_Collection. 
                 
             
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Final_Review_Contact_Method_Sel_By_Invoice_Collection_Final_Review_Log_Id]
     (
         @Invoice_Collection_Final_Review_Log_Id INT
         , @Invoice_Collection_Officer_Id INT = NULL
         , @Sort_Col VARCHAR(50) = 'Commodity_Name'
         , @Sort_Order VARCHAR(15) = 'ASC'
     )
AS
    BEGIN
        SET NOCOUNT ON;

        CREATE TABLE #Vendor_Dtls
             (
                 Account_Vendor_Name VARCHAR(200)
                 , Account_Vendor_Type CHAR(8)
                 , Invoice_Collection_Account_Config_Id INT
             );


        DECLARE
            @Source_Type_Client INT
            , @Source_Type_Account INT
            , @Source_Type_Vendor INT
            , @Contact_Level_Client INT
            , @Contact_Level_Account INT
            , @Contact_Level_Vendor INT
            , @Select_SQL_Statement NVARCHAR(MAX)
            , @Join_SQL_Statement1 NVARCHAR(MAX)
            , @Join_SQL_Statement NVARCHAR(MAX)
            , @Group_By NVARCHAR(MAX)
            , @Order_By NVARCHAR(MAX)
            , @Where_clause NVARCHAR(MAX);


        SELECT
            @Source_Type_Client = MAX(CASE WHEN c.Code_Value = 'Client Primary Contact' THEN c.Code_Id
                                      END)
            , @Source_Type_Account = MAX(CASE WHEN c.Code_Value = 'Account Primary Contact' THEN c.Code_Id
                                         END)
            , @Source_Type_Vendor = MAX(CASE WHEN c.Code_Value = 'Vendor Primary Contact' THEN c.Code_Id
                                        END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'InvoiceSourceType'
            AND c.Code_Value IN ( 'Account Primary Contact', 'Client Primary Contact', 'Vendor Primary Contact' );


        SELECT
            @Contact_Level_Client = MAX(CASE WHEN c.Code_Value = 'Client' THEN c.Code_Id
                                        END)
            , @Contact_Level_Account = MAX(CASE WHEN c.Code_Value = 'Account' THEN c.Code_Id
                                           END)
            , @Contact_Level_Vendor = MAX(CASE WHEN c.Code_Value = 'Vendor' THEN c.Code_Id
                                          END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'ContactLevel';

        INSERT INTO #Vendor_Dtls
             (
                 Account_Vendor_Name
                 , Account_Vendor_Type
                 , Invoice_Collection_Account_Config_Id
             )
        SELECT
            Account_Vendor_Name = CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                                            AND asbv1.Account_Id IS NOT NULL THEN scha.Account_Vendor_Name
                                      WHEN v.VENDOR_NAME IS NOT NULL THEN v.VENDOR_NAME
                                      WHEN icav.VENDOR_NAME IS NOT NULL
                                           AND  asbv1.Account_Id IS NOT NULL THEN icav.VENDOR_NAME
                                      ELSE ucha.Account_Vendor_Name
                                  END
            , Account_Vendor_Type = CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                                              AND   asbv1.Account_Id IS NOT NULL THEN scha.Account_Type
                                        WHEN e.ENTITY_NAME IS NOT NULL THEN e.ENTITY_NAME
                                        WHEN icav.VENDOR_NAME IS NOT NULL
                                             AND asbv1.Account_Id IS NOT NULL THEN 'Supplier'
                                        ELSE ucha.Account_Type
                                    END
            , icac.Invoice_Collection_Account_Config_Id
        FROM
            dbo.Invoice_Collection_Final_Review_Log icccm
            INNER JOIN dbo.Invoice_Collection_Final_Review_Log_Queue_Map icccmqm
                ON icccm.Invoice_Collection_Final_Review_Log_Id = icccmqm.Invoice_Collection_Final_Review_Log_Id
            INNER JOIN dbo.Invoice_Collection_Queue icq
                ON icccmqm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN Core.Client_Hier_Account ucha
                ON ucha.Account_Id = icac.Account_Id
            INNER JOIN dbo.Code cpc
                ON cpc.Code_Id = icac.Chase_Priority_Cd
            LEFT OUTER JOIN(Core.Client_Hier_Account scha
                            INNER JOIN Core.Client_Hier_Account ucha1
                                ON ucha1.Meter_Id = scha.Meter_Id
                                   AND  ucha1.Account_Type = 'Utility'
                            INNER JOIN dbo.CONTRACT c
                                ON c.CONTRACT_ID = scha.Supplier_Contract_ID
                            INNER JOIN dbo.ENTITY ce
                                ON c.CONTRACT_TYPE_ID = ce.ENTITY_ID
                                   AND  ce.ENTITY_NAME = 'Supplier')
                ON ucha.Account_Id = ucha1.Account_Id
                   AND  scha.Account_Type = 'Supplier'
                   AND  (CASE WHEN icac.Invoice_Collection_Service_End_Dt IS NULL
                                   OR   icac.Invoice_Collection_Service_End_Dt > GETDATE() THEN GETDATE()
                             ELSE icac.Invoice_Collection_Service_End_Dt
                         END) BETWEEN scha.Supplier_Meter_Association_Date
                              AND     scha.Supplier_Meter_Disassociation_Date
            LEFT OUTER JOIN(dbo.Account_Consolidated_Billing_Vendor asbv
                            INNER JOIN dbo.ENTITY e
                                ON asbv.Invoice_Vendor_Type_Id = e.ENTITY_ID
                                   AND  e.ENTITY_NAME = 'Supplier'
                            LEFT OUTER JOIN dbo.VENDOR v
                                ON v.VENDOR_ID = asbv.Supplier_Vendor_Id)
                ON asbv.Account_Id = ucha.Account_Id
                   AND  icac.Invoice_Collection_Service_End_Dt BETWEEN asbv.Billing_Start_Dt
                                                               AND     asbv.Billing_End_Dt
            LEFT OUTER JOIN(dbo.Account_Consolidated_Billing_Vendor asbv1
                            INNER JOIN dbo.ENTITY e1
                                ON asbv1.Invoice_Vendor_Type_Id = e1.ENTITY_ID
                                   AND  e1.ENTITY_NAME = 'Supplier'
                            LEFT OUTER JOIN dbo.VENDOR v1
                                ON v1.VENDOR_ID = asbv1.Supplier_Vendor_Id)
                ON asbv1.Account_Id = ucha.Account_Id
            LEFT OUTER JOIN(dbo.Invoice_Collection_Account_Contact icc
                            INNER JOIN dbo.Account_Invoice_Collection_Source aics
                                ON icc.Invoice_Collection_Account_Config_Id = aics.Invoice_Collection_Account_Config_Id
                            INNER JOIN dbo.Contact_Info ci
                                ON ci.Contact_Info_Id = icc.Contact_Info_Id
                                   AND  icc.Is_Primary = 1
                                   AND  (   (   @Source_Type_Client = aics.Invoice_Source_Type_Cd
                                                AND @Contact_Level_Client = ci.Contact_Level_Cd)
                                            OR  (   @Source_Type_Account = aics.Invoice_Source_Type_Cd
                                                    AND @Contact_Level_Account = ci.Contact_Level_Cd)
                                            OR  (   @Source_Type_Vendor = aics.Invoice_Source_Type_Cd
                                                    AND @Contact_Level_Vendor = ci.Contact_Level_Cd))
                            INNER JOIN dbo.Vendor_Contact_Map vcm
                                ON ci.Contact_Info_Id = vcm.Contact_Info_Id
                            INNER JOIN dbo.VENDOR icav
                                ON icav.VENDOR_ID = vcm.VENDOR_ID)
                ON icc.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
        WHERE
            ucha.Account_Type = 'Utility'
            AND icccm.Invoice_Collection_Final_Review_Log_Id = @Invoice_Collection_Final_Review_Log_Id
            AND (   @Invoice_Collection_Officer_Id IS NULL
                    OR  icac.Invoice_Collection_Officer_User_Id = @Invoice_Collection_Officer_Id)
        GROUP BY
            CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                      AND   asbv1.Account_Id IS NOT NULL THEN scha.Account_Vendor_Name
                WHEN v.VENDOR_NAME IS NOT NULL THEN v.VENDOR_NAME
                WHEN icav.VENDOR_NAME IS NOT NULL
                     AND asbv1.Account_Id IS NOT NULL THEN icav.VENDOR_NAME
                ELSE ucha.Account_Vendor_Name
            END
            , CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                        AND asbv1.Account_Id IS NOT NULL THEN scha.Account_Type
                  WHEN e.ENTITY_NAME IS NOT NULL THEN e.ENTITY_NAME
                  WHEN icav.VENDOR_NAME IS NOT NULL
                       AND  asbv1.Account_Id IS NOT NULL THEN 'Supplier'
                  ELSE ucha.Account_Type
              END
            , icac.Invoice_Collection_Account_Config_Id;

        SELECT
            @Select_SQL_Statement = N'SELECT ch.Client_Name, ch.Site_name, cha.Account_Number, cha.Alternate_Account_Number, ch.Country_Name, ch.State_Name
            , ISNULL(vd.Account_Vendor_Type, cha.Account_Type) Account_Type
            , CASE WHEN LEN(Commodities.Commodity_Name) > 0 THEN
                       LEFT(Commodities.Commodity_Name, LEN(Commodities.Commodity_Name) - 1)
                  ELSE Commodities.Commodity_Name
              END AS Commodity_Name, ISNULL(vd.Account_Vendor_Name, cha.Account_Vendor_Name) Account_Vendor_Name
            , ifc.Code_Value Invoice_Frequency , CAST(icq.Collection_Start_Dt AS VARCHAR(15)) + ''#'' + CAST(icq.Collection_End_Dt AS VARCHAR(15)) + ''/''
              + LEFT(ISNULL(icmrt.Code_Value, ''''), 1) Period_to_chase, ci.Email_Address Contact_Email, ci.First_Name + '' '' + ci.Last_Name Username, ci.Phone_Number Contact_Phone
            , ch.Client_Id, aic.Code_Value source_Type, icac.Invoice_Collection_Account_Config_Id, icccm.Invoice_Final_Review_Comment, icccmqm.Invoice_Collection_Queue_Id';


        SELECT
            @Join_SQL_Statement = N'    FROM
            dbo.Invoice_Collection_Final_Review_Log icccm
            INNER JOIN dbo.Invoice_Collection_Final_Review_Log_Queue_Map icccmqm
                ON icccm.Invoice_Collection_Final_Review_Log_Id = icccmqm.Invoice_Collection_Final_Review_Log_Id
            INNER JOIN dbo.Invoice_Collection_Queue icq
                ON icccmqm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = icac.Account_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN dbo.USER_INFO ui
                ON ui.USER_INFO_ID = icac.Invoice_Collection_Officer_User_Id
            INNER JOIN dbo.Invoice_Collection_Queue_Month_Map icqmm
                ON icq.Invoice_Collection_Queue_Id = icqmm.Invoice_Collection_Queue_Id
            INNER JOIN(dbo.Account_Invoice_Collection_Month aicm
                       LEFT OUTER JOIN dbo.Account_Invoice_Collection_Frequency aicfe
                           ON aicm.Account_Invoice_Collection_Frequency_Id = aicfe.Account_Invoice_Collection_Frequency_Id
                       LEFT OUTER JOIN dbo.Code ifc
                           ON ifc.Code_Id = aicfe.Invoice_Frequency_Cd
                       LEFT OUTER JOIN dbo.Invoice_Collection_Global_Config_Value icgcv
                           ON aicm.Invoice_Collection_Global_Config_Value_Id = icgcv.Invoice_Collection_Global_Config_Value_Id
                       LEFT OUTER JOIN dbo.Code icgc
                           ON icgc.Code_Id = icgcv.Invoice_Frequency_Cd)
                ON icqmm.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id
            LEFT OUTER JOIN dbo.Code cpc
                ON cpc.Code_Id = icac.Chase_Priority_Cd
            LEFT OUTER JOIN dbo.Code icmrt
                ON icmrt.Code_Id = icq.Invoice_Request_Type_Cd';
        SELECT
            @Join_SQL_Statement1 = N' LEFT OUTER JOIN(dbo.Invoice_Collection_Account_Contact icc
                            INNER JOIN dbo.Account_Invoice_Collection_Source aics
                                ON icc.Invoice_Collection_Account_Config_Id = aics.Invoice_Collection_Account_Config_Id
                                   AND  aics.Is_Primary = 1
                            INNER JOIN dbo.Contact_Info ci
                                ON ci.Contact_Info_Id = icc.Contact_Info_Id
                                   AND  icc.Is_Primary = 1
                                   AND  (   ( ' + CAST(@Source_Type_Client AS VARCHAR(50))
                                   + N'= aics.Invoice_Source_Type_Cd
                                                AND  ' + CAST(@Contact_Level_Client AS VARCHAR(50))
                                   + N' = ci.Contact_Level_Cd)
                                            OR  (    ' + CAST(@Source_Type_Account AS VARCHAR(50))
                                   + N' = aics.Invoice_Source_Type_Cd
                                                    AND  ' + CAST(@Contact_Level_Account AS VARCHAR(50))
                                   + N' = ci.Contact_Level_Cd)
                                            OR  (    ' + CAST(@Source_Type_Vendor AS VARCHAR(50))
                                   + N' = aics.Invoice_Source_Type_Cd
                                                    AND  ' + CAST(@Contact_Level_Vendor AS VARCHAR(50))
                                   + N' = ci.Contact_Level_Cd)))
                ON icc.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            LEFT OUTER JOIN dbo.Code aic
                ON aic.Code_Id = aics.Invoice_Source_Type_Cd
            CROSS APPLY
        (   SELECT
                c.Commodity_Name + '',''
            FROM
                dbo.Invoice_Collection_Queue icqe
                INNER JOIN dbo.Invoice_Collection_Account_Config icac1
                    ON icqe.Invoice_Collection_Account_Config_Id = icac1.Invoice_Collection_Account_Config_Id
                INNER JOIN Core.Client_Hier_Account cha
                    ON cha.Account_Id = icac1.Account_Id
                INNER JOIN dbo.Commodity c
                    ON c.Commodity_Id = cha.Commodity_Id
            WHERE
                icq.Invoice_Collection_Queue_Id = icqe.Invoice_Collection_Queue_Id
            GROUP BY
                c.Commodity_Name
            FOR XML PATH('''')) Commodities(Commodity_Name)
            LEFT OUTER JOIN #Vendor_Dtls vd
                ON vd.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id';

        SELECT
            @Where_clause = N'       WHERE
            icccm.Invoice_Collection_Final_Review_Log_Id = '
                            + CAST(ISNULL(@Invoice_Collection_Final_Review_Log_Id, 0) AS VARCHAR(50)) + N'
			and (' + CAST(ISNULL(@Invoice_Collection_Officer_Id, 0) AS VARCHAR(50))
                            + N' = 0 
           
                    OR  icac.Invoice_Collection_Officer_User_Id = '
                            + CAST(ISNULL(@Invoice_Collection_Officer_Id, 0) AS VARCHAR(50)) + N')';

        SELECT
            @Group_By = N'
        GROUP BY
            ch.Client_Name
            , ch.Site_name
            , cha.Account_Number
            , cha.Alternate_Account_Number
            , ch.Country_Name
            , ch.State_Name
            , ISNULL(vd.Account_Vendor_Type, cha.Account_Type)
            , CASE WHEN LEN(Commodities.Commodity_Name) > 0 THEN
                       LEFT(Commodities.Commodity_Name, LEN(Commodities.Commodity_Name) - 1)
                  ELSE Commodities.Commodity_Name
              END
            , ISNULL(vd.Account_Vendor_Name, cha.Account_Vendor_Name)
            , ifc.Code_Value
            , CAST(icq.Collection_Start_Dt AS VARCHAR(15)) , CAST(icq.Collection_End_Dt AS VARCHAR(15))
            , ci.Email_Address
            , ci.Phone_Number
            , ch.Client_Id
            , aic.Code_Value
            , icac.Invoice_Collection_Account_Config_Id
            , ci.First_Name
            , ci.Last_Name
            , icccm.Invoice_Final_Review_Comment
            , icccmqm.Invoice_Collection_Queue_Id
            , icmrt.Code_Value';

        SELECT
            @Order_By = N'  ORDER BY '
                        + CASE WHEN @Sort_Col = 'Commodity_Name' THEN
                                   +' CASE WHEN LEN(Commodities.Commodity_Name) > 0 THEN LEFT(Commodities.Commodity_Name, LEN(Commodities.Commodity_Name) - 1)
																ELSE Commodities.Commodity_Name
																END '
                              WHEN @Sort_Col = 'Account_Vendor_Name' THEN
                                  +' ISNULL(vd.Account_Vendor_Name, cha.Account_Vendor_Name) '
                              WHEN @Sort_Col = 'Period_to_chase' THEN +'CAST(icq.Collection_Start_Dt AS VARCHAR(15))'
                              ELSE @Sort_Col
                          END + N' ' + @Sort_Order;


        EXEC (@Select_SQL_Statement + @Join_SQL_Statement + @Join_SQL_Statement1 + @Where_clause + @Group_By + @Order_By);



    END;









GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Final_Review_Contact_Method_Sel_By_Invoice_Collection_Final_Review_Log_Id] TO [CBMSApplication]
GO
