SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*****          

NAME: dbo.Cost_Usage_Account_Dtl_Del_By_Client_Hier_Id 

DESCRIPTION: To delete the details Cost_Usage_Site_Dtl_Id for a month , for deletion purpose

      
INPUT PARAMETERS:          
	NAME				DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Data_Source_Cd	varchar
	@Client_Hier_Id	varchar
	@Bucket_Master_Id	varchar
	@Service_Month	varchar
    
OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
table data

USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
	   EXEC dbo.Cost_Usage_Account_Dtl_Del_By_Client_Hier_Id 100350,10918,101697,'2013-10-01',1234
  Rollback Tran

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	AS			Arun Skaria
	RK          Raghu Kalvapudi
MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	AS			12-NOV-13	CREATED
	RK          11/25/2013  Removed invoice check.
*****/

CREATE PROCEDURE [dbo].[Cost_Usage_Account_Dtl_Del_By_Client_Hier_Id]
      (
       @Data_Source_Cd INT
      ,@Client_Hier_Id INT
      ,@Bucket_Master_Id INT
      ,@Service_Month DATE
      ,@AccountId INT )
AS
BEGIN
      SET NOCOUNT ON;
           
      DELETE FROM
            dbo.Cost_Usage_Account_Dtl
      WHERE
            ACCOUNT_ID = @AccountId
            AND Client_Hier_Id = @Client_Hier_Id
            AND Bucket_Master_Id = @Bucket_Master_Id
            AND Service_Month = @Service_Month
END




;
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Dtl_Del_By_Client_Hier_Id] TO [CBMSApplication]
GO
