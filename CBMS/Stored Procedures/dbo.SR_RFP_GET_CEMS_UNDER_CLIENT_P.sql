SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.SR_RFP_GET_CEMS_UNDER_CLIENT_P 
@userId varchar,
@sessionId varchar,
@clientId int,
@siteName varchar(200)
as
	set nocount on
	select	

	cemMap.USER_INFO_ID,
	userinfo.LAST_NAME,
	userinfo.FIRST_NAME,
	userinfo.EMAIL_ADDRESS,
	client.CLIENT_NAME,
	client.CLIENT_ID,
	@siteName SITE_NAME
	
	
	
from	CLIENT_CEM_MAP cemMap,
	USER_INFO userinfo,
	CLIENT

	
	
where	
	cemMap.CLIENT_ID = @clientId AND 
	client.client_id=cemMap.CLIENT_ID and
	cemMap.USER_INFO_ID = userinfo.USER_INFO_ID
	


--exec SR_RFP_GET_CEMS_UNDER_CLIENT_P 1,1,102
--select * from RM_CEM_TEAM where 
--select * from CLIENT_CEM_MAP where client_id =102
--SELECT * FROM USER_INFO WHERE USER_INFO_ID=70
--not in (select c.client_id from CLIENT_CEM_MAP c,CLIENT_CEM_MAP c1 where c.client_id=c1.client_id)
--select * from USER_INFO
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_CEMS_UNDER_CLIENT_P] TO [CBMSApplication]
GO
