SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE dbo.SR_GET_UTILITIES_UNDER_STATE_P 
@userId varchar,
@sessionId varchar,
@stateId int

as
	set nocount on
select	v.VENDOR_ID,
	v.VENDOR_NAME 

from 	VENDOR v(nolock),
	VENDOR_STATE_MAP vsm(nolock),
	entity venType(nolock)

where 	vsm.STATE_ID = @stateId  
	and v.VENDOR_ID = vsm.VENDOR_ID 
	and v.vendor_type_id = venType.entity_id 
	and venType.entity_type = 155
	and venType.entity_name = 'Utility'

order by v.vendor_name
GO
GRANT EXECUTE ON  [dbo].[SR_GET_UTILITIES_UNDER_STATE_P] TO [CBMSApplication]
GO
