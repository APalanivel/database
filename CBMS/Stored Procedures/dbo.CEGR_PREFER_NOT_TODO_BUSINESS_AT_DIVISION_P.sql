SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE     PROCEDURE dbo.CEGR_PREFER_NOT_TODO_BUSINESS_AT_DIVISION_P
	@USER_ID VARCHAR(10),
	@SESSION_ID VARCHAR(20),
	@DIVISION_ID INT
	AS
begin
set nocount on
SELECT 
	VEND.VENDOR_NAME VENDOR_NAME
FROM
	PREFER_NOT_TODO_BUSINESS PREFER_NOT,
	VENDOR VEND
WHERE
	PREFER_NOT.VENDOR_ID = VEND.VENDOR_ID
	AND PREFER_NOT.DIVISION_ID = @DIVISION_ID
end
GO
GRANT EXECUTE ON  [dbo].[CEGR_PREFER_NOT_TODO_BUSINESS_AT_DIVISION_P] TO [CBMSApplication]
GO
