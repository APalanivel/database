SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********  
NAME:	[dbo].[Invoice_Recalc_Get_All_Processing]   

DESCRIPTION:     

	
INPUT PARAMETERS:    
Name								DataType		Default		Description    
----------------------------------------------------------------------------    
@User_Info_Id					 	INT      

OUTPUT PARAMETERS:    
Name					DataType		Default		Description    
----------------------------------------------------------------------------    

USAGE EXAMPLES:    
---------------------------------------------------------------------------- 

EXEC dbo.CODE_SEL_BY_CodeSet_Name
    @CodeSet_Name = 'Request Status'
    , @Code_Value = 'Pending'

EXEC dbo.CODE_SEL_BY_CodeSet_Name
    @CodeSet_Name = 'Request Status'
    , @Code_Value = 'Error'


EXEC dbo.CODE_SEL_BY_CodeSet_Name
    @CodeSet_Name = 'Request Status'
    , @Code_Value = 'Completed'
 
declare @tvp_Invoice_Recalc_Process tvp_Invoice_Recalc_Process

insert into @tvp_Invoice_Recalc_Process values (10, 100429)

EXEc dbo.Invoice_Recalc_Get_All_Processing
    @tvp_Invoice_Recalc_Process = @tvp_Invoice_Recalc_Process
    , @User_Info_Id = 49

Select * from dbo.Invoice_Recalc_Process_Batch
Select * from dbo.Invoice_Recalc_Process_Batch_Dtl


AUTHOR INITIALS:    
Initials	Name    
----------------------------------------------------------------------------    
NR			Narayana Reddy
 
MODIFICATIONS     
Initials	Date			Modification    
-----------------------------------------------------------------------------    
NR			2019-09-03		Add Contract -  Created.
******/



CREATE PROCEDURE [dbo].[Invoice_Recalc_Process_Batch_Upd]
     (
         @Invoice_Recalc_Process_Batch_Id INT
         , @Invoice_Recalc_Process_Batch_Dtl_Id INT
         , @Status_Cd INT
         , @Error_Dsc NVARCHAR(MAX) = NULL
         , @User_Info_Id INT
     )
AS
    BEGIN

        SET NOCOUNT ON;


        UPDATE
            irpbd
        SET
            irpbd.Status_Cd = @Status_Cd
            , irpbd.Updated_User_Id = @User_Info_Id
            , irpbd.Last_Change_Ts = GETDATE()
            , irpbd.Error_Dsc = @Error_Dsc
        FROM
            dbo.Invoice_Recalc_Process_Batch_Dtl irpbd
        WHERE
            irpbd.Invoice_Recalc_Process_Batch_Id = @Invoice_Recalc_Process_Batch_Id
            AND irpbd.Invoice_Recalc_Process_Batch_Dtl_Id = @Invoice_Recalc_Process_Batch_Dtl_Id;



        UPDATE
            irb
        SET
            irb.Status_Cd = @Status_Cd
            , irb.Updated_User_Id = @User_Info_Id
            , irb.Last_Change_Ts = GETDATE()
        FROM
            dbo.Invoice_Recalc_Process_Batch irb
        WHERE
            Invoice_Recalc_Process_Batch_Id = @Invoice_Recalc_Process_Batch_Id;




    END;

GO
GRANT EXECUTE ON  [dbo].[Invoice_Recalc_Process_Batch_Upd] TO [CBMSApplication]
GO
