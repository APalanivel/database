SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
     
/*********       
NAME:  dbo.Cu_Invoice_Sel_By_Account_Id
   
INPUT PARAMETERS:        
Name              DataType          Default     Description        
------------------------------------------------------------        
@cu_invoice_id		INT      
@account_id			INT				NULL   
        
OUTPUT PARAMETERS:        
Name              DataType          Default     Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:   
	EXEC Cu_Invoice_Sel_By_Account_Id 6891
	EXEC Cu_Invoice_Sel_By_Account_Id 6891,6704
	EXEC Cu_Invoice_Sel_By_Account_Id 2928574
     
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
    
Initials	Date		Modification      
------------------------------------------------------------      
			02/17/2010  Created    
SSR			03/12/2010	Removed Nolock Hints and added table Owner Name 
SKA			03/18/2010	Removed Cu_invoice_account_map(Account_id) with cu_invoice_service_month(Account_id)
						
******/  

CREATE  PROCEDURE dbo.Cu_Invoice_Sel_By_Account_Id
 ( @cu_invoice_id INT      
 , @account_id INT = NULL   
 )    
AS    
BEGIN    
    
    SET NOCOUNT ON;
    
	SELECT
		ci.cu_invoice_id    
		, ci.cbms_image_id    
		, ci.ubm_id    
		, ci.ubm_invoice_id    
		, ci.ubm_account_code    
		, ci.ubm_client_code    
		, ci.ubm_city    
		, ci.ubm_state_code    
		, ci.ubm_vendor_code    
		, ci.ubm_account_number    
		, ci.account_group_id    
		, cism.account_id    
		, ci.client_id    
		, ci.vendor_id    
		, ci.begin_date    
		, ci.end_date    
		, ci.billing_days    
		, ci.ubm_currency_code    
		, ci.currency_unit_id    
		, ci.current_charges    
		, ci.is_default    
		, ci.is_processed    
		, ci.is_reported    
		, ci.is_dnt    
		, ci.do_not_track_id    
		, ci.updated_by_id    
		, ci.updated_date    
		, ci.ubm_invoice_identifier    
		, ci.ubm_feed_file_name    
		, ci.is_manual    
		, ci.is_duplicate    
	FROM 
		dbo.cu_invoice ci 
		LEFT JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
			ON cism.CU_INVOICE_ID = ci.CU_INVOICE_ID     
	WHERE 
		(ci.cu_invoice_id = @cu_invoice_id)
		AND (@account_id IS NULL OR cism.account_id = @account_id)     
	GROUP BY
		ci.cu_invoice_id    
		, ci.cbms_image_id    
		, ci.ubm_id    
		, ci.ubm_invoice_id    
		, ci.ubm_account_code    
		, ci.ubm_client_code    
		, ci.ubm_city    
		, ci.ubm_state_code    
		, ci.ubm_vendor_code    
		, ci.ubm_account_number    
		, ci.account_group_id    
		, cism.account_id    
		, ci.client_id    
		, ci.vendor_id    
		, ci.begin_date    
		, ci.end_date    
		, ci.billing_days    
		, ci.ubm_currency_code    
		, ci.currency_unit_id    
		, ci.current_charges    
		, ci.is_default    
		, ci.is_processed    
		, ci.is_reported    
		, ci.is_dnt    
		, ci.do_not_track_id    
		, ci.updated_by_id    
		, ci.updated_date    
		, ci.ubm_invoice_identifier    
		, ci.ubm_feed_file_name    
		, ci.is_manual    
		, ci.is_duplicate
	
END 
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Sel_By_Account_Id] TO [CBMSApplication]
GO
