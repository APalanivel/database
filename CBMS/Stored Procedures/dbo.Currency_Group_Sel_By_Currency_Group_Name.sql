SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*****
NAME:
	[dbo].[Currency_Group_Sel_By_Currency_Group_Name] 

DESCRIPTION:
	Stored procedure for Selecting currency group Id.
	   
INPUT PARAMETERS:
      Name					DataType          Default     Description
-------------------------------------------------------------------
	
	@Currency_Group_Name	VARCHAR
	   
OUTPUT PARAMETERS:
      Name              DataType          Default     Description
--------------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------------------------    
	EXEC  Currency_Group_Sel_By_Currency_Group_Name 'standard'
AUTHOR INITIALS:
	Initials    Name			
------------------------------------------------------------
	AL          AJEESH		
                
MODIFICATIONS:
	Initials    Date			Modification
------------------------------------------------------------
	AL			08/09/2012		Created
	
******/	


CREATE PROCEDURE [dbo].[Currency_Group_Sel_By_Currency_Group_Name]  
	( 
		  @Currency_Group_Name		VARCHAR(200)  

	)
AS  
BEGIN  
	SET NOCOUNT ON;
    
	SELECT 
		CG.currency_group_id  
	FROM 
		dbo.CURRENCY_GROUP CG 
	WHERE 
		CG.currency_group_name =  @Currency_Group_Name	 
END 



;
GO
GRANT EXECUTE ON  [dbo].[Currency_Group_Sel_By_Currency_Group_Name] TO [CBMSApplication]
GO
