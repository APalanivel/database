SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.DELTOOL_ACCOUNT_GET_ISDELETED_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	int       	          	
	@account_id    	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

--select * from budget_account where budget_id=1490 and a
--select * from account where account_number = 'account2'
--exec DELTOOL_ACCOUNT_GET_DEPENDENCIES_P -1,@account_id
--exec DELTOOL_ACCOUNT_GET_ISDELETED_P -1,40335



CREATE        procedure dbo.DELTOOL_ACCOUNT_GET_ISDELETED_P
( 
	@userId int
	,@account_id int
)
AS
begin
--declare @budgetId int
--select @budgetId = 0
--select @budgetId = (select budget_id from budget_account where budget_account.account_id = @account_id)
--if(@budgetId > 0)

	select  budget.budget_id,is_deleted ,
                case when is_Posted_to_dv > 0
                     then 'Y'
                     else 'N'
                   end  as 'is_Posted_to_dv'
           from   budget,
                  budget_account
           where budget_account.account_id=@account_id
                 and budget.budget_id = budget_account.budget_id
           

END
GO
GRANT EXECUTE ON  [dbo].[DELTOOL_ACCOUNT_GET_ISDELETED_P] TO [CBMSApplication]
GO
