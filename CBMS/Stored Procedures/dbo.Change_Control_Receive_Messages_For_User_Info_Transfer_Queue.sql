
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO


/******
NAME:		Change_Control_Receive_Messages_For_User_Info_Transfer_Queue

DESCRIPTION:
	automaitcially activates when a record is recieved on the User_Info_Transfer_Queue
	and manages the changes based on the Message Type


INPUT PARAMETERS:
	Name						DataType		Default	Description
------------------------------------------------------------


OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
 -- Procedure is only executed on User_Info_Transfer_Queue

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	KVK			Vinay Kacham
	
MODIFICATIONS

	Initials	Date			Modification
------------------------------------------------------------
	KVK			02/06/2014		Created
	DSC			06/01/2015		MAINT-3511: Convert Service Broker Message Monitoring to Queued update
******/
CREATE PROCEDURE [dbo].[Change_Control_Receive_Messages_For_User_Info_Transfer_Queue]
AS 
BEGIN 
      SET NOCOUNT ON; 

      DECLARE
            @Param_Definition NVARCHAR(100) = N'@Message XML, @Conversation_Handle UNIQUEIDENTIFIER' -- footprint of message procedures
           ,@CG_Iterator BIT = 1 -- Flag used to iterate through conversation Groups

      DECLARE @QName NVARCHAR(200) = 'User_Info_Transfer_Queue'
      DECLARE @CurrentDateTime DATETIME = getdate()
      DECLARE @MaxLoopcnt INT = 0
      DECLARE @MinLoopcnt INT = 1

      DECLARE @Messages TABLE -- Table used to store failed messages for a conversation_Group
            ( 
             Message_Sequence_Number INT
            ,Conversation_Handle UNIQUEIDENTIFIER
            ,SERVICE_NAME VARCHAR(255)
            ,CONTRACT_NAME VARCHAR(255)
            ,Message_Type VARCHAR(255)
            ,Message_Body VARBINARY(MAX) )


      DECLARE
            @QueueName NVARCHAR(200)
           ,@MessageType NVARCHAR(200)
           ,@MessageReceivedTs DATETIME
           ,@MessageSequenceNumber INT
           ,@ConversationGroupId UNIQUEIDENTIFIER
           ,@ConversationHandle UNIQUEIDENTIFIER
           ,@Servicename NVARCHAR(200)
           ,@Contractname NVARCHAR(200)
           ,@ErrorText VARCHAR(MAX)
           ,@MessageBody VARBINARY(MAX)
           ,@MessageStatus VARCHAR(200)
           ,@MessageCompletedTs DATETIME
           ,@MessageBatchCount INT


      WHILE ( @Cg_Iterator = 1 ) 
            BEGIN
                  DECLARE @Conversation_Group_Id UNIQUEIDENTIFIER = NULL; 

                  GET CONVERSATION GROUP @Conversation_Group_Id	-- get a conversation group to process
		FROM User_Info_Transfer_Queue

/**************************************Track Queue messages int Message Monitor Table **********************************************************/
                  DECLARE @SBMM AS TABLE
                        ( 
                         Id INT NOT NULL
                                IDENTITY(1, 1)
                        ,Queue_Name NVARCHAR(200)
                        ,Message_Type NVARCHAR(200)
                        ,Message_Received_Ts DATETIME
                        ,Message_Sequence_Number INT
                        ,Conversation_Group_Id UNIQUEIDENTIFIER
                        ,Conversation_Handle UNIQUEIDENTIFIER
                        ,Service_name NVARCHAR(200)
                        ,Contract_name NVARCHAR(200)
                        ,Error_Text VARCHAR(MAX)
                        ,Message_Body VARBINARY(MAX)
                        ,Message_Status VARCHAR(200)
                        ,Message_Completed_Ts DATETIME
                        ,Message_Batch_Count INT )
      
                  INSERT      INTO @SBMM
                              ( Queue_Name
                              ,Message_Type
                              ,Message_Received_Ts
                              ,Message_Sequence_Number
                              ,Conversation_Group_Id
                              ,Conversation_Handle
                              ,Service_name
                              ,Contract_name
                              ,Error_Text
                              ,Message_Body
                              ,Message_Status
                              ,Message_Completed_Ts
                              ,Message_Batch_Count )
                              SELECT
                                    @QName
                                   ,Message_Type_name
                                   ,getdate() AS Message_Received_Ts
                                   ,Message_Sequence_number
                                   ,[Conversation_Group_Id]
                                   ,[Conversation_Handle]
                                   ,[Service_Name]
                                   ,[Service_Contract_Name]
                                   ,'' AS [error_message]
                                   ,[Message_Body]
                                   ,'In Progress'
                                   ,NULL AS Message_Completed_Ts
                                   ,NULL AS Message_Batch_Count
                              FROM
                                    User_Info_Transfer_Queue WITH ( NOLOCK )
                              WHERE
                                    Conversation_Group_Id = @Conversation_Group_Id
                                    AND [Message_Body] IS NOT NULL  

                  SELECT
                        @MaxLoopcnt = count(1)
                  FROM
                        @SBMM AS s               
                  

                  WHILE ( @MaxLoopcnt >= @MinLoopcnt ) 
                        BEGIN
						
                              SELECT
                                    @QueueName = s.Queue_Name
                                   ,@MessageType = s.Message_Type
                                   ,@MessageReceivedTs = s.Message_Received_Ts
                                   ,@MessageSequenceNumber = s.Message_Sequence_Number
                                   ,@ConversationGroupId = s.Conversation_Group_Id
                                   ,@ConversationHandle = s.Conversation_Handle
                                   ,@Servicename = s.Service_name
                                   ,@Contractname = s.Contract_name
                                   ,@ErrorText = s.Error_Text
                                   ,@MessageBody = s.Message_Body
                                   ,@MessageStatus = s.Message_Status
                                   ,@MessageCompletedTs = s.Message_Completed_Ts
                                   ,@MessageBatchCount = s.Message_Batch_Count
                              FROM
                                    @SBMM AS s
                              WHERE
                                    s.Id = @MinLoopcnt
								     
								     
                              EXEC dbo.Service_Broker_Message_Monitor_Send 
                                    @Queue_Name = @QueueName
                                   ,@Message_Type = @MessageType
                                   ,@Message_Received_Ts = @MessageReceivedTs
                                   ,@Message_Sequence_Number = @MessageSequenceNumber
                                   ,@Conversation_Group_Id = @ConversationGroupId
                                   ,@Conversation_Handle = @ConversationHandle
                                   ,@Service_name = @Servicename
                                   ,@Contract_name = @Contractname
                                   ,@Error_Text = @ErrorText
                                   ,@Message_Body = @MessageBody
                                   ,@Message_Status = @MessageStatus
                                   ,@Message_Completed_Ts = @MessageCompletedTs
                                   ,@Message_Batch_Count = @MessageBatchCount
                                   ,@op_Code = 'I' 
								     
                              SET @MinLoopcnt = @MinLoopcnt + 1
                        END

/*Start Processing Queue Messages********************************************************************************************************/ 
		


		
                  IF @Conversation_Group_Id IS NOT NULL 
                        BEGIN
                              BEGIN TRY 
                                    BEGIN TRANSACTION
                                    DECLARE @M_Iterator INT = 1
					 
                                    WHILE ( @M_Iterator = 1 )		-- Cycle through all messages in CG
                                          BEGIN
                                                DECLARE
                                                      @I_Conversation_Handle UNIQUEIDENTIFIER = NULL		-- Incmming message handler 
                                                     ,@I_Message_Type SYSNAME = NULL						-- Incomming Message Type
                                                     ,@I_Message VARCHAR(MAX) = NULL 					-- Incomming Message
                                                     ,@Stmt NVARCHAR(2000) = NULL						-- Statement to execute
                                                     ,@Message_Type_Exists BIT = 0 						-- Flag to validate tha tthe message type exists
                                                     ,@O_Message_Batch_Count INT
                                                     ,@I_message_sequence_number INT;
						
                                                SAVE TRANSACTION savepoint					-- Set individual save points for each message
						
						;
                                                RECEIVE TOP (1)
							@I_Conversation_Handle = Conversation_Handle
							,@I_Message_Type = Message_Type_Name
							,@I_Message = Message_Body
							,@I_message_sequence_number = Message_Sequence_Number	
						FROM 
							User_Info_Transfer_Queue
						WHERE 
							Conversation_Group_Id = @Conversation_Group_Id

                                                IF @I_Conversation_Handle IS NOT NULL -- Process message 
                                                      BEGIN
							-- Get the statement to run for this message type 
                                                            SELECT
                                                                  @Message_Type_Exists = 1
                                                                 ,@Stmt = N'EXEC ' + PROCEDURE_NAME + ' @Message, @Conversation_Handle'
                                                            FROM
                                                                  Service_Broker_Message_Procedure
                                                            WHERE
                                                                  Queue_Name = 'User_Info_Transfer_Queue'
                                                                  AND Message_Type = @I_Message_Type

                                                            IF @Message_Type_Exists = 1  -- Execute procedure for known the message type
                                                                  BEGIN 
                                                                        EXEC sp_EXECUTESQL 
                                                                              @Statement = @Stmt
                                                                             ,@params = @Param_Definition
                                                                             ,@Message = @I_message
                                                                             ,@Conversation_Handle = @I_Conversation_Handle

                                                                        IF @I_message IS NOT NULL -- we wont track messages whose message is empty
                                                                              BEGIN
																		-- Update the Processed Message to Completed status  
                                                                                    SET @CurrentDateTime = getdate()
								
                                                                                    EXEC dbo.Service_Broker_Message_Monitor_Send 
                                                                                          @Queue_Name = @QName
                                                                                         ,@Message_Type = @I_Message_Type
                                                                                         ,@Message_Received_Ts = NULL
                                                                                         ,@Message_Sequence_Number = @I_message_sequence_number
                                                                                         ,@Conversation_Group_Id = @Conversation_Group_Id
                                                                                         ,@Conversation_Handle = @I_Conversation_Handle
                                                                                         ,@Service_name = NULL
                                                                                         ,@Contract_name = NULL
                                                                                         ,@Error_Text = ''
                                                                                         ,@Message_Body = NULL
                                                                                         ,@Message_Status = 'Completed'
                                                                                         ,@Message_Completed_Ts = @CurrentDateTime
                                                                                         ,@Message_Batch_Count = @O_Message_Batch_Count
                                                                                         ,@op_Code = 'U'                                                                    
                                                                              END 	


                                                                  END
                                                            ELSE 
                                                                  BEGIN 
                                                                        RAISERROR ('Unknown message Type: %s', 16, 1, @I_Message_Type)	-- if the message type is not known then thow an error
                                                                  END
                                                      END
                                                ELSE 
                                                      BEGIN															-- Quit processes when no message was recieved
                                                            SET @M_Iterator = 0 
                                                      END
                                          END
                                    COMMIT TRANSACTION		-- Commit the successfull messages in the Conversation Group
                              END TRY	
                              BEGIN CATCH
                                    DECLARE @ErrorMessage NVARCHAR(4000) = error_message()

                                    IF ( xact_state() <> -1 ) 
                                          BEGIN
                                                ROLLBACK TRANSACTION savePoint -- Rollback the active transaction to the last save 
                                                COMMIT TRANSACTION	-- Commit any messages that executed successfully
                                          END 
                                    ELSE 
                                          BEGIN
                                                ROLLBACK TRANSACTION -- Rollback complete trnasaction if it is not possible to roll back to the save point
                                          END
				
/*Record the errors into Poison Message Table************************************************************************************************/	
                                    DECLARE @msgsCount INT;
				;
                                    RECEIVE 
					Message_Sequence_Number
					,Conversation_Handle
					,SERVICE_NAME
					,Service_Contract_Name
					,Message_Type_Name
					,Message_Body
				FROM 
					User_Info_Transfer_Queue
				INTO @messages
				WHERE 
					Conversation_Group_Id = @Conversation_Group_Id

                                    SELECT
                                          @msgsCount = @@ROWCOUNT

                                    INSERT      Service_Broker_Poison_Message
                                                ( 
                                                 Queue_Name
                                                ,Message_Received_Ts
                                                ,Conversation_Group_Id
                                                ,Message_Sequence_Number
                                                ,Conversation_Handle
                                                ,SERVICE_NAME
                                                ,CONTRACT_NAME
                                                ,Message_Type
                                                ,Error_text
                                                ,MESSAGE_Body )
                                                SELECT
                                                      'User_Info_Transfer_Queue'
                                                     ,getdate()
                                                     ,@Conversation_Group_Id
                                                     ,Message_Sequence_Number
                                                     ,Conversation_Handle
                                                     ,SERVICE_NAME
                                                     ,CONTRACT_NAME
                                                     ,Message_Type
                                                     ,@ErrorMessage
                                                     ,message_Body
                                                FROM
                                                      @Messages


/*Update Message status in Message Monitor table***********************************************************************************************/	   
				   
                                    SELECT
                                          @MaxLoopcnt = max(Message_Sequence_Number)
                                    FROM
                                          @Messages
                                                                      
                                    SET @MinLoopcnt = @MaxLoopcnt - ( @msgsCount - 1 )
                                    SET @CurrentDateTime = getdate()

                                    WHILE ( @MaxLoopcnt >= @MinLoopcnt ) 
                                          BEGIN
                        
                                                SELECT
                                                      @MessageType = s.Message_Type
                                                     ,@MessageSequenceNumber = Message_Sequence_Number
                                                     ,@ConversationHandle = [Conversation_Handle]
                                                FROM
                                                      @Messages AS s
                                                WHERE
                                                      s.Message_Sequence_Number = @MinLoopcnt
                                                      AND message_Body IS NOT NULL
																
                                                IF @MessageType IS NOT NULL 
                                                      BEGIN
                                                            EXEC dbo.Service_Broker_Message_Monitor_Send 
                                                                  @Queue_Name = @QName
                                                                 ,@Message_Type = @MessageType
                                                                 ,@Message_Received_Ts = NULL
                                                                 ,@Message_Sequence_Number = @MessageSequenceNumber
                                                                 ,@Conversation_Group_Id = @Conversation_Group_Id
                                                                 ,@Conversation_Handle = @ConversationHandle
                                                                 ,@Service_name = NULL
                                                                 ,@Contract_name = NULL
                                                                 ,@Error_Text = @ErrorMessage
                                                                 ,@Message_Body = NULL
                                                                 ,@Message_Status = 'Error'
                                                                 ,@Message_Completed_Ts = @CurrentDateTime
                                                                 ,@Message_Batch_Count = NULL
                                                                 ,@op_Code = 'U'
                                                      END
                                                SET @MinLoopcnt = @MinLoopcnt + 1
                        
                                          END 
                        

                                    RAISERROR(60001, 16, 1, 'User_Info_Transfer_Queue', @errormessage) -- Raise Standard Error 
                              END CATCH
                        END
                  ELSE 
                        BEGIN
                              SET @Cg_Iterator = 0 
                        END
            END
END



GO

GRANT EXECUTE ON  [dbo].[Change_Control_Receive_Messages_For_User_Info_Transfer_Queue] TO [sb_Execute]
GO
