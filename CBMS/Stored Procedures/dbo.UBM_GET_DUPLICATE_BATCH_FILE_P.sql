SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--select * from UBM_FEED_FILE_MAP

CREATE    PROCEDURE dbo.UBM_GET_DUPLICATE_BATCH_FILE_P

@userId varchar(10),
@sessionId varchar(20),
@batchId INT


as
	set nocount on
SELECT 
	 FEED_FILE_NAME 
	from UBM_FEED_FILE_MAP (nolock)
WHERE 

	UBM_BATCH_MASTER_LOG_ID =@batchId and  
	FEED_FILE_NAME  not like '%cassimages%'
GO
GRANT EXECUTE ON  [dbo].[UBM_GET_DUPLICATE_BATCH_FILE_P] TO [CBMSApplication]
GO
