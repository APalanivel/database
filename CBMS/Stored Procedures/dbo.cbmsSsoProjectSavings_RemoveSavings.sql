SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE     PROCEDURE [dbo].[cbmsSsoProjectSavings_RemoveSavings]
	( @MyAccountId int
	, @project_id int
	, @savings_id int
	)
AS
BEGIN

	set nocount on

	  delete sso_project_savings_map
	  where sso_project_id = @project_id
	    and sso_savings_id = @savings_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoProjectSavings_RemoveSavings] TO [CBMSApplication]
GO
