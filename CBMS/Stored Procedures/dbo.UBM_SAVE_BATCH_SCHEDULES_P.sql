
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 /******  
                     
 NAME: [dbo].[UBM_SAVE_BATCH_SCHEDULES_P]                 
                      
 DESCRIPTION:  
     To update  colums from  ubm_batch_schedules  table.                      
                      
 INPUT PARAMETERS:  
                     
 Name								DataType			Default			Description  
---------------------------------------------------------------------------------------------------------------
 @ubm_id                            INT
 @day_of_week_number                INT
 @time_one                          VARCHAR(8)
 @time_two                          VARCHAR(8)
 @time_three                        VARCHAR(8)
 @time_four                         VARCHAR(8)    
                      
 OUTPUT PARAMETERS:  
                           
 Name								DataType			Default			Description  
---------------------------------------------------------------------------------------------------------------
                      
 USAGE EXAMPLES:                          
---------------------------------------------------------------------------------------------------------------  
           
 BEGIN TRAN    
 SELECT * FROM dbo.UBM_BATCH_SCHEDULES WHERE UBM_ID=7 AND DAY_OF_WEEK_NUMBER=3
 EXEC dbo.UBM_SAVE_BATCH_SCHEDULES_P 7,3,'4:00','7:00','7:00','7:00'
 SELECT * FROM dbo.UBM_BATCH_SCHEDULES WHERE UBM_ID=7 AND DAY_OF_WEEK_NUMBER=3
 ROLLBACK             
                     
 AUTHOR INITIALS:
   
 Initials				Name  
---------------------------------------------------------------------------------------------------------------
 NR                     Narayana Reddy                        
                       
 MODIFICATIONS:
                       
 Initials				Date			 Modification
---------------------------------------------------------------------------------------------------------------
 NR                     2014-03-14       Adding Header.
                                         MAINT-2661  adding and update the  Scheduled_Time_Three, Scheduled_Time_Four columns.               
                     
******/  
 CREATE  PROCEDURE [dbo].[UBM_SAVE_BATCH_SCHEDULES_P]
      @ubm_id INT
     ,@day_of_week_number INT
     ,@time_one VARCHAR(8)
     ,@time_two VARCHAR(8)
     ,@time_three VARCHAR(8)
     ,@time_four VARCHAR(8)
 AS 
 SET nocount ON  
 UPDATE
      dbo.ubm_batch_schedules
 SET  
      scheduled_time_one = @time_one
     ,scheduled_time_two = @time_two
     ,Scheduled_Time_Three = @time_three
     ,Scheduled_Time_Four = @time_four
 WHERE
      ubm_id = @ubm_id
      AND day_of_week_number = @day_of_week_number 



;
GO

GRANT EXECUTE ON  [dbo].[UBM_SAVE_BATCH_SCHEDULES_P] TO [CBMSApplication]
GO
