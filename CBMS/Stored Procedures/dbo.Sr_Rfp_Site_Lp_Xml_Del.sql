SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Sr_Rfp_Site_Lp_Xml_Del]  
     
DESCRIPTION: 
	It Deletes Sr_Rfp_Lp_Client_Approval for given 
						SR_RFP_LP_CLIENT_APPROVAL_ID

INPUT PARAMETERS:          
NAME							DATATYPE	DEFAULT		DESCRIPTION          
-------------------------------------------------------------------
@SR_RFP_LP_CLIENT_APPROVAL_ID	INT

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	Begin Tran

		EXEC Sr_Rfp_Site_Lp_Xml_Del  1772

	Rollback Tran
    
	SELECT
		a.*
	FROM
		Sr_Rfp_Site_Lp_Xml a
	WHERE
		NOT EXISTS(SELECT 1 FROM Account acc WHERE acc.SITE_ID = a.SITE_ID)
		AND NOT EXISTS(SELECT 1 FROM sr_Rfp_Lp_Send_Client c WHERE c.Sr_Rfp_Site_Lp_Xml_Id = a.Sr_Rfp_Site_Lp_Xml_Id)
    
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------
HG			Harihara Suthan G

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------          
HG		    07/26/2010	CREATED

*/

CREATE PROCEDURE dbo.Sr_Rfp_Site_Lp_Xml_Del
   (
    @Sr_Rfp_Site_Lp_Xml_Id INT
   )
AS
BEGIN

    SET NOCOUNT ON;

    DELETE
   	FROM
		dbo.Sr_Rfp_Site_Lp_Xml
	WHERE
		Sr_Rfp_Site_Lp_Xml_Id = @Sr_Rfp_Site_Lp_Xml_Id

END
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Site_Lp_Xml_Del] TO [CBMSApplication]
GO
