SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:        
 cbms_prod.dbo.GET_ACCOUNT_METERS_P        
        
DESCRIPTION:        
        
        
INPUT PARAMETERS:        
 Name   DataType  Default Description        
------------------------------------------------------------        
 @accountId      int        
        
OUTPUT PARAMETERS:        
 Name   DataType  Default Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:        
------------------------------------------------------------        
        
 EXEC dbo.GET_ACCOUNT_METERS_P 40195        
 EXEC dbo.GET_ACCOUNT_METERS_P 40198        
 EXEC dbo.GET_ACCOUNT_METERS_P 526933        
 EXEC dbo.GET_ACCOUNT_METERS_P 646608        
         
 SELECT TOP 10 mtr.ACCOUNT_ID,mat.* FROM dbo.EC_Meter_Attribute_Tracking mat         
  JOIN dbo.METER mtr ON mat.Meter_Id = mtr.METER_ID        
          
 EXEC dbo.GET_ACCOUNT_METERS_P 1077919        
 EXEC dbo.GET_ACCOUNT_METERS_P 1077922        
         
 EXEC dbo.GET_ACCOUNT_METERS_P 786435        
 EXEC dbo.GET_ACCOUNT_METERS_P 786899        
 EXEC dbo.GET_ACCOUNT_METERS_P 845123        
 EXEC dbo.GET_ACCOUNT_METERS_P 1741258        
    
        
AUTHOR INITIALS:        
 Initials Name        
------------------------------------------------------------        
 RR   Raghu Reddy        
 NR   Narayana Reddy        
 SC   Sreenivasulu Cheerala        
MODIFICATIONS        
        
 Initials Date  Modification        
------------------------------------------------------------        
          7/20/2009 Autogenerated script        
 SSR   03/25/2010 Refer commodity_ID from Commodity table.        
        
 DMR   09/10/2010 Modified for Quoted_Identifier        
 RR   2013-10-16 MAINT-2127 Added Commodity_Id in select list        
 NR   2015-05-06 For AS400-Added Has_Meter_Attributes column in select list.        
 SC   2019-11-13	Added Primary Meter in the get result set         
******/
CREATE PROCEDURE [dbo].[GET_ACCOUNT_METERS_P]
    @accountId INT
AS
    BEGIN

        SET NOCOUNT ON;
        ---To Get the count of meters based on Account and Commodity    


        DECLARE @Tbl_Is_Associated_To_Meter_Attributes TABLE
              (
                  Meter_Id INT
              );

        INSERT INTO @Tbl_Is_Associated_To_Meter_Attributes
             (
                 Meter_Id
             )
        SELECT
            mtr.METER_ID
        FROM
            dbo.METER mtr
        WHERE
            ACCOUNT_ID = @accountId
            AND EXISTS (   SELECT
                                1
                           FROM
                                dbo.EC_Meter_Attribute_Tracking mat
                           WHERE
                                mat.Meter_Id = mtr.METER_ID)
        GROUP BY
            mtr.METER_ID;

        SELECT
            a.METER_ID
            , a.RATE_ID
            , a.PURCHASE_METHOD_TYPE_ID
            , a.ADDRESS_ID
            , a.METER_NUMBER
            , a.TAX_EXEMPT_STATUS
            , b.RATE_NAME
            , com.Commodity_Name AS entity_name
            , d.ADDRESS_LINE1
            , d.ADDRESS_LINE2
            , a.TAX_EXEMPTION_ID
            , com.Commodity_Id
            , d.STATE_ID
            , CASE WHEN iama.Meter_Id IS NOT NULL THEN 1
                  ELSE 0
              END AS Is_Associated_To_Meter_Attributes
            , CASE WHEN ma.EC_Meter_Attribute_Id IS NOT NULL THEN 1
                  ELSE 0
              END AS Meter_Attributes_Available
            , CASE WHEN (acpm.Meter_Id IS NOT NULL) THEN 1
                  ELSE 0
              END AS Is_Primary
        FROM
            dbo.METER a
            INNER JOIN dbo.RATE b
                ON b.RATE_ID = a.RATE_ID
            INNER JOIN dbo.ADDRESS d
                ON d.ADDRESS_ID = a.ADDRESS_ID
            INNER JOIN dbo.Commodity com
                ON com.Commodity_Id = b.COMMODITY_TYPE_ID
            LEFT JOIN @Tbl_Is_Associated_To_Meter_Attributes iama
                ON a.METER_ID = iama.Meter_Id
            LEFT JOIN dbo.EC_Meter_Attribute ma
                ON b.COMMODITY_TYPE_ID = ma.Commodity_Id
                   AND  d.STATE_ID = ma.State_Id
            LEFT JOIN [Budget].[Account_Commodity_Primary_Meter] acpm
                ON acpm.Account_Id = a.ACCOUNT_ID
                   AND  acpm.Commodity_Id = com.Commodity_Id
                   AND  acpm.Meter_Id = a.METER_ID
        WHERE
            a.ACCOUNT_ID = @accountId
        GROUP BY
            a.METER_ID
            , a.RATE_ID
            , a.PURCHASE_METHOD_TYPE_ID
            , a.ADDRESS_ID
            , a.METER_NUMBER
            , a.TAX_EXEMPT_STATUS
            , b.RATE_NAME
            , com.Commodity_Name
            , d.ADDRESS_LINE1
            , d.ADDRESS_LINE2
            , a.TAX_EXEMPTION_ID
            , com.Commodity_Id
            , d.STATE_ID
            , CASE WHEN iama.Meter_Id IS NOT NULL THEN 1
                  ELSE 0
              END
            , CASE WHEN ma.EC_Meter_Attribute_Id IS NOT NULL THEN 1
                  ELSE 0
              END
            , CASE WHEN (acpm.Meter_Id IS NOT NULL) THEN 1
                  ELSE 0
              END;
    END;
GO


GRANT EXECUTE ON  [dbo].[GET_ACCOUNT_METERS_P] TO [CBMSApplication]
GO
