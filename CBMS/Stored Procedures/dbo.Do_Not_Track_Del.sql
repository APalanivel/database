SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Do_Not_Track_Del]  
     
DESCRIPTION: 
	It Deletes Do Not Track Details for Selected Account_id.
      
INPUT PARAMETERS:          
NAME					DATATYPE	DEFAULT		DESCRIPTION          
---------------------------------------------------------------          
@Account_Id				INT	

   
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------ 

	BEGIN TRAN  
		EXEC Do_Not_Track_Del  11304
	ROLLBACK TRAN
	
	SELECT * FROM do_not_track WHERE account_id = 11304

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			27-MAY-10	CREATED

*/

CREATE PROCEDURE dbo.Do_Not_Track_Del
   (
    @Account_Id INT
   )
AS 
BEGIN

    SET NOCOUNT ON ;
   
    DELETE 
   	FROM	
		dbo.DO_NOT_TRACK
	WHERE 
		Account_ID = @Account_Id
END
GO
GRANT EXECUTE ON  [dbo].[Do_Not_Track_Del] TO [CBMSApplication]
GO
