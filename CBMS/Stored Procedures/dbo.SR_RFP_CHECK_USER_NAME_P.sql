SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--exec SR_RFP_CHECK_USER_NAME_P 'NewOne'
CREATE PROCEDURE [DBO].[SR_RFP_CHECK_USER_NAME_P]  

@userName varchar(30)

AS
	set nocount on
	select count(1) from user_info where username = @userName
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_CHECK_USER_NAME_P] TO [CBMSApplication]
GO
