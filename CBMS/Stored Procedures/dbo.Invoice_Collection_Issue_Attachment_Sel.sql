SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                        
 NAME: dbo.Invoice_Collection_Issue_Attachment_Sel            
                        
 DESCRIPTION:                        
			To get Invoice_Collection_Issue_Attachment      
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@Invoice_Collection_Issue_Id INT

                             
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            

    exec [dbo].[Invoice_Collection_Issue_Attachment_Sel] 1                       
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2016-11-21       Created for Invoice Tracking               
                       
******/    

CREATE PROCEDURE [dbo].[Invoice_Collection_Issue_Attachment_Sel]
      ( 
       @Invoice_Collection_Issue_Id INT )
AS 
BEGIN                
      SET NOCOUNT ON;  

      

      SELECT
            ci.CBMS_DOC_ID AS DocName
           ,ci.CBMS_IMAGE_ID AS ImageId
      FROM
            dbo.Invoice_Collection_Issue_Attachment ica
            INNER JOIN dbo.cbms_image ci
                  ON ica.Cbms_Image_Id = ci.CBMS_IMAGE_ID
      WHERE
            ica.Invoice_Collection_Issue_Log_Id = @Invoice_Collection_Issue_Id

END




;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Issue_Attachment_Sel] TO [CBMSApplication]
GO
