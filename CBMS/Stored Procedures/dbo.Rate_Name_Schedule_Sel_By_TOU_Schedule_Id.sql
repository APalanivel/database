SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********  
NAME:  dbo.Rate_Name_Schedule_Sel_By_TOU_Schedule_Id  
  
DESCRIPTION:    
  
Used to select Rate Name and Rate Schedule and tou schedule associated to Tou Schedule Id  
  
INPUT PARAMETERS:  
      Name						DataType          Default     Description  
------------------------------------------------------------  
	 @Time_Of_Use_Schedule_Id   INT  
      
  
OUTPUT PARAMETERS:  
      Name						DataType          Default     Description  
------------------------------------------------------------  
  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
  
 Exec dbo.Rate_Name_Schedule_Sel_By_TOU_Schedule_Id 3  
  
  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 BCH	  Balaraju  
  
  
 Initials Date  Modification  
------------------------------------------------------------  
 BCH  2012-07-12 Created
 BCH  2012-09-06 Formated the code as per summit standards.
  
******/  
CREATE PROCEDURE dbo.Rate_Name_Schedule_Sel_By_TOU_Schedule_Id
      ( 
       @Time_Of_Use_Schedule_Id INT )
AS 
BEGIN  
  
      SET NOCOUNT ON;  
      SELECT
            r.RATE_NAME
           ,r.RATE_ID
           ,rs.RS_START_DATE
           ,rs.RS_END_DATE
           ,rs.RATE_SCHEDULE_ID
           ,tous.Time_Of_Use_Schedule_Id
           ,tous.Schedule_Name
      FROM
            dbo.Time_Of_Use_Schedule tous
            JOIN dbo.Time_Of_Use_Schedule_Term term
                  ON tous.Time_Of_Use_Schedule_Id = term.Time_Of_Use_Schedule_Id
            JOIN dbo.RATE_SCHEDULE rs
                  ON rs.Time_Of_Use_Schedule_Term_Id = term.Time_Of_Use_Schedule_Term_Id
            JOIN dbo.RATE r
                  ON r.RATE_ID = rs.RATE_ID
      WHERE
            tous.Time_Of_Use_Schedule_Id = @Time_Of_Use_Schedule_Id
      ORDER BY
            rs.RS_START_DATE ASC
               
END 
;
GO
GRANT EXECUTE ON  [dbo].[Rate_Name_Schedule_Sel_By_TOU_Schedule_Id] TO [CBMSApplication]
GO
