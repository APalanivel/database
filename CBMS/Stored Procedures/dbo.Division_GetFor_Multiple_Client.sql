SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                        
 NAME: dbo.Invoice_Collection_Account_Config_Bulk_Upload_Sel            
                        
 DESCRIPTION:                        
			To get the details of Invoice_Collection_Account_Config             
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
Division_GetFor_Multiple_Client 123,'14525,14570'
                            
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
--------------------------------------------------------------------------------------------------------------- 

AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
SLP					Sri Lakshmi Pallikonda
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SLP				   2019-10-23		created
*********/
CREATE PROCEDURE [dbo].[Division_GetFor_Multiple_Client]
     (
         @MyAccountId INT
         , @client_id VARCHAR(MAX)
     )
AS
    BEGIN


        SELECT
            d.Division_Id
            , d.Division_Name
        FROM
            Division d WITH (NOLOCK)
        WHERE
            --client_id = @client_id

            (   @client_id IS NULL
                OR  EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.ufn_split(@client_id, ',') us
                               WHERE
                                    us.Segments = d.Client_Id))
        ORDER BY
            d.Division_Name;


    END;


GO
GRANT EXECUTE ON  [dbo].[Division_GetFor_Multiple_Client] TO [CBMSApplication]
GO
