SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Utility_Dtl_Election_Sel

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Vendor_ID				INT
	@Commodity_Id			INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC Utility_Dtl_Election_Sel 30,290
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	SKA			01/12/2011	Created
******/

CREATE PROC [dbo].[Utility_Dtl_Election_Sel]
      ( 
       @Vendor_ID INT
      ,@Commodity_Id INT )
AS 
BEGIN
      SET NOCOUNT ON
	
      SELECT
            utl_el.Utility_Dtl_Election_Id
           ,utl_el.Election_Name
           ,utl_el.Enrollment_Period
           ,utl_el.Notification_Deadline
           ,utl_el.Other_Stipulation
           ,utl_el.Comment_ID
           ,cmt.Comment_Text
      FROM
            dbo.VENDOR_COMMODITY_MAP vcm
            JOIN dbo.Utility_Dtl_Election utl_el
                  ON vcm.VENDOR_COMMODITY_MAP_ID = utl_el.VENDOR_COMMODITY_MAP_ID
            LEFT JOIN dbo.Comment cmt
                  ON cmt.Comment_ID = utl_el.Comment_ID
      WHERE
            vcm.VENDOR_ID = @Vendor_ID
            AND vcm.COMMODITY_TYPE_ID = @Commodity_Id

END
GO
GRANT EXECUTE ON  [dbo].[Utility_Dtl_Election_Sel] TO [CBMSApplication]
GO
