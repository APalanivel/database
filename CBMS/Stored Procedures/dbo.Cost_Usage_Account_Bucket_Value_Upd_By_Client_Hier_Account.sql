SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
     [dbo].Cost_Usage_Account_Bucket_Value_Upd_By_Client_Hier_Account   
  
DESCRIPTION:  
	Updating account bucket values by Account Id.
	Using for aggregation.
        
INPUT PARAMETERS:  
Name				    DataType          Default     Description  
-----------------------------------------------------------  
@Account_Id		    INT
@Site_Client_Hier_Id    INT
     
OUTPUT PARAMETERS:  
Name			DataType          Default     Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------      
begin tran

exec dbo.Cost_Usage_Account_Bucket_Value_Upd_By_Client_Hier_Account 440819, 1204

select top 10 * from Cost_Usage_Account_Dtl where account_id = 440819 and Client_Hier_Id = 1204

rollback tran
        
AUTHOR INITIALS:  
 Initials   Name   
------------------------------------------------------------  
 BCH		  Balaraju     
 AP		  Athmaram Pabbathi
                    
MODIFICATIONS:  
 Initials	  Date        Modification  
------------------------------------------------------------  
 AP		  2012-03-28  Created to replace dbo.Cost_Usage_Account_Bucket_Value_Upd_By_Account
******/
CREATE PROCEDURE dbo.Cost_Usage_Account_Bucket_Value_Upd_By_Client_Hier_Account
      ( 
       @Account_Id INT
      ,@Site_Client_Hier_Id INT )
AS 
BEGIN    
      SET NOCOUNT ON ;    
      
      UPDATE
            cuad
      SET   
            cuad.Bucket_Value = 0
      FROM
            dbo.Cost_Usage_Account_Dtl cuad
      WHERE
            cuad.ACCOUNT_ID = @Account_Id
            AND cuad.Client_Hier_Id = @Site_Client_Hier_Id
END
;
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Bucket_Value_Upd_By_Client_Hier_Account] TO [CBMSApplication]
GO
