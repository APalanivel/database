SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
   
 dbo.CBMS_ADD_SITE_P  
   
 DESCRIPTION:     
   
 This procedure used to save the Site added to the division.  
    
   
 INPUT PARAMETERS:    
 Name               DataType          Default        Description    
------------------------------------------------------------      
@SITE_TYPE_ID           INT,  
@SITE_NAME              VARCHAR(200),  
@DIVISION_ID            INT,  
@UBMSITE_ID             VARCHAR(30),  
@SITE_REFERENCE_NUMBER  VARCHAR(30),  
@SITE_PRODUCT_SERVICE   VARCHAR(2000),  
@PRODUCTION_SCHEDULE    VARCHAR(2000),  
@SHUTDOWN_SCHEDULE      VARCHAR(2000),  
@IS_ALTERNATE_POWER     BIT,  
@IS_ALTERNATE_GAS       BIT,  
@DOING_BUSINESS_AS      VARCHAR(200),  
@NAICS_CODE             VARCHAR(30),  
@TAX_NUMBER             VARCHAR(30),  
@DUNS_NUMBER            VARCHAR(30),  
@CLOSED                 BIT,  
@NOT_MANAGED            BIT,  
@CONTRACTING_ENTITY     VARCHAR(200),  
@CLIENT_LEGAL_STRUCTURE VARCHAR(4000),  
@firstName              VARCHAR(50),  
@lastName               VARCHAR(50),  
@contactEmailId         VARCHAR(50),  
@emailCC                VARCHAR(400),  
@emailApproval          BIT,  
@dbViewApproval         BIT,  
@Client_Id              INT,  
@Analyst_Mapping_Cd  INT  
   
 OUTPUT PARAMETERS:    
 Name            DataType  Default Description    
------------------------------------------------------------    
 @siteId        INT   
  
  
  USAGE EXAMPLES:    
------------------------------------------------------------    
   
  
AUTHOR INITIALS:    
 Initials Name    
------------------------------------------------------------    
 HG		Hari      
 AKR    Ashok Kumar Raju    
 TP		Anoop
 MSV	Muhamed Shahid V
 MODIFICATIONS     
 Initials Date  Modification    
------------------------------------------------------------    
 DR   07/06/2009 modified for the sitegroup division table split and implementation of SV replication        
 HG   7/7/2009 Client_Id param added to save client_id in Site table.  
 AKR   2012-09-18  Added Analyst_Maping_Cd Column    
 AKR   2012-10-09  Modified Code to Standards
 TP		7/3/2014	Modifed for blacksotne Portfolio
 MSV	16-AUG-2019 Modified for Client Data Transfer : Parameters Added  @Weather_Station_Code, @Time_Zone_Id
******/  
CREATE PROCEDURE [dbo].[CBMS_ADD_SITE_P]
      (
       @SITE_TYPE_ID INT
      ,@SITE_NAME VARCHAR(200)
      ,@DIVISION_ID INT
      ,@UBMSITE_ID VARCHAR(30)
      ,@SITE_REFERENCE_NUMBER VARCHAR(30)
      ,@SITE_PRODUCT_SERVICE VARCHAR(2000)
      ,@PRODUCTION_SCHEDULE VARCHAR(2000)
      ,@SHUTDOWN_SCHEDULE VARCHAR(2000)
      ,@IS_ALTERNATE_POWER BIT
      ,@IS_ALTERNATE_GAS BIT
      ,@DOING_BUSINESS_AS VARCHAR(200)
      ,@NAICS_CODE VARCHAR(30)
      ,@TAX_NUMBER VARCHAR(30)
      ,@DUNS_NUMBER VARCHAR(30)
      ,@CLOSED BIT
      ,@NOT_MANAGED BIT
      ,@CONTRACTING_ENTITY VARCHAR(200)
      ,@CLIENT_LEGAL_STRUCTURE VARCHAR(4000)
      ,@firstName VARCHAR(50)
      ,@lastName VARCHAR(50)
      ,@contactEmailId VARCHAR(50)
      ,@emailCC VARCHAR(400)
      ,@emailApproval BIT
      ,@dbViewApproval BIT
      ,@Client_Id INT
      ,@Analyst_Mapping_Cd INT
      ,@siteId INT OUT 
	  ,@Portfolio_Client_Hier_Reference_Number INT = NULL
	  ,@Weather_Station_Code VARCHAR(10) = NULL
	  ,@Time_Zone_Id INT = NULL
	  )
AS
BEGIN  
   
      SET NOCOUNT ON  

      INSERT      INTO dbo.SITE
                  (SITE_TYPE_ID
                  ,SITE_NAME
                  ,DIVISION_ID
                  ,UBMSITE_ID
                  ,SITE_REFERENCE_NUMBER
                  ,SITE_PRODUCT_SERVICE
                  ,PRODUCTION_SCHEDULE
                  ,SHUTDOWN_SCHEDULE
                  ,IS_ALTERNATE_POWER
                  ,IS_ALTERNATE_GAS
                  ,DOING_BUSINESS_AS
                  ,NAICS_CODE
                  ,TAX_NUMBER
                  ,DUNS_NUMBER
                  ,CLOSED
                  ,NOT_MANAGED
                  ,CONTRACTING_ENTITY
                  ,CLIENT_LEGAL_STRUCTURE
                  ,LP_CONTACT_FIRST_NAME
                  ,LP_CONTACT_LAST_NAME
				  ,LP_CONTACT_EMAIL_ADDRESS
                  ,LP_CONTACT_CC
                  ,IS_PREFERENCE_BY_EMAIL
                  ,IS_PREFERENCE_BY_DV
                  ,Client_Id
                  ,Analyst_Mapping_Cd
                  ,Portfolio_Client_Hier_Reference_Number
				  ,Weather_Station_Code
				  ,Time_Zone_Id )
      VALUES
                  (@SITE_TYPE_ID
                  ,@SITE_NAME
                  ,@DIVISION_ID
                  ,@UBMSITE_ID
                  ,@SITE_REFERENCE_NUMBER
                  ,@SITE_PRODUCT_SERVICE
                  ,@PRODUCTION_SCHEDULE
                  ,@SHUTDOWN_SCHEDULE
                  ,@IS_ALTERNATE_POWER
                  ,@IS_ALTERNATE_GAS
                  ,@DOING_BUSINESS_AS
                  ,@NAICS_CODE
                  ,@TAX_NUMBER
                  ,@DUNS_NUMBER
                  ,@CLOSED
                  ,@NOT_MANAGED
                  ,@CONTRACTING_ENTITY
                  ,@CLIENT_LEGAL_STRUCTURE
                  ,@firstName
                  ,@lastName
                  ,@contactEmailId
                  ,@emailCC
                  ,@emailApproval
                  ,@dbViewApproval
                  ,@Client_Id
                  ,@Analyst_Mapping_Cd
                  ,@Portfolio_Client_Hier_Reference_Number
				  ,@Weather_Station_Code
				  ,@Time_Zone_Id )
      SELECT
            @siteId = scope_identity()  
  
      RETURN  
  
END;
GO


GRANT EXECUTE ON  [dbo].[CBMS_ADD_SITE_P] TO [CBMSApplication]
GO
