SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







/******        
NAME:      
 dbo.Variance_Failed_Test_Details_Sel_By_Account_Commodity

DESCRIPTION:
		Used to select variance failed test dtls for the given account , commodity and service month
		
 INPUT PARAMETERS:      
	Name			DataType  Default Description      
------------------------------------------------------------      
	@Account_Id	INT
	@Commodity_Id	INT		NULL
	@Service_Month	DATE
	
 OUTPUT PARAMETERS:     
 Name   DataType  Default Description      
------------------------------------------------------------      
 USAGE EXAMPLES:      
------------------------------------------------------------
	EXEC dbo.Variance_Failed_Test_Details_Sel_By_Account_Commodity 34054, 290, '1/1/2010'
	EXEC dbo.Variance_Failed_Test_Details_Sel_By_Account_Commodity 34054, NULL, '1/1/2010'


	EXEC dbo.Variance_Failed_Test_Details_Sel_By_Account_Commodity 399225, 67 , '2007-07-01'
	EXEC dbo.Variance_Failed_Test_Details_Sel_By_Account_Commodity 341032,100028,'2007-07-01'
	
	 SELECT DISTINCT TOP 100
		  vl.Account_Id 
		  ,vl.Service_Month
		  ,vl.Commodity_Id
	 FROM
		  Variance_Log vl
		  JOIN Variance_Closed_Reason vcr
				ON vcr.Account_Id = vl.Account_Id
				   AND vcr.Service_Month = vl.Service_Month
	 WHERE
		  vl.is_Failure = 1 
		  AND vl.closed_Dt iS NOT NULL
		  AND vl.Commodity_ID NOT IN (290,291)
	
	
 AUTHOR INITIALS:
 Initials	Name
------------------------------------------------------------
 HG		Hari
 AP		Athmaram Pabbathi
 AP1	Arunkumar Palanivel
 
 MODIFICATIONS
 Initials	     Date		     Modification
------------------------------------------------------------
 HG			03/17/2010	Created
 HG			04/08/2010	CTE created on Variance_Log and Self Join with Variance_Log table is removed as results can derived directly.
 HG			04/16/2010	Default value (NULL) set for @Commodity_id parameter, so that this procedure can be used to get all the variance entries logged for the given account and service month.
 HG			05/06/2010	added local parameter to control the parameter sniffing with in the procedure.
 HG			05/10/2010	Usage examples added.
 AP			08/18/2011	Removed COST_USAGE_ID column as part of ADLDT changes.
 AKR        2013-07-16  Modified for Detailed Variance, Added UOM_Id and UOM_Name
 AP1		Feb 20,2020	Modified procedure for AVT							
******/

CREATE PROCEDURE [dbo].[Variance_Failed_Test_Details_Sel_By_Account_Commodity]
      (
      @Account_Id    INT
    , @Commodity_Id  INT = NULL
    , @Service_Month DATE )
AS
      BEGIN

            SET NOCOUNT ON;

            DECLARE
                  @Account_Id_lv    INT  = @Account_Id
                  -- ,@Commodity_Id_lv INT	= @Commodity_id
                , @Service_Month_lv DATE = @Service_Month;

            WITH Cte_Closed_Reason_Type
            AS ( SELECT
                              vcr.Account_Id
                            , vcr.Service_Month
                            , vcr.Closed_DT
                            , (     SELECT
                                                cast(c1.Code_Dsc AS VARCHAR(MAX)) + '|' + cast(c.Code_Dsc AS VARCHAR(MAX)) + ','
                                    FROM        dbo.Variance_Closed_Reason vrt
                                                JOIN
                                                dbo.Variance_Closed_Reason_Category vrc
                                                      ON vrt.Reason_Cd = vrc.Closed_Reason_Id
                                                JOIN
                                                dbo.Code c
                                                      ON c.Code_Id = vrc.Closed_Reason_Cd
                                                JOIN
                                                dbo.Code c1
                                                      ON c1.Code_Id = vrc.Closure_Category_Cd
                                    WHERE       ( vrt.Account_Id = @Account_Id_lv )
                                                --AND ( @Commodity_Id_lv IS NULL
                                                --      OR rt.Commodity_Id = @Commodity_Id_lv )
                                                AND   ( vrt.Service_Month = @Service_Month_lv )
                                                AND   ( vrt.Closed_DT = vcr.Closed_DT )
                                    GROUP BY    c.Code_Dsc
                                              , c1.Code_Dsc
                                    FOR XML PATH('')) AS Closed_reason_type
                 FROM         dbo.Variance_Closed_Reason vcr
                 WHERE        ( vcr.Account_Id = @Account_Id_lv )
                              --AND ( @Commodity_Id IS NULL
                              --      OR vcr.Commodity_Id = @Commodity_Id_lv )
                              AND   ( vcr.Service_Month = @Service_Month_lv )
                 GROUP BY     vcr.Account_Id
                            , vcr.Service_Month
                            , vcr.Closed_DT )
            SELECT
                        vl.Variance_Log_Id
                      , substring(cr.Closed_reason_type, 1, len(cr.Closed_reason_type) - 1) AS closed_reason_type
                      , vl.Variance_Status_Cd
                      , vl.Variance_Status_Desc AS variance_status_type
                      , vl.Category_Name
                      , isnull(vl.Current_Value, 0) AS current_value
                      , vl.Test_Description
                      , isnull(vl.Baseline_Value, 0) AS baseline_value
                      , isnull(vl.Low_Tolerance, 0) AS low_tolerance
                      , isnull(vl.High_Tolerance, 0) AS high_tolerance
                      , vl.Is_Failure
                      , vl.Execution_Dt
                      , vl.Closed_Dt
                      , vl.Commodity_ID
                      , vl.Is_Inclusive
                      , vl.Is_Multiple_Condition
                      , vl.Closed_By_User_Name
                      , Ref_Variance_Log_Id = isnull(vl.Ref_Variance_Log_Id, vl.Variance_Log_Id)
                      , vl.UOM_Cd UOM_Id
                      , vl.UOM_Label UOM_Name
            FROM        dbo.Variance_Log vl
                        JOIN
                        Cte_Closed_Reason_Type cr
                              ON cr.Account_Id = vl.Account_ID
                                 AND cr.Service_Month = vl.Service_Month
                                 AND cr.Closed_DT = vl.Closed_Dt
            WHERE       ( vl.Account_ID = @Account_Id_lv )
                        --AND ( @Commodity_Id_lv IS NULL
                        --      OR vl.Commodity_ID = @Commodity_Id_lv )
                        AND   ( vl.Service_Month = @Service_Month_lv )
                        AND   ( vl.Closed_Dt IS NOT NULL )
                        AND   ( vl.Is_Failure = 1 )
            GROUP BY    vl.Variance_Log_Id
                      , cr.Closed_reason_type
                      , vl.Variance_Status_Cd
                      , vl.Variance_Status_Desc
                      , vl.Category_Name
                      , isnull(vl.Current_Value, 0)
                      , vl.Test_Description
                      , isnull(vl.Baseline_Value, 0)
                      , isnull(vl.Low_Tolerance, 0)
                      , isnull(vl.High_Tolerance, 0)
                      , vl.Is_Failure
                      , vl.Execution_Dt
                      , vl.Closed_Dt
                      , vl.Commodity_ID
                      , vl.Is_Inclusive
                      , vl.Is_Multiple_Condition
                      , vl.Closed_By_User_Name
                      , vl.Ref_Variance_Log_Id
                      , vl.Commodity_ID
                      , vl.UOM_Cd
                      , vl.UOM_Label
            ORDER BY    vl.Closed_Dt;

      END;
      ;
GO


GRANT EXECUTE ON  [dbo].[Variance_Failed_Test_Details_Sel_By_Account_Commodity] TO [CBMSApplication]
GO
