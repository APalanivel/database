SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
                           
 NAME: dbo.Client_App_Access_Role_Exist_By_Client_Role_Name                      
                            
 DESCRIPTION:        
   Check weather role name  Exist or Not in the client app access role table .                            
                            
 INPUT PARAMETERS:        
                           
 Name                               DataType          Default       Description        
---------------------------------------------------------------------------------------------------------------      
 @Client_Id                         INT   
 @App_Access_Role_Name              NVARCHAR(255)         
                            
 OUTPUT PARAMETERS:        
                                 
 Name                               DataType          Default       Description        
---------------------------------------------------------------------------------------------------------------      
                            
 USAGE EXAMPLES:                                
---------------------------------------------------------------------------------------------------------------     
     
 EXEC dbo.Client_App_Access_Role_Exist_By_Client_Role_Name 110,'All Managers Edit 1'   
 EXEC dbo.Client_App_Access_Role_Exist_By_Client_Role_Name 110,'Admins'   
    
 AUTHOR INITIALS:      
         
 Initials                   Name        
---------------------------------------------------------------------------------------------------------------      
 NR                         Narayana Reddy                              
                             
 MODIFICATIONS:      
                             
 Initials               Date            Modification      
---------------------------------------------------------------------------------------------------------------      
 NR                     2014-02-03      Created for RA Admin user management.                          
                           
******/     
  
CREATE PROCEDURE [dbo].[Client_App_Access_Role_Exist_By_Client_Role_Name]
      ( 
       @Client_Id INT
      ,@App_Access_Role_Name NVARCHAR(255) )
AS 
BEGIN    
        
      SET NOCOUNT ON;    
      DECLARE @App_Access_Role_Name_Exist BIT = 0  
  
      SELECT
            @App_Access_Role_Name_Exist = 1
      FROM
            dbo.Client_App_Access_Role
      WHERE
            CLIENT_ID = @Client_Id
            AND App_Access_Role_Name = @App_Access_Role_Name    
        
      SELECT
            @App_Access_Role_Name_Exist  
          
END  
  

;
GO
GRANT EXECUTE ON  [dbo].[Client_App_Access_Role_Exist_By_Client_Role_Name] TO [CBMSApplication]
GO
