SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE        PROCEDURE [dbo].[cbmsInvProKarmaNotReported_Save]
	( @MyAccountId int 
	, @inv_prokarma_not_reported_id int = null
	, @inv_prokarma_not_reported_batch_id int 
	, @source_file varchar(200)
	, @client_name varchar(200) = null
	, @city_state varchar(500) = null
	, @vendor_name varchar(200) = null
	, @account_number varchar(200) = null
	, @bill_month datetime = null
	, @barcode varchar(200) 
	, @bill_status varchar(200) = null
	, @remarks varchar(2000) = null
	)
AS
BEGIN

	declare @this_id int

	set nocount on

	  declare @inv_sourced_image_id int
		, @count int
		, @length int
		, @char char(1)
		, @is_num bit

	set @length = charindex('_', @barcode) - 1

	if (@length > 0 and @length < 10)
	begin
		set @is_num = 1
		set @count = 1

		while (@count <= @length)
		begin
			set @char = substring(@barcode, @count, 1)			
			if not (ascii(@char) between 48 and 57)
			begin
				set @is_num = 0
				set @count = @length
			end
			set @count = @count + 1
		end -- end while

		if @is_num = 1
		begin
			set @inv_sourced_image_id = convert(int, left(@barcode, @length))
		end

	end -- end length


	insert into inv_prokarma_not_reported
		( inv_prokarma_not_reported_batch_id
		, source_file
		, inv_sourced_image_id
		, client_name
		, city_state
		, vendor_name
		, account_number
		, bill_month
		, barcode
		, bill_status
		, remarks
		)
	values
		( @inv_prokarma_not_reported_batch_id
		, @source_file
		, @inv_sourced_image_id
		, @client_name
		, @city_state
		, @vendor_name
		, @account_number
		, @bill_month
		, @barcode
		, @bill_status
		, @remarks
		)

	set @this_id = @@IDENTITY

--	set nocount off

	exec cbmsInvProKarmaNotReported_Get @MyAccountId, @this_id


END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvProKarmaNotReported_Save] TO [CBMSApplication]
GO
