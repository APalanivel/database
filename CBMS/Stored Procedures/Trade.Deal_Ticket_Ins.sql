SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        Trade.Deal_Ticket_Ins                    
                      
Description:                      
        To load forecast volumes for on-boarded sites
                      
Input Parameters:                      
    Name							DataType        Default     Description                        
--------------------------------------------------------------------------------
	@RM_Client_Hier_Onboard_Id		INT
    @Batch_Status_Cd				INT
    @User_Info_Id					INT
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	  
                    
Usage Examples:                          
--------------------------------------------------------------------------------
	DECLARE @Deal_Ticket_Id INT
	EXEC Trade.Deal_Ticket_Ins 235,291,1,'2018-11-14','2018-11-14',1,1,1,1,1,1,1,1,1,1,1,1,1,1,@Deal_Ticket_Id OUT
	SELECT * FROM Trade.Deal_Ticket		
	SELECT @Deal_Ticket_Id
		                     
 Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy   
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			2018-11-14     Global Risk Management - Created 
                     
******/
CREATE PROCEDURE [Trade].[Deal_Ticket_Ins]
    (
        @Client_Id INT
        , @Commodity_Id INT
        , @Is_Client_Generated BIT
        , @Hedge_Start_Dt DATE
        , @Hedge_End_Dt DATE
        , @Hedge_Type_Cd INT
        , @Contract_Id VARCHAR(MAX) = NULL
        , @Hedge_Allocation_Type_Cd INT
        , @Price_Index_Id INT
        , @Currency_Unit_Id INT
        , @Uom_Type_Id INT
        , @Deal_Ticket_Frequency_Cd INT
        , @Trade_Pricing_Option_Cd INT
        , @Trade_Action_Type_Cd INT
        , @Deal_Ticket_Type_Cd INT
        , @Deal_Status_Cd INT
        , @Workflow_Id INT
        , @Is_Individual_Site_Pricing_Required BIT
        , @Comment_Id INT = NULL
        , @Is_Bid BIT
        , @User_Info_Id INT
        , @Deal_Ticket_Id INT OUT
        , @Hedge_Mode_Type_Id INT = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        --ALTER TABLE [Trade].[Deal_Ticket] DROP CONSTRAINT [un_Deal_Ticket__Deal_Ticket_Number] UNIQUE NONCLUSTERED  ([Deal_Ticket_Number]) ON [DB_DATA01]

        DECLARE @Fixed_Dt DATE = NULL;

        SELECT
            @Hedge_Mode_Type_Id = ENTITY_ID
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_NAME = 'Index'
            AND ENTITY_DESCRIPTION = 'HEDGE_MODE_TYPE'
            AND @Hedge_Mode_Type_Id IS NULL;


        INSERT INTO Trade.Deal_Ticket
             (
                 Client_Id
                 , Deal_Ticket_Number
                 , Commodity_Id
                 , Is_Client_Generated
                 , Hedge_Start_Dt
                 , Hedge_End_Dt
                 , Hedge_Type_Cd
                 , Hedge_Allocation_Type_Cd
                 , Price_Index_Id
                 , Currency_Unit_Id
                 , Uom_Type_Id
                 , Deal_Ticket_Frequency_Cd
                 , Trade_Pricing_Option_Cd
                 , Trade_Action_Type_Cd
                 , Deal_Ticket_Type_Cd
                 , Deal_Status_Cd
                 , Workflow_Id
                 , Is_Individual_Site_Pricing_Required
                 , Comment_Id
                 , Created_User_Id
                 , Created_Ts
                 , Updated_User_Id
                 , Last_Change_Ts
                 , Is_Bid
                 , Hedge_Mode_Type_Id
             )
        VALUES
            (@Client_Id
             , -1
             , @Commodity_Id
             , @Is_Client_Generated
             , @Hedge_Start_Dt
             , @Hedge_End_Dt
             , @Hedge_Type_Cd
             , @Hedge_Allocation_Type_Cd
             , @Price_Index_Id
             , @Currency_Unit_Id
             , @Uom_Type_Id
             , @Deal_Ticket_Frequency_Cd
             , @Trade_Pricing_Option_Cd
             , @Trade_Action_Type_Cd
             , @Deal_Ticket_Type_Cd
             , @Deal_Status_Cd
             , @Workflow_Id
             , @Is_Individual_Site_Pricing_Required
             , @Comment_Id
             , @User_Info_Id
             , GETDATE()
             , @User_Info_Id
             , GETDATE()
             , @Is_Bid
             , @Hedge_Mode_Type_Id);

        SELECT  @Deal_Ticket_Id = SCOPE_IDENTITY();

        UPDATE
            Trade.Deal_Ticket
        SET
            Deal_Ticket_Number = @Deal_Ticket_Id
        WHERE
            Deal_Ticket_Id = @Deal_Ticket_Id;

        INSERT INTO Trade.Deal_Ticket_Contract
             (
                 Deal_Ticket_Id
                 , CONTRACT_ID
                 , Created_User_Id
                 , Created_Ts
             )
        SELECT
            @Deal_Ticket_Id
            , CAST(Segments AS INT)
            , @User_Info_Id
            , GETDATE()
        FROM
            dbo.ufn_split(@Contract_Id, ',')
        WHERE
            @Contract_Id IS NOT NULL
        GROUP BY
            CAST(Segments AS INT);


        INSERT INTO Trade.Deal_Ticket_Audit
             (
                 Deal_Ticket_Id
                 , Audit_Type_Description
                 , User_Info_Id
                 , Audit_Ts
             )
        SELECT  @Deal_Ticket_Id, 'Initiated', @User_Info_Id, GETDATE();

        EXEC Trade.Deal_Ticket_Last_Updated_Ins @Deal_Ticket_Id;



    END;







GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Ins] TO [CBMSApplication]
GO
