SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.INSERT_ADDITIONAL_CHARGE_P
	@charge_master_id INT,
	@load_profile_specification_id INT,
	@charge_value DECIMAL(32,16),
	@charge_sign BIT
AS
BEGIN

	SET NOCOUNT ON

	INSERT INTO dbo.ADDITIONAL_CHARGE(CHARGE_MASTER_ID,
		LOAD_PROFILE_SPECIFICATION_ID, 
		CHARGE_VALUE, 
		CHARGE_SIGN ) 
	VALUES (@charge_master_id, 
		@load_profile_specification_id,
		@charge_value,
		@charge_sign)

END
GO
GRANT EXECUTE ON  [dbo].[INSERT_ADDITIONAL_CHARGE_P] TO [CBMSApplication]
GO
