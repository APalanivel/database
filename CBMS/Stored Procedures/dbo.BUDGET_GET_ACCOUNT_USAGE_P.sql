SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- exec BUDGET_GET_ACCOUNT_USAGE_P '','',1, '01/01/2008', '12/01/2008'
CREATE  PROCEDURE dbo.BUDGET_GET_ACCOUNT_USAGE_P
	@user_id varchar(10),
	@session_id varchar(20),
	@budget_account_id int,
	@from_month datetime, 
	@to_month datetime
	AS
	begin
	set nocount on

		if (select budget.commodity_type_id
		    from budget join budget_account on budget_account.budget_id = budget.budget_id 
			and budget_account.budget_account_id = @budget_account_id
		    ) = 291 --//Natural Gas
		begin
			select	budget_usage.month_identifier,
				(budget_usage.volume * conv.conversion_factor)as budget_usage,
				budget_usage_type.entity_name 
		 
			from 	budget_account
				join budget_usage on budget_usage.account_id = budget_account.account_id
				and budget_account.budget_account_id = @budget_account_id
				and budget_usage.commodity_type_id = 291
				and budget_usage.month_identifier between @from_month and @to_month
				join consumption_unit_conversion conv on conv.base_unit_id = budget_usage.volume_unit_type_id
				and conv.converted_unit_id = 25 --// MMBtu
				join entity budget_usage_type on budget_usage_type.entity_id = budget_usage.budget_usage_type_id
	
		end else --//Electirc Power
		begin
			select	budget_usage.month_identifier,
				(budget_usage.volume * conv.conversion_factor)as budget_usage,
				budget_usage_type.entity_name 
		 
			from 	budget_account
				join budget_usage on budget_usage.account_id = budget_account.account_id
				and budget_account.budget_account_id = @budget_account_id
				and budget_usage.commodity_type_id = 290
				and budget_usage.month_identifier between @from_month and @to_month
				join consumption_unit_conversion conv on conv.base_unit_id = budget_usage.volume_unit_type_id
				and conv.converted_unit_id = 12 --// kWh
				join entity budget_usage_type on budget_usage_type.entity_id = budget_usage.budget_usage_type_id
		
		end

		
	end









GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_ACCOUNT_USAGE_P] TO [CBMSApplication]
GO
