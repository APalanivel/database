SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Get_App_Config_Values

DESCRIPTION:
	To get the App config values for the given source and target path

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Source			VARCHAR(255)

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	SELECT * FROM App_Config

	EXEC dbo.Get_App_Config_Values 'Duke'
	EXEC dbo.Get_App_Config_Values 'Schneider'
	EXEC dbo.Get_App_Config_Values 'Retro'
	EXEC dbo.Get_App_Config_Values 'Archive'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS
	Initials	Date			Modification
------------------------------------------------------------
	HG			2014-10-17		Created for Duke requirement
		
******/
CREATE PROCEDURE [dbo].[Get_App_Config_Values] ( @Source VARCHAR(255) )
AS 
BEGIN

      SET NOCOUNT ON
      DECLARE @App_Id INT = 3

      DECLARE @Server_Data TABLE
            ( 
             Source VARCHAR(255)
            ,Column_Name VARCHAR(255)
            ,App_Config_Cd VARCHAR(255) )

      INSERT      INTO @Server_Data
                  ( 
                   Source
                  ,Column_Name
                  ,App_Config_Cd )
                  SELECT
                        'Duke' AS Source
                       ,'Server_Name'
                       ,'Duke_Server'
                  UNION
                  SELECT
                        'Duke' AS Source
                       ,'User_Name'
                       ,'Duke_User_Name'
                  UNION
                  SELECT
                        'Duke' AS Source
                       ,'Password'
                       ,'Duke_Password'
                  UNION
                  SELECT
                        'Duke' AS Source
                       ,'Inbound_Folder'
                       ,'Duke_Inbound_Folder'
                  UNION
                  SELECT
                        'Duke' AS Source
                       ,'Outbound_Folder'
                       ,'Duke_Outbound_Folder'
                  UNION
                  SELECT
                        'Duke' AS Source
                       ,'File_Extension'
                       ,'Duke_File_Extension'
                  UNION
                  SELECT
                        'Duke' AS Source
                       ,'Archive_Folder'
                       ,'Duke_Archive_Folder'
                  UNION
                  SELECT
                        'Duke' AS Source
                       ,'Is_Secured'
                       ,'Duke_Is_Secured'
                  UNION
                  SELECT
                        'Duke' AS Source
                       ,'Is_Delete_From_Source'
                       ,'Duke_Is_Delete_From_Source'
                  UNION
                  SELECT
                        'Duke' AS Source
                       ,'Success_Email_Address'
                       ,'Duke_Success_Email_Address'
                  UNION
                  SELECT
                        'Duke' AS Source
                       ,'Failure_Email_Address'
                       ,'Duke_Failure_Email_Address'
                  UNION

		-- Schneider
                  SELECT
                        'Schneider' AS Source
                       ,'Server_Name'
                       ,'Schneider_Server'
                  UNION
                  SELECT
                        'Schneider' AS Source
                       ,'User_Name'
                       ,'Schneider_User_Name'
                  UNION
                  SELECT
                        'Schneider' AS Source
                       ,'Password'
                       ,'Schneider_Password'
                  UNION
                  SELECT
                        'Schneider' AS Source
                       ,'Inbound_Folder'
                       ,'Schneider_Inbound_Folder'
                  UNION
                  SELECT
                        'Schneider' AS Source
                       ,'Outbound_Folder'
                       ,'Schneider_Outbound_Folder'
                  UNION
                  SELECT
                        'Schneider' AS Source
                       ,'File_Extension'
                       ,'Schneider_File_Extension'
                  UNION
                  SELECT
                        'Schneider' AS Source
                       ,'Archive_Folder'
                       ,'Schneider_Archive_Folder'
                  UNION
                  SELECT
                        'Schneider' AS Source
                       ,'Is_Secured'
                       ,'Schneider_Is_Secured'
                  UNION
                  SELECT
                        'Schneider' AS Source
                       ,'Is_Delete_From_Source'
                       ,'Schneider_Is_Delete_From_Source'
                  UNION
                  SELECT
                        'Schneider' AS Source
                       ,'Success_Email_Address'
                       ,'Schneider_Success_Email_Address'
                  UNION
                  SELECT
                        'Schneider' AS Source
                       ,'Failure_Email_Address'
                       ,'Schneider_Failure_Email_Address'
                  UNION

		-- Retro
                  SELECT
                        'Retro' AS Source
                       ,'Server_Name'
                       ,'Retro_Server'
                  UNION
                  SELECT
                        'Retro' AS Source
                       ,'User_Name'
                       ,'Retro_User_Name'
                  UNION
                  SELECT
                        'Retro' AS Source
                       ,'Password'
                       ,'Retro_Password'
                  UNION
                  SELECT
                        'Retro' AS Source
                       ,'Inbound_Folder'
                       ,'Retro_Inbound_Folder'
                  UNION
                  SELECT
                        'Retro' AS Source
                       ,'Outbound_Folder'
                       ,'Retro_Outbound_Folder'
                  UNION
                  SELECT
                        'Retro' AS Source
                       ,'File_Extension'
                       ,'Retro_File_Extension'
                  UNION
                  SELECT
                        'Retro' AS Source
                       ,'Archive_Folder'
                       ,'Retro_Archive_Folder'
                  UNION
                  SELECT
                        'Retro' AS Source
                       ,'Is_Secured'
                       ,'Retro_Is_Secured'
                  UNION
                  SELECT
                        'Retro' AS Source
                       ,'Is_Delete_From_Source'
                       ,'Retro_Is_Delete_From_Source'
                  UNION
                  SELECT
                        'Retro' AS Source
                       ,'Success_Email_Address'
                       ,'Retro_Success_Email_Address'
                  UNION
                  SELECT
                        'Retro' AS Source
                       ,'Failure_Email_Address'
                       ,'Retro_Failure_Email_Address'
                  UNION
                  
		-- Archive
                  SELECT
                        'Archive' AS Source
                       ,'Server_Name'
                       ,'Archive_Server'
                  UNION
                  SELECT
                        'Archive' AS Source
                       ,'User_Name'
                       ,'Archive_User_Name'
                  UNION
                  SELECT
                        'Archive' AS Source
                       ,'Password'
                       ,'Archive_Password'
                  UNION
                  SELECT
                        'Archive' AS Source
                       ,'Home_Folder'
                       ,'Archive_Home_Folder'
                  UNION
                  SELECT
                        'Archive' AS Source
                       ,'Is_Secured'
                       ,'Archive_Is_Secured'

      SELECT
            MAX(CASE WHEN sd.Column_Name = 'Server_Name' THEN src.App_Config_Value
                END) AS Host
           ,MAX(CASE WHEN sd.Column_Name = 'User_Name' THEN src.App_Config_Value
                END) AS [User_Name]
           ,MAX(CASE WHEN sd.Column_Name = 'Password' THEN src.App_Config_Value
                END) AS [Password]
           ,MAX(CASE WHEN sd.Column_Name = 'Inbound_Folder' THEN src.App_Config_Value
                END) AS Inbound_Folder
           ,MAX(CASE WHEN sd.Column_Name = 'Outbound_Folder' THEN src.App_Config_Value
                END) AS Outbound_Folder
           ,MAX(CASE WHEN sd.Column_Name = 'File_Extension' THEN src.App_Config_Value
                END) AS File_Extension
           ,MAX(CASE WHEN sd.Column_Name = 'Archive_Folder' THEN src.App_Config_Value
                END) AS Archive_Folder
           ,MAX(CASE WHEN sd.Column_Name = 'Is_Secured' THEN src.App_Config_Value
                END) AS Is_Secured
           ,MAX(CASE WHEN sd.Column_Name = 'Is_Delete_From_Source' THEN src.App_Config_Value
                END) AS Is_Delete_From_Source
           ,MAX(CASE WHEN sd.Column_Name = 'Success_Email_Address' THEN src.App_Config_Value
                END) AS Success_Email_Address
           ,MAX(CASE WHEN sd.Column_Name = 'Failure_Email_Address' THEN src.App_Config_Value
                END) AS Failure_Email_Address
      FROM
            dbo.App_Config src
            INNER JOIN @Server_Data sd
                  ON sd.App_Config_Cd = src.App_Config_Cd
      WHERE
            src.App_Id = @App_Id
            AND sd.Source = @Source

END
;
GO
GRANT EXECUTE ON  [dbo].[Get_App_Config_Values] TO [CBMSApplication]
GO
