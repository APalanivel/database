SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[GET_SITES_FOR_APPROVAL_P]  
     
DESCRIPTION:

      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@userId			VARCHAR
@sessionId		VARCHAR
@clientId		INT

OUTPUT PARAMETERS:     
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

EXEC dbo.GET_SITES_FOR_APPROVAL_P 1,1,235

EXEC dbo.GET_SITES_FOR_APPROVAL_P 1,1,10069

AUTHOR INITIALS:
INITIALS	NAME          
------------------------------------------------------------ 
HG			Harihara Suthan G
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------
HG			10/18/2010	Comments header added with the default SET statements
						As the pagination changes breaking the existing functionality reverted the changes pre Pagination version.
						Client, Division, Site, VwSitename tables replaced by Client_Hier table

*/

CREATE PROCEDURE dbo.GET_SITES_FOR_APPROVAL_P
	@userId		VARCHAR(10)
	,@sessionId VARCHAR(20)
	,@clientId	INT
AS
BEGIN

	SET NOCOUNT ON

	select 
		ch.site_Id
		,RTRIM(ch.city) + ', ' + ch.State_name + ' (' + ch.site_name + ')'	AS	site_name
	from
		Core.Client_Hier ch
	WHERE
		ch.client_id  = @clientId
		AND ch.Site_Id > 0
	order by
		Site_Name

END
GO
GRANT EXECUTE ON  [dbo].[GET_SITES_FOR_APPROVAL_P] TO [CBMSApplication]
GO
