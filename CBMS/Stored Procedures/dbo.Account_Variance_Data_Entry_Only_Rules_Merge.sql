SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********   
NAME:  dbo.Account_Variance_Data_Entry_Only_Rules_Merge  
 
DESCRIPTION:  Used to delete and insert Is_Data_Entry Rules for account

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
@Account_id						int
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:  
	
	Account_Variance_Data_Entry_Only_Rules_Merge 12345
	
	

------------------------------------------------------------  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
NK   Nageswara Rao Kosuri         


Initials Date  Modification  
------------------------------------------------------------  
NK	01/21/2010  Created


******/  


CREATE PROCEDURE [dbo].[Account_Variance_Data_Entry_Only_Rules_Merge]
@Account_id	INT,
@Is_Data_Entry_Only BIT
		
AS
BEGIN
	
	
	MERGE INTO dbo.Variance_Rule_Dtl_Account_Override vrdac      
      USING(      
                  SELECT vrdtl.variance_rule_dtl_id AS Variance_Rule_Dtl_Id,
						@Account_id AS Account_Id
				  FROM variance_rule_dtl vrdtl 		
				  WHERE vrdtl.is_data_entry_only = 1
						AND Is_Active = 1 
						AND NOT EXISTS (SELECT
                                                                        1
                                                                  FROM  Core.Client_Hier_Account cha
                                                                        JOIN
                                                                        dbo.Account_Variance_Consumption_Level avcl1
                                                                              ON avcl1.ACCOUNT_ID = cha.Account_Id
                                                                        JOIN
                                                                        dbo.Variance_Consumption_Level vcl
                                                                              ON vcl.Variance_Consumption_Level_Id = avcl1.Variance_Consumption_Level_Id
                                                                                 AND vcl.Commodity_Id = cha.Commodity_Id
                                                                        JOIN
                                                                        dbo.Bucket_Master bm
                                                                              ON bm.Commodity_Id = vcl.Commodity_Id
                                                                        JOIN
                                                                        dbo.Variance_category_Bucket_Map vcbm
                                                                              ON vcbm.Bucket_master_Id = bm.Bucket_Master_Id
                                                                        JOIN
                                                                        dbo.Variance_Parameter vp
                                                                              ON vp.Variance_Category_Cd = vcbm.Variance_Category_Cd
                                                                        JOIN
                                                                        dbo.Variance_Parameter_Baseline_Map vpbm
                                                                              ON vpbm.Variance_Parameter_Id = vp.Variance_Parameter_Id
                                                                        JOIN
                                                                        dbo.Variance_Rule vr
                                                                              ON vr.Variance_Baseline_Id = vpbm.Variance_Baseline_Id
                                                                        JOIN
                                                                        dbo.Variance_Rule_Dtl vrd1
                                                                              ON vrd1.Variance_Rule_Id = vr.Variance_Rule_Id
                                                                        JOIN
                                                                        dbo.Variance_Parameter_Commodity_Map vpcm
                                                                              ON vpcm.Variance_Parameter_Id = vp.Variance_Parameter_Id
                                                                        JOIN
                                                                        dbo.Variance_Processing_Engine vpe
                                                                              ON vpe.Variance_Processing_Engine_Id = vpcm.variance_process_Engine_id
                                                                        JOIN
                                                                        dbo.Code vpcat
                                                                              ON vpcat.Code_Id = vpe.Variance_Engine_Cd
                                                                        JOIN
                                                                        dbo.Variance_Consumption_Extraction_Service_Param_Value vesp
                                                                              ON vesp.Variance_Consumption_Level_Id = vcl.Variance_Consumption_Level_Id
                                                                  WHERE cha.Account_Id = @Account_id
                                                                        AND   cha.Account_Type = 'Utility'
                                                                        AND   vpcat.Code_Value = 'Advanced Variance Test'
																		AND vrd1.Variance_Rule_Dtl_Id =vrdtl.Variance_Rule_Dtl_Id)
            ) src      
      ON vrdac.Variance_Rule_Dtl_Id = src.Variance_Rule_Dtl_Id      
            AND vrdac.Account_Id = src.Account_Id      
           
      WHEN MATCHED AND (@Is_Data_Entry_Only = 0 OR @Is_Data_Entry_Only IS NULL) 	THEN	
		 DELETE      
      
	  WHEN NOT MATCHED BY TARGET  AND @Is_Data_Entry_Only = 1 THEN      
			INSERT(Variance_Rule_Dtl_Id        
			,ACCOUNT_ID     			       
		   )      
		   VALUES( src.Variance_Rule_Dtl_Id      
		   ,@Account_Id  
		   )  ;  
	
END
GO
GRANT EXECUTE ON  [dbo].[Account_Variance_Data_Entry_Only_Rules_Merge] TO [CBMSApplication]
GO
