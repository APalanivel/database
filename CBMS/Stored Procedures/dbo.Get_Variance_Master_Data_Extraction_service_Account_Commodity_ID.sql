SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/******                              
 NAME: dbo.[Get_Variance_Master_Data_Extraction_service_Account_Commodity_ID]                 
                              
  
                              
 INPUT PARAMETERS:                
                           
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
@Account_Id			INT
@Commodity_ID      INT               NULL   
@service_month	   Datetime
      
                                  
 OUTPUT PARAMETERS:                
                                 
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
                              
 USAGE EXAMPLES:                                  
---------------------------------------------------------------------------------------------------------------                                  
      
--Commodity Level      
   
 EXEC Get_Variance_Master_Data_Extraction_service_Account_Commodity_ID 1688,290; 
     
                             
 AUTHOR INITIALS:  
 ArunKumar Palanivel	AP            
             
 Initials              Name              
--------------------------------------------------------------------------------------------------------------- 
AP				Jan 24 2020 Procedure is created to get the header information for extraction service 
     Steps(added by Arun):

	 
 MODIFICATIONS:            
                
 Initials              Date             Modification            
---------------------------------------------------------------------------------------------------------------    
AP						Jan 24,2020		New procedure to get header info for extraction service        
   
******/
CREATE PROCEDURE [dbo].[Get_Variance_Master_Data_Extraction_service_Account_Commodity_ID]
      (
      @Account_Id    INT
    , @Commodity_Id  INT
    , @Service_Month DATE )
AS
      BEGIN

            SET NOCOUNT ON;

            DECLARE
                  @Billing_Start_Date            DATETIME
                , @Billing_End_Date              DATETIME
                , @Variance_Consumption_Level_id INT
                --, @Error_Metric                  VARCHAR(200)
                --, @Sigma_Value                   VARCHAR(10)
                , @outlier_analysis              VARCHAR(100)
                , @commodity_name                VARCHAR(100)
				,@Account_Consumption_Level		varchar(200)

            SELECT
                  @Billing_Start_Date = cuabd.Billing_Start_Dt
                , @Billing_End_Date = cuabd.Billing_End_Dt
                , @Variance_Consumption_Level_id = vcl.Variance_Consumption_Level_Id
                , @commodity_name = cm.Commodity_Name
				,@Account_Consumption_Level = vcl.Consumption_Level_Desc
            FROM  dbo.Cost_Usage_Account_Dtl CUAD
                  JOIN
                  dbo.Cost_Usage_Account_Billing_Dtl cuabd
                        ON cuabd.Account_Id = CUAD.ACCOUNT_ID
                           AND cuabd.Service_Month = CUAD.Service_Month
				JOIN dbo.Variance_category_Bucket_Map vcbm
				ON vcbm.Bucket_master_Id = CUAD.Bucket_Master_Id
                  JOIN
                  dbo.Variance_Category_Required_Map vcrm
                        ON vcrm.Bucket_Master_Id = CUAD.Bucket_Master_Id
                  JOIN
                  Core.Client_Hier_Account cha
                        ON cha.Account_Id = cuabd.Account_Id
                  JOIN
                  dbo.Account_Variance_Consumption_Level AVCL
                        ON AVCL.ACCOUNT_ID = CUAD.ACCOUNT_ID
                  JOIN
                  dbo.Variance_Consumption_Level vcl
                        ON vcl.Variance_Consumption_Level_Id = AVCL.Variance_Consumption_Level_Id
                  JOIN
                  dbo.Commodity cm
                        ON cm.Commodity_Id = vcl.Commodity_Id
            WHERE CUAD.ACCOUNT_ID = @Account_Id
                  AND   cha.Commodity_Id = @Commodity_Id
                  AND   CUAD.Service_Month = @Service_Month
                  AND   vcl.Commodity_Id = @Commodity_Id;

				   
            SELECT
                  @outlier_analysis = CASE WHEN 
				  param_value =0 THEN 'Disabled'
				  WHEN param_value =1 THEN 'Enabled'
				  END
            FROM  dbo.Variance_Engine_Param_Value VCESPV
                  JOIN
                  dbo.Variance_Extraction_Service_Param VESP
                        ON VESP.Variance_Extraction_Service_Param_Id = VCESPV.Variance_Extraction_Service_Param_Id
            WHERE VESP.Param_Name = 'Linear_Regression_Outlier_Analysis'
                  --AND   VCESPV.Variance_Consumption_Level_Id = @Variance_Consumption_Level_id;
				   
				   

            SELECT
                  @Account_Id AS CurrentAccountId
                , cast(@Billing_Start_Date AS DATE) AS [Billing Period start Date]
                , cast(@Billing_End_Date AS DATE) AS [Billing Period ENd Date]
                , cast(@Service_Month AS DATE) AS [ServiceMonth]
                , @commodity_name AS [Commodity Name]
              --  , @Sigma_Value AS [ThresholdSigma]
                , @outlier_analysis AS [OutlierAnanlysis]
               -- , @Error_Metric AS [Test Calculation Type]
				,@Account_Consumption_Level AS [Account Consumption Level]




      END;

GO
GRANT EXECUTE ON  [dbo].[Get_Variance_Master_Data_Extraction_service_Account_Commodity_ID] TO [CBMSApplication]
GO
