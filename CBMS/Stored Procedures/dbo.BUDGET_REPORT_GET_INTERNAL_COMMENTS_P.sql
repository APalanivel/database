SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.BUDGET_REPORT_GET_INTERNAL_COMMENTS_P
	@budget_id int,
	@comments_key int,
	@owner_type varchar(30)
	AS
	begin
		set nocount on

		declare @budget_account_id int, @owner_type_id int
		
		if @owner_type = 'Division'
		begin
			select @budget_account_id = min(budget_account.budget_account_id)
			from
			division d 
			join site s on d.division_id = s.division_id
			and d.division_id = @comments_key
			join account utilacc on s.site_id = utilacc.site_id
			join budget_account on utilacc.account_id = budget_account.account_id
			and budget_account.budget_id = @budget_id
			set @owner_type_id = 701 --//Division owner type

		end
		else if @owner_type = 'Site'
		begin
			select @budget_account_id = min(budget_account.budget_account_id)
			from
			site s 
			join account utilacc on s.site_id = utilacc.site_id
			and s.site_id = @comments_key
			join budget_account on utilacc.account_id = budget_account.account_id
			and budget_account.budget_id = @budget_id
			set @owner_type_id = 702 --//Site owner type
		end
		else -- 'Account'
		begin
			select @budget_account_id = @comments_key
			set @owner_type_id = 1309 --//Account owner type
		end


		select  budget_account_id,
			budget_detail_comments
		from 	budget_detail_comments_owner_map 
		where 	budget_account_id = @budget_account_id 
			and owner_type_id = @owner_type_id

	end



GO
GRANT EXECUTE ON  [dbo].[BUDGET_REPORT_GET_INTERNAL_COMMENTS_P] TO [CBMSApplication]
GO
