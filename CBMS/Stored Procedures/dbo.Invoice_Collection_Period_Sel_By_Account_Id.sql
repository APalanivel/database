SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Invoice_Collection_Period_Sel_By_Account_Id       
              
Description:              
			This sproc is to get the Invoice Collection periods
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
	 @Account_Id						INT
                        
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

   Exec dbo.Invoice_Collection_Period_Sel_By_Account_Id 1269921,1,3
   
   Exec dbo.Invoice_Collection_Period_Sel_By_Account_Id 6045,1,3
   
   Exec dbo.Invoice_Collection_Period_Sel_By_Account_Id 13598
   
  
   
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RR				Raghu Reddy
 
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RR				2017-02-28		Created CP-54
    RKV             2017-10-07      Added filter to eliminate the Archived records from icq table
    HG				2018-01-16		SE2017-389, Modified to not return the not managed accounts data, if the account is deleted then all the IC data associated to that account will be deleted as a part of Utility_Account_History_Del_By_Account_Id             
******/ 
CREATE PROCEDURE [dbo].[Invoice_Collection_Period_Sel_By_Account_Id]
      (
       @Account_Id INT
      ,@StartIndex INT = 1
      ,@EndIndex INT = 2147483647 )
AS
BEGIN

      SET NOCOUNT ON;
      DECLARE @Account_Not_Managed BIT = 0

      SELECT
            @Account_Not_Managed = ISNULL(NOT_MANAGED, 0)
      FROM
            ACCOUNT
      WHERE
            ACCOUNT_ID = @Account_Id;

      WITH  CTE_Invoice_Collection
              AS ( SELECT
                        icac.Invoice_Collection_Service_Start_Dt
                       ,icac.Invoice_Collection_Service_End_Dt
                       ,ROW_NUMBER() OVER ( ORDER BY icac.Invoice_Collection_Service_Start_Dt, icac.Invoice_Collection_Service_End_Dt ) AS Row_Num
                       ,COUNT(1) OVER ( ) Total_Rows
                   FROM
                        dbo.Invoice_Collection_Account_Config icac
                        INNER JOIN dbo.Invoice_Collection_Queue icq
                              ON icac.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
                        INNER JOIN dbo.Code icqsc
                              ON icqsc.Code_Id = icq.Status_Cd
                   WHERE
                        icac.Account_Id = @Account_Id
                        AND @Account_Not_Managed = 0
                        AND icqsc.Code_Value <> 'Archived'
                        AND ( EXISTS ( SELECT
                                          1
                                       FROM
                                          dbo.Invoice_Collection_Chase_Log_Queue_Map icclq
                                          INNER JOIN dbo.Invoice_Collection_Chase_Log iccl
                                                ON icclq.Invoice_Collection_Chase_Log_Id = iccl.Invoice_Collection_Chase_Log_Id
                                          INNER JOIN dbo.Code cd
                                                ON iccl.Status_Cd = cd.Code_Id
                                          INNER JOIN dbo.Codeset cs
                                                ON cd.Codeset_Id = cs.Codeset_Id
                                       WHERE
                                          cs.Codeset_Name = 'IC Chase Status'
                                          AND cd.Code_Value = 'Close'
                                          AND icq.Invoice_Collection_Queue_Id = icclq.Invoice_Collection_Queue_Id )
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Invoice_Collection_Issue_Log ici
                                          WHERE
                                                icq.Invoice_Collection_Queue_Id = ici.Invoice_Collection_Queue_Id )
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Invoice_Collection_Exception_Comment ice
                                          WHERE
                                                icq.Invoice_Collection_Queue_Id = ice.Invoice_Collection_Queue_Id )
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Code icecd
                                                INNER JOIN dbo.Codeset icecs
                                                      ON icecd.Codeset_Id = icecs.Codeset_Id
                                          WHERE
                                                icq.Status_Cd = icecd.Code_Id
                                                AND icecd.Code_Value IN ( 'Processed' )
                                                AND icecs.Codeset_Name = 'ICE Status' )
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Code icrcd
                                                INNER JOIN dbo.Codeset icrcs
                                                      ON icrcd.Codeset_Id = icrcs.Codeset_Id
                                          WHERE
                                                icq.Status_Cd = icrcd.Code_Id
                                                AND icrcd.Code_Value IN ( 'Received' )
                                                AND icrcs.Codeset_Name = 'ICR Status' ) )
                   GROUP BY
                        icac.Invoice_Collection_Service_Start_Dt
                       ,icac.Invoice_Collection_Service_End_Dt)
            SELECT
                  cic.Invoice_Collection_Service_Start_Dt
                 ,cic.Invoice_Collection_Service_End_Dt
                 ,cic.Total_Rows
            FROM
                  CTE_Invoice_Collection cic
            WHERE
                  cic.Row_Num BETWEEN @StartIndex AND @EndIndex
            ORDER BY
                  Row_Num; 
			     
      
END;
;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Period_Sel_By_Account_Id] TO [CBMSApplication]
GO
