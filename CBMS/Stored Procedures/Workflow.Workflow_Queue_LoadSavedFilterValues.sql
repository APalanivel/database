SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
NAME:      
 [Workflow].[Workflow_Queue_LoadSavedFilterValues]         
  
DESCRIPTION:     
The Stored Procedure will return the Saved Filter information         
  
INPUT PARAMETERS:              
 Name         DataType  Default Description      
-------------------------------------------------------------------------                        
 OUTPUT PARAMETERS:              
 Name   DataType  Default Description    
-------------------------------------------------------------------------             
 USAGE EXAMPLES:   
-------------------------------------------------------------------------             
 EXEC [Workflow].[Workflow_Queue_LoadSavedFilterValues]   147                      
       
 EXEC [Workflow].[Workflow_Queue_LoadSavedFilterValues]                         
 Workflow_Queue_Saved_Filter_Query_Id =147 
  
AUTHOR INITIALS:              
 Initials Name             
-------------------------------------------------------------------------                   
 CPK    
          
******/  
CREATE  PROCEDURE [Workflow].[Workflow_Queue_LoadSavedFilterValues]  
     (  
         @Saved_Filter_Id INT         
     )  
AS  
    BEGIN  
        
			SELECT  
				Selected_KeyValue,
                Selected_Value 
            FROM  
                Workflow.Workflow_Queue_Saved_Filter_Query_Value sv 
                  
            WHERE  
                Workflow_Queue_Saved_Filter_Query_Id = @Saved_Filter_Id
  
        
    END;  
GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_LoadSavedFilterValues] TO [CBMSApplication]
GO
