SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

  
/******        

  
NAME:        
 dbo.cbmsSsoProject_Save    
    
DESCRIPTION:        
 Used to insert in to sso_project table      
    
INPUT PARAMETERS:        
Name      DataType  Default Description        
------------------------------------------------------------        
@MyAccountId    INT      
@project_id     INT      
@project_title    VARCHAR(600)      
@project_description  VARCHAR(4000) NULL    
@commodity_type_id   INT      
@projected_end_date   DATETIME      
@project_ownership_type_id INT      
@is_urgent     BIT    
@project_status_type_id  INT      
@project_category_type_id INT      
@sus_project_subcategory_cd INT    NULL      
    
    
OUTPUT PARAMETERS:        
Name      DataType  Default Description        
------------------------------------------------------------        
USAGE EXAMPLES:        
------------------------------------------------------------      
    
EXEC dbo.cbmsSsoProject_Save -1,3620,'Rate Review','The power contract with Quest Energy expired 3/15/06.      
    
This site will go back to tariff based on cost & usage analysis performed by Summit.      
As per Vic Tuttle, Summit will give notice to Quest Energy regarding the switch back to tariff.       
*Completed...Jeremy Warner contacted Quest Energy on 1/4/06.', 832,'2006-01-13 00:00:00.000',769,0,703,1488 ,'2006-01-04'     
    
AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
GB   Geetansu Behera        
CMH  Chad Hattabaugh         
HG   Hari    
SKA  Shobhit Kr Agrawal    
    
MODIFICATIONS         
Initials Date  Modification        
------------------------------------------------------------        
GB				Created    
SKA 08/07/09	Modified as per the coding standards   
GB	8 Aug 2009	set the default value Null for @sus_project_subcategory_cd instead of -1 
    
    
******/      
    
CREATE PROCEDURE [dbo].[cbmsSsoProject_Save]    
(      
  @MyAccountId INT,      
  @project_id INT = NULL,      
  @project_title VARCHAR(600),      
  @project_description VARCHAR(4000) = NULL,      
  @commodity_type_id INT,      
  @projected_end_date DATETIME,      
  @project_ownership_type_id INT,      
  @is_urgent BIT,      
  @project_status_type_id INT,      
  @project_category_type_id INT,      
  @sus_project_subcategory_cd INT = NULL,
  @Start_Date DATETIME 
  
 )        
AS        
BEGIN        
      
    SET NOCOUNT ON        

	 DECLARE @Sso_project_id INT  
	  , @err INT  

	 SET @Sso_project_id = @project_id       

	 BEGIN TRAN  

	  INSERT INTO dbo.Sso_project(     
		   project_title,  
		   project_description,     
		   project_status_type_id,     
		   commodity_type_id,     
		   projected_end_date,     
		   project_ownership_type_id,      
		   is_urgent,     
		   [start_date],     
		   updated_date,     
		   project_category_type_id,     
		   sus_proj_subcategory_cd,
		   Project_Created_Ts
		   )        
	  VALUES(    
		   @project_title,     
		   @project_description,     
		   @project_status_type_id,     
		   @commodity_type_id,     
		   @projected_end_date,     
		   @project_ownership_type_id,      
		   @is_urgent,     
		   @Start_Date,    
		   GETDATE(),     
		   @project_category_type_id,     
		   @sus_project_subcategory_cd
		   ,GETDATE())        

	  SELECT @err = @@ERROR, @Sso_project_id = SCOPE_IDENTITY()  
	  IF @Err != 0 GOTO StmtFailed  

	  EXEC cbmsEntityAudit_Log @MyAccountId, 'sso_project', @Sso_project_id, 'Insert'      
	  SET @Err = @@ERROR  
	  IF @Err != 0 GOTO StmtFailed  


	StmtFailed:  

	 IF @Err <> 0  
		  BEGIN  
			ROLLBACK TRAN    
		  END   
	 ELSE  
		  BEGIN    
			COMMIT TRAN  
		  END      

	 SELECT @Sso_project_id AS "sso_project_id"    
  
END      

GO
GRANT EXECUTE ON  [dbo].[cbmsSsoProject_Save] TO [CBMSApplication]
GO
