SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                 
/******                        
 NAME: dbo.RM_Onboard_Sites_Sel_By_Division_Country            
                        
 DESCRIPTION:                        
			To Get RM On Board Sites based on Division    
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@divisionId					INT
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 
   	exec RM_Onboard_Sites_Sel_By_Division_Country @divisionId=62,@Country_Name='USA'


                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2014-11-07       Created                
                       
******/


CREATE  PROCEDURE [dbo].[RM_Onboard_Sites_Sel_By_Division_Country]
      ( 
       @divisionId INT
      ,@Country_Name VARCHAR(200) = NULL )
AS 
BEGIN 
      SET NOCOUNT ON
      SELECT
            ch.SITE_ID
           ,RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' Site_Name
           ,ch.SITE_REFERENCE_NUMBER
      FROM
            core.Client_Hier ch
            JOIN RM_ONBOARD_HEDGE_SETUP hedge
                  ON hedge.SITE_ID = ch.SITE_ID
                     AND hedge.INCLUDE_IN_REPORTS = 1
      WHERE
            ( @Country_Name IS NULL
              OR ch.Country_Name = @Country_Name )
            AND ch.Sitegroup_Id = @divisionId
            AND ch.Site_Not_Managed = 0
            AND ch.Site_Closed = 0
            AND ch.Site_Id > 0
      ORDER BY
            RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
END
;
GO
GRANT EXECUTE ON  [dbo].[RM_Onboard_Sites_Sel_By_Division_Country] TO [CBMSApplication]
GO
