SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE [dbo].[cbmsELMarketData_GetIndexesForSource]
	( @source varchar(200) = null
	)

AS
BEGIN

	   select price_point_id
		, price_point
	     from el_market_data with (nolock)
	    where source = @source
		order by price_point

END
GO
GRANT EXECUTE ON  [dbo].[cbmsELMarketData_GetIndexesForSource] TO [CBMSApplication]
GO
