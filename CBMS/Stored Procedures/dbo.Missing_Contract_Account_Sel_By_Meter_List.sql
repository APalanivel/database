SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
	 dbo.Missing_Contract_Account_Sel_By_Meter_List    
    
DESCRIPTION:    
    
 Rolling_Meters.
     
    
INPUT PARAMETERS:    
 Name					DataType	 Default		Description    
--------------------------------------------------------------------    
 
    
OUTPUT PARAMETERS:    
 Name					DataType	 Default		Description    
--------------------------------------------------------------------    
    
USAGE EXAMPLES:    
--------------------------------------------------------------------    


EXEC Missing_Contract_Account_Sel_By_Meter_List '17878' ,'2019-01-01','2019-12-01'


select getdate()
2020-06-08 00:00:00.000
 

    
AUTHOR INITIALS:    
	Initials		Name    

--------------------------------------------------------------------    
    NR				Narayana Reddy       
    
MODIFICATIONS    
    
 Initials		Date			Modification    
--------------------------------------------------------------------    
 NR				2019-05-30		Created For - Add Contract Project.
 NR				2020-06-08		MAINT-10227 - Added Inputs Begin & End date.

  
******/

CREATE PROCEDURE [dbo].[Missing_Contract_Account_Sel_By_Meter_List]
    (
        @Meter_Id VARCHAR(MAX)
        , @Supplier_Account_begin_Dt DATETIME
        , @Supplier_Account_End_Dt DATETIME
    )
AS
    SET NOCOUNT ON;

    BEGIN

        SELECT
            cha.Account_Id
            , cha.Account_Number
            , cha.Meter_Id
            , cha.Meter_Number
            , cha.Supplier_Contract_ID
            , cha.Supplier_Account_Config_Id
            , cha.Supplier_Account_begin_Dt
            , NULLIF(cha.Supplier_Account_End_Dt, '9999-12-31') AS Supplier_Account_End_Dt
            , cha.Account_Vendor_Id
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Supplier_Contract_ID = -1
            AND cha.Account_Type = 'Supplier'
            AND EXISTS (   SELECT
                                1
                           FROM
                                dbo.ufn_split(@Meter_Id, ',') us
                           WHERE
                                us.Segments = cha.Meter_Id)
            AND (   cha.Supplier_Account_begin_Dt BETWEEN @Supplier_Account_begin_Dt
                                                  AND     ISNULL(@Supplier_Account_End_Dt, '9999-12-31')
                    OR  cha.Supplier_Account_End_Dt BETWEEN @Supplier_Account_begin_Dt
                                                    AND     ISNULL(@Supplier_Account_End_Dt, '9999-12-31')
                    OR  @Supplier_Account_begin_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                                   AND     cha.Supplier_Account_End_Dt
                    OR  ISNULL(@Supplier_Account_End_Dt, '9999-12-31') BETWEEN cha.Supplier_Account_begin_Dt
                                                                       AND     cha.Supplier_Account_End_Dt)
        GROUP BY
            cha.Account_Id
            , cha.Account_Number
            , cha.Meter_Id
            , cha.Meter_Number
            , cha.Supplier_Contract_ID
            , cha.Supplier_Account_Config_Id
            , cha.Supplier_Account_begin_Dt
            , NULLIF(cha.Supplier_Account_End_Dt, '9999-12-31')
            , cha.Account_Vendor_Id;

    END;





GO
GRANT EXECUTE ON  [dbo].[Missing_Contract_Account_Sel_By_Meter_List] TO [CBMSApplication]
GO
