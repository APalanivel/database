
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
    
/******              
    
NAME: dbo.BudgetDetail_SearchDetailsForBudgetVariance_By_Site_For_Client_Division_Site    
         
DESCRIPTION:    
    
 To get budget usage and cost of all Sites of given filter criteria and budget id    
    
INPUT PARAMETERS:              
 NAME     DATATYPE DEFAULT  DESCRIPTION              
---------------------------------------------------------------          
 @Budget_Id    INT    
 @Client_Id    INT    
 @Sitegroup_Id   INT   NULL    
 @Site_Client_Hier_Id    INT   NULL    
 @Currency_Unit_Id  INT   NULL    
 @View_Type    VARCHAR(20) NULL    
 @UOM_Type_Id   INT   NULL    
    
OUTPUT PARAMETERS:    
 NAME     DATATYPE DEFAULT  DESCRIPTION    
---------------------------------------------------------------  
    
  
USAGE EXAMPLES:    
------------------------------------------------------------    
    
 EXECUTE dbo.BudgetDetail_SearchDetailsForBudgetVariance_By_Site_For_Client_Division_Site     
    @Budget_Id = 44    
   ,@Client_Id = 10002    
   ,@Currency_Unit_Id = NULL    
   ,@View_Type = NULL    
   ,@UOM_Type_Id = NULL    
     
 EXEC BudgetDetail_SearchDetailsForBudgetVariance_By_Site_For_Client_Division_Site 7593,223,12221003,178514  
  
    
AUTHOR INITIALS:    
 INITIALS NAME    
------------------------------------------------------------    
 AP   Athmaram Pabbathi    
 HG   Harihara Suthan G  
 RR   Raghu Reddy
 RKV  Ravi Kumar Vegesna  
    
MODIFICATIONS    
 INITIALS DATE  MODIFICATION    
------------------------------------------------------------    
 AP   04/05/2012  Created for Addnl Data Changes to replace below SPs    
       -- dbo.cbmsBudgetDetail_SearchDetailsForBudgetVariance (To return Division level Data for given Client)    
 HG   2012-07-10  MAINT-1359, Division_id and Division_Name added as application is referring this column.    
       - @Site_Client_Hier_Id filter was not applier added code for that  
 RR   2012-08-31 Maint-1499 Sub select to get the values from dbo.Budget_Details have group by on Site_Id, Site_Name, Client_Hier_Id,   
       Sitegroup_Id, Sitegroup_Name. If more than one budget account have same value, its considering the value only once   
       and getting filtered by this. Added BUDGET_ACCOUNT_ID(dbo.Budget_Details) in group by to fetch each budget account value.
 RKV  2013-11-07 MAINT-2345, Budget_Account.Is_Deleted = 0 condition is Added to exclude the deleted accounts 
         
*/    
CREATE PROCEDURE dbo.BudgetDetail_SearchDetailsForBudgetVariance_By_Site_For_Client_Division_Site
      ( 
       @Budget_Id INT
      ,@Client_Id INT
      ,@Sitegroup_Id INT = NULL
      ,@Site_Client_Hier_Id INT = NULL
      ,@Currency_Unit_Id INT = NULL
      ,@View_Type VARCHAR(20) = NULL
      ,@UOM_Type_Id INT = NULL )
AS 
BEGIN    
      SET NOCOUNT ON    
    
      DECLARE
            @Currency_Factor DECIMAL(18, 5)
           ,@Currency_Unit_Name VARCHAR(10)
           ,@MMBtu_UOM_Type_Id INT    
    
    
      SELECT
            @Currency_Unit_Name = cu.Currency_Unit_Name
      FROM
            dbo.Currency_Unit cu
      WHERE
            ( @Currency_Unit_Id IS NULL
              AND cu.Currency_Unit_Name = 'USD' )
            OR cu.Currency_Unit_Id = @Currency_Unit_Id    
          
      SELECT
            @Currency_Factor = 1
      WHERE
            @Currency_Unit_Name = 'USD'    
    
      SELECT
            @Currency_Factor = bcm.Conversion_Factor
      FROM
            dbo.Budget_Currency_Map bcm
      WHERE
            bcm.Budget_Id = @Budget_Id
            AND bcm.Currency_Unit_Id = @Currency_Unit_Id
            AND @Currency_Unit_Name != 'USD'    
    
    
      SELECT
            @MMBtu_UOM_Type_Id = uom.Entity_Id
      FROM
            dbo.Entity uom
      WHERE
            uom.Entity_Name = 'MMBtu'
            AND uom.Entity_Description = 'Unit for Gas'    
                
      SELECT
            @UOM_Type_Id = @MMBtu_UOM_Type_Id
      WHERE
            @UOM_Type_Id IS NULL    
    
    
    
      SELECT
            x.Site_Name
           ,x.Site_Id
           ,x.Client_Hier_Id AS Site_Client_Hier_Id
           ,x.Sitegroup_Id AS Division_Id
           ,x.Sitegroup_Name AS Division_Name
           ,x.[Month]
           ,x.Budget_Id
           ,SUM(x.Usage) * uc.Conversion_Factor AS Usage
           ,SUM(x.Total_Cost) / @Currency_Factor AS Total_Cost
           ,SUM(x.Other_Unit) / @Currency_Factor AS Other_Unit
           ,SUM(x.Other_Fixed) / @Currency_Factor AS Other_Fixed
           ,( ( ( SUM(x.Commodity_Generation) / @Currency_Factor ) / ( CASE WHEN @View_Type = 'Unit Cost' THEN ( CASE WHEN SUM(x.Usage) = 0 THEN 1
                                                                                                                      ELSE SUM(x.Usage)
                                                                                                                 END )
                                                                            ELSE 1
                                                                       END ) ) / ( CASE WHEN @View_Type = 'Unit Cost' THEN uc.Conversion_Factor
                                                                                        ELSE 1
                                                                                   END ) ) AS Commodity_Generation
           ,ISNULL(SUM(x.total_cost) / NULLIF(SUM(x.usage) * uc.Conversion_Factor, 0), 0) / @Currency_Factor AS Unit_Cost
           ,( ( SUM(x.Transmission) / @Currency_Factor ) / ( CASE WHEN @View_Type = 'Unit Cost' THEN ( CASE WHEN SUM(x.Usage) = 0 THEN 1
                                                                                                            ELSE SUM(x.Usage)
                                                                                                       END )
                                                                  ELSE 1
                                                             END ) ) / ( CASE WHEN @View_Type = 'Unit Cost' THEN uc.Conversion_Factor
                                                                              ELSE 1
                                                                         END ) AS Transmission
           ,( ( SUM(x.Distribution) / @Currency_Factor ) / ( CASE WHEN @View_Type = 'Unit Cost' THEN ( CASE WHEN SUM(x.Usage) = 0 THEN 1
                                                                                                            ELSE SUM(x.Usage)
                                                                                                       END )
                                                                  ELSE 1
                                                             END ) ) / ( CASE WHEN @View_Type = 'Unit Cost' THEN uc.Conversion_Factor
                                                                              ELSE 1
                                                                         END ) AS Distribution
           ,( ( SUM(x.Transportation) / @Currency_Factor ) / ( CASE WHEN @View_Type = 'Unit Cost' THEN ( CASE WHEN SUM(x.Usage) = 0 THEN 1
                                                                                                              ELSE SUM(x.Usage)
                                                                                                         END )
                                                                    ELSE 1
                                                               END ) ) / ( CASE WHEN @View_Type = 'Unit Cost' THEN uc.Conversion_Factor
                                                                                ELSE 1
                                                                           END ) AS Transportation
           ,( ( SUM(x.Other_Cost) / @Currency_Factor ) / ( CASE WHEN @View_Type = 'Unit Cost' THEN ( CASE WHEN SUM(x.Usage) = 0 THEN 1
                                                                                                          ELSE SUM(x.Usage)
                                                                                                     END )
                                                                ELSE 1
                                                           END ) ) / ( CASE WHEN @View_Type = 'Unit Cost' THEN uc.Conversion_Factor
                                                                            ELSE 1
                                                                       END ) AS Other_Cost
           ,( ( SUM(x.Taxes) / @Currency_Factor ) / ( CASE WHEN @View_Type = 'Unit Cost' THEN ( CASE WHEN SUM(x.Usage) = 0 THEN 1
                                                                                                     ELSE SUM(x.Usage)
                                                                                                END )
                                                           ELSE 1
                                                      END ) ) / ( CASE WHEN @View_Type = 'Unit Cost' THEN uc.Conversion_Factor
                                                                       ELSE 1
                                                                  END ) AS Taxes
      FROM
            ( SELECT
                  cha.Site_Id
                 ,cha.Site_Name
                 ,cha.Client_Hier_Id
                 ,cha.Sitegroup_Id
                 ,cha.Sitegroup_Name
                 ,CONVERT(VARCHAR(7), bd.Month_Identifier, 111) AS [Month]
                 ,ISNULL(bd.Variable_Value * ISNULL(bd.Budget_Usage, bu.Volume), 0) AS Commodity_Generation
                 ,( ( ISNULL(bd.Variable_Value, 0) + ISNULL(bd.Rates_Tax_Value, 0) + ISNULL(bd.Sourcing_Tax_Value, 0) + ISNULL(bd.Transportation_Value, 0) + ISNULL(bd.Transmission_Value, 0) + ISNULL(bd.Distribution_Value, 0) + ISNULL(bd.Other_Bundled_Value, 0) ) * COALESCE(bd.Budget_Usage, bu.Volume, 0) ) + ISNULL(bd.Other_Fixed_Costs_Value, 0) AS Total_Cost
                 ,( ISNULL(bd.Other_Bundled_Value, 0) * COALESCE(bd.Budget_Usage, bu.Volume, 0) ) + ISNULL(bd.Other_Fixed_Costs_Value, 0) AS Other_Cost
                 ,COALESCE(bd.Budget_Usage, bu.Volume, 0) AS Usage
                 ,ISNULL(bd.Transmission_Value, 0) * COALESCE(bd.Budget_Usage, bu.Volume, 0) AS Transmission
                 ,ISNULL(bd.Distribution_Value, 0) * COALESCE(bd.Budget_Usage, bu.Volume, 0) AS Distribution
                 ,ISNULL(bd.Transportation_Value, 0) * COALESCE(bd.Budget_Usage, bu.Volume, 0) AS Transportation
                 ,@Budget_Id AS Budget_Id
                 ,ISNULL(bd.Other_Bundled_Value, 0) AS Other_Unit
                 ,ISNULL(bd.Other_Fixed_Costs_Value, 0) Other_Fixed
                 ,ISNULL(bd.Rates_Tax_Value, 0) + ISNULL(Sourcing_Tax_Value, 0) * COALESCE(bd.Budget_Usage, bu.Volume, 0) AS Taxes
              FROM
                  ( SELECT
                        ch.Client_Hier_Id
                       ,ch.Site_Id
                       ,ch.Site_Name
                       ,cha.Account_Id
                       ,ba.Budget_Account_Id
                       ,b.Commodity_Type_Id
                       ,ch.Sitegroup_Id
                       ,ch.Sitegroup_Name
                    FROM
                        Core.Client_Hier ch
                        INNER JOIN Core.Client_Hier_Account cha
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.Budget_Account ba
                              ON ba.Account_Id = cha.Account_Id
                        INNER JOIN dbo.Budget b
                              ON b.Budget_Id = ba.Budget_Id
                    WHERE
                        ch.Client_Id = @Client_Id
                        AND ( ch.Sitegroup_Id = @Sitegroup_Id
                              OR @Sitegroup_Id IS NULL )
                        AND ( @Site_Client_Hier_Id IS NULL
                              OR ch.Client_Hier_id = @Site_Client_Hier_Id )
                        AND ch.Division_Not_Managed = 0
                        AND ch.Site_Not_Managed = 0
                        AND ch.Site_Closed = 0
                        AND cha.Account_Not_Managed = 0
                        AND cha.Account_Type = 'Utility'
                        AND b.Budget_Id = @Budget_Id
                        AND ba.IS_DELETED = 0
                    GROUP BY
                        ch.Client_Hier_Id
                       ,ch.Site_Id
                       ,ch.Site_Name
                       ,cha.Account_Id
                       ,b.Commodity_Type_Id
                       ,ba.Budget_Account_Id
                       ,ch.Sitegroup_Id
                       ,ch.Sitegroup_Name ) cha
                  LEFT OUTER JOIN dbo.Budget_Details bd
                        ON bd.Budget_Account_Id = cha.Budget_Account_Id
                  LEFT OUTER JOIN dbo.Budget_Usage bu
                        ON bu.Account_Id = cha.Account_Id
                           AND bd.Month_Identifier = bu.Month_Identifier
                           AND cha.Commodity_Type_Id = bu.Commodity_Type_Id
              GROUP BY
                  cha.Site_Id
                 ,cha.Site_Name
                 ,cha.Client_Hier_Id
                 ,cha.Sitegroup_Id
                 ,cha.Sitegroup_Name
                 ,cha.BUDGET_ACCOUNT_ID
                 ,CONVERT(VARCHAR(7), bd.Month_Identifier, 111)
                 ,ISNULL(bd.Variable_Value * ISNULL(bd.Budget_Usage, bu.Volume), 0)
                 ,( ( ISNULL(bd.Variable_Value, 0) + ISNULL(bd.Rates_Tax_Value, 0) + ISNULL(bd.Sourcing_Tax_Value, 0) + ISNULL(bd.Transportation_Value, 0) + ISNULL(bd.Transmission_Value, 0) + ISNULL(bd.Distribution_Value, 0) + ISNULL(bd.Other_Bundled_Value, 0) ) * COALESCE(bd.Budget_Usage, bu.Volume, 0) ) + ISNULL(bd.Other_Fixed_Costs_Value, 0)
                 ,( ISNULL(bd.Other_Bundled_Value, 0) * COALESCE(bd.Budget_Usage, bu.Volume, 0) ) + ISNULL(bd.Other_Fixed_Costs_Value, 0)
                 ,COALESCE(bd.Budget_Usage, bu.Volume, 0)
                 ,ISNULL(bd.Transmission_Value, 0) * COALESCE(bd.Budget_Usage, bu.Volume, 0)
                 ,ISNULL(bd.Distribution_Value, 0) * COALESCE(bd.Budget_Usage, bu.Volume, 0)
                 ,ISNULL(bd.Transportation_Value, 0) * COALESCE(bd.Budget_Usage, bu.Volume, 0)
                 ,ISNULL(bd.Other_Bundled_Value, 0)
                 ,ISNULL(bd.Other_Fixed_Costs_Value, 0)
                 ,ISNULL(bd.Rates_Tax_Value, 0) + ISNULL(Sourcing_Tax_Value, 0) * COALESCE(bd.Budget_Usage, bu.Volume, 0) ) x
            JOIN dbo.Consumption_Unit_Conversion uc
                  ON uc.Base_Unit_Id = @MMBtu_UOM_Type_Id
                     AND uc.Converted_Unit_Id = @UOM_Type_Id
      GROUP BY
            x.Site_Id
           ,x.Site_Name
           ,x.Client_Hier_Id
           ,x.[Month]
           ,x.Budget_Id
           ,uc.Conversion_Factor
           ,x.Sitegroup_Id
           ,x.Sitegroup_Name
      ORDER BY
            x.Site_Name
           ,x.[Month]    
    
END;  
  
;
;
GO



GRANT EXECUTE ON  [dbo].[BudgetDetail_SearchDetailsForBudgetVariance_By_Site_For_Client_Division_Site] TO [CBMSApplication]
GO
