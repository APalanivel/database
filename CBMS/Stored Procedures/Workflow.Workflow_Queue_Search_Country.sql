SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:    [Workflow].[Workflow_Queue_Search_Country]  
DESCRIPTION:   
------------------------------------------------------------     
 INPUT PARAMETERS:      
 Name   DataType  Default Description      
 @CountryId int      
 @Key_Word varchar(200)    
 @Start_Index INT    
 @End_Index INT   
------------------------------------------------------------      
 OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
 USAGE EXAMPLES:      
EXEC [Queue].[Queue_Country_GetAll]    
EXEC[Queue].[Queue_Country_GetAll] 84    
EXEC[Queue].[Queue_Country_GetAll] null,'al'    
EXEC[Queue].[Queue_Country_GetAll] NULL,NULL,1,75    
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
TRK   Ramakrishna  Summit Energy   
   
 MODIFICATIONS       
 Initials Date   Modification      
------------------------------------------------------------      
 TRK    AUG-2019  Created  
  
******/  
   
CREATE PROCEDURE [Workflow].[Workflow_Queue_Search_Country]    
(       
 @CountryId int = null,       
 @Key_Word varchar(200) = null,      
 @Start_Index INT = 1,      
 @End_Index INT = 2147483647      
)      
AS      
BEGIN      
 SET NOCOUNT ON;      
 WITH CTE_Count      
 AS      
 (      
 SELECT        
  country_id,       
  country_name,      
  ROW_NUMBER() OVER (ORDER BY  country_id,country_name) AS Row_Num      
 FROM       
  country       
 WHERE       
  country_id = isNull(@CountryId, country_id) AND  country_name Like '%' + isNull(@Key_Word, country_name) + '%'      
  )       
  SELECT  CN.country_id,       
    CN.country_name       
  FROM CTE_Count CN WHERE Row_Num BETWEEN @Start_Index  AND     @End_Index      
        ORDER BY country_name      
                  
      
END        
GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_Search_Country] TO [CBMSApplication]
GO
