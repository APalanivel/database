SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.[CBMS_PREFER_NOT_TODO_BUSINESS_P]


DESCRIPTION: Inserts data into the  PREFER_NOT_TODO_BUSINESS table.

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------------    
@vendorId               int,
@divisionId             int
    
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
exec CBMS_PREFER_NOT_TODO_BUSINESS_P @vendorid = 3051, @divisionId = 2968


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates
 DMR		  09/10/2010 Modified for Quoted_Identifier
	  

******/
CREATE PROCEDURE dbo.CBMS_PREFER_NOT_TODO_BUSINESS_P

@vendorId int,
@divisionId int


AS




INSERT INTO PREFER_NOT_TODO_BUSINESS (
										VENDOR_ID,
										DIVISION_ID
									 ) 
							 VALUES (
										@vendorId,
										@divisionId
									)

RETURN
GO
GRANT EXECUTE ON  [dbo].[CBMS_PREFER_NOT_TODO_BUSINESS_P] TO [CBMSApplication]
GO
