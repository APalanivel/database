SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--exec UPDATE_MARKET_FORECAST_DETAILS_P 2,1,1,'Risk Tolerance - 25',2005,'03/28/2007','Canadian natural gas'

CREATE            PROCEDURE dbo.UPDATE_MARKET_FORECAST_DETAILS_P

@QuaterYear int,
@LowPrice decimal(32,16),
@highPrice decimal(32,16),
@risk_tolerance varchar(50),
@year int,
@asOfDate Datetime,
@marketName varchar(50)


AS
begin
	set nocount on
declare @risk_tolerance_id int,@marketId int,@recCount int
select @risk_tolerance_id = (select entity_id from entity where entity_name = @risk_tolerance and entity_type=1063)
select @marketId = (select rm_market_id from rm_market where rm_market_name=@marketName)

SELECT @recCount= count(*) from rm_market_forecast
WHERE 
	  rm_market_id = @marketId AND 
	  ASofDate = @asOfDate and
          forecast_quarter = @QuaterYear and
          forecast_year = @year and
	  RISK_TOLERANCE_TYPE_ID = @risk_tolerance_id
        
if(@recCount =0)
begin
     insert into rm_market_forecast (RM_MARKET_ID,
                                     Hi_RANGE_PRICE,
				     LOW_RANGE_PRICE,
				     RISK_TOLERANCE_TYPE_ID,
                                     FORECAST_YEAR,
                                     FORECAST_QUARTER,
                                     AsofDate)
	                     values(@marketId,
                                    @highPrice,
                                    @LowPrice,
	                            @risk_tolerance_id,
                                    @year,
                                    @QuaterYear,
                                    @asOfDate) 
                             --where FORECAST_YEAR = @year 
                                 --  AND ASofDate = @asOfDate
end
else
begin
	UPDATE rm_market_forecast SET 
	        rm_market_id = @marketId,
		LOW_RANGE_PRICE = @LowPrice,
		HI_RANGE_PRICE = @highPrice
		--RISK_TOLERANCE_TYPE_ID = @risk_tolerance_id
	
	WHERE 
		FORECAST_YEAR = @year AND 
		FORECAST_QUARTER = @QuaterYear and
                ASofDate = @asOfDate and 
		RISK_TOLERANCE_TYPE_ID = @risk_tolerance_id

end

end
GO
GRANT EXECUTE ON  [dbo].[UPDATE_MARKET_FORECAST_DETAILS_P] TO [CBMSApplication]
GO
