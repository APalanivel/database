SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 dbo.Get_Cbms_Vendors
   
DESCRIPTION:  
  Get Client names based on State / Vendor
  
INPUT PARAMETERS:  
 Name			DataType  Default Description  
------------------------------------------------------------  
  @Start_Index  INT 
  @End_Index    INT                
  
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  

 EXEC dbo.Get_Cbms_Vendors 1,100
 
AUTHOR			    INITIALS:  
 Initials			Name  
------------------------------------------------------------  
 SP                 Srinivas patchava
  
MODIFICATIONS:  
 Initials	Date		Modification  
------------------------------------------------------------  
SP			07-06-2019	Created
******/

CREATE PROCEDURE [dbo].[Get_Cbms_Vendors]
     (
         @Start_Index INT = 1
         , @End_Index INT = 2147483647
     )
AS
    BEGIN

        SET NOCOUNT ON;
        WITH Cte_Vendors
        AS (
               SELECT
                    VENDOR_ID
                    , VENDOR_NAME
                    , Row_Num = ROW_NUMBER() OVER (ORDER BY
                                                       v.VENDOR_ID)
                    , Total_Rows = COUNT(1) OVER ()
               FROM
                    dbo.VENDOR v
           )
        SELECT
            ven.VENDOR_ID
            , ven.VENDOR_NAME
            , ven.Row_Num
            , ven.Total_Rows
        FROM
            Cte_Vendors ven
        WHERE
            ven.Row_Num BETWEEN @Start_Index
                        AND     @End_Index
        ORDER BY
            VENDOR_NAME;

    END;

GO
GRANT EXECUTE ON  [dbo].[Get_Cbms_Vendors] TO [CBMSApplication]
GO
