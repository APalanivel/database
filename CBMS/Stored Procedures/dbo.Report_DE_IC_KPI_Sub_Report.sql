SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.Report_DE_IC_KPI_Sub_Report            
                        
 DESCRIPTION:                        
			This will allow the managers to gain a better understanding of the scope of officers and their allocations of  chase.
			                  
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                       
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            


   EXEC [dbo].[Report_DE_IC_KPI_Sub_Report] 
      @Start_Dt = '2017-01-01'
     ,@End_Dt = '2017-02-01'
     ,@User_Info_Id = 7789
       
    
   EXEC [dbo].[Report_DE_IC_KPI_Sub_Report] 
      @Start_Dt = '2016-02-01'
     ,@End_Dt = '2016-02-28'
     ,@User_Info_Id = 7789
     ,@Input_Type = 'ICR_All'

      
   EXEC [dbo].[Report_DE_IC_KPI_Sub_Report] 
      @Start_Dt = '2016-02-01'
     ,@End_Dt = '2016-02-28'
     ,@User_Info_Id = 7789
     ,@Input_Type = 'ICR_Processed'
 
   EXEC [dbo].[Report_DE_IC_KPI_Sub_Report] 
      @Start_Dt = '2016-02-01'
     ,@End_Dt = '2016-02-28'
     ,@User_Info_Id = 7789
     ,@Input_Type = 'ICRs_Brought_Forward'
           
   EXEC [dbo].[Report_DE_IC_KPI_Sub_Report] 
      @Start_Dt = '2017-02-01'
     ,@End_Dt = '2017-02-28'
     ,@User_Info_Id = 7789
     ,@Input_Type = 'ICRs_Total'
      
   EXEC [dbo].[Report_DE_IC_KPI_Sub_Report] 
      @Start_Dt = '2017-02-01'
     ,@End_Dt = '2017-02-28'
     ,@User_Info_Id = 7789
     ,@Input_Type = 'never_created_ICR'
  
   EXEC [dbo].[Report_DE_IC_KPI_Sub_Report] 
      @Start_Dt = '2017-02-01'
     ,@End_Dt = '2017-02-28'
     ,@User_Info_Id = 7789
     ,@Input_Type = 'Other_than_UBM_Resolved'
         
   EXEC [dbo].[Report_DE_IC_KPI_Sub_Report] 
      @Start_Dt = '2017-02-01'
     ,@End_Dt = '2017-02-28'
     ,@User_Info_Id = 7789
     ,@Input_Type = 'Other_than_UBM'

   EXEC [dbo].[Report_DE_IC_KPI_Sub_Report] 
      @Start_Dt = '2017-02-01'
     ,@End_Dt = '2017-02-28'
     ,@User_Info_Id = 7789
     ,@Input_Type = 'Other_than_UBM'

   EXEC [dbo].[Report_DE_IC_KPI_Sub_Report] 
      @Start_Dt = '2017-02-01'
     ,@End_Dt = '2017-02-28'
     ,@User_Info_Id = 7789
     ,@Input_Type = 'ICRs_Carried_Forward'
      
      
     
             
       SELECT top 1 ch.Client_Id FROM core.Client_Hier ch WHERE ch.Client_Name LIKE 'A J Rorato'                    
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam  
 NR					   Narayana Reddy         
        
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2017-01-25       Created for Invoice Tracking.     
 NR					   2018-01-02       SE2017-388 - IC Reports - To excluded the  Demo Clients.                 
          
                       
******/                 
   
   
CREATE  PROCEDURE [dbo].[Report_DE_IC_KPI_Sub_Report]
      ( 
       @Start_Dt DATE
      ,@End_Dt DATE
      ,@User_Info_Id INT
      ,@Input_Type VARCHAR(100)
      ,@Is_Invoice_Collection_Source_UBM INT = 0
      ,@UBM_Id INT = -1 )
AS 
BEGIN


      DECLARE
            @UBM_Source INT
           ,@Vendor_Source INT
           ,@Client_Source INT
           ,@Online INT
           ,@Vendor_Primary_Contact INT
           ,@Client_Primary_Contact INT
           ,@Account_Primary_Contact INT
           ,@Mail_Redirect INT
           ,@Issue_Open_Status INT
           ,@IC_Type_ICR_Code_Id INT
           ,@IC_Type_ICE_Code_Id INT
           ,@IC_Exception_Type_UBM_Missing_Code_Id INT
           ,@ICR_Status_Resolved INT
           ,@ICE_Status_Resolved INT
           ,@ICE_Status_Open INT
           ,@ICR_Status_Open INT
           ,@Exclude_Demo_Client_Type_Id INT
           
      CREATE TABLE #Final_Table
            ( 
             Client_Name VARCHAR(200)
            ,Country_Name VARCHAR(200)
            ,Collection_Start_Dt DATE
            ,Collection_End_Dt DATE
            ,Commodity_Name VARCHAR(MAX)
            ,Account_Type CHAR(8)
            ,Account_Vendor_Name VARCHAR(200)
            ,Account_Number VARCHAR(50) )        
           

   
      SELECT
            @Exclude_Demo_Client_Type_Id = e.ENTITY_ID
      FROM
            dbo.ENTITY e
      WHERE
            ENTITY_NAME = 'Demo'
            AND ENTITY_TYPE = 125
            AND ENTITY_DESCRIPTION = 'Client Type'
            
            
            
      SELECT
            @UBM_Source = MAX(CASE WHEN c.Code_Value = 'UBM' THEN C.Code_Id
                              END)
           ,@Vendor_Source = MAX(CASE WHEN c.Code_Value = 'Vendor' THEN C.Code_Id
                                 END)
           ,@Client_Source = MAX(CASE WHEN c.Code_Value = 'Client' THEN C.Code_Id
                                 END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'InvoiceCollectionSource'
            AND c.Code_Value IN ( 'UBM', 'Vendor', 'Client' )

      SELECT
            @Issue_Open_Status = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            Code_value = 'Open'
            AND cs.Codeset_Name = 'IC Chase Status'

      SELECT
            @IC_Type_ICR_Code_Id = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Invoice Collection Type'
            AND C.Code_Value = 'ICR'
	 
	             
      SELECT
            @IC_Type_ICE_Code_Id = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Invoice Collection Type'
            AND C.Code_Value = 'ICE'
   
      SELECT
            @IC_Exception_Type_UBM_Missing_Code_Id = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'IC Exception Type'
            AND C.Code_Value = 'UBM Missing'
            
      SELECT
            @ICR_Status_Resolved = MAX(CASE WHEN c.Code_Value = 'Resolved' THEN c.Code_Id
                                       END)
           ,@ICR_Status_Open = MAX(CASE WHEN c.Code_Value = 'Open' THEN c.Code_Id
                                   END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            Code_value IN ( 'Resolved', 'Open' )
            AND cs.Codeset_Name = 'ICR Status'
                           
      SELECT
            @ICE_Status_Resolved = MAX(CASE WHEN c.Code_Value = 'Resolved' THEN c.Code_Id
                                       END)
           ,@ICE_Status_Open = MAX(CASE WHEN c.Code_Value = 'Open' THEN c.Code_Id
                                   END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            Code_value IN ( 'Resolved', 'Open' )
            AND cs.Codeset_Name = 'ICE Status'
                               
      SELECT
            @Online = MAX(CASE WHEN c.Code_Value = 'Online' THEN C.Code_Id
                          END)
           ,@Vendor_Primary_Contact = MAX(CASE WHEN c.Code_Value = 'Vendor Primary Contact' THEN C.Code_Id
                                          END)
           ,@Client_Primary_Contact = MAX(CASE WHEN c.Code_Value = 'Client Primary Contact' THEN C.Code_Id
                                          END)
           ,@Account_Primary_Contact = MAX(CASE WHEN c.Code_Value = 'Account Primary Contact' THEN C.Code_Id
                                           END)
           ,@Mail_Redirect = MAX(CASE WHEN c.Code_Value = 'Mail Redirect' THEN C.Code_Id
                                 END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'InvoiceSourceType'
            AND c.Code_Value IN ( 'Online', 'Vendor Primary Contact', 'Client Primary Contact', 'Account Primary Contact', 'Mail Redirect' )
          
            
      INSERT      INTO #Final_Table
                  ( 
                   Client_Name
                  ,Country_Name
                  ,Collection_Start_Dt
                  ,Collection_End_Dt
                  ,Commodity_Name
                  ,Account_Type
                  ,Account_Vendor_Name
                  ,Account_Number )
                  SELECT
                        ch.Client_Name
                       ,ch.Country_Name
                       ,icq.Collection_Start_Dt
                       ,icq.Collection_End_Dt
                       ,CASE WHEN LEN(Commodities.Commodity_Name) > 0 THEN LEFT(Commodities.Commodity_Name, LEN(Commodities.Commodity_Name) - 1)
                             ELSE Commodities.Commodity_Name
                        END AS Commodity_Name
                       ,cha.Account_Type
                       ,cha.Account_Vendor_Name
                       ,cha.Account_Number
                  FROM
                        dbo.Invoice_Collection_Queue icq
                        INNER JOIN dbo.Invoice_Collection_Account_Config icac
                              ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                        INNER JOIN core.Client_Hier_Account cha
                              ON icac.Account_Id = cha.Account_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        CROSS APPLY ( SELECT
                                          c.Commodity_Name + ','
                                      FROM
                                          Invoice_Collection_Queue icqe
                                          INNER JOIN Invoice_Collection_Account_Config icac1
                                                ON icqe.Invoice_Collection_Account_Config_Id = icac1.Invoice_Collection_Account_Config_Id
                                          INNER JOIN core.Client_Hier_Account cha
                                                ON cha.Account_Id = icac1.Account_Id
                                          INNER JOIN dbo.Commodity c
                                                ON c.Commodity_Id = cha.Commodity_Id
                                      WHERE
                                          icq.Invoice_Collection_Queue_Id = icqe.Invoice_Collection_Queue_Id
                                      GROUP BY
                                          c.Commodity_Name
                        FOR
                                      XML PATH('') ) Commodities ( Commodity_Name )
                  WHERE
                        icq.Invoice_Collection_Queue_Type_Cd = @IC_Type_ICR_Code_Id
                        AND ( icq.Created_Ts BETWEEN @Start_Dt AND @End_Dt )
                        AND icq.Is_Manual = 0
                        AND ( @Is_Invoice_Collection_Source_UBM = -1
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                                INNER JOIN dbo.Account_Invoice_Collection_UBM_Source_Dtl ubm
                                                      ON aic.Account_Invoice_Collection_Source_Id = ubm.Account_Invoice_Collection_Source_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value = 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 1
                                                AND ( @UBM_Id = -1
                                                      OR ubm.UBM_Id = @UBM_Id ) )
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value <> 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 0 ) )
                        AND icac.Invoice_Collection_Officer_User_Id = @User_Info_Id
                        AND ( @Input_Type = 'ICR_All'
                              OR @Input_Type = 'ICRs_Total'
                              OR ( @Input_Type = 'ICRs_Carried_Forward'
                                   AND NOT EXISTS ( SELECT
                                                      1
                                                    FROM
                                                      dbo.Invoice_Collection_Queue icq1
                                                      INNER JOIN dbo.Invoice_Collection_Account_Config icac
                                                            ON icq1.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                    WHERE
                                                      ( icq1.Invoice_Collection_Queue_Type_Cd = @IC_Type_ICR_Code_Id
                                                        AND icq1.Received_Status_Updated_Dt IS NOT NULL )
                                                      AND ( icq1.Created_Ts BETWEEN @Start_Dt AND @End_Dt )
                                                      AND icq1.Is_Manual = 0
                                                      AND icac.Invoice_Collection_Officer_User_Id = @User_Info_Id
                                                      AND @Input_Type = 'ICRs_Carried_Forward'
                                                      AND icq1.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                                                      AND ( @Is_Invoice_Collection_Source_UBM = -1
                                                            OR EXISTS ( SELECT
                                                                              1
                                                                        FROM
                                                                              dbo.Account_Invoice_Collection_Source aic
                                                                              INNER JOIN dbo.Code c
                                                                                    ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                                                              INNER JOIN dbo.Account_Invoice_Collection_UBM_Source_Dtl ubm
                                                                                    ON aic.Account_Invoice_Collection_Source_Id = ubm.Account_Invoice_Collection_Source_Id
                                                                        WHERE
                                                                              Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                                              AND c.Code_Value = 'UBM'
                                                                              AND aic.Is_Primary = 1
                                                                              AND @Is_Invoice_Collection_Source_UBM = 1
                                                                              AND ( @UBM_Id = -1
                                                                                    OR ubm.UBM_Id = @UBM_Id ) )
                                                            OR EXISTS ( SELECT
                                                                              1
                                                                        FROM
                                                                              dbo.Account_Invoice_Collection_Source aic
                                                                              INNER JOIN dbo.Code c
                                                                                    ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                                                        WHERE
                                                                              Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                                              AND aic.Is_Primary = 1
                                                                              AND c.Code_Value <> 'UBM'
                                                                              AND aic.Is_Primary = 1
                                                                              AND @Is_Invoice_Collection_Source_UBM = 0 ) ) ) ) )
                        AND ch.Client_Type_Id <> @Exclude_Demo_Client_Type_Id
                  GROUP BY
                        ch.Client_Name
                       ,ch.Country_Name
                       ,icq.Collection_Start_Dt
                       ,icq.Collection_End_Dt
                       ,CASE WHEN LEN(Commodities.Commodity_Name) > 0 THEN LEFT(Commodities.Commodity_Name, LEN(Commodities.Commodity_Name) - 1)
                             ELSE Commodities.Commodity_Name
                        END
                       ,cha.Account_Type
                       ,cha.Account_Vendor_Name
                       ,cha.Account_Number
                       ,icq.Invoice_Collection_Queue_Id
                  UNION ALL
                  SELECT
                        ch.Client_Name
                       ,ch.Country_Name
                       ,ic.Service_Month AS Collection_Start_Dt
                       ,ic.Service_Month AS Collection_End_Dt
                       ,NULL AS Commodity_Name
                       ,cha.Account_Type
                       ,cha.Account_Vendor_Name
                       ,cha.Account_Number
                  FROM
                        dbo.Account_Invoice_Collection_Month ic
                        INNER JOIN dbo.Invoice_Collection_Account_Config icac
                              ON ic.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                        INNER JOIN core.Client_Hier_Account cha
                              ON icac.Account_Id = cha.Account_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.Invoice_Collection_Queue_Month_Map icqmm
                                     WHERE
                                          ic.Account_Invoice_Collection_Month_Id = icqmm.Account_Invoice_Collection_Month_Id
                                          AND icqmm.Created_Ts BETWEEN @Start_Dt AND @End_Dt )
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map AB
                                     WHERE
                                          ic.Account_Invoice_Collection_Month_Id = AB.Account_Invoice_Collection_Month_Id
                                          AND AB.Created_Ts BETWEEN @Start_Dt AND @End_Dt )
                        AND icac.Invoice_Collection_Officer_User_Id = @User_Info_Id
                        AND ( @Input_Type = 'never_created_ICR' )
                        AND ch.Client_Type_Id <> @Exclude_Demo_Client_Type_Id
                  GROUP BY
                        ch.Client_Name
                       ,ch.Country_Name
                       ,ic.Service_Month
                       ,ic.Service_Month
                       ,cha.Account_Type
                       ,cha.Account_Vendor_Name
                       ,cha.Account_Number
                  UNION ALL
                  SELECT
                        ch.Client_Name
                       ,ch.Country_Name
                       ,icq.Collection_Start_Dt AS Collection_Start_Dt
                       ,icq.Collection_End_Dt AS Collection_End_Dt
                       ,CASE WHEN LEN(Commodities.Commodity_Name) > 0 THEN LEFT(Commodities.Commodity_Name, LEN(Commodities.Commodity_Name) - 1)
                             ELSE Commodities.Commodity_Name
                        END AS Commodity_Name
                       ,cha.Account_Type
                       ,cha.Account_Vendor_Name
                       ,cha.Account_Number
                  FROM
                        dbo.Invoice_Collection_Queue icq
                        INNER JOIN dbo.Invoice_Collection_Account_Config icac
                              ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                        INNER JOIN core.Client_Hier_Account cha
                              ON icac.Account_Id = cha.Account_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        CROSS APPLY ( SELECT
                                          c.Commodity_Name + ','
                                      FROM
                                          Invoice_Collection_Queue icqe
                                          INNER JOIN Invoice_Collection_Account_Config icac1
                                                ON icqe.Invoice_Collection_Account_Config_Id = icac1.Invoice_Collection_Account_Config_Id
                                          INNER JOIN core.Client_Hier_Account cha
                                                ON cha.Account_Id = icac1.Account_Id
                                          INNER JOIN dbo.Commodity c
                                                ON c.Commodity_Id = cha.Commodity_Id
                                      WHERE
                                          icq.Invoice_Collection_Queue_Id = icqe.Invoice_Collection_Queue_Id
                                      GROUP BY
                                          c.Commodity_Name
                        FOR
                                      XML PATH('') ) Commodities ( Commodity_Name )
                  WHERE
                        icq.Invoice_Collection_Queue_Type_Cd = @IC_Type_ICR_Code_Id
                        AND icq.Status_Cd = @ICR_Status_Open
                        AND icq.Created_Ts < @Start_Dt
                        AND icq.Is_Manual = 0
                        AND ( @Is_Invoice_Collection_Source_UBM = -1
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                                INNER JOIN dbo.Account_Invoice_Collection_UBM_Source_Dtl ubm
                                                      ON aic.Account_Invoice_Collection_Source_Id = ubm.Account_Invoice_Collection_Source_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value = 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 1
                                                AND ( @UBM_Id = -1
                                                      OR ubm.UBM_Id = @UBM_Id ) )
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value <> 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 0 ) )
                        AND icac.Invoice_Collection_Officer_User_Id = @User_Info_Id
                        AND ( @Input_Type = 'ICRs_Brought_Forward'
                              OR @Input_Type = 'ICRs_Total' )
                        AND ch.Client_Type_Id <> @Exclude_Demo_Client_Type_Id
                  GROUP BY
                        ch.Client_Name
                       ,ch.Country_Name
                       ,icq.Collection_Start_Dt
                       ,icq.Collection_End_Dt
                       ,CASE WHEN LEN(Commodities.Commodity_Name) > 0 THEN LEFT(Commodities.Commodity_Name, LEN(Commodities.Commodity_Name) - 1)
                             ELSE Commodities.Commodity_Name
                        END
                       ,cha.Account_Type
                       ,cha.Account_Vendor_Name
                       ,cha.Account_Number
                       ,icq.Invoice_Collection_Queue_Id
                  UNION ALL
                  SELECT
                        ch.Client_Name
                       ,ch.Country_Name
                       ,icq.Collection_Start_Dt AS Collection_Start_Dt
                       ,icq.Collection_End_Dt AS Collection_End_Dt
                       ,CASE WHEN LEN(Commodities.Commodity_Name) > 0 THEN LEFT(Commodities.Commodity_Name, LEN(Commodities.Commodity_Name) - 1)
                             ELSE Commodities.Commodity_Name
                        END AS Commodity_Name
                       ,cha.Account_Type
                       ,cha.Account_Vendor_Name
                       ,cha.Account_Number
                  FROM
                        dbo.Invoice_Collection_Queue icq
                        INNER JOIN dbo.Invoice_Collection_Account_Config icac
                              ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                        INNER JOIN core.Client_Hier_Account cha
                              ON icac.Account_Id = cha.Account_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        CROSS APPLY ( SELECT
                                          c.Commodity_Name + ','
                                      FROM
                                          Invoice_Collection_Queue icqe
                                          INNER JOIN Invoice_Collection_Account_Config icac1
                                                ON icqe.Invoice_Collection_Account_Config_Id = icac1.Invoice_Collection_Account_Config_Id
                                          INNER JOIN core.Client_Hier_Account cha
                                                ON cha.Account_Id = icac1.Account_Id
                                          INNER JOIN dbo.Commodity c
                                                ON c.Commodity_Id = cha.Commodity_Id
                                      WHERE
                                          icq.Invoice_Collection_Queue_Id = icqe.Invoice_Collection_Queue_Id
                                      GROUP BY
                                          c.Commodity_Name
                        FOR
                                      XML PATH('') ) Commodities ( Commodity_Name )
                  WHERE
                        icq.Invoice_Collection_Queue_Type_Cd = @IC_Type_ICE_Code_Id
                        AND icq.Status_Cd = @ICE_Status_Resolved
                        AND icq.Invoice_Collection_Exception_Type_Cd <> @IC_Exception_Type_UBM_Missing_Code_Id
                        AND ( icq.Created_Ts BETWEEN @Start_Dt AND @End_Dt )
                        AND icq.Is_Manual = 0
                        AND ( @Is_Invoice_Collection_Source_UBM = -1
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                                INNER JOIN dbo.Account_Invoice_Collection_UBM_Source_Dtl ubm
                                                      ON aic.Account_Invoice_Collection_Source_Id = ubm.Account_Invoice_Collection_Source_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value = 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 1
                                                AND ( @UBM_Id = -1
                                                      OR ubm.UBM_Id = @UBM_Id ) )
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value <> 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 0 ) )
                        AND icac.Invoice_Collection_Officer_User_Id = @User_Info_Id
                        AND @Input_Type = 'Other_than_UBM_Resolved'
                        AND ch.Client_Type_Id <> @Exclude_Demo_Client_Type_Id
                  GROUP BY
                        ch.Client_Name
                       ,ch.Country_Name
                       ,icq.Collection_Start_Dt
                       ,icq.Collection_End_Dt
                       ,CASE WHEN LEN(Commodities.Commodity_Name) > 0 THEN LEFT(Commodities.Commodity_Name, LEN(Commodities.Commodity_Name) - 1)
                             ELSE Commodities.Commodity_Name
                        END
                       ,cha.Account_Type
                       ,cha.Account_Vendor_Name
                       ,cha.Account_Number
                       ,icq.Invoice_Collection_Queue_Id
                  UNION ALL
                  SELECT
                        ch.Client_Name
                       ,ch.Country_Name
                       ,icq.Collection_Start_Dt AS Collection_Start_Dt
                       ,icq.Collection_End_Dt AS Collection_End_Dt
                       ,CASE WHEN LEN(Commodities.Commodity_Name) > 0 THEN LEFT(Commodities.Commodity_Name, LEN(Commodities.Commodity_Name) - 1)
                             ELSE Commodities.Commodity_Name
                        END AS Commodity_Name
                       ,cha.Account_Type
                       ,cha.Account_Vendor_Name
                       ,cha.Account_Number
                  FROM
                        dbo.Invoice_Collection_Queue icq
                        INNER JOIN dbo.Invoice_Collection_Account_Config icac
                              ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                        INNER JOIN core.Client_Hier_Account cha
                              ON icac.Account_Id = cha.Account_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        CROSS APPLY ( SELECT
                                          c.Commodity_Name + ','
                                      FROM
                                          Invoice_Collection_Queue icqe
                                          INNER JOIN Invoice_Collection_Account_Config icac1
                                                ON icqe.Invoice_Collection_Account_Config_Id = icac1.Invoice_Collection_Account_Config_Id
                                          INNER JOIN core.Client_Hier_Account cha
                                                ON cha.Account_Id = icac1.Account_Id
                                          INNER JOIN dbo.Commodity c
                                                ON c.Commodity_Id = cha.Commodity_Id
                                      WHERE
                                          icq.Invoice_Collection_Queue_Id = icqe.Invoice_Collection_Queue_Id
                                      GROUP BY
                                          c.Commodity_Name
                        FOR
                                      XML PATH('') ) Commodities ( Commodity_Name )
                  WHERE
                        icq.Invoice_Collection_Queue_Type_Cd = @IC_Type_ICE_Code_Id
                        AND icq.Status_Cd = @ICE_Status_Open
                        AND icq.Invoice_Collection_Exception_Type_Cd <> @IC_Exception_Type_UBM_Missing_Code_Id
                        AND ( icq.Created_Ts BETWEEN @Start_Dt AND @End_Dt )
                        AND icq.Is_Manual = 0
                        AND ( @Is_Invoice_Collection_Source_UBM = -1
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                                INNER JOIN dbo.Account_Invoice_Collection_UBM_Source_Dtl ubm
                                                      ON aic.Account_Invoice_Collection_Source_Id = ubm.Account_Invoice_Collection_Source_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value = 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 1
                                                AND ( @UBM_Id = -1
                                                      OR ubm.UBM_Id = @UBM_Id ) )
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value <> 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 0 ) )
                        AND icac.Invoice_Collection_Officer_User_Id = @User_Info_Id
                        AND @Input_Type = 'Other_than_UBM'
                        AND ch.Client_Type_Id <> @Exclude_Demo_Client_Type_Id
                  GROUP BY
                        ch.Client_Name
                       ,ch.Country_Name
                       ,icq.Collection_Start_Dt
                       ,icq.Collection_End_Dt
                       ,CASE WHEN LEN(Commodities.Commodity_Name) > 0 THEN LEFT(Commodities.Commodity_Name, LEN(Commodities.Commodity_Name) - 1)
                             ELSE Commodities.Commodity_Name
                        END
                       ,cha.Account_Type
                       ,cha.Account_Vendor_Name
                       ,cha.Account_Number
                       ,icq.Invoice_Collection_Queue_Id
                  UNION ALL
                  SELECT
                        ch.Client_Name
                       ,ch.Country_Name
                       ,icq.Collection_Start_Dt AS Collection_Start_Dt
                       ,icq.Collection_End_Dt AS Collection_End_Dt
                       ,CASE WHEN LEN(Commodities.Commodity_Name) > 0 THEN LEFT(Commodities.Commodity_Name, LEN(Commodities.Commodity_Name) - 1)
                             ELSE Commodities.Commodity_Name
                        END AS Commodity_Name
                       ,cha.Account_Type
                       ,cha.Account_Vendor_Name
                       ,cha.Account_Number
                  FROM
                        dbo.Invoice_Collection_Queue icq
                        INNER JOIN dbo.Invoice_Collection_Account_Config icac
                              ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                        INNER JOIN core.Client_Hier_Account cha
                              ON icac.Account_Id = cha.Account_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.Code icqsc
                              ON icqsc.Code_Id = icq.Status_Cd
                        CROSS APPLY ( SELECT
                                          c.Commodity_Name + ','
                                      FROM
                                          Invoice_Collection_Queue icqe
                                          INNER JOIN Invoice_Collection_Account_Config icac1
                                                ON icqe.Invoice_Collection_Account_Config_Id = icac1.Invoice_Collection_Account_Config_Id
                                          INNER JOIN core.Client_Hier_Account cha
                                                ON cha.Account_Id = icac1.Account_Id
                                          INNER JOIN dbo.Commodity c
                                                ON c.Commodity_Id = cha.Commodity_Id
                                      WHERE
                                          icq.Invoice_Collection_Queue_Id = icqe.Invoice_Collection_Queue_Id
                                      GROUP BY
                                          c.Commodity_Name
                        FOR
                                      XML PATH('') ) Commodities ( Commodity_Name )
                  WHERE
                        icqsc.Code_Value IN ( 'Received', 'Resolved', 'Processed' )
                        AND ( icq.Created_Ts BETWEEN @Start_Dt AND @End_Dt )
                        AND icq.Is_Manual = 0
                        AND icac.Invoice_Collection_Officer_User_Id = @User_Info_Id
                        AND @Input_Type = 'ICRs_Received_Not_Processed'
                        AND ( @Is_Invoice_Collection_Source_UBM = -1
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                                INNER JOIN dbo.Account_Invoice_Collection_UBM_Source_Dtl ubm
                                                      ON aic.Account_Invoice_Collection_Source_Id = ubm.Account_Invoice_Collection_Source_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND c.Code_Value = 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 1
                                                AND ( @UBM_Id = -1
                                                      OR ubm.UBM_Id = @UBM_Id ) )
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Account_Invoice_Collection_Source aic
                                                INNER JOIN dbo.Code c
                                                      ON aic.Invoice_Collection_Source_Cd = c.Code_Id
                                          WHERE
                                                Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                                                AND aic.Is_Primary = 1
                                                AND c.Code_Value <> 'UBM'
                                                AND aic.Is_Primary = 1
                                                AND @Is_Invoice_Collection_Source_UBM = 0 ) )
                        AND ch.Client_Type_Id <> @Exclude_Demo_Client_Type_Id
                  GROUP BY
                        ch.Client_Name
                       ,ch.Country_Name
                       ,icq.Collection_Start_Dt
                       ,icq.Collection_End_Dt
                       ,CASE WHEN LEN(Commodities.Commodity_Name) > 0 THEN LEFT(Commodities.Commodity_Name, LEN(Commodities.Commodity_Name) - 1)
                             ELSE Commodities.Commodity_Name
                        END
                       ,cha.Account_Type
                       ,cha.Account_Vendor_Name
                       ,cha.Account_Number
                                                             
                        
                                              
                 
                                           
      SELECT
            Client_Name
           ,Country_Name
           ,Collection_Start_Dt
           ,Collection_End_Dt
           ,Commodity_Name
           ,Account_Type
           ,Account_Vendor_Name
           ,Account_Number
      FROM
            #Final_Table


      DROP TABLE #Final_Table
                              
END;



;

;
GO

GRANT EXECUTE ON  [dbo].[Report_DE_IC_KPI_Sub_Report] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_IC_KPI_Sub_Report] TO [CBMSApplication]
GO
