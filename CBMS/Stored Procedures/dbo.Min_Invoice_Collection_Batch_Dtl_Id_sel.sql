SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Min_Invoice_Collection_Batch_Dtl_Id_sel         
                
Description:                
   This sproc is used to fill the ICQ Batch Table.        
                             
 Input Parameters:                
    Name										DataType   Default   Description                  
--------------------------------------------------------------------------------------                  
                         
   
 Output Parameters:                      
    Name        DataType   Default   Description                  
--------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
--------------------------------------------------------------------------------------     
  exec dbo.Min_Invoice_Collection_Batch_Dtl_Id_sel
    
Author Initials:                
    Initials  Name                
--------------------------------------------------------------------------------------                  
 RKV    Ravi Kumar Vegesna  
 Modifications:                
    Initials        Date   Modification                
--------------------------------------------------------------------------------------                  
    RKV    2017-02-03  Created For Invoice_Collection.           
               
******/   
CREATE PROCEDURE [dbo].[Min_Invoice_Collection_Batch_Dtl_Id_sel]
      ( 
       @Invoice_Collection_Batch_Type_Cd INT = NULL )
AS 
BEGIN  
      SET NOCOUNT ON;  
        
      
      DECLARE
            @Status_Cd INT
           ,@Invoice_Collection_Batch_Id INT
      
      SELECT
            @Status_Cd = Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Std_Column_Name = 'Invoice_Collection_Queue_Batch_Status_Cd'
            AND c.Code_Value = 'In Progress'
     
      SELECT
            @Invoice_Collection_Batch_Id = MIN(Invoice_Collection_Batch_Id)
      FROM
            dbo.Invoice_Collection_Batch icb
            INNER JOIN dbo.Code sc
                  ON sc.Code_Id = Status_Cd
      WHERE
            sc.Code_Value = 'Pending'
            AND ( @Invoice_Collection_Batch_Type_Cd IS NULL
                  OR icb.Invoice_Collection_Batch_Type_Cd = @Invoice_Collection_Batch_Type_Cd )
            
  
      UPDATE
            Invoice_Collection_Batch
      SET   
            Status_Cd = @Status_Cd
      WHERE
            Invoice_Collection_Batch_Id = @Invoice_Collection_Batch_Id
            
         
      SELECT
            @Invoice_Collection_Batch_Id Invoice_Collection_Batch_Id

END  
  





;
GO
GRANT EXECUTE ON  [dbo].[Min_Invoice_Collection_Batch_Dtl_Id_sel] TO [CBMSApplication]
GO
