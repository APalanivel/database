
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Bucket_Category_Rule_Sel_By_Category_Bucket_Prioriy_Order]  
     
DESCRIPTION: 
	To get the Bucket category rule by Category Bucket Id and Priority order

INPUT PARAMETERS:
NAME					   DATATYPE	DEFAULT DESCRIPTION
------------------------------------------------------------
@Category_Bucket_Master_Id  INT		
@Priority_Order		   INT
@CU_Aggregation_Level_Cd	   VARCHAR(25)	NULL	   Invoice/MultiMonth/Site/Account

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC Bucket_Category_Rule_Sel_By_Category_Bucket_Prioriy_Order 101034, 1,'Site'
	EXEC Bucket_Category_Rule_Sel_By_Category_Bucket_Prioriy_Order 101034, 1

	SELECT * FROM dbo.Bucket_Category_Rule

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
HG		Harihara Suthan G
RKV     Ravi Kumar Vegesna

MODIFICATIONS
INITIALS	DATE		    MODIFICATION
------------------------------------------------------------          
HG		09/08/2011    Created as a part of Additionol data enhancement
RKV     2016-10-04  Added two Parameters Country_Id and State_Id,also added new join 
					Country_State_Category_Bucket_Aggregation_Rule as part of PF Enhancement.  
*/

CREATE PROCEDURE [dbo].[Bucket_Category_Rule_Sel_By_Category_Bucket_Prioriy_Order]
      ( 
       @Category_Bucket_Master_Id INT
      ,@Priority_Order INT
      ,@CU_Aggregation_Level_Cd VARCHAR(25) = NULL
      ,@Country_Id INT = NULL
      ,@State_Id INT = NULL )
AS 
BEGIN

      SET NOCOUNT ON

      SELECT
            r.Category_Bucket_Master_Id
           ,CASE WHEN cscbar.Category_Bucket_Master_Id IS NULL THEN cd.Code_Value
                 ELSE @CU_Aggregation_Level_Cd + 'CountryStateAggregationRule'
            END AS Aggregation_Type
           ,ISNULL(cscbar.Is_Aggregate_Category_Bucket, r.Is_Aggregate_Category_Bucket) Is_Aggregate_Category_Bucket
           ,r.Child_Bucket_Master_Id
           ,ISNULL(csalvcd.Code_Value, cdlvl.Code_Value) AS Aggregation_Level
      FROM
            dbo.Bucket_Category_Rule r
            INNER JOIN dbo.Code cd
                  ON r.Aggregation_Type_CD = cd.Code_Id
            INNER JOIN dbo.Code cdlvl
                  ON r.CU_Aggregation_Level_Cd = cdlvl.Code_Id
            LEFT OUTER JOIN ( dbo.Country_State_Category_Bucket_Aggregation_Rule cscbar
                              INNER JOIN code csalvcd
                                    ON csalvcd.Code_Id = cscbar.CU_Aggregation_Level_Cd
                                       AND csalvcd.Code_Value = @CU_Aggregation_Level_Cd )
                              ON r.Category_Bucket_Master_Id = cscbar.Category_Bucket_Master_Id
                                 AND ( @Country_Id IS NULL
                                       OR cscbar.Country_Id = @Country_Id )
                                 AND ( @State_Id IS NULL
                                       OR cscbar.State_Id = @Country_Id )
                                 AND cscbar.Priority_Order = @Priority_Order
      WHERE
            r.Category_Bucket_Master_Id = @Category_Bucket_Master_Id
           AND @Priority_Order = ISNULL(cscbar.Priority_Order, r.Priority_Order)
            AND ( @CU_Aggregation_Level_Cd IS NULL
                  OR cdlvl.Code_Value = @CU_Aggregation_Level_Cd )

END;
;
GO

GRANT EXECUTE ON  [dbo].[Bucket_Category_Rule_Sel_By_Category_Bucket_Prioriy_Order] TO [CBMSApplication]
GO
