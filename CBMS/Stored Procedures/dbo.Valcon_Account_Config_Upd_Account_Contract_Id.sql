SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 dbo.Valcon_Account_Config_Upd_Account_Contract_Id 
  
DESCRIPTION:  
 It is used to update the Is_config_Completer make as zero when Recalc is deleted . 
  
INPUT PARAMETERS:  
 Name						DataType			Default					Description  
----------------------------------------------------------------------------------------  
 @Account_Id				INT   
 @Contract_Id				INT					  
 @User_Info_Id				INT
  
OUTPUT PARAMETERS:  
 Name					DataType			Default					Description  
----------------------------------------------------------------------------------------  
  
USAGE EXAMPLES:  
----------------------------------------------------------------------------------------

BEGIN TRAN 

EXEC [dbo].[Valcon_Account_Config_Upd_Account_Contract_Id]
    @Account_Id = 1
    , @Contract_Id = 1
    , @User_Info_Id = 49

	
ROLLBACK TRAN 
  
AUTHOR INITIALS:  
 Initials		Name  
----------------------------------------------------------------------------------------  
 NR				Narayana Reddy
    
MODIFICATIONS  
 Initials			Date				Modification  
----------------------------------------------------------------------------------------  
  NR				2019-10-03			Created For Add Contract.  

******/
CREATE PROCEDURE [dbo].[Valcon_Account_Config_Upd_Account_Contract_Id]
    (
        @Account_Id INT
        , @Contract_Id INT
        , @User_Info_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        UPDATE
            vac
        SET
            vac.Is_Config_Complete = 0
            , vac.Last_Change_Ts = GETDATE()
            , vac.Updated_User_Id = @User_Info_Id
        FROM
            dbo.Valcon_Account_Config vac
        WHERE
            vac.Account_Id = @Account_Id
            AND vac.Contract_Id = @Contract_Id
            AND vac.Is_Config_Complete = 1;


    END;
    ;

GO
GRANT EXECUTE ON  [dbo].[Valcon_Account_Config_Upd_Account_Contract_Id] TO [CBMSApplication]
GO
