SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.DMO_Supplier_Account_Meter_Dtls_Sel_By_Utility_Account
           
DESCRIPTION:             
			To get DMO supplier accounts and meters details associated to utility account
			
INPUT PARAMETERS:            
	Name				DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Utility_Account_Id INT
    @StartIndex			INT			1
    @EndIndex			INT			2147483647
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT TOP 10 * FROM Core.Client_Hier_Account chautil 
		INNER JOIN core.Client_Hier_Account chasupp ON chautil.Meter_Id = chasupp.Meter_Id
      WHERE chasupp.Account_Type = 'Supplier' AND chasupp.Supplier_Contract_ID = -1 AND chautil.Account_Type = 'Utility'

            
	EXEC dbo.DMO_Supplier_Account_Meter_Dtls_Sel_By_Utility_Account 499983,1,25
		
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-02-17	Contract placeholder - CP-54 Created
******/

CREATE PROCEDURE [dbo].[DMO_Supplier_Account_Meter_Dtls_Sel_By_Utility_Account]
      ( 
       @Utility_Account_Id INT
      ,@StartIndex INT = 1
      ,@EndIndex INT = 2147483647 )
AS 
BEGIN

      SET NOCOUNT ON;
      
      WITH  Cte_Accs
              AS ( SELECT
                        chasupp.Account_Id
                       ,chasupp.Account_Number
                       ,ROW_NUMBER() OVER ( ORDER BY chasupp.Account_Id ) Row_Num
                       ,COUNT(1) OVER ( ) Total_Rows
                   FROM
                        Core.Client_Hier_Account chautil
                        INNER JOIN core.Client_Hier_Account chasupp
                              ON chautil.Meter_Id = chasupp.Meter_Id
                   WHERE
                        chasupp.Account_Type = 'Supplier'
                        AND chasupp.Supplier_Contract_ID = -1
                        AND chautil.Account_Type = 'Utility'
                        AND chautil.Account_Id = @Utility_Account_Id
                   GROUP BY
                        chasupp.Account_Id
                       ,chasupp.Account_Number)
            SELECT
                  ca.Account_Id AS DMO_Supplier_Account_Id
                 ,ca.Account_Number AS DMO_Supplier_Account_Number
                 ,ca.Total_Rows
            FROM
                  Cte_Accs ca
            WHERE
                  ca.Row_Num BETWEEN @StartIndex AND @EndIndex                    
                                                             
END;
;


;
GO
GRANT EXECUTE ON  [dbo].[DMO_Supplier_Account_Meter_Dtls_Sel_By_Utility_Account] TO [CBMSApplication]
GO
