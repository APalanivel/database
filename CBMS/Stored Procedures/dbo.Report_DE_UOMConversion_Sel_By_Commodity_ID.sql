SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
      
/******      
  
  
NAME:      
dbo.Report_DE_UOMConversion_Sel_By_Commodity_ID     
  
DESCRIPTION:      
Used to get UOM Conversion from COMMODITY_UOM_CONVERSION table for multiple base UOM's   
  
INPUT PARAMETERS:      
Name   DataType Default Description      
------------------------------------------------------------      
@Commodity_Id INT   
@Base_UOM_Cd  VARCHAR(max)    
  
            
OUTPUT PARAMETERS:      
Name   DataType Default Description      
------------------------------------------------------------      
USAGE EXAMPLES:      
------------------------------------------------------------    
  
EXEC dbo.UOMConversion_Sel_By_Commodity_ID 290,'82'
  
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
 AKR	Ashok Kumar Raju       
  
MODIFICATIONS       
Initials	Date		Modification      
------------------------------------------------------------      
AKR		 2012-07-31     Cloned UOMConversion_Sel_By_Commodity_ID to suit for Multiple Base UOM for SSRS Report.
  
******/  

CREATE PROCEDURE [dbo].[Report_DE_UOMConversion_Sel_By_Commodity_ID]
      ( 
       @Commodity_Id AS INT
      ,@Base_UOM_Cd AS VARCHAR(MAX) )
AS 
BEGIN    
    
      SET NOCOUNT ON     
      
      DECLARE @Base_UOM_Cd_List TABLE ( Base_UOM_Cd INT )
      
      INSERT      INTO @Base_UOM_Cd_List
                  ( 
                   Base_UOM_Cd )
                  SELECT
                        us.Segments
                  FROM
                        dbo.ufn_split(@Base_UOM_Cd, ',') us
      
      SELECT
            @Commodity_Id AS BASE_COMMODITY_ID
           ,cm.COMMODITY_NAME AS BASE_COMMODITY_NAME
           ,cuc.Base_UOM_Cd AS BASE_UOM_CD
           ,con.Code_Value AS BASE_UOM_VALUE
           ,cuc.BASE_COMMODITY_ID AS CONVERTED_COMMODITY_ID
           ,cm.COMMODITY_NAME AS CONVERTED_COMMODITY_NAME
           ,cuc.BASE_UOM_CD AS CONVERTED_UOM_CD
           ,c.CODE_VALUE AS CONVERTED_UOM_VALUE
           ,cuc1.CONVERSION_FACTOR
      FROM
            dbo.COMMODITY_UOM_CONVERSION cuc
            LEFT OUTER JOIN dbo.COMMODITY cm
                  ON cm.COMMODITY_ID = cuc.CONVERTED_COMMODITY_ID
            LEFT OUTER JOIN dbo.CODE c
                  ON cuc.CONVERTED_UOM_CD = c.CODE_ID
            LEFT OUTER JOIN dbo.COMMODITY_UOM_CONVERSION cuc1
                  ON cuc.BASE_COMMODITY_ID = cuc1.Converted_Commodity_Id
                     AND cuc.BASE_UOM_CD = cuc1.CONVERTED_UOM_CD
                     AND cuc1.BASE_COMMODITY_ID = @Commodity_Id
                     AND cuc1.Base_UOM_Cd IN ( SELECT
                                                Base_UOM_Cd
                                               FROM
                                                @Base_UOM_Cd_List )
                     AND cuc1.Is_Active = 1
                     AND cuc1.Converted_Commodity_Id = @Commodity_Id
            LEFT OUTER JOIN dbo.Code con
                  ON con.Code_Id = cuc1.Base_UOM_Cd
      WHERE
            cuc.BASE_COMMODITY_ID = cuc.CONVERTED_COMMODITY_ID
            AND cuc.BASE_UOM_CD = cuc.CONVERTED_UOM_CD
            AND cuc.IS_ACTIVE = 1
            AND cm.IS_ACTIVE = 1
            AND c.IS_ACTIVE = 1
            AND cm.IS_SUSTAINABLE = 1  
  
END  

;

GO
GRANT EXECUTE ON  [dbo].[Report_DE_UOMConversion_Sel_By_Commodity_ID] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_UOMConversion_Sel_By_Commodity_ID] TO [CBMSApplication]
GO
