SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        Trade.RM_Hedge_Site_Dtls_Sel_For_Deal_Ticket                      
                        
Description:                        
        To get site's hedge configurations  
                        
Input Parameters:                        
    Name    DataType        Default     Description                          
--------------------------------------------------------------------------------  
 @Client_Id   INT  
    @Commodity_Id  INT  
    @Hedge_Type  INT  
    @Start_Dt DATE  
    @End_Dt  DATE  
    @Contract_Id  VARCHAR(MAX)  
    @Site_Id   INT    NULL  
                        
 Output Parameters:                              
 Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
     
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
 EXEC Trade.RM_Hedge_Site_Dtls_Sel_For_Deal_Ticket  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2017-01-01',@End_Dt='2017-01-01',@Hedge_Type=586,@Contract_Id = '162277,162278'  
  
 EXEC Trade.RM_Hedge_Site_Dtls_Sel_For_Deal_Ticket  @Client_Id = 10003,@Commodity_Id = 291  
  ,@Start_Dt='2019-06-01',@End_Dt='2019-07-01',@Hedge_Type=586,@Contract_Id = '98108,82556'  
    
 EXEC Trade.RM_Hedge_Site_Dtls_Sel_For_Deal_Ticket  @Client_Id = 10003,@Commodity_Id = 291  
  ,@Start_Dt='2019-06-01',@End_Dt='2019-07-01',@Hedge_Type=586,@Contract_Id = '151396,151395' 
    
 EXEC [Trade].[RM_Hedge_Site_Dtls_Sel_For_Deal_Ticket]  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2019-01-01',@End_Dt='2019-02-01',@Hedge_Type=586,@Contract_Id = '148361'  
    
 EXEC [Trade].[RM_Hedge_Site_Dtls_Sel_For_Deal_Ticket]  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2019-01-01',@End_Dt='2019-02-01',@Hedge_Type=586,@Site_Str = '24401,24415'  
    
  
Author Initials:                        
    Initials    Name                        
--------------------------------------------------------------------------------  
    RR          Raghu Reddy     
                         
 Modifications:                        
    Initials	Date		Modification                        
--------------------------------------------------------------------------------  
	RR          02-11-2018  Created GRM
	RR			2019-09-23	GRM-1368 - Added Account_Vendor_Name to select 
                       
******/
CREATE PROCEDURE [Trade].[RM_Hedge_Site_Dtls_Sel_For_Deal_Ticket]
    (
        @Client_Id INT
        , @Commodity_Id INT
        , @Hedge_Type INT
        , @Start_Dt DATE
        , @End_Dt DATE
        , @Contract_Id VARCHAR(MAX) = NULL
        , @Uom_Id INT = NULL
        , @Site_Str VARCHAR(MAX) = NULL
        , @Division_Id VARCHAR(MAX) = NULL
        , @RM_Group_Id VARCHAR(MAX) = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Hedge_Type_Input VARCHAR(200);

        CREATE TABLE #Sites
             (
                 Contract_Id INT
                 , Client_Hier_Id INT
                 , Site_name VARCHAR(200)
                 , Vendor_Id INT
                 , Site_Id INT
                 , Account_Vendor_Name VARCHAR(200)
             );

        DECLARE @Count_Sites INT;

        CREATE TABLE #RM_Group_Sites
             (
                 [Site_Id] INT
             );

        SELECT
            @Hedge_Type_Input = ENTITY_NAME
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_ID = @Hedge_Type;

        INSERT INTO #RM_Group_Sites
             (
                 Site_Id
             )
        SELECT
            ch.Site_Id
        FROM
            dbo.Cbms_Sitegroup_Participant rgd
            INNER JOIN dbo.Code grp
                ON grp.Code_Id = rgd.Group_Participant_Type_Cd
            INNER JOIN dbo.CONTRACT c
                ON c.CONTRACT_ID = rgd.Group_Participant_Id
            INNER JOIN Core.Client_Hier_Account chasupp
                ON chasupp.Supplier_Contract_ID = c.CONTRACT_ID
            INNER JOIN Core.Client_Hier_Account chautil
                ON chasupp.Meter_Id = chautil.Meter_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = chasupp.Client_Hier_Id
        WHERE
            (   EXISTS (   SELECT
                                1
                           FROM
                                dbo.ufn_split(@RM_Group_Id, ',') con
                           WHERE
                                rgd.Cbms_Sitegroup_Id = CAST(Segments AS INT))
                AND grp.Code_Value = 'Contract'
                AND chasupp.Account_Type = 'Supplier'
                AND chautil.Account_Type = 'Utility');

        INSERT INTO #RM_Group_Sites
             (
                 Site_Id
             )
        SELECT
            ch.Site_Id
        FROM
            dbo.Cbms_Sitegroup_Participant rgd
            INNER JOIN dbo.Code grp
                ON grp.Code_Id = rgd.Group_Participant_Type_Cd
            INNER JOIN Core.Client_Hier ch
                ON ch.Site_Id = rgd.Group_Participant_Id
        WHERE
            (   EXISTS (   SELECT
                                1
                           FROM
                                dbo.ufn_split(@RM_Group_Id, ',') con
                           WHERE
                                rgd.Cbms_Sitegroup_Id = CAST(Segments AS INT))
                AND grp.Code_Value = 'Site');





        ----------------------To get site own config  
        INSERT INTO #Sites
             (
                 Contract_Id
                 , Client_Hier_Id
                 , Site_name
                 , Vendor_Id
                 , Site_Id
                 , Account_Vendor_Name
             )
        SELECT
            suppacc.Supplier_Contract_ID
            , ch.Client_Hier_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
            , suppacc.Account_Vendor_Id
            , ch.Site_Id
            , suppacc.Account_Vendor_Name
        FROM
            Core.Client_Hier ch
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON ch.Client_Hier_Id = chob.Client_Hier_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY et
                ON et.ENTITY_ID = chhc.Hedge_Type_Id
            INNER JOIN Trade.RM_Account_Forecast_Volume afv
                ON afv.Client_Hier_Id = ch.Client_Hier_Id
                   AND  chob.Commodity_Id = afv.Commodity_Id
            INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = afv.Uom_Id
                   AND  cuc.CONVERTED_UNIT_ID = ISNULL(chob.RM_Forecast_UOM_Type_Id, afv.Uom_Id)
            INNER JOIN Core.Client_Hier_Account cha
                ON afv.Client_Hier_Id = cha.Client_Hier_Id
                   AND  afv.Account_Id = cha.Account_Id
            INNER JOIN Core.Client_Hier_Account suppacc
                ON cha.Meter_Id = suppacc.Meter_Id
            INNER JOIN dbo.CONTRACT con
                ON con.CONTRACT_ID = suppacc.Supplier_Contract_ID
        WHERE
            ch.Site_Id > 0
            AND ch.Client_Id = @Client_Id
            AND chob.Commodity_Id = @Commodity_Id
            AND (   @Start_Dt BETWEEN chhc.Config_Start_Dt
                              AND     chhc.Config_End_Dt
                    OR  @End_Dt BETWEEN chhc.Config_Start_Dt
                                AND     chhc.Config_End_Dt
                    OR  chhc.Config_Start_Dt BETWEEN @Start_Dt
                                             AND     @End_Dt
                    OR  chhc.Config_End_Dt BETWEEN @Start_Dt
                                           AND     @End_Dt)
            AND (   (   @Hedge_Type_Input = 'Physical'
                        AND et.ENTITY_NAME IN ( 'Physical', 'Physical & Financial' ))
                    OR  (   @Hedge_Type_Input = 'Financial'
                            AND et.ENTITY_NAME IN ( 'Financial', 'Physical & Financial' )))
            AND (   @Contract_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Contract_Id, ',') con
                                   WHERE
                                        suppacc.Supplier_Contract_ID = CAST(Segments AS INT)))
            AND (   @Site_Str IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Site_Str, ',')
                                   WHERE
                                        CAST(Segments AS INT) = ch.Site_Id))
            AND (   @Division_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Division_Id, ',')
                                   WHERE
                                        CAST(Segments AS INT) = ch.Sitegroup_Id))
            AND (   @RM_Group_Id IS NULL
                    OR  EXISTS (SELECT  1 FROM  #RM_Group_Sites rgs WHERE   rgs.Site_Id = ch.Site_Id))
            AND afv.Service_Month BETWEEN @Start_Dt
                                  AND     @End_Dt
            AND suppacc.Account_Type = 'Supplier'
        GROUP BY
            suppacc.Supplier_Contract_ID
            , ch.Client_Hier_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
            , suppacc.Account_Vendor_Id
            , ch.Site_Id
            , suppacc.Account_Vendor_Name;



        ----------------------To get default config                         
        INSERT INTO #Sites
             (
                 Contract_Id
                 , Client_Hier_Id
                 , Site_name
                 , Vendor_Id
                 , Site_Id
                 , Account_Vendor_Name
             )
        SELECT
            suppacc.Supplier_Contract_ID
            , ch.Client_Hier_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
            , suppacc.Account_Vendor_Id
            , ch.Site_Id
            , suppacc.Account_Vendor_Name
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier chclient
                ON ch.Client_Id = chclient.Client_Id
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON chclient.Client_Hier_Id = chob.Client_Hier_Id
                   AND  ch.Country_Id = chob.Country_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY et
                ON et.ENTITY_ID = chhc.Hedge_Type_Id
            INNER JOIN Trade.RM_Account_Forecast_Volume afv
                ON afv.Client_Hier_Id = ch.Client_Hier_Id
                   AND  chob.Commodity_Id = afv.Commodity_Id
            INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = afv.Uom_Id
                   AND  cuc.CONVERTED_UNIT_ID = ISNULL(chob.RM_Forecast_UOM_Type_Id, afv.Uom_Id)
            INNER JOIN Core.Client_Hier_Account cha
                ON afv.Client_Hier_Id = cha.Client_Hier_Id
                   AND  afv.Account_Id = cha.Account_Id
            INNER JOIN Core.Client_Hier_Account suppacc
                ON cha.Meter_Id = suppacc.Meter_Id
            INNER JOIN dbo.CONTRACT con
                ON con.CONTRACT_ID = suppacc.Supplier_Contract_ID
        WHERE
            ch.Site_Id > 0
            AND ch.Client_Id = @Client_Id
            AND chclient.Sitegroup_Id = 0
            AND chob.Commodity_Id = @Commodity_Id
            AND (   @Start_Dt BETWEEN chhc.Config_Start_Dt
                              AND     chhc.Config_End_Dt
                    OR  @End_Dt BETWEEN chhc.Config_Start_Dt
                                AND     chhc.Config_End_Dt
                    OR  chhc.Config_Start_Dt BETWEEN @Start_Dt
                                             AND     @End_Dt
                    OR  chhc.Config_End_Dt BETWEEN @Start_Dt
                                           AND     @End_Dt)
            AND (   (   @Hedge_Type_Input = 'Physical'
                        AND et.ENTITY_NAME IN ( 'Physical', 'Physical & Financial' ))
                    OR  (   @Hedge_Type_Input = 'Financial'
                            AND et.ENTITY_NAME IN ( 'Financial', 'Physical & Financial' )))
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.RM_Client_Hier_Onboard siteob1
                               WHERE
                                    siteob1.Client_Hier_Id = ch.Client_Hier_Id
                                    AND siteob1.Commodity_Id = chob.Commodity_Id)
            AND (   @Contract_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Contract_Id, ',') con
                                   WHERE
                                        suppacc.Supplier_Contract_ID = CAST(Segments AS INT)))
            AND (   @Site_Str IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Site_Str, ',')
                                   WHERE
                                        CAST(Segments AS INT) = ch.Site_Id))
            AND (   @Division_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Division_Id, ',')
                                   WHERE
                                        CAST(Segments AS INT) = ch.Sitegroup_Id))
            AND (   @RM_Group_Id IS NULL
                    OR  EXISTS (SELECT  1 FROM  #RM_Group_Sites rgs WHERE   rgs.Site_Id = ch.Site_Id))
            AND afv.Service_Month BETWEEN @Start_Dt
                                  AND     @End_Dt
            AND suppacc.Account_Type = 'Supplier'
        GROUP BY
            suppacc.Supplier_Contract_ID
            , ch.Client_Hier_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
            , suppacc.Account_Vendor_Id
            , ch.Site_Id
            , suppacc.Account_Vendor_Name;

        SELECT
            ss.Contract_Id
            , con.ED_CONTRACT_NUMBER AS Contract
            , ss.Site_name
            , sscnt.Cnt
            , ss.Vendor_Id
            , ss.Client_Hier_Id
            , ss.Site_Id
            , ss.Account_Vendor_Name
        FROM
            #Sites ss
            INNER JOIN dbo.CONTRACT con
                ON con.CONTRACT_ID = ss.Contract_Id
            INNER JOIN
            (   SELECT
                    scs.Contract_Id
                    , COUNT(DISTINCT scs.Client_Hier_Id) AS Cnt
                FROM
                    #Sites scs
                GROUP BY
                    scs.Contract_Id) sscnt
                ON sscnt.Contract_Id = ss.Contract_Id
        GROUP BY
            ss.Contract_Id
            , ss.Site_name
            , sscnt.Cnt
            , con.ED_CONTRACT_NUMBER
            , ss.Vendor_Id
            , ss.Client_Hier_Id
            , ss.Site_Id
            , ss.Account_Vendor_Name;

    END;





GO
GRANT EXECUTE ON  [Trade].[RM_Hedge_Site_Dtls_Sel_For_Deal_Ticket] TO [CBMSApplication]
GO
