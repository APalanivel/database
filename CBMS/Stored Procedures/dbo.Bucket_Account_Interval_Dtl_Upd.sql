SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:        
        
 dbo.Bucket_Account_Interval_Dtl_Upd        
        
DESCRIPTION:        
        
 This procedure is to Update dbo.Bucket_Account_Interval_Dtl table from given input.        
         
        
INPUT PARAMETERS:        
   Name         DataType     Default     Description        
-----------------------------------------------------------------------------------------------        
      @tvp_Bucket_Account_Interval_Dtl AS tvp_Bucket_Account_Interval_Dtl        
      @Updated_User_Id      INT        
      @Last_Changed_Ts      DATETIME     NULL        
        
OUTPUT PARAMETERS:        
 Name          DataType     Default     Description        
-----------------------------------------------------------------------------------------------            
        
USAGE EXAMPLES:        
-----------------------------------------------------------------------------------------------        
 Example1:- Starts Here    
 BEGIN TRAN     
     
	 DECLARE @tvp tvp_Bucket_Account_Interval_Dtl      
	 INSERT INTO @tvp values (100001,50001,110,'1/1/2013','1/5/2013',100350,150,12,null)      
	      
	 EXEC  dbo.Bucket_Account_Interval_Dtl_Ins @tvp,49,null      
	     
	 SELECT  * FROM Bucket_Account_Interval_Dtl WHERE  Client_Hier_Id = 100001 AND Account_Id = 50001 AND Bucket_Master_Id = 110 AND Service_Start_Dt = '1/1/2013'    
							AND Service_End_Dt = '1/5/2013' AND Data_Source_Cd  = 100350       
	    
	 DELETE FROM @tvp    
	     
	 INSERT INTO @tvp values (100001,50001,110,'1/1/2013','1/5/2013',100350,100,12,123)    
	         
	 EXEC dbo.Bucket_Account_Interval_Dtl_Upd  @tvp,49,NULL        
	                            
	 SELECT  * FROM Bucket_Account_Interval_Dtl WHERE  Client_Hier_Id = 100001  AND Account_Id = 50001 AND  Bucket_Master_Id = 110 AND Service_Start_Dt = '1/1/2013'    
                        AND Service_End_Dt = '1/5/2013' AND Data_Source_Cd  = 100350     
    
 ROLLBACK TRAN       
 Example1:- Ends Here    
        
AUTHOR INITIALS:                    
 Initials  Name        
------------------------------------------------------------------------------------------------        
   BCH         Balaraju        
               
MODIFICATIONS        
 Initials  Date         Modification        
------------------------------------------------------------------------------------------------        
    BCH   2013-03-07   Created        
******/
CREATE PROCEDURE dbo.Bucket_Account_Interval_Dtl_Upd
      ( 
       @Tvp_Bucket_Account_Interval_Dtl tvp_Bucket_Account_Interval_Dtl READONLY
      ,@Updated_User_Id INT
      ,@Last_Changed_Ts DATETIME = NULL )
AS 
BEGIN        

      SET NOCOUNT ON ;        

      UPDATE
            baid
      SET
            baid.Bucket_Daily_Avg_Value = tvp.Bucket_Daily_Avg_Value
           ,baid.Uom_Type_Id = tvp.Uom_Type_Id
           ,baid.Currency_Unit_Id = tvp.Currency_Unit_Id
           ,baid.Updated_User_Id = @Updated_User_Id
           ,baid.Last_Changed_Ts = ISNULL(@Last_Changed_Ts, GETDATE())
      FROM
            @tvp_Bucket_Account_Interval_Dtl tvp
            JOIN dbo.Bucket_Account_Interval_Dtl baid
                  ON tvp.Client_Hier_Id = baid.Client_Hier_Id
                     AND tvp.Account_Id = baid.Account_Id
                     AND tvp.Bucket_Master_Id = baid.Bucket_Master_Id
                     AND tvp.Service_Start_Dt = baid.Service_Start_Dt
                     AND tvp.Service_End_Dt = baid.Service_End_Dt
                     AND tvp.Data_Source_Cd = baid.Data_Source_Cd   

END
;
GO
GRANT EXECUTE ON  [dbo].[Bucket_Account_Interval_Dtl_Upd] TO [CBMSApplication]
GO
