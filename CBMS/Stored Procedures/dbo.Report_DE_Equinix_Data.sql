SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  dbo.Report_DE_Equinix_Data
 
 DESCRIPTION:   
 
	This procedure is used to export the data needed for the Equinix feed. 
 
 INPUT PARAMETERS:
 Name						DataType						Default				Description
------------------------------------------------------------------------------------------------------------------------

 @Client_Name				VARCHAR(200)
 @Index_Name				NVARCHAR(255)
 @Months					INT
 @Fuel_Cell_Sitegroup_Name	VARCHAR (500)  = 'Sites with Fuel Cells'
 OUTPUT PARAMETERS:  

 Name						DataType						Default				Description
------------------------------------------------------------------------------------------------------------------------
  USAGE EXAMPLES:  
------------------------------------------------------------------------------------------------------------------------

      EXEC dbo.Report_DE_Equinix_Data
            @Client_Name = 'Equinix, Inc.'
          , @Index_Name = N'PUE'
          , @Months = 6;

AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------------------------------------------------------------------
 HG			Harihara suthan Ganesan

 MODIFICATIONS   
 Initials	Date		Modification  
------------------------------------------------------------------------------------------------------------------------
 HG			2019-05-02	Modified the service month format to MMM-YY
 HG			2019-05-09	SE2017-739 Modified the service month format to Month-YY
 HG			2019-05-29	Using Natural Gas inplace of 'Fuel Cell - Natural Gas' 
 HG			2020-04-14	SE2017-947, Modified the logic used to return the C/U data
									Any site in Fuel Cell Sites Updated
										Usage -- EP + Alt fuel cell
										Cost -- EP, Alt fuel, NG
									Remaining
										Usage - EP
										Cost - EP
******/
CREATE PROCEDURE [dbo].[Report_DE_Equinix_Data]
(
	@Client_Name				VARCHAR(200)  = 'Equinix, Inc.'
	, @Index_Name				NVARCHAR(255) = 'PUE'
	, @Months					INT			  = 6
	, @Fuel_Cell_Sitegroup_Name VARCHAR(500)  = 'Fuel Cell Sites updated'
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE
		@Client_Id INT
		, @Area_Uom_Id INT
		, @Currency_Unit_Id INT
		, @Currency_Group_Id INT
		, @Start_Dt DATE
		, @End_Dt DATE
		, @SQL_STMT NVARCHAR(MAX) = N''
		, @Fuel_Cell_Sitegroup_Id INT;


	SET @Start_Dt = DATEADD(MM, -@Months, DATEADD(mm, DATEDIFF(mm, 0, GETDATE()), 0));
	SET @End_Dt = DATEADD(DD, -1, DATEADD(mm, DATEDIFF(mm, 0, GETDATE()), 0));

	CREATE TABLE #Client_Hier_Data
	(
		Client_Hier_Id	 INT		  NOT NULL PRIMARY KEY CLUSTERED
		, Site_name		 VARCHAR(200) NOT NULL
		, Country_Name	 VARCHAR(200) NULL
		, Fuel_Cell_Site BIT		  NOT NULL
			  DEFAULT(0)
	);

	CREATE TABLE #Client_Hier_Cost_Usage
	(
		Client_Hier_Id	   INT			   NOT NULL
		, Service_Month	   DATE			   NOT NULL
		, Total_Usage	   DECIMAL(28, 10) NULL
		, Total_Cost	   DECIMAL(28, 10) NULL
		, Currency_Unit_Id INT			   NULL
		,
		PRIMARY KEY CLUSTERED(Client_Hier_Id, Service_Month)
	);

	CREATE TABLE #Last_Year_Client_Hier_Cost_Usage
	(
		Client_Hier_Id	   INT			   NOT NULL
		, Service_Month	   DATE			   NOT NULL
		, Total_Usage	   DECIMAL(28, 10) NULL
		, Total_Cost	   DECIMAL(28, 10) NULL
		, Currency_Unit_Id INT			   NULL
		,
		PRIMARY KEY CLUSTERED(Client_Hier_Id, Service_Month)
	);

	CREATE TABLE #Cu_Invoice_Currency_Unit
	(
		Client_Hier_Id			   INT			NOT NULL
		, Service_Month			   DATE			NOT NULL
		, Invoice_Currency_Unit_Id INT			NOT NULL
		, Currency_Unit_Name	   VARCHAR(255) NOT NULL
	);

	CREATE TABLE #Last_Year_Cu_Invoice_Currency_Unit
	(
		Client_Hier_Id			   INT			NOT NULL
		, Service_Month			   DATE			NOT NULL
		, Invoice_Currency_Unit_Id INT			NULL
		, Currency_Unit_Name	   VARCHAR(255) NULL
	);

	CREATE TABLE #Attribute_Data
	(
		Client_Hier_Id	  INT			NOT NULL
		, Attribute_Name  NVARCHAR(255) NOT NULL
		, Service_Month	  DATE			NOT NULL
		, Attribute_Value VARCHAR(50)	NULL
		,
		PRIMARY KEY CLUSTERED(Client_Hier_Id, Service_Month)
	);


	CREATE TABLE #IP_Site_Client_Hier_Sum
	(
		Client_Hier_Id	INT	 NOT NULL
		, Service_Month DATE NOT NULL
		, Expected_Cnt	INT	 NOT NULL
		, Received_Cnt	INT	 NOT NULL
		,
		PRIMARY KEY CLUSTERED(Client_Hier_Id, Service_Month)
	);

	CREATE TABLE #CHA
	(
		Client_Hier_Id INT NOT NULL
		, Account_Id   INT NOT NULL
		, Commodity_Id INT NOT NULL
		,
		PRIMARY KEY CLUSTERED(Client_Hier_Id, Account_Id, Commodity_Id)
	);

	CREATE TABLE #Buckets
	(
		Bucket_Master_Id		 INT		  NOT NULL
		, Commodity_Name		 VARCHAR(255) NOT NULL
		, Commodity_Id			 INT		  NOT NULL
		, Bucket_Type			 VARCHAR(25)  NOT NULL
		, Kwh_Uom_Id			 INT		  NULL
		, Fuel_Cell_Site_Buckets BIT		  NOT NULL
			  DEFAULT(0)
	);

	INSERT INTO #Buckets
		(
			Bucket_Master_Id
			, Commodity_Name
			, Commodity_Id
			, Bucket_Type
			, Kwh_Uom_Id
			, Fuel_Cell_Site_Buckets
		)
	SELECT
		bm.Bucket_Master_Id
		, com.Commodity_Name
		, com.Commodity_Id
		, bt.Code_Value
		, CASE WHEN bm.Bucket_Name = 'Total Cost' THEN NULL ELSE uom.ENTITY_ID END
		, 0
	FROM
		dbo.Commodity com
		INNER JOIN dbo.Bucket_Master bm
			ON bm.Commodity_Id = com.Commodity_Id
		INNER JOIN dbo.Code bt
			ON bt.Code_Id = bm.Bucket_Type_Cd
		INNER JOIN dbo.ENTITY uom
			ON uom.ENTITY_TYPE = com.UOM_Entity_Type AND uom.ENTITY_NAME = 'kwh'
	WHERE
		com.Commodity_Name = 'Electric Power' AND bm.Bucket_Name IN ( 'Total Usage', 'Total Cost' );

	INSERT INTO #Buckets
		(
			Bucket_Master_Id
			, Commodity_Name
			, Commodity_Id
			, Bucket_Type
			, Kwh_Uom_Id
			, Fuel_Cell_Site_Buckets
		)
	SELECT
		bm.Bucket_Master_Id
		, com.Commodity_Name
		, com.Commodity_Id
		, bt.Code_Value
		, CASE WHEN bm.Bucket_Name = 'Total Cost' THEN NULL ELSE uom.ENTITY_ID END
		, 1
	FROM
		dbo.Commodity com
		INNER JOIN dbo.Bucket_Master bm
			ON bm.Commodity_Id = com.Commodity_Id
		INNER JOIN dbo.Code bt
			ON bt.Code_Id = bm.Bucket_Type_Cd
		INNER JOIN dbo.ENTITY uom
			ON uom.ENTITY_TYPE = com.UOM_Entity_Type AND uom.ENTITY_NAME = 'kwh'
	WHERE
		com.Commodity_Name IN ( 'Electric Power', 'Alternative Energy-Fuel Cell', 'Natural Gas' )
		AND bm.Bucket_Name IN ( 'Volume', 'Total Usage', 'Total Cost' )
		AND (
				com.Commodity_Name <> 'Natural Gas' OR bm.Bucket_Name = 'Total Cost'
			);

	SELECT
		@Client_Id = ch.Client_Id
		, @Currency_Group_Id = ch.Client_Currency_Group_Id
	FROM
		Core.Client_Hier ch
	WHERE
		ch.Client_Name = @Client_Name AND ch.Site_Id = 0 AND ch.Sitegroup_Id = 0;

	SELECT
		@Fuel_Cell_Sitegroup_Id = sg.Sitegroup_Id
	FROM
		dbo.Sitegroup sg
		INNER JOIN dbo.USER_INFO ui
			ON ui.USER_INFO_ID = sg.Owner_User_Id
	WHERE
		sg.Sitegroup_Name = @Fuel_Cell_Sitegroup_Name AND sg.Client_Id = @Client_Id AND ui.USERNAME = 'mkerbow2';

	SELECT
		@Area_Uom_Id = c.Code_Id
	FROM
		dbo.Code c
		INNER JOIN dbo.Codeset cs
			ON c.Codeset_Id = cs.Codeset_Id
	WHERE
		cs.Codeset_Name = 'Area Uom' AND c.Code_Value = 'Sq. m' AND cs.Std_Column_Name = 'Attribute_Uom';

	SELECT @Currency_Unit_Id = cu.CURRENCY_UNIT_ID FROM dbo.CURRENCY_UNIT cu WHERE cu.CURRENCY_UNIT_NAME = 'USD';

	INSERT INTO #Client_Hier_Data
		(
			Client_Hier_Id
			, Site_name
			, Country_Name
			, Fuel_Cell_Site
		)
	SELECT
		ch.Client_Hier_Id
		, ch.Site_name
		, ch.Country_Name
		, 0
	FROM
		Core.Client_Hier ch
	WHERE
		ch.Client_Id = @Client_Id
		AND ch.Site_Id > 0
		AND ch.Sitegroup_Id > 0
		AND EXISTS (
					   SELECT
						   *
					   FROM
						   Core.Client_Hier fch
						   JOIN dbo.Sitegroup_Site sgs
							   ON sgs.Sitegroup_id = fch.Sitegroup_Id
					   WHERE
						   fch.Sitegroup_Name IN ( 'ROW Metered Power Sites', 'NA Metered Power Sites' )
						   AND sgs.Site_id = ch.Site_Id
				   )
		AND NOT EXISTS (
						   SELECT 1 FROM dbo.Sitegroup_Site sgs WHERE sgs.Sitegroup_id = @Fuel_Cell_Sitegroup_Id
																	  AND sgs.Site_id = ch.Site_Id
					   )
	GROUP BY
		ch.Client_Hier_Id
		, ch.Site_name
		, ch.Country_Name;


	INSERT INTO #Client_Hier_Data
		(
			Client_Hier_Id
			, Site_name
			, Country_Name
			, Fuel_Cell_Site
		)
	SELECT
		ch.Client_Hier_Id
		, ch.Site_name
		, ch.Country_Name
		, 1
	FROM
		Core.Client_Hier ch
	WHERE
		ch.Client_Id = @Client_Id
		AND ch.Site_Id > 0
		AND ch.Sitegroup_Id > 0
		AND EXISTS (
					   SELECT 1 FROM dbo.Sitegroup_Site sgs WHERE sgs.Sitegroup_id = @Fuel_Cell_Sitegroup_Id
																  AND sgs.Site_id = ch.Site_Id
				   );



	INSERT INTO #CHA
		(
			Client_Hier_Id
			, Account_Id
			, Commodity_Id
		)
	SELECT
		ch.Client_Hier_Id
		, cha.Account_Id
		, cha.Commodity_Id
	FROM
		#Client_Hier_Data ch
		INNER JOIN #Client_Hier_Data chd
			ON chd.Client_Hier_Id = ch.Client_Hier_Id
		INNER JOIN Core.Client_Hier_Account cha
			ON cha.Client_Hier_Id = ch.Client_Hier_Id
	WHERE
		EXISTS (
				   SELECT 1 FROM #Buckets b WHERE b.Commodity_Id = cha.Commodity_Id
												  AND b.Fuel_Cell_Site_Buckets = chd.Fuel_Cell_Site
			   )
	GROUP BY
		ch.Client_Hier_Id
		, cha.Account_Id
		, cha.Commodity_Id;

	INSERT INTO #Client_Hier_Cost_Usage
		(
			Client_Hier_Id
			, Service_Month
			, Total_Usage
			, Total_Cost
			, Currency_Unit_Id
		)
	SELECT
		cuchs.Client_Hier_Id
		, cuchs.Service_Month
		, SUM(	 CASE WHEN b.Bucket_Type = 'Determinant' THEN
						  cuchs.Bucket_Value * con.CONVERSION_FACTOR
				 END
			 ) AS Total_Usage
		, SUM(	 CASE WHEN b.Bucket_Type = 'Charge' THEN
						  cuchs.Bucket_Value * cuc.CONVERSION_FACTOR
				 END
			 ) AS Total_Cost
		, MAX(cuchs.CURRENCY_UNIT_ID)
	FROM
		CBMS.dbo.Cost_Usage_Site_Dtl cuchs
		INNER JOIN #Client_Hier_Data cha
			ON cha.Client_Hier_Id = cuchs.Client_Hier_Id
		INNER JOIN #Client_Hier_Data chd
			ON chd.Client_Hier_Id = cha.Client_Hier_Id
		INNER JOIN #Buckets b
			ON b.Bucket_Master_Id = cuchs.Bucket_Master_Id AND b.Fuel_Cell_Site_Buckets = chd.Fuel_Cell_Site
		LEFT OUTER JOIN CBMS.dbo.CONSUMPTION_UNIT_CONVERSION con
			ON con.BASE_UNIT_ID = cuchs.UOM_Type_Id AND con.CONVERTED_UNIT_ID = b.Kwh_Uom_Id
		LEFT OUTER JOIN CBMS.dbo.CURRENCY_UNIT_CONVERSION cuc
			ON cuc.CURRENCY_GROUP_ID = @Currency_Group_Id
			   AND cuc.BASE_UNIT_ID = cuchs.CURRENCY_UNIT_ID
			   AND cuc.CONVERTED_UNIT_ID = @Currency_Unit_Id
			   AND cuc.CONVERSION_DATE = cuchs.Service_Month
	WHERE
		(
			@Start_Dt IS NULL OR cuchs.Service_Month >= @Start_Dt
		)
		AND (
				@End_Dt IS NULL OR cuchs.Service_Month <= @End_Dt
			)
	GROUP BY
		cuchs.Client_Hier_Id
		, cuchs.Service_Month;

	INSERT INTO #Last_Year_Client_Hier_Cost_Usage
		(
			Client_Hier_Id
			, Service_Month
			, Total_Usage
			, Total_Cost
			, Currency_Unit_Id
		)
	SELECT
		cuchs.Client_Hier_Id
		, cuchs.Service_Month
		, SUM(	 CASE WHEN b.Bucket_Type = 'Determinant' THEN
						  cuchs.Bucket_Value * con.CONVERSION_FACTOR
				 END
			 ) AS Total_Usage
		, SUM(	 CASE WHEN b.Bucket_Type = 'Charge' THEN
						  cuchs.Bucket_Value * cuc.CONVERSION_FACTOR
				 END
			 ) AS Total_Cost
		, MAX(cuchs.CURRENCY_UNIT_ID)
	FROM
		CBMS.dbo.Cost_Usage_Site_Dtl cuchs
		INNER JOIN #Client_Hier_Data cha
			ON cha.Client_Hier_Id = cuchs.Client_Hier_Id
		INNER JOIN #Client_Hier_Data chd
			ON chd.Client_Hier_Id = cha.Client_Hier_Id
		INNER JOIN #Buckets b
			ON b.Bucket_Master_Id = cuchs.Bucket_Master_Id AND b.Fuel_Cell_Site_Buckets = chd.Fuel_Cell_Site
		LEFT OUTER JOIN CBMS.dbo.CONSUMPTION_UNIT_CONVERSION con
			ON con.BASE_UNIT_ID = cuchs.UOM_Type_Id AND con.CONVERTED_UNIT_ID = b.Kwh_Uom_Id
		LEFT OUTER JOIN CBMS.dbo.CURRENCY_UNIT_CONVERSION cuc
			ON cuc.CURRENCY_GROUP_ID = @Currency_Group_Id
			   AND cuc.BASE_UNIT_ID = cuchs.CURRENCY_UNIT_ID
			   AND cuc.CONVERTED_UNIT_ID = @Currency_Unit_Id
			   AND cuc.CONVERSION_DATE = cuchs.Service_Month
	WHERE
		(
			DATEADD(YY, -1, @Start_Dt) IS NULL OR cuchs.Service_Month >= DATEADD(YY, -1, @Start_Dt)
		)
		AND (
				DATEADD(YY, -1, @End_Dt) IS NULL OR cuchs.Service_Month <= DATEADD(YY, -1, @End_Dt)
			)
	GROUP BY
		cuchs.Client_Hier_Id
		, cuchs.Service_Month;

	WITH cte_Max_Invoice
	AS (   SELECT
			   cha.Client_Hier_Id
			   , cism.SERVICE_MONTH
			   , MAX(cism.CU_INVOICE_ID) AS CU_INVOICE_ID
		   FROM
			   dbo.CU_INVOICE_SERVICE_MONTH cism
			   INNER JOIN dbo.CU_INVOICE inv
				   ON inv.CU_INVOICE_ID = cism.CU_INVOICE_ID
			   INNER JOIN #CHA cha
				   ON cism.Account_ID = cha.Account_Id
			   INNER JOIN #Client_Hier_Data chd
				   ON chd.Client_Hier_Id = cha.Client_Hier_Id
		   WHERE
			   inv.IS_REPORTED = 1
			   AND inv.IS_PROCESSED = 1
			   AND EXISTS (
							  SELECT 1 FROM #Buckets b WHERE b.Commodity_Id = cha.Commodity_Id
															 AND b.Fuel_Cell_Site_Buckets = chd.Fuel_Cell_Site
						  )
			   AND cism.SERVICE_MONTH BETWEEN @Start_Dt AND @End_Dt
		   GROUP BY
			   cha.Client_Hier_Id
			   , cism.SERVICE_MONTH)
	INSERT INTO #Cu_Invoice_Currency_Unit
		(
			Client_Hier_Id
			, Service_Month
			, Invoice_Currency_Unit_Id
			, Currency_Unit_Name
		)
	SELECT
		cem.Client_Hier_Id
		, cem.SERVICE_MONTH
		, ci.CURRENCY_UNIT_ID
		, cu.CURRENCY_UNIT_NAME
	FROM
		dbo.CU_INVOICE ci
		INNER JOIN cte_Max_Invoice cem
			ON cem.CU_INVOICE_ID = ci.CU_INVOICE_ID
		INNER JOIN dbo.CURRENCY_UNIT cu
			ON cu.CURRENCY_UNIT_ID = ci.CURRENCY_UNIT_ID
	GROUP BY
		cem.Client_Hier_Id
		, cem.SERVICE_MONTH
		, ci.CURRENCY_UNIT_ID
		, cu.CURRENCY_UNIT_NAME;

	INSERT INTO #Cu_Invoice_Currency_Unit
		(
			Client_Hier_Id
			, Service_Month
			, Invoice_Currency_Unit_Id
			, Currency_Unit_Name
		)
	SELECT
		chcu.Client_Hier_Id
		, chcu.Service_Month
		, chcu.Currency_Unit_Id
		, cu.CURRENCY_UNIT_NAME
	FROM
		#Client_Hier_Cost_Usage chcu
		JOIN dbo.CURRENCY_UNIT cu
			ON cu.CURRENCY_UNIT_ID = chcu.Currency_Unit_Id
	WHERE
		NOT EXISTS (
					   SELECT
						   1
					   FROM
						   #Cu_Invoice_Currency_Unit icu
					   WHERE
						   icu.Client_Hier_Id = chcu.Client_Hier_Id AND icu.Service_Month = chcu.Service_Month
				   )
	GROUP BY
		chcu.Client_Hier_Id
		, chcu.Service_Month
		, chcu.Currency_Unit_Id
		, cu.CURRENCY_UNIT_NAME;


	INSERT INTO #Last_Year_Cu_Invoice_Currency_Unit
		(
			Client_Hier_Id
			, Service_Month
			, Invoice_Currency_Unit_Id
			, Currency_Unit_Name
		)
	SELECT
		lcu.Client_Hier_Id
		, lcu.Service_Month
		, ic.Invoice_Currency_Unit_Id
		, ic.Currency_Unit_Name
	FROM
		#Last_Year_Client_Hier_Cost_Usage lcu
		JOIN #Cu_Invoice_Currency_Unit ic
			ON ic.Client_Hier_Id = lcu.Client_Hier_Id AND MONTH(ic.Service_Month) = MONTH(lcu.Service_Month);


	WITH cte_Max_Invoice
	AS (   SELECT
			   cha.Client_Hier_Id
			   , cism.SERVICE_MONTH
			   , MAX(cism.CU_INVOICE_ID) AS CU_INVOICE_ID
		   FROM
			   dbo.CU_INVOICE_SERVICE_MONTH cism
			   INNER JOIN dbo.CU_INVOICE inv
				   ON inv.CU_INVOICE_ID = cism.CU_INVOICE_ID
			   INNER JOIN #CHA cha
				   ON cism.Account_ID = cha.Account_Id
			   INNER JOIN #Client_Hier_Data chd
				   ON chd.Client_Hier_Id = cha.Client_Hier_Id
		   WHERE
			   inv.IS_REPORTED = 1
			   AND inv.IS_PROCESSED = 1
			   AND EXISTS (
							  SELECT 1 FROM #Buckets b WHERE b.Commodity_Id = cha.Commodity_Id
															 AND b.Fuel_Cell_Site_Buckets = chd.Fuel_Cell_Site
						  )
			   AND cism.SERVICE_MONTH BETWEEN DATEADD(YY, -1, @Start_Dt) AND DATEADD(YY, -1, @End_Dt)
		   GROUP BY
			   cha.Client_Hier_Id
			   , cism.SERVICE_MONTH)
	INSERT INTO #Last_Year_Cu_Invoice_Currency_Unit
		(
			Client_Hier_Id
			, Service_Month
			, Invoice_Currency_Unit_Id
			, Currency_Unit_Name
		)
	SELECT
		cem.Client_Hier_Id
		, cem.SERVICE_MONTH
		, ci.CURRENCY_UNIT_ID
		, cu.CURRENCY_UNIT_NAME
	FROM
		dbo.CU_INVOICE ci
		INNER JOIN cte_Max_Invoice cem
			ON cem.CU_INVOICE_ID = ci.CU_INVOICE_ID
		INNER JOIN dbo.CURRENCY_UNIT cu
			ON cu.CURRENCY_UNIT_ID = ci.CURRENCY_UNIT_ID
	WHERE
		NOT EXISTS (
					   SELECT
						   1
					   FROM
						   #Last_Year_Cu_Invoice_Currency_Unit lcu
					   WHERE
						   lcu.Client_Hier_Id = cem.Client_Hier_Id AND lcu.Service_Month = cem.SERVICE_MONTH
				   )
	GROUP BY
		cem.Client_Hier_Id
		, cem.SERVICE_MONTH
		, ci.CURRENCY_UNIT_ID
		, cu.CURRENCY_UNIT_NAME;


	INSERT INTO #Last_Year_Cu_Invoice_Currency_Unit
		(
			Client_Hier_Id
			, Service_Month
			, Invoice_Currency_Unit_Id
			, Currency_Unit_Name
		)
	SELECT
		chcu.Client_Hier_Id
		, chcu.Service_Month
		, chcu.Currency_Unit_Id
		, cu.CURRENCY_UNIT_NAME
	FROM
		#Last_Year_Client_Hier_Cost_Usage chcu
		JOIN dbo.CURRENCY_UNIT cu
			ON cu.CURRENCY_UNIT_ID = chcu.Currency_Unit_Id
	WHERE
		NOT EXISTS (
					   SELECT
						   1
					   FROM
						   #Last_Year_Cu_Invoice_Currency_Unit icu
					   WHERE
						   icu.Client_Hier_Id = chcu.Client_Hier_Id AND icu.Service_Month = chcu.Service_Month
				   )
	GROUP BY
		chcu.Client_Hier_Id
		, chcu.Service_Month
		, chcu.Currency_Unit_Id
		, cu.CURRENCY_UNIT_NAME;

	INSERT INTO #IP_Site_Client_Hier_Sum
		(
			Client_Hier_Id
			, Service_Month
			, Expected_Cnt
			, Received_Cnt
		)
	SELECT
		cha.Client_Hier_Id
		, ip.SERVICE_MONTH
		, SUM(CASE WHEN ip.IS_EXPECTED = 1 THEN 1 ELSE 0 END)
		, SUM(CASE WHEN ip.IS_RECEIVED = 1 THEN 1 ELSE 0 END)
	FROM(
			SELECT
				Client_Hier_Id
				, Account_Id
			FROM
				#CHA cha
			GROUP BY
				cha.Client_Hier_Id
				, cha.Account_Id
		) cha
		INNER JOIN CBMS.dbo.INVOICE_PARTICIPATION ip
			ON ip.Client_Hier_Id = cha.Client_Hier_Id AND ip.ACCOUNT_ID = cha.Account_Id
	WHERE
		ip.SERVICE_MONTH BETWEEN @Start_Dt AND @End_Dt
	GROUP BY
		cha.Client_Hier_Id
		, ip.SERVICE_MONTH;

	INSERT INTO #Attribute_Data
		(
			Client_Hier_Id
			, Attribute_Name
			, Service_Month
			, Attribute_Value
		)
	SELECT
		ch.Client_Hier_Id
		, ca.Attribute_Name
		, cat.Service_Month
		, CONVERT(
					 VARCHAR(100)
					 , (CASE WHEN c.Code_Value = 'Area' THEN
								 CAST(CAST((CONVERT(DECIMAL(28, 10), cat.Attribute_Value) * uom.ConversionFactor) AS DECIMAL(28, 3)) AS VARCHAR(100))
							ELSE
								CASE WHEN CAST(CAST(cat.Attribute_Value AS DECIMAL(28, 3)) AS INT) <> CAST(cat.Attribute_Value AS DECIMAL(28, 3)) THEN
										 CAST(CAST(cat.Attribute_Value AS DECIMAL(28, 3)) AS VARCHAR(100))
									ELSE
										CAST(CAST(CAST(cat.Attribute_Value AS DECIMAL(28, 3)) AS INT) AS VARCHAR(50))
								END
						END
					   )
				 ) AS Attribute_Value
	FROM
		#Client_Hier_Data ch
		INNER JOIN DV2.Core.Client_Attribute_Tracking cat
			ON cat.Client_Hier_Id = ch.Client_Hier_Id
		INNER JOIN DV2.Core.Client_Attribute ca
			ON ca.Client_Attribute_id = cat.Client_Attribute_id
		INNER JOIN dbo.Code c
			ON c.Code_Id = ca.Attribute_Type_Cd
		INNER JOIN dbo.Code freq
			ON freq.Code_Id = ca.Frequency_Cd
		LEFT OUTER JOIN DV2.Core.Attribute_Type_UOM_Conversion uom
			ON cat.Is_Currency_UOM = 0
			   AND c.Code_Value IN ( 'Area' )
			   AND uom.Base_UOM_Cd = cat.Attribute_UOM
			   AND uom.Converted_UOM_Cd = @Area_Uom_Id
	WHERE
		ca.Attribute_Name = @Index_Name
		AND ca.CLIENT_ID = @Client_Id
		AND freq.Code_Value <> 'Constant'
		AND (
				@Start_Dt IS NULL OR cat.Service_Month >= @Start_Dt
			)
		AND (
				@End_Dt IS NULL OR cat.Service_Month <= @End_Dt
			);

	SELECT
		@SQL_STMT =
		@SQL_STMT
		+ N'SELECT  
            ch.Site_name AS [Site Name]  
            , datename(MONTH, dd.DATE_D) + ''-''+ right(datename(YEAR,dd.DATE_D),2) AS [Service Month]
	        , CAST(CAST(SUM(ischs.Received_Cnt) AS DECIMAL(10,2)) / CAST(NULLIF(SUM(ischs.Expected_Cnt), 0) AS DECIMAL(10,2)) * 100.0 AS DECIMAL(5,2)) AS [Invoice Participation]  
            
			, MAX(CAST(cu.Total_Usage AS DECIMAL(28, 2))) AS [Total Volume (Calced field)/Usage (kWh/Invoiced Currency unit)]
		    
			, CAST((MAX(CAST(cu.Total_Cost * inv_cur_conv.Conversion_Factor  AS DECIMAL(28, 2)) ))  
              / NULLIF(MAX(CAST(cu.Total_Usage AS DECIMAL(28, 2))), 0) AS DECIMAL(28,3)) AS [Unit Cost (Calced field)(kWh/Invoicing currency unit)]            
			
			, MAX(' + @Index_Name + N') AS ' + @Index_Name
		+ N'   

			, '''' AS [Check Box]
			, '''' AS [Notes:]

			, ISNULL(cicu.Currency_Unit_Name,ly_inv_cu.Currency_Unit_Name) AS [Invoicing Currency] 

            , CAST((MAX(CAST(Last_cu.Total_Cost * inv_cur_conv_ly.Conversion_Factor AS DECIMAL(28, 2))))  
              / NULLIF(MAX(CAST(Last_cu.Total_Usage AS DECIMAL(28, 2))), 0) AS DECIMAL(28,3)) AS [Unit Cost Same Month Last Year (kWh/Invoiced currency)]  

            , CAST(ABS((MAX(CAST(cu.Total_Cost * inv_cur_conv.Conversion_Factor AS DECIMAL(28, 2))))  
              / NULLIF(MAX(CAST(cu.Total_Usage AS DECIMAL(28, 2))), 0)  
              - (MAX(CAST(Last_cu.Total_Cost * inv_cur_conv_ly.Conversion_Factor AS DECIMAL(28, 2))))  
              / NULLIF(MAX(CAST(Last_cu.Total_Usage AS DECIMAL(28, 2))), 0)) AS DECIMAL(28,3)) AS [Unit Cost Variance (E vs. J) (Absolute kWh/Invoiced currency)]  

            , ch.Country_Name AS Country  
        FROM  
            #Client_Hier_Data ch  
            CROSS JOIN meta.DATE_DIM dd  
            LEFT OUTER JOIN #IP_Site_Client_Hier_Sum ischs  
                ON ch.Client_Hier_Id = ischs.Client_Hier_Id  
                   AND  ischs.Service_Month = dd.DATE_D                     

			LEFT OUTER JOIN #Client_Hier_Cost_Usage cu  
                ON cu.Client_Hier_Id = ch.Client_Hier_Id  
                   AND  cu.Service_Month = dd.DATE_D  

            LEFT OUTER JOIN #Cu_Invoice_Currency_Unit cicu  
                ON cicu.Client_Hier_Id = ch.Client_Hier_Id  
                   AND  cicu.Service_Month = dd.DATE_D  

            LEFT OUTER JOIN dbo.CURRENCY_UNIT_CONVERSION inv_cur_conv  
  ON inv_cur_conv.CURRENCY_GROUP_ID = ' + CAST(@Currency_Group_Id AS VARCHAR(100))
		+ N' 
				   AND  inv_cur_conv.BASE_UNIT_ID = ' + CAST(@Currency_Unit_Id AS VARCHAR(100))
		+ N' 
                   AND  inv_cur_conv.CONVERTED_UNIT_ID = ISNULL(cicu.Invoice_Currency_Unit_Id,'
		+ CAST(@Currency_Unit_Id AS VARCHAR(100)) + N')'
		+ N'
                   AND  inv_cur_conv.CONVERSION_DATE = cu.Service_Month			

            LEFT OUTER JOIN #Last_Year_Client_Hier_Cost_Usage Last_cu  
                ON Last_cu.Client_Hier_Id = ch.Client_Hier_Id  
                   AND  Last_cu.Service_Month = DATEADD(YY, -1, dd.DATE_D)

			LEFT OUTER JOIN #Last_Year_Cu_Invoice_Currency_Unit  ly_inv_cu
				ON ly_inv_cu.Client_Hier_Id = ch.Client_Hier_Id
                   AND ly_inv_cu.Service_Month = Last_cu.Service_Month

            LEFT OUTER JOIN dbo.CURRENCY_UNIT_CONVERSION inv_cur_conv_ly  
                ON inv_cur_conv_ly.CURRENCY_GROUP_ID = ' + CAST(@Currency_Group_Id AS VARCHAR(100))
		+ N' 
				   AND  inv_cur_conv_ly.BASE_UNIT_ID = ' + CAST(@Currency_Unit_Id AS VARCHAR(100))
		+ N' 
                   AND  inv_cur_conv_ly.CONVERTED_UNIT_ID = ISNULL(ly_inv_cu.Invoice_Currency_Unit_Id,'
		+ CAST(@Currency_Unit_Id AS VARCHAR(100)) + N')'
		+ N'
                   AND  inv_cur_conv_ly.CONVERSION_DATE = Last_cu.Service_Month			
  
            LEFT OUTER JOIN #Attribute_Data t  
            PIVOT (   MIN(Attribute_Value) 
             FOR Attribute_Name IN (' + @Index_Name
		+ N')) Metric  
                ON ch.Client_Hier_Id = Metric.Client_Hier_Id  
                   AND  (   Metric.Service_Month = dd.DATE_D  
                            OR  Metric.Service_Month = ''1900-01-01'')  
        WHERE  
            dd.DATE_D BETWEEN ''' + CAST(@Start_Dt AS VARCHAR(100)) + N'''  
                      AND   ''' + CAST(@End_Dt AS VARCHAR(100))
		+ N'''   
        GROUP BY  
            ch.Site_name  
            , ch.Country_Name  
			, datename(MONTH, dd.DATE_D) + ''-''+ right(datename(YEAR,dd.DATE_D),2)
            , CONVERT(DATE, dd.DATE_D, 106)  
            ,  ISNULL(cicu.Currency_Unit_Name,ly_inv_cu.Currency_Unit_Name)
        ORDER BY  
            ch.Site_name
            , CONVERT(DATE, dd.DATE_D, 106)';

	PRINT @SQL_STMT;

	EXEC(@SQL_STMT);


	DROP TABLE
		#Client_Hier_Data
		, #CHA;
	DROP TABLE #Attribute_Data;
	DROP TABLE #Client_Hier_Cost_Usage;
	DROP TABLE #Cu_Invoice_Currency_Unit;
	DROP TABLE #IP_Site_Client_Hier_Sum;
	DROP TABLE
		#Last_Year_Client_Hier_Cost_Usage
		, #Last_Year_Cu_Invoice_Currency_Unit;
	DROP TABLE #Buckets;

END;

GO
GRANT EXECUTE ON  [dbo].[Report_DE_Equinix_Data] TO [CBMSApplication]
GO
