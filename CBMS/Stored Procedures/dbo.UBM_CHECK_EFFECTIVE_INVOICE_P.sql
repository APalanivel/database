SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE dbo.UBM_CHECK_EFFECTIVE_INVOICE_P
	@masterLogId int
AS
	set nocount on
	SELECT COUNT(UBM_INVOICE_ID) FROM UBM_INVOICE WHERE UBM_BATCH_MASTER_LOG_ID = @masterLogId
GO
GRANT EXECUTE ON  [dbo].[UBM_CHECK_EFFECTIVE_INVOICE_P] TO [CBMSApplication]
GO
