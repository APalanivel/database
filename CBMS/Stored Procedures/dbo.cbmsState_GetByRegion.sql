SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsState_GetByRegion

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@region_id     	int       	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE     PROCEDURE [dbo].[cbmsState_GetByRegion]
( 
	@MyAccountId int 
	, @region_id int = null
)
AS
BEGIN

	   select st.state_id
		, st.state_name
		, r.region_id
		, r.region_name
		,st.state_name as stateprovince_name
	     from state st
	     join region r on r.region_id = st.region_id
	    where r.region_id = @region_id
	 order by st.state_name

END
GO
GRANT EXECUTE ON  [dbo].[cbmsState_GetByRegion] TO [CBMSApplication]
GO
