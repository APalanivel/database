SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******

NAME:
	CBMS.dbo.GET_EXPECTED_VOLUME_FOR_MARK_TO_MARKET_INDEX_REPORT_P

DESCRIPTION:

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
   @userId			VARCHAR,
   @sessionId		VARCHAR,
   @fromDate		VARCHAR(12),
   @toDate			VARCHAR(12),
   @fromYear		VARCHAR(12),
   @toYear			VARCHAR(12),
   @clientId		INT,
   @siteId			VARCHAR(1000),
   @unitId			INT          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	
	EXEC dbo.GET_EXPECTED_VOLUME_FOR_MARK_TO_MARKET_INDEX_REPORT_P '-1','-1','01/01/2008','12/01/2008','2008','2008',190,'',16

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/24/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on	        	
	
******/



CREATE PROCEDURE dbo.GET_EXPECTED_VOLUME_FOR_MARK_TO_MARKET_INDEX_REPORT_P
    @userId		VARCHAR,
    @sessionId	VARCHAR,
    @fromDate	VARCHAR(12),
    @toDate		VARCHAR(12),
    @fromYear	VARCHAR(12),
    @toYear		VARCHAR(12),
    @clientId	INT,
    @siteId		VARCHAR(1000),
    @unitId		INT
AS 
    BEGIN

        SET NOCOUNT ON ;

        DECLARE @selectQuery1 VARCHAR(8000)
        DECLARE @fromQuery1 VARCHAR(8000)
        DECLARE @whereQuery1 VARCHAR(8000)

        DECLARE @selectQuery2 VARCHAR(8000)
        DECLARE @fromQuery2 VARCHAR(8000)
        DECLARE @whereQuery2 VARCHAR(8000)

        DECLARE @finalQuery1 VARCHAR(8000)
        DECLARE @finalQuery2 VARCHAR(8000)

        IF ( @siteId = '0' ) 
            BEGIN
                SET @siteId = ''
            END

        SET @selectQuery1 = ' SELECT CAST( SUM(volumedetails.VOLUME* consumption.CONVERSION_FACTOR) as decimal(32,3)) TOTAL_VOLUME,
	CONVERT (Varchar(12), volumedetails.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER,entity.ENTITY_NAME  HEDGE_TYPE'

        SET @fromQuery1 = ' FROM RM_FORECAST_VOLUME_DETAILS volumedetails,RM_FORECAST_VOLUME volume,CONSUMPTION_UNIT_CONVERSION consumption,
	RM_ONBOARD_HEDGE_SETUP onboard,	ENTITY entity'		
			 					
        SET @whereQuery1 = ' WHERE volumedetails.RM_FORECAST_VOLUME_ID=volume.RM_FORECAST_VOLUME_ID AND		
		   volume.FORECAST_AS_OF_DATE IN(select MAX(FORECAST_AS_OF_DATE)from  RM_FORECAST_VOLUME
		   where FORECAST_YEAR =' + @fromYear + ' AND CLIENT_ID='
            + STR(@clientId)
            + ' UNION select MAX(FORECAST_AS_OF_DATE)from  RM_FORECAST_VOLUME where FORECAST_YEAR ='
            + @toYear + ' AND
		   CLIENT_ID=' + STR(@clientId)
            + ')AND  volumedetails.MONTH_IDENTIFIER BETWEEN  ' + ''''
            + CONVERT(VARCHAR(12), @FromDate, 101) + '''' + '  
		   AND  ' + '''' + CONVERT(VARCHAR(12), @toDate, 101) + ''''
            + '  AND volumedetails.SITE_ID=onboard.SITE_ID AND
		   onboard.VOLUME_UNITS_TYPE_ID=consumption.BASE_UNIT_ID AND consumption.CONVERTED_UNIT_ID='
            + STR(@unitId) + ' AND
		  onboard.HEDGE_TYPE_ID=entity.ENTITY_ID '


        SET @selectQuery2 = ' SELECT CAST( SUM(volumedetails.VOLUME* consumption.CONVERSION_FACTOR) as decimal(32,3)) TOTAL_VOLUME,
							  CONVERT (Varchar(12), volumedetails.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER,entity.ENTITY_NAME  HEDGE_TYPE'
        SET @fromQuery2 = ' FROM RM_FORECAST_VOLUME_DETAILS volumedetails,RM_FORECAST_VOLUME volume,CONSUMPTION_UNIT_CONVERSION consumption,
							RM_ONBOARD_HEDGE_SETUP onboard,	ENTITY entity'					 					
        SET @whereQuery2 = ' WHERE volumedetails.RM_FORECAST_VOLUME_ID=volume.RM_FORECAST_VOLUME_ID AND		
							 volume.FORECAST_AS_OF_DATE IN(select MAX(FORECAST_AS_OF_DATE)from  RM_FORECAST_VOLUME
							 where FORECAST_YEAR =' + @fromYear + ' AND CLIENT_ID='
            + STR(@clientId)
			+ ' UNION select MAX(FORECAST_AS_OF_DATE)from  RM_FORECAST_VOLUME where FORECAST_YEAR ='
            + @toYear + ' AND CLIENT_ID=' + STR(@clientId)
            + ')AND  volumedetails.MONTH_IDENTIFIER BETWEEN  ' + ''''
            + CONVERT(VARCHAR(12), @FromDate, 101) + '''' + '  
			  AND  ' + '''' + CONVERT(VARCHAR(12), @toDate, 101) + ''''
            + '  AND volumedetails.SITE_ID=onboard.SITE_ID AND
		   onboard.VOLUME_UNITS_TYPE_ID=consumption.BASE_UNIT_ID AND consumption.CONVERTED_UNIT_ID='
            + STR(@unitId) + ' AND
		  onboard.HEDGE_TYPE_ID=entity.ENTITY_ID '

        IF @siteId <> ''
            OR @siteId <> NULL 
            BEGIN
                SELECT  @whereQuery1 = @whereQuery1
								+ ' AND volumedetails.SITE_ID IN( select DISTINCT hedge.SITE_ID from	RM_ONBOARD_HEDGE_SETUP hedge
   				   where hedge.SITE_ID in (' + @siteId
								+ ') AND hedge.INCLUDE_IN_REPORTS=1 AND	
				   hedge.HEDGE_TYPE_ID IN(Select ENTITY_ID from ENTITY where  ENTITY_TYPE=273 AND	
				   ENTITY_NAME like ''financial'')) '

						SELECT  @whereQuery2 = @whereQuery2
								+ ' AND volumedetails.SITE_ID IN( select DISTINCT hedge.SITE_ID from	RM_ONBOARD_HEDGE_SETUP hedge
   				   where hedge.SITE_ID in (' + @siteId
								+ ') AND hedge.INCLUDE_IN_REPORTS=1 AND	
				   hedge.HEDGE_TYPE_ID IN(Select ENTITY_ID from ENTITY where  ENTITY_TYPE=273 AND	
				   ENTITY_NAME like ''Physical'')) '
            END
        SELECT  @finalQuery1 = @selectQuery1 + @fromQuery1 + @whereQuery1
                + ' GROUP BY MONTH_IDENTIFIER,entity.ENTITY_NAME '
		    
        SELECT  @finalQuery2 = @selectQuery2 + @fromQuery2 + @whereQuery2
                + ' GROUP BY MONTH_IDENTIFIER,entity.ENTITY_NAME '

		EXEC ( @finalQuery1 + ' UNION ' + @finalQuery2 )
		
END
GO
GRANT EXECUTE ON  [dbo].[GET_EXPECTED_VOLUME_FOR_MARK_TO_MARKET_INDEX_REPORT_P] TO [CBMSApplication]
GO
