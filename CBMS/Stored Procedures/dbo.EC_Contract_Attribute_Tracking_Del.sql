SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.EC_Contract_Attribute_Tracking_Del           
              
Description:              
        To delete Data to EC_Contract_Attribute_Tracking table.              
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
    @EC_Contract_Attribute_Tracking_Id		INT
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                
	Declare @EC_Contract_Attribute_Tracking_Id_Input int 
	BEGIN TRAN  
	SELECT * FROM dbo.EC_Contract_Attribute_Tracking WHERE EC_Contract_Attribute_Value='test' and Contract_id=10866
	EXEC dbo.EC_Contract_Attribute_Tracking_Ins 
       @Contract_Id =10866
      ,@EC_Contract_Attribute_Id =2
      ,@Start_Dt ='2015-05-06'
      ,@End_Dt ='2015-06-06'
      ,@EC_Contract_Attribute_Value ='test'
      ,@User_Info_Id =100
	SELECT @EC_Contract_Attribute_Tracking_Id_Input=EC_Contract_Attribute_Tracking_Id FROM dbo.EC_Contract_Attribute_Tracking WHERE EC_Contract_Attribute_Value='test' and Contract_id=10866


	SELECT * FROM EC_Contract_Attribute_Tracking WHERE EC_Contract_Attribute_Tracking_Id=@EC_Contract_Attribute_Tracking_Id_Input
			
	EXEC dbo.EC_Contract_Attribute_Tracking_Del 
      @EC_Contract_Attribute_Tracking_Id = @EC_Contract_Attribute_Tracking_Id_Input
    
	SELECT * FROM EC_Contract_Attribute_Tracking WHERE EC_Contract_Attribute_Tracking_Id=@EC_Contract_Attribute_Tracking_Id_Input
	ROLLBACK TRAN                      
             
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2015-09-16		Created For AS400-PII.         
             
******/ 
CREATE PROCEDURE [dbo].[EC_Contract_Attribute_Tracking_Del]
      ( 
       @EC_Contract_Attribute_Tracking_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 
      
      
      DELETE
            emat
      FROM
            dbo.EC_Contract_Attribute_Tracking emat
      WHERE
            emat.EC_Contract_Attribute_Tracking_Id = @EC_Contract_Attribute_Tracking_Id
      
            
END;
;
GO
GRANT EXECUTE ON  [dbo].[EC_Contract_Attribute_Tracking_Del] TO [CBMSApplication]
GO
