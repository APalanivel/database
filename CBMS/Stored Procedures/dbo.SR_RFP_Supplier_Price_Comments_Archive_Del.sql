SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[SR_RFP_Supplier_Price_Comments_Archive_Del]  
     
DESCRIPTION: 
	It Deletes SR RFP Supplier Price Comments Acrhive for Selected 
						SR RFP Supplier Price Comments Archive Id.
      
INPUT PARAMETERS:          
NAME										DATATYPE	DEFAULT		DESCRIPTION          
-------------------------------------------------------------------------------          
@SR_RFP_Supplier_Price_Comments_Archive_Id	INT	

   
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------

	Begin Tran
		EXEC SR_RFP_Supplier_Price_Comments_Archive_Del  206
	Rollback Tran

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR		    30-MAY-10	CREATED

*/

CREATE PROCEDURE dbo.SR_RFP_Supplier_Price_Comments_Archive_Del
   (
    @SR_RFP_Supplier_Price_Comments_Archive_Id INT
   )
AS
BEGIN

    SET NOCOUNT ON;

    DELETE
   	FROM
		dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS_ARCHIVE
	WHERE
		SR_RFP_SUPPLIER_PRICE_COMMENTS_ARCHIVE_ID = @SR_RFP_Supplier_Price_Comments_Archive_Id

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_Supplier_Price_Comments_Archive_Del] TO [CBMSApplication]
GO
