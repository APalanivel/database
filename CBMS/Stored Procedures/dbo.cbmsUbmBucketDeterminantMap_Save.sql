SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 cbms_prod.dbo.cbmsUbmBucketDeterminantMap_Save  
 DESCRIPTION: INPUT PARAMETERS:  
 Name								DataType		 Default				Description  
------------------------------------------------------------------------------------------  
 @ubm_bucket_determinant_map_id		int        null         
 @ubm_id							int                     
 @ubm_bucket_code					varchar(200)              
 @commodity_type_id					int                     
 @bucket_type_id					int  
                    
 OUTPUT PARAMETERS:  
 Name								DataType		 Default				Description  
------------------------------------------------------------------------------------------  
 USAGE EXAMPLES:  
------------------------------------------------------------------------------------------  
EXEC cbmsUbmBucketDeterminantMap_Save 10,1,'E Rate Adj',290,344  
SELECT * FROM ubm_bucket_determinant_map 

AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------------------------------------  
 SSR   Sharad Srivastava 
 RKV   Ravi Kumar Vegesna
 
 MODIFICATIONS   
 Initials Date				Modification  
------------------------------------------------------------------------------------------  
          7/20/2009			Autogenerated script  
    SSR   28/01/2010		Replaced bucket_type_id with bucket_master_id            
    SSR   03/04/2010		Removed Input parameter @MyaccountID  
    RKV   2015-08-28		Added new Table Ubm_Bucket_Determinant_Ec_Invoice_Sub_Bucket_Map insert as part of AS400 PII
	RKV   2018-10-24		D20-165 changed to merge functionality 

******/
CREATE PROCEDURE [dbo].[cbmsUbmBucketDeterminantMap_Save]
    (
        @ubm_bucket_determinant_map_id INT = NULL
        , @ubm_id INT
        , @ubm_bucket_code VARCHAR(200)
        , @commodity_type_id INT
        , @bucket_type_id INT
        , @State_id INT
        , @EC_Invoice_Sub_Bucket_Master_Id INT = NULL
        , @User_Info_Id INT
        , @Ubm_Sub_Bucket_Code VARCHAR(200) = NULL
    )
AS
    BEGIN



        SET NOCOUNT ON;

        BEGIN TRY
            BEGIN TRAN;




            MERGE dbo.UBM_BUCKET_DETERMINANT_MAP Tgt
            USING (   SELECT
                            @ubm_id ubm_id
                            , @ubm_bucket_code ubm_bucket_code
                            , @commodity_type_id commodity_type_id
                            , @bucket_type_id bucket_Master_id) Src
            ON Tgt.UBM_ID = Src.ubm_id
               AND  Tgt.UBM_BUCKET_CODE = Src.ubm_bucket_code
               AND  Tgt.COMMODITY_TYPE_ID = Src.commodity_type_id
            WHEN MATCHED THEN UPDATE SET
                                  Tgt.Bucket_Master_Id = Src.bucket_Master_id
            WHEN NOT MATCHED THEN INSERT (UBM_ID
                                          , UBM_BUCKET_CODE
                                          , COMMODITY_TYPE_ID
                                          , Bucket_Master_Id)
                                  VALUES
                                      (Src.ubm_id
                                       , Src.ubm_bucket_code
                                       , Src.commodity_type_id
                                       , Src.bucket_Master_id);


            SELECT
                @ubm_bucket_determinant_map_id = UBM_BUCKET_DETERMINANT_MAP_ID
            FROM
                dbo.UBM_BUCKET_DETERMINANT_MAP
            WHERE
                UBM_ID = @ubm_id
                AND UBM_BUCKET_CODE = @ubm_bucket_code
                AND COMMODITY_TYPE_ID = @commodity_type_id
                AND Bucket_Master_Id = @bucket_type_id
                AND @ubm_bucket_determinant_map_id IS NULL;




            MERGE dbo.Ubm_Bucket_Determinant_Ec_Invoice_Sub_Bucket_Map Tgt
            USING (   SELECT
                            @ubm_bucket_determinant_map_id ubm_bucket_determinant_map_id
                            , @State_id State_id
                            , @Ubm_Sub_Bucket_Code Ubm_Sub_Bucket_Code
                            , @EC_Invoice_Sub_Bucket_Master_Id EC_Invoice_Sub_Bucket_Master_Id
                            , @User_Info_Id User_Info_Id
                      WHERE
                            @ubm_bucket_determinant_map_id IS NOT NULL
                            AND @EC_Invoice_Sub_Bucket_Master_Id IS NOT NULL
                            AND @Ubm_Sub_Bucket_Code IS NOT NULL) src
            ON Tgt.Ubm_Bucket_Determinant_Map_Id = src.ubm_bucket_determinant_map_id
               AND  Tgt.State_Id = src.State_id
               AND  Tgt.Ubm_Sub_Bucket_Code = src.Ubm_Sub_Bucket_Code
            WHEN MATCHED THEN UPDATE SET
                                  Tgt.EC_Invoice_Sub_Bucket_Master_Id = src.EC_Invoice_Sub_Bucket_Master_Id
            WHEN NOT MATCHED THEN INSERT (Ubm_Bucket_Determinant_Map_Id
                                          , State_Id
                                          , Ubm_Sub_Bucket_Code
                                          , EC_Invoice_Sub_Bucket_Master_Id
                                          , Created_User_Id)
                                  VALUES
                                      (src.ubm_bucket_determinant_map_id
                                       , src.State_id
                                       , src.Ubm_Sub_Bucket_Code
                                       , src.EC_Invoice_Sub_Bucket_Master_Id
                                       , src.User_Info_Id);




            COMMIT TRAN;
        END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRAN;
                END;
            EXEC dbo.usp_RethrowError;
        END CATCH;




        SELECT
            @ubm_bucket_determinant_map_id AS ubm_bucket_determinant_map_id
            , @ubm_id AS ubm_id
            , @ubm_bucket_code AS ubm_bucket_code
            , @commodity_type_id AS commodity_type_id
            , @bucket_type_id AS bucket_type_id;


    END;

GO

GRANT EXECUTE ON  [dbo].[cbmsUbmBucketDeterminantMap_Save] TO [CBMSApplication]
GO
