SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******    
NAME:    
	dbo.Client_Sel_By_Client_Name

DESCRIPTION:    
	Used to search client from client table  

INPUT PARAMETERS:    
Name			DataType	Default Description    
------------------------------------------------------------    
@client_name	VARCHAR(200)  

OUTPUT PARAMETERS:    
Name			DataType	Default Description    
------------------------------------------------------------    
USAGE EXAMPLES:    

	dbo.Client_Sel_By_Client_Name 'a'  

AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
GB   Geetansu Behera    
CMH  Chad Hattabaugh     
HG   Hari  
SKA  Shobhit Kr Agrawal  

MODIFICATIONS     
Initials Date  Modification    
------------------------------------------------------------    
GB      created  
SKA   22-JUL-09 Modified  as per coding standards
SKA   06-SEP-09 Like operator is used as per requirement
GB	  16-SEP-2009 Removed client_id from where condition
CMH	  09/16/2009 Added Check / RaisError for Wildcard 

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.Client_Sel_By_Client_Name
	@User_Id INT,
	@client_name VARCHAR(200)  
AS  

BEGIN  

SET NOCOUNT ON 

	IF substring(@Client_Name,1,1) IN('%', '?')
		RAISERROR('Invalid search Parameter. Like condition cannot begin with wildcard', 16, 0)
		
		SELECT CLIENT_ID,CLIENT_NAME  
		FROM dbo.CLIENT  
		WHERE client_name LIKE  @client_name +'%'
			ORDER BY CLIENT_NAME  
END
GO
GRANT EXECUTE ON  [dbo].[Client_Sel_By_Client_Name] TO [CBMSApplication]
GO
