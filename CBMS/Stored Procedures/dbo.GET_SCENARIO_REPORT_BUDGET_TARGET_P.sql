SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE dbo.GET_SCENARIO_REPORT_BUDGET_TARGET_P 
@userId varchar(10),
@sessionId varchar(20),
@clientId integer,
@divisionId integer,
@siteId integer,
@startDate datetime,
@endDate datetime,
@startYear int,
@endYear int

as
	set nocount on
if @clientId>0 and @divisionId=0 and @siteId=0

SELECT 
		sum(A.priceVolume) priceVolume,
		sum(A.VOLUME) volume,
		CASE 	sum(A.VOLUME)
			WHEN 0 THEN 0.0
			ELSE sum(A.priceVolume)/sum(A.VOLUME)
			END AS BudgetWACOGPRICE,
		--A.SITEID,
		--A.TARGETPRICE,
		DATEPART(MONTH,A.DEALMONTHYEAR) dealTicketMonth,
		DATEPART(YEAR,A.DEALMONTHYEAR) dealTicketYear,
		A.MONTH_IDENTIFIER MONTH_IDENTIFIER

	FROM

	(

	SELECT 
		volumedetails.VOLUME*RM_TARGET_BUDGET.BUDGET_TARGET priceVolume,
		volumedetails.VOLUME VOLUME,
		volumedetails.RM_FORECAST_VOLUME_DETAILS_ID FORECAST_ID,
		volumedetails.SITE_ID SITEID,
		RM_TARGET_BUDGET.BUDGET_TARGET TARGETPRICE,
		RM_TARGET_BUDGET.RM_TARGET_BUDGET_ID TARGETID,
		volumedetails.MONTH_IDENTIFIER DEALMONTHYEAR,
		RM_TARGET_BUDGET.MONTH_IDENTIFIER MI2,
		CONVERT (Varchar(12), RM_TARGET_BUDGET.MONTH_IDENTIFIER, 101)  MONTH_IDENTIFIER

	FROM 
		RM_FORECAST_VOLUME_DETAILS volumedetails,
		RM_FORECAST_VOLUME volume,
		RM_TARGET_BUDGET,
		RM_BUDGET,
		RM_BUDGET_SITE_MAP

	WHERE
		--volumedetails.MONTH_IDENTIFIER=RM_TARGET_BUDGET.MONTH_IDENTIFIER AND
		volumedetails.RM_FORECAST_VOLUME_ID=volume.RM_FORECAST_VOLUME_ID AND
		volume.CLIENT_ID=@clientId AND
		volume.FORECAST_AS_OF_DATE IN
		(
		select MAX(FORECAST_AS_OF_DATE) from  RM_FORECAST_VOLUME where FORECAST_YEAR =@startYear AND CLIENT_ID=@clientId
		UNION 	
		select MAX(FORECAST_AS_OF_DATE) from  RM_FORECAST_VOLUME where FORECAST_YEAR =@endYear AND CLIENT_ID=@clientId
		) 
		AND volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @startDate, 101) AND CONVERT(Varchar(12), @endDate, 101) AND
		RM_BUDGET.client_id=@clientId and
		RM_TARGET_BUDGET.rm_budget_id=RM_BUDGET.rm_budget_id and
		volumedetails.MONTH_IDENTIFIER=RM_TARGET_BUDGET.MONTH_IDENTIFIER and
		RM_BUDGET_SITE_MAP.SITE_ID=volumedetails.site_id and
		--RM_BUDGET.SITE_ID=volumedetails.site_id AND

		RM_TARGET_BUDGET_ID IN
	(

		SELECT 
			RM_TARGET_BUDGET_id
		FROM
			RM_TARGET_BUDGET,
			RM_BUDGET
		WHERE

		RM_BUDGET.RM_BUDGET_ID=RM_TARGET_BUDGET.RM_BUDGET_ID AND
		RM_BUDGET.CLIENT_ID=@clientId AND
		RM_BUDGET.DIVISION_ID IS NULL AND
		RM_BUDGET.SITE_ID IS NULL AND
		RM_BUDGET.IS_BUDGET_APPROVED=1 AND 
		RM_BUDGET.RM_BUDGET_ID IN 
			( 
				select	RM_BUDGET_ID 
				from	RM_BUDGET
				where	CONVERT(Varchar(12),@startDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH										
				UNION										
				select RM_BUDGET_ID 					
				from	RM_BUDGET					
				where CONVERT(Varchar(12), @endDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH
				UNION 	
				select RM_BUDGET_ID 
				from	RM_BUDGET
				where BUDGET_START_MONTH BETWEEN  CONVERT(Varchar(12),@startDate, 101)  AND CONVERT(Varchar(12),@endDate, 101)
				UNION										
				select RM_BUDGET_ID 					
				from	RM_BUDGET					
				where BUDGET_END_MONTH BETWEEN  CONVERT(Varchar(12),@startDate, 101)  AND CONVERT(Varchar(12),@endDate, 101)
			) 
				AND RM_TARGET_BUDGET.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @startDate, 101) AND CONVERT(Varchar(12), @endDate, 101)
	)

	AND
	(
		volumedetails.SITE_ID IN--First consider all sites for this client 
	(
		SELECT	
			SITE.SITE_ID
		FROM	
			SITE,DIVISION,RM_BUDGET_SITE_MAP
		WHERE
			DIVISION.CLIENT_ID=@clientId AND
			SITE.DIVISION_ID=DIVISION.DIVISION_ID and
			SITE.SITE_ID!=RM_BUDGET_SITE_MAP.SITE_ID
			
	)
	AND volumedetails.SITE_ID NOT IN--Exclude those sites which have budget at SiteLevel or DivisionLevel
	(
			SELECT 
			DISTINCT RM_BUDGET.SITE_ID FROM RM_TARGET_BUDGET,RM_BUDGET
			WHERE 
			RM_BUDGET.RM_BUDGET_ID=RM_TARGET_BUDGET.RM_BUDGET_ID AND
			RM_BUDGET.CLIENT_ID=@clientId AND
			RM_BUDGET.SITE_ID IS NOT NULL 
			AND RM_BUDGET.IS_BUDGET_APPROVED=1 AND	RM_BUDGET.RM_BUDGET_ID IN 
			( 
				select	RM_BUDGET_ID 
				from	RM_BUDGET
				where	CONVERT(Varchar(12),@startDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH										
				UNION										
				select RM_BUDGET_ID 					
				from	RM_BUDGET					
				where CONVERT(Varchar(12), @endDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH
				UNION 	
				select RM_BUDGET_ID 
				from	RM_BUDGET
				where BUDGET_START_MONTH BETWEEN  CONVERT(Varchar(12),@startDate, 101)  AND CONVERT(Varchar(12),@endDate, 101)
				UNION										
				select RM_BUDGET_ID 					
				from	RM_BUDGET					
				where BUDGET_END_MONTH BETWEEN  CONVERT(Varchar(12),@startDate, 101)  AND CONVERT(Varchar(12),@endDate, 101)
			) 
				AND RM_TARGET_BUDGET.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @startDate, 101) AND CONVERT(Varchar(12), @endDate, 101)
			UNION

			SELECT SITE_ID FROM SITE 
		
			WHERE 
		
			DIVISION_ID IN
			(
				SELECT 
				DISTINCT RM_BUDGET.DIVISION_ID FROM RM_TARGET_BUDGET,RM_BUDGET
				WHERE 
				RM_BUDGET.RM_BUDGET_ID=RM_TARGET_BUDGET.RM_BUDGET_ID AND
				RM_BUDGET.CLIENT_ID=@clientId AND
				RM_BUDGET.DIVISION_ID IS NOT NULL and
				RM_BUDGET.SITE_ID is NULL 
				AND RM_BUDGET.IS_BUDGET_APPROVED=1 AND	RM_BUDGET.RM_BUDGET_ID IN 
			( 
				select	RM_BUDGET_ID 
				from	RM_BUDGET
				where	CONVERT(Varchar(12),@startDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH										
				UNION										
				select RM_BUDGET_ID 					
				from	RM_BUDGET					
				where CONVERT(Varchar(12), @endDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH
				UNION 	
				select RM_BUDGET_ID 
				from	RM_BUDGET
				where BUDGET_START_MONTH BETWEEN  CONVERT(Varchar(12),@startDate, 101)  AND CONVERT(Varchar(12),@endDate, 101)
				UNION										
				select RM_BUDGET_ID 					
				from	RM_BUDGET					
				where BUDGET_END_MONTH BETWEEN  CONVERT(Varchar(12),@startDate, 101)  AND CONVERT(Varchar(12),@endDate, 101)
			) 
				AND RM_TARGET_BUDGET.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @startDate, 101) AND CONVERT(Varchar(12), @endDate, 101)
		  	)
			
		)
	    )
			

	UNION




	SELECT 
		volumedetails.VOLUME*RM_TARGET_BUDGET.BUDGET_TARGET priceVolume,
		volumedetails.VOLUME,
		volumedetails.RM_FORECAST_VOLUME_DETAILS_ID,
		volumedetails.SITE_ID,
		RM_TARGET_BUDGET.BUDGET_TARGET,
		RM_TARGET_BUDGET.RM_TARGET_BUDGET_ID,
		volumedetails.MONTH_IDENTIFIER,
		RM_TARGET_BUDGET.MONTH_IDENTIFIER,
		CONVERT (Varchar(12), RM_TARGET_BUDGET.MONTH_IDENTIFIER, 101)  MONTH_IDENTIFIER

	FROM 
		RM_FORECAST_VOLUME_DETAILS volumedetails,
		RM_FORECAST_VOLUME volume,
		RM_TARGET_BUDGET,
		RM_BUDGET_SITE_MAP,
		RM_BUDGET

	WHERE
		--volumedetails.MONTH_IDENTIFIER=RM_TARGET_BUDGET.MONTH_IDENTIFIER AND
		volumedetails.RM_FORECAST_VOLUME_ID=volume.RM_FORECAST_VOLUME_ID AND		
		volume.FORECAST_AS_OF_DATE IN
		(
		select MAX(FORECAST_AS_OF_DATE) from  RM_FORECAST_VOLUME where FORECAST_YEAR =@startYear AND CLIENT_ID=@clientId
		UNION 	
		select MAX(FORECAST_AS_OF_DATE) from  RM_FORECAST_VOLUME where FORECAST_YEAR =@endYear AND CLIENT_ID=@clientId
		) 
		AND 
		volume.CLIENT_ID=@clientId 
		AND
		volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @startDate, 101) AND CONVERT(Varchar(12), @endDate, 101) AND
		RM_BUDGET.client_id=@clientId and
		RM_TARGET_BUDGET.rm_budget_id=RM_BUDGET.rm_budget_id and
		volumedetails.MONTH_IDENTIFIER=RM_TARGET_BUDGET.MONTH_IDENTIFIER and
		RM_BUDGET_SITE_MAP.SITE_ID=volumedetails.site_id and
		--RM_BUDGET.SITE_ID=volumedetails.site_id AND

		RM_TARGET_BUDGET_ID IN
	(

		SELECT 
			RM_TARGET_BUDGET_id
		FROM
			RM_TARGET_BUDGET,
			RM_BUDGET
		WHERE

		RM_BUDGET.RM_BUDGET_ID=RM_TARGET_BUDGET.RM_BUDGET_ID AND
		RM_BUDGET.CLIENT_ID=@clientId AND
		RM_BUDGET.DIVISION_ID IS NOT NULL AND
		
		RM_BUDGET.IS_BUDGET_APPROVED=1 AND 
		RM_BUDGET.RM_BUDGET_ID IN 

			( 
				select	RM_BUDGET_ID 
				from	RM_BUDGET
				where	CONVERT(Varchar(12),@startDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH										
				UNION										
				select RM_BUDGET_ID 					
				from	RM_BUDGET					
				where CONVERT(Varchar(12), @endDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH
				UNION 	
				select RM_BUDGET_ID 
				from	RM_BUDGET
				where BUDGET_START_MONTH BETWEEN  CONVERT(Varchar(12),@startDate, 101)  AND CONVERT(Varchar(12),@endDate, 101)
				UNION										
				select RM_BUDGET_ID 					
				from	RM_BUDGET					
				where BUDGET_END_MONTH BETWEEN  CONVERT(Varchar(12),@startDate, 101)  AND CONVERT(Varchar(12),@endDate, 101)
			) 
				AND RM_TARGET_BUDGET.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @startDate, 101) AND CONVERT(Varchar(12), @endDate, 101)
	)

	AND
	volumedetails.SITE_ID IN

	(
		SELECT SITE_ID FROM SITE 
		
		WHERE 
		
		DIVISION_ID IN
		(
			SELECT 
			DISTINCT RM_BUDGET.DIVISION_ID FROM RM_TARGET_BUDGET,RM_BUDGET
			WHERE 
			RM_BUDGET.RM_BUDGET_ID=RM_TARGET_BUDGET.RM_BUDGET_ID AND
			RM_BUDGET.CLIENT_ID=@clientId AND
			RM_BUDGET.SITE_ID IS NULL AND
			RM_BUDGET.DIVISION_ID IS NOT NULL 
			AND RM_BUDGET.IS_BUDGET_APPROVED=1 AND	RM_BUDGET.RM_BUDGET_ID IN 
			( 
				select	RM_BUDGET_ID 
				from	RM_BUDGET
				where	CONVERT(Varchar(12),@startDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH										
				UNION										
				select RM_BUDGET_ID 					
				from	RM_BUDGET					
				where CONVERT(Varchar(12), @endDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH
				UNION 	
				select RM_BUDGET_ID 
				from	RM_BUDGET
				where BUDGET_START_MONTH BETWEEN  CONVERT(Varchar(12),@startDate, 101)  AND CONVERT(Varchar(12),@endDate, 101)
				UNION										
				select RM_BUDGET_ID 					
				from	RM_BUDGET					
				where BUDGET_END_MONTH BETWEEN  CONVERT(Varchar(12),@startDate, 101)  AND CONVERT(Varchar(12),@endDate, 101)
			) 
			AND RM_TARGET_BUDGET.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @startDate, 101) AND CONVERT(Varchar(12), @endDate, 101)
			
	)
	AND volumedetails.SITE_ID NOT IN
	(
			SELECT 
			DISTINCT RM_BUDGET.SITE_ID FROM RM_TARGET_BUDGET,RM_BUDGET
			WHERE 
			RM_BUDGET.RM_BUDGET_ID=RM_TARGET_BUDGET.RM_BUDGET_ID AND
			RM_BUDGET.CLIENT_ID=@clientId AND
			RM_BUDGET.SITE_ID IS NOT NULL AND
			RM_BUDGET.DIVISION_ID IS NOT NULL AND 
			RM_BUDGET.IS_BUDGET_APPROVED=1 AND	
			RM_BUDGET.RM_BUDGET_ID IN 
			( 
				select	RM_BUDGET_ID 
				from	RM_BUDGET
				where	CONVERT(Varchar(12),@startDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH										
				UNION										
				select RM_BUDGET_ID 					
				from	RM_BUDGET					
				where CONVERT(Varchar(12), @endDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH
				UNION 	
				select RM_BUDGET_ID 
				from	RM_BUDGET
				where BUDGET_START_MONTH BETWEEN  CONVERT(Varchar(12),@startDate, 101)  AND CONVERT(Varchar(12),@endDate, 101)
				UNION										
				select RM_BUDGET_ID 					
				from	RM_BUDGET					
				where BUDGET_END_MONTH BETWEEN  CONVERT(Varchar(12),@startDate, 101)  AND CONVERT(Varchar(12),@endDate, 101)
			) 
				AND RM_TARGET_BUDGET.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @startDate, 101) AND CONVERT(Varchar(12), @endDate, 101)
			
	   )
	 )


	UNION



	--FOR SITE

	SELECT 
		volumedetails.VOLUME*RM_TARGET_BUDGET.BUDGET_TARGET priceVolume,
		volumedetails.VOLUME,
		volumedetails.RM_FORECAST_VOLUME_DETAILS_ID,
		volumedetails.SITE_ID,
		RM_TARGET_BUDGET.BUDGET_TARGET,
		RM_TARGET_BUDGET.RM_TARGET_BUDGET_ID,
		volumedetails.MONTH_IDENTIFIER,
		RM_TARGET_BUDGET.MONTH_IDENTIFIER,
		CONVERT (Varchar(12), RM_TARGET_BUDGET.MONTH_IDENTIFIER, 101)  MONTH_IDENTIFIER

	FROM 
		RM_FORECAST_VOLUME_DETAILS volumedetails,
		RM_FORECAST_VOLUME volume,
		RM_TARGET_BUDGET,
		RM_BUDGET_SITE_MAP,
		RM_BUDGET

	WHERE
		--volumedetails.MONTH_IDENTIFIER=RM_TARGET_BUDGET.MONTH_IDENTIFIER AND
		volumedetails.RM_FORECAST_VOLUME_ID=volume.RM_FORECAST_VOLUME_ID AND		
		volume.FORECAST_AS_OF_DATE IN
		(
		select MAX(FORECAST_AS_OF_DATE) from  RM_FORECAST_VOLUME where FORECAST_YEAR =@startYear AND CLIENT_ID=@clientId
		UNION 	
		select MAX(FORECAST_AS_OF_DATE) from  RM_FORECAST_VOLUME where FORECAST_YEAR =@endYear AND CLIENT_ID=@clientId
		) 
		AND 
		volume.CLIENT_ID=@clientId 
		AND
		volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @startDate, 101) AND CONVERT(Varchar(12), @endDate, 101) AND
		RM_BUDGET.client_id=@clientId and
		RM_TARGET_BUDGET.rm_budget_id=RM_BUDGET.rm_budget_id and
		volumedetails.MONTH_IDENTIFIER=RM_TARGET_BUDGET.MONTH_IDENTIFIER and
		RM_BUDGET_SITE_MAP.SITE_ID=volumedetails.site_id and
		RM_BUDGET.SITE_ID=volumedetails.site_id AND
		RM_TARGET_BUDGET_ID IN
	(

		SELECT 
			RM_TARGET_BUDGET_ID
		FROM
			RM_TARGET_BUDGET,
			RM_BUDGET
		WHERE

		RM_BUDGET.RM_BUDGET_ID=RM_TARGET_BUDGET.RM_BUDGET_ID AND
		RM_BUDGET.CLIENT_ID=@clientId AND
		RM_BUDGET.SITE_ID IS NOT NULL AND
		RM_BUDGET.IS_BUDGET_APPROVED=1 AND 
		RM_BUDGET.RM_BUDGET_ID IN 
			( 
				select	RM_BUDGET_ID 
				from	RM_BUDGET
				where	CONVERT(Varchar(12),@startDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH										
				UNION										
				select RM_BUDGET_ID 					
				from	RM_BUDGET					
				where CONVERT(Varchar(12), @endDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH
				UNION 	
				select RM_BUDGET_ID 
				from	RM_BUDGET
				where BUDGET_START_MONTH BETWEEN  CONVERT(Varchar(12),@startDate, 101)  AND CONVERT(Varchar(12),@endDate, 101)
				UNION										
				select RM_BUDGET_ID 					
				from	RM_BUDGET					
				where BUDGET_END_MONTH BETWEEN  CONVERT(Varchar(12),@startDate, 101)  AND CONVERT(Varchar(12),@endDate, 101)
			) 
				AND RM_TARGET_BUDGET.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @startDate, 101) AND CONVERT(Varchar(12), @endDate, 101)
	)

	AND
	volumedetails.SITE_ID IN

	(
		SELECT 
		DISTINCT RM_BUDGET.SITE_ID FROM RM_TARGET_BUDGET,RM_BUDGET
		WHERE 
			RM_BUDGET.RM_BUDGET_ID=RM_TARGET_BUDGET.RM_BUDGET_ID AND
			RM_BUDGET.CLIENT_ID=@clientId AND
			RM_BUDGET.SITE_ID IS NOT NULL 
			AND RM_BUDGET.IS_BUDGET_APPROVED=1 AND	RM_BUDGET.RM_BUDGET_ID IN 
			( 
				select	RM_BUDGET_ID 
				from	RM_BUDGET
				where	CONVERT(Varchar(12),@startDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH										
				UNION										
				select RM_BUDGET_ID 					
				from	RM_BUDGET					
				where CONVERT(Varchar(12), @endDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH
				UNION 	
				select RM_BUDGET_ID 
				from	RM_BUDGET
				where BUDGET_START_MONTH BETWEEN  CONVERT(Varchar(12),@startDate, 101)  AND CONVERT(Varchar(12),@endDate, 101)
				UNION										
				select RM_BUDGET_ID 					
				from	RM_BUDGET					
				where BUDGET_END_MONTH BETWEEN  CONVERT(Varchar(12),@startDate, 101)  AND CONVERT(Varchar(12),@endDate, 101)
			) 
				AND RM_TARGET_BUDGET.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @startDate, 101) AND CONVERT(Varchar(12), @endDate, 101)
	 )
	) AS A group by DATEPART(MONTH,A.DEALMONTHYEAR),DATEPART(YEAR,A.DEALMONTHYEAR),A.MONTH_IDENTIFIER

else if @clientId>0 and @divisionId>0 and @siteId=0

SELECT 
	sum(A.priceVolume),
	sum(A.VOLUME),
	CASE 	sum(A.VOLUME)
			WHEN 0 THEN 0.0
			ELSE sum(A.priceVolume)/sum(A.VOLUME)
			END AS BudgetWACOGPRICE,
	--A.SITEID,
	--A.TARGETPRICE,
	DATEPART(MONTH,A.DEALMONTHYEAR) dealTicketMonth,
	DATEPART(YEAR,A.DEALMONTHYEAR) dealTicketYear,
	A.MONTH_IDENTIFIER MONTH_IDENTIFIER

FROM
(

		SELECT 
			volumedetails.VOLUME*RM_TARGET_BUDGET.BUDGET_TARGET priceVolume,
			volumedetails.VOLUME VOLUME,
			volumedetails.RM_FORECAST_VOLUME_DETAILS_ID FORECAST_ID,
			volumedetails.SITE_ID SITEID,
			RM_TARGET_BUDGET.BUDGET_TARGET TARGETPRICE,
			RM_TARGET_BUDGET.RM_TARGET_BUDGET_ID TARGETID,
			volumedetails.MONTH_IDENTIFIER DEALMONTHYEAR,
			RM_TARGET_BUDGET.MONTH_IDENTIFIER MI2,
			CONVERT (Varchar(12), RM_TARGET_BUDGET.MONTH_IDENTIFIER, 101)  MONTH_IDENTIFIER
		FROM 
			RM_FORECAST_VOLUME_DETAILS volumedetails,
			RM_FORECAST_VOLUME volume,
			RM_TARGET_BUDGET,
			RM_BUDGET,
			RM_BUDGET_SITE_MAP

		WHERE
			--volumedetails.MONTH_IDENTIFIER=RM_TARGET_BUDGET.MONTH_IDENTIFIER AND
			volumedetails.RM_FORECAST_VOLUME_ID=volume.RM_FORECAST_VOLUME_ID AND		
			volume.FORECAST_AS_OF_DATE IN
			(
			select MAX(FORECAST_AS_OF_DATE) from  RM_FORECAST_VOLUME where FORECAST_YEAR =@startYear AND CLIENT_ID=@clientId
			UNION 	
			select MAX(FORECAST_AS_OF_DATE) from  RM_FORECAST_VOLUME where FORECAST_YEAR =@endYear AND CLIENT_ID=@clientId
			) 
			AND 
			volume.CLIENT_ID=@clientId 
			AND
			volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @startDate, 101) AND CONVERT(Varchar(12), @endDate, 101) AND
			RM_BUDGET.client_id=@clientId and
			RM_TARGET_BUDGET.rm_budget_id=RM_BUDGET.rm_budget_id and
			volumedetails.MONTH_IDENTIFIER=RM_TARGET_BUDGET.MONTH_IDENTIFIER and
			RM_BUDGET_SITE_MAP.SITE_ID=volumedetails.site_id and
			--RM_BUDGET.SITE_ID=volumedetails.site_id AND

			RM_TARGET_BUDGET_ID IN
		(
		
			SELECT 
				RM_TARGET_BUDGET_ID
			FROM
				RM_TARGET_BUDGET,
				RM_BUDGET
			WHERE
		
			RM_BUDGET.RM_BUDGET_ID=RM_TARGET_BUDGET.RM_BUDGET_ID AND
			RM_BUDGET.CLIENT_ID=@clientId AND
			RM_BUDGET.DIVISION_ID IS NOT NULL AND
			RM_BUDGET.SITE_ID IS NULL AND
			RM_BUDGET.IS_BUDGET_APPROVED=1 AND 
			RM_BUDGET.RM_BUDGET_ID IN 
			 	( 
					select	RM_BUDGET_ID 
					from	RM_BUDGET
					where	CONVERT(Varchar(12),@startDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH										
					UNION										
					select RM_BUDGET_ID 					
					from	RM_BUDGET					
					where CONVERT(Varchar(12), @endDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH
					UNION 	
					select RM_BUDGET_ID 
				 	from	RM_BUDGET
					where BUDGET_START_MONTH BETWEEN  CONVERT(Varchar(12),@startDate, 101)  AND CONVERT(Varchar(12),@endDate, 101)
					UNION										
					select RM_BUDGET_ID 					
					from	RM_BUDGET					
					where BUDGET_END_MONTH BETWEEN  CONVERT(Varchar(12),@startDate, 101)  AND CONVERT(Varchar(12),@endDate, 101)
		  		) 
					AND RM_TARGET_BUDGET.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @startDate, 101) AND CONVERT(Varchar(12), @endDate, 101)
		)
		
		AND
		volumedetails.SITE_ID IN
		
		(
			SELECT SITE_ID FROM SITE 
			
			WHERE 
			
			DIVISION_ID IN
			(
				SELECT 
				DISTINCT RM_BUDGET.DIVISION_ID FROM RM_TARGET_BUDGET,RM_BUDGET,RM_BUDGET_SITE_MAP
				WHERE 
				RM_BUDGET.RM_BUDGET_ID=RM_TARGET_BUDGET.RM_BUDGET_ID AND
				RM_BUDGET.CLIENT_ID=@clientId AND
				RM_BUDGET.SITE_ID IS NULL AND
				RM_BUDGET.DIVISION_ID=@divisionId 
				AND RM_BUDGET.IS_BUDGET_APPROVED=1 AND	RM_BUDGET.RM_BUDGET_ID IN 
			 	( 
					select	RM_BUDGET_ID 
					from	RM_BUDGET
					where	CONVERT(Varchar(12),@startDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH										
					UNION										
					select RM_BUDGET_ID 					
					from	RM_BUDGET					
					where CONVERT(Varchar(12),@endDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH
					UNION 	
					select RM_BUDGET_ID 
				 	from	RM_BUDGET
					where BUDGET_START_MONTH BETWEEN  CONVERT(Varchar(12),@startDate, 101)  AND CONVERT(Varchar(12),@endDate, 101)
					UNION										
					select RM_BUDGET_ID 					
					from	RM_BUDGET					
					where BUDGET_END_MONTH BETWEEN  CONVERT(Varchar(12),@startDate, 101)  AND CONVERT(Varchar(12),@endDate, 101)
			  	) 
				AND RM_TARGET_BUDGET.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @startDate, 101) AND CONVERT(Varchar(12), @endDate, 101)
				AND SITE.SITE_ID!=RM_BUDGET_SITE_MAP.SITE_ID
				
			)
		AND volumedetails.SITE_ID NOT IN
		(
				SELECT 
				DISTINCT RM_BUDGET.SITE_ID FROM RM_TARGET_BUDGET,RM_BUDGET
				WHERE 

				RM_BUDGET.RM_BUDGET_ID=RM_TARGET_BUDGET.RM_BUDGET_ID AND
				RM_BUDGET.CLIENT_ID=@clientId AND
				RM_BUDGET.SITE_ID IS NOT NULL 
				AND RM_BUDGET.IS_BUDGET_APPROVED=1 AND	RM_BUDGET.RM_BUDGET_ID IN 
			 	( 
					select	RM_BUDGET_ID 
					from	RM_BUDGET
					where	CONVERT(Varchar(12),@startDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH										
					UNION										
					select RM_BUDGET_ID 					
					from	RM_BUDGET					
					where CONVERT(Varchar(12),@endDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH
					UNION 	
					select RM_BUDGET_ID 
				 	from	RM_BUDGET
					where BUDGET_START_MONTH BETWEEN  CONVERT(Varchar(12),@startDate, 101)  AND CONVERT(Varchar(12),@endDate, 101)
					UNION										
					select RM_BUDGET_ID 					
					from	RM_BUDGET					
					where BUDGET_END_MONTH BETWEEN  CONVERT(Varchar(12),@startDate, 101)  AND CONVERT(Varchar(12),@endDate, 101)
		  		) 
					AND RM_TARGET_BUDGET.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @startDate, 101) AND CONVERT(Varchar(12), @endDate, 101)
				
		   )
	 )

		
UNION



--FOR SITE

SELECT 
	volumedetails.VOLUME*RM_TARGET_BUDGET.BUDGET_TARGET priceVolume,
	volumedetails.VOLUME VOLUME,
	volumedetails.RM_FORECAST_VOLUME_DETAILS_ID FORECAST_ID,
	volumedetails.SITE_ID SITEID,
	RM_TARGET_BUDGET.BUDGET_TARGET TARGETPRICE,
	RM_TARGET_BUDGET.RM_TARGET_BUDGET_ID TARGETID,
	volumedetails.MONTH_IDENTIFIER DEALMONTHYEAR,
	RM_TARGET_BUDGET.MONTH_IDENTIFIER MI2,
	CONVERT (Varchar(12), RM_TARGET_BUDGET.MONTH_IDENTIFIER, 101)  MONTH_IDENTIFIER

FROM 
	RM_FORECAST_VOLUME_DETAILS volumedetails,
	RM_FORECAST_VOLUME volume,
	RM_TARGET_BUDGET,
	RM_BUDGET_SITE_MAP,
	RM_BUDGET

WHERE
	--volumedetails.MONTH_IDENTIFIER=RM_TARGET_BUDGET.MONTH_IDENTIFIER AND
	volumedetails.RM_FORECAST_VOLUME_ID=volume.RM_FORECAST_VOLUME_ID AND		
	volume.FORECAST_AS_OF_DATE IN
	(
	select MAX(FORECAST_AS_OF_DATE) from  RM_FORECAST_VOLUME where FORECAST_YEAR =@startYear AND CLIENT_ID=@clientId
	UNION 	
	select MAX(FORECAST_AS_OF_DATE) from  RM_FORECAST_VOLUME where FORECAST_YEAR =@endYear AND CLIENT_ID=@clientId
	) 
	AND 
	volume.CLIENT_ID=@clientId 
	AND
	volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @startDate, 101) AND CONVERT(Varchar(12), @endDate, 101) AND
	RM_BUDGET.client_id=@clientId and
	RM_TARGET_BUDGET.rm_budget_id=RM_BUDGET.rm_budget_id and
	volumedetails.MONTH_IDENTIFIER=RM_TARGET_BUDGET.MONTH_IDENTIFIER and
	RM_BUDGET_SITE_MAP.SITE_ID=volumedetails.site_id and
	RM_BUDGET.SITE_ID=volumedetails.site_id AND

	RM_TARGET_BUDGET_ID IN
(

	SELECT 
		RM_TARGET_BUDGET_ID
	FROM
		RM_TARGET_BUDGET,
		RM_BUDGET
	WHERE

	RM_BUDGET.RM_BUDGET_ID=RM_TARGET_BUDGET.RM_BUDGET_ID AND
	RM_BUDGET.CLIENT_ID=@clientId AND
	RM_BUDGET.SITE_ID IS NOT NULL AND
	RM_BUDGET.IS_BUDGET_APPROVED=1 AND 
	RM_BUDGET.RM_BUDGET_ID IN 
	 	( 
			select	RM_BUDGET_ID 
			from	RM_BUDGET
			where	CONVERT(Varchar(12),@startDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH										
			UNION										
			select RM_BUDGET_ID 					
			from	RM_BUDGET					
			where CONVERT(Varchar(12),@endDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH
			UNION 	
			select RM_BUDGET_ID 
		 	from	RM_BUDGET
			where BUDGET_START_MONTH BETWEEN  CONVERT(Varchar(12),@startDate, 101)  AND CONVERT(Varchar(12),@endDate, 101)
			UNION										
			select RM_BUDGET_ID 					
			from	RM_BUDGET					
			where BUDGET_END_MONTH BETWEEN  CONVERT(Varchar(12),@startDate, 101)  AND CONVERT(Varchar(12),@endDate, 101)
  		) 
			AND RM_TARGET_BUDGET.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @startDate, 101) AND CONVERT(Varchar(12), @endDate, 101)
)

AND
volumedetails.SITE_ID IN

(
	SELECT 
	DISTINCT RM_BUDGET.SITE_ID FROM RM_TARGET_BUDGET,RM_BUDGET
	WHERE 
		RM_BUDGET.RM_BUDGET_ID=RM_TARGET_BUDGET.RM_BUDGET_ID AND
		RM_BUDGET.CLIENT_ID=@clientId AND
		RM_BUDGET.SITE_ID IS NOT NULL 
		AND RM_BUDGET.IS_BUDGET_APPROVED=1 AND	RM_BUDGET.RM_BUDGET_ID IN 
	 	( 
			select	RM_BUDGET_ID 
			from	RM_BUDGET
			where	CONVERT(Varchar(12),@startDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH										
			UNION										
			select RM_BUDGET_ID 					
			from	RM_BUDGET					
			where CONVERT(Varchar(12),@endDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH
			UNION 	
			select RM_BUDGET_ID 
		 	from	RM_BUDGET
			where BUDGET_START_MONTH BETWEEN  CONVERT(Varchar(12),@startDate, 101)  AND CONVERT(Varchar(12),@endDate, 101)
			UNION										
			select RM_BUDGET_ID 					
			from	RM_BUDGET					
			where BUDGET_END_MONTH BETWEEN  CONVERT(Varchar(12),@startDate, 101)  AND CONVERT(Varchar(12),@endDate, 101)
  		) 
			AND RM_TARGET_BUDGET.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @startDate, 101) AND CONVERT(Varchar(12), @endDate, 101)
 )

) AS A group by DATEPART(MONTH,A.DEALMONTHYEAR),DATEPART(YEAR,A.DEALMONTHYEAR),A.MONTH_IDENTIFIER

else if @clientId>0 and @siteId>0


SELECT 
	sum(volumedetails.VOLUME*targetBudget.BUDGET_TARGET) priceVolume,
	sum(volumedetails.VOLUME) totalVolume,
	CASE sum(volumedetails.VOLUME)
		WHEN 0 THEN 0.0
		ELSE sum(volumedetails.VOLUME*targetBudget.BUDGET_TARGET)/sum(volumedetails.VOLUME)
		END AS BudgetWACOGPRICE,
	--sum(volumedetails.VOLUME*targetBudget.BUDGET_TARGET)/sum(volumedetails.VOLUME) BudgetWACOGPRICE,
	DATEPART(MONTH,volumedetails.MONTH_IDENTIFIER) dealTicketMonth,
	DATEPART(YEAR,volumedetails.MONTH_IDENTIFIER) dealTicketYear,
	CONVERT (Varchar(12), targetBudget.MONTH_IDENTIFIER, 101)  MONTH_IDENTIFIER

FROM 
	RM_FORECAST_VOLUME_DETAILS volumedetails,
	RM_FORECAST_VOLUME volume,
	RM_TARGET_BUDGET targetBudget,
	RM_BUDGET,
	RM_BUDGET_SITE_MAP

WHERE
	--volumedetails.MONTH_IDENTIFIER=targetBudget.MONTH_IDENTIFIER AND
	volumedetails.RM_FORECAST_VOLUME_ID=volume.RM_FORECAST_VOLUME_ID AND		
	volume.FORECAST_AS_OF_DATE IN
	(
	select MAX(FORECAST_AS_OF_DATE) from  RM_FORECAST_VOLUME where FORECAST_YEAR =@startDate  AND CLIENT_ID=@clientId
	UNION 	
	select MAX(FORECAST_AS_OF_DATE) from  RM_FORECAST_VOLUME where FORECAST_YEAR =@endYear AND CLIENT_ID=@clientId
	) 
	AND 
	volume.CLIENT_ID=@clientId 
	AND
	volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12),@startDate, 101) AND CONVERT(Varchar(12),@endDate, 101) AND
	RM_BUDGET.client_id=@clientId and
	targetBudget.rm_budget_id=RM_BUDGET.rm_budget_id and
	volumedetails.MONTH_IDENTIFIER=targetBudget.MONTH_IDENTIFIER and
	RM_BUDGET_SITE_MAP.SITE_ID=volumedetails.site_id and
	RM_BUDGET.SITE_ID=volumedetails.site_id AND

	RM_TARGET_BUDGET_ID IN
(

	SELECT 
		RM_TARGET_BUDGET_ID
	FROM
		RM_TARGET_BUDGET,
		RM_BUDGET
	WHERE

	RM_BUDGET.RM_BUDGET_ID=RM_TARGET_BUDGET.RM_BUDGET_ID AND
	RM_BUDGET.CLIENT_ID=@clientId AND
	RM_BUDGET.SITE_ID IS NOT NULL AND
	RM_BUDGET.IS_BUDGET_APPROVED=1 AND 
	RM_BUDGET.RM_BUDGET_ID IN 
	 	( 
			select	RM_BUDGET_ID 
			from	RM_BUDGET
			where	CONVERT(Varchar(12),@startDate , 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH										
			UNION										
			select RM_BUDGET_ID 					
			from	RM_BUDGET					
			where CONVERT(Varchar(12),@endDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH
			UNION 	
			select RM_BUDGET_ID 
		 	from	RM_BUDGET
			where BUDGET_START_MONTH BETWEEN  CONVERT(Varchar(12),@startDate , 101)  AND CONVERT(Varchar(12),@endDate, 101)
			UNION										
			select RM_BUDGET_ID 					
			from	RM_BUDGET					
			where BUDGET_END_MONTH BETWEEN  CONVERT(Varchar(12),@startDate , 101)  AND CONVERT(Varchar(12),@endDate, 101)
  		) 
			AND targetBudget.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @startDate , 101) AND CONVERT(Varchar(12), @endDate, 101)
)

AND
volumedetails.SITE_ID IN

(
	SELECT 
	DISTINCT RM_BUDGET.SITE_ID FROM RM_TARGET_BUDGET,RM_BUDGET,RM_BUDGET_SITE_MAP
	WHERE 
		RM_BUDGET.RM_BUDGET_ID=RM_TARGET_BUDGET.RM_BUDGET_ID AND
		RM_BUDGET.CLIENT_ID=@clientId AND
		RM_BUDGET.SITE_ID =@siteId AND

		RM_BUDGET.SITE_ID!=RM_BUDGET_SITE_MAP.SITE_ID
		AND RM_BUDGET.IS_BUDGET_APPROVED=1 AND	RM_BUDGET.RM_BUDGET_ID IN 
	 	( 
			select	RM_BUDGET_ID 
			from	RM_BUDGET
			where	CONVERT(Varchar(12),@startDate , 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH										
			UNION										
			select RM_BUDGET_ID 					
			from	RM_BUDGET					
			where CONVERT(Varchar(12),@endDate, 101) BETWEEN  BUDGET_START_MONTH  AND BUDGET_END_MONTH
			UNION 	
			select RM_BUDGET_ID 
		 	from	RM_BUDGET
			where BUDGET_START_MONTH BETWEEN  CONVERT(Varchar(12),@startDate , 101)  AND CONVERT(Varchar(12),@endDate, 101)
			UNION										
			select RM_BUDGET_ID 					
			from	RM_BUDGET					
			where BUDGET_END_MONTH BETWEEN  CONVERT(Varchar(12),@startDate , 101)  AND CONVERT(Varchar(12),@endDate, 101)
  		) 
			AND RM_TARGET_BUDGET.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @startDate , 101) AND CONVERT(Varchar(12), @endDate, 101) 
 )

group by DATEPART(MONTH,volumedetails.MONTH_IDENTIFIER),DATEPART(YEAR,volumedetails.MONTH_IDENTIFIER),targetBudget.MONTH_IDENTIFIER
GO
GRANT EXECUTE ON  [dbo].[GET_SCENARIO_REPORT_BUDGET_TARGET_P] TO [CBMSApplication]
GO
