SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[GET_ALL_FORECAST_VOLUME_DETAILS_P]  
     
DESCRIPTION:

      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@userId			VARCHAR
@sessionId		VARCHAR
@clientId		INT

OUTPUT PARAMETERS:     
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

EXEC dbo.GET_ALL_FORECAST_VOLUME_DETAILS_P 1,1,235

EXEC dbo.GET_ALL_FORECAST_VOLUME_DETAILS_P 1,1,10069

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
HG			Harihara Suthan G

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
HG			10/18/2010	Comments header added with the default SET statements
						As the pagination changes breaking the existing functionality reverted the changes pre Pagination version.
						Site table join removed as we are not fetching or filtering anything from that.
*/

CREATE PROCEDURE dbo.GET_ALL_FORECAST_VOLUME_DETAILS_P
	@userId		VARCHAR(10)
	,@sessionId VARCHAR(20)
	,@clientId	INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT
		volume.forecast_year
		,details.site_id
		,details.volume
		,CONVERT(VARCHAR(12), details.month_identifier, 101)  month_identifier
		,volumeType.entity_name
		,ISNULL(details.rm_forecast_volume_id, 0)
		,ISNULL(details.volume_units_type_id, 0)
	FROM
		dbo.rm_forecast_volume volume
		INNER JOIN dbo.rm_forecast_volume_details details
			ON details.rm_forecast_volume_id = volume.rm_forecast_volume_id
		INNER JOIN dbo.Entity volumeType
			ON volumeType.entity_id = details.volume_type_id
	WHERE
		volume.client_id = @clientId

END
GO
GRANT EXECUTE ON  [dbo].[GET_ALL_FORECAST_VOLUME_DETAILS_P] TO [CBMSApplication]
GO
