SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_LP_ADD_ADDITIONAL_ROW_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name              DataType          Default     Description    
---------------------------------------------------------------------------------    
	@user_id             varchar(10),
	@session_id          varchar(20),
	@rfp_account_id      int,
	@row_name            varchar(100),
	@row_value           varchar(200)
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- Test an insert
--exec dbo.SR_RFP_LP_ADD_ADDITIONAL_ROW_P
--	@user_id = 1,
--	@session_id = -1,
--	@rfp_account_id = 20381 ,
--	@row_name = 'Test Data',
--	@row_value = ''


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_LP_ADD_ADDITIONAL_ROW_P
	@user_id varchar(10),
	@session_id varchar(20),
	@rfp_account_id int,
	@row_name varchar(100),
	@row_value varchar(200)
	
	AS
	
SET NOCOUNT ON

	INSERT INTO sr_rfp_lp_account_additional_row(
													sr_rfp_account_id, 
													row_name, 
													row_value, 
													row_type_id
												 )
	VALUES										 (
													@rfp_account_id, 
													@row_name, 
													@row_value, 
													1157
												 )
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_LP_ADD_ADDITIONAL_ROW_P] TO [CBMSApplication]
GO
