SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                                                                    
NAME:  Dbo.Delete_Primary_Meter_By_Meter_Id 
                                                                                    
DESCRIPTION: To Delete the primary meter or non primary meter based on input meter id.
                                                                                             
INPUT PARAMETERS:                                                                                    
           Name         DataType  Default Description                                                                                    
------------------------------------------------------------                                                                                    
@Meter_Id INT                   
                                                              
OUTPUT PARAMETERS:                                                                                    
 Name   DataType  Default Description                                                                                    
------------------------------------------------------------                                                                                    
                                                                                    
USAGE EXAMPLES:                                                                                    
------------------------------------------------------------                
Begin Tran  
--Case 1 if it is only the Primary meter Do not allow to delete meter

EXEC Dbo.Delete_Primary_Meter_By_Meter_Id                
@Meter_ID= 1091776

--Case 2 :- if both the meters are primary,User can delete either of the meter

EXEC Dbo.Delete_Primary_Meter_By_Meter_Id                
@Meter_ID=1459602 --1459601

--Case 3 :- if two primary and one non primary,To delete this meter User should change Primary meter flag to other meter

EXEC Dbo.Delete_Primary_Meter_By_Meter_Id                
@Meter_ID=1459606--1459604-- 1459603

--Case 4 :- if two primary and one non primary,To delete this meter, user should change the primary meter flag to other meter.

EXEC Dbo.Delete_Primary_Meter_By_Meter_Id                
@Meter_ID=1459612--1459611

Rollback Tran                
                                                
AUTHOR INITIALS:                                                                                    
 Initials Name                                                                                    
------------------------------------------------------------                                                                                    
SC   Sreenivasulu Cheerala                                                              
                                                                                    
MODIFICATIONS                                                                                    
 Initials Date  Modification                                                                                    
------------------------------------------------------------                                                                                    
 SC   2020-05-21   Initial Created   
                                                                                    
******/
CREATE PROCEDURE [dbo].[Delete_Primary_Meter_By_Meter_Id]
    (
        @Meter_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Account_Id INT = 0;
        DECLARE @Count_OF_Meters INT = 0;
        DECLARE @Commodity_Id INT = 0;
        SELECT
            @Account_Id = cha.Account_Id
            , @Commodity_Id = cha.Commodity_Id
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Meter_Id = @Meter_Id
            AND cha.Account_Type = 'Utility';

        SELECT
            @Count_OF_Meters = COUNT(DISTINCT cha.Meter_Id)
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Account_Id = @Account_Id
            AND cha.Commodity_Id = @Commodity_Id
            AND cha.Account_Type = 'Utility';
        DECLARE @Meters_By_Account AS TABLE
              (
                  Meter_Id INT NOT NULL
                  , Is_Primary BIT NOT NULL DEFAULT 0
                  , Commodity_Id INT NOT NULL
              );
        INSERT INTO @Meters_By_Account
             (
                 Meter_Id
                 , Is_Primary
                 , Commodity_Id
             )
        SELECT
            m.METER_ID
            , CASE WHEN acpm.Meter_Id = m.METER_ID
                        AND cha.Commodity_Id = acpm.Commodity_Id THEN 1
                  ELSE 0
              END
            , cha.Commodity_Id
        FROM
            dbo.METER m
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = m.ACCOUNT_ID
            LEFT JOIN Budget.Account_Commodity_Primary_Meter acpm
                ON acpm.Account_Id = m.ACCOUNT_ID
                   AND  acpm.Meter_Id = m.METER_ID
        WHERE
            m.ACCOUNT_ID = @Account_Id
        GROUP BY
            m.METER_ID
            , CASE WHEN acpm.Meter_Id = m.METER_ID
                        AND cha.Commodity_Id = acpm.Commodity_Id THEN 1
                  ELSE 0
              END
            , cha.Commodity_Id;

        SELECT
            CASE WHEN MBA.Is_Primary = 0 THEN 1
                WHEN MBA.Is_Primary = 1
                     AND @Count_OF_Meters = 1
                     AND EXISTS (   SELECT
                                        1
                                    FROM
                                        Budget.Account_Commodity_Primary_Meter acpm
                                    WHERE
                                        acpm.Account_Id = @Account_Id
                                        AND acpm.Meter_Id <> @Meter_Id) THEN 1
                WHEN MBA.Is_Primary = 1
                     AND @Count_OF_Meters = 1
                     AND NOT EXISTS (   SELECT
                                            1
                                        FROM
                                            Budget.Account_Commodity_Primary_Meter acpm
                                        WHERE
                                            acpm.Account_Id = @Account_Id
                                            AND acpm.Meter_Id <> @Meter_Id) THEN 0
                ELSE 0
            END Is_Meter_Delete
        FROM
            @Meters_By_Account MBA
        WHERE
            MBA.Meter_Id = @Meter_Id
            AND MBA.Commodity_Id = @Commodity_Id;
    END;
GO
GRANT EXECUTE ON  [dbo].[Delete_Primary_Meter_By_Meter_Id] TO [CBMSApplication]
GO
