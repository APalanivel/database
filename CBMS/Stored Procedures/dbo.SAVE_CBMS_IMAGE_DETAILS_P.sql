SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SAVE_CBMS_IMAGE_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Cbms_Image_Type_ID	int       	          	
	@Cbms_Doc_ID   	varchar(200)	          	
	@Date_Imaged   	datetime  	          	
	@Cbms_Image_Size	decimal(18,0)	          	
	@Content_Type  	varchar(200)	          	
	@CBMS_Image_Directory	varchar(255)	          	
	@CBMS_Image_FileName	varchar(255)	          	
	@CBMS_Image_Location_Id	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--sp_helptext SAVE_CBMS_IMAGE_DETAILS_P
CREATE PROCEDURE dbo.SAVE_CBMS_IMAGE_DETAILS_P
   (
	  @Cbms_Image_Type_ID INT
	, @Cbms_Doc_ID VARCHAR(200)
	, @Date_Imaged DATETIME
	, @Cbms_Image_Size DECIMAL(18,0)
	, @Content_Type VARCHAR(200)
	, @CBMS_Image_Directory VARCHAR(255)
	, @CBMS_Image_FileName VARCHAR(255)
	, @CBMS_Image_Location_Id INT)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Cbms_Image_Id INT
  
	
	INSERT INTO dbo.Cbms_Image(Cbms_Image_Type_ID
		, Cbms_Doc_ID
		, Date_Imaged
		, Cbms_Image_Size
		, Content_Type
		, CBMS_Image_Directory
		, CBMS_Image_FileName
		, CBMS_Image_Location_Id)
	VALUES(@Cbms_Image_Type_ID
		, @Cbms_Doc_ID
		, @Date_Imaged
		, @Cbms_Image_Size
		, @Content_Type
		, @CBMS_Image_Directory
		, @CBMS_Image_FileName
		, @CBMS_Image_Location_Id )
		
	
	SET @cbms_Image_ID = SCOPE_IDENTITY()
	
	UPDATE dbo.CBMS_Image_Location
		SET Active_File_Cnt = ISNULL(Active_File_Cnt, 0) + 1
	WHERE CBMS_Image_Location_Id = @CBMS_Image_Location_Id
		AND Active_Directory = @CBMS_Image_Directory

	SELECT @cbms_Image_ID AS Cbms_Image_ID
	
	RETURN

END
GO
GRANT EXECUTE ON  [dbo].[SAVE_CBMS_IMAGE_DETAILS_P] TO [CBMSApplication]
GO
