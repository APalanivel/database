
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  /******                        
 NAME: dbo.CU_Invoice_Account_Dtls_Sel            
                        
 DESCRIPTION:  
	This will fetch the accounts for the Cu_Invoice_Id
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
-------------------------------------------------------------------------------------------------------------      
 @Cu_Invoice_Id                INT              
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
-------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
-------------------------------------------------------------------------------------------------------------                            
 
         EXEC dbo.CU_Invoice_Account_Dtls_Sel 6579767
         EXEC dbo.CU_Invoice_Account_Dtls_Sel 8836478--multiple service months
         EXEC dbo.CU_Invoice_Account_Dtls_Sel 11091262--multiple service months
         EXEC dbo.CU_Invoice_Account_Dtls_Sel 14343185--multiple accounts
         EXEC dbo.CU_Invoice_Account_Dtls_Sel 14343185--multiple accounts
         EXEC dbo.CU_Invoice_Account_Dtls_Sel 110006--multiple sites
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
-------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
-------------------------------------------------------------------------------------------------------------      
 SP                    2014-05-6        Created 
 SP					   2017-05-18		small enhancement,SE2017-153 added sitegroup_id,Display_Account_Number.                     
                       
******/    

  CREATE PROCEDURE [dbo].[CU_Invoice_Account_Dtls_Sel] @Cu_Invoice_Id AS INT
  AS 
  BEGIN

      SET NOCOUNT ON
    
      SELECT
            cha.Account_Id
           ,cha.Account_Number
           ,ch.Site_name
           ,cism.SERVICE_MONTH
           ,cism.CU_INVOICE_ID
           ,ch.Client_Id
           ,ch.Client_Hier_Id
           ,ch.Sitegroup_Id
           ,cha.Display_Account_Number
      FROM
            dbo.CU_INVOICE_SERVICE_MONTH cism
            INNER JOIN core.Client_Hier_Account cha
                  ON cha.ACCOUNT_ID = cism.ACCOUNT_ID
            INNER JOIN core.Client_Hier ch
                  ON ch.Client_Hier_Id = cha.Client_Hier_Id
      WHERE
            cism.CU_INVOICE_ID = @Cu_Invoice_Id
      GROUP BY
            cha.Account_Id
           ,cha.Account_Number
           ,ch.Site_name
           ,cism.SERVICE_MONTH
           ,cism.CU_INVOICE_ID
           ,ch.Client_Id
           ,ch.Client_Hier_Id
           ,ch.Sitegroup_Id
           ,cha.Display_Account_Number
      


  END;

;
GO

GRANT EXECUTE ON  [dbo].[CU_Invoice_Account_Dtls_Sel] TO [CBMSApplication]
GO
