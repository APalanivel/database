SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsMarket_Price_Point_Detail_Save

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Market_Price_Point_ID	int       	          	
	@Market_Price_Point_Value	decimal(32,16)	          	
	@Market_Price_Point_Detail_Date	smalldatetime	          	
	@Market_Price_Point_Future_Date	smalldatetime	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

create         Procedure dbo.cbmsMarket_Price_Point_Detail_Save 
	( @Market_Price_Point_ID int
	, @Market_Price_Point_Value decimal (32,16)
	, @Market_Price_Point_Detail_Date smalldatetime
	, @Market_Price_Point_Future_Date smalldatetime
	)
As
BEGIN

	/*

	This sproc updates a handful of tables and supports replication to both the 
		SSO and SV databases.

	*/


	set nocount on

	declare @ThisId_1 int 
	

	set @ThisId_1 =(select Market_Price_Point_ID from Market_Price_Point_Detail where Market_Price_Point_ID=@Market_Price_Point_ID and Market_Price_Point_Detail_Date = @Market_Price_Point_Detail_Date and Market_Price_Point_Future_Date = @Market_Price_Point_Future_Date )

	if @ThisId_1 is null
	begin

		/*---------------------------------------------------------------------------------------------------------------------|
		|									|
		|	This is an new entry for the Price point of the on the particulr time, CREATE THE RECORD			|
		|									|
		|----------------------------------------------------------------------------------------------------------------------*/


		-- INSERT THE USER ACCOUNT ROW
		insert into Market_Price_Point_Detail
			( Market_Price_Point_ID
			, Market_Price_Point_Value
			, Market_Price_Point_Detail_Date
			, Market_Price_Point_Future_Date
						)
		values
			( @Market_Price_Point_ID 
			, @Market_Price_Point_Value 
			, @Market_Price_Point_Detail_Date 
			, @Market_Price_Point_Future_Date 
			)

		


	end -- end insert
	else	/*---------------------------------------------------------------------------------------------------------------------|
		|									|
		|	This is an existing Price on the particular time, UPDATE THE RECORD			|
		|									|
		|---------------------------------------------------------------------------------------------------------------------*/	
	begin

		

		    update Market_Price_Point_Detail
		      set Market_Price_Point_ID = @Market_Price_Point_ID
			, Market_Price_Point_Value = @Market_Price_Point_Value
			, Market_Price_Point_Detail_Date = @Market_Price_Point_Detail_Date
			, Market_Price_Point_Future_Date = @Market_Price_Point_Future_Date
			
		    where Market_Price_Point_ID = @Market_Price_Point_ID and Market_Price_Point_Detail_Date= @Market_Price_Point_Detail_Date and Market_Price_Point_Future_Date = @Market_Price_Point_Future_Date

	

		
	
	end	-- INSERT/UPDATE

	



	
END
GO
GRANT EXECUTE ON  [dbo].[cbmsMarket_Price_Point_Detail_Save] TO [CBMSApplication]
GO
