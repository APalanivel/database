SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: dbo.Savings_Category_Division_Map_Del  

DESCRIPTION:

	Used to delete the entries from Savings_Category_Division_Map
      
INPUT PARAMETERS:          
	NAME						DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@SAVINGS_CATEGORY_TYPE_ID	INT
	@Division_Id				INT
OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------


  Begin Tran
	EXEC Savings_Category_Division_Map_Del  156, 33
  Rollback Tran

	SELECT TOP 100 * FROM Savings_Category_Division_Map

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			20-JULY-10	CREATED

*/

CREATE PROCEDURE dbo.Savings_Category_Division_Map_Del
    (
      @Savings_Category_Type_Id	INT
      ,@Division_Id				INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE
		dbo.SAVINGS_CATEGORY_DIVISION_MAP
	WHERE
		SAVINGS_CATEGORY_TYPE_ID = @Savings_Category_Type_Id
		AND DIVISION_ID = @Division_Id

END
GO
GRANT EXECUTE ON  [dbo].[Savings_Category_Division_Map_Del] TO [CBMSApplication]
GO
