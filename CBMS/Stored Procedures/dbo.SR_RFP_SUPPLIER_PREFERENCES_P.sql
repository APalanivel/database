
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 dbo.Locale_Notification_Template_Sel_By_Notification_Type    
 
DESCRIPTION:  

INPUT PARAMETERS:    
Name							DataType		Default			Description    
------------------------------------------------------------------------------    
@user_id						VARCHAR(10)
@session_id						VARCHAR(20)
@sr_supplier_contact_info_id	INT			
					

OUTPUT PARAMETERS:    
Name							DataType		Default			Description    
-----------------------------------------------------------------------------    
 
USAGE EXAMPLES:    
------------------------------------------------------------------------------    
    
EXEC dbo.SR_RFP_SUPPLIER_PREFERENCES_P 0,0,1698
EXEC dbo.SR_RFP_SUPPLIER_PREFERENCES_P 0,0,1690
EXEC dbo.SR_RFP_SUPPLIER_PREFERENCES_P 0,0,1679


AUTHOR INITIALS:    
Initials	Name    
------------------------------------------------------------------------------    
NR			Narayana Reddy			

MODIFICATIONS:
Initials	Date		Modification    
------------------------------------------------------------------------------    
NR			2016-07-15	GCS - 5b - GCS-1186 Added Locale_Cd Values.	
						GCS-1255 -Last Name preferred	
NR			2016-08-04	MAINT-4203 - Added FIRST_NAME,LAST_NAME.			
						
    
******/

CREATE  PROCEDURE [dbo].[SR_RFP_SUPPLIER_PREFERENCES_P]
      ( 
       @user_id VARCHAR(10)
      ,@session_id VARCHAR(20)
      ,@sr_supplier_contact_info_id INT )
AS 
SET NOCOUNT ON 

BEGIN

      SELECT
            contact_info.is_rfp_preference_website
           ,contact_info.is_rfp_preference_email
           ,info.FIRST_NAME + ' ' + info.LAST_NAME AS first_name
           ,info.email_address
           ,cd.Code_Id AS Locale_Cd
           ,cd.Code_Value AS Locale_Value
      FROM
            sr_supplier_contact_info contact_info
            INNER JOIN user_info info
                  ON contact_info.USER_INFO_ID = info.USER_INFO_ID
            INNER JOIN dbo.Code cd
                  ON cd.Code_Value = info.locale_code
            INNER JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            contact_info.sr_supplier_contact_info_id = @sr_supplier_contact_info_id
            AND cs.Codeset_Name = 'LocalizationLanguage'  
     
     
END 





;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_SUPPLIER_PREFERENCES_P] TO [CBMSApplication]
GO
