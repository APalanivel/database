SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
NAME:          
       
DESCRIPTION: Client_Invoice_Process_Tracking      
 This procedure is used to check if the invoice tracking is allowed based on the State mapped in UBM_INVOICE table.      
      
          
 INPUT PARAMETERS:          
 Name    DataType  Default   Description          
------------------------------------------------------------------------------------------------------------------------                  
       
 OUTPUT PARAMETERS:          
 Name    DataType  Default   Description          
------------------------------------------------------------------------------------------------------------------------                  
       
 USAGE EXAMPLES:          
------------------------------------------------------------------------------------------------------------------------                  
 EXEC [Client_Invoice_Process_Tracking] 89865720 

 EXEC [Client_Invoice_Process_Tracking] 75254748         
 EXEC [Client_Invoice_Process_Tracking_New] 75254748  
          
AUTHOR INITIALS:          
 Initials	Name          
------------------------------------------------------------------------------------------------------------------------                  
 TRK		Ramakrishna Thummala      
       
 MODIFICATIONS           
       
 Initials	Date		Modification          
------------------------------------------------------------------------------------------------------------------------                  
 TRK		2020-01-13  Created  
******/
CREATE PROCEDURE [dbo].[Client_Invoice_Process_Tracking]
    (@CU_INVOICE_ID INT)
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @Invoice_Ubm_State TABLE
        (
            [CU_INVOICE_ID]   INT
           ,[UBM_CLIENT_CODE] VARCHAR (1000)
           ,[CLIENT_ID]       INT
           ,[BEGIN_DATE]      DATETIME
           ,[END_DATE]        DATETIME
           ,[STATE_NAME]      VARCHAR (100)
           ,[COMMODITY_ID]    INT
        );

    DECLARE @Invoice_Region_Country_State TABLE
        (
            [CU_INVOICE_ID]   INT
           ,[UBM_CLIENT_CODE] VARCHAR (1000)
           ,[CLIENT_ID]       INT
           ,[BEGIN_DATE]      DATETIME
           ,[END_DATE]        DATETIME
           ,[STATE_NAME]      VARCHAR (100)
           ,[COMMODITY_ID]    INT
           ,[REGION_ID]       INT
           ,[COUNTRY_ID]      INT
           ,[STATE_ID]        INT
        );

    DECLARE @Is_Invoice_DNT BIT = 1;

    INSERT INTO @Invoice_Ubm_State
    (
        CU_INVOICE_ID
       ,UBM_CLIENT_CODE
       ,CLIENT_ID
       ,BEGIN_DATE
       ,END_DATE
       ,STATE_NAME
       ,COMMODITY_ID
    )
    SELECT
        CI.CU_INVOICE_ID
       ,UCM.UBM_CLIENT_CODE
       ,UCM.CLIENT_ID
       ,CI.BEGIN_DATE
       ,CI.END_DATE
       ,UI.STATE
       ,CID.COMMODITY_TYPE_ID
    FROM
        dbo.CU_INVOICE AS CI
        INNER JOIN dbo.CU_INVOICE_DETERMINANT AS CID
            ON CID.CU_INVOICE_ID = CI.CU_INVOICE_ID
        INNER JOIN dbo.UBM_CLIENT_MAP AS UCM
            ON UCM.UBM_CLIENT_CODE = CI.UBM_CLIENT_CODE
               AND CI.UBM_ID = UCM.UBM_ID
        INNER JOIN dbo.UBM_INVOICE AS UI
            ON UI.UBM_INVOICE_ID = CI.UBM_INVOICE_ID
    WHERE
        CI.CU_INVOICE_ID = @CU_INVOICE_ID
        AND CID.COMMODITY_TYPE_ID <> -1
    GROUP BY CI.CU_INVOICE_ID
            ,UCM.UBM_CLIENT_CODE
            ,UCM.CLIENT_ID
            ,CI.BEGIN_DATE
            ,CI.END_DATE
            ,UI.STATE
            ,CID.COMMODITY_TYPE_ID;

    INSERT INTO @Invoice_Ubm_State
    (
        CU_INVOICE_ID
       ,UBM_CLIENT_CODE
       ,CLIENT_ID
       ,BEGIN_DATE
       ,END_DATE
       ,STATE_NAME
       ,COMMODITY_ID
    )
    SELECT
        CI.CU_INVOICE_ID
       ,UCM.UBM_CLIENT_CODE
       ,UCM.CLIENT_ID
       ,CI.BEGIN_DATE
       ,CI.END_DATE
       ,UI.STATE
       ,CID.COMMODITY_TYPE_ID
    FROM
        dbo.CU_INVOICE AS CI
        INNER JOIN dbo.CU_INVOICE_CHARGE AS CID
            ON CID.CU_INVOICE_ID = CI.CU_INVOICE_ID
        INNER JOIN dbo.UBM_CLIENT_MAP AS UCM
            ON UCM.UBM_CLIENT_CODE = CI.UBM_CLIENT_CODE
               AND CI.UBM_ID = UCM.UBM_ID
        INNER JOIN dbo.UBM_INVOICE AS UI
            ON UI.UBM_INVOICE_ID = CI.UBM_INVOICE_ID
    WHERE
        CI.CU_INVOICE_ID = @CU_INVOICE_ID
        AND CID.COMMODITY_TYPE_ID <> -1
        AND NOT EXISTS (
                           SELECT 1
                           FROM
                               @Invoice_Ubm_State ciu
                           WHERE
                               ciu.COMMODITY_ID = CID.COMMODITY_TYPE_ID
                       )
    GROUP BY CI.CU_INVOICE_ID
            ,UCM.UBM_CLIENT_CODE
            ,UCM.CLIENT_ID
            ,CI.BEGIN_DATE
            ,CI.END_DATE
            ,UI.STATE
            ,CID.COMMODITY_TYPE_ID;

    INSERT INTO @Invoice_Region_Country_State
    (
        CU_INVOICE_ID
       ,UBM_CLIENT_CODE
       ,CLIENT_ID
       ,BEGIN_DATE
       ,END_DATE
       ,STATE_NAME
       ,COMMODITY_ID
       ,REGION_ID
       ,COUNTRY_ID
       ,STATE_ID
    )
    SELECT
        CUI.CU_INVOICE_ID
       ,CUI.UBM_CLIENT_CODE
       ,CUI.CLIENT_ID
       ,CUI.BEGIN_DATE
       ,CUI.END_DATE
       ,CUI.STATE_NAME
       ,CUI.COMMODITY_ID
       ,S.REGION_ID
       ,S.COUNTRY_ID
       ,S.STATE_ID
    FROM
        @Invoice_Ubm_State AS CUI
        INNER JOIN dbo.STATE AS S
            ON S.STATE_NAME = CUI.STATE_NAME;

    SELECT
        CI.CU_INVOICE_ID
       ,CI.UBM_CLIENT_CODE
       ,CI.CLIENT_ID
       ,CI.BEGIN_DATE
       ,CI.END_DATE
       ,CI.STATE_NAME
       ,CI.COMMODITY_ID
       ,CI.REGION_ID
       ,CI.COUNTRY_ID
       ,CI.STATE_ID
       ,CIDC.Client_Invoice_DNT_Config_Id
    INTO #Client_DNT_Config
    FROM
        @Invoice_Region_Country_State AS CI
        INNER JOIN dbo.Client_Invoice_DNT_Config AS CIDC
            ON CI.CLIENT_ID = CIDC.CLIENT_ID
               AND CI.COMMODITY_ID = CIDC.Commodity_Id
    WHERE
        CI.BEGIN_DATE >= CIDC.Effective_Start_Dt
        AND CI.END_DATE <= CIDC.Effective_End_Dt;


    SELECT *
    INTO #DNT_State
    FROM (

             -- Configuration at Region level
             SELECT
                 st.STATE_ID
                ,st.STATE_NAME
                ,dc.COMMODITY_ID
             FROM
                 dbo.STATE st
                 CROSS JOIN #Client_DNT_Config dc
             WHERE
                 EXISTS (
                            SELECT 1
                            FROM
                                dbo.Client_Invoice_DNT_Config_Region_Map rm
                            WHERE
                                st.REGION_ID = rm.REGION_ID
                                AND rm.Client_Invoice_DNT_Config_Id = dc.Client_Invoice_DNT_Config_Id
                        )
                 AND NOT EXISTS (
                                    SELECT 1
                                    FROM
                                        dbo.Client_Invoice_DNT_Config_Country_Map cm
                                    WHERE
                                        cm.Client_Invoice_DNT_Config_Id = dc.Client_Invoice_DNT_Config_Id
                                )
                 AND NOT EXISTS (
                                    SELECT 1
                                    FROM
                                        dbo.Client_Invoice_DNT_Config_State_Map sm
                                    WHERE
                                        sm.Client_Invoice_DNT_Config_Id = dc.Client_Invoice_DNT_Config_Id
                                )
             UNION
             -- Configuration at Country level
             SELECT
                 st.STATE_ID
                ,st.STATE_NAME
                ,dc.COMMODITY_ID
             FROM
                 dbo.STATE st
                 CROSS JOIN #Client_DNT_Config dc
             WHERE
                 EXISTS (
                            SELECT 1
                            FROM
                                dbo.Client_Invoice_DNT_Config_Country_Map cm
                            WHERE
                                cm.COUNTRY_ID = st.COUNTRY_ID
                                AND cm.Client_Invoice_DNT_Config_Id = dc.Client_Invoice_DNT_Config_Id
                        )
                 AND NOT EXISTS (
                                    SELECT 1
                                    FROM
                                        dbo.Client_Invoice_DNT_Config_State_Map sm
                                    WHERE
                                        sm.Client_Invoice_DNT_Config_Id = dc.Client_Invoice_DNT_Config_Id
                                )
             UNION
             -- Configuration at State level
             SELECT
                 st.STATE_ID
                ,st.STATE_NAME
                ,dc.COMMODITY_ID
             FROM
                 dbo.STATE st
                 CROSS JOIN #Client_DNT_Config dc
             WHERE
                 EXISTS (
                            SELECT 1
                            FROM
                                dbo.Client_Invoice_DNT_Config_State_Map sm
                            WHERE
                                sm.STATE_ID = st.STATE_ID
                                AND sm.Client_Invoice_DNT_Config_Id = dc.Client_Invoice_DNT_Config_Id
                        )
             UNION
             SELECT
                 st.STATE_ID
                ,st.STATE_NAME
                ,dc.COMMODITY_ID
             FROM
                 dbo.STATE st
                 CROSS JOIN #Client_DNT_Config dc
             WHERE
                 NOT EXISTS (
                                SELECT 1
                                FROM
                                    dbo.Client_Invoice_DNT_Config_State_Map sm
                                WHERE
                                    sm.Client_Invoice_DNT_Config_Id = dc.Client_Invoice_DNT_Config_Id
                            )
                 AND NOT EXISTS (
                                    SELECT 1
                                    FROM
                                        dbo.Client_Invoice_DNT_Config_Country_Map cm
                                    WHERE
                                        cm.Client_Invoice_DNT_Config_Id = dc.Client_Invoice_DNT_Config_Id
                                )
                 AND NOT EXISTS (
                                    SELECT 1
                                    FROM
                                        dbo.Client_Invoice_DNT_Config_Region_Map rm
                                    WHERE
                                        rm.Client_Invoice_DNT_Config_Id = dc.Client_Invoice_DNT_Config_Id
                                )
         ) x;

    -- When there are no configuration exists
    SELECT @Is_Invoice_DNT = 0
    WHERE
        NOT EXISTS (
                       SELECT 1
                       FROM
                           #Client_DNT_Config
                   );

    -- Atleast one commodity of the invoice is tracked ( Invoice commodity doesn't existing in the 

    SELECT @Is_Invoice_DNT = 0
    FROM
        @Invoice_Ubm_State cu
    WHERE
        NOT EXISTS (
                       SELECT 1
                       FROM
                           #Client_DNT_Config ci
                       WHERE
                           ci.COMMODITY_ID = cu.COMMODITY_ID
                   )
        AND @Is_Invoice_DNT = 1;

    SELECT @Is_Invoice_DNT = 0
    FROM
        #Client_DNT_Config ci
    WHERE
        NOT EXISTS (
                       SELECT 1
                       FROM
                           #DNT_State dnt
                       WHERE
                           dnt.STATE_ID = ci.STATE_ID
                           AND dnt.COMMODITY_ID = ci.COMMODITY_ID
                   )
        AND @Is_Invoice_DNT = 1;

    SELECT @Is_Invoice_DNT Invoice_Rule;

    DROP TABLE #Client_DNT_Config;
    DROP TABLE #DNT_State;
END;
GO
GRANT EXECUTE ON  [dbo].[Client_Invoice_Process_Tracking] TO [CBMSApplication]
GO
