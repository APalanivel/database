SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsCPIndexSourcing_GetForwardForDate

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@clearport_index_id	int       	null      	
	@index_detail_date	datetime  	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE  PROCEDURE [dbo].[cbmsCPIndexSourcing_GetForwardForDate]
	( @clearport_index_id int = null
	, @index_detail_date datetime = null
	)
AS
BEGIN

	select cd.index_detail_date
		, cd.index_detail_value
		, cm.clearport_index_month
		, ci.clearport_index
	     from clearport_index_detail cd  with (nolock)
    	      join clearport_index_months cm with (nolock) on cm.clearport_index_month_id = cd.clearport_index_month_id
  	      join clearport_index ci with (nolock) on ci.clearport_index_id = cm.clearport_index_id 
   	     where cm.clearport_index_id = @clearport_index_id
  	       and cd.index_detail_date = @index_detail_date


END
GO
GRANT EXECUTE ON  [dbo].[cbmsCPIndexSourcing_GetForwardForDate] TO [CBMSApplication]
GO
