SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*********
NAME:  
    dbo.Variance_Closed_Reason_INS  
 
DESCRIPTION:  
    Used to insert selected closed variance reasons from review variance screen  

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
	@Reason_Id	    INT	
 	@Account_Id	    INT
	@Commodity_id	    INT
	@Service_Month	    DATE

OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:   
------------------------------------------------------------  
	SELECT
		cd.*
	FROM
		Code cd 
		JOIN Codeset cs
			ON cs.codeset_id = cd.Codeset_id
	WHERE
		cs.Codeset_Name = 'VarianceClosedReason'

	BEGIN TRAN
		EXEC dbo.Variance_Closed_Reason_INS 100229,498,NULL,'2011-09-01'
		
		SELECT * FROM Variance_Closed_Reason WHERE Reason_Cd = 100229 AND Account_ID = 498 AND Service_Month = '2011-09-01'
	ROLLBACK TRAN	

AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
NK		Nageswara Rao Kosuri         
HG		Harihara Suthan G
AP	    Athmaram Pabbathi
AP1		Arunkumar Palanivel

Initials Date  Modification  
------------------------------------------------------------  
NK		10/31/2009  Created
HG		03/17/2010	Account_Id, Commodity_id and service_month input parameters added for other service requirement.
AP		08/18/2011  Removed @Cost_Usage_Id from parameter & insert script as a part of ADLDT changes.
AP1		Feb 20,2020	Modified procedure to accomodate AVT project changes
******/

CREATE PROCEDURE [dbo].[Variance_Closed_Reason_INS]
      ( 
       @Reason_Id INT
      ,@Account_Id INT
      ,@Service_Month DATE
	  ,@User_id INT,
	  @Category_id AS [TVP_Category_Ids] READONLY )
AS 
BEGIN

      SET NOCOUNT ON ;

	  DECLARE @inserted_value INT

	  IF EXISTS (SELECT 1 FROM @Category_id)

	  begin

      INSERT      INTO dbo.Variance_Closed_Reason
                  ( 
                   Reason_Cd
                  ,Closed_Dt
                  ,Account_Id
                  ,Service_Month,
				  Created_User_Id,
				  Created_Ts )
                  SELECT
                        @Reason_Id
                       ,getdate()
                       ,@Account_Id
                       ,@Service_Month,
					   @User_id,
					   getdate()


					   SET @inserted_value = scope_identity() 
					   INSERT INTO dbo.Variance_Closed_Reason_Category_Map (
					                                                             Variance_Closed_Reason_id
					                                                           , Variance_Category_Cd
					                                                       )
					   SELECT DISTINCT @inserted_value,c.Code_Id FROM @Category_id vlog
					   JOIN  dbo.code c 
			ON VLog.category_Name = c.Code_Value
			JOIN dbo.Codeset cs
			ON cs.Codeset_Id = c.Codeset_Id
			WHERE cs.Codeset_name ='VarianceCategory'

			end

END
;
GO

GRANT EXECUTE ON  [dbo].[Variance_Closed_Reason_INS] TO [CBMSApplication]
GO
