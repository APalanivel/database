
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********  
NAME: dbo.User_Security_Role_INS_For_Client_Map   

DESCRIPTION:     
Adding Client's corporate security role to the user using Mapping Page.
	
INPUT PARAMETERS:    
Name		           DataType        Default         Description    
----------------------------------------------------------------    
@Client_Id               INT              


OUTPUT PARAMETERS:    
Name   DataType  Default Description    
------------------------------------------------------------    

USAGE EXAMPLES:    
------------------------------------------------------------    
BEGIN TRAN
-- Set the portfolio for some client
DECLARE @Output INT
EXEC dbo.Portfolio_Client_UPD 109, 108, @Output OUTPUT
EXEC dbo.User_Security_Role_INS_For_Client_Map 109

-- Set one more client to the same portfolio
EXEC dbo.Portfolio_Client_UPD 102, 108, @Output OUTPUT
EXEC dbo.User_Security_Role_INS_For_Client_Map 102

SELECT * FROM dbo.Security_Role WHERE Client_Id IN (102, 109) AND Is_Corporate = 1

BEGIN TRAN
-- This external user, 8062, has access to 102
SELECT
      USR.*
FROM
      dbo.User_Security_Role USR
      JOIN dbo.User_Info UI
            ON USR.User_Info_Id = UI.User_Info_Id
      JOIN dbo.Security_Role SR
            ON USR.Security_Role_Id = SR.Security_Role_Id
WHERE
      SR.Client_Id = 102
      AND Is_Corporate = 1
      AND UI.Access_Level = 1

-- 102 now has a portfolio id of 108.
DECLARE @Output INT
EXEC dbo.Portfolio_Client_UPD 102, 108, @Output OUTPUT

-- As of now 108 has only 102 under it
EXEC Client_SEL_By_Portfolio_Client_Id 108

-- Now 109 is also has 108 as portfolio
DECLARE @Output1 INT
EXEC dbo.Portfolio_Client_UPD 109, 108, @Output1 OUTPUT

EXEC Client_SEL_By_Portfolio_Client_Id 108

-- Now the above user will have access to 109 also as he has access to 102 which was the only client under 108
EXEC dbo.User_Security_Role_INS_For_Client_Map 109

SELECT * FROM dbo.User_Security_Role WHERE User_Info_Id = 8062

-- Now suppose we add two clients to 108 to which user 8062 doesn't have access
DECLARE @Output2 INT
EXEC dbo.Portfolio_Client_UPD 110, 108, @Output2 OUTPUT
EXEC dbo.Portfolio_Client_UPD 111, 108, @Output2 OUTPUT

EXEC Client_SEL_By_Portfolio_Client_Id 108

-- Now the user will not have access to the newly added client or anyother client added here on as he doesn't 
-- have access to 110
EXEC dbo.User_Security_Role_INS_For_Client_Map 111

SELECT * FROM dbo.User_Security_Role WHERE User_Info_Id = 8062

ROLLBACK

AUTHOR INITIALS:    
Initials	Name    
------------------------------------------------------------    
RK          Rajesh Kasoju
 
MODIFICATIONS     
Initials	       Date		       Modification    
------------------------------------------------------------    
 RK                07/03/2012      Created    
 RK                14/03/2012      Kept the filter 'count(USR.Security_Role_Id) = @Portfolio_Client_Cnt' to ensure when the number of clients under that portfolio from client_hier table
                                   and the number of client's security roles in the user_security_role for the user matches, we insert the new client's security role
 CPE				2012-04-06		Added the condition to check that the user is a user of the portfolio of the affected client.( MAINT - 1257 )
******/

CREATE PROCEDURE [dbo].[User_Security_Role_INS_For_Client_Map] ( @Client_Id INT )
AS 
BEGIN

      SET NOCOUNT ON ;
      
      DECLARE @Portfolio_Clients TABLE
            ( 
             Client_Id INT PRIMARY KEY CLUSTERED )
      
      DECLARE
            @Portfolio_Client_Cnt INT
           ,@Security_Role_Id INT
           ,@Portfolio_Client_Id INT
	
	  -- Get the portfolio client id of the newly added Client
      SELECT
            @Portfolio_Client_Id = Portfolio_Client_Id
      FROM
            core.Client_Hier
      WHERE
            Client_Id = @Client_Id
            AND SiteGroup_Id = 0
			
	  -- Get the corporate level security role id of the newly added client.
      SELECT
            @Security_Role_Id = Security_Role_Id
      FROM
            dbo.Security_Role
      WHERE
            Client_Id = @Client_Id
            AND Is_Corporate = 1

	  -- Get the other clients that are under the same portfolio. Exclude the newly added client
      INSERT      INTO @Portfolio_Clients
                  ( 
                   Client_Id )
                  SELECT
                        CH.Client_Id
                  FROM
                        core.Client_Hier CH
                  WHERE
                        CH.Portfolio_Client_Id = @Portfolio_Client_Id
                        AND CH.Sitegroup_Id = 0
                        AND CH.Client_Id <> @Client_Id
                  GROUP BY
                        CH.Client_Id 

      SET @Portfolio_Client_Cnt = @@Rowcount

	  -- Insert into user_Security_role for the users who have access to all the clients under the portfolio
	  -- count of entries in user_Security_role should match with the count of clients under the portfolio
      INSERT      INTO dbo.User_Security_Role
                  ( 
                   Security_Role_Id
                  ,User_Info_Id
                  ,Last_Change_Ts )
                  SELECT
                        @Security_Role_Id
                       ,USR.User_Info_Id
                       ,getdate()
                  FROM
                        dbo.User_Security_Role USR
                        JOIN dbo.Security_Role SR
                              ON USR.Security_Role_Id = SR.Security_Role_Id
                        JOIN @Portfolio_Clients PC
                              ON SR.Client_Id = PC.Client_Id
                        JOIN dbo.User_Info UI
                              ON UI.User_Info_Id = USR.User_Info_Id
                  WHERE
                        SR.Is_Corporate = 1
                        AND UI.Client_Id = @Portfolio_Client_Id				-- Only the users of the portfolio client
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.User_Security_Role UISR
                                         WHERE
                                          UISR.Security_Role_Id = @Security_Role_Id
                                          AND UISR.User_Info_Id = USR.User_Info_Id )
                  GROUP BY
                        USR.User_Info_Id
                  HAVING
                        count(USR.Security_Role_Id) = @Portfolio_Client_Cnt	-- Have access to all other clients of the portfolio
                  
END

;
GO

GRANT EXECUTE ON  [dbo].[User_Security_Role_INS_For_Client_Map] TO [CBMSApplication]
GO
