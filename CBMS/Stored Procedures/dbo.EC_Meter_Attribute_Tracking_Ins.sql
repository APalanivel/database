SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
    
/******                          
Name:   dbo.EC_Meter_Attribute_Tracking_Ins                       
            
Description:                          
        To insert Data to EC_Meter_Attribute_Tracking table.                          
            
 Input Parameters:                          
     Name        DataType   Default   Description                            
----------------------------------------------------------------------------------------                            
     @Meter_Id       INT            
     @EC_Meter_Attribute_Id    INT            
     @Start_Dt       DATE            
     @End_Dt       DATE            
     @EC_Meter_Attribute_Value   NVARCHAR(255)            
     @User_Info_Id      INT            
            
 Output Parameters:                                
    Name        DataType   Default   Description                            
----------------------------------------------------------------------------------------                            
            
 Usage Examples:                              
----------------------------------------------------------------------------------------                            
 BEGIN TRAN              
 SELECT * FROM dbo.EC_Meter_Attribute_Tracking WHERE EC_Meter_Attribute_Value='test' and meter_id=1            
 EXEC dbo.EC_Meter_Attribute_Tracking_Ins             
      @Meter_Id =1            
      ,@EC_Meter_Attribute_Id =2            
      ,@Start_Dt ='2015-05-06'            
      ,@End_Dt ='2015-06-06'            
      ,@EC_Meter_Attribute_Value ='test'            
      ,@User_Info_Id =100            
 SELECT * FROM dbo.EC_Meter_Attribute_Tracking WHERE EC_Meter_Attribute_Value='test' and meter_id=1            
 ROLLBACK TRAN                 
            
 BEGIN TRAN             
  SELECT TOP 1 ae.*,cd.Code_Value FROM dbo.Account_Exception ae             
    JOIN  dbo.Code cd ON ae.Exception_Status_Cd = cd.Code_Id            
    JOIN dbo.Codeset cs ON cd.Codeset_Id = cs.Codeset_Id            
    WHERE cs.Codeset_Name = 'Exception Status' AND cd.Code_Value = 'New'             
  SELECT * FROM dbo.EC_Meter_Attribute_Tracking WHERE EC_Meter_Attribute_Value='test' and meter_id=749559            
  EXEC dbo.EC_Meter_Attribute_Tracking_Ins             
    @Meter_Id =749559            
    ,@EC_Meter_Attribute_Id =2            
    ,@Start_Dt ='2015-05-06'            
    ,@End_Dt ='2015-06-06'            
    ,@EC_Meter_Attribute_Value ='test'            
    ,@User_Info_Id =16            
  SELECT TOP 1 ae.*,cd.Code_Value FROM dbo.Account_Exception ae             
    JOIN  dbo.Code cd ON ae.Exception_Status_Cd = cd.Code_Id            
    JOIN dbo.Codeset cs ON cd.Codeset_Id = cs.Codeset_Id            
    WHERE ae.Meter_Id = 749559              
  SELECT * FROM dbo.EC_Meter_Attribute_Tracking WHERE EC_Meter_Attribute_Value='test' and meter_id=749559            
 ROLLBACK TRAN                    
            
Author Initials:                          
    Initials  Name                          
----------------------------------------------------------------------------------------                            
 NR    Narayana Reddy     
 SC  Sreenivasulu Cheerala                          
 Modifications:                          
    Initials        Date   Modification                          
----------------------------------------------------------------------------------------                            
    NR    2015-04-22  Created For AS400.                 
    SP    2016-02-02  Invoice collection,regression issue when this sproc is called Invoice collection exceptions also getting closed            
         so made exception level by adding @Missing_Meter_Attribute_Exception_Type_Cd while update.                
 KVK    07/07/2017  modified to insert into Entity Audit with audit type as 2 instead of 1               
 SC  2020-04-21  Modified to close the missing meter attribute exception if and only if actual meter attribute exists/    
******/    
CREATE PROCEDURE [dbo].[EC_Meter_Attribute_Tracking_Ins]    
     (    
         @Meter_Id INT    
         , @EC_Meter_Attribute_Id INT    
         , @Start_Dt DATE    
         , @End_Dt DATE    
         , @EC_Meter_Attribute_Value NVARCHAR(255)    
         , @User_Info_Id INT    
         , @Meter_Attribute_Type_Cd INT    
         , @Vednor_Type_Id INT = NULL    
     )    
AS    
    BEGIN    
        SET NOCOUNT ON;    
    
        DECLARE    
            @New_Exception_Status_Cd INT    
            , @Closed_Exception_Status_Cd INT    
            , @Account_Id INT    
            , @Missing_Meter_Attribute_Exception_Type_Cd INT    
            , @Actual_Attribute_Type_Cd INT;    
    
        SELECT    
            @Actual_Attribute_Type_Cd = cd.Code_Id    
        FROM    
            dbo.Code cd    
            JOIN dbo.Codeset cs    
                ON cd.Codeset_Id = cs.Codeset_Id    
        WHERE    
            cd.Code_Value = 'Actual'    
            AND cs.Codeset_Name = 'MeterAttributeType';    
    
        SELECT    
            @New_Exception_Status_Cd = cd.Code_Id    
        FROM    
            dbo.Code cd    
            JOIN dbo.Codeset cs    
                ON cd.Codeset_Id = cs.Codeset_Id    
        WHERE    
            cs.Codeset_Name = 'Exception Status'    
            AND cd.Code_Value = 'New';    
    
        SELECT    
            @Closed_Exception_Status_Cd = cd.Code_Id    
        FROM    
            dbo.Code cd    
            JOIN dbo.Codeset cs    
                ON cd.Codeset_Id = cs.Codeset_Id    
        WHERE    
            cs.Codeset_Name = 'Exception Status'    
            AND cd.Code_Value = 'Closed';    
    
    
        SELECT    
            @Account_Id = Account_Id    
        FROM    
            Core.Client_Hier_Account cha    
        WHERE    
            Meter_Id = @Meter_Id    
            AND Account_Type = 'Utility';    
    
    
        SELECT    
            @Missing_Meter_Attribute_Exception_Type_Cd = C.Code_Id    
        FROM    
            dbo.Code C    
            INNER JOIN dbo.Codeset CS    
                ON C.Codeset_Id = CS.Codeset_Id    
        WHERE    
            CS.Codeset_Name = 'Exception Type'    
            AND C.Code_Value = 'Missing Meter Attribute';    
        INSERT INTO dbo.EC_Meter_Attribute_Tracking    
             (    
                 Meter_Id    
                 , EC_Meter_Attribute_Id    
                 , Start_Dt    
                 , End_Dt    
                 , EC_Meter_Attribute_Value    
                 , Created_User_Id    
                 , Created_Ts    
                 , Updated_User_Id    
                 , Last_Change_Ts    
                 , Meter_Attribute_Type_Cd    
             )    
        VALUES    
            (@Meter_Id    
             , @EC_Meter_Attribute_Id    
             , @Start_Dt    
             , @End_Dt    
             , @EC_Meter_Attribute_Value    
             , @User_Info_Id    
             , GETDATE()    
             , @User_Info_Id    
             , GETDATE()    
             , @Meter_Attribute_Type_Cd);    
    
    
        EXEC dbo.ADD_ENTITY_AUDIT_ITEM_P    
            @entity_identifier = @Account_Id    
            , @user_info_id = @User_Info_Id    
            , @audit_type = 2    
            , @entity_name = 'ACCOUNT_TABLE'    
            , @entity_type = 500;    
    

        UPDATE    
            ae    
        SET    
            ae.Exception_Status_Cd = @Closed_Exception_Status_Cd    
            , ae.Closed_By_User_Id = @User_Info_Id    
            , ae.Closed_Ts = GETDATE()    
            , ae.Last_Change_Ts = GETDATE()    
            , ae.Updated_User_Id = @User_Info_Id    
        FROM    
            dbo.Account_Exception ae    
        WHERE    
            ae.Meter_Id = @Meter_Id  
            AND ae.Exception_Status_Cd = @New_Exception_Status_Cd    
            AND ae.Exception_Type_Cd = @Missing_Meter_Attribute_Exception_Type_Cd    
            AND EXISTS (   SELECT    
                                1    
                           FROM    
                                dbo.EC_Meter_Attribute_Tracking emat    
                           WHERE    
                                emat.Meter_Attribute_Type_Cd = @Actual_Attribute_Type_Cd    
                                AND emat.Meter_Id = @Meter_Id);    
    
    
    END; 
GO



GRANT EXECUTE ON  [dbo].[EC_Meter_Attribute_Tracking_Ins] TO [CBMSApplication]
GO
