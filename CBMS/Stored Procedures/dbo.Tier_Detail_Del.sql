SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
    
/******              
    
NAME: [DBO].[Tier_Detail_Del]      
         
DESCRIPTION:     
 To Delete Tier_Detail records based on  Tier_Detail_Id.    
          
INPUT PARAMETERS:              
NAME   DATATYPE DEFAULT  DESCRIPTION              
------------------------------------------------------------              
@Tier_Detail_Id  INT          
                    
OUTPUT PARAMETERS:              
NAME   DATATYPE DEFAULT  DESCRIPTION       
           
------------------------------------------------------------              
USAGE EXAMPLES:              
------------------------------------------------------------      
 BEGIN TRAN
 EXEC Tier_Detail_Del  5    
 ROLLBACK
     
 BEGIN TRAN
 EXEC Tier_Detail_Del  10    
 ROLLBACK 
     
         
AUTHOR INITIALS:              
INITIALS NAME              
------------------------------------------------------------              
GK  GOPI    
              
MODIFICATIONS               
INITIALS DATE  MODIFICATION              
------------------------------------------------------------              
GK  02-MAY-11  CREATED

*/

CREATE PROCEDURE dbo.Tier_Detail_Del
@Tier_Detail_Id INT
AS
BEGIN

      SET NOCOUNT ON

      DELETE FROM
            dbo.Tier_detail
      WHERE
            Tier_Detail_Id = @Tier_Detail_Id      

END    

GO
GRANT EXECUTE ON  [dbo].[Tier_Detail_Del] TO [CBMSApplication]
GO
GO