SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	cbms_prod.dbo.cbmsAccountAudit_Get

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@account_audit_id	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.cbmsAccountAudit_Get 27
	EXEC dbo.cbmsAccountAudit_Get 10000

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script
	SSR			03/29/2010	Added table Owner name,Removed Entity(Entity_id) with Commodity(Commodity_id)
				04/05/2010  Removed @Myaccountid,Vwsitename view,Replaced Distinct with Group by Clause
 DMR		  09/10/2010 Modified for Quoted_Identifier
				
******/
CREATE PROCEDURE dbo.cbmsAccountAudit_Get
	(
	@account_audit_id INT
	)
AS

BEGIN

	SET NOCOUNT ON;
	
	SELECT
		aa.account_audit_id
		, aa.account_audit_batch_id
		, acct.account_id
		, acct.account_number
		, aa.audit_status_type_id
		, ast.entity_name audit_status_type
		, aa.month_to_review
		, aa.audit_issue
		, aa.response_image_id
		, aa.closed_date
		, aa.closed_reason_type_id
		, cr.entity_name closed_reason_type
		, aa.audit_comment
		, aa.auditor_id
		, ui.first_name + SPACE(1) + ui.last_name auditor
		, c.client_id
		, c.client_name
		, c.fiscalyear_startmonth_type_id fiscal_year_start_month_type_id
		, fy.entity_id fiscal_year_start_month_type
		, s.site_id
		, RTRIM(ad.city) + ', ' + st.state_name + ' (' + s.site_name + ')' Site_Name  
		, v.vendor_id
		, v.vendor_name
		, com.Commodity_Name commodity
	FROM 
		dbo.account_audit aa 
		JOIN
			(
			SELECT
				m.ACCOUNT_ID
				,m.METER_ID
			FROM
				dbo.Meter m

			UNION ALL

			SELECT
				map.account_id
				, map.meter_id
			FROM
				dbo.supplier_account_meter_map AS map
				JOIN dbo.account a
					ON a.account_id = map.ACCOUNT_ID 
			WHERE
				( map.meter_disassociation_date > a.Supplier_Account_Begin_Dt
					OR map.meter_disassociation_date IS NULL
				)
			) vam
			ON vam.ACCOUNT_ID = aa.ACCOUNT_ID		
		JOIN dbo.METER um
			ON um.METER_ID = vam.METER_ID
		JOIN dbo.Account uacc
			ON uacc.ACCOUNT_ID = um.ACCOUNT_ID
		JOIN dbo.RATE r
			ON r.RATE_ID = um.RATE_ID
		JOIN dbo.Commodity com
			ON com.Commodity_Id = r.commodity_type_id

		JOIN dbo.Site s
			ON S.site_id = uacc.site_id
		JOIN dbo.client c
			ON c.client_id = s.client_id
		JOIN ADDRESS Ad
			ON Ad.ADDRESS_ID = s.PRIMARY_ADDRESS_ID
		JOIN STATE st
			ON st.STATE_ID =Ad.STATE_ID		
		JOIN dbo.entity fy 
			ON fy.entity_id = c.fiscalyear_startmonth_type_id
		JOIN dbo.entity ast 
			ON ast.entity_id = aa.audit_status_type_id
		JOIN dbo.account acct 
			ON acct.account_id = aa.account_id
		JOIN dbo.vendor v 
			ON v.vendor_id = acct.vendor_id
		JOIN dbo.user_info ui 
			ON ui.user_info_id = aa.auditor_id
		LEFT OUTER JOIN dbo.entity cr 
			ON cr.entity_id = aa.closed_reason_type_id	
	WHERE 
		aa.account_audit_id = @account_audit_id
	GROUP BY 
			 aa.account_audit_id
			, aa.account_audit_batch_id
			, acct.account_id
			, acct.account_number
			, aa.audit_status_type_id
			, ast.entity_name 
			, aa.month_to_review
			, aa.audit_issue
			, aa.response_image_id
			, aa.closed_date
			, aa.closed_reason_type_id
			, cr.entity_name 
			, aa.audit_comment
			, aa.auditor_id
			, ui.first_name 
			, ui.last_name 
			, c.client_id
			, c.client_name
			, c.fiscalyear_startmonth_type_id 
			, fy.entity_id 
			, s.site_id
			, ad.city
			, st.state_name 
			, s.site_name 
			, v.vendor_id
			, v.vendor_name
			, com.Commodity_Name
	
END
GO
GRANT EXECUTE ON  [dbo].[cbmsAccountAudit_Get] TO [CBMSApplication]
GO
