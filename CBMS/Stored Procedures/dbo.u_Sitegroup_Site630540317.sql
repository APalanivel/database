SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[u_Sitegroup_Site630540317]
		@c1 int = NULL,
		@c2 int = NULL,
		@c3 datetime = NULL,
		@pkc2 int = NULL,
		@pkc1 int = NULL,
		@bitmap binary(1),
		@MSp2pPreVersion varbinary(32) , @MSp2pPostVersion varbinary(32) 

as
begin  
if (substring(@bitmap,1,1) & 2 = 2) or
 (substring(@bitmap,1,1) & 1 = 1)
begin 
update [dbo].[Sitegroup_Site] set
		[Sitegroup_id] = case substring(@bitmap,1,1) & 1 when 1 then @c1 else [Sitegroup_id] end,
		[Site_id] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [Site_id] end,
		[Last_Change_Ts] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [Last_Change_Ts] end		,$sys_p2p_cd_id = @MSp2pPostVersion

where [Site_id] = @pkc1
  and [Sitegroup_id] = @pkc2
		and ($sys_p2p_cd_id = @MSp2pPreVersion or $sys_p2p_cd_id is null)
end  
else
begin 
update [dbo].[Sitegroup_Site] set
		[Last_Change_Ts] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [Last_Change_Ts] end		,$sys_p2p_cd_id = @MSp2pPostVersion

where [Site_id] = @pkc1
  and [Sitegroup_id] = @pkc2
		and ($sys_p2p_cd_id = @MSp2pPreVersion or $sys_p2p_cd_id is null)
end 
if @@rowcount = 0
begin  
	declare @cur_version varbinary(32) 
		,@conflict_type int = 1
		,@conflict_type_txt nvarchar(20) = N'Update-Update'
		,@reason_code int = 1
		,@reason_text nvarchar(720) = NULL
		,@is_on_disk_winner bit = 0
		,@is_incoming_winner bit = 0
		,@peer_id_current_node int = NULL
		,@peer_id_incoming int
		,@tranid_incoming nvarchar(40)
		,@peer_id_on_disk int
		,@tranid_on_disk nvarchar(40)
	select @peer_id_incoming = sys.fn_replvarbintoint(@MSp2pPostVersion)
		,@tranid_incoming = sys.fn_replp2pversiontotranid(@MSp2pPostVersion)
	select @cur_version = $sys_p2p_cd_id from [dbo].[Sitegroup_Site] 
where [Site_id] = @pkc1
  and [Sitegroup_id] = @pkc2
	if @@rowcount = 0  
			select @conflict_type = 3, @conflict_type_txt = N'Update-Delete', @is_on_disk_winner = 1, @reason_text = formatmessage(22823), @reason_code = 0
	else 
	begin  
		select @peer_id_on_disk = sys.fn_replvarbintoint(@cur_version)
			,@tranid_on_disk = sys.fn_replp2pversiontotranid(@cur_version)
		if(@peer_id_incoming > @peer_id_on_disk)
			set @is_incoming_winner = 1
		else
			set @is_on_disk_winner = 1
	end  
		select @peer_id_current_node = p.originator_id from syspublications p join sysarticles a on a.pubid = p.pubid where a.objid = object_id(N'[dbo].[Sitegroup_Site]') and p.options & 0x1 = 0x1
	if (@peer_id_current_node is not null) 
	begin 
		if (@reason_text is NULL)
			if (@peer_id_incoming > @peer_id_on_disk)
			begin  
				select @reason_text = formatmessage(22822,@peer_id_incoming,@peer_id_on_disk,@peer_id_current_node)
			end  
			else  
			begin  
				select @reason_text = formatmessage(22821,@peer_id_incoming,@peer_id_on_disk,@peer_id_current_node)
			end  
		create table #change_id (change_id varbinary(8))
		insert [dbo].[conflict_dbo_Sitegroup_Site] (
		[Sitegroup_id],
		[Site_id],
		[Last_Change_Ts]
			,__$originator_id
			,__$origin_datasource
			,__$tranid
			,__$conflict_type
			,__$is_winner
			,__$reason_code
			,__$reason_text
			,__$update_bitmap
			,__$pre_version)
		output inserted.__$row_id into #change_id
		select 
    @pkc2,
    @pkc1,
    @c3			,@peer_id_current_node
			,@peer_id_incoming
			,@tranid_incoming
			,@conflict_type
			,@is_incoming_winner
			,@reason_code
			,@reason_text
			,@bitmap
			,@MSp2pPreVersion
		insert [dbo].[conflict_dbo_Sitegroup_Site] (

		[Sitegroup_id],
		[Site_id],
		[Last_Change_Ts]			,__$originator_id
			,__$origin_datasource
			,__$tranid
			,__$conflict_type
			,__$is_winner
			,__$reason_code
			,__$reason_text
			,__$pre_version
			,__$change_id)
		select 

		[Sitegroup_id],
		[Site_id],
		[Last_Change_Ts]			,@peer_id_current_node
			,@peer_id_on_disk
			,@tranid_on_disk
			,@conflict_type
			,@is_on_disk_winner
			,@reason_code
			,@reason_text
			,NULL
			,(select change_id from #change_id)
		from [dbo].[Sitegroup_Site] 

where [Site_id] = @pkc1
  and [Sitegroup_id] = @pkc2
	end 
	if(@peer_id_incoming > @peer_id_on_disk)
	begin  
if (substring(@bitmap,1,1) & 2 = 2) or
 (substring(@bitmap,1,1) & 1 = 1)
begin 
update [dbo].[Sitegroup_Site] set
		[Sitegroup_id] = case substring(@bitmap,1,1) & 1 when 1 then @c1 else [Sitegroup_id] end,
		[Site_id] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [Site_id] end,
		[Last_Change_Ts] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [Last_Change_Ts] end		,$sys_p2p_cd_id = @MSp2pPostVersion

where [Site_id] = @pkc1
  and [Sitegroup_id] = @pkc2
end  
else
begin 
update [dbo].[Sitegroup_Site] set
		[Last_Change_Ts] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [Last_Change_Ts] end		,$sys_p2p_cd_id = @MSp2pPostVersion

where [Site_id] = @pkc1
  and [Sitegroup_id] = @pkc2
end 
	end  
		if exists(select * from syspublications p join sysarticles a on a.pubid = p.pubid where a.objid = object_id(N'[dbo].[Sitegroup_Site]') and p.options & 0x10 = 0x10)
			raiserror(22815, 10, -1, @conflict_type_txt, @peer_id_current_node, @peer_id_incoming, @tranid_incoming, @peer_id_on_disk, @tranid_on_disk) with log
		else
			raiserror(22815, 16, -1, @conflict_type_txt, @peer_id_current_node, @peer_id_incoming, @tranid_incoming, @peer_id_on_disk, @tranid_on_disk) with log
end 
end 
GO
