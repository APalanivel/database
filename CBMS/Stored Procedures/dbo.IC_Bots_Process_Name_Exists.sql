SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          
            
 NAME: dbo.IC_Bots_Process_Name_Exists			
            
 DESCRIPTION:            
    To Save the Bots Process names    
            
 INPUT PARAMETERS:            
           
 Name                               DataType            Default      Description            
---------------------------------------------------------------------------------------------------------------          
@IC_Bots_Process_Name				VARCHAR(200)
     
             
 OUTPUT PARAMETERS:           
                  
 Name                               DataType            Default      Description            
---------------------------------------------------------------------------------------------------------------          
            
 USAGE EXAMPLES:                
---------------------------------------------------------------------------------------------------------------          


EXEC dbo.IC_Bots_Process_Name_Exists
    @IC_Bots_Process_Name = 'Hellow Welcome Bots'
   


 AUTHOR INITIALS:          
            
 Initials               Name            
---------------------------------------------------------------------------------------------------------------          
 NR                     Narayana Reddy              
             
 MODIFICATIONS:           
           
 Initials               Date            Modification          
---------------------------------------------------------------------------------------------------------------          
 NR                     2020-05-27      Created for SE2017-963          
           
******/


CREATE PROC [dbo].[IC_Bots_Process_Name_Exists]
    (
        @IC_Bots_Process_Name VARCHAR(200)
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @IC_Bots_Process_Name_Exists BIT = 0;


        SELECT
            @IC_Bots_Process_Name_Exists = 1
        FROM
            dbo.IC_Bots_Process ibp
        WHERE
            ibp.IC_Bots_Process_Name = @IC_Bots_Process_Name;


        SELECT  @IC_Bots_Process_Name_Exists AS IC_Bots_Process_Name_Exists;

    END;

GO
GRANT EXECUTE ON  [dbo].[IC_Bots_Process_Name_Exists] TO [CBMSApplication]
GO
