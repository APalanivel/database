SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    Trade.Deal_Ticket_Claim
   
    
DESCRIPTION:   
   
  
    
INPUT PARAMETERS:    
	Name          DataType       Default        Description    
-----------------------------------------------------------------------------    
	@User_Info_Id		INT
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec Trade.Deal_Ticket_Claim 49
	
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	RR          Raghu Reddy
    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	RR          2019-01-10	GRM Proejct.
     
    
******/

CREATE PROC [Trade].[Deal_Ticket_Claim]
      ( 
       @User_Info_Id INT
      ,@Deal_Ticket_Id VARCHAR(MAX) )
AS 
BEGIN
      SET NOCOUNT ON;
      
      DECLARE @QUEUE_ID INT
      
      SELECT
            @QUEUE_ID = QUEUE_ID
      FROM
            dbo.USER_INFO
      WHERE
            USER_INFO_ID = @User_Info_Id
      
      UPDATE
            dt
      SET   
            dt.Queue_Id = @QUEUE_ID
      FROM
            Trade.Deal_Ticket dt
      WHERE
            EXISTS ( SELECT
                        1
                     FROM
                        dbo.ufn_split(@Deal_Ticket_Id, ',')
                     WHERE
                        CAST(segments AS INT) = dt.Deal_Ticket_Id )
END;
GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Claim] TO [CBMSApplication]
GO
