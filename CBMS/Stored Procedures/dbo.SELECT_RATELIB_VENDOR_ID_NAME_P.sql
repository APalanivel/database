SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE dbo.SELECT_RATELIB_VENDOR_ID_NAME_P
	@stateId INT
AS
BEGIN

	 SET NOCOUNT ON

	SELECT v.VENDOR_ID, 
		v.VENDOR_NAME 
	FROM dbo.VENDOR v INNER JOIN dbo.VENDOR_STATE_MAP vsm ON  vsm.VENDOR_ID = v.VENDOR_ID
		INNER JOIN dbo.entity venType ON venType.entity_id = v.vendor_type_id
	WHERE vsm.STATE_ID = @stateId
		AND venType.entity_name = 'Utility'
	ORDER BY v.vendor_name

END
GO
GRANT EXECUTE ON  [dbo].[SELECT_RATELIB_VENDOR_ID_NAME_P] TO [CBMSApplication]
GO
