SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Contract_Outside_Contract_Term_Config_Del          
              
Description:              
        To insert Data into Contract_Level_Supplier_Recalc_Type table.              
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
     @Contract_Outside_Contract_Term_Config_Id						INT
    
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                
	
	BEGIN TRAN  

	SELECT * FROM Contract_Outside_Contract_Term_Config WHERE Contract_Outside_Contract_Term_Config_Id =1148520

    EXEC dbo.Contract_Outside_Contract_Term_Config_Del 
      @Contract_Outside_Contract_Term_Config_Id =1148520    
    
	SELECT * FROM Contract_Outside_Contract_Term_Config WHERE Contract_Outside_Contract_Term_Config_Id =1148520

	ROLLBACK TRAN             
             
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2019-06-26		Created For Add contract.   
             
******/
CREATE PROCEDURE [dbo].[Contract_Outside_Contract_Term_Config_Del]
     (
         @Contract_Outside_Contract_Term_Config_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;


        DELETE
        cloct
        FROM
            dbo.Contract_Outside_Contract_Term_Config cloct
        WHERE
            cloct.Contract_Outside_Contract_Term_Config_Id = @Contract_Outside_Contract_Term_Config_Id;



    END;


GO
GRANT EXECUTE ON  [dbo].[Contract_Outside_Contract_Term_Config_Del] TO [CBMSApplication]
GO
