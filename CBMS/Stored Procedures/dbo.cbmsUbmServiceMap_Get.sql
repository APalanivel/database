SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[cbmsUbmServiceMap_Get]
	( @MyAccountId int
	, @ubm_service_map_id int
	)
AS
BEGIN

	   select ubm_service_map_id
		, ubm_id
		, ubm_service_code
		, ubm_service_type_id
		, commodity_type_id
	     from ubm_service_map
	    where ubm_service_map_id = @ubm_service_map_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsUbmServiceMap_Get] TO [CBMSApplication]
GO
