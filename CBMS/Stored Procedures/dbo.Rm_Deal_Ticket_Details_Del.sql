SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: DBO.Rm_Deal_Ticket_Details_Del  

DESCRIPTION: It Deletes Rm Deal Ticket Details for Selected Rm Deal Ticket Details Id.
      
INPUT PARAMETERS:          
	NAME							DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Rm_Deal_Ticket_Details_Id		INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
	EXEC dbo.Rm_Deal_Ticket_Details_Del 7927
  Rollback Tran
  
  SELECT TOP 100 * FROM RM_DEAL_TICKET_DETAILS a WHERE NOT EXISTS(SELECT 1 FROM RM_DEAL_TICKET_VOLUME_DETAILS b WHERE a.RM_DEAL_TICKET_DETAILS_ID = b.RM_DEAL_TICKET_DETAILS_ID)

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			31-AUG-10	CREATED

*/

CREATE PROCEDURE dbo.Rm_Deal_Ticket_Details_Del
    (
      @Rm_Deal_Ticket_Details_Id	 INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.Rm_Deal_Ticket_Details
	WHERE
		Rm_Deal_Ticket_Details_Id = @Rm_Deal_Ticket_Details_Id
END
GO
GRANT EXECUTE ON  [dbo].[Rm_Deal_Ticket_Details_Del] TO [CBMSApplication]
GO
