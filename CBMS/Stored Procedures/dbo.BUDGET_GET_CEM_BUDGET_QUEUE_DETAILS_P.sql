
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: [dbo].[BUDGET_GET_CEM_BUDGET_QUEUE_DETAILS_P]                      
                      
 DESCRIPTION:   
 
                            
                        
 INPUT PARAMETERS:                        
                       
 Name                               DataType            Default				Description                        
---------------------------------------------------------------------------------------------------------------                      
 @ClientId							INT

 OUTPUT PARAMETERS:          
 Name                                DataType            Default        Description
---------------------------------------------------------------------------------------------------------------

 USAGE EXAMPLES:
---------------------------------------------------------------------------------------------------------------

	exec BUDGET_GET_CEM_BUDGET_QUEUE_DETAILS_P 207

	exec BUDGET_GET_CEM_BUDGET_QUEUE_DETAILS_P 235

	exec BUDGET_GET_CEM_BUDGET_QUEUE_DETAILS_P 1003

	exec BUDGET_GET_CEM_BUDGET_QUEUE_DETAILS_P 11923

	exec BUDGET_GET_CEM_BUDGET_QUEUE_DETAILS_P 11231

	SELECT Client_Id, COUNT(1) CNT FROM Budget GROUP BY Client_id ORDER BY Cnt DESC

 AUTHOR INITIALS:      
 Initials               Name
---------------------------------------------------------------------------------------------------------------
 HG						Hariharasuthan Ganesan

 MODIFICATIONS:

 Initials               Date            Modification
---------------------------------------------------------------------------------------------------------------
 HG						2014-04-04		MAINT-2699,Modified for performance
											-- Comments header added
******/

CREATE PROCEDURE dbo.BUDGET_GET_CEM_BUDGET_QUEUE_DETAILS_P ( @ClientId INT )
AS
BEGIN

      SET NOCOUNT ON

      CREATE TABLE #Budget_Account_Dtl
            ( 
             Budget_Id INT
            ,Budget_Account_Id INT
            ,Account_Id INT
            ,Commodity_Id INT
            ,Account_Service_Level_Id INT
            ,Tariff_Transport VARCHAR(20)
            ,Last_Supplier_Type_Contract_End_Dt DATE
            ,CD_Create_Multiplier DECIMAL(32, 16)
            ,Rates_Completed_Date DATE
            ,Rates_Reviewed_Date DATE
            ,Cannot_Performed_By INT
            ,Sourcing_Completed_Date DATE
            ,PRIMARY KEY CLUSTERED ( Budget_Id, Budget_Account_Id ) )

      DECLARE @Budgets TABLE
            ( 
             Budget_Id INT PRIMARY KEY CLUSTERED
            ,Budget_Name VARCHAR(200)
            ,Commodity_Id INT
            ,Due_Date DATETIME
            ,No_Of_Accounts INT
            ,Cannot_Perform_Accounts INT )

      DECLARE
            @Large_Industrial_Id INT
           ,@Small_Industrial_Id INT
           ,@Commercial_Id INT
           ,@Other_Id INT

      SELECT
            @Large_Industrial_Id = Entity_Id
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_TYPE = 708
            AND ENTITY_NAME = 'A'
            AND ENTITY_DESCRIPTION = 'Large Industrial'

      SELECT
            @Small_Industrial_Id = Entity_Id
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_TYPE = 708
            AND ENTITY_NAME = 'B'
            AND ENTITY_DESCRIPTION = 'Small Industrial'

      SELECT
            @Commercial_Id = Entity_Id
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_TYPE = 708
            AND ENTITY_NAME = 'C'
            AND ENTITY_DESCRIPTION = 'Commercial'

      SELECT
            @Other_Id = Entity_Id
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_TYPE = 708
            AND ENTITY_NAME = 'D'
            AND ENTITY_DESCRIPTION = 'Other'

      INSERT      INTO @Budgets
                  (
                   Budget_Id
                  ,Budget_Name
                  ,Commodity_Id
                  ,Due_Date
                  ,No_Of_Accounts
                  ,Cannot_Perform_Accounts )
                  SELECT
                        b.Budget_Id
                       ,b.Budget_Name
                       ,b.COMMODITY_TYPE_ID
                       ,b.Due_Date
                       ,COUNT(ba.Budget_Account_Id) AS No_Of_Accounts
                       ,COUNT(CASE WHEN ba.Cannot_Performed_By IS NOT NULL THEN ba.Budget_Account_Id
                              END) AS Cannot_Perform_Accounts
                  FROM
                        dbo.Budget b
                        INNER JOIN dbo.Budget_Account ba
                              ON ba.Budget_Id = b.Budget_Id
                  WHERE
                        b.client_id = @clientId
                        AND ba.Is_Deleted = 0
                        AND b.Is_Shown_On_Cem_Home_Page = 1
                  GROUP BY
                        b.Budget_Id
                       ,b.Budget_Name
                       ,b.COMMODITY_TYPE_ID
                       ,b.Due_Date
   
      INSERT      INTO #Budget_Account_Dtl
                  ( 
                   Budget_Id
                  ,Budget_Account_Id
                  ,Account_Id
                  ,Commodity_Id
                  ,Account_Service_Level_Id
                  ,Tariff_Transport
                  ,Last_Supplier_Type_Contract_End_Dt
                  ,CD_Create_Multiplier
                  ,Rates_Completed_Date
                  ,Rates_Reviewed_Date
                  ,Cannot_Performed_By
                  ,Sourcing_Completed_Date )
                  SELECT
                        ba.Budget_Id
                       ,ba.Budget_Account_Id
                       ,ba.Account_Id
                       ,ta.Commodity_Id
                       ,ta.Account_Service_level_Cd
                       ,ta.Tariff_Transport
                       ,ta.Last_Supplier_Type_Contract_End_Dt
                       ,ba.CD_Create_Multiplier
                       ,ba.Rates_Completed_Date
                       ,ba.Rates_Reviewed_Date
                       ,ba.Cannot_Performed_By
                       ,ba.Sourcing_Completed_Date
                  FROM
                        ( SELECT
                              ucha.Account_Id
                             ,ucha.Account_Service_level_Cd
                             ,ucha.Commodity_Id
                             ,CASE WHEN MAX(con.Contract_End_Date) >= GETDATE() THEN 'Transport'
                                   ELSE 'Tariff'
                              END AS Tariff_Transport
                             ,MAX(con.Contract_End_Date) AS Last_Supplier_Type_Contract_End_Dt
                          FROM
                              Core.Client_Hier_Account ucha
                              LEFT OUTER JOIN ( Core.Client_Hier_Account scha
                                                INNER JOIN dbo.Contract con
                                                      ON con.CONTRACT_ID = scha.Supplier_Contract_ID
                                                INNER JOIN dbo.ENTITY ct
                                                      ON ct.ENTITY_ID = con.Contract_Type_Id )
                                                ON scha.Meter_Id = ucha.Meter_Id
                                                   AND scha.Account_Type = 'Supplier'
                                                   AND ct.ENTITY_NAME = 'Supplier'
                          WHERE
                              ucha.Account_Type = 'Utility'
                          GROUP BY
                              ucha.Account_Id
                             ,ucha.Account_Service_level_Cd
                             ,ucha.Commodity_Id ) ta
                        INNER JOIN dbo.Budget_Account ba
                              ON ba.Account_Id = ta.Account_Id
                        INNER JOIN @Budgets bdg
                              ON bdg.BUDGET_ID = ba.Budget_Id
                                 AND bdg.Commodity_Id = ta.Commodity_Id
                  WHERE
                        ba.IS_DELETED = 0

      SELECT
            bdg.Budget_Id
           ,bdg.Budget_Name
           ,bdg.Due_Date
           ,ISNULL(CAST(( ( ( ISNULL(bdg_tariff_acc.Account_Cnt, 0) + ISNULL(bdg_acc_type.Account_Cnt, 0) + ISNULL(bdg_transport_acc.Account_Cnt, 0) ) * 100 ) / NULLIF(bdg.No_Of_Accounts, 0) ) AS INT), 0) AS Percentage_Complete
           ,ISNULL(bdg.Cannot_Perform_Accounts, 0) AS Cannot_Perform_Accounts
      FROM
            @Budgets bdg --Budget Tariff Account
            LEFT OUTER JOIN ( SELECT
                                    Budget_Id
                                   ,COUNT(1) Account_Cnt
                              FROM
                                    #Budget_Account_Dtl bdg_tariff_acc
                              WHERE
                                    bdg_tariff_acc.Tariff_Transport = 'Tariff'
                                    AND bdg_tariff_acc.Account_Service_Level_Id IN ( @Large_Industrial_Id, @Small_Industrial_Id )
                                    AND bdg_tariff_acc.Rates_Completed_Date IS NOT NULL
                                    AND bdg_tariff_acc.Rates_Reviewed_Date IS NOT NULL
                                    AND bdg_tariff_acc.Cannot_Performed_By IS NULL
                              GROUP BY
                                    Budget_Id ) bdg_tariff_acc
                  ON bdg_tariff_acc.Budget_Id = bdg.Budget_Id
            -- Budget Account Type
            LEFT OUTER JOIN ( SELECT
                                    Budget_Id
                                   ,COUNT(1) AS Account_Cnt
                              FROM
                                    #Budget_Account_Dtl bdg_acc_type
                              WHERE
                                    bdg_acc_type.Tariff_Transport = 'Tariff'
                                    AND bdg_acc_type.Account_Service_level_Id IN ( @Commercial_Id, @Other_Id )
                                    AND bdg_acc_type.Cd_Create_Multiplier IS NOT NULL
                                    AND bdg_acc_type.Cannot_Performed_By IS NULL
                              GROUP BY
                                    Budget_Id ) bdg_acc_type
                  ON bdg_acc_type.Budget_Id = bdg.Budget_Id
            -- Budget Transport account
            LEFT OUTER JOIN ( SELECT
                                    Budget_Id
                                   ,COUNT(1) AS Account_Cnt
                              FROM
                                    #Budget_Account_Dtl bdg_transport_acc
                              WHERE
                                    bdg_transport_acc.Tariff_Transport = 'Transport'
                                    AND bdg_transport_acc.Cannot_Performed_By IS NULL
                                    AND bdg_transport_acc.Sourcing_Completed_Date IS NOT NULL
                                    AND bdg_transport_acc.Rates_Completed_Date IS NOT NULL
                                    AND bdg_transport_acc.Rates_Reviewed_Date IS NOT NULL
                              GROUP BY
                                    Budget_Id ) bdg_transport_acc
                  ON bdg_transport_acc.Budget_Id = bdg.Budget_Id
      ORDER BY
            bdg.Budget_Id DESC
   
      DROP TABLE #Budget_Account_Dtl
				
END
;
GO


GRANT EXECUTE ON  [dbo].[BUDGET_GET_CEM_BUDGET_QUEUE_DETAILS_P] TO [CBMSApplication]
GO
