
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_IS_RFP_ACCOUNT_GROUP_ID_POSTED_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@accountId     	int       	          	
	@rfpId         	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC dbo.SR_RFP_IS_RFP_ACCOUNT_GROUP_ID_POSTED_P 12105078,13279
	EXEC dbo.SR_RFP_IS_RFP_ACCOUNT_GROUP_ID_POSTED_P 10012036,13279

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2016-03-14	Global Sourcing - Phase3 - GCS-524 - Modified "RFP sent to supplier status" entity values

******/

CREATE PROCEDURE [dbo].[SR_RFP_IS_RFP_ACCOUNT_GROUP_ID_POSTED_P]
      ( 
       @accountId INT
      ,@rfpId INT )
AS 
BEGIN
      SET nocount ON

      DECLARE @accountGroupId INT	
      SELECT
            @accountGroupId = sr_rfp_account_id
      FROM
            sr_rfp_account
      WHERE
            sr_rfp_id = @rfpId
            AND account_id = @accountId
            AND is_deleted = 0

      DECLARE @isBidGroup INT	
      SELECT
            @isBidGroup = isnull(( SELECT
                                    sr_rfp_bid_group_id
                                   FROM
                                    sr_rfp_account
                                   WHERE
                                    sr_rfp_id = @rfpId
                                    AND account_id = @accountId ), 0)

      DECLARE @entityId INT
      SELECT
            @entityId = entity_id
      FROM
            entity
      WHERE
            entity_type = 1013
            AND entity_name = 'post'

      DECLARE @cnt INT
      SELECT
            @cnt = count(1)
      FROM
            sr_rfp_send_supplier sendSupp
           ,entity e
      WHERE
            sendSupp.sr_account_group_id = @accountGroupId
            AND sendSupp.is_bid_group = @isBidGroup
            AND sendSupp.status_type_id = e.entity_id
            AND e.entity_id = @entityId

      RETURN @cnt
END
;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_IS_RFP_ACCOUNT_GROUP_ID_POSTED_P] TO [CBMSApplication]
GO
