SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[SR_RFP_LP_Comment_Del]  
     
DESCRIPTION: 
	It Deletes SR RFP Load Profile Comment for Selected 
						SR RFP Load Profile Comment Id.
      
INPUT PARAMETERS:          
NAME					  DATATYPE	DEFAULT		DESCRIPTION          
-------------------------------------------------------------------          
@SR_RFP_LP_Comment_Id	  INT	

   
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------
  
	Begin Tran
		EXEC SR_RFP_LP_Comment_Del  1312
	Rollback Tran
    
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR			PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR		    28-MAY-10	CREATED     

*/  

CREATE PROCEDURE dbo.SR_RFP_LP_Comment_Del
   (
    @SR_RFP_LP_Comment_Id INT
   )
AS 
BEGIN

    SET NOCOUNT ON;
   
    DELETE 
   	FROM	
		dbo.SR_RFP_LP_COMMENT
	WHERE 
		SR_RFP_LP_COMMENT_ID = @SR_RFP_LP_Comment_Id
END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_LP_Comment_Del] TO [CBMSApplication]
GO
