SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********  
NAME:	[dbo].[IC_Supllier_Account_Changes_Queue_Ins]

DESCRIPTION:     

	
INPUT PARAMETERS:    
Name								DataType		Default		Description    
----------------------------------------------------------------------------    
@User_Info_Id					 	INT      

OUTPUT PARAMETERS:    
Name					DataType		Default		Description    
----------------------------------------------------------------------------    

USAGE EXAMPLES:    
----------------------------------------------------------------------------   
 


EXEC dbo.CODE_SEL_BY_CodeSet_Name
    @CodeSet_Name = 'Request Status'
    , @Code_Value = 'Completed'

BEGIN TRAN

Select  * from  dbo.IC_Supllier_Account_Changes_Queue where Account_Id = 109320

EXEC [dbo].[IC_Supllier_Account_Changes_Queue_Ins]
    @Contract_Id = 57495
    , @Account_Id = 109320
    , @User_Info_Id = 16

Select  * from  dbo.IC_Supllier_Account_Changes_Queue where Account_Id = 109320


ROLLBACK TRAN

AUTHOR INITIALS:    
Initials	Name    
----------------------------------------------------------------------------    
NR			Narayana Reddy
 
MODIFICATIONS     
Initials	Date			Modification    
-----------------------------------------------------------------------------    
NR			2020-04-22		Created for Small enhancement
******/

CREATE PROCEDURE [dbo].[IC_Supllier_Account_Changes_Queue_Ins]
    (
        @Contract_Id INT
        , @Account_Id INT = NULL
        , @User_Info_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Pending_Status_Cd INT;

        SELECT
            @Pending_Status_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Request Status'
            AND c.Code_Value = 'Pending';


        INSERT INTO dbo.IC_Supllier_Account_Changes_Queue
             (
                 Account_Id
                 , Status_Cd
                 , Event_By
                 , Event_Dt
                 , IC_Supllier_Account_Changes_Batch_Id
             )
        SELECT
            cha.Account_Id
            , @Pending_Status_Cd
            , @User_Info_Id
            , GETDATE()
            , NULL
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Supplier_Contract_ID = @Contract_Id
            AND (   @Account_Id IS NULL
                    OR  cha.Account_Id = @Account_Id)
            AND cha.Account_Type = 'Supplier'
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.IC_Supllier_Account_Changes_Queue icsac
                               WHERE
                                    icsac.Account_Id = @Account_Id
                                    AND icsac.Status_Cd = @Pending_Status_Cd
                                    AND icsac.IC_Supllier_Account_Changes_Batch_Id IS NULL)
            AND EXISTS (   SELECT
                                1
                           FROM
                                dbo.Invoice_Collection_Account_Config icac
                           WHERE
                                icac.Account_Id = @Account_Id)
        GROUP BY
            cha.Account_Id;



    END;

GO
GRANT EXECUTE ON  [dbo].[IC_Supllier_Account_Changes_Queue_Ins] TO [CBMSApplication]
GO
