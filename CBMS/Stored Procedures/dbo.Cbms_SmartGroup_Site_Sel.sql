SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 
 dbo.SmartGroup_Site_SEL

 DESCRIPTION:   
 
       This procedure process the rules of a smart group and fetch the valid sites.

 INPUT PARAMETERS:  
	Name            DataType    Default     Description  
 ------------------------------------------------------------    
	@SiteGroup_id   INT                  

 OUTPUT PARAMETERS:  
	Name            DataType      Default   Description  
 ------------------------------------------------------------

 USAGE EXAMPLES:  
 ------------------------------------------------------------  


       EXEC dbo.Cbms_SmartGroup_Site_Sel 4

       
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	NR          Narayana Reddy

MODIFICATIONS
	Initials	Date          Modification
------------------------------------------------------------
	NR          2018-09-27    Create for Global Risk Management
******/

CREATE PROCEDURE [dbo].[Cbms_SmartGroup_Site_Sel]
      ( 
       @Cbms_Sitegroup_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;

      DECLARE
            @Inclusive BIT
           ,@Rule_Count SMALLINT;

      DECLARE @Tbl_Rules TABLE ( Rule_Id INT NOT NULL );

      DECLARE
            @CurRuleId INT
           ,@Condition VARCHAR(50)
           ,@SourceValue VARCHAR(50)
           ,@TargetTest VARCHAR(50)
           ,@Stmt VARCHAR(8000)
           ,@WClause VARCHAR(4000)
           ,@Group_Id INT
           ,@Client_Id VARCHAR(50);

      CREATE TABLE #SiteList ( Site_id INT );


      SET @Inclusive = ( SELECT
                              MAX(CAST(sgr.Is_Inclusive AS INT))
                         FROM
                              dbo.CBMS_Sitegroup_Rule sgr
                         WHERE
                              sgr.Cbms_Sitegroup_Id = @Cbms_Sitegroup_Id );

      SELECT
            @Client_Id = CAST(Client_Id AS VARCHAR(50))
      FROM
            dbo.Cbms_Sitegroup
      WHERE
            Cbms_Sitegroup_Id = @Cbms_Sitegroup_Id;

      INSERT      @Tbl_Rules
                  ( 
                   Rule_Id )
                  SELECT
                        sgr.CBMS_Sitegroup_Rule_Id
                  FROM
                        dbo.Cbms_Sitegroup sg
                        INNER JOIN dbo.CBMS_Sitegroup_Rule sgr
                              ON sg.Cbms_Sitegroup_Id = sgr.Cbms_Sitegroup_Id
                  WHERE
                        sg.Cbms_Sitegroup_Id = @Cbms_Sitegroup_Id;


      SELECT
            @Rule_Count = COUNT(Rule_Id)
      FROM
            @Tbl_Rules;

      SELECT
            @CurRuleId = MIN(Rule_Id)
      FROM
            @Tbl_Rules;

      WHILE ( @CurRuleId IS NOT NULL ) 
            BEGIN

                  SET @Stmt = '';
                  SET @WClause = '';
                  SET @Condition = '';
                  SET @SourceValue = '';
                  SET @TargetTest = '';

                  SELECT
                        @SourceValue = MAX(CASE WHEN fp.Param_Name = 'SourceValue' THEN fp.Default_Value
                                           END)
                       ,@Condition = MAX(CASE WHEN fp.Param_Name = 'Condition' THEN cd.Code_Value
                                         END)
                       ,@TargetTest = MAX(CASE WHEN fp.Param_Name = 'TargetTest' THEN rpv.Param_Value
                                          END)
                       ,@Stmt = MAX(cfc.Base_Stmt)
                  FROM
                        dbo.CBMS_Sitegroup_Rule sgr
                        INNER JOIN dbo.Cbms_Rule_Filter rf
                              ON sgr.Cbms_Rule_Filter_Id = rf.Cbms_Rule_Filter_Id
                        INNER JOIN dbo.Cbms_Filter_Param fp
                              ON rf.Cbms_Rule_Filter_Id = fp.Cbms_Rule_Filter_Id
                        INNER JOIN dbo.Cbms_Filter_Class cfc
                              ON cfc.Cbms_Filter_Class_Id = rf.Cbms_Filter_Class_Id
                        LEFT JOIN dbo.Cbms_Sitegroup_Rule_Param_Value rpv
                              ON sgr.CBMS_Sitegroup_Rule_Id = rpv.CBMS_Sitegroup_Rule_Id
                                 AND fp.Cbms_Filter_Param_Id = rpv.Cbms_Filter_Param_Id
                        LEFT JOIN dbo.Code cd
                              ON CONVERT(VARCHAR, cd.Code_Id) = rpv.Param_Value
                        LEFT JOIN dbo.Codeset cs
                              ON cs.Codeset_Id = cd.Codeset_Id
                  WHERE
                        sgr.CBMS_Sitegroup_Rule_Id = @CurRuleId;

                  SELECT
                        @WClause = ' CH.Client_Id = ' + @Client_Id + ' AND ' + @SourceValue + @Condition + @TargetTest;


                  SET @WClause = @WClause + ' GROUP BY CH.Site_Id';

                  SET @Stmt = 'INSERT INTO #SiteList ' + @Stmt + @WClause;



                  EXEC (@Stmt);
                -----------********** Rule Processing block End *************---------------------------

                  SELECT
                        @CurRuleId = MIN(Rule_Id)
                  FROM
                        @Tbl_Rules
                  WHERE
                        Rule_Id > @CurRuleId;

            END; -- End for Rule WHILE Loop

      IF @Inclusive = 1 
            BEGIN

                  SELECT
                        sl.Site_id
                       ,RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
                  FROM
                        #SiteList sl
                        INNER JOIN Core.Client_Hier ch
                              ON sl.Site_id = ch.Site_Id
                  WHERE
                        sl.Site_id > 0
                  GROUP BY
                        sl.Site_id
                       ,RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
                  HAVING
                        COUNT(1) >= @Rule_Count;    -- All: Must be Equal | Not Needed for ANY

            END;
      ELSE 
            BEGIN

                  SELECT
                        sl.Site_id
                       ,RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
                  FROM
                        #SiteList sl
                        INNER JOIN Core.Client_Hier ch
                              ON sl.Site_id = ch.Site_Id
                  WHERE
                        sl.Site_id > 0
                  GROUP BY
                        sl.Site_id
                       ,RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')';

            END;

      DROP TABLE #SiteList;

END;




GO
GRANT EXECUTE ON  [dbo].[Cbms_SmartGroup_Site_Sel] TO [CBMSApplication]
GO
