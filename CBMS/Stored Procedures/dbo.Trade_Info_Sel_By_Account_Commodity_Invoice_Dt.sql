SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  Trade_Info_Sel_By_Account_Commodity_Invoice_Dt  
  
DESCRIPTION:  
  get Trade information by Invoice dt, Account, Commodity.  
  
  
INPUT PARAMETERS:  
 Name      DataType  Default Description  
------------------------------------------------------------  
 @Account_Id     INT  
    @Commodity_Id    INT  
    @Start_Dt     DATE  
    @End_Dt      DATE  
  
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
   
exec Trade_Info_Sel_By_Account_Commodity_Invoice_Dt  
      @Account_Id = 1450012  
    , @Commodity_Id = 291  
    , @Start_dt = '05/01/2018'  
    , @End_dt = '08/31/2018'  
  
-- Utility  
exec Trade_Info_Sel_By_Account_Commodity_Invoice_Dt  
      @Account_Id = 94484  
    , @Commodity_Id = 291  
    , @Start_dt = '01/01/2019'  
    , @End_dt = '03/28/2019'  
  
-- Supplier Account  
exec Trade_Info_Sel_By_Account_Commodity_Invoice_Dt_pradip  
      @Account_Id = 117813  
    , @Commodity_Id = 291  
    , @Start_dt = '05/01/2019'  
    , @End_dt = '08/31/2019'  
  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 RKV   Ravi kumar Raju  
 RR   Raghu Reddy  
   
MODIFICATIONS  
  
 Initials Date  Modification  
------------------------------------------------------------  
 RKV   2013-02-26 SE2017-566 created  
 HG   2018-06-21 MAINT-7511, Modified to remove the filter condition Hedget_Type = Physical on the sub query of RM_ONBOARD_HEDGE_SETUP.  
 RR   04-06-2019 GRM  GRM-914 Modified the script to refer new schema  
******/  
CREATE PROCEDURE [dbo].[Trade_Info_Sel_By_Account_Commodity_Invoice_Dt] --_pradip  
      (  
      @Account_Id   INT  
    , @Commodity_Id INT  
    , @Start_Dt     DATE  
    , @End_Dt       DATE )  
AS  
      BEGIN  
            SET NOCOUNT ON;  
  
  
            DECLARE  
                  @Physical                           INT  
                , @fixed_price                        INT  
                , @nymex                              INT  
                , @order_executed                     INT  
                , @trigger                            INT  
                , @Individual_month_Pricing_Entity_Id INT  
                , @Weighted_Strip_Price_Entity_Id     INT  
                , @Trigger_Status_Fixed               INT  
                , @Trigger_Status_Closed              INT  
                , @Commodity_Name                     VARCHAR(50);  
  
            DECLARE @CHA TABLE  
                  ( Client_Hier_Id INT  
                  , Account_Id     INT  
                  , PRIMARY KEY CLUSTERED (  
                          Client_Hier_Id  
                        , Account_Id ));  
  
            DECLARE @Trade_Info TABLE  
                  ( RM_Deal_Ticket_Id  INT  
                  , Trade_Id           INT  
                  , Deal_Initiated_Dt  DATE  
                  , Trade_Start_Dt     DATE  
                  , Trade_End_Dt       DATE  
                  , Trade_Type         VARCHAR(255)  
                  , Hedge_Volume       DECIMAL(20, 2)  
                  , Uom_Id             INT  
                  , Frequency_Type     VARCHAR(255)  
                  , Hedge_Price        DECIMAL(8, 4)  
                  , Currency_Unit_Id   INT  
                  , Time_Of_Day        VARCHAR(255)  
                  , Trade_Level        VARCHAR(255)  
                  , Months             DATE  
                  , Hedge_Date         DATE  
                  , DEAL_TICKET_NUMBER VARCHAR(200)  
                  , Client_Hier_Id     INT  
                  , Account_Id         INT  
                  , Contract_Id        INT 
				  , Trade_Action_Type varchar(256));  
  
            SELECT  
                  @Individual_month_Pricing_Entity_Id = max(  CASE WHEN cd.Code_Value = 'Individual Month Pricing'  
                                                                         THEN cd.Code_Id  
                                                              END)  
                , @Weighted_Strip_Price_Entity_Id = max(  CASE WHEN cd.Code_Value = 'Weighted Strip Price'  
                                                                     THEN cd.Code_Id  
                                                          END)  
            FROM  dbo.Code cd  
                  INNER JOIN  
                  dbo.Codeset cs  
                        ON cd.Codeset_Id = cs.Codeset_Id  
            WHERE cs.Codeset_Name = 'Trade Pricing Option';  
  
            SELECT  
                  @Physical = ENTITY_ID  
            FROM  dbo.ENTITY  
            WHERE ENTITY_DESCRIPTION = 'HEDGE_TYPE'  
                  AND   ENTITY_NAME LIKE 'Physical';  
  
            SELECT  
                  @fixed_price = cd.Code_Id  
            FROM  dbo.Code cd  
                  INNER JOIN  
                  dbo.Codeset cs  
                        ON cd.Codeset_Id = cs.Codeset_Id  
            WHERE cs.Codeset_Name = 'Deal Ticket Type'  
                  AND   cd.Code_Value = 'Fixed Price';  
  
            SELECT  
                  @nymex = ENTITY_ID  
            FROM  dbo.ENTITY  
            WHERE ENTITY_DESCRIPTION = 'HEDGE_MODE_TYPE'  
                  AND   ENTITY_NAME = 'nymex';  
  
            SELECT  
                  @order_executed = Workflow_Status_Id  
            FROM  Trade.Workflow_Status  
            WHERE Workflow_Status_Name = 'Order Executed';  
  
            SELECT  
                  @Commodity_Name = Commodity_Name  
            FROM  dbo.Commodity  
            WHERE Commodity_Id = @Commodity_Id;  
  
            INSERT INTO @CHA (  
                                   Client_Hier_Id  
                                 , Account_Id  
                             )  
                        SELECT  
                                    ucha.Client_Hier_Id  
                                  , ucha.Account_Id  
                        FROM        Core.Client_Hier ch  
                                    INNER JOIN  
                                    Core.Client_Hier_Account ucha  
                                          ON ucha.Client_Hier_Id = ch.Client_Hier_Id  
                        WHERE       ucha.Account_Id = @Account_Id  
                                    AND   ucha.Commodity_Id = @Commodity_Id  
                                    AND   ucha.Account_Type = 'Utility'  
                        GROUP BY    ucha.Client_Hier_Id  
                                  , ucha.Account_Id;  
  
            INSERT INTO @CHA (  
                                   Client_Hier_Id  
                                 , Account_Id  
                             )  
                        SELECT  
                                    ucha.Client_Hier_Id  
                                  , ucha.Account_Id  
                        FROM        Core.Client_Hier ch  
                                    INNER JOIN  
                                    Core.Client_Hier_Account ucha  
                                          ON ucha.Client_Hier_Id = ch.Client_Hier_Id  
                                    INNER JOIN  
                                    Core.Client_Hier_Account sa  
                                          ON sa.Meter_Id = ucha.Meter_Id  
                                             AND sa.Client_Hier_Id = ucha.Client_Hier_Id  
                        WHERE       sa.Account_Id = @Account_Id  
                                    AND   sa.Commodity_Id = @Commodity_Id  
                                    AND   sa.Account_Type = 'Supplier'  
                                    AND   ucha.Account_Type = 'Utility'  
                                    AND   NOT EXISTS  
                              (     SELECT  
                                          1  
                                    FROM  @CHA cha  
                                    WHERE cha.Client_Hier_Id = ucha.Client_Hier_Id  
                                          AND   cha.Account_Id = ucha.Account_Id )  
                        GROUP BY    ucha.Client_Hier_Id  
                                  , ucha.Account_Id;  
  
         INSERT INTO @Trade_Info (  
                                          RM_Deal_Ticket_Id  
                                        , Trade_Id  
                                        , Deal_Initiated_Dt  
                                        , Trade_Start_Dt  
                                        , Trade_End_Dt  
                                        , Trade_Type  
                                        , Hedge_Volume  
                                        , Uom_Id  
                                        , Frequency_Type  
                                        , Hedge_Price  
                                        , Currency_Unit_Id  
                                        , Time_Of_Day  
                                        , Trade_Level  
                                        , Months  
                                        , Hedge_Date  
                                        , DEAL_TICKET_NUMBER  
                                        , Client_Hier_Id  
                                        , Account_Id  
                                        , Contract_Id
										, Trade_Action_Type  
                                    )  
                        SELECT  
                              dt.Deal_Ticket_Id AS RM_DEAL_TICKET_ID  
                            , vol.Trade_Number AS Trade_Id  
                            , dt.Created_Ts AS DEAL_INITIATED_DATE  
                            , dd.FIRST_DAY_OF_MONTH_D  
                            , dd.LAST_DAY_OF_MONTH_D Trade_End_dt  
                            , 'Volume' Trade_Type  
                            , CASE frq.Code_Value  
                                   WHEN 'Daily'  
                                         THEN cast(vol.Total_Volume AS DECIMAL(20, 2))  
                                   WHEN 'Monthly'  
                                         THEN cast(vol.Total_Volume AS DECIMAL(20, 2))  
                                   ELSE  cast(vol.Total_Volume AS DECIMAL(20, 2))  
                              END AS Hedge_Volume  
                            , dt.Uom_Type_Id AS UNIT_TYPE_ID  
                            , frq.Code_Value AS ENTITY_NAME  
                            , cast(tp.Trade_Price AS DECIMAL(8, 4)) AS Hedge_Price  
                            , dt.Currency_Unit_Id AS CURRENCY_TYPE_ID  
                            , '' Time_Of_Day  
                            , 'Account' TradeLevel  
                            , dd.FIRST_DAY_OF_MONTH_D AS Month_Year  
                            , dt.Created_Ts AS Hedge_Date  
                            , dt.Deal_Ticket_Number AS DEAL_TICKET_NUMBER  
                            , vol.Client_Hier_Id  
                            , vol.Account_Id  
                            , vol.Contract_Id 
							, tActionType.Code_Value  as Trade_Action_Type
                        FROM  Trade.Deal_Ticket dt  
                              INNER JOIN  
                              Trade.Deal_Ticket_Client_Hier dtch  
                                    ON dt.Deal_Ticket_Id = dtch.Deal_Ticket_Id  
                              INNER JOIN  
                              Trade.Deal_Ticket_Client_Hier_Volume_Dtl vol  
                                    ON dtch.Client_Hier_Id = vol.Client_Hier_Id  
                                       AND dtch.Deal_Ticket_Id = vol.Deal_Ticket_Id  
                              INNER JOIN  
                              @CHA cha  
                                    ON cha.Client_Hier_Id = vol.Client_Hier_Id  
                                       AND cha.Account_Id = vol.Account_Id  
                              INNER JOIN  
                              Trade.Trade_Price tp  
                                    ON vol.Trade_Price_Id = tp.Trade_Price_Id  
                              INNER JOIN  
                              meta.DATE_DIM dd  
                                    ON dd.DATE_D = vol.Deal_Month  
                              INNER JOIN  
                              dbo.Code frq  
                                    ON dt.Deal_Ticket_Frequency_Cd = frq.Code_Id  
								INNER JOIN  
                              dbo.Code tActionType 
                                    ON dt.Trade_Action_Type_Cd = tActionType.Code_Id 
         WHERE dt.Hedge_Type_Cd = @Physical  
                              AND   tp.Trade_Price IS NOT NULL -- Only the order executed records should have price assigned.  
                              AND  
                                    (     vol.Deal_Month  
                              BETWEEN @Start_Dt AND @End_Dt  
                                          OR    dd.LAST_DAY_OF_MONTH_D  
                              BETWEEN @Start_Dt AND @End_Dt )  
                              AND   dt.Commodity_Id = @Commodity_Id;  
  
            SELECT  
                        ti.RM_Deal_Ticket_Id  
                      , ti.Trade_Id  
                      , cast(ti.Deal_Initiated_Dt AS DATE) AS Deal_Initiated_Dt  
                      , cast(ti.Trade_Start_Dt AS DATE) AS Trade_Start_Dt  
                      , cast(ti.Trade_End_Dt AS DATE) AS Trade_End_Dt  
                      , ti.Trade_Type  
                      , sum(ti.Hedge_Volume) AS Hedge_Volume  
                      , uom.ENTITY_NAME AS Uom  
                      , ti.Frequency_Type  
                      , ti.Hedge_Price  
                      , cu.CURRENCY_UNIT_NAME AS Currency_Unit_Name  
                      , ti.Time_Of_Day  
                      , ti.Trade_Level  
                      , substring(datename(MONTH, ti.Months), 1, 3) + '-' + datename(YEAR, ti.Months) AS Month_Year  
                      , cast(ti.Deal_Initiated_Dt AS DATE) AS Hedge_Date  
                      , ti.DEAL_TICKET_NUMBER  
                      , cast(sum(ti.Hedge_Volume) AS DECIMAL(20, 2)) * cast(ti.Hedge_Price AS DECIMAL(8, 4)) AS Hedge_Amount 
					  ,  Trade_Action_Type

            FROM        @Trade_Info ti  
                        INNER JOIN  
                        dbo.CURRENCY_UNIT cu  
                              ON cu.CURRENCY_UNIT_ID = ti.Currency_Unit_Id  
                        INNER JOIN  
                        dbo.ENTITY uom  
                              ON uom.ENTITY_ID = ti.Uom_Id  
            GROUP BY    ti.RM_Deal_Ticket_Id  
                      , ti.Trade_Id  
                      , cast(ti.Deal_Initiated_Dt AS DATE)  
                      , cast(ti.Trade_Start_Dt AS DATE)  
                      , cast(ti.Trade_End_Dt AS DATE)  
                      , ti.Trade_Type  
                      , uom.ENTITY_NAME  
                      , ti.Frequency_Type  
                      , ti.Hedge_Price  
                      , cu.CURRENCY_UNIT_NAME  
                      , ti.Time_Of_Day  
                      , ti.Trade_Level  
                      , substring(datename(MONTH, ti.Months), 1, 3) + '-' + datename(YEAR, ti.Months)  
                      , cast(ti.Deal_Initiated_Dt AS DATE)  
                      , ti.DEAL_TICKET_NUMBER  
                      , cast(ti.Hedge_Price AS DECIMAL(8, 4))
					  ,Trade_Action_Type;  
  
      END;  
GO
GRANT EXECUTE ON  [dbo].[Trade_Info_Sel_By_Account_Commodity_Invoice_Dt] TO [CBMSApplication]
GO
