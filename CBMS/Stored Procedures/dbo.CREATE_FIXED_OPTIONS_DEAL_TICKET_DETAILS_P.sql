SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.CREATE_FIXED_OPTIONS_DEAL_TICKET_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(16)	          	
	@dealTicketId  	int       	          	
	@hedgePrice    	decimal(32,16)	          	
	@monthIdentifier	datetime  	          	
	@triggerPrice  	decimal(32,16)	          	
	@triggerStatusType	int       	          	
	@totalVolume   	decimal(32,16)	          	
	@dealTransactionDate	datetime  	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE          PROCEDURE DBO.CREATE_FIXED_OPTIONS_DEAL_TICKET_DETAILS_P 

@userId varchar(10),
@sessionId varchar(16),
@dealTicketId int, 
@hedgePrice decimal(32,16), 
@monthIdentifier datetime, 
@triggerPrice decimal(32,16), 
@triggerStatusType int, 
@totalVolume decimal(32,16), 
@dealTransactionDate datetime

AS
begin
	set nocount on
	INSERT INTO RM_DEAL_TICKET_DETAILS 
	(RM_DEAL_TICKET_ID, HEDGE_PRICE, MONTH_IDENTIFIER, TRIGGER_PRICE, 
	TRIGGER_STATUS_TYPE_ID, TOTAL_VOLUME, DEAL_TRANSACTION_DATE)
	VALUES
	(@dealTicketId, @hedgePrice, @monthIdentifier, @triggerPrice, 
	@triggerStatusType, @totalVolume, @dealTransactionDate)
end
GO
GRANT EXECUTE ON  [dbo].[CREATE_FIXED_OPTIONS_DEAL_TICKET_DETAILS_P] TO [CBMSApplication]
GO
