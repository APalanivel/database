SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                            
 NAME: dbo.Sourcing_Dashboard_Is_Rolling_Meter_Sel_By_User_Info_Id         
                            
 DESCRIPTION:                            
   To get Supplier Account Details                           
                            
 INPUT PARAMETERS:              
                         
 Name       DataType         Default       Description            
------------------------------------------------------------------------------         
@User_Info_Id     INT    
                            
 OUTPUT PARAMETERS:              
                               
 Name                        DataType         Default       Description            
------------------------------------------------------------------------------         
                            
 USAGE EXAMPLES:                                
------------------------------------------------------------------------------         
     
EXEC dbo.Sourcing_Dashboard_Is_Rolling_Meter_Sel_By_User_Info_Id    
     @User_Info_Id = 113   
	 
	 select * from user_info  where user_info_id=113 
  
   cbms 

                           
 AUTHOR INITIALS:            
           
 Initials              Name            
------------------------------------------------------------------------------         
 NR      Narayana Reddy    
                             
 MODIFICATIONS:          
              
 Initials              Date             Modification          
------------------------------------------------------------------------------         
 NR                    2019-05-29       Created for - Add Contract.    
                           
******/
CREATE PROCEDURE [dbo].[Sourcing_Dashboard_Is_Rolling_Meter_Sel_By_User_Info_Id]
    (
        @User_Info_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        ;
        WITH cte_accounts
        AS (
               SELECT
                    con.CONTRACT_ID
                    , con.ED_CONTRACT_NUMBER
                    , con.CONTRACT_START_DATE
                    , con.CONTRACT_END_DATE
                    , meterMap.METER_ID
                    , con.COMMODITY_TYPE_ID Commodity_Id
                    , utilacc.Account_Vendor_Id Account_Vendor_Id
                    , utilacc.Account_Id
                    , ch.Client_Id
               FROM
                    Core.Client_Hier ch
                    INNER JOIN Core.Client_Hier_Account utilacc
                        ON utilacc.Client_Hier_Id = ch.Client_Hier_Id
                    INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP AS meterMap
                        ON meterMap.METER_ID = utilacc.Meter_Id
                    INNER JOIN dbo.CONTRACT AS con
                        ON con.CONTRACT_ID = meterMap.Contract_ID
               WHERE
                    utilacc.Account_Type = 'Utility'
                    AND meterMap.Is_Rolling_Meter = 1
           )
             , Cte_Accout_User
        AS (
               SELECT
                    acc.CONTRACT_ID
                    , acc.ED_CONTRACT_NUMBER
                    , acc.CONTRACT_START_DATE
                    , acc.CONTRACT_END_DATE
                    , acc.METER_ID
                    , vcam.Analyst_Id AS User_Id
                    , acc.Commodity_Id
                    , acc.Client_Id
               FROM
                    cte_accounts acc
                    LEFT JOIN dbo.VENDOR_COMMODITY_MAP vcm
                        ON acc.Account_Vendor_Id = vcm.VENDOR_ID
                           AND  acc.Commodity_Id = vcm.COMMODITY_TYPE_ID
                    LEFT JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                        ON vcm.VENDOR_COMMODITY_MAP_ID = vcam.Vendor_Commodity_Map_Id
               GROUP BY
                   acc.CONTRACT_ID
                   , acc.ED_CONTRACT_NUMBER
                   , acc.CONTRACT_START_DATE
                   , acc.CONTRACT_END_DATE
                   , acc.METER_ID
                   , vcam.Analyst_Id
                   , acc.Commodity_Id
                   , acc.Client_Id
           )
        SELECT
            ca.CONTRACT_ID
            , ca.ED_CONTRACT_NUMBER
            , CAST(ca.CONTRACT_START_DATE AS DATE) AS CONTRACT_START_DATE
            , CAST(ca.CONTRACT_END_DATE AS DATE) AS CONTRACT_END_DATE
            , LEFT(mn.METER_NUMBER, LEN(mn.METER_NUMBER) - 1) AS METER_NUMBER
            , COUNT(1) OVER () AS Cnt_Is_Rolling_Meter
            , ca.Commodity_Id
            , ca.Client_Id
        FROM
            Cte_Accout_User ca
            CROSS APPLY
        (   SELECT
                m.METER_NUMBER + '^'
            FROM
                dbo.METER m
                INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                    ON samm.METER_ID = m.METER_ID
            WHERE
                samm.Contract_ID = ca.CONTRACT_ID
                AND samm.Is_Rolling_Meter = 1
            GROUP BY
                m.METER_NUMBER
            FOR XML PATH('')) mn(METER_NUMBER)
        WHERE
            ca.User_Id = @User_Info_Id
        GROUP BY
            ca.CONTRACT_ID
            , ca.ED_CONTRACT_NUMBER
            , CAST(ca.CONTRACT_START_DATE AS DATE)
            , CAST(ca.CONTRACT_END_DATE AS DATE)
            , LEFT(mn.METER_NUMBER, LEN(mn.METER_NUMBER) - 1)
            , ca.Commodity_Id
            , ca.Client_Id;


    END;








GO
GRANT EXECUTE ON  [dbo].[Sourcing_Dashboard_Is_Rolling_Meter_Sel_By_User_Info_Id] TO [CBMSApplication]
GO
