SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO



--exec SR_RFP_GET_SWITCH_UTILITY_INFO_P 272
CREATE PROCEDURE dbo.SR_RFP_GET_SWITCH_UTILITY_INFO_P
	@RFP_ID INT
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT
		c.client_name as clientName,
		s.site_id as siteId, 	
		vwSite.site_name as siteName,	
		a.account_id as accountId, 
		a.account_number as accountNumber,	
		dbo.SR_RFP_FN_GET_RATES(rfp_account.sr_rfp_account_id) as rateName,
		dbo.SR_RFP_FN_GET_CONTRACTS(a.account_id,convert(datetime, convert( varchar(10), getdate(), 101 )), rfp.commodity_type_id) as contracts,	
		rfp_account.sr_rfp_account_id as rfpAccountId,	
		utilitySwitch.return_to_tariff_date as returnToTariffDate,
		utilitySwitch.return_to_tariff_type_id as returnToTariffTypeId, 
		utilitySwitch.is_switch_rate_estimated as isSwitchRateEstimated,
		utilitySwitch.utility_switch_supplier_date as utilitySwitchSupplierDate,
		utilitySwitch.utility_switch_supplier_type_id as utilitySwitchSupplierTypeId,
		utilitySwitch.is_switch_supplier_estimated as isSwitchSupplierEstimated,
       	utilitySwitch.change_notice_image_id as changeNoticeImageId,
		utilitySwitch.flow_verification_image_id as flowVerificationImageId,
		utilitySwitch.supplier_notice_image_id as supplierNoticeImageId,
		cbmsImage1.cbms_doc_id as changeNoticeDocId ,
		cbmsImage2.cbms_doc_id as flowVerificationDocId ,
		cbmsImage3.cbms_doc_id as supplierNoticeDocId ,
		cbmsImage1.content_type as changeNoticeContentType  ,
		cbmsImage2.content_type as flowVerificationContentType,
		cbmsImage3.content_type as supplierNoticeContentType
		, case when trvw.Tariff_Transport is null  
		   then 'Tariff'  
		   else 'Transport'  
		   end tariff_transport 		
	FROM	
		sr_rfp_account rfp_account
		JOIN account a  on a.account_id = rfp_account.account_id 	
		JOIN sr_rfp rfp on rfp.sr_rfp_id = rfp_account.sr_rfp_id
		JOIN site s on s.site_id = a.site_id 		
		JOIN vwSiteName vwSite on  vwSite.site_id = s.site_id
		JOIN division d on d.division_id = s.division_id	
		JOIN client c on c.client_id = d.client_id
		left JOIN sr_rfp_utility_switch utilitySwitch on utilitySwitch.sr_account_group_id = rfp_account.sr_rfp_account_id and utilitySwitch.is_bid_group=0
		left JOIN cbms_image cbmsImage1 on cbmsImage1.cbms_image_id = utilitySwitch.change_notice_image_id
		left JOIN cbms_image cbmsImage2 on cbmsImage2.cbms_image_id = utilitySwitch.flow_verification_image_id
		left JOIN cbms_image cbmsImage3 on cbmsImage3.cbms_image_id = utilitySwitch.supplier_notice_image_id
		left JOIN BUDGET_TRANSPORT_ACCOUNT_VW trvw(nolock) on trvw.account_id = a.account_id and trvw.commodity_type_id = rfp.commodity_type_id	
	WHERE rfp_account.sr_rfp_id = @RFP_ID
		and rfp_account.is_deleted = 0	
	ORDER BY c.client_name

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_SWITCH_UTILITY_INFO_P] TO [CBMSApplication]
GO
