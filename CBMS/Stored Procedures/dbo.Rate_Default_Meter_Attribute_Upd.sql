SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                              
Name: [dbo].[Rate_Default_Meter_Attribute_Upd]                   
                
Description:                              
        To update Data to Rate_Default_Meter_Attribute table.                              
                
 Input Parameters:                              
     Name        DataType   Default   Description                                
----------------------------------------------------------------------------------------                                
@Rate_Default_Meter_Attribute_Id      INT            
@EC_Meter_Attribute_Id       INT            
@Start_Dt          DATE            
@End_Dt           DATE            
@EC_Meter_Attribute_Value      NVARCHAR(255)            
@User_Info_Id         INT            
@Meter_Attribute_Type_Cd      INT            
@Vendor_Type_Cd         INT            
@Attribute_Value_Requirement_Option_Cd   INT            
                
 Output Parameters:                                    
    Name        DataType   Default   Description                                
----------------------------------------------------------------------------------------                                
                
 Usage Examples:                                  
----------------------------------------------------------------------------------------                                
 BEGIN TRAN                  
               
 EXEC [dbo].[Rate_Default_Meter_Attribute_Upd]               
   @Rate_Default_Meter_Attribute_Id =1                    
  ,@EC_Meter_Attribute_Id=123                  
  ,@Start_Dt ='2020-01-01'                    
  ,@End_Dt ='2020-12-01'                      
  ,@EC_Meter_Attribute_Value ='2311'                
  ,@User_Info_Id  =49                
  ,@Meter_Attribute_Type_Cd=123                   
  ,@Attribute_Value_Requirement_Option_Cd=10293                     
 SELECT * FROM Budget.Rate_Default_Meter_Attribute               
 ROLLBACK TRAN                     
             
Author Initials:                              
    Initials  Name                              
----------------------------------------------------------------------------------------                                
SC  Sreenivasulu Cheerala            
                  
 Modifications:                              
    Initials        Date  Modification                              
----------------------------------------------------------------------------------------                                
 SC   2020-01-09    Initially Created             
******/
CREATE PROCEDURE [dbo].[Rate_Default_Meter_Attribute_Upd]
    (
        @Rate_Default_Meter_Attribute_Id INT
        , @EC_Meter_Attribute_Id INT
        , @EC_Meter_Attribute_Value NVARCHAR(255) = NULL
        , @Start_Dt DATE
        , @End_Dt DATE = NULL
        , @User_Info_Id INT
        , @Meter_Attribute_Type_Cd INT
        , @Attribute_Value_Requirement_Option_Cd INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        IF EXISTS (   SELECT
                            1
                      FROM
                            Budget.Rate_Default_Meter_Attribute
                      WHERE
                            Rate_Default_Meter_Attribute_Id = @Rate_Default_Meter_Attribute_Id)
            UPDATE
                Budget.Rate_Default_Meter_Attribute
            SET
                EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id
                , Attribute_Value_Type_Cd = @Meter_Attribute_Type_Cd
                , Start_Dt = @Start_Dt
                , End_Dt = @End_Dt
                , EC_Meter_Attribute_Value = @EC_Meter_Attribute_Value
                , Attribute_Value_Requirement_Option_Cd = @Attribute_Value_Requirement_Option_Cd
                , Updated_User_Id = @User_Info_Id
                , Last_Change_Ts = GETDATE()
            WHERE
                Rate_Default_Meter_Attribute_Id = @Rate_Default_Meter_Attribute_Id;
    END;
GO
GRANT EXECUTE ON  [dbo].[Rate_Default_Meter_Attribute_Upd] TO [CBMSApplication]
GO
