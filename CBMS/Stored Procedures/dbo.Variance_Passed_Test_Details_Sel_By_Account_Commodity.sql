
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.Variance_Passed_Test_Details_Sel_By_Account_Commodity

DESCRIPTION:
	Used to select variance passed test details in review variance page for the given account, commodity and service month

INPUT PARAMETERS:
	Name			DataType	Default	Description
------------------------------------------------------------
	@Account_Id	INT
	@Commodity_Id	INT		     NULL
	@Service_Month	DATE

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Variance_Passed_Test_Details_Sel_By_Account_Commodity 158109,291,	'2011-01-01'

	SELECT TOP 10 * FROM VAriance_log WHERE Is_Failure = 0 AND closed_Dt iS NULL ORDER BY Variance_Log_ID DESC

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
    AP		Athmaram Pabbathi
    
MODIFICATIONS
	Initials	Date		   Modification
------------------------------------------------------------
	HG		03/17/2010   Created
	HG		04/08/2010   CTE added for Variance Log and self join on Variance_Log removed as it is not required.
	HG		04/16/2010   Default value (NULL) set for @Commodity_id parameter, so that this procedure can be used to get all the variance entries logged for the given account and service month.
	AP		08/18/2011   Removed COST_USAGE_ID column as part of ADLDT changes.
	AP		10/03/2011   Added table alias & column qualifiers
	AKR     2013-07-16   Modified for Detailed Variance, Added UOM_Id and UOM_Name
******/

CREATE PROCEDURE dbo.Variance_Passed_Test_Details_Sel_By_Account_Commodity
      ( 
       @Account_Id INT
      ,@Commodity_Id INT = NULL
      ,@Service_Month DATE )
AS 
BEGIN

      SET NOCOUNT ON ;

      SELECT
            VL.variance_log_id
           ,VL.Variance_Status_Cd
           ,VL.Variance_Status_Desc AS variance_status_type
           ,VL.Category_Name
           ,VL.current_value
           ,VL.Test_Description
           ,VL.baseline_value
           ,VL.low_tolerance
           ,VL.high_tolerance
           ,VL.Is_Failure
           ,VL.Execution_Dt
           ,VL.Commodity_Id
           ,VL.Is_Inclusive
           ,VL.Is_Multiple_Condition
           ,Ref_Variance_Log_Id = ISNULL(VL.Ref_Variance_Log_Id, VL.variance_log_id)
           ,VL.Closed_Dt
           ,vl.UOM_Cd UOM_Id
           ,vl.UOM_Label UOM_Name
      FROM
            dbo.Variance_Log VL
      WHERE
            ( VL.Account_ID = @Account_Id )
            AND ( @Commodity_Id IS NULL
                  OR VL.Commodity_ID = @Commodity_Id )
            AND ( VL.Service_Month = @Service_Month )
            AND ( VL.Closed_Dt IS NULL )
            AND ( VL.Is_Failure = 0 )
      GROUP BY
            VL.variance_log_id
           ,VL.Variance_Status_Cd
           ,VL.Variance_Status_Desc
           ,VL.Category_Name
           ,VL.current_value
           ,VL.Test_Description
           ,VL.baseline_value
           ,VL.low_tolerance
           ,VL.high_tolerance
           ,VL.Is_Failure
           ,VL.Execution_Dt
           ,VL.Commodity_Id
           ,VL.Is_Inclusive
           ,VL.Is_Multiple_Condition
           ,VL.Ref_Variance_Log_Id
           ,VL.Closed_Dt
           ,vl.UOM_Cd
           ,vl.UOM_Label
      ORDER BY
            VL.Execution_Dt

END ;
;
GO


GRANT EXECUTE ON  [dbo].[Variance_Passed_Test_Details_Sel_By_Account_Commodity] TO [CBMSApplication]
GO
