SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.GET_CBMS_IMAGE_ID_FOR_MIS_OUTLOOKS_OF_CEM_P
	@PUBLICATION_CATEGORY_TYPE_ID INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT MAX(DISTINCT CBMS_IMAGE_ID) AS Expr3
	FROM dbo.SSO_PUBLICATIONS
	WHERE PUBLICATION_CATEGORY_TYPE_ID = @PUBLICATION_CATEGORY_TYPE_ID

END
GO
GRANT EXECUTE ON  [dbo].[GET_CBMS_IMAGE_ID_FOR_MIS_OUTLOOKS_OF_CEM_P] TO [CBMSApplication]
GO
