SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.CHECK_DATES_ARE_VALID_P  
	@rateId INT,  
	@rateScheduleId INT,  
	@startDate DATETIME,  
	@endDate DATETIME  
AS  
BEGIN  
  
	SET NOCOUNT ON  

	SELECT rs_start_date  
	FROM dbo.rate_schedule rs  
	WHERE rs.rate_id= @rateId 
		AND rs.rate_schedule_id != @rateScheduleId  
		AND ( (rs_end_date IS NOT NULL  
			   AND ( (rs.rs_start_date <= @startDate AND rs.rs_end_date >= @startDate)
				  OR (rs.rs_start_date <= @endDate AND rs.rs_end_date >= @endDate)	
				  OR (rs.rs_start_date >= @startDate AND rs.rs_end_date <= @endDate) 
				  OR (rs.rs_start_date <= @startDate AND rs.rs_end_date >= @endDate)
				)
			)
			OR (rs_end_date IS NULL AND(rs_start_date >= @startDate AND rs_start_date <= @endDate))
		  )
  
END
GO
GRANT EXECUTE ON  [dbo].[CHECK_DATES_ARE_VALID_P] TO [CBMSApplication]
GO
