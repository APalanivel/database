SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                 
/******                        
 NAME: dbo.Site_Address_Exist            
                        
 DESCRIPTION:                        
			To check the Site Address is created for given Site ,If already exists then returns 1 else 0.                        
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@Site_Id						INT
@Is_Site_Address_Exist	        BIT			   OUTPUT             
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 
   DECLARE @Is_Site_Address_Exist BIT 
   EXEC [dbo].[Site_Address_Exist] 
      @Site_Id = 27
     ,@Is_Site_Address_Exist = @Is_Site_Address_Exist OUTPUT  
   SELECT
      @Is_Site_Address_Exist  
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2014-12-08       Created                
                       
******/       
                
CREATE PROCEDURE [dbo].[Site_Address_Exist]
      ( 
       @Site_Id INT
      ,@Is_Site_Address_Exist BIT OUTPUT )
AS 
BEGIN                
      SET NOCOUNT ON;      
      
      SET @Is_Site_Address_Exist = 0       
      
      SELECT
            @Is_Site_Address_Exist = 1
      FROM
            dbo.site
      WHERE
            PRIMARY_ADDRESS_ID IS NOT NULL
            AND SITE_ID = @Site_Id
            
                        
                       
END 
;
GO
GRANT EXECUTE ON  [dbo].[Site_Address_Exist] TO [CBMSApplication]
GO
