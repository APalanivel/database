SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE         procedure [dbo].[cbmsSsoClientInfo_GetAlternateFuelForSite]
	( @MyAccountId int
	, @site_id int
	)
AS
BEGIN


           select v.vendor_name
		, a.storage_capacity
		, a.conversion
		, e.entity_name alternate_fuel
	     from alternate_fuel a
	     join vendor v on v.vendor_id = a.vendor_id
	     join entity e on e.entity_id = a.alternate_fuel_type_id 
	    where a.site_id = @site_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoClientInfo_GetAlternateFuelForSite] TO [CBMSApplication]
GO
