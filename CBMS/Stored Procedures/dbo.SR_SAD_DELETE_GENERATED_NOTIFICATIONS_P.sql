SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- exec dbo.SR_SAD_DELETE_GENERATED_NOTIFICATIONS_P 'Expiration','Less than 30 days', 10880, 23315
 
CREATE PROCEDURE dbo.SR_SAD_DELETE_GENERATED_NOTIFICATIONS_P
	@category_type varchar(150),
	@duration_type varchar(150),
	@account_id int,
	@contract_id int
	AS
	set nocount on
	declare @category_type_id int, @duration_type_id int

	select @category_type_id = entity_id from entity(nolock) where entity_type = 1001 and entity_name = @category_type
	select @duration_type_id = entity_id from entity(nolock) where entity_type = 1047 and entity_name = @duration_type
	

	delete sr_batch_log_details
		
	from 	sr_batch_log_details log_details(nolock),
		sr_batch_log batch_log (nolock),
		entity category(nolock),
		entity duration(nolock)

	where	batch_log.sr_batch_master_log_id = (select max(master.sr_batch_master_log_id) from sr_batch_master_log master(nolock), sr_batch_log batch(nolock)
						where master.sr_batch_master_log_id = batch.sr_batch_master_log_id and batch.batch_category_type_id = @category_type_id) 
		and batch_log.batch_category_type_id = @category_type_id
		and category.entity_id = batch_log.batch_category_type_id
		and batch_log.batch_duration_type_id = @duration_type_id
		and duration.entity_id = batch_log.batch_duration_type_id
		and log_details.sr_batch_log_id = batch_log.sr_batch_log_id 
		and log_details.account_id = @account_id
		and log_details.contract_id = @contract_id
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_DELETE_GENERATED_NOTIFICATIONS_P] TO [CBMSApplication]
GO
