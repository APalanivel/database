SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[DMR_UPDATE_CHARGE_FROM_DETERMINANT](@CU_invoice_ID Int)
as

Begin Tran
Update C
  Set COMMODITY_TYPE_ID = S.Commodity_type_id,
      ubm_service_type_id = S.ubm_service_type_id,
      ubm_service_code = S.ubm_service_code
From cu_invoice_charge C
      Inner Join
     (Select  commodity_type_id, ubm_service_type_id, ubm_service_code, cu_invoice_id
        From cu_invoice_determinant
       where cu_invoice_id = @cu_invoice_id
     Group By commodity_type_id, ubm_service_type_id, ubm_service_code, cu_invoice_id) S  on S.CU_Invoice_ID = C.CU_Invoice_ID
Where C.CU_invoice_ID = @cu_invoice_id

Select *
From cu_invoice_charge
Where cu_invoice_id = @Cu_invoice_id

Commit Tran
GO
GRANT EXECUTE ON  [dbo].[DMR_UPDATE_CHARGE_FROM_DETERMINANT] TO [CBMSApplication]
GO
