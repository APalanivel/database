SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        Trade.Open_Trigger_Open_Deal_Ticket_Sel                   
                        
Description:                        
        To get site's hedge configurations  
                        
Input Parameters:                        
    Name    DataType        Default     Description                          
--------------------------------------------------------------------------------  
 @Client_Id   INT  
    @Commodity_Id  INT  
    @Hedge_Type  INT  
    @Start_Dt DATE  
    @End_Dt  DATE  
    @Contract_Id  VARCHAR(MAX)  
    @Site_Id   INT    NULL  
                        
 Output Parameters:                              
 Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
     
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
DECLARE @Trade_tvp_Hedge_Contract_Sites AS Trade.tvp_Hedge_Contract_Sites
	INSERT INTO @Trade_tvp_Hedge_Contract_Sites
	VALUES  ( 155898, 2177 )
EXEC Trade.Open_Trigger_Open_Deal_Ticket_Sel  @Client_Id = 1005,@Commodity_Id = 291  
	  ,@Start_Dt='2019-07-01',@End_Dt='2019-07-01',@Hedge_Type=586,@Uom_Id=25,@Index_Id=325,@Hedge_Mode_Type_Id=621
	  ,@Deal_Ticket_Frequency_Cd= 102744,@Trade_tvp_Hedge_Contract_Sites=@Trade_tvp_Hedge_Contract_Sites 

DECLARE @Trade_tvp_Hedge_Contract_Sites AS Trade.tvp_Hedge_Contract_Sites
		INSERT INTO @Trade_tvp_Hedge_Contract_Sites
		VALUES  ( 145427, 2372 ),( 145427, 2030 )
	EXEC Trade.Open_Trigger_Open_Deal_Ticket_Sel  @Client_Id = 1005,@Commodity_Id = 291  
	  ,@Start_Dt='2019-01-01',@End_Dt='2019-12-01',@Hedge_Type=586,@Uom_Id=25,@Index_Id=325,@Hedge_Mode_Type_Id=621
	  ,@Deal_Ticket_Frequency_Cd= 102749
	  ,@Trade_tvp_Hedge_Contract_Sites=@Trade_tvp_Hedge_Contract_Sites 
  
Author Initials:                        
    Initials    Name                        
--------------------------------------------------------------------------------  
    RR          Raghu Reddy     
                         
 Modifications:                        
    Initials	Date		Modification                        
--------------------------------------------------------------------------------  
	RR          02-11-2018	Created GRM  
                       
******/
CREATE PROCEDURE [Trade].[Open_Trigger_Open_Deal_Ticket_Sel]
    (
        @Client_Id INT
        , @Commodity_Id INT
        , @Hedge_Type INT
        , @Start_Dt DATE
        , @End_Dt DATE
        , @Contract_Id VARCHAR(MAX) = NULL
        , @Uom_Id INT = NULL
        , @Site_Str VARCHAR(MAX) = NULL
        , @Division_Id VARCHAR(MAX) = NULL
        , @RM_Group_Id VARCHAR(MAX) = NULL
        , @Hedge_Mode_Type_Id INT = NULL
        , @Index_Id INT = NULL
        , @Deal_Ticket_Frequency_Cd INT = NULL
        , @Trade_tvp_Hedge_Contract_Sites AS Trade.tvp_Hedge_Contract_Sites READONLY
    )
WITH RECOMPILE
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @RM_Group_Sites INT
            , @Hedge_Contract_Sites_Cnt INT;

        CREATE TABLE #Open_Triggers
             (
                 Service_Month DATE
             );

        CREATE TABLE #Open_Deals
             (
                 Service_Month DATE
             );

        CREATE TABLE #Financila_Deals
             (
                 Service_Month DATE
             );


        CREATE TABLE #RM_Group_Sites
             (
                 [Site_Id] INT
             );

        INSERT INTO #RM_Group_Sites
             (
                 Site_Id
             )
        SELECT
            ch.Site_Id
        FROM
            dbo.Cbms_Sitegroup_Participant rgd
            INNER JOIN dbo.Code grp
                ON grp.Code_Id = rgd.Group_Participant_Type_Cd
            INNER JOIN dbo.CONTRACT c
                ON c.CONTRACT_ID = rgd.Group_Participant_Id
            INNER JOIN Core.Client_Hier_Account chasupp
                ON chasupp.Supplier_Contract_ID = c.CONTRACT_ID
            INNER JOIN Core.Client_Hier_Account chautil
                ON chasupp.Meter_Id = chautil.Meter_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = chasupp.Client_Hier_Id
        WHERE
            (   EXISTS (   SELECT
                                1
                           FROM
                                dbo.ufn_split(@RM_Group_Id, ',') con
                           WHERE
                                rgd.Cbms_Sitegroup_Id = CAST(Segments AS INT))
                AND grp.Code_Value = 'Contract'
                AND chasupp.Account_Type = 'Supplier'
                AND chautil.Account_Type = 'Utility');

        INSERT INTO #RM_Group_Sites
             (
                 Site_Id
             )
        SELECT
            ch.Site_Id
        FROM
            dbo.Cbms_Sitegroup_Participant rgd
            INNER JOIN dbo.Code grp
                ON grp.Code_Id = rgd.Group_Participant_Type_Cd
            INNER JOIN Core.Client_Hier ch
                ON ch.Site_Id = rgd.Group_Participant_Id
        WHERE
            (   EXISTS (   SELECT
                                1
                           FROM
                                dbo.ufn_split(@RM_Group_Id, ',') con
                           WHERE
                                rgd.Cbms_Sitegroup_Id = CAST(Segments AS INT))
                AND grp.Code_Value = 'Site');

        INSERT INTO #RM_Group_Sites
             (
                 Site_Id
             )
        SELECT  CAST(Segments AS INT)FROM   dbo.ufn_split(@Site_Str, ',') con;

        INSERT INTO #RM_Group_Sites
             (
                 Site_Id
             )
        SELECT
            ch.Site_Id
        FROM
            Core.Client_Hier ch
        WHERE
            ch.Site_Id > 0
            AND (EXISTS (   SELECT
                                1
                            FROM
                                dbo.ufn_split(@Division_Id, ',') con
                            WHERE
                                ch.Sitegroup_Id = CAST(Segments AS INT)));

        SELECT  @RM_Group_Sites = NULLIF(COUNT(1), 0)FROM   #RM_Group_Sites;

        SELECT
            @Hedge_Contract_Sites_Cnt = COUNT(1)
        FROM
            @Trade_tvp_Hedge_Contract_Sites;

        INSERT INTO #Open_Triggers
             (
                 Service_Month
             )
        SELECT
            dtv.Deal_Month
        FROM
            Core.Client_Hier ch
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtv
                ON ch.Client_Hier_Id = dtv.Client_Hier_Id
            INNER JOIN Trade.Deal_Ticket dt
                ON dtv.Deal_Ticket_Id = dt.Deal_Ticket_Id
                   AND  dt.Commodity_Id = @Commodity_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dtv.Client_Hier_Id = dtch.Client_Hier_Id
                   AND  dt.Deal_Ticket_Id = dtch.Deal_Ticket_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status dtchws
                ON dtchws.Deal_Ticket_Client_Hier_Id = dtch.Deal_Ticket_Client_Hier_Id
            INNER JOIN dbo.Code c
                ON dt.Deal_Ticket_Type_Cd = c.Code_Id
            INNER JOIN Trade.Workflow_Status_Map wsm
                ON wsm.Workflow_Status_Map_Id = dtchws.Workflow_Status_Map_Id
            INNER JOIN Trade.Workflow_Status ws
                ON ws.Workflow_Status_Id = wsm.Workflow_Status_Id
        WHERE
            ch.Client_Id = @Client_Id
            AND (   dtchws.Trade_Month IS NULL
                    OR  dtv.Deal_Month = dtchws.Trade_Month)
            AND dtchws.Is_Active = 1
            AND c.Code_Value IN ( 'Downside Trigger', 'Upside Trigger' )
            AND ws.Workflow_Status_Name = 'Order Placed'
            AND (   @RM_Group_Sites IS NULL
                    OR  EXISTS (SELECT  1 FROM  #RM_Group_Sites rgs WHERE   rgs.Site_Id = ch.Site_Id))
            AND (   @Hedge_Contract_Sites_Cnt = 0
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        Core.Client_Hier chhdg
                                        INNER JOIN Core.Client_Hier_Account chautl
                                            ON chhdg.Client_Hier_Id = chautl.Client_Hier_Id
                                        INNER JOIN Core.Client_Hier_Account cha
                                            ON chautl.Meter_Id = cha.Meter_Id
                                        INNER JOIN @Trade_tvp_Hedge_Contract_Sites hcs
                                            ON cha.Supplier_Contract_ID = hcs.Contract_Id
                                               AND  chhdg.Site_Id = hcs.Site_Id
                                   WHERE
                                        chautl.Account_Type = 'Utility'
                                        AND cha.Account_Type = 'Supplier'
                                        AND cha.Client_Hier_Id = ch.Client_Hier_Id))
            AND dtv.Deal_Month BETWEEN @Start_Dt
                               AND     @End_Dt
        GROUP BY
            dtv.Deal_Month;

        INSERT INTO #Open_Deals
             (
                 Service_Month
             )
        SELECT
            dtv.Deal_Month
        FROM
            Core.Client_Hier ch
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtv
                ON ch.Client_Hier_Id = dtv.Client_Hier_Id
            INNER JOIN Trade.Deal_Ticket dt
                ON dtv.Deal_Ticket_Id = dt.Deal_Ticket_Id
                   AND  dt.Commodity_Id = @Commodity_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dtv.Client_Hier_Id = dtch.Client_Hier_Id
                   AND  dt.Deal_Ticket_Id = dtch.Deal_Ticket_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status dtchws
                ON dtchws.Deal_Ticket_Client_Hier_Id = dtch.Deal_Ticket_Client_Hier_Id
            INNER JOIN dbo.Code c
                ON dt.Deal_Ticket_Type_Cd = c.Code_Id
            INNER JOIN Trade.Workflow_Status_Map wsm
                ON wsm.Workflow_Status_Map_Id = dtchws.Workflow_Status_Map_Id
            INNER JOIN Trade.Workflow_Status ws
                ON ws.Workflow_Status_Id = wsm.Workflow_Status_Id
        WHERE
            ch.Client_Id = @Client_Id
            AND (   dtchws.Trade_Month IS NULL
                    OR  dtv.Deal_Month = dtchws.Trade_Month)
            AND dtchws.Is_Active = 1
            AND (   (   c.Code_Value IN ( 'Downside Trigger', 'Upside Trigger' )
                        AND ws.Workflow_Status_Name NOT IN ( 'Order Placed', 'DT Created', 'Expired'
                                                             , 'Marked To Cancel', 'Not Supported', 'Completed'
                                                             , 'Closed', 'Order Executed', 'Canceled' ))
                    OR  (   c.Code_Value IN ( 'Fixed Price' )
                            AND ws.Workflow_Status_Name NOT IN ( 'Canceled', 'DT Created', 'Expired'
                                                                 , 'Marked To Cancel', 'Not Supported', 'Completed'
                                                                 , 'Closed', 'Order Executed' )))
            AND (   @RM_Group_Sites IS NULL
                    OR  EXISTS (SELECT  1 FROM  #RM_Group_Sites rgs WHERE   rgs.Site_Id = ch.Site_Id))
            AND (   @Hedge_Contract_Sites_Cnt = 0
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        Core.Client_Hier chhdg
                                        INNER JOIN Core.Client_Hier_Account chautl
                                            ON chhdg.Client_Hier_Id = chautl.Client_Hier_Id
                                        INNER JOIN Core.Client_Hier_Account cha
                                            ON chautl.Meter_Id = cha.Meter_Id
                                        INNER JOIN @Trade_tvp_Hedge_Contract_Sites hcs
                                            ON cha.Supplier_Contract_ID = hcs.Contract_Id
                                               AND  chhdg.Site_Id = hcs.Site_Id
                                   WHERE
                                        chautl.Account_Type = 'Utility'
                                        AND cha.Account_Type = 'Supplier'
                                        AND cha.Client_Hier_Id = ch.Client_Hier_Id))
            AND dtv.Deal_Month BETWEEN @Start_Dt
                               AND     @End_Dt
        GROUP BY
            dtv.Deal_Month;

        INSERT INTO #Financila_Deals
             (
                 Service_Month
             )
        SELECT
            dtv.Deal_Month
        FROM
            Core.Client_Hier ch
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtv
                ON ch.Client_Hier_Id = dtv.Client_Hier_Id
            INNER JOIN Trade.Trade_Price tp
                ON tp.Trade_Price_Id = dtv.Trade_Price_Id
            INNER JOIN Trade.Deal_Ticket dt
                ON dtv.Deal_Ticket_Id = dt.Deal_Ticket_Id
                   AND  dt.Commodity_Id = @Commodity_Id
            INNER JOIN dbo.ENTITY e
                ON dt.Hedge_Type_Cd = e.ENTITY_ID
        WHERE
            tp.Trade_Price IS NOT NULL
            AND e.ENTITY_NAME = 'Financial'
            AND ch.Client_Id = @Client_Id
            AND (   @RM_Group_Sites IS NULL
                    OR  EXISTS (SELECT  1 FROM  #RM_Group_Sites rgs WHERE   rgs.Site_Id = ch.Site_Id))
            AND (   @Hedge_Contract_Sites_Cnt = 0
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        Core.Client_Hier chhdg
                                        INNER JOIN Core.Client_Hier_Account chautl
                                            ON chhdg.Client_Hier_Id = chautl.Client_Hier_Id
                                        INNER JOIN Core.Client_Hier_Account cha
                                            ON chautl.Meter_Id = cha.Meter_Id
                                        INNER JOIN @Trade_tvp_Hedge_Contract_Sites hcs
                                            ON cha.Supplier_Contract_ID = hcs.Contract_Id
                                               AND  chhdg.Site_Id = hcs.Site_Id
                                   WHERE
                                        chautl.Account_Type = 'Utility'
                                        AND cha.Account_Type = 'Supplier'
                                        AND cha.Client_Hier_Id = ch.Client_Hier_Id))
            AND dtv.Deal_Month BETWEEN @Start_Dt
                               AND     @End_Dt
        GROUP BY
            dtv.Deal_Month;


        SELECT
            dd.DATE_D AS Deal_Month
            , CASE WHEN ot.Service_Month IS NOT NULL THEN 1
                  ELSE 0
              END AS Open_Trigger
            , CASE WHEN od.Service_Month IS NOT NULL THEN 1
                  ELSE 0
              END AS Open_Deal_Ticket
            , CASE WHEN fd.Service_Month IS NOT NULL THEN 1
                  ELSE 0
              END AS Financial_DT_Exists
        FROM
            meta.DATE_DIM dd
            LEFT JOIN #Open_Triggers ot
                ON dd.DATE_D = ot.Service_Month
            LEFT JOIN #Open_Deals od
                ON od.Service_Month = dd.DATE_D
            LEFT JOIN #Financila_Deals fd
                ON fd.Service_Month = dd.DATE_D
        WHERE
            dd.DATE_D BETWEEN @Start_Dt
                      AND     @End_Dt;



    END;

GO
GRANT EXECUTE ON  [Trade].[Open_Trigger_Open_Deal_Ticket_Sel] TO [CBMSApplication]
GO
