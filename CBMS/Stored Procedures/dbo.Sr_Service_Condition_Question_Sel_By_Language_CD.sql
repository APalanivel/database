SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Service_Condition_Question_Sel_By_Language_CD]
           
DESCRIPTION:             
			To get all service condition questions, used in service condition template setup page, users will
			select required questions and maps to the defined category
			
			
INPUT PARAMETERS:            
	Name			DataType		Default		Description  
---------------------------------------------------------------------------------  
	@Language_CD	INT				NULL

OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
        EXEC dbo.Sr_Service_Condition_Question_Sel_By_Language_CD 
        EXEC dbo.Sr_Service_Condition_Question_Sel_By_Language_CD 100157
        EXEC dbo.Sr_Service_Condition_Question_Sel_By_Language_CD 100158
        
        EXEC dbo.Sr_Service_Condition_Question_Sel_By_Language_CD NULL,290
        EXEC dbo.Sr_Service_Condition_Question_Sel_By_Language_CD NULL,291
        
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-02-22	Global Sourcing - Phase3 - GCS-448 Created
******/
CREATE PROCEDURE [dbo].[Sr_Service_Condition_Question_Sel_By_Language_CD]
      ( 
       @Language_CD INT = NULL
      ,@Commodity_Id INT = NULL )
AS 
BEGIN

      SET NOCOUNT ON;
      
      SELECT
            @Language_CD = isnull(@Language_CD, cd.Code_Id)
      FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cd.Code_Value = 'en-US'
            AND cs.Codeset_Name = 'LocalizationLanguage'
            
      SELECT
            sscq.Sr_Service_Condition_Question_Id
           ,isnull(sscqlv.Question_Label_Locale_Value, sscq.Question_Label) AS Question_Label_Locale_Value
           ,sscq.Default_Sr_Service_Condition_Category_Id
      FROM
            dbo.Sr_Service_Condition_Question sscq
            LEFT JOIN dbo.Sr_Service_Condition_Question_Locale_Value sscqlv
                  ON sscq.Sr_Service_Condition_Question_Id = sscqlv.Sr_Service_Condition_Question_Id
                     AND sscqlv.Language_Cd = @Language_CD
            INNER JOIN dbo.Sr_Service_Condition_Category sscc
                  ON sscq.Default_Sr_Service_Condition_Category_Id = sscc.Sr_Service_Condition_Category_Id
            INNER JOIN dbo.Sr_Service_Condition_Question_Commodity_Map qcm
                  ON qcm.Sr_Service_Condition_Question_Id = sscq.Sr_Service_Condition_Question_Id
      WHERE
            @Commodity_Id IS NULL
            OR qcm.Commodity_Id = @Commodity_Id
      GROUP BY
            sscq.Sr_Service_Condition_Question_Id
           ,isnull(sscqlv.Question_Label_Locale_Value, sscq.Question_Label)
           ,sscq.Default_Sr_Service_Condition_Category_Id
           ,sscc.Display_Seq
      ORDER BY
            sscc.Display_Seq
                  
            
END;
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Service_Condition_Question_Sel_By_Language_CD] TO [CBMSApplication]
GO
