SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[Bulk_Process_BatchDtl_UpdateCharges_InvEvent_Ins]  
    @Batch_Id INT ,
	@User_Id INT 
AS  
    BEGIN  
  
        SET NOCOUNT ON;  
  
        BEGIN TRY  
  
            insert into dbo.cu_invoice_event                  
				 (                  
				    cu_invoice_id          
				   ,event_date          
				   ,event_by_id          
				   ,event_description                 
				 )  
			select 	 Cu_Invoice_Id,
						getdate() as event_date,
						@User_Id as event_by_id,
						'Update Bulk Excel - Charge' AS event_description 
				 from logdb.dbo.Xl_Bulk_Invoice_Process_Batch_Dtl 
				 where XL_Bulk_Invoice_Process_Batch_Id = @BATCH_ID
					and (Error_msg is null or Error_msg = '')   
			group by  Cu_Invoice_Id
							 
  
        END TRY  
        BEGIN CATCH  
             
            EXEC [dbo].[usp_RethrowError];  
  
        END CATCH;  
    END;
	
	
GO
GRANT EXECUTE ON  [dbo].[Bulk_Process_BatchDtl_UpdateCharges_InvEvent_Ins] TO [CBMSApplication]
GO
