SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE dbo.RA_GET_VENDORS_P 
@userId varchar,
@sessionId varchar

as
	set nocount on
	Select	VENDOR_ID,
		VENDOR_NAME 
	
	from 	VENDOR 
	
	
	order by VENDOR_NAME
GO
GRANT EXECUTE ON  [dbo].[RA_GET_VENDORS_P] TO [CBMSApplication]
GO
