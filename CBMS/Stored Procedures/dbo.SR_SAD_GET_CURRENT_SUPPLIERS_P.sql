SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_CURRENT_SUPPLIERS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@analystId     	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE   PROCEDURE dbo.SR_SAD_GET_CURRENT_SUPPLIERS_P
--@stateId int,
@analystId int

AS
set nocount on
select 	  v.vendor_id,
	v.vendor_name

from 	contract con,
	vendor v,	
	account a

where 	con.contract_id = a.account_id	
	and a.vendor_id = v.vendor_id

group by  v.vendor_id,	v.vendor_name
order by v.vendor_name
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_CURRENT_SUPPLIERS_P] TO [CBMSApplication]
GO
