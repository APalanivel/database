SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE [dbo].[cbmsELMarketDataDetail_GetForDateAndPricePoint]
	( @price_point_id varchar(200) = null
	, @price_point_date datetime = null
	)

AS
BEGIN

	   select price_point_detail_id
		, price_point_value	
		, price_point_date
		, price_point_id
	     from el_market_data_detail with (nolock)
	    where price_point_id = @price_point_id 
	      and price_point_date = @price_point_date 

END
GO
GRANT EXECUTE ON  [dbo].[cbmsELMarketDataDetail_GetForDateAndPricePoint] TO [CBMSApplication]
GO
