SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Sr_Rfp_Site_History_Del_By_Site_Id]  
     
DESCRIPTION: 

	To Delete RFP history associated with the given Site Id.
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Site_Id		INT

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	BEGIN TRAN
		EXEC Sr_Rfp_Site_History_Del_By_Site_Id  11284
	ROLLBACK TRAN

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
HG			Harihara Suthan G
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
HG			07/23/2010	Created
HG			08/06/2010	Missed out to specify the variable name @Sr_Rfp_Lp_Client_Approval_Id in the Delete from @Sr_Rfp_Lp_Client_Approval_List thus it is not deleting the remaining rows from the table Sr_Rfp_Lp_Client_Approval, added that variable to remove only the current entry got deleted.
*/
CREATE PROCEDURE dbo.Sr_Rfp_Site_History_Del_By_Site_Id
	 @Site_Id	INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE	 @Sr_Rfp_Lp_Send_Client_Id		INT
			,@Sr_Rfp_Site_Lp_Xml_Id			INT
			,@Sr_Rfp_Lp_Client_Approval_Id	INT

	DECLARE @Sr_Rfp_Lp_Send_Client_List TABLE(Sr_Rfp_Lp_Send_Client_Id INT PRIMARY KEY CLUSTERED)
	DECLARE @Sr_Rfp_Site_Lp_Xml_List TABLE(Sr_Rfp_Site_Lp_Xml_Id INT PRIMARY KEY CLUSTERED)
	DECLARE @Sr_Rfp_Lp_Client_Approval_List TABLE(Sr_Rfp_Lp_Client_Approval_Id INT PRIMARY KEY CLUSTERED)

	INSERT INTO @Sr_Rfp_Lp_Client_Approval_List
	(
		Sr_Rfp_Lp_Client_Approval_Id
	)
	SELECT
		Sr_Rfp_Lp_Client_Approval_Id
	FROM
		dbo.Sr_Rfp_Lp_Client_Approval
	WHERE
		SITE_ID = @Site_Id


	INSERT INTO @Sr_Rfp_Site_Lp_Xml_List
	(
		Sr_Rfp_Site_Lp_Xml_Id
	)
	SELECT
		Sr_Rfp_Site_Lp_Xml_Id
	FROM
		dbo.Sr_Rfp_Site_Lp_Xml
	WHERE
		SITE_ID = @Site_Id

	INSERT INTO @Sr_Rfp_Lp_Send_Client_List
	(
		Sr_Rfp_Lp_Send_Client_Id
	)
	SELECT
		Sr_Rfp_Lp_Send_Client_Id
	FROM
		dbo.Sr_Rfp_Lp_Send_Client sc
		JOIN @Sr_Rfp_Site_Lp_Xml_List xl
			ON xl.Sr_Rfp_Site_Lp_Xml_Id = sc.SR_RFP_SITE_LP_XML_ID


	BEGIN TRY
		BEGIN TRAN

			WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Lp_Send_Client_List)
				BEGIN

					SELECT TOP 1 @Sr_Rfp_Lp_Send_Client_Id = Sr_Rfp_Lp_Send_Client_ID FROM @Sr_Rfp_Lp_Send_Client_List

					EXEC dbo.Sr_Rfp_Lp_Send_Client_Del @Sr_Rfp_Lp_Send_Client_Id

					DELETE
						@Sr_Rfp_Lp_Send_Client_List
					WHERE
						Sr_Rfp_Lp_Send_Client_Id = @Sr_Rfp_Lp_Send_Client_ID

				END

			WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Site_Lp_Xml_List)
				BEGIN

					SELECT TOP 1 @Sr_Rfp_Site_Lp_Xml_Id = Sr_Rfp_Site_Lp_Xml_Id FROM @Sr_Rfp_Site_Lp_Xml_List

					EXEC dbo.Sr_Rfp_Site_Lp_Xml_Del @Sr_Rfp_Site_Lp_Xml_Id

					DELETE
						@Sr_Rfp_Site_Lp_Xml_List
					WHERE
						Sr_Rfp_Site_Lp_Xml_Id = @Sr_Rfp_Site_Lp_Xml_Id

				END


			WHILE EXISTS(SELECT 1 FROM @Sr_Rfp_Lp_Client_Approval_List)
				BEGIN

					SELECT TOP 1 @Sr_Rfp_Lp_Client_Approval_Id = Sr_Rfp_Lp_Client_Approval_Id FROM @Sr_Rfp_Lp_Client_Approval_List

					EXEC dbo.Sr_Rfp_Lp_Client_Approval_Del @Sr_Rfp_Lp_Client_Approval_Id

					DELETE
						@Sr_Rfp_Lp_Client_Approval_List
					WHERE
						Sr_Rfp_Lp_Client_Approval_Id = @Sr_Rfp_Lp_Client_Approval_Id

				END

		COMMIT TRAN
	END TRY
	BEGIN CATCH

	IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRAN
		END
	
		EXEC dbo.usp_RethrowError

	END CATCH

END
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Site_History_Del_By_Site_Id] TO [CBMSApplication]
GO
