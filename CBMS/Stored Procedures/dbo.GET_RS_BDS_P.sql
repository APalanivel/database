SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE dbo.GET_RS_BDS_P
AS
BEGIN

	SET NOCOUNT ON

	SELECT bd.billing_determinant_id parent_bdId, 
		bd.billing_determinant_master_id parent_masterId, 
		bdm.bd_display_id parent_displayId,   
		bdm.bd_parent_id parent_rate_id, 
		bd.bd_parent_id parent_rs_id   
	FROM dbo.billing_determinant bd INNER JOIN dbo.billing_determinant_master bdm ON  bdm.billing_determinant_master_id = bd.billing_determinant_master_id 
		INNER JOIN dbo.entity ent1 ON ent1.entity_id = bdm.bd_parent_type_id
		INNER JOIN dbo.entity ent2 ON ent2.entity_id = bdm.bd_parent_type_id			
	WHERE ent1.entity_name = 'rate'
		AND ent1.entity_type = 120
		AND ent2.entity_name = 'Rate Schedule'
		AND ent2.entity_type = 120

END
GO
GRANT EXECUTE ON  [dbo].[GET_RS_BDS_P] TO [CBMSApplication]
GO
