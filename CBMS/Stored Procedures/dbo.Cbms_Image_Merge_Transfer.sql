SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:  Cbms_Image_Merge_Transfer        
        
DESCRIPTION:        
 Merges changes from [//Change_Control/Service/CBMS/Cbms_Image_Merge_Transfer] Service to the Cbms_Image_Merge_Transfer table        
        
INPUT PARAMETERS:        
 Name					DataType			Default		Description        
-------------------------------------------------------------------------------------------------------  
 @@Message				XML								XML string of changes to the client heir table        
 ,@Conversation_Handle  UNIQUEIDENTIFER					Conversation Handle that sent the message         
        
OUTPUT PARAMETERS:        
 Name      DataType  Default Description        
------------------------------------------------------------------------------------------------------        
@Message_Batch_Count  INT    Returns Count of source Records       
        
        
USAGE EXAMPLES:        
------------------------------------------------------------------------------------------------------  
  
  This sp should be executed from SQL Server Service Broker 
        
AUTHOR INITIALS:        
 Initials	Name        
------------------------------------------------------------------------------------------------------  
 DSC		Kaushik        


MODIFICATIONS        
        
 Initials	Date			Modification        
------------------------------------------------------------        
 DSC		02/03/2014		Created        
******/        
CREATE PROCEDURE [dbo].[Cbms_Image_Merge_Transfer]
      (
       @Message XML
      ,@Conversation_Handle UNIQUEIDENTIFIER )
AS
BEGIN        
        
      SET NOCOUNT ON;        
               
      DECLARE
            @Idoc INT
           ,@Op_Code CHAR(1)

      EXEC sp_xml_preparedocument
            @idoc OUTPUT
           ,@Message        
                         

      DECLARE @Cbms_Image_Changes TABLE
            (
             CBMS_IMAGE_ID INT NOT NULL
            ,CBMS_IMAGE_TYPE_ID INT
            ,CBMS_DOC_ID VARCHAR(200)
            ,DATE_IMAGED DATETIME
            ,BILLING_DAYS_ADJUSTMENT SMALLINT
            ,CBMS_IMAGE_SIZE DECIMAL(18, 0)
            ,CONTENT_TYPE VARCHAR(200)
            ,INV_SOURCED_IMAGE_ID INT
            ,is_reported BIT
            ,Cbms_Image_Path VARCHAR(255)
            ,App_ConfigID INT
            ,CBMS_Image_Directory VARCHAR(255)
            ,CBMS_Image_FileName VARCHAR(255)
            ,CBMS_Image_Location_Id INT
			,Last_Change_TS DATETIME
            ,[Op_Code] CHAR(1) )

      INSERT      INTO @Cbms_Image_Changes
                  ( CBMS_IMAGE_ID
                  ,CBMS_IMAGE_TYPE_ID
                  ,CBMS_DOC_ID
                  ,DATE_IMAGED
                  ,BILLING_DAYS_ADJUSTMENT
                  ,CBMS_IMAGE_SIZE
                  ,CONTENT_TYPE
                  ,INV_SOURCED_IMAGE_ID
                  ,is_reported
                  ,Cbms_Image_Path
                  ,App_ConfigID
                  ,CBMS_Image_Directory
                  ,CBMS_Image_FileName
                  ,CBMS_Image_Location_Id
				  ,Last_Change_Ts
                  ,Op_Code )
                  SELECT
                        CBMS_IMAGE_ID
                       ,CBMS_IMAGE_TYPE_ID
                       ,CBMS_DOC_ID
                       ,DATE_IMAGED
                       ,BILLING_DAYS_ADJUSTMENT
                       ,CBMS_IMAGE_SIZE
                       ,CONTENT_TYPE
                       ,INV_SOURCED_IMAGE_ID
                       ,is_reported
                       ,Cbms_Image_Path
                       ,App_ConfigID
                       ,CBMS_Image_Directory
                       ,CBMS_Image_FileName
                       ,CBMS_Image_Location_Id
					   ,Last_Change_Ts
                       ,Op_Code
                  FROM
                        openxml (@idoc, '/Cbms_Image_Changes/Cbms_Image_Change',2)        
        WITH ( 
			 CBMS_IMAGE_ID INT 
            ,CBMS_IMAGE_TYPE_ID INT
            ,CBMS_DOC_ID VARCHAR(200)
			,DATE_IMAGED DATETIME
            ,BILLING_DAYS_ADJUSTMENT SMALLINT 
            ,CBMS_IMAGE_SIZE DECIMAL(18,0)
            ,CONTENT_TYPE VARCHAR(200)
            ,INV_SOURCED_IMAGE_ID INT
            ,is_reported BIT
            ,Cbms_Image_Path VARCHAR(255)
            ,App_ConfigID INT
            ,CBMS_Image_Directory VARCHAR(255)
            ,CBMS_Image_FileName VARCHAR(255)
            ,CBMS_Image_Location_Id INT
			,Last_Change_Ts DATETIME
            ,Op_Code CHAR(1) 
          )   

       
      EXEC sp_xml_removedocument
            @idoc 


      IF EXISTS ( SELECT
                        1
                  FROM
                        @Cbms_Image_Changes
                  WHERE
                        op_code = 'I' )
            BEGIN

                  DECLARE @HoldIdent INT  
  
                  SELECT
                        @HoldIdent = ident_current('Cbms_Image')  



                  SET IDENTITY_INSERT dbo.Cbms_Image ON

                  INSERT      INTO dbo.Cbms_Image
                              (CBMS_IMAGE_ID
                              ,CBMS_IMAGE_TYPE_ID
                              ,CBMS_DOC_ID
                              ,DATE_IMAGED
                              ,BILLING_DAYS_ADJUSTMENT
                              ,CBMS_IMAGE_SIZE
                              ,CONTENT_TYPE
                              ,INV_SOURCED_IMAGE_ID
                              ,is_reported
                              ,Cbms_Image_Path
                              ,App_ConfigID
                              ,CBMS_Image_Directory
                              ,CBMS_Image_FileName
                              ,CBMS_Image_Location_Id
							  ,Last_Change_Ts
							   )
                              SELECT
                                    CBMS_IMAGE_ID
                                   ,CBMS_IMAGE_TYPE_ID
                                   ,CBMS_DOC_ID
                                   ,DATE_IMAGED
                                   ,BILLING_DAYS_ADJUSTMENT
                                   ,CBMS_IMAGE_SIZE
                                   ,CONTENT_TYPE
                                   ,INV_SOURCED_IMAGE_ID
                                   ,is_reported
                                   ,Cbms_Image_Path
                                   ,App_ConfigID
                                   ,CBMS_Image_Directory
                                   ,CBMS_Image_FileName
                                   ,CBMS_Image_Location_Id
								   ,Last_Change_Ts
                              FROM
                                    @Cbms_Image_Changes upc
                              WHERE
                                    Op_Code = 'I'


                  SET IDENTITY_INSERT dbo.Cbms_Image OFF

                  DBCC CHECKIDENT ('Cbms_Image', RESEED, @holdident)  WITH NO_INFOMSGS;

            END 


      MERGE dbo.Cbms_Image tgt
      USING
            ( SELECT
                  CBMS_IMAGE_ID
                 ,CBMS_IMAGE_TYPE_ID
                 ,CBMS_DOC_ID
                 ,DATE_IMAGED
                 ,BILLING_DAYS_ADJUSTMENT
                 ,CBMS_IMAGE_SIZE
                 ,CONTENT_TYPE
                 ,INV_SOURCED_IMAGE_ID
                 ,is_reported
                 ,Cbms_Image_Path
                 ,App_ConfigID
                 ,CBMS_Image_Directory
                 ,CBMS_Image_FileName
                 ,CBMS_Image_Location_Id
				 ,Last_Change_Ts
                 ,Op_Code
              FROM
                  @Cbms_Image_Changes upc
              WHERE
                  upc.Op_Code != 'I' ) src
      ON src.CBMS_IMAGE_ID = tgt.CBMS_IMAGE_ID
      WHEN MATCHED AND src.Op_Code = 'U' AND src.Last_Change_Ts >= tgt.Last_Change_Ts
      THEN UPDATE SET
                    tgt.CBMS_IMAGE_TYPE_ID = src.CBMS_IMAGE_TYPE_ID
                   ,tgt.CBMS_DOC_ID = src.CBMS_DOC_ID
                   ,tgt.DATE_IMAGED = src.DATE_IMAGED
                   ,tgt.BILLING_DAYS_ADJUSTMENT = src.BILLING_DAYS_ADJUSTMENT
				   ,tgt.CBMS_IMAGE_SIZE = src.CBMS_IMAGE_SIZE
                   ,tgt.CONTENT_TYPE = src.CONTENT_TYPE
                   ,tgt.INV_SOURCED_IMAGE_ID = src.INV_SOURCED_IMAGE_ID
                   ,tgt.is_reported = src.is_reported
                   ,tgt.Cbms_Image_Path = src.Cbms_Image_Path
                   ,tgt.App_ConfigID = src.App_ConfigID
                   ,tgt.CBMS_Image_Directory = src.CBMS_Image_Directory
                   ,tgt.CBMS_Image_FileName = src.CBMS_Image_FileName
                   ,tgt.CBMS_Image_Location_Id = src.CBMS_Image_Location_Id
				   ,tgt.Last_Change_Ts = src.Last_Change_Ts
      WHEN MATCHED AND src.Op_Code = 'D' THEN
            DELETE;
				
	
END



;
GO
GRANT EXECUTE ON  [dbo].[Cbms_Image_Merge_Transfer] TO [sb_Execute]
GO
