SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [DBO].[Recalc_Header_Del]  

DESCRIPTION: It Deletes Recalc Header for Given Recalc Header Id.   
And also This sproc should not be deleting the records from Cu_Invoice_Recalc_Response if Is_Locked is true OR Is_Send_To_Sys is true  
      
INPUT PARAMETERS:          
	NAME				DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Recalc_Header_Id	INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  BEGIN TRAN
	SELECT * FROM dbo.Cu_Invoice_Recalc_Response cirr  WHERE Recalc_Header_Id = 1456708
	SELECT * FROM dbo.RECALC_HEADER rh WHERE RECALC_HEADER_ID = 1456708

        EXEC Recalc_Header_Del 
            @Recalc_Header_Id = 1456708
    SELECT * FROM dbo.Cu_Invoice_Recalc_Response cirr  WHERE Recalc_Header_Id = 1456708
	SELECT * FROM dbo.RECALC_HEADER rh WHERE RECALC_HEADER_ID = 1456708

  ROLLBACK TRAN
  
  
    BEGIN TRAN
	SELECT * FROM dbo.Cu_Invoice_Recalc_Response cirr  WHERE Recalc_Header_Id = 1456998
	SELECT * FROM dbo.RECALC_HEADER rh WHERE RECALC_HEADER_ID = 1456998

        EXEC Recalc_Header_Del 
            @Recalc_Header_Id = 1456998
    SELECT * FROM dbo.Cu_Invoice_Recalc_Response cirr  WHERE Recalc_Header_Id = 1456998
	SELECT * FROM dbo.RECALC_HEADER rh WHERE RECALC_HEADER_ID = 1456998

  ROLLBACK TRAN

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH
	RKV			Ravi Kumar Vegesna
	PR			Pradip

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			18-JUN-10	CREATED.
	RKV         2015-10-29  Added Table Cu_Invoice_Recalc_Response as part of AS400-PII
	NR			2017-10-18	SE2017-263 - Add Filter to should not be deleting the records from Cu_Invoice_Recalc_Response if Is_Locked=1
	PR			2018-03-20	Recal Data Interval - Removed Data From Cu_Invoice_Recalc_Determinant.
*/ 
CREATE PROCEDURE [dbo].[Recalc_Header_Del]
      ( 
       @Recalc_Header_Id INT )
AS 
BEGIN  
  
        
      BEGIN TRY  
            SET NOCOUNT ON;  
                             
    
            DELETE
                  cr
            FROM
                  dbo.Cu_Invoice_Recalc_Response cr
            WHERE
                  cr.Recalc_Header_Id = @Recalc_Header_Id
                  AND NOT EXISTS ( SELECT
                                    1
                                   FROM
                                    Cu_Invoice_Recalc_Response cirr
                                   WHERE
                                    cr.Cu_Invoice_Recalc_Response_Id = cirr.Cu_Invoice_Recalc_Response_Id
                                    AND cr.Recalc_Header_Id = cirr.Recalc_Header_Id
                                    AND cirr.Is_Locked = 1 )  
                                    
            DELETE
                  cird
            FROM
                  dbo.Cu_Invoice_Recalc_Determinant cird
            WHERE
                  cird.Recalc_Header_Id = @Recalc_Header_Id  
                 
  
            DELETE
                  rh
            FROM
                  dbo.RECALC_HEADER rh
            WHERE
                  rh.RECALC_HEADER_ID = @Recalc_Header_Id
                  AND NOT EXISTS ( SELECT
                                    1
                                   FROM
                                    Cu_Invoice_Recalc_Response cir
                                   WHERE
                                    rh.RECALC_HEADER_ID = cir.Recalc_Header_Id
                                    AND cir.Is_Locked = 1 )  
             
                  
                    
  
      END TRY  
      BEGIN CATCH  
  

            EXEC dbo.usp_RethrowError  
  
      END CATCH  
  
END;  
  

GO
GRANT EXECUTE ON  [dbo].[Recalc_Header_Del] TO [CBMSApplication]
GO
