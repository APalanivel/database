SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--exec  GET_ACTUAL_SITES_UNDER_DIVISION_P '-1','-1',62



CREATE PROCEDURE dbo.GET_ACTUAL_SITES_UNDER_DIVISION_P 
@userId varchar,
@sessionId varchar,
@divisionId int
as
select	site.SITE_ID,
	site.SITE_NAME

from	SITE site,

	DIVISION division 


where 	division.DIVISION_ID=@divisionId AND
	site.DIVISION_ID=division.DIVISION_ID 
order by site.SITE_NAME
GO
GRANT EXECUTE ON  [dbo].[GET_ACTUAL_SITES_UNDER_DIVISION_P] TO [CBMSApplication]
GO
