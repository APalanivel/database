
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.BUDGET_UPDATE_BUDGET_ACCOUNTS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@user_id       	varchar(10)	          	
	@session_id    	varchar(20)	          	
	@accountId     	int       	          	
	@budgetId      	int       	          	
	@is_reforecast 	bit       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	-- select * from budget_account where budget_id = 255


	-- select * from budget_account where budget_id = 290
	-- delete from budget_account where budget_account_id = 256
	-- exec dbo.BUDGET_UPDATE_BUDGET_ACCOUNTS_P -1,-1,33376,290



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	RR			2017-06-01	SE2017-26 - Modified to call script dbo.Budget_Account_Queue_Ins_Upd to add/update budget accounts to queue							

******/

CREATE    PROCEDURE [dbo].[BUDGET_UPDATE_BUDGET_ACCOUNTS_P]
      ( 
       @user_id VARCHAR(10)
      ,@session_id VARCHAR(20)
      ,@accountId INT
      ,@budgetId INT
      ,@is_reforecast BIT )
AS 
BEGIN
      SET NOCOUNT ON;
      
      DECLARE
            @cnt INT
           ,@Budget_Account_Id INT
      SELECT
            @cnt = COUNT(budget_account_id)
      FROM
            budget_account
      WHERE
            budget_account.budget_id = @budgetId
            AND budget_account.account_id = @accountId
	
      IF ( @cnt > 0 ) 
            BEGIN
	
                  UPDATE
                        BUDGET_ACCOUNT
                  SET   
                        BUDGET_ACCOUNT.account_id = @accountId
                       ,is_deleted = 0
                  WHERE
                        BUDGET_ACCOUNT.account_id = @accountId
                        AND BUDGET_ACCOUNT.budget_id = @budgetId
	      
							
            END
      ELSE 
            BEGIN

                  INSERT      INTO BUDGET_ACCOUNT
                              ( 
                               ACCOUNT_ID
                              ,BUDGET_ID
                              ,is_deleted
                              ,is_reforecast )
                  VALUES
                              ( 
                               @accountId
                              ,@budgetId
                              ,0
                              ,@is_reforecast )
	
				
            END
            
      SELECT
            @Budget_Account_Id = BUDGET_ACCOUNT_ID
      FROM
            dbo.BUDGET_ACCOUNT
      WHERE
            ACCOUNT_ID = @accountId
            AND BUDGET_ID = @budgetId
			
      EXEC dbo.Budget_Account_Queue_Ins_Upd 
            @Budget_Id = @budgetId
           ,@Budget_Account_Id = @Budget_Account_Id
			

END


;
GO

GRANT EXECUTE ON  [dbo].[BUDGET_UPDATE_BUDGET_ACCOUNTS_P] TO [CBMSApplication]
GO
