
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 [dbo].[Recalc_Type_Sel_By_Service_Month_Commodity_Account_Id]
  
DESCRIPTION:  
  
 Return the details of current Recalc Type for the commodity Based on Account_Id   
   
INPUT PARAMETERS:  
 Name				DataType			Default				Description  
------------------------------------------------------------------------------  
@Service_Month		DATE
@Account_Id			INT
@Commodity_ID		INT  
OUTPUT PARAMETERS:  
 Name				DataType			Default				Description  
------------------------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------------------------  
  select * from cu_invoice_service_month where cu_invoice_id = 35234563
  
 EXEC dbo.Recalc_Type_Sel_By_Service_Month_Commodity_Account_Id 
      @Service_Month = '2015-06-01'
     ,@Account_Id = 732549
     ,@Commodity_ID = 290  
 
 
AUTHOR INITIALS:  
 Initials		Name  
------------------------------------------------------------------------------  
 RKV			RAVI KUMAR VEGESNA  
  
MODIFICATIONS  
  
 Initials		Date				Modification  
------------------------------------------------------------------------------  
 RKV			2015-11-05			Created as Part of AS400-Phase-11 . 
   
******/  
  
CREATE PROCEDURE [dbo].[Recalc_Type_Sel_By_Service_Month_Commodity_Account_Id]
      ( 
       @Service_Month DATE
      ,@Account_Id INT
      ,@Commodity_ID INT )
AS 
BEGIN  
  
      SET NOCOUNT ON  
  
      SELECT
            acirt.Account_Id
           ,acirt.Commodity_ID
           ,c.Commodity_Name
           ,acirt.Invoice_Recalc_Type_Cd
           ,cd.Code_Value AS Recalc_Type
      FROM
            dbo.Account_Commodity_Invoice_Recalc_Type acirt
            INNER JOIN dbo.Code cd
                  ON acirt.Invoice_Recalc_Type_Cd = cd.Code_Id
            INNER JOIN dbo.Commodity c
                  ON acirt.Commodity_ID = c.Commodity_Id
      WHERE
            acirt.Account_Id = @Account_Id
            AND acirt.Commodity_ID = @Commodity_ID
            AND @Service_Month BETWEEN Start_Dt
                               AND     ISNULL(End_Dt, '2099-01-01')                                          
  
END 



;
;
GO


GRANT EXECUTE ON  [dbo].[Recalc_Type_Sel_By_Service_Month_Commodity_Account_Id] TO [CBMSApplication]
GO
