SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE  PROCEDURE [dbo].[cbmsInvProKarmaNotReportedBatch_Get]
	( @MyAccountId int )
AS
BEGIN

	declare @this_id int

	set nocount on

	insert into inv_prokarma_not_reported_batch ( batch_date ) values ( getdate() )

	set @this_id = @@IDENTITY

	set nocount off

	   select inv_prokarma_not_reported_batch_id
		, batch_date
	     from inv_prokarma_not_reported_batch
	    where inv_prokarma_not_reported_batch_id = @this_id


END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvProKarmaNotReportedBatch_Get] TO [CBMSApplication]
GO
