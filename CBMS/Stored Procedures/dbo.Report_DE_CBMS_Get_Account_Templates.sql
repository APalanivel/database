
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*****  
          
NAME: Report_DE_CBMS_Get_Account_Templates        
      
DESCRIPTION:          
USED BY DTS PACKAGE ACCOUNTTEMPLATES TO GENERATE COMMAND DELIMITED FILE THAT IS SEND TO PROKARM          
      
INPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
                     
  
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:
------------------------------------------------------------  
EXEC Report_DE_CBMS_Get_Account_Templates        

AUTHOR INITIALS:
 Initials		Name  
------------------------------------------------------------
 AKR			Ashok Kumar Raju
 RKV			Ravi Kumar Vegesna

MODIFICATIONS:  
 Initials		Date		Modification  
------------------------------------------------------------  
DMR				4/14/2009   ADDED ACCOUNT_ID TO THE SELECT STATEMENT          
SSR				03/18/2010	Removed Cu_invoice_Account_map(Account_id) with Cu_invoice_service_month(account_ID)       
SSR				03/23/2010	Removed View vwsitename      
							Removed Nolock and added table Owner Name (Partaily Modified)      
SSR				04/22/2010	Removed Distinct and added group by Clause      
SKA				05/28/2010	Added the TODO statement      
PD				11/02/2010	Added a WHERE clause to filter out AT&T data
TODO:						
 This needs to be fine tuned, duplication of tables removed, views removed.      
AKR				2012-05-01  Cloned from CBMSGETACCOUNTTEMPLATES. Mdofied to use Client_Hier Tables 
RKV				2014-01-07  Maint-2357 Added Meter_Number Column  
HG				2014-08-01	MAINT-2975, modified to match with the logic in stored procedure Report_DE_ProkarmaTemplates
******/  
  
CREATE PROCEDURE [dbo].[Report_DE_CBMS_Get_Account_Templates]
AS 
BEGIN          
      
      SET NOCOUNT ON;      
      
      DECLARE @INVOICE TABLE
            ( 
             ACCOUNT_ID INT NOT NULL
            ,CU_INVOICE_ID INT NOT NULL )          
      
      INSERT      INTO @INVOICE
                  ( 
                   ACCOUNT_ID
                  ,CU_INVOICE_ID )
                  SELECT
                        sm.account_id
                       ,cha.Template_Cu_Invoice_Id AS cu_invoice_id
                  FROM
                        dbo.cu_invoice cu
                        INNER JOIN dbo.cu_invoice_service_month sm
                              ON sm.cu_invoice_id = cu.cu_invoice_id
                        INNER JOIN Core.Client_Hier_Account cha
                              ON sm.Account_ID = cha.Account_Id
                                 AND cha.Template_Cu_Invoice_Id = cu.CU_INVOICE_ID
                        INNER JOIN dbo.CU_INVOICE_CHARGE D
                              ON D.CU_INVOICE_ID = CU.CU_INVOICE_ID
                  WHERE
                        cu.is_processed = 1
                        AND cu.IS_DNT = 0
                        AND cu.IS_DUPLICATE = 0
                        AND cha.Meter_Country_Name NOT IN ( 'Belgium', 'Netherlands', 'Luxembourg' )
                        AND CHa.account_not_managed = 0
                        AND D.CHARGE_VALUE IS NOT NULL
                  GROUP BY
                        sm.account_id
                       ,cha.Template_Cu_Invoice_Id
                      
      
      SELECT
            ch.CLIENT_NAME
           ,RTRIM(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' Site_Name
           ,CASE WHEN cha.Account_Type = 'Utility' THEN cha.Account_Number
                 ELSE ISNULL(cha.Account_Number, 'Blank for ' + cha.Account_Vendor_Name) + ' (' + CONVERT(VARCHAR, cha.Supplier_Account_Begin_Dt, 101) + '-' + CONVERT(VARCHAR, cha.Supplier_Account_End_Dt, 101) + ')'
            END AS Account_Number
           ,cha.Account_Vendor_Name AS Vendor_Name
           ,cha.Meter_Number
           ,X.ACCOUNT_ID
           ,X.CU_INVOICE_ID
           ,SM.SERVICE_MONTH
      FROM
            core.client_hier ch
            INNER JOIN core.Client_Hier_Account cha
                  ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN @INVOICE X
                  ON X.ACCOUNT_ID = cha.ACCOUNT_ID
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH SM
                  ON SM.CU_INVOICE_ID = X.CU_INVOICE_ID
      WHERE
            ( CH.Client_Name <> 'AT&T Services, Inc.'
              OR ( CH.Client_Name = 'AT&T Services, Inc.'
                   AND CH.Country_Name <> 'USA' ) )
            AND CH.Client_Not_Managed = 0
      GROUP BY
            ch.CLIENT_NAME
           ,ch.city
           ,ch.state_name
           ,ch.site_name
           ,CASE WHEN cha.Account_Type = 'Utility' THEN cha.Account_Number
                 ELSE ISNULL(cha.Account_Number, 'Blank for ' + cha.Account_Vendor_Name) + ' (' + CONVERT(VARCHAR, cha.Supplier_Account_Begin_Dt, 101) + '-' + CONVERT(VARCHAR, cha.Supplier_Account_End_Dt, 101) + ')'
            END
           ,cha.Account_Vendor_Name
           ,cha.Meter_Number
           ,X.ACCOUNT_ID
           ,X.CU_INVOICE_ID
           ,SM.SERVICE_MONTH           
   
END
;
GO


GRANT EXECUTE ON  [dbo].[Report_DE_CBMS_Get_Account_Templates] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_CBMS_Get_Account_Templates] TO [CBMSApplication]
GO
