SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE  procedure dbo.cbmsRmMarketOutlook_Get

	(@AccountId int
	, @OutlookId int
	)

as
begin
	select o.rm_market_outlook_id
		, o.rm_market_group_id
		, g.rm_market_group
		, o.interval_type_id
		, i.entity_name interval_type
		--, o.cbms_image_id
		--, o.overview
		, o.pub_date
		--, o.rm_market_outlook_title
	from rm_market_outlook o
	join entity i with (nolock) on i.entity_id = o.interval_type_id
 	join rm_market_group g with (nolock) on g.rm_market_group_id = o.rm_market_group_id
	where o.rm_market_outlook_id = @OutlookId
end



GO
GRANT EXECUTE ON  [dbo].[cbmsRmMarketOutlook_Get] TO [CBMSApplication]
GO
