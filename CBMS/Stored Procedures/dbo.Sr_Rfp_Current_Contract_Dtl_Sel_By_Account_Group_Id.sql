
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Rfp_Current_Contract_Dtl_Sel_By_Account_Group_Id]
           
DESCRIPTION:             
			To get the current contract associated to the account/group
			
			
INPUT PARAMETERS:            
	Name					DataType		Default		Description  
---------------------------------------------------------------------------------  
	@Sr_Account_Group_Id	INT
    @Is_Bid_Group			INT
    @Sr_Rfp_Id				INT

OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
        EXEC dbo.Sr_Rfp_Current_Contract_Dtl_Sel_By_Account_Group_Id 12104540,0,13244
        EXEC dbo.Sr_Rfp_Current_Contract_Dtl_Sel_By_Account_Group_Id 12104889,0,13244
        
        EXEC dbo.Sr_Rfp_Current_Contract_Dtl_Sel_By_Account_Group_Id 12120000,0,14537
        EXEC dbo.Sr_Rfp_Current_Contract_Dtl_Sel_By_Account_Group_Id 10013359,1,14537
        
        EXEC dbo.Sr_Rfp_Current_Contract_Dtl_Sel_By_Account_Group_Id 10012631,1,13781
		EXEC dbo.SR_RFP_GET_RFP_CHECKLIST_DATA_P 13781,'meter_numbers',-1 
		SELECT dbo.SR_RFP_FN_GET_CONTRACT_DETAILS_FOR_CHECKLIST(132296,'2017-10-06',291)

        
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-04-29	Created
	RR			2017-10-06	MAINT-5846 Modified to return active contracts only, same as checklist page, applied filter on CONTRACT_END_DATE 
				2017-10-12	MAINT-6087 Deleted accounts should not consider to get contracts, added IS_DELETED filter
******/
CREATE PROCEDURE [dbo].[Sr_Rfp_Current_Contract_Dtl_Sel_By_Account_Group_Id]
      ( 
       @Sr_Account_Group_Id INT
      ,@Is_Bid_Group INT
      ,@Sr_Rfp_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Commodity_Id INT
      DECLARE @Current_Date DATETIME = CONVERT(DATETIME, CONVERT(VARCHAR(10), GETDATE(), 101))
      DECLARE @MAX_CONTRACT_END_DATE DATETIME
      
      SELECT
            @Commodity_Id = COMMODITY_TYPE_ID
      FROM
            dbo.SR_RFP
      WHERE
            SR_RFP_ID = @Sr_Rfp_Id
            
      DECLARE @Contracts AS TABLE
            ( 
             CONTRACT_ID INT
            ,CONTRACT_START_DATE DATETIME
            ,CONTRACT_END_DATE DATETIME
            ,VENDOR_NAME VARCHAR(200)
            ,CONTRACT_PRICING_SUMMARY VARCHAR(1000) );
      INSERT      INTO @Contracts
                  ( 
                   CONTRACT_ID
                  ,CONTRACT_START_DATE
                  ,CONTRACT_END_DATE
                  ,VENDOR_NAME
                  ,CONTRACT_PRICING_SUMMARY )
                  SELECT
                        con.CONTRACT_ID
                       ,con.CONTRACT_START_DATE
                       ,con.CONTRACT_END_DATE
                       ,ven.VENDOR_NAME
                       ,con.CONTRACT_PRICING_SUMMARY
                  FROM
                        dbo.METER met
                        INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP map
                              ON map.meter_id = met.meter_id
                        INNER JOIN dbo.ACCOUNT suppacc
                              ON map.account_id = suppacc.account_id
                        INNER JOIN dbo.CONTRACT con
                              ON con.contract_id = map.contract_id
                        INNER JOIN dbo.VENDOR ven
                              ON suppacc.vendor_id = ven.vendor_id
                        INNER JOIN dbo.ENTITY typ
                              ON typ.ENTITY_ID = con.CONTRACT_TYPE_ID
                        INNER JOIN dbo.SR_RFP_ACCOUNT rfpacc
                              ON met.ACCOUNT_ID = rfpacc.ACCOUNT_ID
                  WHERE
                        con.commodity_type_id = @Commodity_Id
                        AND rfpacc.SR_RFP_ID = @Sr_Rfp_Id
                        AND typ.ENTITY_NAME = 'Supplier'
                        AND typ.ENTITY_DESCRIPTION = 'Contract Type'
                        AND ( ( @Is_Bid_Group = 0
                                AND rfpacc.SR_RFP_BID_GROUP_ID IS NULL
                                AND rfpacc.SR_RFP_ACCOUNT_ID = @Sr_Account_Group_Id )
                              OR ( @Is_Bid_Group = 1
                                   AND rfpacc.SR_RFP_BID_GROUP_ID IS NOT NULL
                                   AND rfpacc.SR_RFP_BID_GROUP_ID = @Sr_Account_Group_Id ) )
                        AND con.CONTRACT_END_DATE > @Current_Date
                        AND rfpacc.IS_DELETED = 0
                  GROUP BY
                        con.CONTRACT_ID
                       ,con.CONTRACT_START_DATE
                       ,con.CONTRACT_END_DATE
                       ,ven.VENDOR_NAME
                       ,con.CONTRACT_PRICING_SUMMARY
                       
      SELECT
            @MAX_CONTRACT_END_DATE = MAX(CONTRACT_END_DATE)
      FROM
            @Contracts
            
      DELETE FROM
            @Contracts
      WHERE
            CONTRACT_END_DATE <> @MAX_CONTRACT_END_DATE
             
      SELECT
            REPLACE(LEFT(vndr.vndrs, LEN(vndr.vndrs) - 1), '&amp;', '&') AS VENDOR_NAME
           ,REPLACE(LEFT(cps.cpss, LEN(cps.cpss) - 1), '&amp;', '&') AS CONTRACT_PRICING_SUMMARY
           ,REPLACE(LEFT(stdt.stdts, LEN(stdt.stdts) - 1), '&amp;', '&') AS CONTRACT_START_DATE
           ,REPLACE(LEFT(endt.endts, LEN(endt.endts) - 1), '&amp;', '&') AS CONTRACT_END_DATE
      FROM
            @Contracts ccv
            CROSS APPLY ( SELECT
                              vndr.VENDOR_NAME + ', '
                          FROM
                              @Contracts vndr
                          GROUP BY
                              vndr.VENDOR_NAME
            FOR
                          XML PATH('') ) vndr ( vndrs )
            CROSS APPLY ( SELECT
                              cps.CONTRACT_PRICING_SUMMARY + ', '
                          FROM
                              @Contracts cps
                          GROUP BY
                              cps.CONTRACT_PRICING_SUMMARY
            FOR
                          XML PATH('') ) cps ( cpss )
            CROSS APPLY ( SELECT
                              REPLACE(CONVERT(VARCHAR(11), stdt.CONTRACT_START_DATE, 106), ' ', '-') + ', '
                          FROM
                              @Contracts stdt
                          GROUP BY
                              stdt.CONTRACT_START_DATE
            FOR
                          XML PATH('') ) stdt ( stdts )
            CROSS APPLY ( SELECT
                              REPLACE(CONVERT(VARCHAR(11), endt.CONTRACT_END_DATE, 106), ' ', '-') + ', '
                          FROM
                              @Contracts endt
                          GROUP BY
                              endt.CONTRACT_END_DATE
            FOR
                          XML PATH('') ) endt ( endts )
      
                  
                  
            
END;


;
GO

GRANT EXECUTE ON  [dbo].[Sr_Rfp_Current_Contract_Dtl_Sel_By_Account_Group_Id] TO [CBMSApplication]
GO
