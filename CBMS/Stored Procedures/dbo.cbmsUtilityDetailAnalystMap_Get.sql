SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
/******
NAME:
	[DBO].[cbmsUtilityDetailAnalystMap_Get]

DESCRIPTION:
	Returns the Analyst responsible for a Utility

INPUT PARAMETERS:
NAME				DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
@MyAccountId		 int
@utility_detail_id   int
@group_info_id		 int 


OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------

	EXEC cbmsUtilityDetailAnalystMap_Get 49, 10059, 135
	EXEC cbmsUtilityDetailAnalystMap_Get 49,9962,135	

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
  KH		Kevin Horton
  HG		Harihara Suthan G

MODIFICATIONS
INITIALS	DATE		MODIFICATION
---------------------------------------------------------------
 KH			2/1/2010	Modified to pull the correct analyst_id
 HG			01/11/2011	Script modified to fix #22107
						-- As per the requirement modified the procedure to pull the region manager as analyst id if no analysts mapped to the given utility.

******/

CREATE PROCEDURE [dbo].[cbmsUtilityDetailAnalystMap_Get]
	(
	 @MyAccountId			INT
	,@utility_detail_id		INT
	,@group_info_id			INT )
AS
BEGIN

      SET NOCOUNT ON

      SELECT
            udam.utility_detail_analyst_map_id
           ,v.vendor_id									AS Utility_detail_id
           ,gi.group_info_id
           ,ISNULL(udam.analyst_id, rmm.User_Info_Id)	AS Analyst_Id
      FROM
            dbo.VENDOR v
            INNER JOIN dbo.VENDOR_STATE_MAP stmap
                  ON stmap.VENDOR_ID = v.VENDOR_ID

            INNER JOIN dbo.State st
                  ON st.state_id = stmap.state_id
            INNER JOIN dbo.Region reg
                  ON reg.region_id = st.region_id
            INNER JOIN dbo.region_manager_map rmm
                  ON rmm.region_id = reg.region_id

            INNER JOIN dbo.UTILITY_DETAIL ud
                  ON ud.VENDOR_ID = v.VENDOR_ID
            LEFT OUTER JOIN
				(dbo.Utility_Detail_Analyst_Map udam
				 JOIN dbo.GROUP_INFO gi
					ON gi.GROUP_INFO_ID = udam.Group_Info_ID
				)
				ON udam.Utility_Detail_ID = ud.UTILITY_DETAIL_ID
					AND gi.GROUP_INFO_ID = @group_info_id
      WHERE
            v.VENDOR_ID = @utility_detail_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsUtilityDetailAnalystMap_Get] TO [CBMSApplication]
GO
