
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.UPDATE_LATITUDE_LONGITUDE_FOR_ADDRESS_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@addressId							int,
@latitude						decimal(32,16),
@longitude						decimal(32,16)
@Is_System_Generated_Geocode		BIT						1
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- Test update an entry
--exec dbo.UPDATE_LATITUDE_LONGITUDE_FOR_ADDRESS_P
--@addressId =1,
--@latitude = 0,  -- Original value = 35.7553649999999980
--@longitude = 0 -- Original value = -77.8786120000000040

---- reupdate the entry


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter
		SP		Sandeep Pigilam

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates
	  DMR	 09/10/2010	   Modified for Quoted_Identifier
	  SP	 2014-01-16	   MAINT-2444 Added New Column @Is_System_Generated_Geocode		  


******/
CREATE PROCEDURE dbo.UPDATE_LATITUDE_LONGITUDE_FOR_ADDRESS_P
      @addressId INT
     ,@latitude DECIMAL(32, 16)
     ,@longitude DECIMAL(32, 16)
     ,@Is_System_Generated_Geocode BIT = 1
AS 
SET NOCOUNT ON
	

UPDATE
      ADDRESS
SET   
      GEO_LAT = @latitude
     ,GEO_LONG = @longitude
     ,Is_System_Generated_Geocode = @Is_System_Generated_Geocode
WHERE
      ADDRESS_ID = @addressId

;
GO

GRANT EXECUTE ON  [dbo].[UPDATE_LATITUDE_LONGITUDE_FOR_ADDRESS_P] TO [CBMSApplication]
GO
