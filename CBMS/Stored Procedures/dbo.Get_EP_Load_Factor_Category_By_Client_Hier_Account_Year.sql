SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********     
NAME:  dbo.Get_EP_Load_Factor_Category_By_Client_Hier_Account_Year    
   
DESCRIPTION:

	This procedure is used to compare re aggregated EP load factor data with old values ( EP Load factor is not saved any where in system it is calculated in run time and shown in page)	

INPUT PARAMETERS:      
      Name         DataType		Default     Description      
------------------------------------------------------------
@Client_Hier_Id		INT
@Account_Id			INT    
@Year				INT

OUTPUT PARAMETERS:      
      Name              DataType          Default     Description      
------------------------------------------------------------      
      
USAGE EXAMPLES:    

	EXEC Get_EP_Load_Factor_Category_By_Client_Hier_Account_Year 1131, 113, 2007  
	EXEC Get_EP_Load_Factor_Category_By_Client_Hier_Account_Year 1131, 114, 2007

   SELECT TOP 100 * FROM dbo.Cost_Usage_Account_Dtl WHERE Bucket_Master_Id = 101174
   SELECT * FROM dbo.Bucket_Master wHERE BUCKET_Name = 'Load Factor'

------------------------------------------------------------    
AUTHOR INITIALS:    
Initials	Name    
------------------------------------------------------------    
 HG			Harihara Suthan G 

Initials	 Date		Modification
------------------------------------------------------------
HG			2012-05-15  Created
******/

CREATE PROCEDURE dbo.Get_EP_Load_Factor_Category_By_Client_Hier_Account_Year
      ( 
       @Client_Hier_Id INT
      ,@Account_id INT
      ,@Year INT )
AS 
BEGIN    
 
      SET NOCOUNT ON

      DECLARE
            @EP_LF_Bucket_Master_Id INT
           ,@Kwh_Uom_Id INT
           ,@KW_Uom_Id INT

      DECLARE @Service_Month TABLE
            ( 
             Service_Month DATE PRIMARY KEY CLUSTERED )
      DECLARE @Billing_Days TABLE
            ( 
             Service_Month DATE PRIMARY KEY CLUSTERED
            ,Billing_Days INT )
      DECLARE @Old_LF_Value TABLE
            ( 
             Service_Month DATE PRIMARY KEY CLUSTERED
            ,Bucket_Value DECIMAL(32, 16) )
      DECLARE @New_LF_Value TABLE
            ( 
             Service_Month DATE PRIMARY KEY CLUSTERED
            ,Bucket_Value DECIMAL(28, 10) )

      INSERT      INTO @Service_Month
                  ( 
                   Service_Month )
                  SELECT
                        Date_D
                  FROM
                        meta.Date_Dim
                  WHERE
                        Year_Num = @Year

      INSERT      INTO @Billing_Days
                  ( 
                   Service_Month
                  ,Billing_Days )
                  SELECT
                        sm.Service_Month
                       ,bd.Billing_Days
                  FROM
                        @Service_Month sm
                        CROSS APPLY ( SELECT
                                          datediff(D, min(i.Begin_Date), max(i.End_Date))
                                      FROM
                                          dbo.cu_invoice i
                                          JOIN dbo.cu_invoice_service_month im
                                                ON im.cu_invoice_id = i.cu_invoice_id
                                      WHERE
                                          im.Account_ID = @account_id
                                          AND im.SERVICE_MONTH = sm.Service_Month
                                          AND i.is_default = 1 ) bd ( Billing_Days )

      SELECT
            @Kwh_Uom_Id = max(case WHEN ENTITY_NAME = 'KWH' THEN Entity_Id
                              END)
           ,@Kw_Uom_Id = max(case WHEN ENTITY_NAME = 'KW' THEN Entity_Id
                             END)
      FROM
            dbo.Entity
      WHERE
            Entity_Name IN ( 'KWh', 'KW' )
            AND ENTITY_DESCRIPTION = 'Unit for electricity'

      SELECT
            @EP_LF_Bucket_Master_Id = bm.Bucket_Master_Id
      FROM
            dbo.Bucket_Master bm
            JOIN dbo.Code bt
                  ON bt.Code_Id = bm.Bucket_Type_Cd
            JOIN dbo.Commodity com
                  ON com.Commodity_Id = bm.Commodity_Id
      WHERE
            bt.Code_Value = 'Determinant'
            AND bm.Bucket_Name = 'Load Factor'
            AND com.Commodity_Name = 'Electric Power'

      INSERT      INTO @Old_LF_Value
                  ( 
                   Service_Month
                  ,Bucket_Value )
                  SELECT
                        sm.Service_Month
                       ,( ( cu.EL_USAGE * usguom.CONVERSION_FACTOR ) / nullif(( ( cu.EL_ACTUAL_DEMAND * dmduom.CONVERSION_FACTOR ) * bd.Billing_Days * 24 ), 0) ) * 100
                  FROM
                        dbo.Cost_Usage cu
                        INNER JOIN core.Client_Hier ch
                              ON ch.Site_Id = cu.Site_Id
                        INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION usguom
                              ON usguom.BASE_UNIT_ID = cu.EL_UNIT_OF_MEASURE_TYPE_ID
                                 AND usguom.Converted_Unit_Id = @Kwh_Uom_Id
                        INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION dmduom
                              ON dmduom.BASE_UNIT_ID = cu.EL_ACTUAL_DEMAND_UOM_TYPE_ID
                                 AND dmduom.Converted_Unit_Id = @KW_Uom_Id
                        INNER JOIN @Service_Month sm
                              ON sm.Service_Month = cu.Service_Month
                        INNER JOIN @Billing_Days bd
                              ON bd.Service_Month = cu.SERVICE_MONTH
                  WHERE
                        ch.Client_Hier_Id = @Client_Hier_Id
                        AND cu.ACCOUNT_ID = @Account_id

      INSERT      INTO @New_LF_Value
                  ( 
                   Service_Month
                  ,Bucket_Value )
                  SELECT
                        cuad.Service_Month
                       ,cuad.Bucket_Value
                  FROM
                        dbo.Cost_Usage_Account_Dtl cuad
                        INNER JOIN @Service_Month sm
                              ON sm.Service_Month = cuad.Service_Month
                  WHERE
                        Client_Hier_Id = @Client_Hier_Id
                        AND Account_Id = @Account_id
                        AND Bucket_Master_Id = @EP_LF_Bucket_Master_Id

      SELECT
            sm.Service_Month
           ,ov.Bucket_Value AS Existing_LF_Value
           ,nv.Bucket_Value AS New_LF_Value
           ,nv.Bucket_Value - ov.Bucket_Value AS LF_Difference
      FROM
            @Service_Month sm
            LEFT OUTER JOIN @New_LF_Value nv
                  ON nv.Service_Month = sm.Service_Month
            LEFT OUTER JOIN @Old_LF_Value ov
                  ON ov.Service_Month = sm.Service_Month
			           

END
;
GO
GRANT EXECUTE ON  [dbo].[Get_EP_Load_Factor_Category_By_Client_Hier_Account_Year] TO [CBMSApplication]
GO
