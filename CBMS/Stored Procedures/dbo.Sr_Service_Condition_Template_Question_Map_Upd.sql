SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Service_Condition_Template_Question_Map_Upd]
           
DESCRIPTION:             
			To update question display order within the template's category
			
INPUT PARAMETERS:            
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  
	@Sr_Service_Condition_Template_Id	INT
    @Sr_Service_Condition_Category_Id	INT
    @User_Info_Id						INT
    @Display_Seq INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
	BEGIN TRAN
		EXEC dbo.Sr_Service_Condition_Template_Question_Sel_By_Template_Language_CD 3 
		EXEC dbo.Sr_Service_Condition_Template_Question_Map_Upd 
			@Sr_Service_Condition_Template_Id = 3
			,@Sr_Service_Condition_Category_Id = 4
			,@Sr_Service_Condition_Question_Id = 5
			,@Display_Seq = 2
			,@User_Info_Id = 49
		EXEC dbo.Sr_Service_Condition_Template_Question_Sel_By_Template_Language_CD 3 
	ROLLBACK
	
	BEGIN TRANSACTION
		SELECT * FROM dbo.Sr_Service_Condition_Template_Question_Map WHERE Sr_Service_Condition_Template_Category_Map_Id = 371
		EXEC dbo.Sr_Service_Condition_Template_Question_Map_Ins 9,5,130,49,1  
		SELECT * FROM dbo.Sr_Service_Condition_Template_Question_Map WHERE Sr_Service_Condition_Template_Category_Map_Id = 371
		EXEC dbo.Sr_Service_Condition_Template_Question_Map_Upd 9,5,130,49,1  
		SELECT * FROM dbo.Sr_Service_Condition_Template_Question_Map WHERE Sr_Service_Condition_Template_Category_Map_Id = 371
	ROLLBACK TRANSACTION
		
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-02-22	Global Sourcing - Phase3 - GCS-448 Created
******/
CREATE PROCEDURE [dbo].[Sr_Service_Condition_Template_Question_Map_Upd]
      ( 
       @Sr_Service_Condition_Template_Id INT
      ,@Sr_Service_Condition_Category_Id INT
      ,@Sr_Service_Condition_Question_Id INT
      ,@User_Info_Id INT
      ,@Display_Seq INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      UPDATE
            ssctqm
      SET   
            ssctqm.Display_Seq = @Display_Seq
           ,ssctqm.Updated_User_Id = @User_Info_Id
           ,ssctqm.Last_Change_Ts = getdate()
      FROM
            dbo.Sr_Service_Condition_Template_Category_Map ssctcm
            INNER JOIN dbo.Sr_Service_Condition_Template_Question_Map ssctqm
                  ON ssctqm.Sr_Service_Condition_Template_Category_Map_Id = ssctcm.Sr_Service_Condition_Template_Category_Map_Id
      WHERE
            ssctcm.Sr_Service_Condition_Template_Id = @Sr_Service_Condition_Template_Id
            AND ssctcm.Sr_Service_Condition_Category_Id = @Sr_Service_Condition_Category_Id
            AND ssctqm.Sr_Service_Condition_Question_Id = @Sr_Service_Condition_Question_Id
                  
            
END;
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Service_Condition_Template_Question_Map_Upd] TO [CBMSApplication]
GO
