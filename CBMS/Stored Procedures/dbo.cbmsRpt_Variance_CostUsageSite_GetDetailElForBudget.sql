SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
    
/******    
NAME:    
 CBMS.dbo.cbmsRpt_Variance_CostUsageSite_GetDetailElForBudget    
    
DESCRIPTION:    
    
    
INPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
 @MyAccountId    int                       
 @currency_unit_id int                       
 @el_unit_of_measure_type_id int                       
 @ng_unit_of_measure_type_id int                       
 @report_year    int                       
 @client_id      int                       
 @division_id    int        null           
 @site_id        int        null           
    
OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
cbmsRpt_Variance_CostUsageSite_GetDetailElForBudget_Additional 128,3,12,25,2006,235,258,1899   
    
AUTHOR INITIALS:    
 Initials Name    
------------------------------------------------------------    
 AKR    Ashok Kumar Raju   
    
MODIFICATIONS    
    
 Initials Date  Modification    
------------------------------------------------------------    
     9/21/2010 Modify Quoted Identifier    
AKR  7/10/2012 Modified as a Part of Additional Data  
******/    
    
CREATE  PROCEDURE [dbo].[cbmsRpt_Variance_CostUsageSite_GetDetailElForBudget]    
      (   
       @MyAccountId INT  
      ,@currency_unit_id INT  
      ,@el_unit_of_measure_type_id INT  
      ,@ng_unit_of_measure_type_id INT  
      ,@report_year INT  
      ,@client_id INT  
      ,@division_id INT = NULL  
      ,@site_id INT = NULL )  
AS   
BEGIN    
    
      SET nocount ON    
      SET ansi_warnings OFF    
    
   -- sproc driven    
      DECLARE  
            @session_uid UNIQUEIDENTIFIER  
           ,@begin_date DATETIME  
           ,@end_date DATETIME  
           ,@Bucket_Master_Id_EL_Cost INT  
           ,@Bucket_Master_Id_EL_Usage INT  
    
      SET @session_uid = newid()    
    
    
 /* load the months into the rpt_service_month table */    
      EXEC cbmsRpt_ServiceMonth_LoadForReportYear   
            @MyAccountId  
           ,@session_uid  
           ,@report_year  
           ,@client_id    
    
 /* get min and max dates for this report from rpt_service_month table */    
      SELECT  
            @begin_date = min(service_month)  
           ,@end_date = max(service_month)  
      FROM  
            rpt_service_month  
      WHERE  
            session_uid = @session_uid    
              
         
      SELECT  
            @Bucket_Master_Id_EL_Usage = max(case WHEN bm.Bucket_Name = 'Total Usage' THEN bm.Bucket_Master_Id  
                                             END)  
           ,@Bucket_Master_Id_EL_Cost = max(case WHEN bm.Bucket_Name = 'Total Cost' THEN bm.Bucket_Master_Id  
                                            END)  
      FROM  
            dbo.Bucket_Master bm  
            INNER JOIN dbo.Commodity c  
                  ON bm.Commodity_Id = c.Commodity_Id  
      WHERE  
            c.Commodity_Name = 'Electric Power'        
    
      DECLARE  
            @month1 DATETIME  
           ,@month2 DATETIME  
           ,@month3 DATETIME  
           ,@month4 DATETIME  
           ,@month5 DATETIME  
           ,@month6 DATETIME    
      DECLARE  
            @month7 DATETIME  
           ,@month8 DATETIME  
           ,@month9 DATETIME  
           ,@month10 DATETIME  
           ,@month11 DATETIME  
           ,@month12 DATETIME    
    
      SELECT  
            @month1 = max(case WHEN month_number = 1 THEN service_month  
                               ELSE NULL  
                          END)  
           ,@month2 = max(case WHEN month_number = 2 THEN service_month  
                               ELSE NULL  
                          END)  
           ,@month3 = max(case WHEN month_number = 3 THEN service_month  
                               ELSE NULL  
                          END)  
           ,@month4 = max(case WHEN month_number = 4 THEN service_month  
         ELSE NULL  
                          END)  
           ,@month5 = max(case WHEN month_number = 5 THEN service_month  
                               ELSE NULL  
                          END)  
           ,@month6 = max(case WHEN month_number = 6 THEN service_month  
                               ELSE NULL  
                          END)  
           ,@month7 = max(case WHEN month_number = 7 THEN service_month  
                               ELSE NULL  
                          END)  
           ,@month8 = max(case WHEN month_number = 8 THEN service_month  
                               ELSE NULL  
                          END)  
           ,@month9 = max(case WHEN month_number = 9 THEN service_month  
                               ELSE NULL  
                          END)  
           ,@month10 = max(case WHEN month_number = 10 THEN service_month  
                                ELSE NULL  
                           END)  
           ,@month11 = max(case WHEN month_number = 11 THEN service_month  
                                ELSE NULL  
                           END)  
           ,@month12 = max(case WHEN month_number = 12 THEN service_month  
                                ELSE NULL  
                           END)  
      FROM  
            rpt_service_month  
      WHERE  
            session_uid = @session_uid    
    
      EXEC cbmsRpt_ServiceMonth_RemoveForSession   
            @MyAccountId  
           ,@session_uid    
    
      SET ansi_warnings ON    
    
     
      
      SELECT  
            ch.Site_Id  
           ,rtrim(ad.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' Site_Name  
           ,ch.Sitegroup_Id Division_Id  
           ,ch.Sitegroup_Name Division_Name  
           ,ch.client_Id  
           ,Ch.Client_Name  
           ,field_type = 'Usage'  
           ,field = 'Usage'  
           ,month1 = sum(case WHEN cusd.service_month = @month1 THEN isnull(cusd.Bucket_Value, 0) * isnull(cuc.conversion_factor, 0)  
                              ELSE 0  
                         END)  
           ,month2 = sum(case WHEN cusd.service_month = @month2 THEN isnull(cusd.Bucket_Value, 0) * isnull(cuc.conversion_factor, 0)  
                              ELSE 0  
                         END)  
           ,month3 = sum(case WHEN cusd.service_month = @month3 THEN isnull(cusd.Bucket_Value, 0) * isnull(cuc.conversion_factor, 0)  
                              ELSE 0  
                         END)  
           ,month4 = sum(case WHEN cusd.service_month = @month4 THEN isnull(cusd.Bucket_Value, 0) * isnull(cuc.conversion_factor, 0)  
                              ELSE 0  
                         END)  
           ,month5 = sum(case WHEN cusd.service_month = @month5 THEN isnull(cusd.Bucket_Value, 0) * isnull(cuc.conversion_factor, 0)  
                              ELSE 0  
                         END)  
           ,month6 = sum(case WHEN cusd.service_month = @month6 THEN isnull(cusd.Bucket_Value, 0) * isnull(cuc.conversion_factor, 0)  
                              ELSE 0  
                         END)  
           ,month7 = sum(case WHEN cusd.service_month = @month7 THEN isnull(cusd.Bucket_Value, 0) * isnull(cuc.conversion_factor, 0)  
                              ELSE 0  
                         END)  
           ,month8 = sum(case WHEN cusd.service_month = @month8 THEN isnull(cusd.Bucket_Value, 0) * isnull(cuc.conversion_factor, 0)  
                              ELSE 0  
                         END)  
           ,month9 = sum(case WHEN cusd.service_month = @month9 THEN isnull(cusd.Bucket_Value, 0) * isnull(cuc.conversion_factor, 0)  
                              ELSE 0  
                         END)  
           ,month10 = sum(case WHEN cusd.service_month = @month10 THEN isnull(cusd.Bucket_Value, 0) * isnull(cuc.conversion_factor, 0)  
                               ELSE 0  
                          END)  
           ,month11 = sum(case WHEN cusd.service_month = @month11 THEN isnull(cusd.Bucket_Value, 0) * isnull(cuc.conversion_factor, 0)  
                           ELSE 0  
                          END)  
           ,month12 = sum(case WHEN cusd.service_month = @month12 THEN isnull(cusd.Bucket_Value, 0) * isnull(cuc.conversion_factor, 0)  
                               ELSE 0  
                          END)  
           ,total = sum(case WHEN ( cusd.service_month BETWEEN @month1 AND @month12 ) THEN isnull(cusd.Bucket_Value, 0) * isnull(cuc.conversion_factor, 0)  
                             ELSE 0  
                        END)  
           ,month1_complete = max(case WHEN ip.service_month = @month1 THEN isnull(ip.el_is_complete, 0)  
                                       ELSE 0  
                                  END)  
           ,month2_complete = max(case WHEN ip.service_month = @month2 THEN isnull(ip.el_is_complete, 0)  
                                       ELSE 0  
                                  END)  
           ,month3_complete = max(case WHEN ip.service_month = @month3 THEN isnull(ip.el_is_complete, 0)  
                                       ELSE 0  
                                  END)  
           ,month4_complete = max(case WHEN ip.service_month = @month4 THEN isnull(ip.el_is_complete, 0)  
                                       ELSE 0  
                                  END)  
           ,month5_complete = max(case WHEN ip.service_month = @month5 THEN isnull(ip.el_is_complete, 0)  
                                       ELSE 0  
                                  END)  
           ,month6_complete = max(case WHEN ip.service_month = @month6 THEN isnull(ip.el_is_complete, 0)  
                                       ELSE 0  
                                  END)  
           ,month7_complete = max(case WHEN ip.service_month = @month7 THEN isnull(ip.el_is_complete, 0)  
                                       ELSE 0  
                                  END)  
           ,month8_complete = max(case WHEN ip.service_month = @month8 THEN isnull(ip.el_is_complete, 0)  
                                       ELSE 0  
                                  END)  
           ,month9_complete = max(case WHEN ip.service_month = @month9 THEN isnull(ip.el_is_complete, 0)  
                                       ELSE 0  
                                  END)  
           ,month10_complete = max(case WHEN ip.service_month = @month10 THEN isnull(ip.el_is_complete, 0)  
                                        ELSE 0  
                                   END)  
           ,month11_complete = max(case WHEN ip.service_month = @month11 THEN isnull(ip.el_is_complete, 0)  
                                        ELSE 0  
                                   END)  
           ,month12_complete = max(case WHEN ip.service_month = @month12 THEN isnull(ip.el_is_complete, 0)  
                                        ELSE 0  
                                   END)  
           ,month1_published = max(case WHEN ip.service_month = @month1 THEN isnull(ip.el_is_published, 0)  
                                        ELSE 0  
                                   END)  
           ,month2_published = max(case WHEN ip.service_month = @month2 THEN isnull(ip.el_is_published, 0)  
                                        ELSE 0  
                                   END)  
           ,month3_published = max(case WHEN ip.service_month = @month3 THEN isnull(ip.el_is_published, 0)  
                                        ELSE 0  
                                   END)  
           ,month4_published = max(case WHEN ip.service_month = @month4 THEN isnull(ip.el_is_published, 0)  
                                        ELSE 0  
                                   END)  
           ,month5_published = max(case WHEN ip.service_month = @month5 THEN isnull(ip.el_is_published, 0)  
                                        ELSE 0  
                                   END)  
           ,month6_published = max(case WHEN ip.service_month = @month6 THEN isnull(ip.el_is_published, 0)  
                                        ELSE 0  
 END)  
           ,month7_published = max(case WHEN ip.service_month = @month7 THEN isnull(ip.el_is_published, 0)  
                                        ELSE 0  
                                   END)  
           ,month8_published = max(case WHEN ip.service_month = @month8 THEN isnull(ip.el_is_published, 0)  
                                        ELSE 0  
                                   END)  
           ,month9_published = max(case WHEN ip.service_month = @month9 THEN isnull(ip.el_is_published, 0)  
                                        ELSE 0  
                                   END)  
           ,month10_published = max(case WHEN ip.service_month = @month10 THEN isnull(ip.el_is_published, 0)  
                                         ELSE 0  
                                    END)  
           ,month11_published = max(case WHEN ip.service_month = @month11 THEN isnull(ip.el_is_published, 0)  
                                         ELSE 0  
                                    END)  
           ,month12_published = max(case WHEN ip.service_month = @month12 THEN isnull(ip.el_is_published, 0)  
                                         ELSE 0  
                                    END)  
           ,month1_under_review = max(case WHEN ip.service_month = @month1 THEN isnull(ip.el_under_review, 0)  
                                           ELSE 0  
                                      END)  
           ,month2_under_review = max(case WHEN ip.service_month = @month2 THEN isnull(ip.el_under_review, 0)  
                                           ELSE 0  
                                      END)  
           ,month3_under_review = max(case WHEN ip.service_month = @month3 THEN isnull(ip.el_under_review, 0)  
                                           ELSE 0  
                                      END)  
           ,month4_under_review = max(case WHEN ip.service_month = @month4 THEN isnull(ip.el_under_review, 0)  
                                           ELSE 0  
                                      END)  
           ,month5_under_review = max(case WHEN ip.service_month = @month5 THEN isnull(ip.el_under_review, 0)  
                                           ELSE 0  
                                      END)  
           ,month6_under_review = max(case WHEN ip.service_month = @month6 THEN isnull(ip.el_under_review, 0)  
                                           ELSE 0  
                                      END)  
           ,month7_under_review = max(case WHEN ip.service_month = @month7 THEN isnull(ip.el_under_review, 0)  
                                           ELSE 0  
                                      END)  
           ,month8_under_review = max(case WHEN ip.service_month = @month8 THEN isnull(ip.el_under_review, 0)  
                                           ELSE 0  
                                      END)  
           ,month9_under_review = max(case WHEN ip.service_month = @month9 THEN isnull(ip.el_under_review, 0)  
                                           ELSE 0  
                                      END)  
           ,month10_under_review = max(case WHEN ip.service_month = @month10 THEN isnull(ip.el_under_review, 0)  
                                            ELSE 0  
                                       END)  
           ,month11_under_review = max(case WHEN ip.service_month = @month11 THEN isnull(ip.el_under_review, 0)  
                                            ELSE 0  
                                       END)  
           ,month12_under_review = max(case WHEN ip.service_month = @month12 THEN isnull(ip.el_under_review, 0)  
                                            ELSE 0  
                                       END)  
           ,report_year = @report_year  
           ,commodity_type = 'Electric Power'  
      FROM  
            core.Client_Hier ch  
            INNER JOIN site s  
                  ON ch.Site_Id = s.SITE_ID  
            INNER JOIN dbo.ADDRESS ad  
                  ON s.PRIMARY_ADDRESS_ID = ad.ADDRESS_ID  
   LEFT JOIN dbo.Cost_Usage_Site_Dtl cusd  
                  ON cusd.Client_Hier_Id = ch.Client_Hier_Id  
            LEFT OUTER JOIN dbo.consumption_unit_conversion cuc  
                  ON cuc.base_unit_id = cusd.UOM_Type_Id  
                     AND cuc.converted_unit_id = @el_unit_of_measure_type_id  
            LEFT OUTER JOIN dbo.invoice_participation_site ip  
                  ON ip.site_id = s.site_id  
                     AND ip.service_month = cusd.service_month  
      WHERE  
            ch.client_id = @client_id  
            AND ( @division_id IS NULL  
                  OR ch.Sitegroup_Id = @division_id )  
            AND ( @site_id IS NULL  
                  OR ch.site_id = @site_id )  
            AND ( ip.is_published = 1  
                  OR ip.is_published IS NULL )  
            AND s.closed = 0  
            AND s.not_managed = 0  
            AND cusd.Bucket_Master_Id = @Bucket_Master_Id_EL_Usage  
      GROUP BY  
            ch.site_id  
           ,ad.city  
           ,ch.state_name  
           ,ch.site_name  
           ,ch.sitegroup_Id  
           ,ch.Sitegroup_Name  
           ,ch.client_Id  
           ,Ch.Client_Name  
      UNION ALL  
      SELECT  
            ch.Site_Id  
           ,rtrim(ad.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' Site_Name  
           ,ch.Sitegroup_Id Division_Id  
           ,ch.Sitegroup_Name Division_Name  
           ,ch.client_Id  
           ,Ch.Client_Name  
           ,field_type = 'Total Cost'  
           ,field = 'Total Cost'  
           ,month1 = sum(case WHEN cusd.service_month = @month1 THEN isnull(cusd.Bucket_Value, 0) * isnull(cuc.conversion_factor, 0)  
                              ELSE 0  
                         END)  
           ,month2 = sum(case WHEN cusd.service_month = @month2 THEN isnull(cusd.Bucket_Value, 0) * isnull(cuc.conversion_factor, 0)  
                              ELSE 0  
                         END)  
           ,month3 = sum(case WHEN cusd.service_month = @month3 THEN isnull(cusd.Bucket_Value, 0) * isnull(cuc.conversion_factor, 0)  
                              ELSE 0  
                         END)  
           ,month4 = sum(case WHEN cusd.service_month = @month4 THEN isnull(cusd.Bucket_Value, 0) * isnull(cuc.conversion_factor, 0)  
                              ELSE 0  
                         END)  
           ,month5 = sum(case WHEN cusd.service_month = @month5 THEN isnull(cusd.Bucket_Value, 0) * isnull(cuc.conversion_factor, 0)  
                              ELSE 0  
                         END)  
           ,month6 = sum(case WHEN cusd.service_month = @month6 THEN isnull(cusd.Bucket_Value, 0) * isnull(cuc.conversion_factor, 0)  
                              ELSE 0  
                         END)  
           ,month7 = sum(case WHEN cusd.service_month = @month7 THEN isnull(cusd.Bucket_Value, 0) * isnull(cuc.conversion_factor, 0)  
                              ELSE 0  
                         END)  
           ,month8 = sum(case WHEN cusd.service_month = @month8 THEN isnull(cusd.Bucket_Value, 0) * isnull(cuc.conversion_factor, 0)  
                              ELSE 0  
                         END)  
           ,month9 = sum(case WHEN cusd.service_month = @month9 THEN isnull(cusd.Bucket_Value, 0) * isnull(cuc.conversion_factor, 0)  
                              ELSE 0  
                         END)  
           ,month10 = sum(case WHEN cusd.service_month = @month10 THEN isnull(cusd.Bucket_Value, 0) * isnull(cuc.conversion_factor, 0)  
                               ELSE 0  
                          END)  
           ,month11 = sum(case WHEN cusd.service_month = @month11 THEN isnull(cusd.Bucket_Value, 0) * isnull(cuc.conversion_factor, 0)  
                               ELSE 0  
                          END)  
           ,month12 = sum(case WHEN cusd.service_month = @month12 THEN isnull(cusd.Bucket_Value, 0) * isnull(cuc.conversion_factor, 0)  
                               ELSE 0  
                          END)  
           ,total = sum(case WHEN cusd.service_month BETWEEN @month1 AND @month12 THEN isnull(cusd.Bucket_Value, 0) * isnull(cuc.conversion_factor, 0)  
                             ELSE 0  
                        END)  
           ,month1_complete = max(case WHEN ip.service_month = @month1 THEN isnull(ip.el_is_complete, 0)  
                                       ELSE 0  
                                  END)  
           ,month2_complete = max(case WHEN ip.service_month = @month2 THEN isnull(ip.el_is_complete, 0)  
                                       ELSE 0  
                                  END)  
           ,month3_complete = max(case WHEN ip.service_month = @month3 THEN isnull(ip.el_is_complete, 0)  
                                       ELSE 0  
                                  END)  
           ,month4_complete = max(case WHEN ip.service_month = @month4 THEN isnull(ip.el_is_complete, 0)  
                                       ELSE 0  
                                  END)  
           ,month5_complete = max(case WHEN ip.service_month = @month5 THEN isnull(ip.el_is_complete, 0)  
                                       ELSE 0  
                                  END)  
           ,month6_complete = max(case WHEN ip.service_month = @month6 THEN isnull(ip.el_is_complete, 0)  
                                       ELSE 0  
                                  END)  
           ,month7_complete = max(case WHEN ip.service_month = @month7 THEN isnull(ip.el_is_complete, 0)  
                                       ELSE 0  
                                  END)  
           ,month8_complete = max(case WHEN ip.service_month = @month8 THEN isnull(ip.el_is_complete, 0)  
                                       ELSE 0  
                                  END)  
           ,month9_complete = max(case WHEN ip.service_month = @month9 THEN isnull(ip.el_is_complete, 0)  
                                       ELSE 0  
                                  END)  
           ,month10_complete = max(case WHEN ip.service_month = @month10 THEN isnull(ip.el_is_complete, 0)  
                                        ELSE 0  
                                   END)  
           ,month11_complete = max(case WHEN ip.service_month = @month11 THEN isnull(ip.el_is_complete, 0)  
                                        ELSE 0  
                                   END)  
           ,month12_complete = max(case WHEN ip.service_month = @month12 THEN isnull(ip.el_is_complete, 0)  
                                        ELSE 0  
                                   END)  
           ,month1_published = max(case WHEN ip.service_month = @month1 THEN isnull(ip.el_is_published, 0)  
                                        ELSE 0  
                                   END)  
           ,month2_published = max(case WHEN ip.service_month = @month2 THEN isnull(ip.el_is_published, 0)  
                                        ELSE 0  
                                   END)  
           ,month3_published = max(case WHEN ip.service_month = @month3 THEN isnull(ip.el_is_published, 0)  
                                        ELSE 0  
                                   END)  
           ,month4_published = max(case WHEN ip.service_month = @month4 THEN isnull(ip.el_is_published, 0)  
                                        ELSE 0  
                                   END)  
           ,month5_published = max(case WHEN ip.service_month = @month5 THEN isnull(ip.el_is_published, 0)  
                                        ELSE 0  
                                   END)  
           ,month6_published = max(case WHEN ip.service_month = @month6 THEN isnull(ip.el_is_published, 0)  
                                        ELSE 0  
                                   END)  
           ,month7_published = max(case WHEN ip.service_month = @month7 THEN isnull(ip.el_is_published, 0)  
                                        ELSE 0  
                                   END)  
           ,month8_published = max(case WHEN ip.service_month = @month8 THEN isnull(ip.el_is_published, 0)  
                                  ELSE 0  
                                   END)  
           ,month9_published = max(case WHEN ip.service_month = @month9 THEN isnull(ip.el_is_published, 0)  
                                        ELSE 0  
                                   END)  
           ,month10_published = max(case WHEN ip.service_month = @month10 THEN isnull(ip.el_is_published, 0)  
                                         ELSE 0  
                                    END)  
           ,month11_published = max(case WHEN ip.service_month = @month11 THEN isnull(ip.el_is_published, 0)  
                                         ELSE 0  
                                    END)  
           ,month12_published = max(case WHEN ip.service_month = @month12 THEN isnull(ip.el_is_published, 0)  
                                         ELSE 0  
                                    END)  
           ,month1_under_review = max(case WHEN ip.service_month = @month1 THEN isnull(ip.el_under_review, 0)  
                                           ELSE 0  
                                      END)  
           ,month2_under_review = max(case WHEN ip.service_month = @month2 THEN isnull(ip.el_under_review, 0)  
                                           ELSE 0  
                                      END)  
           ,month3_under_review = max(case WHEN ip.service_month = @month3 THEN isnull(ip.el_under_review, 0)  
                                           ELSE 0  
                                      END)  
           ,month4_under_review = max(case WHEN ip.service_month = @month4 THEN isnull(ip.el_under_review, 0)  
                                           ELSE 0  
                                      END)  
           ,month5_under_review = max(case WHEN ip.service_month = @month5 THEN isnull(ip.el_under_review, 0)  
                                           ELSE 0  
                                      END)  
           ,month6_under_review = max(case WHEN ip.service_month = @month6 THEN isnull(ip.el_under_review, 0)  
                                           ELSE 0  
                                      END)  
           ,month7_under_review = max(case WHEN ip.service_month = @month7 THEN isnull(ip.el_under_review, 0)  
                                           ELSE 0  
                                      END)  
           ,month8_under_review = max(case WHEN ip.service_month = @month8 THEN isnull(ip.el_under_review, 0)  
                                           ELSE 0  
                                      END)  
           ,month9_under_review = max(case WHEN ip.service_month = @month9 THEN isnull(ip.el_under_review, 0)  
                                           ELSE 0  
                                      END)  
           ,month10_under_review = max(case WHEN ip.service_month = @month10 THEN isnull(ip.el_under_review, 0)  
                                            ELSE 0  
                                       END)  
           ,month11_under_review = max(case WHEN ip.service_month = @month11 THEN isnull(ip.el_under_review, 0)  
                                            ELSE 0  
                                       END)  
           ,month12_under_review = max(case WHEN ip.service_month = @month12 THEN isnull(ip.el_under_review, 0)  
                                            ELSE 0  
                                       END)  
           ,report_year = @report_year  
           ,commodity_type = 'Electric Power'  
      FROM  
            core.Client_Hier ch  
            INNER JOIN site s  
                  ON ch.Site_Id = s.SITE_ID  
            INNER JOIN dbo.ADDRESS ad  
                  ON s.PRIMARY_ADDRESS_ID = ad.ADDRESS_ID  
            LEFT JOIN dbo.Cost_Usage_Site_Dtl cusd  
                  ON cusd.Client_Hier_Id = ch.Client_Hier_Id  
            LEFT OUTER JOIN currency_unit_conversion cuc  
                  ON cuc.base_unit_id = cusd.currency_unit_id  
                     AND cuc.converted_unit_id = @currency_unit_id  
                     AND cuc.conversion_date = cusd.service_month  
                     AND cuc.currency_group_id = ch.Client_Currency_Group_Id  
            LEFT OUTER JOIN invoice_participation_site ip WITH ( NOLOCK )  
                                                               ON ip.site_id = s.site_id  
                                                                  AND ip.service_month = cusd.service_month  
      WHERE  
            ch.client_id = @client_id  
            AND ( @division_id IS NULL  
                  OR ch.Sitegroup_Id = @division_id )  
            AND ( @site_id IS NULL  
                  OR ch.site_id = @site_id )  
            AND ( ip.is_published = 1  
                  OR ip.is_published IS NULL )  
            AND s.closed = 0  
            AND s.not_managed = 0  
            AND cusd.Bucket_Master_Id = @Bucket_Master_Id_EL_Cost  
      GROUP BY  
            ch.site_id  
           ,ad.city  
           ,ch.state_name  
           ,ch.site_name  
           ,ch.sitegroup_Id  
           ,ch.Sitegroup_Name  
           ,ch.client_Id  
           ,Ch.Client_Name  
      ORDER BY  
            Site_Name  
           ,field_type DESC    
    
END    
  

;
GO
GRANT EXECUTE ON  [dbo].[cbmsRpt_Variance_CostUsageSite_GetDetailElForBudget] TO [CBMSApplication]
GO
