SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Meter_DMO_Supplier_Account_IC_Chase_History_Dtls_Sel_By_Meter_Contract_Dates]
           
DESCRIPTION:             
			To get map/unmap countries and SR pricing products
			
INPUT PARAMETERS:            
	Name					DataType		Default		Description  
---------------------------------------------------------------------------------  
    @Meter_Id				VARCHAR(MAX)
    @Contract_Start_Date	DATETIME
    @Contract_End_Date		DATETIME


OUTPUT PARAMETERS:
	Name			DataType		Default		Description  
---------------------------------------------------------------------------------  


 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
	SELECT * FROM dbo.SUPPLIER_ACCOUNT_METER_MAP WHERE Contract_ID = -1

	SELECT * FROM dbo.SUPPLIER_ACCOUNT_METER_MAP WHERE METER_ID IN (861608,861760,21957) AND Contract_ID = -1
	EXEC dbo.Meter_DMO_Supplier_Account_IC_Chase_History_Dtls_Sel_By_Meter_Contract_Dates '861608,861760,21957','2017-03-01','2017-04-01'
	EXEC dbo.Meter_DMO_Supplier_Account_IC_Chase_History_Dtls_Sel_By_Meter_Contract_Dates '861608,861760,21957','2017-01-01','2017-12-01'

	SELECT * FROM dbo.SUPPLIER_ACCOUNT_METER_MAP WHERE METER_ID IN (861608,861760,21957) AND Contract_ID = -1
	EXEC dbo.Meter_DMO_Supplier_Account_IC_Chase_History_Dtls_Sel_By_Meter_Contract_Dates '861608,861760,21957','2015-03-01','2015-04-01'
	EXEC dbo.Meter_DMO_Supplier_Account_IC_Chase_History_Dtls_Sel_By_Meter_Contract_Dates '861608,861760,21957','2015-01-01','2015-12-01'
	EXEC dbo.Meter_DMO_Supplier_Account_IC_Chase_History_Dtls_Sel_By_Meter_Contract_Dates '861608,861760,21957','2015-04-01','2015-12-01'


	EXEC Meter_DMO_Supplier_Account_IC_Chase_History_Dtls_Sel_By_Meter_Contract_Dates '1231176, 1215176','2020-04-01 00:00:00.0','2020-12-31 00:00:00.0'


 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-03-08	Contract placeholder CP-77 Created
	NR			2020-06-05	MAINT-10144 - Added group by clause.
******/

CREATE PROCEDURE [dbo].[Meter_DMO_Supplier_Account_IC_Chase_History_Dtls_Sel_By_Meter_Contract_Dates]
    (
        @Meter_Id VARCHAR(MAX)
        , @Contract_Start_Date DATETIME
        , @Contract_End_Date DATETIME
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Meters_List TABLE
              (
                  Meter_Id INT PRIMARY KEY
                  , METER_NUMBER VARCHAR(50)
              );

        DECLARE @Accounts_List TABLE
              (
                  Account_Id INT
              );

        DECLARE @Chase_History_Accounts_List TABLE
              (
                  Account_Id INT PRIMARY KEY
              );

        INSERT INTO @Meters_List
             (
                 Meter_Id
                 , METER_NUMBER
             )
        SELECT
            Segments
            , cha.Meter_Number
        FROM
            dbo.ufn_split(@Meter_Id, ',')
            INNER JOIN Core.Client_Hier_Account cha
                ON Segments = cha.Meter_Id
        GROUP BY
            Segments
            , cha.Meter_Number;


        INSERT INTO @Accounts_List
             (
                 Account_Id
             )
        SELECT
            map.ACCOUNT_ID
        FROM
            dbo.SUPPLIER_ACCOUNT_METER_MAP map
            INNER JOIN @Meters_List mtrs
                ON mtrs.Meter_Id = map.METER_ID
            INNER JOIN Core.Client_Hier_Account cha
                ON map.ACCOUNT_ID = cha.Account_Id
                   AND  map.METER_ID = cha.Meter_Id
        WHERE
            map.Contract_ID = -1
            AND (   (   map.METER_DISASSOCIATION_DATE IS NOT NULL
                        AND (   @Contract_Start_Date BETWEEN map.METER_ASSOCIATION_DATE
                                                     AND     map.METER_DISASSOCIATION_DATE
                                OR  @Contract_End_Date BETWEEN map.METER_ASSOCIATION_DATE
                                                       AND     map.METER_DISASSOCIATION_DATE
                                OR  map.METER_ASSOCIATION_DATE BETWEEN @Contract_Start_Date
                                                               AND     @Contract_End_Date
                                OR  map.METER_DISASSOCIATION_DATE BETWEEN @Contract_Start_Date
                                                                  AND     @Contract_End_Date))
                    OR  (   map.METER_DISASSOCIATION_DATE IS NULL
                            AND (   @Contract_Start_Date >= map.METER_ASSOCIATION_DATE
                                    OR  @Contract_End_Date >= map.METER_ASSOCIATION_DATE)));




        -- chase history 
        INSERT INTO @Chase_History_Accounts_List
             (
                 Account_Id
             )
        SELECT
            al.Account_Id
        FROM
            dbo.Invoice_Collection_Chase_Log iccl
            INNER JOIN dbo.Invoice_Collection_Chase_Log_Queue_Map icclqm
                ON iccl.Invoice_Collection_Chase_Log_Id = icclqm.Invoice_Collection_Chase_Log_Id
            INNER JOIN dbo.Invoice_Collection_Queue icq
                ON icclqm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN @Accounts_List al
                ON icac.Account_Id = al.Account_Id
            INNER JOIN dbo.Code sc
                ON sc.Code_Id = iccl.Status_Cd
        WHERE
            sc.Code_Value = 'Close'
        GROUP BY
            al.Account_Id;

        INSERT INTO @Chase_History_Accounts_List
             (
                 Account_Id
             )
        SELECT
            al.Account_Id
        FROM
            dbo.Invoice_Collection_Issue_Log icil
            INNER JOIN dbo.Invoice_Collection_Queue icq
                ON icil.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN @Accounts_List al
                ON icac.Account_Id = al.Account_Id
            INNER JOIN dbo.Code isc
                ON isc.Code_Id = icil.Issue_Status_Cd
        WHERE
            isc.Code_Value = 'Close'
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    @Chase_History_Accounts_List chal
                               WHERE
                                    chal.Account_Id = al.Account_Id);

        INSERT INTO @Chase_History_Accounts_List
             (
                 Account_Id
             )
        SELECT
            al.Account_Id
        FROM
            dbo.Invoice_Collection_Exception_Comment icec
            INNER JOIN dbo.Invoice_Collection_Queue icq
                ON icec.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN @Accounts_List al
                ON icac.Account_Id = al.Account_Id
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                @Chase_History_Accounts_List chal
                           WHERE
                                chal.Account_Id = al.Account_Id);


        INSERT INTO @Chase_History_Accounts_List
             (
                 Account_Id
             )
        SELECT
            al.Account_Id
        FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN @Accounts_List al
                ON icac.Account_Id = al.Account_Id
            INNER JOIN dbo.Code icqtc
                ON icqtc.Code_Id = icq.Invoice_Collection_Queue_Type_Cd
            INNER JOIN dbo.Code sc
                ON sc.Code_Id = icq.Status_Cd
        WHERE
            icqtc.Code_Value = 'ICR'
            AND sc.Code_Value = 'Received'
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    @Chase_History_Accounts_List chal
                               WHERE
                                    chal.Account_Id = al.Account_Id);

        INSERT INTO @Chase_History_Accounts_List
             (
                 Account_Id
             )
        SELECT
            al.Account_Id
        FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN @Accounts_List al
                ON icac.Account_Id = al.Account_Id
            INNER JOIN dbo.Code icqtc
                ON icqtc.Code_Id = icq.Invoice_Collection_Queue_Type_Cd
            INNER JOIN dbo.Code sc
                ON sc.Code_Id = icq.Status_Cd
        WHERE
            icqtc.Code_Value = 'ICE'
            AND sc.Code_Value = 'Processed'
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    @Chase_History_Accounts_List chal
                               WHERE
                                    chal.Account_Id = al.Account_Id);



        SELECT
            mtrs.Meter_Id
            , map.METER_ASSOCIATION_DATE
            , map.METER_DISASSOCIATION_DATE
            , cha.Account_Vendor_Id
            , cha.Account_Vendor_Name
            , map.ACCOUNT_ID
            , CASE WHEN chal.Account_Id IS NULL THEN 0
                  ELSE 1
              END IC_Chase_History_Exists
        FROM
            dbo.SUPPLIER_ACCOUNT_METER_MAP map
            INNER JOIN @Meters_List mtrs
                ON mtrs.Meter_Id = map.METER_ID
            INNER JOIN Core.Client_Hier_Account cha
                ON map.ACCOUNT_ID = cha.Account_Id
                   AND  map.METER_ID = cha.Meter_Id
            LEFT OUTER JOIN @Chase_History_Accounts_List chal
                ON chal.Account_Id = map.ACCOUNT_ID
        WHERE
            map.Contract_ID = -1
            AND (   (   map.METER_DISASSOCIATION_DATE IS NOT NULL
                        AND (   @Contract_Start_Date BETWEEN map.METER_ASSOCIATION_DATE
                                                     AND     map.METER_DISASSOCIATION_DATE
                                OR  @Contract_End_Date BETWEEN map.METER_ASSOCIATION_DATE
                                                       AND     map.METER_DISASSOCIATION_DATE
                                OR  map.METER_ASSOCIATION_DATE BETWEEN @Contract_Start_Date
                                                               AND     @Contract_End_Date
                                OR  map.METER_DISASSOCIATION_DATE BETWEEN @Contract_Start_Date
                                                                  AND     @Contract_End_Date))
                    OR  (   map.METER_DISASSOCIATION_DATE IS NULL
                            AND (   @Contract_Start_Date >= map.METER_ASSOCIATION_DATE
                                    OR  @Contract_End_Date >= map.METER_ASSOCIATION_DATE)))
        GROUP BY
            mtrs.Meter_Id
            , map.METER_ASSOCIATION_DATE
            , map.METER_DISASSOCIATION_DATE
            , cha.Account_Vendor_Id
            , cha.Account_Vendor_Name
            , map.ACCOUNT_ID
            , CASE WHEN chal.Account_Id IS NULL THEN 0
                  ELSE 1
              END;






    END;


    ;

GO
GRANT EXECUTE ON  [dbo].[Meter_DMO_Supplier_Account_IC_Chase_History_Dtls_Sel_By_Meter_Contract_Dates] TO [CBMSApplication]
GO
