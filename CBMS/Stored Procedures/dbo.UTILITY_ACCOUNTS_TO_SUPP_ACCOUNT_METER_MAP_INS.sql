SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 dbo.UTILITY_ACCOUNTS_TO_SUPP_ACCOUNT_METER_MAP_INS    
    
DESCRIPTION:    
 INSERTS ASSIGNED/REASSIGNED SUPPLIER_ACCOUNT DETAILS INTO SUPP_ACCOUNT_METER_MAP TABLE    
     
 @Is_New_Account
    
INPUT PARAMETERS:    
NAME						DATATYPE	DEFAULT		DESCRIPTION    
------------------------------------------------------------    
@CONTRACT_ID				INT    
@ACCOUNT_ID					INT    
@METER_ID					INT    
@METER_ASSOCIATION_DT		DATETIME    
@METER_DISASSOCIATION_DT	DATETIME    
@ISNEWACCOUNT				BOOLEAN


OUTPUT PARAMETERS:    
NAME						DATATYPE	DEFAULT		DESCRIPTION    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
    
 SELECT  TOP 2 mt.ACCOUNT_ID,mt.NOT_EXPECTED,mt.NOT_MANAGED FROM dbo.ACCOUNT mt 
	WHERE NOT EXISTS ( SELECT 1 FROM dbo.SUPPLIER_ACCOUNT_METER_MAP samm
						WHERE mt.ACCOUNT_ID = samm.ACCOUNT_ID ) AND mt.ACCOUNT_TYPE_ID=37 AND NOT_EXPECTED=1 AND NOT_MANAGED =0

SELECT TOP 2 mt.METER_ID FROM dbo.METER mt 
	WHERE NOT EXISTS ( SELECT 1 FROM dbo.SUPPLIER_ACCOUNT_METER_MAP samm
						WHERE mt.METER_ID = samm.METER_ID)
BEGIN TRANSACTION
	EXECUTE dbo.UTILITY_ACCOUNTS_TO_SUPP_ACCOUNT_METER_MAP_INS 
	      @CONTRACT_ID = 1
	     ,@ACCOUNT_ID = 7596 --14236, 4714
	     ,@METER_ID = 6629
	     ,@METER_ASSOCIATION_DT = '2013-06-11 11:25:08'
	     ,@METER_DISASSOCIATION_DT = '2013-06-11 11:25:08'
	     ,@IS_NEW_ACCOUNT = 1
	SELECT * FROM dbo.SUPPLIER_ACCOUNT_METER_MAP WHERE ACCOUNT_ID=7596 AND METER_ID=6629
	SELECT * FROM dbo.INVOICE_PARTICIPATION_QUEUE WHERE ACCOUNT_ID = 7596
ROLLBACK TRANSACTION

    
AUTHOR INITIALS:    
INITIALS NAME    
------------------------------------------------------------    
MGB		BHASKARAN GOPALAKRISHNAN    
HG		Hari
RR		Raghu Reddy    


MODIFICATIONS    
INITIALS	DATE		MODIFICATION    
------------------------------------------------------------    
MGB			21-AUG-09	CREATED    
HG			29-OCT-09	Parameters renamed.  
MGB			17-DEC-09	Added a parameter @IS_NEW_ACCOUNT. 
						When new supplier acount is created inserts account_id into invoice_participation_queue table, so added cbmsInvoiceParticipation_InsertSupplierAccount.
RR			2013-06-10	ENHANCE-38 Its an enhancement the user have given ability to make account not expected or not managed while creating
							the account, so added the script part to add the event to invoice_participation_queue if the account is marked as 
							not managed or not expected.
*/
CREATE PROCEDURE [dbo].[UTILITY_ACCOUNTS_TO_SUPP_ACCOUNT_METER_MAP_INS]
    (
        @CONTRACT_ID INT
        , @ACCOUNT_ID INT
        , @METER_ID INT
        , @METER_ASSOCIATION_DT DATETIME
        , @METER_DISASSOCIATION_DT DATETIME
        , @IS_NEW_ACCOUNT BIT
        , @Supplier_Account_Config_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @site_id INT
            , @Not_Expected BIT
            , @Not_Managed BIT
            , @Not_Expected_By_Id INT;


        INSERT INTO dbo.SUPPLIER_ACCOUNT_METER_MAP
             (
                 Contract_ID
                 , ACCOUNT_ID
                 , METER_ID
                 , METER_ASSOCIATION_DATE
                 , METER_DISASSOCIATION_DATE
                 , Supplier_Account_Config_Id
             )
        VALUES
            (@CONTRACT_ID
             , @ACCOUNT_ID
             , @METER_ID
             , @METER_ASSOCIATION_DT
             , @METER_DISASSOCIATION_DT
             , @Supplier_Account_Config_Id);

        SELECT
            @site_id = ad.ADDRESS_PARENT_ID
        FROM
            dbo.METER m
            JOIN dbo.ADDRESS ad
                ON ad.ADDRESS_ID = m.ADDRESS_ID
        WHERE
            m.METER_ID = @METER_ID;

        SELECT
            @Not_Expected = acc.NOT_EXPECTED
            , @Not_Managed = acc.NOT_MANAGED
        FROM
            dbo.ACCOUNT acc
        WHERE
            acc.ACCOUNT_ID = @ACCOUNT_ID;

        IF @IS_NEW_ACCOUNT = 1
            BEGIN

                EXEC dbo.cbmsInvoiceParticipation_InsertSupplierAccount
                    16
                    , @ACCOUNT_ID
                    , @site_id;

                IF @Not_Managed = 1
                    BEGIN
                        EXEC cbmsInvoiceParticipationQueue_Save
                            93
                            , 7 -- Make account not managed      
                            , NULL
                            , NULL
                            , NULL
                            , @ACCOUNT_ID
                            , NULL
                            , 1;
                    END;

                IF @Not_Expected = 1
                    BEGIN
                        EXEC cbmsInvoiceParticipationQueue_Save
                            93
                            , 8 -- Make account not expected      
                            , NULL
                            , NULL
                            , NULL
                            , @ACCOUNT_ID
                            , NULL
                            , 1;
                    END;
            END;

    END;


    ;

GO


GRANT EXECUTE ON  [dbo].[UTILITY_ACCOUNTS_TO_SUPP_ACCOUNT_METER_MAP_INS] TO [CBMSApplication]
GO
