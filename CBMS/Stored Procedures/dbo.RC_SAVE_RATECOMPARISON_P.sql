SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.RC_SAVE_RATECOMPARISON_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@vendor_id     	int       	          	
	@rate_comparison_name	varchar(200)	          	
	@client_id     	int       	          	
	@site_id       	int       	          	
	@region_id     	int       	          	
	@state_id      	int       	          	
	@city          	varchar(20)	          	
	@rate_id       	int       	          	
	@created_date  	datetime  	          	
	@account_id    	int       	          	
	@meter_id      	int       	          	
	@start_date    	datetime  	          	
	@end_date      	datetime  	          	
	@compared_with 	varchar(4000)	          	
	@reportxml     	text      	          	
	@compariedRatesWithIds	varchar(4000)	          	
	@isbypass      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE          PROCEDURE dbo.RC_SAVE_RATECOMPARISON_P
@userId varchar(10),
@vendor_id int,
@rate_comparison_name varchar(200),
@client_id int,  
@site_id   int, 
@region_id int,
@state_id  int,
@city varchar(20),
@rate_id int,
@created_date datetime ,
@account_id int,
@meter_id int,
@start_date datetime,
@end_date datetime,
@compared_with varchar(4000),
@reportxml text,
@compariedRatesWithIds varchar(4000),
@isbypass int


as
set nocount on
declare @result int
declare @status int

select @result = entity_id from entity  where entity_type=700 and entity_name like 'Rate Comparison Document not set up'
select @status = entity_id from entity  where entity_type=701 and entity_name like 'Not Reviewed' 

insert into RC_RATE_COMPARISON 
(UTILITY_ID, CREATED_BY, RATE_COMPARISON_NAME, CLIENT_ID, SITE_ID, 
REGION_ID, STATE_ID, CITY, RATE_ID, CREATION_DATE,ACCOUNT_ID,
METER_ID,START_DATE,END_DATE,RESULT_TYPE_ID,STATUS_TYPE_ID,RATE_COMPARED_WITH,
RATE_COMPARISON_REPORT_XML, COMPARED_RATES_WITH_IDS,IS_RATE_COMPARISON_BYPASS)
values (
@vendor_id ,
@userId,
@rate_comparison_name ,
@client_id ,  
@site_id   , 
@region_id ,
@state_id  ,
@city ,
@rate_id ,
@created_date  ,
@account_id ,
@meter_id ,
@start_date ,
@end_date,
@result,
@status,
@compared_with,
@reportxml,
@compariedRatesWithIds,
@isbypass)
GO
GRANT EXECUTE ON  [dbo].[RC_SAVE_RATECOMPARISON_P] TO [CBMSApplication]
GO
