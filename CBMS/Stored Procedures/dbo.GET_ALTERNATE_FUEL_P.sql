SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE   PROCEDURE dbo.GET_ALTERNATE_FUEL_P
	@siteID int
	AS
	begin
		set nocount on

		select 	s.IS_ALTERNATE_POWER,
			s.IS_ALTERNATE_GAS,
			a.alternate_fuel_id,
			a.vendor_id,
			v.VENDOR_name,
			a.ALTERNATE_FUEL_TYPE_ID,
			e.ENTITY_NAME,
			a.STORAGE_CAPACITY,
			a.CONVERSION, 
			a.IS_REPORTED 
		from  	alternate_fuel a
			join site s on s.site_id = a.site_id
			and a.site_id = @siteID
			join vendor v on v.vendor_id  = a.vendor_id
			join entity e on e.entity_id = a.ALTERNATE_FUEL_TYPE_ID
			

	end
GO
GRANT EXECUTE ON  [dbo].[GET_ALTERNATE_FUEL_P] TO [CBMSApplication]
GO
