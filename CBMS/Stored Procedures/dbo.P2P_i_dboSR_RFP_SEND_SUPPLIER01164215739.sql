SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[P2P_i_dboSR_RFP_SEND_SUPPLIER01164215739]
		@c1 int,
		@c2 int,
		@c3 bit,
		@c4 int,
		@c5 int,
		@c6 datetime,
		@c7 int,
		@c8 int,
		@c9 uniqueidentifier,
		@c10 int,
		@c11 datetime
	,@MSp2pPostVersion varbinary(32) 
as
begin  
begin try
	insert into [dbo].[SR_RFP_SEND_SUPPLIER](
		[SR_RFP_SEND_SUPPLIER_ID],
		[SR_ACCOUNT_GROUP_ID],
		[IS_BID_GROUP],
		[SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID],
		[STATUS_TYPE_ID],
		[DUE_DATE],
		[SR_RFP_EMAIL_LOG_ID],
		[BID_STATUS_TYPE_ID],
		[msrepl_tran_version],
		[Time_Zone_Id],
		[Due_Dt_By_Timezone],
		$sys_p2p_cd_id
	) values (
    @c1,
    @c2,
    @c3,
    @c4,
    @c5,
    @c6,
    @c7,
    @c8,
    @c9,
    @c10,
    @c11,
		@MSp2pPostVersion	) 
end try
begin catch
if @@error in (2627, 2601) 
begin  
	declare @cur_version varbinary(32) 
		,@conflict_type int = 5
		,@conflict_type_txt nvarchar(20) = N'Insert-Insert'
		,@reason_code int = 1
		,@reason_text nvarchar(720) = NULL
		,@is_on_disk_winner bit = 0
		,@is_incoming_winner bit = 0
		,@peer_id_current_node int = NULL
		,@peer_id_incoming int
		,@tranid_incoming nvarchar(40)
		,@peer_id_on_disk int
		,@tranid_on_disk nvarchar(40)
	select @peer_id_incoming = sys.fn_replvarbintoint(@MSp2pPostVersion)
		,@tranid_incoming = sys.fn_replp2pversiontotranid(@MSp2pPostVersion)
	select @cur_version = $sys_p2p_cd_id from [dbo].[SR_RFP_SEND_SUPPLIER] 
where [SR_RFP_SEND_SUPPLIER_ID] = @c1
	if @@rowcount = 0  
			exec sys.sp_replrethrow
	else 
	begin  
		select @peer_id_on_disk = sys.fn_replvarbintoint(@cur_version)
			,@tranid_on_disk = sys.fn_replp2pversiontotranid(@cur_version)
		if(@peer_id_incoming > @peer_id_on_disk)
			set @is_incoming_winner = 1
		else
			set @is_on_disk_winner = 1
	end  
		select @peer_id_current_node = p.originator_id from syspublications p join sysarticles a on a.pubid = p.pubid where a.objid = object_id(N'[dbo].[SR_RFP_SEND_SUPPLIER]') and p.options & 0x1 = 0x1
	if (@peer_id_current_node is not null) 
	begin 
		if (@reason_text is NULL)
			if (@peer_id_incoming > @peer_id_on_disk)
			begin  
				select @reason_text = formatmessage(22825,@peer_id_incoming,@peer_id_on_disk,@peer_id_current_node)
			end  
			else  
			begin  
				select @reason_text = formatmessage(22824,@peer_id_incoming,@peer_id_on_disk,@peer_id_current_node)
			end  
		create table #change_id (change_id varbinary(8))
		insert [dbo].[conflict_dbo_SR_RFP_SEND_SUPPLIER] (
		[SR_RFP_SEND_SUPPLIER_ID],
		[SR_ACCOUNT_GROUP_ID],
		[IS_BID_GROUP],
		[SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID],
		[STATUS_TYPE_ID],
		[DUE_DATE],
		[SR_RFP_EMAIL_LOG_ID],
		[BID_STATUS_TYPE_ID],
		[msrepl_tran_version],
		[Time_Zone_Id],
		[Due_Dt_By_Timezone]
			,__$originator_id
			,__$origin_datasource
			,__$tranid
			,__$conflict_type
			,__$is_winner
			,__$reason_code
			,__$reason_text
			,__$update_bitmap
			,__$pre_version)
		output inserted.__$row_id into #change_id
		select 
    @c1,
    @c2,
    @c3,
    @c4,
    @c5,
    @c6,
    @c7,
    @c8,
    @c9,
    @c10,
    @c11			,@peer_id_current_node
			,@peer_id_incoming
			,@tranid_incoming
			,@conflict_type
			,@is_incoming_winner
			,@reason_code
			,@reason_text
			,NULL
			,NULL
		insert [dbo].[conflict_dbo_SR_RFP_SEND_SUPPLIER] (

		[SR_RFP_SEND_SUPPLIER_ID],
		[SR_ACCOUNT_GROUP_ID],
		[IS_BID_GROUP],
		[SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID],
		[STATUS_TYPE_ID],
		[DUE_DATE],
		[SR_RFP_EMAIL_LOG_ID],
		[BID_STATUS_TYPE_ID],
		[msrepl_tran_version],
		[Time_Zone_Id],
		[Due_Dt_By_Timezone]			,__$originator_id
			,__$origin_datasource
			,__$tranid
			,__$conflict_type
			,__$is_winner
			,__$reason_code
			,__$reason_text
			,__$pre_version
			,__$change_id)
		select 

		[SR_RFP_SEND_SUPPLIER_ID],
		[SR_ACCOUNT_GROUP_ID],
		[IS_BID_GROUP],
		[SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID],
		[STATUS_TYPE_ID],
		[DUE_DATE],
		[SR_RFP_EMAIL_LOG_ID],
		[BID_STATUS_TYPE_ID],
		[msrepl_tran_version],
		[Time_Zone_Id],
		[Due_Dt_By_Timezone]			,@peer_id_current_node
			,@peer_id_on_disk
			,@tranid_on_disk
			,@conflict_type
			,@is_on_disk_winner
			,@reason_code
			,@reason_text
			,NULL
			,(select change_id from #change_id)
		from [dbo].[SR_RFP_SEND_SUPPLIER] 

where [SR_RFP_SEND_SUPPLIER_ID] = @c1
	end 
	if(@peer_id_incoming > @peer_id_on_disk)
	begin  
update [dbo].[SR_RFP_SEND_SUPPLIER] set
		[SR_ACCOUNT_GROUP_ID] = @c2,
		[IS_BID_GROUP] = @c3,
		[SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID] = @c4,
		[STATUS_TYPE_ID] = @c5,
		[DUE_DATE] = @c6,
		[SR_RFP_EMAIL_LOG_ID] = @c7,
		[BID_STATUS_TYPE_ID] = @c8,
		[msrepl_tran_version] = @c9,
		[Time_Zone_Id] = @c10,
		[Due_Dt_By_Timezone] = @c11		,$sys_p2p_cd_id = @MSp2pPostVersion

where [SR_RFP_SEND_SUPPLIER_ID] = @c1
	end  
		if exists(select * from syspublications p join sysarticles a on a.pubid = p.pubid where a.objid = object_id(N'[dbo].[SR_RFP_SEND_SUPPLIER]') and p.options & 0x10 = 0x10)
			raiserror(22815, 10, -1, @conflict_type_txt, @peer_id_current_node, @peer_id_incoming, @tranid_incoming, @peer_id_on_disk, @tranid_on_disk) with log
		else
			raiserror(22815, 16, -1, @conflict_type_txt, @peer_id_current_node, @peer_id_incoming, @tranid_incoming, @peer_id_on_disk, @tranid_on_disk) with log
end 
else  
	EXEC sys.sp_replrethrow
end catch 
end  
GO
