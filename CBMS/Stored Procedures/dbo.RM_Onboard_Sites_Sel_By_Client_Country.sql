SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                 
/******                        
 NAME: dbo.RM_Onboard_Sites_Sel_By_Client_Country            
                        
 DESCRIPTION:                        
			To Get RM On Board Sites based on Client    
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@clientId					INT
@Start_Index				INT					0
@End_Index					INT				 2147483647    
@Country_Name			VARCHAR(200)		   NULL               
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 
   	exec RM_Onboard_Sites_Sel_By_Client_Country 235,1,25,'USA'
	exec RM_Onboard_Sites_Sel_By_Client_Country 147,1,25,'USA'
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2014-11-07       Created                
                       
******/    

CREATE PROCEDURE [dbo].[RM_Onboard_Sites_Sel_By_Client_Country]
      @clientId INT
     ,@Start_Index INT = 0
     ,@End_Index INT = 2147483647
     ,@Country_Name VARCHAR(200) = NULL
AS 
SET nocount ON;  
   
WITH  RM_Onboard_US_Sites_Pagination
        AS ( SELECT
                  ch.Site_Id
                 ,RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' Site_Name
                 ,ch.Site_Reference_Number
                 ,COUNT(1) OVER ( ) AS total
                 ,ROW_NUMBER() OVER ( ORDER BY ch.city, ch.state_name, ch.site_name ) AS RowNum
             FROM
                  core.Client_Hier ch
                  INNER JOIN dbo.RM_ONBOARD_HEDGE_SETUP hedge
                        ON hedge.SITE_ID = ch.SITE_ID
             WHERE
                  ch.Client_Id = @clientId
                  AND ( @Country_Name IS NULL
                        OR ch.Country_Name = @Country_Name )
                  AND ch.Site_Closed = 0
                  AND ch.Site_Not_Managed = 0
                  AND hedge.SITE_ID = ch.SITE_ID
                  AND hedge.INCLUDE_IN_REPORTS = 1)
      SELECT
            SITE_ID
           ,SITE_NAME
           ,SITE_REFERENCE_NUMBER
           ,total
           ,rowNum
      FROM
            RM_Onboard_US_Sites_Pagination
      WHERE
            rowNum BETWEEN @Start_Index AND @End_Index
      ORDER BY
            rowNum
;
GO
GRANT EXECUTE ON  [dbo].[RM_Onboard_Sites_Sel_By_Client_Country] TO [CBMSApplication]
GO
