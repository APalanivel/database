SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Invoice_Collection_Chase_History_Exists_For_Invoice_Collection_Account_Config_Id       
              
Description:              
			This sproc is to check whether the account config or invoice changes are updated or not    
                           
 Input Parameters:              
    Name									DataType								Default			Description                
-----------------------------------------------------------------------------------------------------------------                
	
    @Invoice_Collection_Account_Config_Id	INT
    
 Output Parameters:                    
    Name								DataType			Default			Description                
-----------------------------------------------------------------------------------------------------------------                
              
 Usage Examples:                  
-----------------------------------------------------------------------------------------------------------------                


 EXEC dbo.Invoice_Collection_Chase_History_Exists_For_Invoice_Collection_Account_Config_Id 
      @Invoice_Collection_Account_Config_Id = 303
     
              
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2017-03-14		Created For Invoice_Collection.  
	RKV             2019-01-04      MAINT-8063,  Eliminate the Archive records from ICQ history check(Chase/Issue).       
             
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Chase_History_Exists_For_Invoice_Collection_Account_Config_Id]
     (
         @Invoice_Collection_Account_Config_Id INT
     )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Is_Chase_History_Exists BIT = 0
            , @Archived_ICR_Status_Cd INT
			 ,@Archived_ICE_Status_Cd INT;

        SELECT
            @Archived_ICR_Status_Cd = c.Code_Id
        FROM
            Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            c.Code_Value = 'Archived'
            AND cs.Codeset_Name = 'ICR Status';

		 SELECT
           @Archived_ICE_Status_Cd =  c.Code_Id
        FROM
            Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            c.Code_Value = 'Archived'
            AND cs.Codeset_Name = 'ICE Status'


        SELECT
            @Is_Chase_History_Exists = 1
        FROM
            dbo.Invoice_Collection_Chase_Log iccl
            INNER JOIN dbo.Invoice_Collection_Chase_Log_Queue_Map icclqm
                ON iccl.Invoice_Collection_Chase_Log_Id = icclqm.Invoice_Collection_Chase_Log_Id
            INNER JOIN dbo.Invoice_Collection_Queue icq
                ON icclqm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN dbo.Code sc
                ON sc.Code_Id = iccl.Status_Cd
        WHERE
            sc.Code_Value = 'Close'
            AND icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
			AND icq.Status_Cd not in (@Archived_ICE_Status_Cd,@Archived_ICR_Status_Cd)
            AND @Is_Chase_History_Exists = 0;


        SELECT
            @Is_Chase_History_Exists = 1
        FROM
            dbo.Invoice_Collection_Issue_Log icil
            INNER JOIN dbo.Invoice_Collection_Queue icq
                ON icil.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN dbo.Code isc
                ON isc.Code_Id = icil.Issue_Status_Cd
        WHERE
            icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
			AND icq.Status_Cd not in (@Archived_ICE_Status_Cd,@Archived_ICR_Status_Cd)
            AND @Is_Chase_History_Exists = 0;

        SELECT
            @Is_Chase_History_Exists = 1
        FROM
            dbo.Invoice_Collection_Exception_Comment icec
            INNER JOIN dbo.Invoice_Collection_Queue icq
                ON icec.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
        WHERE
            icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
            AND @Is_Chase_History_Exists = 0;


        SELECT
            @Is_Chase_History_Exists = 1
        FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN dbo.Code icqtc
                ON icqtc.Code_Id = icq.Invoice_Collection_Queue_Type_Cd
            INNER JOIN dbo.Code sc
                ON sc.Code_Id = icq.Status_Cd
        WHERE
            icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
            AND icqtc.Code_Value = 'ICR'
            AND sc.Code_Value = 'Received'
            AND @Is_Chase_History_Exists = 0;

        SELECT
            @Is_Chase_History_Exists = 1
        FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN dbo.Code icqtc
                ON icqtc.Code_Id = icq.Invoice_Collection_Queue_Type_Cd
            INNER JOIN dbo.Code sc
                ON sc.Code_Id = icq.Status_Cd
        WHERE
            icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
            AND icqtc.Code_Value = 'ICE'
            AND sc.Code_Value = 'Processed'
            AND @Is_Chase_History_Exists = 0;

        SELECT  @Is_Chase_History_Exists Is_Chase_History_Exists;

    END;

GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Chase_History_Exists_For_Invoice_Collection_Account_Config_Id] TO [CBMSApplication]
GO
