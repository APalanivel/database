SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                                                              
NAME:  Budget.Budget_Commodity_Calculation_Level_Merge                                                                      
                                                                              
DESCRIPTION: To Update Budget Calculation levels Account or Meter.                                                                              
             Used at:-Client_Info-->Search Clinets --> Manage Clients --> Manage Sites --> Manage Utility Accounts --> Edit Utility Account                                                                 
                                                                              
INPUT PARAMETERS:                                                                              
           Name         DataType  Default Description                                                                              
------------------------------------------------------------                                                                              
 @Account_Id     INT          
 @Commodity_Budget_Level_Cd VARCHAR(MAX)          
    @User_Info_Id    INT              
                                                        
OUTPUT PARAMETERS:                                                                              
 Name   DataType  Default Description                                                                              
------------------------------------------------------------                                                                              
                                                                              
USAGE EXAMPLES:                                                                              
------------------------------------------------------------          
Begin Tran                                                                              
EXEC Budget.Budget_Commodity_Calculation_Level_Merge           
 @Account_Id=206          
 ,@Commodity_Budget_Level_Cd ='290|103116,291|103117'                   
 ,@User_Info_Id   =49              
  
  EXEC Budget.Budget_Commodity_Calculation_Level_Merge           
 @Account_Id=204          
 ,@Commodity_Budget_Level_Cd ='290|103116,291|103117'                   
 ,@User_Info_Id   =49  
         
Rollback Tran          
                                          
AUTHOR INITIALS:                                                                              
 Initials Name                                                                              
------------------------------------------------------------                                                                              
SC   Sreenivasulu Cheerala                                                        
                                                                              
MODIFICATIONS                                                                              
 Initials Date  Modification                                                                              
------------------------------------------------------------                                                                              
 SC   2019-10-30   Created   for Budget 2.0                                                             
******/
CREATE PROCEDURE [Budget].[Budget_Commodity_Calculation_Level_Merge]
    (
        @Account_Id INT
        , @Commodity_Budget_Level_Cd VARCHAR(MAX)
        , @User_Info_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Commodity_Budget_Level TABLE
              (
                  Commodity_ID INT
                  , Budget_Level_Cd INT
              );

        INSERT INTO @Commodity_Budget_Level
             (
                 Commodity_ID
                 , Budget_Level_Cd
             )
        SELECT
            CAST(SUBSTRING(us.Segments, 1, CHARINDEX('|', us.Segments) - 1) AS INT)
            , SUBSTRING(us.Segments, CHARINDEX('|', us.Segments) + 1, LEN(us.Segments))
        FROM
            dbo.ufn_split(@Commodity_Budget_Level_Cd, ',') us;


        MERGE Budget.Account_Commodity_Budget_Calc_Level AS Tgt
        USING @Commodity_Budget_Level AS Src
        ON Tgt.[Account_Id] = @Account_Id
           AND  Tgt.[Commodity_Id] = Src.[Commodity_ID]
        WHEN MATCHED THEN UPDATE SET
                              Tgt.Budget_Calc_Level_Cd = Src.Budget_Level_Cd
                              , Tgt.Last_Change_Ts = GETDATE()
                              , Tgt.Updated_User_Id = @User_Info_Id
        WHEN NOT MATCHED THEN INSERT (Account_Id
                                      , Commodity_Id
                                      , Budget_Calc_Level_Cd
                                      , Created_User_Id
                                      , Created_Ts
                                      , Updated_User_Id
                                      , Last_Change_Ts)
                              VALUES
                                  (@Account_Id
                                   , Src.Commodity_ID
                                   , Src.Budget_Level_Cd
                                   , @User_Info_Id
                                   , GETDATE()
                                   , @User_Info_Id
                                   , GETDATE());
    END;
GO
GRANT EXECUTE ON  [Budget].[Budget_Commodity_Calculation_Level_Merge] TO [CBMSApplication]
GO
