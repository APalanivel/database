SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******

NAME: dbo.Make_Utility_Account_Expected_By_Account_id
  
DESCRIPTION:

	Used to update the account details and make the account expected.

	Making an accoung managed has to do following activities as well
		- Update the invoice participation as Managed	(Procedure cbmsinvoiceparticipationqueue_save will do this part)
		- Update other information of account table.
		
INPUT PARAMETERS:      
	Name										DataType          Default     Description      
-----------------------------------------------------------------------------------------------------      
	@Vendor_Id									INTEGER
	@Site_Id									INTEGER
	@Invoice_Src_Type_Id						INTEGER
	@Account_Number								VARCHAR(50)
	@Account_Id									INTEGER
	@User_Info_Id								INTEGER
	@Ubm_Account_Code							VARCHAR(200)
	@Service_Level_Id							INT
	@Eligibility_Dt								DATETIME
	@Watch_List_Group_Info_Id					INT
	@Is_Data_Entry_Only							INT
	@Is_Level2_Recalc_Validation_Required		BIT

OUTPUT PARAMETERS:
	Name										DataType          Default     Description      
-----------------------------------------------------------------------------------------------------      


USAGE EXAMPLES:
-----------------------------------------------------------------------------------------------------      

	BEGIN TRAN
		EXEC dbo.Make_Utility_Account_Expected_By_Account_id
			@Vendor_Id									= 28
			,@Site_Id									= 542
			,@Invoice_Src_Type_Id						= 33
			,@Account_Number							= '9743399036'
			,@Account_Id								= 784
			,@User_Info_Id								= 15
			,@Ubm_Account_Code							= NULL
			,@Service_Level_Id							= 859
			,@Eligibility_Dt							= NULL
			
			,@Is_Data_Entry_Only						= 0
			
	ROLLBACK TRAN
	
	
AUTHOR INITIALS:
 Initials	Name
-----------------------------------------------------------------------------------------------------      
  PNR		PANDARINATH
  SP		Sandeep Pigilam
  RKV		Ravi Kumar Vegesna
  NR		Narayana Reddy


MODIFICATIONS

 Initials	Date			Modification
-----------------------------------------------------------------------------------------------------      
   PNR		06/15/2010		Created
   SP		2014-07-18		 Data Operations Enhancements Phase III added Is_Level2_Recalc_Validation_Required Column in Update
   RKV      2015-09-21		Removed the parameter @Is_Level2_Recalc_Validation_Required as part of AS400-II 		
   NR		2017-03-07		Contract Place holder  - Removed  @Is_Consolidated_Billing_Posted_To_Utility input parameter.  
   NR		2019-04-03		Data2.0 - Removed WATCHLIST-GROUP-INFO-ID  column from Account Table.	
   RKV		2019-12-30	D20-1762  Removed UBM Parameters 	

******/

CREATE PROCEDURE [dbo].[Make_Utility_Account_Expected_By_Account_id]
     (
         @Vendor_Id INTEGER
         , @Site_Id INTEGER
         , @Invoice_Src_Type_Id INTEGER
         , @Account_Number VARCHAR(50)
         , @Account_Id INTEGER
         , @User_Info_Id INTEGER
         , @Service_Level_Id INT
         , @Eligibility_Dt DATETIME
         , @Is_Data_Entry_Only INT
     )
AS
    BEGIN

        SET NOCOUNT ON;

        BEGIN TRY
            BEGIN TRAN;

            EXEC dbo.cbmsInvoiceParticipationQueue_Save
                @User_Info_Id
                , 18
                , NULL
                , NULL
                , NULL
                , @Account_Id
                , NULL
                , 1;

            UPDATE
                dbo.ACCOUNT
            SET
                VENDOR_ID = @Vendor_Id
                , SITE_ID = @Site_Id
                , INVOICE_SOURCE_TYPE_ID = @Invoice_Src_Type_Id
                , ACCOUNT_NUMBER = @Account_Number
                , NOT_EXPECTED = 0
                , NOT_EXPECTED_BY_ID = NULL
                , NOT_EXPECTED_DATE = NULL
                , USER_INFO_ID = @User_Info_Id
                , SERVICE_LEVEL_TYPE_ID = @Service_Level_Id
                , ELIGIBILITY_DATE = @Eligibility_Dt
                , Is_Data_Entry_Only = @Is_Data_Entry_Only
            WHERE
                ACCOUNT_ID = @Account_Id;

            

            COMMIT TRAN;
        END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRAN;
                END;

            EXEC dbo.usp_RethrowError;

        END CATCH;
    END;
    ;

    ;

GO



GRANT EXECUTE ON  [dbo].[Make_Utility_Account_Expected_By_Account_id] TO [CBMSApplication]
GO
