SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Utility_Dtl_Election_Upd

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	 @Utility_Dtl_Election_Id	INT
     @Election_Name				VARCHAR(100)
     @Enrollment_Period			VARCHAR(500) = NULL
     @Notification_Deadline		VARCHAR(500) = NULL
     @Other_Stipulation			VARCHAR(500) = NULL
     @Comment_ID				INT = NULL

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC Utility_Dtl_Election_Upd 1,'Election_Name TEST','Enrollment_Period TEST','Notification_Deadline TEST','Other_Stipulation TEST'
	
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	SKA			01/12/2011	Created
******/

CREATE PROC dbo.Utility_Dtl_Election_Upd
      (
       @Utility_Dtl_Election_Id INT
      ,@Election_Name VARCHAR(100)
      ,@Enrollment_Period VARCHAR(500) = NULL
      ,@Notification_Deadline VARCHAR(500) = NULL
      ,@Other_Stipulation VARCHAR(500) = NULL
      ,@Comment_ID INT = NULL )
AS 
BEGIN
 
      SET nocount ON ;

      UPDATE
            Utility_Dtl_Election
      SET   
            @Election_Name = @Election_Name
           ,Enrollment_Period = @Enrollment_Period
           ,Notification_Deadline = @Notification_Deadline
           ,Other_Stipulation = @Other_Stipulation
           ,Comment_ID = @Comment_ID
      WHERE
            Utility_Dtl_Election_Id = @Utility_Dtl_Election_Id
END                        
GO
GRANT EXECUTE ON  [dbo].[Utility_Dtl_Election_Upd] TO [CBMSApplication]
GO
