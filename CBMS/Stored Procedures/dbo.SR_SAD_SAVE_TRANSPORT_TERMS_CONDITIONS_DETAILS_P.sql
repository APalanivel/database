SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO

CREATE   PROCEDURE dbo.SR_SAD_SAVE_TRANSPORT_TERMS_CONDITIONS_DETAILS_P
@userId varchar(10),
@sessionId varchar(20),
@srDealTicketId int,
@transportationTypeId int,
@supplyTypeId int,
@inclusiveOfFuelTypeId int,
@srDtDeliveryPointId int

AS
	set nocount on
IF (select count(1) from SR_DT_TRANSPORT_TERMS_CONDITIONS where SR_DEAL_TICKET_ID = @srDealTicketId) = 0

BEGIN


	insert into SR_DT_TRANSPORT_TERMS_CONDITIONS
		(SR_DEAL_TICKET_ID, TRANSPORTATION_TYPE_ID, SUPPLY_TYPE_ID, 
		INCLUSIVE_OF_FUEL_TYPE_ID, SR_DT_DELIVERY_POINT_ID)
	
	
	values	(@srDealTicketId, @transportationTypeId, @supplyTypeId, @inclusiveOfFuelTypeId, @srDtDeliveryPointId)


END

ELSE

BEGIN


	update SR_DT_TRANSPORT_TERMS_CONDITIONS set TRANSPORTATION_TYPE_ID=@transportationTypeId,
		SUPPLY_TYPE_ID=@supplyTypeId, INCLUSIVE_OF_FUEL_TYPE_ID=@inclusiveOfFuelTypeId,
		SR_DT_DELIVERY_POINT_ID=@srDtDeliveryPointId

	where
		SR_DEAL_TICKET_ID=@srDealTicketId
END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_SAVE_TRANSPORT_TERMS_CONDITIONS_DETAILS_P] TO [CBMSApplication]
GO
