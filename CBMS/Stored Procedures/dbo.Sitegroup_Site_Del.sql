SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Sitegroup_Site_Del]  

DESCRIPTION: It Deletes Site Group Site for Given Site Id,Site Group Id.
      
INPUT PARAMETERS:          
	NAME				DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Sitegroup_Id		INT
    @Site_Id			INT
    
    
OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  BEGIN TRAN
	   EXEC dbo.Sitegroup_Site_Del 105,27
  ROLLBACK TRAN

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			28-JUN-10	CREATED

*/

CREATE PROCEDURE dbo.Sitegroup_Site_Del
    (
		 @Sitegroup_Id	INT
		,@Site_Id		INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.Sitegroup_Site 
	WHERE
		Site_Id = @Site_Id
		AND Sitegroup_Id = @Sitegroup_Id
		
END
GO
GRANT EXECUTE ON  [dbo].[Sitegroup_Site_Del] TO [CBMSApplication]
GO
