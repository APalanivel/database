SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.UPDATE_SITE_NOT_MANAGED_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@userId           varchar(10),
@sessionId        varchar(20),
@siteId           int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- Run Test update  and check the values at sv
--exec dbo.UPDATE_SITE_NOT_MANAGED_P
--@userId =1,
--@sessionId = -1,
--@siteId = 27

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.UPDATE_SITE_NOT_MANAGED_P

@userId varchar(10),
@sessionId varchar(20),
@siteId int
as
	
set nocount on

	update account set not_managed=1,not_expected=1,not_expected_date=getdate() where site_id in 
	(@siteId)


	update account set not_managed=1,not_expected=1,not_expected_date=getdate() where account_id in (
		select distinct suppacc.ACCOUNT_ID  from account utilacc, account suppacc, 
		 vendor ven, contract con, 	   meter met,supplier_account_meter_map map 
	   	where map.meter_id = met.meter_id 
		   and met.account_id = utilacc.account_id 
		   and utilacc.site_id = @siteId 
		   and map.account_id = suppacc.account_id )
GO
GRANT EXECUTE ON  [dbo].[UPDATE_SITE_NOT_MANAGED_P] TO [CBMSApplication]
GO
