
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.Invoice_Participation_Sel_By_Site_Commodity_Service_Month

DESCRIPTION:

	Used to select the Invoice participation data for the given Site, commodity and service month

INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@Client_Hier_Id	Int
	@Commodity_Id		Int
	@Service_Month		Date

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Invoice_Participation_Sel_By_Site_Commodity_Service_Month 77181,290,'2010-05-01'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG		Harihara Suthan G
	AP		Athmaram Pabbathi

MODIFICATIONS
    Initials	Date		Modification
------------------------------------------------------------
    HG      	03/09/2010	Created
    SKA		04/30/2010	Removed cu_invoice_account_map(Account_id) with cu_invoice_service_month(Account_id)
    SSR		07/09/2010  Removed the logic of fetching data from SAMM table
					    (( SAMM.meter_disassociation_date > Account.Supplier_Account_Begin_Dt  
						    OR SAMM.meter_disassociation_date IS NULL ))
					    (After contract enhancement application is populating both Meter_association_date and meter_disassociation_Date column supplier_Account_meter_map and removing the entries when ever is_history = 1)
	AP		03/20/2012   Following changes made during Addnl Data
						  -- Replaced @Site_Id parameter with @Client_Hier_Id
						  -- Removed all base tables and used Client_Hier & Client_Hier_Account tables
						  -- Removed Account_Type_Id from return result
******/

CREATE PROCEDURE dbo.Invoice_Participation_Sel_By_Site_Commodity_Service_Month
      @Client_Hier_Id INT
     ,@Commodity_Id INT
     ,@Service_Month DATE
AS 
BEGIN

      SET NOCOUNT ON ;
      DECLARE @Site_Id INT

      SELECT
            @Site_Id = ch.Site_Id
      FROM
            Core.Client_Hier ch
      WHERE
            ch.Client_Hier_Id = @Client_Hier_Id

      SELECT
            cha.Account_Id
           ,cha.Account_Number
           ,cha.Account_Type
           ,cha.Vendor_Id
           ,cha.Vendor_Name
           ,ip.Is_Expected
           ,ip.Is_Received
           ,ip.Recalc_Under_Review
           ,ip.Variance_Under_Review
           ,i.Ubm_Id
           ,i.Cbms_Image_Id
           ,i.Is_Processed AS Is_Posted
      FROM
            ( SELECT
                  @Site_Id AS Site_Id
                 ,cha.Client_Hier_Id
                 ,cha.Account_Id
                 ,isnull(cha.Account_Number, 'No Account Number') AS Account_Number
                 ,cha.Account_Type
                 ,cha.Account_Vendor_Id AS Vendor_Id
                 ,cha.Account_Vendor_Name AS Vendor_Name
              FROM
                  Core.Client_Hier_Account cha
              WHERE
                  cha.Client_Hier_Id = @Client_Hier_Id
                  AND cha.Commodity_Id = @Commodity_Id
              GROUP BY
                  cha.Client_Hier_Id
                 ,cha.Account_Id
                 ,isnull(cha.Account_Number, 'No Account Number')
                 ,cha.Account_Type
                 ,cha.Account_Vendor_Id
                 ,cha.Account_Vendor_Name ) cha
            INNER JOIN dbo.Invoice_Participation ip
                  ON ip.Account_Id = cha.Account_Id
                     AND ip.Site_Id = cha.Site_Id
            LEFT OUTER JOIN ( dbo.Cu_Invoice i
                              INNER JOIN dbo.CU_Invoice_Service_Month sm
                                    ON sm.CU_Invoice_Id = i.CU_Invoice_Id
                                       AND sm.Service_Month = @Service_Month )
                              ON sm.account_id = cha.account_id
                                 AND i.is_reported = 1
      WHERE
            ip.Service_Month = @Service_Month
            AND ip.Is_Expected = 1
      ORDER BY
            cha.Account_Type DESC
           ,cha.Account_Number ASC
           ,cha.Vendor_Name ;

END
;
GO

GRANT EXECUTE ON  [dbo].[Invoice_Participation_Sel_By_Site_Commodity_Service_Month] TO [CBMSApplication]
GO
