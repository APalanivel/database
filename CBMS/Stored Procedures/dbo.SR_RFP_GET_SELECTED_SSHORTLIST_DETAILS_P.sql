SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_SELECTED_SSHORTLIST_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@accountGroupId	int       	          	
	@isBidGroup    	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE     PROCEDURE DBO.SR_RFP_GET_SELECTED_SSHORTLIST_DETAILS_P

@accountGroupId int,
@isBidGroup int


AS
set nocount on
select 	shortListDetails.SR_RFP_SELECTED_PRODUCTS_ID AS shortListProductId,
	shortListDetails.SR_RFP_ACCOUNT_TERM_ID AS shortListAccountTermId,	
	shortListDetails.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID AS shortListSuppContVendorMapId

from	SR_RFP_SOP_SHORTLIST_DETAILS shortListDetails,
	SR_RFP_SOP_SHORTLIST shortList
where 	
	shortList.SR_ACCOUNT_GROUP_ID = @accountGroupId	
	AND shortList.IS_BID_GROUP=  @isBidGroup 
	AND shortList.SR_RFP_SOP_SHORTLIST_ID = shortListDetails.SR_RFP_SOP_SHORTLIST_ID
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_SELECTED_SSHORTLIST_DETAILS_P] TO [CBMSApplication]
GO
