SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
NAME:              
 CBMS.dbo.EC_Meter_Attribute_Tracking_For_Excel_Upd              
              
DESCRIPTION:              
              
INPUT PARAMETERS:              
 Name     DataType  Default Description              
---------------------------------------------------------------              
 @EC_Meter_Attribute_Tracking_Id INT  
, @EC_Meter_Attribute_Id INT  
, @EC_Meter_Attribute_Value NVARCHAR(255)  
, @Start_Dt DATE  
, @End_Dt DATE  
, @User_Info_Id INT  
, @Meter_Attribute_Type_Value NVARCHAR(255)  
, @Vendor_Type_Value NVARCHAR(255) = NULL              
              
OUTPUT PARAMETERS:              
 Name     DataType  Default Description              
------------------------------------------------------------              
               
USAGE EXAMPLES:              
------------------------------------------------------------              
              
 SELECT * FROM dbo.EC_Meter_Attribute              
 SELECT TOP 10 * FROM dbo.METER              
 BEGIN TRANSACTION              
  DECLARE @Tracking_Id INT              
  SELECT * FROM dbo.EC_Meter_Attribute_Tracking WHERE Meter_Id = 1              
  EXEC dbo.EC_Meter_Attribute_Tracking_Ins 1, 2, '2017-01-01', '2017-12-31', 'Test_1_Test', 16              
  SELECT @Tracking_Id = EC_Meter_Attribute_Tracking_Id FROM dbo.EC_Meter_Attribute_Tracking WHERE Meter_Id = 1               
    AND EC_Meter_Attribute_Id = 2 AND EC_Meter_Attribute_Value = 'Test_1_Test'              
  SELECT * FROM dbo.EC_Meter_Attribute_Tracking WHERE Meter_Id = 1              
     --AND EC_Meter_Attribute_Id = 2 AND EC_Meter_Attribute_Value = 'Test_1_Test'              
  EXEC dbo.EC_Meter_Attribute_Tracking_Upd @Tracking_Id, 2, 'Test_2_Test', '2017-01-01', '2017-12-31', 16              
  SELECT * FROM dbo.EC_Meter_Attribute_Tracking WHERE EC_Meter_Attribute_Tracking_Id = @Tracking_Id              
 ROLLBACK TRANSACTION              
              
              
AUTHOR INITIALS:              
 Initials Name              
------------------------------------------------------------              
SLP	 Sri Lakshmi Pallikonda         
MODIFICATIONS              
 Initials Date  Modification              
------------------------------------------------------------              
 SLP  2020-01-17 Created for bulk Meter Attributes          
******/


CREATE PROCEDURE [dbo].[EC_Meter_Attribute_Tracking_For_Excel_Upd]
    (
        @EC_Meter_Attribute_Tracking_Id INT
        , @EC_Meter_Attribute_Id INT
        , @EC_Meter_Attribute_Value NVARCHAR(400)
        , @Start_Dt DATE
        , @End_Dt DATE
        , @User_Info_Id INT
        , @Meter_Attribute_Type_Value NVARCHAR(400)
        , @Vendor_Type_Value VARCHAR(25) = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Account_Id INT
            , @Meter_ID INT
            , @New_Exception_Status_Cd INT
            , @Closed_Exception_Status_Cd INT
            , @Missing_Meter_Attribute_Exception_Type_Cd INT
            , @Actual_Attribute_Type_Cd INT
            , @Vendor_Type_Cd INT
            , @Meter_Attribute_Type_Cd INT;


        SELECT
            @Actual_Attribute_Type_Cd = cd.Code_Id
        FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cd.Code_Value = 'Actual'
            AND cs.Codeset_Name = 'MeterAttributeType';

        SELECT
            @New_Exception_Status_Cd = cd.Code_Id
        FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Status'
            AND cd.Code_Value = 'New';



        SELECT
            @Vendor_Type_Cd = cd.Code_Id
        FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cd.Code_Value = @Vendor_Type_Value
            AND cs.Codeset_Name = 'VendorType';


        SELECT
            @Meter_Attribute_Type_Cd = cd.Code_Id
        FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cd.Code_Value = @Meter_Attribute_Type_Value
            AND cs.Codeset_Name = 'MeterAttributeType';

        SELECT
            @Closed_Exception_Status_Cd = cd.Code_Id
        FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Status'
            AND cd.Code_Value = 'Closed';
        SELECT
            @Missing_Meter_Attribute_Exception_Type_Cd = C.Code_Id
        FROM
            dbo.Code C
            INNER JOIN dbo.Codeset CS
                ON C.Codeset_Id = CS.Codeset_Id
        WHERE
            CS.Codeset_Name = 'Exception Type'
            AND C.Code_Value = 'Missing Meter Attribute';
        SELECT
            @Account_Id = cha.Account_Id
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN dbo.EC_Meter_Attribute_Tracking emat
                ON cha.Meter_Id = emat.Meter_Id
        WHERE
            cha.Account_Type = 'Utility'
            AND EC_Meter_Attribute_Tracking_Id = @EC_Meter_Attribute_Tracking_Id;
        SELECT
            @Meter_ID = emat.Meter_Id
        FROM
            dbo.EC_Meter_Attribute_Tracking emat
        WHERE
            emat.EC_Meter_Attribute_Tracking_Id = @EC_Meter_Attribute_Tracking_Id;

        EXEC dbo.ADD_ENTITY_AUDIT_ITEM_P
            @entity_identifier = @Account_Id
            , @user_info_id = @User_Info_Id
            , @audit_type = 2
            , @entity_name = 'ACCOUNT_TABLE'
            , @entity_type = 500;

        UPDATE
            mat
        SET
            EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id
            , Start_Dt = @Start_Dt
            , End_Dt = @End_Dt
            , EC_Meter_Attribute_Value = @EC_Meter_Attribute_Value
            , Updated_User_Id = @User_Info_Id
            , Last_Change_Ts = GETDATE()
            , mat.Meter_Attribute_Type_Cd = @Meter_Attribute_Type_Cd
        FROM
            dbo.EC_Meter_Attribute_Tracking mat
        WHERE
            mat.EC_Meter_Attribute_Tracking_Id = @EC_Meter_Attribute_Tracking_Id;


        ---Close Missing Meter Attribute if at least one Actual Meter attribute exists.        
        IF EXISTS (   SELECT
                            1
                      FROM
                            dbo.Account_Exception ae
                      WHERE
                            ae.Meter_Id = @Meter_ID
                            AND ae.Exception_Status_Cd = @New_Exception_Status_Cd
                            AND ae.Exception_Type_Cd = @Missing_Meter_Attribute_Exception_Type_Cd
                            AND ae.Closed_By_User_Id IS NULL
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.EC_Meter_Attribute_Tracking emat
                                           WHERE
                                                emat.Meter_Attribute_Type_Cd = @Actual_Attribute_Type_Cd
                                                AND emat.Meter_Id = @Meter_ID))
            BEGIN
                UPDATE
                    ae
                SET
                    ae.Exception_Status_Cd = @Closed_Exception_Status_Cd
                    , ae.Closed_By_User_Id = @User_Info_Id
                    , ae.Closed_Ts = GETDATE()
                    , ae.Last_Change_Ts = GETDATE()
                    , ae.Updated_User_Id = @User_Info_Id
                FROM
                    dbo.Account_Exception ae
                WHERE
                    ae.Meter_Id = @Meter_ID
                    AND ae.Exception_Status_Cd = @New_Exception_Status_Cd
                    AND ae.Exception_Type_Cd = @Missing_Meter_Attribute_Exception_Type_Cd
                    AND Closed_By_User_Id IS NULL
                    AND EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.EC_Meter_Attribute_Tracking emat
                                   WHERE
                                        emat.Meter_Attribute_Type_Cd = @Actual_Attribute_Type_Cd
                                        AND emat.Meter_Id = @Meter_ID);
            END;


			
    END;
GO
GRANT EXECUTE ON  [dbo].[EC_Meter_Attribute_Tracking_For_Excel_Upd] TO [CBMSApplication]
GO
