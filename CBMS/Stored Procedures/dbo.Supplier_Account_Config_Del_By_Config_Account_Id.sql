SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.Supplier_Account_Config_Del_By_Config_Account_Id

DESCRIPTION:

INPUT PARAMETERS:
Name								DataType		Default			Description
-------------------------------------------------------------------------------
 @Supplier_Account_Config_Id		INT					
 
 
OUTPUT PARAMETERS:
Name								DataType		Default			Description
-------------------------------------------------------------------------------
	
USAGE EXAMPLES:
-------------------------------------------------------------------------------
  Begin Tran

exec Supplier_Account_Config_Del_By_Config_Account_Id
    @Supplier_Account_Config_Id = 1
    , @Account_Id = 1

Rollback tran


AUTHOR INITIALS:
	Initials	Name
-------------------------------------------------------------------------------
	NR			Narayana Reddy	
	
MODIFICATIONS
	Initials	Date			Modification
-------------------------------------------------------------------------------
    NR				2019-06-26		Created For Add contract.   

******/

CREATE PROCEDURE [dbo].[Supplier_Account_Config_Del_By_Config_Account_Id]
     (
         @Supplier_Account_Config_Id INT
         , @Account_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;


        DELETE
        sac
        FROM
            dbo.Supplier_Account_Config sac
        WHERE
            sac.Account_Id = @Account_Id
            AND sac.Supplier_Account_Config_Id = @Supplier_Account_Config_Id
            AND sac.Contract_Id = -1;


        DELETE
        samm
        FROM
            dbo.SUPPLIER_ACCOUNT_METER_MAP samm
        WHERE
            samm.ACCOUNT_ID = @Account_Id
            AND samm.Supplier_Account_Config_Id = @Supplier_Account_Config_Id
            AND samm.Contract_ID = -1;

    END;

GO
GRANT EXECUTE ON  [dbo].[Supplier_Account_Config_Del_By_Config_Account_Id] TO [CBMSApplication]
GO
