SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






--exec BUDGET_GET_ACCOUNTS_FOR_MANAGE_BUDGET_P 1307
CREATE            PROCEDURE dbo.BUDGET_GET_ACCOUNTS_FOR_MANAGE_BUDGET_P
	@budgetId int
	AS
	begin
		set nocount on
		declare @originalBudgetId int
		select @originalBudgetId =  isnull(original_budget_id,0) from budget where budget_id = @budgetId
		
		if(@originalBudgetId > 0)
		begin
			select 	bacc.budget_account_id 
			from 	budget_account bacc,
				BUDGET_ACCOUNT_TYPE_VW type_vw
			where 	bacc.budget_id = @budgetId
				and bacc.is_reforecast = 1
				and type_vw.budget_id = bacc.budget_id
				and type_vw.budget_account_id = bacc.budget_account_id
				--and type_vw.budget_account_type in('A&B', 'C&D Created')

		end
		else
		begin
			select 	type_vw.budget_account_id 
			from 	BUDGET_ACCOUNT_TYPE_VW type_vw
			where 	type_vw.budget_id = @budgetId
				--and type_vw.budget_account_type in('A&B', 'C&D Created')

		end	

	end



			


			
			

		
			






















GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_ACCOUNTS_FOR_MANAGE_BUDGET_P] TO [CBMSApplication]
GO
