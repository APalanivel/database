SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.Account_Invoice_Collection_Source_Sel            
                        
 DESCRIPTION:                        
			To get the details of invoice collection ACCOUNT level config                  
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@Invoice_Collection_Account_Config_Id	INT
                              
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 
 
  EXEC [dbo].[Account_Invoice_Collection_Source_Sel] 
      @Invoice_Collection_Account_Config_Id = 33063
      
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2016-12-09       Created for invoice tracking
 RKV					2019-06-07      Added File and image sources to the select statement.                
 NR						2020-06-08		SE2017-963 BOT Process - Removed Enroll_In_Bot flag.            
                     
******/

CREATE PROCEDURE [dbo].[Account_Invoice_Collection_Source_Sel]
    (
        @Invoice_Collection_Account_Config_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            aic.Account_Invoice_Collection_Source_Id
            , aic.Invoice_Collection_Account_Config_Id
            , aic.Invoice_Collection_Source_Cd
            , aic.Invoice_Source_Type_Cd
            , aic.Invoice_Source_Method_of_Contact_Cd
            , aic.Is_Primary
            , aic.Collection_Instruction
            , ub.UBM_Id
            , ub.UBM_Type_Cd
            , COALESCE(dt.URL, aicdfefsd.URL) URL
            , COALESCE(dt.Login_Name, aicdfefsd.Login_Name) Login_Name
            , COALESCE(dt.Passcode, aicdfefsd.Passcode) Passcode
            , COALESCE(dt.Instruction, aicdfefsd.Website_Instructions) Instruction
            , COALESCE(dt.Instruction_Document_Image_Id, aicdfefsd.Instruction_Document_Image_Id) Instruction_Document_Image_Id
            , ci.CBMS_DOC_ID
            , aicdfefsd.File_Source_Cd
            , aicdfefsd.Data_Feed_Type_Cd
            , aicdfefsd.Vendor_Id File_Vendor_Id
            , aicdfefsd.Generic_Key
            , aicdfeisd.Image_Source_Cd
            , aicdfeisd.Vendor_Id Image_Vendor_Id
            , aicdfeisd.URL Image_Url
            , aicdfeisd.Login_Name Image_Login_Name
            , aicdfeisd.Passcode Image_Passcode
            , aicdfeisd.Image_Load_Location
        FROM
            dbo.Account_Invoice_Collection_Source aic
            LEFT JOIN dbo.Account_Invoice_Collection_Online_Source_Dtl dt
                ON aic.Account_Invoice_Collection_Source_Id = dt.Account_Invoice_Collection_Source_Id
            LEFT JOIN dbo.Account_Invoice_Collection_UBM_Source_Dtl ub
                ON aic.Account_Invoice_Collection_Source_Id = ub.Account_Invoice_Collection_Source_Id
            LEFT OUTER JOIN dbo.Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl AS aicdfefsd
                ON aicdfefsd.Account_Invoice_Collection_Source_Id = aic.Account_Invoice_Collection_Source_Id
            LEFT OUTER JOIN dbo.Account_Invoice_Collection_Data_Feed_Etl_Image_Source_Dtl AS aicdfeisd
                ON aicdfeisd.Account_Invoice_Collection_Source_Id = aic.Account_Invoice_Collection_Source_Id
            LEFT JOIN dbo.cbms_image ci
                ON ci.CBMS_IMAGE_ID = COALESCE(
                                          dt.Instruction_Document_Image_Id, aicdfefsd.Instruction_Document_Image_Id)
        WHERE
            Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id;


    END;



GO
GRANT EXECUTE ON  [dbo].[Account_Invoice_Collection_Source_Sel] TO [CBMSApplication]
GO
