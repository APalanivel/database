SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:  Sr_Supplier_Contact_Info_Merge_Transfer

        
DESCRIPTION:        
 Merges changes from User_Info Service to the Sr_Supplier_Contact_Info table        
        
INPUT PARAMETERS:        
 Name					DataType		Default		Description        
-------------------------------------------------------------------------------------------------------  
 @@Message				XML							string of changes to the client heir table        
 ,@Conversation_Handle  UNIQUEIDENTIFER				Conversation Handle that sent the message         
        
OUTPUT PARAMETERS:        
 Name      DataType  Default Description        
------------------------------------------------------------------------------------------------------        
@Message_Batch_Count  INT    Returns Count of source Records       
        
        
USAGE EXAMPLES:        
------------------------------------------------------------------------------------------------------  
  
  This sp should be executed from SQL Server Service Broker 
        
AUTHOR INITIALS:        
 Initials	Name        
------------------------------------------------------------------------------------------------------  
 DSC		Kaushik        


MODIFICATIONS        
        
 Initials	Date			Modification        
------------------------------------------------------------        
 DSC		02/13/2014		Created        
******/        
CREATE PROCEDURE [dbo].[Sr_Supplier_Contact_Info_Merge_Transfer]
( 
 @Message XML
,@Conversation_Handle UNIQUEIDENTIFIER )
AS 
BEGIN        

      
        
      SET NOCOUNT ON;        
               
      DECLARE @Idoc INT  

      EXEC sp_xml_preparedocument 
            @idoc OUTPUT
           ,@Message        
                         

      DECLARE @Sr_Supplier_Contact_Info_Changes TABLE
            ( 
             [SR_SUPPLIER_CONTACT_INFO_ID] [int]
            ,[USER_INFO_ID] [int]
            ,[WORK_PHONE] [varchar](50)
            ,[CELL_PHONE] [varchar](50)
            ,[FAX] [varchar](50)
            ,[ADDRESS_LINE1] [varchar](200)
            ,[ADDRESS_LINE2] [varchar](200)
            ,[CITY] [varchar](100)
            ,[STATE_ID] [int]
            ,[ZIP] [varchar](50)
            ,[ADDITIONAL_CONTACT_INFO] [varchar](1000)
            ,[VENDOR_TYPE_ID] [int]
            ,[PRIVILEGE_TYPE_ID] [int]
            ,[IS_RFP_PREFERENCE_WEBSITE] [bit]
            ,[IS_RFP_PREFERENCE_EMAIL] [bit]
            ,[IS_UPDATED] [bit]
            ,[EMAIL_SENT_DATE] [datetime]
            ,[Op_Code] [Char](1)
            ,[Last_Change_Ts] [datetime] )

      INSERT      INTO @Sr_Supplier_Contact_Info_Changes
                  ( 
                   SR_SUPPLIER_CONTACT_INFO_ID
                  ,USER_INFO_ID
                  ,WORK_PHONE
                  ,CELL_PHONE
                  ,FAX
                  ,ADDRESS_LINE1
                  ,ADDRESS_LINE2
                  ,CITY
                  ,STATE_ID
                  ,ZIP
                  ,ADDITIONAL_CONTACT_INFO
                  ,VENDOR_TYPE_ID
                  ,PRIVILEGE_TYPE_ID
                  ,IS_RFP_PREFERENCE_WEBSITE
                  ,IS_RFP_PREFERENCE_EMAIL
                  ,IS_UPDATED
                  ,EMAIL_SENT_DATE
                  ,Op_Code
                  ,Last_Change_Ts )
                  SELECT
                        SR_SUPPLIER_CONTACT_INFO_ID
                       ,USER_INFO_ID
                       ,WORK_PHONE
                       ,CELL_PHONE
                       ,FAX
                       ,ADDRESS_LINE1
                       ,ADDRESS_LINE2
                       ,CITY
                       ,STATE_ID
                       ,ZIP
                       ,ADDITIONAL_CONTACT_INFO
                       ,VENDOR_TYPE_ID
                       ,PRIVILEGE_TYPE_ID
                       ,IS_RFP_PREFERENCE_WEBSITE
                       ,IS_RFP_PREFERENCE_EMAIL
                       ,IS_UPDATED
                       ,EMAIL_SENT_DATE
                       ,Op_Code
                       ,Last_Change_Ts
                  FROM
                        OPENXML (@idoc, '/Sr_Supplier_Contact_Info_Changes/Sr_Supplier_Contact_Info_Change',2)        
        WITH ( 
					[SR_SUPPLIER_CONTACT_INFO_ID] [int] ,
					[USER_INFO_ID] [int] ,
					[WORK_PHONE] [varchar](50) ,
					[CELL_PHONE] [varchar](50) ,
					[FAX] [varchar](50) ,
					[ADDRESS_LINE1] [varchar](200) ,
					[ADDRESS_LINE2] [varchar](200) ,
					[CITY] [varchar](100) ,
					[STATE_ID] [int] ,
					[ZIP] [varchar](50) ,
					[ADDITIONAL_CONTACT_INFO] [varchar](1000) ,
					[VENDOR_TYPE_ID] [int]  ,
					[PRIVILEGE_TYPE_ID] [int] ,
					[IS_RFP_PREFERENCE_WEBSITE] [bit] ,
					[IS_RFP_PREFERENCE_EMAIL] [bit] ,
					[IS_UPDATED] [bit] ,
					[EMAIL_SENT_DATE] [datetime] ,
					[Op_Code] [Char](1),
					[Last_Change_Ts][datetime]
			 )   

           
      EXEC sp_xml_removedocument 
            @idoc  


		
      UPDATE
            tgt
      SET   
            USER_INFO_ID = src.USER_INFO_ID
           ,WORK_PHONE = src.WORK_PHONE
           ,CELL_PHONE = src.CELL_PHONE
           ,FAX = src.FAX
           ,ADDRESS_LINE1 = src.ADDRESS_LINE1
           ,ADDRESS_LINE2 = src.ADDRESS_LINE2
           ,CITY = src.CITY
           ,STATE_ID = src.STATE_ID
           ,ZIP = src.ZIP
           ,ADDITIONAL_CONTACT_INFO = src.ADDITIONAL_CONTACT_INFO
           ,VENDOR_TYPE_ID = src.VENDOR_TYPE_ID
           ,PRIVILEGE_TYPE_ID = src.PRIVILEGE_TYPE_ID
           ,IS_RFP_PREFERENCE_WEBSITE = src.IS_RFP_PREFERENCE_WEBSITE
           ,IS_RFP_PREFERENCE_EMAIL = src.IS_RFP_PREFERENCE_EMAIL
           ,IS_UPDATED = src.IS_UPDATED
           ,EMAIL_SENT_DATE = src.EMAIL_SENT_DATE
           ,Last_Change_Ts = src.Last_Change_Ts
      FROM
            Sr_Supplier_Contact_Info tgt
            INNER JOIN @Sr_Supplier_Contact_Info_Changes src
                  ON tgt.SR_SUPPLIER_CONTACT_INFO_ID = src.SR_SUPPLIER_CONTACT_INFO_ID
                     AND ( src.Last_Change_Ts >= tgt.Last_Change_Ts )
	
	
			
END

;
GO
GRANT EXECUTE ON  [dbo].[Sr_Supplier_Contact_Info_Merge_Transfer] TO [sb_Execute]
GO
