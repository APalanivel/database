SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********
NAME:  dbo.TOU_Schedule_Term_Upd

DESCRIPTION:  

Used to Update TOU Schedule start date,end date and cbms image id by given values.

INPUT PARAMETERS:
      Name								DataType          Default     Description
------------------------------------------------------------
	  @Time_Of_Use_Schedule_Term_Id		INT 
      @Start_Dt							DATETIME
      @End_Dt							DATETIME
      @CBMS_IMAGE_ID					DATETIME
				

OUTPUT PARAMETERS:
      Name								DataType          Default     Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------

	select * from Time_Of_Use_Schedule_Term where time_of_use_schedule_term_id=8
	begin tran
	Exec dbo.TOU_Schedule_Term_Upd 8,'2011-01-31 00:00:00.000',	'2011-07-31 00:00:00.000',null
	select * from Time_Of_Use_Schedule_Term where time_of_use_schedule_term_id=8
	rollback tran
	

AUTHOR INITIALS:
	Initials Name
------------------------------------------------------------
	BCH		 Balaraju


	Initials Date		Modification
------------------------------------------------------------
	BCH		 2012-07-12 Created

******/
CREATE PROCEDURE dbo.TOU_Schedule_Term_Upd
( 
 @Time_Of_Use_Schedule_Term_Id INT
,@Start_Dt DATETIME
,@End_Dt DATETIME = NULL
,@CBMS_IMAGE_ID INT = NULL )
AS 
BEGIN
      SET NOCOUNT ON ;
      DECLARE @Time_Of_Use_Schedule_Id INT
      
      SELECT
            @Time_Of_Use_Schedule_Id = Time_Of_Use_Schedule_Id
      FROM
            Time_Of_Use_Schedule_Term
      WHERE
            Time_Of_Use_Schedule_Term_Id = @Time_Of_Use_Schedule_Term_Id
      
      BEGIN TRY
            BEGIN TRAN
	
			-- End a previous open term if the passed in start date is greater than the corresponding start date
            UPDATE
                  dbo.Time_Of_Use_Schedule_Term
            SET   
                  End_Dt = DATEADD(DD, -1, @Start_Dt)
            WHERE
                  Time_Of_Use_Schedule_Id = @Time_Of_Use_Schedule_Id
                  AND End_Dt = '2099-12-31'
                  AND Start_Dt < @Start_Dt


            UPDATE
                  term
            SET   
                  term.Start_Dt = @Start_Dt
                 ,term.End_Dt = ISNULL(@End_Dt, '2099-12-31')
                 ,term.CBMS_IMAGE_ID = @CBMS_IMAGE_ID
            FROM
                  dbo.Time_Of_Use_Schedule_Term term
            WHERE
                  term.Time_Of_Use_Schedule_Term_Id = @Time_Of_Use_Schedule_Term_Id

			-- Delete the holidays that are mapped to the term which do not belong to the new time frame
            DELETE
                  TOUSTH
            FROM
                  dbo.Time_Of_Use_Schedule_Term_Holiday TOUSTH
                  JOIN dbo.Holiday_Master HM
                        ON TOUSTH.Holiday_Master_Id = HM.Holiday_Master_Id
            WHERE
                  TOUSTH.Time_Of_Use_Schedule_Term_Id = @Time_Of_Use_Schedule_Term_Id
                  AND ( YEAR(HM.Holiday_Dt) < YEAR(@Start_Dt)
                        OR YEAR(HM.Holiday_Dt) > YEAR(@End_Dt) )
            COMMIT TRAN
      END TRY
      BEGIN CATCH
            IF @@TRANCOUNT > 0 
                  ROLLBACK
            EXEC dbo.usp_RethrowError
      END CATCH
END



;
GO
GRANT EXECUTE ON  [dbo].[TOU_Schedule_Term_Upd] TO [CBMSApplication]
GO
