SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    [Workflow].[Workflow_GetPermission]
DESCRIPTION: Get the Permission infor for the User
------------------------------------------------------------   
 INPUT PARAMETERS:    
 Name   DataType  Default Description    
 @USER_INFO_ID INT
 @PERMISSION_NAME NVARCHAR(4000) 
------------------------------------------------------------    
 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
 ------------------------------------------------------------    
 USAGE EXAMPLES:    
EXEC [workflow].[Workflow_GetPermission] @USER_INFO_ID = 49,               -- int
                                     @PERMISSION_NAME = N'queue.invoice.Filtergroup.viewUnAssignedInvoices'          -- nvarchar(4000)
                                    
------------------------------------------------------------    
AUTHOR INITIALS:    
Initials	Name    
------------------------------------------------------------    
TRK			Ramakrishna Thummala	Summit Energy 
 
 MODIFICATIONS     
 Initials Date   Modification    
------------------------------------------------------------    
 TRK		  Sep-13-2019  Created

******/


CREATE PROCEDURE [Workflow].[Workflow_GetPermission]  
(  
@USER_INFO_ID INT,  
@PERMISSION_NAME NVARCHAR(4000)  
)  
AS  
BEGIN  
  
DECLARE @Cnt INT   
 SELECT  @Cnt= COUNT(1)  
 FROM group_info GI   
 JOIN user_info_group_info_map UIG ON GI.group_info_id = UIG.group_info_id   
 JOIN user_info U ON U.user_info_id = UIG.user_info_id   
 JOIN group_info_permission_info_map GIPM ON GIPM.group_info_id = GI.group_info_id   
 JOIN permission_info P WITH (NOLOCK) ON P.permission_info_id = GIPM.permission_info_id   
 WHERE U.USER_INFO_ID=@USER_INFO_ID   
 AND P.PERMISSION_NAME = @PERMISSION_NAME  
  
IF @Cnt > 0  
 BEGIN   
 SELECT  'True' AS 'Permission Exist'  
 END   
ELSE   
 SELECT 'False' AS 'Permission Exist'  
END  
GO
GRANT EXECUTE ON  [Workflow].[Workflow_GetPermission] TO [CBMSApplication]
GO
