SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Contract_Meter_Volume_Dtl_Ins_Calcuted_Volumes]
           
DESCRIPTION:             
			To insert calculated contract meter volumes
			
INPUT PARAMETERS:            
	Name					DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Meter_Id				INT
    @Contract_Id			INT
    @Volume					DECIMAL(32, 16)
    @Start_Dt				DATETIME
    @End_Dt					DATETIME
    @Bucket_Master_Id		INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
BEGIN TRANSACTION
	SELECT * FROM dbo.CONTRACT_METER_VOLUME WHERE METER_ID = 100 AND CONTRACT_ID = 100
	EXEC dbo.Contract_Meter_Volume_Dtl_Ins_Calcuted_Volumes 100,100,1200,'2016-01-01','2016-12-01',20,239
	SELECT * FROM dbo.CONTRACT_METER_VOLUME WHERE METER_ID = 100 AND CONTRACT_ID = 100
	SELECT sum(VOLUME) FROM dbo.CONTRACT_METER_VOLUME WHERE METER_ID = 100 AND CONTRACT_ID = 100
ROLLBACK TRANSACTION

		
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2020-04-16	GRM - Contract volumes enhancement - Created
	RR			2020-05-09	GRMUER-81 - Added new optional input @Is_Edited	
	
******/
CREATE PROCEDURE [dbo].[Contract_Meter_Volume_Dtl_Ins_Calcuted_Volumes]
    (
        @Meter_Id INT
        , @Contract_Id INT
        , @Volume DECIMAL(32, 16)
        , @Start_Dt DATETIME
        , @End_Dt DATETIME
        , @Bucket_Master_Id INT
        , @Is_Edited BIT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Short_Value DECIMAL(32, 16)
            , @Cnt INT
            , @Last_Row INT;

        DECLARE @Meter_Volumes TABLE
              (
                  Meter_Id INT
                  , Contract_Id INT
                  , Volume DECIMAL(32, 16)
                  , MONTH_IDENTIFIER DATETIME
                  , Row_Num INT IDENTITY(1, 1)
              );

        SELECT  @Cnt = DATEDIFF(MONTH, @Start_Dt, @End_Dt) + 1;


        INSERT INTO @Meter_Volumes
             (
                 Meter_Id
                 , Contract_Id
                 , Volume
                 , MONTH_IDENTIFIER
             )
        SELECT
            @Meter_Id
            , @Contract_Id
            , CAST(@Volume AS INT) / @Cnt
            , dd.DATE_D
        FROM
            meta.DATE_DIM dd
        WHERE
            dd.DATE_D BETWEEN DATEADD(dd, -DATEPART(dd, @Start_Dt) + 1, @Start_Dt)
                      AND     @End_Dt
        GROUP BY
            dd.DATE_D;

        SET @Last_Row = @@ROWCOUNT;

        SELECT
            @Short_Value = CAST(@Volume AS DECIMAL(32, 16)) - SUM(CAST(mv.Volume AS DECIMAL(32, 16)))
        FROM
            @Meter_Volumes mv;

        UPDATE
            mv
        SET
            Volume = Volume + @Short_Value
        FROM
            @Meter_Volumes mv
        WHERE
            mv.Row_Num = @Last_Row;

        MERGE INTO dbo.Contract_Meter_Volume_Dtl AS tgt
        USING
        (   SELECT
                cmv.CONTRACT_METER_VOLUME_ID
                , @Bucket_Master_Id AS Bucket_Master_Id
                , mv.Volume
                , ISNULL(@Is_Edited, 0) AS Is_Edited
            FROM
                @Meter_Volumes mv
                INNER JOIN dbo.CONTRACT_METER_VOLUME cmv
                    ON cmv.CONTRACT_ID = mv.Contract_Id
                       AND  cmv.METER_ID = mv.Meter_Id
                       AND  cmv.MONTH_IDENTIFIER = mv.MONTH_IDENTIFIER) AS src
        ON tgt.CONTRACT_METER_VOLUME_ID = src.CONTRACT_METER_VOLUME_ID
           AND  tgt.Bucket_Master_Id = src.Bucket_Master_Id
        WHEN MATCHED THEN UPDATE SET
                              tgt.Volume = src.Volume
                              , tgt.Is_Edited = src.Is_Edited
        WHEN NOT MATCHED BY TARGET THEN INSERT (CONTRACT_METER_VOLUME_ID
                                                , Bucket_Master_Id
                                                , Volume
                                                , Is_Edited)
                                        VALUES
                                            (src.CONTRACT_METER_VOLUME_ID
                                             , src.Bucket_Master_Id
                                             , src.Volume
                                             , src.Is_Edited);
    END;
GO
GRANT EXECUTE ON  [dbo].[Contract_Meter_Volume_Dtl_Ins_Calcuted_Volumes] TO [CBMSApplication]
GO
