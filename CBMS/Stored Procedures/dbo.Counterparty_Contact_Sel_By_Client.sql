SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                
NAME:                                
    dbo.Counterparty_Contact_Sel_By_Client                                
                                
DESCRIPTION:                                
                           
                                
INPUT PARAMETERS:                                
	Name				DataType		Default Description                                
---------------------------------------------------------------
	@Client_Id			INT
    @Commodity_Id		INT
    @Start_Dt			DATE
    @End_Dt				DATE
    @Hedge_Type			INT
                          
                       
OUTPUT PARAMETERS:                                
 Name   DataType  Default Description                                
---------------------------------------------------------------    

	EXEC dbo.Counterparty_Sel_By_Client_Keyword @Client_Id = 11236,@Keyword = 'America'
	
	EXEC dbo.Counterparty_Contact_Sel_By_Client @Client_Id = 11236
	EXEC dbo.Counterparty_Contact_Sel_By_Client @Client_Id = 11236,@RM_Counterparty_Id = 1
	EXEC dbo.Counterparty_Contact_Sel_By_Client @Client_Id = 11236,@RM_Counterparty_Id = 2
	EXEC dbo.Counterparty_Contact_Sel_By_Client @Client_Id = 11236,   @Contact_Info_Id='5439,5424'             
 
USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
                   
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	RR			2018-10-25  Created For Risk Management

******/
CREATE PROCEDURE [dbo].[Counterparty_Contact_Sel_By_Client]
      ( 
       @Client_Id INT
      ,@RM_Counterparty_Id INT = NULL
      ,@Contact_Info_Id VARCHAR(MAX) = NULL )
AS 
BEGIN

      SET NOCOUNT ON;

      SELECT
            rc.RM_COUNTERPARTY_ID
           ,rc.COUNTERPARTY_NAME
           ,ci.First_Name
           ,ci.Last_Name
           ,Email_Address
           ,Phone_Number
           ,Mobile_Number
           ,ci.Contact_Info_Id
           ,ci.Created_Ts
      FROM
            dbo.RM_COUNTERPARTY rc
            INNER JOIN dbo.Counterparty_Client_Contact_Map ccm
                  ON rc.RM_COUNTERPARTY_ID = ccm.Counterparty_Id
            INNER JOIN dbo.Contact_Info ci
                  ON ccm.Contact_Info_Id = ci.Contact_Info_Id
      WHERE
            ccm.Client_Id = @Client_Id
            AND ( @RM_Counterparty_Id IS NULL
                  OR rc.RM_COUNTERPARTY_ID = @RM_Counterparty_Id )
            AND ( @Contact_Info_Id IS NULL
                  OR EXISTS ( SELECT
                                    1
                              FROM
                                    dbo.ufn_split(@Contact_Info_Id, ',')
                              WHERE
                                    CAST(Segments AS INT) = ci.Contact_Info_Id ) );

END;



GO
GRANT EXECUTE ON  [dbo].[Counterparty_Contact_Sel_By_Client] TO [CBMSApplication]
GO
