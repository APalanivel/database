SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE    PROCEDURE dbo.CHECK_CONTRACT_EXIST_FOR_SITE_P
@userId varchar(10),
@sessionId varchar(20),
@siteId int,
@monthIdentifier datetime



 AS
begin

	set nocount on
	SELECT VOLUME VOLUME1 FROM CONTRACT_METER_VOLUME WHERE METER_ID IN
	(select METER_ID from meter where account_id in
	(
	
		select account_id from account where site_id=@siteId
	)) AND MONTH_IDENTIFIER=@monthIdentifier

end
--select * from entity where entity_type=285
GO
GRANT EXECUTE ON  [dbo].[CHECK_CONTRACT_EXIST_FOR_SITE_P] TO [CBMSApplication]
GO
