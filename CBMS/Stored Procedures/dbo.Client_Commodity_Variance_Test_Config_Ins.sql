SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******              
Name:   dbo.dbo.Client_Commodity_Variance_Test_Config_Ins          
              
Description:              
        To insert Data into dbo.Client_Commodity_Variance_Test_Config table.              
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
@Client_Commodity_Id					INT
@Start_Dt								DATE
@End_Dt									DATE				 NULL
@Variance_Test_Type_Cd					INT
@User_Info_Id							INT    
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                

              
 Usage Examples:                  
----------------------------------------------------------------------------------------                

	
	BEGIN TRAN  
	SELECT * FROM dbo.Client_Commodity_Variance_Test_Config WHERE Client_Commodity_Id=20001668
    
    EXEC dbo.Client_Commodity_Variance_Test_Config_Ins 
      @Client_Commodity_Id = 20001668
     ,@Start_Dt = '2015-01-01'
     ,@End_Dt= NULL
     ,@Variance_Test_Type_Cd = 102246
     ,@User_Info_Id = 49
     
	
	SELECT * FROM dbo.Client_Commodity_Variance_Test_Config WHERE Client_Commodity_Id=20001668
	ROLLBACK TRAN             


SELECT
      c.CLIENT_NAME
     ,cm.Commodity_Name
     ,cc.Client_Commodity_Id
     ,cc.Commodity_Service_Cd
FROM
      core.Client_Commodity cc
      INNER JOIN dbo.Commodity cm
            ON cc.Commodity_Id = cm.Commodity_Id
      INNER JOIN dbo.CLIENT c
            ON cc.CLIENT_ID = c.CLIENT_ID
WHERE
      c.CLIENT_ID = 11278
      AND NOT EXISTS ( SELECT
                        1
                       FROM
                        dbo.Client_Commodity_Variance_Test_Config ccv
                       WHERE
                        ccv.Client_Commodity_Id = cc.Client_Commodity_Id )
                        
                                     
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	SP				Sandeep Pigilam
	
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    SP				2016-11-01		Created for Variance Test Enhancements Phase II.
         
             
******/ 

CREATE  PROCEDURE [dbo].[Client_Commodity_Variance_Test_Config_Ins]
      ( 
       @Client_Commodity_Id INT
      ,@Start_Dt DATE
      ,@End_Dt DATE = NULL
      ,@Variance_Test_Type_Cd INT
      ,@User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 

                  
      INSERT      INTO dbo.Client_Commodity_Variance_Test_Config
                  ( 
                   Client_Commodity_Id
                  ,Start_Dt
                  ,End_Dt
                  ,Variance_Test_Type_Cd
                  ,Created_Ts
                  ,Created_User_Id
                  ,Updated_User_Id
                  ,Last_Change_Ts )
      VALUES
                  ( 
                   @Client_Commodity_Id
                  ,@Start_Dt
                  ,@End_Dt
                  ,@Variance_Test_Type_Cd
                  ,GETDATE()
                  ,@User_Info_Id
                  ,@User_Info_Id
                  ,GETDATE() )
    
            
END

;
GO
GRANT EXECUTE ON  [dbo].[Client_Commodity_Variance_Test_Config_Ins] TO [CBMSApplication]
GO
