SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE       PROCEDURE [dbo].[cbmsBudgets_GetForClientAndCommodity]
	( @myaccountid int = null
	, @client_id int = null
	, @commodity_type_id int = null
	, @year int = null
	)
AS
BEGIN

	   select budget_id
		, client_id
		, budget_name
		, date_initiated
		, budget_start_year
		, budget_start_month
		, posted_by
		, commodity_type_id
		, cbms_image_id
		, (convert(varchar(10),budget_date, 101)) as report_date
		, budget_date as full_report_date
	     from budget
	    where client_id = @client_id
	      and commodity_type_id = @commodity_type_id
	      and budget_start_year = @year
		order by full_report_date desc

END
GO
GRANT EXECUTE ON  [dbo].[cbmsBudgets_GetForClientAndCommodity] TO [CBMSApplication]
GO
