SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********    
NAME:  dbo.Report_DE_ProkarmaTemplates      
     
DESCRIPTION:    
 procedure is used for data extracts via DTS packages    
    
INPUT PARAMETERS:        
      Name     DataType          Default     DescriptiON        
------------------------------------------------------------        
  
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------        
        
USAGE EXAMPLES:       
    
 EXEC dbo.Report_DE_ProkarmaTemplates
     
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials	Name      
------------------------------------------------------------            
HG			Harihara Suthan G    
DMR			Deana Ritter    
PD			Padmanava Debnath 
RR			Raghu Reddy 
NR			Narayana Reddy

Initials	Date		ModificatiON      
------------------------------------------------------------      
			07/20/2009	Created    
 HG			03/23/2010	Bucket_type_id column renamed to Bucket_Master_id in Cu_Invoice_Charge and Cu_Invoice_Determinant table.    
						  Entity table reference to get Bucket_name replaced by Bucket_Master table    
						  Entity table reference to get Commodity_name replaced by Commodity table.    
 DMR		08/20/2010  Removed Meter table from Account CTE as this was generating multiple results unnecessarily.  This cause invoices to be    
						incorrectly reported as group accounts.    
 PD			11/02/2010	Added WHERE clause in the CTE to filter out AT&T data  
 DMR		06/20/2011   Removed join to Invoice Charge account in pulling base invoice as we now receive determinants with no charges.   
							Per Erin Dierking 6/20/2011.  
 DMR		07/12/2011   Complete re-write to use Client_Hier tables, remove invoice charge table and eliminate Belgium records to   
						   assist in the file size.  Used isnull(cu.ubm_id, 1) instead of cu.ubm_id is null or cu.ubm_in in (1,2) due to  
						   execution plan.  Execution plan was doing conversions of the numeric values to constant scalers.  This change  
						   simplified the execution plan and reduced execution time by 1/3.  Converted from using RPT_CU_Invoice tables to  
						   using temporary tables.    
DMR			08/10/2011   Due to SSIS being unable to retrieve meta data ont he resulset, the block where 1=2 was added.  Converting to  
							table variables extended the execution time from 5 min to 45 min.  These are massive resultsets. This hint   
							was referenced by http://www.sqlservercentral.com/articles/Integration+Services+(SSIS)/65112/   
                       
DMR			10/14/2011	Modified to include any AT&T accounts that are NOT in US and Canada.   
DMR			05/08/2012  MAINT-1307 Modified to take number of history days as a parameter so that we can run for All Templates.  Also included  
						   the business logic to exclude invoices that are marked as DNT or Duplicate.                      
HG			2013-11-07	MAINT-2356, Removed filter condition added to exclude the france accounts  
							-- Corrected the country name of "Luxemb urg" to "Luxembourg" as per the name in CBMS COUNTRY table  
HG			2014-02-07	MAINT-2357, Meter_Number column added and returning Display_Account_Number to show the supplier account dates as well
HG			2014-02-11	MAINT-2502 ,  removed Canada as an exclusion for AT&T accounts
HG			2014-02-28	MAINT-2600, Meter_Number column removed
RR			2014-06-25	Data Operations-2 Modified the logic to get template invoice id, @NumDays is not in use
HG			2014-07-28	MAINT-2951, modified to return the supplier account dates after account number instead of the Display Account_Number
								- Also modified to remote the filter condition on Ubm_Id which excluded other Ubms data
HG			2016-04-05	AS400/CIP enhancement, Modified to remove the filter condition added to exclude the Belgium, netherland and luxembourgh country accounts
HG			2017-01-13	MAINT-4800, modified to return only the invoice templates modified for the given number of days, sort order changed to match with CBMS
HG			2017-03-17	MAINT-5085, modified to consider the invoice if that invoice is assigned to an account as master template in the last 7 days or that master template is modified in the last 7 days.
NR			2019-02-12	Telamon data feed - Added Sub_Item_Code column for Charge.
HG			2019-09-30	PRSUPPORT-2996, converting the values to the column data width instead of leaving it to default 30 characters.
******/
CREATE PROCEDURE [dbo].[Report_DE_ProkarmaTemplates]
      (
      @NumDays INT = -7 )
AS
      BEGIN

            SET NOCOUNT ON;
            SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
            DECLARE @StartDate DATE;

            IF @NumDays IS NULL
                  BEGIN
                        SELECT
                              @StartDate = '1/1/1900';
                  END;
            ELSE
                  BEGIN
                        SELECT
                              @StartDate = dateadd(DAY, @NumDays, getdate());
                  END;

            /*****************************************************************************************
   This is here entirely as a work around for SSIS and temporary tables.  This statement
   allows SSIS to be able to return the meta data about the stored procedure to map columns.
*****************************************************************************************/
            IF 1 = 2
                  BEGIN
                        SELECT
                              cast(NULL AS INT) AS 'Client Id'
                            , cast(NULL AS VARCHAR(200)) AS 'Client'
                            , cast(NULL AS INT) AS 'Site Id'
                            , cast(NULL AS VARCHAR(MAX)) AS 'Site'
                            , cast(NULL AS VARCHAR(MAX)) AS 'State'
                            , cast(NULL AS INT) AS 'Account Id'
                            , cast(NULL AS VARCHAR(500)) AS 'Account Number'
                            , cast(NULL AS INT) AS 'Vendor Id'
                            , cast(NULL AS VARCHAR(200)) AS 'Vendor Name'
                            , cast(NULL AS VARCHAR(8)) AS 'Account Type'
                            , cast(NULL AS INT) AS 'Invoice Id'
                            , cast(NULL AS VARCHAR) AS 'Service Month'
                            , cast(NULL AS VARCHAR(200)) AS 'Currency'
                            , cast(NULL AS VARCHAR(200)) AS 'Item Type'
                            , cast(NULL AS VARCHAR(200)) AS 'Commodity Type'
                            , cast(NULL AS VARCHAR(200)) AS 'Item Name'
                            , cast(NULL AS VARCHAR(200)) AS 'Item Code'
                            , cast(NULL AS VARCHAR(200)) AS 'Sub Item Code'
                            , cast(NULL AS VARCHAR(200)) AS 'Unit of Measure'
                            , cast(NULL AS VARCHAR(200)) AS 'Item Value'
                            , cast(NULL AS VARCHAR(200)) AS 'Updated By';

                  END;

            CREATE TABLE #ACCOUNT_CU_INVOICE
                  ( ACCOUNT_ID       INT NOT NULL
                  , CU_INVOICE_ID    INT NOT NULL
                  , UPDATED_BY_ID    INT
                  , CURRENCY_UNIT_ID INT
                  , service_month    DATE );
            CREATE INDEX #ACCOUNT_CU_INVOICE__CU_Invoice
            ON #ACCOUNT_CU_INVOICE (
                  CU_INVOICE_ID
                , ACCOUNT_ID );

            CREATE TABLE #CU_INVOICE_DETAIL
                  ( CU_INVOICE_ID              INT          NOT NULL
                  , ITEM_TYPE                  VARCHAR(200) NOT NULL
                  , COMMODITY_TYPE             VARCHAR(200) NULL
                  , ITEM_NAME                  VARCHAR(200) NULL
                  , ITEM_CODE                  VARCHAR(200) NOT NULL
                  , Sub_Item_Code              VARCHAR(200) NULL
                  , UNIT_OF_MEASURE            VARCHAR(200) NULL
                  , ITEM_VALUE                 VARCHAR(200) NOT NULL
                  , CU_DETERMINANT_CHARGE_CODE INT );
            CREATE INDEX #CU_INVOICE_DETAIL__CU_Invoice
            ON #CU_INVOICE_DETAIL ( CU_INVOICE_ID );

            CREATE TABLE #Account_Dtl
                  ( Client_ID           INT
                  , Client_name         VARCHAR(200)
                  , Site_ID             INT
                  , Site_name           VARCHAR(MAX)
                  , State_Name          VARCHAR(MAX)
                  , Account_ID          INT
                  , Account_Number      VARCHAR(1000)
                  , Account_Vendor_ID   INT
                  , Account_Vendor_Name VARCHAR(200)
                  , Account_Type        VARCHAR(8));
            CREATE INDEX #Account_Dtl_Account_ID
            ON #Account_Dtl ( Account_ID );

            INSERT INTO #ACCOUNT_CU_INVOICE (
                                                  ACCOUNT_ID
                                                , CU_INVOICE_ID
                                                , UPDATED_BY_ID
                                                , CURRENCY_UNIT_ID
                                                , service_month
                                            )
                        SELECT
                                    sm.Account_ID
                                  , cha.Template_Cu_Invoice_Id AS cu_invoice_id
                                  , cu.UPDATED_BY_ID
                                  , cu.CURRENCY_UNIT_ID
                                  , sm.SERVICE_MONTH
                        FROM        dbo.CU_INVOICE cu
                                    INNER JOIN
                                    dbo.CU_INVOICE_SERVICE_MONTH sm
                                          ON sm.CU_INVOICE_ID = cu.CU_INVOICE_ID
                                    INNER JOIN
                                    Core.Client_Hier_Account cha
                                          ON sm.Account_ID = cha.Account_Id
                                             AND cha.Template_Cu_Invoice_Id = cu.CU_INVOICE_ID
                        WHERE       cu.IS_PROCESSED = 1
                                    AND   IS_DNT = 0
                                    AND   IS_DUPLICATE = 0
                                    AND
                                          (     cha.Template_Cu_Invoice_Last_Change_Ts >= @StartDate
                                                OR    cu.UPDATED_DATE > @StartDate )
                        GROUP BY    sm.Account_ID
                                  , cha.Template_Cu_Invoice_Id
                                  , cu.UPDATED_BY_ID
                                  , cu.CURRENCY_UNIT_ID
                                  , sm.SERVICE_MONTH;

            INSERT INTO #CU_INVOICE_DETAIL (
                                                 CU_INVOICE_ID
                                               , ITEM_TYPE
                                               , COMMODITY_TYPE
                                               , ITEM_NAME
                                               , ITEM_CODE
                                               , Sub_Item_Code
                                               , UNIT_OF_MEASURE
                                               , ITEM_VALUE
                                               , CU_DETERMINANT_CHARGE_CODE
                                           )
                        SELECT
                              d.CU_INVOICE_ID
                            , 'Determinant' item_type
                            , c.Commodity_Name commodity_type
                            , d.DETERMINANT_NAME item_name
                            , bm.Bucket_Name item_code
                            , sbm.Sub_Bucket_Name AS Sub_Item_Code
                            , u.ENTITY_NAME unit_of_measure
                            , d.DETERMINANT_VALUE item_value
                            , dbo.ufn_Remove_Special_Characters(d.CU_DETERMINANT_CODE, 'ABCDE')
                        FROM  #ACCOUNT_CU_INVOICE cu
                              INNER JOIN
                              dbo.CU_INVOICE_DETERMINANT d
                                    ON cu.CU_INVOICE_ID = d.CU_INVOICE_ID
                              INNER JOIN
                              dbo.Commodity c
                                    ON c.Commodity_Id = d.COMMODITY_TYPE_ID
                              INNER JOIN
                              dbo.Bucket_Master bm
                                    ON bm.Bucket_Master_Id = d.Bucket_Master_Id
                              INNER JOIN
                              dbo.ENTITY u
                                    ON u.ENTITY_ID = d.UNIT_OF_MEASURE_TYPE_ID
                              LEFT OUTER JOIN
                              dbo.EC_Invoice_Sub_Bucket_Master sbm
                                    ON sbm.EC_Invoice_Sub_Bucket_Master_Id = d.EC_Invoice_Sub_Bucket_Master_Id
                        WHERE d.DETERMINANT_VALUE IS NOT NULL;

            INSERT INTO #CU_INVOICE_DETAIL (
                                                 CU_INVOICE_ID
                                               , ITEM_TYPE
                                               , COMMODITY_TYPE
                                               , ITEM_NAME
                                               , ITEM_CODE
                                               , Sub_Item_Code
                                               , UNIT_OF_MEASURE
                                               , ITEM_VALUE
                                               , CU_DETERMINANT_CHARGE_CODE
                                           )
                        SELECT
                              d.CU_INVOICE_ID
                            , 'Charge' item_type
                            , c.Commodity_Name commodity_type
                            , d.CHARGE_NAME item_name
                            , bm.Bucket_Name item_code
                            , sbm.Sub_Bucket_Name AS Sub_Item_Code
                            , NULL unit_of_measure
                            , d.CHARGE_VALUE item_value
                            , dbo.ufn_Remove_Special_Characters(d.CU_DETERMINANT_CODE, 'ABCDE')
                        FROM  #ACCOUNT_CU_INVOICE cu
                              INNER JOIN
                              dbo.CU_INVOICE_CHARGE d
                                    ON d.CU_INVOICE_ID = cu.CU_INVOICE_ID
                              INNER JOIN
                              dbo.Commodity c
                                    ON c.Commodity_Id = d.COMMODITY_TYPE_ID
                              INNER JOIN
                              dbo.Bucket_Master bm
                                    ON bm.Bucket_Master_Id = d.Bucket_Master_Id
                              LEFT OUTER JOIN
                              dbo.EC_Invoice_Sub_Bucket_Master sbm
                                    ON sbm.EC_Invoice_Sub_Bucket_Master_Id = d.EC_Invoice_Sub_Bucket_Master_Id
                        WHERE d.CHARGE_VALUE IS NOT NULL;

            INSERT      #Account_Dtl (
                                           Client_ID
                                         , Client_name
                                         , Site_ID
                                         , Site_name
                                         , State_Name
                                         , Account_ID
                                         , Account_Number
                                         , Account_Vendor_ID
                                         , Account_Vendor_Name
                                         , Account_Type
                                     )
                        SELECT
                                    CH.Client_Id
                                  , CH.Client_Name
                                  , CH.Site_Id
                                  , CH.Site_name
                                  , CH.State_Name
                                  , CHA.Account_Id
                                  , CHA.Account_Number
                                  , CHA.Account_Vendor_Id
                                  , CHA.Account_Vendor_Name
                                  , CHA.Account_Type
                        FROM        Core.Client_Hier_Account CHA
                                    INNER JOIN
                                    Core.Client_Hier CH
                                          ON CHA.Client_Hier_Id = CH.Client_Hier_Id
                        WHERE       (     CH.Client_Name <> 'AT&T Services, Inc.'
                                          OR
                                                (     CH.Client_Name = 'AT&T Services, Inc.'
                                                      AND  CH.Country_Name <> 'USA' ))
                                    AND  CHA.Account_Not_Managed = 0
                                    AND  CHA.Account_Type = 'Utility'
                                    AND  CH.Client_Not_Managed = 0
                                    AND  EXISTS
                              (     SELECT
                                          1
                                    FROM  #ACCOUNT_CU_INVOICE acu
                                    WHERE acu.ACCOUNT_ID = CHA.Account_Id )
                        GROUP BY    CH.Client_Id
                                  , CH.Client_Name
                                  , CH.Site_Id
                                  , CH.Site_name
                                  , CH.State_Name
                                  , CHA.Account_Id
                                  , CHA.Account_Number
                                  , CHA.Account_Vendor_Id
                                  , CHA.Account_Vendor_Name
                                  , CHA.Account_Type
                        UNION ALL
                        SELECT
                                    CH.Client_Id
                                  , CH.Client_Name
                                  , CH.Site_Id
                                  , CH.Site_name
                                  , CH.State_Name
                                  , CHA.Account_Id
                                  , isnull(CHA.Account_Number, 'Blank for ' + CHA.Account_Vendor_Name) + ' (' + convert(VARCHAR, CHA.Supplier_Account_begin_Dt, 101) + '-' + convert(VARCHAR, CHA.Supplier_Account_End_Dt, 101) + ')' AS Account_Number
                                  , CHA.Account_Vendor_Id
                                  , CHA.Account_Vendor_Name
                                  , CHA.Account_Type
                        FROM        Core.Client_Hier_Account CHA
                                    INNER JOIN
                                    Core.Client_Hier CH
                                          ON CHA.Client_Hier_Id = CH.Client_Hier_Id
                                    INNER JOIN
                                    dbo.SUPPLIER_ACCOUNT_METER_MAP MAP
                                          ON CHA.Account_Id = MAP.ACCOUNT_ID
                                             AND     CHA.Meter_Id = MAP.METER_ID
                        WHERE       (     CH.Client_Name <> 'AT&T Services, Inc.'
                                          OR
                                                (     CH.Client_Name = 'AT&T Services, Inc.'
                                                      AND  CH.Country_Name <> 'USA' ))
                                    AND  CHA.Account_Type = 'Supplier'
                                    AND  CHA.Account_Not_Managed = 0
                                    AND  MAP.IS_HISTORY = 0
                                    AND  CH.Client_Not_Managed = 0
                                    AND  EXISTS
                              (     SELECT
                                          1
                                    FROM  #ACCOUNT_CU_INVOICE acu
                                    WHERE acu.ACCOUNT_ID = CHA.Account_Id )
                        GROUP BY    CH.Client_Id
                                  , CH.Client_Name
                                  , CH.Site_Id
                                  , CH.Site_name
                                  , CH.State_Name
                                  , CHA.Account_Id
                                  , isnull(CHA.Account_Number, 'Blank for ' + CHA.Account_Vendor_Name) + ' (' + convert(VARCHAR, CHA.Supplier_Account_begin_Dt, 101) + '-' + convert(VARCHAR, CHA.Supplier_Account_End_Dt, 101) + ')'
                                  , CHA.Account_Vendor_Id
                                  , CHA.Account_Vendor_Name
                                  , CHA.Account_Type;

            SELECT
                        vam.Client_ID AS 'Client Id'
                      , vam.Client_name AS 'Client'
                      , vam.Site_ID AS 'Site Id'
                      , vam.Site_name AS 'Site'
                      , vam.State_Name AS 'State'
                      , vam.Account_ID AS 'Account Id'
                      , vam.Account_Number AS 'Account Number'
                      , vam.Account_Vendor_ID AS 'Vendor Id'
                      , vam.Account_Vendor_Name AS 'Vendor Name'
                      , vam.Account_Type AS 'Account Type'
                      , cu.CU_INVOICE_ID AS 'Invoice Id'
                      , convert(VARCHAR, cu.service_month, 101) AS 'Service Month'
                      , cur.CURRENCY_UNIT_NAME AS 'Currency'
                      , isnull(convert(VARCHAR(200), x.ITEM_TYPE), '') AS 'Item Type'
                      , isnull(convert(VARCHAR(200), x.COMMODITY_TYPE), '') AS 'Commodity Type'
                      , isnull(convert(VARCHAR(200), x.ITEM_NAME), '') AS 'Item Name'
                      , isnull(convert(VARCHAR(200), x.ITEM_CODE), '') AS 'Item Code'
                      , isnull(convert(VARCHAR(200), x.Sub_Item_Code), '') AS 'Sub Item Code'
                      , isnull(convert(VARCHAR(200), x.UNIT_OF_MEASURE), '') AS 'Unit of Measure'
                      , isnull(convert(VARCHAR(200), x.ITEM_VALUE), '') AS 'Item Value'
                      , isnull(convert(VARCHAR(200), ui.FIRST_NAME + space(1) + ui.LAST_NAME), '') AS 'Updated By'
            FROM        #ACCOUNT_CU_INVOICE cu
                        INNER JOIN
                        #CU_INVOICE_DETAIL x
                              ON x.CU_INVOICE_ID = cu.CU_INVOICE_ID
                        INNER JOIN
                        #Account_Dtl vam
                              ON vam.Account_ID = cu.ACCOUNT_ID
                        INNER JOIN
                        dbo.CURRENCY_UNIT cur
                              ON cur.CURRENCY_UNIT_ID = cu.CURRENCY_UNIT_ID
                        INNER JOIN
                        dbo.USER_INFO ui
                              ON ui.USER_INFO_ID = cu.UPDATED_BY_ID
            GROUP BY    vam.Client_ID
                      , vam.Client_name
                      , vam.Site_ID
                      , vam.Site_name
                      , vam.State_Name
                      , vam.Account_ID
                      , vam.Account_Number
                      , vam.Account_Vendor_ID
                      , vam.Account_Vendor_Name
                      , vam.Account_Type
                      , cu.CU_INVOICE_ID
                      , convert(VARCHAR, cu.service_month, 101)
                      , cur.CURRENCY_UNIT_NAME
                      , isnull(convert(VARCHAR(200), x.ITEM_TYPE), '')
                      , isnull(convert(VARCHAR(200), x.COMMODITY_TYPE), '')
                      , isnull(convert(VARCHAR(200), x.ITEM_NAME), '')
                      , isnull(convert(VARCHAR(200), x.ITEM_CODE), '')
                      , isnull(convert(VARCHAR(200), x.Sub_Item_Code), '')
                      , isnull(convert(VARCHAR(200), x.UNIT_OF_MEASURE), '')
                      , isnull(convert(VARCHAR(200), x.ITEM_VALUE), '')
                      , isnull(convert(VARCHAR(200), ui.FIRST_NAME + space(1) + ui.LAST_NAME), '')
                      , x.CU_DETERMINANT_CHARGE_CODE
            ORDER BY    vam.Client_name ASC
                      , vam.Site_name
                      , vam.Account_Number
                      , isnull(convert(VARCHAR(200), x.ITEM_TYPE), '') DESC
                      , x.CU_DETERMINANT_CHARGE_CODE;

            DROP TABLE #ACCOUNT_CU_INVOICE;
            DROP TABLE #Account_Dtl;
            DROP TABLE #CU_INVOICE_DETAIL;

      END;
      ;


GO






GRANT EXECUTE ON  [dbo].[Report_DE_ProkarmaTemplates] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_ProkarmaTemplates] TO [CBMSApplication]
GO
