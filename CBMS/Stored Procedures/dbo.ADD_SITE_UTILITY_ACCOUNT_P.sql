SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE    PROCEDURE dbo.ADD_SITE_UTILITY_ACCOUNT_P
	@vendor_id int,
	@site_id int,
	@invoice_source_type_id int,
	@account_number varchar(50),
	@account_type_id int
	AS
	begin
		set nocount on
		insert into account(vendor_id, 
				site_id,
				invoice_source_type_id,
				account_number, 
				account_type_id)
		values(@vendor_id,
		       @site_id,
		       @invoice_source_type_id, 
		       @account_number, 
		       @account_type_id)
	end






GO
GRANT EXECUTE ON  [dbo].[ADD_SITE_UTILITY_ACCOUNT_P] TO [CBMSApplication]
GO
