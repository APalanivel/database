SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[SR_RFP_UPLOAD_SWITCH_UTILITY_FLOW_VERIFICATION_P]
	@userId INT,
	@sessionId VARCHAR(100),    
	--@cbmsImageDocId VARCHAR(200),     
	--@cbmsImage image ,    
	--@contentType VARCHAR(200),    
	@cbmsImageId INT,  -- added by Jaya    
	@accountGroupId INT,    
	@isBidGroup BIT,     
	@returnToTarrifDate DATETIME,
	@returnToTarrifTypeId INT ,    
	@isSwitchRate BIT,   
	@switchSupplierDate DATETIME,
	@switchSupplierTypeId INT,
	@isSwitchSupplier BIT, 
	@rfpId INT
AS    
BEGIN

	SET NOCOUNT ON
	
	DECLARE @entityId INT	    

	SELECT @entityId = Entity_ID FROM dbo.Entity (NOLOCK) WHERE Entity_Name = 'Flow Verification' AND Entity_Type = 100
	    
	/*    
	 INSERT INTO CBMS_IMAGE (CBMS_IMAGE_TYPE_ID, CBMS_DOC_ID, CBMS_IMAGE, CONTENT_TYPE, DATE_IMAGED)     
	 VALUES (@entityId, @cbmsImageDocId, @cbmsImage, @contentType, getDate())    
	    
	declare @cbmsImageId INT    
	select @cbmsImageId = (select @@Identity)  */    
	    
	--added by Jaya for updating entityid    
	     
	UPDATE dbo.CBMS_IMAGE SET CBMS_IMAGE_TYPE_ID = @entityId WHERE CBMS_IMAGE_ID = @cbmsImageId
	    
	INSERT INTO dbo.SR_RFP_UTILITY_SWITCH(SR_ACCOUNT_GROUP_ID,
		IS_BID_GROUP,
		RETURN_TO_TARIFF_DATE,
		RETURN_TO_TARIFF_TYPE_ID,
		IS_SWITCH_RATE_ESTIMATED,
		UTILITY_SWITCH_SUPPLIER_DATE,
		UTILITY_SWITCH_SUPPLIER_TYPE_ID,
		IS_SWITCH_SUPPLIER_ESTIMATED,
		FLOW_VERIFICATION_IMAGE_ID,
		UPLOADED_BY,UPLOADED_DATE)
	VALUES(@accountGroupId,
		@isBidGroup,
		@returnToTarrifDate,
		@returnToTarrifTypeId,
		@isSwitchRate,
		@switchSupplierDate,
		@switchSupplierTypeId,
		@isSwitchSupplier,
		@cbmsImageId,
		@userId,
		GETDATE())

	/*UPDATE SR_RFP_CHECKLIST     
	SET IS_VERIFY_FLOW = 1,    
	UTILITY_SWITCH_DEADLINE_DATE = @returnToTarrifDate    
	WHERE SR_RFP_ACCOUNT_ID =     
	 (SELECT SR_RFP_ACCOUNT_ID FROM SR_RFP_ACCOUNT WHERE SR_RFP_ACCOUNT_ID = @accountGroupId    
	 AND SR_RFP_ID = @rfpId) */    
	    
	IF (@isBidGroup = 0)
	 BEGIN
	 
		UPDATE dbo.SR_RFP_CHECKLIST
			SET IS_VERIFY_FLOW = 1,
				UTILITY_SWITCH_DEADLINE_DATE = @returnToTarrifDate,
				UTILITY_SWITCH_SUPPLIER_DATE = @switchSupplierDate
		WHERE SR_RFP_ACCOUNT_ID =  @accountGroupId
	 END
	ELSE IF (@isBidGroup > 0 )
	 BEGIN
	 
		UPDATE rfpCheckList
			SET rfpCheckList.IS_VERIFY_FLOW = 1,
				rfpCheckList.UTILITY_SWITCH_DEADLINE_DATE = @returnToTarrifDate,
				rfpCheckList.UTILITY_SWITCH_SUPPLIER_DATE = @switchSupplierDate
		FROM dbo.SR_RFP_CHECKLIST rfpCheckList
			INNER JOIN dbo.SR_RFP_ACCOUNT rfpAccount ON rfpAccount.SR_RFP_ACCOUNT_ID = rfpCheckList.SR_RFP_ACCOUNT_ID
		WHERE rfpAccount.SR_RFP_BID_GROUP_ID = @accountGroupId
	 
	 END

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_UPLOAD_SWITCH_UTILITY_FLOW_VERIFICATION_P] TO [CBMSApplication]
GO
