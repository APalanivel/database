SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/******************************************************************************************************    
NAME :	dbo.Get_Available_Sites_For_User  
   
DESCRIPTION: This procedure used to get all the available sites for a client with respect to the pertcular user.
And if any search parameter has value we have to filter ad select the sites.
   
 INPUT PARAMETERS:    
 Name             DataType               Default        Description    
--------------------------------------------------------------------      
@MyAccountId			INT								User Identification Number
@MyClientID				INT								User Client ID
@Keyword				VARCHAR				NULL		Search keyword
@StartRecordNumber		INT					0			Start of rersult set
@EndRecordNumber		INT				2147483647  		End of result set
@AssignedDivisions		VARCHAR				EMPTY		Assigned divisionId's from Right side list box
@AssignedSites			VARCHAR				EMPTY		Assigned SiteId's from Right side list box
  
 
 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
--------------------------------------------------------------------
site_id			INT			Site Identification number 
site_name		VARCHAR		Site name
division_Id		INT			Divition Identification number
  
AUTHOR INITIALS:    
 Initials Name    
-------------------------------------------------------------------    
KVK K VINAY KUMAR 
   
MODIFICATIONS     
 Initials Date  Modification    
--------------------------------------------------------------------  
 KVK		  08/25/2010		1.split the camma separated value by using "ufn_Split"
								2.Aligned the code properly
 BM			  08/27/2010		1.Optimized the query and coding standards are applied.
								2.Included the column SiteID_DivisionID in select
KVK 12/02/2010 @EndRecordNumber as set to 2147483647  
								
******************************************************************************************************/

CREATE  PROCEDURE [dbo].[Get_Available_Sites_For_User]
( 
 @MyAccountId INT
,@MyClientID INT
,@Keyword VARCHAR(100) = null
,@StartRecordNumber INT = 0
,@EndRecordNumber INT = 2147483647
,@AssignedDivisions VARCHAR(MAX) = ''
,@AssignedSites VARCHAR(MAX) = '' )
AS 
BEGIN  
    SET NOCOUNT ON ;  
	
	-- Assigned Divisions into a temp table 		
    SELECT
        Segments AS AssignedDivisionID
    INTO
        #AssignedDivisionIds
    FROM
        dbo.ufn_split(@AssignedDivisions, ',') ;    
		   
	-- Assigned sites into a temp table		
    SELECT
        Segments AS AssignedSiteID
    INTO
        #AssignedSiteIds
    FROM
        dbo.ufn_split(@AssignedSites, ',') ;     

    -- Select the Sites based on search and additional filters into a CTE
    WITH    tmpSitelist
              AS ( SELECT
                    S.SITE_ID
                   ,S.SITE_NAME
                   ,S.USER_INFO_ID
                   ,S.Client_ID
                   ,D.Division_Name
                   ,D.Division_Id
                   ,CAST(S.SITE_ID AS VARCHAR(100)) + '_' + CAST(D.Division_Id AS VARCHAR(100)) AS SiteID_DivisionID
                   ,ROW_NUMBER() OVER ( ORDER BY S.SITE_NAME ASC ) AS Record_Number
                   FROM
                    Division AS D
                    INNER JOIN [SITE] AS S
                        ON S.DIVISION_ID = D.Division_Id
                   WHERE
                    D.Client_Id = @MyClientID
                    AND D.Division_Id NOT IN ( SELECT
                                                AssignedDivisionID
                                               FROM
                                                #AssignedDivisionIds )
                    AND S.SITE_ID NOT IN ( SELECT
                                            AssignedSiteID
                                           FROM
                                            #AssignedSiteIds )
                    AND ( COALESCE(@Keyword, '') = ''
                          OR S.SITE_NAME like '%' + @Keyword + '%' ))
        SELECT
            SITE_ID
           ,SITE_NAME
           ,Division_Id
           ,SiteID_DivisionID
        FROM
            tmpSitelist
        WHERE
            Record_Number BETWEEN @StartRecordNumber
                          AND     @EndRecordNumber ;  
	
    DROP TABLE #AssignedDivisionIds ;
    DROP TABLE #AssignedSiteIds ;
END  


GO
GRANT EXECUTE ON  [dbo].[Get_Available_Sites_For_User] TO [CBMSApplication]
GO
