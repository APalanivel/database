SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.Commodity_Sel_By_Site_Id

DESCRIPTION:

	Used to Select all Commodity associated with the given Site_Id 
	

INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@Site_Id			int						

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Commodity_Sel_By_Site_Id 35
	EXEC dbo.Commodity_Sel_By_Site_Id 1573
	EXEC dbo.Commodity_Sel_By_Site_Id 2364
	

	

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
    RKV         Ravi Kumar Vegesna

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	RKV        	2017-01-20	Created for Invoice Collection
******/

CREATE PROCEDURE [dbo].[Commodity_Sel_By_Site_Id] ( @Site_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
	
      SELECT
            cha.Commodity_Id
           ,comm.Commodity_Name
      FROM
            core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account cha
                  ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN dbo.Commodity comm
                  ON cha.Commodity_Id = comm.Commodity_Id
      WHERE
            ch.Site_Id = @Site_Id
      GROUP BY
            cha.Commodity_Id
           ,comm.Commodity_Name
      ORDER BY
            comm.Commodity_Name

END;
;
GO
GRANT EXECUTE ON  [dbo].[Commodity_Sel_By_Site_Id] TO [CBMSApplication]
GO
