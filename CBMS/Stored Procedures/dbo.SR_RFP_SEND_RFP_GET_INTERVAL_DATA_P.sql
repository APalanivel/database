SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
      dbo.SR_RFP_SEND_RFP_GET_INTERVAL_DATA_P

DESCRIPTION:
	this object used to bring the RFP interval data 

INPUT PARAMETERS:
      Name              	DataType          Default     Description
------------------------------------------------------------
	@user_id 		VARCHAR(10),  
	@session_id 		VARCHAR(20),  
	@sr_rfp_account_id 	INT        

OUTPUT PARAMETERS:
      Name              DataType          Default     Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	 exec SR_RFP_SEND_RFP_GET_INTERVAL_DATA_P '','',16106

AUTHOR INITIALS:
      Initials    Name
------------------------------------------------------------
	SKA 	Shobhit Kumar Agrawal
 
MODIFICATIONS

      Initials    Date			Modification
------------------------------------------------------------
      SKA           22-Jul-2009 Modified as per coding standards
	  							cbms_image column removed from select query to fix the bug # 10151.

*/
CREATE PROCEDURE dbo.SR_RFP_SEND_RFP_GET_INTERVAL_DATA_P  
	@user_id VARCHAR(10),
	@session_id VARCHAR(20), 
	@sr_rfp_account_id INT
AS  
BEGIN
	
	SET NOCOUNT ON

	SELECT ird.sr_rfp_lp_interval_data_id,
		ird.interval_name,
		cbms_img.cbms_doc_id,
		cbms_img.cbms_image_id
  	FROM dbo.sr_rfp_lp_interval_data ird (NOLOCK)
		INNER JOIN dbo.cbms_image cbms_img (NOLOCK)
			ON ird.cbms_image_id = cbms_img.cbms_image_id
		INNER JOIN dbo.sr_rfp_account rfp_account (NOLOCK)
			ON rfp_account.sr_rfp_account_id = ird.sr_rfp_account_id
	WHERE ird.sr_rfp_account_id = @sr_rfp_account_id  
		AND rfp_account.is_deleted = 0  

END
  
  






GO
GRANT EXECUTE ON  [dbo].[SR_RFP_SEND_RFP_GET_INTERVAL_DATA_P] TO [CBMSApplication]
GO
