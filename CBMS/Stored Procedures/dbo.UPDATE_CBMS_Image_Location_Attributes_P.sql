SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UPDATE_CBMS_Image_Location_Attributes_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@CBMS_Image_Location_Id	int       	          	
	@Active_Directory	varchar(255)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE dbo.UPDATE_CBMS_Image_Location_Attributes_P(@CBMS_Image_Location_Id INT
	, @Active_Directory VARCHAR(255)
	)
AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE dbo.CBMS_Image_Location
		SET Active_Directory = @Active_Directory
			, Active_File_Cnt = 0
	WHERE CBMS_Image_Location_Id = @CBMS_Image_Location_Id
		AND Active_Directory <> @Active_Directory

END
GO
GRANT EXECUTE ON  [dbo].[UPDATE_CBMS_Image_Location_Attributes_P] TO [CBMSApplication]
GO
