SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE    Procedure [dbo].[cbmsSessionInfo_GetWorking]
 ( @session_info_id int )
AS
BEGIN

	   select si.session_info_id
		, si.user_info_id
		, si.session_xml
	     from session_info si with (nolock)
	    where si.session_info_id = @session_info_id


END
GO
GRANT EXECUTE ON  [dbo].[cbmsSessionInfo_GetWorking] TO [CBMSApplication]
GO
