SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********             
NAME:  dbo.Site_Commodity_Analyst_Merge            
           
DESCRIPTION:       
      
Used to Insert/update the data into dbo.Site_Commodity_Analyst_Merge table.      
          
INPUT PARAMETERS:              
      Name     DataType          Default     Description              
------------------------------------------------------------              
@Site_Commodity_Analyst  Site_Commodity_Analyst_Type      
              
              
OUTPUT PARAMETERS:              
      Name              DataType          Default     Description              
------------------------------------------------------------              
              
USAGE EXAMPLES:         
      
BEGIN TRAN      
select * from Site_Commodity_Analyst where site_id=1704 and commodity_id=290     
DECLARE @Site_Commodity_Analyst AS Site_Commodity_Analyst_Type      
INSERT into @Site_Commodity_Analyst      
values(39899,290,105)      
EXEC DBO.Site_Commodity_Analyst_Merge @Site_Commodity_Analyst      
select * from Site_Commodity_Analyst where site_id=1704 and commodity_id=290     
ROLLBACK TRAN      
      
SELECT  top 10  a.Site_Id,b.Commodity_Id FROM core.Client_Hier a JOIN core.Client_Hier_Account b ON a.Client_Hier_Id = b.Client_Hier_Id       
WHERE a.Site_Closed=0 AND a.Site_Not_Managed=0  GROUP BY a.Site_Id,b.Commodity_Id order by  a.Site_Id desc      
      
select * from Site_Commodity_Analyst   where site_Id = 1704 and commodity_Id = 290    
------------------------------------------------------------        
          
AUTHOR INITIALS:            
Initials Name            
------------------------------------------------------------            
BCH   Balaraju  Chalumuri      
          
          
Initials Date  Modification            
------------------------------------------------------------            
BCH  2012-09-17  Created new sp (for POCO Project)      
******/      
CREATE PROCEDURE dbo.Site_Commodity_Analyst_Merge
      ( 
       @Site_Commodity_Analyst AS Site_Commodity_Analyst_Type READONLY )
AS 
BEGIN      
      SET NOCOUNT ON ;      


      BEGIN TRY                
            BEGIN TRAN  

            MERGE dbo.Site_Commodity_Analyst AS Tgt
                  USING 
                        ( SELECT
                              sca.Site_Id AS Site_Id
                             ,sca.Commodity_Id AS Commodity_Id
                             ,sca.Analyst_User_Info_Id AS Analyst_User_Info_Id
                          FROM
                              @Site_Commodity_Analyst sca ) AS Src
                  ON Tgt.Site_Id = Src.Site_Id
                        AND Tgt.Commodity_Id = Src.Commodity_Id
                  WHEN MATCHED AND src.Analyst_User_Info_Id IS NOT NULL
                        THEN       
                  UPDATE SET  
                              Tgt.Analyst_User_Info_Id = Src.Analyst_User_Info_Id
                  WHEN MATCHED AND src.Analyst_User_Info_Id IS NULL
                        THEN DELETE
                  WHEN NOT MATCHED AND Src.Analyst_User_Info_Id IS NOT NULL
                        THEN       
                       INSERT
                              ( 
                               Site_Id
                              ,Commodity_Id
                              ,Analyst_User_Info_Id )
                         VALUES
                              ( 
                               Src.Site_Id
                              ,Src.Commodity_Id
                              ,Src.Analyst_User_Info_Id ) ;     
       
            DELETE
                  aca
            FROM
                  dbo.ACCOUNT acc
                  INNER JOIN @Site_Commodity_Analyst sca
                        ON sca.site_Id = acc.SITE_ID
                  INNER JOIN dbo.Account_Commodity_Analyst aca
                        ON acc.ACCOUNT_ID = aca.Account_Id
                           AND sca.commodity_Id = aca.Commodity_Id
      
            COMMIT TRAN                
      END TRY                
      BEGIN CATCH                
            IF @@TRANCOUNT > 0 
                  ROLLBACK TRAN                
                
            EXEC dbo.usp_RethrowError                
                
      END CATCH                
      
                        
END 


;
GO
GRANT EXECUTE ON  [dbo].[Site_Commodity_Analyst_Merge] TO [CBMSApplication]
GO
