SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******************************************************************************************************         

NAME: dbo.Client_Document_Category_Sel_By_ClientId

    
DESCRIPTION:    

      To get both client-specific and global categories for documents search 


INPUT PARAMETERS:    

     NAME					DATATYPE		DEFAULT           DESCRIPTION    
------------------------------------------------------------------------    
	@Client_Id				INT		

                    

OUTPUT PARAMETERS:              

      NAME              DATATYPE    DEFAULT           DESCRIPTION       
------------------------------------------------------------              


USAGE EXAMPLES:              

EXEC dbo.Client_Document_Category_Sel_By_ClientId 616

	 

AUTHOR INITIALS:              

      INITIALS    NAME              
------------------------------------------------------------              
      RMG         Rani Mary George


MODIFICATIONS:    

      INITIALS		DATE				MODIFICATION              
------------------------------------------------------------              
      RMG 			11-SEP-14			CREATED

******************************************************************************************************/ 
CREATE PROCEDURE [dbo].[Client_Document_Category_Sel_By_ClientId] 
	( 
		@Client_Id INT 
	)
AS 
BEGIN
	
      SET NOCOUNT ON ;   

      SELECT
            Client_Document_Category_Id
           ,Category_Name
      FROM
            dbo.Client_Document_Category
      WHERE
            Client_Id = @Client_Id
            OR Client_Id = -1
      ORDER BY
            Category_Name
				

END





;
GO
GRANT EXECUTE ON  [dbo].[Client_Document_Category_Sel_By_ClientId] TO [CBMSApplication]
GO
