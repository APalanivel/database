SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE dbo.UPDATE_SEASON_P
	@seasonName VARCHAR(200),
	@seasonFromDate DATETIME,
	@seasonToDate DATETIME,
	@seasonId INT
AS
BEGIN

	SET NOCOUNT ON

	UPDATE dbo.season
		SET season_name = @seasonName ,
			season_from_date = @seasonFromDate ,
			season_to_date = @seasonToDate
	WHERE season_id = @seasonId 

END
GO
GRANT EXECUTE ON  [dbo].[UPDATE_SEASON_P] TO [CBMSApplication]
GO
