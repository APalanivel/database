SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_SET_SUMMIT_BOC_FOR_SUPPLIER_PLACE_BID_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                 DataType          Default     Description    
---------------------------------------------------------------------------------    
@bidRequirementId          int,
@delivery                  varchar(200),
@transServiceLevelId       int,
@supplierNominationId      int,
@supplierBalancingId       int,
@summitComments            varchar(4000),
@deliveryPowerId           int,
@transServiceLevelPowerId  int,
@selectedProductId         int,
@accountTermId             int,
@productName               varchar(200),
@supplierContactId         INT
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
--EXEC [dbo].[SR_RFP_SET_SUMMIT_BOC_FOR_SUPPLIER_PLACE_BID_P] 
--@bidRequirementId =0,
--@delivery = 'Nicor Citygate',
--@transServiceLevelId = 1077 ,
--@supplierNominationId = 1081,
--@supplierBalancingId = 1081,
--@summitComments = 'Test Data' ,

--@deliveryPowerId = 1148,
--@transServiceLevelPowerId = 1080,

--@selectedProductId =  4099,
--@accountTermId = 26360,
--@productName = 'Test Data',
--@supplierContactId = NULL


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[SR_RFP_SET_SUMMIT_BOC_FOR_SUPPLIER_PLACE_BID_P] 
@bidRequirementId int,
@delivery varchar(200),
@transServiceLevelId int,
@supplierNominationId int,
@supplierBalancingId int,
@summitComments varchar(4000),
@deliveryPowerId int,
@transServiceLevelPowerId int,
@selectedProductId int,
@accountTermId int,
@productName varchar(200),
@supplierContactId INT


as


set nocount on

declare @newIdentity int
declare @newIdentity1 int
DECLARE @isSubProduct int
DECLARE @prodTermMapId int


if @productName is not null
	begin
		select @isSubProduct=1
	end
else
	begin
		select @isSubProduct=null
	end

if @bidRequirementId=0

BEGIN
 
	
		INSERT INTO SR_RFP_BID_REQUIREMENTS 
		(
			DELIVERY_POINT,
			TRANSPORTATION_LEVEL_TYPE_ID,
			NOMINATION_TYPE_ID,
			BALANCING_TYPE_ID,
			COMMENTS,
			DELIVERY_POINT_POWER_TYPE_ID,
			SERVICE_LEVEL_POWER_TYPE_ID
		)  
		
		VALUES
		
		(
			@delivery,
			@transServiceLevelId,
			@supplierNominationId,
			@supplierBalancingId,
			@summitComments,
			@deliveryPowerId ,
			@transServiceLevelPowerId 
			
		)
		
		SELECT @newIdentity=SCOPE_IDENTITY()

		INSERT INTO SR_RFP_BID 
		(
			PRODUCT_NAME,
			SR_RFP_BID_REQUIREMENTS_ID,
			SR_RFP_SUPPLIER_PRICE_COMMENTS_ID,
			SR_RFP_SUPPLIER_SERVICE_ID,
			IS_SUB_PRODUCT
		)  
		
		VALUES
		
		(
			@productName,
			@newIdentity,
			null,
			null,
			@isSubProduct
		)		
		select @newIdentity1=SCOPE_IDENTITY()

		INSERT INTO SR_RFP_TERM_PRODUCT_MAP 
		(
			SR_RFP_ACCOUNT_TERM_ID,
			SR_RFP_SELECTED_PRODUCTS_ID,
			SR_RFP_BID_ID,
			SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
		)
		VALUES 
		(
			@accountTermId,
			@selectedProductId,
			@newIdentity1,
			@supplierContactId
		)

		

END

--UPDATE operation

else if @bidRequirementId>0

begin
		
		UPDATE 
			SR_RFP_BID_REQUIREMENTS SET 
			DELIVERY_POINT=@delivery,
			TRANSPORTATION_LEVEL_TYPE_ID=@transServiceLevelId,
			NOMINATION_TYPE_ID=@supplierNominationId,
			BALANCING_TYPE_ID=@supplierBalancingId,
			COMMENTS=@summitComments,
			DELIVERY_POINT_POWER_TYPE_ID=@deliveryPowerId,
			SERVICE_LEVEL_POWER_TYPE_ID=@transServiceLevelPowerId
		WHERE
			SR_RFP_BID_REQUIREMENTS_ID=@bidRequirementId


		UPDATE  
			SR_RFP_BID 
		SET 
			PRODUCT_NAME=@productName 
		WHERE 
			SR_RFP_BID_REQUIREMENTS_ID=@bidRequirementId


		
end
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_SET_SUMMIT_BOC_FOR_SUPPLIER_PLACE_BID_P] TO [CBMSApplication]
GO
