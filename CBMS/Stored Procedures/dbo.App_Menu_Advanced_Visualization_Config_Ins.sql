SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME: [dbo].[App_Menu_Advanced_Visualization_Config_Ins] 
 
DESCRIPTION:
	Adding a Menu to published Qlik App

INPUT PARAMETERS:
Name			DataType	Default  Description
------------------------------------------------------------------------------------

OUTPUT PARAMETERS:
Name			DataType    Default   Description
-------------------------------------------------------------------------------------

USAGE EXAMPLES:                                  
--------------------------------------------------------------------------------------
BEGIN TRAN

	EXEC [dbo].[App_Menu_Advanced_Visualization_Config_Ins] 
		@Client_Id = 11278,
		@Advanced_Visualization_URL = 'sense/app/a719dbfb-5d8d-40ff-ac3e-86771942b46a/sheet/TtvyYzz/state/analysis',
		@Report_Title_Key = 'InvoiceCollection',
		@Report_Desc_Key = 'ICdesc',
		@Report_Bread_Crumb_Key = 'InvoiceCollection',
		@App_Menu_Id = 557,
		@User_Id = 16															

ROLLBACK

AUTHOR INITIALS:

Initials	Name
--------------------------------------------------------------------------------------
MSV			Muhamed Shahid V

MODIFICATIONS:
Initials	Date			Modification
-------------------------------------------------------------------------------------
MSV			14 May 2019		Created

******/ 
CREATE PROCEDURE [dbo].[App_Menu_Advanced_Visualization_Config_Ins]
(
	@Client_Id INT,
	@Advanced_Visualization_URL NVARCHAR(MAX),
	@Report_Title_Key NVARCHAR(255),
	@Report_Desc_Key NVARCHAR(200),
	@Report_Bread_Crumb_Key NVARCHAR(200),
	@App_Menu_Id INT,
	@User_Id INT
)
AS
BEGIN
	SET NOCOUNT ON
	  
	BEGIN TRY
		BEGIN TRAN

		DECLARE @Parent_Menu_Id INT,
				@AMAVCId INT,
				@Base_URL NVARCHAR(MAX) = '/MyData/AdvancedVisualization/GlobalReport.aspx'
		
		INSERT INTO dbo.App_Menu_Advanced_Visualization_Config
		(
			App_Menu_Id,
			Base_URL,
			Advanced_Visualization_URL,
			Report_Title_Key,
			Report_Desc_Key,
			Report_Bread_Crumb_Key,
			Created_User_Id,
			Created_Ts,
			Updated_User_Id,
			Last_Change_Ts
		)
		SELECT @App_Menu_Id,
				@Base_URL,
				@Advanced_Visualization_URL,
				@Report_Title_Key,
				@Report_Desc_Key,
				@Report_Bread_Crumb_Key,
				@User_Id,
				GETDATE(),
				@User_Id,
				GETDATE()
		WHERE NOT EXISTS
		(
			SELECT 1
			FROM dbo.App_Menu_Advanced_Visualization_Config amavc
			WHERE amavc.Advanced_Visualization_URL = @Advanced_Visualization_URL
		);

		INSERT INTO dbo.App_Menu_Client_Config
		(
			APP_MENU_ID,
			App_Menu_Advanced_Visualization_Config_Id,
			Client_Id,
			Created_User_Id,
			Created_Ts,
			Updated_User_Id,
			Last_Change_Ts
		)
		SELECT @App_Menu_Id,
				amavc.App_Menu_Advanced_Visualization_Config_Id,
				@Client_Id,
				@User_Id,
				GETDATE(),
				@User_Id,
				GETDATE()
		FROM dbo.App_Menu_Advanced_Visualization_Config amavc
		WHERE amavc.Advanced_Visualization_URL = @Advanced_Visualization_URL
		AND NOT EXISTS
		(
			SELECT 1
			FROM dbo.App_Menu_Client_Config amcc
			WHERE amcc.App_Menu_Advanced_Visualization_Config_Id = amavc.App_Menu_Advanced_Visualization_Config_Id
		);

		SELECT @Parent_Menu_Id = am.PARENT_MENU_ID
		FROM dbo.APP_MENU am
		WHERE am.APP_MENU_ID = @App_Menu_Id

		SELECT @AMAVCId = App_Menu_Advanced_Visualization_Config_Id
		FROM dbo.App_Menu_Advanced_Visualization_Config
		WHERE App_Menu_Id = @Parent_Menu_Id;

		INSERT INTO dbo.App_Menu_Client_Config
		(
			APP_MENU_ID,
			App_Menu_Advanced_Visualization_Config_Id,
			Client_Id,
			Created_User_Id,
			Created_Ts,
			Updated_User_Id,
			Last_Change_Ts
		)
		SELECT @Parent_Menu_Id,
				@AMAVCId,
				amcc.Client_Id,
				@User_Id,
				GETDATE(),
				@User_Id,
				GETDATE()
		FROM dbo.App_Menu_Client_Config amcc
		WHERE amcc.APP_MENU_ID = @App_Menu_Id
			AND @AMAVCId IS NOT NULL
			AND NOT EXISTS
			(
				SELECT 1
				FROM dbo.App_Menu_Client_Config amcc2
				WHERE amcc2.APP_MENU_ID = @Parent_Menu_Id
						AND amcc2.Client_Id = amcc.Client_Id
			);
			
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
				BEGIN
					ROLLBACK
					EXEC dbo.usp_RethrowError
				END
	END CATCH		
END;
GO
GRANT EXECUTE ON  [dbo].[App_Menu_Advanced_Visualization_Config_Ins] TO [CBMSApplication]
GO
