SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[cbmsWatchList_Add]
	( @MyAccountId int
	, @account_id int
	, @comments varchar(4000)
	)
AS
BEGIN

set nocount on

  declare @this_id int

	select @this_id = watch_list_id
	  from watch_list
	 where account_id = @account_id

if @this_id is null
begin

	insert into watch_list (user_info_id, account_id, comments)
		values (@MyAccountId, @account_id, @comments)


	set @this_id = @@IDENTITY
end

exec cbmsWatchList_Get @MyAccountId, @this_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsWatchList_Add] TO [CBMSApplication]
GO
