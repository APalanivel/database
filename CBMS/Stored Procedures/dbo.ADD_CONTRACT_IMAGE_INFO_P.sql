SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
                           
 NAME:  dbo.ADD_CONTRACT_IMAGE_INFO_P                        
                            
 DESCRIPTION:        
				TO get the roles information for clint app acess role table .                            
                            
 INPUT PARAMETERS:        
                           
 Name                                   DataType        Default       Description        
----------------------------------------------------------------------------------
 @cbms_mage_id				             INT          
 @description                            INT
 @document_type_id						 INT
                         
 OUTPUT PARAMETERS:        
                                 
 Name                                   DataType        Default       Description        
----------------------------------------------------------------------------------
                            
 USAGE EXAMPLES:                                
----------------------------------------------------------------------------------
       
 Exec  dbo.ADD_CONTRACT_IMAGE_INFO_P 11 ,235         
                           
 AUTHOR INITIALS:      
         
 Initials               Name        
-----------------------------------------------------------------------------------
 NR                     Narayana Reddy                              
                             
 MODIFICATIONS:      
                             
 Initials               Date            Modification      
------------------------------------------------------------------------------------
 NR                     2019-10-18      Created for Add Contract.                     
                           
******/

CREATE PROCEDURE [dbo].[ADD_CONTRACT_IMAGE_INFO_P]
    (
        @cbms_mage_id INT
        , @description VARCHAR(500)
        , @document_type_id INT
        , @contract_id INT
        , @signature_type_id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Exception_Status_Cd INT;

        SELECT
            @Exception_Status_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Status'
            AND cs.Std_Column_Name = 'Exception_Status_Cd'
            AND c.Code_Value = 'Closed';





        INSERT INTO dbo.CONTRACT_CBMS_IMAGE_MAP
             (
                 CBMS_IMAGE_ID
                 , DESCRIPTION
                 , DOCUMENT_TYPE_ID
                 , CONTRACT_ID
                 , SIGNATURE_TYPE_ID
             )
        VALUES
            (@cbms_mage_id
             , @description
             , @document_type_id
             , @contract_id
             , @signature_type_id);




        UPDATE
            ce
        SET
            ce.Exception_Status_Cd = @Exception_Status_Cd
            , ce.Last_Change_Ts = GETDATE()
        FROM
            dbo.Contract_Exception ce
            INNER JOIN dbo.Code c
                ON c.Code_Id = ce.Exception_Type_Cd
        WHERE
            ce.Contract_Id = @contract_id
            AND c.Code_Value = 'Missing Contract Image';





    END;




GO
GRANT EXECUTE ON  [dbo].[ADD_CONTRACT_IMAGE_INFO_P] TO [CBMSApplication]
GO
