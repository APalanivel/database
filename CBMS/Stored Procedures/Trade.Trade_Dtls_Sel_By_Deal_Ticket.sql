SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.Trade_Dtls_Sel_By_Deal_Ticket                        
                          
Description:                          
            
                          
Input Parameters:                          
    Name   DataType        Default     Description                            
--------------------------------------------------------------------------------    
 @Deal_Ticket_Id INT  
                          
Output Parameters:                                
 Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
     
                        
Usage Examples:                              
--------------------------------------------------------------------------------    
     
       Exec Trade.Trade_Dtls_Sel_By_Deal_Ticket  131861  
       Exec Trade.Trade_Dtls_Sel_By_Deal_Ticket  131893  
       Exec Trade.Trade_Dtls_Sel_By_Deal_Ticket  131477  
       Exec Trade.Trade_Dtls_Sel_By_Deal_Ticket  132175  
                         
 Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy            
                           
 Modifications:                          
    Initials	Date           Modification                          
--------------------------------------------------------------------------------    
    RR			2019-03-22     Global Risk Management - Created                        
	RR			2019-03-22     GRM-1533 - Perfomamce tuning, removed multiple references to the table 
								Trade.Deal_Ticket_Client_Hier_TXN_Status, loaded required into temp table and used further in query
                         
******/
CREATE PROCEDURE [Trade].[Trade_Dtls_Sel_By_Deal_Ticket]
    (
        @Deal_Ticket_Id INT
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
    )
WITH RECOMPILE
AS
    BEGIN
        SET NOCOUNT ON;

        CREATE TABLE #Trade_NumStatus
             (
                 Deal_Ticket_Id INT
                 , Trade_Id INT
                 , Trade_Status NVARCHAR(255)
                 , Rnk INT
                 , Trade_Executed_Ts DATETIME
                 , Client_Hier_Id INT
             );

        CREATE TABLE #Trade_Dtls
             (
                 Trade_Id INT
                 , Site_Name VARCHAR(800)
                 , Client_Hier_Id INT
                 , Trade_Status NVARCHAR(255)
                 , Trade_Price VARCHAR(50)
                 , Counter_Party VARCHAR(200)
                 , Row_Num INT
                 , Cbms_Counterparty_Id INT
                 , Client_Id INT
                 , Sitegroup_Id INT
                 , Site_Id INT
                 , Trade_Executed_Ts DATETIME
             );

        CREATE TABLE #Counter_Party
             (
                 Trade_Id INT
                 , Counter_Party VARCHAR(200)
                 , Cbms_Counterparty_Id INT
             );

        CREATE TABLE #Trade_Vols
             (
                 Trade_Id INT
                 , Total_Volume DECIMAL(28, 0)
             );

        DECLARE
            @Total_Cnt INT
            , @Hedge_Type VARCHAR(200)
            , @Legacy_DT_Order_Placed_Dt DATE
            , @Legacy_DT_Order_Executed_Dt DATE;

        SELECT
            @Legacy_DT_Order_Placed_Dt = MAX(CASE WHEN e.ENTITY_NAME = 'Order Placed' THEN rmdtts.DEAL_TRANSACTION_DATE
                                             END)
            , @Legacy_DT_Order_Executed_Dt = ISNULL(
                                                 MAX(rdt.VERBAL_CONFIRMATION_DATE)
                                                 , MAX(CASE WHEN e.ENTITY_NAME = 'Order Executed' THEN
                                                                rmdtts.DEAL_TRANSACTION_DATE
                                                       END))
        FROM
            dbo.RM_DEAL_TICKET rdt
            INNER JOIN RM_DEAL_TICKET_TRANSACTION_STATUS rmdtts
                ON rmdtts.RM_DEAL_TICKET_ID = rdt.RM_DEAL_TICKET_ID
            INNER JOIN ENTITY e
                ON e.ENTITY_ID = rmdtts.DEAL_TRANSACTION_STATUS_TYPE_ID
        WHERE
            rmdtts.RM_DEAL_TICKET_ID = @Deal_Ticket_Id
            AND e.ENTITY_NAME IN ( 'Order Placed', 'Order Executed' );

        SELECT
            @Hedge_Type = et.ENTITY_NAME
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN dbo.ENTITY et
                ON dt.Hedge_Type_Cd = et.ENTITY_ID
        WHERE
            dt.Deal_Ticket_Id = @Deal_Ticket_Id;

        SELECT
            *
        INTO
            #Deal_Ticket_Client_Hier_TXN_Status
        FROM
            Trade.Deal_Ticket_Client_Hier_TXN_Status dtchts
        WHERE
            EXISTS (   SELECT
                            1
                       FROM
                            Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtchvd
                       WHERE
                            dtchvd.Deal_Ticket_Id = @Deal_Ticket_Id
                            AND dtchts.Cbms_Trade_Number = dtchvd.Trade_Number);

        SELECT
            dtchts.Cbms_Trade_Number
            , dtchts.Assignee
        INTO
            #RM
        FROM
            #Deal_Ticket_Client_Hier_TXN_Status dtchts
            INNER JOIN
            (   SELECT
                    dtchts2.Cbms_Trade_Number
                    , MAX(dtchts2.Id) AS id
                FROM
                    #Deal_Ticket_Client_Hier_TXN_Status dtchts2
                GROUP BY
                    dtchts2.Cbms_Trade_Number) txn2
                ON txn2.Cbms_Trade_Number = dtchts.Cbms_Trade_Number
                   AND  txn2.id = dtchts.Id;
        INSERT INTO #Counter_Party
             (
                 Trade_Id
                 , Counter_Party
                 , Cbms_Counterparty_Id
             )
        SELECT
            trdtxn.Cbms_Trade_Number
            , vndr.VENDOR_NAME
            , trdtxn.Cbms_Counterparty_Id
        FROM
            #Deal_Ticket_Client_Hier_TXN_Status trdtxn
            INNER JOIN dbo.VENDOR vndr
                ON vndr.VENDOR_ID = trdtxn.Cbms_Counterparty_Id
                   AND  @Hedge_Type = 'Physical'
        WHERE
            trdtxn.Cbms_Counterparty_Id IS NOT NULL
        GROUP BY
            trdtxn.Cbms_Trade_Number
            , vndr.VENDOR_NAME
            , trdtxn.Cbms_Counterparty_Id;

        INSERT INTO #Counter_Party
             (
                 Trade_Id
                 , Counter_Party
                 , Cbms_Counterparty_Id
             )
        SELECT
            trdtxn.Cbms_Trade_Number
            , rmcp.COUNTERPARTY_NAME
            , trdtxn.Cbms_Counterparty_Id
        FROM
            #Deal_Ticket_Client_Hier_TXN_Status trdtxn
            INNER JOIN dbo.RM_COUNTERPARTY rmcp
                ON rmcp.RM_COUNTERPARTY_ID = trdtxn.Cbms_Counterparty_Id
                   AND  @Hedge_Type = 'Financial'
        WHERE
            trdtxn.Cbms_Counterparty_Id IS NOT NULL
        GROUP BY
            trdtxn.Cbms_Trade_Number
            , rmcp.COUNTERPARTY_NAME
            , trdtxn.Cbms_Counterparty_Id;


        INSERT INTO #Trade_NumStatus
             (
                 Deal_Ticket_Id
                 , Trade_Id
                 , Trade_Status
                 , Rnk
                 , Trade_Executed_Ts
                 , Client_Hier_Id
             )
        SELECT
            @Deal_Ticket_Id
            , dtchvd.Trade_Number AS Trade_Id
            , ws.Workflow_Status_Name AS Trade_Status
            , CASE WHEN ws.Workflow_Status_Name = 'Order Placed' THEN 1
                  WHEN ws.Workflow_Status_Name = 'Order Executed' THEN 2
                  WHEN ws.Workflow_Status_Name = 'Canceled' THEN 3
                  WHEN ws.Workflow_Status_Name = 'Expired' THEN 4
                  WHEN ws.Workflow_Status_Name = 'Bid Emails Generated' THEN 5
                  WHEN ws.Workflow_Status_Name = 'Marked to cancel' THEN 6
                  WHEN ws.Workflow_Status_Name = 'Assigned to trader' THEN 7
                  WHEN ws.Workflow_Status_Name = 'Back to CM' THEN 8
                  WHEN ws.Workflow_Status_Name = 'Not supported' THEN 9
                  WHEN ws.Workflow_Status_Name = 'Ready to trade' THEN 10
                  ELSE 11
              END AS Rnk
            , MAX(dtchvd.Trade_Executed_Ts) AS Trade_Executed_Ts
            , dtchvd.Client_Hier_Id
        FROM
            Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtchvd
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dtch.Client_Hier_Id = dtchvd.Client_Hier_Id
                   AND  dtch.Deal_Ticket_Id = dtchvd.Deal_Ticket_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status dtchws
                ON dtchws.Deal_Ticket_Client_Hier_Id = dtch.Deal_Ticket_Client_Hier_Id
            INNER JOIN Trade.Workflow_Status_Map wsm
                ON wsm.Workflow_Status_Map_Id = dtchws.Workflow_Status_Map_Id
            INNER JOIN Trade.Workflow_Status ws
                ON ws.Workflow_Status_Id = wsm.Workflow_Status_Id
        WHERE
            dtchvd.Deal_Ticket_Id = @Deal_Ticket_Id
            AND dtchws.Is_Active = 1
            AND (   dtchws.Trade_Month IS NULL
                    OR  dtchws.Trade_Month = dtchvd.Deal_Month)
            AND (   dtchws.Contract_Id IS NULL
                    OR  dtchws.Contract_Id = dtchvd.Contract_Id)
        GROUP BY
            dtchvd.Trade_Number
            , ws.Workflow_Status_Name
            , dtchws.Is_Active
            , dtchvd.Client_Hier_Id;


        INSERT INTO #Trade_Dtls
             (
                 Trade_Id
                 , Site_Name
                 , Client_Hier_Id
                 , Trade_Status
                 , Trade_Price
                 , Counter_Party
                 , Row_Num
                 , Cbms_Counterparty_Id
                 , Client_Id
                 , Sitegroup_Id
                 , Site_Id
                 , Trade_Executed_Ts
             )
        SELECT
            tn.Trade_Id AS Trade_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_Name
            , ch.Client_Hier_Id
            , CASE WHEN COUNT(DISTINCT tn.Trade_Status) > 1 THEN 'Multiple'
                  ELSE tn.Trade_Status
              END AS Trade_Status
            , CASE WHEN COUNT(DISTINCT ttp.Trade_Price) > 1 THEN 'Multiple'
                  ELSE MAX(CAST(ttp.Trade_Price AS VARCHAR(20)))
              END AS Trade_Price
            , cp.Counter_Party
            , DENSE_RANK() OVER (ORDER BY
                                     tn.Trade_Id DESC)
            , cp.Cbms_Counterparty_Id
            , ch.Client_Id
            , ch.Sitegroup_Id
            , ch.Site_Id
            , tn.Trade_Executed_Ts
        FROM
            #Trade_NumStatus tn
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtchvd
                ON dtchvd.Trade_Number = tn.Trade_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dtch.Client_Hier_Id = dtchvd.Client_Hier_Id
                   AND  dtch.Deal_Ticket_Id = dtchvd.Deal_Ticket_Id
            INNER JOIN Core.Client_Hier ch
                ON dtch.Client_Hier_Id = ch.Client_Hier_Id
            LEFT JOIN Trade.Trade_Price ttp
                ON dtchvd.Trade_Price_Id = ttp.Trade_Price_Id
            LEFT JOIN #Counter_Party cp
                ON cp.Trade_Id = tn.Trade_Id
            INNER JOIN
            (   SELECT
                    tns.Trade_Id
                    , MIN(tns.Rnk) AS Rnk
                FROM
                    #Trade_NumStatus tns
                GROUP BY
                    tns.Trade_Id) minrnk
                ON tn.Trade_Id = minrnk.Trade_Id
                   AND  tn.Rnk = minrnk.Rnk
        GROUP BY
            ch.Client_Hier_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
            , tn.Trade_Status
            , cp.Counter_Party
            , tn.Trade_Id
            , cp.Cbms_Counterparty_Id
            , ch.Client_Id
            , ch.Sitegroup_Id
            , ch.Site_Id
            , tn.Trade_Executed_Ts;

        INSERT INTO #Trade_Vols
             (
                 Trade_Id
                 , Total_Volume
             )
        SELECT
            vol.Trade_Number
            , SUM(vol.Total_Volume) AS Total_Volume
        FROM
            Trade.Deal_Ticket_Client_Hier_Volume_Dtl vol
        WHERE
            vol.Deal_Ticket_Id = @Deal_Ticket_Id
        GROUP BY
            vol.Trade_Number;

        SELECT  @Total_Cnt = MAX(Row_Num)FROM   #Trade_Dtls;

        SELECT
            trd.Trade_Id
            , CASE WHEN COUNT(DISTINCT trd.Client_Hier_Id) > 1 THEN 'Multiple'
                  ELSE MAX(trd.Site_Name)
              END AS Site_Name
            , MAX(tv.Total_Volume) AS Total_Volume
            , Trade_Status
            , CASE WHEN COUNT(DISTINCT trd.Trade_Price) > 1 THEN 'Multiple'
                  ELSE MAX(trd.Trade_Price)
              END AS Trade_Price
            , CASE WHEN COUNT(DISTINCT trd.Cbms_Counterparty_Id) > 1 THEN 'Multiple'
                  ELSE MAX(trd.Counter_Party)
              END AS Counter_Party
            , rm.Assignee AS Risk_Manager
            , REPLACE(
                  CONVERT(VARCHAR(20), ISNULL(@Legacy_DT_Order_Placed_Dt, MAX(trdtxnop.Date_Data_Amended)), 106), ' '
                  , '-') AS Order_Place
            , REPLACE(
                  CONVERT(VARCHAR(20), ISNULL(@Legacy_DT_Order_Executed_Dt, MAX(trd.Trade_Executed_Ts)), 106), ' ', '-') AS Date_Executed
            , trd.Row_Num
            , @Total_Cnt AS Total
            , trd.Client_Id
            , CASE WHEN COUNT(DISTINCT trd.Client_Hier_Id) > 1 THEN -1
                  ELSE MAX(trd.Sitegroup_Id)
              END AS Sitegroup_Id
            , CASE WHEN COUNT(DISTINCT trd.Client_Hier_Id) > 1 THEN -1
                  ELSE MAX(trd.Site_Id)
              END AS Site_Id
        FROM
            #Trade_Dtls trd
            LEFT JOIN #Deal_Ticket_Client_Hier_TXN_Status trdtxnop
                ON trd.Trade_Id = trdtxnop.Cbms_Trade_Number
                   AND  trdtxnop.Transaction_Status IN ( 'Order placed' )
            INNER JOIN #Trade_Vols tv
                ON tv.Trade_Id = trd.Trade_Id
            LEFT JOIN #RM rm
                ON trd.Trade_Id = rm.Cbms_Trade_Number
        WHERE
            Row_Num BETWEEN @Start_Index
                    AND     @End_Index
        GROUP BY
            trd.Trade_Id
            , trd.Trade_Status
            , rm.Assignee
            , trd.Row_Num
            , trd.Client_Id;

        DROP TABLE #Trade_Dtls;
        DROP TABLE #Trade_NumStatus;
        DROP TABLE #Trade_Vols;
        DROP TABLE #Deal_Ticket_Client_Hier_TXN_Status;
        DROP TABLE #RM;
        DROP TABLE #Counter_Party;


    END;









GO
GRANT EXECUTE ON  [Trade].[Trade_Dtls_Sel_By_Deal_Ticket] TO [CBMSApplication]
GO
