SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
                           
 NAME:  dbo.IC_Bots_Process_Invoice_Collection_Account_Config_Map_Ins_Upd                     
                            
 DESCRIPTION:        
				TO save the Bot information for IC Data .                            
                            
 INPUT PARAMETERS:        
                           
 Name                                   DataType        Default       Description        
------------------------------------------------------------------------------------   
@Invoice_Collection_Account_Config_Id	INT
@IC_Bots_Process_Id						INT				NULL
@Enroll_In_Bot							BIT
@User_Info_Id							INT
		                          
 OUTPUT PARAMETERS:        
                                 
 Name                                   DataType        Default       Description        
------------------------------------------------------------------------------------   
                            
 USAGE EXAMPLES:                                
------------------------------------------------------------------------------------   
 
 BEGIN TRAN      

 Select * from dbo.IC_Bots_Process_Invoice_Collection_Account_Config_Map    

 Exec dbo.IC_Bots_Process_Invoice_Collection_Account_Config_Map_Ins_Upd 1,NULL,0,49     
 
 Select * from dbo.IC_Bots_Process_Invoice_Collection_Account_Config_Map   
 
 ROLLBACK TRAN 
                           
 AUTHOR INITIALS:      
         
 Initials               Name        
------------------------------------------------------------------------------------   
 NR                     Narayana Reddy                              
                             
 MODIFICATIONS:      
                             
 Initials               Date            Modification      
------------------------------------------------------------------------------------   
 NR                     2020-06-03      Created for SE2017-963 BOT Process.                          
                           
******/

CREATE PROC [dbo].[IC_Bots_Process_Invoice_Collection_Account_Config_Map_Ins_Upd]
    (
        @Invoice_Collection_Account_Config_Id INT
        , @IC_Bots_Process_Id INT = NULL
        , @Enroll_In_Bot BIT = NULL
        , @User_Info_Id INT
        , @Preserve_Existing_Value_When_Null BIT = 0
    )
AS
    BEGIN
        SET NOCOUNT ON;




        UPDATE
            ibpicacm
        SET
            ibpicacm.IC_Bots_Process_Id = CASE WHEN @Preserve_Existing_Value_When_Null = 1 THEN
                                                   ISNULL(CASE WHEN @IC_Bots_Process_Id = 0 THEN NULL
                                                              ELSE @IC_Bots_Process_Id
                                                          END, CASE WHEN @IC_Bots_Process_Id = 0 THEN NULL
                                                                   ELSE ibpicacm.IC_Bots_Process_Id
                                                               END)
                                              ELSE CASE WHEN @IC_Bots_Process_Id = 0 THEN NULL
                                                       ELSE @IC_Bots_Process_Id
                                                   END
                                          END
            , ibpicacm.Enroll_In_Bot = CASE WHEN @Preserve_Existing_Value_When_Null = 1 THEN
                                                ISNULL(@Enroll_In_Bot, ibpicacm.Enroll_In_Bot)
                                           ELSE ISNULL(@Enroll_In_Bot, 0)
                                       END
            , ibpicacm.Updated_User_Id = @User_Info_Id
            , ibpicacm.Last_Change_Ts = GETDATE()
        FROM
            dbo.IC_Bots_Process_Invoice_Collection_Account_Config_Map ibpicacm
        WHERE
            ibpicacm.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id;





        INSERT INTO dbo.IC_Bots_Process_Invoice_Collection_Account_Config_Map
             (
                 Invoice_Collection_Account_Config_Id
                 , IC_Bots_Process_Id
                 , Enroll_In_Bot
                 , Created_User_Id
                 , Created_Ts
                 , Updated_User_Id
                 , Last_Change_Ts
             )
        SELECT
            @Invoice_Collection_Account_Config_Id
            , @IC_Bots_Process_Id
            , ISNULL(@Enroll_In_Bot, 0)
            , @User_Info_Id
            , GETDATE()
            , @User_Info_Id
            , GETDATE()
        WHERE
            (   ISNULL(@IC_Bots_Process_Id, 0) > 0
                OR  ISNULL(@Enroll_In_Bot, 0) = 1)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.IC_Bots_Process_Invoice_Collection_Account_Config_Map ibpicacm
                               WHERE
                                    ibpicacm.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id);



    END;



GO
GRANT EXECUTE ON  [dbo].[IC_Bots_Process_Invoice_Collection_Account_Config_Map_Ins_Upd] TO [CBMSApplication]
GO
