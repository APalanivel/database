SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_RPT_GET_UTILITIES_UNDER_STATES_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@stateIds      	varchar(100)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.SR_SAD_RPT_GET_UTILITIES_UNDER_STATES_P  '-1','-1','2,3'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/28/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on.
******/

CREATE PROCEDURE dbo.SR_SAD_RPT_GET_UTILITIES_UNDER_STATES_P 
@userId		VARCHAR,
@sessionId	VARCHAR,
@stateIds	VARCHAR(100)
AS
BEGIN
	
	SET NOCOUNT ON ;
	
	DECLARE @sql VARCHAR(1000)
	
	SELECT @sql	=	' select v.VENDOR_ID, v.VENDOR_NAME from VENDOR v (nolock), '
			+' VENDOR_STATE_MAP vsm(nolock), entity venType(nolock)     '
			+' where  vsm.STATE_ID in (select IntValue  from dbo.SR_SAD_RPT_FN_GET_CSV_TO_INT('+''''+@stateIds+''''+'))'
			+'  and v.VENDOR_ID = vsm.VENDOR_ID '
			+' and v.vendor_type_id = venType.entity_id  and venType.entity_type = 155 '
			+' and venType.entity_name = ''Utility'' order by v.vendor_name '
			 
	EXEC (@sql)
END	
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_RPT_GET_UTILITIES_UNDER_STATES_P] TO [CBMSApplication]
GO
