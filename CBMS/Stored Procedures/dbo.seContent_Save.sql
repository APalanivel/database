SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE      Procedure dbo.seContent_Save
	( @ContentLocaleId int
	, @ContentPlacementId int
	, @AccountId int
	, @ContentText text = null
	)
As
BEGIN
	set nocount on
	declare @ThisId int
	declare @OrigEntityId int
	declare @ThisVersion int
	declare @StatusId int
	declare @EntityTypeId int


		select @ThisVersion = isNull(max(Version), 0) + 1
		  from seContent
		 where ContentPlacementId = @ContentPlacementId

		--set @EntityTypeId = dbo.dmlLookup_GetId ('Audit Entity Type', 'Content')
		set @StatusId = 8--dbo.dmlLookup_GetId ('Content Status', 'Draft')

	insert into seContent
		( ContentLocaleId
		, ContentPlacementId
		, Version
		, CurrentStatusId
		, ContentText
		)
	values
		( @ContentLocaleId
		, @ContentPlacementId
		, @ThisVersion
		, @StatusId
		, @ContentText
		)
	set @ThisId = @@IDENTITY

	  /*select @OrigEntityId = ContentId
	    from seContent
	   where ContentLocaleId = @ContentLocaleId
	     and ContentPlacementId = @ContentPlacementId
	     and Version = 1
	
	exec dmlAuditLog_Save @EntityTypeId, @OrigEntityId, @ThisId, @ThisVersion, @StatusId, @AccountId
*/

	exec seContent_Get @ThisId

END
GO
GRANT EXECUTE ON  [dbo].[seContent_Save] TO [CBMSApplication]
GO
