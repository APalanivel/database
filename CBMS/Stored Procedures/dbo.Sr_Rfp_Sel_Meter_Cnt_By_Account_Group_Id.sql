SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
 dbo.Sr_Rfp_Sel_Meter_Cnt_By_Account_Group_Id

DESCRIPTION:
	For email body to show the meter count for given rfp_account_group_id. 

INPUT PARAMETERS:
	Name				DataType		Default			Description
-------------------------------------------------------------------------
 @rfp_account_group_id	INT

OUTPUT PARAMETERS:
	Name				DataType		Default			Description
-------------------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------------------

 EXEC dbo.Sr_Rfp_Sel_Meter_Cnt_By_Account_Group_Id 
      @rfp_account_group_id = 827
     
 EXEC dbo.Sr_Rfp_Sel_Meter_Cnt_By_Account_Group_Id 
      @rfp_account_group_id = 10012206
      
   EXEC dbo.Sr_Rfp_Sel_Meter_Cnt_By_Account_Group_Id 
      @rfp_account_group_id =10012153
    
    
AUTHOR INITIALS:
Initials		Name
-------------------------------------------------------------------------
NR				Narayana Reddy
	
MODIFICATIONS

Initials		Date			Modification
-------------------------------------------------------------------------
NR				2016-06-09		Created for GCS-Phase-5	- GCS-1019

******/

CREATE PROCEDURE [dbo].[Sr_Rfp_Sel_Meter_Cnt_By_Account_Group_Id]
      ( 
       @rfp_account_group_id INT )
AS 
BEGIN
      SET NOCOUNT ON
     
      SELECT
            ch.Client_Name
           ,bid_group.GROUP_NAME
           ,COUNT(DISTINCT ch.Site_Id) AS Site_Cnt
           ,COUNT(DISTINCT cha.Account_Id) AS Account_Cnt
           ,COUNT(cha.Meter_Id) AS Meter_Cnt
      FROM
            SR_RFP_ACCOUNT AS rfp_account
            INNER JOIN SR_RFP_BID_GROUP AS bid_group
                  ON rfp_account.SR_RFP_BID_GROUP_ID = bid_group.SR_RFP_BID_GROUP_ID
            INNER JOIN dbo.SR_RFP sr
                  ON rfp_account.SR_RFP_ID = sr.SR_RFP_ID
            INNER JOIN core.Client_Hier_Account cha
                  ON rfp_account.ACCOUNT_ID = cha.Account_Id
                     AND cha.Commodity_Id = sr.COMMODITY_TYPE_ID
            INNER JOIN core.Client_Hier ch
                  ON cha.Client_Hier_Id = ch.Client_Hier_Id
      WHERE
            ( rfp_account.SR_RFP_BID_GROUP_ID = @rfp_account_group_id )
            AND ( rfp_account.IS_DELETED = 0 )
      GROUP BY
            ch.Client_Name
           ,bid_group.GROUP_NAME  
                     
      
END

;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Sel_Meter_Cnt_By_Account_Group_Id] TO [CBMSApplication]
GO
