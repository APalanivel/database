SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:   dbo.Vendor_sel_Vendor_Type           
                  
Description:                  
   This sproc is to get the Account_Vendor_Details.          
                               
 Input Parameters:                  
    Name        DataType   Default   Description                    
----------------------------------------------------------------------------------------                    
 @Vendor_Type      INT         Utility/Supplier    
    @client_Id       INT      NULL    
    @Account_Id      INT      NULL                        
     
 Output Parameters:                        
    Name        DataType   Default   Description                    
----------------------------------------------------------------------------------------                    
                  
 Usage Examples:                      
----------------------------------------------------------------------------------------       
    
   Exec dbo.Vendor_sel_Vendor_Type 'Utility',235,27531    
   Exec dbo.Vendor_sel_Vendor_Type 'Utility',Null,290        
      
       
Author Initials:                  
    Initials  Name                  
----------------------------------------------------------------------------------------                    
 RKV    Ravi Kumar Vegesna    
 Modifications:                  
    Initials        Date   Modification                  
----------------------------------------------------------------------------------------                    
    RKV    2016-12-29  Created For Invoice_Collection.      
	SLP    2019-09-30  Included logic to get all Vendors when client is not passed         
                 
******/

CREATE PROCEDURE [dbo].[Vendor_sel_Vendor_Type]
(
    @Vendor_Type CHAR(8),
    @client_Id INT = NULL,
    @Account_Id INT = NULL
)
AS
BEGIN

    DECLARE @Contact_Level_Vendor INT;

    SELECT @Contact_Level_Vendor = c.Code_Id
    FROM dbo.Code c
        INNER JOIN dbo.Codeset cs
            ON c.Codeset_Id = cs.Codeset_Id
    WHERE cs.Codeset_Name = 'ContactLevel'
          AND c.Code_Value = 'Vendor';


    CREATE TABLE #Vendor_Account_Dtls
    (
        Account_Id INT,
        Account_Vendor_Type CHAR(8),
        Account_Vendor_Id INT,
        Account_Vendor_Name VARCHAR(200)
    );

    --Collecting the Utility Account Vendors        

    INSERT INTO #Vendor_Account_Dtls
    (
        Account_Id,
        Account_Vendor_Type,
        Account_Vendor_Id,
        Account_Vendor_Name
    )
    SELECT ucha.Account_Id,
           Account_Vendor_Type = CASE
                                     WHEN scha.Account_Vendor_Name IS NOT NULL
                                          AND asbv1.Account_Id IS NOT NULL THEN
                                         scha.Account_Type
                                     WHEN e.ENTITY_NAME IS NOT NULL THEN
                                         e.ENTITY_NAME
                                     WHEN icav.VENDOR_NAME IS NOT NULL
                                          AND asbv1.Account_Id IS NOT NULL THEN
                                         'Supplier'
                                     ELSE
                                         ucha.Account_Type
                                 END,
           Account_Vendor_Id = CASE
                                   WHEN scha.Account_Vendor_Name IS NOT NULL
                                        AND asbv1.Account_Id IS NOT NULL THEN
                                       scha.Account_Vendor_Id
                                   WHEN e.ENTITY_NAME IS NOT NULL THEN
                                       v.VENDOR_ID
                                   WHEN icav.VENDOR_NAME IS NOT NULL
                                        AND asbv1.Account_Id IS NOT NULL THEN
                                       icav.VENDOR_ID
                                   ELSE
                                       ucha.Account_Vendor_Id
                               END,
           Account_Vendor_Name = CASE
                                     WHEN scha.Account_Vendor_Name IS NOT NULL
                                          AND asbv1.Account_Id IS NOT NULL THEN
                                         scha.Account_Vendor_Name
                                     WHEN e.ENTITY_NAME IS NOT NULL THEN
                                         v.VENDOR_NAME
                                     WHEN icav.VENDOR_NAME IS NOT NULL
                                          AND asbv1.Account_Id IS NOT NULL THEN
                                         icav.VENDOR_NAME
                                     ELSE
                                         ucha.Account_Vendor_Name
                                 END
    FROM dbo.Invoice_Collection_Account_Config icac
        INNER JOIN Core.Client_Hier_Account ucha
            ON icac.Account_Id = ucha.Account_Id
        INNER JOIN Core.Client_Hier ch
            ON ucha.Client_Hier_Id = ch.Client_Hier_Id
        LEFT OUTER JOIN Core.Client_Hier_Account scha
            ON ucha.Meter_Id = scha.Meter_Id
               AND scha.Account_Type = 'Supplier'
               AND icac.Invoice_Collection_Service_End_Dt
               BETWEEN scha.Supplier_Account_begin_Dt AND scha.Supplier_Account_End_Dt
        LEFT OUTER JOIN(dbo.Account_Consolidated_Billing_Vendor asbv
        INNER JOIN dbo.ENTITY e
            ON asbv.Invoice_Vendor_Type_Id = e.ENTITY_ID
               AND e.ENTITY_NAME = 'Supplier'
        LEFT OUTER JOIN dbo.VENDOR v
            ON v.VENDOR_ID = asbv.Supplier_Vendor_Id)
            ON asbv.Account_Id = ucha.Account_Id
               AND icac.Invoice_Collection_Service_End_Dt
               BETWEEN asbv.Billing_Start_Dt AND asbv.Billing_End_Dt
        LEFT OUTER JOIN(dbo.Account_Consolidated_Billing_Vendor asbv1
        INNER JOIN dbo.ENTITY e1
            ON asbv1.Invoice_Vendor_Type_Id = e1.ENTITY_ID
               AND e1.ENTITY_NAME = 'Supplier'
        LEFT OUTER JOIN dbo.VENDOR v1
            ON v1.VENDOR_ID = asbv1.Supplier_Vendor_Id)
            ON asbv1.Account_Id = ucha.Account_Id
        LEFT OUTER JOIN(dbo.Invoice_Collection_Account_Contact icc
        INNER JOIN Contact_Info ci
            ON ci.Contact_Info_Id = icc.Contact_Info_Id
               AND icc.Is_Primary = 1
               AND ci.Contact_Level_Cd = @Contact_Level_Vendor
        INNER JOIN dbo.Vendor_Contact_Map vcm
            ON ci.Contact_Info_Id = vcm.Contact_Info_Id
        INNER JOIN dbo.VENDOR icav
            ON icav.VENDOR_ID = vcm.VENDOR_ID)
            ON icc.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
    WHERE ucha.Account_Type = 'Utility'
          AND
         
          (
              @client_Id IS NOT NULL
              AND ch.Client_Id = @client_Id
          )
          AND
          (
              @Account_Id IS NULL
              OR ucha.Account_Id = @Account_Id
          )
          AND icac.Is_Chase_Activated = 1
    GROUP BY CASE
                 WHEN scha.Account_Vendor_Name IS NOT NULL
                      AND asbv1.Account_Id IS NOT NULL THEN
                     scha.Account_Type
                 WHEN e.ENTITY_NAME IS NOT NULL THEN
                     e.ENTITY_NAME
                 WHEN icav.VENDOR_NAME IS NOT NULL
                      AND asbv1.Account_Id IS NOT NULL THEN
                     'Supplier'
                 ELSE
                     ucha.Account_Type
             END,
             CASE
                 WHEN scha.Account_Vendor_Name IS NOT NULL
                      AND asbv1.Account_Id IS NOT NULL THEN
                     scha.Account_Vendor_Id
                 WHEN e.ENTITY_NAME IS NOT NULL THEN
                     v.VENDOR_ID
                 WHEN icav.VENDOR_NAME IS NOT NULL
                      AND asbv1.Account_Id IS NOT NULL THEN
                     icav.VENDOR_ID
                 ELSE
                     ucha.Account_Vendor_Id
             END,
             CASE
                 WHEN scha.Account_Vendor_Name IS NOT NULL
                      AND asbv1.Account_Id IS NOT NULL THEN
                     scha.Account_Vendor_Name
                 WHEN e.ENTITY_NAME IS NOT NULL THEN
                     v.VENDOR_NAME
                 WHEN icav.VENDOR_NAME IS NOT NULL
                      AND asbv1.Account_Id IS NOT NULL THEN
                     icav.VENDOR_NAME
                 ELSE
                     ucha.Account_Vendor_Name
             END,
             ucha.Account_Id;


    --Collecting the Supplier Account Vendors       

    INSERT INTO #Vendor_Account_Dtls
    (
        Account_Id,
        Account_Vendor_Type,
        Account_Vendor_Id,
        Account_Vendor_Name
    )
    SELECT cha.Account_Id,
           cha.Account_Type,
           cha.Account_Vendor_Id,
           cha.Account_Vendor_Name
    FROM Core.Client_Hier ch
        INNER JOIN Core.Client_Hier_Account cha
            ON ch.Client_Hier_Id = cha.Client_Hier_Id
        INNER JOIN dbo.Invoice_Collection_Account_Config icac
            ON cha.Account_Id = icac.Account_Id
               AND icac.Is_Chase_Activated = 1
    WHERE
       
        (
            @client_Id IS NOT NULL
            AND ch.Client_Id = @client_Id
        )
        AND
        (
            @Account_Id IS NULL
            OR cha.Account_Id = @Account_Id
        )
        AND cha.Account_Type = 'Supplier'
    GROUP BY cha.Account_Id,
             cha.Account_Type,
             cha.Account_Vendor_Id,
             cha.Account_Vendor_Name;


    --get the all Vendor details when client is not passed  
    INSERT INTO #Vendor_Account_Dtls
    (
        Account_Id,
        Account_Vendor_Type,
        Account_Vendor_Id,
        Account_Vendor_Name
    )
    SELECT '',
           ventyp.ENTITY_NAME,
           vndr.VENDOR_ID,
           vndr.VENDOR_NAME
    FROM dbo.VENDOR vndr
        INNER JOIN dbo.ENTITY ventyp
            ON vndr.VENDOR_TYPE_ID = ventyp.ENTITY_ID
    WHERE @client_Id IS NULL
          AND vndr.IS_HISTORY = 0
          AND
          (
              @Vendor_Type IS NULL
              OR ventyp.ENTITY_NAME = @Vendor_Type
          )
    GROUP BY vndr.VENDOR_ID,
             vndr.VENDOR_NAME,
             ventyp.ENTITY_NAME;



    SELECT vad.Account_Vendor_Id,
           vad.Account_Vendor_Name
    FROM #Vendor_Account_Dtls vad
    WHERE vad.Account_Vendor_Type = @Vendor_Type
    GROUP BY vad.Account_Vendor_Id,
             vad.Account_Vendor_Name
			 order by vad.Account_Vendor_Name asc;


END;

;

GO
GRANT EXECUTE ON  [dbo].[Vendor_sel_Vendor_Type] TO [CBMSApplication]
GO
