SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Audit_Log_Sel_By_Account_Id]  
     
DESCRIPTION: 

	To Get Audit log details for the given account id
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Account_Id		INT						
@StartIndex		INT			1			
@EndIndex		INT			2147483647
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        
  
	EXEC Audit_Log_Sel_By_Account_Id  101527

	EXEC Audit_Log_Sel_By_Account_Id  116867
     
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR			PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR			25-MAY-10	CREATED     
*/  

CREATE PROCEDURE dbo.Audit_Log_Sel_By_Account_Id
	(
	@Account_Id INT
	,@StartIndex	INT		= 1
	,@EndIndex		INT		= 2147483647
	)
AS
BEGIN

	SET NOCOUNT ON;
	
	WITH Cte_Audit_List AS
	(
		SELECT
			ea.User_Info_Id
			,ea.MODIFIED_DATE
			,ea.AUDIT_TYPE
			,ROW_NUMBER() OVER(ORDER BY ea.MODIFIED_DATE) Row_Num
			,COUNT(1) OVER() Total_Rows
		FROM
			dbo.ENTITY_AUDIT ea
			JOIN dbo.ENTITY et
		  		 ON et.ENTITY_ID = ea.ENTITY_ID
			JOIN dbo.USER_INFO ui
		 		 ON ui.USER_INFO_ID = ea.USER_INFO_ID
		WHERE
			ea.ENTITY_IDENTIFIER = @Account_Id
			AND et.ENTITY_NAME = 'Account_Table'
			AND et.ENTITY_DESCRIPTION = 'Table_Type'

	)
	SELECT
		ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME		Audit_User_Name
		,CONVERT(DATETIME,al.MODIFIED_DATE,101) Audit_Date
		,CASE 
			WHEN al.AUDIT_TYPE = 1 THEN 'Created'
			WHEN al.AUDIT_TYPE = 2 THEN 'Edited'
		 END										Audit_Event
		,al.Total_Rows
	FROM
		Cte_Audit_List al
		JOIN dbo.USER_INFO ui
	 		 ON ui.USER_INFO_ID = al.USER_INFO_ID
	WHERE
		al.Row_Num BETWEEN @StartIndex AND @EndIndex

END
GO
GRANT EXECUTE ON  [dbo].[Audit_Log_Sel_By_Account_Id] TO [CBMSApplication]
GO
