SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Pricing_Product_Upd]
           
Description:             
			To update SR pricing Product details
			
INPUT PARAMETERS:            
	Name					DataType	Default		Description  
---------------------------------------------------------------------------------  
	@SR_PRICING_Product_ID	INT
    @Countrty_Id_List		VARCHAR(MAX)
    @User_Info_Id			INT


OUTPUT PARAMETERS:
	Name			DataType		Default		Description  
---------------------------------------------------------------------------------  


 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT * FROM dbo.Sr_Pricing_Product
	
	BEGIN TRANSACTION  
		SELECT * FROM dbo.Sr_Pricing_Product WHERE SR_PRICING_Product_ID = 29
		EXEC dbo.Sr_Pricing_Product_Upd 29,291,'Block Price - Vendor Collected','Updated'
		SELECT * FROM dbo.Sr_Pricing_Product WHERE SR_PRICING_Product_ID = 29
	ROLLBACK TRANSACTION


 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2015-09-22	Global Sourcing - Phase2 -Created
								
******/


CREATE  PROCEDURE [dbo].[Sr_Pricing_Product_Upd]
      ( 
       @Sr_Pricing_Product_ID INT
      ,@Commodity_Type_Id INT
      ,@Product_Name VARCHAR(100)
      ,@Product_Description VARCHAR(4000) )
AS 
BEGIN

      SET NOCOUNT ON;
      
      UPDATE
            dbo.SR_PRICING_PRODUCT
      SET   
            COMMODITY_TYPE_ID = @Commodity_Type_Id
           ,PRODUCT_NAME = @Product_Name
           ,PRODUCT_DESCRIPTION = @Product_Description
      WHERE
            SR_PRICING_PRODUCT_ID = @Sr_Pricing_Product_ID
            
END;
GO
GRANT EXECUTE ON  [dbo].[Sr_Pricing_Product_Upd] TO [CBMSApplication]
GO
