SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsCuInvoiceDeterminant_UpdateUomMapping

DESCRIPTION:


INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@cu_invoice_id		int
	@commodity_type_id	int
	@unit_of_measure_type_id int
	@ubm_unit_of_measure_code varchar(200)

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.cbmsCuInvoiceDeterminant_UpdateUomMapping 3450294,67,1409,gal

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hari
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
				07/20/2009	Created
	HG			04/09/2010	In the query which used to find Error count Commodity_type_id > 0 filter condition added to exclude the unmapped commodity entries.
							unncessary Subselect and Cu_Invoice table Join removed from the query returns error count.
******/

CREATE PROCEDURE dbo.cbmsCuInvoiceDeterminant_UpdateUomMapping
   (
	@cu_invoice_id				int
	, @commodity_type_id		int
	, @unit_of_measure_type_id	int
	, @ubm_unit_of_measure_code varchar(200)
   )
AS 
BEGIN

	SET NOCOUNT ON

	UPDATE
		dbo.CU_INVOICE_DETERMINANT
		SET unit_of_measure_type_id = @unit_of_measure_type_id
	WHERE
		cu_invoice_id = @cu_invoice_id
		AND commodity_type_id = @commodity_type_id
		AND ubm_unit_of_measure_code = @ubm_unit_of_measure_code
		AND unit_of_measure_type_id IS NULL

	SELECT
		@cu_invoice_id cu_invoice_id
		, COUNT(1) det_count
	FROM
		cu_invoice_determinant d
	WHERE
		d.cu_invoice_id = @cu_invoice_id
		AND d.unit_of_measure_type_id IS NULL
		AND d.COMMODITY_TYPE_ID >0

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCuInvoiceDeterminant_UpdateUomMapping] TO [CBMSApplication]
GO
