
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                    
/******                            
 NAME: dbo.Utility_Analyst_Map_Sel_By_Country_State_Analyst_Status                
                            
 DESCRIPTION:                            
   To get list of Utility Analysts with permissions  'operations'  and 'Invoice Processors' irrespective of Is_History                  
                            
 INPUT PARAMETERS:              
                         
 Name                        DataType         Default       Description            
---------------------------------------------------------------------------------------------------------------          
@Country_Id      INT    NULL  
@State_Id      INT    NULL  
@Show_History_Users    INT    -1                      
                            
 OUTPUT PARAMETERS:              
                               
 Name                        DataType         Default       Description            
---------------------------------------------------------------------------------------------------------------          
                            
 USAGE EXAMPLES:                                
---------------------------------------------------------------------------------------------------------------                                

  --in the grid u need to send this @Show_History_Users=0.no need to send country or state  
    EXEC [dbo].[Utility_Analyst_Map_Sel_By_Country_State_Analyst_Status]     
       @Show_History_Users=0  
            
 --in the search you need to send country and @Show_History_Users=-1    
   EXEC [dbo].[Utility_Analyst_Map_Sel_By_Country_State_Analyst_Status]     
      @Country_Id=4   
      ,@Show_History_Users=-1   

 --in the search you need to send country AND state and @Show_History_Users=-1    
   EXEC [dbo].[Utility_Analyst_Map_Sel_By_Country_State_Analyst_Status]     
      @Country_Id=4 
      ,@State_Id=18  
      ,@Show_History_Users=-1       
    
	EXEC [dbo].[Utility_Analyst_Map_Sel_By_Country_State_Analyst_Status] @Group_Name='ABC'
	EXEC [dbo].[Utility_Analyst_Map_Sel_By_Country_State_Analyst_Status] @Group_Name='Regulated Markets Budgets'
	EXEC [dbo].[Utility_Analyst_Map_Sel_By_Country_State_Analyst_Status] @Group_Name='Budget Reviewer Queue'
    
          
                           
 AUTHOR INITIALS:            
           
	Initials	Name            
---------------------------------------------------------------------------------------------------------------                          
	SP          Sandeep Pigilam
	RR			Raghu Reddy
                             
 MODIFICATIONS:          
	Initials	Date		Modification          
---------------------------------------------------------------------------------------------------------------          
	SP			2014-02-24	Created
	RR			2017-05-31  SE2017-26 Modified script to list users assgined with "Distribution Budgets" group also
******/     
       
    
CREATE PROCEDURE [dbo].[Utility_Analyst_Map_Sel_By_Country_State_Analyst_Status]
      ( 
       @Country_Id INT = NULL
      ,@State_Id INT = NULL
      ,@Show_History_Users INT = -1
      ,@Group_Name VARCHAR(200) = NULL )
      WITH RECOMPILE
AS 
BEGIN      
      SET NOCOUNT ON;      
     
      DECLARE @Analyst_List TABLE
            ( 
             Analyst_Id INT PRIMARY KEY CLUSTERED
            ,Analyst_Name VARCHAR(100) ) 
            
      SELECT
            @Group_Name = NULL
      WHERE
            @Group_Name NOT IN ( 'Regulated Markets Budgets', 'Budget Reviewer Queue' )
            
  
      INSERT      INTO @Analyst_List
                  ( 
                   Analyst_Id
                  ,Analyst_Name )
                  SELECT
                        ui.USER_INFO_ID AS Analyst_Id
                       ,ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME AS Analyst_Name
                  FROM
                        dbo.USER_INFO ui
                        INNER JOIN dbo.USER_INFO_GROUP_INFO_MAP gm
                              ON ui.USER_INFO_ID = gm.USER_INFO_ID
                        INNER JOIN dbo.Group_Info gi
                              ON gm.GROUP_INFO_ID = gi.GROUP_INFO_ID
                  WHERE
                        gi.GROUP_NAME IN ( 'operations', 'Invoice Processors' )
                        AND ( @Show_History_Users = -1
                              OR ui.IS_HISTORY = @Show_History_Users )
                        AND @Group_Name IS NULL
                  GROUP BY
                        ui.USER_INFO_ID
                       ,ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME
                  HAVING
                        COUNT(gi.Group_Info_ID) = 2
                        
                        
                        
                        
      INSERT      INTO @Analyst_List
                  ( 
                   Analyst_Id
                  ,Analyst_Name )
                  SELECT
                        ui.USER_INFO_ID AS Analyst_Id
                       ,ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME AS Analyst_Name
                  FROM
                        dbo.USER_INFO ui
                        INNER JOIN dbo.USER_INFO_GROUP_INFO_MAP gm
                              ON ui.USER_INFO_ID = gm.USER_INFO_ID
                        INNER JOIN dbo.Group_Info gi
                              ON gm.GROUP_INFO_ID = gi.GROUP_INFO_ID
                  WHERE
                        gi.GROUP_NAME IN ( 'operations', 'Distribution Budgets' )
                        AND ( @Show_History_Users = -1
                              OR ui.IS_HISTORY = @Show_History_Users )
                        AND ( @Group_Name IS NOT NULL
                              AND @Group_Name = 'Regulated Markets Budgets' )
                  GROUP BY
                        ui.USER_INFO_ID
                       ,ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME
                       
      INSERT      INTO @Analyst_List
                  ( 
                   Analyst_Id
                  ,Analyst_Name )
                  SELECT
                        ui.USER_INFO_ID AS Analyst_Id
                       ,ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME AS Analyst_Name
                  FROM
                        dbo.USER_INFO ui
                        INNER JOIN dbo.USER_INFO_GROUP_INFO_MAP gm
                              ON ui.USER_INFO_ID = gm.USER_INFO_ID
                        INNER JOIN dbo.Group_Info gi
                              ON gm.GROUP_INFO_ID = gi.GROUP_INFO_ID
                  WHERE
                        gi.GROUP_NAME IN ( 'Budget Reviewer' )
                        AND ( @Show_History_Users = -1
                              OR ui.IS_HISTORY = @Show_History_Users )
                        AND ( @Group_Name IS NOT NULL
                              AND @Group_Name = 'Budget Reviewer Queue' )
                  GROUP BY
                        ui.USER_INFO_ID
                       ,ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME
                  
           
           
      SELECT
            al.Analyst_Id
           ,al.Analyst_Name
      FROM
            @Analyst_List al
      WHERE
            ( ( @Country_Id IS  NULL
                AND @State_Id IS  NULL )
              OR EXISTS ( SELECT
                              1
                          FROM
                              dbo.VENDOR_STATE_MAP ac
                              INNER JOIN dbo.UTILITY_DETAIL ud
                                    ON ud.VENDOR_ID = ac.VENDOR_ID
                              INNER JOIN dbo.Utility_Detail_Analyst_Map udam
                                    ON udam.Utility_Detail_ID = ud.UTILITY_DETAIL_ID
                              INNER JOIN dbo.State st
                                    ON st.State_Id = ac.STATE_ID
                          WHERE
                              ( @State_Id IS NULL
                                OR st.STATE_ID = @State_Id )
                              AND ( @Country_Id IS NULL
                                    OR st.COUNTRY_ID = @Country_Id )
                              AND udam.Analyst_ID = al.Analyst_Id ) )
      GROUP BY
            al.Analyst_Id
           ,al.Analyst_Name
      ORDER BY
            al.Analyst_Name  
     
       
       
END;


;
GO

GRANT EXECUTE ON  [dbo].[Utility_Analyst_Map_Sel_By_Country_State_Analyst_Status] TO [CBMSApplication]
GO
