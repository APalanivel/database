SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



--exec BUDGET_GET_BUDGET_REPORT_ACCOUNTS_FOR_MANAGE_BUDGET_P 304,235,258,null,290

CREATE                     PROCEDURE dbo.BUDGET_GET_BUDGET_REPORT_ACCOUNTS_FOR_MANAGE_BUDGET_P
	@budgetId int,
	@clientId int,
	@divisionId int,
	@siteId int ,
	@commodityId int
	AS
	begin
		set nocount on
		
if @divisionId is null and @siteId is null
begin
		select 	ba.budget_account_id,
			case when isnull(b.original_budget_id, 0) > 0
			then ba.is_reforecast
			else 1
			end is_reforecast,
			d.division_name,
			d.division_id , 
			--vwname.site_name , 
			s.site_name,
			--vwname.site_id
			s.site_id

		from 	budget b
			join budget_account ba  on  b.budget_id =  ba.budget_id
			and b.budget_id = @budgetId 
			and b.commodity_type_id = @commodityId
			join BUDGET_ACCOUNT_TYPE_VW type_vw on type_vw.budget_id = ba.budget_id 
			and type_vw.budget_account_id = ba.budget_account_id 
			--and type_vw.budget_account_type in('A&B', 'C&D Created')
			join ACCOUNT ACCT on  acct.account_id = ba.account_id 
			JOIN SITE S WITH(NOLOCK) ON (acct.site_id = s.site_id and S.SITE_ID = COALESCE(@siteId, s.site_id))
			--JOIN vwSiteName vwname on (vwname.site_id = S.site_id )
			JOIN DIVISION D WITH(NOLOCK) ON (D.DIVISION_ID = S.DIVISION_ID 
			AND  D.DIVISION_ID = COALESCE(@divisionId, D.DIVISION_ID))
			JOIN CLIENT C WITH(NOLOCK) ON (C.CLIENT_ID = D.CLIENT_ID 
			AND C.CLIENT_ID = COALESCE(@clientId, c.client_id))

			order by d.division_name , s.site_name

end
else if @siteId is null
begin
		select 	ba.budget_account_id,
			case when isnull(b.original_budget_id, 0) > 0
			then ba.is_reforecast
			else 1
			end is_reforecast,
			--vwname.site_name , 
			s.site_name,
			--vwname.site_id
			s.site_id

		from 	budget b
			join budget_account ba  on  b.budget_id =  ba.budget_id
			and b.budget_id = @budgetId 
			and b.commodity_type_id = @commodityId
			join BUDGET_ACCOUNT_TYPE_VW type_vw on type_vw.budget_id = ba.budget_id 
			and type_vw.budget_account_id = ba.budget_account_id 
			--and type_vw.budget_account_type in('A&B', 'C&D Created')
			join ACCOUNT ACCT on  acct.account_id = ba.account_id 
			JOIN SITE S WITH(NOLOCK) ON (acct.site_id = s.site_id and S.SITE_ID = COALESCE(@siteId, s.site_id))
			--JOIN vwSiteName vwname on (vwname.site_id = S.site_id )
			JOIN DIVISION D WITH(NOLOCK) ON (D.DIVISION_ID = S.DIVISION_ID 
			AND  D.DIVISION_ID = COALESCE(@divisionId, D.DIVISION_ID))
			JOIN CLIENT C WITH(NOLOCK) ON (C.CLIENT_ID = D.CLIENT_ID 
			AND C.CLIENT_ID = COALESCE(@clientId, c.client_id))

			order by s.site_name 

end
else
begin
		select 	ba.budget_account_id,
			case when isnull(b.original_budget_id, 0) > 0
			then ba.is_reforecast
			else 1
			end is_reforecast
			

		from 	budget b
			join budget_account ba  on  b.budget_id =  ba.budget_id
			and b.budget_id = @budgetId 
			and b.commodity_type_id = @commodityId
			join BUDGET_ACCOUNT_TYPE_VW type_vw on type_vw.budget_id = ba.budget_id 
			and type_vw.budget_account_id = ba.budget_account_id 
			--and type_vw.budget_account_type in('A&B', 'C&D Created')
			join ACCOUNT ACCT on  acct.account_id = ba.account_id 
			JOIN SITE S WITH(NOLOCK) ON (ACCT.site_id = s.site_id 
			and S.SITE_ID = COALESCE(@siteId, s.site_id))
			JOIN DIVISION D WITH(NOLOCK) ON (D.DIVISION_ID = S.DIVISION_ID 
			AND  D.DIVISION_ID = COALESCE(@divisionId, D.DIVISION_ID))
			JOIN CLIENT C WITH(NOLOCK) ON (C.CLIENT_ID = D.CLIENT_ID 
			AND C.CLIENT_ID = COALESCE(@clientId, c.client_id))


end

	end



			


			
			

		
			
































GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_BUDGET_REPORT_ACCOUNTS_FOR_MANAGE_BUDGET_P] TO [CBMSApplication]
GO
