
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
NAME: dbo.Report_EP_NG_Contract_Expiration                
          
DESCRIPTION:            
     To Get the Contracts that are having Expiration date between the given Sartdate and EndDate for Utility Accounts.               
          
INPUT PARAMETERS:              
Name   DataType  Default Description            
------------------------------------------------------------              
@Client_Name     VARCHAR(100)              
@Commodity       INT             
@Region          VARCHAR(100)               
@StateName       VARCHAR (100)               
@StartDate       DATE             
@EndDate         DATE             
@RM_Analyst      VARCHAR(100)               
@RM_Manager      VARCHAR(100)               
            
OUTPUT PARAMETERS:              
Name   DataType  Default Description             
------------------------------------------------------------             
USAGE EXAMPLES:             
------------------------------------------------------------             
            
EXEC dbo.Report_EP_NG_Contract_Expiration '11538,11231,10009,153,11415','291,290','1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20','30,12,13,14,15,50','1/1/2010','4/1/2011','127','35'        
          
EXEC dbo.Report_EP_NG_Contract_Expiration '11231',290,'1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20','35,12,13,14,15,50','1/1/2008','8/1/2012','10633','8757'             
            
            
            
Initials Name             
------------------------------------------------------------              
AKR         Ashok Kumar Raju
SP			Sandeep Pigilam           
  
          
Initials Date  Modification             
------------------------------------------------------------               
AKR   07/05/2011  Created SP  
AKR   08/25/2011  Used the Fully Qualified column Names  
AKR   09/02/2011  corrected the spelling for [Regulated Marketes Manager] as [Regulated Markets Manager]  
AKR   2014-06-05  Modified to remove CHA.Supplier_Meter_Disassociation_Date >= CHA.Supplier_Account_End_Dt  condition.
SP	  2014-09-09  For Data Transition used a table variable @Group_Legacy_Name to handle the hardcoded group names  

******/                
              
CREATE PROCEDURE [dbo].[Report_EP_NG_Contract_Expiration]
      ( 
       @Client_Id_List VARCHAR(MAX)
      ,@Commodity_Id_List VARCHAR(100)
      ,@Region_ID_List VARCHAR(MAX)
      ,@State_Id_List VARCHAR(MAX)
      ,@StartDate DATE
      ,@EndDate DATE
      ,@RM_Analyst_List VARCHAR(MAX)
      ,@RM_Manager_List VARCHAR(MAX) )
AS 
BEGIN            
      SET NOCOUNT ON            
                    
      DECLARE @Client_Table TABLE
            ( 
             Client_id INT PRIMARY KEY )            
      DECLARE @Region_Table TABLE
            ( 
             Region_id INT PRIMARY KEY )            
      DECLARE @State_Table TABLE
            ( 
             State_id INT PRIMARY KEY )           
      DECLARE @Commodity_Table TABLE ( Commodity_id INT )       
             
      DECLARE @RM_Analyst_Table TABLE ( RM_id INT PRIMARY KEY )         
      DECLARE @RM_Manager_Table TABLE ( RA_id INT PRIMARY KEY )           
            
      DECLARE @RA_RM_Details TABLE
            ( 
             [Regulated Markets Analyst] VARCHAR(30)
            ,[Regulated Markets Manager] VARCHAR(30)
            ,Vendor_ID INT
            ,Commodity_ID INT )     

      DECLARE @Group_Legacy_Name TABLE
            ( 
             GROUP_INFO_ID INT
            ,GROUP_NAME VARCHAR(200)
            ,Legacy_Group_Name VARCHAR(200) )                 
                    
      INSERT      INTO @Client_Table
                  ( 
                   Client_id )
                  SELECT
                        Segments
                  FROM
                        dbo.ufn_split(@Client_id_List, ',')            
                    
      INSERT      INTO @Region_Table
                  ( 
                   Region_id )
                  SELECT
                        Segments
                  FROM
                        dbo.ufn_split(@Region_ID_List, ',')            
                    
      INSERT      INTO @State_Table
                  ( 
                   State_id )
                  SELECT
                        Segments
                  FROM
                        dbo.ufn_split(@State_Id_List, ',')          
             
      INSERT      INTO @Commodity_Table
                  ( 
                   Commodity_id )
                  SELECT
                        Segments
                  FROM
                        dbo.ufn_split(@Commodity_ID_List, ',')         
                                 
      INSERT      INTO @RM_Analyst_Table
                  ( 
                   RM_id )
                  SELECT
                        Segments
                  FROM
                        dbo.ufn_split(@RM_Analyst_List, ',')         
                              
      INSERT      INTO @RM_Manager_Table
                  ( 
                   RA_id )
                  SELECT
                        Segments
                  FROM
                        dbo.ufn_split(@RM_Manager_List, ',')      


      INSERT      INTO @Group_Legacy_Name
                  ( 
                   GROUP_INFO_ID
                  ,GROUP_NAME
                  ,Legacy_Group_Name )
                  EXEC dbo.Group_Info_Sel_By_Group_Legacy 
                        @Group_Legacy_Name_Cd_Value = 'Regulated_Market'                              
             
      DECLARE @UtilityContractId INT            
                        
                          
      SELECT
            @UtilityContractId = en.ENTITY_ID
      FROM
            dbo.ENTITY en
      WHERE
            en.ENTITY_NAME = 'Utility'
            AND en.ENTITY_DESCRIPTION = 'Contract Type'            
                     
                    
                    
      CREATE TABLE #AllAccounts
            ( 
             Client_Hier_Id INT
            ,Account_Id INT
            ,Meter_Id INT
            ,Supplier_Contract_ID INT
            ,Supplier_Meter_Disassociation_Date DATETIME
            ,Supplier_Account_End_Dt DATETIME
            ,Account_Number VARCHAR(50)
            ,Account_Vendor_Id INT
            ,Account_Vendor_Name VARCHAR(200)
            ,Rate_Name VARCHAR(200)
            ,Account_Type VARCHAR(200)
            ,Client_Id INT
            ,Client_Name VARCHAR(200)
            ,Sitegroup_Id INT
            ,Sitegroup_Name VARCHAR(200)
            ,Site_Id INT
            ,Site_name VARCHAR(200)
            ,City VARCHAR(200)
            ,State_Id INT
            ,State_Name VARCHAR(20)
            ,ZipCode VARCHAR(30)
            ,Region_Name VARCHAR(200)
            ,Commodity_Id INT )            
      CREATE TABLE #FinalRS
            ( 
             Client_Name VARCHAR(200)
            ,Sitegroup_Name VARCHAR(200)
            ,Site_Name VARCHAR(200)
            ,City VARCHAR(200)
            ,State_Name VARCHAR(20)
            ,ZipCode VARCHAR(30)
            ,Region_Name VARCHAR(200)
            ,Utility_Account_Number VARCHAR(50)
            ,Commodity_Name VARCHAR(200)
            ,Utility VARCHAR(200)
            ,CONTRACT_START_DATE DATETIME
            ,CONTRACT_END_DATE DATETIME
            ,ED_CONTRACT_NUMBER VARCHAR(15)
            ,[Contract Notification Date] DATETIME
            ,CONTRACT_PRICING_SUMMARY VARCHAR(1000)
            ,CONTRACT_COMMENTS VARCHAR(4000)
            ,RENEWAL_TYPE VARCHAR(200)
            ,RATE_NAME VARCHAR(200)
            ,[Regulated Markets Analyst] VARCHAR(200)
            ,[Regulated Markets Manager] VARCHAR(200)
            ,Account_count INT
            ,Site_count INT
            ,Division_Count INT
            ,City_count INT
            ,Region_count INT
            ,Zip_COunt INT
            ,Vendor_count INT
            ,State_count INT
            ,Rate_count INT
            ,RA_count INT
            ,RM_count INT
            ,CEM_count INT
            ,CEM_Name VARCHAR(200) )            
                      
      INSERT      @RA_RM_Details
                  ( 
                   [Regulated Markets Analyst]
                  ,[Regulated Markets Manager]
                  ,Vendor_ID
                  ,Commodity_ID )
                  SELECT
                        ui.first_name + SPACE(1) + ui.last_name [Regulated Markets Analyst]
                       ,ui1.first_name + SPACE(1) + ui1.last_name [Regulated Markets Manager]
                       ,VCM.vendor_id
                       ,com.Commodity_ID
                  FROM
                        dbo.utility_detail_analyst_map uam
                        JOIN dbo.user_info ui
                              ON ui.user_info_id = uam.analyst_id
                        JOIN dbo.group_info gi
                              ON gi.group_info_id = uam.group_info_id
                        INNER JOIN @Group_Legacy_Name gil
                              ON gi.GROUP_INFO_ID = gil.GROUP_INFO_ID
                        JOIN dbo.utility_detail ud
                              ON ud.utility_detail_id = uam.utility_detail_id
                        JOIN dbo.VENDOR_COMMODITY_MAP VCM
                              ON VCM.vendor_id = ud.vendor_id
                        JOIN dbo.Commodity com
                              ON com.Commodity_Id = vcm.COMMODITY_TYPE_ID
                        JOIN dbo.vendor_state_map vsm
                              ON vsm.vendor_id = VCM.vendor_id
                        JOIN dbo.state st
                              ON st.state_id = vsm.state_id
                        JOIN dbo.region_manager_map rmm
                              ON rmm.region_id = st.region_id
                        JOIN dbo.user_info ui1
                              ON ui1.user_info_id = rmm.user_info_id
                  WHERE
                        ui.USER_INFO_ID IN ( SELECT
                                                RM_ID
                                             FROM
                                                @RM_Analyst_Table )
                        AND ui1.USER_INFO_ID IN ( SELECT
                                                      RA_ID
                                                  FROM
                                                      @RM_Manager_Table )
                        AND st.STATE_ID IN ( SELECT
                                                State_Id
                                             FROM
                                                @State_Table )
                        AND st.Region_ID IN ( SELECT
                                                Region_ID
                                              FROM
                                                @Region_Table )
                        AND VCM.COMMODITY_TYPE_ID IN ( SELECT
                                                            Commodity_Id
                                                       FROM
                                                            @Commodity_Table )            
                                               
      INSERT      #AllAccounts
                  ( 
                   Client_Hier_Id
                  ,Account_Id
                  ,Meter_Id
                  ,Supplier_Contract_ID
                  ,Supplier_Meter_Disassociation_Date
                  ,Supplier_Account_End_Dt
                  ,Account_Number
                  ,Account_Vendor_Id
                  ,Account_Vendor_Name
                  ,Rate_Name
                  ,Account_Type
                  ,Client_Id
                  ,Client_Name
                  ,Sitegroup_Id
                  ,Sitegroup_Name
                  ,Site_Id
                  ,Site_name
                  ,City
                  ,State_Id
                  ,State_Name
                  ,ZipCode
                  ,Region_Name
                  ,Commodity_Id )
                  SELECT
                        CHA.Client_Hier_Id
                       ,CHA.Account_Id
                       ,CHA.Meter_Id
                       ,CHA.Supplier_Contract_ID
                       ,CHA.Supplier_Meter_Disassociation_Date
                       ,CHA.Supplier_Account_End_Dt
                       ,CHA.Account_Number
                       ,CHA.Account_Vendor_Id
                       ,CHA.Account_Vendor_Name
                       ,CHA.Rate_Name
                       ,CHA.Account_Type
                       ,CH.Client_Id
                       ,ch.Client_Name
                       ,CH.Sitegroup_Id
                       ,ch.Sitegroup_Name
                       ,CH.Site_Id
                       ,ch.Site_Name
                       ,ch.City
                       ,CH.State_Id
                       ,ch.State_Name
                       ,ch.ZipCode
                       ,ch.Region_Name
                       ,cha.Commodity_Id
                  FROM
                        Core.Client_Hier CH
                        JOIN Core.Client_Hier_Account CHA
                              ON CH.Client_Hier_Id = CHA.Client_Hier_Id
                  WHERE
                        CH.Client_Id IN ( SELECT
                                                client_id
                                          FROM
                                                @Client_Table )
                        AND CH.Site_Not_Managed = 0
                        AND CH.Site_Id > 0
                        AND CH.State_Id IN ( SELECT
                                                State_Id
                                             FROM
                                                @State_Table )
                        AND CH.Region_ID IN ( SELECT
                                                Region_ID
                                              FROM
                                                @Region_Table )
                        AND CHA.Commodity_Id IN ( SELECT
                                                      Commodity_Id
                                                  FROM
                                                      @Commodity_Table );            
      WITH  maxcontract_cte
              AS ( SELECT
                        CHA.Meter_Id [MeterID]
                       ,MAX(c.CONTRACT_END_DATE) [MaxContractExp]
                       ,C.CONTRACT_TYPE_ID [ContractType]
                       ,c.Contract_Classification_Cd [Class]
                   FROM
                        dbo.CONTRACT C
                        JOIN #AllAccounts CHA
                              ON C.CONTRACT_ID = CHA.Supplier_Contract_ID
                   WHERE
                        c.CONTRACT_TYPE_ID = @UtilityContractId
                        AND c.CONTRACT_END_DATE BETWEEN @StartDate
                                                AND     @EndDate
                        AND c.COMMODITY_TYPE_ID IN ( SELECT
                                                      Commodity_Id
                                                     FROM
                                                      @Commodity_Table )
                        AND CHA.Account_Type = 'Supplier'
                   GROUP BY
                        CHA.Meter_Id
                       ,c.CONTRACT_TYPE_ID
                       ,c.Contract_Classification_Cd)
            INSERT      #FinalRS
                        ( 
                         Client_Name
                        ,Sitegroup_Name
                        ,Site_Name
                        ,City
                        ,State_Name
                        ,ZipCode
                        ,Region_Name
                        ,Utility_Account_Number
                        ,Commodity_Name
                        ,Utility
                        ,CONTRACT_START_DATE
                        ,CONTRACT_END_DATE
                        ,ED_CONTRACT_NUMBER
                        ,[Contract Notification Date]
                        ,CONTRACT_PRICING_SUMMARY
                        ,CONTRACT_COMMENTS
                        ,RENEWAL_TYPE
                        ,RATE_NAME
                        ,[Regulated Markets Analyst]
                        ,[Regulated Markets Manager]
                        ,Account_count
                        ,Site_count
                        ,Division_Count
                        ,City_count
                        ,Region_count
                        ,Zip_COunt
                        ,Vendor_count
                        ,State_count
                        ,Rate_count
                        ,RA_count
                        ,RM_count
                        ,CEM_count
                        ,CEM_Name )
                        SELECT
                              CHA.Client_Name
                             ,CHA.Sitegroup_Name
                             ,CHA.Site_Name
                             ,CHA.City
                             ,CHA.State_Name
                             ,CHA.ZipCode
                             ,CHA.Region_Name
                             ,cha.Account_Number
                             ,com.commodity_name commodity_Name
                             ,CHA.Account_Vendor_Name
                             ,con.CONTRACT_START_DATE
                             ,con.CONTRACT_END_DATE
                             ,con.ED_CONTRACT_NUMBER
                             ,con.CONTRACT_END_DATE - con.NOTIFICATION_DAYS [Contract Notification Date]
                             ,con.CONTRACT_PRICING_SUMMARY
                             ,con.CONTRACT_COMMENTS
                             ,Renew.ENTITY_NAME RENEWAL_TYPE
                             ,CHA.RATE_NAME RATE_NAME
                             ,rr.[Regulated Markets Analyst] [Regulated Markets Analyst]
                             ,rr.[Regulated Markets Manager] [Regulated Markets Manager]
                             ,DENSE_RANK() OVER ( PARTITION BY con.Ed_contract_number ORDER BY cha.Account_id ) Account_count
                             ,DENSE_RANK() OVER ( PARTITION BY con.Ed_contract_number ORDER BY CHA.Site_id ) Site_count
                             ,DENSE_RANK() OVER ( PARTITION BY con.Ed_contract_number ORDER BY CHA.sitegroup_id ) Division_Count
                             ,DENSE_RANK() OVER ( PARTITION BY con.Ed_contract_number ORDER BY CHA.city ) City_count
                             ,DENSE_RANK() OVER ( PARTITION BY con.Ed_contract_number ORDER BY CHA.Region_Name ) Region_count
                             ,DENSE_RANK() OVER ( PARTITION BY con.Ed_contract_number ORDER BY CHA.ZipCode ) Zip_count
                             ,DENSE_RANK() OVER ( PARTITION BY con.Ed_contract_number ORDER BY cha.Account_vendor_id ) Vendor_count
                             ,DENSE_RANK() OVER ( PARTITION BY con.Ed_contract_number ORDER BY CHA.State_id ) State_count
                             ,DENSE_RANK() OVER ( PARTITION BY con.Ed_contract_number ORDER BY CHA.RATE_NAME ) Rate_count
                             ,DENSE_RANK() OVER ( PARTITION BY con.Ed_contract_number ORDER BY rr.[Regulated Markets Analyst] ) RA_count
                             ,DENSE_RANK() OVER ( PARTITION BY con.Ed_contract_number ORDER BY rr.[Regulated Markets Manager] ) RM_count
                             ,DENSE_RANK() OVER ( PARTITION BY con.Ed_contract_number ORDER BY ui.USERNAME ) CEM_count
                             ,ui.first_name + SPACE(1) + ui.last_name CEM_Name
                        FROM
                              dbo.contract con
                              JOIN maxcontract_cte mc
                                    ON mc.Class = con.Contract_Classification_Cd
                                       AND mc.ContractType = con.CONTRACT_TYPE_ID
                                       AND mc.MaxContractExp = con.CONTRACT_END_DATE
                              JOIN dbo.ENTITY Renew
                                    ON con.RENEWAL_TYPE_ID = Renew.ENTITY_ID
                              JOIN #AllAccounts SA
                                    ON SA.Supplier_Contract_ID = con.CONTRACT_ID
                                       AND sa.meter_id = mc.meterid
                              JOIN #AllAccounts CHA
                                    ON mc.meterid = cha.meter_id
                                       AND CHA.Account_Type = 'Utility'
                              JOIN @RA_RM_Details rr
                                    ON rr.VENDOR_ID = CHA.Account_Vendor_Id
                                       AND rr.commodity_id = con.COMMODITY_TYPE_ID
                              JOIN dbo.commodity com
                                    ON com.Commodity_Id = con.COMMODITY_TYPE_ID
                              JOIN dbo.CLIENT_CEM_MAP ccm
                                    ON ccm.CLIENT_ID = CHA.Client_Id
                              JOIN dbo.USER_INFO ui
                                    ON ui.USER_INFO_ID = ccm.USER_INFO_ID            
                                      
                                                            
                                      
      SELECT
            con_final.Client_Name
           ,CASE WHEN con1.Division_count > 1 THEN 'Multiple'
                 ELSE con_final.Sitegroup_Name
            END Division_Name
           ,CASE WHEN con1.site_count > 1 THEN 'Multiple'
                 ELSE con_final.site_Name
            END Site_Name
           ,CASE WHEN con1.City_count > 1 THEN 'Multiple'
                 ELSE con_final.City
            END City
           ,CASE WHEN con1.State_count > 1 THEN 'Multiple'
                 ELSE con_final.State_Name
            END State_Name
           ,CASE WHEN con1.Zip_Count > 1 THEN 'Multiple'
                 ELSE CAST(con_final.ZipCode AS VARCHAR(10))
            END ZipCode
           ,CASE WHEN con1.Region_Count > 1 THEN 'Multiple'
                 ELSE con_final.Region_Name
            END Region_Name
           ,CASE WHEN con1.Account_count > 1 THEN 'Multiple'
                 ELSE con_final.Utility_Account_Number
            END Utility_Account_Number
           ,con_final.Commodity_Name
           ,CASE WHEN con1.Vendor_count > 1 THEN 'Multiple'
                 ELSE con_final.Utility
            END Utility
           ,CONVERT(VARCHAR(20), con_final.Contract_Start_Date, 101) Contract_Start_Date
           ,CONVERT(VARCHAR(10), con_final.Contract_End_Date, 101) Contract_End_Date
           ,con_final.ED_Contract_Number
           ,CONVERT(VARCHAR(10), con_final.[Contract Notification Date], 101) [Contract Notification Date]
           ,con_final.Contract_Pricing_Summary
           ,con_final.Contract_Comments
           ,con_final.Renewal_Type
           ,CASE WHEN con1.Rate_count > 1 THEN 'Multiple'
                 ELSE con_final.Rate_Name
            END Rate_Name
           ,CASE WHEN con1.RA_count > 1 THEN 'Multiple'
                 ELSE con_final.[Regulated Markets Analyst]
            END [Regulated Markets Analyst]
           ,CASE WHEN con1.RM_count > 1 THEN 'Multiple'
                 ELSE con_final.[Regulated Markets Manager]
            END [Regulated Markets Manager]
           ,CASE WHEN con1.CEM_count > 1 THEN 'Multiple'
                 ELSE con_final.cem_name
            END cem_name
      FROM
            #FinalRS con_final
            JOIN ( SELECT
                        ED_CONTRACT_NUMBER
                       ,MAX(Account_count) Account_count
                       ,MAX(Site_count) Site_count
                       ,MAX(division_Count) division_Count
                       ,MAX(City_Count) City_Count
                       ,MAX(Region_Count) Region_Count
                       ,MAX(Zip_Count) Zip_Count
                       ,MAX(Rate_Count) Rate_Count
                       ,MAX(Vendor_COunt) Vendor_COunt
                       ,MAX(State_COUNT) State_COUNT
                       ,MAX(RA_Count) [RA_Count]
                       ,MAX(RM_Count) [RM_COunt]
                       ,MAX(CEM_Count) CEM_Count
                   FROM
                        #FinalRS con2
                   GROUP BY
                        con2.ED_CONTRACT_NUMBER ) con1
                  ON con_final.ED_CONTRACT_NUMBER = con1.ED_CONTRACT_NUMBER
      GROUP BY
            con_final.Client_Name
           ,CASE WHEN con1.Division_count > 1 THEN 'Multiple'
                 ELSE con_final.Sitegroup_Name
            END
           ,CASE WHEN con1.site_count > 1 THEN 'Multiple'
                 ELSE con_final.site_Name
            END
           ,CASE WHEN con1.City_count > 1 THEN 'Multiple'
                 ELSE con_final.City
            END
           ,CASE WHEN con1.State_count > 1 THEN 'Multiple'
                 ELSE con_final.State_Name
            END
           ,CASE WHEN con1.Zip_Count > 1 THEN 'Multiple'
                 ELSE CAST(con_final.ZipCode AS VARCHAR(10))
            END
           ,CASE WHEN con1.Region_Count > 1 THEN 'Multiple'
                 ELSE con_final.Region_Name
            END
           ,CASE WHEN con1.Account_count > 1 THEN 'Multiple'
                 ELSE con_final.Utility_Account_Number
            END
           ,con_final.Commodity_Name
           ,CASE WHEN con1.Vendor_count > 1 THEN 'Multiple'
                 ELSE con_final.Utility
            END
           ,con_final.CONTRACT_START_DATE
           ,con_final.CONTRACT_END_DATE
           ,con_final.ED_CONTRACT_NUMBER
           ,con_final.[Contract Notification Date]
           ,con_final.CONTRACT_PRICING_SUMMARY
           ,con_final.CONTRACT_COMMENTS
           ,con_final.RENEWAL_TYPE
           ,CASE WHEN con1.Rate_count > 1 THEN 'Multiple'
                 ELSE con_final.Rate_NAME
            END
           ,CASE WHEN con1.RA_count > 1 THEN 'Multiple'
                 ELSE con_final.[Regulated Markets Analyst]
            END
           ,CASE WHEN con1.RM_count > 1 THEN 'Multiple'
                 ELSE con_final.[Regulated Markets Manager]
            END
           ,CASE WHEN con1.CEM_count > 1 THEN 'Multiple'
                 ELSE con_final.cem_name
            END           
END  
  
  
  
;
GO


GRANT EXECUTE ON  [dbo].[Report_EP_NG_Contract_Expiration] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_EP_NG_Contract_Expiration] TO [CBMSApplication]
GO
