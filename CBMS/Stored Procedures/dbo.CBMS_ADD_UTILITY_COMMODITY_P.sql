SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********     
NAME:    
	dbo.CBMS_ADD_UTILITY_COMMODITY_P    

DESCRIPTION:  This procedure used to insert the records in VENDOR_COMMODITY_MAP table

INPUT PARAMETERS:    
      Name                        DataType          Default     Description    
---------------------------------------------------------------------------    
@vendor_id                        int,
@commodity_type_id                int
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
exec  dbo.CBMS_ADD_UTILITY_COMMODITY_P
   @vendor_id = 3051,
   @commodity_type_id = 290


Initials Name    
------------------------------------------------------------    
DR       Deana Ritter

Initials Date  Modification    
------------------------------------------------------------    
DR    8/4/2009	   Removed Linked Server Updates
 DMR		  09/10/2010 Modified for Quoted_Identifier

          
******/
CREATE PROCEDURE dbo.CBMS_ADD_UTILITY_COMMODITY_P
@vendor_id int,
@commodity_type_id int

as


INSERT INTO VENDOR_COMMODITY_MAP 
(
	VENDOR_ID,
	COMMODITY_TYPE_ID,
	IS_HISTORY
) 
VALUES 
(
	@vendor_id, 
	@commodity_type_id, 
	0
)
GO
GRANT EXECUTE ON  [dbo].[CBMS_ADD_UTILITY_COMMODITY_P] TO [CBMSApplication]
GO
