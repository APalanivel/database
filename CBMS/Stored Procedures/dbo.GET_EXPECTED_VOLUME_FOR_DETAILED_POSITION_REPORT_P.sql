
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_EXPECTED_VOLUME_FOR_DETAILED_POSITION_REPORT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@fromDate      	varchar(12)	          	
	@toDate        	varchar(12)	          	
	@fromYear      	varchar(12)	          	
	@toYear        	varchar(12)	          	
	@clientId      	int       	          	
	@divisionOrSiteId	int       	          	
	@levelStatus   	int       	          	
	@unitId        	int 
	@Country_Name   VARCHAR(200)		NULL      	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
EXEC GET_EXPECTED_VOLUME_FOR_DETAILED_POSITION_REPORT_P 
      @userId = 1
     ,@sessionId = 1
     ,@fromDate = '01/01/2004'
     ,@toDate = '12/31/2004'
     ,@fromYear = '2004'
     ,@toYear = '2004'
     ,@clientId = 10086
     ,@divisionOrSiteId = 627
     ,@levelStatus = 1
     ,@unitId = 25
     ,@Country_Name = NULL


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
    SP			Sandeep Pigilam
    

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier
 SP			  2014-11-19 For RM Scenario Report Enhancement,Position and Hedge report Filter out non US sites added @Country_Name as optional input param .
						 


******/

CREATE PROCEDURE [dbo].[GET_EXPECTED_VOLUME_FOR_DETAILED_POSITION_REPORT_P]
      @userId VARCHAR
     ,@sessionId VARCHAR
     ,@fromDate VARCHAR(12)
     ,@toDate VARCHAR(12)
     ,@fromYear VARCHAR(12)
     ,@toYear VARCHAR(12)
     ,@clientId INT
     ,@divisionOrSiteId INT
     ,@levelStatus INT
     ,@unitId INT
     ,@Country_Name VARCHAR(200) = NULL
AS 
SET NOCOUNT ON


DECLARE
      @financial INT
     ,@Physical INT
     
DECLARE @RM_FORECAST_VOLUME TABLE
      ( 
       FORECAST_AS_OF_DATE DATETIME )     
SELECT
      @financial = ENTITY_ID
FROM
      ENTITY
WHERE
      ENTITY_TYPE = 273
      AND ENTITY_NAME LIKE 'financial'

SELECT
      @Physical = ENTITY_ID
FROM
      ENTITY
WHERE
      ENTITY_TYPE = 273
      AND ENTITY_NAME LIKE 'Physical'

INSERT      INTO @RM_FORECAST_VOLUME
            ( 
             FORECAST_AS_OF_DATE )
            SELECT
                  MAX(FORECAST_AS_OF_DATE)
            FROM
                  RM_FORECAST_VOLUME
            WHERE
                  FORECAST_YEAR = @fromYear
                  AND CLIENT_ID = @clientId
            UNION
            SELECT
                  MAX(FORECAST_AS_OF_DATE)
            FROM
                  RM_FORECAST_VOLUME
            WHERE
                  FORECAST_YEAR = @toYear
                  AND CLIENT_ID = @clientId       

IF @levelStatus = 1 
      SELECT
            CAST(SUM(volumedetails.VOLUME * consumption.CONVERSION_FACTOR) AS DECIMAL(15, 3)) TOTAL_VOLUME
           ,CONVERT (VARCHAR(12), volumedetails.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER
           ,'Financial' HEDGE_TYPE
      FROM
            RM_FORECAST_VOLUME_DETAILS volumedetails
            INNER JOIN RM_FORECAST_VOLUME volume
                  ON volumedetails.RM_FORECAST_VOLUME_ID = volume.RM_FORECAST_VOLUME_ID
            INNER JOIN RM_ONBOARD_HEDGE_SETUP onboard
                  ON volumedetails.SITE_ID = onboard.SITE_ID
            INNER JOIN CONSUMPTION_UNIT_CONVERSION consumption
                  ON onboard.VOLUME_UNITS_TYPE_ID = consumption.BASE_UNIT_ID
      WHERE
            EXISTS ( SELECT
                        1
                     FROM
                        core.Client_Hier ch
                        INNER JOIN dbo.RM_ONBOARD_HEDGE_SETUP hedge
                              ON ch.Site_Id = hedge.SITE_ID
                     WHERE
                        ch.CLIENT_ID = @clientId
                        AND ( @Country_Name IS NULL
                              OR ch.Country_Name = @Country_Name )
                        AND hedge.INCLUDE_IN_REPORTS = 1
                        AND hedge.HEDGE_TYPE_ID = @financial
                        AND volumedetails.SITE_ID = ch.SITE_ID )
            AND EXISTS ( SELECT
                              1
                         FROM
                              @RM_FORECAST_VOLUME fcv
                         WHERE
                              volume.FORECAST_AS_OF_DATE = fcv.FORECAST_AS_OF_DATE )
            AND volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(VARCHAR(12), @fromDate, 101)
                                               AND     CONVERT(VARCHAR(12), @toDate, 101)
            AND consumption.CONVERTED_UNIT_ID = @unitId
      GROUP BY
            MONTH_IDENTIFIER
      UNION
      SELECT
            CAST(SUM(volumedetails.VOLUME * consumption.CONVERSION_FACTOR) AS DECIMAL(15, 3)) TOTAL_VOLUME
           ,CONVERT (VARCHAR(12), volumedetails.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER
           ,'Physical' HEDGE_TYPE
      FROM
            RM_FORECAST_VOLUME_DETAILS volumedetails
            INNER JOIN RM_FORECAST_VOLUME volume
                  ON volumedetails.RM_FORECAST_VOLUME_ID = volume.RM_FORECAST_VOLUME_ID
            INNER JOIN RM_ONBOARD_HEDGE_SETUP onboard
                  ON volumedetails.SITE_ID = onboard.SITE_ID
            INNER JOIN CONSUMPTION_UNIT_CONVERSION consumption
                  ON onboard.VOLUME_UNITS_TYPE_ID = consumption.BASE_UNIT_ID
      WHERE
            EXISTS ( SELECT
                        1
                     FROM
                        core.Client_Hier ch
                        INNER JOIN dbo.RM_ONBOARD_HEDGE_SETUP hedge
                              ON ch.Site_Id = hedge.SITE_ID
                     WHERE
                        ch.CLIENT_ID = @clientId
                        AND ( @Country_Name IS NULL
                              OR ch.Country_Name = @Country_Name )
                        AND hedge.INCLUDE_IN_REPORTS = 1
                        AND hedge.HEDGE_TYPE_ID = @Physical
                        AND volumedetails.SITE_ID = ch.SITE_ID )
            AND EXISTS ( SELECT
                              1
                         FROM
                              @RM_FORECAST_VOLUME fcv
                         WHERE
                              volume.FORECAST_AS_OF_DATE = fcv.FORECAST_AS_OF_DATE )
            AND volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(VARCHAR(12), @fromDate, 101)
                                               AND     CONVERT(VARCHAR(12), @toDate, 101)
            AND consumption.CONVERTED_UNIT_ID = @unitId
      GROUP BY
            MONTH_IDENTIFIER



ELSE 
      IF @levelStatus = 2 
            SELECT
                  CAST(SUM(volumedetails.VOLUME * consumption.CONVERSION_FACTOR) AS DECIMAL(15, 3)) TOTAL_VOLUME
                 ,CONVERT (VARCHAR(12), volumedetails.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER
                 ,'Financial' HEDGE_TYPE
            FROM
                  RM_FORECAST_VOLUME_DETAILS volumedetails
                  INNER JOIN RM_FORECAST_VOLUME volume
                        ON volumedetails.RM_FORECAST_VOLUME_ID = volume.RM_FORECAST_VOLUME_ID
                  INNER JOIN RM_ONBOARD_HEDGE_SETUP onboard
                        ON volumedetails.SITE_ID = onboard.SITE_ID
                  INNER JOIN CONSUMPTION_UNIT_CONVERSION consumption
                        ON onboard.VOLUME_UNITS_TYPE_ID = consumption.BASE_UNIT_ID
            WHERE
                  EXISTS ( SELECT
                              1
                           FROM
                              core.Client_Hier ch
                              INNER JOIN dbo.RM_ONBOARD_HEDGE_SETUP hedge
                                    ON ch.Site_Id = hedge.SITE_ID
                           WHERE
                              ch.Sitegroup_Id = @divisionOrSiteId
                              AND ( @Country_Name IS NULL
                                    OR ch.Country_Name = @Country_Name )
                              AND hedge.INCLUDE_IN_REPORTS = 1
                              AND hedge.HEDGE_TYPE_ID = @Financial
                              AND volumedetails.SITE_ID = ch.SITE_ID )
                  AND EXISTS ( SELECT
                                    1
                               FROM
                                    @RM_FORECAST_VOLUME fcv
                               WHERE
                                    volume.FORECAST_AS_OF_DATE = fcv.FORECAST_AS_OF_DATE )
                  AND volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(VARCHAR(12), @fromDate, 101)
                                                     AND     CONVERT(VARCHAR(12), @toDate, 101)
                  AND consumption.CONVERTED_UNIT_ID = @unitId
            GROUP BY
                  MONTH_IDENTIFIER
            UNION
            SELECT
                  CAST(SUM(volumedetails.VOLUME * consumption.CONVERSION_FACTOR) AS DECIMAL(15, 3)) TOTAL_VOLUME
                 ,CONVERT (VARCHAR(12), volumedetails.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER
                 ,'Physical' HEDGE_TYPE
            FROM
                  RM_FORECAST_VOLUME_DETAILS volumedetails
                  INNER JOIN RM_FORECAST_VOLUME volume
                        ON volumedetails.RM_FORECAST_VOLUME_ID = volume.RM_FORECAST_VOLUME_ID
                  INNER JOIN RM_ONBOARD_HEDGE_SETUP onboard
                        ON volumedetails.SITE_ID = onboard.SITE_ID
                  INNER JOIN CONSUMPTION_UNIT_CONVERSION consumption
                        ON onboard.VOLUME_UNITS_TYPE_ID = consumption.BASE_UNIT_ID
            WHERE
                  EXISTS ( SELECT
                              1
                           FROM
                              core.Client_Hier ch
                              INNER JOIN dbo.RM_ONBOARD_HEDGE_SETUP hedge
                                    ON ch.Site_Id = hedge.SITE_ID
                           WHERE
                              ch.Sitegroup_Id = @divisionOrSiteId
                              AND ( @Country_Name IS NULL
                                    OR ch.Country_Name = @Country_Name )
                              AND hedge.INCLUDE_IN_REPORTS = 1
                              AND hedge.HEDGE_TYPE_ID = @Physical
                              AND volumedetails.SITE_ID = ch.SITE_ID )
                  AND EXISTS ( SELECT
                                    1
                               FROM
                                    @RM_FORECAST_VOLUME fcv
                               WHERE
                                    volume.FORECAST_AS_OF_DATE = fcv.FORECAST_AS_OF_DATE )
                  AND volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(VARCHAR(12), @fromDate, 101)
                                                     AND     CONVERT(VARCHAR(12), @toDate, 101)
                  AND consumption.CONVERTED_UNIT_ID = @unitId
            GROUP BY
                  MONTH_IDENTIFIER


      ELSE 
            IF @levelStatus = 3 
                  SELECT
                        CAST(SUM(volumedetails.VOLUME * consumption.CONVERSION_FACTOR) AS DECIMAL(15, 3)) TOTAL_VOLUME
                       ,CONVERT (VARCHAR(12), volumedetails.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER
                       ,'Financial' HEDGE_TYPE
                  FROM
                        RM_FORECAST_VOLUME_DETAILS volumedetails
                        INNER JOIN RM_FORECAST_VOLUME volume
                              ON volumedetails.RM_FORECAST_VOLUME_ID = volume.RM_FORECAST_VOLUME_ID
                        INNER JOIN RM_ONBOARD_HEDGE_SETUP onboard
                              ON volumedetails.SITE_ID = onboard.SITE_ID
                        INNER JOIN CONSUMPTION_UNIT_CONVERSION consumption
                              ON onboard.VOLUME_UNITS_TYPE_ID = consumption.BASE_UNIT_ID
                  WHERE
                        EXISTS ( SELECT
                                    1
                                 FROM
                                    core.Client_Hier ch
                                    INNER JOIN dbo.RM_ONBOARD_HEDGE_SETUP hedge
                                          ON ch.Site_Id = hedge.SITE_ID
                                 WHERE
                                    ch.Site_Id = @divisionOrSiteId
                                    AND ( @Country_Name IS NULL
                                          OR ch.Country_Name = @Country_Name )
                                    AND hedge.INCLUDE_IN_REPORTS = 1
                                    AND hedge.HEDGE_TYPE_ID = @Financial
                                    AND volumedetails.SITE_ID = ch.SITE_ID )
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          @RM_FORECAST_VOLUME fcv
                                     WHERE
                                          volume.FORECAST_AS_OF_DATE = fcv.FORECAST_AS_OF_DATE )
                        AND volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(VARCHAR(12), @fromDate, 101)
                                                           AND     CONVERT(VARCHAR(12), @toDate, 101)
                        AND consumption.CONVERTED_UNIT_ID = @unitId
                  GROUP BY
                        MONTH_IDENTIFIER
                  UNION
                  SELECT
                        CAST(SUM(volumedetails.VOLUME * consumption.CONVERSION_FACTOR) AS DECIMAL(15, 3)) TOTAL_VOLUME
                       ,CONVERT (VARCHAR(12), volumedetails.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER
                       ,'Physical' HEDGE_TYPE
                  FROM
                        RM_FORECAST_VOLUME_DETAILS volumedetails
                        INNER JOIN RM_FORECAST_VOLUME volume
                              ON volumedetails.RM_FORECAST_VOLUME_ID = volume.RM_FORECAST_VOLUME_ID
                        INNER JOIN RM_ONBOARD_HEDGE_SETUP onboard
                              ON volumedetails.SITE_ID = onboard.SITE_ID
                        INNER JOIN CONSUMPTION_UNIT_CONVERSION consumption
                              ON onboard.VOLUME_UNITS_TYPE_ID = consumption.BASE_UNIT_ID
                  WHERE
                        EXISTS ( SELECT
                                    1
                                 FROM
                                    core.Client_Hier ch
                                    INNER JOIN dbo.RM_ONBOARD_HEDGE_SETUP hedge
                                          ON ch.Site_Id = hedge.SITE_ID
                                 WHERE
                                    ch.Site_Id = @divisionOrSiteId
                                    AND ( @Country_Name IS NULL
                                          OR ch.Country_Name = @Country_Name )
                                    AND hedge.INCLUDE_IN_REPORTS = 1
                                    AND hedge.HEDGE_TYPE_ID = @Physical
                                    AND volumedetails.SITE_ID = ch.SITE_ID )
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          @RM_FORECAST_VOLUME fcv
                                     WHERE
                                          volume.FORECAST_AS_OF_DATE = fcv.FORECAST_AS_OF_DATE )
                        AND volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(VARCHAR(12), @fromDate, 101)
                                                           AND     CONVERT(VARCHAR(12), @toDate, 101)
                        AND consumption.CONVERTED_UNIT_ID = @unitId
                  GROUP BY
                        MONTH_IDENTIFIER





            ELSE 
                  IF @levelStatus = 4 
                        SELECT
                              CAST(SUM(volumedetails.VOLUME * consumption.CONVERSION_FACTOR) AS DECIMAL(15, 3)) TOTAL_VOLUME
                             ,CONVERT (VARCHAR(12), volumedetails.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER
                             ,'Financial' HEDGE_TYPE
                        FROM
                              RM_FORECAST_VOLUME_DETAILS volumedetails
                              INNER JOIN RM_FORECAST_VOLUME volume
                                    ON volumedetails.RM_FORECAST_VOLUME_ID = volume.RM_FORECAST_VOLUME_ID
                              INNER JOIN RM_ONBOARD_HEDGE_SETUP onboard
                                    ON volumedetails.SITE_ID = onboard.SITE_ID
                              INNER JOIN CONSUMPTION_UNIT_CONVERSION consumption
                                    ON onboard.VOLUME_UNITS_TYPE_ID = consumption.BASE_UNIT_ID
                        WHERE
                              EXISTS ( SELECT
                                          1
                                       FROM
                                          core.Client_Hier ch
                                          INNER JOIN dbo.RM_GROUP_SITE_MAP groupMap
                                                ON ch.Site_Id = groupMap.SITE_ID
                                          INNER JOIN dbo.RM_ONBOARD_HEDGE_SETUP hedge
                                                ON ch.SITE_ID = hedge.SITE_ID
                                       WHERE
                                          groupMap.RM_GROUP_ID = @divisionOrSiteId
                                          AND ( @Country_Name IS NULL
                                                OR ch.Country_Name = @Country_Name )
                                          AND hedge.INCLUDE_IN_REPORTS = 1
                                          AND hedge.HEDGE_TYPE_ID = @Financial
                                          AND volumedetails.SITE_ID = ch.SITE_ID )
                              AND EXISTS ( SELECT
                                                1
                                           FROM
                                                @RM_FORECAST_VOLUME fcv
                                           WHERE
                                                volume.FORECAST_AS_OF_DATE = fcv.FORECAST_AS_OF_DATE )
                              AND volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(VARCHAR(12), @fromDate, 101)
                                                                 AND     CONVERT(VARCHAR(12), @toDate, 101)
                              AND consumption.CONVERTED_UNIT_ID = @unitId
                        GROUP BY
                              MONTH_IDENTIFIER
                        UNION
                        SELECT
                              CAST(SUM(volumedetails.VOLUME * consumption.CONVERSION_FACTOR) AS DECIMAL(15, 3)) TOTAL_VOLUME
                             ,CONVERT (VARCHAR(12), volumedetails.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER
                             ,'Physical' HEDGE_TYPE
                        FROM
                              RM_FORECAST_VOLUME_DETAILS volumedetails
                              INNER JOIN RM_FORECAST_VOLUME volume
                                    ON volumedetails.RM_FORECAST_VOLUME_ID = volume.RM_FORECAST_VOLUME_ID
                              INNER JOIN RM_ONBOARD_HEDGE_SETUP onboard
                                    ON volumedetails.SITE_ID = onboard.SITE_ID
                              INNER JOIN CONSUMPTION_UNIT_CONVERSION consumption
                                    ON onboard.VOLUME_UNITS_TYPE_ID = consumption.BASE_UNIT_ID
                        WHERE
                              EXISTS ( SELECT
                                          1
                                       FROM
                                          core.Client_Hier ch
                                          INNER JOIN dbo.RM_GROUP_SITE_MAP groupMap
                                                ON ch.Site_Id = groupMap.SITE_ID
                                          INNER JOIN dbo.RM_ONBOARD_HEDGE_SETUP hedge
                                                ON ch.SITE_ID = hedge.SITE_ID
                                       WHERE
                                          groupMap.RM_GROUP_ID = @divisionOrSiteId
                                          AND ( @Country_Name IS NULL
                                                OR ch.Country_Name = @Country_Name )
                                          AND hedge.INCLUDE_IN_REPORTS = 1
                                          AND hedge.HEDGE_TYPE_ID = @Physical
                                          AND volumedetails.SITE_ID = ch.SITE_ID )
                              AND EXISTS ( SELECT
                                                1
                                           FROM
                                                @RM_FORECAST_VOLUME fcv
                                           WHERE
                                                volume.FORECAST_AS_OF_DATE = fcv.FORECAST_AS_OF_DATE )
                              AND volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(VARCHAR(12), @fromDate, 101)
                                                                 AND     CONVERT(VARCHAR(12), @toDate, 101)
                              AND consumption.CONVERTED_UNIT_ID = @unitId
                        GROUP BY
                              MONTH_IDENTIFIER

;
GO

GRANT EXECUTE ON  [dbo].[GET_EXPECTED_VOLUME_FOR_DETAILED_POSITION_REPORT_P] TO [CBMSApplication]
GO
