SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********    
NAME:  dbo.Validate_Contract_Lock_Dtl_Dates    
    
DESCRIPTION:      
    
 Validates the passed in start and end dates for a Contract Lock.    
 The passed in start and end dates should not overlap with the dates of any other Contract Locks for a Contract.    
 The SP will return the start date of the Contract Lock which has dates that are overlapping with the passed in dates.    
 If this SP doesn't return any records, it means the passed in dates are valid.    
    
INPUT PARAMETERS:    
      Name        DataType  Default     Description    
-----------------------------------------------------------------------------------    
   @Contract_Id   INT    
   @Start_Dt      DATETIME    
   @Expiration_Dt DATETIME       
    
OUTPUT PARAMETERS:    
      Name        DataType          Default     Description    
------------------------------------------------------------    
    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
 EXEC dbo.Validate_Contract_Lock_Dtl_Dates 82530,'2011-01-01','2011-01-01'
 EXEC dbo.Validate_Contract_Lock_Dtl_Dates 82530,'2010-11-01','2012-01-01'
 
    
AUTHOR INITIALS:    
 Initials Name    
------------------------------------------------------------    
 AKR   Ashok Kumar Raju    
    
    
 Initials Date  Modification    
------------------------------------------------------------    
 AKR 2012-09-20  Created    
    
******/    
CREATE PROCEDURE dbo.Validate_Contract_Lock_Dtl_Dates    
(     
 @Contract_Id INT    
,@Start_Dt DATETIME    
,@Expiration_Dt DATETIME  
  )    
AS     
BEGIN    
      SET NOCOUNT ON    
     
          
      SELECT    
            cld.Start_Dt    
      FROM    
            dbo.Contract_Lock_Dtl cld    
      WHERE    
           cld.Contract_Id = @Contract_Id  
           AND @Start_Dt BETWEEN cld.Start_Dt AND cld.Expiration_Dt    
           AND @Expiration_Dt BETWEEN cld.Start_Dt AND cld.Expiration_Dt    
END    
;
GO
GRANT EXECUTE ON  [dbo].[Validate_Contract_Lock_Dtl_Dates] TO [CBMSApplication]
GO
