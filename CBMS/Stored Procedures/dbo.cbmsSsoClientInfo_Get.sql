SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE        procedure [dbo].[cbmsSsoClientInfo_Get]
	( @MyAccountId int
	, @client_id int
	)
AS
BEGIN


	select c.client_id
		, ct.entity_name client_type
		, c.client_name
		, fys.entity_name fiscalyear_startmonth_type
		, c.ubm_service_id
		, ubms.ubm_service_name
		, u.ubm_name
		, c.ubm_start_date
		, c.dsm_strategy
		, c.is_sep_issued
		, c.sep_issue_date
		, vw.start_month
		, (usr.first_name + ' ' + usr.last_name) full_name
		, d.trigger_rights
		, d.client_id
	   from client c
	   join entity ct on ct.entity_id = c.client_type_id
--	   join entity rft on rft.entity_id = c.report_frequency_type_id
	   join entity fys on fys.entity_id = c.fiscalyear_startmonth_type_id
left outer join ubm_service ubms on ubms.ubm_service_id = c.ubm_service_id
	   join vwClientFiscalYearStartMonth vw on vw.client_id = c.client_id
	   join client_cem_map cem on cem.client_id = c.client_id
	   join user_info usr on usr.user_info_id = cem.user_info_id
left outer join ubm u on u.ubm_id = ubms.ubm_id
left outer	   join (select client_id
			, trigger_rights 
		from division
		where is_corporate_division = 1) d on d.client_id = c.client_id
     	   where c.client_id = @client_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoClientInfo_Get] TO [CBMSApplication]
GO
