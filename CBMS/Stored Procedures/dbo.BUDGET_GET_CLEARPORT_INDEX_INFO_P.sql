SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO













--exec BUDGET_GET_CLEARPORT_INDEX_INFO_P 1,1

CREATE  PROCEDURE dbo.BUDGET_GET_CLEARPORT_INDEX_INFO_P 
@userId varchar,
@sessionId varchar
as 
begin
	set nocount on

	select clearport_index_id, clearport_index from clearport_index order by clearport_index 
end













GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_CLEARPORT_INDEX_INFO_P] TO [CBMSApplication]
GO
