SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:      
	dbo.Client_Contract_Vendor_Dtls_Sel_By_Keyword    
    
DESCRIPTION: 
			Vendor name Smart search implemented.
  
INPUT PARAMETERS:  
	Name				DataType			Default			Description      
------------------------------------------------------------------------    
	@Vendor_Name		VARCHAR(200)		NULL
	@Vendor_Type_Id		INT					NULL
  
OUTPUT PARAMETERS:      
 Name				DataType			Default			Description      
------------------------------------------------------------------------    
  
USAGE EXAMPLES:  
------------------------------------------------------------------------    
  
 EXEC dbo.Client_Contract_Vendor_Dtls_Sel_By_Keyword 'America','Utility'
 EXEC dbo.Client_Contract_Vendor_Dtls_Sel_By_Keyword 'American Building Maintena','Utility'
 EXEC dbo.Client_Contract_Vendor_Dtls_Sel_By_Keyword NULL,'Utility'
 
   
   
Author Initials:          
	Initials	Name        
------------------------------------------------------------------------    
	RR			Raghu Reddy
         
 Modifications:          
	Initials	Date			Modification          
------------------------------------------------------------------------    
	RR			2019-06-23		GRM - Created

******/

CREATE PROCEDURE [dbo].[Client_Contract_Vendor_Dtls_Sel_By_Keyword]
    (
        @Vendor_Name VARCHAR(200) = NULL
        , @Vendor_Type VARCHAR(200) = NULL
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Tbl_Vendors AS TABLE
              (
                  VENDOR_ID INT
                  , VENDOR_NAME VARCHAR(200)
                  , Row_Num INT
              );

        DECLARE @Vendor_Type_Id INT = NULL;

        SELECT
            @Vendor_Type_Id = e.ENTITY_ID
        FROM
            dbo.ENTITY e
        WHERE
            ENTITY_NAME = @Vendor_Type
            AND ENTITY_TYPE = 155;

        INSERT INTO @Tbl_Vendors
             (
                 VENDOR_ID
                 , VENDOR_NAME
                 , Row_Num
             )
        SELECT
            v.VENDOR_ID
            , v.VENDOR_NAME
            , ROW_NUMBER() OVER (ORDER BY
                                     v.VENDOR_NAME)
        FROM
            dbo.VENDOR v
            INNER JOIN dbo.ENTITY e
                ON v.VENDOR_TYPE_ID = e.ENTITY_ID
        WHERE
            EXISTS (   SELECT
                            1
                       FROM
                            Core.Client_Hier_Account cha
                       WHERE
                            cha.Account_Type = 'supplier'
                            AND v.VENDOR_ID = cha.Account_Vendor_Id)
            AND (   @Vendor_Name IS NULL
                    OR  v.VENDOR_NAME LIKE '%' + @Vendor_Name + '%')
            AND (   @Vendor_Type_Id IS NULL
                    OR  v.VENDOR_TYPE_ID = @Vendor_Type_Id)
            AND v.IS_HISTORY = 0;

        SELECT
            VENDOR_ID
            , VENDOR_NAME
            , Row_Num
        FROM
            @Tbl_Vendors
        WHERE
            Row_Num BETWEEN @Start_Index
                    AND     @End_Index
        ORDER BY
            Row_Num;

    END;
GO
GRANT EXECUTE ON  [dbo].[Client_Contract_Vendor_Dtls_Sel_By_Keyword] TO [CBMSApplication]
GO
