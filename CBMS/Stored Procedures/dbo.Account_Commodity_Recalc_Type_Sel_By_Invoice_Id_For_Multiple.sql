SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:      
 Account_Commodity_Recalc_Type_Sel_By_Invoice_Id_For_Multiple      
      
DESCRIPTION:      
      
      
INPUT PARAMETERS:      
 Name				DataType		Default				Description      
---------------------------------------------------------------------      
 @cu_invoice_id		Int      
 @Commodity_Id		Int                   
      
OUTPUT PARAMETERS:      
 Name				DataType		Default				Description      
---------------------------------------------------------------------      
      
USAGE EXAMPLES:      
---------------------------------------------------------------------      


 EXEC Account_Commodity_Recalc_Type_Sel_By_Invoice_Id_For_Multiple 75254082,290

 

EXEC Account_Commodity_Recalc_Type_Sel_By_Invoice_Id_For_Multiple 
 @cu_invoice_id =90031651
, @Commodity_Id = 290
 , @Account_Id  = 1280212 

EXEC Account_Commodity_Recalc_Type_Sel_By_Invoice_Id_For_Multiple 93935724,290 ,1909646
      
AUTHOR INITIALS:  
    
 Initials	Name      
---------------------------------------------------------------------      
   RKV      Ravi Vegesna
      
MODIFICATIONS      
      
Initials	Date			Modification      
---------------------------------------------------------------------      
			2019-10-01		Created  
HG			2019-06-30		Modified to return the recors only for the given commodity id
NR			2019-11-05		Add Contract - Get the recalc from account_commodity_Recalc table.
NR			2019-12-17		MAINT-9643 - Reverted as per existing If No recalc we dont't retun the any record for supplier & utility. 
NR			2020-22-01		MAINT-9643 - Removed ED_CONTRACT_NUMBER column.
NR			2020-02-10		PRSUPPORT-3494 - Handled Consolidated accounts if any recalc copied from Contract level.
NR			2020-05-15		PRSUPPORT-3823 - Retuen  the records if supplier or utiltiy any one is not null recalc.
NR			2020-05-27		SE2017-980 - Recalc retun based invoice begin & end date insted of servicemonth.
******/
CREATE PROCEDURE [dbo].[Account_Commodity_Recalc_Type_Sel_By_Invoice_Id_For_Multiple]
    (
        @cu_invoice_id INT
        , @Commodity_Id INT = NULL
        , @Account_Id INT = NULL
    )
AS
    BEGIN

        DECLARE
            @Out_Account_Id INT
            , @Max_Account_Row_Num INT
            , @Counter INT = 1;

        CREATE TABLE #Account_List
             (
                 Id INT IDENTITY(1, 1)
                 , Account_Id INT
             );

        CREATE TABLE #All_Account_Type_Recalcs
             (
                 Account_Commodity_Invoice_Recalc_Type_Id INT
                 , Account_Id INT
                 , Commodity_Id INT
                 , Code_Id INT
                 , Code_Value VARCHAR(25)
                 , Start_Dt DATE
                 , End_Dt DATE
                 , Determinant_Source_Cd INT
             );



        DECLARE @Group_Legacy_Name TABLE
              (
                  GROUP_INFO_ID INT
                  , GROUP_NAME VARCHAR(200)
                  , Legacy_Group_Name VARCHAR(200)
              );

        INSERT INTO @Group_Legacy_Name
             (
                 GROUP_INFO_ID
                 , GROUP_NAME
                 , Legacy_Group_Name
             )
        EXEC dbo.Group_Info_Sel_By_Group_Legacy
            @Group_Legacy_Name_Cd_Value = 'Standing Data Team';


        INSERT INTO #Account_List
             (
                 Account_Id
             )
        SELECT
            cism.Account_ID
        FROM
            dbo.CU_INVOICE_SERVICE_MONTH cism
        WHERE
            cism.CU_INVOICE_ID = @cu_invoice_id
            AND (   @Account_Id IS NULL
                    OR  cism.Account_ID = @Account_Id)
        GROUP BY
            cism.Account_ID;

        SELECT  @Max_Account_Row_Num = MAX(al.Id)FROM   #Account_List al;

        WHILE (@Counter <= @Max_Account_Row_Num)
            BEGIN

                SET @Out_Account_Id = NULL;


                SELECT
                    @Out_Account_Id = al.Account_Id
                FROM
                    #Account_List al
                WHERE
                    al.Id = @Counter;


                INSERT INTO #All_Account_Type_Recalcs
                     (
                         Account_Commodity_Invoice_Recalc_Type_Id
                         , Account_Id
                         , Commodity_Id
                         , Code_Id
                         , Code_Value
                         , Start_Dt
                         , End_Dt
                         , Determinant_Source_Cd
                     )
                EXEC dbo.Suppllier_Utility_Account_Commodity_Invoice_Recalc_Type_Sel_By_Account_Id
                    @Account_Id = @Out_Account_Id;

                SET @Counter = @Counter + 1;

            END;





        DECLARE @Account_Service_Month_Recacl_type TABLE
              (
                  Account_id INT
                  , Commodity_Id INT
                  , Recalc_Type_Cd INT
                  , Recalc_Type VARCHAR(25)
                  , Recalc_Type_Dsc VARCHAR(255)
                  , Determinant_Source_Cd INT
              );


        -- to consider the latest recalc_type after removing the recalc_type 'No_Recalc'
        INSERT INTO @Account_Service_Month_Recacl_type
             (
                 Account_id
                 , Commodity_Id
                 , Recalc_Type_Cd
                 , Recalc_Type
                 , Recalc_Type_Dsc
                 , Determinant_Source_Cd
             )
        SELECT
            cism.Account_ID
            , acirt.Commodity_Id
            , acirt.Code_Id
            , c.Code_Value
            , c.Code_Dsc
            , acirt.Determinant_Source_Cd
        FROM
            #All_Account_Type_Recalcs acirt
            INNER JOIN dbo.Code c
                ON c.Code_Id = acirt.Code_Id
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON acirt.Account_Id = cism.Account_ID
        WHERE
            cism.CU_INVOICE_ID = @cu_invoice_id
            AND (   @Account_Id IS NULL
                    OR  cism.Account_ID = @Account_Id)
            AND (   cism.Begin_Dt BETWEEN acirt.Start_Dt
                                  AND     ISNULL(acirt.End_Dt, '9999-12-31')
                    OR  cism.End_Dt BETWEEN acirt.Start_Dt
                                    AND     ISNULL(acirt.End_Dt, '9999-12-31')
                    OR  acirt.Start_Dt BETWEEN cism.Begin_Dt
                                       AND     cism.End_Dt
                    OR  ISNULL(acirt.End_Dt, '9999-12-31') BETWEEN cism.Begin_Dt
                                                           AND     cism.End_Dt)
            AND acirt.Code_Value NOT LIKE 'No Recalc%'
            AND (   @Commodity_Id IS NULL
                    OR  acirt.Commodity_Id = @Commodity_Id)
        GROUP BY
            cism.Account_ID
            , acirt.Commodity_Id
            , acirt.Code_Id
            , c.Code_Value
            , c.Code_Dsc
            , acirt.Determinant_Source_Cd;

        SELECT
            cism.Account_ID
            , cha.Account_Number
            , com.Commodity_Id
            , com.Commodity_Name
            , CASE WHEN e.ENTITY_NAME = 'Utility' THEN acirt.Recalc_Type
                  ELSE NULL
              END AS Recalc_Type
            , CASE WHEN e.ENTITY_NAME = 'Utility' THEN acirt.Recalc_Type_Cd
                  ELSE NULL
              END AS Invoice_Recalc_Type_Cd
            , cha.Account_Group_ID
            , e.ENTITY_ID AS Account_Type_Id
            , e.ENTITY_NAME AS Account_Type
            , CASE WHEN e.ENTITY_NAME = 'Utility' THEN NULL
                  ELSE acirt.Recalc_Type_Dsc
              END AS contract_recalc_type
            , CASE WHEN e.ENTITY_NAME = 'Utility' THEN NULL
                  ELSE acirt.Recalc_Type_Cd
              END AS contract_recalc_Cd
            , ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME AS analyst
            , ch.Client_Id
            , ch.Client_Name
            , CASE WHEN ciacrl.Is_Locked IS NULL THEN 0
                  ELSE ciacrl.Is_Locked
              END Is_Locked
            , acirt.Determinant_Source_Cd
            , Source_Cd.Code_Value AS Determinant_Source
        INTO
            #Account_Commodity_Recalc_Type
        FROM
            dbo.CU_INVOICE_SERVICE_MONTH cism
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = cism.Account_ID
            LEFT OUTER JOIN @Account_Service_Month_Recacl_type acirt
                ON acirt.Account_id = cism.Account_ID
                   AND  cha.Commodity_Id = acirt.Commodity_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN dbo.Commodity com
                ON com.Commodity_Id = cha.Commodity_Id
            INNER JOIN dbo.ENTITY e
                ON e.ENTITY_NAME = cha.Account_Type
            LEFT JOIN(dbo.UTILITY_DETAIL ud
                      JOIN Utility_Detail_Analyst_Map udmap
                          ON udmap.Utility_Detail_ID = ud.UTILITY_DETAIL_ID
                      JOIN GROUP_INFO gi
                          ON gi.GROUP_INFO_ID = udmap.Group_Info_ID
                      INNER JOIN @Group_Legacy_Name gil
                          ON gi.GROUP_INFO_ID = gil.GROUP_INFO_ID)
                ON ud.VENDOR_ID = cha.Account_Vendor_Id
            LEFT OUTER JOIN USER_INFO ui
                ON ui.USER_INFO_ID = udmap.Analyst_ID
            LEFT OUTER JOIN dbo.CONTRACT AS con
                ON con.CONTRACT_ID = cha.Supplier_Contract_ID
            LEFT OUTER JOIN dbo.Cu_Invoice_Account_Commodity ciac
                ON ciac.Account_Id = cha.Account_Id
                   AND  ciac.Commodity_Id = cha.Commodity_Id
                   AND  ciac.Cu_Invoice_Id = cism.CU_INVOICE_ID
            LEFT OUTER JOIN dbo.Cu_Invoice_Account_Commodity_Recalc_Lock ciacrl
                ON ciacrl.Cu_Invoice_Account_Commodity_Id = ciac.Cu_Invoice_Account_Commodity_Id
            LEFT OUTER JOIN dbo.Code Source_Cd
                ON Source_Cd.Code_Id = acirt.Determinant_Source_Cd
        WHERE
            cism.CU_INVOICE_ID = @cu_invoice_id
            AND (   @Account_Id IS NULL
                    OR  cism.Account_ID = @Account_Id)
            AND e.ENTITY_DESCRIPTION = 'Account Type'
            AND (   @Commodity_Id IS NULL
                    OR  com.Commodity_Id = @Commodity_Id)
            AND (   e.ENTITY_NAME = 'Utility'
                    OR  (   e.ENTITY_NAME = 'Supplier'
                            AND (   cism.Begin_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                                  AND     cha.Supplier_Account_End_Dt
                                    OR  cism.End_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                                    AND     cha.Supplier_Account_End_Dt
                                    OR  cha.Supplier_Account_begin_Dt BETWEEN cism.Begin_Dt
                                                                      AND     cism.End_Dt
                                    OR  cha.Supplier_Account_End_Dt BETWEEN cism.Begin_Dt
                                                                    AND     cism.End_Dt)))
        GROUP BY
            cism.Account_ID
            , cha.Account_Number
            , com.Commodity_Id
            , com.Commodity_Name
            , CASE WHEN e.ENTITY_NAME = 'Utility' THEN acirt.Recalc_Type
                  ELSE NULL
              END
            , CASE WHEN e.ENTITY_NAME = 'Utility' THEN acirt.Recalc_Type_Cd
                  ELSE NULL
              END
            , cha.Account_Group_ID
            , e.ENTITY_ID
            , e.ENTITY_NAME
            , CASE WHEN e.ENTITY_NAME = 'Utility' THEN NULL
                  ELSE acirt.Recalc_Type_Dsc
              END
            , CASE WHEN e.ENTITY_NAME = 'Utility' THEN NULL
                  ELSE acirt.Recalc_Type_Cd
              END
            , ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME
            , ch.Client_Id
            , ch.Client_Name
            , CASE WHEN ciacrl.Is_Locked IS NULL THEN 0
                  ELSE ciacrl.Is_Locked
              END
            , acirt.Determinant_Source_Cd
            , Source_Cd.Code_Value;

        SELECT
            Account_ID
            , Account_Number
            , Commodity_Id
            , Commodity_Name
            , Recalc_Type
            , Invoice_Recalc_Type_Cd
            , Account_Group_ID
            , Account_Type_Id
            , Account_Type
            , contract_recalc_type
            , contract_recalc_Cd
            , analyst
            , Client_Id
            , Client_Name
            , Is_Locked
            , Determinant_Source_Cd
            , Determinant_Source
        FROM
            #Account_Commodity_Recalc_Type;


        DROP TABLE #Account_Commodity_Recalc_Type;
        DROP TABLE #Account_List;
        DROP TABLE #All_Account_Type_Recalcs;

    END;










GO
GRANT EXECUTE ON  [dbo].[Account_Commodity_Recalc_Type_Sel_By_Invoice_Id_For_Multiple] TO [CBMSApplication]
GO
