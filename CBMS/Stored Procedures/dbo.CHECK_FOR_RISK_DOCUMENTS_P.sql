SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.CHECK_FOR_RISK_DOCUMENTS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@clientId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE PROCEDURE  dbo.CHECK_FOR_RISK_DOCUMENTS_P
@userId varchar(10),
@sessionId varchar(20),
@clientId int
AS
begin
set nocount on
select 	count( risk.cbms_image_id) 
from 	RM_RISK_PLAN_PROFILE risk
where 	(risk.document_type_id  =  (select entity_id 
						from entity 
						where entity_type = 501
				      		and entity_name = 'Miscellaneous')
	 or risk.document_level_type_id =  (select entity_id 
						from entity 
						where entity_type = 502
				      		and entity_name = 'Division')	

	 or risk.document_level_type_id =  (select entity_id 
						from entity 

						where entity_type = 502
				      		and entity_name = 'Site')	
	)
	

	and risk.client_id = @clientId

end
GO
GRANT EXECUTE ON  [dbo].[CHECK_FOR_RISK_DOCUMENTS_P] TO [CBMSApplication]
GO
