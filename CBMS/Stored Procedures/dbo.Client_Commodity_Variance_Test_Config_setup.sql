SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Client_Commodity_Variance_Test_Config_Ins

DESCRIPTION:



INPUT PARAMETERS:
	Name									DataType		Default	Description
-------------------------------------------------------------------------------
	@Client_Commodity_Id							INT    
	@track_level							INT  
	@commodity_service_cd					INT
	@User_Info_Id							INT  
     	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
--------------------------------------------------------------------------------
@Client_Commodity_Id INT OUT

USAGE EXAMPLES:
--------------------------------------------------------------------------------





AUTHOR INITIALS:
	RKV			Ravi Kumar Vegesna
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
 RKV      		01/19/2010	MAINT- 8377 , Created 
******/

CREATE PROCEDURE [dbo].[Client_Commodity_Variance_Test_Config_setup]
     (
         @Client_Commodity_Id INT
         , @track_level INT
         , @commodity_service_cd INT
         , @User_Info_Id INT
     )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Variance_Start_Date DATE
            , @Hier_Level VARCHAR(25)
            , @commodity_service VARCHAR(25);

        SELECT
            @Variance_Start_Date = ap.App_Config_Value
        FROM
            dbo.App_Config ap
        WHERE
            ap.App_Config_Cd = 'Variance_Start_Date';

        SELECT
            @Hier_Level = c.Code_Value
        FROM
            dbo.Code AS c
        WHERE
            c.Code_Id = @track_level;

        SELECT
            @commodity_service = c.Code_Value
        FROM
            dbo.Code AS c
        WHERE
            c.Code_Id = @commodity_service_cd;


        INSERT INTO dbo.Client_Commodity_Variance_Test_Config
             (
                 Client_Commodity_Id
                 , Start_Dt
                 , End_Dt
                 , Variance_Test_Type_Cd
                 , Created_Ts
                 , Created_User_Id
                 , Updated_User_Id
                 , Last_Change_Ts
             )
        SELECT
            @Client_Commodity_Id
            , @Variance_Start_Date
            , NULL
            , vt.Code_Id
            , GETDATE()
            , @User_Info_Id
            , @User_Info_Id
            , GETDATE()
        FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code vt
                ON vt.Codeset_Id = cs.Codeset_Id
                   AND  vt.Code_Value = 'Full Variance Testing'
        WHERE
            @Hier_Level = 'Account'
            AND @commodity_service = 'Invoice'
            AND cs.Codeset_Name = 'VarianceTestType';


    END;

GO
GRANT EXECUTE ON  [dbo].[Client_Commodity_Variance_Test_Config_setup] TO [CBMSApplication]
GO
