SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.Account_DMO_Config_Del
           
DESCRIPTION:             
			To delete DMO configurations
			
INPUT PARAMETERS:            
	Name						DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Account_DMO_Config_Id		INT			NULL
    @Client_Hier_DMO_Config_Id	INT			NULL
    @Account_Id					INT
    @User_Info_Id				INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT TOP 10 * FROM dbo.Account_DMO_Config
	SELECT TOP 10 * FROM dbo.Client_Hier_DMO_Config
            
	BEGIN TRANSACTION
		DECLARE @Account_DMO_Config_Id INT
		SELECT * FROM  dbo.Account_DMO_Config 
			WHERE Account_Id = 1 AND Commodity_Id = 290 AND DMO_Start_Dt= '2017-1-1' AND DMO_End_Dt='2017-12-31'
		EXEC dbo.Account_DMO_Config_Ins_Upd 1,290,'2018-1-1','2018-12-31',NULL,NULL,16
		SELECT * FROM  dbo.Account_DMO_Config 
			WHERE Account_Id = 1 AND Commodity_Id = 290 AND DMO_Start_Dt= '2018-1-1' AND DMO_End_Dt='2018-12-31'
		SELECT @Account_DMO_Config_Id = Account_DMO_Config_Id FROM  dbo.Account_DMO_Config 
			WHERE Account_Id = 1 AND Commodity_Id = 290 AND DMO_Start_Dt= '2018-1-1' AND DMO_End_Dt='2018-12-31'
		EXEC dbo.Account_DMO_Config_Del @Account_DMO_Config_Id,NULL,1,16
		SELECT * FROM  dbo.Account_DMO_Config 
			WHERE Account_Id = 1 AND Commodity_Id = 290 AND DMO_Start_Dt= '2018-1-1' AND DMO_End_Dt='2018-12-31'
	ROLLBACK TRANSACTION
	
	BEGIN TRANSACTION
		SELECT * FROM  dbo.Account_Not_Applicable_DMO_Config WHERE Account_Id = 22452
		EXEC dbo.Account_DMO_Config_Del NULL,50,22452,16
		SELECT * FROM  dbo.Account_Not_Applicable_DMO_Config WHERE Account_Id = 22452
	ROLLBACK TRANSACTION
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-01-25	Contract placeholder - CP-4 Created
******/

CREATE PROCEDURE [dbo].[Account_DMO_Config_Del]
      ( 
       @Account_DMO_Config_Id INT = NULL
      ,@Client_Hier_DMO_Config_Id INT = NULL
      ,@Account_Id INT
      ,@User_Info_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Client_Hier_Id INT
      
      SELECT
            @Client_Hier_Id = Client_Hier_Id
      FROM
            Core.Client_Hier_Account
      WHERE
            Account_Id = @Account_Id
      
      DELETE FROM
            dbo.Account_DMO_Config
      WHERE
            @Account_DMO_Config_Id IS NOT NULL
            AND Account_DMO_Config_Id = @Account_DMO_Config_Id
            
      INSERT      INTO dbo.Account_Not_Applicable_DMO_Config
                  ( 
                   Client_Hier_Id
                  ,Account_Id
                  ,Client_Hier_DMO_Config_Id
                  ,Created_User_Id
                  ,Created_Ts )
                  SELECT
                        @Client_Hier_Id
                       ,@Account_Id
                       ,Client_Hier_DMO_Config_Id
                       ,@User_Info_Id
                       ,GETDATE()
                  FROM
                        dbo.Client_Hier_DMO_Config
                  WHERE
                        @Client_Hier_DMO_Config_Id IS NOT NULL
                        AND Client_Hier_DMO_Config_Id = @Client_Hier_DMO_Config_Id
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Account_Not_Applicable_DMO_Config
                                         WHERE
                                          Account_Id = @Account_Id
                                          AND Client_Hier_Id = @Client_Hier_Id
                                          AND Client_Hier_DMO_Config_Id = @Client_Hier_DMO_Config_Id )

END;
;



;
GO
GRANT EXECUTE ON  [dbo].[Account_DMO_Config_Del] TO [CBMSApplication]
GO
