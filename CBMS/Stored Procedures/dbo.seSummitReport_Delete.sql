SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.seSummitReport_Delete

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@ReportId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE      Procedure [dbo].[seSummitReport_Delete]

	(
		@ReportId int
	)
As
BEGIN

	SET NOCOUNT ON

--	  DECLARE @rpl_active varchar(200)
--	   select @rpl_active = entity_name from entity where entity_type = 50000

	  DECLARE @rpl_sv_active varchar(200)
	   select @rpl_sv_active = entity_name from entity where entity_type = 50001




	 delete 
		 from seSummitReport 
		where ReportId = @ReportId

/*
	 delete 
		 from [01SUMMIT-DB2].sso_prod_1.dbo.seSummitReport 
		where ReportId = @ReportId

	 delete 
		 from [01SUMMIT-DB2].sso_prod_2.dbo.seSummitReport 
		where ReportId = @ReportId
*/

END
GO
GRANT EXECUTE ON  [dbo].[seSummitReport_Delete] TO [CBMSApplication]
GO
