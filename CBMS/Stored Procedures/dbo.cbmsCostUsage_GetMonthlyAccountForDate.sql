
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
NAME:  
 cbms_prod.dbo.cbmsContract_GetSiteCountForAccountMonth  
 
 DESCRIPTION:   
 
 INPUT PARAMETERS:  
 Name		 DataType  Default Description  
------------------------------------------------------------  
 @Account_Id    INT                     
 @Service_Month DATETIME  
               
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 USAGE EXAMPLES:  
------------------------------------------------------------  

	EXEC dbo.cbmsCostUsage_GetMonthlyAccountForDate 3, 12,25,'9/1/2005',1011, 2492

AUTHOR INITIALS:  
 Initials	  Name  
------------------------------------------------------------  
 AP		  Athmaram Pabbathi
 
 MODIFICATIONS   
 Initials	  Date	    Modification  
------------------------------------------------------------  
 AP		  2012-04-11  Addnl Data Changes
					   -- Replaced Cost_Usage with Cost_Usage_Account_Dtl
					   -- Removed @MyAccountId unused parameter
					   -- Removed Cost_Usage_Id from result set; 
					   -- Replaced dbo.CLIENT_CURRENCY_GROUP_MAP with Client_Hier for getting Currency_Group_Id
******/ 

CREATE  PROCEDURE [dbo].[cbmsCostUsage_GetMonthlyAccountForDate]
      ( 
       @Currency_Unit_Id INT
      ,@EL_Unit_of_Measure_Type_Id INT
      ,@NG_Unit_of_Measure_Type_Id INT
      ,@Report_Date DATETIME
      ,@Client_Id INT
      ,@Account_Id INT )
AS 
BEGIN
      SET NOCOUNT ON

      DECLARE
            @Client_Currency_Group_Id INT
           ,@NG_Commodity_Id INT
           ,@EL_Commodity_Id INT

      DECLARE @Cost_Usage_Bucket_Id TABLE
            ( 
             Bucket_Master_Id INT PRIMARY KEY CLUSTERED
            ,Bucket_Name VARCHAR(200)
            ,Bucket_Type VARCHAR(25) )

      SELECT
            @Client_Currency_Group_Id = ch.Client_Currency_Group_Id
      FROM
            Core.Client_Hier ch
      WHERE
            ch.Client_Id = @Client_Id
            AND ch.Sitegroup_Id = 0
            AND ch.Site_Id = 0

      SELECT
            @EL_Commodity_Id = max(case WHEN com.Commodity_Name = 'Electric Power' THEN com.Commodity_Id
                                   END)
           ,@NG_COmmodity_Id = max(case WHEN com.Commodity_Name = 'Natural Gas' THEN com.Commodity_Id
                                   END)
      FROM
            dbo.Commodity com
      WHERE
            com.Commodity_Name IN ( 'Electric Power', 'Natural Gas' ) ;

      INSERT      INTO @Cost_Usage_Bucket_Id
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Bucket_Type )
                  EXEC dbo.Cost_usage_Bucket_Sel_By_Commodity 
                        @Commodity_id = @NG_Commodity_Id 

      INSERT      INTO @Cost_Usage_Bucket_Id
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Bucket_Type )
                  EXEC dbo.Cost_usage_Bucket_Sel_By_Commodity 
                        @Commodity_id = @EL_Commodity_Id 

      SELECT
            sum(case WHEN bm.Commodity_Id = @EL_Commodity_Id
                          AND bkt.Bucket_Type = 'Total Usage' THEN cuad.Bucket_Value * uc.Conversion_Factor
                END) AS EL_Usage
           ,sum(case WHEN bm.Commodity_Id = @EL_Commodity_Id
                          AND bkt.Bucket_Type = 'Total Cost' THEN cuad.Bucket_Value * cur_conv.Conversion_Factor
                END) AS EL_Cost
           ,sum(case WHEN bm.Commodity_Id = @NG_Commodity_Id
                          AND bkt.Bucket_Type = 'Total Usage' THEN cuad.Bucket_Value * uc.Conversion_Factor
                END) AS NG_Usage
           ,sum(case WHEN bm.Commodity_Id = @NG_Commodity_Id
                          AND bkt.Bucket_Type = 'Total Cost' THEN cuad.Bucket_Value * uc.Conversion_Factor
                END) AS NG_Cost
           ,cuad.Service_Month
      FROM
            dbo.Cost_Usage_Account_Dtl cuad
            INNER JOIN @Cost_Usage_Bucket_Id bkt
                  ON bkt.Bucket_Master_Id = cuad.Bucket_Master_Id
            INNER JOIN dbo.Bucket_Master bm
                  ON bm.Bucket_Master_Id = bkt.Bucket_Master_Id
            LEFT OUTER JOIN dbo.Currency_Unit_Conversion cur_conv
                  ON cur_conv.Base_Unit_Id = cuad.Currency_Unit_Id
                     AND cur_conv.Conversion_Date = cuad.Service_Month
                     AND cur_conv.Currency_Group_Id = @Client_Currency_Group_Id
                     AND cur_conv.Converted_Unit_Id = @Currency_Unit_Id
            LEFT OUTER JOIN dbo.Consumption_Unit_Conversion uc
                  ON uc.Base_Unit_Id = cuad.UOM_Type_Id
                     AND ( ( bm.Commodity_Id = @EL_Commodity_Id
                             AND uc.Converted_Unit_Id = @EL_Unit_of_Measure_Type_Id )
                           OR ( bm.Commodity_Id = @NG_Commodity_Id
                                AND uc.Converted_Unit_Id = @NG_Unit_of_Measure_Type_Id ) )
      WHERE
            cuad.Account_Id = @Account_Id
            AND cuad.Service_Month = @Report_Date
      GROUP BY
            cuad.Service_Month


END
;
GO

GRANT EXECUTE ON  [dbo].[cbmsCostUsage_GetMonthlyAccountForDate] TO [CBMSApplication]
GO
