
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.GET_SITES_UNDER_GROUP_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------         	
	@userId			VARCHAR
    @sessionId		VARCHAR
    @groupId		INT
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES
------------------------------------------------------------

EXEC dbo.GET_SITES_UNDER_GROUP_P -1,-1,119
EXEC dbo.GET_SITES_UNDER_GROUP_P -1,-1,225

	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	NR			Narayaan Reddy
	
	
MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	NR			2014-10-29	Added header.
							Replaced base tables and view(VWSITENAME) with client hier tables
								
******/
CREATE PROCEDURE [dbo].[GET_SITES_UNDER_GROUP_P]
      @userId VARCHAR
     ,@sessionId VARCHAR
     ,@groupId INT
AS 
BEGIN
      SET NOCOUNT ON
      
       
      SELECT
            ch.SITE_ID
           ,rtrim(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' AS SITE_NAME
      FROM
            core.Client_Hier ch
            INNER JOIN dbo.RM_ONBOARD_HEDGE_SETUP hedge
                  ON ch.Site_Id = hedge.SITE_ID
            INNER JOIN dbo.RM_GROUP_SITE_MAP groupMap
                  ON hedge.SITE_ID = groupMap.SITE_ID
      WHERE
            groupMap.RM_GROUP_ID = @groupId
            AND hedge.INCLUDE_IN_REPORTS = 1
      ORDER BY
            ch.Site_name
            
     
END



 
;
GO

GRANT EXECUTE ON  [dbo].[GET_SITES_UNDER_GROUP_P] TO [CBMSApplication]
GO
