SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******************************************************************************************************
NAME : dbo.[Site_Del_For_Client_Data_Transfer]

DESCRIPTION: This procedure used to delete a Site

 INPUT PARAMETERS:      

 Name			DataType		Default			Description      
--------------------------------------------------------------------        

 @Message		XML   

 OUTPUT PARAMETERS:      

 Name   DataType  Default Description      
--------------------------------------------------------------------      

  USAGE EXAMPLES:
--------------------------------------------------------------------      
	BEGIN TRAN
		DECLARE @Client_Hier_Id int
		EXEC [Site_Del_For_Client_Data_Transfer] 
			@Client_Hier_Id = 761621
	ROLLBACK TRAN

AUTHOR INITIALS:      

 Initials		Name      
-------------------------------------------------------------------       
 MSV			Muhamed Shahid V

 MODIFICATIONS  

 Initials		Date			Modification  
--------------------------------------------------------------------  
 MSV			05 Aug 2019		Created 	

*****************************************************************************************************/
CREATE PROCEDURE [dbo].[Site_Del_For_Client_Data_Transfer]
	(
		@Client_Hier_Id INT
	)
AS
BEGIN
	
    SET NOCOUNT ON

    DECLARE
        @Site_Id INT
        ,@User_Info_Id INT
		
    SELECT
        @User_Info_Id = ui.User_Info_Id
    FROM
        dbo.USER_INFO ui
    WHERE
        ui.username = 'conversion'
	
	SELECT	@Site_Id = ch.Site_Id
	FROM Core.Client_Hier ch 
	WHERE ch.Client_Hier_Id = @Client_Hier_Id   

	BEGIN TRY
		BEGIN TRAN;

		EXEC Delete_Site_From_Roles 
			@Site_Id = @Site_Id

		EXEC Site_History_Del_By_Site_Id 
			@Site_Id = @Site_Id,
			@User_Info_Id = @User_Info_Id

		COMMIT TRAN
	END TRY
	BEGIN CATCH

		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRAN;
		END;

		EXEC dbo.usp_RethrowError;
	END CATCH;
END;

GO
GRANT EXECUTE ON  [dbo].[Site_Del_For_Client_Data_Transfer] TO [CBMSApplication]
GO
