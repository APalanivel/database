SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.User_Push_Notification_Bulk_Ins            
                        
 DESCRIPTION:                        
			To Bulk insert  the User_Push_Notification table.                        
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
 @Notification_Type_Cd_List  VARCHAR(MAX)               
 @User_Info_Id_List          VARCHAR(MAX)       
 @User_Info_Id		           INT                      
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 BEGIN TRAN
 
 SELECT ui.USER_INFO_ID,Notification_Type_Cd,c.Code_Value FROM dbo.USER_INFO ui INNER JOIN dbo.User_Push_Notification upn 
		ON ui.USER_INFO_ID = upn.User_Info_Id INNER JOIN dbo.Code c ON c.Code_Id=upn.Notification_Type_Cd
		WHERE ui.USER_INFO_ID IN (61215,61214)         
		      
 EXEC [dbo].[User_Push_Notification_Bulk_Ins] 
      @User_Info_Id_List = '61215,61214'
     ,@Notification_Type_Cd_List = '102128,102129,102130'
     ,@User_Info_Id = 49
     
  SELECT ui.USER_INFO_ID,Notification_Type_Cd,c.Code_Value FROM dbo.USER_INFO ui INNER JOIN dbo.User_Push_Notification upn 
		ON ui.USER_INFO_ID = upn.User_Info_Id INNER JOIN dbo.Code c ON c.Code_Id=upn.Notification_Type_Cd
		WHERE ui.USER_INFO_ID IN (61215,61214)   

           
 ROLLBACK TRAN	      


                            
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 NR                    Narayana Reddy         
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 NR                    2016-08-12       Created For MAINT-4014(4237).               
                       
******/    
 
CREATE PROCEDURE [dbo].[User_Push_Notification_Bulk_Ins]
      ( 
       @User_Info_Id_List VARCHAR(MAX)
      ,@Notification_Type_Cd_List VARCHAR(MAX) = NULL
      ,@User_Info_Id INT )
AS 
BEGIN                
      SET NOCOUNT ON;   
      
      
      DECLARE @Notification_Mode_Cd INT
      
      
      SELECT
            @Notification_Mode_Cd = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            c.Code_Value = 'Email'
            AND cs.Codeset_Name = 'Notification Mode'
            AND cs.Std_Column_Name = 'Notification_Mode_Cd'  
            
                
                    
      DECLARE @Notification_Type_Cd_Tbl TABLE
            ( 
             Notification_Type_Cd INT PRIMARY KEY CLUSTERED ) 

      DECLARE @User_Info_Id_Tbl TABLE
            ( 
             User_Info_Id INT PRIMARY KEY CLUSTERED )                                 
                  
                  
      INSERT      INTO @Notification_Type_Cd_Tbl
                  ( 
                   Notification_Type_Cd )
                  SELECT
                        ufn.Segments
                  FROM
                        dbo.ufn_split(@Notification_Type_Cd_List, ',') ufn      


      INSERT      INTO @User_Info_Id_Tbl
                  ( 
                   User_Info_Id )
                  SELECT
                        ufn.Segments
                  FROM
                        dbo.ufn_split(@User_Info_Id_List, ',') ufn                                     
  
      BEGIN TRY                        
            BEGIN TRANSACTION     
              
            INSERT      INTO dbo.User_Push_Notification
                        ( 
                         User_Info_Id
                        ,Notification_Type_Cd
                        ,Notification_Mode_Cd
                        ,Created_User_Id
                        ,Created_Ts )
                        SELECT
                              ui.User_Info_Id
                             ,ntc.Notification_Type_Cd
                             ,@Notification_Mode_Cd
                             ,@User_Info_Id
                             ,GETDATE()
                        FROM
                              @Notification_Type_Cd_Tbl ntc
                              CROSS APPLY @User_Info_Id_Tbl ui
                        WHERE
                              NOT EXISTS ( SELECT
                                                1
                                           FROM
                                                dbo.User_Push_Notification upn
                                           WHERE
                                                upn.Notification_Type_Cd = ntc.Notification_Type_Cd
                                                AND upn.User_Info_Id = ui.User_Info_Id )  
                 
                       
            COMMIT TRANSACTION                                 
                                   
                                   
      END TRY                    
      BEGIN CATCH                    
            IF @@TRANCOUNT > 0 
                  BEGIN    
                        ROLLBACK TRANSACTION    
                  END                   
            EXEC dbo.usp_RethrowError                    
      END CATCH  
                       
END;


;
GO
GRANT EXECUTE ON  [dbo].[User_Push_Notification_Bulk_Ins] TO [CBMSApplication]
GO
