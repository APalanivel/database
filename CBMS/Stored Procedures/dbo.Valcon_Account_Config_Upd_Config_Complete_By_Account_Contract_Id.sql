SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********    
NAME: dbo.Valcon_Account_Config_Upd_Config_Complete_By_Account_Contract_Id    
  
DESCRIPTION:       
  
   
INPUT PARAMETERS:      
Name        DataType  Default  Description      
----------------------------------------------------------------------------      
@Account_Id       INT  
@Contract_Id      INT  
@User_Info_Id      INT  
  
OUTPUT PARAMETERS:      
Name     DataType  Default  Description      
----------------------------------------------------------------------------      
  
USAGE EXAMPLES:      
----------------------------------------------------------------------------   
  
  
   
BEGIN TRAN  
  
SELECT  * FROM  dbo.Valcon_Account_Config where  Account_Id = 1322445 AND Contract_Id = 143740
  
  
EXEC dbo.Valcon_Account_Config_Upd_Config_Complete_By_Account_Contract_Id
    @Account_Id = 1322445
    , @Contract_Id = 143740
    , @User_Info_Id = 49 
  
SELECT  * FROM  dbo.Valcon_Account_Config where  Account_Id = 1322445 AND Contract_Id = 143740
  
ROLLBACK  
  
  
AUTHOR INITIALS:      
Initials Name      
----------------------------------------------------------------------------      
NR   Narayana Reddy  
   
MODIFICATIONS       
Initials Date   Modification      
-----------------------------------------------------------------------------      
NR   2019-09-03  Add Contract -  Created.  
  
******/


CREATE PROCEDURE [dbo].[Valcon_Account_Config_Upd_Config_Complete_By_Account_Contract_Id]
    (
        @Account_Id INT = NULL
        , @Contract_Id INT
        , @User_Info_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        CREATE TABLE #Account_List
             (
                 Id INT IDENTITY(1, 1)
                 , Account_Id INT
             );

        DECLARE
            @Supplier_Account_Begin_Dt DATE
            , @Supplier_Account_End_Dt DATE
            , @Commodity_Id INT
            , @Recacl_Is_Missing BIT = 0
            , @Counter INT = 0
            , @Total_Rows INT
            , @Account_Id_Out INT;

        SELECT
            @Commodity_Id = c.COMMODITY_TYPE_ID
        FROM
            dbo.CONTRACT c
        WHERE
            c.CONTRACT_ID = @Contract_Id;

        INSERT INTO #Account_List
             (
                 Account_Id
             )
        SELECT
            cha.Account_Id
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Account_Type = 'Supplier'
            AND cha.Supplier_Contract_ID = @Contract_Id
            AND cha.Supplier_Contract_ID <> -1
            AND cha.Account_Number <> 'Not Yet Assigned'
            AND (   @Account_Id IS NULL
                    OR  cha.Account_Id = @Account_Id)
        GROUP BY
            cha.Account_Id;



        SELECT  @Total_Rows = MAX(al.Id)FROM    #Account_List al;

        WHILE (@Counter <= @Total_Rows)
            BEGIN

                SET @Account_Id_Out = NULL;
                SET @Supplier_Account_Begin_Dt = NULL;
                SET @Supplier_Account_End_Dt = NULL;
                SET @Recacl_Is_Missing = 0;


                SELECT
                    @Account_Id_Out = al.Account_Id
                FROM
                    #Account_List al
                WHERE
                    al.Id = @Counter;


                SELECT
                    @Supplier_Account_Begin_Dt = sac.Supplier_Account_Begin_Dt
                    , @Supplier_Account_End_Dt = sac.Supplier_Account_End_Dt
                FROM
                    dbo.Supplier_Account_Config sac
                WHERE
                    sac.Account_Id = @Account_Id_Out
                    AND sac.Contract_Id = @Contract_Id
                    AND sac.Contract_Id <> -1;





                SELECT
                    @Recacl_Is_Missing = 1
                FROM
                (   SELECT  TOP 1
                            Date_D
                    FROM
                        meta.Date_Dim_Expd dd1
                    WHERE
                        dd1.Date_D BETWEEN @Supplier_Account_Begin_Dt
                                   AND     @Supplier_Account_End_Dt
                        AND NOT EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.Account_Commodity_Invoice_Recalc_Type acirt
                                                CROSS JOIN meta.Date_Dim_Expd dd
                                           WHERE
                                                dd.Date_D BETWEEN acirt.Start_Dt
                                                          AND     ISNULL(acirt.End_Dt, '9999-12-31')
                                                AND acirt.Account_Id = @Account_Id_Out
                                                AND acirt.Commodity_ID = @Commodity_Id
                                                AND dd1.Date_D = dd.Date_D)) X;




                UPDATE
                    vac
                SET
                    vac.Is_Config_Complete = 0
                    , vac.Last_Change_Ts = GETDATE()
                    , vac.Updated_User_Id = @User_Info_Id
                FROM
                    dbo.Valcon_Account_Config vac
                WHERE
                    vac.Account_Id = @Account_Id_Out
                    AND vac.Contract_Id = @Contract_Id
                    AND vac.Is_Config_Complete = 1
                    AND @Recacl_Is_Missing = 1;


                SET @Counter = @Counter + 1;


            END;


    END;






GO
GRANT EXECUTE ON  [dbo].[Valcon_Account_Config_Upd_Config_Complete_By_Account_Contract_Id] TO [CBMSApplication]
GO
