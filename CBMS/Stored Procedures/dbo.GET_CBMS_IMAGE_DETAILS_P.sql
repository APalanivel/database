
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*******          
NAME:            
           
 dbo.GET_CBMS_IMAGE_DETAILS_P          
           
 DESCRIPTION:             
           
 Retrieves CBMS Image Details.          
           
 INPUT PARAMETERS:            
 Name				DataType	Default		Description            
------------------------------------------------------------              
  @CBMS_IMAGE_ID	int          
           
 OUTPUT PARAMETERS:            
 Name				DataType	Default		Description            
------------------------------------------------------------            
          
  USAGE EXAMPLES:            
------------------------------------------------------------            
          
 EXEC GET_CBMS_IMAGE_DETAILS_P 6506455
   
          
AUTHOR INITIALS:            
 Initials Name            
------------------------------------------------------------            
 GP   Garrett Page    12/15/2009          
 LD   Lalith Denduluri         
 AC   Ajay Chejarla           
 JR   Jishnu Radhakrishnan    
          
 MODIFICATIONS             
 Initials Date  Modification            
------------------------------------------------------------                   
 DMR  09/10/2010 Modified for Quoted_Identifier          
 LD   11/08/2012 added 3 additional columns in select list          
 AC   8/20/2014  added storage type selection        
 JR   05/03/2016 Updated path separator based on Storage_Type_Cd
 KH   08/30/2016 added imagetype to the select   
******/          
CREATE PROCEDURE [dbo].[GET_CBMS_IMAGE_DETAILS_P] ( @cbmsImageID INT )
AS
BEGIN
     
      SET NOCOUNT ON;         

      DECLARE @FileSystemType INT;    

      SELECT
            @FileSystemType = c.Code_Id
      FROM
            Code c
            JOIN Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            c.Code_Value = 'File System storage'
            AND cs.Codeset_Name = 'Image Storage Types';    

      SELECT
            img.CBMS_DOC_ID ImageName
           ,img.CONTENT_TYPE ImageContent
           ,img.CBMS_IMAGE_SIZE ImageSize
           ,CASE WHEN imgPath.Storage_Type_Cd = @FileSystemType THEN img.CBMS_Image_Directory + '\' + CBMS_Image_FileName
                 ELSE img.CBMS_Image_Directory + '/' + CBMS_Image_FileName
            END AS ImagePath
           ,imgPath.Image_Drive AS ImageRepository
           ,img.CBMS_DOC_ID
           ,img.Cbms_Image_Path AS HttpImagePath
           ,cd.Code_Dsc AS AuthenticationType
           ,imgPath.UserName AS UserName
           ,imgPath.Password AS Password
           ,c.Code_Value AS StorageType
           ,img.CBMS_IMAGE_TYPE_ID AS ImageTypeId
           ,e.ENTITY_NAME AS ImageType
      FROM
            dbo.cbms_image img
            INNER JOIN dbo.CBMS_Image_Location AS imgPath
                  ON imgPath.CBMS_Image_Location_Id = img.CBMS_Image_Location_Id
            INNER JOIN Code c
                  ON imgPath.Storage_Type_Cd = c.Code_Id
            LEFT JOIN Code cd
                  ON cd.Code_Id = imgPath.Authentication_Type_Cd
            LEFT JOIN ENTITY e
                  ON e.ENTITY_ID = img.CBMS_IMAGE_TYPE_ID
      WHERE
            img.CBMS_IMAGE_ID = @cbmsImageID;          
          
END;

;
GO




GRANT EXECUTE ON  [dbo].[GET_CBMS_IMAGE_DETAILS_P] TO [CBMSApplication]
GO
