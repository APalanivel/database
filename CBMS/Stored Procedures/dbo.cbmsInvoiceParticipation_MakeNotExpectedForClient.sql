SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[cbmsInvoiceParticipation_MakeNotExpectedForClient]
   (
     @MyAccountId int
   , @client_id int
	
   )
AS 
   BEGIN

      update   invoice_participation
      set      is_expected = 0
      where    site_id in (
               select distinct
                        s.site_id
               from     client cl
                        join division d
                           on d.client_id = cl.client_id
                        join site s
                           on s.division_id = d.division_id
               where    cl.client_id = @client_id )
               and is_received = 0

   END





GO
GRANT EXECUTE ON  [dbo].[cbmsInvoiceParticipation_MakeNotExpectedForClient] TO [CBMSApplication]
GO
