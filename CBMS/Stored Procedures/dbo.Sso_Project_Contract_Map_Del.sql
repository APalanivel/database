SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Sso_Project_Contract_Map_Del]  

DESCRIPTION: It Deletes Sso Project Contract Map for Given Contract Id and Sso Project Id.
      
INPUT PARAMETERS:          
	NAME				DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Sso_Project_Id		INT
	@Contract_Id		INT
	

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
	EXEC Sso_Project_Contract_Map_Del 1640,24458
  Rollback Tran

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			17-JUN-10	CREATED

*/

CREATE PROCEDURE dbo.Sso_Project_Contract_Map_Del
    (
       @Sso_Project_Id INT
      ,@Contract_Id	   INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.SSO_PROJECT_CONTRACT_MAP
	WHERE
		Sso_Project_Id = @Sso_Project_Id
		AND Contract_Id = @Contract_Id
END
GO
GRANT EXECUTE ON  [dbo].[Sso_Project_Contract_Map_Del] TO [CBMSApplication]
GO
