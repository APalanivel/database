SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[dbo.sp_MSins_dboRA_User_Monthly_Login_Count]
    @c1 int,
    @c2 date,
    @c3 int
as
begin  
	insert into [dbo].[RA_User_Monthly_Login_Count](
		[USER_INFO_ID],
		[Login_Month],
		[Login_Cnt]
	) values (
    @c1,
    @c2,
    @c3	) 
end  
GO
