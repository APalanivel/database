SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_MOVE_DT_TO_OTHER_USER_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@deal_ticket_id	int       	          	
	@with_whom     	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--select * from SR_DEAL_TICKET_DETAILS where SR_DEAL_TICKET_DETAILS_ID = 204
--exec dbo.SR_SAD_GET_DT_SEARCH_QUEUE_DETAILS_P 3,0,0,0,0,0,0




CREATE  PROCEDURE dbo.SR_SAD_MOVE_DT_TO_OTHER_USER_P

@deal_ticket_id int,
@with_whom int

AS
set nocount on


update SR_DEAL_TICKET_DETAILS set WITH_WHOM = @with_whom where SR_DEAL_TICKET_ID = @deal_ticket_id
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_MOVE_DT_TO_OTHER_USER_P] TO [CBMSApplication]
GO
