SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******         
                     
 NAME: dbo.CU_Invoice_CU_Data_Correction_Batch_Dtl_Upd                      
                        
 DESCRIPTION:                        
        To update  the batch details status.                        
                        
 INPUT PARAMETERS:        
                       
 Name											DataType           Default       Description        
--------------------------------------------------------------------------------------- 
 @CU_Invoice_CU_Data_Correction_Batch_Dtll_Id	INT
 @Status_Cd										INT              
                        
 OUTPUT PARAMETERS:             
                        
 Name										DataType           Default       Description        
--------------------------------------------------------------------------------------- 
                        
 USAGE EXAMPLES:        
--------------------------------------------------------------------------------------- 
   BEGIN TRAN    
            
   EXEC dbo.CU_Invoice_CU_Data_Correction_Batch_Dtl_Upd 
      @CU_Invoice_CU_Data_Correction_Batch_Dtl_Id = 1
     ,@Status_Cd = 100
    
                  
   ROLLBACK TRAN  
                     
 AUTHOR INITIALS:        
       
 Initials               Name        
--------------------------------------------------------------------------------------- 
 NR                     Narayana Reddy
                         
 MODIFICATIONS:      
       
 Initials               Date             Modification      
--------------------------------------------------------------------------------------- 
 NR                     2017-03-09      Created for Contract Placeholder.                     
                       
******/    
CREATE	 PROCEDURE [dbo].[CU_Invoice_CU_Data_Correction_Batch_Dtl_Upd]
      ( 
       @CU_Invoice_CU_Data_Correction_Batch_Dtl_Id INT
      ,@Status_Cd INT
      ,@Error_Msg VARCHAR(MAX)= NULL)
AS 
BEGIN 
                       
      SET NOCOUNT ON;  
      
      UPDATE
            cicdcbd
      SET   
            Status_Cd = @Status_Cd
           ,Last_Change_Ts = GETDATE()
           ,Error_Msg=@Error_Msg
      FROM
            dbo.CU_Invoice_CU_Data_Correction_Batch_Dtl cicdcbd
      WHERE
            cicdcbd.CU_Invoice_CU_Data_Correction_Batch_Dtl_Id = @CU_Invoice_CU_Data_Correction_Batch_Dtl_Id
      
     
      

END




;
GO
GRANT EXECUTE ON  [dbo].[CU_Invoice_CU_Data_Correction_Batch_Dtl_Upd] TO [CBMSApplication]
GO
