SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
  /*********                             
NAME:                            
   Workflow.Get_User_Last_Visited_Filter                          
                        
DESCRIPTION:                            
  This procedure used to get the data in Workflow.Get_User_Last_Visited_Filter                          
                        
INPUT PARAMETERS:                            
                         
Name         DataType  Default Description                            
-----------------------------------------------------------------------------                         
    @User_Info_Id INT,      
    @Session_Id INT                         
                                 
OUTPUT PARAMETERS:                            
Name    DataType  Default Description                            
------------------------------------------------------------                            
                            
USAGE EXAMPLES:                            
------------------------------------------------------------                            
EXEC Workflow.Get_User_Last_Visited_Filter                        
                   
AUTHOR INITIALS:                            
Initials  Name                            
------------------------------------------------------------                            
SM    Sandeep Manda                         
                        
                        
MODIFICATIONS                             
 Initials Date   Modification                            
------------------------------------------------------------                            
SM   2019-09-05  SP Created                        
                        
******/      
      
CREATE PROCEDURE [Workflow].[Get_User_Last_Visited_Filter]      
(      
    @User_Info_Id INT,      
    @Session_Id INT      
)      
AS      
BEGIN      
   
      SELECT User_Last_Visited_Filter_Id,
			User_Info_Id,      
            Workflow_Queue_Id,      
            Workflow_Queue_Saved_Filter_Name  
        FROM Workflow.User_Last_Visited_Filter 
		WHERE User_Info_Id=@User_Info_Id 
		AND Session_Id=@Session_Id

END;      

GO
GRANT EXECUTE ON  [Workflow].[Get_User_Last_Visited_Filter] TO [CBMSApplication]
GO
