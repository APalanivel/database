SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.[cbmsAppMenu_Save]


DESCRIPTION: 


INPUT PARAMETERS:    
      Name                  DataType          Default     Description    
------------------------------------------------------------------    
 @MyAccountId                 int
 @menu_id                     int               null
 @menu_profile_id             int
 @permission_info_id          int 
 @display_text                varchar(50)
 @menu_description            varchar(200)      null
 @target_server               varchar(10)
 @target_action               varchar(500)
 @menu_level                  int 
 @display_order               int 
 @parent_menu_id              int               null
 @app_module_id               int               null
	
        
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
--exec cbmsAppMenu_Save
--	 @MyAccountId = 1
--	, @menu_id = null
--	, @menu_profile_id = 1
--	, @permission_info_id =181
--	, @display_text = 'Test Data'
--	, @menu_description ='Test Data'
--	, @target_server = ''
--	, @target_action = ''
--	, @menu_level = 1
--	, @display_order = 2
--	, @parent_menu_id = null
--	, @app_module_id = null
--go


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates
 DMR		  09/10/2010 Modified for Quoted_Identifier
	  

******/
CREATE PROCEDURE [dbo].[cbmsAppMenu_Save]
	( @MyAccountId int
	, @menu_id int = null
	, @menu_profile_id int
	, @permission_info_id int 
	, @display_text varchar(50)
	, @menu_description varchar(200) = null
	, @target_server varchar(10)
	, @target_action varchar(500)
	, @menu_level int 
	, @display_order int 
	, @parent_menu_id int = null
	, @app_module_id int = null
	)
AS

BEGIN

	SET NOCOUNT ON

declare @ThisId int
set @ThisId = @menu_id

	if @menu_id is null
	begin

		insert into app_menu
			( app_menu_profile_id
			, permission_info_id
			, display_text
			, menu_description
			, target_server
			, target_action
			, menu_level
			, display_order
			, parent_menu_id
			, app_module_id
			)
		values
			( @menu_profile_id
			, @permission_info_id
			, @display_text
			, @menu_description
			, @target_server
			, @target_action
			, @menu_level
			, @display_order
			, @parent_menu_id
			, @app_module_id
			)
	
		set @ThisId = scope_identity()
	end
	else
	begin
	
	   update app_menu
	      set app_menu_profile_id = @menu_profile_id
		, permission_info_id = @permission_info_id
		, display_text = @display_text
		, menu_description = @menu_description
		, target_server = @target_server
		, target_action = @target_action
		, menu_level = @menu_level
		, display_order = @display_order
		, parent_menu_id = @parent_menu_id
		, app_module_id = @app_module_id
	    where app_menu_id = @menu_id
	end
	
--	set nocount off

	exec cbmsAppMenu_Get @MyAccountId, @ThisId
	
	
END
GO
GRANT EXECUTE ON  [dbo].[cbmsAppMenu_Save] TO [CBMSApplication]
GO
