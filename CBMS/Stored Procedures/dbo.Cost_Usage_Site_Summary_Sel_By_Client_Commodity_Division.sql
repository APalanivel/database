
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 dbo.Cost_Usage_Site_Summary_Sel_By_Client_Commodity_Division  
  
 DESCRIPTION:  
   
 INPUT PARAMETERS:    
 Name    DataType  Default Description    
------------------------------------------------------------    
 @Client_Id   INT                       
 @Commodity_Id  INT                       
 @Sitegroup_Id  INT        NULL  
 @Currency_Unit_Id INT  
 @Uom_Id   INT  
 @Begin_Dt   DATETIME  
 @End_Dt   DATETIME  
 @Site_Not_Managed INT        0  
 @Country_Id  INT        NULL  
   
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 USAGE EXAMPLES:  
------------------------------------------------------------  
 EXEC Cost_Usage_Site_Summary_Sel_By_Client_Commodity_Division 218,67,353,3,1594,'2009-11-01','2009-12-01'  
 EXEC Cost_Usage_Site_Summary_Sel_By_Client_Commodity_Division 10044,67,2605,39,1563,'1/1/2009','12/1/2009'  
 EXEC Cost_Usage_Site_Summary_Sel_By_Client_Commodity_Division 10861,67,1445, 3,1408,'1/1/2009','12/1/2009'  
  
AUTHOR INITIALS:  
 Initials   Name  
------------------------------------------------------------  
 HG			Hari  
 AP			Athmaram Pabbathi  
 BCH		Balaraju
 RR			Raghu Reddy  
   
 MODIFICATIONS  
 Initials	Date	    Modification  
------------------------------------------------------------  
 HG			2/08/2010   Created  
 SKA		03/12/2010  Used Left join for Cost_Usage_Site_Dtl instead of Inner join to fetch all the sites by default and Cost_Usage data if available.  
 HG			03/26/2010  Invoice participation results seperated to CTE to get the site level invoice participation(as it has multiple entries for same site and different account)  
						and the result of CTE joined with Cost_Usage_Site_Dtl to get the final output.  
 AP			09/12/2011  Removed hardcoded buckets and used table variable, Code table and used dbo.Cost_usage_Bucket_Sel_By_Commodity SP to load buckets.        
 BCH		2012-03-19  Removed site,address and state tables and used core.client_hier instead.Added client_hier_id column to select list in Cte_Site_IP_Dtl.  
 RR			2012-07-09  Removed the script replcing null values with zeros for Total_Cost, Volume, Unit_Cost
******/  
CREATE PROCEDURE dbo.Cost_Usage_Site_Summary_Sel_By_Client_Commodity_Division
      ( 
       @Client_Id INT
      ,@Commodity_Id INT
      ,@Sitegroup_Id INT
      ,@Currency_Unit_Id INT
      ,@UOM_Id INT
      ,@Begin_Dt DATE
      ,@End_Dt DATE
      ,@Site_Not_Managed INT = NULL
      ,@Country_Id INT = NULL )
AS 
BEGIN  
      SET NOCOUNT ON  
  
      DECLARE @Cost_Usage_Bucket_Id TABLE
            ( 
             Bucket_Master_Id INT PRIMARY KEY CLUSTERED
            ,Bucket_Name VARCHAR(200)
            ,Bucket_Type VARCHAR(25) )  
  
  
      INSERT      INTO @Cost_Usage_Bucket_Id
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Bucket_Type )
                  EXEC dbo.Cost_usage_Bucket_Sel_By_Commodity 
                        @Commodity_id = @Commodity_Id ;  
  
      WITH  Cte_Site_IP_Dtl
              AS ( SELECT
                        ch.client_hier_id
                       ,ch.Site_ID
                       ,ch.Client_Currency_Group_Id
                       ,rtrim(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' Site_Name
                       ,case WHEN ch.site_not_managed = 1 THEN 'Inactive'
                             ELSE 'Active'
                        END Active
                       ,max(case WHEN ip.Is_Expected = 1
                                      AND ip.Is_Received = 1 THEN 1
                                 ELSE 0
                            END) Is_Complete
                       ,max(convert(INT, ip.Is_Received)) Is_published
                       ,max(case WHEN ip.Recalc_Under_Review = 1
                                      OR ip.Variance_Under_Review = 1 THEN 1
                                 ELSE 0
                            END) Is_Under_Review
                   FROM
                        core.client_hier ch
                        LEFT OUTER JOIN dbo.Invoice_Participation ip
                              ON ch.Site_ID = ip.Site_ID
                                 AND ip.Service_Month BETWEEN @Begin_Dt AND @End_Dt
                   WHERE
                        ch.Site_Id > 0
                        AND ch.Sitegroup_Id = @Sitegroup_Id
                        AND ( @Site_Not_Managed IS NULL
                              OR ch.Site_Not_Managed = @Site_Not_Managed )
                        AND ( @Country_Id IS NULL
                              OR ch.Country_Id = @Country_Id )
                   GROUP BY
                        ch.client_hier_id
                       ,ch.Site_ID
                       ,ch.Client_Currency_Group_Id
                       ,ch.City
                       ,ch.State_Name
                       ,ch.Site_Name
                       ,ch.Site_Not_Managed)
            SELECT
                  s.Site_ID
                 ,s.Client_Hier_Id
                 ,s.Site_Name
                 ,s.Active
                 ,sum(case WHEN CUB.Bucket_Type = 'Charge' THEN cusd.Bucket_Value * cc.Conversion_Factor
                           ELSE 0
                      END) AS Total_Cost
                 ,sum(case WHEN CUB.Bucket_Type = 'Determinant' THEN cusd.Bucket_Value * uc.Conversion_Factor
                           ELSE 0
                      END) AS Volume
                 ,sum(case WHEN CUB.Bucket_Type = 'Charge' THEN cusd.Bucket_Value * cc.Conversion_Factor
                           ELSE 0
                      END) / nullif(sum(case WHEN CUB.Bucket_Type = 'Determinant' THEN cusd.Bucket_Value * uc.Conversion_Factor
                                             ELSE 0
                                        END), 0) AS Unit_Cost
                 ,isnull(max(s.Is_Complete), 0) AS Is_Complete
                 ,isnull(max(s.Is_published), 0) AS Is_published
                 ,isnull(max(s.Is_Under_Review), 0) Is_Under_Review
            FROM
                  Cte_Site_IP_Dtl s
                  LEFT OUTER JOIN dbo.Cost_Usage_Site_Dtl cusd
                        ON s.Client_Hier_Id = cusd.Client_Hier_Id
                           AND cusd.Service_Month BETWEEN @Begin_Dt AND @End_Dt
                  LEFT OUTER JOIN @Cost_Usage_Bucket_Id CUB
                        ON CUB.Bucket_Master_Id = cusd.Bucket_Master_Id
                  LEFT OUTER JOIN dbo.Consumption_Unit_Conversion uc
                        ON uc.Base_Unit_ID = cusd.UOM_Type_Id
                           AND uc.Converted_Unit_ID = @UOM_Id
                  LEFT JOIN dbo.Currency_Unit_Conversion cc
                        ON cc.Currency_Group_Id = s.Client_Currency_Group_Id
                           AND cc.Base_Unit_Id = cusd.Currency_Unit_ID
                           AND cc.Converted_Unit_Id = @Currency_Unit_Id
                           AND cc.Conversion_Date = cusd.Service_Month
            GROUP BY
                  s.SITE_ID
                 ,s.Client_Hier_Id
                 ,s.Site_Name
                 ,s.Active
            ORDER BY
                  S.Site_Name
  
END  
;
GO


GRANT EXECUTE ON  [dbo].[Cost_Usage_Site_Summary_Sel_By_Client_Commodity_Division] TO [CBMSApplication]
GO
