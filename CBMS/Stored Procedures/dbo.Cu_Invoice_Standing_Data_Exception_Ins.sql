SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
                    
Name:           
 dbo.Cu_Invoice_Standing_Data_Exception_Ins               
                      
Description:                      
   To insert Invoice Exception      
                      
 Input Parameters:                      
 Name							DataType		Default				Description                        
-------------------				-----------		----------			-----------------------------------------------------        
@Cu_Invoice_Id INT
@Exception_Type_Cd INT
@Account_Id INT 
@Commodity_Id INT 
@User_Info_Id   INT  = NULL      
            
Output Parameters:                            
 Name      DataType   Default     Description                        
---------------------------------------------------------------------------------------------        
                      
Usage Examples:                          
---------------------------------------------------------------------------------------------    
     
begin tran 
	declare @Cu_Invoice_Id INT = 87982
		, @Tvp_Cu_Invoice_SDE Tvp_Cu_Invoice_SDE 

	INSERT INTO @Tvp_Cu_Invoice_SDE VALUES(103014,1504431 ,291)
		EXEC  [Cu_Invoice_Standing_Data_Exception_Ins]    @Cu_Invoice_Id = 93946233, @Tvp_Cu_Invoice_SDE = @Tvp_Cu_Invoice_SDE

	select * from Cu_Invoice_Standing_Data_Exception WHERE Cu_Invoice_Id =   93946233	

rollback 
      
         
Author Initials:                      
 Initials			Name                      
-------------		--------------------------------------------------------------------------------        
HKT					Harish Kumar Tirumandyam               
         
Modifications:                      
Initials		Date				Modification                      
------------	----------------	-----------------------------------------------------------------        
 HKT			2020-04-15			Created For Data Purple      
 HKT			2020-05-01			Added Group Routed to           
 HKT            2020-05-27          Added Detail Table Logic           
******/
CREATE PROCEDURE [dbo].[Cu_Invoice_Standing_Data_Exception_Ins]
(
	@Cu_Invoice_Id		  INT
	, @Tvp_Cu_Invoice_SDE Tvp_Cu_Invoice_SDE READONLY
	, @User_Info_Id		  INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Exception_Status_Cd INT;

	DECLARE @Exception_Type_Group_Map TABLE
	(
		Exception_Type_Cd INT NOT NULL
		, User_Info_Id	  INT NOT NULL
	);

	INSERT INTO @Exception_Type_Group_Map
		(
			Exception_Type_Cd
			, User_Info_Id
		)
	SELECT
		e.Exception_Type_Cd
		, ISNULL(ui.USER_INFO_ID, rmm.USER_INFO_ID)
	FROM
		@Tvp_Cu_Invoice_SDE e
		INNER JOIN dbo.Standing_Data_Exception_Group_Map sdegm
			ON sdegm.Exception_Type_Cd = e.Exception_Type_Cd
		INNER JOIN Core.Client_Hier_Account cha
			ON cha.Account_Id = e.Account_Id
		INNER JOIN dbo.UTILITY_DETAIL ud
			ON ud.VENDOR_ID = cha.Account_Vendor_Id
		LEFT JOIN(dbo.Utility_Detail_Analyst_Map map
				  INNER JOIN dbo.GROUP_INFO gi
					  ON gi.GROUP_INFO_ID = map.Group_Info_ID
				  INNER JOIN dbo.USER_INFO ui
					  ON ui.USER_INFO_ID = map.Analyst_ID)
			ON map.Utility_Detail_ID = ud.UTILITY_DETAIL_ID AND map.Group_Info_ID = sdegm.Managed_By_Group_Info_Id
		LEFT OUTER JOIN dbo.STATE st
			ON st.STATE_ID = cha.Meter_State_Id
		LEFT OUTER JOIN dbo.REGION reg
			ON reg.REGION_ID = st.REGION_ID
		LEFT OUTER JOIN dbo.REGION_MANAGER_MAP rmm
			ON rmm.REGION_ID = reg.REGION_ID
	WHERE
		sdegm.Managed_By_Group_Info_Id IS NOT NULL
		AND sdegm.Managed_By_User_Info_Id IS NULL
		AND cha.Account_Type = 'Utility';

	INSERT INTO @Exception_Type_Group_Map
		(
			Exception_Type_Cd
			, User_Info_Id
		)
	SELECT
		e.Exception_Type_Cd
		, ISNULL(ui.USER_INFO_ID, rmm.USER_INFO_ID)
	FROM
		@Tvp_Cu_Invoice_SDE e
		INNER JOIN dbo.Standing_Data_Exception_Group_Map sdegm
			ON sdegm.Exception_Type_Cd = e.Exception_Type_Cd
		INNER JOIN Core.Client_Hier_Account cha
			ON cha.Account_Id = e.Account_Id
		INNER JOIN Core.Client_Hier_Account ucha
			ON ucha.Meter_Id = cha.Meter_Id
		INNER JOIN dbo.UTILITY_DETAIL ud
			ON ud.VENDOR_ID = ucha.Account_Vendor_Id
		LEFT JOIN(dbo.Utility_Detail_Analyst_Map map
				  INNER JOIN dbo.GROUP_INFO gi
					  ON gi.GROUP_INFO_ID = map.Group_Info_ID
				  INNER JOIN dbo.USER_INFO ui
					  ON ui.USER_INFO_ID = map.Analyst_ID)
			ON map.Utility_Detail_ID = ud.UTILITY_DETAIL_ID AND map.Group_Info_ID = sdegm.Managed_By_Group_Info_Id
		LEFT OUTER JOIN dbo.STATE st
			ON st.STATE_ID = cha.Meter_State_Id
		LEFT OUTER JOIN dbo.REGION reg
			ON reg.REGION_ID = st.REGION_ID
		LEFT OUTER JOIN dbo.REGION_MANAGER_MAP rmm
			ON rmm.REGION_ID = reg.REGION_ID
	WHERE
		sdegm.Managed_By_Group_Info_Id IS NOT NULL
		AND sdegm.Managed_By_User_Info_Id IS NULL
		AND cha.Account_Type = 'Supplier'
		AND ucha.Account_Type = 'Utility'
		AND NOT EXISTS (
						   SELECT 1 FROM @Exception_Type_Group_Map egm WHERE egm.Exception_Type_Cd = e.Exception_Type_Cd
					   );

	INSERT INTO @Exception_Type_Group_Map
		(
			Exception_Type_Cd
			, User_Info_Id
		)
	SELECT
		e.Exception_Type_Cd
		, sdegm.Managed_By_User_Info_Id
	FROM
		@Tvp_Cu_Invoice_SDE e
		INNER JOIN dbo.Standing_Data_Exception_Group_Map sdegm
			ON sdegm.Exception_Type_Cd = e.Exception_Type_Cd
	WHERE
		sdegm.Managed_By_User_Info_Id IS NOT NULL
				AND NOT EXISTS (
						   SELECT 1 FROM @Exception_Type_Group_Map egm WHERE egm.Exception_Type_Cd = e.Exception_Type_Cd
					   );
	-- @Exception_Status_Cd
	SELECT
		@Exception_Status_Cd = cd.Code_Id
	FROM
		dbo.Code cd
		JOIN dbo.Codeset cs
			ON cd.Codeset_Id = cs.Codeset_Id
	WHERE
		cs.Codeset_Name = 'Exception Status' AND cd.Code_Value = 'New';

	-- insert  dbo.Cu_Invoice_Standing_Data_Exception
	INSERT INTO dbo.Cu_Invoice_Standing_Data_Exception
		(
			[Cu_Invoice_Id]
			, [Exception_Type_Cd]
			, [Queue_Id]
			, [Exception_Status_Cd]
			, [Closed_By_User_Id]
			, [Closed_Ts]
			, [Updated_User_Id]
		)
	SELECT
		@Cu_Invoice_Id AS [Cu_Invoice_Id]
		, e.Exception_Type_Cd AS [Exception_Type_Cd]
		, ui.QUEUE_ID AS [Queue_Id]
		, @Exception_Status_Cd AS [Exception_Status_Cd]
		, NULL AS [Closed_By_User_Id]
		, NULL AS Closed_Ts
		, ISNULL(@User_Info_Id, 16) AS Updated_User_Id
	FROM
		@Tvp_Cu_Invoice_SDE e
		INNER JOIN @Exception_Type_Group_Map etgm
			ON etgm.Exception_Type_Cd = e.Exception_Type_Cd
		INNER JOIN dbo.USER_INFO ui
			ON ui.USER_INFO_ID = etgm.User_Info_Id
	GROUP BY
		e.Exception_Type_Cd
		, ui.QUEUE_ID;


	INSERT INTO [dbo].[Cu_Invoice_Standing_Data_Exception_Dtl]
		(
			[Cu_Invoice_Standing_Data_Exception_Id]
			, [Account_Id]
			, [Commodity_Id]
			, [Status_Cd]
			, [Created_Ts]
			, [Created_User_Id]
			, [Last_Change_Ts]
			, [Updated_User_Id]
		)
	SELECT
		MAX(cisde.Cu_Invoice_Standing_Data_Exception_Id)
		, e.Account_Id
		, e.Commodity_Id
		, @Exception_Status_Cd
		, GETDATE() AS Created_Ts
		, @User_Info_Id AS Created_User_Id
		, GETDATE() AS Last_Change_Ts
		, @User_Info_Id AS Updated_User_Id
	FROM
		dbo.Cu_Invoice_Standing_Data_Exception cisde
		INNER JOIN @Tvp_Cu_Invoice_SDE e
			ON e.Exception_Type_Cd = cisde.Exception_Type_Cd
	WHERE
		cisde.Cu_Invoice_Id = @Cu_Invoice_Id
		AND cisde.Exception_Type_Cd = e.Exception_Type_Cd
		AND cisde.Exception_Status_Cd = @Exception_Status_Cd
	GROUP BY
		cisde.Cu_Invoice_Id
		, cisde.Exception_Type_Cd
		, e.Account_Id
		, e.Commodity_Id;



END;
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Standing_Data_Exception_Ins] TO [CBMSApplication]
GO
