SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                 
/******                        
 NAME: [dbo].[Site_Name_Upd_By_Client_Hier]            
                        
 DESCRIPTION:                        
			To Update Sitename and Division based on Client Hier                      
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
 @Site_Name					VARCHAR(200)
 @Division_Id				   INT 
 @Client_Hier_Id			   INT                     
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            

BEGIN TRAN
EXEC [dbo].[Site_Name_Upd_By_Client_Hier] 
      @Site_Name = 'Brooksville'
     ,@Division_Id = 52
     ,@Client_Hier_Id = 1130 
ROLLBACK
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2014-12-08       Created                
                       
******/                 
           
CREATE PROCEDURE [dbo].[Site_Name_Upd_By_Client_Hier]
      ( 
       @Site_Name VARCHAR(200)
      ,@Division_Id INT
      ,@Client_Hier_Id INT )
AS 
BEGIN                
      SET NOCOUNT ON;     
      
      UPDATE
            s
      SET   
            s.SITE_NAME = @Site_Name
           ,s.Division_Id = @Division_Id
      FROM
            dbo.SITE s
            INNER JOIN core.Client_Hier ch
                  ON s.site_Id = ch.Site_Id
      WHERE
            ch.Client_Hier_Id = @Client_Hier_Id
           

                        
                       
END 

;
GO
GRANT EXECUTE ON  [dbo].[Site_Name_Upd_By_Client_Hier] TO [CBMSApplication]
GO
