SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Season_Sel_By_Season_Id

DESCRIPTION:
	It is used to select season by given season id.

INPUT PARAMETERS:
	Name						  DataType		Default	Description
------------------------------------------------------------
	@Season_Id					  INT


OUTPUT PARAMETERS:
	Name						  DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
					
Exec dbo.Season_Sel_By_Season_Id 32
                   
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

	BCH			Balaraju Chalumuri

MODIFICATIONS

	Initials	Date		 Modification
-----------------------------------------------------------
	BCH			2012-07-017  Created
******/ 
CREATE PROCEDURE dbo.Season_Sel_By_Season_Id 
	@Season_Id INT
AS 
BEGIN
      SET NOCOUNT ON ;

      SELECT
            SEASON_ID
           ,SEASON_NAME
           ,SEASON_FROM_DATE
           ,SEASON_TO_DATE
      FROM
            dbo.SEASON
      WHERE
            SEASON_ID = @Season_Id
END
;
GO
GRANT EXECUTE ON  [dbo].[Season_Sel_By_Season_Id] TO [CBMSApplication]
GO
