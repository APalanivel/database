SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE [dbo].[cbmsAccount_GetBillingDaysAdjustment]
	( @MyAccountId int
	, @account_id int
	, @working_date datetime
	)
AS
BEGIN

	   select max(isNull(rs.billing_days_adjustment, 0)) billing_days_adjustment
	     from account a
	     join vwAccountMeter vam on vam.account_id = a.account_id
	     join meter m on m.meter_id = vam.meter_id
	     join rate r on r.rate_id = m.rate_id
  left outer join rate_schedule rs on rs.rate_id = r.rate_id and rs.rs_start_date <= @working_date and rs.rs_end_date >= @working_date
	    where a.account_id = @account_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsAccount_GetBillingDaysAdjustment] TO [CBMSApplication]
GO
