SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_SET_SUPPLIER_BOC_DELIVERY_FUEL_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@deliveryId int,
@deliveryPipeLineComments varchar(4000),
@fuelInBidTypeId varchar(10),
@fuelInbidValue varchar(100),
@transportInBidtypeid varchar(10),
@transportInBidValue varchar(100),
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    
@deliveryIdentityId  int outPUT

USAGE EXAMPLES:
------------------------------------------------------------
---- TEST 1
----EXEC  SR_RFP_GET_SUPPLIER_BOC_DELIVERY_FUEL_P 1,1,931

---- TEST 2
---- Check the values before and after insert of record at SV
--SELECT * FROM SV..SR_RFP_DELIVERY_FUEL WHERE SR_RFP_DELIVERY_FUEL_ID =(SELECT MAX(SR_RFP_DELIVERY_FUEL_ID) FROM SV..SR_RFP_DELIVERY_FUEL)
---- 197208

---- Insert a test entry
--EXEC [dbo].[SR_RFP_SET_SUPPLIER_BOC_DELIVERY_FUEL_P] 
--@deliveryId = 0,
--@deliveryPipeLineComments = 'Test Data',
--@fuelInBidTypeId = NULL,
--@fuelInbidValue = NULL,
--@transportInBidtypeid = NULL,
--@transportInBidValue = NULL,
--@deliveryIdentityId = NULL

---- Delete the entry
--DELETE SV..SR_RFP_DELIVERY_FUEL WHERE SR_RFP_DELIVERY_FUEL_ID =(SELECT MAX(SR_RFP_DELIVERY_FUEL_ID) FROM SV..SR_RFP_DELIVERY_FUEL)



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[SR_RFP_SET_SUPPLIER_BOC_DELIVERY_FUEL_P] 

@deliveryId int,
@deliveryPipeLineComments varchar(4000),
@fuelInBidTypeId varchar(10),
@fuelInbidValue varchar(100),
@transportInBidtypeid varchar(10),
@transportInBidValue varchar(100),
@deliveryIdentityId int outPUT

AS
	
SET NOCOUNT ON

if @deliveryId=0

begin


		INSERT INTO SR_RFP_DELIVERY_FUEL
		(
			DELIVERY_PIPELINE_COMMENTS,
			FUEL_IN_BID_TYPE_ID,
			FUEL_IN_BID_VALUE,
			TRANSPORT_IN_BID_TYPE_ID,
			TRANSPORT_IN_BID_VALUE
		)
		VALUES
		(
			@deliveryPipeLineComments,
			@fuelInBidTypeId,
			@fuelInbidValue,
			@transportInBidtypeid,
			@transportInBidValue			
		)
	
		SELECT @deliveryIdentityId=SCOPE_IDENTITY()
						

end

if @deliveryId>0

BEGIN
	UPDATE SR_RFP_DELIVERY_FUEL SET

	DELIVERY_PIPELINE_COMMENTS=@deliveryPipeLineComments,
	FUEL_IN_BID_TYPE_ID=@fuelInBidTypeId,
	FUEL_IN_BID_VALUE=@fuelInbidValue,
	TRANSPORT_IN_BID_TYPE_ID=@transportInBidtypeid,
	TRANSPORT_IN_BID_VALUE=@transportInBidValue

	WHERE SR_RFP_DELIVERY_FUEL_ID=@deliveryId
	
   SELECT @deliveryIdentityId=@deliveryID
END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_SET_SUPPLIER_BOC_DELIVERY_FUEL_P] TO [CBMSApplication]
GO
