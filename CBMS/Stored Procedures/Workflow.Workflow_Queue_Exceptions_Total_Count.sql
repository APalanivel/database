SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/******      
NAME:    [Workflow].[Workflow_Queue_Exceptions_Total_Count]  
DESCRIPTION:  it'll Return the CBMS Home Page Exceptions Counts details for each  Queue Modules  
------------------------------------------------------------     
 INPUT PARAMETERS:      
 Name   DataType  Default Description      
 @userID   VARCHAR(MAX),       
 @moduleID INT      
------------------------------------------------------------      
 OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
 USAGE EXAMPLES:      
 EXEC [Workflow].[Workflow_Queue_Exceptions_Total_Count] @User_ID   ='115680' --(Current User)  --115681,115680
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
TRK   Ramakrishna Thummala Summit Energy 
AP	Arunkumar Palanivel  
   
 MODIFICATIONS       
 Initials Date   Modification      
------------------------------------------------------------      
 TRK    Sep-2019  Created  
 AP		Oct 30,2019	Modified procedure to improve the performance. THis take time to get all unassigned exception count.
		So modified SP
  
******/  
  
CREATE PROCEDURE [Workflow].[Workflow_Queue_Exceptions_Total_Count]          
(          
  @User_ID   VARCHAR(MAX) --(Current User)  
)          
AS          
BEGIN       

SET NOCOUNT ON      
DECLARE @User_input INT ,  
  @Workflow_Sub_Queue_Name NVARCHAR(255),    
  @TOTAL_COUNT INT,   
  @PAGE_ROW_COUNT INT,  
  @Date_IN_QUEUE_COUNT INT,
  @MODULE_ID INT,
  @All_My_Exceptions INT,
  @All_My_Exceptions_10_Days INT,
  @All_Unassigned_Exceptions INT,
  @All_Unassigned_Exceptions_10_Days INT




SET @User_input  = (SELECT queue_id FROM user_info WHERE user_info_id =@User_ID )         
SELECT @MODULE_ID = Workflow_Queue_Id FROM Workflow.Workflow_Queue   WHERE Workflow_Queue_Name='Invoice'


CREATE TABLE #Workflow_Sub_Queue_Name 
 (   
 Workflow_Sub_Queue_Name NVARCHAR(255),  
 )  
   
 ;WITH CTE AS(  
 SELECT  DISTINCT        
  WQS.workflow_sub_queue_name  
  FROM workflow.workflow_sub_queue WQS        
  INNER JOIN permission_info P WITH (NOLOCK) ON WQS.permission_info_id = P.permission_info_id        
  INNER JOIN group_info_permission_info_map AS GIPM WITH (NOLOCK) ON P.permission_info_id = GIPM.permission_info_id        
  INNER JOIN group_info GI ON GIPM.group_info_id = GI.group_info_id        
  INNER JOIN user_info_group_info_map UIG ON GI.group_info_id = UIG.group_info_id         
  INNER JOIN user_info U ON U.user_info_id = UIG.user_info_id        
  WHERE U.USER_INFO_ID=@User_ID AND WQS.workflow_sub_queue_name ='Unassigned'  
UNION ALL  
 SELECT          
  WQS.workflow_sub_queue_name  
  FROM workflow.workflow_sub_queue WQS WHERE WQS.workflow_sub_queue_name = 'All My Exceptions' AND WQS.Permission_Info_Id = -1   
  )  
  INSERT INTO #Workflow_Sub_Queue_Name  (Workflow_Sub_Queue_Name)  
  SELECT  workflow_sub_queue_name FROM CTE  
  
 SET @TOTAL_COUNT = (SELECT count(DISTINCT cud.CU_INVOICE_ID) FROM dbo.CU_EXCEPTION_DENORM CUD 
						WHERE CUD.QUEUE_ID =@User_input)

	 SET @Date_IN_QUEUE_COUNT = (SELECT count(DISTINCT cud.CU_INVOICE_ID) FROM dbo.CU_EXCEPTION_DENORM CUD 
						WHERE CUD.QUEUE_ID =@User_input
						AND cud.DATE_IN_QUEUE< dateadd (dd, -10, GETDATE()) )

	 --get my exception count 
  
SET @All_My_Exceptions = isnull(@TOTAL_COUNT,0) 
SET @All_My_Exceptions_10_Days = isnull (@Date_IN_QUEUE_COUNT,0)
                 
 
 IF EXISTS (SELECT 1 FROM #Workflow_Sub_Queue_Name WHERE Workflow_Sub_Queue_Name='unassigned')

 BEGIN
 DECLARE @Table_Count table
(Invoice_Count int,
Count_Description varchar(100))
;WITH cte
AS ( SELECT DISTINCT
            cuex.CU_INVOICE_ID
     FROM   dbo.CU_EXCEPTION_DENORM cuex
            JOIN
            dbo.QUEUE qt
                  ON cuex.QUEUE_ID = qt.QUEUE_ID
            JOIN
            dbo.ENTITY et
                  ON et.ENTITY_ID = qt.QUEUE_TYPE_ID
     WHERE  et.ENTITY_NAME = 'public' 
	 AND CUEX.EXCEPTION_TYPE <> 'FAILED RECALC')
    , cte1
AS ( SELECT DISTINCT
            cte.CU_INVOICE_ID
     FROM   cte
            JOIN
            dbo.CU_EXCEPTION_DENORM CU
                  ON cte.CU_INVOICE_ID = CU.CU_INVOICE_ID
     WHERE  CU.DATE_IN_QUEUE < dateadd(dd, -10, getdate())) --55336
INSERT @Table_Count  
SELECT
      count(CU_INVOICE_ID) AS Invoice_Count
    , 'Total Count' AS Count_Description
FROM  cte
UNION
SELECT
      count(CU_INVOICE_ID) AS Invoice_Count
    , '10 days Count' AS Count_Description
FROM  cte1;

  
  SET @All_Unassigned_Exceptions = isnull ((SELECT  invoice_count FROM @Table_Count WHERE Count_Description = 'Total Count'),0)
 SET @All_Unassigned_Exceptions_10_Days =isnull((SELECT invoice_count FROM @Table_Count WHERE Count_Description = '10 days Count') ,0)
  

END
 
 IF EXISTS (SELECT 1 FROM #Workflow_Sub_Queue_Name WHERE Workflow_Sub_Queue_Name ='Unassigned' )
 BEGIN
 
			 SELECT    
			@All_My_Exceptions AS [All My Exceptions],  
			@All_My_Exceptions_10_Days AS [All My Exceptions_10_Days],  
			@All_Unassigned_Exceptions AS [All Unassigned Exceptions],  
			@All_Unassigned_Exceptions_10_Days AS  [All Unassigned Exceptions_10_Days]  
			END
			ELSE 
			BEGIN 
			SELECT 
			@All_My_Exceptions AS [All My Exceptions],  
			@All_My_Exceptions_10_Days AS [All My Exceptions_10_Days]
			END
 
  DROP TABLE #Workflow_Sub_Queue_Name 

END              
GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_Exceptions_Total_Count] TO [CBMSApplication]
GO
