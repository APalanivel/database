SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********       
NAME:      
 dbo.Client_dtl_by_Account_Id_Sel
  
DESCRIPTION:  This procedure used to get the details of Client sel by Account.

INPUT PARAMETERS:      
      Name					DataType          Default     Description      
----------------------------------------------------------------------      
	@Account_Id				INT

OUTPUT PARAMETERS:      
      Name              DataType          Default     Description      
----------------------------------------------------------------------      
      
      
USAGE EXAMPLES:      
----------------------------------------------------------------------     

EXEC  Client_dtl_by_Account_Id_Sel @Account_id = 1475398         
  
  
Initials	Name      
----------------------------------------------------------------------      
HG			Harihara suthan G  
NR			Narayana Reddy

Initials	Date			Modification      
----------------------------------------------------------------------      
HG          2018-03-15		Created for interval data recalc
NR			2018-12-04		Data2.0 - Added Client_Hier_Id. 

***************/
CREATE PROCEDURE [dbo].[Client_dtl_by_Account_Id_Sel]
    (
        @Account_id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            ch.Client_Id
            , ch.Client_Name
            , ch.Site_name
            , cha.Account_Number
            , ch.Time_Zone
            , ch.Client_Hier_Id
        FROM
            Core.Client_Hier_Account cha
            JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
        WHERE
            cha.Account_Id = @Account_id
        GROUP BY
            ch.Client_Id
            , ch.Client_Name
            , ch.Site_name
            , cha.Account_Number
            , ch.Time_Zone
            , ch.Client_Hier_Id;

    END;





GO
GRANT EXECUTE ON  [dbo].[Client_dtl_by_Account_Id_Sel] TO [CBMSApplication]
GO
