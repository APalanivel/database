SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
NAME:   [dbo].[Not_Reported_Invoices_For_Review]  
   
                 
DESCRIPTION:                   
 The point of this query is so that the Invoice Collection team can identify months where no reported invoice exists (i.e. no cost/usage), but the ICR record was tied to an invoice that was made Not Reported.      
                 
INPUT PARAMETERS:                  
Name     DataType  Default   Description        
------------------------------------------------------------------------        
     
        
 OUTPUT PARAMETERS:                  
Name     DataType  Default   Description        
------------------------------------------------------------------------      
               
 USAGE EXAMPLES:                  
------------------------------------------------------------------------                       
Execute dbo.Not_Reported_Invoices_For_Review 1234
       
  
 AUTHOR INITIALS:               
 Initials  Name  
------------------------------------------------------------------------  
 SP    Srinivasarao Patchava.  
  
 moDIFICATIONS:                   
 Initials  Date   Modification                  
------------------------------------------------------------------------      
 SP            29/10/2018       created.  
******/
CREATE PROCEDURE [dbo].[Not_Reported_Invoices_For_Review]
    (
        @Client_Id INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;
        SELECT
            ch.Client_Name
            , ch.Site_name AS Site_Name
            , cha.Account_Number
            , aicm.Service_Month AS Service_Month
            , q.Collection_Start_Dt AS Collection_Start_Dt
            , q.Collection_End_Dt AS Collection_End_Dt
            , c.Code_Value AS ICR_Status
            , ci.CU_INVOICE_ID
        FROM
            dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map imap
            INNER JOIN dbo.Invoice_Collection_Queue_Month_Map qmMap
                ON qmMap.Account_Invoice_Collection_Month_Id = imap.Account_Invoice_Collection_Month_Id
            INNER JOIN dbo.Account_Invoice_Collection_Month aicm
                ON aicm.Account_Invoice_Collection_Month_Id = qmMap.Account_Invoice_Collection_Month_Id
            INNER JOIN dbo.Invoice_Collection_Queue q
                ON q.Invoice_Collection_Queue_Id = qmMap.Invoice_Collection_Queue_Id
            INNER JOIN dbo.CU_INVOICE ci
                ON ci.CU_INVOICE_ID = imap.Cu_Invoice_Id
            INNER JOIN dbo.Code c
                ON c.Code_Id = q.Status_Cd
            INNER JOIN dbo.Invoice_Collection_Account_Config ac
                ON ac.Invoice_Collection_Account_Config_Id = q.Invoice_Collection_Account_Config_Id
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = ac.Account_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN dbo.Code cc
                ON cc.Code_Id = q.Invoice_Collection_Queue_Type_Cd
            INNER JOIN dbo.Code cce
                ON cce.Code_Id = q.Invoice_Collection_Exception_Type_Cd
        WHERE
            (c.Code_Value = 'Resolved')
            AND ch.CLIENT_ID = @Client_Id
			AND ci.IS_REPORTED = 0 
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.CU_INVOICE_SERVICE_MONTH cism
                                    INNER JOIN dbo.CU_INVOICE ci1
                                        ON ci1.CU_INVOICE_ID = cism.CU_INVOICE_ID
                               WHERE
                                    cism.Account_ID = cha.Account_Id
                                    AND cism.SERVICE_MONTH = aicm.Service_Month
                                    AND ci1.IS_REPORTED = 1)
        GROUP BY
            ch.Client_Name
            , ch.Site_name
            , cha.Account_Number
            , q.Collection_Start_Dt
            , q.Collection_End_Dt
            , c.Code_Value
            , aicm.Service_Month
            , ci.CU_INVOICE_ID;



    END;


GO
GRANT EXECUTE ON  [dbo].[Not_Reported_Invoices_For_Review] TO [CBMS_SSRS_Reports]
GO
