
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME: [DBO].[Commodity_Ins]

DESCRIPTION:

	This stored procedure used to stored the new services and loads the following legacy data needed for the service
		- Bucket_Master
		- Bucket_Category_Rule
		- Entity (Unit of measurement mapping)
		- Variance_Consumption_Level
		- Country_Commodity_Uom

INPUT PARAMETERS:
	NAME							DATATYPE	DEFAULT		DESCRIPTION
-----------------------------------------------------------------------
	@Commodity_Name					VARCHAR(50)
	@Commodity_Dsc					VARCHAR(200)
	@Service_Mapping_Instructions	VARCHAR(max) NULL
	@Is_Weather_Adjusted			BIT
	@Default_Uom_Cd					INT						Default Uom Cd
	@Associated_Uom_Cd				VARCHAR(max) NULL		Comma seperated UOM cd value selected by the user
	@Created_By_User_Id				INT						User who is creating the service

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION
-----------------------------------------------------------------------

USAGE EXAMPLES:
-----------------------------------------------------------------------

BEGIN TRAN

	EXEC dbo.Commodity_Ins
		  @Commodity_Name = 'Test Commodity Ins112323'
		 , -- varchar(50)
		  @Commodity_Dsc = 'Test Commodity Ins'
		 , -- varchar(200)
		  @Service_Mapping_Instructions = NULL
		 , -- varchar(max)
		  @Is_Weather_Adjusted = 0
		 , -- bit
		  @Default_Uom_Cd = 100095
		 , -- int
		  @Associated_Uom_Cd = '100089,100095'
		 , -- varchar(max)
		  @Created_By_User_Id = 16 -- int
		  ,@Is_Client_Specific = 1

		 -- SELECT * FROM dbo.Commodity WHERE Commodity_Name = 'Test Commodity Ins112323'
		 -- SELECT * FROM Bucket_Master bm JOIN dbo.Commodity com ON com.Commodity_id = bm.Commodity_Id WHERE com.Commodity_Name = 'Test Commodity Ins'
			--SELECT * FROM dbo.Country_Commodity_Uom WHERE commodity_id = 101044
ROLLBACK TRAN


AUTHOR INITIALS:
	INITIALS	NAME
-----------------------------------------------------------------------
	GB			Geetansu Behera
	CMH			Chad Hattabaugh
	DMR			Deana Ritter
	HG			Harihara Suthan G
	TP			Anoop

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
-----------------------------------------------------------------------
	SKA			31/08/2009	Created individual _INS SP against Commodity_Ins_Upd
	SKA			6/09/2009	Change the default value for @IS_SUSTAINABLE from 0 to 1
	DMR			09/10/2010	Modified for Quoted_Identifier
	HG			2013-04-22	Modified for manage services requirement
	HG			2015-07-10	Gobal CBMS Sourcing Enh - Modified to add the default uom for country in Country_Commodity_Uom
	TP			2015-09-01	DSM Changes - Added new column Is_Client_Specific
*/


CREATE PROCEDURE [dbo].[Commodity_Ins]
      ( 
       @Commodity_Name VARCHAR(50)
      ,@Commodity_Dsc VARCHAR(200)
      ,@Service_Mapping_Instructions VARCHAR(MAX) = NULL
      ,@Is_Weather_Adjusted BIT
      ,@Default_Uom_Cd INT
      ,@Associated_Uom_Cd VARCHAR(MAX)
      ,@Created_By_User_Id INT
      ,@Is_Sustainable BIT = 1
      ,@Is_Alternate_Fuel BIT = 1
      ,@Is_Client_Specific BIT )
AS 
BEGIN

      SET NOCOUNT ON;

	-- Save data in to Commodity table
	-- Call a stored procedure to add Uom to Entity table & save default consumption level
	-- Call a sproc to map the default uom for all the country
	-- Call a stored procedure to save the other commodity default data Variance_Consumption_Level, Bucket_Master, Bucket_Category_Rule

      DECLARE
            @Bucket_Aggregation_Cd_Other_Services INT
           ,@Uom_Entity_Type INT
           ,@Source_Cd INT
           ,@Display_Sq INT
           ,@Default_Uom_Entity_Type_Id INT
           ,@Commodity_Id INT
           ,@System_User_Id INT;

      SELECT
            @Source_Cd = cd.Code_id
      FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code cd
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Std_Column_Name = 'Source_Cd'
            AND cd.Code_Value = 'Default';

      SELECT
            @System_User_Id = User_Info_Id
      FROM
            dbo.User_Info
      WHERE
            Username = 'conversion';

      SELECT
            @Bucket_Aggregation_Cd_Other_Services = cd.Code_Id
      FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code cd
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Std_Column_Name = 'Bucket_Aggregation_Cd'
            AND cd.Code_Value = 'Other Service';

      BEGIN TRY
            BEGIN TRAN;

            SELECT
                  @Display_Sq = max(com.Display_Sq) + 1
            FROM
                  dbo.Commodity com;

            SET @Display_Sq = isnull(@Display_Sq, 1);
            SET @Commodity_Dsc = isnull(nullif(@Commodity_Dsc, ''), @Commodity_Name);

            INSERT      INTO dbo.Commodity
                        ( 
                         Commodity_Name
                        ,Commodity_Dsc
                        ,Is_Alternate_Fuel
                        ,Is_Sustainable
                        ,Base_UOM_Cd
                        ,Display_Sq
                        ,Is_Active
                        ,Source_Cd
                        ,Bucket_Aggregation_Cd
                        ,Service_Mapping_Instructions
                        ,Is_Weather_Adjusted
                        ,Created_By_User_Id
                        ,Created_Ts
                        ,Last_Change_By_User_Id
                        ,Last_Change_Ts
                        ,Is_Client_Specific )
            VALUES
                        ( 
                         @Commodity_Name
                        ,@Commodity_Dsc
                        ,@Is_Sustainable
                        ,@Is_Sustainable
                        ,@Default_Uom_Cd
                        ,@Display_Sq
                        ,1-- Is_Active
                        ,@Source_Cd
                        ,@Bucket_Aggregation_Cd_Other_Services
                        ,@Service_Mapping_Instructions
                        ,@Is_Weather_Adjusted
                        ,@Created_By_User_Id
                        ,getdate() -- Created_Ts
                        ,@Created_By_User_Id
                        ,getdate() -- Last_Change_Ts
                        ,@Is_Client_Specific );

            SET @Commodity_Id = scope_identity();

            EXEC dbo.Commodity_Uom_Ins_Into_Entity 
                  @Commodity_Id = @Commodity_Id
                 ,@Associated_Uom_Cd = @Associated_Uom_Cd
                 ,@Uom_Entity_Type_Id = @Uom_Entity_Type OUT;

            SELECT
                  @Default_Uom_Entity_Type_Id = uomid.ENTITY_ID
            FROM
                  dbo.Code uomcd
                  INNER JOIN dbo.Entity uomid
                        ON uomid.ENTITY_NAME = uomcd.Code_Value
            WHERE
                  uomcd.Code_Id = @Default_Uom_Cd
                  AND uomid.ENTITY_TYPE = @Uom_Entity_Type;

            UPDATE
                  dbo.Commodity
            SET   
                  Default_UOM_Entity_Type_Id = @Default_Uom_Entity_Type_Id
                 ,UOM_Entity_Type = @Uom_Entity_Type
            WHERE
                  Commodity_Id = @Commodity_Id;

			-- Assign the default UOM for all the country
			
            EXEC dbo.Country_Commodity_Uom_Merge 
                  @Country_Id = NULL
                 ,@Commodity_Id = @Commodity_Id
                 ,@Default_Uom_Type_Id = @Default_Uom_Entity_Type_Id
                 ,@User_Info_Id = @System_User_Id;

			--Add New buckets to the commodity
            EXEC dbo.Add_Bucket_Master_And_Bucket_Category_Rule_For_New_Commodity 
                  @Commodity_Id = @Commodity_Id;

			-- Add Variance rule to the new commodity
            EXEC dbo.Add_Variance_Default_Consumption_Level_For_New_Commodity 
                  @Commodity_Id = @Commodity_Id;
	
            SELECT
                  @Commodity_Id AS Commodity_Id

            COMMIT TRAN;
      END TRY
      BEGIN CATCH
            IF @@TRANCOUNT > 0 
                  BEGIN
                        ROLLBACK;
                  END;

            EXEC dbo.usp_RethrowError;

      END CATCH;

END;

;


;
GO



GRANT EXECUTE ON  [dbo].[Commodity_Ins] TO [CBMSApplication]
GO
