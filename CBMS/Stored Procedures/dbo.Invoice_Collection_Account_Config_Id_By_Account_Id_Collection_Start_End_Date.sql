SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Invoice_Collection_Account_Config_Id_By_Account_Id_Collection_Start_End_Date       
              
Description:              
			 This Sp is used to get the Invoice_Collection_Account_Config_Id for the 
			 given Account_Id Collection Start Date and Collection End Date
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
	                      
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

   EXEC Invoice_Collection_Account_Config_Id_By_Account_Id_Collection_Start_End_Date 1096000,'2016-08-02','2016-09-24'
   
   
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2017-01-24		Created For Invoice_Collection.         
             
******/ 
CREATE PROCEDURE [dbo].[Invoice_Collection_Account_Config_Id_By_Account_Id_Collection_Start_End_Date]
      ( 
       @Account_Id INT
      ,@Collection_Start_Date DATE
      ,@Collection_End_Date DATE )
AS 
BEGIN
      SET NOCOUNT ON;
      
      SELECT
            icac.Invoice_Collection_Account_Config_Id
      FROM
            dbo.Invoice_Collection_Account_Config icac
      WHERE
            icac.Account_Id = @Account_Id
            AND icac.Invoice_Collection_Service_Start_Dt <= @Collection_Start_Date
            AND icac.Invoice_Collection_Service_End_Dt >= @Collection_End_Date
      GROUP BY
            icac.Invoice_Collection_Account_Config_Id
      
                                
END






;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Account_Config_Id_By_Account_Id_Collection_Start_End_Date] TO [CBMSApplication]
GO
