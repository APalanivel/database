
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	cbms_prod.dbo.cbmsAccount_GetManagedBy

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@account_id    	int          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.cbmsAccount_GetManagedBy 289851
	EXEC dbo.cbmsAccount_GetManagedBy 248149
	EXEC dbo.cbmsAccount_GetManagedBy 245110

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan G
	SP	        Sandeep Pigilam           

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
							/* branched to improve performance. Scott Drake 1/12/2006 */
							/* modified to use new utility detail analyst map tables. Kevin HortON 1/21/2010 */

	HG        	08/10/2010	Comments header added
							Rewrote the query to remove IF/THEN else
							Changed the logic of getting Queue_id, if there is no Utility analyst mapping for that vendor select  the queue id of the regional manager.
    SP	        2014-09-09  For Data Transition used a table variable @Group_Legacy_Name to handle the hardcoded group names  
							

******/
CREATE PROCEDURE [dbo].[cbmsAccount_GetManagedBy] ( @account_id INT )
AS 
BEGIN

      SET NOCOUNT ON
	
      DECLARE @Group_Legacy_Name TABLE
            ( 
             GROUP_INFO_ID INT
            ,GROUP_NAME VARCHAR(200)
            ,Legacy_Group_Name VARCHAR(200) )

      INSERT      INTO @Group_Legacy_Name
                  ( 
                   GROUP_INFO_ID
                  ,GROUP_NAME
                  ,Legacy_Group_Name )
                  EXEC dbo.Group_Info_Sel_By_Group_Legacy 
                        @Group_Legacy_Name_Cd_Value = 'Invoice_Control' 

      INSERT      INTO @Group_Legacy_Name
                  ( 
                   GROUP_INFO_ID
                  ,GROUP_NAME
                  ,Legacy_Group_Name )
                  EXEC dbo.Group_Info_Sel_By_Group_Legacy 
                        @Group_Legacy_Name_Cd_Value = 'Regulated_Market';

      WITH  Cte_Account_Dtl
              AS ( SELECT
                        uacc.ACCOUNT_ID
                       ,uacc.ACCOUNT_TYPE_ID
                       ,uacc.SERVICE_LEVEL_TYPE_ID
                       ,s.PRIMARY_ADDRESS_ID Address_Id
                       ,uacc.VENDOR_ID
                   FROM
                        dbo.Account uacc
                        JOIN dbo.METER m
                              ON m.ACCOUNT_ID = uacc.ACCOUNT_ID
                        JOIN dbo.Site s
                              ON s.SITE_ID = uacc.SITE_ID
                   WHERE
                        uacc.ACCOUNT_ID = @account_id
                   UNION
                   SELECT
                        sacc.ACCOUNT_ID
                       ,sacc.ACCOUNT_TYPE_ID
                       ,sacc.SERVICE_LEVEL_TYPE_ID
                       ,m.ADDRESS_ID
                       ,uacc.VENDOR_ID
                   FROM
                        dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                        JOIN dbo.Account sacc
                              ON sacc.ACCOUNT_ID = samm.ACCOUNT_ID
                        JOIN dbo.METER m
                              ON m.METER_ID = samm.METER_ID
                        JOIN dbo.Account uacc
                              ON uacc.ACCOUNT_ID = m.ACCOUNT_ID
                   WHERE
                        sacc.ACCOUNT_ID = @account_id )
            SELECT
                  acc.account_id
                 ,acc.account_type_id
                 ,acc.service_level_type_id
                 ,map.analyst_id managed_by_id
                 ,ISNULL(ui.queue_id, uir.QUEUE_ID) queue_id
                 ,ui.first_name + SPACE(1) + ui.last_name managed_by
                 ,uir.user_info_id region_mgr_id
            FROM
                  Cte_Account_Dtl acc
                  JOIN dbo.Entity typ
                        ON typ.ENTITY_ID = acc.ACCOUNT_TYPE_ID
                  JOIN dbo.utility_detail ud
                        ON ud.vendor_id = acc.vendor_id
                  LEFT JOIN ( dbo.Utility_Detail_Analyst_Map map
                              JOIN dbo.group_info gi
                                    ON gi.group_info_id = map.Group_Info_ID
                              INNER JOIN @Group_Legacy_Name gil
                                    ON gi.Group_Info_ID = gil.GROUP_INFO_ID
                              JOIN dbo.user_info ui
                                    ON ui.user_info_id = map.analyst_id )
                              ON map.Utility_Detail_ID = ud.UTILITY_DETAIL_ID
                                 AND gil.Legacy_GROUP_NAME = CASE WHEN typ.Entity_Name = 'Supplier' THEN 'Invoice_Control'
                                                                  WHEN typ.Entity_Name = 'Utility' THEN 'Regulated_Market'
                                                             END
                  LEFT OUTER JOIN dbo.Address addr
                        ON addr.address_id = acc.address_id
                  LEFT OUTER JOIN dbo.state st
                        ON st.state_id = addr.state_id
                  LEFT OUTER JOIN dbo.regiON reg
                        ON reg.region_id = st.region_id
                  LEFT OUTER JOIN dbo.region_manager_map rmm
                        ON rmm.region_id = reg.region_id
                  LEFT OUTER JOIN dbo.user_info uir
                        ON uir.user_info_id = rmm.user_info_id

END

;
GO

GRANT EXECUTE ON  [dbo].[cbmsAccount_GetManagedBy] TO [CBMSApplication]
GO
