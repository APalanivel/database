SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsCPIndex_GetByIndex

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@clearport_index	varchar(200)	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE    PROCEDURE [dbo].[cbmsCPIndex_GetByIndex]
	( @clearport_index varchar(200) = null
	)
AS
BEGIN

	   select clearport_index_id
		, clearport_index
	     from clearport_index with (nolock)
	    where clearport_index = @clearport_index

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCPIndex_GetByIndex] TO [CBMSApplication]
GO
