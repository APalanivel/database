SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Invoice_Collection_Queue_Del_Invoice_Collection_Account_Config_Id         
                
Description:                
   This sproc is used to fill the ICQ Batch Table.        
                             
 Input Parameters:                
    Name										DataType   Default   Description                  
--------------------------------------------------------------------------------------                  
                         
   
 Output Parameters:                      
    Name        DataType   Default   Description                  
--------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
--------------------------------------------------------------------------------------     
  exec dbo.Invoice_Collection_Queue_Del_Invoice_Collection_Account_Config_Id 285,266
    
Author Initials:                
    Initials  Name                
--------------------------------------------------------------------------------------                  
 RKV    Ravi Kumar Vegesna  
 Modifications:                
    Initials        Date   Modification                
--------------------------------------------------------------------------------------                  
    RKV    2017-02-03  Created For Invoice_Collection.
    RKV    2017-04-14  Given space between status and to in Event_Desc column of Invoice_Collection_Queue_Event table
    RKV    2017-09-15  MAINT-5643, Added code to close the issues for the Archived ICQ's 
    RKV    2017-10-23  MAINT-6130, Added Filter on config dates and icq dates.
	RKV    2019-04-08  MAINT - 8212,Added Invoice_Collection_Queue_Id filter in all the joins
	RKV    2019-07-16  Ic - Added new table Exlcude Comments.
	RKV    2019-09-03  IC-Added functionlaity for two newly added actions Final Review and Next Action Date.
    RKV    2020-03-26  IC-Tool revamp Removed update  of ICConfig Start Date and End Date           
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Queue_Del_Invoice_Collection_Account_Config_Id]
     (
         @Invoice_Collection_Account_Config_Id INT
         , @User_Info_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;

        CREATE TABLE #Invoice_Collection_Queue
             (
                 Invoice_Collection_Queue_Id INT
             );

        CREATE TABLE #Account_Invoice_Collection_Month
             (
                 Account_Invoice_Collection_Month_Id INT PRIMARY KEY CLUSTERED
             );

        DECLARE
            @Icr_Archive_Cd INT
            , @Ice_Archive_Cd INT
            , @Issue_Close_cd INT;



        SELECT
            @Issue_Close_cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            c.Code_Value = 'Close'
            AND cs.Codeset_Name = 'IC Chase Status';



        SELECT
            @Icr_Archive_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            c.Code_Value = 'Archived'
            AND cs.Codeset_Name = 'ICR Status';

        SELECT
            @Ice_Archive_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            c.Code_Value = 'Archived'
            AND cs.Codeset_Name = 'ICE Status';


        DECLARE @icq_Account_Month_Ids TABLE
              (
                  Invoice_Collection_Queue_Id INT
                  , Account_Invoice_Collection_Month_Id INT
              );

        INSERT INTO @icq_Account_Month_Ids
             (
                 Invoice_Collection_Queue_Id
                 , Account_Invoice_Collection_Month_Id
             )
        SELECT
            icq.Invoice_Collection_Queue_Id
            , aicm.Account_Invoice_Collection_Month_Id
        FROM
            dbo.Invoice_Collection_Account_Config icac
            INNER JOIN dbo.Invoice_Collection_Queue icq
                ON icac.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
            INNER JOIN dbo.Invoice_Collection_Queue_Month_Map icqmm
                ON icq.Invoice_Collection_Queue_Id = icqmm.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Account_Invoice_Collection_Month aicm
                ON icqmm.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                dbo.Invoice_Collection_Account_Config icac1
                                INNER JOIN dbo.Invoice_Collection_Queue icq1
                                    ON icac.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
                           WHERE
                                (   icq1.Collection_Start_Dt BETWEEN icac1.Invoice_Collection_Service_Start_Dt
                                                             AND     icac1.Invoice_Collection_Service_End_Dt
                                    OR  icq1.Collection_End_Dt BETWEEN icac1.Invoice_Collection_Service_Start_Dt
                                                               AND     icac1.Invoice_Collection_Service_End_Dt
                                    OR  icac1.Invoice_Collection_Service_Start_Dt BETWEEN icq1.Collection_Start_Dt
                                                                                  AND     icq1.Collection_End_Dt
                                    OR  icac1.Invoice_Collection_Service_End_Dt BETWEEN icq1.Collection_Start_Dt
                                                                                AND     icq1.Collection_End_Dt)
                                AND icac1.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
                                AND icq.Invoice_Collection_Queue_Id = icq1.Invoice_Collection_Queue_Id)
            AND icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id;


        INSERT INTO #Account_Invoice_Collection_Month
             (
                 Account_Invoice_Collection_Month_Id
             )
        SELECT
            icqmm.Account_Invoice_Collection_Month_Id
        FROM
            @icq_Account_Month_Ids iami
            INNER JOIN dbo.Invoice_Collection_Queue_Month_Map icqmm
                ON iami.Account_Invoice_Collection_Month_Id = icqmm.Account_Invoice_Collection_Month_Id
        GROUP BY
            icqmm.Account_Invoice_Collection_Month_Id
        HAVING
            COUNT(icqmm.Account_Invoice_Collection_Month_Id) = 1;







        -- Collecting the ICQ which are to be deleted.
        INSERT INTO #Invoice_Collection_Queue
             (
                 Invoice_Collection_Queue_Id
             )
        SELECT
            icq.Invoice_Collection_Queue_Id
        FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN dbo.Invoice_Collection_Queue_Month_Map icqmm
                ON icq.Invoice_Collection_Queue_Id = icqmm.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Account_Invoice_Collection_Month aicm
                ON aicm.Account_Invoice_Collection_Month_Id = icqmm.Account_Invoice_Collection_Month_Id
            INNER JOIN @icq_Account_Month_Ids us
                ON us.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id
                   AND  us.Invoice_Collection_Queue_Id = icqmm.Invoice_Collection_Queue_Id
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                dbo.Code c
                                INNER JOIN dbo.Codeset cs
                                    ON c.Codeset_Id = cs.Codeset_Id
                           WHERE
                                c.Code_Value IN ( 'Resolved', 'Excluded' )
                                AND cs.Codeset_Name = 'ICE Status'
                                AND c.Code_Id = icq.Status_Cd)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Code c
                                    INNER JOIN dbo.Codeset cs
                                        ON c.Codeset_Id = cs.Codeset_Id
                               WHERE
                                    c.Code_Value IN ( 'Resolved', 'Excluded' )
                                    AND cs.Codeset_Name = 'ICR Status'
                                    AND c.Code_Id = icq.Status_Cd)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Invoice_Collection_Chase_Log_Queue_Map icclqm
                                    INNER JOIN dbo.Invoice_Collection_Chase_Log iccl
                                        ON icclqm.Invoice_Collection_Chase_Log_Id = iccl.Invoice_Collection_Chase_Log_Id
                                    INNER JOIN dbo.Code c
                                        ON c.Code_Id = iccl.Status_Cd
                               WHERE
                                    icclqm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                                    AND c.Code_Value = 'Close')
			 AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Invoice_Collection_Final_Review_Log_Queue_Map icfrlqm
                                    INNER JOIN dbo.Invoice_Collection_Final_Review_Log icfrl
                                        ON icfrlqm.Invoice_Collection_Final_Review_Log_Id = icfrl.Invoice_Collection_Final_Review_Log_Id
                                    INNER JOIN dbo.Code c
                                        ON c.Code_Id = icfrl.Status_Cd
                               WHERE
                                    icfrlqm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                                    AND c.Code_Value = 'Close')
			AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Invoice_Collection_Activity_Log_Queue_Map icalqm
                                    INNER JOIN dbo.Invoice_Collection_Activity_Log ical
                                        ON icalqm.Invoice_Collection_Activity_Log_Id = ical.Invoice_Collection_Activity_Log_Id
                                    
                               WHERE
                                    icalqm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                                    )
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Invoice_Collection_Issue_Log icil
                               WHERE
                                    icil.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Invoice_Collection_Exception_Comment icec
                               WHERE
                                    icec.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Invoice_Collection_Queue_Exclude_Comment icqec
                               WHERE
                                    icqec.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id)
            AND icq.Is_Manual = 0;



        BEGIN TRY
            BEGIN TRAN;
            INSERT INTO dbo.Invoice_Collection_Queue_Event
                 (
                     Invoice_Collection_Queue_Id
                     , Event_Desc
                     , Event_By_User_Id
                 )
            SELECT
                icq.Invoice_Collection_Queue_Id
                , 'Status updated from ' + c.Code_Value + ' to Archived for configuration Date change'
                , @User_Info_Id
            FROM
                dbo.Invoice_Collection_Queue icq
                INNER JOIN dbo.Code c
                    ON c.Code_Id = icq.Status_Cd
                INNER JOIN dbo.Invoice_Collection_Queue_Month_Map icqmm
                    ON icq.Invoice_Collection_Queue_Id = icqmm.Invoice_Collection_Queue_Id
                INNER JOIN dbo.Account_Invoice_Collection_Month aicm
                    ON aicm.Account_Invoice_Collection_Month_Id = icqmm.Account_Invoice_Collection_Month_Id
                INNER JOIN @icq_Account_Month_Ids us
                    ON us.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id
                       AND  us.Invoice_Collection_Queue_Id = icqmm.Invoice_Collection_Queue_Id
            WHERE
                NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #Invoice_Collection_Queue icq1
                               WHERE
                                    icq1.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id)
                AND icq.Is_Manual = 0;





            UPDATE
                icil
            SET
                Issue_Status_Cd = @Issue_Close_cd
            FROM
                dbo.Invoice_Collection_Issue_Log icil
                INNER JOIN dbo.Invoice_Collection_Queue icq
                    ON icil.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                INNER JOIN dbo.Invoice_Collection_Queue_Month_Map icqmm
                    ON icq.Invoice_Collection_Queue_Id = icqmm.Invoice_Collection_Queue_Id
                INNER JOIN dbo.Account_Invoice_Collection_Month aicm
                    ON aicm.Account_Invoice_Collection_Month_Id = icqmm.Account_Invoice_Collection_Month_Id
                INNER JOIN @icq_Account_Month_Ids us
                    ON us.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id
                       AND  us.Invoice_Collection_Queue_Id = icqmm.Invoice_Collection_Queue_Id
            WHERE
                NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #Invoice_Collection_Queue icq1
                               WHERE
                                    icq1.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id)
                AND icq.Is_Manual = 0;



            UPDATE
                icq
            SET
                Status_Cd = CASE WHEN icqt.Code_Value = 'ICR' THEN @Icr_Archive_Cd
                                ELSE @Ice_Archive_Cd
                            END
            FROM
                dbo.Invoice_Collection_Queue icq
                INNER JOIN dbo.Code c
                    ON c.Code_Id = icq.Status_Cd
                INNER JOIN dbo.Code icqt
                    ON icqt.Code_Id = icq.Invoice_Collection_Queue_Type_Cd
                INNER JOIN dbo.Invoice_Collection_Queue_Month_Map icqmm
                    ON icq.Invoice_Collection_Queue_Id = icqmm.Invoice_Collection_Queue_Id
                INNER JOIN dbo.Account_Invoice_Collection_Month aicm
                    ON aicm.Account_Invoice_Collection_Month_Id = icqmm.Account_Invoice_Collection_Month_Id
                INNER JOIN @icq_Account_Month_Ids us
                    ON us.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id
                       AND  us.Invoice_Collection_Queue_Id = icqmm.Invoice_Collection_Queue_Id
            WHERE
                NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #Invoice_Collection_Queue icq1
                               WHERE
                                    icq1.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id)
                AND icq.Is_Manual = 0;



            DELETE
            icclqm
            FROM
                dbo.Invoice_Collection_Chase_Log_Queue_Map icclqm
                INNER JOIN dbo.Invoice_Collection_Chase_Log iccl
                    ON icclqm.Invoice_Collection_Chase_Log_Id = iccl.Invoice_Collection_Chase_Log_Id
                INNER JOIN dbo.Code c
                    ON c.Code_Id = iccl.Status_Cd
                INNER JOIN dbo.Invoice_Collection_Queue icq
                    ON icq.Invoice_Collection_Queue_Id = icclqm.Invoice_Collection_Queue_Id
                INNER JOIN dbo.Invoice_Collection_Account_Config icac
                    ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            WHERE
                icclqm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                AND c.Code_Value = 'OPEN'
                AND icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id;

--Delete from Final REview Log
			DELETE
            icfrlqm
            FROM
                dbo.Invoice_Collection_Final_Review_Log_Queue_Map icfrlqm
                INNER JOIN dbo.Invoice_Collection_Final_Review_Log iccl
                    ON icfrlqm.Invoice_Collection_Final_Review_Log_Id = iccl.Invoice_Collection_Final_Review_Log_Id
                INNER JOIN dbo.Code c
                    ON c.Code_Id = iccl.Status_Cd
                INNER JOIN dbo.Invoice_Collection_Queue icq
                    ON icq.Invoice_Collection_Queue_Id = icfrlqm.Invoice_Collection_Queue_Id
                INNER JOIN dbo.Invoice_Collection_Account_Config icac
                    ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            WHERE
                icfrlqm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                AND c.Code_Value = 'OPEN'
                AND icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id;

	
            --Delete from invoice_Map
            DELETE
            aicmcim
            FROM
                dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map aicmcim
                INNER JOIN dbo.Account_Invoice_Collection_Month aicm
                    ON aicm.Account_Invoice_Collection_Month_Id = aicmcim.Account_Invoice_Collection_Month_Id
                INNER JOIN #Account_Invoice_Collection_Month us
                    ON us.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id;

            -- deleting ICQ Mapping
            DELETE
            icqmm
            FROM
                dbo.Invoice_Collection_Queue_Month_Map icqmm
                INNER JOIN dbo.Account_Invoice_Collection_Month aicm
                    ON aicm.Account_Invoice_Collection_Month_Id = icqmm.Account_Invoice_Collection_Month_Id
                INNER JOIN @icq_Account_Month_Ids us
                    ON us.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id
                       AND  us.Invoice_Collection_Queue_Id = icqmm.Invoice_Collection_Queue_Id;

            --Deleting the ICQ Table
            DELETE
            icq
            FROM
                dbo.Invoice_Collection_Queue icq
            WHERE
                EXISTS (   SELECT
                                1
                           FROM
                                #Invoice_Collection_Queue icq1
                           WHERE
                                icq1.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id);

            --Deleting the month IDs
            DELETE
            aicm
            FROM
                dbo.Account_Invoice_Collection_Month aicm
                INNER JOIN #Account_Invoice_Collection_Month us
                    ON us.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id;
            


            COMMIT TRAN;
        END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0
                ROLLBACK TRAN;

            EXEC dbo.usp_RethrowError;

        END CATCH;

        DROP TABLE #Invoice_Collection_Queue;

    END;






    ;




GO


GRANT EXECUTE ON  [dbo].[Invoice_Collection_Queue_Del_Invoice_Collection_Account_Config_Id] TO [CBMSApplication]
GO
