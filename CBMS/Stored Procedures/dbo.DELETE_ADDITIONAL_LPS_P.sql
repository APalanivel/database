SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE dbo.DELETE_ADDITIONAL_LPS_P
	@loadprofileSpecificationID INT
AS
BEGIN

	SET NOCOUNT ON

	DELETE FROM dbo.additional_charge
	WHERE load_profile_specification_id = @loadprofileSpecificationID

END
GO
GRANT EXECUTE ON  [dbo].[DELETE_ADDITIONAL_LPS_P] TO [CBMSApplication]
GO
