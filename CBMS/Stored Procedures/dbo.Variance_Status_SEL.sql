SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********       
NAME:  dbo.Variance_Status_SEL      
     
DESCRIPTION:  Used to select variance status      
    
INPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
      
            
        
OUTPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:       
    
 Variance_Status_SEL     
  
     
    
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
NK  Nageswara Rao Kosuri
    
Initials Date  Modification      
------------------------------------------------------------      
NK 10/29/2009  created    

******/      
    
CREATE PROCEDURE dbo.Variance_Status_SEL    
    
AS    
BEGIN    
     
 SET NOCOUNT ON;    
     
SELECT cd.Code_Id,    
 cd.Code_Dsc   
FROM dbo.CODE cd    
INNER JOIN dbo.Codeset cs     
ON cs.Codeset_Id = cd.CodeSet_Id    
WHERE cs.CodeSet_Name = 'VarianceStatus'      
 
 AND Is_Active = 1    
ORDER BY cd.Display_seq      
  
END 
GO
GRANT EXECUTE ON  [dbo].[Variance_Status_SEL] TO [CBMSApplication]
GO
