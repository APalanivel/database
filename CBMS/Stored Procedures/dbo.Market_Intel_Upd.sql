SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******      
 
NAME:    
 dbo.Market_Intel_Upd  
   
DESCRIPTION:    
  Used to Update data into market_intel table  
    
INPUT PARAMETERS:    
 Name    DataType  Default Description    
------------------------------------------------------------    
@Market_Intel_Id  INT  
@publish_date  DATETIME  
@market_intel_title VARCHAR(250)  
@market_intel  TEST  
@publication_cd  INT  
              
OUTPUT PARAMETERS:    
Name    DataType  Default Description    
------------------------------------------------------------    
  
USAGE EXAMPLES:    
------------------------------------------------------------  
   
 EXEC dbo.Market_Intel_Upd 12,500,'2009-05-29 10:21:00','Rate Change Notification - Pepco of D.C.','<P>Pepco is seeking a 6.1% increase in distribution components of rates. If approved,&nbsp;the change&nbsp;would take effect the first quarter, 2010. <span
 class=396423612-24042008><font face=Arial size=2>As specifics are discovered, a follow-up information will be posted.</font></span></P>'   
  
  
AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
GB  Geetansu Behera    
CMH  Chad Hattabaugh     
HG  Hari   
SKA  Shobhit Kr Agrawal  
   
   
MODIFICATIONS     
  
Initials Date  Modification    
------------------------------------------------------------    
GB      created  
SKA  08/07/09  Modified as per the coding standard  
  
******/  
  
  
CREATE PROCEDURE [dbo].[Market_Intel_Upd]
(  
 @Market_Intel_Id INT,  
 @publish_date DATETIME,  
 @market_intel_title VARCHAR(250),  
 @market_intel TEXT,  
 @publication_cd INT  
 )  
AS  
BEGIN  
	SET NOCOUNT ON  

	UPDATE dbo.MARKET_INTEL  
		SET	PUBLISH_DATE = @publish_date,  
			PUBLICATION_CD = @publication_cd,  
			MARKET_INTEL_TITLE = @market_intel_title,  
			MARKET_INTEL  = @market_intel  
	WHERE MARKET_INTEL_ID = @Market_Intel_Id  
  
END  
GO
GRANT EXECUTE ON  [dbo].[Market_Intel_Upd] TO [CBMSApplication]
GO
