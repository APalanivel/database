SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_BATCH_REPORT_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@batchId       	int       	          	
	@filter        	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE    PROCEDURE dbo.GET_BATCH_REPORT_DETAILS_P 
@userId varchar(10),
@sessionId varchar(20),
@batchId int,
@filter int
 AS
IF @filter  = 1 -- All batch  expected reports for a batch

begin
	SELECT 	
		expectedReport.reportType,
		expectedReport.reportName,
		expectedReport.reportListId,
		expectedReport.clientName,
		expectedReport.clientId,
		expectedReport.divisionName,
		expectedReport.divisionId,
		expectedReport.siteName,
		expectedReport.siteId,
		expectedReport.addr1,
		--expectedReport.logDate,
		expectedReport.status,
		@batchId batchId
	FROM
		(select   category.report_category_name reportType,
			list.report_name reportName,
			list.report_list_id reportListId,
			cli.client_name clientName,
			cli.client_id clientId,
			div.division_name divisionName,
			isnull(div.division_id,0) divisionId,
			vw.site_name siteName,
			isnull(sit.site_id,0) siteId,
			addr.address_line1 addr1,
			--null logDate,
			'Failure' status
		from 	rm_batch_configuration config,
			rm_batch_configuration_details details
			left join division div on (div.division_id = details.division_id)
			left join site sit on (sit.site_id = details.site_id)
			left join vwSiteName vw on(vw.site_id = sit.site_id)
			left join address addr on(addr.address_id = sit.primary_address_id),
			rm_report_batch batch,
			report_list list,
			report_category category,
			report_list_report_category_map map,
			client cli
		where 	config.rm_batch_configuration_master_id = batch.rm_batch_configuration_master_id
			and details.rm_batch_configuration_id = config.rm_batch_configuration_id
			and list.report_list_id = config.report_list_id
			and map.report_list_id = list.report_list_id
			and category.report_category_id = map.report_category_id
			and cli.client_id = config.client_id
			and batch.rm_report_batch_id = @batchId
		group by category.report_category_name,
			list.report_name,
			list.report_list_id,
			cli.client_name,
			cli.client_id,
			div.division_name,
			div.division_id,
			sit.site_id,
			vw.site_name,
			addr.address_line1,
			config.rm_batch_configuration_master_id
		UNION
		select 
			category.report_category_name reportType,
			list.report_name reportName,
			list.report_list_id reportListId,
			cli.client_name clientName,
			cli.client_id clientId,
			null divisionName,
			0 divisionId,
			null siteName,
			0 siteId,
			null,
			--null logDate,
			'Failure' status
	 
		from 
			rm_batch_configuration config, 
			rm_report_batch batch,
			report_list list,
			report_category category,
			report_list_report_category_map map,
			client cli

 
		where 
			config.rm_batch_configuration_master_id = batch.rm_batch_configuration_master_id
			and list.report_list_id = config.report_list_id
			and map.report_list_id = list.report_list_id
			and category.report_category_id = map.report_category_id
			and cli.client_id = config.client_id
			and config.corporate_report  = 1 -- boolean
			and batch.rm_report_batch_id =  @batchId

		group by 
			category.report_category_name,
			list.report_name,
			list.report_list_id,
			cli.client_name,
			cli.client_id,
			config.rm_batch_configuration_master_id


	) expectedReport
	WHERE   NOT EXISTS ( select 1 from
			(select category.report_category_name reportType,
				list.report_name reportName,
				list.report_list_id reportListId,
				cli.client_name clientName,
				cli.client_id clientId,
				div.division_name divisionName,
				isnull(div.division_id,0) divisionId,
				sit.site_name siteName,
				isnull(sit.site_id,0) siteId,
				null addr1,
				--batchLog.log_time logDate,
				'Success' status
			from 	rm_report_batch_log batchLog
				left join division div on(div.division_id = batchLog.division_id)
				left join site sit on(sit.site_id = batchLog.site_id),
				rm_report_batch batch,
				rm_report report,
				report_list list,
				client cli,
				report_category category,

				report_list_report_category_map map
			where 	batch.rm_report_batch_id = @batchId
				and batchLog.rm_report_batch_id = batch.rm_report_batch_id
				and report.rm_report_id = batchLog.rm_report_id
				and list.report_list_id = report.report_list_id
				and cli.client_id = batchLog.client_id
				and map.report_list_id = list.report_list_id
				and category.report_category_id = map.report_category_id
			group by category.report_category_name,
				list.report_name,
				list.report_list_id,
				cli.client_name,
				cli.client_id,
				div.division_name,
				div.division_id,
				sit.site_name,
				sit.site_id) batchReport
	WHERE 	
		expectedReport.reportName = batchReport.reportName
		and expectedReport.clientId = batchReport.clientId
		and expectedReport.divisionId = batchReport.divisionId
		and expectedReport.siteId = batchReport.siteId  )


UNION

	select category.report_category_name reportType,
		list.report_name reportName,
		list.report_list_id reportListId,
		cli.client_name clientName,
		cli.client_id clientId,
		div.division_name divisionName,
		isnull(div.division_id,0) divisionId,
		sit.site_name siteName,
		isnull(sit.site_id,0) siteId,
		null addr1,
		--batchLog.log_time logDate,
		'Success' status,
		@batchId batchId
	from 	rm_report_batch_log batchLog
		left join division div on(div.division_id = batchLog.division_id)
		left join site sit on(sit.site_id = batchLog.site_id),
		rm_report_batch batch,
		rm_report report,
		report_list list,
		client cli,
		report_category category,
		report_list_report_category_map map
	where 	batch.rm_report_batch_id = @batchId
		and batchLog.rm_report_batch_id = batch.rm_report_batch_id
		and report.rm_report_id = batchLog.rm_report_id
		and list.report_list_id = report.report_list_id
		and cli.client_id = batchLog.client_id
		and map.report_list_id = list.report_list_id
		and category.report_category_id = map.report_category_id
	group by category.report_category_name,
		list.report_name,
		list.report_list_id,
		cli.client_name,
		cli.client_id,
		div.division_name,
		div.division_id,
		sit.site_name,
		sit.site_id

end

ELSE IF  @filter  = 2 -- All Successfully created batch  reports for a batch

begin
	select category.report_category_name reportType,
		list.report_name reportName,
		list.report_list_id reportListId,
		cli.client_name clientName,
		cli.client_id clientId,
		div.division_name divisionName,
		isnull(div.division_id,0) divisionId,
		sit.site_name siteName,
		isnull(sit.site_id,0) siteId,
		null addr1,
		--batchLog.log_time logDate,
		'Success' status,
		@batchId batchId
	from 	rm_report_batch_log batchLog
		left join division div on(div.division_id = batchLog.division_id)
		left join site sit on(sit.site_id = batchLog.site_id),
		rm_report_batch batch,
		rm_report report,
		report_list list,
		client cli,
		report_category category,
		report_list_report_category_map map
	where 	batch.rm_report_batch_id = @batchId
		and batchLog.rm_report_batch_id = batch.rm_report_batch_id
		and report.rm_report_id = batchLog.rm_report_id
		and list.report_list_id = report.report_list_id
		and cli.client_id = batchLog.client_id
		and map.report_list_id = list.report_list_id
		and category.report_category_id = map.report_category_id
	group by category.report_category_name,
		list.report_name,
		list.report_list_id,
		cli.client_name,
		cli.client_id,
		div.division_name,
		div.division_id,
		sit.site_name,
		sit.site_id

end

ELSE IF  @filter  = 3 -- All failed batch  reports for a batch

begin
	SELECT 	
		expectedReport.reportType,
		expectedReport.reportName,
		expectedReport.reportListId,
		expectedReport.clientName,
		expectedReport.clientId,
		expectedReport.divisionName,
		expectedReport.divisionId,
		expectedReport.siteName,
		expectedReport.siteId,
		expectedReport.addr1,
		--expectedReport.logDate,
		expectedReport.status,
		@batchId batchId

	FROM

		(select   category.report_category_name reportType,
			list.report_name reportName,
			list.report_list_id reportListId,
			cli.client_name clientName,
			cli.client_id clientId,
			div.division_name divisionName,
			isnull(div.division_id,0) divisionId,
			vw.site_name siteName,
			isnull(sit.site_id,0) siteId,
			addr.address_line1 addr1,
			--null logDate,
			'Failure' status
		from 	rm_batch_configuration config,
			rm_batch_configuration_details details
			left join division div on (div.division_id = details.division_id)
			left join site sit on (sit.site_id = details.site_id)
			left join vwSiteName vw on(vw.site_id = sit.site_id)
			left join address addr on(addr.address_id = sit.primary_address_id),
			rm_report_batch batch,
			report_list list,
			report_category category,
			report_list_report_category_map map,
			client cli
		where 	config.rm_batch_configuration_master_id = batch.rm_batch_configuration_master_id
			and details.rm_batch_configuration_id = config.rm_batch_configuration_id
			and list.report_list_id = config.report_list_id
			and map.report_list_id = list.report_list_id
			and category.report_category_id = map.report_category_id
			and cli.client_id = config.client_id
			and batch.rm_report_batch_id = @batchId
		group by category.report_category_name,
			list.report_name,
			list.report_list_id,
			cli.client_name,
			cli.client_id,
			div.division_name,
			div.division_id,
			sit.site_id,
			vw.site_name,
			addr.address_line1,
			config.rm_batch_configuration_master_id
		UNION
		select 
			category.report_category_name reportType,
			list.report_name reportName,
			list.report_list_id reportListId,
			cli.client_name clientName,
			cli.client_id clientId,
			null divisionName,
			0 divisionId,
			null siteName,
			0 siteId,
			null,
			--null logDate,
			'Failure' status
	 
		from 
			rm_batch_configuration config, 
			rm_report_batch batch,
			report_list list,
			report_category category,
			report_list_report_category_map map,
			client cli

 
		where 
			config.rm_batch_configuration_master_id = batch.rm_batch_configuration_master_id
			and list.report_list_id = config.report_list_id
			and map.report_list_id = list.report_list_id
			and category.report_category_id = map.report_category_id
			and cli.client_id = config.client_id
			and config.corporate_report  = 1 -- boolean
			and batch.rm_report_batch_id =  @batchId

		group by 
			category.report_category_name,
			list.report_name,
			list.report_list_id,
			cli.client_name,
			cli.client_id,
			config.rm_batch_configuration_master_id


	) expectedReport
	WHERE   NOT EXISTS ( select 1 from
			(select category.report_category_name reportType,
				list.report_name reportName,
				list.report_list_id reportListId,
				cli.client_name clientName,
				cli.client_id clientId,
				div.division_name divisionName,
				isnull(div.division_id,0) divisionId,
				sit.site_name siteName,
				isnull(sit.site_id,0) siteId,
				null addr1,
				--batchLog.log_time logDate,
				'Success' status
			from 	rm_report_batch_log batchLog
				left join division div on(div.division_id = batchLog.division_id)
				left join site sit on(sit.site_id = batchLog.site_id),
				rm_report_batch batch,
				rm_report report,
				report_list list,
				client cli,
				report_category category,
				report_list_report_category_map map
			where 	batch.rm_report_batch_id = @batchId
				and batchLog.rm_report_batch_id = batch.rm_report_batch_id
				and report.rm_report_id = batchLog.rm_report_id
				and list.report_list_id = report.report_list_id
				and cli.client_id = batchLog.client_id
				and map.report_list_id = list.report_list_id
				and category.report_category_id = map.report_category_id
			group by category.report_category_name,
				list.report_name,
				list.report_list_id,
				cli.client_name,
				cli.client_id,
				div.division_name,

				div.division_id,

				sit.site_name,
				sit.site_id) batchReport
	WHERE 	
		expectedReport.reportName = batchReport.reportName
		and expectedReport.clientId = batchReport.clientId
		and expectedReport.divisionId = batchReport.divisionId
		and expectedReport.siteId = batchReport.siteId  )

end

GO
GRANT EXECUTE ON  [dbo].[GET_BATCH_REPORT_DETAILS_P] TO [CBMSApplication]
GO
