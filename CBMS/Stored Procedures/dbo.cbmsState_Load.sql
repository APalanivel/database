SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsState_Load

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@state_id      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE     PROCEDURE [dbo].[cbmsState_Load]
( 
	@MyAccountId int 
	, @state_id int
)
AS
BEGIN

	   select st.state_id
		, st.state_name
		, r.region_id
		, r.region_name
		, c.country_name
		, c.country_id
		,st.state_name as stateprovince_name
	     from state st
	     join region r on r.region_id = st.region_id
	     join country c on c.country_id = st.country_id
	    where st.state_id = @state_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsState_Load] TO [CBMSApplication]
GO
