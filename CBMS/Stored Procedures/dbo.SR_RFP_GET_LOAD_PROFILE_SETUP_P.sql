SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--exec dbo.SR_RFP_GET_LOAD_PROFILE_SETUP_P '1','1',70

CREATE    PROCEDURE dbo.SR_RFP_GET_LOAD_PROFILE_SETUP_P
	@user_id varchar(10),
	@session_id varchar(20),
	@rfp_id int
	AS
	set nocount on
	if( select count(sr_rfp_load_profile_setup_id) 
	    from sr_rfp_load_profile_setup (nolock)
	    where sr_rfp_id = @rfp_id ) > 0
	begin
		select	0 is_default,
			ven.vendor_id,
			ven.vendor_name,
			setup.sr_rfp_load_profile_setup_id,
			setup.sr_rfp_account_id,
			setup.additional_rows,
		       	setup.updated_month_selector_type_id as month_selector_type_id
		
		from	sr_rfp_load_profile_setup setup(nolock),
			sr_rfp_account rfp_acc(nolock),
			account acc(nolock),
			vendor ven(nolock)
		
		where 	setup.sr_rfp_id = @rfp_id
			and rfp_acc.sr_rfp_id = setup.sr_rfp_id
			and rfp_acc.sr_rfp_account_id = setup.sr_rfp_account_id
			and rfp_acc.is_deleted = 0
			and acc.account_id = rfp_acc.account_id
			and ven.vendor_id = acc.vendor_id
	
		order by ven.vendor_name

	end
	else
	begin
		select	
			1 is_default,
			ven.vendor_id,
			ven.vendor_name,
			setup.sr_load_profile_default_setup_id as sr_rfp_load_profile_setup_id,
			0 as sr_rfp_account_id,
			setup.additional_rows,
		       	setup.month_selector_type_id
		from
			sr_load_profile_default_setup setup(nolock),
			sr_rfp rfp(nolock),
			sr_rfp_account rfp_acc(nolock),
			account acc(nolock),
			vendor ven(nolock)
		
		where 
			rfp.sr_rfp_id = @rfp_id
			and rfp_acc.sr_rfp_id = rfp.sr_rfp_id
			and acc.account_id = rfp_acc.account_id
			and rfp_acc.is_deleted = 0
			and setup.vendor_id = acc.vendor_id
			and setup.commodity_type_id = rfp.commodity_type_id
			and ven.vendor_id = setup.vendor_id
			
		order by ven.vendor_name
	end
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_LOAD_PROFILE_SETUP_P] TO [CBMSApplication]
GO
