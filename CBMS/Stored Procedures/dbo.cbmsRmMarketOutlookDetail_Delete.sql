SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  procedure dbo.cbmsRmMarketOutlookDetail_Delete

( @AccountId int
, @OutlookDetailId int
)

AS
BEGIN
	set nocount on

	delete from rm_market_outlook_detail where rm_market_outlook_detail_id = @OutlookDetailId
	
END

	


GO
GRANT EXECUTE ON  [dbo].[cbmsRmMarketOutlookDetail_Delete] TO [CBMSApplication]
GO
