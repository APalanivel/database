SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.GET_CBMS_Image_Location_By_UserName

DESCRIPTION:

INPUT PARAMETERS:
	Name					DataType		Default	Description
---------------------------------------------------------------
	@UserName				VARCHAR

OUTPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	
USAGE EXAMPLES:
------------------------------------------------------------

	Exec GET_CBMS_Image_Location_By_UserName 'raimagesdevtk1'


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	MR			Meera R
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	MR       	2018-05-30	Created for getting CBMSImageLocation Id

******/
CREATE PROCEDURE [dbo].[GET_CBMS_Image_Location_By_UserName]
(
	@Username VARCHAR(50)
)
AS
BEGIN

	SELECT
		CBMS_Image_Location_Id
		,Active_File_Cnt
		,Max_File_Cnt
		,Active_Directory
		,Image_Drive
	FROM
		dbo.CBMS_Image_Location ci
		INNER JOIN Code cd 
			ON cd.Code_Id= ci.Storage_Type_Cd
	WHERE
		ci.UserName = @Username
		AND cd.Code_Value= 'Microsoft Azure storage'

END;






GO
GRANT EXECUTE ON  [dbo].[GET_CBMS_Image_Location_By_UserName] TO [CBMSApplication]
GO
