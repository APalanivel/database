
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
/******            
  
NAME: [DBO].[Cost_Usage_Account_Bucket_Value_Sel_By_Site_Bucket_Master_id]      
         
DESCRIPTION:     
    
 To Get bucket value for given bucket_master id from Cost_usage_Account_Dtl table.    
          
INPUT PARAMETERS:              
NAME    DATATYPE  DEFAULT  DESCRIPTION              
------------------------------------------------------------              
@Client_hier_Id INT  
@Service_Month  DATE    
@Bucket_Master_Id INT    
@Bucket_Type  VARCHAR(200)    
@Uom_Type_Id  INT   NULL    
@Currency_Unit_Id INT   NULL    
@Account_Type  VARCHAR(200) NULL     
          
OUTPUT PARAMETERS:              
NAME   DATATYPE DEFAULT  DESCRIPTION       
------------------------------------------------------------              
    
USAGE EXAMPLES:              
------------------------------------------------------------    
 EXEC Cost_Usage_Account_Bucket_Value_Sel_By_Site_Bucket_Master_id  17654,'2011-11-01', 100595,'Determinant', 12, 3, NULL    
 EXEC Cost_Usage_Account_Bucket_Value_Sel_By_Site_Bucket_Master_id  17654,'2009-05-01', 100589,'Charge', 12, 3, NULL    
   
 EXEC Cost_Usage_Account_Bucket_Value_Sel_By_Site_Bucket_Master_id  1131,'2010-08-01', 100990,'Determinant', 12, 3, NULL    
 EXEC Cost_Usage_Account_Bucket_Value_Sel_By_Site_Bucket_Master_id  1131,'2011-12-01', 100990,'Charge', 12, 3, NULL    
 EXEC Cost_Usage_Account_Bucket_Value_Sel_By_Site_Bucket_Master_id  1257,'2010-07-01', 100164,'Charge', 12, 3, NULL    
  
 EXEC Cost_Usage_Account_Bucket_Value_Sel_By_Site_Bucket_Master_id  528313,'2017-05-01', 100991,'Determinant',12, 3, NULL  
     
 SELECT * FROM bucket_Master WHERE Commodity_id = 290 AND is_SHown_On_Site = 1    
         
AUTHOR INITIALS:              
INITIALS   NAME              
------------------------------------------------------------              
HG    Harihara Suthan G    
AP    Athmaram Pabbathi    
RR    Raghu Reddy  
SP	  Sandeep Pigilam
    
MODIFICATIONS    
INITIALS   DATE     MODIFICATION    
------------------------------------------------------------    
HG    08/05/2011  Created for Additional data requirement    
AP    09/15/2011  Added new parameter @Account_Type to filter account list data    
RR    2012-04-09  Replaced input parameter site id with client hier id   
AP    2012-04-09  Modified final to filter data on Client_Hier_Id also
RKV   2013-06-13  Usage Examples Updated 
SP	  2017-06-23  Data Estimation,Added  Actual_Bucket_Value and Estimated_Bucket_Value in select list.  
*/  
CREATE PROCEDURE [dbo].[Cost_Usage_Account_Bucket_Value_Sel_By_Site_Bucket_Master_Id]
      ( 
       @Client_Hier_Id INT
      ,@Service_Month DATE
      ,@Bucket_Master_Id INT
      ,@Bucket_Type VARCHAR(200)
      ,@Uom_Type_Id INT = NULL
      ,@Currency_Unit_Id INT = NULL
      ,@Account_Type VARCHAR(200) = NULL )
AS 
BEGIN    
    
      SET NOCOUNT ON;    
    
   -- To save the account list of given site, as client_hier_account will have more than one row for account based on the no of meter added to it      
      DECLARE @Account_List TABLE
            ( 
             Account_Id INT PRIMARY KEY CLUSTERED
            ,Account_Type VARCHAR(200)
            ,Currency_Group_Id INT )      
      
      INSERT      INTO @Account_List
                  ( 
                   Account_Id
                  ,Account_Type
                  ,Currency_Group_Id )
                  SELECT
                        cha.Account_Id
                       ,cha.Account_Type
                       ,ch.Client_Currency_Group_Id
                  FROM
                        core.Client_hier ch
                        INNER JOIN core.Client_Hier_Account cha
                              ON cha.Client_hier_Id = ch.Client_Hier_Id
                  WHERE
                        ch.Client_Hier_Id = @Client_hier_Id
                        AND ( @Account_Type IS NULL
                              OR CHA.Account_Type = @Account_Type )
                  GROUP BY
                        cha.Account_Id
                       ,cha.Account_Type
                       ,ch.Client_Currency_Group_Id      
      
      SELECT
            al.Account_Id
           ,al.Account_Type
           ,CASE WHEN @Bucket_Type = 'Determinant' THEN cua.Bucket_Value * UomConv.Conversion_Factor
                 WHEN @Bucket_Type = 'Charge' THEN cua.Bucket_Value * CurConv.Conversion_Factor
            END AS Bucket_Value
           ,CASE WHEN @Bucket_Type = 'Determinant'
                      AND dt.Code_Value = 'Actual' THEN cua.Bucket_Value * UomConv.Conversion_Factor
                 WHEN @Bucket_Type = 'Charge'
                      AND dt.Code_Value = 'Actual' THEN cua.Bucket_Value * CurConv.Conversion_Factor
            END AS Actual_Bucket_Value
           ,CASE WHEN @Bucket_Type = 'Determinant'
                      AND dt.Code_Value = 'Estimated' THEN cua.Bucket_Value * UomConv.Conversion_Factor
                 WHEN @Bucket_Type = 'Charge'
                      AND dt.Code_Value = 'Estimated' THEN cua.Bucket_Value * CurConv.Conversion_Factor
            END AS Estimated_Bucket_Value
           ,dt.Code_Value AS Data_Type
      FROM
            dbo.Cost_Usage_Account_Dtl cua
            INNER JOIN @Account_List al
                  ON al.Account_Id = cua.Account_ID
            LEFT JOIN dbo.Code dt
                  ON cua.Data_Type_Cd = dt.Code_Id
            LEFT OUTER JOIN dbo.Consumption_Unit_Conversion UomConv
                  ON UomConv.Base_Unit_ID = cua.UOM_Type_Id
                     AND UomConv.Converted_Unit_ID = @UOM_Type_id
            LEFT OUTER JOIN dbo.Currency_Unit_Conversion CurConv
                  ON CurConv.Base_Unit_ID = cua.Currency_Unit_ID
                     AND CurConv.Converted_Unit_ID = @Currency_Unit_Id
                     AND curConv.Currency_Group_ID = al.Currency_Group_Id
                     AND curconv.CONVERSION_DATE = cua.Service_Month
      WHERE
            cua.Bucket_Master_Id = @Bucket_Master_Id
            AND cua.Service_Month = @Service_Month
            AND cua.Client_Hier_Id = @Client_Hier_Id  
    
END;
;

;
GO


GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Bucket_Value_Sel_By_Site_Bucket_Master_Id] TO [CBMSApplication]
GO
