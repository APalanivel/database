
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******************************************************************************************************      
NAME : [dbo].[CU_Upload_Validate_Sites_Utlity_Accounts]    
      
DESCRIPTION:      
Validates the specific fields in the xml      
--------------------------------------------------------------------      
INPUT PARAMETERS:      
@SiteAccountXML TEXT(UNTYPED XML STRING)    
@BucketMasterIds dbo.tvp_Bucket    
--------------------------------------------------------------------      
      
OUTPUT PARAMETERS:      
------------------------------------------------------------  
   
USAGE EXAMPLES:      
-----------------------------------------------------------      
      
DECLARE @BMI tvp_Bucket  
  
INSERT      @BMI  
            ( Bucket_Master_Id )  
VALUES  
            ( 100990 )  
      
EXEC [dbo].[CU_Upload_Validate_Sites_Utlity_Accounts]  
      '<?xml version="1.0" standalone="yes"?>  
      <CostUsage EnergyType="Electric Power"><ClientCostUsage ClientID="12068" Site="M?ridien" Account="" ServiceMonth="1/01/2008" /></CostUsage>'      
      
      
AUTHOR INITIALS:      
Initials Name          
-------------------------------------------------------------------      
BM   BIJU MOHAN      
RT   ROMY THOMAS      
AL   AJEESH L  
KVK  K VINAY KUMAR      
RK   Raghu Kalvapudi  
AC   Ajay Chejarla
  
MODIFICATIONS      
Initials Date Modification      
--------------------------------------------------------------------      
BM   12/-6/2010 Modified temporary table to add the field EP_NG_Both      
RT   02/28/2011 Restructured procedure to improve performance      
AL   05/25/2012 Removed EP_NG_Both column, added ClientHierId      
     06/01/2012 Account selectiion filtering with utility account    
KVK  06/14/2013 Added Is_Invoice Validation.    
RK   08/30/2013 Changed input data parameter type to XML.  
RK   09/09/2013 Added New table type parameter BucketMasterIds.    
RK   09/16/2013 Created from  [Validate_SitesNAccounts] (Copied Original Modification Comments too).  
RK   12/06/2013 Modified ClientID in the XML.  
KVK  05/22/2014 mimic the valdiation rules same as CBMS manual entry screen  
AC   7/7/2014   Modified Invoice validation to be same as CBMS screens
******************************************************************************************************/      
CREATE PROCEDURE [dbo].[CU_Upload_Validate_Sites_Utlity_Accounts]  
      (   
       @SiteAccountXML XML  
      ,@BucketMasterIds dbo.tvp_Bucket READONLY )  
AS   
BEGIN      
      DECLARE @xmlHandle INT;       
       
      CREATE TABLE #CostUsageTable  
            (   
             ClientID INT  
            ,EnergyType VARCHAR(1000)  
            ,[Site] VARCHAR(1000)  
            ,SiteId INT  
            ,Account VARCHAR(1000)  
            ,AccountId INT  
            ,ClientHierId INT  
            ,ServiceMonth DATE  
            ,IsInvoice BIT );      
      
      
      
      EXEC sp_xml_preparedocument   
            @XmlHandle OUTPUT  
           ,@SiteAccountXML;      
       
      BEGIN TRY      
            INSERT      INTO #CostUsageTable  
                        SELECT  
                              ClientID  
                             ,EnergyType  
                             ,[Site]  
                             ,SiteId  
                             ,Account  
                             ,AccountId  
                             ,ClientHierId  
                             ,ServiceMonth  
                             ,0  
                        FROM  
                              OPENXML (@XmlHandle, '/CostUsage/ClientCostUsage',1)      
      WITH      
      (      
       ClientID     INT'@ClientID'      
      , EnergyType    VARCHAR(1000)'../@EnergyType'      
      , [Site]     VARCHAR(1000)'@Site'      
      , SiteId     INT      
      , Account          VARCHAR(1000)'@Account'      
      , AccountId        INT      
      , ClientHierId     INT      
      , ServiceMonth     DATE'@ServiceMonth'      
      );      
      
      
            EXEC sp_xml_removedocument   
                  @xmlHandle;      
     
            DECLARE  
                  @Invoice_Cd INT  
 ,@Commodity_Id AS INT;      
        
            SELECT TOP 1  
                  @Commodity_Id = c.Commodity_Id  
            FROM  
                  dbo.Commodity c  
                  INNER JOIN #CostUsageTable cu  
                        ON cu.EnergyType = c.Commodity_Name;         
      
            SELECT  
                  @Invoice_Cd = c.Code_Id  
            FROM  
                  dbo.Code AS c  
                  JOIN dbo.Codeset AS c2  
                        ON c.Codeset_Id = c2.Codeset_Id  
            WHERE  
                  c2.Codeset_Name = 'DataSource'  
                  AND c.Code_Value = 'Invoice'      
  
     
         
            UPDATE  
                  cu  
            SET     
                  ClientHierId = ch.Client_Hier_Id  
                 ,SiteId = ch.SITE_ID  
            FROM  
                  #CostUsageTable AS cu  
                  INNER JOIN Core.Client_Hier ch  
                        ON ch.Client_ID = cu.ClientId  
                           AND ch.SITE_NAME = cu.[Site]  
                           AND ch.Site_Id > 0;         
       
      
           
  --Gets the AccountId based on Site AND/OR Account      
            UPDATE  
                  cu  
            SET     
                  AccountId = cha.account_id  
            FROM  
                  #CostUsageTable AS cu  
                  INNER JOIN Core.Client_Hier_Account cha  
                        ON ( cha.Client_Hier_Id = cu.ClientHierId  
                             AND cha.account_number = cu.Account )  
            WHERE  
                  ( cha.Commodity_Id = @Commodity_Id  
                    AND cha.Account_Type = 'Utility' );      
    
            UPDATE  
                  cu  
            SET     
                  cu.IsInvoice = 1  
            FROM  
                  #CostUsageTable cu  
            WHERE  
                  cu.AccountId IS NOT NULL  
                  AND ( EXISTS ( SELECT  
									1  
							  FROM  
									dbo.CU_INVOICE cui  
									JOIN dbo.CU_INVOICE_SERVICE_MONTH cusm  
										  ON cui.CU_INVOICE_ID = cusm.CU_INVOICE_ID  
									JOIN dbo.CU_INVOICE_DETERMINANT cuid  
										  ON cuid.CU_INVOICE_ID = cui.CU_INVOICE_ID  
							  WHERE  
									cusm.ACCOUNT_ID = cu.AccountId  
									AND cusm.SERVICE_MONTH = cu.ServiceMonth  
									AND cuid.COMMODITY_TYPE_ID = @Commodity_Id  
									AND cui.IS_PROCESSED = 1  
									AND cui.IS_REPORTED = 1  
									AND cui.IS_DNT = 0  
									AND cui.IS_DUPLICATE = 0 )  
						OR EXISTS ( SELECT  
										  1  
									FROM  
										  dbo.CU_INVOICE cui  
										  JOIN dbo.CU_INVOICE_SERVICE_MONTH cusm  
												ON cui.CU_INVOICE_ID = cusm.CU_INVOICE_ID  
										  JOIN dbo.CU_Invoice_Charge cuic  
												ON cuic.CU_INVOICE_ID = cui.CU_INVOICE_ID  
									WHERE  
										  cusm.ACCOUNT_ID = cu.AccountId   
										  AND cusm.SERVICE_MONTH = cu.ServiceMonth  
										  AND cuic.COMMODITY_TYPE_ID = @Commodity_Id  
										  AND cui.IS_PROCESSED = 1  
										  AND cui.IS_REPORTED = 1  
										  AND cui.IS_DNT = 0  
										  AND cui.IS_DUPLICATE = 0 ))   
                                
     
            UPDATE  
                  cu  
            SET     
                  cu.IsInvoice = 1  
            FROM  
                  #CostUsageTable cu  
            WHERE  
                  cu.AccountId IS NULL  
                  AND EXISTS ( SELECT  
                                    1  
                               FROM  
                                    dbo.Cost_Usage_Account_Dtl AS cuad  
                                    JOIN dbo.Bucket_Master bm  
                                          ON cuad.Bucket_Master_Id = bm.Bucket_Master_Id  
                               WHERE  
                                    cuad.Client_Hier_Id = cu.ClientHierId  
                                    AND cuad.Service_Month = cu.ServiceMonth  
                                    AND cuad.Data_Source_Cd = @Invoice_Cd  
                                    AND bm.Commodity_Id = @Commodity_Id )    
      
            SELECT  
                  ClientID  
                 ,EnergyType  
                 ,[Site]  
                 ,SiteId  
                 ,Account  
                 ,AccountId  
                 ,ClientHierId  
                 ,convert(VARCHAR(20), ServiceMonth, 101) AS ServiceMonth  
                 ,IsInvoice  
            FROM  
                  #CostUsageTable AS cut    
      END TRY      
      BEGIN CATCH      
            DECLARE @ErrorMessage NVARCHAR(4000);      
            DECLARE @ErrorSeverity INT;      
            DECLARE @ErrorState INT;       
            SELECT  
                  @ErrorMessage = error_message()  
                 ,@ErrorSeverity = error_severity()  
                 ,@ErrorState = error_state();      
         
            RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);       
           
      END CATCH      
          
END  
;
GO



GRANT EXECUTE ON  [dbo].[CU_Upload_Validate_Sites_Utlity_Accounts] TO [CBMSApplication]
GO
