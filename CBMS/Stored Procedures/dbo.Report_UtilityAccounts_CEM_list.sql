SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
	dbo.Report_UtilityAccounts_CEM_list
	
DESCRIPTION:
	This Procedure is to get the CEM List 
	
INPUT PARAMETERS:
Name			DataType		Default	Description
-------------------------------------------------------------------------------------------

	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------
Report_UtilityAccounts_CEM_list  
	
AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------
SSR			Sharad srivastava

MODIFICATIONS
Initials	Date		Modification
------------------------------------------------------------
 SSR	   09/07/2010	Created
*/
CREATE PROC [dbo].[Report_UtilityAccounts_CEM_list]
AS 
    BEGIN 
    
        SET NOCOUNT ON

        SELECT
            0 USER_INFO_ID
          , 'None' UserName
        UNION ALL
        SELECT
            uinfo.USER_INFO_ID
          , uinfo.FIRST_NAME + SPACE(1) + uinfo.LAST_NAME + SPACE(1) + '( '
            + uinfo.USERNAME + ')' UserName
        FROM
            dbo.Client_CEM_Map cemap
            JOIN dbo.USER_INFO uinfo
                ON uinfo.USER_INFO_ID = cemap.USER_INFO_ID
        GROUP BY
            uinfo.USER_INFO_ID
          , uinfo.FIRST_NAME
          , uinfo.LAST_NAME
          , uinfo.USERNAME
       Order by 
		  USER_INFO_ID,UserName
    END	


GO
GRANT EXECUTE ON  [dbo].[Report_UtilityAccounts_CEM_list] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_UtilityAccounts_CEM_list] TO [CBMSApplication]
GO
