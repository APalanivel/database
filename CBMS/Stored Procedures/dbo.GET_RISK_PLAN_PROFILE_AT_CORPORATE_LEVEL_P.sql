SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_RISK_PLAN_PROFILE_AT_CORPORATE_LEVEL_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@clientId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE  dbo.GET_RISK_PLAN_PROFILE_AT_CORPORATE_LEVEL_P
@userId varchar(10),
@sessionId varchar(20),
@clientId int
AS
set nocount on
select 
	documentType.entity_name, risk.cbms_image_id 
from 	RM_RISK_PLAN_PROFILE risk, entity documentType
where
 	risk.document_type_id = documentType.entity_id
        	and risk.document_type_id  !=  (select entity_id 
						from entity 
						where entity_type = 501
				      		and entity_name = 'Miscellaneous')

	and risk.document_level_type_id =  (select entity_id 
						from entity 
						where entity_type = 502
				      		and entity_name = 'Corporate')	
	and risk.client_id = @clientId
	and risk.site_id is null
	and risk.division_id is null
GO
GRANT EXECUTE ON  [dbo].[GET_RISK_PLAN_PROFILE_AT_CORPORATE_LEVEL_P] TO [CBMSApplication]
GO
