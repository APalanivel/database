SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
NAME:   
    dbo.Cost_Usage_Account_Billing_Dtl_Sel       

DESCRIPTION:  
	This procedure accepts account_id and service month as a input and returns the billing details.
      
INPUT PARAMETERS:          
Name			 DataType	Default	Description          
------------------------------------------------------------          
@Account_Id		 INT
@Service_Month	 DATE
              
OUTPUT PARAMETERS:          
Name			DataType	Default		Description          
------------------------------------------------------------ 

         
USAGE EXAMPLES:          
------------------------------------------------------------ 

Exec dbo.Cost_Usage_Account_Billing_Dtl_Sel 1,'2010-01-01'

AUTHOR INITIALS:          
Initials	Name          
------------------------------------------------------------          
AP			Athmaram Pabbathi
BCH			Balaraju     
  
MODIFICATIONS           
Initials	Date		    Modification          
------------------------------------------------------------          
AP			09/14/2011    Created
BCH			2012-04-16	  Removed @Commodity_Id parameter.

*****/ 
CREATE PROCEDURE dbo.Cost_Usage_Account_Billing_Dtl_Sel
      ( 
       @Account_Id INT
      ,@Service_Month DATE )
AS 
BEGIN
      SET NOCOUNT ON
   
      SELECT
            CUAB.Cost_Usage_Account_Billing_Dtl_Id
           ,CUAB.Billing_Start_Dt
           ,CUAB.Billing_End_Dt
           ,CUAB.Billing_Days
      FROM
            dbo.Cost_Usage_Account_Billing_Dtl CUAB
      WHERE
            CUAB.Account_Id = @Account_Id
            AND CUAB.Service_Month = @Service_Month
    
END
;
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Billing_Dtl_Sel] TO [CBMSApplication]
GO
