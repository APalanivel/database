SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_FORECAST_VOLUMES_FOR_CHART_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@fromDate      	varchar(12)	          	
	@toDate        	varchar(12)	          	
	@fromYear      	varchar(12)	          	
	@toYear        	varchar(12)	          	
	@clientId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE    PROCEDURE dbo.GET_FORECAST_VOLUMES_FOR_CHART_P
@userId varchar,
@sessionId varchar,
@fromDate Varchar(12),
@toDate Varchar(12),
@fromYear Varchar(12),
@toYear Varchar(12),
@clientId int
AS
set nocount on
select  CONVERT(VARCHAR(12), details.month_identifier, 101)  month_identifier, 
	sum(details.volume * conversion.CONVERSION_FACTOR) total_volume

from  	rm_forecast_volume_details details(NOLOCK), 
	rm_onboard_hedge_setup setup(NOLOCK),
	CONSUMPTION_UNIT_CONVERSION conversion(NOLOCK)

where	details.site_id = setup.site_id
	and details.rm_forecast_volume_id in (select rm_forecast_volume_id 
					     from   rm_forecast_volume(NOLOCK) 
					     where  forecast_year=@fromYear 
						    /*and forecast_as_of_date=(select MAX(FORECAST_AS_OF_DATE) 
							    from  RM_FORECAST_VOLUME
							    where FORECAST_YEAR =@fromYear AND
								  CLIENT_ID=@clientId)*/
 
						    and client_id = @clientId
						UNION
                                              select rm_forecast_volume_id 
					     from   rm_forecast_volume (NOLOCK)
					     where  forecast_year=@toYear 
						    /*and forecast_as_of_date=(select MAX(FORECAST_AS_OF_DATE) 
							    from  RM_FORECAST_VOLUME
							    where FORECAST_YEAR =@toYear AND
								  CLIENT_ID=@clientId)*/
 
						    and client_id = @clientId)

	and conversion.converted_unit_id = (select entity_id 
				       from   entity(NOLOCK)
                                       where  entity_type = 102 
					      and entity_name='MMBtu')

	and conversion.base_unit_id = setup.VOLUME_UNITS_TYPE_ID
	and details.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @fromDate, 101) AND CONVERT(Varchar(12), @toDate, 101)
group by month_identifier
GO
GRANT EXECUTE ON  [dbo].[GET_FORECAST_VOLUMES_FOR_CHART_P] TO [CBMSApplication]
GO
