SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.Budget_Detail_Comments_Changes_Sel_By_Audit_Ts

DESCRIPTION:

	Gets all Budget comment changes made between the Audit_Start_Ts and the Audit_End_Ts.
	

INPUT PARAMETERS:
	Name				DataType			Default	Description
------------------------------------------------------------
	@Audit_Start_Ts		DATETIME			Minimum Audit Start Time stamp
	@Audit_End_Ts		DATETIME            Maximum Audit End Time stamp

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	DECLARE @Cur_Ts DateTime = GETDATE()

	EXEC dbo.Budget_Detail_Comments_Changes_Sel_By_Audit_Ts '6/1/2010', @Cur_Ts

	SELECT
		bdc.*
	FROM
		Budget_Detail_Comments bdc
		JOIN Budget_Account ba ON ba.Budget_Account_id = bdc.Budget_Account_id
		JOIN Budget b ON b.Budget_Id = ba.Budget_id
	WHERE
		ba.Is_deleted = 0
		AND b.Is_Posted_To_Dv = 1

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	PNR			06/11/2010	Created
  	
******/

CREATE PROCEDURE dbo.Budget_Detail_Comments_Changes_Sel_By_Audit_Ts
(     
	 @Audit_Start_Ts DATETIME    
	,@Audit_End_Ts   DATETIME 
)    
AS     
BEGIN

	SET NOCOUNT ON;

	SELECT
		ba.ACCOUNT_ID
		,ba.Budget_Id
		,CASE WHEN (b.is_shown_comments_on_dv = 1 OR bdc.is_variable_on_dv = 1) THEN ISNULL(bdc.VARIABLE_COMMENTS, '') ELSE '' END  AS variable_comments
		,CASE WHEN (b.is_shown_comments_on_dv = 1 OR bdc.is_variable_on_dv = 1) THEN ISNULL(bdc.transportation_comments, '') ELSE '' END  AS transportation_comments
		,CASE WHEN (b.is_shown_comments_on_dv = 1 OR bdc.is_variable_on_dv = 1) THEN ISNULL(bdc.distribution_comments, '') ELSE '' END  AS distribution_comments
		,CASE WHEN (b.is_shown_comments_on_dv = 1 OR bdc.is_variable_on_dv = 1) THEN ISNULL(bdc.transmission_comments, '') ELSE '' END  AS transmission_comments
		,CASE WHEN (b.is_shown_comments_on_dv = 1 OR bdc.is_variable_on_dv = 1) THEN ISNULL(bdc.other_bundled_comments, '') ELSE '' END  AS other_bundled_comments
		,CASE WHEN (b.is_shown_comments_on_dv = 1 OR bdc.is_variable_on_dv = 1) THEN ISNULL(bdc.other_fixed_costs_comments, '') ELSE '' END  AS other_fixed_costs_comments
		,CASE WHEN (b.is_shown_comments_on_dv = 1 OR bdc.is_variable_on_dv = 1) THEN ISNULL(bdc.sourcing_tax_comments, '') ELSE '' END  AS sourcing_tax_comments
		,CASE WHEN (b.is_shown_comments_on_dv = 1 OR bdc.is_variable_on_dv = 1) THEN ISNULL(bdc.rates_tax_comments, '') ELSE '' END  AS rates_tax_comments
		,CASE WHEN (b.is_Shown_Comments_On_Dv = 1) THEN b.Comments ELSE '' END AS Budget_Comments
		,bdcadt.Audit_Function Audit_Function
	FROM
		dbo.Budget_Detail_Comments_Audit BdcAdt
		JOIN
		(
			SELECT
				 Budget_Detail_Comments_Id
				,MAX(Audit_Ts) Max_Audit_Ts
			FROM
				dbo.Budget_Detail_Comments_Audit
			WHERE
				Audit_Ts BETWEEN @Audit_Start_Ts
								AND @Audit_End_Ts
			GROUP BY
				Budget_Detail_Comments_Id

		) MaxAdt
			ON MaxAdt.Budget_Detail_Comments_Id = BdcAdt.Budget_Detail_Comments_Id
				  AND MaxAdt.Max_Audit_Ts = BdcAdt.Audit_Ts

		LEFT OUTER JOIN dbo.budget_detail_comments bdc
			ON bdc.BUDGET_DETAIL_COMMENTS_ID = BdcAdt.Budget_Detail_Comments_Id
		INNER JOIN dbo.budget_account ba
			ON ba.BUDGET_ACCOUNT_ID = BdcAdt.Budget_Account_Id
		INNER JOIN dbo.BUDGET b
				ON b.Budget_Id = ba.BUDGET_ID
	WHERE
		ba.is_deleted = 0
		AND b.IS_POSTED_TO_DV = 1

END
GO
GRANT EXECUTE ON  [dbo].[Budget_Detail_Comments_Changes_Sel_By_Audit_Ts] TO [CBMSApplication]
GO
