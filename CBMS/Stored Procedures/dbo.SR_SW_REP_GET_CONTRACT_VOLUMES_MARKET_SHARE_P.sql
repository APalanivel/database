SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- exec SR_SW_REP_GET_CONTRACT_VOLUMES_MARKET_SHARE_P '3516','',0,'07/01/2006','07/30/2006',0,0,648,0

CREATE PROCEDURE dbo.SR_SW_REP_GET_CONTRACT_VOLUMES_MARKET_SHARE_P
	@userId VARCHAR(10),
	@sessionId VARCHAR(50),
	@regionId INT,
	@fromDate VARCHAR(12),
	@toDate VARCHAR(12),
	@stateId INT,
	@utilityId INT,
	@supplierId INT,
	@analystId INT
AS
BEGIN

	SET NOCOUNT ON

	DECLARE @iscoprate INT
		, @Corporate_User INT
		, @privilege_type INT
		, @ng_commodity_type_id INT
		, @Supp_contract_type_id INT
		, @conv_Unit_id INT
	
	DECLARE @SQL VARCHAR(4000)
			, @GroupBy VARCHAR(200)

	SELECT @ng_commodity_type_id = entity_id FROM dbo.entity(NOLOCK) WHERE entity_name='Natural Gas' AND entity_type = 157
	SELECT @Supp_contract_type_id = entity_id FROM dbo.entity WHERE entity_name='Supplier' AND entity_type = 129
	SELECT @conv_Unit_id = entity_id FROM dbo.entity WHERE entity_type = 102 AND entity_name = 'MMBtu'


	SET @SQL ='SELECT ' + ''''+ 'Natural Gas' + ''''+  ' COMMODITY
			, utility.vendor_name AS UTILITY_NAME
			, state_name as STATE_NAME
			, REGION_NAME as REGION_NAME
			, ISNULL(CASE freq_type.entity_name	
				WHEN ' + ''''+ 'Monthly' + ''''+ ' THEN CAST(SUM (vol.volume * conversion.CONVERSION_FACTOR ) AS DECIMAL(20,2))
				WHEN ' + ''''+ 'Daily' + ''''+ ' THEN CAST( SUM (vol.volume * conversion.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, vol.month_identifier))),DATEADD(mm, 1, vol.month_identifier))) ) AS DECIMAL(20,2))
				WHEN ' + ''''+ '30-day Cycle' + ''''+ ' THEN CAST( SUM (vol.volume * conversion.CONVERSION_FACTOR * 30 ) AS DECIMAL(20,2))
				ELSE CAST( SUM (vol.volume * conversion.CONVERSION_FACTOR ) AS DECIMAL(20,2))
				END,0) VOLUME_SERVED 
			, COUNT( DISTINCT utilacc.account_id) AS ACCOUNTS_SERVED
		FROM dbo.account utilacc(NOLOCK)
			JOIN dbo.meter met(NOLOCK) ON met.account_id = utilacc.account_id
			JOIN dbo.supplier_account_meter_map map(NOLOCK) on map.meter_id = met.meter_id
			JOIN dbo.account suppacc(NOLOCK) on map.account_id = suppacc.account_id
			JOIN dbo.contract con(NOLOCK) on suppacc.account_id = con.account_id		
			JOIN dbo.contract_meter_volume vol(NOLOCK) on con.contract_id = vol.contract_id
				AND map.meter_id = vol.meter_id
				AND con.account_id = map.account_id
			JOIN dbo.entity freq_type(NOLOCK) on freq_type.entity_id  = vol.frequency_type_id
			JOIN dbo.CONSUMPTION_UNIT_CONVERSION conversion(NOLOCK) ON conversion.base_unit_id = vol.UNIT_TYPE_ID 
			JOIN dbo.vendor utility(NOLOCK) ON utility.vendor_id = utilacc.vendor_id
			JOIN dbo.vendor supplier(NOLOCK) ON supplier.vendor_id = suppacc.vendor_id
			JOIN dbo.vendor_state_map vendor_map(NOLOCK) ON vendor_map.vendor_id = utility.vendor_id
			JOIN dbo.state (NOLOCK) ON state.state_id = vendor_map.state_id
			
			JOIN dbo.region (NOLOCK) ON region.region_id=state.region_id
			JOIN dbo.UTILITY_DETAIL ON UTILITY_DETAIL.VENDOR_ID=utility.vendor_id
			JOIN dbo.UTILITY_ANALYST_MAP anaMap ON UTILITY_DETAIL.UTILITY_DETAIL_ID=anaMap.UTILITY_DETAIL_ID
			JOIN dbo.user_info ON user_info.user_info_id=anaMap.gas_analyst_id
		WHERE conversion.converted_unit_id = ' + STR(@conv_Unit_id)	+ '
			AND con.contract_type_id ='+ STR(@Supp_contract_type_id) + '
			AND vol.month_identifier BETWEEN CONVERT(VARCHAR(12),'+ ''''+  @fromDate + '''' +',101)  AND CONVERT(VARCHAR(12),'+ + '''' + @toDate + '''' + ',101) 
			AND con.commodity_type_id =' + STR(@ng_commodity_type_id)


		IF @regionId > 0
		 BEGIN
			SET @SQL = @SQL + ' AND region.REGION_ID = '+ STR(@regionId)
		
		 END

		IF @stateId > 0
		 BEGIN
			SET @SQL = @SQL + ' AND region.REGION_ID = '+ STR(@stateId)
		
		 END

		IF @utilityId > 0
		 BEGIN
			SET @SQL = @SQL + ' AND  utility.VENDOR_ID = '+ STR(@utilityId)
		
		 END

		IF @supplierId > 0
		 BEGIN

			SET @SQL = @SQL + ' AND supplier.vendor_id = '+ STR(@supplierId)

		 END

	SET @GroupBy ='
		GROUP BY supplier.vendor_name
			, utility.vendor_name
			, state.STATE_NAME
			, freq_type.entity_name
			, region_name
		ORDER BY utility.vendor_name '
	
	SET @SQL =@SQL + @GroupBy	
--	PRINT @sql	

	EXEC (@SQL)
	


END
GO
GRANT EXECUTE ON  [dbo].[SR_SW_REP_GET_CONTRACT_VOLUMES_MARKET_SHARE_P] TO [CBMSApplication]
GO
