
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
                  
NAME: [dbo].[User_Info_Group_Info_Map_Del_By_Client_Group]
                    
DESCRIPTION:                    
			 To Delete the User Info and Group info Mapped Records.                    
                    
INPUT PARAMETERS:    
                   
Name                              DataType            Default        Description    
---------------------------------------------------------------------------------------------------------------  
@Client_Id                      INT          
@Group_Info_Id_List				VARCHAR(MAX) 
                    
OUTPUT PARAMETERS:      
                      
Name                              DataType            Default        Description    
---------------------------------------------------------------------------------------------------------------  
                    
USAGE EXAMPLES:                        
---------------------------------------------------------------------------------------------------------------   
                       
			BEGIN TRAN      
			    
			SELECT
				Group_Info_Id,count(ui.user_info_id) as Users
			FROM
				dbo.User_Info_Group_Info_Map uigi
				INNER JOIN User_Info ui
					  ON uigi.User_info_id = ui.user_info_id
			WHERE
				ui.client_id = 235
				AND access_level = 1
				AND uigi.Group_Info_Id in (219)
			GROUP BY Group_Info_Id	
	            
			
			EXEC dbo.User_Info_Group_Info_Map_Del_By_Client_Group 
				@Client_Id = 235
			   ,@Group_Info_Id_List = '219'
	           
			SELECT
				Group_Info_Id,count(ui.user_info_id) as Users
			FROM
				dbo.User_Info_Group_Info_Map uigi
				INNER JOIN User_Info ui
					  ON uigi.User_info_id = ui.user_info_id
			WHERE
				ui.client_id = 235
				AND access_level = 1
				AND uigi.Group_Info_Id in (219)
		    GROUP BY Group_Info_Id			
	                            
			      
			ROLLBACK TRAN   


               
AUTHOR INITIALS:    
                  
Initials                Name    
---------------------------------------------------------------------------------------------------------------  
SP                      Sandeep Pigilam      
                     
MODIFICATIONS:   
                    
Initials                Date            Modification  
---------------------------------------------------------------------------------------------------------------  
 SP                     2013-11-27      Created for RA Admin user management     
 SP						2014-03-19		MAINT-2677  modified the sp to accept Deleted Groups and Removed unused column @Assigned_User_Id            
                   
******/         
                  
CREATE  PROCEDURE [dbo].[User_Info_Group_Info_Map_Del_By_Client_Group]
      ( 
       @Client_Id INT
      ,@Group_Info_Id_List VARCHAR(MAX) = NULL )
AS 
BEGIN                  
      SET NOCOUNT ON;    
      
      DECLARE
            @User_Info_Id INT
           ,@Group_Info_Id INT
           ,@Row_Counter INT
           ,@Total_Row_Count INT    
                  
      DECLARE @Group_Info_Id_tbl TABLE
            ( 
             Group_Info_Id INT PRIMARY KEY CLUSTERED )
              
      DECLARE @User_Info_Id_Tbl TABLE
            ( 
             User_Info_Id INT PRIMARY KEY CLUSTERED )          

      DECLARE @User_Info_Group_Info_Map_Del_List TABLE
            ( 
             User_Info_Id INT
            ,Group_Info_Id INT
            ,PRIMARY KEY CLUSTERED ( User_Info_Id, Group_Info_Id )
            ,Row_Num INT IDENTITY(1, 1) )                          
                
                
      INSERT      INTO @Group_Info_Id_tbl
                  ( 
                   Group_Info_Id )
                  SELECT
                        ufn.Segments
                  FROM
                        dbo.ufn_split(@Group_Info_Id_List, ',') ufn  
                     
      INSERT      INTO @User_Info_Id_Tbl
                  ( 
                   User_Info_Id )
                  SELECT
                        ui.USER_INFO_ID
                  FROM
                        dbo.USER_INFO ui
                  WHERE
                        CLIENT_ID = @Client_Id
                        AND ACCESS_LEVEL = 1


 
      INSERT      INTO @User_Info_Group_Info_Map_Del_List
                  ( 
                   User_Info_Id
                  ,Group_Info_Id )
                  SELECT
                        uigi.User_Info_Id
                       ,uigi.Group_Info_Id
                  FROM
                        dbo.USER_INFO_GROUP_INFO_MAP uigi
                        INNER JOIN @User_Info_Id_Tbl
                              ON uigi.USER_INFO_ID = [@User_Info_Id_Tbl].User_Info_Id
                        INNER JOIN @Group_Info_Id_tbl gl
                              ON gl.Group_Info_Id = uigi.GROUP_INFO_ID


      BEGIN TRY                      
            BEGIN TRANSACTION   
            
            SELECT
                  @Total_Row_Count = MAX(Row_Num)
            FROM
                  @User_Info_Group_Info_Map_Del_List    
                        
            SET @Row_Counter = 1     
          
            WHILE ( @Row_Counter <= @Total_Row_Count ) 
                  BEGIN            
                        SELECT
                              @User_Info_Id = User_Info_Id
                             ,@GROUP_INFO_ID = GROUP_INFO_ID
                        FROM
                              @User_Info_Group_Info_Map_Del_List
                        WHERE
                              Row_Num = @Row_Counter            
            
                        EXEC dbo.USER_INFO_GROUP_INFO_MAP_Del 
                              @GROUP_INFO_ID = @GROUP_INFO_ID
                             ,@User_Info_Id = @User_Info_Id

            
                        SET @Row_Counter = @Row_Counter + 1            
                  END    
                          
                                   
                               
            COMMIT TRANSACTION                               
                                 
                                 
      END TRY                  
      BEGIN CATCH                  
            IF @@TRANCOUNT > 0 
                  BEGIN  
                        ROLLBACK TRANSACTION  
                  END                 
            EXEC dbo.usp_RethrowError                  
      END CATCH                                
                         
END;




;
GO

GRANT EXECUTE ON  [dbo].[User_Info_Group_Info_Map_Del_By_Client_Group] TO [CBMSApplication]
GO
