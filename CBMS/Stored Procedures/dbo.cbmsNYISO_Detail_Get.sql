SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO






CREATE  PROCEDURE [dbo].[cbmsNYISO_Detail_Get]
	( @zone_detail_id int
	)
AS
BEGIN

	   select zd.zone_detail_id
		, zd.zone_id
		, zd.date
		, zd.ptid
		, zd.lbmp
		, zd.marginal_cost_losses
		, zd.marginal_cost_congestion
		, zd.pricing_type
		, zd.scarcity
		, z.zone_name
	     from ny_iso_detail zd
   left outer join ny_iso z on z.zone_id = zd.zone_id
	    where zone_detail_id = @zone_detail_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsNYISO_Detail_Get] TO [CBMSApplication]
GO
