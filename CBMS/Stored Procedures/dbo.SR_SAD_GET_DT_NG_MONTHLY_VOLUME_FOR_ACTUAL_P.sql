
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.SR_SAD_GET_DT_NG_MONTHLY_VOLUME_FOR_ACTUAL_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@From_Month    DATETIME  	          	
	@Account_ID   	INT       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
    EXEC SR_SAD_GET_DT_NG_MONTHLY_VOLUME_FOR_ACTUAL_P '01/01/2011', 38009

AUTHOR INITIALS:
    Initials	Name
------------------------------------------------------------
    AP		Athmaram Pabbathi

MODIFICATIONS
    Initials	 Date	   Modification
------------------------------------------------------------
	        	 09/21/2010  Modify Quoted Identifier
    DMR		 09/10/2010	   Modified for Quoted_Identifier
    AP		 08/03/2011  Removed Cost_Usage table and used Cost_Usage_Account_Dtl, Bucket_Master, Commodity tables
    AP		 08/22/2011  Added qualifiers to all objects with the owner name.
    AP		 08/23/2011  Removed NOLOCK & multiple IF statments and modified the logic to handle in single select statment; removed Entity_Type = 101 
					    and added filter on Entity_Description
    AP		 09/22/2011  Removed hardcoded bucket name and used dbo.Cost_usage_Bucket_Sel_By_Commodity instead.
******/
CREATE PROCEDURE [dbo].[SR_SAD_GET_DT_NG_MONTHLY_VOLUME_FOR_ACTUAL_P]
      ( 
       @From_Month DATETIME
      ,@Account_ID INT )
AS 
BEGIN
      SET NOCOUNT ON

      DECLARE
            @Volume DECIMAL(32, 16)
           ,@Converted_Unit_ID INT
           ,@Commodity_ID INT
           ,@Current_Yr_Volume DECIMAL(32, 16)
           ,@Previous_Yr_Volume DECIMAL(32, 16)
           ,@BeforePrev_Yr_Volume DECIMAL(32, 16)

      DECLARE @Cost_Usage_Bucket_Id TABLE
            ( 
             Bucket_Master_Id INT PRIMARY KEY CLUSTERED
            ,Bucket_Name VARCHAR(200)
            ,Bucket_Type VARCHAR(25) )

      SELECT
            @Converted_Unit_ID = uom.Entity_ID
      FROM
            dbo.Entity uom
      WHERE
            uom.Entity_Description = 'Unit for Gas'
            AND uom.Entity_Name = 'MMBtu'


      SELECT
            @Commodity_ID = com.Commodity_ID
      FROM
            dbo.Commodity com
      WHERE
            com.Commodity_Name = 'Natural Gas'

      INSERT      INTO @Cost_Usage_Bucket_Id
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Bucket_Type )
                  EXEC dbo.Cost_usage_Bucket_Sel_By_Commodity 
                        @Commodity_id = @Commodity_Id 
    
      SELECT
            @Current_Yr_Volume = sum(case WHEN CUAD.Service_Month = @From_Month THEN CUAD.Bucket_Value * UC.conversion_factor
                                          ELSE 0
                                     END)
           ,@Previous_Yr_Volume = sum(case WHEN CUAD.Service_Month = dateadd(yy, -1, @From_Month) THEN CUAD.Bucket_Value * UC.conversion_factor
                                           ELSE 0
                                      END)
           ,@BeforePrev_Yr_Volume = sum(case WHEN CUAD.Service_Month = dateadd(yy, -2, @From_Month) THEN CUAD.Bucket_Value * UC.conversion_factor
                                             ELSE 0
                                        END)
      FROM
            dbo.Cost_Usage_Account_Dtl CUAD
            INNER JOIN @Cost_Usage_Bucket_Id CUB
                  ON CUB.Bucket_Master_Id = CUAD.Bucket_Master_Id
            INNER JOIN dbo.Consumption_Unit_Conversion UC
                  ON UC.Base_Unit_ID = CUAD.UOM_Type_Id
      WHERE
            CUAD.Account_ID = @Account_ID
            AND UC.Converted_Unit_ID = @Converted_Unit_ID
            AND ( CUAD.Service_Month = @From_Month
                  OR CUAD.Service_Month = dateadd(yy, -1, @From_Month)
                  OR CUAD.Service_Month = dateadd(yy, -2, @From_Month) )
            AND CUB.Bucket_Type = 'Determinant'
            
      SET @Volume = isnull(@Current_Yr_Volume, 0)
	 
      SELECT
            @Volume = isnull(@Previous_Yr_Volume, 0)
      WHERE
            @Volume <= 0 

      SELECT
            @Volume = isnull(@BeforePrev_Yr_Volume, 0)
      WHERE
            @Volume <= 0 

      SELECT
            @Volume AS Volume
            
END
;
GO

GRANT EXECUTE ON  [dbo].[SR_SAD_GET_DT_NG_MONTHLY_VOLUME_FOR_ACTUAL_P] TO [CBMSApplication]
GO
