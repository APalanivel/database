SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE dbo.GET_All_PRICEINDEX_P
	@entityType int
	AS
	begin
	set nocount on

		select  entity_id, 
			entity_name 
		from 	entity 
		where 	entity_type = @entityType

	end


GO
GRANT EXECUTE ON  [dbo].[GET_All_PRICEINDEX_P] TO [CBMSApplication]
GO
