SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME: [DBO].[Sr_Rfp_Archive_Meter_Contracts_Del]
    
DESCRIPTION:

	It Deletes SR RFP Archive Meter Contracts for Selected Sr RFP Archive Meter Contracts Id.
      
INPUT PARAMETERS:          
NAME								DATATYPE	DEFAULT		DESCRIPTION          
-------------------------------------------------------------------          
@Sr_Rfp_Archive_Meter_Contracts_Id	INT

   
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  
	Begin Tran
		EXEC Sr_Rfp_Archive_Meter_Contracts_Del  15747
	Rollback Tran
    
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR		PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR		27-MAY-10		CREATED     

*/  

CREATE PROCEDURE dbo.Sr_Rfp_Archive_Meter_Contracts_Del
   (
    @Sr_Rfp_Archive_Meter_Contracts_Id INT
   )
AS
BEGIN

    SET NOCOUNT ON;
   
    DELETE	
		dbo.SR_RFP_ARCHIVE_METER_CONTRACTS
	WHERE
		SR_RFP_ARCHIVE_METER_CONTRACTS_ID = @Sr_Rfp_Archive_Meter_Contracts_Id

END
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Archive_Meter_Contracts_Del] TO [CBMSApplication]
GO
