SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                            
 NAME: dbo.Invoice_Collection_LOA_Sel                
                            
 DESCRIPTION:                            
                  To get the details of invoice collection Letter of authorities.            
                            
 INPUT PARAMETERS:              
                         
 Name                        DataType         Default       Description            
---------------------------------------------------------------------------------------------------------------          
@Invoice_Collection_Client_Config_Id      INT               NULL    
    
                                
 OUTPUT PARAMETERS:              
                               
 Name                        DataType         Default       Description            
---------------------------------------------------------------------------------------------------------------          
                            
 USAGE EXAMPLES:                                
---------------------------------------------------------------------------------------------------------------                                
    
--Client Level    
    
  EXEC [dbo].[Invoice_Collection_LOA_Sel]     
      @Invoice_Collection_Client_Config_Id = 1,@Client_Id=2    
          
    
       
  EXEC [dbo].[Invoice_Collection_LOA_Sel]     
      @Invoice_Collection_Client_Config_Id = 1       
                        
                           
 AUTHOR INITIALS:            
           
 Initials              Name            
---------------------------------------------------------------------------------------------------------------                          
 SP      Sandeep Pigilam      
 NR      Narayana Reddy  
 MN      Nagaraju Muppa  
                             
 MODIFICATIONS:          
              
 Initials              Date             Modification          
---------------------------------------------------------------------------------------------------------------          
 SP                    2016-11-21       Created for Invoice Tracking      
 NR                    2017-04-26       SE2017-51 - Added Client_name in output List.    
 HG      2018-07-19  Post Prodn release regression issue fix. Multiple client id handled.   
   
 MN                    2020-01-20       MAINT-9532 - Updated inner join with LEFT outer Join   
						and added null validation condition into columns to display   
						the LOA Startdate and End Date if Image missed unexpectedly.    
******/    
CREATE PROCEDURE [dbo].[Invoice_Collection_LOA_Sel]    
      (    
       @Invoice_Collection_Client_Config_Id INT = NULL    
      ,@Client_Id VARCHAR(MAX) = NULL    
      ,@Start_Index INT = 1    
      ,@End_Index INT = 2147483647    
      ,@Total_Count INT = 0 )    
AS    
BEGIN                    
      SET NOCOUNT ON;    
          
      DECLARE @SQL_Statement NVARCHAR(MAX)  = ''    
                
      SELECT    
            @SQL_Statement = N';WITH CTE_Invoice_Collection_LOA_Sel AS    
(    
SELECT   top  ' + CAST(@End_Index AS NVARCHAR(MAX)) + '  ROW_NUMBER() OVER(ORDER BY icli.Invoice_Collection_LOA_Image_Id) AS Row_Num    
           ,icl.Invoice_Collection_LOA_Id    
           ,icl.Seq_No    
           ,icl.LOA_Valid_From_Dt    
           ,icl.LOA_Valid_End_Dt    
           ,ISNULL(icli.Invoice_Collection_LOA_Image_Id,0) Invoice_Collection_LOA_Image_Id  
           ,ISNULL(icli.Cbms_Image_Id,0) Cbms_Image_Id  
           ,ISNULL(icli.LOA_Description,'''') LOA_Description  
           ,ISNULL(ci.CBMS_DOC_ID,0)  CBMS_DOC_ID  
           ,ch.Client_Name    
           ,COUNT(1) OVER () Total_Count     
      FROM    
            dbo.Invoice_Collection_LOA icl    
            LEFT OUTER JOIN dbo.Invoice_Collection_LOA_Image icli    
                  ON icl.Invoice_Collection_LOA_Id = icli.Invoice_Collection_LOA_Id    
            LEFT OUTER JOIN dbo.cbms_image ci    
                  ON icli.Cbms_Image_Id = ci.CBMS_IMAGE_ID    
            INNER JOIN dbo.Invoice_Collection_Client_Config iccc 
					ON iccc.Invoice_Collection_Client_Config_Id = icl.Invoice_Collection_Client_Config_Id    
            INNER JOIN core.Client_Hier ch    
                  ON iccc.Client_Id = ch.Client_Id    
                     AND ch.Sitegroup_Id = 0    
      WHERE    
            ( ' + CASE WHEN @Invoice_Collection_Client_Config_Id IS NULL THEN '1 = 1 '    
                       ELSE +'    
               icl.Invoice_Collection_Client_Config_Id = ' + CAST(ISNULL(@Invoice_Collection_Client_Config_Id, 0) AS NVARCHAR(100))    
                  END + ' ) ' + CASE WHEN @Client_Id IS NULL THEN ''    
                                     ELSE +'    
                 AND ( EXISTS ( SELECT    
                                    1    
                              FROM    
                                    dbo.ufn_split(' + '''' + @Client_Id + '''' + ', '','') ufn    
                              WHERE    
                                    ufn.Segments = iccc.Client_Id ) )'    
                                END + ')    
         SELECT  Invoice_Collection_LOA_Id    
           ,Seq_No    
           ,LOA_Valid_From_Dt    
           ,LOA_Valid_End_Dt    
           ,Invoice_Collection_LOA_Image_Id    
           ,Cbms_Image_Id    
           ,LOA_Description    
           ,CBMS_DOC_ID    
           ,Client_Name     
           ,Total_Count    
               
           FROM CTE_Invoice_Collection_LOA_Sel         
          WHERE    
           Row_Num BETWEEN ' + CAST(@Start_Index AS VARCHAR(MAX)) + N' AND ' + CAST(@End_Index AS VARCHAR(MAX)) + N'                             
      ORDER BY    
            Seq_No'                
   
      EXECUTE  (@SQL_Statement)       
END;
GO

GRANT EXECUTE ON  [dbo].[Invoice_Collection_LOA_Sel] TO [CBMSApplication]
GO
