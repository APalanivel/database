SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Invoice_Participation_History_Del_For_Account]  
     
DESCRIPTION: 

	To delete Invoice Participation history associated with the given Account Id.
		This procedure will delete/update the records from following list of tables
			- Invoice_Participation_queue deleted
			- Invoice_Participation deleted
			- Invoice_Participation_Site updated.

INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Account_Id		INT						

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	BEGIN TRAN
	
		EXEC Invoice_Participation_History_Del_For_Account  289629
	ROLLBACK TRAN


AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			06/08/2010	Created
*/

CREATE PROCEDURE dbo.Invoice_Participation_History_Del_For_Account
	 @Account_Id		INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE	@Invoice_Participation_Queue_Id		INT
		   ,@Invoice_Participation_Site_Id		INT
		   ,@Site_Id							INT
		   ,@Service_Month						DATETIME

	DECLARE @Invoice_Participation_List TABLE(Account_Id INT, Site_Id INT,Service_Month DATETIME, PRIMARY KEY CLUSTERED(Site_id, Service_Month, Account_Id));
	DECLARE @Invoice_Participation_Queue_List TABLE(Invoice_Participation_Queue_Id INT PRIMARY KEY CLUSTERED);

	INSERT INTO @Invoice_Participation_List
	(
		Account_Id
		,Site_Id
		,Service_Month
	)
	SELECT
		@Account_Id
		,SITE_ID
		,SERVICE_MONTH
	FROM
		dbo.INVOICE_PARTICIPATION
	WHERE
		ACCOUNT_ID = @Account_Id

	INSERT INTO @Invoice_Participation_Queue_List
	(
		 Invoice_Participation_Queue_Id
	)
	SELECT
		Invoice_Participation_Queue_Id
	FROM
		dbo.INVOICE_PARTICIPATION_QUEUE
	WHERE
		ACCOUNT_ID = @Account_Id

	BEGIN TRY
		BEGIN TRAN

				WHILE EXISTS(SELECT 1 FROM @Invoice_Participation_List)
				BEGIN
			
					SELECT TOP 1
						@Site_Id = Site_Id
						, @Service_Month = Service_Month
					FROM
						@Invoice_Participation_List

					EXEC dbo.Invoice_Participation_Del @Account_Id,@Site_Id,@Service_Month

					EXEC dbo.cbmsInvoiceParticipationSite_Save -1,@Site_Id,@Service_Month -- To reset Site level invoice participation.

					DELETE
						@Invoice_Participation_List
					WHERE
						Account_Id = @Account_Id
						AND Site_Id = @Site_Id
						AND Service_Month = @Service_Month

				END

				WHILE EXISTS(SELECT 1 FROM @Invoice_Participation_Queue_List)
				BEGIN
				
					SET @Invoice_Participation_Queue_Id = (SELECT TOP 1 Invoice_Participation_Queue_Id FROM @Invoice_Participation_Queue_List)

					EXEC dbo.Invoice_Participation_Queue_Del @Invoice_Participation_Queue_Id

					DELETE
						@Invoice_Participation_Queue_List
					WHERE
						Invoice_Participation_Queue_Id = @Invoice_Participation_Queue_Id

				END

		COMMIT TRAN
	END TRY

	BEGIN CATCH
    	IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRAN
		END
		
		EXEC dbo.usp_RethrowError

	END CATCH

END
GO
GRANT EXECUTE ON  [dbo].[Invoice_Participation_History_Del_For_Account] TO [CBMSApplication]
GO
