SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Pricing_Product_Sel_By_Commodity_Country_List]
           
DESCRIPTION:             
			To get SR pricing products mapped to selected commodity and all the countries
			
INPUT PARAMETERS:            
	Name			DataType		Default		Description  
---------------------------------------------------------------------------------  
	@Country_Id		VARCHAR(MAX)
    @Commodity_Id	INT			


OUTPUT PARAMETERS:
	Name			DataType		Default		Description  
---------------------------------------------------------------------------------  


 USAGE EXAMPLES:
---------------------------------------------------------------------------------  

	SELECT spp.SR_PRICING_PRODUCT_ID,spp.PRODUCT_NAME,spp.COMMODITY_TYPE_ID,cspp.Country_ID
		FROM dbo.SR_Pricing_Product_Country_Map cspp JOIN dbo.SR_PRICING_PRODUCT spp 
		ON cspp.SR_PRICING_PRODUCT_ID = spp.SR_PRICING_PRODUCT_ID 
		WHERE spp.SR_PRICING_PRODUCT_ID IN (155, 165, 175)
	
	EXEC dbo.Sr_Pricing_Product_Sel_By_Commodity_Country_List 290,'4,6,9'
	EXEC dbo.Sr_Pricing_Product_Sel_By_Commodity_Country_List 290,'4,6,9,84'
	
	EXEC dbo.Sr_Pricing_Product_Sel_By_Commodity_Country_List 291,'32,39'
	EXEC dbo.Sr_Pricing_Product_Sel_By_Commodity_Country_List 291,'32,39,14'

 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-02-17	Global Sourcing - Phase3 - GCS-448 - Created
								
******/
CREATE PROCEDURE [dbo].[Sr_Pricing_Product_Sel_By_Commodity_Country_List]
      ( 
       @Commodity_Id INT
      ,@Country_Id VARCHAR(MAX) )
AS 
BEGIN

      SET NOCOUNT ON;
      DECLARE @Country_Count AS INT
      
      SELECT
            @Country_Count = count(1)
      FROM
            dbo.ufn_split(@Country_Id, ',');
            
      WITH  Cte_Products
              AS ( SELECT
                        srpr.SR_PRICING_PRODUCT_ID
                       ,srpr.PRODUCT_NAME
                       ,sppcm.Country_ID
                       ,rank() OVER ( PARTITION BY srpr.SR_PRICING_PRODUCT_ID ORDER BY sppcm.Country_ID ) AS Country_Count
                   FROM
                        dbo.SR_PRICING_PRODUCT srpr
                        INNER JOIN dbo.SR_Pricing_Product_Country_Map sppcm
                              ON srpr.SR_PRICING_PRODUCT_ID = sppcm.SR_PRICING_PRODUCT_ID
                   WHERE
                        srpr.COMMODITY_TYPE_ID = @Commodity_Id
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.ufn_split(@Country_Id, ',')
                                     WHERE
                                          sppcm.Country_ID = cast(Segments AS INT) ))
            SELECT
                  SR_PRICING_PRODUCT_ID
                 ,PRODUCT_NAME
            FROM
                  Cte_Products
            WHERE
                  Country_Count = @Country_Count
            
                         
END;
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Pricing_Product_Sel_By_Commodity_Country_List] TO [CBMSApplication]
GO
