SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsSsoDocument_Delete

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@sso_document_id	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE  procedure [dbo].[cbmsSsoDocument_Delete]
	( @MyAccountId int
	, @sso_document_id int
	)
AS
BEGIN

	set nocount on

--	  DECLARE @rpl_active varchar(200)
--	   select @rpl_active = entity_name from entity where entity_type = 50000

	  DECLARE @rpl_sv_active varchar(200)
	   select @rpl_sv_active = entity_name from entity where entity_type = 50001


	delete sso_document_owner_map where sso_document_id = @sso_document_id
	delete sso_document where sso_document_id = @sso_document_id
/*
	if @rpl_active = 'True'
	begin

		delete [01SUMMIT-DB2].sso_prod_1.dbo.SSO_DOCUMENT_OWNER_MAP WHERE sso_document_id = @sso_document_id
		delete [01SUMMIT-DB2].sso_prod_1.dbo.SSO_DOCUMENT where sso_document_id = @sso_document_id

		delete [01SUMMIT-DB2].sso_prod_2.dbo.SSO_DOCUMENT_OWNER_MAP WHERE sso_document_id = @sso_document_id
		delete [01SUMMIT-DB2].sso_prod_2.dbo.SSO_DOCUMENT where sso_document_id = @sso_document_id

	end
*/
	exec cbmsEntityAudit_Log @MyAccountId, 'sso_document', @sso_document_id, 'Delete'

--	set nocount off	

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoDocument_Delete] TO [CBMSApplication]
GO
