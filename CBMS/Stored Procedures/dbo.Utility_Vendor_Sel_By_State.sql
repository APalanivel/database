SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.Utility_Vendor_Sel_By_State

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@State_Id		Int				
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Utility_Vendor_Sel_By_State 10


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG			04/28/2010	Created
	SKA			06/04/2010	Added order by clause for ven.Vendor_Name
******/

CREATE PROCEDURE dbo.Utility_Vendor_Sel_By_State
	@State_Id		INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT
		ven.Vendor_id
		,ven.Vendor_Name
	FROM
		dbo.Vendor ven
		JOIN dbo.VENDOR_STATE_MAP vsm
			ON vsm.VENDOR_ID = ven.VENDOR_ID
		JOIN dbo.ENTITY vt
			ON vt.ENTITY_ID = ven.VENDOR_TYPE_ID
	WHERE
		vsm.STATE_ID = @State_Id
		AND vt.ENTITY_NAME = 'Utility'
	ORDER BY ven.Vendor_Name		

END
GO
GRANT EXECUTE ON  [dbo].[Utility_Vendor_Sel_By_State] TO [CBMSApplication]
GO
