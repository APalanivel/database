SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********     
NAME:      
     dbo.Account_Conversion_Insert_Accounts_With_Different_Vendor       
             
DESCRIPTION:                
            
 INPUT PARAMETERS:                
                           
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
                
 OUTPUT PARAMETERS:                
                                 
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
                
 USAGE EXAMPLES:                                  
---------------------------------------------------------------------------------------------------------------                                  
       
 BEGIN TRAN      
 EXEC dbo.Account_Conversion_Insert_Accounts_With_Different_Vendor      
 ROLLBACK TRAN         
        
            
 AUTHOR INITIALS:              
             
 Initials   Name              
---------------------------------------------------------------------------------------------------------------                            
 AKR        Ashok Kumar Raju    
 RR			Raghu Reddy        
 NR			Narayana Reddy       
 SP			Sandeep Pigilam       
 RKV        Ravi Kumar Vegesna    
 PB			Phaneendra Borusu  
 SC			Sreenivasulu Cheerala     
        
 MODIFICATIONS:            
                
 Initials   Date        Modification            
---------------------------------------------------------------------------------------------------------------            
 AKR        2014-06-01  Created.     
 RR			2015-11-30	Global Sourcing - Phase2 - Modified to insert new column Meter_Type into meter table.    
 NR			2016-09-13	MAINT-4233 Added  [Meter_Type],[Alternate_Account_Number],Recalc Type.    
 HG			2017-03-08	CPH change, replaced the Consolidated billing posted to utility flag with the new table insert.    
 HG			2017-03-08	CPH using the temp table built with in SSIS package instead of Physical Staging table.    
 SP			2018-02-13	MAINT-6838,Modify the SQL statement which inserts the data in to Meter table to take the min address id if more than one matches.    
 RKV        2019-04-16  MAINT-8598,Modified the sp to raise the exception if the invoice collection setup is Yes,    
 RKV        2020-01-10  REmove UBM from Account and inserting in newly created table,      
 PB			2020-04-28  MAINT-10216, Modified the SP for avoiding duplicate entries in ENTITY_AUDIT table.  
 SC			2020-05-12  Added Primary meter flag and budget calc type table insert script to the existing code.   
    
*********/
CREATE PROCEDURE [dbo].[Account_Conversion_Insert_Accounts_With_Different_Vendor]
AS
    BEGIN

        SET NOCOUNT ON;
        CREATE TABLE #Loaded_Account_Vendor_Different
             (
                 Account_Id INT PRIMARY KEY CLUSTERED
             );
        ----------If an account /site already exists but the vendor is different then we will convert the account and meter as a new account id, including the variance consumption data.    
        DECLARE
            @invoice_source_type_id INT
            , @Df_Invoice_Reclac_Type_Start_Dt DATE
            , @Exception_Type_Cd INT
            , @Exception_Status_Cd INT
            , @Queue_Id INT
            , @Budget_Calc_Level INT;

        SELECT
            @Budget_Calc_Level = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            c.Code_Value = 'Account'
            AND cs.Codeset_Name = 'BudgetCalcLevel';

        SELECT
            @invoice_source_type_id = a.ENTITY_ID
        FROM
            CBMS.dbo.ENTITY a
        WHERE
            a.ENTITY_NAME = 'UBM'
            AND a.ENTITY_DESCRIPTION = 'Invoice Source';

        SELECT
            @Exception_Type_Cd = Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            Std_Column_Name = 'Exception_Type_Cd'
            AND c.Code_Value = 'Missing IC Data';

        SELECT
            @Exception_Status_Cd = cd.Code_Id
        FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Status'
            AND cd.Code_Value = 'New';

        SELECT
            @Queue_Id = ui.QUEUE_ID
        FROM
            dbo.USER_INFO AS ui
        WHERE
            ui.USER_INFO_ID = 16;

        SELECT
            @Df_Invoice_Reclac_Type_Start_Dt = App_Config_Value
        FROM
            dbo.App_Config ac
        WHERE
            App_Config_Cd = 'Account_Commodity_Invoice_Reclac_Type_Start_Dt';

        INSERT INTO CBMS.dbo.ACCOUNT
             (
                 VENDOR_ID
                 , SITE_ID
                 , ACCOUNT_NUMBER
                 , ACCOUNT_TYPE_ID
                 , INVOICE_SOURCE_TYPE_ID
                 , SERVICE_LEVEL_TYPE_ID
                 , DM_WATCH_LIST
                 , RA_WATCH_LIST
                 , PROCESSING_INSTRUCTIONS
                 , ACCOUNT_GROUP_ID
                 , Is_Data_Entry_Only
                 , Alternate_Account_Number
             )
        OUTPUT
            Inserted.ACCOUNT_ID
        INTO #Loaded_Account_Vendor_Different (Account_Id)
        SELECT  DISTINCT
                conv_a.[Vendor ID]
                , conv_a.[Site id]
                , conv_a.[ACCOUNT NUMBER]
                , 38
                , @invoice_source_type_id
                , ent.ENTITY_ID
                , 0
                , 0
                , NULL
                , NULL
                , CASE WHEN conv_a.DEO = 'YES' THEN 1
                      ELSE 0
                  END
                , NULLIF(RTRIM(LTRIM(conv_a.Alternate_Account_Number)), '')
        FROM
            #Data_Conversion_Accounts_Stage conv_a
            JOIN CBMS.dbo.ENTITY ent
                ON ent.ENTITY_NAME = conv_a.[Service LEVEL]
                   AND  ent.ENTITY_TYPE = 708
            LEFT JOIN dbo.UBM u
                ON u.UBM_NAME = conv_a.UBM
            JOIN CBMS.dbo.ACCOUNT a
                ON a.SITE_ID = conv_a.[Site id]
                   AND  a.ACCOUNT_NUMBER = conv_a.[ACCOUNT NUMBER]
                   AND  a.VENDOR_ID <> conv_a.[Vendor ID]
        WHERE
            conv_a.Is_Validation_Passed = 1
        GROUP BY
            conv_a.[Vendor ID]
            , conv_a.[Site id]
            , u.UBM_ID
            , conv_a.[ACCOUNT NUMBER]
            , conv_a.[UBM Account ID]
            , ent.ENTITY_ID
            , CASE WHEN conv_a.DEO = 'YES' THEN 1
                  ELSE 0
              END
            , conv_a.Alternate_Account_Number;

        DECLARE @Enity_audit_id INT;
        SELECT
            @Enity_audit_id = a.ENTITY_ID
        FROM
            CBMS.dbo.ENTITY a
        WHERE
            a.ENTITY_NAME = 'ACCOUNT_TABLE'
            AND a.ENTITY_DESCRIPTION = 'Table_Type';


        INSERT INTO CBMS.dbo.Account_Ubm_Account_Code_Map
             (
                 Account_Id
                 , Ubm_Id
                 , Ubm_Account_Code
                 , Created_Ts
                 , Updated_Ts
             )
        SELECT
            a.ACCOUNT_ID
            , u.UBM_ID
            , conv_a.[UBM Account ID]
            , GETDATE()
            , GETDATE()
        FROM
            #Data_Conversion_Accounts_Stage conv_a
            INNER JOIN CBMS.dbo.ACCOUNT a
                ON a.SITE_ID = conv_a.[Site id]
                   AND  a.ACCOUNT_NUMBER = conv_a.[ACCOUNT NUMBER]
                   AND  a.VENDOR_ID = conv_a.[Vendor ID]
            INNER JOIN dbo.UBM u
                ON u.UBM_NAME = conv_a.UBM
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                CBMS.dbo.Account_Ubm_Account_Code_Map m
                           WHERE
                                m.Ubm_Id = u.UBM_ID
                                AND m.Account_Id = a.ACCOUNT_ID)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    CBMS.dbo.Account_Ubm_Account_Code_Map m
                               WHERE
                                    m.Ubm_Id = u.UBM_ID
                                    AND m.Ubm_Account_Code = conv_a.[UBM Account ID])
            AND conv_a.Is_Validation_Passed = 1
        GROUP BY
            a.ACCOUNT_ID
            , conv_a.[UBM Account ID]
            , u.UBM_ID;

        INSERT INTO CBMS.dbo.ENTITY_AUDIT
             (
                 ENTITY_ID
                 , ENTITY_IDENTIFIER
                 , USER_INFO_ID
                 , AUDIT_TYPE
                 , MODIFIED_DATE
             )
        SELECT
            @Enity_audit_id
            , a.ACCOUNT_ID
            , 16
            , 1
            , GETDATE()
        FROM
            CBMS.dbo.ACCOUNT a
            JOIN #Data_Conversion_Accounts_Stage conv_a
                ON a.ACCOUNT_NUMBER = conv_a.[ACCOUNT NUMBER]
                   AND  a.SITE_ID = conv_a.[Site id]
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                CBMS.dbo.ENTITY_AUDIT EA
                           WHERE
                                EA.AUDIT_TYPE = 1
                                AND EA.ENTITY_ID = @Enity_audit_id
                                AND EA.ENTITY_IDENTIFIER = a.ACCOUNT_ID)
            AND a.ACCOUNT_TYPE_ID = 38
            AND conv_a.Is_Validation_Passed = 1
            AND a.ACCOUNT_ID IN (   SELECT
                                        a.ACCOUNT_ID
                                    FROM
                                        CBMS.dbo.ACCOUNT a
                                        JOIN (   SELECT
                                                        conv_a.[Vendor ID]
                                                        , conv_a.[Site id]
                                                        , conv_a.[ACCOUNT NUMBER]
                                                        , conv_a.[UBM Account ID]
                                                 FROM
                                                        #Data_Conversion_Accounts_Stage conv_a
                                                        JOIN CBMS.dbo.ACCOUNT a
                                                            ON a.SITE_ID = conv_a.[Site id]
                                                               AND a.ACCOUNT_NUMBER = conv_a.[ACCOUNT NUMBER]
                                                               AND a.VENDOR_ID <> conv_a.[Vendor ID]
                                                 GROUP BY
                                                     conv_a.[Vendor ID]
                                                     , conv_a.[Site id]
                                                     , conv_a.[ACCOUNT NUMBER]
                                                     , conv_a.[UBM Account ID]) x
                                            ON a.ACCOUNT_NUMBER = x.[ACCOUNT NUMBER]
                                               AND  a.SITE_ID = x.[Site id]
                                               AND  a.VENDOR_ID = x.[Vendor ID]
                                               AND  conv_a.[UBM Account ID] = x.[UBM Account ID]
                                    GROUP BY
                                        a.ACCOUNT_ID )
        GROUP BY
            a.ACCOUNT_ID;

        INSERT INTO CBMS.dbo.Account_Variance_Consumption_Level
             (
                 ACCOUNT_ID
                 , Variance_Consumption_Level_Id
             )
        SELECT
            a.ACCOUNT_ID
            , conv_a.[Consumption Level ID]
        FROM
            #Data_Conversion_Accounts_Stage conv_a
            JOIN CBMS.dbo.ACCOUNT a
                ON a.SITE_ID = conv_a.[Site id]
                   AND  a.ACCOUNT_NUMBER = conv_a.[ACCOUNT NUMBER]
        WHERE
            a.ACCOUNT_TYPE_ID = 38
            AND conv_a.Is_Validation_Passed = 1
            AND a.ACCOUNT_ID IN (   SELECT
                                        a.ACCOUNT_ID
                                    FROM
                                        CBMS.dbo.ACCOUNT a
                                        JOIN (   SELECT
                                                        conv_a.[Vendor ID]
                                                        , conv_a.[Site id]
                                                        , conv_a.[ACCOUNT NUMBER]
                                                        , conv_a.[UBM Account ID]
                                                 FROM
                                                        #Data_Conversion_Accounts_Stage conv_a
                                                        JOIN CBMS.dbo.ACCOUNT a
                                                            ON a.SITE_ID = conv_a.[Site id]
                                                               AND a.ACCOUNT_NUMBER = conv_a.[ACCOUNT NUMBER]
                                                               AND a.VENDOR_ID <> conv_a.[Vendor ID]
                                                 GROUP BY
                                                     conv_a.[Vendor ID]
                                                     , conv_a.[Site id]
                                                     , conv_a.[ACCOUNT NUMBER]
                                                     , conv_a.[UBM Account ID]) x
                                            ON a.ACCOUNT_NUMBER = x.[ACCOUNT NUMBER]
                                               AND  a.SITE_ID = x.[Site id]
                                               AND  a.VENDOR_ID = x.[Vendor ID]
                                               AND  conv_a.[UBM Account ID] = x.[UBM Account ID]
                                    GROUP BY
                                        a.ACCOUNT_ID )
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    CBMS.dbo.Account_Variance_Consumption_Level avcl
                               WHERE
                                    avcl.ACCOUNT_ID = a.ACCOUNT_ID
                                    AND avcl.Variance_Consumption_Level_Id = conv_a.[Consumption Level ID])
        GROUP BY
            a.ACCOUNT_ID
            , conv_a.[Consumption Level ID];

        INSERT INTO CBMS.dbo.INVOICE_PARTICIPATION_QUEUE
             (
                 EVENT_TYPE
                 , CLIENT_ID
                 , DIVISION_ID
                 , SITE_ID
                 , ACCOUNT_ID
                 , SERVICE_MONTH
                 , EVENT_BY_ID
                 , EVENT_DATE
             )
        SELECT
            1
            , NULL
            , NULL
            , SITE_ID
            , ACCOUNT_ID
            , NULL
            , 93
            , GETDATE()
        FROM
            CBMS.dbo.ACCOUNT
        WHERE
            ACCOUNT_ID IN (   SELECT
                                    a.ACCOUNT_ID
                              FROM
                                    CBMS.dbo.ACCOUNT a
                                    JOIN (   SELECT
                                                    conv_a.[Vendor ID]
                                                    , conv_a.[Site id]
                                                    , conv_a.[ACCOUNT NUMBER]
                                                    , conv_a.[UBM Account ID]
                                             FROM
                                                    #Data_Conversion_Accounts_Stage conv_a
                                                    JOIN CBMS.dbo.ACCOUNT a
                                                        ON a.SITE_ID = conv_a.[Site id]
                                                           AND a.ACCOUNT_NUMBER = conv_a.[ACCOUNT NUMBER]
                                                           AND a.VENDOR_ID <> conv_a.[Vendor ID]
                                             WHERE
                                                 conv_a.Is_Validation_Passed = 1
                                             GROUP BY
                                                 conv_a.[Vendor ID]
                                                 , conv_a.[Site id]
                                                 , conv_a.[ACCOUNT NUMBER]
                                                 , conv_a.[UBM Account ID]) x
                                        ON a.ACCOUNT_NUMBER = x.[ACCOUNT NUMBER]
                                           AND  a.SITE_ID = x.[Site id]
                                           AND  a.VENDOR_ID = x.[Vendor ID]
                              GROUP BY
                                  a.ACCOUNT_ID )
        GROUP BY
            SITE_ID
            , ACCOUNT_ID;


        INSERT INTO CBMS.dbo.METER
             (
                 RATE_ID
                 , PURCHASE_METHOD_TYPE_ID
                 , ACCOUNT_ID
                 , ADDRESS_ID
                 , METER_NUMBER
                 , Meter_Type_Cd
             )
        SELECT
            conv_a.[Rate ID]
            , e.ENTITY_ID
            , a.ACCOUNT_ID
            , MIN(CASE WHEN conv_a.[Is Default To Primary] = 'NO' THEN ads.ADDRESS_ID
                      ELSE s.PRIMARY_ADDRESS_ID
                  END) address_id
            , conv_a.METER
            , mtrtyp.Code_Id
        FROM
            #Data_Conversion_Accounts_Stage conv_a
            INNER JOIN CBMS.dbo.ENTITY e
                ON e.ENTITY_DESCRIPTION = 'Purchase Method'
                   AND  e.ENTITY_NAME = conv_a.[Purchase Method]
            JOIN CBMS.dbo.SITE s
                ON s.SITE_ID = conv_a.[Site id]
            JOIN CBMS.dbo.ACCOUNT a
                ON a.SITE_ID = s.SITE_ID
                   AND  a.ACCOUNT_NUMBER = conv_a.[ACCOUNT NUMBER]
            LEFT JOIN CBMS.dbo.ADDRESS ads
                ON ads.ADDRESS_PARENT_ID = s.SITE_ID
                   AND  conv_a.[Site Address] = ads.ADDRESS_LINE1
            LEFT JOIN(dbo.Code mtrtyp
                      JOIN dbo.Codeset cs
                          ON mtrtyp.Codeset_Id = cs.Codeset_Id)
                ON mtrtyp.Code_Value = LTRIM(RTRIM(conv_a.Meter_Type))
                   AND  cs.Codeset_Name = 'Meter Type'
        WHERE
            a.ACCOUNT_TYPE_ID = 38
            AND conv_a.Is_Validation_Passed = 1
            AND a.ACCOUNT_ID IN (   SELECT
                                        a.ACCOUNT_ID
                                    FROM
                                        CBMS.dbo.ACCOUNT a
                                        JOIN (   SELECT
                                                        conv_a.[Vendor ID]
                                                        , conv_a.[Site id]
                                                        , conv_a.[ACCOUNT NUMBER]
                                                        , conv_a.[UBM Account ID]
                                                 FROM
                                                        #Data_Conversion_Accounts_Stage conv_a
                                                        JOIN CBMS.dbo.ACCOUNT a
                                                            ON a.SITE_ID = conv_a.[Site id]
                                                               AND a.ACCOUNT_NUMBER = conv_a.[ACCOUNT NUMBER]
                                                               AND a.VENDOR_ID <> conv_a.[Vendor ID]
                                                 GROUP BY
                                                     conv_a.[Vendor ID]
                                                     , conv_a.[Site id]
                                                     , conv_a.[ACCOUNT NUMBER]
                                                     , conv_a.[UBM Account ID]) x
                                            ON a.ACCOUNT_NUMBER = x.[ACCOUNT NUMBER]
                                               AND  a.SITE_ID = x.[Site id]
                                               AND  a.VENDOR_ID = x.[Vendor ID]
                                    GROUP BY
                                        a.ACCOUNT_ID )
        GROUP BY
            conv_a.[Rate ID]
            , e.ENTITY_ID
            , a.ACCOUNT_ID
            , conv_a.METER
            , mtrtyp.Code_Id;

        INSERT INTO dbo.Account_Consolidated_Billing_Vendor
             (
                 Account_Id
                 , Billing_Start_Dt
                 , Billing_End_Dt
                 , Invoice_Vendor_Type_Id
                 , Supplier_Vendor_Id
                 , Created_User_Id
                 , Created_Ts
                 , Updated_User_Id
                 , Last_Change_Ts
             )
        SELECT
            a.ACCOUNT_ID
            , conv_a.Consolidated_Billing_Start_Dt
            , conv_a.Consolidated_Billing_End_Dt
            , vt.ENTITY_ID
            , conv_a.Supplier_Vendor_Id
            , 16
            , GETDATE()
            , 16
            , GETDATE()
        FROM
            #Data_Conversion_Accounts_Stage conv_a
            INNER JOIN CBMS.dbo.ACCOUNT a
                ON a.ACCOUNT_NUMBER = conv_a.[ACCOUNT NUMBER]
                   AND  a.SITE_ID = conv_a.[Site id]
            INNER JOIN dbo.ENTITY vt
                ON vt.ENTITY_NAME = conv_a.Consolidated_Billing_Invoice_Type
                   AND  vt.ENTITY_DESCRIPTION = 'Vendor'
        WHERE
            a.ACCOUNT_TYPE_ID = 38
            AND conv_a.Is_Validation_Passed = 1
            AND a.ACCOUNT_ID IN (   SELECT
                                        a.ACCOUNT_ID
                                    FROM
                                        CBMS.dbo.ACCOUNT a
                                        JOIN (   SELECT
                                                        conv_a.[Vendor ID]
                                                        , conv_a.[Site id]
                                                        , conv_a.[ACCOUNT NUMBER]
                                                        , conv_a.[UBM Account ID]
                                                 FROM
                                                        #Data_Conversion_Accounts_Stage conv_a
                                                        JOIN CBMS.dbo.ACCOUNT a
                                                            ON a.SITE_ID = conv_a.[Site id]
                                                               AND a.ACCOUNT_NUMBER = conv_a.[ACCOUNT NUMBER]
                                                               AND a.VENDOR_ID <> conv_a.[Vendor ID]
                                                 GROUP BY
                                                     conv_a.[Vendor ID]
                                                     , conv_a.[Site id]
                                                     , conv_a.[ACCOUNT NUMBER]
                                                     , conv_a.[UBM Account ID]) x
                                            ON a.ACCOUNT_NUMBER = x.[ACCOUNT NUMBER]
                                               AND  a.SITE_ID = x.[Site id]
                                               AND  a.VENDOR_ID = x.[Vendor ID]
                                    GROUP BY
                                        a.ACCOUNT_ID )
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Account_Consolidated_Billing_Vendor cbv
                               WHERE
                                    cbv.Account_Id = a.ACCOUNT_ID)
        GROUP BY
            a.ACCOUNT_ID
            , conv_a.Consolidated_Billing_Start_Dt
            , conv_a.Consolidated_Billing_End_Dt
            , vt.ENTITY_ID
            , conv_a.Supplier_Vendor_Id;

        INSERT INTO CBMS.dbo.Account_Commodity_Invoice_Recalc_Type
             (
                 Account_Id
                 , Commodity_ID
                 , Start_Dt
                 , End_Dt
                 , Invoice_Recalc_Type_Cd
                 , Created_User_Id
                 , Updated_User_Id
             )
        SELECT
            a.ACCOUNT_ID
            , conv_a.Commodity
            , CASE WHEN DATEPART(
                            dd
                            , CAST(ISNULL(
                                       NULLIF(NULLIF(RTRIM(LTRIM(Recalc_Type_Start_Dt)), ''), '1900-01-01')
                                       , @Df_Invoice_Reclac_Type_Start_Dt) AS DATE)) <> 1 THEN
                       DATEADD(
                           mm
                           , DATEDIFF(
                                 mm, 0
                                 , ISNULL(
                                       NULLIF(RTRIM(LTRIM(Recalc_Type_Start_Dt)), ''), @Df_Invoice_Reclac_Type_Start_Dt))
                           , 0)
                  ELSE
                      ISNULL(
                          NULLIF(NULLIF(RTRIM(LTRIM(Recalc_Type_Start_Dt)), ''), '1900-01-01')
                          , @Df_Invoice_Reclac_Type_Start_Dt)
              END AS Recalc_Type_Start_Dt
            , CASE WHEN DATEPART(dd, NULLIF(NULLIF(RTRIM(LTRIM(Recalc_Type_End_Dt)), ''), '1900-01-01')) <> 1 THEN
                       DATEADD(
                           mm, DATEDIFF(mm, 0, NULLIF(NULLIF(RTRIM(LTRIM(Recalc_Type_End_Dt)), ''), '1900-01-01')), 0)
                  ELSE NULLIF(NULLIF(RTRIM(LTRIM(Recalc_Type_End_Dt)), ''), '1900-01-01')
              END AS Recalc_Type_End_Dt
            , rectyp.Code_Id
            , 16
            , 16
        FROM
            #Data_Conversion_Accounts_Stage conv_a
            INNER JOIN CBMS.dbo.ACCOUNT a
                ON a.ACCOUNT_NUMBER = conv_a.[ACCOUNT NUMBER]
                   AND  a.SITE_ID = conv_a.[Site id]
            INNER JOIN dbo.Code rectyp
                ON rectyp.Code_Value = CASE WHEN conv_a.Recalc_Type IS NULL
                                                 OR LTRIM(RTRIM(conv_a.Recalc_Type)) = '' THEN 'No Recalc'
                                           ELSE LTRIM(RTRIM(conv_a.Recalc_Type))
                                       END
            INNER JOIN dbo.Codeset cs
                ON rectyp.Codeset_Id = cs.Codeset_Id
                   AND  cs.Codeset_Name = 'Recalc Type'
        WHERE
            a.ACCOUNT_TYPE_ID = 38
            AND conv_a.Is_Validation_Passed = 1
            AND a.ACCOUNT_ID IN (   SELECT
                                        a.ACCOUNT_ID
                                    FROM
                                        CBMS.dbo.ACCOUNT a
                                        JOIN (   SELECT
                                                        conv_a.[Vendor ID]
                                                        , conv_a.[Site id]
                                                        , conv_a.[ACCOUNT NUMBER]
                                                        , conv_a.[UBM Account ID]
                                                 FROM
                                                        #Data_Conversion_Accounts_Stage conv_a
                                                        JOIN CBMS.dbo.ACCOUNT a
                                                            ON a.SITE_ID = conv_a.[Site id]
                                                               AND a.ACCOUNT_NUMBER = conv_a.[ACCOUNT NUMBER]
                                                               AND a.VENDOR_ID <> conv_a.[Vendor ID]
                                                 GROUP BY
                                                     conv_a.[Vendor ID]
                                                     , conv_a.[Site id]
                                                     , conv_a.[ACCOUNT NUMBER]
                                                     , conv_a.[UBM Account ID]) x
                                            ON a.ACCOUNT_NUMBER = x.[ACCOUNT NUMBER]
                                               AND  a.SITE_ID = x.[Site id]
                                               AND  a.VENDOR_ID = x.[Vendor ID]
                                    GROUP BY
                                        a.ACCOUNT_ID )
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    CBMS.dbo.Account_Commodity_Invoice_Recalc_Type avcl
                               WHERE
                                    avcl.Account_Id = a.ACCOUNT_ID
                                    AND avcl.Commodity_ID = conv_a.Commodity
                                    AND avcl.Start_Dt = CASE WHEN DATEPART(
                                                                      dd
                                                                      , CAST(ISNULL(
                                                                                 NULLIF(NULLIF(RTRIM(
                                                                                                   LTRIM(
                                                                                                       Recalc_Type_Start_Dt)), ''), '1900-01-01')
                                                                                 , @Df_Invoice_Reclac_Type_Start_Dt) AS DATE)) <> 1 THEN
                                                                 DATEADD(
                                                                     mm
                                                                     , DATEDIFF(
                                                                           mm, 0
                                                                           , ISNULL(
                                                                                 NULLIF(RTRIM(
                                                                                            LTRIM(Recalc_Type_Start_Dt)), '')
                                                                                 , @Df_Invoice_Reclac_Type_Start_Dt))
                                                                     , 0)
                                                            ELSE
                                                                ISNULL(
                                                                    NULLIF(NULLIF(RTRIM(LTRIM(Recalc_Type_Start_Dt)), ''), '1900-01-01')
                                                                    , @Df_Invoice_Reclac_Type_Start_Dt)
                                                        END
                                    AND (   NULLIF(NULLIF(RTRIM(LTRIM(Recalc_Type_End_Dt)), ''), '1900-01-01') IS NULL
                                            OR  avcl.End_Dt = CASE WHEN DATEPART(
                                                                            dd
                                                                            , NULLIF(NULLIF(RTRIM(
                                                                                                LTRIM(
                                                                                                    Recalc_Type_End_Dt)), ''), '1900-01-01')) <> 1 THEN
                                                                       DATEADD(
                                                                           mm
                                                                           , DATEDIFF(
                                                                                 mm, 0
                                                                                 , NULLIF(NULLIF(RTRIM(
                                                                                                     LTRIM(
                                                                                                         Recalc_Type_End_Dt)), ''), '1900-01-01'))
                                                                           , 0)
                                                                  ELSE
                                                                      NULLIF(NULLIF(RTRIM(LTRIM(Recalc_Type_End_Dt)), ''), '1900-01-01')
                                                              END))
        GROUP BY
            a.ACCOUNT_ID
            , conv_a.Commodity
            , conv_a.Recalc_Type_Start_Dt
            , conv_a.Recalc_Type_End_Dt
            , rectyp.Code_Id;

        -- This list will be passed to the SSIS package to update the invoice collection setup as Yes  / No in DVDEHub.                                  
        INSERT INTO #New_Account_Id
             (
                 Client_Hier_Id
                 , Account_Id
                 , Setup_Invoice_Collection
             )
        SELECT
            ch.Client_Hier_Id
            , la.Account_Id
            , conv_a.Setup_Invoice_Collection
        FROM
            #Loaded_Account_Vendor_Different la
            INNER JOIN dbo.ACCOUNT ac
                ON ac.ACCOUNT_ID = la.Account_Id
            INNER JOIN #Data_Conversion_Accounts_Stage conv_a
                ON ac.SITE_ID = conv_a.[Site id]
                   AND  ac.ACCOUNT_NUMBER = conv_a.[ACCOUNT NUMBER]
                   AND  ac.VENDOR_ID = conv_a.[Vendor ID]
            INNER JOIN dbo.SITE s
                ON s.SITE_ID = ac.SITE_ID
            INNER JOIN dbo.Invoice_Collection_Client_Config cc
                ON cc.Client_Id = s.Client_ID
            INNER JOIN Core.Client_Hier ch
                ON ch.Site_Id = s.SITE_ID
        WHERE
            NOT EXISTS (SELECT  1 FROM  #New_Account_Id na WHERE na.Account_Id = la.Account_Id)
            AND conv_a.Is_Validation_Passed = 1
        GROUP BY
            ch.Client_Hier_Id
            , la.Account_Id
            , conv_a.Setup_Invoice_Collection;

        INSERT INTO dbo.Account_Exception
             (
                 Account_Id
                 , Meter_Id
                 , Exception_Type_Cd
                 , Queue_Id
                 , Exception_Status_Cd
                 , Exception_By_User_Id
                 , Exception_Created_Ts
                 , Last_Change_Ts
                 , Updated_User_Id
                 , Commodity_Id
             )
        SELECT
            nai.ACCOUNT_ID
            , CASE WHEN COUNT(m.METER_ID) = 1 THEN MAX(m.METER_ID)
                  ELSE -1
              END
            , @Exception_Type_Cd
            , ui.QUEUE_ID
            , @Exception_Status_Cd
            , 16
            , GETDATE()
            , GETDATE()
            , 16
            , CASE WHEN COUNT(conv_a.Commodity) = 1 THEN MAX(conv_a.Commodity)
                  ELSE -1
              END
        FROM
            #New_Account_Id nai
            INNER JOIN dbo.ACCOUNT ac
                ON ac.ACCOUNT_ID = nai.ACCOUNT_ID
            INNER JOIN #Data_Conversion_Accounts_Stage conv_a
                ON ac.SITE_ID = conv_a.[Site id]
                   AND  ac.ACCOUNT_NUMBER = conv_a.[ACCOUNT NUMBER]
                   AND  ac.VENDOR_ID = conv_a.[Vendor ID]
            INNER JOIN CBMS.dbo.METER m
                ON m.ACCOUNT_ID = nai.ACCOUNT_ID
            INNER JOIN Core.Client_Hier AS ch
                ON ch.Site_Id = ac.SITE_ID
            INNER JOIN dbo.Invoice_Collection_Client_Config AS iccc
                ON ch.Client_Id = iccc.Client_Id
            INNER JOIN dbo.USER_INFO AS ui
                ON ui.USER_INFO_ID = iccc.Invoice_Collection_Officer_User_Id
        WHERE
            nai.Setup_Invoice_Collection = 'Yes'
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Account_Exception ae
                               WHERE
                                    ae.Account_Id = nai.Account_Id
                                    AND ae.Exception_Type_Cd = @Exception_Type_Cd
                                    AND ae.Queue_Id = ui.QUEUE_ID)
        GROUP BY
            nai.ACCOUNT_ID
            , ui.QUEUE_ID;

        ---Insert into Primary Meter table   ;    
        WITH Cte_Primary
        AS (
               SELECT
                    LA.Account_Id
                    , conv_a.Commodity
                    , m.METER_ID
                    , DENSE_RANK() OVER (PARTITION BY
                                             LA.Account_Id
                                             , conv_a.Commodity
                                         ORDER BY
                                             m.METER_ID ASC) AS Row_Num
               FROM
                    #Loaded_Account_Vendor_Different LA
                    INNER JOIN CBMS.dbo.ACCOUNT a
                        ON a.ACCOUNT_ID = LA.Account_Id
                    INNER JOIN #Data_Conversion_Accounts_Stage conv_a
                        ON a.SITE_ID = conv_a.[Site id]
                           AND  a.ACCOUNT_NUMBER = conv_a.[ACCOUNT NUMBER]
                           AND  a.VENDOR_ID = conv_a.[Vendor ID]
                    INNER JOIN dbo.METER m
                        ON m.ACCOUNT_ID = LA.Account_Id
               WHERE
                    NOT EXISTS (   SELECT
                                        1
                                   FROM
                                        Budget.Account_Commodity_Primary_Meter acpm
                                   WHERE
                                        acpm.Account_Id = a.ACCOUNT_ID
                                        AND acpm.Commodity_Id = conv_a.Commodity)
           )
        INSERT INTO Budget.Account_Commodity_Primary_Meter
             (
                 Account_Id
                 , Commodity_Id
                 , Meter_Id
                 , Created_User_Id
                 , Created_Ts
                 , Updated_User_Id
                 , Last_Change_Ts
             )
        SELECT
            cte.Account_Id
            , cte.Commodity
            , cte.METER_ID
            , 16
            , GETDATE()
            , 16
            , GETDATE()
        FROM
            Cte_Primary cte
        WHERE
            cte.Row_Num = 1;

        ---insert Budget Calc Level As Account  for each commodity              
        INSERT INTO Budget.Account_Commodity_Budget_Calc_Level
             (
                 Account_Id
                 , Commodity_Id
                 , Budget_Calc_Level_Cd
                 , Created_User_Id
                 , Created_Ts
                 , Updated_User_Id
                 , Last_Change_Ts
             )
        SELECT  DISTINCT
                LA.Account_Id
                , conv_a.Commodity
                , @Budget_Calc_Level
                , 16
                , GETDATE()
                , 16
                , GETDATE()
        FROM
            #Loaded_Account_Vendor_Different LA
            INNER JOIN CBMS.dbo.ACCOUNT a
                ON LA.Account_Id = a.ACCOUNT_ID
            INNER JOIN #Data_Conversion_Accounts_Stage conv_a
                ON a.SITE_ID = conv_a.[Site id]
                   AND  a.ACCOUNT_NUMBER = conv_a.[ACCOUNT NUMBER]
                   AND  a.VENDOR_ID = conv_a.[Vendor ID]
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                Budget.Account_Commodity_Budget_Calc_Level Acbcl
                           WHERE
                                Acbcl.Account_Id = LA.Account_Id
                                AND Acbcl.Commodity_Id = conv_a.Commodity);

    END;
    ;


GO


GRANT EXECUTE ON  [dbo].[Account_Conversion_Insert_Accounts_With_Different_Vendor] TO [ETL_Execute]
GO
