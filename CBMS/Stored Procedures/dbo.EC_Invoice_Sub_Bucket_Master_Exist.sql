SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.EC_Invoice_Sub_Bucket_Master_Exist       
              
Description:              
			This sproc checks if the sub bucket name  is already present for the Given stae and bucket master id.      
                            
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
    @Bucket_Master_Id					INT
    @State_Id							INT        
    
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------  
 
   Exec dbo.EC_Invoice_Sub_Bucket_Master_Exist 124,344
   
   Exec dbo.EC_Invoice_Sub_Bucket_Master_Exist 100,200         

   Exec dbo.EC_Invoice_Sub_Bucket_Master_Exist 1,2    
   
	
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy               
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2015-04-22		Created For AS400.         
             
******/ 

CREATE PROCEDURE [dbo].[EC_Invoice_Sub_Bucket_Master_Exist]
      ( 
       @State_Id INT
      ,@Bucket_Master_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 
            
      DECLARE @Is_Sub_Bucket_Master_Exist BIT= 0
      
      SELECT
            @Is_Sub_Bucket_Master_Exist = 1
      FROM
            dbo.EC_Invoice_Sub_Bucket_Master eisbm
      WHERE
            eisbm.State_Id = @State_Id
            AND eisbm.Bucket_Master_Id = @Bucket_Master_Id
            
      SELECT
            @Is_Sub_Bucket_Master_Exist AS Sub_Bucket_Master_Exist
            
                 
END



;
GO
GRANT EXECUTE ON  [dbo].[EC_Invoice_Sub_Bucket_Master_Exist] TO [CBMSApplication]
GO
