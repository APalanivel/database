
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.DELETE_BILLING_DETERMINANT_P

DESCRIPTION:
	Deletes the billing determinant and the peak details mapped to it

INPUT PARAMETERS:
	Name									DataType		Default	Description
-------------------------------------------------------------------------------
	@billingDeterminantId					INT

OUTPUT PARAMETERS:
	Name									DataType		Default	Description
-------------------------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------------------------
					
Exec dbo.DELETE_BILLING_DETERMINANT_P 1711

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------------------------
	CPE			Chaitanya Panduga Eshwar

MODIFICATIONS

	Initials	Date			Modification
------------------------------------------------------------------------------
	CPE			2012-07-25		Added the delete statement to delete from Time_Of_Use_Schedule_Term_Peak_Billing_Determinant
******/ 
CREATE PROCEDURE dbo.DELETE_BILLING_DETERMINANT_P
      @billingDeterminantId INT
AS 
BEGIN
	
      SET NOCOUNT ON
      BEGIN TRY
            BEGIN TRAN
	
            DELETE FROM
                  dbo.Time_Of_Use_Schedule_Term_Peak_Billing_Determinant
            WHERE
                  BILLING_DETERMINANT_ID = @billingDeterminantId

            DELETE FROM
                  dbo.BILLING_DETERMINANT
            WHERE
                  BILLING_DETERMINANT_ID = @billingDeterminantId

            COMMIT TRAN
      END TRY
      BEGIN CATCH
            IF @@TRANCOUNT > 0 
                  ROLLBACK TRAN
            EXEC dbo.usp_RethrowError
      END CATCH
END

;
GO

GRANT EXECUTE ON  [dbo].[DELETE_BILLING_DETERMINANT_P] TO [CBMSApplication]
GO
