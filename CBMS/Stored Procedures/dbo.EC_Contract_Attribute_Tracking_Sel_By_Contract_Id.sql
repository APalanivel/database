SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.EC_Contract_Attribute_Tracking_Sel_By_Contract_Id      
              
Description:              
			This sproc get the attribute tracking  details for the given contract id.       
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
    @Contract_Id							INT
    
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

   Exec dbo.EC_Contract_Attribute_Tracking_Sel_By_Contract_Id 804129
   
   Exec dbo.EC_Contract_Attribute_Tracking_Sel_By_Contract_Id 804130
   
	
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna               
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2015-09-15		Created For AS400II.         
             
******/ 

CREATE PROCEDURE [dbo].[EC_Contract_Attribute_Tracking_Sel_By_Contract_Id] ( @Contract_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 
            
      SELECT
            ecat.EC_Contract_Attribute_Tracking_Id
           ,eca.EC_Contract_Attribute_Id
           ,eca.EC_Contract_Attribute_Name
           ,ecat.EC_Contract_Attribute_Value
           ,ecat.Contract_Id
           ,c.Code_Value AS Attribute_Type
      FROM
            dbo.EC_Contract_Attribute eca
            INNER JOIN dbo.EC_Contract_Attribute_Tracking ecat
                  ON eca.EC_Contract_Attribute_Id = ecat.EC_Contract_Attribute_Id
            INNER JOIN dbo.Code c
                  ON c.Code_Id = eca.Attribute_Type_Cd
      WHERE
            ecat.Contract_Id = @Contract_Id
      
      
     
                 
END;
;
GO
GRANT EXECUTE ON  [dbo].[EC_Contract_Attribute_Tracking_Sel_By_Contract_Id] TO [CBMSApplication]
GO
