
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 Ec_Invoice_Sub_Bucket_Master_Id_Sel_By_Ubm__Bucket__Sub_Bucket_Cd    
    
 DESCRIPTION: It Displays Ec Invoice Sub Bucket Master Id for the given UBM Id , Bukcet_Master_Id ,UBM Sub Bucket Code.    
    
INPUT PARAMETERS:    
 Name    DataType  Default Description    
------------------------------------------------------------    
 @Ubm_Id     INT    
    @@Bucket_Master_Id  INT    
    @Ubm_Sub_Bucket_Code NVARCHAR(255)     
     
OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
 EXEC dbo.Ec_Invoice_Sub_Bucket_Master_Id_Sel_By_Ubm__Bucket__Sub_Bucket_Cd 1,291,'Natural Gas'    
EXEC dbo.Ec_Invoice_Sub_Bucket_Master_Id_Sel_By_Ubm__Bucket__Sub_Bucket_Cd    7,350,'Usage - Off-Peak',116  
AUTHOR INITIALS:    
 Initials Name    
------------------------------------------------------------    
 RKV   Ravi Kumar Vegesna    
     
MODIFICATIONS    
    
 Initials		Date		Modification    
------------------------------------------------------------    
 RKV			2016-02-11  Created as part of AS400-II    
 HG				2017-02-17	@State_Id parameter added
******/    
CREATE PROCEDURE [dbo].[Ec_Invoice_Sub_Bucket_Master_Id_Sel_By_Ubm__Bucket__Sub_Bucket_Cd]
      @Ubm_Id INT
     ,@Bucket_Master_Id INT
     ,@Ubm_Sub_Bucket_Code NVARCHAR(255)
     ,@State_Id INT
AS
BEGIN

      SET NOCOUNT ON;    

      SELECT
            ubdsbm.EC_Invoice_Sub_Bucket_Master_Id
           ,ubdm.Bucket_Master_Id
      FROM
            dbo.Ubm_Bucket_Determinant_Ec_Invoice_Sub_Bucket_Map ubdsbm
            INNER JOIN dbo.UBM_BUCKET_DETERMINANT_MAP ubdm
                  ON ubdsbm.Ubm_Bucket_Determinant_Map_Id = ubdm.UBM_BUCKET_DETERMINANT_MAP_ID
      WHERE
            ubdm.Bucket_Master_Id = @Bucket_Master_Id
            AND ubdm.UBM_ID = @Ubm_Id
            AND ubdsbm.State_Id = @State_Id
            AND ubdsbm.Ubm_Sub_Bucket_Code = @Ubm_Sub_Bucket_Code;    
END;
;
GO

GRANT EXECUTE ON  [dbo].[Ec_Invoice_Sub_Bucket_Master_Id_Sel_By_Ubm__Bucket__Sub_Bucket_Cd] TO [CBMSApplication]
GO
