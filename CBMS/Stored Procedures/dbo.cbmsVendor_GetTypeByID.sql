SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE      PROCEDURE [dbo].[cbmsVendor_GetTypeByID]
	( @MyAccountId int 
	, @vendor_id int = null
	)
AS
BEGIN
	set nocount on
	   select distinct v.vendor_id
		, v.vendor_name
		, v.vendor_type_id
	     from vendor v
	    where v.vendor_id = @vendor_id
	 order by v.vendor_name

END
GO
GRANT EXECUTE ON  [dbo].[cbmsVendor_GetTypeByID] TO [CBMSApplication]
GO
