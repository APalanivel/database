SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
	dbo.Default_Value_Sel_By_EC_Meter_Attribute_Name_State_Id_Commodity_Id  

DESCRIPTION:  
	Used to get the default uom of the given commodity

INPUT PARAMETERS:  
	Name							DataType	Default		Description  
------------------------------------------------------------  
	@EC_Meter_Attribute_Name		NVARCHAR	 
	@Client_Hier_Id					INT
	@Commodity_Id					INT

OUTPUT PARAMETERS:  
	Name			DataType	Default		Description  
------------------------------------------------------------  


USAGE EXAMPLES:  
------------------------------------------------------------
	EXEC dbo.Default_Value_Sel_By_EC_Meter_Attribute_Name_State_Id_Commodity_Id  'Billing Days adjustment',10964,290
	
	
AUTHOR INITIALS:  
	Initials	Name  
------------------------------------------------------------  
	RKV			Ravi Kumar Vegesna
	  
MODIFICATIONS   
	Initials	Date		Modification  
------------------------------------------------------------  
	RKV			2016-11-07	Created For MAINT-4317 Billing Days adjustment
******/  
  
CREATE  PROCEDURE [dbo].[Default_Value_Sel_By_EC_Meter_Attribute_Name_State_Id_Commodity_Id]
      @EC_Meter_Attribute_Name NVARCHAR(200)
     ,@Client_Hier_Id INT
     ,@Commodity_Id INT
AS 
BEGIN  
  
      SET NOCOUNT ON  
  
      SELECT
            EC_Meter_Attribute_Value
      FROM
            dbo.EC_Meter_Attribute_Value emav
            INNER JOIN dbo.EC_Meter_Attribute ema
                  ON emav.EC_Meter_Attribute_Id = ema.EC_Meter_Attribute_Id
            INNER JOIN core.Client_Hier ch
                  ON ch.State_Id = ema.State_Id
      WHERE
            ema.EC_Meter_Attribute_Name = @EC_Meter_Attribute_Name
            AND ema.Commodity_Id = @Commodity_Id
            AND ch.Client_Hier_Id = @Client_Hier_Id
            AND Is_Default = 1
END  
;
GO
GRANT EXECUTE ON  [dbo].[Default_Value_Sel_By_EC_Meter_Attribute_Name_State_Id_Commodity_Id] TO [CBMSApplication]
GO
