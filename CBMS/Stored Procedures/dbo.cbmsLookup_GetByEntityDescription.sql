SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********      
      
NAME:                               
     dbo.cbmsLookup_GetByEntityDescription     
      
DESCRIPTION:       
      
      
 INPUT PARAMETERS:                                              
      
 Name                        DataType         Default       Description                                            
---------------------------------------------------------------------------------------------------------------                                          
      
      
 OUTPUT PARAMETERS:                                              
      
 Name                        DataType         Default       Description                                            
---------------------------------------------------------------------------------------------------------------                                          
      
 USAGE EXAMPLES:                                                                
---------------------------------------------------------------------------------------------------------------                                                                
      
 BEGIN TRAN                                    
  
 EXEC dbo.cbmsLookup_GetByEntityDescription    'Open Recalc Status'   
   
 EXEC dbo.cbmsLookup_GetByEntityDescription    'Close Recalc Status'   , 'Error identified'                               
            
 ROLLBACK TRAN                                       
      
      
 AUTHOR INITIALS:                                            
      
 Initials   Name                                            
---------------------------------------------------------------------------------------------------------------                                                          
 ABK        Aditya Bharadwaj      
                                                                        
      
 MODIFICATIONS:                                          
      
 Initials Date        Modification                                          
---------------------------------------------------------------------------------------------------------------                                          
 ABK   2019-06-18  Created for SE2017 - 757                                   
       
*********/

CREATE PROCEDURE [dbo].[cbmsLookup_GetByEntityDescription]
    (
        @Entity_Description VARCHAR(1000)
        , @Entity_Name VARCHAR(200) = NULL
    )
AS
    BEGIN


        SELECT
            e.ENTITY_ID
            , e.ENTITY_NAME
            , e.ENTITY_TYPE
            , e.ENTITY_DESCRIPTION
        FROM
            dbo.ENTITY e
        WHERE
            ENTITY_DESCRIPTION = @Entity_Description
            AND e.ENTITY_NAME IN (   SELECT
                                            CASE WHEN @Entity_Name IS NULL THEN e.ENTITY_NAME
                                                ELSE @Entity_Name
                                            END )
        ORDER BY
            e.DISPLAY_ORDER;
    END;
GO
GRANT EXECUTE ON  [dbo].[cbmsLookup_GetByEntityDescription] TO [CBMSApplication]
GO
