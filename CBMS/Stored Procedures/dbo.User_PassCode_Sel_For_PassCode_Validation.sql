SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  

 NAME: DV2.dbo.User_PassCode_Sel_For_PassCode_Validation  
 
 DESCRIPTION: To validate the Passcode details given by the user at login time.

 INPUT PARAMETERS:  
 Name			 DataType		Default Description  
------------------------------------------------------------  
 @username       VARCHAR(30)              
 
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 
 USAGE EXAMPLES:  
------------------------------------------------------------  
  
	 EXEC dbo.User_PassCode_Sel_For_PassCode_Validation 'dblock'
	 EXEC dbo.User_PassCode_Sel_For_PassCode_Validation 'vangundy'
	 EXEC dbo.User_PassCode_Sel_For_PassCode_Validation 'churchalla'
	 EXEC dbo.User_PassCode_Sel_For_PassCode_Validation 'megan'

	 SELECT * FROM USER_INFO WHERE USERNAME LIKE 'megan'
	 SELECT * FROM USER_INFO WHERE ACCESS_LEVEL IS NULL

 AUTHOR INITIALS:
 Initials	Name
------------------------------------------------------------
 PNR		Pandarinath
 HG			Harihara Suthan G

 MODIFICATIONS
 Initials	Date			Modification
------------------------------------------------------------
 PNR		10/12/2010		Created.
 HG			11/17/2010		Logic to get the User_Passcode_Reuse_limit value modified to have the common code for Internal/DV and SV users.

******/

CREATE PROCEDURE dbo.User_PassCode_Sel_For_PassCode_Validation
	(
	  @UserName VARCHAR(30)
	)
AS 
BEGIN  

    SET NOCOUNT ON ;

	DECLARE @User_PassCode_Reuse_Limit	SMALLINT;

	SELECT
		@User_PassCode_Reuse_Limit = 
									CASE
										WHEN ui.ACCESS_LEVEL = 1 THEN ch.User_PassCode_Reuse_Limit
										WHEN ui.ACCESS_LEVEL IS NULL THEN ac.App_Config_Value
										ELSE 1
									END
	FROM
		dbo.User_Info ui
		LEFT JOIN dbo.App_Config ac
			ON ac.App_Config_Cd = 'SV_User_PassCode_Reuse_Limit'
		LEFT OUTER JOIN Core.Client_Hier ch
			ON ch.Client_Id = ui.Client_Id
				AND ch.Sitegroup_Id = 0
	WHERE
		ui.USERNAME = @UserName

	SET @User_PassCode_Reuse_Limit = ISNULL(@User_PassCode_Reuse_Limit, 0);

	SELECT TOP(@User_PassCode_Reuse_Limit)
		up.User_PassCode_Id
		,up.PassCode
		,up.Active_From_Dt
		,up.Created_By_Id
	FROM
		dbo.User_PassCode up
		JOIN dbo.USER_INFO ui
			 ON up.user_info_id = ui.USER_INFO_ID
	WHERE
		ui.username = @username
	ORDER BY
		up.Last_Change_Ts DESC

END
GO
GRANT EXECUTE ON  [dbo].[User_PassCode_Sel_For_PassCode_Validation] TO [CBMSApplication]
GO
