SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
 NAME:        
  dbo.Client_Commodity_Sel_By_Client_commodity_Id      
       
 DESCRIPTION:        
  Used to get commodity id and site ids  data from CLIENT_COMMODITY table      
        
 INPUT PARAMETERS:        
 Name  DataType Default Description        
------------------------------------------------------------        
 @CLIENT_ID INT      
                  
 OUTPUT PARAMETERS:        
 Name   DataType  Default Description        
------------------------------------------------------------        
     
 USAGE EXAMPLES:        
------------------------------------------------------------      
 EXEC dbo.Client_Commodity_Sel_By_Client_commodity_Id  147    
 EXEC dbo.Client_Commodity_Sel_By_Client_commodity_Id  148    
     
       
 AUTHOR INITIALS:        
 Initials Name        
------------------------------------------------------------        
 GB   Geetansu Behera        
       
 MODIFICATIONS         
 Initials Date  Modification        
------------------------------------------------------------        
 GB     Created      
    
      
      
******/      
      
CREATE PROCEDURE dbo.Client_Commodity_Sel_By_Client_commodity_Id    
  @Client_commodity_Id AS INT     
AS     
BEGIN    
    SET NOCOUNT ON    
        
	DECLARE @commodity_id INT,@commodity_name VARCHAR(25),@is_alternate_fuel BIT, @site_ids VARCHAR(MAX)    
	SELECT @commodity_id = cc.Commodity_Id,@commodity_name = cm.Commodity_Name,@is_alternate_fuel = cm.Is_Alternate_Fuel, 
	@site_ids = COALESCE(@site_ids + ', ', '') + CAST(s.site_id AS varchar(10))    
	FROM core.client_commodity cc INNER JOIN dbo.Commodity cm ON cc.Commodity_Id = cm.Commodity_Id 
		INNER JOIN Site s ON cc.Client_Id=s.Client_ID    
	WHERE Client_Commodity_Id = @Client_commodity_Id    
	 
	SELECT @commodity_id As Commodity_id, @commodity_name As Commodity_name,@is_alternate_fuel As Is_Alternate_Fuel, @site_ids As Site_Id    
END 

GO
GRANT EXECUTE ON  [dbo].[Client_Commodity_Sel_By_Client_commodity_Id] TO [CBMSApplication]
GO
