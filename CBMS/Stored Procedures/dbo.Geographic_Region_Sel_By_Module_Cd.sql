SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
	
		dbo.Geographic_Region_Sel_By_Module_Cd
   
    
DESCRIPTION:   
   
  This procedure to get summary page details for deal ticket summery.
    
INPUT PARAMETERS:    
    Name			DataType       Default        Description    
-----------------------------------------------------------------------------    
	@Client_Id		INT
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec dbo.Geographic_Region_Sel_By_Module_Cd  
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	RR			Raghu Reddy   
    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	RR          2019-01-28  Global Risk Management - Created
     
    
******/  

CREATE PROCEDURE [dbo].[Geographic_Region_Sel_By_Module_Cd] ( @Module_Cd INT = NULL )
AS 
BEGIN  
      SET NOCOUNT ON;	
      
      SELECT
            gr.Geographic_Region_Id
           ,gr.Geographic_Region_Name
      FROM
            dbo.Geographic_Region gr
      WHERE
            @Module_Cd IS NULL
            OR gr.Module_Cd = @Module_Cd
      
  
END;
GO
GRANT EXECUTE ON  [dbo].[Geographic_Region_Sel_By_Module_Cd] TO [CBMSApplication]
GO
