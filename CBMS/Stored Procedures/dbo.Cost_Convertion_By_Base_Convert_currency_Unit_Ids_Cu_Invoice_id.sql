SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Cost_Convertion_By_Base_Convert_currency_Unit_Ids_Cu_Invoice_id          
              
Description:              
                     
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
  @Total_Cost							DECIMAL(28, 10)
  @Base_Currency_Unit_Id				INT
  @Convert_Currency_unit_Id				INT
  @Cu_Invoice_Id						INT
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
    @Account_Commodity_Invoice_Recalc_Type_Id			INT
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                
	
	BEGIN TRAN  
	
	EXEC Cost_Convertion_By_Base_Convert_currency_Unit_Ids_Cu_Invoice_id 500,'EUR','USD',35237309,107997
	
	ROLLBACK TRAN             
             
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2015-08-07		Created For AS400-II.         
             
******/ 
CREATE  PROCEDURE [dbo].[Cost_Convertion_By_Base_Convert_currency_Unit_Ids_Cu_Invoice_id]
      ( 
       @Total_Cost DECIMAL(28, 10)
      ,@Base_Currency_Unit_Name VARCHAR(200)
      ,@Convert_Currency_unit_Name VARCHAR(200)
      ,@Cu_Invoice_Id INT
      ,@Account_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 
	
      DECLARE @Service_Month DATETIME
      
      SELECT
            @Service_Month = MIN(cism.SERVICE_MONTH)
      FROM
            dbo.CU_INVOICE_SERVICE_MONTH cism
      WHERE
            cism.CU_INVOICE_ID = @Cu_Invoice_Id
     
            
      
      SELECT
            @Total_Cost * cuc.Conversion_Factor AS converted_Total_Cost
      FROM
            dbo.CURRENCY_UNIT_CONVERSION cuc
            INNER JOIN dbo.CURRENCY_UNIT bcu
                  ON bcu.CURRENCY_UNIT_ID = cuc.BASE_UNIT_ID
            INNER JOIN dbo.CURRENCY_UNIT ccu
                  ON ccu.CURRENCY_UNIT_ID = cuc.CONVERTED_UNIT_ID
            INNER JOIN core.Client_Hier ch
                  ON ch.Client_Currency_Group_Id = cuc.CURRENCY_GROUP_ID
            INNER JOIN core.Client_Hier_Account cha
                  ON cha.Client_Hier_Id = ch.Client_Hier_Id
      WHERE
            bcu.CURRENCY_UNIT_NAME = @Base_Currency_Unit_Name
            AND ccu.CURRENCY_UNIT_NAME = @Convert_Currency_unit_Name
            AND cha.Account_Id = @Account_Id
            AND cuc.CONVERSION_DATE = @Service_Month
            
      
            
END;

;
GO
GRANT EXECUTE ON  [dbo].[Cost_Convertion_By_Base_Convert_currency_Unit_Ids_Cu_Invoice_id] TO [CBMSApplication]
GO
