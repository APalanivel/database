SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[SR_Deal_Ticket_Account_Map_Del]  
     
DESCRIPTION: 
	It Deletes SR Deal Ticket Account Map for Selected Sr Deal Ticket Accounts Map Id.
      
INPUT PARAMETERS:          
NAME							DATATYPE	DEFAULT		DESCRIPTION          
-------------------------------------------------------------------          
@SR_Deal_Ticket_Account_Map_Id	INT	

   
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------

	Begin Tran

		EXEC SR_Deal_Ticket_Account_Map_Del  22

	Rollback Tran

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			27-MAY-10	CREATED

*/

CREATE PROCEDURE dbo.SR_Deal_Ticket_Account_Map_Del
   (
    @SR_Deal_Ticket_Account_Map_Id INT
   )
AS
BEGIN

    SET NOCOUNT ON;

    DELETE
   	FROM
		dbo.SR_DEAL_TICKET_ACCOUNT_MAP
	WHERE
		SR_DEAL_TICKET_ACCOUNT_MAP_ID = @SR_Deal_Ticket_Account_Map_Id

END
GO
GRANT EXECUTE ON  [dbo].[SR_Deal_Ticket_Account_Map_Del] TO [CBMSApplication]
GO
