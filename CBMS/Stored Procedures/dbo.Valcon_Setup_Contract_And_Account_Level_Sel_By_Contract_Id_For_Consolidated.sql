SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
                             
 NAME: dbo.Valcon_Setup_Contract_And_Account_Level_Sel_By_Contract_Id_For_Consolidated                         
                              
 DESCRIPTION:          
  Get the Contract & Account Level OCT rules.                       
                              
 INPUT PARAMETERS:          
                             
 Name                               DataType          Default       Description          
-------------------------------------------------------------------------------------      
 @Contract_Id      INT        
     
     
                            
 OUTPUT PARAMETERS:          
                                   
 Name                               DataType          Default       Description          
-------------------------------------------------------------------------------------                                
 USAGE EXAMPLES:                                  
-------------------------------------------------------------------------------------                     
  use cbms  
    
EXEC dbo.Valcon_Setup_Contract_And_Account_Level_Sel_By_Contract_Id_For_Consolidated
    @Contract_Id = 167450


EXEC dbo.Valcon_Setup_Contract_And_Account_Level_Sel_By_Contract_Id_For_Consolidated
    @Contract_Id = 167450

exec Valcon_Setup_Contract_And_Account_Level_Sel_By_Contract_Id_For_Consolidated
    167450

exec Valcon_Setup_Contract_And_Account_Level_Sel_By_Contract_Id_For_Consolidated
    172938
   
 Exec Valcon_Setup_Contract_And_Account_Level_Sel_By_Contract_Id_For_Consolidated 142302
                             
 AUTHOR INITIALS:        
           
 Initials                   Name          
-------------------------------------------------------------------------------------      
 NR                     Narayana Reddy                                
                               
 MODIFICATIONS:        
                               
 Initials               Date            Modification        
-------------------------------------------------------------------------------------      
 NR                     2019-06-13      Created for ADD Contract. 
 NR						2020-01-29		MAINT-9782 - Removed Is_processed filter. 
 NR						2020-04-29		MAINT-10157 - Removed contract filter on map table for un associated list to if invoice is mapped any other contract.
 NR						2020-06-01		MAINT-  - Contract Extension - To show the recalcs in account level if that recalcs not related passed contract.                           
******/
CREATE PROCEDURE [dbo].[Valcon_Setup_Contract_And_Account_Level_Sel_By_Contract_Id_For_Consolidated]
    (
        @Contract_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;



        CREATE TABLE #Associated_Invoice_List
             (
                 Account_Id INT
                 , Supplier_Contract_ID INT
                 , Invoice_Cnt INT
             );

        CREATE TABLE #Un_Associated_Invoice_List
             (
                 Account_Id INT
                 , Supplier_Contract_ID INT
                 , Invoice_Cnt INT
             );



        DECLARE
            @Supplier_Vendor_Type_Id INT
            , @Valcon_Account_Config_Type_Cd INT
            , @Contract_Associated_Invoice_Cnt INT
            , @Contract_Dis_Associated_Invoice_Cnt INT;;



        SELECT
            @Supplier_Vendor_Type_Id = e.ENTITY_ID
        FROM
            dbo.ENTITY e
        WHERE
            e.ENTITY_NAME = 'Supplier'
            AND e.ENTITY_TYPE = 155
            AND e.ENTITY_DESCRIPTION = 'Vendor';





        SELECT
            @Valcon_Account_Config_Type_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Valcon Config Type'
            AND c.Code_Value = 'Consolidated Account';

        CREATE TABLE #Account_Commodity_Invoice_Recalc_Type
             (
                 Account_Commodity_Invoice_Recalc_Type_Id INT
                 , Account_Id INT
                 , Commodity_ID INT
                 , Invoice_Recalc_Type_Cd INT
                 , Start_Dt DATE
                 , End_Dt DATE
                 , Source_Cd INT
                 , Contract_Id INT
             );



        CREATE TABLE #Client_Hier_Account
             (
                 Account_Id INT
                 , Account_Number VARCHAR(50)
                 , Meter_Id INT
                 , Contract_Start_Date DATE
                 , Contract_End_Date DATE
                 , Contract_Id INT
                 , Billing_Start_Dt DATE
                 , Billing_End_Dt DATE
                 , Client_Id INT
                 , Site_Id INT
                 , Commodity_ID INT
             );


        INSERT INTO #Client_Hier_Account
             (
                 Account_Id
                 , Account_Number
                 , Meter_Id
                 , Contract_Start_Date
                 , Contract_End_Date
                 , Contract_Id
                 , Billing_Start_Dt
                 , Billing_End_Dt
                 , Client_Id
                 , Site_Id
                 , Commodity_ID
             )
        SELECT
            utility_cha.Account_Id
            , utility_cha.Account_Number
            , utility_cha.Meter_Id
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE
            , c.CONTRACT_ID
            , acbv.Billing_Start_Dt
            , acbv.Billing_End_Dt
            , ch.Client_Id
            , ch.Site_Id
            , utility_cha.Commodity_Id
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account utility_cha
                ON utility_cha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN Core.Client_Hier_Account Supplier_Cha
                ON Supplier_Cha.Meter_Id = utility_cha.Meter_Id
            INNER JOIN dbo.CONTRACT c
                ON Supplier_Cha.Supplier_Contract_ID = c.CONTRACT_ID
            INNER JOIN dbo.Account_Consolidated_Billing_Vendor acbv
                ON utility_cha.Account_Id = acbv.Account_Id
        WHERE
            Supplier_Cha.Supplier_Contract_ID = @Contract_Id
            AND utility_cha.Account_Type = 'Utility'
            AND Supplier_Cha.Account_Type = 'Supplier'
            AND (   c.CONTRACT_START_DATE BETWEEN acbv.Billing_Start_Dt
                                          AND     ISNULL(acbv.Billing_End_Dt, '9999-12-31')
                    OR  c.CONTRACT_END_DATE BETWEEN acbv.Billing_Start_Dt
                                            AND     ISNULL(acbv.Billing_End_Dt, '9999-12-31')
                    OR  acbv.Billing_Start_Dt BETWEEN c.CONTRACT_START_DATE
                                              AND     c.CONTRACT_END_DATE
                    OR  ISNULL(acbv.Billing_End_Dt, '9999-12-31') BETWEEN c.CONTRACT_START_DATE
                                                                  AND     c.CONTRACT_END_DATE)
        GROUP BY
            utility_cha.Account_Id
            , utility_cha.Account_Number
            , utility_cha.Meter_Id
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE
            , c.CONTRACT_ID
            , acbv.Billing_Start_Dt
            , acbv.Billing_End_Dt
            , ch.Client_Id
            , ch.Site_Id
            , utility_cha.Commodity_Id;

        INSERT INTO #Un_Associated_Invoice_List
             (
                 Account_Id
                 , Supplier_Contract_ID
                 , Invoice_Cnt
             )
        SELECT
            cism.Account_ID
            , sal.Contract_Id
            , COUNT(DISTINCT cism.CU_INVOICE_ID)
        FROM
            dbo.CU_INVOICE_SERVICE_MONTH cism
            INNER JOIN #Client_Hier_Account sal
                ON sal.Account_Id = cism.Account_ID
        WHERE
            EXISTS (SELECT  1 FROM  dbo.CU_INVOICE ci WHERE cism.CU_INVOICE_ID = ci.CU_INVOICE_ID)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Contract_Cu_Invoice_Map cmpa
                               WHERE
                                    cmpa.Account_Id = sal.Account_Id
                                    AND cmpa.Cu_Invoice_Id = cism.CU_INVOICE_ID)
            AND (   cism.Begin_Dt BETWEEN sal.Contract_Start_Date
                                  AND     sal.Contract_End_Date
                    OR  cism.End_Dt BETWEEN sal.Contract_Start_Date
                                    AND     sal.Contract_End_Date
                    OR  sal.Contract_Start_Date BETWEEN cism.Begin_Dt
                                                AND     cism.End_Dt
                    OR  sal.Contract_End_Date BETWEEN cism.Begin_Dt
                                              AND     cism.End_Dt)
        GROUP BY
            cism.Account_ID
            , sal.Contract_Id;


        INSERT INTO #Associated_Invoice_List
             (
                 Account_Id
                 , Supplier_Contract_ID
                 , Invoice_Cnt
             )
        SELECT
            ccim.Account_Id
            , ccim.Contract_Id
            , COUNT(DISTINCT ccim.Cu_Invoice_Id)
        FROM
            dbo.Contract_Cu_Invoice_Map ccim
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.Account_ID = ccim.Account_Id
                   AND  cism.CU_INVOICE_ID = ccim.Cu_Invoice_Id
            INNER JOIN #Client_Hier_Account sal
                ON sal.Account_Id = ccim.Account_Id
                   AND  sal.Contract_Id = ccim.Contract_Id
        WHERE
            (   cism.Begin_Dt BETWEEN sal.Contract_Start_Date
                              AND     sal.Contract_End_Date
                OR  cism.End_Dt BETWEEN sal.Contract_Start_Date
                                AND     sal.Contract_End_Date
                OR  sal.Contract_Start_Date BETWEEN cism.Begin_Dt
                                            AND     cism.End_Dt
                OR  sal.Contract_End_Date BETWEEN cism.Begin_Dt
                                          AND     cism.End_Dt)
        GROUP BY
            ccim.Account_Id
            , ccim.Contract_Id;

        SELECT
            @Contract_Associated_Invoice_Cnt = SUM(ail.Invoice_Cnt)
        FROM
            #Associated_Invoice_List ail
        WHERE
            ail.Supplier_Contract_ID = @Contract_Id;
        SELECT
            @Contract_Dis_Associated_Invoice_Cnt = SUM(uail.Invoice_Cnt)
        FROM
            #Un_Associated_Invoice_List uail
        WHERE
            uail.Supplier_Contract_ID = @Contract_Id;
        -------------------------------------------------- Getting all recalcs in temp table and apply ---------------------------------
        DECLARE @Source_cd INT;

        SELECT
            @Source_cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Valcon Source Type'
            AND c.Code_Value = 'Contract';

        INSERT INTO #Account_Commodity_Invoice_Recalc_Type
             (
                 Account_Commodity_Invoice_Recalc_Type_Id
                 , Account_Id
                 , Commodity_ID
                 , Invoice_Recalc_Type_Cd
                 , Start_Dt
                 , End_Dt
                 , Source_Cd
                 , Contract_Id
             )
        SELECT
            acirt.Account_Commodity_Invoice_Recalc_Type_Id
            , acirt.Account_Id
            , acirt.Commodity_ID
            , c.Code_Id
            , acirt.Start_Dt
            , acirt.End_Dt
            , NULL AS Source_Cd
            , NULL AS Contract_Id
        FROM
            dbo.Account_Commodity_Invoice_Recalc_Type acirt
            INNER JOIN dbo.Code c
                ON acirt.Invoice_Recalc_Type_Cd = c.Code_Id
        WHERE
            EXISTS (   SELECT
                            1
                       FROM
                            #Client_Hier_Account sal
                       WHERE
                            sal.Account_Id = acirt.Account_Id
                            AND sal.Commodity_ID = acirt.Commodity_ID)
        GROUP BY
            acirt.Account_Commodity_Invoice_Recalc_Type_Id
            , acirt.Account_Id
            , acirt.Commodity_ID
            , c.Code_Id
            , acirt.Start_Dt
            , acirt.End_Dt;

        -------------------------------Consolidated Account Contract Level ---------------------------------------------   



        INSERT INTO #Account_Commodity_Invoice_Recalc_Type
             (
                 Account_Commodity_Invoice_Recalc_Type_Id
                 , Account_Id
                 , Commodity_ID
                 , Invoice_Recalc_Type_Cd
                 , Start_Dt
                 , End_Dt
                 , Source_Cd
                 , Contract_Id
             )
        SELECT
            acirt.Account_Commodity_Invoice_Recalc_Type_Id
            , acirt.Account_Id
            , acirt.Commodity_ID
            , c.Code_Id
            , acirt.Start_Dt
            , acirt.End_Dt
            , @Source_cd AS Source_Cd
            , crc.Contract_Id
        FROM
            Core.Client_Hier_Account u_cha
            INNER JOIN Core.Client_Hier_Account s_cha
                ON s_cha.Client_Hier_Id = u_cha.Client_Hier_Id
                   AND  s_cha.Meter_Id = u_cha.Meter_Id
            INNER JOIN dbo.Contract_Recalc_Config crc
                ON crc.Contract_Id = s_cha.Supplier_Contract_ID
            INNER JOIN dbo.Account_Commodity_Invoice_Recalc_Type acirt
                ON acirt.Account_Id = u_cha.Account_Id
                   AND  crc.Start_Dt = acirt.Start_Dt
                   AND  ISNULL(crc.End_Dt, '9999-12-31') = ISNULL(acirt.End_Dt, '9999-12-31')
            INNER JOIN dbo.Code c
                ON crc.Supplier_Recalc_Type_Cd = c.Code_Id
        WHERE
            acirt.Source_Cd = @Source_cd
            AND EXISTS (   SELECT
                                1
                           FROM
                                #Client_Hier_Account sal
                           WHERE
                                sal.Account_Id = acirt.Account_Id
                                AND sal.Commodity_ID = acirt.Commodity_ID)
            AND crc.Is_Consolidated_Contract_Config = 1
            AND u_cha.Account_Type = 'utility'
            AND s_cha.Account_Type = 'supplier'
        GROUP BY
            acirt.Account_Commodity_Invoice_Recalc_Type_Id
            , acirt.Account_Id
            , acirt.Commodity_ID
            , c.Code_Id
            , acirt.Start_Dt
            , acirt.End_Dt
            , crc.Contract_Id;


        SELECT
            'Contract Level' AS Configuration_Level
            , COUNT(DISTINCT cha.Meter_Id) AS Meter
            , ISNULL(@Contract_Associated_Invoice_Cnt, 0) AS Associated_Invoices
            , ISNULL(@Contract_Dis_Associated_Invoice_Cnt, 0) AS Un_Associated_Invoices
            , CASE WHEN LEN(uerc.Invoice_Recalc_Type) > 1 THEN
                       LEFT(uerc.Invoice_Recalc_Type, LEN(uerc.Invoice_Recalc_Type) - 1)
                  ELSE uerc.Invoice_Recalc_Type
              END AS Recalc_Type
            , aloct.OCT_Parameter_Cd
            , Parameter_Cd.Code_Value AS OCT_Parameter
            , aloct.OCT_Tolerance_Date_Cd
            , Dates_Cd.Code_Value AS OCT_Tolerance_Date
            , aloct.OCT_Dt_Range_Cd
            , Range_Cd.Code_Value AS OCT_Dt_Range
            , aloct.Config_Start_Dt
            , aloct.Config_End_Dt
            , CASE WHEN aloct.Contract_Outside_Contract_Term_Config_Id IS NOT NULL THEN 1
                  ELSE 0
              END OCT_Rule_Exist
            , vcc.Comment_Id
            , cnt.Comment_Text
            , ISNULL(vcc.Is_Config_Complete, 0) AS Is_Config_Complete
            , aloct.Contract_Outside_Contract_Term_Config_Id
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE
        FROM
            dbo.CONTRACT c
            LEFT JOIN #Client_Hier_Account cha
                ON cha.Contract_Id = c.CONTRACT_ID
            LEFT JOIN #Associated_Invoice_List cism
                ON cism.Supplier_Contract_ID = cha.Contract_Id
                   AND  cism.Account_Id = cha.Account_Id
            LEFT JOIN #Un_Associated_Invoice_List uail
                ON cha.Contract_Id = uail.Supplier_Contract_ID
                   AND  cha.Account_Id = uail.Account_Id
            CROSS APPLY (   SELECT
                                CAST(clsrt_Valu.Supplier_Recalc_Type_Cd AS VARCHAR(100)) + '|'
                                + Sup_Recalc_valu.Code_Value + '(' + CONVERT(VARCHAR, clsrt_Valu.Start_Dt, 106) + ' - '
                                + ISNULL(CONVERT(VARCHAR, clsrt_Valu.End_Dt, 106), 'Open') + ')' + ','
                            FROM
                                dbo.Contract_Recalc_Config clsrt_Valu
                                INNER JOIN dbo.Code Sup_Recalc_valu
                                    ON Sup_Recalc_valu.Code_Id = clsrt_Valu.Supplier_Recalc_Type_Cd
                            WHERE
                                clsrt_Valu.Contract_Id = c.CONTRACT_ID
                                AND clsrt_Valu.Is_Consolidated_Contract_Config = 1
                                AND (   c.CONTRACT_START_DATE BETWEEN clsrt_Valu.Start_Dt
                                                              AND     ISNULL(clsrt_Valu.End_Dt, '9999-12-31')
                                        OR  c.CONTRACT_END_DATE BETWEEN clsrt_Valu.Start_Dt
                                                                AND     ISNULL(clsrt_Valu.End_Dt, '9999-12-31')
                                        OR  clsrt_Valu.Start_Dt BETWEEN c.CONTRACT_START_DATE
                                                                AND     c.CONTRACT_END_DATE
                                        OR  ISNULL(clsrt_Valu.End_Dt, '9999-12-31') BETWEEN c.CONTRACT_START_DATE
                                                                                    AND     c.CONTRACT_END_DATE)
                            ORDER BY
                                clsrt_Valu.Start_Dt
                            FOR XML PATH('')) uerc(Invoice_Recalc_Type)
            LEFT JOIN dbo.Valcon_Contract_Config vcc
                ON vcc.Contract_Id = c.CONTRACT_ID
                   AND  vcc.Valcon_Account_Config_Type_Cd = @Valcon_Account_Config_Type_Cd
            LEFT JOIN(dbo.Contract_Outside_Contract_Term_Config aloct
                      INNER JOIN dbo.Valcon_Contract_Config vc
                          ON vc.Valcon_Contract_Config_Id = aloct.Valcon_Contract_Config_Id
                             AND vc.Valcon_Account_Config_Type_Cd = @Valcon_Account_Config_Type_Cd)
                ON vc.Contract_Id = c.CONTRACT_ID
            LEFT JOIN dbo.Code Parameter_Cd
                ON Parameter_Cd.Code_Id = aloct.OCT_Parameter_Cd
            LEFT JOIN dbo.Code Dates_Cd
                ON Dates_Cd.Code_Id = aloct.OCT_Tolerance_Date_Cd
            LEFT JOIN dbo.Code Range_Cd
                ON Range_Cd.Code_Id = aloct.OCT_Dt_Range_Cd
            LEFT JOIN(dbo.Comment cnt
                      INNER JOIN dbo.Code cde
                          ON cde.Code_Id = cnt.Comment_Type_CD
                             AND cde.Code_Value = 'Valcal Contract Comment')
                ON vcc.Comment_Id = cnt.Comment_ID
        WHERE
            c.CONTRACT_ID = @Contract_Id
        GROUP BY
            Parameter_Cd.Code_Value
            , Dates_Cd.Code_Value
            , Range_Cd.Code_Value
            , aloct.Config_Start_Dt
            , aloct.Config_End_Dt
            , CASE WHEN aloct.Contract_Outside_Contract_Term_Config_Id IS NOT NULL THEN 1
                  ELSE 0
              END
            , aloct.OCT_Tolerance_Date_Cd
            , aloct.OCT_Dt_Range_Cd
            , aloct.OCT_Parameter_Cd
            , CASE WHEN LEN(uerc.Invoice_Recalc_Type) > 1 THEN
                       LEFT(uerc.Invoice_Recalc_Type, LEN(uerc.Invoice_Recalc_Type) - 1)
                  ELSE uerc.Invoice_Recalc_Type
              END
            , cnt.Comment_Text
            , vcc.Comment_Id
            , ISNULL(vcc.Is_Config_Complete, 0)
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE
            , aloct.Contract_Outside_Contract_Term_Config_Id;


        SELECT
            'Account Level' AS Configuration_Level
            , cha.Account_Number
            , cha.Account_Id
            , cha.Contract_Start_Date AS Supplier_Account_begin_Dt
            , cha.Contract_End_Date AS Supplier_Account_End_Dt
            , COUNT(DISTINCT cha.Meter_Id) AS Meter
            , ISNULL(cism.Invoice_Cnt, 0) AS Associated_Invoices
            , ISNULL(uail.Invoice_Cnt, 0) AS Un_Associated_Invoices
            , CASE WHEN LEN(uerc.Invoice_Recalc_Type) > 1 THEN
                       LEFT(uerc.Invoice_Recalc_Type, LEN(uerc.Invoice_Recalc_Type) - 1)
                  ELSE uerc.Invoice_Recalc_Type
              END AS Recalc_Type
            , aloct.OCT_Parameter_Cd
            , Parameter_Cd.Code_Value AS OCT_Parameter
            , aloct.OCT_Tolerance_Date_Cd
            , Dates_Cd.Code_Value AS OCT_Tolerance_Date
            , aloct.OCT_Dt_Range_Cd
            , Range_Cd.Code_Value AS OCT_Dt_Range
            , aloct.Config_Start_Dt
            , aloct.Config_End_Dt
            , CASE WHEN aloct.Account_Outside_Contract_Term_Config_Id IS NOT NULL THEN 1
                  ELSE 0
              END OCT_Rule_Exist
            , cm.Comment_Text
            , vac.Comment_Id
            , ISNULL(vac.Is_Config_Complete, 0) AS Is_Config_Complete
            , aloct.Account_Outside_Contract_Term_Config_Id
            , cha.Client_Id
            , cha.Site_Id
            , src.Code_Value AS Source_Value
            , MIN(CASE WHEN src_acirt.Account_Commodity_Invoice_Recalc_Type_Id IS NOT NULL
                            AND src_acirt.Contract_Id = @Contract_Id
                            AND src_acirt.Source_Cd IS NOT NULL THEN 1
                      ELSE 0
                  END) Is_Contract_Level_Recalc
        FROM
            #Client_Hier_Account cha
            LEFT JOIN #Associated_Invoice_List cism
                ON cha.Contract_Id = cism.Supplier_Contract_ID
                   AND  cha.Account_Id = cism.Account_Id
            LEFT JOIN #Un_Associated_Invoice_List uail
                ON uail.Account_Id = cha.Account_Id
                   AND  uail.Supplier_Contract_ID = cha.Contract_Id
            CROSS APPLY (   SELECT
                                CAST(clsrt_Valu.Invoice_Recalc_Type_Cd AS VARCHAR(100)) + '|'
                                + Sup_Recalc_valu.Code_Value + '(' + CONVERT(VARCHAR, clsrt_Valu.Start_Dt, 106) + ' - '
                                + ISNULL(CONVERT(VARCHAR, clsrt_Valu.End_Dt, 106), 'Open') + ')' + ','
                            FROM
                                #Account_Commodity_Invoice_Recalc_Type clsrt_Valu
                                INNER JOIN dbo.Code Sup_Recalc_valu
                                    ON Sup_Recalc_valu.Code_Id = clsrt_Valu.Invoice_Recalc_Type_Cd
                            WHERE
                                clsrt_Valu.Account_Id = cha.Account_Id
                                AND clsrt_Valu.Commodity_ID = cha.Commodity_ID
                                AND (   cha.Contract_Start_Date BETWEEN clsrt_Valu.Start_Dt
                                                                AND     ISNULL(clsrt_Valu.End_Dt, '9999-12-31')
                                        OR  cha.Contract_End_Date BETWEEN clsrt_Valu.Start_Dt
                                                                  AND     ISNULL(clsrt_Valu.End_Dt, '9999-12-31')
                                        OR  clsrt_Valu.Start_Dt BETWEEN cha.Contract_Start_Date
                                                                AND     cha.Contract_End_Date
                                        OR  ISNULL(clsrt_Valu.End_Dt, '9999-12-31') BETWEEN cha.Contract_Start_Date
                                                                                    AND     cha.Contract_End_Date)
                            ORDER BY
                                clsrt_Valu.Start_Dt
                            FOR XML PATH('')) uerc(Invoice_Recalc_Type)
            LEFT JOIN dbo.Valcon_Account_Config vac
                ON vac.Account_Id = cha.Account_Id
                   AND  vac.Contract_Id = @Contract_Id
            LEFT JOIN dbo.Code src
                ON src.Code_Id = vac.Source_Cd
            LEFT JOIN dbo.Account_Outside_Contract_Term_Config aloct
                ON aloct.Account_Id = cha.Account_Id
                   AND  aloct.Contract_Id = cha.Contract_Id
            LEFT JOIN dbo.Code Parameter_Cd
                ON Parameter_Cd.Code_Id = aloct.OCT_Parameter_Cd
            LEFT JOIN dbo.Code Dates_Cd
                ON Dates_Cd.Code_Id = aloct.OCT_Tolerance_Date_Cd
            LEFT JOIN dbo.Code Range_Cd
                ON Range_Cd.Code_Id = aloct.OCT_Dt_Range_Cd
            LEFT JOIN(dbo.Comment cm
                      INNER JOIN dbo.Code cde
                          ON cde.Code_Id = cm.Comment_Type_CD
                             AND cde.Code_Value = 'Valcal Account Comment')
                ON cm.Comment_ID = vac.Comment_Id
            LEFT JOIN #Account_Commodity_Invoice_Recalc_Type src_acirt
                ON src_acirt.Account_Id = cha.Account_Id
                   AND  src_acirt.Commodity_ID = cha.Commodity_ID
                   AND  (   cha.Contract_Start_Date BETWEEN src_acirt.Start_Dt
                                                    AND     ISNULL(src_acirt.End_Dt, '9999-12-31')
                            OR  cha.Contract_End_Date BETWEEN src_acirt.Start_Dt
                                                      AND     ISNULL(src_acirt.End_Dt, '9999-12-31')
                            OR  src_acirt.Start_Dt BETWEEN cha.Contract_Start_Date
                                                   AND     cha.Contract_End_Date
                            OR  ISNULL(src_acirt.End_Dt, '9999-12-31') BETWEEN cha.Contract_Start_Date
                                                                       AND     cha.Contract_End_Date)
        GROUP BY
            cha.Account_Number
            , cha.Account_Id
            , Parameter_Cd.Code_Value
            , Dates_Cd.Code_Value
            , Range_Cd.Code_Value
            , aloct.Config_Start_Dt
            , aloct.Config_End_Dt
            , CASE WHEN aloct.Account_Outside_Contract_Term_Config_Id IS NOT NULL THEN 1
                  ELSE 0
              END
            , aloct.OCT_Tolerance_Date_Cd
            , aloct.OCT_Dt_Range_Cd
            , aloct.OCT_Parameter_Cd
            , cm.Comment_Text
            , vac.Comment_Id
            , CASE WHEN LEN(uerc.Invoice_Recalc_Type) > 1 THEN
                       LEFT(uerc.Invoice_Recalc_Type, LEN(uerc.Invoice_Recalc_Type) - 1)
                  ELSE uerc.Invoice_Recalc_Type
              END
            , cha.Contract_Start_Date
            , cha.Contract_End_Date
            , cha.Client_Id
            , cha.Site_Id
            , ISNULL(vac.Is_Config_Complete, 0)
            , src.Code_Value
            , ISNULL(cism.Invoice_Cnt, 0)
            , ISNULL(uail.Invoice_Cnt, 0)
            , aloct.Account_Outside_Contract_Term_Config_Id;


        DROP TABLE #Associated_Invoice_List;
        DROP TABLE #Client_Hier_Account;
        DROP TABLE #Un_Associated_Invoice_List;

    END;


























GO
GRANT EXECUTE ON  [dbo].[Valcon_Setup_Contract_And_Account_Level_Sel_By_Contract_Id_For_Consolidated] TO [CBMSApplication]
GO
