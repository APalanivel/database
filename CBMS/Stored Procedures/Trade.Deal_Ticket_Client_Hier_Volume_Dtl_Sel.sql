SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.Deal_Ticket_Client_Hier_Volume_Dtl_Sel                        
                          
Description:                          
        To get site's hedge configurations    
                          
Input Parameters:                          
    Name			DataType        Default     Description                            
--------------------------------------------------------------------------------    
	@Client_Id		INT    
    @Commodity_Id	INT    
    @Hedge_Type		INT    
    @Start_Dt		DATE    
    @End_Dt			DATE    
    @Contract_Id	VARCHAR(MAX)    
    @Site_Id		INT				NULL    
                          
Output Parameters:                                
	Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
                        
Usage Examples:                              
--------------------------------------------------------------------------------    
   
 EXEC Trade.Deal_Ticket_Client_Hier_Volume_Dtl_Sel  291,'18508,18520'  
 EXEC Trade.Deal_Ticket_Client_Hier_Volume_Dtl_Sel  498,'18508'  
 EXEC Trade.Deal_Ticket_Client_Hier_Volume_Dtl_Sel  288,'18508,18520' 
 
 EXEC Trade.Deal_Ticket_Client_Hier_Volume_Dtl_Sel 132006  
    
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR          2019-02-08  Created GRM    
	RR			2019-11-09	GRM-1487 Added @Trade_Number input and modified script to pull executed months only in the trade
	RR			18-10-2019	GRM-1573 Added Total_Volume_Monthly to retrun volumes in monthly always, required to disaply invidual
							site/account volumes in respective frequency but the totals should always at be monthly

******/
CREATE PROCEDURE [Trade].[Deal_Ticket_Client_Hier_Volume_Dtl_Sel]
    (
        @Deal_Ticket_Id INT
        , @Client_Hier_Id VARCHAR(MAX) = NULL
        , @Trade_Number INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Deal_Ticket_Frequency VARCHAR(25);

        SELECT
            @Deal_Ticket_Frequency = cd.Code_Value
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN dbo.Code cd
                ON cd.Code_Id = dt.Deal_Ticket_Frequency_Cd
        WHERE
            dt.Deal_Ticket_Id = @Deal_Ticket_Id;

        SELECT
            ch.Client_Hier_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
            , cha.Account_Number
            , vol.Deal_Month
            , vol.Total_Volume
            , RTRIM(ch.City) + ', ' + ch.State_Name AS site_Address
            , MAX(sup_cha.Account_Vendor_Name) AS Supplier
            , c.ED_CONTRACT_NUMBER
            , c.CONTRACT_ID
            , tp.Trade_Price AS price
            , dthv.Trigger_Target_Price
            , CASE WHEN @Deal_Ticket_Frequency = 'Monthly' THEN vol.Total_Volume
                  WHEN @Deal_Ticket_Frequency = 'Daily' THEN vol.Total_Volume * dd.DAYS_IN_MONTH_NUM
              END AS Total_Volume_Monthly
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl vol
                ON dt.Deal_Ticket_Id = vol.Deal_Ticket_Id
            INNER JOIN Core.Client_Hier ch
                ON vol.Client_Hier_Id = ch.Client_Hier_Id
            LEFT JOIN Core.Client_Hier_Account cha
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
                   AND  vol.Account_Id = cha.Account_Id
                   AND  cha.Account_Type = 'utility'
            LEFT JOIN dbo.CONTRACT c
                ON c.CONTRACT_ID = vol.Contract_Id
            LEFT JOIN Core.Client_Hier_Account sup_cha
                ON sup_cha.Client_Hier_Id = cha.Client_Hier_Id
                   AND  sup_cha.Meter_Id = cha.Meter_Id
                   AND  sup_cha.Supplier_Contract_ID = vol.Contract_Id
                   AND  sup_cha.Account_Type = 'supplier'
            INNER JOIN Trade.Deal_Total_Hedge_Volume dthv
                ON dthv.Deal_Ticket_Id = dt.Deal_Ticket_Id
                   AND  vol.Deal_Month = dthv.Deal_Month
            LEFT JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON ch.Client_Hier_Id = dtch.Client_Hier_Id
                   AND  dt.Deal_Ticket_Id = dtch.Deal_Ticket_Id
            LEFT JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
                   AND  vol.Deal_Month = chws.Trade_Month
            LEFT JOIN Trade.Deal_Ticket_Client_Hier_TXN_Status trd
                ON chws.Deal_Ticket_Client_Hier_Workflow_Status_Id = trd.Deal_Ticket_Client_Hier_Workflow_Status_Id
            LEFT JOIN Trade.Trade_Price tp
                ON trd.Trade_Price_Id = tp.Trade_Price_Id
            LEFT JOIN meta.DATE_DIM dd
                ON dd.DATE_D = vol.Deal_Month
        WHERE
            dt.Deal_Ticket_Id = @Deal_Ticket_Id
            AND (   @Client_Hier_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Client_Hier_Id, ',')
                                   WHERE
                                        CAST(Segments AS INT) = ch.Client_Hier_Id))
            AND (   (@Trade_Number IS NULL)
                    OR  (   vol.Trade_Number = @Trade_Number
                            AND tp.Trade_Price IS NOT NULL
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                Trade.Deal_Ticket_Client_Hier dtch
                                                INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                                                    ON chws.Deal_Ticket_Client_Hier_Id = dtch.Deal_Ticket_Client_Hier_Id
                                                INNER JOIN Trade.Workflow_Status_Map wsm
                                                    ON wsm.Workflow_Status_Map_Id = chws.Workflow_Status_Map_Id
                                                INNER JOIN Trade.Workflow_Status ws
                                                    ON ws.Workflow_Status_Id = wsm.Workflow_Status_Id
                                           WHERE
                                                dtch.Deal_Ticket_Id = @Deal_Ticket_Id
                                                AND dtch.Deal_Ticket_Id = dt.Deal_Ticket_Id
                                                AND vol.Client_Hier_Id = dtch.Client_Hier_Id
                                                AND vol.Deal_Month = chws.Trade_Month
                                                AND chws.Is_Active = 1
                                                AND ws.Workflow_Status_Name = 'Order Executed')))
        GROUP BY
            ch.Client_Hier_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
            , cha.Account_Number
            , vol.Deal_Month
            , vol.Total_Volume
            , RTRIM(ch.City) + ', ' + ch.State_Name
            , c.CONTRACT_ID
            , c.ED_CONTRACT_NUMBER
            , dthv.Trigger_Target_Price
            , tp.Trade_Price
            , dd.DAYS_IN_MONTH_NUM;

    END;








GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Client_Hier_Volume_Dtl_Sel] TO [CBMSApplication]
GO
