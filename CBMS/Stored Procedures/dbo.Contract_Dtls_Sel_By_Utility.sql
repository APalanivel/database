
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:      
  dbo.Contract_Dtls_Sel_By_Utility     
      
DESCRIPTION:      
  to get the contract_dtls for the given utility account    
      
INPUT PARAMETERS:      
 Name     DataType  Default    Description      
-------------------------------------------------------------------------------------    
 @Account_Id   INT    
 @Cu_Invoice_Id   INT    
 @commodity_Id   INT    
    
OUTPUT PARAMETERS:      
 Name     DataType  Default    Description      
-------------------------------------------------------------------------------------    
      
USAGE EXAMPLES:      
-------------------------------------------------------------------------------------      
exec Contract_Dtls_Sel_By_Utility 206098,18604132,290    
    
exec Contract_Dtls_Sel_By_Utility 194774,16517207,290     
    
exec Contract_Dtls_Sel_By_Utility 67861,13505316,291    
    
exec Contract_Dtls_Sel_By_Utility 258880,25773598,290    
  
exec Contract_Dtls_Sel_By_Utility 55262,32227387,290  
  
exec Contract_Dtls_Sel_By_Utility 76404,32412669,291  
  
  
    
    
--Prod example    
    
exec Contract_Dtls_Sel_By_Utility 152402,42402537,290    
      
      
AUTHOR INITIALS:      
 Initials Name      
-------------------------------------------------------------------------------------      
 RKV  Ravi Kumar Vegesna      
 NR   Narayana Reddy    
       
MODIFICATIONS      
      
 Initials Date   Modification      
-------------------------------------------------------------------------------------      
 RKV  2016-01-27  Created for AS400-PII     
 NR   2016-07-14  MAINT-4103  Modifude filter condition to get the contracts in betwenn        
       Supplier_Account_begin_Dt and End_Dt.    
 NR   2017-10-13  SE2017-263 - Added Begin_Dt,End_Dt column in output list.  
                
                  
******/      
CREATE PROCEDURE [dbo].[Contract_Dtls_Sel_By_Utility]  
      (   
       @Account_Id INT  
      ,@Cu_Invoice_Id INT  
      ,@commodity_Id INT = NULL )  
AS   
BEGIN        
      SET NOCOUNT ON;        
           
      SELECT  
            c.CONTRACT_ID  
           ,c.ED_CONTRACT_NUMBER  
           ,c.CONTRACT_START_DATE  
           ,c.CONTRACT_END_DATE  
           ,cha.Meter_State_Id  
           ,cism.Begin_Dt AS Invoice_Begin_Dt  
           ,cism.End_Dt AS Invoice_End_Dt  
           ,ciacrc.Contract_Recalc_Begin_Dt  
           ,ciacrc.Contract_Recalc_End_Dt  
           ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Updated_User  
           ,ciacrc.Last_Change_Ts  
      FROM  
            core.Client_Hier_Account cha  
            INNER JOIN core.Client_Hier_Account chat  
                  ON chat.Meter_Id = cha.Meter_Id  
            INNER JOIN dbo.CONTRACT c  
                  ON chat.Supplier_Contract_ID = c.CONTRACT_ID  
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism  
                  ON cism.Account_ID = cha.Account_Id  
            LEFT OUTER JOIN ( dbo.Cu_Invoice_Account_Commodity ciac  
                              INNER JOIN dbo.Cu_Invoice_Account_Commodity_Recalc_Contract ciacrc  
                                    ON ciac.Cu_Invoice_Account_Commodity_Id = ciacrc.Cu_Invoice_Account_Commodity_Id )  
                              ON cism.CU_INVOICE_ID = ciac.Cu_Invoice_Id  
                                 AND cism.Account_ID = ciac.Account_Id  
                                 AND ciac.Commodity_Id = cha.Commodity_Id  
                                 AND c.CONTRACT_ID = ciacrc.Contract_Id  
            LEFT JOIN dbo.USER_INFO ui  
                  ON ui.USER_INFO_ID = ciacrc.Updated_User_Id  
      WHERE  
            cha.Account_Type = 'Utility'  
            AND chat.Account_Type = 'supplier'  
            AND cha.Account_Id = @Account_Id  
           AND ((cism.Begin_Dt BETWEEN chat.Supplier_Account_begin_Dt  
                                   AND     chat.Supplier_Account_End_Dt)
            or ( cism.End_Dt BETWEEN chat.Supplier_Account_begin_Dt  
                           AND     chat.Supplier_Account_End_Dt  ))
            AND cism.CU_INVOICE_ID = @Cu_Invoice_Id  
            AND ( @commodity_Id IS NULL  
                  OR cha.Commodity_Id = @commodity_Id )  
      GROUP BY  
            c.CONTRACT_ID  
,c.ED_CONTRACT_NUMBER  
           ,c.CONTRACT_START_DATE  
           ,c.CONTRACT_END_DATE  
           ,cha.Meter_State_Id  
           ,cism.Begin_Dt  
           ,cism.End_Dt  
           ,ciacrc.Contract_Recalc_Begin_Dt  
           ,ciacrc.Contract_Recalc_End_Dt  
           ,ui.FIRST_NAME + ' ' + ui.LAST_NAME  
           ,ciacrc.Last_Change_Ts     
         
             
END;      
      
  
;
;
GO


GRANT EXECUTE ON  [dbo].[Contract_Dtls_Sel_By_Utility] TO [CBMSApplication]
GO
