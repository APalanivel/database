SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** 

NAME:
		dbo.Contract_Registration_Notifications_Count 

DESCRIPTION: 
				To get the egistration notifications count

INPUT PARAMETERS: 
Name						DataType		Default		Description 
----------------------------------------------------------------------
@Date						DATETIME


OUTPUT PARAMETERS:   
Name						DataType	Default		Description 
---------------------------------------------------------------------- 

USAGE EXAMPLES:   
----------------------------------------------------------------------   

	EXEC dbo.Contract_Registration_Notifications_Count NULL,'2016-06-10'
	EXEC dbo.Contract_Registration_Notifications_Count NULL,'2016-06-11'
	EXEC dbo.Contract_Registration_Notifications_Count NULL,'2016-06-12'
	EXEC dbo.Contract_Registration_Notifications_Count NULL,'2016-06-13'
	EXEC dbo.Contract_Registration_Notifications_Count 21047,'2016-06-12'
	EXEC dbo.Contract_Registration_Notifications_Count 42670,'2016-06-12'
	EXEC dbo.Contract_Registration_Notifications_Count 49,'2016-06-12'



AUTHOR INITIALS:   
	Initials	Name   
----------------------------------------------------------------------   
	RR			Narayana Reddy


MODIFICATIONS:  
	Initials	Date		Modification 
-------------------------------------------------------------------------------   
	RR			2016-06-03  Created  GCS-988 GCS-Phase-5
	 		 
******/
CREATE PROCEDURE [dbo].[Contract_Registration_Notifications_Count]
      ( 
       @Analyst_Id INT = NULL
      ,@Date DATETIME = NULL )
AS 
BEGIN 

      SET NOCOUNT ON;
      
      DECLARE
            @Total_Count AS INT
           ,@Not_Sent_To_Supp_Count INT;
      
      DECLARE @Tbl_Registration TABLE
            ( 
             Contract_Id INT
            ,Meter_Start_End_Date DATE
            ,Notification_Msg_Queue_Id INT );
            
      DECLARE @Cnt_Registration TABLE
            ( 
             Contract_Id INT
            ,Meter_Start_End_Date DATE
            ,Notification_Msg_Queue_Id INT );
            
      DECLARE
            @Default_Analyst INT
           ,@Custom_Analyst INT;
        
      SELECT
            @Default_Analyst = MAX(CASE WHEN c.Code_Value = 'Default' THEN c.Code_Id
                                   END)
           ,@Custom_Analyst = MAX(CASE WHEN c.Code_Value = 'Custom' THEN c.Code_Id
                                  END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Analyst Type';
      
      --SELECT
      --      @Date = isnull(@Date, convert(VARCHAR(10), getdate(), 101))
                  
      INSERT      INTO @Tbl_Registration
                  ( 
                   Contract_Id
                  ,Meter_Start_End_Date
                  ,Notification_Msg_Queue_Id )
                  SELECT
                        fvl.Contract_Id
                       ,fvl.Meter_Term_Dt AS Meter_Start_End_Date
                       ,MAX(fvl.Notification_Msg_Queue_Id) AS Notification_Msg_Queue_Id
                  FROM
                        dbo.Contract_Notification_Log fvl
                        INNER JOIN dbo.Notification_Msg_Queue nmq
                              ON fvl.Notification_Msg_Queue_Id = nmq.Notification_Msg_Queue_Id
                        INNER JOIN dbo.Notification_Template nt
                              ON nmq.Notification_Template_Id = nt.Notification_Template_Id
                        INNER JOIN dbo.Code cd
                              ON cd.Code_Id = nt.Notification_Type_Cd
                        INNER JOIN dbo.Codeset cs
                              ON cd.Codeset_Id = cd.Codeset_Id
                  WHERE
                        cd.Code_Value = 'Registration Notification'
                        AND cs.Codeset_Name = 'Notification Type'
                        AND ( CONVERT(DATE, fvl.Last_Change_Ts, 101) BETWEEN CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD, -90, @Date), 101))
                                                                     AND     @Date )
                  GROUP BY
                        fvl.Contract_Id
                       ,fvl.Meter_Term_Dt;

      INSERT      INTO @Cnt_Registration
                  ( 
                   Contract_Id
                  ,Meter_Start_End_Date
                  ,Notification_Msg_Queue_Id )
                  SELECT
                        cr.Contract_Id
                       ,cr.Meter_Start_End_Date
                       ,cr.Notification_Msg_Queue_Id
                  FROM
                        @Tbl_Registration cr
                        INNER JOIN Core.Client_Hier_Account chasupp
                              ON cr.Contract_Id = chasupp.Supplier_Contract_ID
                                 AND cr.Meter_Start_End_Date = chasupp.Supplier_Meter_Association_Date
                        INNER JOIN Core.Client_Hier ch
                              ON chasupp.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN Core.Client_Hier_Account chautil
                              ON ch.Client_Hier_Id = chautil.Client_Hier_Id
                                 AND chautil.Meter_Id = chasupp.Meter_Id
                        LEFT JOIN dbo.VENDOR_COMMODITY_MAP vcm
                              ON chautil.Account_Vendor_Id = vcm.VENDOR_ID
                                 AND chautil.Commodity_Id = vcm.COMMODITY_TYPE_ID
                        LEFT JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                              ON vcm.VENDOR_COMMODITY_MAP_ID = vcam.Vendor_Commodity_Map_Id
                        INNER JOIN Core.Client_Commodity ccc
                              ON ccc.Client_Id = ch.Client_Id
                                 AND ccc.Commodity_Id = chasupp.Commodity_Id
                        LEFT JOIN dbo.Account_Commodity_Analyst aca
                              ON aca.Account_Id = chautil.Account_Id
                                 AND aca.Commodity_Id = chautil.Commodity_Id
                        LEFT JOIN dbo.Site_Commodity_Analyst sca
                              ON sca.Site_Id = ch.Site_Id
                                 AND sca.Commodity_Id = chasupp.Commodity_Id
                        LEFT JOIN dbo.Client_Commodity_Analyst cca
                              ON ccc.Client_Commodity_Id = cca.Client_Commodity_Id
                  WHERE
                        chautil.Account_Type = 'Utility'
                        AND chasupp.Account_Type = 'Supplier'
                        AND ( @Analyst_Id IS NULL
                              OR @Analyst_Id = CASE WHEN COALESCE(chautil.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) = @Default_Analyst THEN vcam.Analyst_Id
                                                    WHEN COALESCE(chautil.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) = @Custom_Analyst THEN COALESCE(aca.Analyst_User_Info_Id, sca.Analyst_User_Info_Id, cca.Analyst_User_Info_Id)
                                               END )
                  GROUP BY
                        cr.Contract_Id
                       ,cr.Meter_Start_End_Date
                       ,cr.Notification_Msg_Queue_Id;
      SELECT
            @Total_Count = COUNT(1)
      FROM
            @Cnt_Registration;
     
      SELECT
            @Not_Sent_To_Supp_Count = COUNT(1)
      FROM
            @Cnt_Registration tr
      WHERE
            NOT EXISTS ( SELECT
                              1
                         FROM
                              dbo.Notification_Msg_Dtl nmd
                         WHERE
                              tr.Notification_Msg_Queue_Id = nmd.Notification_Msg_Queue_Id );
                              
      SELECT
            @Total_Count AS Total_Count
           ,@Not_Sent_To_Supp_Count AS Not_Sent_To_Supp_Count;
  
END;
;
GO
GRANT EXECUTE ON  [dbo].[Contract_Registration_Notifications_Count] TO [CBMSApplication]
GO
