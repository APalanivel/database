
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 dbo.Report_Client_CEM_CEA_CSA    
     
DESCRIPTION:    
 Gets the list of CEM,CEA and CSA for the list of Clients. used by Report  
         
INPUT PARAMETERS:    
Name               DataType           Default           Description    
-------------------------------------------------------------------------------------------    
 @Client_Id_List   VARCHAR(max)  
 @UserName         VARCHAR(200)                           
 @Client_Managed   INT    
    
OUTPUT PARAMETERS:    
Name   DataType  Default Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
-------------------------------------------------------------    
 EXEC dbo.Report_Client_CEM_CEA_CSA '235,11231',' (All)',-1     
 EXEC dbo.Report_Client_CEM_CEA_CSA '235',' (All)',-1     
 EXEC dbo.Report_Client_CEM_CEA_CSA_IC '210',' (All)',-1     
 
     
AUTHOR INITIALS:    
Initials    Name    
------------------------------------------------------------    
 AKR        Ashok Kumar Raju  
  
 MODIFICATIONS   
 Initials  Date        Modification    
------------------------------------------------------------    
 AKR       2012-03-20  Created    
 AKR       2012-04-04  Modified to add CEM VP Column  
 AKR       2012-09-24  Added the Client Comment Columns
 SP		   2015-03-13  For Ticket 130365 Added Active_Site_Count column in select list.
 LEC	   2016-06-14  Added Client ID
 LEC	   2017-04-13  Added Invoice Collection data
*/
CREATE PROCEDURE [dbo].[Report_Client_CEM_CEA_CSA]
      (
       @Client_Id_List VARCHAR(MAX)
      ,@UserName VARCHAR(200)
      ,@Client_Managed INT )
AS
BEGIN       
      
      SET NOCOUNT ON;      
      SELECT
            c.CLIENT_NAME
           ,c.CLIENT_ID
           ,e.ENTITY_NAME
           ,CASE WHEN c.NOT_MANAGED = 0 THEN 'Active'
                 ELSE 'InActive'
            END AS Client_Status
           ,left(CEM_Names, len(CEM_Names) - 1) CEM
           ,left(CEA_Names, len(CEA_Names) - 1) CEA
           ,left(CSA_Names, len(CSA_Names) - 1) CSA
           ,left(RCT_Names, len(RCT_Names) - 1) RCT
           ,max(CASE WHEN cd.Code_Value = 'CEAComment' THEN com.Comment_Text
                END) [CEA Comment]
           ,max(CASE WHEN cd.Code_Value = 'CEMComment' THEN com.Comment_Text
                END) [CEM Comment]
           ,max(CASE WHEN cd.Code_Value = 'CSAComment' THEN com.Comment_Text
                END) [CSA Comment]
           ,count(DISTINCT s.SITE_ID) AS Active_Site_Count
           ,cast(ic.Invoice_Collection_Setup_Instruction_Comment AS VARCHAR(1000)) [Invoice Collection Setup Instructions]
           ,ic.Invoice_Collection_Service_Start_Dt [Invoice Collection Start Dt]
           ,ic.Invoice_Collection_Service_End_Dt [Invoice Collection End Dt]
           ,uic.FIRST_NAME + ' ' + uic.LAST_NAME [Main Collection Officer]
      FROM
            dbo.CLIENT c
            INNER JOIN dbo.ENTITY e
                  ON c.CLIENT_TYPE_ID = e.ENTITY_ID
            CROSS APPLY ( SELECT
                              ui.FIRST_NAME + ' ' + ui.LAST_NAME + ','
                          FROM
                              dbo.CLIENT_CEM_MAP cem
                              INNER JOIN dbo.USER_INFO ui
                                    ON cem.USER_INFO_ID = ui.USER_INFO_ID
                          WHERE
                              cem.CLIENT_ID = c.CLIENT_ID
                              AND ui.IS_HISTORY = 0
                          ORDER BY
                              ui.USERNAME
            FOR
                          XML PATH('') ) cem ( CEM_Names )
            CROSS APPLY ( SELECT
                              ui.FIRST_NAME + ' ' + ui.LAST_NAME + ','
                          FROM
                              dbo.CLIENT_CEA_MAP cea
                              INNER JOIN dbo.USER_INFO ui
                                    ON cea.USER_INFO_ID = ui.USER_INFO_ID
                          WHERE
                              cea.CLIENT_ID = c.CLIENT_ID
                              AND ui.IS_HISTORY = 0
                          ORDER BY
                              ui.USERNAME
            FOR
                          XML PATH('') ) cea ( CEA_Names )
            CROSS APPLY ( SELECT
                              ui.FIRST_NAME + ' ' + ui.LAST_NAME + ','
                          FROM
                              dbo.CLIENT_CSA_MAP csa
                              INNER JOIN dbo.USER_INFO ui
                                    ON csa.USER_INFO_ID = ui.USER_INFO_ID
                          WHERE
                              csa.CLIENT_ID = c.CLIENT_ID
                              AND ui.IS_HISTORY = 0
                          ORDER BY
                              ui.USERNAME
            FOR
                          XML PATH('') ) csa ( CSA_Names )
            CROSS APPLY ( SELECT
                              ui.FIRST_NAME + ' ' + ui.LAST_NAME + ','
                          FROM
                              dbo.CLIENT_CEM_MAP ccm
                              INNER JOIN dbo.RM_CEM_TEAM rct
                                    ON ccm.USER_INFO_ID = rct.CEM_USER_ID
                              INNER JOIN dbo.USER_INFO ui
                                    ON rct.CEM_PRACTICE_HEAD_ID = ui.USER_INFO_ID
                          WHERE
                              ccm.CLIENT_ID = c.CLIENT_ID
                              AND ui.IS_HISTORY = 0
                          GROUP BY
                              ui.FIRST_NAME + ' ' + ui.LAST_NAME + ','
            FOR
                          XML PATH('') ) rct ( RCT_Names )
            LEFT JOIN dbo.Client_Comment_Map ccm
                  ON ccm.Client_Id = c.CLIENT_ID
            LEFT JOIN dbo.Comment com
                  ON com.Comment_ID = ccm.Comment_Id
            LEFT JOIN dbo.Code cd
                  ON cd.Code_Id = com.Comment_Type_CD
            LEFT JOIN dbo.SITE s
                  ON s.Client_ID = c.CLIENT_ID
                     AND s.NOT_MANAGED = 0
            LEFT JOIN dbo.Invoice_Collection_Client_Config ic
                  ON ic.Client_Id = c.CLIENT_ID
            LEFT JOIN USER_INFO uic
                  ON uic.USER_INFO_ID = ic.Invoice_Collection_Officer_User_Id
      WHERE
            ( c.CLIENT_ID IN ( SELECT
                                    Segments
                               FROM
                                    dbo.ufn_split(@Client_Id_List, ',') ) )
            AND CEM_Names IS NOT NULL
            AND ( @UserName = ' (All)'
                  OR CEM_Names LIKE '%' + @UserName + '%' )
            AND ( @Client_Managed = -1
                  OR c.NOT_MANAGED = @Client_Managed )
      GROUP BY
            c.CLIENT_NAME
           ,c.CLIENT_ID
           ,e.ENTITY_NAME
           ,c.NOT_MANAGED
           ,left(CEM_Names, len(CEM_Names) - 1)
           ,left(CEA_Names, len(CEA_Names) - 1)
           ,left(CSA_Names, len(CSA_Names) - 1)
           ,left(RCT_Names, len(RCT_Names) - 1)
           ,ic.Invoice_Collection_Service_Start_Dt
           ,ic.Invoice_Collection_Service_End_Dt
           ,ic.Invoice_Collection_Setup_Instruction_Comment
           ,uic.FIRST_NAME + ' ' + uic.LAST_NAME;    
  
END;


;

;

;
GO



GRANT EXECUTE ON  [dbo].[Report_Client_CEM_CEA_CSA] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_Client_CEM_CEA_CSA] TO [CBMSApplication]
GO
