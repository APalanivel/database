SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_CONTRACT_UPLOADED_RC_CLIENT_LIST_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@rfpId         	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--exec dbo.SR_RFP_GET_CONTRACT_UPLOADED_RC_CLIENT_LIST_P '','',572





CREATE      PROCEDURE dbo.SR_RFP_GET_CONTRACT_UPLOADED_RC_CLIENT_LIST_P
@userId varchar(10),
@sessionId varchar(20),
@rfpId int

AS
select 	c.client_id, c.client_name
from 	sr_rfp_account rfpAcc(nolock)
	join account a(nolock) on rfpAcc.account_id = a.account_id and rfpAcc.sr_rfp_id = @rfpId
	join vwSiteName vwSite(nolock) on vwSite.site_id = a.site_id
	join client c(nolock) on c.client_id = vwSite.client_id
	left join sr_rc_contract_document_accounts_map accMap (nolock) on accMap.account_id = a.account_id
	     and accMap.sr_rc_contract_document_id is not null

group by c.client_id , c.client_name

order by c.client_name
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_CONTRACT_UPLOADED_RC_CLIENT_LIST_P] TO [CBMSApplication]
GO
