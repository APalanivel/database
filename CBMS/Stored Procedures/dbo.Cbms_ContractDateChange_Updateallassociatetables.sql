SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  dbo.Cbms_ContractDateChange_Updateallassociatetables  

 DESCRIPTION:   
			This procedure is called to update the dates in the contract associated tables 
 
 INPUT PARAMETERS:
 Name					DataType	Default Description
------------------------------------------------------------
 @Contract_Number		VARCHAR(50)
 @Change_Start_Date		DATETIME
 @Change_End_Date		DATETIME
 
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  

 USAGE EXAMPLES:  
------------------------------------------------------------  

BEGIN TRAN
	 
	EXEC dbo.Cbms_ContractDateChange_Updateallassociatetables '86437-0001',NULL,'2017-11-23'
	
	SELECT CONTRACT_ID,CONTRACT_START_DATE,CONTRACT_END_DATE  FROM dbo.CONTRACT WHERE ED_CONTRACT_NUMBER = '86437-0001'
	SELECT ACCOUNT_ID, Contract_ID,METER_ID,METER_ASSOCIATION_DATE,METER_DISASSOCIATION_DATE FROM dbo.SUPPLIER_ACCOUNT_METER_MAP WHERE Contract_ID = 86437 -- acc - 447123
	SELECT ACCOUNT_ID,Supplier_Account_Begin_Dt,Supplier_Account_End_Dt FROM dbo.ACCOUNT WHERE ACCOUNT_ID = 447088

ROLLBACK TRAN

 
 AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 RR			Raghu Reddy
 
 MODIFICATIONS:  
 Initials	Date			Modification  
------------------------------------------------------------  
  RR			11/21/2011  Created. This procedure is part of dbo.cbmsContract_ChangeDate which is used in .net contract date change tool, the procedure 
							is splitted to reuse the same in .net contract date change tool and edit contract page in java to avoid the duplication 
							of functionality.. The procedure accepts only one date start/end date and other date should be NULL
  NR			2019-10-31	Add Contract - Added new config table.
******/

CREATE PROCEDURE [dbo].[Cbms_ContractDateChange_Updateallassociatetables]
    (
        @Contract_Number VARCHAR(50)
        , @Change_Start_Date DATETIME = NULL
        , @Change_End_Date DATETIME = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;


        DECLARE @Contract_Id INT;
        DECLARE @Current_Start_Date DATETIME;
        DECLARE @Current_End_Date DATETIME;


        SELECT
            @Contract_Id = CONTRACT_ID
            , @Current_Start_Date = CONTRACT_START_DATE
            , @Current_End_Date = CONTRACT_END_DATE
        FROM
            dbo.CONTRACT
        WHERE
            ED_CONTRACT_NUMBER = @Contract_Number;



        BEGIN TRY

            BEGIN TRAN;



            UPDATE
                SuppAcc
            SET
                SuppAcc.Supplier_Account_Begin_Dt = ISNULL(@Change_Start_Date, SuppAcc.Supplier_Account_Begin_Dt)
                , SuppAcc.Supplier_Account_End_Dt = CASE WHEN SuppAcc.Supplier_Account_End_Dt = @Current_End_Date THEN
                                                             ISNULL(@Change_End_Date, SuppAcc.Supplier_Account_End_Dt)
                                                        WHEN SuppAcc.Supplier_Account_End_Dt BETWEEN @Current_End_Date
                                                                                             AND     @Change_End_Date THEN
                                                            ISNULL(@Change_End_Date, SuppAcc.Supplier_Account_End_Dt)
                                                        ELSE SuppAcc.Supplier_Account_End_Dt
                                                    END
            FROM
                dbo.Supplier_Account_Config SuppAcc
                JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                    ON samm.ACCOUNT_ID = SuppAcc.Account_Id
                       AND  samm.Contract_ID = SuppAcc.Contract_Id
                       AND  samm.Supplier_Account_Config_Id = SuppAcc.Supplier_Account_Config_Id
            WHERE
                samm.Contract_ID = @Contract_Id;



            UPDATE
                dbo.CONTRACT
            SET
                CONTRACT_START_DATE = ISNULL(@Change_Start_Date, CONTRACT_START_DATE)
                , CONTRACT_END_DATE = ISNULL(@Change_End_Date, CONTRACT_END_DATE)
            WHERE
                CONTRACT_ID = @Contract_Id;

            PRINT 'contract start date updated to ' + CAST(@Change_Start_Date AS VARCHAR(10));

            UPDATE
                dbo.SOPS
            SET
                ContractStartDate = ISNULL(@Change_Start_Date, ContractStartDate)
                , ContractEndDate = ISNULL(@Change_End_Date, ContractEndDate)
            WHERE
                ContractNumber = @Contract_Number;

            UPDATE
                dbo.SR_RFP_ARCHIVE_CHECKLIST
            SET
                CONTRACT_START_DATE = ISNULL(@Change_Start_Date, CONTRACT_START_DATE)
                , CONTRACT_END_DATE = ISNULL(@Change_End_Date, CONTRACT_END_DATE)
            WHERE
                ED_CONTRACT_NUMBER = @Contract_Number;

            UPDATE
                dbo.SR_RFP_CHECKLIST
            SET
                CONTRACT_START_DATE = ISNULL(@Change_Start_Date, CONTRACT_START_DATE)
                , CONTRACT_END_DATE = ISNULL(@Change_End_Date, CONTRACT_END_DATE)
            FROM
                dbo.SR_RFP_CHECKLIST
            WHERE
                NEW_CONTRACT_ID = @Contract_Id;

            UPDATE
                dbo.SR_DT_VERIFICATION
            SET
                CONTRACT_END_DATE = ISNULL(@Change_End_Date, CONTRACT_END_DATE)
            WHERE
                CONTRACT_NUMBER = @Contract_Number;



            IF @Change_Start_Date IS NOT NULL
                BEGIN
                    UPDATE
                        dbo.SUPPLIER_ACCOUNT_METER_MAP
                    SET
                        METER_ASSOCIATION_DATE = CASE WHEN METER_ASSOCIATION_DATE = @Current_Start_Date THEN
                                                          @Change_Start_Date
                                                     WHEN METER_ASSOCIATION_DATE > @Change_Start_Date THEN
                                                         METER_ASSOCIATION_DATE
                                                     WHEN METER_ASSOCIATION_DATE < @Change_Start_Date THEN
                                                         @Change_Start_Date
                                                     ELSE METER_ASSOCIATION_DATE
                                                 END
                    WHERE
                        Contract_ID = @Contract_Id;


                END;

            IF @Change_End_Date IS NOT NULL
                BEGIN
                    UPDATE
                        dbo.SUPPLIER_ACCOUNT_METER_MAP
                    SET
                        METER_DISASSOCIATION_DATE = CASE WHEN METER_DISASSOCIATION_DATE = @Current_End_Date THEN
                                                             @Change_End_Date
                                                        --WHEN METER_DISASSOCIATION_DATE < @Change_End_Date THEN
                                                        --    METER_DISASSOCIATION_DATE
                                                        --WHEN METER_DISASSOCIATION_DATE > @Change_End_Date THEN
                                                        --@Change_End_Date
                                                        WHEN METER_DISASSOCIATION_DATE BETWEEN @Current_End_Date
                                                                                       AND     @Change_End_Date THEN
                                                            @Change_End_Date
                                                        ELSE METER_DISASSOCIATION_DATE
                                                    END
                    WHERE
                        Contract_ID = @Contract_Id;

                END;


            COMMIT TRAN;

        END TRY
        BEGIN CATCH

            ROLLBACK TRAN;
            EXEC usp_RethrowError
                'An error occurred AND the transaction was rolled back';
            PRINT ('-----------------------------------------------------');
            PRINT ('An error occurred AND the transaction was rolled back');

        END CATCH;

    END;



GO
GRANT EXECUTE ON  [dbo].[Cbms_ContractDateChange_Updateallassociatetables] TO [CBMSApplication]
GO
