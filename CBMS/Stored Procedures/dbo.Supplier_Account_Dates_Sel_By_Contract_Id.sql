SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Supplier_Account_Info_Sel_By_Contract_Id

DESCRIPTION:

INPUT PARAMETERS:
Name								DataType		Default			Description
-------------------------------------------------------------------------------
 @Supplier_Account_Config_Id		INT					
 
 
OUTPUT PARAMETERS:
Name								DataType		Default			Description
-------------------------------------------------------------------------------
	
USAGE EXAMPLES:
-------------------------------------------------------------------------------
USE CBMS

EXEC Supplier_Account_Info_Sel_By_Contract_Id
    @Contract_Id = 1


AUTHOR INITIALS:
	Initials	Name
-------------------------------------------------------------------------------
	NR			Narayana Reddy	
	
MODIFICATIONS
	Initials	Date			Modification
-------------------------------------------------------------------------------
    NR				2019-06-26		Created For Add contract.   

******/

CREATE PROCEDURE [dbo].[Supplier_Account_Dates_Sel_By_Contract_Id]
     (
         @Contract_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;


        SELECT
            cha.Account_Id
            , cha.Account_Number
            , cha.Meter_Id
            , cha.Meter_Number
            , cha.Supplier_Account_Config_Id
            , cha.Supplier_Contract_ID
            , cha.Supplier_Account_begin_Dt
            , cha.Supplier_Account_End_Dt
            , cha.Supplier_Meter_Association_Date
            , cha.Supplier_Meter_Disassociation_Date
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN dbo.CONTRACT c
                ON c.CONTRACT_ID = cha.Supplier_Contract_ID
        WHERE
            cha.Account_Type = 'Supplier'
            AND cha.Supplier_Contract_ID = @Contract_Id
        GROUP BY
            cha.Account_Id
            , cha.Account_Number
            , cha.Meter_Id
            , cha.Meter_Number
            , cha.Supplier_Account_Config_Id
            , cha.Supplier_Contract_ID
            , cha.Supplier_Account_begin_Dt
            , cha.Supplier_Account_End_Dt
            , cha.Supplier_Meter_Association_Date
            , cha.Supplier_Meter_Disassociation_Date
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE;
    END;


GO
GRANT EXECUTE ON  [dbo].[Supplier_Account_Dates_Sel_By_Contract_Id] TO [CBMSApplication]
GO
