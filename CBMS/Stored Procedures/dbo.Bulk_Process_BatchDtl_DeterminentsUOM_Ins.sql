SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                            
NAME:    [[Bulk_Process_BatchDtl_DeterminentsUOM_Ins]]                        
                         
DESCRIPTION:                         
                     
                            
AUTHOR INITIALS:                            
 Initials Name                            
------------------------------------------------------------                            
 PK prasan kumar                       
                         
 MODIFICATIONS                           
 Initials Date   Modification            
 PK  12-06-2020  now using existing sp to data in invoice event log         
 PK 16-06-2020  Added check for invoice exist else Failing invoice                       
------------------------------------------------------------                            
                        
******/                        
                        
CREATE  PROCEDURE [dbo].[Bulk_Process_BatchDtl_DeterminentsUOM_Ins]                        
    (                        
        @Batch_Id                INT                        
       ,@Cu_Invoice_Id           INT                        
       ,@Bucket_Code             VARCHAR (50)                        
       ,@Current_Bucket_Name     VARCHAR (255)                        
       ,@Current_Sub_Bucket_Name VARCHAR (255)                
  ,@Current_UOM    VARCHAR (255)                         
       ,@New_Bucket_Name         VARCHAR (255)                        
       ,@New_Sub_Bucket_Name     VARCHAR (255)                        
  ,@New_UOM        VARCHAR (255)                  
       ,@Run_Data_Quality_Test   CHAR (3)                        
       ,@Run_Recalc              CHAR (3)                        
       ,@Run_Variance_Test       CHAR (3)                        
       ,@Event_Log_Ins           BIT = 0                        
       ,@User_Id                 INT                        
    )                        
AS                        
BEGIN                        
                        
    SET NOCOUNT ON;                        
        
 DECLARE                        
            @ErrorVar INT,                        
   @this_id  INT,    
   @statusPending INT,                
   @statuspassauto INT,              
   @successmsg varchar(100),  
   @fail varchar(100),              
   @msg varchar(255)    
                         
    BEGIN TRY                               
              
  SELECT @statusPending = MAX(CASE WHEN [cd].[Code_Value] = 'Pending' THEN [cd].[Code_Id]      END)                 
  , @statuspassauto =  MAX(CASE WHEN [cd].[Code_Value] = 'Completed Auto Sub bucket' THEN [cd].[Code_Id]      END)    
  , @fail =  MAX(CASE WHEN [cd].[Code_Value] = 'Error' THEN [cd].[Code_Id]      END)                                
        FROM                                
               [dbo].[Code] AS [cd]                                
  INNER JOIN [dbo].[Codeset] AS [cs]                                
                   ON [cs].[Codeset_Id] = [cd].[Codeset_Id]                                
  WHERE [cd].[Is_Active] = 1                                
               AND [cs].[Codeset_Name] = 'Batch Status';                     
      
                  
        INSERT INTO LOGDB.dbo.XL_Bulk_Invoice_Process_Batch_Dtl                        
        (                        
            XL_Bulk_Invoice_Process_Batch_Id                        
           ,Cu_Invoice_Id                        
           ,Bucket_Code                        
           ,Current_Bucket_Name                        
           ,Current_Sub_Bucket_Name                    
   ,Current_Uom_Name                    
           ,New_Bucket_Name                        
           ,New_Sub_Bucket_Name                 
   ,New_Uom_Name                       
           ,Run_Data_Quality_Test                  
           ,Run_Recalc                        
           ,Run_Variance_Test                        
           ,Status_Cd                        
           ,Created_Ts                        
           ,Last_Change_Ts                        
        )                        
        VALUES                        
            (                        
                @Batch_Id, @Cu_Invoice_Id, @Bucket_Code, @Current_Bucket_Name, @Current_Sub_Bucket_Name , @Current_UOM            
               ,@New_Bucket_Name, @New_Sub_Bucket_Name, @New_UOM, @Run_Data_Quality_Test, @Run_Recalc, @Run_Variance_Test             
               ,@statusPending, GETDATE(), GETDATE()                        
            );                        
                        
  SELECT @this_id = @@identity;                     
              
          
  IF NOT EXISTS ( SELECT 1 FROM CU_INVOICE WHERE CU_INVOICE_ID =@Cu_Invoice_Id )    
   RAISERROR ('Fail. Incorrect Invoice Id Enetered.', 16, 1 );    
    
  IF @Event_Log_Ins = 1          
   EXEC cbmsCuInvoiceEvent_Save @User_Id,@Cu_Invoice_Id,'Update Bulk Excel - Determinants & UOM'                
                       
  EXEC dbo.Invoice_BulkProcess_Update_UOM @this_id;     
      
                           
                      
  declare @count table                       
  (                       
   Cu_Invoice_Id int,                      
   Total_Count int                      
  )      
                           
  insert into @count select Cu_Invoice_Id,count(1) from LOGDB.dbo.XL_Bulk_Invoice_Process_Batch_Dtl                      
   where XL_Bulk_Invoice_Process_Batch_Id = @Batch_Id                        
   AND CU_INVOICE_ID = @Cu_Invoice_Id                       
   group by CU_INVOICE_ID                     
                
              
  select @successmsg = case when  Status_Cd = @statuspassauto then 'Successful- Automapping selected (Bucket Name) for sub bucket'              
       else 'Successful'            
       end               
   from LOGDB.dbo.XL_Bulk_Invoice_Process_Batch_Dtl                    
   where XL_Bulk_Invoice_Process_Batch_Id = @Batch_Id                
                      
         select @msg =  'Update Bulk Excel - Determinants & UOM - ' + case when 1 = ( select 1 from @count c                       
               inner join (                      
               select Cu_Invoice_Id,                      
                 count(1) Error_Count                       
               from LOGDB.dbo.XL_Bulk_Invoice_Process_Batch_Dtl                      
               where XL_Bulk_Invoice_Process_Batch_Id = @Batch_Id                        
               AND CU_INVOICE_ID = @Cu_Invoice_Id                       
               AND Error_Msg is null                      
               group by CU_INVOICE_ID                      
              ) ec on c.Cu_Invoice_Id = ec.Cu_Invoice_Id                      
               and c.Total_Count = ec.Error_Count                      
             )                      
   then @successmsg                     
   when 1 = ( select 1 from @count c                       
               inner join (                      
               select Cu_Invoice_Id,                      
                 count(1) Error_Count                       
               from LOGDB.dbo.XL_Bulk_Invoice_Process_Batch_Dtl                      
               where XL_Bulk_Invoice_Process_Batch_Id = @Batch_Id                        
               AND CU_INVOICE_ID = @Cu_Invoice_Id                       
               AND Error_Msg is not null                      
               group by CU_INVOICE_ID                      
              ) ec on c.Cu_Invoice_Id = ec.Cu_Invoice_Id                      
               and c.Total_Count = ec.Error_Count                      
             )                      
         then 'Unsuccessful'                      
        else 'Partially Successful'                      
        end                
                  
  IF @Event_Log_Ins = 1          
   EXEC cbmsCuInvoiceEvent_Save @User_Id,@Cu_Invoice_Id,@msg                
                      
    END TRY                        
    BEGIN CATCH         
     
  UPDATE LOGDB.dbo.XL_Bulk_Invoice_Process_Batch_Dtl   
  SET Error_Msg = ERROR_MESSAGE() ,    
   Status_Cd = @fail      
  WHERE XL_Bulk_Data_Process_Batch_Dtl_Id = @this_id                   
                        
        --EXEC dbo.usp_RethrowError;                        
                        
    END CATCH;                        
END; 
GO
GRANT EXECUTE ON  [dbo].[Bulk_Process_BatchDtl_DeterminentsUOM_Ins] TO [CBMSApplication]
GO
