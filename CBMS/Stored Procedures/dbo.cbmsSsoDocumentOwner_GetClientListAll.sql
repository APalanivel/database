SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE    PROCEDURE [dbo].[cbmsSsoDocumentOwner_GetClientListAll]
	( @MyAccountId int
	, @client_id int
	
	)
AS
BEGIN

	  

	   SELECT d.CLIENT_ID
		, c.CLIENT_NAME
		, d.DIVISION_ID
		, d.DIVISION_NAME
		, s.SITE_ID
		, vw.SITE_NAME
	     FROM dbo.DIVISION d 
	     JOIN SITE s ON s.DIVISION_ID = d.DIVISION_ID 
	     JOIN vwSiteName vw ON vw.site_id = s.SITE_ID 
	     JOIN CLIENT c ON d.CLIENT_ID = c.CLIENT_ID
            WHERE (d.CLIENT_ID = @client_id)
	 ORDER BY d.DIVISION_ID
		, vw.SITE_NAME





END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoDocumentOwner_GetClientListAll] TO [CBMSApplication]
GO
