SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_DEALTICKET_DETAILS_FOR_BASIS_ONLY_MARK_TO_MARKET_DETAILED_REPORT_P

DESCRIPTION:

INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	 @UserID			VARCHAR
	,@SessionID			VARCHAR
	,@FromDate			VARCHAR(12)
	,@ToDate			VARCHAR(12)
	,@ClientID			INT
	,@SiteID			VARCHAR(1000)
	,@UnitID			INT
	,@DivisionID		INT
	,@PricePointID		INT
	,@counterPartyID	INT
	,@iscounterParty	INT   	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.GET_DEALTICKET_DETAILS_FOR_BASIS_ONLY_MARK_TO_MARKET_DETAILED_REPORT_P '1','1','01/01/2008','12/01/2008',190,'',16,'','','',''	
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/28/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on
		        	
******/

CREATE PROCEDURE dbo.GET_DEALTICKET_DETAILS_FOR_BASIS_ONLY_MARK_TO_MARKET_DETAILED_REPORT_P
    @UserID				VARCHAR,
    @SessionID			VARCHAR,
    @FromDate			VARCHAR(12),
    @ToDate				VARCHAR(12),
    @ClientID			INT,
    @SiteID				VARCHAR(1000),
    @UnitID				INT,
    @DivisionID			INT,
    @PricePointID		INT,
    @counterPartyID		INT,
    @iscounterParty		INT
AS 
    BEGIN	

        SET NOCOUNT ON ;

        DECLARE @SelectQuery1 VARCHAR(8000),
            @FromQuery1 VARCHAR(8000),
            @WhereQuery1 VARCHAR(8000)

        DECLARE @SelectQuery2 VARCHAR(8000),
            @FromQuery2 VARCHAR(8000),
            @WhereQuery2 VARCHAR(8000)

        DECLARE @SelectQuery3 VARCHAR(8000),
            @FromQuery3 VARCHAR(8000),
            @WhereQuery3 VARCHAR(8000)

        DECLARE @FinalQuery1 VARCHAR(8000),
            @FinalQuery2 VARCHAR(8000),
            @FinalQuery3 VARCHAR(8000)

        DECLARE @entity_ID_HT_Q1 VARCHAR(24),
            @entity_ID_DT_Q1 VARCHAR(12),
            @entity_ID_HMT_Q1 VARCHAR(12),
            @entity_ID_DTST_Q1 VARCHAR(12),
            @entity_ID_PRT_Q2 VARCHAR(24),
            @entity_ID_DT_Q2 VARCHAR(12),
            @entity_ID_HMT_Q2 VARCHAR(12),
            @entity_ID_DT_Q3 VARCHAR(12),
            @entity_ID_DTST_Q2 VARCHAR(24),
            @Site_ID VARCHAR(2000)

        IF ( @siteId = '0' ) 
            BEGIN
                SET @siteId = ''
            END

        SELECT  @entity_ID_DT_Q1 = ENTITY_ID
        FROM    ENTITY
        WHERE   ENTITY_TYPE = 268
                AND ENTITY_NAME = 'fixed price'

        SELECT  @entity_ID_HT_Q1 = COALESCE(@entity_ID_HT_Q1 + ',', '')
                + LTRIM(ENTITY_ID)
        FROM    ENTITY
        WHERE   ENTITY_TYPE = 273
                AND ENTITY_NAME IN ( 'financial', 'physical' )


        SELECT  @entity_ID_HMT_Q1 = ENTITY_ID
        FROM    ENTITY
        WHERE   ENTITY_TYPE = 263
                AND ENTITY_NAME = 'basis'

        SELECT  @entity_ID_DTST_Q1 = ENTITY_ID
        FROM    ENTITY
        WHERE   ENTITY_TYPE = 286
                AND ENTITY_NAME = 'order executed'

        SELECT  @entity_ID_PRT_Q2 = COALESCE(@entity_ID_PRT_Q2 + ',', '')
                + LTRIM(ENTITY_ID)
        FROM    ENTITY
        WHERE   ENTITY_TYPE = 274
                AND ENTITY_NAME IN ( 'weighted strip price',
                                     'individual month pricing' )

        SELECT  @entity_ID_DT_Q2 = ENTITY_ID
        FROM    ENTITY
        WHERE   ENTITY_TYPE = 268
                AND ENTITY_NAME = 'trigger'

        SELECT  @entity_ID_DT_Q3 = ENTITY_ID
        FROM    ENTITY
        WHERE   ENTITY_TYPE = 268
                AND ENTITY_NAME = 'options'

        SELECT  @entity_ID_DTST_Q2 = COALESCE(@entity_ID_DTST_Q2 + ',', '')
                + LTRIM(ENTITY_ID)
        FROM    ENTITY
        WHERE   ENTITY_TYPE = 287
                AND ENTITY_NAME IN ( 'fixed', 'closed' )



        SET @SelectQuery1 = 'SELECT DealTicket.RM_DEAL_TICKET_ID
							, CAST(DealDetails.HEDGE_PRICE AS DECIMAL(8,3)) AS HEDGE_PRICE
							, -1 PREMIUM
							, CONVERT (VARCHAR(12), DealDetails.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER
							, ''-1'' OPTION_NAME
							, CASE CounterParty.COUNTERPARTY_NAME WHEN ISNULL(CounterParty.COUNTERPARTY_NAME,'''') THEN CounterParty.COUNTERPARTY_NAME ELSE ISNULL(Vendor.VENDOR_NAME,'''') END AS COUNTERPARTY_NAME
							, ''Fixed Price'' AS DEAL_TYPE
							, CONVERT (VARCHAR(12), DealStatus.DEAL_TRANSACTION_DATE,101) AS TRANSACTION_DATE
							, (CASE DealTicket.HEDGE_TYPE_ID WHEN 586 THEN ''Physical'' ELSE ''financial'' END) AS HEDGE_TYPE
							, ISNULL(CAST(Conversion.CONVERSION_FACTOR AS DECIMAL(32,6)),''1.000'' ) AS CONVERSION_FACTOR
							, ISNULL(CAST(Consumption.CONVERSION_FACTOR AS DECIMAL(32,6)),''1.000'' ) AS VOLUME_CONVERSION_FACTOR
							, DealTicket.CURRENCY_TYPE_ID AS CURRENCY_TYPE_ID
							, CASE entity1.ENTITY_NAME 
								 WHEN ''Daily'' THEN CAST(SUM (DealVolume.HEDGE_VOLUME*Consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(DD, -(DATEPART(DD, DATEADD(MM, 1, DealDetails.MONTH_IDENTIFIER))),DATEADD(MM, 1, DealDetails.MONTH_IDENTIFIER))) ) AS DECIMAL(20,2))
								 WHEN ''Monthly'' THEN CAST(SUM (DealVolume.HEDGE_VOLUME*Consumption.CONVERSION_FACTOR ) AS DECIMAL(20,2))
								 ELSE CAST( SUM (DealVolume.HEDGE_VOLUME*Consumption.CONVERSION_FACTOR )  AS DECIMAL(20,2))
							  END AS HEDGE_VOLUME '
        SET @FromQuery1 = ' FROM RM_DEAL_TICKET AS DealTicket JOIN RM_DEAL_TICKET_TRANSACTION_STATUS AS DealStatus
								 ON DealTicket.RM_DEAL_TICKET_ID=DealStatus.RM_DEAL_TICKET_ID
								 AND DealTicket.CLIENT_ID='
            + STR(@clientId)
            + '
							JOIN RM_DEAL_TICKET_DETAILS AS DealDetails
								 ON DealTicket.RM_DEAL_TICKET_ID=DealDetails.RM_DEAL_TICKET_ID
							JOIN RM_DEAL_TICKET_VOLUME_DETAILS AS DealVolume
								 ON DealDetails.RM_DEAL_TICKET_DETAILS_ID=DealVolume.RM_DEAL_TICKET_DETAILS_ID
							LEFT OUTER JOIN RM_COUNTERPARTY AS CounterParty
								 ON DealTicket.RM_COUNTERPARTY_ID=CounterParty.RM_COUNTERPARTY_ID
							JOIN CONSUMPTION_UNIT_CONVERSION AS Consumption
								 ON DealTicket.UNIT_TYPE_ID=Consumption.BASE_UNIT_ID
								 AND Consumption.CONVERTED_UNIT_ID='
            + STR(@unitId)
            + '
							JOIN ENTITY AS Entity1
								 ON DealTicket.FREQUENCY_TYPE_ID=Entity1.ENTITY_ID
							LEFT OUTER JOIN RM_CURRENCY_UNIT_CONVERSION AS Conversion
								 ON DealTicket.CLIENT_ID=Conversion.CLIENT_ID
									AND Conversion.CONVERSION_YEAR=(SELECT MAX(CONVERSION_YEAR) FROM RM_CURRENCY_UNIT_CONVERSION WHERE CLIENT_ID='
            + STR(@clientId)
            + ')
							LEFT OUTER JOIN VENDOR AS Vendor
								 ON DealTicket.VENDOR_ID=Vendor.VENDOR_ID '

        SET @WhereQuery1 = ' WHERE DealTicket.DEAL_TYPE_ID = ('
            + @entity_ID_DT_Q1 + ')
							AND DealTicket.HEDGE_TYPE_ID IN ('
            + @entity_ID_HT_Q1 + ')
							AND DealTicket.HEDGE_MODE_TYPE_ID=('
            + @entity_ID_HMT_Q1
            + ' )
							AND
							(								
								  ((' + ''''
            + CONVERT(VARCHAR(12), @FromDate, 101) + ''''
            + ' BETWEEN dealTicket.HEDGE_START_MONTH  AND dealTicket.HEDGE_END_MONTH)
								   OR (' + ''''
            + CONVERT(VARCHAR(12), @toDate, 101) + ''''
            + ' BETWEEN dealTicket.HEDGE_START_MONTH AND dealTicket.HEDGE_END_MONTH)
								  )
								 OR	
								 ((dealTicket.HEDGE_START_MONTH BETWEEN '
            + '''' + CONVERT(VARCHAR(12), @FromDate, 101) + '''' + '  AND '
            + '''' + CONVERT(VARCHAR(12), @toDate, 101) + ''''
            + ')
								   OR (dealTicket.HEDGE_END_MONTH BETWEEN '
            + '''' + CONVERT(VARCHAR(12), @FromDate, 101) + '''' + '  AND '
            + '''' + CONVERT(VARCHAR(12), @toDate, 101) + ''''
            + ')
								  )
							)	
							AND DealStatus.DEAL_TRANSACTION_STATUS_TYPE_ID IN( '
            + @entity_ID_DTST_Q1
            + ')
							AND DealDetails.MONTH_IDENTIFIER BETWEEN '
            + '''' + CONVERT(VARCHAR(12), @FromDate, 101) + '''' + ' AND '
            + '''' + CONVERT(VARCHAR(12), @toDate, 101) + ''''


        SET @SelectQuery2 = ' SELECT DealTicket.RM_DEAL_TICKET_ID
							, CAST(DealDetails.HEDGE_PRICE AS DECIMAL(8,3)) AS HEDGE_PRICE
							, -1 AS PREMIUM
							, CONVERT (VARCHAR(12),DealDetails.MONTH_IDENTIFIER, 101) AS MONTH_IDENTIFIER
							, ''-1'' AS OPTION_NAME
							, CASE CounterParty.COUNTERPARTY_NAME
								WHEN ISNULL(CounterParty.COUNTERPARTY_NAME,'''') THEN CounterParty.COUNTERPARTY_NAME
								ELSE ISNULL(Vendor.VENDOR_NAME,'''')
							  END AS COUNTERPARTY_NAME
							, ''Trigger'' AS DEAL_TYPE
							, ''-1'' AS TRANSACTION_DATE
							, (CASE DealTicket.HEDGE_TYPE_ID WHEN 586 THEN ''Physical'' ELSE ''financial'' END) AS HEDGE_TYPE
							, ISNULL(CAST(Conversion.CONVERSION_FACTOR AS DECIMAL(32,6)),''1.000'' ) AS CONVERSION_FACTOR
							, ISNULL(CAST(Consumption.CONVERSION_FACTOR AS DECIMAL(32,6)),''1.000'' ) AS VOLUME_CONVERSION_FACTOR
							, DealTicket.CURRENCY_TYPE_ID AS CURRENCY_TYPE_ID
							, CASE entity1.ENTITY_NAME
								 WHEN ''Daily'' THEN CAST( SUM (DealVolume.HEDGE_VOLUME*Consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, DealDetails.MONTH_IDENTIFIER))),dateadd(mm, 1, DealDetails.MONTH_IDENTIFIER))) ) AS DECIMAL(20,2))
								 WHEN ''Monthly'' THEN CAST(SUM (DealVolume.HEDGE_VOLUME*Consumption.CONVERSION_FACTOR ) AS DECIMAL(20,2))
								 ELSE CAST(SUM(DealVolume.HEDGE_VOLUME*Consumption.CONVERSION_FACTOR )  AS DECIMAL(20,2))
							  END AS HEDGE_VOLUME '

        SET @FromQuery2 = ' FROM RM_DEAL_TICKET DealTicket JOIN RM_DEAL_TICKET_DETAILS DealDetails
								ON DealTicket.RM_DEAL_TICKET_ID=DealDetails.RM_DEAL_TICKET_ID
								AND DealTicket.CLIENT_ID='
            + STR(@clientId)
            + '
							JOIN RM_DEAL_TICKET_VOLUME_DETAILS Dealvolume
								ON DealDetails.RM_DEAL_TICKET_DETAILS_ID=DealVolume.RM_DEAL_TICKET_DETAILS_ID
							LEFT OUTER JOIN RM_COUNTERPARTY CounterParty
								ON DealTicket.RM_COUNTERPARTY_ID=CounterParty.RM_COUNTERPARTY_ID
							JOIN CONSUMPTION_UNIT_CONVERSION Consumption
								ON DealTicket.UNIT_TYPE_ID=Consumption.BASE_UNIT_ID
								AND Consumption.CONVERTED_UNIT_ID='
            + STR(@unitId)
            + '
							JOIN ENTITY entity1
								ON DealTicket.FREQUENCY_TYPE_ID=entity1.ENTITY_ID
							LEFT OUTER JOIN RM_CURRENCY_UNIT_CONVERSION Conversion
								ON DealTicket.CLIENT_ID=Conversion.CLIENT_ID
									AND Conversion.CONVERSION_YEAR=(SELECT MAX(CONVERSION_YEAR) FROM RM_CURRENCY_UNIT_CONVERSION WHERE CLIENT_ID='
            + STR(@clientId)
            + ')
							LEFT OUTER JOIN VENDOR Vendor 
								ON DealTicket.VENDOR_ID=Vendor.VENDOR_ID '

        SET @WhereQuery2 = ' WHERE DealTicket.PRICING_REQUEST_TYPE_ID IN ('
            + @entity_ID_PRT_Q2 + ' )
							AND	DealTicket.DEAL_TYPE_ID=( '
            + @entity_ID_DT_Q2 + ')
							AND	DealTicket.HEDGE_TYPE_ID IN ('
            + @entity_ID_HT_Q1
            + ')
							AND	DealTicket.HEDGE_MODE_TYPE_ID=('
            + @entity_ID_HMT_Q1
            + ')
							AND
							(								
								  ((' + ''''
            + CONVERT(VARCHAR(12), '' + @FromDate + '', 101) + ''''
            + ' BETWEEN dealTicket.HEDGE_START_MONTH  AND dealTicket.HEDGE_END_MONTH)
								   OR (' + ''''
            + CONVERT(VARCHAR(12), '' + @toDate + '', 101) + ''''
            + ' BETWEEN dealTicket.HEDGE_START_MONTH AND dealTicket.HEDGE_END_MONTH)
								  )
								 OR	
								 ((dealTicket.HEDGE_START_MONTH BETWEEN '
            + '''' + CONVERT(VARCHAR(12), '' + @FromDate + '', 101) + ''''
            + '  AND ' + '''' + CONVERT(VARCHAR(12), '' + @toDate + '', 101)
            + ''''
            + ')
								   OR (dealTicket.HEDGE_END_MONTH BETWEEN '
            + '''' + CONVERT(VARCHAR(12), '' + @FromDate + '', 101) + ''''
            + '  AND ' + '''' + CONVERT(VARCHAR(12), '' + @toDate + '', 101)
            + ''''
            + ')
								  )
							)	
							AND DealDetails.TRIGGER_STATUS_TYPE_ID IN('
            + @entity_ID_DTST_Q2
            + ')
							AND	DealDetails.MONTH_IDENTIFIER BETWEEN '
            + '''' + CONVERT(VARCHAR(12), @FromDate, 101) + '''' + ' AND '
            + '''' + CONVERT(VARCHAR(12), @toDate, 101) + '''' + ' '


        SET @SelectQuery3 = ' SELECT DealTicket.RM_DEAL_TICKET_ID
							, CAST(dealoption.STRIKE_PRICE AS DECIMAL(8,3)) AS HEDGE_PRICE
							, CAST(dealoption.PREMIUM_PRICE AS DECIMAL(8,3)) AS PREMIUM
							, CONVERT (VARCHAR(12), DealDetails.MONTH_IDENTIFIER, 101) AS MONTH_IDENTIFIER
							, entity1.ENTITY_NAME AS OPTION_NAME
							, CASE CounterParty.COUNTERPARTY_NAME 
								WHEN ISNULL(CounterParty.COUNTERPARTY_NAME,'''') THEN CounterParty.COUNTERPARTY_NAME 
								ELSE ISNULL(Vendor.VENDOR_NAME,'''')
							  END AS COUNTERPARTY_NAME
							, ''Option'' AS DEAL_TYPE
							, CONVERT (VARCHAR(12), DealStatus.DEAL_TRANSACTION_DATE,101) AS TRANSACTION_DATE
							, (CASE DealTicket.HEDGE_TYPE_ID WHEN 586 THEN ''Physical'' ELSE ''financial'' END) AS HEDGE_TYPE
							, ISNULL(CAST(Conversion.CONVERSION_FACTOR AS DECIMAL(32,6)),''1.000'' ) AS CONVERSION_FACTOR
							, ISNULL(CAST(Consumption.CONVERSION_FACTOR AS DECIMAL(32,6)),''1.000'' ) AS VOLUME_CONVERSION_FACTOR
							, DealTicket.CURRENCY_TYPE_ID AS CURRENCY_TYPE_ID
							, CASE entity2.ENTITY_NAME
								 WHEN ''Daily'' THEN CAST( SUM (DealVolume.HEDGE_VOLUME*Consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, DealDetails.MONTH_IDENTIFIER))),dateadd(mm, 1, DealDetails.MONTH_IDENTIFIER))) ) AS DECIMAL(20,2))
								 WHEN ''Monthly'' THEN CAST(SUM (DealVolume.HEDGE_VOLUME*Consumption.CONVERSION_FACTOR ) AS DECIMAL(20,2))
								ELSE CAST( SUM (DealVolume.HEDGE_VOLUME*Consumption.CONVERSION_FACTOR )  AS DECIMAL(20,2))
							  END AS HEDGE_VOLUME '

        SET @FromQuery3 = ' FROM RM_DEAL_TICKET Dealticket JOIN RM_DEAL_TICKET_TRANSACTION_STATUS Dealstatus
							ON DealTicket.RM_DEAL_TICKET_ID=DealStatus.RM_DEAL_TICKET_ID
							AND DealTicket.CLIENT_ID='
            + STR(@clientId)
            + '
						JOIN RM_DEAL_TICKET_DETAILS Dealdetails
							ON DealTicket.RM_DEAL_TICKET_ID=DealDetails.RM_DEAL_TICKET_ID
						JOIN RM_DEAL_TICKET_OPTION_DETAILS Dealoption
							ON DealDetails.RM_DEAL_TICKET_DETAILS_ID=dealoption.RM_DEAL_TICKET_DETAILS_ID
						JOIN RM_DEAL_TICKET_VOLUME_DETAILS Dealvolume
							ON DealDetails.RM_DEAL_TICKET_DETAILS_ID=DealVolume.RM_DEAL_TICKET_DETAILS_ID
						LEFT OUTER JOIN RM_COUNTERPARTY Counterparty
					ON DealTicket.RM_COUNTERPARTY_ID=CounterParty.RM_COUNTERPARTY_ID
						JOIN ENTITY entity1
							ON dealoption.OPTION_TYPE_ID=entity1.ENTITY_ID
						JOIN CONSUMPTION_UNIT_CONVERSION Consumption
							ON DealTicket.UNIT_TYPE_ID=Consumption.BASE_UNIT_ID
								AND Consumption.CONVERTED_UNIT_ID='
            + STR(@unitId)
            + ' 
						JOIN ENTITY entity2
							ON DealTicket.FREQUENCY_TYPE_ID=entity2.ENTITY_ID
						LEFT OUTER JOIN RM_CURRENCY_UNIT_CONVERSION conversion
							ON DealTicket.CLIENT_ID=Conversion.CLIENT_ID
								AND Conversion.CONVERSION_YEAR=(SELECT MAX(CONVERSION_YEAR) FROM RM_CURRENCY_UNIT_CONVERSION WHERE CLIENT_ID='
            + STR(@clientId)
            + ')
						LEFT OUTER JOIN VENDOR vENDor 
							ON DealTicket.VENDOR_ID=Vendor.VENDOR_ID '

        SET @WhereQuery3 = ' WHERE DealTicket.DEAL_TYPE_ID=('
            + @entity_ID_DT_Q3 + ')
							AND DealTicket.HEDGE_TYPE_ID IN ('
            + @entity_ID_HT_Q1
            + ')
							AND DealTicket.HEDGE_MODE_TYPE_ID=( '
            + @entity_ID_HMT_Q1
            + ')
							AND
							(								
								  ((' + ''''
            + CONVERT(VARCHAR(12), '' + @FromDate + '', 101) + ''''
            + ' BETWEEN dealTicket.HEDGE_START_MONTH  AND dealTicket.HEDGE_END_MONTH)
								   OR (' + ''''
            + CONVERT(VARCHAR(12), '' + @toDate + '', 101) + ''''
            + ' BETWEEN dealTicket.HEDGE_START_MONTH AND dealTicket.HEDGE_END_MONTH)
								  )
								 OR	
								 ((dealTicket.HEDGE_START_MONTH BETWEEN '
            + '''' + CONVERT(VARCHAR(12), '' + @FromDate + '', 101) + ''''
            + '  AND ' + '''' + CONVERT(VARCHAR(12), '' + @toDate + '', 101)
            + ''''
            + ')
								   OR (dealTicket.HEDGE_END_MONTH BETWEEN '
            + '''' + CONVERT(VARCHAR(12), '' + @FromDate + '', 101) + ''''
            + '  AND ' + '''' + CONVERT(VARCHAR(12), '' + @toDate + '', 101)
            + ''''
            + ')
								  )
							)	
							AND DealStatus.DEAL_TRANSACTION_STATUS_TYPE_ID IN('
            + @entity_ID_DTST_Q1
            + ')
							AND DealDetails.MONTH_IDENTIFIER BETWEEN '
            + '''' + CONVERT(VARCHAR(12), @FromDate, 101) + '''' + ' AND '
            + '''' + CONVERT(VARCHAR(12), @toDate, 101) + '''' + ' '
							
        IF @counterpartyID > 0 
            BEGIN

                IF @iscounterParty = 0 
                    BEGIN

                        SET @FromQuery1 = ' FROM RM_DEAL_TICKET AS DealTicket JOIN RM_DEAL_TICKET_TRANSACTION_STATUS AS DealStatus
										 ON DealTicket.RM_DEAL_TICKET_ID=DealStatus.RM_DEAL_TICKET_ID
										 AND DealTicket.CLIENT_ID='
                            + STR(@clientId)
                            + '
										 AND DealTicket.VENDOR_ID = '
                            + STR(@counterpartyID)
                            + '
									JOIN RM_DEAL_TICKET_DETAILS AS DealDetails
										 ON DealTicket.RM_DEAL_TICKET_ID=DealDetails.RM_DEAL_TICKET_ID
									JOIN RM_DEAL_TICKET_VOLUME_DETAILS AS DealVolume
										 ON DealDetails.RM_DEAL_TICKET_DETAILS_ID=DealVolume.RM_DEAL_TICKET_DETAILS_ID
									LEFT OUTER JOIN RM_COUNTERPARTY AS CounterParty
										 ON DealTicket.RM_COUNTERPARTY_ID=CounterParty.RM_COUNTERPARTY_ID
									JOIN CONSUMPTION_UNIT_CONVERSION AS Consumption
										 ON DealTicket.UNIT_TYPE_ID=Consumption.BASE_UNIT_ID
										 AND Consumption.CONVERTED_UNIT_ID='
                            + STR(@unitId)
                            + '
									JOIN ENTITY AS Entity1
										 ON DealTicket.FREQUENCY_TYPE_ID=Entity1.ENTITY_ID
									LEFT OUTER JOIN RM_CURRENCY_UNIT_CONVERSION AS Conversion
										 ON DealTicket.CLIENT_ID=Conversion.CLIENT_ID
											AND Conversion.CONVERSION_YEAR=(SELECT MAX(CONVERSION_YEAR) FROM RM_CURRENCY_UNIT_CONVERSION WHERE CLIENT_ID='
                            + STR(@clientId)
                            + ')
									LEFT OUTER JOIN VENDOR AS Vendor
										 ON DealTicket.VENDOR_ID=Vendor.VENDOR_ID '

                        SET @FromQuery2 = ' FROM RM_DEAL_TICKET DealTicket JOIN RM_DEAL_TICKET_DETAILS DealDetails
										ON DealTicket.RM_DEAL_TICKET_ID=DealDetails.RM_DEAL_TICKET_ID
										AND DealTicket.CLIENT_ID='
                            + STR(@clientId)
                            + '
										AND DealTicket.VENDOR_ID = '
                            + STR(@counterpartyID)
                            + '
									JOIN RM_DEAL_TICKET_VOLUME_DETAILS Dealvolume
										ON DealDetails.RM_DEAL_TICKET_DETAILS_ID=DealVolume.RM_DEAL_TICKET_DETAILS_ID
									LEFT OUTER JOIN RM_COUNTERPARTY CounterParty
										ON DealTicket.RM_COUNTERPARTY_ID=CounterParty.RM_COUNTERPARTY_ID
									JOIN CONSUMPTION_UNIT_CONVERSION Consumption
										ON DealTicket.UNIT_TYPE_ID=Consumption.BASE_UNIT_ID
										AND Consumption.CONVERTED_UNIT_ID='
                            + STR(@unitId)
                            + '
									JOIN ENTITY entity1
										ON DealTicket.FREQUENCY_TYPE_ID=entity1.ENTITY_ID
									LEFT OUTER JOIN RM_CURRENCY_UNIT_CONVERSION Conversion
										ON DealTicket.CLIENT_ID=Conversion.CLIENT_ID
											AND Conversion.CONVERSION_YEAR=(SELECT MAX(CONVERSION_YEAR) FROM RM_CURRENCY_UNIT_CONVERSION WHERE CLIENT_ID='
                            + STR(@clientId)
                            + ')
									LEFT OUTER JOIN VENDOR Vendor 
										ON DealTicket.VENDOR_ID=Vendor.VENDOR_ID '

                        SET @FromQuery3 = ' FROM RM_DEAL_TICKET Dealticket JOIN RM_DEAL_TICKET_TRANSACTION_STATUS Dealstatus
									ON DealTicket.RM_DEAL_TICKET_ID=DealStatus.RM_DEAL_TICKET_ID
									AND DealTicket.CLIENT_ID='
                            + STR(@clientId)
                            + '
									AND DealTicket.VENDOR_ID = '
                            + STR(@counterpartyID)
                            + '
								JOIN RM_DEAL_TICKET_DETAILS Dealdetails
									ON DealTicket.RM_DEAL_TICKET_ID=DealDetails.RM_DEAL_TICKET_ID
								JOIN RM_DEAL_TICKET_OPTION_DETAILS Dealoption
									ON DealDetails.RM_DEAL_TICKET_DETAILS_ID=dealoption.RM_DEAL_TICKET_DETAILS_ID
								JOIN RM_DEAL_TICKET_VOLUME_DETAILS Dealvolume
									ON DealDetails.RM_DEAL_TICKET_DETAILS_ID=DealVolume.RM_DEAL_TICKET_DETAILS_ID
								LEFT OUTER JOIN RM_COUNTERPARTY Counterparty
									ON DealTicket.RM_COUNTERPARTY_ID=CounterParty.RM_COUNTERPARTY_ID
								JOIN ENTITY entity1
									ON dealoption.OPTION_TYPE_ID=entity1.ENTITY_ID
								JOIN CONSUMPTION_UNIT_CONVERSION Consumption
									ON DealTicket.UNIT_TYPE_ID=Consumption.BASE_UNIT_ID
										AND Consumption.CONVERTED_UNIT_ID='
                            + STR(@unitId)
                            + ' 
								JOIN ENTITY entity2
									ON DealTicket.FREQUENCY_TYPE_ID=entity2.ENTITY_ID
								LEFT OUTER JOIN RM_CURRENCY_UNIT_CONVERSION conversion
									ON DealTicket.CLIENT_ID=Conversion.CLIENT_ID
										AND Conversion.CONVERSION_YEAR=(SELECT MAX(CONVERSION_YEAR) FROM RM_CURRENCY_UNIT_CONVERSION WHERE CLIENT_ID='
                            + STR(@clientId)
                            + ')
								LEFT OUTER JOIN VENDOR vENDor 
									ON DealTicket.VENDOR_ID=Vendor.VENDOR_ID '

                    END
                ELSE 
                    IF @iscounterParty = 1 
                        BEGIN

                            SET @FromQuery1 = ' FROM RM_DEAL_TICKET AS DealTicket JOIN RM_DEAL_TICKET_TRANSACTION_STATUS AS DealStatus
										 ON DealTicket.RM_DEAL_TICKET_ID=DealStatus.RM_DEAL_TICKET_ID
										 AND DealTicket.CLIENT_ID='
                                + STR(@clientId)
                                + '
										 AND DealTicket.RM_COUNTERPARTY_ID = '
                                + STR(@counterpartyId)
                                + '
									JOIN RM_DEAL_TICKET_DETAILS AS DealDetails
										 ON DealTicket.RM_DEAL_TICKET_ID=DealDetails.RM_DEAL_TICKET_ID
									JOIN RM_DEAL_TICKET_VOLUME_DETAILS AS DealVolume
										 ON DealDetails.RM_DEAL_TICKET_DETAILS_ID=DealVolume.RM_DEAL_TICKET_DETAILS_ID
									LEFT OUTER JOIN RM_COUNTERPARTY AS CounterParty
										 ON DealTicket.RM_COUNTERPARTY_ID=CounterParty.RM_COUNTERPARTY_ID
									JOIN CONSUMPTION_UNIT_CONVERSION AS Consumption
										 ON DealTicket.UNIT_TYPE_ID=Consumption.BASE_UNIT_ID
										 AND Consumption.CONVERTED_UNIT_ID='
                                + STR(@unitId)
                                + '
									JOIN ENTITY AS Entity1
										 ON DealTicket.FREQUENCY_TYPE_ID=Entity1.ENTITY_ID
									LEFT OUTER JOIN RM_CURRENCY_UNIT_CONVERSION AS Conversion
										 ON DealTicket.CLIENT_ID=Conversion.CLIENT_ID
											AND Conversion.CONVERSION_YEAR=(SELECT MAX(CONVERSION_YEAR) FROM RM_CURRENCY_UNIT_CONVERSION WHERE CLIENT_ID='
                                + STR(@clientId)
                                + ')
									LEFT OUTER JOIN VENDOR AS Vendor
										 ON DealTicket.VENDOR_ID=Vendor.VENDOR_ID '

                            SET @FromQuery2 = ' FROM RM_DEAL_TICKET DealTicket JOIN RM_DEAL_TICKET_DETAILS DealDetails
										ON DealTicket.RM_DEAL_TICKET_ID=DealDetails.RM_DEAL_TICKET_ID
										AND DealTicket.CLIENT_ID='
                                + STR(@clientId)
                                + '
										AND DealTicket.RM_COUNTERPARTY_ID = '
                                + STR(@counterpartyId)
                                + '
									JOIN RM_DEAL_TICKET_VOLUME_DETAILS Dealvolume
										ON DealDetails.RM_DEAL_TICKET_DETAILS_ID=DealVolume.RM_DEAL_TICKET_DETAILS_ID
									LEFT OUTER JOIN RM_COUNTERPARTY CounterParty
										ON DealTicket.RM_COUNTERPARTY_ID=CounterParty.RM_COUNTERPARTY_ID
									JOIN CONSUMPTION_UNIT_CONVERSION Consumption
										ON DealTicket.UNIT_TYPE_ID=Consumption.BASE_UNIT_ID
										AND Consumption.CONVERTED_UNIT_ID='
                                + STR(@unitId)
                                + '
									JOIN ENTITY entity1
										ON DealTicket.FREQUENCY_TYPE_ID=entity1.ENTITY_ID
									LEFT OUTER JOIN RM_CURRENCY_UNIT_CONVERSION Conversion
										ON DealTicket.CLIENT_ID=Conversion.CLIENT_ID
											AND Conversion.CONVERSION_YEAR=(SELECT MAX(CONVERSION_YEAR) FROM RM_CURRENCY_UNIT_CONVERSION WHERE CLIENT_ID='
                                + STR(@clientId)
                                + ')
									LEFT OUTER JOIN VENDOR Vendor 
										ON DealTicket.VENDOR_ID=Vendor.VENDOR_ID '

                            SET @FromQuery3 = ' FROM RM_DEAL_TICKET Dealticket JOIN RM_DEAL_TICKET_TRANSACTION_STATUS Dealstatus
									ON DealTicket.RM_DEAL_TICKET_ID=DealStatus.RM_DEAL_TICKET_ID
									AND DealTicket.CLIENT_ID='
                                + STR(@clientId)
                                + '
									AND DealTicket.RM_COUNTERPARTY_ID = '
                                + STR(@counterpartyId)
                                + '
								JOIN RM_DEAL_TICKET_DETAILS Dealdetails
									ON DealTicket.RM_DEAL_TICKET_ID=DealDetails.RM_DEAL_TICKET_ID
								JOIN RM_DEAL_TICKET_OPTION_DETAILS Dealoption
									ON DealDetails.RM_DEAL_TICKET_DETAILS_ID=dealoption.RM_DEAL_TICKET_DETAILS_ID
								JOIN RM_DEAL_TICKET_VOLUME_DETAILS Dealvolume
									ON DealDetails.RM_DEAL_TICKET_DETAILS_ID=DealVolume.RM_DEAL_TICKET_DETAILS_ID
								LEFT OUTER JOIN RM_COUNTERPARTY Counterparty
									ON DealTicket.RM_COUNTERPARTY_ID=CounterParty.RM_COUNTERPARTY_ID
								JOIN ENTITY entity1
									ON dealoption.OPTION_TYPE_ID=entity1.ENTITY_ID
								JOIN CONSUMPTION_UNIT_CONVERSION Consumption
									ON DealTicket.UNIT_TYPE_ID=Consumption.BASE_UNIT_ID
										AND Consumption.CONVERTED_UNIT_ID='
                                + STR(@unitId)
                                + ' 
								JOIN ENTITY entity2
									ON DealTicket.FREQUENCY_TYPE_ID=entity2.ENTITY_ID
								LEFT OUTER JOIN RM_CURRENCY_UNIT_CONVERSION conversion
									ON DealTicket.CLIENT_ID=Conversion.CLIENT_ID
										AND Conversion.CONVERSION_YEAR=(SELECT MAX(CONVERSION_YEAR) FROM RM_CURRENCY_UNIT_CONVERSION WHERE CLIENT_ID='
                                + STR(@clientId)
                                + ')
								LEFT OUTER JOIN VENDOR vENDor 
									ON DealTicket.VENDOR_ID=Vendor.VENDOR_ID '

                        END

            END


        IF @divisionId <> ''
            OR @divisionId != NULL 
            BEGIN
                SELECT  @FromQuery1 = @FromQuery1
                        + ', DIVISION division, SITE site '
                SELECT  @FromQuery2 = @FromQuery2
                        + ', DIVISION division, SITE site '
                SELECT  @FromQuery3 = @FromQuery3
                        + ', DIVISION division, SITE site '
		 
                SELECT  @WhereQuery1 = @WhereQuery1
                        + ' AND DealVolume.site_id = site.site_id
			AND site.division_id = division.division_id
			AND division.division_id = ' + STR(@divisionId) + '
			AND division.client_id = ' + STR(@clientId) + '  '

                SELECT  @WhereQuery2 = @WhereQuery2
                        + ' AND DealVolume.site_id = site.site_id
			AND site.division_id = division.division_id
			AND division.division_id = ' + STR(@divisionId) + '
			AND division.client_id = ' + STR(@clientId) + '  '

                SELECT  @WhereQuery3 = @WhereQuery3
                        + ' AND DealVolume.site_id = site.site_id
			AND site.division_id = division.division_id
			AND division.division_id = ' + STR(@divisionId) + '
			AND division.client_id = ' + STR(@clientId) + '  '

            END

        IF @siteId <> ''
            OR @siteId != NULL 
            BEGIN

                SELECT  @WhereQuery1 = @WhereQuery1
                        + ' AND DealVolume.SITE_ID IN( SELECT DISTINCT hedge.SITE_ID FROM RM_ONBOARD_HEDGE_SETUP hedge
			WHERE hedge.SITE_ID in (' + @siteId
                        + ') AND hedge.INCLUDE_IN_REPORTS=1 )   '

                SELECT  @WhereQuery2 = @WhereQuery2
                        + ' AND DealVolume.SITE_ID IN( SELECT DISTINCT hedge.SITE_ID FROM RM_ONBOARD_HEDGE_SETUP hedge
			WHERE hedge.SITE_ID in (' + @siteId
                        + ') AND hedge.INCLUDE_IN_REPORTS=1 )   '

                SELECT  @WhereQuery3 = @WhereQuery3
                        + ' AND DealVolume.SITE_ID IN( SELECT DISTINCT hedge.SITE_ID FROM RM_ONBOARD_HEDGE_SETUP hedge
			WHERE hedge.SITE_ID in (' + @siteId
                        + ') AND hedge.INCLUDE_IN_REPORTS=1 )  '
		

            END

        IF @PricePointID <> ''
            OR @PricePointID != NULL 
            BEGIN

                SELECT  @WhereQuery1 = @WhereQuery1
                        + ' AND DealTicket.PRICE_INDEX_ID='
                        + STR(@PricePointID) + ' ' 
			
                SELECT  @WhereQuery2 = @WhereQuery2
                        + ' AND DealTicket.PRICE_INDEX_ID='
                        + STR(@PricePointID) + ' ' 

                SELECT  @WhereQuery3 = @WhereQuery3
                        + ' AND DealTicket.PRICE_INDEX_ID='
                        + STR(@PricePointID) + ' ' 

            END

	
        SELECT  @FinalQuery1 = @SelectQuery1 + @FromQuery1 + @WhereQuery1
                + ' GROUP BY DealTicket.RM_DEAL_TICKET_ID,DealDetails.HEDGE_PRICE,DealDetails.MONTH_IDENTIFIER,CounterParty.COUNTERPARTY_NAME,DealStatus.DEAL_TRANSACTION_DATE,Conversion.CONVERSION_FACTOR,Consumption.CONVERSION_FACTOR,DealTicket.CURRENCY_TYPE_ID,entity1.ENTITY_NAME,Vendor.VENDOR_NAME, DealTicket.HEDGE_TYPE_ID '
		    
        SELECT  @FinalQuery2 = @SelectQuery2 + @FromQuery2 + @WhereQuery2
                + ' GROUP BY DealTicket.RM_DEAL_TICKET_ID,DealDetails.HEDGE_PRICE,DealDetails.MONTH_IDENTIFIER,CounterParty.COUNTERPARTY_NAME,Conversion.CONVERSION_FACTOR,Consumption.CONVERSION_FACTOR,DealTicket.CURRENCY_TYPE_ID,entity1.ENTITY_NAME,Vendor.VENDOR_NAME, DealTicket.HEDGE_TYPE_ID '

        SELECT  @FinalQuery3 = @SelectQuery3 + @FromQuery3 + @WhereQuery3
                + ' GROUP BY DealTicket.RM_DEAL_TICKET_ID,dealoption.STRIKE_PRICE,dealoption.PREMIUM_PRICE,DealDetails.MONTH_IDENTIFIER,entity1.ENTITY_NAME,CounterParty.COUNTERPARTY_NAME,DealStatus.DEAL_TRANSACTION_DATE,Conversion.CONVERSION_FACTOR,Consumption.CONVERSION_FACTOR,DealTicket.CURRENCY_TYPE_ID,entity2.ENTITY_NAME,Vendor.VENDOR_NAME, DealTicket.HEDGE_TYPE_ID '

        print ( @FinalQuery1 + ' UNION ALL ' + @FinalQuery2 + ' UNION ALL'
                + @FinalQuery3 )
        EXEC
            ( @FinalQuery1 + ' UNION ALL ' + @FinalQuery2 + ' UNION ALL'
              + @FinalQuery3
            )
	
    END
GO
GRANT EXECUTE ON  [dbo].[GET_DEALTICKET_DETAILS_FOR_BASIS_ONLY_MARK_TO_MARKET_DETAILED_REPORT_P] TO [CBMSApplication]
GO
