SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


--exec dbo.GET_ALL_CONTRACT_LEVEL_P

CREATE     PROCEDURE dbo.GET_ALL_CONTRACT_LEVEL_P
	as
	begin
		set nocount on

		select	Entity_Id ,
		        Entity_name 
		from	entity 
		where	entity_type = 1053 
		order by Entity_name

	end
GO
GRANT EXECUTE ON  [dbo].[GET_ALL_CONTRACT_LEVEL_P] TO [CBMSApplication]
GO
