SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******

NAME:
		dbo.Filtered_Code_Sel_By_Codeset_User_Permission

DESCRIPTION:
	To get the filtered codes based on the user permission.

INPUT PARAMETERS:
	NAME					DATATYPE			DEFAULT			DESCRIPTION
--------------------------------------------------------------------------------
	@Codeset_Name			VARCHAR(25)			Name of the codeset
	@Filtered_Codeset		VARCHAR(25)			Name of the filtered codeset
	@User_Info_Id			INT					RA Login user Id

OUTPUT PARAMETERS:
	NAME					DATATYPE			DEFAULT			DESCRIPTION
--------------------------------------------------------------------------------

USAGE EXAMPLES:
--------------------------------------------------------------------------------

	 EXEC dbo.Filtered_Code_Sel_By_Codeset_User_Permission
      'Notification Type'
     ,'Document Notification'
     ,49;
     

AUTHOR INITIALS:
	INITIALS	NAME
--------------------------------------------------------------------------------
	NR          Narayana Reddy 

MODIFICATIONS
	INITIALS	DATE			MODIFICATION
--------------------------------------------------------------------------------
	NR          2014-05-08      AS400/CIP Integration - Created script to show the notifications in
								User management page email report dropdown .
******/

CREATE PROCEDURE [dbo].[Filtered_Code_Sel_By_Codeset_User_Permission]
      ( 
       @Codeset_Name VARCHAR(25)
      ,@Filtered_Codeset VARCHAR(25)
      ,@User_Info_Id INT )
AS 
BEGIN


      SET NOCOUNT ON

      DECLARE @Permission_List TABLE
            ( 
             Permission_Info_Id INT PRIMARY KEY CLUSTERED )

      INSERT      INTO @Permission_List
                  ( 
                   Permission_Info_Id )
                  SELECT
                        pm.PERMISSION_INFO_ID
                  FROM
                        dbo.Group_info_permission_info_map pm
                        JOIN dbo.USER_INFO_GROUP_INFO_MAP gm
                              ON gm.group_info_id = pm.group_info_id
                  WHERE
                        gm.USER_INFO_ID = @User_Info_Id
                  GROUP BY
                        pm.PERMISSION_INFO_ID

      INSERT      INTO @Permission_List
                  ( 
                   Permission_Info_Id )
                  SELECT
                        pm.PERMISSION_INFO_ID
                  FROM
                        dbo.Group_info_permission_info_map pm
                        INNER JOIN dbo.Client_App_Access_Role_Group_Info_Map gm
                              ON pm.GROUP_INFO_ID = gm.GROUP_INFO_ID
                        INNER JOIN dbo.User_Info_Client_App_Access_Role_Map am
                              ON gm.Client_App_Access_Role_Id = am.Client_App_Access_Role_Id
                  WHERE
                        am.USER_INFO_ID = @User_Info_Id
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          @Permission_List pl
                                         WHERE
                                          pl.Permission_Info_Id = pm.PERMISSION_INFO_ID )
                  GROUP BY
                        pm.PERMISSION_INFO_ID;

      SELECT
            fltrCd.Code_Id
           ,fltrCd.Code_Value
           ,fltrCd.Code_Dsc
           ,fltrCd.Display_Seq
           ,fltrCd.Is_Default
      FROM
            dbo.Filtered_Code_Permission fcp
            INNER JOIN dbo.Code cdsetfc
                  ON cdsetfc.Code_Id = fcp.Codeset_Filtered_Cd
            INNER JOIN dbo.Codeset cdset
                  ON cdset.Codeset_Id = cdsetfc.Codeset_Id
                     AND cdset.Std_Column_Name = 'Filtered_CodeSet_Cd'
            INNER JOIN dbo.Code fltrCd
                  ON fltrCd.Code_Id = fcp.Filtered_Code_Id
            INNER JOIN dbo.Codeset fltrcdset
                  ON fltrcdset.Codeset_Id = fltrCd.Codeset_Id
            INNER JOIN @Permission_List pl
                  ON pl.Permission_Info_Id = fcp.PERMISSION_INFO_ID
      WHERE
            fltrcdset.Codeset_Name = @Codeset_Name
            AND cdsetfc.Code_Value = @Filtered_Codeset
      GROUP BY
            fltrCd.Code_Id
           ,fltrCd.Code_Value
           ,fltrCd.Code_Dsc
           ,fltrCd.Display_Seq
           ,fltrCd.Is_Default
      ORDER BY
            fltrCd.Code_Value
END;

;
GO
GRANT EXECUTE ON  [dbo].[Filtered_Code_Sel_By_Codeset_User_Permission] TO [CBMSApplication]
GO
