SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE dbo.UPDATE_NYMEX_HISTORY_P
@userId varchar,
@sessionId varchar,
@monthIdentifier datetime,
@settledPrice  decimal (32,16)


as
	set nocount on
	insert into rm_nymex_history (month_identifier,settlement_price) values
(@monthIdentifier,@settledPrice)
GO
GRANT EXECUTE ON  [dbo].[UPDATE_NYMEX_HISTORY_P] TO [CBMSApplication]
GO
