SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[cbmsELMarketData_Save]
	( @price_point_id int
	, @price_point varchar(200) = null
	, @source varchar(200) = null
	)
AS
BEGIN

	set nocount on

	  declare @this_id int

	      set @this_id = @price_point_id


	if @this_id is null
	begin

		insert into el_market_data
			( price_point
			, source
			)
		 values
			( @price_point
			, @source
			)

		set @this_id = @@IDENTITY
--		select @@IDENTITY as price_point_id

	end
	else
	begin

		   update el_market_data with (rowlock)
		      set price_point = @price_point
			, source = @source
		    where price_point_id = @this_id

	end

--	set nocount off

	exec cbmsElMarketData_Get @this_id


END


SET QUOTED_IDENTIFIER ON
GO
GRANT EXECUTE ON  [dbo].[cbmsELMarketData_Save] TO [CBMSApplication]
GO
