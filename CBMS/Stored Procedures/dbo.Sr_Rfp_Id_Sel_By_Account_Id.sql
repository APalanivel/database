SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Sr_Rfp_Id_Sel_By_Account_Id]  
     
DESCRIPTION: 
	To Get RFP id for Selected Account Id.
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Account_Id		INT						
@StartIndex		INT			1			
@EndIndex		INT			2147483647
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:          
------------------------------------------------------------
  
	EXEC dbo.Sr_Rfp_Id_Sel_By_Account_Id  101527

AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR			PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			25-MAY-10	CREATED

*/

CREATE PROCEDURE dbo.Sr_Rfp_Id_Sel_By_Account_Id
	(
	@Account_Id		INT
	,@StartIndex	INT		= 1
	,@EndIndex		INT		= 2147483647
	)
AS
BEGIN

	SET NOCOUNT ON;

	WITH Cte_Sr_Rfp_List AS
	(
	
		SELECT
			SR_RFP_ID
			,ROW_NUMBER() OVER(ORDER BY SR_RFP_ID) Row_Num
			, COUNT(1) OVER() Total_Rows
		FROM
			dbo.SR_RFP_ACCOUNT
		WHERE
			Account_Id = @Account_Id
			AND IS_DELETED = 0
	)
	SELECT
		rfp.SR_RFP_ID
		,rfp.Total_Rows
	FROM
		Cte_Sr_Rfp_List rfp
	WHERE
		rfp.Row_Num BETWEEN @StartIndex AND @EndIndex

END
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Id_Sel_By_Account_Id] TO [CBMSApplication]
GO
