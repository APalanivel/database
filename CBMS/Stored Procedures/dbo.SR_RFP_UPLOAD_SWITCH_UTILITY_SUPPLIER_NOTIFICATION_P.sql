SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[SR_RFP_UPLOAD_SWITCH_UTILITY_SUPPLIER_NOTIFICATION_P] 
	@userId INT,
	@sessionId VARCHAR(100),
	--@cbmsImageDocId VARCHAR(200),     
	--@cbmsImage image ,    
	--@contentType VARCHAR(200),    
	@cbmsImageId INT,  -- added by Jaya    
	@accountGroupId INT,
	@isBidGroup BIT, 
	@returnToTarrifDate DATETIME,
	@returnToTarrifTypeId INT,
	@isSwitchRate BIT,
	@switchSupplierDate DATETIME,
	@switchSupplierTypeId INT,
	@isSwitchSupplier BIT, 
	@rfpId INT
AS
BEGIN
  
	SET NOCOUNT ON
	
	DECLARE @entityId INT
	
	SELECT @entityId = Entity_ID FROM dbo.Entity (NOLOCK) WHERE Entity_Name = 'Supplier Notification' AND entity_type = 100
	    
	/*    
	 INSERT INTO CBMS_IMAGE (CBMS_IMAGE_TYPE_ID, CBMS_DOC_ID, CBMS_IMAGE, CONTENT_TYPE, DATE_IMAGED)     
	 VALUES (@entityId, @cbmsImageDocId, @cbmsImage, @contentType, getDate())    
	    
	declare @cbmsImageId INT    
	select @cbmsImageId = (select @@Identity)  */    
	    
	--added by Jaya for updating entityid    
	     
	UPDATE dbo.CBMS_IMAGE SET CBMS_IMAGE_TYPE_ID = @entityId where CBMS_IMAGE_ID = @cbmsImageId

	INSERT INTO dbo.SR_RFP_UTILITY_SWITCH(SR_ACCOUNT_GROUP_ID,
		IS_BID_GROUP,
		RETURN_TO_TARIFF_DATE,    
		RETURN_TO_TARIFF_TYPE_ID,    
		IS_SWITCH_RATE_ESTIMATED,    
		UTILITY_SWITCH_SUPPLIER_DATE,    
		UTILITY_SWITCH_SUPPLIER_TYPE_ID,    
		IS_SWITCH_SUPPLIER_ESTIMATED,    
		SUPPLIER_NOTICE_IMAGE_ID,    
		UPLOADED_BY,UPLOADED_DATE)	    
	VALUES(@accountGroupId,
		@isBidGroup,
		@returnToTarrifDate,
		@returnToTarrifTypeId,
		@isSwitchRate,
		@switchSupplierDate,
		@switchSupplierTypeId,
		@isSwitchSupplier,
		@cbmsImageId,
		@userId,
		GETDATE())    

	IF (@isBidGroup = 0)
	 BEGIN
	 
		UPDATE dbo.SR_RFP_CHECKLIST
			SET IS_NOTICE_GIVEN = 1
		WHERE SR_RFP_ACCOUNT_ID =  @accountGroupId

	 END
	ELSE IF (@isBidGroup > 0 )
	 BEGIN
	 
		UPDATE rfpCheckList
			SET IS_NOTICE_GIVEN = 1
		FROM dbo.SR_RFP_CHECKLIST rfpCheckList 
			INNER JOIN dbo.SR_RFP_ACCOUNT rfpAcct ON rfpAcct.SR_RFP_ACCOUNT_ID = rfpCheckList.SR_RFP_ACCOUNT_ID
		 WHERE rfpAcct.SR_RFP_BID_GROUP_ID = @accountGroupId

	 END

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_UPLOAD_SWITCH_UTILITY_SUPPLIER_NOTIFICATION_P] TO [CBMSApplication]
GO
