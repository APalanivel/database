
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Move_Charge_Dtl_by_Old_Cu_invoice_id

DESCRIPTION:
	
Tables
	
INPUT PARAMETERS:
	Name			   DataType		Default	Description
------------------------------------------------------------	     	          	
	 @Old_cu_invoice_id 	INT       	          	
	 @New_cu_invoice_id 	INT 
	 @Cu_invoice_Charge_ID  INT       	          	
	 @is_checked			BIT

	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC Move_Charge_Dtl_by_Old_Cu_invoice_id 2928606,2928619,17622733,0
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SSR			Sharad Srivastava
	RKV         Ravi kumar vegesna
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	SSR        	04/19/2010	Created
	RKV         2015-06-25  Added column EC_Invoice_Sub_Bucket_Master_Id as part of AS400
******/

CREATE PROCEDURE [dbo].[Move_Charge_Dtl_by_Old_Cu_invoice_id]
      @Old_cu_invoice_id INT
     ,@New_cu_invoice_id INT
     ,@Cu_invoice_Charge_ID INT
     ,@is_checked BIT
AS 
BEGIN

      SET NOCOUNT ON
		
      DECLARE @CHARGEOUT_TBL TABLE
            ( 
             ID INT IDENTITY(1, 1)
            ,NEW_CU_INVOICE_ID INT
            ,OLD_CU_INVOICE_ID INT
            ,NEW_CU_INVOICE_CHARGE_ID INT )
															
	
      INSERT      INTO CU_INVOICE_CHARGE
                  ( 
                   CU_INVOICE_ID
                  ,COMMODITY_TYPE_ID
                  ,UBM_SERVICE_TYPE_ID
                  ,UBM_SERVICE_CODE
                  ,Bucket_Master_Id
                  ,UBM_BUCKET_CODE
                  ,CHARGE_NAME
                  ,CHARGE_VALUE
                  ,UBM_INVOICE_DETAILS_ID
                  ,CU_DETERMINANT_CODE
                  ,EC_Invoice_Sub_Bucket_Master_Id )
      OUTPUT      inserted.CU_INVOICE_ID
                 ,@Old_cu_invoice_id
                 ,Inserted.CU_INVOICE_CHARGE_ID
                  INTO @CHARGEOUT_TBL
                  SELECT
                        @New_cu_invoice_id
                       ,COMMODITY_TYPE_ID
                       ,UBM_SERVICE_TYPE_ID
                       ,UBM_SERVICE_CODE
                       ,Bucket_Master_Id
                       ,UBM_BUCKET_CODE
                       ,CHARGE_NAME
                       ,CASE WHEN @is_checked = 0 THEN '0'
                             ELSE CHARGE_VALUE
                        END
                       ,UBM_INVOICE_DETAILS_ID
                       ,CU_DETERMINANT_CODE
                       ,EC_Invoice_Sub_Bucket_Master_Id
                  FROM
                        dbo.CU_INVOICE_CHARGE
                  WHERE
                        CU_INVOICE_ID = @Old_cu_invoice_id
                        AND CU_INVOICE_CHARGE_ID = @Cu_invoice_Charge_ID
			
			
      UPDATE
            CU_INVOICE_CHARGE
      SET   
            CHARGE_VALUE = CASE WHEN @is_checked = 1 THEN '0'
                                ELSE CHARGE_VALUE
                           END
      WHERE
            CU_INVOICE_ID = @Old_cu_invoice_id
            AND CU_INVOICE_CHARGE_ID = @Cu_invoice_Charge_ID
			
			
      INSERT      INTO CU_INVOICE_CHARGE_ACCOUNT
                  ( 
                   ACCOUNT_ID
                  ,Charge_Expression
                  ,Charge_Value
                  ,CU_INVOICE_CHARGE_ID )
                  SELECT
                        ACCOUNT_ID
                       ,Charge_Expression
                       ,CASE WHEN @is_checked = 0 THEN 0
                             ELSE Charge_Value
                        END
                       ,NEW_CU_INVOICE_CHARGE_ID
                  FROM
                        dbo.CU_INVOICE_CHARGE_ACCOUNT
                        CROSS APPLY @CHARGEOUT_TBL
                  WHERE
                        CU_INVOICE_CHARGE_ID = @CU_INVOICE_CHARGE_ID
		

      UPDATE
            CU_INVOICE_CHARGE_ACCOUNT
      SET   
            Charge_Value = CASE WHEN @is_checked = 1 THEN 0
                                ELSE Charge_Value
                           END
      WHERE
            CU_INVOICE_CHARGE_ID = @CU_INVOICE_CHARGE_ID
		

END


;
GO

GRANT EXECUTE ON  [dbo].[Move_Charge_Dtl_by_Old_Cu_invoice_id] TO [CBMSApplication]
GO
