
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
Name:  
 CBMS.dbo.Cost_Usage_Account_EL_Demand_Sum_Sel_For_Site
 
 Description:  
 
 Input Parameters:  
    Name			    DataType		Default Description  
------------------------------------------------------------------------  
    @Report_Year	    INT			    
    @Client_Id		    INT			    NULL
    @Division_Id	    INT			    NULL
    @Client_Hier_Id	    INT			    NULL
    @RegionId		    INT			    NULL
    @Country_Id			INT			    NULL
    @Site_Not_Managed   INT			    NULL
    @StartIndex			INT				1
    @EndIndex			INT				2147483647 

 
 Output Parameters:  
 Name					Datatype		Default Description  
------------------------------------------------------------  
 
 Usage Examples:
------------------------------------------------------------  

EXECUTE dbo.Cost_Usage_Account_EL_Demand_Sum_Sel_For_Site 2011, 10069, NULL, NULL, NULL, NULL, NULL,1,20
EXECUTE dbo.Cost_Usage_Account_EL_Demand_Sum_Sel_For_Site 2011,null,NULL, NULL, NULL, NULL, NULL,1,10
EXECUTE dbo.Cost_Usage_Account_EL_Demand_Sum_Sel_For_Site 2008, 170, NULL, 2195, NULL, NULL, NULL,1,30


EXECUTE dbo.Cost_Usage_Account_EL_Demand_Sum_Sel_For_Site 2011, 235, NULL, NULL, NULL, NULL, NULL,1,25
 
Author Initials:  
 Initials	  Name
------------------------------------------------------------
 AP			  Athmaram Pabbathi
 BCH		  Balaraju Chalumuri
 AKR          Ashok Kumar Raju
 
 Modifications :  
 Initials	  Date	    Modification  
------------------------------------------------------------  
 AP			10/10/2011  Created and replaced flowlling SPs as a part of Addtl Data changes
					   - dbo.cbmsCostUsageSite_LoadForSite
 AP			04/02/2012  Replaced @Site_Id param with @Client_Hier_Id; added Client_Hier_Id in result set
 AP			04/30/2012  Added Data_Source_Code in the result set
 BCH		2012-08-06 (For MAIN -1295) Added StartIndex and EndIndex input parameters for Pagination and Pivoted rows for Performance issue.
 AKR        2012-08-15   Modified the logic to fill @Service_Month
******/
CREATE PROCEDURE [dbo].Cost_Usage_Account_EL_Demand_Sum_Sel_For_Site
      ( 
       @Report_Year INT
      ,@Client_Id INT = NULL
      ,@Division_Id INT = NULL
      ,@Client_Hier_Id INT = NULL
      ,@Region_Id INT = NULL
      ,@State_Id INT = NULL
      ,@Site_Type_Id INT = NULL
      ,@StartIndex INT = 1
      ,@EndIndex INT = 2147483647 )
AS 
BEGIN
      SET NOCOUNT ON

      DECLARE
            @Start_Month DATE
           ,@End_Month DATE
           ,@Usd_Currency_Unit_Id INT
           ,@Site_Type_Name VARCHAR(200)
           ,@El_Commodity_Id INT
           ,@El_Commodity_Name VARCHAR(50)
            
            
            
      DECLARE @Service_Month TABLE
            ( 
             Service_Month DATE
            ,Month_Number SMALLINT )  
            
      CREATE TABLE #Client_Dtl
            ( 
             Client_Name VARCHAR(200)
            ,Division_Name VARCHAR(200)
            ,City VARCHAR(200)
            ,Address_Line1 VARCHAR(200)
            ,State_Name VARCHAR(20)
            ,Region_Name VARCHAR(200)
            ,Client_Hier_Id INT
            ,Site_Id INT
            ,Client_Currency_Group_Id INT
            ,Total_Rows INT )

      CREATE CLUSTERED INDEX IDX_Client_Dtl_Site_Id ON #Client_Dtl(Client_Hier_Id, Site_Id)

      SELECT
            @El_Commodity_Id = Commodity_Id
           ,@El_Commodity_Name = Commodity_Name
      FROM
            dbo.Commodity
      WHERE
            Commodity_Name = 'Electric Power'
		
      SELECT
            @Usd_Currency_Unit_Id = CU.Currency_Unit_Id
      FROM
            dbo.Currency_Unit CU
      WHERE
            CU.Symbol = 'USD'
            
      SELECT
            @Site_Type_Name = E.Entity_Name
      FROM
            dbo.Entity E
      WHERE
            E.Entity_Id = @Site_Type_Id             

      INSERT      INTO @Service_Month
                  ( 
                   Service_Month
                  ,Month_Number )
                  SELECT
                        dd.Date_D
                       ,row_number() OVER ( ORDER BY dd.Date_D )
                  FROM
                        ( SELECT
                              dateadd(m, ch.Client_Fiscal_Offset, cast('1/1/' + cast(@Report_Year AS VARCHAR(4)) AS DATE)) Start_month
                          FROM
                              Core.Client_Hier ch
                          WHERE
                              ch.Client_ID = @Client_Id
                              AND ch.Sitegroup_Id = 0
                              AND ch.Site_Id = 0
                          UNION ALL
                          SELECT
                              cast('1/1/' + cast(@report_year AS VARCHAR(4)) AS VARCHAR(10))
                          WHERE
                              @client_id IS NULL ) X
                        CROSS JOIN meta.Date_Dim dd
                  WHERE
                        dd.Date_D BETWEEN x.Start_month
                                  AND     dateadd(MONTH, -1, ( dateadd(YEAR, 1, X.Start_Month) ))  
  
      SELECT
            @Start_Month = min(sm.Service_Month)
           ,@End_Month = max(sm.Service_Month)
      FROM
            @Service_Month sm ;
                
                
      WITH  CTE_Client_Detail
              AS ( SELECT
                        ch.Client_Name
                       ,ch.Sitegroup_Name AS division_name
                       ,ch.city
                       ,ch.Site_Address_Line1 AS address_line1
                       ,ch.State_Name
                       ,ch.Region_Name
                       ,ch.Client_Hier_Id
                       ,ch.Site_Id
                       ,ch.Client_Currency_Group_Id
                       ,row_number() OVER ( ORDER BY ch.Client_Name , ch.Sitegroup_Name , ch.City , ch.Site_Id ) Row_Num
                       ,count(1) OVER ( ) Total_Rows
                   FROM
                        Core.Client_Hier ch
                   WHERE
                        ( @Client_Id IS NULL
                          OR CH.Client_Id = @Client_Id )
                        AND ( @Division_Id IS NULL
                              OR CH.Sitegroup_Id = @Division_Id )
                        AND ( @Client_Hier_Id IS NULL
                              OR CH.Client_Hier_Id = @Client_Hier_Id )
                        AND ( @Site_Type_Id IS NULL
                              OR ch.Site_Type_Name = @Site_Type_Name )
                        AND ( @State_Id IS NULL
                              OR CH.State_Id = @State_Id )
                        AND ( @Region_Id IS NULL
                              OR CH.Region_ID = @Region_Id )
                        AND CH.Client_Not_Managed = 0
                        AND CH.Site_Not_Managed = 0
                        AND CH.Site_Closed = 0
                        AND CH.Division_Not_Managed = 0
                        AND CH.Site_Id > 0
                   GROUP BY
                        ch.Client_Name
                       ,ch.Sitegroup_Name
                       ,ch.city
                       ,ch.Site_Address_Line1
                       ,ch.State_Name
                       ,ch.Region_Name
                       ,ch.City
                       ,ch.Client_Hier_Id
                       ,ch.Site_Id
                       ,ch.Client_Currency_Group_Id)
            INSERT      INTO #Client_Dtl
                        ( 
                         Client_Name
                        ,Division_Name
                        ,City
                        ,Address_Line1
                        ,State_Name
                        ,Region_Name
                        ,Client_Hier_Id
                        ,Site_Id
                        ,Client_Currency_Group_Id
                        ,Total_Rows )
                        SELECT
                              Client_Name
                             ,Division_Name
                             ,City
                             ,address_line1
                             ,State_Name
                             ,Region_Name
                             ,Client_Hier_Id
                             ,Site_Id
                             ,Client_Currency_Group_Id
                             ,Total_Rows
                        FROM
                              CTE_Client_Detail
                        WHERE
                              Row_Num BETWEEN @StartIndex AND @EndIndex ; 
                    
      SELECT
            CDtl.Client_Name
           ,CDtl.Division_Name
           ,CDtl.City
           ,CDtl.Address_Line1
           ,CDtl.State_Name
           ,CDtl.Region_Name
           ,CDtl.City AS Site_Name
           ,CDtl.Site_Id
           ,CDtl.Client_Hier_Id
           ,@Start_Month AS Fiscal_Start
           ,@End_Month AS Fiscal_End
           ,sum(case WHEN cusd.Month_Number = 1 THEN cusd.Bucket_Value
                END) Month1
           ,sum(case WHEN cusd.Month_Number = 2 THEN cusd.Bucket_Value
                END) Month2
           ,sum(case WHEN cusd.Month_Number = 3 THEN cusd.Bucket_Value
                END) Month3
           ,sum(case WHEN cusd.Month_Number = 4 THEN cusd.Bucket_Value
                END) Month4
           ,sum(case WHEN cusd.Month_Number = 5 THEN cusd.Bucket_Value
                END) Month5
           ,sum(case WHEN cusd.Month_Number = 6 THEN cusd.Bucket_Value
                END) Month6
           ,sum(case WHEN cusd.Month_Number = 7 THEN cusd.Bucket_Value
                END) Month7
           ,sum(case WHEN cusd.Month_Number = 8 THEN cusd.Bucket_Value
                END) Month8
           ,sum(case WHEN cusd.Month_Number = 9 THEN cusd.Bucket_Value
                END) Month9
           ,sum(case WHEN cusd.Month_Number = 10 THEN cusd.Bucket_Value
                END) Month10
           ,sum(case WHEN cusd.Month_Number = 11 THEN cusd.Bucket_Value
                END) Month11
           ,sum(case WHEN cusd.Month_Number = 12 THEN cusd.Bucket_Value
                END) Month12
           ,CUSD.Commodity_Name
           ,CUSD.Bucket_Name
           ,CUSD.Bucket_Type
           ,max(case WHEN cusd.Month_Number = 1 THEN cusd.Data_Source_Code
                END) Month1_Data_Scource_Code
           ,max(case WHEN cusd.Month_Number = 2 THEN cusd.Data_Source_Code
                END) Month2_Data_Scource_Code
           ,max(case WHEN cusd.Month_Number = 3 THEN cusd.Data_Source_Code
                END) Month3_Data_Scource_Code
           ,max(case WHEN cusd.Month_Number = 4 THEN cusd.Data_Source_Code
                END) Month4_Data_Scource_Code
           ,max(case WHEN cusd.Month_Number = 5 THEN cusd.Data_Source_Code
                END) Month5_Data_Scource_Code
           ,max(case WHEN cusd.Month_Number = 6 THEN cusd.Data_Source_Code
                END) Month6_Data_Scource_Code
           ,max(case WHEN cusd.Month_Number = 7 THEN cusd.Data_Source_Code
                END) Month7_Data_Scource_Code
           ,max(case WHEN cusd.Month_Number = 8 THEN cusd.Data_Source_Code
                END) Month8_Data_Scource_Code
           ,max(case WHEN cusd.Month_Number = 9 THEN cusd.Data_Source_Code
                END) Month9_Data_Scource_Code
           ,max(case WHEN cusd.Month_Number = 10 THEN cusd.Data_Source_Code
                END) Month10_Data_Scource_Code
           ,max(case WHEN cusd.Month_Number = 11 THEN cusd.Data_Source_Code
                END) Month11_Data_Scource_Code
           ,max(case WHEN cusd.Month_Number = 12 THEN cusd.Data_Source_Code
                END) Month12_Data_Scource_Code
           ,CDtl.Total_Rows
      FROM
            #Client_Dtl CDtl
            LEFT OUTER JOIN ( SELECT
                                    CUSD.Client_Hier_Id
                                   ,CUSD.Service_Month
                                   ,'Total Demand' AS Bucket_Name
                                   ,CD.Code_Value AS Bucket_Type
                                   ,coalesce(nullif(sum(case WHEN BM.Bucket_Name = 'Demand' THEN CUSD.Bucket_Value * UOMConv.Conversion_Factor
                                                             ELSE 0
                                                        END), 0), nullif(sum(case WHEN BM.Bucket_Name = 'Billed Demand' THEN CUSD.Bucket_Value * UOMConv.Conversion_Factor
                                                                                  ELSE 0
                                                                             END), 0), nullif(sum(case WHEN BM.Bucket_Name = 'Contract Demand' THEN CUSD.Bucket_Value * UOMConv.Conversion_Factor
                                                                                                       ELSE 0
                                                                                                  END), 0), 0.0) AS Bucket_Value
                                   ,@El_Commodity_Name Commodity_Name
                                   ,dsc.Code_Value AS Data_Source_Code
                                   ,Sm.Month_Number
                              FROM
                                    dbo.Cost_Usage_Site_Dtl CUSD
                                    INNER JOIN @Service_Month Sm
                                          ON Sm.Service_Month = CUSD.Service_Month
                                    INNER JOIN dbo.Bucket_Master BM
                                          ON BM.Bucket_Master_Id = CUSD.Bucket_Master_Id
                                    INNER JOIN dbo.Code CD
                                          ON CD.Code_Id = BM.Bucket_Type_Cd
                                    INNER JOIN dbo.Consumption_Unit_Conversion UOMConv
                                          ON UOMConv.Base_Unit_Id = CUSD.UOM_Type_Id
                                             AND UOMConv.Converted_Unit_Id = BM.Default_Uom_Type_Id
                                    INNER JOIN dbo.Code dsc
                                          ON dsc.Code_Id = cusd.Data_Source_cd
                              WHERE
                                    BM.Commodity_Id = @El_Commodity_Id
                                    AND BM.Bucket_Name IN ( 'Demand', 'Billed Demand', 'Contract Demand' )
                                    AND CD.Code_Value = 'Determinant'
                              GROUP BY
                                    CUSD.Client_Hier_Id
                                   ,CUSD.Service_Month
                                   ,CD.Code_Value
                                   ,dsc.Code_Value
                                   ,Sm.Month_Number ) AS CUSD
                  ON CUSD.Client_Hier_Id = CDtl.Client_Hier_Id
      GROUP BY
            CDtl.Client_Name
           ,CDtl.Division_Name
           ,CDtl.City
           ,CDtl.Address_Line1
           ,CDtl.State_Name
           ,CDtl.Region_Name
           ,CDtl.Site_Id
           ,CDtl.Client_Hier_Id
           ,CUSD.Commodity_Name
           ,CUSD.Bucket_Name
           ,CUSD.Bucket_Type
           ,CDtl.Total_Rows
      ORDER BY
            CDtl.Client_Name
           ,CDtl.Division_Name
           ,CDtl.City
           ,CDtl.Site_Id

      DROP TABLE #Client_Dtl
END 

;
GO

GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_EL_Demand_Sum_Sel_For_Site] TO [CBMSApplication]
GO
