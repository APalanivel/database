SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE [dbo].[cbmsInvoiceActivity_LoadClosedReason]
	( @MyAccountId int
	, @begin_date datetime
	, @end_date datetime
	, @client_id int = null
	, @division_id int = null
	, @site_id int = null
	, @region_id int = null
	, @country_id int = null
	, @state_id int = null
	, @vendor_type_id int = null
	, @vendor_id int = null
	, @analyst_id int = null
	)
AS
BEGIN

	set nocount on
	set @end_date = DateAdd(d, 1, @end_date)
	set nocount off


	   select cr.entity_name closed_reason_type
		, isNull(x.reason_count, 0) reason_count
	     from entity cr
  left outer join (
		   select it.closed_reason_type_id
			, count(*) reason_count
		     from invoice_image_token it 
	  left outer join site s on s.site_id = it.site_id
	  left outer join division d on d.division_id = s.division_id
	  left outer join address ad on ad.address_id = s.primary_address_id
	  left outer join state st on st.state_id = ad.state_id
	  left outer join account a on a.account_id = it.account_id
	  left outer join vendor v on v.vendor_id = a.vendor_id
		    where (it.event_date between @begin_date and @end_date)
		      and (s.site_id 		= isNull(@site_id, s.site_id) 		or (s.site_id is null and @site_id is null))
		      and (d.division_id 	= isNull(@division_id, d.division_id) 	or (d.division_id is null and @division_id is null))
		      and (d.client_id 		= isNull(@client_id, d.client_id) 	or (d.client_id is null and @client_id is null))
		      and (st.region_id 	= isNull(@region_id, st.region_id) 	or (st.region_id is null and @region_id is null))
		      and (st.country_id 	= isNull(@country_id, st.country_id) 	or (st.country_id is null and @country_id is null))
		      and (st.state_id 		= isNull(@state_id, st.state_id) 	or (st.state_id is null and @state_id is null))
		      and (v.vendor_type_id 	= isNull(@vendor_type_id, v.vendor_type_id) or (v.vendor_type_id is null and @vendor_type_id is null))
		      and (a.vendor_id 		= isNull(@vendor_id, a.vendor_id) 	or (a.vendor_id is null and @vendor_id is null))
		      and it.is_closed = 1
		      and it.is_default = 1
		 group by it.closed_reason_type_id
		  ) x on x.closed_reason_type_id = cr.entity_id
	    where cr.entity_type = 652
	 order by cr.entity_name



END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvoiceActivity_LoadClosedReason] TO [CBMSApplication]
GO
