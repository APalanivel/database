SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[cbmsContractCbmsImageMap_GetForContract]
	( @MyAccountId int
	, @contract_id int
	)
AS
BEGIN

	   select m.contract_id
		, m.cbms_image_id
		, i.cbms_doc_id
	     from contract_cbms_image_map m
	     join cbms_image i on i.cbms_image_id = m.cbms_image_id
	    where m.contract_id = @contract_id 

END



GO
GRANT EXECUTE ON  [dbo].[cbmsContractCbmsImageMap_GetForContract] TO [CBMSApplication]
GO
