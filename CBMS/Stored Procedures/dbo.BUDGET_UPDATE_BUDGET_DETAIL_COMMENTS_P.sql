SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.BUDGET_UPDATE_BUDGET_DETAIL_COMMENTS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@budget_account_id	int       	          	
	@commodityType 	varchar(20)	          	
	@general_comments	varchar(1000)	          	
	@variable_comments	varchar(1000)	          	
	@distribution_comments	varchar(1000)	          	
	@other_bundled_comments	varchar(1000)	          	
	@other_fixed_costs_comments	varchar(1000)	          	
	@sourcing_tax_comments	varchar(1000)	          	
	@rates_tax_comments	varchar(1000)	          	
	@is_variable_on_dv	bit       	          	
	@is_distribution_on_dv	bit       	          	
	@is_other_bundled_on_dv	bit       	          	
	@is_other_fixed_costs_on_dv	bit       	          	
	@is_sourcing_tax_on_dv	bit       	          	
	@is_rates_tax_on_dv	bit       	          	
	@show_on_dv    	bit       	          	
	@transportation_comments	varchar(1000)	          	
	@is_transportation_on_dv	bit       	          	
	@transmission_comments	varchar(1000)	          	
	@is_transmission_on_dv	bit       	          	
	@manager_comments	varchar(4000)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

--select * from budget_account where budget_account_id=75518




CREATE        PROCEDURE dbo.BUDGET_UPDATE_BUDGET_DETAIL_COMMENTS_P
	@userId varchar(10),
	@sessionId varchar(20),
	@budget_account_id int,
	@commodityType varchar(20),
	@general_comments varchar(1000),
	@variable_comments varchar(1000),
	@distribution_comments varchar(1000),
	@other_bundled_comments varchar(1000),
	@other_fixed_costs_comments varchar(1000),
	@sourcing_tax_comments varchar(1000),
	@rates_tax_comments varchar(1000),
	@is_variable_on_dv bit,
	@is_distribution_on_dv bit,
	@is_other_bundled_on_dv bit,
	@is_other_fixed_costs_on_dv bit,
	@is_sourcing_tax_on_dv bit,
	@is_rates_tax_on_dv bit,
	@show_on_dv bit,
	@transportation_comments varchar(1000),
	@is_transportation_on_dv bit,
	@transmission_comments varchar(1000),
	@is_transmission_on_dv bit
	,@manager_comments varchar(4000)
	
	
	AS
begin
	
	set nocount on

		update budget 
		set 
		is_shown_comments_on_dv=@show_on_dv
		where budget_id=(select budget_id from budget_account where budget_account_id = @budget_account_id)

		update budget_account 
		set
		budget_comments=@general_comments
		where budget_account_id=@budget_account_id

		update budget_account 
		set
		manager_comments=@manager_comments
		where budget_account_id=@budget_account_id

		declare @is_record_exists int
		select @is_record_exists=count(budget_account_id) from budget_detail_comments where budget_account_id=@budget_account_id
		if(@is_record_exists = 0)
		begin
			insert into budget_detail_comments(
			budget_account_id,
			variable_comments,
			is_variable_on_dv,
			distribution_comments,
			is_distribution_on_dv,
			other_bundled_comments,
			is_other_bundled_on_dv,
			other_fixed_costs_comments,
			is_other_fixed_costs_on_dv,
			sourcing_tax_comments,
			is_sourcing_tax_on_dv,
			rates_tax_comments,
			is_rates_tax_on_dv,
			transportation_comments,
			is_transportation_on_dv,
			transmission_comments,
			is_transmission_on_dv)
			values(
			@budget_account_id,
			@variable_comments,
			@is_variable_on_dv,
			@distribution_comments,
			@is_distribution_on_dv,
			@other_bundled_comments,
			@is_other_bundled_on_dv,
			@other_fixed_costs_comments,
			@is_other_fixed_costs_on_dv,
			@sourcing_tax_comments,
			@is_sourcing_tax_on_dv,
			@rates_tax_comments,
			@is_rates_tax_on_dv,
			@transportation_comments,
			@is_transportation_on_dv,
			@transmission_comments,
			@is_transmission_on_dv)
		end
		else
		begin
			if(@commodityType = 'Natural Gas')
			begin	
				update budget_detail_comments
				set
				variable_comments=@variable_comments,
				is_variable_on_dv=@is_variable_on_dv,
				distribution_comments=@distribution_comments,
				is_distribution_on_dv=@is_distribution_on_dv,
				transportation_comments=@transportation_comments,
				is_transportation_on_dv=@is_transportation_on_dv,
				other_bundled_comments=@other_bundled_comments,
				is_other_bundled_on_dv=@is_other_bundled_on_dv,
				other_fixed_costs_comments=@other_fixed_costs_comments,
				is_other_fixed_costs_on_dv=@is_other_fixed_costs_on_dv,
				sourcing_tax_comments=@sourcing_tax_comments,
				is_sourcing_tax_on_dv=@is_sourcing_tax_on_dv,
				rates_tax_comments=@rates_tax_comments,
				is_rates_tax_on_dv=@is_rates_tax_on_dv
				where budget_account_id=@budget_account_id
			end
			else
			begin
				update budget_detail_comments
				set
				variable_comments=@variable_comments,
				is_variable_on_dv=@is_variable_on_dv,
				distribution_comments=@distribution_comments,
				is_distribution_on_dv=@is_distribution_on_dv,
				transmission_comments=@transmission_comments,
				is_transmission_on_dv=@is_transmission_on_dv,
				other_bundled_comments=@other_bundled_comments,
				is_other_bundled_on_dv=@is_other_bundled_on_dv,
				other_fixed_costs_comments=@other_fixed_costs_comments,
				is_other_fixed_costs_on_dv=@is_other_fixed_costs_on_dv,
				sourcing_tax_comments=@sourcing_tax_comments,
				is_sourcing_tax_on_dv=@is_sourcing_tax_on_dv,
				rates_tax_comments=@rates_tax_comments,
				is_rates_tax_on_dv=@is_rates_tax_on_dv
				where budget_account_id=@budget_account_id
			end
		end

	end
GO
GRANT EXECUTE ON  [dbo].[BUDGET_UPDATE_BUDGET_DETAIL_COMMENTS_P] TO [CBMSApplication]
GO
