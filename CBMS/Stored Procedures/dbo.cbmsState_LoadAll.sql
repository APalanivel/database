SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                            
 NAME: [dbo].[cbmsState_LoadAll]                
                            
 DESCRIPTION:                            
          
                            
 INPUT PARAMETERS:              
                         
 Name                        DataType         Default       Description            
---------------------------------------------------------------------------------------------------------------          
 @MyAccountId				     INT
 @country_id					 INT 
 @region_id						 INT
 @state_name					 VARCHAR(200)
 @Start_Index					 INT 
 @End_Index						 INT 
 @Total_Row_Count				 INT
              
                            
 OUTPUT PARAMETERS:              
                               
 Name                        DataType         Default       Description            
---------------------------------------------------------------------------------------------------------------          
                            
 USAGE EXAMPLES:                                
---------------------------------------------------------------------------------------------------------------                                
   
   EXEC [dbo].[cbmsState_LoadAll]   7347,@state_name='K',@Start_Index=1,@End_Index=3
           
                           
 AUTHOR INITIALS:            
           
 Initials              Name            
---------------------------------------------------------------------------------------------------------------                          
 
 MODIFICATIONS:          
              
 Initials              Date             Modification          
---------------------------------------------------------------------------------------------------------------          
              
                           
******/
CREATE PROCEDURE [dbo].[cbmsState_LoadAll]
    (
        @MyAccountId INT
        , @country_id INT = NULL
        , @region_id INT = NULL
        , @state_name VARCHAR(200) = NULL
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
        , @Total_Row_Count INT = 0
    )
AS
    BEGIN
        SET NOCOUNT ON;
        WITH Cte_States
        AS (
               SELECT
                    st.STATE_ID
                    , st.STATE_NAME
                    , r.REGION_ID
                    , r.REGION_NAME
                    , c.COUNTRY_NAME
                    , c.COUNTRY_ID
                    , st.STATE_NAME AS stateprovince_name
                    , ROW_NUMBER() OVER (ORDER BY
                                             st.STATE_ID) Row_Num
                    , COUNT(1) OVER () AS Total_Row_Count
               FROM
                    STATE st
                    JOIN REGION r
                        ON r.REGION_ID = st.REGION_ID
                    JOIN COUNTRY c
                        ON c.COUNTRY_ID = st.COUNTRY_ID
               WHERE
                    c.COUNTRY_ID = ISNULL(@country_id, c.COUNTRY_ID)
                    AND r.REGION_ID = ISNULL(@region_id, r.REGION_ID)
                    AND st.STATE_NAME LIKE '%' + ISNULL(@state_name, st.STATE_NAME) + '%'
               GROUP BY
                   st.STATE_ID
                   , st.STATE_NAME
                   , r.REGION_ID
                   , r.REGION_NAME
                   , c.COUNTRY_NAME
                   , c.COUNTRY_ID
                   , st.STATE_NAME
           )
        SELECT
            cs.STATE_ID
            , cs.STATE_NAME
            , cs.REGION_ID
            , cs.REGION_NAME
            , cs.COUNTRY_NAME
            , cs.COUNTRY_ID
            , cs.stateprovince_name
        FROM
            Cte_States cs
        WHERE
            cs.Row_Num BETWEEN @Start_Index
                       AND     @End_Index
        ORDER BY
            cs.COUNTRY_NAME DESC
            , cs.STATE_NAME;

    END;

GO
GRANT EXECUTE ON  [dbo].[cbmsState_LoadAll] TO [CBMSApplication]
GO
