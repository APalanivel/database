SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.DELTOOL_ACCOUNT_GET_HEADER_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@account_id    	varchar(20)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

--SELECT * FROM CLIENT
--SELECT * FROM ADdRESS
--SELECT * FROM STATE
--SELECT * FROM METER


--EXEC DELTOOL_ACCOUNT_GET_HEADER_P -1,32497
--EXEC DELTOOL_ACCOUNT_GET_HEADER_P -1,8066


CREATE                     PROCEDURE dbo.DELTOOL_ACCOUNT_GET_HEADER_P
	( @MyAccountId int
	, @account_id varchar (20)
	)
AS
BEGIN

	set nocount on


SELECT ST.STATE_NAME 
 ,ACCT.ACCOUNT_NUMBER
 ,V.VENDOR_NAME
 ,M.METER_NUMBER
 ,C.CLIENT_NAME
 ,ADDR.CITY
 
  
 FROM ACCOUNT ACCT
 JOIN VENDOR V WITH(NOLOCK) ON V.VENDOR_ID = ACCT.VENDOR_ID
 JOIN METER M WITH(NOLOCK) ON M.ACCOUNT_ID = ACCT.ACCOUNT_ID
 JOIN ADDRESS ADDR WITH(NOLOCK) ON ADDR.ADDRESS_ID = M.ADDRESS_ID  
 JOIN STATE ST WITH(NOLOCK) ON ST.STATE_ID = ADDR.STATE_ID 
 JOIN SITE S WITH(NOLOCK) ON S.SITE_ID = ADDR.ADDRESS_PARENT_ID
 JOIN DIVISION D WITH(NOLOCK) ON D.DIVISION_ID = S.DIVISION_ID
 JOIN CLIENT C WITH(NOLOCK) ON C.CLIENT_ID = D.CLIENT_ID AND C.NOT_MANAGED = 0 
 WHERE ACCT.ACCOUNT_ID = @account_id
 AND ACCT.ACCOUNT_TYPE_ID = 38
 
 ORDER BY ACCT.ACCOUNT_ID 



END
GO
GRANT EXECUTE ON  [dbo].[DELTOOL_ACCOUNT_GET_HEADER_P] TO [CBMSApplication]
GO
