SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_INSERT_ACCOUNT_METER_DETAILS_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@rfpAccountId int,
@meterId int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
--EXEC dbo.SR_RFP_INSERT_ACCOUNT_METER_DETAILS_P
--@rfpAccountId = 22442,
--@meterId = 3653

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_INSERT_ACCOUNT_METER_DETAILS_P
@rfpAccountId int,
@meterId int

as
	
set nocount on

INSERT INTO SR_RFP_ACCOUNT_METER_MAP
(
	METER_ID, 
	SR_RFP_ACCOUNT_ID 
)
VALUES 
(
	@meterId,
	@rfpAccountId 
)
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_INSERT_ACCOUNT_METER_DETAILS_P] TO [CBMSApplication]
GO
