SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 	dbo.Sr_Rfp_Bid_Group_Sel_By_Rfp  
  
DESCRIPTION:  
  
INPUT PARAMETERS:  
	Name			DataType	Default		Description  
------------------------------------------------------------  
	@rfpId          INT     
         
   
OUTPUT PARAMETERS:  
	 Name		DataType	Default		Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
  
	EXEC dbo.Sr_Rfp_Bid_Group_Sel_By_Rfp 13591
	EXEC dbo.Sr_Rfp_Bid_Group_Sel_By_Rfp 13614
	EXEC dbo.Sr_Rfp_Bid_Group_Sel_By_Rfp 12873
    
   
AUTHOR INITIALS:  
	Initials	Name  
------------------------------------------------------------  
	RR			Raghu Reddy
   
MODIFICATIONS  
  
	Initials	Date		Modification  
------------------------------------------------------------  
	RR			2016-09-19	MAINT-4236 Created
	
******/
  
CREATE PROCEDURE [dbo].[Sr_Rfp_Bid_Group_Sel_By_Rfp] @rfpId INT
AS 
BEGIN  
  
      SET NOCOUNT ON;  
      
      DECLARE @Tbl_Bidgroup TABLE
            ( 
             Sr_Rfp_Id INT
            ,Sr_Rfp_Bid_Group_Id INT
            ,Group_Name VARCHAR(200)
            ,Sort_Order INT )
            
      INSERT      INTO @Tbl_Bidgroup
                  ( 
                   Sr_Rfp_Id
                  ,Sr_Rfp_Bid_Group_Id
                  ,Group_Name
                  ,Sort_Order )
                  SELECT
                        sra.SR_RFP_ID
                       ,srbg.SR_RFP_BID_GROUP_ID
                       ,srbg.GROUP_NAME
                       ,1 AS Sort_Order
                  FROM
                        dbo.SR_RFP_ACCOUNT sra
                        INNER JOIN dbo.SR_RFP_BID_GROUP srbg
                              ON sra.SR_RFP_BID_GROUP_ID = srbg.SR_RFP_BID_GROUP_ID
                  WHERE
                        sra.SR_RFP_ID = @rfpId
                        AND sra.IS_DELETED = 0
                  GROUP BY
                        sra.SR_RFP_ID
                       ,srbg.SR_RFP_BID_GROUP_ID
                       ,srbg.GROUP_NAME
                       
      INSERT      INTO @Tbl_Bidgroup
                  ( 
                   Sr_Rfp_Id
                  ,Sr_Rfp_Bid_Group_Id
                  ,Group_Name
                  ,Sort_Order )
                  SELECT
                        @rfpId AS Sr_Rfp_Id
                       ,-1 AS Sr_Rfp_Bid_Group_Id
                       ,'Ungrouped' AS Group_Name
                       ,2 AS Sort_Order
                  WHERE
                        EXISTS ( SELECT
                                    1
                                 FROM
                                    dbo.SR_RFP_ACCOUNT sra
                                 WHERE
                                    sra.SR_RFP_ID = @rfpId
                                    AND sra.IS_DELETED = 0
                                    AND sra.SR_RFP_BID_GROUP_ID IS NULL )
                        
      SELECT
            Sr_Rfp_Id
           ,Sr_Rfp_Bid_Group_Id
           ,Group_Name
      FROM
            @Tbl_Bidgroup
      ORDER BY
            Sort_Order
           ,Group_Name
                 
END;
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Bid_Group_Sel_By_Rfp] TO [CBMSApplication]
GO
