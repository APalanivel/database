
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_SAVE_CLOSURE_SITE_DETAILS_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
	@rfp_id              int
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
Exec dbo.SR_RFP_SAVE_CLOSURE_SITE_DETAILS_P 274

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DR		Deana Ritter
    AKR     Ashok Kumar Raju
    
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
 AKR      2012-01-21 Changes Summit to Schnider Electric as a part of Name Change Project

******/
CREATE   PROCEDURE dbo.SR_RFP_SAVE_CLOSURE_SITE_DETAILS_P ( @rfpId INT )
AS 
BEGIN
      SET NOCOUNT ON ;

      DECLARE @temp TABLE ( [site_id] [int] )

	--to get all sites under the rfp
      INSERT      INTO @temp
                  ( 
                   site_id )
                  SELECT DISTINCT
                        account.site_id
                  FROM
                        sr_rfp_account
                       ,account
                  WHERE
                        sr_rfp_account.account_id = account.account_id
                        AND sr_rfp_account.sr_rfp_id = @rfpId--@rfpId
                        AND sr_rfp_account.is_deleted = 0
			    
	
      DECLARE
            @siteid INT
           ,@allacccount INT
           ,@closedacccount INT
           ,@projectid INT
           ,@user_id VARCHAR(10)

      WHILE ( ( SELECT
                  count(site_id)
                FROM
                  @temp ) > 0 ) 
            BEGIN--while begin
                  SET @siteid = ( SELECT TOP 1 * FROM @temp )

	--print @siteid

                  IF ( SELECT
                        count(*)
                       FROM
                        RFP_SITE_PROJECT
                       WHERE
                        RFP_ID = @rfpid
                        AND SITE_ID = @siteId ) > 0 
                        BEGIN --if 1
                              SET @projectId = ( SELECT
                                                      SSO_PROJECT_ID
                                                 FROM
                                                      RFP_SITE_PROJECT
                                                 WHERE
                                                      RFP_ID = @rfpid
                                                      AND SITE_ID = @siteid )
                              SET @user_id = ( SELECT TOP 1
                                                user_info_id
                                               FROM
                                                client_cem_map
                                               ,vwsitename
                                               WHERE
                                                vwsitename.site_id = @siteid
                                                AND vwsitename.client_id = client_cem_map.client_id )

	/*if (select count(SSO_PROJECT_ACTIVITY_ID) from SSO_PROJECT_ACTIVITY  where SSO_PROJECT_ID = @projectId and ACTIVITY_DESCRIPTION = 'Summit is taking action to implement the approved path forward. DashboarDView will reflect the actions taken and will categorize any new supporting information, contracts or other documents.' )= 0
	begin --if 2
		insert into SSO_PROJECT_ACTIVITY (	SSO_PROJECT_ID , 
							CREATED_BY_ID ,
							ACTIVITY_DATE,
							ACTIVITY_DESCRIPTION 
				) values

				(	@projectId , 
					@user_id , 
					getDate() , 
					'Summit is taking action to implement the approved path forward. DashboarDView will reflect the actions taken and will categorize any new supporting information, contracts or other documents.'

				)
	end--if 2*/
	
	--query to select all accounts for that rfp id and siteid
                              SELECT
                                    @allacccount = count(sr_rfp_account_id)
                              FROM
                                    sr_rfp_account
                                   ,account
                              WHERE
                                    sr_rfp_account.sr_rfp_id = @rfpId
                                    AND sr_rfp_account.is_deleted = 0
                                    AND sr_rfp_account.account_id = account.account_id
                                    AND account.site_id = @siteid 

	--query to select all closed accounts for that rfp id and siteid
                              SELECT
                                    @closedacccount = count(sr_rfp_account_id)
                              FROM
                                    sr_rfp_closure
                                   ,sr_rfp_account
                                   ,account
                              WHERE
                                    sr_rfp_closure.sr_account_group_id = sr_rfp_account.sr_rfp_account_id
                                    AND sr_rfp_account.sr_rfp_id = @rfpId
                                    AND sr_rfp_account.is_deleted = 0
                                    AND sr_rfp_account.account_id = account.account_id
                                    AND account.site_id = @siteid 

                              IF ( @allacccount = @closedacccount ) 
                                    BEGIN--if 3

                                          IF ( SELECT
                                                count(SSO_PROJECT_ACTIVITY_ID)
                                               FROM
                                                SSO_PROJECT_ACTIVITY
                                               WHERE
                                                SSO_PROJECT_ID = @projectId
                                                AND ACTIVITY_DESCRIPTION = 'Schneider Electric is taking action to implement the approved path forward. Resource Advisor will reflect the actions taken and will categorize any new supporting information, contracts or other documents.' ) = 0 
                                                BEGIN --if 7
                                                      INSERT      INTO SSO_PROJECT_ACTIVITY
                                                                  ( 
                                                                   SSO_PROJECT_ID
                                                                  ,CREATED_BY_ID
                                                                  ,ACTIVITY_DATE
                                                                  ,ACTIVITY_DESCRIPTION )
                                                      VALUES
                                                                  ( 
                                                                   @projectId
                                                                  ,@user_id
                                                                  ,getdate()
                                                                  ,'Schneider Electric is taking action to implement the approved path forward. Resource Advisor will reflect the actions taken and will categorize any new supporting information, contracts or other documents.' )
                                                END--if 7

                                          IF ( SELECT
                                                count(SSO_PROJECT_STEP_ID)
                                               FROM
                                                SSO_PROJECT_STEP
                                               WHERE
                                                SSO_PROJECT_ID = @projectId
                                                AND STEP_NO = 5 ) > 0 
                                                BEGIN --if 4
                                                      UPDATE
                                                            sso_project_step
                                                      SET   
                                                            is_active = 0
                                                           ,is_complete = 1
                                                      WHERE
                                                            sso_project_id = @projectid
                                                            AND step_no = 5
	--added for 18499
                                                      UPDATE
                                                            sso_project
                                                      SET   
                                                            project_status_type_id = ( SELECT
                                                                                          entity_id
                                                                                       FROM
                                                                                          entity
                                                                                       WHERE
                                                                                          entity_type = 601
                                                                                          AND entity_name = 'Completed' )
                                                      WHERE
                                                            sso_project_id = @projectid 
	
                                                END--if 4
                                          ELSE 
                                                BEGIN
                                                      INSERT      INTO SSO_PROJECT_STEP
                                                                  ( 
                                                                   SSO_PROJECT_ID
                                                                  ,STEP_NO
                                                                  ,IS_ACTIVE
                                                                  ,IS_COMPLETE )
                                                      VALUES
                                                                  ( 
                                                                   @projectId
                                                                  ,5
                                                                  ,0
                                                                  ,1 )
	--added for 18499
                                                      UPDATE
                                                            sso_project
                                                      SET   
                                                            project_status_type_id = ( SELECT
                                                                                          entity_id
                                                                                       FROM
                                                                                          entity
                                                                                       WHERE
                                                                                          entity_type = 601
                                                                                          AND entity_name = 'Completed' )
                                                      WHERE
                                                            sso_project_id = @projectid 


                                                END--else

                                    END--if 3
                              ELSE 
                                    BEGIN --//if3 else
                                          IF ( @allacccount <> @closedacccount
                                               AND @closedacccount >= 1 )-- if6 is added for Bz21487 
                                                BEGIN --if 6

                                                      IF ( SELECT
                                                            count(SSO_PROJECT_ACTIVITY_ID)
                                                           FROM
                                                            SSO_PROJECT_ACTIVITY
                                                           WHERE
                                                            SSO_PROJECT_ID = @projectId
                                                            AND ACTIVITY_DESCRIPTION = 'Schneider Electric is taking action to implement the approved path forward. Resource Advisor will reflect the actions taken and will categorize any new supporting information, contracts or other documents.' ) = 0 
                                                            BEGIN --if 7
                                                                  INSERT      INTO SSO_PROJECT_ACTIVITY
                                                                              ( 
                                                                               SSO_PROJECT_ID
                                                                              ,CREATED_BY_ID
                                                                              ,ACTIVITY_DATE
                                                                              ,ACTIVITY_DESCRIPTION )
                                                                  VALUES
                                                                              ( 
                                                                               @projectId
                                                                              ,@user_id
                                                                              ,getdate()
                                                                              ,'Schneider Electric is taking action to implement the approved path forward. Resource Advisor will reflect the actions taken and will categorize any new supporting information, contracts or other documents.' )
                                                            END--if 7

                                                      IF ( SELECT
                                                            count(SSO_PROJECT_STEP_ID)
                                                           FROM
                                                            SSO_PROJECT_STEP
                                                           WHERE
                                                            SSO_PROJECT_ID = @projectId
                                                            AND STEP_NO = 5 ) = 0 
                                                            BEGIN --if 5
                                                                  INSERT      INTO SSO_PROJECT_STEP
                                                                              ( 
                                                                               SSO_PROJECT_ID
                                                                              ,STEP_NO
                                                                              ,IS_ACTIVE
                                                                              ,IS_COMPLETE )
                                                                  VALUES
                                                                              ( 
                                                                               @projectId
                                                                              ,5
                                                                              ,1
                                                                              ,0 )
		--added for 18499
                                                                  UPDATE
                                                                        sso_project
                                                                  SET   
                                                                        project_status_type_id = ( SELECT
                                                                                                      entity_id
                                                                                                   FROM
                                                                                                      entity
                                                                                                   WHERE
                                                                                                      entity_type = 601
                                                                                                      AND entity_name = 'Open' )
                                                                  WHERE
                                                                        sso_project_id = @projectid 
	
                                                            END--if 5
                                                END --if 6
                                    END --//if3 else end
                        END --if 1

                  DELETE FROM
                        @temp
                  WHERE
                        site_id = @siteid
	
            END--while end
END

;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_SAVE_CLOSURE_SITE_DETAILS_P] TO [CBMSApplication]
GO
