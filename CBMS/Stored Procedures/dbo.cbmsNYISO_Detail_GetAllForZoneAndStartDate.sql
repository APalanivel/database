SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO




create PROCEDURE [dbo].[cbmsNYISO_Detail_GetAllForZoneAndStartDate]
	( @zone_id int
	, @year int
	, @pricing_type varchar(50)
	, @date datetime
	)
AS
BEGIN

	   select zd.zone_detail_id
		, zd.zone_id
		, zd.date
		, zd.ptid
		, zd.lbmp
		, zd.marginal_cost_losses
		, zd.marginal_cost_congestion
		, zd.pricing_type
		, zd.scarcity	
	     from ny_iso_detail zd
	    where zone_id = @zone_id
	      and datepart(yyyy,zd.date) = @year
	      and pricing_type = @pricing_type
	      and zd.date >= @date
	 order by date

END

GO
GRANT EXECUTE ON  [dbo].[cbmsNYISO_Detail_GetAllForZoneAndStartDate] TO [CBMSApplication]
GO
