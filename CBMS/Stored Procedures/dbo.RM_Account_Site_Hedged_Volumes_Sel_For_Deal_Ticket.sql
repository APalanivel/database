SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                            
Name:                            
        dbo.RM_Account_Site_Hedged_Volumes_Sel_For_Deal_Ticket                          
                            
Description:                            
                            
Input Parameters:                            
    Name			DataType        Default     Description                              
--------------------------------------------------------------------------------      
	@Client_Id		INT      
    @Commodity_Id	INT      
    @Hedge_Type		INT      
    @Start_Dt		DATE      
    @End_Dt			DATE      
    @Contract_Id	VARCHAR(MAX)      
    @Site_Id		INT    NULL      
                            
Output Parameters:                                  
	Name            Datatype        Default  Description                                  
--------------------------------------------------------------------------------      
                          
Usage Examples:                                
--------------------------------------------------------------------------------      
DECLARE @Trade_tvp_Total_Forecast_Current_Deal_Hedged_ToDate_Volumes AS Trade.tvp_Total_Forecast_Current_Deal_Hedged_ToDate_Volumes     
  INSERT INTO @Trade_tvp_Total_Forecast_Current_Deal_Hedged_ToDate_Volumes    
  VALUES ( '2020-01-01', 193135, 207672,10937 )     
 DECLARE @Trade_tvp_Hedge_Contract_Sites AS Trade.tvp_Hedge_Contract_Sites    
  INSERT INTO @Trade_tvp_Hedge_Contract_Sites    
  VALUES ( 156525, 273780 ),( 160699, 1950 ),( 165676, 273771 ),( 166580, 273780 )    
 EXEC dbo.RM_Account_Site_Hedged_Volumes_Sel_For_Deal_Ticket  @Client_Id = 1005,@Commodity_Id = 291      
  ,@Start_Dt='2020-01-01',@End_Dt='2020-01-01',@Hedge_Type=586,@Uom_Id = 25    
  ,@Trade_tvp_Total_Forecast_Current_Deal_Hedged_ToDate_Volumes= @Trade_tvp_Total_Forecast_Current_Deal_Hedged_ToDate_Volumes    
  ,@Trade_tvp_Hedge_Contract_Sites= @Trade_tvp_Hedge_Contract_Sites    
  ,@Deal_Ticket_Frequency_Cd = 102749,@Trade_Action_Type_Cd=102751,@Index_Id=325       
Author Initials:                            
    Initials    Name                            
--------------------------------------------------------------------------------      
    RR          Raghu Reddy         
                             
 Modifications:                            
    Initials	Date        Modification                            
--------------------------------------------------------------------------------      
	RR          2019-08-30  Created GRM-1491 
	RKV         2019-09-18  Changed the Aggrigtion from Sum to max for Hedged_Volume in CTE.
	RR			2109-09-25	GRM-1530 - Added Site_Forecast_Volume to select list
	RR			2019-10-01	GRM-1550 Modified to exclude hedged volume in current deal volume allocation
	RR			18-10-2019	GRM-1575 Modified to retrun Site_Forecast_Volume as per selected frequency
	RR			2019-10-31	GRM-1550 Modified to exclude already hedged volume in current deal volume calculation
	RR			2020-01-24	GRM-1690 Reverted GRM-1550 fix
	RR			2020-01-24	GRM-1768 Site level hedged volumes are calculating as per deal ticket frequency daily/monthly 
******/
CREATE PROCEDURE [dbo].[RM_Account_Site_Hedged_Volumes_Sel_For_Deal_Ticket]
    (
        @Client_Id INT
        , @Commodity_Id INT
        , @Hedge_Type INT
        , @Start_Dt DATE
        , @End_Dt DATE
        , @Contract_Id VARCHAR(MAX) = NULL
        , @Uom_Id INT = NULL
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
        , @Trade_tvp_Total_Forecast_Current_Deal_Hedged_ToDate_Volumes AS Trade.tvp_Total_Forecast_Current_Deal_Hedged_ToDate_Volumes READONLY
        , @Hedge_Mode_Type_Id INT = NULL
        , @Index_Id INT = NULL
        , @Deal_Ticket_Frequency_Cd INT = NULL
        , @Trade_Action_Type_Cd INT = NULL
        , @Trade_tvp_Hedge_Contract_Sites AS Trade.tvp_Hedge_Contract_Sites READONLY
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Total_Cnt INT;

        CREATE TABLE #DT_Hedged_Volumes
             (
                 Client_Hier_Id INT
                 , Account_Id INT
                 , Deal_Month DATE
                 , Hedged_Volume DECIMAL(28, 0)
                 , Deal_Ticket_Id INT
                 , Contract_Id INT
             );

        CREATE TABLE #Hedged_Volumes
             (
                 Client_Hier_Id INT
                 , Account_Id INT
                 , Deal_Month DATE
                 , Hedged_Volume DECIMAL(28, 0)
             );

        CREATE TABLE #DT_Site_Hedged_Volumes
             (
                 Client_Hier_Id INT
                 , Account_Id INT
                 , Deal_Month DATE
                 , Hedged_Volume DECIMAL(28, 0)
                 , Deal_Ticket_Id INT
                 , Contract_Id INT
             );

        CREATE TABLE #Site_Hedged_Volumes
             (
                 Client_Hier_Id INT
                 , Deal_Month DATE
                 , Hedged_Volume DECIMAL(28, 0)
             );


        CREATE TABLE #Volumes
             (
                 Client_Hier_Id INT
                 , Site_Id INT
                 , Site_name VARCHAR(200)
                 , Account_Id INT
                 , Account_Number VARCHAR(50)
                 , Contract_Vendor VARCHAR(500)
                 , Service_Month DATE
                 , Forecast_Volume DECIMAL(28, 0)
                 , Uom_Id INT
                 , Hedged_Volume DECIMAL(28, 0)
                 , Max_Hedge_Volume DECIMAL(28, 0)
                 , Max_Hedge_Percentage DECIMAL(5, 2)
                 , Do_Not_Hedge BIT
                 , Site_Not_Managed BIT
                 , Contract_Id INT
                 , Contract_Volume DECIMAL(28, 0)
                 , Client_Id INT
                 , Sitegroup_Id INT
                 , State_Id INT
             );

        CREATE TABLE #Allotted_Monthly_Volumes
             (
                 Service_Month DATE
                 , Allotted_Hedge_Volume DECIMAL(28, 0)
                 , Total_Hedged_Volume DECIMAL(28, 0)
             );

        CREATE TABLE #Adjust_Lagging_Diff_Volumes
             (
                 Row_Num INT
                 , Service_Month DATE
                 , Forecast_Volume INT
                 , Current_Deal_Volume INT
                 , Diff_Volume INT
                 , Adjust_Volume INT
                 , New_Row_Num INT
             );

        CREATE TABLE #Adjust_Leading_Diff_Volumes
             (
                 Row_Num INT
                 , Service_Month DATE
                 , Forecast_Volume INT
                 , Current_Deal_Volume INT
                 , Diff_Volume INT
                 , Adjust_Volume INT
                 , New_Row_Num INT
             );

        DECLARE @Common_Uom_Id INT;

        DECLARE
            @Hedge_Type_Input VARCHAR(200)
            , @Hedge_Mode_Type VARCHAR(200)
            , @Hedge_Index VARCHAR(200);
        DECLARE
            @Deal_Ticket_Frequency VARCHAR(25)
            , @Trade_Action_Type VARCHAR(25)
            , @Hedge_Contract_Sites_Cnt INT;


        DECLARE @Tbl_tvp_Total_Forecast_Current_Deal_Hedged_ToDate_Volumes AS TABLE
              (
                  Deal_Month DATE NOT NULL
                  , Total_Hedge_Volume DECIMAL(28, 6) NOT NULL
                  , Total_Forecast_Volume DECIMAL(28, 6) NOT NULL
                  , Volume_Hedged_ToDate DECIMAL(28, 6) NULL
                  , UNIQUE
                    (
                        Deal_Month
                    )
              );

        SELECT
            @Deal_Ticket_Frequency = Code_Value
        FROM
            dbo.Code
        WHERE
            Code_Id = @Deal_Ticket_Frequency_Cd;

        SELECT
            @Trade_Action_Type = Code_Value
        FROM
            dbo.Code
        WHERE
            Code_Id = @Trade_Action_Type_Cd;

        SELECT
            @Hedge_Contract_Sites_Cnt = COUNT(1)
        FROM
            @Trade_tvp_Hedge_Contract_Sites;



        SELECT
            @Hedge_Type_Input = ENTITY_NAME
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_ID = @Hedge_Type;

        SELECT
            @Hedge_Mode_Type = ENTITY_NAME
        FROM
            dbo.ENTITY
        WHERE
            (   @Hedge_Mode_Type_Id IS NOT NULL
                AND ENTITY_ID = @Hedge_Mode_Type_Id)
            OR  (   @Hedge_Mode_Type_Id IS NULL
                    AND ENTITY_NAME = 'Index'
                    AND ENTITY_DESCRIPTION = 'HEDGE_MODE_TYPE');


        SELECT
            @Hedge_Index = pridx.ENTITY_NAME
        FROM
            dbo.ENTITY pridx
        WHERE
            pridx.ENTITY_ID = @Index_Id;

        INSERT INTO #DT_Hedged_Volumes
             (
                 Client_Hier_Id
                 , Account_Id
                 , Deal_Month
                 , Hedged_Volume
                 , Deal_Ticket_Id
                 , Contract_Id
             )
        SELECT
            dtv.Client_Hier_Id
            , dtv.Account_Id
            , dtv.Deal_Month
            , CASE WHEN frq.Code_Value = 'Daily' THEN MAX((CASE WHEN trdact.Code_Value = 'Buy' THEN dtv.Total_Volume
                                                               WHEN trdact.Code_Value = 'Sell' THEN -dtv.Total_Volume
                                                           END) * dtcuc.CONVERSION_FACTOR) * dd.DAYS_IN_MONTH_NUM
                  ELSE MAX((CASE WHEN trdact.Code_Value = 'Buy' THEN dtv.Total_Volume
                                WHEN trdact.Code_Value = 'Sell' THEN -dtv.Total_Volume
                            END) * dtcuc.CONVERSION_FACTOR)
              END AS Hedged_Volume
            , dtv.Deal_Ticket_Id
            , dtv.Contract_Id
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtv
                ON dtv.Deal_Ticket_Id = dt.Deal_Ticket_Id
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION dtcuc
                ON dtcuc.BASE_UNIT_ID = dtv.Uom_Type_Id
                   AND  dtcuc.CONVERTED_UNIT_ID = @Uom_Id
            INNER JOIN dbo.PRICE_INDEX pridx
                ON pridx.PRICE_INDEX_ID = dt.Price_Index_Id
            INNER JOIN dbo.ENTITY idx
                ON pridx.INDEX_ID = idx.ENTITY_ID
            INNER JOIN dbo.ENTITY hmt
                ON dt.Hedge_Mode_Type_Id = hmt.ENTITY_ID
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dtv.Client_Hier_Id = dtch.Client_Hier_Id
                   AND  dt.Deal_Ticket_Id = dtch.Deal_Ticket_Id
            INNER JOIN Trade.Trade_Price tp
                ON dtv.Trade_Price_Id = tp.Trade_Price_Id
            INNER JOIN dbo.Code trdact
                ON dt.Trade_Action_Type_Cd = trdact.Code_Id
            INNER JOIN dbo.Code frq
                ON dt.Deal_Ticket_Frequency_Cd = frq.Code_Id
            INNER JOIN meta.DATE_DIM dd
                ON dd.DATE_D = dtv.Deal_Month
        WHERE
            dt.Client_Id = @Client_Id
            AND dt.Commodity_Id = @Commodity_Id
            AND dtv.Deal_Month BETWEEN @Start_Dt
                               AND     @End_Dt
            AND (   (   @Hedge_Mode_Type = 'Index'
                        AND @Hedge_Index = 'Nymex'
                        AND hmt.ENTITY_NAME = 'Index')
                    OR  (   @Hedge_Mode_Type = 'Basis'
                            AND hmt.ENTITY_NAME = 'Basis')
                    OR  (   @Hedge_Mode_Type = 'Index'
                            AND @Hedge_Index <> 'Nymex'
                            AND hmt.ENTITY_NAME = 'Index'))
            AND tp.Trade_Price IS NOT NULL
            AND EXISTS (   SELECT
                                1
                           FROM
                                Core.Client_Hier ch
                                INNER JOIN Core.Client_Hier_Account cha
                                    ON ch.Client_Hier_Id = cha.Client_Hier_Id
                                INNER JOIN Core.Client_Hier_Account suppacc
                                    ON cha.Meter_Id = suppacc.Meter_Id
                                INNER JOIN @Trade_tvp_Hedge_Contract_Sites hcs
                                    ON suppacc.Supplier_Contract_ID = hcs.Contract_Id
                                       AND  ch.Site_Id = hcs.Site_Id
                           WHERE
                                cha.Account_Type = 'Utility'
                                AND suppacc.Account_Type = 'Supplier'
                                AND cha.Client_Hier_Id = dtv.Client_Hier_Id
                                AND cha.Account_Id = dtv.Account_Id
                                AND dd.DATE_D = dtv.Deal_Month)
        GROUP BY
            dtv.Client_Hier_Id
            , dtv.Account_Id
            , dtv.Deal_Month
            , dtv.Deal_Ticket_Id
            , frq.Code_Value
            , dd.DAYS_IN_MONTH_NUM
            , dtv.Contract_Id;

        INSERT INTO #Hedged_Volumes
             (
                 Client_Hier_Id
                 , Account_Id
                 , Deal_Month
                 , Hedged_Volume
             )
        SELECT
            Client_Hier_Id
            , Account_Id
            , Deal_Month
            , SUM(Hedged_Volume) Hedged_Volume
        FROM
            #DT_Hedged_Volumes
        GROUP BY
            Client_Hier_Id
            , Account_Id
            , Deal_Month;

        INSERT INTO #DT_Site_Hedged_Volumes
             (
                 Client_Hier_Id
                 , Account_Id
                 , Deal_Month
                 , Hedged_Volume
                 , Deal_Ticket_Id
                 , Contract_Id
             )
        SELECT
            dtv.Client_Hier_Id
            , dtv.Account_Id
            , dtv.Deal_Month
            , CASE WHEN frq.Code_Value = 'Daily' THEN MAX((CASE WHEN trdact.Code_Value = 'Buy' THEN dtv.Total_Volume
                                                               WHEN trdact.Code_Value = 'Sell' THEN -dtv.Total_Volume
                                                           END) * dtcuc.CONVERSION_FACTOR) * dd.DAYS_IN_MONTH_NUM
                  ELSE MAX((CASE WHEN trdact.Code_Value = 'Buy' THEN dtv.Total_Volume
                                WHEN trdact.Code_Value = 'Sell' THEN -dtv.Total_Volume
                            END) * dtcuc.CONVERSION_FACTOR)
              END AS Hedged_Volume
            , dtv.Deal_Ticket_Id
            , dtv.Contract_Id
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtv
                ON dtv.Deal_Ticket_Id = dt.Deal_Ticket_Id
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION dtcuc
                ON dtcuc.BASE_UNIT_ID = dtv.Uom_Type_Id
                   AND  dtcuc.CONVERTED_UNIT_ID = @Uom_Id
            INNER JOIN dbo.PRICE_INDEX pridx
                ON pridx.PRICE_INDEX_ID = dt.Price_Index_Id
            INNER JOIN dbo.ENTITY idx
                ON pridx.INDEX_ID = idx.ENTITY_ID
            INNER JOIN dbo.ENTITY hmt
                ON dt.Hedge_Mode_Type_Id = hmt.ENTITY_ID
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dtv.Client_Hier_Id = dtch.Client_Hier_Id
                   AND  dt.Deal_Ticket_Id = dtch.Deal_Ticket_Id
            INNER JOIN Trade.Trade_Price tp
                ON dtv.Trade_Price_Id = tp.Trade_Price_Id
            INNER JOIN dbo.Code trdact
                ON dt.Trade_Action_Type_Cd = trdact.Code_Id
            INNER JOIN dbo.Code frq
                ON dt.Deal_Ticket_Frequency_Cd = frq.Code_Id
            INNER JOIN meta.DATE_DIM dd
                ON dd.DATE_D = dtv.Deal_Month
        WHERE
            dt.Client_Id = @Client_Id
            AND dt.Commodity_Id = @Commodity_Id
            AND dtv.Deal_Month BETWEEN @Start_Dt
                               AND     @End_Dt
            AND (   (   @Hedge_Mode_Type = 'Index'
                        AND @Hedge_Index = 'Nymex'
                        AND hmt.ENTITY_NAME = 'Index')
                    OR  (   @Hedge_Mode_Type = 'Basis'
                            AND hmt.ENTITY_NAME = 'Basis')
                    OR  (   @Hedge_Mode_Type = 'Index'
                            AND @Hedge_Index <> 'Nymex'
                            AND hmt.ENTITY_NAME = 'Index'))
            AND tp.Trade_Price IS NOT NULL
            AND EXISTS (   SELECT
                                1
                           FROM
                                Core.Client_Hier ch
                                INNER JOIN @Trade_tvp_Hedge_Contract_Sites hcs
                                    ON ch.Site_Id = hcs.Site_Id
                           WHERE
                                ch.Client_Hier_Id = dtv.Client_Hier_Id)
        GROUP BY
            dtv.Client_Hier_Id
            , dtv.Account_Id
            , dtv.Deal_Month
            , dtv.Deal_Ticket_Id
            , frq.Code_Value
            , dd.DAYS_IN_MONTH_NUM
            , dtv.Contract_Id;

        INSERT INTO #Site_Hedged_Volumes
             (
                 Client_Hier_Id
                 , Deal_Month
                 , Hedged_Volume
             )
        SELECT
            shv.Client_Hier_Id
            , shv.Deal_Month
            , CASE WHEN @Deal_Ticket_Frequency = 'Monthly' THEN CAST(SUM(shv.Hedged_Volume) AS DECIMAL(28, 0))
                  WHEN @Deal_Ticket_Frequency = 'Daily' THEN
                      CAST((SUM(shv.Hedged_Volume) / dd.DAYS_IN_MONTH_NUM) AS DECIMAL(28, 0))
              END AS Hedged_Volume
        FROM
            #DT_Site_Hedged_Volumes shv
            INNER JOIN meta.DATE_DIM dd
                ON dd.DATE_D = shv.Deal_Month
        GROUP BY
            shv.Client_Hier_Id
            , shv.Deal_Month
            , dd.DAYS_IN_MONTH_NUM;


        ----------------------To get site own config      
        INSERT INTO #Volumes
             (
                 Client_Hier_Id
                 , Site_Id
                 , Site_name
                 , Account_Id
                 , Account_Number
                 , Contract_Vendor
                 , Service_Month
                 , Forecast_Volume
                 , Uom_Id
                 , Hedged_Volume
                 , Max_Hedge_Volume
                 , Max_Hedge_Percentage
                 , Do_Not_Hedge
                 , Site_Not_Managed
                 , Contract_Id
                 , Contract_Volume
                 , Client_Id
                 , Sitegroup_Id
                 , State_Id
             )
        SELECT
            ch.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
            , cha.Account_Id
            , cha.Account_Number
            , con.ED_CONTRACT_NUMBER + '(' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_START_DATE, 106), ' ', '-')
              + ' - ' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_END_DATE, 106), ' ', '-') + ')/'
              + suppacc.Account_Vendor_Name AS Contract_Vendor
            , dd.DATE_D
            , MAX(afv.Forecast_Volume * cuc.CONVERSION_FACTOR) AS Forecast_Volume
            , ISNULL(@Uom_Id, chob.RM_Forecast_UOM_Type_Id) AS Uom_Id
            , MAX(dtv1.Hedged_Volume) AS Hedged_Volume
            , MAX((chhc.Max_Hedge_Pct * afv.Forecast_Volume * cuc.CONVERSION_FACTOR) / 100) AS Max_Hedge_Volume
            , chhc.Max_Hedge_Pct AS Max_Hedge_Percentage
            , CASE WHEN MAX(afvsitecheck.Client_Hier_Id) IS NULL THEN 1
                  ELSE 0
              END
            , ch.Site_Not_Managed
            , con.CONTRACT_ID
            , CASE WHEN frq.ENTITY_NAME = 'Daily' THEN
                       MAX(ISNULL(cmv.VOLUME * mvcuc.CONVERSION_FACTOR * dd.DAYS_IN_MONTH_NUM, 0))
                  ELSE MAX(ISNULL(cmv.VOLUME * mvcuc.CONVERSION_FACTOR, 0))
              END AS Contract_Volume
            , ch.Client_Id
            , ch.Sitegroup_Id
            , ch.State_Id
        FROM
            Core.Client_Hier ch
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON ch.Client_Hier_Id = chob.Client_Hier_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY et
                ON et.ENTITY_ID = chhc.Hedge_Type_Id
            INNER JOIN Core.Client_Hier_Account cha
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN Core.Client_Hier_Account suppacc
                ON cha.Meter_Id = suppacc.Meter_Id
            INNER JOIN dbo.CONTRACT con
                ON con.CONTRACT_ID = suppacc.Supplier_Contract_ID
            CROSS JOIN meta.DATE_DIM dd
            LEFT JOIN Trade.RM_Account_Forecast_Volume afv
                ON cha.Client_Hier_Id = afv.Client_Hier_Id
                   AND  chob.Commodity_Id = afv.Commodity_Id
                   AND  cha.Account_Id = afv.Account_Id
                   AND  dd.DATE_D = afv.Service_Month
                   AND  afv.Service_Month BETWEEN chhc.Config_Start_Dt
                                          AND     chhc.Config_End_Dt
                   AND  afv.Service_Month BETWEEN @Start_Dt
                                          AND     @End_Dt
                   AND  afv.Service_Month BETWEEN con.CONTRACT_START_DATE
                                          AND     con.CONTRACT_END_DATE
            LEFT JOIN Trade.RM_Account_Forecast_Volume afvsitecheck
                ON cha.Client_Hier_Id = afvsitecheck.Client_Hier_Id
                   AND  chob.Commodity_Id = afvsitecheck.Commodity_Id
                   AND  dd.DATE_D = afvsitecheck.Service_Month
                   AND  afvsitecheck.Service_Month BETWEEN chhc.Config_Start_Dt
                                                   AND     chhc.Config_End_Dt
                   AND  afvsitecheck.Service_Month BETWEEN @Start_Dt
                                                   AND     @End_Dt
                   AND  afvsitecheck.Service_Month BETWEEN con.CONTRACT_START_DATE
                                                   AND     con.CONTRACT_END_DATE
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = afv.Uom_Id
                   AND  cuc.CONVERTED_UNIT_ID = ISNULL(@Uom_Id, chob.RM_Forecast_UOM_Type_Id)
            LEFT JOIN #Hedged_Volumes dtv1
                ON afv.Client_Hier_Id = dtv1.Client_Hier_Id
                   AND  afv.Account_Id = dtv1.Account_Id
                   AND  dd.DATE_D = dtv1.Deal_Month
            LEFT JOIN dbo.CONTRACT_METER_VOLUME cmv
                ON cha.Meter_Id = cmv.METER_ID
                   AND  suppacc.Supplier_Contract_ID = cmv.CONTRACT_ID
                   AND  CAST(cmv.MONTH_IDENTIFIER AS DATE) = dd.DATE_D
            LEFT JOIN dbo.ENTITY frq
                ON cmv.FREQUENCY_TYPE_ID = frq.ENTITY_ID
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION mvcuc
                ON mvcuc.BASE_UNIT_ID = cmv.UNIT_TYPE_ID
                   AND  mvcuc.CONVERTED_UNIT_ID = ISNULL(@Uom_Id, chob.RM_Forecast_UOM_Type_Id)
        WHERE
            ch.Site_Id > 0
            AND ch.Client_Id = @Client_Id
            AND chob.Commodity_Id = @Commodity_Id
            AND (   @Start_Dt BETWEEN chhc.Config_Start_Dt
                              AND     chhc.Config_End_Dt
                    OR  @End_Dt BETWEEN chhc.Config_Start_Dt
                                AND     chhc.Config_End_Dt
                    OR  chhc.Config_Start_Dt BETWEEN @Start_Dt
                                             AND     @End_Dt
                    OR  chhc.Config_End_Dt BETWEEN @Start_Dt
                                           AND     @End_Dt)
            AND (   (   @Hedge_Type_Input = 'Physical'
                        AND et.ENTITY_NAME IN ( 'Physical', 'Physical & Financial' ))
                    OR  (   @Hedge_Type_Input = 'Financial'
                            AND et.ENTITY_NAME IN ( 'Financial', 'Physical & Financial' )))
            AND EXISTS (   SELECT
                                1
                           FROM
                                @Trade_tvp_Hedge_Contract_Sites hcs
                           WHERE
                                suppacc.Supplier_Contract_ID = hcs.Contract_Id
                                AND ch.Site_Id = hcs.Site_Id)
            AND dd.DATE_D BETWEEN @Start_Dt
                          AND     @End_Dt
            AND cha.Account_Type = 'Utility'
            AND suppacc.Account_Type = 'Supplier'
            AND dd.DATE_D BETWEEN con.CONTRACT_START_DATE
                          AND     con.CONTRACT_END_DATE
            AND dd.DATE_D BETWEEN chhc.Config_Start_Dt
                          AND     chhc.Config_End_Dt
        GROUP BY
            ch.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
            , cha.Account_Id
            , cha.Account_Number
            , dd.DATE_D
            , ISNULL(@Uom_Id, chob.RM_Forecast_UOM_Type_Id)
            , con.ED_CONTRACT_NUMBER + '(' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_START_DATE, 106), ' ', '-')
              + ' - ' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_END_DATE, 106), ' ', '-') + ')/'
              + suppacc.Account_Vendor_Name
            , chhc.Max_Hedge_Pct
            , ch.Site_Not_Managed
            , con.CONTRACT_ID
            , frq.ENTITY_NAME
            , ch.Client_Id
            , ch.Sitegroup_Id
            , ch.State_Id;


        ----------------------To get default config        
        INSERT INTO #Volumes
             (
                 Client_Hier_Id
                 , Site_Id
                 , Site_name
                 , Account_Id
                 , Account_Number
                 , Contract_Vendor
                 , Service_Month
                 , Forecast_Volume
                 , Uom_Id
                 , Hedged_Volume
                 , Max_Hedge_Volume
                 , Max_Hedge_Percentage
                 , Do_Not_Hedge
                 , Site_Not_Managed
                 , Contract_Id
                 , Contract_Volume
                 , Client_Id
                 , Sitegroup_Id
                 , State_Id
             )
        SELECT
            ch.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
            , cha.Account_Id
            , cha.Account_Number
            , con.ED_CONTRACT_NUMBER + '(' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_START_DATE, 106), ' ', '-')
              + ' - ' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_END_DATE, 106), ' ', '-') + ')/'
              + suppacc.Account_Vendor_Name AS Contract_Vendor
            , dd.DATE_D
            , MAX(afv.Forecast_Volume * cuc.CONVERSION_FACTOR) AS Forecast_Volume
            , ISNULL(@Uom_Id, chob.RM_Forecast_UOM_Type_Id) AS Uom_Id
            , MAX(dtv1.Hedged_Volume) AS Hedged_Volume
            , MAX((chhc.Max_Hedge_Pct * afv.Forecast_Volume * cuc.CONVERSION_FACTOR) / 100) AS Max_Hedge_Volume
            , chhc.Max_Hedge_Pct AS Max_Hedge_Percentage
            , CASE WHEN MAX(afvsitecheck.Client_Hier_Id) IS NULL THEN 1
                  ELSE 0
              END
            , ch.Site_Not_Managed
            , con.CONTRACT_ID
            , CASE WHEN frq.ENTITY_NAME = 'Daily' THEN
                       MAX(ISNULL(cmv.VOLUME * mvcuc.CONVERSION_FACTOR * dd.DAYS_IN_MONTH_NUM, 0))
                  ELSE MAX(ISNULL(cmv.VOLUME * mvcuc.CONVERSION_FACTOR, 0))
              END AS Contract_Volume
            , ch.Client_Id
            , ch.Sitegroup_Id
            , ch.State_Id
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier chclient
                ON ch.Client_Id = chclient.Client_Id
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON chclient.Client_Hier_Id = chob.Client_Hier_Id
                   AND  ch.Country_Id = chob.Country_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY et
                ON et.ENTITY_ID = chhc.Hedge_Type_Id
            INNER JOIN Core.Client_Hier_Account cha
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN Core.Client_Hier_Account suppacc
                ON cha.Meter_Id = suppacc.Meter_Id
            INNER JOIN dbo.CONTRACT con
                ON con.CONTRACT_ID = suppacc.Supplier_Contract_ID
            CROSS JOIN meta.DATE_DIM dd
            LEFT JOIN Trade.RM_Account_Forecast_Volume afv
                ON cha.Client_Hier_Id = afv.Client_Hier_Id
                   AND  chob.Commodity_Id = afv.Commodity_Id
                   AND  cha.Account_Id = afv.Account_Id
                   AND  dd.DATE_D = afv.Service_Month
                   AND  afv.Service_Month BETWEEN chhc.Config_Start_Dt
                                          AND     chhc.Config_End_Dt
                   AND  afv.Service_Month BETWEEN @Start_Dt
                                          AND     @End_Dt
                   AND  afv.Service_Month BETWEEN con.CONTRACT_START_DATE
                                          AND     con.CONTRACT_END_DATE
            LEFT JOIN Trade.RM_Account_Forecast_Volume afvsitecheck
                ON cha.Client_Hier_Id = afvsitecheck.Client_Hier_Id
                   AND  chob.Commodity_Id = afvsitecheck.Commodity_Id
                   AND  dd.DATE_D = afvsitecheck.Service_Month
                   AND  afvsitecheck.Service_Month BETWEEN chhc.Config_Start_Dt
                                                   AND     chhc.Config_End_Dt
                   AND  afvsitecheck.Service_Month BETWEEN @Start_Dt
                                                   AND     @End_Dt
                   AND  afvsitecheck.Service_Month BETWEEN con.CONTRACT_START_DATE
                                                   AND     con.CONTRACT_END_DATE
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = afv.Uom_Id
                   AND  cuc.CONVERTED_UNIT_ID = ISNULL(@Uom_Id, chob.RM_Forecast_UOM_Type_Id)
            LEFT JOIN #Hedged_Volumes dtv1
                ON afv.Client_Hier_Id = dtv1.Client_Hier_Id
                   AND  dd.DATE_D = dtv1.Deal_Month
            LEFT JOIN dbo.CONTRACT_METER_VOLUME cmv
                ON cha.Meter_Id = cmv.METER_ID
                   AND  suppacc.Supplier_Contract_ID = cmv.CONTRACT_ID
                   AND  CAST(cmv.MONTH_IDENTIFIER AS DATE) = dd.DATE_D
            LEFT JOIN dbo.ENTITY frq
                ON cmv.FREQUENCY_TYPE_ID = frq.ENTITY_ID
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION mvcuc
                ON mvcuc.BASE_UNIT_ID = cmv.UNIT_TYPE_ID
                   AND  mvcuc.CONVERTED_UNIT_ID = ISNULL(@Uom_Id, chob.RM_Forecast_UOM_Type_Id)
        WHERE
            ch.Site_Id > 0
            AND ch.Client_Id = @Client_Id
            AND chclient.Sitegroup_Id = 0
            AND chob.Commodity_Id = @Commodity_Id
            AND (   @Start_Dt BETWEEN chhc.Config_Start_Dt
                              AND     chhc.Config_End_Dt
                    OR  @End_Dt BETWEEN chhc.Config_Start_Dt
                                AND     chhc.Config_End_Dt
                    OR  chhc.Config_Start_Dt BETWEEN @Start_Dt
                                             AND     @End_Dt
                    OR  chhc.Config_End_Dt BETWEEN @Start_Dt
                                           AND     @End_Dt)
            AND (   (   @Hedge_Type_Input = 'Physical'
                        AND et.ENTITY_NAME IN ( 'Physical', 'Physical & Financial' ))
                    OR  (   @Hedge_Type_Input = 'Financial'
                            AND et.ENTITY_NAME IN ( 'Financial', 'Physical & Financial' )))
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.RM_Client_Hier_Onboard siteob1
                               WHERE
                                    siteob1.Client_Hier_Id = ch.Client_Hier_Id
                                    AND siteob1.Commodity_Id = chob.Commodity_Id)
            AND EXISTS (   SELECT
                                1
                           FROM
                                @Trade_tvp_Hedge_Contract_Sites hcs
                           WHERE
                                suppacc.Supplier_Contract_ID = hcs.Contract_Id
                                AND ch.Site_Id = hcs.Site_Id)
            AND dd.DATE_D BETWEEN @Start_Dt
                          AND     @End_Dt
            AND cha.Account_Type = 'Utility'
            AND suppacc.Account_Type = 'Supplier'
            AND dd.DATE_D BETWEEN con.CONTRACT_START_DATE
                          AND     con.CONTRACT_END_DATE
            AND dd.DATE_D BETWEEN chhc.Config_Start_Dt
                          AND     chhc.Config_End_Dt
        GROUP BY
            ch.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
            , cha.Account_Id
            , cha.Account_Number
            , dd.DATE_D
            , ISNULL(@Uom_Id, chob.RM_Forecast_UOM_Type_Id)
            , con.ED_CONTRACT_NUMBER + '(' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_START_DATE, 106), ' ', '-')
              + ' - ' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_END_DATE, 106), ' ', '-') + ')/'
              + suppacc.Account_Vendor_Name
            , chhc.Max_Hedge_Pct
            , ch.Site_Not_Managed
            , con.CONTRACT_ID
            , frq.ENTITY_NAME
            , ch.Client_Id
            , ch.Sitegroup_Id
            , ch.State_Id;




        SELECT
            vol.Client_Hier_Id
            , vol.Site_Id
            , vol.Account_Id
            , vol.Service_Month
            , COUNT(DISTINCT vol.Contract_Id) AS Contracts_Cnt
        INTO
            #Volumes_Contracts_Cnt
        FROM
            #Volumes vol
        GROUP BY
            vol.Client_Hier_Id
            , vol.Site_Id
            , vol.Account_Id
            , vol.Service_Month;



        INSERT INTO @Tbl_tvp_Total_Forecast_Current_Deal_Hedged_ToDate_Volumes
             (
                 Deal_Month
                 , Total_Hedge_Volume
                 , Total_Forecast_Volume
                 , Volume_Hedged_ToDate
             )
        SELECT
            Deal_Month
            , Total_Hedge_Volume
            , Total_Forecast_Volume
            , Volume_Hedged_ToDate
        FROM
            @Trade_tvp_Total_Forecast_Current_Deal_Hedged_ToDate_Volumes;
        WITH Cte_tfv
        AS (
               SELECT
                    tvp.Deal_Month
                    , SUM(vol.Forecast_Volume / vcc.Contracts_Cnt) AS Total_Forecast_Volume
               FROM
                    #Volumes vol
                    INNER JOIN @Tbl_tvp_Total_Forecast_Current_Deal_Hedged_ToDate_Volumes tvp
                        ON tvp.Deal_Month = vol.Service_Month
                    INNER JOIN #Volumes_Contracts_Cnt vcc
                        ON vol.Client_Hier_Id = vcc.Client_Hier_Id
                           AND  vol.Site_Id = vol.Site_Id
                           AND  vol.Account_Id = vcc.Account_Id
                           AND  vol.Service_Month = vcc.Service_Month
               GROUP BY
                   tvp.Deal_Month
           )
        UPDATE
            tvp
        SET
            tvp.Total_Forecast_Volume = vol.Total_Forecast_Volume
        FROM
            Cte_tfv vol
            INNER JOIN @Tbl_tvp_Total_Forecast_Current_Deal_Hedged_ToDate_Volumes tvp
                ON tvp.Deal_Month = vol.Deal_Month
        WHERE
            tvp.Total_Forecast_Volume = -1;


        SELECT
            vol.Client_Hier_Id
            , vol.Site_Id
            , vol.Site_name
            , vol.Account_Id
            , vol.Account_Number
            , vol.Contract_Vendor
            , vol.Service_Month
            , CASE WHEN @Deal_Ticket_Frequency = 'Monthly' THEN
                       CAST(ISNULL((vol.Forecast_Volume / vcc.Contracts_Cnt), 0) AS DECIMAL(28, 0))
                  WHEN @Deal_Ticket_Frequency = 'Daily' THEN
                      CAST(ISNULL(((vol.Forecast_Volume / vcc.Contracts_Cnt) / dd.DAYS_IN_MONTH_NUM), 0) AS DECIMAL(28, 0))
              END AS Forecast_Volume
            , vol.Uom_Id
            , CASE WHEN @Deal_Ticket_Frequency = 'Monthly' THEN CAST(ISNULL(vol.Hedged_Volume, 0) AS DECIMAL(28, 0))
                  WHEN @Deal_Ticket_Frequency = 'Daily' THEN
                      CAST(ISNULL(vol.Hedged_Volume / dd.DAYS_IN_MONTH_NUM, 0) AS DECIMAL(28, 0))
              END AS Hedged_Volume
            , CASE WHEN @Deal_Ticket_Frequency = 'Monthly' THEN
                       CAST(ISNULL(((vol.Hedged_Volume / NULLIF(vol.Forecast_Volume, 0)) * 100), 0) AS DECIMAL(28, 0))
                  WHEN @Deal_Ticket_Frequency = 'Daily' THEN
                      CAST(ISNULL(
                               ((vol.Hedged_Volume / NULLIF(vol.Forecast_Volume, 0)) * 100) / dd.DAYS_IN_MONTH_NUM, 0) AS DECIMAL(28, 0))
              END AS Percent_RM_Forecast_Hedged
            , CASE WHEN @Deal_Ticket_Frequency = 'Monthly' THEN
                       CAST(ISNULL((vol.Max_Hedge_Volume / vcc.Contracts_Cnt), 0) AS DECIMAL(28, 0))
                  WHEN @Deal_Ticket_Frequency = 'Daily' THEN
                      CAST(ISNULL(((vol.Max_Hedge_Volume / vcc.Contracts_Cnt) / dd.DAYS_IN_MONTH_NUM), 0) AS DECIMAL(28, 0))
              END AS Max_Hedge_Volume
            , ISNULL((vol.Max_Hedge_Percentage), 0) AS Max_Hedge_Percentage
            , Do_Not_Hedge
            , vol.Site_Not_Managed
            , vol.Contract_Id
            , DENSE_RANK() OVER (ORDER BY
                                     vol.Site_name
                                     , vol.Account_Id
                                     , vol.Contract_Id) Row_Num
            , CASE WHEN @Deal_Ticket_Frequency = 'Monthly' THEN CAST(vol.Contract_Volume AS DECIMAL(28, 0))
                  WHEN @Deal_Ticket_Frequency = 'Daily' THEN
                      CAST(vol.Contract_Volume / dd.DAYS_IN_MONTH_NUM AS DECIMAL(28, 0))
              END AS Contract_Volume
            , CASE WHEN @Trade_Action_Type = 'Buy' THEN
                       CASE WHEN @Deal_Ticket_Frequency = 'Monthly' THEN
                                CAST(ISNULL(
                                         (((vol.Forecast_Volume) / vcc.Contracts_Cnt)
                                          / NULLIF(tvp.Total_Forecast_Volume, 0)) * tvp.Total_Hedge_Volume, 0) AS DECIMAL(28, 0))
                           WHEN @Deal_Ticket_Frequency = 'Daily' THEN
                               CAST(ISNULL(
                                        ((((vol.Forecast_Volume) / vcc.Contracts_Cnt)
                                          / NULLIF(tvp.Total_Forecast_Volume, 0)) * tvp.Total_Hedge_Volume)
                                        / dd.DAYS_IN_MONTH_NUM, 0) AS DECIMAL(28, 0))
                       END
                  WHEN @Trade_Action_Type = 'Sell' THEN
                      CASE WHEN @Deal_Ticket_Frequency = 'Monthly' THEN
                               CAST(ISNULL(
                                        ((vol.Hedged_Volume / vcc.Contracts_Cnt) / NULLIF(tvp.Total_Forecast_Volume, 0))
                                        * tvp.Total_Hedge_Volume, 0) AS DECIMAL(28, 0))
                          WHEN @Deal_Ticket_Frequency = 'Daily' THEN
                              CAST(ISNULL(
                                       (((vol.Hedged_Volume / vcc.Contracts_Cnt) / NULLIF(tvp.Total_Forecast_Volume, 0))
                                        * tvp.Total_Hedge_Volume) / dd.DAYS_IN_MONTH_NUM, 0) AS DECIMAL(28, 0))
                      END
              END AS Current_Deal_Volume
            , 0 AS Volume_Adjusted
            , vol.Client_Id
            , vol.Sitegroup_Id
            , vol.State_Id
        INTO
            #Final_Volumes
        FROM
            #Volumes vol
            LEFT JOIN @Tbl_tvp_Total_Forecast_Current_Deal_Hedged_ToDate_Volumes tvp
                ON tvp.Deal_Month = vol.Service_Month
            INNER JOIN #Volumes_Contracts_Cnt vcc
                ON vol.Client_Hier_Id = vcc.Client_Hier_Id
                   AND  vol.Site_Id = vol.Site_Id
                   AND  vol.Account_Id = vcc.Account_Id
                   AND  vol.Service_Month = vcc.Service_Month
            LEFT JOIN meta.DATE_DIM dd
                ON dd.DATE_D = vol.Service_Month;




        SELECT  @Total_Cnt = MAX(fv.Row_Num)FROM    #Final_Volumes fv;

        --------Site level adjustment
        DELETE  FROM #Allotted_Monthly_Volumes;
        DELETE  FROM #Adjust_Lagging_Diff_Volumes;

        INSERT INTO #Allotted_Monthly_Volumes
             (
                 Service_Month
                 , Allotted_Hedge_Volume
                 , Total_Hedged_Volume
             )
        SELECT
            vol.Service_Month
            , SUM(vol.Current_Deal_Volume) AS Current_Deal_Volume
            , tvp.Total_Hedge_Volume
        FROM
            #Final_Volumes vol
            LEFT JOIN @Tbl_tvp_Total_Forecast_Current_Deal_Hedged_ToDate_Volumes tvp
                ON tvp.Deal_Month = vol.Service_Month
        GROUP BY
            vol.Service_Month
            , tvp.Total_Hedge_Volume;

        INSERT INTO #Adjust_Lagging_Diff_Volumes
             (
                 Row_Num
                 , Service_Month
                 , Forecast_Volume
                 , Current_Deal_Volume
                 , Diff_Volume
                 , Adjust_Volume
                 , New_Row_Num
             )
        SELECT
            fv.Row_Num
            , fv.Service_Month
            --, CAST(rchfv.Forecast_Volume * cuc.CONVERSION_FACTOR AS DECIMAL(28, 0))
            , fv.Forecast_Volume
            , fv.Current_Deal_Volume
            --, CAST(rchfv.Forecast_Volume * cuc.CONVERSION_FACTOR AS DECIMAL(28, 0))
            --  - ISNULL(shv.Hedged_Volume, fv.Hedged_Volume) - fv.Current_Deal_Volume
            , fv.Forecast_Volume - shv.Hedged_Volume - fv.Current_Deal_Volume
            , mtvol.Total_Hedged_Volume - mtvol.Allotted_Hedge_Volume
            , ROW_NUMBER() OVER (PARTITION BY
                                     fv.Service_Month
                                 ORDER BY
                                     fv.Site_name
                                     , fv.Account_Id)
        FROM
            #Final_Volumes fv
            INNER JOIN #Allotted_Monthly_Volumes mtvol
                ON fv.Service_Month = mtvol.Service_Month
            INNER JOIN Trade.RM_Client_Hier_Forecast_Volume rchfv
                ON rchfv.Client_Hier_Id = fv.Client_Hier_Id
                   AND  rchfv.Service_Month = fv.Service_Month
                   AND  rchfv.Commodity_Id = @Commodity_Id
            INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = rchfv.Uom_Id
                   AND  cuc.CONVERTED_UNIT_ID = @Uom_Id
            LEFT JOIN #Site_Hedged_Volumes shv
                ON shv.Client_Hier_Id = fv.Client_Hier_Id
                   AND  shv.Deal_Month = fv.Service_Month
        WHERE
            fv.Forecast_Volume - shv.Hedged_Volume - fv.Current_Deal_Volume > 0
            AND mtvol.Total_Hedged_Volume - mtvol.Allotted_Hedge_Volume > 0
            AND fv.Current_Deal_Volume > 0;


        WITH Cte_Lagging_Site
        AS (
               SELECT
                    Forecast_Volume
                    , Current_Deal_Volume
                    , Current_Deal_Volume + CASE WHEN Adjust_Volume >= Diff_Volume THEN Diff_Volume
                                                WHEN Adjust_Volume < Diff_Volume THEN Adjust_Volume
                                                ELSE 0
                                            END AS New_Current_Deal_Volume
                    , Diff_Volume AS New
                    , Row_Num
                    , Adjust_Volume - Diff_Volume AS Rmd
                    , New_Row_Num
                    , Service_Month
               FROM
                    #Adjust_Lagging_Diff_Volumes t
               WHERE
                    New_Row_Num = 1
               UNION ALL
               SELECT
                    t.Forecast_Volume
                    , t.Current_Deal_Volume
                    , t.Current_Deal_Volume + (CASE WHEN t1.Rmd > 0
                                                         AND t1.Rmd >= t.Diff_Volume THEN t.Diff_Volume
                                                   WHEN t1.Rmd > 0
                                                        AND t1.Rmd < t.Diff_Volume THEN t1.Rmd
                                                   ELSE 0
                                               END) AS New_Current_Deal_Volume
                    , (CASE WHEN t1.Rmd > 0
                                 AND t1.Rmd >= t.Diff_Volume THEN t.Diff_Volume
                           WHEN t1.Rmd > 0
                                AND t1.Rmd < t.Diff_Volume THEN t1.Rmd
                           ELSE 0
                       END) AS New
                    , t.Row_Num
                    , t1.Rmd - t.Diff_Volume Rmd
                    , t.New_Row_Num
                    , t.Service_Month
               FROM
                    #Adjust_Lagging_Diff_Volumes t
                    INNER JOIN Cte_Lagging_Site t1
                        ON t.New_Row_Num = t1.New_Row_Num + 1
                           AND  t.Service_Month = t1.Service_Month
               WHERE
                    t1.Rmd > 0
           )
        UPDATE
            fv
        SET
            fv.Current_Deal_Volume = ct.New_Current_Deal_Volume
            , fv.Volume_Adjusted = 1
        FROM
            #Final_Volumes fv
            INNER JOIN Cte_Lagging_Site ct
                ON fv.Row_Num = ct.Row_Num
                   AND  fv.Service_Month = ct.Service_Month;

        --------Site level adjustment
        DELETE  FROM #Allotted_Monthly_Volumes;
        DELETE  FROM #Adjust_Lagging_Diff_Volumes;

        INSERT INTO #Allotted_Monthly_Volumes
             (
                 Service_Month
                 , Allotted_Hedge_Volume
                 , Total_Hedged_Volume
             )
        SELECT
            vol.Service_Month
            , SUM(vol.Current_Deal_Volume) AS Current_Deal_Volume
            , tvp.Total_Hedge_Volume
        FROM
            #Final_Volumes vol
            LEFT JOIN @Tbl_tvp_Total_Forecast_Current_Deal_Hedged_ToDate_Volumes tvp
                ON tvp.Deal_Month = vol.Service_Month
        GROUP BY
            vol.Service_Month
            , tvp.Total_Hedge_Volume;

        INSERT INTO #Adjust_Lagging_Diff_Volumes
             (
                 Row_Num
                 , Service_Month
                 , Forecast_Volume
                 , Current_Deal_Volume
                 , Diff_Volume
                 , Adjust_Volume
                 , New_Row_Num
             )
        SELECT
            fv.Row_Num
            , fv.Service_Month
            , fv.Forecast_Volume
            , fv.Current_Deal_Volume
            , fv.Forecast_Volume - fv.Hedged_Volume - fv.Current_Deal_Volume
            , mtvol.Total_Hedged_Volume - mtvol.Allotted_Hedge_Volume
            , ROW_NUMBER() OVER (PARTITION BY
                                     fv.Service_Month
                                 ORDER BY
                                     fv.Site_name
                                     , fv.Account_Id)
        FROM
            #Final_Volumes fv
            INNER JOIN #Allotted_Monthly_Volumes mtvol
                ON fv.Service_Month = mtvol.Service_Month
            INNER JOIN Trade.RM_Client_Hier_Forecast_Volume rchfv
                ON rchfv.Client_Hier_Id = fv.Client_Hier_Id
                   AND  rchfv.Service_Month = fv.Service_Month
                   AND  rchfv.Commodity_Id = @Commodity_Id
            INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = rchfv.Uom_Id
                   AND  cuc.CONVERTED_UNIT_ID = @Uom_Id
            LEFT JOIN #Site_Hedged_Volumes shv
                ON shv.Client_Hier_Id = fv.Client_Hier_Id
                   AND  shv.Deal_Month = fv.Service_Month
        WHERE
            (shv.Hedged_Volume + fv.Current_Deal_Volume) < CAST(rchfv.Forecast_Volume * cuc.CONVERSION_FACTOR AS DECIMAL(28, 0))
            AND mtvol.Total_Hedged_Volume - mtvol.Allotted_Hedge_Volume > 0
            AND fv.Current_Deal_Volume > 0;


        WITH Cte_Lagging_Site
        AS (
               SELECT
                    Forecast_Volume
                    , Current_Deal_Volume
                    , Current_Deal_Volume + CASE WHEN Adjust_Volume >= Diff_Volume THEN Diff_Volume
                                                WHEN Adjust_Volume < Diff_Volume THEN Adjust_Volume
                                                ELSE 0
                                            END AS New_Current_Deal_Volume
                    , Diff_Volume AS New
                    , Row_Num
                    , Adjust_Volume - Diff_Volume AS Rmd
                    , New_Row_Num
                    , Service_Month
               FROM
                    #Adjust_Lagging_Diff_Volumes t
               WHERE
                    New_Row_Num = 1
               UNION ALL
               SELECT
                    t.Forecast_Volume
                    , t.Current_Deal_Volume
                    , t.Current_Deal_Volume + (CASE WHEN t1.Rmd > 0
                                                         AND t1.Rmd >= t.Diff_Volume THEN t.Diff_Volume
                                                   WHEN t1.Rmd > 0
                                                        AND t1.Rmd < t.Diff_Volume THEN t1.Rmd
                                                   ELSE 0
                                               END) AS New_Current_Deal_Volume
                    , (CASE WHEN t1.Rmd > 0
                                 AND t1.Rmd >= t.Diff_Volume THEN t.Diff_Volume
                           WHEN t1.Rmd > 0
                                AND t1.Rmd < t.Diff_Volume THEN t1.Rmd
                           ELSE 0
                       END) AS New
                    , t.Row_Num
                    , t1.Rmd - t.Diff_Volume Rmd
                    , t.New_Row_Num
                    , t.Service_Month
               FROM
                    #Adjust_Lagging_Diff_Volumes t
                    INNER JOIN Cte_Lagging_Site t1
                        ON t.New_Row_Num = t1.New_Row_Num + 1
                           AND  t.Service_Month = t1.Service_Month
               WHERE
                    t1.Rmd > 0
           )
        UPDATE
            fv
        SET
            fv.Current_Deal_Volume = ct.New_Current_Deal_Volume
            , fv.Volume_Adjusted = 1
        FROM
            #Final_Volumes fv
            INNER JOIN Cte_Lagging_Site ct
                ON fv.Row_Num = ct.Row_Num
                   AND  fv.Service_Month = ct.Service_Month;

        ------------------------------------------------------
        INSERT INTO #Adjust_Leading_Diff_Volumes
             (
                 Row_Num
                 , Service_Month
                 , Forecast_Volume
                 , Current_Deal_Volume
                 , Diff_Volume
                 , Adjust_Volume
                 , New_Row_Num
             )
        SELECT
            fv.Row_Num
            , fv.Service_Month
            , fv.Forecast_Volume
            , fv.Current_Deal_Volume
            , fv.Forecast_Volume - fv.Current_Deal_Volume
            , mtvol.Allotted_Hedge_Volume - mtvol.Total_Hedged_Volume
            , ROW_NUMBER() OVER (PARTITION BY
                                     fv.Service_Month
                                 ORDER BY
                                     fv.Site_name
                                     , fv.Account_Id)
        FROM
            #Final_Volumes fv
            INNER JOIN #Allotted_Monthly_Volumes mtvol
                ON fv.Service_Month = mtvol.Service_Month
        WHERE
            mtvol.Allotted_Hedge_Volume - mtvol.Total_Hedged_Volume > 0
            AND fv.Current_Deal_Volume > 0;

        WITH Cte_Leading
        AS (
               SELECT
                    Forecast_Volume
                    , Current_Deal_Volume
                    , Current_Deal_Volume - CAST((CASE WHEN Adjust_Volume >= Diff_Volume
                                                            AND Diff_Volume <= Current_Deal_Volume THEN Diff_Volume
                                                      WHEN Adjust_Volume < Diff_Volume
                                                           AND  Adjust_Volume <= Current_Deal_Volume THEN Adjust_Volume
                                                      ELSE Current_Deal_Volume
                                                  END) AS INT) AS New_Current_Deal_Volume
                    , CAST(CASE WHEN Adjust_Volume >= Diff_Volume THEN Diff_Volume
                               WHEN Adjust_Volume < Diff_Volume THEN Adjust_Volume
                               ELSE 0
                           END AS INT) AS New
                    , Row_Num
                    --,Adjust_Volume - Diff_Volume AS Rmd    
                    , CAST((Adjust_Volume - CASE WHEN Diff_Volume <= Current_Deal_Volume THEN Diff_Volume
                                                ELSE Current_Deal_Volume
                                            END) AS INT) AS Rmd
                    , New_Row_Num
                    , Service_Month
               FROM
                    #Adjust_Leading_Diff_Volumes t
               WHERE
                    New_Row_Num = 1
               UNION ALL
               SELECT
                    t.Forecast_Volume
                    , t.Current_Deal_Volume
                    , t.Current_Deal_Volume - (CASE WHEN t1.Rmd > 0
                                                         AND t1.Rmd <= t.Diff_Volume
                                                         AND t1.Rmd <= t.Current_Deal_Volume THEN t1.Rmd
                                                   WHEN t1.Rmd > 0
                                                        AND t1.Rmd > t.Diff_Volume
                                                        AND t.Diff_Volume <= t.Current_Deal_Volume THEN t.Diff_Volume
                                                   ELSE t.Current_Deal_Volume
                                               END) AS New_Current_Deal_Volume
                    , (CASE WHEN t1.Rmd > 0
                                 AND t1.Rmd <= t.Diff_Volume THEN t1.Rmd
                           WHEN t1.Rmd > 0
                                AND t1.Rmd > t.Diff_Volume THEN t.Diff_Volume
                           ELSE 0
                       END) AS New
                    , t.Row_Num
                    , t1.Rmd - CASE WHEN t.Diff_Volume <= t.Current_Deal_Volume THEN t.Diff_Volume
                                   ELSE t.Current_Deal_Volume
                               END AS Rmd
                    , t.New_Row_Num
                    , t.Service_Month
               FROM
                    #Adjust_Leading_Diff_Volumes t
                    INNER JOIN Cte_Leading t1
                        ON t.New_Row_Num = t1.New_Row_Num + 1
                           AND  t.Service_Month = t1.Service_Month
               WHERE
                    t1.Rmd > 0
           )
        UPDATE
            fv
        SET
            fv.Current_Deal_Volume = ct.New_Current_Deal_Volume
            , fv.Volume_Adjusted = 1
        FROM
            #Final_Volumes fv
            INNER JOIN Cte_Leading ct
                ON fv.Row_Num = ct.Row_Num
                   AND  fv.Service_Month = ct.Service_Month
        OPTION (MAXRECURSION 32767);


        WITH Cte_Volumes
        AS (
               SELECT
                    fv.Client_Hier_Id
                    , fv.Site_Id
                    , fv.Site_name
                    , fv.Service_Month
                    , SUM(ISNULL(fv.Current_Deal_Volume, 0)) AS Current_Deal_Volume
                    , SUM(ISNULL(fv.Forecast_Volume, 0)) AS Forecast_Volume
                    , fv.Uom_Id
                    , MAX(ISNULL(shv.Hedged_Volume, 0)) AS Hedged_Volume
                    , fv.Do_Not_Hedge
                    , fv.Site_Not_Managed
                    , SUM(ISNULL(fv.Volume_Adjusted, 0)) AS Volume_Adjusted
                    , fv.Client_Id
                    , fv.Sitegroup_Id
                    , fv.State_Id
                    , MAX(ISNULL(shv.Hedged_Volume, 0)) + SUM(ISNULL(fv.Current_Deal_Volume, 0)) AS New_Total_Volume
                    , CAST(ISNULL(
                               ((MAX(ISNULL(shv.Hedged_Volume, 0)) + SUM(ISNULL(fv.Current_Deal_Volume, 0)))
                                / NULLIF(SUM(ISNULL(fv.Forecast_Volume, 0)), 0)) * 100, 0) AS DECIMAL(28, 0)) AS New_TotalPercent_Of_RM_Forecast
                    , DENSE_RANK() OVER (ORDER BY
                                             fv.Site_name) Row_Num
               FROM
                    #Final_Volumes fv
                    LEFT JOIN #Site_Hedged_Volumes shv
                        ON shv.Client_Hier_Id = fv.Client_Hier_Id
                           AND  fv.Service_Month = shv.Deal_Month
               GROUP BY
                   fv.Client_Hier_Id
                   , fv.Site_Id
                   , fv.Site_name
                   , fv.Service_Month
                   , fv.Uom_Id
                   , fv.Do_Not_Hedge
                   , fv.Site_Not_Managed
                   , fv.Client_Id
                   , fv.Sitegroup_Id
                   , fv.State_Id
           )
        SELECT
            fv.Client_Hier_Id
            , fv.Site_Id
            , fv.Site_name
            , fv.Service_Month
            , fv.Current_Deal_Volume
            , fv.Forecast_Volume
            , fv.Uom_Id
            , fv.Hedged_Volume
            , fv.Do_Not_Hedge
            , fv.Site_Not_Managed
            , tc.Total_Cnt
            , fv.Volume_Adjusted
            , fv.Client_Id
            , fv.Sitegroup_Id
            , fv.State_Id
            , fv.New_Total_Volume
            , fv.New_TotalPercent_Of_RM_Forecast
            , fv.Row_Num
            , CASE WHEN @Deal_Ticket_Frequency = 'Monthly' THEN
                       CAST((rchfv.Forecast_Volume * cuc.CONVERSION_FACTOR) AS DECIMAL(28, 0))
                  WHEN @Deal_Ticket_Frequency = 'Daily' THEN
                      CAST(((rchfv.Forecast_Volume * cuc.CONVERSION_FACTOR) / dd.DAYS_IN_MONTH_NUM) AS DECIMAL(28, 0))
              END AS Site_Forecast_Volume
        FROM
            Cte_Volumes fv
            CROSS JOIN
            (   SELECT
                    MAX(Row_Num) AS Total_Cnt
                FROM
                    Cte_Volumes) tc
            LEFT JOIN Trade.RM_Client_Hier_Forecast_Volume rchfv
                ON rchfv.Client_Hier_Id = fv.Client_Hier_Id
                   AND  rchfv.Commodity_Id = @Commodity_Id
                   AND  rchfv.Service_Month = fv.Service_Month
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = rchfv.Uom_Id
                   AND  cuc.CONVERTED_UNIT_ID = ISNULL(@Uom_Id, fv.Uom_Id)
            INNER JOIN meta.DATE_DIM dd
                ON dd.DATE_D = fv.Service_Month
        WHERE
            fv.Row_Num BETWEEN @Start_Index
                       AND     @End_Index
        ORDER BY
            fv.Row_Num;


        DROP TABLE #Volumes;
        DROP TABLE #Allotted_Monthly_Volumes;
        DROP TABLE #Final_Volumes;
        DROP TABLE #Adjust_Leading_Diff_Volumes;
        DROP TABLE #Adjust_Lagging_Diff_Volumes;
        DROP TABLE #Volumes_Contracts_Cnt;
        DROP TABLE #Hedged_Volumes;
        DROP TABLE #DT_Hedged_Volumes;
        DROP TABLE #DT_Site_Hedged_Volumes;
        DROP TABLE #Site_Hedged_Volumes;

    END;
GO
GRANT EXECUTE ON  [dbo].[RM_Account_Site_Hedged_Volumes_Sel_For_Deal_Ticket] TO [CBMSApplication]
GO
