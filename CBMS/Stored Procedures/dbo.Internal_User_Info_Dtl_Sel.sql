SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  

 NAME: dbo.Internal_User_Info_Dtl_Sel  
 
 DESCRIPTION:   
		For invoice collect owner drop down to need to show only internal user.

 INPUT PARAMETERS:  
 Name				DataType		Default Description  
------------------------------------------------------------

 OUTPUT PARAMETERS:
 Name				DataType		Default Description
------------------------------------------------------------

 USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Internal_User_Info_Dtl_Sel 
	
 AUTHOR INITIALS:
 Initials	Name
------------------------------------------------------------
 NR			Narayana Reddy			

 MODIFICATIONS
 Initials	Date			Modification
------------------------------------------------------------
 NR			2017-09-12		MAINT-5899 - Created.
******/

CREATE PROCEDURE [dbo].[Internal_User_Info_Dtl_Sel]
AS 
BEGIN

      SET NOCOUNT ON 

      SELECT
            ui.USER_INFO_ID
           ,ui.FIRST_NAME + ' ' + ui.LAST_NAME + ' (' + ui.USERNAME + ')' AS Display_User_Name
      FROM
            dbo.USER_INFO ui
      WHERE
            ACCESS_LEVEL = 0
            AND IS_HISTORY = 0
      ORDER BY
            ui.FIRST_NAME + ' ' + ui.LAST_NAME + '(' + ui.USERNAME + ')'

     

END

;
GO
GRANT EXECUTE ON  [dbo].[Internal_User_Info_Dtl_Sel] TO [CBMSApplication]
GO
