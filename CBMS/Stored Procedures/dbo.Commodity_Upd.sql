SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******    
NAME:    
	[dbo].[Commodity_Upd]  

DESCRIPTION:    
	Used to update the COMMODITY while delete the commodity from COMMODITY_EMISSION table

INPUT PARAMETERS:    
Name					DataType Default Description    
------------------------------------------------------------    
@Commodity_Id			INT,   
          
OUTPUT PARAMETERS:    
Name   DataType  Default Description    
------------------------------------------------------------    
USAGE EXAMPLES:    
------------------------------------------------------------  

EXEC dbo.Commodity_Upd  1

AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    


MODIFICATIONS     
Initials	Date		Modification    
------------------------------------------------------------    
Created new SP to delete commodity it is a part of Commodity_Emission_Source_Del

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[Commodity_Upd] 
	@Commodity_Id INT
AS  

BEGIN  

SET NOCOUNT ON;  
 
	BEGIN TRY
	BEGIN TRANSACTION  
	  
	  UPDATE dbo.COMMODITY   
	  SET IS_SUSTAINABLE = 0
	  WHERE COMMODITY_ID = @Commodity_Id    
	 
	 COMMIT TRANSACTION
	END TRY	 
	
	BEGIN CATCH
		ROLLBACK TRANSACTION
		EXEC dbo.usp_RethrowError
	END CATCH

END
GO
GRANT EXECUTE ON  [dbo].[Commodity_Upd] TO [CBMSApplication]
GO
