SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE [dbo].[cbmsDoNotTrack_CreateUBMMapping]
	( @MyAccountId int
	, @do_not_track_id int
	, @ubm_id int
	, @ubm_account_code varchar(200)
	)
AS
BEGIN

	SET NOCOUNT ON

	   update do_not_track
	      set ubm_id = @ubm_id
		, ubm_account_code = @ubm_account_code
	    where do_not_track_id = @do_not_track_id

--	SET NOCOUNT OFF

	exec cbmsDoNotTrack_Get @MyAccountId, @do_not_track_id


END
GO
GRANT EXECUTE ON  [dbo].[cbmsDoNotTrack_CreateUBMMapping] TO [CBMSApplication]
GO
