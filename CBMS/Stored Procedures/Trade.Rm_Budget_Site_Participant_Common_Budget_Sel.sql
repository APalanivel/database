SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.Rm_Budget_Site_Participant_Common_Budget_Sel                        
                          
Description:                          
        To get market price and forecast pirce if a selected index   
                          
Input Parameters:                          
    Name    DataType        Default     Description                            
--------------------------------------------------------------------------------    
	@Index_Id   INT    
    @Start_Dt	Date    
	@End_Dt		Date
                          
 Output Parameters:                                
	Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
Usage Examples:                              
--------------------------------------------------------------------------------    
	SELECT * FROM Trade.Rm_Budget_Participant rbp 
	WHERE rbp.Client_Hier_Id IN (5690,268359,673435,764782,764783) ORDER BY rbp.Client_Hier_Id
	EXEC Trade.Rm_Budget_Site_Participant_Common_Budget_Sel @Client_Hier_Id ='5690,268359,673435,764782,764783'
	EXEC Trade.Rm_Budget_Site_Participant_Common_Budget_Sel @Client_Hier_Id ='5690,268359,673435'
	EXEC Trade.Rm_Budget_Site_Participant_Common_Budget_Sel @Client_Hier_Id ='764782,764783'
	EXEC Trade.Rm_Budget_Site_Participant_Common_Budget_Sel @Client_Hier_Id ='5690,268359,673435'

	EXEC Trade.Rm_Budget_Site_Participant_Common_Budget_Sel  @Client_Id = 1005,@Start_Index=1,@End_Index = 25
    
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR			2019-12-23	RM-Budgets Enahancement - Created
	RR			2020-03-23	GRM-1788 - Applied the filters to select the budgtes that are completley covering input term	                
******/
CREATE PROCEDURE [Trade].[Rm_Budget_Site_Participant_Common_Budget_Sel]
    (
        @Commodity_Id INT = NULL
        , @Client_Hier_Id VARCHAR(MAX) = NULL
        , @Start_Dt DATE = NULL
        , @End_Dt DATE = NULL
        , @Index_Id INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Site_Cnt INT;

        SELECT
            @Site_Cnt = COUNT(DISTINCT us.Segments)
        FROM
            dbo.ufn_split(@Client_Hier_Id, ',') us;

        WITH Cte_Budgets
        AS (
               SELECT
                    rb.Rm_Budget_Id
                    , rb.Budget_NAME
                    , rb.Budget_Start_Dt
                    , rb.Budget_End_Dt
                    , indx.ENTITY_NAME AS Index_Name
                    , uom.ENTITY_NAME AS UOM_Name
                    , cu.CURRENCY_UNIT_NAME
                    , RANK() OVER (PARTITION BY
                                       rb.Rm_Budget_Id
                                   ORDER BY
                                       rbp.Client_Hier_Id) AS Rm_Budget_Cnt
                    , CASE rb.Is_Client_Generated WHEN 1 THEN 'Yes'
                          ELSE 'No'
                      END AS Is_Client_Generated
               FROM
                    Trade.Rm_Budget rb
                    INNER JOIN Trade.Rm_Budget_Participant rbp
                        ON rbp.Rm_Budget_Id = rb.Rm_Budget_Id
                    INNER JOIN dbo.ENTITY indx
                        ON indx.ENTITY_ID = rb.Index_Id
                    INNER JOIN dbo.ENTITY uom
                        ON rb.Uom_Type_Id = uom.ENTITY_ID
                    INNER JOIN dbo.CURRENCY_UNIT cu
                        ON rb.Currency_Unit_Id = cu.CURRENCY_UNIT_ID
               WHERE
                    EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.ufn_split(@Client_Hier_Id, ',') us
                               WHERE
                                    CAST(us.Segments AS INT) = rbp.Client_Hier_Id)
                    AND rb.Budget_Start_Dt <= @Start_Dt
                    AND rb.Budget_Start_Dt <= @End_Dt
                    AND rb.Budget_End_Dt >= @Start_Dt
                    AND rb.Budget_End_Dt >= @End_Dt
                    AND (   @Commodity_Id IS NULL
                            OR  rb.Commodity_Id = @Commodity_Id)
                    AND (   @Index_Id IS NULL
                            OR  rb.Index_Id = @Index_Id)
           )
        SELECT
            Rm_Budget_Id
            , Budget_NAME
            , Budget_Start_Dt
            , Budget_End_Dt
            , Index_Name
            , UOM_Name
            , CURRENCY_UNIT_NAME
            , Rm_Budget_Cnt
            , Is_Client_Generated
        FROM
            Cte_Budgets
        WHERE
            Rm_Budget_Cnt = @Site_Cnt
        GROUP BY
            Rm_Budget_Id
            , Budget_NAME
            , Budget_Start_Dt
            , Budget_End_Dt
            , Index_Name
            , UOM_Name
            , CURRENCY_UNIT_NAME
            , Rm_Budget_Cnt
            , Is_Client_Generated;



    END;


GO
GRANT EXECUTE ON  [Trade].[Rm_Budget_Site_Participant_Common_Budget_Sel] TO [CBMSApplication]
GO
